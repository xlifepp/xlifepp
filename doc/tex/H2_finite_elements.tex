%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lun\'eville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,svgnames,english]{article}

\usepackage[mode=user,toplevel=chapter]{xlifepp}
\usepackage{amsfonts}
\usepackage{amsmath}

\newcommand\va{\mathbf{a}}
\newcommand\vb{\mathbf{b}}
\newcommand\vc{\mathbf{c}}
\newcommand\vd{\mathbf{d}}
\newcommand\vzo{\mathbf{0}}
\newcommand\vE{\mathbf{E}}
\newcommand\vH{\mathbf{H}}
\newcommand\vn{\mathbf{n}}
\newcommand\vU{\mathbf{U}}
\newcommand\vV{\mathbf{V}}
\newcommand\Id{\text{Id}}
\newcommand\curl{\text{curl\,}}
\newcommand\vcurl{\textbf{curl\,}}
\newcommand\vgrad{\textbf{grad\,}}
\newcommand\vlambda{\boldsymbol{\lambda}}
\newcommand\vpsi{\boldsymbol{\Psi}}
\newcommand\vmu{\boldsymbol{\mu}}
\newcommand\veta{\boldsymbol{\eta}}
\newcommand\vzeta{\boldsymbol{\zeta}}
\newcommand\vtheta{\boldsymbol{\theta}}
\newcommand\vtau{\boldsymbol{\tau}}
\newcommand\vsigma{\boldsymbol{\sigma}}
\newcommand\vvarphi{\boldsymbol{\varphi}}
\newcommand\vxi{\boldsymbol{\xi}}
\renewcommand\div{\text{div}\,}
\newcommand\grad{\text{grad}\,}
\newcommand\half{\frac{1}{2}}
\newcommand\Rn{\mathbb{R}^3}
\newcommand\dsp{\displaystyle}

%\date{} % date %
\title{H2 finite elements} % titre %
\author{Eric {\scshape Lun\'eville}} % auteur %

\begin{document}
\maketitle

The purpose of this paper is to list the main finite elements that can be used to attack fourth order PDE (e.g. bilaplacian equation), more precisely finite elements that lead to C1 approximations that are required to get H2 conformity. To guarantee C1 continuity, such elements generally involve the values and some derivatives at  element nodes.

\section{1D elements}
Let \(\widehat{S}=[0,1]\) the unit segment.

\subsection*{Cubic Hermite (HC1-P3)}

\[
\quad \quad(H1D-P3)\quad\quad\left|\begin{array}{lll}
\dsp \widehat{K}&=&\widehat{S}\\
\dsp \widehat{P}&=&P^3[\widehat{x}]=\text{span}\big\{1,\widehat{x},\widehat{x}^2,\widehat{x}^3\big\}\\
\dsp \widehat{\Sigma}&=&\big\{p(0),p(1),p'(0),p'(1))\big\}
\end{array}\right.
\]

\begin{minipage}{11cm}
with shape functions:
\[
\begin{array}{l}
    \dsp \tau_1(\widehat{x})=2\widehat{x}^3-3\widehat{x}^2+1\\
    \dsp \tau_2(\widehat{x})=3\widehat{x}^2-2\widehat{x}^3
\end{array}
\quad
\begin{array}{l}
    \dsp \tilde{\tau}_1(\widehat{x})=\widehat{x}^3-2\widehat{x}^2\\
    \dsp \tilde{\tau}_2(\widehat{x})=\widehat{x}^3-\widehat{x}^2 
\end{array}
\]
\end{minipage}
\begin{minipage}{6cm}
\begin{figure}[H]
    \centering
    \includePict[width=6cm]{hermite1D_shape_functions.png}
\end{figure}
\end{minipage}

\subsection*{Hermite (HC1-Pk)}

The cubic Hermite element may be generalized using C1-Pk spline:

\[
\quad \quad(H1D-Pk)\quad \quad\left|\begin{array}{lll}
    \dsp \widehat{K}&=&\widehat{S}\\
    \dsp \widehat{P}&=&P^k[\widehat{x}]=\text{span}\big\{1,\widehat{x},\dots,\widehat{x}^k\big\}\\
    \dsp \widehat{\Sigma}&=&\big\{p(0),p(1),p'(0),p'(1), p\big(\frac{i}{k-2}\big),\ 1\leq i\leq k-3\big\}
\end{array}\right.
\]

For instance, the shape functions for Hermite \(k=4\) are given by
\[
\begin{array}{ll}
    \dsp \tau_1(\widehat{x})=-8\widehat{x}^4+18\widehat{x}^3-11\widehat{x}^2+1& (\tau_1(0)=1)\\
    \dsp \tau_2(\widehat{x})=-8\widehat{x}^4+14\widehat{x}^3-5\widehat{x}^2& (\tau_2(1)=1)\\
    \dsp \tau_3(\widehat{x})=16\widehat{x}^4-32\widehat{x}^3+16\widehat{x}^2& (\tau_3\big(\frac{1}{2}\big)=1)
\end{array}
\quad
\begin{array}{ll}
    \dsp \tilde{\tau}_1(\widehat{x})=-2\widehat{x}^4+5\widehat{x}^3-4\widehat{x}^2+\widehat{x}& (\tilde{\tau}_1'(0)=1)\\
    \dsp \tilde{\tau}_2(\widehat{x})=2\widehat{x}^4-3\widehat{x}^3+\widehat{x}^2& (\tilde{\tau}_2'(1)=1)\\
\end{array}
\]

\subsection*{Quintic Hermite (HC2-P5)}

\[
(H1DC2-P5)\quad \quad\left|\begin{array}{lll}
    \dsp \widehat{K}&=&\widehat{S}\\
    \dsp \widehat{P}&=&P^5[\widehat{x}]=\text{span}\big\{1,\widehat{x},\widehat{x}^2,\widehat{x}^3,\widehat{x}^4,\widehat{x}^5\big\}\\
    \dsp \widehat{\Sigma}&=&\big\{p(0),p(1),p'(0),p'(1),p''(0),p''(1)\big\}
\end{array}\right.
\]

\section{Triangle elements}

In the sequel, we note the vertices of the unit triangle : \(\widehat{M}_1=(0,0),\ \widehat{M}_2=(1,0),\ \widehat{M}_3=(0,1)\).

\subsection{2D Hermite}

\[
(H-P3)\quad \quad\left|\begin{array}{lll}
    \dsp \widehat{K}&=&\widehat{T}\\
    \dsp \widehat{P}&=&P^3[\widehat{x},\widehat{y}]=\text{span}\big\{\widehat{x}^i\widehat{y}^j,\ 0\leq i+j\leq 3\big\}, \ \mbox{dim}\,P^3=10\\[5pt]
    \dsp \widehat{\Sigma}&=&\left\{
    \begin{array}{l}
        p(\widehat{M}_i),\ \partial_xp(\widehat{M}_i),\ \partial_yp(\widehat{M}_i),,\ i=1,2,3,\ p(\widehat{M}_{123}) 
    \end{array}
    \right\}
\end{array}\right.
\]

\begin{figure}[H]
    \centering
    \includePict[width=5cm]{Hermite_P3.png}
    \caption{The Hermite P3 in 2D}
\end{figure}
The H\_P3 Hermite element leads to \(C^0\) approximation, but not \(C^1\); continuity of derivatives occurs only on vertices!\\

Since the Hermite element involves normal derivatives, care must be taken when the element is transported by affine transformation. Denote \(\big(\widehat{\cal{L}}_i\big)_{1\leq i\leq 21}\) the linear forms defining the set of DoFs \(\widehat{\Sigma}\) and let \(\big(\widehat{\tau}_i\big)_{1\leq i\leq 10}\in P^3[\widehat{x},\widehat{y}]\) be the associated shape function, i.e. \(\widehat{\cal{L}}_i(\widehat{\tau}_j)=\delta_{ij},\ \forall 1\leq i,j\leq 10\). Let \(T\) be any triangle with vertices \(M_1,M_2,M_3\), its DoFs  \(\big({\cal L}_i\big)_{1\leq i\leq 10}\) ant its shape functions  \(\big(\tau_i\big)_{1\leq i\leq 21}\) (\({\cal L}_i(\tau_j)=\delta_{ij}\)). We consider the affine transformation \(F\) that maps \(\widehat{T}\) to \(T\) : 
\[F(\widehat{x},\widehat{y})=B\left(\begin{array}{c}\widehat{x}\\\widehat{y}\end{array}\right)+b.\]
Following \cite{K2018}, the shape functions \(\boldsymbol{\tau}=\big(\tau_1,\ldots,\tau_{10}\big)^t\) are related to reference shape functions \(\widehat{\boldsymbol{\tau}}=\big(\widehat{\tau}_1,\ldots,\widehat{\tau}_{10}\big)^t\) with the \(10\times 10\) matrix \(\mathbb{M}\):

\[
\boldsymbol{\tau}=\mathbb{M}\big(\widehat{\boldsymbol{\tau}}\circ F^{-1}\big),\quad
\mathbb{M}=\left[\begin{array}{ccccccc}
    1& & & & & &  \\[-3pt]
     &B& & & & &  \\[-3pt]
     & &1& & & &  \\[-3pt]
     & & &B& & &  \\[-3pt]
     & & & &1& &  \\[-3pt]
     & & & & &B&  \\[-3pt]
     & & & & & &1 
     \end{array}
\right]
\]

\subsection{The Argyris element}

The Argyris element is an H2 conform element designed for triangles. On the unit triangle \(\widehat{T}\) with vertices \(\widehat{M}_1=(0,0),\widehat{M}_2=(1,0),\widehat{M}_3=(0,1)\), it is defined as\\

\[
(ARG)\quad \quad\left|\begin{array}{lll}
    \dsp \widehat{K}&=&\widehat{T}\\
    \dsp \widehat{P}&=&P^5[\widehat{x},\widehat{y}]=\text{span}\big\{\widehat{x}^i\widehat{y}^j,\ 0\leq i+j\leq 5\big\}, \ \mbox{dim}\,P^5=21\\[5pt]
    \dsp \widehat{\Sigma}&=&\left\{
    \begin{array}{l}
        p(\widehat{M}_i),\ \partial_xp(\widehat{M}_i),\ \partial_yp(\widehat{M}_i),\ \partial_{xx}p(\widehat{M}_i),\ \partial_{yy}p(\widehat{M}_i),\ \partial_{xy}p(\widehat{M}_i),\ \partial_{n_i}p(\widehat{N}_{i}),\ i=1,2,3 
    \end{array}
    \right\}
\end{array}\right.
\]

 \begin{figure}[H]
     \centering
     \includePict[width=5cm]{Argyris.png}
     \caption{The Argyris element}
 \end{figure}
Again, to transport reference shape functions to any element, the more complex transformation has to be used (see \cite{K2018},\cite{DS2008}):
{\setlength{\arraycolsep}{1pt}

\[
\begin{array}{cccc}
& \mathbb{E}\mbox{ matrix }21\times 24 & \mathbb{V}\mbox{ matrix }24\times 24&\mathbb{D}\mbox{ matrix }24\times 21\\
\mathbb{M}^t=&\left[\begin{array}{cccccccccccc}
    1&  &   & &   &   & &   &   &   &   &\\[0pt]
    &I_2&   & &   &   & &   &   &   &   &\\[0pt]
    &   &I_3& &   &   & &   &   &   &   &\\[0pt]
    &   &   &1&   &   & &   &   &   &   &\\[0pt]
    &   &   & &I_2&   & &   &   &   &   &\\[0pt]
    &   &   & &   &I_3& &   &   &   &   &\\[0pt]
    &   &   & &   &   &1&   &   &   &   &\\[0pt]
    &   &   & &   &   & &I_2&   &   &   &\\[0pt]
    &   &   & &   &   & &   &I_3&   &   &\\[0pt]
    &   &   & &   &   & &   &   &P_1&P_2&P_3
\end{array}\right]&
\left[\begin{array}{cccccccccccc}
    1&   &      & &   &      & &   &      &   &   &\\[0pt]
     &B^t&      & &   &      & &   &      &   &   &\\[0pt]
     &   &\Theta& &   &      & &   &      &   &   &\\[0pt]
     &   &      &1&   &      & &   &      &   &   &\\[0pt]
     &   &      & &B^t&      & &   &      &   &   &\\[0pt]
     &   &      & &   &\Theta& &   &      &   &   &\\[0pt]
     &   &      & &   &      &1&   &      &   &   &\\[0pt]
     &   &      & &   &      & &B^t&      &   &   &\\[0pt]
     &   &      & &   &      & &   &\Theta&   &   &\\[0pt]
     &   &      & &   &      & &   &      &Q_1&   &\\[0pt]
     &   &      & &   &      & &   &      &   &Q_2&\\[0pt]
     &   &      & &   &      & &   &      &   &   &Q_3
\end{array}\right]&
\left[\begin{array}{cccccccccc}
    1  &        &       &       &        &       &      &        &      &     \\[0pt]
       &I_2     &       &       &        &       &      &        &      &     \\[0pt]
       &        &I_3    &       &        &       &      &        &      &     \\[0pt]
       &        &       &   1   &        &       &      &        &      &     \\[0pt]
       &        &       &       &I_2     &       &      &        &      &     \\[0pt]
       &        &       &       &        &I_3    &      &        &      &     \\[0pt]
       &        &       &       &        &       &1     &        &      &     \\[0pt]
       &        &       &       &        &       &      &I_2     &      &     \\[0pt]
       &        &       &       &        &       &      &        &I_3   &     \\[0pt]
       &        &       &-\boldsymbol{\rho}_1&\boldsymbol{\sigma}_1&-\boldsymbol{\tau}_1&\boldsymbol{\rho}_1&\boldsymbol{\sigma}_1&\boldsymbol{\tau}_1&P_1^t\\[0pt]
-\boldsymbol{\rho}_2&\boldsymbol{\sigma}_2&-\boldsymbol{\tau}_2&       &        &       &\boldsymbol{\rho}_2&\boldsymbol{\sigma}_2&\boldsymbol{\tau}_2&P_2^t\\[0pt]
-\boldsymbol{\rho}_3&\boldsymbol{\sigma}_3&-\boldsymbol{\tau}_3&\boldsymbol{\rho}_3 &\boldsymbol{\sigma}_3&\boldsymbol{\tau}_3 &      &        &      &P_2^t\\[0pt]
\end{array}\right]
\end{array}
\]
}
with  (\(\boldsymbol{\tau}=(-n^y,n^x)^t\))
{\setlength{\arraycolsep}{1pt}
\[
P_1=\left[\begin{array}{cc}1&0\\[0pt]0&0\\[0pt]0&0\end{array}\right]\quad
P_2=\left[\begin{array}{cc}0&0\\[0pt]1&0\\[0pt]0&0\end{array}\right]\quad
P_3=\left[\begin{array}{cc}0&0\\[0pt]0&0\\[0pt]1&0\end{array}\right]\quad
Q^i=\left[\begin{array}{cc}\widehat{\boldsymbol{n}}_i&\widehat{\boldsymbol{t}}_i\end{array}\right]^tB^t\left[\begin{array}{cc}\boldsymbol{n}_i&\boldsymbol{t}_i\end{array}\right]\quad i=1,2,3
\]
}
{\setlength{\arraycolsep}{2pt}
\[
\Theta=\left[\begin{array}{ccc}
b_{11}^2&2b_{11}b_{21}&b_{21}^2\\[0pt]
b_{12}b_{11}&b_{12}b_{21}+b_{11}b_{22}&b_{21}b_{22}\\
b_{12}^2&2b_{22}b_{12}&b_{22}^2
\end{array}\right]
\]
}

\[
\boldsymbol{\rho}_i=\left[\begin{array}{c}0\\[0pt]\frac{15}{8\ell_i}\end{array}\right]\quad \boldsymbol{\sigma}_i=\left[\begin{array}{c}\boldsymbol{0}\\\frac{7}{16}\boldsymbol{t}_i^t\end{array}\right]\quad
\boldsymbol{\tau}_i=\frac{\ell_i}{32}\left[\begin{array}{ccc}0&0&0\\\big(t_i^x\big)^2&2t_i^xt_i^y&\big(t_i^y\big)^2\end{array}\right]\quad i=1,2,3
\]

\[
\ell_1=|M_3-M_2|,\ \ell_2=|M_3-M_1|,\ \ell_3=|M_2-M_1|.
\]

It reads also
{\setlength{\arraycolsep}{2pt}
\[
\mathbb{M}=\left[\begin{array}{cccccccccc}
   1& &        & & &        & & &        &\boldsymbol{\alpha}_1^t\\[0pt]
    &B&        & & &        & & &        &\boldsymbol{\alpha}_2^t\\[0pt]
    & &\Theta^t& & &        & & &        &\boldsymbol{\alpha}_3^t\\[0pt]
    & &        &1& &        & & &        &\boldsymbol{\alpha}_4^t\\[0pt]
    & &        & &B&        & & &        &\boldsymbol{\alpha}_5^t\\[0pt]
    & &        & & &\Theta^t& & &        &\boldsymbol{\alpha}_6^t\\[0pt]
    & &        & & &        &1& &        &\boldsymbol{\alpha}_7^t\\[0pt]
    & &        & & &        & &B&        &\boldsymbol{\alpha}_8^t\\[0pt]
    & &        & & &        & & &\Theta^t&\boldsymbol{\alpha}_9^t\\[0pt]
    & &        & & &        & & &        &\boldsymbol{\alpha}_{10}^t
\end{array}\right]
\]
}

\[
\begin{array}{l}\begin{array}{lll}\boldsymbol{\alpha}_1=-P_2Q^2\boldsymbol{\rho}_2-P_3Q^3\boldsymbol{\rho}_3 & \boldsymbol{\alpha}_2=P_2Q^2\boldsymbol{\sigma}_2+P_3Q^3\boldsymbol{\sigma}_3& \boldsymbol{\alpha}_3=-P_2Q^2\boldsymbol{\tau}_2-P_3Q^3\boldsymbol{\tau}_3\\
\boldsymbol{\alpha}_4=-P_1Q^1\boldsymbol{\rho}_1+P_3Q^3\boldsymbol{\rho}_3 & \boldsymbol{\alpha}_5=P_1Q^1\boldsymbol{\sigma}_1+P_3Q^3\boldsymbol{\sigma}_3& \boldsymbol{\alpha}_6=-P_1Q^1\boldsymbol{\tau}_1+P_3Q^3\boldsymbol{\tau}_3\\[0pt]
\boldsymbol{\alpha}_7=P_1Q^1\boldsymbol{\rho}_1+P_2Q^2\boldsymbol{\rho}_2 & \boldsymbol{\alpha}_8=P_1Q^1\boldsymbol{\sigma}_1+P_2Q^2\boldsymbol{\sigma}_2& \boldsymbol{\alpha}_9=P_1Q^1\boldsymbol{\tau}_1+P_2Q^2\boldsymbol{\tau}_2\end{array}\\[0pt]
\ \boldsymbol{\alpha}_{10}=P_1Q^1P_1^t+P_2Q^2P_2^t+P_3Q^3P_3^t.
\end{array}
\]

Note that

{\setlength{\arraycolsep}{1pt}
\[
\begin{array}{l}
P_1Q_1=\left[\begin{array}{cc}Q^1_{11} & Q^1_{12}\\0&0\\0&0\end{array}\right]\quad
P_2Q_2=\left[\begin{array}{cc}0&0\\Q^2_{11} & Q^2_{12}\\0&0\end{array}\right]\quad
P_3Q_3=\left[\begin{array}{cc}0&0\\0&0\\Q^3_{11} & Q^3_{12}\end{array}\right]\quad
\boldsymbol{\alpha}_{10}=\left[\begin{array}{ccc}Q^1_{11} & 0 &0\\0&Q^2_{11}&0\\0&0&Q^3_{11}\end{array}\right]\\[-5pt]
\\
\begin{array}{llll}
\mbox{with } &Q_{12}^1=b_{11}n^y_1-b_{21}n^x_1& \quad Q_{12}^2=b_{22}n^x_2-b_{12}n^y_2&\quad Q_{12}^3=\frac{\sqrt{2}}{2}\big((b_{21}+b_{22})n^x_3-(b_{11}+b_{12})n^y_3\big)\\[5pt]
&Q_{11}^1=-(b_{11}n^x_1+b_{21}n^y_1)& \quad Q_{11}^2=-(b_{12}n^x_2+b_{22}n^y_2)&\quad Q_{11}^3=\frac{\sqrt{2}}{2}\big((b_{11}+b_{12})n^x_3+(b_{21}+b_{22})n^y_3\big)
\end{array}
\end{array}
\]
}
We get \small
\[
\begin{array}{ll}
\boldsymbol{\alpha}_{1} &=-\left[\begin{array}{cc}0&0\\Q^2_{11} &Q^2_{12}\\0&0\end{array}\right]
                         \left[\begin{array}{c}0\\[0pt]\frac{15}{8\ell_2}\end{array}\right]
                        -\left[\begin{array}{cc}0&0\\0&0\\Q^3_{11} & Q^3_{12}\end{array}\right]
                         \left[\begin{array}{c}0\\[0pt]\frac{15}{8\ell_3}\end{array}\right]
                       =-\frac{15}{8}\left[\begin{array}{c}0\\[0pt]\frac{Q^2_{12}}{\ell_2}\\[0pt] \frac{Q^3_{12}}{\ell_3}\end{array}\right]\\
\boldsymbol{\alpha}_{2}&=\frac{7}{16}\left[\begin{array}{cc}0&0\\Q^2_{11}&Q^2_{12}\\0&0\end{array}\right]
                        \left[\begin{array}{cc}0&0\\t_2^x&t_2^y\end{array}\right] 
                       +\frac{7}{16}\left[\begin{array}{cc}0&0\\0&0\\Q^3_{11}&Q^3_{12}\end{array}\right]
                        \left[\begin{array}{cc}0&0\\t_3^x&t_3^y\end{array}\right] 
                       =\frac{7}{16}\left[\begin{array}{cc}0&0\\Q^2_{12}t_2^x & Q^2_{12}t_2^y\\Q^3_{12}t_3^x & Q^3_{12}t_3^y \end{array}\right]\\
\boldsymbol{\alpha}_{3}&=-\left[\begin{array}{cc}0&0\\Q^2_{11} &Q^2_{12}\\0&0\end{array}\right]
                         \frac{\ell_2}{32}\left[\begin{array}{ccc}0&0&0\\\big(t_2^x\big)^2&2t_2^xt_2^y&\big(t_2^y\big)^2\end{array}\right]
                        -\left[\begin{array}{cc}0&0\\0&0\\Q^3_{11} & Q^3_{12}\end{array}\right]
                         \frac{\ell_3}{32}\left[\begin{array}{ccc}0&0&0\\\big(t_3^x\big)^2&2t_3^xt_3^y&\big(t_3^y\big)^2\end{array}\right]\\
                        &=-\frac{1}{32}\left[\begin{array}{ccc}0&0&0\\\ell_2Q^2_{12}(t_2^x)^2 & 2\ell_2Q^2_{12}t_2^xt_2^y & \ell_2Q^2_{12}(t_2^y)^2\\\ell_3Q^3_{12}(t_3^x)^2 & 2\ell_3Q^3_{12}t_3^xt_3^y & \ell_3Q^3_{12}(t_3^y)^2\end{array}\right]\\
\boldsymbol{\alpha}_{4} &=-\left[\begin{array}{cc}Q^1_{11} &Q^1_{12}\\0&0\\0&0\\\end{array}\right]
                           \left[\begin{array}{c}0\\[0pt]\frac{15}{8\ell_1}\end{array}\right]
                          +\left[\begin{array}{cc}0&0\\0&0\\Q^3_{11} & Q^3_{12}\end{array}\right]
                           \left[\begin{array}{c}0\\[0pt]\frac{15}{8\ell_3}\end{array}\right]
                         =\frac{15}{8}\left[\begin{array}{c}-\frac{Q^1_{12}}{\ell_1}\\[0pt]0\\[0pt] \frac{Q^3_{12}}{\ell_3}\end{array}\right]\\
\boldsymbol{\alpha}_{5}&=\frac{7}{16}\left[\begin{array}{cc}Q^1_{11} &Q^1_{12}\\0&0\\0&0\\\end{array}\right]
                         \left[\begin{array}{cc}0&0\\t_1^x&t_1^y\end{array}\right] 
                        +\frac{7}{16}\left[\begin{array}{cc}0&0\\0&0\\Q^3_{11}&Q^3_{12}\end{array}\right]
                          \left[\begin{array}{cc}0&0\\t_3^x&t_3^y\end{array}\right] 
                        =\frac{7}{16}\left[\begin{array}{cc}Q^1_{12}t_1^x & Q^1_{12}t_1^y\\0&0\\Q^3_{12}t_3^x & Q^3_{12}t_3^y \end{array}\right]\\
\boldsymbol{\alpha}_{6}&=-\left[\begin{array}{cc}Q^1_{11} &Q^1_{12}\\0&0\\0&0\\\end{array}\right]
                          \frac{\ell_1}{32}\left[\begin{array}{ccc}0&0&0\\\big(t_1^x\big)^2&2t_1^xt_1^y&\big(t_1^y\big)^2\end{array}\right]
                         +\left[\begin{array}{cc}0&0\\0&0\\Q^3_{11} & Q^3_{12}\end{array}\right]
                          \frac{\ell_3}{32}\left[\begin{array}{ccc}0&0&0\\\big(t_3^x\big)^2&2t_3^xt_3^y&\big(t_3^y\big)^2\end{array}\right]\\
                       &=\frac{1}{32}\left[\begin{array}{ccc}-\ell_1Q^1_{12}(t_1^x)^2 & -2\ell_1Q^1_{12}t_1^xt_1^y &  -\ell_1Q^1_{12}(t_1^y)^2\\0&0&0\\\ell_3Q^3_{12}(t_3^x)^2 & 2\ell_3Q^3_{12}t_3^xt_3^y & \ell_3Q^3_{12}(t_3^y)^2\end{array}\right]\\
\boldsymbol{\alpha}_{7} &=\left[\begin{array}{cc}Q^1_{11} &Q^1_{12}\\0&0\\0&0\\\end{array}\right]
                       \left[\begin{array}{c}0\\[0pt]\frac{15}{8\ell_1}\end{array}\right]
                       +\left[\begin{array}{cc}0&0\\Q^2_{11} & Q^2_{12}\\0&0\end{array}\right]
                       \left[\begin{array}{c}0\\[0pt]\frac{15}{8\ell_2}\end{array}\right]
                       =\frac{15}{8}\left[\begin{array}{c}\frac{Q^1_{12}}{\ell_1}\\[0pt] \frac{Q^2_{12}}{\ell_2}\\[0pt] 0\end{array}\right]\\
\boldsymbol{\alpha}_{8}&=\frac{7}{16}\left[\begin{array}{cc}Q^1_{11} &Q^1_{12}\\0&0\\0&0\\\end{array}\right]
                       \left[\begin{array}{cc}0&0\\t_1^x&t_1^y\end{array}\right] 
                       +\frac{7}{16}\left[\begin{array}{cc}0&0\\Q^2_{11} & Q^2_{12}\\0&0\end{array}\right]
                       \left[\begin{array}{cc}0&0\\t_2^x&t_2^y\end{array}\right] 
                       =\frac{7}{16}\left[\begin{array}{cc}Q^1_{12}t_1^x & Q^1_{12}t_1^y\\Q^2_{12}t_2^x & Q^2_{12}t_2^y\\0&0\end{array}\right]\\
\boldsymbol{\alpha}_{9}&=\left[\begin{array}{cc}Q^1_{11} &Q^1_{12}\\0&0\\0&0\\\end{array}\right]
                       \frac{\ell_1}{32}\left[\begin{array}{ccc}0&0&0\\\big(t_1^x\big)^2&2t_1^xt_1^y&\big(t_1^y\big)^2\end{array}\right]
                       +\left[\begin{array}{cc}0&0\\Q^2_{11} & Q^2_{12}\\0&0\end{array}\right]
                       \frac{\ell_2}{32}\left[\begin{array}{ccc}0&0&0\\\big(t_2^x\big)^2&2t_2^xt_2^y&\big(t_2^y\big)^2\end{array}\right]\\
                       &=\frac{1}{32}\left[\begin{array}{ccc}\ell_1Q^1_{12}(t_1^x)^2 & 2\ell_1Q^1_{12}t_1^xt_1^y &  \ell_1Q^1_{12}(t_1^y)^2\\\ell_2Q^2_{12}(t_2^x)^2 & 2\ell_2Q^2_{12}t_2^xt_2^y & \ell_2Q^2_{12}(t_2^y)^2\\0&0&0\end{array}\right]\\
\end{array}
\]
\normalsize
So the shape functions are related to the reference shape functions as following:
\begin{itemize}
\item For a nodal DOF : \(\phi_i=\widehat{\phi_i}\circ F^{-1}+\boldsymbol{\alpha}_{3i-2}^t\widehat{\phi_{\boldsymbol{n}}}\circ F^{-1},\ i=1,2,3\)
\item For the 2 derivative DOFs : \(d\phi_i=B\widehat{d\phi_i}\circ F^{-1}+\boldsymbol{\alpha}_{3i-1}^t\widehat{\phi_{\boldsymbol{n}}}\circ F^{-1},\ i=1,2,3\)
\item For the 3 second derivative DOFs :  \(d2\phi_i=\Theta^t\widehat{d2\phi_i}\circ F^{-1}+\boldsymbol{\alpha}_{3i}^t\widehat{\phi_{\boldsymbol{n}}}\circ F^{-1},\ i=1,2,3\)
\item For the 3 normal derivative DOFs : \(\displaystyle 
    \phi_{\boldsymbol{n}}=\boldsymbol{\alpha}_{10}\widehat{\phi_{\boldsymbol{n}}}\circ F^{-1}\).
\end{itemize}

When computing the elementary matrix \(\mathbb{A}^K_{ij}=a(\boldsymbol{\Phi}_i,\boldsymbol{\Phi}_j)\), it is more efficient to compute \(\mathbb{A}^{\widehat{K}}_{ij}=a(\widehat{\boldsymbol{\Phi}}_i,\widehat{\boldsymbol{\Phi}}_j)\) and then to do the product \(\mathbb{A}^K=\mathbb{M}\mathbb{A}^{\widehat{K}}\mathbb{M}^t\). Because it avoids applying matrix \(\mathbb{M}\) at each quadrature point.

\subsection{The Morley finite element}

The Morley element is designed for simplex (triangle in 2D, tetrahedron in 3D). It leads to non-conforming approximations (neither in \(H^2\) nor in \(H^1\)) but gives convergent approximations (order 1), see \cite{MZ2006}. 

\subsubsection*{Morley element on triangle}

On the unit triangle \(\widehat{T}\) with vertices \(\widehat{M}_1=(0,0),\widehat{M}_2=(1,0),\widehat{M}_3=(0,1)\), it is defined as\\

\[(MOR2D)\quad \quad\left|\begin{array}{lll}
    \dsp \widehat{K}&=&\widehat{T}\\
    \dsp \widehat{P}&=&P^2[\widehat{x},\widehat{y}]=\text{span}\big\{\widehat{x}^i\widehat{y}^j,\ 0\leq i+j\leq 2\big\}, \ \mbox{dim}\,P^2=6\\[5pt]
    \dsp \widehat{\Sigma}&=&\left\{
    \begin{array}{l}
        p(\widehat{M}_i),\partial_{n_i}p(\widehat{N}_{i}),\ i=1,2,3 
    \end{array}
    \right\}
\end{array}\right.
\]

\begin{figure}[H]
    \centering
    \includePict[width=5cm]{MorleyTriangle.png}
    \caption{The Morley element on triangle}
\end{figure}
Following \cite{K2018}, the shape functions \(\boldsymbol{\tau}=\big(\tau_1,\ldots,\tau_{6}\big)^t\) are related to reference shape functions \(\widehat{\boldsymbol{\tau}}=\big(\widehat{\tau}_1,\ldots,\widehat{\tau}_{6}\big)^t\) with the \(6\times 6\) matrix \(\mathbb{M}\): 
{\setlength{\arraycolsep}{2pt}
\[
\boldsymbol{\tau}=\mathbb{M}\big(\widehat{\boldsymbol{\tau}}\circ F^{-1}\big),\quad
\mathbb{M}=\left[\begin{array}{cccccc}
\ 1\ &0    &  0  &           0                         &\displaystyle \frac{-Q_{12}^2}{\ell_2}&\displaystyle\frac{-Q_{12}^3}{\ell_3}\\[7pt]
0    &\ 1\ &  0  &\displaystyle\frac{-Q_{12}^1}{\ell_1}&          0                           &\displaystyle\frac{Q_{12}^3}{\ell_3} \\[7pt]
0    &0    &\ 1\ &\displaystyle\frac{Q_{12}^1}{\ell_1} &\displaystyle\frac{Q_{12}^2}{\ell_2}  &   0                                 \\[7pt]
0    &0    &0    &Q^1_{11}                             &          0                           &   0                                 \\[7pt]
0    &0    &0    &        0                            &          Q^2_{11}                    &   0                                 \\[7pt]
0    &0    &0    &        0                            &          0                           &   Q^3_{11}
\end{array}
\right]
\]

with \(Q^i=\left[\begin{array}{cc}\widehat{\boldsymbol{n}}_i&\widehat{\boldsymbol{t}}_i\end{array}\right]^tB^t\left[\begin{array}{cc}\boldsymbol{n}_i&\boldsymbol{t}_i\end{array}\right]\quad i=1,2,3.\)
}\\

On any triangle (\((\lambda_i)_{1\leq i\leq n+1}\) the barycentric coordinates), the shape functions are given by:
\[
\left|
\begin{array}{l}
    \tau_1=\dsp 1-\lambda_2-\lambda_3+2\lambda_2\lambda_3 -(\nabla\lambda_2,\nabla \lambda_3)\Big(\frac{\lambda_2(\lambda_2-1)}{\|\nabla \lambda_2\|^2}
    +\frac{\lambda_3(\lambda_3-1)}{\|\nabla \lambda_3\|^2} \Big)\\[8pt]
    \tau_2=\dsp 1-\lambda_1-\lambda_3+2\lambda_1\lambda_3 -(\nabla\lambda_1,\nabla \lambda_3)\Big(\frac{\lambda_1(\lambda_1-1)}{\|\nabla \lambda_1\|^2}
    +\frac{\lambda_3(\lambda_3-1)}{\|\nabla \lambda_3\|^2} \Big)\\[8pt]
    \tau_3=\dsp 1-\lambda_1-\lambda_2+2\lambda_1\lambda_2 -(\nabla\lambda_1,\nabla \lambda_2)\Big(\frac{\lambda_1(\lambda_1-1)}{\|\nabla \lambda_i\|^2}
    +\frac{\lambda_2(\lambda_2-1)}{\|\nabla \lambda_2\|^2} \Big)\\
    \tau_{k+3}=\dsp \frac{1}{\|\nabla\lambda_k\|}\lambda_k(\lambda_k-1)\quad1\leq k\leq 3\\
\end{array}
\right..
\]

Taking \(\lambda_1=1-\widehat{x}-\widehat{y},\ \lambda_2=\widehat{x}\) and \(\lambda_3=\widehat{y}\) we get the shape functions of the reference element.
\subsubsection*{Morley element in any dimension}
Morley element can be generalized in any dimension on \(\widehat{T}_n\) simplex (see \cite{MZ2006}) :\\

\[
(MORnD)\quad \quad\left|\begin{array}{lll}
    \dsp \widehat{K}&=&\widehat{T}_n\\
    \dsp \widehat{P}&=&P^2[\widehat{x}_1,\dots\widehat{x}_n]\\[5pt]
    \dsp \widehat{\Sigma}&=&\left\{
    \begin{array}{l}
        p(\widehat{M}_{ij}),\ 1\leq i<j \leq n+1,\partial_{n_i}p(\widehat{N}_{i}),\ 1\leq i \leq n+1
    \end{array}
    \right\}
\end{array}\right.
\]

with \(\widehat{M}_{ij}\) barycenter of the  subsubsimplex \(\widehat{S}_{ij}\) without vertices \(\widehat{M}_i,\widehat{M}_j\), and \(\widehat{N}_i\) the barycenter of the subsimplex \(\widehat{F}_i\) without vertices \(\widehat{M}_i\).  Dofs can also be interpreting as

\[
\dsp \widehat{\Sigma}=\left\{
    \displaystyle\frac{1}{|\widehat{S}_{ij}|}\int_{\widehat{S}_{ij}}p\quad 1\leq i<j \leq n+1,\quad
    \displaystyle\frac{1}{|\widehat{F}_{i}|}\int_{\widehat{F}_{i}}\frac{\partial p}{\partial n}\quad 1\leq i \leq n+1 
\right\}.
\]
\begin{figure}[H]
    \centering
    \includePict[width=5cm]{MorleyTetrahedron.png}
    \caption{The Morley element on tetrahedron (10 DoFs)}
\end{figure}
On any n-simplex (\((\lambda_i)_{1\leq i\leq n+1}\) the barycentric coordinates), the shape functions are given by:

\[
\left|
\begin{array}{rclcll}
    \mbox{normal DoFs} & : & \varphi_i&=&\dsp \frac{1}{2\|\nabla\lambda_i\|}\lambda_i(n\lambda_i-2)&1\leq i\leq n+1\\[8pt]
    \mbox{vertex DoFs}&:&  \Phi_{ij}&=&1-(n-1)(\lambda_i+\lambda_j)+n(n-1)\lambda_i\lambda_j\\
             & & & &\dsp  -(n-1)(\nabla\lambda_i,\nabla \lambda_j)\Big(\frac{\lambda_i(n\lambda_i-2)}{2\|\nabla \lambda_i\|^2}
             +\frac{\lambda_j(n\lambda_j-2)}{2\|\nabla \lambda_j\|^2}
             \Big)&1\leq i<j\leq n+1\\
\end{array}
\right..
\]

The nodal value interpolant is given by (\(v\in H^2(T)\))
\[
\Pi_Tv=\sum_{1\leq i<j\leq n+1}\frac{\Phi_{ij}}{|S_{ij}|}\int_{S_{ij}}v+\sum_{1\leq j\leq n+1}\frac{\varphi_j}{|F_j|}\int_{F_j}\frac{\partial v}{\partial n}.\]

\subsection{The Bell finite element}
The Bell element is the Argyris element where normal derivatives DoFs on edge \(E_k\), \(k=1,2,3\) are removed:\\

\[
(BELL)\quad \quad\left|\begin{array}{lll}
    \dsp \widehat{K}&=&\widehat{T}\\
    \dsp \widehat{P}&=&\widetilde{P}^5[\widehat{K}]=\big\{p\in P^5[\widehat{x},\widehat{y}]; \partial_np\in P^3[E_k],\ k=1,2,3\big\}, \ \mbox{dim}\,\widetilde{P}^5=18;\ P^4[\widehat{K}]\subset \widetilde{P}^5[\widehat{K}]\subset P^5[\widehat{K}] \\[5pt]
    \dsp \widehat{\Sigma}&=&\left\{
    \begin{array}{l}
        p(\widehat{M}_i),\ \partial_xp(\widehat{M}_i),\ \partial_yp(\widehat{M}_i),\ \partial_{xx}p(\widehat{M}_i),\ \partial_{yy}p(\widehat{M}_i),\ \partial_{xy}p(\widehat{M}_i),\ i=1,2,3 
    \end{array}
    \right\}
\end{array}\right.
\]
\begin{figure}[H]
    \centering
    \includePict[width=5cm]{BellTriangle.png}
    \caption{The Bell element}
\end{figure}
If the solution of a fourth order PDE is smooth, Bell element approximation leads to third order error in \(H^2\) norm, to be compared to fourth order error with the Argyris element. The transport of shape functions is similar to the transport of Argyris shape functions up to a scale factor on \(Q^i\) matrices.\\

The Bell shape functions on the triangle (\((x_1,y_1), (x_2,y_2), (x_3,y_3)\)) at first vertex are given by  (same order as in \(\Sigma\) )
\begin{figure}[H]
    \centering
    \includePict[width=12cm]{BellShapeFunctions.png}
\end{figure}

\begin{minipage}{8cm}
with \(\begin{array}{lll}
   b_1=x_3-x2,&b_2=x_1-x_3,&b_3=x_2-x_1,\\
   c_1=y_3-y2,&c_2=y_1-y_3,&c_3=y_2-y_1,
\end{array}\)
\end{minipage}
\begin{minipage}{8cm}
\begin{figure}[H]
    \centering
    \includePict[width=5cm]{BellTriangle2.png}
\end{figure}
\end{minipage}\\
Other shape functions are obtained by cyclic permutation of the barycentric coordinates. When reference triangle is considered, some simplifications are occurring ( \(b_2=c_3=0;\ b_3=c_1=1;\ b_1=c_2=-1; \ell_2=\ell'_3=0;\ \ell'_2=\ell_3=1;\ \ell_1=\ell'_1=0.5\)) 
\section{The Bogner-Fox-Schmit finite elements}
The Bogner-Fox-Schmit elements (BFS) are defined from the tensor product of 1D Hermite element on Cartesian element (say rectangle or cuboid). 

\subsection{2D Bogner-Fox-Schmit on rectangle}
In the sequel, we note the vertices of the unit square : \(\widehat{M}_1=(0,0),\ \widehat{M}_2=(1,0),\ \widehat{M}_3=(1,1),\ \widehat{M}_4=(0,1),\ \) The low order BFS is defined as:\\

\[
(BFS-Q3)\quad \quad\left|\begin{array}{lll}
    \dsp \widehat{K}&=&\widehat{C}=[0,1]\times[0,1]\\
    \dsp \widehat{P}&=&Q^3[\widehat{x},\widehat{y}]=\text{span}\big\{\widehat{x}^i\widehat{y}^j,\ 0\leq i,j\leq 3\big\}, \ \mbox{dim}\,Q^3=16\\
    \dsp \widehat{\Sigma}&=&\big\{p(\widehat{M}_i),\partial_xp(\widehat{M}_i),\partial_yp(\widehat{M}_i),\partial_{xy}p(\widehat{M}_i),\ i=1,2,3,4\big\}
\end{array}\right.
\]

The shape functions are given by (\(\tau_i\) the 1D cubic Hermite shape functions)
\[
\begin{array}{llll}
    \varphi_1(\widehat{x},\widehat{y})=\tau_1(\widehat{x})\tau_1(\widehat{y}),& \varphi_2(\widehat{x},\widehat{y})=\tau_2(\widehat{x})\tau_1(\widehat{y}),&\varphi_3(\widehat{x},\widehat{y})=\tau_2(\widehat{x})\tau_2(\widehat{y}),& \varphi_4(\widehat{x},\widehat{y})=\tau_1(\widehat{x})\tau_2(\widehat{y})\\[3pt]
    \varphi_1^x(\widehat{x},\widehat{y})=\tau_3(\widehat{x})\tau_1(\widehat{y}),& \varphi_2^x(\widehat{x},\widehat{y})=\tau_4(\widehat{x})\tau_1(\widehat{y}),& \varphi_3^x(\widehat{x},\widehat{y})=\tau_4(\widehat{x})\tau_2(\widehat{y}),& \varphi_4^x(\widehat{x},\widehat{y})=\tau_3(\widehat{x})\tau_2(\widehat{y})\\[3pt]
    \varphi_1^y(\widehat{x},\widehat{y})=\tau_1(\widehat{x})\tau_3(\widehat{y}),& \varphi_2^y(\widehat{x},\widehat{y})=\tau_2(\widehat{x})\tau_3(\widehat{y}),& \varphi_3^y(\widehat{x},\widehat{y})=\tau_2(\widehat{x})\tau_4(\widehat{y}),& \varphi_4^y(\widehat{x},\widehat{y})=\tau_1(\widehat{x})\tau_4(\widehat{y})\\[3pt]
    \varphi_1^{xy}(\widehat{x},\widehat{y})=\tau_3(\widehat{x})\tau_3(\widehat{y}),& \varphi_2^{xy}(\widehat{x},\widehat{y})=\tau_4(\widehat{x})\tau_3(\widehat{y}),& \varphi_3^{xy}(\widehat{x},\widehat{y})=\tau_4(\widehat{x})\tau_4(\widehat{y}),& \varphi_4^{xy}(\widehat{x},\widehat{y})=\tau_3(\widehat{x})\tau_4(\widehat{y})
\end{array}
\]

\begin{figure}[H]
    \centering
    \includePict[width=10cm]{Bogner-Fox-Schmit_Q3_shape_functions.png}
    \caption{The Bogner-Fox-Schmit Q3 shape functions (\(\varphi_2,\varphi_2^x, \varphi_2^y,\varphi_2^{xy}\))}
\end{figure}
From tensor product of Hermite C1-Pk basis, BFS may be extended to any order \(k\) (see \cite{SZ2010}):\\

\[
(BFS-QK)\quad \quad\left|\begin{array}{lll}
    \dsp \widehat{K}&=&\widehat{C}\\
    \dsp \widehat{P}&=&Q^k[\widehat{x},\widehat{y}]=\text{span}\big\{\widehat{x}^i\widehat{y}^j,\ 0\leq i,j\leq k\big\}, \ \mbox{dim}\,Q^k=(k+1)^2\\
    \dsp \widehat{\Sigma}&=&\left\{
    \begin{array}{ll}
        p\big(\frac{i}{k-2},\frac{j}{k-2}\big)&0\leq i,j\leq k-2\\
        \partial_xp(\frac{i}{k-2},j)&0\leq i\leq k-2\mbox{ and }j=0,1\\
        \partial_yp(i,\frac{j}{k-2})&0\leq j\leq k-2\mbox{ and }i=0,1\\
        \partial_{xy}p(i,j)&0\leq i,j\leq 1
    \end{array}
    \right\}
\end{array}\right.
\]

These elements have interior nodal points as soon as \(k\geq 4\). The next figure (picked from \cite{SZ2010})shows the location and the type of DoFs (dot for a nodal value, circle or single arrow for first derivatives and double arrow for second order derivatives).
\begin{figure}[H]
    \centering
    \includePict[width=15cm]{Bogner-Fox-Schmit_element.png}
    \caption{The family of Bogner-Fox-Schmit rectangles}
\end{figure}
in \cite{SZ2010}, for \(k\geq 5\) another extension (named second family of C1-Qk) involving third order derivatives is proposed.
\subsection{3D Bogner-Fox-Schmit on cuboid}
By the same way, BFS may be constructed on the unit cube \(\widehat{Q}=[0,1]\times[0,1]\times[0,1]\), \(k\geq 3\) :\\

\[
(BFS-QK)\quad \quad\left|\begin{array}{lll}
    \dsp \widehat{K}&=&\widehat{C}\\
    \dsp \widehat{P}&=&Q^k[\widehat{x},\widehat{y}]=\text{span}\big\{\widehat{x}^i\widehat{y}^j,\ 0\leq i,j\leq k\big\}, \ \mbox{dim}\,Q^k=(k+1)^3\\[5pt]
    \dsp \widehat{\Sigma}&=&\left\{
    \begin{array}{ll}
        p\big(\frac{i}{k-2},\frac{j}{k-2},\frac{\ell}{k-2}\big)&0\leq i,j,\ell\leq k-2\\
        \partial_xp(i,\frac{j}{k-2},\frac{\ell}{k-2}&0\leq j,\ell\leq k-2\mbox{ and }i=0,1\\
        \partial_yp(\frac{i}{k-2},j,\frac{\ell}{k-2})&0\leq i,j\leq k-2\mbox{ and }j=0,1\\
        \partial_zp(\frac{i}{k-2},\frac{j}{k-2},\ell)&0\leq i,j\leq k-2\mbox{ and }\ell=0,1\\
        \partial_yp(i,\frac{j}{k-2})&0\leq j\leq k-2\mbox{ and }i=0,1\\
        \partial_{xy}p(i,j,\frac{\ell}{k-2})&0\leq i,j\leq 1\mbox{ and } 0\leq\ell\leq k-2\\
        \partial_{xz}p(i,\frac{j}{k-2},\ell)&0\leq i,\ell\leq 1\mbox{ and } 0\leq j \leq k-2\\
        \partial_{yz}p(\frac{i}{k-2},j,\ell)&0\leq j,\ell\leq 1\mbox{ and } 0\leq i\leq k-2\\
        \partial_{xyz}p(i,j,\ell)&0\leq i,j,\ell\leq 1
    \end{array}
    \right\}
\end{array}\right.
\]

For \(k=3\), the 3D BFS has \(4^3=64\) DoFs, all located on the vertices of the cube. At each vertex, there are 8 DoFs (the value, the three first derivatives and four mixed derivative). When \(k\geq 4\) there are some nodal value DoFs in the interior of element.

\section{Macroelements}

Macroelements are elements allowing to work with C1 piecewise polynomial spaces. Hsieh-Clough-Tocher (triangle) and Fraeijs de Veubeke-Sander(quadrilateral) are the most common C1 macroelements. The material exposed here comes from \cite{DDPR79}. 

\subsection{Macrotriangle}

We consider a triangle with vertices (\(a_1,a_2,a_3\)) and an interior point \(b\) (barycenter of \(T\) for instance). This triangle is split in three triangles: \(T_1(a_2,a_3,b)\), \(T_2(a_1,a_3,b)\), \(T_3(a_1,a_2,b)\).
\begin{figure}[H]
\centering
\includePict[width=5.5cm]{macroTriangle.png}
\caption{Macrotriangle}
\end{figure}

We define the cubic Hsieh-Clough-Tocher macrotriangle as:\\

\[
(HCT)\quad \quad\left|\begin{array}{lll}
    \dsp K&=&(T_1,T_2,T_3)\\
    \dsp P&=&P^3[T_1,T_2,T_3]=\big\{p\in C^1(T); p_{|T_i}\in P^3[T_i],\ 1\leq i\leq 3\big\},\ \mbox{dim}\,P=12  \\[2pt]
    \dsp \Sigma&=&\left\{
    \begin{array}{l}
        p(a_i),\ \partial_xp(a_i),\ \partial_yp(a_i),\ \partial_{n}p(b_i),\ i=1,2,3 
    \end{array}
    \right\}
\end{array}\right.
\]

where \(b_i\) is any interior point (the middle point for instance) of \(i^{\text{th}}\) edge of \(T\). 
\begin{figure}[H]
    \centering
    \includePict[width=5.5cm]{HCT.png}
    \caption{Hsieh-Clough-Tocher (HCT) macrotriangle}
\end{figure}
This element may be generalized at any order \(n\), considering\\

\[
(HCT)\quad \quad\left|\begin{array}{lll}
    \dsp K&=&(T_1,T_2,T_3)\\
    \dsp P&=&P^n[T_1,T_2,T_3]=\big\{p\in C^1(T); p_{|T_i}\in P^n[T_i],\ 1\leq i\leq 3\big\}, \\[2pt]
    \dsp \Sigma&=&\left\{
    \begin{array}{l}
        p(a_i),\ \partial_xp(a_i),\ \partial_yp(a_i);\ i=1,2,3\\
        \partial_{n}p(b_{ij});\ 1\leq j\leq n-2,\ p(\widetilde{b}_{ij}),\ 1\leq j\leq n-3,\ i=1,2,3\\
        \partial_{n}p(c_{ij}),\ 1\leq j\leq n-4;\ p(\widetilde{c}_{ij});\ 1\leq j\leq n-4,\ i=1,2,3\\
        p(b),\ \partial_xp(b),\ \partial_yp(b);\\
        \mbox{if }n\geq 6\  p(q_{ij}),\ 1\leq j\leq \frac{(n-4)(n-5)}{2},\ i=1,2,3
    \end{array}
    \right\}
\end{array}\right.
\]

where \(b_{ij},\widetilde{b}_{ij}\) are distinct points located on the \(i^{\text{th}}\) exterior edge, \(c_{ij},\widetilde{c}_{ij}\) are distinct points located on the \(i^{\text{th}}\) interior edge and \(q_{ij}\) are interior points of \(T_i\) with the property that any polynomial of degree \(n-6\) vanishing at these point is the zero polynomial. 

\subsection{Summary}

{\small
\begin{center}
\begin{tabular}{c|c|c|c|c|c|c|c|c}
    \emph{short name} & \emph{full name} & \emph{support} & \emph{space} &\emph{dim} &\emph{H1}& \emph{H2}& \emph{order}&\emph{XLiFE++}\\ 
    \hline 
    HERP3 &Hermite P3& segment& P3 & 4&yes & yes & 2& yes\\
    \hline
    HERP3 &Hermite P3 & triangle& P3 &10& yes & no& ?& yes\\
    MOR&Morley& triangle& P2& 6&no & no& 1& not yet\\
    HCT& Hsieh-Clough-Tocher&triangle&piecewise P3&12&yes&yes&2& not yet\\
    BELL & Bell& triangle&P4\(\subset\)P\(\subset\) P5&18&yes & yes&3& not yet \\
    ARG & Argyris& triangle&P5&21&yes & yes&4& not yet\\ 
    \hline
    FdV& Fraeijs de Veubeke&quadrangle&piecewise P3&16&yes&yes&2& not yet\\
    BFSQ3&Bogner-Fox-Schmit Q3&quadrilateral&Q3&16&yes&yes&2& no\\
    BFSQk&Bogner-Fox-Schmit Qk&quadrilateral&Qk&\((k+1)^2\)&yes&yes&\(k-1\)& no\\
    \hline
    MOR&Morley&tetrahedron& P2& 10&no & no& 1& not yet\\
    HCT& Hsieh-Clough-Tocher&tetrahedron&piecewise P3&?&yes&yes&2?& no\\
    ZEN& Zenisek C1-P9&tetrahedron&P9&220&yes&yes&4?&no\\
    \hline
    BFSQk&Bogner-Fox-Schmit Qk&cuboid&Qk&\((k+1)^3\)&yes&yes&\(k-1\)& no\\
\end{tabular}
\end{center}
}

The Zenisek element is the equivalent on tetrahedron of the Argyris element on triangle!
\newpage
\begin{thebibliography}{1}
	\bibitem{SZ2010}S. Zhang, \textit{On the Full C1-Qk Finite Element Spaces on Rectangles and Cuboids}
    Adv. Appl. Math. Mech., Vol. 2, No. 6, pp. 701-721, 2010, DOI: 10.4208/aamm.09-m0993
    
   \bibitem{MZ2006} Ming, W., Xu, J. \textit{The Morley element for fourth order elliptic equations in any dimensions}. Numer. Math. 103, 155–169 (2006). 
   https://doi.org/10.1007/s00211-005-0662-x
   
   \bibitem{V2020} Valdman J.\textit{MATLAB Implementation of C1 Finite Elements: Bogner-Fox-Schmit Rectangle.} In: Wyrzykowski R., Deelman E., Dongarra J., Karczewski K. (eds) Parallel Processing and Applied Mathematics. PPAM 2019. Lecture Notes in Computer Science, vol 12044. Springer, Cham. https://doi.org/10.1007/978-3-030-43222-5\_22
   
  \bibitem{DDPR79} Douglas Jr., Dupont T., Percell, P., Scott R., \textit{A family of \(C^1\) finite elements with optimal approximation properties for various Galerkin methods for 2nd and 4th order problems}, ESAIM : Mathematical Modelling and Numerical Analysis - Mod\'elisation Math\'ematique et Analyse Num\'erique, vol 13, 227--255, 1979, http://www.numdam.org/item/M2AN\_1979\_\_13\_3\_227\_0/
  
  \bibitem{K2018} Kirby R.C. \textit{A general approach to transforming finite elements}. The SMAI journal of computational mathematics, Tome 4 (2018), pp. 197-224. doi : 10.5802/smai-jcm.33. http://www.numdam.org/item/SMAI-JCM\_2018\_\_4\_\_197\_0/
  
  \bibitem{DS2008} Dominguez V., Sayas FJ.  \textit{Algorithm 884: A Simple Matlab Implementation of the Argyris Element}. ACM Trans. Math. Software 35, July 2008 https://doi.org/10.1145/1377612.1377620
  
	
\end{thebibliography}

\end{document}