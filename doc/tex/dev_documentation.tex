%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%
 
\documentclass[11pt,svgnames,english]{report}

\usepackage[mode=dev]{xlifepp}
\usepackage{tikz-uml}
\usepackage{array}
\newcolumntype{P}[1]{>{\raggedright}p{#1}}

\hypersetup{%
  pdftitle={Developer documentation for XLiFE++},%
  pdfauthor={N. Kielbasiewicz, E. Lunéville},%
  pdfpagemode=UseOutlines,%
  linkbordercolor=0 0 0,%
  linkcolor=black,%
  citecolor=black,%
  colorlinks=true%
}%

%\date{}
\title{Developer documentation}
\author{Nicolas {\scshape Kielbasiewicz}, Eric {\scshape Lunéville}, Manh Ha {\scshape Nguyen}, Yvon {\scshape Lafranche}}

\begin{document}
\maketitle
\tableofcontents

\chapter{General organization}
\inputDoc*{general_organization}

\chapter{Initialization and global definitions}
\inputDoc*{initialization}

\chapter{The \libtitle{utils} library}

The \lib{utils} library collects all the general useful classes and functionalities of the \xlifepp library. It is an independent library. It addresses:

\begin{description}
\item[\class{String}] capabilities (mainly additional functions to the \class{std::string} class),
\item[\class{AngleUnit}] class to deal with angle unit,
\item[\class{Point}] class to deal with point of any dimension,
\item[\class{Vector}] class to deal with numerical vectors (say real or complex vectors),
\item[\class{Matrix}] class to deal with numerical matrices with dense storage (small matrices),
\item[\class{Value}] class to handle any real/complex scalar/vector/matrix value,
\item[\class{VectorEntry}] class to deal with any scalar/vector real/complex vector,
\item[\class{Collection}] class to deal with collection of anything, inheriting from std::vector,
\item[\class{Parameter}] classes to deal with general set of user parameters,
\item[\class{Tabular}] classes to deal with tabulated values
\item[\class{Function}] class encapsulating user functions,
\item[\class{SymbolicFunction}] class handling symbolic functions,
\item[\class{Kernel}] class encapsulating user kernels,
\item[\class{ThreadData}] class handling FE data computation (normal vectors, \ldots),
\item[\class{Transformation}] class to handle geometric transformations,
\item[\class{Messages}] classes to handle error, warning and info messages,
\item[\class{Trace}] class to handle log messages,
\item[\class{Timer}] class to handle timer utilities,
\item[\class{Memory}] class to handle memory utilities,
\item[\class{PrintStream}] class that handles the output stream in multi-thread environment
\item[\class{Environment}] class to handle \xlifepp runtime variables,
\item[\class{Graph}] class to deal with graph,
\item[\class{KdTree}] class to handle kdtree (binary separation of kd points with a fast search of the nearest point),
\item[\class{EigenInterface}] class that handles the interface to Eigen library
\end{description}

\inputDoc*{Environment}
\inputDoc*{Messages}
\inputDoc*{Trace}
\inputDoc*{Timer}
\inputDoc*{Memory}
\inputDoc*{String}
\inputDoc*{AngleUnit}
\inputDoc*{Point}
\inputDoc*{Vector}
\inputDoc*{Matrix}
\inputDoc*{Transformation}
\inputDoc*{collection}
\inputDoc*{Parameters}
\inputDoc*{Tabular}
\inputDoc*{Function}
\inputDoc*{Kernel}
\inputDoc*{ThreadData}
\inputDoc*{SymbolicFunction}
\inputDoc*{Graph}
\inputDoc*{Value}
\inputDoc*{KdTree}
\inputDoc*{Polynomials}

\chapter{The \libtitle{geometry} library}

The \lib{geometry} library collects all the classes and functionalities related to meshes.
The main class is the \class{Mesh} class which handles all stuff to describe a 1D, 2D or 3D mesh. This class provides also basic meshing tools (for simple geometries such as quadrangle, hexahedron, disk, sphere, \dots), and import and export tools.
The other important class is the \class{GeomDomain} class, which means geometrical domain, describing parts of mesh, in particular boundaries or parts of boundary.
\class{GeomDomain} objects are involved in integral linear or bilinear forms as integration domain (see the \lib{form} lib). The atomic object of any mesh or geometric domain is the geometric element (\class{GeomElement} class) which can handle either a full description of a geometric element (\class{MeshElement} class) or the description of a side of geometric element (say parent elements and side numbers). Note that a side element may also carry a \class{MeshElement} structure if required; by default it does not.\\

So the \lib{geometry} library is organized as follows:

\begin{description}
\item[\class{Mesh}] class to deal with 1D, 2D or 3D mesh,
\item[\class{Geometry}] class to handles global geometric data such as bounding box, parametrization,
\item[\class{GeomDomain}] class to deal with geometrical domains (whole computational domain, subdomains, \ldots),
\item[\class{GeomElement}] class to deal with elements of a mesh or geometrical domains,
\item[\class{MeshElement}] class handling full description of an element of mesh (nodes, numbering, \ldots),
\item[\class{GeomMapData}] class storing geometric data (Jacobian matrix of the map to reference
element, differential element, normal vector, \ldots).
\end{description}

The scheme on figure \ref{diag_geometry} shows the organization of the \lib{geometry} library
and its interactions with the \lib{space} and \lib{finiteElements} libraries.

\begin{figure}
\includeTikZPict[width=18cm]{geometry.png}
\caption{\lib{geometry} library organization}\label{diag_geometry}
\end{figure}

\inputDoc*{Geometry}
\inputDoc*{Mesh}
\inputDoc*{opencascade}
\inputDoc*{Domain}
\inputDoc*{GeomElement}
\inputDoc*{transformations}
\inputDoc*{GeomMapData}
\inputDoc*{Parametrization}

\chapter{The \libtitle{finiteElements} library}

The \lib{finiteElements} library concerns all the stuff related to the elementary objects of a finite elements discretization. When these objects are provided by a mesh tool, they are \class{GeomElement} (for geometric element) and when they also carry interpolation information, they are \class{Element}. In few words, a \class{Mesh} is a collection of \class{GeomElement} objects and a finite element \class{Space} is a collection of \class{Element} objects. In the framework of the finite elements, it is common (and useful) to link any element to a reference element, named in this library: \class{GeomRefElement} and \class{RefElement}. \class{GeomRefElement} is a class whom inherit fundamental geometric elements \class{GeomSegment}, \class{GeomTriangle}, \ldots and \class{RefElement} is a class whom inherit all the finite element types \class{LagrangeSegment}, \class{LagrangeTriangle}, \class{LagrangeTetrahedron}, \ldots The \class{Interpolation} class carries some general information on the finite element such that the finite element type and its conforming space. Besides, this library provides integration method stuff (\class{IntegrationMethod}, \class{Quadrature}, \class{QuadratureRule}).

\begin{figure}[H]
\centering
\includeTikZPict[width=9cm]{finiteElements.png}
\caption{The \lib{finiteElements} library main class diagram}
\end{figure}

\inputDoc*{Interpolation}
\inputDoc*{RefDof}
\inputDoc*{GeomRefElement}
\inputDoc*{RefElement}
\inputDoc*{elements}
\inputDoc*{IntegrationMethod}

\chapter{The \libtitle{space} library}

The \lib{space} library collects all the classes and functionalities related to functions spaces. In the framework of the code, a space is a finite dimension vector space of functions defined on a geometrical domain (see \class{GeomDomain}), in other words, the space spanned by a finite number of independent functions (global basis functions). This collection of functions may be given either explicitly (say spectral basis) or using local definition of functions on geometric elements (say finite element basis).  Besides, a space may be also a subspace of a space (in the meaning of a function subspace or the trace of a space on a geometric subdomain) or a tensor product of spaces. The space we used is no more than the finite dimension space involved in discrete variational formulations except the essential boundary conditions.
Then the \class{Space} class is an essential concept of the code, in particular for users who have to deal with. A finite element space is essentially a list of \class{Elements} which supports finite element. A spectral space is essentially a list of \class{GlobalFunction} defined by the user either in an analytical form or an interpolated form. Finally, there are two related classes: the \class{Unknown} class to deal with an abstract element of a space  and \class{Dof} which represents a generalized index of an unknown.  

\begin{figure}[H]
\includeTikZPict[width=17cm]{space.png}
\caption{\lib{space} library organization} 
\label{diag_space}
\end{figure}

\inputDoc*{Space}
\inputDoc*{DoF}
\inputDoc*{Unknown}
\inputDoc*{Element}
\inputDoc*{SpectralBasis}

\chapter{The \libtitle{operator} library}

The \lib{operator} library collects the classes and functionalities related to the description of differential operators acting on unknown or test function in linear, bilinear form and essential condition expressions. As we wish to propose to end users, a C++ description of such operators as close as possible to the mathematical description, few operators are overloaded and a lot of possibilities are offered, for instance:

\begin{center}
\begin{tabular}{|l|l|l|}
\hline
mathematical expression & C++ translation & comment\\
\hline
\(\nabla(u)\) & \cmd{grad}(u) & u unknown \\
\(\nabla(u).\nabla(v)\) & \cmd{grad}(u)|\cmd{grad}(v) & u unknown, v test function \\
\((A*\nabla(u)).\nabla(v)\) & (A*\cmd{grad}(u))|\cmd{grad}(v) & u unknown, v test function, A matrix
\\
\((F(x)*\nabla(u)).\nabla(\overline{v})\) & (F*\cmd{grad}(u))|\cmd{grad}(\cmd{conj}(v)) & u unknown, v test function,
F function \\
\hline
\end{tabular}
\end{center}

As a consequence, this library is quite intricate. It is based on 

\begin{itemize}
\item The class of differential operator: \class{DifferentialOperator} collecting all the
differential operators that can be applied to an unknown or a test function;
\item The class of operand: \class{Operand},  describing the operations performed at left
and right of a differential operator acting on an unknown or a test function;
\item The class of operator acting on unknown: \class{OperatorOnUnknown} which collects the
differential operator acting on unknown and the left and right operands if exists;
\item The \class{OperatorOnUnknowns} which relies two operators on unknown as they appear in a bilinear form;
\item The class of linear combination of operators acting on unknown: \class{LcOperatorOnUnknown} which manages a list of pair of \class{OperatorOnUnknown} and complex coefficient. This class is also able to attach geometric domain to each pair. 
\end{itemize}

A linear form will be defined from one \class{OperatorOnUnknown} object and a bilinear form
from \class{OperatorOnUnknowns} objects; see the \lib{form} library\\

Besides, this library provides additional classes to deal with expression involving \class{Kernel} functions:

\begin{itemize}
\item The \class{OperatorOnKernel} class which handles differential operator applied to a kernel;
\item The \class{KernelOperatorOnUnknowns} class which relies two operators on unknown and a kernel;
\item The \class{TensorKernel} class inherited from \class{Kernel} and implementing particular kernel.
\end{itemize}

\inputDoc*{DifferentialOperator}
\inputDoc*{Operand}
\inputDoc*{OperatorOnUnknown}
\inputDoc*{OperatorOnUnknowns}
\inputDoc*{LcOperatorOnUnknown}
\inputDoc*{LcOperatorOnUnknowns}
\inputDoc*{OperatorOnKernel}
\inputDoc*{KernelOperatorOnUnknowns}
\inputDoc*{TensorKernel}

\chapter{The \libtitle{form} library}

This library provides the main \class{LinearForm} and \class{BilinearForm} classes which are the fundamental user classes to describe variational formulation. Linear and bilinear forms involved in variational formulation may have multiple expressions. For the moment, the library deals with:

\begin{itemize}
\item Single integral form involved in standard variational formulation, say 

\[
\int_{D} \mathcal{L}(u) \ \ \text{or }\int_{D} \mathcal{L}_1(u)\, O\,  \mathcal{L}_2(v)
\]

where \(D\) is a geometrical domain, \(u\), \(v\) are unknowns or test functions, \(\mathcal{L}\) are linear operators (see \class{OperatorOnUnknown} class) and \(O\) is an algebraic operation.

Canonical examples are:

\[
\int_\Omega f\,u,\ \ \text{or }\int_\Omega A\nabla(u).\nabla(v)
\]

and for Galerkin Discontinuous approach (\(S_{\Omega}\) the internal sides of elements of \(\Omega\)):

\[
\int_{S_\Omega}[u]\{v\}.
\]

\item Double integral form involved in integral representation or integral equation variational
formulation, say 

\[
\int_{D_1} \int_{D_2} K(x_1,x_2)\,O\, {\cal L}(u)(x_1) \ \ \text{or }\int_{D_1} \int_{D_2}
K(x_1,x_2)\,O\, {\cal L}(u)(x_1)\,O\, {\cal L}(v)(x_2)
\]

where \(D_1\) and \(D_2\) are geometrical domains, \(u\), \(v\) are unknowns or test functions, \(\mathcal{L}\) are linear operators and \(K(x_1,x_2)\) is a kernel function.
\end{itemize}

Besides, it should be friendly to deal with multi-unknown formulation. For instance the formulation of Stokes problem involves the pressure and the velocity as unknowns. Thus, linear (resp. bilinear) forms taken into account are lists of linear combination of basic linear (resp. bilinear) forms. A basic linear (resp. bilinear) form is a simple or a double integral of an operator acting on unknown (resp. a "product" of operators acting on unknown), see the \class{OperatorOnUnknown} class. Only basic linear (resp. bilinear) forms having the same unknown (resp. a couple of unknowns) may be combined in a linear combination. On the other hand, lists of linear combination of basic linear (resp. bilinear) forms may be algebraically combined, combination being done on each unknown (resp. a couple of unknowns) separately. A multi-unknown linear form may be seen as a vector of linear form indexed by unknown and a multi-unknown bilinear form may be seen as a matrix of linear form indexed by a couple of unknowns. In this way, a single unknown linear form is a vector of size one and a "single" unknown bilinear form is a matrix one by one.\\

To intend to this organization, the following classes are provided:

\begin{description}
\item[\class{BasicLinearForm} (resp. \class{BasicBilinearForm})] class is the abstract base
class of basic linear (resp. bilinear) form,
\item[\class{IntgLinearForm} (resp. \class{IntgBilinearForm})] class inherits from {\class
BasicLinearForm} (resp. \class{BasicBilinearForm}) and deals with forms defined with an integral
on a geometrical domain,
\item[\class{DoubleIntgLinearForm} (resp. \class{DoubleIntgBilinearForm})] class inherits
from \class{BasicLinearForm} (resp. \class{BasicBilinearForm}) and deals with forms defined
with an integral on a product of geometrical domains (double integral). Such integrals are
involved in integral equation formulation,
\item[\class{UserBilinearForm}] class inherits
from \class{BasicBilinearForm} and manages bilinear forms defined from a user function building elementary matrices. Because of the low-level programming and the management of internal classes of \xlifepp, this approach is reserved for advanced users.
\item[\class{LcLinearForm} (resp. \class{LcBilinearForm})] class stores a linear combination
of \class{ BasicLinearForm} (resp. \class{ BasicBilinearForm}) objects having same unknown
(resp. a couple of unknowns),
\item[\class{LinearForm} (resp. \class{BilinearForm})] class stores a list of \class{LcLinearForm}
(resp. \class{LcBilinearForm}) objects using map indexed by unknown (resp. a couple of unknowns).
\end{description}

The organization of linear forms looks like:

\begin{center} 
\includeTikZPict[width=14cm]{lform.png}
\end{center}

and the organization of bilinear forms is similar:

\begin{center} 
\includeTikZPict[width=14cm]{blform.png}
\end{center}

\inputDoc*{LinearForm}
\inputDoc*{BilinearForm}

\chapter{The \libtitle{largeMatrix} library}

The \lib{largeMatrix} library deals with large matrices stored in different ways: dense storages (all the matrix values are stored), skyline storages (values on a row or a column are stored from the first nonzero values) and compressed sparse storage (only nonzero values are stored). For each of these storages, different "access" methods, driving how the matrix values are stored, are proposed: by rows, by columns, by rows and columns (say dual access) and symmetric access which is like dual access but with same row and column access. The symmetric access is well suited for matrices having symmetry property (symmetric, skew-symmetric, self-adjoint or skew-adjoint), or matrix with symmetric storage property, currently met in finite element approximation. Note that the row dense storage corresponds to the storage used by the \class{Matrix} class defined in the \lib{utils}. As other access methods may be useful in finite element methods, it is the reason why the \lib{largeMatrix} library provides dense storages.  

Large matrices under consideration may be real or complex value matrices, but also matrices of real/complex matrices. This last case allows dealing with vector variational problems where the unknown and test function are vector functions, using the same storage structure as in the scalar case.\\

More precisely, the \lib{largeMatrix} library is based on the template class \class{LargeMatrix<T>} which mainly handles a vector to store the matrix values and a pointer to a storage describing how the matrix values are stored. Different storage structures are organized in a hierarchy of classes, terminal classes carrying specific data and methods of storages, in particular the product of a matrix and a vector.

\begin{figure}[H]
\centering
\includeTikZPict[width=13cm]{largeMatrix.png}
\caption{The \lib{largeMatrix} library main class diagram}
\end{figure}

\inputDoc{LargeMatrix}
\inputDoc*{MatrixStorage}
\inputDoc*{DenseStorage}
\inputDoc*{CsStorage}
\inputDoc*{SkylineStorage}

\chapter{The \libtitle{hierarchicalMatrix} library}\label{chap_hmatrix}

The \lib{hierarchicalMatrix} library deals with large matrices that are represented by a hierarchical tree, each tree node being either a real submatrix (leaf) or a virtual submatrix addressing up to four nodes:

\begin{figure}[H]
\centering
\includePict[width=16cm]{hmatrix1.png}
\caption{Hierarchical matrix}
\end{figure}

The matrix may be not squared and therefore the submatrices too.\\
 
Such representation comes from mesh partition (clustering) where far interactions in BEM  may be approximated by low rank matrices, so reducing the storage of matrix and making the matrix vector product faster. The \lib{hierarchicalMatrix} library proposes 

\begin{itemize}
\item some template classes to deal with clusters of Points, FeDofs, Elements, \ldots: \class{ClusterTree<I>} and \class{ClusterNode<I>} 
\item some template classes to deal with approximated matrices: \class{ApproximateMatrix<T>} and \class{LowRankMatrix<T>} 
\item some template classes addressing hierarchical matrices: \class{HMatrix<T, I>}, \class{HMatrixNode<T, I>} and \class{HMatrixEntry<I>}
\end{itemize}

\inputDoc*{clustering}
\inputDoc*{ApproximateMatrix}
\inputDoc*{HMatrix}

\chapter{The \libtitle{term} library}

This library addresses the \class{Term} class and its inherited classes that handle the numerical representations of linear and bilinear forms 
involved in problems to be solved, say vectors and matrices (\class{LargeMatrix} class).

The \class{Term} class is an abstract class collecting general data (name, computing information) and has four children:
\begin{description}
\item[\class{TermVector}] class to represent linear forms involving multiple unknowns,
\item[\class{SuTermVector}] class to represent linear forms involving only one unknown,
\item[\class{TermMatrix}] to represent bilinear forms involving multiple pairs of unknowns,
\item[\class{SuTermMatrix}] to represent bilinear forms involving only one pair of unknowns,
\end{description}

\class{TermVector} class is linked to a \class{LinearForm} object (defined on multiple unknowns) and handles a map of \class{SuTermVector} object indexed by unknown. In a same way, \class{TermMatrix} class is linked to a \class{BiLinearForm} object (defined on multiple unknown pairs) and handles a map of \class{SuTermMatrix} object indexed by unknown pair. \class{SuTermVector} and \class{SuTermMatrix} are related to \class{SuLinearForm} and \class{SuBiLinearForm} and carries the numerical representations as vector or matrix. This is a block representation.\\

For instance, consider the following two unknowns \((u,p)\in V\times H\) problem:

\[
\left\{
\begin{array}{lll}
a(u,v)+b(p,v)&=&f(v)\\
c(u,q)+d(p,q)&=&g(q)\\
\end{array}
\right.
\]

where \(a\), \(b\), \(c\), \(d\) are bilinear forms defines respectively on \(V\times V\), \(H\times V\), \(V\times H\), \(H\times H\) and \(f\), \(g\) are linear forms defined on \(V\) and \(H\). The block representation is

\[
\left[\begin{array}{cc}
\mathbb{A}&\mathbb{B}\\
\mathbb{C}&\mathbb{D}\\
\end{array}
\right]
\left(\begin{array}{c}
U\\
P\\
\end{array}
\right)
=
\left(\begin{array}{c}
F\\
G\\
\end{array}
\right)
\]

matrices being stored with their own storages. In order to shadow the type of coefficients (real, complex, scalar, vector, matrix) the vector and matrix representations are managed through the \class{VectorEntry} class (see \lib{utils} library) and \class{MatrixEntry} class (see \lib{largeMatrix} library).\\

In certain circumstances the block representation is not well adapted (matrix factorization, essential condition to apply). So the representation is moved to a scalar global representation, block representation may be or not kept.\\

This library collects the main algorithms of vector and matrix computation, in particular 

\begin{itemize}
\item computation from linear and bilinear form (see the \lib{computation} subdirectory)
\item computation from explicit vector, matrix or function (nodal computation)
\item linear combination of \class{TermVector} or \class{TermMatrix} (see \class{LcTerm} class)
\item the elimination process due to essential conditions (in relation with \lib{essentialConditions} library)
\item the interface to solvers (direct solvers, iterative solvers or eigen solvers defined in \lib{largeMatrix}, \lib{solvers}, \lib{eigenSolvers} libraries)
\item some import and export facilities (see the \lib{ioTerms} subdirectory)
\end{itemize} 

\emph{\class{TermMatrix} and \class{TermVector} are end user classes, therefore the functions that interact with users has to be simply designed!}

\inputDoc*{Term}
\inputDoc*{TermVector}
\inputDoc*{SuTermVector}
\inputDoc*{TermVectors}
\inputDoc*{KernelOperatorOnTermvector}
\inputDoc*{TermMatrix}
\inputDoc*{SuTermMatrix}
\inputDoc*{LcTerm}
\inputDoc*{SymbolicTermMatrix}
\inputDoc*{computationAlgorithms}
\inputDoc*{Projector}

\chapter{The \libtitle{essentialConditions} library}

\inputDoc*{EssentialConditions}

\chapter{The \libtitle{solvers} library}

The \lib{solvers} library comes with some iterative solver methods which target a linear system. 
These solvers can be divided into two kinds of group. One is for symmetric linear system and another is for a non-symmetric system.
Each solver uses \lib{largeMatrix} and \lib{Vector} as inputs and returns the result in a \lib{Vector}. 
The type of problem, real or complex, needs specified also as another input of a solver.
In some cases, a solver can take advantage of a \class{Preconditioner} as an extra input to decrease the conditioner number of the system.
A \class{Preconditioner}, which is also a \lib{LargeMatrix} involved with matrix factorization, is only available in the type of \class{SkylineStorage} and \class{CsStorage}.

All solvers in the library are based on the class \class{IterativeSolver}, which provides some basic properties and methods.
Specific iterative algorithms are implemented in the inherited class for two cases: with and without a preconditioner.

\begin{figure}[H]
\begin{center}
\includeTikZPict[width=11cm]{solvers.png}
\end{center}
\caption{The \lib{solvers} library main class diagram}
\end{figure}

\inputDoc*{IterativeSolver}
\inputDoc*{Preconditioner}
\inputDoc*{Solvers}

\chapter{The \libtitle{umfpackSupport} library}

Besides some built-in direct solvers for sparse linear system, \xlifepp provides an interface to \umfpack, a set of routines for solving non-symmetric sparse linear systems, \(Ax=b\), using the Unsymmetric MultiFrontal method. More information can be found at  \url{http://www.cise.ufl.edu/research/sparse/umfpack/}.\\

The aim of \lib{umfpackSupport} is to make ease of the \umfpack for \xlifepp's users, allowing them to solve the linear system \(Ax=b\) with a simple call.  Instead of invoking "multi-argument" \umfpack  solver,  user can make call of them as any built-in iterative solver in the context of \xlifepp.

\inputDoc*{UmfpackSupport}

\chapter{The \libtitle{eigenSolvers} library}

The \lib{eigenSolvers} library comes with built-in solvers of eigen problem which deal with sparse matrices. The user interface of eigen solvers is similar to the \lib{solvers}: all solvers are invoked with operator().

To calculate eigenvalues and eigenvector, at the moment, nearly all finite-element libraries rely on a well-known large scale eigen solver \arpack. Although this library is efficient and robust in computing varieties of eigen problem, it relies largely on other libraries: \blas and \lapack. The fact that in order to use \arpack, users must already have these two installed, as well as all these libs are in Fortran,  can make programming an efficient-C++ finite-element application a harsh work. \\
In order to ease users out of the complexities and dependencies, \xlifepp supplies an intern eigensolver which is capable of solving large scale eigen problems with several algorithms.
Two goals motivated the development of this intern eigensolver: independence and simplicity. The intention of \textit{independence}, as mentioned above, is to free \xlifepp from other Fortran libraries. The concept of \textit{simplicity} drives the development of this part to allow users to call this eigen solver like other solvers (e.x:  GmResSolver, \ldots) in \xlifepp. This is encouraged by promoting code modularization and multiple level of access to solvers and data.
In the following, we outline the intern eigen solver structure. In particular, we present

\begin{description}
\item[\lib{eigenCore}] the eigen solver of \emph{dense} matrix
\item[\lib{eigenSparse}] the eigen solver of \emph{sparse} matrix based on \lib{eigenCore}
\end{description}

\inputDoc*{EigenCore}
\inputDoc*{EigenSparse}

\chapter{The \libtitle{arpackppSupport} library}

The \lib{arpackppSupport} library contains the interface of \xlifepp with \arpack library through \arpackpp. Because of commonality and stability, \arpack library is chosen as the external eigen solver.\\

\arpack is a well known collection of FORTRAN subroutines designed to compute a few eigenvalues and eigenvectors of large scale sparse matrices and pencils. It implements a variant of the Arnoldi process for finding eigenvalues called Implicit restarted Arnoldi method (IRAM) and is capable of solving a great variety of problems from single precision positive definite symmetric problems to double precision complex non-Hermitian generalized problems. 
\arpackpp is the C++ an interface of \arpack.\\

\xlifepp offers an interface to \arpackpp through a well-organized collection of classes which hide all the complexity of \arpackpp. User can easily call an eigen solver like a call to an iterative solver. Moreover, this interface offers advanced users a flexible way to make use of all features of \arpackpp with a minimum effort by modifying some classes.
This package is organized into 2 layers:

\begin{itemize}
\item \class{Users Interface} wraps all \arpackpp interface and provides simple call prototype to user
\item \class{Arpack++ Interface} is collection of classes interacting directly with \arpackpp
\end{itemize}

In order to use \arpackpp, \arpack must be already installed.
\arpackpp package can be found at \url{http://www.ime.unicamp.br/~chico/arpack++/}. However, because of the deprecation of this version, a patch at \url{http://reuter.mit.edu/index.php/software/arpackpatch/} needs to be applied to make sure a correct compilation.\\
The following is organized in "bottom-up", the \arpackpp interface is described then Users Interface.

\inputDoc*{EigenSolversArpackpp}

\chapter{The \libtitle{mathResources} library}

The \lib{mathResources} library collects all stuff related to special functions, standard Green's functions, exact solutions and useful numerical tools such as basic quadratures, fast Fourier transform, spline approximations, root finding, ODE solvers.

\inputDoc*{SpecialFunctions}
\inputDoc*{Spline} 
%\inputDoc*{NumericalTools}

\chapter{Multi-threading with OpenMP}

OpenMP is a shared-memory application programming interface (API) whose features are based on prior efforts to facilitate shared-memory parallel programming. OpenMP is intended to be suitable for implementation on a broad range of SMP architectures, even for uniprocessor computers also. 
OpenMP can be added to a sequential program in Fortran, C, or C++ to describe how the work is to be shared among threads that will execute on different processors or cores and to order accesses to shared data as needed. The appropriate insertion of OpenMP features into a sequential program will allow applications to benefit from shared-memory parallel architectures with minimal modification to the code.
Because of these advantages, OpenMP has been added to \xlifepp in order to exploit its considerable parallelism in heavy computation parts.
In a typical Finite Element package, assembling the stiffness matrices costs most of the total runtime then solving linear equation
systems.

\inputDoc*{openMP}

\appendix

\chapter{External libraries}

\inputDoc*{install_ext_libs}

\chapter{Documentation policy}

\inputDoc*{documentation_policy}

\chapter{Tests policy}

In order to maintain backward compatibility of the code during its development, it is mandatory to have various test functions. Some of them are local test functions, says low level tests, and concerns "exhaustive" tests of functionalities of a class or a collection of classes or utilities. Others are global test functions, say high level test functions, involving a lot of classes of the library, for instance, solving a physical problem from the mesh up to the results. 

\inputDoc*{tests_policy}

\chapter{Compiling and linking policy}

\inputDoc*{compiling_policy}

\end{document}
