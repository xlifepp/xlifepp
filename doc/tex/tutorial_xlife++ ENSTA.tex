%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%
 
\documentclass[11pt,svgnames,english]{report}

\usepackage[mode=user, defaultautorefname=user]{xlifepp}

\hypersetup{%
  pdftitle={XLiFE++ tutorial},%
  pdfauthor={N. Kielbasiewicz, E. Lun\'eville},%
  pdfpagemode=UseOutlines,%
  linkbordercolor=0 0 0,%
  linkcolor=black,%
  citecolor=black,%
  colorlinks=true%
}%

%\date{} % date %
\title{\xlifepp tutorial} % titre %
\author{Nicolas {\scshape Kielbasiewicz}, Eric {\scshape Lunéville}} % auteur %

\begin{document}

\pagenumbering{roman}

\maketitle

\tableofcontents

\chapter*{Preface}

\xlifepp is the heir of 2 main finite elements library developed in \poems laboratory, namely \melina (and its C++ avatar \melinapp) and \montjoie, respectively developed since 1989 and 2003. It is a C++ high level library devoted to extended finite elements methods. Writing programs using \xlifepp needs only basic knowledge of C++ language, so that it can be used to teach finite elements methods, but it is quite perfect for research.

\medskip

\xlifepp is self-consistent. It provides advanced mesh tools, with refinement methods, has every kind of elements (including pyramids) needed by finite elements methods, boundary elements methods or discontinuous Galerkin methods, direct/iterative solvers and eigen solvers. Next to this, it provides also a wide range of interfaces to well-known libraries or software programs, such that \umfpack, \arpackpp, and an advanced interface to the mesh generator \gmsh, so that you can do everything needed in a single program.

\medskip


This documentation is dedicated to students at Master level, to engineers and researchers at any level, in so far as partial differential equations are concerned.

\chapter{Introduction}
\pagenumbering{arabic}

Partial differential equations (PDE hereafter) are the core of modeling. A wide range of problems in Physics, Engineering, Mathematics, Banking, \ldots are modeled by PDEs.

\medskip

\xlifepp is a C++ library designed to solve these equations numerically. It is a free extended library based on finite elements methods. It is an autonomous library, providing everything you need for solving such problems, including interfaces to specific external libraries or software's, such as \gmsh, \arpackpp, \umfpack, \ldots

\medskip

What does \xlifepp do?

\begin{itemize}
	\setlength\itemsep{0em}
\item Problem description (real or complex) by their variational formulations, with full access to the internal vectors or matrices;
\item Multi-variables, multi-equations, 1D, 2D and 3D, linear or non-linear coupled systems;
\item Easy geometric input by composite description, to build meshes thanks to \gmsh;
\item Easy automatic mesh generation on elementary geometries, based on refinement methods;
\item Very high level user-friendly typed input language with full algebra of analytic and finite elements functions. Your main program will be very similar to the mathematical representation;
\item A wide range of finite elements: segments, triangles, quadrangles, hexahedra, tetrahedra, prisms and pyramids
\item A wide set of internal linear direct and iterative solvers (LU, Cholesky, BiCG, BiCGStab, CG, CGS, GMRES, QMR, SOR, SSOR, \ldots) and internal eigenvalues and eigenvectors solvers, plus additional interfaces to external solvers (\arpackpp, \umfpack,\ldots);
\item A full documentation suite: source documentation (online or inside sources), user documentation (PDF), developer documentation (PDF);
\item An experimental parallel version using OpenMP.
\end{itemize}

\section{Installation}

\subsection{Requirements}

Basically, \xlifepp installation depends on \cmake. To use the full abilities of \xlifepp, you may also install \gmsh, \arpackpp, \umfpack. To plot solutions, installing \paraview will be the better choice (unless you are an advanced user of \gmsh).

So before proceeding to the installation procedure, it is mandatory to install (prefer 64 bits distributions if you have to choose):
\begin{itemize}
\setlength\itemsep{-0.2em}
\item A code editor of your choice.

\windows users can download for instance Codeblocks 20.03 at \url{http://www.codeblocks.org/downloads/26} (it is adviced to choose the distribution codeblocks-20.03mingw-setup.exe).

\macos users can download for instance the Apple EDI Xcode from the Mac App Store and install command-line tools, or Visual Studio Code (\url{https://code.visualstudio.com}).
\item \cmake at \url{https://cmake.org/download/}
\item a C++ compiler. 

On \windows, if you install CodeBlocks including MINGW, a C++ compiler is available. Otherwise, \windows users have to install a C++ compiler, for instance the MingW-W64 C++ compiler available at \url{https://sourceforge.net/projects/mingw-w64/files/mingw-w64/}, e.g. MinGW-W64 GCC-8.1.0 x86\_64-posix-seh.

For \macos users, even if you want to choose the default Apple C++ compiler (clang++), you will need to install g++ (not release 10), for instance from the Homebrew package manager (\url{https://brew.sh/index_fr}), in order to get the Fortran library necessary to deal with external dependencies written in Fortran (\arpack, \blas, \lapack, \amos, \ldots) and the gfortran compiler.
\item \gmsh at \url{http://gmsh.info/\#Download}
\item \paraview at \url{https://www.paraview.org/download/}
\item \blas, \lapack, \arpack and \suitesparse for \linux and \macos users, through your favorite package manager
\end{itemize}

\subsection{Download and installation}

\xlifepp is downloadable at the following url \url{https://uma.ensta-paris.fr/soft/XLiFE++/} in different packages (source or binaries). The most recent binary distribution (release 2.3) will be enough for you. When \xlifepp will be installed, you will find a single directory containing everything. What do you have to know about the content of the \xlifepp directory?

\begin{description}
\setlength\itemsep{-0.2em}
\item[bin] the binary directory contains the xlifepp\_project\_setup gui for every OS. Its use will be explained later.
\item[doc] the doc directory contains the present user guide, the developer guide (also in PDF) and the source documentation (html)
\item[src] the source directory contains all C++ sources of the \xlifepp library
\item[tests] the test directory contains all unitary and system tests to check your installation
\item[lib] After the compilation, the lib directory will contain the static libraries of \xlifepp.
\item[usr] This directory contains the user files to write and compile a C++ program using \xlifepp
\item[CMakeLists.txt] the \cmake compilation script.
\end{description}
To compile \xlifepp, you just have to run \cmake on the CMakeLists.txt file. Depending on \cmake results in the fact that you can install and use easily \xlifepp under macOS,  Windows, and Linux.

\subsubsection*{Installation of \unix binaries (Linux64, macOS64, macOS64-clang++)}

First of all, you have to unzip the archive (\linux) or open the disk image (\macos) and copy the xlifepp directory wherever you want. Now, you will have to execute \cmake to configure the installation:

\begin{enumerate}
\setlength\itemsep{-0.1em}
\item Create a {\tt build} directory inside the install directory;
\item Go to the {\tt build} directory to run the \cmake command. Normally, you will only have to set the C++ compiler, but if necessary, you will have to give additional information about paths to external dependencies. For \macos users, you will also have to set the path to the gfortran library. The whole syntax is as follows:
\begin{lstlisting}{language=}
cmake .. [-DCMAKE_CXX_COMPILER=<compiler>]
         [-DXLIFEPP_BLAS_LIB_DIR=<blas_dir>]
         [-DXLIFEPP_LAPACK_LIB_DIR=<lapack_dir>]
         [-DXLIFEPP_ARPACK_LIB_DIR=<arpack_dir>]
         [-DXLIFEPP_SUITESPARSE_HOME_DIR=<suitesparse_dir>]
         [-DXLIFEPP_GMSH_EXECUTABLE=<gmsh_exe>]
         [-DXLIFEPP_PARAVIEW_EXECUTABLE=<paraview_dir>]
         [-DXLIFEPP_FORTRAN_LIB_DIR=<fortran_dir>]
\end{lstlisting}
\item You will have to add the \xlifepp {\tt bin} directory to your PATH;
\item To configure a working directory where to write a program using \xlifepp, you will have to run the script {\tt xlifepp.sh}. You will for instance choose a code editor (CodeBlocks - Unix Makefiles for instance) or a classical command line behavior (Unix Makefiles)
\end{enumerate}

\subsubsection*{Installation of \windows binaries (Win64)}

Before installing \xlifepp, a GCC compiler and CMAKE must be installed. If you are not using the C++ compiler included in CodeBlocks, you have to declare the {\tt bin} folder of the gcc compiler in the \windows path\footnote{A simple tool to edit the window path is {\scshape Windows Path Editor} (\url{https://rix0rrr.github.io/WindowsPathEditor/})}.\\

When downloading binaries under \windows, all the libraries that \xlifepp requires are provided in a binary form. So you just have to run the installer
\begin{center}
	\verb|xlifepp-binaries-v2.3-2022-04-22-windows-g++.exe-8.1.0-64bits.exe|
\end{center} with {\bfseries administrator elevation} (right button click). By default, the installer proposes to install the \xlifepp library in the \verb|C:\Program Files\xlifepp| directory. If you decide to install it in an other directory, do not use a directory with a name containing non standard character (accent, special character, ...).  If a previous distribution of \xlifepp is installed in the directory you choose, the installer can remove it itself. Furthermore, it is highly recommended to install every component. It may appear a system security window telling that \xlifepp is not authenticated; force the installation !\\

Next, in the bin subdirectory of the \xlifepp install directory, you will find {\ttfamily xlifepp\_configure.exe}. It appears also in the \windows main menu (XLiFE++ folder). To run it, \textbf{administrator elevation} is required.
\begin{figure}[H]
	\begin{center}
		\includePict[width=10cm]{bin_win_conf_0.png}
	\end{center}
\end{figure}

\begin{enumerate}
	\setlength\itemsep{-1em}
	\item First, you have to set the folder containing \xlifepp, it is set to \verb|C:\Program Files\xlifepp| by default:
	\begin{figure}[H]
		\begin{center}
			\includePict[width=10cm]{bin_win_conf_1.png}
		\end{center}
	\end{figure}
	\item As mentioned in the banner, a C++ compiler, \cmake and \gmsh have to be installed on your computer. An EDI such as {\scshape CodeBlocks}, and \paraview are not mandatory but highly recommended. Click on the {\ttfamily Configure} button.
	\begin{figure}[H]
		\begin{center}
			\includePict[width=10cm]{bin_win_conf_2.png}
		\end{center}
	\end{figure}
    \item You can modify the paths to tools or add some missing. Don't forget to click again {\ttfamily Configure} button.
\begin{figure}[H]
	\begin{center}
		\includePict[width=10cm]{bin_win_conf_3.png}
	\end{center}
\end{figure}
	\item When everything is OK, message "Configuration complete" is displayed. To develop your own \xlifepp application, you can click on the {\ttfamily New project} button or run {\ttfamily xlifepp\_new\_project.exe} located in \emph{bin} directory of XLife++ and available from the Windows menu too:
	\begin{figure}[H]
		\begin{center}
			\includePict[width=10cm]{xlifepp_new_project.png}
		\end{center}
	\end{figure}
\item You have to choose the folder where to create your project, the project type (CodeBlocks with MinGW Makefiles by default)\footnote{other choices have not been tested, so it is advised to use CodeBlocks with MinGW Makefiles} and the main file you want to use as model and finally click on the \emph{Generate} button.
	\begin{figure}[H]
	\begin{center}
		\includePict[width=10cm]{xlifepp_new_project_2.png}
	\end{center}
\end{figure}
\item If generation succeeds, the project is created in the {\ttfamily build} folder of your project folder. If you choose {\scshape CodeBlocks} as generator, you can run it directly by clicking on the {\ttfamily run CodeBlocks} button:
\begin{figure}[H]
	\begin{center}
		\includePict[width=10cm]{xlifepp_new_project_3.png}
	\end{center}
\end{figure}
\item Finally, running  {\scshape CodeBlocks}, your \xlifepp project may be easily compiled by clicking on the {\ttfamily build} button \includePict[scale=0.22]{codeblocks_build_icon.png}:
\begin{figure}[H]
	\begin{center}
		\includePict[width=17cm]{xlifepp_new_project_4.png}
	\end{center}
\end{figure}
\end{enumerate}

\section{How to use \xlifepp}

\subsection{Writing a program using \xlifepp}

All the \xlifepp library is defined in the namespace \emph{\textbf{xlifepp}}. Then the users,
if they refer to library objects, have to add once in their programs the command \emph{using
namespace xlifepp}. Besides, they have to use the "super" header file \emph{\textbf{xlife++.h}}
only in the main. A main program looks like, for instance:

\vspace{.2cm}

\begin{lstlisting}
#include "xlife++.h"
using namespace xlifepp;

int main()
{
   init(en); // mandatory initialization of xlife++
   ...
}
\end{lstlisting}

\subsection{Compiling a \xlifepp program using commands}

\subsubsection{On ENSTA linux system}

\begin{enumerate}
\item Execute the command  \emph{useuma xlifepp}.
\item Create your working directory, say \emph{dirapp}.
\item execute the command \emph{xlifepp.sh} and answer to the questions:\\
\ \ \ Directory: \emph{dirapp}\\
\ \ \ Generator: CodeBlocks - Unix Makefiles\\
\ \ \ Copy main.cpp: Y(es)
\item Open the CodeBlocks project (\emph{XLiFE++-user.cbp}) generated in your \emph{dirapp}, compile and run it.
\item You can now modify the \emph{main.cpp} file to deal with your own application.
\end{enumerate}
If you do not use {\scshape CodeBlocks}, to build your executable you can run the \emph{make} command in a terminal.

\subsubsection{On any system}
Same method as previous except the first command. You have to localize in the \emph{bin} directory of the XLiFE++ install directory the \emph{./xlifepp.sh} file or \emph{./xlifepp.bat} file for windows architecture.


\chapter{Getting started}
\label{ch.gettingstarted}
\inputDoc*{getting_started}

In this example, all fundamental objects have been exposed. Please notice how close to the Mathematics, \xlifepp input language is. Other examples may be found in the user documentation available on the web site of \xlifepp.
%\chapter{Examples}
%\label{ch.examples}
%\inputDoc*{examples}
%
%
%
%\chapter{\xlifepp written in C++}
%\label{ch.xlifepp_and_cpp}
%\inputDoc*{xlifepp_and_cpp}
%
%\chapter*{License and credits}
%
%\section*{License}
%\xlifepp is copyright (C) 2010-2014 by E. Lunéville and N. Kielbasiewicz and is distributed under the terms of the GNU General Public License (GPL) (Version 3 or later, see https://www.gnu.org/licenses/gpl-3.0.en.html). This means that everyone is free to use \xlifepp and to redistribute it on a free basis. \xlifepp is not in the public domain; it is copyrighted and there are restrictions on its distribution. You cannot integrate \xlifepp (in full or in parts) in any closed-source software you plan to distribute (commercially or not). If you want to integrate parts of \xlifepp into a closed-source software, or want to sell a modified closed-source version of \xlifepp, you will need to obtain a different license. Please contact us directly for more information.\\
%
%The developers do not assume any responsibility in
%the numerical results obtained using the \xlifepp library and are not responsible of bugs. 

\section*{Credits}
The \xlifepp library has been mainly developed by E. Lunéville and N. Kielbasiewicz of POEMS lab (UMR 7231, CNRS-ENSTA ParisTech-INRIA). Some parts are inherited from Melina++ library developed by D. Martin (IRMAR lab, Rennes University, now retired) and E. Lunéville. Other contributors are:
\begin{itemize}
\item Y. Lafranche (IRMAR lab), mesh tools using subdivision algorithms
\item C. Chambeyron (POEMS lab), iterative solvers
\item M.H N'Guyen (POEMS lab), eigen solvers and OpenMP implementation
\end{itemize}


\end{document}

