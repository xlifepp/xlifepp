\documentclass[10pt,svgnames,english]{article}

\usepackage[mode=user,toplevel=chapter]{xlifepp}

\hypersetup{%
    pdftitle={Helmholtz 3D BEM convergence versus mesh order},%
    pdfauthor={E. Lun\'eville},%
    pdfpagemode=UseOutlines,%
    linkbordercolor=0 0 0,%
    linkcolor=black,%
    citecolor=black,%
    colorlinks=true%
}%

%\date{} % date %
\title{Helmholtz 3D BEM convergence versus mesh order} % titre %
\author{Eric {\scshape Lunéville}} % auteur %

\begin{document}

\maketitle

This document describes some convergence results of BEM methods for the 3D Helmholtz problem on the unit sphere versus the mesh order and the FE Lagrange order.The next code computes the errors for mesh of order $1\le p\le 5$ and Lagrange FE of order $0\le k\le p$ and for mesh steps from $0.1$ up to $0.7$ uniformly distributed. Next figures present the absolute error at point $(0.,0.,2.)$ versus the mesh step. In legends, \verb|q| is the slope of the curves (log-log) in a coherent region, say a numerical estimate of the order of the approximation.

\section*{Helmhotz 3D problem}
The following Hemlmoltz problem is concerned:
\begin{equation}\label{laplace}
    \begin{dcases}
        -\Delta  +k^2u=0 & \text{in } \Omega=\{|x|>1\}\\
        u=-u_i & \text{on }\Gamma=\{|x|=1\}\\
        \displaystyle \int_{|x|=R} \left|\frac{\partial u}{\partial r}-ik\,u\right|^2d\gamma\stackrel{R\rightarrow +\infty}{\longrightarrow}0&\text{ cond. de Sommerfeld}
    \end{dcases}
\end{equation}
with $u_i$ the incident field (the plane wave $u_i(x_1,x_2,x_3)=e^{ik\,x_1}$ in experiments).\\

It can be solved by using the Dirichlet single layer integral equation:
$$\int_{\Gamma}\int_{\Gamma} u(y)G(x,y)v(x)d\gamma_yd\gamma_x = -\int_{\Gamma} u_i(x)v(x)d\gamma_x$$
with $G$ the Green kernel:
$$G(x,y)=\frac{e^{ik|x-y|}}{4\pi|x-y|}.$$

The following \xlifepp code computes the errors versus the mesh order and the FE Lagrange order for several mesh steps:

\begin{lstlisting}
Kernel G = Helmholtz3dKernel(pars);  //load Helmholtz3D kernel
Function f= = Function(uinc,"uinc", pars);
FunctionscatSol = Function(scatteredFieldSphereDirichlet,pars);
Real hmin=0.1, hmax=0.71, hstep=0.1;// h on sphere
Number momin=1, momax=5;
Number omin=0;
Number imo0=12, imo1=12, imo2=10;  //order of quadratures for singular integral
Number iro=10;                     //order of quadrature for intg. rep.
IntegrationMethods ssim(_method=_SauterSchwabIM,_order=imo0,_bound=0.,_method=_defaultRule,_order=imo1, _bound=2.,_method=_defaultRule,_order=imo2);
Number nbn=1+(hmax-hmin)/hstep;  nh.resize(nbn);
Real h=hmin;
for(Number i=0;i<nbn; i++,h+=hstep) nh[i]=h;
Number nbpts=1; Complex cv;
vector<Point> pts(nbpts);
pts[0]=Point(0.,0.,2.);
Vector<Complex> val(nbpts), sol(nbpts);
for(Number i=0;i<nbpts;i++) sol[i]=scatSol(pts[i],cv);
std::ofstream eout(filename.c_str());
String ext, fnmesh;

for(Number mo=momin;mo<=momax; mo++)  //loop on mesh order
{
  ext="_mo="+xlifepp::tostring(mo);
  ofstream eout((filename+ext+".dat").c_str());  // error file
  for (Number i=0; i<nbn; i++) // loop on mesh size
  {
    h=nh[i]; ext+="_h="+xlifepp::tostring(h);
    fnmesh="m_o"+xlifepp::tostring(mo)+"_l="+xlifepp::tostring(h)+".msh";
    Mesh msp=Mesh(fnmesh,_name=fnmesh); // load mesh
    Domain sphere = msp.domain(0);
    for(order = omin; order< mo+1; order++) // loop on FE order
    {
      ext+="_P="+xlifepp::tostring(order);
      Space V(_domain=sphere, _FE_type=Lagrange, _order=order, _name="V", _notOptimizeNumbering);
      Unknown u(V, _name="u"); TestFunction v(u, _name="v");
      BilinearForm bf= = intg(sphere,sphere,u*G*v,_method=ssim);
      LinearForm fv= -intg(sphere,f*v,_quad=_defaultRule,_order=imo2);
      TermMatrix A(bf, _name="A");
      TermVector B(fv, _name="B");
      TermVector U = directSolve(A, B);
      LinearForm irf=intg(sphere,G*u, _quad=_defaultRule,_order=iro);
      integralRepresentation(pts,irf,U,val);
      Real err=0.;
      for(Number i=0;i<nbpts;i++)  err+=abs(val[i]-sol[i]);
      err/=nbpts;
      eout<<V.dimSpace()<<" "<<h<<" "<<err<<" "<<tA<<" "<<tS<<" "<<tR<<eol;
    } //end loop FE order
  } //end loop mesh size
  eout.close();
} //end loop mesh order
\end{lstlisting}
\vspace{4mm}
\begin{warningbox}
 GMSH provides two ways to mesh a sphere : built-in kernel and OpenCascade kernel. When using the built-in kernel, nodes are not located precisely on the sphere (error around $10^{-5}$ with the unit sphere) whereas when using the OpenCascade kernel they are located with a tolerance of $10^{-15}$. It isn't really a problem when dealing with low order meshes or low order elements but in this study is crucial to get a good approximation of the sphere.
 
\end{warningbox}
\mbox{}\\

The following convergence orders can be observed:
\begin{center}
    \begin{tabular}{|c|c|c|c|c|c|c|}
        \hline
        mesh/FE order & 0 & 1 & 2 & 3 & 4 & 5\\
        \hline
        \hline
        1 & $\sim$ 2 & $\sim$ 2 & & & & \\
        \hline 
        2 & $\sim$ 2 & $\sim$ 4 & $\sim$ 4 & & & \\
        \hline 
        3 & $\sim$ 2 & $\sim$ 4.1 & $\sim$ 4.1 & $\sim$ 4.1 & & \\
        \hline 
        4 & $\sim$ 1.8 & $\sim$ 5& $\sim$ 5.5 & $\sim$ 5.5 & $\sim$ 5.5 & \\
        \hline 
        5 & $\sim$ 1.9 & $\sim$ 6.1 & $\sim$ 6.2 & $\sim$ 5.8 & $\sim$ 5.8 & $\sim$ 5.8\\
        \hline
    \end{tabular}
\end{center}

The rate of convergence appears to be governed mainly by the mesh order ($q$), i.e. $h^{q+1}$, with the finite element error behaving at higher order. Except for mesh order 2 where it can be noted a super-convergence effect ($h^4$ instead of $h^3$). 

\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Helmholtz3d_sphere_BEM_DirSL_mesh_order=1.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Helmholtz3d_sphere_BEM_DirSL_mesh_order=2.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Helmholtz3d_sphere_BEM_DirSL_mesh_order=3.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Helmholtz3d_sphere_BEM_DirSL_mesh_order=4.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Helmholtz3d_sphere_BEM_DirSL_mesh_order=5.png}
\end{center}




\end{document}

