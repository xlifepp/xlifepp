\documentclass[10pt,svgnames,english]{article}

\usepackage[mode=user,toplevel=chapter]{xlifepp}
\usepackage{algorithm}
\usepackage{algorithmic}


\hypersetup{%
    pdftitle={Quadrature for self-influence coefficient in 2D},%
    pdfauthor={E. Lun\'eville},%
    pdfpagemode=UseOutlines,%
    linkbordercolor=0 0 0,%
    linkcolor=black,%
    citecolor=black,%
    colorlinks=true%
}%

%\date{} % date %
\title{Quadrature for self-influence coefficient in 2D} % titre %
\author{Eric {\scshape Lun\'eville}} % auteur %

\begin{document}
\maketitle
We focus here on the computation of self-influence coefficients in case of 2D-BEM :
$$I=\int_{\Gamma_\ell}\int_{\Gamma_\ell} G(x,y)w_i(x)\tau_j(y)dx\,dy$$
where 
\begin{itemize}
    \item $\Gamma_\ell=F_\ell(]0,1[])$ a curved or a linear arc
    \item $G(x,y)$ a kernel, singular when $x=y$ (typically in $\log|x-y|$, $1/|x-y|$)
    \item $w_i(x)=\hat{w}_i(F_\ell^{-1}(x))$ and $\tau_j(y)=\hat{\tau}_j(F_\ell^{-1}(x))$ some FE basis function
\end{itemize}
Integral $I$ reads
$$I=\int_0^1\int_0^1 G(F_\ell(\hat{x}),F_\ell(\hat{y}))\hat{w}_i(\hat{x})\hat{\tau}_j(\hat{y})|J_\ell(\hat{x})||J_\ell(\hat{y})|d\hat{x}\,d\hat{y}.$$
Because the singularity along the line $x-y=0$, using a standard product of Gauss-Legendre formulae may require a lot of points of quadrature, typically 300x300 points to get $10^-9$ accuracy. In literature, we get a lot of papers dealing with integral having a singularity at one point but not on a line. Besides they focus on particular kernel, using explicitly some analytical properties. In this paper, we propose a method combining standard Gauss-Legendre, but concentrating points close to the singular line. \\

In the sequel we set $H(\hat{x},\hat{y})=G(F_\ell(\hat{x}),F_\ell(\hat{y}))\,\hat{w}_i(\hat{x})\hat{\tau}_j(\hat{y})|J_\ell(\hat{x})||J_\ell(\hat{y})|$. 
The integral is written as
$$I=\int_0^1\int_0^u H(u-v,v)\,dv\,du+\int_0^1\int_u^1 H(1+u-v,v)\, dv\,du.$$
The first integral is defined on the lower triangle $x+y<1$, integrating on lines $x+y=u$, $0<u<1$ while the second is defined on the upper triangle $x+y>1$ integrating on lines $x+y=1+u$, $0<u<1$. Now using the variable changes  $v=(1+s)\frac{u}{2}$ ($s=\frac{2v} {u}-1$) and $v=\frac{1+s+(1-s)u}{2}$ ($s=\frac{2v-1-u} {1-u}$)
$$I=\int_0^1\frac{u}{2}\int_{-1}^1 H\left((1-s)\frac{u}{2},(1+s)\frac{u}{2}\right)\,ds\,du-\int_0^1\frac{u}{2}\int_{-1}^1 H\left(\frac{1+u-s(1-u)}{2},\frac{1+u+s(1-u)}{2}\right) ds\,du.$$
Both, intern integrals are singular in $s=0$. Following \cite{KYP2019}, using the $s=t^p$ substitution with $p$ an odd integer:
$$I=\int_0^1\frac{pu}{2}\int_{-1}^1 H\left((1-t^p)\frac{u}{2},(1+t^p)\frac{u}{2}\right)t^{p-1}\,dt\,du-\int_0^1\frac{pu}{2}\int_{-1}^1 H\left(\frac{1+u-t^p(1-u)}{2},\frac{1+u+t^p(1-u)}{2}\right)t^{p-1} dt\,du.$$
This substitution cancels the singularity at $s=0$ as $p>1$. Introducing two Gauss-Legendre rule $(\omega_m^u,\ x_m^u)_{m=1,M}$ and $(\omega_n^t,\ x_n^t)_{n=1,N}$ with $N$ odd in order to avoid the quadrature point 0, the integral is approximated by
$$
\begin{array}{ll}
    I&\displaystyle \approx\frac{1}{4}\sum_{m=1,M}p\omega_m^u\tilde{x}_m^u\sum_{n=1,N}\omega_n^t H\left((1-(x_n^t)^p)\frac{\tilde{x}_m^u}{2},(1+(x_n^t)^p))\frac{\tilde{x}_m^u}{2}\right)(x_n^t)^{p-1}\\
&\displaystyle -\frac{1}{4}\sum_{m=1,M}p\omega_m^u\tilde{x}_m^u\sum_{n=1,N}\omega_n^t H\left(\frac{1+\tilde{x}_m^u-(x_n^t)^p(1-\tilde{x}_m^u)}{2},\frac{1+\tilde{x}_m^u+(x_n^t)^p(1-\tilde{x}_m^u)}{2}\right)(x_n^t)^{p-1}
\end{array}$$
where $\tilde{x}_m^u=\frac{1+x_m^u}{2}$.\\

The following picture shows the distribution of Gauss points for M=9, N=9 and p=5.
\begin{center}
\includePict[width=10cm]{self2D_M=9_N=9_P=5.png}
\end{center}

To validate this quadrature we use the Dirichlet problem for Helmholtz equation on the unity disk and the single layer BEM equation :
$$\int_\Gamma \int_\Gamma G(x,y)u(y).\bar{v}(y)dx\,dy=\int_\Gamma f(y).\bar{v}(y)dy$$
The boundary of disk is meshed with linear element (P1 mesh) or quadratic element (P2 mesh). As the exact solution is known for an incident plane wave, an $L^2$ error can be evaluated on a domain exterior to the disk.
\begin{center}
    \includePict[width=16cm]{cv_bem_sl_2d.png}
\end{center}
It can be observed that with a poor quadrature the convergence rate of BEM (P2 mesh M=10, N=10 in red) is not captured. Even with a more  expansive quadrature M=30, N=300 (cyan curve) errors for small steps are not well captured. With the new quadrature with M=25, N=15 and P=9 (black curve) the BEM errors are well captured!
\begin{thebibliography}{1}
\bibitem{KYP2019} Kirkup, Stephen , Yazdani, Javad and Papazafeiropoulos, George. (2019). Quadrature Rules for Functions with a Mid-Point Logarithmic Singularity in the Boundary Element Method Based on the \verb?x = t^p? Substitution. American Journal of Computational Mathematics. 9. 282-301. 10.4236/ajcm.2019.94021. 
\end{thebibliography}

\end{document}