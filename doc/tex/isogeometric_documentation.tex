\documentclass[10pt,svgnames,english]{article}

\usepackage[mode=user,toplevel=chapter]{xlifepp}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{verbatim} 


\hypersetup{%
    pdftitle={Geodesics documentation},%
    pdfauthor={E. Lun\'eville},%
    pdfpagemode=UseOutlines,%
    linkbordercolor=0 0 0,%
    linkcolor=black,%
    citecolor=black,%
    colorlinks=true%
}%

%\date{} % date %
\title{Isogeometric computation} % titre %
\author{Eric {\scshape Lun\'eville}} % auteur %

\begin{document}
\maketitle
The idea of isogeometric elements consist in using elements defined on the space of parameters describing the integration domain.
\begin{center}
    \includePict[width=16cm]{isogeometric.png}
\end{center}
Let the map \(\varphi:\tilde{\Sigma}\subset\mathbb{R}^{m} \mapsto \Sigma\subset\mathbb{R}^n\) with (\(m\le n=1,2,3\)). We assume that \(\varphi\) is a diffeomorphism. Considering \(\tilde{\mathcal{T}}_h=\bigcup_\ell\tilde{T}_\ell\)  a conforming mesh of \(\tilde{\Sigma}\), the transported mesh on \(\Sigma\) is  defined as \(\mathcal{T}_h=\bigcup_\ell T_\ell\) with \(T_\ell=\varphi(\tilde{T}_\ell)\). 
Let \(\hat{\tau}_k\) any shape function on the reference element (say \(\hat{T}\)), by transporting it on \(\Sigma\):
\[
\tau_k^\ell(x)=\hat{\tau}_k\big( F_\ell^{-1}\big(\varphi^{-1}(x)\big)\big)\quad \forall x\in T_\ell, \quad \hat{\tau}_k(\hat{x})=\tau_k^\ell\big(\varphi\big( F_\ell(\hat{x})\big)\big)\quad \forall \hat{x}\in \hat{T}\quad\]
the basis function are defined as usual by collecting the shape functions \(\tau_k^\ell\), regarding the sharing DoF rules:
\[w_i=\tau_k^\ell\text{ on }T_\ell .\]
The approximation space \(V_h=\left\{w_i\right\}_i\) will be a discrete subspace of \(H^s(\Sigma)\), \(s\) depending on the finite element used, the regularity of \(\varphi\) and the assembling process considered (at least \(L^2(\Sigma)\) for common FE approximations).\\

In FE computation, various integrals are handled, for instance:
\[
\begin{array}{ll}
\displaystyle a_{ij}=\int_\Sigma \mathcal{O}_u(w_j,\nabla w_j, \nabla^2w_j,\sigma)\otimes  \mathcal{O}_v(w_i,\nabla w_i,\nabla^2w_i,\sigma)d\sigma& \text{standard FEM bilinear form}\\[3mm]
\displaystyle  b_{ij}=\int_\Gamma\int_\Sigma \mathcal{O}_u(w_j,\nabla w_j, \nabla^2w_j,\sigma)\otimes \mathcal{O}_kK(\gamma,\sigma) \otimes \mathcal{O}_v(w_i,\nabla w_i,\nabla^2w_i,\gamma)d\sigma\gamma& \text{generalized BEM bilinear form}
\end{array}
\]

where \(\mathcal{O}_u\) and \(\mathcal{O}_v\) stand for operators involving values and derivatives of basis function and may be some additional user functions.  \(\mathcal{O}_k\) stands for an operator on kernel \(K\) and symbols \(\otimes\) represents any product operator (ordinary product, inner product, cross product, contracted product).\\

Expanding the first integral, we get (second derivatives are omitted):

\[
\begin{array}{ll}
a_{ij}&=\displaystyle \sum_\ell \int_{T_\ell} \mathcal{O}_u(w_j,\nabla w_j,\sigma)\otimes  \mathcal{O}_v(w_i,\nabla w_i,\sigma)d\sigma\\
&=\displaystyle \sum_\ell \int_{T_\ell} \mathcal{O}_u(\tau^\ell_q,\nabla \tau^\ell_q, \sigma)\otimes  \mathcal{O}_v(\tau^\ell_p,\nabla \tau^\ell_p,\sigma)d\sigma\quad (p,q\text{ are respectively related to }i,j)\\
&=\displaystyle \sum_\ell \int_{\hat{T}} \mathcal{O}_u\big(\hat{\tau}_q(\hat{x}),G_\ell(\hat{x})\hat{\nabla}\hat{\tau}_q(\hat{x}), \varphi_\ell(\hat{x})\big)\otimes  \mathcal{O}_v\big(\hat{\tau}_p(\hat{x}), G_\ell(\hat{x})\hat{\nabla}\hat{\tau}_p(\hat{x}),\varphi_\ell(\hat{x})\big)|J_{\varphi_\ell}(\hat{x})|d\hat{x}\\
\end{array}
\]

where \(\varphi_\ell(\hat{x})=\varphi\circ F_\ell(\hat{x})\), \(J_{\varphi_\ell}=J_\ell\tilde{J}\) and \(G_\ell(\hat{x})=\tilde{J}^{-t}(\tilde{x}) J^{-t}_\ell(\hat{x})\) with \(J_\ell\) the jacobian of \(F_\ell\) and \(\tilde{J}\) the jacobian of \(\varphi\). Denoting \(\tilde{x}=\varphi^{-1}(x),\ \hat{x}=F_\ell^{-1}(\tilde{x})\), we have:
\[
\nabla_x\tau_k^\ell(x)=\tilde{J}^{-t}(\tilde{x}) J^{-t}_\ell(\hat{x})\hat{\nabla}\hat{\tau}_k(\hat{x})= G_\ell(\hat{x})\hat{\nabla}\hat{\tau}_k(\hat{x}).\]
When \(m<n\), \(\tilde{J}^{-t}\) is the pseudo-inverse \(\tilde{J}(\tilde{J}^t \tilde{J})^{-t}.\)\\

For BEM integral, the calculus gives:
\[
\begin{array}{ll}
    b_{ij}&=\displaystyle \sum_k \sum_\ell \int_{T_\ell} \int_{T_k}\mathcal{O}_u(w_j,\nabla w_j,\sigma)\otimes K(\gamma,\sigma) \otimes \mathcal{O}_v(w_i,\nabla w_i,\gamma)d\sigma\,d\gamma\\
    &=\displaystyle \sum_k \sum_\ell \int_{\hat{T}} \int_{\hat{T}}\mathcal{O}_u\big(\hat{\tau}_q(\hat{y}),G_\ell(\hat{y})\hat{\nabla}\hat{\tau}_q(\hat{y}), \varphi_\ell(\hat{y})\big)\otimes \mathcal{O}_kK(\varphi_k(\hat{x}),\varphi_\ell(\hat{y})) \otimes \mathcal{O}_v\big(\hat{\tau}_p(\hat{x}),G_k(\hat{x})\hat{\nabla}\hat{\tau}_p(\hat{x}), \varphi_k(\hat{x})\big)|J_{\varphi_\ell}(\hat{y})|\,|J_{\varphi_k}(\hat{x})|\,d\hat{y}\,d\hat{x}.\\
\end{array}\]

Note that shape functions or finite elements could not be the same.\\

\xlifepp compute single integrals using the following algorithm:\\

\hspace{5mm}\fbox{\parbox{14cm}{
 loop on elements \(T_\ell\)\\
\mbox{}\hspace{5mm} retry data related to \(T_\ell\)\\
\mbox{}\hspace{5mm} initialize elementary matrix \(M^\ell=0\)\\
\mbox{}\hspace{5mm}loop on quadrature points \(\hat{x}_r\)\\
\mbox{}\hspace{10mm}compute shape values \(\hat{\tau}_p(\hat{x}_r)\) and \(\hat{\nabla}\hat{\tau}_p(\hat{x}_r)\ \forall p\)\\
\mbox{}\hspace{10mm}compute geometric data (jacobian, normal, \ldots) at \(x_r\)\\
\mbox{}\hspace{10mm}apply transformation \(G_\ell\) to \(\hat{\nabla}\hat{\tau}_p(\hat{x}_r)\ \forall p\)\\
\mbox{}\hspace{10mm}apply other transformation (Piola, \ldots) if required\\
\mbox{}\hspace{10mm}compute the vector \(O_u(\hat{x}_r)_p=\mathcal{O}_u\big(\hat{\tau}_p(\hat{x}_r),G_\ell(\hat{x}_r)\hat{\nabla}\hat{\tau}_p(\hat{x}_r), \varphi_\ell(\hat{x}_r)\big)\ \forall p\)\\
\mbox{}\hspace{10mm}compute the vector \(O_v(\hat{x}_r)_p=\mathcal{O}_v\big(\hat{\tau}_p(\hat{x}_r),G_\ell(\hat{x}_r)\hat{\nabla}\hat{\tau}_p(\hat{x}_r), \varphi_\ell(\hat{x}_r)\big)\  \forall p\)\\
\mbox{}\hspace{10mm}compute \(M_{pq}^\ell+=\omega_r\,|J_{\varphi_\ell}(\hat{x}_r)|\, O_u(\hat{x}_r)_q\otimes O_v(\hat{x}_r)_p \ \forall p,q\) (accumulate, \(\omega_r\) quadrature weight)\\
\mbox{}\hspace{5mm}assembly elementary matrix \(M^\ell\) to global matrix \(M\)
}}\\


Note that the standard FEM algorithm is recovered by considering \(\varphi=Id\)! For BEM integrals the principle remains the same.\\

The question is how to implement the isogeometric approach in \xlifepp?\\

To build the map \(F_\ell\), the elements \(\tilde{T}_\ell\) are required. Using Lagrange isoparametric approximation the map is given by \(F_\ell(\hat{x})=\sum_p x^\ell_p\hat{\tau}_p(\hat{x})\) where \(\tilde{x}^\ell_p\) denotes the nodes of element  \(\tilde{T}_\ell\). There are two possibilities:
\begin{itemize}
    \item use  \(\tilde{T}_\ell\) in the loop if they have been built; it means to work with the intermediate mesh and as a consequence to manage it 
    \item rebuild \(\tilde{T}_\ell\) from \(T_\ell\) using the inverse map \(\varphi^{-1}\) : \(\tilde{x}^\ell_p=\varphi^{-1}(x^\ell_p)\); that it may be costly
\end{itemize}
The second solution present the advantage to less impact the \xlifepp architecture. To make it more effective, the intermediate nodes  \(\tilde{x}^\ell_p\) can be precomputed and stored in the original geometric element (\(T_\ell\)).
\vspace{1mm}
\begin{lstlisting}[deletekeywords={[3]x,dx}]
class MeshElement
{
  public:
  \ldots
  std::vector<Point*> isoNodes;  //! pointer to isonodes      
\end{lstlisting}
 \vspace{1mm}
These intermediate nodes are constructed whenever an isogeometric computation is required on a domain, by the \class{MeshDomain} function:
\vspace{1mm}
\begin{lstlisting}[deletekeywords={[3]x,dx}]
void MeshDomain::buildIsoNodes() const     
\end{lstlisting}
\vspace{1mm}
Note that the isonodes are created on the stack and are deleted by the \class{MeshDomain} destructor. Contrary to the original mesh nodes that are collected together in a \class{Mesh} object uniquely, iso nodes are duplicated if they are shared by two \class{MeshElement}. However, if two adjacent elements address two different parametrizations, the shared nodes might have not the same related isonodes!\\

Regarding the job to do in the computation algorithms : mapping the quadrature points from reference space to the real domain using \(\phi\circ F_\ell\) and computing the jacobian  \(D(\phi\circ F_\ell)=D\,\phi(F_\ell)D\,F_\ell\), a simple way to do it consists in adapting the \class{GeomMap} : adding some additional data and modifying some computation functions (\cmd{geomMap}, \cmd{geomapInverse}, \cmd{computeJacobian}):
\vspace{1mm}
\begin{lstlisting}[deletekeywords={[3]x,dx}]
class GeomMapda
{
  \ldots
  Parametrization* isoPar_p; //pointer to isoGeometric parametrization (default 0)
  bool useParametrization;   //if true take into account isogeometric parametrization (default false)        
\end{lstlisting}
\vspace{1mm}
The \cmd{geomMap} function is adapted as following:
\begin{lstlisting}[deletekeywords={[3]x,dx}]
Point GeomMapData::geomMap(const ShapeValues& shv, number_t side)
{
  Point pt(std::vector<real_t>(spaceDim, 0.));    
  //apply standard Fl transformation
  \ldots
  if(!useParametrization) return pt;  // no additional mapping
  return (*isoPar_p)(pt);             // additional mapping
}
\end{lstlisting}
\vspace{1mm}

Besides, we have to find a way to offer to the users the possibility to use isogeometric approximation. Attach an additional flag to the bilinear form seems to be the simplest method :
\vspace{1mm}
\begin{lstlisting}[deletekeywords={[3]x,dx,symmetry}]
class BasicBilinearForm
{ \ldots
  public:
   mutable SymType symmetry;       
   bool isoGeometric;  //isogeometric flag
\end{lstlisting}
All the pseudo constructors would have an additional argument, for instance :
\vspace{1mm}
\begin{lstlisting}[deletekeywords={[3]x,dx}]
BilinearForm intg(const GeomDomain&, const OperatorOnUnknowns&,const IntegrationMethod&, 
                  bool iso=false, SymType = _undefSymmetry);    
\end{lstlisting}

All isogeometric bilinear forms on the same domain should be gathered in computational loops and separated from standard bilinear forms in order to avoid treating both cases simultaneously. In practice, we decide to treat both cases in the same computation imposing to use the isogeometric computation for all bilinear forms (same type and same domain) as soon as one of these forms has been declared isogeometric.

\subsection*{Some examples}
\subsubsection*{1D arc}
As a first example, consider the circular arc $\Omega=\big\{(\cos \theta,\sin \theta),\ 0\le \theta\le \frac{\pi}{2}\big\}$ and the following program computing the arc length using the mass matrix $\mathbb{M}$ related to the FE approximation used (say P1); more precisely the scalar product $(\mathbb{M}U|U)$ with $U_i=1\ \forall i$:
\vspace{1mm}
\begin{lstlisting}[]
CircArc C(_center=Point(0.,0.,0.),_v1=Point(1.,0.,0.),_v2=Point(0.,1.,0.),_nnodes=5,_domain_name="Omega");
Mesh M(C,_segment,1,_gmsh);
Domain omega=Me.domain("Omega");
Space V(omega,inlk,"V"); Unknown u(V,"u");TestFunction v(u,"v");
TermMatrix M(intg(omega,u*v,GaussLegendre,5),"M");            //standard computation
TermMatrix Mi(intg(omega,u*v,isoGeo,GaussLegendre,5),"Mi");   //isogeometric computation
TermVector un(u,omega,1.);
Real l1=real(M*un|un), li=real(Mi*un|un), l=pi_/2;
thePrintStream<<"exact length = "<<l<<eol;
thePrintStream<<"     M*un|un = "<<l1<<" error="<<abs(l1-l)<<eol;
thePrintStream<<"    Mi*un|un = "<<li<<" error="<<abs(li-l)<<eol;
\end{lstlisting}
Without surprise, we get he following results:\small
\begin{verbatim}
exact length = 1.5708
M*un|un = 1.5688 error=0.0100738
Mi*un|un = 1.5708 error=6.66134e-16
\end{verbatim}
\normalsize
showing the efficiency of the isogeometric approach!
\begin{center}
    \includePict[width=10cm]{isoArc.png}
\end{center}
 \subsubsection*{2D crown}
 In order to validate the computation in 2D, we consider first a quarter of a crown $\Omega=\left\{(r\cos\theta, r\sin\theta),\ \epsilon<r<1,\ 0 < \theta < \frac{\pi}{2}\right\} $. The following code shows how to implement it. As the crown is not a canonical geometry in \xlifepp, there is no parametrization attached to. However, the usual disk parametrization may be used. This is what is done 'manually' here.
 \vspace{1mm}
 \begin{lstlisting}[]
 Real h=0.1, eps=0.1;
 CircArc Ce(_center=Point(0.,0.),_v1=Point(1.,0.),_v2=Point(0.,1.),_hsteps=h);
 CircArc Ci(_center=Point(0.,0.),_v1=Point(0.,eps),_v2=Point(eps,0.),_hsteps=h);
 Segment S1(_v1=Point(0.,1.),_v2=Point(0.,eps),_hsteps=h);
 Segment S2(_v1=Point(eps,0.),_v2=Point(1,0.),_hsteps=h);
 Geometry Cour=surfaceFrom(Ce+S1+Ci+S2,"Omega",true);
 Mesh M(Cour,_triangle,1,_gmsh);
 M.geometry_p=new Disk(_center=Point(0.,0.),_radius=1,_angle1=0, _angle2=90*deg_,_nnodes=10,_domain_name="cour");
 Domain omega=M.domain("Omega");
 omega.setGeometry(M.geometry_p);
 Space V(omega,inlk,"V"); Unknown u(V,"u");TestFunction v(u,"v");
 TermMatrix M(intg(omega,u*v,GaussLegendre,5),"M");            //standard computation
 TermMatrix Mi(intg(omega,u*v,isoGeo,GaussLegendre,5),"Mi");   //isogeometric computation
 TermVector un(u,omega,1.);
 Real l1=real(M*un|un), li=real(Mi*un|un), l=pi_/4;
 thePrintStream<<"exact surface = "<<l<<eol;
 thePrintStream<<"      M*un|un = "<<l1<<" error="<<abs(l1-l)<<eol;
 thePrintStream<<"     Mi*un|un = "<<li<<" error="<<abs(li-l)<<eol;
\end{lstlisting}
We get again an excellent result for the isogeometric computation with an error comparable to $10^{-16}$.
\small
\begin{verbatim}
exact surface = 0.777544
M*un|un  = 0.777066 error=0.000478127
Mi*un|un = 0.777544 error=1.11022e-16
\end{verbatim}
\normalsize
 \begin{center}
     \includePict[width=8cm, trim=100mm 0 100mm 0, clip]{isoCrown.png}
     \includePict[width=8cm, trim=100mm 0 100mm 0, clip]{isoCrownPar.png}\\
     left: isogeometric mesh and P1 mesh (dashed), right: related mesh in parameters space
 \end{center}
 

  \subsubsection*{2D disk}
  The following example deals with a quarter of a disk $\Omega=\left\{(r\cos\theta, r\sin\theta),\ 0<r<1,\ 0 < \theta < \frac{\pi}{2}\right\} $. As it can be noticed, it does not work well because the map $(r,\theta) \rightarrow \Omega$ is degenerated at $r=0$. As a consequence, some curved triangles are missed and the isogeometric error is of same order of the standard P1 error!
  \vspace{1mm}
  \begin{lstlisting}[]
  Disk D(_center=Point(0.,0.),_radius=1,_angle1=0, _angle2=90*deg_,_nnodes=9,_domain_name="Omega");
  Mesh M(D,_triangle,1,_gmsh);
  Domain omeg=M.domain("Omega");
  Space V(omega,inlk,"V");
  Unknown u(V,"u");TestFunction v(u,"v");
  TermMatrix M(intg(omega,u*v,GaussLegendre,9),"M");
  TermMatrix Mi(intg(omega,u*v,isoGeo,GaussLegendre,9),"Mi");
  TermVector un(uh,omega,1.);
  Real l1=real(M*un|un), li=real(Mi*un|un), l=pi_/4;
  thePrintStream<<"exact surface = "<<l<<eol;
  thePrintStream<<"      M*un|un = "<<l1<<" error="<<abs(l1-l)<<eol;
  thePrintStream<<"     Mi*un|un = "<<li<<" error="<<abs(li-l)<<eol;
  \end{lstlisting}
  \small
  \begin{verbatim}
  exact surface = 0.785398
  M*un|un = 0.780361 error=0.00503688
  Mi*un|un = 0.781308 error=0.00409062
  \end{verbatim}
  \normalsize
  \begin{center}
  	\includePict[width=8cm, trim=100mm 0 100mm 0, clip]{isoDisk.png}
  	\includePict[width=8cm, trim=100mm 0 100mm 0, clip]{isoDiskPar.png}\\
  	left: isogeometric mesh and P1 mesh (dashed), right: related mesh in parameters space
  \end{center}
To cure this default, one way consists in approximate the quarter of disk by a quarter of crown 
$$\Omega_\varepsilon=\left\{(r\cos\theta, r\sin\theta),\ \varepsilon<r<1,\ 0 < \theta < \frac{\pi}{2}\right\}$$
 with $\varepsilon$ enough small. The approximation error can be quantified because $\text{mes}(\Omega_\epsilon)=\frac{\pi}{4}(1-\epsilon^2)$. So the approximation error is $\frac{\pi}{4}\varepsilon^2$ and $\varepsilon$ can be adjust to control the error. The following code do this job guaranteeing an error of $10^{-8}$:
  \vspace{2mm}
 \begin{lstlisting}[]
 Real h=0.1; Number p=8; //to get 8 decimal precision
 Real eps=sqrt(4./pi_)*pow(10,(-0.5*p));
 CircArc Ce(_center=Point(0.,0.),_v1=Point(1.,0.),_v2=Point(0.,1.),_hsteps=h);
 CircArc Ci(_center=Point(0.,0.),_v1=Point(0.,eps),_v2=Point(eps,0.),_hsteps=h);
 Segment S1(_v1=Point(0.,1.),_v2=Point(0.,eps),_hsteps=h);
 Segment S2(_v1=Point(eps,0.),_v2=Point(1,0.),_hsteps=h);
 Geometry Cour=surfaceFrom(Ce+S1+Ci+S2,"Omega",true);
 Mesh M(Cour,_triangle,1,_gmsh);
 M.geometry_p=new Disk(_center=Point(0.,0.),_radius=1,_angle1=0, _angle2=90*deg_,_nnodes=10,_domain_name="cour");
 Domain omega=M.domain("Omega");
 omega.setGeometry(M.geometry_p);
 Space V(omega,inlk,"V"); Unknown u(V,"u");TestFunction v(u,"v");
 TermMatrix M(intg(omega,u*v,GaussLegendre,5),"M");            //standard computation
 TermMatrix Mi(intg(omega,u*v,isoGeo,GaussLegendre,5),"Mi");   //isogeometric computation
 TermVector un(u,omega,1.);
 Real l1=real(M*un|un), li=real(Mi*un|un), l=pi_/4;
 thePrintStream<<"quarter of disk, eps="<<eps<<eol;
 thePrintStream<<"exact surface = "<<l<<eol;
 thePrintStream<<"M*un|un  = "<<l1<<" error="<<abs(l1-l)<<eol;
 thePrintStream<<"Mi*un|un = "<<li<<" error="<<abs(li-l)<<eol;
 \end{lstlisting}
   \vspace{2mm}
 As the result shows, the computation error of order $10^{-8}$  is recovered as expected. Zooming to the origin, we can observe that the P1 mesh has two elongated elements that do not seem to pollute the computation!
 \small
 \begin{verbatim}
 quarter of disk, eps=0.000112838
 exact surface = 0.785398
 M*un|un  = 0.784137 error=0.00126105
 Mi*un|un = 0.785398 error=1e-08
 \end{verbatim}
 \normalsize
 \begin{center}
 \includePict[width=8cm, trim=100mm 0 100mm 0, clip]{isoDiskCrown.png}
 \includePict[width=8cm, trim=100mm 0 100mm 0, clip]{isoDiskCrownPar.png}\\
 left: isogeometric mesh and P1 mesh (dashed), right: related mesh in parameters space\\
 \includePict[width=8cm, trim=100mm 0 100mm 0, clip]{isoDiskCrownZoom.png}\\
 zooming the mesh to the origin
 \end{center}
This approach works also for the quasi-disk 
$$\Omega_\varepsilon=\left\{(r\cos\theta, r\sin\theta),\ \varepsilon<r<1,\ 0 < \theta < 2\pi\right\}$$
With the following code 
 \vspace{2mm}
\begin{lstlisting}[]
Disk De(_center=Point(0.,0.),_radius=1,_hsteps=h,_domain_name="Omega");
Disk Di(_center=Point(0.,0.),_radius=eps,_hsteps=h,_domain_name="Omega");
Geometry Dime=De-Di;
Mesh M(Dime,_triangle,1,_gmsh);
Domain omega=M.domain("Omega");
omega.setGeometry(&De);
Space V(omega,inlk,"VD");
Unknown u(V,"ud");TestFunction vd(u,"v");
TermMatrix M(intg(omega,u*v,GaussLegendre,5),"MD");
TermMatrix Mi(intg(omega,u*v,isoGeo,GaussLegendre,5),"MDi");
TermVector un(u,omega,1.);
thePrintStream<<"internal disk, eps="<<eps<<eol;
Real l1=real(D*un|un), li= real(Mi*un|un), l=pi_;
thePrintStream<<"exact surface = "<<l<<eol;
thePrintStream<<"  M*un|un = "<<l1<<" error="<<std::abs(l1-l)<<eol;
thePrintStream<<" Mi*un|un = "<<li<<" error="<<std::abs(li-l)<<eol;
\end{lstlisting}
\vspace{2mm}
we get
\small
\begin{verbatim}
internal disk, eps=0.000112838
exact surface = 3.14159
M*un|un = 3.12145 error=0.0201475
Mi*un|un = 3.14159 error=4e-08
\end{verbatim}
 \normalsize
The error remains the measure of the disk of radius $\varepsilon$, that is $4\,10^{-8}$. But unfortunately, the original P1 mesh produced by \gmsh is too refined near the origin and the profit of the isogeometric approach is no so interesting.
\begin{center}
    \includePict[width=8cm, trim=100mm 0 100mm 0, clip]{isoFullDisk.png}
    \includePict[width=8cm, trim=100mm 0 100mm 0, clip]{isoFullDiskPar.png}\\
    left: isogeometric mesh and P1 mesh (dashed), right: related mesh in parameters space\\
\end{center}

An other way to overcome the difficulty consists in using other mapping. For instance the elliptical grid mapping which maps the square $[-1,1]\times[-1,1]$ to the disk $D(0,R)$ with the following transforms
$$\textrm{(square to disk)  }
\left|\begin{array}{l}
\displaystyle x =R\, u\sqrt{1-\frac{v^2}{2}}\\[4mm]
\displaystyle y =R\, v\sqrt{1-\frac{u^2}{2}}
\end{array}\right.\quad
\textrm{(disk to square)  }
\left|\begin{array}{l}
\displaystyle u=\frac{1}{2}\sqrt{2+\frac{x^2-y^2}{R^2}+2\sqrt{2}\frac{x}{R}}-\frac{1}{2}\sqrt{2+\frac{x^2-y^2}{R^2}-2\sqrt{2}\frac{x}{R}}\\[4mm]
\displaystyle v=\frac{1}{2}\sqrt{2+\frac{y^2-x^2}{R^2}+2\sqrt{2}\frac{y}{R}}-\frac{1}{2}\sqrt{2+\frac{y^2-x^2}{R^2}-2\sqrt{2}\frac{y}{R}}
\end{array}\right.
$$
This parametrization induces "singularities" at disk points $(\pm\frac{\sqrt{2}}{2},\pm\frac{\sqrt{2}}{2})$ related to the corners of the square. More precisely, the jacobian becomes infinite at these points! By moving, slightly the boundary of the parametrization, say choosing $R+\varepsilon,$ instead $R$, the results are quite good. For instance, for the disk of radius $R=1$ and $\varepsilon=10^{-8}$ we get the following results:
\small
\begin{verbatim}
moving parameter eps=1.e-8
exact surface = 3.14159
M*un|un = 3.12145 error=0.00504416
Mi*un|un = 3.14159 error=1.74e-6
\end{verbatim}
\normalsize
It has been obtained with the following code
 \vspace{2mm}
\begin{lstlisting}[]
Vector<Real> ellPar(const Point& uv, Parameters& pars, DiffOpType d=_id)
{
Number dim=2;
Real R=1+eps;
Vector<Real> xy(dim);
Real u=2*uv[0]-1, v=2*uv[1]-1, u2=u*u, v2=v*v, ru=sqrt(1-0.5*u2), rv=sqrt(1-0.5*v2);
switch (d)
  {
  case _id : xy[0]=u*rv*R;xy[1]=v*ru*R;break;
  case _d1 : xy[0]=2*rv*R;xy[1]=-u*v*R/ru;break;
  case _d2 : xy[0]=-u*v*R/rv;xy[1]=2*ru*R;break;
  default  : parfun_error("Elliptical disk parametrization", d);
  }
  return xy;
}
Vector<Real> invEllPar(const Point& xy, Parameters& pars, DiffOpType d=_id)
{
Number dim=2;
Real R=1+eps, R2=R*R;
Vector<Real> uv(dim);
Real x=xy[0], y=xy[1], x2=x*x, y2=y*y, r=2*sqrt(2);
switch (d)
  {
  case _id:
    uv[0]=0.5*(1+0.5*sqrt(2+(x2-y2)/R2+r*x/R)-0.5*sqrt(2+(x2-y2)/R2-r*x/R));
    uv[1]=0.5*(1+0.5*sqrt(2+(y2-x2)/R2+r*y/R)-0.5*sqrt(2+(y2-x2)/R2-r*y/R));
    break;
  default: parfun_error("Inverse elliptical disk parametrization", d);
  }
  return uv;
}
...
Disk D(_center=Point(0.,0.),_radius=1,_hsteps=h,_domain_name="Omega");
Mesh M(D,_triangle,1,_gmsh);
Domain omega=M.domain("Omega");
Parametrization par(0,1,0,1,ellPar,invEllPar,pars);
omega.setParametrization(par);  // associate new parametrization
Space V(omega,inlk,"VD");
Unknown u(V,"u");TestFunction v(u,"v");
TermMatrix M(intg(omega,u*v,GaussLegendre,5),"M");
TermMatrix Mi(intg(omega,u*v,isoGeo,GaussLegendre,5),"Mi");
TermVector un(u,omega,1.);
Real l1=real(M*un|un), li= real(Mi*un|un), l=pi_;
thePrintStream<<"moving parameter eps="<<eps<<eol;
thePrintStream<<"exact surface = "<<l<<eol;
thePrintStream<<"  M*un|un = "<<l1<<" error="<<std::abs(l1-l)<<eol;
thePrintStream<<" Mi*un|un = "<<li<<" error="<<std::abs(li-l)<<eol;
\end{lstlisting}
\vspace{2mm}
\begin{center}
	\includePict[width=8cm, trim=100mm 0 100mm 0, clip]{isoDiskEll.png}
	\includePict[width=8cm, trim=100mm 0 100mm 0, clip]{isoDiskEllPar.png}\\
	left: isogeometric mesh and P1 mesh (dashed), right: related mesh in parameters space\\
\end{center}
Other maps can be used, see \cite{ch2019} for other choices.

\subsubsection*{2D BEM computation}

\begin{thebibliography}{9}
\bibitem{ch2019} Chamberlain Fong, \emph{Elliptification of Rectangular Imagery}, Joint Mathematics Meetings 2019, SIGMAA-ARTS, arXiv : 1709.07875v3, 2017
\end{thebibliography}



\end{document}