\documentclass[10pt,svgnames,english]{article}

\usepackage[mode=user,toplevel=chapter]{xlifepp}

\hypersetup{%
    pdftitle={Laplace 2d convergence versus mesh order},%
    pdfauthor={E. Lun\'eville},%
    pdfpagemode=UseOutlines,%
    linkbordercolor=0 0 0,%
    linkcolor=black,%
    citecolor=black,%
    colorlinks=true%
}%

%\date{} % date %
\title{Laplace 2d convergence versus mesh order} % titre %
\author{Eric {\scshape Lunéville}} % auteur %

\begin{document}

\maketitle

This document describes some convergence results for the 2D Laplace problem versus the mesh order and the FE Lagrange order. Several geometries have been considered (disk, ellipse and disk sector). As GMSH is able to produce meshes up to the order 5, the previous code computes the errors for mesh of order $1\le p\le 5$ and Lagrange FE of order $1\le k\le p+1$ and for mesh steps from $0.01$ up to $0.4$ uniformly distributed. Next figures present the $L^2$  and $C^0$ error curves collected by mesh order. In legends, \verb|q| is the slope of the curves (log-log) in a coherent region, say a numerical estimate of the order of the approximation.

\section*{Disk}
The following Laplace-Dirichlet problem is concerned:
\begin{equation}\label{laplace}
    \begin{dcases}
        -\Delta u=0 & \text{in } \Omega=\{|x|<1\}\\
        u=g & \text{on }\Gamma=\{|x|=1\}\
    \end{dcases}
\end{equation}
with $g(\theta)=sin(3\theta)$ leading to the unique solution $u(r,\theta)=r^3\sin(3\theta)$.\\

The following \xlifepp code computes the $L^2(\Omega)$ and $C^0(\Omega)$ errors versus the mesh order and the FE Lagrange order for several mesh steps:

\begin{lstlisting}
Number P=5, K=3;
Real hmin=0.02, hmax=0.42, dh=(hmax-hmin)/20;
std::ofstream erout;
for(Number p=1;p<=P; p++)
{
  erout.open("L2Error_mo="+tostring(p)+".dat");
  for(Real h=hmin; h<=hmax; h+=dh)
  {
     Mesh mesh2d(Disk(_center=Point(0.,0.),_radius=1.,_hsteps=h,_side_names={"r=1"}),
                      _shape=_triangle, _order=p, _generator=_gmsh);
     Domain omega=mesh2d.domain("Omega"), gamma=mesh2d.domain("r=1");
     for(Number k=1;k<=p+1; k++)
     {
       elapsedTime();
       Space V(_domain=omega,_FE_type=Lagrange,_order=k,_name="V");
       Unknown u(V,_name="u");  TestFunction v(u,_name="v");
       BilinearForm auv = intg(omega,grad(u)|grad(v));
       EssentialConditions ecs= (u|gamma=g);
       TermMatrix A(auv,ecs);
       TermVector B(v,omega,0.);
       TermVector U=directSolve(A,B);
       TermVector Uex(u,omega,uex);
       TermMatrix M(intg(omega,u*v));
       TermVector E=Uex-U;
       Real erl2=sqrt(real((M*E|E))), erc0=norminfty(E), t=elapsedTime();
       erout<<k<<" "<<h<<" "<<erl2<<" "<<erc0<<" "<<V.nbdofs()<<" "<<t<<eol;
     }
  }
  erout.close();
}
\end{lstlisting}
\vspace{4mm}
The following order of convergence of L2 error can be observed:
\begin{center}
    \begin{tabular}{|c|c|c|c|c|c|c|}
        \hline
        mesh/FE order & 1 & 2 & 3 & 4 & 5 & 6\\
        \hline
        \hline
        1 & $\sim$ 2.3 & $\sim$ 2 & & & & \\
        \hline 
        2 & $\sim$ 1.9 & $\sim$ 3.5 & $\sim$ 3.75 & & & \\
        \hline 
        3 & $\sim$ 1.9 & $\sim$ 2 & $\sim$ 3.4 & $\sim$ 4.1 & & \\
        \hline 
        4 & $\sim$ 1.9 & $\sim$ 2 & $\sim$ 3.4 & $\sim$ 4.3 & $\sim$ 4.9 & \\
        \hline 
        5 & $\sim$ 1.9 & $\sim$ 2 & $\sim$ 3.4 & $\sim$ 4.3 & $\sim$ 5 & $\sim$ 5\\
        \hline
    \end{tabular}
\end{center}

There is a super convergence effect for P2 approximation on mesh of order 2. For any mesh order, using the same order FE approximation (iso-parametric approximation) is the best compromise. Note that for the high FE order (6), a slight deterioration can be observed for the finest mesh (h=0.02), due to the accumulation of rounding effects. 
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Disk_2D_mesh_Order=1.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Disk_2D_mesh_Order=2.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Disk_2D_mesh_Order=3.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Disk_2D_mesh_Order=4.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Disk_2D_mesh_Order=5.png}
\end{center}


\section*{Ellipse}
Now consider the same problem but with $\Omega$ an ellipse of radius 1.4 and 0.7 (surface close to the disk surface). The following order of convergence of L2 error can be observed:
\begin{center}
    \begin{tabular}{|c|c|c|c|c|c|c|}
        \hline
        mesh/FE order & 1 & 2 & 3 & 4 & 5 & 6\\
        \hline
        \hline
        1 & $\sim$ 2.2 & $\sim$ 3.3 & & & & \\
        \hline 
        2 & $\sim$ 1.6 & $\sim$ 3.3 & $\sim$ 3.6 & & & \\
        \hline 
        3 & $\sim$ 1.6 & $\sim$ 2 & $\sim$ 3.4 & $\sim$ 4.3 & & \\
        \hline 
        4 & $\sim$ 1.9 & $\sim$ 2 & $\sim$ 3.5 & $\sim$ 4.2 & $\sim$ 5 & \\
        \hline 
        5 & $\sim$ 1.9 & $\sim$ 2 & $\sim$ 3.5 & $\sim$ 4.3 & $\sim$ 5.1 & $\sim$ 5.7\\
        \hline
    \end{tabular}
\end{center}
The convergence rates are similar to those of the disk case except for the P2 approximlation on mesh of order 1, that is better (3 instead of 2). Except of the last case, using a same order FE approximation (iso-parametric approximation) is the best compromise.
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Ellipse_2D_mesh_Order=1.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Ellipse_2D_mesh_Order=2.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Ellipse_2D_mesh_Order=3.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Ellipse_2D_mesh_Order=4.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Ellipse_2D_mesh_Order=5.png}
\end{center}
\section*{Quarter of disk}
The following Laplace problem is concerned:
\begin{equation}\label{laplace}
\begin{dcases}
-\Delta u=f & \text{in } \Omega\\
        u=0 & \text{on }\Gamma\\
        \frac{\partial u}{\partial_n}=g & \text{on }\Sigma
\end{dcases}
\end{equation}

where $\Omega= \{0\le r\le 1, 0\le \theta \le \frac{\pi}{2}\}$ is a quarter of disk, $\Sigma=\{ r=1, 0\le \theta \le \frac{\pi}{2}\}$ and $\Gamma=\{0\le r\le 1, \theta=0\}$. With the data $g(\theta)=3\sin(3\theta)$ the unique solution is $u(r,\theta)=r^3\sin(3\theta)$. We get the following convergence rate:

\begin{center}
    \begin{tabular}{|c|c|c|c|c|c|c|}
        \hline
        mesh/FE order & 1 & 2 & 3 & 4 & 5 & 6\\
        \hline
        \hline
        1 & $\sim$ 1.9 & $\sim$ 1.9 & & & & \\
        \hline 
        2 & $\sim$ 1.9 & $\sim$ 3.4 & $\sim$ 4.2 & & & \\
        \hline 
        3 & $\sim$ 1.9 & $\sim$ 2.2 & $\sim$ 3.3 & $\sim$ 4.2 & & \\
        \hline 
        4 & $\sim$ 1.9 & $\sim$ 2.2 & $\sim$ 3.3 & $\sim$ 4.4 & $\sim$ 4.4 & \\
        \hline 
        5 & $\sim$ 1.8 & $\sim$ 2.1 & $\sim$ 3.2 & $\sim$ 4.2 & $\sim$ 5.2 & $\sim$ 5.6\\
        \hline
    \end{tabular}
\end{center}


Convergence rates are rather similar to the previous ones except in some cases where it can be observed super convergence effects. The fact that the boundary has edges seems to have no significant effect. 


\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Sector_2D_mesh_Order=1.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Sector_2D_mesh_Order=2.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Sector_2D_mesh_Order=3.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Sector_2D_mesh_Order=4.png}
\end{center}
\begin{center}
    \includePict[width=15cm, trim={3cm 1cm 3cm 1cm}]{cv_Laplace_Sector_2D_mesh_Order=5.png}
\end{center}

\end{document}

