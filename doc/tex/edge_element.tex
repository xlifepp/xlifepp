% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%
 
\documentclass[10pt,svgnames,english]{article}

\usepackage[mode=user,toplevel=chapter]{xlifepp}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{verbatim} 
\hypersetup{%
	pdftitle={Div-Curl element documentation},%
	pdfauthor={E. Lun\'eville},%
	pdfpagemode=UseOutlines,%
	linkbordercolor=0 0 0,%
	linkcolor=black,%
	citecolor=black,%
	colorlinks=true%
}%

%\date{}
\title{Div and curl conforming element}
\author{Eric {\sc Lunéville}}

\begin{document}

\parindent=0pt

\maketitle
\tableofcontents
\newpage
We present here a synthesis of finite elements that can be used to interpolate $H(div)$ and $H(curl)$ spaces. There are provided using the de Rham diagram either in continuous case or discrete case:
\[
\begin{array}{ccccccc}
H^1(\Omega) & \xrightarrow{\nabla} & H(\text{curl};\Omega ) & \xrightarrow{\nabla \times} &  H(\text{div};\Omega ) & \xrightarrow{\nabla .} & L^2(\Omega)\\
\pi_h\downarrow& & r_h\downarrow & & w_h\downarrow & & p_h\downarrow\\
V_h & \xrightarrow{\nabla} & R_h & \xrightarrow{\nabla \times} & D_h & \xrightarrow{\nabla .} & L_h
\end{array}
\]
where $\pi_h$, $ r_h$, $w_h$ and $p_h$ are interpolation operators. This diagram implies that derivative operators and projectors commute, for instance :
$$\nabla \pi_h v=r_h\nabla v.$$ 
A complete explanation based on exterior calculus may be found in (\cite{AFW2006}). The material exposed here mainly comes from the book of P. Monk (\cite{Monk2003}). We give only results useful for implementation.

\section*{Notation}
In the following, we denotes 
\begin{itemize}
\setlength{\itemsep}{0pt}
\item $x_1$, $x_2$, $x_3$ the cartesian coordinates
\item $\hat{S}$ the reference segment, say $[0,1]$
\item $\hat{T}$ the reference triangle, say $\left\{(x_1,x_2)\in [0,1]^2, x_1+x_2\leq 1\right\}$
\item $\hat{K}$ the reference tetrahedron, say $\left\{(x_1,x_2,x_3)\in [0,1]^3, x_1+x_2+x_3\leq 1\right\}$
\item $\hat{Q}$ the reference quadrangle, say $\left\{(x_1,x_2)\in [0,1]^2\right\}$
\item $\hat{H}$ the reference hexahedron, say $\left\{(x_1,x_2,x_3)\in [0,1]^3\right\}$
\item $\hat{f}$ any face of a volumic element
\item $\hat{e}$ any edge of a volumic or surfacic element
\item $\hat{v}$ any vertex of an element
\end{itemize}
We introduce the multi-index $\alpha=(\alpha_1,\alpha_2, ...\alpha_n)$ with $\alpha_i\geq 0$ and $|\alpha|= \alpha_1+\alpha_2+ ... +\alpha_n$. Polynoms are expressed in terms of $x^\alpha$ (say  $x_1^{\alpha_1}x_2^{\alpha_2}... x_n^{\alpha_n}$) :
 \renewcommand{\arraystretch}{1.5} 
$$
\begin{array}{|c|c|c|}
\hline
\text{space} & \text{dimension}& \text{comment} \\
\hline 
 & & \\[-6mm]
\displaystyle P_k=\left\{\sum_{|a|\leq k}a_\alpha x^\alpha \right\} & \displaystyle C^{n+k}_k=\frac{(n+k)!}{k!\,n!}& \text{standard space}\\[6mm]
\displaystyle\tilde{P}_k=\left\{\sum_{|\alpha|= k}a_\alpha x^\alpha \right\} &
\begin{array}{l}
\displaystyle n=2\ :\ k+1\\
\displaystyle n=3\ :\ \frac{1}{2}(k+2)(k+1)
\end{array}
& \text{homogenous of degree }k\\[6mm]
\displaystyle S_k=\left\{p\in(\tilde{P}_k)^n;\ x.p=0 \right\} &\begin{array}{l}
\displaystyle n=2\ :\ k\\
\displaystyle n=3\ :\ k(k+2)
\end{array}  & \text{involved in Hdiv and Hcurl}\\
\displaystyle\tilde{Q}_{lmn}=\left\{\sum_{\alpha_1\leq l,\alpha_2\leq m, \alpha_3\leq n }a_\alpha x^\alpha \right\} &\displaystyle (l+1)(m+1)(n+1)&\\

\hline
\end{array}
$$
\section{Div conforming element}
\subsection{On triangle - RTF\_k/NF1\_k and BDM\_k/NF2\_k}
\subsubsection*{Raviart-Thomas element (RTF\_k)}
This element is also known as Rao–Wilton–Glisson element. It is defined by ($\hat{T},\hat{D}_k, \hat{\Sigma}_k$), $\forall k\geq 1$:
$$\hat{D}_k=P_{k-1}^2 \oplus \tilde{P}_{k-1}\hat{x}\ \ \ \dim \hat{D}_k=k(k+2)$$
The dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over edge } \hat{e} & \displaystyle u \rightarrow \int_{\hat{e}} u.\hat{n}\, q\ \ \ q\in P_{k-1}(\hat{e})& k \text{ dofs by edge}\\[3mm]
\text{over triangle }\hat{T},\ k>1 & \displaystyle  u \rightarrow \int_{\hat{T}} u.q\ \ \ q\in (P_{k-2}(\hat{T}))^2& k(k-1) \text{ dofs}
\end{array}
$$
\subsubsection*{RTF\_1}
RTF\_1 has 3 dofs given by the normal moment on the edge $\int_{\hat{e}} u.\hat{n}$ (taking $q=1$ on $\hat{e} )$. Besides, considering the quadrature formula using middle point $\hat{m}$ of the edge $\hat{e}$ that integrates exactly polynoms of degree 1, edge dofs can be chosen as $u\rightarrow u(\hat{m}).\hat{n}$.
\begin{center}
\includePict[width=5cm]{RT1.png} 
\end{center}
\subsubsection*{RTF\_2}
RTF\_2 has 2 dofs by edge $\int_{\hat{e}} u.\hat{n}\ q_1$,  $\int_{\hat{e}} u.\hat{n}\ q_2$ with $q_1,q_2$ any basis of $P_1(\hat{e})$. Choosing the two points Gauss-Legendre quadrature that exactly integrates polynoms of degree 3 over an edge ($\hat{g}_1,\hat{g}_2$ the quadrature points on $\hat{e}$) , and first degree polynoms $q_1$ and $q_2$ such that $q_1(\hat{g}_1)=1,\ q_1(\hat{g}_2)=0$ and   $q_2(\hat{g}_1)=0,\ q_2(\hat{g}_2) =1$, the dofs on edge $\hat{e}$ have the expression $u\rightarrow u(\hat{g}_1).\hat{n}$, $u\rightarrow u(\hat{g}_2).\hat{n}$.\\
Besides it has two dofs related to the triangle, for instance $\int_{\hat{T}} u_1$,  $\int_{\hat{T}} u_2$.
\begin{center}
\includePict[width=5cm]{RT2.png} 
\end{center}

\subsubsection*{Brezzi-Douglas-Marini element (BDM\_k/NF2\_k)}
The BDM element is also a div conforming which is constructed from standard $(P_k)^2$ polynoms space. For this reason, a better accuracy might be hoped. It is defined by ($\hat{T},(\hat{P}_k)^2, \hat{\Sigma}_k$), $\forall k\geq 1$:\
$$\dim (\hat{P}_k)^2=(k+1)(k+2)$$
The dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over edge } \hat{e} & \displaystyle u \rightarrow \int_{\hat{e}} u.\hat{n}\, q\ \ \ q\in P_{k}(\hat{e})& (k+1) \text{ dofs by edge}\\[3mm]
\text{over triangle }\hat{T},\ k>1 & \displaystyle  u \rightarrow \int_{\hat{T}} u.q\ \ \ q\in (R_{k-1}(\hat{T}))^2& k^2-1 \textrm{ dofs}
\end{array}
$$
where 
$$R_{k-1}=(P_{k-2})^2\oplus S_{k-1}$$
 is the polynoms space involved in curl conforming Nedelec element.
 
 \subsubsection*{BDM\_1}
 BDM1 has 6 dofs given by 2 normal moments on each edge :
 $$\int_{\hat{e}} u.\hat{n}\, q_1\ \ \textrm{ and} \int_{\hat{e}} u.\hat{n}\, q_2$$
 where $q_1,q_2$ is a basis of $P_1(\hat{e})$.\\
  Again, by choosing Gauss-Legendre quadrature rule with 2 points on edge ($\hat{g}_1,\hat{g}_2$) that integrates exactly polynoms of degree 3, and choosing  
 $q1$ and $q2$ such that $q1(\hat{g}_1)=1,\ q1(\hat{g}_2)=0$ and   $q2(\hat{g}_1)=0,\ q2(\hat{g}_2) =1$, the dofs on edge $\hat{e}$ have the expression $u\rightarrow u(\hat{g}_1).\hat{n}$, $u\rightarrow u(\hat{g}_2).\hat{n}$.\\
 \begin{center}
 \includePict[width=5cm]{BDM1.png} 
 \end{center}
 \subsubsection*{BDM\_2}
 BDM\_2 has 3 dofs by edge 
 $$\int_{\hat{e}} u.n\ q_1,\   \int_{\hat{e}} u.\hat{n}\ q_2,\   \int_{\hat{e}} u.\hat{n}\ q_3$$
 with $q_1,q_2,q_3$ any basis of $P_2(\hat{e})$. \\
 
 Choosing the 3 points ($\hat{g}_1,\hat{g}_2,\ \hat{g}_3)$ Gauss-Legendre quadrature that exactly integrates polynoms of degree 5 over an edge, and second degree polynoms $q_1$, $q_2$, $q_3$ such that $q_i(\hat{g}_j)=\delta_{ij}$, the dofs on edge $\hat{e}$ have the expression :
 $$u\rightarrow u(\hat{g}_1).\hat{n},\ u\rightarrow u(\hat{g}_2).\hat{n},\ u\rightarrow u(\hat{g}_3).\hat{n}$$
 Besides it has 3 dofs related to the triangle
 $$\int_{\hat{T}} u.q_1,\   \int_{\hat{T}} u.q_2,\   \int_{\hat{T}} u.q_3$$
  with $q_1,q_2,q_3$ any basis of $R_1(\hat{T})$. As basis, one can choose : 
 $$
 q_1(x)=\left( \begin{array}{c}
1\\0
 \end{array}\right),\ \ 
  q_1(x)=\left( \begin{array}{c}
 0\\1
  \end{array}\right),\ \
   q_3(x)=\left( \begin{array}{c}
  x_1\\x_2
   \end{array}\right).
 $$
  \begin{center}
 \includePict[width=5cm]{BDM2.png} 
 \end{center}

\subsection{On tetrahedron : Nedelec element - NF1\_k and NF2\_k}
Nedelec elements (first and second family) are no more than extensions to 3D of RT and BDM elements. So the spaces anf dofs involved are similar.\\

\subsubsection*{ Nedelec first family - NF1\_k}
The NF1\_k element is defined by ($\hat{K},\hat{D}_k, \hat{\Sigma}_k$), $\forall k\geq 1$:
$$\hat{D}_k=P_{k-1}^3 \oplus \tilde{P}_{k-1}\hat{x}\ \ \ \dim \hat{D}_k=\frac{1}{2}(k+3)(k+1)k$$
and the dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over face } \hat{f} & \displaystyle u \rightarrow \int_{\hat{f}} u.\hat{n}\, q\ \ \ q\in P_{k-1}(\hat{f})& \frac{1}{2}(k+1)k \text{ dofs by face}\\[3mm]
\text{over tetrahedron }\hat{K},\ k>1 & \displaystyle  u \rightarrow \int_{\hat{K}} u.q\ \ \ q\in (P_{k-2}(\hat{K}))^3&  \frac{1}{2}(k+1)(k-1)k \text{ dofs}
\end{array}
$$
\subsubsection*{NF1\_1}
NF1\_1 has 4 dofs given by the normal moment on the 4 faces (taking $q=1$ on $\hat{f} )$:
 $$\int_{\hat{f}} u.\hat{n}$$
Considering the quadrature formula using centroid face point $\hat{m}$ of the face $\hat{m}$ that integrates exactly polynoms of degree 1, face dofs can be chosen as :
$$u\rightarrow u(\hat{m}).\hat{n}$$
\begin{center}
\includePict[width=5cm]{N11.png} 
\end{center}
\subsubsection*{NF1\_2}
NF1\_2 has 15 dofs : 3 normal components on each face $\hat{f}$ :
  $$\int_{\hat{f}} u.\hat{n}\ q_1,  \int_{\hat{f}} u.\hat{n}\ q_2, \int_{\hat{f}} u.\hat{n}\ q_3$$
with $q_1,q_2,q_3$ any basis of $P_1(\hat{f})$. \\
Choosing a 3 points quadrature\footnote{Hammer-Stroud or nodes of non comforming P1},  that exactly integrates polynoms of degree 2 over a triangle (face of tetrahedron) ($\hat{g}_1,\hat{g}_2, \hat{g}_3$ the quadrature points on $\hat{f}$) , and first degree polynoms $q_1$, $q_2$, $q_3$ such that $q_i(\hat{g}_j)=\delta_{ij}$, the dofs on facee $\hat{f}$ have nodal expressions:
$$u\rightarrow u(\hat{g}_i).\hat{n}, u\rightarrow u(\hat{g}_2).\hat{n},\ u\rightarrow u(\hat{g}_3).\hat{n}$$
Besides there are 3 dofs related to the tetrahedron, that can be chosen as the component dofs :
$$\int_{\hat{T}} u_1,  \int_{\hat{T}} u_2,\ \int_{\hat{T}} u_3.$$
\begin{center}
\includePict[width=5cm]{N12.png} 
\end{center}

\subsubsection*{ Nedelec second family - NF2\_k}
The NF2\_k element is also a div conforming which is constructed from standard $(P_k)^3$ polynoms space. For this reason, a better accuracy might be hoped. It is the extension to 3D of BDM element and is defined by ($\hat{K},(\hat{P}_k)^3, \hat{\Sigma}_k$), $\forall k\geq 1$:\
$$\dim (\hat{P}_k)^3=\frac{1}{2}(k+1)(k+2)(k+3)$$
and the dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over face } \hat{f} & \displaystyle u \rightarrow \int_{\hat{f}} u.\hat{n}\, q\ \ \ q\in P_{k}(\hat{f})& \frac{1}{2}(k+1)(k+2) \text{ dofs by face}\\[3mm]
\text{over tetrahedron }\hat{K},\ k>1 & \displaystyle  u \rightarrow \int_{\hat{T}} u.q\ \ \ q\in (R_{k-1}(\hat{T}))^2& \frac{1}{2}(k-1)(k+1)(k+2) \textrm{ dofs}
\end{array}
$$
where 
$$R_{k-1}=(P_{k-2})^3\oplus S_{k-1}$$
 is the polynoms space involved in curl conforming Nedelec element.
 
 \subsubsection*{NF2\_1}
 NF2\_1 has 12 dofs given by 3 normal moments on each face :
 $$\int_{\hat{f}} u.\hat{n}\, q_i\ \ i=1,3$$
 where $(q_i)_{i=1,3}$ is a basis of $P_1(\hat{f})$.\\
Choosing a 3 points quadrature that exactly integrates polynoms of degree 2 over a triangle (face of tetrahedron) ($\hat{g}_1,\hat{g}_2, \hat{g}_3$ the quadrature points on $\hat{f}$) , and first degree polynoms $q_i$ such that $q_i(\hat{g}_j)=\delta_{ij}$, the dofs on face $\hat{f}$ have nodal expressions:
$$u\rightarrow u(\hat{g}_i).\hat{n}\ \ i=1,3$$.\\
 \begin{center}
 \includePict[width=5cm]{N21.png} 
 \end{center}
 \subsubsection*{NF2\_2}
 NF2\_2 has 30 dofs :\\
 
 6 normal moment dofs on each face :\\
 $$\int_{\hat{f}} u.\hat{n}\, q_i\ \ i=1,6$$
 $(q_i)_{i=1,3}$ is a basis of $P_1(\hat{f})$\\
 
 and 6 moments dofs on tetrahedron
 $$\int_{\hat{K}} u.q_i,\ \ \ i=1,6$$
 $(q_i)_{i=1,6}$ is any basis of $R_1)$. 
 Again, moment dofs on face may be interpreted as nodal dof by applying a 6 points quadrature formula that exactly integrates polynoms of degree 3.
  \begin{center}
 \includePict[width=5cm]{N22.png} 
 \end{center}
\subsection{On quadrangle - RTCF\_k/NFC1\_k and BDMCF\_k/NFC2\_k}
Construction of div conforming element on quadrangle $\hat{Q}$ is rather similar to the construction on triangle. Again two families can be considered : one is an extension of RT element, the other an extension of BDM element.
\subsubsection*{First family RTCF\_k/NFC1\_k}
It is defined by ($\hat{Q},\hat{D}_k, \hat{\Sigma}_k$), $\forall k\geq 1$:
$$\hat{D}_k=Q_{k,k-1}\times Q_{k-1,k}\ \ \ \dim \hat{D}_k=2k(k+1)$$
and the dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over edge } \hat{e} & \displaystyle u \rightarrow \int_{\hat{e}} u.\hat{n}\, q\ \ \ q\in P_{k-1}(\hat{e})& k \text{ dofs by edge}\\[3mm]
\text{over quadrangle }\hat{Q},\ k>1 & \displaystyle  u \rightarrow \int_{\hat{Q}} u.q\ \ \ q\in Q_{k-2,k-1}\times Q_{k-1,k-2}& 2k(k-1) \text{ dofs}
\end{array}
$$
Using quadrature formula with $k$ points $g_i$ that integrates exactly polynoms of degree $2k-1$ on edge and a basis $q_i$ such that $q_i(g_j)=\delta_{ij}$ moment dofs on edge may be interpreted as nodal dofs.
\begin{center}
\includePict[width=12cm]{RTc1RTc2.png} 
\end{center}
\subsubsection*{Second family BDMCF\_k/NFC2\_k}
It is defined by ($\hat{Q},\hat{D}_k, \hat{\Sigma}_k$), $\forall k\geq 1$:
$$\hat{D}_k=(P_k)^2\oplus\textrm{span}\left\{\textbf{curl}\,\hat{x}_1^{k+1}\hat{x}_2,\textbf{curl}\,\hat{x}_1\,\hat{y}_2^{k+1}\right\}  \ \ \dim \hat{D}_k=(k+2)(k+1)+2$$
and the dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over edge } \hat{e} & \displaystyle u \rightarrow \int_{\hat{e}} u.\hat{n}\, q\ \ \ q\in P_{k}(\hat{e})& k+1 \text{ dofs by edge}\\[3mm]
\text{over quadrangle }\hat{Q},\ k>1 & \displaystyle  u \rightarrow \int_{\hat{Q}} u.q\ \ \ q\in (P_{k-2})^2& k(k-1) \text{ dofs}
\end{array}
$$
Using quadrature formula with $k$ points $g_i$ that integrates exactly polynoms of degree $2k-1$ on edge and a basis $q_i$ such that $q_i(g_j)=\delta_{ij}$ moment dofs on edge may be interpreted as nodal dofs.
\begin{center}
\includePict[width=12cm]{BDMc1BDMc2.png} 
\end{center}

\subsection{On hexahedron - NFC1\_k and AAF\_k/NFC2\_k}
Div conforming elements on hexahedron $\hat{H}$ is rather similar to div conforming elements on quadrangle. Again two families inherited from RT and BDM are available.
\subsubsection*{First family NFC1\_k}
Name NFC1\_k is provided by fem table from \url{http://femtable.org} website. It is defined by ($\hat{H},\hat{D}_k, \hat{\Sigma}_k$), $\forall k\geq 1$:
$$\hat{D}_k=Q_{k,k-1,k-1}\times Q_{k-1,k,k-1}\times Q_{k-1,k-1,k}\ \ \ \dim \hat{D}_k=3k^2(k+1)$$
and the dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over face } \hat{f} & \displaystyle u \rightarrow \int_{\hat{f}} u.\hat{n}\, q\ \ \ q\in Q_{k-1,k-1}(\hat{f})& k^2 \text{ dofs by face}\\[3mm]
\text{over hexahedron }\hat{H},\ k>1 & \displaystyle  u \rightarrow \int_{\hat{Q}} u.q\ \\
& \ q\in Q_{k-2,k-1,k-1}\times Q_{k-1,k-2,k-1}\times Q_{k-1,k-1,k-2}& 3k^2(k-1) \text{ dofs}
\end{array}
$$
Using quadrature formula with $k$ points $g_i$ that integrates exactly polynoms of degree $2k-1$ on edge and a basis $q_i$ such that $q_i(g_j)=\delta_{ij}$ moment dofs on edge may be interpreted as nodal dofs.
\begin{center}
\includePict[width=12cm]{Nc1Nc2.png} 
\end{center}


\subsubsection*{Second family AAF\_k/NFC2\_k}
This material comes from paper of Arnold and Awanu \cite{AA2014}. AAf element is defined by ($\hat{Q},\hat{D}_k, \hat{\Sigma}_k$), $\forall k\geq 1$:
$$
\begin{array}{l}
\hat{D}_k=(P_k)^3\oplus\textrm{span}\left\{
\textrm{curl}\left(\hat{x}_2\hat{x}_3(w_2-w_3),\,\hat{x}_3\hat{x}_1(w_3-w_1), \,\hat{x}_1\hat{x}_2(w_1-w_2)\right)
\right\}  \\
\hspace{10mm} \textrm{with } w_1\in P_k[\hat{x}_2,\hat{x}_3],\ w_2\in P_k[\hat{x}_1,\hat{x}_3], \ w_3\in P_k[\hat{x}_1,\hat{x}_2] \\
\dim \hat{D}_k=\frac{1}{2}(k+1)(k^2+5k+12)
 \end{array}$$
and the dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over face } \hat{f} & \displaystyle u \rightarrow \int_{\hat{f}} u.\hat{n}\, q\ \ \ q\in P_{k}(\hat{f})&\frac{1}{2}(k+2)(k+1) \text{ dofs by face}\\[3mm]
\text{over hexahedron }\hat{H},\ k>1 & \displaystyle  u \rightarrow \int_{\hat{Q}} u.q\ \ \ q\in (P_{k-2})^3& \frac{1}{2}k(k-1)(k+1) \text{ dofs}
\end{array}
$$
\begin{center}
\includePict[width=12cm]{AAf1AAf2.png} 
\end{center}

\section{Curl conforming element}
\subsection{On triangle : Nedelec elements - NE1\_k and BDME\_k/NE2\_k}
\subsubsection*{Nedelec first family NE1\_k}
Let us start with the first Nedelec family that it is defined by ($\hat{T},\hat{R}_k, \hat{\Sigma}_k$), $\forall k\geq 1$:
$$\hat{R}_k=P_{k-1}^2 \oplus S_{k}\ \ \ \dim \hat{R}_k=k(k+2)$$
and the dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over edge } \hat{e} & \displaystyle u \rightarrow \int_{\hat{e}} u.\hat{\tau}\, q\ \ \ q\in P_{k-1}(\hat{e})& k \text{ dofs by edge}\\[3mm]
\text{over triangle }\hat{T},\ k>1 & \displaystyle  u \rightarrow \int_{\hat{T}} u.q\ \ \ q\in (P_{k-2}(\hat{T}))^2& k(k-1) \text{ dofs}
\end{array}
$$
\begin{center}
\includePict[width=10cm]{NT1.png} 
\end{center}
Nedelec first family is dual to Raviart-Thomas element. The shape functions for $N1_1$ element are:
\footnotesize
$$ 
\left(\begin{array}{c} -y\\x\end{array}\right),\ 
\left(\begin{array}{c} -y\\1+x\end{array}\right),\ 
\left(\begin{array}{c} 1-y\\x\end{array}\right) 
$$
\normalsize
and for $N1_2$:
\footnotesize
$$
\left(\begin{array}{c} 4y-8y^2 \\ -2x+8xy\end{array}\right),\ 
\left(\begin{array}{c} 2y-8xy \\ -4x+8x^2\end{array}\right),\ 
\left(\begin{array}{c} -6y+8y^2+8xy \\ -4+6y+12x-8xy-8x^2\end{array}\right) 
\left(\begin{array}{c} 4y-8y^2\\ 2-6y-2x+8xy\end{array}\right)
$$
$$ 
\left(\begin{array}{c} -2+2y+6x-8x.y\\ -4x+8x^2\end{array}\right),\ 
\left(\begin{array}{c} 4-12y-6x+8y^2+8xy \\ 6x-8xy-8x^2\end{array}\right),\ 
\left(\begin{array}{c} 16y-16y^2-8xy \\ -8x+16xy+8x^2\end{array}\right),\ 
\left(\begin{array}{c} -8y+8y^2+16xy \\ 16x-8xy-16x^2\end{array}\right)
$$
\normalsize
\subsubsection*{Nedelec second family BDME\_k/NE2\_k}
The Nedelec second family is dual to the BDMF family. It is defined by  ($\hat{T},(\hat{P}_k)^2, \hat{\Sigma}_k$), $\forall k\geq 1$:\
$$\dim (\hat{P}_k)^2=(k+1)(k+2)$$
The dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over edge } \hat{e} & \displaystyle u \rightarrow \int_{\hat{e}} u.\hat{\tau}\, q\ \ \ q\in P_{k}(\hat{e})& (k+1) \text{ dofs by edge}\\[3mm]
\text{over triangle }\hat{T},\ k>1 & \displaystyle  u \rightarrow \int_{\hat{T}} u.q\ \ \ q\in D_{k-1}(\hat{T})& k^2-1 \textrm{ dofs}
\end{array}
$$
where $D_{k-1}(\hat{T})=P_{k-2}^2 \oplus \tilde{P}_{k-2}.\hat{x}$.

\begin{center}
\includePict[width=10cm]{NT2.png} 
\end{center}

\subsection{On tetrahedron : Nedelec elements - NE1\_k and NE2\_k}
\subsubsection*{Nedelec first family NE1\_k}
Let us start with the first Nedelec family that it is defined by ($\hat{K},\hat{R}_k, \hat{\Sigma}_k$), $\forall k\geq 1$:
$$\hat{R}_k=P_{k-1}^3 \oplus S_{k}\ \ \ \dim \hat{R}_k=\frac{1}{2}k(k+2)(k+3)$$
and the dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over edge } \hat{e} & \displaystyle u \rightarrow \int_{\hat{e}} u.\hat{\tau}\, q\ \ \ q\in P_{k-1}(\hat{e})& k \text{ dofs by edge}\\[3mm]
\text{over face } \hat{f},\ k>1 & \displaystyle u \rightarrow \int_{\hat{f}} u.q\ \ \ q\in \{P_{k-2}^3(\hat{f});\ q.n=0\}& k(k-1) \text{ dofs by face}\\[3mm]
\text{over tetrahedron }\hat{K},\ k>2 & \displaystyle  u \rightarrow \int_{\hat{K}} u.q\ \ \ q\in (P_{k-3}(\hat{K}))^3& \frac{1}{2}k(k-1)(k-2) \text{ dofs}
\end{array}
$$
\begin{center}
\includePict[width=10cm]{NE1t.png} 
\end{center}

\subsubsection*{Nedelec second family NE2\_k}
The second Nedelec family is defined by ($\hat{K},\hat{P}_k, \hat{\Sigma}_k$), $\forall k\geq 1$:
$$\hat{P}_k=P_{k}^3 \ \ \ \dim \hat{P}_k=\frac{1}{2}(k+1)(k+2)(k+3)$$
and the dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over edge } \hat{e} & \displaystyle u \rightarrow \int_{\hat{e}} u.\hat{\tau}\, q\ \ \ q\in P_{k}(\hat{e})& k+1 \text{ dofs by edge}\\[3mm]
\text{over face } \hat{f},\ k>1 & \displaystyle u \rightarrow \int_{\hat{f}} u.q\ \ \ q\in D_{k-1}(\hat{f})& (k-1)(k+1) \text{ dofs by face}\\[3mm]
\text{over tetrahedron }\hat{K},\ k>2 & \displaystyle  u \rightarrow \int_{\hat{K}} u.q\ \ \ q\in D_{k-2}(\hat{K})& \frac{1}{2}(k-1)(k+1)(k-2) \text{ dofs}
\end{array}
$$
where $D_{k-1}(\hat{f}) = P_{k-2}^2(\hat{f}) +  \tilde{P}_{k-2}*x$ and $D_{k-2}(\hat{K})=P_{k-3}^3(\hat{f}) +  \tilde{P}_{k-3}(\hat{f})*x$.
\begin{center}
\includePict[width=10cm]{NE2.png} 
\end{center}
\subsection{On quadrangle : RTC1\_k/NEC1\_k and BDMCE\_k/NEC2\_k}
\subsubsection*{First family RTC1\_k/NEC1\_k}
Name  RTC1\_k is provided by the fem table from \url{http://femtable.org} website. It is similar to the first family Nedelec edge element on quadrangle, so it is also named /NEC1\_k. It is defined by ($\hat{H},\hat{D}_k, \hat{\Sigma}_k$), $\forall k\geq 1$:
$$\hat{D}_k=Q_{k-1,k}\times Q_{k,k-1}\ \ \ \dim \hat{D}_k=2k(k+1)$$
and the dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over edge } \hat{e} & \displaystyle u \rightarrow \int_{\hat{e}} u.\hat{\tau}\, q\ \ \ q\in P_{k-1}(\hat{e})& k \text{ dofs by edge}\\[3mm]
\text{over quadrangle }\hat{Q},\ k\ge 2 & \displaystyle  u \rightarrow \int_{\hat{Q}} u.q\ \ q\in Q_{k-1,k-2}\times Q_{k-2,k-1}& 2k(k-1) \text{ dofs}
\end{array}
$$
Using quadrature formula with $k$ points $g_i$ that integrates exactly polynoms of degree $2k-1$ on edge and a basis $q_i$ such that $q_i(g_j)=\delta_{ij}$ moment dofs on edge may be interpreted as nodal dofs.
\begin{center}
	\includePict[width=12cm]{NEC1.png} 
\end{center}
\subsubsection*{Second family BDMCE\_k/NEC2\_k}

\subsection{On hexahedron : NEC1\_k and AAE\_k/NEC2\_k}
\subsubsection*{Nedelec first family NEC1\_k}
Name NEC1\_k is provided by fem table from \url{http://femtable.org} website. It is defined by ($\hat{H},\hat{D}_k, \hat{\Sigma}_k$), $\forall k\geq 1$:
$$\hat{D}_k=Q_{k-1,k,k}\times Q_{k,k-1,k}\times Q_{k,k,k-1}\ \ \ \dim \hat{D}_k=3k(k+1)^2$$
and the dofs $\hat{\Sigma}_k$ :
$$
\begin{array}{lll}
\text{over edge } \hat{e} & \displaystyle u \rightarrow \int_{\hat{e}} u.\hat{\tau}\, q\ \ \ q\in P_{k-1}(\hat{e})& k \text{ dofs by edge}\\[3mm]
\text{over face } \hat{f}, k\ge 2 & \displaystyle u \rightarrow \int_{\hat{f}} u\times\hat{n}.q\ \ \ q\in Q_{k-2,k-1}(\hat{f})\times Q_{k-1,k-2}(\hat{f}) & 2k(k-1) \text{ dofs by face}\\[3mm]
\text{over hexahedron }\hat{H},\ k\ge 2 & \displaystyle  u \rightarrow \int_{\hat{H}} u.q\ \\
& \ q\in Q_{k-1,k-2,k-2}\times Q_{k-2,k-1,k-2}\times Q_{k-2,k-2,k-1}& 3k(k-1)^2 \text{ dofs}
\end{array}
$$
Using quadrature formula with $k$ points $g_i$ that integrates exactly polynoms of degree $2k-1$ on edge and a basis $q_i$ such that $q_i(g_j)=\delta_{ij}$ moment dofs on edge may be interpreted as nodal dofs.
\begin{center}
	\includePict[width=12cm]{NCE.png} 
\end{center}
\subsubsection*{Second family AAE\_k/NEC2\_k}
\section{Nomenclature}
In the following table, the "name" column corresponds to the name given in the periodic FE table from \url{http://femtable.org}. N stands for Nedelec, RT for Raviart-Thomas, BDM for Brezzi-Douglas-Marini, AA for Arnold-Awanu. The letter F stands for a face element (say a $H(\text{div})$ conforming element) and the letter E for an edge element  (say a $H(\text{curl})$ conforming element). In order to standard the element names, \xlifepp uses the prefix N for any edge or face element, E or F to specify if it is an edge or a face element, a  C stands for an element defined on a quadrangle or a hexahedron and k stands for the element order.
 
\subsection{First families}
\footnotesize
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|}
	\hline 
	\rule[-1ex]{0pt}{2.5ex} \textit{shape} & \textit{type} & FE \textit{name} & \xlifepp \textit{names} & \textit{generic shortcuts}& \xlifepp \textit{file}\\ 
	\hline \hline
	\rule[-1ex]{0pt}{2.5ex} triangle & edge  & RTE\_k & RTE\_k, \textbf{NE1\_k}& N1\_k, N\_k &NedelecTriangle\\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} triangle & face &RTF\_k & RTF\_k, \textbf{NF1\_k}& RT1\_k, RT\_k &RaviartThomasTriangle\\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} quadrangle & edge & RTCE\_k &  RTCE\_k, \textbf{NEC1\_k}& N1\_k, N\_k&NedelecEdgeQuadrangle  \\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} quadrangle & face   &RTCF\_k &RTCF\_k, \textbf{NFC1\_k}&  RT1\_k, RT\_k&NedelecFaceQuadrangle\\ 
	\hline 
	\rule[-1ex]{0pt}{2.5ex} tetrahedron & edge   & NE1\_k &\textbf{NE1\_k}& N1\_k, N\_k &NedelecEdgeTetrahedron \\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} tetrahedron & face  &NF1\_k & \textbf{NF1\_k}& RT1\_k, RT\_k &NedelecFaceTetrahedron\\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} hexahedron & edge   & NCE\_k &  NCE\_k, \textbf{NEC1\_k}& N1\_k, N\_k&NedelecEdgeHexahedron  \\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} hexahedron & face  &NCF\_k &NCF\_k, \textbf{NFC1\_k}&  RT1\_k, RT\_k&NedelecFaceHexahedron\\ 
	\hline 
\end{tabular} 
\end{center}
\normalsize
\subsection{Second families}
\footnotesize
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|}
	\hline 
	\rule[-1ex]{0pt}{2.5ex} shape & type  & FE name & \xlifepp names & generic shortcuts\\ 
	\hline \hline
	\rule[-1ex]{0pt}{2.5ex} triangle & edge    & BDME\_k & BDME\_k, \textbf{NE2\_k}& N2\_k  \\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} triangle & face    &BDMF\_k & BDMF\_k, \textbf{NF2\_k}& RT2\_k \\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} quadrangle & edge    & BDMCE\_k &BDMCE\_k, \textbf{NEC2\_k}& N2\_k  \\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} quadrangle & face     &BDMCF\_k & BDMCF\_k, \textbf{NFC2\_k}& RT2\_k \\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} tetrahedron & edge    & NE2\_k &\textbf{NE2\_k}& N2\_k  \\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} tetrahedron & face    &NF2\_k &  \textbf{NF2\_k}& RT2\_k \\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} hexahedron & edge    & AAE\_k &AAE\_k, \textbf{NEC2\_k}& N2\_k  \\ 
	\hline
	\rule[-1ex]{0pt}{2.5ex} hexahedron & face    &AAF\_k & AAF\_k, \textbf{NFC2\_k}& RT2\_k \\ 
	\hline 
\end{tabular} 
\end{center}
\normalsize
Names in bold corresponds to the internal nomenclature of \xlifepp. Note that the internal nomemclature is the same in 2D and 3D. Even if it seems unfair, we choose to use "N" (Nedelec) as shortcut to edge elements and "RT" (Raviart-Thomas) as shortcut to face elements.

\section{Numerical process to compute shape functions}
For low order elements, there exists some simple expressions of shape functions. For high order elements, some may exist but generally not. We propose here a numerical method to get them at any order.

\subsection{Degree of freedom}
For div conforming elements, there exist always side dofs (face in 3D, edge in 2D) and element dofs :
$$
\begin{array}{lll}
\textrm{side dof} & \displaystyle l_f\ :\ u\rightarrow \int_f u.n\,p\,ds\ \ p\in \mathcal{Q}(s)& k\geq 1\\[3mm]
\textrm{element dof} & \displaystyle l_k\ :\ u\rightarrow \int_k u. p\,dk\ \ p\in \mathcal{P}(k) & k\geq 2\\

\end{array}
$$
while for curl conforming elements, there exist always edge dofs, face dofs in 3D, and element dofs:
$$
\begin{array}{lll}
\textrm{edge dof} &\displaystyle l_e\ :\  u\rightarrow \int_e u.t\,p\,de\ \ p\in \mathcal{R}(e)& k\geq 1\\[3mm]
\textrm{face dof} &\displaystyle l_f\ :\  u\rightarrow \int_f u.t\ p\,df\ \ p\in \mathcal{Q}(f) & k\geq 2\\[3mm]
\textrm{element dof} &\displaystyle l_k\ :\  u\rightarrow \int_k u.p\,dk\ \ p\in \mathcal{P}(k) & k\geq 3\\
\end{array}
$$

In order to compute shape functions, the first step consists in choosing a basis of polynoms space involved in degrees of freedom. Assume for the moment, that the basis is chosen, say $(p_i)_{i=1,m_p}$ where $m_p$ is the dimension of $\mathcal{P}$, $(q_i)_{i=1,m_q}$ where $m_q$ is the dimension of $\mathcal{Q}$ and $(r_i)_{i=1,m_r}$ where $m_r$ is the dimension of $\mathcal{R}$. Then, we have to deal with the explicit degrees of freedom :
$$(l_f(p_i))_{i=1,m_p},\ (l_e(r_i))_{i=1,m_r},\ (l_f(q_i))_{i=1,m_q}$$ 

\subsection{Shape functions}
Corresponding to degrees of freedom, we introduce the shape functions (some may not exist) belonging to the polynoms space of the element, say $\mathcal{S}$ :
$$
\begin{array}{ll}
(s^k_i)_{i=1,m_p}  & \textrm{such that } l_k(p_j)(s^k_i)=\delta_{ij}, \ l_e(r_j)(s^k_i)=0,\ l_f(q_j)(s^k_i)=0 \\
(s^f_i)_{i=1,m_q}  & \textrm{such that } l_f(q_j)(s^f_i)=\delta_{ij}, \ l_e(r_j)(s^f_i)=0,\ l_k(p_j)(s^f_i)=0 \\
(s^e_i)_{i=1,m_r}  & \textrm{such that } l_e(r_j)(s^e_i)=\delta_{ij}, \ l_f(q_j)(s^e_i)=0,\ l_k(p_j)(s^e_i)=0 
\end{array}
$$

\subsection{General process to compute shape functions}
Assume that a basis $(\tau_m)_{m=1,m_s}$ of the space $\mathcal{S}$ (dimension $m_s$) and all geometrical material (normal and tangential vector) are known. Thus , shape functions can be searched  as:
$$s^k_i = \sum_{m=1,m_s}a^k_{im} \tau_m.$$
The coefficients $A^k_i=(a^k_{im})_{m=1,m_s}$ are the solution of the linear system (because the unisolvance property):

$$
\begin{array}{llll}
\displaystyle \sum_{m=1,m_s}a^k_{im}l_k(p_j)(\tau_m)& =&\delta_{ij}&\forall j=1,m_p\\
\displaystyle \sum_{m=1,m_s}a^k_{im}l_f(q_j)(\tau_m)& =&0&\forall j=1,m_q\\
\displaystyle\sum_{m=1,m_s}a^k_{im}l_e(r_j)(\tau_m)& =&0&\forall j=1,m_r\\
\end{array}
$$
say 
$$\mathbb{L}_kA^k_i=D^k_i$$
where 

$$\mathbb{L}_k=
\left[
\begin{array}{c}
  \begin{array}{ccc}
   \vdots & \vdots &\vdots\\
   \vdots &  l_k(p_j)(\tau_m) &\vdots\\
   \vdots & \vdots &\vdots\\
  \end{array}\\
  \hline
   \begin{array}{ccc}
     \vdots & \vdots &\vdots\\
     \vdots &  l_f(q_j)(\tau_m) &\vdots\\
     \vdots & \vdots &\vdots\\
    \end{array}\\
    \hline
  \begin{array}{ccc}
       \vdots & \vdots &\vdots\\
       \vdots &  l_e(r_j)(\tau_m) &\vdots\\
       \vdots & \vdots &\vdots\\
      \end{array}\\
\end{array}
\right]
,\ \ 
D^k_i=
\left[
\begin{array}{c}
  \begin{array}{c}
   \vdots\\
   \delta_{ij}\\
   \vdots\\
  \end{array}\\
  \hline
   \begin{array}{ccc}
     \vdots\\
        0\\
        \vdots\\
    \end{array}\\
    \hline
  \begin{array}{ccc}
      \vdots\\
         0\\
         \vdots\\
      \end{array}\\
\end{array}
\right].
$$
As a consequence, such quantities have to be evaluated ($v$ means $n$ or $t$):

$$l_k(p_j)(\tau_m)= \int_k \tau_m.p_jdk,\ \ l_f(q_j)(\tau_m)= \int_f \tau_m.v\,p_jdf, \ \ 
l_e(r_j)(\tau_m)=\int_e \tau_m.v\,,p_jde.$$
To perform exact calculation, a quadrature formula integrated exactly polynoms $\tau_m\,p_j$ has to be used.\\

The process is simple, but organizing computation is quite challenging because of managment of different cases, different supports, \ldots The main point is to have a good description of space basis involved in the process. 


\newpage
\begin{thebibliography}{exemple}
\bibitem{AFW2006} D.N. Arnold, R.S. Falk, R. Winther, \textit{Finite element exterior calculus,
homological techniques, and applications}, Acta Numerica (2006), pp. 1–155. 

\bibitem{Monk2003} P. Monk, \textit{Finite Element Methods for Maxwell's Equations}, Clarendon Press, Oxford, 2003.

\bibitem{AA2014} D. N. Arnold and G. Awanou, \textit{Finite Differential Forms on Cubical Meshes}, Math. Comput., 83:1551-1570, 2014.


\end{thebibliography}


\end{document}
