\documentclass[10pt,svgnames,english]{article}

\usepackage[mode=user,toplevel=chapter]{xlifepp}

\hypersetup{%
    pdftitle={Discontinuous Galerkin documentation},%
    pdfauthor={E. Lun\'eville},%
    pdfpagemode=UseOutlines,%
    linkbordercolor=0 0 0,%
    linkcolor=black,%
    citecolor=black,%
    colorlinks=true%
}%
%\date{} % date %
\title{OpenCascade installation} % titre %
\author{Eric {\scshape Lunéville}} % auteur %
\begin{document}
\maketitle
OpenCascade (in short OCCT) is a free CAD library\footnote{© 2011-2022 OPEN CASCADE SAS} available on internet (\url{https://dev.opencascade.org/}). It is a multi-platform library written in C++. To deal with complex geometries, \xlifepp uses this library if OpenCascade option has been activated when installing \xlifepp. This document describes how to get and install OpenCascade to work with \xlifepp. When writting this documentation, the current version was Open CASCADE Technology 7.7.0.
\section{Get OpenCascade}
OpenCascade is available in the following forms:
\begin{itemize}
    \setlength{\itemsep}{-2pt}
    \item source package as a tgz archive at \url{https://dev.opencascade.org/release},
    \item Linux package at \url{https://repology.org/project/opencascade-occt/versions},
    \item homebrew (MacOS) distribution at \url{https://formulae.brew.sh/formula/opencascade},
    \item Windows binary setup, built with Visual Code at \url{https://dev.opencascade.org/release}.
    
\end{itemize}
\section{Installing from source package}
\subsection{Windows platform}
This installation requires the following tools installed :
\begin{itemize}
    \setlength{\itemsep}{-2pt}
    \item \textbf{C, C++ and Fortran compilers} with MinGW at \url{https://sourceforge.net/projects/mingw-w64/files/}, choose the x86\_64-posix-seh distribution
    \item \textbf{Codeblocks }, C++ development environment (\url{https://sourceforge.net/projects/codeblocks/files/Binaries/20.03/Windows/codeblocks-20.03mingw-setup.exe/download})
    \item \textbf{cmake} : \url{https://github.com/Kitware/CMake/releases/download/v3.26.3/cmake-3.26.3-windows-x86_64.msi}
\end{itemize}
The following describes the process to compile the OpenCascade library.
\begin{itemize}
    \item Download the \textbf{opencascade-7.7.0.tgz} file and decompress it (\textbf{opencascade-7.7.0} directory).
    \begin{center}
        \includePict[width=14cm]{OpenCascade_dir.png}
         \end{center}
     \item Create the sub-directory \textbf{3thrParty} in the main directory  
     \item Download the 3rd party components. The minimum requirements are FreeType 2.6.3 and Tcl/Tk 8.6 available at  \url{https://dev.opencascade.org/resources/download/3rd-party-components}. Decompress archives in the \textbf{3thrParty} directory. 
      \begin{center}
        \includePict[width=14cm]{OpenCascade_3thrParty.png}
    \end{center}
    \item run \textbf{cmake} (gui), specifying as source code location the main OCCT directory (\textbf{opencascade-7.7.0})  and as the build binary directory a new sub-directory (for instance \textbf{build\_gcc}) and click on \textbf{Configure} button.
    \begin{center}
        \includePict[width=14cm]{OpenCascade_cmake0.png}
    \end{center}
Choose \textbf{Codeblocks-MinGW Makefiles} as generator and choose \textbf{Specific native compilers}. In the next window, give the location of your c, c++ and fortran compilers.
\begin{center}
    \includePict[width=14cm]{OpenCascade_cmake00.png}
\end{center}
    Specify as \textbf{3RDPARTY} variable, the location of the third party directory (\textbf{E:/opencascade-7.7.0/3thrParty}) and click again on \textbf{Configure} button. When configuring is done, click on the \textbf{Generate} button. 
    \begin{center}
        \includePict[width=14cm]{OpenCascade_cmake.png}
    \end{center}
    If everything is ok, \textbf{cmake} tells\textit{ Project generating}. Close the \textbf{cmake} window.
    \item Run \textbf{CodeBlocks} with \underline{administrator privilege} and load the file\textbf{ OCCT.cbp} located in the build directory (\textbf{E:/opencascade-7.7.0/build\_gcc})
    \begin{center}
        \includePict[width=14cm]{OpenCascade_codeblocks.png}
    \end{center}
Select the target \textbf{install} and click on the \textbf{build} button (yellow wheel). It may take a long time to compile all the OCCT library. At the end of the build process a new directory \textbf{OCCT} is created in \textbf{C:\/ Program Files (x86)}. It contains all the header files of OCCT and all the libraries in \textbf{dll} and \textbf{dll.a} forms.
    \begin{center}
        \includePict[width=14cm]{OpenCascade_OCCT_dir.png}
    \end{center}
\item Copy all Open Cascade dlls (in \textbf{C:\textbackslash Program Files (x86)\textbackslash OCCT\textbackslash win64\textbackslash gcc\textbackslash}) to \textbf{C:\textbackslash Windows\textbackslash System32} directory
\begin{center}
    \includePict[width=14cm]{OpenCascade_OCCT_dll.png}
\end{center}
\item When building XLiFE++ with Open Cascade, the configuration file \textbf{OpenCASCADEConfig.cmake} located in  \textbf{E:/opencascade-7.7.0/build\_gcc} is not working. The lines 92-103 have to be commented and replace by one line: \small
\begin{verbatim}
# Import OpenCASCADE targets.
# foreach(_comp ${OpenCASCADE_FIND_COMPONENTS})
# if (NOT ";${OpenCASCADE_MODULES};" MATCHES "${_comp}")
# set(OpenCASCADE_FOUND False)
# set(OpenCASCADE_NOTFOUND_MESSAGE "Specified unsupported component: ${_comp}")
# if (NOT OpenCASCADE_FIND_QUIETLY)
# message (ERROR ": ${OpenCASCADE_NOTFOUND_MESSAGE}")
# endif()
# else()
# include("${CMAKE_CURRENT_LIST_DIR}/OpenCASCADE${_comp}Targets.cmake")
# endif()
# endforeach()

include("${CMAKE_CURRENT_LIST_DIR}/OpenCASCADETargets.cmake")
\end{verbatim}
\normalsize
\item To build \xlifepp with OpenCascade, run\textbf{ cmake} and create a new build (source : \xlifepp directory, build directory : \textbf{build\_gcc\_occt} for instance). Configure as usual and activate the \textbf{XLIFEPP\_ENABLE\_OPENCASCADE} option and fill in the \textbf{OpenCASCADE\_DIR} variable with the build directory related to OpenCascade (in the exemple : \textbf{E:/opencascade-7.7.0/build\_gcc}).
\begin{center}
    \includePict[width=16cm]{OpenCascade_cmake_xlife_occt.png}
\end{center}
Click on \textbf{Configure button} and then on \textbf{Generate button}.
\end{itemize}

\end{document}

