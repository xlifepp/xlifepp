\documentclass{article}

\usepackage[mode=user,toplevel=chapter]{xlifepp}

\hypersetup{%
    pdftitle={Geodesics documentation},%
    pdfauthor={E. Lun\'eville},%
    pdfpagemode=UseOutlines,%
    linkbordercolor=0 0 0,%
    linkcolor=black,%
    citecolor=black,%
    colorlinks=true%
}%

\date{} % date %
\title{Geodesics documentation} % titre %
\author{Eric {\scshape Lunéville}} % auteur %

\begin{document}
\maketitle

In 3D high-frequency wave propagation, asymptotic computation (UTD) requires the knowledge of geodesics, the ray of propagation on a surface. Regarding the geometry description, \xlifepp proposes several classes to deal with geodesics. Before presenting these classes, recall some basics about geodesics on a  3D-manifdold \(\mathcal{S}\) (immersed surface in \(\mathbb{R}^3\)).\\

\section{Geodesic}

Let \((u,v)\in \mathbb{R}^2\rightarrow \varphi(u,v)\in\mathbb{R}^3\) a parametrization of the surface  \(\mathcal{S}\). A \(C^0\) curve \(t\rightarrow \gamma(t)=\varphi(u(t),v(t))\), \(t\in[0,1]\) is a geodesic if it minimizes its length:

\[
L(\gamma)=\int_0^1\left\|\frac{d\gamma(t)}{dt}\right\|
\]

If more regularity on \(\gamma\) is assumed (say \(C^2\)), the variational calculus leads to the following characterization in terms of ODE, set in the \(uv\)-parameters space:
\begin{equation}\label{sysdif}
\left\{
\begin{aligned}
    u''(t)+\Gamma^1_{11}{(u')}^2+\Gamma^1_{12}u'v'+\Gamma^1_{22}{(v')}^2 & = 0 \\
    v''(t)+\Gamma^2_{11}{(u')}^2+\Gamma^2_{12}u'v'+\Gamma^2_{22}{(v')}^2 & = 0 \\ 
\end{aligned}
\right.
\end{equation}

where \(\Gamma^k_{ij}\) are the Christoffel symbols (components of second derivatives of \(\varphi\) in basis \((\partial_u \varphi,\partial_v \varphi)\)):
\[
\begin{aligned}
\Gamma^k_{ij} & = \frac{1}{2}\sum_m g^{-1}_{km}\left(\frac{\partial g_{mj}}{\partial x_i}+\frac{\partial g_{mi}}{\partial x_j}-\frac{\partial g_{ij}}{\partial x_m}\right)\\
g &= \left[\begin{array}{cc}
    |\partial_u\varphi|^2 & \partial_u \varphi . \partial_v\varphi \\
    \partial_u\varphi . \partial_v\varphi & |\partial_v\varphi|^2
\end{array}\right] \quad \text{(metric tensor)}.
\end{aligned}
\]

Be care with the fact that curves that satisfy the EDO does not always realize the shortest path between two given points, more precisely: 
\begin{itemize}
\item Through a point of a surface, there exists one and only one geodesic of given tangent;
\item By two points of the surface which are quite close, there exists a single geodesic passing through these points; this geodesic then corresponds to the path of minimal length joining these points.
\end{itemize}

In the following, only geodesics with a given starting point and a given starting derivative will be considered.

\subsubsection*{Geodesics on a plane \(\Pi\)}

The geodesic starting at the point \(x_0\in P_0+\Pi\) in the direction \(d_0\in\Pi\) is the half line \(x_0+td_0\), \(t\ge 0\).

\subsubsection*{Geodesics on a sphere \(\mathcal{S}(C,R)\)}

The geodesic starting at \(x_0\in \mathcal{S}\) (\(|x_0-C|=R\)) in the direction \(d_0\in T\mathcal{S}_{x_0}=\{d\in\mathbb{R}^3, d.(x_0-C)=0\}\) is the circle \(\mathcal{C}(O,R)\) in the plane \((C,x_0-C,d_0)\):

\[
x(\theta)=C+\cos \theta (x_0-C)+ \sin \theta \frac{d_0}{|d_0|}R,\quad\quad \theta \in [0, 2\pi[.
\]

\begin{center}
\includePict[width=5cm]{sphere_geodesic.png}
\end{center}

\subsubsection*{Geodesics on a circular cylinder}

Consider a general circular cylinder defined by \(C\) the center of the cylinder basis, \(T\) the center of the cylinder top and \(A\) a point of the basis cylinder.
Thus, the cylinder radius is \(R=|A-C|\), the cylinder height \(h=|T-C|\) and the normalized cylinder direction is given by \(\mathbf{d}=(T-C)/|T-C|\). Introducing the normalized vector \(\mathbf{r}=(A-C)/|A-C|\), the cylinder parametrization reads:
\[
\varphi(u,v)= C+R\cos u\,\mathbf{r} + R\sin u\,\mathbf{d}\times \mathbf{r}+h\,v\,\mathbf{d},\quad\quad (u,v)\in[0,2\pi]\times[0,1].\]

\begin{center}
\includePict[width=4cm]{cylinder_def.png}
\end{center}

The geodesic starting at \(x_0=C+y_0\) with direction \(d_0\) are given by 
\[
g(s)= C+R\cos(as+b)\mathbf{r}+R\sin(as+b)\mathbf{d}\times\mathbf{r}+h(ls+m)\mathbf{d}
\]
with \(\displaystyle m=y_0.\mathbf{d}\), \(\displaystyle l=d_0.\mathbf{d}\),
\(\displaystyle b=\text{atan2}\big(y_0.(d\times\mathbf{r}), y_0.\mathbf{r}\big)\) and

\[
a=\left\{
\begin{array}{ll}
   \displaystyle  -\frac{d_0.\mathbf{r}}{\sin b}& \text{if } \sin b\ne 0\\[4mm]
   \displaystyle \frac{d_0.(d\times\mathbf{r})}{\cos b}& \text{if } \cos b\ne 0
\end{array}
\right.
\]

Regarding the starting direction \(d_0\), the following situations occur:

\begin{itemize}
\item If \(d_0.\mathbf{d}=0\), thus \(l=0\) and the geodesic is a circle \(\mathcal{C}(C,R)\) in the plane parallel to the cylinder basic an containing the starting point \(x_0\);
\item If \(d_0.\mathbf{r}=0\), thus \(a=0\) and the geodesic is the half-line \(x=x_0+t\mathbf{d}\);
\item Else it is a circular helix with a step \(\displaystyle \frac{2\pi h l}{a}\).
\end{itemize}

\begin{center}
\includePict[width=4cm]{cylinder_geodesics.png}
\end{center}

\subsubsection*{Geodesics on a circular cone}

Consider the circular cone defined by its apex \(A\), the rotation axis unitary vector \(d\) and the cone angle \(\alpha \in ]0,\pi/2[\). One of its parametrization using polar coordinates (\(u=r,\ v=\theta\)) is:

\[
\varphi(u,v)=A+\mathcal{R}_\nu\left(u \cos v, u \sin v,p\,u \right)\quad\quad p=\frac{1}{\tan \alpha}
\]

with \(\mathcal{R}_\nu\) the rotation around the vector \(n=d\times z\) (\(z=(0,0,1)\)) and angle \(\nu=\text{acos}\,(d.z)\):

\[
\mathcal{R}_\nu w= \cos \nu\, w+(1-\cos\nu)\,(w.n)n+\sin\nu\, (n\times w).
\]

Because geodesics will be transformed using the same rotation and translation, in the following we omit them. The geodesics (see e.g. \url{https://conversationofmomentum.wordpress.com/2014/09/07/geodesics-on-a-cone/}) are given by:

\[
r(\theta)=\frac{a}{\cos((\theta-b)\sin\alpha)},\ z(\theta)=p\,r(\theta)\quad\text{or }r(\theta)=a, z=p\,a
\]

with \(a, b\) two constants to be determined. For instance, consider the starting point \(x_0=(r_0\cos \theta_0,r_0\sin \theta_0,p\,r_0)\) and the starting direction \(d_0=(\sin(\nu_0-\theta_0),\cos(\nu_0-\theta_0),p\,\sin\nu_0 )\). If \(\nu_0 \ne (2n+1)\pi/2\), \(n\in\mathbb{Z}\), looking for the geodesic \(\theta\rightarrow (r(\theta)\cos(\theta+\theta_0),r(\theta)\sin(\theta+\theta_0),p\,r(\theta))\) with \(\theta\ge 0\) and taking into account the starting conditions:

\[
a=r_0\cos(b\sin\alpha)\quad \quad r'(0)=-\frac{a\sin\alpha\sin (b\sin\alpha)}{\cos^2(b\sin\alpha)}=-r_0\sin\alpha\tan(b\sin\alpha)=r_0\tan\nu_0
\]

leading to \(\displaystyle b=-\frac{\text{atan}\,\left(\frac{\tan\nu_0}{\sin\alpha}\right)}{\sin\alpha}\).\\

When \(\nu_0 = (2n+1)\pi/2\),  \(n\in\mathbb{Z}\), the geodesic is a located on a generating line of the cone (meridian).
\begin{center}
\includePict[width=5cm]{cone_pisur10_geodesics.png}
\includePict[width=5cm]{cone_pisur10_geodesics2.png}\\
Geodesics on \(\pi/10\)-cone for various starting directions.
\end{center}

\section{Geodesic classes}

XLiFE++ handles several types of geodesics:
\begin{itemize}
\item Geodesic built from the surface parametrization (\class{ParametrizedGeodesic} class)
\item Geodesic built from the surface mesh (\class{MeshedGeodesic} class)
\item Geodesic described by an analytic function (\class{AnalyticGeodesic} class)
\item Geodesic described by a geometric object (\class{GeometricGeodesic} class)
\item Geodesic composed of some geodesics (\class{CompositeGeodesic} class)
\end{itemize}

All these classes inherit from the base class \class{Geodesic} collecting all common data, in particular a finite vector of geodesic points approximating the geodesic curve and additional vectors related to other quantities such as curvilinear abscissa, curvatures and derivatives:

\begin{lstlisting} 
class Geodesic
{protected :
  GeomDomain* dom_p;          //geometric domain where lives the geodesic
  Geometry* geo_p;            //geometry where lives the geodesic
  Parametrization* par_p;     //pointer to parametrization related to geo_p (if exists)
 public:
  GeodesicType type_;         //type of Geodesic (see enum)
  vector<Point> xs_;	      //list of geodesic points in 3D space
  vector<Point> dxs_;         //list of geodesic tangent vectors in 3D space
  vector<Point> normals_;     //list of normal surfaces at geodesics point in 3D space
  vector<real_t> curAbcs_;    //list of geodesic curvilinear abcissa in 3D space
  vector<Vector<real_t> > curvatures_;//list of curvatures (Gauss, mean and normal curvature)
  bool withCurAbc;            //true if curvilinear abcissa are computed
  bool withTangent;           //true if tangent and normal vectors are computed
  bool withCurvatures;        //true if curvatures have to be computed
  Point sx_;                  //starting point (allocated if computed)
  Point sdx_;                 //starting direction (allocated if computed)
  Point ex_;                  //ending point (allocated if computed)
  Point edx_;                 //ending direction (allocated if computed)
  real_t length_;             //length of geodesic if computed (else 0)
  MeshedGeodesic* meshgeod_;  //projection of geodesic on mesh (see buildMeshedGeodesic function)
  mutable Vector<complex_t> field_;      //to store a field on geodesic (only scalar complex up to now)   
  mutable vector<FieldType> fieldTypes_; //to store field types on geodesic (specific to HF computation)
  //function pointer to curvatures and surface normal computations (may be 0)
  Vector<real_t> (*fcur_p)(const Point& p, const Point& d, bool fromParameters, Parameters& pars); 
  Vector<real_t> (*fnor_p)(const Point& p, bool fromParameters, Parameters& pars);  
  Parameters params_;         //fcur_p anf fnor_p parameters storing additional data 
}
\end{lstlisting}

Up to now, the geodesic types and the field types are

\begin{lstlisting}
enum GeodesicType{_geodesic=0, _parametrizedGeodesic, _meshedGeodesic,_analyticGeodesic,
                  _geometricGeodesic, _compositeGeodesic};
enum FieldType{_noFieldType,_terminator,_lightOP,_lightFock,_shadowFock,_creeping,_focal};
\end{lstlisting}

The abstract \class{Geodesic} class mainly proposes the virtual function

\begin{lstlisting}[deletekeywords={[3]x,dx}]
virtual Geodesic& compute(Point& x, Point& dx, real_t& l, real_t lmax, number_t n, real_t dt=0);
\end{lstlisting}

"computing" the geodesic, that is filling the vector \var{xs\_} and others if required. \var{x} and \var{dx} are respectively the starting point and the starting direction, \var{lmax} is the maximal length of the geodesic, \verb?l? the effective geodesic length, \var{n} the number of points of the discrete geodesic and \var{dt} an optional computational step used by some type of geodesics.\\

It proposes a function doing the projection of the discrete geodesic onto the surface mesh of a geometric domain:

\begin{lstlisting}
void buildMeshedGeodesic(const GeomDomain& dom, bool trace=false);
\end{lstlisting}

It offers also some printing and saving facilities:

\begin{lstlisting}
void virtual print(ostream& os) const;                //user
void printData(ostream& os) const;                    //internal
void virtual saveToFile(const string_t& file)  const; //user
void virtual saveToFileG(const string_t& file) const; //internal
\end{lstlisting}
\vspace{1mm}

\subsection{ParametrizedGeodesic class}

The \class{ParametrizedGeodesic} class handles geodesics built from a surface parametrization. It requires either a geometric domain or a geometry equipped with a parametrization or an explicit parametrization. The computation of the geodesic is done by solving the differential system \eqref{sysdif} using a Runge-Kutta method of order 4 (RK4). As it works in the parameters space (say \((u,v)\)), the \class{ParametrizedGeodesic} class manages the vectors \var{us\_} and \var{dus\_} storing the parameters and parameter derivatives related to the geodesic 3D points (\var{xs\_}). This class proposes several constructors regarding the description of the surface: 

\begin{lstlisting}[deletekeywords={[3]x,dx}]
ParametrizedGeodesic(GeomDomain& dom, bool wCA=false, bool wT=false, bool wC=false);
ParametrizedGeodesic(Geometry& geom,  bool wCA=false, bool wT=false, bool wC=false);
ParametrizedGeodesic(const Parametrization& par, bool wCA=false, bool wT=false, bool wC=false);
ParametrizedGeodesic(Geometry& geom, const Point& x, const Point& dx, real_t lmax,
                     number_t nmax, real_t dt=0, bool wCA=false, bool wT=false, bool wC=false);
\end{lstlisting}

The last constructor also triggers the computation of the geodesic. The computation of the geodesic is based on the following functions

\begin{lstlisting}[deletekeywords={[3]x,dx}]
virtual Geodesic& compute(Point& x, Point& dx, real_t& l, real_t lmax, number_t n, real_t dt=0);  
Geodesic& compute(const Parametrization& par, Point& uv, Point& duv, real_t& l, real_t lmax, number_t& k, 
                  number_t n, real_t dt); 
Geodesic& computePiecewise(Point& x, Point& dx, real_t& l, real_t lmax, number_t n, real_t dt=0); 
\end{lstlisting}

The first \cmd{compute} function is the main computation function calling either the second \cmd{compute} function in the case of a regular parametrization (say \(C^1\)) or the \cmd{computePiecewise} function in the case of a piecewise parametrization. For a piecewise parametrization, the computation of geodesic is done calling the \cmd{compute} function using regular parametrization on each regular patch and when reaching the patch boundary, moving to the neighbor patch. When changing of patch, a new geodesic direction is computed according to the following rule:
\begin{equation}
 d_2=d_1-(d_1|n_1)(n_1+n_2)\label{dirgeod}
\end{equation}
where \(d_1\) and \(d_2\) are the old and new geodesic direction, \(n_1\) and \(n_2\) the normal vectors to the old and new patches at transition point.
\begin{center}
\includePict[width=4.5cm,trim={0 1cm 0 0},clip]{calcul_direction.png}
\end{center}
\subsection{MeshedGeodesic class}
The \class{MeshedGeodesic} class handles geodesics built from a surface mesh. The geodesic is constructed by traveling from elements to neighbor elements, leading to a discrete geodesic made of segments:
\begin{center}
\includePict[width=5cm,trim={0 1cm 0 0},clip]{direction_triangles.png}
\end{center}
The geodesic direction \(d_2\) in next triangle is deduced from the geodesic direction \(d_1\) in the previous triangle using again the formula (\ref{dirgeod}).\\

During the building process the list of crossed elements are stored in a vector:

\begin{lstlisting} 
class MeshedGeodesic : public Geodesic
{ public:
  vector<GeoNumPair> elts_; 
};
\end{lstlisting}

where \class{GeoNumPair} is a {\bf pair}<\class{GeomElement}*,\class{number\_t}>, the second item storing the outward side number.\\

This class proposes mainly a constructor from a \verb?GeomDomain? object embedding a surface mesh:

\begin{lstlisting}
MeshedGeodesic(GeomDomain& dp, bool wCA=false, bool wT=false, bool wC=false)
\end{lstlisting}

and the virtual function \verb?compute? building the geodesic

\begin{lstlisting} [deletekeywords={[3]x,dx}]
virtual Geodesic& compute(Point& x, Point& dx, real_t& l, real_t lmax, number_t n, real_t dt=0);
\end{lstlisting}

The \verb?Geodesic::buildMeshedGeodesic? function builds a \verb?MeshedGeodesic? object related to any kind of geodesic by projecting the geodesic points onto a surface mesh. Obviously, this function do nothing if the geodesic is already a \verb?MeshedGeodesic?!\\

\begin{warningbox}
Note that curvatures (Gauss, mean, normal) on meshed geodesic are available only if the \verb?GeomDomain? object describing the meshed surface has a related parametrization or if the function pointer \class{Geodesic}::\var{fcur\_p} is allocated!
\end{warningbox}

\begin{warningbox}
Up to now, \class{MeshedGeodesic} class supports only triangular mesh.
\end{warningbox}

\subsection{GeometricGeodesic class}

The \class{GeometricGeodesic} class allows to describe the geodesic using XLiFE++ geometry objects. It handles a pointer to a \class{Geometry} object:

\begin{lstlisting} 
class GeometricGeodesic : public Geodesic
{protected :
  Geometry * geom_p;  //1D geometry describing the geodesic
}
\end{lstlisting}

It offers several constructors depending on the given object: \class{Geometry} or \class{GeomDomain} and computation parameters:

\begin{lstlisting} 
GeometricGeodesic(GeomDomain& dom, Geometry& geom, const Point& x, const Point& dx, real_t lmax,   
                  bool wCA=false, bool wT=false, bool wC=false); 
GeometricGeodesic(GeomDomain& dom, const Point& x, const Point& dx, real_t lmax,
                  bool wCA=false, bool wT=false, bool wC=false); 
GeometricGeodesic(Geometry& geom, const Point& x, const Point& dx, real_t lmax,
                  bool wCA=false, bool wT=false, bool wC=false); 
GeometricGeodesic(Geometry& geom, const Point& x, const Point& dx, real_t lmax, number_t nmax, real_t dl=0, 
                  bool wCA=false, bool wT=false, bool wC=false);
\end{lstlisting}

When a simple surface is given using either a \class{GeomDomain} or a \class{Geometry}, constructors can build the geodesic as a \class{GeometricGeodesic} using the function \class{GometricGeodesic}::\cmd{build}

\begin{lstlisting} 
void build(real_t lmax); //effective construction regarding surface shape
\end{lstlisting}

\begin{warningbox}
Up to now only plane geometries (\class{Ellipse}, \class{Polygon} and theirs inherited geometries) are handled, giving segments as geodesics.
\end{warningbox}

All the constructors build the discrete geodesic, calling the virtual \cmd{compute} function.\\

\subsection{AnalyticalGeodesic class}

A geodesic may be also described by an analytical function. This is the purpose of the \class{AnalyticalGeodesic} class that manages a \class{Parametrization} object defined from a 1d parametrization (a curve):

\begin{lstlisting}
class AnalyticGeodesic : public Geodesic
{protected :
   Parametrization parg_; 
};
\end{lstlisting}

Several constructors are proposed:

\begin{lstlisting} 
AnalyticGeodesic(GeomDomain& dom, const Parametrization& par, bool wCA=false, bool wT=false, bool wC=false);
AnalyticGeodesic(const Parametrization& par, bool wCA=false, bool wT=false, bool wC=false);
AnalyticGeodesic(const GeomDomain& dom,bool wCA=false, bool wT=false, bool wC=false);
AnalyticGeodesic(const Geometry& geom,bool wCA=false, bool wT=false, bool wC=false);
AnalyticGeodesic(const Geometry& geom, const Point& x, const Point& dx, real_t lmax,
                 number_t nbp, real_t dt=0, bool wCA=false, bool wT=false, bool wC=false);
void build(const Geometry& geo,bool wCA=false, bool wT=false, bool wC=false);
virtual Geodesic& compute(Point& x, Point& dx, real_t& l, real_t lmax, number_t n, real_t dt=0);
\end{lstlisting}

The two first constructors give explicitly the parametrization of the geodesic while the other ones build it from the surface given either from a   \class{Geometry} object or  \class{GeomDomain} object (\class{AnalyticGeodesic}::\cmd{build} function). Up to now, only\class{TrunkSidePart} objects  are handled. The last one triggers the computation of the discrete geodesic calling the \cmd{compute} virtual function. 
\subsection{CompositeGeodesic}
When a geometry is a union of canonical geometries (say a composite geometry), a geodesic (either parametrized or analytical or geometrical) may be built on each canonical geometry. Matching geodesics (starting position to ending position and starting direction to ending direction) on interfaces of canonical geodesics allows to build a global geodesic. Each canonical geometry must have a parametrization! This is the purpose of the \class{CompositeGeodesic} class that manages the list of geodesics on canonical geometries:

\begin{lstlisting}
class CompositeGeodesic : public Geodesic
{protected :
   list<Geodesic*> geodesics_;   //list of pointers to Geodesic
};
\end{lstlisting}

There are mainly two constructors from \class{Geometry} object or \class{GeomDomain} object:

\begin{lstlisting}
CompositeGeodesic(GeomDomain& dom,bool wCA=false, bool wT=false, bool wC=false);  
CompositeGeodesic(Geometry& geo,bool wCA=false, bool wT=false, bool wC=false);    
\end{lstlisting}

The \class{CompositeGeodesic}::\cmd{compute} function, from a starting position and a starting direction locates the first canonical geometry and build the associated geodesic using either a \class{GeometricGeodesic} or  a \class{AnalyticalGeodesic} or a \class{ParametrizedGeodesic} in this priority order. The geodesic is built in order to reach a boundary of the geometry at an ending point with an ending direction. A new geometry, having this ending point at its boundary and such that the ending direction be incoming is searched. Then a new geodesic is built from the ending point and the ending direction. The process is repeated until the total length of geodesics is less than the given length \var{lmax}. At the end of the process, several geodesics have been constructed and "stored" in the  \var{geodesics\_} list. To build the global geodesic, that is to fill the vector \var{xs\_}? and may be other vectors, users have to call the member function:

\begin{lstlisting}
void toGlobal();
\end{lstlisting}

\subsection{Examples}

To illustrate the use of geodesic classes, the case of a cylinder with a top and a bottom is considered. This is a complex case because XLiFE++ constructs such a geometry as a composite geometry composed of 12 patches (4 for the lateral surface of the cylinder, 4 for the bottom and 4 for the top).

\begin{center}
\includePict[width=3cm]{cylinder_patches.png}
\end{center}
As a consequence, the parametrization is a piecewise parametrization.\\

In XLiFE++, the cylinder with unit disk as basis and height = 2 is constructed as following:

\begin{lstlisting}
Cylinder cyl(_basis=Disk(_center=Point(0.,0.,0.),_radius=1.,_nnodes=40),_direction=Point(0.,0.,2.),
             _nnodes=Numbers(40,40,40),_domain_name="Cylinder");
Mesh mc(cyl,_triangle);
Domain dc=mc.domain("Cylinder");
\end{lstlisting}

An associated mesh and geometric domain are also defined.\\

In order to get the geodesic starting at point \(x=(1,0,0.1)\) in direction \(dx=(0,1,0.2)\) with length 12, the following commands are used:

\begin{lstlisting}[deletekeywords={[3]x,dx}]
Point x(1,0,0.1), dx(0,1,0.2); Real l=0.;

ParametrizedGeodesic pgcyl(dcyl,true,true);
pgcyl.compute(x,dx,l, 12 ,2000, 0.1);

MeshedGeodesic mgcyl(dcyl,true,true);
mgcyl.compute(x,dx,l, 12 ,2000, 0.1);

CompositeGeodesic cgcyl(dcyl.geometry()->boundary());
cgcyl.compute(x,dx,l, 12 ,2000, 0.02);
cgcyl.toGlobal();
\end{lstlisting}

The next figure shows the geodesic get by the different methods.

\begin{center}
\includePict[width=16cm]{cylinder_geodesic_different_methods.png}
\end{center}

The computed geodesics are very similar. The next figure shows on the same plot the geodesic got using a \class{ParametrizedGeodesic} object (red curve) and the geodesic got using a \class{MeshedGeodesic} object (blue curve).

\begin{center}
\includePict[width=15cm]{cylinder_geodesic_different_methods2.png}
\end{center}

A difference between \class{ParametrizedGeodesic} and \class{MeshedGeodesic} computations may be observed but both the curves are rather close. To be more precise, the error between \class{MeshedGeodesic} and \class{ParametrizedGeodesic} has been quantified. On the next figure the \(l^2\) error (average for several starting angles) is plotted versus the mesh step in log-log axis.

\begin{center}
\includePict[width=14cm]{meshedGeodesic_error.png}
\end{center}

For large starting angle, the geodesics are only \(C^0\) because they cross the non \(C^1\) interfaces between the cylinder boundary and the bottom/top while for small starting angles they stay in the cylinder boundary and are more regular (say \(C^1\)). As a consequence, when the mesh step goes to 0, for small starting angles \class{MeshedGeodesic} approaches \class{ParametrizedGeodesic} with a better precision (order 2) than the case of large starting angles (order 1).\\

The last figure shows a set of geodesics starting from points located to a terminator (shadow-light interface) with the same starting angle and same length (2). It appears some complex geodesic interferences, in particular a focus point and some caustics curves due to the non \(C^1\) interfaces.

\begin{center}
\includePict[width=11cm]{cylinder_parametrized_geodesics_l=2.png}
\end{center}

\newpage

\section{Annexes}

\subsection*{Matlab code plotting cone geodesics}

\lstinputlisting[language=Matlab, xleftmargin=0.5cm, frame=l, escapechar=@] {inputs/pictures/cone.m}

%\begin{thebibliography}{9}
%\bibitem{bouche91}Bouche D.,Molinet F., Mittra R., \emph{Asymptotic Methods in Electromagnetics}, Springer-Verlag (1991)

%\end{thebibliography}

\end{document}
