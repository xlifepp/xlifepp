%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

\ProvidesPackage{xlifepp}

\usepackage{ifthen}
\usepackage{pgfopts}
\usepackage{iftex}

% package option management
\pgfkeys{/xlifepp/.cd, mode/.initial=dev, toplevel/.initial=chapter, defaultautorefname/.initial=auto}

\ProcessPgfOptions{/xlifepp}%

\pgfkeys{/xlifepp/.cd, mode/.get=\xlifeppMode, toplevel/.get=\xlifeppTopLevel, defaultautorefname/.get=\xlifeppDefaultAutoRefName}

\ifPDFTeX
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
\else
  \usepackage{fontspec}
\fi
\ifthenelse{\equal{\xlifeppMode}{webuser}\OR\equal{\xlifeppMode}{webdev}}{}{%
  \ifPDFTeX
    \usepackage{amssymb}
    \usepackage{fourier}
    \usepackage{mathtools}
  \else
    \usepackage{mathtools}
    \usepackage{fourier-otf}
  \fi
}%
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{graphbox} % not compatible with tex4ht and/or tcolorbox, but unnecessary
\usepackage{subcaption}
\usepackage{xcolor}
\usepackage{fancyvrb}
\usepackage{fvextra}
\usepackage{enumitem}
\usepackage{tikz}
\usepackage{xspace}
\usepackage{textcomp}
\usepackage{float}
\usepackage{multirow}
\usepackage{tcolorbox}
\usepackage{lettrine}
\usepackage{manfnt}
\usepackage{framed}
\usepackage{babel}
\usepackage{microtype}
\usepackage{listings}
\usepackage[plainpages=false,pdfpagelabels,bookmarksnumbered,breaklinks=true]{hyperref}
%\usepackage{cleveref} % not compatible with tex4ht, 

\newcommand{\includePict}[2][]{%
  \includegraphics[#1]{inputs/pictures/#2}%
}%

\newcommand{\xautoref}[1]{%
\ifthenelse{\equal{\xlifeppDefaultAutoRefName}{auto}}{%
  \autoref{#1}%
}{%
  user documentation of {\scshape XLiFE++}%
}%
}%

\ifthenelse{\equal{\xlifeppTopLevel}{chapter}}{%
  \makeatletter
  \newcommand{\xtitleOne}[1]{%
    \@ifstar{\chapter*{#1}}{\chapter{#1}}%
  }%
  \newcommand{\xtitleTwo}[1]{%
    \@ifstar{\section*{#1}}{\section{#1}}%
  }%
  \newcommand{\xtitleThree}[1]{%
    \@ifstar{\subsection*{#1}}{\subsection{#1}}%
  }%
  \newcommand{\xtitleFour}[1]{%
    \@ifstar{\subsubsection*{#1}}{\subsubsection{#1}}%
  }%
  \newcommand{\xtitleFive}[1]{%
    \@ifstar{\paragraph*{#1}}{\paragraph{#1}}%
  }%
  \newcommand{\xtitleSix}[1]{%
    \@ifstar{\subparagraph*{#1}}{\subparagraph{#1}}%
  }%
  \makeatother
}{%
  \ifthenelse{\equal{\xlifeppTopLevel}{part}}{%
    \makeatletter
    \newcommand{\xtitleOne}[1]{%
      \@ifstar{\part*{#1}}{\part{#1}}%
    }%
    \newcommand{\xtitleTwo}[1]{%
      \@ifstar{\chapter*{#1}}{\chapter{#1}}%
    }%
    \newcommand{\xtitleThree}[1]{%
      \@ifstar{\section*{#1}}{\section{#1}}%
    }%
    \newcommand{\xtitleFour}[1]{%
      \@ifstar{\subsection*{#1}}{\subsection{#1}}%
    }%
    \newcommand{\xtitleFive}[1]{%
      \@ifstar{\subsubsection*{#1}}{\subsubsection{#1}}%
    }%
    \newcommand{\xtitleSix}[1]{%
      \@ifstar{\paragraph*{#1}}{\paragraph{#1}}%
    }%
    \makeatother
  }{%
  
  }
}

\xdef\xlifeppDirectory{\xlifeppMode}
\ifthenelse{\equal{\xlifeppMode}{webdev}}{%
  \xdef\xlifeppDirectory{dev}%
}{}%
\ifthenelse{\equal{\xlifeppMode}{webuser}}{%
  \xdef\xlifeppDirectory{user}%
}{}%

\ifthenelse{\equal{\xlifeppMode}{dev}\or\equal{\xlifeppMode}{user}}{%
  \usetikzlibrary{fit,arrows,shadows,intersections,shapes,positioning,patterns}
}{}%

% specific macros
\makeatletter
\newcommand{\inputDoc}{%
  \@ifstar{\inputDocStar}{\inputDocNoStar}%
}%
\makeatother

\newcommand{\inputDocStar}[1]{\input{inputs/\xlifeppDirectory/#1.tex}}%

\newcommand{\inputDocNoStar}[1]{%
\newpage%
\inputDocStar{#1}%
}%

\ifthenelse{\equal{\xlifeppMode}{webuser}\OR\equal{\xlifeppMode}{webdev}}{%
  \newcommand{\inputTikZ}[1]{%
    \includegraphics{inputs/tikz/#1.png}%
  }%
}{%
  \newcommand{\inputTikZ}[1]{%
    \input{inputs/tikz/#1.tikz}%
  }%
}%

\newcommand{\includeTikZPict}[2][]{%
  \includegraphics[#1]{inputs/tikz/#2}%
}%

\newcommand{\inputCode}[2][]{%
  \lstinputlisting[#1]{inputs/samples/#2}%
}%

\newcommand{\inputXlifeppExample}[2][]{%
  \lstinputlisting[#1]{../../examples/#2}%
}%

\definecolor{xlifeppcolor}{RGB}{0,127,127}%

\makeatletter
\ifthenelse{\equal{\xlifeppMode}{dev}\OR\equal{\xlifeppMode}{user}}{%
\renewcommand{\maketitle}{
\begin{titlepage}%
\begin{tikzpicture}[overlay, xshift=-0.1\paperwidth, yshift=-0.9\paperheight]
\coordinate (topleft) at (0,\paperheight);%
\coordinate (bottomright) at (\paperwidth,0);%
\coordinate (topright) at (topleft -| bottomright);%
\coordinate (bottomleft) at (topleft |- bottomright);%
\coordinate (center) at (barycentric cs:topleft=0.5,bottomright=0.5);%
\coordinate[left=0.5\textwidth, above=0.5\textheight] (bbTL) at (center);%
\coordinate[right=0.5\textwidth, below=0.5\textheight] (bbBR) at (center);%
\coordinate (bbTR) at (bbTL -| bbBR);%
\coordinate (bbBL) at (bbTL |- bbBR);%
\useasboundingbox (bbTL) rectangle (bbBR);%
\coordinate (left) at (topleft |- center);%
\coordinate[left=6cm] (shadetopright) at (topleft -| center.west);%
\coordinate (shadebottomright) at (bottomleft -| shadetopright);%
\shade[left color=xlifeppcolor!70!white, right color=xlifeppcolor!31!white] (topleft) rectangle (shadebottomright);%
%%%%%%%% logo
\draw (topright) node[below left=1.5cm and 1.5cm] {\includePict[width=5cm]{xlife.png}};%
%%%%%%%% title
\draw (left) node[above right=4cm and 5cm, name=title, text width=14cm] {%
\Huge \bfseries \color{xlifeppcolor} \@title%
};%
\coordinate[below right=0.5em and 2ex] (titleBL) at (title.south west);%
\coordinate[below left=0.5em and 2ex] (titleBR) at (title.south east);%
\draw[line width=0.5em, xlifeppcolor] (titleBL) -- (titleBR);%
%%%%%%%% author
\draw (left) node[above right=1cm and 5cm, name=author, text width=11cm] {%
\Large \@author%
};%
%%%%%%%% date
\draw (left) node[below right=3cm and 5cm, name=date] {%
\normalsize \@date%
};%
\end{tikzpicture}%
\end{titlepage}
}%

% Les entetes de chapitre
%
\def\@makechapterhead#1{%
  \hbox to \hsize{%
  \parindent \z@ \raggedright \reset@font%
   \rlap{%
   \begin{tikzpicture}%
   \definecolor{xlifeppcolor}{RGB}{0,127,127}%
   \begin{scope}%
   \draw (0,0) node[fill=xlifeppcolor, minimum size=4.5em, rounded corners, name=number] {\reset@font\fontsize{4em}{4em}\selectfont \color{white} \thechapter};%
   \end{scope}%
   \draw (1.5,0) node[anchor=west,black!80!white, text width=0.7\paperwidth] {\vbox{\reset@font\bfseries\huge \color{xlifeppcolor} #1}};%
   \coordinate[below=1em] (linestart) at (number.south west);%
   \draw[line width=0.25em, xlifeppcolor] (linestart) -- ++(\textwidth,0);%
   \end{tikzpicture}}%
  }%
  \vskip 40pt%
}

\def\@makeschapterhead#1{%
  \hbox to \hsize{%
  \parindent \z@ \raggedright \reset@font%
   \rlap{%
   \begin{tikzpicture}%
   \definecolor{xlifeppcolor}{RGB}{0,127,127}%
   \begin{scope}%
   \draw (0,0) node[fill=xlifeppcolor, minimum size=4.5em, rounded corners, name=number] {};%
   \end{scope}%
   \draw (1.5,0) node[anchor=west,black!80!white] {\vbox{\advance\hsize by -6em\reset@font\sffamily\bfseries\huge \color{xlifeppcolor} #1}};%
   \coordinate[below=1em] (linestart) at (number.south west);%
   \draw[line width=0.25em, xlifeppcolor] (linestart) -- ++(\textwidth,0);%
   \end{tikzpicture}}%
  }%
  \vskip 40pt%
 }
}{}%
\makeatother

\setcounter{MaxMatrixCols}{10}
\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}

\geometry{a4paper, top=2.5cm, bottom=2.5cm, left=1.8cm, right=1.8cm}
%\itemsep -0.15cm
\parindent=0pt
\setlist[description]{itemsep=0pt}

% default style for color boxes
\tcbset{colback=xlifeppcolor!5!white, colframe=xlifeppcolor!75!black}
% loading library listings in tcolorbox
\tcbuselibrary{listingsutf8,skins,theorems}
\tcbset{listing engine=listings}

\newcommand{\melina}{{\scshape Melina}\xspace}
\newcommand{\melinapp}{{\scshape Melina++}\xspace}
\newcommand{\freefem}{{\scshape Freefem++}\xspace}
\newcommand{\montjoie}{{\scshape Montjoie}\xspace}
\newcommand{\gmsh}{{\scshape Gmsh}\xspace}
\newcommand{\occ}{{\scshape OpenCASCADE}\xspace}
\newcommand{\convmesh}{{\scshape Convmesh}\xspace}
\newcommand{\doxygen}{{\scshape Doxygen}\xspace}
\newcommand{\paraview}{{\scshape Paraview}\xspace}
\newcommand{\git}{{\scshape Git}\xspace}
\newcommand{\cmake}{{\scshape CMake}\xspace}
\newcommand{\matlab}{{\scshape Matlab}\xspace}
\newcommand{\octave}{{\scshape Octave}\xspace}

\newcommand{\xlifepp}{{\scshape XLiFE++}\xspace}
\newcommand{\arpack}{{\scshape Arpack}\xspace}
\newcommand{\arpackpp}{{\scshape Arpack++}\xspace}
\newcommand{\blas}{{\scshape Blas}\xspace}
\newcommand{\openblas}{{\scshape OpenBlas}\xspace}
\newcommand{\lapack}{{\scshape Lapack}\xspace}
\newcommand{\eispack}{{\scshape Eispack}\xspace}
\newcommand{\umfpack}{{\scshape UmfPack}\xspace}
\newcommand{\magma}{{\scshape Magma}\xspace}
\newcommand{\suitesparse}{{\scshape SuiteSparse}\xspace}
\newcommand{\eigen}{{\scshape Eigen}\xspace}
\newcommand{\amos}{{\scshape Amos}\xspace}
\newcommand{\fortran}{{\scshape Fortran}\xspace}
\newcommand{\cpp}{{\scshape C++} \xspace}
\newcommand{\omp}{{\scshape OpenMP}\xspace}

\newcommand{\poems}{{\scshape POems}\xspace}
\newcommand{\irmar}{{\scshape IRMAR}\xspace}

\newcommand{\unix}{{\scshape Unix}\xspace}
\newcommand{\linux}{{\scshape Linux}\xspace}
\newcommand{\macos}{{\scshape Mac OS}\xspace}
\newcommand{\windows}{{\scshape Windows}\xspace}
\newcommand{\docker}{{\scshape Docker}\xspace}
\newcommand{\dockertoolbox}{{\scshape Docker ToolBox}\xspace}

\newcommand{\lib}[1]{{\color{xlifeppcolor}\itshape #1}}
\newcommand{\class}[1]{{\color{xlifeppcolor}\ttfamily #1}}
\newcommand{\libtitle}[1]{{\itshape #1}}
\newcommand{\classtitle}[1]{{\ttfamily #1}}
\newcommand{\cmd}[1]{{\color{blue!70!black}\ttfamily #1}}
\newcommand{\var}[1]{{\ttfamily #1}}
\newcommand{\param}[1]{{\bfseries #1}}
\newcommand{\cmakeopt}[1]{{\color{xlifeppcolor!50!red}\bfseries #1}}

\ifthenelse{\equal{\xlifeppMode}{dev}}{%
\newcommand{\displayInfos}[1]{
\pgfkeys{/xlifepp/info/.cd, library/.initial={}, test/.initial={},%
                                           header/.initial={}, implementation/.initial={},%
                                           header dep/.initial={}}%
\pgfkeys{/xlifepp/info/.cd,#1}%
\pgfkeys{/xlifepp/info/.cd, library/.get=\xlifeppLibrary, test/.get=\xlifeppTest,%
                                           header/.get=\xlifeppHeader, implementation/.get=\xlifeppImplementation,%
                                           header dep/.get=\xlifeppHeaderDep}%

\small 
\vspace{5mm}
\goodbreak
\hrule
\vspace{1mm}
\begin{tabular}{rp{13.5cm}}
library : & {\bfseries \xlifeppLibrary } \\
header : & {\bfseries \xlifeppHeader } \\
implementation : & {\bfseries \xlifeppImplementation } \\
unitary tests : & {\bfseries \xlifeppTest } \\
header dependences : & {\bfseries \xlifeppHeaderDep }
\end{tabular}
\vspace{1mm}
\hrule
\vspace{5mm}
\normalsize
}%
}{}%

\ifthenelse{\equal{\xlifeppMode}{webuser}\OR\equal{\xlifeppMode}{webdev}}{%
  %\newsavebox{\xlifeppbox}
  \newenvironment{focusbox}{\includegraphics{inputs/icons/focus.png}}{}%
  \newenvironment{ideabox}{\includegraphics{inputs/icons/idea.png}}{}%
  \newenvironment{advancedbox}{\includegraphics{inputs/icons/advanced.png}}{}%
  \newenvironment{warningbox}{\includegraphics{inputs/icons/warning.png}}{}%
  \newenvironment{errorbox}{\includegraphics{inputs/icons/error.png}}{}%
  \newenvironment{toxicbox}{\includegraphics{inputs/icons/toxic.png}}{}%
  \newenvironment{cpp11box}{\includegraphics{inputs/icons/cpp11.png}}{}%
  \newenvironment{windowsbox}{\includegraphics{inputs/icons/windows.png}}{}%
  \newenvironment{macosbox}{\includegraphics{inputs/icons/macos.png}}{}%
  \newenvironment{linuxbox}{\includegraphics{inputs/icons/linux.png}}{}%
  %\renewcommand{\textdbend}{\includegraphics{inputs/tikz/textdbend.png}}
}{%
  \newtcolorbox{focusbox}{%
                colback=black!75!white!5!white, colframe=black!75!white!75!black,%
                before upper={\lettrine[image,lines=2,findent=1ex]{inputs/icons/focus.png}},%
                before=\vspace{0.5em}\par, after=\vspace{0.25em}\par,%
                height=4em, height plus=\textheight, left=1ex, right=1ex}
  \newtcolorbox{ideabox}{%
                colback=green!5!white, colframe=green!75!black,%
                before upper={\lettrine[image,lines=2,findent=1ex]{inputs/icons/idea.png}},% 
                before=\vspace{0.5em}\par, after=\vspace{0.25em}\par,%
                height=4em, height plus=\textheight, left=1ex, right=1ex}
  \newtcolorbox{advancedbox}{%
                colback=xlifeppcolor!5!white, colframe=xlifeppcolor!75!black,%
                before upper={\lettrine[image,lines=2,findent=1ex]{inputs/icons/advanced.png}},% 
                before=\vspace{0.5em}\par, after=\vspace{0.25em}\par,%
                height=4em, height plus=\textheight, left=1ex, right=1ex}
  \newtcolorbox{warningbox}{%
                colback=orange!5!white, colframe=orange!75!black,%
                before upper={\lettrine[image,lines=2,findent=1ex]{inputs/icons/warning.png}},%
                before=\vspace{0.5em}\par, after=\vspace{0.25em}\par,%
                height=4em, height plus=\textheight, left=1ex, right=1ex}
  \newtcolorbox{errorbox}{%
                colback=red!5!white, colframe=red!75!black,%
                before upper={\lettrine[image,lines=2,findent=1ex]{inputs/icons/error.png}},%
                before=\vspace{0.5em}\par, after=\vspace{0.25em}\par,%
                height=4em, height plus=\textheight, left=1ex, right=1ex}
  \newtcolorbox{toxicbox}{%
                colback=violet!5!white, colframe=violet!75!black,%
                before upper={\lettrine[image,lines=2,findent=1ex]{inputs/icons/toxic.png}},%
                before=\vspace{0.5em}\par, after=\vspace{0.25em}\par,%
                height=4em, height plus=\textheight, left=1ex, right=1ex}
  \newtcolorbox{cpp11box}{%
                colback=white, boxrule=0pt, leftrule=0.5mm, sharpish corners, colframe=gray,%
                before upper={\lettrine[image,lines=2,findent=1ex]{inputs/icons/cpp11.png}},%
                before=\vspace{0.5em}\par, after=\vspace{0.25em}\par,%
                height=4em, height plus=\textheight, left=1ex, right=1ex}
  \newtcolorbox{windowsbox}{%
                colback=white, boxrule=0pt, leftrule=0.5mm, sharpish corners, colframe=gray,%
                before upper={\lettrine[image,lines=2,findent=1ex]{inputs/icons/windows.png}},%
                before=\vspace{0.5em}\par, after=\vspace{0.25em}\par,%
                height=4em, height plus=\textheight, left=1ex, right=1ex}
  \newtcolorbox{macosbox}{%
                colback=white, boxrule=0pt, leftrule=0.5mm, sharpish corners, colframe=gray,%
                before upper={\lettrine[image,lines=2,findent=1ex]{inputs/icons/macos.png}},%
                before=\vspace{0.5em}\par, after=\vspace{0.25em}\par,%
                height=4em, height plus=\textheight, left=1ex, right=1ex}
  \newtcolorbox{linuxbox}{%
                colback=white, boxrule=0pt, leftrule=0.5mm, sharpish corners, colframe=gray,%
                before upper={\lettrine[image,lines=2,findent=1ex]{inputs/icons/linux.png}},%
                before=\vspace{0.5em}\par, after=\vspace{0.25em}\par,%
                height=4em, height plus=\textheight, left=1ex, right=1ex}
}%

\usepackage{xlifepp-listings-style}

\ifthenelse{\equal{\xlifeppMode}{webuser}\OR\equal{\xlifeppMode}{webdev}}{%
  \lstset{language=xlife,%
  style=web,%
  }%
}{%
  \lstset{language=xlife,%
  style=main,%
  }%
}%

\endinput%
