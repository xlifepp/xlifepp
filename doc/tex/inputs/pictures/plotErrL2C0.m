h=2;
l2=3;
c0=4;
t=5;
mo=5;
s=mo+1;
cols=['b' 'g' 'r' 'c' 'm' 'y' 'k'];
figure('Color','w')
hold on
leg=cell(2*s,1);
for k=1:s
plot(md(k:s:end,2),md(k:s:end,l2),'LineWidth',1.5,'Color',cols(k),'Marker','+')
plot(md(k:s:end,2),md(k:s:end,c0),'LineWidth',1.,'Color',cols(k),'Marker','+','LineStyle','--')
Yl2=log(md(k:s:end-10,l2));
Yc0=log(md(k:s:end-10,c0));
X=log(md(k:s:end-10,2));
n=length(X); sx=sum(X);
ql2 =(n*X'*Yl2-sx*sum(Yl2))/(n*X'*X-sx*sx);
qc0 =(n*X'*Yc0-sx*sum(Yc0))/(n*X'*X-sx*sx);
s1=['P' num2str(k) ' L2 error (q=' num2str(round(ql2,2)) ')'];
s2=['P' num2str(k) ' C0 error (q=' num2str(round(qc0,2)) ')'];
leg{2*(k-1)+1}=s1;
leg{2*k}=s2;
end
xlabel('h');
ylabel('error');
xlim([0.02 0.42]);
legend(leg,'EdgeColor','w','FontSize',18);
title(['Laplace2D on ellipse, mesh order ' num2str(mo)],'FontSize',24);
ax=gca;
get(ax)
ax.XScale='log';
ax.YScale='log';
ax.YGrid='on';
ax.YMinorGrid='on';
ax.FontSize=18;
ax.XLim=[0.02 0.42];
ax.XTick=[0.050000000000000 0.100000000000000 0.150000000000000 0.200000000000000 0.250000000000000 0.300000000000000 0.350000000000000 0.400000000000000];