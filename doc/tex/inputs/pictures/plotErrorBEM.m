h=2;
l2=3;
c0=4;
t=5;
mo=3;
s=mo+1;
cols=['b' 'g' 'r' 'c' 'm' 'y' 'k'];
figure('Color','w')
hold on
leg=cell(s,1);
for k=1:s
plot(md(k:s:end,2),md(k:s:end,3),'LineWidth',1.5,'Color',cols(k),'Marker','+')
Yl2=log(md(k:s:end-4*s,3));
X=log(md(k:s:end-4*s,2));
n=length(X); sx=sum(X);
ql2 =(n*X'*Yl2-sx*sum(Yl2))/(n*X'*X-sx*sx);
s1=['P' num2str(k-1) ' error (q~' num2str(round(ql2,2)) ')'];
leg{k}=s1;
end
xlabel('h');
ylabel('error');
xlim([0.09 0.71]);
legend(leg,'EdgeColor','w','FontSize',18);
title(['Helmholtz 3D on sphere, BEM Dirichlet SL, mesh order ' num2str(mo)],'FontSize',24);
ax=gca;
get(ax)
ax.XScale='log';
ax.YScale='log';
ax.YGrid='on';
ax.YMinorGrid='on';
ax.FontSize=18;
ax.XLim=[0.09 0.71];
ax.XTick=[0.050000000000000 0.100000000000000 0.150000000000000 0.200000000000000 0.250000000000000 0.300000000000000 0.350000000000000 0.400000000000000];