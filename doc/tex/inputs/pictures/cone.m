%plot cone
figure('Color','white')
alpha=pi/10;h=10;s=sin(alpha);
r = linspace(0,h*tan(alpha),50) ;
th = linspace(0,2*pi,50) ;
[R,T] = meshgrid(r,th) ;
X = R.*cos(T); Y = R.*sin(T); Z = R/tan(alpha);
surf(X,Y,Z,'Facecolor',[0.9,0.9,0.9],'Edgecolor',[0.9 0.9 0.9],'Facealpha',0.7)
axis equal; hold on
%plot geodesic
h0=2; r0=h0*tan(alpha);eps=0.001; % starting point
nu0=pi/2-eps;                     % starting direction
b=-atan(tan(nu0)/s)/s;
a=r0*cos(b*s);
t0=0; tmax=b+acos(a/(h*tan(alpha)))/s;
t=t0:tmax/100:tmax; 
r=a./cos((t-b)*s);
x=(r+eps).*cos(t+t0); y=(r+eps).*sin(t+t0)+eps; z=r/tan(alpha);
plot3(x,y,z,'-k','Linewidth',3)