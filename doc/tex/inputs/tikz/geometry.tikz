\begin{tikzpicture}[font=\tiny]
\begin{umlpackage}{space}
\umlclass{Space}{SpaceType spaceType \\ String name \\ SobolevType conformingSpace \\ Dimen dimFun \\ Dimen dimCom}{}
\umlclass[x=7, anchor=north]{SpSpace}{std::vector<SpDof> dofs}{}
\umlclass[x=14, anchor=north]{FeSpace}{const Interpolation* interpolation\_p \\ std::vector<FeDof> dofs}{}
\umlclass[x=22, anchor=north]{SubSpace}{Space* parent\_p \\ std::vector<Number> dofNumbers\_}{}
\umlclass[x=7, y=-5]{SpectralBasis}{Number numberOfFun\_ \\ Number dimFun\_ \\ ValueType returnedType\_ \\ StrucType returnedStruct\_ \\ FuncFormType funcFormType\_}{}
\umlclass[x=14, y=-5]{Element}{Number number\_ \\ RefElement* refElt\_p \\ GeomElement* geomElt\_p \\ std::vector<Number> dofNumbers \\ std::vector<Element*> parents\_;}{}
\umlclass[x=22, y=-4]{FeSubSpace}{std::vector<std::vector<Number> > dofRanks}{}
\umlVHinherit[anchor2=20]{SpSpace}{Space}
\umlVHinherit[anchor2=20]{FeSpace}{Space}
\umlVHinherit[anchor2=20]{SubSpace}{Space}
\umlinherit{FeSubSpace}{SubSpace}
\umluniaggreg{SpSpace}{SpectralBasis}
\umluniaggreg[anchors= -50 and 62, stereo=vector]{FeSpace}{Element}
\umluniaggreg{FeSubSpace}{Element}
\umluniaggreg[anchors=110 and -120, stereo=vector]{Element}{FeSpace}
\end{umlpackage}
\begin{umlpackage}[y=-10]{geometry}
\umlclass[y=-2]{Mesh}{std::vector<Point> nodes \\ std::vector<Number> vertices\_ \\ bool isMadeOfSimplices\_ \\ Dimen order\_}{}
\umlclass[x=6]{Domain}{String name \\ Dimen dim \\ DomainType domType \\ Domain* domain\_p}{}
\umlclass[x=12, anchor=north]{MeshDomain}{MeshDomain* parent\_p \\ std::set<ShapeType> shapeTypes}{}
\umlclass[x=20, anchor=north]{CompositeDomain}{SetOperationType setOpType\_ \\ std::vector<const Domain*> domains\_}{}
\umlclass[x=12, y=-5]{GeomElement}{Number number\_ \\ std::vector<GeoNumPair> parentSides\_}{}
\umlclass[y=-7]{Geometry}{}{}
\umlclass[x=20, y=-9]{MeshElement}{std::vector<Point*> nodes \\ std::vector<Number> nodeNumbers \\ std::vector<Number> vertexNumbers \\ short int orientation \\ std::vector<Real> measures \\ std::vector<Number> sideNumbers \\ std::vector<Number> sideOfSideNumbers \\ const RefElement* refElt\_p \\ Number index\_ \\ const Dimen spaceDim\_}{}
\umlclass[x=7, y=-9]{GeomMapData}{Point currentPoint \\ Matrix<Real> jacobianMatrix \\ Matrix<Real> inverseJacobianMatrix \\ Real jacobianDeterminant \\ Real differentialElement \\ Vector<Real> normalVector \\ Matrix<Real> metricTensor \\ Real metricTensorDeterminant}{}
\umlVHuniaggreg[stereo=vector, pos stereo=1.5, anchor2=160]{Mesh}{Domain}
\umluniaggreg[anchors=205 and 20]{Domain}{Mesh}
\umlVHinherit[anchor2=20]{MeshDomain}{Domain}
\umlVHinherit[anchor2=20]{CompositeDomain}{Domain}
\umluniaggreg[stereo=vector]{MeshDomain}{GeomElement}
\umlVHuniaggreg[anchors=150 and -20]{GeomElement}{Mesh}
\umlVHuniaggreg[stereo=vector, pos stereo=1.5, arg=elements\_, pos=2, align=right, anchors=-40 and 170]{Mesh}{GeomElement}
\umlVHuniaggreg[stereo=vector, pos stereo=1.5, arg=sides\_, pos=2, align=right, anchors=-40 and 180]{Mesh}{GeomElement}
\umlVHuniaggreg[stereo=vector, pos stereo=1.5, arg=sideOfSides\_, pos=2, align=right, anchors=-40 and 190]{Mesh}{GeomElement}
\umlunicompo{Mesh}{Geometry}
\umlVHuniaggreg[anchors=-70 and 160]{GeomElement}{MeshElement}
\umluniaggreg{MeshElement}{GeomMapData}
\umluniaggreg[anchors=-20 and 200]{GeomMapData}{MeshElement}
\end{umlpackage}

\begin{umlpackage}[x=22,y=-25]{finiteElements}
\umlemptyclass{RefElement}
\end{umlpackage}

\tikzumlset{draw=red}
\umlVHVuniaggreg[arm2=2.5cm]{Space}{Domain}
\umlVHVuniaggreg[arm2=2.5cm]{SpectralBasis}{Domain}
\umlHVHuniaggreg[arm1=7.5cm, anchor1=-20]{Element}{GeomElement}
\umlHVHuniaggreg[arm1=8cm, anchor1=-10]{Element}{RefElement}
\end{tikzpicture}
