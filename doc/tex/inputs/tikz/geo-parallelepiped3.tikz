\definecolor{xlife}{RGB}{0,127,127}%
\begin{tikzpicture}[scale=0.8]
\coordinate (v1) at (0,0,0);
\coordinate (v2) at (8,0,0);
\coordinate (v3) at (8,0,-4);
\coordinate (v4) at (0,0,-4);
\coordinate (v5) at (1,3,0);
\coordinate (v6) at (9,3,0);
\coordinate (v7) at (9,3,-4);
\coordinate (v8) at (1,3,-4);

\coordinate (p12) at (barycentric cs:v1=1,v2=1);
\coordinate (p23) at (barycentric cs:v2=1,v3=1);
\coordinate (p34) at (barycentric cs:v3=1,v4=1);
\coordinate (p14) at (barycentric cs:v4=1,v1=1);
\coordinate (p56) at (barycentric cs:v5=1,v6=1);
\coordinate (p67) at (barycentric cs:v6=1,v7=1);
\coordinate (p78) at (barycentric cs:v7=1,v8=1);
\coordinate (p58) at (barycentric cs:v8=1,v5=1);
\coordinate (p15) at (barycentric cs:v1=1,v5=1);
\coordinate (p26) at (barycentric cs:v2=1,v6=1);
\coordinate (p37) at (barycentric cs:v3=1,v7=1);
\coordinate (p48) at (barycentric cs:v4=1,v8=1);

\coordinate (c1234) at (barycentric cs:p12=1,p34=1);
\coordinate (c5678) at (barycentric cs:p56=1,p78=1);
\coordinate (c1265) at (barycentric cs:p12=1,p56=1);
\coordinate (c4378) at (barycentric cs:p34=1,p78=1);
\coordinate (c2376) at (barycentric cs:p23=1,p67=1);
\coordinate (c1485) at (barycentric cs:p14=1,p58=1);

\coordinate (c) at (barycentric cs:v1=1,v7=1);

\draw (p15) -- (c1265) node[midway, below] {$n_1$} -- (p56) node[midway, left] {$n_{14}$}-- (v5) node[midway, below] {$n_7$}-- (p15) node[midway, left] {$n_{13}$};
\draw (v5) -- (v8) node[midway, left] {$n_{12}$} -- (v7) node[midway, above] {$n_{11}$} -- (p67) node[near start, left] {$n_{10}$} -- (c5678) node[midway, above] {$n_9$} -- (p56) node[near start, left] {$n_8$};
\draw (v7) -- (p37) node[midway, right] {$n_{17}$} -- (c2376) node[midway, right] {$n_4$} -- (c) node[midway, below] {$n_3$} -- (c1265)  node[midway, right] {$n_2$};
\draw (c) -- (c5678) node[near start, right] {$n_{15}$};
\draw (c2376) -- (p67) node[near end, left] {$n_{16}$};
\draw[dashed] (p15) -- (p48) node[midway, right] {$n_6$};
\draw[dashed] (p48) -- (v8) node[midway, right] {$n_{18}$};
\draw[dashed] (p48) -- (p37) node[near end, below] {$n_5$};

\fill[xlife!50!white] (v1) circle (3pt);
\fill[xlife!50!white] (v2) circle (3pt);
\fill[xlife!50!white] (v3) circle (3pt);
\fill[xlife!50!white] (v4) circle (3pt);
\fill[xlife!75!black] (v5) circle (3pt) node[above] {$p_1$};
\fill[xlife!50!white] (v6) circle (3pt);
\fill[xlife!75!black] (v7) circle (3pt) node[above] {$p_2$};
\fill[xlife!75!black] (v8) circle (3pt) node[above] {$p_3$};
\fill[xlife!75!black] (p15) circle (3pt) node[left] {$p_4$};
\fill[xlife!75!black] (p37) circle (3pt) node[right] {$p_5$};
\fill[xlife!75!black] (p48) circle (3pt) node[above left] {$p_6$};
\fill[xlife!75!black] (p56) circle (3pt) node[above left] {$p_7$};
\fill[xlife!75!black] (p67) circle (3pt) node[above left] {$p_8$};
\fill[xlife!75!black] (c1265) circle (3pt) node[below] {$p_9$};
\fill[xlife!75!black] (c2376) circle (3pt) node[below] {$p_{10}$};
\fill[xlife!75!black] (c5678) circle (3pt) node[above] {$p_{11}$};
\fill[xlife!75!black] (c) circle (3pt) node[below right] {$p_{12}$};

\draw (4,0.5,0) node[name=sn0, red!50!black] {$sn_1$};
\draw (9,4,-4) node[name=sn1, red!50!black] {$sn_2$};
\draw (0, 1, 0) node[name=sn2, red!50!black] {$sn_3$};
\draw (-1,4, -4) node[name=sn3, red!50!black] {$sn_4$};
\draw (-2,2.5,-2) node[name=sn4, red!50!black] {$sn_5$};
\draw (11,3,-2) node[name=sn5, red!50!black] {$sn_6$};
\draw (8.5,1.5,0) node[name=sn6, red!50!black] {$sn_7$};
\draw (6,0.5,-1) node[name=sn7, red!50!black] {$sn_8$};

\begin{scope}
\clip (p15) -- (p48) -- (c4378) -- (c1265) -- (p15);
\draw[red!50!black, ->, >=triangle 60, dashed] (sn0) .. controls +(170:1cm) and +(-110:1.5cm) .. (barycentric cs:p15=1,c4378=1);
\end{scope}
\begin{scope}
\clip (p15) -- (c1265) -- (sn0.south east) -- (sn0.south west) -- (p15);
\draw[red!50!black, ->, >=triangle 60] (sn0) .. controls +(170:1cm) and +(-110:1.5cm) .. (barycentric cs:p15=1,c4378=1);
\end{scope}

\draw[red!50!black, ->, >=triangle 60] (sn1) .. controls +(180:1.5cm) and +(45:1.5cm) .. (barycentric cs:v5=1,v7=2);

\draw[red!50!black, ->, >=triangle 60] (sn2) .. controls +(0:1.5cm) and +(-120:1.5cm) .. (barycentric cs:p15=1,p56=1);

\begin{scope}
\clip (v8) -- (v7) -- (v4) -- (v8);
\draw[red!50!black, ->, >=triangle 60, dashed] (sn3) .. controls +(0:1.5cm) and +(120:1.5cm) .. (barycentric cs:v3=1,v8=2);
\end{scope}
\begin{scope}
\clip (v8) -- (v7) --  (sn3.north west) -- (v8);
\draw[red!50!black, ->, >=triangle 60] (sn3) .. controls +(0:1.5cm) and +(120:1.5cm) .. (barycentric cs:v3=1,v8=2);
\end{scope}

\begin{scope}
\clip (p15) -- (v5) -- (v8) -- (p48) -- (p15);
\draw[red!50!black, ->, >=triangle 60, dashed] (sn4) .. controls +(-20:1cm) and +(180:1.5cm) .. (barycentric cs:p15=1.5,v8=1);
\end{scope}
\begin{scope}
\clip (v1) -- (v5) --  (sn4.north west) -- (v1);
\draw[red!50!black, ->, >=triangle 60] (sn4) .. controls +(-20:1cm) and +(180:1.5cm) .. (barycentric cs:p15=1.5,v8=1);
\end{scope}

\draw[red!50!black, ->, >=triangle 60] (sn5) .. controls +(-160:1cm) and +(0:1cm) .. (barycentric cs:c2376=1,v7=1);

\draw[red!50!black, ->, >=triangle 60] (sn6) .. controls +(170:1cm) and +(-120:1.75cm) .. (barycentric cs:c=1,p67=1);

\draw[red!50!black, ->, >=triangle 60] (sn7) .. controls +(90:1.5cm) and +(-20:1cm) .. (barycentric cs:c=1,c=1,p56=2);
\end{tikzpicture}
