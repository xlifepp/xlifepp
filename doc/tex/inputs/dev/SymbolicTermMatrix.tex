%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{SymbolicTermMatrix} class}

Sometimes, numerical methods involve more complex combinations of \class{TermMatrix}  than linear combinations, for instance 
\( \mathbb{A}=\mathbb{M} + \mathbb{K}  \mathbb{M}^{-1}\mathbb{K}^t\)  where  \(\mathbb{M}\) and  \(\mathbb{K}\) are sparse matrices. Generally, it is not a good idea to compute  \(\mathbb{A}\) because the result is a dense matrix. The purpose of the \class{SymbolicTermMatrix} class is to describe as symbolic this matrix, not to compute it but to compute \(\mathbb{A}X\) with \(X\) a \class{TermVector}.\\
A \class{SymbolicTermMatrix} object is a node of the binary tree with the following data :
\begin{itemize}
	\item A symbolic operation (one of id, +, -,*, /, inv, tran, conj, adj);
	\item A TermMatrix object (pointer to), may be 0;
	\item Up to two SymbolicTermMatrix objects (pointer to);
	\item A coefficient (complex scalar)  applied to the operation.
\end{itemize}
Using these data, the symbolic form of the \( \mathbb{A}\) matrix is presented on figure \ref{fig_tree_symb_matrix}. On a node, there is either a \class{TermMatrix} (leaves) or a pair of \class{SymbolicTermMatrix}.
\vspace{.1cm}
\begin{figure}[H]
	\centering
	\includePict[width=12cm]{SymbolicTermMatrix.png}
	\caption{ Tree representation of matrix \(\mathbb{M} + \mathbb{K}  \mathbb{M}^{-1}\mathbb{K}^t\)} 
	\label{fig_tree_symb_matrix}
\end{figure}
So the \class{SymbolicTermMatrix} class has the following data members
\vspace{.1cm}
\begin{lstlisting}
class SymbolicTermMatrix
{public:
  SymbolicTermMatrix* st1_, *st2_; 
  const TermMatrix* tm_;           
  complex_t coef_;                 
  SymbolicOperation op_;           
  bool delMat_;                  
\end{lstlisting}
\vspace{.2cm}
the following constructors and destructor stuff
\vspace{.1cm}
\begin{lstlisting}
SymbolicTermMatrix();
SymbolicTermMatrix(const TermMatrix&, const complex_t& =complex_t(1.,0.));
SymbolicTermMatrix(SymbolicOperation, SymbolicTermMatrix*, SymbolicTermMatrix*=0);
SymbolicTermMatrix(LcTerm<TermMatrix>& lc);
~SymbolicTermMatrix();
void add(LcTerm<TermMatrix>&, std::vector<std::pair<const TermMatrix*, complex_t> >::iterator);
SymbolicTermMatrix(const SymbolicTermMatrix&);
SymbolicTermMatrix& operator=(const SymbolicTermMatrix&);            
\end{lstlisting}
The \var{delMat} variable is a flag telling the TermMatrix can be deleted because one of the constructors creates a copy of the original one. \\

\vspace{.1cm}
\begin{lstlisting}
SymbolicTermMatrix();
SymbolicTermMatrix(const TermMatrix&, const complex_t& =complex_t(1.,0.));
SymbolicTermMatrix(SymbolicOperation, SymbolicTermMatrix*, SymbolicTermMatrix*=0);
SymbolicTermMatrix(LcTerm<TermMatrix>& lc);
~SymbolicTermMatrix();
void add(LcTerm<TermMatrix>&, std::vector<std::pair<const TermMatrix*, complex_t> >::iterator);
SymbolicTermMatrix(const SymbolicTermMatrix&);
SymbolicTermMatrix& operator=(const SymbolicTermMatrix&);            
\end{lstlisting}
\vspace{.2cm}
The class offers classic print stuff :
\vspace{.1cm}
\begin{lstlisting}
string_t asString() const;
void print(std::ostream&) const;
void print(PrintStream& os) const {print(os.currentStream());}
friend std::ostream& operator<<(std::ostream& ,const SymbolicTermMatrix&)
\end{lstlisting}
\vspace{.2cm}

Besides, a lot of operators are provided:
\vspace{.1cm}
\begin{lstlisting}
SymbolicTermMatrix& operator *(const TermMatrix&,SymbolicTermMatrix&);
SymbolicTermMatrix& operator *(SymbolicTermMatrix&, const TermMatrix&);
SymbolicTermMatrix& operator +(const TermMatrix&,SymbolicTermMatrix&);
SymbolicTermMatrix& operator +(SymbolicTermMatrix&, const TermMatrix&);
SymbolicTermMatrix& operator -(const TermMatrix&,SymbolicTermMatrix&);
SymbolicTermMatrix& operator -(SymbolicTermMatrix&, const TermMatrix&);
SymbolicTermMatrix& operator +(LcTerm<TermMatrix>&,SymbolicTermMatrix&);
SymbolicTermMatrix& operator +(SymbolicTermMatrix&, LcTerm<TermMatrix>&);
SymbolicTermMatrix& operator -(LcTerm<TermMatrix>&,SymbolicTermMatrix&);
SymbolicTermMatrix& operator -(SymbolicTermMatrix&, LcTerm<TermMatrix>&);
SymbolicTermMatrix& operator *(LcTerm<TermMatrix>&,SymbolicTermMatrix&);
SymbolicTermMatrix& operator *(SymbolicTermMatrix&, LcTerm<TermMatrix>&);
SymbolicTermMatrix& operator *(SymbolicTermMatrix&, SymbolicTermMatrix&);
SymbolicTermMatrix& operator +(SymbolicTermMatrix&, SymbolicTermMatrix&);
SymbolicTermMatrix& operator -(SymbolicTermMatrix&, SymbolicTermMatrix&);
SymbolicTermMatrix& conj(SymbolicTermMatrix&);
SymbolicTermMatrix& adj(SymbolicTermMatrix&);
SymbolicTermMatrix& tran(SymbolicTermMatrix&);
SymbolicTermMatrix& operator *(SymbolicTermMatrix&, const complex_t&);
SymbolicTermMatrix& operator *(const complex_t&, SymbolicTermMatrix&);
SymbolicTermMatrix& operator /(SymbolicTermMatrix&, const complex_t&);
SymbolicTermMatrix& inv(const TermMatrix&);
SymbolicTermMatrix& inv(SymbolicTermMatrix&);
\end{lstlisting}
\vspace{.2cm}
Because some syntaxes may be ambiguous, the operator \verb|~| is overloaded in order to move a \class{TermMatrix} to a \class{SymbolicTermMatrix}:
\vspace{.1cm}
\begin{lstlisting}
SymbolicTermMatrix& operator~(const TermMatrix&):
\end{lstlisting}
\vspace{.2cm}
The most important functions are function that compute recursively the matrix vector product \(\mathbb{A}X\) :
\vspace{.1cm}
\begin{lstlisting}
TermVector operator*(const SymbolicTermMatrix&, const TermVector&);
TermVector multMatrixVector(const SymbolicTermMatrix&, const TermVector&);
TermVector operator*(const TermVector&, const SymbolicTermMatrix&);
TermVector multVectorMatrix(const TermVector&, const SymbolicTermMatrix&);
\end{lstlisting}
\vspace{.2cm}
\displayInfos{
	library=term, 
	header=SymbolicTermMatrix.hpp, 
	implementation=SymbolicTermMatrix.cpp,
	test=unit\_TermMatrix.cpp,
	header dep={TermMatrix.hpp, config.h, utils.h}
}