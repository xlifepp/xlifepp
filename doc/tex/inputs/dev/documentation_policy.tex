%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
The documentation policy of the library is based on three complementary documentations:
\begin{itemize}
\item An online documentation compliant with \doxygen syntax,
\item A user documentation describing main features of the library with some examples,
\item A developer documentation describing as possible all the features and given additional
explanations.
\end{itemize}
It is the responsibility and the duty of the developers to write these documentations \textbf{in
English}.

\section{Inline documentation}

The inline documentation is compliant with \doxygen syntax and the HTML documentation is automatically
generated with majors updates by the source control manager.

This inline documentation follows a documentation convention available on the development page
of the \xlifepp website: \url{http://uma.ensta-paris.fr/soft/XLiFE++/}. We will
sum it up now in the following:

\subsection{General points}

\begin{enumerate}
\item The reference language of the documentation is English.
\item Function or operation declarations  are documented with brief (one-line, one-sentence)
comments and only brief comments.
\item Function or operation definitions (inline or not) are documented by detailed comments.
\end{enumerate}

\subsection{Documentation of a file}

Here is the structure of the comment you have to write to document a file (header or source)
:

\begin{lstlisting}
/*!
    brief comment

    \file Parameter.hpp
    \author T. Anderson
    \date 25 jan 2011
    \since 3 mar 2007

    next part of detailed comment
 */
\end{lstlisting}

This comment is dedicated to present the content of the file: list of classes, list of external
functions, \ldots It is written at the top of the file before everything else.

\medskip

Some additional remarks:

\begin{itemize}
\item Thanks to the \lstinline{QT_AUTOBRIEF = YES} parameter, the first line of the comment
is interpreted as the brief comment. Therefore, the brief comment has to be placed before everything
else. There is another solution with the macro \(\backslash\)brief:

\begin{lstlisting}
/*!
    \file Parameter.hpp
    \author T. Anderson
    \date 25 jan 2011
    \since 3 mar 2007

    \brief brief comment
    
    next part of detailed comment
 */
\end{lstlisting}

\item The \(\backslash\)date macro is the last modification date.
\item The \(\backslash\)since macro is the file creation date.
\item When there are several authors, you will use the \(\backslash\)authors macro:

\begin{lstlisting}
/*!
    brief comment

    \file Parameter.hpp
    \authors T. Anderson, R. Cypher
    \date 25 jan 2011
    \since 3 mar 2007
    
    next part of detailed comment
 */
\end{lstlisting}
In this case, authors are separated by commas.
\end{itemize}

The file documentation rules are:

\begin{enumerate}
\item The file documentation is written at the top of the file, before everything else,
\item The file documentation follows one of the previous syntaxes, with the 4 pieces of information
file name, author(s), creation date and last modification date.
\end{enumerate}

\subsection{Documentation of a class, a typedef, an enum or a struct}

\begin{enumerate}
\item A class documentation is written just before the class declaration/definition in the
header file. 
\item The structure of the class documentation is as follows:

\begin{lstlisting}
/*!
    \class A
    brief comment associated to A

    next part of detailed comment associated to A
 */
class A {
\ldots
};
\end{lstlisting}
\item To document a typedef, you will use the \(\backslash\)typedef macro instead of the \(\backslash\)class
macro;
\item To document a struct, you will use the \(\backslash\)struct macro instead of the \(\backslash\)class
macro;
\item To document an enum, you will use the \(\backslash\)enum macro instead of the \(\backslash\)class
macro.
\end{enumerate}

\subsection{Documentation of attributes}

\begin{enumerate}
\item Attributes can be documented according to one of the following syntax:
\begin{lstlisting}
class A {
  public :
    //! comment associated to i
    int i;
    double j; //!< comment associated to j
    /*!
      comment associated to k
     */
    B k;
    \ldots
\end{lstlisting}
\end{enumerate}

\subsection{Documentation of functions and constructors}

\begin{enumerate}
\item Functions and operations declarations are documented with a brief comment, and only a
brief comment. The syntax, shown here for operations but valid for external functions, is one
of the following 
:
\begin{lstlisting}
class A {
  public :
    \ldots
   //! brief comment associated to declaration of f
   void f();

   /*! 
       brief comment associated to declaration of g
    */
   void g();
   
   void h(); //!< comment associated to declaration of h
};
\end{lstlisting}
\item Functions and operations definitions (inline or not) are documented with detailed comments.
If the declaration has not been commented first, the first line of the comment will be the
brief comment, as seen in previous sections. The syntax, shown here for inline operations but
valid for the others types, is one of the following:

\begin{lstlisting}
class A {
  public :
    B b;
    \ldots
   //! comment associated to definition of f
   void f() {
   \ldots
   }
   void g() //! comment associated to definition of g
   {
   \ldots
   }
   A() //! comment associated to definition of composite constructor
   : B() 
   {
   \ldots
   }

   /*!
      comment associated to definition of h
    */
   void h() {
   \ldots
   }
};
\end{lstlisting}

You may notice that for the function g, the comment marking is //! and not //!< and that for
the constructor in the same case, the comment is placed before components constructor calls.
\end{enumerate}

Let's see how to document function arguments.

\subsection{Documentation of arguments}

\begin{enumerate}
\item In the case only of a function definition (or an operation definition), you may want
to document arguments and return value. The syntax is one of the following 
:
\begin{lstlisting}
/*!
    comment of function f
 */
int f (double d, //!< comment of d
       int i //!< comment of i
      ) {
\ldots
}
/*!
    comment of function g
    \param d comment of d
    \param i comment of i
    \return comment of return value
 */
int g (double d, int i) {
\ldots
}
\end{lstlisting}
\end{enumerate}

\section{User and developer documentation}

User documentation as well developer documentation have to be written \textbf{in \LaTeX{} format}.
Each \TeX\ file concerns a package of files of the library, namely a topic of the library, for
instance Parameters. For each package \lib{xxx}, two files have to be written: the user documentation
file, named  \emph{xxx.tex} placed in subdirectory {\ttfamily doc/pdf/inputs/dev/} and the developer
documentation file, also named \emph{xxx.tex}, but placed in the subdirectory {\ttfamily doc/pdf/inputs/usr/}.
In these files, there are only latex section items (one or fewer) and subsection items, no
chapter items. All these files are collected in the main tex files: \emph{user\_documentation.tex}
and \emph{dev\_documentation.tex} in chapter environment, each chapter corresponding to a folder
of the library. \\

For instance, the Parameters user documentation (\emph{Parameters.tex}) looks like:

\begin{lstlisting}[language={}]
\section{Parameters classes}

In order to attached some user's data to anything (in particular functions), two classes 
(Parameter and Parameters) are proposed. The \class{Parameter} class handles a single data 
of type \emph{integer, real, complex, string} or \emph{void *} with the possibility to name 
the parameter. The \class{Parameters} class handles a list of \class{Parameter} objects.\\

\subsection{The \class{Parameter} object}

It is easy to define a parameter by its constructor or the assignment operation:
\ldots
\subsection{The \class{Parameters} object: List of parameters}
\ldots

\end{lstlisting}

The Parameters developer documentation (\emph{Parameters.tex}) looks like:

\begin{lstlisting}[language={}]
\section{Parameters}

It may be useful to have a data structure to store freely any kind of parameters (values, 
string, \ldots) with a very simple interface for the end user. In particular, such structure 
could be attached to user functions (see chap. \ref{chapfunctions}). This is achieved with 
two classes: the \class{Parameter} class which define one parameter and the 
\class{Parameters} class which manages a list of \class{Parameter} objects.

\subsection{The \class{Parameter} class}

The aim of the \class{Parameter} class is to encapsulate in the same structure, a data of 
integer type, real type, complex type, string type and pointer type (void pointer) with the 
capability to name the parameter. Thus, this class proposes as private members:
\ldots
\subsection{The \class{Parameters} class}
\ldots

\displayInfos{library=utils, header=Parameters.hpp, implementation=Parameters.cpp,
 test=test\_Parameters.cpp, header dep={config.h, String.hpp}, stl dep={map, iostream, 
sstream}}
\end{lstlisting}

\begin{itemize}
\item The end of a file has to be exposed the file dependencies with the following form:

\displayInfos{library=utils, header=Parameters.hpp, implementation=Parameters.cpp, test=test\_Parameters.cpp,
header dep={config.h, String.hpp}}

For this purpose, you have to use the {\bfseries \(\backslash\)displayInfos} macro.

\item To handle some C++ lines, you have to use the environment \emph{lstlisting}:

\begin{verbatim}
\begin{lstlisting}
class Parameter
{\ldots};
\end{lstlisting}
\end{verbatim}

\begin{lstlisting}
class Parameter
{\ldots};
\end{lstlisting}
\vspace{2mm}
or if you want to ignore language colors:
\begin{verbatim}
\begin{lstlisting}[language={}]
class Parameter
{\ldots};
\end{lstlisting}
\end{verbatim}

\begin{lstlisting}[language={}]
class Parameter
{\ldots};
\end{lstlisting}

The language coloring is automatic. You can add words in the language coloring
database in the file \emph{xlifepp-listings-style.sty}.

\item When you name a class, for example \class{Messages}, you have to use the {\bfseries \(\backslash\)class}
macro:
\begin{verbatim}
The \class{Messages} class
\end{verbatim}

\item When you name a sub-library of \xlifepp (a subdirectory of src), you have to use the {\bf
\(\backslash\)lib} macro:
\begin{verbatim}
The \lib{utils} library
\end{verbatim}

\item When you name a function, you have to use the {\bfseries \(\backslash\)cmd} macro:
\begin{verbatim}
The \cmd{findDomain} function
\end{verbatim}

\item When you name the library \xlifepp, you have to use the {\bfseries \(\backslash\)xlifepp} macro. For external libraries of software programs, such as \gmsh or \arpackpp, you have to use {\bfseries \(\backslash\)gmsh} or {\bfseries \(\backslash\)arpackpp} macros. The full available list is : {\bfseries \(\backslash\)xlifepp}, {\bfseries \(\backslash\)gmsh}, {\bfseries \(\backslash\)arpack}, {\bfseries \(\backslash\)arpackpp}, {\bfseries \(\backslash\)blas}, {\bfseries \(\backslash\)lapack}, {\bfseries \(\backslash\)umfpack}, {\bfseries \(\backslash\)eispack}, {\bfseries \(\backslash\)melina}, {\bfseries \(\backslash\)melinapp}, {\bfseries \(\backslash\)montjoie}, {\bfseries \(\backslash\)paraview}, {\bfseries \(\backslash\)freefem}, {\bfseries \(\backslash\)doxygen}, {\bfseries \(\backslash\)cmake}, {\bfseries \(\backslash\)git} and {\bfseries \(\backslash\)convmesh}.

\item When you add a package documentation in \emph{user\_documentation.tex} or \emph{dev\_documentation.tex},
you have to use the {\bfseries \(\backslash\)inputDoc} with the basename of the file in argument, and
to add it in the main itemize of the library:

\begin{lstlisting}[language={[LaTeX]TeX}]
\chapter{The \lib{utils} library}

The \lib{utils} library collects all the general useful classes and functionalities of the 
\xlifepp library. It is an independent library. It addresses:

\begin{itemize}
\item \class{String} capabilities (mainly additional functions to the 
\class{std::string} class)
\item \class{Point} class to deal with point of any dimension
\item \class{Vector} class to deal with numerical vectors (say real or complex vectors) 
\item \class{Matrix} class to deal with numerical matrices with dense storage (small 
matrices)
\item \class{Parameter} classes to deal with general set of user parameters
\item \class{Function} class encapsulating user functions
\item \class{Messages} classes to handle error, warning and info messages
\end{itemize}

\inputDoc{String}
\inputDoc{Point}
\inputDoc{Vector}
\inputDoc{Matrix}
\inputDoc{Parameters}
\inputDoc{Functions}
\inputDoc{Messages}
\end{lstlisting}

\item When you add a general documentation in \emph{user\_documentation.tex} or \emph{dev\_documentation.tex},
you have to use the {\bfseries \(\backslash\)inputDoc*} with the basename of the file in argument,
and to add it in the main itemize of the library:

\begin{lstlisting}[language={[LaTeX]TeX}]
\chapter{Tests policy}

In order to maintain backward compatibility of the code during its development, it is 
mandatory to have various test functions. Some of them are local test functions, says 
low level tests, and concerns "exhaustive" tests of functionalities of a class or a 
collection of classes or utilities. Others are global test functions, say high level test 
functions, involving a lot of classes of the library, for instance, solving a physical 
problem from the mesh up to the results.

\inputDoc*{tests_policy}
\end{lstlisting}
\end{itemize}



