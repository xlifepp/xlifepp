%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{SymbolicFunction} class}\label{s.symbfunction}

The \class{SymbolicFunction} class handles a function described in a symbolic way, that is an expression involving variables, constant and operators. In fact, a \class{SymbolicFunction} object is a node of a tree:
\vspace{.1cm}
\begin{lstlisting}
class SymbolicFunction
{
   public:
   const SymbolicFunction* fn1, *fn2;  //nodes involved 0, 1 or 2 (0 by default)
   VariableName var;                   //variable involved (undef by default)
   SymbolicOperation op;               //operation involved (id by default)
   complex_t coef;                     //coefficient to apply to
   complex_t par;                      //additional parameter 
   ... 
}
\end{lstlisting}  
\vspace{.2cm} 
A node may be: 
\begin{itemize}
	\item A constant value stored in \var{coef} with \var{fn1=fn2=0} and \var{var=\_varundef};
	\item A variable name (\var{\_x1}, or {\var\_x2} or {\var\_x3} or {\var\_r} or {\var\_theta} or {\var\_phi}) stored in \var{var}, with  \var{fn1=fn2=0};
	\item A unary operation stored in \var{op} and acting either on the variable or the \class{SymbolicFunction} object \var{fn1};   
	\item A binary operation stored in \var{op} and acting on the \class{SymbolicFunction} objects \var{fn1} and \var{fn2}.
\end{itemize}
The unary and binary operators are given by the enumeration:
\vspace{.1cm}
\begin{lstlisting}
enum SymbolicOperation 
{_idop=0, _plus, _minus, _multiply, _divide, _power,    
 _equal, _different, _less, _lessequal, _greater, _greaterequal, _and, _or,   
 _abs, _realPart, _imagPart, _sqrt, _squared, _sin, _cos, _tan,        //unary
 _asin, _acos, _atan, _sinh, _cosh, _tanh, _asinh, _acosh, _atanh,     //unary
 _exp, _log, _log10, _pow, _not                                        //unary
};
\end{lstlisting}  
\vspace{.2cm} 
Particular \class{SymbolicFunction} associated to variable name are predefined in the {\em globalScopeData.hpp} file: {\var x\_1}, {\var x\_2}, {\var x\_3}, {\var r\_}, {\var theta\_}, {\var phi\_}. They have to be used when defining a symbolic function.\\

As an example, the function \var{f(x\_1,x\_2)=  (x\_1+x\_2)<1 \&\& exp(x\_1)>2} has the following tree representation:
\begin{figure}[H]
\centering
\includePict[width=14cm]{SymbolicFunction_tree.png}
\caption{example of SymbolicFunction tree}
\end{figure}

\begin{cpp11box}
The functions asinh, acosh, atanh are only available when compiling with a C++ version greater than 2011.
\end{cpp11box}


\vspace{.2cm} 
The \class{SymbolicFunction} class provides some basic constructors for each kind of node:
\vspace{.1cm} 
\begin{lstlisting}
explicit SymbolicFunction(VariableName v=_varUndef);
explicit SymbolicFunction(const real_t&);
explicit SymbolicFunction(const complex_t&);
SymbolicFunction(const SymbolicFunction&, SymbolicOperation, 
                 complex_t c=1.,complex_t p=0.);
SymbolicFunction(const SymbolicFunction&, const SymbolicFunction&, 
                 SymbolicOperation, complex_t c=1., complex_t p=0.);
SymbolicFunction(const SymbolicFunction&);
~SymbolicFunction();
SymbolicFunction& operator=(const SymbolicFunction&);
\end{lstlisting}  
\vspace{.2cm} 
The copy constructor and the assign operator do full copy of object referenced by pointers \var{fn1} and \var{fn2}. So, the destructor deletes them if they are assigned. Note that the variable \var{coef} and \var{par} are complex. Obviously, they may store some real scalars and the code interprets the imaginary part to know if they are real.\\

In order to make easier the construction of \class{SymbolicFunction} object, a lot of function or operator are provided, for instance:
\vspace{.1cm} 
\begin{lstlisting}
//binary operation on SymbolicFunction's
SymbolicFunction& operator+ (const SymbolicFunction&, const SymbolicFunction&);
SymbolicFunction& operator- (const SymbolicFunction&, const SymbolicFunction&);
SymbolicFunction& operator* (const SymbolicFunction&, const SymbolicFunction&);
SymbolicFunction& operator/ (const SymbolicFunction&, const SymbolicFunction&);
SymbolicFunction& operator^ (const SymbolicFunction&, const SymbolicFunction&);
SymbolicFunction& operator==(const SymbolicFunction&, const SymbolicFunction&);
SymbolicFunction& operator!=(const SymbolicFunction&, const SymbolicFunction&);
SymbolicFunction& operator> (const SymbolicFunction&, const SymbolicFunction&);
SymbolicFunction& operator>=(const SymbolicFunction&, const SymbolicFunction&);
SymbolicFunction& operator< (const SymbolicFunction&, const SymbolicFunction&);
SymbolicFunction& operator<=(const SymbolicFunction&, const SymbolicFunction&);
SymbolicFunction& operator&&(const SymbolicFunction&, const SymbolicFunction&);
SymbolicFunction& operator||(const SymbolicFunction&, const SymbolicFunction&);
\end{lstlisting} 
\vspace{.1cm} 
\begin{lstlisting}
//unary operation on SymbolicFunction
SymbolicFunction& operator-(const SymbolicFunction&);
SymbolicFunction& operator+(const SymbolicFunction&);
SymbolicFunction& operator!(const SymbolicFunction&);
SymbolicFunction& abs(const SymbolicFunction&);
SymbolicFunction& real(const SymbolicFunction&);
SymbolicFunction& imag(const SymbolicFunction&);
SymbolicFunction& sqrt(const SymbolicFunction&);
SymbolicFunction& squared(const SymbolicFunction&);
SymbolicFunction& sin(const SymbolicFunction&);
SymbolicFunction& cos(const SymbolicFunction&);
SymbolicFunction& tan(const SymbolicFunction&);
SymbolicFunction& asin(const SymbolicFunction&);
SymbolicFunction& acos(const SymbolicFunction&);
SymbolicFunction& atan(const SymbolicFunction&);
SymbolicFunction& sinh(const SymbolicFunction&);
SymbolicFunction& cosh(const SymbolicFunction&);
SymbolicFunction& tanh(const SymbolicFunction&);
SymbolicFunction& exp(const SymbolicFunction&);
SymbolicFunction& log(const SymbolicFunction&);
SymbolicFunction& log10(const SymbolicFunction&);
SymbolicFunction& pow(const SymbolicFunction&, const double&);
#if __cplusplus > 199711L
SymbolicFunction& asinh(const SymbolicFunction&);
SymbolicFunction& acosh(const SymbolicFunction&);
SymbolicFunction& atanh(const SymbolicFunction&);
#endif
\end{lstlisting}  
The binary operators are also provided for \class{SymbolicFunction} object and constant (real or complex).\\

Besides, some basic tools are proposed by the class:
\vspace{.1cm} 
\begin{lstlisting}
bool isSingle() const;  //true if a constant or variable
bool isConst() const;   //true if a constant expression
set<VariableName> listOfVar() const; //list of involved variables
number_t numberOfVar()const; //number of involved variables
void print(ostream& = cout) const;  
void printTree(ostream& =cout, int lev=0) const; 
\end{lstlisting}  
\vspace{.2cm} 
Finally, by overloading the operator (), the symbolic function can be evaluated for any set of real values or complex values:
\vspace{.1cm} 
\begin{lstlisting}
complex_t operator()(const vector<complex_t>& xs) const;
complex_t operator()(const complex_t& x1) const;
complex_t operator()(const complex_t& x1, const complex_t& x2) const;
complex_t operator()(const complex_t& x1, const complex_t& x2, 
                     const complex_t& x3) const;
real_t operator()(const vector<real_t>& xs) const;
real_t operator()(const real_t& x1) const;
real_t operator()(const real_t& x1, const real_t& x2) const;
real_t operator()(const real_t& x1, const real_t& x2, const real_t& x3) const;
\end{lstlisting}
\vspace{.1cm}
These member functions use the fundamental functions that performs computations:
\vspace{.1cm}
\begin{lstlisting}
inline real_t evalOp(SymbolicOperation op,const real_t& x,const real_t& y);
inline real_t evalFun(SymbolicOperation op,const real_t& x,const real_t& p=0.);
inline complex_t evalOp(SymbolicOperation op,const complex_t& x, 
                        const complex_t& y);
inline complex_t evalFun(SymbolicOperation op,const complex_t& z, 
                         const complex_t& p=0.);
\end{lstlisting}
\vspace{.2cm}
Hereafter, some examples of construction of \class{SymbolicFunction} objects:
\vspace{.1cm}
\begin{lstlisting}
SymbolicFunction fs;
fs = x_1+x_2-1; 
fs = 2*x_1-3*x_2; 
fs = 5*(2*x_1-3*x_2);
fs = 4*sqrt(2*x_1-3*x_2)/x_2;  
fs = sin(cos(sqrt(abs(log(x_1*x_1+x_2*x_2+x_3*x_3)))));
fs = (1.-sin(x_1))*(cos(1.+x_1)/5 + 2*sin(3*x_1));
fs = pow(squared(x_1), 0.5);
fs = SymbolicFunction(3);
fs = -sin(x_1);
fs = +sin(-x_1);
fs = cos(x_2)-2*sqrt(x_1*x_2);
fs = x_1 < 1;
fs = (x_1+x_2)<=1 && exp(x_1)>2;
fs = (x_1+x_2)^3;
fs = (1-r_*cos(2.*theta_))^3;
\end{lstlisting}

\begin{focusbox}
When evaluating a symbolic function involving polar or spherical variables (say {\var r\_}, {\var theta\_} or {\var phi\_}), cartesian coordinates $(x,y,z)$ must be passed; $r$, $\theta$ and $\phi$ will be evaluated as follows:
$$r=|(x,y,z)|,\quad \theta=\text{atan2}(y,x),\quad \phi=\text{asin}(z/r).$$
\begin{lstlisting}
 SymbolicFunction fs= (1-r_*cos(2.*theta_))^3;
 cout<<fs(0.5,0.5);  // gives 1 (r=0.5 and theta=pi/4)
\end{lstlisting}
\end{focusbox}
\displayInfos{library=utils, header=SymbolicFunction.hpp, implementation=SymbolicFunction.cpp, test=unit\_SymbolicFunction.cpp,
header dep={config.h, Messages.hpp}}
