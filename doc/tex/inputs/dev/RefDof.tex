%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{RefDof} class}

The \class{RefDof} class is a class representing a generalized degree of freedom in a reference
element.

It is defined by:

\begin{itemize}
\item A support that can be a point, a side, a side of side or the whole element. When it is a point, we also have its coordinates.
\item A dimension, that means the number of components of shape functions of the DoF.
\item Projections data (type and direction) when the DoF is a projection DoF. The projection
type is 0 for a general projection, 1 for a  \(u\dot n\) DoF, 2 for a \(u\times n\) DoF.
\item Derivative data (order and direction) when the DoF is a derivative DoF. The order is
0 when the DoF is defined by an 'integral' over its support, or \(n\) for a Hermite DoF derivative
of order \(n>0\) or a moment DoF of order \(n>0\).
\item A shareable property: A DoF is shareable when it is shared between adjacent elements according
to space conformity. Such a DoF can be shared or not according to interpolation. For example,
every DoFs of a Lagrange H1-confirming element on a side is shared, while DoFs in discontinuous
interpolation are not.
\end{itemize}

\begin{lstlisting}
class RefDof
{
 private:
 bool sharable_;              //D.o.F is shared according to space conformity
 DofLocalization where_;      //hierarchic localization of DoF
 number_t supportNum_;        //support localization
 number_t index_;             //rank of the DoF in its localization
 dimen_t supportDim_;         //dimension of the D.o.F geometric support
 number_t nodeNum_;           //node number when a point DoF
 dimen_t dim_;                //number of components of shape functions of D.o.F
 std::vector<real_t> coords_; //coordinates of D.o.F support if supportDim_=0
 number_t order_;             //order of a derivative D.o.F or a moment D.o.F
 std::vector<real_t> derivativeVector_; //direction vector(s) of a derivative
 ProjectionType projectionType_;        //type of projection
 std::vector<real_t> projectionVector_; //direction vector(s) of a projection
 DiffOpType diffop_;                    //DoF differential operator (_id,_dx,_dy, \ldots)
 string_t name_;                        //D.o.F-type name for documentation
\end{lstlisting}
\vspace{.2cm}
This class proposes mainly a full constructor and accessors to member data.\\
\begin{focusbox}
    For non-nodal DoFs (\verb?supportDim_=0?), virtual coordinates are also defined. They are useful when dealing with essential conditions.
\end{focusbox}
\begin{focusbox}
    Up to now, derivative vectors are not used and the normal vector involved in \(n.\nabla\) DoFs is stored in the projection vector.
\end{focusbox}

In order to sort DoFs efficiently, the following class is offered:
\begin{lstlisting}[deletekeywords={[3] where}]
class DofKey
{
 public:
 DofLocalization where;
 number_t v1, v2;           //used for vertex, edge, element DoFs
 std::vector<number_t> vs;  //used for face DoFs
 number_t locIndex;         //local DoF index
 }
\end{lstlisting}
\vspace{.2cm} 
associated to the comparison operator:
\begin{lstlisting}
bool operator<(const DofKey& k1, const DofKey& k2);
\end{lstlisting}
\vspace{.2cm} 
  
 
\displayInfos{library=finiteElements, header=RefDof.hpp, implementation=RefDof.cpp, test={test\_segment.cpp,
test\_triangle.cpp, test\_quadrangle.cpp, test\_tetrahedron.cpp, test\_hexahedron.cpp, test\_prism.cpp},
header dep={config.h, utils.h}}

