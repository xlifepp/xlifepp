%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{OperatorOnKernel} class}

Kernel objects describe functions of two points, say \(K(x,y)\). The \class{OperatorOnKernel} class is intended to store the 
differential operators acting on Kernel, one for \(x\) derivatives and one for \(y\) derivatives :
\vspace{.1cm}
\begin{lstlisting}
class OperatorOnKernel
{
  protected :
    const Kernel* ker_p;            //Kernel involved in operator
    DifferentialOperator* xdifOp_p; //x differential operator involved in operator
    DifferentialOperator* ydifOp_p; //v differential operator involved in operator
    ValueType type_;                //type of returned value ( _real, _complex)
    StrucType struct_;              //structure of returned value (_scalar, _vector, _matrix)
	}
\end{lstlisting}
\vspace{.1cm}
A null pointer \emph{ker\_p} means the kernel function \(K(x,y)=1\) and by default \emph{xdifOp\_p} and \emph{ydifOp\_p} point to identity differential operator.\\
\vspace{.2cm}
\class{OperatorOnKernel} object are constructed either by constructor members or by external functions: 
\vspace{.1cm}
\begin{lstlisting}
OperatorOnKernel();
OperatorOnKernel(const Kernel*, DiffOpType = _id, DiffOpType = _id, ValueType vt=_real, StrucType st=_scalar); 
OperatorOnKernel(const Kernel&, DiffOpType = _id, DiffOpType = _id);
OperatorOnKernel(const OperatorOnKernel&);
OperatorOnKernel& operator=(const OperatorOnKernel&);

OperatorOnKernel& id(const Kernel&);                           //id(k)
OperatorOnKernel& grad_x(const Kernel&);                       //grad_x(k)
OperatorOnKernel& grad_y(const Kernel&);                       //grad_y(k)
\ldots
OperatorOnKernel& id(OperatorOnKernel&);                       //id(opk)
OperatorOnKernel& grad_x(OperatorOnKernel&);                   //grad_x(opk)
OperatorOnKernel& grad_y(OperatorOnKernel&);                   //grad_y(opk)
\ldots
OperatorOnKernel& operator*(UnitaryVector, const Kernel&);     //n*ker 
OperatorOnKernel& operator*(const Kernel&, UnitaryVector);     //ker*n 
OperatorOnKernel& operator|(UnitaryVector, const Kernel&);     //n|ker 
OperatorOnKernel& operator|(const Kernel&, UnitaryVector);     //ker|n 
OperatorOnKernel& operator^(UnitaryVector, const Kernel&);     //n^ker 
OperatorOnKernel& operator*(UnitaryVector, OperatorOnKernel&); //n*opker
OperatorOnKernel& operator*(OperatorOnKernel&, UnitaryVector); //opker*n
OperatorOnKernel& operator|(UnitaryVector, OperatorOnKernel&); //n|opker
OperatorOnKernel& operator|(OperatorOnKernel&, UnitaryVector); //opker|n
OperatorOnKernel& operator^(UnitaryVector, OperatorOnKernel&); //n^opker
\end{lstlisting}
\vspace{.1cm}
Note that operators return reference to object dynamically allocated.\\
\vspace{.1cm}
The class provides some member accessors and property accessors:
\vspace{.1cm}
\begin{lstlisting}
const Kernel* kernel() const;
DiffOpType xdifOpType() const;
DiffOpType ydifOpType() const;
DifferentialOperator*& xdifOp_();
DifferentialOperator& xdifOp() const;
DifferentialOperator*& ydifOp_();
DifferentialOperator& ydifOp() const;
ValueType& valueType();
ValueType valueType()const;
StrucType& strucType();
StrucType strucType() const;
Dimen xdiffOrder() const;
Dimen ydiffOrder() const;
bool voidKernel() const;
\end{lstlisting}
\vspace{.3cm}
and print facilities
\vspace{.1cm}
\begin{lstlisting}
void print(std::ostream&) const;                          
friend std::ostream& operator<<(std::ostream& os, const OperatorOnKernel& opk);
\end{lstlisting}
\vspace{.3cm}
Finally the class interfaces the computation of \class{OperatorOnKernel} at a couple of points or a list of couples of points using template functions: 
\vspace{.1cm}
\begin{lstlisting}
template <typename T>
T& eval(const Point&,const Point&, T&) const;              
template <typename T>
std::vector<T>& eval(const std::vector<Point>&,const std::vector<Point>&, std::vector<T>&) const;              
\end{lstlisting}
\vspace{.3cm}

\displayInfos{library=operator, header=OperatorOnKernel.hpp, implementation=OperatorOnKernbel.cpp,
test=test\_operator.cpp, header dep={DifferentialOperator.hpp, Operand.hpp, config.h, utils.h}}

