%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{TermMatrix} class}

The \class{TermMatrix} class carries numerical representation of a bilinear form  and more generally any matrix attached to a pair of unknowns. It follows a similar organization to \class{TermVector} class but manages row and column information:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] map}]
class TermMatrix : public Term
{protected :
BilinearForm bilinForm_;                   //bilinear form if exists
map<uvPair, SuTermMatrix*> suTerms_;  //list of SuTermMatrix                    
MatrixEntry* scalar_entries_p;             //scalar entries representation      
vector<DofComponent> cDoFs_c;         //column scalar DoFs
vector<DofComponent> cDoFs_r;         //row scalar DoFs
SetOfConstraints* constraints_u_p;         //constraints to apply to row  
SetOfConstraints* constraints_v_p;         //constraints to apply to column 
MatrixEntry* rhs_matrix_p;      //matrix used by right-hand side correction
}                        
\end{lstlisting}
\vspace{.3cm}
The bilinear form is a multiple unknown pairs form, may be reduced to a single unknown pair form (see \lib{form} library). This bilinear form is void if \class{TermMatrix} is not explicitly construct from a bilinear form (linear combination, nodal construction, \ldots). In particular, this bilinear form is not updated when \class{TermMatrix} objects are combined. \\
The map \verb? suTerms_? carries each matrix block related to a pair of unknowns. When there is only one element in map, it means that the \class{TermMatrix} is actually a single unknown pair term matrix. \\
As block representation may be too restrictive in certain cases, it is possible to create its non block representation in \emph{scalar\_entries\_p} (\class{MatrixEntry} pointer). The \class{MatrixEntry} object created has to be of real scalar or complex scalar type. In that case, non block row and non block column numbering, consisting in vectors of \class{DofComponent} (Unknown pointer, DoF number, component number), are also defined (\verb?cDoFs_r? and \verb?cDoFs_c?).\\

Essential conditions (conditions on space functions) induce special treatments in computation of \class{TermMatrix}. It is the reason why they are handled with  
\class{SetOfConstraints} objects which are the algebraic representations of essential conditions (see \lib{essentialConditions} for details). \class{SetOfConstraints} on row may differ from  \class{SetOfConstraints} on column (it is often the same). In case of non-homogeneous essential condition, the part of matrix that is eliminated contributes to the right-hand side; this part is stored into \verb?rhs_matrix_p? \class{MatrixEntry} pointer. Obviously, the pointers \verb?constraints_u_p, constraints_v_p, rhs_matrix_p? are null pointer whenever no essential conditions are imposed.\\

The \class{TermMatrix} class provides useful constructors and assign operators designed for end users:
\vspace{.1cm}
\begin{lstlisting}
TermMatrix(const string_t & na="?");                                                     

//constructors with no essential boundary conditions and options
TermMatrix(const BilinearForm&, const string_t& na="");
TermMatrix(const BilinearForm&, TermOption, const string_t& na="");
TermMatrix(const BilinearForm&, TermOption, TermOption, const string_t& na="");
\ldots
//constructor with one essential boundary condition (on u and v)
TermMatrix(const BilinearForm&, const EssentialConditions&, const string_t& na="");
TermMatrix(const BilinearForm&, const EssentialConditions&, TermOption, 
           const string_t& na="");
\ldots
//constructor with one essential boundary condition and explicit reduction method
TermMatrix(const BilinearForm&, const EssentialConditions&, const ReductionMethod&, 
           const string_t& na="");
TermMatrix(const BilinearForm&, const EssentialConditions&, const ReductionMethod&, 
           TermOption, const string_t& na="");
\ldots
//constructor with  two essential boundary conditions (on u and v)
TermMatrix(const BilinearForm&, const EssentialConditions&, 
           const EssentialConditions&, const string_t& na="");
TermMatrix(const BilinearForm&, const EssentialConditions&, 
           const EssentialConditions&, TermOption, const string_t& na="");
\ldots
//constructor with two essential boundary conditions and explicit reduction method
TermMatrix(const BilinearForm&, const EssentialConditions&, const EssentialConditions&, 
           const ReductionMethod&, const string_t& na="");
TermMatrix(const BilinearForm&, const EssentialConditions&, const EssentialConditions&,
           const ReductionMethod&, TermOption, const string_t& na="");
\ldots
//effective constructor
void build(const BilinearForm& blf, const EssentialConditions* ecu, 
           const EssentialConditions* ecv, const vector<TermOption>&, 
           const string_t& na);                                

//copy constructors
TermMatrix(const TermMatrix &, const string_t& na=""); 
// diagonal TermMatrix from a TermVector or a vector
TermMatrix(const TermVector &, const string_t& na=""); 
template<typename T>
TermMatrix(const Unknown&, const GeomDomain&, const Vector<T> &, 
           const string_t& na="");   
//TermMatrix from a SuTermMatrix                    
TermMatrix(const SuTermMatrix &, const string_t& na=""); //full copy
TermMatrix(SuTermMatrix *, const string_t& na="");       //pointer copy
//TermMatrix of the form opker(xi,yj)                       
TermMatrix(const Unknown&, const GeomDomain&, const Unknown&, 
           const GeomDomain&, const OperatorOnKernel&, 
           const string_t& na="");
//TermMatrix from a row dense matrix (Matrix)
TermMatrix(const Matrix<T>& mat,const string_t& ="");                                  
TermMatrix(const vector<T>& mat, number_t m, number_t n=0, const string_t& ="");   
void buildFromRowDenseMatrix(const vector<T>&, number_t, number_t, const string_t&);
// TermMatrix from a linear combination                  
TermMatrix(const LcTerm&,const string_t& na="");   
            
//assign operator
TermMatrix& operator=(const TermMatrix&); 
TermMatrix& operator=(const LcTerm&);   
\end{lstlisting}
\vspace{.2cm}
The \class{ReductionMethod} class allows the user to specify the reduction method he wants, one of the enumeration \class{ReductionMethodType}: {\itshape \_pseudoReduction, \_realReduction, \_penalizationReduction, \_dualReduction}. It is also possible to provide an additional real/complex parameter (say alpha) : the diagonal coefficient of the pseudo-eliminated part or the penalization coefficient.
\vspace{.1cm}
\begin{lstlisting}    
ReductionMethodType method;  //type of reduction method
complex_t alpha;            //diagonal or penalization coefficient
ReductionMethod(ReductionMethodType= _noReduction, const complex_t& = 1.);
void print(ostream& out) const;
friend :ostream& operator << (ostream&, const ReductionMethod&);                                  
\end{lstlisting}

\begin{focusbox}
When reduction method is not specified, the pseudo reduction with 1 as diagonal coefficient is used. {\bfseries Only the pseudo reduction method is available!}
\end{focusbox}

\begin{focusbox}
When TermMatrix is constructed, it is automatically computed except if it has been set not to be computed, using the \class{TermOption} \textit{\_notCompute} in its construction.
\end{focusbox}

Developers may use other construction/destruction stuff:
\vspace{.1cm}
\begin{lstlisting}    
void initFromBlf(const BilinearForm &, const String& na ="",
                 bool noass = false);
void initTermVector(TermVector&, ValueType, bool col=true);    
template<typename T>
void initTermVectorT(TermVector&,const T&, bool col=true);                                                                                  
void insert(const SuTermMatrix &);                                   
void insert(SuTermMatrix *); 
void copy(const TermMatrix&);      
void clear();                                
virtual ~TermMatrix();
                                  
\end{lstlisting}
\vspace{.2cm}
This class provides a lot of accessors and properties (shortcuts to other class accessors):
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] set}]
bool isSingleUnknown() const;
set<const Unknown*> rowUnknowns() const;
set<const Unknown*> colUnknowns() const;
Number nbTerms() const;
ValueType valueType() const;   
AccessType storageAccess() const;  
void setStorage(StorageType, AccessType);                             
StorageType storageType() const                                     
bool hasConstraints() const;
FactorizationType factorization() const;
SymType globalSymmetry() const;
TermMatrix operator()(const Unknown&,const Unknown&) const;
 
//stuff reserved to developers
cit_mustm begin() const;
it_mustm begin();
cit_mustm end() const;
it_mustm end();
map<uvPair, SuTermMatrix*>& suTerms();
MatrixEntry*& scalar_entries();
const MatrixEntry* scalar_entries() const;
MatrixEntry*& rhs_matrix();
const MatrixEntry* rhs_matrix() const;
const vector<DofComponent>& cDoFsr() const;
const vector<DofComponent>& cDoFsc() const;
vector<DofComponent>& cDoFsr();
vector<DofComponent>& cDoFsc();                                                   
SuTermMatrix& subMatrix(const Unknown*,const Unknown*);
const SuTermMatrix& subMatrix(const Unknown*,const Unknown*) const;  
SuTermMatrix* subMatrix_p(const Unknown*,const Unknown*);           
const SuTermMatrix* subMatrix_p(const Unknown*,const Unknown*) const; 

TermVector& getRowCol(number_t, AccessType, TermVector&) const;       
TermVector row(number_t) const;                                      
TermVector column(number_t) const;                               
template <typename T>
LargeMatrix<T>& getLargeMatrix(StrucType =_undefStrucType) const;    
template <typename T>
HMatrix<T,FeDof>& getHMatrix(StrucType =_undefStrucType) const;      
\end{lstlisting}

\begin{focusbox}
The access \cmd{operator} returns a \class{SuTermMatrix} as a \class{TermMatrix} (obviously a single unknown one). Note that it is a full copy. To avoid copy, use \cmd{subMatrix} member functions. 
\end{focusbox}

\verb?it_mustm? and \verb?cit_mustm? are iterator aliases:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] map}]
typedef map<uvPair, SuTermMatrix*>::iterator it_mustm;
typedef map<uvPair, SuTermMatrix*>::const_iterator cit_mustm;
\end{lstlisting}
\vspace{.2cm}
It is also possible to get row or column (index starts from 1) of the TermMatrix if it is a single unknown one and to access to the LargeMatrix or the HMatrix object that really stores the matrix values:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] set}]
TermVector& getRowCol(number_t, AccessType, TermVector&) const;       
TermVector row(number_t) const;                                      
TermVector column(number_t) const;                               
template <typename T>
LargeMatrix<T>& getLargeMatrix(StrucType =_undefStrucType) const;    
template <typename T>
HMatrix<T,FeDof>& getHMatrix(StrucType =_undefStrucType) const;      
\end{lstlisting}

\begin{focusbox}
The \cmd{setStorage} function allows the user to impose the matrix storage.
\end{focusbox}

The class proposes to users interfaces to computation algorithms (FE computation and algebraic operations):

\begin{lstlisting}
void compute(); 
void compute(const LcTerm&,const String& na=""); 

template<typename T> TermMatrix& operator*=(const T&);     
template<typename T> TermMatrix& operator/=(const T&);                
TermMatrix& operator+=(const TermMatrix&);
TermMatrix& operator-=(const TermMatrix&);
friend TermVector& multMatrixVector(const TermMatrix&,const TermVector&, TermVector&);  
friend TermVector& multVectorMatrix(const TermMatrix&, const TermVector&, TermVector&);
friend TermVector operator*(const TermMatrix&,const TermVector&);  
friend TermVector operator*(const TermVector&, const TermMatrix&);    
friend TermMatrix operator*(const TermMatrix&, const TermMatrix&);                                                  
\end{lstlisting}

The computation of  \class{TermMatrix} is a quite intricate process in case of essential conditions to apply to bilinear form. The implementation of \verb?compute()? looks like (some details are omitted):
\vspace{.1cm}
\begin{lstlisting}
void TermMatrix::compute()
{
  //compute suterms
  for(it_mustm it = suTerms_.begin(); it != suTerms_.end(); it++) 
      it->second->compute();
  if(constraints_u_p==0 && constraints_v_p==0) {computed() = true; return;}
  //deal with essential conditions
  bool global=false;
  if(constraints_u_p!=0) global=constraints_u_p->isGlobal();
  if(constraints_v_p!=0 && !global) global=constraints_u_p->isGlobal();
  if(global) toGlobal(_noStorage, _noAccess, _noSymmetry, false);   
  else toScalar();       
  switch(computingInfo_.elimination)
    {
      case _noElimination             :  break;
      case _pseudoElimination         :  pseudoElimination(); break;
    }
  computed() = true;
}
\end{lstlisting}
\vspace{.2cm}
The principle of pseudo-elimination consists in combining some rows and columns, and "eliminating" some rows and columns (in fact set to 0 except diagonal coefficient). Two cases are to be considered:
\begin{itemize}
\item case of essential conditions which do not couple unknowns
\item case of essential conditions which couples unknowns (global constraints)
\end{itemize}
In the first case, the pseudo-elimination process may be performed on each  matrix of \class{SuTermMatrix} involving unknowns concerned by essential conditions whereas in the second case, the process must be performed and the whole matrix of \class{TermMatrix}. If no such representation exists, we have to create it (\verb?toGlobal? function). See the \verb?pseudoElimination()? function.\\
\\

This class offers many interfaces to factorization tools and direct solvers :  
\vspace{.1cm}
\begin{lstlisting}
//developers stuff
void initTermVector(TermVector&, ValueType, bool col=true);
template<typename T>
void initTermVectorT(TermVector&, const T&, bool col=true);  

void toScalar(bool keepEntries=false);
void toGlobal(StorageType, AccessType, SymType=_noSymmetry, bool keepSuTerms=false);
void toSkyline(); 
void mergeNumbering(map<const Unknown*, list<SuTermMatrix* > >&, map<SuTermMatrix*, vector<Number > >&, vector<DofComponent>&, AccessType); 
void addIndices(vector<set<Number> >&, MatrixStorage*, const vector<Number>&, const vector<Number>&); 
void mergeBlocks();
friend TermVector prepareLinearSystem(TermMatrix&, TermVector&, MatrixEntry*&, VectorEntry*&, bool toScal=false);          
void pseudoElimination();     

//direct solver member functions
void ldltSolve(const TermVector&, TermVector&);                     
void ldlstarSolve(const TermVector&,TermVector&);                   
void luSolve(const TermVector&,TermVector&);                        
void umfpackSolve(const TermVector&,TermVector&);                
void sorSolve(const TermVector& V, TermVector& R, const Real w, SorSolverType sType );
void sorUpperSolve(const TermVector& V, TermVector& R, const Real w );
void sorDiagonalMatrixVector(const TermVector& V, TermVector& R, const Real w );
void sorLowerSolve(const TermVector& V, TermVector& R, const Real w );
void sorDiagonalSolve(const TermVector& V, TermVector& R, const Real w );
      
\end{lstlisting}
\vspace{.2cm}
End users have to use the following external functions:
\vspace{.1cm}
\begin{lstlisting}
//factorization tool
void ldltFactorize(TermMatrix&, TermMatrix&);                  
void ldlstarFactorize(TermMatrix&, TermMatrix&);              
void luFactorize(TermMatrix&, TermMatrix&);                   
void umfpackFactorize(TermMatrix&, TermMatrix&);               
void factorize(TermMatrix&, TermMatrix&, FactorizationType ft=_noFactorization);   

//direct solver (one right-hand side)
TermVector factSolve(TermMatrix&, const TermVector&);         
TermVector ldltSolve(TermMatrix&, const TermVector&);          
TermVector ldlstarSolve(TermMatrix&, const TermVector&);       
TermVector luSolve(TermMatrix&, const TermVector&);           
TermVector gaussSolve(TermMatrix&, const TermVector&, bool keepA = false); 
TermVector umfpackSolve(TermMatrix&, const TermVector&, bool keepA = false);
TermVector magmaSolve(TermMatrix& A, const TermVector& B, bool useGPU=false, 
                      bool keepA=false); 
TermVector lapackSolve(TermMatrix& A, const TermVector& B, bool keepA=false);
TermVector directSolve(TermMatrix&, const TermVector&, bool keepA = false); 
TermVector schurSolve(TermMatrix&, const TermVector&, const Unknown&, const Unknown&, 
                      bool keepA = false); 

//direct solver (few right-hand sides)
TermVectors factSolve(TermMatrix&, const vector<TermVector>&);   
TermVectors ldltSolve(TermMatrix&, const vector<TermVector>&, TermMatrix&); 
TermVectors ldlstarSolve(TermMatrix&, const vector<TermVector>&, TermMatrix&);
TermVectors luSolve(TermMatrix&, const vector<TermVector>&, TermMatrix&); 
TermVectors gaussSolve(TermMatrix&, const vector<TermVector>&,bool keepA = false);   
TermVectors umfpackSolve(TermMatrix&, const vector<TermVector>&,bool keepA = false);
TermVectors directSolve(TermMatrix&, const vector<TermVector>&,bool keepA = false);

// direct solver (TermMatrix right-hand side)
TermMatrix factSolve(TermMatrix&, TermMatrix&);                    
TermMatrix directSolve(TermMatrix&, TermMatrix&, KeepStatus=_keep); 
TermMatrix inverse(TermMatrix&);                                   
\end{lstlisting}
\vspace{.2cm}
\cmd{factorize}, \cmd{factSolve} and \cmd{directSolve} functions are general functions that determine automatically the adapted methods to apply.\\

\begin{warningbox}
To use \cmd{umfpackSolve}, \cmd{magmaSolve} or \cmd{lapackSolve}, libraries \umfpack, \magma, \lapack-\blas have to be installed and activated. \cmd{directSolve} chooses \umfpack solver when matrix is sparse and \umfpack available, and \lapack solver when matrix is dense and \lapack available. \umfpack solver is always faster than \xlifepp solvers, but \lapack solver may be slower than the \xlifepp Gauss solver if non optimized \lapack-\blas libraries are used.
\end{warningbox}

Note that the \cmd{factSolve} and \cmd{directSolve} functions can use a \class{TermMatrix} as right-hand side. In that case, they compute \(\mathbb{A}^{-1}\mathbb{B}\) using a factorization of \(\mathbb{A}\). The  \cmd{inverse} function computes the inverse of a \class{TermMatrix} using the \cmd{directSolve} function with the identity matrix as right-hand side. These functions provide some \class{TermMatrix} that are stored as column dense storage. So be care with memory usage!\\

\begin{focusbox}
Up to now, the usage of \cmd{factSolve}, \cmd{directSolve} functions with a \class{TermMatrix} as right-hand side and \cmd{inverse} is restricted to single unknown \class{TermMatrix}.
\end{focusbox}

In a same way, interfaces to various iterative solvers are provided :
\vspace{.1cm}
\begin{lstlisting}
//! general iterative solver
TermVector iterativeSolveGen(IterativeSolverType isType, TermMatrix& A,  
       TermVector& B, const TermVector& X0, const PreconditionerTerm& P, 
       const real_t tol, const number_t iterMax, const real_t omega,
       const number_t krylovDim, const number_t verboseLevel);
//! general front end for iterativeSolve
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, 
      const PreconditionerTerm& P, const vector<Parameter>& ps);

//! user front-end for iterative solver
inline TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const PreconditionerTerm& P, 0 to 5 Parameter);
inline TermVector iterativeSolve(TermMatrix& A, TermVector& B, const PreconditionerTerm& P, 0 to 6 Parameter);
inline TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, 0 to 6 Parameter);
inline TermVector iterativeSolve(TermMatrix& A, TermVector& B, 0 to 6 Parameter);

//! user front-end for BiCG solver
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const PreconditionerTerm& P, 0 to 4 Parameter);
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const PreconditionerTerm& P, 0 to 4 Parameter);
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, 0 to 4 Parameter);
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, 0 to 4 Parameter);

//! user front-end for BiCGStab solver
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const PreconditionerTerm& P, 0 to 4 Parameter);
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const PreconditionerTerm& P, 0 to 4 Parameter);
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, 0 to 4 Parameter);
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, 0 to 4 Parameter);

//! user front-end for CG solver
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const PreconditionerTerm& P, 0 to 4 Parameter);
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const PreconditionerTerm& P, 0 to 4 Parameter);
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, 0 to 4 Parameter);
inline TermVector cgSolve(TermMatrix& A, TermVector& B, 0 to 4 Parameter);

//! user front-end for CGS solver
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const PreconditionerTerm& P, 0 to 4 Parameter);
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const PreconditionerTerm& P, 0 to 4 Parameter);
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, 0 to 4 Parameter);
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, 0 to 4 Parameter);

//! user front-end for GMRes solver
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const PreconditionerTerm& P, 0 to 4 Parameter);
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const PreconditionerTerm& P, 0 to 4 Parameter);
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, 0 to 4 Parameter);
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, 0 to 4 Parameter);

//! user front-end for QMR solver
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const PreconditionerTerm& P, 0 to 4 Parameter);
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const PreconditionerTerm& P, 0 to 4 Parameter);
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, 0 to 4 Parameter);
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, 0 to 4 Parameter);
\end{lstlisting}
\vspace{.2cm}
\cmd{iterativeSolve(\ldots)} are the front end of all other iterative methods.\\
\cmd{iterativeSolveGen(\ldots)} are the front end of all iterative methods.\\

To compute eigenvalues, internal solvers or interface to \arpack solvers are available:

\vspace{.1cm}
\begin{lstlisting}
//! general eigen solver
EigenElements eigenSolveGen(TermMatrix* pA, TermMatrix* pB, number_t nev, string_t which, real_t tol, EigenComputationalMode eigCompMode,
                            complex_t sigma, const char mode, EigenSolverType solver);
//! general eigen solver
EigenElements eigenInternGen(TermMatrix* pA, TermMatrix* pB, number_t nev, string_t which, real_t tol, EigenComputationalMode eigCompMode,
                            complex_t sigma, bool isShift);

//! general front-end for generalized eigen solver
EigenElements eigenSolve(TermMatrix& A, TermMatrix& B, vector<Parameter> ps);

//! user front-end for eigen solver
EigenElements eigenSolve(TermMatrix& A, TermMatrix& B, 0 to 10 Parameter);
EigenElements eigenSolve(TermMatrix& A, 0 to 10 Parameter);

//! user front-end for internal eigen solver
inline EigenElements eigenInternSolve(TermMatrix& A, TermMatrix& B, 0 to 9 Parameter);
inline EigenElements eigenInternSolve(TermMatrix& A, 0 to 9 Parameter);

//! user front-end for external Arpack eigen solver
inline EigenElements arpackSolve(TermMatrix& A, TermMatrix& B, 0 to 9 Parameter);
inline EigenElements arpackSolve(TermMatrix& A, 0 to 9 Parameter);
\end{lstlisting}
\vspace{.2cm}
The \class{EigenElements} class handles the list of eigenvalues and the list of eigenvectors. See the \class{TermVector} section.\\

\begin{warningbox}
\arpack solvers are available only if \arpack library is installed.
\end{warningbox}

When addressing integral representations, say 
\[r(x)=\int_{\Sigma}op(K)(x,y)*op(u)(y)\,dy\]
one can be interested in the matrix with coefficients
\[r_{ij}=\int_{\Sigma}op(K)(x_i,y)*op(w_j)(y)\,dy\]
where \((x_i)_i\) are some points and \((w_j)_j\) some shape functions related to the unknown \(u\).
Special functions, named \var{integralFunction} will produce such matrices embedded in a \class{TermMatrix} object:
\begin{lstlisting}
TermMatrix integralRepresentation(const Unknown&, const GeomDomain&, 
                                  const LinearForm&); 
TermMatrix integralRepresentation(const GeomDomain&, const LinearForm&);                 
TermMatrix integralRepresentation(const vector<Point>&, const LinearForm&);         
\end{lstlisting}
When no domain is explicitly passed, one shadow \class{PointsDomain} object will be created from the list of points given. When no unknown is explicitly  passed, one (minimal) space and related shadow unknown will be created to represent the row unknown.\\
These functions call the fundamental computation function:
\begin{lstlisting}
template<typename T, typename K>
void SuTermMatrix::computeIR(const SuLinearForm&f, T*, K&, const vector<Point>& xs);    
\end{lstlisting}
\vspace{2mm}
Once a \class{TermMatrix} is computed, it is possible to access to one of its coefficient and to change its value. Because, \class{TermMatrix} is a block matrix indexed by unknowns, a pair of unknowns has to be specified except if it is a single unknown matrix. Row index and col index of the sub-matrix coefficient may be specified either by numbers or by DoFs:
\begin{lstlisting}
Value getValue(number_t,number_t) const;                                      
Value getValue(const Unknown&,const Unknown&,number_t,number_t) const;         
void  setValue(number_t, number_t, const Value&);                               
void  setValue(const Unknown&,const Unknown&,number_t,number_t,const Value&); 
Value getValue(const Dof& du,const Dof& dv) const;   
void  setValue(const Dof& du,const Dof& dv,const Value& val);
Value getValue(const Unknown& u,const Unknown& v,const Dof& du,const Dof& dv) const;
void  setValue(const Unknown& u,const Unknown& v, const Dof& du, const Dof& dv, const Value& val);
\end{lstlisting}
\vspace{2mm}
These functions use some \class{Value} objects that have to be real/complex scalars or real/complex matrices according to the matrix coefficient type!\\

There exists also functions that addresses scalar representation when matrix is related to vector unknown:
\begin{lstlisting}
Value getScalarValue(const Unknown&,const Unknown&,number_t,number_t,dimen_t=0, dimen_t=0) const;
void  setScalarValue(const Unknown&,const Unknown&,number_t,number_t,const Value&,dimen_t=0,dimen_t=0);
Value getScalarValue(number_t,number_t,dimen_t=0,dimen_t=0) const;  
void  setScalarValue(number_t,number_t,const Value&,dimen_t=0,dimen_t=0); 
\end{lstlisting}
\vspace{2mm}
The following function can assign a same value to rows or columns:
\begin{lstlisting}
void  setRow(const Value&,number_t r1,number_t r2); 
void  setCol(const Value&,number_t c1,number_t c2); 
void  setRow(const Unknown&,const Unknown&,const Value&,number_t r1,number_t r2); 
void  setCol(const Unknown&,const Unknown&,const Value&,number_t c1,number_t c2);  
void  setRow(const Value& val,const Dof& d);                                    
void  setCol(const Value& val,const Dof& d);
void  setRow(const Unknown& u,const Unknown& v,const Value& val,const Dof& d);
void  setCol(const Unknown& u,const Unknown& v,const Value& val,const Dof& d);
\end{lstlisting}
\vspace{2mm}
The link between DoF and row/col index is given by the following functions:
\begin{lstlisting}
number_t rowRank(const Dof&) const;
number_t colRank(const Dof&) const;  
number_t rowRank(const Unknown&, const Unknown&, const Dof&) const;  
number_t colRank(const Unknown&, const Unknown&, const Dof&) const;  
\end{lstlisting}

\begin{focusbox}
The \cmd{setValue}, \cmd{setRow}, \cmd{setCol} functions never modify the matrix storage!
\end{focusbox}

\begin{warningbox}
These functions are no longer available if the matrix has moved to its global representation and lost its block representation!
\end{warningbox}

Finally, some print and export functions are provided:
\vspace{.1cm}
\begin{lstlisting}
//as member function
void print(ostream&) const;                                      
void viewStorage(ostream&) const; 
void saveToFile(const String&, StorageType, bool=false) const;
//as non-member function
void saveToFile(const String&, const TermMatrix&, StorageType, 
                bool enc = false); 
\end{lstlisting}
\vspace{.2cm}

\displayInfos{
library=term, 
header=TermMatrix.hpp, 
implementation=TermMatrix.cpp,
test=test\_TermMatrix.cpp,
header dep={SuTermMatrix.hpp, termUtils.hpp, Term.hpp, form.h, config.h, utils.h}
}
