%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Geometric data of a reference element}

\subsection{The \classtitle{GeomRefElement} class}

The \class{GeomRefElement} object carries geometric data of a reference element: type, dimension,
number of vertices, sides and side of sides, measure, and coordinates of vertices. Furthermore,
to help to find geometric data on sides, it also carries type, vertex numbers of each side
on the first hand, and vertex numbers and relative number to parent side of each side of side
on the second hand.

\begin{lstlisting}
class GeomRefElement
{
protected:
  ShapeType shapeType_;     //element shape
  const Dimen dim_;         //element dimension
  const Number nbVertices_, nbSides_, nbSideOfSides_; //number of vertices, \ldots
  const Real measure_;                       //length or area or volume 
  vector<Real> centroid_;                    //coordinates of centroid
  vector<Real> vertices_;                    //coordinates of vertices
  vector<ShapeType> sideShapeTypes_;         //shape type of each side
  vector<vector<Number> > sideVertexNumbers_; 
  vector<vector<Number> > sideOfSideVertexNumbers_; 
  vector<vector<Number> > sideOfSideNumbers_;
		
public:
  static vector<GeomRefElement*> theGeomRefElements; //vector carrying all GeomReferenceElements
};
\end{lstlisting}
\vspace{0.2cm}
It offers:

\begin{itemize}
\item few constructors (the default one, a general one with dimension, measure, centroid, and
number of vertices, edges and faces and one more specific for each dimension):
\vspace{.1cm}
\begin{lstlisting}
GeomRefElement(); //default constructor
GeomRefElement(ShapeType, const Dimen d, const Real m, const Real c, 
               const Number v, const Number e, const Number f);        //general 
GeomRefElement(ShapeType, const Real m = 1.0, const Real c = 0.5);     //1D 
GeomRefElement(ShapeType, const Real m, const Real c, const Number v); //2D
GeomRefElement(ShapeType, const Real m, const Real c, const Number v, 
               const Number e);                                        //3D
\end{lstlisting}
\vspace{.2cm}
\item a private copy constructor and a private assign operator,
\item some public access functions to data:
\begin{lstlisting}[deletekeywords={[3] begin}]
Dimen dim() const;
Number nbSides() const;
Number nbSideOfSides() const;
Real measure() const;
vector<Real>::const_iterator centroid() const;
vector<Real>::const_iterator vertices() const;
vector<ShapeType> sideShapeTypes() const;
const std::vector<std::vector<Number> >& sideVertexNumbers() const;
const std::vector<std::vector<Number> >& sideOfSideVertexNumbers() const;
\end{lstlisting}
\vspace{.2cm}
\item some public methods to get data on sides and side of sides:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] id, dim}]
String shape(const Number sideNum = 0) const; //element (side) shape as a string
ShapeType shapeType(const Number sideNum = 0) const; /element (side) shape as a ShapeType
bool isSimplex() const;                              //true if a simplex
Number nbVertices(const Number sideNum = 0) const;   //number of element (side) vertices
Number sideVertex(const Number id, const Number sideNum = 0) const;//number of element (side) 
std::vector<Real>::const_iterator vertex(const Number vNum) const; //coordinates of n-th vertex 
Number sideVertexNumber(const Number vNum, const Number sideNum) const;
Number sideOfSideVertexNumber(const Number vNum, const Number sideOfSideNum) const;
Number sideOfSideNumber(const Number i, const Number sideNum) const; //local number of i-th edge
void rotateVertices(const number_t newFirst,vector<number_t>&) const;// circular permutation 
vector<real_t> sideToElt(number_t,vector<real_t>::const_iterator) const; 
virtual Real measure(const Dimen dim, const Number sideNum = 0) const = 0;
// projection of a point onto ref element
virtual std::vector<real_t> projection(const vector<real_t>&, real_t&) const;
// test if a point belongs to current element
virtual bool contains(std::vector<real_t>& p, real_t tol= theTolerance) const;    
    
 \end{lstlisting}
\vspace{.2cm}
\item some protected append methods for vertices:
\vspace{.1cm}
\begin{lstlisting}
//build vertex 1d/2d/3d coordinates
void vertex(vector<Real>::iterator& it, const Real x1);
void vertex(vector<Real>::iterator& it, const Real x1, const Real x2);
void vertex(vector<Real>::iterator& it, const Real x1, const Real x2, const Real x3);
 \end{lstlisting}
\vspace{.2cm}
\item some public error handlers:
\vspace{.1cm}
\begin{lstlisting}
void noSuchSideOfSide(const Number) const;
void noSuchSide(const Number) const;
void noSuchSide(const Number, const Number, const Number = 0, const Number = 0) const;
void noSuchFunction(const String& s) const;
\end{lstlisting}
\vspace{.2cm}
\item an external search function in the static run-time list of \class{GeomRefElement}:
\vspace{.1cm}
\begin{lstlisting}
GeomRefElement* findGeomRefElement(ShapeType);
\end{lstlisting}
\end{itemize}
 
\displayInfos{library=finiteElements, header=GeomRefElement.hpp, implementation=GeomRefElement.cpp,
test={test\_segment.cpp, test\_triangle.cpp, test\_quadrangle.cpp, test\_tetrahedron.cpp, test\_hexahedron.cpp, test\_prism.cpp},
header dep={config.h, GeomRefSegment.hpp, GeomRefTriangle.hpp, GeomRefQuadrangle.hpp,
GeomRefTetrahedron.hpp, GeomRefHexahedron.hpp, GeomRefPrism.hpp}}

\subsection{Child classes of \classtitle{GeomRefElement}}

The child classes of \class{GeomRefElement} are \class{GeomRefSegment} in 1D, \class{GeomRefTriangle}
and \class{GeomRefQuadrangle} in 2D and \class{GeomRefHexahedron}, \class{GeomRefTetrahedron}, \class{GeomRefPrism} and \class{GeomRefPyramid} in 3D.

Except implementation of virtual functions, these child classes provide 2 new member functions
used in class constructors:

\begin{lstlisting}
void sideNumbering();       //local numbers of vertices on sides
void sideOfSideNumbering(); //local numbers of vertices on side of sides
\end{lstlisting}
\vspace{0.2cm}

The last one is not defined for \class{GeomRefSegment}.

\displayInfos{library=finiteElements, header=GeomRefSegment.hpp, implementation=GeomRefSegment.cpp,
test=test\_segment.cpp, header dep={config.h, GeomRefElement.hpp }}

\displayInfos{library=finiteElements, header=GeomRefTriangle.hpp, implementation=GeomRefTriangle.cpp,
test=test\_triangle.cpp, header dep={config.h, GeomRefElement.hpp }}

\displayInfos{library=finiteElements, header=GeomRefQuadrangle.hpp, implementation=GeomRefQuadrangle.cpp,
test=test\_quadrangle.cpp, header dep={config.h, GeomRefElement.hpp }}

\displayInfos{library=finiteElements, header=GeomRefTetrahedron.hpp, implementation=GeomRefTetrahedron.cpp,
test=test\_tetrahedron.cpp, header dep={config.h, GeomRefElement.hpp }}

\displayInfos{library=finiteElements, header=GeomRefHexahedron.hpp, implementation=GeomRefHexahedron.cpp,
test=test\_hexahedron.cpp, header dep={config.h, GeomRefElement.hpp }}

\displayInfos{library=finiteElements, header=GeomRefPrism.hpp, implementation=GeomRefPrism.cpp,
test=test\_prism.cpp, header dep={config.h, GeomRefElement.hpp }}

\displayInfos{library=finiteElements, header=GeomRefPyramid.hpp, implementation=GeomRefPyramid.cpp,
test=test\_pyramid.cpp, header dep={config.h, GeomRefElement.hpp }}

