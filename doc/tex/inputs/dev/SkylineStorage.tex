%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Skyline storages}

With dense storages, skyline storages are the only storages that are compatibles with factorization methods like LU. Skyline storages consist in storage rows or  columns from its first nonzero value up to its last nonzero value. When lower triangular part is addressed, the last nonzero value on a row is the diagonal matrix entry. When upper triangular part is addressed, the last nonzero value on a column is the diagonal matrix entry.  Only dual and symmetric skyline storages are proposed here (resp. \class{DualSkylineStorage} and \class{SymSkylineStorage} classes).
\begin{itemize}
\item Dual skyline stores first the diagonal of the matrix, then the strict lower "triangular" part and the strict upper "triangular" part of the matrix using the following storage vectors (\(A\) is a \(m\times n\) matrix):\\
\hspace{10mm} values=(0, \(A_{11}, \ldots,A_{ii}, \ldots, A_{il},\ldots, A_{i,p_i}, \ldots, A_{i,l_i},\ldots, A_{q_j,j}, \ldots, A_{q_j,c_j}\))\\
\hspace{10mm} rowPointer=(\(s_1,\ldots,s_i,\ldots,s_m,s_{m+1}\))\\
\hspace{10mm} colPointer=(\(t_1,\ldots,t_j,\ldots,t_n,t_{n+1}\))\\
where 
\[
\begin{array}{l}
l_i=\min(i-1,n-1),\ c_j=\min(j-1,m-1)\\
p_i=\min{0<j<l_i,\ A_{ij}\ne 0} \text{ and } q_j=\min{0<i<c_j,\ A_{ij}\ne 0}\\
s_1=0;\ s_i=s_{i-1}+i-s_{i-1}\ \forall i=2,m,  \text{ and }  t_1=0;\ t_j=s_{j-1}+j-t_{j-1}\ \forall j=2,n
\end{array}
\]
The rowPointer (resp. colPointer) vector gives the position in lower (resp. upper) part of the first nonzero entry of a row (resp. a column); \(s_{m+1}\) (resp.\(t_{n+1}\)) gives the number of entries stored in lower part (resp. upper part). \\ \\
For instance, the following \(5\times 6\) non-symmetric matrix 
\[
A=\left[
\begin{array}{cccccc}
11 & 12 & 0 & 14 & 0 & 16\\
0 & 22 & 23 & 0 & 0 & 26 \\
31 & 32 & 33 & 0 & 0 & 0 \\
0 & 0 & 43 & 44 & 0 & 0 \\
51 & 0 & 53 & 54 & 55 & 0
\end{array}
\right]
\]
has the following storage vectors: \\ \\
\hspace{10mm} values     = (0 11 22 33 44 45\ \ 22\ \ 31 32\ \ 43\ \ 51 0 53 54 \ \ 12\ \ 23\ \ 14 0 0 \ \ 16 26 0 0) \\
\hspace{10mm} rowPointer = (0 0 0 2 3 7)\\
\hspace{10mm} colPointer = (0 0 1 2 5 5 8) \\
\\
The length of row \(i\) (resp. column \(j\)) is given by \(s_{i+1}-s_i\) (resp. \(t_{t+1}-t_j\)). The position in values vector of entry \((i,j)\) is given by the following relations (\(1\le i\le m\), \(1\le j\le n\)):
\[
\begin{array}{l}
\text{if } i=j\ \  \text{adr}(i,j)=i \\
\text{if } p_i\le j\le l_i \ \  \text{adr}(i,j)=\min(m,n)+s_{i+1}+j-i\\
\text{if } q_j\le i\le c_j \ \  \text{adr}(i,j)=\min(m,n)+s_{m+1}+t_{j+1}+i-j\\
\end{array}
\]
\end{itemize}

\subsection{The \classtitle{SkylineStorage} class}

The \class{SkylineStorage} class has no member attribute. It has some basic constructors just calling \class{MatrixStorage} constructors:
\vspace{.1cm}
\begin{lstlisting}
class SkylineStorage : public MatrixStorage
{public :
  SkylineStorage(AccessType = _dual);       
  SkylineStorage(Number, AccessType = _dual); 
  SkylineStorage(Number, Number, AccessType = _dual); 
  virtual ~SkylineStorage() {}                       
\end{lstlisting}
\vspace{.2cm} 
\class{SkylineStorage} objects do not have to be instantiated. This abstract class acts as an interface to particular skyline storages and gathers all common functionalities of skyline storages, some being virtuals (some template line declarations are omitted): 
\vspace{.1cm}
\begin{lstlisting}
virtual Number size() const = 0;       
virtual Number lowerPartSize() const = 0;
virtual Number upperPartSize() const {return 0;}
template<typename Iterator>
 void printEntriesTriangularPart (StrucType, Iterator&, Iterator&, const std::vector<Number>&, Number, Number, Number, const String&, Number, std::ostream&) const;
 void printCooTriangularPart(std::ostream&, Iterator&, const std::vector<Number>&, Number, Number, bool, SymType sym = _noSymmetry) const;         
 void printCooDiagonalPart(std::ostream&, Iterator&, Number) const;
                             
template <typename T>
 void loadSkylineFromFileDense(std::istream&, std::vector<T>&, std::vector<Number>&, std::vector<Number>&, SymType, bool);
 void loadSkylineFromFileCoo(std::istream&, std::vector<T>&, std::vector<Number>&, std::vector<Number>&, SymType, bool);
 
template<typename MatIterator, typename VecIterator, typename ResIterator>
 void diagonalMatrixVector(MatIterator&, VecIterator&, ResIterator&, ResIterator&) const;
 void lowerMatrixVector(const std::vector<Number>&, MatIterator&, VecIterator&, ResIterator&, SymType sym) const;
 void upperMatrixVector(const std::vector<Number>&, MatIterator&, VecIterator&, ResIterator&, SymType) const;
 void diagonalVectorMatrix(MatIterator&, VecIterator&, ResIterator&, ResIterator&) const;
 void lowerVectorMatrix(const std::vector<Number>&, MatIterator&, VecIterator&, ResIterator&, SymType sym) const;
 void upperVectorMatrix(const std::vector<Number>&, MatIterator&, VecIterator&, ResIterator&, SymType) const;
 void sumMatrixMatrix(Mat1Iterator&, Mat2Iterator&, ResIterator&, ResIterator&) const;
\end{lstlisting}
\vspace{.1cm} 
The \class{SkylineStorage} class provides the real code of product of matrix and vector, using iterators as arguments to by-pass the template type of matrix values. \\
In order to deal with some specific solvers, this class also comes with several following functions. For simplicity, we only declare template line once.
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] size\_t}]
template<typename MatIterator, typename VecIterator, typename XIterator>
void bzLowerSolver(const MatIterator&, const MatIterator&, const VecIterator&,  const XIterator&, const XIterator&, const std::vector<size_t>::const_iterator) const;
void bzLowerD1Solver(const MatIterator&, const VecIterator&, const XIterator&, const XIterator&, const std::vector<size_t>::const_iterator) const;
void bzLowerConjD1Solver(const MatIterator&, const VecIterator&,  const XIterator&, const XIterator&,  const std::vector<size_t>::const_iterator) const;
void bzDiagonalSolver(const MatIterator&, const VecIterator&,  const XIterator&, const XIterator&) const;
void bzUpperSolver(const MatRevIterator&, const MatRevIterator&, const VecRevIterator&,  const XRevIterator&, const XRevIterator&, const std::vector<size_t>::const_reverse_iterator) const;
void bzUpperD1Solver(const MatRevIterator&, const VecRevIterator&, const XRevIterator&, const XRevIterator&, const std::vector<size_t>::const_reverse_iterator) const;
void bzUpperConjD1Solver(const MatRevIterator&, const VecRevIterator&, const XRevIterator&, const XRevIterator&, const std::vector<size_t>::const_reverse_iterator) const;
\end{lstlisting}
Like the product of matrix and vector, these functions take advantage of iterators as arguments to by-pass the template type of matrix values.
\vspace{.2cm} 
\displayInfos{library=largeMatrix, header=SkylineStorage.hpp, implementation=SkylineStorage.cpp, test={test\_LargeMatrixSkylinetorage.cpp}, 
header dep={MatrixStorage.hpp, config.h, utils.h}}

\subsection{\classtitle{Dual/SymSkylineStorage} classes}

Dual skyline storage travels the matrix as diagonal, strict lower and upper parts. Row pointers vectors are attached to strict lower part and column pointers vectors are attached to strict upper part:
\vspace{.1cm}
\begin{lstlisting}
class DualSkylineStorage : public SkylineStorage
{protected :
  std::vector<Number> rowPointer_; // vector of positions of begining of rows
  std::vector<Number> colPointer_; // vector of positions of begining of cols
  \ldots
\end{lstlisting}
\vspace{.2cm} 
\class{DualSkylineStorage} class provides basic constructor (no construction of storage vectors), constructor from a pair of global numbering vectors or list of column indices by rows.
Both of them create the compressed storage vector using the auxiliary function \verb?buildStorage?:
\vspace{.1cm}
\begin{lstlisting}
DualSkylineStorage(Number nr = 0, Number nc = 0);
DualSkylineStorage(Number, Number, const std::vector< std::vector<Number> >&,
                   const std::vector< std::vector<Number> >&);
DualSkylineStorage(Number, Number, const std::vector< std::vector<Number> >&);
~DualSkylineStorage() {};  
\end{lstlisting}
\vspace{.2cm} 
The class provides most of the virtual methods declared in \class{MatrixStorage} class. For sake of simplicity, we report here only function with \verb?Real? type but versions with \verb?Complex?, \verb?Matrix<Real>?, \verb?Matrix<Complex>? and mixed types are also provided. Some template line declarations are also omitted. 
\vspace{.1cm}
\begin{lstlisting}
Number size() const;
Number lowerPartSize() const ;
Number upperPartSize() const;
Number pos(Number i, Number j, SymType s=_noSymmetry) const;
void positions(const std::vector<Number>&, const std::vector<Number>&,
               std::vector<Number>&, bool errorOn=true, 
               SymType s=_noSymmetry) const;
void printEntries(std::ostream&, const std::vector<Real>&, Number, SymType) const;
void printCooMatrix(std::ostream&, const std::vector<Real>&, 
                    SymType s=_noSymmetry) const;
void loadFromFileDense(std::istream&, std::vector<Real>&, SymType, bool );
void loadFromFileCoo(std::istream& , std::vector<Real>&, SymType, bool);
template<typename M, typename V, typename R>
  void multMatrixVector(const std::vector<M>&, const std::vector<V>&, 
                        std::vector<R>&) const; 
  void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, 
                        std::vector<R>&) const;
  void multMatrixVector(const std::vector<Real>&, const std::vector<Real>&, 
                        std::vector<Real>&, SymType) const;
  void multVectorMatrix(const std::vector<Real>&, const std::vector<Real>&, 
                        std::vector<Real>&, SymType) const;
template<typename M1, typename M2, typename R>
  void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const;
template<typename T>
  void setDiagValueDualSkyline(std::vector<T>&, const T);
template<typename M>
void lu(std::vector<M>& m, std::vector<M>& fa) const;
template<typename M, typename V, typename X>
  void lowerD1Solver(const std::vector<M>&, std::vector<V>&, std::vector<X>&) const;
  void diagonalSolver(const std::vector<M>&, std::vector<V>&, std::vector<X>&) const;
  void upperD1Solver(const std::vector<M>&, std::vector<V>&, std::vector<X>&, const SymType) const;
  void upperSolver(const std::vector<M>&, std::vector<V>&, std::vector<X>&) const;
\end{lstlisting}
\vspace{.2cm} 

The \class{SymSkylineStorage} class is very similar to the \class{DualSkylineStorage} class, except that it addresses only square matrix with symmetric storage and, thus, does not manage column pointers vectors because the upper part has same storage as lower part (with transposition) :
\vspace{.1cm}
\begin{lstlisting}
class SymSkylineStorage : public SkylineStorage
{protected :
  std::vector<Number> rowPointer_; //!< vector of positions of begining of cols
  \ldots
\end{lstlisting}
\vspace{.1cm}
Note that the matrix may be not symmetric. In that case its upper part is stored.\\
Besides some similar functions to \class{DualSkylineStorage} class', the \class{SymSkylineStorage} provides more several functions to factorize matrix: LDLt and LDL* factorization 
\vspace{.1cm}
\begin{lstlisting}
template<typename M>
    void ldlt(std::vector<M>& m, std::vector<M>& fa, const SymType sym = _symmetric) const;
    void ldlstar(std::vector<M>& m, std::vector<M>& fa) const;
\end{lstlisting}
\vspace{.2cm}
XXX=Dual or Sym

\displayInfos{library=largeMatrix, header=XXXSkylineStorage.hpp, implementation=XXXSkylineStorage.cpp, test={test\_LargeMatrixSkylineStorage.cpp}, header dep={SkylineStorage.hpp, MatrixStorage.hpp, config.h, utils.h}}
