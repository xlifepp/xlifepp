%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{Timer} class}

The \class{Timer} class is a utility class to perform computational time analysis (CPU time
and elapsed time) and manage dates. It uses the following data members which store time, CPU time and system time in various format.
\vspace{.2cm}
\begin{lstlisting}
class Timer
{private:
    time_t t;                       //time type provided by ctime
    tm localt;                      //time structure provided by ctime
    long int sec_, microSec_;       //time in seconds and microseconds 
                                    // since 01/01/1970 00:00:00
    long int usrSec_, usrMicroSec_; // cputime in seconds and microseconds 
                                    // since beginning of current process
    long int sysSec_, sysMicroSec_; // system time in seconds and microseconds 
                                    // since beginning of current process
 \ldots
};
\end{lstlisting}
\vspace{.2cm}
The \class{Timer} class has only one constructor (no argument) which initializes the start
time. It provides a few basic functionalities:
\vspace{.2cm}
\begin{lstlisting}
    void update()                // converts time to tm struct
    int year()                   // year as an int
    unsigned short int month()   // month as an int [0-11]
    unsigned short int day()     // day of month as an int [0-31]
    unsigned short int hour()    // hour of day (24h clock) 
    unsigned short int minutes() // minutes of hour [0-59]
    unsigned short int seconds() // seconds of minute [0-59]
    void getCpuTime()            // update cputime interval since beginning of process
    double deltaCpuTime(Timer*)  // returns elapsed time interval since "ot" time
    void getTime()               // update time in s and ms since 01/01/1970 00:00:00
    double deltaTime(Timer*)     // returns elapsed time interval since "ot" time
\end{lstlisting}
\vspace{.2cm}
Associated to these member functions, there are the following external functions (user functions):
\vspace{.2cm}
\begin{lstlisting}
String theTime()           // returns current time 
String theDate()           // returns current date as dd.mmm.yyyy
String theShortDate()      // returns current date as mm/dd/yyyy or dd/mm/yyyy 
String theLongDate()       // returns current date as Month Day,Year or Day Month Year
String theIsoDate()        // returns ISO8601 format of current date (yyyy-mm-dd)
String theIsoTime()        // returns ISO8601 format of current time (hh-mi-ss)

double cpuTime()                   // returns cputime in sec.since last call
double cpuTime(const String&)      // same and prints it with comment 
double totalCpuTime()              // returns cputime in sec.since first call
double totalCpuTime(const String&) //same and prints it with comment
double elapsedTime()               // returns elapsed time  in sec. since last call
double elapsedTime(const String&)  // same and prints it with comment
double totalElapsedTime()          // returns elapsed time in sec. since first call
double totalElapsedTime(const String&) // returns elapsed time interval in sec. 
                                       // same and prints it with comment
\end{lstlisting}
\vspace{.2cm}

Note that this class is OS dependant because Unix and Windows do not provide the same system
time functions. The macro OS\_IS\_WIN32 set in \emph{setup.hpp} is used to select the right
version.
In particular, for Unix platform the class uses some functions provided by the header \emph{sys/time.h}
and for Windows platform, some functions provided by the header \emph{sys/timeb.h}, this header
requiring the headers  \emph{windef.h, stdio.h, cstdarg, winbase.h}.\\

The function \emph{timerInit} in the \emph{init.cpp} file creates two \class{Timer}
objects (\emph{theStartTime\_p} and \emph{theLastTime\_p}) at the beginning of the execution.
These objects have a global scope and are used by the \class{Timer} class 
to make easier the time computation analysis for the user. There is no reason to create other
instances of \class{Timer}. \\

So, it is easy to the user to perform time computation analysis. For instance:
\vspace{.2cm}
\begin{lstlisting}
#include "xlife++.h"
using namespace xlifepp;
int main()
{
  init(fr);  //initializes timers
  //task 1
  \ldots
  cpuTime("CPU time for task 1");
  elapsedTime("ellapsed time for task 1");
  //task 2
  \ldots
  cpuTime("CPU time for task 2");
  elapsedTime("ellapsed time for task 2");
  //end of tasks
  totalCpuTime("total CPU time");
  totalElapsedTime("total ellapsed time");
}
\end{lstlisting}
\vspace{.2cm}

\displayInfos{library=utils, header=Timer.hpp, implementation=Timer.cpp, header dep={config.h}}

