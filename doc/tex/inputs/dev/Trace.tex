%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Managing traces}

\subsection{The \classtitle{Trace} class}

The \class{Trace} class is devoted to keep trace of runtime of the hierarchy of function calls.
Thus, we can have access to a stack of called functions, the next position to be used and the
last one. Thus, this class proposes as members:
\vspace{.1cm}
\begin{lstlisting}
class Trace {
  private:
    number_t pos_;                    //!< next position in trace object list member
    static const number_t maxPos_=36; //!< last position in trace object list member
    String fList[maxPos_];            //!< stack of called functions

    static const String theLogFile_;  //!< name of log file
    static bool isLogged_;

  public:
    static std::ofstream theLogStream_;
  \ldots
};
\end{lstlisting}
\vspace{.2cm}
It offers:

\begin{itemize}
\item a default constructor that initializes pos\_ to 0, and private copy constructor and assignment
operator
\item some public access functions to data:
\begin{lstlisting}
static const String logFile() { return theLogFile_; }
static bool isLogged() { return isLogged_; }
static void logOn() { isLogged_ = true; }
static void logOff() { isLogged_ = false; }
number_t length() const; //!< function length returns the number of items of f_list
\end{lstlisting}
\vspace{.2cm}
\item some public utilities methods for adding trace or having info on current position:
\vspace{.1cm}
\begin{lstlisting}
void push(const String&);       //!< "pushes" name into Trace function list member
void pop();                     //!< "pops" current input out of a Trace function list member
String current(const number_t); //!< returns string of last l to current inputs of a Trace object list
String current();               //!< returns last input string of a Trace object list
String list();                  //!< returns function list of a Trace object list
 \end{lstlisting}
\vspace{.2cm}
\item some public display management methods:
\vspace{.1cm}
\begin{lstlisting}
void indent();                  //!< function indent prints context-depending indentation to log file
void print(std::ofstream&);     //!< prints function list to opened ofstream or to default print file
void print();
\end{lstlisting}
\vspace{.2cm}
\item some public methods to build log messages:
\vspace{.1cm}
\begin{lstlisting}
void log(); //!< outputs indentation to log file
template<typename T_0> void log(const T_0& s);
template<typename T_0, typename T_1>  void log(const T_0& s, const T_1& t1)
template<typename T_0, typename T_1, typename T_2> void log(const T_0& s, const T_1& t1, const T_2& t2);
template<typename T_0, typename T_1, typename T_2, typename T_3> void log(const T_0& s, const T_1& t1, const T_2& t2, const T_3& t3);
template<typename T_0, typename T_1, typename T_2, typename T_3, typename T_4> void log(const T_0& s, const T_1& t1, const T_2& t2, const T_3& t3, const T_4& t4);
\end{lstlisting}
\vspace{.2cm}
\item some external functions to manage trace errors:
\vspace{.1cm}
\begin{lstlisting}
void incompleteFunction(const String& s = "");
void invalidFunction(const String& s = "");
void constructorError();
set setNewHandler();
\end{lstlisting}
\vspace{.2cm}
\item some external functions to manage verbose level:
\vspace{.1cm}
\begin{lstlisting}
void setGlobalVerboseLevel(const unsigned int);
unsigned int verboseLevel(const unsigned int);
\end{lstlisting}
\vspace{.2cm}
\item one external functions to manage  the message location :
\vspace{.1cm}
\begin{lstlisting}
String & where(const String& s);
\end{lstlisting}
\end{itemize}

\subsection{How to use the verbose level ?}

The verbose level is managed through 2 global variables : {\ttfamily theVerboseLevel} and {\ttfamily theGlobalVerboseLevel}, and 2 functions. The behavior is the following :

\begin{itemize}
\item To set globally the maximum verbose level to an int value \(i\), you can use the following syntax : \lstinline{setGlobalVerboseLevel(i);} This sets the variable {\ttfamily theGlobaVerboseLevel} and overrides every local definition of the verbose level to higher values. This function is to be used once at the beginning of a main program.
\item To set locally the maximum verbose level to a value \(i\), you can use the following syntax :
\begin{lstlisting}
int j=verboseLevel(i);
if (i < theGlobalVerboseLevel) {
  j=theVerboseLevel}=i;
} else { 
  j=theGlobaVerboseLevel;
}
\end{lstlisting}
\item To get the current maximum verbose level, you can use directly the variable {\ttfamily theVerboseLevel}
\end{itemize}

\subsection{How to know where do messages come ?}

To see how to throw messages, please read the previous section about it. Here, we will discuss how a message knows where it is and how to give is more information about it.

The main mechanism is to use the functions \cmd{push} and \cmd{pop} respectively at the beginning and the end of whatever function. This piece of information is dealt with by messages handlers automatically. But we cannot do it for every function in the code. We have to reserve it for the top-level functions in order not to slow execution.

For the deepest functions, there is another way : using the \cmd{where} function. This function is to be used just before a message call where \cmd{push} and \cmd{pop} are not used. Messages will deal with this additional information automatically. As a result, there is no need to define message format with the routine name as message data.

\displayInfos{library=utils, header=Trace.hpp, implementation=Trace.cpp, header dep={config.h,
String.hpp}}

