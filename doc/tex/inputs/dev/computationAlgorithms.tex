%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The computation algorithms}

\subsection{Matrix computation}

In this section, we explain how the computation of matrix from bilinear form works. This computation depends on the type of the  bilinear form (single integral term, double integral term, \ldots) and the types of unknown and test function. This section addresses only the computation of \class{SuTermMatrix}.\\

The principle of computation, implemented in  \verb?SuTermMatrix::compute()?, consists in 4 steps:
\begin{itemize}
\item collect basic bilinear forms along their domain and their computation type (see \verb?SuTermMatrix::getSuBlfs()? function.):\\

\begin{tabular}{|l|p{7cm}|p{3.5cm}|}
\hline\verb?_FEComputation?   & single integral on a domain & u, v in FE spaces \\ 
\hline\verb?_FEextComputation?   & single integral on a boundary domain with non-tangential derivatives & u, v in FE spaces \\ 
\hline\verb?_DGComputation?   & single integral on a side domain involving mean-jump operators & u, v in FE spaces (discontinuous) \\ 
\hline\verb?_IEComputation?   & double integral on a domains pair & u, v in FE space, standard Kernel \\ 
\hline\verb?_SPComputation?   & single integral on a domain & u, v in SP spaces \\ 
\hline\verb?_FESPComputation? & single integral on a domain & one in SP space, other in FE space \\ 
\hline\verb?_IESPComputation? & double integral on a domains pair & u, v in FE space, TensorKernel \\ 
\hline\verb?_IEHMComputation? & double integral on a domains pair & u, v in FE space, HMatrix \\   
\hline 
\end{tabular}    
\item construct a storage consistent with all collections to compute (imposed by user or chosen between compressed sparse storage or dense storage)
\item create \class{MatrixEntry} with the right structure and value types
\item for each collection, call the computation function regarding its computation type
\end{itemize}
This last step looks like in \verb?SuTermMatrix::compute()?:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] str}]
if(FEsublfs.size()>0)    compute<_FEComputation>(FEsublfs, vt, str);
if(FEextsublfs.size()>0) compute<_FEextComputation>(FEextsublfs, vt, str);
if(SPsublfs.size()>0)    compute<_SPComputation>(SPsublfs, vt, str);
if(FESPsublfs.size()>0)  compute<_FESPComputation>(FESPsublfs, vt, str);
if(IEsublfs.size()>0)    compute<_IEComputation>(IEsublfs, vt, str);
if(IEextsublfs.size()>0) compute<_IEComputation>(IEextsublfs, vt, str);
if(IESPsublfs.size()>0)  compute<_IESPComputation>(IESPsublfs, vt, str);
if(DGsublfs.size()>0)    compute<_DGComputation>(DGsublfs, vt, str);
\end{lstlisting}
where all specializations of \verb?compute<unsigned int CM>? are implemented in the \textit{XXMatrixComputation.hpp} files included add the end of the \textit{SuTermMatrix.hpp} file. All the computation algorithms mainly work as follows:
\begin{verbatim}
retry general data (spaces, subspaces, domains, \ldots) 
loop on element on domain 1 
  retry element data (local to global numbering)
  (loop on element on domain 2)     only for IE and IESP computation
    (retry element data)
     compute shapevalues
     loop on basic bilinear form 
        loop on quadrature points
           compute operator on unknowns
          add in elementary matrix using tensorial operations
        assembly in global matrix 
\end{verbatim}

\begin{focusbox}
For sake of simplicity, evaluation of operators returns always a \textbf{scalar vector}, even if the unknowns are vector ones. So this vector has to be correctly reinterpreted when it is added in elementary block matrix.
\end{focusbox}
 
 
When there is a HMatrix computation that requires a specific representation (HMatrixEntry), this one is achieved after all the computations requiring a MatrixEntry. Then the MatrixEntry (if it exists one) is merged in the HMatrixEntry. Obviously, anything cannot be merged to HMatrixEntry!\\

For IE computation, some specialized function are devoted to particular integration method adapted to singular kernels (Sauter-Schwab method for instance). They are implemented in particular files (\textit{SauterSchwabIM.hpp}, \textit{LenoirSallesIM.hpp}, \textit{DuffyIM.hpp}), see the \textit{src/term/computation} directory.\\
 
Up to now, most of the computation functions have been parallelized (multi-threading with omp).

\subsection{Understanding TermMatrix operations}
The following figure sketches out how the operations are processed in the class hierarchy from \class{TermMatrix} to storage classes that implement the computation algorithms:
\begin{figure}[H]
\begin{center}
\includePict[width=14cm]{TermMatrix_Operation.png}
\end{center}
\caption{The process of a TermMatrix operation}
\end{figure} 
  
\subsection{Vector computation}

For the moment, the \class{SuTermVector} class provides 
\begin{itemize}
	\item the \verb?computeFE? function that computes single integral on a FE space
	\item the \verb?computeIR? function that computes integral representation
\end{itemize}  
These functions are implemented in the \textit{FeVectorComputation.hpp} file included by the \textit{SuTermVector.hpp} file. It works as matrix computation but it is simpler.
