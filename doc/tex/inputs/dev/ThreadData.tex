%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{ThreadData} class}

The \class{ThreadData} class is a utility class that manages some FE computation data (normal vectors, current element, current DoF, current domain) that may be got by user function or user kernel. When the computation is done on several threads, these data have multiple instances (one by thread), this is the reason why a \class{ThreadData} object handles some vectors of data pointers:\\

\begin{lstlisting}
vector<Vector<real_t>*> theCurrentNxs;     // normal pointers or x-normal pointers
vector<Vector<real_t>*> theCurrentNys;     // y-normal pointers
vector<GeomElement*> theCurrentElements;   // element pointers
vector<Dof*> theCurrentDofs;               // DoF pointers
vector<number_t> theCurrentBasisIndexes;   // basis indexes
GeomDomain* currentDomainx;                // domain pointer or domain_x pointer
GeomDomain* currentDomainy;                // domain_y pointer
\end{lstlisting}  
\vspace{2mm}
It has only one a basic constructor and a resize function that is called when the number of threads changes:
\vspace{1mm}
\begin{lstlisting}
ThreadData(number_t s=1);
void resize(number_t);
\end{lstlisting}
\vspace{2mm}
Besides, it offers various functions to set or get the data:
\vspace{1mm}
\begin{lstlisting}
void setNx(Vector<real_t>* p);
void setNy(Vector<real_t>* p);
void setElement(GeomElement* e);
void setDomainx(GeomDomain* p);
void setDomainy(GeomDomain* p);
void setDof(Dof* p);
void setBasisIndex(number_t i);
void setNx(Vector<real_t>* p, number_t t);
void setNy(Vector<real_t>* p, number_t t);
void setElement(GeomElement* p, number_t t);
void setDof(Dof* p, number_t t);
void setBasisIndex(number_t t, number_t t);

Vector<real_t>& getNx(number_t t) const;
Vector<real_t>& getNx() const;
Vector<real_t>& getNy(number_t t) const;
Vector<real_t>& getNy() const;
GeomElement& getElement(number_t t);
GeomElement& getElement() const;
GeomDomain& getDomainx() const;
GeomDomain& getDomainy() const;
Dof& getDof(number_t t) const;
Dof& getDof() const;
FeDof& getFeDof(number_t t) const;
FeDof& getFeDof() const; 
number_t getBasisIndex(number_t t) const;
number_t getBasisIndex() const;
\end{lstlisting}
\vspace{2mm}
Print utility are also available
\vspace{1mm}
\begin{lstlisting}
void print(std::ostream&) const;
void print(PrintStream& os) const {print(os.currentStream());}
\end{lstlisting}
\vspace{2mm}
When starting, \xlifepp create one instance of \class{ThreadData} : \var{theThreadData} (see {\em globalScopeData.hpp, config.hpp}). To avoid the user to deal explicitly with it, some external functions (wrappers to member functions) are also proposed:
\vspace{1mm}
\begin{lstlisting}
inline void setNx(Vector<real_t>* p, number_t t);  
inline void setNy(Vector<real_t>* p, number_t t);
inline void setElement(GeomElement* p, number_t t);
inline void setDof(Dof* p,number_t t);
inline void setBasisIndex(number_t i,number_t t)
inline void setNx(Vector<real_t>* p);
inline void setNy(Vector<real_t>* p);
inline void setElement(GeomElement* p);
inline void setDomain(GeomDomain* p);
inline void setDomainx(GeomDomain* p);
inline void setDomainy(GeomDomain* p);
inline void setDof(Dof* p);
inline void setBasisIndex(number_t i)
inline Vector<real_t>& getN(number_t t);
inline Vector<real_t>& getNx(number_t t);
inline Vector<real_t>& getNy(number_t t ;
inline GeomElement& getElement(number_t t);
inline Dof& getDof(number_t t);
inline FeDof& getFeDof(number_t t);
inline number_t getBasisIndex(number_t t);
inline Vector<real_t>& getN();
inline Vector<real_t>& getNx();
inline Vector<real_t>& getNy();
inline GeomElement& getElement();
inline GeomDomain& getDomain();
inline GeomDomain& getDomainx();
inline GeomDomain& getDomainy();
inline Dof& getDof();
inline FeDof& getFeDof();
inline number_t getBasisIndex();
\end{lstlisting}
\vspace{2mm}
This machinery allows the user to simply deal with such computation data:
\begin{lstlisting}
Real fun(cons Point& x, Parameters& pars=theDefaultParameters) 
{\ldots
 Vector<Real>& n = getN();      // get current normal vector
 GeomElement& elt=getElement(); // get current element 
 Number i=getBasisIndex();      // get current basis index
 \ldots}

int main()
{\ldots
 Function F(fun);     //link a C++ function fun to a Function object
 F.require(_n);       //tell that function fun will use normal vector
 F.require("element");//tell that function fun will use element
\ldots
}
\end{lstlisting}

\displayInfos{library=utils, header=ThreadData.hpp, implementation=ThreadData.cpp, test=test\_ThreadData.cpp,
	header dep={config.h, utils.h}} 