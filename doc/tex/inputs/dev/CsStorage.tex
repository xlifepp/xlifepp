%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Compressed sparse storages}

Compressed sparse storages are designed to store only nonzero values of a matrix. It exists several methods to do it. The well known CSR/CSC storage being the most common one, we chose it. It consists in storing nonzero values of a row (resp. column) in a compressed vector and their column indices (resp. row indices) in a compressed vector. A vector of addresses where begin rows (resp. columns) is also required. More precisely, for an \(m\times n\) matrix
\begin{itemize}
\item Row compressed storage is defined by the three vectors: \\
\hspace{10mm} values = (0,\(A_{1\mathcal{C}_1}\), \ldots, \(A_{i\mathcal{C}_i}\), \ldots, \(A_{m\mathcal{C}_m}\)) \\
\hspace{10mm} colIndex = (\(\mathcal{C}_1\), \ldots, \(\mathcal{C}_i\), \ldots, \(\mathcal{C}_m\)) \\
\hspace{10mm} rowPointer = (\(p_1\),\ldots,\(p_i\),\ldots\(,p_m\), \(p_{m+1}\)) \\

where \(\mathcal{C}_i\) is the set of index \(j\) where \(A_{ij}\ne 0\) and \(p_i\) is the position in vector colIndex of the first entry of row \(i\le m\); \(p_{m+1}\) gives the number of nonzero values. By convention, all the indices begins at 0 and first entry of values vector is always 0! \\
For instance, the following \(5\times 6\) non-symmetric matrix 
\[
A=\left[
\begin{array}{cccccc}
11 & 12 & 0 & 14 & 0 & 16\\
0 & 22 & 23 & 0 & 0 & 26 \\
31 & 32 & 33 & 0 & 0 & 0 \\
0 & 0 & 43 & 44 & 0 & 0 \\
51 & 0 & 53 & 54 & 55 & 0
\end{array}
\right]
\]
has the row compressed storage vectors: \\
\hspace{10mm} values     = (0 11 12 14 16\ \ 22 23 26\ \ 31 32 33\ \ 43 44\ \ 51 53 54 55) \\
\hspace{10mm} colIndex   = (0 1 3 5\ \ 1 2 5\ \ 0 1 2\ \ 2 3\ \ 0 2 3 4) \\
\hspace{10mm} rowPointer = (0 4 7 10 12 16) \\
\item Row compressed storage is defined by the three vectors: \\
\hspace{10mm} values = (0,\(A_{\mathcal{R}_11}\), \ldots, \(A_{\mathcal{R}_jj}\), \ldots, \(A_{\mathcal{R}_nn}\)) \\
\hspace{10mm} rowIndex = (\(\mathcal{R}_1\), \ldots, \(\mathcal{R}_j\), \ldots, \(\mathcal{R}_n\)) \\
\hspace{10mm} colPointer = (\(p_1\),\ldots,\(p_j\),\ldots\(,p_n\), \(p_{n+1}\)) \\
where \(\mathcal{R}_j\) is the set of index \(i\) where \(A_{ij}\ne 0\) and \(p_j\) is the position in rowIndex vector of the first entry of column \(j\le n\); \(p_{n+1}\) gives the number of nonzero values. By convention, all the indices begins at 0 and first entry of values vector is always 0! \\
For instance, the previous  \(5\times 6\) non-symmetric matrix  \(A\)  has the column compress storage vectors: \\
\hspace{10mm} values     = (0 11 31 51\ \ 12 22 32\ \ 23 33 43 53\ \ 55\ \ 16 26) \\
\hspace{10mm} rowIndex   = (0 2 4\ \ 0 1 2\ \ 1 2 3 4\ \ 0 3 4\ \ 4\ \ 0 1) \\
\hspace{10mm} colPointer = (0 4 7 10 12 16) \\
\item Dual compressed storage is defined by the five vectors: \\
\hspace{10mm} values = (0,\text{diag}(A), \(A_{\mathcal{C}_11}\), \ldots, \(A_{\mathcal{C}_ii}\), \ldots, \(A_{\mathcal{C}_mm}\), \(A_{\mathcal{R}_11}\), \ldots, \(A_{\mathcal{R}_jj}\), \ldots, \(A_{\mathcal{R}_nn}\)) \\
\hspace{10mm} colIndex = (\(\mathcal{C}_1\), \ldots, \(\mathcal{C}_i\), \ldots, \(\mathcal{C}_m\)) \\ 
\hspace{10mm} rowPointer = (\(p_1\),\ldots,\(p_i\),\ldots\(,p_m\), \(p_{m+1}\)) \\
\hspace{10mm} rowIndex = (\(\mathcal{R}_1\), \ldots, \(\mathcal{R}_j\), \ldots, \(\mathcal{R}_n\)) \\
\hspace{10mm} colPointer = (\(q_1\), \ldots, \(q_j\), \ldots, \(,q_n\), \(q_{n+1}\)) \\
where \(\mathcal{C}_i\) is the set of index \(j\) where \(A_{ij}\ne 0\) and \(j\le\min(i-1,n)\),
\(\mathcal{R}_j\) is the set of index \(i\) where \(A_{ij}\ne 0\) and \(i\le\min(j-1,m)\),  \(p_i\) (resp. \(q_j\)) is the position in colIndex (resp. rowIndex) vector of the first entry of row \(i\le m\) (resp. column \(j\le n\)). The colIndex and rowPointer vectors describes the row compressed storage of the strict lower part of matrix while the rowIndex and colPointer vectors describes the column compressed storage of the strict upper part of matrix. In this storage, all the diagonal entries of matrix are always first stored even the null entries!\\
For instance, the previous  \(5\times 6\) non-symmetric matrix  \(A\)  has the column compress storage vectors: \\
\hspace{10mm} values     = (0 11 22 33 44 55\ \ 31 32\ \ 43\ \ 51 53 54 \ \ 23 \ \ 14 44 \ \ 16 26) \\
\hspace{10mm} colIndex = (0 1\ \ 2\ \ 0 2 3) \\ 
\hspace{10mm} rowPointer = (0 0 0 2 3 6) \\
\hspace{10mm} rowIndex   = (0\ \ 1\ \ 0\ \ 0 1) \\
\hspace{10mm} colPointer = (0 0 1 2 2 3 5) \\
Note that for an empty row \(i\) (resp. column \(j\)), rowPointer(\(i\))=rowPointer(\(i-1\)) (resp. colPointer(\(j\))=colPointer(\(j-1\))). By the way, rowPointer(\(i\))-rowPointer(\(i-1\)) gives the number of stored entries on row \(i-1\).
\item Symmetric compressed storage is devoted to square matrix with symmetric storage, the matrix may be not symmetric. In that case, only the storage of lower triangular part of matrix is described by the three vectors (those of row part of dual storage): \\
\hspace{10mm} values = (0,\text{diag}(A), \(A_{\mathcal{C}_11}\), \ldots, \(A_{\mathcal{C}_ii}\), \ldots, \(A_{\mathcal{C}_mm}\)) if \(A\) has a symmetry property \\
\hspace{10mm} values = (0,\text{diag}(A), \(A_{\mathcal{C}_11}\), \ldots, \(A_{\mathcal{C}_ii}\), \ldots, \(A_{\mathcal{C}_mm}\),\(A_{\mathcal{C}_11}\), \ldots, \(A_{\mathcal{C}_jj}\), \ldots, \(A_{\mathcal{C}_mm}\)) if \(A\) has no symmetry property \\
\hspace{10mm} colIndex = (\(\mathcal{C}_1\), \ldots, \(\mathcal{C}_i\), \ldots, \(\mathcal{C}_m\)) \\ 
\hspace{10mm} rowPointer = (\(p_1\), \ldots, \(p_i\), \ldots, \(,p_m\), \(p_{m+1}\)) \\
where the meaning of \(\mathcal{C}_i\) and \(p_i\) is the same as dual storage.\\
For instance, the \(5\times 5\) non-symmetric matrix with symmetric storage
\[
B=\left[
\begin{array}{ccccc}
11 & 0 & 13 & 0 & 15 \\
0 & 22 & 23 & 0 & 0 \\
31 & 32 & 33 & 34 & 35\\
0 & 0 & 43 & 44 & 45 \\
51 & 0 & 53 & 54 & 55 
\end{array}
\right]
\]
has the symmetric compressed storage vectors: \\
\hspace{10mm} values     = (0 11 22 33 44 55\ \ 31 32\ \ 43\ \ 51 53 54\ \ 13 23\ \ 34\ \ 15 35 45) \\
\hspace{10mm} colIndex = (0 1\ \ 2\ \ 0 2 3) \\ 
\hspace{10mm} rowPointer = (0 0 0 2 3 6)\\
\end{itemize}

\begin{warningbox}
Be careful with the fact that actual matrix values begins at address 1 in values vector defined outside the storage stuff.
\end{warningbox}

\subsection{The \classtitle{ CsStorage} class}

The \class{CsStorage} class has no member attribute. There are some basic constructors just calling \class{MatrixStorage} constructors:
\vspace{.1cm}
\begin{lstlisting}
class CsStorage : public MatrixStorage
{public :
  CsStorage(AccessType = _dual);       
  CsStorage(Number, AccessType = _dual); 
  CsStorage(Number, Number, AccessType = _dual); 
  virtual ~CsStorage() {}                       
 protected :
  void buildCsStorage(const std::vector<std::vector<Number> >&,
                      std::vector<Number>&, std::vector<Number>&);
\end{lstlisting}
\vspace{.1cm} 
The \verb?buildCsStorage? is a general member function building storage vectors from lists of column indices by row. It is used by any child class of \class{CsStorage} class. 
\class{CsStorage} objects do not have to be instantiated. This class acts as an interface to particular compressed storages and gathers all common functionalities of dense storages, some being virtual (some template line declaration are omitted): 
\vspace{.1cm}
\begin{lstlisting}
virtual Number size() const = 0;       
virtual Number lowerPartSize() const;
virtual Number upperPartSize() const;
template<typename Iterator>
  void printEntriesAll(StrucType, Iterator& , const std::vector<Number>&, 
                       const std::vector<Number>&, Number, Number, Number, 
                       const String&, Number, std::ostream&) const; 
  void printEntriesTriangularPart(StrucType, Iterator&, Iterator&, 
                       const std::vector<Number>&, const std::vector<Number>&, 
                       Number, Number, Number, const String&, Number, 
                       std::ostream&) const;
  void printCooTriangularPart(std::ostream&, Iterator&, const std::vector<Number>&,
                              const std::vector<Number>&, bool, 
                              SymType sym = _noSymmetry) const; 
  void printCooDiagonalPart(std::ostream&, Iterator&, Number) const;
	
template <typename T>
    void loadCsFromFileDense(std::istream&, std::vector<T>&, std::vector<Number>&,
                             std::vector<Number>&, SymType, bool);
    void loadCsFromFileCoo(std::istream&, std::vector<T>&, std::vector<Number>&,
                           std::vector<Number>&, SymType, bool);
    void loadCsFromFileDense(std::istream&, std::vector<T>&, std::vector<Number>&,
                             std::vector<Number>&, std::vector<Number>&, 
                             std::vector<Number>&, SymType, bool);
    void loadCsFromFileCoo(std::istream&, std::vector<T>&, std::vector<Number>&, 
                           std::vector<Number>&, std::vector<Number>&, 
                           std::vector<Number>&, SymType, bool);
\end{lstlisting}
\vspace{.2cm} 
The \class{CsStorage} class provides the real code of product of matrix and vector, using iterators as arguments to by-pass the template type of matrix values. For dual and symmetric storage, the computation is performed by splitting the matrix in its diagonal, lower and upper part (template line are omitted): 
\vspace{.1cm}
\begin{lstlisting}
template<typename MatIterator, typename VecIterator, typename ResIterator>
  void diagonalMatrixVector(MatIterator&, VecIterator&, ResIterator&, 
                            ResIterator&) const;
  void lowerMatrixVector(const std::vector<Number>&, const std::vector<Number>&,
                         MatIterator&, VecIterator&, ResIterator&, SymType sym) const;
  void upperMatrixVector(const std::vector<Number>&, const std::vector<Number>&,
                         MatIterator&, VecIterator&, ResIterator&, SymType) const;
  void rowMatrixVector(const std::vector<Number>&, const std::vector<Number>&,
                       MatIterator&, VecIterator&, ResIterator&)  const;
  void columnMatrixVector(const std::vector<Number>&, const std::vector<Number>&,
                          MatIterator&, VecIterator&, ResIterator&)  const;
  void diagonalVectorMatrix(MatIterator&, VecIterator&, ResIterator&,
                            ResIterator&) const;
  void lowerVectorMatrix(const std::vector<Number>&, const std::vector<Number>&,
                         MatIterator&, VecIterator&, ResIterator&, SymType sym) const;
  void upperVectorMatrix(const std::vector<Number>&, const std::vector<Number>&,
                         MatIterator&, VecIterator&, ResIterator&, SymType) const;
  void rowVectorMatrix(const std::vector<Number>&, const std::vector<Number>&,
                       MatIterator&, VecIterator&, ResIterator&)  const;
  void columnVectorMatrix(const std::vector<Number>&, const std::vector<Number>&,
                          MatIterator&, VecIterator&, ResIterator&)  const;
\end{lstlisting}
\vspace{.1cm} 
Besides the product of matrix and vector, the  \class{CsStorage} provides solvers for matrix in its diagonal, lower and upper part
\vspace{.1cm}
\begin{lstlisting}
    template<typename MatIterator, typename VecIterator, typename ResIterator>
    void bzSorDiagonalMatrixVector(MatIterator& itd, const VecIterator& itvb,
                                   ResIterator& itrb, const Real w) const;
                                   
    template<typename MatIterator, typename VecIterator, typename XIterator>
    void bzSorLowerSolver(const MatIterator&, const MatIterator&,
                          VecIterator&, XIterator&, XIterator&,
                          const std::vector<Number>&, const std::vector<Number>&, const Real) const;
                          
    template<typename MatIterator, typename VecIterator, typename XIterator>
    void bzSorDiagonalSolver(const MatIterator& itdb, VecIterator& itbb,
                             XIterator& itxb, XIterator& itxe, const Real w) const;
                             
    template<typename MatRevIterator, typename VecRevIterator, typename XRevIterator>
    void bzSorUpperSolver(const MatRevIterator&, const MatRevIterator&,
                          VecRevIterator&, XRevIterator&, XRevIterator&,
                          const std::vector<Number>&, const std::vector<Number>&, const Real) const;
\end{lstlisting}
\vspace{.1cm} 
Addition of two matrices are implemented by
\vspace{.1cm}
\begin{lstlisting}
    template<typename Mat1Iterator, typename Mat2Iterator, typename ResIterator>
    void sumMatrixMatrix(Mat1Iterator&, Mat2Iterator&, ResIterator&, ResIterator&) const;
\end{lstlisting}
\vspace{.1cm} 
\displayInfos{library=largeMatrix, header=CsStorage.hpp, implementation=CsStorage.cpp, test={test\_LargeMatrixCstorage.cpp}, header dep={MatrixStorage.hpp, config.h, utils.h}}

\subsection{\classtitle{ Row/ColCsStorage} classes}

As mentioned before, row compressed storage are based on the column indices vector and the row pointers vector. So the \class{RowCsStorage} class carries these two vectors:
\vspace{.1cm}
\begin{lstlisting}
class RowCsStorage : public CsStorage
{protected :
  std::vector<Number> colIndex_;   //vector of column index of nonzero value
  std::vector<Number> rowPointer_; //vector of positions of begining of rows
  \ldots
\end{lstlisting}
\vspace{.2cm} 
The class provides a default constructor with no construction of storage vectors and constructors from a list of \((i,j)\) index (coordinates) or a list of column indices by row, constructing the compressed storage vectors:
\vspace{.1cm}
\begin{lstlisting}
RowCsStorage(Number nr=0, Number nc=0);
RowCsStorage(Number, Number, const std::vector< std::vector<Number> >&,
             const std::vector< std::vector<Number> >&); 
RowCsStorage(Number, Number, const std::vector< std::vector<Number> >&); 
~RowCsStorage() {}; 
\end{lstlisting}
\vspace{.2cm} 
The class provides most of the virtual methods declared in \class{MatrixStorage} class. For sake of simplicity, we report here only function with \verb?Real? type but versions with \verb?Complex?, \verb?Matrix<Real>?, \verb?Matrix<Complex>? and mixed types are also provided. Some template line declarations are also omitted.
\vspace{.1cm}
\begin{lstlisting}
Number size() const;
Number pos(Number i, Number j, SymType s=_noSymmetry) const;
void positions(const std::vector<Number>&, const std::vector<Number>&,
               std::vector<Number>&, bool errorOn=true, 
               SymType s=_noSymmetry) const;
void printEntries(std::ostream&, const std::vector<Real>&, Number, SymType) const;
void printCooMatrix(std::ostream&, const std::vector<Real>&, 
                    SymType s=_noSymmetry) const;
void loadFromFileDense(std::istream&, std::vector<Real>&, SymType, bool );
void loadFromFileCoo(std::istream& , std::vector<Real>&, SymType, bool);
template<typename M, typename V, typename R>
  void multMatrixVector(const std::vector<M>&, const std::vector<V>&, 
                        std::vector<R>&) const; 
  void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, 
                        std::vector<R>&) const;
  void multMatrixVector(const std::vector<Real>&, const std::vector<Real>&, 
                        std::vector<Real>&, SymType) const;
  void multVectorMatrix(const std::vector<Real>&, const std::vector<Real>&, 
                        std::vector<Real>&, SymType) const;
template<typename M1, typename M2, typename R>
  void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const; //!< templated row dense Matrix + Matrix
  void addMatrixMatrix(const std::vector<Real>& m, const std::vector<Real>& v, std::vector<Real>& rv) const
    { addMatrixMatrix<>(m, v, rv);}
\end{lstlisting}
\vspace{.1cm} 
There is no \verb?printDenseMatrix? function because the general one defined in \class{MatrixStorage} class is sufficiently efficient. \\
\\
The \class{ColCsStorage} class is very similar to the \class{RowCsStorage} class, except that it manages a row indices vector and a column pointers vector:
\vspace{.1cm}
\begin{lstlisting}
class ColCsStorage : public CsStorage
{protected :
  std::vector<Number> rowIndex_;   //!< vector of row index of nonzero value
  std::vector<Number> colPointer_; //!< vector of positions of begining of cols
  \ldots
\end{lstlisting}
\vspace{.2cm}
Its constructors have a similar structure (same arguments) and all prototypes of member functions are the same as \class{ColCsStorage} member functions. We do not repeat them. 
\vspace{.1cm}
XXX=Row or Col
\displayInfos{library=largeMatrix, header=XXXCsStorage.hpp, implementation=XXXCsStorage.cpp, test={test\_LargeMatrixCsStorage.cpp}, header dep={CsStorage.hpp, MatrixStorage.hpp, config.h, utils.h}}

\subsection{\classtitle{Dual/SymCsStorage} classes}

Dual compressed storage travels the matrix as diagonal, strict lower and upper parts. Column indices and row pointers vectors are attached to strict lower part and row indices and column pointers vectors are attached to strict upper part:
\vspace{.1cm}
\begin{lstlisting}
class DualCsStorage : public CsStorage
{protected :
  std::vector<Number> colIndex_;   //vector of column index of nonzero value 
  std::vector<Number> rowPointer_; //vector of positions of begining of rows
  std::vector<Number> rowIndex_;   //vector of row index of nonzero value 
  std::vector<Number> colPointer_; //vector of positions of begining of cols
  \ldots
\end{lstlisting}
\vspace{.2cm} 
\class{DualCsStorage} class provides basic constructor (no construction of storage vectors), constructor from a pair of global numbering vectors or list of column indices by rows.
Both of them create the compressed storage vector using the auxiliary function \verb?buildStorage?:
\vspace{.1cm}
\begin{lstlisting}
DualCsStorage(Number nr=0, Number nc=0);
DualCsStorage(Number, Number, const std::vector< std::vector<Number> >&,
              const std::vector< std::vector<Number> >&);  
DualCsStorage(Number, Number, const std::vector<std::vector<Number> >&);
void buildStorage(const std::vector<std::vector<Number> >&);
~DualCsStorage() {};   
\end{lstlisting}
\vspace{.2cm} 
The class provides most of the virtual methods declared in \class{MatrixStorage} class. For sake of simplicity, we report here only function with \verb?Real? type but versions with \verb?Complex?, \verb?Matrix<Real>?, \verb?Matrix<Complex>? and mixed types are also provided. Some template line declarations are also omitted. 
\vspace{.1cm}
\begin{lstlisting}
Number size() const;
Number lowerPartSize() const ;
    Number upperPartSize() const;
Number pos(Number i, Number j, SymType s=_noSymmetry) const;
void positions(const std::vector<Number>&, const std::vector<Number>&,
               std::vector<Number>&, bool errorOn=true, 
               SymType s=_noSymmetry) const;
void printEntries(std::ostream&, const std::vector<Real>&, Number, SymType) const;
void printCooMatrix(std::ostream&, const std::vector<Real>&, 
                    SymType s=_noSymmetry) const;
void loadFromFileDense(std::istream&, std::vector<Real>&, SymType, bool );
void loadFromFileCoo(std::istream& , std::vector<Real>&, SymType, bool);
template<typename M, typename V, typename R>
  void multMatrixVector(const std::vector<M>&, const std::vector<V>&, 
                        std::vector<R>&) const; 
  void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, 
                        std::vector<R>&) const;
  void multMatrixVector(const std::vector<Real>&, const std::vector<Real>&, 
                        std::vector<Real>&, SymType) const;
  void multVectorMatrix(const std::vector<Real>&, const std::vector<Real>&, 
                        std::vector<Real>&, SymType) const;
template<typename M1, typename M2, typename R>
  void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const;
  void addMatrixMatrix(const std::vector<Real>& m, const std::vector<Real>& v, std::vector<Real>& rv) const
    { addMatrixMatrix<>(m, v, rv);}
\end{lstlisting}
\vspace{.2cm} 

The \class{SymCsStorage} class is very similar to the \class{DualCsStorage} class, except that it addresses only square matrix with symmetric storage and, 
thus, does not manage row indices vector and column pointers vectors because the upper part has same storage as lower part (transposed). Note that matrix may have no symmetry property but a symmetric storage. In that case, the upper part is stored. 
\vspace{.1cm}
\begin{lstlisting}
class SymCsStorage : public CsStorage
{protected :
  std::vector<Number> colndex_;   //!< vector of row index of nonzero value
  std::vector<Number> rowPointer_; //!< vector of positions of begining of cols
  \ldots
\end{lstlisting}
\vspace{.1cm}
Contrary to the row compressed storage, in the SymCsStorage, the diagonal is first stored then the lower part of the matrix. \\
Its constructors have a similar structure (same arguments) to the \class{DualCsStorage} constructors and all prototypes of member functions are the same as \class{DualCsStorage} member functions. We do not repeat them.

Different from other \class{CsStorage} classes, the \class{SymCsStorage} class provides some functions to deal with the \class{SorSolver} class and \class{SsorSolver} class. Only specialized functions with \verb?Real? are listed in the following 
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] x}]
void sorDiagonalMatrixVector(const std::vector<Real>& m, const std::vector<Real>& v, std::vector<Real>& rv, const Real w) const
{ sorDiagonalMatrixVector<>(m, v, rv, w); }
void sorLowerMatrixVector(const std::vector<Real>& m, const std::vector<Real>& v, std::vector<Real>& rv, const Real w, const SymType sym) const
{ sorLowerMatrixVector<>(m, v, rv, w, sym); }
void sorUpperMatrixVector(const std::vector<Real>& m, const std::vector<Real>& v, std::vector<Real>& rv, const Real w, const SymType sym) const
{ sorUpperMatrixVector<>(m, v, rv, w, sym); }
void sorDiagonalSolve(const std::vector<Real>& m, const std::vector<Real>& b, std::vector<Real>& x, const Real w) const
{ sorDiagonalSolve<>(m, b, x, w); }
void sorLowerSolve(const std::vector<Real>& m, const std::vector<Real>& b, std::vector<Real>& x, const Real w) const
{ sorLowerSolve<>(m, b, x, w); }
void sorUpperSolve(const std::vector<Real>& m, const std::vector<Real>& b, std::vector<Real>& x, const Real w, const SymType sym) const
{ sorUpperSolve<>(m, b, x, w, sym); }
\end{lstlisting}
\vspace{.1cm}
XXX=Dual or Sym

\displayInfos{library=largeMatrix, header=XXXCsStorage.hpp, implementation=XXXCsStorage.cpp, test={test\_LargeMatrixCsStorage.cpp}, header dep={CsStorage.hpp, MatrixStorage.hpp, config.h, utils.h}}
