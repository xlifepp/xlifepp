%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{Element} class}

A finite element is mainly defined by its geometric support and its finite element interpolation. It carries also its DoF numbering. The dedicated \class{Element} class is defined as follows:

\vspace{0.1cm}
\begin{lstlisting}[deletekeywords={[3] DoFNumbers}]
class Element
{
  protected:
    FeSpace* feSpace_p;             //pointer to Finite Element parent space
    Number number_;                 //element number in FeSpace
  public:
    FeSubSpace* feSubSpace_p;       //pointer to Finite Element parent sub space 
    RefElement* refElt_p;           //pointer to reference element object
    GeomElement* geomElt_p;         //pointer to Geometric Element support
    std::vector<Number> DoFNumbers; //global numbering of element DoFs 
    std::vector<Element*> parents_; // parent elements when a side of element
};
\end{lstlisting}
\vspace{0.1cm}

The \class{Element} class provides:
\begin{itemize}
\item One constructor
\vspace{0.1cm}
\begin{lstlisting}
Element(FeSpace*, Number, RefElement*, GeomElement*, FeSubspace*=0);
\end{lstlisting}
\vspace{0.1cm}
\item Some accessors:
\vspace{0.1cm}
\begin{lstlisting}
Dimen dim() const;             //dimension of the element
Number number() const;         //return number_ attribute
ShapeType shapeType() const;   //shape of the element
FEType feType() const;         //FE type ( _Lagrange, _Hermite, \ldots)
Number feOrder() const;        //FE numtype (order for Lagrange)
Dimen dimFun() const;          //dimension of shape functions
\end{lstlisting}
\vspace{0.1cm}
\item A DoFs renumbering function and a tool to get index of DoF:
\vspace{0.1cm}
\begin{lstlisting}
void DoFsRenumbered(const std::vector<Number>&, std::vector<FeDof>&);
number_t refDofNumber(number_t) const;
\end{lstlisting}
\vspace{0.1cm}
\item Some utilities to compute normal vectors at a physical point and to access to:
\vspace{0.1cm}
\begin{lstlisting}
ShapeValues computeShapeValues(const Point&, bool withderivative = false) const;   
Vector<real_t> computeNormalVector(const Point&) const;                                
Vector<real_t> computeNormalVector() const;                                            
const Vector<real_t>& normalVector() const;                                            
Vector<real_t>& normalVector();                                                        
\end{lstlisting}
\vspace{0.1cm}
\item Some interpolation tools:
\vspace{0.1cm}
\begin{lstlisting}
Point toReferenceSpace(const Point& p) const; // physical to reference space
template<typename T>
  T& interpolate(const Vector<T>&, const Point&, const std::vector<number_t>&, 
                 T&, DiffOpType =_id) const;
template<typename T>
   Vector<T>& interpolate(const Vector<T>&, const Point&, 
                          const std::vector<number_t>&, Vector<T>&, 
                          DiffOpType =_id) const;                           
template<typename T>
  T& interpolate(const Vector<Vector<T> >&, const Point&, 
                 const std::vector<number_t>&, T&, 
                 DiffOpType = _id)const;                                  
template<typename T>
  T& interpolate(const VectorEntry&, const Point&, 
                 const std::vector<number_t>&, T&, 
                 DiffOpType =_id)const;                                       
\end{lstlisting}

\begin{focusbox}
Interpolation tools are useful when evaluating an element of FE space at a given point, say \(P\). It is an expansive process requiring to locate the element containing the point \(P\), to get its location in reference space (inverse map) to apply local interpolation. The previous \cmd{interpolate} functions are not concerned by the localization of element containing \(P\), they only compute local interpolation.
\end{focusbox}

\item A function dedicated to split a finite element in first order finite elements:
\vspace{0.1cm}
\begin{lstlisting}[deletekeywords={[3] map}]
std::vector<std::pair<ShapeType, std::vector<Number> > > 
     splitO1(std::map<Number,Number>* renumbering) const; 
\end{lstlisting}
\vspace{0.1cm}
\item Some print functions:
\vspace{0.1cm}
\begin{lstlisting}
void print(std::ostream&) const; //!< print utility
friend std::ostream& operator<<(std::ostream&, const Element&);
\end{lstlisting}
\vspace{0.1cm}
\end{itemize}

\displayInfos{library=space, header=Element.hpp, implementation=Element.cpp, test=test\_Space.cpp,
header dep={config.h, utils.h, finiteElements.h, geometry.h, Dof.hpp, SpectralBasis.hpp}}
