%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lun\'eville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
\section{Some basic numerical tools}

Are collected here various basic numerical tools: binomial coefficients, roots of low degree polynomials,  simple quadratures, FFT, ode solver, random generators.

\subsection{Binomial coefficients and polynomial roots}

\xlifepp provides 3 functions related to binomial coefficients:
\begin{lstlisting}
Number_t binomialCoefficient(int n, int k); // compute C_n^k = = n!/(k!*(n-k)!)
void binomialCoefficients(vector<number_t>& row); // compute n-th row of Pascal binomial coefficients 
void binomialCoefficientsScaled(vector<real_t>& row); // compute n-th row of Pascal binomial coefficients scaled by (n-1)! 
\end{lstlisting}
and functions given roots of polynomials of degree 2, 3 (Cardan formula), 4 (Ferrari formula):
\begin{lstlisting} 
vector<complex_t> quadratic(real_t a, real_t b, real_t c);          //roots of degree 2 polynomial (real)
vector<complex_t> quadratic(complex_t a, complex_t b, complex_t c); //roots of degree 2 polynomial (complex)
vector<complex_t> cardan(real_t a, real_t b, real_t c, real_t d);   //roots of degree 3 polynomial (real)
vector<complex_t> cardan(complex_t a, complex_t b, complex_t c, complex_t d);// roots of degree 3 polynomial (complex)
vector<complex_t> ferrari(real_t a, real_t b, real_t c, real_t d, real_t e); // roots of degree 4 polynomial 
\end{lstlisting}
\subsection{Basic quadrature methods and FFT}
\xlifepp proposes some useful basic quadrature methods (rectangle, trapeze, Simpson, Laguerre, adaptive trapeze) to compute 1D integrals. They compute integrals of real/complex function either given by a C++ function of the following form, T being a real value (float, double, \ldots) type or a complex type (complex<float>, complex<double>, \ldots):
\begin{lstlisting} 
T f(real_t){\ldots}
T f(real_t,Parameters&){\ldots}
\end{lstlisting}
or by a vector of values given by an explicit vector or an iterator. So, all the methods are templated by the type T of function values and possibly by a generic iterator (say Iterator).  
\begin{lstlisting} 
T rectangle(number_t n, real_t h, Iterator itb, T& intg);
T rectangle(const vector<T>& f, real_t h);
T rectangle(T(*f)(real_t), real_t a, real_t b, number_t n);
T rectangle(T(*f)(real_t, Parameters&), Parameters& pars, real_t a, real_t b, number_t n);

T trapz(number_t n, real_t h, Iterator itb, T& intg);
T trapz(const vector<T>& f, real_t h);
T trapz(T(*f)(real_t), real_t a, real_t b, number_t n);
T trapz(T(*f)(real_t, Parameters&), Parameters& pars, real_t a, real_t b, number_t n);

T simpson(number_t n, real_t h, Iterator itb, T& intg);
T simpson(const vector<T>& f, real_t h);
T simpson(T(*f)(real_t), real_t a, real_t b, number_t n);
T simpson(T(*f)(real_t, Parameters&), Parameters& pars, real_t a, real_t b, number_t n);

T laguerre(T(*f)(real_t), real_t t0, real_t a, number_t nq, vector<real_t>& points, vector<real_t>& weights);
T laguerre(T(*f)(real_t,Parameters&), Parameters& pars, real_t t0, real_t a, number_t nq, vector<real_t>& points, vector<real_t>& weights);

T adaptiveTrapz(T(*f)(real_t), real_t a, real_t b, real_t eps=1E-6)
T adaptiveTrapz(T(*f)(real_t,Parameters&), Parameters& pars, real_t a, real_t b, real_t eps=1E-6)
\end{lstlisting}
Note that Laguerre's quadrature uses a table of points and weights created by the function
\begin{lstlisting} 
void LaguerreTable(number_t n, std::vector<real_t>& points, std::vector<real_t>& weights);    
\end{lstlisting}
and \cmd{adaptativeTrapz} uses the class
\begin{lstlisting} 
template<typename T> class TrapzInterval
{public :
  real_t x1,x2;
  T f1, f2;
  TrapzInterval(real_t a, real_t b, const T& fa, const T& fb)   : x1(a), x2(b), f1(fa), f2(fb) {}
};
\end{lstlisting}
\vspace{2mm}
Besides, some elementary FFT (Fast Fourier Transformation-1d) tools are provided doing the FFT or inverse FFT of a vector of real/complex values (template T), the number \(N\) of values being of the form \(2^n\). If not, the number used in FFT and IFFT is the closest \(\tilde{N}=2^n\) not greater than \(N\). This vector of values can be passed either as an explicit vector or an implicit one through an iterator:
\begin{lstlisting} 
template<typename IterA, typename IterB> 
 void ffta(IterA ita, IterB itb, number_t log2n, real_t q)
 void fft(IterA ita, IterB itb, number_t log2n)
 void ifft(IterA ita, IterB itb, number_t log2n)

template<typename T>
 vector<complex_t> fft(const vector<T>& f);
 vector<complex_t>& fft(const vector<T>& f, vector<complex_t>& g);
 vector<complex_t> ifft(const std::vector<T>& f);
 vector<complex_t>& ifft(const std::vector<T>& f, std::vector<complex_t>& g);
 
number_t bitReverse(number_t x, int log2n);
\end{lstlisting}
Implementation of the FFT comes from C++ Cookbook by Jeff Cogswell, Jonathan Turkanis, Christopher Diggins, D. Ryan Stephen.
\subsection{ODE solvers}
\xlifepp provides some ODE solvers : Euler, Runge-Kutta 4 and Runge-Kutta 45 that is an adaptive step time solver based on the Dortmand-Prince method (see \url{http://en.wikipedia.org/wiki/Dormand-Prince_method}). All solvers are templated by the type of the state \(y\in V\) involved in the first order differential equation:
\[
\left\{\ \begin{array}{ll}
    y'(t)=f(t,y(t))&\ t\in]t_0,t_f[\\
    y(t_0)=y_0
    \end{array}\right.\]
with \(f: ]t_0,t_f[\times V \rightarrow V\) a non-stiff function.\\

ODE solvers are based on template abstract class \class{OdeSolver} that collects common stuff:
\begin{lstlisting} 
template <class T> class OdeSolver
{protected :
  T& (*f_)(real_t, const T&, T&);    // f(t,x) : R x K -> K (K=R, C, Rn, Cn, \ldots)
  real_t t0_,tf_;       // initial and final time
  real_t dt_;           // time step
  T y0_;                // initial state
  std::list<std::pair<real_t,T> > tys_;    // list of (time step, state)
  string_t name_;       // name of solver
  public :
  OdeSolver(T& (*f)(real_t, const T&, T&),real_t t0, real_t tf, real_t dt, const T& y0, const string_t& na="");
  virtual compute()=0;  //abstract class
  const std::list<std::pair<real_t,T> >& tys() const; // access to (time, state) sequence
  vector<T> states() const; // access to states y
  vector<T> times() const   // access to time step t
  void saveToFile(const string_t& fn) const;
};
\end{lstlisting}
The user function \(f\) may be any C++ function of the form (T any scalar/vector/matrix type):
\begin{lstlisting} 
T& f(real_t t, const T& y, T& fty);  //fty the returned value
// for instance, with XLiFE++ common types
Real f(Real t, const Real& y, Real& fty){\ldots; return fty;}                // real scalar ODE
Complex& f(Real t, const Complex& y, Complex& fty){\ldots; return fty;}      // complex scalar ODE
Reals& f(Real t, const Reals& y, Reals& fty){\ldots; return fty;}            // real vector ODE
Complexes& f(Real t, const Complexes& y, Complexes& fty){\ldots; return fty;}// complex vector ODE
\end{lstlisting} 
Particular solvers are classes inheriting from \class{OdeSolver} that handle some additional data and provide a constructor and the \verb?compute? function:
\begin{lstlisting}
template <class T> class EulerT : public OdeSolver<T>
{ public :
  EulerT(T& (*f)(real_t, const T&, T&), real_t t0, real_t tf, real_t dt,const T& y0);
  void compute();
};

template <class T> class RK4T : public OdeSolver<T>
{ public :
    RK4T(T& (*f)(real_t, const T&, T&), real_t t0, real_t tf, real_t dt,const T& y0);
    void compute();
};

template <class T> class Ode45T : public OdeSolver<T>
{public :
 real_t a,b,c,d,s;            //coefficients of RK45
 number_t nbTry_;             // number of try when adapting time step
 real_t minScale_, maxScale_; // min and max scale of time step
 real_t prec_;                // precision required
 Ode45T(T& (*f)(real_t, const T&, T&), real_t t0, real_t tf, real_t dt,const T& y0, real_t prec=1E-6);
 Ode45T(T& (*f)(real_t, const T&, T&), real_t t0, real_t tf, real_t dt,const T& y0,
        number_t nbtry, real_t minscale, real_t maxscale,real_t prec=1E-6)
 void compute();
 real_t rk45(real_t t, T& y, T& yy, real_t dt);  //one step of Runge-Kutta 4-5
};    
\end{lstlisting} 
When solving real scalar ODE, the following aliases are available:
\begin{lstlisting}
typedef EulerT<real_t> Euler;
typedef RK4T<real_t> RK4;
typedef Ode45T<real_t> Ode45;   
\end{lstlisting}
In adaptive RK45 method, the time step may grow to become very large. To still get a meaningful number of times step, the time step is limited by the initial guess \verb?dt?. Even if the initial guess \verb?dt? is very large, the method should be convergent if the problem is not stiff.\\

The following example deals with the linear pendulum (ODE of order 2):
\begin{lstlisting}
Real omg=1., th0=pi_/4, thp0=0.;
Reals& f(Real t, const Reals& y, Reals& fyt)
{fyt.resize(2);
 fyt[0]=y[1]; fyt[1]=-omg*omg*y[0];  
 return fyt;}
Reals yex(Real t) //exact solution
{Reals yy(2,0.);
 yex[0] = thp0*sin(omg*t)/omg+ th0*cos(omg*t);
 yex[1] = thp0*cos(omg*t)- th0*omg*sin(omg*t);
 return yex;}
\ldots
Real a=0,b=1,dt=0.01, errsup=0;
Reals y0(2,0.);y0[0]=th0; y0[1]=thp0;
list<pair<Real,Reals>> ty = EulerT<Reals>(f,a,b,dt,y0).tys();
for(auto it=ty.begin();it!=ty.end();++it) 
    errsup=max(errsup,norm(it->second-yex(it->first)));
theCout<<"Euler : nb dt="<<ty.size()<<" error sup = "<<errsup<<eol;

ty = RK4T<Reals>(f,a,b,dt,y0).tys(); errsup=0.;
for(auto it=ty.begin();it!=ty.end();++it) 
    errsup=max(errsup,norm(it->second-yex(it->first)));
theCout<<"RK4   : nb dt="<<ty.size()<<" error sup = "<<errsup<<eol;

ty = Ode45T<Reals>(f,a,b,0.1,y0).tys(); errsup=0.;
for(auto it=ty.begin();it!=ty.end();++it) 
   errsup=max(errsup,norm(it->second-yex(it->first)));
theCout<<"Ode45 : nb dt="<<ty.size()<<" error sup = "<<errsup<<eol;
\end{lstlisting}
As the  \verb?compute()? function is called by the constructors, so once the object is created, the field is computed! The previous code gives
\begin{verbatim}
Euler : nb dt=101 error sup = 0.00393671
RK4   : nb dt=101 error sup = 6.54499e-11
Ode45 : nb dt=12  error sup = 5.25629e-08
\end{verbatim}
showing the advantage to use Ode45!\\

The following end user functions (wrapping ode objects) are also provided:
\begin{lstlisting}
template <typename T>
  Vector<T> euler(T& (*f)(real_t,const T&,T&), real_t a, real_t b, real_t dt, const T& y0);
  Vector<T> rk4(T& (*f)(real_t,const T&,T&), real_t a, real_t b, real_t dt, const T& y0);
  pair<Vector<real_t>,Vector<T> > ode45(T& (*f)(real_t,const T&,T&), real_t a, real_t b, 
                                       real_t dt, const T& y0, real_t prec=1.E-6);
\end{lstlisting}

\subsection{Random generators}

\xlifepp provides two random generators: uniform distribution and normal distribution. When C+11 is available (most of the time), it wraps STL random generators. If not, uniform distribution is based on the standard C random generator (rand) and normal distribution is built from the Box-Muller or Marsaglia methods. The proposed methods address either scalar or vector distribution :
\begin{lstlisting}
template <typename T>
void uniformDistribution(vector<T>& v, real_t a=0., real_t b=1.);
void uniformDistribution(vector<T>& mat, number_t n, number_t m, real_t a=0., real_t b=1.);
vector<T> uniformDistribution(number_t n, real_t a=0., real_t b=1.);
vector<T> uniformDistribution(number_t n, number_t m, real_t a=0., real_t b=1.);
void normalDistribution(vector<T>& v, real_t mu=0., real_t sigma=1.);
void normalDistribution(vector<T>& mat, number_t n, number_t m, real_t mu=0., real_t sigma=1.);
vector<T> normalDistribution(number_t n, real_t mu=0., real_t sigma=1.);
vector<T> normalDistribution(number_t n, number_t m, real_t mu=0., real_t sigma=1.);
\end{lstlisting}
These end user functions call effective random generators:
\begin{lstlisting}
//uniform distribution using <random> if C++11 available else using rand(); 
real_t uniformDistribution(real_t a=0., real_t b=1.);                                    
void uniformDistribution(real_t* mat, real_t a, real_t b,number_t n=1, number_t m=1);    
void uniformDistribution(real_t* mat,number_t n=1, number_t m=1);                         
void uniformDistribution(complex_t* mat, real_t a, real_t b, number_t n=1, number_t m=1); 
void uniformDistribution(complex_t* mat, number_t n=1, number_t m=1);                    

//uniform distribution using C style rand() function
real_t uniformDistributionC(real_t a=0., real_t b=1.);                                   
void uniformDistributionC(real_t* mat, real_t a, real_t b,number_t n=1, number_t m=1);   
void uniformDistributionC(real_t* mat,number_t n=1, number_t m=1);                       
void uniformDistributionC(complex_t* mat, real_t a, real_t b, number_t n=1, number_t m=1);
void uniformDistributionC(complex_t* mat, number_t n=1, number_t m=1);                    
    
//normal distribution using <random> if C++11 available else using rand(); 
real_t normalDistribution(real_t mu=0., real_t sigma=1.,GaussianGenerator gg=_MarsagliaGenerator); 
void normalDistribution(real_t* mat, number_t n=1, number_t m=1);                                
void normalDistribution(real_t* mat, real_t mu, real_t sigma, number_t n=1, number_t m=1);         
void normalDistribution(complex_t* mat, real_t mu, real_t sigma, number_t n=1, number_t m=1);   
void normalDistribution(complex_t* mat, number_t n=1, number_t m=1);                              
                         
//normal distribution based on C style rand() function
real_t normalBoxMuller(real_t mu=0., real_t sigma=1.);  
real_t normalMarsaglia(real_t mu=0., real_t sigma=1.); 
real_t normalDistributionC(real_t mu=0., real_t sigma=1.,GaussianGenerator gg=_MarsagliaGenerator); 
void normalDistributionC(real_t* mat, real_t mu, real_t sigma, number_t n=1, number_t m=1);  
void normalDistributionC(real_t* mat, number_t n=1, number_t m=1);  
void normalDistributionC(complex_t* mat, real_t mu, real_t sigma, number_t n=1, number_t m=1);  
void normalDistributionC(complex_t* mat, number_t n=1, number_t m=1); 
void normalDistributionC(real_t* mat, real_t mu, real_t sigma, GaussianGenerator gg, number_t n=1, number_t m=1);  
void normalDistributionC(real_t* mat, GaussianGenerator gg, number_t n=1, number_t m=1); 
void normalDistributionC(complex_t* mat, real_t mu, real_t sigma, GaussianGenerator gg,number_t n=1, number_t m=1); 
void normalDistributionC(complex_t* mat, GaussianGenerator gg, number_t n=1, number_t m=1);                     
\end{lstlisting}
The random engine has to be initialized before. It is done by the \verb?init()? function of \xlifepp using \verb?srand()? or the Mersenne Twister 19937 random engine if C++11 is available.
\section{Special functions, Green kernels and exact solutions}
\xlifepp provides some special functions. Some are parts of \xlifepp, others are provided by the AMOS library through a wrapper. Besides, classical Green functions (Helmholtz, Laplace, Maxwell) and exact solutions of scattering problem are proposed.

\subsection{Special functions}

Notations of special functions follow the HANDBOOK Of MATHEMATICAL FUNCTIONS, Ed. M.ABRAMOWITZ \& I.A. STEGUN. They are computed using either local implementation  or the AMOS library that it is provided by default  when installing \xlifepp.

\subsubsection*{Bessel functions}

\vspace{.1cm}
\begin{lstlisting}
vector<real_t> besselJ0N(real_t x, number_t n);  // internal
real_t    besselJ0(real_t x);                    // internal
real_t    besselJ1(real_t x);                    // internal
real_t    besselJ(real_t x, int_t n);            // internal
complex_t besselJ0(const complex_t& z);          // AMOS
complex_t besselJ1(const complex_t& z);          // AMOS
complex_t besselJ(const complex_t& z, real_t n); // AMOS
real_t    besselJ(real_t x, real_t nu);          // AMOS
complex_t besselJ(const complex_t& z, real_t nu);// AMOS

vector<real_t> besselY0N(real_t x, number_t n);  // internal
\ldots same as J
vector<real_t> besselI0N(real_t x, number_t n);  // internal
\ldots same as J
vector<real_t> besselK0N(real_t x, number_t n);  // internal
\ldots same as J
vector<real_t> besseH10N(real_t x, number_t n);  // internal
\ldots same as J
vector<real_t> besseH20N(real_t x, number_t n);  // internal
\ldots same as J

real_t struveNotH0(real_t x);  // Struve function of order 0, internal
real_t struveNotH1(real_t x);  // Struve function of order 1, internal
pair<real_t, real_t> struveNotH01(real_t x); // returns both H_0(x) and H_1(x), internal

// spherical Bessel function, of first and second kind for 0..N
vector<real_t> sphericalbesselJ0N(real_t x, number_t N); 
vector<real_t> sphericalbesselY0N(real_t x, number_t N); 
\end{lstlisting}
\subsubsection*{Airy functions}
\begin{lstlisting}
complex_t airy(real_t x, DiffOpType d=_id);          // Ai(x) or Ai'(x), AMOS
complex_t airy(const complex_t& z, DiffOpType d=_id);// Ai(z) or Ai'(z), AMOS
complex_t biry(real_t x, DiffOpType d=_id);          // Bi(x) or Bi'(x), AMOS
complex_t biry(const complex_t& z, DiffOpType d=_id);// Bi(z) or Bi'(z), AMOS
\end{lstlisting}
\subsubsection*{Exponential integral}
\[E1(z) = \int_z^{\infty} t^{-1} e^{-t} dt = -\gamma - log(z) + \sum_{n>0} \frac{(-z)^n} {n n!}.\]
\begin{lstlisting}
complex_t e1z(const complex_t& z);   // return E1(z)
complex_t eInz(const complex_t& z);  // return E1(z) + gamma + log(z)
complex_t expzE1z(const complex_t&); // return exp(z)*E1(z)
complex_t zexpzE1z(const complex_t&);// return z*exp(z)*E1(z)
complex_t ascendingSeriesOfE1(const complex_t& z);   // ascending series in E1  (small z)
complex_t continuedFractionOfE1(const complex_t& z); // continued fraction in E1 (large z)
\end{lstlisting}

\subsubsection*{Erf function}

The erf function
\[
\mbox{erf }z =\frac{2}{\sqrt{\pi}}\int_0^z e^{-t^2}dt\]
is available from 
\begin{lstlisting}
complex_t erf(complex_t z);
\end{lstlisting}
that wraps erf stuff coming from libcerf (\url{https://jugit.fz-juelich.de/mlz/libcerf}) developed by  Steven G. Johnson and Joachim Wuttke.

\subsubsection*{Gamma function}

\[
\Gamma(z) = \int_0^{\infty} t^{z-1}e^{-t} dt \mbox{ for }Re(z) > 0\]
with \( \Gamma(1-z)=\displaystyle\frac{\pi}{\sin(\pi z)\Gamma(z)}\), \(\Gamma(z+1)= z \Gamma(z)\) and \(\Gamma(n+1) = n! \ \mbox{ for integer } n > 0\).\\

\begin{lstlisting}
real_t gammaFunction(int_t n);             // return Gamma(n) = (n-1)!
real_t gammaFunction(real_t);              // return Gamma(x) for x > 0
complex_t gammaFunction(const complex_t&); // return Gamma(z) for re(z) > 0
real_t logGamma(real_t);                   // return Log(Gamma(x))
complex_t logGamma1(const complex_t&);     // return Log(Gamma(z))
complex_t logGamma(const complex_t&);      // Paul Godfrey's Lanczos implementation 
real_t diGamma(int_t n);                   // return -gamma + sum_1^{n-1} 1/n
real_t diGamma(real_t);                    // return dx Log(Gamma(x)) for x > 0
complex_t diGamma(const complex_t&);       // return dz Log(Gamma(z)) for re(z) > 0
\end{lstlisting}
\subsubsection*{Polynomials}
See \url{http://en.wikipedia.org/wiki} for definition of following polynomials :
\begin{lstlisting}
void chebyshevPolynomials(real_t, vector<real_t>&);
void gegenbauerPolynomials(real_t lambda, real_t, vector<real_t>&);
void jacobiPolynomials(real_t a, real_t b, real_t, vector<real_t>&);
void jacobiPolynomials01(real_t a, real_t b, real_t, vector<real_t>&); 
void legendrePolynomials(real_t, vector<real_t>&);
void legendrePolynomialsDerivative(real_t, vector<real_t>&);
void legendreFunctions(real_t, vector<vector<real_t> >& Pml);
void legendreFunctionsDerivative(real_t,const vector<vector<real_t>>&,vector<vector<real_t>>&);
\end{lstlisting}

\subsection{Green kernels}

\xlifepp provides kernel (Green function) for Laplace and Helmholtz problems in 2D or 3D and Maxwell problem in 3D. For each problem, the functions computing the kernel and its derivatives are available and a build function constructing the related Kernel object is also provided. We detail  the case of Helmholtz Green function, other kernels being similar.

\subsubsection*{Helmholtz kernels}

The 2D Helmholtz kernel in free space is: 
\[K(x,y)=\frac{i}{4} H^{(1)}_0(k|x-y|)\]
where \( H^{(1)}_0\) is the Hankel function of the first kind of order 0. Its implementation is the following

\begin{lstlisting}
Kernel Helmholtz2dKernel(Parameters& = defaultParameters);     // construct a Helmholtz2d kernel
Kernel Helmholtz2dKernelReg(Parameters& = defaultParameters);  // construct a Helmholtz2d kernel regular part
Kernel Helmholtz2dKernelSing(Parameters& = defaultParameters); // construct a Helmholtz2d kernel singular part
Kernel Helmholtz2dKernel(const real_t& k);        // construct a Helmholtz2d kernel from real k
void initHelmholtz2dKernel(Kernel&, Parameters&); // initialize kernel data
// computation functions
complex_t Helmholtz2d(const Point& x, const Point& y, Parameters& pa = defaultParameters);                  // value
Vector<complex_t> Helmholtz2dGradx(const Point& x, const Point& y, Parameters& pa = defaultParameters);     // gradx
Vector<complex_t> Helmholtz2dGrady(const Point& x, const Point& y, Parameters& pa = defaultParameters);     // grady
Matrix<complex_t> Helmholtz2dGradxy(const Point& x, const Point& y, Parameters& pa = defaultParameters);    // gradxy
complex_t Helmholtz2dNxdotGradx(const Point& x, const Point&y, Parameters& pa = defaultParameters);         // nx.gradx
complex_t Helmholtz2dNydotGrady(const Point& x, const Point&y, Parameters& pa = defaultParameters);         // ny.grady
complex_t Helmholtz2dReg(const Point& x, const Point& y, Parameters& pa = defaultParameters);               // reg value
Vector<complex_t> Helmholtz2dGradxReg(const Point& x, const Point& y, Parameters& pa = defaultParameters);  // reg gradx
Vector<complex_t> Helmholtz2dGradyReg(const Point& x, const Point& y, Parameters& pa = defaultParameters);  // reg grady
Matrix<complex_t> Helmholtz2dGradxyReg(const Point& x, const Point& y, Parameters& pa = defaultParameters); // reg gradxy
complex_t Helmholtz2dSing(const Point& x, const Point& y, Parameters& pa = defaultParameters);              // sing value
Vector<complex_t> Helmholtz2dGradxSing(const Point& x, const Point& y, Parameters& pa = defaultParameters); // sing gradx
Vector<complex_t> Helmholtz2dGradySing(const Point& x, const Point& y, Parameters& pa = defaultParameters); // sing grady
Matrix<complex_t> Helmholtz2dGradxySing(const Point& x, const Point& y, Parameters& pa = defaultParameters);// sing gradxy
\end{lstlisting}
\verb?reg? and \verb?sing? versions  are required when computing integral involving \(x=y\) case and using particular integration method (see Quadrature).
The pseudo Kernel constructors call the \verb?initHelmholtz2dKernel? function that associates computing functions to the Kernel object:
\begin{lstlisting}
void initHelmholtz2dKernel(Kernel& K, Parameters& pars)
{
 K.dimPoint=2;
 K.name="Helmholtz 2D kernel";
 K.shortname="Helmz2D";
 K.singularType = _logr;
 K.singularOrder = 1;
 K.singularCoefficient= -over2pi_;
 K.symmetry=_symmetric;
 K.userData.push(pars);
 K.kernel = Function(Helmholtz2d, 2,K.userData);
 K.gradx = Function(Helmholtz2dGradx, 2, K.userData);
 K.grady = Function(Helmholtz2dGrady, 2, K.userData);
 K.gradxy = Function(Helmholtz2dGradxy, 2, K.userData);
 K.ndotgradx = Function(Helmholtz2dNxdotGradx, 2, K.userData);
 K.ndotgrady = Function(Helmholtz2dNydotGrady, 2, K.userData);
 K.singPart = new Kernel(Helmholtz2dKernelSing(pars));
 K.regPart = new Kernel(Helmholtz2dKernelReg(pars));
}
\end{lstlisting}

This function shows how to proceed to create a new kernel type. But, to use the Helmholtz kernel it is sufficient to build the Kernel object:
\begin{lstlisting}
Kernel H2d = Helmholtz2dKernel(k);
\end{lstlisting}
Besides the free space kernel, \xlifepp provides the 2D Helmholtz kernel in half-space and the 2D Helmholtz kernel in the strip \(]-\infty,+\infty[\times]0,h[\):
\begin{lstlisting}
Kernel Helmholtz2dHalfPlaneKernel(real_t k, real_t t1=1., real_t t2=0., real_t a=0., real_t b=0.,
                                  BoundaryCondionType bct=_Dirichlet);
Kernel Helmholtz2dStripKernel(BoundaryCondionType bct, real_t k, real_t h=1.,
                              number_t n=1000, real_t l=-1., real_t e=1.E-6);  
\end{lstlisting}

Only Dirichlet or Neumann boundary condition are handled. In case of the strip, \(n\) is the maximum of terms in the modal expansion used to compute the kernel.\\

The Helmholtz kernel in 3d free space 
\[K(x, y)=\frac{e^{ik|x-y|}}{4\pi|x-y|}  \]
is also available:
\begin{lstlisting}
Kernel Helmholtz3dKernel(const real_t& k);    // construct a Helmholtz3d kernel from real k
Kernel Helmholtz3dKernel(const complex_t& k); // construct a Helmholtz3d kernel from complex k
\end{lstlisting}

\subsubsection*{Laplace kernels}

 The Laplace kernels in 2d and 3d free space are given by
 \[K(x, y)=\frac{\log(|x-y|}{2\pi} \mbox{ and }K(x, y)=\frac{1}{4 \pi|x-y|}\]
 and can be constructed using:
\begin{lstlisting}
Kernel Laplace2dKernel(Parameters& pars = defaultParameters );
Kernel Laplace3dKernel(Parameters& pars = defaultParameters ); 
\end{lstlisting}
For the moment, they do not provide singular and regular parts!

\subsubsection*{Maxwell kernel}

Only the 3D free space kernel is available:
\[K(x, y)=H_k(x, y)\mathbb{I}_3 +\frac{1}{k^2} \mbox{Hess}\big(H_k(x, y)-H_{k\sqrt{s}}(x, y)\big)\]
where \(H_k\) is the 3d Helmholtz kernel. \(s\) is a regularization parameter (0 by default and means no regularization):
\begin{lstlisting}
Kernel Maxwell3dKernel(const real_t& k, const real_t& s=0);       
Kernel Maxwell3dKernel(const complex_t& k, const real_t& s=0);
\end{lstlisting}

\subsection{Exact solutions}




