%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex

\section {\arpackpp interface layer}

\arpackpp interface is a group of class which is an interface between \xlifepp and \arpackpp. Each class of this group corresponds to a specific eigen problem with a computational mode.\\
\begin{figure} [H]
\centering
\includeTikZPict[width=13cm]{largeMatrixArpackpp.png}
\caption{Hierarchy of LargeMatrixArpackpp}
\end{figure}
\class{LargeMatrixArpackpp} is an interface class between \xlifepp and \arpackpp.  This class is an abstract base class so that all classes to be used in the program are derived from this one. This class defines some basic properties and methods which are used in all derived classes.
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] x, y, name}]
template<typename K, class Mat>
class LargeMatrixArpackpp
{
  public:
    LargeMatrixArpackpp(const Mat& matA, const Number sizeProblem);
    LargeMatrixArpackpp(const Mat& matA, const Number sizeProblem, const String name);
    virtual ~LargeMatrixArpackpp();    
    virtual void multOPx (K* x, K* y) = 0;
    String kindOfFactorization() const {return factName_;}
    
  protected:
    const Mat* matA_p;     //< Left hand-side matrix A, generally stiffness matrix (given data)
    Number size_;    //< Size of problem
    String name_;     //< Name of problem
    String factName_;     //< Name of the algorithm used to compute the factorization pointed to by fact_p    
    Mat* fact_p;  //<Factorization of matB, matAsI or matAsB in order to speed up linear  systems solving.
    
  protected:
    void (Mat::*solver_)(std::vector<K>& b, std::vector<K>& x);
    void checkDims(const Mat* mat);    
    void factorize(const Mat* mat);    
    void bzMultAx(K* x, K* y);     //< Matrix-vector product y <- A * x
    void array2Vector(const K*, std::vector<K>&);     //< Convert array to Vector
    void vector2Array(std::vector<K>&, K*);     //< Convert Vector to Array
}; // end of Class LargeMatrixArpackpp
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=eigenSolvers, test={test\_EigenSolverArpackpp.hpp}, header dep={}}

\subsection {The \classtitle{LargeMatrixArpackppStdReg} class}

\class{LargeMatrixArpackppStdReg} class is an interface between \xlifepp and \arpackpp designed to enable the use of \arpackpp's own classes for standard problems in regular mode. This class allows \xlifepp users to solve real symmetric, non-symmetric and complex standard eigen problem in regular mode. This class holds information of matrices and some methods related to product matrix-vector. In general, only matrix-vector product \(y\leftarrow Ax\)  is needed.
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] x, y}]
template<typename K, class Mat>
class LargeMatrixArpackppStdReg: public LargeMatrixArpackpp<K, Mat>
{
  public:
    LargeMatrixArpackppStdReg(const Mat& matA, const Number sizeProblem);
    virtual ~LargeMatrixArpackppStdReg() {}
    virtual void multOPx(K* x, K* y);     //< Matrix-vector product y <- A * x
    
  private:
    //! Duplicating such object is disabled
    LargeMatrixArpackppStdReg(const LargeMatrixArpackppStdReg&);
    LargeMatrixArpackppStdReg& operator=(const LargeMatrixArpackppStdReg&);
}; // end of Class LargeMatrixArpackppStdReg
\end{lstlisting}
\vspace{.1cm}
\displayInfos{library=eigenSolvers, test={test\_EigenSolverArpackpp.hpp}, header dep={LargeMatrixArpackpp.hpp}}

\subsection{The \classtitle{LargeMatrixArpackppStdShf} class}

\class{LargeMatrixArpackppStdShf} class is an interface between \xlifepp and \arpackpp designed to enable the use of \arpackpp's own classes for standard problems in shift and inverse mode. This class allows \xlifepp users to solve real symmetric, non-symmetric and complex standard eigen problem in shift and inverse mode. This class holds information of matrices and some methods related to product matrix-vector. In general, we need to provide matrix-vector product \(y\leftarrow (A-\sigma I)^{-1}x\). However, thanks to some built-in special solvers of \xlifepp, only \((A-\sigma I)\) is enough.
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] x, y}]
template<typename K, class Mat>
class LargeMatrixArpackppStdShf: public LargeMatrixArpackpp<K, Mat>
{
  public:
    LargeMatrixArpackppStdShf(const Mat& matA, const Number sizeProblem, const K sigma);
    virtual ~LargeMatrixArpackppStdShf();
    virtual void multOPx (K* x, K* y);     //< Matrix-vector product y <- inv(A-sigma*Id) * x
  private:
    const Mat* matAsI_p; //<         A - sigma Id matrix for shift and invert mode in standard problems
    bool localAlloc_;     //< flag to indicate if dynamic memory is allocated to store the matrix (A - sigma*Id)
    
    //! Duplicating such object is disabled
    LargeMatrixArpackppStdShf(const LargeMatrixArpackppStdShf&);
    LargeMatrixArpackppStdShf& operator=(const LargeMatrixArpackppStdShf&);
}; // end of Class LargeMatrixArpackppStdShf
\end{lstlisting}
\vspace{.1cm}
\displayInfos{library=eigenSolvers, test={test\_EigenSolverArpackpp.hpp}, header dep={LargeMatrixArpackpp.hpp}}

\subsection{The \classtitle{LargeMatrixArpackppGen} class}

\class{LargeMatrixArpackppGen} is an abstract class which plays a role of an interface between \xlifepp and \arpackpp designed to enable the use of \arpackpp's own classes for generalized eigen problem \(Ax=\lambda Bx\). All classes for generalized problem are derived from this one.
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] name, x, y}]
template<typename K, class Mat>
class LargeMatrixArpackppGen: public LargeMatrixArpackpp<K, Mat>
{
  public:
    LargeMatrixArpackppGen(const Mat& matA, const Mat& matB, const Number sizeProblem);
    LargeMatrixArpackppGen(const Mat& matA, const Mat& matB, const Number sizeProblem, const String name);
    virtual ~LargeMatrixArpackppGen() {}
    virtual void multBx (K* x, K* y); //<   Matrix-vector product y <- B * x required for all derived classes
    
  protected:
    const Mat* matB_p; //< Right hand-side matrix B, e.g. mass matrix (given data)
    Mat* matAsB_p; //!< Matrix (A - sigma*B)
    bool localAlloc_; //<     flag to indicate if dynamic memory is allocated to store A - sigma B,
    
  private:
    //! Duplicating such object is disabled
    LargeMatrixArpackppGen(const LargeMatrixArpackppGen&);
    LargeMatrixArpackppGen& operator=(const LargeMatrixArpackppGen&);    
}; // end of Class LargeMatrixArpackppGen
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=eigenSolvers, test={test\_EigenSolverArpackpp.hpp}, header dep={LargeMatrixArpackpp.hpp}}

\subsection{The \classtitle{LargeMatrixArpackppGenReg} class}

\class{LargeMatrixArpackppGenReg} class is interface classes between \xlifepp and \arpackpp designed
 to enable the use of \arpackpp's own classes for generalized problems in regular mode except for real symmtric generalize problem. This class holds information of matrices and some methods related to product matrix-vector. In general, we need to provide matrix-vector product \(w\leftarrow (B)^{-1}Ax\) and \(z\leftarrow Bx\)
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] x, y}]
template<typename K, class Mat>
class LargeMatrixArpackppGenReg: public LargeMatrixArpackppGen<K, Mat>
{
  public:
    LargeMatrixArpackppGenReg(const Mat& matA, const Mat& matB, const Number sizeProblem);
    virtual ~LargeMatrixArpackppGenReg() {};
    virtual void multOPx (K* x, K* y);  //< Matrix-vector product y <- inv(B)*A * x
    
  private:
    //! Duplicating such object is disabled
    LargeMatrixArpackppGenReg(const LargeMatrixArpackppGenReg&);
    LargeMatrixArpackppGenReg& operator=(const LargeMatrixArpackppGenReg&);    
}; // end of Class LargeMatrixArpackppGenReg
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=eigenSolvers, test={test\_EigenSolverArpackpp.hpp}, header dep={LargeMatrixArpackppGen.hpp}}

\subsection{The \classtitle{LargeMatrixArpackppSymGenReg} class}

\class{LargeMatrixArpackppGenReg} is basically not different from class \class{LargeMatrixArpackppGenReg}, except on how the calculation results are returned. Maybe in the future, we don't need this class anymore.
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] x, y}]
template<typename K, class Mat>
class LargeMatrixArpackppSymGenReg: public LargeMatrixArpackppGen<K, Mat>
{
  public:
    LargeMatrixArpackppSymGenReg(const Mat& matA, const Mat& matB, const Number sizeProblem);
    virtual ~LargeMatrixArpackppSymGenReg() {}
    virtual void multOPx (K* x, K* y);  //< Matrix-vector product y <- inv(B)*A * x
    
  private:
    //! Duplicating such object is disabled
    LargeMatrixArpackppSymGenReg(const LargeMatrixArpackppSymGenReg&);
    LargeMatrixArpackppSymGenReg& operator=(const LargeMatrixArpackppSymGenReg&);    
}; // end of Class LargeMatrixArpackppSymGenReg
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=eigenSolvers, test={test\_EigenSolverArpackpp.hpp}, header dep={LargeMatrixArpackppGen.hpp}}

\subsection{The \classtitle{LargeMatrixArpackppGenShf} class}

Class \class{LargeMatrixArpackppGenShf} is interface between \xlifepp and \arpackpp designed to enable the use of \arpackpp's own classes for generalized problems in shift and invert mode. All classes for generalized problem in relevant shift and invert mode  (including Buckling and Cayley) are derived from this one.
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] x, y, name}]
template<typename K, class Mat>
class LargeMatrixArpackppGenShf: public LargeMatrixArpackppGen<K, Mat>
{
  public:
    LargeMatrixArpackppGenShf(const Mat& matA, Mat& matB, const Number sizeProblem, const K sigma, const String name = "Generalized Shift and Invert");
    virtual ~LargeMatrixArpackppGenShf() {}
    virtual void multOPx (K* x, K* y); //< Matrix-vector product y <- inv(A-sigma B)*x
    
  private:
    //! Duplicating such object is disabled
    LargeMatrixArpackppGenShf(const LargeMatrixArpackppGenShf&);
    LargeMatrixArpackppGenShf& operator=(const LargeMatrixArpackppGenShf&);
}; // end of Class LargeMatrixArpackppGenShf
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=eigenSolvers, test={test\_EigenSolverArpackpp.hpp}, header dep={LargeMatrixArpackppGen.hpp}}

\subsection{The \classtitle{LargeMatrixArpackppGenBuc} class}

Class \class{LargeMatrixArpackppGenBuc} is derived from class \class{LargeMatrixArpackppGenShf} to solve generalized problems Buckling mode. Two products matrix-vector need provided: \(w\leftarrow (A-\sigma B)^{-1}x\) and \(z\leftarrow Ax\)
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] x, y}]
template<typename K, class Mat>
class LargeMatrixArpackppGenBuc: public LargeMatrixArpackppGenShf<K, Mat>
{
  public:
    LargeMatrixArpackppGenBuc(const Mat& matA, Mat& matB, const Number sizeProblem, const K sigma);
    virtual ~LargeMatrixArpackppGenBuc() {}
    virtual void multBx (K* x, K* y);//<  Matrix-vector product y <- A * x
    
  private:
    //! Duplicating such object is disabled
    LargeMatrixArpackppGenBuc(const LargeMatrixArpackppGenBuc&);
    LargeMatrixArpackppGenBuc& operator=(const LargeMatrixArpackppGenBuc&);
}; // end of Class LargeMatrixArpackppGenBuc
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=eigenSolvers, test={test\_EigenSolverArpackpp.hpp}, header dep={LargeMatrixArpackppGen.hpp}}

\subsection{The \classtitle{LargeMatrixArpackppGenCay} class}

Class \class{LargeMatrixArpackppGenCay} is derived from class \class{LargeMatrixArpackppGenShf} to solve generalized problems in Cayley mode. Three products matrix-vector need provided: \(y\leftarrow (A-\sigma B)^{-1}z\), \(w\leftarrow Az\) and \(u\leftarrow Bz\)
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] x, y}]
template<typename K, class Mat>
class LargeMatrixArpackppGenBuc: public LargeMatrixArpackppGenShf<K, Mat>
{
  public:
    LargeMatrixArpackppGenBuc(const Mat& matA, Mat& matB, const Number sizeProblem, const K sigma);
    virtual ~LargeMatrixArpackppGenBuc() {}
    virtual void multBx (K* x, K* y);//<  Matrix-vector product y <- A * x
    
  private:
    //! Duplicating such object is disabled
    LargeMatrixArpackppGenBuc(const LargeMatrixArpackppGenBuc&);
    LargeMatrixArpackppGenBuc& operator=(const LargeMatrixArpackppGenBuc&);
}; // end of Class LargeMatrixArpackppGenBuc
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=eigenSolvers, test={test\_EigenSolverArpackpp.hpp}, header dep={LargeMatrixArpackppGen.hpp}}

\subsection{The \classtitle{LargeMatrixArpackppGenCsh} class}

Class \class{LargeMatrixArpackppGenCsh} is derived from class \class{LargeMatrixArpackppGen} to solve real non-symmetric generalized problem in complex shift and invert mode. Three products matrix-vector need provided: \(y\leftarrow (A-\sigma B)^{-1}z\), \(w\leftarrow Az\) and \(u\leftarrow Bz\)
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] x, y}]
template<typename K, class Mat>
class LargeMatrixArpackppGenCsh : public LargeMatrixArpackppGen<K, Mat>
{
  public:
    LargeMatrixArpackppGenCsh(const Mat& matA, Mat& matB, const Number sizeProblem, Complex sigma, const char part);
    virtual ~LargeMatrixArpackppGenCsh() {};    
    /*!
        Matrix-vector product y <- real(inv(A - sigma*B)) if part = 'R'
                              y <- imag(inv(A - sigma*B)) if part = 'I'
    */
    void virtual multOPx (K* x, K* y);
    void multAx (K* x, K* y); //< Matrix-vector product y <- A * x
  private:
    //! Duplicating such object is disabled
    LargeMatrixArpackppGenCsh(const LargeMatrixArpackppGenCsh&);
    LargeMatrixArpackppGenCsh& operator=(const LargeMatrixArpackppGenCsh&);
}; // end of Class LargeMatrixArpackppGenCsh
\end{lstlisting}
\vspace{.1cm}
\displayInfos{library=eigenSolvers, test={test\_EigenSolverArpackpp.hpp}, header dep={LargeMatrixArpackppGen.hpp}}

\section{Users interface layer}

User interface contains classes which wrap all \arpackpp interface layer and provide user a simple prototype. All eigensolvers are invoked by operator(), which is similar to other iterative solvers of \lib{solvers} library.  There are two principal classes, each of which solves a specific eigen problem:
\begin{itemize}
\item \class{EigenSolverStdArpackpp} for standard eigen problem \(Ax=\lambda x\);
\item \class{EigenSolverGenArpackpp} for generalized eigen problem \(Ax=\lambda Bx\).
\end{itemize}
For each type of eigen problem, input matrix \verb?A? decides the value type (Real or Complex) of eigenvalue and/or eigenvector found.
There are three cases corresponding to input matrix \verb?A?: real symmetric, complex and real non-symmetric. For the first two cases, the eigenvalue and/or eigenvector found have the same value type of matrix \verb?A? (i.e: Real if matrix \verb?A? is real symmetric and Complex if matrix \verb?A? is complex). For the real non-symmetric case, eigenvalue and/or eigenvector can be complex. Based on value type of input matrix and output eigenvalue (and/or eigenvector), an operator () can be invoked to solve an eigen problem correctly. There are three prototypes of operator() corresponding to three cases:
\begin{itemize}
\item \class{Real symmetric case}: input matrix \verb?A? is real symmetric and output eigenvalues/eigenvectors are real;
\item \class{Real non-symmetric case}: input matrix \verb?A? is real non-symmetric and output eigenvalues/eigenvectors are complex;
\item \class{Complex case}: input matrix \verb?A? is complex and output eigenvalues/eigenvectors are complex.
\end{itemize}
\begin{figure}[H]
\centering
\includeTikZPict[width=13cm]{eigenSolverBaseArpackpp.png}
\caption{\class{EigenSolverBaseArpackpp} class}
\end{figure}

\subsection{The \classtitle{EigenSolverBaseArpackpp} class}

The \class{EigenSolverBaseArpackpp} is the base class which provides some methods to construct object and retrieve eigenvalue/eigenvector returned by \arpackpp. All other wrapper classes are derived from this one.
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] epsilon, size}]
class EigenSolverBaseArpackpp
{
  public:
    EigenSolverBaseArpackpp(const String& nam);
    EigenSolverBaseArpackpp(const String& nam, const Number maxOfIt, const Real epsilon);
    virtual ~EigenSolverBaseArpackpp();    
    // Iterative solver name for documentation purposes
    String name() { return name_; }    
    // Some functions to extract Eigenvalue and EigenVector returned from Arpack
    // Extract EigenValue and EigenVector
    template<class ArProblem, class EigValue, class EigVector>
    Number extractEigenValueVector(ArProblem& arProb,
                                   EigValue& eigValue, EigVector& eigVector,
                                   Number nev, Number size, bool isReturnVector);
                                   
    // Extract EigenValue and EigenVector for non-symmetric problem
    template<class ArProblem, class EigValue, class EigVector>
    Number extractEigenValueVectorNonSym(ArProblem& arProb,
                                         EigValue& eigValue, EigVector& eigVector,
                                         Number nev, Number size, bool isReturnVector);
                                         
    // Set some parameters with user-defined value
    template<class ArProblem>
    void setParameters(ArProblem& arProb);
  protected:
    // Extract result EigenVector
    template<typename K, class ArProblem>
    void extractEigenVector(ArProblem& arProb, std::vector<K>& result, Number size, Number idx);
    
    // Extract result EigenVector of non-symmetric problem
    template<class ArProblem>
    void extractEigenVectorNonSym(ArProblem& arProb, std::vector<Complex>& result, Number size, Number idx, bool isNegative, bool isNegativeLeft);
 \ldots
};   
\end{lstlisting}
\vspace{.1cm}

\subsection{The \classtitle{EigenSolverStdArpackpp} class}

The \class{EigenSolverStdArpackpp} class enables the use of \arpackpp to solve the standard eigen problem \(Ax=\lambda x\) in all computational modes. Moreover, this class simplifies the post-processing stage, the found eigenvalues/eigenvectors are returned directly to user. However, advanced user can easily customize the few lines of code to get more features of \arpackpp (e.g. Schur vector, \ldots). There are three prototypes of operator () corresponding to three cases: real symmetric, complex and  real non-symmetric.
\begin{itemize}
\item Real symmetric:
\vspace{.1cm}
\begin{lstlisting}
template<class Mat>
Number operator()(Mat& matA, Vector<Real>& eigValue, std::vector<Vector<Real> >& eigVector, Number nev, EigenComputationalMode compMode, Real sigma, const char* calMode = "LM")
\end{lstlisting}
\vspace{.1cm}
\item Real non-symmetric:
\vspace{.1cm}
\begin{lstlisting}
template<class Mat>
Number operator()(Mat& matA, Vector<Complex>& eigValue, std::vector<Vector<Complex> >& eigVector, Number nev, EigenComputationalMode compMode, Complex sigma, const char* calMode = "LM")
\end{lstlisting}
\vspace{.1cm}
\item Complex:
\vspace{.1cm}
\begin{lstlisting}
template<class Mat>
Number operator()(Mat& matA, Vector<Complex>& eigValue, std::vector<Vector<Complex> >& eigVector, Number nev, EigenComputationalMode compMode, Real sigma, const char* calMode = "LM")
\end{lstlisting}
\vspace{.1cm}
\end{itemize}

\begin{focusbox}
If \verb?compMode? isn't \verb?_regular?, only largeMatrix with storage of \verb?skylineStorage? can be used as matrix input.
\end{focusbox}

The operator() returns a value specifying the number of eigenvalues/eigenvectors found by \arpackpp. This value is equal or less than the input \verb?nev?, which determines the number of eigenvalues/eigenvectors which user needs. \verb?sigma? determines shift value in Shift mode (also in Buckling and Cayley mode). By default, \verb?nev? eigenvalues with the largest magnitude are turned; however, the function can return eigenvalues with smaller magnitude by changing \verb?calMode? to {"SM"}.

The \class{EigenComputationalMode} is enumeration:
\vspace{.1cm}
\begin{lstlisting}
enum EigenComputationalMode {_regular, _buckling, _cayley, _shiftInv, _cplxShiftInv};
\end{lstlisting}
\vspace{.1cm}
In order to solve eigen problem with \arpackpp, it is necessary to define a \arpackpp problem then link this problem to \xlifepp through \arpackpp interface layer (i.e. \class{LargeMatrixArpackpp}, \ldots). There are three \arpackpp classes which are used to define a standard eigen problem:  \class{ARSymStdEig}, \class{ARNonSymStdEig}, \class{ARCompStdEig}. \\
The following functions define \arpackpp problem and make link to \xlifepp via \arpackpp interface layer.
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] size}]
template<typename K, class MatArpackpp, class Arpackpp, class Mat, class EigValue, class EigVector>
Number regMode(Mat& matA, Number size, Number nev, EigValue& eigValue, EigVector& eigVector, const char* calMode);
Number shfInvMode(Mat& matA, Number size, Number nev, K sigma, EigValue& eigValue, EigVector& eigVector, const char* calMode);
Number regModeNonSym(Mat& matA, Number size, Number nev, EigValue& eigValue, EigVector& eigVector, const char* calMode);
Number shfInvModeNonSym(Mat& matA, Number size, Number nev, K sigma, EigValue& eigValue, EigVector& eigVector, const char* calMode);
\end{lstlisting}
\vspace{.1cm}

\begin{focusbox}
Users can easily customize the parameters of \arpackpp problem corresponding to their need.
\end{focusbox}

\displayInfos{library=eigenSolvers, test={test\_EigenSolverArpackpp.hpp}, header dep={EigenSolverBaseArpackpp.hpp, LargeMatrixArpackppStdReg.hpp, LargeMatrixArpackppStdShf.hpp}}

\subsection{The \classtitle{EigenSolverGenArpackpp} class}

The \class{EigenSolverStdArpackpp} class enables the use of \arpackpp to solve the generalized eigen problem \(Ax=\lambda Bx\) in all computational modes. Moreover, this class simplifies the post-processing stage, the found eigenvalues/eigenvectors are returned directly to user. However, advanced user can easily customize several lines of code to get more features of \arpackpp (e.g. Schur vector, \ldots). There are three prototypes of operator () corresponding to three cases: real symmetric, complex and  real non-symmetric.
\begin{itemize}
\item Real symmetric:
\vspace{.1cm}
\begin{lstlisting}
template<class Mat>
Number operator()(const Mat& matA, Mat& matB, Vector<Real>& eigValue, std::vector<Vector<Real> >& eigVector, Number nev, EigenComputationalMode compMode, Real sigmaR, const char* calMode = "LM");
\end{lstlisting}
\vspace{.1cm}
\item Real non-symmetric:
\vspace{.1cm}
\begin{lstlisting}
template<class Mat>
Number operator()(const Mat& matA, Mat& matB, Vector<Complex>& eigValue, std::vector<Vector<Complex> >& eigVector, Number nev, EigenComputationalMode compMode, Complex sigma, const char* calMode = "LM");
\end{lstlisting}
\vspace{.1cm}
\item Complex:
\vspace{.1cm}
\begin{lstlisting}
template<class Mat>
Number operator()(const Mat& matA, Mat& matB, Vector<Complex>& eigValue, std::vector<Vector<Complex> >& eigVector, Number nev, EigenComputationalMode compMode, Real sigmaR, const char* calMode = "LM");
\end{lstlisting}
\vspace{.1cm}
\item Real non-symmetric with complex shift:
\vspace{.1cm}
\begin{lstlisting}
template<class Mat>
Number operator()(const Mat& matA, Mat& matB, Vector<Complex>& eigValue, std::vector<Vector<Complex> >& eigVector, Number nev, EigenComputationalMode compMode, const char mode, Complex sigma, const char* calMode = "LM");
\end{lstlisting}
\vspace{.1cm}
\end{itemize}

\begin{focusbox}
If \verb?compMode? isn't \verb?_regular?, only largeMatrix with storage of \verb?skylineStorage? can be used as matrix input.
\end{focusbox}

Having matrix {B} as an additional input, the operator() of \class{EigenSolverGenArpackpp} class is called with the same parameters as the case of \class{EigenSolverStdArpackpp}. Moreover, it has one more prototype which is served for real non-symmetric case with complex shift value. Besides the same parameters as other prototypes, it is necessary for user to specify which part of complex shift value:real or imaginary, to use in the shift mode by parameter \verb?mode?. This input can be {'R'} for real part or {'I'} for imaginary part of complex shift value \verb?sigma?.
Same as \class{EigenSolverStdArpackpp}, it needs to define \arpackpp problems and link them to \xlifepp. \class{ARSymGenEig}, \class{ARNonSymGenEig} and \class{ARCompGenEig} class are used to define \arpackpp generalized problem. \\
The following functions define \arpackpp problem and make link to \xlifepp via \arpackpp interface layer.
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] size}]
template<typename K, class MatArpackpp, class Arpackpp, class Mat, class EigValue, class EigVector>
Number regMode(const Mat& matA, const Mat& matB, Number size, Number nev, EigValue& eigValue, EigVector& eigVector, const char* calMode);
Number regModeNonSym(const Mat& matA, const Mat& matB, Number size, Number nev, EigValue& eigValue, EigVector& eigVector, const char* calMode);
Number symShfBucMode(const Mat& matA, Mat& matB, Number size, Number nev, K sigma, const char mode, EigValue& eigValue, EigVector& eigVector, const char* calMode);
Number symCaleyMode(const Mat& matA, Mat& matB, Number size, Number nev, K sigma, EigValue& eigValue, EigVector& eigVector, const char* calMode);
Number shfMode(const Mat& matA, Mat& matB, Number size, Number nev, K sigma, EigValue& eigValue, EigVector& eigVector, const char* calMode);
Number shfModeNonSym(const Mat& matA, Mat& matB, Number size, Number nev, K sigma, EigValue& eigValue, EigVector& eigVector, const char* calMode);
Number complexShfInvMode(const Mat& matA, Mat& matB, Number size, Number nev, Real sigmaR, Real sigmaI, const char mode, EigValue& eigValue, EigVector& eigVector, const char* calMode);
\end{lstlisting}
\vspace{.1cm}

\begin{focusbox}
Users can easily customize the parameters of \arpackpp problem corresponding to their need.
\end{focusbox}

\displayInfos{library=eigenSolvers, test={test\_EigenSolverArpackpp.hpp}, header dep={EigenSolverBaseArpackpp.hpp, LargeMatrixArpackppGenReg.hpp, LargeMatrixArpackppGenShf.hpp}}

\subsection{Examples}

The following matrices and vectors are used for all examples below.
\vspace{.1cm}
\begin{lstlisting}
const Number rowNum = 20;
const Number colNum = 20;  
const Number nev = 15;
const String rMatrixDataSym("rSym20.data");
const String rMatrixDataSymPosDef("rSymPos20.data");
const String rMatrixDataNoSym("rnonSym20.data");  
const String cMatrixDataSym("c20.data");
const String cMatrixData("c20b.data");
Vector<Real> rEigValue(colNum);
Vector<Complex> cEigValue(colNum);
std::vector<Vector<Real> > rEigVector(nev, rEigValue);
std::vector<Vector<Complex> > cEigVector(nev, cEigValue);

LargeMatrix<Real> rMatSkylineSym(dataPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _skyline, _sym);
LargeMatrix<Real> rMatSkylineSymSym(dataPathTo(rMatrixDataSym), _dense, rowNum, _skyline, _symmetric);
  
// A trick to make matrix A and matrix B have the same storage (just for testing purpose)
LargeMatrix<Real> rMatPosDefTemp(dataPathTo(rMatrixDataSymPosDef), _dense, rowNum, _skyline, _symmetric);
LargeMatrix<Real> rMatPosDefSym(rMatSkylineSymSym);
  for (Number i = 1; i < rowNum + 1; i++)
    for (Number j = 1; j < colNum + 1; j++)
    { rMatPosDefSym(i, j) = rMatPosDefTemp(i, j); }

// Positive definite in real non-symmetric case    
LargeMatrix<Real> rMatPosDefNoSymTemp(dataPathTo(rMatrixDataSymPosDef), _dense, rowNum, colNum, _skyline, _sym);
LargeMatrix<Real> rMatPosDefNoSym(rMatSkylineSym);
  for (Number i = 1; i < rowNum + 1; i++)
    for (Number j = 1; j < colNum + 1; j++)
    { rMatPosDefNoSym(i, j) = rMatPosDefNoSymTemp(i, j); }
    
//  Complex Large Matrix
LargeMatrix<Complex> cMatSkylineSymSym(dataPathTo(cMatrixDataSym), _dense, rowNum, _skyline, _symmetric);

Real sigma = 10.0;
Complex csigma(-700, 20.0);
EigenSolverStdArpackpp eigenSolverStd;
EigenSolverGenArpackpp eigenSolverGen;
\end{lstlisting}

\subsubsection{Standard eigenvalue problem}

Real symmetric case
\vspace{.1cm}
\begin{lstlisting}
// Regular mode
if ( 0 != eigenSolverStd(rMatSkylineSymSym, rEigValue, rEigVector, nev, _regular, sigma) ) {
    out << "Eigenvalue regular mode (Skyline Sym) : " << rEigValue << std::endl;
    out << "Eigenvector regular mode (Skyline Sym) : " << rEigVector << std::endl;   
}
// Shift and invert mode
if ( 0 != eigenSolverStd(rMatSkylineSymSym, rEigValue, rEigVector, nev, _shiftInv, sigma) ) {
    out << "Eigenvalue shift and inverse mode :" << rEigValue << std::endl;
    out << "Eigenvalue shift and inverse mode: " << rEigVector << std::endl;
}
\end{lstlisting}
\vspace{.1cm}
Real non -symmetric case
\vspace{.1cm}
\begin{lstlisting}
// Regular mode
if ( 0 != eigenSolverStd(rMatSkylineSym, cEigValue, cEigVector, nev, _regular, sigma)) {
    out << "Eigenvalue regular mode (Skyline Sym) : " << cEigValue << std::endl;
    out << "Eigenvector regular mode (Skyline Sym) : " << cEigVector << std::endl;    
 }
//Shift and invert mode
if ( 0 != eigenSolverStd(rMatSkylineSym, cEigValue, cEigVector, nev, _shiftInv, sigma)) {
    out << "Eigenvalue shift and inverse mode (Skyline Sym) :" << cEigValue << std::endl;
    out << "Eigenvalue shift and inverse mode (Skyline Sym): " << cEigVector << std::endl;    
}
\end{lstlisting}
\vspace{.1cm}
Complex case
\vspace{.1cm}
\begin{lstlisting}
// Regular mode
if ( 0 != eigenSolverStd(cMatSkylineSymSym, cEigValue, cEigVector, nev, _regular, csigma)) {
    out << "Eigenvalue regular mode (Skyline Sym) : " << cEigValue << std::endl;
    out << "Eigenvector regular mode (Skyline Sym) : " << cEigVector << std::endl;
}
// Shift and invert mode
if ( 0 != eigenSolverStd(cMatSkylineSymSym, cEigValue, cEigVector, nev, _shiftInv, csigma)) {
    out << "Eigenvalue shift and inverse mode (Skyline Sym) : " << cEigValue << std::endl;
    out << "Eigenvector shift and inverse mode (Skyline Sym) : " << cEigVector << std::endl;
}
\end{lstlisting}
\vspace{.1cm}

\subsubsection{Generalized eigenvalue problem}

Real symmetric case
\vspace{.1cm}
\begin{lstlisting}
// Regular mode
if ( 0 != eigenSolverGen(rMatSkylineSymSym, rMatPosDefSym, rEigValue, rEigVector, nev, _regular, sigma) ) {
    out << "Eigenvalue regular mode (Skyline Sym):" << rEigValue << std::endl;
    out << "Eigenvalue regular mode (Skyline Sym): " << rEigVector << std::endl;
}
// Shift and invert mode
if ( 0 != eigenSolverGen(rMatSkylineSymSym, rMatPosDefSym, rEigValue, rEigVector, nev, _shiftInv, sigma) ) {
    out << "Eigenvalue shift and inverse mode (Skyline Sym) :" << rEigValue << std::endl;
    out << "Eigenvalue shift and inverse mode (Skyline Sym) : " << rEigVector << std::endl;
}
// Buckling mode
if ( 0 != eigenSolverGen(rMatPosDefSym, rMatSkylineSymSym, rEigValue, rEigVector, nev, _buckling, sigma) ) {
    out << "Eigenvalue buckling mode (Skyline Sym) (positive definite):" << rEigValue << std::endl;
    out << "Eigenvalue buckling mode (Skyline Sym) (positive definite): " << rEigVector << std::endl;
}
// Cayley mode  
if ( 0 != eigenSolverGen(rMatSkylineSymSym, rMatPosDefSym, rEigValue, rEigVector, nev, _cayley, sigma) ) {
    out << "Eigenvalue cayley mode (Skyline Sym):" << rEigValue << std::endl;
    out << "Eigenvalue cayley mode (Skyline Sym): " << rEigVector << std::endl;
}
\end{lstlisting}
\vspace{.1cm}
Real non -symmetric case
\vspace{.1cm}
\begin{lstlisting}
// Regular mode
if ( 0 != eigenSolverGen(rMatSkylineSym, rMatPosDefSym, cEigValue, cEigVector, nev, _regular, sigma) ) {
    out << "Eigenvalue regular mode  (Skyline Dual) : " << cEigValue << std::endl;
    out << "Eigenvector regular mode (Skyline Dual) : " << cEigVector << std::endl;
}
// Shift and invert mode
if ( 0 != eigenSolverGen(rMatSkylineSym, rMatPosDefNoSym, cEigValue, cEigVector, nev, _shiftInv, sigma) ) {
    out << "Eigenvalue shift and inverse mode :" << cEigValue << std::endl;
    out << "Eigenvalue shift and inverse mode: " << cEigVector << std::endl;
}  
// Complex shift with real part
if ( 0 != eigenSolverGen(rMatSkylineSym, rMatPosDefNoSym, cEigValue, cEigVector, nev, _cplxShiftInv, 'R', csigma) ) {
    out << "Eigenvalue complex shift and inverse mode (Real part) :" << cEigValue << std::endl;
    out << "Eigenvalue complex shift and inverse mode (Real part): " << cEigVector << std::endl;
}  
// Complex shift with imaginary part
if ( 0 != eigenSolverGen(rMatSkylineSym, rMatPosDefNoSym, cEigValue, cEigVector, nev, _cplxShiftInv, 'I', csigma) ) {
    out << "Eigenvalue complex shift and inverse mode (Imaginary part):" << cEigValue << std::endl;
    out << "Eigenvalue complex shift and inverse mode (Imaginary part): " << cEigVector << std::endl;
}
\end{lstlisting}
\vspace{.1cm}
Complex case
\vspace{.1cm}
\begin{lstlisting}
// Regular mode
if ( 0 != eigenSolverGen(cMatSkylineSymSym, cMatPosDef, cEigValue, cEigVector, nev, _regular, csigma)) {
    out << "Eigenvalue regular mode (Skyline Sym) : " << cEigValue << std::endl;
    out << "Eigenvector regular mode (Skyline Sym) : " << cEigVector << std::endl;
}
// Shift and invert mode
if ( 0 != eigenSolverGen(cMatSkylineSymSym, cMatPosDef, cEigValue, cEigVector, nev, _shiftInv, csigma, "LM")) {
    out << "Eigenvalue shift and inverse mode (Skyline Sym) : " << cEigValue << std::endl;
    out << "Eigenvector shift and inverse mode (Skyline Sym) : " << cEigVector << std::endl;
}
\end{lstlisting}
\vspace{.1cm}
Besides making use of class \class{EigenSolverStdArpackpp} and class \class{EigenSolverGenArpackpp}, we can call \arpackpp problem object directly. However, it's only recommended for advanced users!!
The following examples describe how to use \arpackpp object directly.
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]size}]
  //1. Create interface object
  LargeMatrixArpackppStdReg<Real, LargeMatrix<Real> > intMat(rMatSkylineSymSym, size);

  //2. Create Arpack++ problem object
  ARSymStdEig<Real, LargeMatrixArpackppStdReg<Real, LargeMatrix<Real> > >  arProb(size, nev, &intMat, &LargeMatrixArpackppStdReg<Real, LargeMatrix<Real> >::multOPx, (char *)"LM");

  //3. Compute eigenvalues
  Int nconv = arProb.FindEigenvalues();

  for (Int i = 0; i < nconv; ++i) {
      out << arProb.Eigenvalue(i) << "  ";
  }
  out << "\n";
  //4. Change some parameters
  arProb.ChangeNev(nev-2);
  arProb.ChangeTol(1.e-6);

  nconv = arProb.FindEigenvectors();

  for (Int i = 0; i < nconv; ++i) {
    out << arProb.Eigenvalue(i) << "  ";
  }
  out << "\n";

  //4. Retrieve eigenVector
  Real* eigenVec_p = 0;
  for (Int i = 0; i < nconv; ++i) {
      eigenVec_p = arProb.RawEigenvector(i);
      out << "Eigenvector [" << i << "] :" ;
      for (Int j = 0; j < size; j++) {
          out <<   *eigenVec_p << "  ";
          eigenVec_p++;
      }
      out << "\n";
  }

  out << "\n";
\end{lstlisting}
\vspace{.1cm}
