%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{DifferentialOperator} class}

The \class{DifferentialOperator} class is intended to describe various operators that can act on a scalar function or a vector function. They may be either differential operator
(involving derivatives) or 0 order operators (not involving derivative). The attributes of
this class are the following: 
\vspace{.2cm}
\begin{lstlisting}
class DifferentialOperator
{
 private:
   DiffOpType type_;         //type of differential operator 
   number_t order_;          //derivation order
   bool requiresExtension_;  //does operator involve non-tangential derivatives
   bool requiresNormal_;     //is normal vector required for this operator
   String name_;             //operator name
\end{lstlisting}
\vspace{.2cm}
The differential operators supported are list in the \textit{DiffOpType} enumeration:
\vspace{.2cm}
\begin{lstlisting}
enum DiffOpType
{
_id, _d0, _dt = _d0, _d1, _dx = _d1, _d2, _dy = _d2, _d3, _dz = _d3,
_grad, _nabla = _grad, _div, _curl, _rot = _curl,
_gradS, _nablaS = _gradS, _divS, _curlS, _rotS = _curlS,
_nx, _ndot, _ncross, _ncrossncross, _ndotgrad, _ndiv, _ncrosscurl,
_divG, _gradG, _nablaG = _gradG, _curlG, _rotG = _curlG, _epsilon,
_jump, _mean}
\end{lstlisting}
\vspace{.2cm}
with the following meaning (u is either a scalar or vector unknown, x, y and z are the Cartesian
coordinates and n is the normal):
\begin{center}
\begin{tabular}{|l|l|l|l|}
  \hline
  enum value & mathematical & built-in functions& unknown\\
  \hline
  \_id & identity & \verb?id(u)? or \verb?u? & scalar or vector\\
	\_d0=\_dt & \(\partial_t\) & \verb?d0(u)? or \verb?dt(u)?& scalar or vector\\
	\_d1=\_dx & \(\partial_x\) & \verb?d1(u)? or \verb?dx(u)?& scalar or vector\\
	\_d2=\_dy & \(\partial_y\) & \verb?d2(u)? or \verb?dy(u)?& scalar or vector\\
	\_d3=\_dz & \(\partial_z\) & \verb?d3(u)? or \verb?dz(u)?& scalar or vector\\
	\_grad=\_nabla & \(\nabla\) & \verb?grad(u)? or \verb?nabla(u)?& scalar or vector\\
	\_div & \(\mathrm{div}\) & \verb?div(u)?& vector\\
	\_curl=\_rot & \(\mathrm{curl}\) & \verb?curl(u) or \verb?rot(u)& vector\\
	\_gradS=\_nablaS & \(\nabla_\tau\) (surfacic) & \verb?gradS(u)? or \verb?nablaS(u)?& scalar
or vector\\
	\_divS & \(\mathrm{div}_\tau\) (surfacic) & \verb?divS(u)?& vector\\
	\_curlS=\_rotS & \(\mathrm{curl}_\tau\) (surfacic) & \verb?curlS(u)? or \verb?rotS(u)?& vector\\
		\_gradG=\_nablaG & \(\nabla_{abc}=(a\partial_x,b\partial_y,c\partial_z)\) & \verb?gradG(u,a,b,c)?
or \verb?nablaG(u,a,b,c)?& scalar or vector\\
	\_divG & \(\nabla_{abc}.\) & \verb?divG(u,a,b,c)?& vector\\
	\_curlG=\_rotG & \(\nabla_{abc}\times\) & \verb?curlG(u,a,b,c)? or \verb?rotS(u,a,b,c)?& vector\\
	\_epsilon & elastic tensor& \verb?epsilon(u)?& vector\\
	\_nx & \(n*\)& \verb?nx(u)? or \verb?_n*u?& scalar\\
	\_ndot & \(n.\)& \verb?ndot(u)? or \verb?_n.u?& vector\\
	\_ncross & \(n\times\)& \verb?ncross(u)? or \verb?_n^u?& vector\\
	\_ncrossncross & \(n\times n\times\)& \verb?ncrossncross(u)? or \verb?_n^_n^u?& vector\\
	\_ndotgrad & \(n.\nabla\)& \verb?ndotgrad(u)? or \verb?_n.grad(u)?& scalar\\
	\_ndiv & \(n\mathrm{div}\)& \verb?ndiv(u)? or \verb?_n*div(u)?& vector\\
	\_ncrosscurl & \(n\times\mathrm{curl}\)& \verb?ncrosscurl(u)? or \verb?_n^curl(u)?& vector\\
	\_jump & \([\ ]\) (jump across)& \verb?jump(u)?& scalar or vector\\
	\_mean & \(\{\ \}\) (mean across)& \verb?mean(u)?& scalar or vector\\
  \hline
\end{tabular}
\end{center}
There is only one definition of differential operators stored (using their pointers) in the
static variable:
\vspace{.1cm}
\begin{lstlisting}
static std::vector<DifferentialOperator*> theDifferentialOperators;
\end{lstlisting}
\vspace{.1cm}
There is an explicit constructor of DifferentialOperator objects, but they are also created
using the find function:
\vspace{.1cm}
\begin{lstlisting}
DifferentialOperator* findDifferentialOperator(DiffOpType); 
\end{lstlisting}
\vspace{.1cm}
which creates the differential operator of a given type if it does not exist and returns a
pointer to the differential operator if it exists or has just been created. \\
This class provides some useful accessors functions (only read):
\vspace{.2cm}
\begin{lstlisting}
String name() const {return name_;}                 
DiffOpType type() const {return type_;}             
number_t order() const {return order_;}             
bool normalRequired() const{return requiresNormal_;}
bool extensionRequired() const{ return requiresExtension_;}
\end{lstlisting}
\vspace{.2cm}
and printing facilities:
\vspace{.1cm}
\begin{lstlisting}
std::ostream& operator<<(std::ostream&, const DifferentialOperator&); 
void printListDiffOp(std::ostream&); 
\end{lstlisting}
\vspace{.2cm}
  
\displayInfos{library=operator, header=DifferentialOperator.hpp, implementation=DifferentialOperator.cpp,
test=test\_operator.cpp, header dep={config.h, utils.h}}
