%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{DoF management}\label{s.dof}

\subsection{The \classtitle{Dof} class}

Linked to the \class{Space} class, the \class{DoF} class stores characteristics of degree
of freedom. Degree of freedom is like a key index of components of element of space. Generally,
it  handles a numeric index and carries some additional information. For instance, in case
of Lagrange interpolation it carries the geometrical point associated to the basis function.
From \class{DoF} class inherit the classes \class{FeDoF} and \class{SpDoF}.
\vspace{.1cm}
\begin{lstlisting}
class Dof
{protected :
    number_t id_;             //id of DoF
    DofType DoFtype_;         //type of DoF
 public :
    Dof(DofType t=_otherDof, number_t i=0);   
    virtual~Dof(){};
    DofType DoFType()const;
    number_t id() const;
    number_t& id();
    virtual void print(std::ostream&) const;  
    friend std::ostream& operator<<(std::ostream&, const Dof &);
};
\end{lstlisting}
\vspace{.2cm}
The type of DoF are enumerated by
\vspace{.1cm}
\begin{lstlisting}
enum DofType {_feDof,_spDof,_otherDof};
\end{lstlisting}
\vspace{.2cm}

\subsection{\classtitle{FeDof} and \classtitle{SpDof} classes}

Inherited classes are very simple:
\vspace{.1cm}
\begin{lstlisting}
class FeDof: public Dof
{protected :
    FeSpace* space_p;        // pointer to FeSpace
    const RefDof*  refDof_p;  // pointer to a Reference D.o.F
    bool shared_;             // implies D.o.F is shared between elements
    Vector<Real> derVector_;  // direction vector(s) of a derivative D.o.F (specific interpolation)
    Vector<Real> projVector_; // direction vector(s) of a projection D.o.F (specific interpolation)
    std::vector<std::pair<Element*, Number> > inElements; // list of elements related to Dof and local number of DoF in elements
 public :
    FeDof();               
    FeDof(FeSpace& sp, number_t i=0)      
    virtual void print(std::ostream&) const;
};
\end{lstlisting}
\vspace{.2cm}
\begin{lstlisting}
class SpDof : public Dof
{protected :
    SpSpace * space_p;        //pointer to SpSpace
public :
    SpDof()                                 
    SpDof(SpSpace& sp, number_t i)          
    virtual void print(std::ostream&) const;
};
\end{lstlisting}
\vspace{.2cm}

\displayInfos{library=space, header=DoF.hpp, implementation=DoF.cpp, test=test\_Space.cpp,
header dep={config.h, utils.h}}

\subsection{The \classtitle{DofComponent} class}

This class defined an extended representation of DoF, useful to address components of a vector DoF:
\vspace{.1cm}
\begin{lstlisting}
class DofComponent
{
public :
    const Unknown* u_p;   //unknown  
    Number DoFnum;        //DoF number   
    Dimen numc;           //component   
		
    DofComponent(const Unknown* u=0, Number dn=0, Dimen nc=1);
    DofComponent dual() const;
    void print(std::ostream& os) const;
    friend std::ostream& operator<<(std::ostream&, const DofComponent&);
};
\end{lstlisting}
\vspace{.2cm}
The \verb?dual()? function returns the 'dual' DoF component. Besides, this class provides some comparison operators (partial ordering) :
\vspace{.1cm}
\begin{lstlisting}
bool operator < (const DofComponent&, const DofComponent&);
bool operator ==(const DofComponent&, const DofComponent&);
bool operator !=(const DofComponent&, const DofComponent&);
\end{lstlisting}
\vspace{.1cm}
and some functions manipulating list of component DoFs:
\vspace{.1cm}
\begin{lstlisting}
vector<DofComponent> createCDoFs(const Unknown* u, const vector<Number>& DoFs);
vector<Number> renumber(const vector<DofComponent>&, const vector<DofComponent>&); 
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=space, header=DoF.hpp, implementation=DoF.cpp, test=test\_Space.cpp,
header dep={config.h, utils.h}}
