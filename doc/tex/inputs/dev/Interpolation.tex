%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{Interpolation} class}

The purpose of the \class{Interpolation} class is to store data related to finite element
interpolation. It concerns only general description of an interpolation: type of the finite
element interpolation (Lagrange, Hermite, \ldots), a subtype among standard, Gauss-Lobatto (only
for Lagrange) and first or second family (only for Nedelec), the "order" of the interpolation
for Lagrange family and the space conformity (L2, H1, Hdiv, \ldots). For instance, a Lagrange standard
P1 with H1 conforming is the classic P1 Lagrange interpolation but a Lagrange standard P1 with
L2 conforming means a discontinuous interpolation (like Galerkin discontinuous approximation)
where the vertices of an element are not shared. For the moment, this class is only intended
to support the following choices:
\begin{center}
\begin{tabular}{|l|l|l|}
  \hline
  type & subtype & order \\
  \hline
  Lagrange & standard, Gauss-Lobatto & any order \\
 \hline
  Hermite & standard &  \\
  \hline
  Morley &  standard&  order 2\\
  \hline
  Argyris &  standard& order 5\\
  \hline
  Crouzet-Raviart & standard &  order 1\\
  \hline
  Raviart-Thomas & standard &  any order\\
   \hline
  Nedelec & first or second family &  any order\\
	\hline
  Nedelec face & first or second family &  any order\\
  	\hline
  Nedelec edge & first or second family &  any order\\
  
    	\hline
\end{tabular}
\end{center}

\begin{focusbox}
Nedelec face and Nedelec edge are specific to 3D. Nedelec face corresponds to the 2D Raviart-Thomas family and Nedelec edge corresponds to the 2D Nedelec family. 
\end{focusbox}

\begin{ideabox}
The \class{Interpolation} class is intended to support a lot of finite element family, but all are not available. See child classes of \class{RefElement} class to know which families are actually available. 
\end{ideabox}

This class is mainly used with the \class{GeomElement} class (definition of the geometric
transformation) and the \class{Space} class (definition of the finite element approximation
related to the discretized space).\\ 

The class \class{Interpolation} has the following attributes:
\vspace{.2cm}
\begin{lstlisting}
class Interpolation
{private:
   const FEType type_;       //interpolation type 
   const FESubType subtype_; //interpolation subtype 
   const number_t numtype_;  //additional number type (degree for Lagrange)
   SobolevType conformSpace_;//conforming space  
   bool isoparametric_;      //isoparametric flag
   String name_;             //name of the interpolation type
   String subname_;          //interpolation sub_name
\ldots
};
\end{lstlisting}
\vspace{.2cm}
\textbf{FEType}, \textbf{FESubType} and \textbf{SpaceType} are enumerations defined in the
\emph{config.hpp}  file as follows:
\vspace{.2cm}
\begin{lstlisting}
enum SobolevType{L2=0,H1,Hdiv,Hcurl,Hrot=Hcurl,H2,Hinf};
enum FEType {Lagrange,Hermite,CrouzeixRaviart,Nedelec,RaviartThomas,NedelecFace, NedelecEdge,BuffaChristiansen, BuffaChristiansen,_Morley,_Argyris};
enum FESubType  {standard=0,gaussLobatto,firstFamily,secondFamily};
\end{lstlisting}
\vspace{.2cm}

In addition, there is a static attribute to manage a unique list of instantiated \class{Interpolation}
objects.
\vspace{.2cm}
\begin{lstlisting}
static std::vector<Interpolation*> theInterpolations; 
\end{lstlisting}
\vspace{.2cm}
This class proposes some access member functions and  functions returning some interpolation
properties:
\vspace{.2cm}
\begin{lstlisting}
FEType type() const;              
FESubType subtype() const;  
number_t numtype() const;      
SobolevType conformSpace() const; 
String name() const;            
String subName() const;   
String conformSpaceName();                       
bool isIsoparametric() const;
void isIsoparametric(bool is) ;
bool isContinuous();     
bool areDofsAllScalar();  
number_t maximumDegree(); 
\end{lstlisting}
\vspace{.2cm}
some error member functions:
\vspace{.2cm}
\begin{lstlisting}
void badType() const;
void badSubType() const;
void badSpace() const;
void badType(const number_t) const;
void badSubType(const number_t) const;
void badDegree(const number_t) const;
\end{lstlisting}
\vspace{.2cm}
There is only one explicit constructor (no copy constructor):
\vspace{.2cm}
\begin{lstlisting}
Interpolation(FEType, FESubType, number_t, SobolevType);
\end{lstlisting}
\vspace{.2cm}
that initializes the attributes and add the created object in the static list \textbf{theInterpolations}.
\\

The process creating an \class{Interpolation} object is governed by other classes (in particular
the \class{Space} class) using the function:
\vspace{.2cm}
\begin{lstlisting}
Interpolation* findInterpolation(FEType, FESubType, number_t, SpaceType);
Interpolation& interpolation(FEType, FESubType, number_t, SobolevType);  
\end{lstlisting}
\vspace{.2cm}
These functions try to find an existing \class{Interpolation} object corresponding to the given
parameters, if it does not exist one, a new object is created by calling the constructor.\\

It is possible to work with a collection of \class{Interpolation} objects that are handles as a vector by the \class{Interpolations} class (see the definition of \class{PCollection}). It may be used as following:
\vspace{.1cm}
\begin{lstlisting}
Interpolations ints(3);
for(Number i=1;i<=3;i++) ints(i)=interpolation(Lagrange,_standard,i,H1);
\end{lstlisting}
\displayInfos{library=finiteElements, header=Interpolation.hpp, implementation=Interpolation.cpp,
test=test\_Interpolation.cpp, header dep={config.h, utils.h}}

