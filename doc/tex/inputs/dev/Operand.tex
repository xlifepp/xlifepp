%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{Operand} class}

The \class{Operand} class stores a \class{AlgebraicOperator}, a user data (either \class{Function}
or \class{Value}) and conjugate, transpose flags. It is mainly used by the \class{OperatorOnUnknown}
class as left or right-hand side of a generalized differential operator (see the next section).
The only allowed algebraic operations defined in the \class{AlgebraicOperator} enumeration, are:\\
\hspace*{5mm}\(\bullet\) the standard product, operator \verb?*? \\
\hspace*{5mm}\(\bullet\) the inner product (hermitian product), operator \verb?|? \\
\hspace*{5mm}\(\bullet\) the cross product, operator \verb?^? \\
\hspace*{5mm}\(\bullet\) the contracted product, operator \verb?%? \\

\vspace{.2cm}
\begin{lstlisting}
enum AlgebraicOperator {_product,_innerProduct,_crossProduct,_contractedProduct};
class Operand
{
protected :
   const Value* val_p;           // pointer to operand object (a Value)
   const Function * fun_p;       // pointer to Function object
   bool isFunction_;             // function flag
   bool conjugate_;              // true if the operand has to be conjugated
   bool transpose_;              // true if the operand has to be transposed
   AlgebraicOperator operation_; // operand operator
\end{lstlisting}
\vspace{.2cm}
This class provides few public constructors:
\vspace{.1cm}
\begin{lstlisting}
Operand(const Function&,AlgebraicOperator); // from a Function
Operand(const Value&,AlgebraicOperator);    // from a Value  (Value is copied)
Operand(const Value*,AlgebraicOperator);    // from a Value* (Value is not copied)
template <typename T> 
Operand(const T &,AlgebraicOperator);       // from a scalar, a Vector or a Matrix
\end{lstlisting}
\vspace{.1cm}
The templated constructor avoids the user to give explicitly a Value object, "ordinary" objects
such scalar, vector or matrix   
may be passed to the constructor.\\

The other member functions are some accessors functions:
\vspace{.1cm}
\begin{lstlisting}
bool isFunction() const;
bool conjugate() const;
bool& conjugate();
bool transpose() const;
bool& transpose();
AlgebraicOperator operation() const;
const Value& value() const;              
const Function& function() const;        
template<class T> T& valueT() const;    
\end{lstlisting}
\vspace{.2cm}
There are two printing facilities:
\vspace{.1cm}
\begin{lstlisting}
void Operand::print(std::ostream&) const;   
std::ostream& operator<<(std::ostream&,const Operand &);
\end{lstlisting}

\begin{focusbox}
The \class{Operand} class is an internal class linked to the \class{OperatorOnUnknown} class.
Normally, an end user does not use this class.
\end{focusbox}

\displayInfos{library=operator, header=Operand.hpp, implementation=Operand.cpp, test=test\_operator.cpp,
header dep={config.h, utils.h}}
