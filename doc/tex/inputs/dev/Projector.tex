%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{Projector} class}
The  \class{Projector} class is intended to handle projection from one FE space, say \(V\) to another FE space, say \(W\). The projection \(w\in W\) of a \(v \in V\) is based on a bilinear form, say \(a(.,.)\), defined on both spaces :
\[a(w,\tilde{w}) =a(v,\tilde{w})\, \ \forall \tilde{w}\in W.\]
Let \((w_i)_{i=1,n}\) a basis of \(W\) and \((v_i)_{i=1,m}\) a basis of \(V\), the above problem is equivalent to the matrix problem:
\[
\mathbb{A}\,W=\mathbb{B}\,V\]
where \(\mathbb{A}_{ij}=a(w_j,w_i)\), \(\mathbb{B}_{ij}=a(v_j,w_i)\), \(v=\sum_{i=1,m}V_i\,v_i\) and \(w=\sum_{i=1,n}W_i\,w_i\).
The bilinear form should be symmetric and positive on the space \(W\) in order to the matrix  \(\mathbb{A}\) be invertible. The most simple example is the \(L^2\) projection related to the bilinear form:
\[a(w,\tilde{w})=\int_{\Omega }w\,\tilde{w}\,d\Omega.\]

As a consequence, the  \class{Projector} class manages the following data:
\vspace{.1cm}
\begin{lstlisting}
class Projector
{
public:
ProjectorType projectorType;  //!< type of projection
string_t name;                //!< name of projection
protected:
Space* V_, *W_;               //!< spaces involved
Unknown* u_V, *u_W;           //!< internal unknowns
const GeomDomain* domain_;    //!< domain  involved in bilinear forms
BilinearForm* a_V, *a_W;      //!< bilinear forms used by projector
mutable TermMatrix* A_, *B_;  //!< matrix a(W,W) and a(W,V)
mutable TermMatrix* invA_B;   //!< matrix inv(A)*B if allocated
}
\end{lstlisting}
\vspace{.1cm}
where the projector type is one of the following:
\vspace{.1cm}
\begin{lstlisting}
enum ProjectorType {_noProjectorType=0, _userProjector, _L2Projector, _H1Projector, _H10Projector};
\end{lstlisting}
\vspace{.1cm}
Because, bilinear forms in \xlifepp are linked to unknowns/spaces, the class manages two bilinear forms and their matrix representation. The unknowns used by the \class{Projector} are not some user unknowns but are specific to the class. Finally, it can  allocate a \class{TermMatrix} object representing the matrix \(\mathbb{A}^{-1}\,\mathbb{B}\).\\

Besides, in order to save time and memory, all the projectors that have been handled (explicitly or implicitly) are listed in a static vector:
\vspace{.1cm}
\begin{lstlisting}
static std::vector<Projector*> theProjectors;
\end{lstlisting}
\vspace{.1cm}
Projector are constructed from spaces, a projector type or a bilinear form and an optional name. When dealing with the restriction of a space to a domain, the domain has to be given. When dealing with projection of vector unknown, the sizes have to be specified.
\vspace{.1cm}
\begin{lstlisting}
Projector(Space& V, Space& W, ProjectorType pt=_L2Projector,
          const string_t& na="");                                      
Projector(Space& V, Space& W, const GeomDomain&, ProjectorType pt=_L2Projector, 
          const string_t& na="");     
Projector(Space& V, Space& W, BilinearForm& a, const string_t& na="");
Projector(Space& V, dimen_t nbcV, Space& W, dimen_t nbcW,
          ProjectorType pt=_L2Projector, const string_t& na="");       
Projector(Space& V, dimen_t nbcV, Space& W, dimen_t nbcW, const GeomDomain&,
          ProjectorType pt=_L2Projector, const string_t& na="");       
Projector(Space& V, dimen_t nbcV, Space& W, dimen_t nbcW, BilinearForm& a,
          const string_t& na="");                                     
~Projector();
void init(dimen_t nbc_V, dimen_t nbc_W);              
\end{lstlisting}
\vspace{.1cm}
The \var{init} function does really the job of construction. In particular, it computes the matrix \var{A\_} and \var{B\_} and do a factorization of \var{A\_}.\\

Note that the \class{TermMatrix} is not allocated at the construction. To allocate it, one has to call the member function: 
\vspace{.1cm}
\begin{lstlisting}
TermMatrix& asTermMatrix(const Unknown&, const Unknown&, 
                         const string_t& = "invA_B");
\end{lstlisting}
\vspace{.1cm}
that returns a reference to the \class{TermMatrix} \var{invA\_B} and deletes the \class{TermMatrix} \var{A\_} and \var{B\_}.\\

Once constructed, projectors can process \class{TermVector}'s by using one of the three operator functions:
\vspace{.1cm}
\begin{lstlisting}
TermVector operator()(TermVector& V,const Unknown& u) const; 
TermVector operator()(const TermVector& V) const;          
TermVector& operator()(TermVector& V, TermVector& PV) const;  
\end{lstlisting}
\vspace{.1cm}
Because an unknown is always linked to any \class{TermVector}, the user can pass the unknown either in an explicit way or in an implicit way when the \class{TermVector} result is passed to the operator.  When no unknown is passed to, the unknown of the projection will be the first unknown linked to the space \(W\). If the \class{TermMatrix} \var{invA\_B} is allocated, the projection does the product \var{invA\_B * V}, if not the projection does the product \var{B\_ * V}  and solve the linear system  \var{A\_ * X = B\_ * V} using the \var{factSolve} functions.  \\

Projection can also be processed by using the operator *:
\vspace{.1cm}
\begin{lstlisting}
friend TermVector operator*(const Projector& P, const TermVector& V);
\end{lstlisting}
\vspace{.1cm}
that only calls \var{P(V)}.\\

Users can compute the  projection of a \class{TermVector} without instantiating a \class{Projector} object:
\vspace{.1cm}
\begin{lstlisting}
TermVector projection(const TermVector& V, Space& W, ProjectorType pt=_L2Projector);
\end{lstlisting}
\vspace{.1cm}
In that case, a \class{Projector} object is created in the back.\\

Two functions deal with the list of all projectors:
\vspace{.1cm}
\begin{lstlisting}
friend Projector& findProjector(Space& V, Space& W, 
                                ProjectorType pt=_L2Projector); 
static void clearGlobalVector();  
\end{lstlisting}
\vspace{.1cm}
The \cmd{findProjector} creates a new projector if projector has not been found.\\

Finally, there are some print stuff:  
\vspace{.1cm}
\begin{lstlisting}
void print(ostream&) const;
friend ostream& operator<<(ostream&, const Projector&);
\end{lstlisting}
\vspace{.1cm}

\displayInfos{
	library=term, 
	header=Projector.hpp, 
	implementation=Projector.cpp,
	test=unit\_Projector.cpp,
	header dep={Term.hpp, config.h, utils.h}
}



