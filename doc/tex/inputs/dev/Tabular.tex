%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{Tabular} class}

The \class{Tabular} class allows managing tabulated values of a function. More precisely, it handles the values of a function on a uniform grid of any dimension, mainly providing some tools to create the table, to save the table to a file, to load a table from a file, and to evaluate the values of the function at some points by interpolation.\\
More precisely,  \class{Tabular} is a template class inherited from the \verb?std::vector<T>? class:
\vspace{.1cm}
\begin{lstlisting}
template <typename T>
class Tabular : public std::vector<T>
{
public:
number_t dim;                 // grid dimension
std::vector<real_t> start;    // starting coordinates
std::vector<real_t> step;     // coordinate steps
std::vector<number_t> nbstep; // number of  steps
std::vector<string_t> name;   // coordinate names
std::vector<number_t> bs;     // block sizes (interna)
\ldots
\end{lstlisting}
\vspace{.2cm}
It handles the standard C++ functions with the following prototypes
\vspace{.1cm}
\begin{lstlisting}
typedef T(*funT1) (real_t);
typedef T(*funTP1)(real_t, Parameters&);
typedef T(*funT2) (real_t, real_t);
typedef T(*funTP2)(real_t, real_t, Parameters&);
typedef T(*funT3) (real_t, real_t, real_t);
typedef T(*funTP3)(real_t, real_t,real_t, Parameters&);
\end{lstlisting}
\vspace{.2cm}
There are few ways to construct a \class{Tabular} object, either passing a function or not:
\vspace{.1cm}
\begin{lstlisting}
Tabular(real_t x0,real_t dx,number_t nx,const string_t& nax="x1");
Tabular(real_t x0,real_t dx,number_t nx,real_t y0,real_t dy,number_t ny,
        const string_t& nax="x1",const string_t& nay="x2");
Tabular(real_t x0,real_t dx,number_t nx,real_t y0,real_t dy,number_t ny,
        real_t z0, real_t dz, number_t nz, const string_t& nax="x1",
        const string_t& nay="x2", const string_t& naz="x3"); 
void addCoordinate(real_t c,real_t dc,number_t nc,const string_t& nac="");
Tabular(real_t x0, real_t dx,number_t nx,funT1 f,const string_t& nax="x1");
Tabular(real_t x0,real_t dx,number_t nx,funTP1 f,Parameters& pars, 
        const string_t& nax="x1");
Tabular(real_t x0,real_t dx,number_t nx,real_t y0,real_t dy,number_t ny,funT1 f,
        const string_t& nax="x1", const string_t& nay="x2")   ;
Tabular(real_t x0,real_t dx,number_t nx,real_t y0,real_t dy,number_t ny,funT1 f,
        Parameters& pars, const string_t& nax="x1", const string_t& nay="x2");
Tabular(real_t x0,real_t dx,number_t nx,real_t y0,real_t dy,number_t ny,
        real_t z0, real_t dz, number_t nz, funT3 f, const string_t& nax="x1",
        const string_t& nay="x2", const string_t& naz="x3");
Tabular(real_t x0,real_t dx,number_t nx,real_t y0,real_t dy,number_t ny,
        real_t z0, real_t dz, number_t nz, funT3 f, Parameters& pars,
        const string_t& nax="x1", const string_t& nay="x2", const string_t& naz="x3"); 
Tabular(const string_t& filename);
\end{lstlisting}
\vspace{.2cm}
The first functions where no function is passed, construct only a 1D, 2D, 3D  grid, the function values will be set using the member functions \cmd{createTable}:
\vspace{.1cm}
\begin{lstlisting}
void createTable(funT1 f);
void createTable(funTP1 f, Parameters& pars);
void createTable(funT2 f);
void createTable(funTP2 f, Parameters& pars);
void createTable(funT3 f);
void createTable(funTP3 f, Parameters& pars);
\end{lstlisting}
\vspace{.2cm}
The function \cmd{addCoordinate} allows adding more directions in the uniform grid, and then to build grids of any dimension. The function involved in some constructors or in functions \cmd{createTable} must be consistent with the grid (same number of arguments than the grid dimension).\\

The values of the function are stored in a contiguous vector:
\begin{verbatim} 
   [ v0.00 v0.01 \ldots v0.0n v0.10 v0.11 \ldots v0.1n \ldots]
\end{verbatim}

So, the access to the \(i_1,i_2,\ldots,i_d\)  values is given by \(k= i_1(s_2\,s_3\, \ldots\, s_d)+i_2(s_3\,  \ldots\, s_d)+ \ldots i_d\) where \(s_n\) is the size of the \(n^{th}\) grid coordinate.\\

Saving the table of values to a file is done by the function:
\vspace{.1cm}
\begin{lstlisting}
void saveToFile(const string_t& filename,  const string_t& comment="Tabular") const;
\end{lstlisting}
\vspace{.1cm}
using the following ASCII format:
\begin{verbatim}
  dim
  comment
  name1 x1 dx1 nx1 name2 x2 dx2 nx2 \ldots named xd dxd nxd
  \ldots
  x1_i1 x2_i2 \ldots xd_id v_i1i2\ldotsid
  \ldots	
\end{verbatim}
and load from a file by using
\vspace{.1cm}
\begin{lstlisting}
void loadFromFile(const string_t& filename);
\end{lstlisting}
\vspace{.1cm}
or straightforward from the constructor.\\

The main operation consists in evaluating the function at any point using the tabulated values. It is done by using \(Q^k\) interpolation (linear in 1D) and the \cmd{operator}:
\vspace{.1cm}
\begin{lstlisting}
T& operator()(real_t x) const;
T& operator()(real_t x, real_t y) const;
T& operator()(real_t x, real_t y, real_t z) const;
T& operator()const std::vector<real_t>& z) const;
T valrec(number_t d, number_t k, const std::vector<number_t>& is,
         const std::vector<number_t>& bs,const std::vector<real_t>& as) const;
\end{lstlisting}
\vspace{.1cm}
The \cmd{valrec} function does a recursive evaluation and it is called by the \cmd{operator} when the grid dimension is greater than 2.\\

Finally, the  \class{Tabular} class provides some internal tools and some print stuff:
\vspace{.1cm}
\begin{lstlisting}
void clear();  //reset the object
void globalToIndex(number_t k, number_t bs, number_t d,
     std::vector<number_t>::iterator ijk) const;
void print(std::ostream&)  const;
std::ostream& operator<<(std::ostream&,const Tabular<T>&);
\end{lstlisting}

\displayInfos{library=utils, header=Tabular.hpp, implementation=Tabular.hpp, test=test\_Tabular.cpp, header dep={config.h, utils.h}}