%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex

\section{Approximate matrix}

In the context of hierarchical matrix, some sub-matrices may be "compressed" to save memory and time computation. \xlifepp provides a general class \class{ApproximateMatrix} to deal with real or complex approximate matrix that can be 
\begin{itemize}
	\item low rank matrix represented as the product \(UDV^*\) (\class{LowRankMatrix})
	\item sinus cardinal decomposition (\class{SCDMatrix}), not yet available
	\item fast multipole representation  (\class{FMMMatrix}), not yet available
\end{itemize}

\subsection{The \classtitle{ ApproximateMatrix} class}

The \class{ApproximateMatrix} class is a template abstract class (templated by the type of coefficients) that manages only the type of approximation, the name of the approximate matrix and defines all virtual functions that children has to implement: 
\vspace{.1cm}
\begin{lstlisting}	
template <typename T>
class ApproximateMatrix
{public :
MatrixApproximationType approximationType;     //type of approximation
string_t name;                                 //optional name useful for doc
virtual ~ApproximateMatrix() {};               //virtual destructor
virtual ApproximateMatrix<T>* clone() const=0; //creation of a clone	
virtual number_t numberOfRows() const =0;      //nb of rows
virtual number_t numberOfCols() const =0;      //nb of columns
virtual number_t nbNonZero() const =0;         //nb of nonzeros coefficient
virtual number_t rank() const =0;              //rank of matrix
virtual vector<T>& multMatrixVector(const vector<T>&, vector<T>&) const = 0; 
virtual vector<T>& multVectorMatrix(const vector<T>&, vector<T>&) const = 0;
virtual real_t norm2() const =0;                  //Frobenius norm
virtual real_t norminfty() const=0;               //infinite norm
virtual LargeMatrix<T> toLargeMatrix() const = 0; //convert to LargeMatrix 
virtual void print(std::ostream&) const =0;       //print to stream
};
\end{lstlisting}
\vspace{.1cm}
Up to now, the only children available is the templated \class{LowRankMatrix<T>} class.

\subsection{The \classtitle{ LowRankMatrix} class}

A low rank matrix is a matrix of the form 
\[U\,D\,V^*\]
where \(U\) is an \(m\times r\) matrix, \(V\) is an \(n\times r\) matrix and \(D\) is an \(r\times r\) diagonal matrix. The rank of a such matrix is at most \(r\). So it is a low rank representation of an \(m\times n\) matrix if \(r\) is small compared to \(m\), \(n\). Contrary to the class name suggests, some matrix may be not low rank representation. Think about the SVD (singular value decomposition) of an \(m\times n\) matrix that can be handled by this class (\(r=\min(m,n)\)).\\

The \class{LowRankMatrix} class is derived from the \class{ApproximateMatrix} and handles the \(U,V\) matrices as \class{Matrix} objects of \xlifepp (dense matrix with major row access) and the diagonal matrix \(D\) as a \class{Vector} object of \xlifepp :
\vspace{.1cm}
\begin{lstlisting}
template <typename T>
class LowRankMatrix : public ApproximateMatrix<T>
{public :
 Matrix<T> U_, V_;
 Vector<T> D_;
 \ldots
}
\end{lstlisting}
\vspace{.1cm}

Different constructors are available: 	
\vspace{.1cm}
\begin{lstlisting}
LowRankMatrix();    //default constructor
//constructor from explicit matrices and vector
LowRankMatrix(const Matrix<T>&, const Matrix<T>&, const Vector<T>&, 
              const string_t& na="");
LowRankMatrix(const Matrix<T>&, const Matrix<T>&, const string_t& na="");    
//constructor from dimensions              
LowRankMatrix(number_t m, number_t n, number_t r, const string_t& na="");                 
LowRankMatrix(number_t m, number_t n, number_t r, bool noDiag,
              const string_t& na="");     
//constructor from matrix iterators and vector iterator
template <typename ITM, typename ITV> 
LowRankMatrix(number_t m, number_t n, number_t r,
              ITM itmu, ITM itmv, ITV itd, const string_t& na="");   
template <typename ITM> 
LowRankMatrix(number_t m, number_t n, number_t r,
              ITM itmu, ITM itmv, const string_t& na="");            
\end{lstlisting}
\vspace{.1cm}
The \class{LowRankMatrix} class provides all virtual functions declared by the \class{ApproximateMatrix} class:  	
\vspace{.1cm}
\begin{lstlisting}
ApproximateMatrix<T>* clone() const;   
number_t numberOfRows() const;
number_t numberOfCols() const;
number_t nbNonZero() const;
number_t rank() const;
bool hasDiag() const;
vector<T>& multMatrixVector(const vector<T>&, vector<T>&) const; 
vector<T>& multVectorMatrix(const vector<T>&, vector<T>&) const;
LargeMatrix<T> toLargeMatrix() const; 
real_t norm2() const;                 
real_t norminfty() const;                
void print(std::ostream&) const;    
\end{lstlisting}
\vspace{.1cm}
In addition, the class offers specific algebraic operations as member functions:
\vspace{.1cm}
\begin{lstlisting}        
LowRankMatrix<T> svd(real_t eps) const;       
LowRankMatrix<T>& operator+=(const LowRankMatrix<T>&);
LowRankMatrix<T>& operator-=(const LowRankMatrix<T>&);
LowRankMatrix<T>& operator*=(const T&);               
LowRankMatrix<T>& operator/=(const T&);               
LowRankMatrix<T>& add(const LowRankMatrix<T>&, const T&, real_t eps=0);
\end{lstlisting}
\vspace{.1cm}
and as external functions to the class (template declaration is omitted):
\vspace{.1cm}
\begin{lstlisting}
LowRankMatrix<T> combine(const LowRankMatrix<T>&, const T&,
                         const LowRankMatrix<T>&, const T&, real_t eps=0);
LowRankMatrix<T> operator+(const LowRankMatrix<T>&, const LowRankMatrix<T>&);
LowRankMatrix<T> operator-(const LowRankMatrix<T>&, const LowRankMatrix<T>&);
LowRankMatrix<T> operator-(const LowRankMatrix<T>&);
LowRankMatrix<T> operator+(const LowRankMatrix<T>&);
LowRankMatrix<T> operator*(const LowRankMatrix<T>&, const T&);
LowRankMatrix<T> operator/(const LowRankMatrix<T>&, const T&);
LowRankMatrix<T> operator*(const T&, const LowRankMatrix<T>&);
\end{lstlisting}
\vspace{.1cm}
Note that the combination of a low rank matrix \(L_1=U_1D_1V_1\) of rank \(r_1\) and a low rank matrix \(L_2=U_2D_2V_2\) of rank \(r_2\) produces a low rank matrix \(L=U\,D\,V\) of rank \(r=r_1+r_2\) because
\[
L=a_1L_1+a_2L_2=\left[\begin{array}{cc}U_1 & U_2\end{array}\right]
\left[\begin{array}{cc}
a_1D_1 & 0 \\
0 & a_2D_2
\end{array}\right]
\left[\begin{array}{c}V_1\\ V_2\end{array}\right].
\]
When nonzero, the {\ttfamily eps} argument in the {\ttfamily LowRankMatrix::add} and {\ttfamily combine} functions is used to re-compress the combined matrix with a truncated svd.\\

Finally, some compression methods based on SVD are implemented too. These compression methods "produce" a \class{LowRankMatrix} from a dense matrix stored in major column access and passed by a pointer to its first coefficient (template declaration is omitted):
\vspace{.1cm}
\begin{lstlisting}
void svdCompression(T* mat, number_t m, number_t n, number_t r, 
                    LowRankMatrix<T>& lrm);
void svdCompression(T* mat, number_t m, number_t n, real_t eps, 
                    LowRankMatrix<T>& lrm);
void rsvdCompression(T* mat, number_t m, number_t n, number_t r, 
                     LowRankMatrix<T>& lrm);
void rsvdCompression(T* mat, number_t m, number_t n, real_t eps, 
                     LowRankMatrix<T>& lrm);
void r3svdCompression(T* mat, number_t m, number_t n, real_t eps, 
                      LowRankMatrix<T>& lrm,
                      number_t t = 0, number_t p = 0, number_t q = 0, 
                      number_t maxit = 0)
\end{lstlisting}
\vspace{.1cm}
The SVD compression functions do the full svd of the given matrix and then truncate the number of singular values and singular vectors keeping either the \(r\) largest singular values/vectors or the singular values greater than a given {\ttfamily eps}. The fact that this process leads to a good approximate matrix results from the Eckart–Young–Mirsky theorem.\\

As the full svd is an expansive algorithm time computation, some alternative methods based on the random svd are also available. Random svd consists in capturing the matrix range using only few gaussian random vectors and doing a svd on a smaller matrix. Again, two versions of random svd are available, one providing a low rank matrix of a given rank, the other one providing a low rank matrix with a control on the approximate error. This last method iterates on the rank, so it is more expansive. The r3svd  is a more sophisticated  iterative method (see https://arxiv.org/ftp/arxiv/papers/1605/1605.08134.pdf).

\begin{focusbox}
As svd and qr algorithms have not been implemented for the \class{Matrix} class, \xlifepp uses the Eigen library that is provided when installing \xlifepp.
\end{focusbox}

\begin{warningbox}
svd, rsvd and aca compression does not work for matrix of matrices!
\end{warningbox}
	
\displayInfos{library=hierarchicalMatrix, header=ApproximateMatrix.hpp, implementation=ApproximateMatrix.cpp, test={test\_HMatrix.cpp}, header dep={largeMatrix.h, config.h, utils.h}}
