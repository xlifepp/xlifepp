%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{Point} class}

The purpose of the \class{Point} class is to deal with point in any dimension. It is derived
from the \emph{std:vector<Real>}. 
\vspace{.2cm}
\begin{lstlisting}
typedef std::vector<Real>::iterator ivr_t;
typedef std::vector<Real>::const_iterator civr_t;

class Point : public std::vector<Real>
{public:
 static Real tolerance; 
 \ldots
}
\end{lstlisting}
\vspace{.2cm}
The static member \emph{tolerance} is used in comparison operations. By default, its value
is 0; As a child class of \class{std::vector}, the Point class does not have copy constructor
and assignment = operator.

It offers some basic constructors:
\vspace{.2cm}
\begin{lstlisting}
Point(const int, const Real v = 0.); // dimension and constant value
Point(const Dimen, const Real v = 0.); // dimension and constant value
Point(const Real x1); // 1D constructor by coordinate
Point(const Real x1, const Real x2); // 2D constructor by coordinates
Point(const Real x1, const Real x2, const Real x3); // 3D constructor by coordinates
Point(const Dimen d, Real* pt); // constructor by dimension and Real array
Point(const std::vector<Real>& pt); // constructor by stl vector
Point(const std::vector<Real>::const_iterator, Dimen); // constructor by stl vector iterator
~Point(); // destructor
\end{lstlisting}
\vspace{.2cm}
One can access (both read and read/write access) to the coordinates either by index (from 1
to dim) or by explicit name x(), y() and z() (restricted to 3D point):
\vspace{.2cm}
\begin{lstlisting}
Real operator()(const Dimen d) const; //the d-th coordinates (r)
Real& operator()(const Dimen d);      //d-th coordinates (r/w)
Real  x() const; //the first coordinate  (r)
Real& x();       //the first coordinate  (r/w)
Real  y() const; //the second coordinate (r)
Real& y();       //the second coordinate (r/w)
Real  z() const; //the third coordinate  (r)
Real& z();       //the third coordinate  (r/w)
std::vector<Real> toVect(const Point &)  //!<convert to a std::vector
\end{lstlisting}
\vspace{.2cm}
Through overloaded operator, the Point class provides some algebraic operations and distance
computation functions:
\vspace{.2cm}
\begin{lstlisting}
Point& operator+=(const Point &); //add a point to the current point
Point& operator-=(const Point &); //subtract a point to the current point
Point& operator+=(const Real);  //add a constant point to the current point
Point& operator-=(const Real);  //subtract a constant point to the current point
Point& operator*=(const Real ); //scale the current point
Point& operator/=(const Real ); //scale the current point
Real squareDistance(const Point&) const; //square distance to another point
Real distance(const Point&) const; //distance to another point
\end{lstlisting}
\vspace{.2cm}
Note that size compatibility tests are performed on such computations.\\

Algebraic operations on points may be also be processed by external function to the class:
\vspace{.2cm}
\begin{lstlisting}
Point operator+(const Point &);                 //same point (completeness)
Point operator-(const Point &);                 //opposite point
Point operator+(const Point &,const Point &);   //sum of two points
Point operator-(const Point &,const Point &);   //difference of two points
Point operator+(const Point &,const Real );     //sum of two points
Point operator-(const Point &,const Real );     //difference of two points
Point operator+(const Real,const Point & );     //sum of two points
Point operator-(const Real,const Point & );     //difference of two points
Point operator*(const Real ,const Point &);     //scale a point
Point operator*(const Point &,const Real );     //scale a point
Point operator/(const Point &,const Real );     //scale a point
Real pointDistance(const Point&,const Point&);  //distance between two points
Real squareDistance(const Point&,const Point&); //square distance between two points
\end{lstlisting}
\vspace{.2cm}
\cmd{distance} is a function of the \emph{std::iterator} class, it is the reason why the
distance between two points is named \cmd{pointDistance}. \\

Finally, to order (partially) a couple of points, the comparison operators (==, !=, <, >, <=
and >=) are overloaded:
\vspace{.2cm}
\begin{lstlisting}
bool operator==(const Point &,const Point &); //equality of two points
bool operator!=(const Point &,const Point &); //not equality of two points
bool operator< (const Point &,const Point &); //leather than 
bool operator<=(const Point &,const Point &); //leather than or equal 
bool operator> (const Point &,const Point &); //greater than
bool operator>=(const Point &,const Point &); //greater between or equal 
\end{lstlisting}
\vspace{.2cm}
The comparison between points are performed using the tolerance value \(\tau\) (\var{Point::tolerance})
which means for points \(p\) and \(q\) in \(\mathbb{R}^n\):
\[
\begin{array}{l}
p==q\ {\rm if}\  |p-q|\leq \tau \\
p<q\ {\rm if}\ \exists i\leq n,\ \forall j<i,\  |p_j-q_j|\leq \tau\  {\rm and}\ p_j<q_j-\tau.

\end{array}
\]

\displayInfos{library=utils, header=Point.hpp, implementation=Point.cpp, test=test\_Point.cpp,
header dep=config.h}
