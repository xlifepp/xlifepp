%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The polynomial classes}

In order to deal with general polynomial spaces (using formal polynomials), \xlifepp provides the following template classes
\begin{itemize}
\item \class{MonomialT} to deal with monomials of the form \(x^iy^jz^k\)
\item \class{PolynomialT} to deal with polynomials as combination of monomials, say

\[
p(x,y,z)=\sum_{i,j,k\ge 0} \alpha_{ijk}x^iy^jz^k
\]

\item \class{PolynomialBasisT} to deal with set of polynomials, may be a polynomial basis:
	
\[
P[x,y,z]=\text{span}\left\{ P_\ell,\ \ell=1,L \right\}
\]

\item \class{PolynomialsBasisT} to deal with set of vector polynomials, may be a polynomial basis:

\[
\mathbf{P}[x,y,z]=\text{span}\left\{ \mathbf{P}_\ell=
\left(
\begin{array}{c}
P^x_\ell \\P^y_\ell\\P^z_\ell
\end{array}\right),\ \ell=1,L \right\}
\]

\item \class{PolyNodeT} to represent a polynomial as tree, useful to evaluate polynomial in a better way (Horner algorithm)
\end{itemize}

These classes can manage one, two or three variables and are templated by the type of the variable (K) which is of real\_t type by default.\\

Note that there is no specific class to deal with vector polynomial, they are described as \cmd{std::vector<PolynomialT>}.\\

For users, some useful aliases are available:
\vspace{.1cm}
\begin{lstlisting}
typedef MonomialT<real_t>  Monomial;
typedef PolynomialT<real_t> Polynomial;
typedef PolynomialBasisT<real_t> PolynomialBasis; 
typedef PolynomialsBasisT<real_t> PolynomialsBasis;
\end{lstlisting}
\vspace{.3cm}
Some usual scalar polynomials spaces may be constructed in 1d, 2d or 3d:

\[
\begin{array}{lll}
Pn[x,y,z]  &  :\ \sum a_{ijk} x^iy^jz^k  & i,j,k \ge 0,\   i+j+k \le n \\
PHn[x,y,z]  & :\ \sum a_{ijk} x^iy^jz^k  & i,j,k \ge 0,\  i+j+k = n \\
Qn[x,y,z]  &  :\ \sum a_{ijk} x^iy^jz^k  & i,j,k \ge 0,\  i,j,k \le n \\
Qns[x,y,z]  & :\ \sum a_{ijk} x^iy^jz^k  & i,j,k \ge 0,\  i\le nx, j\le ny, k\le nz
\end{array}
\]

Some particular vector polynomials spaces related to FE space are also provided (see \class{PolynomialsBasisT} class). They can be addressed using the enumeration:

\begin{lstlisting}
enum PolynomialSpace {_Pk,_PHk,_Qk,_Qks,_Rk, _SHk,_Dk,_DQk,_DQ2k};
\end{lstlisting}

Main algebraic operations are supported and some formal derivative operations too. Polynomials may be evaluated at point \((x,y,z)\) using operator ().\\

Besides, Polynomials may have a tree representation using \class{PolyNodeT} class. It may be useful when evaluating large polynomials.

\subsection{The \classtitle{MonomialT} class}
This class deals with monomials of the form :
\[x_1^{a_1}x_2^{a_2}x_3^{a_3}\]
storing only the three power numbers \(a_1,\ a_2,\ a_3\):
\vspace{.1cm}
\begin{lstlisting}
template <typename K = real_t>
class MonomialT
{
  public:
    dimen_t a1, a2, a3;
    \ldots
};
\end{lstlisting}
\vspace{.3cm}
Thus, it allows only to deal with monomials of 3 variables of type K. Note that no coefficient is attached to a monomial.\\

\class{MonomialT} class provides only one fundamental constructor,  overloaded operator () to evaluate monomial at a point or a vector of points, product by a monomial, comparison operators and print facilities:
\vspace{.1cm}
\begin{lstlisting}
MonomialT(dimen_t p1=0, dimen_t p2=0, dimen_t p3=0)
K operator()(const K& x1, const K& x2 = K(1), const K& x3 = K(1))const
K operator() (const std::vector<K>& x) const
MonomialT<K>& operator*=(const MonomialT<K>& m);
void swapVar(dimen_t v1, dimen_t v2, dimen_t v3);
string_t asString() const;
void print(std::ostream& out) const;
\end{lstlisting}
\vspace{.3cm}
\begin{lstlisting}
MonomialT<K> operator*(const MonomialT<K>& m1, const MonomialT<K>& m2);
friend std::ostream& operator<<(std::ostream& out, const MonomialT& m);
bool operator== (const MonomialT<K>& m1, const MonomialT<K>& m2);
bool operator!= (const MonomialT<K>& m1, const MonomialT<K>& m2);
bool operator < (const MonomialT<K>& m1, const MonomialT<K>& m2);
bool operator > (const MonomialT<K>& m1, const MonomialT<K>& m2);
\end{lstlisting}
\vspace{.3cm}

\subsection{The \classtitle{PolynomialT} class}
The \class{PolynomialT} class deals with polynomial up to 3 variables of value type K stored as a list of pairs of \class{MonomialT} object and a coefficient of type K:
\vspace{.1cm}
\begin{lstlisting}
template <typename K=real_t>
class PolynomialT
{
  public:
    std::list<std::pair<MonomialT<K>, K > > monomials;  
    real_t epsilon;                                     
    mutable PolyNodeT<K> tree;  
    \ldots
}                        
\end{lstlisting}
\vspace{.2cm}
The member data \var{epsilon} is used to round to 0 the very small coefficients of the polynomial. By default, its value is 100000*\var{theEpsilon} (about \(10^{-10}\) in double precision). The member data \var{tree} may store a tree representation (\class{PolyNode} object) of the polynomial, that is useful to evaluate it at a point in a faster and more stable way (generalized Horner algorithm).\\

The class provides simple constructors from monomials and a copy constructor:
\vspace{.1cm}
\begin{lstlisting}
PolynomialT();
PolynomialT(const MonomialT<K>&,const K& =K(1));
PolynomialT(const MonomialT<K>& ,const K&, const MonomialT<K>&, const K&);
PolynomialT(const PolynomialT<K>&);
PolynomialT<K>& operator=(const PolynomialT<K>& p);
\end{lstlisting}
\vspace{.2cm}
There are some useful tools:
\vspace{.1cm}
\begin{lstlisting}
void push_back(const K&, const MonomialT<K>&);  //add a monomial part
(const_) iterator begin() (const) ;
(const_)iterator end()(const) ;
dimen_t degree() const;
void clean(real_t asZero);  //remove "zero" part
void clean();
void swapVar(dimen_t v1, dimen_t v2, dimen_t v3);  
static PolynomialT<K> zero();
bool isZero() const;
\end{lstlisting}
\vspace{.2cm}
To evaluate the polynomial two methods are available. One evaluating the polynomial as a linear combination of monomials, the other one using a generalized Horner algorithm based on a tree representation of the polynomial:
\vspace{.1cm}
\begin{lstlisting}
void buildTree() const;    
K eval (const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const ;
K evalTree(const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const;
K operator() (const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const;
K operator() (const std::vector<K>& x) const;
\end{lstlisting}
\vspace{.1cm}
If tree representation is built, the \cmd{eval} functions used always tree representation.\\

Main algebraic operations are available, either as internal or external functions to the class:
\vspace{.1cm}
\begin{lstlisting}
PolynomialT<K>& operator *=(const MonomialT<K>&);
PolynomialT<K>& operator *=(const K&);
PolynomialT<K>& operator /=(const K& k);
PolynomialT<K>& operator +=(const MonomialT<K>&);
PolynomialT<K>& operator -=(const MonomialT<K>&);
PolynomialT<K>& operator +=(const PolynomialT<K>&);
PolynomialT<K>& operator -=(const PolynomialT<K>&);
PolynomialT<K>& operator *=(const PolynomialT<K>&);

//extern operation
template <typename K>
PolynomialT<K> operator *(const PolynomialT<K>& ,const MonomialT<K>&);   
PolynomialT<K> operator *(const MonomialT<K>&, const PolynomialT<K>&);
PolynomialT<K> operator *(const MonomialT<K>&, const K&);
PolynomialT<K> operator *(const K&, const MonomialT<K>&);
PolynomialT<K> operator /(const MonomialT<K>&, const K&);
PolynomialT<K> operator *(const PolynomialT<K>&, const K&);
PolynomialT<K> operator *(const K&, const PolynomialT<K>&);
PolynomialT<K> operator /(const PolynomialT<K>&, const K&);
PolynomialT<K> operator -(const PolynomialT<K>&);
PolynomialT<K> operator +(const PolynomialT<K>&, const PolynomialT<K>&);
PolynomialT<K> operator -(const PolynomialT<K>&, const PolynomialT<K>&);
PolynomialT<K> operator *(const PolynomialT<K>&, const PolynomialT<K>&);
\end{lstlisting}
\vspace{.3cm}
Using the operator \verb|(.)|, polynomials may be chained (say \(q(p1(x,y,z),p2(x,y,z), p3(x,y,z)\)) :
\vspace{.1cm}
\begin{lstlisting}
template <typename K>
PolynomialT<K> operator()(const PolynomialT<K>& px, 
                          const PolynomialT<K>& py=PolynomialT<K>(),
                          const PolynomialT<K>& pz=PolynomialT<K>()) const; 
PolynomialT<K>& replace(VariableName, const PolynomialT<K>&);                
\end{lstlisting}
\vspace{.3cm}
It is also possible to get some derivatives and integrals of polynomials:
\vspace{.1cm}
\begin{lstlisting}
template <typename K>
PolynomialT<K> dx(const MonomialT<K>&);
PolynomialT<K> dy(const MonomialT<K>&);
PolynomialT<K> dz(const MonomialT<K>&);
vector<PolynomialT<K> > grad(const MonomialT<K>&, dimen_t =3);
vector<PolynomialT<K> > curl(const MonomialT<K>&, dimen_t =3);
PolynomialT<K> derivative(VariableName, const MonomialT<K>&);
PolynomialT<K> integral(VariableName, const MonomialT<K>&);

PolynomialT<K> dx(const PolynomialT<K>&);
PolynomialT<K> dy(const PolynomialT<K>&);
PolynomialT<K> dz(const PolynomialT<K>&);
vector<PolynomialT<K> > grad(const PolynomialT<K>&, dimen_t =3);
vector<PolynomialT<K> > curl(const PolynomialT<K>&, dimen_t =3);
PolynomialT<K> derivative(VariableName, const PolynomialT<K>&);
PolynomialT<K> integral(VariableName, const PolynomialT<K>&);

vector<PolynomialT<K> > dx(const vector<PolynomialT<K> >&);
vector<PolynomialT<K> > dy(const vector<PolynomialT<K> >&);
vector<PolynomialT<K> > dz(const vector<PolynomialT<K> >&);
vector<PolynomialT<K> > curl(const vector<PolynomialT<K> >&);
PolynomialT<K> div(const vector<PolynomialT<K> >&);
PolynomialT<K> dot(const vector<PolynomialT<K> >&, const vector<K>&);
vector<PolynomialT<K> > derivative(VariableName,const vector<PolynomialT<K> >&)
vector<PolynomialT<K> > integral(VariableName,const vector<PolynomialT<K> >);

\end{lstlisting}
\vspace{.3cm}
Finally, some print facilities are provided:
\vspace{.1cm}
\begin{lstlisting}
string_t asString() const;  
void print(std::ostream&) const;
friend std::ostream& operator<<(std::ostream&, const PolynomialT&)
\end{lstlisting}
\vspace{.1cm}   

\subsection{The \classtitle{PolyNodeT} class}
In order to be computed in a faster and a more stable way, a polynomial may be represented as a tree and evaluated by traveling this tree. In this tree representation, each node represents one of the factor \(1,\ x_1,\ x_2\) or \(x_3\). When it is a leaf, the coefficient of the corresponding monomial is stored. For instance the polynomial \(2+4x+5xy+xz+3yz+5z^2\) has the following tree :
\begin{figure}[H]
\begin{center}
\includePict[width=17cm]{polynomial_tree}
\end{center}
\end{figure}
Horizontal lines represents addition while vertical lines represent product. Note that the tree representation is not unique.\\

The \class{PolyNodeT} class manages for each node a factor type and a coefficient, and some pointers to travel the tree:
\vspace{.1cm}
\begin{lstlisting}
template <typename K=real_t>
class PolyNodeT
{public:
    dimen_t type;        // 0: 1, 1: x1, 2:x2, 3:x3
    K coef;
    PolyNodeT<K>* parent, * child, * right;
    \ldots
};
\end{lstlisting}
\vspace{.3cm}
It offers only a few member functions to build the polynomial tree:
\vspace{.1cm}
\begin{lstlisting}
PolyNodeT(dimen_t =0, PolyNodeT<K>* =0, const K& =K(0));
~PolyNodeT();
void clear();
void insert(const MonomialT<K>&, const K&); 
bool rootNode() const;
bool isZero() const {return (coef==K(0) && child==0);}
void print(std::ostream&) const;
void printTree(std::ostream&, std::string&) const;
string_t varString() const; 
\end{lstlisting}
\vspace{.3cm}
The most important function is the operator ():
\vspace{.1cm}
\begin{lstlisting}
K operator()(const K& x, const K& y=K(1), const K& z=K(1)) const;
\end{lstlisting}
\vspace{.1cm}
that evaluates the polynomial at a point \((x,y,z)\).

\subsection{The \classtitle{PolynomialBasisT} class}

A space of polynomials (of finite dimension \(p\)) may be described by one of its basis, say a set of \(p\) polynomials.  The \class{PolynomialBasisT} class manages this basis as a list of \class{PolynomialT} objects. In fact, it inherits from std::list\verb?<?PolynomialT\verb?<?K\verb?>? \verb?>?:
\vspace{.1cm}
\begin{lstlisting}
template <typename K=real_t>
class PolynomialBasisT : public std::list<PolynomialT<K> >
{public:
    dimen_t dim;       // number of variables in Polynomials
    string_t name;     // basis name
  \ldots
};
\end{lstlisting}
\vspace{.3cm}
There exists some basic constructors from monomials and a general constructor for rather standard polynomial spaces identified by the enumerator \cmd{PolynomialSpace}:
\vspace{.1cm}
\begin{lstlisting}
enum PolynomialSpace {_Pk,_PHk,_Qk,_Qks,_Rk, _SHk,_Dk,_DQk,_DQ2k};

PolynomialBasisT(dimen_t =0, const string_t& ="");
PolynomialBasisT(dimen_t, const MonomialT<K>&, const string_t& na="");
PolynomialBasisT(dimen_t, const MonomialT<K>&, const MonomialT<K>&, 
                 const string_t& ="");
PolynomialBasisT(PolynomialSpace, dimen_t, dimen_t, dimen_t =0, dimen_t =0);
void buildPk(dimen_t);
void buildPHk(dimen_t);
void buildQk(dimen_t);
void buildQks(dimen_t , dimen_t =0, dimen_t=0);
void buildTree(); 
\end{lstlisting}
\vspace{.3cm}
The (scalar) polynomial spaces that can be constructed are (\(n\) number of variables, \(\alpha\) multi-index):

\[
\begin{array}{|l|l|c|}
\hline
\text{polynomial space} & \text{dimension}& \text{\xlifepp}\\
\hline 
 & & \\[-2mm]
\displaystyle P_k=\left\{\sum_{|a|\leq k}a_\alpha x^\alpha \right\} & \displaystyle C^{n+k}_k=\frac{(n+k)!}{k!\,n!} & \text{\_Pk}\\[6mm]
\displaystyle\tilde{P}_k=\left\{\sum_{|\alpha|= k}a_\alpha x^\alpha \right\} &
\begin{array}{l}
\displaystyle n=2\ :\ k+1\\
\displaystyle n=3\ :\ \frac{1}{2}(k+2)(k+1)
\end{array}
& \text{\_PHk} \\[6mm]
\displaystyle Q_k=\left\{\sum_{\alpha_1\leq k,\alpha_2\leq k, \alpha_3\leq k }a_\alpha x^\alpha \right\} &\displaystyle (k+1)^n& \text{\_Qk}\\[6mm]
\displaystyle\tilde{Q}_{lmn}=\left\{\sum_{\alpha_1\leq l,\alpha_2\leq m, \alpha_3\leq n }a_\alpha x^\alpha \right\} &\displaystyle (l+1)(m+1)(n+1)& \text{\_Qks}\\[4mm]
\hline
\end{array}
\]

Several functions are devoted to the management of the list:

\begin{lstlisting}
void push_back(const PolynomialT<K>&);
void add(const MonomialT<K>&);
void add(const PolynomialT<K>&);
void add(const PolynomialBasisT&);
iterator begin();
iterator end();
const_iterator begin() const;
const_iterator end() const;
size_t size() const;
void resize(size_t n);
void clean();
void clean(real_t asZero);
dimen_t degree() const;
void swapVar(dimen_t v1, dimen_t v2, dimen_t v3);   
\end{lstlisting}

\begin{focusbox}
The \class{PolynomialsBasisT} class is intended to deal with polynomial basis, but there is no tool to check independence of polynomials! 
\end{focusbox}

It is also possible to evaluate all the polynomials at a point using different ways:
\vspace{.1cm}
\begin{lstlisting}
std::vector<K>& eval(std::vector<K>& res, const K& x1, const K& x2 = K(1), 
                     const K& x3 = K(1)) const;
std::vector<K> eval(const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const;
std::vector<K>& evalTree(std::vector<K>& res, const K& x1, const K& x2 = K(1), 
                         const K& x3 = K(1)) const;
std::vector<K> evalTree(const K& x1, const K& x2 = K(1), 
                        const K& x3 = K(1)) const;
std::vector<K> operator()(const K& x1, const K& x2 = K(1), 
                          const K& x3 = K(1)) const;
std::vector<K>& operator()(std::vector<K>& res, const K& x1, const K& x2 = K(1), 
                           const K& x3 = K(1)) const:
\end{lstlisting}
\vspace{.3cm}
As usual, there are some print facilities:
\vspace{.1cm}
\begin{lstlisting}
void print(std::ostream&) const;
friend std::ostream& operator<<(std::ostream&, const PolynomialBasisT<K>& );
\end{lstlisting}
\vspace{.3cm}

\subsection{The \classtitle{PolynomialsBasisT} class}
In finite element approximation, some vector polynomial spaces are required. This is the role of the \class{PolynomialsBasisT} class. There is no class to deal with vector polynomial, they are represented by some \class{vector<PolynomialT>}. As a consequence, the \class{PolynomialsBasisT} class inherits from \class{list<std::vector<PolynomialT<K> > >}:
\vspace{.1cm}
\begin{lstlisting}
template <typename K=real_t>
class PolynomialsBasisT : public std::list<std::vector<PolynomialT<K> > >
{
public:
dimen_t dimVar;            // dimension of Polynomials (number of variables)
dimen_t dimVec;            // dimension of vectors of Polynomials
string_t name;             // basis name
\ldots
};
\end{lstlisting}
\vspace{.3cm}
Several constructors are available. Some construct product spaces from  \class{PolynomialBasisT} objects. There is a general constructor allowing to get particular vector polynomial spaces enumerated in \cmd{PolynomialSpace}:


\vspace{.1cm}
\begin{lstlisting}
PolynomialsBasisT(dimen_t =0, dimen_t =0, const string_t& ="");  
PolynomialsBasisT(const PolynomialBasisT<K>&, dimen_t, const string_t& na=""); 
PolynomialsBasisT(const PolynomialBasisT<K>&, const PolynomialBasisT<K>&, 
                  const string_t& ="");                                        
PolynomialsBasisT(const PolynomialBasisT<K>&, const PolynomialBasisT<K>&,
                  const PolynomialBasisT<K>&, const string_t& na="");          
PolynomialsBasisT(PolynomialSpace, dimen_t, dimen_t);         
void buildSHk(dimen_t);
void buildRk(dimen_t);
void buildDk(dimen_t);
void buildDQk(dimen_t);
void buildDQ2k(dimen_t);
void buildTree();                
\end{lstlisting}
\vspace{.3cm}
The following FE spaces are available:

\[
\begin{array}{|l|l|c|}
\hline
\text{polynomial space} & \text{dimension}& \text{\xlifepp}\\
\hline 
& & \\[-2mm]
\displaystyle S_k=\left\{p\in(\tilde{P}_k)^n;\ x.p=0 \right\} & n\dim\tilde{P}_k-\dim\tilde{P}_{k+1}  & \text{\_Sk}\\[2mm]
\displaystyle D_k=(P_{k-1})^n \oplus \tilde{P}_{k-1}x &
n\dim P_{k-1}+\dim\tilde{P}_{k-1} &  \text{\_Dk} \\[6mm]
\displaystyle R_k=(P_{k-1})^n \oplus Sk &  n\dim P_{k-1}+\dim S_{k} &  \text{\_Rk} \\[6mm]
DQk= Q_{k,k-1,k-1} \times Q_{k-1,k,k-1} \times Q_{k-1,k-1,k} & 3k(k-1)^2& \text{\_DQk} \\[6mm]
DQ2k_{2d} = (P_k)^2 \oplus \text{span}\left\{ \text{curl}\, x_1^{k+1}x_2, \text{curl}\, x_2^{k+1}x_1 \right\} & 2(\dim P_{k}+1)&  \text{\_DQ2k} \\[6mm]
\hline
\end{array}
\]

The operator * allows building tensor polynomials space, say if \(P[x_1,x_2]\) and  \(Q[x_3]\) are two polynomials sets, \((P*Q)[x_1,x_2,x_3]\) is the set:

\[
\left\{ p_i(x_1,x_2)q_j(x_3),\ i=1,m,\ j=1,n\right\},
\]

\begin{lstlisting}
PolynomialBasisT<K> operator*(const PolynomialBasisT<K>& P, 
                              const PolynomialBasisT<K>& Q)
\end{lstlisting}

Member functions are similar to member functions of the \class{PolynomialBasisT} class:

\begin{lstlisting}
void push_back(const std::vector<PolynomialT<K> >&);
void add(const PolynomialT<K>&);
void add(const PolynomialT<K>&, const PolynomialT<K>&);
void add(const PolynomialT<K>&, const PolynomialT<K>&, 
         const PolynomialT<K>&);
void add(const std::vector<PolynomialT<K> >&);
void add(const PolynomialsBasisT&);
iterator begin();
iterator end();
const_iterator begin() const;
const_iterator end() const;
size_t size() const;
void resize(size_t);
dimen_t degree() const;
void swapVar(dimen_t, dimen_t, dimen_t);
void clean();
void clean(real_t asZero);

//evaluation of basis
vector<vector<K> >& eval(vector<vector<K> >&, const K&, const K& = K(1), 
                        const K& = K(1)) const;
vector<vector<K> >  eval(const K&, const K& = K(1), const K& = K(1)) const;
vector<vector<K> >& evalTree(vector<vector<K> >&, const K&,
                             const K& = K(1), const K& = K(1)) const;
vector<vector<K> >  evalTree(const K&, const K& = K(1), const K& = K(1)) const;
vector<vector<K> >& operator()(vector<vector<K> >&, const K&, 
                               const K& = K(1), const K& = K(1)) const;
                               
//print facilities
void print(std::ostream&) const;
friend std::ostream& operator<<(std::ostream&, const PolynomialsBasisT<K>&)
\end{lstlisting}

\displayInfos{library=utils, header=polynomials.hpp, implementation=polynomials.hpp,
test=test\_polynomials.cpp, header dep={config.h, utils.h}}
