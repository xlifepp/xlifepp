%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Reference elements}

\subsection{The \classtitle{ShapeValues} class}

A \class{ShapeValues} object stores the evaluation of a shape function at a given point. Thus,
it carries 2 real vectors of:
\vspace{0.1cm}
\begin{lstlisting}
class ShapeValues
{
public:
  vector<Real> w;          //shape functions at a  point
  vector<vector<Real>> dw; //first derivatives (dx,dy,[dz])
  vector<vector<Real>> d2w;//2nd derivatives   (dxx,dyy,dxy,[dzz,dxz, dyz])
  
};
\end{lstlisting}
\vspace{0.2cm}

It offers:
\begin{itemize}
\item basic constructors:
\begin{lstlisting}
ShapeValues();	//!< default constructor
ShapeValues(const ShapeValues&);		 	 //copy constructor
ShapeValues& operator=(const ShapeValues&);  // assignment operator=
ShapeValues(const RefElement&);              //constructor with associated RefElement 
ShapeValues(const RefElement&, const Dimen); //constructor with associated RefElement 
\end{lstlisting}
\item two public size accessors and empty query:
\begin{lstlisting}[deletekeywords={[3] size}]
Dimen dim() const;
Number nbDofs() const;
bool isEmpty() const;
\end{lstlisting}
\item some functions addressing the data:
\begin{lstlisting}
void resize(const RefElement&, const Dimen); // resize 
void set(const real_t);            // set shape functions to const value
void assign(const ShapeValues&);   // fast assign of shapevalues into current 
\end{lstlisting}
\item some mapping functions
\begin{lstlisting}
void extendToVector(dimen_t d);  //extend scalar shape function to vector 
void map(const ShapeValues&, const GeomMapData&, bool der1, bool der2);  //standard mapping
void contravariantPiolaMap(const ShapeValues&, const GeomMapData&, bool der1, bool der2); 
void covariantPiolaMap(const ShapeValues&, const GeomMapData&, bool der1, bool der2);     
void Morley2dMap(const ShapeValues&, const GeomMapData&, bool der1, bool der2);    
void Argyris2dMap(const ShapeValues& rsv, const GeomMapData& gd, bool der1, bool der2);   
void changeSign(const std::vector<real_t>&, dimen_t);  //change sign of shape functions according to a sign vector
\end{lstlisting}
\end{itemize}

The contravariant and covariant Piola maps are respectively used for Hdiv and Hcurl finite element families. They preserve respectively the normal and tangential traces. If \(J\) denotes the jacobian of the mapping from the reference element to any element, the covariant map is \(J^{-t}\mathbf{\hat{u}}\) and the contravariant map \(J/|J|\mathbf{\hat{u}}\) where \(\mathbf{\hat{u}}\) is a vector field in the reference space. The Morley and Argyris map are specific to these elements, they preserve first and second derivatives involved in such elements.\\

\displayInfos{library=finiteElements, header=ShapeValues.hpp, implementation=ShapeValues.cpp,
header dep={config.h, RefElement.hpp, utils.h}}

\subsection{The \classtitle{RefElement} class}

The \class{RefElement} is the main class of the \lib{finiteElement} library. First, it has
an object of each type previously seen in this chapter, namely \class{Interpolation}, {\class
GeomRefElement}, \class{ShapeValues} and \class{RefDof}. Next, It is the mother class of
the wide range of true reference elements, identified by shape and interpolation type. That
is why it is a complex class collecting a lot of information and providing  many numbering functions such as the number of DoFs, the same number on vertices, on sides, on side of sides, \ldots

To define completely a reference element, we also need data on sides and on side of sides,
such as DoF numbers and reference elements. Side and side of side reference elements will be
built only when they are needed, equivalent to say only they have meaning. It is the case for
H1 finite elements, but not for Hcurl and Hdiv finite elements. 

So, the \class{RefElement} attributes are:

\begin{lstlisting}
class RefElement
{
public:
  GeomRefElement* geomRefElem_p;        //pointer to geometric reference element
  const Interpolation* interpolation_p; //interpolation parameters
  vector<RefDof*> refDofs;          //local reference Degrees of Freedom
  FEMapType mapType;                //type of map applied to shape functions of reference element
  DofCompatibility DoFCompatibility;//compatibility rule to applied to side DoFs 
  dimen_t dimShapeFunction;         //dimension of shape function
  bool rotateDof;                   //if true, apply rotation (given by children) to shape values 
  number_t maxDegree;               //maximum degree of shape functions

protected:
  String name_;	  //reference element name (for documentation)
  Number nbDofs_; //number of Degrees Of Freedom
  Number nbPts_;  //number of Points (for local coordinates of points)
  Number nbDofsOnVertices_;   //nb of sharable D.o.F on vertices (first D.o.F)
  Number nbDofsInSideOfSides_;//nb of sharable D.o.F on side of sides (edges)
  Number nbDofsInSides_;      //nb of sharable D.o.F on sides (faces or edges)
  Number nbInternalDofs_;     //nb of non-sharable D.o.F's
  vector<RefElement*> sideRefElems_;       //pointers to side reference elements 
  vector<RefElement*> sideOfSideRefElems_; //pointers to side of side reference elements

public:
  vector<vector<Number>> sideDofNumbers_;       //DoF numbers for each side
  vector<vector<Number>> sideOfSideDofNumbers_; //DoF numbers for each side of side
  PolynomialsBasis Wk;                     //shape functions basis as polynomials
  vector<PolynomialsBasis> dWk;            //derivatives of shape functions basis as polynomials
  bool hasShapeValues;                     //ref element has shapevalues, generally true
  map<Quadrature*,vector<ShapeValues>> qshvs_;   //temporary structure to store shape values 
  map<Quadrature*,vector<ShapeValues>> qshvs_aux;//other temporary structure to store shape values
  
  static vector<RefElement*> theRefElements; //to store run-time RefElement pointers
  static const bool isSharable_ = true;
\end{lstlisting}
\vspace{0.2cm}
The following enumeration collects the types of affine mapping from reference element to any element. It depends on finite element type:
\begin{lstlisting}
enum FEMapType {_standardMap,_contravariantPiolaMap,_covariantPiolaMap,_MorleyMap,_ArgyrisMap};    
\end{lstlisting}
\vspace{0.2cm}   
It offers:

\begin{itemize}
\item Some constructors, the default one and a constructor by shape type and interpolation:
\vspace{0.1cm}
\begin{lstlisting}
RefElement(); //default constructor
RefElement(ShapeType, const Interpolation* ); //constructor by shape & interpolation
\end{lstlisting}
\vspace{0.2cm}
Note that the copy constructor and the assignment operator are private.
\item Some public accessors:
\begin{lstlisting}
const Interpolation& interpolation() const;
String name() const;
Number nbPts() const;               
Number nbDofsOnVertices() const;    //nb of D.o.F supported by vertices 
Number nbDofsInSides() const;       //nb of D.o.F strictly supported by side
Number nbDofsInSideOfSides() const; //nb of D.o.F strictly supported by side of side
bool isSimplex() const ;            //true if shape is a simplex
dimen_t dim() const;                //element dimension

const RefElement* refElement(Number sideNum = 0) const; //reference element of side
const GeomRefElement* geomRefElement(Number sideNum = 0) const; //GeomRefElement of side       
Number nbDofs(const Number sideNum = 0, const Dimen sideDim = 0) const;//number of  D.o.F by side
Number nbInternalDofs(const Number sideNum = 0, const Dimen sideDim = 0) const;
ShapeType shapeType() const;   //shape of element
\end{lstlisting}
\item Some build functions of side and side of side reference elements:
\begin{lstlisting}[deletekeywords={[3] x}]
void sideOfSideRefElement();      //reference element constructors on element edges
void sideRefElement();            //reference element constructors on element faces
\end{lstlisting}
\item Some functions related to shape function computation
\begin{lstlisting}[deletekeywords={[3] x}]
void buildPolynomialTree();       //build tree representation of polynomial shape functions
number_t shapeValueSize() const;
virtual void computeShapeFunctions()  //compute shape functions
virtual void computeShapeValues(vector<Real>::const_iterator it_pt, bool der1 = true, 
             bool der2=false)  const = 0; //compute shape functions at point
\end{lstlisting}
Shape function of high order element are boring to find. So, \xlifepp provides tools to get them in a formal way, using \class{Polynomials} class. The virtual function \verb?computeShapeFunctions? computes them and the \verb?buildPolynomialTree? function builds a tree representation of the formal representation of shape functions, in order to make faster their computations.
\item Some virtual functions to guarantee correct matching of DoFs:
\begin{lstlisting}[deletekeywords={[3] x}]
virtual vector<number_t> DoFsMap(const number_t& i, const number_t& j, const number_t& k=0) const;  
virtual number_t sideDofsMap(const number_t& n, const number_t& i, const number_t& j, 
                             const number_ & k=0) const; 
virtual number_t sideofsideDofsMap(const number_t& n, const number_t& i, const number_t& j=0)const;
number_t sideOf(number_t) const;       //side number of a DoF
number_t sideOfSideOf(number_t) const; //return side of side number of a DoF 
\end{lstlisting}
\item Some general build functions for DoFs:
\begin{lstlisting}
void LagrangeRefDofs(const int, const int, const int, const Dimen);
\end{lstlisting}
\vspace{0.2cm}
As the inheritance diagram is based on shape, these functions specify the interpolation type
\item For output purpose,  a \class{RefElement} may be split in first order elements, either simplices or of same shape, at every order:
\vspace{.1cm}
\begin{lstlisting}
virtual vector<vector<number_t>> splitP1() const;
virtual vector<pair<ShapeType,vector<number_t>>> splitO1() const;
\end{lstlisting}
\item Some external functions to find a reference element in the static list of \class{RefElement}
and create it if not found:
\begin{lstlisting}
RefElement* findRefElement(ShapeType, const Interpolation*);
RefElement* selectRefSegment(const Interpolation*);
RefElement* selectRefTriangle(const Interpolation*);
RefElement* selectRefQuadrangle(const Interpolation*);
RefElement* selectRefTetrahedron(const Interpolation*);
RefElement* selectRefPrism(const Interpolation*);
RefElement* selectRefHexahedron(const Interpolation*);
RefElement* selectRefPyramid(const Interpolation*);
\end{lstlisting}
\end{itemize}

\displayInfos{library=finiteElements, header=RefElement.hpp, implementation=RefElement.cpp,
test={test\_segment.cpp, test\_triangle.cpp, test\_quadrangle.cpp, test\_tetrahedron.cpp, test\_hexahedron.cpp, test\_prism.cpp, test\_pyramid.cpp}, header dep={config.h, utils.h, Interpolation.hpp, ShapeValues.hpp, GeomRefElement.hpp, RefDof.hpp}}

\subsection{Child classes for segment}

The \class{RefSegment} class has three main child classes : 
\begin{itemize}
\item \class{LagrangeStdSegment} which implements standard Lagrange element at any order
\item \class{LagrangeGLSegment} which implements  Lagrange element based on Gauss-Lobatto points at any order
\item \class{HermiteStdSegment} which implements  standard Hermite element, currently only P3 Hermite
\end{itemize} 
\begin{figure}[H]
\centering
\inputTikZ{segment}
\caption{The \class{RefSegment} main class diagram.}
\end{figure}

\subsection{Child classes for triangle}

The \class{RefTriangle} class provides the following types of element:
\begin{itemize}
\item standard Lagrange element at any order \class{LagrangeStdTriangle <\_Pk>} and \class{LagrangeStdTrianglePk}; the template  \class{LagrangeStdTriangle<\_Pk>} class is defined only for low order (up to 6) with better performance and \class{LagrangeStdTrianglePk} class is designed for any order. It uses a general representation of shape functions on xy-monoms basis get from the solving of a linear system. This method is not stable for very high order.
\item \class{HermiteStdTriangle} which implements  standard Hermite element, currently only P3 Hermite (not H2 conforming)
\item \class{CrouzeixRaviartStdTriangle} which implements  the Crouzeix-Raviart element, currently only P1  (not H1 conforming)
\item \class{RaviartThomasTriangle} which implements the Raviart-Thomas elements (Hdiv conforming): \class{ RaviartThomasStdTriangleP1} for Raviart-Thomas standard P1 element and \class{RaviartThomasStdTrianglePk}  for Raviart-Thomas at any order.
\item \class{NedelecTriangle} which implements  the Nedelec elements (Hcurl conforming): \class{NedelecFirstTriangleP1} for Nedelec first family of order 1 element, \class{NedelecFirstTrianglePk} for Nedelec first family of any order and \class{NedelecSecondTrianglePk} for Nedelec second family of any order.
\item \class{MorleyTriangle} and \class{ArgyrisTriangle} elements designed for H2-approximation (non H2 conforming and H2 conforming).
\end{itemize} 
\begin{figure}[H]
\centering
\inputTikZ{triangle}
\caption{The \class{RefTriangle} main class diagram.}
\end{figure}

\subsection{Child classes for quadrangle}

\begin{figure}[H]
\centering
\inputTikZ{quadrangle}
\caption{The \class{RefQuadrangle} main class diagram.}
\end{figure}

\subsection{Child classes for tetrahedron}

\begin{figure}[H]
\centering
\inputTikZ{tetrahedron}
\caption{The \class{RefTetrahedron} main class diagram.}
\end{figure}

The \class{RefTetrahedron} class provides the following types of element:
\begin{itemize}
\item \class{LagrangeStdTetrahedron<\_Pk>} class is defined only for low order (up to 6) with better performance and \class{LagrangeStdTetrahedronPk} class is designed for any order. It uses a general representation of shape functions on xyz-monoms basis get from the solving of a linear system. This method is not stable for very high order.
\item \class{CrouzeixRaviartTetrahedron} which implements  the Crouzeix-Raviart element, currently only P1  (not H1 conforming)
\item \class{NedelecFaceTetrahedron} which implements  the Nedelec elements Hdiv conforming: \class{NedelecFaceFirstTetrahedronPk} for Nedelec first family of any order and \class{NedelecFaceSecondTetrahedronPk} for Nedelec second family of any order. The second family is not yet available.
\item \class{NedelecEdgeTetrahedron} which implements  the Nedelec elements Hrot conforming: \class{NedelecEdgeFirstTetrahedronPk} for Nedelec first family of any order and \class{NedelecEdgeSecondTetrahedronPk} for Nedelec second family of any order. The second family is not yet available.
\end{itemize}
Short identifiers of face/edge elements are defined in the following enumerations:
\begin{lstlisting}
enum FeFaceType
{
  _RT_1 = 1, RT_1 = _RT_1, _NF1_1 = _RT_1, NF1_1 = _RT_1,
  _RT_2 ,    RT_2 = _RT_2, _NF1_2 = _RT_2, NF1_2 = _RT_2,
  _RT_3 ,    RT_3 = _RT_3, _NF1_3 = _RT_3, NF1_3 = _RT_3,
  _RT_4 ,    RT_4 = _RT_4, _NF1_4 = _RT_4, NF1_4 = _RT_4,
  _RT_5 ,    RT_5 = _RT_5, _NF1_5 = _RT_5, NF1_5 = _RT_5,
  _BDM_1 ,    BDM_1 = _BDM_1, _NF2_1 = _BDM_1, NF2_1 = _BDM_1,
  _BDM_2 ,    BDM_2 = _BDM_2, _NF2_2 = _BDM_2, NF2_2 = _BDM_2,
  _BDM_3 ,    BDM_3 = _BDM_3, _NF2_3 = _BDM_3, NF2_3 = _BDM_3,
  _BDM_4 ,    BDM_4 = _BDM_4, _NF2_4 = _BDM_4, NF2_4 = _BDM_4,
  _BDM_5 ,    BDM_5 = _BDM_5, _NF2_5 = _BDM_5, NF2_5 = _BDM_5
};

enum FeEdgeType
{
  _N1_1 = 1, N1_1 = _N1_1, _NE1_1 = _N1_1, NE1_1 = _N1_1,
  _N1_2 ,    N1_2 = _N1_2, _NE1_2 = _N1_2, NE1_2 = _N1_2,
  _N1_3 ,    N1_3 = _N1_3, _NE1_3 = _N1_3, NE1_3 = _N1_3,
  _N1_4 ,    N1_4 = _N1_4, _NE1_4 = _N1_4, NE1_4 = _N1_4,
  _N1_5 ,    N1_5 = _N1_5, _NE1_5 = _N1_5, NE1_5 = _N1_5,
  _N2_1 ,    N2_1 = _N2_1, _NE2_1 = _N2_1, NE2_1 = _N2_1,
  _N2_2 ,    N2_2 = _N2_2, _NE2_2 = _N2_2, NE2_2 = _N2_2,
  _N2_3 ,    N2_3 = _N2_3, _NE2_3 = _N2_3, NE2_3 = _N2_3,
  _N2_4 ,    N2_4 = _N2_4, _NE2_4 = _N2_4, NE2_4 = _N2_4,
  _N2_5 ,    N2_5 = _N2_5, _NE2_5 = _N2_5, NE2_5 = _N2_5
};
\end{lstlisting}
The naming convention is based on the FE periodic table. Note that there is an equivalence between  NF1\_k and RT\_k (Raviart Thomas), NF2\_k BDM\_k (Brezzi Douglas Marini), N1\_k and NE1\_k and, N2\_k and NE2\_k. \\

\begin{ideabox}
\class{NedelecEdgeTetrahedron} and  \class{NedelecFaceTetrahedron} classes use a general process to build shape functions as polynomials related to moment DoFs. To match DoFs on shared edge or shared face, the ascending numbering of vertices is implicitly used even if it is not in fact. This method is particular touchy in case of face DoFs of Nedelec Edge element. Indeed, such DoFs depend on the choice of two tangent vectors, generally two edges of faces of the reference tetrahedron that are mapped to some edges of physical element in a non-trivial way. To ensure a correct matching of such DoFs on a shared face, a rotation has to be applied to shape values to guarantee that they correspond to the same tangent vectors. This is the role of the member function \cmd{NedelecEdgeFirstTetrahedronPk::rotateDofs}. Note that if the element vertex numbering and face vertex numbering are ascending, {\itshape rotateDofs} does nothing. This trick is commonly used to overcome this difficulty but as \xlifepp makes no assumption on geometry, this DoFs rotation is mandatory.  
 More details on edge/face elements are provided in a specific paper.
\end{ideabox}


\subsection{Child classes for hexahedron}

\begin{figure}[H]
\centering
\inputTikZ{hexahedron}
\caption{The \class{RefHexahedron} main class diagram.}
\end{figure}

\subsection{Child classes for prism}

\begin{figure}[H]
\centering
\inputTikZ{prism}
\caption{The \class{RefPrism} main class diagram.}
\end{figure}

\subsection{Child classes for pyramid}

\begin{figure}[H]
\centering
\inputTikZ{pyramid}
\caption{The \class{RefPyramid} main class diagram.}
\end{figure}

