%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{KdTree} class}

The \class{KdTree} class implements the well known kdtree which is a binary tree where objects are separated using component by component separation. Usually objects are points, but the following implementation is templated to deal with any class T that have components that can be separated. More precisely, the T class has to provide the following: 
\vspace{.1cm}
\begin{lstlisting}
T::SepValueType;
T::operator()(int);
T::dim();
int Tcompare(const T& t, int c, const T::SepValueType& s);
//return  -1 (t_c<s), 1 (t_c>s) or 0 (t_c=s)
int maxSeparator(const T& t1, const T& tT2, int &c, T::SepValueType& s);
// find the component index c where separation is maximal, update s as the middle value and return the comparison of t1 and t2 (1,-1,0)
\end{lstlisting}
\vspace{.3cm}
Such tree may find the nearest object of a given object in a fast way (\(n\log\,n\) in the most cases and \(n\) in worst cases).\\

The \class{KdTree} class is based on the \class{KdNode} class which is in fact the main class, \class{KdTree} handling the root node. A \class{KdNode} is either a  separation line (component that separates two points and value of separation) or a terminal leaf containing a separated object.

\subsection*{\classtitle{KdNode} class}

\vspace{.1cm}
\begin{lstlisting}
template <class T>
class KdNode
{
private :
   typedef typename T::SepValueType SvType; 
   KdNode * parent_;      //!< pointer to parent node
   KdNode * left_;        //!< pointer to left node
   KdNode * right_;       //!< pointer to right node
   const T * obj_;        //!< object pointer linked to node
   int separator_;        //!< separation component
   SvType sv_;            //!< separation value
}
\end{lstlisting}
\vspace{.3cm}
It has two basic constructors, a destructor and a node insertion function
\vspace{.1cm}
\begin{lstlisting}
KdNode();
KdNode(const T *O,int s, KdNode * p);
~KdNode();
void insert(const T *);         
\end{lstlisting}
\vspace{.3cm}
The \cmd{insert} function is the fundamental function used in building of kdtree : it travels in tree up to find a free box containing the object to be inserted; if no free box is found, the last non-empty box containing object to be inserted and another object is split in two boxes. The tree obtained by this method has not an optimal balancing !
\begin{figure}[H]
\centering
\includePict[width=8cm]{kdtree.pdf}
\caption{example of separation of 2d points}
\end{figure}
The class provides some utilities
\vspace{.1cm}
\begin{lstlisting}
Number& depth(Number &) const;      //!< return node depth
void print(std::ostream&, std::string &) const;
void printBoxes(std::ostream &os, Box<SvType> b) const;
template <class S>
friend std::ostream& operator<<(std::ostream& os,const KdNode<S> & node);
\end{lstlisting}
\vspace{.3cm}
and the important function to search the nearest object of a given object:
\vspace{.1cm}
\begin{lstlisting}
void searchNearest(const T *, const T *&, SvType &);  
\end{lstlisting}
\vspace{.3cm}
\begin{figure}[H]
\centering
\includePict[width=8cm]{kdtree_search.pdf}
\end{figure}

\subsection*{\classtitle{KdTree} class}

As already mentioned \class{KdTree} handles the root node of tree and mainly encapsulated the \class{KdNode} functions
\vspace{.1cm}
\begin{lstlisting}
template <class T>
class KdTree
{
private :
 typedef typename T::SepValueType SvType;   //separator value type
 KdNode<T>* root;                           //rootnode of the tree
 Dimen dimT;                                //object dimension
 }
\end{lstlisting}
\vspace{.3cm}
\vspace{.1cm}
\begin{lstlisting}
KdTree() {root=new KdNode<T>();dimT=0;}
KdTree(const std::vector<T>&);
~KdTree() {delete root;};
bool isVoid() const;
Dimen dim() const;
void insert(const T&);                    
void insert(const std::vector<T>&);      
Number depth() const;                     
const T* searchNearest(const T &) const;  
void print(std::ostream& out) const;
template <typename R>
 void printBoxes(std::ostream &os, const Box<R>& rb);
template <typename R>
 void printBoxes(const std::string& file, const Box<R>& rb);
template <class S>
   friend std::ostream& operator<<(std::ostream& os,const KdTree<S>& node);
\end{lstlisting}
\vspace{.3cm}
\displayInfos{library=utils, header=KdTree.hpp, implementation=KdTree.hpp,
test=test\_KdTree.cpp, header dep={space.h, config.h, utils.h}}
