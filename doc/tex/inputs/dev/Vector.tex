%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{Vector} class}

The purpose of the \class{Vector} class is mainly to deal with complex or real vector. It
is derived from the \class{std:vector<K>} and templated by the vector type:
\vspace{.2cm}
\begin{lstlisting}
template<typename K>
class Vector : public std::vector<K>
{
public:
    typedef K type_t;
    typedef typename std::vector<K>::iterator ivk_t;
    typedef typename std::vector<K>::const_iterator civk_t;
    \ldots
\end{lstlisting}
\vspace{.2cm}
As it is templated, it is also possible to deal with vector of vectors and generally vector
of anything. But some algebraic functions no longer work.\\

It offers some basic constructors and assignment function:
\vspace{.2cm}
\begin{lstlisting}
Vector();                        //default constructor
Vector(const dimen_t);           //constructor by length
Vector(const dimen_t, const K&); //constructor by length for constant vector
void assign(const K&);           //assign const scalar value to all entries       
Vector<K>& operator=(const K&);  //assign const scalar value to all entries
\end{lstlisting}
\vspace{.2cm}
The default constructor initializes a vector of length 1 with a "0" value (\emph{K}).\\

One can access (both read and read/write access) to the component either by iterators (\cmd{begin}
and \cmd{end}) or by index from 1 to the length:
\vspace{.2cm}
\begin{lstlisting}
size_t size() const;        //overloaded size
ivk_t begin();              //overloaded iterator begin()
civk_t begin() const;       //overloaded const iterator begin()
ivk_t end();                //overloaded iterator end()
civk_t end() const;         //overloaded const iterator end()
K  operator()(int i) const; //i-th component (i=1..n) (r)
K& operator()(int i);       //i-th component (i=1..n) (r/w)  
\end{lstlisting}
\vspace{.2cm}

There are also generalized accessors that allow to get or set a part of vector, indices being given either by lower and upper indices or by a vector of indices:
\vspace{.2cm}
\begin{lstlisting}
Vector<K> get(Number i1, Number i2) const;          //get i1->i2 components
Vector<K> get(const std::vector<Number>& is) const; //get 'is' components
void set(Number i1, Number i2, const std::vector<K>& vec);  //set i1->i2 components
void set(const std::vector<Number>& is, const std::vector<K>& vec); //set 'is' components
Vector<K> operator()(const std::vector<Number>& is) const;  //get 'is' components
Vector<K> operator()(Number i1, Number i2) const;     //get i1->i2 components
\end{lstlisting}
\vspace{.2cm}

Through overloaded operators, the \class{Vector} class provides the following (internal) algebraic
operations:
\vspace{.2cm}
\begin{lstlisting}
Vector<K>& operator+=(const K&); //add a scalar to current vector
Vector<K>& operator-=(const K&); //substract a scalar to current vector
Vector<K>& operator*=(const K&); //multiply the current vector by a scalar
Vector<K>& operator/=(const K&); //divide the current vector by a scalar
Vector<K>& operator+=(const Vector<K>&); //add a vector to current vector
Vector<K>& operator-=(const Vector<K>&); //substract a vector to current vector
\end{lstlisting}
\vspace{.2cm}
Note that compatibility tests are performed on such computations:
\vspace{.2cm}
\begin{lstlisting}
void mismatchSize(const String&, const size_t) const;
void divideByZero(const String&) const;
void complexCastWarning(const String&, const dimen_t) const;
\end{lstlisting}
\vspace{.2cm}
The class also provides norm computations:
\vspace{.2cm}
\begin{lstlisting}
Real norminfty() const;     //sup norm
Real norm2squared() const;  //squared quadratic norm
Real norm2() const;         //<quadratic norm
\end{lstlisting}
\vspace{.2cm}
and output facilities (templated ostream \(<<\)):
\vspace{.2cm}
\begin{lstlisting}
void print(std::ostream& ) const;   
template<typename K>
  std::ostream& operator<<(std::ostream&,const Vector<K>&); 
\end{lstlisting}  
\vspace{.2cm}
Algebraic operations are defined as external template functions (\emph{template <typename
K>} is omitted):
\vspace{.2cm}
\begin{lstlisting}
Vector<K> operator+(const Vector<K>&);                  //+U
Vector<K> operator-(const Vector<K>&);                  //-U
Vector<K> operator+(const Vector<K>&,const Vector<K>&); //U+V
Vector<K> operator+(const K&, const Vector<K>&);        //x+U
Vector<K> operator+(const Vector<K>&,const K&);         //U+x
Vector<K> operator-(const Vector<K>&,const Vector<K>&); //U-V
Vector<K> operator-(const K&, const Vector<K>&);        //x-U
Vector<K> operator-(const Vector<K>&,const K&);         //U-x
Vector<K> operator*(const K&, const Vector<K>&);        //x*U
Vector<K> operator*(const Vector<K>&, const K&);        //U*x
Vector<K> operator/(const Vector<K>&, const K&);        //U/x
Vector<K> conj(const Vector<K>&);                       //conjugate(U)
\end{lstlisting}
\vspace{.2cm}
Not that the conjugate function is defined for every vector of type \emph{K}; it assumes that
the \emph{conj} function is also defined for \emph{K} value and returns a \emph{K} value.\\

To insure automatic cast from real to complex (not the contrary) and particular functions related
to complex, some template specializations are provided:
\begin{lstlisting}
std::ostream& operator<<(std::ostream&, const Vector<Real>&);    // real vector flux insertion 
std::ostream& operator<<(std::ostream&, const Vector<Complex>&); //complex vector flux insertion
Vector<Complex> operator+(const Vector<Real>&, const Vector<Complex>&); //rU+cV
Vector<Complex> operator+(const Vector<Complex>&, const Vector<Real>&); //cV+rU
Vector<Complex> operator-(const Vector<Real>&, const Vector<Complex>&); //rU-cV
Vector<Complex> operator-(const Vector<Complex>&, const Vector<Real>&); //cV-rU
Vector<Complex> operator+(const Vector<Real>&, const Complex&);         //rU+cx
Vector<Complex> operator+(const Complex&, const Vector<Real>&);         //cx+rU
Vector<Complex> operator+(const Vector<Complex>&, const Real&);         //cU+rx
Vector<Complex> operator+(const Real&, const Vector<Complex>&);         //rx+cU
Vector<Complex> operator-(const Vector<Real>&, const Complex&);         //rU-cx
Vector<Complex> operator-(const Complex&, const Vector<Real>&);         //cx-rU
Vector<Complex> operator-(const Vector<Complex>&, const Real&);         //cU-rx
Vector<Complex> operator-(const Real&, const Vector<Complex>&);         //rx-cU
Vector<Complex> operator*(const Complex&, const Vector<Real>&);         //cx*rU
Vector<Complex> operator*(const Vector<Real>&, const Complex&);         //rU*cx
Vector<Complex> operator*(const Real&, const Vector<Complex>&);         //rx*cU
Vector<Complex> operator*(const Vector<Complex>&, const Real&);         //cU*rx
Vector<Complex> operator/(const Vector<Real>&, const Complex&);         //rU/cx
Vector<Complex> operator/(const Vector<Complex>&, const Real&);         //cU/rx
Vector<Real> real(const Vector<Complex>&);                                //real(U)
Vector<Real> imag(const Vector<Complex>&);                                //imag(U)
Vector<Complex> cmplx(const Vector<Real>&);                            
Vector<Complex> cmplx(const Vector<Complex>&);                           
\end{lstlisting}
\vspace{.2cm}
Note that the operations and their specializations allow all standard operations a user can
wait from a real or complex vector. In particular, operations involving a casting from real
to complex. On the other hand, be cautious when using a \emph{Vector\(<\)Vector\(<\)K\(>>\) V},
all algebraic operations involving \emph{V} and a scalar (of type K) are not supported, only
those involving  \emph{V} and a \emph{vector<K>} are working. Besides, automatic casting from
real to complex of vector of vectors is not supported, use the \emph{cmplx} function to convert
to complex type. 
\vspace{.1cm}
\begin{lstlisting}
Vector<Vector<Complex> > cmplx(const Vector<Vector<Real> >&);
\end{lstlisting}

\begin{focusbox}
Although the \class{Point} class also inherits from \class{std::vector} and offers similar
algebraic computations facilities to the \class{Vector} class, it is not linked to the {\class
Vector} class because it deals only with real vector and offers other functionalities, in particular comparison capabilities.
\end{focusbox}

Useful aliases hiding template syntax, are available for end users:
\begin{lstlisting}
typedef Vector<Real> Reals;
typedef Vector<Complex> Complexes;
typedef Vector<Real> RealVector;
typedef Vector<Complex> ComplexVector;
typedef Vector<Vector<Real> > RealVectors;
typedef Vector<Vector<Complex> > ComplexVectors;
\end{lstlisting}
They are defined in \emph{users\_typedef.hpp} (\lib{init} library).

\displayInfos{library=utils, header=Vector.hpp, implementation=Vector.cpp, test=test\_Vector.cpp,
header dep=config.h}
