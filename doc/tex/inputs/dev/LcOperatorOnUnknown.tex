%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{LcOperatorOnUnknown} class}

The\class{LcOperatorOnUnknown} class deals with linear combination of \class{OperatorOnUnknown} objects:
\[
\sum_{i=1,n} c_i\ op(u_i) | dom_i\]
where \(c_i\) is a complex coefficient, \(op(u_i)\) an operator on unknown \(u_i\) (\class{OperatorOnUnknown} object) and possibly a geometrical domain \(dom_i\) (\class{Domain } object).
\class{LcOperatorOnUnknown} objects are useful to describe essential condition (see \lib{essentialConditions} library):
\[
\sum_{i=1,n} c_i\ op(u_i) | dom_i = g.\]
\class{LcOperatorOnUnknown} objects are also useful to create
\class{LcOperatorOnUnknowns} in a condensed way:
\[
\left( \sum_{i=1,n} c^u_i\ op(u_i) \right)\ O\ \left( \sum_{j=1,n} c^v_j\
op(u_j) \right) = \sum_{i,j=1,n} c^u_i c^v_jop(u_i)\ op(u_i)\ O\ op(v_j).\]

The\class{LcOperatorOnUnknown} class inherits from \class{std::vector} class:
\vspace{.1cm}
\begin{lstlisting}
typedef std::pair<OperatorOnUnknown*, Complex> OpuValPair;

class LcOperatorOnUnknown : public std::vector<OpuValPair>
{
public :
    std::vector<Domain*> domains_;  
}
\end{lstlisting}
\vspace{.3cm}
The class provides some basic constructors and related stuff (copy, clear)
\vspace{.1cm}
\begin{lstlisting}
LcOperatorOnUnknown() {};
LcOperatorOnUnknown(const OperatorOnUnknown&, const Real& = 1.);                  
LcOperatorOnUnknown(const OperatorOnUnknown&, const Complex&);                
LcOperatorOnUnknown(const OperatorOnUnknown&, Domain&, const Real& = 1.);  
LcOperatorOnUnknown(const OperatorOnUnknown&, Domain&, const Complex&);    
LcOperatorOnUnknown(const Unknown&, const Real& = 1.);                          
LcOperatorOnUnknown(const Unknown&, const Complex&);                      
LcOperatorOnUnknown(const Unknown&, Domain&, const Real& = 1.);       
LcOperatorOnUnknown(const Unknown&, Domain&, const Complex&);              
LcOperatorOnUnknown(const LcOperatorOnUnknown&);                  
~LcOperatorOnUnknown();                                            
LcOperatorOnUnknown& operator =(const LcOperatorOnUnknown&);       
void clear();                                                      
void copy(const LcOperatorOnUnknown& lc);  
\end{lstlisting}
\vspace{.3cm}
To insert new item in the list, there are insertion member functions and overloaded operators:
\vspace{.1cm}
\begin{lstlisting}
void insert(const OperatorOnUnknown&, GeomDomain* =0);                 // insert op(u) 
void insert(const Real&, const OperatorOnUnknown&, GeomDomain* =0);    // insert a*op(u) 
void insert(const Complex&, const OperatorOnUnknown&, GeomDomain* =0); // insert a*op(u) 
void insert(const Unknown&, GeomDomain* =0);                           // insert u on D 
void insert(const Real&, const Unknown&, GeomDomain* =0);              // insert a*u on D 
void insert(const Complex&, const Unknown&, GeomDomain* =0);           // insert a*u on D 
LcOperatorOnUnknown& operator+=(const OperatorOnUnknown&);            // lcop += opu
LcOperatorOnUnknown& operator+=(const Unknown&);              // lcop += u
LcOperatorOnUnknown& operator+=(const LcOperatorOnUnknown&);  // lcop += lcop
LcOperatorOnUnknown& operator-=(const OperatorOnUnknown&);    // lcop -= opu
LcOperatorOnUnknown& operator-=(const Unknown&);              // lcop -= u
LcOperatorOnUnknown& operator-=(const LcOperatorOnUnknown&);  // lcop -= lcop
LcOperatorOnUnknown& operator*=(const Real&);                 // lcop *= r
LcOperatorOnUnknown& operator*=(const Complex&);              // lcop *= c
LcOperatorOnUnknown& operator/=(const Real&);                 // lcop /= r
LcOperatorOnUnknown& operator/=(const Complex&);              // lcop /= c
\end{lstlisting}
\vspace{.3cm}
External operators may also be used:
\vspace{.1cm}
\begin{lstlisting}
LcOperatorOnUnknown operator+(const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator-(const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator+(const LcOperatorOnUnknown&, const OperatorOnUnknown&);
LcOperatorOnUnknown operator-(const LcOperatorOnUnknown&, const OperatorOnUnknown&);
LcOperatorOnUnknown operator+(const OperatorOnUnknown&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator-(const OperatorOnUnknown&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator+(const LcOperatorOnUnknown&, const Unknown&);
LcOperatorOnUnknown operator-(const LcOperatorOnUnknown&, const Unknown&);
LcOperatorOnUnknown operator+(const Unknown&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator-(const Unknown&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator+(const LcOperatorOnUnknown&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator-(const LcOperatorOnUnknown&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator*(const LcOperatorOnUnknown&, const Real&);
LcOperatorOnUnknown operator*(const LcOperatorOnUnknown&, const Complex&);
LcOperatorOnUnknown operator*(const Real&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator*(const Complex&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator/(const LcOperatorOnUnknown&, const Real&);
LcOperatorOnUnknown operator/(const LcOperatorOnUnknown&, const Complex&);
LcOperatorOnUnknown operator+(const OperatorOnUnknown&, const OperatorOnUnknown&);
LcOperatorOnUnknown operator-(const OperatorOnUnknown&, const OperatorOnUnknown&);
LcOperatorOnUnknown operator+(const OperatorOnUnknown&, const Unknown&);
LcOperatorOnUnknown operator-(const OperatorOnUnknown&, const Unknown&);
LcOperatorOnUnknown operator+(const Unknown&, const OperatorOnUnknown&);
LcOperatorOnUnknown operator-(const Unknown&, const OperatorOnUnknown&);
LcOperatorOnUnknown operator+(const Unknown&, const Unknown&);
LcOperatorOnUnknown operator-(const Unknown&, const Unknown&);
\end{lstlisting}
\vspace{.3cm}
\class{GeomDomain} may be affected using \cmd{setDomain} or | operator:
\vspace{.1cm}
\begin{lstlisting}
void setDomain(GeomDomain&);                        
LcOperatorOnUnknown& operator|(GeomDomain&);       
LcOperatorOnUnknown operator|(const Unknown&,GeomDomain&);
LcOperatorOnUnknown operator|(const OperatorOnUnknown&,GeomDomain&);
\end{lstlisting}
\vspace{.1cm}
Note that when setting the domain, all previous domain definitions are overwritten.\\

It provides the following accessors and property accessors:
\vspace{.1cm}
\begin{lstlisting}
bool isSingleUnknown() const;                             
const Unknown* unknown() const;                          
std::set<const Unknown*> unknowns() const;                
bool withDomains() const;                                
bool isSingleDomain() const;                             
Domain* domain() const;                             
std::set<Domain*> domains() const;                  
Complex coefficient() const;                             
std::vector<Complex> coefficients() const;                
std::set<DiffOpType> diffOperators() const;              
void print(std::ostream&, bool withdomain=true) const;
friend std::ostream& operator<<(std::ostream&, const LcOperatorOnUnknown&);
\end{lstlisting}
\vspace{.3cm}
To create essential condition from  \class{LcOperatorOnUnknown}, operator = is overloaded as following:
\begin{lstlisting}
EssentialCondition operator=(const Real &);                   
EssentialCondition operator=(const Complex &);               
EssentialCondition operator=(const Function &);              
\end{lstlisting}
\vspace{.3cm}
These functions are implemented in \emph{EssentialCondition.cpp}!   

\displayInfos{library=operator, header=LcOperatorOnUnknowns.hpp, implementation=LcOperatorOnUnknowns.cpp,
test=test\_EssentialCondition.cpp, header dep={OperatorOnUnknown.hpp, config.h, utils.h}}
