%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{LcOperatorOnUnknowns} class}

The\class{LcOperatorOnUnknowns} class deals with linear combination of \class{OperatorOnUnknowns} objects:
\[
\sum_{i=1,n} c_i\ op(u_i)\ O\ op(v_i) \]
where \(c_i\) is a complex coefficient, \(op(u_i)\) an operator on unknown \(u_i\)
(\class{OperatorOnUnknown} object), \(O\) an \class{algebraic operator} and \(op(v_i)\) an operator on test function \(v_i\)
(\class{OperatorOnUnknown} object).
\class{LcOperatorOnUnknowns} objects are useful to describe linear and bilinear
form (see \lib{form} library):
\[
\int_\Omega \sum_{i=1,n} c_i\ op(u_i)\ O\ op(v_i).\]

The\class{LcOperatorOnUnknowns} class inherits from \class{std::vector} class:
\vspace{.1cm}
\begin{lstlisting}
typedef std::pair<OperatorOnUnknowns *, complex_t> OpusValPair;

class LcOperatorOnUnknowns : public std::vector<OpusValPair>
\end{lstlisting}
\vspace{.3cm}
The class provides some basic constructors and related stuff (copy, clear)
\vspace{.1cm}
\begin{lstlisting}
    LcOperatorOnUnknowns() {}
    LcOperatorOnUnknowns(const OperatorOnUnknowns &, const real_t & = 1.);
    LcOperatorOnUnknowns(const OperatorOnUnknowns &, const complex_t &);
    LcOperatorOnUnknowns(const LcOperatorOnUnknowns &);
    ~LcOperatorOnUnknowns();  
    LcOperatorOnUnknowns &operator=(const LcOperatorOnUnknowns &);
    void clear();
    void copy(const LcOperatorOnUnknowns &lc);  
\end{lstlisting}
\vspace{.3cm}
To insert new item in the list, there are insertion member functions and overloaded operators:
\vspace{.1cm}
\begin{lstlisting}
void insert(const OperatorOnUnknowns &);                    // insert opuv
void insert(const real_t &, const OperatorOnUnknowns &);    // insert a*opuv
void insert(const complex_t &, const OperatorOnUnknowns &); // insert
a*opuv
LcOperatorOnUnknowns &operator+=(const OperatorOnUnknowns &);   // lcopuv += opuv
LcOperatorOnUnknowns &operator+=(const LcOperatorOnUnknowns &); // lcopuv += lcopuv
LcOperatorOnUnknowns &operator-=(const OperatorOnUnknowns &);   // lcopuv -= opuv
LcOperatorOnUnknowns &operator-=(const LcOperatorOnUnknowns &); // lcopuv -= lcopuv
LcOperatorOnUnknowns &operator*=(const real_t &);               // lcopuv *= r
LcOperatorOnUnknowns &operator*=(const complex_t &);            // lcopuv *= c
LcOperatorOnUnknowns &operator/=(const real_t &);               // lcopuv /= r
LcOperatorOnUnknowns &operator/=(const complex_t &);            // lcopuv /= c
\end{lstlisting}
\vspace{.3cm}
External operators may also be used:
\vspace{.1cm}
\begin{lstlisting}
// OperatoronUnknowns and OperatorOnUnknowns
LcOperatorOnUnknowns operator+(const OperatorOnUnknowns &, const OperatorOnUnknowns &);
LcOperatorOnUnknowns operator-(const OperatorOnUnknowns &, const OperatorOnUnknowns &);

// LcOperatorOnUnknowns and LcOperatorOnUnknowns
LcOperatorOnUnknowns operator+(const LcOperatorOnUnknowns &);
LcOperatorOnUnknowns operator-(const LcOperatorOnUnknowns &);
LcOperatorOnUnknowns operator+(const LcOperatorOnUnknowns &, const LcOperatorOnUnknowns &);
LcOperatorOnUnknowns operator-(const LcOperatorOnUnknowns &, const LcOperatorOnUnknowns &);

// LcOperatorOnUnknowns and OperatorOnUnknowns
LcOperatorOnUnknowns operator+(const LcOperatorOnUnknowns &, const OperatorOnUnknowns &);
LcOperatorOnUnknowns operator-(const LcOperatorOnUnknowns &, const OperatorOnUnknowns &);
LcOperatorOnUnknowns operator+(const OperatorOnUnknowns &, const LcOperatorOnUnknowns &);
LcOperatorOnUnknowns operator-(const OperatorOnUnknowns &, const LcOperatorOnUnknowns &);

// LcOperatorOnUnknowns and scalar
LcOperatorOnUnknowns operator*(const LcOperatorOnUnknowns &, const real_t &);
LcOperatorOnUnknowns operator*(const LcOperatorOnUnknowns &, const complex_t &);
LcOperatorOnUnknowns operator*(const real_t &, const LcOperatorOnUnknowns &);
LcOperatorOnUnknowns operator*(const complex_t &, const LcOperatorOnUnknowns &);
LcOperatorOnUnknowns operator/(const LcOperatorOnUnknowns &, const real_t &);
LcOperatorOnUnknowns operator/(const LcOperatorOnUnknowns &, const complex_t &);

// LcOperatorOnUnknown and OperatorOnUnkown
LcOperatorOnUnknowns operator*(const LcOperatorOnUnknown &, const OperatorOnUnknown &);
LcOperatorOnUnknowns operator*(const OperatorOnUnknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator%(const LcOperatorOnUnknown &, const OperatorOnUnknown &);
LcOperatorOnUnknowns operator%(const OperatorOnUnknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator|(const LcOperatorOnUnknown &, const OperatorOnUnknown &);
LcOperatorOnUnknowns operator|(const OperatorOnUnknown &, const LcOperatorOnUnknown &);

// LcOperatorOnUnknown and Unkown
LcOperatorOnUnknowns operator*(const LcOperatorOnUnknown &, const Unknown &);
LcOperatorOnUnknowns operator*(const Unknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator%(const LcOperatorOnUnknown &, const Unknown &);
LcOperatorOnUnknowns operator%(const Unknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator|(const LcOperatorOnUnknown &, const Unknown &);
LcOperatorOnUnknowns operator|(const Unknown &, const LcOperatorOnUnknown &);

// LcOperatorOnUnknown and LcOperatorOnUnkown
LcOperatorOnUnknowns operator*(const LcOperatorOnUnknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator%(const LcOperatorOnUnknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator|(const LcOperatorOnUnknown &, const LcOperatorOnUnknown &);
\end{lstlisting}

It provides the following accessors and property accessors:
\vspace{.1cm}
\begin{lstlisting}
    bool isSingleUVPair() const;                        // true if all terms involve the same unknown
    const OperatorOnUnknown *opu(number_t i = 1) const; // return ith left OperatorOnUnknown involved in LcOperator's
    const OperatorOnUnknown *opv(number_t i = 1) const; // return ith right OperatorOnUnknown involved in LcOperator's
    const Unknown *unknownu(number_t i = 1) const;      // return ith left Unknown involved in LcOperator
    const Unknown *unknownv(number_t i = 1) const;      // return ith right
    Unknown involved in LcOperator
    complex_t coefficient(number_t i = 1) const; // return ith coefficient
    std::vector<complex_t> coefficients() const; // return vector of coefficients involved in combination
  void print(std::ostream &) const;                                
  friend std::ostream &operator<<(std::ostream &, const LcOperatorOnUnknowns &);
\end{lstlisting}  

\displayInfos{library=operator, header=LcOperatorOnUnknowns.hpp, implementation=LcOperatorOnUnknowns.cpp,
test=unit\_LcOperatorOnUnknowns.cpp, header dep={LcOperatorOnUnknown.hpp,
OperatorOnUnknown.hpp, config.h, utils.h}}
