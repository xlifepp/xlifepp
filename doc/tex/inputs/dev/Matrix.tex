%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{Matrix} class}

The purpose of the \class{Matrix} class is to deal with complex or real dense matrices. It
is derived from the \emph{std:vector<K>} and templated by the coefficient type; the coefficients
are stored row by row.
\vspace{.2cm}
\begin{lstlisting}
template<typename K>
class Matrix : public std::vector<K>
{private:
   dimen_t rows_;      //!<number of rows 
 public:
   typedef K type_t;   //!<alias on matrix coefficient type
   typedef typename std::vector<K>::iterator itvk_t;
   typedef typename std::vector<K>::const_iterator citvk_t;
   \ldots
\end{lstlisting}
\vspace{.2cm}
As it is templated, it is also possible to deal with matrix of matrices. But all functions
are not supported, in particular those involving autocast.\\

It offers some basic constructors and assignment function:
\vspace{.2cm}
\begin{lstlisting}
Matrix();                                         //by default
Matrix(const dimen_t, const dimen_t);             //by dimensions
Matrix(const dimen_t r, const dimen_t, const K&); //by dimensions and constant 
Matrix(const Matrix<K>&);                         //by copy
Matrix(const vector<K>&);                         //diagonal square matrix from vector
Matrix(const vector<vector<K> >&);                //from a vector of vectors
Matrix(const vector<Vector<K> >&);                //from a vector of Vectors
Matrix(const dimen_t, const SpecialMatrix);       //for special square matrix 
Matrix(const String&);                            //from file
Matrix(const char *);                             //from file
\end{lstlisting}
\vspace{.2cm}
As the \class{Vector} class inherited from \class{std::vector}, the constructor from {\class
std::vector} of \class{std::vector} works also for \class{Vector} of \class{std::vector},
for \class{std::vector} of \class{ Vector} and for \class{Vector} of \class{Vector}. The
special matrices are enumerated by the \emph{enum}:
\vspace{.2cm}
\begin{lstlisting}
enum SpecialMatrix{ _zeroMatrix=0, _idMatrix, _onesMatrix, _hilbertMatrix }; 
\end{lstlisting}
\vspace{.2cm}
Besides the constructors, the following assignment operators are allowed:
\vspace{.2cm}
\begin{lstlisting}
Matrix<K>& operator=(const Matrix<K>&); 
Matrix<K>& operator=(const Matrix<KK>&);
Matrix<K>& operator=(const vector<vector<K> >&);
Matrix<K>& operator=(const vector<Vector<K> >&);
private: void assign(const KK&);
\end{lstlisting}
Be cautious with the generalized templated version \lstinline{Matrix<K>\& operator=(const Matrix<KK>\&)}.\\

One can access to the dimensions, to the coefficients (both read and read/write access) either
by iterators (\cmd{begin} and \cmd{end}) or by index from 1 to the length, to the rows
or the columns (both read and read/write access), to the main diagonal:
\vspace{.2cm}
\begin{lstlisting}
size_t  size() const;              //overloaded size()
size_t  vsize() const;             //column length (number of rows)
dimen_t numberOfRows() const;      //number of rows
size_t hsize() const;              //row length (number of columns)
dimen_t numberOfColumns() const;   //number of columns
itvk_t  begin();                   //overloaded iterator begin
citvk_t begin() const;             //overloaded const_iterator begin
itvk_t  end();                     //overloaded iterator end
citvk_t end() const;               //overloaded const_iterator begin
const K& operator()(const dimen_t, const dimen_t) const;//protected access  
K& operator()(const dimen_t, const dimen_t);            //read/write access 
Vector<K> row(const dimen_t) const;           //return r-th row (r > 0) as Vector
void row(const dimen_t, const vector<K>&):    //set r-th row from a stl vector
Vector<K> column(const dimen_t) const;        //return c-th  column as a Vector
void column(const dimen_t, const vector<K>&); //set c-th column from a stl vector
Vector<K> diag() const;                       //return diagonal as a Vector
Matrix<K>& diag(const K);                     //reset matrix as a diag. matrix 
Matrix<K>& diag(constvector<K>&);             //reset matrix as a diag. matrix
\end{lstlisting}
\vspace{.2cm}
Be care with functions on the diagonal, they change the current matrix to a diagonal matrix.\\

There are also generalized accessors that allow to extract or set a part of matrix. The row indices and column indices may be given either by lower and upper indices or by vector of indices:
\vspace{.2cm}
\begin{lstlisting}
Matrix<K> get(const std::vector<Dimen>&, const std::vector<Dimen>&) const;
Matrix<K> get(const std::pair<Dimen,Dimen>&, const std::pair<Dimen, Dimen>&) const;
Matrix<K> get(Dimen, const std::pair<Dimen, Dimen>&) const;
Matrix<K> get(const std::pair<Dimen,Dimen>&, Dimen) const;
Matrix<K> get(Dimen, const std::vector<Dimen>&) const;
Matrix<K> get(const std::vector<Dimen>&, Dimen) const;
Matrix<K> get(Dimen, Dimen, Dimen, Dimen) const;
Matrix<K> operator()(const std::vector<Dimen>&, const std::vector<Dimen>&) const;
Matrix<K> operator()(const std::pair<Dimen,Dimen>&, const std::pair<Dimen, Dimen>&) const;
Matrix<K> operator()(Dimen, const std::pair<Dimen, Dimen>&) const;
Matrix<K> operator()(const std::pair<Dimen,Dimen>&, Dimen) const;
Matrix<K> operator()(Dimen, const std::vector<Dimen>&) const;
Matrix<K> operator()(const std::vector<Dimen>&, Dimen) const;
Matrix<K> operator()(Dimen, Dimen, Dimen, Dimen) const;
void set(const std::vector<Dimen>&, const std::vector<Dimen>&, const std::vector<K>&);
void set(const std::pair<Dimen,Dimen>&, const std::pair<Dimen, Dimen>&, const std::vector<K>&);
void set(Dimen, const std::pair<Dimen, Dimen>&, const std::vector<K>&);
void set(const std::pair<Dimen,Dimen>&, Dimen, const std::vector<K>&);
void set(Dimen, const std::vector<Dimen>&, const std::vector<K>&);
void set(const std::vector<Dimen>&, Dimen, const std::vector<K>&);
void set(Dimen, Dimen, Dimen, Dimen, const std::vector<K>&);
\end{lstlisting}
\vspace{.2cm}

Through overloaded operators, the \class{Matrix} class provides the following (internal) algebraic
operations:
\vspace{.2cm}
\begin{lstlisting}
Matrix<K>& operator+=(const Matrix<KK>&);//add a matrix to the matrix 
Matrix<K>& operator+=(const K&);         //add a scalar to the matrix 
Matrix<K>& operator-=(const Matrix<KK>&);//substract a matrix to the matrix 
Matrix<K>& operator-=(const K&);         //substract a scalar to the matrix 
Matrix<K>& operator*=(const KK&);        //multiply by a scalar the matrix
Matrix<K>& operator/=(const KK&);        //divide by a scalar the matrix
\end{lstlisting}
\vspace{.2cm}
Be careful, the operations such as \lstinline{Matrix<Matrix<K> > += Matrix<K>} does not work.\\

Moreover, the \class{Matrix} class provide a lot of functionalities:
\vspace{.2cm}
\begin{lstlisting}
bool shareDims(const Matrix<KK>&) const; //true if matrices bear same dimensions
bool isSymmetric() const;                //symmetric matrix test
bool isSkewSymmetric() const;            //skew-symmetric matrix test
bool isSelfAdjoint() const;              //self adjoint matrix test
bool isSkewAdjoint() const;              //skew adjoint matrix test
Matrix<K>& transpose();                  //matrix self-transpostion
Matrix<K>& adjoint();                    //matrix self-adjoint
Matrix<Real> real() const;               //real part of a matrix
Matrix<Real> imag() const;               //imaginary part of a matrix
\end{lstlisting}
\vspace{.2cm}
and input/output and error utilities:
\vspace{.2cm}
\begin{lstlisting}
void loadFromFile(const char*);          
void saveToFile(const char*);            
istream& operator >> (istream&, Matrix<K>&);
ostream& operator<<(ostream&, const Matrix<Real>&);    
ostream& operator<<(ostream&, const Matrix<Complex>&); 
void print(ofstream&);
void print() const; 

void nonSquare(const String&, const size_t, const size_t) const;     
void mismatchDims(const String& ,const size_t, const size_t) const; 
void divideByZero(const String&) const;
void isSingular() const; 
void complexCastWarning(const String&, const size_t, const size_t) const; 
\end{lstlisting}
\vspace{.2cm}

Finally, this class provides almost algebraic operations as external functions, with specialized
versions allowing automatic cast from real to complex type:
\begin{lstlisting}
bool operator==(const Matrix<K>&,const Matrix<K>&);     //matrix comparison 
bool operator!=(const Matrix<K>&,const Matrix<K>&);     //matrix comparison 

Matrix<K> operator+(const Matrix<K> &);                 //unary operator+
Matrix<K> operator-(const Matrix<K> &);                 //unary operator-

Matrix<K> operator+(const Matrix<K>&, const Matrix<K>&);//matrix + matrix
Matrix<K> operator+(const KK&, const Matrix<K>&);       //scalar + matrix
Matrix<K> operator+(const Matrix<K>&, const KK&)        //matrix + scalar 
Matrix<Complex> operator+(const Complex&, const Matrix<Real>&);         
Matrix<Complex> operator+(const Matrix<Real>&, const Complex&);         
Matrix<Complex> operator+(const Matrix<Real>&, const Matrix<Complex>&); 
Matrix<Complex> operator+(const Matrix<Complex>&, const Matrix<Real>&); 

Matrix<K> operator-(const Matrix<K>&, const Matrix<K>&);  //matrix - matrix
Matrix<K> operator-(const KK&, const Matrix<K>&);         //scalar - matrix
Matrix<K> operator-(const Matrix<K>&, const KK&)          //matrix - scalar 
Matrix<Complex> operator-(const Complex&, const Matrix<Real>&);        
Matrix<Complex> operator-(const Matrix<Real>&, const Complex&);       
Matrix<Complex> operator-(const Matrix<Real>&, const Matrix<Complex>&);
Matrix<Complex> operator-(const Matrix<Complex>&, const Matrix<Real>&); 

Matrix<K> operator*(const KK&, const Matrix<K>&);      //scalar * matrix
Matrix<K> operator*(const Matrix<K>& ,const KK&);      //matrix * scalar
Matrix<Complex> operator*(const Complex&, const Matrix<Real>&); 
Matrix<Complex> operator*(const Matrix<Real>&, const Complex&); 
Matrix<K> operator/(const Matrix<K>&, const KK&);      //matrix / scalar
Matrix<Complex> operator/(const Matrix<Real>&, const Complex&); 

Vector<K> operator*(const Matrix<K>&, const Vector<V>&);  //matrix * vector
Vector<Complex> operator*(const Matrix<Real>&, const Vector<Complex>&); 
Vector<Complex> operator*(const Matrix<Complex>&, const Vector<Real>&); 

Matrix<K> operator*(const Matrix<K>&, const Matrix<K>&);  //matrix * matrix
Matrix<Complex> operator*(const Matrix<Real>&, const Matrix<Complex>&); 
Matrix<Complex> operator*(const Matrix<Complex>&, const Matrix<Real>&); 

Matrix<K> transpose(const Matrix<K>&);       //transpose matrix
void diag(Matrix<K>&);                       //change matrix into its diagonal 
Matrix<Real> real(const Matrix<Complex>&);   //real part 
Matrix<Real> imag(const Matrix<Complex>&);   //imaginary part
Matrix<Real> conj(const Matrix<Real>&);      //conjugate complex matrix
Matrix<Complex> cmplx(const Matrix<Real>&);  //cast a real matrix to complex one
Matrix<Complex> conj(const Matrix<Complex>&);//conjugate complex matrix
\end{lstlisting}
\vspace{.2cm}
Some of algebraic operations are based on general template functions using iterators on vector
or matrix:
\begin{lstlisting}
void transpose(const dimen_t, const dimen_t, M_it, MM_it); //transposition
void adjoint(const dimen_t, const dimen_t, M_it, MM_it);   //adjoint 
void matvec(M_it, const V1_it, const V1_it, V2_it, V2_it); //matrix * vector
V2_it matvec(const Matrix<K>&, const V1_it, const V2_it);  //matrix * vector 
void vecmat(M_it, const V1_it, const V1_it, V2_it, V2_it); //vector * matrix 
V2_it vecmat(const Matrix<K>&, const V1_it, const V2_it);  //vector * matrix 
void matmat(A_it, const dimen_t, B_it, const dimen_t, const dimen_t, R_it); //matrix * matrix
\end{lstlisting}
\vspace{.2cm}
\emph{Use these algebraic operations only with \emph{Matrix<real\_t>} and \emph{Matrix<complex\_t>}.
They do not all work for more complicated structure such that \emph{Matrix<Matrix<K> >}.}\\
\\
If the \eigen library is available (it should be, unless you can't use \omp), QR and SVD decomposition are available:
\begin{lstlisting}
void qr(const Matrix<T>&, Matrix<T>&, Matrix<T>&);
void svdMat(const Matrix<T>&, Matrix<T>&, Vector<T>&, Matrix<T>&, real_t eps=0);
\end{lstlisting}
\vspace{.2cm}

Useful aliases hiding template syntax, are available for end users:
\begin{lstlisting}
typedef Matrix<Real> RealMatrix,
typedef Matrix<Complex> ComplexMatrix;
typedef Matrix<Matrix<Real> > RealMatrices;
typedef Matrix<Matrix<Complex> > ComplexMatrices;
\end{lstlisting}
They are defined in \emph{users\_typedef.hpp} (\lib{init} library).

\displayInfos{library=utils, header=Matrix.hpp, implementation=Matrix.cpp, test=test\_Matrix.cpp,
header dep={config.h, Vector.hpp, Trace.hpp, Messages.hpp, String.hpp, Algorithms.hpp, complexUtils.hpp}}

\vspace{5mm}
