%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex


\section{License}

\xlifepp is copyright (C) 2010-2022 by E. Lunéville and N. Kielbasiewicz and is distributed under the terms of the GNU General Public License (GPL) (Version 3 or later, see https://www.gnu.org/licenses/gpl-3.0.en.html). This means that everyone is free to use \xlifepp and to redistribute it on a free basis. \xlifepp is not in the public domain; it is copyrighted and there are restrictions on its distribution. You cannot integrate \xlifepp (in full or in parts) in any closed-source software you plan to distribute (commercially or not). If you want to integrate parts of \xlifepp into a closed-source software, or want to sell a modified closed-source version of \xlifepp, you will need to obtain a different license. Please contact us directly for more information.\\

The developers do not assume any responsibility in the numerical results obtained using the \xlifepp library and are not responsible for bugs. 

\section{Credits}

The \xlifepp library has been mainly developed by E. Lunéville and N. Kielbasiewicz of \poems lab (UMR 7231, CNRS-ENSTA Paris-INRIA). Some parts are inherited from \melinapp library developed by D. Martin (\irmar lab, Rennes University, now retired) and E. Lunéville. Other contributors are:
\begin{itemize}
\item Y. Lafranche (\irmar lab), mesh tools using subdivision algorithms, wrapper to \arpack
\item C. Chambeyron (\poems lab), iterative solvers, unitary tests, PhD students' support
\item M.H. N'Guyen (\poems lab), eigen solvers and OpenMP implementation
\item N. Salles (\poems lab), boundary element methods
\item L. Pesudo (\poems lab), boundary element methods and HF coupling
\item P. Navaro (\irmar lab), continuous integration
\item E. Darrigrand-Lacarrieu (\irmar lab), fast multipole methods
\item E. Peillon (\poems lab), evolution of (bi)linear forms
\end{itemize}

\section{Installation}

\xlifepp is under an open git repository on the INRIA GitLab instance. So developer access to source code needs to log to the GitLab and to define an ssh-key for authentication. 
The first part is to be registered to INRIA GitLab and to be accepted as member of \xlifepp project. To do so, please do as follows:

\begin{enumerate}
\item Please go to \url{https://GitLab.inria.fr/xlifepp/xlifepp/}.
\item If you have already had a user account, please go to step \#4
\item If you don't have a user account, please create it.
\item If you have already been member of the \xlifepp project on INRIA GitLab, please go to step \#6
\item If you are not member of the \xlifepp project on INRIA GitLab, please contact one of the \xlifepp administrators to join the \xlifepp development team and wait until your registration to \xlifepp project on INRIA GitLab is done to go to next section.
\item For further documentation, you may go to the \xlifepp main page:

\centering\url{http://uma.ensta-paris.fr/soft/XLiFE++/}
\end{enumerate}

\subsection{On macOS and Linux}

To install \xlifepp in order to take part to its development, you may use the following steps:

\begin{enumerate}
\item Please install \git (\url{http://www.git-scm.com}) and \cmake (\url{http://cmake.org}).
\item If you have already generated an ssh key and want to use it for \xlifepp project, please go to step \#4
\item If you don't have generated an ssh key, please use the command {\ttfamily ssh-keygen} to generate one.
\item To add the public ssh key you want to use, please go to your account settings on INRIA GitLab and add the key by clicking on ``SSH keys'' at the bottom of the left menu. Copy the public key where it is needed, validate and register changes to your account. This may take some time so that the key is truly activated.
\item On \url{https://gitlab.inria.fr/xlifepp/xlifepp/}, you see the {\ttfamily git clone \ldots} command-line you have to run in a terminal to create your local \xlifepp repository.
\item You can now compile and develop. See next section.
\end{enumerate}

\subsection{Under Windows}

To install \xlifepp in order to take part to its development, you may use the following steps:

\begin{enumerate}
\item Please install \git (\url{http://www.git-scm.com}) and \cmake (\url{http://cmake.org}). On Windows, git is usable with the contextual menu showed with clicks on the right mouse button.
\item Please open Git GUI in the contextual menu.
\item Please click on ``Cloning existing repository''. A window opens.
\item Please open the ``Help'' tab and select ``Show SSH key''
\item Click on ``Generate key''. The public key will be shown. Copy it.
\item To add the public ssh key, please go to your account settings on INRIA GitLab and add the key by clicking on ``SSH keys'' at the bottom of the left menu. Copy the public key where it is needed, validate and register changes to your account. This may take some time so that the key is truly activated.
\item To set the ``Source location'' in the Git GUI window, please go to \url{https://gitlab.inria.fr/xlifepp/xlifepp/} to see the {\ttfamily git clone \ldots} command-line. Copy the url related to "Clone with SSH" (or "Clone with HTTPS"). Define a non-existing directory for the ``Target' Directory'. And click on ``Clone'' to create your local repository.
\item You can now compile and develop. See next section.
\end{enumerate}

\section{Compilation}

Compilation of \xlifepp is managed by \cmake, a high-level cross-platform compilation tool, running on macOS, Windows, and Linux. This application only needs a configuration file named CMakeLists.txt, at the root directory of the \xlifepp archive. Whatever the OS, \cmake also asks for another directory where to put generated files for compilation, called build directory hereafter. This directory can be everywhere. It will contain compilation files (objects files, \ldots), a Makefile or an IDE project file named XLiFE++ (for Eclipse, CodeBlocks, Visual Studio, XCode, \ldots). So we suggest you to set this directory as a subdirectory of \xlifepp install directory, with the name "build" for instance.

\subsection{Under macOS and Linux}

\begin{figure}[H]
\begin{center}
\includePict[width=10cm]{cmake-ccmake.png}
\end{center}
\vspace{-1cm} 
\caption{{\ttfamily ccmake} (macOS, Unix)}
\end{figure}

When running {\ttfamily ccmake}, the default \cmake GUI under macOS or Linux, the build directory is given in argument.
\begin{lstlisting}{language=}
ccmake [options] path/to/CMakeLists.txt path/to/buildDirectory
\end{lstlisting}

When running \cmake GUI application, you have to set the directory ``Where is the source code'' containing CMakeLists.txt you want to run \cmake on, and to set the build directory: ``Where to build the binaries''. Then, you click the ``Configure'' button. It will ask you the generator you want. Then, you click the ``Generate'' button, to generate your IDE project file or your Makefile.

\begin{figure}[H]
\begin{center}
  \includePict[width=6cm]{cmake-macOS.png}
  \includePict[width=6cm]{cmake-Windows.png}
\end{center}
\vspace{-1cm} 
\caption{\cmake application (macOS on the left and Windows on the right)}
\end{figure}

When running \cmake in command-line mode, the build directory is generally the directory in which you are when calling the \cmake command. If you want to know the general case, please take a look at \cmake option -b.

To compile \xlifepp, you just have to run \cmake on the CMakeLists.txt file:
\begin{lstlisting}{language=}
cmake path/to/CMakeLists.txt
\end{lstlisting}
If you generated a project file, you launch your IDE on this file and can compile and run tests. The following command chooses for instance to use codeblocks on Unix platform:
\begin{lstlisting}{language=}
cmake path/to/CMakeLists.txt -G "CodeBlocks - Unix Makefiles"
\end{lstlisting}

If you generated a Makefile, you have to run the {\ttfamily make} command, to compile \xlifepp sources and tests. If you want to compile \xlifepp sources only, the target is "libs":

\begin{lstlisting}
make libs
make
\end{lstlisting}

If you generated an IDE project file, you have to open it and compile target all to compile everything, sources and tests, or target libs, to compile sources only.

\subsection{Under Windows}

When running \cmake GUI application, you have to set the directory ``Where is the source code'' containing CMakeLists.txt you want to run \cmake on, and to set the build directory: ``Where to build the binaries''. Then, you click the ``Configure'' button. It will ask you the generator you want. Then, you click the ``Generate'' button, to generate your IDE project file or your Makefile.

\begin{figure}[H]
\begin{center}
\includePict[width=15cm]{cmake-Windows.png}
\end{center}
\vspace{-0.5cm}
\caption{{\ttfamily cmake} (Windows), after you clicked the configure button.}
\end{figure}

\begin{ideabox}
You can have the list of available generators inside \cmake help. It does not tell that eclipse, codeblocks, \ldots are installed, but it tells that they can be installed on your computer and how you can tell \cmake to use them as generators.
\end{ideabox}

\subsection{Useful general options to \cmake}

Four options may be very useful:
\begin{description}
\item[\cmakeopt{CMAKE\_CXX\_COMPILER}] This option is used to set the compiler \cmake will use to compile your code:
\begin{lstlisting}
cmake path/to/CMakeLists.txt -DCMAKE_CXX_COMPILER=g++-48
\end{lstlisting}
\item[\cmakeopt{CMAKE\_BUILD\_TYPE}] This option is used to set the type of build (debug, release, \ldots). Possible values are essentially Debug and Release. The default value is Release.
\begin{lstlisting}
cmake path/to/CMakeLists.txt -DCMAKE_BUILD_TYPE=Debug
\end{lstlisting}
\item[\cmakeopt{XLIFEPP\_PRECISION}] This option is used to set the precision of numerical types (int, \ldots). Possible values are STD, LONG and LONGLONG. The default value is LONG.
\begin{lstlisting}
cmake path/to/CMakeLists.txt -DXLIFEPP_PRECISION=LONG
\end{lstlisting}
Except if you have a very good reason, you are not supposed to use this option.
\item[\cmakeopt{XLIFEPP\_STRING\_TYPE}] This option is used to set string type: STD (for std::string) and WIDE (for std::wstring). The default value is STD.
\begin{lstlisting}
cmake path/to/CMakeLists.txt -DXLIFEPP_STRING_TYPE=STD
\end{lstlisting}
Except if you have a very good reason, you are not supposed to use this option.
\end{description}

\subsection{Configuring \xlifepp with \gmsh and/or \paraview}

\begin{description}
\item[\cmakeopt{XLIFEPP\_GMSH\_EXECUTABLE}] To specify the \gmsh binary with full path. If \gmsh is in your PATH, it will be automatically detected, else you can use this option:
\begin{lstlisting}
cmake path/to/CMakeLists.txt -DXLIFEPP_GMSH_EXECUTABLE=gmsh/exe/with/full/path
\end{lstlisting}
\item[\cmakeopt{XLIFEPP\_PARAVIEW\_EXECUTABLE}] To specify the \paraview binary with full path. If \paraview is in your PATH, it will be automatically detected, else you can use this option:
\begin{lstlisting}
cmake path/to/CMakeLists.txt -DXLIFEPP_PARAVIEW_EXECUTABLE=paraview/exe/with/full/path
\end{lstlisting}
\end{description}

\subsection{Configuring \xlifepp with \omp}

\begin{description}
\item[\cmakeopt{XLIFEPP\_ENABLE\_OMP}] Activates/Deactivates use of OpenMP 
\begin{lstlisting}
cmake path/to/CMakeLists.txt -DXLIFEPP_ENABLE_OMP=ON
\end{lstlisting}
\end{description}

\subsection{Configuring \xlifepp with \arpack}

Configuring with \arpack also means configuring with \blas and \lapack. When asked, these 3 libraries are automatically searched in standard paths. If these libraries are installed in other paths, you can give it directly.
Otherwise, sources files of \arpack inside \xlifepp sources will be compiled and used.

\begin{description}
\item[\cmakeopt{XLIFEPP\_ENABLE\_ARPACK}] Activates/Deactivates configuration with \arpack. Default is OFF.
\item[\cmakeopt{XLIFEPP\_SYSTEM\_ARPACK}] To use a system distribution of \arpack (ON) or the provided one (OFF). Default is OFF.
\item[\cmakeopt{XLIFEPP\_BLAS\_LIB\_DIR}] To specify the directory containing \blas library
\item[\cmakeopt{XLIFEPP\_LAPACK\_LIB\_DIR}] To specify the directory containing \lapack library
\item[\cmakeopt{XLIFEPP\_ARPACK\_LIB\_DIR}] To specify the directory containing \arpack library
\end{description}

See \autoref{a.s.arpackpp} for more details

\subsection{Configuring \xlifepp with \umfpack}

Configuring with \umfpack also means configuring with \blas and \lapack. It can also mean configuring with other libraries provided by \suitesparse, insofar as \umfpack is now distributed inside \suitesparse.

\begin{description}
\item[\cmakeopt{XLIFEPP\_ENABLE\_UMFPACK}] Activates/Deactivates configuration with \umfpack. Default is OFF.
\item[\cmakeopt{XLIFEPP\_BLAS\_LIB\_DIR}] To specify the directory containing \blas library
\item[\cmakeopt{XLIFEPP\_LAPACK\_LIB\_DIR}] To specify the directory containing \lapack library
\item[\cmakeopt{XLIFEPP\_UMFPACK\_INCLUDE\_DIR}] To specify the directory containing \umfpack header
\item[\cmakeopt{XLIFEPP\_UMFPACK\_LIB\_DIR}] To specify the directory containing \umfpack header
\item[\cmakeopt{XLIFEPP\_SUITESPARSE\_HOME\_DIR}] To specify the home directory of \suitesparse, including \umfpack. This option is to be used if you compiled \suitesparse by yourself.
In this case, \umfpack will be searched in the UMFPACK subdirectory.
\end{description}

There are specific options to each library provided by \suitesparse \umfpack may depend on. See \autoref{a.s.umfpack} for more details.

\begin{focusbox}
Using \cmake generates a cache file (CMakeCache.txt) containing value of every option defined during execution of \cmake, so that when reusing \cmake on the same file, it reuses CMakeCache.txt, unless you change some options. This is why XLIFEPP\_ENABLE\_XXX options can take the value OFF.
\end{focusbox}

\section{Folder organization}

The \xlifepp main directory is organized as follows :

\begin{description}
\item[bin] the binary directory containing the test executable and the output files for test execution
\item[CMakeLists.txt] this is the parameters file for \cmake
\item[doc] the documentation directory with the source documentation (the {\bfseries api} subdirectory), generated by \doxygen, and the TeX/PDF documentation (the {\bfseries tex} subdirectory)
\item[etc] this directory contains templates files for installation, parameters files for \doxygen, dictionaries and messages formats
\item[include] this directory contains the header aliases for each sub-library of \xlifepp and config files
\item[lib] this directory contains the static sub-libraries of \xlifepp
\item[README] the README file
\item[src] this directory contains all source and header files. Each subdirectory represents a sub-library of \xlifepp
\item[tests] this directory contains source files for unitary and non-regressive tests
\item[usr] this directory contains all files needed by the user to write a program using \xlifepp and compile it.
\end{description}

\section{Namespace}

The whole \xlifepp library is defined in the namespace \emph{\textbf{xlifepp}}. Then, the developers
have to encapsulate all their developments in this namespace either in  the header files (.hpp)
or in the implementation files (.cpp). For instance, a header file looks like :

\begin{lstlisting}
#ifndef FILE_HPP
#define FILE_HPP

#include "otherfile.hpp"
#include <sysinclude>

namespace xlifepp{
 \ldots

} //end of namespace 
#endif 
\end{lstlisting}

\section{Headers}

All the header files of the library end by \emph{.hpp} and are located in the folder related
to the topics of the header file. There are four particular header files located in the \emph{init}
folder:
\begin{itemize}
\item \textit{setup.hpp} defining the pre-compiler macro related to the install path, the operating
system, the precision type, the string type, the language and debug feature. Be cautious, changing
one of this macro implies the recompilation of all the library.
\item \textit{config.hpp} declaring all the general stuff : scalar definitions, string definition,
enumeration, numerical constants and global variables used by general tools. It includes the
header \emph{setup.hpp}. Note that this file may be (and must be) included everywhere.
\item \textit{globalScopeData.hpp} initializing global variables declared in \emph{config.hpp}.
This file has to be included once.
\end{itemize}
Some header interface files are defined in the \emph{include} folder, their name end with \emph{.h}.
The header interface files are 
\begin{itemize}
\item \textit{\emph{lib}.h} for each library folder (e.g. \emph{init.h}, \emph{utils.h}, \ldots).
This header file included all the header files (.hpp) of the library folder and is used by
other libraries that require objects of this library folder.
\item \textit{config.h} is an alias to the \emph{config.hpp} located in the \emph{init} folder.
\item \textit{globalScopeData.h} is an alias to the \emph{globalScopeData.hpp} located in the
\lib{init} folder.
\item \textit{xlife++.h} is the global header file. As this file includes the \emph{globalScopeData.hpp}
initializing all the global variables it must be included only in the main program.
\end{itemize}

\begin{figure}[H]
\centering
\inputTikZ{header}
\caption{Headers organization}
\end{figure}

From a practical point of view:

\begin{itemize}
\item When you develop a new class or utilities in the \emph{mystuff.cpp} file in the library
\lib{lib}, create the header file \emph{mystuff.hpp} and add the alias \emph{\#include "../src/lib/mystuff.hpp"}
in the \emph{lib.h} located in the \emph{include} folder;
\item Include the \emph{config.h} in your files to access to all general definitions, local
header files (.hpp) if you refer to some stuff of the library and global header files (.h)
if you refer to some stuff of other libraries;
\item When you develop a new library (\lib{mylib}), you have to create the \emph{mylib.h}
header file in the \emph{include} folder and add \emph{\#include "mylib.h" } in the \emph{xlife++.h}.
\end{itemize}

\section{Development principles}
This section gives some "philosophical" principles to develop the library \xlifepp. These
principles can be summed up in the following commandments : 
\begin{itemize}
\item \textbf{Code has to be efficient}

It means that time-consuming functions and memory consuming object has to be designed to be
the most efficient as possible. It is not required else. 
\item \textbf{Be friendly with end users}

No templated and not too much end user's classes, no pointers, friendly syntaxes, \ldots
\item \textbf{Be friendly with not advanced developers}

It means that the intermediate code has to be not too intricate.
\item \textbf{Make code documentation}

Write inline documentation along \doxygen syntax and outline documentation in Latex: the developer
documentation gives principles, useful comments and a description of classes and functions,
user documentation describes how to use the end user's classes and functions.  
\item \textbf{Write test functions}

It is mandatory to perform low level tests and to archive it (see chapter Test Policy)  
\end{itemize}
As usual, there may be exceptions to the rules, but they have to be approved.

\subsection*{Dealing with multithreading}
Up to now, \xlifepp supports OMP if library is compiled with the OMP flag. It is up to the developer to use it or not but  when heavy computation it is advised to. As \xlifepp already uses OMP in all FE computations or matrix computations, be care when using it at a high level of the code.

\subsection*{How to encapsulate a class hierarchy in an end user class}
The problem is to propose to end user a single class (\class{User}) masking a class hierarchy
(\class{Child1},\class{Child2},\ldots that inherits from \class{User}):
\vspace{.1cm}
\begin{lstlisting}
class Child1 : public User {\ldots};
class Child2 : public User {\ldots};
\end{lstlisting}
\vspace{.2cm}
There are at least three solutions:
\begin{itemize}
\item Using child pointers of child classes inherited from \class{BaseChild} base class:
\vspace{.1cm}
\begin{lstlisting}
class BaseChild {\ldots}
class Child1 : public BaseChild {\ldots};
class Child2 : public BaseChild {\ldots};

class User
{ChildType childType;        //type of child
 Child1 * ch1_p;             //pointer to Child1 object
 Child2 * ch2_p;             //pointer to Child2 object
 \ldots                         //common attributes and functions
};
\end{lstlisting}
\vspace{.1cm}
All the member functions of \class{User} class analyze the child type to address the right
child functions.
\item Using pointer to \class{BaseChild} base class:
\vspace{.1cm}
\begin{lstlisting}
class BaseChild {\ldots}
class Child1 : public BaseChild {\ldots};
class Child2 : public BaseChild {\ldots};

class User
{BasicChild * bch_p;         //pointer to BaseChild object
 \ldots
};
\end{lstlisting}
\vspace{.1cm}
All the member functions of \class{User} class call their equivalent \class{BaseChild} member
functions.
\item Using a contraction of the previous model named "abstract/non-abstract pattern":
\vspace{.1cm}
\begin{lstlisting}
class User
{User* us_p;    //pointer to child or itself if it is a child         
 virtual Child1* child1()
   {if(us_p!=this) us_p->child1();
    return 0;}
};

class Child1 : public User
{Child1* child1(){return this;}
 \ldots
};
class Child2 : public User
{Child2* child2(){return this;}
 \ldots
};
\end{lstlisting}
\vspace{.1cm}
This model is named "abstract/non-abstract" because \class{User} class is a non-abstract class
but is used as an abstract class. In this model, \class{User} class attributes are duplicated.
If they take a large amount of memory, encapsulate it in a structure and use a pointer to this
structure :
\vspace{.1cm}
\begin{lstlisting}
class Attributes
{\ldots
};

class User
{User* us_p;       //pointer to child or itself if it is a child         
 Attributes* at_p; //pointer to User attributes 
};
\end{lstlisting}
\vspace{.1cm}
\end{itemize}
Each of them has their advantages and defaults. Use the one you prefer.

