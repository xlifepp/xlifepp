%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{LargeMatrix} class}

The \class{LargeMatrix} class deals with large matrices with different types of storages. It is a template class \verb?LargeMatrix<T>? where \verb?T? is either the type \verb?Real?, \verb?Complex?, \verb?Matrix<Real>? or \verb?Matrix<Complex>?. Although it is possible to instantiate \verb?LargeMatrix<T>? with other types, most functionalities do not work because some member functions are overloaded only for the previous types.\\

The class mainly manages a vector storing the matrix values and a pointer to a storage structure (\class{MatrixStorage} base class):

\begin{lstlisting}[deletekeywords={[3] name, valueType, strucType}]
template <typename T>
class LargeMatrix{
public :
  ValueType valueType;      // type of values (real, complex)
  StrucType strucType;      // struc of values (scalar, matrix)
  Number nbRows;            // number of rows counted in T
  Number nbCols;            // number of columns counted in T
  SymType sym;              // type of symmetry
  Dimen nbRowsSub;          // number of rows of submatrices (1 if scalar values)
  Dimen nbColsSub;          // number of columns of submatrices (1 if scalar values)
  String name;              // optionnal name, useful for documentation
protected :
  vector<T> values_;   // list of values ordered along storage method
  MatrixStorage* storage_p; // pointer to the matrix storage
   \ldots
\end{lstlisting}

When \verb?T=Matrix<Real>? or \verb?T=Matrix<Complex>?, the number of rows and columns are counted in \verb?Matrix? not in scalar!

\begin{focusbox}
The first value (index 0) of the vector \verb?values_? is not used for storing a matrix value. It contains the default value (in general \verb?T()?) returned when trying to access to a matrix value outside the storage. Be care with default Matrix<T>(), it is a \(0 \times 0\) matrix!
\end{focusbox}

There are various constructors : three from an existing storage, some from file where the large matrix is loaded from the file and some from matrix dimensions and a default value. A large matrix which is created by the copy constructor (shallow copy) will share a same storage with the input one. Be care with these last constructors, they allocate the vector of values only for dense storage.

\begin{lstlisting}[deletekeywords={[3] dims}]
LargeMatrix();
LargeMatrix(const LargeMatrix<T>&);                                                            
LargeMatrix(MatrixStorage*, const T&= T(), SymType= _noSymmetry); 
LargeMatrix(MatrixStorage* ms, dimPair dims, SymType sy = _noSymmetry);
LargeMatrix(MatrixStorage*, SymType);  
LargeMatrix(Number, Number, StorageType = _dense, AccessType = _row, const T& = T());                   
LargeMatrix(Number, Number, StorageType = _dense, SymType = _symmetric, const T& = T());                    
LargeMatrix(const String&, StorageType, Number , Number, StorageType = _dense, 
            AccessType = _row,  Number nsr=1, Number nsc=1);                                  
LargeMatrix(const String&, StorageType, Number, StorageType = _dense, 
            SymType = _symmetric,  Number nsr=1);                                         
 ~LargeMatrix(); 

void init(MatrixStorage*, const T&, SymType);                   
void allocateStorage(StorageType, AccessType, const T& = T()); 
void setType(const T&);                                                            
\end{lstlisting}
\vspace{.1cm} 
The \verb?StorageType?, \verb?AccessType? and \verb?SymType? are enumerations :
\vspace{.1cm}
\begin{lstlisting}
enum StorageType{_noStorage=0,_dense,_cs,_skyline,_coo};
enum AccessType {_noAccess=0,_sym,_row,_col,_dual};    
enum SymType    {_noSymmetry=0,_symmetric,_skewSymmetric,_selfAdjoint,_skewAdjoint, 
                 _diagonal};                                              
\end{lstlisting}
\vspace{.1cm}
The storage type \verb?_cs? means compressed sparse. It refers to the well known CSR/CSC storage (see the next section for details on storage). The storage type \verb?_coo? means coordinates storage (\(i,j,a_{ij}\)). So far, it is not proposed as a storage method for large matrix, but large matrix may be saved to file in this format. Constructors use the utilities: \cmd{init}, \cmd{allocateStorage} and \cmd{setType}. The \cmd{setType} function uses some utilities (external functions) to get the dimensions of sub-matrices:
\vspace{.1cm}
\begin{lstlisting}
pair<Dimen,Dimen> dimsOf(const Real&);
pair<Dimen,Dimen> dimsOf(const Complex&);
pair<Dimen,Dimen> dimsOf(const Matrix<Real>&);
pair<Dimen,Dimen> dimsOf(const Matrix<Complex>&);
bool isDiagonal() const;                     
bool isId(const double & tol=0.) const;     
\end{lstlisting}

\begin{focusbox}
Note that for the moment, it is not possible to load from a file a matrix as a matrix of matrices (use only \verb?nsr=1? and \verb?nsc=1? arguments in constructors from file).
\end{focusbox}

There are few facilities to access to matrix values :

\begin{lstlisting} 
Number pos(Number i, Number j) const;   
void positions(const vector<number_t>&, const vector<number_t>&,
               vector<number_t>&, bool = false) const; 
vector<pair<number_t, number_t>> getCol(number_t, number_t =1, number_t=0) const;  
vector<pair<number_t, number_t>> getRow(number_t, number_t =1, number_t =0) const;  
T operator()(number_t adr) const;
T& operator()(number_t adr);
typename vector<T>::iterator at(number_t, number_t);
typename vector<T>::const_iterator at(number_t i, number_t j) const;                            
T operator() (Number i, Number j) const;                               
T& operator()(Number i, Number j, bool =true); 
Number nbNonZero() const;                                             
void resetZero();                                                  
\end{lstlisting}
\vspace{.1cm}    
The member function \verb?pos(i,j)? returns the address in vector \verb?values_? of the matrix value \(M_{ij}\) for \(i,j\ge 1\). The returned address begins at 1 except when the value is outside the storage where the function returns 0. The access operator \verb?(i,j)? returns the matrix value or a reference to it  with the \emph{non-const} version. In that case, it is possible to activate (\verb?errorOn=true?) an error handler to prevent access to values outside the storage. It is the default behavior. When \verb?errorOn=false?, the access operator returns a reference to \verb?values_[0]?, thus you can modify its value. This trick avoids performing tests like \verb?if(pos(i,j)!=0)\ldots? before writing matrix values. To be safe at end, reset the \verb?values_[0]? to 0 with \cmd{resetZero} function. Note that the skyline or compressed storage structure can not be dynamically changed when "inserting" a new value.

\begin{focusbox}
\verb?pos(i,j)?, \verb?getRow? and \verb?getCol? function should not be used in heavy computations because it is time-consuming. In heavy computation, write codes based on storage structure!
\end{focusbox}

As \class{LargeMatrix} class provides a lot of functions to modify its contents, some being time expansive regarding the storage:
\vspace{.1cm}
\begin{lstlisting}   
void deleteRows(number_t, number_t);             
void deleteCols(number_t, number_t);             
void setColToZero(number_t c1=0, number_t c2=0);
void setRowToZero(number_t r1=0, number_t r2=0); 
void roundToZero(real_t aszero=10*theEpsilon); 

LargeMatrix<T>& operator*=(const K&);           
LargeMatrix<T>& operator/=(const K&);           
LargeMatrix<T>& operator+=(LargeMatrix<T>&);                        
LargeMatrix<T>& operator-=(LargeMatrix<T>&);    
 
LargeMatrix<T>& add(iterator&,iterator&);                            
LargeMatrix<T>& add(const vector<T>&, const vector<number_t>&,const vector<number_t>&);               
LargeMatrix<T>& add(const T& c, const vector<number_t>&, const vector<number_t>&);                
LargeMatrix<T>& add(const LargeMatrix<K>&, const vector<number_t>&,const vector<number_t>&, C);                  
void addColToCol(number_t, number_t, complex_t a, bool =false); 
void addRowToRow(number_t, number_t, complex_t a, bool =false);
void copyVal(const LargeMatrix<T>&, const vector<number_t>&, const vector<number_t>&);                                    
LargeMatrix<T>& assign(const LargeMatrix<K>&, const vector<number_t>&, const vector<number_t>&); 
\end{lstlisting}
\vspace{.2cm}  
Some functions are provided to change the scalar/matrix representation of the matrix or the storage: 
\vspace{.1cm}
\begin{lstlisting}   
void toSkyline();                        
void toStorage(MatrixStorage*);           
LargeMatrix<K>* toScalar(K);             
void toScalarEntries(const vector<Matrix<K> >&, vector<K>&, const MatrixStorage&);
void toUnsymmetric(AccessType at=_sym);      
void extract2UmfPack(vector<int_t>& colPointer,vector<int_t>& rowIndex,vector<T>& matA) const;  
bool getCsColUmfPack(vector<int_t>& colPointer,vector<int_t>& rowIndex, const T*& matA) const;      
\end{lstlisting}
\vspace{.2cm} 
Matrices that are stored as Dense or Skyline can be factorized as LU, LDLt, LDL* and matrices that are stored as Skyline or Sparse can be factorized using UMFPack if it is available. All the stuff related to solve linear system is also available.
\vspace{.1cm}
\begin{lstlisting}   
void ldltFactorize();                        
void ldlstarFactorize();                    
void luFactorize(bool withPermutation=true); 
void umfpackFactorize(); 

void ldltSolve(vector<S1>& vec, vector<S2>& res) const;
void ldlstarSolve(vector<S>& vec, vector<T>& res) const;
void luSolve(vector<S1>& vec, vector<S2>& res) const;
void umfluSolve(vector<S1>& vec, vector<S2>& res) const;

void sorLowerMatrixVector(const vector<S1>& vec, vector<S2>& res, real_t) const;
void sorDiagonalMatrixVector(const vector<S1>& vec, vector<S2>& res,real_t) const;
void sorUpperMatrixVector(const vector<S1>& vec, vector<S2>& res,real_t) const;
void sorLowerSolve(const vector<S1>& vec, vector<S2>& res,real_t) const;
void sorDiagonalSolve(const vector<S1>& vec, vector<S2>& res,real_t) const;
void sorUpperSolve(const vector<S1>& vec, vector<S2>& res,real_t ) const;

real_t umfpackSolve(const vector<S>& vec, 
vector<typename Conditional<NumTraits<ScalarType>::IsComplex, ScalarType, S>::type>& res,
bool soa=true);    
real_t umfpackSolve(const vector<vector<S>*>&,
vector<vector<typename Conditional<NumTraits<ScalarType>::IsComplex, ScalarType,S>::type>* >&,
bool soa=true);  
real_t umfpackSolve(const vector<int_t>& colPointer, const vector<int_t>& rowIndex,
const vector<T>& values, const vector<S>& vec,
vector<typename Conditional<NumTraits<ScalarType>::IsComplex, ScalarType, S>::type>& res,
bool soa=true); 
\end{lstlisting}
\vspace{.2cm} 
In a same way, some functions interfaces eigensolvers:
\vspace{.1cm}
\begin{lstlisting}   

friend number_t eigenDavidsonSolve(const LargeMatrix<K>* pA, const LargeMatrix<K>* pB, 
       vector<pair<complex_t,Vector<complex_t> > >& res, number_t nev, real_t tol, 
       string_t which, bool isInverted, FactorizationType fac, bool isShift);
friend number_t eigenKrylovSchurSolve(const LargeMatrix<K>* pA, const LargeMatrix<K>* pB,
       vector<pair<complex_t,Vector<complex_t> > >& res,number_t nev, real_t tol, 
       string_t which, bool isInverted, FactorizationType fac, bool isShift);
             
\end{lstlisting}
\vspace{.2cm}  
The Frobenius norm \(\sqrt{\sum{|a_{ij}|^2}}\) and infinity norm \(\max_{|a_{ij}|}\) are provided:
\vspace{.1cm}
\begin{lstlisting}   
real_t norm2() const;                                                
real_t norminfty() const;                                            
real_t partialNormOfCol(number_t, number_t, number_t) const;
\end{lstlisting}
\vspace{.2cm}  
Some operations are available as external functions to the class. \\

The \class{LargeMatrix} supplies methods to add two large matrices, which share a same storage. The matrix result will use the same storage as the two added matrices. If the result doesn't point to the storage of addend, after the addition, it will be forced to use this storage, and the number of object sharing this storage increases by one. Similar to multiplication of matrix-vector, these functions are also external templates, with specializations to mixed real and complex types.
\vspace{.1cm}
\begin{lstlisting}
friend void addMatrixMatrix(const LargeMatrix<S>& matA, const LargeMatrix<S>& matB, LargeMatrix<S>& matC);
friend void addMatrixMatrix(const LargeMatrix<Complex>& matA, const LargeMatrix<Real>& matB, LargeMatrix<Complex>& matC);
friend void addMatrixMatrix(const LargeMatrix<Real>& matA, const LargeMatrix<Complex>& matB, LargeMatrix<Complex>& matC);
\end{lstlisting}
\vspace{.1cm}
The operator \verb?+? is overloaded for \class{LargeMatrix} in the same manner.
\vspace{.1cm}
\begin{lstlisting}
LargeMatrix<T> operator+(const LargeMatrix<T>& matA, const LargeMatrix<T>& matB);
LargeMatrix<Complex> operator+(const LargeMatrix<Real>& matA, const LargeMatrix<Complex>& matB);
LargeMatrix<Complex> operator+(const LargeMatrix<Complex>& matA, const LargeMatrix<Real>& matB);
\end{lstlisting}

\begin{focusbox}
The symmetry of the result matrix depends on the ones of both addends. If one of these has \verb?_noSymmetry? as \verb?SymType?, the result will have the same \verb?SymType? (i.e: \verb?_noSymmetry?).
\end{focusbox}

A large matrix can multiply by a scalar with the external template functions:
\vspace{.1cm}
\begin{lstlisting}
friend LargeMatrix<S> multMatrixScalar(const LargeMatrix<S>&, const S);
friend LargeMatrix<Complex> multMatrixScalar(const LargeMatrix<Complex>&, const Real);
friend LargeMatrix<Complex> multMatrixScalar(const LargeMatrix<Real>&, const Complex);
\end{lstlisting}
\vspace{.1cm}
Like \cmd{addMatrixMatrix}, the result of \cmd{multMatrixScalar} shares the same storage of the input large matrix.\\
The operator \verb?*? can be also overloaded in the same manner.
\vspace{.1cm}
\begin{lstlisting}
LargeMatrix<T> operator*(const LargeMatrix<T>& mat, const T v);
LargeMatrix<T> operator*(const T v, const LargeMatrix<T>& mat);
LargeMatrix<Complex> operator*(const LargeMatrix<Complex>& mat, const Real v);
LargeMatrix<Complex> operator*(const Real v, const LargeMatrix<Complex>& mat);
LargeMatrix<Complex> operator*(const LargeMatrix<Real>& mat, const Complex v);
LargeMatrix<Complex> operator*(const Complex v, const LargeMatrix<Real>& mat);
\end{lstlisting}
\vspace{.2cm}
The product of matrix and vector is one of the most important operation required. The \class{LargeMatrix} class interfaces matrix-vector product and the  vector-matrix matrix; the product being really done by the storage classes. They are external template functions to the class (declared friend of class), with specializations to mixed real and complex types (only casting from real to complex is allowed) :        
\vspace{.1cm}
\begin{lstlisting}
void multMatrixVector(const LargeMatrix<S>&, const vector<S>&, vector<S>&);
void multVectorMatrix(const LargeMatrix<S>&, const vector<S>&, vector<S>&);
void multMatrixVector(const LargeMatrix<Matrix<S> >&, 
                      const vector<Vector<S> >&, vector<Vector<S> >&);
void multVectorMatrix(const LargeMatrix<Matrix<S> >&, 
                      const vector<Vector<S> >&, vector<Vector<S> >&);                         
\end{lstlisting}
\vspace{.1cm}  
The operator \verb?*? is overloaded for \class{LargeMatrix} and vector or vector and \class{LargeMatrix} in a same way:
\goodbreak
\vspace{.1cm}
\begin{lstlisting}
friend vector<S> operator*(const LargeMatrix<S>&, const vector<S>&);
friend vector<S> operator*(const vector<S>&, const LargeMatrix<S>&);
friend vector<Vector<S> operator*(const LargeMatrix<Matrix<S> >&, 
                                  const vector<Vector<S> >&);
friend vector<Vector<S> operator*(const vector<Vector<S> >&,
                                  const LargeMatrix<Matrix<S> >&);
\ldots
\end{lstlisting}
\vspace{.1cm}
The product of two matrices is provided, but be cautious, up to now the result matrix is a dense matrix (except if one matrix is a diagonal one):
\vspace{.1cm}
\begin{lstlisting}
void multMatrixMatrix(const LargeMatrix<SA>&, const LargeMatrix<SB>&, LargeMatrix<SR>&);
\end{lstlisting}
\vspace{.1cm}

The \class{LargeMatrix} also provides some functions to factorize a matrix and solve the factorized system. Like \cmd{multMatrixVector}, these functions are external (friend of class), template with hybrid complex-real specialization.
\vspace{.1cm}
\begin{lstlisting}
void ldltFactorize(LargeMatrix<S>& mat);    
void ldlstarFactorize(LargeMatrix<S>& mat);    
void luFactorize(LargeMatrix<S>& mat);  
\end{lstlisting}

\begin{focusbox}
These functions are available for  \class{SkylineStorage} and \class{DenseStorage}.
\end{focusbox}

If a  matrix is factorized, some other operation are available 
\vspace{.1cm}
\begin{lstlisting}
void multFactMatrixVector(const LargeMatrix<S1>&, const vector<S2>&, vector<S3>&);
void multVectorFactMatrix(const LargeMatrix<S1>&, const vector<S2>&, vector<S3>&);
void multInverMatrixVector(const LargeMatrix<S1>& mat, const vector<S2>& vec,
     vector<typename Conditional<NumTraits<S1>::IsComplex, S1, S2>::type>& res,
     FactorizationType);
\end{lstlisting}
\vspace{.1cm}
	
In order to generate a diagonal matrix which uses an existent matrix as prototype, \class{LargeMatrix} class provides function
\vspace{.1cm}
\begin{lstlisting}
LargeMatrix<S> diagonalMatrix(const LargeMatrix<S>&, const S);                 
\end{lstlisting}
\vspace{.1cm}
A special version of this function is a function to generate an identity matrix from an existing one.
\vspace{.1cm}
\begin{lstlisting}
LargeMatrix<S> identityMatrix(const LargeMatrix<S>&);            
\end{lstlisting}

\begin{focusbox}
These functions work with \class{LargeMatrix} class having all kinds of matrix storage except \class{RowCsStorage} class and \class{ColCsStorage} class.
\end{focusbox}

Finally, the class has input/output member functions : 

\begin{lstlisting}
void print(ostream&) const;                                      
void viewStorage(ostream&) const;                                
ostream& operator<<(ostream&, const LargeMatrix<S>&);
void saveToFile(const String &, StorageType, bool encode=false) const;                    
void loadFromFile(const String&, StorageType);  
String encodeFileName(const String&, StorageType) const;                       
\end{lstlisting}
\vspace{.1cm}
Only two formats are allowed for saving matrix to file or loading matrix from file : 
\begin{itemize}
\item the dense format (\verb?_dense?), where all the matrix values are written row by row (one row by line), space character separates each value :
\begin{verbatim}
A11 A12 \ldots A1n                       re(A11) im(A11)  \ldots re(A1n) im(A1n)
A21 A22 \ldots A2n   if complex values   re(A21) im(A21)  \ldots re(A2n) im(A2n)
\ldots                                    \ldots
Am1 Am2 \ldots Amn                       re(Am1) im(Am1)  \ldots re(Amn) im(Amn)
\end{verbatim}
\item the coordinates format (\verb?_coo?)
\begin{verbatim}
i1 j1 Ai1j1                           i1 j1 re(Ai1j1) im(Ai1j1) 
i2 j2 Ai2j2     if complex values     i2 j2 re(Ai2j2) im(Ai2j2) 
\ldots
\end{verbatim}
\end{itemize}

\begin{focusbox}
For matrix of matrices (\verb?T=Matrix<Real>? or \verb?T=Matrix<Complex>?), submatrix structure is not preserved when writing matrix values!
\end{focusbox}

To keep some general information about matrix, with the argument \verb?encode=true? in the \verb?saveFile? function, the name of file may be modified by inserting the string :
\begin{center}
\verb?(m_n_storageType_valueType_strucType_p_q)?
\end{center}
where \verb?m? and \verb?n? are the "dimensions" of the matrix, \verb?storageType = dense or coo?, \verb?valueType = real or complex? , \verb?strucType = scalar or matrix? and \verb?p? and \verb?q? are the dimensions of sub-matrices. In case of scalar value, these parameters are omitted. This trick avoids including information in file in order that it is easily loadable by other software programs, in particular Matlab.

\subsection*{Some examples}

Manipulate a \(3\times 2\) matrix with row dense storage:
\vspace{.1cm}
\begin{lstlisting}
Vector<Real> x(2,2);
LargeMatrix<Real> A(3,2,_dense,_row,1.);
out<<"matrix A : "<<A;
out<<"access A(1,1)= "<<A(1,1);
out<<"product A*x : "<<A*x;
A.saveToFile("A_dense.mat",_dense);
LargeMatrix<Real> Areload("A_dense.mat",_dense,3,2,_dense,_row);
\end{lstlisting}
\vspace{.2cm}
Manipulate a \(3\times 2\) matrix of \(2\times 2\) real matrices with dual dense storage:
\vspace{.1cm}
\begin{lstlisting}
Matrix<Real> I1(2,_idMatrix);
Vector<Real> x(6,3);
LargeMatrix<Matrix<Real> > B(6,3,_dense,_dual,2*I1);
out<<"product X*B : "<<X*B;
\end{lstlisting}
\vspace{.2cm}
Manipulate a \(9\times 9\) symmetric matrix with symmetric compressed storage:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] x}]
//construct storage for a Q1 mesh
Number n=3;
vector<vector<Number> > elts((n-1)*(n-1),vector<Number>(4));
for(Number j=1; j<=n-1; j++)
  for(Number i=1; i<=n-1; i++) {
     Number e=(j-1)*(n-1)+i-1, p=(j-1)*n+i;
     elts[e][0]=p; elts[e][1]=p+1; elts[e][2]=p+n; elts[e][3]=p+n+1;
     }
SymCsStorage* cssym= new SymCsStorage(n*n,elts,elts);
//construct large matrix with symmetric compressed storage
LargeMatrix<Real> C(cssym,1.,_symmetric);
C.viewStorage(out);
Vector<Real> x(n*n,0.);for(Number i=0;i<n*n;i++) x1[i]=i;
out<<"product C*x : "<<C*x;
C.saveToFile("C_coo.mat"),_coo);
//construct large matrix with symmetric compressed storage
LargeMatrix<Real> C1(cssym,1.,_symmetric);
out<<"addition C+C1 "<< C+C1;
// multiply a matrix with a scalar
out<<"product C*10.0 "<< C*10.0;
// generate a diagonal matrix
out<<"diagonal matrix from matrix C " << diagonalMatrix(C, 10.0);
// generate an identity matrix
out<<"identity matrix from matrix C " << identityMatrix(C);
\end{lstlisting}
\vspace{.1cm}
Note that compressed storage has to be built before to instantiate a large matrix with compressed storage. The function \cmd{viewStorage} produced the output:
\begin{verbatim}
symmetric_compressed sparse (csr,csc), size = 29, 9 rows, 9 columns
 (shared by 1 objects).
          123456789
       1  d\ldots\ldots..
       2  xd\ldots\ldots.
       3  .xd\ldots\ldots
       4  xx.d\ldots..
       5  xxxxd\ldots.
       6  .xx.xd\ldots
       7  \ldotsxx.d..
       8  \ldotsxxxxd.
       9  \ldots.xx.xd
          123456789
\end{verbatim}
\vspace{.1cm}
LDLt-Factorize a positive definite 3x3 matrix with symmetric skyline storage:
\vspace{.1cm}
\begin{lstlisting}
LargeMatrix<Real> rResSymSkylineLdlt(inputsPathTo("matrix3x3SymPosDef.data"), _dense, rowNum, _skyline, _symmetric);
ldltFactorize(rResSymSkylineLdlt);
out << "The result of LDLt factorizing sym skyline matrix is " << rResSymSkylineLdlt << endl;
\end{lstlisting}
\vspace{.1cm}
\displayInfos{library=largeMatrix, header=LargeMatrix.hpp, implementation=LargeMatrix.cpp, test={test\_LargeMatrixDenseStorage.cpp, test\_LargeMatrixCsStorage.cpp}, header dep={MatrixStorage.hpp, DenseStorage.hpp, CsStorage.hpp, SkylineStorage.hpp, config.h, utils.h}}
