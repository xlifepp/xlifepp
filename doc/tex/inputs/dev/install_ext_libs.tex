%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Installation and use of \blas and \lapack libraries}\label{a.s.blas}

Using \umfpack or \arpack means using \blas and \lapack libraries. \xlifepp offers the ability to choose your \blas/\lapack installation:

\begin{itemize}
\item Using \blas/\lapack installed with \umfpack or \arpack
\item Using default \blas/\lapack installed on your computer
\item Using standard \blas/\lapack libraries, such as \openblas.
\end{itemize}

To do so, you just have to use \cmakeopt{XLIFEPP\_LAPACK\_LIB\_DIR} and/or \cmakeopt{XLIFEPP\_BLAS\_LIB\_DIR} to set the directory containing \lapack and \blas libraries:
\begin{lstlisting}[language=]
cmake [\ldots] -DXLIFEPP_BLAS_LIB_DIR=path/to/Blas/library/directory -DXLIFEPP_LAPACK_LIB_DIR=path/to/Lapack/library/directory path/to/CMakeLists.txt
\end{lstlisting}

\begin{warningbox}
\cmakeopt{XLIFEPP\_LAPACK\_LIB\_DIR} and/or \cmakeopt{XLIFEPP\_BLAS\_LIB\_DIR} options are useless if you do not activate configuration with \umfpack or \arpack
\end{warningbox}

\section{Installation and use of \umfpack library}\label{a.s.umfpack}

The prerequisite to make use of \umfpack is to have it installed or at least its libraries are compiled. The \lib{umfpackSupport} can be linked with or without \umfpack in case of none of its functions is invoked. Otherwise, any try to use its provided functions can lead to a linkage error. Details to compile and install \umfpack can be found at \url{http://www.cise.ufl.edu/research/sparse/umfpack/}. All the steps will be described below supposing \umfpack {\bfseries already installed or compiled} in the user's system. \\
In order to make use of \umfpack routines, user must configure \cmake with options: \cmakeopt{XLIFEPP\_ENABLE\_UMFPACK}, \cmakeopt{XLIFEPP\_UMFPACK\_INCLUDE\_DIR} and \cmakeopt{XLIFEPP\_UMFPACK\_LIB\_DIR}.

\vspace{.1cm}
\begin{lstlisting}[language=]
cmake  -DXLIFEPP_ENABLE_UMFPACK=ON -DXLIFEPP_UMFPACK_INCLUDE_DIR=path/to/UMFPACK/include/directory -DXLIFEPP_UMFPACK_LIB_DIR=path/to/UMFPACK/library/directory path/to/CMakeLists.txt
\end{lstlisting}
\vspace{.1cm}

\begin{focusbox}
Theoretically, \umfpack does not need to use \blas/\lapack, but as it is highly recommended by \umfpack (for accuracy reasons), \xlifepp demand that you use \blas/\lapack.
\end{focusbox}

\umfpack is provided by \suitesparse. When looking how \umfpack is compiled, it seems that it can depend (maybe in the same way as \blas/\lapack) on other libraries provided by \suitesparse.

In this case, you have a simpler way to define paths: by using \cmakeopt{XLIFEPP\_SUITESPARSE\_HOME\_DIR}. When given alone, it considers the given directory as the home directory containing every library provided by \suitesparse with a specific tree structure. If it is not the case, you can use more specific options of the form \cmakeopt{XLIFEPP\_XXX\_INCLUDE\_DIR} and \cmakeopt{XLIFEPP\_XXX\_LIB\_DIR}, where XXX can be AMD, COLAMD, CAMD, CCOLAMD, CHOLMOD, SUITESPARSECONFIG or UMFPACK.

\section{Installation and use of \arpackpp library}\label{a.s.arpackpp}

\arpackpp distribution can be obtained from the following URL: \url{http://www.ime.unicamp.br/~chico/arpack++/}. However, because of the deprecation of this version, a patch at \url{http://reuter.mit.edu/index.php/software/arpackpatch/} needs to be applied to make sure a correct compilation. Users of Unix-like system can follow the instructions on \url{https://help.ubuntu.com/community/Arpack%2B%2B} to make the patch. And this patch is often not enough to compile correctly with recent compilers. \\
So \xlifepp contains its own patched release of \arpackpp, used by default.

\medskip

Because \arpackpp is an interface to the original \arpack \fortran library, this library must be available when installing the \cpp code. Although \fortran files are not distributed along with \arpackpp, they can be easily downloaded from Netlib and are even available under some Unix-like systems.

The \blas and \lapack routines are required by the \arpack \fortran, so make sure these two libraries are installed in the system. Like \arpack, these two libraries are available under some Unix-like systems.

By default, the intern eigensolver of \xlifepp is used for calculating eigenvalues and eigenvectors. To make use of \arpackpp, users must configure \cmake with options : \cmakeopt{XLIFEPP\_ENABLE\_ARPACK} and \cmakeopt{XLIFEPP\_ARPACK\_LIB\_DIR}.

The current directory is the root directory containing \xlifepp source code. To enable \arpack, we use the command:
\vspace{.1cm}
\begin{lstlisting}
cmake -DXLIFEPP_ENABLE_ARPACK=ON -DXLIFEPP_ARPACK_LIB_DIR=path/to/arpack/libraries/directory path/to/CMakeLists.txt
\end{lstlisting}
\vspace{.1cm}
After configuring, we can make the library
\vspace{.1cm}
\begin{lstlisting}
make
\end{lstlisting}
\vspace{.1cm}

Now, there are additional options. Indeed, the default behavior of \xlifepp installation process is to compile and use a provided distribution of \arpack. If you want to configure \xlifepp with another distribution, you may set the option \cmakeopt{XLIFEPP\_SYSTEM\_ARPACK} to ON.

At last, when a system distribution of \arpack is such that the name of the library file contains the version number, the standard detection process of \cmake will not find it. In this case, instead of setting \cmakeopt{XLIFEPP\_ARPACK\_LIB\_DIR}, you will use \cmakeopt{XLIFEPP\_ARPACK\_LIB}.
