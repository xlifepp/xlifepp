%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Geometric element}\label{sec_GeomElement}

The atomic object of a mesh is a geometric element, that is a segment, a polygon or a polyhedron
of physical space with flat or curved faces or edges. A general way to describe it, consists
in giving a polynomial interpolation and a reference geometric element in reference space.
Thus, a geometric element is the image by the map of a reference geometric element. This map
is defined from the polynomial interpolation and some nodes of the geometric element: vertices
and extra points regarding the order of the polynomial interpolation. 
\begin{center}
\includePict[trim = 0mm 6cm 0mm 0mm, width=9cm]{P1map.pdf}
\end{center}
\begin{center}
\includePict[trim = 0mm 9cm 0mm 0mm, clip,  width=9cm]{P2map.pdf}
\end{center}
Geometric elements are the geometric supports of finite element. In other words, a finite element
is defined from a geometric element and an interpolation (different from interpolation used
in geometric element!), see \class{Element} class in \lib{space} library.\\

As some finite elements may be required on a boundary of domain, geometric element can be a
side of geometric element and even a side of side. It is the reason why a geometric element
can possibly manage side information (parent and side number).

\subsection{The \classtitle{GeomElement} class}

The \class{GeomElement} class has only four protected members:
\vspace{.1cm}
\begin{lstlisting}
class GeomElement
{protected :
   const Mesh* mesh_p;                     //pointer to the mesh
   Number number_;                         //unique id number 
   MeshElement* meshElement_p;             //pointer to a MeshElement
   std::vector<GeoNumPair> parentSides_;   //if a side element
   \ldots
\end{lstlisting}
\vspace{.1cm}
The most important member \verb?meshElement_p? is a pointer to a \class{MeshElement} object
collecting real geometric information (nodes, numbering, reference element, map data, \ldots).
This class is detailed in the next section. \\

Besides, it manages some additional information:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] phi}]
Number materialId;    // material id (default= 0)
Number domainId;      // domain id (default=0)
Real theta, phi;      // angles to describe local curvature (default 0,0)
\end{lstlisting}
\vspace{.1cm}

\verb?GeoNumPair? is an alias for storing a pair of parent geometric element and a side number:
 \vspace{.1cm}
\begin{lstlisting}
typedef std::pair<GeomElement*,Number> GeoNumPair;
\end{lstlisting}
\vspace{.1cm}
When the geometric element is a side of a geometric element, \verb?parentSides_? is not empty
and by default, the \verb?meshElement_p? is null. Note that it is possible to build it. Of
course, when geometric element is not a side element (say plain element), \verb?parentSides_?
is empty.\\

This class provides few public constructors, an assignment operator and a destructor:
\vspace{.1cm}
\begin{lstlisting}
GeomElement(const Mesh* m=0, Number n = 0);                   
GeomElement(const Mesh*, const RefElement*, Dimen, Number);  //for plain element
GeomElement(GeomElement*, Number, Number);                   //for side element
GeomElement(const GeomElement&); 
GeomElement& operator =(const GeomElement&);
\end{lstlisting}
\vspace{.2cm}
some simple accessors:
\vspace{.1cm}
\begin{lstlisting}
Number number() const;   
Number& number();         
bool hasMeshElement() const;
bool isSideElement() const;
\end{lstlisting}
\vspace{.2cm}
and complex accessors working for plain or side elements, even if \verb?meshElement_p? is null
:
\vspace{.1cm}
\begin{lstlisting}
const MeshElement* meshElement() const; 
MeshElement* meshElement();       
const GeomElement* parent(Number i=0) const; 
const GeoNumPair parentSide(Number i) const;
std::vector<GeoNumPair>& parentSides();
Dimen elementDim() const;    
const RefElement* refElement(Number s = 0) const;   
const GeomRefElement* geomRefElement(Number s = 0) const; 
Number numberOfVertices() const; 
Number numberOfSides() const;     
Number numberOfSideOfSides() const;
ShapeType shapeType(Number s = 0) const;             
Number vertexNumber(Number i) const;      
Number nodeNumber(Number i) const;                      
std::vector<Number> vertexNumbers(Number s = 0) const; 
std::vector<Number> nodeNumbers(Number s = 0) const;       
\end{lstlisting}
\vspace{.1cm}
\verb?s? means a side number of elements and when it is null, function applies to element itself.
Most of these member functions are recursive. It allows getting data for plain, side and side
of side elements (side of side of side is not managed). Be cautious, these member functions
does not check the integrity of data. For instance, the function \cmd{meshElement} may return
a null pointer (case of a side element). \\

For a side element, it is possible to construct, if really necessary, its \class{MeshElement}
structure using the member function:
\vspace{.1cm}
\begin{lstlisting}
void buildSideMeshElement(); 
void deleteMeshElement();        // destroy meshElement information
\end{lstlisting}
\vspace{.2cm}
There is an encoding function which constructs a string key with global numbers of vertices
of the geometric element (\verb?s=0?) or its side (\verb?s?). This function is used to build
global lists of sides (edge in 2D or face in 3D), of side of sides (edge in 3D) and vertices
(see the \class{Mesh} class).
\vspace{.1cm}
\begin{lstlisting}
String encodeElement(Number s=0) const;                 
\end{lstlisting}
\vspace{.2cm}
Related to these global lists, the following functions return temporary lists of adjacent elements
by side, by side of side or by vertex:
\vspace{.1cm}
\begin{lstlisting}
GeoNumPair elementsSharingSide(Number s) const;                                  
std::vector<GeoNumPair> elementsSharingVertex(Number v,bool o=false) const;  
std::vector<GeoNumPair> elementsSharingSideOfSide(Number s, bool o=false) const;
\end{lstlisting}
\vspace{.2cm}
When the boolean argument \verb?o? is true, it means that elements in lists are adjacent \textbf{only}
by side of side or only by vertex. When it is false, all elements are listed. Obviously, these
functions work only if the global lists of sides, of side of sides and vertices have been constructed
before. By default, they are not (see the \class{Mesh} class).\\

The class provides a function to test if a point belongs to element:
\vspace{.1cm}
\begin{lstlisting}
bool contains(const std::vector<Real>& p);      
\end{lstlisting}
\vspace{.2cm}
It is possible to compare \class{GeomElement} objects by their indices (\verb?number_?) using
the operators:
\vspace{.1cm}
\begin{lstlisting}
bool operator== (const GeomElement&, const GeomElement&);
bool operator<  (const GeomElement&, const GeomElement&);
\end{lstlisting}
\vspace{.2cm}
For output purpose, you can split a GeomElement in first order elements, either simplices or of same shape:
\vspace{.1cm}
\begin{lstlisting}
std::vector<GeomElement*> splitP1() const;
std::vector<GeomElement*> splitO1() const;
\end{lstlisting}
\vspace{.2cm}
Finally, the \class{GeomElement} class provides usual printing facilities:
\vspace{.1cm}
\begin{lstlisting}
void print(std::ostream&) const ; 
friend std::ostream& operator<< (std::ostream&, const GeomElement&); 
\end{lstlisting}
\vspace{.2cm}

\subsection{The \classtitle{MeshElement} class}

\class{MeshElement} is the real class storing geometric element data:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] nodes, vertexNumbers, nodeNumbers}]
class MeshElement
{public:
  std::vector<Point*> nodes;              //nodes of element 
  std::vector<Number> nodeNumbers;        //global numbers of node
  std::vector<Number> vertexNumbers;      //global numbers of vertices
  std::vector<Number> sideNumbers;        //global numbers of sides
  std::vector<Number> sideOfSideNumbers;  //global numbers of side of sides
  short int orientation;                  //element orientation 
  std::vector<Real> measures;             //measure of element and its sides
  GeomMapData* geomMapData_p;             //useful data for geometric map 
 private:
  Number index_;                          //element index 
  const Dimen spaceDim_;                  //space dimension
  const RefElement* refElt_p;       //pointer to associated Reference Element 
  \ldots
\end{lstlisting}
\vspace{.1cm}
The vector \verb?nodes? gives direct access to the node coordinates of the element (it is
redundant).  The numbering vectors \verb?nodeNumbers?, \verb?vertexNumbers?, \verb?sideNumbers?
and \verb?sideOfSideNumbers? are related to global vectors defined in \class{Mesh} ; \verb?nodes?,
\verb?vertices_?, \verb?sides_? and \verb?sideOfSide_?. Note that the  \verb?sideNumbers? and
\verb?sideOfSideNumbers? vectors are not built by default (see the \verb?buildSides? and \verb?buildSideOfSides?
member functions of \class{Mesh} class). The \verb?nodeNumbers? and \verb?vertexNumbers? vectors
are the same for one order polynomial geometric interpolation. \\

The orientation of an element is defined by default as the sign of the determinant of the jacobian of the geometric map from reference element to geometric element; this map being well-defined through the \class{RefElement} pointer \verb?refElt_p? and the geometric element nodes. But, this orientation may be changed by the function \cmd{setOrientationForManifold} which set the orientation in order the normal computed from jacobian be always the outward normal.\\

Besides, the \class{MeshElement} class manages
a pointer to a \class{GeomMapData} object storing computational data related to the geometric
map: jacobian matrix, its inverse, its determinant, \ldots see the next section.

\begin{focusbox}
As it refers to some \class{Mesh} class members, \class{MeshElement} object should not be
instantiated independently of a mesh.
\end{focusbox}

The class has only one basic constructor initializing the \class{RefElement} pointer, the
space dimension and the element index:
\vspace{.1cm}
\begin{lstlisting}
MeshElement();
MeshElement(const RefElement*, Dimen, Number); 
~MeshElement();
\end{lstlisting}
\vspace{.1cm}
It means that numbering information has to be given outside the constructor. Generally, this
is done by meshing tools.\\

There are some useful accessors, most of them providing same information as those defined in
the \class{GeomElement} interface class:
\vspace{.1cm}
\begin{lstlisting}
Number index() const;
Dimen spaceDim() const;
Number order() const;
Real measure(const Number s = 0) const;
const RefElement* refElement(Number s = 0) const;
const GeomRefElement* geomRefElement(Number i = 0) const;
Dimen elementDim() const;
ShapeType shapeType(Number s = 0) const;
Number vertexNumber(Number i) const;
Number nodeNumber(Number i) const;
std::vector<Number> verticesNumbers(Number s=0) const;
Number numberOfNodes() const;
Number numberOfVertices() const;
Number numberOfSides() const;
Number numberOfSideOfSides() const;
\end{lstlisting}
\vspace{.2cm}

Besides, there are some computation functions related to geometric element:
\vspace{.1cm}
\begin{lstlisting}
void computeMeasures();
void computeMeasure();
void computeMeasureOfSides();
void computeOrientation();
bool contains(const std::vector<Real>&);
void clearGeomMapData();
\end{lstlisting}
\vspace{.1cm}
The functions related to jacobian computations are defined in the \class{GeomMapData} class.
The \verb?geomMapData_p? pointer may be used to store map data and the function \verb?clearGeomMapData()?
deletes the \class{GeomMapData} object.\\

Finally, \class{MeshElement} information are displayed with:
\vspace{.1cm}
\begin{lstlisting}
void print(std::ostream&) const;
friend std::ostream& operator<< (std::ostream&, const MeshElement&);  
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=geometry, header=GeomElement.hpp, implementation=GeomElement.cpp, test=test\_GeomElement.cpp,
header dep={config.h, utils.h}}

