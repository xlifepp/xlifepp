%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Solvers}

Each solver class in the \lib{solvers} library implements a specific iterative method to solve a linear system: \verb?A*X=B? in two cases: Real or Complex.  The algorithm of all solvers except SOR and SSOR can work with and without a \class{Preconditioner}. Each solver takes \class{largeMatrix} \(matA\) and \class{Vector} \(vecB\) as inputs and a \class{Vector} \(vecX\) as output whose value is initialized with the input \class{Vector} \(vecX0\).\\
A solver without a preconditioner is invoked with the overloading operator():
\vspace{.1cm}
\begin{lstlisting}
template<class Mat, class VecB, class VecX>
VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, ValueType solType)
\end{lstlisting}
\vspace{.1cm}
Meanwhile, solver with a preconditioner \(pc\) can be used with the call
\vspace{.1cm}
\begin{lstlisting}
 template<class Mat, class VecB, class VecX, class Prec>
 VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, Prec& pc, ValueType solType)
\end{lstlisting}
\vspace{.1cm}
User must specify the type of solver \verb?solType?, which can be \verb?_real? or \verb?_complex?. \\
The algorithm corresponds to without-preconditioner solver
\vspace{.1cm}
\begin{lstlisting}
template<typename K, class Mat, class VecB, class VecX>
void algorithm(Mat& matA, VecB& vecB, VecX& vecX, VecX& vecR)
\end{lstlisting}
\vspace{.1cm}
and to a preconditioned one
\vspace{.1cm}
\begin{lstlisting}
 template<typename K, class Mat, class VecB, class VecX, class Prec>
 void algorithm(Mat& matA, VecB& vecB, VecX& vecX, VecX& vecR, Prec& pc)
\end{lstlisting}
\vspace{.1cm}

\subsection {The \classtitle{BicgSolver} class}

This class implements the Biconjugate Gradient method to solve a linear system.
There are several basic constructors calling \class{IterativeSolver} constructors
\vspace{.1cm}
\begin{lstlisting}
class BicgSolver : public IterativeSolver
{
  public:
    //! Default Constructor
    BicgSolver() : IterativeSolver(_bicg) {}
    //! Full constructor
    BicgSolver(real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = 0)
    : IterativeSolver(_bicg, maxOfIt, eps, vb) {}

    //! contructors with key-value system
    BicgSolver(const Parameter& p1) : IterativeSolver(_bicg)
    {
      std::vector<Parameter> ps(1, p1);
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_bicg;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    BicgSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_bicg)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_bicg;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    BicgSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_bicg)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_bicg;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    \ldots
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=solvers, test={test\_BicgSolver.hpp}, header dep={IterativeSolver.hpp, Preconditioner.hpp}}

\subsection {The \classtitle{BicgStabSolver} class}

This class implements the Biconjugate Gradient Stabilized method to solve a linear system.
There are several basic constructors calling \class{IterativeSolver} constructors
\vspace{.1cm}
\begin{lstlisting}
class BicgStabSolver : public IterativeSolver
{
  public:
    //! Default Constructor
    BicgStabSolver() : IterativeSolver(_bicgstab) {}
    //! Full Constructor
    BicgStabSolver(real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = 0)
    : IterativeSolver(_bicgstab, maxOfIt, eps, vb) {}

    //! contructors with key-value system
    BicgStabSolver(const Parameter& p1) : IterativeSolver(_bicgstab)
    {
      std::vector<Parameter> ps(1, p1);
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_bicgstab;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    BicgStabSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_bicgstab)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_bicgstab;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    BicgStabSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_bicgstab)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_bicgstab;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    \ldots
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=solvers, test={test\_BicgStabSolver.hpp}, header dep={IterativeSolver.hpp, Preconditioner.hpp}}

\subsection {The \classtitle{CgSolver} class}

This class implements the Conjugate Gradient method to solve a linear system.
There are several basic constructors calling \class{IterativeSolver} constructors
\vspace{.1cm}
\begin{lstlisting}
class CgSolver : public IterativeSolver
{
  public:
    //! Default constructor
    CgSolver(): IterativeSolver(_cg) {}
    //! Full constructor
    CgSolver(real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = 0)
    : IterativeSolver(_cg, maxOfIt, eps, vb) {}

    //! contructors with key-value system
    CgSolver(const Parameter& p1) : IterativeSolver(_cg)
    {
      std::vector<Parameter> ps(1, p1);
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_cg;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    CgSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_cg)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_cg;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    CgSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_cg)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_cg;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    \ldots
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=solvers, test={test\_CgSolver.hpp}, header dep={IterativeSolver.hpp, Preconditioner.hpp}}

\subsection {The \classtitle{CgsSolver} class}

This class implements the Conjugate Gradient Squared method to solve a linear system.
There are several basic constructors calling \class{IterativeSolver} constructors
\vspace{.1cm}
\begin{lstlisting}
class CgsSolver : public IterativeSolver
{
  public:
    //! Default constructor
    CgsSolver()
      : IterativeSolver(_cgs) {}
    //! Full constructor
    CgsSolver(real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = 0)
      : IterativeSolver(_cgs, maxOfIt, eps, vb) {}

    //! contructors with key-value system
    CgsSolver(const Parameter& p1) : IterativeSolver(_cgs)
    {
      std::vector<Parameter> ps(1, p1);
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_cgs;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    CgsSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_cgs)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_cgs;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    CgsSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_cgs)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_cgs;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    \ldots
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=solvers, test={test\_CgsSolver.hpp}, header dep={IterativeSolver.hpp, Preconditioner.hpp}}

\subsection {The \classtitle{GmresSolver} class}

This class implements the Generalized Minimal Residual method to solve a linear system.
Different from other solvers, the \class{GmresSolver} has more constructors: user can specify the Krylov dimension in the constructor.
\vspace{.1cm}
\begin{lstlisting}
class GmresSolver : public IterativeSolver
{
  private:
    number_t krylovDim_;  //!< Dimension of Krylov space

  public:
    //! Constructor with Krylov dimension
    GmresSolver(number_t kd=defaultKrylovDimension)
      : IterativeSolver(_gmres, defaultMaxIterations, theDefaultConvergenceThreshold),
        krylovDim_(kd) {}
    //! Full constructor
    GmresSolver(number_t kd, real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = 0)
    : IterativeSolver(_gmres, maxOfIt, eps, vb), krylovDim_(kd) {}

    //! contructors with key-value system
    GmresSolver(const Parameter& p1) : IterativeSolver(_gmres)
    {
      std::vector<Parameter> ps(1, p1);
      real_t omega=1.;
      IterativeSolverType ist=_gmres;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim_, verboseLevel_, name_, ist);
    }
    GmresSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_gmres)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      real_t omega=1.;
      IterativeSolverType ist=_gmres;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim_, verboseLevel_, name_, ist);
    }
    GmresSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_gmres)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      real_t omega=1.;
      IterativeSolverType ist=_gmres;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim_, verboseLevel_, name_, ist);
    }
    GmresSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4) : IterativeSolver(_gmres)
    {
      std::vector<Parameter> ps(4);
      ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4;
      real_t omega=1.;
      IterativeSolverType ist=_gmres;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim_, verboseLevel_, name_, ist);
    }
    \ldots
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=solvers, test={test\_GmresSolver.hpp}, header dep={IterativeSolver.hpp, Preconditioner.hpp}}

\subsection {The \classtitle{QmrSolver} class}

This class implements the Quasi-Minimal Residual method to solve a linear system.
There are several basic constructors calling \class{IterativeSolver} constructors
\vspace{.1cm}
\begin{lstlisting}
class QmrSolver : public IterativeSolver
{
  public:
    //! Constructor
    QmrSolver()
      : IterativeSolver(_qmr) {}
    //! Full Constructor
    QmrSolver(real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = 0)
    : IterativeSolver(_qmr, maxOfIt, eps, vb) {}

    //! contructors with key-value system
    QmrSolver(const Parameter& p1) : IterativeSolver(_qmr)
    {
      std::vector<Parameter> ps(1, p1);
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_qmr;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    QmrSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_qmr)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_qmr;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    QmrSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_qmr)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_qmr;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    \ldots
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=solvers, test={test\_QmrSolver.hpp}, header dep={IterativeSolver.hpp, Preconditioner.hpp}}

\subsection {The \classtitle{SorSolver} class}

This class implements the Successive Over Relaxation method to solve a linear system.
For this solver, user can have more choices to specify the relaxation factor \(omega\) in the constructor.
Different from other solvers above, \class{SorSolver} doesn't work with a preconditioner.
\vspace{.1cm}
\begin{lstlisting}
class SorSolver : public IterativeSolver
{
  private:
    real_t omega_;  //!< relaxation factor of SOR method

  public:
    //! Constructor with omega
    SorSolver(real_t w=1.) : IterativeSolver(_sor), omega_(w) {}
    //! Full constructor
    SorSolver(real_t w, real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = 0)
    : IterativeSolver(_sor, maxOfIt, eps, vb), omega_(w) {}

    //! contructors with key-value system
    SorSolver(const Parameter& p1) : IterativeSolver(_sor)
    {
      std::vector<Parameter> ps(1, p1);
      number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_sor;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega_, krylovDim, verboseLevel_, name_, ist);
    }
    SorSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_sor)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_sor;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega_, krylovDim, verboseLevel_, name_, ist);
    }
    SorSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_sor)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_sor;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega_, krylovDim, verboseLevel_, name_, ist);
    }
    SorSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4) : IterativeSolver(_sor)
    {
      std::vector<Parameter> ps(4);
      ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4;
      number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_sor;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega_, krylovDim, verboseLevel_, name_, ist);
    }
    \ldots
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=solvers, test={test\_SorSolver.hpp}, header dep={IterativeSolver.hpp, Preconditioner.hpp}}

\subsection {The \classtitle{SsorSolver} class}

This class implements the Symmetric Successive Over Relaxation method to solve a linear system.
Like \class{SorSolver}, the \class{SsorSolver} only works without a preconditioner and user can specify the relaxation factor \(omega\) in the constructor.
\vspace{.1cm}
\begin{lstlisting}
class SsorSolver : public IterativeSolver
{
  private:
    real_t omega_;  //!< relaxation factor of SOR method

  public:
    //! Constructor with omega
    SsorSolver(real_t w=1.) : IterativeSolver(_ssor), omega_(w) {}
    //! Full constructor
    SsorSolver(real_t w, real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = 0)
    : IterativeSolver(_ssor, maxOfIt, eps, vb), omega_(w) {}

    //! contructors with key-value system
    SsorSolver(const Parameter& p1) : IterativeSolver(_ssor)
    {
      std::vector<Parameter> ps(1, p1);
      number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_ssor;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega_, krylovDim, verboseLevel_, name_, ist);
    }
    SsorSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_ssor)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_ssor;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega_, krylovDim, verboseLevel_, name_, ist);
    }
    SsorSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_ssor)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_ssor;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega_, krylovDim, verboseLevel_, name_, ist);
    }
    SsorSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4) : IterativeSolver(_ssor)
    {
      std::vector<Parameter> ps(4);
      ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4;
      number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_ssor;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega_, krylovDim, verboseLevel_, name_, ist);
    }
    \ldots
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=solvers, test={test\_SsorSolver.hpp}, header dep={IterativeSolver.hpp, Preconditioner.hpp}}
