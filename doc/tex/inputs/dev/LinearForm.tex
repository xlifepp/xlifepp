%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Linear forms}

Linear forms are managed using 
\begin{itemize}
\item \class{BasicLinearForm} abstract class of basic linear form,
\item \class{IntgLinearForm} class inheriting from \class{BasicLinearForm} and dealing with
integral form,
\item \class{DoubleIntgLinearForm} class inheriting from \class{BasicLinearForm} and dealing
with double integral forms,
\item \class{SuLinearForm} class storing linear combination of \class{ BasicLinearForm} objects
having the same unknown,
\item \class{LinearForm} class storing a list of \class{SuLinearForm} objects using a map
indexed by unknown. 
\end{itemize}

\subsection{The \classtitle{BasicLinearForm} class}

The \class{BasicLinearForm} class is an abstract class having only an unknown as attribute
and its associated accessor:
\vspace{.1cm}
\begin{lstlisting}
class BasicLinearForm
{protected:
   const Unknown* u_p;           //pointer to unknown
 public :
   const Unknown& up() const     //return the unknown
\end{lstlisting}
\vspace{.2cm}
The child classes inheriting from \class{BasicLinearForm} have to provide the following member
functions:
\vspace{.1cm}
\begin{lstlisting}
virtual ~BasicLinearForm(){};              //virtual destructor
virtual BasicLinearForm* clone() const =0; //clone of the linear form
virtual LinearFormType type() const =0;    //return the type of the linear form
virtual ValueType valueType() const=0;     //return the value type of the linear form
virtual void print(std::ostream&) const =0;//print utility
\end{lstlisting}
\vspace{.1cm}
The virtual \cmd{clone} function is used to construct a copy of the child objects from parent.

\subsection{The \classtitle{IntgLinearForm} class}

The \class{IntgLinearForm} class handles linear forms defined by a (single) integral over
a geometric domain:   
\[
\int_{D} {\cal L}(u)\]
where \(D\) is a geometric domain (\class{Domain} class) and \({\cal L}\) a linear operator (\class{OperatorOnUnknown} class) acting on unknown \(u\) (\class{Unknown} class).\\
This class is defined as follows:
\vspace{.1cm}
\begin{lstlisting}
class IntgLinearForm : public BasicLinearForm
{protected :
 const GeomDomain* domain_p; //geometric domain of the integral (pointer)
 const OperatorOnUnknown* opu_p; //operator on unknown (pointer)
\end{lstlisting}
\vspace{.2cm}
It has some public constructors and some public accessors:
\vspace{.1cm}
\begin{lstlisting}
IntgLinearForm(const GeomDomain&,const OperatorOnUnknown&); 
const OperatorOnUnknown* opu() const;         
const GeomDomain* domain() const;
virtual BasicLinearForm* clone() const;       
virtual LinearFormType type() const         
virtual ValueType valueType() const        
virtual void print(std::ostream&) const;   
\end{lstlisting}    
\vspace{.2cm}

\subsection{The \classtitle{DoubleIntgLinearForm} class}

The \class{DoubleIntgLinearForm} class handles linear forms defined by a double integral over
a product of geometric domains:   
\[
\int_{D_x}\int_{D_y} \mathcal{L}(u)\]
where \(D_x\), \(D_y\) are geometric domain (\class{GeomDomain} class) and \(\mathcal{L}\) a linear operator
(\class{OperatorOnUnknown} class) acting on unknown \(u\) (\class{Unknown} class).\\
Except, there are two geometric domains, this class is very similar to the \class{IntgLinearForm}
class:
\vspace{.1cm}
\begin{lstlisting}
class DoubleIntgLinearForm : public BasicLinearForm
{protected :
 const OperatorOnUnknown* opu_p;  // operator on unknown (pointer)
 const GeomDomain* domainx_p; // first geometric domain (say x variable)
 const GeomDomain* domainy_p; // second geometric domain (say y variable)
\end{lstlisting}
\vspace{.2cm}
It provides some public constructors and some public accessors:
\vspace{.1cm}
\begin{lstlisting}
DoubleIntgLinearForm(const GeomDomain&, const GeomDomain&,const OperatorOnUnknown&);          
const OperatorOnUnknown* opu() const;
const GeomDomain* domainx() const;       
const GeomDomain* domainy() const;       
virtual BasicLinearForm* clone() const; 
virtual LinearFormType type() const;     
virtual ValueType valueType() const;     
virtual void print(std::ostream&)const;
\end{lstlisting} 
\vspace{.2cm}

\subsection{The \classtitle{SuLinearForm} class}

\class{BasicLinearForm} objects may be linearly combined to produce a linear combination of
\class{BasicLinearForm} objects stored as a list of pair of \class{BasicLinearForm} object
and a complex scalar in the \class{SuLinearForm} class. 
\vspace{.1cm}
\begin{lstlisting}
typedef std::pair<BasicLinearForm*,complex_t> lfPair;
class SuLinearForm
{protected:
   std::vector<lfPair> lfs_; //list of pairs of basic linear form and coefficient
   \ldots
\end{lstlisting}
\vspace{.1cm}
All the \class{BasicLinearForm} objects must have the same unknown ! It is the reason why this
class has no pointer to an unknown; it refers to the first basic linear form to get its unknown.\\

Because \class{BasicLinearForm} objects are copied for safety reason, this class provides
default and basic constructors but also a copy constructor, a destructor and the overload assignment
operator: 
\vspace{.1cm}
\begin{lstlisting}
SuLinearForm(){};                         
SuLinearForm(const SuLinearForm&);        
~SuLinearForm();                           
SuLinearForm& operator=(const SuLinearForm&);  
\end{lstlisting}
\vspace{.2cm}
It provides few accessors (some being const and non-const):
\vspace{.1cm}
\begin{lstlisting}
number_t size() const;    
std::vector<lfPair>& lfs();
const std::vector<lfPair>& lfs() const; 
lfPair& operator()(number_t n);
const lfPair& operator()(number_t n) const;
const Unknown* unknown() const;  
const Space* space() const;     
LinearFormType type() const;    
ValueType valueType() const;     
\end{lstlisting}
\vspace{.2cm}
It is possible to perform linear combination of linear combinations using the following overloaded
operators:
\vspace{.1cm}
\begin{lstlisting}
SuLinearForm& SuLinearForm::operator +=(const SuLinearForm&);   
SuLinearForm& SuLinearForm::operator -=(const SuLinearForm&);   
SuLinearForm& SuLinearForm::operator *=(const complex_t&);      
SuLinearForm& SuLinearForm::operator /=(const complex_t&); 
SuLinearForm operator-(const SuLinearForm&);                       
SuLinearForm operator+(const SuLinearForm&,const SuLinearForm&);   
SuLinearForm operator-(const SuLinearForm&,const SuLinearForm&);   
SuLinearForm operator*(const complex_t&,const SuLinearForm&);      
SuLinearForm operator*(const SuLinearForm&,const complex_t&);      
SuLinearForm operator/(const SuLinearForm&,const complex_t&); 
bool SuLinearForm::checkConsistancy(const SuLinearForm&) const; 
\end{lstlisting}
\vspace{.1cm}
The member function \cmd{checkConsistancy} performs a test to ensure that the unknown is always
the same.\\

Finally, there are some print facilities:
\vspace{.1cm}
\begin{lstlisting}
void SuLinearForm::print(std::ostream&)const;    
std::ostream& operator<<(std::ostream&,const SuLinearForm&); 
\end{lstlisting}

\subsection{The \classtitle{LinearForm} class}

The \class{LinearForm} class is the end user class dealing with general linear form, either
a single unknown linear form (a \class{SuLinearForm} object) or a multiple unknown linear
form (a list of \class{SuLinearForm} objects). In this class, a single unknown linear form
is a multiple unknown linear form with one unknown! The list of \class{SuLinearForm} objects
is stored in a map of \class{SuLinearForm}, indexed by the pointer to \class{SuLinearForm}
unknown:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] map}]
class LinearForm
{protected :
 std::map<const Unknown *,SuLinearForm> mlclf_;  //list of linear combinations of basic forms
 \ldots 
\end{lstlisting}
\vspace{.2cm}
To manage the map, the following aliases are defined:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] map}]
typedef std::map<const Unknown *,SuLinearForm>::iterator it_mulc;
typedef std::map<const Unknown *,SuLinearForm>::const_iterator cit_mulc;
\end{lstlisting}

\begin{focusbox}
When the \class{SuLinearForm} unknown is an unknown component, the \class{SuLinearForm} object
is attached to its parent item! In other words, a component unknown is not indexed in the map.
\end{focusbox}

This class provides only one constructor from a linear combination of forms:
\vspace{.1cm}
\begin{lstlisting}
LinearForm(const SuLinearForm&);  
\end{lstlisting}
\vspace{.2cm}
and offers some accessors and facilities:
\vspace{.1cm}
\begin{lstlisting}
bool isEmpty() const;                                
bool singleUnknown() const;   
SuLinearForm& operator[](const Unknown*);            //protected
const SuLinearForm& operator[](const Unknown*) const;//protected                       
const SuLinearForm& first() const;                   
LinearForm operator()(const Unknown&) const;         
BasicLinearForm& operator()(const Unknown&,number_t);
const BasicLinearForm& operator()(const Unknown&,number_t) const; 
\end{lstlisting}
\vspace{.2cm}
Besides, there are three fundamental external end user's function (\cmd{intg})
which constructs \class{LinearForm} object:
\vspace{.1cm}
\begin{lstlisting}
LinearForm intg(const GeomDomain&,const LcOperatorOnUnknown&);
LinearForm intg(const GeomDomain&,const OperatorOnUnknown&); 
LinearForm intg(const GeomDomain&,const Unknown&);           
LinearForm intg(const GeomDomain&,const GeomDomain&,const OperatorOnUnknown&);
LinearForm intg(const GeomDomain&,const GeomDomain&,const Unknown&);
\end{lstlisting}
\vspace{.2cm}
In order to construct any linear forms, the algebraic operators (\verb?+= -= *= /= + - * /?)
are overloaded for different objects:
\vspace{.1cm}
\begin{lstlisting}
LinearForm& LinearForm::operator +=(const LinearForm&);          
LinearForm& LinearForm::operator -=(const LinearForm&);          
LinearForm& LinearForm::operator *=(const complex_t&);           
LinearForm& LinearForm::operator /=(const complex_t&); 
LinearForm operator+(const LinearForm&,const LinearForm&);     
LinearForm operator-(const LinearForm&,const LinearForm&);   
LinearForm operator*(const complex_t&,const LinearForm&);      
LinearForm operator*(const LinearForm&,const complex_t&);      
LinearForm operator/(const LinearForm&,const complex_t&);          
\end{lstlisting}
\vspace{.2cm}
Finally, the class provides usual print facilities:
\vspace{.1cm}
\begin{lstlisting}
void LinearForm::print(std::ostream&)const;                     
std::ostream& operator<<(std::ostream&,const LinearForm&);  
\end{lstlisting}
\vspace{.2cm}

\subsection*{Example}

To end we show some characteristic examples. In these examples, \verb?u? is a scalar unknown,
\verb?w? is a vector unknown, \verb?f? either a scalar or a scalar function, \verb?h? either
a vector or a vector  function, \verb?g? a scalar or a scalar kernel and \verb?Omega?,\verb?Gamma?
\verb?Sigma? some geometric domains. 
\vspace{.1cm}
\begin{lstlisting}
LinearForm l1=intg(Omega,f*u);
l1+=intg(Omega,h|grad(u));
l1=intg(Omega,f*u)+intg(Omega,h|grad(u));  //equivalent
l1=l1-2*intg(Gamma,Sigma,g*u);
LinearForm l2=intg(Omega,f*div(w))+intg(Gamma,h|(w^n));
LinearForm l3=l1+l2;     //multiple unknowns
l3=intg(Omega,f*u)+intg(Omega,f*div(w))+intg(Omega,h|grad(u))
  +2*intg(Gamma,Sigma,g*u)+intg(Gamma,h|(w^n));  //same result   
\end{lstlisting}
\vspace{.2cm}
  
\displayInfos{library=form, header=LinearForm.hpp, implementation=LinearForm.cpp, test=test\_form.cpp,
header dep={config.h, utils.h}}

