%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex

\section{The \classtitle{Transformation} class}

The \class{Transformation} class is related to 1D-2D-3D transformation given, either by a matrix \(A\) and a vector \(b\)  (general linear transformation \(x\rightarrow Ax+b\)) or by some canonical transformations (may be chained). Up to now, only linear transformations are handled. They are listed in the following enumeration:
\vspace{.1cm}
\begin{lstlisting}
enum TransformType
{ _noTransform, _translation, _rotation2d, _rotation3d, _homothety, _scaling, _ptReflection, _reflection2d, _reflection3d, _composition, _explicitTf };
\end{lstlisting}
\vspace{.2cm}
When the transformation is a chain of some transformations, each of them are listed in a vector :
\vspace{.1cm}
\begin{lstlisting}
class Transformation
{
 protected:
  string_t name_;               //name of the transformation
  TransformType transformType_; //geometrical transformation
  Matrix<real_t> mat_;          //matrix of the transformation (when linear), default Id in R3
  Vector<real_t> vec_;          //vector of the transformation (when linear), default (0,0,0)
  bool is3D_;                   //true if a 3D transformation (set by buildMat)
 private:
  vector<const Transformation*> components_;   //chain rule Transformation if not empty
 \ldots
};
\end{lstlisting}
\vspace{.2cm}
It offers 3 constructors, a clone function and the destructor:
\vspace{.1cm}
\begin{lstlisting}
Transformation(const string_t& nam = "", TransformType trt=_noTransform); //basic constructor
Transformation(real_t a11,real_t a12, real_t a13, real_t a21,real_t a22, real_t a23,
               real_t a31,real_t a32, real_t a33, real_t b1=0, real_t b2=0, real_t b3=0);  
               //explicit constructor from matrix and vector coefficients
Transformation(const Transformation& t);    //copy constructor
virtual Transformation* clone() const;      //virtual clone constructor
virtual ~Transformation();                  //destructor
\end{lstlisting}
\vspace{.2cm}
some accessors to private or protected member data. Besides, some useful functions are provided:
\vspace{.1cm}
\begin{lstlisting}
 void buildMat();                           //build mat_ and vec_ related to the transformation
 virtual void print(std::ostream&) const;   //print Transformation
 virtual void print(PrintStream&)  const;
 friend ostream& operator<<(ostream&, const Transformation&);
\end{lstlisting}
\vspace{.2cm}
To deal with chain of transformation the following function are available, in particular the operators \verb*|*=| and \verb|*|:
\vspace{.1cm}
\begin{lstlisting}
friend Transformation toComposite(const Transformation&);
friend Transformation composeCompositeAndComposite(const Transformation&, const Transformation&);
friend Transformation composeCompositeAndCanonical(const Transformation&, const Transformation&);
friend Transformation composeCanonicalAndComposite(const Transformation&, const Transformation&);
friend Transformation composeCanonicalAndCanonical(const Transformation&, const Transformation&);
Transformation& operator*=(const Transformation&);    //to chain transformation
friend Transformation operator*(const Transformation&, const Transformation&);
\end{lstlisting}
\vspace{.2cm}
To get the action of the transformation on a \class{Point}, the following member function has to be called: 
\vspace{.1cm}
\begin{lstlisting}
 virtual Point apply(const Point&) const;   //apply the transformation
\end{lstlisting}
\vspace{.2cm}
All the canonical transformations (translation, rotation 2D, rotation 3D, \ldots see previous enumeration) are managed through inheriting classes from the \class{Transformation} class. For instance, translations are handled by the \class{Translation} class:
\vspace{.1cm}
\begin{lstlisting}
class Translation : public Transformation
{
 protected:
  vector<real_t> u_; //vector of the translation
 public:
  Translation(vector<real_t> u = vector<real_t>(3,0.));
  Translation(real_t ux, real_t uy = 0., real_t uz = 0.);
  virtual Transformation* clone() const;
  virtual ~Translation() {}
  const vector<real_t>& u() const;
  virtual const Translation* translation() const;
  virtual Translation* translation();
  virtual Point apply(const Point&) const;
  virtual void print(std::ostream&) const;           
  virtual void print(PrintStream& ) const;
};
\end{lstlisting}
\vspace{.2cm}
that proposes some specific constructors and accessors and provides the virtual member functions of the \class{Transformation} class.

\displayInfos{library=utils, header=Transformation.hpp, implementation=Transformation.cpp, test=unit\_Transmission.cpp, header dep={config.h,
Point.hpp, Matrix.hpp, Messages.hpp, Environnement.hpp}}

