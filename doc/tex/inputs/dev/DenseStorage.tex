%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Dense storages}

Dense storages are proposed as children of \class{MatrixStorage} class in order to be consistent with other storages. Contrary to the \class{Matrix} class (see \lib{utils} lib) which is a row dense storage, several dense storages are offered for an \(m\times n\) matrix (say \(A\)) : 
\begin{itemize}
\item the row dense storage (\class{RowDenseStorage} class)
\[(A_{11}, A_{12}, \ldots, A_{1n},A_{21}, A_{22}, \ldots, A_{2n}, \ldots, A_{m1}, A_{m2}, \ldots, A_{mn})\]
\item The column dense storage (\class{ColDenseStorage} class)
\[(A_{11}, A_{21}, \ldots, A_{2m},A_{12}, A_{22}, \ldots, A_{m2}, \ldots, A_{1n}, A_{2n}, \ldots, A_{mn})\]
\item The dual dense storage (\class{DualDenseStorage} class), diagonal part is first stored, then strict lower triangular part and strict upper triangular part:
\[(A_{11}, A_{22}, \ldots, A_{mm}, A_{21}, \ldots, A_{i1},\ldots, A_{i,i-1}, \ldots, A_{12}, \ldots, A_{1j}, \ldots, A_{j-1,j}, \ldots) \ \text{case }m=n\]
\item The symmetric dense storage (\class{SymDenseStorage} class), diagonal part is first stored, then strict lower triangular part :
\[(A_{11}, A_{22}, \ldots, A_{mm}, A_{21}, \ldots, A_{i1},\ldots, A_{i,i-1}, \ldots) \ \text{case }m=n\]
\end{itemize}

Dual storage works also for non-square matrices. Obviously, symmetric storage works only for square matrices! \\ \\
All dense storages inherit from the \class{DenseStorage} abstract class which inherits from  \class{MatrixStorage} abstract class. 

\subsection{The \classtitle{DenseStorage} class}

The \class{DenseStorage} class has no member attribute. There are some basic constructors just calling \class{MatrixStorage} constructors:
\vspace{.1cm}
\begin{lstlisting}
class DenseStorage : public MatrixStorage
{public :
  DenseStorage();
  DenseStorage(AccessType);
  DenseStorage(AccessType, Number);
  DenseStorage(AccessType, Number, Number); 
  virtual ~DenseStorage() {}
  \ldots
\end{lstlisting}
\vspace{.1cm} 
\class{DenseStorage} objects do not have to be instantiated. This class acts as an interface to particular dense storages and gathers all common functionalities of dense storages, some being virtual: 
\begin{itemize}
\item Public size functions: 
\vspace{.1cm}
\begin{lstlisting}
Number size() const; 
virtual Number lowerPartSize() const;
virtual Number upperPartSize() const;
\end{lstlisting}
\vspace{.1cm} 
\item Protected input/output functions (template line is written once):
\vspace{.1cm}
\begin{lstlisting}
template<typename Iterator>
void printScalarEntries(Iterator&, Number, Number, Number, Number, Number,
                        const String&, Number, std::ostream&) const;
void printScalarEntriesTriangularPart(Iterator& , Iterator&, Number, Number,
                                      Number, Number, Number, const String&,
                                      Number, std::ostream&) const;
void printMatrixEntries(Iterator&, Number, Number, const String&, Number, 
                        std::ostream&) const;
void printMatrixEntriesTriangularPart(Iterator&, Iterator&, Number, Number,
                                      const String&, Number, std::ostream&) const;
void loadFromFileDense(std::istream&, std::vector<Real>&, SymType, bool);
void loadFromFileDense(std::istream&, std::vector<Complex>&, SymType, bool);
void loadFromFileCoo(std::istream&, std::vector<Real>&, SymType, bool);
void loadFromFileCoo(std::istream&, std::vector<Complex>&, SymType, bool);
\end{lstlisting}
\vspace{.1cm} 
\item product of matrix and vector (template line is written once): 
\vspace{.1cm}
\begin{lstlisting}
template<typename MatIterator, typename VecIterator, typename ResIterator>
  void diagonalMatrixVector(MatIterator&, VecIterator&, ResIterator&, 
                            ResIterator&) const;
  void diagonalVectorMatrix(MatIterator&, VecIterator&, ResIterator&,
                            ResIterator&) const;
  void lowerMatrixVector(MatIterator&, VecIterator&, VecIterator&, 
                         ResIterator&, ResIterator&, SymType) const;
  void lowerVectorMatrix(MatIterator&, VecIterator&, VecIterator&,   
                         ResIterator&, ResIterator&, SymType) const;
  void upperMatrixVector(MatIterator&, VecIterator&, VecIterator&,
                         ResIterator&, ResIterator&, SymType) const;
  void upperVectorMatrix(MatIterator&, VecIterator&, VecIterator&,
                         ResIterator&, ResIterator&, SymType) const;
  void rowMatrixVector(MatIterator&, VecIterator&, VecIterator&, 
                       ResIterator&, ResIterator&)  const;
  void rowVectorMatrix(MatIterator&, VecIterator&, VecIterator&,
                       ResIterator&, ResIterator&)  const;
  void columnMatrixVector(MatIterator&, VecIterator&, VecIterator&,
                          ResIterator&, ResIterator&)  const;
  void columnVectorMatrix(MatIterator&, VecIterator&, VecIterator&, 
                          ResIterator&, ResIterator&)  const;
\end{lstlisting}
Note that the product of matrix and vector are performed either by row or column access, for diagonal part, lower part or upper part. They use iterators to by-pass the templated matrix values vector! With this trick, product algorithms are all located in \class{DenseStorage} class.
\item Addition of two matrices:
\vspace{.1cm} 
\begin{lstlisting}
template<typename Mat1Iterator, typename Mat2Iterator, typename ResIterator>
void sumMatrixMatrix(Mat1Iterator&, Mat2Iterator&, ResIterator&, ResIterator&) const;
\end{lstlisting}
\vspace{.1cm} 
\end{itemize}
\displayInfos{library=largeMatrix, header=DenseStorage.hpp, implementation=DenseStorage.cpp, test={test\_LargeMatrixDenseStorage.cpp}, header dep={MatrixStorage.hpp, config.h, utils.h}}

\subsection{\classtitle{Row/Col/Dual/SymDenseStorage} classes}

All child classes (\class{RowDenseStorage}, \class{ColDenseStorage}, \class{DualDenseStorage} and \class{SymDenseStorage}) of \class{DenseStorage} class have the same structure. We give here only the description of \class{RowDenseStorage} class. \\
\class{RowDenseStorage} class manages dense storage where matrix values are stored row by row in values vector. It has no member attribute and has constructors just calling \class{DenseStorage} constructors:
\vspace{.1cm}
\begin{lstlisting}
class RowDenseStorage : public DenseStorage
{public :
  RowDenseStorage();
  RowDenseStorage(Number); 
  RowDenseStorage(Number, Number);
  virtual ~RowDenseStorage() {}
  \ldots
\end{lstlisting}
\vspace{.2cm} 
The class provides all the virtual methods declared in \class{MatrixStorage} class :
\vspace{.1cm}
\begin{lstlisting}
Number pos(Number i, Number j, SymType s=_noSymmetry) const;
void positions(const std::vector<Number>&, const std::vector<Number>&,
               std::vector<Number>&, bool errorOn=true, SymType=_noSymmetry ) const;
void printEntries(std::ostream&, const std::vector<Real>&,
                  Number vb = 0, SymType sym = _noSymmetry) const;
template<typename M, typename V, typename R>
  void multMatrixVector(const std::vector<M>&, const std::vector<V>&,
                        std::vector<R>&) const; 
  void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, 
                        std::vector<R>&) const; 
template<typename T>
  void setDiagValueColDense(std::vector<T>& m, const T k);
template<typename M1, typename M2, typename R>
  void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const;
  \ldots
\end{lstlisting}
\vspace{.1cm} 
Only version of \cmd{multMatrixVector} and \cmd{multVectorMatrix} are written here for \verb?Real? type but versions for \verb?Complex?, \verb?Matrix<Real>?, \verb?Matrix<Complex>? and mixed types are also provided. \\
\\
\class{DualDenseStorage} and \class{SymDenseStorage} classes provides also the size of strict lower and strict upper triangular part:
\vspace{.1cm}
\begin{lstlisting}
Number lowerPartSize() const;
Number upperPartSize() const;
\end{lstlisting}
\vspace{.3cm} 
XXX=Row, Col, Dual or Sym
\displayInfos{library=largeMatrix, header=XXXDenseStorage.hpp, implementation=XXXDenseStorage.cpp, test={test\_LargeMatrixDenseStorage.cpp}, header dep={DenseStorage.hpp, MatrixStorage.hpp, config.h, utils.h}}


