%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Parametrization} \label{sec_Parametrization}
\subsection{ The \classtitle{Parametrization} class} 

The \class{Parametrization} class handles geometric maps that can describe either immersed curves in 2D/3D, surfaces in 3D or 1D/2D/3D mapping. \\
\begin{center}
	\includePict[width=12cm]{immersedSurface.png}
\end{center}

The \class{Parametrization} class manages mainly a \xlifepp geometry describing the domain of the parameter variables \(p\) (for instance \(t\), \((u,v)\) or \((x_1,x_2)\)) and a pointer to a function of the form:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]}]
Vector<real_t> (*par_fun)(const Point&, Parameters&, DiffOpType);  
\end{lstlisting}
\vspace{.2cm}
Generally the geometry is either a segment or a rectangle, but other geometries are allowed. The function describing the geometric map may deal with \class{Parameters} object (do not confuse with the parameter variables) and a differential operator
(\verb|_id, _d1,_d2,_d3,_d11, d12, \ldots|) that indicates which derivative is required. Offering first derivatives and second derivatives is not mandatory, but it allows accessing interesting geometrical quantities such as local length, curvature, \dots. 

\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]}]
Geometry* geom_p;  // pointer to the parametrized geometry (may be 0)
Geometry* geomSupport_p;  // pointer to the geometry support of the parameter
par_fun f_p;       // pointer to the parametrization function with parameters

string_t name;     //a parametrization name useful for printing
Parameters params; //optional parameter list passed to par_fun
dimen_t dimg;      //dimension of the geometry, set by init()
dimen_t dim;       //dimension of the arrival space, set by init()
\end{lstlisting}
\vspace{.2cm}
Geometrical quantities can be computed (if derivatives of the map are given) but, for more efficiency, they can be also given as \verb|par_fun| functions;
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]}]
par_fun curvature_p;  //pointer to the curvature function
par_fun length_p;     //pointer to the length function 
par_fun invParametrization_p;  //pointer to the inverse function
par_fun normal_p;     //pointer to the normal function 
par_fun tangent_p;    //pointer to the tangent function 
par_fun curabc_p;     //pointer to the curvilinear abcissa function
par_fun christoffel_p;// pointer to the Christoffel symbols function 
\end{lstlisting}
\vspace{.2cm}
\verb|par_fun| functions always return a real vector even if the natural quantity is a real scalar. Be cautious, the dimension of the returned quantities depends on the parametrization type:  
\begin{center}
\begin{tabular}{|c|c|c|c|}
	\hline
	&1D\(\rightarrow\) 2D & 1D\(\rightarrow\) 3D & 2D\(\rightarrow\) 3D \\
	\hline
	length & ds & ds & ds1,ds2\\
	curvature & c & c1,c2& c1,c2 \\
	normal & n & n1,n2& n \\
	tangent & t & t2& t1,t2 \\
	curvilinear & s & s & s1,s2\\
    Christoffel & & & \(\Gamma_{ij}^k,\ i,j,k=1,2\)\\
	inverse & x1& x1& x1,x2\\
	\hline
\end{tabular}
\end{center}
When these pointers are allocated they are used in priority. Some of these functions are generally easy to compute from the derivatives (curvature, length, normal, tangent) but it is not the case for the curvilinear abscissas (given by an integral of the length) and for the inverse map that requires to solve a non-linear equation. When the curvilinear abscissa is computed, to avoid heavy re-computation, curvilinear abscissas are computed once on a fine discretization and stored in a vector:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]}]
mutable Vector<Vector<real_t> > curabcs_; 
\end{lstlisting}
\vspace{.2cm}  
Up to now, the inverse map is not computed automatically. So if required, the function has to be given explicitly.\\

To offer the possibility to use symbolic functions instead of \verb|par_fun| functions, special \verb|par_fun| functions encapsulating \class{SymbolicFunction} are defined:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]}]
Vector<real_t> symbolic_f(const Point&, Parameters&, DiffOpType);        
Vector<real_t> symbolic_invParametrization(const Point&, Parameters&, DiffOpType);     
Vector<real_t> symbolic_length(const Point&, Parameters&, DiffOpType);   
Vector<real_t> symbolic_curvature(const Point&, Parameters&, DiffOpType); 
Vector<real_t> symbolic_curabc(const Point&, Parameters&, DiffOpType);    
Vector<real_t> symbolic_normal(const Point&, Parameters&, DiffOpType);    
Vector<real_t> symbolic_tangent(const Point&, Parameters&, DiffOpType);  
\end{lstlisting}
\vspace{.2cm}  
The following constructors address construction from explicit or implicit geometries and \verb|par_fun| functions:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]}]
Parametrization(); //default (all set to 0)
Parametrization(const Geometry&, par_fun, const Parameters&, 
                const string_t& na=""); //general case
Parametrization(real_t, real_t, par_fun, const Parameters&, 
                const string_t& na=""); //1D -> nD
Parametrization(real_t, real_t, real_t, real_t, par_fun, const Parameters&, 
                const string_t& na=""); //2D -> nD
\end{lstlisting}
\vspace{.2cm}  
and the following constructors deal with symbolic functions:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]}]
Parametrization(const Geometry&, const SymbolicFunction&, const Parameters&, 
                const string_t& na=""); // 1D->1D 
Parametrization(const Geometry&, const SymbolicFunction&, const SymbolicFunction&,
                const Parameters&, const string_t& na="");  // 1D/2D -> 2D
Parametrization(const Geometry&, const SymbolicFunction&, const SymbolicFunction&, 
                const SymbolicFunction&, const Parameters&, const string_t& na="");  
                // 1D/2D/3D -> 3D
Parametrization(real_t, real_t, const SymbolicFunction&, const Parameters&, 
                const string_t& na="");  //1D->1D 
Parametrization(real_t, real_t, const SymbolicFunction&, const SymbolicFunction&, 
                const Parameters&, const string_t& na="");// 1D->2D
Parametrization(real_t, real_t, const SymbolicFunction&, const SymbolicFunction&, 
                const SymbolicFunction&, const Parameters&, 
                const string_t& na="");// 1D->3D
Parametrization(real_t, real_t, real_t, real_t, const SymbolicFunction&, 
                const SymbolicFunction&, const Parameters&, 
                const string_t& na="");// 2D->2D
Parametrization(real_t, real_t, real_t, real_t, const SymbolicFunction&, 
                const SymbolicFunction&, const SymbolicFunction&, 
                const Parameters&, const string_t& na=""); //2D -> 3D
\end{lstlisting}
\vspace{.2cm} 
Besides, copy constructor, assign operator, destructor and some building tools are defined:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]}]
Parametrization(const Parametrization&);            // copy constructor
Parametrization& operator=(const Parametrization&); // assign operator
~Parametrization();                                 // destructor (clear pointers)

void init();            //initialization, set dim
void clearPointers();   //clear internal pointers (geometry and symbolic functions)
void copy(const Parametrization&); //copy tool
void buildSymbolic(const SymbolicFunction&); 
void buildSymbolic(const SymbolicFunction&, const SymbolicFunction&);
void buildSymbolic(const SymbolicFunction&, const SymbolicFunction&, 
                   const SymbolicFunction&);
\end{lstlisting}
\vspace{.2cm} 
Once \class{Parametrization} object is constructed, the optional \verb|par_fun| functions may be associated to the parametrization:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]}]
void setinvParametrization(par_fun f) {invParametrization_p=f;}
void setLength(par_fun f)    {length_p=f;}
void setCurvature(par_fun f) {curvature_p=f;}
void setCurabc(par_fun f)    {curabc_p=f;}
void setNormal(par_fun f)    {normal_p=f;}
void setTangent(par_fun f)   {tangent_p=f;}
\end{lstlisting}
\vspace{.2cm} 
In a same way, some symbolic functions may be associated to the parametrization:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]}]
void setinvParametrization(const SymbolicFunction&); 
void setinvParametrization(const SymbolicFunction&, const SymbolicFunction&); 
void setLength(const SymbolicFunction&);                              
void setCurvature(const SymbolicFunction&);                           
void setCurvatures(const SymbolicFunction&, const SymbolicFunction&);
void setCurabc(const SymbolicFunction&);       
void setCurabcs(const SymbolicFunction&, const SymbolicFunction&);  
void setNormal(const SymbolicFunction&, const SymbolicFunction&);    
void setNormal(const SymbolicFunction&, const SymbolicFunction&, 
               const SymbolicFunction&);  
void setNormals(const SymbolicFunction&, const SymbolicFunction&, 
                const SymbolicFunction&, const SymbolicFunction&, 
                const SymbolicFunction&, const SymbolicFunction&);  
void setTangent(const SymbolicFunction&, const SymbolicFunction&);           
void setTangent(const SymbolicFunction&, const SymbolicFunction&,
                const SymbolicFunction&); 
void setTangents(const SymbolicFunction&, const SymbolicFunction&,
                 const SymbolicFunction&, const SymbolicFunction&, 
                 const SymbolicFunction&,const SymbolicFunction&);
\end{lstlisting}
\vspace{.2cm}
The following accessors are available:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]}] 
Geometry& geometry() const {return *geom_p;};
RealPair bounds(VariableName v) const; //geometry bounds in direction (_x1,_x2,_x3)
\end{lstlisting}
\vspace{.2cm} 
The class provides methods to compute the parametrization map (using the operator \verb|()|) and its inverse if it is defined:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]}] 
Point operator()(const Point&, DiffOpType d=_id) const;
Point operator()(real_t t, DiffOpType d=_id) const; // 1D shorctut
Point operator()(real_t u, real_t v, DiffOpType d=_id) const; // 2D shorcut
Point toParameter(const Point&) const;     
real_t toRealParameter(const Point&) const; //1D shortcut
\end{lstlisting}
\vspace{.2cm} 
In the following \(f(p)=(f_1(p),f_2(p),[f_3(p)])\) denotes the point given by the parametrization at \(p\).
For 1D parametrization (curve),  the local "lengths" \(ds\) is defined as
\[ds(p)=\sqrt{(f'_1(p))^2+(f'_2(p))^2+[(f'_3(p))^2]}\]
and for 2D parametrization (surface) the two "lengths" are defined as
\[
\begin{array}{l}
ds_1(p)=\sqrt{(d_1f_1(p))^2+(d_1f_2(p))^2+(d_1f_3(p))^2}\\[2mm]
ds_2(p)=\sqrt{(d_2f_1(p))^2+(d_2f_2(p))^2+(d_2f_3(p))^2}
\end{array}
\]

The class provides length functions with different arguments:

\begin{lstlisting}[deletekeywords={[3]}] 
Vector<real_t> lengths(const Point&, DiffOpType =_id) const;        //lengths 
Vector<real_t> lengths(real_t t, DiffOpType d=_id) const;           //1D shortcut
Vector<real_t> lengths(real_t u, real_t v, DiffOpType d=_id) const; //2D shortcut
real_t length(const Point& P, DiffOpType d =_id) const;             //first length
real_t length(real_t t, DiffOpType d =_id) const;                   //1D shortcut 
real_t length(real_t u, real_t v, DiffOpType d =_id) const;         //2D shortcut
real_t bilength(const Point& P, DiffOpType d =_id) const;           //second length
real_t bilength(real_t t, DiffOpType d =_id) const;                 //1D shortcut 
real_t bilength(real_t u, real_t v, DiffOpType d =_id) const;       //2D shortcut
\end{lstlisting}

This scheme of member functions is common to all geometric functions: 's' suffix for the general function, no suffix for the first value, 'bi' prefix for the second value if it is relevant; same 1D, 2D shortcuts. Shortcut versions are no longer written in the following. \\
 
For 1D parametrization, the curvature is given by
\[c(p)=\frac{\|f''(p)\times f'(p)\|}{\|f'(p)\|^3}\]
while for 2D parametrization (in \(\mathbb{R}^3\)), the main curvatures are the eigenvalues \((c_1(p),c_2(p))\) of the Weingarten's matrix
\[
W(p)=\frac{1}{EG-F^2}
\left[ \begin{array}{cc}
MF-LG & NF-LG\\
LF-ME & MF-NE
\end{array}\right]
\]

where \(E=(d_1f(p))^2\), \(F=d_1f(p)d_2f(p)\), \(G=(d_2f(p))^2\), \(L=d_{11}f(p).n\), \(M=2d_{12}f(p).n\) and \(N=d_{22}f(p).n\). The Gauss curvature and the mean curvature are given by

\[
c_G(p)=c_1(p)c_2(p) \text{ and } c_m(p)=\frac{1}{2}(c_1(p)+c_2(p)).
\]

\begin{lstlisting}[deletekeywords={[3]}]
Matrix<real_t> weingarten(const Point& t) const;                //Weingarten matrix
Vector<real_t> curvatures(const Point&, DiffOpType =_id) const; //curvatures (1/2)
real_t curvature(const Point& P, DiffOpType d =_id) const;      //first curvature
real_t bicurvature(const Point& P, DiffOpType d =_id) const;    //second curvature
real_t gausscurvature(const Point& P, DiffOpType d =_id) const; //Gauss curvature
real_t meancurvature(const Point& P, DiffOpType d =_id) const;  //mean curvature
\end{lstlisting}

The curvilinear abscissa is defined for 1D parametrization as (\(p\) is a scalar)
\[
s(p) =\int_0^p ds(t)
\]

and for 2D parametrization (\(p=(u,v)\))

\[
\begin{array}{l}
\displaystyle s_1(p)=\int_0^u ds_1(t,v)dt\\[2mm]
\displaystyle s_2(p)=\int_0^v ds_2(u,t)dt.
\end{array}
\]

\begin{lstlisting}[deletekeywords={[3]}]
Vector<real_t> curabcs(const Point&, DiffOpType =_id) const; //cur. abc. (1/2)
real_t curabc(const Point& P, DiffOpType d =_id) const;      //first cur. abc.
real_t bicurabc(const Point& P, DiffOpType d =_id) const;    //second cur. abc.
\end{lstlisting}

For 1D parametrization, the tangent vector is given by 

\[
T=\frac{f'(p)}{\|f'(p)\|}
\]

the first normal and the binormal (Frenet definition) by:

\[
N=d_sT(P)/c(p)\text{ and } bN=T\times N.
\]

For 2D parametrization, the normal is given by

\[
N=\frac{d_1f(p)\times d_2f(p) }{\|d_1f(p)\times d_2f(p))\|}
\]

and the tangent and bitangent vectors by:

\[
T=\frac{d_1f(p)}{\|d_1f(p)\|} \text{ and } bT=T\times N.
\]

The following member functions are defined:

\begin{lstlisting}[deletekeywords={[3]}]
Vector<real_t> normals(const Point&, DiffOpType =_id) const;       //normal vectors
Vector<real_t> normal(const Point& P, DiffOpType d=_id) const;     //first normal
Vector<real_t> binormal(const Point& P, DiffOpType d=_id) const;   //second normal
Vector<real_t> tangents(const Point&, DiffOpType =_id) const;      //tangent vectors
Vector<real_t> tangent(const Point& P, DiffOpType d=_id) const;    //first tangent
Vector<real_t> bitangent(const Point& P, DiffOpType d=_id) const;  //second tangent
\end{lstlisting}

It provides also the jacobian matrix (say \(J\)), the metric tensor (\(G=J^tJ\), symmetric matrix) stored as a 3[6]-vector:
\[(G_{11},G_{21},G_{22},[,G_{31},G_{32},,G_{33}])\]
and the Christoffel symbols (immersed 2D surface, parametrized by \((u,v)\)), stored as an 8-vector:

\[
(\Gamma_{11}^1,\Gamma_{12}^1,\Gamma_{21}^1,\Gamma_{22}^1,\Gamma_{11}^2,\Gamma_{12}^2,\Gamma_{21}^2,\Gamma_{22}^2)
\]

\begin{lstlisting}[deletekeywords={[3]}]
Matrix<real_t> jacobian(const Point& t, DiffOpType d=_id) const;      //jacobian matrix
Vector<real_t> metricTensor(const Point& t, DiffOpType d=_id) const;  //metric tensor
Vector<real_t> christoffel (const Point& t, DiffOpType d=_id) const;  //Christoffel symbols
\end{lstlisting}

When dealing with curve on immersed surface (\(s\rightarrow \gamma(s)=\varphi(u(s),v(s))\)), the {\em  normal curvature} at \(s\) is defined as
\[
\begin{array}{ll}
  \kappa_n&=(\ddot{\gamma}(s)|n),\quad (n \text{ a normal to the surface})\\[1mm]
          &\displaystyle =\frac{L\dot{u}^2+2M\dot{u}\dot{v}+N\dot{u}^2} {E\dot{u}^2+2F\dot{u}\dot{v}+G\dot{u}^2}
\end{array}
\]

with \(E,F,G\) the coefficients of the 1-form and \(L,M,N\) the coefficients of the 2-form. Be careful, the sign of normal curvature depends on the choice of the surface normal. As the normal curvature depends only on the tangent \(d=\dot{\gamma}(s)\) of the curve at \(\gamma(s)\), a more general tools is provided computing the normal curvature at \(\varphi(u,v)\) relatively to the direction \(d\) (\(\dot{u}=(d|d_v\varphi\times n),\ \dot{v}=(d|d_u\varphi\times n)\)):

\begin{lstlisting}[deletekeywords={[3]}]
real_t normalCurvature(const Point& uv, const Vector<real_t>& d) const;    //normal curvature relatively to d
Vector<real_t> curvatures(const Point& uv, const Vector<real_t>& d) const; //Gauss, mean and normal curvature
\end{lstlisting}

\begin{center}
\includePict[width=5cm]{normalcurvature.png}
\end{center}

Up to now, the geodesic curvature \(\kappa_g\) is not available. Note that the curve curvature \(\kappa\) is related to the normal and geodesic curvatures by \(\kappa^2=\kappa_g^2+\kappa_n^2\).\\

Finally, the \class{Parametrization} provides some printing tools:

\begin{lstlisting}[deletekeywords={[3]}]
void print(std::ostream&) const; 
void print(PrintStream& os) const;
friend ostream& operator<< (std::ostream&, const Parametrization&);
\end{lstlisting}

\subsection{Parametrization of a piecewise geometry}

A piecewise geometry is a \class{Geometry} object that have several geometry components. As a consequence, a parametrization of such object consists in the collection of parametrizations of each geometry component. To deal with, a \class{PiecewiseParametrization}, inheriting of \class{Parametrization}, is provided. It handles 1D case (collection of arcs), 2D case (collection of surface patches) but not yet the 3D case (collection of volume patches)!

\begin{lstlisting}[deletekeywords={[3]}]
class PiecewiseParametrization : public Parametrization
{ public:
  vector<Point> vertices; 
  map<Parametrization*,std::vector<number_t> > vertexIndexes; 
  map<Parametrization*, std::vector<std::pair<Parametrization*,number_t> > > neighborParsMap; 
\ldots};
\end{lstlisting}

with 

\begin{itemize}
\item The vector \var{vertices} collecting all the vertices (global numbering)
\item The map \var{vertexIndexes} collecting for each parametrization the local vertex numbers (relatively to global numbering)
\item The map \var{neighborParsMap} collecting for each parametrization and each of its sides, the neighbor parametrization (if exists) and its relative side number (the side numbering being the following: side \(1\rightarrow v=0\), side \(2\rightarrow u=1\), side \(3\rightarrow v=1\), side \(4\rightarrow u=0\)).
\end{itemize}

\begin{center}
\includePict[width=10cm]{piecewiseParametrization.png}
\end{center}

On the example made of surface patches:
{\small
\[
\begin{array}{ll}
    \text{\var{vertices}:}& (M_1,M_2,M_3,M_4,M_5,M_6,M_7,M_8)\\
    \text{\var{vertexIndexes}:}& [(1,2,3,4),(1,4,6,5),(4,4,7,6),(8,7,4,3)]\\
    \text{\var{neighborParsMap}:} 
    &[ (<0,0>,<0,0>,<\mathcal{P}_4,3>,<\mathcal{P}_2,1>),(<\mathcal{P}_1,4>,<\mathcal{P}_3,4>),<0,0>,<0,0>),\\
    &\ (<0,0>,<\mathcal{P}_4,2>,,<0,0>,<\mathcal{P}_2,2>),(<0,0>,<\mathcal{P}_3,2>,<\mathcal{P}_1,3>,<0,0>)
    ]
\end{array}
\]
}

The \var{neighborParsMap} map allows to travel the piecewise parametrization by crossing patch interfaces. In 2D case, the piecewise parametrization is based on "quadrangular" patches, but they can be degenerated (triangular patches); but not fully degenerated (line).\\
Note that this piecewise parametrization is not global \(C^0\). It is quite easy to produce a  global \(C^0\) parametrization in 1D case, but it is another story in 2D case!\\

In addition to the \class{Parametrization} member functions, the \class{PiecewiseParametrization}  class provides some tools to build the structure \var{vertices}, \var{vertexIndexes} and \var{neighborParsMap} from the sides map (for each side \(s\) of a domain, the list of all geometries connected to the side \(s\)):

\begin{lstlisting}[deletekeywords={[3]}]
void buildNeighborParsMap(map<set<number_t>, list<std::pair<Geometry*, number_t> > >& sidemap);
void buildVertices();
\end{lstlisting}

some tools to locate the parametrization patch and computes various quantities

\begin{lstlisting}[deletekeywords={[3]}]
Parametrization* locateParametrization(const Point& p, const Point& dp, Parametrization* par0, 
                                       Point& q, real_t& d) const;   
Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffComputation dc, 
                                  DiffOpType d=_id) const;
Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const;
Point toParameter(const Point& p) const;
virtual Vector<real_t> lengths(const Point& P, DiffOpType d=_id) const;
virtual Vector<real_t> curvatures(const Point& P, DiffOpType d =_id) const;
\end{lstlisting}

and finally to print information related to:

\begin{lstlisting}[deletekeywords={[3]}]
void print(std::ostream&) const; 
void print(PrintStream& os) const;
\end{lstlisting}

\displayInfos{library=geometry, header=Parametrization.hpp, implementation=Parametrization.cpp, test=test\_Parametrization.cpp,
header dep={config.h, utils.h}}

