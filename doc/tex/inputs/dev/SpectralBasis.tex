%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Spectral basis management}

\subsection{The \classtitle{SpectralBasis} class}

For spectral finite elements, the basis is defined by a finite set of functions globally defined over a geometric domain. The \class{SpectralBasis} class is devoted to this set of functions over a spectral space. This class is defined as follows:

\vspace{0.1cm}
\begin{lstlisting}
class SpectralBasis
{
  protected :
    Number numberOfFun_;        //!< number of function in the basis
    Number dimFun_;             //!< dimension of the spectral functions
    const Domain* domain_;      //!< geometric domain support
    ValueType returnedType_;    //!< type of returned value (one among _real, _complex)
    StrucType returnedStruct_;  //!< structure of returned value (one among _scalar, _vector)
    FuncFormType funcFormType_; //!< type of basis functions (_analytical,_interpolated)
};
\end{lstlisting}
\vspace{0.1cm}

The \class{SpectralBasis} class provides:
\begin{itemize}
\item One constructor
\vspace{0.1cm}
\begin{lstlisting}
SpectralBasis(Number n, Number d, const Domain& g, ValueType r = _real, StrucType s = _scalar);
\end{lstlisting}
\vspace{0.1cm}
\item Some accessors
\vspace{0.1cm}
\begin{lstlisting}
Number numberOfFun() const;
Dimen dimFun() const;
Number dimSpace() const;
FuncFormType funcFormType() const;
\end{lstlisting}
\vspace{0.1cm}
\item some functions about the basis functions :
\vspace{0.1cm}
\begin{lstlisting}
virtual Function& functions(); //!< return basis function object (only for SpectralBasisFun)
template<typename T>
T& functions(Number, const Point&, T&); //!< compute n-th function
template<typename T>
Vector<T>& functions(const Point& P, Vector<T>& res); //!< compute all functions
\end{lstlisting}
\vspace{0.1cm}
\item some print functions
\vspace{0.1cm}
\begin{lstlisting}
virtual void print(std::ostream&) const = 0;
\end{lstlisting}
\vspace{0.1cm}
\end{itemize}

As you may have noticed, the \class{SpectralBasis} class is an abstract class. Indeed, a spectral basis can be defined with an analytical expression or by a set of interpolate functions. That is why the \class{SpectralBasis} class has 2 children : \class{SpectralBasisFun} and \class{SpectralBasisInt}.

\subsection{The \classtitle{SpectralBasisFun} class}

The \class{SpectralBasisFun} class is dedicated to a spectral basis defined with an analytical expression, namely a \class{Function} object.
\vspace{0.1cm}
\begin{lstlisting}
class SpectralBasisFun : public SpectralBasis
{
  protected :
    Function functions_;
  public :
    SpectralBasisFun(Domain&, Function&, Number, Number); //!< constructor
};
\end{lstlisting}
\vspace{0.1cm}

\subsection{The \classtitle{SpectralBasisInt} class}

The \class{SpectralBasisInt} class is dedicated to a spectral basis defined by a set of interpolate functions, namely a set of \class{TermVector} objects:
\vspace{0.1cm}
\begin{lstlisting}
class SpectralBasisInt : public SpectralBasis
{
  protected :
    std::vector<TermVector*> functions_;
  public :
    SpectralBasisInt(Domain&, Number, Number);  //!< constructor
};
\end{lstlisting}
\vspace{0.1cm}

\displayInfos{library=space, header=SpectralBasis.hpp, implementation=SpectralBasis.cpp, test=test\_Space.cpp,
header dep={config.h, utils.h, geometry.h}}
