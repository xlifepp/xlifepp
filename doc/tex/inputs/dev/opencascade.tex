%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{OpenCascade extension}
Open Cascade Technology (OCT) \footnote{Trademark @Open Cascade, https://www.opencascade.com/}  is a third party open source library dedicated to 3D CAD data. It is a powerful library dealing with canonical geometries but providing complex geometrical operations (union, intersection, difference of geometries, fillet, chamfer, \ldots). The standard geometry engine of \xlifepp provides only union or difference in the case of one geometry included in another one (if detection is easy). So to go further, \xlifepp provides an interface to OCT. Obviously, OCT must be installed and activated in \xlifepp (cmake option).\\

\begin{focusbox}
When OCT is activated, the macro \verb|XLIFEPP_WITH_OPENCASCADE| is defined to protect any OCT specific code.
\end{focusbox}
\vspace{2mm}
OCT library provides a very large number of classes, but the central one is {\class{TopoDS\_Shape} encapsulating any geometry, no matter how complex. Any OCT shape is described following the boundary representation (brep):  hierarchical representation of vertices, edges(curve), wires (edge union), faces (from wire or edges), shells (face union), solids (from shell or faces), solids union. We do not give here more details, but developer interesting in OCT interface, should have look the OCT documentation.\\
	
The purpose of the OpenCascade interface is to provide to \xlifepp some new geometries that the standard geometrical engine is not able to manage (in particular intersection of canonical geometries). Besides, OCT geometries must be accessible in \xlifepp for some calculus involving the parametrization of geometries (mesh being not sufficient).

\subsection{The \classtitle{OCData} class}

To any \class{Geometry} may be attached an \class{OCData} object (pointer to):
	
\vspace{.1cm}
\begin{lstlisting}
#ifdef XLIFEPP_WITH_OPENCASCADE
 protected:
 mutable OCData *ocData_; //class handled main OC TopoDS_Shape and related data
#endif
\end{lstlisting}
\vspace{.1cm}
with the \class{OCData} class:
\vspace{.1cm}
\begin{lstlisting}
class OCData
{ private:
  TopoDS_Shape* OCShape_;    //main OC object representing any shape
  TDocStd_Document* OCDoc_;  //additional properties related to OC shape
  ...
};
\end{lstlisting}
\vspace{1mm}
\class{TDocStd\_Document} is an OCT class managing additional properties of \class{TopoDS\_Shape} objects. In the context of \xlifepp, it is used to relate discretization parameters (hstep, nnode) and domain or side domain names to \class{TopoDS\_Shape} objects.
\begin{focusbox}
The \class{OCData} object is not allocated until a geometric function requiring OCT capabilities has been invoked.
\end{focusbox}
\vspace{2mm}

The \class{OCData} class provides a basic constructor, a constructor from \class{Geometry}, the copy constructor, the assign operator and the destructor:
\vspace{1mm}
\begin{lstlisting}
OCData(TopoDS_Shape* ocshape=0, TDocStd_Document* ocdoc=0); 
OCData(const Geometry& shape); 
OCData(const OCData&); 
OCData& operator=(const OCData&);  
~OCData();
\end{lstlisting}
\vspace{1mm}
The constructor from \class{Geometry} is the most important. It calls the member function
\vspace{1mm}
\begin{lstlisting}
void buildOCShape(const Geometry&);   
\end{lstlisting}
\vspace{1mm}
that really creates OCT shapes regarding \class{Geometry} objects. This function creates the \class{TDocStd\_Document} too using the other functions
\vspace{1mm}
\begin{lstlisting}
void buildOCDocG(const Geometry&);    
void buildOCDoc(const Curve&);   
\end{lstlisting}
\vspace{1mm}
\begin{focusbox}
	Tracking OCT shapes to relate them to names and discretization properties is a hard work. It should be revisited to simplify it!
\end{focusbox}
The \class{OCData} class provides also some accessors, print tools and export functions to \textit{brep} and \textit{step} files:
\vspace{1mm}
\begin{lstlisting}
[const] TopoDS_Shape& topoDS() [const];
[const] TDocStd_Document& OCDoc() [const];
ShapeDocPair asShapeDocPair() const;
bool isNull() const {return OCShape_==0;}
bool hasDoc() const {return OCDoc_!=0;}
void transform(const Transformation& t); 
void print(std::ostream&, bool recursive=true) const; 
void print(CoutStream&, bool recursive=true) const; 
void printDoc(std::ostream& out) const; 
void saveToStep(const string_t&) const; 
void saveToBrep(const string_t&) const; 
\end{lstlisting}
\vspace{1mm}

\subsection{Meshing OCT geometries}
As \gmsh deals with \textit{brep} files (using OCT too), \xlifepp exports OCT geometries to \gmsh in the following way:
\begin{itemize}
\item Export the OCT geometry to the \textit{brep} file {\it xlifepp.brep}
\item Create the geo script file {\it xlifepp.geo} containing as first line \verb|Merge "xlifepp.brep";| and then other geo commands relating to step discretization and domain naming. It looks like
\small
\begin{verbatim}
	Merge "xlifepp.brep";
	Characteristic Length{1} = 0.1;
	Characteristic Length{2} = 0.1;
	Characteristic Length{3} = 0.1;
	Characteristic Length{4} = 0.1;
	Physical Line("Gamma") = {2,4};
	Physical Line("Sigma1") = {1};
	Physical Line("Sigma2") = {3};
	Physical Surface("Square") = {1};
\end{verbatim}
\normalsize
This job is done by the function
\begin{lstlisting}
	void saveToBrepGeo(Geometry&, ShapeType, number_t, std::set<MeshOption>, 
							const string_t& geofile, const string_t& brepfile) ; 
\end{lstlisting}
\item Run \gmsh with the script  {\it xlifepp.geo} in non-interactive mode, creating the mesh file {\it xlifepp.msh},
\item Load the mesh file {\it xlifepp.msh} into a \class{Mesh} object.
\end{itemize}
The last two steps are not specific to the OCT extension.\\

To invoke the gmsh OCT meshing process, users have to specify the option \var{\_gmshOC} as mesh generator in Mesh constructors. Note, that if no OCT functions has been previously called, the mesh command will construct recursively all OC objects related to all geometries involved.\\
\begin{warningbox}
In the last versions of OpenCascade, the \textit{brep} file header as slightly changed and is no longer recognized by \gmsh (at least until the version 4.11.1). Loading \textit{brep} files seems ok but \gmsh does nothing! So, for the moment, till \gmsh fixes the problem, the function \verb?void changeBrepHeader(const string_t& fn)? is applied in order to restore the old \textit{bprep} file header.  
\end{warningbox}

\subsection{OCT interactions in \classtitle{Geometry} class}
As mentioned above, \class{Geometry} object may instantiate an \class{OCData} object. There are particular member functions to interact with this \class{OCData} object. They are protected with the macro protection \verb|XLIFEPP_WITH_OPENCASCADE| and are all implemented in the file {\it opencascade.cpp} for  header dependence reasons:
\vspace{1mm}
\begin{lstlisting}
public:
 void initOC();                        
 void clearOC();                      
 void cloneOC(const Geometry&);       
 [const] OCData& ocData() [const];   //built on fly if not allocated
 const TopoDS_Shape& ocShape() const;//built on fly if not allocated
 bool hasDoc() const;
 void buildOCData() const;           //built on fly if not allocated
 void ocTransformP(const Transformation&);
 
 void printOCInfo(CoutStream&) const;  
 void printOCInfo(std::ostream&) const;
 void loadFromBrep(const string_t& file, const Strings& domNames=Strings(),
                   const Strings& sideNames=Strings(), const Reals& hsteps=Reals());   
                     
 void setOCName(OCShapeType oct,const std::vector<number_t>&, const string_t&);  
 void setOCName(OCShapeType oct,number_t num, const string_t& na);
 void setOCHstep(const std::vector<number_t>& nums, real_t hstep);    
 void setOCHstep(number_t num, real_t hstep);                         
 void setOCNnode(const std::vector<number_t>& nums, number_t nnode);   
 void setOCNnode(number_t num, number_t nnode);                        

 Geometry& operator^=(const Geometry& g); //!< build of common part
 friend Geometry operator^(const Geometry& g1, const Geometry& g2);  
\end{lstlisting}
\vspace{3mm}
\begin{warningbox}
If you intend to add new functionalities involving new OCT classes, do not forget to add the corresponding OCT include files to the {\it opencascade.h} file.
\end{warningbox}

The following example, not available using standard machinery, illustrates how users deal with OCT extension :
\vspace{1mm}
\begin{lstlisting}
Sphere S1(_center=Point(1.,0,0.),_radius=1,_hsteps=0.02,
          _domain_name="sphere",_side_names="Sigma");
Sphere S2=translate(S1,1.5,0.,0.);
RevCylinder C(_center1=Point(1,0.,0),_center2=Point(2.5,0.,0.),_radius=0.3,_hsteps=0.02,
              _domain_name="cylinder",_side_names="Gamma");
Geometry G = (S1^S2)-C;
Mesh M(G,tetrahedron,1,_gmshOC);
\end{lstlisting}
\vspace{2mm}
\begin{figure}[H]
	\centering
	\includePict[width=8cm]{OCsphere&sphere-cyl.png}
\end{figure}

\displayInfos{library=geometry, header=OpenCascade.hpp, implementation=OpenCascade.cpp, test=test\_OpenCascade.cpp,
header dep={config.h, utils.h, opencascade.h, geometry1D.hpp, geometry2D.hpp, geometry3D.hpp, Mesh.hpp}}

