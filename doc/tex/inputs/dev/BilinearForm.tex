%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Bilinear forms}

Bilinear forms are managed using 
\begin{itemize}
\item \class{BasicBilinearForm} abstract class of basic linear form,
\item \class{IntgBilinearForm} class inheriting from \class{BasicBilinearForm} and dealing
with integral form,
\item \class{DoubleIntgBilinearForm} class inheriting from \class{BasicBilinearForm} and
dealing with double integral forms,
\item\class{UserBilinearForm} class  inheriting
from \class{BasicBilinearForm} and dealing with bilinear forms defined from a user function building elementary matrices,
\item \class{SuBilinearForm} class storing linear combination of \class{ BasicBilinearForm}
objects having the same unknown,
\item \class{BilinearForm} class storing a list of \class{SuBilinearForm} objects using a
map indexed by unknowns. 
\end{itemize}

\subsection{The \classtitle{BasicBilinearForm} class}

The \class{BasicBilinearForm} class is an abstract class having only the unknowns as attribute
and their associated accessors:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] up, vp}]
class BasicBilinearForm
{protected:
   const Unknown* u_p;             //pointer to unknown u
   const Unknown* v_p;             //pointer to unknown v (test function)
 public :
   const Unknown& up() const;      
   const Unknown& vp() const;
   \ldots      
\end{lstlisting}
\vspace{.1cm}
The 'u' letter always refers to the left unknown of the bilinear form and the 'v' letter to
the right unknown (test function) of the bilinear form. In matrix representation 'u' stands
for columns and 'v' for rows.\\
 
The child classes inheriting from \class{BasicBilinearForm} have to provide the following
member functions:
\vspace{.1cm}
\begin{lstlisting}
virtual ~BasicBilinearForm(){};              
virtual BasicBilinearForm* clone() const =0; 
virtual LinearFormType type() const =0;    
virtual ValueType valueType() const=0;       
virtual void print(std::ostream&) const =0;
\end{lstlisting}
\vspace{.1cm}
The \verb?virtual clone()? function is used to construct a copy of the child objects from parent.

\subsection{The \classtitle{IntgBilinearForm} class}

The \class{IntgBilinearForm} class handles linear forms defined by a (single) integral over
a geometric domain:
\[
\int_{D} \mathcal{L}_1(u) op \mathcal{L}_2(v)\]
where \(D\) is a geometric domain (\class{Domain} class) and \({\cal L}_1\), \({\cal L}_2\)  are
linear operators (\class{OperatorOnUnknown} class) acting on unknowns \(u\) and \(v\) (\class{Unknown} class) and \(op\) is an algebraic product operator. In case of mesh domain, it handles also the quadrature rules, one rule by shape of elements.\\
This class is defined as follows:
\vspace{.1cm}
\begin{lstlisting}
class IntgBilinearForm : public BasicBilinearForm
{protected :
 const GeomDomain* domain_p; //geometric domain of the integral (pointer)
 const OperatorOnUnknown* opu_p; //operator on unknown u (pointer)
 const OperatorOnUnknown* opv_p; //operator on unknown v (pointer)
 AlgebraicOperator aop_;         //algebraic operation between operators
 map<Number, Quadrature*> quadratures_; //!< pointers to quadrature rules 
 \ldots
\end{lstlisting}
\vspace{.1cm}
Note that the \verb?quadratures_? map may be empty. This, is the case when the domain is not of type mesh domain. Generally, the elements of a mesh domain have the same shape, but it may not.
 It is the reason why a map is used to store the quadratures.\\

It has one basic constructor, some public accessors and utilities:
\vspace{.1cm}
\begin{lstlisting}
IntgBilinearForm(const GeomDomain&,const OperatorOnUnknown&, AlgebraicOperator,
                 const OperatorOnUnknown&, QuadRule = _defaultRule, Number =0) 
const OperatorOnUnknown* opu() const; 
const OperatorOnUnknown* opv() const;        
const GeomDomain* domain() const;
virtual BasicBilinearForm* clone() const;       
virtual LinearFormType type() const         
virtual ValueType valueType() const        
void setQuadrature(QuadRule , Number);
virtual void print(std::ostream&) const;   
\end{lstlisting}    
\vspace{.1cm}
The \cmd{setQuadrature} function set the quadrature pointers from a \class{QuadRule} (enumeration of type of quadrature rule) and a quadrature number (degree or order of quadrature rule).
For a full description of quadrature rules see section \ref{quadrature_section}. When \class{QuadRule} is set to \verb?_defaultRule? in constructor, the 'best' rule is chosen regarding the 
shape element and the degree of integrand (see \cmd{Quadrature::bestQuadRule} function).

\subsection{The \classtitle{DoubleIntgBilinearForm} class}

The \class{DoubleIntgBilinearForm} class handles linear forms defined by a double integral
over a product of geometric domains:   
\[
\int_{D_x}\int_{D_y} {\cal L}(u)\]
where \(D_x\), \(D_y\) are geometric domain (\class{GeomDomain} class) and \({\cal L}\) a linear operator
(\class{OperatorOnUnknown} class) acting on unknown \(u\) (\class{Unknownn} class).\\
Except, there are two geometric domains, this class is very similar to the \class{IntgBilinearForm}
class:
\vspace{.1cm}
\begin{lstlisting}
class DoubleIntgBilinearForm : public BasicBilinearForm
{protected :
 const GeomDomain* domainx_p;     //first geometric domain (say x variable)
 const GeomDomain* domainy_p;     //second geometric domain (say y variable)
 const OperatorOnUnknown* opu_p; //operator on unknown u (pointer)
 const OperatorOnUnknown* opv_p; //operator on unknown v (pointer)
 \ldots
\end{lstlisting}
\vspace{.2cm}It provides a basic constructor and some public accessors:
\vspace{.1cm}
\begin{lstlisting}
DoubleIntgBilinearForm(const GeomDomain&,const GeomDomain&,const OperatorOnUnknown&,
                       AlgebraicOperator,const OperatorOnUnknown&);        
const OperatorOnUnknown* opu() const;
const OperatorOnUnknown* opv() const;
const GeomDomain* domainx() const;       
const GeomDomain* domainy() const;       
virtual BasicBilinearForm* clone() const; 
virtual LinearFormType type() const;     
virtual ValueType valueType() const;     
virtual void print(std::ostream&)const;
\end{lstlisting} 
\vspace{.1cm}

\begin{warningbox}
Be cautious in the definition of double integral. The syntax:

\begin{lstlisting}
BilinearForm a=intg(Sigma, Gamma ,u*G*v);          
\end{lstlisting}

has to be interpreted as

\[
\int_{\Sigma_x}\int_{\Gamma_y}u(y)G(x,y)v(x)dy\,dx
\]

where \(u\) (the right unknown) stands for the unknown, \(v\) (the left unknown) being the test function. In matrix representation, it means that \(u\) stands for columns and \(v\) for rows.
\end{warningbox}
\subsection{The \classtitle{UserBilinearForm} class}
The \class{ UserBilinearForm} class allows users to define general bilinear form. It is based on a \verb?BlfFunction?  providing the computation of elementary matrices. This function has always the following signature:
\begin{lstlisting}
void blfun(BlfDataComputation& blfd);
\end{lstlisting}
where the \class{BlfDataComputation} class encapsulates some useful data updated by computation algorithms:
\begin{lstlisting}
class BlfDataComputation
{ public :
  const Element* elt_u, *elt_v;       // Element pointers (FEM or BEM)
  const Element* elt_u2,*elt_v2;      // additional Element pointers (DG)
  const GeomElement* sidelt;          // side element when DG
  vector<Matrix<real_t>> matels;      // real elementary matrices
  vector<Matrix<complex_t>> cmatels;  // complex elementary matrices
  \ldots
\end{lstlisting}
Regarding their own business, users have to fill either real elementary matrices (\verb?matels?) or complex elementary matrices  (\verb?cmatels?) from element data (\verb?elt_u?, \verb?elt_v?, \ldots). 
\begin{toxicbox}
Up to now, only FEM and DG computation algorithms manage  \class{ UserBilinearForm}.
\end{toxicbox}

So, the  \class{ UserBilinearForm} class handles the following data:
\begin{lstlisting}
class UserBilinearForm : public BasicBilinearForm
{ public :
  BlfFunction blfun_;                     // pointer to an extern blf function (0 by default)
  const IntegrationMethod* intgMethod_p;  // pointer to an integration method
  bool requireInvJacobian_;               // requiring jacobian  (default=false)
  bool requireNormal_;                    // requiring normal vector (default=false)
\end{lstlisting}
This class provides two general constructors and a copy constructor:
\begin{lstlisting}
UserBilinearForm(const GeomDomain& dom,const Unknown& u,const Unknown& v, BlfFunction blf, 
  ComputationType ct,SymType st,bool rij,bool rno,const IntegrationMethod& im);
UserBilinearForm(const GeomDomain& domv,const GeomDomain& domu,const Unknown& u,const Unknown& v, 
  BlfFunction blf,ComputationType ct,SymType st,bool rij,bool rno,const IntegrationMethod& im);
UserBilinearForm(const UserBilinearForm&);
BasicBilinearForm* clone() const;     // clone of the UserBilinearForm
~UserBilinearForm();
\end{lstlisting}
In practice, it is advised to use external pseudo-constructors with some default arguments :
\begin{lstlisting}
BilinearForm userBlf(const GeomDomain& g, const Unknown& u, const Unknown& v, BlfFunction blf, 
             ComputationType ct, SymType=_noSymmetry, bool reqIJ=false, bool reqN=false, 
             const IntegrationMethod& im=QuadratureIM(_GaussLegendreRule,3));
BilinearForm userBlf(const GeomDomain& g, const GeomDomain&, const Unknown& u, const Unknown& v, 
             BlfFunction blf, ComputationType ct, SymType=_noSymmetry, bool reqIJ=false,
             bool reqN=false, const IntegrationMethod& im=QuadratureIM(_GaussLegendreRule,3));
\end{lstlisting}
Besides some useful tools are provided:
\begin{lstlisting}
LinearFormType type() const;                 // return the type of the linear form (_userLf)
const IntegrationMethod* intgMethod() const; // pointer to the integration method object
ValueType valueType() const;                 // return the value type
void requireInvJacobian(bool tf=true);       // set on/off the requireInvJacobian flag
void requireNormal(bool tf=true);            // set on/off the requireNormal flag
SymType setSymType() const ;                 // set the symmetry property (done by analysis)
void print(std::ostream&) const;             // print utility
void print(PrintStream& os) const;           // print utility
string_t asString() const;                   // interpret as string for print purpose
\end{lstlisting}
The next simple example shows how to deal with  \class{ UserBilinearForm}. It deals with the \(\int_\Omega\nabla u.\nabla v\) bilinear form that can be handled by standard approach:
\begin{lstlisting}
//compute elementary matrix grad.grad in 2D-P1
void blfGradGrad(BlfDataComputation& blfd)
{ if(blfd.matel().size()==0) blfd.matel() = Matrix<Real>(3,3);
  const Element* elt=blfd.elt_u;   //get element concerned by
  if(elt==0) return;               //no computation
  GeomMapData& mapdata = *elt->geomElt_p->meshElement()->geomMapData_p; //get geometric data
  Matrix<Real> C=(0.5*mapdata.differentialElement)*mapdata.inverseJacobianMatrix
                  *tran(mapdata.inverseJacobianMatrix);
  Matrix<Real> G(2,3,0.);G(1,1)=1;G(2,2)=1;G(1,3)=-1;G(2,3)=-1; //gradient in ref element
  blfd.matel()=tran(G)*C*G;  //elementary matrix
}
\ldots
Mesh ms(Rectangle(_xmin=0.,_xmax=1., _ymin=0., _ymax=1.,_nnodes=10,_domain_name="Omega"),
        _triangle, 1,_structured,_alternateSplit);
Domain omega=ms.domain("Omega");
Space H(omega, Lagrange, 1, "H"); Unknown p(H,"p"); TestFunction t(p,"t");
BilinearForm ublf =
       userBlf(omega,p,t,blfGradGrad,_FEComputation,_symmetric, true, false); // define user blf
TermMatrix Ku(ublf,"Ku"); // use it as usual
\end{lstlisting}
\begin{advancedbox}
 See the \emph{unit\_DG.cpp} file for a more complex example which deals with the discontinuous Galerkin term:
 \[
\int_\Gamma \big\{\nabla_hu_h.n\big\}\big[v\big]\]
\end{advancedbox}

\subsection{The \classtitle{SuBilinearForm} class}

\class{BasicBilinearForm} objects may be linearly combined to produce a linear combination
of \class{BasicBilinearForm} objects stored as a list of pair of \class{BasicBilinearForm}
object and a complex scalar in the \class{SuBilinearForm} class. 
\vspace{.1cm}
\begin{lstlisting}
typedef std::pair<BasicBilinearForm*,complex_t> blfPair;
class SuBilinearForm
{protected:
   std::vector<blfPair> blfs_; //list of pairs of basic bilinear form and coefficient
   \ldots
\end{lstlisting}
\vspace{.1cm}
All the \class{BasicBilinearForm} objects must have the same pair of unknowns ! It is the
reason why this class has no pointer to unknowns; it refers to the first basic bilinear form
to get its unknowns.\\

Because \class{BasicBilinearForm} objects are copied for safety reason, this class provides
default and basic constructors but also a copy constructor, a destructor and the overload assignment
operator: 
\vspace{.1cm}
\begin{lstlisting}
SuBilinearForm(){};                         
SuBilinearForm(const SuBilinearForm&);        
~SuBilinearForm();                           
SuBilinearForm& operator=(const SuBilinearForm&);  
\end{lstlisting}
\vspace{.2cm}
It provides few accessors (some being const and no const):
\vspace{.1cm}
\begin{lstlisting}
number_t size() const;    
std::vector<blfPair>& blfs();
const std::vector<blfPair>& blfs() const; 
blfPair& operator()(number_t n);
const blfPair& operator()(number_t n) const;
const Unknown* up() const;  
const Unknown* vp() const; 
const Space* uSpace() const;  
const Space* vSpace() const;        
LinearFormType type() const;    
ValueType valueType() const;     
\end{lstlisting}
\vspace{.2cm}
It is possible to perform linear combination of linear combinations using the following overloaded
operators:
\vspace{.1cm}
\begin{lstlisting}
SuBilinearForm& SuBilinearForm::operator +=(const SuBilinearForm&);   
SuBilinearForm& SuBilinearForm::operator -=(const SuBilinearForm&);   
SuBilinearForm& SuBilinearForm::operator *=(const complex_t&);      
SuBilinearForm& SuBilinearForm::operator /=(const complex_t&); 
SuBilinearForm operator-(const SuBilinearForm&);                       
SuBilinearForm operator+(const SuBilinearForm&,const SuBilinearForm&);   
SuBilinearForm operator-(const SuBilinearForm&,const SuBilinearForm&);   
SuBilinearForm operator*(const complex_t&,const SuBilinearForm&);      
SuBilinearForm operator*(const SuBilinearForm&,const complex_t&);      
SuBilinearForm operator/(const SuBilinearForm&,const complex_t&); 
bool SuBilinearForm::checkConsistancy(const SuBilinearForm&) const; 
\end{lstlisting}
\vspace{.1cm}
The member function \cmd{checkConsistancy} performs a test to ensure that pair of unknowns
is always the same.\\

Finally, there are some print facilities:
\vspace{.1cm}
\begin{lstlisting}
void SuBilinearForm::print(std::ostream&)const;    
std::ostream& operator<<(std::ostream&,const SuBilinearForm&); 
\end{lstlisting}

\subsection{The \classtitle{BilinearForm} class}

The \class{BilinearForm} class is the end user class dealing with general bilinear form, either
a single unknown bilinear form (a \class{SuBilinearForm} object) or a multiple unknown bilinear
form (a list of \class{SuBilinearForm} objects). In this class, a single unknown bilinear
form is a multiple unknown blinear form with one pair of unknowns! The list of \class{SuBilinearForm}
objects is stored in a map of \class{SuBilinearForm}, indexed by a pair of pointers to unknown
:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] map}]
typedef std::pair<const Unknown *,const Unknown *> uvPair;

class BilinearForm
{protected :
 std::map<uvPair,SuBilinearForm> mlcblf_;  //list of linear combinations of basic forms
 \ldots 
\end{lstlisting}
\vspace{.2cm}
To manage the map, the following aliases are defined:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] map}]
typedef std::map<uvPair,SuBilinearForm>::iterator it_mublc;
typedef std::map<uvPair,SuBilinearForm>::const_iterator cit_mublc;
\end{lstlisting}

\begin{focusbox}
When the \class{SuBilinearForm} unknowns owns an unknown component, the \class{SuBilinearForm}
object is attached to its parent item! In other words, component unknowns are not indexed in
the map.
\end{focusbox}

This class provides only one constructor from a linear combination of forms:
\vspace{.1cm}
\begin{lstlisting}
BilinearForm(const SuBilinearForm&);  
\end{lstlisting}
\vspace{.2cm}
and proposes some accessors and facilities:
\vspace{.1cm}
\begin{lstlisting}
bool singleUnknown() const; 
bool isEmpty() const;      
SuBilinearForm& operator[](const uvPair&);             //protected
const SuBilinearForm& operator[](const uvPair&) const; //protected                               
const SuBilinearForm& first() const;                   
BilinearForm operator()(const Unknown&,const Unknown&) const;         
BasicBilinearForm& operator()(const Unknown&,const Unknown&,number_t);
const BasicBilinearForm& operator()(const Unknown&,const Unknown&,number_t) const; 
\end{lstlisting}
\vspace{.2cm}
Besides, there are three fundamental external end-user's function (\verb?intg? and \verb?intg_intg?)
which constructs \class{BilinearForm} object:
\vspace{.1cm}
\begin{lstlisting}
BilinearForm intg(const GeomDomain&,const OperatorOnUnknown&,
                  AlgebraicOperator,const OperatorOnUnknown&);          
BilinearForm intg(const GeomDomain&,const OperatorOnUnknowns&);
BilinearForm intg(const GeomDomain&,const LcOperatorOnUnknowns&);
BilinearForm intg(const GeomDomain&,const GeomDomain&,
                       const OperatorOnUnknown&,AlgebraicOperator,
                       const OperatorOnUnknown&);                    
BilinearForm intg(const GeomDomain&,const GeomDomain&,const OperatorOnUnknowns&);                          
\end{lstlisting}
\vspace{.1cm}
\class{OperatorUnknows} is a simple class used to store the two operators and the algebraic
operation.\\

In order to construct any linear forms, the algebraic operators (\verb?+= -= *= /= + - * /?)
are oveloaded for different objects:
\vspace{.1cm}
\begin{lstlisting}
BilinearForm& BilinearForm::operator +=(const BilinearForm&);          
BilinearForm& BilinearForm::operator -=(const BilinearForm&);          
BilinearForm& BilinearForm::operator *=(const complex_t&);           
BilinearForm& BilinearForm::operator /=(const complex_t&); 
BilinearForm operator+(const BilinearForm&,const BilinearForm&);     
BilinearForm operator-(const BilinearForm&,const BilinearForm&);   
BilinearForm operator*(const complex_t&,const BilinearForm&);      
BilinearForm operator*(const BilinearForm&,const complex_t&);      
BilinearForm operator/(const BilinearForm&,const complex_t&);          
\end{lstlisting}
\vspace{.2cm}
Finally, the class provides usual print facilities:
\vspace{.1cm}
\begin{lstlisting}
void BilinearForm::print(std::ostream&)const;                     
std::ostream& operator<<(std::ostream&,const BilinearForm&);  
\end{lstlisting}
\vspace{.2cm}

\subsection*{Example}

To end we show some characteristic examples. In these examples, \verb?u?,\verb?v?  are scalar
unknowns, \verb?p?, \verb?q?  are vector unknowns, \verb?f? either a scalar or a scalar function,
\verb?h? either a vector or a vector  function, \verb?g? a scalar or a scalar kernel and \verb?Omega?,\verb?Gamma?
\verb?Sigma? some geometric domains. 
\vspace{.1cm}
\begin{lstlisting}
BilinearForm a1=intg(Omega,grad(u)|grad(v));
a1-=k2*intg(Omega,f*u*v);
a1=intg(Omega,grad(u)|grad(v))-intg(Omega,f*u*v);   //same result
BilinearForm a2=intg(Omega,div(p)*q)+eps*intg(Omega,p*q);
BilinearForm a3=a1+a2;
a3=intg(Omega,grad(u)|grad(v))+intg(Omega,div(p)*q) //same result
  -intg(Omega,f*u*v)+eps*intg(Gamma,p*q); 
\end{lstlisting}
\vspace{.2cm}

\displayInfos{library=form, header=BilinearForm.hpp, implementation=BilinearForm.cpp, test=test\_form.cpp, header dep={config.h, utils.h}}
