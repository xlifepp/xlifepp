%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{Function} class}\label{s.function}

The object function aims to encapsulate the user functions with additional information,
says parameter data. This encapsulation are concerned with functions of a point or a couple
of points, required by finite element computations at a low level. These functions are typically
some physical parameters involved in integrals or source data functions. They may require other
constant parameters. It is the reason why it is necessary to attach to the function a list
of parameters (constant values of real type, complex type, \ldots). This class is designed to
deal with standard functions involved in a finite element software (24 types):
\begin{lstlisting}
    r         function(const Point&, Parameters&)
    Vector<r> function(const Vector<Point>&, Parameters&)
    r         function(const Point&, const Point&, Parameters&)
    Vector<r> function(const Vector<Point>&, const Vector<Point>&, Parameters&)
\end{lstlisting}   
with r of type: 
\begin{lstlisting}
real_t, complex_t, Vector<real_t>, Vector<complex_t>, Matrix<real_t>, Matrix<complex_t>
\end{lstlisting}  
\vspace{.2cm}
The functions (resp. kernels) taking a point or a couple of points as input argument are called
"scalar" functions (resp. kernel) whereas those taking a vector of points or a couple of vector
of points are called "vector" functions (resp. kernels). Do not confuse a "vector" function
with a function returning a vector and taking a point as input argument. For a such function,
its vector form returns a vector of vectors. \\

The object function is based on the non-template class \class{Function}:
\begin{lstlisting}[deletekeywords={[3] map}]
template<typename T_> class Function
{class Function
 protected:
 ValueType type_returned_;    // type of returned value ( _real, _complex)
 StrucType struct_returned_;  // struct of returned value (_scalar,_vector,_matrix)
 FunctType type_function_;    // type of the function (_function or _kernel}
 ArgType argType_;            // type of the argument (_point,_vector_point)
 mutable Parameters* params_; // pointer to parameter list
 dimPair dims_;               // dimension of returned values
 String name_;                // name of function and comments
 void * fun_;                 // store pointer to function in a void *
 void* table_;                // Tabular pointer as void*  (default 0)
 Function* pointToTabx_;      // Function to associate Point or Point pair to variables 
 //flag in OperatorOnUnknown construction (see operator lib)
 mutable bool check_type_;    // to activate a checking of arguments 
 mutable bool conjugate_;     // temporary flag for conjugate operation  
 mutable bool transpose_;     // temporary flag for transpose operation  
 //to manage data requirements
 bool requireNx;              // true if function requires normal or x-normal vector
 bool requireNy;              // true if function (kernel) requires y-normal vector
 bool requireElt;             // true if function requires element
 bool requireDom;             // true if requires domain
 bool requireDof;             // true if requires DoF
 //special attributes for kernel
 mutable bool xpar;           // true if x is consider as a parameter 
 mutable Point xory;          //  x or y as parameter of the kernel 
 public:
 static std::map<String,std::pair<ValueType,StrucType> > returnTypes;  
 static std::map<String,std::pair<ValueType,StrucType> > returnArgs;  
}
\end{lstlisting}

which manages the user function storing its pointer in the void pointer \var{fun\_}, its parameters
list in the Parameters pointer \var{params\_} and a function name (optional) in the string
\var{name\_}. There are also members to store characteristics of the function (type and arguments
type) allowing to identify quickly the return type of the function. The member \var{check\_type\_}
is a flag used in safe evaluation process of the function. The two static maps are used to
the management of the types.

\begin{focusbox}
We choose to use a non-template class for two reasons: to avoid template paradigm to the
end users and to have a simpler way to transmit such object function in different part of the
library. The counterpart of this choice is a more complex Function class, but the complexity
is only located in the class.  
\end{focusbox}

For each type of function is defined a useful alias of the pointer function:
\begin{lstlisting}
typedef r(fun_xx_t)(const Point&, Parameters&);
typedef Vector<r>(vfun_xx_t)(const Vector<Point>&, Parameters&);
typedef r(ker_xx_t)(const Point&, const Point&, Parameters&);
typedef Vector<r>(vker_xx_t)(const Vector<Point>&, const Vector<Point>&, Parameters&);
\end{lstlisting}  
where \emph{r} is the return type and \emph{xx} a code of the return type (sr, sc, vr, vc, mr, mc);
s for scalar, v for  vector, m for matrix, r for real and c for complex. 

A \class{Function}, using the \verb?void*? pointer \verb?table_?, can be related to a \class{Tabular} object that handles tabulated values.  \verb?table_? is a \verb?void*? pointer because the \class{Tabular} is a template class. In that case the function is evaluated using interpolation of tabulated values. By default, the Tabular dimension has to be consistent with the type of function and the dimension of input point. If it is planned to use a Tabular with a dimension less than the point dimension, the \class{Function} pointer \verb?pointToTabx_?  has to be set by a \class{Function} that relates a \class{Point} to a \verb?Vector<real_t>? containing the coordinates in the \class{Tabular}. 

The \class{Function} proposes:
\begin{itemize}
\item Two constructors for each type of function (named and unnamed):
\begin{lstlisting}
  Function(fun_sr_t& f, Parameters& pa = def_parameters);  //real scalar type
  Function(fun_sr_t& f, const String& na, Parameters& pa = def_parameters);
  Function(fun_sc_t& f, Parameters& pa = def_parameters);  //complex scalar type
  Function(fun_sc_t& f, const String& na, Parameters& pa = def_parameters);
  \ldots
\end{lstlisting}
which initialize the void pointer and compute the return types using the member function \cmd{init}.

A function may be constructed from tabulated values, specifying a \class{Tabular} object:
\begin{lstlisting}
template <typename T>
Function(Tabular<T>& tab, dimen_t dp, const string_t& na, 
         FunctType ft =_function, Function* fpx =0) 
\end{lstlisting}
Note that it is also possible to create some tabulated values from the function itself using \cmd{createTabular} member functions:
\begin{lstlisting}
void createTabular(real_t x0, real_t dx, number_t nx, const string_t& nax="x") ;
void createTabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, 
         number_t ny, const string_t& nax="x", const string_t& nay="y");
\ldots
\end{lstlisting}
Up to now, only scalar function (not kernel) can be tabulated.

\item Evaluation capabilities (unsafe but fast) for each type of function:
\begin{lstlisting}[deletekeywords={[3] x}]
real_t fun_sr(const Point &x)
complex_t fun_sc(const Point &x)
\ldots
\end{lstlisting}
As these functions only recast the void pointer  to the required pointer function:
\begin{lstlisting}[deletekeywords={[3] x, y}]
real_t fun_sr(const Point &x)
   {return reinterpret_cast<fun_sr_t*>(fun_)(x,*params_);}
\end{lstlisting}
they are unsafe !

\item Templated overloaded \cmd{operator()}, insensitive to the form of function (safe but not as
fast as previous)
\begin{lstlisting}[deletekeywords={[3] x}]
template <typename T_>
T_& operator()(const Point & x, T_ & res) const;
T_& operator()(const Point & x,const Point& y, T_& res) const;
Vector<T_>& operator()(const Vector<Point>& x, Vector<T_>& res);
Vector<T_>& operator()(const Vector<Point>& x, 
                       const Vector<Point>& y,Vector<T_> & res);
\end{lstlisting}
Note that these functions return a reference, contrary to the direct straight functions which
return a value. Besides, by testing the input arguments, they are able to deal with single
point or vector of points independently of the scalar or vector form of the user function 
:
\begin{lstlisting}
template<typename T_>
T_& Function::operator()(const Point & x, T_ & res)
{  if(checkType_)checkFunctionType(res, _function); // check the type of arguments
   if(argType_ == _pointArg)  // case of a function of a point
   {
     if(functionType_==_function)
      {if(table_==0)
         { typedef typename PointerF<T>::fun_t* funT;
           funT f = reinterpret_cast<funT>(fun_);
           res = f(x, *params_);
         }
       else res=funTable(x,res);
    }
\ldots
if(transpose_) res=tran(res);
if(conjugate_) res=conj(res);
return res;
}

}
\end{lstlisting}
Moreover, using the \cmd{checkFunctionType}, they can also perform a control of the type
of the function. As the checking function uses expensive RTTI capabilities, this possibility
is conditioned by the \var{checkType\_} state (true or false) which is automatically reset
to false by the \cmd{checkFunctionType}.

\item Accessors to the function characteristics:
\begin{lstlisting}
ValueType typeReturned() const;
StrucType structReturned() const;
ValueType valueType() const;
StrucType strucType() const;
FunctType typeFunction() const;
ArgType typeArg() const ;
bool conjugate() const;
bool& conjugate();
void conjugate(bool v) const;
bool transpose() const;
bool& transpose();
void transpose(bool v) const;
bool isVoidFunction() const;
const void* fun_p() const;
Parameters& params();
Parameters& params() const;
Parameters*& params_p();
const Parameters* params_p() const;
dimPair dims() const;
string_t name() const;
\end{lstlisting}
\item Some functions to manage the function parameters and X/Y parameter in kernel:
\begin{lstlisting}
void addParam(Parameter& pa;
template<typename K>
void setParam(K& v, const string_t& name) const;  
Parameter& parameter(const string_t&) const;      
Parameter& parameter(const char* s) const;       
Parameter& parameter(const size_t) const;         
void setX(const Point& P) const; // for kernel
void setY(const Point& P) const;
\end{lstlisting}
\item Some functions to manage the function data required during computation:
\begin{lstlisting}
bool normalRequired() const;
void require(UnitaryVector uv, bool tf=true);
void require(const string_t& s, bool tf=true);
\end{lstlisting}
\item a print utility:
\begin{lstlisting}
void printInfo() const
\end{lstlisting}
\end{itemize}
To deal with some data that are available during FE computation such as normal vectors, current element, current DoF or current domain, there are two things to do : tell to \xlifepp that the Function will use such data
\begin{lstlisting}
Function F(fun);     //link a C++ function fun to a Function object
F.require(_n);       //tell that function fun will use normal vector
F.require("element");//tell that function fun will use element
\end{lstlisting}
 and get data in the C++ function related to the Function object:
 \begin{lstlisting}
 Real fun(const Point& x, Parameters& pars=theDefaultParameters) 
 {\ldots
  Vector<Real>& n = getN();      //get the normal vector
  GeomElement& elt=getElement(); //get element 
  \ldots
 }
 \end{lstlisting}
 This machinery is based on the \class{ThreadData} class that manages one instance of data by thread.

\displayInfos{library=utils, header=Function.hpp, implementation=Function.cpp, test=test\_Function.cpp,
header dep={config.h, Parameters.hpp, Point.hpp, String.hpp, Vector.hpp}}
