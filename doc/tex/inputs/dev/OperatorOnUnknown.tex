%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{OperatorOnUnknown} class}

The \class{OperatorOnUnknow} class is intended to store the description of an expression involving
a differential operator acting on an unknown (or a test function) with possibly operations
at the left and the right. More precisely, the general form of the operator on unknown is:
\begin{center}
\textbf{tr(data) op dif( tr(u) ) op tr(data)}
\end{center}
where
\begin{itemize}
\item \textbf{u} is an unknown,
\item \textbf{dif} a differential operator,
\item \textbf{data} is a user data (value or function),
\item \textbf{op} an algebraic operation among \verb?*? (product), \verb?|? (inner product),
\verb?^? (cross product), \verb?:? (contracted product),
\item \textbf{tr} a transformation among conj (conjugate), tran (transpose) and adj (adjoint).
\end{itemize}
All is optional except the unknown u. \\

The simplest form is \textbf{u} and a more complex one is for instance, something like this
(\(f\) a matrix and \(g\) a vector):
\begin{center}
\textbf{adj}(\textbf{f})* \textbf{grad}(\textbf{conj}(\textbf{u}[2])) | \textbf{conj}(\textbf{g})
\end{center}
which mathematically reads:
\[f^*\,\nabla \overline{u}_2\,.\,\overline{g}\]
The class has the following attributes:
\vspace{.1cm}
\begin{lstlisting}
class OperatorOnUnknown
{protected :
   const Unknown * u_p;            //unknown involved in operator
   bool conjugateUnknown_;         //true if the unknown has to be conjugated
   DifferentialOperator * difOp_p; //differential operator involved in operator
   Operand * leftOperand_p;        //object before the diff operator
   Operand * rightOperand_p;       //object after the diff operator
   bool leftPriority_;             //priority order flag, true if left operand is prior
   Vector<complex_t> coefs_;       //coefficients for operator (gradG, divG, curlG)
   ValueType type_;                //returned value (_real,_complex)
   StrucType struct_;              //returned structure (_scalar,_vector,_matrix)	 
\end{lstlisting}
\vspace{.2cm}
\class{Operand} is a class storing a user data (value or function) and an algebraic operation
(\verb?* | ^ %?).\\

The class provides some basic constructors (from unknown and differential operator type), some
useful constructors (from unknown and function or value),  a copy constructor, the assignment
operator and a destructor:
\vspace{.1cm}
\begin{lstlisting}
OperatorOnUnknown(const Unknown* un=0,DiffOpType=_id);  
OperatorOnUnknown(const Unknown& un,DiffOpType=_id); 
OperatorOnUnknown(const Unknown&,const Function&,AlgebraicOperator,bool);   
OperatorOnUnknown(const Unknown&,const Value&,AlgebraicOperator,bool);     
OperatorOnUnknown(const OperatorOnUnknown &);           
~OperatorOnUnknown();                                   
OperatorOnUnknown& operator=(const OperatorOnUnknown &);
\end{lstlisting}
\vspace{.2cm}
Note that the \class{DifferentialOperator} object are created on the fly when creating \class{OperatorOnUnknown} from \class{Unknown} and \class{DiffOpType}. \\

The class provides some accessors (read or write):
\vspace{.1cm}
\begin{lstlisting}
DiffOpType difOpType() const ;
DifferentialOperator*& difOp() ;
ValueType& type();
Operand*& leftOperand();
Operand*& rightOperand();
Vector<complex_t>& coefs();
const Vector<complex_t>& coefs() const;
bool leftPriority() const;
bool& leftPriority();
\end{lstlisting}
\vspace{.2cm}
and  some utilities to update class attributes and print them:
\vspace{.1cm}
\begin{lstlisting}
void setReturnedType(const Unknown*,DiffOpType);
void updateReturnedType(AlgebraicOperator,ValueType,StrucType,bool); 
void print(std::ostream&) const;   
std::ostream& operator<<(std::ostream&,const OperatorOnUnknown &);  //extern          
\end{lstlisting}
\vspace{.2cm}
The first two functions allow us managing the returned types and check consistency of operations and the last two
functions output on stream the \class{OperatorOnUnknown} characteristics.\\

To deal with the general syntax of \class{OperatorOnUnknown}, a lot of external functions are required,
in particular some overloaded operators. The first family of functions concerns the creation
of simple \class{OperatorOfUnknown} object from a differential operator and an unknown. Their names
are the names of differential operator type (\class{DiffOpType} enumeration) without the leading underscore.
We give only a few examples here (see OperatorOnUnknown.hpp header file for the complete list):
\vspace{.1cm}
\begin{lstlisting}
OperatorOnUnknown& id(const Unknown&);
OperatorOnUnknown& d0(const Unknown&);
\ldots
OperatorOnUnknown& grad(const Unknown&);
OperatorOnUnknown& nabla(const Unknown&);
\ldots
OperatorOnUnknown& nx(const Unknown&);
\ldots
OperatorOnUnknown& gradG (const Unknown&,const complex_t&,const complex_t&,
                                         const complex_t&,const complex_t&);
\ldots
OperatorOnUnknown& mean(const Unknown&);
OperatorOnUnknown& jump(const Unknown&);                  
\end{lstlisting}
\vspace{.2cm}
These functions construct an \class{OperatorOnUnknown} in the heap memory and return a reference in
order to be used in a chained expression. In this context, it may occur a memory leak if
nobody delete explicitly the \class{OperatorOnUnknown} object created on the fly.\\

To manage general syntax, the following c++ operators have been overloaded:\\
\hspace*{5mm} \verb?*? : usual consistent product between scalars, vectors and matrices \\
\hspace*{5mm} \verb?|? : inner product of vectors, for complex objects it is a hermitian product\\
\hspace*{5mm} \verb?^? : cross product of vectors\\
\hspace*{5mm} \verb?%? : contracted product (the second term is transposed)

These operators are enumerated in the \class{AlgebraicOperator} enumeration.\\ 

They are overloaded for \class{Unknown} or \class{OperatorOnUnknown} and unitaryVector (\class{UnitaryVector} enumerates the normal vector \_ n and the tangential vector \_ t). When differential operators involve operation with normal, you can use the previous function (for instance \textit{ndot}) or use their mathematical syntax (for instance \textit{n*u}):
\vspace{.1cm}
\begin{lstlisting}
OperatorOnUnknown& operator*(UnitaryVector,const Unknown &);    // n*u 
OperatorOnUnknown& operator*(const Unknown &,UnitaryVector);    // u*n
OperatorOnUnknown& operator|(UnitaryVector,const Unknown &);    // n|u 
OperatorOnUnknown& operator^(UnitaryVector,const Unknown &);    // n^u 
OperatorOnUnknown& operator|(const Unknown &,UnitaryVector);    // u|n 
OperatorOnUnknown& operator|(UnitaryVector,OperatorOnUnknown&); // n|grad(u)
OperatorOnUnknown& operator^(UnitaryVector,OperatorOnUnknown&); // n^(n^u) or n^curl(u) 
OperatorOnUnknown& operator*(UnitaryVector,OperatorOnUnknown&); // n*div 
OperatorOnUnknown& operator|(OperatorOnUnknown&,UnitaryVector); // grad(u)|n 
OperatorOnUnknown& operator*(OperatorOnUnknown&,UnitaryVector); // div*n 
\end{lstlisting}
\vspace{.1cm}
Note that all cases are not considered. It should be !?\\

To deal with left and right-hand side operations, these algebraic operators have been also overloaded
for different types of objects (\class{Unknown}, \class{OperatorOnUnknown}, \class{Function}, \class{Value}, user c++ function
or user value). The operator overloading with c++ function or user value avoids the user to
explicitly encapsulate its own data (c++ function or scalar, vector, matrix) in Function object
or Value object. There are shortcuts. In the following, only prototypes of * operator are presented.

\subsection{Operations between \classtitle{Unknown} and \classtitle{Function}}
\vspace{.1cm}
\begin{lstlisting}
OperatorOnUnknown&  operator*(const Unknown &,const Function&);    // u*F
OperatorOnUnknown&  operator*(const Function&,const Unknown &);    // F*u
template <typename T>
  OperatorOnUnknown& operator*(const Unknown &,T( )(const Point&,Parameters&));
  OperatorOnUnknown& operator*(T( )(const Point&,Parameters&),const Unknown &);
  OperatorOnUnknown& operator*(const Unknown &,
                               T( )(const Vector<Point>&,Parameters&));
  OperatorOnUnknown& operator*(T( )(const Vector<Point>&,Parameters&),
                               const Unknown &);
\ldots
\end{lstlisting}
\vspace{.1cm}
The two first declarations concern Function object encapsulating c++ user functions whereas
 the templated forms concerns c++ user functions. As mentioned before, it avoids the user to
encapsulate his c++ functions. Note that only c++ functions that can be encapsulated in Function
object are working in templated forms. Other functions may be accepted in compilation process,
but the result during execution may be hazardous!

\begin{warningbox}
Kernel type functions can not be use straightforward in operator. Contrary to the ordinary
functions, they have to be encapsulated in \class{Function} objects!
\end{warningbox}

\subsection{Operations between \classtitle{Unknown} and \classtitle{Value}}

\vspace{.1cm}
\begin{lstlisting}
OperatorOnUnknown&  operator*(const Unknown&,const Value&);       // u*V
OperatorOnUnknown&  operator*(const Value&,const Unknown&);       // V*u
template<typename T>
  OperatorOnUnknown&  operator*(const Unknown& ,const T& )        // u*T
  OperatorOnUnknown&  operator*(const T& ,const Unknown&)         // T*u
\ldots
\end{lstlisting}
\vspace{.1cm}
The two first declarations concern Value object encapsulating c++ user data and the templated
forms concerns c++ user data. Only c++ user data that can be encapsulated in Value object are
working in templated forms. \\

\subsection{Operations between \classtitle{OperatorOnUnknown} and \classtitle{Function}}

The principle is the same as in previous cases.
\vspace{.1cm}
\begin{lstlisting}
OperatorOnUnknown& operator*(OperatorOnUnknown&,const Function &); // op(u)*F
OperatorOnUnknown& operator*(const Function &,OperatorOnUnknown&); // F*op(u)
template<typename T>
  OperatorOnUnknown& operator*(OperatorOnUnknown&, T( )(const Point&,Parameters&))
  OperatorOnUnknown& operator*(T( )(const Point&,Parameters&), OperatorOnUnknown&)
  OperatorOnUnknown& operator*(OperatorOnUnknown&,
                               T( )(const Vector<Point>&,Parameters&))
  OperatorOnUnknown& operator*(T( )(const Vector<Point>&,Parameters&),
                               OperatorOnUnknown&)
\ldots
\end{lstlisting}
\vspace{.1cm}

\subsection{Operations between \classtitle{OperatorOnUnknown} and \classtitle{Value}}

\vspace{.1cm}
\begin{lstlisting}
OperatorOnUnknown& operator*(OperatorOnUnknown&,const Value &);   // op(u)*V
OperatorOnUnknown& operator*(const Value &,OperatorOnUnknown&);   // V*op(u)
template<typename T> 
  OperatorOnUnknown& operator*(OperatorOnUnknown&,const T&);      // op(u)*T
  OperatorOnUnknown& operator*(const T&,OperatorOnUnknown&);      // T*op(u)
\ldots
\end{lstlisting}
\vspace{.2cm}
Most of the previous overloaded operators use the following update functions:
\vspace{.1cm}
\begin{lstlisting}
OperatorOnUnknown& updateRight(OperatorOnUnknown&,const Function&,AlgebraicOperator);  
OperatorOnUnknown& updateLeft (OperatorOnUnknown&,const Function&,AlgebraicOperator);  
OperatorOnUnknown& updateRight(OperatorOnUnknown&,const Value&,AlgebraicOperator);
OperatorOnUnknown& updateLeft (OperatorOnUnknown&,const Value&,AlgebraicOperator);
template<typename T>
  OperatorOnUnknown& updateRight(OperatorOnUnknown&,const T&,AlgebraicOperator);
  OperatorOnUnknown& updateLeft(OperatorOnUnknown&,const T&,AlgebraicOperator);
\end{lstlisting}
\vspace{.2cm}

Note that it is possible to use the transformations \textit{conj}, \textit{tran} and \textit{adj}
on Unknown, Value or Function. These functions are defined in the files related to these classes.
For simplicity, it is not possible to apply such transformations to an \class{OperatorOnUnknown} object
or a part of the expression. For instance, the syntax \textit{conj(grad(u))} is not supported.
\\
Finally, we give some examples to show how this machinery works:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] opu}]
//define some C++ functions
real_t F(const Point &P,Parameters& pa = defaultParameters) {\ldots}
Vector<real_t> vecF(const Point &P,Parameters& pa = defaultParameters) {\ldots}
Matrix<real_t> matF(const Point &P,Parameters& pa = defaultParameters) {\ldots}
//define some constant values
real_t pi=3.14159; complex_t i(0,1);vector<real_t> v(3,1); matrix<complex_t> A(3,3,i);
//assume u is an Unknown (a vector of dimension 3)
OperatorOnUnknown opu=u;    //the simplest one
opu=grad(u);                //involve only differential operator
opu=_n^curl(u);              //equivalent to ncrosscurl(u)
opu=v^u;                    //cross product with vector 
opu=vecF^u;                 //cross product with vector function
opu=A*grad(u);              //left product with a matrix
opu=grad(u)*matF;           //right product with a matrix function
opu=(matF*grad(u))%A;       //product and contracted product
opu=curl(conj(u))|conj(v);  //use conj transformation
\end{lstlisting}
\vspace{.2cm}
Be careful with operator priority. The C++ priority rules are applied and not the mathematical
ones. In doubt, use parenthesis.
The code performs some constancy checking on operand and operator but only on structure (scalar,
vector, matrix) not on the dimension (see \textit{upadateReturnedType} and \textit{setReturnedType}
functions).
\displayInfos{library=operator, header=OperatorOnUnknown.hpp, implementation=OperatorOnUnknown.cpp,
test=test\_operator.cpp, header dep={config.h, utils.h}}

