%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Mesh management}

The \class{Mesh} class handles mesh description, that is a collection of mesh elements.
\class{Mesh} objects are built either from internal meshing tools or from mesh files provided
by external meshing tools as \gmsh for instance. To avoid complexity to end users, we
decide to have a unique mesh class with no inheritance. It means that each meshing tool should
be a member function of the \class{Mesh} class. These member functions are distributed in
various files (see the Meshing tools section). \\

Basically, a mesh is a collection of points (vertices and intermediate points), of geometric
elements (\class{GeomElement}) to carry the computation domain partition, of geometric domains
(\class{Domain}): a list of elements or a list of sides of elements. Note that curved elements
are supported using geometric interpolation of order greater than one. This \class{Mesh} class
supports also mesh of surface in 3D space and mesh of line in 2D space. Besides the basic geometric
data, it is also able to manage global list of sides (say faces of a 3D mesh, edges of a 2D
mesh) or global list of sides of sides (edges of a 3D mesh). By default, these lists are not
constructed.\\

General information on a mesh such as mesh name, space dimension, name of variables, bounding
box are collected in the \class{Geometry} class. In the future, this class should be developed
to handle other information, in particular boundary parametrizations useful to describe in
an analytical way the domain to mesh, allowing to provide high order mesh and refinement meshing
tools.

\subsection{The \classtitle{Mesh} class}
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] nodes}]
class Mesh
{
 public :
  Geometry* geometry_p;                   // global geometric information
  std::vector<Point> nodes;               // list of all mesh nodes
  Number lastIndex_;                      // last index of GeomElements

 protected :
  String name_;                           // name for documentation purposes
  String comment_;                        // comment documentation purposes
  std::vector<GeomElement*> elements_;    // list of geometric elements (MeshElement)
  std::vector<GeomDomain*> domains_;      // list of geometric domains
  std::vector<Number> vertices_;          // list of vertices indexed by their number in nodes vector
  bool isMadeOfSimplices_;                // only segments, triangles or tetrahedra
  Dimen order_;                           // order of the mesh = max of element orders
  std::vector<GeomElement*> sides_;       // list of sides (edges in 2D, face in 3D), built if required
  std::vector<GeomElement*> sideOfSides_; // list of sides of sides (edges in 3D), built if required
  std::vector<std::vector<GeoNumPair> > vertexElements_; // for each vertex v, list of elements having vertex v, built if required
  mutable Mesh* firstOrderMesh_p;         // underlying first order mesh when mesh has elements of order greater than one
\ldots
\end{lstlisting}
\vspace{.1cm}
\class{GeomElement} class is described in detail in section \ref{sec_GeomElement}. In few
words, the \class{GeomElement} class stores in a \class{MeshElement} object the numbering
vectors of a mesh element (node numbers, vertex numbers, side numbers, side of side numbers)
and a pointer to the reference element (\class{RefElement}) supporting the geometric interpolation
of the element (define the map to reference geometric element). In case of a side element,
the \class{GeomElement} class stores the parent sides and the side numbers. Note that in that
case, \class{MeshElement} object does not necessarily exist! \\

Regarding meshing tools, the \class{Mesh} class provides various constructor : one reading a mesh file, two general constructors from any geometry, a constructor meshing one reference element, a constructor extruding a 1D/2D mesh and a constructor converting mesh elements to another type:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] name, order, shape, mesh}]
Mesh();
Mesh(const String& filename, const String&, IOFormat mft = _undefFormat);
Mesh(const Mesh&);
~Mesh();
Mesh(const Geometry& g, Number order = 1, MeshGenerator mg = _defaultGenerator, 
     const String& name = ""); // for 1D geometry
Mesh(const Geometry& g, ShapeType sh, Number order = 1, MeshGenerator mg = _defaultGenerator, const String& name = ""); // for 2D and 3D geometries
Mesh(ShapeType shape); // for reference element 
Mesh(const Mesh& ms, const Point& O, const Point& d , number_t nbl, number_t namingDomain=0, number_t namingSection=0, number_t namingSide=0, const string_t& meshName="")  //for extruded mesh
Mesh(const Mesh& mesh, ShapeType sh, const string_t name=""); // for element converter
\end{lstlisting}
See \autoref{ss.meshtools} for an exhaustive list of meshing tools.\\

As most members are protected, there are useful accessors:
\vspace{.1cm}
\begin{lstlisting}
const std::vector<GeomElement*>& elements() const;
const std::vector<Domain*>& domains() const;
const std::vector<GeomElement*>& sides()const;
const std::vector<GeomElement*>& sideOfSides()const;
const std::vector<Number>& vertices()const;
const String& name() const;
bool isMadeOfSimplices()const;
const GeomElement& element(Number i) const;
const Domain& domains(Number i) const;
const GeomElement& side(Number i) const;
const GeomElement& sideOfSide(Number i) const;
const Number vertex(Number i) const;
const Point& vertexPoint(Number i) const;
Dimen spaceDim() const ;
Dimen meshDim() const;
Number nbOfElements() const;
Number nbOfDomains() const;
Number nbOfSides() const;
Number nbOfSideOfSides() const;
Number nbOfVertices() const;
Number nbOfNodes() const;
\end{lstlisting}
\vspace{.1cm}
There are all constant and some are useful shortcuts.\\

The \class{Mesh} class provides four important member functions to complete on demand the construction of a mesh: functions to build the list of sides, the list of sides of sides and the list of vertex elements and a function to compute element orientation (sign of jacobian determinant) and the measure
of element and its sides:
\vspace{.1cm}
\begin{lstlisting}
void buildSides();
void buildSideOfSides();
void buildVertexElements();
void buildGeomData();
\end{lstlisting}
\vspace{.1cm}
Note that \cmd{buildSides}  and \cmd{buildSideOfSides} functions update some data of
\class{GeomElement}s.\\

Finally, the class has usual printing facilities:
\vspace{.1cm}
\begin{lstlisting}
void print(std::ostream &) const;
friend std::ostream& operator<<(std::ostream&, const Mesh&);
\end{lstlisting}
\vspace{.2cm}
\subsubsection*{Example}
We give a basic example of using the \class{Mesh} class. It concerns the meshing of the rectangle
\([0,1]\times[1,3]\) with regular P1 geometric elements (\(20\) by \(40\)) where the boundary \([0,1]\times\{1\}\)
is named Gamma\_1 and the boundary \(\{0\}\times[1,3]\) is named Gamma\_2:
\vspace{.1cm}
\begin{lstlisting}
std::vector<String> sidenames(4,"");sidenames[0]="Gamma_1";sidenames[2]="Gamma_2";
Mesh mesh2d(Rectangle(0,1,1,3),_triangle,20,40,sidenames,"P1 mesh of [0,1]x[1,3]");
mesh2d.buildSides();   //build sides list
verboseLevel(3);std::cout<<mesh2d;
\end{lstlisting}
\vspace{.2cm}
It produces the following output:
\vspace{.1cm}
\scriptsize
\begin{verbatim}
Mesh'P1 mesh of [0,1]x[1,3]'
  space dimension : 2, element dimension : 2
  Geometry rectangle [0,1]x[1,3] of dimension 2, BoundingBox [0,1]x[1,3], names of variable : x y 
  number of elements : 1600, number of vertices : 861, number of nodes : 861, number of domains : 3 ( Omega Gamma_1 Gamma_2 )
list of elements (1600) : 
geometric element 1 : triangle_Lagrange_1, orientation +1 measure = 0.00125
   nodes : 2 ->(0.05, 0) 22 ->(0, 0.05) 1 ->(0, 0) 
   vertices : 2 ->(0.05, 0) 22 ->(0, 0.05) 1 ->(0, 0)  
   measure of sides = 0.0707107 0.05 0.05
   sides : 61 21 1    sideOfSides : unset   adjacent elements : 2 
geometric element 2 : triangle_Lagrange_1, orientation +1 measure = 0.00125
   nodes : 22 ->(0, 0.05) 2 ->(0.05, 0) 23 ->(0.05, 0.05) 
   vertices : 22 ->(0, 0.05) 2 ->(0.05, 0) 23 ->(0.05, 0.05)  
   measure of sides = 0.0707107 0.05 0.05
   sides : 61 62 63    sideOfSides : unset   adjacent elements : 1 3 41 
geometric element 3 : triangle_Lagrange_1, orientation +1 measure = 0.00125
   nodes : 3 ->(0.1, 0) 23 ->(0.05, 0.05) 2 ->(0.05, 0) 
   vertices : 3 ->(0.1, 0) 23 ->(0.05, 0.05) 2 ->(0.05, 0)  
   measure of sides = 0.0707107 0.05 0.05
   sides : 64 62 2    sideOfSides : unset   adjacent elements : 2 4 
\ldots
geometric element 1600 : triangle_Lagrange_1, orientation +1 measure = 0.00125
   nodes : 860 ->(0.95, 2) 840 ->(1, 1.95) 861 ->(1, 2) 
   vertices : 860 ->(0.95, 2) 840 ->(1, 1.95) 861 ->(1, 2)  
   measure of sides = 0.0707107 0.05 0.05
   sides : 2458 2459 2460    sideOfSides : unset   adjacent elements : 1599 
list of vertices (861) : 
1 -> (0, 0)
2 -> (0.05, 0)
3 -> (0.1, 0)
\ldots
861 -> (1, 2)
list of nodes (861) : 
1 -> (0, 0)
2 -> (0.05, 0)
3 -> (0.1, 0)
\ldots
861 -> (1, 2)
list of sides (2460) : 
side 1 -> geometric side element 1601 : side 3 of element 1 
side 2 -> geometric side element 1602 : side 3 of element 3 
side 3 -> geometric side element 1603 : side 3 of element 5 
\ldots
side 2460 -> geometric side element 4059 : side 3 of element 1600 
list of sides of sides (0) : unset
list of domains (3) : 
 Domain 'Omega' of dimension 2 from mesh 'P1 mesh of [0,1]x[1,3]'
geometric element 1 : triangle_Lagrange_1, orientation +1 measure = 0.00125
geometric element 2 : triangle_Lagrange_1, orientation +1 measure = 0.00125
geometric element 3 : triangle_Lagrange_1, orientation +1 measure = 0.00125
\ldots
geometric element 1600 : triangle_Lagrange_1, orientation +1 measure = 0.00125
 Domain 'Gamma_1' of dimension 1 from mesh 'P1 mesh of [0,1]x[1,3]'
geometric side element 1601 : side 3 of element 1 
geometric side element 1602 : side 3 of element 3 
geometric side element 1603 : side 3 of element 5 
\ldots
geometric side element 1620 : side 3 of element 39 
 Domain 'Gamma_2' of dimension 1 from mesh 'P1 mesh of [0,1]x[1,3]'
geometric side element 1621 : side 2 of element 1 
geometric side element 1622 : side 2 of element 41 
geometric side element 1623 : side 2 of element 81 
\ldots
geometric side element 1660 : side 2 of element 1561 
\end{verbatim}
\normalsize


\subsection{Meshing tools}\label{ss.meshtools}

In this section, we will study in details how work the \class{Mesh} constructors from \class{Geometry}.

\begin{lstlisting}[deletekeywords={[3] name, order}]
Mesh(const Geometry& g, Number order = 1, MeshGenerator mg = _defaultGenerator, const String& name = ""); // constructor from 1D geometry
Mesh(const Geometry& g, ShapeType sh, Number order = 1, MeshGenerator mg = _defaultGenerator, const String& name = ""); // constructor from 2D and 3D geometries
\end{lstlisting}

The \class{MeshGenerator} argument can have the following values :

\begin{lstlisting}
enum MeshGenerator
{
  _defaultGenerator=0,
  _structured, structured=_structured,
  _subdiv, subdiv=_subdiv,
  _gmsh, gmsh=_gmsh
};
\end{lstlisting}

We will now look at the behavior of the different generators :

\subsubsection{Structured meshes}

The "structured" generator works only on \class{Segment}, \class{Rectangle}, and \class{Parallelepiped}
geometries, and for P1, Q1, prism 1 or pyramid 1 finite elements. In this way, you call the following underlying methods :

\begin{lstlisting}
void meshP1Segment(const Segment&, Number, const std::vector<String>&, const String& na="");
void meshP1Parallelogram(Parallelogram&, number_t, number_t, const std::vector<string_t>&, 
                         set<MeshOption> mos=set<MeshOption>());
void meshQ1Parallelogram(Parallelogram&, number_t, number_t, const std::vector<string_t>&);
void meshP1Parallelepiped(const Parallelepiped& , Number, Number, Number, const vector<String_t>&,const String& na="");
void meshQ1Parallelepiped(const Parallelepiped& , Number, Number, Number, const vector<String>&,const String& na="");
void meshPr1Parallelepiped(Parallelepiped& , number_t, number_t, number_t, const vector<string_t>&,const string_t& na="");
void meshPy1Parallelepiped(Parallelepiped& , number_t, number_t, number_t, const vector<string_t>&,const string_t& na="");
\end{lstlisting}
\vspace{1mm}
The mesh options allowed in \cmd{meshP1Parallelogram} are related to the splitting of elementary parallelograms in two triangles :  \verb?_leftSplit? to split along the lines \(x+y=c\) (default), \verb?_rightSplit? to split along the lines \(x-y=c\), \verb?_alternateSplit? to split alternately along the lines \(x+y=c\) and \(x-y=c\), and \verb?_randomSplit? to split randomly.

\subsubsection{Meshes with {\em subdivision} algorithm}

The subdivision algorithm enables to mesh a wide set of geometries with triangles, quadrangles, tetrahedra
or hexahedra: \class{Ball}, \class{Cube}, \class{RevCylindricVolume} and \class{SetOfElems}. This
algorithm takes into account 2 main parameters : \var{nbsubdiv} and \var{order}. The first one controls
the number of subdivisions to be done (mesh refinement), and the second one is the polynomial order of
approximation of the geometry, which can take any (positive integer) value.
\par\smallskip
To use subdivision algorithm, dedicated constructors for the geometries are available:
\par\medskip
\begin{lstlisting}[deletekeywords={[3] x, y, z, edgeLen, type}]
Cube(Real x, Real y, Real z, Real edgeLen = 1., int nboctants=1, number_t nbsubdiv=0);
Cube(const Point& p1, const Point& p2, const Point& p3, const Point& p4, int nboctants, number_t nbsubdiv);
Cube(Real x, Real y, Real z, Real edgeLen, Real theta1, Dimen axis1, int nboctants=1, number_t nbsubdiv=0);
Cube(Real x, Real y, Real z, Real edgeLen, Real theta1, Dimen axis1, Real theta2, Dimen axis2, int nboctants=1, number_t nbsubdiv=0);
Cube(Real x, Real y, Real z, Real edgeLen, Real theta1, Dimen axis1, Real theta2, Dimen axis2, Real theta3, Dimen axis3, int nboctants=1, number_t nbsubdiv=0);
Ball(Real x, Real y, Real z, Real radius = 1., int nboctants=8, number_t nbsubdiv=0, number_t type=1);
Ball(Real x, Real y, Real z, Real radius, Real theta1, Dimen axis1, int nboctants=8, number_t nbsubdiv=0, number_t type=1);
Ball(Real x, Real y, Real z, Real radius, Real theta1, Dimen axis1, Real theta2, Dimen axis2, int nboctants=8, number_t nbsubdiv=0, number_t type=1);
Ball(Real x, Real y, Real z, Real radius, Real theta1, Dimen axis1, Real theta2, Dimen axis2, Real theta3, Dimen axis3, int nboctants=8, number_t nbsubdiv=0, number_t type=1);
RevCylindricVolume(Point p1, Point p2, Real radius, int nbslices=0, number_t nbsubdiv=0, number_t type=1, CylinderEndShape endShape1=_cesFlat, CylinderEndShape endShape2=_cesFlat, Real distance1=0., Real distance2=0.);
SetOfElems(const std::vector<Point>& pts, const std::vector<std::vector<number_t> >& tri, const std::vector<std::vector<number_t> >& bound, const number_t nbsubdiv=1);
\end{lstlisting}
\par\medskip
The parameter {\ttfamily nboctants} determines which part of the geometry is to be meshed ; it is clarified in the
following figures :

\begin{figure}[H]
 \begin{center}
  \includePict[scale=0.9]{P1VolMeshTetSphere1.pdf}
  \includePict[scale=0.9]{P1VolMeshTetSphere2.pdf}
  \includePict[scale=0.9]{P1VolMeshTetSphere3.pdf}
  \includePict[scale=0.9]{P1VolMeshTetSphere4.pdf}
 \end{center}
 \begin{center}
  \includePict[scale=0.9]{P1VolMeshTetSphere5.pdf}
  \includePict[scale=0.9]{P1VolMeshTetSphere6.pdf}
  \includePict[scale=0.9]{P1VolMeshTetSphere7.pdf}
  \includePict[scale=0.9]{P1VolMeshTetSphere8.pdf}
 \end{center}
 \caption{Meshes of the different portions of the sphere according to the number of octants.}
 \label{sphere_meshes}
\end{figure}

\begin{figure}[H]
 \begin{center}
  \includePict[scale=0.8]{P1VolMeshHexCube1.pdf}
  \includePict[scale=0.8]{P1VolMeshHexCube2.pdf}
  \includePict[scale=0.8]{P1VolMeshHexCube3.pdf}
  \includePict[scale=0.8]{P1VolMeshHexCube4.pdf}
 \end{center}
 \begin{center}
  \includePict[scale=0.8]{P1VolMeshHexCube5.pdf}
  \includePict[scale=0.8]{P1VolMeshHexCube6.pdf}
  \includePict[scale=0.8]{P1VolMeshHexCube7.pdf}
  \includePict[scale=0.8]{P1VolMeshHexCube8.pdf}
 \end{center}
 \caption{Meshes of the different portions of the cube according to the number of octants.}
 \label{cube_meshes}
\end{figure}


In this way, you call the following underlying methods :

\begin{lstlisting}[deletekeywords={[3] name, type, order, shape, bounds}]
void subdvMesh(Ball& sph, const ShapeType shape, const int nboctants, const number_t nbsubdiv=0, const number_t order=1, const number_t type=1, const string_t& name = "", const string_t& TeXFilename = "");
void subdvMesh(Cube& cub, const ShapeType shape, const int nboctants, const number_t nbsubdiv=0, const number_t order=1, const string_t& name = "", const string_t& TeXFilename = "");
void subdvMesh(RevTrunk& cyl, const ShapeType shape, const number_t nbSubDomains=1, const number_t nbsubdiv=0, const number_t order=1, const number_t type=1, const string_t& name = "", const string_t& TeXFilename = "");
void subdvMesh(Disk& carc, const ShapeType shape, const number_t nbsubdiv, const number_t order, const number_t type, const string_t& name, const string_t& TeXFilename);
void subdvMesh(const std::vector<Point>& pts, const std::vector<std::vector<number_t> >& elems, const std::vector<std::vector<number_t> >& bounds, const ShapeType shape, const number_t nbsubdiv=0, const number_t order=1, const string_t& name = "", const string_t& TeXFilename = "");
\end{lstlisting}

\subsubsection{Meshes with nested calls to \gmsh}

This generator enables to mesh whatever geometry, canonical, "composite" or "loop". To use it, you need \gmsh installed on your computer before \xlifepp is installed, in order to be detected. You can define meshes as if it was \gmsh directly, so that finite elements up to order 5 are allowed here.

In this way, you call the underlying external functions :

\begin{lstlisting}[deletekeywords={[3] order}]
void Mesh::saveToGeo(Geometry& g, number_t order, const string_t& filename);
void Mesh::saveToGeo(Geometry& g, ShapeType sh, number_t order, const string_t& filename);
void saveSegmentToGeo(Segment& s, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveEllArcToGeo(EllArc& a, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveCircArcToGeo(CircArc& a, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void savePolygonToGeo(Polygon& p, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveTriangleToGeo(Triangle& t, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveQuadrangleToGeo(Quadrangle& q, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveEllipseToGeo(Ellipse& e, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void savePolyhedronToGeo(Polyhedron& p, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveTetrahedronToGeo(Tetrahedron& t, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveHexahedronToGeo(Hexahedron& h, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveEllipsoidToGeo(Ellipsoid& e, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveTrunkToGeo(Trunk& t, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveCylinderToGeo(Cylinder& c, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveConeToGeo(Cone& c, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveRevTrunkToGeo(RevTrunk& t, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveRevCylinderToGeo(RevCylinder& c, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
void saveRevConeToGeo(RevCone& c, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids, bool withLoopsStorage = true, bool withSideNames = true);
\end{lstlisting}
\subsubsection{Meshes from extrusion}
The extrusion constructor can extrude any 1D or 2D mesh in the \(\vec{OD}\) direction using \(nbl\) layers of same width (regular extrusion). More precisely, any point \(M\) of the section mesh is extruded in \(nbl\) points :
\[M_k=M + O +k*\vec{OD}.\]  
When a 1D section, extruded mesh is made with quadrangles. When a 2D triangular mesh section, extruded mesh is made with prisms and  when a 2D quadrangular mesh section, extruded mesh is made with hexahedra.

\begin{lstlisting}[deletekeywords={[3] order}]
Mesh(const Mesh& ms, const Point& O, const Point& D , number_t nbl,
     number_t namingDomain=0, number_t namingSection=0, number_t namingSide=0,
     const string_t& meshName=""); //constructor
void buildExtrusion(const Mesh& ms, const Point& O, const Point& D, 
    number_t nbl, number_t namingDomain, number_t namingSection, 
    number_t namingSide, const string_t& meshName); //effective 
\end{lstlisting}

The boundary domains created by the extrusion process come from the boundary domains of the original section. This process is controlled by the 3 parameters \var{namingDomain}, \var{namingSection}, \var{namingSide} taking one of the values 0, 1 or 2, with the following rules:\\
\mbox{}\hspace{0.2cm}\(\bullet\) 0 : domains are not created \\
\mbox{}\hspace{0.2cm}\(\bullet\) 1 : one extruded domain is created for any domain of the original section \\
\mbox{}\hspace{0.2cm}\(\bullet\) 2 : for each layer, one extruded domain is created for any domain of the original section \\
\\
Be cautious, the side domains of extruded domain are created from side domains of the given section. Thus, if the given section has no side domains, the extruded domains will have no side domains! The naming convention is the following:\\
\mbox{}\hspace{0.2cm}\(\bullet\) Domains and side domains keep their name with the extension "\_e"  or "\_e1", "\_e2", \ldots, "\_en" \\
\mbox{}\hspace{0.2cm}\(\bullet\) Section domains have the name of original domains with the extension "\_0",\ldots,"\_n"  \\
\mbox{}\hspace{0.2cm}\(\bullet\) When \var{namingDomain=0}, the full domain is always created and named "Omega".\\ 

The figure \ref{mesh_extrusion} illustrates the naming rules of domains.
\begin{figure}[H]
\begin{center}
\includePict[scale=0.8]{mesh_extrusion.png}
\end{center}
\caption{Mesh extrusion, domains naming}
\label{mesh_extrusion}
\end{figure}
Note that the \var{ meshPr1Parallelepiped} function uses this extrusion process.

\subsubsection{Meshes from element splitting}
Sometimes it may be useful to split elements into elements of another type, for instance to produce mesh of pyramids that are not provided by standard meshing software programs. To do this, a general constructor is offered:
\begin{lstlisting}[deletekeywords={[3] order}]
Mesh(const Mesh& mesh, ShapeType sh, const string_t name="");
\end{lstlisting}
that calls the effective functions that processes the splitting.\\

Up to now, only one splitting function is available:
\begin{lstlisting}[deletekeywords={[3] order}]
void buildPyramidFromHexadron(const Mesh& hexMesh,const string_t& meshName="");
\end{lstlisting}
that produces a pyramid mesh from a hexahedron mesh.\\

Note that the \cmd{meshPy1Parallelepiped} function uses this splitting function.


\displayInfos{library=geometry, header=Mesh.hpp, implementation={Mesh.cpp, subdivision/SubdvMesh.cpp}, test=test\_Mesh.cpp,
header dep={config.h, utils.h, GeomElement.hpp, Domain.hpp, Geometry.hpp}}

