%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{SuTermMatrix} class}

The \class{SuTermMatrix} class carries numerical representation of a single unknown pair bilinear form and more generally any matrix attached to a pair of unknowns (row unknown and row column).

\begin{focusbox}
By convention, 'u' refers to column (unknown of bilinear form) and 'v' to row (test function in bilinear form).
\end{focusbox}

\begin{lstlisting}
class SuTermMatrix : public Term
{protected :
 SuBilinearForm* sublf_p;            //bilinear form through pointer
 const Unknown* u_p;                 //column unknown
 const TestFunction* v_p;            //row unknown
 mutable Space* space_u_p;           //pointer to u-space
 mutable Space* space_v_p;           //pointer to v-space
 vector<Space *> subspaces_u;   //u-subspaces involved
 vector<Space *> subspaces_v;   //v-subspaces involved
 MatrixEntry* entries_p;             //matrix as LargeMatrix
 MatrixEntry* scalar_entries_p;      //scalar matrix as LargeMatrix
 vector<DofComponent> cDoFs_u;  //component u-DoF list
 vector<DofComponent> cDoFs_v;  //component v-DoF list
 MatrixEntry* rhs_matrix_p;          //correction matrix when essential cond.
} 
\end{lstlisting}
\vspace{.3cm}
The \verb?sublf_p? contains a pointer to a \class{SuBiLinearForm} (single unknown pair bilinear form). It may be null, that means there is no explicit bilinear form attached to the \class{SuTermMatrix}. \\
The \verb?space_u_p? (resp. \verb?space_u_p?) pointer points to the largest subspace involved in \class{SuTermMatrix} columns (resp. rows). These pointers should never be null because they carry the row and column DoFs numbering. For this reason too, the largest subspaces have to be correctly updated during any operation on \class{SuTermMatrix}. The \class{Space* } vector \verb?subspaces_u? (resp. \verb?subspaces_v?) contains the subspaces (\underline{as subspace of the largest space}) attached to basic bilinear forms defined in the \class{SuBiLinearForm} pointer (if defined), see \verb?buildSubspaces? function.\\

\begin{warningbox}
Do not confuse the \verb?space_u_p? pointer and \verb?u_p->space_p? pointer that specifies the whole space. They may be the same!
\end{warningbox}

The numerical representation of matrix is defined in the \verb?entries_p? \class{MatrixEntry } pointer as a real/complex matrix or a real/complex matrix of matrices, regarding the value type (real or complex) and the structure of the unknowns (scalar or vector). If required, matrix of matrices representation may be expanded to a matrix of scalars stored in the  \verb?scalar_entries_p? \class{MatrixEntry} pointer. In that case, the \verb?cDoFs_u? and \verb?cDoFs_v? vectors of \class{DofComponent } give the row and column numbering (in \class{DofComponent}) of entries. Note that if \class{SuTermMatrix} is of scalar type, \verb?scalar_entries_p = entries_p?.\\
In case of essential conditions applied to, \verb?rhs_matrix_p? \class{MatrixEntry} pointer may be allocated to store the eliminated part of \class{SuTermMatrix}. Note that, \class{SuTermMatrix} class does not manage \class{SetOfConstraints} objects, the treatment of essential condition being driven by  \class{TermMatrix}!\\

The \class{SuTermMatrix} class also handles HMatrix (hierarchical matrix):
\vspace{.1cm}
\begin{lstlisting}
HMatrixEntry<FeDof>* hm_entries_p; //hierarchical matrix 
HMatrixEntry<FeDof>* hm_scalar_entries_p; //scalar hierarchical matrix 
ClusterTree<FeDof>*  cluster_u;    //column cluster tree
ClusterTree<FeDof>*  cluster_v;    //row cluster tree
\end{lstlisting}
\vspace{.2cm}
HMatrix stuff is described in chapter \ref{chap_hmatrix}. Let's remember that the hierarchical matrix are built from the clustering of row and column indices ({\ttfamily cluster\_u, cluster\_v}). There are two HMatrixEntry pointers, one to handle any matrix (scalar or matrix), the other one to handle the scalar representation of a matrix of matrices if it is required. When HMatrix is a scalar one, both the pointers are the same.\\
 
The \class{SuTermMatrix} class provides some constructors from bilinear form, a constructor from SuTermVector (diagonal matrix), a constructor from row dense matrix, a constructor from linear combination and a copy constructor: 
\vspace{.1cm}
\begin{lstlisting}
// basic constructor
SuTermMatrix(const Unknown*, Space*, const Unknown*, Space*, MatrixEntry*, const String& na=""); 
//constructor from bilinear form 
SuTermMatrix(SuBilinearForm* sublf = 0, const String& na="", bool noass=false);                   
SuTermMatrix(SuBilinearForm* sublf = 0, const String& na="", 
             ComputingInfo cp= ComputingInfo());  
SuTermMatrix(SuBilinearForm*, const Unknown*, const Unknown*, Space*, Space*,
             const vector<Space *>&, const vector<Space *>&, 
             const String&, MatrixEntry* =0);
//constructor of diagonal matrix
SuTermMatrix(SuTermVector &, const String& na="");        
SuTermMatrix(const Unknown*, Space*, const Unknown*, Space*, SuTermVector&,
              StorageType=_noStorage, AccessType=_noAccess, const String& na="");                         
void diagFromSuTermVector(const Unknown*, Space*, const Unknown*,Space*, SuTermVector&,
                          StorageType=_noStorage, AccessType=_noAccess,
                          const String& na=""); 
// constructor from a row dense matrix
SuTermMatrix(const Unknown*, Space*, const Unknown*, Space*, const vector<T>&, number_t, number_t, const string_t& ="");
//constructor from linear combination          
SuTermMatrix(const LcTerm&,const String& na="");
//copy constructor and assign operator
SuTermMatrix(const SuTermMatrix&,const String& na=""); 
void copy(const SuTermMatrix&);                                  
SuTermMatrix& operator=(const SuTermMatrix&);
//destructor, clear
virtual ~SuTermMatrix();
void clear();           
\end{lstlisting}
\vspace{.2cm}
There is no specific constructor for SuTermMatrix of type Hmatrix. It is induced by some properties of the bilinear form; more precisely by a particular choice of integration method.\\
  
It provides a lot of accessors and property functions:
\vspace{.1cm}
\begin{lstlisting}
TermType termType() const;              
ValueType valueType() const;
StrucType strucType() const;           
Space& space_u() const;
Space& space_v() const;
Space* space_up() const;
Space* space_vp() const;
Space*& space_up();
Space*& space_vp();
const Unknown* up() const;
const Unknown* vp() const;
const Unknown*& up();
const Unknown*& vp();
MatrixEntry*& entries();
const MatrixEntry* entries() cons;
MatrixEntry*& scalar_entries();
const MatrixEntry* scalar_entries() const;
MatrixEntry*& rhs_matrix();
const MatrixEntry* rhs_matrix() const;
const vector<DofComponent>& cDoFsu() const;
const vector<DofComponent>& cDoFsv() cons;
vector<DofComponent>& cDoFsu();
vector<DofComponent>& cDoFsv();
SymType symmetry() const;               
StorageType storageType() const;
MatrixStorage* storagep() const;        
void setStorage(StorageType, AccessType);
void toStorage(StorageType, AccessType);
\end{lstlisting}
\vspace{.2cm}
The main computation functions and related stuff follow:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] set}]
void compute();                                  
void compute(const LcTerm&,const String& na=""); 
template<unsigned int CM>
void compute(const vector<SuBilinearForm>&, ValueType, StrucType); 
template<typename T, typename K>
void computeIR(const SuLinearForm&f, T*, K&, const vector<Point>&);    

void buildSubspaces();
void setStorage(StorageType, AccessType);
void toStorage(StorageType, AccessType);     
void addDenseToStorage(const vector<SuBilinearForm>&,MatrixStorage*) const;
void toScalar(bool keepEntries=false);           
vector<SuBilinearForm> getSublfs(ComputationType, ValueType&) const;
void updateStorageType(const vector<SuBilinearForm>&,set<Number>&,
                       set<Number>&,StorageType&) const;
\end{lstlisting}
\vspace{.2cm}
\class{SuTermMatrix} may be modified by the following algebraic operators:
\vspace{.1cm}
\begin{lstlisting}
template<typename T>
SuTermMatrix& operator*=(const T&);           
template<typename T>
SuTermMatrix& operator/=(const T&);
SuTermMatrix& operator+=(const SuTermMatrix&);
SuTermMatrix& operator-=(const SuTermMatrix&);
friend SuTermVector operator*(const SuTermMatrix&, const SuTermVector&); 
friend SuTermMatrix operator*(const SuTermMatrix&, const SuTermMatrix&); 
\end{lstlisting}
\vspace{.2cm}
It is also possible to access to one matrix coefficient, to change it, to assign a given value to a row or a column:
\vspace{.1cm}
\begin{lstlisting}
Value getValue(number_t, number_t) const;           
Value getScalarValue(number_t, number_t, dimen_t=0, dimen_t=0) const;  
void  setValue(number_t, number_t, const Value&);                          
void  setScalarValue(number_t, number_t, const Value&, dimen_t=0, dimen_t=0);
void  setRow(const Value&, number_t r1, number_t r2);  
void  setCol(const Value&, number_t r1, number_t r2);   
number_t rowRank(const Dof&) const;      
number_t rowRank(const DofComponent&) const; 
number_t colRank(const Dof&) const;            
number_t colRank(const DofComponent&) const;   
\end{lstlisting}
\vspace{.1cm}
The row/col index may be either numbers or DoFs; the link between DoF and index number is got using \cmd{rowRank}, \cmd{colRank} functions.\\

\begin{focusbox}
The \cmd{setValue}, \cmd{setRow}, \cmd{setCol} functions never modify the matrix storage!
\end{focusbox}

Finally, there are some print and export stuff:

\begin{lstlisting}
void print(ostream&) const;        
void print(ostream&, bool) const;  
void viewStorage(ostream&) const; 
void saveToFile(const String&, StorageType, bool=false) const;
\end{lstlisting}

Most of the previous functions works when SuTermMatrix handles a HMatrix, but some not. As there is no check, be cautious! Besides, there are some specific functions related to Hmatrix representation:

\begin{lstlisting}
bool hasHierarchicalMatrix() const; 
HMatrix<T,FeDof>& getHMatrix(StrucType str=_undefStrucType) const;  
\end{lstlisting}

\displayInfos{
library=term, 
header=SuTermMatrix.hpp, 
implementation=SuTermMatrix.cpp,
test=test\_TermMatrix.cpp,
header dep={Term.hpp, LcTerm.hpp, termUtils.hpp, form.h, config.h, utils.h}
}
