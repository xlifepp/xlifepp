%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Messages management}

Handling error, warning and info messages is an essential part of a code. In C++, a choice could
be using exceptions. As \xlifepp is a very large code, with a high number of levels (routines
called by routines themselves called by routines \ldots), using exceptions has a lot of constraints.
The other way is to develop our own classes.  The message machinery works as follows:
\begin{itemize}
\item A message is a string of characters including free text and particular codes (as much
as you want) of the form \%s, \%i, \%r, \%c and \%b which will be substituted respectively
by a string, an integer, a real, a complex and a boolean.
\item For each message is defined a unique string identifier, a type (ERROR, WARNING or INFO),
a runtime behavior (STOP or CONT) and an output console flag (STDOUT or NOSTDOUT).
\item All the messages are listed in a file named \textit{messages.txt} in the directory \textit{etc/Messages/lang}
where \textit{lang} is the language code; for instance \textit{en} for the English, \textit{fr}
for the French, \ldots
\item By translating the English reference \textsl{messages.txt} file in another language
it is easy to internationalize the \xlifepp code.
\end{itemize}
For instance, the beginning of the English \textit{messages.txt} is 
\begin{verbatim}
   #/
   ============================ internal messages =================================

   # logon INFO CONT STDOUT Log file "%s" activated.
   # logoff INFO CONT STDOUT Log file "%s" deactivated.
   # errorundef ERROR STOP STDOUT Error message of type %s and id %i undefined (%s)!
   This might occur with a double string message Id in a message.txt file
   or the forgetting of the message type, the behavior type or the output type
   \ldots
\end{verbatim}
Note that a message format may be defined on several lines (the next character \# is the end
delimiter of a message) and a comment begins by \#/. \\


The message management is based on three classes: \class{MsgData} (storage of the values to
be included), \class{MsgFormat} (multilingual message format) and \class{Messages}, the main
class collecting all formats and methods to produce and output the formatted messages.

\subsection{The \classtitle{MsgData} class}

The aim of the \class{MsgData} class is to store in the same structure, all sorts of variables
to use to build a message, of various types: integer, real, complex, string and bool. Thus,
this class proposes as private members:
\vspace{.1cm}
\begin{lstlisting}
class MsgData {
  private :
    std::vector<int> i_;         // to store int type data
    std::vector<real_t> r_;      // to store real_t type data
    std::vector<complex_t c_;    // to store complex_t type data
    std::vector<String> s_;      // to store string data
    std::vector<bool> b_;        // to store boolean data
    bool read_;                  // flag to specify that the structure has been read
  \ldots
};
\end{lstlisting}
\vspace{.2cm}
It offers:

\begin{itemize}
\item a default constructor:
\vspace{.1cm}
\begin{lstlisting}
MsgData() : i_(0), r_(0), c_(0), s_(0), b_(0), read_(true) {}
\end{lstlisting}
\vspace{.2cm}
\item some public access functions to data
\begin{lstlisting}
yyy xxxParameter(const unsigned int n);
// where (xxx,yyy)=(int,int), (real,real_t), (complex,complex_t), (string,string), (boolean,bool)
bool read();
\end{lstlisting}
\vspace{.2cm}
\item a public method to specify data was read
\vspace{.1cm}
\begin{lstlisting}
void readData();
 \end{lstlisting}
\vspace{.2cm}
\item some public append methods for each managed data type or variant (unsigned int or char*
for example)
\vspace{.1cm}
\begin{lstlisting}
void push(const T t); // T is int, real_t, string, \ldots
 \end{lstlisting}
\vspace{.2cm}
\item some public streaming operators, appending data of various types
\vspace{.1cm}
\begin{lstlisting}
MsgData& operator<<(const T i) // T is int, real_t, string, \ldots
\end{lstlisting}
\vspace{.2cm}
\item a private reset method, to reset every list of data
\vspace{.1cm}
\begin{lstlisting}
void reset_();
\end{lstlisting}
\end{itemize}

\subsection{The \classtitle{MsgFormat} class}

The \class{MsgFormat} Object contains a message format for output. In addition, it contains
information on message such as the message type, the message behavior, the message output
and the message id. Thus, the \class{MsgFormat} class proposes as private members:
\vspace{.1cm}
\begin{lstlisting}
enum MsgType {_error=0,_warning,_info};

class MsgFormat {
  private :
    String format_;        // format of message
    MsgType type_;         // flag for warning/error
    bool stop_;            // flag for stop/continue
    bool consoleOut_;      // flag for output to console
    String ids_;           // string id of format
  \ldots
};
\end{lstlisting}

It offers:

\begin{itemize}
\item Two constructors, the default one and the full one:
\vspace{.1cm}
\begin{lstlisting}
MsgFormat(const String &ms, const String &ids, MsgType t, bool s, bool c);
\end{lstlisting}
\vspace{.2cm}
\item Some accessors, one for each attribute:
\vspace{.1cm}
\begin{lstlisting}
String format() const {return format_;}
MsgType type() const {return type_;}
bool stop() const {return stop_;}
bool console() const {return consoleOut_;}
String stringId() const {return ids_;}
\end{lstlisting}
\end{itemize}

\subsection{The \classtitle{Messages} class}

The \class{Messages} class manages the list of messages format and the display of every message.
Thus, this class proposes as private members:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] map}]
class Messages {
  private :
    String msgType_;                          // message type
    std::map<String,MsgFormat*> stringIndex_; // index of messages by string id
    std::ofstream* msgStream_p;               // file stream where are sent messages
    String msgFile_;                          // file where are sent messages
  \ldots
};
\end{lstlisting}

It offers:

\begin{itemize}
\item two constructors, the default one and the full one:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] msgFile, msgType}]
Messages(const std::string& file, std::ofstream& out, const std::string& msgFile, const std::string& msgType = std::string("XLiFE++"));
\end{lstlisting}
\vspace{.2cm}
\item Some accessors:
\vspace{.1cm}
\begin{lstlisting}
String msgType() const;
String msgFile();
std::ofstream* msgStream() const;
\end{lstlisting}
\vspace{.2cm}
\item Some utilities:
\vspace{.1cm}
\begin{lstlisting}
int numberOfMessages() const;
void loadFormat(const String&);     // load format from a file
void printList(std::ofstream&);     // print the list of all messages
MsgFormat* find(const String&);     // find an error format
void append(MsgFormat&);            // append a new message format in list
void appendFromFile(const String&); // append a new message format in list
\end{lstlisting} 
\end{itemize}

\subsection{External functions}

To throw messages, some external functions with useful aliases and shortcuts functions are provided.
The general functions are:
\vspace{.2cm}
\begin{lstlisting}[deletekeywords={[3] msgType}]
String message(const String& msgIds, MsgData& msgData=theMessageData, 
               Messages* msgSrc=theMessages_p);                           
void msg(const String& msgIds, MsgData& msgData, MsgType& msgType, Messages* msgSrc);               
\end{lstlisting}
\vspace{.1cm}
The first one ({\itshape message}) reads the message format and replaces the strings \%i, \%r, \%c, \%s
and \%b by their values given by the \class{MsgData} object. 
It produces a \class{String} containing the formatted message. This function is called by the
{\itshape msg} function which outputs the formatted message to the console or to a print file. 
Note that this function may be also called by any function to retrieve the formatted message
without output. \\
The \textit{msg} function is called by the three useful functions:
\vspace{.2cm}
\begin{lstlisting}[frame=single]
void error(const String& msgIds, MsgData& msgData=theMessageData, 
           Messages* msgSrc=theMessages_p); 
void info(const String& msgIds, MsgData& msgData=theMessageData, 
          Messages* msgSrc=theMessages_p); 
void warning(const String& msgIds, MsgData& msgData=theMessageData, 
             Messages* msgSrc=theMessages_p);
\end{lstlisting}
which are aliases for the {\itshape msg} function, giving the value of the {\ttfamily msgType} argument.
These are the functions to be used when you need to throw messages in other classes of \xlifepp
library. \\

Besides, for sake of simplicity, some shortcut functions are also provided. They avoid to manage
explicitly the \class{MsgData} object storing the message values. These shortcut functions
are template functions (one, two or three template parameters), so they only work up to three
message values! 
\vspace{.2cm}
\begin{lstlisting}
String message(const String& msgIds, const T& v, Messages* msgSrc=theMessages_p);
String message(const String& msgIds, const T1& v1,const T2& v2, 
               Messages* msgSrc=theMessages_p);
String message(const String& msgIds, const T1& v1,const T2& v2, const T3& v3, 
               Messages* msgSrc=theMessages_p);
void error(const String& msgIds, const T& v, Messages* msgSrc=theMessages_p);
void error(const String& msgIds, const T1& v1,const T2& v2, 
           Messages* msgSrc=theMessages_p);
void error(const String& msgIds, const T1& v1,const T2& v2,const T3& v3, 
           Messages* msgSrc=theMessages_p);
//the same for warning and info functions
\end{lstlisting}

\subsection{Throwing messages}

There is one global \class{MsgData} object: \textit{theMessageData} which is used as default
argument of message functions. Using it, it is very easy to throw messages. For instance, these
lines show how to throw an error message:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] x, y}]
 if(x.size()!=y.size())
    {theMessageData<<name()<<x.size()<<y.size();  //store the message values
     error("ker_bad_dim");                        //throw the error message ker_bad_dim
    }
\end{lstlisting} 
The message format \textit{ker\_bad\_dim} has the following definition:
\begin{verbatim}
  # ker_bad_dim ERROR STOP STDOUT Call of kernel \%s 
  with vectors of incompatible dimensions : (\%i,\%i)
\end{verbatim}

If you want to use international messages in your own output stream, you cannot use the function
\textit{info} because it outputs the message on the default print stream. You have to use the
function \textit{message} which returns the message as a \class{String} without printing output.
For instance:
\vspace{.1cm}
\begin{lstlisting}
std::ostream& operator<<(std::ostream& out,const Space & sp)
{   theMessageData<<sp.name()<<sp.typeOfSpace()<<sp.domain().name(); 
    out<<message("space_def");                                            
    return out;
}
\end{lstlisting} 
with the message format \textit{space\_def}:
\begin{verbatim}
  # space_def INFO CONT STDOUT space %s of type %s on the geometrical domain %s
\end{verbatim}
Using shortcut functions, the previous example reads:
\vspace{.1cm}
\begin{lstlisting}
std::ostream& operator<<(std::ostream& out,const Space & sp)
{   out<<message("space_def",sp.name(),sp.typeOfSpace(),sp.domain().name());                                            
    return out;
}
\end{lstlisting} 
 
\displayInfos{library=utils, header=Messages.hpp, implementation=Messages.cpp, test=test\_Messages.cpp,
header dep={config.h, String.hpp}}

