%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Parameters management}

It may be useful to have a data structure to store freely any kind of parameters (values, string,
\ldots) with a very simple interface for the end user. In particular, such structure could be
attached to user functions (see \autoref{s.function}). This is achieved with two classes
: the \class{Parameter} class which define one parameter and the \class{Parameters} class
which manages a list of \class{Parameter} objects.

\subsection{The \classtitle{Parameter} class}

The aim of the \class{Parameter} class is to encapsulate in the same structure, a data of
integer type, of real type, of complex type, of string type, of real vector/complex vector type, of real vector/complex matrix type or of pointer type (void pointer) with the
capability to name the parameter. Thus, this class proposes as private members:
\vspace{.1cm}
\begin{lstlisting}
class Parameter
{private :
  int i_;                  //!<to store int type data
  real_t r_;               //!<to store real_t type data
  complex_t c_;            //!<to store complex_t type data
  String s_;               //!<to store string data
  void * p_;               //!<to store pointer
  String name_;            //!<parameter name
  number_t type_;          //!<type of value
  \ldots
  }
\end{lstlisting} 
\vspace{.2cm}
It offers \\
- a copy constructor and constructors associated to each managed type T (\emph{int, real\_t,
complex\_t, string, char*, void *, real vector, complex vector, real matrix, complex matrix}), with optional naming:
\vspace{.1cm}
\begin{lstlisting}
Parameter(const T, const String& nm = String(""));   
Parameter(const Parameter&, const String& nm = String(""));
\end{lstlisting}
\vspace{.2cm}
- assignment operator = with specialized cases
\vspace{.1cm}
\begin{lstlisting}
Parameter& operator=(const Parameter& p);
Parameter& operator=(const T&);             
\end{lstlisting}
\vspace{.2cm}
The real/complex vector and real/complex matrix are managed using the void pointer p\_. More precisely, when a parameter is defined from a vector (or a matrix), for instance :
\begin{lstlisting}
Parameter par(std::vector<Real>(5,1.);          
\end{lstlisting}
a copy of the given vector is allocated, and its pointer is stored as a void pointer in member p\_. In assignment operator, the \emph{deletePointer} member function is used to free memory previously allocated.\\
\\
The class provides\\
- some access functions to data
\begin{lstlisting}
T get_s() const; //(T,s)=(int,i),(real_t,r),(complex_t,c),(String,s),(void *,p)
                 //(T,s)=(std::vector<real_t>&,rv),(std::vector<complex_t>&,cv)
                 //(T,s)=(Matrix<real_t>&,rm), (Matrix<complex_t>&,cm)
number_t get_value_type(const String&);
\end{lstlisting}
\vspace{.2cm}
- access to the parameter name and its type
\vspace{.1cm}
\begin{lstlisting}
string name();              //acces to name
void name(const String &);  //to change the name
number_t type() const;      //access to type
string_t typeName() const ; //access to type name
 \end{lstlisting}
\vspace{.2cm}
- some print and read facilities
\vspace{.1cm}
\begin{lstlisting}
ostream& operator<<(ostream&, const Parameter&);
void print(const Parameter&);
istream& operator>>(istream&, Parameter&);
 \end{lstlisting}
\vspace{.2cm}
- some control functions
\vspace{.1cm}
\begin{lstlisting}
void illegalOperation(const String& t1, const String& op, const String& t2) const;
void illegalOperation2(const String& op, const String& t) const;
void undefinedParameter(const String& t) const ;
void illegalDivision() const; 
\end{lstlisting}
\vspace{.2cm}
When it has meaning, the operator +=, -=, *=, /=, +, -, *, /, ==, !=, >, >=, <, <= are overloaded
for parameters, and it is possible to call standard mathematical functions (\emph{abs}, \emph{conj},
\emph{log}, \ldots) with a \emph{Parameter} object as argument. Obviously, these operations are not
allowed for pointer or string type parameter.
\\
The pointer type parameter has been introduced to give to the user the possibility to store
anything in a parameter (useful in the context of user functions). \textbf{It is the responsibility
of the users to well manage their void pointers !}\\

There are casting operators associated to the \class{Parameter} class. Some are explicit:
\vspace{.1cm}
\begin{lstlisting}
real_t real(const Parameter& p);
complex_t cmplx(const Parameter& p);
String str(const Parameter& p);
void * pointer(const Parameter& p);
\end{lstlisting}
\vspace{.2cm}
and the others are implicit (cast operator):
\vspace{.1cm}
\begin{lstlisting}
operator int();                    //!<cast to int
operator real_t();                 //!<cast to real_t
operator complex_t();              //!<cast to complex_t
operator String();                 //!<cast to String
operator void*();                  //!<cast to void*
operator std::vector<real_t>();    //!< cast to real vector
operator std::vector<complex_t>(); //!< cast to complex vector
operator Matrix<real_t>();         //!< cast to real matrix
operator Matrix<complex_t>();      //!< cast  to complex matrix
\end{lstlisting}
\vspace{.1cm}
These cast operators allow to write such syntax:
\begin{lstlisting}
Parameter pi(Real(3.1415926), "the real pi");
real_t x=pi;  //autocast
\end{lstlisting}  
\vspace{.1cm}
Unfortunately, casting a parameter to complex does not fully work:
\vspace{.1cm}
\begin{lstlisting}
Parameter i(complex_t(0,1)); //i is a complex parameter
complex_t j=i;               //is working  
j=i;                         //does not work !!!  
\end{lstlisting}
The compiler cannot resolve an ambiguity on the operator =  : \emph{complex\_t}=\emph{real\_t}
or \emph{complex\_t}=\emph{complex\_t}. The reason is that the autocast of the Parameter can
produce either a \emph{complex\_t} or \emph{real\_t} ! It is not possible to omit the \emph{complex\_t}
autocast version because then the form \emph{complex\_t j=i;} no longer works.  There is probably
a solution, but we did not find it !\\

The Parameter class is very simple to use, for instance:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] names}]
Parameter k(3.1415926,"k");              //named real parameter
Parameter i(complex(0.,1.),"i");         //named complex parameter
Parameter s("my string");                //no named string parameter
Parameter pv(RealVector(5,0.5),"vec 5");
Parameter pam(Matrix<Real>(3,_hilbertMatrix),"hilbert matrix");
Vector<String> mat_names(3);
mat_names(1)="iron";mat_names(2)="aluminium";mat_names(1)="air";
Parameter names(&mat_names,"mat_names"); //named pointer parameter
real_t vk=get_r();
Parameter ik=i*k;                        //operation on parameters
ik.name("i*k");                          //rename parameter
\end{lstlisting}
\vspace{.2cm}

\subsection{The \classtitle{Parameters} class: list of parameters}

The \class{Parameters} class manages a list of \class{Parameter} objects. Using stream operator
\(>>\), it offers a friendly way to add a parameter in the list and an index or string index
to access to the parameters of the list. It is based on the \emph{map} structure of the STL:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] map}]
class Parameters
{private :
  std::map<String,int> index_;      //!<associative list to manage string index
  std::vector<Parameter*> list_;    //!<list of parameter's pointers
}
\end{lstlisting}
The list of parameters pointers is stored in a vector thus providing direct access to a parameter
of the list. The associative map \emph{index\_} also provides direct access to a parameter
using a string index.\\

This class proposes constructors with a single parameter initialization with an optional name:
\begin{lstlisting}[deletekeywords={[3] name}]
Parameters();
Parameters(Parameter&);                                     
Parameters(const void*, const string_t& nm = string_t("")); 
template<typename T>
  Parameters(const T& t, const string_t& nm = string_t(""));
\end{lstlisting}
some list insertion facilities:
\begin{lstlisting}
void push(Parameter&);                    //!< append a parameter into list
Parameters& operator<<(Parameter*);       //!< insert a parameter into list 
Parameters& operator<<(Parameter&);       //!< insert a parameter into list 
Parameters& operator<<(const Parameter&); //!< insert a parameter into list 
Parameters& operator<<(const void*);      //!< insert a void pointer into list 
template<typename T>
  Parameters& operator<<(const T& t)     // insert a T value in list 
\end{lstlisting}
some direct access operators (by index, string index or parameter):
\begin{lstlisting}
Parameter& operator()(unsigned int);
Parameter& operator()(const String&);
Parameter& operator()(const char *);
Parameter& operator()(const Parameter&); 
\end{lstlisting}
and some print utilities:
\begin{lstlisting}
void print(std::ostream&) const ;
void print() const ;
friend void print(const Parameters&);
friend std::ostream& operator<<(std::ostream&, const Parameters&);
\end{lstlisting}

This parameter list is easy to use:
\begin{lstlisting}
Parameter k(3.1415926,"k");  //named real parameter
RealVector v(10,0.);         //a real vector
Parameter pv(v,"v1");        //associate vector v to a parameter pv 
Parameters params;           //new list of parameters   
params<<k<<"a string"<<pv;   //insert parameters or explicit data
RealVector x=params("v1");   //retrieve vector v1
\end{lstlisting}
When you retrieve a parameter from the list, you have to know the type of the parameter to
cast it in the right type. Note that the member function \emph{type} of the Parameter class
gives the parameter type.\\

There exists a \emph{defaultParameters} global variable declared in the header \emph{config.h}
and initialized as an empty list in the header \emph{globalScopeData.h}.

There exists an alias of \class{Parameters}: \class{Options} for the management of user options.

What is an option ? An option has:

\begin{itemize}
\item A name, that will be used to get the value of an option, as for a \class{Parameter}.

\begin{warningbox}
An option name cannot start with a sequence of "-" characters.
\end{warningbox}

\item One or several keys used to define an option (key in a file or command line option). A key cannot be used twice and some keys are forbidden, due to some keys dedicated to system options.
\item A value, that can be scalar (\class{Int}, \class{Number}, \class{Real}, \class{Complex} or \class{String}) or vector (\class{Ints}, \class{Numbers}, \class{Reals}, \class{Complexes} or \class{Strings}). This is the default value of the option and determines its data type. For vector types, size is not relevant.
\end{itemize}

This class provides a default constructor. In order to define a user option, you can use the function \cmd{addOption}:

\begin{lstlisting}
Options opts;
opts.addOption("t1", 1.2);
opts.addOption("t2", "tata");
opts.addOption("t3", "-t", Complex(0.,1.));
opts.addOption("t4", Strings("-tu","-tv"), Reals(1.1, -2.4, 0.7));
opts.addOption("t5", Strings("tata","titi"));
\end{lstlisting}

To parse options from file and or command line arguments, you may use one of the following line:

\begin{lstlisting}
opts.parse(argc, argv);
String filename="param.txt";
opts.parse(filename);
opts.parse(filename, argc, argv);
\end{lstlisting}

where {\ttfamily argv} and {\ttfamily argv} are the arguments of the {\ttfamily main} function dedicated to command line arguments of the executable. When using both filename and command line arguments, the latter are given priority.

\medskip

Let's now talk about how use options in a file or command-line.

First option is named "t1". So it can be used through the key "t1", "-t1" or "--t1". Fourth option is named "t4" and has 2 aliases: "-tu" et "-tv". So it can be used through the key "t4", "-t4", "--t4", "-tu" or "-tv".

\begin{lstlisting}
./exec --t1 2.5 -t2 tutu -t3 2.5 -0.1 -t4 2.2 -4.8 1.4  -t5 "tutu" toto "ta ta" titi
\end{lstlisting}

When passing options from command line, you consider a \class{Complex} value as 2 \class{Real} values.
Furthermore, quotes are not necessary for \class{String} values, unless the value contains special characters such as blank spaces, escape characters, \ldots Here is an example of ASCII file used to define options:

\begin{lstlisting}
t1 3.7
t2 "ti ti"
t3 (2.5,-0.2)
t4 2.5 -2 3.4 4.7
t5 "titi" tutu "ta ta" toto
\end{lstlisting}

When passing options from a file, a \class{Complex} value is now written as a complex, namely real part and imaginary part are delimited by a comma and inside parentheses. As for command-line case, quotes are not necessary for \class{String} values, unless the value contains special characters such as blank spaces, escape characters, \ldots

\displayInfos{library=utils, header=Parameters.hpp, implementation=Parameters.cpp, test=test\_Parameters.cpp,
header dep={config.h, String.hpp}}
