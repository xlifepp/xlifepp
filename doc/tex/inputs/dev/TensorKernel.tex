%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{TensorKernel} class}

The\class{TensorKernel} class inherited from \class{Kernel} handles some particular kernels, say tensor kernels:
\[
K(x,y)= \sum_{m,\, n } \phi_n(y) \mathbb{A}_{mn} \psi_m(x).
\]

This kernel is involved in DirichletToNeumann map.

\begin{focusbox}
In case of complex basis functions, the \class{TensorKernel} is often involved in integral of the following form:

\[
\sum _{mn} \mathbb{A}_{mn}\int_\Sigma w_j(y)*\overline{\phi}_n(y)\int_\Gamma \tau_i(x)*\psi_m(x)
\]

where appears a conjugate operation on \(\phi_n\) functions.\\
By default, computation functions do not apply the conjugate operation. So if you have to applied to, there is a flag to enforce the conjugate operation.
\end{focusbox}

Because, \class{Kernel} class uses \class{SpectralBasis} class to handles functions \(\phi_n\) and \(\psi_n\), it can not be located in \lib{utils} library. It is the reason why it is located  in \lib{term} library.
\vspace{.1cm}
\begin{lstlisting}
class TensorKernel : public Kernel
{
protected :
  const SpectralBasis* phi_p, *psi_p; //pointers to SpectralBasis object
  VectorEntry* matrix_p;              //matrix stored as a vector 
public :
  bool isDiag;          //!< true if matrix is diagonal matrix 
  Function xmap, ymap;  //!< maps applied to point x and y
  bool isConjugate;     //!< conjugate phi_p in IESP computation (default=false)
\end{lstlisting}
\vspace{.3cm}
\class{SpectralBasis} class manages either a family of explicit functions given by a \class{Function} object or a family of interpolated functions given as a vector of \class{TermVector} (see \class{SpectralBasisFun} class and \class{SpectralBasisInt} defined in \lib{space} library). Stored as a \class{MatrixEntry} object, the matrix \(\mathbb{A}\) may be of real or complex type.\\
The\class{TensorKernel} class provides some explicit constructors from \class{SpectralBasis} and vector<T> (matrix \(\mathbb{A}\)) and some implicit constructors from \class{Unknown} associated to a spectral space equipped with a spectral basis, and vector<T>. 
\vspace{.1cm}
\begin{lstlisting}
TensorKernel();
template<typename T>
 TensorKernel(const SpectralBasis&, const std::vector<T>&, bool =false); 
template<typename T>
 TensorKernel(const SpectralBasis&, const std::vector<T>&, const SpectralBasis&); 
template<typename T>
 TensorKernel(const Unknown&, const std::vector<T>&, bool =false); 
template<typename T>
  TensorKernel(const Unknown&, const std::vector<T>&, const Unknown&); 
template<typename T>
  TensorKernel(const std::vector<TermVector>&, const std::vector<T>&, bool =false); 
template<typename T>
  TensorKernel(const std::vector<TermVector>&, const std::vector<T>&, const std::vector<TermVector>&); 
~TensorKernel();                   
\end{lstlisting}
\vspace{.3cm}
Besides, it provides some accessors and properties:
\vspace{.1cm}
\begin{lstlisting}
const VectorEntry& vectorEntry() const {return *matrix_p;}
const SpectralBasis* phip() const {return phi_p;}
const SpectralBasis* psip() const {return psi_p;}
const SpectralBasis& phi()  const {return *phi_p;}
const SpectralBasis& psi()  const {return *psi_p;}
const TensorKernel* tensorKernel() const;
TensorKernel* tensorKernel();

Dimen dimOfPhi() const;
Dimen dimOfPsi() const;
Number nbOfPhi() const;
Number nbOfPsi() const;
bool phiIsAnalytic() const;
bool psiIsAnalytic() const;
virtual KernelType type() const;
virtual bool isSymbolic() const;
virtual ValueType valueType() const;         
virtual StrucType structType() const;  
bool sameBasis() const;
\end{lstlisting}
\vspace{.3cm}
It proposes two template functions that compute the product of \(\mathbb{A}_m\) or \(\mathbb{A}_{mn}\) by something of type T, expecting a result of same type!
\vspace{.1cm}
\begin{lstlisting}
template<typename T>
 T kernelProduct(Number, const T&) const;        
template<typename T>
 T kernelProduct(Number, Number, const T&) const;
\end{lstlisting}
\vspace{.3cm}

\displayInfos{library=operator, header=TensorKernel.hpp, implementation=TensorKernel.cpp,
test=test\_EssentialCondition.cpp, header dep={space.h, config.h, utils.h}}
