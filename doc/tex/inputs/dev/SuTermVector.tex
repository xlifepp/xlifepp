%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{SuTermVector} class}

The \class{SuTermVector} class carries numerical representation of a single linear form and more generally any vector attached to an approximation space. 
\vspace{.1cm}
\begin{lstlisting}
class SuTermVector : public Term
{
protected :
    SuLinearForm* sulf_p; 
    const Unknown* u_p;           
    mutable Space* space_p;          
    std::vector<Space*> subspaces;   
    VectorEntry* entries_p;                     
    VectorEntry* scalar_entries_p;    
    std::vector<DofComponent> cDoFs_;
    }     
\end{lstlisting}
\vspace{.3cm}
The \verb?sulf_p? contains a pointer to a \class{SuLinearForm} (single unknown linear form). It may be null, that means there is no explicit linear form attached to the \class{SuTermVector}. \\
The \verb?space_p? pointer points to the largest subspace involved in \class{SuTermVector}. This pointer should not be null because it carries the DoFs numbering of vector. For this reason too, the largest subspace has to be correctly updated during any operation on \class{SuTermVector}. The \class{Space* } vector \verb?subspaces? contains the subspaces (as subspace of the largest space) attached to basic linear forms defined in the \class{SuLinearForm} pointer (if defined), see \cmd{buildSubspaces} function.

\begin{warningbox}
Do not confuse the \verb?space_p? pointer and \verb?u_p->space_p? pointer that specifies the whole space. They may be the same!
\end{warningbox}

The numerical representation of vector is defined in the \verb?entries_p? \class{VectorEntry} pointer as a real/complex vector or a real/complex vector of vectors, regarding the value type (real or complex) and the structure of the unknown (scalar or vector). If required, vector of vectors representation may be expanded to a vector of scalars stored in the  \verb?scalar_entries_p? \class{VectorEntry} pointer. In that case the \verb?cDoFs_? vector of \class{DofComponent} gives the numbering (in \class{DofComponent}) of entries. Note that if \class{SuTermVector} is of scalar type, \verb?scalar_entries_p = entries_p?.\\

The \class{SuTermVector} class provides one constructor from linear form, some constructors from constant value, functions or symbolic functions, one constructor from linear combination and some copy constructors: 
\vspace{.1cm}
\begin{lstlisting}
//constructor from linear form
SuTermVector(SuLinearForm* sulf = 0, const String& na = "", bool noass = false); 
//constructors from constant value or function (no linear form)
SuTermVector(const String&, const Unknown*, Space*, ValueType vt = _real, Number n = 0, Dimen nv = 0, bool noass = false); //vector of zeros
template <typename T>
SuTermVector(const Unknown&, const GeomDomain&, const Vector<T>&, 
             const String&, bool);
template <typename T>
SuTermVector(const Unknown&, const GeomDomain&, const T&, 
             const String&, bool);
SuTermVector(const Unknown&, const GeomDomain&, funSR_t&, 
             const String& na="", bool noass=false);         
SuTermVector(const Unknown&, const GeomDomain&, funSC_t&, 
             const String& na="", bool noass =false);         
SuTermVector(const Unknown&, const GeomDomain&, funVR_t&, 
             const String& na="", bool noass =false);         
SuTermVector(const Unknown&, const GeomDomain&, funVC_t&, 
             const String& na="", bool noass =false);
//constructor from explicit function of SuTermVector's
SuTermVector(const SuTermVector&, funSR1_t& f, const string_t& na="");                       
SuTermVector(const SuTermVector&, const SuTermVector&, funSR2_t& f, const string_t& na=""); 
SuTermVector(const SuTermVector&, funSC1_t& f, const string_t& na="");  
SuTermVector(const SuTermVector&, const SuTermVector&, funSC2_t& f, const string_t& na="");  
//constructor from symbolic function of SuTermVector's
SuTermVector(const SuTermVector&, const SymbolicFunction& , const string_t& na="");  
SuTermVector(const SuTermVector&, const SuTermVector&, const SymbolicFunction&,
             const string_t& na="");                                                
SuTermVector(const SuTermVector&, const SuTermVector&, const SuTermVector&,
             const SymbolicFunction&, const string_t& na="");                     
//constructor of a vector SuTermVector from scalar SutermVector's
SuTermVector(const Unknown&, const SuTermVector&, const SuTermVector&, const string_t& ="");
SuTermVector(const Unknown&, const std::list<const SuTermVector*>&, const string_t& ="");
//constructor from linear combination (no linear form) 
SuTermVector(const LcTerm&);
//copy constructors and assign operator                  
SuTermVector(const SuTermVector&);           
template<typename T>
SuTermVector(const SuTermVector&, const T&);  
SuTermVector& operator=(const SuTermVector&);
//other useful stuff 
virtual ~SuTermVector();
void copy(const SuTermVector&);    //full copy                     
void initFromFunction(const Unknown&, const GeomDomain&, const Function&, 
                      const String& na="", bool noass=false);    
void clear(); 
\end{lstlisting}
\vspace{.2cm}
A lot of useful accessors and shortcuts to some properties are proposed: 
\vspace{.1cm}
\begin{lstlisting}
string_t name();
void name(const string_t&);
SuLinearForm* sulfp() const;
SuLinearForm*& sulfp();
VectorEntry*& entries();
const VectorEntry* entries() const;
TermType termType() const;
ValueType valueType() const;         
StrucType strucType() const;         
const Unknown* up() const;
const Unknown*& up();
number_t nbOfComponents() const;
const Space* spacep() const;
Space*& spacep();
set<const Space*> unknownSpaces() const;
number_t nbDofs() const; 
number_t size() const; 
VectorEntry*& scalar_entries();
const VectorEntry* scalar_entries() const;
VectorEntry* actual_entries() const;
const vector<DofComponent>& cDoFs() const;
vector<DofComponent>& cDoFs();
const Dof& DoF(number_t n) const;              
const GeomDomain* domain() const;               
\end{lstlisting}
\vspace{.2cm}
\class{SuTermVector} may be modified by the following operators and functions:
\vspace{.1cm}
\begin{lstlisting}
SuTermVector& operator+=(const SuTermVector&);           
SuTermVector& operator-=(const SuTermVector&);          
template<typename T>
 SuTermVector& operator*=(const T&);                      
template<typename T>
 SuTermVector& operator/=(const T&);    
SuTermVector& merge(const SuTermVector&);                  
SuTermVector& toAbs();                                    
SuTermVector& toReal();                                   
SuTermVector& toImag();                                   
SuTermVector operator()(const ComponentOfUnknown&) const; 
SuTermVector& toComplex();                                
complex_t maxValAbs() const;                   
void setValue(number_t, const Value&);          
void setValue(const Value&, const GeomDomain&);      
void setValue(const Function&, const GeomDomain&);     
void setValue(const SuTermVector&, const GeomDomain&); 
void setValue(const SuTermVector&);               
\end{lstlisting}
\vspace{.2cm}
The operator \verb?()? extracts unknown component term vector as a \class{SuTermVector} when \verb?Unknown? is a vector unknown. For instance if \verb?u?  is a vector unknown, \verb?T(u[2])? returns a \class{SuTermVector} containing the elements of \verb?T? corresponding to second component.\\
\vspace{.2cm}
Some values can be extracted or computed from:
\vspace{.1cm}
\begin{lstlisting}
Value getValue(number_t) const; 
SuTermVector* onDomain(const GeomDomain& dom) const; 
template<typename T>
 T& operator()(const vector<real_t>&, T&) const;       
SuTermVector* interpolate(const Unknown&, const GeomDomain&);
Value evaluate(const Point&) const;            
template<typename T>
 Vector<T>& asVector(Vector<T>&) const;  
\end{lstlisting}
\vspace{.2cm}
\class{SuTermVector} provides real computation algorithms, in particular FE computation ones: 
\vspace{.1cm}
\begin{lstlisting}
void buildSubspaces();             
void compute();                 //!< compute from linear form
void compute(const LcTerm&);   
template<typename T, typename K>
void computeFE(const SuLinearForm&, Vector<T>&, K&);     
template<typename T, typename K>
void computeIR(const SuLinearForm&, Vector<T>&, K& vt, const vector<Point>&,
               const Vector<T>&, const vector<Vector<real_t> >* nxs=0); 
\end{lstlisting}
\vspace{.2cm}    
Some particular member functions allow to change the internal representation: 
\vspace{.1cm}
\begin{lstlisting}
void toScalar(bool keepVector=false); 
void toVector(bool keepEntries=false);
void extendTo(const SuTermVector&);   
void extendTo(const Space&);          
void extendScalarTo(const vector<DofComponent>&, bool useDual = false);
SuTermVector* mapTo(const GeomDomain&, const Unknown&, bool =true) const;  
void changeUnknown(const Unknown&, const Vector<number_t>&);                
void adjustScalarEntries(const vector<DofComponent>&);     
\end{lstlisting}
\vspace{.2cm}
Some external function are provided to compute norms, \ldots
\vspace{.1cm}
\begin{lstlisting}
Complex innerProduct(const SuTermVector&, const SuTermVector&);
Complex hermitianProduct(const SuTermVector&, const SuTermVector&);
Real norm1(const SuTermVector&);       
Real norm2(const SuTermVector&);       
Real norminfty(const SuTermVector&);  
\end{lstlisting}
\vspace{.2cm}
Finally, there are some print and export stuff:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] list, map}]
void print(ostream&) const;  
void print(ostream&, bool r, bool h) const; // raw and header option
void saveToFile(const String&, bool encode=false) const;

//extern save functions
void saveToFile(const string_t&, const Space*, const list<SuTermVector*>&, 
                IOFormat iof=_raw, bool withDomName=true);
void saveToMsh(ostream&, const Space*, const list<SuTermVector*>&, 
               vector<Point>, splitvec_t, const GeomDomain*);
void saveToMtlb(ostream&, const Space*, const list<SuTermVector*>&, 
                vector<Point>, splitvec_t, const GeomDomain*);
void saveToVtk(ostream&, const Space*, const list<SuTermVector*>&, 
               vector<Point>,splitvec_t, const GeomDomain*);
void saveToVtkVtu(ostream&, const Space*, const list<SuTermVector*>&, 
                  vector<Point>, splitvec_t, const GeomDomain*);
pair<vector<Point>, map<number_t, number_t> > ioPoints(const Space* sp);
splitvec_t ioElementsBySplitting(const Space*, map<number_t, number_t>);
\end{lstlisting}
\vspace{.2cm}

\displayInfos{
library=term, 
header=SuTermVector.hpp, 
implementation=SuTermVector.cpp,
test=test\_TermVector.cpp,
header dep={Term.hpp, LcTerm.hpp, termUtils.hpp, form.h, config.h, utils.h}
}
