%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Unknown management}

\subsection{The \classtitle{Unknown} class}

The \class{Unknown} class manages element of a space (\class{Space}). It is mainly used as
an abstract object in the description of a variational problem. It has the \class{ComponentOfUnknown}
class as child which represents a component of a vector unknown. Do not confuse vector unknown on a scalar space and scalar unknown on a vector space. Vector unknown on a scalar space is a way to define an unknown on a product of scalar spaces (same space!), for instance the displacement field. Whereas on a vector space (say a space spanned by vector functions), unknown are generally a scalar one; for instance Hrot or Hdiv conform spaces. Up to now, it is not possible to deal with general product spaces.
\vspace{.1cm}
\begin{lstlisting}
class Unknown
{protected:
   String name_;             // name of the unknown
   Space * space_p;          // pointer to the space where the unknown lives
   mutable bool conjugate_;  // temporary flag for conjugate operation
   Unknown * dualUnknown_;   // pointer to dual Unknown (test function)
   Number rank_;             // rank to order unknowns
  public:
   bool isUnknown;           // true if an unknown, false if a test function
   static std::vector<Unknown*> theUnknowns;  //list of all Unknowns
\end{lstlisting}
\vspace{.1cm}
The temporary \verb?conjugate_? flag is used to mark in a \class{OperatorOnUnknown} construction
that an unknown is conjugate. The static attribute \verb?theUnknowns? traces all defined unknowns. 
Besides, each unknown has a rank, by default its creation number (first unknown created has rank 1 and so on). 
This rank is used to order blocks in vector or matrix  in case of multiple unknowns terms.\\
There is no class to deal with test function. There exists only an alias \verb?TestFunction? of \class{Unknown}: 
\vspace{.1cm}
\begin{lstlisting}
typedef Unknown TestFunction;
\end{lstlisting}
\vspace{.1cm}
So, to distinguish, unknown from test function, the flag \verb?isUnknown? is used. It is set to true when using the constructor from a Space and set to false when using the constructor from unknown, implicitly the test function constructor:
\vspace{.1cm}
\begin{lstlisting}
Unknown();                
Unknown(const string_t&, Space&, 
        dimen_t d=1, number_t r=0);// unknown constructor from space
Unknown(Unknown&,const string_t& na="",
        number_t r=0);            // test function constructor from unknown 
~Unknown();
\end{lstlisting}
\vspace{.2cm}
and the following accessors:
\vspace{.1cm}
\begin{lstlisting}
String name() const;              // return name
Space * space() const;            // return pointer to space
UnknownType type() const;         // return type of unknown
StrucType strucType() const;      // return the structure type of an unknown
number_t index() const;           // return index of unknown
bool conjugate() const;           // return the conjugate state flag
bool& conjugate();                // return the conjugate state flag
void conjugate(bool v) const ;    // set the conjugate state flag
friend Unknown& conj(Unknown &);  // conjugate an unknown 
Number rank() const;              // return the unknown rank
\end{lstlisting}
\vspace{.1cm}
The member function \verb?index()? returns the rank of the unknown in the \verb?theUnknowns?
list. It may be useful to order the unknowns. The member function \verb?type()? returns the
type of unknown as an element of the UnknownType enumeration (defined in \textit{config.hpp}
header file):
\vspace{.1cm}
\begin{lstlisting}
enum UnknownType {_feUnknown,_spUnknown,_mixedUnknown};
\end{lstlisting}
\vspace{.2cm}
There are particular virtual member functions linked to the \class{ComponentOfUnknown} child
class:
\vspace{.1cm}
\begin{lstlisting}
virtual dimen_t nbOfComponents() const;  // dimension of unknown
virtual bool isComponent() const;        // true if a ComponentOfUnknown
virtual const Unknown* parent() const;   // return parent (or itself)
virtual Dimen componentIndex() const;    // return component index if a ComponentOfUnknown, 0 if not
virtual const ComponentOfUnknown* asComponent() const; // return Unknown as a ComponentOfUnknown if it is
Unknown& operator[](dimen_t);            // create the ComponentOfUnknown u_i
\end{lstlisting}
\vspace{.2cm}
\class{Unknown} class provides printing facility also:
\vspace{.1cm}
\begin{lstlisting}
friend std::ostream& operator<<(std::ostream&, const Unknown&);

void setRanks(std::vector<Unknown*>&, const std::vector<Number>&); 
void setRanks(Unknown&, Number r);
void setRanks(Unknown&, Number, Unknown&, Number);
void setRanks(Unknown&, Number, Unknown&, Number, \ldots);
\end{lstlisting}
\vspace{.1cm}
The \verb?setRanks? functions are useful to define the internal order of unknowns in multiple unknowns objects such as \class{TermMatrix} or \class{TermVector}. The unknown ranks have to be unique, but it is not mandatory that they follow. By default, first created unknown has rank 1, the second created has rank 2, and so on. 
\vspace{.2cm}

\subsection{The \classtitle{ComponentOfUnknown} class}

To access to a component of a vector unknown as an unknown, the \class{ComponentOfUnknown}
child class is provided:
\vspace{.1cm}
\begin{lstlisting}
class ComponentOfUnknown: public Unknown
{protected :
   const Unknown * u_p;     //Unknown which this unknown is a component
   Dimen i_;                //index of the component
 public :
   ComponentOfUnknown(const Unknown &,dimen_t);
   virtual Dimen nbOfComponents() const; 
   virtual Dimen dimFun() const;             
   virtual bool isComponent() const;
   virtual const Unknown* parent() const;
   virtual Dimen componentIndex() const;
   virtual const ComponentOfUnknown* asComponent() const;
};
\end{lstlisting}
\vspace{.1cm}
Note that the operator \verb?Unknown::[](Dimen)? creates a \verb?ComponentOnUnknown? object
returned as a \verb?Unknown? object.

\subsection{The \classtitle{Unknowns} class}
\class{Unknowns} is an alias of \class{PCollection<Unknown>} class that manages a collection of \class{Unknown} objects, in fact a \class{std::vector<Unknown*>}. Be cautious when using it because the Unknown pointers are shared; in particular when using temporary instance of Unknown! It is the reason why the \class{PCollectionItem} class is overloaded to protect pointer in assign syntax \cmd{sp(i)=Unknown(\ldots)}:   
\vspace{.1cm}
\begin{lstlisting}
template<> class PCollectionItem<Unknown>
{public:
typename std::vector<Unknown*>::iterator itp;
PCollectionItem(typename std::vector<Unknown*>::iterator it) : itp(it){}
Unknown& operator = (const Unknown& sp); //protected assignment
operator Unknown&(){return **itp;}     //autocast PCollectionItem->Unknown&
};
\end{lstlisting}
\vspace{.2cm}
Main usages of this class are the following:
\vspace{.1cm}
\begin{lstlisting}
Unknown u1(V1,"u1"), u2(V2,"u2"), u3(V3,"u3");
Unknowns us1(u1,u2,u3); 
Unknowns us2; us2<<u1<<u2<<u3;
Unknowns us5(5);
for(Number i=1;i<=5;i++) us5(i)=Unknown(Vs5(i),"u_"+tostring(i));
\end{lstlisting}

\begin{cpp11box}
If C++11 is available (the library has to be compiled in C++11), the following syntax is also working:

\bigskip

\begin{lstlisting}
Unknowns us={u1,u2,u3};
\end{lstlisting}
\end{cpp11box}

In a same way, \class{TestFunctions} is an alias of \class{PCollection<TestFunction*>} class that manages a collection of \class{TestFunction} objects. It works as the \class{Unknowns} class. The following function allows to create a collection of testfunctions related to a collection of unknowns:

\begin{lstlisting}
TestFunctions dualOf(const Unknowns& us);
\end{lstlisting}

\displayInfos{library=space, header=Unknown.hpp, implementation=Unknown.cpp, test=test\_Space.cpp,
header dep={config.h, utils.h}}
