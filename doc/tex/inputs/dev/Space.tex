%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{Space management}

\subsection{The \classtitle{Space} and \classtitle{SpaceInfo} classes}

The \class{Space} class manages different kind of spaces using inherited classes: finite element
space (\class{FeSpace}), spectral space (\class{SpSpace}), subspace (\class{SubSpace})
and product space (\class{ProdSpace}). To avoid the user to deal explicitly with this space
hierarchy, we use the following programming paradigm: \class{Space} class has a pointer
to a \class{Space} as attribute, which contains either its own address or the address of a child. In
a sense, the \class{Space} class is not an abstract class but has the behavior of an abstract
one. When an end user declares a Space object, he induces the construction of a child Space.
With this trick, an end user instantiates only \class{Space} object, never the child spaces.
As a consequence, it requires a little more memory because member attributes are duplicated.
In order to limit this effect, the common characteristics of a space are collected in the {\class
SpaceInfo} class:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] dimCom}]
class SpaceInfo
 {public :
  String name_;                 // name of space
  Domain * domain_p;            // geometric domain supporting the space
  SobolevType spaceConforming_; // conforming space (L2,H1,Hdiv,Hcurl,H2,Hinf)
  dimen_t dimFun_;              // number of components of the basis functions
  Dimen dimCom;                 // number of components of the unknowns
  SpaceType spaceType_;         // space type (FESpace,SPSpace,SubSpace,ProductSpace)
  SpaceInfo(){};
  SpaceInfo(const String &,SobolevType,Domain *,dimen_t , SpaceType)
 };
\end{lstlisting}
\vspace{.2cm}
The general characteristics of a space are the geometric domain supporting the basis functions,
the space conformity which "tells" the regularity of the basis functions and the dimension
of the basis functions. The space conformity property is a way to distinguish discontinuous
finite element approximation from continuous one. The space conformity is given by the SobolevType
enumeration declared in the \textit{config.hpp} header file:
\vspace{.1cm}
\begin{lstlisting}
enum SobolevType {L2=0, L2=_L2,H1, H1=_H1,_Hdiv, Hdiv=_Hdiv,_Hcurl, Hcurl=_Hcurl,
                  _Hrot=_Hcurl, Hrot=_Hrot,_H2, H2=_H2,_Hinf, Hinf=_Hinf}
\end{lstlisting}
\vspace{.1cm}
To be user-friendly, several names are available for the same space.\\

The parent class \class{Space} has only two pointers and a static vector managing the list
of all defined spaces:
\vspace{.1cm}
\begin{lstlisting}
class Space
{protected :
  Space * space_p;                       //pointer to the "true" space (child)
  SpaceInfo * spaceInfo_p;               //pointer to the space information structure
public :
  static std::vector<Space*> theSpaces;  //unique list of defined spaces
\end{lstlisting}
\vspace{.2cm}
Regarding the type of spaces, the \class{Space} provides several constructors to construct child spaces:
\vspace{.1cm}
\begin{lstlisting}
//constructors of trivial space of dim n, say Rn
Space(number_t, const string_t& ="Rn");  
Space(number_t, const GeomDomain&, const string_t& ="Rn");

// internal constructors addressing spectral space 
Space(const SpectralBasis&, const string_t&);  //from spectral basis

// internal constructors addressing FeSpace type from Interpolation or FE types
Space(const GeomDomain&, const Interpolation&, const string_t&, bool opt = true);

// constructors addressing SubSpace type
Space(const GeomDomain&, Space&, const string_t& na = ""); 
Space(const GeomDomain&, const vector<number_t>&, Space&, const string_t& na = ""); 
Space(const std::vector<number_t>&, Space&, const string_t& na = "");     

// tools related to construtors     
void buildSpFun(const GeomDomain&, const Function&, number_t, 
                dimen_t, const string_t&);         
SubSpace* createSubSpace(const GeomDomain&, Space&, const string_t& na);               
void createSubSpaces(const vector<const GeomDomain*>&,vector<Space*>&);   
Space* buildSubspaces(const vector<const GeomDomain*>&, vector<Space*>&);   
\end{lstlisting}
\vspace{.1cm}
These constructors work as follows (\textit{(child)Space} denotes any inherited space class):
\vspace{.1cm}
\begin{lstlisting}
 (child)Space* csp_p=new (child)Space(\ldots); //create the (child)Space
 space_p=static_cast<Space *>(csp_p);       //cast (child)Space* to Space*
 spaceInfo_p=csp_p->spaceInfo_p;            //copy the SpaceInfo pointer
 theSpaces.push_back(this);                 //store space in list of spaces
\end{lstlisting}
\vspace{.1cm}
Note that it is the child constructors that set all the attributes (general ones and specific
ones).

\subsection{The key-value system}

Most of the \class{Space} constructors use a key-value system, so there are \class{Space} constructors taking from 1 to 13 inputs of type \class{Parameter}:

\begin{center}
\begin{tabular}{|p{4.5cm}|p{6cm}|p{5.5cm}|}
\hline
key(s) & authorized types / possible values & involved kind of space \\
\hline
\hline
\param{\_domain} & \( \)\class{Domain} & Finite element space \\
\hline
\param{\_FE\_type} & Lagrange, Hermite, CrouzeixRaviart, Nedelec, NedelecFace, NedelecEdge or Morley & Finite element space \\
\hline
\param{\_FE\_subtype} & standard, GaussLobattoPoints, firstFamily or secondFamily & Finite element space \\
\hline
\param{\_Sobolev\_type} & L2, H1, Hdiv, Hcurl, Hrot, H2, Hinf or Linf & Finite element space \\
\hline
\param{\_order} & single unsigned integer & Finite element space \\
\hline
\param{\_interpolation} & P0 to P10, P1BubbleP3, Q0 to Q10, RT\_1 to RT\_5, NF1\_1 to NF1\_5, BDM\_1 to BDM\_5, NF2\_1 to NF2\_5, N1\_1 to N1\_5, N2\_1 to N2\_5 & Finite element space \\
\hline
\param{\_name} & \( \)\class{String} &  every kind of space \\
\hline
\param{\_optimizeNumbering}, \hspace{4ex} \param{\_notOptimizeNumbering} & \( \)no value & Finite element space \\
\hline
\param{\_withLocateData}, \hspace{4ex} \param{\_withoutLocateData} & \( \)no value & Finite element space \\
\hline
\param{\_basis} & \( \)\class{Function}, \class{TermVector} & spectral space \\
\hline
\param{\_dim} & single unsigned integer & spectral space \\
\hline
\param{\_basis\_dim} & single unsigned integer & spectral space \\
\hline
\end{tabular}
\end{center}

These constructors transfer the building work to the following routines:

\begin{lstlisting}
void buildSpace(const std::vector<Parameter>& ps);
\end{lstlisting}

This routine works in the same way as the internal constructors described in the previous subsection.

\subsection{\classtitle{Space} member functions}

The \class{Space} class provides some accessors, some being virtual: 
\vspace{.1cm}
\begin{lstlisting}
string_t name() const;                //space name
SobolevType conformingSpace() const;  //space conformity
const GeomDomain* domain();           //related domain
dimen_t dimDomain() const;            //dimension of related domain
dimen_t dimPoint() const ;            //dimension of points
dimen_t dimFun() const;               //dimension of basis functions
SpaceType typeOfSpace() const;        //type of space
SpaceType typeOfSubSpace() const;     //type of sub-space
virtual bool isSpectral() const;      //true if spectral space
virtual bool isFE() const;            //true if FE space or FeSubspace
const Space* space() const;           //pointer to true space object
virtual number_t dimSpace() const;    //space dimension 
virtual number_t nbDofs()const;       //number of DoFs
virtual const FeSpace* feSpace() const;      //pointer to child fespace  
virtual FeSpace* feSpace();                  //pointer to child fespace 
virtual const FeSubSpace* feSubSpace() const;//pointer to child fesubspace
virtual const SpSpace* spSpace() const;      //pointer to child fesubspace
virtual const SubSpace* subSpace() const;    //pointer to child subspace 
virtual SubSpace* subSpace();                //pointer to child subspace 
virtual const Space* rootSpace() const;      //pointer to root space 
virtual Space* rootSpace();                  //pointer to root space 
virtual ValueType valueType() const;         //value type of basis function
virtual StrucType strucType() const;         //structure type of basis function
virtual bool include(const Space*) const;    //inclusion test
virtual bool extensionRequired() const;      //rue if space has no trace space
\end{lstlisting}
\vspace{.1cm}
To understand how works the non abstract/abstract class paradigm, here is the implementation
of the access function \textit{feSpace}:
\vspace{.1cm}
\begin{lstlisting}
FeSpace* Space::feSpace() const
{if(space_p!=this) return space_p->feSpace();    //call the true feSpace
 error("is_not_fespace",name());
 return 0;
}
FeSpace* FeSpace::feSpace() const {return *this;}
\end{lstlisting}
\vspace{.1cm}
The test \verb?space_p!=this? means that the current space is a user space (it has a child),
the \cmd{fespace} function is called by its child. By polymorphic behavior, if \verb?space_p?
is a \class{FeSpace} the \cmd{FeSpace::fespace} is called else \cmd{Space::fespace} is
again called, but with \verb?space_p=this? and an error is thrown because the current space
is not a \class{FeSpace}. \\

As you will see in section \ref{s.dof}, the DoF number is relative to the parent space. To get the global number of a DoF or the DoF itself and to manage DoFs, Space provides the following functions :\\
\vspace{0.1cm}
\begin{lstlisting}
virtual number_t DoFId(number_t) const;        //n-th DoF id of space
virtual std::vector<number_t> DoFIds() const;  //DoF ids on space
virtual const Dof& DoF(number_t) const;        //n-th DoF (n=1,\ldots)
virtual std::pair<number_t, number_t> renumberDofs();//optimize DoFs numbering
virtual void shiftDofs(number_t n);     //shift DoFs numbering from n (SpSpace)           
\end{lstlisting}
\vspace{0.2cm}
When space is a FE space or a FE subspace, tools to access to finite element information are provided:
\vspace{0.1cm}
\begin{lstlisting}
virtual const Interpolation* interpolation() const;    
virtual number_t nbOfElements() const;                  
virtual const Element* element_p(number_t k) const:    
virtual const Element* element_p(GeomElement* gelt) const;
virtual number_t numElement(GeomElement* gelt) const;      
virtual vector<number_t> elementDofs(number_t k) const;    
virtual vector<number_t> elementParentDofs(number_t k) const; 
virtual const set<RefElement*>& refElements() const;         
virtual void buildgelt2elt() const;  
virtual const vector<FeDof>& feDofs() const;                
virtual const Point& feDofCoords(number_t k) const;              
virtual void buildDoFid2rank() const;                        // built on fly                           
virtual const map<number_t, number_t>&  DoFid2rank() const;  // built on fly 
virtual Dof& locateDof(const Point&) const;                                 
\end{lstlisting}
\vspace{0.2cm}
Finally, the \class{Space} provides some printing facilities: 
\vspace{.1cm}
\begin{lstlisting}
void printSpaceInfo(std::ostream &) const; 
virtual void print(std::ostream &) const;
friend std::ostream& operator<<(std::ostream&,const Space &);
\end{lstlisting}
\vspace{.2cm}
Some functions managing subspaces (comparison, merging) are also available:
\begin{lstlisting}
bool compareSpaceUsingSize(const Space*, const Space*); 
vector<number_t> renumber(const Space*, const Space*);  
Space& subSpace(Space&, const GeomDomain&);       
inline Space& operator |(Space&, const GeomDomain&);
Space* mergeSubspaces(Space*&, Space*&, bool newSubspaces=false);    
Space* mergeSubspaces(std::vector<Space*>&, bool newSubspaces=false);
Space* unionOf(std::vector<Space*>&);                               
vector<Point> DoFCoords(Space&, const GeomDomain&);  
\end{lstlisting}
\vspace{.2cm}
Linked to the \class{Space} class, the \class{DoF} class stores characteristics of degree
of freedom. Degree of freedom is like a key index of components of element of space. Generally,
it  handles a numeric index and carries some additional information. From \class{DoF} class
inherit the classes \class{FeDoF} and \class{SpDoF} (see section DoF).\\
\displayInfos{library=space, header=Space.hpp, implementation=Space.cpp, test=test\_Space.cpp,
header dep={config.h, geometry.h, Df.hpp}}

Now, let's have a look at the child space classes.

\subsection{The \classtitle{FeSpace} class}

A finite elements space is defined from a finite set of functions given by an interpolation method on a mesh. 
The inherited \class{FeSpace} class is defined as follows:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] elements}]
class FeSpace : public Space
{protected :
  const Interpolation* interpolation_p;// pointer to finite element interpolation
  bool optimizedNumbering_;  // true if DoF numbering is bandwidth optimized
 public:
  vector<Element> elements;  //list of finite elements
  vector<FeDof> DoFs;        //list of global degrees of freedom
  set<RefElement *> refElts; //set of RefElement pointers involved
  mutable map<GeomElement*, number_t> gelt2elt; //map GeomElement->element number
  mutable map<number_t, number_t> DoFid2rank_;  //map DoF iD->rank in DoFs vector
  static Number lastEltIndex;    //to manage a unique index for elements
};
\end{lstlisting}
\vspace{.1cm}{
Besides the two fundamental lists of elements and DoFs, two useful maps \var{gelt2elt} and \var{DoFid2rank} may be constructed on the fly. The first map is built when some interpolation operations are handled and the second one when some FE subspaces are involved. The set \var{XXX} collects the types of FE elements involved in the space, generally only one type.
This class provides one constructor, using two member functions:
\vspace{.1cm}
\begin{lstlisting}
FeSpace(const Domain&, const Interpolation&, Dimen, const String&, bool opt=true, bool withLocateData=true);

void buildElements();   //construct the list of elements
void buildFeDofs();     //construct the list of FeDofs
void buildgelt2elt() const;   //construct the map gelt2elt 
void buildDoFid2rank() const; //construct the map DoFid2rank
void buildBCRTSpace();  //construction of BuffaChristiansen-RT space
\end{lstlisting}
The \cmd{buildElements} member functions create the list of elements (\class{Element} objects) from the list of geometric elements belonging to the domain supporting the space. The \cmd{buildFeDofs} function is the most important one. It constructs from reference DoFs the list of global DoFs using a general algorithm based on the \class{DofKey} class that indexes DoF regarding its localization in the mesh:
\vspace{.1cm}
\begin{lstlisting}
class DofKey
{public:
 DofLocalization where;
 number_t v1, v2;           //used for vertex, edge, element DoFs
 std::vector<number_t> vs;  //used for face DoFs
 number_t locIndex;
 friend bool operator<(const DofKey& k1, const DofKey& k2);
};
\end{lstlisting}
\vspace{.1cm}
Using this indexing, the algorithm building the DoFs set works as follows:
\vspace{.1cm}

\[
\left| 
\begin{array}{l}
\bullet \ \text{case of no shared DoF (L2 conforming space), concatenate all local DoFs and exit}\\
\bullet \ \text{case of shared DoFs  }\\
 \ \ \ \ \bullet \ \text{construct a DofKey map by traveling elements }\\
 \ \ \ \ \bullet \ \text{construct the list of global DoFs from the DofKey map and update elements}
\end{array}
\right.
\]

To insure the right matching between shared DoFs on edge or face, this algorithm uses the \cmd{sideDofsMap} and \cmd{sideOfSideDofsMap} member functions of finite element classes. These functions relate DoF numbering to one edge/face to another edge/face.\\
    
The \class{FeSpace} class provides also some specific accessors and properties:
\vspace{.1cm}
\begin{lstlisting}
virtual Number nbDofs() const; //number of DoFs (a DoF may be a vector DoF)
Dimen dimElements() const;     //return the dimension of FE elements
number_t nbOfElements() const; //number of elements 
const Interpolation* interpolation() const; //return pointer to interpolation
virtual Space* rootSpace();         //root space pointer
virtual number_t dimSpace()const;   //the space dimension
virtual number_t nbDofs()const;     //number of DoFs (may be a vector DoF)
virtual number_t DoFId(number_t n) const;     //the DoF id of n-th space DoF 
virtual vector<number_t> DoFIds() const;      //the DoF ids on space
virtual const Dof& DoF(number_t n) const;     //n-th FeDof as Dof (n=1,\ldots)
virtual const FeDof& feDoF(number_t n) const  //n-th FeDof (n=1,\ldots)
virtual const std::vector<FeDof>& feDofs() const; //DoFs vector
dimen_t dimElements() const;                  //the dimension of FE elements
const Interpolation* interpolation() const;   //pointer to interpolation
const set<RefElement*>& refElements() const;  //set of pointer to RefElement 
number_t maxDegree() const;                   //max degree of shape functions involved
virtual bool include(const Space*) const;     //true if space is included in current space
virtual ValueType valueType() const;          //value type of basis function 
virtual StrucType strucType() const;          //structure type of basis function 
virtual bool isSpectral() const;              //true if spectral space
virtual bool isFE() const;                    //true if FE space or FeSubspace
virtual bool extensionRequired() const;       //true if space has no trace space
\end{lstlisting}
\vspace{.1cm}
The \class{FeSpace} class has functions related to renumbering:
\vspace{.1cm}
\begin{lstlisting}
Graph graphOfDofs(); //create DoF connection graph
const Element* element_p(Number k) const; //k-th (k>=0) element (pointer)
vector<Number> elementDofs(Number k) const; //DoFs ranks (local numbering) of k-th (>=0) element
vector<Number> elementParentDofs(Number k) const; //DoFs ranks (parent numbering, same as local) of k-th (>=0) element
const map<number_t, number_t>&  DoFid2rank() const; //map DoF to rank, built on fly
void shiftDofs(number_t);  //shift DoFs numbering from n (see SpSpace)
\end{lstlisting}
\vspace{.1cm}
and functions related to interpolation operations:
\vspace{.1cm}
\begin{lstlisting}
vector<number_t> DoFsOn(const GeomDomain&) const; //set of DoF numbers of DoFs 
Dof& locateDof(const Point &) const;              //DoF nearest a given point
template <typename T, typename K>
T& interpolate(const Vector<K>&, const Point&, T&, DiffOpType =_id) const; //interpolation value at P
\end{lstlisting}
\vspace{.1cm}
\displayInfos{library=space, header=FeSpace.hpp, implementation=FeSpace.cpp, test=test\_Space.cpp,
header dep={config.h, Elements.hpp, Space.hpp, DoF.hpp, finiteElement.h}}

\subsection{The \classtitle{SpSpace} class}

A spectral space is defined by a finite set of functions given by analytic expression or by
interpolated form. These global basis functions defined over a geometric domain are collected
in the \class{SpectralBasis} class declared in the \textit{SpectralBasis.hpp} header file
(see the section SpectralBasis). The inherited \class{SpSpace} class is defined as follows:
\vspace{.1cm}
\begin{lstlisting}
class SpSpace : public Space
{
  protected :
    SpectralBasis* spectralBasis_; //! object function encapsulating the spectral functions
    //either defined in an analytical form or by interpolated functions (TermVector)
  public :
    std::vector<SpDof> DoFs;       //! list of global degrees of freedom
};
\end{lstlisting}
\vspace{.1cm}
This class provides one constructor, a destructor, several accessors and a printing function:
\vspace{.1cm}
\begin{lstlisting}
SpSpace(const String&, Domain&, number_t, dimen_t, SpectralBasis*);
~SpSpace();
SpectralBasis* spectralBasis() const;
virtual Number nbDofs() const;     //!< number of DoFs (a DoF may be a vector DoF)
\end{lstlisting}
\vspace{.1cm}

\displayInfos{library=space, header=SpSpace.hpp, implementation=SpSpace.cpp, test=test\_Space.cpp,
header dep={config.h, Elements.hpp, Space.hpp, DoF.hpp}}


\subsection{The \classtitle{SubSpace} and \classtitle{FeSubSpace} classes}

A subspace is part of a space. Its DoFs are a subset of the parent DoFs. The inherited \class{SubSpace} class is defined as follows:
\vspace{.1cm}
\begin{lstlisting}
class SubSpace : public Space
{
  protected :
    Space* parent_p;                     //!< the parent space
    std::vector<Number> DoFNumbers_; //!< global numbers of D.o.Fs in parent D.o.Fs numbering
};
\end{lstlisting}
\vspace{.1cm}
This class provides two constructor, a destructor, and several specific accessors about the {\ttfamily DoFNumbers\_} attribute or about the type of subspace :
\vspace{.1cm}
\begin{lstlisting}
SubSpace()                                                //!< default constructor
SubSpace(const Domain&, Space&, const String& na = ""); //!< constructor specifying geom domain, parent space and name
~SubSpace();                                              //! destructor

std::vector<Number>& DoFNumbers()                         //!< access to DoFNumbers list
const std::vector<Number>& DoFNumbers() const             //!< access to DoFNumbers list (const)
std::vector<Number> DoFRootNumbers() const;               //!< access to DoFNumbers list in root space numbering
Number DoFRootNumber(Number k) const;                     //!< access to DoF number in root space numbering
virtual Number nbDofs() const                             //! number of DoFs (a DoF may be a vector DoF)
virtual bool isFeSubspace();
virtual const FeSubSpace* fesubspace() const;
\end{lstlisting}
\vspace{.1cm}
It also provides functions for numbering management:
\vspace{.1cm}
\begin{lstlisting}
void createNumbering();   //!< create numbering of subspace DoFs
void DoFsOfFeSubspace();  //!< build DoFs numbering of Fe SubSpace
void DoFsOfSpSubspace();  //!< build DoFs numbering of Sp SubSpace
void DoFsOfSubSubspace(); //!< build DoFs numbering of Subspace of SubSpace
\end{lstlisting}
\vspace{.1cm}
\displayInfos{library=space, header=SubSpace.hpp, implementation=SubSpace.cpp, test=test\_Space.cpp,
header dep={config.h, Elements.hpp, Space.hpp, Dof.hpp}}


The \class{FeSubSpace} class concerns subspaces of finite element spaces. It is defined as follows:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] elements}]
class FeSubSpace : public SubSpace
{
  public :
    std::vector<Element*> elements;                //!< list of finite elements (or subelements)
    std::vector<std::vector<Number> > DoFRanks;    //!< numbering of D.o.Fs of domain elements in domain D.o.Fs
};
\end{lstlisting}
\vspace{.1cm}
This class provides one constructor, a destructor, and several specific functions, also provided by the \class{FeSpace} class:
\vspace{.1cm}
\begin{lstlisting}
FeSubSpace(const Domain&, Space&, const String& na = ""); //!< constructor specifying geom domain, parent space and name
~FeSubSpace();                                         //!< destructor
Number nbOfElements() const;                           //!< number of elements
const Element* element_p(Number k) const;              //!< access to k-th (k>=0) element (pointer)
std::vector<Number> elementDofs(Number k) const;       //!< access to DoFs ranks (local numbering) of k-th (>=0) element
std::vector<Number> elementParentDofs(Number k) const; //!< access to DoFs ranks (parent numbering) of k-th (>=0) element
\end{lstlisting}
\vspace{.1cm}
\displayInfos{library=space, header=FeSubSpace.hpp, implementation=FeSubSpace.cpp, test=test\_Space.cpp,
header dep={config.h, Elements.hpp, SubSpace.hpp, DoF.hpp}}

\subsection{The \classtitle{ProdSpace} class}

The \class{ProdSpace} class manages product of spaces using a vector of space pointers:
\vspace{.1cm}
\begin{lstlisting}
class ProdSpace : public Space
{
  protected :
    std::vector<Space*> spaces_;    
  public :
    virtual Number dimSpace()const;
    virtual Number nbDoFs() const;
    virtual const Space* rootSpace() const           
    {return static_cast<const Space*>(this);}
    virtual Space* rootSpace()                       
    {return static_cast<Space*>(this);}
    virtual Number DoFId(Number n) const         
    {error("no_DoF_numbering", name());
     return 0;}
};
\end{lstlisting}
\vspace{.1cm}
It is currently not used.

\subsection{The \classtitle{Spaces} class}

\class{Spaces} is an alias of \class{PCollection<Space>} class that manages a collection of \class{Space} objects, in fact a \\ \class{std::vector<Space*>}. Be cautious when using it because the Space pointers are shared; in particular when using temporary instance of Space! It is the reason why the \class{PCollectionItem} class is overloaded to protect pointer in assign syntax \cmd{sp(i)=Space(\ldots)}:   
\vspace{.1cm}
\begin{lstlisting}
template<> class PCollectionItem<Space>
{public:
 typename std::vector<Space*>::iterator itp;
 PCollectionItem(typename std::vector<Space*>::iterator it) : itp(it){}
 Space& operator = (const Space& sp); //protected assignment
 operator Space&(){return **itp;}     //autocast PCollectionItem->Space&
};
\end{lstlisting}
\vspace{.2cm}
Main usages of this class are the following:
\vspace{.1cm}
\begin{lstlisting}
Space V1(omega,_P1,"V1"), V2(omega,_P2,"V2"), V3(omega,_P3,"V3"); 
Spaces Vs2(V1,V2);
Spaces Vs3(V1,V2,V3); 
Spaces Vs4; Vs4<<V1<<V2<<V3<<V1;
Spaces Vs5(5);
for(Number i=1;i<=5;i++)
   Vs5(i)=Space(omega,interpolation(Lagrange,_standard,i,H1),"V_"+tostring(i));
\end{lstlisting}

\begin{cpp11box} 
If C++11 is available (the library has to be compiled in C++11), the following syntax is also working:

\bigskip

\begin{lstlisting}
Spaces vs={V1,V2,V3};
\end{lstlisting}
\end{cpp11box}

Collection of spaces can also be set by the following functions  from one or several domains and one or several interpolations:
\vspace{.1cm}
\begin{lstlisting}
Spaces spaces(const Domains&,const Interpolations&,bool=true);    
Spaces spaces(const Domains&,const Interpolation&,bool=true);     
Spaces spaces(const GeomDomain&,const Interpolations&,bool=true); 
\end{lstlisting}
\vspace{.2cm}

\displayInfos{library=space, header=ProdSpace.hpp, implementation=ProdSpace.cpp, test=test\_space.cpp,
header dep={config.h, Space.hpp}}
