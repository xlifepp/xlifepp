%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{MatrixStorage} class}

The \class{MatrixStorage} is the abstract base class of all supported matrix storages. Vectors carrying storage pointers are defined in child classes and the vector storing the matrix entries is defined outside the storage (see \class{LargeMatrix} class for instance). Various matrix storages are defined by three characteristics :
\begin{itemize}
\item A StorageType:  \verb?_dense? for dense storage, \verb?_cs? for compressed sparse storage (CSR/CSC) and \verb?_skyline? for skyline storage; 
\item An AccessType: \verb?_row? for a row wise access, \verb?_col? for a column wise access, \verb?_dual? for a row wise access for lower triangular part of the matrix and a column wise access for the upper triangular part of the matrix, and \verb?_sym? if the storage is symmetric (only the lower triangular part is described with a row wise access;
\item A BuildStorageType: describing the way the storage is constructed :\verb?_undefBuild, _feBuild, _dgBuild,?\verb?_diagBuild, _ecBuild, _globalBuild, _otherBuild?;
\item A flag telling if the storage comes from a vector to scalar conversion.
\end{itemize}
\begin{lstlisting}
class MatrixStorage
{protected:
   StorageType storageType_;     // storage type 
   AccessType accessType_;       // access type 
   StorageBuildType buildType_;  // storage build type
   bool scalarFlag_;             // scalar conversion flag
   Number nbRows_;               // number of rows
   Number nbCols_;               // number of columns
   Number nbObjectsSharingThis_; // number of matrices sharing "this" storage
   \ldots
\end{lstlisting}
\vspace{.1cm} 
The number of rows and columns are counted in value type of the matrix.\\ \\
It manages also a string index to identify the storage (generally encoding the row/col space pointers)
\begin{lstlisting}
 public:
   String stringId;  // string identifier based on characteristic pointers
\end{lstlisting}
\vspace{.1cm} 
The class provides some constructors which only initialize members of the class.   
\vspace{.1cm}
\begin{lstlisting}
MatrixStorage();           
MatrixStorage(StorageType, AccessType at = _noAccess);  
MatrixStorage(StorageType, AccessType, Number, Number); 
virtual ~MatrixStorage();  
\end{lstlisting}
\vspace{.1cm} 
Protected members may be read by accessors:
\vspace{.1cm}
\begin{lstlisting}
String name() const;                       
StorageType storageType() const;      
AccessType accessType() const;
StorageBuildType buildType() const;
Number nbOfRows() const;
Number nbOfColumns() const;
Number numberOfObjects() const           
void objectPlus() ;                       
void objectMinus();  
\end{lstlisting}
\vspace{.1cm} 
All storage object pointers are stored in a static vector
\begin{lstlisting}
static vector<MatrixStorage*> theMatrixStorages; // list of all matrix storages  
static void clearGlobalVector();                 // delete all MatrixStorage objects
static void printAllStorages(ostream&);          // print the list of MatrixStorage in memory
static void printAllStorages(PrintStream& os);
\end{lstlisting}
and some static functions dealing with are available.\\

When building a new storage, it is possible to look for an existing storage by using the external function:
\begin{lstlisting}
MatrixStorage* findMatrixStorage(const String& id, StorageType st, AccessType at, 
               StorageBuildType sb, bool scal=false, Number nbr=0, Number nbc=0);
\end{lstlisting}
Travelling \verb?theMatrixStorages? vector this function checks if it exists a storage having the same characteristics (id, storage type, access type, build type) and may check the scalar flag and the number of rows or columns if nonzero values are given.\\

The class provides virtual member functions returning the sizes of storage (number of matrix entries stored) :
\vspace{.1cm}
\begin{lstlisting}
virtual Number size() const = 0;          
Number diagonalSize() const;             
virtual Number lowerPartSize() const=0;  
virtual Number upperPartSize() const=0;  
\end{lstlisting}
\vspace{.2cm} 
Most of the member functions depend on type of storage, so they are virtual member functions. Besides, as storages do not depend on value type of matrix,  the storage classes are not templated by the matrix value type. Because virtual template functions are not allowed in C++, all the value type specializations are explicit in base class and in child classes. Up to now, the supported specializations are \verb?Real?, \verb?Complex?, \verb?Matrix<Real>? and  \verb?Matrix<Complex>?. We describe here only the \verb?Real? specialization. \\ \\
There are two functions related to matrix entry position in storage, the first one gives the position of entry \((i,j)\) with \(i,j\ge 1\) and the second one compute the positions of the entries of the submatrix given by its row indices and column indices. These functions give \textbf{positions from index 1} if the entry is inside the storage else the returned position is 0. In the second function, it is possible to activate an error when entry index is outside the storage.
In both functions, the symmetry has to be specified if the matrix has a symmetry property, in that case the position is always in lower triangular part. Be care with the fact that a matrix with no symmetry may have a symmetric storage! 
\vspace{.1cm}
\begin{lstlisting}
virtual Number pos(Number i, Number j, SymType sym=_noSymmetry) const;
virtual void positions(const std::vector<Number>&, const std::vector<Number>&,
                       std::vector<Number>&, bool errorOn=true, SymType=_noSymmetry) const;   
\end{lstlisting}
\vspace{.2cm} 
To print, to save to file or load from file large matrices, the following functions are proposed :
\vspace{.1cm}
\begin{lstlisting}
virtual void printEntries(std::ostream&, const std::vector<Real>&,
                          Number vb = 0, SymType sym = _noSymmetry) const;
template<typename T>
  void printDenseMatrix(std::ostream&, const std::vector<T>&, SymType s=_noSymmetry) const;
  void printCooMatrix(std::ostream&, const std::vector<T>&, SymType s=_noSymmetry) const;
virtual void loadFromFileDense(std::istream&, std::vector<Real>&, SymType, bool);
virtual void loadFromFileCoo(std::istream&, std::vector<Real>&, SymType, bool);
virtual void visual(std::ostream&) const;
\end{lstlisting}
\vspace{.1cm} 
The \cmd{printEntries} function print on output stream the stored matrix entries in a well suited presentation, the \cmd{printDenseMatrix} function print the matrix entries in a dense format as it was dense and the \cmd{printCooMatrix} function print the matrix entries in a coordinates format (\(i,j,A_{ij}\)). An \(m\times n\) matrix saved as dense produced a file like:
\begin{verbatim}
    A11 A12 \ldots A1n                       re(A11) im(A11)  \ldots re(A1n) im(A1n)
    A21 A22 \ldots A2n   if complex values   re(A21) im(A21)  \ldots re(A2n) im(A2n)
    \ldots                                    \ldots
    Am1 Am2 \ldots Amn                       re(Am1) im(Am1)  \ldots re(Amn) im(Amn)
\end{verbatim}
and when saved as coordinates format :
\begin{verbatim}                 
    i1 j1 Ai1j1                           i1 j1 re(Ai1j1) im(Ai1j1) 
    i2 j2 Ai2j2     if complex values     i2 j2 re(Ai2j2) im(Ai2j2) 
    \ldots                                   \ldots
\end{verbatim}
Note that \cmd{printDenseMatrix} and  \cmd{printCooMatrix} functions are template functions. Their coding is not optimal because they use the virtual \verb?pos? function
and other external utilities which "interpret" the type of the template:
\vspace{.1cm}
\begin{lstlisting}
void printDense(ostream&, const Real&, Number);
void printDense(ostream&, const Complex&, Number);
template<typename T> 
  void printDense(ostream&, const Matrix<T>&, Number); 
  void printCoo(ostream&, const Matrix<T>&, Number);
void printCoo(ostream&, const Real&, Number);
void printCoo(ostream&, const Complex&, Number);
Number numberOfRows(const Real&);
Number numberOfRows(const Complex&);
Number numberOfCols(const Real&);
Number numberOfCols(const Complex&);
template<typename T> Number numberOfRows(const Matrix<T>&);
template<typename T> Number numberOfCols(const Matrix<T>& m);
\end{lstlisting}
\vspace{.2cm} 
The \verb?visual? function prints only the matrix entries inside the storage as 'x' characters (d for diagonal entries):
\begin{lstlisting}[deletekeywords={[3] size,dx}]
matrix storage :row_compressed sparse (csr,csc), size = 49, 9 rows, 9 columns (shared by 0 objects).
          123456789
       1  dx.xx\ldots.
       2  xdxxxx\ldots
       3  .xd.xx\ldots
       4  xx.dx.xx.
       5  xxxxdxxxx
       6  .xx.xd.xx
       7  \ldotsxx.dx.
       8  \ldotsxxxxdx
       9  \ldots.xx.xd
          123456789
\end{lstlisting}
\vspace{.1cm} 
The \class{MatrixStorage} class provides the product matrix \(\times\) vector and the product vector \(\times\) matrix:
\vspace{.1cm}
\begin{lstlisting}
virtual 
 void multMatrixVector(const vector<Real>&, const vector<Real>&, vector<Real>&, SymType) const;
 void multMatrixVector(const vector< Matrix<Real> >&, const vector<Vector<Real>>&, vector Vector<Real>>&, SymType) const;
 void multVectorMatrix(const vector<Real>&, const vector<Real>&, vector<Real>&, SymType) const;
 void multVectorMatrix(const vector<Matrix<Real>>&, const vector<Vector<Real>>&, vector<Vector<Real>>&, SymType) const;
\end{lstlisting}
\vspace{.1cm} 
Note that mixing real vector/matrix and complex vector/matrix is also provided. Each child class implements the best algorithm for the product.\\
The addition of two matrices is supplied with:
\vspace{.1cm}
\begin{lstlisting}
virtual void addMatrixMatrix(const vector<Real>&, const vector<Real>&, 
			     vector<Real>&) const;
\end{lstlisting}
\vspace{.1cm} 
Similar to multiplication of matrix-vector, this function is implemented in each child class.
The \class{MatrixStorage} class also provides the matrix factorizations:
\vspace{.1cm}
\begin{lstlisting}
virtual
 void ldlt(vector<Real>&, vector<Real>&, const SymType sym = _symmetric) const;
 void ldlt(vector<Complex>&, vector<Complex>&, const SymType sym = _symmetric) const;    
 void lu(vector<Real>&, vector<Real>&, const SymType sym = _noSymmetry) const;
 void lu(vector<Complex>&, vector<Complex>&, const SymType sym = _noSymmetry) const;    
 void ldlstar(vector<Real>&, vector<Real>&) const;
 void ldlstar(vector<Complex>&, vector<Complex>&) const;
\end{lstlisting}
\vspace{.1cm}
as well as some special solvers
\vspace{.1cm}
\begin{lstlisting}
virtual 
 void lowerD1Solver(const vector<Real>&, vector<Real>&, vector<Real>&) const;
 void diagonalSolver(const vector<Real>&, vector<Real>&, vector<Real>&) const
 void upperD1Solver(const vector<Real>&, vector<Real>&, vector<Real>&, 
                    const SymType sym = _noSymmetry) const;
 void upperSolver(const vector<Real>&, vector<Real>&, vector<Real>&, 
                  const SymType sym = _noSymmetry) const;
 void sorDiagonalMatrixVector(const vector<Real>&, const vector<Real>&, vector<Real>&, 
                              const Real) const;
 void sorLowerMatrixVector(const vector<Real>&, const vector<Real>&, vector<Real>& ,Real, 
                           const SymType = _noSymmetry) const;
 void sorUpperMatrixVector(const vector<Real>&, const vector<Real>&, vector<Real>&, Real, 
                           const SymType = _noSymmetry) const;
 void sorDiagonalSolve(const vector<Real>&, const vector<Real>&, vector<Real>&, Real) const;
 void sorLowerSolve(const vector<Real>&, const vector<Real>&, vector<Real>&, Real) const;
 void sorUpperSolve(const vector<Real>&, const vector<Real>&, vector<Real>&, Real, 
                    const SymType sym = _noSymmetry) const;
\end{lstlisting}
\vspace{.1cm}
Each descendant will implement the specific algorithm for these virtual functions.
Values on the diagonal of a matrix is set with the function
\vspace{.1cm}
\begin{lstlisting}
virtual void setDiagValue(vector<Real>&, Real);
\end{lstlisting}
\vspace{.1cm}
\displayInfos{library=largeMatrix, header=MatrixStorage.hpp, implementation=MatrixStorage.cpp, test={test\_LargeMatrix...Storage.cpp}, header dep={config.h, utils.h}}
                        
