%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{Value} class}

The \class{Value} class is a utility class to encapsulates user's data involved in a differential
expression. Such data may be a function (\class{Function} object) or any real or complex scalar,
vector or matrix value. This class is designed as follows:
\vspace{.2cm}
\begin{lstlisting}
class Value
{
protected :
  ValueType type_;   //type of returned value ( _real, _complex)
  StrucType struct_; //structure of returned value (_scalar, _vector, _matrix,
	                         _vectorofvector, _vectorofmatrix, _matrixofmatrix)
  void * value_p;          //void pointeur to the true value 
  mutable bool conjugate_; //temporary conjugate state flag 
  mutable bool transpose_; //temporary transposition state flag 
\end{lstlisting}
\vspace{.2cm}
The attributes \textit{conjugate\_} and \textit{transpose\_} are temporary flags used when
chaining Value object in a differential expression.
They have no meaning outside a particular context!\\
In order to recast safely the void pointer \textit{value\_p} pointing to the actual data, this
class uses the Run Time Information, to store the names of data type supported:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] map}]
typedef std::pair<ValueType,StrucType> structPair;
typedef std::pair<String,structPair> mapPair;
static std::map<String,structPair > theValueTypeRTInames;  
static void valueTypeRTINamesInit();                        
\end{lstlisting}
\vspace{.2cm}
The only constructors allowed are the following: 
\vspace{.1cm}
\begin{lstlisting}
Value(const real_t &);
Value(const Vector<real_t> &);
Value(const Matrix<real_t> &);
Value(const Vector<Vector<real_t> > &);
Value(const Vector<Matrix<real_t> >&);
Value(const Matrix<Matrix<real_t> >&);
Value(const complex_t &);
Value(const Vector<complex_t> &);
Value(const Matrix<complex_t> &);
Value(const Vector<Vector<complex_t> > &);
Value(const Vector<Matrix<complex_t> >&);
Value(const Matrix<Matrix<complex_t> >&);
\end{lstlisting}
\vspace{.2cm}
They correspond to the data types that are supported by this encapsulation class. These constructors
create in memory a copy of the given value.\\

There are two template functions to check the data types:
\vspace{.1cm}
\begin{lstlisting}
template<typename T> static bool checkTypeInList(const T &); 
template<typename T> static bool isTypeInList(const T&);                        
template<typename T> void checkType(const T&) const;  
\end{lstlisting}
\vspace{.2cm}
The two first ones checks if the type T is in the list of allowed types and the second one
compares the type T to the type of the current Value. To get the actual value, you have to
use the templated member function: 
\vspace{.1cm}
\begin{lstlisting}
template<class T> T& value() const;
\end{lstlisting}
\vspace{.2cm}
Note that you have to explicit the template argument T. This function checks the value type
and recasts the void pointer. \\

Besides, there are accessors/modifiers member functions:
\vspace{.1cm}
\begin{lstlisting}
ValueType valueType() const{return type_;}    
StrucType strucType() const{return struct_;} 
bool  conjugate()const {return conjugate_;}   
bool& conjugate() {return conjugate_;}       
void  conjugate(bool v) const {conjugate_=v;} 
bool  transpose()const {return transpose_;}  
bool& transpose() {return transpose_;}        
void  transpose(bool v) const {transpose_=v;} 
\end{lstlisting}
\vspace{.2cm}          
In order to conjugate or transpose (or both) a value in an expression, this class provides
the functions:
\vspace{.1cm}
\begin{lstlisting}   
Value& conj(Value &);                
Value& trans(Value &);                 
Value& adj(Value &);                   
\end{lstlisting}
\vspace{.1cm} 
Be care, these functions do not modify the value but just set the\textit{ conjugate\_}/\textit{transpose\_}
flag to its opposite.\\

Finally, there are some printing functions:
\vspace{.1cm}
\begin{lstlisting}   
static void Value::printValueTypeRTINames(std::ostream&);   
void Value::print(std::ostream &) const;
friend std::ostream& operator<<(std::ostream&,const Value &);              
\end{lstlisting}
\vspace{.2cm} 
Here, we give a small example to illustrate how the \class{Value} class works:
\vspace{.1cm}
\begin{lstlisting}   
Value::printValueTypeRTINames(cout);     //print on stdout the RTI names of value types
real_t s=1.;
Value Vs(s);                             //create Value object
cout<<"real Value Vs(s) :"<<Vs;          //print Value
real_t r =Vs.value<real_t>();            //get as a real
Vector<complex_t> vc(3,complex_t(0,1));	 //a vector of complex values
Value Vvc(vc);
cout<<"complex vector Value Vvc(vc) :"<<Vvc;         //print Value
Vector<complex_t> wc=Vvc.value<Vector<complex_t> >();//get as a complex vector      
\end{lstlisting}
\vspace{.2cm} 

\displayInfos{library=operator, header=Value.hpp, implementation=Value.cpp, test=test\_operator.cpp,
header dep={config.h, utils.h}}
