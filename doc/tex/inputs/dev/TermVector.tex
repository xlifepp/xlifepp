%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../dev_documentation.tex
 
\section{The \classtitle{TermVector} class}

The \class{TermVector} class carries numerical representation of a linear form and more generally any vector attached to an approximation space:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] map}]
class TermVector : public Term
{
 protected :
 LinearForm linForm_;                                 
 map<const Unknown*, SuTermVector*> suTerms_;  
 VectorEntry* entries_p;                               
 VectorEntry* scalar_entries_p;                      
 vector<DofComponent> cDoFs_;                    
\end{lstlisting}
\vspace{.3cm}
The linear form is a multiple unknowns form, may be reduced to a single unknown form (see \lib{form} library). 
This linear form is void if \class{TermVector} is not explicitly construct from a linear form (linear combination, nodal construction). 
The map \emph{suTerms\_} carries each vector block related to an unknown. 
When there is only one element in map, it means that the \class{TermVector} is actually a single unknown term vector. 
The \emph{scalar\_entries\_p} pointer may be allocated to concatenate the block vectors. When unknowns are vector unknowns,  
the representation may be not suited in certain cases (direct solver), so it is possible to create its scalar representation 
using the \emph{scalar\_entries\_p} (\class{VectorEntry} pointer). 
The \class{VectorEntry} object created has to be of real scalar or complex scalar type. In that case, a non block row numbering consisting in vector of \class{DofComponent} (Unknown pointer, DoF number, component number) is also defined (\emph{cDoFs\_}).\\

The \class{TermVector} class provides useful constructors and assign operators designed for end users:
\vspace{.1cm}
\begin{lstlisting}
//default constructor
TermVector(const String& na = "", bool noass = false); 

//from linear form with options and no essential conditions
TermVector(const LinearForm&, const string_t& na="");
TermVector(const LinearForm&, TermOption, const string_t& na="");
TermVector(const LinearForm&, TermOption, TermOption, const string_t& na="");
TermVector(const LinearForm&, TermOption, TermOption, TermOption, 
           const string_t& na="");

//from linear form with options and one essential condition
TermVector(const LinearForm&, const EssentialConditions&, 
           const string_t& na="");
TermVector(const LinearForm&, const EssentialConditions&, TermOption, 
           const string_t& na="");
TermVector(const LinearForm&, const EssentialConditions&, TermOption, 
           TermOption, const string_t& na="");
TermVector(const LinearForm&, const EssentialConditions&, TermOption, 
           TermOption, TermOption, const string_t& na="");

void build(const LinearForm &, const EssentialConditions*, 
           const vector<TermOption>&, const string_t&);

//from value or function (interpolation) 
template <typename T>
  TermVector(const Unknown&, const Domain&, const T&, const String& na = ""); 

//copy like
TermVector(const TermVector&, const String& na = ""); 
template <typename T>
  TermVector(const TermVector&, const T&, const String& na);
TermVector(const SuTermVector &, const String& na="");    
TermVector(SuTermVector *, const String& na=""); 
void copy(const TermVector&, const String &);  

//constructor from explicit function of TermVector's
TermVector(const TermVector&, funSR1_t& f, const string_t& na="");               
TermVector(const TermVector&, TermVector&, funSR2_t& f, const string_t& na="");  
TermVector(const TermVector&, funSC1_t& f, const string_t& na="");               
TermVector(const TermVector&, TermVector&, funSC2_t& f, const string_t& na="");  single unknown

//constructor from symbolic function of TermVector's
TermVector(const TermVector&, const SymbolicFunction& , const string_t& na="");
TermVector(const TermVector&, const TermVector&, const SymbolicFunction& fs,
const string_t& na="");                                             
TermVector(const TermVector&, const TermVector&, const TermVector&,
const SymbolicFunction& fs, const string_t& na="");                 

//constructor of a vector TermVector from scalar TermVector's (single unknown, same space)
TermVector(const Unknown&, const TermVector&, const TermVector&, const string_t& ="");    
TermVector(const Unknown&, const TermVector&, const TermVector&, const TermVector&, const string_t& ="");
TermVector(const Unknown&, const TermVector&, const TermVector&, const TermVector&, const TermVector&, const string_t& ="");

//from linear combination      
TermVector(const LcTerm&);  
//assignment                              
TermVector& operator=(const TermVector&);                 
TermVector& operator=(const LcTerm&); 
//insert SuTermVector in TermVector
void insert(const SuTermVector &);                        
void insert(SuTermVector *);  
void insert(const Unknown*, SuTermVector*);                           
//destructor, clear                    
virtual ~TermVector(); 
void clear();                                     
\end{lstlisting}
\vspace{.3cm}

When TermVector is constructed from linear form, it is automatically computed except if it has set to be not computed using the \class{TermOption} \textit{\_notCompute} in its construction.\\ \class{TermOption}s are managed as an enumeration:
\begin{lstlisting}
enum TermOption
{
 //general
 _compute,                 //default behavior
 _notCompute,
 _assembled,               //default behavior
 _unassembled,             //not yet available
 //only for TermMatrix
 _nonSymmetricMatrix,      //to force symmetry property 
 _symmetricMatrix,
 _selfAdjointMatrix,
 _skewSymmetricMatrix,
 _skewAdjointMatrix,
 _csRowStorage,            //to force storage
 _csColStorage,
 _csDualStorage,
 _csSymStorage,
 _denseRowStorage,
 _denseColStorage,
 _denseDualStorage,
 _skylineSymStorage,
 _skylineDualStorage,
 _pseudoReduction,         //default behavior
 _realReduction,           //not yet available
 _penalizationReduction    //not yet available
};
\end{lstlisting}

Constructors involving \class{SuTermVector} class are reserved to developers. End users should not have to access to \class{SuTermVector} class, but we decide to let this access free to advanced users. Constructors involving \class{LcTerm} class are implicitly used by users when they write combination of terms in a symbolic way (see \class{LcTerm} class).\\
The class provides some useful accessors (for users or developers):
\vspace{.1cm}
\begin{lstlisting}
//user accessors
const LinearForm& linearForm() const;
Number nbOfUnknowns() cons;
bool isSingleUnknown() const;
set<const Unknown*> unknowns() const;
ValueType valueType() const;
Number size() const;
Number nbDofs() const;
Number nbDofs(const Unknown&) const;
const vector<DofComponent>& cDoFs() const;
vector<DofComponent>& cDoFs(;
const Dof& DoF(const Unknown&, Number ) const;      

//developer accessors                             
cit_mustv begin() const;
it_mustv begin();
cit_mustv end() const;
it_mustv end();
SuTermVector* firstSut() const;
SuTermVector* firstSut();
const Unknown* unknown() const;
VectorEntry*& entries();
const VectorEntry* entries() const;
VectorEntry*& scalar_entries();
const VectorEntry* scalar_entries() const;
VectorEntry* actual_entries() const;
\end{lstlisting}
\vspace{.3cm}
\emph{it\_mustv} and \emph{cit\_mustv} are iterator aliases:\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3] map}]
typedef map<const Unknown*, SuTermVector*>::iterator it_mustv;
typedef map<const Unknown*, SuTermVector*>::const_iterator cit_mustv;
\end{lstlisting}
\vspace{.3cm}
Access to a block of \class{TermVector} say a \class{SuTermVector} is proposed in two ways: either using \cmd{subVector} functions returning reference or pointer to \class{SuTermVector} objects or  using \cmd{operator} returning a new \class{TermVector} object, the \class{SuTermVector} object being copied. This last method is addressed to end users. For particular purpose, is also possible to reinterpret a \class{TermVector} as a raw vector or a raw vector of vectors (say a \class{Vector<S>} or a \class{Vector<Vector<S> >} with S a Real or a Complex). \\
\vspace{.1cm}
\begin{lstlisting}
const SuTermVector& subVector() const;
SuTermVector& subVector(const Unknown*);
const SuTermVector& subVector(const Unknown*) const;
SuTermVector& subVector(const Unknown&);  
const SuTermVector& subVector(const Unknown&) const;  
SuTermVector* subVector_p(const Unknown*); 
const SuTermVector* subVector_p(const Unknown*) const;
TermVector operator()(const Unknown&) const; //user usage 
template<typename T>
  Vector<T>& asVector(Vector<T>&) const;
\end{lstlisting}
\vspace{.3cm}
The main computing operations are related to the construction of \class{TermVector} from linear form and construction from linear combination \class{TermVector}:
\vspace{.1cm}
\begin{lstlisting}
void compute();                                        
void compute(const LcTerm&);   //implicit                          
TermVector& operator+=(const TermVector&);             
TermVector& operator-=(const TermVector&);             
template<typename T>
 TermVector& operator*=(const T&);                     
template<typename T>
 TermVector& operator/=(const T&);  
\end{lstlisting}
\vspace{.2cm}
Note that the evaluation of \class{TermVector} defined by a value or a function is done during creation of \class{TermVector}. The algebraic operators +=, -=, *=, /= do the computation, it is not delayed as it is the case for +,-,* and / operators. The linear combination is based on \class{LcTerm} class which manages a list of pairs of \class{Term} and coefficient. Algebraic operators on \class{TermVector}'s produce \class{LcTerm} object carrying the linear combination that will be computed by \class{TermVector} constructor or assignment operator:
\vspace{.1cm}
\begin{lstlisting}
const TermVector& operator+(const TermVector&);
LcTerm operator-(const TermVector&);
LcTerm operator+(const TermVector&, const TermVector&);
LcTerm operator-(const TermVector&, const TermVector&);
LcTerm operator+(const LcTerm&, const TermVector&);
LcTerm operator-(const LcTerm&, const TermVector&);
LcTerm operator+(const TermVector&, const LcTerm&);
LcTerm operator-(const TermVector&, const LcTerm&);
template<typename T>
 LcTerm operator*(const TermVector&, const T&);
template<typename T>
 LcTerm operator*(const T&, const TermVector& )
template<typename T>
 LcTerm operator/(const TermVector&, const T&)
\end{lstlisting}
\vspace{.3cm}

To manage different representation of \class{TermVector}, the following function are provided:
\vspace{.1cm}
\begin{lstlisting}
void toScalar(bool keepEntries=false); 
void toVector(bool keepEntries=false);                 
void toGlobal(bool keepSuTerms=false);                 
void toLocal(bool keepGlobal=false);                   
void adjustScalarEntries(const vector<DofComponent>&); 
\end{lstlisting}
\vspace{.3cm}
Note the difference between \emph{toScalar} and \emph{toGlobal}. \emph{toScalar} forces the scalar representation of \class{SuTermVector}'s and \emph{toGlobal} go to non block representation, allocating the \emph{scalar\_entries\_p} pointer of \class{TermVector}. \emph{toGlobal} operation induces  \emph{toScalar} operation. \emph{toLocal} reconstructs block representation from global representation (inverse of \emph{toGlobal}) and \emph{toVector} returns to vector representation (inverse of \emph{toScalar}).\\

{ \renewcommand{\arraystretch}{2}
\[
 \begin{array}{cccc}
 \begin{array}{c}
 \text{\small SuTermVector} \\ [-5mm] \text{\small entries\_p}
 \end{array}
 &\begin{array}{|c|}
   \hline 
   \begin{array}{|c|} \hline \vdots \\ \hline \end{array} \\   
   \begin{array}{c}  \vdots  \end{array} \\ 
   \begin{array}{|c|} \hline \vdots \\  \hline \end{array}\\ [2mm]    
  \hline
 \end{array}
 &\begin{array}{c}
      \text{\small SuTermVector} \\ [-5mm] \text{\small scalar\_entries\_p}
   \end{array}
 &\begin{array}{|c|}  
          \hline 
          \begin{array}{c} \vdots \\ \end{array} \\  
          \begin{array}{c} \vdots  \end{array} \\  
          \begin{array}{c} \vdots \\  \end{array} \\ [2mm] 
          \hline
        \end{array}\\
 
 & & \stackrel{toScalar}{\Longrightarrow} & \\ 
  
  \begin{array}{c}
  \text{\small SuTermVector} \\ [-5mm] \text{\small entries\_p}
  \end{array}
   &\begin{array}{|c|}  
     \hline 
     \begin{array}{|c|} \hline \vdots \\ \hline \end{array} \\  
     \begin{array}{c} \vdots  \end{array} \\  
     \begin{array}{|c|} \hline \vdots \\ \hline \end{array} \\ [2mm] 
     \hline
   \end{array}
   &\begin{array}{c}
     \text{\small SuTermVector} \\ [-5mm] \text{\small scalar\_entries\_p}
   \end{array}
   &\begin{array}{|c|}  
         \hline 
         \begin{array}{c} \vdots \\ \end{array} \\  
         \begin{array}{c} \vdots  \end{array} \\  
         \begin{array}{c} \vdots \\  \end{array} \\ [2mm] 
         \hline
       \end{array}\\
\end{array} 
\ \ \ \ \ \stackrel{toGlobal}{\Longrightarrow} \ \ \ \ \ \ \\  
 \begin{array}{|c|} \hline \vdots \  \\ \vdots \\ \vdots \\ \vdots \\ \vdots \\ \vdots \\ \vdots \\ \vdots \\ \hline \end{array} \\
 \begin{array}{c}
  \text{\small TermVector} \\[-5mm] \text{\small scalar\_entries\_p}
  \end{array}
\]
}

In some circumstances, to avoid recomputation, it may be useful to change the unknowns of a \class{TermVector} without changing its content, with an optional management of components of unknown:
\vspace{.1cm}
\begin{lstlisting}
void changeUnknown(const Unknown&, const Unknown&,
                   const Vector<Number>& = Vector<Number>()); 
void changeUnknown(const Unknown&,
                   const Vector<Number>& = Vector<Number>());
void changeUnknown(const Unknown&, Number);          //shortcut
void changeUnknown(const Unknown&, Number, Number);  //shortcut    
void changeUnknown(const Unknown&, Number, Number, Number); //shortcut
\end{lstlisting}
\vspace{.2cm}
The principle of \cmd{changeUnknown} functions is the following :
\begin{itemize}
\item When two unknowns (u, v) are specified, the unknown u is replaced by the unknown v and the reference to unknown u does no longer exists in \class{TermVector};
\item When a renumbering components vector (rc) is given too, components are re-settled along this vector;
\item When v is not specified, only components are re-settled.
\end{itemize}
Here are some examples:

\begin{itemize}
\item u a scalar unknown, v a scalar unknown, replace unknown u by unknown v
\item u a scalar unknown, v a 3-vector unknown, rc=[2] leads to sutv = [0 sutv 0]
\item u a 4-vector unknown, v a 2-vector unknown, rc=[2,4] leads to sutv = [0 sutv1 0 sutv2]
\item u a 3-vector unknown, rc=[3,0,1], re-settle components as [sutu3 0 sutu1]
\end{itemize}

\begin{warningbox}
Be cautious that there is no check of the space consistency! So use it with care.
\end{warningbox}

It is possible to address values of a \class{TermVector} by unknown and index, to get it or to set it:
\vspace{.1cm}
\begin{lstlisting}
Value getValue(const Unknown&, Number) const;                 
void setValue(const Unknown&, Number, const Value&);         
void setValue(const Unknown&, Number, const Real&);           
void setValue(const Unknown&, Number, const Complex&);        
void setValue(const Unknown&, Number, const vector<Real>&);    
void setValue(const Unknown&, Number, const vector<Complex>&);                     
\end{lstlisting}
\vspace{.2cm}
The \class{Value} class handles different type of values, in particular \cmd{Real} and \cmd{Complex} values.\\

It is also possible to change some values of a \class{TermVector} by specifying the unknown that it is concerned, the geometrical part where values will be changed and the values that can be given either by a constant, a function or a \class{TermVector}: 
\vspace{.1cm}
\begin{lstlisting}
number_t DoFRank(const Unknown&, const Dof&)const;              
number_t DoFRank(const Dof&) const;                             
Value getValue(const Unknown&, number_t) const;                   
Value getValue(const Unknown& u, const Dof& DoF) const           
void setValue(const Unknown&, number_t, const Value&);            
void setValue(const Unknown& u, const Dof& DoF, const Value& v)  
void setValue(const Unknown&, number_t, const real_t&);           
void setValue(const Unknown&, number_t, const complex_t&);       
void setValue(const Unknown&, number_t, const std::vector<real_t>&);   
void setValue(const Unknown&, number_t, const std::vector<complex_t>&); 
template <typename T>
void setValue(const Unknown&, const GeomDomain&, const T&);        
void setValue(const Unknown&, const GeomDomain&, const TermVector&);  
void setValue(const Unknown&, const TermVector&);                                    
\end{lstlisting}
\vspace{.2cm}
In all \cmd{setValue} and \cmd{getValue} functions, the unknown may be omitted. In that case, the first unknown is assumed.\\

Most powerful functions are functions that can evaluate any function of approximation space (\(V_h\)) (representing by a \class{TermVector} U) at any point, using space interpolation :
\[
u_h(x)=\sum_{i=1,n} U_{u,i}w_i(x)
\]
where \(U_{u,i}\) is the \(i^{th}\) components related to the unknown \(u\) of \class{TermVector} \(U\) and \((w_i)_{i=1,n}\) the basis functions of approximation space \(V_h\).
\vspace{.1cm}
\begin{lstlisting}
Value evaluate(const Unknown&, const Point&) const;    
template<typename T>
 T& operator()(const vector<Real>&, T&) const;      
template<typename T>
 T& operator()(const Unknown&, const vector<Real>&, T&) const;  
\end{lstlisting}
\vspace{.2cm}

Some additional computational tools are available:
\vspace{.1cm}
\begin{lstlisting}
TermVector& toAbs();                                   
TermVector& toReal();                                  
TermVector& toImag();  
TermVector& toComplex();                                   
TermVector& toConj();   
TermVector& roundToZero(real_t aszero=10*theEpsilon);                                                                      
Real norm2() const;                                   
Complex maxValAbs() const;                            
\end{lstlisting}
\vspace{.2cm}
\class{TermVector} may be related to essential conditions either by defining an essential condition from a \class{TermVector} (e.g. \cmd{ u=TU}) or by applying some essential conditions (\class{SetOfConstraints} or \class{EssentialConditions} objects) to it:
\vspace{.1cm}
\begin{lstlisting}
EssentialCondition operator = (const complex_t &);         
TermVector& applyEssentialConditions(const SetOfConstraints&,const ReductionMethod& =ReductionMethod()); 
TermVector& applyEssentialConditions(const EssentialConditions&,const ReductionMethod& =ReductionMethod()); 
TermVector& applyBoundaryConditions(const EssentialConditions& ecs,const ReductionMethod& rm=ReductionMethod()); 
\end{lstlisting}
\vspace{.2cm}
In order to use a \class{TermVector} as an interpolated function, the \cmd{toFunction} builds a new function where the \class{TermVector} is passed to the \class{Parameters} of some particular functions that implements the interpolation:
\vspace{.1cm}
\begin{lstlisting}
const Function& toFunction() const; 
real_t fun_EC_SR(const Point& P, Parameters& pars);  
real_t fun_EC_SC(const Point& P, Parameters& pars);
real_t fun_EC_VR(const Point& P, Parameters& pars);
real_t fun_EC_VC(const Point& P, Parameters& pars);                          
\end{lstlisting}
\vspace{.2cm}
 
The following functions produce new \class{TermVector}'s that are defined on another domain, one mapping the values using a predefined map between two domains the other restricting the values to one domain (included in the current one):
\vspace{.1cm}
\begin{lstlisting}                   
TermVector mapTo(const GeomDomain&, const Unknown&, bool errOutDom =true) const;             
TermVector onDomain(const GeomDomain&) const;           
\end{lstlisting}
In a same way, a \class{TermVector} defined on a side domain (say $\Gamma$) may be extended to a domain (say $\Omega$) using a map $m : \Omega \rightarrow \Sigma$ and an extension function $e$ as follows :
$$ ext(f)(M) = f(m(M))*e(M)$$
where $f(M)=\sum_i f_iw_i(M)$. The following external functions are provided :
\vspace{.1cm}
\begin{lstlisting}                   
TermVector extension(const TermVector& f,const Function& m,const GeomDomain& dom,const Unknown& u);
TermVector extension(const TermVector& f,const Function& m,const GeomDomain& dom,const Unknown& u,const Function& e);          
\end{lstlisting}
The first one corresponds to a constant extension ($e=1$). The unknown $u$ must be attached to a Lagrange interpolation space ! The function $f$ may be an analytical function given by a \xlifepp object function :
\begin{lstlisting}                   
TermVector extension(const Function& f const Function& g,const GeomDomain& dom,const Unknown& u);
TermVector extension(const Function& f,const Function& g,const GeomDomain& dom,const Unknown& u,const Function& h);
\end{lstlisting}
These functions are based on punctual functions:
\begin{lstlisting}                   
Value extension(const Function& f,const Function& g,const Point& P);
Value extension(const Function& f,const Function& g,const Point& P,const Function& h);
Value extension(const TermVector& f,const Function& g,const Point& P);
Value extension(const TermVector& f,const Function& g,const Point& P,const Function& h);      
\end{lstlisting}

The class proposes some  basic output functions:
\vspace{.1cm}
\begin{lstlisting}
void print(ostream&) const;                      
void saveToFile(const String&, bool encode=false) const;                     
\end{lstlisting}
\vspace{.2cm}

Besides, a few functionalities are also provides as external functions:
\vspace{.1cm}
\begin{lstlisting}
//linear combination
const TermVector& operator+(const TermVector&);
LcTerm operator-(const TermVector&);
LcTerm operator+(const TermVector&, const TermVector&);
LcTerm operator-(const TermVector&, const TermVector&);
LcTerm operator+(const LcTerm&, const TermVector&);
LcTerm operator-(const LcTerm&, const TermVector&);
LcTerm operator+(const TermVector&, const LcTerm&);
LcTerm operator-(const TermVector&, const LcTerm&);
template<typename T>
 LcTerm operator*(const TermVector&, const T&);
template<typename T>
 LcTerm operator*(const T&, const TermVector&);
template<typename T>
 LcTerm operator/(const TermVector&, const T&)
 
//inner, hermitian product, norm, \ldots
TermVector abs(const TermVector&);
TermVector real(const TermVector&);
TermVector imag(const TermVector&);
TermVector conj(const TermVector&);
Complex innerProduct(const TermVector&, const TermVector&);
Complex hermitianProduct(const TermVector&, const TermVector&);
Complex operator|(const TermVector&, const TermVector&);
Real norm(const TermVector&, Number l=2);    
Real norm1(const TermVector&);         
Real norm2(const TermVector&);         
Real norminfty(const TermVector&); 

//output functions 
void print(ostream&) const;                      
void saveToFile(const String&, bool encode=false) const;
void saveToFile(const String&, const list<const TermVector*>&, IOFormat iof=_raw);
void saveToFile(const String&, const TermVector&, IOFormat iof=_raw);
void saveToFile(const String&, const TermVector&, const TermVector&, 
                IOFormat iof=_raw);
void saveToFile(const String&, const TermVector&, const TermVector&, 
                const TermVector&, IOFormat iof=_raw);
void saveToFile(const String&, const TermVector&, const TermVector&, 
                const TermVector&, const TermVector&, IOFormat iof=_raw);
\end{lstlisting}
\vspace{.2cm}
Available export format are 
\begin{itemize}\addtolength{\itemsep}{-0.5\baselineskip}
	\item \verb|_raw| : export only the values in a raw format
	\item \verb|_vtk| : export mesh and values displayable with Paraview
	\item \verb|_vtu| : export mesh and values displayable with Paraview
	\item \verb|_matlab| : export mesh and values as an executable Matlab file
	\item \verb|_xyzv|: export mesh nodes and values as x y z v1 v2 \ldots
\end{itemize}
Integral representation is a particular operation mixing numerical quadrature and interpolation :
\[
  u(x_i)=\int_{\Gamma} opk(K)(x_i,y)\, aop\, opu(p)(y)dy\ \ i=1,n.
\]
where  \(K\) is a kernel, \(opk\) an operator on kernel (\class{OperatorOnKernel}), \(p\) a layer potential, \(opu\) an operator on unknown (\class{OperatorOnUnknown}) and \(aop\) an algebraic operator. 
\vspace{.1cm}
\begin{lstlisting} 
template<typename T>
Vector<T>& integralRepresentation(const vector<Point>& xs, 
           const LinearForm& lf, const TermVector& U, Vector<T>& val)          
TermVector integralRepresentation(const Unknown&, const GeomDomain&, 
           const LinearForm&, const TermVector&); 
\end{lstlisting}
\vspace{.1cm} 
For instance,  the single layer integral representation on a list of points is computed as follows: 
\vspace{.1cm} 
\begin{lstlisting} 
Vector<Point>Points;
\ldots
Vector<Real> val;
integralRepresentation(Points,intg(gamma,G(_y) * u), U, val);
\end{lstlisting}
\vspace{.1cm}
or if the list of points is the list of nodes of a domain:
\vspace{.1cm} 
\begin{lstlisting} 
 TermVector IR=integralRepresentation(Omega,intg(gamma,G(_y) * u), U, val);
 \end{lstlisting}
\vspace{.3cm}
For most functions of TermVector, the algorithms travel the map of \class{SuTermVector}, calling \class{SuTermVector}'s functions. For instance, the compute function looks like:
\vspace{.1cm}
\begin{lstlisting}
void TermVector::compute()
{
  for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++) 
    {
      if(!it->second->computed()) it->second->compute();
    }
  computed() = true;
}
\end{lstlisting}
\vspace{.3cm} 
\displayInfos{
library=term, 
header=TermVector.hpp, 
implementation=TermVector.cpp,
test=test\_TermVector.cpp,
header dep={SuTermVector.hpp, termUtils.hpp, Term.hpp, form.h, config.h, utils.h}
}
