%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
This chapter is devoted to the basics of C++ language required to use \xlifepp. It is addressed to people who do not know C++.

\section{Instruction sequence}

All C++ instructions (ending by semicolon) are defined in block delimited by braces:
\begin{lstlisting}
{
 instruction;
 instruction;
 \ldots
} 
\end{lstlisting}
Instruction block may be nested in other one:
\begin{lstlisting}
{
 instruction;
 {
  instruction ;
  instruction ;
 }
 \ldots
} 
\end{lstlisting}
and are naturally involved in tests, loops, \ldots and functions.\\

A function is defined by its name, a list of input argument types, an output argument type and an instruction sequence in an instruction block:
\begin{lstlisting}
argout name_of_function(argin1, argin2, \ldots)
{
 instruction;
 instruction;
 \ldots
 return something;
} 
\end{lstlisting}
The main program is a particular function returning an error code:
\begin{lstlisting}
int main()
{
 \ldots
 return 0;  //no error
}
\end{lstlisting}
 
\section{Variables}

In C++, any variable has to be declared, say defined by specifying its type. Fundamental types are:
\begin{itemize}
\item Integer number: \textbf{int} (\var{Int} type in \xlifepp), \textbf{unsigned int} (\var{Number} type in \xlifepp) and \textbf{short unsigned int} (\var{Dimen} type in \xlifepp)
\item Real number: \textbf{float} for single precision (32bits) or \textbf{double} (64bits) for double precision (\var{Real} type in \xlifepp)
\item Boolean: \textbf{bool} that takes \var{true} (1) or \var{false} (0) as value
\item Character: \textbf{char} containing one of the standard ASCII character 
\end{itemize}

All other types are derived types (pointer, reference) or classes (\textbf{Complex}, \textbf{String} for instance).\\

All variable names must begin with a letter of the alphabet. Do not begin by underscore (\_) because it is used by \xlifepp. After the first initial letter, variable names can also contain letters and numbers. No spaces or special characters, however, are allowed. Upper-case characters are distinct from lower-case characters.\\

A variable may be declared anywhere. When they are declared before the beginning of the main, they are available anywhere in the file where they are declared.\\

\begin{focusbox}
All variables declared in an instruction block are deleted at the end of the block.
\end{focusbox}

\section{Basic operations}

The C++ provides a lot of operators. The main ones are :

\begin{itemize}
\item \verb|=| : assignment
\item \verb|+, -, * , /| :  usual algebraic operators 
\item \verb|+=, -=, *=, /= | : operation on left variable
\item \verb|++, --|: to increment by 1 (\verb|+=1|) and decrement by 1 (\verb|-=1|)
\item \verb|== != < > <= >= !| : comparison operators and negation
\item \verb|&&|, || : logical and, or
\item \verb|<<, >>| : to insert in a stream (read, write)
\end{itemize}

All these operators may work on object of a class if they have been defined for this class. See documentation of a class to know what operators it supports.\\

The operators \verb|+=, -=, *=, /= | may be useful when they act on large structure because they, generally, do not modify their representation and avoid copy. 

\section{if, switch, for and while}

The syntax of test is the following:
\vspace{.2cm}
\begin{lstlisting}
if (predicate)
{
  \ldots
}
else if (predicate2)
{
\ldots
}
else
{
  \ldots
}
\end{lstlisting}
\vspace{.2cm}
{\ttfamily else if} and {\ttfamily else} blocks are optional, and you can have as many {\ttfamily else if} blocks as you want.
{\ttfamily predicate} is a boolean or an expression returning a boolean (\textit{true} or \textit{false}):
\vspace{.2cm}
\begin{lstlisting}[deletekeywords={[3]x,y}]
if ((x==3 && y<=2) || (! a>b))
{
  \ldots
}
\end{lstlisting}
\vspace{.2cm}
For multiple choice, use the \textbf{switch} instruction:
\vspace{.2cm}
\begin{lstlisting}
switch (i)
{
  case 0:
  {
    \ldots
    break;
  }
  case 1:
  {
    \ldots
    break;
  }
  \ldots
 default :
  {
    \ldots.
  }
}
\end{lstlisting}
\vspace{.2cm}

The \textbf{switch} variable has to be of enumeration type (integer or explicit enumeration).\\

The syntax of the \textbf{for} loop is the following:
\vspace{.2cm}
\begin{lstlisting}
for( initialization; end_test; incrementing sequence)
{
 \ldots
}
\end{lstlisting}
\vspace{.2cm}
The simplest loop is:
\vspace{.2cm}
\begin{lstlisting}
for( int i=0; i< n; i++)
{
 \ldots
}
\end{lstlisting}
\vspace{.2cm}
An other example with no initializer and two incrementing values:
\vspace{.2cm}
\begin{lstlisting}
int i=1, j=10;
for(; i< n && j>0; i++, j--)
{
 \ldots
}
\end{lstlisting}
\vspace{.2cm}
A \textbf{for} loop may be replaced by a \textbf{while} loop:
\vspace{.2cm}
\begin{lstlisting}
int i=0;
while(i<n)
{
 \ldots
 i++;
}
\end{lstlisting}

\section{In/out operations}

The simplest way to print something on screen is to use the predefined output stream \textbf{cout} with operator \verb|<<|:

\begin{lstlisting}[deletekeywords={[3]x}]
Real x=2.25;
Number i=3;
String msg=" Xlife++ :";
cout << msg << " x=" << x << " i=" << i << eol;
\end{lstlisting}

\textbf{eol} is the \xlifepp end of line. You can insert in output stream any object of a class as the operator \verb|<<| is defined for this class. Almost all \xlifepp classes offer this feature.\\

To read information from keyboard, you have to use the predefined input stream \textbf{cin} with operator \verb|>>|:    
\begin{lstlisting}[deletekeywords={[3]x}]
Real x;
Number i=3;
cin >> i >> x;
\end{lstlisting}

The program waits for an input of a real value, then for an input of integer value.\\

To print on a file, the method is the same except that you have to define an output stream on a file :

\begin{lstlisting}[deletekeywords={[3]x}]
ofstream out;
out.open("myfile");
Real x=2.25;
Number i=3;
String msg=" Xlife++ :";
out << msg << " x=" << x << " i=" << i << eol;
out.close();
\end{lstlisting} 
To read from a file :
\begin{lstlisting}[deletekeywords={[3]x}]
ifstream in;
in.open("myfile");
Real x;
Number i=3;
in >> i >> x;
in.close();
\end{lstlisting}

The file has to be compliant with data to read. The default separators are white space and carriage return (end of line).\\

To read and write on file in a same time, use \textbf{fstream}.\\

\begin{focusbox}
All stream stuff id defined in the C++ standard template library (STL). To use it, write at the beginning of your c++ files :

\begin{lstlisting}
#include <iostream>
#include <fstream>
\ldots

using namespace std;
\end{lstlisting} 
\end{focusbox}

\section{Using standard functions}

The STL library provides some fundamental functions such as \textbf{abs, sqrt, power, exp, sin, \ldots}. To use it, you have to include the \textit{cmath} header file :
\begin{lstlisting}[deletekeywords={[3]x,y}]
#include <cmath>
using namespace std;
\ldots
double pi=4*atan(1);
double y=sqrt(x);
\ldots
\end{lstlisting}
\vspace{.1cm} 

\section{Use of classes}

The C++ allows defining new types of variable embedding complex structure : say class. A class may handle some data (member) and functions (member functions). A variable of a class is called an object.\\ 

In \xlifepp, you will have only to use it, not to define new one. The main questions are : how to create an object of a class, how to access to its members and how to apply operations on it. To illustrate concepts, we will use the very simple Complex class: 

\begin{lstlisting}[deletekeywords={[3]x,y}]
class Complex
{
  public:
  float x, y;
  Complex(float a=0, float b=0) : x(a), y(b){} 
  float abs() {return sqrt(x*x+y*y);
  Complex& operator+=(const Complex& c)
          {x+=c.x;y+=c.y;return *this;}
  \ldots
};
\end{lstlisting}

Classes have special functions, called constructors, to create object. They have the name of the class and are invoked at the declaration of the object:

\begin{lstlisting}
int main()
{
  Complex z1;          //default constructor       
  Complex z2(1,0);     //explicit constructor
  Complex z4(z2);      //copy constructor
  Complex z5=z3;       //use copy constructor
  
  Complex z4=Complex(0,1);  
}
\end{lstlisting}

Copy constructor and operator \verb|=| are always defined. When operator \verb|=| is used in a declaration, the copy constructor is invoked. The last instruction uses the explicit constructor and the copy constructor. In practice, compiler are optimized to avoid copy.\\ 

To address a member or a member function, you have to use the operator point (.):

\begin{lstlisting}[deletekeywords={[3]x,y,z}]
int main()
{
  Complex z(0,1);
  float r=z.x;
  float i=z.y;
  float a=z.abs();
}
\end{lstlisting}

and to use operators, use it as usual:

\begin{lstlisting}
int main()
{
  Complex z1(0,1), z2(1,0);
  z1+=z2;
}
\end{lstlisting}
\vspace{.2cm}
Most of \xlifepp user's classes have been developed to be close to natural usage.   

\section{Understanding memory usage}

In scientific computing, the computer memory is often asked intensively. So its usage has to be well managed:
\begin{itemize}
\item Avoid copy of large structures (mainly \class{TermMatrix});
\item Clear large object (generally it exists a \cmd{clear} function). You do not have to delete objects, they are automatically destroyed at the end of the blocks where they have been declared !
\item When it is possible, use \verb|+=, -=, *=, /=| operators instead of \verb|+, -, *, /| operators which induce some copies;
\item In large linear combination of \class{TermMatrix}, do not use partial combinations which also induce unnecessary copies and more computation time.
\end{itemize}  

\section{Main user's classes of \xlifepp}

For sake of simplicity, the developers choose to limit the number of user's classes and to restrict the use of template paradigm. Up to now the only template objects are \class{Vector} and \class{Matrix} to deal with real or complex vectors/matrices. The name of every \xlifepp class begins with a capital letter.\\

\xlifepp provides some utility classes (see user documentation for details) :

\begin{description}[itemsep=-5pt]
\item[\class{String}] to deal with character string
\item[\class{Strings}] to deal with vector of character strings
\item[\class{Number}] to deal with unsigned (positive) integers
\item[\class{Numbers}] to deal with vector of unsigned (positive) integers
\item[\class{Real}] to deal with floats, whatever the precision.
\item[\class{Reals}] to deal with vector of floats, whatever the precision.
\item[\class{Complex}] to deal with complexes
\item[\class{Vector<T>}] to deal with numerical vectors (T is a real/complex scalar or real/complex Vector)
\item[\class{Matrix<T>}] to deal with numerical matrices (T is a real/complex scalar or real/complex Matrix)
\item[\class{RealVector}, \class{RealVectors}, \class{RealMatrix},  \class{RealMatrices}] are aliases of previous real vectors and matrices
\item[\class{Complexes}, \class{ComplexVector}, \class{ComplexVectors}, \class{ComplexMatrix}, \class{ComplexMatrices}] are aliases of previous complex vectors and matrices
\item[\class{Point}] to deal with Point in 1D, 2D, 3D
\item[\class{Parameter}] structure to deal with named parameter of type Real, Complex, Integer, String
\item[\class{Parameters}] a list of parameters
\item[\class{Function}] generalized function handling a c++ function and a list of parameters
\item[\class{Kernel}] generalized kernel managing a \class{Function} (the kernel) and some additional data (singularity type, singularity order, \ldots)
\item[\class{TensorKernel}] a special form of kernel useful to DtN map 
\end{description}
\vspace{.2cm}

\xlifepp also provides the main user's modelling classes:

\begin{description}
\item[\class{Geometry}] to describe geometric objects (segment, rectangle, ellipse, ball, cylinder, \ldots). Each geometry has its own modelling class (\class{Segment}, \class{Rectangle}, \class{Ellipse}, \class{Ball}, \class{Cylinder}, \ldots)
\item[\class{Mesh}] mesh structure containing nodes, geometric elements, \ldots
\item[\class{Domain}] alias of geometric domains describing part of the mesh, in particular boundaries, and 
\class{Domains} to deal with vectors of \class{Domain}'s
\item[\class{Space}] class handles discrete spaces (FE space or spectral space) and \class{Spaces} some vectors of \class{Space}'s
\item[\class{Unknown}, \class{TestFunction}] abstract elements of space and \class{Unknowns}, \class{TestFunctions} to handle vector of \class{Unknown}'s and \class{TestFunction}'s
\item[\class{LinearForm}] symbolic representation of a linear form
\item[\class{BiLinearForm}] symbolic representation of a bilinear form
\item[\class{EssentialCondition}] symbolic representation of an essential condition on a geometric domain
\item[\class{EssentialConditions}] list of essential conditions
\item[\class{TermVector}] algebraic representation of a linear form or element of space as vector
\item[\class{TermVectors}] list of \class{TermVector}'s
\item[\class{TermMatrix}] algebraic representation of a bilinear form
\item[\class{EigenElements}] list of eigen pairs (eigen value, eigen vector)
\end{description} 
