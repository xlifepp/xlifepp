%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\section{\classtitle{Function}}

In order to deal with functions with parameters of any kind it is necessary to use an object function which is related to a \class{Parameters} object (a list of parameters, see \class{Parameters} documentation). This approach allows passing friendly, at low level of the code, some user's functions, say functions defined in the main program. 

\subsection{User function and object function}

When you want to deal with the integral term:

\[
\int_{\Omega}e^{ikx}\;u(x,y,z)\;v(x,y,z)\,d\Omega,
\]

where the loop of finite element computation requires the computation of the function \(f(x)=e^{ikx}\) on quadrature points, it is necessary to pass the function \(f\) to the low-level code where the finite element loop is implemented. Most functions are function of a point or a list of points. So, only four kinds of function are concerned:

\begin{itemize}
\item Function of an n-dimensional point (see \ref{s.point} for the class \class{Point});
\item function of two n-dimensional points, usually named kernel;
\item function of a vector of n-dimensional point;
\item function of two vectors of n-dimensional points.
\end{itemize}

The functions may also have a \class{Parameters} as input argument if necessary (default value);
for example, the real \(k\) in the previous example. The output argument may be of any type (real,
complex, vector, matrix, \ldots), but has to be compatible which the type required in computation.\\
The way to define such a function is the following. First, define a standard C++ function,
for instance:\\

\begin{lstlisting}[deletekeywords={[3]x}]
Complex f(const Point & P, Parameters& pa = default_Parameters)
{
Real k=pa(1);    // k is the first parameter of the parameter's list pa
// Real k=pa("k"); is available if your have a parameter with name "k"
Real x=P(1);     // x is the first coordinate of the point P
return exp(i*k*x); // return a complex value result, 
//return exp(i*pa(1)*P(1)); is also possible
}
\end{lstlisting}

Then create in your main program a \class{Parameters} object, a \class{Function} object from C++ function and you \class{Parameters} object:\\

\begin{lstlisting}[deletekeywords={[3]x}]
{
 Parameters pars(1,"k");
 Function F(f,pars);
}
\end{lstlisting}

If you have to deal with the integral term involving a real value matrix (with no parameter
involved in this example):

\[
\int_{\Omega}A\nabla u\,.\nabla v\,d\Omega,
\]

you may define:\\

\begin{lstlisting}
Matrix<Real> A(const Point & P, Parameters& pa = default_Parameters)
{
  Matrix<Real> vA(3,3);
  \ldots
  return vA;
}
\end{lstlisting}

\begin{warningbox}
Note that even if your function does not involve some parameters, the second argument of type Parameters is mandatory in the function definition.
\end{warningbox}

For a kernel type function, it is quite similar. You have to specify two points as input arguments:\\

\begin{lstlisting}
Complex Green_Helmholtz_3D(const Point& M,const Point& P, Parameters& pa = default_Parameters)
{
  Real r=distance(M,P);  // we assume a distance function exists
  Real k=pa(1);
  Real eps=pa(2);
  if(r>eps) return exp(i*k*r)/r;
  else \ldots
}
\end{lstlisting}

The vector form of a \class{Function} is a function working with a vector of points and returning a vector of results (values on each point). It may be useful when  computing n values together is really faster than computing n times a single value. Such a function should be declared, for instance, as follows:\\

\begin{lstlisting}
Vector<Complex> vf(const Vector<Point> & vP, Parameters& pa = default_Parameters)
{
  Real k=pa(1);    //k is the first parameter of the parameter's list pa
  uint n=vP.size();
  Vector<Complex> res(n);
  for(int j=1;j<=n;j++) res(j)=exp(i*k*vP(j)(1));
  return res;
}
\end{lstlisting}

Note that the function has to return a \class{Vector} object. For a function involving a couple
of vectors of points, the syntax of the declaration of the function is:

\begin{lstlisting}
Vector<Complex> vf(const Vector<Point> & vP,const Vector<Point> & vQ, Parameters& pa = default_Parameters)
\end{lstlisting}

The functions defined by the user may be used directly as argument of some functions of the
library. But in most cases it is necessary to define explicitly the object \class{Function}
associated to the user function. This is the way to do this:\\

\begin{lstlisting}
Parameters par;            
par<<2.<<.000001;             //k and eps values, inserted in a parameter list
Function funcf(f,par);        //define a scalar function object using parameters
Function funcA(A);            //define a matrix function object
Function funcG(Green_Hemholtz_3D, par); //define a scalar kernel
Function funcvf(vf,par);      //define a scalar function in its vector form
\end{lstlisting}

Do not confuse the vector form of a \class{Function} and a function which returns a vector!
The vector form means a function which computes a quantity (scalar, vector, matrix) on a set
of a points or bipoints, the result being a vector of scalars, vectors or matrices. For most
applications, scalar form of \class{Function} are sufficient. Vector form is an extension
allowing the user to compute the function more efficiently in the case of multiple evaluations.\\

When the user wants to associate some parameters to his function, it is mandatory to define
the object \class{Function} because it stores the list of parameters. To understand the role
of the object \class{Function}, note that if P is a \class{Point}, the two instructions\\

\begin{lstlisting}
r=f(P,par); // call directly the user function f
funcf(P,r); // call the user function f using the object function funcf
\end{lstlisting}

are allowed and give the same result. In other words, the object \class{Function} shadows
the \class{Parameters} object. In this example, using an object function seems to be artificial.
The object function has a real interest when internal computational routines requires user's
functions, because it is easier to send one type of object encapsulating various type of function
rather than many different objects.\\ 

When you have to pass an object \class{Function} to a function which requires such an object,
it is possible to use the constructor syntax Function(f, param), avoiding the explicit creation
of the object \class{Function}:\\

\begin{lstlisting}
Parameters par;            
par<<2.<<.000001;          // value of k and eps
compute(Function(f,par));  // compute requiring an object function
\end{lstlisting}

\begin{focusbox}
When using a function returning a vector or a matrix, because \xlifepp checks the dimension(s) of the returned object by using a fake point or a normal vector, it may occur some consistency problems with the dimension of points or normals that is set to 3 by default. You can change it by specifying explicitly the dimension of the function when building it:\\

\begin{lstlisting}
Function f(dnuinc,pars);
f.dimPoint=2;              //change the point dimension
//or
Function f(dnuinc,2,pars); // specifying the point dimension when building
\end{lstlisting}
\end{focusbox}

It works in the same way for kernels.

\subsection*{Dealing with normal vectors}

Sometimes, user functions have to deal with some normal vectors and obviously the normal vectors will depend on the point where the function is evaluated. For instance, a function computing the normal derivative of a given incident field (e.g. a plane wave \(e^{ikx}\)), will look like:\\

\begin{lstlisting}
Complex dnuinc(const Point& P,  Parameters& pa = default_Parameters)
{
 Real x=P(1), k=pa("k");    // get k from parameters
 Reals n=getN();           // get the normal vector at P
 return i_*k*exp(i_*k*x)*n(1);
}
\end{lstlisting}

When this function is passed to FE computation routines, the normal vector will be evaluated and transmitted to the function only if 
the \class{Function} object associated to the function, has declared to use the normal vector. It is done by the following instructions in the main program:\\

\begin{lstlisting}
Parameters pars(1,"k");      // declare k in the parameters
Function f(dnuinc,pars);     // associate parameters to Function object
f.require(_n);               // tell the function will use the normal
TermVector B(intg(Sigma,f*v));  // use function f 
\end{lstlisting}

The normal vector refers to the domain on which the linear or bilinear form (involving the function) acts. The orientation of  normal vector is described in section \ref{sec_normals}.\\

If you want to call a function involving a normal vector out of the context of FE computations, you have to "transmit" explicitly the normal vectors required by your function. The following code shows how to do this:\\

\begin{lstlisting}
real_t f(const Point& x, Parameters& pa = defaultParameters)
{ Vector<real_t>& n=getVector(_n); // get the normal vector
  return n(1);}
  \ldots
Function F(f); F.require(_n);       //say the function is using normal vector
Point P(1.,0.);                     //point belonging to gamma 
Vector<Real> n1=gamma.getNormal(P); //get the normal vector to gamma at P
setNx(n1);                          //pass the normal vector to the context
Real r = F(P,r);                    //compute F at P using n1
\end{lstlisting}

Note that the normal vector returned by the \var{getNormal} function, is not interpolated. As a consequence, normal vector on an edge of the mesh is the normal vector of one of the elements supporting the edge!\\

The \cmd{setNx} is thread dependant, so it can be used in a multithreading context.\\

\begin{advancedbox}
Other data are available in user function, depending on the computation context : the y-normal vector (\cmd{getNy}), the current element (\cmd{getElement}), the current Dof (\cmd{getDof}) or FEDof  (\cmd{getFEDof}), the domains (\cmd{getDomain}, \cmd{getDomainx}, \cmd{getDomainy}). All this data are returned as references.
\end{advancedbox}


\subsection*{Dealing with spectral family}
A spectral family is defined either from a \xlifepp function or from a collection of \class{TermVector}. When it is defined from a function, the function has to propose the computation for each basis index, say \(n\). This basis index is managed by the \xlifepp computation algorithms and transmitted either to the \class{Parameters}  attached to the function or to a global data structure (thread safe) . To retry it as a \class{Number}, users have to call either the function \cmd{Parameters::basisIndex() } or the global function  \cmd{getBasisIndex}:
\vspace{1mm}
\begin{lstlisting}
Real cosny(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Real h=pa("h");                    //get the parameter h (user definition)
  Number n=pa.basisIndex()-1;        //get the index using the Parameters
  Number n=getBasisIndex()-1;        //get the index using the global function
  if(n==0) return std::sqrt(1./h);
  else     return std::sqrt(2./h)*std::cos(n*pi_*(y+h*0.5)/h);
}
\end{lstlisting}
\vspace{1mm}
\begin{warningbox}
    Note that basis index set by internal algorithms, starts from 1!
\end{warningbox}
\vspace{1mm}
\begin{focusbox}
    Because it is safer, it is advised to use the global function \cmd{getBasisIndex} except if two functions are involved at the same time with different indexes (case not handled for the moment). Do not use the direct access to the parameter \cmd{pa("basis index")} which is not secured.
\end{focusbox}
\vspace{1mm}
The basis index is automatically set by internal algorithms in their computation loops. But, in user context, it may be set by calling either \cmd{pa.set("basis index", i)} or the global function \cmd{setBasisIndex(Number)} if you are using the \cmd{getBasisIndex} function.
\begin{lstlisting}
Function cos(cosny, params);
TermVectors cos_int(N);
for(Number n=1; n<=N; n++)
{
  setBasisIndex(n); // update the basis index               
  cos_int(n)=TermVector(u,sigmaP,cos,"c"+tostring(n-1));
}
\end{lstlisting}
\vspace{1mm}
\begin{toxicbox}
 Do not mix the \class{Parameters} way and the global way!
 \end{toxicbox}

\subsection*{Sum up}

\begin{itemize}
\item To define a function of one point returning a value of type T and its associated object \class{Function}:\\

\begin{lstlisting}
T namefunction(const Point& P,Parameters& pa=default_Parameters) 
Function nameofobjectfunction(namefunction,[param]);
\end{lstlisting}

\item to define a function of two points (a kernel) returning a value of type T and its associated object \emph{Function}:\\

\begin{lstlisting}
T namefunction(const Point& P,const Point& Q,Parameters& pa=default_Parameters) 
Function nameofobjectfunction(namefunction,[param]);
\end{lstlisting}

\item to define a vector function of one point returning a vector of value of type T and its associated object \class{Function}:\\

\begin{lstlisting}
Vector<T> namefunction(const Vector<Point>& P,Parameters& pa=default_Parameters) 
Function nameofobjectfunction(namefunction,[param]);
\end{lstlisting}

\item to define a vector function of two points (a vector kernel) returning a vector of value of type T and its associated object Function:\\

\begin{lstlisting}
Vector<T> namefunction(const Vector<Point> &,const Vector<Point> &, Parameters& pa=default_Parameters) 
Function nameofobjectfunction(namefunction,[param]);
\end{lstlisting}

\item to avoid the explicit construction of the object function (useful when you have to pass the function as an argument):\\

\begin{lstlisting}
Function(namefunction,[param]);
\end{lstlisting}

\item to declare that the object function uses the normal vector:\\

\begin{lstlisting}
Function nameofobjectfunction(namefunction,[param]);
nameofobjectfunction.require(_n);
\end{lstlisting}
\end{itemize}

\begin{focusbox}
For the people who used \melina Fortran, this approach replaces the famous \emph{fctrm.f} and
the \emph{tbasso} vector machinery.
\end{focusbox}

\subsection{Advanced user}

\subsubsection*{Delaying computations}

It may occur that the function you plan to use is a very complex one, involving some heavy computations that you want to compute only once by storing some intermediate results somewhere.
In order to allow flexibility to the user, it is advised to use the capabilities of \class{Parameters} object to store void pointers. For instance, the first time the function is called, you can compute some reusable quantities and store them in any structure with dynamic memory allocation and store the pointer of this structure in the \class{Parameters} object.
The next time the function is called, as you have access to this void pointer (do not forget to recast it), you can recover your data.

\subsubsection*{Calling a \classtitle{Function} object}

If you have to compute the values of the function via the object \class{Function}, there are
mainly two ways to do it:
\begin{itemize}
\item using an alias to the pointer function (requires that you know the type of function and
arguments)
\begin{lstlisting}
Point P=Point(0,0), Q=Point(1,1);
Complex c=funcf.funSC(P);     //a function returning a complex scalar (funSC)
Complex g=funcG.kerSC(P,Q);   //a kernel returning a complex scalar (kerSC)
Matrix<Real> m=funcA.funMR(P);//a function returning a real matrix scalar(funMR)
\end{lstlisting}
As this method uses a recasting of a void pointer with no checking, it can cause segmentation
errors if there is a misfit between the type of function required and the real function stored
in the void pointer! It is possible to check the type of arguments by using the utility functions
\cmd{typeReturned}, \cmd{structReturned}, \cmd{typeFunction} and \cmd{typeArg}. 
This direct method is offered to developers in order to have the best performance.

\item Using the safe overloaded operator (), allowing to deal with point or vector of points

\begin{lstlisting}
Point P=Point(0,0), Q=Point(1,1);
Complex c;           // complex to store the result
funcf.checkTypeOn(); // activate checking mode
funcf(P,c);          // compute a complex scalar function at point P
                     // checking mode is disabled after the computation
Vector<Point> pts;   // a vector of points
pts(1)=P;pts(2)=Q;
vector<Complex> vc;  // vector to store the result
funcf.checkTypeOn(); // activate the checking mode
funcf(Pts,vc);       // compute function at a vector of points

\end{lstlisting}
This method does not require the knowledge of the exact type of the function (the output argument
must be compatible !). It allows scalar or vector form independently of the form of the user
function. Note that, contrary to the first method, this method uses a reference to return the
values, so you have to manage its memory allocation. When the function is called with a vector
of points as input, the vector result is resized if it is too small. Using the \cmd{checkTypeOn}
function, it is possible to activate the checking of the type of argument. After computation,
the \var{checkType} variable is reset to \emph{false} in order to avoid unnecessary rechecking.
As the checking process invokes RTTI functions (expensive in time), activate wisely this option.
So, if you have to evaluate many times the function, activate the checking only for the first
evaluation. Note that when the checking process is deactivated, this method is still slightly
more expensive than the first one.   
\end{itemize}
