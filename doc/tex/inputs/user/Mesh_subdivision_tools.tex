%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
When the {\bfseries subdivision algorithm} is chosen (mg = {\tt\_subdiv}), one can create a mesh of any order based on
the following volumetric geometries:

\begin{itemize}
\item the sphere meshed by tetrahedra,
\item the cube meshed by tetrahedra or hexahedra,
\item the cone or truncated cone, which may be a cylinder, meshed by tetrahedra or hexahedra.
\end{itemize}

The following surface geometries are also handled:

\begin{itemize}
\item the boundary of the sphere meshed by triangles,
\item the boundary of the cube meshed by quadrangles,
\item the boundary of the cone or truncated cone meshed by triangles or quadrangles,
\item the disk meshed by triangles or quadrangles,
\item mesh built from an initial set of triangles or quadrangles in 2D or 3D.
\end{itemize}

The principle is to start from an initial mesh, a kind of ``seed" mesh, consisting of a set of elements, and build the mesh by subdividing each of them by cutting each edge in the middle until a prescribed so-called ``subdivision level" is reached. A subdivision level equal to 0 gives a mesh reduced to the initial mesh. A triangle and a quadrangle is subdivided into 4 pieces ; a tetrahedron and a hexahedron is subdivided into 8 pieces. Thus, at each subdivision, the number of elements of the mesh is multiplied by 4 for a surface mesh, by 8 for a volumetric one, and the characteristic dimension of the elements is halved.

One has to declare an object of type \class{Geometry}, more precisely of type \class{Sphere}, \class{Ball},\class{Cube}, \class{RevTrunk}, \class{RevCone}, \class{RevCylinder}, \class{Disk} or \class{SetOfElems} using one of the available constructors, e.g.:\\

\begin{lstlisting}[deletekeywords={[3]bounds}]
SetOfElems(const std::vector<Point>& pts, const std::vector<std::vector<number_t> >& elems, const std::vector<std::vector<number_t> >& bounds, const ShapeType esh, const number_t nbsubdiv=1);
\end{lstlisting}

\subsubsection{Mesh of a ball (or sphere) with tetrahedra}

The seed of the mesh consists of a unique tetrahedron inside each octant of the Cartesian axes.
We can choose the number of octants to be taken into account, from 1 to 8, to mesh different portions of the sphere.
The figure \ref{volSphere_meshes} shows this in the case of  a subdivision level equal to 2. Each figure corresponds to
the result of the following code, using the unit sphere and \(nboctants\) varying from 1 to 8:\\

\begin{lstlisting}[deletekeywords={[3]order}]
order = 1, nbpts=5, meshType = 1;
Ball sph(_center=Point(0.,0.,0.), _radius=1., _nboctants=nboctants, _nnodes=nbpts, _type=meshType);
Mesh m(sph, _tetrahedron, order, _subdiv);
\end{lstlisting}

\begin{focusbox}
The class \class{Sphere} could have been used instead of \class{Ball}, leading to the same result.
\end{focusbox}

Each color corresponds to a different boundary domain.
The default value of the argument meshType is 1; setting it to 0 leads to a so-called flat mesh, where the points created during the algorithm are not projected onto the sphere, thus keeping the shape of the initial mesh. We can see the effect of this choice on figure \ref{sphereTetFlat_meshes}.

\begin{figure}[H]
\begin{center}
\includePict[scale=0.9]{P1VolMeshTetSphere1.pdf}
\includePict[scale=0.9]{P1VolMeshTetSphere2.pdf}
\includePict[scale=0.9]{P1VolMeshTetSphere3.pdf}
\includePict[scale=0.9]{P1VolMeshTetSphere4.pdf}
\end{center}
\begin{center}
\includePict[scale=0.9]{P1VolMeshTetSphere5.pdf}
\includePict[scale=0.9]{P1VolMeshTetSphere6.pdf}
\includePict[scale=0.9]{P1VolMeshTetSphere7.pdf}
\includePict[scale=0.9]{P1VolMeshTetSphere8.pdf}
\end{center}
\caption{Volumic meshes of the different portions of the ball according to the number of octants.}
\label{volSphere_meshes}
\end{figure}

\begin{figure}[H]
\begin{center}
\includePict{P1VolMeshTetSphereFlat1.pdf}
\includePict{P1VolMeshTetSphereFlat2.pdf}
\includePict{P1VolMeshTetSphereFlat3.pdf}
\includePict{P1VolMeshTetSphereFlat4.pdf}
\end{center}
\begin{center}
\includePict{P1VolMeshTetSphereFlat5.pdf}
\includePict{P1VolMeshTetSphereFlat6.pdf}
\includePict{P1VolMeshTetSphereFlat7.pdf}
\includePict{P1VolMeshTetSphereFlat8.pdf}
\end{center}
\caption{Volumic meshes of the different portions of the ``flat ball" according to the number of octants.}
\label{sphereTetFlat_meshes}
\end{figure}

If we specifically want the mesh inside the whole sphere, it can be usefull to start from an icosahedron because of its geometric properties, which lead to a more isotropic mesh than the one based on the 8 octants of the space. On the other hand, there will be no interface planes defined.

In order to activate this option, the argument nboctants should simply be set to 0. The following code gives the figure \ref{sphereTetIcosa_meshes}, which shows both the round and flat results of the algorithm:\\

\begin{lstlisting}[deletekeywords={[3]order}]
nboctants = 0, nbpts=5; meshType = 1;
Ball sph(_center=Point(0.,0.,0.), _radius=1., _nboctants=nboctants, _nnodes=nbpts, _type=meshType);
order = 1;
Mesh m(sph, _tetrahedron, order, _subdiv);
    
meshType = 0;
Ball sph2(_center=Point(0.,0.,0.), _radius=1., _nboctants=nboctants, _nnodes=nbpts, _type=meshType);
Mesh m2(sph2, _tetrahedron, order, _subdiv);
\end{lstlisting}

\begin{figure}[H]
\centering
\includePict{P1VolMeshTetSphere0.pdf}\hskip2cm
\includePict{P1VolMeshTetSphereFlat0.pdf}
\caption{Volumic meshes starting from an icosahedron.}
\label{sphereTetIcosa_meshes}
\end{figure}

\subsubsection{Mesh of a cube with tetrahedra or hexahedra}

The selection of the octants is also used in the case of the cube as shown on the figure \ref{vol_cube_meshes},
which is the result of the following code, with no subdivision and \(nboctants\) varying from 1 to 8:\\

\begin{lstlisting}[deletekeywords={[3]order,cube}]
order = 1, nbpts=2;
Cube cube(_center=Point(0.,0.,0.), _length=2., _nboctants=nboctants, _nnodes=nbpts);
Mesh m(cube, _hexahedron, order, _subdiv);
\end{lstlisting}

Notice that the edge length of the total cube is 2, so that the cube in the first octant is the so-called unit cube. Apart the choice of the mesh element (tetrahedron or hexahedron), the main interest of this case is the easy creation of a L-shape domain (3 octants) and the Fichera corner (7 octants), classical benchmark problem in the analysis of corner and edge singularities. It is shown on figure \ref{vol_cube_meshes} in an unsual position; in order to put the missing cube in the first octant, one must apply a rotation, which is done by the following code:\\

\begin{lstlisting}[deletekeywords={[3]order,cube}]
order = 1, nboctants=7, nbpts=2;
Cube cube(_center=Point(0.,0.,0.), _length=2., _nboctants=nboctants, _nnodes=nbpts); cube.rotate3d(Point(0.,0.,0.), 1.,0.,0., pi);
Mesh m(cube, _hexahedron, order, _subdiv);
\end{lstlisting}

The two additional arguments define the rotation of angle 180 degrees around the first axis (X-axis); the result is shown on figure \ref{vol_cube_meshes}, at the last position (bottom right). If necessary, one can specify one or two more rotations in the form (angle, naxis). The angle is to be given in degrees and naxis defines the rotation axis: it is the number of the absolute axis, thus 1, 2 or 3.

\begin{figure}[H]
\begin{center}
\includePict[scale=0.8]{P1VolMeshHexCube1.pdf}
\includePict[scale=0.8]{P1VolMeshHexCube2.pdf}
\includePict[scale=0.8]{P1VolMeshHexCube3.pdf}
\includePict[scale=0.8]{P1VolMeshHexCube4.pdf}
\end{center}
\begin{center}
\includePict[scale=0.8]{P1VolMeshHexCube5.pdf}
\includePict[scale=0.8]{P1VolMeshHexCube6.pdf}
\includePict[scale=0.8]{P1VolMeshHexCube7.pdf}
\includePict[scale=0.8]{P1VolMeshHexCube8.pdf}\hskip2cm
\includePict[scale=0.8]{P1VolMeshHexCubeFich.pdf}
\end{center}
\caption{Volumic meshes of the different portions of the cube according to the number of octants.}
\label{vol_cube_meshes}
\end{figure}

\subsubsection{Mesh of a cylinder with tetrahedra or hexahedra}

The subdivision algorithm can handle the case of a cylinder of revolution, whose axis is defined by two points P1 and P2, and delimited by the two planes containing the two points and orthogonal to the axis.
As an example, we consider the ``unit" cylinder of radius 1 and height 1. The following code produces the first two meshes shown on figure \ref{cyl_meshes}:\\

\begin{lstlisting}[deletekeywords={[3]order}]
radius=1.;
nbpts=3;
Point P1(0.,0.,0.), P2(0.,0.,1.);
RevCylinder cyl1(_center1=P1, _center2=P2, _radius=radius, _nnodes=nbpts);
order=1;
Mesh mT(cyl, _tetrahedron, order, _subdiv);
Mesh mH(cyl, _hexahedron,  order, _subdiv);
\end{lstlisting}

\begin{figure}[H]
\begin{center}
\includePict{P1VolMeshTetCyl1.pdf}\hskip1cm
\includePict{P1VolMeshHexCyl1.pdf}\hskip2cm
\includePict{P1VolMeshTetCylE1.pdf}
\end{center}
\caption{Volumic meshes of the ``unit" cylinder with tetrahedra and hexahedra.}
\label{cyl_meshes}
\end{figure}

Obviously, this is a poor approximation of the cylinder. To get a more accurate description, the user can then increase the number of elements (greater value of nbsubdiv) or increase the approximation order (or both).\\

In the case of a tetrahedron mesh, each end-plane may be covered by a ``hat", that is to say a solid whose shape may be a cone or an ellipsoid. The last drawing of figure \ref{cyl_meshes} shows such a configuration, with an ellipsoid on the side of P1 (keyword \_gesEllipsoid) whose apex is at radius/2 from the basis of the cylinder, and a cone on the side of P2 (keyword \_gesCone) whose apex is at radius from the other basis of the cylinder. It is obtained by the following code:

\begin{lstlisting}[deletekeywords={[3]order}]
radius=1.;
nbpts=5;
RevCylinder cyl2e(_center1=Point(0.,0.,0.), _center2=Point(0.,0.,1.), _radius=radius, _end1_shape=_gesEllipsoid, _end1_distance=radius/2, _end2_shape=_gesCone, _end2_distance=radius, _nnodes=nbpts);
order=1;
Mesh P1VolMeshTetCylE(cyl2e, _tetrahedron, order, _subdiv);
\end{lstlisting}

Two other keywords exist: \_gesNone and \_gesFlat. They have an equivalent meaning in the case of a solid body.
They are the default value and indicate that no ``hat" should be added at the corresponding end.

\subsubsection{Mesh of a cone or a truncated cone with tetrahedra or hexahedra}

A truncated cone of revolution is defined by an axis, given by two points P1 and P2, delimited by the two planes containing the two points and orthogonal to the axis. The two circular sections are defined by two radii. The following code produces the first two meshes shown on figure \ref{vol_cone_meshes}:\\

\begin{lstlisting}[deletekeywords={[3]order}]
nbpts=5;
radius1=0., radius2=1.;
Point P1(-1.,-1.,0.), P2(0.,0.,2.);
RevCone cone(_center=P2, _radius=radius2, _apex=P1, _nnodes=nbpts);
order=1;
Mesh mT(cone, _tetrahedron, order, _subdiv);

radius1=0.5;
RevTrunk cone2(_center1=P1, _radius1=radius1, _center2=P2, _radius2=radius2, _nnodes=nbpts);
Mesh mH(cone2, _hexahedron, order, _subdiv);
\end{lstlisting}

The number of slices is 0, which means that a suitable default value is automatically computed from the length of the axis and the radii. The first object is a "true" cone since one radius is 0 ; it can be meshed exactly with tetrahedra. Using hexahedra for this geometry is not advised since the elements will be degenerated at the apex of the cone. Moreover, the radius cannot be 0, it should be at least 1.e-15, leading to a "near true" cone, but with highly degenerated hexahedra close to the apex. Hexahedra are more suitable to build a trucated cone ; an example is shown on the middle drawing of the figure \ref{vol_cone_meshes}.

\begin{figure}[H]
\begin{center}
\includePict[scale=0.8]{P1VolMeshTetCone0.pdf}\hskip1cm
\includePict[scale=0.8]{P1VolMeshHexCone0.pdf}\hskip2cm
\includePict[scale=0.8]{P1VolMeshTetConeE0.pdf}
\end{center}
\caption{Volumic meshes of the cone and truncated cone with tetrahedra and hexahedra.}
\label{vol_cone_meshes}
\end{figure}

The following code produces the last mesh shown on figure \ref{vol_cone_meshes}:\\

\begin{lstlisting}[deletekeywords={[3]order}]
nbpts=5;
radius1=0.6, radius2=1.;
RevTrunk cone1(_center1=Point(-1.,-1.,0.), _radius1=radius1, _center2=Point(0.,0.,2.), _radius2=radius2, _end1_shape=_gesCone, _end1_distance=1.5, _end2_Shape=_gesEllipsoid, _end2_distance=0.7, _nnodes=nbpts);
order=1;
Mesh mTE(cone1, _tetrahedron, order, _subdiv);
\end{lstlisting}

In the same way as for the cylinder, the truncated cone can be ``covered" with a solid. This is only available for a mesh made of tetrahedra. We show a cone and an ellipsoid put at each end of a truncated cone, respectively on the side of P1 and on the side of P2.

\subsubsection{Mesh of a sphere with triangles}

The same logic described previously for a mesh of tetrahedra apply here for a mesh of triangles. The following code leads to meshes of the boundary of the unit sphere, and the result is shown on figure \ref{surfSphere_meshes}:\\

\begin{lstlisting}[deletekeywords={[3]order}]
    order = 1, nbpts=5;
    Ball sph(_center=Point(0.,0.,0.), _radius=1., _nboctants=nboctants, _nnodes=nbpts);
    Mesh m(sph, _triangle, order, _subdiv);
\end{lstlisting}

\begin{figure}[H]
\begin{center}
\includePict[scale=0.9]{P1SurfMeshTriSphere1.pdf}\hskip1cm
\includePict[scale=0.9]{P1SurfMeshTriSphere2.pdf}\hskip1cm
\includePict[scale=0.9]{P1SurfMeshTriSphere3.pdf}\hskip1cm
\includePict[scale=0.9]{P1SurfMeshTriSphere4.pdf}
\end{center}
\begin{center}
\includePict[scale=0.9]{P1SurfMeshTriSphere5.pdf}
\includePict[scale=0.9]{P1SurfMeshTriSphere6.pdf}
\includePict[scale=0.9]{P1SurfMeshTriSphere7.pdf}
\includePict[scale=0.9]{P1SurfMeshTriSphere8.pdf}
\end{center}
\caption{Surfacic meshes of the different portions of the boundary of the sphere according to the number of octants.}
\label{surfSphere_meshes}
\end{figure}

Again, if the argument meshType is set to 0, we get the ``flat" version of the meshes, i.e. the
meshes obtained from the subdivision of the nboctants initial triangles (see figure \ref{sphereTriFlat_meshes}).

\begin{figure}[H]
\begin{center}
\includePict{P1SurfMeshTriSphereFlat1.pdf}
\includePict{P1SurfMeshTriSphereFlat2.pdf}
\includePict{P1SurfMeshTriSphereFlat3.pdf}
\includePict{P1SurfMeshTriSphereFlat4.pdf}
\end{center}
\begin{center}
\includePict{P1SurfMeshTriSphereFlat5.pdf}
\includePict{P1SurfMeshTriSphereFlat6.pdf}
\includePict{P1SurfMeshTriSphereFlat7.pdf}
\includePict{P1SurfMeshTriSphereFlat8.pdf}
\end{center}
\caption{Surfacic meshes of the different portions of the ``flat sphere" according to the number of octants.}
\label{sphereTriFlat_meshes}
\end{figure}

If we specifically want the mesh of the whole sphere, it can be usefull to start from an icosahedron because of its geometric properties, which lead to a more isotropic mesh than the one based on the 8 octants of the space. On the other hand, there will be no interface planes defined.

In order to activate this option, the argument nboctants should simply be set to 0. The following code gives the figure \ref{sphereTriIcosa_meshes}, which shows both the round and flat results of the algorithm:\\

\begin{lstlisting}[deletekeywords={[3]order}]
nboctants = 0, nbpts=5; meshType = 1;
Ball sph(_center=Point(0.,0.,0.), _radius=1., _nboctants=nboctants, _nnodes=nbpts, _type=meshType);
order = 1;
Mesh m(sph, _triangle, order, _subdiv);
    
meshType = 0;
Ball sph2(_center=Point(0.,0.,0.), _radius=1., _nboctants=nboctants, _nnodes=nbpts, _type=meshType);
Mesh m2(sph2, _triangle, order, _subdiv);
\end{lstlisting}

\begin{figure}[H]
\begin{center}
\includePict{P1SurfMeshTriSphere0.pdf}\hskip2cm
\includePict{P1SurfMeshTriSphereFlat0.pdf}
\end{center}
\caption{Surfacic meshes starting from an icosahedron.}
\label{sphereTriIcosa_meshes}
\end{figure}

\subsubsection{Mesh of a cube with quadrangles}

We can obtain the mesh of the surface of a cube, or part of it, with quadrangles, by using the same logic described just above for the sphere. Consider the following code:\\

\begin{lstlisting}[deletekeywords={[3]order,cube}]
    order = 1, nbpts=2;
    Cube cube(_center=Point(0.,0.,0.), _length=2., _nboctants=nboctants, _nnodes=nbpts);
    Mesh m(cube, _quadrangle, order, _subdiv);
\end{lstlisting}

Letting nboctants vary from 1 to 8, then 0, lead to the objects shown on figure \ref{surf_cube_meshes}
below.

\begin{figure}[H]
\begin{center}
\includePict[scale=0.7]{P1SurfMeshQuaCube1.pdf}
\includePict[scale=0.7]{P1SurfMeshQuaCube2.pdf}
\includePict[scale=0.7]{P1SurfMeshQuaCube3.pdf}
\includePict[scale=0.7]{P1SurfMeshQuaCube4.pdf}
\end{center}
\begin{center}
\includePict[scale=0.7]{P1SurfMeshQuaCube5.pdf}
\includePict[scale=0.7]{P1SurfMeshQuaCube6.pdf}
\includePict[scale=0.7]{P1SurfMeshQuaCube7.pdf}
\includePict[scale=0.7]{P1SurfMeshQuaCube8.pdf}\hskip2cm
\includePict[scale=0.7]{P1SurfMeshQuaCube0.pdf}
\end{center}
\caption{Surfacic meshes of the different portions of the cube according to the number of octants.}
\label{surf_cube_meshes}
\end{figure}

The last object (nboctants = 0) is the simplest mesh of a cube made of 6 quadrangles (squares here). By subdividing it once, we get the previous yellow object (nboctants = 8) made of 24 quadrangles.

\subsubsection{Mesh of a cone or a truncated cone with triangles}

We can build a mesh of the surface of a truncated cone with triangles. The following code produces
the first two drawings of the figure \ref{tri_cyl_meshes}:\\

\begin{lstlisting}[deletekeywords={[3]order}]
radius=1.;
nbslices=1, nbpts=3;
Point P1(0.,0.,0.), P2(0.,0.,1.);
RevCylinder cyl1(_center1=P1, _center2=P2, _radius=radius, _nnodes=nbpts);
order=1;
Mesh mT(cyl, _triangle, order, _subdiv, fname);

nbpts=5;
RevCylinder cylE(_center1=P1, _center2=P2, _radius=radius, _end1_shape=_gesFlat, _end1_distance=0., _end2_shape=_gesNone, _end2_distance=0., _nnodes=nbpts);
Mesh mTE(cylE, _triangle, order, _subdiv);
\end{lstlisting}

\begin{figure}[H]
\begin{center}
\includePict{P1SurfMeshTriCyl1.pdf}\hskip2cm
\includePict{P1SurfMeshTriCylE1.pdf}
\end{center}
\caption{Surfacic meshes of a cylinder.}
\label{tri_cyl_meshes}
\end{figure}

\begin{lstlisting}[deletekeywords={[3]order}]
nbpts=5;
radius1=0.5, radius2=1.;
Point P1(-1.,-1.,0.), P2(0.,0.,2.);
RevTrunk cone3(_center1=P1, _radius1=radius1, _center2=P2, _radius2=radius2, _end1_shape=_gesNone, _end1_distance=0., _end2_shape=_gesFlat, _end2_distance=0., _nnodes=nbpts);
order=1;
Mesh mT(cone3, _triangle, order, _subdiv);

RevTrunk cone1(_center1=P1, _radius1=radius1, _center2=P2, _radius2=radius2, _end1_shape=_gesCone, _end1_distance=1.5, _end2_shape=_gesEllipsoid, _end2_distance=0.7, _nnodes=nbpts);
Mesh mTE(cone1, _triangle, order, _subdiv);
\end{lstlisting}

\begin{figure}[H]
\begin{center}
\includePict[scale=0.8]{P1SurfMeshTriCone0.pdf}\hskip2cm
\includePict[scale=0.8]{P1SurfMeshTriConeE0.pdf}
\end{center}
\caption{Surfacic meshes of a truncated cone.}
\label{tri_cone_meshes}
\end{figure}

\subsubsection{Mesh of a cone or a truncated cone with quadrangles}

We can build a mesh of the surface of a truncated cone with quadrangles. The following code produces the first two drawings of the figure \ref{qua_cone_meshes}:\\

\begin{lstlisting}[deletekeywords={[3]order}]
nbpts=5;
radius1=0.5, radius2=1.;
Point P1(-1.,-1.,0.), P2(0.,0.,2.);
RevTrunk cone2(_center1=P1, _radius1=radius1, _center2=P2, _radius2=radius2, _nnodes=nbpts);
order=1;
Mesh mQ(cone2, _quadrangle, order, _subdiv);

RevTrunk cone3(_center1=P1, _radius1=radius1, _center2=P2, _radius2=radius2, _end1_shape=_gesNone, _end1_distance=0., _end2_shape=_gesFlat, _end2_distance=0., _nnodes=nbpts);
Mesh mQF(cone3, _quadrangle, order, _subdiv, "mQF");
mQF.printInfo();
\end{lstlisting}

The left truncated cone is opened at both ends (this is the default) ; thus it has two boundaries
shown in green (bottom) and orange (top). The object cone2 is the same as the one used previously
to make the mesh of hexahedra (see figure \ref{vol_cone_meshes}).

The second drawing shows the same object bearing a "lid" on its top (on the side of P2). Indeed, such a truncated cone may be closed at one or both ends by a plane "lid". This is obtained by specifying the geometric end shape to be used at each end: \_gesNone means that the cone is left opened, which is the default behavior, and \_gesFlat means that a plane "lid" is requested.
The objet proposed in this example has thus one boundary at its other end (on the side of P1),
shown as an orange line.

In both cases, the requested number of slices is 0 ; thus, the algorithm decided to create two slices displayed in magenta and yellow. The result of the instruction \lstinline{mQF.printInfo();} is the following:

\begin{lstlisting}[language=]
Mesh'mQF' (cone - Quadrangle mesh)
  space dimension : 3, element dimension : 2
  Geometry   of shape type revolution volume based on cone of dimension 3,
  BoundingBox [-1.45412,0.908248]x[-1.45412,0.908248]x[-0.288675,2.57735], names of variable : x, y, z
  number of elements : 208, number of vertices : 217, number of nodes : 217, number of domains : 5
    domain number 0: Omega (whole domain)
    domain number 1: Sigma_1 (End subdomain on the side of end point 2)
    domain number 2: Sigma_2 (Slice 1)
    domain number 3: Sigma_3 (Slice 2)
    domain number 4: Kappa_1 (Boundary: End curve on the side of end point 1)
\end{lstlisting}

\begin{figure}[H]
\begin{center}
\includePict[scale=0.8]{P1SurfMeshQuaCone0.pdf}
\includePict[scale=0.8]{P1SurfMeshQuaConeE0.pdf}\hskip1cm
\includePict{P1SurfMeshQuaCyl1.pdf}
\end{center}
\caption{Surfacic meshes of a cone and a cylinder.}
\label{qua_cone_meshes}
\end{figure}

The last drawing shows the surfacic mesh of a cylinder, since it is a particular kind of cone.
The cylinder is the same as the one shown on figure \ref{cyl_meshes}. The code that produces
this mesh is:

\begin{lstlisting}[deletekeywords={[3]order}]
radius=1.;
nbpts=3;
Point P1(0.,0.,0.), P2(0.,0.,1.);
RevCylinder cyl(_center1=P1, _center2=P2, _radius=radius, _nnodes=nbpts);
order=1;
Mesh mQCyl(cyl, _quadrangle, order, _subdiv);
\end{lstlisting}

The cylinder is opened at both ends and thus has two boundaries shown as a green line and
an orange line.

\subsubsection{Mesh of a disk or a part of a disk}

We can build the mesh of a disk or a portion of a disk, with triangles or quadrangles.
Using the following code, we get the result shown on figure \ref{disk_meshes}.

\begin{lstlisting}[deletekeywords={[3]order}]
radius=2.;
nbpts=5, order=1;
Disk pdisk(_center=Point(0.,1.), _radius=radius, _angle1=10., _angle2=300., _nnodes=nbpts);
Mesh meshTriDisk(pdisk, _triangle, order, _subdiv);
Mesh meshQuaDisk(pdisk, _quadrangle, order, _subdiv);
\end{lstlisting}

\begin{figure}[H]
\begin{center}
\includePict[scale=0.5]{P1SurfMeshTriDisk.pdf}
\includePict[scale=0.5]{P1SurfMeshQuaDisk.pdf}
\end{center}
\caption{Meshes of a portion of disk with triangles and quadrangles.}
\label{disk_meshes}
\end{figure}

\subsubsection{Mesh from a set of triangles or quadrangles}

This possibility is designed to build a mesh starting from an elementary set of elements. Generally, this initial mesh is build ``manually". This gives a flexible mean to create a mesh which cannot be obtained with another constructor, but without having to resort to the help of a more complicated solution (like an external mesh generator in particular).

Such meshes can be made in 2D or in 3D with triangles or quadrangles. Figure \ref{setofel_meshes} shows two examples, in 3D with triangles, in 2D with quadrangles on a domain with a hole.

\begin{figure}[H]
\begin{center}
\includePict[scale=0.7]{P1SurfMeshTriSet.pdf}\hskip3cm
\includePict[scale=0.7]{P1SurfMeshQuaSet.pdf}
\end{center}
\caption{Meshes from initial set of triangles and quadrangles.}
\label{setofel_meshes}
\end{figure}

The program that produces it looks like the following:

\medskip

\begin{lstlisting}[deletekeywords={[3]order}]
order = 1, nbsubdiv = 1;
SetOfElems sot(tpts, telems, tbounds, _triangle, nbsubdiv);
Mesh mT(sot, _triangle, order, _subdiv);
SetOfElems soq(qpts, qelems, qbounds, _quadrangle, nbsubdiv);
Mesh mQ(soq, _quadrangle, order, _subdiv);
\end{lstlisting}

The mesh of triangle is based on an initial set of 2 triangles \{1,2,3\} and \{1,4,2\}, stored in the vector elems. The 4 points are Point(0.,0.,0.), Point(1.,0.,0.), Point(0.,1.,0.3), Point(0.,-1.,0.3) stored in the vector tpts. Four boundaries are defined. A boundary is simply defined by the list of point numbers lying on it, in any order. Thus, here, the four boundaries are \{1,4\}, \{4,2\}, \{2,3\} and \{1,3\}; they are stored in the vector tbounds. The same apply for the set of quadrangles.
