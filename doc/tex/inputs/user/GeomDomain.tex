%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
Related to mesh, the geometric domains are fundamental objects because they are the support of integrals or boundary conditions involved in variational problem. These domains are defined by mesh tools, using names and sidenames in definition of geometries or given as physical domain in 'geo' file.

\subsection{Retrieving domains}

In order to be used in program, the domains have to be 'retrieved' as  \class{Domain} object from mesh:\\

\begin{lstlisting}
Strings sn("y=0", "y=1", "x=0", "x=1");
Mesh mesh2d(SquareGeo(_origin=Point(0.,0.), _length=1, _nnodes=20, _side_names=sn),_triangle,1,_structured);
Domain omega=mesh2d.domain("Omega");
Domain sigmaM=mesh2d.domain("x=0");
Domain sigmaP=mesh2d.domain("x=1");
Domain gammaM=mesh2d.domain("y=0");
Domain gammaP=mesh2d.domain("y=1");
\end{lstlisting}

By default, \var{"Omega"} is the string name of the main domain of mesh. \\

It is possible to rename a domain of a mesh:

\begin{lstlisting}
Strings sn("", "", "x=0", "x=1/2-");
Mesh mesh2d(Rectangle(_origin=Point(0.,0.), _xlength=0.5, _ylength=1, _nnodes=Numbers(20,40), _side_names=sn),_triangle,1,_structured);
mesh2d.renameDomain("Omega","Omega-");
sn[2] = "x=1/2+"; sn[3] = "x=1";
Mesh mesh2d_p(Rectangle(_origin=Point(0.5,0.), _xlength=0.5, _ylength=1, _nnodes=Numbers(20,40), _side_names=sn),_triangle,1,_structured);
mesh2d_p.renameDomain("Omega","Omega+");
mesh2d.merge(mesh2d_p);
Domain omegaM=mesh2d.domain("Omega-");
Domain omegaP=mesh2d.domain("Omega+");
Domain sigmaM=mesh2d.domain("x=0");
Domain sigmaP=mesh2d.domain("x=1");
Domain gamma=mesh2d.domain("x=1/2- or x=1/2+");
\end{lstlisting}

In this example, the unit square is split in two domains \(\Omega^+\) and \(\Omega^-\) using the 
\cmd{merge} and \cmd{renameDomain} functions. Note that the merging process of meshes concatenates 'same' domain in a new one named "name1 or name2".

\subsection{Dealing with normals of a domain}\label{sec_normals}

Normal vectors may be required in many variational forms, in particular when using BEM like methods. In (bi)linear forms, they appear with symbolic names \verb|_n, _nx ,_ny| that corresponds to real normal vectors. The question is which normal vectors are selected by \xlifepp.\\

Note that  the normal vectors of a domain, say \(\Gamma\), are computed only if the domain is a manifold, say a surface/curve domain in a 3d/2d space. If they are required, they are automatically computed with the following default rules:
   
\begin{itemize}
\item if \(\Sigma\) is a boundary (or a part of) of a unique domain \(\Omega\), the selected normals are the outwards vectors to \(\Omega\)
\item if \(\Sigma\) is a boundary between two domains (an interface), the selected normals are the towards infinite vectors
\item if \(\Sigma\) is not a boundary, say an immersed manifold, the selected normals are the towards infinite vectors
\end{itemize}

\begin{figure}[H]
\centering
\includePict[trim=0cm 4.5cm 0cm 4cm, clip=true, width=15cm]{normals.pdf}
\end{figure}

\begin{focusbox}
When the boundary or the manifold is not closed, the normal orientations are consistent, but the selected orientation is not predictable. 
\end{focusbox}

The user can modify the normal orientation by using the member function of \class{Domain} class:

\cmd{setNormalOrientation(OrientationType[, Domain])} where \cmd{OrientationType} has one of the following values (default is \var{\_undefOrientationType}):

\begin{lstlisting}
_towardsInfinite, _outwardsInfinite   // for any side domain
_towardsDomain, _outwardsDomain       // for any boundary or interface
\end{lstlisting}

To change the normal orientation of a side domain \verb|Sigma|, write for instance

\begin{lstlisting}
Sigma.setNormalOrientation(_towardsInfinite); //towards infinite normals
Sigma.setNormalOrientation(_outwardsDomain);  //outwards normals, 
                                              //UNSAFE for an interface!
Sigma.setNormalOrientation(_outwardsDomain,Omega);//outwards normals to Omega
\end{lstlisting}

All normal vectors of an open manifold are oriented consistently, but the global orientation is unpredictable because it depends on the first node encountered. Check the orientation by visualizing the normal vectors (see below). The global orientation may be controlled by using a virtual "interior" point that orients the normal vector related to this interior point:\\

\begin{lstlisting}
Sigma.setNormalOrientation(_towardsInfinite, Point(0,0)); 
\end{lstlisting}

This trick allows dealing with simple open manifolds, but for complex geometries (union of open manifolds for instance) the global orientation of normal vector remains uncontrollable.\\

For visualization purpose, the normal vectors can be exported to a vtk file using:\\

\begin{lstlisting}
Sigma.saveNormalsToFile("n_Sigma",_vtk);
\end{lstlisting}

They can be also collected in a \class{TermVector} object:\\

\begin{lstlisting}
Space V1(Omega,P1,"V1");  Unknown u1(V1,"u1",3);
TermVector Ns = normalsOn(Sigma,u1);
\end{lstlisting}

The normals are computed by L2 projection on the space \(W\) related to the \class{Unknown} used. More precisely, the normals \(n\) in space \(W\) are got by solving the problem: 

\[
\int_{\Sigma} n|t = \int_{\Sigma} n_0|t \ \ \ \forall t\in W
\]

where \(n_0\) is the normal to the element faces. If (\(w_j\)) denotes the basis of \(W\) and \(N\) the vector representing the normal in \(W\) basis (\(n=\sum_j n_j\,w_j\)), the following linear system is solved:

\[
\mathbb{A}N=B,\ \ \ \text{with }\mathbb{A}=\int_{\Sigma}w_i|w_j \ \text{and } B_i=\int_{\Sigma}n_0|w_i
\]

The unknown may be an explicit vector unknown or a scalar unknown if the space is a space of vectors. In the case of a P0 unknown, the normals are normals on element faces with no projection. In the case of Pk Lagrange unknown, the normals are some interpolated normals on Lagrange DoFs.

\subsection{Map of domains}

Some processes require a geometric map between two domains. For instance, to deal with periodic condition related to two side domains:

\[
u_{|\Sigma^+}=u_{|\Sigma^-}
\]

The elimination process uses the geometric map \(F\:\ \Sigma^+ \longrightarrow \Sigma^-\). The simple way to define such map is the following:\\

\begin{lstlisting}
Reals mapPM(const Point& P, Parameters& pa = defaultParameters)
{
   Point Q(P);
   Q(1)-=1;
   return Q;
}
\ldots
defineMap(sigmaP, sigmaM, mapPM);  
\end{lstlisting}

Note that the {\ttfamily mapPm} function returns a \var{Vector<Real>} which is more general than a \var{Point}. Respect this prototype !

\begin{ideabox}
When dealing with curved domains, generally meshed with P1,P2, \ldots elements, you can use some natural maps from one domain to the other (think about the scaling transformation  of a circle to another circle whereas the circles are approximated by polygons). In that case, points are transported using the given geometrical transformation and an additional projection to guarantee that the image points belong to the arrival domain. Obviously, the projection induces an error related to the mesh size. 
\end{ideabox}

\begin{center}
\includePict[width=7cm]{defineMap.png}
\end{center}

\begin{warningbox}
It is currently not possible to define two different maps for a pair of domains.
\end{warningbox}

\subsection{Assign properties to domains}

In some problems, physical properties may be different from a domain to other one. This may be managed by differentiating integrals in variational formulation:

\[
\int_{\Omega_1}\rho_1(x)u(x)\,v(x)+\int_{\Omega_2}\rho_2(x)u(x)\,v(x).
\]

But it may be too intricate if there are a lot of domains or integrals. So there is an alternative method consisting in defining a unique function \(\rho\) and deal with a unique integral:   

\[
\int_{\Omega}\rho(x)u(x)\,v(x)
\]

and assign id to domains that are subdomains:\\

\begin{lstlisting}
Real rho(const Point&P, Parameters& pars=defaultParameters)
{
 Number mat=materialIdFromParameters(pars);
 if(mat==1) return \ldots
 else return \ldots
}
\ldots
Domain omega=mesh2d.domain("omega");     //whole domain
Domain omega1=mesh2d.domain("omega_1");  //subdomain 
Domain omega2=mesh2d.domain("omega_2");  //subdomain
omega1.setMaterialId(1);
omega2.setMaterialId(2);
\end{lstlisting}

\subsection{Create domain of sides}
It may be useful to get the domain of all sides (points, edges, faces) of an existing domain. For instance the set of all faces of a volumic domain, the set of all edges of a surfacic domain. The \verb?sides(Domain)? function or the  \verb?Domain::sidesDomain()? member function do the job:\\
\begin{lstlisting}
Mesh msq(SquareGeo(_origin=Point(0.,0.),_length=1.,_nnodes=2,_domain_name="sq",_side_names="bsq"),
        _triangle,1,_structured);
Domain sq=msq.domain("sq"), bsq=msq.domain("bsq");
Domain ssq=sq.sidesDomain();   // edges of square domain
Domain ssq2 = sides(sq);       // alternate call
Domain sbsq=bsq.sidesDomain(); // edges of square boundary domain

Mesh mcu(Cube(_origin=Point(0.,0.,0.),_length=1.,_nnodes=2,_domain_name="cu"),
         _hexahedron,1,_structured);
Domain cu=mcu.domain("cu");
Domain scu=cu.sidesDomain(); // faces of cube domain

Mesh msp(Sphere(_center=Point(0.,0.,0.),_radius=1.,_nnodes=2,_domain_name="sph"),
         _triangle,1,_gmsh);
Domain sph=msp.domain("sph");
Domain ssph=sph.sidesDomain(); // edges of sphere domain
\end{lstlisting}
Note that by applying \verb?sidesDomain()? on a side domain, you get the sides of sides:
\begin{lstlisting}
Mesh mcu(Cube(_origin=Point(0.,0.,0.),_length=1.,_nnodes=2,_domain_name="cu"),
         _hexahedron,1,_structured);
Domain cu=mcu.domain("cu");
Domain scu=cu.sidesDomain();   // faces of cube domain
Domain sscu=scu.sidesDomain(); // edges of cube domain    
\end{lstlisting}
By default, the name of domain is "sides of \textit{domain.name}", by specifying a string as argument of  \verb?sidesDomain()? you can change it.

\begin{focusbox}
 By using the \verb?internalSides(Domain)? function or the  \verb?Domain::internalSidesDomain()? member function, it is also possible to get the domain of internal sides, i.e. the union of all sides excluding the sides located on the boundary (in other words the union of shared sides). 
\end{focusbox}
\subsection{Set operation on domain}
There exists two set operations : the union of domains (\verb?merge? function and + operator) and the difference of two domains (- operator). The \verb?merge? function accepts up to 6 domains. When there are more than 2 domains to merge, it is more efficient to use the \verb?merge? function than to use the + operator. The following example illustrates the use of set operations on domains:
\begin{lstlisting}
Rectangle R(_origin=Point(0.,0.),_xlength=1., _ylength=1.,_nnodes=31,_domain_name="Omega",
            _side_names={"Gamma1","Gamma2","Gamma3","Gamma4"}); 
Domain omega=mR.domain("Omega"), gamma1=mR.domain("Gamma1"), gamma2=mR.domain("Gamma2"),
                                 gamma3=mR.domain("Gamma3"), gamma2=mR.domain("Gamma4");
Domain gamma12 = merge(gamma1,gamma2);   // union of elements of gamma1 and gamma2
Domain gamma34 = gamma3 + gamma4;        // union of elements of gamma1 and gamma4
Domain gamma = merge(gamma1,gamma2,gamma3,gamma4,"gamma"); // union of elements of gammax
Domain sigma = sides(omega);     // set of all sides of elements of omega
Domain sigma0 = sigma - gamma;   // set of all internal sides of elements of omega
Domain d = sigma + omega;  // ERROR : elements have not the same dimension
\end{lstlisting}

\subsection{Cracking a domain}

There exist two ways in \xlifepp to crack a domain. The first one consists in using the \gmsh capabilities and the other one consists in using the intern cracking tool that it is attached to the \class{Mesh} class.

\subsubsection{Cracking a domain using \gmsh capabilities}

Theoretically, \gmsh allows you to crack domains (1D cracks in 2D meshes, 1D or 2D cracks in 3D meshes). Cracks can be opened or not.
A crack is opened when some boundary nodes of the domain to crack are duplicated as the other nodes, else it will be a closed crack.

To notify that the segment has to be cracked, you just call the \cmd{crack} routine on it. This is a general routine defining both opened and closed cracks through 2 additional optional arguments. Default behavior is closed cracks.
You can call the routine \cmd{closedCrack} (only the geometry in argument) to define a closed crack.
You can call the routine \cmd{openCrack} (the geometry and a domain name) to define an opened crack. In this case, the domain name is the boundary domain of the geometry you want to crack that will be opened. Let's see following examples to understand this.

There are two ways to define a geometry with a crack inside it: the direct one and the indirect one.

\subsubsection*{Defining cracks directly}

This way is the way you should always do to define a crack. A crack is a geometry inside a geometry of bigger dimension. So the geometry to be cracked must be defined as a meshed "hole" inside the container geometry. 

\begin{center}
\begin{tabular}{cc}
\begin{minipage}{5cm}
\begin{figure}[H]
\inputTikZ{crack-direct}
\end{figure}
\end{minipage}
&
\begin{minipage}{12cm}
\begin{lstlisting}
Point x1(0,0,0), x2(1,0,0), x3(1,1,0), x4(0,1,0), x5(0.2,0.2,0), x6(0.8,0.8,0), x7(0.2,0,0), x8(0.8,1,0);
Rectangle rrect8(_v1=x1, _v2=x2, _v4=x4, _domain_name="Omega", _side_names="Gamma");
Segment scrack(_v1=x5, _v2=x6, _nnodes=3, _domain_name="Crack", _side_names="Sigma");
openCrack(scrack,"Sigma");
Mesh m(rrect8+scrack, _triangle, 1, _gmsh);
\end{lstlisting}
\end{minipage}
\end{tabular}
\end{center}

Here, it is an opened crack. A side name is given to both ends of the segment. This name will be given to the routine \cmd{openCrack} to tell which ends are to be opened. Here, it is both. 

\subsubsection*{Defining cracks indirectly}

This way is called indirect, compared to the previous one, insofar as you have to link the geometry you want to crack to the boundaries of the parent geometry and define surfaces from their boundaries:

\begin{center}
\begin{tabular}{cc}
\begin{minipage}{5cm}
\begin{figure}[H]
\inputTikZ{crack-indirect}
\end{figure}
\end{minipage}
&
\begin{minipage}{12cm}
\begin{lstlisting}
Point x1(0,0,0), x2(1,0,0), x3(1,1,0), x4(0,1,0), x5(0.2,0.2,0), x6(0.8,0.8,0), x7(0.2,0,0), x8(0.8,1,0);
Segment s1(_v1=x1, _v2=x7, _domain_name="Gamma");
Segment s2(_v1=x2, _v2=x7, _domain_name="Gamma");
Segment s3(_v1=x3, _v2=x2, _domain_name="Gamma");
Segment s4(_v1=x8, _v2=x3, _domain_name="Gamma");
Segment s5(_v1=x8, _v2=x4, _domain_name="Gamma");
Segment s6(_v1=x4, _v2=x1, _domain_name="Gamma");
Segment s7(_v1=x7, _v2=x5);
Segment s8(_v1=x5, _v2=x6, _nnodes=3, _domain_name="Crack");
Segment s9(_v1=x6, _v2=x8);
crack(s8);
Geometry sf1=surfaceFrom(s7+s8+s9+s5+s6+s1,"Omega1");
Geometry sf2=surfaceFrom(s7+s8+s9+s4+s3+s2,"Omega2");
Mesh m(sf1+sf2, _triangle, 1, _gmsh);
\end{lstlisting}
\end{minipage}
\end{tabular}
\end{center}

Here, it is a closed crack.

\begin{focusbox}
In this example, surfaces have different domain names. You can also give the same domain name
\end{focusbox}

\subsubsection*{Which way is better ?}

\begin{center}
\begin{tabular}{|c|c|c|}
\cline{2-3} 
\multicolumn{1}{c|}{} & direct way & indirect way \\
\hline
1D crack in 2D mesh & 100\% safe & 100\% safe \\
\hline
2D crack in 3D mesh & not 100\% safe & 100\% safe \\
\hline
1D crack in 3D mesh & to be tested & to be tested \\
\hline
\end{tabular}
\end{center}

\begin{focusbox}
\gmsh team is currently working on improving their crack engine to be 100\% whatever the case.
\end{focusbox}

\subsubsection*{A look at the mesh file}

Let's see the resulting mesh file for the indirect example above:\\

\inputCode[style=draft, lastline=12]{after_crack.msh}
\inputCode[style=highlight, firstline=13, lastline=15]{after_crack.msh}
\inputCode[style=draft, firstline=16, lastline=21]{after_crack.msh}
\inputCode[style=highlight, firstline=22, lastline=22]{after_crack.msh}
\inputCode[style=draftend, firstline=23, lastline=31]{after_crack.msh}

Bounds of the cracked domain are not duplicated (nodes 1 and 2), whereas the middle node of the cracked domain is duplicated (nodes 3 and 10):\\

\inputCode[style=draft, firstline=32, lastline=33]{after_crack.msh}
\inputCode[style=highlight, firstline=34, lastline=35]{after_crack.msh}
\inputCode[style=draft, firstline=36, lastline=41]{after_crack.msh}
\inputCode[style=highlight, firstline=42, lastline=43]{after_crack.msh}
\inputCode[style=draftend, firstline=44]{after_crack.msh}

Segments are also duplicated. If you're familiar with the msh file format, by reading elements 11 and 24 for instance, we can deduce that domain "Omega1" has geometrical reference 7 and domain "Omega2" has geometrical reference 11. These references will be used with the cracked domain name to name sides of the crack, namely "Crack\_7" and "Crack\_11".

\subsubsection{Cracking a domain using intern tool}

The way to crack a domain is very similar to the previous one, except the crack function acts on a mesh and a side domain and not on the geometry:\\

\begin{lstlisting}
Point x1(0,0,0), x2(1,0,0), x3(1,1,0), x4(0,1,0), x5(0.2,0.2,0), 
      x6(0.8,0.8,0), x7(0.2,0,0), x8(0.8,1,0);
Rectangle r(_v1=x1, _v2=x2, _v4=x4, _domain_name="Omega", _side_names="Gamma");
Segment s(_v1=x5, _v2=x6, _nnodes=3, _domain_name="Sigma");
Mesh m(r+s, _triangle, 1, _gmsh); //mesh as usual
Domain sigma=m.domain("Sigma");   //get the crack domain
m.crack(sigma);                   //mesh is now cracked
\end{lstlisting}

By default, the crack is not open (end points are not duplicated). By specifying {\verb|_opencrack|} when calling the crack function, the crack will be open (end points are duplicated):\\

\begin{lstlisting}
m.crack(sigma,_openCrack);   
\end{lstlisting}

Once the mesh is cracked, the sides of the crack are named \textit{xxx+} and \textit{xxx-} where \textit{xxx} is the name  of the original domain supporting the crack. It is possible to crack a domain with few segments, naming the segments with the same name. Even, one can crack a closed domain (for instance a polygon).\\

This tool is working in 2D and 3D, but fails when trying to crack a domain with a triple point (for instance an Y shape crack).

\subsection{Having statistics about the mesh quality of a domain}

You may want to known statistics about the mesh you use : the minimum, maximum and average value of :

\begin{itemize}
\item measure and size of elements
\item measure of side of elements
\end{itemize}

To have these statistics, you have to call \cmd{computeStatistics} on each domain you want information :

\begin{lstlisting}
Domain omega=\ldots
omega.computeStatistics();
theCout << omega << eol;
\end{lstlisting}

\cmd{computeStatistics} have an optional boolean argument, whose default value is false, meaning deactivation of the computation of side statistics.
\subsection{Summary of domain operations}
We only list here the stuff interesting users:
\begin{center}
\begin{tabular}{|l|l|l|}
 \hline \multicolumn{3}{|c|}{Properties} \\
 \hline \verb?dom.numberOfElements()?& \(\rightarrow\) Number& number of elements in domain\\
 \hline \verb?dom.setNormalOrientation()?& \(\rightarrow\) void& set normal orientation of a side domain\\
 \hline \verb?dom.parametrization()?& \(\rightarrow\) Parametrization& access to parametrization if defined\\
 \hline \verb?dom.setMaterialId(id)?& \(\rightarrow\) void& associates a material id\\
 \hline \verb?dom.setDomainId(id)?& \(\rightarrow\) void& associates a domain id\\
 \hline \verb?dom.measure()?& \(\rightarrow\) Real& measure of domain\\
 \hline \verb?defineMap(dom1,dom2,m)?& \(\rightarrow\) void& relates dom1 and dom2 with a map\\
 
 \hline \multicolumn{3}{|c|}{Domain building} \\
 \hline \verb?sides(dom)?& \(\rightarrow\) Domain & domain of sides of elements of \verb?dom?\\
 \hline \verb?internalSides(dom)?& \(\rightarrow\) Domain & domain of internal sides of elements of \verb?dom?\\
 \hline \verb?crack(dom1,dom2)?& \(\rightarrow\) Domain& crack domain from both sides of the crack\\
 \hline \verb?dom.extendDomain()?& \(\rightarrow\) Domain& extension domain of a side domain\\
 \hline \verb?dom.fictitiousDomain()?& \(\rightarrow\) Domain& fictitious domain of a side domain\\
 \hline \verb?merge(dom1,dom2, \ldots)?& \(\rightarrow\) Domain& union of n domains with same element dimension\\
 \hline \verb?dom1 + dom2?& \(\rightarrow\) Domain& union of 2 domains with same element dimension\\
 \hline \verb?dom1 - dom2?& \(\rightarrow\) Domain& difference of 2 domains \\ 
\hline
\end{tabular}
\end{center}

\section{Dealing with parametrizations of geometrical domains}

\xlifepp may associate a \class{Parametrization} object to each \class{Geometry} object. It is accessible using the member function \class{Geometry}::\cmd{parametrization}() : 
\begin{lstlisting}
Disk di(_center=Point(0.,0.,0.),_radius=1.,_nnodes=20);
const Parametrization& par=di.parametrization(); 
const Parametrization& par=di.boundaryParametrization(); 
\end{lstlisting}
Parametrization exists for any canonical geometry and also for composite geometry, but in that case it is a \class{PiecewiseParametrization} object (inheriting from \class{Parametrization}). It is also possible to access to the parametrization of the boundary. \class{Parametrization} class offers access to some useful geometrical properties (derivatives, jacobian, curvilinear abscissa curvatures, normal and tangent vectors, \ldots); see \class{Parametrization} documentation. \class{Parametrization} objects are attached to the geometry of a \class{GeomDomain} object, but they can be accessed straightforward using the same functions:
\begin{lstlisting}
Disk di(_center=Point(0.,0.,0.),_radius=1.,_nnodes=20,domain_name="Omega");
Mesh m(di,_triangle,1,_gmsh);
Domain omega=m.domain("Omega");
const Parametrization& par=omega.parametrization(); 
const Parametrization& par=omega.boundaryParametrization(); 
\end{lstlisting} 
\begin{warningbox}
Parametrizations are not yet available for geometries coming from OpenCascade.
\end{warningbox}

\section{A full example with periodic cavities}

You want to mesh a rectangular domain, but on the bottom side of the rectangle, you want to have periodic cavities with the following pattern:

\begin{figure}[H]
\centering
\includePict[width=14cm]{meshWithPeriodicCavities.png}
\end{figure}

\begin{figure}[H]
\centering
\inputTikZ{cavityT}
\caption{Definition of the cavity}
\end{figure}

First, you have to define the first cavity, according to the previous figure:\\

\begin{lstlisting}
// Definition of the cavity
Real l1=0.06, l2=0.04, l3=0.08, h1=0.05, h2=0.1, s=0.01;
Point po(1.,0.);
Point pa=po+Point(l1,0.);
Point pb=pa+Point(0.,h1);
Point pc=pb+Point(-l2,0.);
Point pd=pc+Point(0.,h2);
Point pe=pd+Point(2.*l2+l3,0.);
Point pf=pe+Point(0.,-h2);
Point pg=pf+Point(-l2,0.);
Point ph=pg+Point(0.,-h1);
Point pi=ph+Point(l1,0.);

Segment s1(_v1=po, _v2=pa, _hsteps=s), s2(_v1=pa, _v2=pb, _hsteps=s),
        s3(_v1=pb, _v2=pc, _hsteps=s), s4(_v1=pc, _v2=pd, _hsteps=s),
        s5(_v1=pd, _v2=pe, _hsteps=s), s6(_v1=pe, _v2=pf, _hsteps=s),
        s7(_v1=pf, _v2=pg, _hsteps=s), s8(_v1=pg, _v2=ph, _hsteps=s),
        s9(_v1=ph, _v2=pi, _hsteps=s);

Geometry cavity=s1+s2+s3+s4+s5+s6+s7+s8+s9;
\end{lstlisting}

When done, you can define the other cavities as results of translations of the first cavity:\\

\begin{lstlisting}
// Definition of the cavities
Real cL=2.*l1+l3; // cavity length
Number nbcav=10; // number of cavities
Geometry cavities=cavity;
for (Number n=1;n<nbcav; n++) { cavities+=translate(cavity,n*cL,0.); }
\end{lstlisting}

Finally, we define borders of the main domain, and mesh the resulting \class{Geometry} defined with \cmd{surfaceFrom}:\\

\begin{lstlisting}[deletekeywords={[3] borders}]
// full geometry
Real sb=0.05;
Point p1(0.,0.), p2(2.+nbcav*cL,0.), p3(2.+nbcav*cL,1.), p4(0.,1.);
Segment s0(_v1=p1, _v2=po, _hsteps=Reals(sb,s)),
        s10(_v1=Point(1.+nbcav*cL,0.), _v2=p2, _hsteps=Reals(s,sb)),
        s11(_v1=p2, _v2=p3, _hsteps=sb, _domain_name="SigmaP"),
        s12(_v1=p3, _v2=p4, _hsteps=sb),
        s13(_v1=p4, _v2=p1, _hsteps=sb, _domain_name="SigmaM");

Geometry borders=s0+cavities+s10+s11+s12+s13;

 //create mesh
Mesh mesh2d(surfaceFrom(borders,"Omega"), triangle, 1, _gmsh);
Domain omega=mesh2d.domain("Omega");
Domain sigmaP=mesh2d.domain("SigmaP");
Domain sigmaM=mesh2d.domain("SigmaM");
\end{lstlisting}


