%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\section{\classtitle{Kernel}}

\subsection{User kernels}

The \class{Function} class allows defining kernel type function, say function of two points. But, to deal with integral equation, more information are required. It is the role of the \class{Kernel} class. A \class{Kernel} object manages mainly:
\begin{lstlisting}[deletekeywords={[3] kernel, symmetry, name}]
Function kernel;              // kernel function
Function gradx;               // x derivative
Function grady;               // y derivative
Function gradxy;              // x,y derivative
Function ndotgradx;           // nx.gradx if available
Function ndotgrady;           // ny.grady if available
Function curlx;               // curl_x if available
Function curly;               // curl_y if available
Function curlxy;              // curl_x curl_y if available
Function divx;                // div_x if available
Function divy;                // div_y if available
Function divxy;               // div_x div_y if available
Function dx1;                 // d_x1 if available
Function dx2;                 // d_x2 if available 
Function dx3;                 // d_x3 if available 
Kernel* singPart;             // singular part of kernel
Kernel* regPart;              // regular part of kernel

Dimen dimPoint;               // dimension of points
SingularityType singularType; // singularity (_notsingular, _r, _logr,_loglogr)
Real singularOrder;           // order of singularity
Complex singularCoefficient;  // coefficient of singularity
SymType symmetry;             // kernel symmetry :_noSymmetry, _symmetric, ...
String name;                  // kernel name
Parameters userData;          // to store some additional information
\end{lstlisting}

When dealing with a matrix kernel it may be useful to give \cmd{d\_x1, d\_x2, d\_x3} because it is not possible to define the gradient of a matrix kernel as a \class{Function}.\\

So when defining a new one, you have to provide such information. To understand how it works, this is the example of Helmholtz3d kernel.\\

First define all the functions as ordinary c++ functions:\\

\begin{lstlisting}[deletekeywords={[3] x, y}]
// kernel G(k; x, y)=exp(i*k*r)/(4*pi*r)
Complex Helmholtz3d(const Point& x, const Point& y, Parameters& pa)
{
  Real k = real(pa("k"));
  Real r = x.distance(y);
  Complex ikr = Complex(0., 1.) * k * r;
  return over4pi * std::exp(ikr) / r;
}

Vector<Complex> Helmholtz3dGradx(const Point& x, const Point& y, Parameters& pa)
{
  Real k = real(pa("k"));
  Real r2 = x.squareDistance(y);
  Real r = std::sqrt(r2);
  Complex ikr = Complex(0., 1.) * k * r;
  Complex dr = (ikr - 1.) / r2;
  Vector<Complex> g1(3);
  scaledVectorTpl(over4pi * exp(ikr)*dr / r, x.begin(), x.end(), y.begin(), g1.begin());
  return g1;
}

Vector<Complex> Helmholtz3dGrady(const Point& x, const Point& y,Parameters& pa)
{
  Real k = real(pa("k"));
  Real r2 = x.squareDistance(y);
  Real r = std::sqrt(r2);
  Complex ikr = Complex(0., 1.) * k * r;
  Complex dr = (ikr - 1.) / r2;
  Vector<Complex> g1(3);
  scaledVectorTpl(-over4pi * exp(ikr)*dr / r, x.begin(), x.end(), y.begin(), g1.begin());
  return g1;
}
\end{lstlisting}

Define regular part functions:\\

\begin{lstlisting}[deletekeywords={[3] x, y}]
// regular part: G_reg(k; x, y)=(exp(i*k*r)-1)/(4*pi*r)
Complex Helmholtz3dReg(const Point& x, const Point& y, Parameters& pa)
{
  Complex g;
  Real k = real(pa("k"));
  Real kr = k * x.distance(y);
  Complex ikr = Complex(0., kr);
  if (std::abs(kr) < 1.e-04)
  {
    int n=4; // for abs(kr)<1.e-4 this is a good choice for n (checked)
    g = 1 + ikr / n--;
    while (n > 1) {g = 1 + g * ikr / n--;}
    return g *= Complex(0., over4pi * k);
  }
  else return over4pi * k * (std::exp(ikr) - 1.) / kr;
}

Vector<Complex> Helmholtz3dGradxReg(const Point& x, const Point& y, Parameters& pa)
{
  Real k = real(pa("k"));
  Real r = x.distance(y);
  Complex ikr = Complex(0., k*r);
  Complex t = over4pi * (1. + std::exp(ikr)*(ikr - 1.))/ r;
  Vector<Complex> g(3);
  scaledVectorTpl(t/ r, x.begin(), x.end(), y.begin(), g.begin());
  return g;
}

Vector<Complex> Helmholtz3dGradyReg(const Point& x, const Point& y, Parameters& pa)
{
  Real k = real(pa("k"));
  Real r = x.distance(y);
  Complex ikr = Complex(0., k*r);
  Complex t = - over4pi * (1. + std::exp(ikr)*(ikr - 1.))/ r;
  Vector<Complex> g(3);
  scaledVectorTpl(t/ r, x.begin(), x.end(), y.begin(), g.begin());
  return g;
}
\end{lstlisting}

Define singular part functions:\\

\begin{lstlisting}[deletekeywords={[3] x, y}]
//construct Helmholtz3d Kernel singular part: G_sing(k; x, y)=1/(4*pi*r)
Complex Helmholtz3dSing(const Point& x, const Point& y, Parameters& pa)
{
  Real r = x.distance(y);
  return over4pi/r;
}

Vector<Complex> Helmholtz3dGradxSing(const Point& x, const Point& y, Parameters& pa)
{
  Real r = x.distance(y);
  return -over4pi / (r*r);
  Complex t = -over4pi / (r*r);
  Vector<Complex> g(3);
  scaledVectorTpl(t, x.begin(), x.end(), y.begin(), g.begin());
  return g;
}

Vector<Complex> Helmholtz3dGradySing(const Point& x, const Point& y, Parameters& pa)
{
  Real r = x.distance(y);
  Complex t = over4pi / (r*r);
  Vector<Complex> g(3);
  scaledVectorTpl(t, x.begin(), x.end(), y.begin(), g.begin());
  return g;
}
\end{lstlisting}
\vspace{.5cm}

Now construct \class{Kernel} objects:\\

\begin{lstlisting}[deletekeywords={[3] kernel, symmetry, name}]
Parameters pars;
pars<<Parameter(1.,"k");       

Kernel H3Dreg;   //regular part
H3Dreg.name="Helmholtz 3D kernel regular part";
H3Dreg.singularType =_notsingular;
H3Dreg.singularOrder = 0;
H3Dreg.singularCoefficient = over4pi;
H3Dreg.symmetry=_symmetric;
H3Dreg.userData = pars;
H3Dreg.dimPoint = 3;
H3Dreg.kernel = Function(Helmholtz3dReg, pars);
H3Dreg.gradx = Function(Helmholtz3dGradxReg, pars);
H3Dreg.grady = Function(Helmholtz3dGradyReg, pars);

Kernel H3Dsing;   //singular part
H3Dsing.name="Helmholtz 3D kernel, singular part";
H3Dsing.singularType =_r;
H3Dsing.singularOrder = -1;
H3Dsing.singularCoefficient = over4pi;
H3Dsing.symmetry=_symmetric;
H3Dsing.userData = pars;
H3Dsing.dimPoint = 3;
H3Dsing.kernel = Function(Helmholtz3dSing, pars);
H3Dsing.gradx  = Function(Helmholtz3dGradxSing, pars);
H3Dsing.grady  = Function(Helmholtz3dGradySing, pars);

Kernel H3D;   //kernel
H3D.name="Helmholtz 3D kernel";
H3D.singularType =_r;
H3D.singularOrder = -1;
H3D.singularCoefficient = over4pi;
H3D.symmetry=_symmetric;
H3D.userData = pars;
H3D.dimPoint = 3;
H3D.kernel = Function(Helmholtz3d, pars);
H3D.gradx  = Function(Helmholtz3dGradx, pars);
H3D.grady  = Function(Helmholtz3dGrady, pars);
H3D.regPart  = &H3Dreg;
H3D.singPart = &H3Dsing;
\end{lstlisting}

\begin{warningbox}
If you do not define singular and regular part kernels, some computations will not be available.
\end{warningbox}

In fact the Helmholtz kernels are defined in \lib{mathsResources} library of \xlifepp. To load it, use the following code:\\

\begin{lstlisting}
Parameters pars;
pars<<Parameter(1.,"k");       
Kernel H3D = Helmholtz3dKernel(pars);
\end{lstlisting}

\subsection{Dealing with normal vectors}

For \class{Kernel} objects, one can pass to some kernel functions either \_nx vector or \_ny vector or both using the same method as one used for \class{Function}:\\

\begin{lstlisting}
Complex G(const Point& P,const Point& Q, Parameters& pa = default_Parameters)
{
Reals nx= getNx(); // get the normal vector at P
Reals ny= getNy(); // get the normal vector at Q
...
}
...
Parameters pars(1,"k"); // declare k in the parameters
Kernel K(g,pars);       // associate parameters to Kernel object
K.require(_nx);         // declare that kernel uses x-normal
K.require(_ny);         // declare that kernel uses y-normal
TermMatrix B(intg(Sigma,Sigma,u*K*v));  // use kernel K 
\end{lstlisting}

Kernel functions defined in \xlifepp managed the normal vectors, so the user has not to deal with.

\begin{focusbox}
By default, the dimension of points of a Kernel is 3. When you define a 2D kernel, it may happen some troubles with point dimensions, in particular if the kernel function involves some operations sensitive to the point dimension. You can cure this problem by testing point dimensions in the kernel function or by specifying the point dimension when building Kernel:

\begin{lstlisting}  
Real ker(const Point& x, const Point& x, Parameters& pa=defaultParameters)  
{...}
...
Kernel K(ker); K.dimPoint=2;
//or
Kernel K(ker,2);
\end{lstlisting} 
\end{focusbox}

\begin{ideabox}	
If you develop a new kernel for your own use, contact the administrators. Maybe they will be happy to integrate your work in \xlifepp.
\end{ideabox}

\subsection{Tensor kernel}
Tensor kernels are kernels of the following form:
\[K(x,y)=\sum_{1\le m\le M}\sum_{1\le n\le N} \psi_m(x)\mathbb{A}_{mn}\phi_n(y)\]
with \(\mathbb{A}\) an \(M\times N\) matrix. Such kernel is involved, for instance, in Dirichlet to Neumann method using spectral expansion. In many cases, the matrix is a diagonal one:
\[K(x,y)=\sum_{1\le n\le N} \psi_n(x)\lambda_n\phi_n(y).\]
In the meaning of \xlifepp, the families \((\psi_m)_{1\le m\le M}\) and  \((\psi_n)_{1\le n\le N}\) may be considered as some basis of spectral space, say \class{SpectralBasis} object. To define a \class{TensorKernel}, you have to specify either one or two \class{Function} defining the spectral families, one or two \class{SpectralBasis}, one or two \class{Unknown} defined on a spectral \class{Space} or one or two \class{TermVectors} handling numerical spectral family. The matrix \(\mathbb{A}\) must be also given as a \class{Matrix} or a \class{Vector} if \(\mathbb{A}\) is diagonal.
\begin{lstlisting}  
Real cosny(const Point& P, Parameters& pa = defaultParameters)
{
Real y=P(2);
Real h=pa("h");                //get the parameter h (user definition)
int_t n=basis_index();         //get the index of function to compute
if(n==0) return sqrt(1./h);
else     return sqrt(2./h)*std::cos(n*pi_*(y+h*0.5)/h);
}
...
Real k=2.; Number N=10;
Parameters params(1.,"h");
Vector<Complex> A(N);
for(Number n=0; n<N; n++) A[n]=sqrt(Complex(k*k-n*n*pi_*pi_/(h*h)));  // matrix A
Function cosN(cosny,params);                                          // spectral Function's
TensorKernel tkp(cosN,A);                                             // define TensorKernel                    
\end{lstlisting} 
In an alternate way, you can also construct the \class{TensorKernel} as follows:
\begin{lstlisting}  
Space Sp(_domain=sigma, _basis=cosN, _dim=N, _name="vect{cos(n*pi*y)}");
Unknown phi(Sp,"phi");     // spectral unknown
TensorKernel tkp(phi,A);
\end{lstlisting} 
Doing this, a spectral unknown is available to deal with problem involving such unknown. All the following constructors are available:
\begin{lstlisting}  
template<typename T>
TensorKernel(SpectralBasis&, vector<T>&, bool conj = false);          
TensorKernel(SpectralBasis&, Matrix<T>&, bool conj = false);         
TensorKernel(SpectralBasis&, vector<T>&, SpectralBasis&);
TensorKernel(SpectralBasis&, Matrix<T>&, SpectralBasis&);
TensorKernel(Function, vector<T>&, bool conj = false); 
TensorKernel(Function, Matrix<T>&, bool conj = false);  
TensorKernel(Function, vector<T>&, Function);     
TensorKernel(Function, Matrix<T>&, Function);         
TensorKernel(Unknown&, vector<T>&, bool conj = false);   
TensorKernel(Unknown&, Matrix<T>&, bool conj = false);        
TensorKernel(Unknown&, vector<T>&, Unknown&);
TensorKernel(Unknown&, Matrix<T>&, Unknown&); 
TensorKernel(vector<TermVector>&, vector<T>&, bool conj = false);
TensorKernel(vector<TermVector>&, Matrix<T>&, bool conj = false); 
TensorKernel(vector<TermVector>&, vector<T>&, vector<TermVector>&); 
TensorKernel(vector<TermVector>&, Matrix<T>&, vector<TermVector>&);
\end{lstlisting}
By default, the second family (may be same as the first one) is not conjugated in computation. By activating the option \verb?conj? to \verb?true?, the second family will be conjugated in computation.\\

It is possible to associate some geometric maps (\(f,g\)) to a tensor kernel:
\[K(x,y)=\sum_{1\le m\le M}\sum_{1\le n\le N} \psi_m(f(x))\mathbb{A}_{mn}\phi_n(g(y))\].
\begin{lstlisting}  
Vector<Real> f(const Point& P, Parameters& pa = defaultParameters){...}
Vector<Real> g(const Point& P, Parameters& pa = defaultParameters){...}
...
TensorKernel tkp(cosN,A);  
tkp.xmap=Function(f);   
tkp.ymap=Function(g);
\end{lstlisting}
\vspace{2mm}
When \class{TensorKernel} is defined from vector spectral families (i.e. \(\boldsymbol{\phi}_n\) is a vector function), the vector tensor kernel reads 
\[
\mathbb{K}(x,y)=\sum_{1\le m\le M}\sum_{1\le n\le N} \boldsymbol{\phi}_n(y)\mathbb{A}_{mn}\boldsymbol{\psi}_m^t(x)\]
with \(\mathbb{K}\) a matrix dim(\(\boldsymbol{\phi}_n\))\(\times\) dim(\(\boldsymbol{\psi}_m\)). This vector kernel can be used in bilinear form as follows:
\[
\int_{\Gamma}\int_{\Sigma}\mathbf{u}(y)|(K(x,y)*\mathbf{v}(x))=\sum_{1\le m\le M}\sum_{1\le n\le N}\mathbb{A}_{mn}\int_{\Gamma}(\mathbf{u}|\boldsymbol{\phi}_n)
\int_{\Sigma}(\mathbf{v}|\boldsymbol{\psi}_m)
\]

with dim(\(\boldsymbol{u}\))=dim(\(\boldsymbol{\phi}_n\)) and  dim(\(\boldsymbol{v}\))=dim(\(\boldsymbol{\psi}_m\)) and \((.|.)\) the inner product. In terms of FE (\(\boldsymbol{w}_j\) and \(\boldsymbol{\tau}_i\) basis functions related respectively to \(u\) and \(v\)), \xlifepp produces the matrix

\[
\mathbb{T}_{ij} = \sum_{1\le m\le M}\sum_{1\le n\le N}\mathbb{A}_{mn}\int_{\Gamma}(\boldsymbol{w}_j|\boldsymbol{\phi}_n)
\int_{\Sigma}(\boldsymbol{\tau}_i|\boldsymbol{\psi}_m)
\]

