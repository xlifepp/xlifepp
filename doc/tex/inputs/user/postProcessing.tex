%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex

\section{Integral representation}

In the context of integral equation, the solution of IE is a potential on the boundary (\(\Gamma\)).
This potential is not easy to interpret, so the final step of a BEM is often the reconstruction
of the field outside \(\Gamma\).
For instance, the Helmholtz diffraction Dirichlet problem may be solved using a single layer potential
\(q=[\partial_n u_]{|\Gamma}\) and the diffracted field outside the boundary \(\Gamma\) is given by 
\[u(x)=\int_{\Gamma}G(x,y)\,q(y)\,dy.\]
\xlifepp addresses the general form of integral representation :
\[u(x)=\int_{\Gamma} opk(G(x,y))\otimes opu(q(y))\,dy.\]
where \(opk\) is an operator on kernel, \(opu\) an operator on unknown and \(\otimes\) one of the operation
\(*\), \(|\), \(\wedge\) or \(\%\). The previous example corresponds to \(opk=id\), \(opu=id\) and \(\otimes=*\).
To deal with such integral representation, the user has to define a linear form from \cmd{intg} constructor:

\begin{lstlisting}
LinearForm ri=intg(Gamma, G*q);  //default integration method

LinearForm ri=intg(Gamma, G*q, Gauss_Legendre,3);//specifying quadrature rule

IntegrationMethods ims(Gauss_Legendre,10, 1., Gauss_Legendre, 3);
LinearForm ri=intg(Gamma, G*q, ims); //specifying 2 quadrature rules
\end{lstlisting}

In these expressions, Gamma is a \class{Domain} object, G a \class{Kernel} object and q an \class{Unknown} object. Singular integration method is required if you intend to evaluate the integral representation at points close to the boundary \(\Gamma\).\\

The linear form may be a linear combination of  \cmd{intg}:

\begin{lstlisting}
IntegrationMethods ims(Gauss_Legendre,10, 1., Gauss_Legendre, 3);
LinearForm ri=intg(Gamma, G*q, ims) + intg(Gamma, (grad_x(G)|_nx)*q, ims)     
\end{lstlisting}

There are several methods to compute the integral representation.

\subsection{Direct method}

To effectively compute integral representation you have to specify the vector representing the numerical potential, a \class{TermVector} object (say Q) and the points where to evaluate it. There are many way to give points:

\begin{itemize}
\item compute at one point x:

\begin{lstlisting}[deletekeywords={[3] x}]
LinearForm ri=intg(Gamma, G*q, Gauss_Legendre,3);  
Complex val;
Point x(0,0,2);
integralRepresentation(x,ri,Q,val);    
\end{lstlisting}

\item compute at an explicit list of points:

\begin{lstlisting}
LinearForm ri=intg(Gamma, G*q, Gauss_Legendre,3);  
Vector<Point> xs(10); 
xs(1)= Point(0,0,2); \ldots
Vector<Complex> val(10);
integralRepresentation(xs,ri,Q,val);    
\end{lstlisting}

\item compute at an implicit list of points of a \class{Domain} object (say omega):

\begin{lstlisting}
LinearForm ri=intg(Gamma, G*q, Gauss_Legendre,3);  
Vector<Point> xs; 
Vector<Complex> val;
integralRepresentation(omega, ri, Q, val, xs); //val and xs are filled by function  
\end{lstlisting}

\item compute at an implicit list of node points of an interpolation on a \class{Domain}:
\begin{lstlisting}
LinearForm ri=intg(Gamma, G*q, Gauss_Legendre,3); 
TermVector U=integralRepresentation(u, omega, ri, Q);  //u unknown on a Lagrange space   
\end{lstlisting}
\end{itemize}

\begin{focusbox}
In the previous syntaxes the type of output \var{val} has to be consistent with data. For instance, \var{val} is of complex type if G or Q is of complex type.
The last syntax is more robust because the type is determined by the function. 
Besides, this syntax returns a TermVector that may be straight exported to a file for visualization. 
\end{focusbox}

Note that, integral representations may return a vector of vectors but not a vector of matrices. For instance, if you want to compute the gradient of the integral representation (scalar), write:

\begin{lstlisting}
Vector<Vector<Complex> > gsl;
integralRepresentation(xs, intg(Gamma,grad_x(K)*u,ims), dnU, gsl);
\end{lstlisting}

or

\begin{lstlisting}
Unknown us(V,"us",2); //vector unknown !
TermVector gSL=integralRepresentation(us,omega,intg(Gamma,grad_x(K)*u,ims), dnU);
\end{lstlisting}

In this last form, attach to your \class{TermVector} a vector unknown with the dimension of the result.

\subsection{Matrix method}

There exists another way to deal with integral representations. It consists in computing the matrix 
\[R_{ij}=\int_{\Gamma} opk(G(x_i,y))\otimes opu(\tau_j(y))\,dy.\]
Special functions will produce such matrices embedded in a \class{TermMatrix} object:

\begin{lstlisting}
TermMatrix integralRepresentation(const Unknown&, const GeomDomain&, 
                                  const LinearForm&); 
TermMatrix integralRepresentation(const GeomDomain&, const LinearForm&);                 
TermMatrix integralRepresentation(const std::vector<Point>&, 
                                  const LinearForm&);         
\end{lstlisting}

When no domain is explicitly passed, one shadow \class{PointsDomain} object will be created from the list of points given. When no unknown is explicitly  passed, one (minimal) space and related shadow unknown will be created to represent the row unknown.\\
The following example (2D diffraction problem) shows how to use this method of integral representation:   

\begin{lstlisting}
Number nmesh=25;
Disk disk_int(_center=Point(0.,0.,0.),_radius=1.,
              _nnodes=nmesh,_side_names="Gamma");
Disk disk_ext(_center=Point(0.,0.,0.),_radius=2.,_nnodes=2*nmesh,
              _domain_name="Omega",_side_names="Sigma");
Mesh mesh(disk_ext-disk_int,_triangle, 1, _gmsh);
Domain Gamma = mesh.domain("Gamma"), Sigma = mesh.domain("Sigma"), Omega=mesh.domain("Omega");
// define spaces, unknown and testfunction
Space V0(_domain=Gamma, _interpolation=P0, _name="V0", _notOptimizeNumbering);
Unknown u0(V0,"u0"); TestFunction v0(u0,"v0");
Space V1(_domain=Omega, _interpolation=P1, _name="V1", _notOptimizeNumbering);
Unknown u(V1,"u");
//define Kernel and integration method
Kernel H=Helmholtz3dKernel(k);
IntegrationMethods ims(Duffy,8,0.,Gauss_Legendre,6,2.,Gauss_Legendre,3);
//define forms
LinearForm lf = intg(Gamma,g*v0); //rhs linear form
BilinearForm aSL=intg(Gamma,Gamma,u0*H*v0,ims); //single layer bininear form
LinearForm lSL(intg(Gamma,H*u0,Gauss_Legendre,6)); //intg. rep. linear form
//build system and solve it
TermVector rhs(lf);
TermMatrix ASL(aSL);
TermVector uSL = gmresSolve(ASL, rhs, _tolerance=1.0e-6, _maxIt=500);
//intg rep on Omega producing a TermMatrix
TermMatrix R=integralRepresentation(u,Omega,lSL);
TermVector U=R*uSL;     
\end{lstlisting}

This method is a little more time expansive than computing directly the integral representation. Thus, if there are a lot of integral representations to do with different data, it may be of interest. Obviously, it is memory consuming.\\

\subsection{Kernel interpolation method}

When points \(x\) are far from boundary \(\Gamma\), an alternate method consists in computing IR by interpolation method. Let \(\Omega\) the domain where IR is evaluated and 
\(V_\Omega\) a Lagrange finite element space of interpolation defined on \(\Omega\). Denote \((w_i)_i\) the basis functions associated to \(V_\Omega\) space. 
Let \(W_\Gamma\) a Lagrange finite element space defined on \(\Gamma\) and \((\tau_j)_j\) the basis functions associated to it. Interpolated the kernel 
at nodes \(x_i\in \Omega\) and \(y_j\in \Gamma\), IR is approximated by

\[u(x_i)\approx\int_\Gamma \sum_i\sum_j G(x_i,y_j)w_i(x)\,\tau_j(y)\,q(y)\,dy\,dx.\]

If \(q\) has the following decomposition \(q(y)=\sum_k q_k \sigma_k(y)\) we have :
\[u(x_i)\approx \sum_i\sum_j\sum_k G(x_i,y_j)w_i(x)q_k \int_\Gamma \tau_j(y) \sigma_k(y)\,dy\,dx\]
that reads in vector form (\(U=(u(x_i)_i)\), \(Q=(q_k)_k\)) :
\[U=\mathbb{G}*\mathbb{M}*Q\]
with \(\mathbb{G}\) the matrix \((G(x_i,y_j))_{ij}\) and \(\mathbb{M}\) the mass matrix :
\[
\mathbb{M}_{jk}=\int_\Gamma \tau_j\,\sigma_k.\]
This example shows how it is done with \xlifepp:

\begin{lstlisting} 
Space Vq(_domain=Omega, _interpolation=P0); Unknown q(Vq,"q"); 
computation of Q \ldots
Space Vo(_domain=Omega, _interpolation=P1); Unknown u(Vo,"u"); 
Space Vg(_domain=Gamma, _interpolation=P1); Unknown v(Vg,"v"); 
TermMatrix Gi(u, Omega, v, Gamma, G, "Gi"); //G(xi,yj)
TermMatrix M(intg(Gamma,q*v),"M"); compute(M);
TermVector U=Gi*(M*Q);
\end{lstlisting}

Because kernel is interpolated, the mesh of \(\Omega\) does not be too coarse. Vg may be chosen equal to Vq.
This method is generally faster than previous ones because computation of the mass matrix is a fast process,
but interpolation method fails at points \(x_j\) too close to the boundary \(Gamma\).\\
  
\section{Output functions}

\subsection{Print objects}

Most of the objects appearing in the user main program may be printed in a simple way to the screen or into
a file, using the output operator \verb|<<|.

\begin{lstlisting}
BilinearForm a=intg(omega, u*v); 
TermMatrix A(a,"'A");
compute(A);
TermVector Un(omega,u,1,"'U");
TermVector X=A*Un;
theCout << "A*un = " << X << eol;      //print to screen and to the file print.txt
thePrintStream << "A*un=" << X << eol; //print to the file print.txt
\end{lstlisting}

\var{thePrintStream} is a \xlifepp predefined object allowing to print into the file {\ttfamily print.txt} created in the current directory. {\ttfamily theCout} is an \xlifepp object allowing to print both to the screen and file {\ttfamily print.txt}.\\

See \autoref{ch.init} to learn how to manage the verbosity.

In order to print into a specific file, first create an \emph{ofstream} object, then print to it:

\begin{lstlisting}
std::ofstream out("myfile.dat"); // out is an ofstream object associated
                                 // with the file myfile.dat
out << "A*un=" << X << eol;      // print into this file
out.close();
\end{lstlisting}

\subsection{Export \classtitle{TermMatrix} and \classtitle{TermVector}}

We want to exploit easily the data contained in the objects produced during the computation. The objects concerned are mainly \class{TermVector} objects since \class{TermVector} is the type of nearly all the computation results of interest to the user. This may also concern \class{TermMatrix} objects if further postprocessing is envisaged.

As mentioned just above, printing objects using the \verb|<<| operator may produce big files containing a lot of information, generally used to control the different steps of the computation. But we often need to handle the data values outside \xlifepp, either in a raw format or in a specific one corresponding to some particular software.

The \cmd{saveToFile} commands have been designed for that purpose. They exist in two main forms: 
\begin{itemize}
\item member function : {\ttfamily Object.\cmd{saveToFile}("FileName", options);}
\item external function : {\tt\cmd{saveToFile}("FileName", Object, options);}
\end{itemize}

They behave slightly differently according to the kind of object they act on. Details follow:

\begin{description}
\item[\class{TermMatrix} object:] \( \)

\medskip

\begin{lstlisting}
BilinearForm a=intg(omega, u*v); 
TermMatrix A(a,"'A"); compute(A);
A.saveToFile("A.dat",_dense);       // dense format 
A.saveToFile("A.coo",_coo, true);   // coordinate format 
saveToFile("As.coo",A,_coo, true);  // works also (external function form)
\end{lstlisting}

Only two formats are available:

\begin{itemize}
\item Dense format (\_\textit{dense} option) : all the matrix coefficients are written, line by line;
\item Coordinate format (\_\textit{coo} option), which corresponds to the \matlab/\octave sparse format:
only nonzero matrix coefficients are written in the form \(\ i \quad j \quad a_{ij}\).
\end{itemize}

When the last argument is set to true (false by default), structure information are added to the file name.
In the previous example, the file name looks like  \verb|As(30_30_coo_real_scalar).coo|.\\

\item[\class{TermVector} object:] \( \)

\medskip

\begin{lstlisting}
TermVector U1, U2;
\ldots computation of U1 and U2 \ldots
U1.saveToFile("U1.dat");            // raw format only
U1.saveToFile("U1.dat", true);      // raw format, structure added to name
\end{lstlisting}

With the member function form, the only available output format is raw. To select another format, the external function form should be used:

\begin{lstlisting}
saveToFile("U1.dat",U1, _raw, true);// idem previous via external function

saveToFile("U.dat", U1, U2, _raw);  // two TermVectors in the same file
saveToFile("U.vtk", U1, U2, _vtk);  // idem, export in vtk format
saveToFile("U.vtu", U1, U2, _vtu);  // idem, export in vtu format
saveToFile("U.msh", U1, U2, _msh);  // idem, export in msh format
saveToFile("U.m", U1, U2, _matlab); // idem, export in Matlab/Octave format
saveToFile("U.dat", U1, U2, _xyzv); // idem, export nodes and values
\end{lstlisting}

Except the \emph{raw} format which outputs only data values, all formats embed mesh information. The
corresponding files are intended to be read by visualization software programs:

\begin{itemize}\addtolength{\itemsep}{-0.5\baselineskip}
\item \emph{vtk} and \emph{vtu} formats are compatible with \paraview,
\item \emph{msh} format is compatible with \gmsh,
\item \emph{matlab} format is compatible with \matlab and \octave,
\item \emph{xyzv} format produces ASCII files with \textit{x y z v1 v2 \ldots} on each line.
\end{itemize}

\begin{warningbox}
Because of the geometrical information involved, different TermVectors which are exported using one of those formats must be defined on the same space in order to be compared.\\
\end{warningbox}

\begin{ideabox}
A mesh can also be exported in \emph{vtk}, \emph{msh} or \emph{mel} format using a command bearing
the same name, \cmd{saveToFile}. This can be useful for conversion from one format to another, or for visualization purpose.

\begin{lstlisting}
Mesh demo = \ldots;
demo.printInfo();  // prints general information about the mesh to the screen

demo.saveToFile("demo.vtk", _vtk, true); // export the mesh in vtk format
demo.saveToFile("demo.msh", _msh);       // export the mesh in msh format
demo.saveToFile("demo.mel", _mel);       // export the mesh in melina format

// Idem with the external function form:
saveToFile("demo.vtk", demo, _vtk, true);
saveToFile("demo.msh", demo, _msh);
saveToFile("demo.mel", demo, _mel);
\end{lstlisting}
 
The last argument is optional ; its default value is false. For the \emph{vtk} and the \emph{msh} formats, if this argument is true, an individual file is created for each subdomain or boundary subdomain. In this case, each filename contains the name of the corresponding subdomain.
\end{ideabox}

\end{description}

\section{Graphical exploitation}\label{sec_graphicexpl}

By itself, \xlifepp is a finite element library and as such does not own any graphical possibilities.
At present, three main software programs are targeted through the ouput formats mentioned in the previous section.
\par\medskip
From a practical point of view, in order to obtain a graphical representation of the computation result,
one has to process the output file produced by the \cmd{saveToFile} command, by the corresponding software.
Minimal knowledge is required in order to use \gmsh and \paraview. We invite the user to refer to the
documentation of these software programs. Most of the computation results shown is this documentation are produced
by \paraview and most of the meshes are displayed using \gmsh.
\par\medskip
The \matlab/\octave format is intended to be (easily) used as follows. We assume we have a {\ttfamily .m} script
file, say {\ttfamily V\_1\_Omega.m} (produced by \cmd{saveToFile}, see above), containing the first vector
computed and related to the domain {\ttfamily Omega}
(maybe an eigenvector or the solution to a linear system, stored as a \class{TermVector} object in both cases).
One has to launch \matlab or \octave, eventually change to the directory containing the {\ttfamily .m} file and
type in 
\begin{lstlisting}[language=]
>> V_1_Omega
\end{lstlisting}
at the prompt (the execution of the script can also be achieved using the menus
if the GUI is available).
We thus automatically get one or several figures (one for each component of each unknown).
The user is then free to make further computations using the data present in memory (see below) or modify
the attributes of the figures.
\par\medskip
The curves gathered on the figure \ref{Sy_EV_png} are obtained via this procedure, by typing successively
{\ttfamily Sy\_1\_Omega}, \dots {\ttfamily Sy\_8\_Omega}. The corresponding mesh, based on the
interpolation nodes, is shown on figure \ref{Sy_1_OmegaFig1_png}. It is obtained by typing \par\smallskip
\begin{lstlisting}[language=]
>> opts=['mesh']; Sy_1_Omega
\end{lstlisting}

Notice that in this case, only the mesh is drawn and that the result would be the same with any of the other
files {\ttfamily Sy\_i\_Omega.m}.
\begin{figure}[H]
\begin{center}
\includePict[scale=0.4]{Sy_1_OmegaFig1.png}
\end{center}
\vspace{-1cm}
\caption{({\em A full example.}) Computational domain: segment \([0, \pi]\).}
\label{Sy_1_OmegaFig1_png}
\end{figure}
Another example with a 2D geometry is shown on figure \ref{eigs_2_OmegaFig1_png}.
\begin{figure}[H]
\begin{center}
\includePict[scale=0.4]{eigs_2_OmegaFig1.png}
\includePict[scale=0.4]{eigs_2_OmegaFig2.png}
\end{center}
\vspace{-1cm}
\caption{A 2D example.}
\label{eigs_2_OmegaFig1_png}
\end{figure}

\par\medskip
The script is not interactive except for volumic data: the visualization is made by slicing the 3D domain with
a plane and the user is invited to choose the cutting plane defined by a point and a vector normal to the plane,
shown by a red thick line on the mesh figure. The position of the cutting plane is updated on this figure as
its definition changes and the corresponding slices are displayed in other figures.
\par
Let's show what the command window looks like in such a case, here with \octave:

\begin{lstlisting}[language=]
GNU Octave, version 4.0.3
Copyright (C) 2016 John W. Eaton and others.
This is free software; see the source code for copying conditions.
 \ldots

>> what
M-files in directory /tmp/demo/xlifepp:

   eigs_1_Omega.m
>>
>> eigs_1_Omega
The intersection plane is defined by the point P and the orthogonal vector V.

Current value of P = 12.566      12.566      12.566
Current value of V = 1  -1   0
  1 : change P and draw
  2 : change V and draw
  3 : draw without change
  0 : quit
Your choice (other value = no change) : 3
Value range for unknown 1 : Real part = [-8.9881e-20      0.10878]   Imag part = [0 0]

Current value of P = 12.566      12.566      12.566
Current value of V = 1  -1   0
  1 : change P and draw
  2 : change V and draw
  3 : draw without change
  0 : quit
Your choice (other value = no change) : 0
Tuning suggestions:
 figure(N)
 subplot(2,2,k)
 rotate3d,   grid,   box
 view(2),  view(3),  view([Nx Ny Nz])
 shading faceted
 axis equal,  axis normal
 set(gca,'xtick',[])       or  set(gca,'xtick',[\ldots])
 caxis([\ldots])
 xlabel(\ldots), ylabel(\ldots), title(\ldots)
 print('-dpng',eigs_1_OmegaFig2.png)
>>
\end{lstlisting}

Before the user types in {\ttfamily 3} to answer the first question, the figure \ref{eigs_1_OmegaFig1_png} has been displayed, showing the domain mesh and an initial cutting plane. By choosing the value {\ttfamily 3}, the user accepts the current settings and the figure \ref{eigs_1_OmegaFig2_png} is drawn. The user then terminates by answering {\ttfamily 0}, and some hints to modify the aspect of the figures are displayed.

\begin{figure}[H]
\centering
\includePict[scale=0.6]{eigs_1_OmegaFig1.png}
\caption{Computational domain Omega.}
\label{eigs_1_OmegaFig1_png}
\end{figure}

\begin{figure}[H]
\centering
\includePict[scale=0.6]{eigs_1_OmegaFig2.png}
\caption{Data representation corresponding to the selected slice.}
\label{eigs_1_OmegaFig2_png}
\end{figure}
When the script has terminated, we can observe the variables present in the workspace:

\begin{lstlisting}[language=]
>> whos
Variables in the current scope:

   Attr Name            Size                     Bytes  Class
   ==== ====            ====                     =====  =====
        coord        5859x3                     140616  double
        domaindim       1x1                          8  double
        domainname      1x5                          5  char
        elem         4832x8                     309248  double
        elemtype     4832x1                      38656  double
        interpDeg       1x1                          8  double
        opts            0x0                          0  char
        spacedim        1x1                          8  double
        unknown         1x8                          8  char
        val          5859x1                      46872  double

Total is 66940 elements using 535429 bytes

>> quit
\end{lstlisting}

This allows the user to make any processing of his own with these data. The definitions of the
variables are the following:

\begin{itemize}
\def\hfb{\hfill\break}
\item {\ttfamily coord} (real)\hfb
    Coordinates of the interpolation nodes, one node per row.
    The nodes are implicitly numbered from 1 to {\ttfamily size(coord,1)}.
\item {\ttfamily domaindim} (integer)\hfb
    Dimension of the domain (1, 2 or 3).
\item {\ttfamily domainname} (string)\hfb
    Name of the domain.
\item {\ttfamily elem} (integer)\hfb
    Array containing the lists of elements: {\ttfamily elem(i, :)} is an element of type {\ttfamily elemtype(i)}.
    Format of the array: one element per row, column {\ttfamily i} holds the i-th
    interpolation node, given by its number in the array {\ttfamily coord} above.
\item {\ttfamily elemtype} (integer)\hfb
    Vector containing the type of each element present in the mesh, in the same order
    as the array {\ttfamily elem} above.
    Each type is a code number in \xlifepp's internal codification:\hfb
     2 = point, 3 = segment, 4 = triangle, 5 = quadrangle,\hfb
     6 = tetrahedron, 7 = hexahedron, 8 = prism, 9 = pyramid.
\item {\ttfamily interpDeg} (integer)\hfb
    Interpolation degree used during the computation.
\item {\ttfamily opts} (string)\hfb
    1-column array containing options given as keywords, one keyword per row. This allows
    to relaunch a script file, and thus get a new figure according to the chosen options.
    Available options are:\hfb
    \hglue15pt - {\ttfamily 'chsign'} : allows drawing values with opposite sign\hfb
    \hglue15pt - {\ttfamily 'mesh'}   : draws only the mesh (no other graph is drawn)\hfb
    \hglue15pt - {\ttfamily 'silent'} : suppresses display of graphical tuning hints
\item {\ttfamily spacedim} (integer)\hfb
    Dimension of the space (1, 2 or 3).
\item {\ttfamily unknown} (string)\hfb
    1-column array containing the name of the unknowns, or their components in the vector
    case, one name per row.
\item {\ttfamily val} (real or complex)\hfb
    Values corresponding to the unknowns, stored column-wise, one column per component unknown.
    Each row contains the value at a node, or in an element if the interpolation degree is 0,
    in the same order as the array {\ttfamily coord} or {\ttfamily elem} respectively.
    Indeed, the graphical function 'patch' used, makes the correspondence according to the number
    of rows of this array, which should be {\ttfamily size(coord, 1)} or {\ttfamily size(elem, 1)} respectively.
\end{itemize}

The variables {\ttfamily domaindim}, {\ttfamily domainname}, {\ttfamily interpDeg} and {\ttfamily spacedim} are defined
for information or consistency check purpose. Indeed, {\ttfamily spacedim} should equal {\ttfamily size(coord,2)}
and {\ttfamily domaindim} should be less or equal {\ttfamily spacedim} and be consistent with the element types.
