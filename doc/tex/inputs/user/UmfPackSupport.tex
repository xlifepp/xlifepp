%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\subsection{Interface with \umfpack library}

\umfpack is a set of routines for solving non-symmetric sparse linear systems, \(Ax=b\), using the Unsymmetric MultiFrontal method. Well-known for the performance, it's widely used in research as well as industry. The \lib{umfpackSupport} lib is an interface to \umfpack, allows user to take advantage of \umfpack in the context of \xlifepp

\subsubsection{Basic Usage}

The goal of the interface between \xlifepp and \umfpack is to allow user to take advantage of \umfpack without considering its complexities in the context of \xlifepp. Instead of calling multiple complicated C routines, user can invoke the \umfpack solver for the linear system \(Ax=b\) in the same way as other built-in iterative solvers of \xlifepp. The class presenting this interface is \class{UmfPackSolver}
 \vspace{.1cm}
\begin{lstlisting}
UmfPackSolver umfpackSolver;
\end{lstlisting}
\vspace{.1cm}
For example, to solve the linear system \(Ax=b\) with: A is a largematrix, b and x are std::vector:
\vspace{.1cm}
\begin{lstlisting}
UmfPackSolver umfpackSolver;
LargeMatrix<Real> A(dataPathTo("matrix3x3Sym.data"), _dense, 3,3,_dense,_row);
std::vector<Real> b(3), x(3);
umfpackSolver(A,b,x);
std::cout << "Matrix A is " << A << std::endl;
std::cout << "Vector b is " << b << std::endl;
std::cout << "Vector x is " << x << std::endl;
\end{lstlisting}
\vspace{.1cm}
Moreover, instead of using largeMatrix A, user can provide directly a compressed sparse column matrix with three std::vector (val, row\_ind, col\_ptr) where val is an array of the (top-to-bottom, then left-to-right-bottom) nonzero values of the matrix; row\_ind is the row indices corresponding to the values; and, col\_ptr is the list of val indexes where each column starts.
\vspace{.1cm}
\begin{lstlisting}
UmfPackSolver umfpackSolver;
Real valsArr[] = {8,2,9,2,9,4,9,4,20};
std::vector<Real> vals(valsArr, valsArr + sizeof(valsArr) / sizeof(Real) );
int rowIdxArr[] = {0,1,2,0,1,2,0,1,2};
std::vector<int> rowIdx(rowIdxArr, rowIdxArr + sizeof(rowIdxArr) / sizeof(int) );
int colPtrArr[] = {0,3,6,9};
std::vector<int> colPtr(colPtrArr, colPtrArr + sizeof(colPtrArr) / sizeof(int) );
std::vector<Real> b(3), x(3);
umfpackSolver(colPtr, rowIdx, vals, b, x);
std::cout << "Vector b is " << b << std::endl;
std::cout << "Vector x is " << x << std::endl;
\end{lstlisting}
\vspace{.1cm}
Another way is to extract the storage of the largeMatrix A and convert them to compressed sparse column matrix with three components
\vspace{.1cm}
\begin{lstlisting}
UmfPackSolver umfpackSolver;
LargeMatrix<Real> A(dataPathTo("matrix3x3Sym.data"), _dense, 3,3,_dense,_row);
std::vector<Real> vals;
std::vector<int> rowIdx, colPtr;
A.extract2UmfPack(colPtr, rowIdx, vals);
std::vector<Real> b(3), x(3);
umfpackSolver(colPtr, rowIdx, vals, b, x);
std::cout << "Vector b is " << b << std::endl;
std::cout << "Vector x is " << x << std::endl;
\end{lstlisting}
\vspace{.1cm}

\subsubsection{Advanced Usage}

There are some cases where we may wish to do more with the LU factorization of a matrix than solve a linear system then it's the coming of \class{UmfPackLU}. This class allows not only to solve a linear system but also extract the LU factorization of a matrix. Remind that the LU factorization of a matrix has form: \(P(R)AQ=LU\) where L is lower triangular matrix, U is upper triangular matrix, P and Q are permutation vectors and R is the scale factor.\\
Because the \class{UmfPackLU} class targets sparse matrix which is presented by \class{largeMatrix} class of \xlifepp, the expected template argument for the \class{UmfPackLU} is \class{largeMatrix}. By using the \class{UmfPackLU}, we can factorize a matrix: one by constructor
\vspace{.1cm}
\begin{lstlisting}
LargeMatrix<Real> A(dataPathTo("matrix3x3Sym.data"), _dense, 3,3,_dense,_row);
UmfPackLU<LargeMatrix<Real> > umfLUReal(A);
\end{lstlisting}
\vspace{.1cm}
or by method {\bfseries compute}
\vspace{.1cm}
\begin{lstlisting}
LargeMatrix<Real> A(dataPathTo("matrix3x3Sym.data"), _dense, 3,3,_dense,_row);
UmfPackLU<LargeMatrix<Real> > umfLUReal;
umfLUReal.compute(A);
\end{lstlisting}
\vspace{.1cm}
After that, we can extract all objects relating to the LU factorization. The matrix L is returned in compressed row form (with the column indices in each row sorted in ascending order). The matrix U is compressed column form (with sorted column). These two matrix are returned in a struct which \class{UmfPackLU} contains:
\vspace{.1cm}
\begin{lstlisting}
    struct LUMatrixType {
        std::vector<int> outerIdx;
        std::vector<int> innerIdx;
        std::vector<ScalarType> values;
    };
\end{lstlisting}
\vspace{.1cm}
where the {\itshape innerIdx} is the row (column) indices corresponding to the values in case of lower (upper) triangular matrix; and, {\itshape outerIdx} is the list of val indexes where each column (row) starts, {\itshape values} contains nonzero values of matrix. \\
The permutations \(P\) and \(Q\) are represented as permutation vectors, where \(P[k] = i\) means that row i of the original matrix is the k-th row of \(PAQ\), and where \(Q[k] = j\) means that column j of the original matrix is the k-th column of \(PAQ\).
\vspace{.1cm}
\begin{lstlisting}
inline const std::vector<int>& permutationP() const;
inline const std::vector<int>& permutationQ() const;
\end{lstlisting}
\vspace{.1cm}
The vector R is the scale factor in the LU factorization. The first value of {\itshape scaleFactorR} defines how the scale factors Rs are to be interpreted. If it's one(1), then the scale factors {\itshape scaleFactorR[i+1]} are to be used by multiplying row i of matrix A by {\itshape scaleFactorR[i]}. Otherwise, the entries in row i are to be divided by {\itshape scaleFactorR[i+1]}.

