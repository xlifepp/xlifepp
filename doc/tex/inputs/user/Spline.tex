%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lun\'eville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex

\section{Splines}\label{s.spline}

Generally speaking, a spline is a piecewise polynomial curve that approaches, in a sense to be specified, an ordered list of points. The simplest one is the spline of degree 1 (linear spline) which connects the points by segments. \xlifepp provides the Hermite cubic spline (C2), the Catmull-Row spline (C1), the B-spline and rational B-Spline (Ck) and the Bézier curve (Ck). The two first ones are  interpolation splines while the last ones are approximation spline. Combining two rational B-splines in two directions gives a surface approximation, named NURBS acronym of Non-Uniform Rational B-Spline.

\subsection{C2 spline}\label{ss.c2spline}

Let \((t_i)_{0\leq i\leq n}\) and  \((y_i)_{0\leq i\leq n}\). There exists a unique \(C^2\) piecewise cubic polynomial  such that \(q(t_i)=y_i \text{ for } {0\leq i\leq n}\) and \(q'(t_0)=y'_0,\ q'(t_n)=y'_n\). Each cubic polynomial may be constructed from the knowledge of \(b_i=q'(t_i),\ 0\leq i\leq n\) which is the solution of the following linear system
(\( h_i=t_i-t_{i-1},\ h_{i,j}=2(h_i+h_j) \)):

\small
\[
\left[ 
\begin{array}{ccccccc}
1   & 0       &         &             &        &           &         \\
h_2 & h_{1,2} & h_1     &             &        &           &         \\
    & \ddots  & \ddots  & \ddots      &        &           &         \\
    &         & h_{i+1} & h_{i,i+1}   & h_i    &           &         \\
    &         &         & \ddots      & \ddots & \ddots    &         \\
    &         &         &             & h_n    & h_{n-1,n} & h_{n-1} \\
    &         &         &             &        & 0         & 1       \\
\end{array}
\right]
\cdot
\left[ 
\begin{array}{c}
b_0 \\
b_1	\\
\vdots \\
b_i \\
\vdots \\
b_{n-1} \\
b_{n}
\end{array}
\right]
=
\left[ 
\begin{array}{c}
y'_0 \\
\dfrac{3}{h_1h_2}\left( h_1^2(y_2-y_1) +h^2_2(y_1-y_0)\right) \\
\vdots \\
\dfrac{3}{h_ih_{i+1}}\left( h_i^2(y_{i+1}-y_i)+h_{i+1}^2(y_i-y_{i-1})\right) \\
\vdots \\
\dfrac{3}{h_{n-1}h_{n}}\left( h_{n-1}^2(y_{n}-y_{n-1})+h_{n}^2(y_{n-1}-y_{n-2})\right) \\
y'_n
\end{array}
\right]
\]
\normalsize

The spline is said to be {\bfseries clamped} because derivatives at ends are imposed.

\begin{center}
\includePict[width=12cm,trim={0 6cm 0 0},clip]{clamped_spline.png}
\end{center}

Other boundary conditions are usual : \(q''_1(t_0)=q''_{n}(t_n)=0\) leading to {\bfseries natural} spline 

\begin{center}
\includePict[width=12cm,trim={0 6cm 0 0},clip]{natural_spline.png}
\end{center}

or periodicity conditions : \(q'_1(t_0)=q'_n(t_n)\) and \(q''_1(t_0)=q''_n(t_n)\) leading to {\bfseries periodic} spline. 

\begin{center}
\includePict[width=12cm,trim={0 6cm 0 0},clip]{periodic_spline.png}
\end{center}

To interpolate any set of points  \((P_i)_{0\leq i\leq n}\) in \(\mathbb{R}^2\) or \(\mathbb{R}^3\), the previous method is extended as following. Let  \(T=(t_i)_{0\leq i\leq n}\) a set of parameters (knots) and a set of scalar values \(Y =(y_i)_{0\leq i\leq n}\), we denote \(\mathcal{S}_{T,Y}\) the spline function related to the sets \(T,Y\). The C2 spline related to the set of points \((P_i)\) is defined as:

\[
Q(t)=\left(\mathcal{S}_{T,P1}(t), \mathcal{S}_{T,P^2}(t), \mathcal{S}_{T,P^3}(t)\right)
\]

where \(P^k=(P_i^k)_{i=0,n}\) with \(P_i^k \) the \(k^{th}\) coordinate of the point \(P_i\).\\

The usual choices of parametrization are 

\begin{itemize}
    \setlength\itemsep{-2pt}
\item The x-parametrization: \(t_i=P_i^1,\ 0\leq i \leq n\) (gives the original C2 spline);
\item The uniform parametrization: \(t_i=i,\ 0\leq i \leq n\);
\item The chordal parametrization: \(t_0=0,\  t_{i}= t_{i-1}+\left\|P_{i}-P_{i-1}\right\|, \  1\leq i \leq n\);
\item The centripetal parametrization: \(t_0=0,\  t_{i}= t_{i-1}+\left\|P_{i}-P_{i-1}\right\|^{1/2}, \  1\leq i \leq n\).
\end{itemize}

\begin{focusbox}
To make the user life easier, the parameter is pullback to the interval \([0,1]\). In other words, user addresses the spline parametrization using \(s\in [0,1]\) that is mapped to \(t=t_0+(1-s)t_n\) where \(t_0\), \(t_n\) are the spline parameter bounds . All splines work in the same way. 
\end{focusbox}

\xlifepp provides the \class{C2Spline} class. There are two ways to construct a C2 spline, either by giving two  vectors of reals \((T,Y)\) (classical C2 spline) or giving a vector of points \(P\) (extended C2 spline). The following optional parameters may be given at construction:

\begin{itemize}\setlength\itemsep{-2pt}
\item the type of parametrization : one of {\bfseries \_xParametrization},  \_uniformParametrization, \_chordalParametrization, \_centripetalParametrization
\item the boundary conditions : two of  \_naturalBC, \_clampedBC, \_periodicBC
\item the derivatives at end points (vector of reals) required when using clamped conditions
\end{itemize}

\begin{lstlisting}
Number n=5;
Real x=0, dx=pi_/n;
vector<Point> points(n+1);
for(Number i=0;i<=n;i++, x+=dx) points[i]=Point(x,sin(x));
C2Spline csn(points,_xParametrization);             //natural spline
C2Spline csc(points,_xParametrization,_clampedBC,_clampedBC,
             Reals(1.,1.),Reals(1.,-1.));           //clamped spline
C2Spline csp(points,_xParametrization,_periodicBC); //periodic spline
\end{lstlisting}

\begin{center}
\includePict[width=12cm]{C2Spline_Xlife.png}
\end{center}

\begin{warningbox}
	When \_xParametrization is selected, the curve is regarded as \(x \rightarrow y(x)\) curve and the returned "values"  are 1D points. In contrast, when another parametrization is selected, the curve is regarded as \(t\rightarrow (x(t),y(t))\)
	and  the returned "values" are 2D points.
\end{warningbox}

\begin{warningbox}
	Be cautious with periodic conditions. For a periodic open curve (e.g. \(\sin x\) on \([0,2\pi]\)) use only the \_xParametrization mode and for a closed periodic curve use other parametrization mode.
\end{warningbox}

Once a \class{C2Spline} object is constructed, using either the function \var{evaluate} or the operator \verb|()|, it can be evaluated at any parameter \(t\in [0,1]\).\vspace{3mm}
\begin{lstlisting}
...
C2Spline csn(points,_xParametrization); 
Real t=0.5;                   //midle parameter
Point P=csn(t);               //value
Point dP=csn(t,_dt);          //first derivative  (vector as a point)
Point d2P=csn(t,_dt2);        //second derivative (vector as a point)
\end{lstlisting}
\vspace{3mm}
\class{C2Spline} allocates also a \class{Parametrization} object related to the spline parametrization. Using this object, other quantities such as curvilinear abscissa, normal or tangent vector, curvatures are available (see \class{Parametrization} class):\vspace{3mm}
\begin{lstlisting}
...
C2Spline csn(points,_xParametrization); 
Real t=0.5;   //midle parameter
const Parametrization& pa=csn.parametrization();
Real  c=pa.curvature(t);
Real  s=pa.curabc(t);
Reals no=pa.normal(t);
Reals ta=pa.tangent(t);
\end{lstlisting}

\subsection{Catmull-Rom spline}\label{ss.catmullromspline}

Another way to construct interpolation spline consists in imposing also the tangent vectors \((T_i)\) at control points \(P_i\). Several methods have been proposed and the Catmull-Rom method is one of them. On the interval \([t_i,t_{i+1}]\) the spline \(Q\) is defined by

\[
Q(t)=
\left[ 
\begin{array}{cccc}
 1 &t &t^2 &t^3
 \end{array}
 \right]
\left[ 
\begin{array}{cccc}
   0    &    1     &    0      &    0    \\
-\alpha &    0     &  \alpha   &    0    \\
2\alpha & \alpha-3 & 3-2\alpha & -\alpha \\
-\alpha & 2-\alpha & \alpha -2 & \alpha
\end{array}
\right]
\left[ 
\begin{array}{c}
P_{i-1} \\
P_i \\
P_{i+1} \\
P_{i+2}
\end{array}
\right]
\]

where \(\alpha\) is a tension parameter which is related to the underlying parametrization. The standard choices of \(\alpha\) are:
\begin{itemize} \setlength\itemsep{-2pt}
\item \(\alpha=0\) : the standard Catmull-Rom spline,
\item \(\alpha=1\) : the chordal Catmull-Rom spline,
\item \(\alpha=\frac{1}{2}\) : the centripetal Catmull-Rom spline.
\end{itemize}

The Catmull-Rom spline are local (moving a point modifies only the curve in the neighborhood of the point), the approximation is \(C^1\) but not \(C^2\), the computation is fast. The centripetal Catmull-Rom spline has additional properties :  there is no self-intersection, cusp will never occur, and it follows the control points in a better way.\\

Note that to close a curve, it is sufficient to add the two last points at the beginning and the two first points at the end:

\[
(P_1\ P_2 \ P_3 \ \dots P_{n-2}\  P_{n-1}\ P_{n})\longrightarrow ( P_{n-1}\  P_{n}\ \ P_1\ P_2 \ P_3 \ \dots P_{n-2}\ P_{n-1}\  P_{n}\ \ P_1\ P_2)
\]

and build Catmull-Rom spline on segments \([P_n,P_1],\ [P_1,P_2],\ \dots,\ [P_{n-2},P_{n-1}],\ [P_{n-1},P_{n}]\).\\

On the next figure, the influence of the parameter \(\alpha\) may be observed; the value \(\alpha=0.5\) giving the best result.

\begin{center}
\includePict[width=10cm]{CatmullRom_spline.png}
\end{center}

\xlifepp provides the \class{CatmullRomSpline} class. It has only one constructor from a vector of points \(P\)  and optional parameters are available:
\begin{itemize}\setlength\itemsep{-2pt}
\item the type of parametrization : one of { \_xParametrization},  \_uniformParametrization, {\bf\_chordalParametrization}, \_centripetalParametrization
\item the tension parameter \(\alpha\) (default value : 0.5)
\item the boundary conditions : two of  \_undefBC, {\bfseries \_naturalBC}, \_clampedBC, \_periodicBC
\item the tangent vectors at end points (vector of reals) required when using clamped conditions
\end{itemize}

\begin{lstlisting}
Number n=6;
vector<Point> pts(ns+1);
Real s=0, ds=2*pi_/ns;
for(Number i=0;i<=ns;i++,s+=ds) pts[i]=Point(2*cos(s),sin(s)); //points on ellipse
CatmullRomSpline cat(pts); 
\end{lstlisting}

In this example, as the last point is the same as the first point, the curve will be automatically closed.

\begin{center}
\includePict[width=12cm]{CatmullRom_per_Xlife.png}
\end{center}

Once a \class{CatmullRomSpline} object is constructed, using either the function \var{evaluate} or the operator \verb|()|, it can be evaluated at any parameter \(t\) in \([0,1]\).\\

\begin{lstlisting}
\ldots
CatmullRomSpline cat(pts); 
Real t=0.5                    //midle parameter
Point P=cat(t);               //value
Point dP=cat(t,_dt);          //first derivative  (vector as a point)
Point d2P=cat(t,_dt2);        //second derivative (vector as a point)
\end{lstlisting}

\class{CatmullRomSpline} allocates also a \class{Parametrization} object related to the spline parametrization. Using this object, other quantities such as curvilinear abscissa, normal or tangent vector, curvatures are available (see \class{Parametrization} class):\\

\begin{lstlisting}
\ldots
CatmullRomSpline cat(pts); 
Real t=0.5                    //midle parameter
const Parametrization& pa=cat.parametrization();
Real  c=pa.curvature(t);
Real  s=pa.curabc(t);
Reals no=pa.normal(t);
Reals ta=pa.tangent(t);
\end{lstlisting}

\subsection{Bézier curve} \label{ss.bezierspline}

For the set of control points \((P_i)_{0\leq i\leq n}\), the {\bfseries Bézier curve} is defined by

\[
Q(t)=\sum_{i=0}^n B^n_i(t)P_i\ \ \text{for } t\in[0,1]
\]

where \(B^n_i=C_n^i\,t^i(1-t)^{n-i}\) are the Bernstein polynomials (\(\sum_{i=0}^n B^n_i=1\)). Note that the degree of polynomials is equal to the number of points minus 1. There are the following properties
\begin{itemize}\setlength\itemsep{-2pt}
\item \(Q(0)=P_0\) and \(Q(1)=P_n\), but the curve does not interpolate the interior points \((P_1,\dots,P_{n-1})\),
\item \(\overrightarrow{P_0P_1}\) (resp. \(\overrightarrow{P_{n-1}P_n}\)) is a tangent vector to the curve at \(P_0\) (resp. \(P_n\)),
\item the curve is inside the convex hull of the control points,
\item the curve is \(C^\infty\).
\end{itemize} 

\begin{center}
\includePict[width=6cm]{Bezier_curve.png}
\end{center}

\xlifepp provides the \class{BezierSpline} class with only one simple constructor from a vector of points \(P\):\\

\begin{lstlisting}
Number n=5;
vector<Point> points(n+1);
Real x=0, dx=pi_/n;
for(Number i=0;i<=n;i++, x+=dx) points[i]=Point(x,sin(x));
BezierSpline bz(points);
\end{lstlisting}

The parameter \(t\) lives always in the interval \([0,1]\).

\begin{center}
\includePict[width=12cm]{BezierSpline.png}
\end{center} 
 
Once a \class{BezierSpline} object is constructed, using either the function \var{evaluate} or the operator \verb|()|, it can be evaluated at any parameter \(t\in[0,1]\):\\

\begin{lstlisting}
\ldots
BezierSpline bz(points);
Real t=0.5;                  //midle parameter
Point P=bz(t);               //value
Point dP=bz(t,_dt);          //first derivative  (vector as a point)
Point d2P=bz(t,_dt2);        //second derivative (vector as a point)
\end{lstlisting}

\class{BezierSpline} allocates also a \class{Parametrization} object related to the spline parametrization. Using this object, other quantities such as curvilinear abscissa, normal or tangent vector, curvatures are available (see \class{Parametrization} class):\\

\begin{lstlisting}
\ldots
BezierSpline bz(points);
Real t=0.5;                   
const Parametrization& pa=bz.parametrization();
Real  c=pa.curvature(t);
Real  s=pa.curabc(t);
Reals no=pa.normal(t);
Reals ta=pa.tangent(t);
\end{lstlisting}

\subsection{B-Spline}\label{ss.bspline}

The {\bfseries B-spline} curve is a generalization of the Bézier curve. Let \(t_0\leq t_1\ \dots \leq t_m\) a set of \(m+1\) knots and the B-spline functions of degree \(k\) defined by recurrence:

\[
\begin{array}{cl}
\text{for }0\leq i\leq m- 1 & B_{i,0}(t)=\left\{\begin{array}{ll} 
1 & t_i\leq t < t_{i+1}\\
0 & \text{else}
\end{array}\right.\\
\text{for } k\geq 1, \  0\leq i\leq m-k-1 &  B_{i,k}(t)=\dfrac{t-t_i}{t_{i+k}-t_i}B_{i,k-1}(t)+\dfrac{t_{i+k+1}-t}{t_{i+k+1}-t_{i+1}}B_{i+1,k-1}(t)
\end{array}
\]

with the convention \(\dfrac{\bullet}{0}=0\).\\

The B-spline functions have a lot of properties, in particular

\begin{itemize}\setlength\itemsep{-1pt}
\item \(B_{i,k}\) is a polynomial of order \(k\) on intervals \([t_j,t_{j+1}]\) with support \([t_i,t_{i+k+1}]\),
\item \(0<B_{i,k}(t)<1\) for \(t\in ]t_i,t_{i+k+1}]\),
\item \(B_{i,k}\) is \(C^\infty\) at right and is \(C^{k-r}\) at knots of multiplicity \(r\)
\end{itemize}

For the set of \(n+1\) control points \(P_0,P_1,\dots, P_{n}\) and the set of knots \( t_0\leq t_1\ \dots \leq t_m\) with \(m\geq n+k+1\), the B-spline curve is defined by

\[
Q(t)=\sum_{i=0}^{n} B_{i,k}(t)P_i\ \ \text{for } t_k\leq t\leq t_{n+1}.
\]

The B-spline curve is inside the convex hull of the control points, it is \(C^{k-1}\) if all the knots are of multiplicity 1,  moving a point \(P_i\) induces a modification of the points related to the interval \([t_i,t_{i+k}]\). A Bézier curve is a B-Spline with a  knots vector of the form [0,0,\dots 0,1,1,\dots 1].\\

To clamp the curve at \(P_0\) choose \(t_0=t_1\dots=t_k<t_{k+1}\) and to close the curve duplicate the \(k+1\) first points at the end of the set of points.\\

\(\bullet\) Clamped B-spline of degree 3 with 6 control points and knots [0,0,0,0,1,2,3,3,3,3] plotted on \([0,3]\):

\begin{center}
\includePict[width=9cm]{B_Spline3_clamped.png}
\end{center}

A rational B-spline is defined as following:

\[
Q(t)=\frac{\displaystyle\sum_{i=0}^{n} \omega_iB_{i,k}(t)P_i}{\displaystyle\sum_{i=0}^{n} \omega_i B_{i,k}(t)}\ \ \text{for } t_k\leq t\leq t_{n+1}
\]

where \((\omega_i)_{i=0,n}\) are some weights. When the weight \(\omega_i\) is large, the curve goes to the control point \(P_i\). When all the weights are equal to 1, the rational B-spline coincides with the B-spline. 

\begin{center}
\includePict[width=12cm,trim={0 11cm 0 0},clip]{rationalBspline.png}
\end{center}

If \(t_0=t_1=\dots=t_k<t_{k+1}\) and \(P_0\neq P_1\) then

\[
\begin{array}{l}
Q(t_k)=P_0,\\
Q'(t_k)=\displaystyle \frac{k}{t_{k+1}-t_k}\,\frac{w_1}{w_0}(P_1-P_0),\\
Q'(t_k)\times Q''(t_k)=\displaystyle \frac{k^2(k-1)}{(t_{k+1}-t_k)^2(t_{k+2}-t_k)}\,\frac{w_1w_2}{w_0^2}(P_1-P_0)\times(P_2-P_0),\\[5mm]
\kappa = \displaystyle \frac{k-1}{k}\frac{t_{k+1}-t_k}{t_{k+2}-t_k}\,\frac{w_1w_2}{w_0^2}\frac{2A}{c^3} \ \ \ \textrm{(curvature)}
\end{array}
\]

with \(A\) the surface of the triangle \((P_0,P_1,P_2)\) and \(c=||P_1-P_0||\). That gives a way to match the rational B-spline to another curve up to \(C^2\).\\

By solving a linear inverse problem, the control points may be calculated in order to interpolate a given set of points. In that case, the spline is named interpolation B-spline. For such spline, it is possible to impose the derivative vectors at the endpoints when the spline is clamped.\\

\xlifepp provides the \class{BSpline} class with constructors that takes a vector of points \(P\) and some optional data:
\begin{itemize}\setlength\itemsep{-2pt}
\item the type of B-spline, either {\bfseries \_splineApproximation} or {\_splineInterpolation}
\item the degree of B-spline, default value 3
\item the type of parametrization : one of {\bfseries \_uniformParametrization}, \_chordalParametrization, \_centripetalParametrization
\item the boundary conditions : two of  {\bfseries \_undefBC}, \_naturalBC, \_clampedBC, \_periodicBC
\item derivative vectors at end points when clamped B-spline is selected
\item the vector of weights (default 1)
\end{itemize}

The given vector of points is either the list of control points (approximation B-spline) or the list of interpolation points (interpolation B-spline). When the type of B-spline is not given, it is assumed to be an approximation B-spline:\\

\begin{lstlisting}
Number n=6;
vector<Point> pts(n+1);
Real s=0, ds=2*pi_/n;
for(Number i=0;i<=n;i++,s+=ds) pts[i]=Point(2*cos(s),sin(s)); //points on ellipse
BSpline bs(points,3,_periodicBC);
\end{lstlisting}

Specifying a periodic condition at starting point stands obviously for periodic condition at ending point!\\
 
To use some weights, define a vector of weights of the size of vector of control points:\\

\begin{lstlisting}
vector<real_t> we(pts.size(),1.);
for(Number k=0;k<we.size();k+=2) we[k]=0.5;
BSpline bsw(pts,3,_periodicBC,_periodicBC,we);
\end{lstlisting}

Because of the signature of the constructor, both boundary conditions have to be specified even they are redundant!

\begin{center}
\includePict[width=12cm]{B_Spline_periodic_xlife.png}
\end{center} 

Once a \class{BSpline} object is constructed, using either the function \var{evaluate} or the operator \verb|()|, it can be evaluated at any parameter \(t\in[0,1]\):\\

\begin{lstlisting}
\ldots
BSpline bs(points,3,_periodicBC);
Real t=0.5;                   //midle parameter
Point P=bs(t);                //value
Point dP=bs(t,_dt);           //first derivative  (vector as a point)
Point d2P=bs(t,_dt2);         //second derivative (vector as a point)
\end{lstlisting}

\class{BSpline} allocates also a \class{Parametrization} object related to the spline parametrization. Using this object, other quantities such as curvilinear abscissa, normal or tangent vector, curvatures are available (see \class{Parametrization} class):\\

\begin{lstlisting}
\ldots
BSpline bs(points,3,_periodicBC);
Real t=0.5;                   //midle parameter
const Parametrization& pa=bs.parametrization();
Real  c=pa.curvature(t);
Real  s=pa.curabc(t);
Reals no=pa.normal(t);
Reals ta=pa.tangent(t);
\end{lstlisting}

The following example shows how to build a clamped interpolation B-spline with a uniform parametrization from 5 points located on the sine curve:\\

\begin{lstlisting}
\ldots
vector<Point> pts(5,Point(0.,0.));
pts[1]=Point(pi_/4,sqrt(2)/2.);pts[2]=Point(pi_/2,1.);
pts[3]=Point(3*pi_/4,sqrt(2)/2.);pts[4]=Point(pi_,0.);
Reals d0(2,1.),d1=d0; d1[1]=-1.;  //derivatives at end points
BSpline bs(_SplineInterpolation, pts, 3,_uniformParametrization, 
           _clampedBC,_clampedBC,d0,d1);
\end{lstlisting}

\begin{center}
\includePict[width=12cm]{interpolation_B_spline_clamped.png}
\end{center}
When specifying only {\_splineInterpolation} and a set of points in constructor, the B-spline will be a natural interpolation B-spline of degree 3  with a uniform parametrization (not rational).  

\subsection{Spline surface (nurbs)}\label{ss.nurbs}

The most common method to approximate surface are NURBS (non-uniform rational B-spline) that are no more than the cross-product of two rational B-splines. Let 
\begin{itemize}\setlength\itemsep{-2pt}
\item a set of \((m+1) \times (n+1)\) control points \(P_{ij}\), \(0\leq i\leq m\) and \(0\leq j\leq n\)
\item a knot vector in \(u\)-direction and  \(v\)-direction : \(U=[u_0,u_1,\dots u_k]\) and \(V=[v_0,v_1,\dots v_\ell]\),
\item the degree \(p\) in \(u\)-direction and the degree \(q\) in \(v\)-direction
\item \(k=m+p+1\) and \(\ell=n+q+1\)
\end{itemize}

The NURBS surface is defined by

\[
(u,v) \longrightarrow Q(u,v)=\displaystyle \frac{\displaystyle\sum_{i=0}^m\sum_{j=0}^n\omega_{ij}B_{i,p}(u)\,B_{j,q}(v)P_{ij}}
{\displaystyle\sum_{i=0}^m\sum_{j=0}^n\omega_{ij}B_{i,p}(u)\,B_{j,q}(v)}.
\]

To deal with nurbs, \xlifepp provides the \class{Nurbs} class. There is only one constructor from a vector of vectors of points (control points) and optional parameters:

\begin{itemize}
\item the degrees of B-spline along \(u\) /\(v\) parameter (default is 3)
\item the boundary conditions along \(u\) /\(v\) parameter, for each parameter, two of \_naturalBC, {\bfseries \_clampedBC}, \_periodicBC
\item the vector of vectors of weights (default no weight)
\end{itemize}

\begin{lstlisting}
Number n=4;
Real ds=pi_/(2*n), u=-pi_/2, v=0;
PointMatrix pts(2*n+1,Points(n+1));
for(Number i=0;i<=2*n;i++,u+=ds)  // points on the quarter of unit sphere 
{
  v=0;
  for(Number j=0;j<=n;j++,v+=ds) pts[i][j]=Point(cos(u)*cos(v),cos(u)*sin(v),sin(u));
}
NURBS nuA(_splineApproximation, pts);  //create approximation nurbs
NURBS nuI(_splineInterpolation, pts);  //create interpolation nurbs
\end{lstlisting}

\begin{center}
	\includePict[width=6cm]{nurbsA.png}\hspace{1cm}
	\includePict[width=6cm]{nurbsI.png}\\
	NURBS approximation and NURBS interpolation
\end{center}

Once a \class{Nurbs} object is constructed, using either the function \var{evaluate} or the operator \verb|()|, it can be evaluated at any parameter \((u,v)\in[0,1]\times [0,1]\):\\

\begin{lstlisting}
\ldots
Point P=nu(0.5,0.5);              //value
Point duP=nu(0.5,0.5,_d1);        //first derivative  (vector as a point)
Point dvP=nu(0.5,0.5,_d2);        //first derivative  (vector as a point)
\end{lstlisting}
\vspace{2mm}
\class{Nurbs} allocates also a \class{Parametrization} object related to the spline parametrization. Using this object, other quantities such as curvilinear abscissa, normal or tangent vector, curvatures are available (see \class{Parametrization} class):\\
\vspace{2mm}
\begin{lstlisting}
\ldots
const Parametrization& pa=nu.parametrization();
real_t u=0.5, v=0.5;
Reals cu=pa.curvatures(u,v);  //two main curvatures
Reals no=pa.normal(u,v);      //normal vector 
Reals ta=pa.tangents(u,v);    //two orthogonal tangent vectors in a same vector
\end{lstlisting}

