%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\section{\classtitle{Point}}\label{s.point}

A finite element library deals obviously with points. The purpose of the \class{Point} class
is to deal with points in any dimension and providing some algebraic operations on points and
comparison capabilities. This class is used by the \class{Function} class encapsulating user
functions.\\

There are mainly four ways to construct a point:\\

\begin{lstlisting}
Point p4(4,0.);     // dimension(4) and value(0) p1=(0,0,0,0)
Point p1(2.);       // a 1D point p1=(2);
Point p2(1.,0.);    // a 2D point p1=(1,0);
Point p3(1.,0.,1.); // a 3D point p1=(1,0,1);
Real v[]={1,2,3};   // array of real_t
Point p4(3, v);           // dimension and real_t array
std::vector<Real> w(3,0); // the std:vector (0,0,0)
Point p5(w);              // stl vector
\end{lstlisting}

To access to:

\begin{itemize}
\item the dimension \(n\) of a point p: \lstinline{p.size},
\item to the i-th coordinate (\(1\leq i\leq n\)) of a point p: \lstinline{p(i)} or \lstinline{p[i-1]}
\item to the x, y or z coordinate (restricted to \(n\leq 3\)): \lstinline{p.x}, \lstinline{p.y} or
\lstinline{p.z}
\item the vector storing the point: \lstinline{p.toVect};
\end{itemize}

You can use the coordinate accessors in reading or writing mode. A simple example:\\

\begin{lstlisting}
Point p(1.,0.,1.);              // a 3D point p=(1,0,1);
p(1)=2;                         // modify the first coordinate
p.y()=3.;                       // modify the second coordinate
std::vector<Real> v=p.toVect(); // convert point to vector
\end{lstlisting}

Using standard operators (+=, -=, +, -, * and /), it is possible to perform algebraic computations
on points up to linear combinations:\\

\begin{lstlisting}
Point p(1.,0.,1.),q(0.,0.,1.),r(1.,2.,3.); // some 3D points
Point g=(p+q+r)/3; // compute the barycenter of p,q,r
(p+=q)/=2;         // p contains the middle of p and q          
\end{lstlisting}

Besides, there are some functions to compute the distance or the square of distance between
two points:\\

\begin{lstlisting}
Point p(1.,0.;1.),q(0.,0.,1.),r(1.,2.,3.); // some 3D points
real_t d=p.distance(q); // compute distance between p and q
d=pointDistance(p,q);   // alternative syntax
d=p.squareDistance(q);  // square of the distance between p and q
d=squareDistance(p,q);  // alternative syntax
\end{lstlisting}

Finally, comparing points is possible using standard operators ==, !=, <, >, <= or >=. The comparison uses a tolerance factor \(\tau\) defined by the variable \emph{Point::tolerance} (p, q being points of \(\mathbb{R}^n\)):

\[
\begin{array}{l}
p==q\ {\rm if}\  |p-q|\leq \tau \\
p<q\ {\rm if}\ \exists i\leq n,\ \forall j<i,\  |p_j-q_j|\leq \tau\  {\rm and}\ p_j<q_j-\tau.

\end{array}
\]

The other comparison operators !=, >, <= or >= are naturally defined from == and < operators.
By default, the tolerance is set to 0. Below is an example:\\

\begin{lstlisting}
Point p(1.,0.,1.),q(0.,0.,1.); // some 3D points
bool r=(p==q);        // r=false
r=(p!=q);             // r=true
r=(p<q);              // r= false
Real eps=.00001;
Point::tolerance=eps; // change the tolerance factor to eps
r=(p==(p+eps/2));     // r=true
\end{lstlisting}

Geometrical transformations on points work as on geometries. Please see \autoref{s.geotransfo} for definition and use of transformations routines.

Then, if you want to create a new \class{Point} by applying a transformation on a \class{Point}, you should use one of the following functions instead:\\

\begin{lstlisting}
//! apply a geometrical transformation on a Point (external)
Point transform(const Point& p, const Transformation& t);
//! apply a translation on a Point (external)
Point translate(const Point& p, std::vector<Real> u = std::vector<Real>(3,0.));
Point translate(const Point& p, Real ux, Real uy = 0., Real uz = 0.);
//! apply a rotation 2d on a Point (external)
Point rotate2d(const Point& p, const Point& c = Point(0.,0.), Real angle = 0.);
//! apply a rotation 3d on a Point (external)
Point rotate3d(const Point& p, const Point& c = Point(0.,0.,0.), std::vector<Real> u = std::vector<Real>(3,0.), Real angle = 0.);
Point rotate3d(const Point& p, Real ux, Real uy, Real angle);
Point rotate3d(const Point& p, Real ux, Real uy, Real uz, Real angle);
Point rotate3d(const Point& p, const Point& c, Real ux, Real uy, Real angle);
Point rotate3d(const Point& p, const Point& c, Real ux, Real uy, Real uz, Real angle);
//! apply a homothety on a Point (external)
Point homothetize(const Point& p, const Point& c = Point(0.,0.,0.), Real factor = 1.);
Point homothetize(const Point& p, Real factor);
//! apply a point reflection on a Point (external)
Point pointReflect(const Point& p, const Point& c = Point(0.,0.,0.));
//! apply a reflection2d on a Point (external)
Point reflect2d(const Point& p, const Point& c = Point(0.,0.), std::vector<Real> u = std::vector<Real>(2,0.));
Point reflect2d(const Point& p, const Point& c, Real ux, Real uy = 0.);
//! apply a reflection3d on a Point (external)
Point reflect3d(const Point& p, const Point& c = Point(0.,0.,0.), std::vector<Real> u = std::vector<Real>(3,0.));
Point reflect3d(const Point& p, const Point& c, Real ux, Real uy, Real uz = 0.);
\end{lstlisting}

For instance:\\

\begin{lstlisting}
Point p1;
Point p2=translate(p1, 0.,0.,1.);
\end{lstlisting}
