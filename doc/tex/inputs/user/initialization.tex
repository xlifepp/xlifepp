%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex

\xtitleTwo{The init function}

As said in \autoref{ch.gettingstarted}, every program using \xlifepp begins by a call to the \cmd{init} function, taking up to 4 key/value arguments:

\begin{description}
\item[\param{\_lang}] enum to set the language for print and log messages. Possible values are \emph{en} for English, \emph{fr} for French, \emph{de} for German, or \emph{es} for Spanish. Default value is \emph{en}.
\item[\param{\_verbose}] integer to set the verbose level. Default value is 1.
\item[\param{\_trackingMode}] boolean to set if in the log file, you have a backtrace of every call to a \xlifepp routine. Default value is false.
\item[\param{\_isLogged}] boolean to activate log. Default value is false.
\end{description}

Furthermore, the \cmd{init} function loads functionalities linked to the trace of where such messages come from. If this function is not called, \xlifepp cannot work !!!
These parameters can also be set from dedicated command line options:

\begin{lstlisting}[deletekeywords={[3] current,number,string,language,set,for}]
Command line options for XLiFE++ executables:
  -h                               prints the current help
  -j <num>, -jobs <num>,           define the number of threads. Default is
  --jobs <num>                     -1, meaning automatical behavior.
  -l <string>, -lang <string>,     define the language used for messages.
  --lang <string>                  Default is en.
  -vl <num>, -verbose-level <num>, define the verbose level.
  --verbose-level <num>            Default is 0.
  -v                               set the verbose level to 1.
  -vv                              set the verbose level to 2.
  -vvv                             set the verbose level to 3.
\end{lstlisting}

To deal with these command line options, you just have to give standard arguments to the \cmd{init} function:

\begin{lstlisting}
init(argc, argc, _lang=fr);
\end{lstlisting}

\xtitleTwo{Managing your own options}

\xlifepp provides an \class{Options} object to manage user options, that can be read from the command line or from files.

What is an option ? An option has:

\begin{itemize}
\item A name, that will be used to get the value of an option, as for a \class{Parameter}.

\begin{warningbox}
An option name cannot start with a sequence of "-" characters. First characters must be alphanumerical.
\end{warningbox}

\item One or several keys used to define an option (key in a file or command line option). A key cannot be used twice, and some keys are forbidden, due to some keys dedicated to system options (see \autoref{s.tutorial} about the \cmd{init} function).
\item A value, that can be scalar (\class{Int}, \class{Number}, \class{Real}, \class{Complex} or \class{String}) or vector (\class{Ints}, \class{Numbers}, \class{Reals}, \class{Complexes} or \class{Strings}). This is the defautl value of the option and determines its data type. For vector types, size is not relevant.
\end{itemize}

This class provides a default constructor. In order to define a user option, you can use the function \cmd{addOption}:

\begin{lstlisting}
Options opts;
opts.addOption("t1", 1.2);
opts.addOption("t2", "tata");
opts.addOption("t3", "-t", Complex(0.,1.));
opts.addOption("t4", Strings("-tu","-tv"), Reals(1.1, -2.4, 0.7));
opts.addOption("t5", Strings("tata","titi"));
\end{lstlisting}

To parse options from file and or command line arguments, you may use one of the following line:

\begin{lstlisting}
opts.parse(argc, argv);
String filename="param.txt";
opts.parse(filename);
opts.parse(filename, argc, argv);
\end{lstlisting}

where {\ttfamily argv} and {\ttfamily argv} are the arguments of the {\ttfamily main} function dedicated to command line arguments of the executable. When using both filename and command line arguments, the latter are given priority.

\medskip

Let's now talk about how use options in a file or command-line.

First option is named "t1". So it can be used through the key "t1", "-t1" or "--t1". Fourth option is named "t4" and has 2 aliases: "-tu" et "-tv". So it can be used through the key "t4", "-t4", "--t4", "-tu" or "-tv".

\begin{lstlisting}
./exec --t1 2.5 -t2 tutu -t3 2.5 -0.1 -t4 2.2 -4.8 1.4  -t5 "tutu" toto "ta ta" titi
\end{lstlisting}

When passing options from command line, you consider a \class{Complex} value as 2 \class{Real} values.
Furthermore, quotes are not necessary for \class{String} values, unless the value contains special characters such as blank spaces, escape characters, \ldots Here is an example of ASCII file used to define options:

\begin{lstlisting}
t1 3.7
t2 "ti ti"
t3 (2.5,-0.2)
t4 2.5 -2 3.4 4.7
t5 "titi" tutu "ta ta" toto
\end{lstlisting}

When passing options from a file, a \class{Complex} value is now written as a complex, namely real part and imaginary part are delimited by a comma and inside parentheses. As for command-line case, quotes are not necessary for \class{String} values, unless the value contains special characters such as blank spaces, escape characters, \ldots

\xtitleTwo{Using \xlifepp with global parameters}

\xtitleThree{Global constants and objects}

Some global constants are available and may be useful to you:

\begin{description}
\item[pi\_] the \(\pi\) constant with the \class{Real} precision
\item[i\_] the imaginary number with the \class{Complex} precision
\item[theEulerConst] the Euler-Mascheroni constant
\item[theTolerance] the precision used to convergence in norms
\item[theEpsilon] the machine epsilon
\end{description}

Some usefulw objets are also available:

\begin{description}
\item[thePrintStream\_] the dedicated file stream to the print.txt file generated while executing a program using \xlifepp.
\item[theCout] Using this stream object means using either the standard output stream {\ttfamily std::cout} and the previous file stream {\ttfamily thePrintStream\_}. This is the reason why we recommend you to always use this stream.
\item[eol] An alias to {\ttfamily std::endl}
\end{description}

\xtitleThree{Multi-threading}

\xlifepp, when configured with \omp, uses automatically multi-threading. We saw that the \cmd{init} function enables you to define the number of threads that will be used, but you cal also change the number of threads everywhere you want in your program. To modify its value, use the \cmd{numberOfThreads} function:

\begin{lstlisting}
numberOfThreads(24);
\ldots
numberOfThreads(12);
\ldots
\end{lstlisting}

When used without argument, the routine returns the number of threads currently defined.

\begin{focusbox}
In multithreading environment, the stream {\ttfamily theCout} prints only on the thread 0.
\end{focusbox}

\xtitleThree{The verbosity}

We saw that the \cmd{init} function enables you to define the verbosity, but you cal also change the verbose level everywhere you want in your program. To modify its value, use the \cmd{verboseLevel(\ldots)} function:

\begin{lstlisting}
verboseLevel(10);
\ldots
verboseLevel(0);
\ldots
\end{lstlisting}

When the verbose level is set to 0, nothing is printed except the errors and warnings. \\

\begin{focusbox}
In multithreading environment, it may appear other print files ({\ttfamily printxx.txt}) corresponding to outputs of each thread. 
\end{focusbox}
