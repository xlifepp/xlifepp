%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
% Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex

Open Cascade Technology (OCT) \footnote{Trademark @Open Cascade, https://www.opencascade.com/}  is a third party open source library dedicated to 3D CAD data. It is a powerful library dealing with canonical geometries but providing complex geometrical operations (union, intersection, difference of geometries, fillet, chamfer, \ldots). The standard geometry engine of \xlifepp provides only union or difference in the case of one geometry included in another one (if detection is easy). So to go further, \xlifepp provides an interface to OCT. Obviously, OCT must be installed and activated in \xlifepp (cmake option).
\begin{warningbox}
OCT interface is still experimental. Use it with cautious!	
\end{warningbox}
When OCT is enabled, it runs only if the user invokes an explicit OCT function. OCT objects (\class{OCData} class) are embedded in \xlifepp geometry objects but are not allocated by default. If an OCT function is invoked, OCT object will be allocated on the fly.\\

The main interest of OCT interface is to mesh complex object, for instance the intersection  of two spheres perforated by a cylinder:\\

\begin{lstlisting}[deletekeywords={[3]x}]
Sphere S1(_center=Point(1.,0,0.),_radius=1,_hsteps=0.02,_domain_name="S",_side_names="Sigma");
Sphere S2=translate(S1,1.5,0.,0.); // _side_names "Sigma_2"
RevCylinder C(_center1=Point(1,0.,0),_center2=Point(2.5,0.,0.),_radius=0.3,_hsteps=0.02,
              _domain_name="C",_side_names="Gamma");
Geometry G=(S1^S2)-C;  //intersection of spheres minus cylinder
\end{lstlisting}
\vspace{2mm}
At this stage, the geometry \var{G} handles only a symbolic description and does not handle any OCT object. It can be not dealt with usual mesh tools of \xlifepp addressing only the standard gmsh engine. Now invoking the \class{Mesh} constructor with the mesh generator option \var{\_gmshOC} will produce the creation of OCT objects related to geometries involved (here spheres and cylinder)  and an OCT object related to the geometry \var{G} that will be exported to \gmsh in a particular way.\\

\begin{lstlisting}[deletekeywords={[3]x}]
Mesh M(G,_triangle,1,_gmshOC);
\end{lstlisting}
\vspace{2mm}
\begin{figure}[H]
	\centering
	\includePict[width=8cm]{OCsphere&sphere-cyl.png}
\end{figure}
In this example, the name of domains created by \var{Mesh} are quite natural : "Sigma" and "Sigma\_2" for the sphere boundaries and "Gamma" for the cylinder boundary. But in some more complex cases, it can be not so intuitive to recover domain names, especially when new faces are created. There is a quite simple way to recover them. Indeed, when meshing with \var{\_gmshOC} generator, 3 files are created in the folder containing the executable file:
\begin{itemize}
	\item {\it xlifepp.brep} containing the {\it brep} export of the OCT object
	\item {\it xlifepp.geo}, a gmsh script file containing the command to load the {\it brep} file and some additional commands related to mesh discretization and domain names. 
	\item {\it xlifepp.msh}, the mesh file created by \gmsh and loaded by \xlifepp to create the \class{Mesh} object.
\end{itemize}  
Loading the {\it xlifepp.geo} in \gmsh and configuring it to see curve/surface labels will allow you to identify easier domain names.
\begin{figure}[H]
	\centering
	\includePict[width=12cm]{gmshOC_viewGeo}
\end{figure}
\vspace{2mm}
The next example shows how powerful is OCT and how \xlifepp can manage complex geometries:
\begin{lstlisting}[deletekeywords={[3]x}]
Cone co(_center1=Point(0.,0.,0.),_v1=Point(1.,0.,0.),_v2=Point(0.,1.,0.),_apex=Point(0.,0.,1),
        _hsteps=0.3,_side_names="gamma");
Cylinder cy(_center1=Point(0.,0.,0.),_v1=Point(0.7,0.,0.),_v2=Point(0.,0.7,0.),
            _center2=Point(0.,0.,7.), _hsteps=1,_side_names="gamma");
Sphere sp(_center=Point(0.,0.,7.),_radius=1.,_hsteps=0.3,_side_names="gamma");
Geometry ant=cy+co+sp;
Real R=30., aR=0.999*R, d=0.2;
Sphere spcor(_center=Point(0.,0.,0.),_radius=R,_hsteps=2,_side_names="sigma");
Geometry cor=toComposite(spcor);
for(number_t i=0;i<8;i++)
 for(number_t j=0;j<7;j++)
 {
  Real ir = i+d*(2*std::rand() * (1.0 / RAND_MAX)-1),
         jr=  j+d*(2*std::rand() * (1.0 / RAND_MAX)-1);
  Real t=pi_*(ir/4-1), p=pi_/8*(jr-3);
  Transformation tf = Translation(Point(aR*cos(t)*cos(p),aR*sin(t)*cos(p),aR*sin(p)))
                    * Rotation3d(Point(0,0,0),Point(0,0,1),t);
  tf *= Rotation3d(Point(0,0,0),Point(0,1,0),pi_/2-p);
  cor+=transform(ant,tf);
 }
Mesh M(cor,_triangle,1,_gmshOC);
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includePict[width=7cm]{corona_mesh}
\end{figure}
\subsubsection*{Available geometrical operations}
\begin{center}
	\begin{tabular}{|p{1.5cm}|p{7cm}|p{7cm}|}
		\hline
		operation & with OC extension & standard geometry engine \\
		\hline
		\hline
		\verb|+| & merge geometries (fusion)& union of disjoint or included geometries\\
		\hline
		\verb|-| & difference of geometries (cut) & make a hole (included geometries) \\
		\hline
		\verb|^|& intersection of geometries (common) & not available \\
		\hline
		\verb|%|& not managed & force inclusion \\
		\hline
	\end{tabular}
\end{center}

\subsubsection*{Importing brep files}
OCT extension provides a new interesting feature :  loading a  {\it brep} file in a \class{Geometry} object and mesh it using the \var{\_gmshOC} generator:
\begin{lstlisting}[deletekeywords={[3]x}]
Geometry naca("NACA63-412.brep");
naca.setOCName(_solid,1,"naca");
naca.setOCName(_face,1,"extrados"); //extrados face
naca.setOCName(_face,2,"intrados"); //intrados face
naca.setOCName(_face,Numbers(3,4),"lateral"); //lateral faces
naca.setOCHstep(Numbers(1,3),0.2);
naca.setOCHstep(Numbers(2,4),0.1);
Mesh(naca,_tetrahedron,1,_gmshOC);
\end{lstlisting}
In this example, after viewing in \gmsh the "NACA63-412.brep" file and identify numbers of elementary objects (solid, face, point), some particular functions (\var{setOCName}, \var{setOCHstep}, \var{setOCNnode}) allow controlling names and mesh size parameters.
\begin{figure}[H]
	\centering
	\includePict[width=14cm]{naca_mesh}
\end{figure}
