%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
% Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex

As an alternative to mesh an extruded geometry, it is possible to extrude a 1D or a 2D mesh using the following mesh constructor:\\

\begin{lstlisting}
Mesh(const Mesh& ms, const Point& O, const Point& D , number_t nbl,
number_t namingDomain=0, number_t namingSection=0, number_t namingSide=0,
const string_t& meshName="");
\end{lstlisting}

where \(\vec{OD}\) defines the direction of extrusion and  \(nbl\) the number of layers of same width (regular extrusion). More precisely, any point \(M\) of the section mesh is extruded using \(nbl\) points :

\[
M_k=M + O +k*\vec{OD}.
\]

When a 1D section, extruded mesh is made with quadrangles. When a 2D triangular mesh section, extruded mesh is made with prisms and  when a 2D quadrangular mesh section, extruded mesh is made with hexahedra.\\

The boundary domains created by the extrusion process come from the boundary domains of the original section. This process is controlled by the 3 parameters \var{namingDomain}, \var{namingSection}, \var{namingSide} taking one of the values 0, 1 or 2, with the following rules:\\

\begin{description}
\item[0:] domains are not created
\item[1:] one extruded domain is created for any domain of the original section
\item[2:] for each layer, one extruded domain is created for any domain of the original section
\end{description}

Be cautious, the side domains of extruded domain are created from the side domains of the given section. Thus, if the given section has no side domains, the extruded domains will have no side domain! The naming convention is the following:\\

\begin{itemize}
\item Domains and side domains keep their name with the extension "\_e"  or "\_e1", "\_e2", \ldots, "\_en"
\item Section domains have the name of original domains with the extension "\_0", \ldots,"\_n"
\item When \var{namingDomain=0}, the full domain is always created and named "Omega".
\end{itemize}

The following figure illustrates the naming rules of domains.

\begin{figure}[H]
\begin{center}
\includePict[scale=0.5]{mesh_extrusion.png}
\end{center}
\caption{Mesh extrusion, domains naming}
\label{mesh_extrusion_domain}
\end{figure}

For instance, to mesh a tubular domain using the mesh extrusion of a crown:\\

\begin{lstlisting}
Disk dext(_center=Point(0.,0.), _radius=1.,_nnodes=20, _domain_name="Omega",
          _side_names="Sigma");
Disk dint(_center=Point(0.,0.), _radius=0.5,_nnodes=10,_side_names="Gamma");
Mesh crown(dext-dint,_triangle,1,_gmsh);
Mesh tube(crown,Point(0,0,0),Point(0,0,1),10,1,1,1);
\end{lstlisting}

\begin{figure}[H]
\begin{center}
\includePict[scale=0.3]{crown_extrude.png}
\end{center}
\caption{Tube prismatic mesh from extrusion of a crown mesh}
\label{mesh_extrusion_crown}
\end{figure}
