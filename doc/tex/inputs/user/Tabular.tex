%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\section{\classtitle{Tabular}}

The \class{Tabular} class allows managing tabulated values of a function. More precisely, it handles the values of a function on a uniform grid of any dimension, mainly providing some tools to create the table, to save the table to a file, to load a table from a file, and to evaluate the values of the function at some points by interpolating from the values on the grid.\\

Up to now, only scalar (T is Real or Complex) functions of 1,2 or 3 real coordinates, may be with a \class{Parameters}, are handled:
\begin{lstlisting}[deletekeywords={[3]x}]
T f1  (Real);
T f1p (Real, Parameters&);
T f2  (Real, Real);
T f2p (Real, Real, Parameters&);
T f3  (Real, Real, Real);
T f3p (Real, Real, Real, Parameters&);
\end{lstlisting}
\vspace{.1cm}
To create a \class{Tabular} define your function and give the grid parameters (the starting coordinate x0, the step dx, the number of steps nbx, an optional name) along each coordinate:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]x,y}]
Real  f1(Real x){return sin(x);}
\ldots
Tabular<Real> t1(0.,0.01,100,f1);
\end{lstlisting}
\vspace{.1cm}
Now, you can print the \class{Tabular}, save the \class{Tabular} to a file and evaluate the tabulated function at any point inside the grid domain:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]x}]
theCout<<t1;
t1.saveToFile("f1.tab","my optional comment");
Real y=t1(0.5);
\end{lstlisting}
\vspace{.1cm}
If you saved a \class{Tabular} to a file, you can load it in a new \class{Tabular} of the same type:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]x,y}]
Tabular<Real> t1("f1.tab");
\end{lstlisting}
\vspace{.1cm}
To deal with a 2D or 3D grid, it is quite similar:
\vspace{.1cm}
\begin{lstlisting}[deletekeywords={[3]x,y}]
Real  f2(Real x, Real y){return sin(x*y);}
\ldots
Tabular<Real> t2(0.,0.01,100,0.,0.005,50,f2);
theCout<<t2;
t2.saveToFile("f2.tab","my optional comment");
Real y=t2(0.5,0.5);
\end{lstlisting}

\begin{ideabox}
If you need a grid of dimension greater than 3, contact the developers. But be care, a 4D grid with 100 steps in each direction requires storing \(10^8\) values, almost 1 Go of memory for double values. 
\end{ideabox}

\subsection*{Tabulated Function}

In order to use tabulated function in FE computation, it is possible to associate a \class{Tabular} to a \class{Function}:
\begin{lstlisting}[deletekeywords={[3]x,y}]
Real f(Real x, Real y){return sin(x*y);}
\ldots
Tabular<Real> tab(0.,0.01,100, 0.,0.005,50, f);
Function F(tab, 2);  // 2 is the dimension of Point in the function!
\end{lstlisting}

To be consistent, the dimension of the input \class{Point} of the function has to be specified to the dimension of the \class{Tabular} ! Up to now, \class{Tabular} dimension is limited to 3.\\

It may be of interest to use a Tabular with a dimension less than the Point dimension; for instance a Tabular defined only for the radial coordinate. In that case, a \class{Function} relating Point to radial coordinates must be defined:

\begin{lstlisting}[deletekeywords={[3]x,y}]
Real f(Real x, Real y){return sin(x*y);}
Real radial(const Point& X, Parameters& pars=defaultParameters) {return norm(X);}
\ldots
Tabular<Real> tab(0.,0.01,100,0.,0.005,50,f);
Function R(radial);     // create R function from radial function
Function F(tab, 2, R);  // 2 is the dimension of Point in the function!
\end{lstlisting}

Note that it is also possible to associate a \class{Tabular} to the function itself:

\begin{lstlisting}[deletekeywords={[3]x,y}]
Real f(const Point& X, Parameters& pars=defaultParameters){return sin(x*y);}
\ldots
Tabular<Real> tab(0.,0.01,100,0.,0.005,50,f);
Function F(f, 2);  // 2 is the dimension of Point in the function!
F.createTabular(0.,0.01,100,0.,0.005,50); // F is now tabulated
\end{lstlisting} 

Once a \class{Tabular} is handled by a \class{Function}, it will be used in any circumstances!\\

\begin{warningbox}
Tabulated \class{Function} works only for {\bfseries scalar} functions !
\end{warningbox}
