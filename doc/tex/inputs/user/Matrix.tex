%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\section{\classtitle{Matrix}}

The purpose of the \class{Matrix} class is mainly to deal with complex or real \textbf{dense}
matrices. In particular, this class is used in the definition of the user functions (see the
section Function).  This class is compliant with the \class{Vector} class. Although, it can
deal with matrices of anything, it is only fully functional for real or complex matrices:\\

\begin{lstlisting}
Matrix<Real> rA;         // an empty matrix
Matrix<Real> rB(3,2);    // a 3@\(\times\)@2 zeros matrix
Matrix<Real> rC(3,2,1);  // a 3@\(\times\)@2 ones matrix
Vector<Real> w(3,2.5);   // w=[2.5 2.5 2.5]
Complex i(0,1);          // the complex i
Matrix<Real> cA(3,2,i);  // a 3@\(\times\)@2 i matrix 
\end{lstlisting}

It is possible to construct diagonal matrix from a \class{Vector} or a matrix from a {\class
Vector} of \class{Vector}, to load (and save) a matrix from a file and to construct particular
matrices ({\ttfamily \_zeroMatrix, \_onesMatrix, \_idMatrix, \_hilbertMatrix}):\\

\begin{lstlisting}
Vector<Real> u(3,2.);              // vector [2. 2. 2.]
Matrix<Real> rA(u);                // 3@\(\times\)@3 matrix with u as diagonal
Matrix<Real> rB("mat.dat");        // matrix loaded from "mat.dat" file
Matrix<Real> rO(3,_zeroMatrix);    // a 3@\(\times\)@3 zeros matrix
Matrix<Real> r1(3,_onesMatrix);    // a 3@\(\times\)@3 ones matrix
Matrix<Real> rI(3,_idMatrix);      // a 3@\(\times\)@3 identity matrix
Matrix<Real> rH(3,_hilbertMatrix); // the 3@\(\times\)@3 Hilbert matrix
\end{lstlisting}

Construction of complex matrix from real data are allowed (automatic cast). But the contrary
is not.\\

There are some functions to access to the matrix properties:\\

\begin{lstlisting}
numberOfRows(), numberOfColumns()
isSymmetric(), isSkewSymmetric(), isSelfAdjoint(), isSkewAdjoint()
\end{lstlisting}

and some utilities to access to a coefficient, a row or a column or the diagonal of the matrix
:\\

\begin{lstlisting}
Matrix<Real> A(2,2,1);   //a 2@\(\times\)@2 ones matrix
A(1,1)=2.;                 //change the coefficient A11
Vector<Real> r=A.row(1); //first row of A
r=A.column(2);             //second column of A
r=A.diag();                //diagonal of A
A.column(1,r);             //assign a vector to the first column
A.row(2,r);                //assign a vector to the second row
A.diag(r);                 //assign a vector to the diagonal
\end{lstlisting}

All these functions support automatic cast from real to complex but not the contrary.\\

Advanced users can use member functions \cmd{begin} and \cmd{end} returning respectively iterators (or const iterators) to the beginning and the end of the Matrix.
The data values of the matrix are stored according to the C convention, i.e. row-wise.\\

There are also generalized access tools either to extract submatrix (\cmd{get} or \cmd{operator}) or to set submatrix of matrix (\cmd{set}):\\

\begin{lstlisting}
Matrix<Real> M(3,3);       
for(Number i=1;i<=3;i++)   
  for(Number j=1;j<=3;j++) 
	   M(i,j)=i+j;                //M=[2 3 4; 3 4 5; 4 5 6] 
Matrix<Real> N= M.get(2,3,2,3); //N=[4 5; 5 6] 
//N=M(2,3,2,3) gives the same
Vector<Number> is(2);
is(1)=1;is(2)=3;
N= M.get(is,is);                //N=[2 4; 4 6] 
//N=M(is,is) gives the same
Matrix<Real> Z(2,2,0);          //Z=[0 0; 0 0] 
M.set(1,2,1,2,Z);               //M=[0 0 4; 0 0 5; 4 5 6] 
Matrix<Real> U(2,2,1);          //U=[1 1; 1 1] 
M.set(is,is,Z);                 //M=[1 0 1; 0 0 5; 1 5 1] 
\end{lstlisting}

Other syntaxes are proposed, see the developer's documentation.\\

Besides, the \class{Matrix} class proposes some transformations either as internal functions
or external functions:\\

\begin{lstlisting}
Matrix<Real> A(2,2,1),B;
Matrix<Complex> C(2,2,i),D;
A.transpose();     // self transposition of A
B=transpose(A);    // transposition of A, A not changed
C.adjoint();       // self transposition and conjugate C
D=adjoint(A);      // transpose and conjugate, C not changed
B=diag(A);         // from diagonal of A to a diagonal matrix
A=real(C);         // real part of C
B=imag(C);         // imaginary part of C
D=conj(C);         // conjugate of C
D=cmplx(A);        // forced casting from real to complex
Real n2=norm2(A);      //Frobenius norm 
Real ninf=norminfty(A);//infinite norm 
\end{lstlisting}

Standard algebraic operations (+=, -=, *=, /=, +, -, *, /) are supported by the \class{Matrix} class.
Some shortcuts are also possible, for instance a matrix plus a scalar, a scalar plus a matrix,
\ldots Automatic cast from real to complex is supported. There is no comparison operator.\\
\\
The \class{Matrix} proposes some solvers:\\

\begin{lstlisting}[deletekeywords={[3] row}]
Matrix<Real> A
RealVector B; 
\ldots
// solve AX=B or AXs=Bxs using Gauss reduction
gaussSolver(A, B, piv, row); 
gaussMultipleSolver(A, B, nbrhs, piv, row); 
//inverse of a square matrix
RealMatrix invA=inverse(A);
// QR factorization
RealMatrix Q,R;
qr(A,Q,R);
// SVD factorization A= U S V*, if A a (m,n)-matrix, U is a (m,r)-matrix, V a (n,r)-matrix and S a r-vector where r=min(m,n)
RealMatrix U, V;
RealVector S;   //singular values
svdMat(A, U, S, V);
\end{lstlisting}

\begin{warningbox}
QR and SVD are available only if \eigen library is set on.
\end{warningbox}

\begin{focusbox}
It is also possible to deal with matrix of matrices, for instance:

\bigskip

\begin{lstlisting}
Matrix<Real> ones(2,2,1);          //a 2@\(\times\)@2 ones matrix
Matrix<Matrix<Real> > A(2,2,ones); //a 2@\(\times\)@2 matrix of 2@\(\times\)@2 ones matrix
\end{lstlisting}

but all operations are not supported for such matrices!
\end{focusbox}

To avoid explicit templates in user program, the following aliases are provided:

\begin{description}
\item[\class{RealMatrix}] stands for \class{Matrix<Real>},
\item[\class{ComplexMatrix}] stands for \class{Matrix<Complex>}.
\item[\class{RealMatrices}] stands for \class{Matrix<Matrix<Real> >},
\item[\class{ComplexMatrices}] stands for \class{Matrix<Matrix<Complex> >}.
\end{description}
