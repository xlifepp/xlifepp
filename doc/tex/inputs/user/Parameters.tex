%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\section{\classtitle{Parameters}}\label{a.s.parameters}

In order to attach some user's data to anything (in particular functions), two classes (\class{Parameter} and \class{Parameters}) are proposed. The \class{Parameter} class handles a single data of type \emph{integer, real, complex, string, real/complex vector/matrix} or \emph{void *} with the possibility to name the parameter. The \class{Parameters} class handles a list of \class{Parameter} objects.\\

\subsection{The \classtitle{Parameter} object}

It is easy to define a parameter by its constructor or the assignment operation:
\begin{lstlisting}[deletekeywords={[3]name, value}]
Parameter p(value,[name]);
Parameter p=value;
\end{lstlisting}
where \emph{value} is of type integer, real, complex, string (or char*), RealVector, ComplexVector, RealMatrix, ComplexMatrix or void * and \emph{name} is an optional string defining the parameter name. \\ 

Once a parameter is set, it is possible to get its name (if defined), its type, its value and
print it:
\begin{lstlisting}
Parameter k(1.,"frequency");
cout<<"parameter "<<k.name()<<" type "<<k.type()<<" value="<<real(k);
k.print(); // print name and value
cout<<k;   // print only its value
RealMatrix H(5,_hilbertMatrix);     // hilbert matrix 5x5
Parameter mat(H,"Hilbert matrix");  // H as parameter
\end{lstlisting}
\vspace{1mm}
The use of type \emph{void }* allows the user to deal with data of any kind. This nice possibility
is for advanced users because a \emph{void *} variable is unsafe in C++: 
\begin{lstlisting}
list<String> lst;                   // a list of string
lst.push_back("Helmholtz"); lst.push_back("Laplace");
Parameter par(&lst,"problem list"); //void * parameter
cout<<par;                          // print the pointer not the list
list<String> >& rlst=static_cast<list<String>&>(*pointer(par)); // be sure !!!
//or
list<String> >& rlst=static_cast<list<String>&>(*par.get_p());  // be sure !!!
cout<<rlst;                         // print the list not the pointer 
\end{lstlisting}
\vspace{1mm}
The functions to get the value are \lstinline{integer}, \lstinline{real}, \lstinline{cmplx}, \lstinline{string} and \lstinline{pointer}. Be cautious,
the user must invoke the "get" function compatible with the parameter type. In case of misfit
call, an error may occur or not if a logical cast is possible (only integer to real and real
to complex).
\begin{lstlisting}
Parameter k(1.,"frequency"); // a real parameter
Real r=real(k);              // compatible get
Complex c=cmplx(k);          // no compatible get, but cast real to complex
String s=string(k);          // no compatible get, error
void * q=pointer(k);         // no compatible get, error
\end{lstlisting}
\vspace{1mm}
A \class{Parameter} object can be automatically cast to its right value:
\begin{lstlisting}
Parameter k(1.,"frequency");     // a real parameter
Parameter i(complex_t(0,1),"i"); // a complex parameter
Parameter mat(RealMatrix(5,_hilbertMatrix),"Hilbert matrix");// a matrix parameter
Real r=k;    // auto cast to real 
r=k;         // auto cast to real 
Complex c=k; // auto cast to complex 
c=i;         // does not work !!! cannot resolve ambiguity of complex class
c=k;         //          complex-> complex, double -> complex
String s=string(k);  // error, incompatible types
void * q=pointer(k); // error, incompatible types
RealMatrix H=mat;    // ok
\end{lstlisting}
\vspace{1mm}

For numerical type parameters (integer, real or complex), it is possible to apply algebraic
operations (+=, -=, *=, /=, +, -, *, /), comparison operations (==, !=, >, >=, <, <=). The result is a Parameter. These operations do not yet work on vectors and matrices.
% and mathematical functions (abs, conj, log, log10, exp, sin, cos, tan, sinh, cosh, tanh, acos, asin, atan, acosh, asinh, atanh, pow).
\begin{lstlisting}
Parameter k(1.,"frequency");
Parameter k2=k*k;
\end{lstlisting}
\vspace{1mm}

\subsection{The \classtitle{Parameters} object: list of \classtitle{Parameter}}

The \class{Parameter} object is a brick of the more interesting class \class{Parameters}
which handles a list of \class{Parameter}. With this class, the user is able to deal with
lists of anything of the type of numerics (integer, real, complex, vector, matrix) or string type or pointer
type. In particular, these parameters lists can be attached to functions as Parameters object of the
function (see the class \class{Function} documentation).\\

A \class{Parameters} object is simply defined by constructors taking one explicit data of
type supported by the \class{Parameter} class or one \class{Parameter} object:
\begin{lstlisting}[deletekeywords={[3]name, value}]
Parameters ps(value,[name]);
\end{lstlisting}
where \emph{value} is of type integer, real, complex, real/complex vector/matrix, string (or char*) or void * and \emph{name}
is an optional string defining the parameter name. When \emph{value} is a \class{Parameter} object,
\emph{name} is not required.\\ 

The main operations on the list are the insertion and the extraction of parameter values. To
insert a parameter in the list, you can use the \lstinline{push} function or the stream operator \(<<\)
:\\
\begin{lstlisting}[deletekeywords={[3] value}]
Parameters ps;
ps.push(param);
ps<<value;
\end{lstlisting}
where \emph{value} is of type integer, real, complex, string (or char*), void * or is a \class{Parameter}
object. \\
For instance :
\begin{lstlisting}[deletekeywords={[3] params}]
Parameters params(2.,"k");  //initialize from one data
params<<Parameter(1.,"rho")<<Parameter(3.,"eps");   //insert 2 real
params<<3.1415926;   //insert a real with no name get it by its index (4)
params<<Parameter(RealVector(5,1.),"v"); //insert a vector
params<<Parameter(RealxMatrix(5,_hilbertMatrix),"H"); //insert a matrix
\end{lstlisting}
To extract a parameter from the list, you have to use the direct access operator () specifying
its rank (from 1) in the parameters list or its parameter name or the parameter itself:
\begin{lstlisting}[deletekeywords={[3]name}]
Parameter p=ps(i);    // i is an integer index
Parameter p=ps(name); // name is a string
Parameter p=ps(q);    // q is a parameter
\end{lstlisting}
If a parameter has no name (case of a value insertion with no name) a default name is given (\emph{parameterx}
with x its rank in the list)! To get the value of the parameter, capabilities of the \class{Parameter}
class may be used. It is also possible to use the assignment operator = :
\begin{lstlisting}[deletekeywords={[3]name, params}]
Parameters params;
Real k=params("k").get_r();  //use get with name
Real rho=params("rho");    //work also
Real pi=params(4);         //no name available
RealVector v=params("v");  //get vector
RealMatrix H=params("H");  //get matrix
\end{lstlisting}
where \emph{value\_type} is the type of the parameter (be cautious with type compatibility).
\\

This class provides print facilities of a list of parameters:
\begin{lstlisting}[deletekeywords={[3] params}]
Parameters params;
params.print();           // print on a default print file
params.print(out);        // out is an output stream
out << params;
\end{lstlisting}

Finally, the class provides a void list of parameters: \emph{Parameters::default\_Parameters}.
\\


An example:
\begin{lstlisting}[deletekeywords={[3]x}]
Parameter height=3;
Parameters data;
data << height << Parameter(4, "width") << "case 1" << 1.5;
// String "case 1" has default name "parameter3"
// Real_t "1.5" has default name "parameter4"
Parameter a=data("height"); // acces by name, contains height
Parameter c=data(4);        // acces by rank, contains 1.5
data(1)=2;                  // replace the value 3 by 2
data("height")=2;           // same effect, height is thereafter modified
data(height)=2;             // same effect
double x=data(4);           // x contains 1.5
x=data("width");            // x contains 4
\end{lstlisting}
\vspace{.2cm}
Note that there is no possibility to delete a parameter of the list and, contrary to the {\class
Parameter} class, no algebraic operations may be performed on list of parameters.
