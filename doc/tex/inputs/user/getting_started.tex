%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex

\section{The variational approach}

Before learning in details what \xlifepp is able to do, let us explain the basics with an example, the {\bfseries Helmholtz} equation:

\begin{center}
For a given function f(x, y), find a function u(x, y) satisfying
\begin{equation}
\label{eq.helmholtz}
\begin{cases}
\displaystyle -\Delta u(x,y) + u(x,y)=f(x,y) & \forall (x,y) \in \Omega \\
\displaystyle \frac{\partial u}{\partial n}(x,y)=0 & \forall (x,y) \in \partial\Omega
\end{cases}
\end{equation}
\end{center}

To solve this problem by a finite element method, \xlifepp is based on its variational formulation: find \(u\in H^1(\Omega)\) such that \(\forall v\in H^1(\Omega)\) 

\vspace{-0.5cm}
\begin{center}
\begin{equation}
\label{fv.helmholtz}
\displaystyle \int_\Omega\nabla u.\nabla v\,dx\,dy -\int_\Omega u\,v\,dx\,dy=\int_\Omega f\,v\,dx\,dy. 
\end{equation}
\end{center}
All the mathematical objects involved in the variational formulation are described in \xlifepp. The following program solves the Helmholtz problem with \(f(x,y)=\cos{\pi x} \cos{\pi y}\) and \(\Omega\) is the unit square.
\vspace{.1cm}
\inputCode[numbers=left, numberstyle=\tiny, numbersep=5pt, deletekeywords={[3]x, y}]{helmholtz.cpp}

Please notice how close to the Mathematics, \xlifepp input language is.

\section{How does it work ?}\label{s.tutorial}

This first example shows how \xlifepp executes all the usual steps required by the {\bfseries Finite Element Method}. Let us walk through them one by one.

\begin{description}
\item[line 12:] Every program using \xlifepp begins by a call to the \lstinline{init} function, taking up to 4 key/value arguments but only 2 are relevant for users:
\begin{description}
\item[\param{\_verbose}] integer to set the verbose level. Default value is 1.
\item[\param{\_lang}] enum to set the language for print and log messages. Possible values are \emph{en} for English, \emph{fr} for French, \emph{de} for German, or \emph{es} for Spanish. Default value is \emph{en}.
\end{description}

Furthermore, the \lstinline{init} function loads functionalities linked to the trace of where such messages come from. If this function is not called, \xlifepp cannot work !!!

\inputCode[firstline=12, lastline=12]{helmholtz.cpp}

See \xautoref{ch.init} to learn how to define command line options or options files specific to your program and how to use them.

\item[lines 13-14:] The mesh will be generated on the unit square geometry with 11 nodes per edge. Arguments of a geometry are given with a key/value system. \param{\_origin} is the bottom left front vertex of \class{SquareGeo}. Next, we specify the mesh element type (here triangle), the mesh element order (here 1), the mesh tool (here structured). The main mesh tools are 'structured' for simple geometries (rectangle, cube, \ldots) and 'gmsh' for general geometries. See \xautoref{ch.meshdef} for more examples of mesh definitions.

\inputCode[firstline=13, lastline=14]{helmholtz.cpp}

\item[line 15:] The main domain, named "Omega" in the mesh, is defined.
\inputCode[firstline=15, lastline=15]{helmholtz.cpp}
\item[line 16:] A finite element space is generally a space of polynomial functions on elements, triangles here only. Here \emph{sp} is defined as the space of continuous functions which are affine on each triangle \(T_k\) of the domain \(\Omega\), usually named \(V_h\). The dimension of such a space is finite, so we can define a basis.

\[
sp(\Omega,P1)=\left\{ w(x,y) \mbox{ such that } \exists (w_1,\ldots, w_N) \in \mathbb{R}^N, w(x,y) = \sum_{i=1}^N w_k \varphi_k(x,y)\right\}
\]

where \(N\) is the space dimension, i.e. the number of nodes (the number of vertices here).

Currently, \xlifepp implements the following Lagrange elements : \(P_k\) on segment, triangle and tetrahedron, \(Q^k\) on quadrangle and hexahedron, \(O_k\) on prism and pyramid (see \xautoref{s.meshbuild} for more details). Other non Lagrange elements are also available.

\inputCode[firstline=16, lastline=16]{helmholtz.cpp}
\item[lines 17-20:] The unknown \(u\) here is an approximation of the solution of the problem. \(v\) is declared as test function. This comes from the variational formulation of \autoref{eq.helmholtz} : multiplying both sides of equation and integrating over \(\Omega\), we obtain :

\[
-\int_{\Omega} v \Delta u dx dy +\int_{\Omega} v u dx dy = \int_{\Omega} v f dx dy
\]

Then, using Green's formula, the problem is converted into finding \(u\) such that :

\begin{equation}
\label{eq.helmholtz.vf}
a(u,v) = \int_{\Omega} \nabla u \cdot \nabla v dx dy + \int_{\Omega} u v dx dy = \int_{\Omega} f v dx dy = l(v)
\end{equation}

The 4 next lines in the program declare \(u\) and \(v\) and define the forms \(a\) and \(l\).

\inputCode[firstline=17, lastline=20]{helmholtz.cpp}
Please notice that:

\begin{itemize}
\item The test function is defined from the unknown. The reason is that the test function is dual to the unknown. Through the unknown, v is also defined on the same space.
\item The right-hand side needs the definition of the function \(f\). Such function can be defined as a classical C++ function, but with a particular prototype. In this example, \(f\) (i.e. \(cosxcosy\)) is a scalar function. So it takes 2 arguments : the first one is a \class{Point}, containing coordinates \(x\) and \(y\). The second one is optional and contains parameters to use inside the function. Here, the \class{Parameters} object is not used. At last, as a scalar function, it returns a \class{Real}, but it should be a \class{Complex} or a real/complex vector (\class{Reals}, \class{Complexes}).
\inputCode[firstline=4, lastline=9, deletekeywords={[3]x, y}]{helmholtz.cpp}
\end{itemize}
\item[lines 21-22:] The previous definitions are a description of the variational form. Now, we have to build the matrix and the right-hand side vector which are the algebraic representations of the  linear forms in the finite element space. This is done by the first 2 following lines.
\inputCode[firstline=21, lastline=22]{helmholtz.cpp}
\item[lines 23-24:] Once Matrix and vector being assembled, you can now choose the solver you want. Here, a conjugate gradient solver is used with an initial guess vector equal to the constant vector 1.

\xlifepp offers you a various choice of direct or iterative solvers:
\begin{itemize}
\item \(LU\), \(LDU\), \(LL^t\), \(LDL^t\), \(LDL^*\) factorizations
\item BICG, BiCGStab, CG, CGS, GMRES, QMR, Sor, SSor, solvers
\item internal eigen solver
\item interfaces to external packages such as \umfpack, \arpack
\end{itemize}

See \xautoref{ch.solvers} for more details.

\inputCode[firstline=23, lastline=24]{helmholtz.cpp}
\item[line 25:] To save the solution, \xlifepp provides an export to Paraview format file (vtu).
\inputCode[firstline=25, lastline=25]{helmholtz.cpp}
\item[line 26:] This is the end of the program. A "main" function always ends with this line.
\inputCode[firstline=26, lastline=26]{helmholtz.cpp}
\end{description}
