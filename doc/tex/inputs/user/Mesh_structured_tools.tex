%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex

When the {\bfseries structured mesh generator} is chosen (mg = {\tt\_structured}), one can create a mesh of order~1:
\begin{itemize}
\item of a segment,
\item of a parallelogram with triangles or quadrangles,
\item of a parallelepiped with hexahedra, prisms or pyramids.
\end{itemize}
One has to declare an object of type \class{Geometry}, more precisely of one of its derived type \class{Segment}, \class{Parallelogram}, \class{Rectangle}, \class{Square}, \class{Parallelepiped}, \class{Cuboid} or \class{Cube} using one of these constructors, that allow in particular to specifiy the mesh refinement by setting the number of points (nodes) on each edge, including the two endpoints.\\

Example 1:\\

\begin{lstlisting}
Strings sn(2);
sn[0] = "Sigma_1";
Mesh mesh1dP1(Segment(_xmin=0, _xmax=1, _nnodes=11, _side_names=sn), 1, _structured, "P1 mesh of [0,1], step=0.1");
\end{lstlisting}
This builds a mesh of the interval \([0,1]\) with 10 subintervals. The boundary domain Sigma\_1, corresponding to the lower bound \(0\) of the interval, will be created ; the other one will not be created since it has no name. The second argument is the mesh order ; in the case of a structured mesh, the only possible value is 1.\\

It can be noticed that the segment may have been defined by two points in the plane or in the space as well.\\

Example 2:\\

\begin{lstlisting}
Strings sn(4);
sn[0] = "Gamma_1"; sn[2] = "Gamma_2";
Mesh mesh2dP1(Rectangle(_xmin=0, _xmax=1, _ymin=1, _ymax=3, _nnodes=Numbers(3, 5), _side_names=sn), _triangle, 1, _structured, "P1 mesh of [0,1]x[1,3]");
\end{lstlisting}
This builds a mesh of the rectangle \([0,1]\times[1,3]\) with triangles. The interval \([0,1]\) is subdivided into 2 subintervals; the interval \([1,3]\) is subdivided into 4 subintervals. Only the two domains Gamma\_1 and Gamma\_2 will be created.
For a rectangle \([a,b]\times[c,d]\), the correspondence of the sidenames is the following:\\

sideNames[0] is \([a,b]\times{c}\), sideNames[1] is \({b}\times[c,d]\),

sideNames[2] is \([a,b]\times{d}\), sideNames[3] is \({a}\times[c,d]\).\\

Some options may be used to control the splitting of elementary rectangle int triangles :
\begin{lstlisting}
Rectangle R(_xmin=0,_xmax=1,_ymin=1,_ymax=3, _nnodes=Numbers(6, 10));
Mesh mesh2dP1l(R,_triangle,1,_structured); // default is _leftSplit
Mesh mesh2dP1r(R,_triangle,1,_structured,_rightSplit);
Mesh mesh2dP1a(R,_triangle,1,_structured,_alternateSplit);
Mesh mesh2dP1s(R,_triangle,1,_structured,_randomSplit);
\end{lstlisting}
\begin{figure}[H]
\centering
\includePict[width=16cm]{meshSplitP1.png}
\caption{left, right, alternate and random split of a P1 mesh of a rectangle}
\end{figure}

Example 3:\\

\begin{lstlisting}
Strings sn(4);
sn[0] = "Sigma_1"; sn[1] = "Sigma_2";
Mesh mesh2dQ1(Rectangle(_xmin=1, _xmax=2, _ymin=1, _ymax=3, _nnodes=Numbers(3, 5), _side_names=sn), _quadrangle, 1, _structured, "Q1 mesh of [1,2]x[1,3]");
\end{lstlisting}

This builds a mesh of the rectangle \([1,2]\times[1,3]\) with quadrangles. Only the two domains Sigma\_1 and Sigma\_2 will be created.
See example 2 for other commentaries.\\

Example 4:\\

\begin{lstlisting}
Strings sn( "z=1", "z=5", "y=1", "y=3", "x=0", "x=1");
Mesh mesh3dQ1(Cuboid(_xmin=0, _xmax=1, _ymin=1, _ymax=3, _zmin=1, _zmax=5, _nnodes=Numbers(3, 5, 9), _side_names=sn), _hexahedron, 1, _structured, "Q1 mesh of [0,1]x[1,3]x[1,5]");
\end{lstlisting}

This builds a mesh of the parallelepiped \([0,1]\times[1,3]\times[1,5]\) with hexahedra. The interval \([0,1]\) is subdivided into 2 subintervals ; the interval \([1,3]\) is subdivided into 4 subintervals ; the interval \([1,5]\) is subdivided into 8 subintervals. The 6 boundary domains will be created with their corresponding names.
For a parallelepiped \([a,b]\times[c,d]\times[e,f]\), the correspondence of the sidenames is the following:

sideNames[0] is \([a,b]\times[c,d]\times{e}\), sideNames[1] is \([a,b]\times[c,d]\times{f}\),

sideNames[2] is \([a,b]\times{c}\times[e,f]\), sideNames[3] is \([a,b]\times{d}\times[e,f]\),

sideNames[4] is \({a}\times[c,d]\times[e,f]\), sideNames[5] is \({b}\times[c,d]\times[e,f]\).
