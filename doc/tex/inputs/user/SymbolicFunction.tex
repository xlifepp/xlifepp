%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\section{\classtitle{SymbolicFunction}}

Some finite element constructions require passing as argument a \class{SymbolicFunction} object, that is a symbolic expression of a function. Such object is built in by writing any expression involving

\begin{itemize}
\item some constants (real or complex)
\item some variables : \var{x\_1}, \var{x\_2}, \var{x\_3}, \var{r\_}, \var{theta\_}, \var{phi\_}
\item some algebraic operators : \verb| + - * / ^ |(power)
\item some boolean operators : \verb?&& ||  <  <=  > >=  ==  !=  !? (not)
\item some mathematical functions : \verb|abs, realPart, imagPart, sqrt, squared, sin, cos, tan,| 
\verb|asin, acos, atan, sinh, cosh, tanh, asinh, acosh, atanh, exp, log, log10|
\end{itemize}

The result of a boolean expression is either 0 (false) or 1 (true). 

\begin{cpp11box}
The functions \verb|asinh, acosh, atanh| are only available when compiling with C++ 2011 standard.
\end{cpp11box}

To deal with a symbolic function is very easy. For instance consider the function 
\[f(x_1,x_2)=[1-x_1+\cos(x_2)]^3.\]
To define it as a symbolic function, write:

\begin{lstlisting}[deletekeywords={[3]x}]
SymbolicFunction fs = (1-x_1+cos(x_2))^3;  
\end{lstlisting}

To evaluate it, write:

\begin{lstlisting}[deletekeywords={[3]x}]
Real r = fs(-1,0);  
Complex c = fs(i_,0);  
\end{lstlisting}

When evaluating a symbolic function involving polar or spherical variables (say {\var r\_}, {\var theta\_} or {\var phi\_}), cartesian coordinates $(x,y,z)$ must be passed; $r$, $\theta$ and $\phi$ being evaluated as follows:
$$r=|(x,y,z)|,\quad \theta=\text{atan2}(y,x),\quad \phi=\text{asin}(z/r).$$
\begin{lstlisting}
SymbolicFunction fs= (1-r_*cos(2.*theta_))^3;
cout<<fs(0.5,0.5);  // gives 1 (r=0.5 and theta=pi/4)
\end{lstlisting}

You can mix algebraic and boolean expressions. Consider the following function:
\[
f(x_1)=\left\{
\begin{array}{cl}
e^{x_1} &  x_1 \leq 0\\
1 & x_1 >0
\end{array}
\right.
\]
To define it, write:

\begin{lstlisting}[deletekeywords={[3]x}]
SymbolicFunction fs = (x_1<=0)*exp(x_1)+(x_1>0);  
\end{lstlisting}
