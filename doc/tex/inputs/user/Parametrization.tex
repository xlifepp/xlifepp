%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\section{\classtitle{Parametrization}}\label{s.paramatrization}

Besides canonical geometries, \xlifepp proposes to define geometries from parametrization of its boundaries or a mapping of a canonical domain. This is the purpose of the \class{Parametrization} object that deals with function \(f\):

\[
\left|\begin{array}{ccc}
\hat{\Omega}\subset \mathbb{R}^p & \longrightarrow &\Omega \subset \mathbb{R}^n\\
x&\longrightarrow& f(x)
\end{array}\right.
\]

where \(x\) is the parameter (say \(t\), or \((u,v)\)) and \(p\leq n\). When \(p=1\), parametrizations concern curves, when \(p=2\) parametrizations concerns surfaces. Note that \(p\) can be equal to \(n\). In that case, \(f\) is a mapping (for instance the polar mapping). In practice, interesting situations correspond to 2D/3D curve or 3D surface.\\

There exist two ways to construct a \class{Parametrization} object, either by giving an explicit C++ function or using symbolic functions.\\

The explicit C++ function must be  of the form:\\

\begin{lstlisting}[deletekeywords={[3]x}]
RealVector f(const Point& x, Parameters& pars, DiffOpType d)
\end{lstlisting}

where \verb|x| is the parameter vector, \verb|pars| a list of additional parameters (do not confuse with the parameter vector) and \verb|d| a differential operator (one of \verb|_id,_d1,_d2,_d11, \ldots|) telling which derivative is requested.

As a first example, consider the circle arc

\[
\Omega=\left\{(R\cos t,R\sin t),\ t\in\hat{\Omega}=[-\frac{\pi}{4}, \frac{\pi}{4}] \right\}.
\]

The \xlifepp code looks like:\\

\begin{lstlisting}[deletekeywords={[3]x}]
RealVector fc(const Point& x, Parameters& pars, DiffOpType d)
{ Real R=pars("R"), t=x(1);
  if(d==_id) return RealVector(R*cos(t),R*sin(t)); 
  parfun_error("fc",d);
}
  \ldots
  Parameters pars(1.,"R");
  Parametrization pc(-pi_/4, pi_/4, fc, pars, "arc");
\end{lstlisting}

The parametrization name (here "arc") is not mandatory.\\

When functions are quite simple, symbolic functions may be used:\\

\begin{lstlisting}[deletekeywords={[3]x}]
Real R=1.;
Parametrization pc(-pi_/4, pi_/4, R*cos(x_1), R*sin(x_1), Parameters(), "arc");
\end{lstlisting}

In that case, an empty parameter is passed to the parametrization and \verb|R| is passed to the symbolic functions.\\

To construct parametrizations, the following syntaxes are available:\\

\begin{lstlisting}[deletekeywords={[3]x, name}]
//1D parametrization on segment [a,b]
Parametrization pa(a,b,c++_fun,parameters,name);           
Parametrization pa(a,b,symb_fun,symb_fun,parameters,name); 
Parametrization pa(a,b,symb_fun,symb_fun,symb_fun,parameters,name);     
//2D parametrization on rectangle[a,b]x[c,d]
Parametrization pa(a,b,c,d,c++_fun,parameters,name);   
Parametrization pa(a,b,c,d,symb_fun,symb_fun,symb_fun,parameters,name); 
//general constructors
Parametrization pa(geometry,c++_fun,parameters,name);      
Parametrization pa(geometry,symb_fun,symb_fun,parameters,name);
Parametrization pa(geometry,symb_fun,symb_fun,symb_fun,parameters,name);
\end{lstlisting}

Once constructed, the \class{Parametrization} can compute the point at a parameter:\\

\begin{lstlisting}[deletekeywords={[3]x}]
Parametrization pc(-pi_/4, pi_/4, cos(x_1), sin(x_1), Parameters(), "arc");
Point P=pc(pi_\8);
Parametrization ps(-pi_/2,pi_/2,-pi_,pi_,cos(x_1)*cos(x_2),cos(x_1)*sin(x_2),sin(x_1),
      Parameters(),"unit sphere");
Point Q=ps(0,0);
\end{lstlisting}

If derivatives are available, it is also possible to compute some important geometrical quantities at parameter : the local lengths (ds), the curvilinear abscissas (s), the normal vector (N), the binormal vector (bN, only for 3D curve), the tangent vector (T), the bitangent vector (b, only for 3D surface), the curvatures (principal curvatures in 3D)\footnote{Mathematical definitions are available in the developer documentation}:\\

\begin{lstlisting}[deletekeywords={[3]x}]
RealVector Parametrization.lengths(x1,[x2])
Real Parametrization.length(x1,[x2])
Real Parametrization.bilength(x1,x2)          //only 2D parametrization
RealVector Parametrization.curabcs(x1,[x2])
Real Parametrization.curabc(x1,[x2])
Real Parametrization.bicurabc(x1,x2)          //only 2D parametrization
RealVector Parametrization.normals(x1,[x2])
RealVector Parametrization.normal(x1,[x2])
RealVector Parametrization.binormal(x1)       //only 3D curve
RealVector Parametrization.tangents(x1,[x2])
RealVector Parametrization.tangent(x1,[x2])
RealVector Parametrization.bitangent(x1,x2)   //only 3D surface
RealVector Parametrization.curvatures(x1,[x2])
Real Parametrization.curvature(x1,[x2])
Real Parametrization.bicurvature(x1,x2)       //only 3D surface
Real Parametrization.gaussCurvature(x1,x2)    //only 3D surface
Real Parametrization.meanCurvature(x1,x2)     //only 3D surface
real_t normalCurvature(x1,x2,d)               //normal curvature relatively to d (3D surface)
RealVector curvatures((x1,x2,d)               //Gauss, mean and normal curvature (3D surface)
Real Parametrization.torsion(x1)              //only 3D curve
RealMatrix weingarten(x1,x2)                  //only 3D surface
RealMatrix jacobian(x1,[x2])                  //jacobian matrix
RealVector metricTensor(x1,[x2])               //metric tensor
RealVector christoffel (x1,[x2])               //Christoffel symbols  (3D surface)
\end{lstlisting}

Computations of most of these geometrical quantities require first and second derivatives of the parametrization. Only the computation of the torsion requires third derivatives. These derivatives are available when using symbolic functions but have to be given when using a C++ function, for instance:\\

\begin{lstlisting}[deletekeywords={[3]x}]
RealVector fc(const Point& x, Parameters& pars, DiffOpType d)
{ Real R=pars("R"), t=x(1);
  switch (d)
  {case _id  : return RealVector( R*cos(t), R*sin(t)); //no derivative
   case _d1  : return RealVector(-R*sin(t), R*cos(t)); //first derivative
   case _d11 : return RealVector(-R*cos(t),-R*sin(t)); //second derivative
   case _d111: return RealVector( R*sin(t),-R*cos(t)); //third derivative
   default : parfun_error("fc",d); 
  }
  return RealVector();
}
\end{lstlisting}

Note that curvilinear abcissas are  integrals. In \xlifepp, these integrals are approximated using a trapeze method with 1000 points and a linear interpolation. This number of points may be changed using:\\

\begin{lstlisting}[deletekeywords={[3]x}]
Parametrization pc(-pi_/4, pi_/4, cos(x_1), sin(x_1), Parameters(), "arc");
pc.np = 500; // change the number of points of trapeze method
\end{lstlisting}

Note that it is also possible to associate C++ functions or symbolic functions to the lengths, the curvilinear abscissas, the normals, the tangents, the curvatures using the setlength, setcurabc, setnormal, settangent and setcurvature function. The inverse of the parametrization may be called if the user has given it either in C++ form or in symbolic form:\\

\begin{lstlisting}[deletekeywords={[3]x}]
\ldots
RealVector invfc(const Point& p, Parameters& pars, DiffOpType d)
{  if(d!=_id) parfun_error("invfc",d);
   return RealVector(1,atan2(tp(2),tp(1)));
}
\ldots
Parametrization pc(-pi_/4, pi_/4, fc, pars, "arc");
pc.setinvParametrization(invfc);
Real t=pc.toRealParameter(Point(0.,0.)); //get parameter related to (0,0)
\end{lstlisting}
\vspace{.2cm}
All canonical geometries have an internal parametrization defined either on \([0,1]\) or \([0,1]\times [0,1] \) or \([0,1]\times [0,1]\times [0,1]\). See the developer documentation for the expression of the canonical parametrizations.\\

When geometry is composite, the parametrization is managed by the \class{PiecewiseParametrization} class inheriting from \class{Parametrization} class. The piecewise parametrization is the collection of "local" parametrizations related to canonical geometries that compose the composite geometry. It is not globally \(C0\), but it is possible to travel through with the help of an additional map that handles the neighbor parametrizations (see the developer documentation for details). Most of geometrical functions are available.
 