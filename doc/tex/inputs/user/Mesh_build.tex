%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\xlifepp owns some constructors that allow to create meshes based on simple geometries in one, two or three dimensions. The constructors to use are defined as follows:\\

\begin{lstlisting}[deletekeywords={[3]order,name}]
//! constructor from 1D geometries
Mesh (const Geometry& g, Number order = 1, MeshGenerator mg = _defaultGenerator, 
      const String& name = "");
//! constructor from 2D or 3D geometries
Mesh (const Geometry& g, ShapeType sh, Number order = 1, MeshGenerator mg = _defaultGenerator, 
      const String& name = ""); 
//! constructor from 2D or 3D geometries with additional options (up to 3)
Mesh (const Geometry& g, ShapeType sh, Number order = 1, MeshGenerator mg = _defaultGenerator,
      MeshOption o1, const String& name = ""); 
Mesh (const Geometry& g, ShapeType sh, Number order = 1, MeshGenerator mg = _defaultGenerator, 
      MeshOption o1, MeshOption o2, const String& name = ""); 
Mesh (const Geometry& g, ShapeType sh, Number order = 1, MeshGenerator mg = _defaultGenerator, 
      MeshOption o1, MeshOption o2, MeshOption o3, const String& name = ""); 
\end{lstlisting}

The arguments are:

\begin{description}
\item[\var{g}] is the geometrical object to be meshed (such as \class{Segment}, \class{Quadrangle}, \class{Hexahedron}, \dots, all of them being declared in the file {\ttfamily geometries.hpp}), 
\item[\var{sh}] is the shape of the mesh elements (\_segment, \_triangle, \_quadrangle, \_tetrahedron, \_hexahedron),
\item[\var{order}] is the interpolation order of the mesh elements ; it depends on the way the mesh is generated (see below),
\item[\var{mg}] defines the way the object is computed: 
\begin{description}
\item[\var{\_structured}]: a structured mesh can be built for canonical geometries only (\class{Segment}, \class{Parallelogram},  \class{Rectangle}, \class{Square}, \class{Parallelepiped}, \class{Cuboid} and \class{Cube}); the order of the mesh is necessarily one,
\item[\var{\_subdiv}]: an unstructured mesh can be built using the so-called subdivision basic algorithm for the following geometries : \class{Cube}, \class{Ball}, \class{RevTrunk}, \class{RevCone}, \class{RevCylinder}, \class{Disk} and \class{SetOfElems};
the order can be any integer \(k>0\),
\item[\var{\_gmsh}]: for more complicated geometries, with a nested call of the \gmsh software; the order depends on the chosen shape (refer to \gmsh documentation).
\end{description}
\item[\var{oi}] defines some additional options proposed by some mesh methods (mesh from parametrization and structured mesh of a parallelogram with triangles) and defined in the following enumeration:
\begin{lstlisting}[deletekeywords={[3]order,name}]
enum MeshOption{_defaultMeshOption=0, _unstructuredMesh, _structuredMesh,
                _leftSplit ,_rightSplit, _randomSplit, _alternateSplit}
\end{lstlisting}
\item[\var{name}] defines the mesh name.
\end{description}

Examples:\\

\begin{lstlisting}
// P1 structured mesh of segment [0,1] with 10 nodes. Domain is Omega
Mesh m1D(Segment(_xmin=0., _xmax=1., _nnodes=10, _domain_name="Omega"), 1, _structured);
// P1 unstructured mesh of disk of center (0,0,1) and radius 2.5 with 40 nodes. Domain is Omega and side domain is Gamma
Mesh m2D(Disk(_center=Point(0.,0.,1.), _radius=2.5, _nnodes=40, _domain_name="Omega" _side_names="Gamma"), _triangle, 1, _subdiv);
// Q2 unstructured mesh (using gmsh) of cube [0,2]x[0,1]x[0,4] with 20 nodes on x edges, 10 nodes on y edges and 40 nodes on z edges 
Mesh m3D(Cube(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v4=Point(0.,1.,0.), _v5=Point(0.,0.,4.), _nnodes=Numbers(20,10,40), _domain_name="Omega"), _hexahedron, 2, _gmsh);
\end{lstlisting}

This is described in more detail in next paragraph.\\

Moreover, it is possible to subdivide an existing mesh of order 1: a new mesh is created using the subdivision algorithm mentioned above. The corresponding constructor is defined as follows:\\

\begin{lstlisting}[deletekeywords={[3]order}]
//! constructor from a mesh to be subdivided
Mesh (const Mesh& msh, Number nbsubdiv, Number order = 1);
\end{lstlisting}

The arguments are:

\begin{description}
\item[\var{msh}] is the input mesh object, i.e. the given mesh to be subdivided ; it should consist of triangles, quadrangles,
tetrahedra or hexahedra ;
\item[\var{nbsubdiv}] is the number of subdivisions to be performed ;
\item[\var{order}] is the order of the final mesh ; its default value is 1.
\end{description}

Example:\\

\begin{lstlisting}
Mesh m1("mesh.msh", "My Mesh", msh);
Mesh subm1(m1,2);
\end{lstlisting}

This builds a mesh subm1 which is obtained by subdividing twice the mesh m1, itself read from the file "mesh.msh".

Once a mesh is created, it is possible to get information about what it is made of using the function printInfo,
which displays on the terminal general information about the mesh: characteristic data, domains, \ldots:\\

\begin{lstlisting}
Mesh m1("mesh.msh", "My Mesh", msh);
m1.printInfo();
\end{lstlisting}

\begin{focusbox}
If you want to mesh a 2D geometry with segment elements, only borders will be meshed. The same goes for 3D geometries mesh with triangles or quadrangles.
\end{focusbox}
