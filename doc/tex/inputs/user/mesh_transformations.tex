%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
% Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex

Geometrical transformations on meshes work as on geometries. Please see \autoref{s.geotransfo} for definition and use of transformations routines.

Then, if you want to apply a transformation and modify the input object, you can use one of the following functions:\\

\begin{lstlisting}
//! apply a geometrical transformation on a Mesh
Mesh& Mesh::transform(const Transformation& t);
//! apply a translation on a Mesh
Mesh& Mesh::translate(std::vector<Real> u = std::vector<Real>(3,0.));
Mesh& Mesh::translate(Real ux, Real uy = 0., Real uz = 0.);
//! apply a rotation 2d on a Mesh
Mesh& Mesh::rotate2d(const Point& c = Point(0.,0.), Real angle = 0.);
//! apply a rotation 3d on a Mesh
Mesh& Mesh::rotate3d(const Point& c = Point(0.,0.,0.), std::vector<Real> u = std::vector<Real>(3,0.), Real angle = 0.);
Mesh& Mesh::rotate3d(Real ux, Real uy, Real angle);
Mesh& Mesh::rotate3d(Real ux, Real uy, Real uz, Real angle);
Mesh& Mesh::rotate3d(const Point& c, Real ux, Real uy, Real angle);
Mesh& Mesh::rotate3d(const Point& c, Real ux, Real uy, Real uz, Real angle);
//! apply a homothety on a Mesh
Mesh& Mesh::homothetize(const Point& c = Point(0.,0.,0.), Real factor = 1.);
Mesh& Mesh::homothetize(Real factor);
//! apply a point reflection on a Mesh
Mesh& Mesh::pointReflect(const Point& c = Point(0.,0.,0.));
//! apply a reflection2d on a Mesh
Mesh& Mesh::reflect2d(const Point& c = Point(0.,0.), std::vector<Real> u = std::vector<Real>(2,0.));
Mesh& Mesh::reflect2d(const Point& c, Real ux, Real uy = 0.);
//! apply a reflection3d on a Mesh
Mesh& Mesh::reflect3d(const Point& c = Point(0.,0.,0.), std::vector<Real> u = std::vector<Real>(3,0.));
Mesh& Mesh::reflect3d(const Point& c, Real ux, Real uy, Real uz = 0.);
\end{lstlisting}

For instance:\\

\begin{lstlisting}
Mesh m;
m.translate(0.,0.,1.);
\end{lstlisting}

However, if you want now to create a new \class{Mesh} by applying a transformation on a \class{Mesh}, you should use one of the following functions instead:\\

\begin{lstlisting}
//! apply a geometrical transformation on a Mesh (external)
Mesh transform(const Mesh& m, const Transformation& t);
//! apply a translation on a Mesh (external)
Mesh translate(const Mesh& m, std::vector<Real> u = std::vector<Real>(3,0.));
Mesh translate(const Mesh& m, Real ux, Real uy = 0., Real uz = 0.);
//! apply a rotation 2d on a Mesh (external)
Mesh rotate2d(const Mesh& m, const Point& c = Point(0.,0.), Real angle = 0.);
//! apply a rotation 3d on a Mesh (external)
Mesh rotate3d(const Mesh& m, const Point& c = Point(0.,0.,0.), std::vector<Real> u = std::vector<Real>(3,0.), Real angle = 0.);
Mesh rotate3d(const Mesh& m, Real ux, Real uy, Real angle);
Mesh rotate3d(const Mesh& m, Real ux, Real uy, Real uz, Real angle);
Mesh rotate3d(const Mesh& m, const Point& c, Real ux, Real uy, Real angle);
Mesh rotate3d(const Mesh& m, const Point& c, Real ux, Real uy, Real uz, Real angle);
//! apply a homothety on a Mesh (external)
Mesh homothetize(const Mesh& m, const Point& c = Point(0.,0.,0.), Real factor = 1.);
Mesh homothetize(const Mesh& m, Real factor);
//! apply a point reflection on a Mesh (external)
Mesh pointReflect(const Mesh& m, const Point& c = Point(0.,0.,0.));
//! apply a reflection2d on a Mesh (external)
Mesh reflect2d(const Mesh& m, const Point& c = Point(0.,0.), std::vector<Real> u = std::vector<Real>(2,0.));
Mesh reflect2d(const Mesh& m, const Point& c, Real ux, Real uy = 0.);
//! apply a reflection3d on a Mesh (external)
Mesh reflect3d(const Mesh& m, const Point& c = Point(0.,0.,0.), std::vector<Real> u = std::vector<Real>(3,0.));
Mesh reflect3d(const Mesh& m, const Point& c, Real ux, Real uy, Real uz = 0.);
\end{lstlisting}

For instance:\\

\begin{lstlisting}
Mesh m1;
Mesh m2=translate(m1, 0.,0.,1.);
\end{lstlisting}

Applying a transformation on a \class{Mesh} object means applying the transformation on the underlying \class{Geometry} object and adding the suffix "\_{}prime" to the mesh name and the domain names.
