%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
% Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex

\xlifepp allows you to apply geometrical transformations on \class{Mesh},  \class{Geometry} and \class{Geometry} children objects. The main type is \class{Transformation}. It can be a canonical transformation or a composition of transformations.

\subsection{Canonical transformations}

In the following, we will consider straight lines and planes.

A straight line is fully defined by a point and a direction. The latter is a vector of components (2 or 3). This is a reason why we will write a straight line as follows : \(\left(\Omega, \vec{d}\right)\)

A plane is fully defined by a point and a normal vector. This is a reason why we will write a plane as follows : \(\left[\Omega, \vec{n}\right]\)

\subsubsection{Translations}

Point B is the image of point A by a translation of vector \(\vec{u}\) if and only if
\[
\overrightarrow{AB}=\vec{u}
\]

A translation can be defined by an STL vector (size 2 or 3) or its components:\\

\begin{lstlisting}
Vector<Real> u;
Real ux, uy, uz;
Translation t1(u), t2(ux, uy), t3(ux, uy, uz);
\end{lstlisting}

\begin{focusbox}
{\ttfamily u} can be omitted. If so, its default value is the 3d zero vector. {\ttfamily uy} and {\ttfamily uz} can be omitted too. If so, their default value is 0.
\end{focusbox}

\begin{ideabox}
As the \class{Vector} class inherits from \cmd{std::vector} you can use it in place of  \class{Vector} because all prototypes are based on \cmd{std::vector}.
\end{ideabox}

\subsubsection{2d rotations}

Point B is the image of point A by the 2d rotation of center \(\Omega\) and of angle \(\theta\) if and only if

\[
\overrightarrow{\Omega B} = \left(
\begin{array}{cc}
\cos\theta & -\sin\theta \\
\sin\theta & \cos\theta
\end{array}
\right) \overrightarrow{\Omega A}
\]

A 2d rotation is defined by a point and an angle (in radians):\\

\begin{lstlisting}
Point omega;
Real angle;
Rotation2d r(omega, angle);
\end{lstlisting}

\begin{focusbox}
{\ttfamily angle} can be omitted. If so, its default value is 0 and  {\ttfamily omega} can be omitted too. If so, its default value is the 3d zero point.
\end{focusbox}

\subsubsection{3d rotations}

Point B is the image of point A by the 3d rotation of axis \(\left(\Omega, \vec{d}\right)\) and of angle \(\theta\) (in radians) if and only if

\[
\overrightarrow{\Omega B} = \cos\theta \; \overrightarrow{\Omega A} + \left( 1 -\cos\theta\right) \; \overrightarrow{\Omega A} \cdot \vec{n} + \sin\theta \; \vec{n} \wedge \overrightarrow{\Omega A} \quad \text{(Rodrigues' rotation formulae)}
\]

where \(\displaystyle \vec{n}=\frac{\vec{u}}{||\vec{u}||}\) (the unitary direction).

\medskip

The direction can be defined by an STL vector or by its components:\\

\begin{lstlisting}[deletekeywords={[3] dx, dy, dz}]
Point omega;
Vector<Real> d;
Real dx, dy, dz;
Real theta;
Rotation3d r1(omega, d, theta), r2(omega, dx, dy, dz, theta);
\end{lstlisting}

\begin{focusbox}
In the first syntax, {\ttfamily angle} can be omitted. If so, its default value is 0. and {\ttfamily d} can also be omitted. If so, its default value is the 3d zero vector. \\
In the second syntax, {\ttfamily dz} can be omitted too. If so, its default value is 0.
\end{focusbox}

\subsubsection{Homotheties}

Point B is the image of point A by the homothety of center \(\Omega\) and of factor \(k\) if and only if

\[
\overrightarrow{\Omega B} = k \; \overrightarrow{\Omega A}
\]

\begin{lstlisting}
Point omega(1.,2.,3.);
Real k=2.;
Homothety h(omega, k);
\end{lstlisting}

\begin{focusbox}
{\ttfamily factor} can be omitted. If so, its default value is 0. and {\ttfamily omega} can also be omitted. If so, its default value is the 3d zero vector.
\end{focusbox}

\subsubsection{Point reflections}

Point B is the image of point A by the point reflection of center \(\Omega\) if and only if
s
\[
\overrightarrow{\Omega B} = - \overrightarrow{\Omega A}
\]

It is a homothety of factor -1 and same center.

\begin{lstlisting}
Point omega(1.,2.,3.);
PointReflection h(omega); // omega can still be omitted, as for homothety
\end{lstlisting}

\subsubsection{2d reflections}

Point B is the image of point A by the 2d reflection of axis \(\left(\Omega,\vec{d}\right)\) if and only if

\[
\overrightarrow{AB} =  2 \overrightarrow{AH} \quad \text{where \(H\) is the orthogonal projection of \(A\) on \(\left(\Omega,\vec{d}\right)\)}
\]

\begin{lstlisting}[deletekeywords={[3] dx, dy}]
Point omega(1.,2.,3.);
Vector<Real> d(1.,0.,0.);
Real dx=1., dy=0.;
Reflection2d r1(omega, d), r2(omega, dx, dy);
\end{lstlisting}

\begin{focusbox}
In the first syntax, {\ttfamily d} can be omitted. If so, its default value is the 2d zero vector and {\ttfamily omega} can be omitted. If so, its default value is the 2d zero point.
\end{focusbox}

\subsubsection{3d reflections}

Point B is the image of point A by the 2d reflection of plane \(\left[\Omega,\vec{n}\right]\) if and only if

\[
\overrightarrow{AB} =  2 \overrightarrow{AH} \quad \text{where \(H\) is the orthogonal projection of \(A\) on \(\left[\Omega,\vec{n}\right]\)}
\]

\begin{lstlisting}[deletekeywords={[3] nx, ny, nz}]
Point omega(1.,2.,3.);
Vector<Real> n;
Real nx, ny, nz;
Reflection3d r1(omega, n), r2(omega, nx, ny, nz);
\end{lstlisting}

\begin{focusbox}
In the first syntax, {\ttfamily n} can be omitted. If so, its default value is the 3d zero vector and {\ttfamily omega} can be omitted. If so, its default value is the 3d zero point.
\end{focusbox}

\subsection{Composition of transformations}

To define a composition of transformations, you can use the operator * between canonical transformations, an is the following example:\\

\begin{lstlisting}
Rotation2d r1(Point(0.,0.), 120.);
Reflection2d r2(Point(1.,-1.), 1.,2.5, -3.);
Translation t1(-1.,4.);
Homothety h (Point(-1.,0.), -3.2);
Transformation t = r1*h*r2*t1;
\end{lstlisting}

Composition * has to be understood as usual composition operator \(\circ\) : {\ttfamily t(P)=r1(h(r2(t1(P))))}.

\subsection{Applying transformations}

\subsubsection{How to apply a transformation ?}

In this paragraph, we will look at the \class{Cube} object, but you have same functions for any canonical or composite \class{Geometry}.

If you want to apply a transformation and modify the input object, you can use one of the following functions:\\

\begin{lstlisting}
//! apply a geometrical transformation on a Cube
Cube& Cube::transform(const Transformation& t);
//! apply a translation on a Cube
Cube& Cube::translate(std::vector<Real> u = std::vector<Real>(3,0.));
Cube& Cube::translate(Real ux, Real uy = 0., Real uz = 0.);
//! apply a rotation 2d on a Cube
Cube& Cube::rotate2d(const Point& c = Point(0.,0.), Real angle = 0.);
//! apply a rotation 3d on a Cube
Cube& Cube::rotate3d(const Point& c = Point(0.,0.,0.), std::vector<Real> u = std::vector<Real>(3,0.), Real angle = 0.);
Cube& Cube::rotate3d(Real ux, Real uy, Real angle);
Cube& Cube::rotate3d(Real ux, Real uy, Real uz, Real angle);
Cube& Cube::rotate3d(const Point& c, Real ux, Real uy, Real angle);
Cube& Cube::rotate3d(const Point& c, Real ux, Real uy, Real uz, Real angle);
//! apply a homothety on a Cube
Cube& Cube::homothetize(const Point& c = Point(0.,0.,0.), Real factor = 1.);
Cube& Cube::homothetize(Real factor);
//! apply a point reflection on a Cube
Cube& Cube::pointReflect(const Point& c = Point(0.,0.,0.));
//! apply a reflection2d on a Cube
Cube& Cube::reflect2d(const Point& c = Point(0.,0.), std::vector<Real> u = std::vector<Real>(2,0.));
Cube& Cube::reflect2d(const Point& c, Real ux, Real uy = 0.);
//! apply a reflection3d on a Cube
Cube& Cube::reflect3d(const Point& c = Point(0.,0.,0.), std::vector<Real> u = std::vector<Real>(3,0.));
Cube& Cube::reflect3d(const Point& c, Real ux, Real uy, Real uz = 0.);
\end{lstlisting}

For instance,

\begin{lstlisting}
Cube c;
c.translate(0.,0.,1.);
\end{lstlisting}

If you want now to create a new \class{Cube} by applying a transformation on a \class{Cube}, you should use one of the following functions instead:\\

\begin{lstlisting}
//! apply a geometrical transformation on a Cube (external)
Cube transform(const Cube& m, const Transformation& t);
//! apply a translation on a Cube (external)
Cube translate(const Cube& m, std::vector<Real> u = std::vector<Real>(3,0.));
Cube translate(const Cube& m, Real ux, Real uy = 0., Real uz = 0.);
//! apply a rotation 2d on a Cube (external)
Cube rotate2d(const Cube& m, const Point& c = Point(0.,0.), Real angle = 0.);
//! apply a rotation 3d on a Cube (external)
Cube rotate3d(const Cube& m, const Point& c = Point(0.,0.,0.), std::vector<Real> u = std::vector<Real>(3,0.), Real angle = 0.);
Cube rotate3d(const Cube& m, Real ux, Real uy, Real angle);
Cube rotate3d(const Cube& m, Real ux, Real uy, Real uz, Real angle);
Cube rotate3d(const Cube& m, const Point& c, Real ux, Real uy, Real angle);
Cube rotate3d(const Cube& m, const Point& c, Real ux, Real uy, Real uz, Real angle);
//! apply a homothety on a Cube (external)
Cube homothetize(const Cube& m, const Point& c = Point(0.,0.,0.), Real factor = 1.);
Cube homothetize(const Cube& m, Real factor);
//! apply a point reflection on a Cube (external)
Cube pointReflect(const Cube& m, const Point& c = Point(0.,0.,0.));
//! apply a reflection2d on a Cube (external)
Cube reflect2d(const Cube& m, const Point& c = Point(0.,0.), std::vector<Real> u = std::vector<Real>(2,0.));
Cube reflect2d(const Cube& m, const Point& c, Real ux, Real uy = 0.);
//! apply a reflection3d on a Cube (external)
Cube reflect3d(const Cube& m, const Point& c = Point(0.,0.,0.), std::vector<Real> u = std::vector<Real>(3,0.));
Cube reflect3d(const Cube& m, const Point& c, Real ux, Real uy, Real uz = 0.);
\end{lstlisting}

For instance,

\begin{lstlisting}
Cube c1;
Cube c2=translate(c1,0.,0.,1.);
\end{lstlisting}

\begin{focusbox}
Of course, you can not apply a 2d rotation or a 2d reflection for geometries defined by 3d points !
\end{focusbox}

\subsubsection{What does a transformation really do ?}

Applying a transformation on an object means computing the image of each point defining the object. But it can also change names.

When you create a new object by applying a transformation on an object, names are modified. Indeed, the transformation add a suffix "\_{}prime". It concerns geometry names and sidenames.

\begin{focusbox}
When you transform a \class{Geometry}, it also applies the transformation on the underlying bounding box.
\end{focusbox}
