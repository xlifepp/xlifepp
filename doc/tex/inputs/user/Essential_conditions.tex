%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\section{Essential conditions}

Essential conditions are conditions that appear in spaces involved in variational problem. The most common one is the Dirichlet condition on a boundary : \(u=0\) on \(\Gamma\) (homogeneous)  or \(u=g\) on \(\Gamma\) (non-homogeneous). But there are others : transmission condition on a boundary, periodic condition between two boundaries, null average on a domain, \ldots \xlifepp provides a symbolic description of such conditions based on operator's stuff already described.\\ 
 
The general syntax of an essential condition is the following
\begin{center}
(a1 \(\otimes\) op1(u1))|D1 +/- (a2 \(\otimes\) op2(u2))|D2 = f 
\end{center}
where 
\begin{itemize}\setlength\itemsep{-2pt}
\item a1, a2 are some constants
\item \(\otimes\) is any algebraic operator (\var{*}, \var{|}, \var{\%}, \verb?^?)
\item op1, op2 are some operators on unknown
\item u1, u2 are some unknowns
\item D1, D2 are some domains
\item f is a constant or a function
\end{itemize}
Some classic scalar expressions are :
\begin{center}
\begin{tabular}{|c|c|c|}
\hline \rule[-2ex]{0pt}{5.5ex} u|D = 0  & homogeneous Dirichlet condition \\ 
\hline \rule[-2ex]{0pt}{5.5ex} u|D = f  & non-homogeneous Dirichlet condition \\ 
\hline \rule[-2ex]{0pt}{5.5ex} u1|D - u2|D = 0 & homogeneous transmission condition  \\ 
\hline \rule[-2ex]{0pt}{5.5ex} u|D1 - u|D2 = 0 & homogeneous periodic condition  \\ 
\hline \rule[-2ex]{0pt}{5.5ex} u|D1 - g * u|D2 = 0 & quasi periodic condition (g function) \\ 
\hline 
\end{tabular} 
\end{center}

Obviously, syntax supports more than conditions that the program can really deal with !
  
\begin{warningbox}
As the operator priority rules are the C++ rules, omitted parenthesis may induce some hazardous compilation errors. In doubt, use parenthesis.
\end{warningbox}
 
To declare essential condition, users have to instantiate \class{EssentialConditions} object, which handles a set of conditions:
 
\begin{lstlisting}
Strings sn("y=0", "y=1", "x=0", "x=1");
Mesh mesh2d(Square(_origin=Point(0.,0.), _length=1, _nnodes=10, _side_names=sn), _triangle, 1, _structured);
Domain omega=mesh2d.domain("Omega");
Domain sigmaM=mesh2d.domain("x=0");
Domain sigmaP=mesh2d.domain("x=1");
Space V(_domain=omega, _interpolation=P1, _name="V");
Unknown u(V, "u");
EssentialConditions ecs = (u|sigmaM = 1);
\end{lstlisting}

or using a function:

\begin{lstlisting}
Real un(const Point& P, Parameters& pa = defaultParameters)
{
 return 1.;
}
  
EssentialConditions ecs = (u|sigmaM = un);
\end{lstlisting}

To concatenate conditions, use the operator \& :

\begin{lstlisting}
EssentialConditions ecs = (u|sigmaM = 1) & (u|sigmaP = 1) ;
\end{lstlisting}

It is possible to mix conditions. Here is a case with two unknowns related by a transmission condition:

\begin{lstlisting}
\ldots
Domain sigmaM=mesh2d.domain("x=0");
Domain sigmaP=mesh2d.domain("x=1");
Domain gamma=mesh2d.domain("x=1/2- or x=1/2+");
Space VM(_domain=omegaM, _interpolation=P2, _name="VM");
Unknown uM(VM, "u-");
Space VP(_domain=omegaP, _interpolation=P2, _name="VP");
Unknown uP(VP, "u+");
EssentialConditions ecs = (uM|sigmaM = 1) & (uP|sigmaP = 1)
                        & ((uM|gamma) - (uP|gamma) = 0);
\end{lstlisting}

To deal with periodic condition, the map related to the two domains involved is required:

\begin{lstlisting}
Vector<Real> mapPM(const Point& P, Parameters& pa = defaultParameters)
{
   Point Q(P);
   Q(1)-=1;
   return Q;
}
\ldots
Domain omega=mesh2d.domain("Omega");
Domain sigmaM=mesh2d.domain("x=0");
Domain sigmaP=mesh2d.domain("x=1");
Domain gammaM=mesh2d.domain("y=0");
Domain gammaP=mesh2d.domain("y=1");
Space V(_domain=omega, _interpolation=P1, _name="V");
Unknown u(V,"u");

defineMap(sigmaP, sigmaM, mapPM);  
EssentialConditions ecs = (u|gammaM = 0) & (u|gammaP = 0)
                        & ((u|sigmaP) - (u|sigmaM) = 0);
\end{lstlisting}

\begin{focusbox}
\xlifepp uses a very powerful process to deal with essential condition: all constraints are merged in a unique linear constraints' system which is reduced using a QR algorithm. This process is able to detect redundant or conflicting constraints. When some are redundant, they are deleted. When some are in conflict, they are also deleted but the right-hand side related components are  averaged. For instance, this occurs when two Dirichlet conditions are not compatible at the intersection of two boundaries. In both cases a warning message is handled. It is the responsibility of user to check possible conflict.\\
The QR algorithm stops whenever the pivot (max of modulus of coefficients of a matrix column) is below a tolerance set by default to $100\varepsilon_{mach}\approx 2.10^{-14}$ in double precision. It may happen that this tolerance factor to be too small, it can be changed:
\begin{lstlisting}[deletekeywords={[3]}]
EssentialConditions ecs=...;
ecs.redundancyTolerance(1.e-10); 
\end{lstlisting}
\end{focusbox}

\begin{focusbox}
Contrary to the mathematical point of view, in \xlifepp the essential conditions are NOT attached to spaces but to algebraic representation of bilinear forms (see next section). This choice avoids defining multiple spaces.
\end{focusbox}

\subsubsection*{Advanced usage}

Most of essential conditions (except moment condition) are expressed as punctual relations. More precisely, the symbolic expression \(op(u)_{|D}\) is translated as 
\[op(u)(M_i)=\sum_j u_j\, op(w_j)(M_i),\text{ for some points } M_i\in D.\]
When using a Lagrange approximation, points \(M_i\) are simply the support of the Lagrange DoFs belonging to the domain \(D\). When non Lagrange element are concerned, virtual point supports, often defined in the reference element, are used. If there is no virtual point support, mesh nodes of the domain are used. \\
Using points that are shared by two elements (vertices for instance) may cause some troubles when operator or approximation involved is not continuous across element. To bypass this difficulty, it is possible to use internal points of elements instead of boundary points by modifying the property \var{ecMethod} of the essential condition:

\begin{lstlisting}
EssentialCondition ec = (ncross(u)|gamma = 0);
ec.ecMethod = _internalNodeEC;
EssentialConditions ecs = ec;
\end{lstlisting}

Be cautious, do not confuse \class{EssentialCondition} dealing with one condition and \class{EssentialConditions} dealing with several conditions.\\

Using internal nodes generates more punctual relations than using DoF points, but the reduction process deals with very well. 
