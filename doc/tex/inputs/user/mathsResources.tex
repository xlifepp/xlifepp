%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\section{Mathematical resources}

Mathematical resources are defined in \lib{mathsResources} library of \xlifepp. Classical kernels excepted (for Laplace, Helmholtz and Maxwell 2d or 3d problems), it contains a lot of stuff shown here.

\subsection{Random generators}

\xlifepp offers a uniform distribution generator and a normal distribution generator. As C++11 now provides a lot of random generators, contrary to C98 that proposes only a uniform distribution generator, the front end user functions will use prior the C++11 generators if C++11 is available (default behavior from gcc 6.1). These front end functions are the following:

\begin{lstlisting}
Real uniformDistribution(Real a=0., Real b=1.);    //real from uniform distribution on [a,b[
Real normalDistribution(Real mu=0., Real sigma=1.);//real from normal distribution (mu,sigma)
\end{lstlisting}
\cmd{a}, \cmd{b}, \cmd{mu}, \cmd{sigma} have the specified default values if they are not given. For instance to build a vector of random values of size 100:
\begin{lstlisting}
Reals us(100), ns(100);
for(Number i=1; i<=100; i++) us(i) = uniformDistribution();   
for(Number i=1; i<=100; i++) ns(i) = normalDistribution(2.,5.); // normal mu=2, sigma=5
\end{lstlisting}
\vspace{5mm}
Random vectors or random matrices can be constructed in a direct way : 
\begin{lstlisting}
Reals us = uniformDistribution<Real>(100);       // uniform on [0,1[
Reals ns = normalDistribution<Real>(100,2.,5.);  // normal distribution mu=2, sigma=5
RealMatrix mn =  normalDistribution<Real>(10,10);// normal distribution mu=0, sigma=1 (default)
\end{lstlisting}

\begin{focusbox}
Works also with complex vectors or complex matrices.
\end{focusbox} 

\begin{warningbox}
	Because of multiple random functions proposed, do not omit the dot in distribution parameters (Real) when they are passed explicitly.
\end{warningbox} 
\vspace{5mm}
\begin{ideabox}
There exist also functions addressing directly double or complex pointers to deal with C vectors, for instance:
\begin{lstlisting}
Reals us(100);
uniformDistribution(&us[0],-1.,1.,100); //fill us using uniform distribution on [-1,1[
\end{lstlisting}
\end{ideabox}
\vspace{5mm}
If you want to address the C98 random generators when C++11 is on, you have to use the "C" version of previous functions:
\begin{lstlisting}
Real uniformDistributionC(Real a=0., Real b=1.);    //C-style uniform distribution on [a,b[
Real normalDistributionC(Real mu=0., Real sigma=1.);//C-style normal distribution (mu,sigma)
\end{lstlisting}
In C-style, two normal distribution generators are available : the Marsiglia generator (default) ant the 
Box-Muller generator. You can specify it by using keyword (\verb|_MarsagliaGenerator| or \verb|_BoxMullerGenerator|) after normal distribution parameters:
\begin{lstlisting}
Reals us(100):
normalDistributionC(&us[0],0.,1.,_BoxMullerGenerator,100);
\end{lstlisting}
\vspace{5mm}
\begin{ideabox}
Random generators are automatically initialized when starting \xlifepp applications. Initialization value (the seed) is based on current time. It is possible to re-initialize random engines them with your own seed :
\begin{lstlisting}
int myseed=1000;
initRandomGenerators(myseed);
\end{lstlisting}
\end{ideabox}


\subsection{Gauss formulae}

You can use:

\begin{itemize}
\item the Gauss-Legendre formula
\begin{lstlisting}[deletekeywords={[3] weights}]
void gaussLegendreRule(Number n, Reals& points, Reals& weights);
\end{lstlisting}
\item the Gauss-Lobatto formula
\begin{lstlisting}[deletekeywords={[3] weights}]
void gaussLobattoRule(Number n, Reals& points, Reals& weights);
\end{lstlisting}
\item the Gauss-Jacobi formula
\begin{lstlisting}[deletekeywords={[3] weights}]
void gaussJacobiRule(Number n, Real a, Real b, Reals& points, Reals& weights);
\end{lstlisting}
\end{itemize}

The (n+1)/2 first positive points in ascending order only are returned.

\subsection{Exact solutions}

The field scattered by a solid sphere of radius \(R\) of an incoming field \(\Phi_w= e^{i\,k\,x}\) with Neumann or Dirichlet boundary conditions on the sphere are available with the following functions:

\begin{lstlisting}[deletekeywords={[3] x}]
Complex scatteredFieldSphereNeumann(const Point& x, Parameters& param);
Complex scatteredFieldSphereDirichlet(const Point& x, Parameters& param);
\end{lstlisting}


The field scattered by a solid disk of radius \(R\) of an incoming field \(\Phi_w= \exp^{i*k*x}\) with Neumann or Dirichlet boundary conditions on the disk are available with the following functions:

\begin{lstlisting}[deletekeywords={[3] x}]
Complex scatteredFieldDiskDirichlet(const Point& x, Parameters& param);
Complex scatteredFieldDiskNeumann(const Point& x, Parameters& param);
\end{lstlisting}

These four functions require an associated \class{Parameters} as second argument with
\begin{itemize}
\item a \class{Parameter} bearing the name "k" and holding the value of the wave number k.
\item a \class{Parameter} bearing the name "radius" and holding the value of the sphere radius.
\end{itemize}

The field scattered by a solid sphere of radius R of an incoming field \(\Phi_w=  e^{i\,k\,z}\) with tangential boundary condition \(E\times n = 0\) (electric field) on the sphere is available with the following function:

\begin{lstlisting}[deletekeywords={[3] x}]
Complexes scatteredFieldMaxwellExn(const Point& x, Parameters& param);
\end{lstlisting}

This function requires an associated \class{Parameters} as second argument with a \class{Parameter} bearing the name "k" and holding the value of the wave number k.


At last, the spherical harmonics \(Y^{m}_l\) up to order \(n\) on the unit sphere and their gradient are available with the following functions:

\begin{lstlisting}[deletekeywords={[3] x}]
void sphericalHarmonics(const Point& x, std::vector<Complexes>& Yml);
void sphericalHarmonicsSurfaceGrad(const Point& x, std::vector<std::vector<Complexes> >& gradylm);
\end{lstlisting}

\subsection{Special functions}

\subsubsection{Bessel and Hankel functions}

Bessel and Hankel functions are available at any order (and also for real orders) for real or complex argument (through a wrapper to the \amos library).

\begin{itemize}
\item Bessel functions of the first kind \(J_N(x)\)
\begin{lstlisting}[deletekeywords={[3] x}]
Real besselJ(Real x, Number N);
Complex besselJ(const Complex& z, Real N);
Real besselJ0(Real x); // shortcut for order 0 and real case
Complex besselJ0(const Complex& z); // shortcut for order 0 and complex case
Real besselJ1(Real x); // shortcut for order 1 and real case
Complex besselJ1(const Complex& z); // shortcut for order 1 and complex case
Reals besselJ0N(Real x, Number N); // shortcut for orders 0 to N and real case
\end{lstlisting}

\item Bessel functions of the second kind \(Y_N(x)\)
\begin{lstlisting}[deletekeywords={[3] x}]
Real besselY(Real x, Number N);
Complex besselY(const Complex& z, Real N);
Real besselY0(Real x); // shortcut for order 0 and real case
Complex besselY0(const Complex& z); // shortcut for order 0 and complex case
Real besselY1(Real x); // shortcut for order 1 and real case
Complex besselY1(const Complex& z); // shortcut for order 1 and complex case
Reals besselY0N(Real x, Number N); // shortcut for orders 0 to N and real case
\end{lstlisting}

\item Modified Bessel functions of the first kind \(I_N(x)\)
\begin{lstlisting}[deletekeywords={[3] x}]
Real besselI(Real x, Number N);
Complex besselI(const Complex& z, Real N);
Real besselI0(Real x); // shortcut for order 0 and real case
Complex besselI0(const Complex& z); // shortcut for order 0 and complex case
Real besselI1(Real x); // shortcut for order 1 and real case
Complex besselI1(const Complex& z); // shortcut for order 1 and complex case
Reals besselI0N(Real x, Number N);   // shortcut for orders 0 to N and real case
\end{lstlisting}

\item Modified Bessel functions of the second kind \(K_N(x)\)
\begin{lstlisting}[deletekeywords={[3] x}]
Real besselK(Real x, Number N);
Complex besselK(const Complex& z, Real N);
Real besselK0(Real x); // shortcut for order 0 and real case
Complex besselK0(const Complex& z); // shortcut for order 0 and complex case
Real besselK1(Real x); // shortcut for order 1 and real case
Complex besselK1(const Complex& z); // shortcut for order 1 and complex case
Reals besselK0N(Real x, Number N); // shortcut for orders 0 to N and real case
\end{lstlisting}

\item Hankel functions of the first kind \(H1_N(x)\)
\begin{lstlisting}[deletekeywords={[3] x}]
Complex hankelH1(Real x, Number N); // general routine for real case
Complex hankelH1(const Complex& z, Real N); // general routine for complex case and real order
Complex hankelH10(Real x); // shortcut for order 0 and real case
Complex hankelH10(const Complex& z); //shortcut for order 0 and complex case
Complex hankelH11(Real x); // shortcut for order 1 and real case
Complex hankelH11(const Complex& z); //shortcut for order 1 and complex case
Complexes hankelH10N(Real x, Number N); //shortcut for orders 0 to N and real case
\end{lstlisting}
\goodbreak
\item Hankel functions of the second kind \(H2_N(x)\)
\begin{lstlisting}[deletekeywords={[3] x}]
Complex hankelH2(Real x, Number N); // general routine for real case
Complex hankelH2(const Complex& z, Real N); // general routine for complex case and real order
Complex hankelH20(Real x); // shortcut for order 0 and real case
Complex hankelH20(const Complex& z); //shortcut for order 0 and complex case
Complex hankelH21(Real x); // shortcut for order 1 and real case
Complex hankelH21(const Complex& z); //shortcut for order 1 and complex case
Complexes hankelH20N(Real x, Number N); //shortcut for orders 0 to N and real case
\end{lstlisting}

\item Airy and Biry functions
\begin{lstlisting}[deletekeywords={[3] x}]
Complex airy(Real x, DiffOpType d=_id); // general function for real case
Complex airy(const Complex& z, DiffOpType d=_id); //general function for complex case
Complex biry(Real x, DiffOpType d=_id); // general function for real case
Complex biry(const Complex& z, DiffOpType d=_id); //general function for complex case
\end{lstlisting}
\end{itemize}

\subsubsection{Gamma  and exponential integral functions}

The gamma function and related functions are available with the following routines:
\begin{lstlisting}[deletekeywords={[3] x}]
Real gammaFunction(Int n);
Real gammaFunction(Real x);
Complex gammaFunction(const Complex& z);
Real logGamma(Real x);
Complex logGamma1(const Complex& z);
Complex logGamma(const Complex& z);
Real diGamma(Int n);
Real diGamma(Real x);
Complex diGamma(const Complex& z);
\end{lstlisting}

The exponential integrals functions and related stuff are provided by the following functions:
\begin{lstlisting}
Complex e1z(const Complex& z); // return E1(z)
Complex eInz(const Complex& z); // return E1(z) + gamma + log(z) = \sum_{n>0} (-z)^n / n n!
Complex expzE1z(const Complex& z); // return exp(z)*E1(z)
Complex zexpzE1z(const Complex& z); // return z*exp(z)*E1(z)
Complex ascendingSeriesOfE1(const Complex& z); // ascending series in E1 formula (used for 'small' z)
Complex continuedFractionOfE1(const Complex& z); // continued fraction in E1 formula (used for 'large' z)
\end{lstlisting}
\vspace{2mm}
The erf function
\[
\mbox{erf }z =\frac{2}{\sqrt{\pi}}\int_0^z e^{-t^2}dt\]
is also available from 
\begin{lstlisting}
    complex_t erf(complex_t z);
\end{lstlisting}

\subsubsection{Orthogonal polynomials}

Chebyshev, Gegenbauer, Jacobi and Legendre polynomials  up to order \(n\) are available using the following functions:
\begin{lstlisting}[deletekeywords={[3] x}]
void chebyshevPolynomials(Real x, Reals& val);
void gegenbauerPolynomials(Real lambda, Real x, Reals& val);
void jacobiPolynomials(real_t a, real_t b, real_t x, Reals& val);
void jacobiPolynomials01(Real a, Real b, Real x, Reals& val); // Jacobi polynomials on [0, 1]
void legendrePolynomials(Real x, Reals& val);
void legendrePolynomialsDerivative(Real x, Reals& val);
void legendreFunctions(Real x, std::vector<Reals>& Pml);
void legendreFunctionsDerivative(Real x, const std::vector<Reals>& Plm, std::vector<Reals>& dPlm);
\end{lstlisting}

\subsection{Computation of polynomial roots}

You can use:

\begin{itemize}
\item the quadratic method for degree 2 polynomials
\begin{lstlisting}
Complexes quadratic(Real a, Real b, Real c);
Complexes quadratic(Complex a, Complex b, Complex c);
\end{lstlisting}
\item the Cardan method for degree 3 polynomials
\begin{lstlisting}
Complexes cardan(Real a, Real b, Real c, Real d);
Complexes cardan(Complex a, Complex b, Complex c, Complex d);
\end{lstlisting}
\item the Ferrari method for degree 4 polynomials
\begin{lstlisting}
Complexes ferrari(Real a, Real b, Real c, Real d, Real e);
\end{lstlisting}
\end{itemize}

\subsection{Basic quadrature methods and FFT}

\xlifepp proposes some useful basic quadrature methods (rectangle, trapeze, Simpson, Laguerre, adaptive trapeze) to compute 1D integrals (\(h=\frac{b-a}{n}\)) : 
\[
\begin{array}{lll}
    \textrm{rectangle}&: &\displaystyle \int_a^b f(t)\,dt\sim h\sum_{i=0,n-1} f(a+ih) \\[5mm]
    \textrm{trapeze} & : &\displaystyle \int_a^b f(t)\,dt\sim \frac{h}{2} \left(f(a)+4\sum_{i=1,n/2} f(a+(2i-1)h) +2\sum_{i=1,n/2-1} f(a+2ih)+f(b)\right)\ \ (n \textrm{ even})\\[5mm]
    \textrm{Simpson} & : &\displaystyle \int_a^b f(t)\,dt\sim \frac{h}{3} \left(f(a)+2\sum_{i=1,n-1} f(a+ih) + f(b)\right)
\end{array}
\]

with \(f\) a real or complex function. They respectively integrate exactly P0 polynomials, P1 polynomials and P3 polynomials and approximate the integral with order 1, 2 and 3. \\They compute integrals of real/complex function either given by a C++ function of the form (T being a real value (float, double, \ldots) type or a complex type (complex<float>, complex<double>, \ldots))
\begin{lstlisting} 
    T f(real_t){\ldots}
    T f(real_t,Parameters&){\ldots}
\end{lstlisting}
or by a vector of values given by an explicit vector or an iterator. So, all the methods are templated by the type T of function values and possibly by a generic iterator (say Iterator).  
\begin{lstlisting} 
T rectangle(number_t n, real_t h, Iterator itb, T& intg);
T rectangle(const vector<T>& f, real_t h);
T rectangle(T(*f)(real_t), real_t a, real_t b, number_t n);
T rectangle(T(*f)(real_t, Parameters&), Parameters& pars, real_t a, real_t b, number_t n);
    
T trapz(number_t n, real_t h, Iterator itb, T& intg);
T trapz(const vector<T>& f, real_t h);
T trapz(T(*f)(real_t), real_t a, real_t b, number_t n);
T trapz(T(*f)(real_t, Parameters&), Parameters& pars, real_t a, real_t b, number_t n);
    
T simpson(number_t n, real_t h, Iterator itb, T& intg);
T simpson(const vector<T>& f, real_t h);
T simpson(T(*f)(real_t), real_t a, real_t b, number_t n);
T simpson(T(*f)(real_t, Parameters&), Parameters& pars, real_t a, real_t b, number_t n);
    
T laguerre(T(*f)(real_t), real_t t0, real_t a, number_t nq, vector<real_t>& points, vector<real_t>& weights);
T laguerre(T(*f)(real_t,Parameters&), Parameters& pars, real_t t0, real_t a, number_t nq, vector<real_t>& points, vector<real_t>& weights);
    
T adaptiveTrapz(T(*f)(real_t), real_t a, real_t b, real_t eps=1E-6)
T adaptiveTrapz(T(*f)(real_t,Parameters&), Parameters& pars, real_t a, real_t b, real_t eps=1E-6)
\end{lstlisting}
For instance, to compute 
\[I=\int_0^\pi \sin x\, dx\]
\begin{lstlisting} 
Real f(Real x){return sin(x);}
\ldots
Real ir=rectangle(f, 0., pi_, 20);
Real it=trapz(f, 0.,pi_, 20);
Real is=simpson(f, 0., pi_, 20);
Real ia=adaptiveTrapz(f, 0., pi_, 0.00001);
\end{lstlisting} 
\vspace{2mm}
\xlifepp provides also the standard 1D discrete fast Fourier transform  with \(2^n\) values:
\[g_k = \sum_{j=0,n-1} f_je^{-2i\pi\frac{jk}{n}}.\]
FFT and inverse FFT  are computed using some functions addressing real or complex vectors of size \(2^n\):

\begin{lstlisting}[deletekeywords={[3] sin}]
    Number ln=6, n=64;
    Vector<Complex> f(n),g(n),f2(n);
    for(Number i=0; i<n; i++) f[i]=std::sin(2*i*pi_/(n-1));
    fft(f,g); ifft(g,f2); // f2 should be close to f
    // or
    Vector<Complex> fhat=fft(f), fihat=ifft(f);
\end{lstlisting}

\begin{focusbox}
    If the vector you give is not of \(2^n\) length, \cmd{fft}/\cmd{ifft} computation will use only the first \(2^n\) values with \(2^n\) the greater value less than the vector size. Remember that even the input vector is a real vector the output is always a complex vector.
\end{focusbox}

Advanced users can also use some functions \cmd{fft}/\cmd{ifft} addressing iterators that can handle any collection of real or complex values :
\begin{lstlisting} 
    template<typename IterA, typename IterB> 
    void ffta(IterA ita, IterB itb, number_t log2n, real_t q)
    void fft(IterA ita, IterB itb, number_t log2n)
    void ifft(IterA ita, IterB itb, number_t log2n)
    
    template<typename T>
    vector<complex_t> fft(const vector<T>& f);
    vector<complex_t>& fft(const vector<T>& f, vector<complex_t>& g);
    vector<complex_t> ifft(const std::vector<T>& f);
    vector<complex_t>& ifft(const std::vector<T>& f, std::vector<complex_t>& g);
\end{lstlisting}
\subsection{ODE solvers}
\xlifepp provides some ODE solvers : Euler, Runge-Kutta 4 and Runge-Kutta 45 that is an adaptive step time solver based on the Dortmand-Prince method (see \url{http://en.wikipedia.org/wiki/Dormand-Prince_method}). All solvers are templated by the type of the state \(y\in V\) involved in the first order differential equation:
\[
\left\{\ \begin{array}{ll}
    y'(t)=f(t,y(t))&\ t\in]t_0,t_f[\\
    y(t_0)=y_0
\end{array}\right.\]
with \(f: ]t_0,t_f[\times V \rightarrow V\) a non-stiff function.\\

The user function \(f\) may be any C++ function of the form (T any scalar/vector/matrix type):
\begin{lstlisting} 
T& f(real_t t, const T& y, T& fty);  //fty the returned value
// for instance, with XLiFE++ common types
 Real f(Real t, const Real& y, Real& fty){\ldots; return fty;}                // real scalar ODE
 Complex& f(Real t, const Complex& y, Complex& fty){\ldots; return fty;}      // complex scalar ODE
 Reals& f(Real t, const Reals& y, Reals& fty){\ldots; return fty;}            // real vector ODE
 Complexes& f(Real t, const Complexes& y, Complexes& fty){\ldots; return fty;}// complex vector ODE
\end{lstlisting} 
ODE solvers are objects of template classes \class{EulerT}, \class{RK4T} and \class{Ode45T}, but they can also be invoked by simple functions
\begin{lstlisting}
template <typename T> 
Vector<T> euler(T& (*f)(Real, const Real& y, Real& fty),  Real a, Real b, Real dt, const T& y0);
Vector<T> rk4(T& (*f)(Real, const Real& y, Real& fty),  Real a, Real b, Real dt, const T& y0);
pair<Vector<Real>,Vector<T>> ode45(T& (*f)(Real, const Real& y, Real& fty),  Real a, Real b, Real dt, const T& y0, Real prec);
\end{lstlisting} 

In adaptive RK45 method, the time step may grow to become very large. To still get a meaningful number of times step, the time step is limited by the initial guess \verb?dt?. Even if the initial guess \verb?dt? is very large, the method should be convergent if the problem is not stiff.\\

The following example deals with the linear pendulum (ODE of order 2):
\begin{lstlisting}
Real omg=1., th0=pi_/4, thp0=0.;
Reals& f(Real t, const Reals& y, Reals& fyt)
{fyt.resize(2);
 fyt[0]=y[1]; fyt[1]=-omg*omg*y[0];  
 return fyt;}
Reals yex(Real t) //exact solution
{Reals yy(2,0.);
 yex[0] = thp0*sin(omg*t)/omg + th0*cos(omg*t);
 yex[1] = thp0*cos(omg*t) - th0*omg*sin(omg*t);
 return yex;}
 ...
 Real t0=0,tf=1,dt=0.01, t=t0, errsup=0;
 Reals y0(2,0.);y0[0]=th0; y0[1]=thp0;
 Vector<Reals> sol = euler(f,t0,tf,dt,y0);
 for(Number i=0;i<sol.size(); i++, t+=dt) 
     errsup=max(errsup,norm(sol[i]-yex(t)));
 theCout<<"euler : nb dt="<<sol.size()<<" error sup = "<<errsup<<eol;
    
 t=t0; errsup=0;
 sol = rk4(f,t0,tf,dt,y0);
 for(Number i=0;i<sol.size(); i++, t+=dt) 
     errsup=max(errsup,norm(sol[i]-yex(t)));
 theCout<<"rk4   : nb dt="<<sol.size()<<" error sup = "<<errsup<<eol;
    
 t=t0; errsup=0;
 pair<Reals,Vector<Reals>> solp = ode45(f,t0,tf,0.1,y0,1E-6);
 Number n=solp.first.size();
 for(Number i=0;i<n; i++) 
     errsup=std::max(errsup,norm(solp.second[i]-yex(solp.first[i])));
 theCout<<"ode45 : nb dt="<<n<<" error sup = "<<errsup<<eol;
\end{lstlisting}
Because, ode45 is an adaptive method, the time step is not constant. So the \cmd{ode45} function return times and state values as a pair of vectors. The previous code gives
\begin{verbatim}
    euler : nb dt=101 error sup = 0.00393671
    rk4   : nb dt=101 error sup = 6.54499e-11
    ode45 : nb dt=12  error sup = 5.25629e-08
\end{verbatim}
showing the advantage to use Ode45! 
\begin{ideabox}
    Contrary to Euler and RK4 method, Ode45 handles some internal parameters : \cmd{nbtry\_} the maximum number of tries (12) when adapting the time step, \cmd{minscale\_}(0.125) and {maxscale\_}(4) to limit the variation of the time step at each iteration. If you want to modify these internal parameters, you have to use an object of the \class{Ode45T} class; see the dev documentation.
\end{ideabox}