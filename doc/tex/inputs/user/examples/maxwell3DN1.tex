%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../../user_documentation.tex

Dealing with 3D Maxwell problem using Nedelec elements is very similar to the 2D case. We consider the academic Maxwell problem:

\[
\mathrm{curl}\,\mathrm{curl}\, \mathbf{E}-\omega^2\mu\,\varepsilon \mathbf{E} = \mathbf{f}  \quad \text{in }\Omega \\
\]
equipped with the natural boundary condition:
\[
\mathrm{curl}\, \mathbf{E}\times n= \mathbf{g}  \quad \text{on }\partial \Omega.
\]
It has the following weak form:
\[
\left|\begin{array}{l}
\text{find }\mathbf{E}\in V=\{\mathbf{v}\in H(curl,\Omega)\} \text{ such that }  \\
\displaystyle \int_\Omega\mathrm{curl}\,\mathbf{E}.\mathrm{curl}\,\mathbf{v}  
-\omega^2\mu\,\varepsilon \int_\Omega \mathbf{E} . \mathbf{v}  
= \int_\Omega \mathbf{f} . \mathbf{v}+ \int_{\partial \Omega} \mathbf{g} . \mathbf{v} \quad \forall \mathbf{v} \in V.
\end{array}\right.
\]

In the example we use as a solution 
\[
\mathbf{E}_{ex}(x,y,z)=\left(\begin{array}{c}
    -2\cos(a\,x)\sin(a\,y)\sin(a\,z)\\
   \sin(a\,x)\cos(a\,y)\sin(a\,z)\\
   \sin(a\,x)\sin(a\,y)\cos(a\,z)
\end{array}
\right)
\]

with \(f=\mathrm{curl}\,\mathrm{curl}\, \mathbf{E}_{ex}-\omega^2\mu\,\varepsilon \mathbf{E}_{ex}\) and \(g=\mathrm{curl}\, \mathbf{E}_{ex}\times n\).\\

Using the first family Nedelec's elements, the \xlifepp program looks like:\\

\inputXlifeppExample[deletekeywords={[3] x,y}]{maxwell3dN1.cpp}

\medskip

As Nedelec finite elements approximation are not conforming in H1, the solution is not continuous across elements (only tangent component is continuous). So to represent the solution, it is projected on H1 as follows:
\[
\left|\begin{array}{l}
\text{find }\mathbf{E}_1\in L^2(\Omega)\text{ such that}\\
\displaystyle \int_{\Omega}\mathbf{E}_1\,\mathbf{w}=\int_{\Omega}\mathbf{E}\,\mathbf{w} \quad \forall \mathbf{w}\in L^2(\Omega).
\end{array}\right.
\]

Using an H1 conforming approximation for \(\mathbf{E}_1\) leads to a continuous representation of the projection. We show on the next figure the module of the field \(E\) provided by this example:

\begin{figure}[H]
\centering
\includePict[width=16cm]{ex_maxwell3dN1_app.png}
\caption{Module of the solution of the Maxwell 3D problem using Nedelec first family elements}
\end{figure}
Alternatively, the \cmd{interpolate} function provides the interpolation of \(E\) on nodes of a P1 approximation.\\

On the next figure, the convergence curve shows a rate convergence of \(L^2\) error in \(h^{1.3}\) :

\begin{figure}[H]
\centering
\includePict[width=13cm]{Nedelec3d_convergence.png}
\caption{\(L^2\) errors versus the step \(h\) for first order Nedelec first family approximation}
\end{figure}
