%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../../user_documentation.tex

We want to solve the acoustic diffraction of a plane wave on the disk of radius 1, with the boundary \(\Gamma\):

\[
\begin{cases}
\Delta u + k^2u= 0 & \text{ in }\mathbb{R}^2/D \\
\partial_nu = g & \text{ on }\Gamma\ (n \text{ the outward normal})
\end{cases}
\]

where \(g=\partial_n\left( e^{ikx}\right)\).

\medskip

Let \(\Omega\) be a domain that strictly surrounding the disk \emph{D} and  \(\Sigma\) its boundary. We have to point out that in this case, we use normals going outside the domain of computation \(\Omega\) but then the normal on the obstacle (defined on \(\Gamma\)) is going inside the obstacle, that is opposite to usual case (see Figure \ref{figGeoConfFEIR}). Then, because of the normal inverted, the solution \(u\) may be represented by the integral representation formula (\(G\) is the Green function related to the 2D Helmholtz equation in free space):

\begin{equation}
\forall x\in \Sigma,\ \ u(x)=-\int_{\Gamma}\partial_{n_y}G(x,y)\,u(y)\,dy+ \int_{\Gamma}G(x,y)\,\partial_{n_y}u(y)\,dy \label{intgrep}
\end{equation}

say, because the boundary condition:

\[
\ u(x)=-\int_{\Gamma}\partial_{n_y}G(x,y)\,u(y)\,dy+ \int_{\Gamma}G(x,y)\,g(y)\,dy.
\]

\(n_y\) is the outward normal (to \(\Omega\) not the obstacle) on \(\Gamma\) and \(n_x\) will denote the outward normal on \(\Sigma\). Now matching values and normal derivative on \(\Sigma\), we introduce the boundary condition:

\[
(\partial_{n_x}+\lambda)u(x) = -(\partial_{n_x}+\lambda)\int_{\Gamma}\partial_{n_y}G(x,y)\,u(y)\,dy
+(\partial_{n_x}+\lambda)\int_{\Gamma}G(x,y)\,g(y)\,dy
\]

that reads, because \(G\) is not singular on \(\Gamma\times\Sigma\):

\[
\begin{array}{rl}
(\partial_{n_x}+\lambda)u(x) = &\displaystyle -\int_{\Gamma}\partial_{n_x}\partial_{n_y}G(x,y)\,u(y)\,dy -\lambda\int_{\Gamma}\partial_{n_y}G(x,y)\,u(y)\,dy\\[5mm]
+&\displaystyle \int_{\Gamma}\partial_{n_x}G(x,y)\,g(y)\,dy +\lambda\int_{\Gamma}G(x,y)\,g(y)\,dy = \mathcal{R}_\lambda(u)(x)
\end{array}
\]

Using this exact boundary condition, if \(Im(\lambda)\neq 0)\) the initial problem is equivalent to:

\[
\begin{cases}
\Delta u + k^2u= 0 & \text{ in }\Omega \\
\partial_nu = g & \text{ on }\Gamma\\
(\partial_{n_x}+\lambda)u = \mathcal{R}_\lambda(u) & \text{ on }\Sigma
\end{cases}
\]

Its variational formulation in \( V=H^1(\Omega)\) is:

\[
\left|
\begin{array}{c}
\text{find } u\in V \text{ such that } \forall v\in V \\
\displaystyle \int_\Omega \nabla u.\nabla \bar{v} - k^2\int_\Omega  u\, \bar{v} + \lambda\int_{\Sigma}u\,\bar{v}
+\int_\Sigma\int_{\Gamma}  u(y)\partial_{n_x}\partial_{n_y}G(x,y)\,\bar{v}(x)
+\lambda\int_\Sigma\int_{\Gamma}  u(y)\partial_{n_y}G(x,y)\,\bar{v}(x) \\
\hfill \displaystyle =\int_\Gamma g\,\bar{v}+\int_\Sigma\int_{\Gamma}  g(y)\partial_{n_x}G(x,y)\,\bar{v}(x)+\lambda\int_\Sigma\int_{\Gamma}  g(y)G(x,y)\,\bar{v}(x).
\end{array}
\right.
\]

Considering the geometrical configuration:

\begin{figure}[H]
\centering
\includePict[width=5cm, trim = 9.5cm 5.5cm 12.5cm 3cm, clip=true]{FE-IR.pdf}
\caption{Geometrical configuration for the FEM-Integral Representation problem. The normal on \(\Gamma\) is going inside the obstacle (to point outside \(\Omega\)).}
\label{figGeoConfFEIR}
\end{figure}

the variational formulation is implemented as follows:\\

\inputXlifeppExample[deletekeywords={[3] x, mesh}]{helmholtz2d_FE_IR.cpp}

\medskip

In the beginning, some geometric parameters used to design crown surrounded by a square, are given. Next the mesh is generated using gmsh mode and the geometrical domains are get from the mesh. The normal orientations are chosen in order to have outwards normals to the crown \verb|omega|.\\

Then a P2 Lagrange space over the elements of the crown \verb|omega| is constructed and all bilinear and linear forms involved in variational form are defined. Then the TermMatrix and TermVector are computed and the problem is solved using a direct method (Umfpack if it is installed, LU factorization else), that leads to the solution \verb|U| in the crown \verb|omega|.\\

Finally, using integral representation formula \ref{intgrep}, the solution is computed in the exterior domain \verb|omega_ext|. The vectors \verb|U| and \verb|Uext| are diffracted fields. To get total field, the incident field has to be added to the diffracted filed. This is the final job that it is done.\\

The real part of the total field computed is presented on the figure \ref{fig_FE-IR}.

\begin{figure}[H]
\centering
\includePict[width=7cm, trim=2cm 1.5cm 0.5cm 1.5cm, clip=true]{helmholtz2d_FE-IR.png}
\caption{2D Helmholtz diffraction problem using FE-IR method: real part of the total field}
\label{fig_FE-IR}
\end{figure}
