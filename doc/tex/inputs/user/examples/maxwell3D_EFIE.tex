%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../../user_documentation.tex

Solving diffraction of an electromagnetic plane wave on a obstacle using BEM is more intricate. Indeed, it is a vector problem and it involves Raviart-Thomas elements. We show how \xlifepp can deal easily with.

\medskip

Let \(\Gamma\) be the boundary of a bounded domain \(\Omega\) of \(\mathbb{R}^3\), we want to solve the Maxwell problem on the exterior domain \(\Omega_e\):

\[
\left\{
\begin{array}{ll}
\mathbf{\mathrm{curl}}\,\mathbf{E}-ik\mathbf{H}=0 & \text{in } \Omega_e \\
\mathbf{\mathrm{curl}}\,\mathbf{H}+ik\mathbf{E}=0 & \text{in } \Omega_e \\
\mathbf{E}\times \mathbf{n}=0 & \text{on }\Gamma\\
\displaystyle \lim\limits_{|x| \to \infty}\left( (\mathbf{H}-\mathbf{H}_{inc})\times \frac{x}{|x|}-(\mathbf{E}-\mathbf{E}_{inc})\right)=0 & \text{(Silver-Muller condition)}
\end{array}
\right.
\]

where \((\mathbf{E}_{inc},\mathbf{H}_{inc})\) is an incident field (a solution of Maxwell equation in free space), for instance a plane wave.\\

The EFIE (Electric Field Integral Equation) consists in finding the potential \(\mathbf{J}\) in the space

\[
H_{\mathrm{div}}(\Gamma)=\left\{\mathbf{V}\in L^2(\Gamma)^3, \mathbf{V}.\mathbf{n}=0, \mathrm{div}_{\Gamma}\,\mathbf{V}\in  L^2(\Gamma) \right\}
\]

such that, \(\forall \mathbf{V}\in H_{\mathrm{div}}(\Gamma)\)

\[
k\int_{\Gamma}\int_{\Gamma}\mathbf{J}(y)\,G(x,y).\mathbf{V}(x)-
\frac{1}{k}\int_{\Gamma}\int_{\Gamma}\mathrm{div}_{\Gamma}\,\mathbf{J}(y)\,G(x,y)\,\mathrm{div}_{\Gamma}\mathbf{V}(x)=
-\int_{\Gamma}\mathbf{E}_{inc}.\mathbf{V}
\]

where \(G\) is the Green function related to the Helmholtz 3D problem in free space.\\

This equation has a unique solution, except for a discrete set of wavenumbers corresponding to the resonance frequencies of the cavity \(\Omega\).\\

Using the Stratton-Chu representation formula, the scattered electric field may be reconstructed in \(\Omega_e\):

\[
\mathbf{E}(x)=\mathbf{E}_{inc}(x)+\frac{1}{k}\int_\Gamma \nabla_xG(x,y)\,\mathrm{div}_{\Gamma}\mathbf{J}(y)
+k \int_\Gamma G(x,y)\,\mathbf{J}(y).
\]

This problem is implemented in \xlifepp as follows:\\

\inputXlifeppExample[deletekeywords={[3] dx}]{maxwell3D_EFIE.cpp}

\medskip

In order to build an approximated space of \(H_{\text{div}}(\Gamma)\) we use the Raviart-Thomas element of order 1.

As the integrals involved in bilinear form are singular, we use here the Sauter-Schwab method to compute them when two triangles are adjacent, a quadrature method of order 5 if the two triangles are close (\(0<d(T1,T2)<2h\)) and a quadrature method of order 3 when the two triangles are far (\(d(T1,T2)>=2h\)).

Note that the unknowns in RT approximation are the normal fluxes on the edge of the triangulation. In order to plot the potential \(\mathbf{J}\), we have to move to a P1 representation, say \(\widetilde{\mathbf{J}}\). This can be done using a L2 projection from  \(H_{\text{div}}(\Gamma)\) to  \(L^2(\Gamma)\):

\[
\int_{\Gamma}\widetilde{\mathbf{J}}\,|\,\mathbf{V}=\int_{\Gamma}\mathbf{J}\,|\,\mathbf{V}\ \ \ \forall V\in L^2(\Gamma)
\]

This is what is done by the \xlifepp function \cmd{projection}.

We obtain the following potential:

\begin{figure}[H]
\centering
\includePict[width=6cm, trim=10cm 6cm 10cm 1cm,clip=true]{J_real.png}
\caption{3D Maxwell problem on the unit sphere, using EFIE, potential}
\end{figure}

On the following figures, we show the approximated electric field and the exact electric field. The component \(E_y\) is not shown because it is zero.

\begin{figure}[H]
\centering
\includePict[width=12cm, trim=1cm 3.5cm 1cm 2cm, clip=true]{Ex_real.png}
\caption{3D Maxwell problem on the unit sphere, using EFIE, x component}
\end{figure}

\begin{figure}[H]
\centering
\includePict[width=12cm, trim=1cm 3.5cm 1cm 2cm, clip=true]{Ez_real.png}
\caption{3D Maxwell problem on the unit sphere, using EFIE, y component}
\end{figure}
