%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../../user_documentation.tex

A well-known method to avoid some complex meshes consists in using the fictitious domain method where the boundary condition on an inside obstacle is taken into account by a Lagrange multiplier. To illustrate it, we consider the following 2D Laplace problem
in the domain \(\Omega=C\backslash B(O,R)\)with \(C=]0,1[\times]0,1[\) the unit square, \(B(O,R)\) the ball with center \(C\) and radius \(R\) such that \(B\subset C\):\\

\begin{minipage}{8cm}
\[
\begin{cases}
-\Delta u = f & \text{ in }\Omega \\
u=0 & \text{ on }\partial C=\Sigma\\
u=g & \text{ on }\partial B=\Gamma
\end{cases}
\]
\end{minipage}
\begin{minipage}{10cm}
\centering
\includePict[width=5cm]{fictitious_dom.png}
\end{minipage}

\medskip

Let us introduce the minimization problem:

\[
\underset{\tilde{v}\in\tilde{V}(g)}\min \frac{1}{2}\int_C |\nabla \tilde{V}|^2-\int_C\tilde{f}\tilde{v}
\]

where

\[
\tilde{V}(g)=\left\{\tilde{v}\in H^1(C),\ \tilde{v}_{|\Sigma}=0 \text{ and }\tilde{v}_{|\Gamma}=g\right\}\text{ and }\tilde{f}=\begin{cases}
f & \text{ in }\Omega \\
0  & \text{ in }B
\end{cases}.
\]

It has a unique solution \(\tilde{u}\in \tilde{V}(g)\) such that \( \tilde{u}=u\) on \(\Omega\) and there exists \(p\in H^{- \frac{1}{2}}(\Gamma)\) such that 

\[
\begin{cases}
\displaystyle \int_C \nabla\tilde{u}.\nabla \tilde{v}-\int_\Gamma p\,\tilde{v}=\int_C\tilde{f}\, \tilde{v}& \forall \tilde{v}\in  H^1_0(C) \\[4mm]
\displaystyle \int_\Gamma\tilde{u}\, q=\int_\Gamma g\, q &\forall q\in H^{-\frac{1}{2}}(\Gamma),
\end{cases}
\]

where the integrals on \(\Gamma\) are to be understood as the duality product on \(H^{-\frac{1}{2}}(\Gamma)\times H^{\frac{1}{2}}(\Gamma)\).\\

The key idea of fictitious domain method is to use two different meshes for \(C\) and \(\Gamma\). As a consequence, the computation of integrals on \(\Gamma\) involves shape functions that lie on two different meshes, inducing interpolation operations from one mesh to the other one. \xlifepp manages this interpolation operations in an hidden way for the user:\\

\inputXlifeppExample[deletekeywords={[3]circle}]{laplace2dP1-fictitious_domain.cpp}

\medskip

To extract the solution on the real domain \(\Omega\), a L2 projection is used:\\

\begin{lstlisting}[deletekeywords={[3]name, mesh}]
// create a mesh for Omega
Disk disk(_center=Point(0.5,0.5),_radius =R, _hsteps=0.05,
          _domain_name = "Obstacle", _side_names="Gamma");
Mesh mesh(rectangle-disk,_triangle);
Domain omega = mesh.domain("Omega");
Space V(omega,P1,"V"); Unknown u(V,"u"); TestFunction v(u,"v");

//compute L2 projection of ut on omega
TermMatrix M(intg(omega,u*v)), M2(intg(omega,ut*v));
TermVector PUt = directSolve(M,M2*Ut);
saveToFile("PUt",PUt,_vtu);
\end{lstlisting}

\medskip

Note that the computation of the matrix \verb|M2| also requires a computation of an integrals involving two meshes!

\begin{figure}[H]
\centering
\includePict[width=15cm]{fictitious_domain.png}
\caption{Solution of the Laplace 2D problem with the fictitious domain method}
\end{figure}
