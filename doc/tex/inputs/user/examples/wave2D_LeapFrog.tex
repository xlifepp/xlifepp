%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../../user_documentation.tex

So far, only the harmonic problems were considered. Time problem may also be solved using \xlifepp. But there is no specific tools dedicated to. Users have to implement the time loop related to the finite difference time scheme they choose.\\

As an example, consider the wave equation:

\[
\begin{cases}
\displaystyle \frac{\partial^2u}{\partial t^2}-c^2\Delta u = f & \text{ in } \Omega\times\left]0,T\right[\\
\displaystyle \frac{\partial u}{\partial n}=0  & \text{ in } \partial\Omega\times\left]0,T\right[\\
\displaystyle u(x,0)=\frac{\partial u}{\partial t}(x,0)=0 & \text{ in } \Omega
\end{cases}
\]

Using classical leap-frog scheme with time discretization \(t^n=n\Delta t\), leads to (\(u^n\) approximates \(u(x,t^n)\)):

\[
\begin{cases}
u^{n+1}=2u^n-u^{n-1}+(c\Delta t)^2\Delta u^n+(\Delta t)^2f^n & \text{ in } \Omega,\ \forall n>1\\
\dfrac{\partial u^n}{\partial n}=0  & \text{ in } \partial\Omega,\ \forall n>1\\
u^0=u^1=0 & \text{ in } \Omega
\end{cases}
\]

or, in variational form, \(\forall v\in V=H^1(\Omega)\):

\[
\left\{
\begin{array}{l}
\displaystyle \int_{\Omega}u^{n+1}v=2\int_{\Omega}u^{n}v-\int_{\Omega}u^{n-1}v-(c\Delta t)^2\int_{\Omega}\nabla u^n.\nabla v+(\Delta t)^2\int_{\Omega}f^n\,v\ \ \forall n>1 \\
u^0=u^1=0 \text{ in } \Omega
\end{array}\right.
\]

When approximating space \(V\) by a finite dimension space \(V_h\) with basis \((w_i)_{i=1,p}\), the variational formulation is reinterpreted in terms of matrices and vectors as follows:

\[
\left\{
\begin{array}{l}
\displaystyle U^{n+1}=2U^n-U^{n-1}-\mathbb{M}^{-1}\left((c\Delta t)^2\mathbb{K} U^n-(\Delta t)^2F^n\right)\ \ \forall n>1\\[.3cm]
U^0=U^1=0 \text{ in } \Omega
\end{array}\right.
\]

where

\[
\mathbb{M}_{ij}=\int_{\Omega}w_iw_j,\ \mathbb{K}_{ij}=\int_{\Omega}\nabla w_i.\nabla w_j,\ (F^n)_i=\int_{\Omega}f^n\,w_i.
\]

The \xlifepp implementation of this scheme on the unity square when using P1 Lagrange interpolation looks like (\(f(x,t)=h(t)g(x)\)):\\

\inputXlifeppExample{wave_2d_leap-frog.cpp}

\medskip

Note the very simple syntax taken into account the leap-frog scheme. The \autoref{fig.wave2d} represents the solution at different instants for a constant source localized in disk with center \((0.5,0.5)\), radius \(R=0.02\) and time excitation that is a Gaussian function. For chosen parameter \(dt=0.04\), the leap-frog scheme is stable (it satisfies the CFL condition) but dispersion effects obviously appear.

\begin{figure}[H]
\centering
\includePict[width=8cm]{wave2d.png}
\caption{Solution of the wave equation at different instants for a constant source localized in disk with center \((0.5,0.5)\)}
\label{fig.wave2d}
\end{figure}
