%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../../user_documentation.tex

Now, we consider a waveguide equipped wit a PML (Perfectly Matched Layer). Just recall that such a layer allows to absorb the waves without any reflection if the layer is infinite. When the PML is bounded, there is only some weak reflections due to a reflection on the back of the PML, more and more strong as the layer becomes thin.
\begin{figure}[H]
    \centering
    \includePict[width=10cm]{MaxwellHalfGuide.png}
    \caption{Maxwell half wave-guide}
\end{figure}
The strong formulation is the following ($\mu\,\varepsilon=1$):
\[
\left\{\begin{array}{ll}
\mathrm{curl}\,\mathrm{curl}\, \mathbf{E}-\omega^2 \mathbf{E} = \mathbf{f}  & \text{in }\Omega_g \\
\mathrm{curl}\, \mathbb{A}^{-1}\mathrm{curl}\,\mathbf{E}-\omega^2 \mathbb{A}\mathbf{E} = \mathbf{f}  & \text{in }\Omega_d \quad\text{(PML)}\\
\mathbf{E}\times n= g  & \text{on }\Sigma_g\\
\mathrm{curl}\, \mathbf{E}\times n= 0  & \text{on }\Sigma_d\\
\mathbf{E}\times n= 0  & \text{on }\Gamma_g\cup\Gamma_d\\
\left[\mathbf{E}\times n\right]_{\Sigma_0}= 0,\quad \left[\mathrm{curl}\, \mathbf{E}\times n\right]_{\Sigma_0}=0  & \text{(transmission condition)}\\
\end{array}\right.
\]
with ($\alpha\in\mathbb{C}$)
\[
\mathbb{A}=\left[ 
\begin{array} {ccc}
    \frac{1}{\alpha}& 0& 0\\
    0& \alpha & 0\\
    0 & 0 &\alpha
    \end{array}
    \right].
\]
Let us introduce 
\[ V_g^0=\{\mathbf{v}\in H(curl,\Omega_g),\,\mathbf{v}\times n=0\text{ on }\Gamma_g\}, \quad
   V_d^0=\{\mathbf{v}\in H(curl,\Omega_d),\,\mathbf{v}\times n=0\text{ on }\Gamma_d\}
\]
and
\[ W = \left\{(\mathbf{v_g},\mathbf{v_d})\in V^0_g\times V^0_d \text{ s.t. } \mathbf{v_g}\times n=\mathbf{v_d}\times n\text{ on }\Sigma_0  \right\}.
\]
The strong formulation has the following weak form:
\[
\left|\begin{array}{l}
\text{find }(\mathbf{E_g},\mathbf{E_d}) \in W\text{ such that } \mathbf{E_g}\times n=g \text{ on } \Sigma_g\text{ and }\forall (\mathbf{v_g},\mathbf{v_d})\in W\text{ with } \mathbf{v_g}\times n=0 \text{ on } \Sigma_g:\\
\displaystyle \int_{\Omega_g}\mathrm{curl}\,\mathbf{E_g}.\mathrm{curl}\,\mathbf{v_g} -\omega^2\int_{\Omega_g} \mathbf{E_g} . \mathbf{v_g}  
+ \int_{\Omega_d}\mathbb{A}^{-1}\mathrm{curl}\,\mathbf{E_d}.\mathrm{curl}\,\mathbf{v_d} -\omega^2\int_{\Omega_d} \mathbb{A}\mathbf{E_d}.\mathbf{v_d}  
= 0.
\end{array}\right.
\]
Note that we have to deal with many essential conditions, in particular the transmission condition  $\mathbf{E_g}\times n=\mathbf{E_d}\times n$ on $\Sigma_0$.\\

In the following example, a 'Neumann' mode of the wave-guide is used as exact solution. Using the first family Nedelec's elements (NE11 or NE12) the \xlifepp program looks like:\\

\inputXlifeppExample[deletekeywords={[3] x,y}]{maxwell3dPML.cpp}

\medskip
\begin{figure}[H]
\centering
\includePict[width=12cm]{Maxwell_PML.png}
\caption{Examples of solution of Maxwell equation in a half wave-guide using PML and Nedelec first family approximations}
\end{figure}
