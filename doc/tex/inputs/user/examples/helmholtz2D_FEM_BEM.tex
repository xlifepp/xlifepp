%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../../user_documentation.tex

We want to solve the acoustic propagation of a plane wave in a heterogeneous medium. In order to do that, we distinguish a domain \(\Omega\) that is heterogeneous, its boundary \(\Gamma\) and the exterior domain \(\Omega_{\text{ext}}\) that is homogeneous (see Figure \ref{figFEMBEMdomains}).

\begin{figure}[H]
\centering
\inputTikZ{example_FEMBEM}
\caption{Domains for computation: \(\Omega\) the heterogeneous medium, \(\Omega_{\text{ext}}\) the homogeneous exterior domain and \(\Gamma = \partial \Omega\).}
\label{figFEMBEMdomains}
\end{figure}

We solve:

\[
\begin{cases}
\displaystyle \Delta u(x) + k^2 \eta^2(x) u(x) = 0 & \text{ in }\mathbb{R}^2 \\
u(x) = -u_i(x) & \text{ on } \Gamma
\end{cases}
\]

with \(\eta(x) = 1\) in \(\Omega_{\text{ext}}\), and \(\eta(x)\) that can vary in \(\Omega\), and finally with \(u_i=e^{ikx}\).

We will use: \(\Omega = \left[-0.5, 0.5\right]^2\) and 

\begin{align}
\eta(x) = \begin{cases} 
\exp \left( - (x_1^2 - 0.25)*(x_2^2-0.25)/(2.*0.05) \right), \text{ when } \max(x_1, x_2) < 0.5.\\
1 \text{ otherwise.}
\end{cases} \notag
%\eta(x) = 1.+3.*\left(sin \left(3 \pi (x_1 + 0.5)\right)*\sin\left(5 \pi (x_2 + 0.5) \right) \right) \notag
\end{align}

\begin{figure}[H]
\centering
\includePict[width=10cm, trim=0cm 16cm 0cm 12cm, clip=true]{helmholtz2d_FEM_BEM_eta.png}
\caption{\(\eta(x)\) in \(\Omega \cup \Omega_{\text{ext}}\).}
\label{figFEMBEMeta}
\end{figure}

We decompose the problem in a coupled system of two equations: 

\begin{itemize}
\item in the FEM part, the solution solves the following equation:

\[
\Delta u + k^2 \eta^2 u = 0
\]

which gives the variational formulation: 

\begin{equation}
\left|\begin{array}{c}
\text{Find } u \in H^1(\Omega) \text{ such that}: \\
\int_{\Omega} \nabla u(x) \cdot \nabla \bar{v}(x) dx - k^2 \int_{\Omega} \eta^2(x) u(x) \bar{v}(x) dx - \int_{\Gamma} \lambda(x) \bar{v}(x) dx = 0, \quad \forall v \in H^1(\Omega) \\
\end{array}
\right.,
\end{equation}

with \(\lambda = \frac{\partial u}{\partial n}\) is the normal trace of \(u\) on \(\Gamma\).

\item in the BEM part, we solve:

\begin{equation}
\begin{cases}
\Delta u + k^2 u = 0 & \text{ in } \Omega_{\text{ext}} \\
u = -u_i & \text{ on } \Gamma.
\end{cases}
\end{equation}

The scattered field verifies:

\begin{align}
u_s(x) = - S_{\Gamma}\lambda (x) + K_{\Gamma}u(x), x \in \Omega_{\text{ext}},
\end{align}

with \(u\) the total field solution of the equation and \(\lambda\) the normal trace of \(u\) on \(\Gamma\), \(S_{\Gamma}\) and \(K_{\Gamma}\) are the single and double layer boundary potentials:

\begin{align}
S_{\Gamma} \phi(x) & = \int_{\Gamma} G(x,y) \phi(y) dy, \notag \\
K_{\Gamma} \phi(x) & = \int_{\Gamma} \frac{\partial G(x,y)}{\partial n_y} \phi(y) dy \notag,
\end{align}

and

\[
G(x,y) = \frac{e^{i k \|x-y\|}}{4 \pi \|x-y\|}
\]

Since \(u_s = u - u_i\), and taking the limit when \(x\) goes to \(\Gamma\), we obtain the integral equation:

\begin{align}
\left(\frac{I}{2} - K_{\Gamma}\right)u(x) + S_{\Gamma}\lambda(x) = u_i(x), x \in \Gamma.
\end{align}

The resulting variational formulation for the BEM part is then: 

\begin{equation}
\left|
\begin{array}{c}
\text{Find } u \in H^{1/2}(\Gamma) \text{ and } \lambda \in H^{-1/2}(\Gamma) \text{ such that}: \\
\displaystyle \frac{1}{2} \int_{\Gamma} u(x) \bar{\tau}(x) dx - \int_{\Gamma \times \Gamma} u(y) \frac{\partial G(x,y)}{\partial n_y} \bar{\tau}(x) dy dx + \int_{\Gamma \times \Gamma} \lambda(y) G(x,y) \bar{\tau}(x) dy dx \\
\hfill \displaystyle = \int_{\Gamma} u_i(x) \bar{\tau}(x) dx, \forall \tau \in H^{1/2}(\Gamma).
\end{array}
\right.
\end{equation}
\end{itemize}

By adding the variational formulations relatives to the two linked problems, we obtain the final variational formulation.

Finally, the solution is obtained directly from \(u\) for the FEM part and we need to compute the integral representation to obtain \(u_s\), the scattered field, and then to add the incident field to obtain the total field for this problem.

The last step is to merge the FEM solution in \(\Omega\) and the BEM solution in \(\Omega_{\text{ext}}\) to obtain a solution on the whole domain \(\Omega \cup \Omega_{\text{ext}}\) to simplify the visualization.

\begin{figure}[H]
\centering
\includePict[width=8cm, trim=2cm 3cm 3.5cm 3cm, clip=true]{helmholtz2d_FEM_BEM_sol.png}
\caption{Solution of the FEM-BEM problem.}
\label{figFEMBEMsol}
\end{figure}

The code of this example follows:

\medskip

\inputXlifeppExample[deletekeywords={[3] hsize, find}]{helmholtz2d_FEM_BEM.cpp}

\medskip

%Let \(\Omega\) be a domain that strictly surrounding the disk \(D\) and  \(\Sigma\) its boundary. We have to point out that in this case, we use normals going outside the domain of computation \(\Omega\) but then the normal on the obstacle (defined on \(\Gamma\)) is going inside the obstacle, that is opposite to usual case (see Figure \ref{figGeoConfFEIR}). Then, because of the normal inverted, the solution \(u\) may be represented by the integral representation formula (\(G\) is the Green function related to the 2D Helmholtz equation in free space):
%\begin{equation}
%\forall x\in \Sigma,\ \ u(x)=-\int_{\Gamma}\partial_{n_y}G(x,y)\,u(y)\,dy+ \int_{\Gamma}G(x,y)\,\partial_{n_y}u(y)\,dy \label{intgrep}
%\end{equation}
%say, because the boundary condition:
%\[ u(x)=-\int_{\Gamma}\partial_{n_y}G(x,y)\,u(y)\,dy+ \int_{\Gamma}G(x,y)\,g(y)\,dy. \]
%\(n_y\)  is the outward normal (to \(\Omega\) not the obstacle) on \(\Gamma\) and \(n_x\) will denote the outward normal on \(\Sigma\). Now matching values and normal derivative on \(\Sigma\), we introduce the boundary condition:
%\[ \partial_{n_x}+\lambda)u(x) = -(\partial_{n_x}+\lambda)\int_{\Gamma}\partial_{n_y}G(x,y)\,u(y)\,dy
%+(\partial_{n_x}+\lambda)\int_{\Gamma}G(x,y)\,g(y)\,dy \]
%that reads, because \(G\) is not singular on \(\Gamma\times\Sigma\):
%\[
%\begin{array}{rl}
%(\partial_{n_x}+\lambda)u(x) = &\displaystyle -\int_{\Gamma}\partial_{n_x}\partial_{n_y}G(x,y)\,u(y)\,dy -\lambda\int_{\Gamma}\partial_{n_y}G(x,y)\,u(y)\,dy\\[5mm]
%+&\displaystyle \int_{\Gamma}\partial_{n_x}G(x,y)\,g(y)\,dy +\lambda\int_{\Gamma}G(x,y)\,g(y)\,dy = \mathcal{R}_\lambda(u)(x)
%\end{array}
%\]
%Using this exact boundary condition, if \(Im(\lambda)\neq 0)\) the initial problem is equivalent to :
%\[
%\left\{\begin{array}{ll}
%\Delta u + k^2u= 0 & \textrm{ in }\Omega \\
%\partial_nu = g & \textrm{ on }\Gamma\\
%(\partial_{n_x}+\lambda)u = \mathcal{R}_\lambda(u) & \textrm{ on }\Sigma
%\end{array}\right.
%\]
%Its variational formulation in \( V=H^1(\Omega)\) is:
%\[
%\left\{\begin{array}{l}
%\textrm{find }u\in V \textrm{ such that } \forall v\in V \\
%\displaystyle \int_\Omega \nabla u.\nabla \bar{v} - k^2\int_\Omega  u\, \bar{v} + \lambda\int_{\Sigma}u\,\bar{v}
%+\int_\Sigma\int_{\Gamma}  u(y)\partial_{n_x}\partial_{n_y}G(x,y)\,\bar{v}(x)
%+\lambda\int_\Sigma\int_{\Gamma}  u(y)\partial_{n_y}G(x,y)\,\bar{v}(x) \\[5mm]
%\hfill \displaystyle =\int_\Gamma g\,\bar{v}+\int_\Sigma\int_{\Gamma}  g(y)\partial_{n_x}G(x,y)\,\bar{v}(x)+\lambda\int_\Sigma\int_{\Gamma}  g(y)G(x,y)\,\bar{v}(x).
%\end{array}\right.
%\]
%\vspace{.2cm}
%Considering the geometrical configuration:
%\begin{figure}[h]
%\centering \includePict[width=20cm, trim = 0cm 6cm 0cm 3cm, clip = true]{FE-IR.pdf}
%\caption{Geometrical configuration for the FEM-Integral Representation problem. The normal on \(\Gamma\) is going inside the obstacle (to point outside \(\Omega\)).}
%\label{figGeoConfFEIR}
%\end{figure}
%the variational formulation is implemented as follows
%\vspace{.2cm}
%
%\inputXlifeppExample{helmholtz2d_FE_IR.cpp}
%\vspace{.2cm}
%In the beginning, some geometric parameters used to design crown surrounded by a square, are given. Next the mesh is generated using gmsh mode and the geometrical domains are get from the mesh. The normal orientations are chosen in order to have outwards normals to the crown \verb|omega|.\\
%Then a P2 Lagrange space over the elements of the crown \verb|omega| is constructed and all bilinear and linear forms involved in variational form are defined. Then the TermMatrix and TermVector are computed and the problem is solved using a direct method (Umfpack if it is installed, LU factorization else), that leads to the solution \verb|U| in the crown \verb|omega|.
%\\
%Finally, using integral representation formula \ref{intgrep}, the solution is computed in the exterior domain \verb|omega_ext|. The vectors \verb|U| and \verb|Uext| are diffracted fields. To get total field, the incident field has to be added to the diffracted filed. This is the final job that it is done. \\
%
%The real part of the total field computed is presented on the figure \ref{fig_FE-IR}.
%
%\begin{figure}[H]
%\begin{center}
%\includePict[width=12cm, trim = 4cm 5cm 7cm 4cm, clip = true]{helmholtz2d_FE-IR.png}
%\end{center}
%\caption{2D Helmholtz diffraction problem using FE-IR method: real part of the total field}
%\label{fig_FE-IR}
%\end{figure}
