%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../../user_documentation.tex

The second example shows that \xlifepp can also handle non homogeneous Neumann conditions or Robin-Fourier
conditions. This problem also involve a Dirichlet condition. Given three real functions \(f_\Omega\), \(\alpha\)
and \(f_N\), the problem is:

\[
\begin{cases}
 -u'' + u = f_\Omega & \text{ in } \Omega=\left]a,b\right[ \\
 u(a) = 0 & \\
 u'(b) + \alpha(b)\, u(b) = f_N(b) & 
\end{cases}
\]

Its variational formulation is: 

\[
\left|
\begin{array}{c}
\text{Find }u\in V=\left\{v\in L^2(\Omega),\ v'\in L^2(\Omega),\ u(a)=0 \right\} \text{ such that}\\ 
\displaystyle \int_a^bu'(x)\,v'(x)\,dx + \int_a^bu(x)\,v(x)\,dx + \alpha(b)\, u(b) =
\int_a^b f(x)\,v(x)\,dx + f_N(b),\ \ \ \forall v\in V.
\end{array}
\right.
\]

\(\alpha(b) u(b)\) can be interpreted as \(\displaystyle\int_{\{b\}}\alpha(\gamma) u(\gamma)\,v(\gamma)d\gamma\)
and  \(f_N(b)\) can be interpreted as \(\displaystyle\int_{\{b\}} f_N(\gamma)\,v(\gamma)d\gamma\) where \(\gamma\)
is the variable over the side domain here reduced to a point. This allows to handle these conditions in a uniform syntactic way by defining linear forms as shown in the previous examples.\\

The following main program corresponds to solving this problem with \(\displaystyle \alpha(x)=\frac{7}{2} x^2 - 8x\) using the P10 Lagrange element over the interval \(\displaystyle \left] a, b \right[ = \left] 0,\frac{13}{4}\pi\right[\) using 4 elements ; the functions \(f_\Omega(x) = 2\, sin(x)\) and \(f_N(x) = cos(x) + \alpha(x)\, sin(x)\) are chosen so that the solution is \(sin(x)\):\\

\inputXlifeppExample{laplace1dP10Robin.cpp}

\medskip

The following figure shows a graphical representation of the solution using {\scshape Octave} (see \xautoref{sec_graphicexpl}):

\begin{figure}[H]
\centering
\includePict[scale=0.4]{U_OmegaFig1.png}
\includePict[scale=0.4]{U_OmegaFig2.png}
\caption{Solution of the Laplace 1D problem with Dirichlet and Robin conditions}
\end{figure}

The left figure shows the interpolation nodes which form a uniform distribution of points. This is the default behavior.

\medskip

Comparing the exact solution \(U_{ex}\) with the computed one \(U\), at the interpolation abscissae, leads to \(||U-U_{ex}||_\infty = 4.44278\times 10^{-10}\), value which is currently printed by the program.

By adding \lstinline{_FE_subtype=GaussLobatto} to the \class{Space} constructor, one can toggle to the Gauss-Lobatto abscissae which are more suitable with higher interpolation degrees. With this example, choosing these abscissae leads to a better approximation: we then get \(||U-U_{ex}||_\infty = 2.55367\times 10^{-11}\).
