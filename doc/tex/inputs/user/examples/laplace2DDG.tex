%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../../user_documentation.tex

Consider the Laplace problem with homogeneous Dirichlet condition:
\[
\begin{cases}
-\Delta u = f & \text{ in }\Omega \\
u=0 & \text{ on }\partial \Omega
\end{cases}
\]
Consider a discontinuous Galerkin space \(V_h\), e.g. discontinuous \(P^1\)-Lagrange space and let introduce the IP (Interior Penalty) formulation:
\[
\left|
\begin{array}{l}
\text{find } u_h\in V_h \text{ such that } \\
\displaystyle \int_\Omega \nabla_hu_h.\nabla_hv
-\int_\Gamma \big\{\nabla_hu_h.n\big\}\,\big[v\big]
-\int_\Gamma \big[u_h\big]\,\big\{\nabla_hv.n\big\}
+\int_\Gamma\mu \big[u_h\big]\,\big[v\big]
=\int_\Omega f\,v\quad \forall v\in V_h.
\end{array}
\right. 
\]
where \(\Omega=\bigcup T_\ell\),  \(\Gamma =\bigcup \partial T_\ell\) (the set of all sides of mesh elements) and, \(S_{k,\ell}\) denoting the side shared by the elements \(T_k\) and \(T_\ell\):
\[
\big\{v\big\}_{|S_{k\ell}}=\frac{1}{2}\left((v_{|T_k})_{|S_{k\ell}} + (v_{|T_\ell})_{|S_{k\ell}} \right )\quad \big[v\big]_{|S_{k\ell}}=\left((v_{|T_k})_{|S_{k\ell}} - (v_{|T_\ell})_{|S_{k\ell}} \right ).
\]
For a non shared side \(S_k\), we set
\[
\big\{v\big\}_{|S_{k}}=\big[v\big]_{|S_{k}}=(v_{|T_k})_{|S_{k}}.
\]
The operators \(\{.\}\) and \([.]\) are implemented in \xlifepp as \verb?mean(.)? and \verb?jump(.)? operators. The normal vector \(n\) is chosen as the outward normal from \(T_k\) on side \(S_{k\ell}\); in practice outward from the "first" parent element of the side. \(\mu\) is a penalty function usually chosen to be proportional to the inverse of the measure of the side \(S_{k\ell}\). Note that the Dirichlet boundary condition is a natural condition in this formulation.\\

To deal with a such formulation, the following objects have to be constructed from a geometrical domain, say \verb?omega? :
\begin{itemize}
    \item a FE space specifying \(L2\) conformity (discontinuous space) defined on \verb?omega?
    \item the geometrical domain \verb?Gamma? of sides of \verb?omega? using the function \verb?sides(omega)?
    \item the bilinear form related to IP formulation and involving  \verb?mean(.)? and \verb?jump(.)? operators
\end{itemize}

The \xlifepp implementation of this problem using discontinuous P1-Lagrange is the following:\\

\inputXlifeppExample[deletekeywords={[3] x,y}]{laplace2dDGP1.cpp}

\medskip

The \(L^2\) error is about \(0.00317\) for 5400 DoFs.  Using Paraview with the \textit{Warp by scalar} filter that produces elevation, the approximated field \(u\) looks like:
\begin{figure}[H]
\centering
\includePict[width=16cm]{ex_laplace2d_DG_P1.png}
\caption{Solution of the 2D Dirichlet problem on the unit square \([0,1]^2\) with  discontinuous P1-Lagrange elements, IP formulation (left) and NIPG formulation (right) }
\end{figure}
It can be noticed that the field is discontinuous and major errors is located on the corners. NIPG formulation is another penalty method corresponding to the bilinear form:
\[
\displaystyle \int_\Omega \nabla_hu_h.\nabla_hv
-\int_\Gamma \big\{\nabla_hu_h.n\big\}\,\big[v\big]
+\int_\Gamma \big[u_h\big]\,\big\{\nabla_hv.n\big\}
+\int_\Gamma\mu \big[u_h\big]\,\big[v\big]
\]

that is close to the IP formulation but non-symmetric. For NIPG, the \(L^2\) error is weaker (about \( 0.00141\)) and the solution is less polluted at the corners.