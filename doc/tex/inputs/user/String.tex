%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\section{\classtitle{String}, \classtitle{Strings}}

\class{String} is a nice class allowing to deal with char of arrays without managing the memory.
\class{String} is no more than an alias to the string class of the STL which is either standard
string (utf8, by default) or wide string (utf16); this choice is made in the \emph{config.hpp}
header file by setting the macro \emph{WIDE\_STRING}. When this macro changes, all the library
has to be rebuilt!\\

As a string or wstring class of the STL, \class{String} proposes all the functionalities of std::string.
Mainly, you can create, concatenate string, access to char and find string in string:\\

\begin{lstlisting}
String s1("a string");        // create a String
String s2="another string";  // create a String using =
String s12=s1+" and "+s2;     // concatenate String, s3="a"+"b" does not work!
s1+=" and "+s2;               // concatenate String, now s1 is the same as s12 
int l=s1.size();              // number of char of s1, s1.length() gives the same
char a=s1[3];                 // char a='t' (the fourth character)
s1[3]=p;                      // now s1="a spring and another string";
int p=s1.find("string",0);    // find first position of "string" from beginning (if p=-1 not found)
s1.replace(p,5,"spring");     // replace "string" by "spring"
s2=s1.substr(p,5);            // extract string of length 5 from position p
s1.compare(s2);               // alphanumeric comparison, return 0 if equal, 
                              // a negative (positive) value if s1<s2 (s1>s2)
char * c=s1.c_str();          // return pointer to the char array of the string
\end{lstlisting}

There is a lot of variations of these string functions and other functions; see the STL documentation.\\

Some additional functions that may be useful have been introduced:\\

\begin{lstlisting}
template<typename T_>
String tostring(const T_& t);  // 'anything' to String
template<typename T_>
T_ stringto(const String& s);  // String to 'anything'
// returns String converted to lowercase
String lowercase(const String&);
// returns String converted to uppercase
String uppercase(const String&);
// returns String with initial converted to uppercase
String capitalize(const String&);
// trims leading white space from String
String trimLeading(const String&);
// trims trailing white space from String
String trimTrailing(const String&);
// trims leading and trailing white space from String
String trimSpace(const String&);
// delete all white space from String
String delSpace(const String& s);
// search capabilities
int findString(const String, const std::vector<String>&);
\end{lstlisting}

Be cautious with template conversion functions. The template \emph{T\_} type has to be clarified
when invoking \emph{stringto}.\\

\begin{lstlisting}
// examples of conversion stringto
String s="1 2 3";
int i=stringto<int>(s);         // i=1
Real r=stringto<Real>(s);       // r=1.
Complex c=stringto<Complex>(s); // c=(1.,0)
void * p=stringto<void*>(s);    // p=0x1
String ss=stringto<String>(s);  // ss="1"
s="(0,1)";
c=stringto<Complex>(s);         // c=(0.,1.)
\end{lstlisting}

Besides, lists of strings are available using Strings:\\

\begin{lstlisting}
// list of strings
Strings ss("x=0","x=1","y=0","z=0"); //initialize list (up to 5 elements)
Strings ls(10);   //10 empty strings
ls(1)="x=0";      //access to first string of list
String s=ss(3);
cout<<ss;         //output list
\end{lstlisting}

\class{Strings} inherits from \class{std::vector<String>}.
