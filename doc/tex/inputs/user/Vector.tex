%%%%%%%%%%%%%%%%%%%
% XLiFE++ is an extended library of finite elements written in C++
%     Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%

% !TeX root =  ../../user_documentation.tex
 
\section{\classtitle{Vector}}

The purpose of the \class{Vector} class is mainly to deal with complex or real vector. In
particular, this class is used in the definition of the user functions (see the section Function).
 It is a template class mainly used as a real or complex vector:
\vspace{.2cm}
\begin{lstlisting}[frame=single]{}
Vector<Real> u;          // u=[0.]
Vector<Real> v(3);       // v=[0. 0. 0.]
Vector<Real> w(3,2.5);   // w=[2.5 2.5 2.5]
Vector<Complex> cu;      // cu=[(0.,0.)]
Vector<Complex> cv(3);   // cv=[(0.,0.) [(0.,0.) (0.,0.)]
Complex i(0,1);          // the complex i
Vector<Complex> cw(3,i); // cv=[(0.,1.) [(0.,1.) (0.,1.)]
\end{lstlisting}
\vspace{.2cm}
It is also possible to deal with vector of vectors, for instance:
\vspace{.2cm}
\begin{lstlisting}
Vector<Real> ones(3,1); // ones=[1. 1. 1.]
Vector<Vector<Real> > U(4,ones);
    // U=[[1. 1. 1.] [1. 1. 1.] [1. 1. 1.] [1. 1. 1.]]
\end{lstlisting}
\vspace{.2cm}
To access to a vector component (both read and write access) use the operator () with index
from 1 to the vector length:
\vspace{.2cm}
\begin{lstlisting}
Vector<Real> v(3);       // v=[0. 0. 0.]
v(1)=1.;v(2)=2.;v(3)=3.; // v=[1. 2. 3.]
Vector<Complex> cv(3);   // cv=[(0.,0.) [(0.,0.) (0.,0.)]
cv(2)=Complex(1,1);      // cv=[(0.,0.) [(1.,1.) (0.,0.)]
\end{lstlisting}
\vspace{.2cm}
Note that access using operator [] with index from 0 to the vector length -1, is also possible.
Advanced users can use member functions \cmd{begin} and \cmd{end} returning respectively
iterators (or const iterators) to the beginning and the end of the vector.\\

It is also possible to extract some vector components in a new vector or to set some vector components by specifying a set of indices 
either given by lower and upper indices or given by a vector of indices:
\vspace{.2cm}
\begin{lstlisting}[deletekeywords={[3] z}]
Vector<Real> v(5);           // v =[0. 0. 0. 0. 0.]
for(Number i=1;i<=5;i++) 
   v(i)=i*i;                 // v =[1. 5. 9. 16. 25.]
Vector<Real> w=v(3,5);       // w =[9. 16. 25.] 
Vector<Number> is(3);    
is(1)=1;is(2)=3;is(3)=5;     // is=[1 3 5]     
w=v(is);                     // w =[1. 9. 25.]
v.set(1,3,w);                // v =[1. 9. 25. 16. 25.]      
Vector<Real> z(3,0.);        // w =[0. 0. 0.]
v.set(is,z);                 // v =[0. 9. 0. 16. 0.]                
\end{lstlisting}
\vspace{.2cm} 

Standard algebraic operations (+=, -=, *=, /=, +, -, *, /) are supported by the Vector. Some shortcuts
are also possible, for instance a vector plus a scalar, a scalar plus a vector, \ldots Here are
a few examples:
\vspace{.2cm}
\begin{lstlisting}
Vector<Real> u(3,1);     // u=[1. 1. 1.]
Vector<Real> v(3);       // v=[0. 0. 0.]
v=2.;                    // v=[2. 2. 2.]
Vector<Real> w=u+v;      // v=[3. 3. 3.]
w=2.*v;                  // v=[4. 4. 4.]
w-=2.;                   // v=[2. 2. 2.]
Complex i(0,1);          // complex number
Vector<Complex> cv(3,i); // cv=[(0.,1.) [(0.,1.) (0.,1.)]
cv=cv*i;                 // cv=[(-1.,0.) [(-1.,0.) (-1.,0.)]
cv/=2.;                  // cv=[(-0.5,0.) [(0.5.,0.) (0.5.,0.)]
\end{lstlisting}
\vspace{.2cm}
For algebraic operations involving two vectors, the compatibility of the size of vectors is
checked. All the algebraic operations involving a real vector (resp. a complex vector) and
a real scalar (resp. a complex scalar) are supported. Be cautious, as an integer value is not
always certainly  cast to a real value, some operations may be failed during the compiling
process. For instance, the addition between a real vector and an integer does not work, cast
explicitly to a real! 
\vspace{.2cm}
\begin{lstlisting}
Vector<Real> u(3,1); // u=[1. 1. 1.]
Vector<Real> v(3);   // v=[0. 0. 0.]
v=u+2;               // DOES NOT WORK
v=u+2.;              // v=[3. 3. 3.]
\end{lstlisting}
\vspace{.2cm}
Automatic cast from real vector to complex vector is supported. For instance, the following
instructions are legal:
\vspace{.2cm}
\begin{lstlisting}
Complex i(0,1);        // complex number i
Vector<Real> u(3,1);   // u=[1. 1. 1.]
Vector<Complex> cv(3); // cv=[(0.,0.) [(0.,0.) (0.,0.)]
cv=u+2.*i;             // cv=[(1.,2.) [(1.,2.) (1.,2.)]
cv*=3.;                // cv=[(3.,6.) [(3.,6.) (3.,6.)]
\end{lstlisting}
\vspace{.2cm}
\textbf{Be cautious, automatic cast is not supported for vector of vectors.}\\

The class also provides some various functionalities:
\vspace{.2cm}
\begin{lstlisting}
Vector<Real> u(3,1);     // u=[1. 1. 1.]
u.norminfty();           // the sup norm of u
u.norm2squared();        // squared quadratic norm of u
u.norm2();               // quadratic norm of u
cout<<u;                 // output the vector u: [1 1 1]
Vector<Complex> cv(3,i); // cv=[(0.,1.) [(0.,1.) (0.,1.)]
conj(u);                 // conjugate of cv
cv=cmplx(u);             // transform a real vector in a complex one
u=real(cv);              // take the real parts
u=imag(cv);              // take the imaginary parts
\end{lstlisting}
\vspace{.2cm}
Contrary to the \class{Point} class, the \class{Vector} class offers no comparison function.
Note also that there is no link between these two classes except that a \class{Point} may be automatically constructed from a \class{Vector}:
\begin{lstlisting}
Vector<Real> u(3,1);     
Point P=u;
\end{lstlisting}
To avoid explicit templates in user program, the following aliases are provided:
\begin{itemize}[itemsep=-5pt]
\item \class{Reals} or \class{RealVector} stands for \class{Vector<Real>},
\item \class{Complexes} or \class{ComplexVector} stands for \class{Vector<Complex>},
\item \class{RealVectors} stands for \class{Vector<Vector<Real> >},
\item \class{ComplexVectors} stands for \class{Vector<Vector<Complex> >}.
\end{itemize}

