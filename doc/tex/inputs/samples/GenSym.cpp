/*!
  Assumptions (not checked) :
    S real symmetric
    M real symmetric positive definite
*/
GenSym::GenSym(TermMatrix& S, TermMatrix& M)
: ARGenFrame(S), matS_p(&S.matrixData()->getLargeMatrix<Real>()),
                 matM_p(&M.matrixData()->getLargeMatrix<Real>()) {

  fact_p = newSkyline(matM_p);
  ldltFactorize(*fact_p);
}
//!    Matrix-vector products y <- inv(M)*S * x and x <- S * x
void GenSym::MultOPx(Real *x, Real *y) {
  array2Vector(x, lx);
  std::vector<Real> Sx(GetN());
  multMatrixVector(*matS_p, lx, Sx);
  vector2Array(Sx, x);
// Solve linear system. Matlab equivalent: ly = matM_p \ Sx;
  (fact_p->ldltSolve)(Sx, ly); // store the solution into ly
  vector2Array(ly, y);
}
//!   Matrix-vector product y <- M * x
void GenSym::MultBx(Real *x, Real *y) {
  array2Vector(x, lx);
  multMatrixVector(*matM_p, lx, ly);
  vector2Array(ly, y);
}
