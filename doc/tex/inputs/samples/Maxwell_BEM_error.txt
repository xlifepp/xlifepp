npa=5
indirect EFIE L2 error = 1.18462e-02 relative error = 4.74740e-02
  direct EFIE L2 error = 1.54827e-02 relative error = 6.20473e-02
indirect MFIE L2 error = 2.40196e-01 relative error = 9.62592e-01
  direct MFIE L2 error = 6.61435e-01 relative error = 2.65071e+00
         CFIE L2 error = 1.18782e-02 relative error = 4.76022e-02
npa=10
indirect EFIE L2 error = 2.58861e-03 relative error = 1.03739e-02
  direct EFIE L2 error = 3.37564e-03 relative error = 1.35280e-02
indirect MFIE L2 error = 2.58237e-02 relative error = 1.03489e-01
  direct MFIE L2 error = 1.18270e-01 relative error = 4.73970e-01
         CFIE L2 error = 2.58782e-03 relative error = 1.03707e-02
npa=15
indirect EFIE L2 error = 9.91518e-04 relative error = 3.97353e-03
  direct EFIE L2 error = 1.31585e-03 relative error = 5.27329e-03
indirect MFIE L2 error = 1.87530e-02 relative error = 7.51528e-02
  direct MFIE L2 error = 2.34776e-01 relative error = 9.40869e-01
         CFIE L2 error = 9.91443e-04 relative error = 3.97323e-03
npa=20
indirect EFIE L2 error = 5.79046e-04 relative error = 2.32054e-03
  direct EFIE L2 error = 7.67777e-04 relative error = 3.07688e-03
indirect MFIE L2 error = 9.95389e-03 relative error = 3.98904e-02
  direct MFIE L2 error = 6.53318e-02 relative error = 2.61819e-01
         CFIE L2 error = 5.79210e-04 relative error = 2.32120e-03
npa=25
indirect EFIE L2 error = 3.56009e-04 relative error = 1.42671e-03
  direct EFIE L2 error = 4.72993e-04 relative error = 1.89553e-03
indirect MFIE L2 error = 1.10648e-02 relative error = 4.43423e-02
  direct MFIE L2 error = 8.68960e-02 relative error = 3.48238e-01
         CFIE L2 error = 3.56038e-04 relative error = 1.42683e-03
