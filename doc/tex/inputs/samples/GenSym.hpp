class GenSym: public ARGenFrame<Real> {
  public:
    //! constructor
    GenSym(TermMatrix& S, TermMatrix& M);

    //! destructor
    ~GenSym(){ delete fact_p; }

    //! matrix-vector products required : y <- inv(M)*S * x and x <- S * x
    void MultOPx(Real *x, Real *y);

    //! matrix-vector product y <- M * x
    void MultBx(Real *x, Real *y);

  private:
    //! pointers to internal data objects
    const LargeMatrix<Real> *matS_p, *matM_p;
    //! pointer to temporary factorized matrix M
    LargeMatrix<Real>* fact_p;
}; // end of Class GenSym =========================================
