/*!
  Assumptions (not checked) :
    S real
    M real symmetric positive definite
*/
StdNonSym::StdNonSym(TermMatrix& S, TermMatrix& M)
: ARStdFrame(S), matS_p(&S.matrixData()->getLargeMatrix<Real>()),
                 matM_p(&M.matrixData()->getLargeMatrix<Real>()) {

  fact_p = newSkyline(matM_p);
  ldltFactorize(*fact_p);
}
//!    Matrix-vector product y <- inv(M)*S * x
void StdNonSym::MultOPx(Real *x, Real *y) {
  array2Vector(x, lx);
  std::vector<Real> Sx(GetN());
  multMatrixVector(*matS_p, lx, Sx);
// Solve linear system. Matlab equivalent: ly = matM_p \ Sx;
  (fact_p->ldltSolve)(Sx, ly); // store the solution into ly
  vector2Array(ly, y);
}
