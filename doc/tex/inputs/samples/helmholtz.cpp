#include "xlife++.h"
using namespace xlifepp;

Real cosxcosy(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return cos(pi_ * x) * cos(pi_ * y);
}

int main(int argc, char** argv)
{
  init(_lang=fr); // mandatory initialization of xlifepp
  SquareGeo sq(_origin=Point(0.,0.), _length=1, _nnodes=11);
  Mesh mesh2d(sq, triangle, 1, structured);
  Domain omega = mesh2d.domain("Omega");
  Space Vk(_domain=omega, _interpolation=P1, _name="Vk", _optimizeNumbering);
  Unknown u(Vk, "u");
  TestFunction v(u, "v");
  BilinearForm auv = intg(omega, grad(u) | grad(v)) + intg(omega, u * v);
  LinearForm fv=intg(omega, cosxcosy * v);
  TermMatrix A(auv, "a(u,v)");
  TermVector B(fv, "f(v)");
  TermVector X0(u, omega, 1., "X0");
  TermVector U = cgSolve(A, B, X0, _name="U");
  saveToFile("U", U, vtu);
  return 0;
}
