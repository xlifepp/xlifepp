class StdComp: public ARStdFrame<Complex> {
  public:
    //! constructor
    StdComp(TermMatrix& S, TermMatrix& M);

    //! destructor
    ~StdComp(){ delete fact_p; }

    //! matrix-vector product required : y <- inv(M)*S * x
    void MultOPx(Complex *x, Complex *y);

  private:
    //! pointers to internal data objects
    const LargeMatrix<Complex> *matS_p;
    const LargeMatrix<Real> *matM_p;
    //! pointer to temporary factorized matrix M
    LargeMatrix<Real>* fact_p;
}; // end of Class StdComp =========================================
