class StdNonSym: public ARStdFrame<Real> {
  public:
    //! constructor
    StdNonSym(TermMatrix& S, TermMatrix& M);

    //! destructor
    ~StdNonSym(){ delete fact_p; }

    //! matrix-vector product required : y <- inv(M)*S * x
    void MultOPx(Real *x, Real *y);

  private:
    //! pointers to internal data objects
    const LargeMatrix<Real> *matS_p, *matM_p;
    //! pointer to temporary factorized matrix M
    LargeMatrix<Real>* fact_p;
}; // end of Class StdNonSym =========================================
