# XLiFE++ README

## Features

Please refer to the [documentation](https://xlifepp.pages.math.cnrs.fr)

## Installation

Please read the [INSTALL.md](INSTALL.md) file to known how to install and compile XLiFE++

### Requirements

- [CMake](https://cmake.org)
- C++ and FORTRAN compilers
- Optional but strongly recommended: [gmsh](http://gmsh.info/)
- Optional: [Arpack](http://www.caam.rice.edu/software/ARPACK/), [UMFPACK](http://faculty.cse.tamu.edu/davis/suitesparse.html), OpenMP, [Metis](http://glaros.dtc.umn.edu/gkhome/metis/metis/changes/)

## Use

### Using XLIFE++ with scripts

This application is provided in several forms, according to the system used. It allows to
prepare the writing of an executable file through command lines as well as through an IDE.

Under Mac OS X and Unix/Linux, use `./bin/xlifepp.sh`
Under Windows, use `./bin/xlifepp.bat`

In this case, the settings are related to the template file "main.cpp" or a file in `./examples`
which is copied in the working directory. The copied filename should not be changed. Otherwise,
[CMake](https://cmake.org) should be run again manually to take this change into account

    cmake CMakeLists.txt

### Using XLiFE++ with GUI applications

GUI QT-based front-ends to these scripts are available at [XLiFE++ website](https://xlifepp.pages.math.cnrs.fr)

## The XLiFE++ team

The XLiFE++ library has been mainly developed by [É. Lunéville](https://www.ensta-paris.fr/fr/eric-luneville) and [N. Kielbasiewicz](https://www.ensta-paris.fr/fr/nicolas-kielbasiewicz) of [POEMS](https://poems.ensta-paris.fr) lab (UMR 7231, CNRS-ENSTA Paris-Inria). Some parts are inherited from Melina++ library developed by [D. Martin](http://homepage.mac.com/danielmartin/) ([IRMAR](http://anum-maths.univ-rennes1.fr/index.html) lab, Rennes University, now retired) and [É. Lunéville](https://www.ensta-paris.fr/fr/eric-luneville).

### Contributors

- [Colin Chambeyron](https://www.ensta-paris.fr/fr/colin-chambeyron) ([POEMS](https://poems.ensta-paris.fr) lab): iterative solvers, unitary tests, PhD students' support, documentation
- [Yvon Lafranche](https://perso.univ-rennes1.fr/yvon.lafranche/) ([IRMAR](http://anum-maths.univ-rennes1.fr/index.html) lab): mesh tools using subdivision algorithms, wrapper to [Arpack](http://www.caam.rice.edu/software/ARPACK/)
- [Éric Darrigrand](https://perso.univ-rennes1.fr/eric.darrigrand-lacarrieu/) ([IRMAR](http://anum-maths.univ-rennes1.fr/index.html) lab): fast multipole methods
- [Pierre Navaro](https://irmar.univ-rennes.fr/interlocuteurs/pierre-navaro) ([IRMAR](http://anum-maths.univ-rennes1.fr/index.html) lab): continuous integration
- Manh Ha Nguyen ([POEMS](https://poems.ensta-paris.fr) lab): eigen solvers and OpenMP implementation
- Steven Roman ([POEMS](https://poems.ensta-paris.fr) lab): boundary element methods
- Nicolas Salles ([POEMS](https://poems.ensta-paris.fr) lab): boundary element methods
- Laure Pesudo ([POEMS](https://poems.ensta-paris.fr) lab): boundary element methods and HF coupling
- Etienne Peillon ([POEMS](https://poems.ensta-paris.fr) lab): evolution of (bi)linear forms
