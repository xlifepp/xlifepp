#include "xlife++.h"
using namespace xlifepp;

Complex data_g(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), k=pa("k");
  Vector<Complex> g(2, 0.);
  g(1) = i_*k*exp(i_*k*x);
  return dot(g, P/norm2(P));  //dr(e^{ikx}
}

Complex u_inc(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), k=pa("k");
  return exp(i_*k*x);
}

int main(int argc, char** argv)
{
  init(argc, argv, _lang=en); // mandatory initialization of xlifepp
  //parameters
  Number nh = 10;               // number of elements on Gamma
  Real h=2*pi_/nh;              // size of mesh
  Real re=1.+2*h;               // exterior radius
  Number ne=Number(2*pi_*re/h); // number of elements on Sigma
  Real l = 4*re;                // length of exterior square
  Number nr=Number(4*l/h);      // number of elements on exterior square
  Real k= 4, k2=k*k;            // wavenumber
  Parameters pars;
  pars << Parameter(k, "k") << Parameter(k2, "k2");
  Kernel H=Helmholtz2dKernel(pars);
  Function g(data_g, pars);
  Function ui(u_inc, pars);
  //Mesh and domains definition
  Disk d1(_center=Point(0., 0.), _radius=1, _nnodes=nh, _side_names=Strings(4, "Gamma"));
  Disk d2(_center=Point(0., 0.), _radius=re, _nnodes=ne, _domain_name="Omega", _side_names=Strings(4, "Sigma"));
  SquareGeo sq(_center=Point(0., 0.), _length=l, _nnodes=nr, _domain_name="Omega_ext");
  Mesh mesh(sq+(d2-d1), _shape=triangle, _generator=gmsh);
  Domain omega=mesh.domain("Omega");
  Domain sigma=mesh.domain("Sigma");
  Domain gamma=mesh.domain("Gamma");
  Domain omega_ext=mesh.domain("Omega_ext");  //for integral representation
  sigma.setNormalOrientation(_outwardsDomain, omega); //outwards normals
  gamma.setNormalOrientation(_outwardsDomain, omega);

  //create P2 Lagrange interpolation
  Space V(_domain=omega, _interpolation=P2, _name="V");
  Unknown u(V, _name="u");  TestFunction v(u, _name="v");
  // create bilinear form and linear form
  Complex lambda=-i_*k;
  BilinearForm auv = intg(omega, grad(u)|grad(v))-k2*intg(omega, u*v)+lambda*intg(sigma, u*v)
                     + intg(sigma, gamma, u*(grad_y(grad_x(H)|_nx)|_ny)*v)
                     + lambda*intg(sigma, gamma, u*(grad_y(H)|_ny)*v);
  BilinearForm alv = intg(sigma, gamma, u*(grad_x(H)|_nx)*v)+lambda*intg(sigma, gamma, u*H*v);
  TermMatrix A(auv), ALV(alv);
  TermVector B(intg(gamma, g*v));
  TermVector G(u, gamma, g);
  B+=ALV*G;
  //solve linear system AU=F
  TermVector U=directSolve(A, B);
  saveToFile("U.vtk", U);
  //integral representation on omega_ext
  Space Vext(_domain=omega_ext, _interpolation=P2, _name="Vext", _notOptimizeNumbering);
  Unknown uext(Vext, _name="uext");
  TermVector Uext = - integralRepresentation(uext, omega_ext, intg(gamma, (grad_y(H)|_ny)*U))
                    + integralRepresentation(uext, omega_ext, intg(gamma, H*G));
  saveToFile("Uext.vtk", Uext);
  //total field
  TermVector Ui(u, omega, ui), Utot=Ui+U;
  TermVector Uiext(uext, omega_ext, ui), Utotext=Uiext+Uext;
  saveToFile("Utot.vtk", Utot);
  saveToFile("Utotext.vtk", Utotext);
  return 0;
}
