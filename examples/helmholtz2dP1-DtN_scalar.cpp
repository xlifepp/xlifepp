#include "xlife++.h"
using namespace xlifepp;

// data on sigma-
Complex gp(const Point& P, Parameters& pa = defaultParameters)
{
  Real k=pa("k");
  return -i_*k;
}

// spectral functions
Real cosny(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2), h=pa("h");            // get the parameter h (user definition)
  Number n=pa("basis index")-1;         // get the index of function to compute
  if (n==0) return sqrt(1./h);
  else      return sqrt(2./h)*cos(n*pi_*(y+h*0.5)/h);
}

int main(int argc, char** argv)
{
  init(argc, argv, _lang=en); // mandatory initialization of xlifepp

  // mesh domain ]0, l[ x ]-h/2, h/2[
  Real l=pi_, h=1;
  Rectangle r(_xmin=0, _xmax=l, _ymin=-h/2, _ymax=h/2, _hsteps=0.1, 
              _domain_name="Omega", _side_names=Strings("y=-h/2", "x=l", "y=h/2", "x=0"));
  Mesh mesh2d(r, _shape=triangle, _generator=structured);
  Domain omega=mesh2d.domain("Omega");
  Domain sigmaP=mesh2d.domain("x=l"), sigmaM=mesh2d.domain("x=0");

  // create P1 lagrange interpolation
  Space V(_domain=omega, _interpolation=P1, _name="V");
  Unknown u(V, _name="u"); TestFunction v(u, _name="v");

  // parameters
  Real k=1;
  Parameters params(h, "h");
  params << Parameter(k, "k");

  // create spectral space and DtN kernel
  Number N=5;
  Space Sp(_domain=sigmaP, _basis=Function(cosny, params), _dim=N, _name="cos(n*pi*y)");
  Unknown phiP(Sp, _name="phiP");
  Vector<Complex> lambda(N);
  for (Number n=0; n<N; n++) lambda[n]=sqrt(Complex(k*k-n*n*pi_*pi_/(h*h)));
  TensorKernel tkp(phiP, lambda);

  // create bilinear form and linear form
  BilinearForm auv = intg(omega, grad(u)|grad(v)) - k*k*intg(omega, u*v)
                   - i_*intg(sigmaP, sigmaP, u*tkp*v);
  LinearForm fv=intg(sigmaM, Function(gp, params)*v);
  TermMatrix A(auv, _name="A");
  TermVector B(fv, _name="B");

  // solve linear system AX=B using direct method
  TermVector U = directSolve(A, B);
  saveToFile("U", U, _format=vtu);

  return 0;
}
