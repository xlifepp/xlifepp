#include "xlife++.h"
using namespace xlifepp;

Real f(const Point& P, Parameters& pa = defaultParameters)
{return -1.;}

int main(int argc, char** argv)
{
  init(argc, argv, _lang=en); // mandatory initialization of xlifepp

  // mesh and domains
  Strings sn("x=0", "x=1");
  Segment seg(_xmin=0, _xmax=1, _nnodes=101, _domain_name="Omega", _side_names=sn);
  Mesh mesh1d(seg, _generator=structured, _name="P1-mesh");
  Domain omega = mesh1d.domain("Omega");
  Domain sigmaL = mesh1d.domain("x=0"), sigmaR = mesh1d.domain("x=1");

  // space, unknows, and test functions
  Space Vh(_domain=omega, _interpolation=P1, _name="Vh");
  Unknown u(Vh, _name="u");
  TestFunction v(u, _name="v");

  // define problem
  BilinearForm a = intg(omega, grad(u)|grad(v));
  LinearForm lf = intg(omega, f*v);
  EssentialConditions ecs = (u|sigmaL = 0) & (u|sigmaR = 0);

  // compute matrix and rhs
  TermMatrix A(a, ecs, _name="A");
  TermVector F(lf, _name="F");

  // solve linear system and save solution
  TermVector U=directSolve(A, F);
  saveToFile("U_1d", U, _format=vtu);
  return 0;
}
