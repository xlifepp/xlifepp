#include "xlife++.h"
using namespace xlifepp;

Real f(const Point& P, Parameters& pa = defaultParameters)
{return -8.;}

int main(int argc, char** argv)
{
  init(argc, argv, _lang=en); // mandatory initialization of xlifepp

  // create a mesh and Domains
  SquareGeo sq(_origin=Point(0., 0.), _length=1, _nnodes=10, _domain_name="Omega", _side_names=Strings("y=0", "x=1", "y=1", "x=0"));
  Mesh mesh2d(sq, _shape=triangle, _generator=structured);
  Domain omega=mesh2d.domain("Omega");
  Domain sigmaM=mesh2d.domain("x=0"), sigmaP=mesh2d.domain("x=1");

  // create interpolation
  Space V(_domain=omega, _interpolation=P2, _name"V");
  Unknown u(V, _name="u");
  TestFunction v(u, _name="v");

  // create bilinear form and linear form
  BilinearForm auv=intg(omega, grad(u)|grad(v));
  LinearForm fv=intg(omega, f*v);
  EssentialConditions ecs= (intg(sigmaM, u) = 0);
  TermMatrix A(auv, ecs, _name="A");
  TermVector F(fv, _name="B");

  // solve linear system AX=F using LU factorization
  TermVector U=directSolve(A, F);
  saveToFile("U_LA", U, _format=vtu);
  return 0;
}
