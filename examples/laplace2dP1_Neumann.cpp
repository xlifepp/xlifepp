#include "xlife++.h"
using namespace xlifepp;

Real cosx2(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return cos(pi_ * x) * cos(pi_ * y);
}

int main(int argc, char** argv)
{
  init(argc, argv, _lang=en);

  // mesh square
  SquareGeo sq(_origin=Point(0., 0.), _length=1, _nnodes=21);
  Mesh mesh2d(sq, _shape=triangle, _generator=structured);
  Domain omega = mesh2d.domain("Omega");

  // build space and unknown
  Space Vk(_domain=omega, _interpolation=P1, _name="Vk");
  Unknown u(Vk, _name="u");
  TestFunction v(u, _name="v");

  // define variational formulation
  BilinearForm auv = intg(omega, grad(u) | grad(v)) + intg(omega, u * v);
  LinearForm fv=intg(omega, cosx2 * v);

  // compute matrix and right hand side
  TermMatrix A(auv, _name="a(u, v)");
  TermVector B(fv, _name="f(v)");

  // LLt factorize and solve
  TermMatrix LD;
  ldltFactorize(A, LD);
  TermVector U = factSolve(LD, B);

  saveToFile("U_LN", U, _format=vtu);
  return 0;
}
