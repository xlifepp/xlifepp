#include "xlife++.h"
using namespace xlifepp;
using namespace std;

Vector<Real> g(const Point& p, Parameters& pa = defaultParameters)
{
    Vector<Real> r(2,0.);
    r(2)=1.;
    return r;
}

int main(int argc, char** argv)
{
  init(_lang=en); // mandatory initialization of xlifepp
  verboseLevel(1);
  //geometry data
  Square sq(_origin=Point(0.,0.),_length=1., _nnodes=30, _domain_name="Omega",
            _side_names=Strings("Gamma","Gamma", "Gamma", "Sigma"));
  Mesh mesh(sq, _shape=triangle);
  Domain Omega=mesh.domain("Omega");
  Domain Sigma=mesh.domain("Sigma");
  Domain Gamma=mesh.domain("Gamma");
  // solve Stokes 2D for several approximations
  list<pair<InterpolationType,InterpolationType>> pint={{P0,P2},{P1,P2},{P0,CR}};
  for (auto ps:pint)
  {
    Space H1(_domain=Omega,_interpolation=ps.second,_name="H1");
    Unknown u(H1,_name="u",_dim=2); TestFunction v(u,_name="v");
    Space L2(_domain=Omega,_interpolation=ps.first,_name="L2");
    Unknown p(L2,_name="p"); TestFunction q(p,_name="q");
    BilinearForm a = intg(Omega,grad(u)%grad(v))-intg(Omega,p*div(v))-intg(Omega,div(u)*q);
    EssentialConditions ecs = (u|Gamma = 0) & (u | Sigma = g) & (intg(Omega,p)=0);
    TermMatrix A(a,ecs,_name="A");
    TermVector b(u,Omega,0.,_name="b");
    TermVector X=directSolve(A,b);
    String filename=fileNameFromComponents("u", words("interpolation",ps.first)+"_"+words("interpolation",ps.second), "vtu");
    saveToFile(filename, X(p));
    if (ps.second==_CR)
    {
      Space V1(_domain=Omega,_interpolation=P1, _name="V1");
      TermVector u1=projection(X(u),V1,2);
      saveToFile(filename, u1, _data_name="u1");
    }
    else saveToFile(filename, X(u));
  }
  return 0;
}
