#include "xlife++.h"
using namespace xlifepp;

int main(int argc, char** argv) {
  init(argc, argv, _lang=en); // mandatory initialization of XLiFE++

  theCout << " Eigenvalues of the 1D Laplace operator with Neumann conditions\n";
  theCout << " ==============================================================\n";

  Number nbint=1; // number of intervalls
  Number dk=60;   // interpolation degree
  theCout << "Interpolation degree = " << dk << eol;

  // mesh : segment [0, pi]
  Segment seg(_xmin=0, _xmax=pi_, _nnodes=nbint+1);
  Mesh zeroPi(seg);
  Domain omega = zeroPi.domain("Omega");

  Space Vk(_domain=omega, _FE_type=Lagrange, _FE_subtype=GaussLobattoPoints, _order=dk, _Sobolev_type=H1, _name="Vk");
  Unknown u(Vk, _name="u");
  TestFunction v(u, _name="v");

  Number qrodeg = 2*dk+1;
  BilinearForm muv = intg(omega, u * v, _quad=defaultQuadrature, _order=qrodeg), 
               auv = intg(omega, grad(u) | grad(v), _quad=defaultQuadrature, _order=qrodeg);
  // Compute the Stiffness and Mass matrices
  TermMatrix S(auv, _name="auv"), M(muv, _name="muv");

  // The eigenvalue problem writes S x = l M x
  // Compute the nev first eigenvalues with smallest magnitude with Arpack
  Number nev = 8;
  EigenElements areigs = arpackSolve(S, M, _nev=nev, _which="SM");
  // Other computational mode choice, here more powerfull: 
  // EigenElements areigs = arpackSolve(S, M, _nev=nev, _sigma=0.5);

  theCout << arEigInfos();

  theCout << "Eigenvalues :" << eol;
  Number nconv = areigs.numberOfEigenValues();
  for (Number i = 0; i < nconv; i++) { theCout << std::setprecision(17) << areigs.value(i+1).real() << eol; }

  saveToFile("Sy", areigs, _format=matlab);
}
