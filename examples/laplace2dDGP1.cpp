#include "xlife++.h"
using namespace xlifepp;

Real uex(const Point& p, Parameters& pars=defaultParameters)
{return sin(pi_*p(1))*sin(pi_*p(2));}
Real f(const Point& p, Parameters& pars=defaultParameters)
{return 2*pi_*pi_*sin(pi_*p(1))*sin(pi_*p(2));}
Real fmu(const Point& p, Parameters& pars=defaultParameters)
{GeomElement* gelt=getElementP();
 if(gelt!=0) return 1/gelt->measure();
 return 0.;
}

int main(int argc, char** argv)
{
  init(argc, argv, _lang=en); // mandatory initialization of xlifepp
  // create mesh and geometrical domains
  Rectangle R(_origin=Point(0.,0.),_xlength=1., _ylength=1.,_nnodes=31,_domain_name="Omega");
  Mesh mR(R, _shape=triangle, _generator=structured, _split_direction=alternate);
  Domain omega=mR.domain("Omega");
  Domain gamma=sides(omega);  // create domain of sides
  // create discontinuous P1-Lagrange space
  Space V(_domain=omega, _FE_type=Lagrange, _order=1, _Sobolev_type=L2, _name="V");
  Unknown u(V, _name="u"); TestFunction v(u, _name="v");
  // create bilinear form and TermMatrix
  Function mu(fmu); mu.require("element");
  BilinearForm a=intg(omega,grad(u)|grad(v))-intg(gamma,mean(grad(u)|_n)*jump(v))
                -intg(gamma,jump(u)*mean(grad(v)|_n))+intg(gamma,mu*jump(u)*jump(v));
  TermMatrix A(a, _name="A");
  // create the right hand side an solve the linear system
  TermVector F(intg(omega,f*v), _name="F");
  TermVector U=directSolve(A,F);
  saveToFile("U", U, _format=vtu);
  // compute L2 error
  TermMatrix M(intg(omega,u*v), _name="M");
  TermVector Uex(u, omega, uex);
  TermVector E=U-Uex;
  theCout<<"  IP : |U-uex|L2 = "<<sqrt(M*E|E)<<eol;
  return 0;
}
