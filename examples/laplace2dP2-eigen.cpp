#include "xlife++.h"
using namespace xlifepp;

int main(int argc, char** argv)
{
  init(argc, argv, _lang=en); // mandatory initialization of xlifepp

  // mesh square
  SquareGeo sq(_origin=Point(0., 0.), _length=1, _nnodes=20);
  Mesh mesh2d(sq, _shape=triangle, _generator=gmsh);
  Domain omega = mesh2d.domain("Omega");

  // build  P2 interpolation
  Space Vk(_domain=omega, _interpolation=P2, _name="Vk");
  Unknown u(Vk, _name="u");
  TestFunction v(u, _name="v");

  // build eigen system
  BilinearForm auv = intg(omega, grad(u) | grad(v)) + intg(omega, u * v) , 
               muv = intg(omega, u * v);
  TermMatrix A(auv, _name="auv"), M(muv, _name="muv");

  // compute the 10 first smallest in magnitude
  EigenElements eigs = eigenInternSolve(A, M, _nev=10, _mode=krylovSchur, _which="SM");   // internal solver
  theCout << eigs.values;
  saveToFile("eigs", eigs.vectors, _format=vtu);
  return 0;
}
