/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomRefElement.cpp
  \authors D. Martin, N.Kielbasiewicz
  \since 27 dec 2002
  \date 7 aug 2012

  \brief Implementation of xlifepp::GeomRefTriangle class members and related functions
 */

#include "GeomRefElement.hpp"
#include "./point/GeomRefPoint.hpp"
#include "./segment/GeomRefSegment.hpp"
#include "./quadrangle/GeomRefQuadrangle.hpp"
#include "./triangle/GeomRefTriangle.hpp"
#include "./tetrahedron/GeomRefTetrahedron.hpp"
#include "./hexahedron/GeomRefHexahedron.hpp"
#include "./prism/GeomRefPrism.hpp"
#include "./pyramid/GeomRefPyramid.hpp"
#include "utils.h"

namespace xlifepp
{
//--------------------------------------------------------------------------------
//    Constructors, Destructor
//--------------------------------------------------------------------------------

//! default constructor
GeomRefElement::GeomRefElement()
  : shapeType_(_segment), dim_(0), nbVertices_(0), nbSides_(0), nbSideOfSides_(0), measure_(0.)
{}

//! constructor for 1D elements
GeomRefElement::GeomRefElement(ShapeType sh, const real_t m, const real_t c)
  : shapeType_(sh), dim_(1), nbVertices_(2), nbSides_(2), nbSideOfSides_(0),
    measure_(m), centroid_(dim_, c), vertices_(dim_*nbVertices_),
    sideShapeTypes_(nbSides_), sideVertexNumbers_(nbSides_)
{
  // add new GeomRefElement pointer to std::vector of GeomRefElement*
  theGeomRefElements.push_back(this);
}

//! constructor for 2D elements, with number of vertices
GeomRefElement::GeomRefElement(ShapeType sh, const real_t m, const real_t c, const number_t v)
  : shapeType_(sh), dim_(2), nbVertices_(v), nbSides_(v), nbSideOfSides_(v),
    measure_(m), centroid_(dim_, c), vertices_(dim_*nbVertices_),
    sideShapeTypes_(nbSides_), sideVertexNumbers_(nbSides_), sideOfSideVertexNumbers_(nbSideOfSides_), sideOfSideNumbers_(nbSides_)
{
  theGeomRefElements.push_back(this);
}

// constructor for 3D elements, with number of vertices and number of edges
// number of faces is given by Euler's formula: nbVertices_ - nbSideOfSides_ + nbSides_ = 2
GeomRefElement::GeomRefElement(ShapeType sh, const real_t m, const real_t c, const number_t v, const number_t e)
  : shapeType_(sh), dim_(3), nbVertices_(v), nbSides_(static_cast<number_t>(2 - v + e)), nbSideOfSides_(e),
    measure_(m), centroid_(dim_, c), vertices_(dim_*nbVertices_),
    sideShapeTypes_(nbSides_), sideVertexNumbers_(nbSides_), sideOfSideVertexNumbers_(nbSideOfSides_), sideOfSideNumbers_(nbSides_)
{
  theGeomRefElements.push_back(this);
}

// constructor for any dimension elements, with number of vertices, edges and faces
GeomRefElement::GeomRefElement(ShapeType sh, const dimen_t d, const real_t m, const real_t c, const number_t v, const number_t e, const number_t f)
  : shapeType_(sh), dim_(d), nbVertices_(v), nbSides_(f), nbSideOfSides_(e),
    measure_(m), centroid_(dim_, c), vertices_(dim_*nbVertices_),
    sideShapeTypes_(nbSides_), sideVertexNumbers_(nbSides_), sideOfSideVertexNumbers_(nbSideOfSides_), sideOfSideNumbers_(nbSides_)
{
  theGeomRefElements.push_back(this);
}

GeomRefElement::~GeomRefElement() //! destructor
{
  // remove pointer of current object from std::vector of GeomRefElement*
  std::vector<GeomRefElement*>::iterator it(find(theGeomRefElements.begin(), theGeomRefElements.end(), this));
  if(it != theGeomRefElements.end()) { theGeomRefElements.erase(it); }
}

// delete all GeomRefElement objects
void GeomRefElement::clearGlobalVector()
{
  while( GeomRefElement::theGeomRefElements.size() > 0 ) { delete GeomRefElement::theGeomRefElements[0]; }
}

// print the list of GeomRefElements objects in memory
void GeomRefElement::printAllGeomRefElements(std::ostream& out)
{
    number_t vb=theVerboseLevel;
    verboseLevel(1);
    out<<"GeomRefElements in memory: "<<eol;
    std::vector<GeomRefElement*>::iterator it=theGeomRefElements.begin();
    for(; it!=theGeomRefElements.end(); ++it) out<<"  "<<(*it)<<": "<<(**it)<<eol;
    verboseLevel(vb);
}

//--------------------------------------------------------------------------------
// public member functions
//--------------------------------------------------------------------------------
//! returns shape of element as a string
string_t GeomRefElement::shape(const number_t sideNum) const
{
  if (sideNum == 0) { return words("shape", shapeType_); }
  else { return words("shape", sideShapeTypes_[sideNum - 1]); }
}

//! returns shape of element
//number_t GeomRefElement::shapeNumber(const number_t sideNum) const {
//  if (sideNum==0) { return shapeNum_; }
//  else { return sideShapeNums_[sideNum-1]; }
//}

ShapeType GeomRefElement::shapeType(const number_t sideNum ) const
{
  if (sideNum == 0) { return shapeType_; }
  else { return sideShapeTypes_[sideNum - 1]; }
}

//! returns number of vertices
number_t GeomRefElement::nbVertices(const number_t sideNum) const
{
  if (sideNum == 0) { return nbVertices_; }
  else { return sideVertexNumbers_[sideNum - 1].size(); }
}

//! returns coordinates of vNum-th vertex as a std::vector ( vNum = 1,2, ...)
std::vector<real_t>::const_iterator GeomRefElement::vertex(const number_t vNum) const
{
  return vertices_.begin() + dim_ * ((vNum - 1) % nbVertices_);
}

//! returns local number of -i-th vertex ( i = 1,2, ...) on side number sideNum (sideNum = 1,2,...) of dimension d
number_t GeomRefElement::sideVertexNumber(const number_t i, const number_t sideNum) const
{
  number_t localNum = i;
  if (sideNum != 0)
  {
    localNum = sideVertexNumbers_[sideNum - 1][i - 1];
  }
  return localNum;
}

//! returns local number of -i-th vertex ( i = 1,2, ...) on side of side number sideOfSideNum (sideOfSideNum = 1,2,...)
number_t GeomRefElement::sideOfSideVertexNumber(const number_t i, const number_t sideOfSideNum) const
{
  number_t localNum = i;
  if (sideOfSideNum != 0)
  {
    localNum = sideOfSideVertexNumbers_[sideOfSideNum - 1][i - 1];
  }
  return localNum;
}

//! returns local number of -i-th side of side ( i = 1,2, ...) on side number sideNum (sideNum = 1,2,...)
int_t GeomRefElement::sideOfSideNumber(const number_t i, const number_t sideNum) const
{
  return sideOfSideNumbers_[sideNum - 1][i - 1];
}

//! circular permutation of vertices such that vertex newFirst is first new vertex
void GeomRefElement::rotateVertices(const number_t newFirst, std::vector<number_t>& rotatedVertices) const
{
  number_t next(newFirst);
  for ( std::vector<number_t>::iterator it = rotatedVertices.begin() ; it != rotatedVertices.end(); it++ )
  {
    *it = static_cast<number_t>(1 + (next++ -1) % nbVertices_);
  }
}

// test if a point belongs to current element (virtual see child)
bool GeomRefElement::contains(std::vector<real_t>& p, real_t tol) const
{
  error("not_handled","GeomRefElement::contains(p)");
  return false;
}

// compute projection of a point onto current element (virtual see child)
std::vector<real_t> GeomRefElement::projection(const std::vector<real_t>& p, real_t& h) const
{
  error("not_handled","GeomRefElement::projection(p)");
  return p;
}

// return coordinates of a point on side (s>=1) defined in local coordinates, say in curvilinear coordinates
// perm : additional permutation numbering vector (index starts from 0)
std::vector<real_t> GeomRefElement::sideToElt(number_t s, std::vector<real_t>::const_iterator p, const std::vector<number_t>& perm) const
{
  const std::vector<number_t>& sn = sideVertexNumbers_[s-1];
  std::vector<real_t> q(dim_);
  std::vector<real_t>::const_iterator itv;
  real_t x, y, z;
  number_t i=0;
  bool wp = perm.size()>0;
  if (wp) i=perm[0];
  switch (dim_)
  {
   case 1: itv=vertex(sn[i]);
           q[0]= *itv;  break;     // side is a point
   case 2: x=*p; y=1-x;
           itv=vertex(sn[i]);      // side is a segment
           q[0]=y* *itv++; q[1]=y* *itv;
           if (wp) i=perm[1]; else i=1;
           itv=vertex(sn[i]);
           q[0] += x* *itv++; q[1] += x* *itv;
           break;
   case 3: x=*p++; y=*p; z=1-x-y; // side is a triangle, a rectangle, ...
           itv=vertex(sn[i]);
           q[0] = z* *itv++; q[1] = z* *itv++; q[2] = z* *itv;
           if (wp) i=perm[1]; else i=1;
           itv=vertex(sn[i]);
           q[0] += x* *itv++; q[1] += x* *itv++; q[2] += x* *itv;
           if (wp) i=perm[2]; else i=2;
           itv=vertex(sn[i]);
           q[0] += y* *itv++; q[1] += y* *itv++; q[2] += y* *itv;
           break;
    default: where("GeomRefElement::sideToElt");
             error("bad_dim", dim_, 3);
  }
  return q;
}

//--------------------------------------------------------------------------------
// errors handlers
//--------------------------------------------------------------------------------

void GeomRefElement::noSuchFunction(const string_t& s) const //! function not yet implemented for a type of element
{
  error("undef_elem_member_fct", s, this->shape());
}

void GeomRefElement::noSuchSideOfSide(const number_t n) const //! edge number too large
{
  error("bad_sideofside_num", n, shape());
}

void GeomRefElement::noSuchSide(const number_t n) const //! face number too large
{
  error("bad_side_num", n, shape());
}

void GeomRefElement::noSuchSide(const number_t v1, const number_t v2, const number_t v3, const number_t v4) const //! vertex numbers too large
{
  if ( v4 == 0 )
  {
    if ( v3 == 0 )
    {
      error("bad_side_segment_vertex_nums", v1, v2, shape());
    }
    else
    {
      error("bad_side_triangle_vertex_nums", v1, v2, v3, shape());
    }
  }
  else
  {
    error("bad_side_quadrangle_vertex_nums", v1, v2, v3, v4, shape());
  }
}

//=================================================================================
// Extern class related functions
//=================================================================================

//! prints GeomRefElement object to  ostream
std::ostream& operator<<(std::ostream& os, const GeomRefElement& obj)
{
  os << words("geometric reference element") << ": " << words("shape") << " " << obj.shape();
  if ( obj.nbVertices_ > 0 ) { os << ", " << obj.nbVertices_ << " " << words("vertices"); }
  if ( obj.nbSides_ > 0 ) { os << ", " << obj.nbSides_ << " " << words("sides"); }
  if ( obj.nbSideOfSides_ > 0 ) { os << ", " << obj.nbSideOfSides_ << " " << words("sideOfSides"); }
  if ( theVerboseLevel < 11 ) return os;
  std::vector<real_t>::const_iterator itc;
  os << std::endl;
  itc = obj.vertices_.begin();
  for(number_t i = 0; i < obj.nbVertices_; i++)
  {
      os << "  * " << words("vertex") << " " << i + 1 << " -> (";
      for(number_t j=0; j<obj.dim_-1; ++j) os <<*itc++<<" ,";
      os <<*itc++<<")"<<eol;
  }
  itc=obj.centroid_.begin();
  os << "  * " <<words("centroid")<<" : (";
  for(number_t j=0; j<obj.dim_-1; ++j) os <<*itc++<<" ,";
  os <<*itc<<")"<<eol;
  os << "  * " <<words("measure")<<": "<<obj.measure_<<eol;
  for(number_t i = 0; i < obj.nbSides_; i++)
  {
    os << "  * " << words("side") << " " << i + 1 << " -> " << words("shape") << " ";
    if (obj.shape() == "segment") os << words("point");
    else os << words("shape", obj.sideShapeTypes_[i]);
    os << ", ";
    if (obj.sideVertexNumbers_[i].size() == 1) os << words("vertex");
    else  os << words("vertices");
    os << ":";
    for(number_t j = 0; j < obj.sideVertexNumbers_[i].size(); j++)
        os << " " << obj.sideVertexNumbers_[i][j];
    os << std::endl;
  }
  for(number_t i = 0; i < obj.nbSideOfSides_; i++)
  {
      os << "  * " << words("side of side") << " " << i + 1 << " -> "<<obj.sideOfSideVertexNumbers_[i]<<eol;
  }
  for(number_t i = 0; i < obj.sideOfSideNumbers_.size(); i++)
  {
      os << "  * " << words("numbers") << " " << words("on") << " " << words("side")
         << i + 1 << " -> "<<obj.sideOfSideNumbers_[i]<<eol;
  }

  return os;
}

/*!
  GeomRefElementFind
  definition of a Geom Reference Element by shape number
  use existing Geom Reference Element object if one already exists
  otherwise create a new one, in both cases returns pointer to the object
 */
GeomRefElement* findGeomRefElement(ShapeType shape)
{
  // find geometric reference element in std::vector theGeomRefElements
  // (one should not create a new if it already exists)
  for ( std::vector<GeomRefElement*>::iterator it = GeomRefElement::theGeomRefElements.begin(); it != GeomRefElement::theGeomRefElements.end(); it++)
  {
    if ( shape == (*it)->shapeType() ) { return *it; }
  }

  // this is a new one (a geometric reference element never seen before)
  // to be created as a new GeomRefElement object
  // in one of the following Geom"Shape" functions
  //
  // create new reference element
  switch (shape) {
    case _point:       return new GeomRefPoint();       break;
    case _segment:     return new GeomRefSegment();     break;
    case _triangle:    return new GeomRefTriangle();    break;
    case _quadrangle:  return new GeomRefQuadrangle();  break;
    case _tetrahedron: return new GeomRefTetrahedron(); break;
    case _hexahedron:  return new GeomRefHexahedron();  break;
    case _prism:       return new GeomRefPrism();       break;
    case _pyramid:     return new GeomRefPyramid();     break;
    default:
      error("unknown_elem_shape_num", shape);
      break;
  }
  return nullptr;
}

} // end of namespace xlifepp
