/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RefDof.cpp
  \authors D. Martin, N. Kielbasiewicz, E. Lunéville
  \since 19 jan 2005
  \date 10 may 2012

  \brief Implementation of xlifepp::RefDof class members and related functions
 */

#include "RefDof.hpp"

#include <fstream>

namespace xlifepp
{

std::ostream& operator<<(std::ostream& out, const DofLocalization& dl)
{ out << words("dof location", dl); return out; }
std::ostream& operator<<(std::ostream& out, const ProjectionType& pt)
{ out << words("projection type", pt); return out; }

//--------------------------------------------------------------------------------
// Constructors, Destructor
//--------------------------------------------------------------------------------
//! general constructor
RefDof::RefDof(RefElement* relt, bool sh, DofLocalization w, number_t supNum, number_t ind, dimen_t e_dim, dimen_t s_dim, number_t nodeNum,
               dimen_t di , number_t ord, ProjectionType proj_t, DiffOpType dop, const string_t& nm)
  : refElt_p(relt), sharable_(sh), where_(w), supportNum_(supNum), index_(ind), supportDim_(s_dim),  nodeNum_(nodeNum),
    dim_(di), order_(ord), projectionType_(proj_t), diffop_(dop), name_(nm)
{
  if ( supportDim_ == 0 ) { coords_.resize(e_dim); }
}

//! destructor
RefDof::~RefDof() {}

//--------------------------------------------------------------------------------
// accessors to D.o.F coordinates
//--------------------------------------------------------------------------------
void RefDof::coords(real_t c1)
{
  coords_.resize(1);
  std::vector<real_t>::iterator it_c = coords_.begin();
  *it_c = c1;
}

void RefDof::coords(real_t c1,real_t c2)
{
  coords_.resize(2);
  std::vector<real_t>::iterator it_c = coords_.begin();
  *it_c++ = c1; *it_c = c2;
}

void RefDof::coords(real_t c1,real_t c2,real_t c3)
{
  coords_.resize(3);
  std::vector<real_t>::iterator it_c = coords_.begin();
  *it_c++ = c1; *it_c++ = c2; *it_c = c3;
}

void RefDof::coords(const std::vector<real_t>& v)
{
  coords_.resize(v.size());
  std::vector<real_t>::iterator it_c = coords_.begin();
  for ( std::vector<real_t>::const_iterator it_v = v.begin(); it_v != v.end(); it_v++ )
  {
    *it_c++ = *it_v;
  }
}

//--------------------------------------------------------------------------------
// I/O utilities
//--------------------------------------------------------------------------------
//! print Reference D.o.F data
std::ostream& operator<<(std::ostream& os, const RefDof& obj)
{
  os << words("reference DoF") << ": " << obj.name_ << ", "
     << words("sharable") << "? " << words(obj.sharable_)  << ", "
     << words("support") << ": " << words("dim") << " " << obj.supportDim_ << ", "
     << words("dof location",obj.where_)<<" " << obj.supportNum_ << ", "
     << "index "<<obj.index_<<", dif. operator "<<words("diffop",obj.diffop_);
  if(obj.coords_.size()>0)
  {
    os<<",";
    if (obj.supportDim_ > 0) os << " virtual";
    os << " "<<words("coords")<<" = " << std::setprecision(5)<< obj.coords_;
  }
    if(obj.order_>0)
  {
    os << ", "<<words("derivative order")<<": "<<obj.order_;
    if(obj.derivativeVector_.size()>0) os<<", "<<words("derivative vector")<<": "<<obj.derivativeVector_;
  }
  if(obj.projectionType_!=_noProjection)
  {
    os << ", "<<words("projection type",obj.projectionType_);
    if(obj.projectionVector_.size()>0) os<<", "<<words("projection vector")<<": "<<obj.projectionVector_;
  }
  return os;
}

} // end of namespace xlifepp
