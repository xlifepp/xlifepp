/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file RefTetrahedron.hpp
	\authors D. Martin, N. Kielbasiewicz
	\since 21 dec 2002
	\date 6 aug 2012

  \brief Definition of the xlifepp::RefTetrahedron class

	External class related functions
	--------------------------------
	  - selectRefTetrahedron: construction of a tetrahedric Reference Element
	  - tetrahedronLagrangeStd: construction of a Lagrange tetrahedric Reference Element
	  - tetrahedronCrouzeixRaviartStd: construction of a Crouzeix_Raviart tetrahedric Reference Element
 */

#ifndef REF_TETRAHEDRON_HPP
#define REF_TETRAHEDRON_HPP

#include "config.h"
#include "../RefElement.hpp"

namespace xlifepp
{

/*!
	\class RefTetrahedron

	child to class RefElement

	Parent class: RefElement
	Child classes: LagrangeTetrahedron
 */
class RefTetrahedron : public RefElement
{
  public:
    //! constructor by interpolation
    RefTetrahedron(const Interpolation* interp_p) : RefElement(_tetrahedron, interp_p) {}
    //! destructor
    virtual ~RefTetrahedron() {}

    virtual void interpolationData() = 0; //!< returns interpolation data
    virtual void outputAsP1(std::ofstream& os, const int, const std::vector<number_t>&) const { noSuchFunction("outputAsP1"); }
    const splitvec_t& getO1splitting() const // override
       { return RefElement::getO1splitting(); }

    virtual void sideNumbering() = 0; //!< returns side numbering

}; // end of class RefTetrahedron

//================================================================================
// Extern class related functions and declarations
//================================================================================

RefElement* tetrahedronLagrangeStd(const Interpolation*);
RefElement* tetrahedronCrouzeixRaviartStd(const Interpolation*);
RefElement* tetrahedronNedelecFace(const Interpolation*);
RefElement* tetrahedronNedelecEdge(const Interpolation*);

} // end of namespace xlifepp

#endif /* REF_TETRAHEDRON_HPP */
