/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file NedelecEdgeTetrahedron.cpp
  \authors E. Lunéville
  \since 15 september 2015
  \date  15 september 2015

  \brief Implementation of xlifepp::NedelecEdgeTetrahedron class members and related functions
 */

#include "NedelecEdgeTetrahedron.hpp"
#include "../Interpolation.hpp"
#include "../integration/Quadrature.hpp"
#include "../triangle/LagrangeTriangle.hpp"
#include "../segment/LagrangeSegment.hpp"
#include "LagrangeTetrahedron.hpp"
#include "../../geometry/GeomElement.hpp"
#include "utils.h"
#include <iostream>

namespace xlifepp
{

//! NedelecEdgeTetrahedron constructor
NedelecEdgeTetrahedron::NedelecEdgeTetrahedron(const Interpolation* interp_p)
  : RefTetrahedron(interp_p)
{
  name_ += "_NedelecEdge_";
  mapType = _covariantPiolaMap;
  dofCompatibility=_signDofCompatibility;
  dimShapeFunction=3;
}

NedelecEdgeTetrahedron::~NedelecEdgeTetrahedron() {}

//=====================================================================================
/*! Nedelec edge first family of order k on tetrahedron (NE1k)
*/
//=====================================================================================
NedelecEdgeFirstTetrahedronPk::NedelecEdgeFirstTetrahedronPk(const Interpolation* interp_p)
  : NedelecEdgeTetrahedron(interp_p)
{
  name_ += "first family_"+tostring(interp_p->numtype);
  interpolationData();    // build element interpolation data
  sideOfSideNumbering();  // local numbering of dofs on edges
  sideNumbering();        // local numbering of dofs on faces
  pointCoordinates();     // "virtual coordinates" of moment dofs
  //sideRefElement();       // Reference Element on edges
  reverseEdge = true;     // tells to reverse edge orientation when evaluating edge dof using general algorithm in Dof class
}

NedelecEdgeFirstTetrahedronPk::~NedelecEdgeFirstTetrahedronPk() {}

//! interp defines Reference Element interpolation data
void NedelecEdgeFirstTetrahedronPk::interpolationData()
{
  trace_p->push("NedelecEdgeFirstTetrahedronPk::interpolationData");

  number_t k = interpolation_p->numtype;
  nbDofs_= (k*(k+2)*(k+3))/2;
  nbDofsInSideOfSides_=6*k;   //edge
  nbDofsInSides_=4*k*(k-1);   //face
  nbInternalDofs_=k*(k-1)*(k-2)/2;

  /* Creating Degrees Of Freedom of reference element
     in Nedelec first family order k elements, D.o.Fs are defined as moment D.o.Fs
        edge dofs: v-> int_e v.t q,         q in  P_(k-1)[e]               k dofs by edge e
        face dofs (k>1)       : v-> int_f v.q/mes(f),    q in {P_(k-2)[f]^3, q.n=0}     k(k-1) dofs by face
        tetrahedron dofs (k>2): v-> int_t v.q,           q in  P_(k-3)[t]^3             k(k-1)(k-2)/2 dofs
     numbering of D.oFs are given by the basis functions order of dual spaces defining D.o.Fs beginning by edges

     NOTE: moment D.o.Fs can be reinterpreted as punctual D.o.Fs using a well adapted quadrature formulae
            we do not use this representation here
  */
  refDofs.reserve(nbDofs_);
  number_t nbds=nbDofsInSides_/4, nbdss=nbDofsInSideOfSides_/6;
  for(number_t e = 1; e <= 6; ++e)        //edge dofs (rank in dual polynoms basis is used to identify dof)
    for(number_t i=0; i<nbdss; ++i)
      refDofs.push_back(new RefDof(this, true, _onEdge, e, i+1, 3, 1, 0, 3, 0, _crossnProjection,_ncross, "int_e u.t q"));
  if(k>1)
  {
    for(number_t f = 1; f <= 4; ++f)              //face dofs (rank in dual polynoms basis is used to identify dof)
      for(number_t i=0; i<nbds; ++i)
        refDofs.push_back(new RefDof(this, true, _onFace, f, i+1, 3, 2, 0, 3, 0, _noProjection,_id, "int_f u.q"));
    if(k>2)
    {

      for(number_t i=0; i<nbInternalDofs_; ++i) //element dofs (order = i is used to identify dof)
        refDofs.push_back(new RefDof(this, false, _onElement, 0, i+1, 3, 3, 0, 3, 0, _noProjection,_id, "int_k u.q"));
    }
  }

  // compute shape functions as formal polynomials
  computeShapeFunctions();
  buildPolynomialTree();   //construct tree representation
  maxDegree = Wk.degree();

  // compute number of shape functions
  nbshfcts_ = shapeValueSize();

  // requires face dofs rotation
  rotateDof = (k>1);

  trace_p->pop();
}

//! compute shape functions as polynomials using general algorithm
/*  mapping from ref. triangle ((1,0),(0,1), (0,0)) onto tetrahedron face is chosen as follows on face(i,j,k)
                                            on face 1 (4,3,2) : (0,1-x-y,y)
     (x,y) -> x*Vi + y*Vj +(1-x-y)*Vk:   on face 2 (4,1,3) : (y,0,1-x-y)
                                            on face 3 (4,2,1) : (1-x-y,y,0)
                                            on face 4 (1,2,3) : (x,y,1-x-y)
    mapping from ref. segment [0,1] onto tetrahedron edge is chosen as follows on edge (i,j)
                                     on edge 1 (1,4) : (x,0,0)
                                     on edge 2 (2,4) : (0,x,0)
       x -> x*Vi + (1-x)*Vj:     on edge 3 (3,4) : (0,0,x)
                                     on edge 4 (2,3) : (0,x,1-x)
                                     on edge 5 (1,3) : (x,0,1-x)
                                     on edge 6 (1,2) : (x,1-x,0)

     edge dofs: v-> int_e v.t q,         q in  P_(k-1)[e]               k dofs by edge e
     face dofs (k>1)       : v-> int_f v.q/mes(f),    q in {P_(k-2)[f]^3, q.n=0}     k(k-1) dofs by face
     tetrahedron dofs (k>2): v-> int_t v.q,           q in  P_(k-3)[t]^3             k(k-1)(k-2)/2 dofs
*/

void NedelecEdgeFirstTetrahedronPk::computeShapeFunctions()
{
  number_t k = interpolation_p->numtype;
  PolynomialsBasis::iterator itp, itq;
  std::vector<real_t>::const_iterator itpt, itw; //for quadrature rule

  PolynomialsBasis Vk(_Rk,3,k);       //Nedelec first family polynomials space Rk = P_(k-1)^3 + Sk
  Matrix<real_t> L(nbDofs_,nbDofs_);  //matrix of linear system of shape functions

  //EDGES: compute Matrix Lij=l(pi,qj) for all pi in Vk, for all qj in P_(k-1)[e]
  LagrangeStdSegment segPk(findInterpolation(_Lagrange, _standard, k-1, H1));
  segPk.computeShapeFunctions();
  Ek=segPk.Wk;       //dual space for edge dofs: P_(k-1)[x]
  dimen_t d=Vk.degree()+Ek.degree();
  Quadrature* quad=findBestQuadrature(_segment, d);  //quadrature rule on segment of order d (integrates exactly x^d)
  number_t nbq= quad->numberOfPoints();
  number_t j=1;
  for(itp=Vk.begin(); itp!=Vk.end(); ++itp, ++j)
  {
    number_t i=1;
    for(number_t e = 1; e <= 6; ++e)
    {
      Vector<real_t> t(3,0.);   //unit tangential vector chosen as (vj-vi)/|vj-vi| on edge (vi,vj)
      switch(e)
      {
        case 1: t[0]=-1.; break;             //edge (1,4)
        case 2: t[1]=-1.; break;             //edge (2,4)
        case 3: t[2]=-1.; break;             //edge (3,4)
        case 4: t[1]=-1.; t[2]= 1.;  break;  //edge (2,3)
        case 5: t[0]=-1.; t[2]= 1.;  break;  //edge (1,3)
        default: t[0]=-1.; t[1]= 1.;          //edge (1,2)
      }
      Polynomial pt=dot(*itp,t);
      for(itq=Ek.begin(); itq!=Ek.end(); ++itq, ++i)
      {
        L(i,j)=0;
        itpt=quad->point();
        itw=quad->weight();
        Polynomial& pq=(*itq)[0];
        for(number_t q=0; q<nbq; ++q, ++itw)   //compute by 1D quadrature
        {
          real_t x=*itpt++;     //quadrature point
          switch(e)
          {
            case 1: L(i,j)+= pt(x,0,0) * pq(x)** itw ; break;    //on edge (1,4) : x -> (x,0,0)
            case 2: L(i,j)+= pt(0,x,0) * pq(x)** itw ; break;    //on edge (2,4) : x -> (0,x,0)
            case 3: L(i,j)+= pt(0,0,x) * pq(x)** itw ; break;    //on edge (3,4) : x -> (0,0,x)
            case 4: L(i,j)+= pt(0,x,1-x) * pq(x)** itw ; break;  //on edge (2,3) : x -> (0,x,1-x)
            case 5: L(i,j)+= pt(x,0,1-x) * pq(x)** itw ; break;  //on edge (1,3) : x -> (x,0,1-x)
            default: L(i,j)+= pt(x,1-x,0) * pq(x)** itw ; break;  //on edge (1,2) : x -> (x,1-x,0)
          }
        }
      }
    }
  }

  // FACES: compute Matrix Lij=l(pi,qj) for all pi in Vk, qj in {P_(k-2)[f]^3, q.nf=0}
  /* construction of {P_(k-2)[f]^3, q.nf=0}  by face
         T ref. triangle,  F mapping from T to f (see above)
         wi i=1, m=k(k-1)/2   basis of P_(k-2)[T]
         Wi(x,y,z)=wi(F^-1(x,y,z)) is a basis of P_(k-2)[f]
         We choose as tangent vectors the edge of tetrahedron
                    on face 1 (4,3,2) : edge 4-3, edge 4-2 ->   (0,0,Wi)   (0,Wi,0)
                    on face 2 (4,1,3) : edge 4-1, edge 4-3 ->   (Wi,0,0)   (0,0,Wi)
                    on face 3 (4,2,1) : edge 4-2, edge 4-1 ->   (0,Wi,0)   (Wi,0,0)
                    on face 4 (1,2,3) : edge 1-2, edge 1-3 ->   (-Wi,Wi,0) (-Wi,0,Wi)
        Note: in order to correctly match dofs on shared face some rotation will be required (see rotateDofs function)
  */
  if(k>1)
  {
    LagrangeStdTrianglePk triPk(findInterpolation(_Lagrange, _standard, k-2, H1));
    PolynomialsBasis &Q=triPk.Wk;       //create dual space for face dofs: P_(k-2)[x,y]
    dimen_t d=Vk.degree()+Q.degree();
    Quadrature* quad=findBestQuadrature(_triangle, d);  //quadrature rule on triangle of order d (integrates exactly x^d)
    number_t nbq= quad->numberOfPoints();
    number_t j=1;
    for(itp=Vk.begin(); itp!=Vk.end(); ++itp, ++j)
    {
      number_t i=nbDofsInSideOfSides_+1;
      for(number_t f = 1; f <= 4; ++f)
      {
        real_t m=2;  // |J|/mes(f)=2mes(f)/mes(f)=2
        for(itq=Q.begin(); itq!=Q.end(); ++itq, i+=2)
        {
          L(i,j)=0; L(i+1,j)=0;
          itpt=quad->point();
          itw=quad->weight();
          Polynomial& pq=(*itq)[0];
          for(number_t q=0; q<nbq; ++q, ++itw) //compute by 2D quadrature
          {
            real_t x=*itpt++, y=*itpt++;       //quadrature point
            real_t vq=pq(x,y);                 //value of polynomial pq at quadrature point
            real_t v1,v2,v3;
            std::vector<Polynomial>& p=*itp;
            switch(f)
            {
              case 1: //face 1 : x=0 plane (4,3,2)
                v2=p[1].eval(0,1-x-y,y); v3= p[2].eval(0,1-x-y,y);
                L(i,j)  += vq*v3**itw *m;
                L(i+1,j)+= vq*v2**itw *m;
                break;
              case 2: //face 2 : y=0 plane (4,1,3)
                v1=p[0].eval(y,0,1-x-y); v3= p[2].eval(y,0,1-x-y);
                L(i,j)  += vq*v1**itw *m;
                L(i+1,j)+= vq*v3**itw *m;
                break;
              case 3: //face 3 : z=0 plane (4,2,1)
                v1=p[0].eval(1-x-y,y,0); v2= p[1].eval(1-x-y,y,0);
                L(i,j)  += vq*v2**itw *m;
                L(i+1,j)+= vq*v1**itw *m;
                break;
              default: //face 4 : x+y+z=1 plane (1,2,3)
                v1=p[0].eval(x,y,1-x-y); v2= p[1].eval(x,y,1-x-y); v3= p[2].eval(x,y,1-x-y);
                L(i,j)  += vq*(v2-v1)**itw *m;
                L(i+1,j)+= vq*(v3-v1)**itw *m;
            }
          }
        }
      }
    }
  }

  // TETRAHEDRON: compute Matrix Lij=l(pi,qj) for all pi in Vk, qj in P_(k-3)[t]^3
  if(k>2)
  {
    Kk=PolynomialsBasis(PolynomialBasis(_Pk,3,k-3),3); //P_(k-3)[t]^3
    PolynomialsBasis::iterator itr;
    dimen_t d=Vk.degree()+Kk.degree();
    quad=findBestQuadrature(_tetrahedron,d);  //quadrature rule on tetrahedron of order d
    nbq= quad->numberOfPoints();
    j=1;
    for(itp=Vk.begin(); itp!=Vk.end(); ++itp,++j)
    {
      number_t i= nbDofsInSideOfSides_+ nbDofsInSides_+ 1;

      for(itr=Kk.begin(); itr!=Kk.end(); ++itr,++i)
      {
        L(i,j)=0.;
        itpt=quad->point();
        itw=quad->weight();
        for(number_t q=0; q<nbq; ++q, ++itw)
        {
          real_t x=*itpt++, y=*itpt++, z=*itpt++;
          L(i,j)+= dot(eval(*itp,x,y,z),eval(*itr,x,y,z))** itw ;
        }
      }
    }
  }

  //compute coefficients of shape functions by solving linear systems
  Matrix<real_t> sf(nbDofs_,_idMatrix);
  Matrix<real_t> Lo=L;
  real_t eps=theZeroThreshold;
  number_t r;
  bool ok=gaussMultipleSolver(L, sf, nbDofs_, eps, r);
  if(!ok)
  {
    where("NedelecEdgeFirstTetrahedronPk::computeShapeFunctions()");
    error("mat_noinvert");
  }
  sf.roundToZero();
  Matrix<real_t>::iterator itm=sf.begin();
  Vector<real_t> v(nbDofs_);
  Vector<real_t>::iterator itv;
  Wk.dimVar=3;
  Wk.dimVec=3;
  Wk.name="NE1_"+tostring(k);
  for(number_t i=0; i<nbDofs_; i++)
  {
    for(itv=v.begin(); itv!=v.end(); ++itv, ++itm) *itv=*itm;
    Wk.push_back(combine(Vk,v));
  }
  dWk.resize(3);
  dWk[0]=dx(Wk);
  dWk[1]=dy(Wk);
  dWk[2]=dz(Wk);
}

/* builds virtual coordinates of moment dofs of order k
     for edge dofs: coordinates of dofs of Pk-1[s] on segment s=[1/k+1, k/k+1] mapped with the same transformation as those used to compute L matrix
     for face dofs: coordinates of dofs of Pk-2[f] on triangle (k/k+1, 1/k+1), (1/k+1, k/k+1), (1/k+1, 1/k+1) mapped with same transformation as those used to compute L matrix (2 at the same place)
     for internal dofs: coordinates of Pk-3 on tetrahedron, 3 at the same place
*/
void NedelecEdgeFirstTetrahedronPk::pointCoordinates()
{
  std::vector<RefDof*>::iterator it_rd=refDofs.begin();
  number_t k= interpolation_p->numtype, kp=k+1;
  std::vector<RefDof*>::iterator itt;

  //edge dofs
  LagrangeStdSegment segPk(findInterpolation(_Lagrange, _standard, k-1, H1));
  real_t s1=1./kp, s2=real_t(k)/kp;
  for(number_t e = 1; e <= 6; ++e)
    for(itt=segPk.refDofs.begin(); itt!=segPk.refDofs.end(); ++itt, ++it_rd)
    {
      real_t x = * (*itt)->coords();  x=(1-x)*s1+x*s2;
      switch(e)
      {
        case 1: (*it_rd)->coords(x,0,0); break;      //on edge (1,4) : x -> (x,0,0)
        case 2: (*it_rd)->coords(0,x,0); break;      //on edge (2,4) : x -> (0,x,0)
        case 3: (*it_rd)->coords(0,0,x); break;      //on edge (3,4) : x -> (0,0,x)
        case 4: (*it_rd)->coords(0,x,1-x); break;    //on edge (2,3) : x -> (0,x,1-x)
        case 5: (*it_rd)->coords(x,0,1-x); break;    //on edge (1,3) : x -> (x,0,1-x)
        default: (*it_rd)->coords(x,1-x,0); break;    //on edge (1,2) : x -> (x,1-x,0)
      }
    }
  if(k<2) return;

  //face dofs
  LagrangeStdTrianglePk triPk(findInterpolation(_Lagrange, _standard, k-2, H1));
  s1=1./kp; s2=real_t(k-2)/kp;
  for(number_t f = 1; f <= 4; ++f)
    for(itt=triPk.refDofs.begin(); itt!=triPk.refDofs.end(); ++itt)
    {
      std::vector<real_t>::const_iterator itp = (*itt)->coords();
      real_t x=*itp; itp++; real_t y=*itp;
      x= s1+x*s2; y= s1+y*s2;
      switch(f)
      {
        case 1: (*it_rd++)->coords(0,1-x-y,y); (*it_rd++)->coords(0,1-x-y,y); break;   //face 1 : x=0 plane
        case 2: (*it_rd++)->coords(y,0,1-x-y); (*it_rd++)->coords(y,0,1-x-y); break;   //face 2 : y=0 plane
        case 3: (*it_rd++)->coords(1-x-y,y,0); (*it_rd++)->coords(1-x-y,y,0.); break;  //face 3 : z=0 plane
        default: (*it_rd++)->coords(x,y,1-x-y); (*it_rd++)->coords(x,y,1-x-y);          //face 4 : x+y+z=1 plane
      }
    }
  if(k<3) return;

  //internal dofs
  for(number_t i=1; i<=k-2; ++i)   //internal dofs (3 located at the same place)
  {
    real_t x = real_t(i)/kp;
    for(number_t j=1; j<=k-1-i; ++j)
    {
      real_t y = real_t(j)/kp;
      for(number_t l=1; l<=k-j-i; ++l)
      {
        real_t z = real_t(l)/kp;
        (*it_rd++)->coords(x, y, z);
        (*it_rd++)->coords(x, y, z);
        (*it_rd++)->coords(x, y, z);
      }
    }
  }
}

//dof's edge numbering
void NedelecEdgeFirstTetrahedronPk::sideOfSideNumbering()
{
  trace_p->push("NedelecEdgeFirstTetrahedronPk::sideOfSideNumbering");
  number_t nbdss=nbDofsInSideOfSides_/6;
  number_t n=1;
  sideOfSideDofNumbers_.resize(6,std::vector<number_t>(nbdss,0));
  for(number_t s = 0; s < 6; ++s)
    for(number_t i=0; i<nbdss; ++i, ++n)    // nbdss = k dofs by edge
      sideOfSideDofNumbers_[s][i] = n;
  trace_p->pop();

}

//dof's face numbering including edge dofs
void NedelecEdgeFirstTetrahedronPk::sideNumbering()
{
  trace_p->push("NedelecEdgeFirstTetrahedronPk::sideNumbering");
  number_t nbds=nbDofsInSides_/4, nbdss=nbDofsInSideOfSides_/6;
  number_t n=nbDofsInSideOfSides_+1;
  sideDofNumbers_.resize(4,std::vector<number_t>(3*nbdss+nbds,0));
  for(number_t s = 0; s < 4; ++s)
    {
        number_t j=0;
        const std::vector<int_t>& ssn=geomRefElem_p->sideOfSideNumbers(s+1); //edge numbers on face s
        for (number_t k=0;k<3;k++) // include edge dofs
        {
            number_t e=std::abs(ssn[k]);         //edge number
            for(number_t d=0;d<nbdss;++d,++j)
                sideDofNumbers_[s][j] = sideOfSideDofNumbers_[e-1][d];
        }
        for(number_t i=0; i<nbds; ++i, ++j, ++n) // include face dofs
            sideDofNumbers_[s][j] = n;
    }
  trace_p->pop();
}

////dof's face numbering not including edge dofs
//void NedelecEdgeFirstTetrahedronPk::sideNumbering()
//{
//  if(interpolation_p->numtype<2) return;  //no face dof
//  trace_p->push("NedelecEdgeFirstTetrahedronPk::sideNumbering");
//  number_t nbds=nbDofsInSides_/4;
//  number_t n=nbDofsInSideOfSides_+1;
//  sideDofNumbers_.resize(4,std::vector<number_t>(nbds,0));
//  for(number_t s = 0; s < 4; ++s)
//    for(number_t i=0; i<nbds; ++i, ++n)    // nbds = k(k-1) dofs by side (face)
//      sideDofNumbers_[s][i] = n;
//  trace_p->pop();
//}


/* tool for global dofs construction to ensure correct matching of dofs on side (see buildFeDofs in FeSpace class)
   return the new index m from a given one n on local side numbers i,j,k
   m corresponds to the index dof when edge vertices are in ascending order
       when i<j<k (no permutation of edge) index is unchanged (m=n)
   reverse 2 dof blocks
*/
number_t NedelecEdgeFirstTetrahedronPk::sideDofsMap(const number_t& n, const number_t& i, const number_t& j, const number_t& k) const
{
  number_t d=interpolation_p->numtype;
  if(d==2 || (i==1 && j==2)) return n;   //one dof per face or no reordering: nothing to do
  //general case: use the LagrangeTetrahedron::sideDofMap
  number_t m=(n+1)/2;
  LagrangeTetrahedron* tetraPk = reinterpret_cast<LagrangeTetrahedron*>(findRefElement(_tetrahedron, findInterpolation(_Lagrange, _standard, d+1, H1)));
  if(tetraPk->barycentricSideDofs.size()==0) tetraPk->buildBarycentricSideDof();
  m = tetraPk->sideDofsMap(m,i,j,k); //map index in Lagrange
  return 2*m-(n%2);
}

/* tool for global dofs construction to ensure correct matching of dofs on side of side (see buildFeDofs in FeSpace class)
   return the new index m from a given one n on local edge numbers i,j
   m corresponds to the index dof when edge vertices are in ascending order
       if i<j (no permutation of edge) index is unchanged (m=n)
       else reverse n
*/
number_t NedelecEdgeFirstTetrahedronPk::sideofsideDofsMap(const number_t& n, const number_t& i, const number_t& j) const
{
  number_t d=interpolation_p->numtype;
  if(d==1 || (i<j)) return n;   //one dof per edge or no reordering: nothing to do
  if(n<=2) return 3-n;          //"external" side dofs
  return d+3-n;                 //"internal" side dofs
}

//compute shape functions using polynomial representation
void NedelecEdgeFirstTetrahedronPk::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  computeShapeValuesFromShapeFunctions(it_pt,shv,withDeriv,with2Deriv);
}

/* rotation of shapevalues
   because face dofs depends on the choice of two tangent vectors on the face, to ensure the correct matching of dofs on a shared face
   the shape values corresponding of a dofs pair have to be rotated according to ascending vertex numbers on face:
     let n1,n2,n3  be the vertex global numbers of face f, i1 the rank of the min , i3 the rank of the max, i2 the middle  (52,30,44)->(2,3,1)
     regarding i1,i2,i3 we apply the following rotation to the pairs of tangent vectors (q1,q2) and shape values (v1,v2) related to the face dofs

   i1=1  i2=2  i3=3  : no rotation
   i1=1  i2=3  i3=2  : q1=q2, q2=q1      and v1=v2, v2=v1
   i1=2  i2=3  i3=1  : q1=q2-q1, q2=-q1  and v1=v2  v2=-v1-v2
   i1=2  i2=1  i3=3  : q1=-q1, q2=q2-q1  and v1=-v1-v2  v2=v2
   i1=3  i2=2  i3=1  : q1=q1-q2, q2=-q2  and v1=v1  v2=-v1-v2
   i1=3  i2=1  i3=2  : q1=-q2, q2=q1-q2  and v1=-v1-v2, v2=v1

   NOTE: if the vertex numbering of tetrahedron and its faces is ascending, there is no rotation

   ns: vertex numbering of tetrahedron
   shv: shape values
   der1 : if true apply rotation also to first derivatives (default value: false)
   der2 : not yet handled

*/
void NedelecEdgeFirstTetrahedronPk::rotateDofs(const std::vector<number_t>&ns, ShapeValues& shv, bool der1, bool der2) const
{
  if(nbDofsInSides_==0) return;   //no dofs in face
  number_t nbds=nbDofsInSides_/4;
  number_t n1,n2,n3,inds;
  real_t t1,t2,t3;  //temporary vector
  for(number_t f = 1; f <= 4; ++f)
  {
    switch(f)
    {
      case 1: n1 = ns[3]; n2=ns[2]; n3=ns[1]; break;                 //face 1 (4,3,2)
      case 2: n1 = ns[3]; n2=ns[0]; n3=ns[2]; break;                 //face 2 (4,1,3)
      case 3: n1 = ns[3]; n2=ns[1]; n3=ns[0]; break;                 //face 3 (4,2,1)
      case 4: n1 = ns[0]; n2=ns[1]; n3=ns[2]; break;                 //face 4 (1,2,3)
      default:
        where("NedelecEdgeFirstTetrahedronPk::rotateDofs");
        error("index_out_of_range","f", 1, 4);
    }
    inds=nbDofsInSideOfSides_+(f-1)*nbds;
    std::vector<real_t>::iterator itw;
    if(n1<n2)
    {
      if(n3<n1)       //i1=3 i2=1 i3=2 -> q1=-q2 q2=q1-q2 and v1=-v1-v2  v2=v1
      {
        for(number_t k=0; k<nbds; k+=2)
        {
          itw=shv.w.begin()+3*(inds+k);
          t1=*itw; t2=*(itw+1); t3=*(itw+2); //store first vector
          *itw=-*itw-*(itw+3); *(itw+1)=-*(itw+1)-*(itw+4); *(itw+2)=-*(itw+2)-*(itw+5);
          *(itw+3)=t1; *(itw+4)=t2; *(itw+5)=t3;
          if(der1)
          {
            for(number_t d=0; d<3; d++)
            {
              itw=shv.dw[d].begin()+3*(inds+k);
              t1=*itw; t2=*(itw+1); t3=*(itw+2); //store first vector
              *itw=-*itw-*(itw+3); *(itw+1)=-*(itw+1)-*(itw+4); *(itw+2)=-*(itw+2)-*(itw+5);
              *(itw+3)=t1; *(itw+4)=t2; *(itw+5)=t3;
            }
          }
        }
      }
      else if(n3<n2)  //i1=1 i2=3 i3=2 -> q1=q2 q2=q1 and v1=v2  v2=v1
      {
        for(number_t k=0; k<nbds; k+=2)
        {
          itw=shv.w.begin()+3*(inds+k);
          t1=*itw; t2=*(itw+1); t3=*(itw+2); //store first vector
          *itw=*(itw+3); *(itw+1)=*(itw+4); *(itw+2)=*(itw+5);
          *(itw+3)=t1; *(itw+4)=t2; *(itw+5)=t3;
          if(der1)
          {
            for(number_t d=0; d<3; d++)
            {
              itw=shv.dw[d].begin()+3*(inds+k);
              t1=*itw; t2=*(itw+1); t3=*(itw+2); //store first vector
              *itw=*(itw+3); *(itw+1)=*(itw+4); *(itw+2)=*(itw+5);
              *(itw+3)=t1; *(itw+4)=t2; *(itw+5)=t3;
            }
          }
        }
      }
    }
    else  //n2 < n1
    {
      if(n1<n3)        //i1=2 i2=1 i3=3 -> q1=-q1 q2=q2-q1 and v1=-v1-v2  v2=v2
      {
        for(number_t k=0; k<nbds; k+=2)
        {
          itw=shv.w.begin()+3*(inds+k);
          *itw=-*itw-*(itw+3); *(itw+1)=-*(itw+1)-*(itw+4); *(itw+2)=-*(itw+2)-*(itw+5);
          if(der1)
          {
            for(number_t d=0; d<3; d++)
            {
              itw=shv.dw[d].begin()+3*(inds+k);
              *itw=-*itw-*(itw+3); *(itw+1)=-*(itw+1)-*(itw+4); *(itw+2)=-*(itw+2)-*(itw+5);
            }
          }
        }
      }
      else if(n2<n3)   //i1=2 i2=3 i3=1 -> q1=q2-q1 q2=-q1 and v1=v2  v2=-v1-v2
      {
        for(number_t k=0; k<nbds; k+=2)
        {
          itw=shv.w.begin()+3*(inds+k);
          t1=*itw; t2=*(itw+1); t3=*(itw+2); //store first vector
          *itw=*(itw+3); *(itw+1)=*(itw+4); *(itw+2)=*(itw+5);
          *(itw+3)=-t1-*(itw+3); *(itw+4)=-t2-*(itw+4); *(itw+5)=-t3-*(itw+5);
          if(der1)
          {
            for(number_t d=0; d<3; d++)
            {
              itw=shv.dw[d].begin()+3*(inds+k);
              t1=*itw; t2=*(itw+1); t3=*(itw+2); //store first vector
              *itw=*(itw+3); *(itw+1)=*(itw+4); *(itw+2)=*(itw+5);
              *(itw+3)=-t1-*(itw+3); *(itw+4)=-t2-*(itw+4); *(itw+5)=-t3-*(itw+5);
            }
          }
        }
      }
      else            //i1=3 i2=2 i3=1 -> q1=q1-q2 q2=-q2 and v1=v1  v2=-v1-v2
      {
        for(number_t k=0; k<nbds; k+=2)
        {
          itw=shv.w.begin()+3*(inds+k);
          *(itw+3)=-*itw-*(itw+3) ; *(itw+4)=-*(itw+1)-*(itw+4); *(itw+5)=-*(itw+2)-*(itw+5);
          if(der1)
          {
            for(number_t d=0; d<3; d++)
            {
              itw=shv.dw[d].begin()+3*(inds+k);
              *(itw+3)=-*itw-*(itw+3) ; *(itw+4)=-*(itw+1)-*(itw+4); *(itw+5)=-*(itw+2)-*(itw+5);
            }
          }
        }
      }
    }
  }
}

//=====================================================================================
/*! Nedelec edge second family of order k on tetrahedron (NF2k)
*/
//=====================================================================================
NedelecEdgeSecondTetrahedronPk::NedelecEdgeSecondTetrahedronPk(const Interpolation* interp_p)
  : NedelecEdgeTetrahedron(interp_p)
{
  name_ += "_second family_"+tostring(interp_p->numtype);
  error("not_yet_implemented","Nedelec Edge Second Family on tetrahedron");
//   interpolationData();  // build element interpolation data
//   sideNumbering();      // local numbering of points on edges
//   pointCoordinates();   // "virtual coordinates" of moment dofs
}

NedelecEdgeSecondTetrahedronPk::~NedelecEdgeSecondTetrahedronPk() {}

} // end of namespace xlifepp
