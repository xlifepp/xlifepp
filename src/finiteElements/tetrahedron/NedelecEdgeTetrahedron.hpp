/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file NedelecEdgeTetrahedron.hpp
  \authors E. Lunéville
  \since 15 september 2015
  \date  15 september 2015

  \brief Definition of the xlifepp::NedelecEdgeTetrahedron class

  xlifepp::NedelecEdgeTetrahedron defines Nedelec HCurl-conforming edge elements interpolation data on tetrahedron elements
  There are 2 families:
  - the first family (NE1k) is the extension to tetrahedron of the Nedelec first family element  defined on a triangle (N1k)
  - the second family (NE2k) is the extension to tetrahedron of the Nedelec second family element defined on a triangle (N2k)

   member functions
  ----------------
    - interp: interpolation data
    - sideNumbering: local numbering on edges (side)
    - sideDofsMap: side dofs mapping used when assembling
    - computeShapeFunctions: compute shape functions as polynomials (only for any order family)
    - computeShapeValues: evaluate shape functions at a point

  Child classes:
    - xlifepp::NedelecEdgeFirstTetrahedronPk -> Nedelec first family of any order on tetrahedron (NE1k)
    - xlifepp::NedelecEdgeSecondTetrahedronPk -> Nedelec second family of any order on tetrahedron (NE2k)
*/

#ifndef NEDELEC_EDGE_TETRAHEDRON_HPP
#define NEDELEC_EDGE_TETRAHEDRON_HPP

#include "config.h"
#include "RefTetrahedron.hpp"

namespace xlifepp
{

/*!
  \class NedelecEdgeTetrahedron
  (Hcurl-conforming edge elements )

  Parent class  : RefTriangle
  Child classes: NedelecEdgeFirstTetrahedronPk, NedelecEdgeSecondTetrahedronPk
 */
class NedelecEdgeTetrahedron: public RefTetrahedron
{
  public:
    NedelecEdgeTetrahedron(const Interpolation* interp_p);
    virtual ~NedelecEdgeTetrahedron();
}; // end of class NedelecEdgeTetrahedron

/*!
  \class NedelecEdgeFirstTetrahedronPk
  Nedelec edge first family of any order k on tetrahedron t (NE1k)
     space  Vk     : P^3_(k-1) + Sk   with Sk={p in Phk^3, x.p=0}, Phk homogeneous polynomials of degree k, k(k+2)(k+3)/2 dofs
     edge dofs             : v-> int_e v.t q,  q in  P_(k-1)[e]               k dofs by edge e
     face dofs (k>1)       : v-> int_f v.q,    q in {P_(k-2)[f]^3, q.n=0}     k(k-1) dofs by face
     tetrahedron dofs (k>2): v-> int_t v.q,    q in  P_(k-3)[t]^3             k(k-1)(k-2)/2 dofs
 */
class NedelecEdgeFirstTetrahedronPk: public NedelecEdgeTetrahedron
{
  public:
    NedelecEdgeFirstTetrahedronPk(const Interpolation* int_p);
    ~NedelecEdgeFirstTetrahedronPk();
    void interpolationData();  //!< defines reference element interpolation data
    void sideNumbering();      //!< local numbering on faces (side)
    void sideOfSideNumbering();//!< local numbering on edges (side of side)
    void pointCoordinates();   //!< builds virtual coordinates of moment dofs
    number_t sideDofsMap(const number_t& n, const number_t& i,
                         const number_t& j, const number_t& k=0) const;  //!< face dofs mapping
    number_t sideofsideDofsMap(const number_t& n, const number_t& i, const number_t& j) const; //!< edge dof mapping
    void computeShapeFunctions();                                        //!< compute shape functions as polynomials

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool der1 = true, const bool der2 =false) const;          //!< compute shape values at a point
    void rotateDofs(const std::vector<number_t>&, ShapeValues& shv, bool der1=false, bool der2=false) const; //!< rotation of face dofs to ensure matching of dofs on shared face
    virtual Value evalFaceDof(const FeDof& dof, const GeomElement& selt, const Function& f, const Function& gradf, const Function& grad2f) const; //!< specific eval face dof function
  private:
    number_t nbshfcts_;
    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class NedelecEdgeFirstTetrahedronPk


/*!
  \class NedelecEdgeSecondTetrahedronPk
  Nedelec edge second family of any order k on tetrahedron t (NE2k)
     space  Vk    P^3_(k)   (k+1)(k+2)(k+3)/2 dofs
     edge dofs             : v-> int_e v.t q, q in P_(k)[e]                                   k+1  dofs by edge e
     face dofs (k>1)       : v-> int_f v.q,   q in D_(k-1)[f] = P_(k-2)^2 + PH_(k-2)*x        (k-1)(k+1) dofs  by face
     tetrahedron dofs (k>3): v-> int_t v.q,   q in D_(k-2)[t] = P_(k-3)^3 + PH_(k-3)*x        (k-1)(k+1)(k-2)/2 dofs
     ####" NOT IMPLEMENTED ####
 */
class NedelecEdgeSecondTetrahedronPk: public NedelecEdgeTetrahedron
{
  public:
    NedelecEdgeSecondTetrahedronPk(const Interpolation* int_p);
    ~NedelecEdgeSecondTetrahedronPk();
    void interpolationData(){};  //!< defines reference element interpolation data
    void sideNumbering(){};      //!< local numbering on edges (side)
    void pointCoordinates(){};   //!< builds virtual coordinates of moment dofs
//    number_t sideDofsMap(const number_t& n, const number_t& i,
//                         const number_t& j, const number_t& k=0) const;  //!< side dofs mapping
    void computeShapeFunctions(){};                                        //!< compute shape functions as polynomials
    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
         const bool withDeriv = true,const bool with2Deriv=false) const{};          //!< compute shape values at a point
  private:
    number_t nbshfcts_;
    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class NedelecEdgeSecondTetrahedronPk


} // end of namespace xlifepp

#endif /* NEDELEC_EDGE_TETRAHEDRON_HPP */
