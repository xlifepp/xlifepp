/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file RefTetrahedron.cpp
	\authors D. Martin, N. Kielbasiewicz
	\since 16 dec 2002
	\date 6 aug 2012

  \brief Implementation of xlifepp::RefTetrahedron class members and related functions
 */

#include "RefTetrahedron.hpp"
#include "LagrangeTetrahedron.hpp"
#include "CrouzeixRaviartTetrahedron.hpp"
#include "NedelecFaceTetrahedron.hpp"
#include "NedelecEdgeTetrahedron.hpp"
#include "../Interpolation.hpp"
#include "utils.h"

namespace xlifepp
{

//! tetrahedronConstructor construction of a Reference Element by interpolation type and interpolation subtype
RefElement* selectRefTetrahedron(const Interpolation* interp_p)
{
  switch (interp_p->type)
  {
    case _Lagrange:
      switch (interp_p->subtype)
      {
        case _standard: return tetrahedronLagrangeStd(interp_p); break;
        default:        interp_p->badSubType(_tetrahedron); break;
      }
      break;
    case _CrouzeixRaviart:
      switch (interp_p->subtype)
      {
        case _standard: return tetrahedronCrouzeixRaviartStd(interp_p); break;
        default: interp_p->badSubType(_tetrahedron); break;
      }
      break;
      case _Nedelec:
          if(interp_p->conformSpace==_Hdiv) return tetrahedronNedelecFace(interp_p);
          if(interp_p->conformSpace==_Hrot) return tetrahedronNedelecEdge(interp_p);
      case _NedelecFace: return tetrahedronNedelecFace(interp_p);
      case _NedelecEdge: return tetrahedronNedelecEdge(interp_p);
    default: break;
  }

  // Throw error messages
  trace_p->push("selectRefTetrahedron");
  interp_p->badType(_tetrahedron);
  trace_p->pop();
  return nullptr;
}

/*! tetrahedronLagrangeStd construction of a Lagrange standard Reference Element by interpolation number
    particular formulae up to P3 else general method using monomes basis
*/
RefElement* tetrahedronLagrangeStd(const Interpolation* interp_p)
{
  switch (interp_p->numtype)
  {
    case _P0: return new LagrangeStdTetrahedron<_P0>(interp_p); break;
    case _P1: return new LagrangeStdTetrahedron<_P1>(interp_p); break;
    case _P2: return new LagrangeStdTetrahedron<_P2>(interp_p); break;
    case _P3: return new LagrangeStdTetrahedron<_P3>(interp_p); break;
    default:
      return new LagrangeStdTetrahedronPk(interp_p); break;
      break;
  }
  return nullptr;
}

//! tetrahedronCrouzeixRaviartStd construction of a CrouzeixRaviart Reference Element by interpolation number
RefElement* tetrahedronCrouzeixRaviartStd(const Interpolation* interp_p)
{
  switch (interp_p->numtype)
  {
    case 1: return new CrouzeixRaviartStdTetrahedronP1(interp_p); break;
    default:
      trace_p->push("CrouzeixRaviartStdTetrahedronP1");
      interp_p->badDegree(_tetrahedron);
      trace_p->pop();
      break;
  }
  return nullptr;
}

RefElement* tetrahedronNedelecFace(const Interpolation* interp_p)
{
    switch (interp_p->subtype)
  {
    case _firstFamily: return new NedelecFaceFirstTetrahedronPk(interp_p);
    // case _secondFamily: return new NedelecFaceSecondTetrahedronPk(interp_p);
    default:
      trace_p->push("tetrahedronNedelecFace");
      interp_p->badDegree(_tetrahedron);
      trace_p->pop();
      break;
  }
  return nullptr;
}

RefElement* tetrahedronNedelecEdge(const Interpolation* interp_p)
{
    switch (interp_p->subtype)
  {
    case _firstFamily: return new NedelecEdgeFirstTetrahedronPk(interp_p);
    //case _secondFamily: return new NedelecEdgeSecondTetrahedronPk(interp_p);
    default:
      trace_p->push("tetrahedronNedelecEdge");
      interp_p->badDegree(_tetrahedron);
      trace_p->pop();
      break;
  }
  return nullptr;
}

} // end of namespace xlifepp
