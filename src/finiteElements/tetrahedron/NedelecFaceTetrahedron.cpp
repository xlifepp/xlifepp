/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file NedelecFaceTetrahedron.cpp
  \authors E. Lunéville
  \since 15 september 2015
  \date  15 september 2015

  \brief Implementation of xlifepp::NedelecFaceTetrahedron class members and related functions
 */

#include "NedelecFaceTetrahedron.hpp"
#include "../Interpolation.hpp"
#include "../integration/Quadrature.hpp"
#include "../triangle/LagrangeTriangle.hpp"
#include "LagrangeTetrahedron.hpp"
#include "utils.h"
#include <iostream>

namespace xlifepp
{

//! NedelecFaceTetrahedron constructor
NedelecFaceTetrahedron::NedelecFaceTetrahedron(const Interpolation* interp_p)
  : RefTetrahedron(interp_p)
{
  name_ += "_NedelecFace_";
  mapType = _contravariantPiolaMap;
  dofCompatibility=_signDofCompatibility;
  dimShapeFunction=3;
}

NedelecFaceTetrahedron::~NedelecFaceTetrahedron() {}

//=====================================================================================
/*! Nedelec face first family of order k on tetrahedron (NF1k)
*/
//=====================================================================================
NedelecFaceFirstTetrahedronPk::NedelecFaceFirstTetrahedronPk(const Interpolation* interp_p)
  : NedelecFaceTetrahedron(interp_p)
{
  name_ += "first family_"+tostring(interp_p->numtype);
  interpolationData();  // build element interpolation data
  sideNumbering();      // local numbering of points on edges
  pointCoordinates();   // "virtual coordinates" of moment dofs
}

NedelecFaceFirstTetrahedronPk::~NedelecFaceFirstTetrahedronPk() {}

//! interp defines Reference Element interpolation data
void NedelecFaceFirstTetrahedronPk::interpolationData()
{
  trace_p->push("NedelecFaceFirstTetrahedronPk::interpolationData");

  number_t k = interpolation_p->numtype;
  nbDofs_= k*(k+1)*(k+3)/2;
  nbDofsInSides_=2*k*(k+1);
  nbInternalDofs_=k*(k-1)*(k+1)/2;

  /* Creating Degrees Of Freedom of reference element
     in Nedelec first family order k elements, D.o.Fs are defined as moment D.o.Fs
        face dofs: v-> int_f v.n q,  q in P_(k-1)[f]      k(k+1)/2 dofs by face f
        internal dofs: v-> int_t v.q,    q in P_(k-2)[t]^3    k(k-1)(k+1)/2 dofs  only for k > 1
     numbering of D.oFs are given by the basis functions order of dual spaces defining D.o.Fs beginning by faces

     NOTE: moment D.o.Fs can be reinterpreted as punctual D.o.Fs using a well adapted quadrature formulae
            we do not use this representation here
  */
  refDofs.reserve(nbDofs_);
  number_t nbds=nbDofsInSides_/4;
  for(number_t f = 1; f <= 4; ++f)              //face dofs (rank in dual polynoms basis is used to identify dof)
    for(number_t i=0; i<nbds; ++i)
      refDofs.push_back(new RefDof(this, true, _onFace, f, i+1, 3, 2, 0, 3, 0, _dotnProjection,_ndot, "int_f u.n q"));
  for(number_t i=0; i<nbInternalDofs_; ++i) //element dofs (order = i is used to identify dof)
    refDofs.push_back(new RefDof(this, false, _onElement, 0, i+1, 3, 3, 0, 3, 0, _noProjection,_id, "int_k u.q"));

  // compute shape functions as formal polynomials
  computeShapeFunctions();
  buildPolynomialTree();   //construct tree representation
  maxDegree = Wk.degree();

  // compute number of shape functions
  nbshfcts_ = shapeValueSize();

  trace_p->pop();
}

//! compute shape functions as polynomials using general algorithm
/*  mapping from ref. triangle ((1,0,0),(0,1,0), (0,0,0)) onto tetrahedron face is chosen as follows on face (i,j,k)
                     (x,y) -> x*Vi + y*Vj +(1-x-y)*Vk
    gives on face 1 (4,3,2) : (0,1-x-y,y)
          on face 2 (4,1,3) : (y,0,1-x-y)
          on face 3 (4,2,1) : (1-x-y,y,0)
          on face 4 (1,2,3) : (x,y,1-x-y)
*/
void NedelecFaceFirstTetrahedronPk::computeShapeFunctions()
{
  number_t k = interpolation_p->numtype;
  PolynomialsBasis::iterator itp;

  //compute Matrix Lij=l(pi,qj) for all pi in Vk, for all qj in P_(k-1)[f], face D.o.Fs:  v-> int_f v.n q, q in P_(k-1)[f]
  PolynomialsBasis Vk(_Dk,3,k);       //Nedelec first family polynomials space (Dk)
  LagrangeStdTrianglePk triPk(findInterpolation(_Lagrange, _standard, k-1, H1));
  PolynomialsBasis& Q=triPk.Wk;       //create dual space for face dofs: P_(k-1)[x,y]
  PolynomialsBasis::iterator itq;
  dimen_t d=Vk.degree()+Q.degree();
  Quadrature* quad=findBestQuadrature(_triangle, d);  //quadrature rule on triangle of order d (integrates exactly x^d)
  number_t nbq= quad->numberOfPoints();
  Matrix<real_t> L(nbDofs_,nbDofs_);
  number_t j=1;
  real_t sq3=std::sqrt(3)/3.;
  std::vector<real_t>::const_iterator itpt, itw; //for quadrature rule
  for(itp=Vk.begin(); itp!=Vk.end(); ++itp, ++j)
  {
    number_t i=1;
    for(number_t f = 1; f <= 4; ++f)
    {
      Vector<real_t> n(3,0.);   //unit outward normal vector
      switch(f)
      {
        case 1: n[0]=-1.; break;           //face 1 : x=0 plane
        case 2: n[1]=-1.; break;           //face 2 : y=0 plane
        case 3: n[2]=-1.; break;           //face 3 : z=0 plane
        default: n[0]=1.; n[1]=1.; n[2]=1.; n*=sq3; //face 4 : x+y+z=1 plane
      }
      Polynomial pn=dot(*itp,n);
      for(itq=Q.begin(); itq!=Q.end(); ++itq, ++i)
      {
        L(i,j)=0;
        itpt=quad->point();
        itw=quad->weight();
        Polynomial& pq=(*itq)[0];
        for(number_t q=0; q<nbq; ++q, ++itw)   //compute by 2D quadrature
        {
          real_t x=*itpt++, y=*itpt++;     //quadrature point
          switch(f)
          {
            case 1: L(i,j)+= pn(0,1-x-y,y) * pq(x,y)** itw ; break;          //face 1 : x=0 plane, transformation (3,1,2) -> (4,3,2)
            case 2: L(i,j)+= pn(y,0,1-x-y) * pq(x,y)** itw ; break;          //face 2 : y=0 plane, transformation (3,1,2) -> (4,1,3)
            case 3: L(i,j)+= pn(1-x-y,y,0) * pq(x,y)** itw ; break;          //face 3 : z=0 plane, transformation (3,1,2) -> (4,2,1)
            default: L(i,j)+= pn(x,y,1-x-y) * pq(x,y)** itw * 3 * sq3; break; //face 4 : x+y+z=1 plane, transformation (3,1,2) -> (1,2,3)
          }
        }
      }
    }
  }
  if(k>1)   //compute Matrix Lij=l(pi,qj) for all pi in Vk, qj in P_(k-2)[t]^3, element  D.o.Fs:  v-> int_t v.q,  q in P_(k-2)[t]^3
  {
    Kk=PolynomialsBasis(PolynomialBasis(_Pk,3,k-2),3); //P_(k-2)[t]^3
    PolynomialsBasis::iterator itr;
    dimen_t d=Vk.degree()+Kk.degree();
    quad=findBestQuadrature(_tetrahedron,d);  //quadrature rule on tetrahedron of order d
    nbq= quad->numberOfPoints();
    j=1;
    for(itp=Vk.begin(); itp!=Vk.end(); ++itp,++j)
    {
      number_t i=nbDofsInSides_+1;

      for(itr=Kk.begin(); itr!=Kk.end(); ++itr,++i)
      {
        L(i,j)=0.;
        itpt=quad->point();
        itw=quad->weight();
        for(number_t q=0; q<nbq; ++q, ++itw)
        {
          real_t x=*itpt++, y=*itpt++, z=*itpt++;
          L(i,j)+= dot(eval(*itp,x,y,z),eval(*itr,x,y,z))** itw ;
        }
      }
    }
  }

  //compute coefficients of shape functions by solving linear systems
  Matrix<real_t> sf(nbDofs_,_idMatrix);
  Matrix<real_t> Lo=L;
  real_t eps=theZeroThreshold;
  number_t r;
  bool ok=gaussMultipleSolver(L, sf, nbDofs_, eps, r);
  if(!ok)
  {
    where("NedelecFaceFirstTetrahedronPk::computeShapeFunctions()");
    error("mat_noinvert");
  }
  sf.roundToZero();
  Matrix<real_t>::iterator itm=sf.begin();
  Vector<real_t> v(nbDofs_);
  Vector<real_t>::iterator itv;
  Wk.dimVar=3;
  Wk.dimVec=3;
  Wk.name="NF1_"+tostring(k);
  for(number_t i=0; i<nbDofs_; i++)
  {
    for(itv=v.begin(); itv!=v.end(); ++itv, ++itm) *itv=*itm;
    Wk.push_back(combine(Vk,v));
  }
  //Wk.clean(0.000001);
  dWk.resize(3);
  dWk[0]=dx(Wk);
  dWk[1]=dy(Wk);
  dWk[2]=dz(Wk);
}

/* builds virtual coordinates of moment dofs of order k
     for face dofs: coordinates of dofs of Pk-1[f] on triangle Tk2 : (1/k+2,1/k+2),(1/k+2,k/k+2),(k/k+2,k/k+2) mapped to each face f (same transformation using to compute L matrix)
     for internal dofs: coordinates of Pk-2 on tetrahedron (k-1/k+1, 1/k+1, 1/k+1), ( 1/k+1, k-1/k+1, 1/k+1), (1/k+1, 1/k+1, k-1/k+1),(1/k+1, 1/k+1, 1/k+1)
                         3 internal dofs located at the same place
*/
void NedelecFaceFirstTetrahedronPk::pointCoordinates()
{
  std::vector<RefDof*>::iterator it_rd=refDofs.begin();
  number_t k= interpolation_p->numtype, kp=k+2;
  real_t s1=1./kp, s2=real_t((k-1))/kp;
  LagrangeStdTrianglePk triPk(findInterpolation(_Lagrange, _standard, k-1, H1));
  std::vector<RefDof*>::iterator itt;
  for(number_t f = 1; f <= 4; ++f)              //face dofs
    for(itt=triPk.refDofs.begin(); itt!=triPk.refDofs.end(); ++itt, ++it_rd)
    {
      std::vector<real_t>::const_iterator itp = (*itt)->coords();
      real_t x=*itp; itp++;
      real_t y=*itp;
      if(k>1) {x= s1+(x*s2); y= s1+(y*s2);}  //mapped onto Tk2 (except in P0)
      switch(f)
      {
        case 1: (*it_rd)->coords(0,1-x-y,y); break;   //face 1 : x=0 plane
        case 2: (*it_rd)->coords(y,0,1-x-y); break;   //face 2 : y=0 plane
        case 3: (*it_rd)->coords(1-x-y,y,0.); break;  //face 3 : z=0 plane
        default: (*it_rd)->coords(x,y,1-x-y);          //face 4 : x+y+z=1 plane
      }
    }

  for(number_t i=1; i<=k-1; ++i)   //internal dofs (3 located at the same place)
  {
    real_t x = real_t(i)/kp;
    for(number_t j=1; j<=k-i; ++j)
    {
      real_t y = real_t(j)/kp;
      for(number_t l=1; l<=k-j-i+1; ++l)
      {
        real_t z = real_t(l)/kp;
        (*it_rd++)->coords(x, y, z);
        (*it_rd++)->coords(x, y, z);
        (*it_rd++)->coords(x, y, z);
      }
    }
  }
}

//dof's side numbering
void NedelecFaceFirstTetrahedronPk::sideNumbering()
{
  trace_p->push("NedelecFaceFirstTetrahedronPk::sideNumbering");
  number_t nbds=nbDofsInSides_/4;
  number_t n=1;
  sideDofNumbers_.resize(4,std::vector<number_t>(nbds,0));
  for(number_t s = 0; s < 4; ++s)
    for(number_t i=0; i<nbds; ++i, ++n)    // nbds = k(k+1)/2 dofs by side (face)
      sideDofNumbers_[s][i] = n;
  trace_p->pop();
}

/* tool for global dofs construction to ensure correct matching of dofs on side (see buildFeDofs in FeSpace class)
   return the new index m from a given one n on local side numbers i,j,k
   m corresponds to the index dof when face vertices are in ascending order
       when i<j<k (no permutation of face) index is unchanged (m=n)
*/
number_t NedelecFaceFirstTetrahedronPk::sideDofsMap(const number_t& n, const number_t& i, const number_t& j, const number_t& k) const
{
  number_t d=interpolation_p->numtype;
  if(d==1 || (i==1 && j==2 && k==3)) return n;   //one dof per face or no reordering: nothing to do
  if(d==2 || n<=3)     //order 2 or "vertices"
  {
    if(n==i) return 1;
    if(n==j) return 2;
    return 3;
  }
  //general case: use the LagrangeTetrahedron::sideDofMap
  LagrangeTetrahedron* tetraPk = reinterpret_cast<LagrangeTetrahedron*>(findRefElement(_tetrahedron, findInterpolation(_Lagrange, _standard, d+2, H1)));
  if(tetraPk->barycentricSideDofs.size()==0) tetraPk->buildBarycentricSideDof();
  return tetraPk->sideDofsMap(n,i,j,k);

//  if(d==3)     //order 3 : "middle of vertices"
//    {
//      if(i==1)  //case 132
//        switch(n)
//          {
//            case 4: return 6;
//            case 5: return 5;
//            default: return 4;
//          }
//      if(i==2) //case 2xx
//        {
//          if(j==1) //case 213
//            switch(n)
//              {
//                case 4: return 4;
//                case 5: return 6;
//                default: return 5;
//              }
//              else //case 231
//                switch(n)
//              {
//                case 4: return 6;
//                case 5: return 4;
//                default: return 5;
//              }
//        }
//        //case 3xx
//         if(j==1) //case 312
//            switch(n)
//              {
//                case 4: return 5;
//                case 5: return 6;
//                default: return 4;
//              }
//              else //case 321
//                switch(n)
//              {
//                case 4: return 5;
//                case 5: return 4;
//                default: return 6;
//              }
//
//    }
}

//compute shape functions using polynomial representation
void NedelecFaceFirstTetrahedronPk::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  computeShapeValuesFromShapeFunctions(it_pt,shv,withDeriv,with2Deriv);
}

//=====================================================================================
/*! Nedelec face second family of order k on tetrahedron (NF2k)
*/
//=====================================================================================
NedelecFaceSecondTetrahedronPk::NedelecFaceSecondTetrahedronPk(const Interpolation* interp_p)
  : NedelecFaceTetrahedron(interp_p)
{
  name_ += "_second family_"+tostring(interp_p->numtype);
  error("not_yet_implemented","NedelecFaceSecondTetrahedronPk::NedelecFaceSecondTetrahedronPk()");
//   interpolationData();  // build element interpolation data
//   sideNumbering();      // local numbering of points on edges
//   pointCoordinates();   // "virtual coordinates" of moment dofs
}

NedelecFaceSecondTetrahedronPk::~NedelecFaceSecondTetrahedronPk() {}

} // end of namespace xlifepp
