/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file GeomRefTetrahedron.hpp
	\authors D. Martin, N. Kielbasiewicz
	\since 21 dec 2002
	\date 6 aug 2012

  \brief Definition of the xlifepp::GeomRefTetrahedron class

  xlifepp::GeomRefTetrahedorn defines Reference Element geometric data on tetrahedral elements

	Numbering of vertices, edges and faces
	\verbatim
														 Vertex numbers on edges | Vertex numbers on faces
														 Edge:   v1 v2 equation  | Face:  v1  v2  v3  equation
						3                                   1:   1  4  y=0,z=0   |    1:  4   3   2   x=0
					 /| \                                   2:   2  4  x=0,z=0   |    2:  4   1   3   y=0
					/ |   \                                 3:   3  4  x=0,y=0   |    3:  4   2   1   z=0
				 / III    V                                 4:   2  3  x=0,y+z=1 |    4:  1   2   3   x+y+z=1
				/   |       \                               5:   3  1  y=0,x+z=1 | Edge numbers on faces
			 /    |         \                               6:   1  2  z=0,x+z=1 | Face:  e1  e2  e3
			IV    4----II----2                                                   |    1:  3   4   2
		 /    /         /                                                        |    2:  3   5   1
		/   /       /                                                            |    3:  1   6   2
	 /  I     VI                                                                 |    4:  6   4   5
	/ /	   /
 //  /
 1
  \endverbatim

- sideOfSideNumbering local numbering of vertices on edges
- sideNumbering local numbering of vertices on faces and of edges on faces

Just a reminder
  -    Melina ->  ++     Melina ->  ++
  -  Face   1      3   Node   1      1
  -  Face   2      4   Node   2      2
  -  Face   3      1   Node   3      4
  -  Face   4      2   Node   4      3
  -                    Node   5     10
  -  Edge   1      6   Node   6      6
  -  Edge   2      2   Node   7      5
  -  Edge   3      1   Node   8      9
  -  Edge   4      5   Node   9      8
  -  Edge   5      4   Node   10     7
  -  Edge   6      3
*/

#ifndef GEOM_REF_TETRAHEDRON_HPP
#define GEOM_REF_TETRAHEDRON_HPP

#include "config.h"
#include "../GeomRefElement.hpp"

namespace xlifepp
{

/*!
	\class GeomRefTetrahedron
 */
class GeomRefTetrahedron : public GeomRefElement
{
  public:
    //! default constructor
    GeomRefTetrahedron();
    //! destructor
    ~GeomRefTetrahedron() {}

    real_t measure(const dimen_t, const number_t sideNum = 0) const; //!< returns edge length or face area

    // /*! returns a quadrature rule built on edge sideNum (=1,2,..,6) from a 1d quadrature formula
    //  or a quadrature rule built on face (=1,2,3,4) from a triangle quadrature formula
    // */
    // void sideQuadrature(const QuadratureRule&, QuadratureRule&, const number_t sideNum, const dimen_t = 2) const;

    //! returns tangent std::vector on an edge or a face
    void tangentVector(const std::vector<real_t>& jacobianMatrix, std::vector< std::vector<real_t> >& tgv, const number_t, const dimen_t) const;

    number_t vertexOppositeSide(const number_t) const; //!< returns vertex opposite to face number sideNum (=1,2,3,4)
    number_t sideOppositeVertex(const number_t) const; //!< local number of a face opposed to a vertex given by local number v_no
    number_t edgeOppositeEdge(const number_t) const; //!< local number of an edge opposed to a edge given by local number
    number_t sideWithVertices(const number_t, const number_t) const; //!< returns local number of edge bearing vertices with local numbers v1 and v2
    number_t sideWithVertices(const number_t, const number_t, const number_t, const number_t = 0) const; //!< returns local number of face bearing vertices with local numbers v1, v2, v3
    //! node numbers defining first simplex of ref element
    std::vector<number_t> simplexNodes() const
    {std::vector<number_t> sn(4,0); sn[0]=4;sn[1]=1;sn[2]=2;sn[3]=3; return sn;}

    bool contains(std::vector<real_t>& p, real_t tol= theTolerance) const;  //!< test if a point belongs to current element
    std::vector<real_t> projection(const std::vector<real_t>& p, real_t& h) const;    //!< return projection on ref tetahedron

  private:
    void sideOfSideNumbering(); //!< local numbers of vertices on edges for tetrahedra
    void sideNumbering(); //!< local numbers of vertices and edges on faces for tetrahedra

}; // end of class GeomRefTetrahedron

} // end of namespace xlifepp

#endif /* GEOM_REF_TETRAHEDRON_HPP */
