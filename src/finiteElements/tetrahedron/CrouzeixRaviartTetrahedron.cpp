/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file CrouzeixRaviartTetrahedron.cpp
	\authors D. Martin,N. Kielbasiewicz
	\since 23 nov 2004
	\date 6 aug 2012

  \brief Implementation of xlifepp::CrouzeixRaviartTetrahedron class members and related functions
 */

#include "CrouzeixRaviartTetrahedron.hpp"
#include "../Interpolation.hpp"
#include "utils.h"

#include <iostream>

namespace xlifepp
{

//! CrouzeixRaviartTetrahedron constructor for CrouzeixRaviart reference elements
CrouzeixRaviartTetrahedron::CrouzeixRaviartTetrahedron(const Interpolation* interp_p)
  : RefTetrahedron(interp_p)
{
  name_ += "_Crouzeix-Raviart";
  trace_p->push("CrouzeixRaviartTetrahedron::CrouzeixRaviartTetrahedron (" + name_ + ")");

  // build element interpolation data
  interpolationData();
  // local numbering of points on edges
  sideNumbering();
  // *NO* ReferenceElement on Faces nor edges
  maxDegree = 1;
  trace_p->pop();
}

CrouzeixRaviartTetrahedron::~CrouzeixRaviartTetrahedron() {}

//! interp defines Reference Element interpolation data
void CrouzeixRaviartTetrahedron::interpolationData()
{
  trace_p->push("CrouzeixRaviartTetrahedron::interpolationData");

  number_t i_n = interpolation_p->numtype;

  switch ( i_n )
  {
    case _P1:
      nbDofsInSideOfSides_ = nbDofs_ = 4;
      break;
    default:
        error("interp_order_unexpected", 1, i_n);
      break;
  }

  // Creating Degrees Of Freedom of reference element
  // in CrouzeixRaviart standard elements all D.o.F on edges are sharable
  // number of internal nodes (or D.o.F)
  refDofs.reserve(nbDofs_);
  for(number_t s = 1; s <= 4; s++ )
       refDofs.push_back(new RefDof(this, true, _onFace, s, 1, 3, 0, s, 1, 0, _noProjection,_id,"nodal value"));
  //lagrangeRefDofs(1, 0, nbDofs_, 3);

  // compute number of shape functions
  nbshfcts_ = shapeValueSize();

  nbPts_ = 4;
  trace_p->pop();
}

//! sideNumbering defines Reference Element local numbers on edges
void CrouzeixRaviartTetrahedron::sideNumbering()
{
  trace_p->push("CrouzeixRaviartTetrahedron::sideNumbering");
  /*
  	Local numbering of CrouzeixRaviart tetrahedrons

  	.... k=1
   */
  number_t i_n = interpolation_p->numtype;
//  number_t nbSides = geomRefElem_p->nbSides();

  if ( i_n == 1 )
  {
//    number_t dof_Ed(1);
    //		for ( number_t fa = 0; fa < nbSides; fa++ ) {
    //			side_interpolations_.push_back(new ReferenceSideInterpolation(interpolation_p, dof_Ed, dof_Ed));
    //			side_interpolations_[fa]->type(NoInterp);
    //			side_interpolations_[fa]->DOF_numbers[0] = fa+1;
    //		}
  }
  trace_p->pop();
}

//! CrouzeixRaviart standard P1 tetrahedron Reference Element
CrouzeixRaviartStdTetrahedronP1::CrouzeixRaviartStdTetrahedronP1(const Interpolation* interp_p)
  : CrouzeixRaviartTetrahedron(interp_p)
{
  name_ += "_1";
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
}

CrouzeixRaviartStdTetrahedronP1::~CrouzeixRaviartStdTetrahedronP1() {}

//! pointCoordinates defines CrouzeixRaviart Reference Element point coordinates
void CrouzeixRaviartStdTetrahedronP1::pointCoordinates()
{
  trace_p->push("CrouzeixRaviartStdTetrahedronP1::pointCoordinates");
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();
  (*it_rd++)->coords(0, over3_, over3_);
  (*it_rd++)->coords(over3_, 0., over3_);
  (*it_rd++)->coords(0., over3_, over3_);
  (*it_rd)->coords(over3_, over3_, over3_);
  trace_p->pop();
}

void CrouzeixRaviartStdTetrahedronP1::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x1 = *it_pt, x2 = *(it_pt + 1), x3 = *(it_pt + 2);
  std::vector<real_t>::iterator sh0w_i(shv.w.begin());
  *sh0w_i++ = 1. - 3.*x1;       // w_1
  *sh0w_i++ = 1. - 3.*x2;       // w_2
  *sh0w_i++ = 1. - 3.*x3;       // w_3
  *sh0w_i   = 3.*(x1 + x2 + x3) - 2.; // w_4

  if ( withDeriv )
  {
    std::vector< std::vector<real_t> >::iterator sh1w(shv.dw.begin()), sh2w(sh1w + 1), sh3w(sh2w + 1);
    std::vector<real_t>::iterator sh1w_i((*sh1w).begin()), sh2w_i((*sh2w).begin()), sh3w_i((*sh3w).begin());
    *sh1w_i++ = -3.; *sh2w_i++ = 0.; *sh3w_i++ = 0.; // grad w_1
    *sh1w_i++ = 0.; *sh2w_i++ = -3.; *sh3w_i++ = 0.; // grad w_2
    *sh1w_i++ = 0.; *sh2w_i++ = 0.; *sh3w_i++ = -3.; // grad w_3
    *sh1w_i   = 3.; *sh2w_i   = 3.; *sh3w_i   = 3.; // grad w_4
  }
}

} // end of namespace xlifepp
