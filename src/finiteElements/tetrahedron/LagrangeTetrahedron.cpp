/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file LagrangeTetrahedron.cpp
	\authors D. Martin, N. Kielbasiewicz
	\since 17 dec 2002
	\date 6 aug 2012

  \brief Implementation of xlifepp::LagrangeTetrahedron class members and related functions

	xlifepp::LagrangeTetrahedron defines Lagrange Reference Element interpolation data
	xlifepp::LagrangeTetrahedron is child to xlifepp::RefTetrahedron parent to template xlifepp::LagrangeStdTetrahedron<Pk>
 */

#include "LagrangeTetrahedron.hpp"
#include "../Interpolation.hpp"
#include "../splitUtils.hpp"
#include "utils.h"
#include <iostream>

namespace xlifepp
{

//! LagrangeTetrahedron constructor for Lagrange reference elements
LagrangeTetrahedron::LagrangeTetrahedron(const Interpolation* interp_p)
  : RefTetrahedron(interp_p)
{
  name_ += "_Lagrange";
  trace_p->push("LagrangeTetrahedron::LagrangeTetrahedron (" + name_ + ")");
  // build element interpolation data
  interpolationData();
  // define local numbering of points on edges
  sideOfSideNumbering();
  // find or create Reference Element for element edges
  sideOfSideRefElement();
  // define local numbering of points on sides
  sideNumbering();
  // find or create Reference Element for element sides
  sideRefElement();
  //build barycentricSideDofMap: internal dof on side (face) ordered by barycentric coordinates
  buildBarycentricSideDof();   //no effect if order < 3
  maxDegree = interpolation_p->numtype;

  trace_p->pop();
}

LagrangeTetrahedron::~LagrangeTetrahedron() {}

//! interpolationData defines Reference Element interpolation data
void LagrangeTetrahedron::interpolationData()
{
  trace_p->push("LagrangeTetrahedron::interpolationData");

  number_t interpNum = interpolation_p->numtype;
  switch (interpNum)
  {
    case _P0:
      nbDofs_ = nbInternalDofs_ = 1;
      break;
    case _P1:
      nbDofs_ = nbDofsOnVertices_ = 4;
      break;
    default: // Lagrange standard tetrahedron of degree > 1
      nbDofs_ = ((interpNum + 1)*(interpNum + 2)*(interpNum + 3)) / 6;
      nbDofsOnVertices_ = 4;
      nbInternalDofs_ = ((interpNum - 3)*(interpNum - 2)*(interpNum - 1)) / 6;
      nbDofsInSideOfSides_= 6*(interpNum - 1);
      nbDofsInSides_ = nbDofs_ - nbInternalDofs_ - nbDofsOnVertices_ - nbDofsInSideOfSides_;
      break;
  }
  // Creating Degrees Of Freedom of reference element
  // in Lagrange standard elements all D.o.F on sides are sharable
  // number of internal nodes (or D.o.F)
  refDofs.reserve(nbDofs_);
  lagrangeRefDofs(3, nbDofsOnVertices_, nbInternalDofs_, 6, nbDofsInSideOfSides_, 4, nbDofsInSides_);
  //lagrangeRefDofs(1, nbDofsOnVertices_, nbDofs_ - nbInternalDofs_, 3);
  // compute number of shape functions
  nbshfcts_ = shapeValueSize();
  nbPts_ = nbDofs_;
  trace_p->pop();
}

/*!internal side dofs mapping when vertices of side are permuted
    this function returns the number of the n-th internal dof of a face where vertices are permuted
    if no permutation, it returns n

    Numbering of internal side dofs of Pk tetrahedron, p= k-3

        1      2      2          2              2                  2                      2
        p=0    | \    | \        | \            | \                | \                    | \
               3---1  5   4      5   7          5  10              5  13                  5  16
                p=1   |     \    |     \        |     \            |     \                |     \
                      3---6---1  8  10   4      8  14   7          8  17  10              8  20  13
                         p=2     |         \    |  |  \   \        |  | \    \            |  | \    \
                                 3---6---9---1 11  15--13   4     11  20  19   7         11  23  25  10
                                       p=3      |             \    |  |     \    \        |  |     \    \
                                                3---6---9--12---1 14  18--21--16   4     14  26  28  22   7
                                                         p=4       |                 \    |  |         \    \
                                                                   3---6---9--12--15---1 17  21--24--27--19   4
                                                                            p=5           |                     \
                                                                                          3---6---9--12--15--18---1
                                                                                                     p=6
    Map exemple for P4 tetrahedron

    2                         1
    | \                       | \
    |   \                     |   \
    |     \                   |     \                     sideDofsMap(1,3,1,2)=2
    |   2   \                 |   1   \                   sideDofsMap(2,3,1,2)=3
    |   | \   \               |   | \   \                 sideDofsMap(3,3,1,2)=1
    |   3--1    \             |   2--3    \
    |             \           |             \
    3---------------1         2--------------3

   standard face              permuted face

  */
number_t LagrangeTetrahedron::sideDofsMap(const number_t& n, const number_t& i, const number_t& j, const number_t& k) const
{
  if (i==1 && j==2 && k==3) return n; //case (1,2,3)
  //permute barycentric coordinate of dof n
  Triplet<number_t> t=barycentricSideDofs[n-1], tp=t;
  switch (i)
  {
    case 1:
      tp.second=t.third;
      tp.third=t.second;           // case (1,3,2)
      break;
    case 2:
      tp.first=t.second;
      if (j==1) tp.second=t.first;  // case (2,1,3)
      else
      {
        tp.second=t.third;         // case (2,3,1)
        tp.third=t.first;
      }
      break;
    case 3:
      tp.first=t.third;
      if (j==1)
      {
        tp.second=t.first;         // case (3,1,2)
        tp.third=t.second;
      }
      else tp.third=t.first;       // case (3,2,1)
      break;
    default:
      where("LagrangeTetrahedron::sideDofsMap()");
      error("index_out_of_range","i", 1, 3);
  }
  //find new  dof number after permutation
  std::map<Triplet<number_t>,number_t>::const_iterator it=barycentricSideDofMap.find(tp);
  if (it==barycentricSideDofMap.end())
  {
    where("LagrangeTetrahedron::sideDofsMap()");
    error("triplet_not_found");
  }
  return it->second;
}

//create the map of internal dofs on side, ordered by barycentric coordinates
void LagrangeTetrahedron::buildBarycentricSideDof()
{
  number_t k=interpolation_p->numtype;
  if (k<3) return;   //no internal side dofs
  number_t n=k-3;     //number of layers
  number_t i=0;       //dof index
  number_t A=n+1, B=1,C=1;
  for (int p=n; p>=0; p-=3, A-=2, B++, C++)   //layer loop
  {
    number_t a=A, b=B,c=C;
    for (number_t j=0; int_t(j)<=p; j++, a--, b++)   //dof loop
    {
      Triplet<number_t> t(a,b,c);
      if (barycentricSideDofMap.find(t)==barycentricSideDofMap.end()) barycentricSideDofMap.insert(std::make_pair(t,++i));
      t=Triplet<number_t>(c,a,b);
      if (barycentricSideDofMap.find(t)==barycentricSideDofMap.end()) barycentricSideDofMap.insert(std::make_pair(t,++i));
      t=Triplet<number_t>(b,c,a);
      if (barycentricSideDofMap.find(t)==barycentricSideDofMap.end()) barycentricSideDofMap.insert(std::make_pair(t,++i));
    }
  }
  if (i>0)
  {
    barycentricSideDofs.resize(i);
    std::vector<Triplet<number_t> >::iterator itvb=barycentricSideDofs.begin();
    std::map<Triplet<number_t>,number_t>::iterator itm=barycentricSideDofMap.begin(), itme=barycentricSideDofMap.end();
    for (; itm!=itme; ++itm) *(itvb + (itm->second-1)) = itm->first;
  }
}

//! output of Lagrange D.o.f's numbers on underlying tetrahedron P1 D.o.f's
void LagrangeTetrahedron::outputAsP1(std::ofstream& os, const int refNum, const std::vector<number_t>& rks) const
{
  //const number_t w(9);
  number_t interpNum = interpolation_p->numtype;
  switch (interpNum)
  {
    case _P0:
    case _P1:
      simplexVertexOutput(os, refNum, rks[0], rks[1], rks[2], rks[3]);
      break;
    case _P2:
      // 4 half-size tetrahedra with nodes ( 1,10, 9, 5), (10, 2, 8, 6), ( 9, 8, 3, 7), ( 5, 6, 7, 4)
      simplexVertexOutput(os, refNum, rks[0], rks[9], rks[8], rks[4]);
      simplexVertexOutput(os, refNum, rks[9], rks[1], rks[7], rks[5]);
      simplexVertexOutput(os, refNum, rks[8], rks[7], rks[2], rks[6]);
      simplexVertexOutput(os, refNum, rks[4], rks[5], rks[6], rks[3]);
      // 4 tetrahedra forming 2 facing pyramids: ( 5, 8,10, 9), ( 5, 8, 7, 6), ( 8, 5, 7, 9), ( 8, 5,10, 6);
      simplexVertexOutput(os, refNum, rks[4], rks[7], rks[9], rks[8]);
      simplexVertexOutput(os, refNum, rks[4], rks[7], rks[6], rks[5]);
      simplexVertexOutput(os, refNum, rks[7], rks[4], rks[6], rks[8]);
      simplexVertexOutput(os, refNum, rks[7], rks[4], rks[9], rks[5]);
      break;
    default:
      noSuchFunction("outputAsP1");
      break;
  }
}

//! pointCoordinates defines Lagrange Reference Element point coordinates
std::vector<RefDof*>::iterator LagrangeTetrahedron::vertexCoordinates()
{
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();
  (*it_rd++)->coords(1., 0., 0.);
  (*it_rd++)->coords(0., 1., 0.);
  (*it_rd++)->coords(0., 0., 1.);
  (*it_rd++)->coords(0., 0., 0.);
  return it_rd;
}
//NEW VERSION of point coordinates calculation - compliant with face dofs ordering -
//does not used the tensorNumberingTetrahedron function
//checked up to degree 7
void LagrangeTetrahedron::pointCoordinates()
{
  trace_p->push("LagrangeTetrahedron::pointCoordinates");
  // Compute D.o.F's from the correspondence between segment and tetrahedron local numbering
  number_t interpNum = interpolation_p->numtype;
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();

  switch (interpNum)
  {
    case _P0:
      (*it_rd)->coords(.25, .25, .25);
      break;
    case _P1BubbleP3:
      it_rd = vertexCoordinates();
      (*it_rd++)->coords(.25, .25, .25);
      break;
    default:  // Lagrange standard tetrahedron of degree > 0
      std::vector<Point> vs(4,Point(0.,0.,0.));
      vs[0]=Point(1.,0.,0.);   vs[1]=Point(0.,1.,0.);   vs[2]=Point(0.,0.,1.);
      for (number_t i=0;i<4;i++)  (*it_rd++)->coords(vs[i]);
      if (interpNum > 1)
      {
        for (number_t k=1; k<=interpNum-1; k++)
        {
          real_t a=real_t(k)/interpNum;
          for (number_t e=1; e<=6; e++)   //edge dofs
            (*it_rd++)->coords((1-a)*vs[geomRefElem_p->sideOfSideVertexNumber(1,e)-1]+a*vs[geomRefElem_p->sideOfSideVertexNumber(2,e)-1]);
        }
      }
      if (interpNum < 3) { trace_p->pop(); return; }  //no face nodes
      for (number_t f=1; f<=4; f++)   //face dofs
      {
        Point &v1=vs[geomRefElem_p->sideVertexNumber(1,f)-1],
              &v2=vs[geomRefElem_p->sideVertexNumber(2,f)-1],
              &v3=vs[geomRefElem_p->sideVertexNumber(3,f)-1];
        number_t n=interpNum-3;     //number of layers
        number_t A=n+1, B=1, C=1;
        for (int p=n; p>=0; p-=3, A-=2, B++, C++)   //layer loop
        {
          number_t a=A, b=B,c=C;
          if (p==0)(*it_rd++)->coords((v1+v2+v3)/3.);
          else
            for (number_t j=1; int_t(j)<=p; j++, a--, b++)   //dof loop
            {
              (*it_rd++)->coords((a*v1+b*v2+c*v3)/interpNum);
              (*it_rd++)->coords((c*v1+a*v2+b*v3)/interpNum);
              (*it_rd++)->coords((b*v1+c*v2+a*v3)/interpNum);
            }
        }
      }
      if (interpNum < 4) { trace_p->pop(); return;}  //no internal nodes
      if (interpNum==4) //internal node for P4
      {
          (*it_rd)->coords(.25, .25, .25);
          trace_p->pop();
          return;
      }
      //internal nodes: Pk-4 nodes on tetrahedron (1-3/k, 1/k, 1/k ), (1/k, 1-3/k, 1/k), (1/k, 1/k, 1-3/k) , (1/k, 1/k, 1/k)
      //when using general Pk Lagrange, ordering of internal dofs is indifferent because shape functions are computed from coords
      Interpolation* intm1=findInterpolation(_Lagrange,_standard,interpNum-4,H1);
      RefElement* pkm1 = findRefElement(_tetrahedron, intm1);
      std::vector<RefDof*>::iterator itdm1=pkm1->refDofs.begin();
      real_t b=1./interpNum, a=1-4*b;
      for(;itdm1!=pkm1->refDofs.end(); itdm1++, it_rd++)
        (*it_rd)->coords(a*(*itdm1)->point()+b);  //x -> (1-4/k)x+1/k maps [0,1] to [1/k, 1-3/k]
      break;
  }
  trace_p->pop();
}

//! sideNumbering defines Lagrange Reference Element numbering on sides
void LagrangeTetrahedron::sideNumbering()
{
  trace_p->push("LagrangeTetrahedron::sideNumbering");

  number_t interpNum = interpolation_p->numtype;
  number_t interpNum1 = interpNum - 1;
  number_t nbSides = geomRefElem_p->nbSides();
  number_t nbSideOfSides = geomRefElem_p->nbSideOfSides();
  number_t nbVertices = geomRefElem_p->nbVertices();
  number_t nbVerticesPerSide = geomRefElem_p->sideVertexNumbers()[0].size();
  number_t nbDofsPerSideOfSide = interpNum + 1;
  number_t nbDofsPerSide = ((interpNum + 1)*(interpNum + 2)) / 2;
  number_t nbInternalDofsPerSide = ((interpNum - 2)*(interpNum - 1)) / 2;
  number_t more = nbVertices + interpNum1*nbSideOfSides;

  sideDofNumbers_.resize(nbSides);
  if(interpNum==0)  //Q0 interpolation
  {
       for(number_t side = 0; side < nbSides; side++)
       {
           sideDofNumbers_[side].resize(1);
           sideDofNumbers_[side][0]=1;
       }
       trace_p->pop();
       return;
  }
  if (interpNum > 0)
  {
    number_t sideNum = 0;
    for (number_t side = 0; side < nbSides; side++, sideNum++)
    {
      number_t nbSideOfSidesPerSide = 3;
      sideDofNumbers_[side].resize(nbDofsPerSide);
      for (number_t i = 0; i < nbVerticesPerSide; i++)
          sideDofNumbers_[side][i] = geomRefElem_p->sideVertexNumber(i + 1, side + 1);
      number_t i = 0;
      for (number_t sideOfSideNum = 0; sideOfSideNum < nbSideOfSidesPerSide ; sideOfSideNum++)
      {
        i = sideOfSideNum;
        int_t sideOfSideInSideNum = geomRefElem_p->sideOfSideNumber(sideOfSideNum + 1, side + 1);
        if (sideOfSideInSideNum > 0)
        {
          for (number_t e = 2; e < nbDofsPerSideOfSide; e++)
          {
            i += nbSideOfSidesPerSide;
            sideDofNumbers_[side][i] = sideOfSideDofNumbers_[sideOfSideInSideNum - 1][e];
          }
        }
        else
        {
          sideOfSideInSideNum = -sideOfSideInSideNum;
          for (number_t e = nbDofsPerSideOfSide - 1; e > 1; e--)
          {
            i += nbSideOfSidesPerSide;
            sideDofNumbers_[side][i] = sideOfSideDofNumbers_[sideOfSideInSideNum - 1][e];
          }
        }
      }
      for (number_t d = 0; d < nbInternalDofsPerSide; d++) // internal D.o.F in side
        sideDofNumbers_[side][++i] = ++more;
    }
  }
  trace_p->pop();
}

//! sideOfSideNumbering defines Lagrange Reference Element numbering on side of sides
void LagrangeTetrahedron::sideOfSideNumbering()
{
  trace_p->push("LagrangeTetrahedron::sideOfSideNumbering");
  number_t interpNum = interpolation_p->numtype;

  if (interpNum > 0)
  {
    number_t intN = interpNum;
    if (interpNum == _P1BubbleP3) intN = 1;
    number_t nbVertices = geomRefElem_p->nbVertices();
    number_t nbSideOfSides = geomRefElem_p->nbSideOfSides();
    number_t more;
    number_t nbVerticesPerSideOfSide = geomRefElem_p->sideOfSideVertexNumbers()[0].size();
    number_t nbDofsPerSideOfSide = intN + 1;
    sideOfSideDofNumbers_.resize(nbSideOfSides);
    for (number_t sideOfSide = 0; sideOfSide < nbSideOfSides; sideOfSide++)
    {
      sideOfSideDofNumbers_[sideOfSide].resize(nbDofsPerSideOfSide);
      for (number_t j = 0; j < nbVerticesPerSideOfSide; j++)
          sideOfSideDofNumbers_[sideOfSide][j] = geomRefElem_p->sideOfSideVertexNumber(j + 1, sideOfSide + 1);
      if (intN > 1)
      {
        more = nbVertices + 1;
        for (number_t k = nbVerticesPerSideOfSide; k < nbDofsPerSideOfSide; k++, more += nbSideOfSides)
          sideOfSideDofNumbers_[sideOfSide][k] = sideOfSide + more;
      }
    }
  }
	trace_p->pop();
}

//! return nodes numbers of first order tetrahedron elements when splitting current element
std::vector<std::vector<number_t> > LagrangeTetrahedron::splitP1() const {
  std::vector<std::vector<number_t> > splitnum, buf, buf2;
  std::vector<number_t> tuple(4, 0);
  std::vector<number_t> num;
  switch(interpolation_p->numtype) {
    case 0:
    case 1:
      num.resize(4);
      for (number_t i=0; i<num.size(); i++) num[i]=i+1;
      splitnum.push_back(num);
      return splitnum;
    case 2:
      num.resize(10);
      for (number_t i=0; i<num.size(); i++) num[i]=i+1;
      return splitTetrahedronP2ToTetrahedraP1(num);
    case 3:
      num.resize(20);
      for (number_t i=0; i<num.size(); i++) num[i]=i+1;
      return splitTetrahedronP3ToTetrahedraP1(num);
    default:
      return splitLagrange3DToP1();
  }
  return splitnum;
}

//! return nodes numbers of first order tetrahedron elements when splitting current element
splitvec_t LagrangeTetrahedron::splitO1() const
{
  splitvec_t splitnum;
  std::vector<std::vector<number_t> > buf;
  buf=this->splitP1();
  for(number_t i=0; i< buf.size(); i++)
    splitnum.push_back(std::pair<ShapeType, std::vector<number_t> >(_tetrahedron, buf[i]));
  return splitnum;
}

//! Lagrange standard P0 tetrahedron Reference Element
template<>
LagrangeStdTetrahedron<_P0>::LagrangeStdTetrahedron(const Interpolation* interp_p)
  : LagrangeTetrahedron(interp_p)
{
  name_ += "_0";
  pointCoordinates();
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

//! Lagrange standard P1 tetrahedron Reference Element
template<>
LagrangeStdTetrahedron<_P1>::LagrangeStdTetrahedron(const Interpolation* interp_p)
  : LagrangeTetrahedron(interp_p)
{
  name_ += "_1";
  pointCoordinates();
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

//! Lagrange standard Pk tetrahedron Reference Element
template<number_t Pk>
LagrangeStdTetrahedron<Pk>::LagrangeStdTetrahedron(const Interpolation* interp_p)
  : LagrangeTetrahedron(interp_p)
{
  name_ += "_" + tostring(Pk);
  pointCoordinates();
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
  //#ifdef FIG4TEX_OUTPUT
  //	// Draw a LagrangeStdTetrahedron using TeX and fig4TeX
  //	fig4TeX();
  //#endif
}

//! computeShapeValues defines Lagrange Reference Element shape functions
template<>
void LagrangeStdTetrahedron<_P0>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  std::vector<real_t>::iterator sh0w_i=shv.w.begin();
  *sh0w_i = 1.;
  if (withDeriv)
  {
    std::vector<std::vector<real_t> >::iterator sh1w=shv.dw.begin(), sh2w=sh1w+1, sh3w=sh1w+2;
    std::vector<real_t>::iterator sh1w_i=(*sh1w).begin(), sh2w_i=(*sh2w).begin(), sh3w_i=(*sh3w).begin();
    *sh1w_i = 0.;
    *sh2w_i = 0.;
    *sh3w_i = 0.;
  }
}

template<>
void LagrangeStdTetrahedron<_P1>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  std::vector<real_t>::iterator sh0w_i(shv.w.begin());
  *sh0w_i++ = *it_pt;
  *sh0w_i++ = *(it_pt+1);
  *sh0w_i++ = *(it_pt+2);
  *sh0w_i = 1-*it_pt-*(it_pt+1)-*(it_pt+2);

  if (withDeriv)
  {
    std::vector< std::vector<real_t> >::iterator sh1w(shv.dw.begin()), sh2w(sh1w + 1), sh3w(sh1w + 2);
    std::vector<real_t>::iterator sh1w_i((*sh1w).begin()), sh2w_i((*sh2w).begin()), sh3w_i((*sh3w).begin());
    *sh1w_i++ = 1.;
    *sh2w_i++ = 0.;
    *sh3w_i++ = 0.;
    *sh1w_i++ = 0.;
    *sh2w_i++ = 1.;
    *sh3w_i++ = 0.;
    *sh1w_i++ = 0.;
    *sh2w_i++ = 0.;
    *sh3w_i++ = 1.;
    *sh1w_i = -1.;
    *sh2w_i = -1.;
    *sh3w_i = -1.;
  }
}

template<>
void LagrangeStdTetrahedron<_P2>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x1=*it_pt, x2=*(it_pt+1), x3=*(it_pt+2), x4=1.-x1-x2-x3;
  real_t qx1=4.*x1, qx2=4.*x2, qx3=4.*x3, qx4=4.*x4, qx4m1=qx4-1.;
  std::vector<real_t>::iterator sh0w_i = shv.w.begin();
  *sh0w_i++ = x1*(x1+x1-1.);
  *sh0w_i++ = x2*(x2+x2-1.);
  *sh0w_i++ = x3*(x3+x3-1.);
  *sh0w_i++ = x4*(x4+x4-1.);
  *sh0w_i++ = qx1*x4;
  *sh0w_i++ = qx2*x4;
  *sh0w_i++ = qx3*x4;
  *sh0w_i++ = qx2*x3;
  *sh0w_i++ = qx1*x3;
  *sh0w_i = qx1*x2;

  if (withDeriv)
  {
    std::vector< std::vector<real_t> >::iterator sh1w(shv.dw.begin()), sh2w(sh1w + 1), sh3w(sh1w + 2);
    std::vector<real_t>::iterator sh1w_i((*sh1w).begin()), sh2w_i((*sh2w).begin()), sh3w_i((*sh3w).begin());
    *sh1w_i++ = qx1-1.;    *sh2w_i++ = 0.;        *sh3w_i++ = 0.;
    *sh1w_i++ = 0.;        *sh2w_i++ = qx2-1.;    *sh3w_i++ = 0.;
    *sh1w_i++ = 0.;        *sh2w_i++ = 0.;        *sh3w_i++ = qx3-1.;
    *sh1w_i++ = -qx4m1;    *sh2w_i++ = -qx4m1;    *sh3w_i++ = -qx4m1;
    *sh1w_i++ = qx4-qx1;   *sh2w_i++ = -qx1;      *sh3w_i++ = -qx1;
    *sh1w_i++ = -qx2;      *sh2w_i++ = qx4-qx2;   *sh3w_i++ = -qx2;
    *sh1w_i++ = -qx3;      *sh2w_i++ = -qx3;      *sh3w_i++ = qx4-qx3;
    *sh1w_i++ = 0.;        *sh2w_i++ = qx3;       *sh3w_i++ = qx2;
    *sh1w_i++ = qx3;       *sh2w_i++ = 0.;        *sh3w_i++ = qx1;
    *sh1w_i = qx2;         *sh2w_i = qx1;         *sh3w_i = 0.;
  }
}

template<>
void LagrangeStdTetrahedron<_P3>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x1 = *it_pt, x2 = *(it_pt + 1), x3 = *(it_pt + 2), x4 = 1. - x1 - x2 - x3;
  real_t tx1m1 = 3.*x1 - 1., tx2m1 = 3.*x2 - 1., tx3m1 = 3.*x3 - 1., tx4m1 = 3.*x4 - 1.;
  std::vector<real_t>::iterator sh0w_i = shv.w.begin();
  *sh0w_i++ = 0.5*x1*tx1m1*(tx1m1-1.);
  *sh0w_i++ = 0.5*x2*tx2m1*(tx2m1-1.);
  *sh0w_i++ = 0.5*x3*tx3m1*(tx3m1-1.);
  *sh0w_i++ = 0.5*x4*tx4m1*(tx4m1-1.);
  *sh0w_i++ = 4.5*x1*tx1m1*x4;
  *sh0w_i++ = 4.5*x2*tx2m1*x4;
  *sh0w_i++ = 4.5*x3*tx3m1*x4;
  *sh0w_i++ = 4.5*x2*tx2m1*x3;
  *sh0w_i++ = 4.5*x1*tx1m1*x3;
  *sh0w_i++ = 4.5*x1*tx1m1*x2;
  *sh0w_i++ = 4.5*x4*tx4m1*x1;
  *sh0w_i++ = 4.5*x4*tx4m1*x2;
  *sh0w_i++ = 4.5*x4*tx4m1*x3;
  *sh0w_i++ = 4.5*x3*tx3m1*x2;
  *sh0w_i++ = 4.5*x3*tx3m1*x1;
  *sh0w_i++ = 4.5*x2*tx2m1*x1;
  *sh0w_i++ = 27.*x2*x3*x4;
  *sh0w_i++ = 27.*x1*x3*x4;
  *sh0w_i++ = 27.*x1*x2*x4;
  *sh0w_i = 27.*x1*x2*x3;

  if (withDeriv)
  {
    std::vector< std::vector<real_t> >::iterator sh1w(shv.dw.begin()), sh2w(sh1w + 1), sh3w(sh1w + 2);
    std::vector<real_t>::iterator sh1w_i((*sh1w).begin()), sh2w_i((*sh2w).begin()), sh3w_i((*sh3w).begin());
    *sh1w_i++ = 13.5*x1*x1 - 9.*x1 + 1.;          *sh2w_i++ = 0.;                               *sh3w_i++ = 0.;
    *sh1w_i++ = 0.;                               *sh2w_i++ = 13.5*x2*x2 - 9.*x2 + 1.;          *sh3w_i++ = 0.;
    *sh1w_i++ = 0.;                               *sh2w_i++ = 0.;                               *sh3w_i++ = 13.5*x3*x3 - 9.*x3 + 1.;
    *sh1w_i++ = -13.5*x4*x4 + 9.*x4 - 1.;         *sh2w_i++ = -13.5*x4*x4 + 9.*x4 - 1.;         *sh3w_i++ = -13.5*x4*x4 + 9.*x4 - 1.;
    *sh1w_i++ = -4.5*(3.*x1*x1-(1.+6.*x4)*x1+x4); *sh2w_i++ = -4.5*x1*tx1m1;                    *sh3w_i++ = -4.5*x1*tx1m1;
    *sh1w_i++ = -4.5*x2*tx2m1;                    *sh2w_i++ = -4.5*(3.*x2*x2-(1.+6.*x4)*x2+x4); *sh3w_i++ = -4.5*x2*tx2m1;
    *sh1w_i++ = -4.5*x3*tx3m1;                    *sh2w_i++ = -4.5*x3*tx3m1;                    *sh3w_i++ = -4.5*(3.*x3*x3-(1.+6.*x4)*x3+x4);
    *sh1w_i++ = 0.;                               *sh2w_i++ = 4.5*x3*(6.*x2-1.);                *sh3w_i++ = 4.5*x2*tx2m1;
    *sh1w_i++ = 4.5*x3*(6.*x1-1.);                *sh2w_i++ = 0.;                               *sh3w_i++ = 4.5*x1*tx1m1;
    *sh1w_i++ = 4.5*x2*(6.*x1-1.);                *sh2w_i++ = 4.5*x1*tx1m1;                     *sh3w_i++ = 0.;
    *sh1w_i++ = 4.5*((1.-6.*x4)*x1+3.*x4*x4-x4);  *sh2w_i++ = 4.5*x1*(1.-6.*x4);                *sh3w_i++ = 4.5*x1*(1.-6.*x4);
    *sh1w_i++ = 4.5*x2*(1.-6.*x4);                *sh2w_i++ = 4.5*((1.-6.*x4)*x2+3.*x4*x4-x4);  *sh3w_i++ = 4.5*x2*(1.-6.*x4);
    *sh1w_i++ = 4.5*x3*(1.-6.*x4);                *sh2w_i++ = 4.5*x3*(1.-6.*x4);                *sh3w_i++ = 4.5*((1.-6.*x4)*x3+3.*x4*x4-x4);
    *sh1w_i++ = 0.;                               *sh2w_i++ = 4.5*x3*tx3m1;                     *sh3w_i++ = 4.5*x2*(6.*x3-1.);
    *sh1w_i++ = 4.5*x3*tx3m1;                     *sh2w_i++ = 0.;                               *sh3w_i++ = 4.5*x1*(6.*x3-1.);
    *sh1w_i++ = 4.5*x2*tx2m1;                     *sh2w_i++ = 4.5*x1*(6.*x2-1.);                *sh3w_i++ = 0.;
    *sh1w_i++ = -27.*x2*x3;                       *sh2w_i++ = 27.*x3*(x4-x2);                   *sh3w_i++ = 27.*x2*(x4-x3);
    *sh1w_i++ = 27.*x3*(x4-x1);                   *sh2w_i++ = -27.*x3*x1;                       *sh3w_i++ = 27.*x1*(x4-x3);
    *sh1w_i++ = 27.*x2*(x4-x1);                   *sh2w_i++ = 27.*x1*(x4-x2);                   *sh3w_i++ = -27.*x1*x2;
    *sh1w_i = 27.*x2*x3;                          *sh2w_i = 27.*x1*x3;                          *sh3w_i = 27.*x1*x2;
  }
}

template class LagrangeStdTetrahedron<_P2>;
template class LagrangeStdTetrahedron<_P3>;

/*!
	correspondance between segment and tetrahedron local numbering defined such that
	- point number p has coordinates defined by
	- ( x = coords[s2h[0][p]], y = coords[s2h[1][p]], z = coords[s2h[3][p]] )
	where coords are coordinates of the associated 1D Lagrange reference element
	----  No longer used -----
 */
void tensorNumberingTetrahedron(const int interpNum, number_t**& s2t)
{
  // CHECKED ONLY UP TO degree 7 !!!
  int nk(interpNum), nkk, nk2;
  number_t p=0;//, pa=0;
  number_t q1=0, q2=1, q3=2, q4=interpNum, z1=1;
  number_t q3_p, q4_m;

  while (nk > 0)
  {
    // vertices of 'current perimeter'
    s2t[0][p] = q1;
    s2t[1][p] = q2;
    s2t[2][p++] = q2;  // 1
    s2t[0][p] = q2;
    s2t[1][p] = q1;
    s2t[2][p++] = q2;  // 2
    s2t[0][p] = q2;
    s2t[1][p] = q2;
    s2t[2][p++] = q1;  // 3
    s2t[0][p] = q2;
    s2t[1][p] = q2;
    s2t[2][p++] = q2;  // 4
    //  non-vertex points on edges of 'current perimeter'
    q3_p = q3;
    q4_m = q4;
    for (int j = 2; j <= nk; j++)
    {
      s2t[0][p] = q3_p;
      s2t[1][p] = q2;
      s2t[2][p++] = q2;   //Edge# 1->4, points # 5, 11, ...
      s2t[0][p] = q2;
      s2t[1][p] = q3_p;
      s2t[2][p++] = q2;   //Edge# 2->4, points # 6, 12, ...
      s2t[0][p] = q2;
      s2t[1][p] = q2;
      s2t[2][p++] = q3_p; //Edge# 3->4, points # 7, 13, ...
      s2t[0][p] = q2;
      s2t[1][p] = q3_p;
      s2t[2][p++] = q4_m; //Edge# 2->3, points # 8, 14, ...
      s2t[0][p] = q3_p;
      s2t[1][p] = q2;
      s2t[2][p++] = q4_m; //Edge# 1->3, points # 9, 15, ...
      s2t[0][p] = q3_p;
      s2t[1][p] = q4_m;
      s2t[2][p++] = q2;   //Edge# 1->2, points # 10, 16, ...
      q3_p++;
      q4_m--;
    }

    nkk = nk - 3;

    //		for ( int i = pa; i < p; i++ ) { thePrintStream_ <<std::endl<<"  ep "<< i+1 <<" ("<< s2t[0][i] << ", "<<s2t[1][i]<<", "<<s2t[2][i]<<")"; }
    //		pa = p;
    // 'current perimeter' internal points on sides
    number_t x1=q4, x2=q3 + 1, x1m, x2p;
    if (nkk >= 0)
    {
      // Face #1 x = "0"
      tensorTetrahedronSideNumbering(nkk, 0, 1, 2, z1, x1, x2, s2t, p);
      // Face #2 y = "0"
      tensorTetrahedronSideNumbering(nkk, 1, 2, 0, z1, x1, x2, s2t, p);
      // Face #3 z = "0"
      tensorTetrahedronSideNumbering(nkk, 2, 0, 1, z1, x1, x2, s2t, p);
      // Face #4 x+y+z = "1"
//          pa = p;
      nk2 = nkk;
      while (nk2 > 0)
      {
        x1m = x1;
        x2p = x2;
        for (dimen_t j = 1; j <= nk2; j++)
        {
          s2t[0][p] = x2p;
          s2t[1][p] = x1 ;
          s2t[2][p++] = x1m;
          s2t[0][p] = x1 ;
          s2t[1][p] = x1m;
          s2t[2][p++] = x2p;
          s2t[0][p] = x1m;
          s2t[1][p] = x2p;
          s2t[2][p++] = x1;
          x1m--;
          x2p++;
        }
        x1--;
        x2 += 2 ;
        nk2 -= 3;
      }
      if (nk2 == 0)
      {
        s2t[0][p] = x1;
        s2t[1][p] = x1;
        s2t[2][p++] = x1;
      }
    }
    q1 += 3;
    q2--;
    q3 += 3;
    q4--;
    z1--;
    if (nk == interpNum)
    {
      q1 = 4;
      q2 = nk;
      z1 = nk;
    }
    nk -= 4;
  }

  // central point in element if any
  if (nk == 0)
  {
    s2t[0][p] = q1;
    s2t[1][p] = q1;
    s2t[2][p] = q1;
  }
}

void tensorTetrahedronSideNumbering(const int nk, const dimen_t i0, const dimen_t i1, const dimen_t i2, const number_t z1, const number_t xx1, const number_t xx2, number_t**& s2t, number_t& p)
{
  // Define correspondence between segment and local numbering of internal D.o.F's
  // on side of the Lagrange Pk tetrahedron in the same order as the order of numbering
  // of internal D.o.F's of the Lagrange Pk triangle
  int  nk2(nk), x1(xx1), x2(xx2), x1m, x2p;//, pa(p);
  while (nk2 > 0)
  {
    // points on vertices and edges of current 'perimeter' on side
    x1m = x1;
    x2p = x2;
    for (int j = 1; j <= nk2; j++)
    {
      s2t[i0][p] = z1;
      s2t[i1][p] = x1;
      s2t[i2][p++] = x1m;
      s2t[i0][p] = z1;
      s2t[i1][p] = x1m;
      s2t[i2][p++] = x2p;
      s2t[i0][p] = z1;
      s2t[i1][p] = x2p;
      s2t[i2][p++] = x1;
      x1m--;
      x2p++;
    }
    x1--;
    x2 += 2;
    nk2 -= 3;
  }
  // central point on side if any
  if (nk2 == 0)
  {
    s2t[i0][p] = z1;
    s2t[i1][p] = x1;
    s2t[i2][p++] = x1;
  }
}


/*================================================================================================
  Lagrange standard Pk tetrahedron (any order) using formal polynomials to compute shape functions
  ================================================================================================*/

LagrangeStdTetrahedronPk::LagrangeStdTetrahedronPk(const Interpolation* interp_p)
  : LagrangeTetrahedron(interp_p)
{
  name_ += "_" + tostring(interp_p->numtype);
  pointCoordinates();  // local coordinates of Points supporting D.o.F
  initShapeFunCoeffs();
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

LagrangeStdTetrahedronPk::~LagrangeStdTetrahedronPk() {}

/*! init shape functions coefficients in basis monoms by solving linear systems n=nbdofs=(k+1)*(k+2)*(k+3)/6
*/
void LagrangeStdTetrahedronPk::initShapeFunCoeffs()
{
  //build matrix
  Matrix<real_t> M(nbDofs_,nbDofs_);
  std::vector<real_t>::iterator itM=M.begin();
  std::vector<RefDof*>::iterator itd=refDofs.begin();
  std::vector<real_t>::const_iterator itp;
  for (number_t i=0; i<nbDofs_; i++, itM+=nbDofs_, itd++)
  {
    itp=(*itd)->coords();
    monomes(*itp, *(itp+1),*(itp+2), itM, 0);
  }
  //solving system
  shapeFunCoeffs=Matrix<real_t>(nbDofs_,nbDofs_);
  for (number_t k=1; k<=nbDofs_; k++) shapeFunCoeffs(k,k)=1.;
  number_t row;
  real_t piv;
  bool solved=gaussMultipleSolver(M, shapeFunCoeffs, nbDofs_, piv, row);
  if (!solved)
  {
    where("LagrangeStdTetrahedronPk::initShapeFunCoeffs");
    error("mat_noinvert");
  }
}

/*!  computeShapeValues defines Lagrange Pk Reference Element shape functions
     general method using matrix approach:
     the nth basis function is represented as An0 An1*y An2*y^2 + ...+Ank*y^k + ... + AnNx^k
     where An. is the nth row of a matrix stored in RefElement class as shapeFunCoeffs
     this matrix has to be computed before
     the monomes function returns values or derivatives of monoms at a given point
*/
void LagrangeStdTetrahedronPk::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                                                  const bool withDeriv, const bool with2Deriv) const
{
  real_t x=*it_pt, y=*(it_pt+1), z=*(it_pt+2);
  Vector<real_t> mons(nbDofs_);
  monomes(x,y,z,mons.begin(),0);
  shv.w = shapeFunCoeffs*mons;
  if (withDeriv)
  {
    monomes(x,y,z,mons.begin(),1);
    shv.dw[0] = shapeFunCoeffs*mons;
    monomes(x,y,z,mons.begin(),2);
    shv.dw[1] = shapeFunCoeffs*mons;
    monomes(x,y,z,mons.begin(),3);
    shv.dw[2] = shapeFunCoeffs*mons;
  }
}

/*! compute value or derivatives of monomes (1, x, y, z, x2, y2, z2, xy, xz, yz ...) at a point (x,y,z)
    monomes ordering is given by nested loops i=1,n ; j=1,n-i; k=1,n-i-j
    der = 0 : values
    der = 1 : dx values
    der = 2 : dy values
    der = 3 : dz values
*/
void LagrangeStdTetrahedronPk::monomes(real_t x, real_t y, real_t z, std::vector<real_t>::iterator itm, number_t der) const
{
  number_t n=interpolation_p->numtype;
  real_t vi=1.;
  switch (der)
  {
    case 0:
    {
      for (number_t i=0; i<=n; i++, vi*=x)
      {
        real_t vj=1.;
        for (number_t j=0; j<=n-i; j++, vj*=y)
        {
          real_t vk=1.;
          for (number_t k=0; k<=n-i-j; k++,itm++, vk*=z)  *itm = vi*vj*vk;
        }
      }
      return;
    }
    case 1:
    {
      for (number_t j=0; j<(n+1)*(n+2)/2; j++,itm++)    *itm = 0;  //does not depends on x
      for (number_t i=1; i<=n; i++, vi*=x)
      {
        real_t vj=1.;
        for (number_t j=0; j<=n-i; j++,vj*=y)
        {
          real_t vk=1.;
          for(number_t k=0; k<=n-i-j; k++,itm++, vk*=z)  *itm = i*vi*vj*vk;
        }
      }
      return;
    }
    case 2:
    {
      for (number_t i=0; i<=n; i++, vi*=x)
      {
        for (number_t j=0; j<=n-i; j++ ,itm++) *itm=0.; //does not depends on y
        real_t vj=1.;
        for (number_t j=1; j<=n-i; j++,vj*=y)
        {
          real_t vk=1.;
          for(number_t k=0; k<=n-i-j; k++,itm++, vk*=z)  *itm = j*vi*vj*vk;
        }
      }
      return;
    }
    case 3:
    {
      for (number_t i=0; i<=n; i++, vi*=x)
      {
        real_t vj=1.;
        for (number_t j=0; j<=n-i; j++,vj*=y)
        {
          *itm=0.;
          itm++;
          real_t vk=1.;
          for (number_t k=1; k<=n-i-j; k++,itm++, vk*=z)  *itm = k*vi*vj*vk;
        }
      }
      return;
    }
    default:
      where("LagrangeStdTetrahedron::monomes");
      error("bad_derivative",der);
  }
}

// // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //
// #ifdef FIG4TEX_OUTPUT
// //! fig4TeX builds a tex file to display local numbering and coordinates
// void LagrangeTetrahedron::fig4TeX() {
// 	// number_t of points on each edge
// 	number_t nbPtsPerSide(nbDofs(1, 1));
// 	// number_t of points on all edges
// 	number_t nbPtsOnSides = 3*(nbPtsPerSide-1);
// 	char b=char(92);

// 	std::ofstream os;
// 	os.open((fig4TexFileName()).c_str(), ios::out);
// 	os << "%" << b << "input fig4tex.tex"<<b<<"input TeXnicolor.tex"<<b<<"input Fonts.tex";
// 	os <<std::endl<<b<<"fourteenpoint"<<b<<"nopagenumbers";
//   os <<std::endl<<b<<"newbox"<<b<<"mybox";
//   if ( interpolation_p->numtype < 4 )
//   {
//      os <<std::endl<<b<<"figinit{200pt,realistic}";
//      os <<b<<"def"<<b<<"Obdist{17}"<<b<<"def"<<b<<"Valpsi{40}"<<b<<"def"<<b<<"Valtheta{20}";
//   }
//   else
//   {
//      os <<std::endl<<b<<"figinit{300pt,realistic}";
//      os <<b<<"def"<<b<<"Obdist{17}"<<b<<"def"<<b<<"Valpsi{30}"<<b<<"def"<<b<<"Valtheta{30}";
//   }
//   // 3D settings
//   os <<std::endl<<b<<"figsetview("<<b<<"Valpsi,"<<b<<"Valtheta)"<<b<<"figsetobdist("<<b<<"Obdist)";
//   string longName = "Lagrange Tetrahedron of degree ";

//   // Draw element and display vertices
//   number_t pts(0);
//   // define vertices
//   fig4TeXVertexPt(os, geom_ref_elt_p->vertices(), pts);

//   // start postscript file
//   os <<std::endl<<b<<"psbeginfig{}"
//   <<std::endl<<b<<"pssetwidth{1.2}"<<b<<"psline[1,2,3,1]"
//   <<std::endl<<b<<"pssetdash{7}"<<b<<"psline[1,4,2,4,3]";
//   if ( nbPtsPerSide > 4 )
//   {
//      // Draw reduced side on each side
//      for ( number_t fa = 1; fa <= 4 ; fa++ ) fig4TeXSmallFace(os, fa, 3, nbPtsOnSides);
//   }
//   real_t transl(0.);
//   if ( nbInternalDofs_ > 1 )
//   {
//      os <<std::endl<<"% internal element";
//      // Draw reduced tetrahedron inside
//      fig4TeXInternalElement(os, transl);
//      if ( interpolation_p->numtype > 5)
//      {
//         transl = -0.2*interpolation_p->numtype;
//         fig4TeXInternalElement(os, transl);
//      }
//   }
//   os <<std::endl<<b<<"psendfig"; // end of postcript output

//   os <<std::endl<<b<<"figvisu{"<<b<<"mybox}{"<<b<<"bf "<< longName << interpolation_p->numtype <<"}{%"
//   <<std::endl<<b<<"figsetmark{$"<<b<<"figBullet$}"<<b<<"figsetptname{{"<<b<<"bf#1}}";
//   // vertex nodes
//   os <<std::endl<<b<<"figwritesw1:(5pt)"<<std::endl<<b<<"figwritee2:(5pt)"
//   <<std::endl<<b<<"figwriten3:(5pt)"<<std::endl<<b<<"figwritene4:(5pt)";

//   if ( nbPtsPerSide > 2 )
//   {
//      // nodes on edges
//      number_t ed(0);
//      fig4TeXEdgePt(os , ed++, pts, "w");
//      fig4TeXEdgePt(os , ed++, pts, "ne");
//      fig4TeXEdgePt(os , ed++, pts, "w");
//      fig4TeXEdgePt(os , ed++, pts, "ne");
//      fig4TeXEdgePt(os , ed++, pts, "nw");
//      fig4TeXEdgePt(os , ed++, pts, "s");
//      // nodes on sides
//      ed = 0;
//      fig4TeXFacePt(os , ed++, nbPtsOnSides, pts, "ne");
//      fig4TeXFacePt(os , ed++, nbPtsOnSides, pts, "nw");
//      fig4TeXFacePt(os , ed++, nbPtsOnSides, pts, "s");
//      fig4TeXFacePt(os , ed++, nbPtsOnSides, pts, "s");
//   }
//   // internal nodes
//   if ( nbInternalDofs_ > 0 ) fig4TeXInternalPoints(os, transl, pts);
//   // end of output
//   os <<std::endl<< "}" <<b<<"par"<<b<<"leftline{"<<b<<"box"<<b<<"mybox"<<b<<"hfill}" <<std::endl<<b<<"bye"<<std::endl;
//   os.close();
// }

// void LagrangeTetrahedron::fig4TeXInternalElement(std::ofstream& os, const real_t transl)
// {
//   // Draw reduced tetrahedron inside
//   const char b=char(92);
//   number_t pp(11);
//   for ( number_t i_dof = nbDofs_-nbInternalDofs_+1; i_dof < nbDofs_-nbInternalDofs_+5; i_dof++, pp++ )
//   {
//      std::vector<real_t>::const_iterator coor = refDofs[i_dof-1]->coords();
//      os <<std::endl<<b<<"figpt"<<pp<<":(";
//      os << setprecision(5) << transl + *coor++ <<"," << setprecision(5) << *coor++ <<",";
//      os << setprecision(5) << *coor++ <<")";
//   }
//   os<<std::endl<<b<<"psset (color="<<b<<"Redrgb)"
//   <<std::endl<<b<<"pssetdash{8}"<<b<<"pssetwidth{0.4}"<<b<<"psline[11,12,13,11]"
//   <<std::endl<<b<<"pssetdash{8}"<<b<<"psline[11,14,12,14,13]";
//   os<<std::endl<<b<<"psset (color="<<b<<"Blackrgb)";
// }

// void LagrangeTetrahedron::fig4TeXInternalPoints(std::ofstream& os, const real_t transl, number_t& pts)
// {
//   const char b=char(92);
//   os <<std::endl<<b<<"tenpoint"<<b<<"it"<<b<<"color{"<<b<<"Red}"<<std::endl<<b<<"figsetmark{$"<<b<<"diamond$}";
//   std::vector<RefDof*>::const_iterator refDofs_b(refDofs.begin()+pts), it_rd;
//   for ( it_rd = refDofs_b; it_rd != refDofs.end(); it_rd++, pts++ )
//   {
//      std::vector<real_t>::const_iterator coor = (*it_rd)->coords();
//      os <<std::endl<<b<<"figpt0:" << pts+1 <<"(";
//      os << setprecision(5) << transl + *coor++ <<","<< setprecision(5) << *coor++ <<",";
//      os << setprecision(5) << *coor++ <<")";
//      os <<b<<"figwrites0:"<<pts+1<<"(3pt)";
//   }
//   os <<std::endl<<b<<"endcolor{}";
// }

// #endif  /* FIG4TEX_OUTPUT */
// // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //

} // end of namespace xlifepp
