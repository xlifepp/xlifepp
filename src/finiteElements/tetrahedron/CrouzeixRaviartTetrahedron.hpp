/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file CrouzeixRaviartTetrahedron.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 6 aug 2012

  \brief Definition of the xlifepp::CrouzeixRaviartTetrahedron class

  xlifepp::CrouzeixRaviartTetrahedron defines Crouzeix-Raviart non-conforming interpolation data

  member functions
    - interp   interpolation data
    - sideNumbering  local numbering on edges
    - pointCoordinates point coordinates

  child classes
    - xlifepp::CrouzeixRaviartStdTetrahedronP1 Crouzeix-Raviart standard P1 Reference Element
*/

#ifndef CROUZEIX_RAVIART_TETRAHEDRON_HPP
#define CROUZEIX_RAVIART_TETRAHEDRON_HPP

#include "config.h"
#include "RefTetrahedron.hpp"

namespace xlifepp
{

class Interpolation;

/*!
  \class CrouzeixRaviartTetrahedron
  ( conforming elements )

  Parent class: RefTetrahedron
  Child classes: CrouzeixRaviartStdTetrahedronP1
 */
class CrouzeixRaviartTetrahedron : public RefTetrahedron
{
  public:
    CrouzeixRaviartTetrahedron(const Interpolation* interp_p); //!< constructor by interpolation
    virtual ~CrouzeixRaviartTetrahedron(); //!< destructor

  protected:
    void interpolationData(); //!< builds iinterpolation data
    void sideNumbering(); //!< builds side numbering

    virtual void pointCoordinates() = 0; //!< builds point coordinates

  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class CrouzeixRaviartTetrahedron

/*!
  \class CrouzeixRaviartStdTetrahedronP1
 */
class CrouzeixRaviartStdTetrahedronP1: public CrouzeixRaviartTetrahedron
{
  public:
    CrouzeixRaviartStdTetrahedronP1(const Interpolation* interp_p); //!< constructor by interpolation
    ~CrouzeixRaviartStdTetrahedronP1(); //!< destructor

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv=false) const; //!< shape functions

    void pointCoordinates(); //!< builds point coordinates on reference element

}; // end of class CrouzeixRaviartStdTetrahedronP1

} // end of namespace xlifepp

#endif /* CROUZEIX_RAVIART_TETRAHEDRON_HPP */
