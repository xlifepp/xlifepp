/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LagrangeTetrahedron.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 6 aug 2012

  \brief Definition of the xlifepp::LagrangeTetrahedron class

  xlifepp::LagrangeTetrahedron is child to xlifepp::RefTetrahedron and defines Lagrange Reference Element interpolation data

  grandparent class: xlifepp::RefElement

  parent class: xlifepp::RefTetrahedron

  child classes: xlifepp::LagrangeStdTetrahedron<Pk> Lagrange standard Reference Element

  member functions
    - interpolationData interpolation data
    - sideNumbering     local numbers on faces
    - pointCoordinates  coordinates of points (support of D.o.F)
    - vertexCoordinates coordinates of vertices
    - computeShapeValues    computation of shape functions
    - fig4TeX TeX format output of local numbers & coordinates

  external class related functions
    - tensorNumberingHexahedron: builds correspondence between segment and tetrahedron local numbering
*/

#ifndef LAGRANGE_TETRAHEDRON_HPP
#define LAGRANGE_TETRAHEDRON_HPP

#include "config.h"
#include "RefTetrahedron.hpp"

namespace xlifepp
{

class Interpolation;

/*!
  \class LagrangeTetrahedron
  defines Lagrange Reference Element interpolation data on tetrahedra
 */
class LagrangeTetrahedron : public RefTetrahedron
{
  public:
    LagrangeTetrahedron(const Interpolation* interp_p); //!< constructor by interpolation
    virtual ~LagrangeTetrahedron(); //!< destructor

  protected:
    void interpolationData(); //!< builds interpolation data on reference element
    void outputAsP1(std::ofstream& os, const int refNum, const std::vector<number_t>& ranks) const; //!< output of Lagrange D.o.f's numbers on underlyin P1 D.o.f's
    void pointCoordinates(); //!< builds point coordinates on reference element
    std::vector<RefDof*>::iterator vertexCoordinates();
    void sideNumbering(); //!< builds local numbers on faces of reference element
    void sideOfSideNumbering(); //!< builds local numbers on faces of reference element
    std::vector<std::vector<number_t> > splitP1() const;  //!< return nodes numbers of first order tetrahedron elements when splitting current element
    splitvec_t splitO1() const;  //!< return nodes numbers of first order tetrahedron elements when splitting current element

    public:
   //related to global dof construction
    std::map<Triplet<number_t>,number_t> barycentricSideDofMap;           //!< internal dof on side (face) ordered by barycentric coordinates
    std::vector<Triplet<number_t> > barycentricSideDofs;                  //!< dof barycentric coordinates
    void buildBarycentricSideDof();                                       //!< build barycentricSideDofMap
    number_t sideDofsMap(const number_t& n, const number_t& i, const number_t& j, const number_t& k=0) const; //!<internal side dofs mapping when vertices of side are permuted
    number_t sideofsideDofsMap(const number_t& n, const number_t& i, const number_t& j=0) const //! internal side dofs mapping when vertices of side are permuted
    {if(i>j) return (nbDofsInSideOfSides()/6-n+1); return n;}

  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }
// FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //
// #ifdef FIG4TEX_OUTPUT
//     void fig4TeX();
//     void fig4TeXInternalElement(ofstream& os, const real_t transl);
//     void fig4TeXInternalPoints(ofstream& os, const real_t transl, number_t& pts);
// #endif  /* FIG4TEX_OUTPUT */
// FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //

}; // end of class LagrangeTetrahedron

/*!
  \class LagrangeStdTetrahedron
  template class child to class LagrangeTetrahedron

  http://homepage.mac.com/danielmartin/melina++/doc/elements/tetra/LagrangeStdTetrahedron1.pdf
  http://homepage.mac.com/danielmartin/melina++/doc/elements/tetra/LagrangeStdTetrahedron2.pdf
  http://homepage.mac.com/danielmartin/melina++/doc/elements/tetra/LagrangeStdTetrahedron3.pdf
  http://homepage.mac.com/danielmartin/melina++/doc/elements/tetra/LagrangeStdTetrahedron4.pdf
  http://homepage.mac.com/danielmartin/melina++/doc/elements/tetra/LagrangeStdTetrahedron5.pdf
  http://homepage.mac.com/danielmartin/melina++/doc/elements/tetra/LagrangeStdTetrahedron6.pdf
  http://homepage.mac.com/danielmartin/melina++/doc/elements/tetra/LagrangeStdTetrahedron7.pdf
 */
template<number_t Pk>
class LagrangeStdTetrahedron : public LagrangeTetrahedron
{
  public:
    LagrangeStdTetrahedron(const Interpolation* interp_p); //!< constructor by interpolation
    ~LagrangeStdTetrahedron() {} //!< destructor

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv=false) const; //!< shape functions
    const splitvec_t& getO1splitting() const // override
     { return splitO1Scheme; }
  private:
    // storage of order 1 splitting scheme (intended to graphical softwares usage)
    splitvec_t splitO1Scheme;

}; // end of class LagrangeStdTetrahedron

/*
  \class LagrangeStdTetrahedronPk
  any order LagrangeTetrahedron with general formulae
 */
class LagrangeStdTetrahedronPk: public LagrangeTetrahedron
{
  public:
    Matrix<real_t> shapeFunCoeffs;    //!< matrix representing shape functions coefficients in standard polynomials basis

    LagrangeStdTetrahedronPk(const Interpolation* interp_p); //!< constructor by interpolation
    ~LagrangeStdTetrahedronPk(); //!< destructor
    void initShapeFunCoeffs();   //!< compute shape functions coefficients in basis (1,x,y,x2,xy,y2,...)
    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv=false) const;//!< shape functions
    void monomes(real_t, real_t, real_t, std::vector<real_t>::iterator, number_t=0) const;  //!< evaluate monomes basis at a point (x,y)
    const splitvec_t& getO1splitting() const // override
     { return splitO1Scheme; }
  private:
    // storage of order 1 splitting scheme (intended to graphical softwares usage)
    splitvec_t splitO1Scheme;
}; // end of class LagrangeStdTetrahedronPk


//================================================================================
// Extern class related functions
//================================================================================
void tensorNumberingTetrahedron(const int, number_t**& s2t); //!< correspondence between segment and hexahedron local numbering such that

/*
  - point number p has coordinates defined by
  - ( x = coords[s2t[0][p]], y = coords[s2t[1][p]], z = coords[s2t[3][p]] )
  - where coords are coordinates of the associated 1D Lagrange reference element
 */
void tensorTetrahedronSideNumbering(const int nk, const dimen_t, const dimen_t, const dimen_t, const number_t, const number_t, const number_t, number_t**& s2t, number_t&);

} // end of namespace xlifepp

#endif /* LAGRANGE_TETRAHEDRON_HPP */
