/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file GeomRefTetrahedron.cpp
	\authors D. Martin, N. Kielbasiewicz
	\since 15 dec 2002
	\date 6 aug 2012

  \brief Implementation of xlifepp::GeomRefTetrahedron class members and related functions
 */

#include "GeomRefTetrahedron.hpp"
#include "utils.h"

namespace xlifepp
{

//! GeomRefTetrahedron constructor for Geometric Reference Element using 3d base constructor with shape, volume, centroid coords, number of vertices, edges
GeomRefTetrahedron::GeomRefTetrahedron()
  : GeomRefElement(_tetrahedron, over6_, 0.25, 4, 6)
{
  trace_p->push("GeomRefTetrahedron::GeomRefTetrahedron");
  // coordinates of vertices
  std::vector<real_t>::iterator it_v(vertices_.begin());
  vertex(it_v, 1., 0., 0.);
  vertex(it_v, 0., 1., 0.);
  vertex(it_v, 0., 0., 1.);
  vertex(it_v, 0., 0., 0.);

  // vertex numbering on edges
  sideOfSideNumbering();
  // vertex numbering and oriented edge numbering on faces
  sideNumbering();

  trace_p->pop();
}

//! defines Geometric Reference Element local numbering of edge vertices
void GeomRefTetrahedron::sideOfSideNumbering()
{
  sideOfSideVertexNumbers_[0].push_back(1);
  sideOfSideVertexNumbers_[0].push_back(4);
  sideOfSideVertexNumbers_[1].push_back(2);
  sideOfSideVertexNumbers_[1].push_back(4);
  sideOfSideVertexNumbers_[2].push_back(3);
  sideOfSideVertexNumbers_[2].push_back(4);
  sideOfSideVertexNumbers_[3].push_back(2);
  sideOfSideVertexNumbers_[3].push_back(3);
  sideOfSideVertexNumbers_[4].push_back(1);
  sideOfSideVertexNumbers_[4].push_back(3);
  sideOfSideVertexNumbers_[5].push_back(1);
  sideOfSideVertexNumbers_[5].push_back(2);

  sideOfSideNumbers_[0].push_back(-3);
  sideOfSideNumbers_[0].push_back(-4);
  sideOfSideNumbers_[0].push_back(2);
  sideOfSideNumbers_[1].push_back(-1);
  sideOfSideNumbers_[1].push_back(5);
  sideOfSideNumbers_[1].push_back(3);
  sideOfSideNumbers_[2].push_back(-2);
  sideOfSideNumbers_[2].push_back(-6);
  sideOfSideNumbers_[2].push_back(1);
  sideOfSideNumbers_[3].push_back(6);
  sideOfSideNumbers_[3].push_back(4);
  sideOfSideNumbers_[3].push_back(-5);
}

//! defines Geometric Reference Element local numbering of vertices on faces and of edges on faces
void GeomRefTetrahedron::sideNumbering()
{
  for (number_t i = 0; i < nbSides_; i++)
  {
    sideShapeTypes_[i] = _triangle;
  }
  sideVertexNumbers_[0].push_back(4);
  sideVertexNumbers_[0].push_back(3);
  sideVertexNumbers_[0].push_back(2);
  sideVertexNumbers_[1].push_back(4);
  sideVertexNumbers_[1].push_back(1);
  sideVertexNumbers_[1].push_back(3);
  sideVertexNumbers_[2].push_back(4);
  sideVertexNumbers_[2].push_back(2);
  sideVertexNumbers_[2].push_back(1);
  sideVertexNumbers_[3].push_back(1);
  sideVertexNumbers_[3].push_back(2);
  sideVertexNumbers_[3].push_back(3);
}
//
/*
--------------------------------------------------------------------------------
   returns edge length, face area or element volume
--------------------------------------------------------------------------------
*/
real_t GeomRefTetrahedron::measure(const dimen_t d, const number_t sideNum) const
{
  real_t ms(1.);
  switch ( d )
  {
    case 1:
      switch (sideNum)
      {
        case 1: case 2: case 3: ms = 1.; break;
        case 4: case 5: case 6: ms = sqrtOf2_; break;
        default: noSuchSideOfSide(sideNum);
      }
      break;
    case 2:
      switch (sideNum)
      {
        case 1: case 2: case 3: ms = 0.5; break;
        case 4: ms = 0.5 * sqrtOf3_; break;
        default: noSuchSide(sideNum);
      }
      break;
    case 3:  ms = measure_; break;
    default: break;
  }
  return ms;
}

//! returns a quadrature rule built on an edge from a 1d quadrature formula or a rule built on an face from a triangle or quadrangle quadrature formula
//void GeomRefTetrahedron::sideQuadrature(const QuadratureRule& q1, QuadratureRule& qr, const number_t sideNum, const dimen_t d) const
//{
//   /*
//      q1 : input 1d or 2d (triangle) quadrature rule
//      q2 : ouput 3d quadrature rule set on the edge or face
//      sideNum: local number of edge (sideNum = 1,2,3,4,5,6) or face (sideNum = 1,2,3,4)
//      d: side dimension (d = 1 for an edge, d = 2 for a face)
//   */
//   std::vector<real_t>::const_iterator c_1(q1.point()), w_1(q1.weight());
//   std::vector<real_t>::iterator c_i(qr.point()), w_i(qr.weight());
//
//   switch ( d )
//   {
//      case 1:
//         switch ( sideNum )
//         {
//            case 1: // edge (x2 = 0 ; x3 = 0)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, 0., 0., w_i, *w_1 );
//               break;
//            case 2: // edge (x1 = 0 ; x3 = 0)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 0., *c_1, 0., w_i, *w_1 );
//               break;
//            case 3: // edge (x1 = 0 ; x2 = 0)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 0., 0., *c_1, w_i, *w_1 );
//               break;
//            case 4: // edge (x1 = 0 ; x2 + x3 = 1)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 0., *c_1, 1.-*c_1, w_i, *w_1 * sqrt_of_2_);
//               break;
//            case 5: // edge (x2 = 0 ; x1 + x3 = 1)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, 0., 1.-*c_1, w_i, *w_1 * sqrt_of_2_);
//               break;
//            case 6: // edge (x3 = 0 x1 + x2 = 1)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, 1.-*c_1, 0., w_i, *w_1 * sqrt_of_2_);
//               break;
//             default: noSuchEdge(sideNum); break;
//         }
//         break;
//      case 2:
//         switch ( sideNum )
//         {
//            case 1: // face (x1 = 0)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 0., *c_1, *(c_1+1), w_i, *w_1);
//               break;
//            case 2: // face (x2 = 0)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, 0., *(c_1+1), w_i, *w_1);
//               break;
//            case 3: // face (x3 = 0)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, *(c_1+1), 0., w_i, *w_1);
//               break;
//            case 4: // face (x1 + x2 + x3 = 1)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, *(c_1+1), 1-*c_1-*(c_1+1), w_i, *w_1 * sqrt_of_3_);
//               break;
//            default: noSuchFace(sideNum); break;
//         }
//         break;
//      default: break;
//   }
//}
//
//! returns tangent std::vector(s) on edge sideNum (=1,2,...,6) or face sideNum (=1,2,3,4)
void GeomRefTetrahedron::tangentVector(const std::vector<real_t>& jacobianMatrix, std::vector<std::vector<real_t> >& tgv, const number_t sideNum, const dimen_t d) const
{
  std::vector<real_t>::iterator tv0_i(tgv[0].begin()), tv1_i(tv0_i);
  if ( d > 1) { tv1_i = tgv[1].begin(); }
//  std::vector<real_t>::const_iterator it_jm(jacobianMatrix.begin());

  switch ( d )
  {
    case 2:
      noSuchFunction("tangentVector (on faces)");
      switch ( sideNum )
      {
        case 1: // face (x1 = 0)               : 4 -> 3 -> 2
          break;
        case 2: // face (x2 = 0)               : 4 -> 1 -> 3
          break;
        case 3: // face (x3 = 0)               : 4 -> 2 -> 1
          break;
        case 4: // face (x1 + x2 + x3 = 1)     : 1 -> 2 -> 3
          break;
        default: noSuchSide(sideNum); break;
      }
      //			break;
    case 1:
      noSuchFunction("tangentVector (on edges)");
      switch ( sideNum )
      {
        case 1: // edge (x2 = 0 ; x3 = 0)      : 1 -> 4
          break;
        case 2: // edge (x3 = 0 ; x1 = 0)      : 2 -> 4
          break;
        case 3: // edge (x1 = 0 ; x2 = 0)      : 3 -> 4
          break;
        case 4: // edge (x1 = 0 ; x2 + x3 = 1) : 2 -> 3
          break;
        case 5: // edge (x2 = 0 ; x3 + x3 = 1) : 1 -> 3
          break;
        case 6: // edge (x3 = 0 ; x1 + x2 = 1) : 1 -> 2
          break;
        default: noSuchSideOfSide(sideNum); break;
      }
      break;
    default:
      break;
  }
}

//! Returns the local number of a vertex opposed to an edge given by its local number
number_t GeomRefTetrahedron::vertexOppositeSide(const number_t sideNum) const
{
  // here sideNum is the local number of an edge (sideNum = 1,2,3,4 modulo 4)
  return 1 + (sideNum - 1) % 4;
}

//! Returns the local number of a face opposed to a vertex given by local number
number_t GeomRefTetrahedron::sideOppositeVertex(const number_t vNum) const
{
  // here vNum is the local number of a vertex (vNum = 1,2,3,4 modulo 4)
  return 1 + (vNum - 1) % 4;
}

//! Returns the local number of an edge opposed to a edge given by local number
number_t GeomRefTetrahedron::edgeOppositeEdge(const number_t ed_no) const
{
  if ( ed_no < 4 ) { return ed_no + 3; }
  else { return ed_no - 3; }
}

//! Returns the local number of an edge bearing 2 vertices given by their local numbers or the local number of  a face bearing 3 vertices given by their local numbers
number_t GeomRefTetrahedron::sideWithVertices(const number_t vNum1, const number_t vNum2) const
{
  number_t ed_no(0);
  //                                   vNum1=1        vNum1=2      vNum1=3      vNum1=4
  static const number_t ed[4][4] = { {0, 6, 5, 1}, {6, 0, 4, 2}, {5, 4, 0, 3}, {1, 2, 3, 0} };
  ed_no = ed[(vNum1 - 1) % 4][(vNum2 - 1) % 4];
  if ( ed_no == 0 ) { noSuchSide(vNum1, vNum2); }
  return ed_no;
}

number_t GeomRefTetrahedron::sideWithVertices(const number_t vNum1, const number_t vNum2, const number_t vNum3, const number_t vNum4) const
{
  // here vNum_i is the local number of a vertex (vNum_i = 1,2,3,4  modulo 4)
  // Face 1 with vertices {2,3,4} satisfies 10 - vNum1 - vNum2 - vNum3 = 1
  //      2               {1,3,4}           10 - vNum1 - vNum2 - vNum3 = 2
  //      3               {1,2,4}           10 - vNum1 - vNum2 - vNum3 = 3
  //      4               {1,2,3}           10 - vNum1 - vNum2 - vNum3 = 4
  return static_cast<number_t>( 7 - (vNum1 - 1) % 4 - (vNum2 - 1) % 4 - (vNum3 - 1) % 4 );
}

//! test if a point belongs to current tetrahedron
bool GeomRefTetrahedron::contains(std::vector<real_t>& p, real_t tol) const
{
    real_t x=p[0], y=p[1], z=p[2], xyz=x+y+z, tm=-tol, tp1=tol+1.;
//    return (x >= -tol) && (x <= 1.+tol) && (y >= -tol) && (y <= 1.+tol) && (z >= -tol) && (z <= 1.+tol)
//          && (x+y+z<= 1.+tol) ;
    return (x >= tm) && (x <= tp1) && (y >= tm) && (y <= tp1) && (z >= tm) && (z <= tp1) && (xyz >= tm) && (xyz <= tp1);
}

//! return projection on tetahedron and distance to tetahedron (h)
std::vector<real_t> GeomRefTetrahedron::projection(const std::vector<real_t>& p, real_t& h) const
{
    return projectionOnTetrahedron(p,Point(0.,0.,0.),Point(1.,0.,0.),Point(0.,1.,0.), Point(0.,0.,1.), h);
}

} // end of namespace xlifepp
