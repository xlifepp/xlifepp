/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file NedelecFaceTetrahedron.hpp
  \authors E. Lunéville
  \since 15 september 2015
  \date  15 september 2015

  \brief Definition of the xlifepp::NedelecFaceTetrahedron class

  xlifepp::NedelecFaceTetrahedron defines Nedelec HDiv-conforming face elements interpolation data on tetrahedron elements
  There are 2 families:
  - the first family (NF1k) is the extension to tetrahedron of the Raviart-Thomas element  defined on a triangle (RTk)
  - the second family (NF2k) is the extension to tetrahedron of the Brezzi-Douglas-Marini element defined on a triangle (BDMk)

   member functions
  ----------------
    - interp: interpolation data
    - sideNumbering: local numbering on edges (side)
    - sideDofsMap: side dofs mapping used when assembling
    - computeShapeFunctions: compute shape functions as polynomials (only for any order family)
    - computeShapeValues: evaluate shape functions at a point

  Child classes:
    - xlifepp::NedelecFaceFirstTetrahedronPk -> Nedelec first family of any order on tetrahedron (NF1k)
    - xlifepp::NedelecFaceSecondTetrahedronPk -> Nedelec second family of any order on tetrahedron (NF2k)
*/

#ifndef NEDELEC_FACE_TETRAHEDRON_HPP
#define NEDELEC_FACE_TETRAHEDRON_HPP

#include "config.h"
#include "RefTetrahedron.hpp"

namespace xlifepp
{
  class FeDof; //forward class

/*!
  \class NedelecFaceTetrahedron
  (Hdiv-conforming face elements )

  Parent class: RefTriangle
  Child classes: NedelecFaceFirstTetrahedronPk, NedelecFaceSecondTetrahedronPk
 */
class NedelecFaceTetrahedron: public RefTetrahedron
{
  public:
    NedelecFaceTetrahedron(const Interpolation* interp_p);
    virtual ~NedelecFaceTetrahedron();
}; // end of class NedelecFaceTetrahedron

/*!
  \class NedelecFaceFirstTetrahedronPk
  Nedelec face first family of any order k on tetrahedron t (NF1k)
     space  Vk: P^3_(k-1) + PH_(k-1)*(x1,x2,x3),       dim Vk = k(k+1)(k+3)/2
     face dofs: v-> int_f v.n q,  q in P_(k-1)[f]      k(k+1)/2 dofs by face f
     terrahedron dofs: v-> int_t v.q,    q in P_(k-2)[t]^3    k(k-1)(k+1)/2 dofs  only for k>1
 */
class NedelecFaceFirstTetrahedronPk: public NedelecFaceTetrahedron
{
  public:
    NedelecFaceFirstTetrahedronPk(const Interpolation* int_p);
    ~NedelecFaceFirstTetrahedronPk();
    void interpolationData();  //!< defines reference element interpolation data
    void sideNumbering();      //!< local numbering on edges (side)
    void pointCoordinates();   //!< builds virtual coordinates of moment dofs
    number_t sideDofsMap(const number_t& n, const number_t& i,
                         const number_t& j, const number_t& k=0) const;  //!< side dofs mapping
    void computeShapeFunctions();                                        //!< compute shape functions as polynomials

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv=false) const; //!< compute shape values at a point
    virtual Value evalFaceDof(const FeDof& dof, const GeomElement& selt, const Function& f, const Function& gradf, const Function& grad2f) const; //!< specific eval face dof function
  private:
    number_t nbshfcts_;
    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class NedelecFaceFirstTetrahedronPk

/*!
  \class NedelecFaceSecondTetrahedronPk
  Nedelec face second family of any order k on tetrahedron t (NF2k)
     space  Vk: P^3_(k)                            dim Vk = (k+1)(k+2)(k+3)/2
     face dofs: v-> int_f v.n q, q in P_(k)[f]     (k+1)(k+2)/2   dofs by face f
     tetrahedron dofs: v-> int_t v.q,   q in R_(k-1)[t]   (k-1)(k+1)(k+2)/2 dofs  only for k > 1
                        where R_(k-1)[t] = P^3_(k-2) + S(k-1)
                              S_(k-1)[t] = {p in PH_(k-1), x.p=0}
 */
class NedelecFaceSecondTetrahedronPk: public NedelecFaceTetrahedron
{
  public:
    NedelecFaceSecondTetrahedronPk(const Interpolation* int_p);
    ~NedelecFaceSecondTetrahedronPk();
//    void interpolationData();  //!< defines reference element interpolation data
//    void sideNumbering();      //!< local numbering on edges (side)
//    void pointCoordinates();   //!< builds virtual coordinates of moment dofs
//    number_t sideDofsMap(const number_t& n, const number_t& i,
//                         const number_t& j, const number_t& k=0) const;  //!< side dofs mapping
//    void computeShapeFunctions();                                        //!< compute shape functions as polynomials
//
//    void computeShapeValues(std::vector<real_t>::const_iterator it_pt,
//                            const bool withDeriv = true) const;          //!< compute shape values at a point
//  private:
//    number_t nbshfcts_;
//
//    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class NedelecFaceSecondTetrahedronPk


} // end of namespace xlifepp

#endif /* NEDELEC_FACE_TETRAHEDRON_HPP */
