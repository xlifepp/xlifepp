/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RefDof.hpp
  \authors D. Martin, N. Kielbasiewicz, E. Lunéville
  \since 20 jun 2004
  \date 12 feb 2015

  \brief Definition of the xlifepp::RefDof class

  A xlifepp::RefDof object represents a generalized Degreee of Freedom in a reference element, managing

    bool sharable_:  implies D.o.F is shared between adjacent elements according to space conformity
                      such a D.o.F can be shared or not according to interpolation;

    dimen_t supportDim_: dimension of the D.o.F geometric support
                           - 0 when D.o.F support is a point (in the ReferenceElement)
                           - dim-2 when D.o.F support is a 'dim-2' side (1: edge)
                           - dim-1 when D.o.F support is a 'dim-1' side (1: edge, 2: face)
                           - dim when D.o.F support is the whole element
                           NOTE: in future, use SupportType enum instead of supportDim_

    number_t supportNum_: support localisation
                            - vertex number for a vertex dof
                            - edge number in the ReferenceElement when support dof is in a edge and is not a vertex
                            - face number in the ReferenceElement when support dof is in a face and is not on an edge
                            - 0 when support is the whole element
                            NOTE: meaning has slightly changed with the introduction of where_ and index_ data, see below)

    DofLocalization where_: hierachic localization of dof
                              _noWhere: undefined localization
                              _onVertex: mandatory a point dof localized at a vertex
                              _onEdge: dof with support on edge BUT not on edge vertices
                              _onFace: dof with support on face BUT not on face edges (only in 3d)
                              _onElement: dof with support in element BUT no on its faces

    number_t index_: rank of the dof in its localization
                       - for vertex: rank in all dofs on a vertex
                       - for edge: rank in all dofs on an edge (not counted dofs on its vertices)
                       - for face: rank in all dofs on a face (not counted dofs on its edges, vertices)
                       - for element: rank in all dofs in element (not counted dofs on its faces, edges, vertices)

    dimen_t dim_:  number of components of shape functions of D.o.F (vector dof)

    vector<real_t> coords_: coordinates of D.o.F support when supportDim_ = 0

    number_t order_: order of a derivative D.o.F or a moment D.o.F
                      - 0 for an D.o.F defined by an 'integral' over its support (such as a nodal value, an integral over a side, the whole element)
                      - n for a Hermite D.o.F derivative of order n > 0 (supportDim_ = 0) or a moment D.o.F of order n > 0 (supportDim_ > 0)
    vector<real_t> derivativeVector_: direction vector(s) of a derivative D.o.F

    ProjectionType projectionType_:  _noProjection
                                      _givenProjection  for a unit vector given in projectionVector_
                                      _dotnProjection   for a 'u.n' D.o.F ( n is the unit external normal vector on side )
                                      _crossnProjection for a 'uxn' D.o.F (tangential projection)
    vector<real_t> projectionVector_: direction vector(s) of a projection D.o.F

    string_t name_: D.o.F-type name for documentation purpose

    NOTE: hierarchic localization (where_ and index_) have been introduced later to have a more efficient algorithm
           to build global dofs (see FeSpace::buildFeDofs)

    Example of RefDof
                                |sharable|supportDim|  where   |supportNum|index|dim| coords |order|
      P3 triangle      ----------------------------------------------------------------------------
                       refdof 1 |  true  |    0     | onVertex |     1    |  1  | 1 | (1,0)  |  0  |
       2               refdof 2 |  true  |    0     | onVertex |     2    |  1  | 1 | (0,1)  |  0  |
       | \             refdof 3 |  true  |    0     | onVertex |     3    |  1  | 1 | (0,0)  |  0  |
       5   7           refdof 4 |  true  |    0     |  onEdge  |     1    |  1  | 1 | (2,1)/3|  0  |
    e2 |     \  e1     refdof 5 |  true  |    0     |  onEdge  |     2    |  1  | 1 | (0,2)/3|  0  |
       8  10   4       refdof 6 |  true  |    0     |  onEdge  |     3    |  1  | 1 | (1,0)/3|  0  |
       |         \     refdof 7 |  true  |    0     |  onEdge  |     1    |  2  | 1 | (1,2)/3|  0  |
       3---6---9---1   refdof 8 |  true  |    0     |  onEdge  |     2    |  2  | 1 | (0,1)/3|  0  |
            e3         refdof 9 |  true  |    0     |  onEdge  |     3    |  2  | 1 | (2,0)/3|  0  |
                       refdof 10|   no   |    0     | onElement|     0    |  1  | 1 | (1,1)/3|  0  |
 */

#ifndef REF_DOF_HPP
#define REF_DOF_HPP

#include "config.h"
#include "utils.h"

#include <fstream>

namespace xlifepp
{

enum DofLocalization {_nowhere =0, _onVertex, _onEdge , _onFace, _onElement};
//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const DofLocalization& dl);
enum ProjectionType {_noProjection =0, _givenProjection, _dotnProjection, _crossnProjection};
//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const ProjectionType& pt);

class RefElement;   //forward class

/*!
  \class RefDof
  A RefDof object represents a generalized Degree of Freedom in a reference element
 */
class RefDof
{
  private:

    RefElement* refElt_p;                 //!< pointer to refElement related to current refdof (0 if none)
    bool sharable_;                       //!< implies D.o.F is shared between adjacent elements according to space conformity
    DofLocalization where_;               //!< hierarchic localization of dof
    number_t supportNum_;                 //!< support localization
    number_t index_;                      //!< rank of the dof in its localization
    dimen_t supportDim_;                  //!< dimension of the D.o.F geometric support (in future, use SupportType enum instead of supportDim_)
    number_t nodeNum_;                    //!< node number when a point dof
    dimen_t dim_;                         //!< number of components of shape functions of D.o.F
    std::vector<real_t> coords_;          //!< coordinates of D.o.F support when supportDim_ = 0
    number_t order_;                      //!< order of a derivative D.o.F or a moment D.o.F
    std::vector<real_t> derivativeVector_;//!< direction vector(s) of a derivative D.o.F
    ProjectionType projectionType_;       //!< type of projection
    std::vector<real_t> projectionVector_;//!< direction vector(s) of a projection D.o.F
    string_t name_;                       //!< D.o.F-type name for documentation purpose
    DiffOpType diffop_;                   //!< differential operator (_id,_dx,_dy, ...)

    //--------------------------------------------------------------------------------
    // Constructors, Destructor
    //--------------------------------------------------------------------------------
  public:
    //! constructor
    RefDof(RefElement* relt, bool sh, DofLocalization w, number_t supNum, number_t ind, dimen_t e_dim, dimen_t s_dim,
           number_t nodeNum=0, dimen_t di = 1, number_t ord = 0, ProjectionType proj_t = _noProjection,
           DiffOpType dop=_id, const string_t& nm = "nodal value");
    ~RefDof(); //!< destructor

    //--------------------------------------------------------------------------------
    // Public Accessors
    //--------------------------------------------------------------------------------
    //! return RefElement pointer
    RefElement* refElement() const{return refElt_p;}
    //! return DoF sharability
    bool sharable() const { return sharable_; }
    //! return dimension of DoF support
    dimen_t supportDim() const { return supportDim_; }
    //! get DoF localization
    DofLocalization where() const { return where_;}
    //! return DoF localization support (vertex number, edge number, face number)
    number_t supportNumber() const { return supportNum_; }
    //! get rank of dof in set of dofs on a support
    number_t index() const { return index_;}
    //! get rank of a point dof in list of all point dofs
    number_t nodeNumber() const {return nodeNum_;}
    //! get DoF dimension
    dimen_t dim() const { return dim_; }
    //! get DoF order of derivation
    number_t order() const { return order_; }
    //! returns type of projection
    ProjectionType typeOfProjection() const { return projectionType_; }
    //! returns name
    string_t name() const { return name_; }
    std::vector<real_t>& derivativeVector(){return derivativeVector_;}
    const std::vector<real_t>& derivativeVector() const {return derivativeVector_;}
    std::vector<real_t>& projectionVector(){return projectionVector_;}
    const std::vector<real_t>& projectionVector() const {return projectionVector_;}
    DiffOpType& diffop(){return diffop_;}
    const DiffOpType& diffop()const{return diffop_;}

    //@{
    //! return iterator to vector of coordinates
    std::vector<real_t>::const_iterator coords() { return coords_.begin(); }
    std::vector<real_t>::const_iterator coords() const { return coords_.begin(); }
    //@}
    //! returns vector of coordinates
    std::vector<real_t> point() const { return coords_; }
    //! set 1D coordinate
    void coords(const real_t);
    //! set 2D coordinates
    void coords(const real_t, const real_t);
    //! set 3D coordinates
    void coords(const real_t, const real_t, const real_t);
    //! set D.o.F coordinates
    void coords(const std::vector<real_t>&);

    friend std::ostream& operator<<(std::ostream&, const RefDof&);
    friend class RefElement;

  private:
    // copy of a Reference D.o.F object is forbidden
    RefDof(const RefDof&);  //!< no copy constructor
    void operator=(const RefDof&); //!< no assignment operator=

}; // end class RefDof

std::ostream& operator<<(std::ostream&, const RefDof&); //!< print operator

/*!
  \class DofKey
  The DofKey class is useful class to index dofs (see for instance FeSpace::buildFeDofs )
 */
class DofKey
{
  public:
    DofLocalization where;
    number_t v1, v2;           //used for vertex, edge, element dofs
    std::vector<number_t> vs;  //used for face dofs
    number_t locIndex;
    DofKey() : where(_nowhere), v1(0), v2(0), locIndex(0) {}
    DofKey(DofLocalization w, number_t n, number_t i) : where(w), v1(n), v2(0), locIndex(i) {}
    DofKey(DofLocalization w, number_t n1, number_t n2, number_t i) : where(w), v1(n1), v2(n2), locIndex(i) {}
    DofKey(DofLocalization w, std::vector<number_t>& ns, number_t i) : where(w), v1(0), v2(0), vs(ns),locIndex(i) {}
};


inline bool operator<(const DofKey& k1, const DofKey& k2) {
  DofLocalization w1=k1.where, w2=k2.where;
  if(w1 < w2) return true;
  if(w1 > w2) return false;
  switch(w1) {
    case _onVertex:  // test on vertex number
      if (k1.v1 != k2.v1) return (k1.v1 < k2.v1);
      break;
    case _onElement: // first test on element number
      if (k1.v1 != k2.v1) return (k1.v1 < k2.v1);
      break;
    case _onEdge:
      if(k1.v1 < k2.v1) return true;
      if(k1.v1 > k2.v1) return false;
      if(k1.v2 < k2.v2) return true;
      if(k1.v2 > k2.v2) return false;
      break;
    case _onFace:
      for(std::vector<number_t>::const_iterator it1=k1.vs.begin(), it2=k2.vs.begin();
          it1!=k1.vs.end() && it2!=k2.vs.end(); ++it1, ++it2)
      {
        if(*it1 < *it2) return true;
        if(*it1 > *it2) return false;
      }
      break;
    default: error("not_handled","operator<(DofKey,DofKey)");
      break;
  }
  return (k1.locIndex < k2.locIndex);
}

inline std::ostream& operator<<(std::ostream& out, const DofKey& dk)
{
  out<<words("dof location",dk.where);
  if(dk.where==_onVertex || dk.where==_onElement) out<<" "<<dk.v1;
  if(dk.where==_onEdge) out<<" ["<<dk.v1<<" "<<dk.v2<<"]";
  if(dk.where==_onFace)
  {
    out<<" ["<<dk.vs[0];
    for(number_t k=1;k<dk.vs.size(); k++) out<<", "<<dk.vs[k];
    out<<"]";
  }
  out<<" index "<<dk.locIndex;
  return out;
}

} // end of namespace xlifepp

#endif /* REF_DOF_HPP */
