/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file splitUtils.cpp
  \author N. Kielbasiewicz
  \since 11 jun 2013
  \date 12 jun 2013

  \brief Implementation of splitting functions
 */

#include "splitUtils.hpp"
#include "utils.h"

namespace xlifepp
{

//! return nodes numbers of first order tetrahedron elements when splitting first order hexahedron
std::vector<std::vector<number_t> > splitHexahedronQ1ToTetrahedraP1(std::vector<number_t> nodeNumbers)
{
  if (nodeNumbers.size() != 8) error("bad_size","nodeNumbers",8,nodeNumbers.size());
  std::vector<std::vector<number_t> > splitNumbers;
  std::vector<number_t> num(4, 0);
  
  num[0]=nodeNumbers[0]; num[1]=nodeNumbers[1]; num[2]=nodeNumbers[5]; num[3]=nodeNumbers[3];
  splitNumbers.push_back(num);
  num[0]=nodeNumbers[1]; num[1]=nodeNumbers[2]; num[2]=nodeNumbers[5]; num[3]=nodeNumbers[3];
  splitNumbers.push_back(num);
  num[0]=nodeNumbers[2]; num[1]=nodeNumbers[3]; num[2]=nodeNumbers[5]; num[3]=nodeNumbers[6];
  splitNumbers.push_back(num);
  num[0]=nodeNumbers[0]; num[1]=nodeNumbers[3]; num[2]=nodeNumbers[5]; num[3]=nodeNumbers[4];
  splitNumbers.push_back(num);
  num[0]=nodeNumbers[3]; num[1]=nodeNumbers[5]; num[2]=nodeNumbers[7]; num[3]=nodeNumbers[4];
  splitNumbers.push_back(num);
  num[0]=nodeNumbers[6]; num[1]=nodeNumbers[7]; num[2]=nodeNumbers[5]; num[3]=nodeNumbers[3];
  splitNumbers.push_back(num);
  return splitNumbers;
}

//! return nodes numbers of first order tetrahedron elements when splitting second order hexahedron
std::vector<std::vector<number_t> > splitHexahedronQ2ToTetrahedraP1(std::vector<number_t> nodeNumbers)
{
  if (nodeNumbers.size() != 27) error("bad_size","nodeNumbers",27,nodeNumbers.size());
  std::vector<std::vector<number_t> > splitNumbers, buf;
  std::vector<number_t> num(8, 0);
  
  // Q1 sub-hexahedron 1 20 26 13 15 21 27 25
  num[0]=nodeNumbers[0]; num[1]=nodeNumbers[19]; num[2]=nodeNumbers[25]; num[3]=nodeNumbers[12];
  num[4]=nodeNumbers[14]; num[5]=nodeNumbers[20]; num[6]=nodeNumbers[26]; num[7]=nodeNumbers[24];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 20 2 16 26 21 9 22 27
  num[0]=nodeNumbers[19]; num[1]=nodeNumbers[1]; num[2]=nodeNumbers[15]; num[3]=nodeNumbers[25];
  num[4]=nodeNumbers[20]; num[5]=nodeNumbers[8]; num[6]=nodeNumbers[21]; num[7]=nodeNumbers[26];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 13 26 14 4 25 27 6 12
  num[0]=nodeNumbers[12]; num[1]=nodeNumbers[25]; num[2]=nodeNumbers[13]; num[3]=nodeNumbers[3];
  num[4]=nodeNumbers[24]; num[5]=nodeNumbers[26]; num[6]=nodeNumbers[5]; num[7]=nodeNumbers[11];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 26 16 3 14 27 22 18 6
  num[0]=nodeNumbers[25]; num[1]=nodeNumbers[15]; num[2]=nodeNumbers[2]; num[3]=nodeNumbers[13];
  num[4]=nodeNumbers[26]; num[5]=nodeNumbers[21]; num[6]=nodeNumbers[17]; num[7]=nodeNumbers[5];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 15 21 27 25 5 11 23 19
  num[0]=nodeNumbers[14]; num[1]=nodeNumbers[20]; num[2]=nodeNumbers[26]; num[3]=nodeNumbers[24];
  num[4]=nodeNumbers[4]; num[5]=nodeNumbers[10]; num[6]=nodeNumbers[22]; num[7]=nodeNumbers[18];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 21 9 22 27 11 6 10 23
  num[0]=nodeNumbers[20]; num[1]=nodeNumbers[8]; num[2]=nodeNumbers[21]; num[3]=nodeNumbers[26];
  num[4]=nodeNumbers[10]; num[5]=nodeNumbers[5]; num[6]=nodeNumbers[9]; num[7]=nodeNumbers[22];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 25 27 6 12 19 23 17 8
  num[0]=nodeNumbers[24]; num[1]=nodeNumbers[26]; num[2]=nodeNumbers[5]; num[3]=nodeNumbers[11];
  num[4]=nodeNumbers[18]; num[5]=nodeNumbers[22]; num[6]=nodeNumbers[16]; num[7]=nodeNumbers[7];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 27 22 18 6 23 10 7 17
  num[0]=nodeNumbers[26]; num[1]=nodeNumbers[21]; num[2]=nodeNumbers[17]; num[3]=nodeNumbers[5];
  num[4]=nodeNumbers[22]; num[5]=nodeNumbers[9]; num[6]=nodeNumbers[6]; num[7]=nodeNumbers[16];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  return splitNumbers;
}

//! return nodes numbers of first order tetrahedron elements when splitting third order hexahedron
std::vector<std::vector<number_t> > splitHexahedronQ3ToTetrahedraP1(std::vector<number_t> nodeNumbers)
{
  if (nodeNumbers.size() != 64) error("bad_size","nodeNumbers",64,nodeNumbers.size());
  std::vector<std::vector<number_t> > splitNumbers, buf;
  std::vector<number_t> num(8, 0);
  // Q1 sub-hexahedron 1 20 53 13 15 36 57 51
  num[0]=nodeNumbers[0]; num[1]=nodeNumbers[19]; num[2]=nodeNumbers[52]; num[3]=nodeNumbers[12];
  num[4]=nodeNumbers[14]; num[5]=nodeNumbers[35]; num[6]=nodeNumbers[56]; num[7]=nodeNumbers[50];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 13 53 54 25 51 57 60 50
  num[0]=nodeNumbers[12]; num[1]=nodeNumbers[52]; num[2]=nodeNumbers[53]; num[3]=nodeNumbers[24];
  num[4]=nodeNumbers[50]; num[5]=nodeNumbers[56]; num[6]=nodeNumbers[59]; num[7]=nodeNumbers[49];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 25 54 26 4 50 60 46 24
  num[0]=nodeNumbers[24]; num[1]=nodeNumbers[53]; num[2]=nodeNumbers[25]; num[3]=nodeNumbers[3];
  num[4]=nodeNumbers[49]; num[5]=nodeNumbers[59]; num[6]=nodeNumbers[45]; num[7]=nodeNumbers[23];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 20 32 56 53 36 33 58 57
  num[0]=nodeNumbers[19]; num[1]=nodeNumbers[31]; num[2]=nodeNumbers[55]; num[3]=nodeNumbers[52];
  num[4]=nodeNumbers[35]; num[5]=nodeNumbers[32]; num[6]=nodeNumbers[57]; num[7]=nodeNumbers[56];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 53 56 55 54 57 58 59 60
  num[0]=nodeNumbers[52]; num[1]=nodeNumbers[55]; num[2]=nodeNumbers[54]; num[3]=nodeNumbers[53];
  num[4]=nodeNumbers[56]; num[5]=nodeNumbers[57]; num[6]=nodeNumbers[58]; num[7]=nodeNumbers[59];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 54 55 14 26 60 59 45 46
  num[0]=nodeNumbers[53]; num[1]=nodeNumbers[54]; num[2]=nodeNumbers[13]; num[3]=nodeNumbers[25];
  num[4]=nodeNumbers[59]; num[5]=nodeNumbers[58]; num[6]=nodeNumbers[44]; num[7]=nodeNumbers[45];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 32 2 16 56 33 9 39 58
  num[0]=nodeNumbers[31]; num[1]=nodeNumbers[1]; num[2]=nodeNumbers[15]; num[3]=nodeNumbers[55];
  num[4]=nodeNumbers[32]; num[5]=nodeNumbers[8]; num[6]=nodeNumbers[38]; num[7]=nodeNumbers[57];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 56 16 28 55 58 39 40 59
  num[0]=nodeNumbers[55]; num[1]=nodeNumbers[15]; num[2]=nodeNumbers[27]; num[3]=nodeNumbers[54];
  num[4]=nodeNumbers[57]; num[5]=nodeNumbers[38]; num[6]=nodeNumbers[39]; num[7]=nodeNumbers[58];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 55 28 3 14 59 40 18 45
  num[0]=nodeNumbers[54]; num[1]=nodeNumbers[27]; num[2]=nodeNumbers[2]; num[3]=nodeNumbers[13];
  num[4]=nodeNumbers[58]; num[5]=nodeNumbers[39]; num[6]=nodeNumbers[17]; num[7]=nodeNumbers[44];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 15 36 57 51 27 35 61 52
  num[0]=nodeNumbers[14]; num[1]=nodeNumbers[35]; num[2]=nodeNumbers[56]; num[3]=nodeNumbers[50];
  num[4]=nodeNumbers[26]; num[5]=nodeNumbers[34]; num[6]=nodeNumbers[60]; num[7]=nodeNumbers[51];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 51 57 60 50 52 61 64 49
  num[0]=nodeNumbers[50]; num[1]=nodeNumbers[56]; num[2]=nodeNumbers[59]; num[3]=nodeNumbers[49];
  num[4]=nodeNumbers[51]; num[5]=nodeNumbers[60]; num[6]=nodeNumbers[63]; num[7]=nodeNumbers[48];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 50 60 46 24 49 64 47 12
  num[0]=nodeNumbers[49]; num[1]=nodeNumbers[59]; num[2]=nodeNumbers[45]; num[3]=nodeNumbers[23];
  num[4]=nodeNumbers[48]; num[5]=nodeNumbers[63]; num[6]=nodeNumbers[46]; num[7]=nodeNumbers[11];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 36 33 58 57 35 34 62 61
  num[0]=nodeNumbers[35]; num[1]=nodeNumbers[32]; num[2]=nodeNumbers[57]; num[3]=nodeNumbers[56];
  num[4]=nodeNumbers[34]; num[5]=nodeNumbers[33]; num[6]=nodeNumbers[61]; num[7]=nodeNumbers[60];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 57 58 59 60 61 62 63 64
  num[0]=nodeNumbers[56]; num[1]=nodeNumbers[57]; num[2]=nodeNumbers[58]; num[3]=nodeNumbers[59];
  num[4]=nodeNumbers[60]; num[5]=nodeNumbers[61]; num[6]=nodeNumbers[62]; num[7]=nodeNumbers[63];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 60 59 45 46 64 63 48 47
  num[0]=nodeNumbers[59]; num[1]=nodeNumbers[58]; num[2]=nodeNumbers[44]; num[3]=nodeNumbers[45];
  num[4]=nodeNumbers[63]; num[5]=nodeNumbers[62]; num[6]=nodeNumbers[47]; num[7]=nodeNumbers[46];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 33 9 39 58 34 21 38 62
  num[0]=nodeNumbers[32]; num[1]=nodeNumbers[8]; num[2]=nodeNumbers[38]; num[3]=nodeNumbers[57];
  num[4]=nodeNumbers[33]; num[5]=nodeNumbers[20]; num[6]=nodeNumbers[37]; num[7]=nodeNumbers[61];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 58 39 40 59 62 38 37 63
  num[0]=nodeNumbers[57]; num[1]=nodeNumbers[38]; num[2]=nodeNumbers[39]; num[3]=nodeNumbers[58];
  num[4]=nodeNumbers[61]; num[5]=nodeNumbers[37]; num[6]=nodeNumbers[36]; num[7]=nodeNumbers[62];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 59 40 18 45 63 37 30 48
  num[0]=nodeNumbers[58]; num[1]=nodeNumbers[39]; num[2]=nodeNumbers[17]; num[3]=nodeNumbers[44];
  num[4]=nodeNumbers[62]; num[5]=nodeNumbers[36]; num[6]=nodeNumbers[29]; num[7]=nodeNumbers[47];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 27 35 61 52 5 11 41 19
  num[0]=nodeNumbers[26]; num[1]=nodeNumbers[34]; num[2]=nodeNumbers[60]; num[3]=nodeNumbers[51];
  num[4]=nodeNumbers[4]; num[5]=nodeNumbers[10]; num[6]=nodeNumbers[40]; num[7]=nodeNumbers[18];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 52 61 64 49 19 41 44 31
  num[0]=nodeNumbers[51]; num[1]=nodeNumbers[60]; num[2]=nodeNumbers[63]; num[3]=nodeNumbers[48];
  num[4]=nodeNumbers[18]; num[5]=nodeNumbers[40]; num[6]=nodeNumbers[43]; num[7]=nodeNumbers[30];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 49 64 47 12 31 44 29 8
  num[0]=nodeNumbers[48]; num[1]=nodeNumbers[63]; num[2]=nodeNumbers[46]; num[3]=nodeNumbers[11];
  num[4]=nodeNumbers[30]; num[5]=nodeNumbers[43]; num[6]=nodeNumbers[28]; num[7]=nodeNumbers[7];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 35 34 62 61 11 23 42 41
  num[0]=nodeNumbers[34]; num[1]=nodeNumbers[33]; num[2]=nodeNumbers[61]; num[3]=nodeNumbers[60];
  num[4]=nodeNumbers[10]; num[5]=nodeNumbers[22]; num[6]=nodeNumbers[41]; num[7]=nodeNumbers[40];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 61 62 63 64 41 42 43 44
  num[0]=nodeNumbers[60]; num[1]=nodeNumbers[61]; num[2]=nodeNumbers[62]; num[3]=nodeNumbers[63];
  num[4]=nodeNumbers[40]; num[5]=nodeNumbers[41]; num[6]=nodeNumbers[42]; num[7]=nodeNumbers[43];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 64 63 48 47 44 43 17 29
  num[0]=nodeNumbers[63]; num[1]=nodeNumbers[62]; num[2]=nodeNumbers[47]; num[3]=nodeNumbers[46];
  num[4]=nodeNumbers[43]; num[5]=nodeNumbers[42]; num[6]=nodeNumbers[16]; num[7]=nodeNumbers[28];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 34 21 38 62 23 6 22 42
  num[0]=nodeNumbers[33]; num[1]=nodeNumbers[20]; num[2]=nodeNumbers[37]; num[3]=nodeNumbers[61];
  num[4]=nodeNumbers[22]; num[5]=nodeNumbers[5]; num[6]=nodeNumbers[21]; num[7]=nodeNumbers[41];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 62 38 37 63 42 22 10 43
  num[0]=nodeNumbers[61]; num[1]=nodeNumbers[37]; num[2]=nodeNumbers[36]; num[3]=nodeNumbers[62];
  num[4]=nodeNumbers[41]; num[5]=nodeNumbers[21]; num[6]=nodeNumbers[9]; num[7]=nodeNumbers[42];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  // Q1 sub-hexahedron 63 37 30 48 43 10 7 17
  num[0]=nodeNumbers[62]; num[1]=nodeNumbers[36]; num[2]=nodeNumbers[29]; num[3]=nodeNumbers[47];
  num[4]=nodeNumbers[42]; num[5]=nodeNumbers[9]; num[6]=nodeNumbers[6]; num[7]=nodeNumbers[16];
  buf=splitHexahedronQ1ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++)
  {
    splitNumbers.push_back(buf[i]);
  }
  return splitNumbers;
}

//! return nodes numbers of first order hexahedron elements when splitting second order hexahedron
std::vector<std::vector<number_t> > splitHexahedronQ2ToHexahedraQ1(std::vector<number_t> nodeNumbers)
{
  if (nodeNumbers.size() != 27) error("bad_size","nodeNumbers",27,nodeNumbers.size());
  std::vector<std::vector<number_t> > splitNumbers;
  std::vector<number_t> num(8, 0);
  
  // Q1 sub-hexahedron 1 20 26 13 15 21 27 25
  num[0]=nodeNumbers[0]; num[1]=nodeNumbers[19]; num[2]=nodeNumbers[25]; num[3]=nodeNumbers[12];
  num[4]=nodeNumbers[14]; num[5]=nodeNumbers[20]; num[6]=nodeNumbers[26]; num[7]=nodeNumbers[24];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 20 2 16 26 21 9 22 27
  num[0]=nodeNumbers[19]; num[1]=nodeNumbers[1]; num[2]=nodeNumbers[15]; num[3]=nodeNumbers[25];
  num[4]=nodeNumbers[20]; num[5]=nodeNumbers[8]; num[6]=nodeNumbers[21]; num[7]=nodeNumbers[26];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 13 26 14 4 25 27 24 12
  num[0]=nodeNumbers[12]; num[1]=nodeNumbers[25]; num[2]=nodeNumbers[13]; num[3]=nodeNumbers[3];
  num[4]=nodeNumbers[24]; num[5]=nodeNumbers[26]; num[6]=nodeNumbers[23]; num[7]=nodeNumbers[11];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 26 16 3 14 27 22 18 24
  num[0]=nodeNumbers[25]; num[1]=nodeNumbers[15]; num[2]=nodeNumbers[2]; num[3]=nodeNumbers[13];
  num[4]=nodeNumbers[26]; num[5]=nodeNumbers[21]; num[6]=nodeNumbers[17]; num[7]=nodeNumbers[23];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 15 21 27 25 5 11 23 19
  num[0]=nodeNumbers[14]; num[1]=nodeNumbers[20]; num[2]=nodeNumbers[26]; num[3]=nodeNumbers[24];
  num[4]=nodeNumbers[4]; num[5]=nodeNumbers[10]; num[6]=nodeNumbers[22]; num[7]=nodeNumbers[18];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 21 9 22 27 11 6 10 23
  num[0]=nodeNumbers[20]; num[1]=nodeNumbers[8]; num[2]=nodeNumbers[21]; num[3]=nodeNumbers[26];
  num[4]=nodeNumbers[10]; num[5]=nodeNumbers[5]; num[6]=nodeNumbers[9]; num[7]=nodeNumbers[22];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 25 27 24 12 19 23 17 8
  num[0]=nodeNumbers[24]; num[1]=nodeNumbers[26]; num[2]=nodeNumbers[23]; num[3]=nodeNumbers[11];
  num[4]=nodeNumbers[18]; num[5]=nodeNumbers[22]; num[6]=nodeNumbers[16]; num[7]=nodeNumbers[7];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 27 22 18 24 23 10 7 17
  num[0]=nodeNumbers[26]; num[1]=nodeNumbers[21]; num[2]=nodeNumbers[17]; num[3]=nodeNumbers[23];
  num[4]=nodeNumbers[22]; num[5]=nodeNumbers[9]; num[6]=nodeNumbers[6]; num[7]=nodeNumbers[16];
  splitNumbers.push_back(num);
  return splitNumbers;
}

//! return nodes numbers of first order hexahedron elements when splitting third order hexahedron
std::vector<std::vector<number_t> > splitHexahedronQ3ToHexahedraQ1(std::vector<number_t> nodeNumbers)
{
  if (nodeNumbers.size() != 64) error("bad_size","nodeNumbers",64,nodeNumbers.size());
  std::vector<std::vector<number_t> > splitNumbers;
  std::vector<number_t> num(8, 0);
  
  // Q1 sub-hexahedron 1 20 53 13 15 36 57 51
  num[0]=nodeNumbers[0]; num[1]=nodeNumbers[19]; num[2]=nodeNumbers[52]; num[3]=nodeNumbers[12];
  num[4]=nodeNumbers[14]; num[5]=nodeNumbers[35]; num[6]=nodeNumbers[56]; num[7]=nodeNumbers[50];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 13 53 54 25 51 57 60 50
  num[0]=nodeNumbers[12]; num[1]=nodeNumbers[52]; num[2]=nodeNumbers[53]; num[3]=nodeNumbers[24];
  num[4]=nodeNumbers[50]; num[5]=nodeNumbers[56]; num[6]=nodeNumbers[59]; num[7]=nodeNumbers[49];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 25 54 26 4 50 60 46 24
  num[0]=nodeNumbers[24]; num[1]=nodeNumbers[53]; num[2]=nodeNumbers[25]; num[3]=nodeNumbers[3];
  num[4]=nodeNumbers[49]; num[5]=nodeNumbers[59]; num[6]=nodeNumbers[45]; num[7]=nodeNumbers[23];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 20 32 56 53 36 33 58 57
  num[0]=nodeNumbers[19]; num[1]=nodeNumbers[31]; num[2]=nodeNumbers[55]; num[3]=nodeNumbers[52];
  num[4]=nodeNumbers[35]; num[5]=nodeNumbers[32]; num[6]=nodeNumbers[57]; num[7]=nodeNumbers[56];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 53 56 55 54 57 58 59 60
  num[0]=nodeNumbers[52]; num[1]=nodeNumbers[55]; num[2]=nodeNumbers[54]; num[3]=nodeNumbers[53];
  num[4]=nodeNumbers[56]; num[5]=nodeNumbers[57]; num[6]=nodeNumbers[58]; num[7]=nodeNumbers[59];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 54 55 14 26 60 59 45 46
  num[0]=nodeNumbers[53]; num[1]=nodeNumbers[54]; num[2]=nodeNumbers[13]; num[3]=nodeNumbers[25];
  num[4]=nodeNumbers[59]; num[5]=nodeNumbers[58]; num[6]=nodeNumbers[44]; num[7]=nodeNumbers[45];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 32 2 16 56 33 9 39 58
  num[0]=nodeNumbers[31]; num[1]=nodeNumbers[1]; num[2]=nodeNumbers[15]; num[3]=nodeNumbers[55];
  num[4]=nodeNumbers[32]; num[5]=nodeNumbers[8]; num[6]=nodeNumbers[38]; num[7]=nodeNumbers[57];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 56 16 28 55 58 39 40 59
  num[0]=nodeNumbers[55]; num[1]=nodeNumbers[15]; num[2]=nodeNumbers[27]; num[3]=nodeNumbers[54];
  num[4]=nodeNumbers[57]; num[5]=nodeNumbers[38]; num[6]=nodeNumbers[39]; num[7]=nodeNumbers[58];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 55 28 3 14 59 40 18 45
  num[0]=nodeNumbers[54]; num[1]=nodeNumbers[27]; num[2]=nodeNumbers[2]; num[3]=nodeNumbers[13];
  num[4]=nodeNumbers[58]; num[5]=nodeNumbers[39]; num[6]=nodeNumbers[17]; num[7]=nodeNumbers[44];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 15 36 57 51 27 35 61 52
  num[0]=nodeNumbers[14]; num[1]=nodeNumbers[35]; num[2]=nodeNumbers[56]; num[3]=nodeNumbers[50];
  num[4]=nodeNumbers[26]; num[5]=nodeNumbers[34]; num[6]=nodeNumbers[60]; num[7]=nodeNumbers[51];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 51 57 60 50 52 61 64 49
  num[0]=nodeNumbers[50]; num[1]=nodeNumbers[56]; num[2]=nodeNumbers[59]; num[3]=nodeNumbers[49];
  num[4]=nodeNumbers[51]; num[5]=nodeNumbers[60]; num[6]=nodeNumbers[63]; num[7]=nodeNumbers[48];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 50 60 46 24 49 64 47 12
  num[0]=nodeNumbers[49]; num[1]=nodeNumbers[59]; num[2]=nodeNumbers[45]; num[3]=nodeNumbers[23];
  num[4]=nodeNumbers[48]; num[5]=nodeNumbers[63]; num[6]=nodeNumbers[46]; num[7]=nodeNumbers[11];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 36 33 58 57 35 34 62 61
  num[0]=nodeNumbers[35]; num[1]=nodeNumbers[32]; num[2]=nodeNumbers[57]; num[3]=nodeNumbers[56];
  num[4]=nodeNumbers[34]; num[5]=nodeNumbers[33]; num[6]=nodeNumbers[61]; num[7]=nodeNumbers[60];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 57 58 59 60 61 62 63 64
  num[0]=nodeNumbers[56]; num[1]=nodeNumbers[57]; num[2]=nodeNumbers[58]; num[3]=nodeNumbers[59];
  num[4]=nodeNumbers[60]; num[5]=nodeNumbers[61]; num[6]=nodeNumbers[62]; num[7]=nodeNumbers[63];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 60 59 45 46 64 63 48 47
  num[0]=nodeNumbers[59]; num[1]=nodeNumbers[58]; num[2]=nodeNumbers[44]; num[3]=nodeNumbers[45];
  num[4]=nodeNumbers[63]; num[5]=nodeNumbers[62]; num[6]=nodeNumbers[47]; num[7]=nodeNumbers[46];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 33 9 39 58 34 21 38 62
  num[0]=nodeNumbers[32]; num[1]=nodeNumbers[8]; num[2]=nodeNumbers[38]; num[3]=nodeNumbers[57];
  num[4]=nodeNumbers[33]; num[5]=nodeNumbers[20]; num[6]=nodeNumbers[37]; num[7]=nodeNumbers[61];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 58 39 40 59 62 38 37 63
  num[0]=nodeNumbers[57]; num[1]=nodeNumbers[38]; num[2]=nodeNumbers[39]; num[3]=nodeNumbers[58];
  num[4]=nodeNumbers[61]; num[5]=nodeNumbers[37]; num[6]=nodeNumbers[36]; num[7]=nodeNumbers[62];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 59 40 18 45 63 37 30 48
  num[0]=nodeNumbers[58]; num[1]=nodeNumbers[39]; num[2]=nodeNumbers[17]; num[3]=nodeNumbers[44];
  num[4]=nodeNumbers[62]; num[5]=nodeNumbers[36]; num[6]=nodeNumbers[29]; num[7]=nodeNumbers[47];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 27 35 61 52 5 11 41 19
  num[0]=nodeNumbers[26]; num[1]=nodeNumbers[34]; num[2]=nodeNumbers[60]; num[3]=nodeNumbers[51];
  num[4]=nodeNumbers[4]; num[5]=nodeNumbers[10]; num[6]=nodeNumbers[40]; num[7]=nodeNumbers[18];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 52 61 64 49 19 41 44 31
  num[0]=nodeNumbers[51]; num[1]=nodeNumbers[60]; num[2]=nodeNumbers[63]; num[3]=nodeNumbers[48];
  num[4]=nodeNumbers[18]; num[5]=nodeNumbers[40]; num[6]=nodeNumbers[43]; num[7]=nodeNumbers[30];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 49 64 47 12 31 44 29 8
  num[0]=nodeNumbers[48]; num[1]=nodeNumbers[63]; num[2]=nodeNumbers[46]; num[3]=nodeNumbers[11];
  num[4]=nodeNumbers[30]; num[5]=nodeNumbers[43]; num[6]=nodeNumbers[28]; num[7]=nodeNumbers[7];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 35 34 62 61 11 23 42 41
  num[0]=nodeNumbers[34]; num[1]=nodeNumbers[33]; num[2]=nodeNumbers[61]; num[3]=nodeNumbers[60];
  num[4]=nodeNumbers[10]; num[5]=nodeNumbers[22]; num[6]=nodeNumbers[41]; num[7]=nodeNumbers[40];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 61 62 63 64 41 42 43 44
  num[0]=nodeNumbers[60]; num[1]=nodeNumbers[61]; num[2]=nodeNumbers[62]; num[3]=nodeNumbers[63];
  num[4]=nodeNumbers[40]; num[5]=nodeNumbers[41]; num[6]=nodeNumbers[42]; num[7]=nodeNumbers[43];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 64 63 48 47 44 43 17 29
  num[0]=nodeNumbers[63]; num[1]=nodeNumbers[62]; num[2]=nodeNumbers[47]; num[3]=nodeNumbers[46];
  num[4]=nodeNumbers[43]; num[5]=nodeNumbers[42]; num[6]=nodeNumbers[16]; num[7]=nodeNumbers[28];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 34 21 38 62 23 6 22 42
  num[0]=nodeNumbers[33]; num[1]=nodeNumbers[20]; num[2]=nodeNumbers[37]; num[3]=nodeNumbers[61];
  num[4]=nodeNumbers[22]; num[5]=nodeNumbers[5]; num[6]=nodeNumbers[21]; num[7]=nodeNumbers[41];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 62 38 37 63 42 22 10 43
  num[0]=nodeNumbers[61]; num[1]=nodeNumbers[37]; num[2]=nodeNumbers[36]; num[3]=nodeNumbers[62];
  num[4]=nodeNumbers[41]; num[5]=nodeNumbers[21]; num[6]=nodeNumbers[9]; num[7]=nodeNumbers[42];
  splitNumbers.push_back(num);
  // Q1 sub-hexahedron 63 37 30 48 43 10 7 17
  num[0]=nodeNumbers[62]; num[1]=nodeNumbers[36]; num[2]=nodeNumbers[29]; num[3]=nodeNumbers[47];
  num[4]=nodeNumbers[42]; num[5]=nodeNumbers[9]; num[6]=nodeNumbers[6]; num[7]=nodeNumbers[16];
  splitNumbers.push_back(num);
  return splitNumbers;
}

//! return nodes numbers of first order tetrahedron elements when splitting second order tetrahedron
std::vector<std::vector<number_t> > splitTetrahedronP2ToTetrahedraP1(std::vector<number_t> nodeNumbers)
{
  if (nodeNumbers.size() != 10) error("bad_size","nodeNumbers",10,nodeNumbers.size());
  std::vector<std::vector<number_t> > splitNumbers;
  std::vector<number_t> num(4, 0);
  
  // P1 sub-tetrahedron 1 10 9 5
  num[0]=nodeNumbers[0]; num[1]=nodeNumbers[9]; num[2]=nodeNumbers[8]; num[3]=nodeNumbers[4];
  splitNumbers.push_back(num);
  // P1 sub-tetrahedron 10 2 8 6
  num[0]=nodeNumbers[9]; num[1]=nodeNumbers[1]; num[2]=nodeNumbers[7]; num[3]=nodeNumbers[5];
  splitNumbers.push_back(num);
  // P1 sub-tetrahedron 9 8 3 7
  num[0]=nodeNumbers[8]; num[1]=nodeNumbers[7]; num[2]=nodeNumbers[2]; num[3]=nodeNumbers[6];
  splitNumbers.push_back(num);
  // P1 sub-tetrahedron 5 6 7 4
  num[0]=nodeNumbers[4]; num[1]=nodeNumbers[5]; num[2]=nodeNumbers[6]; num[3]=nodeNumbers[3];
  splitNumbers.push_back(num);
  // P1 sub-tetrahedron 6 9 8 10
  num[0]=nodeNumbers[5]; num[1]=nodeNumbers[8]; num[2]=nodeNumbers[7]; num[3]=nodeNumbers[9];
  splitNumbers.push_back(num);
  // P1 sub-tetrahedron 9 10 6 5
  num[0]=nodeNumbers[8]; num[1]=nodeNumbers[9]; num[2]=nodeNumbers[5]; num[3]=nodeNumbers[4];
  splitNumbers.push_back(num);
  // P1 sub-tetrahedron 9 8 7 6
  num[0]=nodeNumbers[8]; num[1]=nodeNumbers[7]; num[2]=nodeNumbers[6]; num[3]=nodeNumbers[5];
  splitNumbers.push_back(num);
  // P1 sub-tetrahedron 9 6 7 5
  num[0]=nodeNumbers[8]; num[1]=nodeNumbers[5]; num[2]=nodeNumbers[6]; num[3]=nodeNumbers[4];
  splitNumbers.push_back(num);
  return splitNumbers;  
}

//! return nodes numbers of first order tetrahedron elements when splitting third order tetrahedron
std::vector<std::vector<number_t> > splitTetrahedronP3ToTetrahedraP1(std::vector<number_t> nodeNumbers)
{
  if (nodeNumbers.size() != 20) error("bad_size","nodeNumbers",20,nodeNumbers.size());
  std::vector<std::vector<number_t> > splitNumbers, buf;
  std::vector<number_t> num(10, 0);
  
  // P2 sub-tetrahedron 1 16 15 11 5 19 18 20 9 10
  num[0]=nodeNumbers[0]; num[1]=nodeNumbers[15]; num[2]=nodeNumbers[14]; num[3]=nodeNumbers[10];
  num[4]=nodeNumbers[4]; num[5]=nodeNumbers[18]; num[6]=nodeNumbers[17]; num[7]=nodeNumbers[19]; num[8]=nodeNumbers[8]; num[9]=nodeNumbers[9];
  buf=splitTetrahedronP2ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++) { splitNumbers.push_back(buf[i]); }
  // P2 sub-tetrahedron 10 2 14 12 19 6 17 8 20 16
  num[0]=nodeNumbers[9]; num[1]=nodeNumbers[1]; num[2]=nodeNumbers[13]; num[3]=nodeNumbers[11];
  num[4]=nodeNumbers[18]; num[5]=nodeNumbers[5]; num[6]=nodeNumbers[16]; num[7]=nodeNumbers[7]; num[8]=nodeNumbers[19]; num[9]=nodeNumbers[15];
  buf=splitTetrahedronP2ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++) { splitNumbers.push_back(buf[i]); }
  // P2 sub-tetrahedron 5 6 7 4 11 12 13 17 18 19
  num[0]=nodeNumbers[4]; num[1]=nodeNumbers[5]; num[2]=nodeNumbers[6]; num[3]=nodeNumbers[3];
  num[4]=nodeNumbers[10]; num[5]=nodeNumbers[11]; num[6]=nodeNumbers[12]; num[7]=nodeNumbers[16]; num[8]=nodeNumbers[17]; num[9]=nodeNumbers[18];
  buf=splitTetrahedronP2ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++) { splitNumbers.push_back(buf[i]); }
  // P2 sub-tetrahedron 9 8 3 13 18 17 7 14 15 20
  num[0]=nodeNumbers[8]; num[1]=nodeNumbers[7]; num[2]=nodeNumbers[2]; num[3]=nodeNumbers[12];
  num[4]=nodeNumbers[17]; num[5]=nodeNumbers[16]; num[6]=nodeNumbers[6]; num[7]=nodeNumbers[13]; num[8]=nodeNumbers[14]; num[9]=nodeNumbers[19];
  buf=splitTetrahedronP2ToTetrahedraP1(num);
  for (number_t i=0;i< buf.size(); i++) { splitNumbers.push_back(buf[i]); }
  // P1 sub-tetrahedron 17 18 19 20
  num.resize(4);
  num[0]=nodeNumbers[16]; num[1]=nodeNumbers[17]; num[2]=nodeNumbers[18]; num[3]=nodeNumbers[19];
  splitNumbers.push_back(num);
  return splitNumbersUnique(splitNumbers);
}

//! return nodes numbers by removing duplicates
std::vector<std::vector<number_t> > splitNumbersUnique(std::vector<std::vector<number_t> > splitnum)
{
  std::vector<std::vector<number_t> > splitNumbers=splitnum, buf=splitnum;
  std::vector<std::vector<number_t> >::iterator it,it2;
  std::sort(buf.begin(),buf.end());
  it=std::unique_copy(buf.begin(),buf.end(),splitNumbers.begin());
  splitNumbers.erase(it,splitNumbers.end());
  std::sort(splitNumbers.begin(),splitNumbers.end());
  return splitNumbers;
}

} // end of namespace xlifepp
