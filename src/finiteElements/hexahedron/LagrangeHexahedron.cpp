/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LagrangeHexahedron.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 17 dec 2002
  \date 7 aug 2012

  \brief Implementation of xlifepp::LagrangeHexahedron members and related functions
 */

#include "LagrangeHexahedron.hpp"
#include "../splitUtils.hpp"
#include "../Interpolation.hpp"
#include "../segment/LagrangeSegment.hpp"
#include "utils.h"

#include <iostream>

namespace xlifepp {


// --------------------------------------------------------------------------------
//    LagrangeHexahedron constructor for Lagrange reference elements
// --------------------------------------------------------------------------------
LagrangeHexahedron::LagrangeHexahedron(const Interpolation* int_p)
  : RefHexahedron(int_p)
{
  name_ += "_Lagrange";
  trace_p->push("LagrangeHexahedron::LagrangeHexahedron (" + name_ + ")");
  //! build element interpolation data
  interpolationData();
  //! define local numbering of points on edges
  sideOfSideNumbering();
  //! find or create Reference Element for element edges
  sideOfSideRefElement();
  //! define local numbering of points on faces
  sideNumbering();
  //! find or create Reference Element for element faces
  sideRefElement();
  //build barycentricSideDofMap: internal dof on side (face) ordered by barycentric coordinates
  buildBarycentricSideDof();   //no effect if order < 2
  maxDegree = 3*interpolation_p->numtype;

  trace_p->pop();
}

LagrangeHexahedron::~LagrangeHexahedron() {}

//! interpolationData defines Reference Element interpolation data
void LagrangeHexahedron::interpolationData()
{
  number_t interpNum = interpolation_p->numtype;
  switch ( interpNum )
  {
    case 0:
      nbDofs_ = nbInternalDofs_ = 1;
      break;
    case 1:
      nbDofs_ = nbDofsOnVertices_ = 8;
      break;
    default: // Lagrange hexahedron of degree > 1
      nbDofs_ = (interpNum + 1) * (interpNum + 1) * (interpNum + 1);
      nbInternalDofs_ = (interpNum - 1) * (interpNum - 1) * (interpNum - 1);
      nbDofsOnVertices_ = 8;
      nbDofsInSideOfSides_ = 12 * (interpNum - 1);
      nbDofsInSides_ = nbDofs_ - nbInternalDofs_ - nbDofsOnVertices_ - nbDofsInSideOfSides_;
      break;
  }
  // Creating Degrees Of Freedom of reference element
  // in Lagrange standard elements all D.o.F on edges are sharable
  refDofs.reserve(nbDofs_);
  lagrangeRefDofs(3, nbDofsOnVertices_, nbInternalDofs_, 12, nbDofsInSideOfSides_,  6, nbDofsInSides_ );
  //lagrangeRefDofs(1, nbDofsOnVertices_, nbDofs_ - nbInternalDofs_, 3);
  // compute number of shape functions
  nbshfcts_ = shapeValueSize();
  /*
  // Lagrange hexahedron Q2 serendipity element
  {
     nbDofs_=20;
  }
  */
  nbPts_ = nbDofs_;
}

//! pointCoordinates defines Lagrange Reference Element point coordinates
std::vector<RefDof*>::iterator LagrangeHexahedron::vertexCoordinates()
{
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();
  (*it_rd++)->coords(1., 0., 0.);
  (*it_rd++)->coords(1., 1., 0.);
  (*it_rd++)->coords(0., 1., 0.);
  (*it_rd++)->coords(0., 0., 0.);
  (*it_rd++)->coords(1., 0., 1.);
  (*it_rd++)->coords(1., 1., 1.);
  (*it_rd++)->coords(0., 1., 1.);
  (*it_rd++)->coords(0., 0., 1.);
  return it_rd;
}

void LagrangeHexahedron::pointCoordinates(const RefElement* rfSeg)
{
  trace_p->push("LagrangeHexahedron::pointCoordinates");
  /*
   Compute D.o.F's from the correspondence between segment and hexahedron local numbering
  */
  number_t interpNum = interpolation_p->numtype;
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();
  switch ( interpNum )
  {
    case 0:
      (*it_rd)->coords(.5, .5, .5);
      break;
    default:  // Lagrange hexahedron of degree > 0
      it_rd = vertexCoordinates();
      if ( interpNum > 1 )
      {
        // correspondence between segment and hexaedron local numbering
        number_t** s2h = new number_t*[3];
        for ( dimen_t d = 0; d < 3; d++ )  s2h[d] = new number_t[nbPts_];
        tensorNumberingHexahedron(interpNum, s2h);
        // find 1d reference element associated with any edge
        const RefElement* sideOfSideRef = (rfSeg == nullptr) ? sideOfSideRefElems_[0] : rfSeg;
        while ( it_rd != refDofs.end() )
        {
          //number_t pt = (*it_rd)->supportNumber() - 1;
          number_t pt = (*it_rd)->nodeNumber() - 1;
          real_t x = *(sideOfSideRef->refDofs[s2h[0][pt]]->coords());
          real_t y = *(sideOfSideRef->refDofs[s2h[1][pt]]->coords());
          real_t z = *(sideOfSideRef->refDofs[s2h[2][pt]]->coords());
           (*it_rd++)->coords(x, y, z);
        }
        for ( dimen_t d = 0; d < 3; d++ ) delete[] s2h[d];
        delete[] s2h;
      }
      break;
  }
  trace_p->pop();
}

//! output of Lagrange D.o.f's numbers on underlying P1 mesh D.o.f's
void LagrangeHexahedron::outputAsP1(std::ofstream& os, const int refNum, const std::vector<number_t>& ranks) const
{
  //int interpNum(interpolation_p->numtype);
  noSuchFunction("outputAsP1");
} // end of outputAsP1

//! sideNumbering defines Lagrange Reference Element numbering on sides
void LagrangeHexahedron::sideNumbering()
{
  trace_p->push("LagrangeHexahedron::sideNumbering");

  number_t interpNum = interpolation_p->numtype;
  number_t interpNum1 = interpNum - 1;
  number_t nbSides = geomRefElem_p->nbSides();
  number_t nbSideOfSides = geomRefElem_p->nbSideOfSides();
  number_t nbVertices = geomRefElem_p->nbVertices();
  number_t nbVerticesPerSide = geomRefElem_p->sideVertexNumbers()[0].size();

  number_t nbDofsPerSideOfSide = interpNum + 1;
  number_t nbDofsPerSide = (interpNum + 1) * (interpNum + 1);
  number_t nbInternalDofsPerSide = (interpNum - 1) * (interpNum - 1);
  number_t more = nbVertices + interpNum1 * nbSideOfSides;

  sideDofNumbers_.resize(nbSides);

  if(interpNum==0)  //P0 interpolation
  {
       for(number_t side = 0; side < nbSides; side++)
       {
           sideDofNumbers_[side].resize(1);
           sideDofNumbers_[side][0]=1;
       }
       return;
  }

  if ( interpNum > 0 )
  {
    number_t sideNum = 0;
    for ( number_t side = 0; side < nbSides; side++, sideNum++ )
    {
      number_t nbSideOfSidesPerSide = 4;
      sideDofNumbers_[side].resize(nbDofsPerSide);
      for ( number_t i = 0; i < nbVerticesPerSide; i++)
      {
        sideDofNumbers_[side][i] = geomRefElem_p->sideVertexNumber(i + 1, side + 1);
      }
      number_t i = 0;
      for ( number_t sideOfSideNum = 0; sideOfSideNum < nbSideOfSidesPerSide ; sideOfSideNum++ )
      {
        i = sideOfSideNum;
        int_t sideOfSideInSideNum = geomRefElem_p->sideOfSideNumber(sideOfSideNum + 1, side + 1);
        if ( sideOfSideInSideNum > 0 )
        {
          for ( number_t e = 2; e < nbDofsPerSideOfSide; e++ )
          {
            i += nbSideOfSidesPerSide;
            sideDofNumbers_[side][i] = sideOfSideDofNumbers_[sideOfSideInSideNum - 1][e];
          }
        }
        else
        {
          sideOfSideInSideNum = -sideOfSideInSideNum;
          for ( number_t e = nbDofsPerSideOfSide - 1; e > 1; e-- )
          {
            i += nbSideOfSidesPerSide;
            sideDofNumbers_[side][i] = sideOfSideDofNumbers_[sideOfSideInSideNum - 1][e];
          }
        }
      }

      // internal D.o.F in side
      for ( number_t d = 0; d < nbInternalDofsPerSide; d++)
      {
        sideDofNumbers_[side][++i] = ++more;
      }
    }
  }
  trace_p->pop();
}

//! sideOfSideNumbering defines Lagrange Reference Element numbering on side of sides
void LagrangeHexahedron::sideOfSideNumbering()
{
  // trace_p->push("LagrangeHexahedron::sideOfSideNumbering");
  number_t interpNum = interpolation_p->numtype;

  if ( interpNum > 0 )
  {
    number_t intN = interpNum;
    //if ( interpNum == _P1BubbleP3 ) { intN = 1; }
    number_t nbVertices = geomRefElem_p->nbVertices();
    number_t nbSideOfSides = geomRefElem_p->nbSideOfSides();
    number_t more;
    number_t nbVerticesPerSideOfSide = geomRefElem_p->sideOfSideVertexNumbers()[0].size();
    number_t nbDofsPerSideOfSide = intN + 1;

    sideOfSideDofNumbers_.resize(nbSideOfSides);
    for (number_t sideOfSide = 0; sideOfSide < nbSideOfSides; sideOfSide++)
    {
      sideOfSideDofNumbers_[sideOfSide].resize(nbDofsPerSideOfSide);
      for (number_t j = 0; j < nbVerticesPerSideOfSide; j++)
      {
        sideOfSideDofNumbers_[sideOfSide][j] = geomRefElem_p->sideOfSideVertexNumber(j + 1, sideOfSide + 1);
      }
      if (intN > 1)
      {
        more = nbVertices + 1;
        for (number_t k = nbVerticesPerSideOfSide; k < nbDofsPerSideOfSide; k++, more += nbSideOfSides)
        {
          sideOfSideDofNumbers_[sideOfSide][k] = sideOfSide + more;
        }
      }
    }
  }
  // trace_p->pop();
}

/*! internal side dofs mapping when vertices of side are permuted
    this function returns the number of the n-th internal dof of a face where vertices are permuted
    if no permutation, it returns n

    Given two hexahedra A and B sharing a face called a for A and b for B, the question is
    to define the same intermediate face from the data related to each the two hexahedra,
    in order to create the bi-univocal correspondence between the dofs lying inside the
    two faces.

    Each face is defined by the numbers of its 4 vertices and the internal dofs are located
    using their barycentric coordinates with respect to these vertices. Sorting the
    vertices according to their numbers in ascending order lead to the same quadruplet for
    a and b. However, in the definition of a and b, the vertices are given in a specific
    order, following the boundary of the face and corresponding to a cycle.

    After sorting, the sequence of numbers may not correspond anymore to a cycle which
    would produce barycentric coordinates inconsistant with the original ones. Thus we
    have to swap some of them in order to recover a cycle and this operation should lead to
    the same sequence when done from the data related to A and from the data related to B.

    Let (i,j,k,l) be the ranks of the vertices after sorting the corresponding vertex
    numbers. The following table gives the final permutations to be applied to order to
    recover a cycle that answer the question:

         i j k l   Path   Permutation   Cycle
         1 2 3 4    +       1 2 3 4       +
         1 2 4 3    Z       1 2 3-4       +
         1 3 2 4    X       1 2-3 4       +
         1 3 4 2    X       1 4-3 2       -
         1 4 2 3    Z       1 4 3-2       -
         1 4 3 2    -       1 4 3 2       -
         2 1 3 4    Z       2 1 4-3       -
         2 1 4 3    -       2 1 4 3       -
         2 3 1 4    Z       2 3 4-1       +
         2 3 4 1    +       2 3 4 1       +
         2 4 1 3    X       2 1-4 3       -
         2 4 3 1    X       2 3-4 1       +
         3 1 2 4    X       3 2-1 4       -
         3 1 4 2    X       3 4-1 2       +
         3 2 1 4    -       3 2 1 4       -
         3 2 4 1    Z       3 2 1-4       -
         3 4 1 2    +       3 4 1 2       +
         3 4 2 1    Z       3 4 1-2       +
         4 1 2 3    +       4 1 2 3       +
         4 1 3 2    Z       4 1 2-3       +
         4 2 1 3    X       4 1-2 3       +
         4 2 3 1    X       4 3-2 1       -
         4 3 1 2    Z       4 3 2-1       -
         4 3 2 1    -       4 3 2 1       -

    Legend:
     Column 1: the 24 possible configurations
     Column 2: shape of the path obtained by drawing a line between two successive vertices
               (+ and - denote a cycle in a direction and in the opposite direction)
     Column 3: permutation to be applied to the given configuration to get a cycle
     Column 4: direction of the cycle (this information does not matter ; it is specified
               only for clearness)

    Applying a permutation to the corresponding configuration leads to a cycle starting from
    the vertex bearing the smallest number. The sequence obtained from the hexahedron A is
    the same as the one obtained from the hexahedron B.
*/
number_t LagrangeHexahedron::sideDofsMap(const number_t& n, const number_t& i, const number_t& j, const number_t& k) const {
  if (i==1 && j==2 && k==3) return n; // case (i,j,k,l) = (1,2,3,4)
  // permute barycentric coordinate of dof n
  Triplet<number_t> t=barycentricSideDofs[n-1], tp=t;
  number_t deg = interpolation_p->numtype, tfourth = deg*deg - t.first - t.second - t.third;
  switch (i) {
    case 1:
      switch (j) {
        // case 2:   do nothing             // case (1,2,3,4)
                                             // case (1,2,4,3)
        case 3:
          if (k==4) { tp.second = tfourth; } // case (1,3,4,2)
          // else   { do nothing }           // case (1,3,2,4)
          break;
        case 4:
          tp.second = tfourth;               // case (1,4,2,3)
                                             // case (1,4,3,2)
          break;
      }
      break;
    case 2:
      tp.first = t.second;
      tp.third = tfourth;
      switch (j) {
        case 1:
          tp.second = t.first;               // case (2,1,3,4)
                                             // case (2,1,4,3)
          break;
        case 3:
          tp.second = t.third;               // case (2,3,1,4)
                                             // case (2,3,4,1)
          break;
        case 4:
          if (k==1) { tp.second = t.first; } // case (2,4,1,3)
          else      { tp.second = t.third; } // case (2,4,3,1)
          break;
      }
      break;
    case 3:
      tp.first = t.third;
      tp.third = t.first;
      switch (j) {
        case 1:
          if (k==4) { tp.second = tfourth; } // case (3,1,4,2)
          // else   { do nothing }           // case (3,1,2,4)
          break;
        // case 2:   do nothing             // case (3,2,1,4)
                                             // case (3,2,4,1)
        case 4:
          tp.second = tfourth;               // case (3,4,1,2)
                                             // case (3,4,2,1)
          break;
      }
      break;
    case 4:
      tp.first = tfourth;
      tp.third = t.second;
      switch (j) {
        case 1:
          tp.second = t.first;               // case (4,1,2,3)
                                             // case (4,1,3,2)
          break;
        case 2:
          if (k==1) { tp.second = t.first; } // case (4,2,1,3)
          else      { tp.second = t.third; } // case (4,2,3,1)
          break;
        case 3:
          tp.second = t.third;               // case (4,3,1,2)
                                             // case (4,3,2,1)
          break;
      }
      break;
    default:
      where("LagrangeHexahedron::sideDofsMap()");
      error("index_out_of_range","i", 1, 4);
  }
  // find new dof number after permutation
  std::map<Triplet<number_t>,number_t>::const_iterator it=barycentricSideDofMap.find(tp);
  if (it==barycentricSideDofMap.end()) {
    where("LagrangeHexahedron::sideDofsMap()");
    error("triplet_not_found");
  }
  return it->second;
}

/*! Create the map of internal dofs on a side (face) of a hexahedron, sorted according to their barycentric coordinates
    with respect to the 4 vertices of the face, numbered (1,2,3,4) below.
    The geometric position of the dofs is the same as those of the quadrangle.

  \verbatim
    3----2  3---6---2  3--10---6---2  3--14--10---6---2  3--18--14--10---6---2
    |    |  |       |  |           |  |               |  |                   |
    4----1  7   9   5  7  15  14   9  7  19  22  18  13  7  23  30  26  22  17
     k=1    |       |  |           |  |               |  |                   |
            4---8---1 11  16  13   5 11  23  25  21   9 11  27  35  34  29  13
               k=2     |           |  |               |  |                   |
                       4---8--12---1 15  20  24  17   5 15  31  36  33  25   9
                            k=3       |               |  |                   |
                                      4---8--12--16---1 19  24  28  32  21   5
                                             k=4         |                   |
                                                         4---8--12--16--20---1
                                                                  k=5
  \endverbatim
   Only the first 3 coordinates are stored since this is sufficient for the objective.
   Fill the reciprocal vector barycentricSideDofs.
*/
void LagrangeHexahedron::buildBarycentricSideDof() {
  number_t k=interpolation_p->numtype;
  if (k<2) return;   // no internal side dofs

// Creation of the map: coordinates -> dof number, automatically sorted according to lexicographic order on coordinates.
// (get dof number given the coordinates)
  using std::make_pair;
  number_t di=0;     // dof index
  number_t perm[] = {3, 0, 1, 2};
  for (number_t i1=1; i1 <= (k-1)/2; i1++) {
    for (number_t i2=i1; i2 < k-i1; i2++) {
      number_t bar[]={ (k-i2)*(k-i1), i2*(k-i1), i2*i1, (k-i2)*i1 }, ind[] = {0, 1, 2, 3};
      barycentricSideDofMap.insert(make_pair(Triplet<number_t> (bar[ind[0]],bar[ind[1]],bar[ind[2]]), ++di));
      for (int n=0; n<3; n++) {
        for (int i=0; i<4; i++) { ind[i] = perm[ind[i]]; }
        barycentricSideDofMap.insert(make_pair(Triplet<number_t> (bar[ind[0]],bar[ind[1]],bar[ind[2]]), ++di));
      }
    }
  }
  if (k % 2 == 0) {// add the remaining central point, when the order is even
    number_t bar = (k*k)/4;
    barycentricSideDofMap.insert(make_pair(Triplet<number_t> (bar,bar,bar), ++di));
  }

// Creation of the vector containing the above coordinates, ordered according the dof number.
// (get the coordinates given the dof number - 1)
// (Identical to LagrangeTetrahedron::buildBarycentricSideDof())
  barycentricSideDofs.resize(di);
  std::map<Triplet<number_t>,number_t>::iterator itm=barycentricSideDofMap.begin();
  for (; itm != barycentricSideDofMap.end(); ++itm) { barycentricSideDofs[itm->second - 1] = itm->first; }

}

//! return nodes numbers of first order tetrahedron elements when splitting current element
std::vector<std::vector<number_t> > LagrangeHexahedron::splitP1() const {
  std::vector<std::vector<number_t> > splitnum, buf;
  std::vector<number_t> num;
  switch(interpolation_p->numtype) {
    case 0:
    case 1:
      num.resize(8);
      for (number_t i=0;i<num.size();i++) num[i]=i+1;
      return splitHexahedronQ1ToTetrahedraP1(num);
    case 2:
      num.resize(27);
      for (number_t i=0;i<num.size();i++) num[i]=i+1;
      return splitHexahedronQ2ToTetrahedraP1(num);
    case 3:
      num.resize(8);
      // Q1 sub-hexahedron 1 20 53 13 15 36 57 51
      num[0]=1; num[1]=20; num[2]=53; num[3]=13; num[4]=15; num[5]=36; num[6]=57; num[7]=51;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 13 53 54 25 51 57 60 50
      num[0]=13; num[1]=53; num[2]=54; num[3]=25; num[4]=51; num[5]=57; num[6]=60; num[7]=50;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 25 54 26 4 50 60 46 24
      num[0]=25; num[1]=54; num[2]=26; num[3]=4; num[4]=50; num[5]=60; num[6]=46; num[7]=24;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 20 32 56 53 36 33 58 57
      num[0]=20; num[1]=32; num[2]=56; num[3]=53; num[4]=36; num[5]=33; num[6]=58; num[7]=57;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 53 56 55 54 57 58 59 60
      num[0]=53; num[1]=56; num[2]=55; num[3]=54; num[4]=57; num[5]=58; num[6]=59; num[7]=60;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 54 55 14 26 60 59 45 46
      num[0]=54; num[1]=55; num[2]=14; num[3]=26; num[4]=60; num[5]=59; num[6]=45; num[7]=46;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 32 2 16 56 33 9 39 58
      num[0]=32; num[1]=2; num[2]=16; num[3]=56; num[4]=33; num[5]=9; num[6]=39; num[7]=58;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 56 16 28 55 58 39 40 59
      num[0]=56; num[1]=16; num[2]=28; num[3]=55; num[4]=58; num[5]=39; num[6]=40; num[7]=59;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 55 28 3 14 59 40 18 45
      num[0]=55; num[1]=28; num[2]=3; num[3]=14; num[4]=59; num[5]=40; num[6]=18; num[7]=45;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 15 36 57 51 27 35 61 52
      num[0]=15; num[1]=36; num[2]=57; num[3]=51; num[4]=27; num[5]=35; num[6]=61; num[7]=52;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 51 57 60 50 52 61 64 49
      num[0]=51; num[1]=57; num[2]=60; num[3]=50; num[4]=52; num[5]=61; num[6]=64; num[7]=49;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 50 60 46 24 49 64 47 12
      num[0]=50; num[1]=60; num[2]=46; num[3]=24; num[4]=49; num[5]=64; num[6]=47; num[7]=12;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 36 33 58 57 35 34 62 61
      num[0]=36; num[1]=33; num[2]=58; num[3]=57; num[4]=35; num[5]=34; num[6]=62; num[7]=61;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 57 58 59 60 61 62 63 64
      num[0]=57; num[1]=58; num[2]=59; num[3]=60; num[4]=61; num[5]=62; num[6]=63; num[7]=64;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 60 59 45 46 64 63 48 47
      num[0]=60; num[1]=59; num[2]=45; num[3]=46; num[4]=64; num[5]=63; num[6]=48; num[7]=47;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 33 9 39 58 34 21 38 62
      num[0]=33; num[1]=9; num[2]=39; num[3]=58; num[4]=34; num[5]=21; num[6]=38; num[7]=62;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 58 39 40 59 62 38 37 63
      num[0]=58; num[1]=39; num[2]=40; num[3]=59; num[4]=62; num[5]=38; num[6]=37; num[7]=63;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 59 40 18 45 63 37 30 48
      num[0]=59; num[1]=40; num[2]=18; num[3]=45; num[4]=63; num[5]=37; num[6]=30; num[7]=48;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 27 35 61 52 5 11 41 19
      num[0]=27; num[1]=35; num[2]=61; num[3]=52; num[4]=5; num[5]=11; num[6]=41; num[7]=19;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 52 61 64 49 19 41 44 31
      num[0]=52; num[1]=61; num[2]=64; num[3]=49; num[4]=19; num[5]=41; num[6]=44; num[7]=31;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 49 64 47 12 31 44 29 8
      num[0]=49; num[1]=64; num[2]=47; num[3]=12; num[4]=31; num[5]=44; num[6]=29; num[7]=8;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 35 34 62 61 11 23 42 41
      num[0]=35; num[1]=34; num[2]=62; num[3]=61; num[4]=11; num[5]=23; num[6]=42; num[7]=41;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 61 62 63 64 41 42 43 44
      num[0]=61; num[1]=62; num[2]=63; num[3]=64; num[4]=41; num[5]=42; num[6]=43; num[7]=44;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 64 63 48 47 44 43 17 29
      num[0]=64; num[1]=63; num[2]=48; num[3]=47; num[4]=44; num[5]=43; num[6]=17; num[7]=29;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 34 21 38 62 23 6 22 42
      num[0]=34; num[1]=21; num[2]=38; num[3]=62; num[4]=23; num[5]=6; num[6]=22; num[7]=42;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 62 38 37 63 42 22 10 43
      num[0]=62; num[1]=38; num[2]=37; num[3]=63; num[4]=42; num[5]=22; num[6]=10; num[7]=43;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      // Q1 sub-hexahedron 63 37 30 48 43 10 7 17
      num[0]=63; num[1]=37; num[2]=30; num[3]=48; num[4]=43; num[5]=10; num[6]=7; num[7]=17;
      buf=splitHexahedronQ1ToTetrahedraP1(num);
      for (number_t i=0;i< buf.size(); i++) { splitnum.push_back(buf[i]); }
      return splitnum;
    default:
      return splitLagrange3DToP1();
  }
  return splitnum;
}

//! return nodes numbers of first order hexahedron elements when splitting current element
splitvec_t LagrangeHexahedron::splitO1() const
{
  splitvec_t splitnum;
  std::vector<std::vector<number_t> > buf;
  std::vector<number_t> tuple(8, 0);
  std::vector<number_t> num;

  switch(interpolation_p->numtype) {
    case 1:
      num.resize(8);
      for (number_t i=0;i<num.size();i++) num[i]=i+1;
      splitnum.push_back(std::make_pair(_hexahedron,num));
      return splitnum;
    case 2:
      num.resize(27);
      for (number_t i=0;i<num.size();i++) num[i]=i+1;
      buf = splitHexahedronQ2ToHexahedraQ1(num);
      for (number_t i=0; i< buf.size(); i++)
      {
        splitnum.push_back(std::make_pair(_hexahedron, buf[i]));
      }
      return splitnum;
    case 3:
      num.resize(64);
      for (number_t i=0;i<num.size();i++) num[i]=i+1;
      buf = splitHexahedronQ3ToHexahedraQ1(num);
      for (number_t i=0; i< buf.size(); i++)
      {
        splitnum.push_back(std::make_pair(_hexahedron, buf[i]));
      }
      return splitnum;
    default:
      return splitLagrange3DToQ1();
  }
  return splitnum;
}

/*!
  standard Lagrange Q_k hexahedron Reference Element shape functions ((k+1)^3 nodes)
  build using standard Lagrange P_k 1D Reference Element shape functions (k+1 nodes)
  at standard (equidistant on edges) points or Gauss-Lobatto abscissae.
  !!! second derivative computation not yet managed
 */
void LagrangeHexahedron::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  int interpNum(interpolation_p->numtype);
  std::vector<real_t>::iterator sh0w_i(shv.w.begin());
  std::vector< std::vector<real_t> >::iterator sh1w, sh2w,sh3w;
  std::vector<real_t>::iterator sh1w_i, sh2w_i, sh3w_i;
  if(withDeriv)
  {
    sh1w=shv.dw.begin();sh2w=sh1w + 1; sh3w = sh1w + 2;
    sh1w_i=sh1w->begin(); sh2w_i=sh2w->begin(); sh3w_i=sh3w->begin();
  }
  switch ( interpNum ) {
    case 0:
      *sh0w_i = 1.;
      if ( withDeriv ) { *sh1w_i = 0.; *sh2w_i = 0.; *sh3w_i = 0.; }
      break;
    case 1:
      {
        // In the (x1, x2, x3) reference space, shape functions of the Lagrange
        // standard P1 hexahedron are given by
        // w_1(x1,x2) =   x1  (1-x2) (1-x3)  |  w_2(x1,x2) =   x1    x2   (1-x3)
        // w_3(x1,x2) = (1-x1)  x2   (1-x3)  |  w_4(x1,x2) = (1-x1)(1-x2) (1-x3)
        // w_5(x1,x2) =   x1  (1-x2)   x3    |  w_6(x1,x2) =   x1    x2     x3
        // w_7(x1,x2) = (1-x1)  x2     x3    |  w_8(x1,x2) = (1-x1)(1-x2)   x3
        real_t x = *it_pt, y = *(it_pt + 1), z = *(it_pt + 2), xm1 = x - 1, ym1 = y - 1, zm1 = z - 1;
        *sh0w_i++ = x * ym1 * zm1; // 0
        *sh0w_i++ = -x * y * zm1; // 1
        *sh0w_i++ = xm1 * y * zm1; // 2
        *sh0w_i++ = -xm1 * ym1 * zm1; // 3
        *sh0w_i++ = -x * ym1 * z; // 4
        *sh0w_i++ = x * y * z;    // 5
        *sh0w_i++ = -xm1 * y * z; // 6
        *sh0w_i   = xm1 * ym1 * z; // 7
        if ( withDeriv ) {
          *sh1w_i++ = ym1 * zm1; *sh2w_i++ = x * zm1;   *sh3w_i++ = x * ym1; // 0
          *sh1w_i++ = -y * zm1;   *sh2w_i++ = -x * zm1;   *sh3w_i++ = -x * y; // 1
          *sh1w_i++ = y * zm1;   *sh2w_i++ = xm1 * zm1; *sh3w_i++ = xm1 * y; // 2
          *sh1w_i++ = -ym1 * zm1; *sh2w_i++ = -xm1 * zm1; *sh3w_i++ = -xm1 * ym1; // 3
          *sh1w_i++ = -ym1 * z;   *sh2w_i++ = -x * z;     *sh3w_i++ = -x * ym1; // 4
          *sh1w_i++ = y * z;     *sh2w_i++ = x * z;     *sh3w_i++ = x * y; // 5
          *sh1w_i++ = -y * z;     *sh2w_i++ = -xm1 * z;   *sh3w_i++ = -xm1 * y; // 6
          *sh1w_i   = ym1 * z;   *sh2w_i   = xm1 * z;   *sh3w_i   = xm1 * ym1; // 7
        }
      }
      break;
    default:
      // 1D shape functions
      RefElement* refSeg = sideOfSideRefElems_[0];
      ShapeValues shv0(*refSeg,withDeriv,with2Deriv), shv1(shv0), shv2(shv0);
      refSeg->computeShapeValues(it_pt++, shv0, withDeriv);
      refSeg->computeShapeValues(it_pt++, shv1, withDeriv);
      refSeg->computeShapeValues(it_pt  , shv2, withDeriv);
      std::vector<real_t>&  shape_0 = shv0.w;
      std::vector<real_t>& dshape_0 = shv0.dw[0];
      std::vector<real_t>&  shape_1 = shv1.w;
      std::vector<real_t>& dshape_1 = shv1.dw[0];
      std::vector<real_t>&  shape_2 = shv2.w;
      std::vector<real_t>& dshape_2 = shv2.dw[0];

      // correspondence between segment and hexahedron local numbering
      number_t** s2h = new number_t*[3];
      for ( dimen_t d = 0; d < 3; d++ ) { s2h[d] = new number_t[(interpNum + 1) * (interpNum + 1) * (interpNum + 1)]; }
      tensorNumberingHexahedron(interpNum, s2h);

      // tensor product (cube) of 1D shape functions
      number_t* seg0(s2h[0]), *seg1(s2h[1]), *seg2(s2h[2]);
      for ( sh0w_i = shv.w.begin(); sh0w_i != shv.w.end(); seg0++, seg1++, seg2++, sh0w_i++ ) {
        *sh0w_i = shape_0[*seg0] * shape_1[*seg1] * shape_2[*seg2];
      }
      if ( withDeriv ) {
        seg0 = s2h[0]; seg1 = s2h[1]; seg2 = s2h[2];
        for ( sh1w_i = (*sh1w).begin(); sh1w_i != (*sh1w).end(); seg0++, seg1++, seg2++, sh1w_i++, sh2w_i++, sh3w_i++ ) {
          *sh1w_i = dshape_0[*seg0] * shape_1[*seg1] * shape_2[*seg2];
          *sh2w_i =  shape_0[*seg0] * dshape_1[*seg1] * shape_2[*seg2];
          *sh3w_i =  shape_0[*seg0] * shape_1[*seg1] * dshape_2[*seg2];
        }
      }
      for ( dimen_t d = 0; d < 3; d++ ) { delete [] s2h[d]; }
      delete[] s2h;

      break;
  }
}

//! Lagrange standard Qk hexahedron Reference Element
LagrangeStdHexahedron::LagrangeStdHexahedron(const Interpolation* int_p)
  : LagrangeHexahedron(int_p)
{
  number_t kd(int_p->numtype);
  name_ += "_" + tostring(kd);
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  // build O1 splitting scheme
  splitO1Scheme = splitO1();

  //#ifdef FIG4TEX_OUTPUT
  // Draw a LagrangeStdHexahedron using TeX and fig4TeX
  // if ( kd > 0 ) fig4TeX();
  //#endif
}

//! Lagrange Gauss-Lobatto Qk hexahedron Reference Element
LagrangeGLHexahedron::LagrangeGLHexahedron(const Interpolation* int_p)
  : LagrangeHexahedron(int_p)
{
  // build O1 splitting scheme (needs uniform distribution of points)
  Interpolation stdInterp(_Lagrange, _standard, int_p->numtype, int_p->conformSpace);
  LagrangeStdSegment stdseg(&stdInterp);
  pointCoordinates(&stdseg); // temporary uniform distribution of points
  splitO1Scheme = splitO1();

  number_t kd(int_p->numtype);
  name_ += "Gauss-Lobatto_" + tostring(kd);
  // local coordinates of Points supporting D.o.F
  pointCoordinates();

  //#ifdef FIG4TEX_OUTPUT
  // Draw a LagrangeGLHexahedron using TeX and fig4TeX
  // if ( kd > 0 ) fig4TeX();
  //#endif
}

//! tensorNumberingHexahedron: correspondence between segment and hexahedron local numbering
void tensorNumberingHexahedron(const int interpNum, number_t**& s2h)
{
  int nk = interpNum;
  number_t p = 0, q1 = 0, q2 = 1, q3 = 2, q4 = interpNum, z1 = 1, z2 = 0, z3 = interpNum, z4 = 2;
  number_t q1_p, q2_p, q3_c, q4_c, z1_p, z2_p, z3_c, z4_c;

  while ( nk > 0 )
  {
    // vertices of 'current perimeter'
    s2h[0][p] = q1; s2h[1][p] = q2; s2h[2][p++] = z1;
    s2h[0][p] = q1; s2h[1][p] = q1; s2h[2][p++] = z1;
    s2h[0][p] = q2; s2h[1][p] = q1; s2h[2][p++] = z1;
    s2h[0][p] = q2; s2h[1][p] = q2; s2h[2][p++] = z1;
    s2h[0][p] = q1; s2h[1][p] = q2; s2h[2][p++] = z2;
    s2h[0][p] = q1; s2h[1][p] = q1; s2h[2][p++] = z2;
    s2h[0][p] = q2; s2h[1][p] = q1; s2h[2][p++] = z2;
    s2h[0][p] = q2; s2h[1][p] = q2; s2h[2][p++] = z2;

    //  non-vertex nodes on edges of 'current perimeter'
    q1_p = q1; q2_p = q2; z1_p = z1; z2_p = z2;
    q3_c = q3; q4_c = q4; z3_c = z3; z4_c = z4;
    for ( dimen_t j = 2; j <= nk; j++ )
    {
      s2h[0][p] = q1;     s2h[1][p] = q1;     s2h[2][p++] = z3_c;   //Edge# 2->6, nodes # 9, 21, ...
      s2h[0][p] = q4_c;   s2h[1][p] = q1;     s2h[2][p++] = z2;     //Edge# 7->6, nodes # 10, 22, ...
      s2h[0][p] = q1;     s2h[1][p] = q4_c;   s2h[2][p++] = z2;     //Edge# 5->6, nodes # 11, 23, ...
      s2h[0][p] = q2;     s2h[1][p] = q2;     s2h[2][p++] = z4_c++; //Edge# 8->4, nodes # 12, 24, ...
      s2h[0][p] = q3_c;   s2h[1][p] = q2;     s2h[2][p++] = z1;     //Edge# 1->4, nodes # 13, 25, ...
      s2h[0][p] = q2;     s2h[1][p] = q3_c;   s2h[2][p++] = z1;     //Edge# 3->4, nodes # 14, 26, ...
      s2h[0][p] = q1;     s2h[1][p] = q2;     s2h[2][p++] = z3_c;   //Edge# 1->5, nodes # 15, 27, ...
      s2h[0][p] = q3_c;   s2h[1][p] = q1;     s2h[2][p++] = z1;     //Edge# 2->3, nodes # 16, 28, ...
      s2h[0][p] = q2;     s2h[1][p] = q3_c;   s2h[2][p++] = z2;     //Edge# 7->8, nodes # 17, 29, ...
      s2h[0][p] = q2;     s2h[1][p] = q1;     s2h[2][p++] = z3_c--; //Edge# 3->7, nodes # 18, 30, ...
      s2h[0][p] = q3_c++; s2h[1][p] = q2;     s2h[2][p++] = z2;     //Edge# 5->8, nodes # 19, 31, ...
      s2h[0][p] = q1;     s2h[1][p] = q4_c--; s2h[2][p++] = z1;     //Edge# 1->2, nodes # 20, 32, ...
    }
    q1++; q2--; q3++; q4--; z1--; z2++; z3--; z4++;
    if ( nk == interpNum ) { q1 = 2; q2 = nk; z1 = nk; z2 = 2; }
    nk -= 2;

    // 'current perimeter' internal nodes on faces

    if ( nk >= 0 )
    {
      number_t pbef = p;
      tensorHexahedronSideNumbering(s2h[1], s2h[2], p, nk, q1, q2 , q3 , q4 , 1); // Face 1 : 2 6 5 1 , x = q1_p
      for ( number_t j = pbef; j < p; j++ ) { s2h[0][j] = q1_p; }
      pbef = p;
      tensorHexahedronSideNumbering(s2h[2], s2h[0], p, nk , q1 , q2 , q3 , q4 , 1); // Face 2 : 7 6 2 3 , y = q1_p
      for ( number_t j = pbef; j < p; j++ ) { s2h[1][j] = q1_p; }
      pbef = p;
      tensorHexahedronSideNumbering(s2h[0], s2h[1], p , nk , q1 , q2 , q3 , q4 , 1); // Face 3 : 5 6 7 8 , z = z2_p
      for ( number_t j = pbef; j < p; j++ ) { s2h[2][j] = z2_p; }
      pbef = p;
      tensorHexahedronSideNumbering(s2h[1], s2h[2], p, nk, q1, q2, q3, q4, -1); // Face 4 : 3 4 8 7 , x = q2_p
      for ( number_t j = pbef; j < p; j++ ) { s2h[0][j] = q2_p; }
      pbef = p;
      tensorHexahedronSideNumbering(s2h[2], s2h[0], p, nk, q1, q2, q3, q4, -1); // Face 5 : 8 4 1 5 , y = q2_p
      for ( number_t j = pbef; j < p; j++ ) { s2h[1][j] = q2_p; }
      pbef = p;
      tensorHexahedronSideNumbering(s2h[0], s2h[1], p, nk, q1, q2, q3, q4, -1); // Face 6 : 1 4 3 2 , z = z1_p
      for ( number_t j = pbef; j < p; j++ ) { s2h[2][j] = z1_p; }
    }
  }
  // central point if any
  if ( nk == 0 ) { s2h[0][p] = q1; s2h[1][p] = q1; s2h[2][p++] = q1; }
}

/*!
  correspondence between segment and quadrangular face numbering

  Examples of local numbering of Lagrange Pk 1D elements
  \verbatim
   2----1  2---3---1  2---4---3---1  2---5---4---3---1  2---6---5---4---3---1
     k=1      k=2          k=3              k=4                  k=5
  \endverbatim
 */
void tensorHexahedronSideNumbering(number_t*& sh1, number_t*& sh2, number_t& p, const int interpNum, const number_t qq1, const number_t qq2, const number_t qq3, const number_t qq4, const short int sens)
{
  int nk = interpNum;
  number_t q1 = qq1, q2 = qq2, q3 = qq3, q4 = qq4, q3_p, q4_m;

  if ( sens == 1 )
  {
    while ( nk > 0 )
    {
      //  Les 4 sommets du 'perimetre' en cours
      sh1[p] = q1; sh2[p++] = q2;
      sh1[p] = q1; sh2[p++] = q1;
      sh1[p] = q2; sh2[p++] = q1;
      sh1[p] = q2; sh2[p++] = q2;

      // Les noeuds 'internes' aux aretes du 'perimetre' en cours
      q3_p = q3; q4_m = q4;
      for ( int j = 2; j <= nk; j++ )
      {
        sh1[p] = q1;     sh2[p++] = q4_m;
        sh1[p] = q3_p;   sh2[p++] = q1;
        sh1[p] = q2;     sh2[p++] = q3_p++;
        sh1[p] = q4_m--; sh2[p++] = q2;
      }
      q1++; q2--; q3++; q4--;
      if ( q1 == 2 ) { q1 = qq3; q2 = qq4; }
      nk -= 2;
    }
  }
  else
  {
    while ( nk > 0 )
    {
      sh1[p] = q1; sh2[p++] = q2;
      sh1[p] = q2; sh2[p++] = q2;
      sh1[p] = q2; sh2[p++] = q1;
      sh1[p] = q1; sh2[p++] = q1;

      q3_p = q3; q4_m = q4;
      for ( int j = 2; j <= nk; j++ )
      {
        sh1[p] = q3_p;   sh2[p++] = q2;
        sh1[p] = q2;     sh2[p++] = q4_m;
        sh1[p] = q4_m--; sh2[p++] = q1;
        sh1[p] = q1;     sh2[p++] = q3_p++;
      }
      q1++; q2--; q3++; q4--;
      if ( q1 == 2 ) { q1 = qq3; q2 = qq4; }
      nk -= 2;
    }
  }
  //  central point if any
  if ( nk == 0 ) { sh1[p] = q1; sh2[p++] = q1; }
}

// // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //
// #ifdef FIG4TEX_OUTPUT
// /*
// --------------------------------------------------------------------------------
//    fig4TeX builds a tex file to display local numbering and coordinates
// --------------------------------------------------------------------------------
// */
// void LagrangeHexahedron::fig4TeX()
// {
//   // Number of points on each edge
//   number_t nb_P_Edge = numberOfDOF(1, 1);
//   // Number of points on all edges
//   number_t nb_P_Edges = 4*(nb_P_Edge-1);
//   char b=char(92);

//   std::ofstream os;
//   os.open((fig4TexFileName()).c_str(), ios::out);
//   os <<"%"<<b<< "input fig4tex.tex"<<b<<"input TeXnicolor.tex"<<b<<"input Fonts.tex";
//   os <<endl<<b<<"fourteenpoint"<<b<<"nopagenumbers";
//   os <<endl<<b<<"newbox"<<b<<"mybox";
//   if ( nb_P_Edge < 6 ) os <<endl<<b<<"figinit{250pt,realistic}";
//   else os <<endl<<b<<"figinit{400pt,realistic}";
//   // 3D settings
//   os <<b<<"def"<<b<<"Obdist{17}"<<b<<"def"<<b<<"Valpsi{16}"<<b<<"def"<<b<<"Valtheta{20}"
//      <<endl<<b<<"figsetview("<<b<<"Valpsi,"<<b<<"Valtheta)"<<b<<"figsetobdist("<<b<<"Obdist)";
//   string longName = "Lagrange Hexahedron of degree ";

//   // Draw element and display vertices
//   number_t pts(0);
//   // define vertices
//   fig4TeXVertexPt(os, geom_ref_elt_p->vertices(), pts);

//   // start postscript file
//   os <<endl<<b<<"psbeginfig{}"
//       <<endl<<b<<"pssetwidth{1}"<<b<<"psline[2,3,7,6,2]"
//       <<endl<<b<<"pssetwidth{1}"<<b<<"psline[5,6,7,8,5]"
//       <<endl<<b<<"pssetwidth{1}"<<b<<"psline[1,2,6,5,1]"
//       <<endl<<b<<"pssetdash{7}"<<b<<"psline[1,4,8,4,3]";
//   if ( nb_P_Edge > 3 )
//   {
//      // Draw reduced face on each face
//      for ( number_t fa = 1; fa <= 6 ; fa++ ) fig4TeXSmallFace(os, fa, 4, nb_P_Edges);
//   }
//   os <<endl<<b<<"psendfig"; // end of postcript output

//   os <<endl<<b<<"figvisu{"<<b<<"mybox}{"<< longName << interpolation_p->numtype <<"}{%"
//      <<endl<<b<<"figsetmark{$"<<b<<"figBullet$}"<<b<<"figsetptname{{"<<b<<"bf#1}}";
//    // vertex nodes
//    os <<endl<<b<<"figwritesw1:(5pt)"<<endl<<b<<"figwritese2:(5pt)"
//       <<endl<<b<<"figwritee3:(5pt)"<<endl<<b<<"figwritenw4:(5pt)"
//       <<endl<<b<<"figwritenw5:(5pt)"<<endl<<b<<"figwritese6:(5pt)"
//       <<endl<<b<<"figwritene7:(5pt)"<<endl<<b<<"figwritenw8:(5pt)";

//   if ( nb_P_Edge > 2 )
//   {
//      // nodes on edges
//      number_t ed(0);
//      fig4TeXEdgePt(os , ed++, pts, "e");
//      fig4TeXEdgePt(os , ed++, pts, "se");
//      fig4TeXEdgePt(os , ed++, pts, "s");
//      fig4TeXEdgePt(os , ed++, pts, "w");
//      fig4TeXEdgePt(os , ed++, pts, "nw");
//      fig4TeXEdgePt(os , ed++, pts, "n");
//      fig4TeXEdgePt(os , ed++, pts, "w");
//      fig4TeXEdgePt(os , ed++, pts, "se");
//      fig4TeXEdgePt(os , ed++, pts, "n");
//      fig4TeXEdgePt(os , ed++, pts, "e");
//      fig4TeXEdgePt(os , ed++, pts, "nw");
//      fig4TeXEdgePt(os , ed++, pts, "s");
//      // nodes on faces
//      ed = 0;
//      fig4TeXFacePt(os , ed++, nb_P_Edges, pts, "s");
//      fig4TeXFacePt(os , ed++, nb_P_Edges, pts, "s");
//      fig4TeXFacePt(os , ed++, nb_P_Edges, pts, "s");
//      fig4TeXFacePt(os , ed++, nb_P_Edges, pts, "s");
//      fig4TeXFacePt(os , ed++, nb_P_Edges, pts, "s");
//      fig4TeXFacePt(os , ed++, nb_P_Edges, pts, "s");
//   }
//   // end of output
//   os <<endl<< "}" <<b<<"par"<<b<<"centerline{"<<b<<"box"<<b<<"mybox}" <<endl<<b<<"bye"<<endl;
//   os.close();
// }
// #endif  /* FIG4TEX_OUTPUT */
// // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //

} // end of namespace xlifepp
