/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file GeomRefHexahedron.cpp
	\authors D. Martin, N. Kielbasiewicz
	\since 15 dec 2002
	\date 7 aug 2012

  \brief Implementation of xlifepp::GeomRefHexahedron class members and related functions
 */

#include "GeomRefHexahedron.hpp"
#include "utils.h"

namespace xlifepp
{

//! GeomRefHexahedron constructor for Geometric Reference Element using 3d base constructor with shape, volume, centroid coords, number of vertices & edges
GeomRefHexahedron::GeomRefHexahedron()
  : GeomRefElement(_hexahedron, 1., 0.5, 8, 12)
{
  trace_p->push("GeomRefHexahedron::GeomRefHexahedron");
  // coordinates of vertices
  std::vector<real_t>::iterator it_v(vertices_.begin());
  vertex(it_v, 1., 0., 0.);
  vertex(it_v, 1., 1., 0.);
  vertex(it_v, 0., 1., 0.);
  vertex(it_v, 0., 0., 0.);
  vertex(it_v, 1., 0., 1.);
  vertex(it_v, 1., 1., 1.);
  vertex(it_v, 0., 1., 1.);
  vertex(it_v, 0., 0., 1.);
  // vertex numbering on edges
  sideOfSideNumbering();
  // vertex numbering and oriented edge numbering on faces
  sideNumbering();

  trace_p->pop();
}

GeomRefHexahedron::~GeomRefHexahedron() {}

//--------------------------------------------------------------------------------
// sideOfsideNumbering: defines Geometric Reference Element local numbering of edge vertices
// sideNumbering: defines Geometric Reference Element local numbering of vertices on faces and of edges on faces
//--------------------------------------------------------------------------------
void GeomRefHexahedron::sideOfSideNumbering()
{
  sideOfSideVertexNumbers_[0].push_back(2);
  sideOfSideVertexNumbers_[0].push_back(6);
  sideOfSideVertexNumbers_[1].push_back(7);
  sideOfSideVertexNumbers_[1].push_back(6);
  sideOfSideVertexNumbers_[2].push_back(5);
  sideOfSideVertexNumbers_[2].push_back(6);
  sideOfSideVertexNumbers_[3].push_back(8);
  sideOfSideVertexNumbers_[3].push_back(4);
  sideOfSideVertexNumbers_[4].push_back(1);
  sideOfSideVertexNumbers_[4].push_back(4);
  sideOfSideVertexNumbers_[5].push_back(3);
  sideOfSideVertexNumbers_[5].push_back(4);
  sideOfSideVertexNumbers_[6].push_back(1);
  sideOfSideVertexNumbers_[6].push_back(5);
  sideOfSideVertexNumbers_[7].push_back(2);
  sideOfSideVertexNumbers_[7].push_back(3);
  sideOfSideVertexNumbers_[8].push_back(7);
  sideOfSideVertexNumbers_[8].push_back(8);
  sideOfSideVertexNumbers_[9].push_back(3);
  sideOfSideVertexNumbers_[9].push_back(7);
  sideOfSideVertexNumbers_[10].push_back(5);
  sideOfSideVertexNumbers_[10].push_back(8);
  sideOfSideVertexNumbers_[11].push_back(1);
  sideOfSideVertexNumbers_[11].push_back(2);

  sideOfSideNumbers_[0].push_back(1);
  sideOfSideNumbers_[0].push_back(-3);
  sideOfSideNumbers_[0].push_back(-7);
  sideOfSideNumbers_[0].push_back(12);
  sideOfSideNumbers_[1].push_back(2);
  sideOfSideNumbers_[1].push_back(-1);
  sideOfSideNumbers_[1].push_back(8);
  sideOfSideNumbers_[1].push_back(10);
  sideOfSideNumbers_[2].push_back(3);
  sideOfSideNumbers_[2].push_back(-2);
  sideOfSideNumbers_[2].push_back(9);
  sideOfSideNumbers_[2].push_back(-11);
  sideOfSideNumbers_[3].push_back(6);
  sideOfSideNumbers_[3].push_back(-4);
  sideOfSideNumbers_[3].push_back(-9);
  sideOfSideNumbers_[3].push_back(-10);
  sideOfSideNumbers_[4].push_back(4);
  sideOfSideNumbers_[4].push_back(-5);
  sideOfSideNumbers_[4].push_back(7);
  sideOfSideNumbers_[4].push_back(11);
  sideOfSideNumbers_[5].push_back(5);
  sideOfSideNumbers_[5].push_back(-6);
  sideOfSideNumbers_[5].push_back(-8);
  sideOfSideNumbers_[5].push_back(-12);

}

void GeomRefHexahedron::sideNumbering()
{
  for (number_t i = 0; i < nbSides_; i++)
    {
      sideShapeTypes_[i] = _quadrangle;
    }

  sideVertexNumbers_[0].push_back(2);
  sideVertexNumbers_[0].push_back(6);
  sideVertexNumbers_[0].push_back(5);
  sideVertexNumbers_[0].push_back(1);
  sideVertexNumbers_[1].push_back(7);
  sideVertexNumbers_[1].push_back(6);
  sideVertexNumbers_[1].push_back(2);
  sideVertexNumbers_[1].push_back(3);
  sideVertexNumbers_[2].push_back(5);
  sideVertexNumbers_[2].push_back(6);
  sideVertexNumbers_[2].push_back(7);
  sideVertexNumbers_[2].push_back(8);
  sideVertexNumbers_[3].push_back(3);
  sideVertexNumbers_[3].push_back(4);
  sideVertexNumbers_[3].push_back(8);
  sideVertexNumbers_[3].push_back(7);
  sideVertexNumbers_[4].push_back(8);
  sideVertexNumbers_[4].push_back(4);
  sideVertexNumbers_[4].push_back(1);
  sideVertexNumbers_[4].push_back(5);
  sideVertexNumbers_[5].push_back(1);
  sideVertexNumbers_[5].push_back(4);
  sideVertexNumbers_[5].push_back(3);
  sideVertexNumbers_[5].push_back(2);
}

//! Returns edge length, face area or element volume
real_t GeomRefHexahedron::measure(const dimen_t d, const number_t sideNum) const
{
  real_t ms(0.);
  switch ( d )
    {
      case 0: ms = 1.; break;
      case 1:
        if ( sideNum >= 1 && sideNum <= nbSideOfSides_ ) { ms = 1.; }
        else { noSuchSideOfSide(sideNum); }
        break;
      case 2:
        if ( sideNum >= 1 && sideNum <= nbSides_ ) { ms = 1.; }
        else { noSuchSide(sideNum); }
        break;
      case 3: ms = measure_;
      default: break;
    }
  return ms;
}

// /*
// --------------------------------------------------------------------------------
//   Returns a quadrature rule built on an edge from a 1d quadrature formula
//   or a quadrature rule built on an face from a quadrangle quadrature formula
// --------------------------------------------------------------------------------
// */
// void GeomRefHexahedron::sideQuadrature(const QuadratureRule& q1, QuadratureRule& qr, const number_t sideNum, const dimen_t d) const
// {
//   /*
//      q1 : input 1d or 2d (quadrangle) quadrature rule
//      q2 : ouput 3d quadrature rule set on the edge or face
//      sideNum: local number of edge (sideNum = 1,2,3,...,12) or face (sideNum = 1,2,3,4,5,6)
//      d: side dimension (d = 1 for an edge, d = 2 for a face)
//   */
//   std::vector<real_t>::const_iterator c_1(q1.point()), w_1(q1.weight());
//   std::vector<real_t>::iterator c_i(qr.point()), w_i(qr.weight());

//   switch ( d )
//   {
//      case 1:
//         switch ( sideNum )
//         {
//            case 1: // edge x1=1 x2=1
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 1., 1., *c_1, w_i, *w_1 );
//               break;
//            case 2: // edge x2=1 x3=1
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, 1., 1., w_i, *w_1 );
//               break;
//            case 3: // edge x1=1 x3=1
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 1., *c_1, 1., w_i, *w_1 );
//               break;
//            case 4: // edge x1=0 x2=0
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 0., 0., *c_1, w_i, *w_1 );
//               break;
//            case 5: // edge x2=0 x3=0
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, 0., 0., w_i, *w_1 );
//               break;
//            case 6: // edge x1=0 x3=0
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 0., *c_1, 0., w_i, *w_1 );
//               break;
//            case 7: // x2=1 x3=0
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 1., *c_1, 0., w_i, *w_1 );
//               break;
//            case 8: // x3=1 x1=0
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 0., *c_1, 1., w_i, *w_1 );
//               break;
//            case 9: // x1=1 x2=0
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 1., 0., *c_1, w_i, *w_1 );
//               break;
//            case 10: // x2=0 x3=1
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, 0., 1., w_i, *w_1 );
//               break;
//            case 11: // x3=0 x1=1
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 1., *c_1, 0., w_i, *w_1 );
//               break;
//            case 12: // x1=0 x2=1
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 0., 1., *c_1, w_i, *w_1 );
//               break;
//            default: noSuchSideOfSide(sideNum); break;
//         }
//         break;
//      case 2:
//         switch ( sideNum )
//         {
//            case 1: // face (x1 = 1)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 1., *(c_1+1), *c_1, w_i, *w_1);
//               break;
//            case 2: // face (x2 = 1)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, 1., *(c_1+1), w_i, *w_1);
//               break;
//            case 3: // face (x3 = 1)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, *(c_1+1), 1., w_i, *w_1);
//               break;
//            case 4: // face (x1 = 0)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, 0., *(c_1+1), *c_1, w_i, *w_1);
//               break;
//            case 5: // face (x2 = 0)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, 0., *(c_1+1), w_i, *w_1);
//               break;
//            case 6: // face (x3 = 0)
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1 += d )
//                  qr.point(c_i, *c_1, *(c_1+1), 0., w_i, *w_1);
//               break;
//            default: noSuchSide(sideNum); break;
//         }
//         break;
//      default:
//         break;
//   }
// }

//! returns tangent vector(s) on edge sideNum (=1,2, ...12) or face sideNum (=1,2,...,6)
void GeomRefHexahedron::tangentVector(const std::vector<real_t>& jacobianMatrix, std::vector <std::vector<real_t> >& tgv, const number_t sideNum, const dimen_t d) const
{
  std::vector<real_t>::iterator tv0_i(tgv[0].begin()), tv1_i(tv0_i);
  if ( d > 1) { tv1_i = tgv[1].begin(); }
//  std::vector<real_t>::const_iterator jm_i(jacobianMatrix.begin());

  noSuchFunction("tangentVector");
  switch ( d )
    {
      case 2:
        switch ( sideNum )
          {
            case 1: // face (x1 = 1)            : 2 -> 6 -> 5 -> 1
              break;
            case 2: // face (x2 = 1)            : 7 -> 6 -> 2 -> 3
              break;
            case 3: // face (x3 = 1)            : 5 -> 6 -> 7 -> 8
              break;
            case 4: // face (x1 = 0)            : 3 -> 4 -> 8 -> 7
              break;
            case 5: // face (x2 = 0)            : 8 -> 4 -> 1 -> 5
              break;
            case 6: // face (x3 = 0)            : 1 -> 4 -> 3 -> 2
              break;
            default: noSuchSide(sideNum);
              break;
          }
      case 1:
        switch ( sideNum )
          {
            case 1: // edge (x1 = 1 ; x2 = 1)   : 2 -> 6
              break;
            case 2: // edge (x2 = 1 ; x3 = 1)   : 7 -> 6
              break;
            case 3: // edge (x3 = 1 ; x1 = 1)   : 5 -> 6
              break;
            case 4: // edge (x1 = 0 ; x2 = 0)   : 8 -> 4
              break;
            case 5: // edge (x2 = 0 ; x3 = 0)   : 1 -> 4
              break;
            case 6: // edge (x3 = 0 ; x1 = 0)   : 3 -> 4
              break;
            case 7: // edge (x1 = 1 ; x2 = 0)   : 1 -> 5
              break;
            case 8: // edge (x2 = 1 ; x3 = 0)   : 2 -> 3
              break;
            case 9: // edge (x3 = 1 ; x1 = 0)   : 7 -> 8
              break;
            case 10: // edge (x1 = 0 ; x2 = 1)   : 3 -> 7
              break;
            case 11: // edge (x2 = 0 ; x3 = 1)   : 5 -> 8
              break;
            case 12: // edge (x3 = 0 ; x1 = 1)   : 1 -> 2
              break;
            default: noSuchSideOfSide(sideNum);
              break;
          }
        break;
      default:
        break;
    }
}

//! returns local number of edge bearing vertices with local numbers v1 and v2
number_t GeomRefHexahedron::sideWithVertices(const number_t vn1, const number_t vn2) const
{
  if(vn1==vn2) noSuchSide(vn1,vn2);
  number_t v1=vn1, v2=vn2;
  if(v1>v2) {v1=vn2; v2=vn1;}
  switch(v1)
    {
      case 1:
        {
          if(v2==2) return 12;
          if(v2==4) return 5;
          if(v2==5) return 7;
        }
        break;
      case 2:
        {
          if(v2==3) return 8;
          if(v2==6) return 1;
        }
        break;
      case 3:
        {
          if(v2==4) return 6;
          if(v2==7) return 10;
        }
        break;
      case 4: if(v2==8) return 4; break;
      case 5:
        {
          if(v2==6) return 3;
          if(v2==8) return 11;
        }
        break;
      case 6:if(v2==7) return 2; break;
      case 7:if(v2==8) return 9;
    }
  noSuchSide(vn1,vn2);
  return 0;
}
/*! returns local number of face bearing vertices with local numbers v1, v2, v3 and v4 if not null
    1:  2   6   5   1   -> 1 2 5 6  100v1+10v2+v3  125 126 156 256
    2:  7   6   2   3   -> 2 3 6 7                 236 237 267 367
    3:  5   6   7   8   -> 5 6 7 8                 567 568 578 678
    4:  3   4   8   7   -> 3 4 7 8                 347 348 378 478
    5:  8   4   1   5   -> 1 4 5 8                 145 148 158 458
    6:  1   4   3   2   -> 1 2 3 4                 123 124 134 234
*/
number_t GeomRefHexahedron::sideWithVertices(const number_t vn1, const number_t vn2, const number_t vn3, const number_t vn4) const
{
  std::set<number_t> vs; vs.insert(vn1); vs.insert(vn2); vs.insert(vn3);
  if(vn4>0) vs.insert(vn4);
  if(*vs.begin()<1 || *vs.rbegin()>8)
  {
      if(vn4==0) noSuchSide(vn1,vn2,vn3); else noSuchSide(vn1,vn2,vn3,vn4);
      return 0;
  }
  std::set<number_t>::iterator itv=vs.begin();
  std::map<number_t,number_t> hm;
  std::map<number_t,number_t>::iterator itm;
  number_t s=0;
  if(vn4==0)
  {
    hm[125]=1; hm[126]=1; hm[156]=1; hm[256]=1; hm[236]=2; hm[237]=2; hm[267]=2; hm[367]=2;
    hm[567]=3; hm[568]=3; hm[578]=3; hm[678]=3; hm[347]=4; hm[348]=4; hm[378]=4; hm[478]=4;
    hm[145]=5; hm[148]=5; hm[158]=5; hm[458]=5; hm[123]=6; hm[124]=6; hm[134]=6; hm[234]=6;
    s=100**itv++; s+=10**itv++; s+=*itv;
    itm=hm.find(s);
    if(itm!=hm.end()) return itm->second; else noSuchSide(vn1,vn2,vn3);
  }
  hm[1256]=1; hm[2367]=2; hm[5678]=3; hm[3478]=4; hm[1458]=5; hm[1234]=6;
  s=1000**itv++; s+=100**itv++; s+=10**itv++;s+=*itv;
  itm=hm.find(s);
  if(itm!=hm.end()) return itm->second; else noSuchSide(vn1,vn2,vn3,vn4);
  return 0;
}

//! test if a point belongs to current element
bool GeomRefHexahedron::contains(std::vector<real_t>& p, real_t tol) const
{
  real_t x=p[0], y=p[1], z=p[2];
  return (x >= -tol) && (x <= 1.+tol) && (y >= -tol) && (y <= 1.+tol) && (z >= -tol) && (z <= 1.+tol);
}

//! return projection on ref hexahedron
std::vector<real_t> GeomRefHexahedron::projection(const std::vector<real_t>& p, real_t& h) const
{
  real_t tol=theTolerance;
  real_t x=p[0], y=p[1], z=p[2];
  std::vector<real_t> q(3,0.);
  bool in = true;
  if(x<=-tol) {q[0]=0.; in=false;}
  else if(x>=1+tol) {q[0]=1.; in=false;}
  else q[0]=x;
  if(y<=-tol) {q[1]=0;; in=false;}
  else if(y>=1+tol) {q[1]=1;; in=false;}
  else q[1]=y;
  if(z<=-tol) {q[2]=0;; in=false;}
  else if(z>=1+tol) {q[2]=1;; in=false;}
  else q[2]=z;
  if(in) h=0; else h=norm(p-q);
  return q;
}

} // end of namespace xlifepp
