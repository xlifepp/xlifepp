/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file NedelecEdgeHexahedron.hpp
  \authors E. Lunéville
  \since 5 march 2018
  \date  5 march 2018

  \brief Definition of the xlifepp::NedelecEdgeHexahedron class

  xlifepp::NedelecEdgeHexahedron defines Nedelec HCurl-conforming edge elements interpolation data on hexahedron elements
  There are 2 families:
  - the first family (NCE1k) is the extension to hexahedron of the first family element  defined on a quadrangle (RTCEk/NSE1k)
  - the second family (NCE2k also named AAek) is the extension to hexahedron of the second family element defined on a quadrangle (BDMCEk/NSE2k)

   member functions
  ----------------
    - interp: interpolation data
    - sideNumbering: local numbering on edges (side)
    - sideDofsMap: side dofs mapping used when assembling
    - computeShapeFunctions: compute shape functions as polynomials (only for any order family)
    - computeShapeValues: evaluate shape functions at a point

  Child classes:
    - xlifepp::NedelecEdgeFirstHexahedronPk -> Nedelec first family of any order on hexahedron (NCE1k)
    - xlifepp::NedelecEdgeSecondHexahedronPk -> Nedelec second family of any order on hexrahedron (NCE2k)
*/

#ifndef NEDELEC_EDGE_HEXAHEDRON_HPP
#define NEDELEC_EDGE_HEXAHEDRON_HPP

#include "config.h"
#include "RefHexahedron.hpp"

namespace xlifepp
{

/*!
  \class NedelecEdgeHexahedron
  (Hcurl-conforming edge elements )

  Parent class: RefTriangle
  Child classes: NedelecEdgeFirstHexahedronPk, NedelecEdgeSecondHexahedronPk
 */
class NedelecEdgeHexahedron : public RefHexahedron
{
  public:
    NedelecEdgeHexahedron(const Interpolation* interp_p);
    virtual ~NedelecEdgeHexahedron();
}; // end of class NedelecEdgeHexahedron

/*!
  \class NedelecEdgeFirstHexahedronPk
  Nedelec edge first family of any order k on tetrahedron t (NCE1k)
     space  Vk: Q_(k-1,k,k) x Q_(k,k-1,k)x Q_(k,k,k-1), Q_(k,l,m) monomials of degree at most k in x1, of degree at most l in x2, of degree at most m in x3, 3k(k+1)^2 dofs
     edge dofs: v-> int_e v.t q,     q in P_(k-1)[e]                 k dofs by edge e
     face dofs (k>1)       : v-> int_f (v x n).q, q in Q_(k-2,k-1) x Q_(k-1,k-2)  2k(k-1) dofs by face
     hexahedron dofs (k>1) : v-> int_t v.q,       q in Q_(k-1,k-2,k-2) x Q_(k-2,k-1,k-2)x Q_(k-2,k-2,k-1)   3k(k-1)^2 dofs
 */
class NedelecEdgeFirstHexahedronPk : public NedelecEdgeHexahedron
{
  public:
    NedelecEdgeFirstHexahedronPk(const Interpolation* int_p);
    ~NedelecEdgeFirstHexahedronPk();
    void interpolationData();  //!< defines reference element interpolation data
    void sideNumbering();      //!< local numbering on faces (side)
    void sideOfSideNumbering();//!< local numbering on edges (side of side)
    void pointCoordinates();   //!< builds virtual coordinates of moment dofs
    number_t sideDofsMap(const number_t& n, const number_t& i,
                         const number_t& j, const number_t& k=0) const;  //!< face dofs mapping
    number_t sideofsideDofsMap(const number_t& n, const number_t& i, const number_t& j) const; //!< edge dof mapping
    void computeShapeFunctions();                                        //!< compute shape functions as polynomials

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool der1 = true, const bool der2=false) const;          //!< compute shape values at a point
    void rotateDofs(const std::vector<number_t>&, ShapeValues& shv,
                    const bool der1 = true, const bool der2=false) const; //!< rotation of face dofs to ensure matching of dofs on shared face
    virtual Value evalFaceDof(const FeDof& dof, const GeomElement& selt, const Function& f, const Function& gradf, const Function& grad2f) const; //!< specific eval face dof function
    virtual Value evalEltDof (const FeDof& dof, const Function& f, const Function& gradf, const Function& grad2f) const; //!< specific eval elt dof
  private:
    number_t nbshfcts_;
    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class NedelecEdgeFirstHexahedronPk


/*!
  \class NedelecEdgeSecondHexahedronPk NOT AVAILABLE
  Nedelec edge second family of any order k on tetrahedron t (NCE2k/AAEk)
     space  Vk    P^3_(k)   (k+1)(k+2)(k+3)/2 dofs
     edge dofs: v-> int_e v.t q, q in P_(k)[e]                                   k+1  dofs by edge e
     face dofs (k>1)       : v-> int_f v.q,   q in D_(k-1)[f] = P_(k-2)^2 + PH_(k-2)*x       (k-1)(k+1) dofs  by face
     hexahedron dofs (k>3) : v-> int_t v.q,   q in D_(k-2)[t] = P_(k-3)^3 + PH_(k-3)*x       (k-1)(k+1)(k-2)/2 dofs
 */
class NedelecEdgeSecondHexahedronPk : public NedelecEdgeHexahedron
{
  public:
    NedelecEdgeSecondHexahedronPk(const Interpolation* int_p);
    ~NedelecEdgeSecondHexahedronPk();
//    void interpolationData();  //!< defines reference element interpolation data
//    void sideNumbering();      //!< local numbering on edges (side)
//    void pointCoordinates();   //!< builds virtual coordinates of moment dofs
//    number_t sideDofsMap(const number_t& n, const number_t& i,
//                         const number_t& j, const number_t& k=0) const;  //!< side dofs mapping
//    void computeShapeFunctions();                                        //!< compute shape functions as polynomials
//
//    void computeShapeValues(std::vector<real_t>::const_iterator it_pt,
//                            const bool withDeriv = true) const;          //!< compute shape values at a point
//  private:
//    number_t nbshfcts_;
//
//    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class NedelecEdgeSecondHexahedronPk


} // end of namespace xlifepp

#endif /* NEDELEC_EDGE_HEXAHEDRON_HPP */
