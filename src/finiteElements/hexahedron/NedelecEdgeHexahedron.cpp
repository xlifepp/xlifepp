/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file NedelecEdgeHexahedron.cpp
  \authors E. Lunéville
  \since 5 march 2018
  \date  5 march 2018

  \brief Implementation of xlifepp::NedelecEdgeHexahedron class members and related functions
 */

#include "NedelecEdgeHexahedron.hpp"
#include "../Interpolation.hpp"
#include "../integration/Quadrature.hpp"
#include "../triangle/LagrangeTriangle.hpp"
#include "../segment/LagrangeSegment.hpp"
#include "LagrangeHexahedron.hpp"
#include "utils.h"
#include <iostream>

namespace xlifepp
{

//! NedelecEdgeHexahedron constructor
NedelecEdgeHexahedron::NedelecEdgeHexahedron(const Interpolation* interp_p)
  : RefHexahedron(interp_p)
{
  name_ += "_Nedelec_Cube_Edge_";
  mapType = _covariantPiolaMap;
  dofCompatibility=_signDofCompatibility;
  dimShapeFunction=3;
}

NedelecEdgeHexahedron::~NedelecEdgeHexahedron() {}

//=====================================================================================
/*! Nedelec edge first family of order k on hexahedron (NE1k)
     Degrees Of Freedom of reference element, defined as moment D.o.Fs
     space  Vk: Q_(k-1,k,k) x Q_(k,k-1,k)x Q_(k,k,k-1),
                     Q_(k,l,m) monomials of degree at most k in x1, at most l in x2, at most m in x3, 3k(k+1)^2 dofs
     edge dofs: u-> int_e u.t q,     q in P_(k-1)[e]                 k dofs by edge e
     face dofs (k>1)       : u-> int_f (u x n).q, q in Q_(k-2,k-1) x Q_(k-1,k-2)  2k(k-1) dofs by face
     tetrahedron dofs (k>1): u-> int_t u.q,       q in Q_(k-1,k-2,k-2) x Q_(k-2,k-1,k-2)x Q_(k-2,k-2,k-1)   3k(k-1)^2 dofs
     numbering of D.oFs are given by the basis functions order of dual spaces defining D.o.Fs beginning by edges, then faces and hexahedron

     NOTE: moment D.o.Fs can be reinterpreted as punctual D.o.Fs using a well adapted quadrature formulae
            we do not use this representation here
  */
//=====================================================================================
NedelecEdgeFirstHexahedronPk::NedelecEdgeFirstHexahedronPk(const Interpolation* interp_p)
  : NedelecEdgeHexahedron(interp_p)
{
  name_ += "first family_"+tostring(interp_p->numtype);
  interpolationData();    // build element interpolation data
  sideOfSideNumbering();  // local numbering of dofs on edges
  sideNumbering();        // local numbering of dofs on faces
  pointCoordinates();     // "virtual coordinates" of moment dofs
  //sideRefElement();       // Reference Element on edges
  reverseEdge = true;     // tells to reverse edge orientation when evaluating edge dof using general algorithm in Dof class
}

NedelecEdgeFirstHexahedronPk::~NedelecEdgeFirstHexahedronPk() {}

//! interp defines Reference Element interpolation data
void NedelecEdgeFirstHexahedronPk::interpolationData()
{
  trace_p->push("NedelecEdgeFirstHexahedronPk::interpolationData");

  number_t k = interpolation_p->numtype;
  nbDofs_= 3*k*(k+1)*(k+1);
  nbDofsInSideOfSides_=12*k;  //edge
  nbDofsInSides_=12*k*(k-1);  //face
  nbInternalDofs_=3*k*(k-1)*(k-1); //volume
  refDofs.reserve(nbDofs_);
  number_t nbds=nbDofsInSides_/6, nbdss=nbDofsInSideOfSides_/12;
  for(number_t e = 1; e <= 12; ++e)        //edge dofs (rank in dual polynoms basis is used to identify dof)
    for(number_t i=0; i<nbdss; ++i)
      refDofs.push_back(new RefDof(this, true, _onEdge, e, i+1, 3, 1, 0, 3, 0, _crossnProjection, _ncross, "int_e u.t q"));
  if(k>1)
    {
      for(number_t f = 1; f <= 6; ++f)              //face dofs (rank in dual polynoms basis is used to identify dof)
        for(number_t i=0; i<nbds; ++i)              //declare with dotnProjection to store later the face normal
          refDofs.push_back(new RefDof(this, true, _onFace, f, i+1, 3, 2, 0, 3, 0, _dotnProjection, _ncross, "int_f (u x n).q"));
      for(number_t i=0; i<nbInternalDofs_; ++i) //element dofs (order = i is used to identify dof)
        refDofs.push_back(new RefDof(this, false, _onElement, 0, i+1, 3, 3, 0, 3, 0, _noProjection, _id, "int_k u.q"));
    }

  // compute shape functions as formal polynomials
  computeShapeFunctions();
  buildPolynomialTree();   //construct tree representation
  maxDegree = Wk.degree();

  // compute number of shape functions
  nbshfcts_ = shapeValueSize();

  // requires face dofs rotation
  rotateDof = (k>1);

  trace_p->pop();
}

/*! compute shape functions as polynomials using general algorithm
     edge dofs: u-> int_e u.t q,     q in P_(k-1)[e]                                           k dofs by edge e
     face dofs (k>1)       : u-> int_f (u x n).q, q in Q_(k-2,k-1) x Q_(k-1,k-2)                            2k(k-1) dofs by face
     hexahedron dofs (k>1) : u-> int_t u.q,       q in Q_(k-1,k-2,k-2) x Q_(k-2,k-1,k-2)x Q_(k-2,k-2,k-1)   3k(k-1)^2 dofs
*/
/*     mapping from ref. quadrangle ((1,0),(1,1), (0,1),(0,0)) onto hexahedron face is chosen as follows on face(i,j,k,l)
                                            on face 1 (2,6,5,1) : (1,1-y,x)   t1=(0,0,1)  t2=(0,-1,0)
                                            on face 2 (7,6,2,3) : (x,1,1-y)   t1=(1,0,0)  t2=(0,0,-1)
      (x,y) -> (1-x-y)*Vi + x*Vj +y*Vl:    on face 3 (5,6,7,8) : (1-y,x,1)   t1=(0,1,0)  t2=(-1,0,0)
               Vi + x(Vj-Vi) + y(Vl-Vi)     on face 4 (3,4,8,7) : (0,1-x,y)   t1=(0,-1,0) t2=(0,0,1)
               Vi + xTij + yTil             on face 5 (8,4,1,5) : (y,0,1-x)   t1=(0,0,-1) t2=(1,0,0)
                                            on face 6 (1,4,3,2) : (1-x,y,0)   t1=(-1,0,0) t2=(0,1,0)
    mapping from ref. segment [0,1] onto tetrahedron edge is chosen as follows on edge (i,j)
                                     on edge 1  (2,6) : (1,1,1-x)
                                     on edge 2  (7,6) : (1-x,1,1)
                                     on edge 3  (5,6) : (1,1-x,1)
                                     on edge 4  (8,4) : (0,0,x)
                                     on edge 5  (1,4) : (x,0,0)
       x -> x*Vi + (1-x)*Vj:     on edge 6  (3,4) : (0,x,0)
                                     on edge 7  (1,5) : (1,0,1-x)
                                     on edge 8  (2,3) : (x,1,0)
                                     on edge 9  (7,8) : (0,x,1)
                                     on edge 10 (3,7) : (0,1,1-x)
                                     on edge 11 (5,8) : (x,0,1)
                                     on edge 12 (1,2) : (1,1-x,0)
*/
void NedelecEdgeFirstHexahedronPk::computeShapeFunctions()
{
  number_t k = interpolation_p->numtype;
  PolynomialsBasis::iterator itp, itq;
  std::vector<real_t>::const_iterator itpt, itw; //for quadrature rule
  PolynomialsBasis Vk(_DQ3k,3,k);     //Nedelec first family polynomials space DQ3k = Qk-1,k,k x Qk,k-1,k x Qk,k,k-1
  Matrix<real_t> L(nbDofs_,nbDofs_);  //matrix of linear system of shape functions

  //EDGES: compute Matrix Lij=l(pi,qj) for all pi in Vk, for all qj in P_(k-1)[e]
  LagrangeStdSegment segPk(findInterpolation(_Lagrange, _standard, k-1, H1));
  segPk.computeShapeFunctions();
  Ek=segPk.Wk;       //dual space for edge dofs: P_(k-1)[x]
  dimen_t d=Vk.degree()+Ek.degree();
  Quadrature* quad=findBestQuadrature(_segment, d);  //quadrature rule on segment of order d (integrates exactly x^d)
  number_t nbq= quad->numberOfPoints();
  number_t j=1;
  for(itp=Vk.begin(); itp!=Vk.end(); ++itp, ++j)
    {
      number_t i=1;
      for(number_t e = 1; e <= 12; ++e)
        {
          Vector<real_t> t(3,0.);   //unit tangential vector chosen as (vj-vi)/|vj-vi| on edge (vi,vj)
          switch(e)
            {
              case 1: t[2]=1.; break; //edge (2,6)
              case 2: t[0]=1.; break; //edge (7,6)
              case 3: t[1]=1.; break; //edge (5,6)
              case 4: t[2]=-1.;break; //edge (8,4)
              case 5: t[0]=-1.;break; //edge (1,4)
              case 6: t[1]=-1.;break; //edge (3,4)
              case 7: t[2]=1.; break; //edge (1,5)
              case 8: t[0]=-1.;break; //edge (2,3)
              case 9: t[1]=-1.;break; //edge (7,8)
              case 10: t[2]=1.; break; //edge (3,7)
              case 11: t[0]=-1.;break; //edge (5,8)
              default: t[1]=1.; break; //edge (1,2)
            }
          Polynomial pt=dot(*itp,t);
          for(itq=Ek.begin(); itq!=Ek.end(); ++itq, ++i)
            {
              L(i,j)=0;
              itpt=quad->point();
              itw=quad->weight();
              Polynomial& pq=(*itq)[0];
              for(number_t q=0; q<nbq; ++q, ++itw)   //compute by 1D quadrature
                {
                  real_t x=*itpt++;     //quadrature point
                  switch(e)
                    {
                      case 1: L(i,j)+= pt(1,1,1-x) * pq(x)** itw ; break;  //on edge (2,6) : x -> (1,1,1-x)
                      case 2: L(i,j)+= pt(1-x,1,1) * pq(x)** itw ; break;  //on edge (7,6) : x -> (1-x,1,1)
                      case 3: L(i,j)+= pt(1,1-x,1) * pq(x)** itw ; break;  //on edge (5,6) : x -> (1,1-x,1)
                      case 4: L(i,j)+= pt(0,0,x) * pq(x)** itw ;   break;  //on edge (8,4) : x -> (0,0,x)
                      case 5: L(i,j)+= pt(x,0,0) * pq(x)** itw ;   break;  //on edge (1,4) : x -> (x,0,0)
                      case 6: L(i,j)+= pt(0,x,0) * pq(x)** itw ;   break;  //on edge (3,4) : x -> (0,x,0)
                      case 7: L(i,j)+= pt(1,0,1-x) * pq(x)** itw ; break;  //on edge (1,5) : x -> (1,0,1-x)
                      case 8: L(i,j)+= pt(x,1,0) * pq(x)** itw ;   break;  //on edge (2,3) : x -> (x,1,0)
                      case 9: L(i,j)+= pt(0,x,1) * pq(x)** itw ;   break;  //on edge (7,8) : x -> (0,x,1)
                      case 10: L(i,j)+= pt(0,1,1-x) * pq(x)** itw ; break;  //on edge (3,7) : x -> (0,1,1-x)
                      case 11: L(i,j)+= pt(x,0,1) * pq(x)** itw ;   break;  //on edge (5,8) : x -> (x,0,1)
                      default: L(i,j)+= pt(1,1-x,0) * pq(x)** itw ; break;  //on edge (1,2) : x -> (1,1-x,0)
                    }
                }
            }
        }
    }

  if(k>1)  //dofs on FACES and HEXAHEDRON
    {
     // FACES: compute Matrix Lij=l(pi,qj)=int_f (pi x n).qj for all pi in Vk, qj in Q_(k-2,k-1) x Q_(k-1,k-2)
     /* construction of Q_(k-2,k-1) x Q_(k-1,k-2)   by face
         S ref. square,  F mapping from S to f: (x,y) -> (1-x-y)*Vi + x*Vj +y*Vl
         (ui,vi)  basis of Q_(k-2,k-1) x Q_(k-1,k-2) [S]
        Note: in order to correctly match dofs on shared face some rotation will be required (see rotateDofs function) */

      PolynomialsBasis Fk(_DQ3k,2,k-1);
      //thePrintStream<<Fk<<eol;
      std::list<std::vector<Polynomial> >::iterator itq2;
      dimen_t d=Vk.degree()+Fk.degree();
      Quadrature* quad=findBestQuadrature(_quadrangle, d);  //quadrature rule on square of order d (integrates exactly x^d)
      number_t nbq= quad->numberOfPoints();
      number_t j=1;
      for(itp=Vk.begin(); itp!=Vk.end(); ++itp, ++j)
        {
          number_t i=nbDofsInSideOfSides_+1;
          L(i,j)=0;
          std::vector<Polynomial>& pj=*itp;  //j th shape function
          for(number_t f = 1; f <= 6; ++f)
            {
              std::vector<real_t> n(3,0.);   //unit outward normal vector
              switch(f)
                {
                  case 1: n[0]=1.;  break; //face (2,6,5,1)
                  case 2: n[1]=1.;  break; //face (7,6,2,3)
                  case 3: n[2]=1.;  break; //face (5,6,7,8)
                  case 4: n[0]=-1.; break; //face (3,4,8,7)
                  case 5: n[1]=-1.; break; //face (8,4,1,5)
                  default: n[2]=-1.;        //face (1,4,3,2)
                }
              for(itq2=Fk.begin(); itq2!=Fk.end(); ++itq2, i++)
                {
                  L(i,j)=0;
                  itpt=quad->point();
                  itw=quad->weight();
                  std::vector<Polynomial>& qi=*itq2;
                  for(number_t q=0; q<nbq; ++q, ++itw)   //2D quadrature
                    {
                      real_t x=*itpt++, y=*itpt++;       //quadrature point
                      real_t x1,x2,x3;
                      real_t qi1=qi[0].eval(x,y), qi2=qi[1].eval(x,y); //polynomial q on ref quadrangle
                      std::vector<real_t> qis(3,0.); //=qi1*Tij+qi2*Til=qi1*(Vj-Vi)+qi2*(Vl-Vi)
                      switch(f)
                        {
                          case 1: x1=1; x2=1-y; x3=x; qis[0]=0;   qis[1]=-qi2; qis[2]=qi1; break; //face (2,6,5,1), x1=1
                          case 2: x1=x; x2=1; x3=1-y; qis[0]=qi1; qis[1]=0;   qis[2]=-qi2; break; //face (7,6,2,3), x2=1
                          case 3: x1=1-y; x2=x; x3=1; qis[0]=-qi2; qis[1]=qi1; qis[2]=0;   break; //face (5,6,7,8), x3=1
                          case 4: x1=0; x2=1-x; x3=y; qis[0]=0;   qis[1]=-qi1; qis[2]=qi2; break; //face (3,4,8,7), x1=0
                          case 5: x1=y; x2=0; x3=1-x; qis[0]=qi2; qis[1]=0;   qis[2]=-qi1; break; //face (8,4,1,5), x2=0
                          default: x1=1-x; x2=y; x3=0; qis[0]=-qi1; qis[1]=qi2; qis[2]=0;          //face (1,4,3,2), x3=0
                        }
                        real_t r=0;
                        L(i,j)+=*itw * std::inner_product(qis.begin(),qis.end(),crossProduct(eval(pj,x1,x2,x3),n).begin(),r);
                    }
                }
            }
        }

      // HEXAHEDRON: compute Matrix Lij=l(pi,qj)=int_h pi.qj for all pi in Vk, qj in Q_(k-1,k-2,k-2) x Q_(k-2,k-1,k-2)x Q_(k-2,k-2,k-1)
      //Kk=PolynomialsBasis(_DQk,3,k-1);
      PolynomialsBasis Kk(_DQk,3,k-1);
      //thePrintStream<<Kk<<eol;
      PolynomialsBasis::iterator itr;
      d=Vk.degree()+Kk.degree();
      quad=findBestQuadrature(_hexahedron,d);  //quadrature rule on hexahedron of order d
      nbq= quad->numberOfPoints();
      j=1;
      for(itp=Vk.begin(); itp!=Vk.end(); ++itp,++j)
        {
          number_t i= nbDofsInSideOfSides_+ nbDofsInSides_+ 1;

          for(itr=Kk.begin(); itr!=Kk.end(); ++itr,++i)
            {
              L(i,j)=0.;
              itpt=quad->point();
              itw=quad->weight();
              for(number_t q=0; q<nbq; ++q, ++itw)
                {
                  real_t x=*itpt++, y=*itpt++, z=*itpt++, r=0;
                  std::vector<real_t> vp=eval(*itp,x,y,z);
                  L(i,j)+= *itw * std::inner_product(vp.begin(),vp.end(),eval(*itr,x,y,z).begin(),r);
                }
            }
        }
    }
    L.saveToFile("L.mat");
  //compute coefficients of shape functions by solving linear systems
  Matrix<real_t> sf(nbDofs_,_idMatrix);
  real_t eps=theZeroThreshold;
  number_t r;
  bool ok=gaussMultipleSolver(L, sf, nbDofs_, eps, r);
  if(!ok)
    {
      where("NedelecEdgeFirstHexahedronPk::computeShapeFunctions()");
      error("mat_noinvert");
    }
  sf.roundToZero();
  Matrix<real_t>::iterator itm=sf.begin();
  Vector<real_t> v(nbDofs_);
  Vector<real_t>::iterator itv;
  Wk.dimVar=3;
  Wk.dimVec=3;
  Wk.name="NCE1_"+tostring(k);
  for(number_t i=0; i<nbDofs_; i++)
    {
      for(itv=v.begin(); itv!=v.end(); ++itv, ++itm) *itv=*itm;
      Wk.push_back(combine(Vk,v));
    }
  dWk.resize(3);
  dWk[0]=dx(Wk);
  dWk[1]=dy(Wk);
  dWk[2]=dz(Wk);
}

/* builds virtual coordinates of moment dofs of order k
     for edge dofs: coordinates of dofs of Pk-1[s] on segment s=[1/k+1, k/k+1] mapped with the same transformation as those used to compute L matrix
     for face dofs: coordinates of dofs of Pk-2[s] x Pk-1[s] and Pk-1[s] x Pk-2[s] mapped with same transformation as those used to compute L matrix
     for internal dofs: coordinates of dofs of Pk-1[s] x Pk-2[s] x Pk-2[s] and Pk-2[s] x Pk-1[s] x Pk-2[s] and Pk-2[s] x Pk-2[s] x Pk-1[s] on hexahedron,
*/
void NedelecEdgeFirstHexahedronPk::pointCoordinates()
{
  std::vector<RefDof*>::iterator it_rd=refDofs.begin();
  number_t k= interpolation_p->numtype, kp=k+1;
  std::vector<RefDof*>::iterator it1;

  //edge dofs
  LagrangeStdSegment segPk(findInterpolation(_Lagrange, _standard, k-1, H1));
  real_t s1=1./kp, s2=real_t(k)/kp;
  for(number_t e = 1; e <= 12; ++e)
    for(it1=segPk.refDofs.begin(); it1!=segPk.refDofs.end(); ++it1, ++it_rd)
      {
        real_t x = * (*it1)->coords();  x=(1-x)*s1+x*s2;
        switch(e)
          {

            case 1:(*it_rd)->coords(1,1,1-x)  ; break;  //on edge (2,6) : x -> (1,1,1-x)
            case 2: (*it_rd)->coords(1-x,1,1) ; break;  //on edge (7,6) : x -> (1-x,1,1)
            case 3: (*it_rd)->coords(1,1-x,1) ; break;  //on edge (5,6) : x -> (1,1-x,1)
            case 4: (*it_rd)->coords(0,0,x)   ; break;  //on edge (8,4) : x -> (0,0,x)
            case 5: (*it_rd)->coords(x,0,0)   ; break;  //on edge (1,4) : x -> (x,0,0)
            case 6: (*it_rd)->coords(0,x,0)   ; break;  //on edge (3,4) : x -> (0,x,0)
            case 7: (*it_rd)->coords(1,0,1-x) ; break;  //on edge (1,5) : x -> (1,0,1-x)
            case 8: (*it_rd)->coords(x,1,0)   ; break;  //on edge (2,3) : x -> (x,1,0)
            case 9: (*it_rd)->coords(0,x,1)   ; break;  //on edge (7,8) : x -> (0,x,1)
            case 10: (*it_rd)->coords(0,1,1-x) ; break;  //on edge (3,7) : x -> (0,1,1-x)
            case 11: (*it_rd)->coords(x,0,1)   ; break;  //on edge (5,8) : x -> (x,0,1)
            default: (*it_rd)->coords(1,1-x,0) ;         //on edge (1,2) : x -> (1,1-x,0)
          }
      }
  if(k<2) return;

  //face dofs
  std::vector<RefDof*>::iterator it2, it3;
  LagrangeStdSegment segPk2(findInterpolation(_Lagrange, _standard, k-2, H1));
  s1=1./kp; s2=real_t(k)/kp;
  number_t t=(k-1)*k;
  for(number_t f = 1; f <= 6; ++f)
    {
      for(it2=segPk2.refDofs.begin(); it2!=segPk2.refDofs.end(); ++it2)
        {
          std::vector<real_t>::const_iterator itp2=(*it2)->coords();
          real_t y=*itp2; y= (1-y)*s1+y*s2;
          for(it1=segPk.refDofs.begin(); it1!=segPk.refDofs.end(); ++it1, ++it_rd)
            {
              std::vector<real_t>::const_iterator itp1 = (*it1)->coords();
              real_t x=*itp1; x=(1-x)*s1+x*s2;
              switch(f)
                {
                  case 1: (*it_rd)->coords(1,1-y,x); (*(it_rd+t))->coords(1,1-x,y); //face (2,6,5,1), x1=1
                             break;
                  case 2: (*it_rd)->coords(x,1,1-y); (*(it_rd+t))->coords(y,1,1-x); //face (7,6,2,3), x2=1
                             break;
                  case 3: (*it_rd)->coords(1-y,x,1); (*(it_rd+t))->coords(1-x,y,1); //face (5,6,7,8), x3=1
                             break;
                  case 4: (*it_rd)->coords(0,1-x,y); (*(it_rd+t))->coords(0,1-y,x); //face (3,4,8,7), x1=0
                             break;
                  case 5: (*it_rd)->coords(y,0,1-x); (*(it_rd+t))->coords(x,0,1-y); //face (8,4,1,5), x2=0
                             break;
                  default: (*it_rd)->coords(1-x,y,0); (*(it_rd+t))->coords(1-y,x,0); //face (1,4,3,2), x3=0
                }
            }
        }
        it_rd=it_rd+t;
    }

  //internal dofs: Pk-1[s] x Pk-2[s] x Pk-2[s] and Pk-2[s] x Pk-1[s] x Pk-2[s] and Pk-2[s] x Pk-2[s] x Pk-1[s]
  number_t k1=(k-1)*(k-1)*k, k2=2*k1;
  for(it1=segPk.refDofs.begin(); it1!=segPk.refDofs.end(); ++it1)
    {
      std::vector<real_t>::const_iterator itp1=(*it1)->coords();
      real_t x=*itp1; x= (1-x)*s1+x*s2;
      for(it2=segPk2.refDofs.begin(); it2!=segPk2.refDofs.end(); ++it2)
        {
          std::vector<real_t>::const_iterator itp2=(*it2)->coords();
          real_t y=*itp2; y= (1-y)*s1+y*s2;
          for(it3=segPk2.refDofs.begin(); it3!=segPk2.refDofs.end(); ++it3, ++it_rd)
            {
              std::vector<real_t>::const_iterator itp3=(*it3)->coords();
              real_t z=*itp3; z= (1-z)*s1+z*s2;
              (*it_rd)->coords(x,y,z);
              (*(it_rd+k1))->coords(y,x,z);
              (*(it_rd+k2))->coords(y,z,x);
            }
        }
    }
}

//dof's edge numbering
void NedelecEdgeFirstHexahedronPk::sideOfSideNumbering()
{
  trace_p->push("NedelecEdgeFirstHexahedronPk::sideOfSideNumbering");
  number_t nbdss=nbDofsInSideOfSides_/12;
  number_t n=1;
  sideOfSideDofNumbers_.resize(12,std::vector<number_t>(nbdss,0));
  for(number_t s = 0; s < 12; ++s)
    for(number_t i=0; i<nbdss; ++i, ++n)    // nbdss = k dofs by edge
      sideOfSideDofNumbers_[s][i] = n;
  trace_p->pop();

}

//dof's face numbering including edge dofs
void NedelecEdgeFirstHexahedronPk::sideNumbering()
{
  trace_p->push("NedelecEdgeFirstHexahedronPk::sideNumbering");
  number_t nbds=nbDofsInSides_/6, nbdss=nbDofsInSideOfSides_/12;
  number_t n=nbDofsInSideOfSides_+1;
  sideDofNumbers_.resize(6,std::vector<number_t>(4*nbdss+nbds,0));
  for(number_t s = 0; s < 6; ++s)
    {
      number_t j=0;
      const std::vector<int_t>& ssn=geomRefElem_p->sideOfSideNumbers(s+1); //edge numbers on face s
      for(number_t k=0; k<4; k++) // include edge dofs
        {
          number_t e=std::abs(ssn[k]);         //edge number
          for(number_t d=0; d<nbdss; ++d,++j)
            sideDofNumbers_[s][j] = sideOfSideDofNumbers_[e-1][d];
        }
      for(number_t i=0; i<nbds; ++i, ++j, ++n) // include face dofs
        sideDofNumbers_[s][j] = n;
    }
  trace_p->pop();
}


/* tool for global dofs construction to ensure correct matching of dofs on side (see buildFeDofs in FeSpace class)
   return the new index m from a given one n on local side numbers i,j,k
   m corresponds to the index dof when edge vertices are in ascending order
       when i<j<k (no permutation of edge) index is unchanged (m=n)
   reverse 2 dof blocks
*/
number_t NedelecEdgeFirstHexahedronPk::sideDofsMap(const number_t& n, const number_t& i, const number_t& j, const number_t& k) const
{
  number_t d=interpolation_p->numtype;
  if(d==2 || (i==1 && j==2)) return n;   //one dof per face or no reordering: nothing to do
  //general case: use the LagrangeHexahedron::sideDofMap
  number_t m=(n+1)/2;
  LagrangeHexahedron* hexaPk = reinterpret_cast<LagrangeHexahedron*>(findRefElement(_hexahedron, findInterpolation(_Lagrange, _standard, d+1, H1)));
  if(hexaPk->barycentricSideDofs.size()==0) hexaPk->buildBarycentricSideDof();
  m = hexaPk->sideDofsMap(m,i,j,k); //map index in Lagrange
  return 2*m-(n%2);
}

/* tool for global dofs construction to ensure correct matching of dofs on side of side (see buildFeDofs in FeSpace class)
   return the new index m from a given one n on local edge numbers i,j
   m corresponds to the index dof when edge vertices are in ascending order
       if i<j (no permutation of edge) index is unchanged (m=n)
       else reverse n
*/
number_t NedelecEdgeFirstHexahedronPk::sideofsideDofsMap(const number_t& n, const number_t& i, const number_t& j) const
{
  number_t d=interpolation_p->numtype;
  if(d==1 || (i<j)) return n;   //one dof per edge or no reordering: nothing to do
  if(n<=2) return 3-n;          //"external" side dofs
  return d+3-n;                 //"internal" side dofs
}

//compute shape functions using polynomial representation
void NedelecEdgeFirstHexahedronPk::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool der1, const bool der2) const
{
  computeShapeValuesFromShapeFunctions(it_pt,shv,der1,der2);
}

/* rotation of shapevalues === ONLY FOR K=2 ====
   because face dofs depends on the choice of two tangent vectors on the face, to ensure the correct matching of dofs on a shared face
   the shape values corresponding of a dofs pair have to be rotated according to ascending vertex numbers on face:
   principle: reference dof is given on face (Vi,Vj,Vk,Vl) by ri(w)=int_F w(x,y).qi(x,y) in the axis system  Vi + x(Vj-Vi) + y(Vl-Vi)
               physical dof is chosen on ascending numbering face (Va,Vb,Vc,Vd) pi(w)=int_F w(x,y).qi(x,y) in the axis system  Va + x(Vb-Va) + y(Vd-Va)
               when Va=Vi and Vb=Vj shapevalues are unchanged, if not, some permutation, sign change or linear combination are to be done
               for instance, look at the case where Va=Vk and Vb=Vj (case 32)

                  Vl=Vd          Vk=Va
                   ---------<-------           pi(w)=int_F w(x,y).qi(x,y) = int_F (w(x,y) x n).(qi_1(x,y)t1+qi_2(x,y)t2)
                   |           t2  |
                   |               |t1        (x,y) -> (1-x,1-y)    p1(w)=int_F (w(x,y) x n).(0,-1)  = -r3(w)       => w1=-(v3+v4)
                   |               |                                p2(w)=int_F (w(x,y) x n).(0, y-1)=  r4(w)-r3(w) => w2=v4
                   |      32      \|/                               p3(w)=int_F (w(x,y) x n).(-1,0)  = -r1(w)       => w3=-(v1+v2)
                   |               |                                p4(w)=int_F (w(x,y) x n).(x-1,0) =  r2(w)-r1(w) => w4=v2
                   |               |
                   |               |                                pi(wj)=delta_ij   ri(vj)=delta_ij
                   -----------------
                 Vi=Vc           Vj=Vb

    let n1,n2,n3,n4  be the vertex global numbers of face f, i1 the rank of the min , i2 the rank of the min of neighboor vertex  (52,30,44,36)->(i1=2, i2=3)
    regarding i1,i2, we apply the following transformation (q1,q2) and shape values

      i1  i2 (i3)
      1   2   4     w1=v1     w2=v2  w3=v3     w4=v4  (no transformation)
      1   4   2     w1=v3     w2=v4  w3=v1     w4=v2
      2   1   3     w1=-v1    w2=-v2 w3=v3+v4  w4=-v4
      2   3   1     w1=v3+v4  w2=-v4 w3=-v1    w4=-v2
      3   2   4     w1=-v3-v4 w2=v4  w3=-v1-v2 w4=v2
      3   4   2     w1=-v1-v2 w2=v2  w3=-v3-v4 w4=v4
      4   1   3     w1=-v3    w2=-v4 w3=v1+v2  w4=-v2
      4   3   1     w1=v1+v2  w2=-v2 w3=-v3    w4=-v4

      for k>1, dual space Fk = Q_(k-2,k-1) x Q_(k-1,k-2), basis being ordered (qi,0)_i=1,k, (0,qi)_i=1,k e.g F2 = vect{(1,0),(y,0),(0,1),(0,x) }
      so permuting tangent induces permutation of dof i and dof i+k


   NOTE: if the vertex numbering of hexahedron and its faces is ascending, there is no tranformation

   ns: vertex numbering of hexahedron
   shv: shape values
   der1 : if true apply rotation also to derivatives (default value: false)
   der2 : if true apply rotation also to second derivatives (default value: false) NOT MANAGED

   # only for k=2 (to be done for k>2)

*/
void NedelecEdgeFirstHexahedronPk::rotateDofs(const std::vector<number_t>& ns, ShapeValues& shv, bool der1, bool der2) const
{
  if(nbDofsInSides_==0) return;   //no dofs in face
  number_t k = interpolation_p->numtype;
  if(k>2) error("not_handled_fe","NedelecEdgeFirstHexahedronPk::rotateDofs", " k>2");
  number_t nbds=nbDofsInSides_/6;  // shoud be even
  number_t inds, m, i1, i2, i3;
  std::vector<real_t> worg=shv.w;
  std::vector<std::vector<real_t> > dworg;
  if(der1) dworg=shv.dw;
  for(number_t f = 1; f <= 6; ++f)
  {
    number_t i1=1, mini=ns[geomRefElem_p->sideVertexNumber(1,f)-1];
    for(number_t i=2;i<=4;i++)
    {
      m=ns[geomRefElem_p->sideVertexNumber(i,f)-1];
      if(m<mini) {i1=i;mini=m;}
    }
    i2= (i1<4?i1+1:1); i3= (i1>1?i1-1:4);
    if(ns[geomRefElem_p->sideVertexNumber(i3,f)-1]<ns[geomRefElem_p->sideVertexNumber(i2,f)-1]) i2=i3;
    i3=10*i1+i2;
    inds=nbDofsInSideOfSides_+(f-1)*nbds;
    std::vector<real_t>::iterator itw, itw2;
    switch(i3)
    {
    case 14: //w1=v3, w2=v4, w3=v1, w4=v2
      {
        itw=shv.w.begin()+3*inds; itw2=worg.begin()+3*inds;
        for(number_t j=6; j<12; j++, itw++) *itw=*(itw2+j);
        for(number_t j=0; j<6; j++, itw++)  *itw=*(itw2+j);
        if(der1)
        {
          for(number_t d=0; d<3; d++)
          {
            itw=shv.dw[d].begin()+3*inds; itw2=dworg[d].begin()+3*inds;
            for(number_t j=6; j<12; j++, itw++) *itw=*(itw2+j);
            for(number_t j=0; j<6; j++, itw++)  *itw=*(itw2+j);
          }
        }
       }
       break;

     case 21: //w1=-v1, w2=-v2, w3=v3+v4, w4=-v4
       {
        itw=shv.w.begin()+3*inds; itw2=worg.begin()+3*inds;
        for(number_t j=0; j<6; j++, itw++)  *itw*=-1;
        for(number_t j=6; j<9; j++, itw++)  *itw= *(itw2+j)+*(itw2+3+j);
        for(number_t j=9; j<12; j++, itw++) *itw*=-1;
        if(der1)
        {
          for(number_t d=0; d<3; d++)
          {
           itw=shv.dw[d].begin()+3*inds; itw2=dworg[d].begin()+3*inds;
           for(number_t j=0; j<6; j++, itw++)  *itw*=-1;
           for(number_t j=6; j<9; j++, itw++)  *itw= *(itw2+j)+*(itw2+3+j);
           for(number_t j=9; j<12; j++, itw++) *itw*=-1;
          }
        }
       }
       break;

       case 23: // w1=v3+v4, w2=-v4, w3=-v1, w4=-v2
       {
        itw=shv.w.begin()+3*inds; itw2=worg.begin()+3*inds;
        for(number_t j=6; j<9; j++, itw++) *itw=*(itw2+j)+*(itw2+3+j);
        for(number_t j=9; j<12; j++, itw++)*itw=-*(itw2+j);;
        for(number_t j=0; j<6; j++, itw++) *itw=-*(itw2+j);
        if(der1)
        {
          for(number_t d=0; d<3; d++)
          {
           itw=shv.dw[d].begin()+3*inds; itw2=dworg[d].begin()+3*inds;
           for(number_t j=6; j<9; j++, itw++) *itw=*(itw2+j)+*(itw2+3+j);
           for(number_t j=9; j<12; j++, itw++)*itw=-*(itw2+j);;
           for(number_t j=0; j<6; j++, itw++) *itw=-*(itw2+j);
          }
        }
       }
       break;

       case 34: // w1=-(v1+v2), w2=v2, w3=-(v3+v4), w4=v4
       {
        itw=shv.w.begin()+3*inds; itw2=worg.begin()+3*inds;
        for(number_t j=0; j<3; j++, itw++) *itw=-*(itw2+j)-*(itw2+3+j);
        itw=itw+3;
        for(number_t j=6; j<9; j++, itw++) *itw=-*(itw2+j)-*(itw2+3+j);
        if(der1)
        {
          for(number_t d=0; d<3; d++)
          {
           itw=shv.dw[d].begin()+3*inds; itw2=dworg[d].begin()+3*inds;
           for(number_t j=0; j<3; j++, itw++) *itw= -*(itw2+j)-*(itw2+3+j);
           itw=itw+3;
           for(number_t j=6; j<9; j++, itw++) *itw=-*(itw2+j)-*(itw2+3+j);
          }
        }
       }
       break;

       case 32: // w1=-(v3+v4), w2=v4, w3=-(v1+v2), w4=v2
       {
        itw=shv.w.begin()+3*inds; itw2=worg.begin()+3*inds;
        for(number_t j=6; j<9; j++, itw++)  *itw= -*(itw2+j)-*(itw2+3+j);
        for(number_t j=9; j<12; j++, itw++) *itw=  *(itw2+j);
        for(number_t j=0; j<3; j++, itw++)  *itw= -*(itw2+j)-*(itw2+3+j);
        for(number_t j=3; j<6; j++, itw++)  *itw=  *(itw2+j);
        if(der1)
        {
          for(number_t d=0; d<3; d++)
          {
           itw=shv.dw[d].begin()+3*inds; itw2=dworg[d].begin()+3*inds;
           for(number_t j=6; j<9; j++, itw++)  *itw= -*(itw2+j)-*(itw2+3+j);
           for(number_t j=9; j<12; j++, itw++) *itw=  *(itw2+j);
           for(number_t j=0; j<3; j++, itw++)  *itw= -*(itw2+j)-*(itw2+3+j);
           for(number_t j=3; j<6; j++, itw++)  *itw=  *(itw2+j);
          }
        }
       }
       break;

       case 43: // w1=v1+v2, w2=-v2, w3=-v3, w4=-v4
       {
        itw=shv.w.begin()+3*inds; itw2=worg.begin()+3*inds;
        for(number_t j=0; j<3; j++, itw++) *itw+=*(itw2+3+j);
        for(number_t j=0; j<9; j++, itw++) *itw*=-1;
        if(der1)
        {
          for(number_t d=0; d<3; d++)
          {
           itw=shv.dw[d].begin()+3*inds; itw2=dworg[d].begin()+3*inds;
           for(number_t j=0; j<3; j++, itw++) *itw+=*(itw2+3+j);
           for(number_t j=0; j<9; j++, itw++) *itw*=-1;
          }
        }
       }
       break;

       case 41: // w1=-v3, w2=-v4, w3=v1+v2, w4=-v2
       {
        itw=shv.w.begin()+3*inds; itw2=worg.begin()+3*inds;
        for(number_t j=6; j<12; j++, itw++)*itw= -*(itw2+j);
        for(number_t j=0; j<3; j++, itw++) *itw= *(itw2+j)+*(itw2+3+j);
        for(number_t j=3; j<6; j++, itw++) *itw= -*(itw2+j);

        if(der1)
        {
          for(number_t d=0; d<3; d++)
          {
           itw=shv.dw[d].begin()+3*inds; itw2=dworg[d].begin()+3*inds;
           for(number_t j=6; j<12; j++, itw++)*itw= -*(itw2+j);
           for(number_t j=0; j<3; j++, itw++) *itw= *(itw2+j)+*(itw2+3+j);
           for(number_t j=3; j<6; j++, itw++) *itw= -*(itw2+j);
          }
        }
       }
       break;

       default: break;
    }
  }
}

//=====================================================================================
/*! Nedelec edge second family of order k on hexahedron (NE2k)
*/
//=====================================================================================
NedelecEdgeSecondHexahedronPk::NedelecEdgeSecondHexahedronPk(const Interpolation* interp_p)
  : NedelecEdgeHexahedron(interp_p)
{
  name_ += "_second family_"+tostring(interp_p->numtype);
  error("not_yet_implemented","NedelecEdgeSecondHexahedronPk::NedelecEdgeSecondHexahedronPk()");
//   interpolationData();  // build element interpolation data
//   sideNumbering();      // local numbering of points on edges
//   pointCoordinates();   // "virtual coordinates" of moment dofs
}

NedelecEdgeSecondHexahedronPk::~NedelecEdgeSecondHexahedronPk() {}

} // end of namespace xlifepp
