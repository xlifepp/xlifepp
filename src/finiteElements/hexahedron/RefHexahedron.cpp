/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file RefHexahedron.cpp
	\authors D. Martin, N. Kielbasiewicz
	\since 11 jan 2003
	\since 6 aug 2012

  \brief Implementation of xlifepp::RefHexahedron class members and related functions
 */

#include "RefHexahedron.hpp"
#include "LagrangeHexahedron.hpp"
#include "NedelecEdgeHexahedron.hpp"
#include "../Interpolation.hpp"
#include "utils.h"

namespace xlifepp
{

//! selectRefHexahedron construction of a Reference Element by interpolation type and interpolation subtype
RefElement* selectRefHexahedron(const Interpolation* int_p)
{
  switch ( int_p->type )
  {
    case _Lagrange:
      switch ( int_p->subtype )
      {
        case _standard: return new LagrangeStdHexahedron(int_p); break;
        case _GaussLobattoPoints: return new LagrangeGLHexahedron(int_p); break;
        default: int_p->badSubType(_hexahedron); break;
      }
        case _NedelecEdge:
            switch ( int_p->subtype )
      {
          case _firstFamily: return new NedelecEdgeFirstHexahedronPk(int_p); break;
          default: int_p->badSubType(_hexahedron); break;
      }
    default: break;
  }

  // Throw error messages
  trace_p->push("selectRefHexahedron");
  int_p->badType(_hexahedron);
  trace_p->pop();
  return nullptr;
}

} // end of namespace xlifepp
