/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LagrangeHexahedron.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 6 aug 2012

  \brief Definition of the xlifepp::LagrangeHexahedron class

  xlifepp::LagrangeHexahedron is child to xlifepp::RefHexahedron and defines Lagrange
  Reference Element interpolation data

  member functions:
  - interpolationData interpolation data
  - sideNumbering     local numbers on faces
  - pointCoordinates  coordinates of points (support of D.o.F)
  - vertexCoordinates coordinates of vertices
  - computeShapeValues    computation of shape functions
  - fig4TeX TeX format output of local numbers & coordinates

  external class related functions:
  - tensorNumberingHexahedron: builds correspondence between segment and hexahedron local numbering
  - tensorHexahedronFaceNumbering: correspondence between segment and quadrangle local numbering on faces
*/

#ifndef LAGRANGE_HEXAHEDRON_HPP
#define LAGRANGE_HEXAHEDRON_HPP

#include "config.h"
#include "RefHexahedron.hpp"

namespace xlifepp {

/*!
  \class LagrangeHexahedron
  defines Lagrange Reference Element interpolation data on hexahedra
 */
class LagrangeHexahedron : public RefHexahedron {
  public:
    LagrangeHexahedron(const Interpolation* int_p); //!< constructor by interpolation
    virtual ~LagrangeHexahedron(); //!< destructor

  protected:
    void interpolationData(); //!< builds interpolation data on reference element
    void pointCoordinates(const RefElement* rfSeg=0); //!< builds point coordinates on reference element
    std::vector<RefDof*>::iterator vertexCoordinates(); //!< returns vector of vertices dofs


    number_t sideofsideDofsMap(const number_t& n, const number_t& i, const number_t& j=0) const //!internal side dofs mapping when vertices of side are permuted
    {if(i>j) return (nbDofsInSideOfSides()/12-n+1); return n;}

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv = true, const bool with2Deriv=false) const; //!< Lagrange Qk shape functions
    void outputAsP1(std::ofstream& os, const int , const std::vector<number_t>& ranks) const; //!< output of Lagrange D.o.f's numbers on underlying P1-tetrahedron D.o.f's
    void sideNumbering(); //!< builds local numbers of D.o.F's on sides of reference element
    void sideOfSideNumbering(); //!< builds local numbers of D.o.F's on side of sides of reference element
    std::vector<std::vector<number_t> > splitP1() const;  //!< return nodes numbers of first order tetrahedron elements when splitting current element
    splitvec_t splitO1() const;  //!< return nodes numbers of first order hexahedron elements when splitting current element

  public:
    //related to global dof construction
    std::map<Triplet<number_t>,number_t> barycentricSideDofMap;           //!< internal dof on side (face) ordered by barycentric coordinates
    std::vector<Triplet<number_t> > barycentricSideDofs;                  //!< dof barycentric coordinates
    void buildBarycentricSideDof();                                       //!< build barycentricSideDofMap
    number_t sideDofsMap(const number_t& n, const number_t& i, const number_t& j, const number_t& k=0) const; //!<internal side dofs mapping when vertices of side are permuted

  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }
    //#ifdef FIG4TEX_OUTPUT
    //    void fig4TeX();
    //#endif  /* FIG4TEX_OUTPUT */
    // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //

}; // end of class LagrangeHexahedron

/*!
  \class LagrangeStdHexahedron
  child class of LagrangeHexahedron with standard interpolation
 */
class LagrangeStdHexahedron : public LagrangeHexahedron {
  public:
    LagrangeStdHexahedron(const Interpolation* int_p); //!< constructor by interpolation
    ~LagrangeStdHexahedron() {}
    const splitvec_t& getO1splitting() const // override
     { return splitO1Scheme; }
  private:
    // storage of order 1 splitting scheme (intended to graphical softwares usage)
    splitvec_t splitO1Scheme;

}; // end class LagrangeStdHexahedron

/*!
  \class LagrangeGLHexahedron
  child class of LagrangeHexahedron with Gauss Lobatto interpolation
 */
class LagrangeGLHexahedron : public LagrangeHexahedron {
  public:
    LagrangeGLHexahedron(const Interpolation* int_p); //!< constructor by interpolation
    ~LagrangeGLHexahedron() {}
    const splitvec_t& getO1splitting() const // override
     { return splitO1Scheme; }
  private:
    // storage of order 1 splitting scheme (intended to graphical softwares usage)
    splitvec_t splitO1Scheme;

}; // end class LagrangeGLHexahedron

//================================================================================
// Extern class related functions
//================================================================================
void tensorNumberingHexahedron(const int, number_t**& s2h); //!< correspondence between segment and hexahedron local numbering such that
/*!
 - point number p has coordinates defined by
 - ( x = coords[s2h[0][p]], y = coords[s2h[1][p]], z = coords[s2h[3][p]] )
 - where coords are coordinates of the associated 1D Lagrange reference element
 - and
 - shape functions are defined by tensor product of shape functions of the 1D reference element
 - w3_p(x,y,z) = w1_{s2h[0][p]}(x) * w1_{s2h[1][p]}(y) * w1_{s2h[2][p]}(z)
 - where w1_k is the k-th shape function of the associated 1D Lagrange reference element
*/
void tensorHexahedronSideNumbering(number_t*&, number_t*&, number_t&, const int, const number_t, const number_t, const number_t, const number_t, const short int); //!< correspondence between segment and hexahedron face local numbering

} // end of namespace xlifepp

#endif /* LAGRANGE_HEXAHEDRON_HPP */
