/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomRefHexahedron.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 7 aug 2012

  \brief Definition of the xlifepp::GeomRefHexahedron class

  xlifepp::GeomRefHexahedron defines Reference Element geometric data for hexahedral elements

  Numbering of vertices, edges and faces

  \verbatim
                           Vertex numbers on edges | Vertex numbers on faces
                           Edge:  v1 v2  equation  | Face: v1  v2  v3  v4  equation
          8------IX-----7     1:  2  6  x1=1,x2=1  |    1:  2   6   5   1   x=1
         /|            /|     2:  7  6  x2=1,x3=1  |    2:  7   6   2   3   y=1
       XI |          II |     3:  5  6  x3=1,x1=1  |    3:  5   6   7   8   z=1
      /   IV        /   X     4:  8  4  x1=0,x2=0  |    4:  3   4   8   7   x=0
     5------III----6    |     5:  1  4  x2=0,x3=0  |    5:  8   4   1   5   y=0
     |    |        |    |     6:  3  4  x3=0,x1=0  |    6:  1   4   3   2   z=0
     |    4-----VI-|----3     7:  1  5  x1=1,x2=0  | Egde numbers on faces (+)
    VII  /        I|   /      8:  2  3  x2=1,x3=0  | Face:  e1  e2  e3  e4
     |  V          | VIII     9:  7  8  x3=1,x1=0  |    1:  +1  -3  -7 +12
     |/            |/        10:  3  7  x1=0,x2=1  |    2:  +2  -1  +8 +10
     1-----XII-----2         11:  5  8  x2=0,x3=1  |    3:  +3  -2  +9 -11
                             12:  1  2  x3=0,x1=1  |    4:  +6  -4  -9 -10
                                                   |    5:  +4  -5  +7 +11
                                                   |    6:  +5  -6  -8 -12
  \endverbatim
  - (+) an edge on a face carries a + sign to show that the vertices on this edge
        are seen in the local face numbering in the same order as the vertices of
        the same edge in the element numbering;
        for instance edge number 1 of face 2 (edge number 2 in the element numbering)
        goes from vertex 7 to vertex 6 in the local numbering of face 1;
        the same edge number 2 goes from vertex 7 to vertex 6 in the element numbering.
        This + direction also corresponds to the increasing order of D.o.F's suported
        by edge points with are not vertices.
  - (-) minus sign indicates otherwise
        For instance edge number 2 of face 3 (edge number 2 in the element numbering)
        goes from vertex 6 to vertex 7 in the local numbering of face 3
        while the same edge number 2 goes from vertex 7 to vertex 6 in the element.

  sideOfSideNumbering local numbering of vertices on side of sides
  sideNumbering local numbering of vertices on sides
*/

#ifndef GEOM_REF_HEXAHEDRON_HPP
#define GEOM_REF_HEXAHEDRON_HPP

#include "config.h"
#include "../GeomRefElement.hpp"
//#include "../QuadratureRule.hpp"

namespace xlifepp
{

/*!
	\class GeomRefHexahedron
	child to class GeomRefElement
 */
class GeomRefHexahedron : public GeomRefElement
{
  public:
    //! default constructor
    GeomRefHexahedron();
    //! destructor
    ~GeomRefHexahedron();

    real_t measure(const dimen_t dim, const number_t sideNum = 0) const; //!< returns edge length or face area
    // /*!
    //  returns a quadrature rule built on edge sideNum (=1,2,...12) from a 1d quadrature formula
    //  or a quadrature rule built on face sideNum (=1,2,...6) from a quadrangle quadrature formula
    // */
    //    void sideQuadrature(const QuadratureRule&, QuadratureRule&, const number_t sideNum, const dimen_t = 2) const;
    //! returns tangent vector on an edge or a face
    void tangentVector(const std::vector<real_t>& jacobianMatrix, std::vector< std::vector<real_t> >& tgv, const number_t, const dimen_t) const;
    //! returns vertex opposite to edge or face number sideNum (=1,2, ...)
    number_t vertexOppositeSide(const number_t s) const { if(s<5) return 1+(s+1)%4; else return 1+s;}
    //! returns edge or face opposite to vertex number v (=1,2,...)
    number_t sideOppositeVertex(const number_t) const { noSuchFunction("sideOppositeVertex");  return 0;}
    //! returns edge opposite to edge number sideNum (=1,2,...)
    number_t edgeOppositeEdge(const number_t) const { noSuchFunction("edgeOppositeEdge");  return 0;}
    //! returns local number of edge bearing vertices with local numbers v1 and v2
    number_t sideWithVertices(const number_t, const number_t) const ;
    //! returns local number of face bearing vertices with local numbers v1, v2 and v3
    number_t sideWithVertices(const number_t, const number_t, const number_t, const number_t = 0) const;
    //! node numbers defining first simplex of ref element
    std::vector<number_t> simplexNodes() const
    {std::vector<number_t> sn(4,0); sn[0]=4;sn[1]=1;sn[2]=3;sn[3]=8; return sn;}

    bool contains(std::vector<real_t>& p, real_t tol= theTolerance) const;         //!< test if a point belongs to current element
    std::vector<real_t> projection(const std::vector<real_t>& p, real_t& h) const; //!< return projection on ref tetahedron



  private:
    void sideOfSideNumbering(); //!< local numbers of vertices on edges for hexahedra
    void sideNumbering(); //!< local numbers of vertices on faces and of edges on faces for hexahedra

}; // end of class GeomRefHexahedron

} // end of namespace xlifepp

#endif /* GEOM_REF_HEXAHEDRON_HPP */
