/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RefHexahedron.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 6 aug 2012

  \brief Definition of the xlifepp::RefHexahedron class

  xlifepp::RefHexahedron defines Reference Element interpolation data on hexahedral elements

  External class related functions
    - hexahedronLagrangeStd: construction of a Lagrange standard hexahedric Reference Element
    - hexahedronLagrangeGaussLobatto: construction of a Lagrange Gauss-Lobatto hexahedric Reference Element
*/

#ifndef REF_HEXAHEDRON_HPP
#define REF_HEXAHEDRON_HPP

#include "config.h"
#include "../RefElement.hpp"

namespace xlifepp
{

//class Interpolation;

/*!
	\class RefHexahedron

	Parent class: RefElement
	Child class: LagrangeHexahedron
 */
class RefHexahedron : public RefElement
{
  public:
    //! constructor by interpolation
    RefHexahedron(const Interpolation* int_p) : RefElement(_hexahedron, int_p) {}
    //! virtual destructor
    virtual ~RefHexahedron() {}
    virtual void interpolationData() = 0; //!< returns interpolation data

    virtual void outputAsP1(std::ofstream& os, const int, const std::vector<number_t>&) const { noSuchFunction("outputAsP1"); }
    virtual void sideNumbering() = 0; //!< returns side numbering
    const splitvec_t& getO1splitting() const // override
       { return RefElement::getO1splitting(); }

}; // end of class RefHexahedron

} // end of namespace xlifepp

#endif /* REF_HEXAHEDRON_HPP */
