/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file RefPrism.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 16 dec 2002
  \date 19 sep 2012

  \brief Implementation of xlifepp::RefPrism members and related functions
*/

#include "RefPrism.hpp"
#include "LagrangePrism.hpp"
#include "utils.h"

namespace xlifepp
{

//! selectRefPrism construction of a prismatic Reference Element by interpolation subtype and number
RefElement* selectRefPrism(const Interpolation* interp_p)
{
  switch (interp_p->type)
  {
    case _Lagrange:
      switch ( interp_p->subtype)
      {
        case _standard: return prismLagrangeStd(interp_p); break;
        default: interp_p->badSubType(_prism); break;
      }
    default: break;
  }
  
  // Throw error messages
  trace_p->push("selectRefPrism");
  interp_p->badType(_prism);
  trace_p->pop();
  return nullptr;
}

//! prismLagrangeStd construction of a prismatic Lagrange Reference Element by interpolation number
RefElement* prismLagrangeStd(const Interpolation* interp_p)
{
  switch ( interp_p->numtype )
  {
    case _P0: return new LagrangeStdPrism<_P0>(interp_p);
      break;
    case _P1: return new LagrangeStdPrism<_P1>(interp_p);
      break;
    case _P2: return new LagrangeStdPrism<_P2>(interp_p);
      break;
    default:
      trace_p->push("prismLagrangeStd");
      interp_p->badDegree(_prism);
      trace_p->pop();
      break;
  }
  return nullptr;
}

} // end of namespace xlifepp
