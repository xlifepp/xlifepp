/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomRefPrism.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 21 dec 2002
  \date 23 sep 2012

  \brief Definition of the xlifepp::GeomRefPrism class

  xlifepp::GeomRefPrism defines Reference element geometric data on prism elements

  Numbering of vertices, edges and faces

  \verbatim
                           Vertex numbers on edges | Vertex numbers on faces
                           Edge:   v1 v2  equation | Face:  v1  v2  v3  v4  eq.
          6-------V-----5     1:   1  2            |    1:  1   2   3       x3=0
         /|             |     2:   2  3            |    2:  1   2   5   4   x1+x2=1
       VI |    IV       |     3:   3  1            |    3:  2   3   6   5   x1=0
      /   IX          VIII    4:   4  5            |    4:  1   3   6   4   x2=0
     4    |             |     5:   5  6            |    5:  4   5   6       x3=1
     |    |             |     6:   6  4            | Edge numbers on faces (+)
     |    3-----II------2     7:   1  4            | Face:  e1  e2  e3  e4
    VII  /                    8:   2  5            |    1:   1  -3  -2
     | III     I              9:   3  6            |    2:   1   7   4  -8
     |/                                            |    3:  -9  -5  -8  +2
     1                                             |    4:  -3  -7  +6  +9
                                                   |    5:   4  -5  -6

  (+) only required for interpolation involving more than 3 nodes on a edge
  \endverbatim

  sideOfSideNumbering local numbering of vertices on edges
  SideNumbering local numbering of vertices on faces and of edges on faces
*/

#ifndef GEOM_REF_PRISM_HPP
#define GEOM_REF_PRISM_HPP

#include "config.h"
#include "../GeomRefElement.hpp"

namespace xlifepp
{

/*!
  \class GeomRefPrism
  child class to GeomRefElement
 */
class GeomRefPrism : public GeomRefElement
{
  public:
    //! default constructor
    GeomRefPrism();
    //! destructor
    ~GeomRefPrism() {}

    real_t measure(const dimen_t dim, const number_t sideNum = 0) const; //!< returns edge length or face area

    //! returns tangent vector on an edge or a face
    void tangentVector(const std::vector<real_t>& jacobianMatrix, std::vector<std::vector<real_t> >& tgv, const number_t, const dimen_t) const;

    number_t vertexOppositeSide(const number_t s) const //! returns vertex opposite to edge or face number sideNum (=1,2, ...)
    { if(s<3) return s+4; else return s-2;}

    number_t sideOppositeVertex(const number_t) const //! returns edge or face opposite to vertex number v
    { noSuchFunction("sideOppositeVertex");  return 0;}

    number_t edgeOppositeEdge(const number_t) const //! returns edge opposite to edge number e
    { noSuchFunction("edgeOppositeEdge");  return 0;}

    number_t sideWithVertices(const number_t, const number_t) const; //!< returns local number of edge bearing vertices with local numbers v1 and v2
    number_t sideWithVertices(const number_t, const number_t, const number_t, const number_t = 0) const; //!< returns local number of face bearing vertices with local numbers v1, v2, v3

    bool contains(std::vector<real_t>& p, real_t tol= theTolerance) const;  //!< test if a point belongs to current element

    //! node numbers defining first simplex of ref element
    std::vector<number_t> simplexNodes() const
    {std::vector<number_t> sn(4,0); sn[0]=3;sn[1]=1;sn[2]=2;sn[3]=6; return sn;}

  private:

    void sideOfSideNumbering(); //!< local numbers of vertices on edges for prisms
    void sideNumbering(); //!< local numbers of vertices and edges on faces for prisms

};

} // end of namespace xlifepp

#endif /* GEOM_REF_PRISM_HPP */
