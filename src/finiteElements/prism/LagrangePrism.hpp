/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LagrangePrism.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 19 sep 2012

  \brief Definition of the xlifepp::LagrangePrism class

  xlifepp::LagrangePrism is child to RefPrism and defines Lagrange Reference Element interpolation data
 */

#ifndef LAGRANGE_PRISM_HPP
#define LAGRANGE_PRISM_HPP

#include "config.h"
#include "RefPrism.hpp"

namespace xlifepp {

//class Interpolation;

/*!
  \class LagrangePrism
  defines Lagrange Reference Element interpolation data on prisms
 */
class LagrangePrism : public RefPrism
{
  public:
    LagrangePrism(const Interpolation* interp_p); //!< constructor by interpolation
    virtual ~LagrangePrism(); //!< destructor
  protected:
    void sideNumbering(); //!< builds local numbers of D.o.F's on faces of reference element
    void sideOfSideNumbering(); //!< builds local numbers of D.o.F's on edges of reference element
    void interpolationData(); //!< builds interpolation data on reference element
    void pointCoordinates(); //!< builds point coordinates on reference element
    void outputAsP1(std::ofstream& os, const int , const std::vector<number_t>& ranks) const; //!< output of Lagrange D.o.f's numbers on underlyin P1 D.o.f's
    std::vector<RefDof*>::iterator vertexCoordinates(); //!< returns vector of vertices dofs
    std::vector<std::vector<number_t> > splitP1() const;  //!< return nodes numbers of first order tetrahedron elements when splitting current element
    splitvec_t splitO1() const;  //!< return nodes numbers of first order prism elements when splitting current element
  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }

}; // end of class LagrangePrism

/*!
  \class LagrangeStdPrism
  template class child to class LagrangePrism

  http://homepage.mac.com/danielmartin/melina++/doc/elements/prism/LagrangeStdPrism1.pdf
  http://homepage.mac.com/danielmartin/melina++/doc/elements/prism/LagrangeStdPrism2.pdf
 */
template<number_t Pk>
class LagrangeStdPrism : public LagrangePrism
{
  public:
    LagrangeStdPrism(const Interpolation* interp_p); //!< constructor by interpolation
    ~LagrangeStdPrism() {} //!< destructor

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv = true, const bool with2Deriv=false) const; //!< shape functions
    void computeShapeFunctions();            // compute shape functions as polynomials
    const splitvec_t& getO1splitting() const // override
     { return splitO1Scheme; }
  private:
    // storage of order 1 splitting scheme (intended to graphical softwares usage)
    splitvec_t splitO1Scheme;

}; // end of class LagrangeStdPrism

//! compute shape functions as polynomials using general algorithm
template<number_t Pk>
void LagrangeStdPrism<Pk>::computeShapeFunctions()
{
  number_t k = interpolation_p->numtype;

  //compute Matrix Lij=l(pi,qj) for all pi in Vk, for all qj in P_(k-1)[e], edge D.o.Fs:  v-> int_e v.t q, q in P_(k-1)[e]
  PolynomialBasis P(_Pk,2,k);
  PolynomialBasis Q(_Qk,1,k); Q.swapVar(3,2,1);
  PolynomialBasis Vk=P*Q;
  Matrix<real_t> L(nbDofs_,nbDofs_);
  number_t j=1;
  PolynomialBasis::iterator itp;
  std::vector<RefDof*>::iterator itrd;
  for (itp=Vk.begin(); itp!=Vk.end(); ++itp, ++j)
  {
         number_t i=1;
         for (itrd=refDofs.begin(); itrd!=refDofs.end(); ++itrd, ++i)
            L(i,j)=(*itp)((*itrd)->point());
  }

  //compute coefficients of shape functions by solving linear systems
  Matrix<real_t> sf(nbDofs_,_idMatrix);
  number_t rank;
  real_t eps=theZeroThreshold;
  bool ok=gaussMultipleSolver(L, sf, nbDofs_, eps, rank);
  if(!ok)
  {
    where("LagrangeStdPrism<Pk>::computeShapeFunctions()");
    error("mat_noinvert");
  }
  sf.roundToZero();
  Matrix<real_t>::iterator itm=sf.begin();
  Vector<real_t> v(nbDofs_);
  Vector<real_t>::iterator itv;
  Wk.dimVar=3;
  Wk.dimVec=1;
  Wk.name="Pr"+tostring(k);
  for(number_t i=0; i<nbDofs_; i++)
  {
    for(itv=v.begin(); itv!=v.end(); ++itv, ++itm) *itv=*itm;
    Wk.add(combine(Vk,v));
  }
  dWk.resize(3);
  dWk[0]=dx(Wk);
  dWk[1]=dy(Wk);
  dWk[2]=dz(Wk);
}

} // end of namespace xlifepp

#endif /* LAGRANGE_PRISM_HPP */
