/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LagrangePrism.cpp
  \author D. Martin, N. Kielbasiewicz
  \since 17 dec 2002
  \date 23 sep 2012

  \brief Implementation of xlifepp::LagrangePrism class members and related functions
*/

#include "LagrangePrism.hpp"
#include "../Interpolation.hpp"
#include "utils.h"

#include <iostream>

namespace xlifepp
{

//! LagrangePrism constructor for Lagrange reference elements
LagrangePrism::LagrangePrism(const Interpolation* interp_p)
  : RefPrism(interp_p)
{
  name_ += "_Lagrange";
  trace_p->push("LagrangePrism::LagrangePrism (" + name_ + ")");

  // build element interpolation data
  interpolationData();
  // define local numbering of points on edges
  sideOfSideNumbering();
  // find or create Reference Element for element edges
  sideOfSideRefElement();
  // define local numbering of points on faces
  sideNumbering();
  // find or create Reference Element for element faces
  sideRefElement();

  trace_p->pop();
}

LagrangePrism::~LagrangePrism() {}

//! interpolationData defines Reference Element interpolation data
void LagrangePrism::interpolationData()
{
  trace_p->push("LagrangePrism::interpolationData");

  number_t interpNum = interpolation_p->numtype;

  switch(interpNum)
    {
      case _P0:
        nbDofs_ = nbInternalDofs_ = 1;
        break;
      case _P1:
        nbDofs_ = nbDofsOnVertices_ = 6;
        break;
      default: // Lagrange prism of degree > 1
        nbDofs_ = (interpNum + 1) * ((interpNum + 1) * (interpNum + 2)) / 2;
        nbInternalDofs_ = (interpNum - 1) * ((interpNum - 1) * (interpNum - 2)) / 2;
        nbDofsOnVertices_ = 6;
        nbDofsInSideOfSides_ = 9 * (interpNum - 1);
        nbDofsInSides_ = nbDofs_ - nbInternalDofs_ - nbDofsOnVertices_ - nbDofsInSideOfSides_;
        break;
    }

  // Creating Degrees Of Freedom of reference element
  // in Lagrange standard elements all D.o.F on faces are sharable
  // number of internal nodes (or D.o.F)

  refDofs.reserve(nbDofs_);
  std::vector<number_t> nbDofsPerSide(5,0);
  if(interpNum >= 2)
    {
      nbDofsPerSide[0]=((interpNum - 2) * (interpNum - 1)) / 2;
      nbDofsPerSide[1]=(interpNum - 1) * (interpNum - 1);
      nbDofsPerSide[2]=(interpNum - 1) * (interpNum - 1);
      nbDofsPerSide[3]=(interpNum - 1) * (interpNum - 1);
      nbDofsPerSide[4]=((interpNum - 2) * (interpNum - 1)) / 2;
    }

  //create refdofs, only for order 0, 1 and 2 !
  //lagrangeRefDofs(3, nbDofsOnVertices_, nbInternalDofs_, 9, nbDofsInSideOfSides_, nbDofsPerSide);
  number_t ne=nbDofsInSideOfSides_/9;
  number_t k=1;  //global dof number
  for(number_t n = 1; n <= nbDofsOnVertices_; n++, k++)
    refDofs.push_back(new RefDof(this, true, _onVertex, n, 1, 3, 0, k, 1, 0, _noProjection,_id,"nodal value"));
  if(interpNum==2)
    {
      for(number_t n = 1; n <= ne; n++)   //dof on edge excluding vertices
        for(number_t e = 1; e <= 9; e++, k++)
          refDofs.push_back(new RefDof(this, true, _onEdge, e, n, 3, 0, k, 1, 0, _noProjection,_id, "nodal value"));
      refDofs.push_back(new RefDof(this, true, _onFace, 2, 1, 3, 0, k++, 1, 0, _noProjection,_id,"nodal value"));
      refDofs.push_back(new RefDof(this, true, _onFace, 3, 1, 3, 0, k++, 1, 0, _noProjection,_id,"nodal value"));
      refDofs.push_back(new RefDof(this, true, _onFace, 4, 1, 3, 0, k, 1, 0, _noProjection,_id,"nodal value"));
    }

  // compute number of shape functions
  nbshfcts_ = shapeValueSize();

  // Lagrange prism serendipity element of degree 2
  //   {
  //      nbDofs_=15;
  //   }
  nbPts_ = nbDofs_;

  trace_p->pop();
}

//! output of Lagrange D.o.f's numbers on underlying tetrahedron _P1 D.o.f's
void LagrangePrism::outputAsP1(std::ofstream& os, const int refNum, const std::vector<number_t>& rks) const
{
  number_t interpNum = interpolation_p->numtype;

  switch(interpNum)
    {
      case _P0:
      case _P1:
        simplexVertexOutput(os, refNum, rks[0], rks[1], rks[2], rks[4]);
        simplexVertexOutput(os, refNum, rks[2], rks[3], rks[4], rks[5]);
        simplexVertexOutput(os, refNum, rks[0], rks[2], rks[3], rks[4]);
        break;
      case _P2:
        // 8 _P1 prisms split each into 3 _P1 tetrahedra
        // 3 tetrahedra
        simplexVertexOutput(os, refNum, rks[ 0], rks[ 6], rks[ 8], rks[15]);
        simplexVertexOutput(os, refNum, rks[ 8], rks[12], rks[15], rks[17]);
        simplexVertexOutput(os, refNum, rks[ 0], rks[ 8], rks[12], rks[15]);
        // 3 tetrahedra
        simplexVertexOutput(os, refNum, rks[ 6], rks[ 1], rks[ 7], rks[13]);
        simplexVertexOutput(os, refNum, rks[ 7], rks[15], rks[14], rks[16]);
        simplexVertexOutput(os, refNum, rks[ 6], rks[ 7], rks[15], rks[13]);
        // 3 tetrahedra
        simplexVertexOutput(os, refNum, rks[12], rks[15], rks[17], rks[ 9]);
        simplexVertexOutput(os, refNum, rks[17], rks[ 3], rks[ 9], rks[11]);
        simplexVertexOutput(os, refNum, rks[12], rks[17], rks[ 3], rks[ 9]);
        // 3 tetrahedra
        simplexVertexOutput(os, refNum, rks[15], rks[13], rks[16], rks[ 4]);
        simplexVertexOutput(os, refNum, rks[16], rks[ 9], rks[ 9], rks[11]);
        simplexVertexOutput(os, refNum, rks[12], rks[17], rks[ 4], rks[10]);
        // 3 tetrahedra
        simplexVertexOutput(os, refNum, rks[ 8], rks[ 7], rks[ 2], rks[16]);
        simplexVertexOutput(os, refNum, rks[ 2], rks[17], rks[16], rks[14]);
        simplexVertexOutput(os, refNum, rks[ 8], rks[ 2], rks[17], rks[16]);
        // 3 tetrahedra
        simplexVertexOutput(os, refNum, rks[17], rks[16], rks[14], rks[10]);
        simplexVertexOutput(os, refNum, rks[14], rks[11], rks[10], rks[ 5]);
        simplexVertexOutput(os, refNum, rks[17], rks[14], rks[11], rks[10]);
        // 3 tetrahedra
        simplexVertexOutput(os, refNum, rks[ 7], rks[ 8], rks[ 6], rks[17]);
        simplexVertexOutput(os, refNum, rks[ 6], rks[16], rks[17], rks[15]);
        simplexVertexOutput(os, refNum, rks[ 7], rks[ 6], rks[16], rks[17]);
        // 3 tetrahedra
        simplexVertexOutput(os, refNum, rks[16], rks[17], rks[15], rks[11]);
        simplexVertexOutput(os, refNum, rks[15], rks[10], rks[12], rks[ 9]);
        simplexVertexOutput(os, refNum, rks[16], rks[15], rks[10], rks[11]);
        break;
      default: noSuchFunction("outputAsP1"); break;
    }
}

//! pointCoordinates defines Lagrange Reference Element point coordinates
std::vector<RefDof*>::iterator LagrangePrism::vertexCoordinates()
{
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();
  (*it_rd++)->coords(1., 0., 0.);
  (*it_rd++)->coords(0., 1., 0.);
  (*it_rd++)->coords(0., 0., 0.);
  (*it_rd++)->coords(1., 0., 1.);
  (*it_rd++)->coords(0., 1., 1.);
  (*it_rd++)->coords(0., 0., 1.);
  return it_rd;
}

void LagrangePrism::pointCoordinates()
{
  trace_p->push("LagrangePrism::pointCoordinates");

  // Local numbering of Lagrange Pk(1D) x Pk(2D) prism
  number_t interpNum = interpolation_p->numtype;
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();

  switch(interpNum)
    {
      case _P0:
        (*it_rd)->coords(over3_, over3_, .5);
        break;
      case _P1:
        it_rd = vertexCoordinates();
        break;
      case _P2:
        it_rd = vertexCoordinates();
        (*it_rd++)->coords(.5, .5, 0.); // 7
        (*it_rd++)->coords(0., .5, 0.); // 8
        (*it_rd++)->coords(.5, 0., 0.); // 9
        (*it_rd++)->coords(.5, .5, 1.); //10
        (*it_rd++)->coords(0., .5, 1.); //11
        (*it_rd++)->coords(.5, 0., 1.); //12
        (*it_rd++)->coords(1., 0., .5); //13
        (*it_rd++)->coords(0., 1., .5); //14
        (*it_rd++)->coords(0., 0., .5); //15
        (*it_rd++)->coords(.5, .5, .5); //16
        (*it_rd++)->coords(0., .5, .5); //17
        (*it_rd)->coords(.5, 0., .5); //18
        break;
      default:
        break;
    }
  trace_p->pop();
}

//! sideNumbering defines Lagrange Reference Element numbering on faces
void LagrangePrism::sideNumbering()
{
  // Local numbering of Lagrange Pk(1D) x Qk (2D) prism
  trace_p->push("LagrangePrism::sideNumbering");

  number_t interpNum = interpolation_p->numtype;
  number_t nbSides = geomRefElem_p->nbSides();
  sideDofNumbers_.resize(nbSides);
  //number_t nb_faces = geom_ref_elt_p->numberOffaces();
  if(interpNum==0)  //P0 interpolation
  {
       for(number_t side = 0; side < nbSides; side++)
       {
           sideDofNumbers_[side].resize(1);
           sideDofNumbers_[side][0]=1;
       }
       return;
  }

  if(interpNum > 0)
    {
      // Lagrange prismatic element (equidistant points) of degree k = 1, 2
      // local numbering of vertices
      number_t sideNum, nbDofsPerSide;

      // face #0 : x3 = 0 (triangular)
      sideNum = 0;
      nbDofsPerSide = ((interpNum + 1) * (interpNum + 2)) / 2;
      sideDofNumbers_[sideNum].resize(nbDofsPerSide);
      sideDofNumbers_[sideNum][0] = 1;
      sideDofNumbers_[sideNum][1] = 3;
      sideDofNumbers_[sideNum][2] = 2;
      if(interpNum == 2)
        {
          sideDofNumbers_[sideNum][3] = 9; // dofs on side of side
          sideDofNumbers_[sideNum][4] = 8;
          sideDofNumbers_[sideNum][5] = 7;
        }

      // face #1 : x1+x2 = 1 (quadrangular)
      sideNum = 1;
      nbDofsPerSide = (interpNum + 1) * (interpNum + 1);
      sideDofNumbers_[sideNum].resize(nbDofsPerSide);
      sideDofNumbers_[sideNum][0] = 1;
      sideDofNumbers_[sideNum][1] = 2;
      sideDofNumbers_[sideNum][2] = 5;
      sideDofNumbers_[sideNum][3] = 4;
      if(interpNum == 2)
        {
          sideDofNumbers_[sideNum][4] = 7; // dofs on side of side
          sideDofNumbers_[sideNum][5] = 14;
          sideDofNumbers_[sideNum][6] = 10;
          sideDofNumbers_[sideNum][7] = 13;
          sideDofNumbers_[sideNum][8] = 16; // internal dof
        }

      // face #2 : x2 = 0 (quadrangular)
      sideNum = 2;
      sideDofNumbers_[sideNum].resize(nbDofsPerSide);
      sideDofNumbers_[sideNum][0] = 2;
      sideDofNumbers_[sideNum][1] = 3;
      sideDofNumbers_[sideNum][2] = 6;
      sideDofNumbers_[sideNum][3] = 5;
      if(interpNum == 2)
        {
          sideDofNumbers_[sideNum][4] = 8; // dofs on side of side
          sideDofNumbers_[sideNum][5] = 15;
          sideDofNumbers_[sideNum][6] = 11;
          sideDofNumbers_[sideNum][7] = 14;
          sideDofNumbers_[sideNum][8] = 17; // internal dof
        }

      // face #3 : x1 = 0 (quadrangular)
      sideNum = 3;
      sideDofNumbers_[sideNum].resize(nbDofsPerSide);
      sideDofNumbers_[sideNum][0] = 3;
      sideDofNumbers_[sideNum][1] = 1;
      sideDofNumbers_[sideNum][2] = 4;
      sideDofNumbers_[sideNum][3] = 6;
      if(interpNum == 2)
        {
          sideDofNumbers_[sideNum][4] = 9; // dofs on side of side
          sideDofNumbers_[sideNum][5] = 13;
          sideDofNumbers_[sideNum][6] = 12;
          sideDofNumbers_[sideNum][7] = 15;
          sideDofNumbers_[sideNum][8] = 18; // internal dof
        }

      // face #4 : x3 = 1 (triangular)
      sideNum = 4;
      nbDofsPerSide = ((interpNum + 1) * (interpNum + 2)) / 2;
      sideDofNumbers_[sideNum].resize(nbDofsPerSide);
      sideDofNumbers_[sideNum][0] = 4;
      sideDofNumbers_[sideNum][1] = 5;
      sideDofNumbers_[sideNum][2] = 6;
      if(interpNum == 2)
        {
          sideDofNumbers_[sideNum][3] = 10; // dofs on side of side
          sideDofNumbers_[sideNum][4] = 11;
          sideDofNumbers_[sideNum][5] = 12;
        }
    }
  trace_p->pop();
}

//! sideOfSideNumbering defines Lagrange Reference Element numbering on side of sides
void LagrangePrism::sideOfSideNumbering()
{
  //  trace_p->push("LagrangePrism::sideOfSideNumbering");
  number_t interpNum = interpolation_p->numtype;

  if(interpNum > 0)
    {
      number_t intN = interpNum;
      //if ( interpNum == _P1BubbleP3 ) { intN = 1; }
      number_t nbVertices = geomRefElem_p->nbVertices();
      number_t nbSideOfSides = geomRefElem_p->nbSideOfSides();
      number_t more;
      number_t nbVerticesPerSideOfSide = geomRefElem_p->sideOfSideVertexNumbers()[0].size();
      number_t nbDofsPerSideOfSide = intN + 1;

      sideOfSideDofNumbers_.resize(nbSideOfSides);
      for(number_t sideOfSide = 0; sideOfSide < nbSideOfSides; sideOfSide++)
        {
          sideOfSideDofNumbers_[sideOfSide].resize(nbDofsPerSideOfSide);
          for(number_t j = 0; j < nbVerticesPerSideOfSide; j++)
            {
              sideOfSideDofNumbers_[sideOfSide][j] = geomRefElem_p->sideOfSideVertexNumber(j + 1, sideOfSide + 1);
            }
          if(intN > 1)
            {
              more = nbVertices + 1;
              for(number_t k = nbVerticesPerSideOfSide; k < nbDofsPerSideOfSide; k++, more += nbSideOfSides)
                {
                  sideOfSideDofNumbers_[sideOfSide][k] = sideOfSide + more;
                }
            }
        }
    }
  //  trace_p->pop();
}

//! return nodes numbers of first order tetrahedron elements when splitting current element
std::vector<std::vector<number_t> > LagrangePrism::splitP1() const {
  std::vector<std::vector<number_t> > splitnum;
  std::vector<number_t> tuple(4,0);
  number_t k=interpolation_p->numtype;
  switch(k) {
    case 0:
    case 1:
      tuple[0]=4; tuple[1]=6; tuple[2]=1; tuple[3]=5;
      splitnum.push_back(tuple);
      tuple[0]=5; tuple[1]=6; tuple[2]=1; tuple[3]=3;
      splitnum.push_back(tuple);
      tuple[0]=3; tuple[1]=1; tuple[2]=5; tuple[3]=2;
      splitnum.push_back(tuple);
      return splitnum;
    case 2:
      // O1 sub-prism 1 7 9 13 16 18
      tuple[0]=13; tuple[1]=18; tuple[2]=1; tuple[3]=16;
      splitnum.push_back(tuple);
      tuple[0]=16; tuple[1]=18; tuple[2]=1; tuple[3]=9;
      splitnum.push_back(tuple);
      tuple[0]=9; tuple[1]=1; tuple[2]=16; tuple[3]=7;
      splitnum.push_back(tuple);
      // O1 sub-prism 7 2 8 16 14 17
      tuple[0]=16; tuple[1]=17; tuple[2]=7; tuple[3]=14;
      splitnum.push_back(tuple);
      tuple[0]=14; tuple[1]=17; tuple[2]=7; tuple[3]=8;
      splitnum.push_back(tuple);
      tuple[0]=8; tuple[1]=7; tuple[2]=14; tuple[3]=2;
      splitnum.push_back(tuple);
      // O1 sub-prism 8 9 7 17 18 16
      tuple[0]=17; tuple[1]=16; tuple[2]=8; tuple[3]=18;
      splitnum.push_back(tuple);
      tuple[0]=18; tuple[1]=16; tuple[2]=8; tuple[3]=7;
      splitnum.push_back(tuple);
      tuple[0]=7; tuple[1]=8; tuple[2]=18; tuple[3]=9;
      splitnum.push_back(tuple);
      // O1 sub-prism 9 8 3 18 17 15
      tuple[0]=18; tuple[1]=15; tuple[2]=9; tuple[3]=17;
      splitnum.push_back(tuple);
      tuple[0]=17; tuple[1]=15; tuple[2]=9; tuple[3]=3;
      splitnum.push_back(tuple);
      tuple[0]=3; tuple[1]=9; tuple[2]=17; tuple[3]=8;
      splitnum.push_back(tuple);
      // O1 sub-prism 13 16 18 4 10 12
      tuple[0]=4; tuple[1]=12; tuple[2]=13; tuple[3]=10;
      splitnum.push_back(tuple);
      tuple[0]=10; tuple[1]=12; tuple[2]=13; tuple[3]=18;
      splitnum.push_back(tuple);
      tuple[0]=18; tuple[1]=13; tuple[2]=10; tuple[3]=16;
      splitnum.push_back(tuple);
      // O1 sub-prism 16 14 17 10 5 11
      tuple[0]=10; tuple[1]=11; tuple[2]=16; tuple[3]=5;
      splitnum.push_back(tuple);
      tuple[0]=5; tuple[1]=11; tuple[2]=16; tuple[3]=17;
      splitnum.push_back(tuple);
      tuple[0]=17; tuple[1]=16; tuple[2]=5; tuple[3]=14;
      splitnum.push_back(tuple);
      // O1 sub-prism 17 18 16 11 12 10
      tuple[0]=11; tuple[1]=10; tuple[2]=17; tuple[3]=12;
      splitnum.push_back(tuple);
      tuple[0]=12; tuple[1]=10; tuple[2]=17; tuple[3]=16;
      splitnum.push_back(tuple);
      tuple[0]=16; tuple[1]=17; tuple[2]=12; tuple[3]=18;
      splitnum.push_back(tuple);
      // O1 sub-prism 18 17 15 12 11 6
      tuple[0]=12; tuple[1]=6; tuple[2]=18; tuple[3]=11;
      splitnum.push_back(tuple);
      tuple[0]=11; tuple[1]=6; tuple[2]=18; tuple[3]=15;
      splitnum.push_back(tuple);
      tuple[0]=15; tuple[1]=18; tuple[2]=11; tuple[3]=17;
      splitnum.push_back(tuple);
      return splitnum;
    default:
      return splitLagrange3DToP1();
  }
  return splitnum;
}

//! return nodes numbers of first order prism elements when splitting current element
splitvec_t LagrangePrism::splitO1() const
{
  splitvec_t splitnum;
  std::vector<number_t> tuple(6,0);
  number_t k=interpolation_p->numtype;
  switch(k)
    {
      case 1:
        tuple[0]=1; tuple[1]=2; tuple[2]=3; tuple[3]=4; tuple[4]=5; tuple[5]=6;
        splitnum.push_back(std::make_pair(_prism,tuple));
        return splitnum;
      case 2:
        tuple[0]=1; tuple[1]=7; tuple[2]=9; tuple[3]=13; tuple[4]=16; tuple[5]=18;
        splitnum.push_back(std::make_pair(_prism,tuple));
        tuple[0]=7; tuple[1]=2; tuple[2]=8; tuple[3]=16; tuple[4]=14; tuple[5]=17;
        splitnum.push_back(std::make_pair(_prism,tuple));
        tuple[0]=8; tuple[1]=9; tuple[2]=7; tuple[3]=17; tuple[4]=18; tuple[5]=16;
        splitnum.push_back(std::make_pair(_prism,tuple));
        tuple[0]=9; tuple[1]=8; tuple[2]=3; tuple[3]=18; tuple[4]=17; tuple[5]=15;
        splitnum.push_back(std::make_pair(_prism,tuple));
        tuple[0]=13; tuple[1]=16; tuple[2]=18; tuple[3]=4; tuple[4]=10; tuple[5]=12;
        splitnum.push_back(std::make_pair(_prism,tuple));
        tuple[0]=16; tuple[1]=14; tuple[2]=17; tuple[3]=10; tuple[4]=5; tuple[5]=11;
        splitnum.push_back(std::make_pair(_prism,tuple));
        tuple[0]=17; tuple[1]=18; tuple[2]=16; tuple[3]=11; tuple[4]=12; tuple[5]=10;
        splitnum.push_back(std::make_pair(_prism,tuple));
        tuple[0]=18; tuple[1]=17; tuple[2]=15; tuple[3]=12; tuple[4]=11; tuple[5]=6;
        splitnum.push_back(std::make_pair(_prism,tuple));
        return splitnum;
      default:
        return splitLagrange3DToPrismO1();
    }
  return splitnum;
}

//! Lagrange standard Pk prism Reference Element
template<number_t Pk>
LagrangeStdPrism<Pk>::LagrangeStdPrism(const Interpolation* interp_p)
  : LagrangePrism(interp_p)
{
  name_ += "_" + tostring(Pk);
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  computeShapeFunctions();
  maxDegree = Wk.degree();
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

//! Lagrange standard _P0 prism Reference Element
template<>
LagrangeStdPrism<_P0>::LagrangeStdPrism(const Interpolation* interp_p)
  : LagrangePrism(interp_p)
{
  name_ += "_0";
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  maxDegree = 1;
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

//! Lagrange standard _P1 prism Reference Element
template<>
LagrangeStdPrism<_P1>::LagrangeStdPrism(const Interpolation* interp_p)
  : LagrangePrism(interp_p)
{
  name_ += "_1";
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  // build O1 splitting scheme
  maxDegree = 2;
  splitO1Scheme = splitO1();
}

//! Lagrange standard _P2 prism Reference Element
template<>
LagrangeStdPrism<_P2>::LagrangeStdPrism(const Interpolation* interp_p)
  : LagrangePrism(interp_p)
{
  name_ += "_2";
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  computeShapeFunctions();
  maxDegree = Wk.degree();
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

//! computeShapeValues defines Lagrange Pk Reference Element shape functions
template<>
void LagrangeStdPrism<_P0>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  std::vector<real_t>::iterator sh0w_i(shv.w.begin());
  *sh0w_i = 1.;
  if(withDeriv)
    {
      std::vector< std::vector<real_t> >::iterator sh1w(shv.dw.begin()), sh2w(sh1w + 1), sh3w(sh1w + 2);
      std::vector<real_t>::iterator sh1w_i((*sh1w).begin()), sh2w_i((*sh2w).begin()), sh3w_i((*sh3w).begin());
      *sh1w_i = 0.; *sh2w_i = 0.; *sh3w_i = 0.;
    }
}

template<>
void LagrangeStdPrism<_P1>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x1 = *it_pt, x2 = *(it_pt + 1), x3 = *(it_pt + 2), x3m1 = x3 - 1, x7 = 1 - x1 - x2;
  std::vector<real_t>::iterator sh0w_i = shv.w.begin();
  *sh0w_i++ = -x1 * x3m1;
  *sh0w_i++ = -x2 * x3m1;
  *sh0w_i++ = -x7 * x3m1;
  *sh0w_i++ = x1 + x1 * x3m1;
  *sh0w_i++ = x2 + x2 * x3m1;
  *sh0w_i   = x7 + x7 * x3m1;
  if(withDeriv)
    {
      std::vector< std::vector<real_t> >::iterator sh1w(shv.dw.begin()), sh2w(sh1w + 1), sh3w(sh1w + 2);
      std::vector<real_t>::iterator sh1w_i((*sh1w).begin()), sh2w_i((*sh2w).begin()), sh3w_i((*sh3w).begin());
      *sh1w_i++ = -x3m1;    *sh2w_i++ = 0;        *sh3w_i++ = -x1;
      *sh1w_i++ = 0;        *sh2w_i++ = -x3m1;    *sh3w_i++ = -x2;
      *sh1w_i++ = x3m1;     *sh2w_i++ = x3m1;     *sh3w_i++ = -x7;
      *sh1w_i++ = x3;       *sh2w_i++ = 0;        *sh3w_i++ = x1;
      *sh1w_i++ = 0;        *sh2w_i++ = x3;       *sh3w_i++ = x2;
      *sh1w_i = -x3;        *sh2w_i = -x3;        *sh3w_i = x7;
    }
}

//compute shape functions using polynomial representation
template<>
void LagrangeStdPrism<_P2>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  computeShapeValuesFromShapeFunctions(it_pt,shv,withDeriv,with2Deriv);
}

//Eric: there are some bugs in computation of derivatives, find the bug is too boring
//template<>
//void LagrangeStdPrism<_P2>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
//{
//  real_t x1 = *it_pt, x2 = *(it_pt + 1), x3 = *(it_pt + 2),
//         x6 = 1 - x3, x7 = 1 - x1 - x2;
//  real_t dx1m1 = x1 + x1 - 1., dx2m1 = x2 + x2 - 1., dx3m1 = x3 - x6, dx7m1 = x7 + x7 - 1.,
//         qx1x2 = 4.*x1 * x2, qx1x7 = 4.*x1 * x7, qx2x7 = 4.*x2 * x7, qx3x6 = 4.*x3 * x6,
//         x1dx1 = x1 * dx1m1, x2dx2 = x2 * dx2m1, x3dx3 = x3 * dx3m1, x6dx6 = -x6 * dx3m1, x7dx7 = x7 * dx7m1;
//
//  std::vector<real_t>::iterator sh0w_i = shv.w.begin();
//  *sh0w_i = x1dx1;
//  *(sh0w_i + 1) = x2dx2;
//  *(sh0w_i + 2) = x7dx7;
//  *(sh0w_i + 6) = qx1x2;
//  *(sh0w_i + 7) = qx2x7;
//  *(sh0w_i + 8) = qx1x7;
//
//  for(number_t i = 0; i < 3; i++)
//    {
//      real_t a = *(sh0w_i + i);
//      *(sh0w_i + i + 12) = a * qx3x6;
//      *(sh0w_i + i + 3)  = a * x3dx3;
//      *(sh0w_i + i)    = a * x6dx6;
//      a = *(sh0w_i + i + 6);
//      *(sh0w_i + i + 15) = a * qx3x6;
//      *(sh0w_i + i + 9)  = a * x3dx3;
//      *(sh0w_i + i + 6)  = a * x6dx6;
//    }
//
//  if(withDeriv)
//    {
//      std::vector< std::vector<real_t> >::iterator sh1w(shv.dw.begin()), sh2w(sh1w + 1), sh3w(sh1w + 2);
//      std::vector<real_t>::iterator sh1w_i((*sh1w).begin()), sh2w_i((*sh2w).begin()), sh3w_i((*sh3w).begin());
//      real_t dp11 = dx1m1 + dx1m1 + 1.;
//      real_t dp13 = -(dx7m1 + dx7m1 + 1.);
//      real_t dp17 = dx2m1 + dx2m1 + 2.;
//      real_t dp22 = dp17 - 1.;
//      real_t dp27 = dp11 + 1.;
//      real_t dp34 = dx3m1 + dx3m1 + 1.;
//      real_t dp31 = dp34 - 2.;
//      *sh1w_i++ = x6dx6 * dp11;           *sh2w_i++ = 0.;                     *sh3w_i++ = x1dx1 * dp31; // 1
//      *sh1w_i++ = 0.;                     *sh2w_i++ = x6dx6 * dp22;           *sh3w_i++ = x2dx2 * dp31; // 2
//      *sh1w_i++ = x6dx6 * dp13;           *sh2w_i++ = x6dx6 * dp13;           *sh3w_i++ = x7dx7 * dp31; // 3
//      *sh1w_i++ = x3dx3 * dp11;           *sh2w_i++ = 0.;                     *sh3w_i++ = x1dx1 * dp34; // 4
//      *sh1w_i++ = 0.;                     *sh2w_i++ = x3dx3 * dp22;           *sh3w_i++ = x2dx2 * dp34; // 5
//      *sh1w_i++ = x3dx3 * dp13;           *sh2w_i++ = x3dx3 * dp13;           *sh3w_i++ = x7dx7 * dp34; // 6
//      *sh1w_i++ = x6dx6 * dp17;           *sh2w_i++ = x6dx6 * dp27;           *sh3w_i++ = qx1x2 * dp31; // 7
//      *sh1w_i++ = -x6dx6 * dp17;          *sh2w_i++ = -x6dx6 * (dp22 + dp13); *sh3w_i++ = qx2x7 * dp31; // 8
//      *sh1w_i++ = -x6dx6 * (dp11 + dp13); *sh2w_i++ = -x6dx6 * dp27;          *sh3w_i++ = qx1x7 * dp31; // 9
//      *sh1w_i++ = x3dx3 * dp17;           *sh2w_i++ = x3dx3 * dp27;           *sh3w_i++ = qx1x2 * dp34; // 10
//      *sh1w_i++ = -x3dx3 * dp17;          *sh2w_i++ = -x3dx3 * (dp22 + dp13); *sh3w_i++ = qx2x7 * dp34; // 11
//      *sh1w_i++ = -x3dx3 * (dp11 + dp13); *sh2w_i++ = -x3dx3 * dp27;          *sh3w_i++ = qx1x7 * dp34; // 12
//      *sh1w_i++ = qx3x6 * dp11;           *sh2w_i++ = 0.;                     *sh3w_i++ = -(2 * x1dx1 * (dp34 + 1)); // 13
//      *sh1w_i++ = 0.;                     *sh2w_i++ = qx3x6 * dp22;           *sh3w_i++ = -x2dx2 * (dp31 + dp34); // 14
//      *sh1w_i++ = qx3x6 * dp13;           *sh2w_i++ = qx3x6 * dp13;           *sh3w_i++ = -x7dx7 * (dp31 + dp34); // 15
//      *sh1w_i++ = dp17 * qx3x6;           *sh2w_i++ = qx3x6 * dp27;           *sh3w_i++ = -(2 * qx1x2 * (dp31 + 1)); // 16
//      *sh1w_i++ = -dp17 * qx3x6;          *sh2w_i++ = -qx3x6 * (dp22 + dp13); *sh3w_i++ = -(2 * qx2x7 * (dp31 + 1)); // 17
//      *sh1w_i   = -qx3x6 * (dp11 + dp13); *sh2w_i   = -qx3x6 * dp27;          *sh3w_i   = -(2 * qx1x7 * (dp31 + 1)); // 18
//    }
//}

} // end of namespace xlifepp
