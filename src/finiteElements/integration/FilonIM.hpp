/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file FilonIM.hpp
  \authors E. Lunéville
  \since 16 jan 2019
  \date  16 jan 2019

  \brief Implementation of the Filon method to compute 1D oscillatory integrals
 */

#ifndef FILONIM_HPP
#define FILONIM_HPP

#include "utils.h"
#include "config.h"
#include "IntegrationMethod.hpp"

namespace xlifepp
{

/*!
  \class FilonIMT
  compute I(x)=intg_0_T f(t)*exp(-ixt)dt using Filon method
              ~ dt sum_j=1,p Cj(x) sum_n=1,N f(t_n-1)exp(-ixt_n-1)
                with Cj(x)= intg_0,1 w_j(s) exp(-ixsdt)ds
                and  wj polynomial basis functions
  order 0 : P0 Lagrange basis w1(s)=1
  order 1 : P1 Lagrange basis w1(s)=1-s, w2(s)=s
  order 2 : P3 Hermite basis  w1(s)=(2s+1)(s-1)^2, w2(s)=s(s-1)^2, w3(s)=(3-2s)s^2, w4(s)=(s-1)s^2

  main functions are
      init()     : computes f(t_n) and f'(t_n) if order 2
      coef(x,j)  : computes Cj(x)
      compute(x) : evaluates I(x)
      operator (): calls compute(x)
      print()    : outputs some informations
*/

template <typename T = complex_t>
class FilonIMT : public SingleIM
{
  typedef T(*funT)(real_t);
  typedef T(*funTP)(real_t, Parameters& pa);

  protected:
    number_t ord_;        //!< interpolation order of (0,1 or 2)
    real_t tf_;           //!< upper bound of integral (lower bound is 0)
    real_t dt_;           //!< grid step if uniform
    number_t ns_;         //!< number of segments
    Vector<T> fn_;        //!< values of f (and df if required) on the grid
    Vector<real_t> tn_;   //!< grid points

  public:
    //! constructors
    FilonIMT(): ord_(0), tf_(0), dt_(0), ns_(0){}
    FilonIMT(number_t o, number_t N, real_t tf, funT f) : ord_(o), tf_(tf)
      {name="Filon_"+tostring(ord_)+"_"+tostring(N); imType=_FilonIM; init(N, f, 0);}
    FilonIMT(number_t o, number_t N, real_t tf, funT f, funT df) :ord_(o), tf_(tf)
      {name="Filon_"+tostring(ord_)+"_"+tostring(N); imType=_FilonIM; init(N, f, df);}
    FilonIMT(number_t o, number_t N, real_t tf, funTP f, Parameters & pars) : ord_(o),tf_(tf)
      {name="Filon_"+tostring(ord_)+"_"+tostring(N); imType=_FilonIM; init(N, f, 0, pars);}
    FilonIMT(number_t o, number_t N, real_t tf, funTP f, funTP df, Parameters& pars) :ord_(o), tf_(tf)
      {name="Filon_"+tostring(ord_)+"_"+tostring(N); imType=_FilonIM; init(N, f, df, pars);}
    FilonIMT(const Vector<T>& vf, real_t tf)
    {
        if(vf.size()<2) error("filon_two_values");
        ord_=1;
        fn_=vf;
        ns_=vf.size()-1;
        tf_=tf;
        dt_=tf/ns_;
        tn_.resize(ns_+1);
        for(number_t k=0;k<ns_+1; k++)tn_[k]=k*dt_;
        name="Filon_"+tostring(ord_)+"_"+tostring(ns_);
    }

    virtual FilonIMT<T>* clone() const      //covariant
    {return new FilonIMT<T>(*this);}

    void init(number_t N, funT f, funT df);                       //!< initialize f values
    void init(number_t N, funTP f, funTP df, Parameters& pars);   //!< initialize f values
    number_t size() const {return tn_.size();}                    //!< grid size
    void clear();                                                 //!< clear object

    template <typename S>
    complex_t coef(const S& x, number_t j) const;                 //!< Filon's coefficients regarding order_
    template <typename S>
    complex_t compute(const S& x) const;                          //!< compute I(x)
    template <typename S>
    complex_t operator()(const S& x) const                        //!< compute I(x)
    {return compute(x);}

    // print stuff
    virtual void print(std::ostream& os) const;    //! print FilonIMT on stream
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

typedef FilonIMT<complex_t> FilonIM;

//---------------------------------------------------------------------
// template implementation
//---------------------------------------------------------------------
template<typename T>
void FilonIMT<T>::init(number_t N, funT f_, funT df_)
{
  if(N==0) error("filon_one_point");
  if(ord_>2) error("filon_order");
  if(ord_==2 && df_==nullptr) error("filon_dif");

  // compute fn
  ns_=N; dt_=tf_/real_t(N);
  real_t t=0., h=dt_;
  number_t p=N+1;
  if(ord_==0) {t=0.5*h; p=N;}
  if(ord_==2) { p=2*(N+1);}
  fn_.resize(p);
  if(ord_==2) tn_.resize(N+1); else tn_.resize(p);
  // fn ordering
  // order 0 : f(h/2) f(3h/2) ... f((N-1/2)*h)        P0 Lagrange
  // order 1 : f(0) f(h) ... f(Nh)                    P1 Lagrange
  // order 2 : f(0) f'(0) f(h) f'(h) ... f(Nh) f'(Nh) P3 Hermite
  if(ord_<2)
    {
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t n=0; n<p; n++)
        {
          real_t nh=n*h;
          tn_[n]=nh;
          fn_[n]=f_(nh);
        }
    }
  else
    {
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t n=0; n<=N; n++)
        {
          real_t nh=n*h;
          tn_[n]=nh;
          fn_[2*n]=f_(nh);
          fn_[2*n+1]=df_(nh);
        }
    }
}

template<typename T>
void FilonIMT<T>::init(number_t N, funTP f_, funTP df_, Parameters& pars)
{
  if(N==0) error("filon_one_point");
  if(ord_>2) error("filon_order");
  if(ord_==2 && df_==nullptr) error("filon_dif");

  // compute fn
  ns_=N; dt_=tf_/real_t(N);
  real_t t=0., h=dt_;
  number_t p=N+1;
  if(ord_==0) {t=0.5*h; p=N;}
  if(ord_==2) { p=2*(N+1);}
  fn_.resize(p);
  if(ord_==2) tn_.resize(N+1); else tn_.resize(p);
  // fn ordering
  // order 0 : f(h/2) f(3h/2) ... f((N-1/2)*h)        P0 Lagrange
  // order 1 : f(0) f(h) ... f(Nh)                    P1 Lagrange
  // order 2 : f(0) f'(0) f(h) f'(h) ... f(Nh) f'(Nh) P3 Hermite
  if(ord_<2)
    {
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t n=0; n<p; n++)
        {
          real_t nh=n*h;
          tn_[n]=nh;
          fn_[n]=f_(nh,pars);
        }
    }
  else
    {
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t n=0; n<=N; n++)
        {
          real_t nh=n*h;
          tn_[n]=nh;
          fn_[2*n]=f_(nh, pars);
          fn_[2*n+1]=df_(nh,pars);
        }
    }
}

template<typename T>
void FilonIMT<T>:: clear()
{
    ord_=0; tf_=0; dt_=0; ns_=0;
    fn_.clear(); tn_.clear();
}

//compute j-th coefficient (j=0 to ord_)
template <typename T> template <typename S>
complex_t FilonIMT<T>::coef(const S& x, number_t j) const
{
  real_t eps =1.e-3;
  complex_t a=i_*x *dt_;
  if(std::abs(a)>eps)
    {
      complex_t e=std::exp(-a);
      complex_t a0 = (1.-e)/a;
      if(ord_==0) return a0;
      complex_t a1 = (a0-e)/a;
      if(ord_==1)
        {
          if(j==0) return a0-a1;
          return a1;
        }
      complex_t a2 = (2.*a1-e)/a;
      complex_t a3 = (3.*a2-e)/a;
      //hermite coefficients: w0, w1, w2, w3 with w0(0)=1, w1'(0)=1, w2(1)=1 and w3'(1)=1
      switch(j)
        {
          case 0: return 2.*a3-3.*a2+a0; break;
          case 1: return dt_*(a3-2.*a2+a1); break;
          case 2: return 3.*a2-2.*a3; break;
          default: return dt_*(a3-a2);
        }
    }
  //abs(a)<=eps
  if(ord_==0) return (6. - 3.*a + a*a)/6.;
  if(ord_==1)
  {
    if(j==0) return (12. - 4.*a + a*a)/24.;
    else     return (12. - 8.*a + 3.*a*a)/24.;
  }
  switch(j)
    {
      case 0: return (30. - 9.*a +2.*a*a)/60.; break;
      case 1: return (10. - 4.*a + a*a)*dt_/120.; break;
      case 2: return (30. - 21.*a + 8.*a*a)/60.; break;
      default:return (-5. + 3.*a - a*a)*dt_/60;
    }
  return 0.;
}

// compute I(x) on an uniform grid
template <typename T> template <typename S>
complex_t FilonIMT<T>::compute(const S& x) const
{
  complex_t I = 0.;
  number_t p=ord_+1;
  if(ord_==2) p++;  //Hermite p=4
  Vector<complex_t> s(p,complex_t(0.));
  complex_t e=std::exp(-i_*x *dt_);
  number_t q=ord_;  if(q==0) q=1;
  number_t qn=0; complex_t en=1.;
  for(number_t n=0; n<ns_; n++, en*=e, qn+=q)
    for(number_t j=0; j<p; j++)
      s[j]+= fn_[qn+j]*en;
  for(number_t j=0; j<p; j++) I+=s[j]*coef(x,j);
  I*=dt_;
  return I;
}

//print FilonIMT on stream
template<typename T>
void FilonIMT<T>::print(std::ostream& os) const
{
  number_t N=tn_.size();
  os<<"Filon Method, order "<<ord_<<", "<<N<<" segments"<<eol;
  if(theVerboseLevel<2) return;
  typename Vector<T>::const_iterator itf = fn_.begin();
  Vector<real_t>::const_iterator it=tn_.begin();
  os<<" tn -> fn ";
  if(ord_==2) os<<" fn' ";
  os<<" : ";
  if(N<=2*theVerboseLevel)
    {
      for(; it!=tn_.end(); ++it)
        {
          os<<" "<<*it<<"->";
          for(number_t i=0; i<std::max(number_t(1),ord_); i++, ++itf) os<<" "<<*itf;
          os<<" ;";
        }
    }
  else
    {
      number_t q=std::max(number_t(1),ord_);
      for(number_t n=0; n<theVerboseLevel; n++, ++it)
        {
          os<<" "<<*it<<"->";
          for(number_t i=0; i<q; i++, ++itf) os<<" "<<*itf;
          os<<" ;";
        }
      os<<" ... ";
      itf = fn_.end()-q*theVerboseLevel;
      it  = tn_.end()-theVerboseLevel;
      for(number_t n=0; n<theVerboseLevel; n++, ++it)
        {
          os<<" "<<*it<<"->";
          for(number_t i=0; i<ord_; i++, ++itf) os<<" "<<*itf;
          os<<" ;";
        }
    }
  os<<eol;
}
}
#endif // FILONIM_HPP_INCLUDED
