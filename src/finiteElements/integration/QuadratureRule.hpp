/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file QuadratureRule.hpp
  \authors D. Martin, E. Lunéville, N. Kielbasiewicz
  \since 16 dec  2008
  \date 7 fev 2013

  \brief Definition of the xlifepp::QuadratureRule class

  xlifepp::QuadratureRule is a class to store quadrature rule points & weights

  Def: A nodal rule is a quadrature rule for which points coincide with the nodes
  of a Reference Finite Element AND are stored in the same order

  1D rules
    - Gauss_Legendre_Rule                      degree 2n+1, n points
    - Gauss_Lobatto_Rule                       degree 2n-1, n points
    - Gauss_Jacobi_Rule                        degree 2n-1, n points
    - trapezoidal_Rule                         degree 1, 2 points
    - Simpson_Rule                             degree 3, 3 points
    - Simpson_3_8_Rule                         degree 3, 4 points
    - Boole_Rule                               degree 5, 5 points
    - Wedge_Rule                               degree 5, 6 points

  Rules over the unit simplex   (less quadrature points BUT with negative weights)
    - TN_Grundmann_Moller_Rule                 degree 2n+1, C^n_n+d+1 points

  Rules over the unit Triangle [ x > 0, y >0, x+y < 1 ]
    - T2_P2_MidEdge_Rule
    - T2_P2_Hammer_Stroud_Rule
    - T2_P3_Albrecht_Collatz_Rule              degree 3, 6 points, Stroud (p.314)
    - T2_P3_Stroud_Rule                        degree 3, 7 points, Stroud (p.308)
    - T2_P5_Radon_Hammer_Marlowe_Stroud_Rule   degree 5, 7 points, Stroud (p.314)
    - T2_P6_Hammer_Rule                        degree 6,12 points, G.Dhatt, G.Touzot (p.298)

  Rules over the unit Tetrahedron [ x > 0, y >0, z>0, x+y+z < 1 ]
    - T3_P2_Hammer_Stroud_Rule                 degree 2, 4 points, Stroud (p.307)
    - T3_P3_Stroud_Rule                        degree 3, 8 points, Stroud (p.308)
    - T3_P5_Stroud_Rule                        degree 5,15 points, Stroud (p.315)

  Symmetrical Gauss rules available on
     - Triangle, order up to 20, except 3
     - Quadrangle, odd order up to 21
     - Tetrahedron, order up to 10, except 4
     - Hexahedron, odd order up to 11
     - Prism, order up to 10
     - Pyramid, order up to 10

  Rules built on lower dimension rules
    - tensorRule          tensor product of quadrature rules
    - conicalRule         conical product for triangles
    - quadrangleNodalRule rule built from 1D nodal rule which are quadrangle nodal rule
    - hexahedronNodalRule rule built from 1D nodal rule which are hexahedron nodal rule

----------
References:
 A.H.Stroud, Approximate calculation of multiple integrals, Prentice Hall, 1971.
 G.Dhatt & G Touzot, The finite element method displayed,  John Wiley & Sons, Chichester, 1984
 A. Grundmann & H.M. Moller, Invariant Integration Formulas for the N-Simplex by Combinatorial Methods,
 SIAM Journal on Numerical Analysis, Vol 15, No 2, Apr. 1978, pages 282-290.
 F.D. Witherden and P.E. Vincent, On the identification of symmetric quadrature rules for finite element methods,
 Computers & Mathematics with Applications, Volume 69, Issue 10, May 2015, Pages 1232 1241
*/

#ifndef QUADRATURE_RULE_HPP
#define QUADRATURE_RULE_HPP

#include "config.h"

namespace xlifepp
{

/*!
  \class QuadratureRule
  represents a quadrature formula points and weights
*/
class QuadratureRule
{
  private:
    std::vector<real_t> coords_;    //!< point coordinates of quadrature rule
    std::vector<real_t> weights_;   //!< weights of quadrature rule
    dimen_t dim_;                   //!< dimension of points

    // Constructors and destructor
    // ----------------------------
  public:
    QuadratureRule() {}                                                 //!< default constructor
    QuadratureRule(const std::vector<real_t>&, real_t);                     //!< constructor for 1 point formula
    QuadratureRule(const std::vector<real_t>&, const std::vector<real_t>&); //!< constructor by coords and weights
    QuadratureRule(dimen_t , number_t);                                     //!< constructor by dimension and number of points

  private:
    void operator=(const QuadratureRule&);      //!< no allocation constructor

  public:
    // public access-to-member functions
    //------------------------------------
    dimen_t dim() const   //! returns dimension of quadrature points
    { return dim_; }
    number_t size() const //! number of points of quadrature rule
    { return static_cast<number_t>(weights_.size()); }
    const std::vector<real_t>& coords() const //! returns vector of point coordinates
    { return coords_; }
    const std::vector<real_t>& weights() const //! returns vector of point coordinates
    { return weights_; }
    std::vector<real_t>::const_iterator point(number_t i = 0) const  //! get i-th quadrature point as a vector (0 <= i)
    { return (coords_.begin() + i * dim_); }
    std::vector<real_t>::iterator point(number_t i = 0) //! get i-th quadrature point as a vector (0 <= i)
    { return (coords_.begin() + i * dim_); }
    std::vector<real_t>::const_iterator weight(number_t i = 0) const //! get const_iterator to i-th quadrature weight
    { return (weights_.begin() + i); }
    std::vector<real_t>::iterator weight(const number_t i = 0) //! get iterator to i-th quadrature weight
    { return (weights_.begin() + i); }
    //! resizes vectors of coordinates and weights
    void resize(dimen_t, number_t);

    //! input accumulator for 1d quadrature point
    void point(std::vector<real_t>::iterator&, real_t,
               std::vector<real_t>::iterator&, real_t);
    //! input accumulator for 2d quadrature point
    void point(std::vector<real_t>::iterator&, real_t, real_t,
               std::vector<real_t>::iterator&, real_t);
    //! input accumulator for 3d quadrature point
    void point(std::vector<real_t>::iterator&, real_t, real_t, real_t,
               std::vector<real_t>::iterator&, real_t);

    void coords(std::vector<real_t>::iterator&, dimen_t, std::vector<real_t>::const_iterator&); //!< input quadrature point coordinates in any dimension
    void coords(std::vector<real_t>::const_iterator);  //!< copy std::vector<real_t> into quadrature point coordinates
    void coords(const std::vector<real_t>&);           //!< copy std::vector<real_t> into quadrature point coordinates
    void coords(real_t);                               //!< barycentric quadrature point coordinates
    void weights(const std::vector<real_t>&);          //!< copy std::vector<real_t> into quadrature weights
    void weights(real_t);                              //!< uniform quadrature weights

    // tensor product rules and conical product rules
    // ---------------------------------------------
    QuadratureRule& tensorRule(const QuadratureRule&, const QuadratureRule&);  //!< tensor product of 2 1D rules of degree d.
    QuadratureRule& tensorRule(const QuadratureRule&, const QuadratureRule&, const QuadratureRule&);  //!< tensor product of 3 1D rules of degree d.
    QuadratureRule& conicalRule(const QuadratureRule&, const QuadratureRule&); //!< conical product of 2 1D rules (see A.H.Stroud, page 18--31 & 314)
    QuadratureRule& conicalRule(const QuadratureRule&, const QuadratureRule&, const QuadratureRule&); //!< conical product of 3 1D rules (see A.H.Stroud, pages 18-31 & 314)

    // nodal tensor product rules for quadrangle and hexahedron
    // --------------------------------------------------------
    QuadratureRule& quadrangleNodalRule(const QuadratureRule&); //!< nodal rule on quadrangles
    QuadratureRule& hexahedronNodalRule(const QuadratureRule&); //!< nodal rule on hexahedra

    // Quadrature rules, D and 2D rules are either used as-is or to build tensor product rules for 2D and 3D elements
    // --------------------------------------------------------------------------------------------------------------

    // 1D Gauss rules
    QuadratureRule& gaussLegendreRules(number_t); //!< Gauss-Legendre quadrature rule
    QuadratureRule& gaussLobattoRules(number_t);  //!< Gauss-Lobatto quadrature rule
    QuadratureRule& gaussJacobiRules(number_t); //!< Gauss-Legendre quadrature rule
    QuadratureRule& ruleOn01(number_t n, bool symmmetric=true); //!< quadrature rule on [0,1]

    // 1D Newton-Cotes closed rules (P_k node rules on [0,1])
    QuadratureRule& trapezoidalRule(); //!< trapezoidal quadrature rule
    QuadratureRule& simpsonRule();     //!< Simpson quadrature rule
    QuadratureRule& simpson38Rule();   //!< Simpson 3-8 quadrature rule
    QuadratureRule& booleRule();       //!< Boole quadratire rule

    // Grundmann-Möller_rules for n dimensional simplex, (with ***NEGATIVE*** weights for degre > 1)
    QuadratureRule& tNGrundmannMollerRule(int, dimen_t); //!< degree 2s+1, C^n_n+d+1 points rule over the unit simplex
    void tNGrundmannMollerSet(int);                      //!< modified version of John Burkhart's GM_RULE_SET function

    // special (Misc) rules for unit Triangle { x + y < 1 , 0 < x,y < 1}
    QuadratureRule& t2P2MidEdgeRule();                  //!< Degree 2, 3 points
    QuadratureRule& t2P2HammerStroudRule();             //!< Degree 2, 3 points, A.H.Stroud, page 307
    QuadratureRule& t2P3AlbrechtCollatzRule();          //!< Degree 3, 6 points, A.H.Stroud, page 314
    QuadratureRule& t2P3StroudRule();                   //!< Degree 3, 7 points, A.H.Stroud, page 308
    QuadratureRule& t2P5RadonHammerMarloweStroudRule(); //!< Degree 5, 7 points, A.H.Stroud, page 314
    QuadratureRule& t2P6HammerRule();                   //!< Degree 6, Hammer 12 internal points, G.Dhatt, G.Touzot page 298

    // special (Misc) rules for unit Tetrahedron { x + y + z < 1 , 0 < x,y,z < 1 }
    QuadratureRule& t3P2HammerStroudRule(); //!< Degree 2, 4 points, A.H.Stroud, page 307
    QuadratureRule& t3P3StroudRule();       //!< Degree 3, 8 points, A.H.Stroud, page 308
    QuadratureRule& t3P5StroudRule();       //!< Degree 5, 15 points, A.H.Stroud, page 315

    // special (Misc) rules for unit pyramid { 0 < x,y < 1 - z , 0 < z < 1 }
    QuadratureRule& pyramidRule(const QuadratureRule& qrxy, const QuadratureRule& qrz); //!< conical product of 3 1D rule
    QuadratureRule& pyramidStroudRule();                //!< Degree 7, 48 points, A.H.Stroud, page 339

    // symmetrical Gauss rules for any shape
    QuadratureRule& symmetricalGaussTriangleRule(number_t);    //!< symmetrical Gauss rule on triangle, order up to 20
    QuadratureRule& symmetricalGaussQuadrangleRule(number_t);  //!< symmetrical Gauss rule on quadrangle, odd order up to 21
    QuadratureRule& symmetricalGaussTetrahedronRule(number_t); //!< symmetrical Gauss rule on tetrahedron, order up to 10
    QuadratureRule& symmetricalGaussHexahedronRule(number_t);  //!< symmetrical Gauss rule on hexahedron, odd order up to 11
    QuadratureRule& symmetricalGaussPrismRule(number_t);       //!< symmetrical Gauss rule on prism, order up to 10
    QuadratureRule& symmetricalGaussPyramidRule(number_t);     //!< symmetrical Gauss rule on pyramid, order up to 10

    //print utilities
    // --------------
    friend std::ostream& operator<<(std::ostream&, const QuadratureRule&);

}; // end class QuadratureRule

std::ostream& operator<<(std::ostream&, const QuadratureRule&); //!< print operator
void compNext(int n, int k, int a[], bool* more, int* h, int* t); //!< computes the compositions of the integer N into K parts

} // end of namespace xlifepp

#endif //QUADRATURE_RULE_HPP
