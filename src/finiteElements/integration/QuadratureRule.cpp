/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file QuadratureRule.cpp
  \authors D. Martin, E. Lunéville, N. Kielbasiewicz
  \since 25 nov 2008
  \date 7 fev 2013

  \brief Implementation of xlifepp::QuadratureRule class members and related functions
 */

#include "QuadratureRule.hpp"
#include "../quadrangle/LagrangeQuadrangle.hpp"
#include "../hexahedron/LagrangeHexahedron.hpp"
#include "mathsResources.h"

namespace xlifepp
{
/*--------------------------------------------------------------------------------
   Constructors and destructor
--------------------------------------------------------------------------------*/
// constructor for 1 point formula
QuadratureRule::QuadratureRule(const std::vector<real_t>& c, real_t w)
  : coords_(c), weights_(1, w), dim_(static_cast<dimen_t>(c.size())) {}

// constructor by coords and weights
QuadratureRule::QuadratureRule(const std::vector<real_t>& c, const std::vector<real_t>& w)
  : coords_(c), weights_(w), dim_(static_cast<dimen_t>(c.size() / w.size())) {}

QuadratureRule::QuadratureRule(dimen_t d, number_t s)
  : coords_(d* s, 0), weights_(s, 0), dim_(d) {}

/*--------------------------------------------------------------------------------
    access-to-member functions (mutators)
--------------------------------------------------------------------------------*/
void QuadratureRule::resize(const dimen_t d, const number_t s)
{
  weights_.resize(s);
  coords_.resize(s*d);
  dim_ = d;
}

// input accumulator for 1d quadrature point
void QuadratureRule::point(std::vector<real_t>::iterator& c_it, real_t c1,
                           std::vector<real_t>::iterator& w_it, real_t w)
{ *c_it++ = c1; *w_it++ = w; }

// input accumulator for 2d quadrature point
void QuadratureRule::point(std::vector<real_t>::iterator& c_it, real_t c1, real_t c2,
                           std::vector<real_t>::iterator& w_it, real_t w)
{ *c_it++ = c1; *c_it++ = c2; *w_it++ = w; }

// input accumulator for 3d quadrature point
void QuadratureRule::point(std::vector<real_t>::iterator& c_it, real_t c1, real_t c2, real_t c3,
                           std::vector<real_t>::iterator& w_it, real_t w)
{  *c_it++ = c1; *c_it++ = c2; *c_it++ = c3; *w_it++ = w; }

// input quadrature point coordinates
void QuadratureRule::coords(std::vector<real_t>::iterator& c_it, const dimen_t d, std::vector<real_t>::const_iterator& c)
{ for(dimen_t dd = 0; dd < d; dd++) { *c_it++ = *c++; } }

// copy std::vector<real_t> into quadrature point coordinates
void QuadratureRule::coords(const std::vector<real_t>& c)
{ coords_ = c;}

// copy std::vector<real_t> into quadrature point coordinates
void QuadratureRule::coords(std::vector<real_t>::const_iterator c_it)
{
  for(std::vector<real_t>::iterator coords_i = coords_.begin(); coords_i != coords_.end(); coords_i++, c_it++)
  { *coords_i = *c_it; }
}

// input constant quadrature point cordinates
void QuadratureRule::coords(real_t c)
{
  for(std::vector<real_t>::iterator coords_i = coords_.begin(); coords_i != coords_.end(); coords_i++)
  { *coords_i = c; }
}

// copy std::vector<real_t> into quadrature weights
void QuadratureRule::weights(const std::vector<real_t>& w)
{ weights_ = w; }

// input constant weigths
void QuadratureRule::weights(real_t w)
{
  for(std::vector<real_t>::iterator weights_i = weights_.begin(); weights_i != weights_.end(); weights_i++)
  { *weights_i = w; }
}

/*--------------------------------------------------------------------------------
   tensor product rules
   - a nD rule of degree d requires a tensor product of n 1D rules of degree d.
--------------------------------------------------------------------------------*/
QuadratureRule& QuadratureRule::tensorRule(const QuadratureRule& q_r1,
    const QuadratureRule& q_r2)
{
  number_t nb_P1 = q_r1.size(), nb_P2 = q_r2.size();
  dimen_t dim_1 = q_r1.dim_, dim_2 = q_r2.dim_;
  resize(dim_1 + dim_2, nb_P1 * nb_P2);

  std::vector<real_t>::iterator c_i = coords_.begin();
  std::vector<real_t>::iterator w_i = weights_.begin();

  std::vector<real_t>::const_iterator c1_i = q_r1.coords_.begin();
  for(std::vector<real_t>::const_iterator w1_i = q_r1.weights_.begin(); w1_i != q_r1.weights_.end(); w1_i++, c1_i += dim_1)
  {
    std::vector<real_t>::const_iterator c2_i(q_r2.coords_.begin());
    for(std::vector<real_t>::const_iterator w2_i = q_r2.weights_.begin(); w2_i != q_r2.weights_.end(); w2_i++)
    {
      std::vector<real_t>::const_iterator c1_ii(c1_i);
      coords(c_i, dim_1, c1_ii); coords(c_i, dim_2, c2_i);
      *w_i++ = (*w1_i) * (*w2_i);
    }
  }
  return *this;
}

QuadratureRule& QuadratureRule::tensorRule(const QuadratureRule& q_r1,
    const QuadratureRule& q_r2,
    const QuadratureRule& q_r3)
{
  number_t nb_P = q_r1.size() * q_r2.size() * q_r3.size();
  resize(3, nb_P);
  std::vector<real_t>::iterator c_i = coords_.begin();
  std::vector<real_t>::iterator w_i = weights_.begin();

  std::vector<real_t>::const_iterator c1_i = q_r1.coords_.begin();
  for(std::vector<real_t>::const_iterator w1_i = q_r1.weights_.begin(); w1_i != q_r1.weights_.end(); w1_i++, c1_i++)
  {
    std::vector<real_t>::const_iterator c2_i = q_r2.coords_.begin();
    for(std::vector<real_t>::const_iterator w2_i = q_r2.weights_.begin(); w2_i != q_r2.weights_.end(); w2_i++, c2_i++)
    {
      std::vector<real_t>::const_iterator c3_i = q_r3.coords_.begin();
      for(std::vector<real_t>::const_iterator w3_i = q_r3.weights_.begin(); w3_i != q_r3.weights_.end(); w3_i++, c3_i++)
      { point(c_i, *c1_i, *c2_i, *c3_i, w_i, (*w1_i) * (*w2_i) * (*w3_i)); }
    }
  }
  return *this;
}

/*--------------------------------------------------------------------------------
   tensor product nodal rules
   - a nD rule of degree d requires a tensor product of n 1D rules of degree d.
--------------------------------------------------------------------------------*/
QuadratureRule& QuadratureRule::quadrangleNodalRule(const QuadratureRule& qr)
{
  number_t nbP1d = qr.size(), nbP = nbP1d * nbP1d;
  resize(2, nbP);

  // correspondence between segment and quadrangle local numbering
  std::vector<number_t> seg2quad(2 * nbP);
  tensorNumberingQuadrangle(nbP1d - 1, seg2quad);

  std::vector<number_t>::iterator r_i = seg2quad.begin();
  std::vector<real_t>::iterator c_i = coords_.begin();
  for(std::vector<real_t>::iterator w_i = weights_.begin(); w_i != weights_.end(); r_i += 2)
    point(c_i, qr.coords_[(*r_i)], qr.coords_[(*(r_i + 1))],
          w_i, qr.weights_[(*r_i)] * qr.weights_[(*(r_i + 1))]);
  return *this;
}

QuadratureRule& QuadratureRule::hexahedronNodalRule(const QuadratureRule& q_r)
{
  number_t nb_P_1d = q_r.size(), nb_P = nb_P_1d * nb_P_1d * nb_P_1d;
  resize(3, nb_P);
  std::vector<real_t>::iterator c_i = coords_.begin();

  // correspondence between segment and hexahedron local numbering
  number_t** seg2hexa;
  seg2hexa = new number_t*[3];
  for(dimen_t d = 0; d < 3; d++) { seg2hexa[d] = new number_t[nb_P]; }
  tensorNumberingHexahedron(nb_P_1d - 1, seg2hexa);

  number_t* t0 = seg2hexa[0];
  number_t* t1 = seg2hexa[1];
  number_t* t2 = seg2hexa[2];
  for(std::vector<real_t>::iterator w_i = weights_.begin(); w_i != weights_.end(); t0++, t1++, t2++)
    point(c_i, q_r.coords_[*t0], q_r.coords_[*t1], q_r.coords_[*t2],
          w_i, q_r.weights_[*t0]*q_r.weights_[*t1]*q_r.weights_[*t2]);

  for(dimen_t d = 0; d < 3; d++) { delete[] seg2hexa[d]; }
  delete[] seg2hexa;

  return *this;
}

/*--------------------------------------------------------------------------------
   conical product rules: see A.H.Stroud, pages 18--31 & 314
   - a rule of degree d over the triangle requires a conical product of a 1D rule q1
     of degree d+1 and a 1D rule q2 of degree d;
   - a rule of degree d over the tetrahedron requires a conical product of a 1D rule q1
     of degree d+2, a 1D rule q2 of degree d+1 and a 1D rule q3 of degree d
--------------------------------------------------------------------------------*/
QuadratureRule& QuadratureRule::conicalRule(const QuadratureRule& q_r1,
    const QuadratureRule& q_r2)
{
  number_t nb_P1 = q_r1.size(), nb_P2 = q_r2.size();
  resize(2, nb_P1 * nb_P2);

  std::vector<real_t>::iterator c_i = coords_.begin();
  std::vector<real_t>::iterator w_i = weights_.begin();

  std::vector<real_t>::const_iterator c1_i = q_r1.coords_.begin();
  for(std::vector<real_t>::const_iterator w1_i = q_r1.weights_.begin();
      w1_i != q_r1.weights_.end(); w1_i++, c1_i ++)
  {
    real_t one_c1 = 1. - *c1_i;
    std::vector<real_t>::const_iterator c2_i(q_r2.coords_.begin());
    for(std::vector<real_t>::const_iterator w2_i = q_r2.weights_.begin();
        w2_i != q_r2.weights_.end(); w2_i++, c2_i++)
    {
      point(c_i, *c1_i, one_c1 * *c2_i, w_i, one_c1 * *w1_i * *w2_i);
    }
  }
  return *this;
}

QuadratureRule& QuadratureRule::conicalRule(const QuadratureRule& q_r1,
    const QuadratureRule& q_r2,
    const QuadratureRule& q_r3)
{
  number_t nb_P1 = q_r1.size(), nb_P2 = q_r2.size(), nb_P3 = q_r3.size();
  resize(3, nb_P1 * nb_P2 * nb_P3);
  std::vector<real_t>::iterator c_i = coords_.begin();
  std::vector<real_t>::iterator w_i = weights_.begin();

  std::vector<real_t>::const_iterator c1_i = q_r1.coords_.begin();
  for(std::vector<real_t>::const_iterator w1_i = q_r1.weights_.begin();
      w1_i != q_r1.weights_.end(); w1_i++, c1_i ++)
  {
    real_t one_c1 = 1. - *c1_i;
    std::vector<real_t>::const_iterator c2_i(q_r2.coords_.begin());
    for(std::vector<real_t>::const_iterator w2_i = q_r2.weights_.begin();
        w2_i != q_r2.weights_.end(); w2_i++, c2_i++)
    {
      real_t one_c2 = 1. - *c2_i;
      std::vector<real_t>::const_iterator c3_i(q_r3.coords_.begin());
      for(std::vector<real_t>::const_iterator w3_i = q_r3.weights_.begin();
          w3_i != q_r3.weights_.end(); w3_i++, c3_i++)
        point(c_i, *c1_i, one_c1 * *c2_i, one_c1 * one_c2 * *c3_i,
              w_i, one_c1 * one_c1 * *w1_i * one_c2 * *w2_i * *w3_i);
    }
  }
  return *this;
}

/*--------------------------------------------------------------------------------
   Quadrature rules: points and weights
   1D and 2D rules are used as-is or to build product rules for 2D and 3D elements
--------------------------------------------------------------------------------*/
// n-point Gauss-Legendre formula exact for P_{2n-1} on [0,1]
QuadratureRule& QuadratureRule::gaussLegendreRules(number_t n)
{
  resize(1, n);
  // Gauss-Legendre rule on [-1 +1], only (n+1)/2 positive points sorted ascendingly, with assorted weights  are returned
  gaussLegendreRule(n, coords_, weights_);
  // Transforms [-1,+1] rule, with only (n+1)/2 positive points given into completed [0,1] n-point rule
  return ruleOn01(n);
}

// n-point Gauss-Lobatto formula exact for P_{2n-3} on [0,1]
QuadratureRule& QuadratureRule::gaussLobattoRules(number_t n)
{
  resize(1, n);
  // Gauss-Lobatto rule on [-1 +1], only (n+1)/2 positive points sorted ascendingly, with assorted weights
  gaussLobattoRule(n, coords_, weights_);
  // Transforms [-1,+1] rule, with only (n+1)/2 positive points given into completed [0,1] n-point rule
  return ruleOn01(n);
}

// n-point Gauss-Jacobi formula exact for P_{2n-3} on [0,1]
QuadratureRule& QuadratureRule::gaussJacobiRules(number_t n)
{
  resize(1, n);
  // Gauss-Jacobi rule on [-1 +1]
  gaussJacobi20Rule(n, coords_, weights_);
  // Transforms [-1,+1] rule into [0,1] n-point rule
  return ruleOn01(n, false);
}

/*--------------------------------------------------------------------------------
   Grundmann-Moller quadrature rules over the n-dimensional simplex
   *** CONTAIN NEGATIVE WEIGHTS *** for all degrees greater than 1
--------------------------------------------------------------------------------*/
QuadratureRule& QuadratureRule::tNGrundmannMollerRule(int s, dimen_t N)
{
  resize(N, binomialCoefficient(N + s + 1, s));
  tNGrundmannMollerSet(s);
  return *this;
}

void QuadratureRule::tNGrundmannMollerSet(int s)
{
  int dim_num(dim_);
  //
  //  Purpose: TNGrundmannMollerset sets a Grundmann-Moeller rule.
  //  Discussion:
  //    This is a revised version of the calculation which seeks to compute
  //    the value of the weight in a cautious way that avoids intermediate
  //    overflow.  Thanks to John Peterson for pointing out the problem on
  //    26 June 2008.
  //    This rule returns weights and abscissas of a Grundmann-Moeller
  //    quadrature rule for the n-dimensional unit simplex.
  //  Licensing: This code is distributed under the GNU LGPL license.
  //  Modified: 26 June 2008
  //  Author: John Burkardt
  //  Reference:
  //    Axel Grundmann, Michael Moeller, Invariant Integration Formulas for the N-Simplex by Combinatorial Methods,
  //    SIAM Journal on Numerical Analysis, Volume 15, Number 2, April 1978, pages 282-290.

  int beta_sum;
  real_t weight;
  int d(2 * s + 1), k(0), n(dim_);
  real_t one_pm = 1.0;
  int* beta = new int[dim_num + 1];
  real_t volume(1.);
  for(int di = 2; di <= dim_num; di++) { volume /= real_t(di); }
  real_t dn_2i(d + n);
  for(int i = 0; i <= s; i++, dn_2i -= 2)
  {
    weight = one_pm;
    int j_hi = std::max(n, std::max(d, d + n - i));
    for(int j = 1; j <= j_hi; j++)
    {
      real_t rj(j);
      if(j <= n) { weight *= rj; }
      if(j <= d) { weight *= dn_2i; }
      if(j <= 2 * s) { weight /= 2.0; }
      if(j <= i) { weight /= rj; }
      if(j <= d + n - i) { weight /= rj; }
    }
    one_pm = - one_pm;
    beta_sum = s - i;
    bool more = false;
    int h = 0;
    int t = 0;
    for(; ;)
    {
      compNext(beta_sum, dim_num + 1, beta, &more, &h, &t);
      weights_[k] = weight * volume;
      for(int di = 0; di < dim_num; di++)
      { coords_[di + k * dim_num] = real_t(2 * beta[di + 1] + 1) / dn_2i; }
      k = k + 1;
      if(!more) { break; }
    }
  }
  delete [] beta;
  return;
}

// ****************************************************************************80
void compNext(int n, int k, int a[], bool* more, int* h, int* t)
// ****************************************************************************80
//
//  Purpose: COMP_NEXT computes the compositions of the integer N into K parts.
//  Discussion:
//    A composition of the integer N into K parts is an ordered sequence
//    of K nonnegative integers which sum to N.  The compositions (1,2,1)
//    and (1,1,2) are considered to be distinct.
//    The routine computes one composition on each call until there are no more.
//    For instance, one composition of 6 into 3 parts is
//    3+2+1, another would be 6+0+0.
//    On the first call to this routine, set MORE = FALSE.  The routine
//    will compute the first element in the sequence of compositions, and
//    return it, as well as setting MORE = TRUE.  If more compositions
//    are desired, call again, and again.  Each time, the routine will
//    return with a new composition.
//    However, when the LAST composition in the sequence is computed
//    and returned, the routine will reset MORE to FALSE, signaling that
//    the end of the sequence has been reached.
//    This routine originally used a STATIC statement to maintain the
//    variables H and T.  I have decided (based on an wasting an
//    entire morning trying to track down a problem) that it is safer
//    to pass these variables as arguments, even though the user should
//    never alter them.  This allows this routine to safely shuffle
//    between several ongoing calculations.
//
//  Example:
//    The 28 compositions of 6 into three parts are:
//      6 0 0,  5 1 0,  5 0 1,  4 2 0,  4 1 1,  4 0 2,
//      3 3 0,  3 2 1,  3 1 2,  3 0 3,  2 4 0,  2 3 1,
//      2 2 2,  2 1 3,  2 0 4,  1 5 0,  1 4 1,  1 3 2,
//      1 2 3,  1 1 4,  1 0 5,  0 6 0,  0 5 1,  0 4 2,
//      0 3 3,  0 2 4,  0 1 5,  0 0 6.
//
//  Licensing: This code is distributed under the GNU LGPL license.
//  Modified: 09 July 2007
//  Author: FORTRAN77 original version by Albert Nijenhuis, Herbert Wilf,
//          C++ translation by John Burkardt.
//  Reference:
//    Albert Nijenhuis, Herbert Wilf,
//    Combinatorial Algorithms for Computers and Calculators,
//    Second Edition, Academic Press, 1978, ISBN: 0-12-519260-6, LC: QA164.N54.
//
//  Parameters:
//    Input, int N, the integer whose compositions are desired.
//    Input, int K, the number of parts in the composition.
//    Input/output, int A[K], the parts of the composition.
//    Input/output, bool *MORE.
//    Set MORE = FALSE on first call.  It will be reset to TRUE on return
//    with a new composition.  Each new call returns another composition until
//    MORE is set to FALSE when the last composition has been computed
//    and returned.
//    Input/output, int *H, *T, two internal parameters needed for the
//    computation.  The user should allocate space for these in the calling
//    program, include them in the calling sequence, but never alter them//
{
  int i;

  if(!(*more))
  {
    *t = n;
    *h = 0;
    a[0] = n;
    for(i = 1; i < k; i++) { a[i] = 0; }
  }
  else
  {
    if(1 < *t) { *h = 0; }

    *h = *h + 1;
    *t = a[*h - 1];
    a[*h - 1] = 0;
    a[0] = *t - 1;
    a[*h] = a[*h] + 1;
  }
  *more = (a[k - 1] != n);
  return;
}

/*--------------------------------------------------------------------------------
   Transforms [-1,+1] rule, with only (n+1)/2 positive points given in ascending order
   into [0,1] n-point rule in ascending order
--------------------------------------------------------------------------------*/
QuadratureRule& QuadratureRule::ruleOn01(number_t n, bool symmetric)
{
  if (symmetric)
  {
    number_t p_h((n - 1) / 2), n_p(n - 1);
    // points and weights on ].5, 1)
    for(number_t p = 0; p < (number_t)(n + 1) / 2; p++, p_h--, n_p--)
    {
      coords_[n_p] = (1. + coords_[p_h]) / 2.;
      weights_[n_p] = weights_[p_h] / 2.;
    }
    n_p = n - 1;
    // symmetry with respect to .5 for points and weights on (0,.5[
    for(number_t p = 0; p < (number_t)(n + 1) / 2; p++, n_p--)
    {
      coords_[p] = 1. - coords_[n_p];
      weights_[p] = weights_[n_p];
    }
  }
  else
  {
    // points and weights on [-1, 1]
    for(number_t p = 0; p < n; p++)
    {
      coords_[p] = (1. + coords_[p]) / 2.;
      weights_[p] = weights_[p] / 2.;
    }
  }
  return *this;
}

/*--------------------------------------------------------------------------------
    Newton-Cotes closed quadrature rules
--------------------------------------------------------------------------------*/
// 2-points trapezoidal rule exact for P_1 using 0, 1 nodes on [0,1]
QuadratureRule& QuadratureRule::trapezoidalRule()
{
  resize(1, 2);
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  point(c_i, 1., w_i, .5);
  point(c_i, 0., w_i, .5);
  return *this;
}

// 3-points Simpson's rule exact for P_3 using end and mid points on [0,1]
QuadratureRule& QuadratureRule::simpsonRule()
{
  resize(1, 3);
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  point(c_i, 1., w_i, over6_);
  point(c_i, 0., w_i, over6_);
  point(c_i, .5 , w_i, 4 * over6_);
  return *this;
}

// 4-points Simpson's 3-eigth rule exact for P_3 using P_3 nodes on [0,1]
QuadratureRule& QuadratureRule::simpson38Rule()
{
  resize(1, 4);
  real_t eighth(.125);
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  point(c_i, 1., w_i, eighth);
  point(c_i, 0., w_i, eighth);
  point(c_i, 2 * over3_, w_i, 3 * eighth);
  point(c_i, over3_, w_i, 3 * eighth);
  return *this;
}

// 5-points Boole's rule exact for P_5 using P_4 nodes on [0,1]
// due to Boole-Moulton, 1960 and mistakenly known as Bode's rule (due to a typo in Abramowitz-Stegun)
QuadratureRule& QuadratureRule::booleRule()
{
  resize(1, 5);
  real_t ninetieth(1. / 90.);
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  point(c_i, 1., w_i, 7 * ninetieth);
  point(c_i, 0., w_i, 7 * ninetieth);
  point(c_i, .75, w_i, 32 * ninetieth);
  point(c_i, .5 , w_i, 12 * ninetieth);
  point(c_i, .25, w_i, 32 * ninetieth);
  return *this;
}

// //! 6-point Wedge's rule exact for P_5 using P_5 nodes on [0,1]
// void QuadratureRule::Wedge_Rule()
// {
//     resize(1, 6);
//     return *this;
// }

/*--------------------------------------------------------------------------------
    Triangle formulae
--------------------------------------------------------------------------------*/
//  Degree 2 - 3 mid-edge point formula, P_1 unisolvent set
QuadratureRule& QuadratureRule::t2P2MidEdgeRule()
{
  resize(2, 3);
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  point(c_i, .5, .5, w_i, over6_);
  point(c_i, 0., .5, w_i, over6_);
  point(c_i, .5, 0., w_i, over6_);
  return *this;
}

//Degree 2 - 3 point Hammer-Stroud formula, A.H.Stroud, page 307, P_1 unisolvent set
QuadratureRule& QuadratureRule::t2P2HammerStroudRule()
{
  resize(2, 3);
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  point(c_i, 2.*over3_, over6_, w_i, over6_);
  point(c_i, over6_, 2.*over3_, w_i, over6_);
  point(c_i, over6_, over6_, w_i, over6_);
  return *this;
}

//Degree 3 - 6 point Albrecht-Collatz formula, A.H.Stroud, page 314 (T2:3-1), with P_2 unisolvent subset ???
QuadratureRule& QuadratureRule::t2P3AlbrechtCollatzRule()
{
  resize(2, 6);
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  const real_t onesixth(1. / 6.), onesixtieth(1 / 60.), ninesixtieth(9. / 60.);
  point(c_i, .5, .5, w_i, onesixtieth);
  point(c_i, .5, 0., w_i, onesixtieth);
  point(c_i, 0., .5, w_i, onesixtieth);
  point(c_i, onesixth, 2.*over3_, w_i, ninesixtieth);
  point(c_i, 2.*over3_, onesixth, w_i, ninesixtieth);
  point(c_i, onesixth, onesixth, w_i, ninesixtieth);
  return *this;
}

//Degree 3 - 7 point Stroud formula, A.H.Stroud, page 308 (Tn:3-3), with P_2 unisolvent subset ???
QuadratureRule& QuadratureRule::t2P3StroudRule()
{
  resize(2, 7);
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  const real_t twothirtieth(2 / 30.);
  point(c_i, 1., 0., w_i, 0.025);
  point(c_i, 0., 1., w_i, 0.025);
  point(c_i, 0., 0., w_i, 0.025);
  point(c_i, .5 , .5 , w_i, twothirtieth);
  point(c_i, 0., .5 , w_i, twothirtieth);
  point(c_i, .5 , 0., w_i, twothirtieth);
  point(c_i, over3_, over3_, w_i, 0.225);
  return *this;
}

//Degree 5 - 7 point Radon & Hammmer-Marlowe-Stroud formula, A.H.Stroud, page 314 (T2:5-1)
QuadratureRule& QuadratureRule::t2P5RadonHammerMarloweStroudRule()
{
  resize(2, 7);
  const real_t sqrt15 = std::sqrt(real_t(15.));
  const real_t w1 = (155 - sqrt15) / 2400., w2 = (155 + sqrt15) / 2400.,
             c1 = (6 - sqrt15) / 21., c2 = (6 + sqrt15) / 21.,
             c3 = (9 + 2 * sqrt15) / 21., c4 = (9 - 2 * sqrt15) / 21.;
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  point(c_i, over3_, over3_, w_i, .1125);
  point(c_i, c1, c1, w_i, w1);
  point(c_i, c1, c3, w_i, w1);
  point(c_i, c3, c1, w_i, w1);
  point(c_i, c2, c2, w_i, w2);
  point(c_i, c2, c4, w_i, w2);
  point(c_i, c4, c2, w_i, w2);
  return *this;
}

//Degree 6 - Hammer 12 internal point rule, (G.Dhatt, G.Touzot page 298)
QuadratureRule& QuadratureRule::t2P6HammerRule()
{
  resize(2, 12);
  const real_t c[4] = {0.063089014491502, 0.249286745170910, 0.310352451033785, 0.053145049844816};
  const real_t w[3] = {0.025422453185103, 0.058393137863189, 0.041425537809187};
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  point(c_i, c[0], c[0], w_i, w[0]);
  point(c_i, 1. - 2.*c[0], c[0], w_i, w[0]);
  point(c_i, c[0], 1. - 2.*c[0], w_i, w[0]);
  point(c_i, c[1], c[1], w_i, w[1]);
  point(c_i, 1. - 2.*c[1], c[1], w_i, w[1]);
  point(c_i, c[1], 1. - 2.*c[1], w_i, w[1]);
  point(c_i, c[2], c[3], w_i, w[2]);
  point(c_i, c[3], c[2], w_i, w[2]);
  real_t ocd(1. - c[2] - c[3]);
  point(c_i, ocd, c[2], w_i, w[2]);
  point(c_i, ocd, c[3], w_i, w[2]);
  point(c_i, c[2], ocd, w_i, w[2]);
  point(c_i, c[3], ocd, w_i, w[2]);
  return *this;
}

/*--------------------------------------------------------------------------------
    Tetrahedron formulae
--------------------------------------------------------------------------------*/
//Degree 2 - 4 point Hammer-Stroud formula, A.H.Stroud, page 307 (Tn:2-1)
QuadratureRule& QuadratureRule::t3P2HammerStroudRule()
{
  resize(3, 4);
  const real_t sqrt5 = std::sqrt(real_t(5.));
  const real_t r = (5 - sqrt5) / 20., s = (5 + 3 * sqrt5) / 20., w = 1. / 24.;
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  point(c_i, s, r, r, w_i, w);
  point(c_i, r, s, r, w_i, w);
  point(c_i, r, r, s, w_i, w);
  point(c_i, r, r, r, w_i, w);
  return *this;
}

//Degree 3 - 8 point Stroud formula, A.H.Stroud, page 308 (Tn:3-2)
QuadratureRule& QuadratureRule::t3P3StroudRule()
{
  resize(3, 8);
  const real_t w1 = 1. / 240, w2 = 9.*w1;
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  point(c_i, 1., 0., 0., w_i, w1);
  point(c_i, 0., 1., 0., w_i, w1);
  point(c_i, 0., 0., 1., w_i, w1);
  point(c_i, 0., 0., 0., w_i, w1);
  point(c_i, 0., over3_, over3_, w_i, w2);
  point(c_i, over3_, 0., over3_, w_i, w2);
  point(c_i, over3_, over3_, 0., w_i, w2);
  point(c_i, over3_, over3_, over3_, w_i, w2);
  return *this;
}

//Degree 5 - 15 point Stroud formula, A.H.Stroud, page 315 (T3:5-1)
QuadratureRule& QuadratureRule::t3P5StroudRule()
{
  resize(3, 15);
  const real_t sqrt15 = std::sqrt(real_t(15.));
  const real_t w1 = (2665. + 14 * sqrt15) / 226800.,
             w2 = (2665. - 14 * sqrt15) / 226800.,
             w3 = 10. / 1134.,
             c1 = (7 - sqrt15) / 34.,    c2 = (7 + sqrt15) / 34.,
             c3 = (10 - 2 * sqrt15) / 40., c4 = (10 + 2 * sqrt15) / 40.,
             c5 = (13 + 3 * sqrt15) / 34., c6 = (13 - 3 * sqrt15) / 34.;
  std::vector<real_t>::iterator c_i(coords_.begin());
  std::vector<real_t>::iterator w_i(weights_.begin());
  point(c_i, .25, .25, .25, w_i, 8. / 405.); // 1
  point(c_i, c1, c1, c1, w_i, w1); //2
  point(c_i, c5, c1, c1, w_i, w1); //3
  point(c_i, c1, c5, c1, w_i, w1); //4
  point(c_i, c1, c1, c5, w_i, w1); //5
  point(c_i, c2, c2, c2, w_i, w2); //6
  point(c_i, c6, c2, c2, w_i, w2); //7
  point(c_i, c2, c6, c2, w_i, w2); //8
  point(c_i, c2, c2, c6, w_i, w2); //9
  point(c_i, c4, c3, c3, w_i, w3); //10
  point(c_i, c3, c4, c3, w_i, w3); //11
  point(c_i, c3, c3, c4, w_i, w3); //12
  point(c_i, c3, c4, c4, w_i, w3); //13
  point(c_i, c4, c3, c4, w_i, w3); //14
  point(c_i, c4, c4, c3, w_i, w3); //15
  return *this;
}

QuadratureRule& QuadratureRule::pyramidRule(const QuadratureRule& qrxy, const QuadratureRule& qrz)
{
  number_t nxy=qrxy.size();
  number_t nz=qrz.size();
  resize(3,nxy*nxy*nz);
  std::vector<real_t>::iterator it_c=coords_.begin();
  std::vector<real_t>::iterator it_w=weights_.begin();
  real_t zz;
  for (number_t i=0; i<nxy; i++)
  {
    for (number_t j=0; j<nxy; j++)
    {
      for (number_t k=0; k<nz; k++)
      {
        zz=1.-qrz.coords_[k];
        point(it_c, qrxy.coords_[i]*zz, qrxy.coords_[j]*zz, qrz.coords_[k], it_w, 0.25*qrxy.weights_[i]*qrxy.weights_[j]*qrz.weights_[k]);
      }
    }
  }
  return *this;
}

// Degree 7, 48 points, A.H.Stroud, page 339
QuadratureRule& QuadratureRule::pyramidStroudRule()
{
  resize(3,48);
  std::vector<real_t>::iterator it_c=coords_.begin();
  std::vector<real_t>::iterator it_w=weights_.begin();

  real_t v=1.;

  real_t u1=0.4850054945e-1;
  real_t u2=0.2386007376;
  real_t u3=0.5170472951;
  real_t u4=0.7958514179;

  real_t a1=0.1108884156;
  real_t a2=0.1434587878;
  real_t a3=0.6863388717e-1;
  real_t a4=0.1035224075e-1;

  real_t r=std::sqrt(real_t(6./7.));
  real_t s=std::sqrt(real_t((114.-3.*std::sqrt(real_t(583.)))/287.));
  real_t t=std::sqrt(real_t((114.+3.*std::sqrt(real_t(583.)))/287.));

  real_t b1=real_t(49./810.)*v;
  real_t b2=real_t((178981.+2769.*std::sqrt(real_t(583.)))/1888920.)*v;
  real_t b3=real_t((178981.-2769.*std::sqrt(real_t(583.)))/1888920.)*v;

  point(it_c, (1.-r)*(1.-u1)/2.,                0., u1, it_w, a1*b1); // 1
  point(it_c, (1.+r)*(1.-u1)/2.,                0., u1, it_w, a1*b1); // 2
  point(it_c,                0., (1.-r)*(1.-u1)/2., u1, it_w, a1*b1); // 3
  point(it_c,                0., (1.+r)*(1.-u1)/2., u1, it_w, a1*b1); // 4
  point(it_c, (1.-s)*(1.-u1)/2., (1.-s)*(1.-u1)/2., u1, it_w, a1*b2); // 5
  point(it_c, (1.-s)*(1.-u1)/2., (1.+s)*(1.-u1)/2., u1, it_w, a1*b2); // 6
  point(it_c, (1.+s)*(1.-u1)/2., (1.-s)*(1.-u1)/2., u1, it_w, a1*b2); // 7
  point(it_c, (1.+s)*(1.-u1)/2., (1.+s)*(1.-u1)/2., u1, it_w, a1*b2); // 8
  point(it_c, (1.-t)*(1.-u1)/2., (1.-t)*(1.-u1)/2., u1, it_w, a1*b3); // 9
  point(it_c, (1.-t)*(1.-u1)/2., (1.+t)*(1.-u1)/2., u1, it_w, a1*b3); // 10
  point(it_c, (1.+t)*(1.-u1)/2., (1.-t)*(1.-u1)/2., u1, it_w, a1*b3); // 11
  point(it_c, (1.+t)*(1.-u1)/2., (1.+t)*(1.-u1)/2., u1, it_w, a1*b3); // 12

  point(it_c, (1.-r)*(1.-u2)/2.,                0., u2, it_w, a2*b1); // 13
  point(it_c, (1.+r)*(1.-u2)/2.,                0., u2, it_w, a2*b1); // 14
  point(it_c,                0., (1.-r)*(1.-u2)/2., u2, it_w, a2*b1); // 15
  point(it_c,                0., (1.+r)*(1.-u2)/2., u2, it_w, a2*b1); // 16
  point(it_c, (1.-s)*(1.-u2)/2., (1.-s)*(1.-u2)/2., u2, it_w, a2*b2); // 17
  point(it_c, (1.-s)*(1.-u2)/2., (1.+s)*(1.-u2)/2., u2, it_w, a2*b2); // 18
  point(it_c, (1.+s)*(1.-u2)/2., (1.-s)*(1.-u2)/2., u2, it_w, a2*b2); // 19
  point(it_c, (1.+s)*(1.-u2)/2., (1.+s)*(1.-u2)/2., u2, it_w, a2*b2); // 20
  point(it_c, (1.-t)*(1.-u2)/2., (1.-t)*(1.-u2)/2., u2, it_w, a2*b3); // 21
  point(it_c, (1.-t)*(1.-u2)/2., (1.+t)*(1.-u2)/2., u2, it_w, a2*b3); // 22
  point(it_c, (1.+t)*(1.-u2)/2., (1.-t)*(1.-u2)/2., u2, it_w, a2*b3); // 23
  point(it_c, (1.+t)*(1.-u2)/2., (1.+t)*(1.-u2)/2., u2, it_w, a2*b3); // 24

  point(it_c, (1.-r)*(1.-u3)/2.,                0., u3, it_w, a3*b1); // 25
  point(it_c, (1.+r)*(1.-u3)/2.,                0., u3, it_w, a3*b1); // 26
  point(it_c,                0., (1.-r)*(1.-u3)/2., u3, it_w, a3*b1); // 27
  point(it_c,                0., (1.+r)*(1.-u3)/2., u3, it_w, a3*b1); // 28
  point(it_c, (1.-s)*(1.-u3)/2., (1.-s)*(1.-u3)/2., u3, it_w, a3*b2); // 29
  point(it_c, (1.-s)*(1.-u3)/2., (1.+s)*(1.-u3)/2., u3, it_w, a3*b2); // 30
  point(it_c, (1.+s)*(1.-u3)/2., (1.-s)*(1.-u3)/2., u3, it_w, a3*b2); // 31
  point(it_c, (1.+s)*(1.-u3)/2., (1.+s)*(1.-u3)/2., u3, it_w, a3*b2); // 32
  point(it_c, (1.-t)*(1.-u3)/2., (1.-t)*(1.-u3)/2., u3, it_w, a3*b3); // 33
  point(it_c, (1.-t)*(1.-u3)/2., (1.+t)*(1.-u3)/2., u3, it_w, a3*b3); // 34
  point(it_c, (1.+t)*(1.-u3)/2., (1.-t)*(1.-u3)/2., u3, it_w, a3*b3); // 35
  point(it_c, (1.+t)*(1.-u3)/2., (1.+t)*(1.-u3)/2., u3, it_w, a3*b3); // 36

  point(it_c, (1.-r)*(1.-u4)/2.,                0., u4, it_w, a4*b1); // 37
  point(it_c, (1.+r)*(1.-u4)/2.,                0., u4, it_w, a4*b1); // 38
  point(it_c,                0., (1.-r)*(1.-u4)/2., u4, it_w, a4*b1); // 39
  point(it_c,                0., (1.+r)*(1.-u4)/2., u4, it_w, a4*b1); // 40
  point(it_c, (1.-s)*(1.-u4)/2., (1.-s)*(1.-u4)/2., u4, it_w, a4*b2); // 41
  point(it_c, (1.-s)*(1.-u4)/2., (1.+s)*(1.-u4)/2., u4, it_w, a4*b2); // 42
  point(it_c, (1.+s)*(1.-u4)/2., (1.-s)*(1.-u4)/2., u4, it_w, a4*b2); // 43
  point(it_c, (1.+s)*(1.-u4)/2., (1.+s)*(1.-u4)/2., u4, it_w, a4*b2); // 44
  point(it_c, (1.-t)*(1.-u4)/2., (1.-t)*(1.-u4)/2., u4, it_w, a4*b3); // 45
  point(it_c, (1.-t)*(1.-u4)/2., (1.+t)*(1.-u4)/2., u4, it_w, a4*b3); // 46
  point(it_c, (1.+t)*(1.-u4)/2., (1.-t)*(1.-u4)/2., u4, it_w, a4*b3); // 47
  point(it_c, (1.+t)*(1.-u4)/2., (1.+t)*(1.-u4)/2., u4, it_w, a4*b3); // 48
  return *this;
}


/*--------------------------------------------------------------------------------
    print utilities
--------------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& os, const QuadratureRule& obj)
{
  os << std::endl << std::setw(entryWidth) << "Weights";
  for(dimen_t d = 0; d < obj.dim_; d++) { os << std::setw(entryWidth - 2) << "Coords[" << d << "]"; }
  std::vector<real_t>::const_iterator c_i = obj.coords_.begin();
  number_t prec=entryPrec;
  if (isTestMode) { prec=testPrec; }
  for(std::vector<real_t>::const_iterator w_i = obj.weights_.begin(); w_i != obj.weights_.end(); w_i++)
  {
    os << std::endl << std::setw(entryWidth) << std::setprecision(prec) << *w_i;
    for(dimen_t d = 0; d < obj.dim_; d++)
    { os << std::setw(entryWidth) << std::setprecision(prec) << *(c_i++); }
  }
  os << std::endl;
  return os;
}

} // end of namespace xlifepp
