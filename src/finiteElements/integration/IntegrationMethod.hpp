/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file IntegrationMethod.hpp
  \authors E. Lunéville
  \since 25 sept 2013
  \date  25 aug 2016

  \brief Definition of the xlifepp::IntegrationMethod class

  xlifepp::IntegrationMethod is a the abstract class of integration methods used in FE, IE, ... integral computation on elements
  All the integration techniques has to inherit from this base class. The main child classes are also declared here (may be elsewhere):
  the inheriting scheme is currently the following:

                           | SingleIM (single intg) ---> | QuadratureIM (standard quadrature based on Quadrature class)
                           |        (abstract)           | PolynomialIM (exact integration of polynoms on a segment, triangle,...)
    IntegrationMethod ---> |                             | LenoirSalles2dIR (Lenoir-Salles analytic method for IE singular integral on segments, Laplace P0 and P1)
                           |                             | LenoirSalles3dIR (Lenoir-Salles analytic method for IE singular integral on triaagles,Laplace P0 and P1)
                           |                             | FilonIM (Filon method for 1D oscillatory integrals intg_0_T f(t)exp(-iat))
       (abstract)          |
                           | DoubleIM (double intg) ---> | ProductIM        (product of single intg methods)
                                    (abstract)           | LenoirSalles2DIM (Lenoir-Salles analytic method for IE singular integral, over a product of segments, Laplace P0 and P1)
                                                         | LenoirSalles3DIM (Lenoir-Salles analytic method for IE singular integral, over a product of triangles, Laplace P0 and P1)
                                                         | SauterSchwabIM   (Sauter-Schwab method for IE singular integral, over a product of triangles)
                                                         | SauterSchwabSymIM(Sauter-Schwab method for IE singular integral with symmetrical kernels, over a product of triangles)
                                                         | DuffyIM          (Duffy method for IE singular integral, over a product of segments)
                                                         | CollinoIM        (Collino semi-analytic method for IE singular integral over a product of triangles, Maxwell RT0)


  The child classes may be used by end users,
     - by instantiating an object of these classes transmitted to linear or bilinear form classes
     - specifying a quadrature name and order in linear or bilinear form definition
  instantiating an object allows to define precisely parameters of integration method which is mandatory for complex method
  while specifying a quadrature name and order is sufficient for simple method (e.g standard quadrature rule) or using default parameters

  to include a new integration method, develop a new class inherited from IntegrationMethod and update the enum IntegrationMethodType

  the class IntegrationMethods collect some IntegrationMethod with additional parameters using the IntgMeth class that manages
    - an IntegrationMethod pointer
    - the part of the function that it is addressed (_allFunction , _regularPart, _singularPart)
    - a real value b saying that this intg method is applied when a criteria is less or equal to b

    in BEM computation, the criteria is generally the distance between two elements

*/

#ifndef INTEGRATION_METHOD_HPP
#define INTEGRATION_METHOD_HPP

#include "Quadrature.hpp"
#include "../ShapeValues.hpp"
#include "config.h"

namespace xlifepp
{

//forward class declaration
class Element;
class AdjacenceInfo;
class KernelOperatorOnUnknowns;
class OperatorOnUnknown;
class IEcomputationParameters;

//========================================================================================
/*!
  \class IntegrationMethod
  abstract class for integration methods
*/
//========================================================================================
class IntegrationMethod
{
  public:
    IntegrationMethodType imType;   //!< integration method type (see enum)
    mutable string_t name;          //!< name of the integration method
    SingularityType singularType;   //!< type of singularity supported (_notsingular, _r, _logr,_loglogr)
    real_t singularOrder;           //!< singularity order
    string_t kerName;               //!< kernel shortname when adapted to a particular kernel (default is empty)
    bool requireRefElement;         //!< true if method require reference element (false)
    bool requirePhyElement;         //!< true if method require physical element (false)
    bool requireNormal;             //!< true if method involves normal (false)

    //! constructor
    IntegrationMethod(IntegrationMethodType imt=_undefIM, const string_t& na="",
                      SingularityType st=_notsingular, real_t so=0, const string_t& kn="", bool ref=false, bool phy=false, bool nor=false)
      : imType(imt), name(na), singularType(st), singularOrder(so), kerName(kn),
        requireRefElement(ref), requirePhyElement(phy), requireNormal(nor) {}

    virtual IntegrationMethod* clone() const =0;

    virtual void print(std::ostream& os) const         //! print IntegrationMethod on stream
    {os<<"IntegrationMethod "<<name;}
    virtual void print(PrintStream& os) const {print(os.currentStream());}

    virtual ~IntegrationMethod() {}     //!< virtual destructor
    virtual bool isSingleIM() const =0; //!< returns true if single IM
    virtual bool isDoubleIM() const =0; //!< returns true if double IM
    IntegrationMethodType type() const {return imType;}      //!< returns type of integration method
    virtual bool useQuadraturePoints() const {return false;} //!< returns true if quadrature points are used
    virtual std::list<Quadrature*> quadratures() const
    {
      //error("not_handled","IntegrationMethod::quadratures()");
      return std::list<Quadrature*>(); // dummy return
    }
};

std::ostream& operator<<(std::ostream&, const IntegrationMethod&);  //!< output IntegrationMethod on stream

//========================================================================================
/*!
  \class SingleIM
  abstract class for integration method of single integral over E
*/
//========================================================================================
class SingleIM : public IntegrationMethod
{
  public:
    bool isSingleIM() const {return true;}
    bool isDoubleIM() const {return false;}

    SingleIM(IntegrationMethodType imt=_undefIM, const string_t na="SingleIM")  //! basic constructor
      :IntegrationMethod(imt,na) {}

    virtual SingleIM* clone() const               // covariant
    {return new SingleIM(*this);}

    virtual void computeIR(const Element*, const Point&, const OperatorOnUnknown&, number_t,
                           const Vector<real_t>* nx, const Vector<real_t>* ny, Vector<real_t>&, dimen_t&) const
    {error("not_handled","SingleIM::computeIR(Element*, Point, OperatorOnUnknown, Number, Reals, Reals, Reals)");}
    virtual void computeIR(const Element*, const Point&, const OperatorOnUnknown&, number_t,
                           const Vector<real_t>* nx, const Vector<real_t>* ny, Vector<complex_t>&, dimen_t&) const
    {error("not_handled","SingleIM::computeIR(Element*, Point, OperatorOnUnknown, Number, Reals, Reals, Complexes)");}

    virtual void print(std::ostream& os) const    //! print IntegrationMethod on stream
    {os<<"Single Integration Method "<<name;}
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

//========================================================================================
/*!
  \class QuadratureIM
  class devoted to integral over a geometric element
  may be dealt with different shapes
*/
//========================================================================================
class QuadratureIM : public SingleIM
{
  public:
    mutable QuadRule quadRule;           //!< QuadRule to be used
    mutable number_t degree;             //!< degree of quadrature
    mutable std::map<ShapeType, Quadrature*> quadratures_;                //!< structure to store Quadrature formula for different shapes
    mutable std::map<ShapeType, std::vector<ShapeValues>* > shapevalues_; //!< structure to store shape values on quadrature points

  public:
    QuadratureIM(QuadRule =_defaultRule, number_t =0);                              //!< default constructor (incomplete)
    QuadratureIM(ShapeType, QuadRule =_defaultRule, number_t =0);                   //!< constructor for one shape
    QuadratureIM(const std::set<ShapeType>&, QuadRule =_defaultRule, number_t =0);  //!< constructor for a set of shapes
    void setQuadratures(const std::set<ShapeType>&) const;                          //!< set Quadratures for some shapes
    void setQuadrature(ShapeType) const;                                            //!< set Quadratures for a shape
    ~QuadratureIM();                         //!< destructor
    void clear();                            //!< deallocate shapevalues vector pointer and clear maps

    virtual QuadratureIM* clone() const      //covariant
    {return new QuadratureIM(*this);}

    virtual bool useQuadraturePoints() const {return true;}

    virtual std::list<Quadrature*> quadratures() const;                //!< return the list of (single) quadratures in a list
    Quadrature* getQuadrature(ShapeType) const;                        //!< return quadrature pointer related to shape
    void setShapeValues(ShapeType, std::vector<ShapeValues>*) const;   //!< set shape values for a shape
    std::vector<ShapeValues>* getShapeValues(ShapeType) const;         //!< return shape values related to shape

    virtual void print(std::ostream& os) const;         //!< print on stream
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

std::ostream& operator<<(std::ostream&, const QuadratureIM&);  //!< output on stream

//========================================================================================
/*!
  \class PolynomialIM
  integral over a geometric element using analytic formula for polynomial integrand
*/
//========================================================================================
class PolynomialIM : public SingleIM
{
  public:
    virtual void print(std::ostream& os) const    //! print IntegrationMethod on stream
    {os<<"Polynomial Integration Method ";}
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

//forward class declaration
class Element;
class AdjacenceInfo;
class KernelOperatorOnUnknowns;
class IEcomputationParameters;

//========================================================================================
/*!
  \class DoubleIM
  abstract class for integration method of double integral over ExF
*/
//========================================================================================
class DoubleIM : public IntegrationMethod
{
  public:
    bool isSingleIM() const {return false;}
    bool isDoubleIM() const {return true;}
    DoubleIM(IntegrationMethodType imt=_undefIM, const string_t& na="DoubleIM")  //! basic constructor
      :IntegrationMethod(imt, na) {}

    virtual DoubleIM* clone() const               //covariant
    {return new DoubleIM(*this);}

    virtual void print(std::ostream& os) const    //! print IntegrationMethod on stream
    {os<<"Double Integration Method "<<name;}
    virtual void print(PrintStream& os) const {print(os.currentStream());}

    //virtual IE computation functions
    virtual void computeIE(const Element*, const Element*,AdjacenceInfo&,const KernelOperatorOnUnknowns&,
                           Matrix<real_t>&,IEcomputationParameters&, Vector<real_t>& vu, Vector<real_t>& vv, Vector<real_t>& vk) const
    { error("not_handled", "DoubleIM::computeIE(Element*, Element*, AdjacenceInfo, KernelOperatorOnUnknowns, RealMatrix, IEComputationParameters, Reals, Reals, Reals)"); }
    virtual void computeIE(const Element*, const Element*,AdjacenceInfo&,const KernelOperatorOnUnknowns&,
                           Matrix<complex_t>&,IEcomputationParameters&, Vector<complex_t>& vu, Vector<complex_t>& vv, Vector<complex_t>& vk) const
    { error("not_handled", "DoubleIM::computeIE(Element*, Element*, AdjacenceInfo, KernelOperatorOnUnknowns, ComplexMatrix, IEComputationParameters, Complexes, Complexes, Complexes)"); }
};

//========================================================================================
/*!
  \class ProductIM
  product of single integration methods
*/
//========================================================================================
class ProductIM : public DoubleIM
{
  protected:
    SingleIM* im_x;     //!< integration method along x
    SingleIM* im_y;     //!< integration method along y

  public:
    ProductIM(SingleIM* imx=nullptr, SingleIM* imy=nullptr);         //!< basic constructor
    ProductIM(const SingleIM& imx, const SingleIM& imy); //!< basic constructor
    ProductIM(const SingleIM& imxy);                     //!< basic constructor
    ProductIM(ShapeType, QuadRule quad_xy=_defaultRule, number_t ord_xy=0);                //!< constructor from a shape and a quadrature rule
    ProductIM(ShapeType, QuadRule quad_x, number_t ord_x, QuadRule quad_y, number_t ord_y);//!< constructor from a shape and two quadrature rules
    ProductIM(QuadRule quad_xy, number_t ord_xy);                                          //!< constructor from a quadrature rule (Quadrature not built in!)
    ProductIM(QuadRule quad_x, number_t ord_x, QuadRule quad_y, number_t ord_y);           //!< constructor from two quadrature rules (Quadrature not built in!)

    ProductIM(const ProductIM&);            //!< copy constructor
    ProductIM& operator=(const ProductIM&); //!< assign operator
    ~ProductIM();                           //!< destructor

    virtual ProductIM* clone() const             //covariant
    {return new ProductIM(*this);}

    SingleIM* getxIM() const                     //! get x-integration method
    {return im_x;}
    SingleIM* getyIM() const                     //! get y-integration method
    {return im_y;}
    virtual bool useQuadraturePoints() const
    {return im_x->useQuadraturePoints() && im_y->useQuadraturePoints();}
    virtual void print(std::ostream& os) const;  //!< print on //stream
    virtual void print(PrintStream& os)  const {print(os.currentStream());}
};

//========================================================================================
/*!
  \class IntgMeth

  structure to handle a triplet (integration method, function part, lower bound)
*/
//========================================================================================
class IntgMeth
{
  public:
    const IntegrationMethod* intgMeth;  //!< pointer to an IntegrationMethod
    FunctionPart functionPart;          //!< function part involved
    real_t bound;                       //!< bound value used by some geometric criterium (for instance d(Ei,Ej)>bound)

    IntgMeth(const IntegrationMethod& im, FunctionPart fp=_allFunction, real_t b=0);
    IntgMeth(const IntgMeth&);
    ~IntgMeth();
    IntgMeth& operator=(const IntgMeth&);

    void print(std::ostream&) const;
    void print(PrintStream& out) const {print(out.currentStream());}
    friend std::ostream& operator<<(std::ostream& out, const IntgMeth& im)
    {im.print(out); return out;}
};

//========================================================================================
/*!
  \class IntegrationMethods

  handles a collection of IntegrationMethod and related criteria
  constructor keys  :
     _method : either an IntegrationMethod object or an IntegrationMethodType or a QuadRule
     _quad   : either a QuadRule or an IntegrationMethodType (default value : _defaultRule)
     _order  : order related to _method or _quad
     _bound  : bound of the relative element distance to apply integration method (default value : theRealMax)
     _function_part: one of _allFunction (default), _regularPart, _singularPart

    multiple keys, up to 21 keys can be given :
      _method=m1,[_order=o1],[_function_part=fp1],_bound=b1,
      _quad=q2,  [_order=o2],[_function_part=fp2],_bound=b2,
      ...,
      _method=mn,[_order=on],[_function_part=fpn]

*/
//========================================================================================
class IntegrationMethods
{
  public:
    std::vector<IntgMeth> intgMethods;  //!< collection of IntegrationMethod
    typedef std::vector<IntgMeth>::const_iterator const_iterator;
    typedef std::vector<IntgMeth>::iterator iterator;

    IntegrationMethods() {} //!< default constructor
    IntegrationMethods(const IntegrationMethod&, FunctionPart =_allFunction, real_t =theRealMax); //!< basic constructor
    IntegrationMethods(const IntegrationMethod&, FunctionPart, real_t,
                       const IntegrationMethod&, FunctionPart =_allFunction, real_t = theRealMax);//!< constructor from 2 integration methods
    IntegrationMethods(const IntegrationMethod&, FunctionPart, real_t, const IntegrationMethod&, FunctionPart, real_t,
                       const IntegrationMethod&, FunctionPart =_allFunction, real_t = theRealMax);//!< constructor from 3 integration methods
    IntegrationMethods(const IntegrationMethod&, const IntegrationMethod&);                       //!< short constructor
    IntegrationMethods(const IntegrationMethod&, real_t, const IntegrationMethod&, real_t, const IntegrationMethod&); //!< short constructor
    IntegrationMethods(QuadRule, number_t);
    IntegrationMethods(const IntegrationMethod&, QuadRule, number_t);
    IntegrationMethods(const IntegrationMethod&, real_t, QuadRule, number_t, real_t, QuadRule, number_t);
    IntegrationMethods(const IntegrationMethod&, real_t, QuadRule, number_t, real_t, QuadRule, number_t, real_t, QuadRule, number_t);
    IntegrationMethods(IntegrationMethodType imt);
    IntegrationMethods(QuadRule, number_t, real_t, QuadRule, number_t);
    IntegrationMethods(QuadRule, number_t, real_t, QuadRule, number_t, real_t, QuadRule, number_t);
    IntegrationMethods(IntegrationMethodType imt, QuadRule, number_t =0);
    IntegrationMethods(IntegrationMethodType imt, number_t, real_t, QuadRule, number_t=0);
    IntegrationMethods(IntegrationMethodType imt, number_t, real_t, QuadRule, number_t, real_t, QuadRule, number_t=0);
    IntegrationMethods(IntegrationMethodType imt, number_t, real_t, QuadRule, number_t, real_t, QuadRule, number_t, real_t, QuadRule, number_t=0);
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p, IntegrationMethodType& imt, IntegrationMethod*& meth, FunctionPart& fp, QuadRule& qr, number_t& qo, real_t& bound);
    IntegrationMethods(Parameter p1)
    {
      std::vector<Parameter> ps={p1};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2)
    {
      std::vector<Parameter> ps={p1, p2};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3)
    {
      std::vector<Parameter> ps={p1, p2, p3};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9, Parameter p10)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9, Parameter p10, Parameter p11)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14,
                       Parameter p15)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14,
                       Parameter p15, Parameter p16)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14,
                       Parameter p15, Parameter p16, Parameter p17)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14,
                       Parameter p15, Parameter p16, Parameter p17, Parameter p18)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14,
                       Parameter p15, Parameter p16, Parameter p17, Parameter p18, Parameter p19)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14,
                       Parameter p15, Parameter p16, Parameter p17, Parameter p18, Parameter p19, Parameter p20)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20};
      build(ps);
    }
    IntegrationMethods(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7,
                       Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14,
                       Parameter p15, Parameter p16, Parameter p17, Parameter p18, Parameter p19, Parameter p20, Parameter p21)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21};
      build(ps);
    }

    void add(const IntegrationMethod&, FunctionPart =_allFunction, real_t =0); //!< add IntegrationMethod to the collection

    const IntgMeth& operator[](number_t i) const {return intgMethods[i];}
    std::vector<IntgMeth>::const_iterator begin() const {return intgMethods.begin();}
    std::vector<IntgMeth>::const_iterator end() const {return intgMethods.end();}
    std::vector<IntgMeth>::iterator begin() {return intgMethods.begin();}
    std::vector<IntgMeth>::iterator end() {return intgMethods.end();}
    void push_back(const IntgMeth& im) {intgMethods.push_back(im);}
    bool empty() const {return intgMethods.empty();}
    number_t size() const {return intgMethods.size();}

    void print(std::ostream&) const;
    void print(PrintStream& out) const {print(out.currentStream());}
    friend std::ostream& operator<<(std::ostream& out, const IntegrationMethods& ims)
    {ims.print(out); return out;}
};


} // end of namespace xlifepp

#endif // INTEGRATION_METHOD_HPP
