/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Quadrature.cpp
  \authors D. Martin, E. Lunéville
  \since 25 nov 2008
  \date 7 fev 2013

  \brief Implementation of xlifepp::Quadrature class members and related functions
 */

#include "Quadrature.hpp"
#include "utils.h"

namespace xlifepp
{
/*--------------------------------------------------------------------------------
 constructors and destructor
--------------------------------------------------------------------------------*/

//default constructor
Quadrature::Quadrature()
  : geomRefElt_p(nullptr), rule(_defaultRule), degree(0), hasPointsOnBoundary(false), name("") {}

//explicit constructor
Quadrature::Quadrature(ShapeType shape, QuadRule r, number_t deg, const string_t& nam, bool pob)
  : geomRefElt_p(findGeomRefElement(shape)), rule(r), degree(deg), hasPointsOnBoundary(pob), name(nam)
{
  theQuadratures.push_back(this); // add new Quadrature pointer to vector of Quadrature*
}

// destructor, remove pointer of current object from vector of Quadrature*
Quadrature::~Quadrature()
{
  std::vector<Quadrature*>::iterator it(find(theQuadratures.begin(), theQuadratures.end(), this));
  if(it != theQuadratures.end()) { theQuadratures.erase(it); }
}

//delete all quadrature objects
void Quadrature::clearGlobalVector()
{
  while(Quadrature::theQuadratures.size() > 0) { delete Quadrature::theQuadratures[0]; }
}

// print the list of quadratures in memory
void Quadrature::printAllQuadratures(std::ostream& out)
{
    number_t vb=theVerboseLevel;
    verboseLevel(1);
    out<<"Quadratures in memory: "<<eol;
    std::vector<Quadrature*>::iterator it=theQuadratures.begin();
    for(; it!=theQuadratures.end(); ++it) out<<"  "<<(*it)<<": "<<(**it)<<eol;
    verboseLevel(vb);
}

//clone of quadrature, not included in the list of quadratures
Quadrature* Quadrature::clone() const
{
  Quadrature* q = new Quadrature();
  q->geomRefElt_p = geomRefElt_p;
  q->rule = rule;
  q->degree = degree;
  q->hasPointsOnBoundary = hasPointsOnBoundary;
  q->name = name;
  return q;
}

/*--------------------------------------------------------------------------------
 Shape free rules
--------------------------------------------------------------------------------*/
void Quadrature::centroidRule()
{
  name += " Centroid degree 1 rule";
  degree = 1;
  hasPointsOnBoundary = false;
  quadratureRule.resize(geomRefElt_p->dim(), 1);
  quadratureRule.coords(geomRefElt_p->centroid());
  quadratureRule.weights(geomRefElt_p->measure());
}

void Quadrature::vertexRule()
{
  name += " degree 1 rule";
  hasPointsOnBoundary = true;
  number_t nb_points = geomRefElt_p->nbVertices();
  quadratureRule.resize(geomRefElt_p->dim(), nb_points);
  quadratureRule.coords(geomRefElt_p->vertices());
  quadratureRule.weights(geomRefElt_p->measure() / nb_points);
}

/*--------------------------------------------------------------------------------
 Error/warning handlers
--------------------------------------------------------------------------------*/
void alternateRule(QuadRule rule, ShapeType shape, const string_t& altRule)
{
  warning("quadrature_alternaterule", words("shape", shape), words("quadrule", rule), altRule);
}
void badNodeRule(int n_nodes, const string_t & name, ShapeType sh)
{
  error("quadrature_nonode", n_nodes , name, words("shape", sh));
}
void badDegreeRule(int degree, const string_t & name, ShapeType sh)
{
  error("quadrature_nodeg", degree , name, words("shape", sh));
}
void noEvenDegreeRule(int degree, const string_t & name, ShapeType sh)
{
  // warning("quadrature_noevendeg", degree, name, words("shape", sh), degree + 1);
}
void upperDegreeRule(int degree, const string_t & name, ShapeType sh)
{
  //warning("quadrature_nosmalldeg", degree, name, words("shape", sh), degree + 1);
}
void maxDegreeRule(int degree, const string_t & name, ShapeType sh, int maxdegree)
{
  //warning("quadrature_maxdeg", degree, name, words("shape", sh), maxdegree);
}

/*--------------------------------------------------------------------------------
 print Quadrature rule functions
--------------------------------------------------------------------------------*/

std::ostream& operator<<(std::ostream& os, const Quadrature& obj)
{
  os.setf(std::ios::scientific);
  os << "  Quadrature formula over a " << obj.geomRefElt_p->shape()
     << ", degree " << obj.degree << ", " << obj.quadratureRule.size() << " points: " << obj.name << ".";
  if(theVerboseLevel<2) return os;
  os << obj.quadratureRule;
  os.unsetf(std::ios::scientific);
  return os;
}

/*--------------------------------------------------------------------------------
 pointQuadrature creates a pseudo-quadrature on a point and returns its pointer
 degree is ignored, centroidRule frame is used.
--------------------------------------------------------------------------------*/
Quadrature* pointQuadrature(QuadRule rule, number_t deg) {
  Quadrature* q = new Quadrature(_point,rule,deg,"default",false);
  q->centroidRule();
  return q;
}

/*--------------------------------------------------------------------------------
 segmentQuadrature creates a quadrature on a segment and returns its pointer
 deg is the degree of the quadrature formulae for Gauss quadrature
     and the order of element (Pk) in case of nodal quadrature
--------------------------------------------------------------------------------*/
Quadrature* segmentQuadrature(QuadRule rule, number_t deg)
{
  trace_p->push("segmentQuadrature");
  Quadrature* q = nullptr;
  switch(rule)
  {
    case _defaultRule:
    case _GaussLegendreRule:
      if(deg%2 == 0)
      {
         noEvenDegreeRule(deg,"Gauss-Legendre",_segment);
         deg+=1;
         q=findQuadrature(_segment,_GaussLegendreRule, deg, false);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_segment, _GaussLegendreRule, deg, "GaussLegendre_"+tostring(deg), false);
         q->quadratureRule.gaussLegendreRules((q->degree + 1) / 2);
      }
      break;
    case _evenGaussLegendreRule:
      if(deg%2 == 1)
      {
         deg+=1;
         q=findQuadrature(_segment,_GaussLegendreRule, deg, false);
      }
      if(q==nullptr)
      {
        q = new Quadrature(_segment, _GaussLegendreRule, deg, "GaussLegendre_"+tostring(deg), false);
        q->quadratureRule.gaussLegendreRules((q->degree + 3) / 2);
      }
      break;
    case _GaussLobattoRule:
      if(deg%2 == 0)
      {
         noEvenDegreeRule(deg,"Gauss-Lobatto",_segment);
         deg+=1;
         q=findQuadrature(_segment,_GaussLegendreRule, deg, true);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_segment, _GaussLobattoRule, deg, "GaussLobatto_"+tostring(deg), true);
         q->quadratureRule.gaussLobattoRules((q->degree + 3) / 2);
      }
      break;
    case _evenGaussLobattoRule:
      if(deg%2 == 1)
      {
         deg+=1;
         q=findQuadrature(_segment,_GaussLegendreRule, deg, true);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_segment, _GaussLobattoRule, deg, "GaussLobatto_"+tostring(deg), true);
         q->quadratureRule.gaussLobattoRules((q->degree + 5) / 2);
      }
      break;
    case _nodalRule:
      if(deg>0 && deg<5) q = new Quadrature(_segment, _nodalRule, deg, "Nodal", true);
      switch(deg)
      {
        case 1: // trapezoidal rule (P_1 nodes)
          q->vertexRule();
          break;
        case 2: // Simpson's rule  (P_2 nodes)
          q->name += " P_2 (Simpson)";
          q->degree = 3;
          q->quadratureRule.simpsonRule();
          break;
        case 3: // Simpson's 3/8 rule  (P_3 nodes)
          q->name += " P_3 (Simpson 3/8)";
          q->degree = 3;
          q->quadratureRule.simpson38Rule();
          break;
        case 4: // Boole's rule  (P_4 nodes)
          q->name += " P_4 (Boole)";
          q->degree = 5;
          q->quadratureRule.booleRule();
          break;
        default:
          badNodeRule(deg + 1,"nodal",_segment);
          break;
      }
      break;
    default:
      alternateRule(rule, _segment, "Gauss-Legendre");
      q=findQuadrature(_segment, _GaussLegendreRule, deg, false);
      if(q==nullptr)
      {
         if(deg%2 == 0) deg+=1;
         q = new Quadrature(_segment, _GaussLegendreRule, deg, "GaussLegendre_"+tostring(deg), false);
         q->quadratureRule.gaussLegendreRules((q->degree + 1) / 2);
      }
      break;
  }
  trace_p->pop();
  return q;
}

/*--------------------------------------------------------------------------------
 triangleQuadrature creates a quadrature on a triangle and returns its pointer
--------------------------------------------------------------------------------*/
Quadrature* triangleQuadrature(QuadRule rule, number_t numb)
{
  trace_p->push("triangleQuadrature");
  Quadrature* q = nullptr;
  QuadRule appliedRule = rule;
  if(rule == _defaultRule)
  {
    if(numb <= 3) { appliedRule = _nodalRule; }
    else if(numb <= 5) { appliedRule = _miscRule; }
    else { appliedRule = _GaussLegendreRule; }
  }

  switch(appliedRule)
  {
    case _nodalRule:
      if(numb>0 && numb<4) q = new Quadrature(_triangle, _nodalRule, numb, "Nodal", true);
      switch(numb)
      {
        case 1:  // Degree 1 - 1 points trapezoidal rule (P_1 nodes)
          q->vertexRule();
          break;
        case 2:  // Degree 2 - 3 points rule, P_2 mi-edge nodes
          q->name += " P_2 mi-edge nodes";
          q->quadratureRule.t2P2MidEdgeRule();
          break;
        case 3:  // Degree 3 - 7 points rule, with P_2 nodes & centroid,
          q->name += " P_3 Stroud (P2 nodes and centroid)";
          q->quadratureRule.t2P3StroudRule();
          break;
        default:
          badNodeRule(((numb + 1) * (numb + 2)) / 2,"nodal",_triangle);
          break;
      }
      break;

    case _GaussLegendreRule:
    {
      if(numb % 2 == 0) // no even degree rule
      {
         noEvenDegreeRule(numb,"Gauss-Legendre",_triangle);
         numb+=1;  // odd degree
         q=findQuadrature(_triangle, _GaussLegendreRule, numb, false);
      }
      if(q==nullptr)
      {
        q = new Quadrature(_triangle, _GaussLegendreRule, numb, "Gauss-Legendre"+tostring(numb), false);
        QuadratureRule quadrule1, quadrule2; //Conical product of rules on [0,1]
        quadrule1.gaussLegendreRules((q->degree + 3) / 2);
        quadrule2.gaussLegendreRules((q->degree + 1) / 2);
        q->quadratureRule.conicalRule(quadrule1, quadrule2);
      }
      break;
    }

    case _GrundmannMollerRule:
      if(numb % 2 == 0) // no even degree rule
      {
         noEvenDegreeRule(numb,"Grundmann-Moller",_triangle);
         numb+=1;  // odd degree
         q=findQuadrature(_triangle, _GrundmannMollerRule, numb, false);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_triangle, _GrundmannMollerRule, numb, "Grundmann-Moller"+tostring(numb), false);
         q->quadratureRule.tNGrundmannMollerRule((q->degree - 1) / 2, 2);
      }
      break;

    case _symmetricalGaussRule: // Symmetrical Gauss rule,  order up to 20 except 3
    {
      if(numb==3)
      {
         upperDegreeRule(numb,"symmetrical Gauss",_tetrahedron);
         numb=4;
         q=findQuadrature(_tetrahedron, _symmetricalGaussRule,numb);
      }
      if(numb>20)
      {
         maxDegreeRule(numb,"symmetrical Gauss",_tetrahedron,20);
         numb=20;
         q=findQuadrature(_tetrahedron, _symmetricalGaussRule, numb);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_triangle, _symmetricalGaussRule, numb, "SymmetricalGauss_"+tostring(numb), false);
         q->quadratureRule.symmetricalGaussTriangleRule(q->degree);
      }
      break;
    }

    case _miscRule:
    default:
      if(appliedRule != _miscRule) { alternateRule(rule, _triangle, "Misc"); }
      if(numb>0 && numb<7) q = new Quadrature(_triangle, _miscRule, numb, "Misc", false);
      switch(numb)
      {
        case 1:  // Degree 1 - 1 point centroid rule
          q->centroidRule();
          break;
        case 2:  // Degree 2 - 3 point Hammer_Stroud rule
          q->name += " P_2 Hammer Stroud";
          q->quadratureRule.t2P2HammerStroudRule();
          q->hasPointsOnBoundary = true;
          break;
        case 3:  // Degree 3 -- 6 point rule with P_2 mid-edge nodes and interior points
          q->name += " P_3 Albrecht-Collatz";
          q->quadratureRule.t2P3AlbrechtCollatzRule();
          q->hasPointsOnBoundary = true;
          break;
        case 4:
          upperDegreeRule(q->degree,"Misc",_triangle);
          q->degree += 1;
          // *NO BREAK HERE* let it fall through to next case
        case 5:  // Degree 5 -- 7 points Radon-Hammmer-Marlowe-Stroud rule
          q->name += " P_5 Radon-Hammmer-Marlowe-Stroud";
          q->quadratureRule.t2P5RadonHammerMarloweStroudRule();
          q->hasPointsOnBoundary = false;
          break;
        case 6:  // Degree 6 -- Hammer 12 internal point rule
          q->name += " P_6 Hammer";
          q->quadratureRule.t2P6HammerRule();
          q->hasPointsOnBoundary = false;
          break;
        default:
          badDegreeRule(numb,"miscellaneous rule",_triangle);
          break;
      }
  }
  trace_p->pop();
  return q;
}

/*--------------------------------------------------------------------------------
 quadrangleQuadrature creates a quadrature on a quadrangle and returns its pointer
--------------------------------------------------------------------------------*/
Quadrature* quadrangleQuadrature(QuadRule rule, number_t numb)
{
  trace_p->push("quadrangleQuadrature");
  Quadrature* q = nullptr;
  switch(rule)
  {
    case _GaussLobattoRule:
    {
      if(numb % 2 == 0) // no even degree rule
      {
         noEvenDegreeRule(numb,"Gauss-Lobatto",_triangle);
         numb+=1;  // odd degree
         q=findQuadrature(_triangle, _GaussLobattoRule, numb, true);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_quadrangle, _GaussLobattoRule, numb, "GaussLobatto_"+tostring(numb), true);
         QuadratureRule quadrule1;
         quadrule1.gaussLobattoRules((q->degree + 3) / 2);
         q->quadratureRule.quadrangleNodalRule(quadrule1);
      }
      break;
    }

    case _nodalRule: // tensor product Newton-Cotes closed type rules using Q_k element nodes
    {
      if(numb>0 && numb<5) q = new Quadrature(_quadrangle, _nodalRule, numb, "Nodal", true);
      QuadratureRule quadrule1;
      switch(numb)
      {
        case 1: // tensor product trapezoidal rule (Q_1 nodes)
          q->name += " Q_1 tensor";
          q->degree = 1;
          quadrule1.trapezoidalRule();
          break;
        case 2: // tensor product Simpson's rule  (Q_2 nodes)
          q->name += " Q_2 (Simpson)";
          q->degree = 3;
          quadrule1.simpsonRule();
          break;
        case 3: // tensor product Simpson's 3/8 rule  (Q_3 nodes)
          q->name += " Q_3 (Simpson 3/8)";
          q->degree = 3;
          quadrule1.simpson38Rule();
        case 4: // tensor product Boole's rule  (Q_4 nodes)
          q->name += " Q_4 (Boole)";
          q->degree = 5;
          quadrule1.booleRule();
          break;
        default:
          badNodeRule((numb + 1) * (numb + 1),"Nodal",_quadrangle);
          break;
      }
      q->quadratureRule.quadrangleNodalRule(quadrule1);
      break;
    }

    case _symmetricalGaussRule: // Symmetrical Gauss rule, odd order up to 21
    {
      if(numb % 2 == 0) // no even degree rule
      {
         noEvenDegreeRule(numb,"Symmetrical Gauss",_quadrangle);
         numb+=1;  // odd degree
         q=findQuadrature(_quadrangle, _symmetricalGaussRule, numb, true);
      }
      if(numb>21)
      {
         maxDegreeRule(numb,"Symmetrical Gauss",_quadrangle,21);
         numb=21;
         q=findQuadrature(_quadrangle, _symmetricalGaussRule, numb, true);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_quadrangle, _symmetricalGaussRule, numb, "SymmetricalGauss_"+tostring(numb), false);
         q->quadratureRule.symmetricalGaussQuadrangleRule(q->degree);
      }
      break;
    }

    case _defaultRule:
    case _GaussLegendreRule:
    default:
    {
      if(rule != _defaultRule && rule != _GaussLegendreRule) { alternateRule(rule, _quadrangle, "Gauss-Legendre"); }
      if(numb%2==0) numb+=1;
      q = new Quadrature(_quadrangle, _GaussLegendreRule, numb, "GaussLegendre_"+tostring(numb), false);
      QuadratureRule quadrule1;
      quadrule1.gaussLegendreRules((q->degree + 1) / 2);
      q->quadratureRule.tensorRule(quadrule1, quadrule1);
      break;
    }
  }
  trace_p->pop();
  return q;
}

/*--------------------------------------------------------------------------------
 tetrahedronQuadrature creates a quadrature on a tetrahedron and returns its pointer
--------------------------------------------------------------------------------*/
Quadrature* tetrahedronQuadrature(QuadRule rule, number_t numb)
{
  trace_p->push("tetrahedronQuadrature");
  Quadrature* q = nullptr;
  QuadRule appliedRule = rule;
  if(rule == _defaultRule)
  {
    if(numb <= 3) { appliedRule = _nodalRule; }
    else if(numb <= 5) { appliedRule = _miscRule; }
    else { appliedRule = _GaussLegendreRule; }
  }

  switch(appliedRule)
  {
    case _nodalRule:
      {
        switch(numb)
        {
         case 1: // (P_1 nodes) Degree 1 Lauffer, P_1 unisolvent
         q = new Quadrature(_tetrahedron, _nodalRule, numb, "Nodal", true);
         q->vertexRule();
         break;
         case 3: // (including P_1 nodes & centroid of faces) Degree 3 -- 8 points rule
         q = new Quadrature(_tetrahedron, _nodalRule, numb, "Nodal", true);
         q->name += " P_3 Stroud";
         q->quadratureRule.t3P3StroudRule();
         break;
        default:
         badNodeRule(((numb + 1) * (numb + 2) * (numb + 3)) / 6, "nodal rule",_tetrahedron);
         break;
        }
      }
      break;

    case _GaussLegendreRule: //conical product of 3 complete rules on [0,1]  (A.H. Stroud pp 18--31)
    {
      if(numb % 2 == 0) // no even degree rule
      {
         noEvenDegreeRule(numb,"Gauss-Legendre",_tetrahedron);
         numb+=1;  // odd degree
         q=findQuadrature(_tetrahedron, _GaussLegendreRule, numb, false);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_tetrahedron, _GaussLegendreRule, numb, "GaussLegendre_"+tostring(numb), false);
         QuadratureRule quadrule1, quadrule2, quadrule3;
         quadrule1.gaussLegendreRules((q->degree + 5) / 2);
         quadrule2.gaussLegendreRules((q->degree + 3) / 2);
         quadrule3.gaussLegendreRules((q->degree + 1) / 2);
         q->quadratureRule.conicalRule(quadrule1, quadrule2, quadrule3);
      }
    }
    break;

    case _GrundmannMollerRule: //(A. Grundmann & H.M. Moller, SIAM Journal on Numerical Analysis, Vol 15, No 2, Apr. 1978, pages 282-290
    {
      if(numb % 2 == 0) // no even degree rule
      {
         noEvenDegreeRule(numb,"Grundmann-Moller",_tetrahedron);
         numb+=1;  // odd degree
         q=findQuadrature(_tetrahedron, _GrundmannMollerRule, numb, false);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_tetrahedron, _GrundmannMollerRule, numb, "GrundmannMoller_"+tostring(numb), false);
         q->quadratureRule.tNGrundmannMollerRule((q->degree - 1) / 2, 3);
      }
      break;
    }

    case _symmetricalGaussRule: // Symmetrical Gauss rule, order up to 10 except 4
    {
      if(numb==4)
      {
        upperDegreeRule(numb,"symmetrical Gauss",_tetrahedron);
        numb=5;
        q=findQuadrature(_tetrahedron, _symmetricalGaussRule,numb);
      }
      if(numb>10)
      {
        maxDegreeRule(numb,"symmetrical Gauss",_tetrahedron,10);
        numb=10;
        q=findQuadrature(_tetrahedron, _symmetricalGaussRule, numb);
      }
      if(q==nullptr)
      {
        q = new Quadrature(_tetrahedron, _symmetricalGaussRule, numb, "SymmetricalGauss_"+tostring(numb), false);
        q->quadratureRule.symmetricalGaussTetrahedronRule(q->degree);
      }
      break;
    }

    case _miscRule:
    default:
      if(appliedRule != _miscRule) { alternateRule(rule, _tetrahedron, "miscellaneous rule"); }
      if(numb>0 || numb<6) q = new Quadrature(_tetrahedron, _miscRule, numb, "Misc", false);
      switch(numb)
      {
        case 1: // Degree 1 centroid rule, P_0 unisolvent
          q->centroidRule();
          break;
        case 2: // Degree 2 -- 4 point Hammer-Stroud
          q->name += " P_2 Hammer-Stroud";
          q->quadratureRule.t3P2HammerStroudRule();
          break;
        case 3: // Degree 3 -- 8 point rule, including P_1 nodes & centroid of faces
          q->name += " P_3 Stroud";
          q->quadratureRule.t3P3StroudRule();
          q->hasPointsOnBoundary = true;
          break;
        case 4:
          upperDegreeRule(q->degree,"Misc",_tetrahedron);
          q->degree += 1;
          // *NO BREAK HERE* let it fall through to next case
        case 5: // Degree 5 Stroud 15 points formula
          q->name += " P_5 Stroud";
          q->quadratureRule.t3P5StroudRule();
          break;
        default:
          badDegreeRule(numb,"miscellaneous rule",_tetrahedron);
          break;
      }
      break;
  }
  trace_p->pop();
  return q;
}

/*--------------------------------------------------------------------------------
 hexahedronQuadrature creates a quadrature on a hexahedron and returns its pointer
--------------------------------------------------------------------------------*/
Quadrature* hexahedronQuadrature(QuadRule rule, number_t numb)
{
  trace_p->push("hexahedronQuadrature");
  Quadrature* q = nullptr;
  switch(rule)
  {
    case _GaussLobattoRule:
    {
      if(numb % 2 == 0) // no even degree rule
      {
         noEvenDegreeRule(numb,"Gauss-Lobatto",_hexahedron);
         numb+=1;  // odd degree
         q=findQuadrature(_hexahedron, _GaussLobattoRule, numb, true);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_hexahedron, _GaussLobattoRule, numb, "Gauss-Lobatto"+tostring(numb), true);
         QuadratureRule quadrule1;
         quadrule1.gaussLobattoRules((q->degree + 3) / 2);
         q->quadratureRule.hexahedronNodalRule(quadrule1);
      }
      break;
    }

    case _nodalRule: // tensor product Newton-Cotes closed type rules using Q_k element nodes
    {
      if(numb>0 && numb<5) q = new Quadrature(_hexahedron, _nodalRule, numb, "Nodal", true);
      QuadratureRule quadrule1;
      switch(numb)
      {
        case 1: // tensor product trapezoidal rule (Q_1 nodes)
          q->name += " Q_1 tensor";
          q->degree = 1;
          quadrule1.trapezoidalRule();
          break;
        case 2: // tensor product Simpson's rule  (Q_2 nodes)
          q->name += " Q_2 (Simpson)";
          q->degree = 3;
          quadrule1.simpsonRule();
          break;
        case 3: // tensor product Simpson's 3/8 rule  (Q_3 nodes)
          q->name += " Q_3 (Simpson 3/8)";
          q->degree = 3;
          quadrule1.simpson38Rule();
        case 4: // tensor product Boole's rule  (Q_4 nodes)
          q->name += " Q_4 (Boole)";
          q->degree = 5;
          quadrule1.booleRule();
          break;
        default:
          badNodeRule((numb + 1) * (numb + 1),"Nodal",_hexahedron);
          break;
      }
      q->quadratureRule.hexahedronNodalRule(quadrule1);
      break;
    }

    case _symmetricalGaussRule: // Symmetrical Gauss rule odd order up to 11
    {
      if(numb % 2 == 0) // no even degree rule
      {
         noEvenDegreeRule(numb,"symmetrical Gauss",_hexahedron);
         numb+=1;  // odd degree
         q=findQuadrature(_hexahedron, _symmetricalGaussRule, numb, true);
      }
      if(q==nullptr)
      {
          q = new Quadrature(_hexahedron, _symmetricalGaussRule, numb, "Symmetrical Gauss"+tostring(numb), false);
          q->quadratureRule.symmetricalGaussHexahedronRule(q->degree);
      }
      break;
    }

    case _defaultRule:
    case _GaussLegendreRule:
    default:
    {
      if(rule != _defaultRule && rule != _GaussLegendreRule) { alternateRule(rule, _hexahedron, "Gauss-Legendre"); }
      if(numb % 2 == 0) // no even degree rule
      {
         noEvenDegreeRule(numb,"Gauss-Legendre",_hexahedron);
         numb+=1;  // odd degree
         q=findQuadrature(_hexahedron, _GaussLegendreRule, numb, false);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_hexahedron, _GaussLegendreRule, numb, "Gauss-Legendre"+tostring(numb), false);
         QuadratureRule quadrule1;
         quadrule1.gaussLegendreRules((q->degree + 1) / 2);
         q->quadratureRule.tensorRule(quadrule1, quadrule1, quadrule1);
      }
      break;
    }
  }
  trace_p->pop();
  return q;
}

/*--------------------------------------------------------------------------------
 prismQuadrature creates a quadrature on a prism and returns its pointer
--------------------------------------------------------------------------------*/
Quadrature* prismQuadrature(QuadRule rule, number_t numb)
{
  trace_p->push("prismQuadrature");
  Quadrature* q = nullptr;
  QuadRule appliedRule = rule;
  if(rule == _defaultRule)
  {
    if(numb <= 1) { appliedRule = _nodalRule; }
    else { appliedRule = _miscRule; }
  }

  switch(appliedRule)
  {
    case _nodalRule:
      switch(numb)
      {
        case 1: // P_1 nodes formula
          q = new Quadrature(_prism, _nodalRule, numb, "Nodal", true);
          q->vertexRule();
          break;
        default:
          badNodeRule((numb + 1) * (((numb + 1) * (numb + 2)) / 2),"Nodal",_prism);
          break;
      }
      break;

      case _symmetricalGaussRule: // Symmetrical Gauss rule up to 10
      {
        if(numb>10)
        {
           maxDegreeRule(numb,"symmetrical Gauss",_prism,10);
           numb=10;
           q=findQuadrature(_prism, _symmetricalGaussRule, numb, false);
        }
        if(q==nullptr)
        {
           q = new Quadrature(_prism, _symmetricalGaussRule, numb, "Symmetrical Gauss"+tostring(numb), false);
           q->quadratureRule.symmetricalGaussPrismRule(q->degree);
        }
      }
      break;

    case _miscRule:
    default:
    {
      if(appliedRule != _miscRule) { alternateRule(rule, _tetrahedron, "Misc"); }
      if(numb>0 && numb<6) q=new Quadrature(_prism, _miscRule, numb, "Misc", true);
      QuadratureRule quadrule1, quadrule2;
      switch(q->degree)
      {
        case 1:
          q->centroidRule();
          q->name += " P_1";
          break;
        case 2:
          upperDegreeRule(numb,"Misc",_prism);
          q->degree += 1;
          // *NO BREAK HERE* let it fall through to next case
        case 3: // tensor product rule of 2-points Gauss-Legendre rule on [0,1] and Stroud 7 points formula on the unit triangle
          q->name += " P_3";
          quadrule1.gaussLegendreRules((q->degree + 1) / 2);
          quadrule2.t2P3StroudRule();
          q->quadratureRule.tensorRule(quadrule2, quadrule1);
          break;
        case 4:
          upperDegreeRule(numb,"Misc",_prism);
          q->degree += 1;
          // *NO BREAK HERE* let it fall through to next case
        case 5: // tensor product of 3-points Gauss-Legendre rule on [0,1] and Radon-Hammmer-Marlowe-Stroud 7 points formula on the unit triangle
          q->name += " P_5";
          quadrule1.gaussLegendreRules((q->degree + 1) / 2);
          quadrule2.t2P5RadonHammerMarloweStroudRule();
          q->quadratureRule.tensorRule(quadrule2, quadrule1);
          break;
        default:
          badDegreeRule(numb,"Misc",_prism);
          break;
      }
      break;
    }
  }
  trace_p->pop();
  return q;
}

/*--------------------------------------------------------------------------------
 pyramidQuadrature creates a quadrature on a prism and returns its pointer
--------------------------------------------------------------------------------*/
Quadrature* pyramidQuadrature(QuadRule rule, number_t numb)
{
  trace_p->push("pyramidQuadrature");
  Quadrature* q = nullptr;
  QuadRule appliedRule = rule;
  if(rule == _defaultRule)
  {
    if(numb <= 3) { appliedRule = _nodalRule; }
    else if(numb <= 5) { appliedRule = _miscRule; }
    else { appliedRule = _GaussLegendreRule; }
  }

  switch(appliedRule)
  {
    case _nodalRule:
      switch(numb)
      {
        case 1: // (P_1 nodes) Degree 1 Lauffer, P_1 unisolvent
          q = new Quadrature(_pyramid, _nodalRule, numb, "Nodal", true);
          q->vertexRule();
          break;
        default:
          badNodeRule(((numb + 1) * (numb + 2) * (numb + 3)) / 6,"Nodal",_pyramid);
          break;
      }
      break;

    case _GaussLegendreRule: // conical product of 3 complete rules on [0,1]  (A.H. Stroud pp 18--31)
    {
      if(numb % 2 == 0) // no even degree rule
      {
         noEvenDegreeRule(numb,"Gauss-Legendre",_pyramid);
         numb+=1;  // odd degree
         q=findQuadrature(_pyramid, _GaussLegendreRule, numb, false);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_pyramid, _GaussLegendreRule, numb, "Gauss-Legendre"+tostring(numb), false);
         QuadratureRule qrGL, qrGJ2;
         qrGL.gaussLegendreRules((q->degree + 1) / 2);
         qrGJ2.gaussJacobiRules((q->degree + 1) / 2);
         q->quadratureRule.pyramidRule(qrGL,qrGJ2);
      }
      break;
    }

    case _symmetricalGaussRule: // Symmetrical Gauss rule up to 10
    {
      if(numb>10)
      {
         maxDegreeRule(numb,"symmetrical Gauss",_pyramid,10);
         numb=10;
         q=findQuadrature(_pyramid, _symmetricalGaussRule, numb, false);
      }
      if(q==nullptr)
      {
         q = new Quadrature(_pyramid, _symmetricalGaussRule, numb, "Symmetrical Gauss"+tostring(numb), false);
         q->quadratureRule.symmetricalGaussPyramidRule(q->degree);
      }
      break;
    }
    case _miscRule:
    default:
      if(appliedRule != _miscRule) { alternateRule(rule, _pyramid, "Misc"); }
      switch(numb)
      {
        case 1: // Degree 1 centroid rule, P_0 unisolvent
          q = new Quadrature(_pyramid, _miscRule, numb, "Misc", false);
          q->centroidRule();
          break;
        case 7: // Degree 7 -- 48 point Stroud
          q = new Quadrature(_pyramid, _miscRule, numb, "Misc", false);
          q->name += " Stroud";
          q->quadratureRule.pyramidStroudRule();
          break;
        default:
          badDegreeRule(numb,"Misc",_pyramid);
          break;
      }
      break;
  }
  trace_p->pop();
  return q;
}

/*
--------------------------------------------------------------------------------
    QuadratureFind
    definition of a Quadrature rule by shape number, rule and degree
    use existing Quadrature object if one already exists
    otherwise create a new one, in both cases returns pointer to the object
--------------------------------------------------------------------------------
*/
Quadrature* findQuadrature(ShapeType shape, QuadRule rule, number_t degree, bool hpob)
{
  // find quadrature rule in vector theQuadratures of class Quadrature
  for(std::vector<Quadrature*>::iterator q = Quadrature::theQuadratures.begin();
      q != Quadrature::theQuadratures.end(); q++)
  {
    if(shape == (*q)->geomRefElt_p->shapeType() &&
        rule == (*q)->rule && degree == (*q)->degree) { return *q; }
  }

  // this is a new Quadrature object (create new quadrature rule according to shape)
  trace_p->push("Quadrature::findQuadrature");
  Quadrature* q = nullptr;
  switch(shape) {
    case _point:       q = pointQuadrature(rule, degree);        break;
    case _segment:     q = segmentQuadrature(rule, degree);      break;
    case _triangle:    q = triangleQuadrature(rule, degree);     break;
    case _quadrangle:  q = quadrangleQuadrature(rule, degree);   break;
    case _tetrahedron: q = tetrahedronQuadrature(rule, degree);  break;
    case _prism:       q = prismQuadrature(rule, degree);        break;
    case _pyramid:     q = pyramidQuadrature(rule, degree);      break;
    case _hexahedron:  q = hexahedronQuadrature(rule, degree);   break;
    default:           error("quadrature_noshapetype", shape);   break;
  }

  trace_p->pop();
  return q;
}

// best QuadRule to be used to integrate polynom of given degree on a given shape
// best means the minimum of quadrature points
QuadRule Quadrature::bestQuadRule(ShapeType sh, number_t deg)
{
  switch(sh) {
    case _point: return _defaultRule;

    case _segment: return _GaussLegendreRule;

    case _quadrangle:
      if(deg < 3 || deg>21) return _GaussLegendreRule;
      return _symmetricalGaussRule;

    case _hexahedron:
      if(deg < 2 || deg>11)  return _GaussLegendreRule;
      return _symmetricalGaussRule;

    case _triangle:
      if(deg < 3)   return _miscRule;
      if(deg == 3)  return _GrundmannMollerRule;
      if(deg > 3 && deg < 21)  return _symmetricalGaussRule;
      return _GaussLegendreRule;

    case _tetrahedron:
      if(deg < 3)  return _miscRule;
      if(deg == 3) return _GrundmannMollerRule;
      if(deg > 3 && deg < 11) return _symmetricalGaussRule;
      return _GrundmannMollerRule;

    case _prism:
      if(deg < 2) return _miscRule;
      if(deg <11) return _symmetricalGaussRule;
      error("quadrature_nodeg", deg, "",words("shape",sh),"Quadrature::bestQuadrature");

    case _pyramid:
      if(deg < 2) return _miscRule;
      if(deg <11) return _symmetricalGaussRule;
      error("quadrature_nodeg", deg, "",words("shape",sh),"Quadrature::bestQuadrature");

    default: error("quadrature_badshape", sh, "Quadrature::bestQuadrature");
  }
  return _GaussLegendreRule;
}

Quadrature* findBestQuadrature(ShapeType sh, number_t ord, bool hpob)
{
  QuadRule bestQR = Quadrature::bestQuadRule(sh, ord);
  return findQuadrature(sh, bestQR, ord);
}


// OLD VERSION of bestQuadRule
//QuadRule Quadrature::bestQuadRule(ShapeType sh, number_t deg)
//{
//  switch(sh)
//  {
//    case _segment:
//    case _quadrangle:
//    case _hexahedron: return _GaussLegendreRule;
//    case _triangle:
//      if(deg < 3) { return _miscRule; }
//      if(deg == 3) { return _GrundmannMollerRule; }
//      if(deg > 3 && deg < 7) { return _miscRule; }
//      if(deg > 6) { return _GaussLegendreRule; }
//    case _tetrahedron:
//      if(deg < 3) { return _miscRule; }
//      return _GrundmannMollerRule;
//    case _prism: return _miscRule;
//    default: error("quadrature_badshape", sh, "Quadrature::bestQuadrature");
//  }
//  return _GaussLegendreRule;
//}

} // end of namespace xlifepp
