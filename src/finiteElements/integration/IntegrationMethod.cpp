/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file IntegrationMethod.cpp
  \author E. Lunéville
  \since 27 sep 2013
  \date  25 aug 2016

  \brief Implementation of xlifepp::IntegrationMethod classes member functions and related utilities
*/

#include "IntegrationMethod.hpp"
#include "utils.h"

namespace xlifepp
{
//-------------------------------------------------------------------------------
// IntegrationMethod class member functions and related functions
//-------------------------------------------------------------------------------
// output IntegrationMethod on stream
std::ostream& operator<<(std::ostream& os, const IntegrationMethod& im)
{
  im.print(os);
  return os;
}

//implementation of Parameter constructors from IntegrationMethod here to avoid cross dependancy
Parameter::Parameter(const IntegrationMethod& im, const string_t& nm, const string_t& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pointerIntegrationMethod)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  IntegrationMethod* imb=const_cast<IntegrationMethod*>(&im);
  p_ = static_cast<void *>(imb);
}

Parameter::Parameter(const IntegrationMethod& im, const string_t& nm, const Strings& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pointerIntegrationMethod)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  IntegrationMethod* imb=const_cast<IntegrationMethod*>(&im);
  p_ = static_cast<void *>(imb);
}

//special function declared in Parameters.hpp
void Parameter::setToIntegrationMethod(const IntegrationMethod& im)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<const void*>(&im); // no copy because im can be modified (HMatrixIM case)
  type_=_pointerIntegrationMethod;
}

void deleteIntegrationMethod(void* p)
{
  p=nullptr;
}

void* cloneIntegrationMethod(const void* p)
{
  return const_cast<void *>(p); // no copy because im can be modified (HMatrixIM case)
}

//-------------------------------------------------------------------------------
// ProductIM class member functions and related functions
//-------------------------------------------------------------------------------
// constructors
ProductIM::ProductIM(SingleIM* imx, SingleIM* imy)
  : DoubleIM(_productIM),im_x(imx),im_y(imy)
{
  if(im_x==nullptr) error("null_pointer", "im_x");
  if(im_y==nullptr) im_y = im_x;
  name=im_x->name+" x "+im_y->name;
  imType=_productIM;
}

ProductIM::ProductIM(const SingleIM& imx, const SingleIM& imy)
  : DoubleIM(_productIM)
{
  im_x = imx.clone();
  im_y = imy.clone();
  name=im_x->name+" x " +im_y->name;
  imType=_productIM;
}

ProductIM::ProductIM(const SingleIM& imxy)
  : DoubleIM(_productIM)
{
  im_x = imxy.clone();
  im_y = im_x;
  name=im_x->name+" x " +im_y->name;
  imType=_productIM;
}

ProductIM::ProductIM(ShapeType sh, QuadRule quad_xy, number_t ord_xy)
{
  im_x = new QuadratureIM(sh, quad_xy, ord_xy); //create single quadrature for x
  im_y = im_x;                                  //same quadrature for x and y
  name=im_x->name+" x " +im_y->name;
  imType=_productIM;
}

ProductIM::ProductIM(ShapeType sh, QuadRule quad_x, number_t ord_x, QuadRule quad_y, number_t ord_y)
{
  im_x = new QuadratureIM(quad_x, ord_x);  //create single quadrature for x
  im_y = new QuadratureIM(quad_y, ord_y);  //create single quadrature for y
  name=im_x->name+" x " +im_y->name;
  imType=_productIM;
}

ProductIM::ProductIM(QuadRule quad_xy, number_t ord_xy)
{
  im_x = new QuadratureIM(quad_xy, ord_xy); //create single quadrature for x
  im_y = new QuadratureIM(quad_xy, ord_xy); //same quadrature for x and y
  name=im_x->name+" x " +im_y->name;
  imType=_productIM;
}

ProductIM::ProductIM(QuadRule quad_x, number_t ord_x, QuadRule quad_y, number_t ord_y)
{
  im_x = new QuadratureIM(quad_x, ord_x);  //create single quadrature for x
  im_y = new QuadratureIM(quad_y, ord_y);  //create single quadrature for y
  name=im_x->name+" x " +im_y->name;
  imType=_productIM;
}

ProductIM::ProductIM(const ProductIM& pim)
{
  im_x=nullptr; im_y=nullptr;
  if (pim.im_x!=nullptr) im_x=pim.im_x->clone();
  if (pim.im_y!=nullptr) im_y=pim.im_y->clone();
  name=pim.name;
  imType=pim.imType;
}

ProductIM::~ProductIM()
{
  if (im_x!=nullptr) delete im_x;
  if (im_y!=nullptr && im_y!=im_x) delete im_y;
}

ProductIM& ProductIM::operator=(const ProductIM& pim)
{
  if (&pim==this) return *this;
  if (im_x!=nullptr && im_x!=pim.im_x) delete im_x;
  if (im_y!=nullptr && im_y!=im_x && im_y!=pim.im_y) delete im_y;
  im_x=nullptr; im_y=nullptr;
  if (pim.im_x!=nullptr) im_x=pim.im_x->clone();
  if (pim.im_y!=nullptr) im_y=pim.im_y->clone();
  name=pim.name;
  imType=pim.imType;
  return *this;
}

// print on stream
void ProductIM::print(std::ostream& os) const
{
  if (theVerboseLevel>0) os<<*im_x<<" x "<<*im_y;
}

//-------------------------------------------------------------------------------
// QuadratureIM class member function and related functions
//-------------------------------------------------------------------------------

QuadratureIM::QuadratureIM(QuadRule rule, number_t deg)
: SingleIM(_quadratureIM, "QuadratureIM")
{
  quadRule=rule;
  degree=deg;
  name = words("quadrule",rule)+"_"+tostring(deg);
  //creation of quadratures is delayed till the shapes will be known
}

// set quadrature for a shape
QuadratureIM::QuadratureIM(ShapeType sh, QuadRule rule, number_t deg)
: SingleIM(_quadratureIM, "QuadratureIM")
{
  imType=_quadratureIM;
  quadRule=rule;
  degree=deg;
  std::set<ShapeType> shapes;
  shapes.insert(sh);
  setQuadratures(shapes);
}

// set quadrature for some shapes
QuadratureIM::QuadratureIM(const std::set<ShapeType>& shapes, QuadRule rule, number_t deg)
: SingleIM(_quadratureIM, "QuadratureIM")
{
  imType=_quadratureIM;
  quadRule=rule;
  degree=deg;
  setQuadratures(shapes);
}

//set quadrature for a shape
void QuadratureIM::setQuadrature(ShapeType sh) const
{
  std::set<ShapeType> shapes;
  shapes.insert(sh);
  setQuadratures(shapes);
}

//set quadratures for a set of shape
void QuadratureIM::setQuadratures(const std::set<ShapeType>& shapes) const
{
  std::set<ShapeType>::const_iterator its;
  std::map<ShapeType, Quadrature*>::iterator itq;
  QuadRule quadr = quadRule;
  number_t deg = degree;
  for (its = shapes.begin(); its != shapes.end(); its++) //loop on shapes of elements
  {
    itq = quadratures_.find(*its);
    if (itq==quadratures_.end())   //quadrature with shape its not found, insert it
    {
      if (quadr == _defaultRule) //check compatibility of quadrature rule with shapes
      {
        if(deg==0) deg=3;  //default degree
        quadr = Quadrature::bestQuadRule(*its, deg);
      }
      Quadrature* qp = findQuadrature(*its, quadr, deg);
      if (qp==nullptr) error("quadrature_not_found");
      quadRule=quadr;  degree=deg;
      quadratures_[*its] = qp;
      shapevalues_[*its] = 0;   //uninitialized shape values
    }
  }
  //update name
  name="";
  if (quadratures_.size()>1) name="quadratures: ";
  for (itq=quadratures_.begin(); itq!=quadratures_.end(); ++itq)
    name+=" "+words("shape",itq->first)+"_"+itq->second->name;
}

//destructor, free shapevalues
QuadratureIM::~QuadratureIM()
{
  clear();
}

//deallocate shapevalues vector pointer and clear maps
void QuadratureIM::clear()
{
  std::map<ShapeType, std::vector<ShapeValues>* >::iterator it=shapevalues_.begin();
  for (; it!=shapevalues_.end(); ++it)
    if (it->second != nullptr) delete it->second;
  shapevalues_.clear();
  quadratures_.clear();
}

// return the list of quadratures in a list
std::list<Quadrature*> QuadratureIM::quadratures() const
{
  std::list<Quadrature*> quads;
  std::map<ShapeType,Quadrature*>::const_iterator it=quadratures_.begin();
  for (; it!=quadratures_.end(); it++) quads.push_back(it->second);
  return quads;
}

// return quadrature pointer related to shape
Quadrature* QuadratureIM::getQuadrature(ShapeType sh) const
{
  std::map<ShapeType,Quadrature*>::const_iterator it=quadratures_.find(sh);
  if (it!=quadratures_.end()) return it->second;
  return nullptr;
}

// set shape values for a shape
void QuadratureIM::setShapeValues(ShapeType sh, std::vector<ShapeValues>* shvs) const
{
  shapevalues_[sh]=shvs;
}

// return shape values related to shape
std::vector<ShapeValues>* QuadratureIM::getShapeValues(ShapeType sh) const
{
  if (shapevalues_.find(sh)==shapevalues_.end()) return nullptr;
  return shapevalues_[sh];
}

void QuadratureIM::print(std::ostream& os) const
{
  std::map<ShapeType, Quadrature*>::const_iterator itq;
  for (itq = quadratures_.begin(); itq != quadratures_.end(); itq++)
    os << words("shape", itq->first) << "->" << itq->second->name
       << " (nbq = " << itq->second->numberOfPoints() << ")";
}

std::ostream& operator<<(std::ostream& os, const QuadratureIM& qim)
{
  qim.print(os);
  return os;
}

//---------------------------------------------------------------------------------------
// IntgMeth member functions and related stuff
//---------------------------------------------------------------------------------------
IntgMeth::IntgMeth(const IntegrationMethod& im, FunctionPart fp, real_t b)
{
  intgMeth=im.clone();   //full copy
  functionPart=fp;
  bound=b;
}

IntgMeth::IntgMeth(const IntgMeth& im)
{
  intgMeth=nullptr;
  functionPart=im.functionPart;
  bound=im.bound;
  if (im.intgMeth!=nullptr) intgMeth=im.intgMeth->clone(); //full copy
}

IntgMeth::~IntgMeth()
{
  if (intgMeth!=nullptr) delete intgMeth;
}

IntgMeth& IntgMeth::operator=(const IntgMeth& im)
{
  if (&im==this) return *this;
  if (intgMeth!=nullptr) delete intgMeth;
  intgMeth=nullptr;
  functionPart=im.functionPart;
  bound=im.bound;
  if (im.intgMeth!=nullptr) intgMeth=im.intgMeth->clone(); //full copy
  return *this;
}


void IntgMeth::print(std::ostream& out) const
{
  string_t part="all function";
  if (functionPart==_regularPart)  part="regular part";
  else if (functionPart==_singularPart) part="singular part";
  out << " " << intgMeth->name << ", " << part << ", dist <= ";
  if (bound==theRealMax) out << "+inf";
  else out << bound;
}

//---------------------------------------------------------------------------------------
// IntegrationMethods member functions and related stuff
//---------------------------------------------------------------------------------------
//full constructors
IntegrationMethods::IntegrationMethods(const IntegrationMethod& im, FunctionPart fp, real_t b)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethod, FunctionPart, Real)", "IntegrationMethods(_quad/_method=?, _order=?, _functionPart=?, _bound=?)");
  add(im, fp, b);
}

IntegrationMethods::IntegrationMethods(const IntegrationMethod& im1, FunctionPart fp1, real_t b1,
                                       const IntegrationMethod& im2, FunctionPart fp2, real_t b2)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethod, FunctionPart, Real, IntegrationMethod, FunctionPart, Real)", "IntegrationMethods(_quad/_method=?, _order=?, _functionPart=?, _bound=?, _quad/_method=?, _order=?, _functionPart=?, _bound=?)");
  add(im1, fp1, b1);
  add(im2, fp2, b2);
}

IntegrationMethods::IntegrationMethods(const IntegrationMethod& im1, FunctionPart fp1, real_t b1,
                                       const IntegrationMethod& im2, FunctionPart fp2, real_t b2,
                                       const IntegrationMethod& im3, FunctionPart fp3, real_t b3)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethod, FunctionPart, Real, IntegrationMethod, FunctionPart, Real, IntegrationMethod, FunctionPart, Real)", "IntegrationMethods(_quad/_method=?, _order=?, _functionPart=?, _bound=?, _quad/_method=?, _order=?, _functionPart=?, _bound=?, _quad/_method=?, _order=?, _functionPart=?, _bound=?)");
  add(im1, fp1, b1);
  add(im2, fp2, b2);
  add(im3, fp3, b3);
}

//full constructors with _allFunction default
IntegrationMethods::IntegrationMethods(const IntegrationMethod& im1,const IntegrationMethod& im2)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethod, IntegrationMethod)", "IntegrationMethods(_quad/_method=?, _order=?, _bound=0., _quad=?, _order=?)");
  add(im1, _allFunction, 0.);
  add(im2, _allFunction, theRealMax);
}

IntegrationMethods::IntegrationMethods(const IntegrationMethod& im1,real_t b1,
                                       const IntegrationMethod& im2, real_t b2, const IntegrationMethod& im3)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethod, Real, IntegrationMethod, Real, IntegrationMethod)", "IntegrationMethods(_quad/_method=?, _order=?, _bound=?, _quad=?, _order=?, _bound=?, _quad=?, _order=?)");
  add(im1, _allFunction, 0.);
  add(im2, _allFunction, b2);
  add(im3, _allFunction, theRealMax);
}

//partial constructors with _allFunction default and QuadRule's
IntegrationMethods::IntegrationMethods(const IntegrationMethod& im1, QuadRule qr2, number_t ord2)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethod, QuadRule, Number)", "IntegrationMethods(_quad/_method=?, _order=?, _bound=0, _quad=?, _order=?)");
  add(im1, _allFunction, 0.);
  if (im1.isDoubleIM()) add(ProductIM(qr2,ord2), _allFunction, theRealMax);  //assume ProductIM
  else add(QuadratureIM(qr2,ord2), _allFunction, theRealMax);               //assume QuadratureIM
}

IntegrationMethods::IntegrationMethods(const IntegrationMethod& im1, real_t b1, QuadRule qr2, number_t ord2, real_t b2,
                                       QuadRule qr3, number_t ord3)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethod, Real, QuadRule, Number, Real, QuadRule, Number)", "IntegrationMethods(_quad/_method=?, _order=?, _bound=?, _quad=?, _order=?, _bound=?, _quad=?, _order=?)");
  add(im1, _allFunction, b1);
  if (im1.isDoubleIM()) //assume ProductIM
  {
    add(ProductIM(qr2,ord2), _allFunction, b2);
    add(ProductIM(qr3,ord3), _allFunction, theRealMax);
  }
  else //assume QuadratureIM
  {
    add(QuadratureIM(qr2,ord2), _allFunction, b2);
    add(QuadratureIM(qr3,ord3), _allFunction, theRealMax);
  }
}
IntegrationMethods::IntegrationMethods(const IntegrationMethod& im1, real_t b1, QuadRule qr2, number_t ord2, real_t b2,
                                       QuadRule qr3, number_t ord3, real_t b3, QuadRule qr4, number_t ord4)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethod, Real, QuadRule, Number, Real, QuadRule, Number, Real, QuadRule, Number)", "IntegrationMethods(_quad/_method=?, _order=?, _bound=?, _quad=?, _order=?, _bound=?, _quad=?, _order=?, _bound=?, _quad=?, _order=?)");
  add(im1,_allFunction,b1);
  if (im1.isDoubleIM()) //assume ProductIM
  {
    add(ProductIM(qr2,ord2), _allFunction, b2);
    add(ProductIM(qr3,ord3), _allFunction, theRealMax);
  }
  else //assume QuadratureIM
  {
    add(QuadratureIM(qr2,ord2), _allFunction, b2);
    add(QuadratureIM(qr3,ord3), _allFunction, b3);
    add(QuadratureIM(qr4,ord4), _allFunction, theRealMax);
  }
}

//! add IntegrationMethod to the collection
void IntegrationMethods::add(const IntegrationMethod& im, FunctionPart fp, real_t b)
{
  intgMethods.push_back(IntgMeth(im, fp, b));
}

//! print IntegrationMethods
void IntegrationMethods::print(std::ostream& out) const
{
  std::vector<IntgMeth>::const_iterator it=intgMethods.begin();
  for(; it!=intgMethods.end(); ++it) {it->print(out); out << " ; ";}
}

//implementation of Parameter constructors from IntegrationMethod here to avoid cross dependancy
Parameter::Parameter(const IntegrationMethods& ims, const string_t& nm, const string_t& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pointerIntegrationMethods)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  IntegrationMethods* imsb=const_cast<IntegrationMethods*>(&ims);
  p_ = static_cast<void *>(imsb);
}

Parameter::Parameter(const IntegrationMethods& ims, const string_t& nm, const Strings& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pointerIntegrationMethods)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  IntegrationMethods* imsb=const_cast<IntegrationMethods*>(&ims);
  p_ = static_cast<void *>(imsb);
}

Parameter& Parameter::operator=(const IntegrationMethods& ims)
{
  if (p_!=nullptr) deletePointer();
  IntegrationMethods* imsb=const_cast<IntegrationMethods*>(&ims);
  p_ = static_cast<void *>(imsb);
  type_=_pointerIntegrationMethods;
  return *this;
}

void deleteIntegrationMethods(void *p)
{
  p=nullptr;
}

void* cloneIntegrationMethods(const void* p)
{
  return const_cast<void *>(p);
}

} // end of namespace xlifepp
