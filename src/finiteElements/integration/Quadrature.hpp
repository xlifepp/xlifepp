/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Quadrature.hpp
  \authors D. Martin, E. Lunéville
  \since 25 nov 2008
  \date 7 fev 2013

  \brief Definition of the xlifepp::Quadrature class

  xlifepp::Quadrature is a the main class of quadrature rules over elements

  rules over the unit segment [0,1]
    - Gauss-Legendre rules (any odd degree) *Default*
    - Gauss-Lobatto rules, tensor product rules (any odd degree)
    - nodal rules using FE nodes as points (up to P4, degree 5)

  tensor product rules build from 1D rules over the unit square [0,1]x[0,1]
    - Gauss-Legendre rules (any odd degree) *Default*
    - Gauss-Lobatto rules, tensor product rules (any odd degree)
    - nodal rules using FE nodes as points (up to Q4, degree 5)

  rules over the unit triangle { x + y < 1 , 0 < x,y < 1}
    - Gauss-Legendre rules, conical product rules build from 1D rules
    - nodal rules using FE nodes as points (up to P3, degree 3)
    - Grundmann-Moller rules (any odd degree)
    - miscelaneous rules relevant to triangular geometry (Hammer-Stroud, degree 1,2,3, 4=5, 6) *Default*

  tensor product rules build from 1D rules over the unit cube [0,1]x[0,1]x[0,1]
    - Gauss-Legendre rules (any odd degree) *Default*
    - Gauss-Lobatto rules, tensor product rules (any odd degree)
    - nodal rules using FE nodes as points (up to Q4, degree 5)

  rules over the unit tetrahedron { x + y + z < 1 , 0 < x,y,z < 1 }
    - Gauss-Legendre rules, conical product rules build from 1D rules
    - nodal rules using FE nodes as points (P1 and P3 Stroud)
    - Grundmann-Moller rules (any odd degree)
    - miscelaneous rules relevant to tetrahedron geometry (Stroud, degree 1,2,3,4=5) *Default*

  rules over the unit triangular prism { x + y < 1 , 0 < x,y,z < 1 }
    - nodal rules using FE nodes as points (only P1)
    - miscellaneous rules build from tensor product (1, 2=3, 4=5) *Default*
*/

#ifndef QUADRATURE_HPP
#define QUADRATURE_HPP

#include "config.h"
#include "QuadratureRule.hpp"
#include "../GeomRefElement.hpp"

namespace xlifepp
{

/*!
  \class Quadrature
  Quadrature defines the quadrature rule in Finite Element Method
*/
class Quadrature
{
  public:
    GeomRefElement* geomRefElt_p;  //!< pointer to geometric reference element bearing quadrature rule
    QuadratureRule quadratureRule; //!< embedded QuadratureRule object
    QuadRule rule;                 //!< type of quadrature rule
    number_t degree;               //!< degree of quadrature rule
    bool hasPointsOnBoundary;      //!< as it reads
    string_t name;                 //!< name of quadrature formula (for documentation purposes)

  public:

    static std::vector<Quadrature*> theQuadratures;    //!< vector to store run-time Quadrature pointers
    static void clearGlobalVector();                   //!< delete all quadrature objects
    static void printAllQuadratures(std::ostream&);  //!< print the list of Quadrature objects in memory
    static void printAllQuadratures(PrintStream& os) {printAllQuadratures(os.currentStream());}
    static QuadRule bestQuadRule(ShapeType, number_t); //!< best QuadRule to be used to integrate polynom of given degree on a given shape


    /*--------------------------------------------------------------------------------
       Constructors, Destructor
       Warning: Constructor should not be invoked directly:
                 use QuadratureFind function to create or find such an object
    --------------------------------------------------------------------------------*/
  public:
    Quadrature();              //!< default constructor
    Quadrature(ShapeType , QuadRule, number_t, const string_t&, bool pob = false); //!< constructor by shape, rule and number or polynomial degree
    ~Quadrature();             //!< destructor
    Quadrature* clone() const; //!< clone of quadrature, not included in the list of quadratures

  private: //  copy of a Quadrature object is not allowed
    Quadrature(const Quadrature&);      //!< no copy constructor
    void operator=(const Quadrature&);  //!< no assignment operator=

  public:
    // accessors to embedded class QuadratureRule private members
    // ----------------------------------------------------------
    dimen_t dim() const                                   //! dimension of points in formula
    { return quadratureRule.dim(); }
    number_t numberOfPoints() const                       //! number of points of quadrature rule
    { return static_cast<number_t>(quadratureRule.size());}
    ShapeType shapeType() const                          //! shape bearing the quadrature
    {return geomRefElt_p->shapeType();}
    std::vector<real_t>::const_iterator point(number_t i = 0) const  //! get iterator to i-th quadrature point  (0 <= i)
    { return quadratureRule.point(i); }
    std::vector<real_t>::const_iterator weight(number_t i = 0) const //! get iterator to first quadrature weight
    { return quadratureRule.weight(i); }
    const std::vector<real_t>& coords() const             //! returns vector of point coordinates
    { return quadratureRule.coords(); }
    const std::vector<real_t>& weights() const            //! returns vector of weights
    { return quadratureRule.weights(); }

    // general rules
    // -------------
    void centroidRule();    //!< define centroid rule for any element
    void vertexRule();      //!< define rule using all vertices for any element

    //Extern class related functions (friend)
    //---------------------------------------
    friend std::ostream& operator<<(std::ostream&, const Quadrature&);
    friend void alternateRule(QuadRule, ShapeType, const string_t&);
    friend Quadrature* segmentQuadrature(QuadRule, number_t);
    friend Quadrature* triangleQuadrature(QuadRule, number_t);
    friend Quadrature* quadrangleQuadrature(QuadRule, number_t);
    friend Quadrature* tetrahedronQuadrature(QuadRule, number_t);
    friend Quadrature* hexahedronQuadrature(QuadRule, number_t);
    friend Quadrature* prismQuadrature(QuadRule, number_t);
    friend Quadrature* pyramidQuadrature(QuadRule, number_t);
}; // end class Quadrature

//Extern class related functions (friend)
//---------------------------------------
std::ostream& operator<<(std::ostream&, const Quadrature&); //!< print operator
Quadrature* segmentQuadrature(QuadRule, number_t);          //!< find or create quadrature rule over the unit segment
Quadrature* triangleQuadrature(QuadRule, number_t);         //!< find or create quadrature rule over the unit triangle
Quadrature* quadrangleQuadrature(QuadRule, number_t);       //!< find or create quadrature rule over the unit square
Quadrature* tetrahedronQuadrature(QuadRule, number_t);      //!< find or create quadrature rule over the unit tetrahedron
Quadrature* hexahedronQuadrature(QuadRule, number_t);       //!< find or create quadrature rule over the unit hexahedron
Quadrature* prismQuadrature(QuadRule, number_t);            //!< find or create quadrature rule over the unit prism
Quadrature* pyramidQuadrature(QuadRule, number_t);          //!< find or create quadrature rule over the unit pyramid

Quadrature* findQuadrature(ShapeType, QuadRule, number_t, bool = false); //!< "ersatz" of constructor (find or create a new quadrature rule)
Quadrature* findBestQuadrature(ShapeType, number_t, bool = false);       //!< "ersatz" of constructor (find or create a new quadrature rule)

void alternateRule(QuadRule, ShapeType, const string_t&);                      //!< display message before choosing an alternate rule
void badNodeRule(int n_nodes, const string_t& name, ShapeType sh);              //!< error message
void badDegreeRule(int degree, const string_t& name, ShapeType sh);             //!< error message
void noEvenDegreeRule(int degree, const string_t& name, ShapeType sh);          //!< warning message
void upperDegreeRule(int degree, const string_t& name, ShapeType sh);           //!< warning message
void maxDegreeRule(int degree, const string_t& name, ShapeType sh, int maxdeg); //!< warning message

} // end of namespace xlifepp

#endif // QUADRATURE_HPP
