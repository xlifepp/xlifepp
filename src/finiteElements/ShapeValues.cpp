/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ShapeValues.cpp
  \authors D. Martin, N. Kielbasiewicz
  \date 01 jul 2005
  \since 14 dec 2012

  \brief Implementation of xlifepp::ShapeValues class members and related functions
 */

#include "ShapeValues.hpp"
#include "RefElement.hpp"
#include "utils.h"

namespace xlifepp
{
/*--------------------------------------------------------------------------------
 ShapeValues constructors, destructors
 --------------------------------------------------------------------------------*/
ShapeValues::ShapeValues() {}

ShapeValues::ShapeValues(const RefElement& refElem, bool der1, bool der2)
{
  resize(refElem.nbShapeFcts(), refElem.geomRefElem_p->dim(), der1, der2);
}

// Unused
//ShapeValues::ShapeValues(const RefElement& refElem, const dimen_t spaceDim_)
//{
//  resize(refElem, spaceDim_);
//}

// Unused //! defines shape function storage size
//void ShapeValues::resize(const RefElement& refElem, const dimen_t d)
//{
//  // resize shape functions vectors
//  w.resize(refElem.shapeValueSize());
//  dw.resize(d,w);
//}

//! resize shape value vectors
void ShapeValues::resize(const number_t nbd, const dimen_t d, bool der1, bool der2)
{
  dimen_t d2=std::max(1,3*(d-1));
  if(w.size()==nbd && dw.size()==d && d2w.size()==d2) return; //same as original size
  w.resize(nbd);
  if(der1) dw.resize(d,w);
  if(der2) d2w.resize(d2,w);
}

/*--------------------------------------------------------------------------------
 ShapeValues mapping
 --------------------------------------------------------------------------------
 rsv: reference ShapeValues object (contains values and derivatives of reference shape functions)
    rsv.w  =  [w1_x w1_y w1_z w2_x w2_y w2_z ... wn_x wn1_y wn_z]   storage of w
    rsv.dw =  [dxw1_x dxw1_y dxw1_z dxw2_x dxw2_y dxw2_z ... dxwn_x dxwn_y dxwn_z] = rsv.dw[0]
              [dyw1_x dyw1_y dyw1_z dyw2_x dyw2_y dyw2_z ... dywn_x dywn_y dywn_z] = rsv.dw[1]
      (3D)    [dzw1_x dzw1_y dzw1_z dzw2_x dzw2_y dzw2_z ... dzwn_x dzwn_y dzwn_z] = rsv.dw[2]
    rsv.d2w = [dxxw1_x dxxw1_y dxxw1_z dxxw2_x dxxw2_y dxxw2_z ... dxxwn_x dxxwn_y dxxwn_z] = rsv.d2w[0]
              [dyyw1_x dyyw1_y dyyw1_z dyyw2_x dyyw2_y dyyw2_z ... dyywn_x dyywn_y dyywn_z] = rsv.d2w[1]
              [dxyw1_x dxyw1_y dxyw1_z dxyw2_x dxyw2_y dxyw2_z ... dxywn_x dxywn_y dxywn_z] = rsv.d2w[2]
      (3D)    [dzzw1_x dzzw1_y dzzw1_z dzzw2_x dzzw2_y dzzw2_z ... dzzwn_x dzzwn_y dzzwn_z] = rsv.d2w[3]
              [dxzw1_x dxzw1_y dxzw1_z dxzw2_x dxzw2_y dxzw2_z ... dxzwn_x dxzwn_y dxzwn_z] = rsv.d2w[4]
              [dyzw1_x dyzw1_y dyzw1_z dyzw2_x dyzw2_y dyzw2_z ... dyzwn_x dyzwn_y dyzwn_z] = rsv.d2w[5]*/

/*!
 map ReferenceElement shape functions (derivatives) unto current element
 compute dw = J^{-t} * rsv.dw for all shape functions

 \note when transformation is a mapping from 1d->2d or 2d->3d
        invJ = (Jt*J)^{-1}*Jt given also by inverseJacobianMatrix

 rsv: reference ShapeValues object (contains values and derivatives of reference shape functions)
 gd: geometric data (contains the inverse of jacobian)
 der: order of derivatives required
 This function fills w and dw attached to the current ShapeValues object
 note: dw has to be sized before !!!
 TODO: adapt algorithm to avoid copy of w and dw
*/
void ShapeValues::map(const ShapeValues& rsv, const GeomMapData& gd, bool der1, bool der2)
{
  if(this!=&rsv)  w = rsv.w; // perhaps this copy may be avoided
  if(der1) // map first derivative
  {
   std::vector<real_t>::const_iterator itjm1 = gd.inverseJacobianMatrix.begin(), itm;
   dimen_t sdim = gd.spaceDim, edim=gd.elementDim;
   number_t nbw=w.size();

   const std::vector< std::vector<real_t> >* odwp = &rsv.dw;
   bool delodwp = false;
   if(this==&rsv) //self computation
    {
      odwp = new std::vector< std::vector<real_t> >(rsv.dw); //copy to prevent self computation
      delodwp = true;
    }
   std::vector<real_t>::iterator itdw;
   dw.resize(sdim,std::vector<real_t>(nbw,0.));
   for(dimen_t i = 0; i < sdim; i++)
    {
      itdw=dw[i].begin();
      for(number_t j=0; j<nbw; j++, itdw++)
        {
          itm=itjm1+i;
          *itdw=0.;
          for(dimen_t k=0; k<edim; k++, itm+=sdim)  *itdw += *itm * (*odwp)[k][j];
        }
    }
   if(delodwp) delete odwp;
  }
  if(der2) // map second derivative
  {
      //To be done
  }
}

/*!
 map ReferenceElement vector shape functions and its derivatives unto current element
 using contravariant Piola map (J the jacobian of the map from reference element onto element)
      w  = J/|J| * v for all shape functions
      dw = J/|J| * J^{-T} * dv for all shape function derivatives
 used for div conforming element

 Note: when transformation is a mapping from 1d->2d or 2d->3d
        invJ = (JT*J)^{-1}*J{T} given by inverseJacobianMatrix
        and the Piola map reads w  = J/sqrt|Jt*J| * v and dw = J/sqrt|Jt*J| * invJ^{T} * dv
 gd: geometric data (contains the jacobian and other geometric stuff, assumed to be up to date)
 der: order of derivatives required

 \note restricted to 1 order geometric element
*/
void ShapeValues::contravariantPiolaMap(const ShapeValues& rsv, const GeomMapData& gd, bool der1, bool der2)
{
  //number_t dimw=rsv.dw.size();
  number_t dimw=gd.elementDim;       // dim of ref shape functions
  number_t nbw =rsv.w.size()/dimw;   // number of shape functions
  map(rsv,gd,der1,der2);             // apply J^{-T} on first derivative
  const Matrix<real_t>& J = gd.jacobianMatrix;
  real_t detJ = std::abs(gd.jacobianDeterminant); //note when 1d->2d or 2d->3d jacobianDeterminant=sqrt(abs(det(Jt*J)))
  number_t m=J.numberOfRows(), n=J.numberOfCols();

  //apply Piola map J/|J| to w
  std::vector<real_t> ow = w; //copy w
  w.resize(nbw*m);
  std::vector<real_t>::iterator itw=w.begin(), it;
  Matrix<real_t>::const_iterator itJ;
  for(number_t s=0; s<nbw; s++)  //apply J/|J|
    {
      itJ = J.begin();
      for(number_t i=0; i<m; i++, ++itw)
        {
          *itw=0;
          it = ow.begin()+s*n;
          for(number_t j=0; j<n; j++, ++itJ, ++it) *itw +=  *itJ * *it /detJ;
        }
    }

  if(der1) // map first derivative
  {
   //apply Piola map J/|J| J^{-T} to dw (J^{-T}dw already done by map)
   for(number_t k=0; k<m; k++)  //loop on derivative index
    {
      ow = dw[k];
      dw[k].resize(nbw*m);
      itw=dw[k].begin();
      for(number_t s=0; s<nbw; s++)  //apply J/|J|
        {
          itJ = J.begin();
          for(number_t i=0; i<m; i++, ++itw)
            {
              *itw=0;
              it = ow.begin()+s*n;
              for(number_t j=0; j<n; j++, ++itJ, ++it) *itw +=  *itJ * *it /detJ;
            }
        }
    }
  }
  if(der2) //map second derivative
  {
      //To be done
  }
}

/*!
 map ReferenceElement vector shape functions and its derivatives unto current element
 using covariant Piola map (J the jacobian of th map from reference element onto element)
      w  = J^{-T} * v for all shape functions
      dw = J^{-T} * J^{-T} * dv for all shape function derivatives
 used for curl conforming element
 Transformation when mapping from 1d->2d or 2d->3d is the same replacing
      J^{-T} by (invJ){T} with invJ = J*(JT*J)^{-1} (see inverseJacobian)
 rsv: reference ShapeValues object (contains values and derivatives of reference shape functions)
 gd: geometric data (contains the jacobian and other geometric stuff, assumed to be up to date)
 der: order of derivatives required

 note: restrict to 1 order geometric element
*/
void ShapeValues::covariantPiolaMap(const ShapeValues& rsv, const GeomMapData& gd, bool der1, bool der2)
{
  //number_t dimw=rsv.dw.size();
  number_t dimw=gd.elementDim;
  number_t nbw=rsv.w.size()/dimw;
  map(rsv,gd,der1,der2);  //apply J^{-T} to derivatives
  Matrix<real_t> J = transpose(gd.inverseJacobianMatrix);
  number_t m=J.numberOfRows(), n=J.numberOfCols();
  //apply Piola map J^{-T} to w
  std::vector<real_t> ow = w; //copy w
  w.resize(nbw*m);
  std::vector<real_t>::iterator itw=w.begin(), it;
  Matrix<real_t>::const_iterator itJ;
  for(number_t s=0; s<nbw; s++)
    {
      itJ = J.begin();
      for(number_t i=0; i<m; i++, ++itw)
        {
          *itw=0;
          it = ow.begin()+s*n;
          for(number_t j=0; j<n; j++, ++itJ, ++it) *itw +=  *itJ * *it;
        }
    }
  if(der1) //map first derivative
  {
   //apply Piola map J^{-T} to (J^{-T}*dw)
   for(number_t k=0; k<m; k++)  //loop on derivative index
    {
      ow = dw[k];
      dw[k].resize(nbw*m);
      itw=dw[k].begin();
      for(number_t s=0; s<nbw; s++)  //apply J/|J|
        {
          itJ = J.begin();
          for(number_t i=0; i<m; i++, ++itw)
            {
              *itw=0;
              it = ow.begin()+s*n;
              for(number_t j=0; j<n; j++, ++itJ, ++it) *itw +=  *itJ * *it;
            }
        }
     }
  }
  if(der2) //map second derivative
  {
      //To be done
  }
}
/*!
  2D Morley transformation mapping ReferenceElement shape functions (s) unto current element
  from Kirby R.C. A general approach to transforming finite elements.The SMAI journal of computational mathematics,
      Tome 4 (2018),197-224.doi:10.5802/smai-jcm.33.
    w1 = s1-a2s5-a3s6  w2 = s2-a1s4+a3s6  w3 = s3+a1s4+a2s5  w4=d1s4  w5=d2s5  w6=d3s6
    a1 = (Q1)_12/L1  a2 = (Q2)_12/L2  a3 = (Q3)_12/L3  di=(Qi)_11
    Qi = [ni ti]Jt[nui taui]
         (ni, ti)   outward normal and tangent vector (pi/2 rotation of normal) on edge i of the reference triangle
         (nui taui) outward normal and tangent vector (pi/2 rotation of normal) on edge i of the current triangle
         Li length of edge i of the current triangle
     here edge i is the edge which does not contain vi vertex
      i=1 : n1=0.5*sqrt(2)(1,1), t1=0.5*sqrt(2)(-1,1) => Q1_11=0.5(g1,g2).nu1 Q1_12=0.5(g1,g2).tau1 with g1=J11-J12, g2=J21-J22
      i=2 : n2=(-1,0), t2=(0,-1) => Q2_11=-(g1,g2).nu1 Q2_12=-(g1,g2).tau1 with g1=J11, g2=J21
      i=3 : n3=(0,-1), t3=(1,0) => Q3_11=(g1,g2).nu1 Q3_12=(g1,g2).tau1 with g1=J12, g2=J22

    for derivatives: dw = MJ^{-t}ds and d2w = MJ^{-t}J^{-t}ds

  rsv: reference ShapeValues object (contains values and derivatives of reference shape functions)
  gd: geometric data (contains the inverse of jacobian)
  der: order of derivatives required
  This function fills w,dw,d2w attached to the current ShapeValues object
  note: dw, d2w has to be sized before !!!
*/
void ShapeValues::Morley2dMap(const ShapeValues& rsv, const GeomMapData& gd, bool der1, bool der2)
{
   std::vector< Vector<real_t> >& ns=gd.sideNV();
   const Matrix<real_t>& J=gd.jacobianMatrix;
   real_t j11=J(1,1), j12=J(1,2),j21=J(2,1),j22=J(2,2);
   real_t a1= (j11*ns[1][1]-j21*ns[1][0])/gd.measures(2), d1=-(j21*ns[1][1]+j11*ns[1][0]);
   real_t a2= (j22*ns[2][0]-j12*ns[2][1])/gd.measures(3), d2=-(j12*ns[2][0]+j22*ns[2][1]);
   real_t a3= 0.5*sqrtOf2_*((j21+j22)*ns[0][0]-(j11+j12)*ns[0][1])/gd.measures(1),
          d3= 0.5*sqrtOf2_*((j11+j12)*ns[0][0]+(j22+j21)*ns[0][1]);
   // Map shape functions
   const std::vector<real_t>& rw=rsv.w;
   w.resize(rw.size());
   w[0]=rw[0]-a2*rw[4]-a3*rw[5];
   w[1]=rw[1]-a1*rw[3]+a3*rw[5];
   w[2]=rw[2]+a1*rw[3]+a2*rw[4];
   w[3]=d1*rw[3];
   w[4]=d2*rw[4];
   w[5]=d3*rw[5];
   if(!der1 && !der2) return;

   const Matrix<real_t>& invJ=gd.inverseJacobianMatrix;
   j11=invJ(1,1); j12=invJ(1,2);j21=invJ(2,1);j22=invJ(2,2);
   if(der1) // Map first derivatives of shape functions
   {
    const std::vector<real_t>* dw0=&rsv.dw[0]; //to prevent case of this=&rsv
    if(this==&rsv) dw0=new std::vector<real_t>(rsv.dw[0]);
    dw[0]=j11**dw0+j21*rsv.dw[1];
    dw[1]=j12**dw0+j22*rsv.dw[1];
    if(this==&rsv) delete dw0;
    for(number_t k=0;k<rsv.dw.size();k++)
    {
       std::vector<real_t>& dwk=dw[k];
       dwk[0]+= -a2*dwk[4]-a3*dwk[5];
       dwk[1]+= -a1*dwk[3]+a3*dwk[5];
       dwk[2]+=  a1*dwk[3]+a2*dwk[4];
       dwk[3]*=  d1;
       dwk[4]*=  d2;
       dwk[5]*=  d3;
    }
   }
   if(der2) // Map second derivatives of shape functions
   {
    const std::vector<real_t> *d2w0=&rsv.d2w[0]; //to prevent case of this=&rsv
    const std::vector<real_t> *d2w1=&rsv.d2w[1];
    if(this==&rsv) {d2w0=new std::vector<real_t>(rsv.d2w[0]);d2w1=new std::vector<real_t>(rsv.d2w[1]);}
    d2w[0]=(j11*j11)**d2w0+(j21*j21)**d2w1+(2*j11*j21)*rsv.d2w[2];
    d2w[1]=(j12*j12)**d2w0+(j22*j22)**d2w1+(2*j12*j22)*rsv.d2w[2];
    d2w[2]=(j11*j12)**d2w0+(j21*j22)**d2w1+(j12*j21+j11*j22)*rsv.d2w[2];
    if(this==&rsv) {delete d2w0; delete d2w1;}
    for(number_t k=0;k<rsv.d2w.size();k++)
    {
       std::vector<real_t>& d2wk=d2w[k];
       d2wk[0]+= -a2*d2wk[4]-a3*d2wk[5];
       d2wk[1]+= -a1*d2wk[3]+a3*d2wk[5];
       d2wk[2]+=  a1*d2wk[3]+a2*d2wk[4];
       d2wk[3]*=  d1;
       d2wk[4]*=  d2;
       d2wk[5]*=  d3;
    }
   }
}

/*!
  2D Argyris transformation mapping ReferenceElement shape functions (s) unto current element
  from Kirby R.C. A general approach to transforming finite elements.The SMAI journal of computational mathematics,
      Tome 4 (2018),197-224.doi:10.5802/smai-jcm.33.
  same notation as Morley map, see XLiFE++ doc for details
  for derivatives: dw = MJ^{-t}ds and d2w = MJ^{-t}J^{-t}ds

  rsv: reference ShapeValues object (contains values and derivatives of reference shape functions)
  gd: geometric data (contains the inverse of jacobian)
  der: order of derivatives required
  This function fills w,dw,d2w attached to the current ShapeValues object
  note: dw, d2w has to be sized before !!!
*/
void ShapeValues::Argyris2dMap(const ShapeValues& rsv, const GeomMapData& gd, bool der1, bool der2)
{
   real_t q8=15./8, s16=7./16, s32=1./32;
   std::vector< Vector<real_t> >& ns=gd.sideNV();
   const Matrix<real_t>& J=gd.jacobianMatrix;
   real_t j11=J(1,1), j12=J(1,2),j21=J(2,1),j22=J(2,2);
   real_t l1=gd.measures(2), l2=gd.measures(3), l3=gd.measures(1);
   real_t n1x=ns[1][0],n1y=ns[1][1], n2x=ns[2][0], n2y=ns[2][1], n3x=ns[0][0], n3y=ns[0][1];
   real_t b1= (j11*n1y-j21*n1x), d1=-(j21*n1y+j11*n1x);
   real_t b2= (j22*n2x-j12*n2y), d2=-(j12*n2x+j22*n2y);
   real_t b3= 0.5*sqrtOf2_*((j21+j22)*n3x-(j11+j12)*n3y), d3= 0.5*sqrtOf2_*((j11+j12)*n3x+(j22+j21)*n3y);
   real_t a1=b1/l1, a2=b2/l2, a3=b3/l3, c1=b1*l1, c2=b2*l2, c3=b3*l3;
   real_t t11=j11*j11, t12=2*j11*j21, t13=j21*j21,
          t21=j12*j11, t22=j12*j21+j11*j22, t23=j21*j22,
          t31=j12*j12, t32=2*j22*j12, t33=j22*j22;
   a1*=q8;a2*=q8;a3*=q8;b1*=s16;b2*=s16;b3*=s16;c1*=s32;c2*=s32;c3*=s32;
   const std::vector<real_t>& rw=rsv.w;
   // Map shape functions
   xlifepp::Argyris2dMap(rw,w,j11,j12,j21,j22, t11,t12,t13,t21,t22,t23,t31,t32,t33,
                a1,a2,a3, b1,b2,b3, c1,c2,c3, d1,d2,d3, n1x,n1y,n2x,n2y,n3x,n3y);
   if(!der1 && !der2) return;

   const Matrix<real_t>& invJ=gd.inverseJacobianMatrix;
   real_t i11=invJ(1,1), i12=invJ(1,2),i21=invJ(2,1),i22=invJ(2,2);
   if(der1) // Map first derivatives of shape functions
   {
    const std::vector<real_t>* dw0=&rsv.dw[0]; //to prevent case of this=&rsv
    if(this==&rsv) dw0=new std::vector<real_t>(rsv.dw[0]);
    dw[0]=i11**dw0+i21*rsv.dw[1];
    dw[1]=i12**dw0+i22*rsv.dw[1];
    if(this==&rsv) delete dw0;
    for(number_t k=0;k<rsv.dw.size();k++)
       xlifepp::Argyris2dMap(dw[k],dw[k],j11,j12,j21,j22, t11,t12,t13,t21,t22,t23,t31,t32,t33,
                a1,a2,a3, b1,b2,b3, c1,c2,c3, d1,d2,d3, n1x,n1y,n2x,n2y,n3x,n3y);
   }
   if(der2) // Map second derivatives of shape functions
   {
    const std::vector<real_t> *d2w0=&rsv.d2w[0]; //to prevent case of this=&rsv
    const std::vector<real_t> *d2w1=&rsv.d2w[1];
    if(this==&rsv) {d2w0=new std::vector<real_t>(rsv.d2w[0]);d2w1=new std::vector<real_t>(rsv.d2w[1]);}
    d2w[0]=(i11*i11)**d2w0+(i21*i21)**d2w1+(2*i11*i21)*rsv.d2w[2];
    d2w[1]=(i12*i12)**d2w0+(i22*i22)**d2w1+(2*i12*i22)*rsv.d2w[2];
    d2w[2]=(i11*i12)**d2w0+(i21*i22)**d2w1+(i12*i21+i11*i22)*rsv.d2w[2];
    if(this==&rsv) {delete d2w0; delete d2w1;}
    for(number_t k=0;k<rsv.d2w.size();k++)
      xlifepp::Argyris2dMap(d2w[k],d2w[k],j11,j12,j21,j22, t11,t12,t13,t21,t22,t23,t31,t32,t33,
                   a1,a2,a3, b1,b2,b3, c1,c2,c3, d1,d2,d3, n1x,n1y,n2x,n2y,n3x,n3y);
   }
}

//apply Argirys map to a vector
void Argyris2dMap(const std::vector<real_t>& rw, std::vector<real_t>& w,
                  real_t j11, real_t j12, real_t j21, real_t j22,
                  real_t t11, real_t t12, real_t t13, real_t t21, real_t t22, real_t t23, real_t t31, real_t t32, real_t t33,
                  real_t a1, real_t a2, real_t a3, real_t b1, real_t b2, real_t b3, real_t c1, real_t c2, real_t c3, real_t d1, real_t d2, real_t d3,
                  real_t n1x, real_t n1y, real_t n2x, real_t n2y, real_t n3x, real_t n3y)
{
   if(&w!=&rw) w.resize(rw.size());
   real_t rw1, rw2, rw3, rw18=rw[18], rw19=rw[19], rw20=rw[20];
   w[0] =rw[0] - (a2*rw19+a3*rw20);                        //vertex 1 value
   w[6] =rw[6] + (a3*rw20-a1*rw18);                        //vertex 2 value
   w[12]=rw[12]+ (a1*rw18+a2*rw19);                        //vertex 3 value
   rw1=rw[1];rw2=rw[2]; // to allow self assignment
   w[1] =(j11*rw1+j12*rw2) - (b2*n2y*rw19-b3*n3y*rw20);  //vertex 1 dx
   w[2] =(j21*rw1+j22*rw2) + (b2*n2x*rw19-b3*n3x*rw20);  //vertex 1 dy
   rw1=rw[7];rw2=rw[8];
   w[7] =(j11*rw1+j12*rw2) + (b1*n1y*rw18+b3*n3y*rw20);  //vertex 2 dx
   w[8] =(j21*rw1+j22*rw2) - (b1*n1x*rw18+b3*n3x*rw20);  //vertex 2 dy
   rw1=rw[13];rw2=rw[14];
   w[13]=(j11*rw1+j12*rw2) + (b1*n1y*rw18-b2*n2y*rw19);  //vertex 3 dx
   w[14]=(j21*rw1+j22*rw2) - (b1*n1x*rw18-b2*n2x*rw19);  //vertex 3 dy
   rw1=rw[3]; rw2=rw[4], rw3=rw[5];
   w[3] =(t11*rw1+t21*rw2+t31*rw3) - (c2*n2y*n2y*rw19+c3*n3y*n3y*rw20);     //vertex 1 dxx
   w[4] =(t12*rw1+t22*rw2+t32*rw3) + 2*(c2*n2x*n2y*rw19+c3*n3x*n3y*rw20);   //vertex 1 dxy
   w[5] =(t13*rw1+t23*rw2+t33*rw3) - (c2*n2x*n2x*rw19+c3*n3x*n3x*rw20);     //vertex 1 dyy
   rw1=rw[9]; rw2=rw[10], rw3=rw[11];
   w[9] =(t11*rw1+t21*rw2+t31*rw3) - (c1*n1y*n1y*rw18-c3*n3y*n3y*rw20);     //vertex 2 dxx
   w[10]=(t12*rw1+t22*rw2+t32*rw3) + 2*(c1*n1x*n1y*rw18-c3*n3x*n3y*rw20);   //vertex 2 dxy
   w[11]=(t13*rw1+t23*rw2+t33*rw3) - (c1*n1x*n1x*rw18-c3*n3x*n3x*rw20);     //vertex 2 dyy
   rw1=rw[15]; rw2=rw[16], rw3=rw[17];
   w[15]=(t11*rw1+t21*rw2+t31*rw3) + (c1*n1y*n1y*rw18+c2*n2y*n2y*rw19);     //vertex 3 dxx
   w[16]=(t12*rw1+t22*rw2+t32*rw3) - 2*(c1*n1x*n1y*rw18+c2*n2x*n2y*rw19);   //vertex 3 dxy
   w[17]=(t13*rw1+t23*rw2+t33*rw3) + (c1*n1x*n1x*rw18+c2*n2x*n2x*rw19);     //vertex 3 dyy
   w[18]=d1*rw18;    //normal derivative 1
   w[19]=d2*rw19;    //normal derivative 2
   w[20]=d3*rw20;    //normal derivative 3
}

/*!
 change sign of shape functions and its derivatives according to a sign vector
     wi, dxwi, dywi, dzwi, dxxwi, ..  becomes  -wi, -dxwi, -dywi, -dzwi, -dxxwi if sign(i)<0
 sign: sign vector of size nbdofs
 dimf: dimension of shape functions
*/
void ShapeValues::changeSign(const std::vector<real_t>& sign, dimen_t dimf, bool der1, bool der2)
{
  number_t nbw=w.size()/dimf, step=1;
  if(sign.size()<nbw) step=nbw/sign.size();
  number_t sd=step*dimf;
  std::vector<real_t>::const_iterator its=sign.begin();
  std::vector<real_t>::iterator itw=w.begin();
  for(; its!=sign.end(); ++its)
    {
      if(*its<0)    //change sign
        for(dimen_t k=0; k< step; ++k)
          for(dimen_t d=0; d< dimf; ++d, ++itw) *itw*=-1;
      else itw+=sd; //keep values
    }
  if(der1) // first derivatives
  {
    std::vector<std::vector<real_t> >::iterator itd=dw.begin();
    for(; itd!=dw.end(); ++itd)
    {
      its=sign.begin();
      itw=itd->begin();
      for(; its!=sign.end(); ++its)
        {
          if(*its<0)    //change sign
            for(dimen_t k=0; k< step; ++k)
              for(dimen_t d=0; d< dimf; ++d, ++itw) *itw*=-1;
          else itw+=sd; //keep values
        }
    }
  }
  if(der2) //second derivatives
  {
    std::vector<std::vector<real_t> >::iterator itd2=d2w.begin();
    for(; itd2!=d2w.end(); ++itd2)
    {
      its=sign.begin();
      itw=itd2->begin();
      for(; its!=sign.end(); ++its)
        {
          if(*its<0)    //change sign
            for(dimen_t k=0; k< step; ++k)
              for(dimen_t d=0; d< dimf; ++d, ++itw) *itw*=-1;
          else itw+=sd; //keep values
        }
    }
  }
}
/*--------------------------------------------------------------------------------
  Miscellaneous transforms !!!
 --------------------------------------------------------------------------------*/
void ShapeValues::set(const real_t w1)
{
  for(std::vector<real_t>::iterator it_w = w.begin(); it_w != w.end(); it_w++) *it_w = w1;
}

/*!
 Extend scalar shape function values to a d-vector shape function values
 values w are extended to (w,0) (0,w) in 2D and (w,0,0) (0,w,0) (0,0,w) in 3D
 in 2D values [dxw;dyw] are extended to [(dxw,0) (0 dxw); (dyw,0) (0,dyw)]
              [dxxw;dyyw;dxyw] are extended to [(dxxw,0) (0 dxxw); (dyyw,0) (0,dyyw); (dxyw,0) (0,dxyw)]
 in 3D values [dxxw;dyyw;dxyw;dzzw;dxzw;dyzw] are extended to
     [(dxxw,0,0) (0,dxxw,0) (0,0,dxxw); (dyyw,0,0) (0,dyyw,0) (0,0,dyyw); (dxyw,0,0) (0,dxyw,0) (0,0,dxyw);
      (dzzw,0,0) (0,dzzw,0) (0,0,dzzw); (dxzw,0,0) (0,dxzw,0) (0,0,dxzw); (dyzw,0,0) (0,dyzw,0) (0,0,dyzw)]
 create a new ShapeValues object
*/
void ShapeValues::extendToVector(dimen_t d)
{
  if(d<2) return;
  std::vector<real_t> ow=w;
  w.assign(d*d*ow.size(),0.);
  std::vector<real_t>::iterator it_ow, it_w=w.begin();
  for(it_ow = ow.begin(); it_ow != ow.end(); it_ow++)
    {
      *it_w++=*it_ow;
      for(dimen_t i=1; i<d; i++) {it_w+=d; *it_w++=*it_ow;}
    }

  std::vector< std::vector<real_t> > odw=dw;
  dw.assign(odw.size(),std::vector<real_t>(d*d*ow.size(),0.));
  std::vector< std::vector<real_t> >::iterator it_odw, it_dw=dw.begin();
  for(it_odw = odw.begin(); it_odw != odw.end(); it_odw++,it_dw++)
    {
      it_w=it_dw->begin();
      for(it_ow = it_odw->begin(); it_ow != it_odw->end(); it_ow++)
        {
          *it_w++=*it_ow;
          for(dimen_t i=1; i<d; i++) {it_w+=d; *it_w++=*it_ow;}
        }
    }
  if(d2w.size()==0) return;
  std::vector< std::vector<real_t> > od2w=d2w;
  d2w.assign(od2w.size(),std::vector<real_t>(d*d*ow.size(),0.));
  std::vector< std::vector<real_t> >::iterator it_od2w, it_d2w=d2w.begin();
  for(it_od2w = od2w.begin(); it_od2w != od2w.end(); it_od2w++,it_d2w++)
    {
      it_w=it_d2w->begin();
      for(it_ow = it_od2w->begin(); it_ow != it_od2w->end(); it_ow++)
        {
          *it_w++=*it_ow;
          for(dimen_t i=1; i<d; i++) {it_w+=d; *it_w++=*it_ow;}
        }
    }
}

/*--------------------------------------------------------------------------------
  output
 --------------------------------------------------------------------------------*/
//! print shape functions and derivatives to output file stream
std::ostream& operator<<(std::ostream& os, const ShapeValues& obj)
{
  os.setf(std::ios::scientific);
  printRowWise(os, "     w:", entriesPerRow, entryWidth, entryPrec, obj.w.begin(), obj.w.end());
  for(dimen_t d = 0; d < obj.dw.size(); d++)
    {
      if(!(obj.dw[0].empty()))
        {
          os << std::endl;
          printRowWise(os, "   d" + tostring(d + 1) + "w:", entriesPerRow, entryWidth, entryPrec, obj.dw[d].begin(), obj.dw[d].end());
        }
    }
  std::vector<string_t> sd(6);sd[0]="d11";sd[1]="d22";sd[2]="d12";sd[3]="d33";sd[4]="d13";sd[5]="d23";
  for(dimen_t d = 0; d < obj.d2w.size(); d++)
    {
      if(!(obj.d2w[0].empty()))
        {
          os << std::endl;
          printRowWise(os, "   "+sd[d]+"w:", entriesPerRow, entryWidth, entryPrec, obj.d2w[d].begin(), obj.d2w[d].end());
        }
    }
  os.unsetf(std::ios::scientific);
  return os;
}

} // end of namespace xlifepp
