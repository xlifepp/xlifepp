/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomRefPoint.hpp
  \author Y. Lafranche
  \since 21 oct 2016
  \date 21 oct 2016

  \brief Definition of the xlifepp::GeomRefPoint class

  xlifepp::GeomRefPoint defines Reference Element geometric data on a point
 */

#ifndef GEOM_REF_POINT_HPP
#define GEOM_REF_POINT_HPP

#include "config.h"
#include "../GeomRefElement.hpp"

namespace xlifepp {

/*!
  \class GeomRefPoint
 */
class GeomRefPoint : public GeomRefElement {
  public:
    //! default constructor
    GeomRefPoint();

    real_t measure(const dimen_t dim, const number_t sideNum = 0) const; //!< returns 1. (Dirac)

    //! returns tangent vector on a side
    void tangentVector(const std::vector<real_t>& jacobianMatrix, std::vector<std::vector<real_t> >& tgv, const number_t, const dimen_t) const { noSuchFunction("tangentVector"); }
    //! returns vertex opposite to vertex number vNum (=1,2)
    number_t vertexOppositeSide(const number_t sideNum) const { noSuchFunction("vertexOppositeSide"); return 0; }
    //! returns vertex to vertex number vNum (=1,2)
    number_t sideOppositeVertex(const number_t vNum) const { noSuchFunction("sideOppositeVertex"); return 0; }
    //! return edge oppsite to edge e
    number_t edgeOppositeEdge(const number_t) const { noSuchFunction("edgeOppositeEdge"); return 0; }
    //! returns side with vertices v1 and v2
    number_t sideWithVertices(const number_t, const number_t) const { noSuchFunction("sideWithVertices"); return 0;}
    //! returns side with vertices v1 and v2
    number_t sideWithVertices(const number_t, const number_t, const number_t, const number_t = 0) const { noSuchFunction("sideWithVertices"); return 0;}

    bool contains(std::vector<real_t>& p, real_t tol= theTolerance) const;              //!< test if a point belongs to current element
    virtual std::vector<real_t> projection(const std::vector<real_t>&, real_t&) const;  //!< projection of a point onto ref element

}; // end of class GeomRefPoint

} // end of namespace xlifepp

#endif /* GEOM_REF_POINT_HPP */
