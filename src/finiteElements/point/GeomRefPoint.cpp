/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomRefPoint.cpp
  \author Y. Lafranche
  \since 21 oct 2016
  \date 21 oct 2016

  \brief Implementation of xlifepp::GeomRefPoint class members and related functions
 */

#include "GeomRefPoint.hpp"

namespace xlifepp {

//! GeomRefPoint constructor for 1D Geometric Reference Element
GeomRefPoint::GeomRefPoint()
: GeomRefElement(_point, 1, 1., 0., 1, 0, 0) {
               // ShapeType, dim, measure, centroid, nbVert, nbSideOfSides, nbSides
  // The dimension of the element is set to 1, although it should conceptually be 0,
  // in order to allow the storage of the fundamental data defining the element.
  vertices_[0] = 0.;
}

//! Returns element length (1.)
real_t GeomRefPoint::measure(const dimen_t d, const number_t sideNum) const {
  return measure_;
}

//! test if a point belongs to current element
bool GeomRefPoint::contains(std::vector<real_t>& p, real_t tol) const {
  real_t x=p[0];
  return (x >=-tol) && (x <= tol);
}

//! projection of a point onto ref element [0,1]
std::vector<real_t> GeomRefPoint::projection(const std::vector<real_t>&p , real_t& h) const {
  return std::vector<real_t>(1,0.);
}

} // end of namespace xlifepp
