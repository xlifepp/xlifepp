/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RefPoint.hpp
  \author Y. Lafranche
  \since 14 Jun 2017
  \date 14 Jun 2017

  \brief Definition of the xlifepp::RefPoint class

  xlifepp::RefPoint defines reference element interpolation data on a point

*/

#ifndef REF_POINT_HPP
#define REF_POINT_HPP

#include "config.h"
#include "../RefElement.hpp"

namespace xlifepp {

class Interpolation;

/*!
  \class RefPoint
  Parent class: RefElement
 */
class RefPoint : public RefElement {
  public:
    //! constructor by interpolation
    RefPoint(const Interpolation* interp_p) : RefElement(_point, interp_p) {
     // build element interpolation data
     nbDofs_ = nbDofsOnVertices_ = 1;
     nbInternalDofs_ = 0;
     // creating D.o.F of reference element
     // in Lagrange standard elements only vertices are sharable
     refDofs.reserve(nbDofs_);
     lagrangeRefDofs(0, nbDofsOnVertices_, nbInternalDofs_);

     // compute number of shape functions
     nbshfcts_ = shapeValueSize();
     nbPts_ = nbDofs_;
    }

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv = true, const bool with2Deriv=false) const {
      shv.w[0] = 1.;
      if (withDeriv) { shv.dw[0][0] = 0.; }
      if (with2Deriv) { shv.d2w[0][0] = 0.; }
    }
    virtual void outputAsP1(std::ofstream& os, const int, const std::vector<number_t>& ranks) const { noSuchFunction("outputAsP1"); }
    const splitvec_t& getO1splitting() const // override
       { return RefElement::getO1splitting(); }

  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class RefPoint

} // end of namespace xlifepp

#endif /* REF_POINT_HPP */
