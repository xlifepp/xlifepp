/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file NedelecEdgeQuadrangle.cpp
  \authors E. Lunéville
  \since 5 march 2018
  \date  5 march 2018

  \brief Implementation of xlifepp::NedelecEdgeQuadrangle class members and related functions
 */

#include "NedelecEdgeQuadrangle.hpp"
#include "../Interpolation.hpp"
#include "../integration/Quadrature.hpp"
#include "../triangle/LagrangeTriangle.hpp"
#include "../segment/LagrangeSegment.hpp"
#include "LagrangeQuadrangle.hpp"
#include "utils.h"
#include <iostream>

namespace xlifepp
{

//! NedelecEdgeQuadrangle constructor
NedelecEdgeQuadrangle::NedelecEdgeQuadrangle(const Interpolation* interp_p)
  : RefQuadrangle(interp_p)
{
  name_ += "_Nedelec_Quadrangle_Edge_";
  mapType = _covariantPiolaMap;
  dofCompatibility=_signDofCompatibility;
  dimShapeFunction=2;
}

NedelecEdgeQuadrangle::~NedelecEdgeQuadrangle() {}

//=====================================================================================
/*! Nedelec edge first family of any order k on quadrangle Q (NEC1_k)
     space  Vk: Q_(k-1,k) x Q_(k,k-1) with Q_(k,l) monomials of degree at most k in x1, of degree at most l in x2, 2k(k+1) dofs
     edge dofs: v-> int_e v.t q,     q in P_(k-1)[e]                 k dofs by edge e
     tetrahedron dofs (k>1): v-> int_t v.q,       q in Q_(k-1,k-2) x Q_(k-2,k-1)  2k(k-1) dofs
  */
//=====================================================================================
NedelecEdgeFirstQuadranglePk::NedelecEdgeFirstQuadranglePk(const Interpolation* interp_p)
  : NedelecEdgeQuadrangle(interp_p)
{
  name_ += "first family_"+tostring(interp_p->numtype);
  interpolationData();    // build element interpolation data
  sideNumbering();        // local numbering of dofs on faces
  pointCoordinates();     // "virtual coordinates" of moment dofs
  //sideRefElement();       // Reference Element on edges
}

NedelecEdgeFirstQuadranglePk::~NedelecEdgeFirstQuadranglePk() {}

//! interp defines Reference Element interpolation data
void NedelecEdgeFirstQuadranglePk::interpolationData()
{
  trace_p->push("NedelecEdgeFirstQuadranglePk::interpolationData");

  number_t k = interpolation_p->numtype;
  nbDofs_= 2*k*(k+1);
  nbDofsInSides_=4*k;  //edge
  nbInternalDofs_=2*k*(k-1); //quadrangle not on edge
  refDofs.reserve(nbDofs_);
  number_t nbds=nbDofsInSides_/4;
  for(number_t e = 1; e <= 4; ++e)        //edge dofs (rank in dual polynoms basis is used to identify dof)
    for(number_t i=0; i<nbds; ++i)
      refDofs.push_back(new RefDof(this, true, _onEdge, e, i+1, 2, 1, 0, 2, 0, _crossnProjection,_ncross,"int_e u.t q"));
  if(k>1)
    {
      for(number_t i=0; i<nbInternalDofs_; ++i) //element dofs (order = i is used to identify dof)
        refDofs.push_back(new RefDof(this, false, _onElement, 0, i+1, 2, 2, 0, 2, 0, _noProjection,_id,"int_k u.q"));
    }

  // compute shape functions as formal polynomials
  computeShapeFunctions();
  buildPolynomialTree();   //construct tree representation
  maxDegree = Wk.degree();

  // compute number of shape functions
  nbshfcts_ = shapeValueSize();

  // requires edge dofs rotation
  //rotateDof = (k>1);
  rotateDof = false;

  trace_p->pop();
}

/*! compute shape functions as polynomials using general algorithm
     edge dofs: v-> int_e v.t q,     q in P_(k-1)[e]                 k dofs by edge e
     tetrahedron dofs (k>1): v-> int_t v.q,       q in Q_(k-1,k-2) x Q_(k-2,k-1)  2k(k-1) dofs
*/
void NedelecEdgeFirstQuadranglePk::computeShapeFunctions()
{
  number_t k = interpolation_p->numtype;
  PolynomialsBasis::iterator itp, itq;
  std::vector<real_t>::const_iterator itpt, itw; //for quadrature rule
  PolynomialsBasis Vk(_DQ3k,2,k);     //Nedelec first family polynomials space DQ3k = Qk-1,k x Qk,k-1
  Matrix<real_t> L(nbDofs_,nbDofs_);  //matrix of linear system of shape functions

  //EDGES: compute Matrix Lij=l(pi,qj) for all pi in Vk, for all qj in P_(k-1)[e]
  LagrangeStdSegment segPk(findInterpolation(_Lagrange, _standard, k-1, H1));
  segPk.computeShapeFunctions();
  Ek=segPk.Wk;       //dual space for edge dofs: P_(k-1)[x]
  dimen_t d=Vk.degree()+Ek.degree();
  Quadrature* quad=findBestQuadrature(_segment, d);  //quadrature rule on segment of order d (integrates exactly x^d)
  number_t nbq= quad->numberOfPoints();
  number_t j=1;
  for(itp=Vk.begin(); itp!=Vk.end(); ++itp, ++j)
    {
      number_t i=1;
      for(number_t e = 1; e <= 4; ++e)
        {
          Vector<real_t> t(2,0.);   //unit tangential vector chosen as (vj-vi)/|vj-vi| on edge (vi,vj)
          switch(e)
            {
              case 1: t[1]=1.;  break; //edge (1,2), x=1
              case 2: t[0]=-1.; break; //edge (2,3), y=1
              case 3: t[1]=-1.; break; //edge (3,4), x=0
              default: t[0]=1.;  break; //edge (4,1), y=0
            }
          Polynomial pt=dot(*itp,t);
          for(itq=Ek.begin(); itq!=Ek.end(); ++itq, ++i)
            {
              L(i,j)=0;
              itpt=quad->point();
              itw=quad->weight();
              Polynomial& pq=(*itq)[0];
              for(number_t q=0; q<nbq; ++q, ++itw)   //compute by 1D quadrature
                {
                  real_t x=*itpt++;     //quadrature point
                  switch(e)
                    {
                      case 1: L(i,j)+= pt(1,x) * pq(x)** itw ; break;    //on edge (1,2) : x -> (1,x)
                      case 2: L(i,j)+= pt(1-x,1) * pq(x)** itw ; break;  //on edge (2,3) : x -> (1-x,1)
                      case 3: L(i,j)+= pt(0,1-x) * pq(x)** itw ; break;  //on edge (3,4) : x -> (0,1-x)
                      default: L(i,j)+= pt(x,0) * pq(x)** itw ; break;    //on edge (4,1) : x -> (x,0)
                    }
                }
            }
        }
    }

  if(k>1)  //dofs on QUADRANGLE
    {
      // compute Matrix Lij=l(pi,qj)=int_h pi.qj for all pi in Vk, qj in Q_(k-1,k-2) x Q_(k-2,k-1)
      Kk=PolynomialsBasis(_DQk,2,k-1);
      PolynomialsBasis::iterator itr;
      d=Vk.degree()+Kk.degree();
      quad=findBestQuadrature(_quadrangle,d);  //quadrature rule on quadrangle of order d
      nbq= quad->numberOfPoints();
      j=1;
      for(itp=Vk.begin(); itp!=Vk.end(); ++itp,++j)
        {
          number_t i= nbDofsInSides_+ 1;

          for(itr=Kk.begin(); itr!=Kk.end(); ++itr,++i)
            {
              L(i,j)=0.;
              itpt=quad->point();
              itw=quad->weight();
              for(number_t q=0; q<nbq; ++q, ++itw)
                {
                  real_t x=*itpt++, y=*itpt++, r=0.;
                  std::vector<real_t> vp=eval(*itp,x,y);
                  L(i,j)+= *itw * std::inner_product(vp.begin(),vp.end(),eval(*itr,x,y).begin(),r);
                }
            }
        }
    }
  //compute coefficients of shape functions by solving linear systems
  Matrix<real_t> sf(nbDofs_,_idMatrix);
  real_t eps=theZeroThreshold;
  number_t r;
  bool ok=gaussMultipleSolver(L, sf, nbDofs_, eps, r);
  if(!ok)
    {
      where("NedelecEdgeFirstQuadranglePk::computeShapeFunctions()");
      error("mat_noinvert");
    }
  sf.roundToZero();
  Matrix<real_t>::iterator itm=sf.begin();
  Vector<real_t> v(nbDofs_);
  Vector<real_t>::iterator itv;
  Wk.dimVar=2;
  Wk.dimVec=2;
  Wk.name="NEC1_"+tostring(k);
  for(number_t i=0; i<nbDofs_; i++)
    {
      for(itv=v.begin(); itv!=v.end(); ++itv, ++itm) *itv=*itm;
      Wk.push_back(combine(Vk,v));
    }
  dWk.resize(2);
  dWk[0]=dx(Wk);
  dWk[1]=dy(Wk);
}

/* builds virtual coordinates of moment dofs of order k
     for edge dofs: coordinates of dofs of Pk-1[s] on segment s=[1/k+1, k/k+1] mapped with the same transformation as those used to compute L matrix
     for internal dofs: coordinates of dofs of Pk-1[s] x Pk-2[s] and Pk-2[s] x Pk-1[s] on quadrangle
*/
void NedelecEdgeFirstQuadranglePk::pointCoordinates()
{
  std::vector<RefDof*>::iterator it_rd=refDofs.begin();
  number_t nbds=nbDofsInSides_/4, k= interpolation_p->numtype, kp=k+1;
  for(number_t e = 1; e <= 4; ++e)              //edge dofs
    for(number_t i=1; i<=nbds; ++i, ++it_rd)
      {
        real_t a = real_t(i)/kp, b=1.-a;
        switch(e)
          {
            case 1: (*it_rd)->coords(1, a); break;
            case 2: (*it_rd)->coords(b, 1); break;
            case 3: (*it_rd)->coords(0, b); break;
            default: (*it_rd)->coords(a, 0);
          }
      }
  if(k<2) return;
  LagrangeStdSegment segPkm1(findInterpolation(_Lagrange, _standard, k-1, H1));
  LagrangeStdSegment segPkm2(findInterpolation(_Lagrange, _standard, k-2, H1));
  real_t s1=1./kp, s2=real_t(k)/kp;
  real_t t1=1./k, t2=real_t(k-1)/k;
  number_t k1=(k-1)*k;
  std::vector<RefDof*>::iterator it1, it2;
  for(it1=segPkm1.refDofs.begin(); it1!=segPkm1.refDofs.end(); ++it1)
    {
      std::vector<real_t>::const_iterator itp1=(*it1)->coords();
      real_t x=*itp1; x= (1-x)*s1+x*s2;
      for(it2=segPkm2.refDofs.begin(); it2!=segPkm2.refDofs.end(); ++it2, ++it_rd)
        {
          std::vector<real_t>::const_iterator itp2=(*it2)->coords();
          real_t y=*itp2; y= (1-y)*t1+y*t2;
          (*it_rd)->coords(x,y);
          (*(it_rd+k1))->coords(y,x);
        }
    }
}

//dof's edge numbering
void NedelecEdgeFirstQuadranglePk::sideNumbering()
{
  trace_p->push("NedelecEdgeFirstQuadranglePk::sideNumbering");
  number_t nbds=nbDofsInSides_/4;
  number_t n=nbDofsInSideOfSides_+1;
  sideDofNumbers_.resize(4,std::vector<number_t>(nbds,0));
  for(number_t s = 0; s < 4; ++s)          // nbds dofs by side (edge)
    for(number_t i=0; i<nbds; ++i, ++n)
      sideDofNumbers_[s][i] = n;
  trace_p->pop();
}


/* tool for global dofs construction to ensure correct matching of dofs on side (see buildFeDofs in FeSpace class)
   return the new index m from a given one n on local side numbers i,j,k
   m corresponds to the index dof when edge vertices are in ascending order
       when i<j<k (no permutation of edge) index is unchanged (m=n)
   reverse 2 dof blocks
*/
number_t NedelecEdgeFirstQuadranglePk::sideDofsMap(const number_t& n, const number_t& i, const number_t& j, const number_t& k) const
{
  number_t d=interpolation_p->numtype;
  if(d==1 || i<j) return n;   //no reverse
  if(n<=2) return 3-n;        //"extremal" side dofs
  return d+3-n;               //"internal" side dofs
}

//compute shape functions using polynomial representation
void NedelecEdgeFirstQuadranglePk::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  computeShapeValuesFromShapeFunctions(it_pt,shv,withDeriv,with2Deriv);
}

/* rotation of shapevalues on edge, if dof have been permuted

   ns: vertex numbering of hexahedron
   shv: shape values
   withDerivative: if true apply rotation also to derivatives (default value: false)

*/
void NedelecEdgeFirstQuadranglePk::rotateDofs(const std::vector<number_t>& ns, ShapeValues& shv, bool withDerivative) const
{
  number_t ord =interpolation_p->numtype;
  if(ord==1) return; //first order, no rotation;

  //swap edge dofs if not ascending ege vertex numbers
  real_t t1,t2;
  number_t n1,n2, nbde = nbDofsInSides_/4;
  for(number_t e = 1; e <= 4; ++e)
    {
      switch(e)
        {
          case 1: n1=ns[0]; n2=ns[1]; break; //edge (1,2)
          case 2: n1=ns[1]; n2=ns[2]; break; //edge (2,3)
          case 3: n1=ns[2]; n2=ns[3]; break; //edge (3,4)
          default: n1=ns[3]; n2=ns[0]; break; //edge (4,1)
        }
      if(n1>n2) //swap dofs on edge e
      {
        number_t shift = 2*(e-1)*nbde;
        std::vector<real_t>::iterator itw=shv.w.begin()+shift;
        t1=*itw; t2=*(itw+1);  //store first vector
        *itw=*(itw+2); *(itw+1)=*(itw+3);
        *(itw+2)=t1; *(itw+3)=t2;
        if(withDerivative)
          for(number_t d=0; d<2; d++)
          {
            itw=shv.dw[d].begin()+shift;
            t1=*itw; t2=*(itw+1);  //store first vector
            *itw=*(itw+2); *(itw+1)=*(itw+3);
            *(itw+2)=t1; *(itw+3)=t2;
          }
          if(ord>2) //swap intern dofs (order >=3)
          {
            error("not_handled_fe","NedelecEdgeFirstHexahedronPk::rotateDofs", " intern edge dofs");
          }
        }
    }
}

//=====================================================================================
/*! Nedelec edge second family of order k on quadrangle (NE2k)
*/
//=====================================================================================
NedelecEdgeSecondQuadranglePk::NedelecEdgeSecondQuadranglePk(const Interpolation* interp_p)
  : NedelecEdgeQuadrangle(interp_p)
{
  name_ += "_second family_"+tostring(interp_p->numtype);
  error("not_yet_implemented","NedelecEdgeSecondQuadranglePk::NedelecEdgeSecondQuadranglePk()");
//   interpolationData();  // build element interpolation data
//   sideNumbering();      // local numbering of points on edges
//   pointCoordinates();   // "virtual coordinates" of moment dofs
}

NedelecEdgeSecondQuadranglePk::~NedelecEdgeSecondQuadranglePk() {}

} // end of namespace xlifepp
