/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file NedelecEdgeQuadrangle.hpp
  \authors E. Lunéville
  \since 5 march 2018
  \date  5 march 2018

  \brief Definition of the xlifepp::NedelecEdgeQuadrangle class

  xlifepp::NedelecEdgeQuadrangle defines Nedelec HCurl-conforming edge elements interpolation data on quadrangle elements
  There are 2 families:
  - the first family NEC1_k also named RTCE_k)
  - the second family NEC2_k also named BDMCE_k

   member functions
  ----------------
    - interp: interpolation data
    - sideNumbering: local numbering on edges (side)
    - sideDofsMap: side dofs mapping used when assembling
    - computeShapeFunctions: compute shape functions as polynomials (only for any order family)
    - computeShapeValues: evaluate shape functions at a point

  Child classes:
    - xlifepp::NedelecEdgeFirstQuadranglePk -> Nedelec first family of any order on quadrangle (NEC1_k)
    - xlifepp::NedelecEdgeSecondQuadranglePk -> Nedelec second family of any order on hexrahedron (NEC2_k)
*/

#ifndef NEDELEC_EDGE_QUADRANGLE_HPP
#define NEDELEC_EDGE_QUADRANGLE_HPP

#include "config.h"
#include "RefQuadrangle.hpp"

namespace xlifepp
{

/*!
  \class NedelecEdgeQuadrangle
  (Hcurl-conforming edge elements )

  Parent class: RefTriangle
  Child classes: NedelecEdgeFirstQuadranglePk, NedelecEdgeSecondQuadranglePk
 */
class NedelecEdgeQuadrangle: public RefQuadrangle
{
  public:
    NedelecEdgeQuadrangle(const Interpolation* interp_p);
    virtual ~NedelecEdgeQuadrangle();
}; // end of class NedelecEdgeQuadrangle

/*!
  \class NedelecEdgeFirstQuadranglePk
  Nedelec edge first family of any order k on quadrangle Q (NEC1_k)
     space  Vk: Q_(k-1,k) x Q_(k,k-1) with Q_(k,l) monomials of degree at most k in x1, of degree at most l in x2, 2k(k+1) dofs
     edge dofs: v-> int_e v.t q,     q in P_(k-1)[e]                 k dofs by edge e
     quadrangle dofs (k>1): v-> int_t v.q,        q in Q_(k-1,k-2) x Q_(k-2,k-1)  2k(k-1) dofs
 */
class NedelecEdgeFirstQuadranglePk: public NedelecEdgeQuadrangle
{
  public:
    NedelecEdgeFirstQuadranglePk(const Interpolation* int_p);
    ~NedelecEdgeFirstQuadranglePk();
    void interpolationData();  //!< defines reference element interpolation data
    void sideNumbering();      //!< local numbering on faces (side)
    void sideOfSideNumbering();//!< local numbering on edges (side of side)
    void pointCoordinates();   //!< builds virtual coordinates of moment dofs
    number_t sideDofsMap(const number_t& n, const number_t& i,
                         const number_t& j, const number_t& k=0) const;  //!< edge dofs mapping
    void rotateDofs(const std::vector<number_t>&, ShapeValues&, bool withDerivative=true) const;
    void computeShapeFunctions();                                        //!< compute shape functions as polynomials

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv=false) const; //!< compute shape values at a point
  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class NedelecEdgeFirstQuadranglePk


/*!
  \class NedelecEdgeSecondQuadranglePk NOT YET AVAILABLE
  Nedelec edge second family of any order k on quadrangle Q NEC2_k/BDMCE_k
 */
class NedelecEdgeSecondQuadranglePk: public NedelecEdgeQuadrangle
{
  public:
    NedelecEdgeSecondQuadranglePk(const Interpolation* int_p);
    ~NedelecEdgeSecondQuadranglePk();
//    void interpolationData();  //!< defines reference element interpolation data
//    void sideNumbering();      //!< local numbering on edges (side)
//    void pointCoordinates();   //!< builds virtual coordinates of moment dofs
//    number_t sideDofsMap(const number_t& n, const number_t& i,
//                         const number_t& j, const number_t& k=0) const;  //!< side dofs mapping
//    void computeShapeFunctions();                                        //!< compute shape functions as polynomials
//
//    void computeShapeValues(std::vector<real_t>::const_iterator it_pt,
//                            const bool withDeriv = true) const;          //!< compute shape values at a point
//  private:
//    number_t nbshfcts_;
//
//    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class NedelecEdgeSecondQuadranglePk


} // end of namespace xlifepp

#endif /* NEDELEC_EDGE_QUADRANGLE_HPP */
