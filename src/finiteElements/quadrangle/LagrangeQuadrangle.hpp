/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LagrangeQuadrangle.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 6 aug 2012

  \brief Definition of the xlifepp::LagrangeQuadrangle class

  xlifepp::LagrangeQuadrangle defines Lagrange Reference Element interpolation data on quadrangular elements

  member functions
  ----------------
    - interpolationData interpolation data
    - outputAsP1
    - pointCoordinates  point coordinates
    - vertexCoordinates coordinates of vertices
    - computeShapeValues    computation of shape functions
    - fig4TeX  TeX format output of local numbering & coordinates

  external class related functions
  --------------------------------
    - tensorNumberingQuadrangle builds correspondence between segment and quadrangle local numbering

  Local numbering of Lagrange Qk quadrangles, k=1, 2,.., 5
  ------------------------------------------

  \verbatim
    3----2  3---6---2  3--10---6---2  3--14--10---6---2  3--18--14--10---6---2
    |    |  |       |  |           |  |               |  |                   |
    4----1  7   9   5  7  15  14   9  7  19  22  18  13  7  23  30  26  22  17
     k=1    |       |  |           |  |               |  |                   |
            4---8---1 11  16  13   5 11  23  25  21   9 11  27  35  34  29  13
               k=2     |           |  |               |  |                   |
                       4---8--12---1 15  20  24  17   5 15  31  36  33  25   9
                            k=3       |               |  |                   |
                                      4---8--12--16---1 19  24  28  32  21   5
                                             k=4         |                   |
                                                         4---8--12--16--20---1
                                                                  k=5
  \endverbatim
*/

#ifndef LAGRANGE_QUADRANGLE_HPP
#define LAGRANGE_QUADRANGLE_HPP

#include "config.h"
#include "RefQuadrangle.hpp"

namespace xlifepp
{

/*!
  \class LagrangeQuadrangle
  defines Lagrange Reference Element interpolation data on quadrangles
 */
class LagrangeQuadrangle : public RefQuadrangle
{
  public:
    LagrangeQuadrangle(const Interpolation* interp_p); //!< constructor by interpolation
    virtual ~LagrangeQuadrangle(); //!< destructor

  protected:
    void interpolationData(); //!< builds interpolation data on reference element
    void sideNumbering(); //!< numbering of side D.O.F's in standard Lagrange elements
    void sideOfSideNumbering(); //!< numbering of side of side D.O.F's in standard Lagrange elements
    //internal side dofs mapping when vertices of side are permuted (k not used in 2d)
    //   standard i=1, j=2 :  [----1----2---- ... ----p-1----p----]     (p=nbDofsInSides_)
    //   permuted i=2, j=1 :  [----p----p-1---- ... ----2----1----]
    number_t sideDofsMap(const number_t& n, const number_t& i, const number_t& j, const number_t& k=0) const
    {
        if(i>j) return nbDofsInSides()/4-n+1;
        return n;
    }
    void pointCoordinates(); //!< builds point coordinates on reference element
    std::vector<RefDof*>::iterator vertexCoordinates(); //!< returns vector of vertices dofs
    std::vector<std::vector<number_t> > splitP1() const;  //!< return nodes numbers of first order triangle elements when splitting current element
    splitvec_t splitO1() const;  //!< return nodes numbers of first order quadrangle elements when splitting current element
    static splitvec_t splitQuad(dimen_t k, dimen_t ori); //!< recursive utility function used by previous one
    std::vector<std::pair<number_t,number_t> > splitP1Side(number_t s) const;//!< return side numbers of first order elements (P1) when splitting current element

  public:
    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv = true, const bool with2Deriv=false) const; //!< Lagrange Qk shape functions
    void outputAsP1(std::ofstream& os, const int , const std::vector<number_t>& ranks) const; //!< output of Lagrange D.o.f's numbers on underlying P1-triangle D.o.f's

  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }
    // // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //
    // #ifdef FIG4TEX_OUTPUT
    //     void fig4TeX();
    // #endif  /* FIG4TEX_OUTPUT */
    // // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //

}; // end of class LagrangeQuadrangle

/*!
  \class LagrangeStdQuadrangle
  child class to class LagrangeQuadrangle
 */
class LagrangeStdQuadrangle : public LagrangeQuadrangle
{
  public:
    LagrangeStdQuadrangle(const Interpolation* interp_p); //!< constructor by interpolation
    ~LagrangeStdQuadrangle() {} //!< destructor
    const splitvec_t& getO1splitting() const // override
     { return splitO1Scheme; }
  private:
    // storage of order 1 splitting scheme (intended to graphical softwares usage)
    splitvec_t splitO1Scheme;

}; // end of class LagrangeStdQuadrangle

/*!
  \class LagrangeGLQuadrangle
  child class to class LagrangeQuadrangle
 */
class LagrangeGLQuadrangle : public LagrangeQuadrangle
{
  public:
    LagrangeGLQuadrangle(const Interpolation* interp_p); //!< constructor by interpolation
    ~LagrangeGLQuadrangle() {} //!< destructor
    const splitvec_t& getO1splitting() const // override
     { return splitO1Scheme; }
  private:
    // storage of order 1 splitting scheme (intended to graphical softwares usage)
    splitvec_t splitO1Scheme;

}; // end of class LagrangeGLQuadrangle

//=================================================================================
// External class related functions
//=================================================================================
/*!
  correspondence between segment and quadrangle local numbering

  point number p has coordinates defined by ( x = coords[s2q[2*p]], y = coords[s2q[2*p+1]] )
  where coords are coordinates of the associated 1D Lagrange reference element
  and shape functions are defined by tensor product of shape functions of the 1D reference element
      w2_p(x,y) = w1_{s2q[2*p]}(x) * w1_{s2q[2*p+1]}(y)
  where w1_k is the k-th shape function of the associated 1D Lagrange reference element
 */
void tensorNumberingQuadrangle(const int, std::vector<number_t>& s2q);

} // end of namespace xlifepp

#endif /* LAGRANGE_QUADRANGLE_HPP */
