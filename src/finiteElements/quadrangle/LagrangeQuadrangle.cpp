/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LagrangeQuadrangle.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 17 dec 2002
  \date 6 aug 2012

  \brief Implementation of xlifepp::LagrangeQuadrangle class members and related functions
 */

#include "LagrangeQuadrangle.hpp"
#include "../Interpolation.hpp"
#include "utils.h"

#include <iostream>

namespace xlifepp
{

//! LagrangeQuadrangle constructor for Lagrange reference elements
LagrangeQuadrangle::LagrangeQuadrangle(const Interpolation* interp_p)
  : RefQuadrangle(interp_p)
{
  name_ += "_Lagrange";
  trace_p->push("LagrangeQuadrangle::LagrangeQuadrangle (" + name_ + ")");
  // build element interpolation data
  interpolationData();
  // local numbering of points on side of sides
  sideOfSideNumbering();
  // local numbering of points on sides
  sideNumbering();
  // Reference Element on edges
  sideRefElement();
  maxDegree = 2*interpolation_p->numtype;
  trace_p->pop();
}

LagrangeQuadrangle::~LagrangeQuadrangle() {}

//! interpolationData defines Reference Element interpolation data
void LagrangeQuadrangle::interpolationData()
{
  number_t interpNum = interpolation_p->numtype;

  switch(interpNum)
    {
      case 0:
        nbDofs_ = nbInternalDofs_ = 1;
        break;
      // Serendipity case ??: // Lagrange quadrangle Q2 serendipity element
      // Serendipity    nbDofs_= 8
      // Serendipity    nbDofsOnVertices_ = 4;
      // Serendipity    break;
      default: // Lagrange quadrangle of order > 0
        nbDofs_ = (interpNum + 1) * (interpNum + 1);
        nbInternalDofs_ = (interpNum - 1) * (interpNum - 1);
        nbDofsOnVertices_ = 4;
        nbDofsInSides_ = nbDofs_ - nbInternalDofs_ - nbDofsOnVertices_;
        break;
    }

  // Creating Degrees Of Freedom of reference element
  // in Lagrange standard (H1-conforming) elements all D.o.F on edges are sharable
  refDofs.reserve(nbDofs_);
  lagrangeRefDofs(2, nbDofsOnVertices_, nbInternalDofs_, 4, nbDofsInSides_);
  // lagrangeRefDofs(1, nbDofsOnVertices_, nbDofs_ - nbInternalDofs_, 2);

  // compute number of shape functions
  nbshfcts_ = shapeValueSize();
  // Lagrange Quadrangle Q2 serendipity element
  //   {
  //      nbDofs_=8;
  //   }
  nbPts_ = nbDofs_;
}


//! create standard Lagrange numbering on sides
void LagrangeQuadrangle::sideNumbering()
{
  number_t interpNum = interpolation_p->numtype;
  /*
  if(interpNum==0)   //Q0 interpolation
    {
      for(number_t side = 0; side < geomRefElem_p->nbSides(); side++)
        {
          sideDofNumbers_[side].resize(1);
          sideDofNumbers_[side][0]=1;
        }
      return;
    }
  */
number_t nbSides = geomRefElem_p->nbSides();
sideDofNumbers_.resize(nbSides);
if(interpNum==0) //P0 interpolation
{
for(number_t side = 0; side < nbSides; side++)
{
sideDofNumbers_[side].resize(1);
sideDofNumbers_[side][0]=1;
}
return;
}


  if(interpNum > 0)
    {
      number_t intN(interpNum);
      // if(interpNum == _P1BubbleP3) { intN = 1; }
      number_t nbVertices = geomRefElem_p->nbVertices();
      number_t nbSides = geomRefElem_p->nbSides();
      number_t more;
      number_t nbVerticesPerSide = geomRefElem_p->sideVertexNumbers()[0].size();
      number_t nbDofsPerSide = intN + 1;

      sideDofNumbers_.resize(nbSides);
      for(number_t side = 0; side < nbSides; side++)
        {
          sideDofNumbers_[side].resize(nbDofsPerSide);
          for(number_t j = 0; j < nbVerticesPerSide; j++)
            {
              sideDofNumbers_[side][j] = geomRefElem_p->sideVertexNumber(j + 1, side + 1);
            }
          if(intN > 1)
            {
              more = nbVertices + 1;
              for(number_t k = nbVerticesPerSide; k < nbDofsPerSide; k++, more += nbSides)
                {
                  sideDofNumbers_[side][k] = side + more;
                }
            }
        }
    }
}

//! create standard Lagrange numbering on side of sides
void LagrangeQuadrangle::sideOfSideNumbering()
{
  number_t interpNum = interpolation_p->numtype;

  if(interpNum > 0)
    {
      number_t nbSideOfSides = geomRefElem_p->nbSideOfSides();
      number_t nbVerticesPerSideOfSide = geomRefElem_p->sideOfSideVertexNumbers()[0].size();

      sideOfSideDofNumbers_.resize(nbSideOfSides);
      for(number_t i = 0; i < nbSideOfSides; i++)
        {
          sideOfSideDofNumbers_[i].resize(nbVerticesPerSideOfSide);
          for(number_t j = 0; j < nbVerticesPerSideOfSide; j++)
            {
              sideOfSideDofNumbers_[i][j] = geomRefElem_p->sideOfSideVertexNumber(j + 1, i + 1);
            }
        }
    }
}

//! pointCoordinates defines Lagrange Reference Element point coordinates
std::vector<RefDof*>::iterator LagrangeQuadrangle::vertexCoordinates()
{
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();
  (*it_rd++)->coords(1., 0.);
  (*it_rd++)->coords(1., 1.);
  (*it_rd++)->coords(0., 1.);
  (*it_rd++)->coords(0., 0.);
  return it_rd;
}

void LagrangeQuadrangle::pointCoordinates()
{
  /*
    Examples of local numbering of Lagrange Qk quadrangles, k=1,2,3,4,5

    3------2   3-----6-----2   3--10---6---2   3--14--10---6---2   3--18--14--10---6---2
    |      |   |           |   |           |   |               |   |                   |
    |      |   |           |   7  15  14   9   7  19  22  18  13   7  23  30  26  22  17
    4------1   7     9     5   |           |   |               |   |                   |
    interp=1   |           |  11  16  13   5  11  23  25  21   9  11  27  35  34  29  13
               |           |   |           |   |               |   |                   |
               4-----8-----1   4---8--12---1  15  20  24  17   5  15  31  36  33  25   9
               interpNum = 2   interpNum = 3   |               |   |                   |
                                               4---8--12--16---1  19  24  28  32  21   5
                                                interpNum = 4      |                   |
                                                                   4---8--12--16--20---1
                                                                      interpNum = 5
   */

  int interpNum = interpolation_p->numtype;
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();
  switch(interpNum)
    {
      case 0:
        (*it_rd)->coords(.5, .5);
        break;
      default: // Lagrange quadrangle of degree > 0
        it_rd = vertexCoordinates();
        if(interpNum > 1)
          {
            // correspondence between segment and quadrangle local numbering
            std::vector<number_t> s2q(2 * nbPts_);
            tensorNumberingQuadrangle(interpNum, s2q);

            number_t pt = 2 * geomRefElem_p->nbVertices();
            // find 1d reference element associated with any edge
            RefElement* sideRef = sideRefElems_[0];
            while(it_rd != refDofs.end())
              {
                real_t x = *(sideRef->refDofs[s2q[pt++]]->coords());
                real_t y = *(sideRef->refDofs[s2q[pt++]]->coords());
                (*it_rd++)->coords(x, y);
              }
          }
    }
}

/*
  Examples of local numbering of Lagrange Qk quadrangles, k=1,2,3,4,5

  3------2   3-----6-----2   3--10---6---2   3--14--10---6---2   3--18--14--10---6---2
  |      |   |           |   |           |   |               |   |                   |
  |      |   |           |   7  15  14   9   7  19  22  18  13   7  23  30  26  22  17
  4------1   7     9     5   |           |   |               |   |                   |
  interp=1   |           |  11  16  13   5  11  23  25  21   9  11  27  35  34  29  13
             |           |   |           |   |               |   |                   |
             4-----8-----1   4---8--12---1  15  20  24  17   5  15  31  36  33  25   9
             interpNum = 2   interpNum = 3   |               |   |                   |
                                             4---8--12--16---1  19  24  28  32  21   5
                                               interpNum = 4     |                   |
                                                                 4---8--12--16--20---1
                                                                     interpNum = 5
  */
//! return nodes numbers of first order triangle elements when splitting current element
std::vector<std::vector<number_t> > LagrangeQuadrangle::splitP1() const
{
  std::vector<std::vector<number_t> > splitnum;
  std::vector<number_t> tuple(3,0);
  switch(interpolation_p->numtype)
    {
      case 0:
      case 1:
        tuple[0]=1; tuple[1]=3; tuple[2]=4;
        splitnum.push_back(tuple);
        tuple[0]=3; tuple[1]=1; tuple[2]=2;
        splitnum.push_back(tuple);
        return splitnum;
      case 2:
        tuple[0]=8; tuple[1]=7; tuple[2]=4;
        splitnum.push_back(tuple);
        tuple[0]=7; tuple[1]=8; tuple[2]=9;
        splitnum.push_back(tuple);
        tuple[0]=1; tuple[1]=9; tuple[2]=8;
        splitnum.push_back(tuple);
        tuple[0]=9; tuple[1]=1; tuple[2]=5;
        splitnum.push_back(tuple);
        tuple[0]=9; tuple[1]=3; tuple[2]=7;
        splitnum.push_back(tuple);
        tuple[0]=3; tuple[1]=9; tuple[2]=6;
        splitnum.push_back(tuple);
        tuple[0]=5; tuple[1]=6; tuple[2]=9;
        splitnum.push_back(tuple);
        tuple[0]=6; tuple[1]=5; tuple[2]=2;
        splitnum.push_back(tuple);
        return splitnum;
      default:
        return splitLagrange2DToP1();
    }
  return splitnum;
}

// creates the list of nodes numbers of first order quadrangle when splitting 2D Lagrange element
// Recursive algorithm based on the definition of the numbering rule over a quadrangle.
splitvec_t LagrangeQuadrangle::splitQuad(dimen_t k, dimen_t ori)
{
  splitvec_t splitnum;
  switch(k)
    {
      case 1:
        {
          int quad[] = {ori+1, ori+2, ori+3, ori+4};
          splitnum.push_back(make_pair(_quadrangle, std::vector<number_t>(quad, quad+4)));
        } break;
      case 2:
        {
          int quad[] = {ori+1, ori+5, ori+9, ori+8};
          splitnum.push_back(make_pair(_quadrangle, std::vector<number_t>(quad, quad+4)));
        }
        {
          int quad[] = {ori+2, ori+6, ori+9, ori+5};
          splitnum.push_back(make_pair(_quadrangle, std::vector<number_t>(quad, quad+4)));
        }
        {
          int quad[] = {ori+3, ori+7, ori+9, ori+6};
          splitnum.push_back(make_pair(_quadrangle, std::vector<number_t>(quad, quad+4)));
        }
        {
          int quad[] = {ori+4, ori+8, ori+9, ori+7};
          splitnum.push_back(make_pair(_quadrangle, std::vector<number_t>(quad, quad+4)));
        }
        break;
      default:
        // treat inside of the quadrangle first
        splitnum = splitQuad(k-2, ori+4*k);
        // then proceed with the boundary quadrangles
        {
          number_t n = ori+4*k, cn[] = {n+2, n+3, n+4, n+1}, s2 = n, s3 = s2+1;
          for(number_t num=1; num<5; num++)   // k-1 quadrangles along each side
            {
              number_t s1 = ori+num, s4 = s2; s2 = s1+4;
              number_t quad[] = {s1, s2, s3, s4}; // first one
              splitnum.push_back(make_pair(_quadrangle, std::vector<number_t>(quad, quad+4)));
              for(number_t j=0; j<k-3; j++)    // k-3 others
                {
                  s2 += 4; s3 += 4;
                  number_t quad[] = {s2-4, s2, s3, s3-4};
                  splitnum.push_back(make_pair(_quadrangle, std::vector<number_t>(quad, quad+4)));
                }
              s2 += 4; s4 = s3; s3 = cn[num-1];
              {
                number_t quad[] = {s2-4, s2, s3, s4}; // last one
                splitnum.push_back(make_pair(_quadrangle, std::vector<number_t>(quad, quad+4)));
              }
            }
        } break;
    }
  return splitnum;
}

//! return nodes numbers of first order quadrangle elements of same shape when splitting current element
splitvec_t LagrangeQuadrangle::splitO1() const
{
  return splitQuad(interpolation_p->numtype, 0);
}
/*
  splitvec_t splitnum;
  std::vector<number_t> tuple(4,0);
  switch (interpolation_p->numtype)
  {
    case 1:
      tuple[0] = 1; tuple[1]=2; tuple[2]=3; tuple[3]=4;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      return splitnum;
    case 2:
      tuple[0]=8; tuple[1]=9; tuple[2]=7; tuple[3]=4;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=1; tuple[1]=5; tuple[2]=9; tuple[3]=8;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=9; tuple[1]=6; tuple[2]=3; tuple[3]=7;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=5; tuple[1]=2; tuple[2]=6; tuple[3]=9;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      return splitnum;
    case 3:
      tuple[0]=8; tuple[1]=16; tuple[2]=11; tuple[3]=4;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=12; tuple[1]=13; tuple[2]=16; tuple[3]=8;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=1; tuple[1]=5; tuple[2]=13; tuple[3]=12;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=16; tuple[1]=15; tuple[2]=7; tuple[3]=11;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=13; tuple[1]=14; tuple[2]=15; tuple[3]=16;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=5; tuple[1]=9; tuple[2]=14; tuple[3]=13;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=15; tuple[1]=10; tuple[2]=3; tuple[3]=7;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=14; tuple[1]=6; tuple[2]=10; tuple[3]=15;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=9; tuple[1]=2; tuple[2]=6; tuple[3]=14;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      return splitnum;
    case 4:
      tuple[0]=8; tuple[1]=20; tuple[2]=15; tuple[3]=4;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=12; tuple[1]=24; tuple[2]=20; tuple[3]=8;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=16; tuple[1]=17; tuple[2]=24; tuple[3]=12;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=1; tuple[1]=5; tuple[2]=17; tuple[3]=16;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=20; tuple[1]=23; tuple[2]=11; tuple[3]=15;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=24; tuple[1]=25; tuple[2]=23; tuple[3]=20;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=17; tuple[1]=21; tuple[2]=25; tuple[3]=24;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=5; tuple[1]=9; tuple[2]=21; tuple[3]=17;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=23; tuple[1]=19; tuple[2]=7; tuple[3]=11;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=25; tuple[1]=22; tuple[2]=19; tuple[3]=23;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=21; tuple[1]=18; tuple[2]=22; tuple[3]=25;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=9; tuple[1]=13; tuple[2]=18; tuple[3]=21;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=19; tuple[1]=14; tuple[2]=3; tuple[3]=7;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=22; tuple[1]=10; tuple[2]=14; tuple[3]=19;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=18; tuple[1]=6; tuple[2]=10; tuple[3]=22;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=13; tuple[1]=2; tuple[2]=6; tuple[3]=18;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      return splitnum;
    case 5:
      tuple[0]=8; tuple[1]=24; tuple[2]=19; tuple[3]=4;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=12; tuple[1]=28; tuple[2]=24; tuple[3]=8;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=16; tuple[1]=32; tuple[2]=28; tuple[3]=12;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=20; tuple[1]=21; tuple[2]=32; tuple[3]=16;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=1; tuple[1]=5; tuple[2]=21; tuple[3]=20;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=24; tuple[1]=31; tuple[2]=15; tuple[3]=19;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=28; tuple[1]=36; tuple[2]=31; tuple[3]=24;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=32; tuple[1]=33; tuple[2]=36; tuple[3]=28;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=21; tuple[1]=25; tuple[2]=33; tuple[3]=32;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=5; tuple[1]=9; tuple[2]=25; tuple[3]=21;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=31; tuple[1]=27; tuple[2]=11; tuple[3]=15;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=36; tuple[1]=35; tuple[2]=27; tuple[3]=31;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=33; tuple[1]=34; tuple[2]=35; tuple[3]=36;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=25; tuple[1]=29; tuple[2]=34; tuple[3]=33;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=9; tuple[1]=13; tuple[2]=29; tuple[3]=25;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=27; tuple[1]=23; tuple[2]=7; tuple[3]=11;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=35; tuple[1]=30; tuple[2]=23; tuple[3]=27;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=34; tuple[1]=26; tuple[2]=30; tuple[3]=35;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=29; tuple[1]=22; tuple[2]=26; tuple[3]=34;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=13; tuple[1]=17; tuple[2]=22; tuple[3]=29;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=23; tuple[1]=18; tuple[2]=3; tuple[3]=7;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=30; tuple[1]=14; tuple[2]=18; tuple[3]=23;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=26; tuple[1]=10; tuple[2]=14; tuple[3]=30;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=22; tuple[1]=6; tuple[2]=10; tuple[3]=26;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=17; tuple[1]=2; tuple[2]=6; tuple[3]=22;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      return splitnum;
    case 6:
      tuple[0]=8; tuple[1]=28; tuple[2]=23; tuple[3]=4;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=12; tuple[1]=32; tuple[2]=28; tuple[3]=8;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=16; tuple[1]=36; tuple[2]=32; tuple[3]=12;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=20; tuple[1]=40; tuple[2]=36; tuple[3]=16;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=24; tuple[1]=25; tuple[2]=40; tuple[3]=20;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=1; tuple[1]=5; tuple[2]=25; tuple[3]=24;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=28; tuple[1]=39; tuple[2]=19; tuple[3]=23;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=32; tuple[1]=44; tuple[2]=39; tuple[3]=28;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=36; tuple[1]=48; tuple[2]=44; tuple[3]=32;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=40; tuple[1]=41; tuple[2]=48; tuple[3]=36;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=25; tuple[1]=29; tuple[2]=41; tuple[3]=40;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=5; tuple[1]=9; tuple[2]=29; tuple[3]=25;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=39; tuple[1]=35; tuple[2]=15; tuple[3]=19;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=44; tuple[1]=47; tuple[2]=35; tuple[3]=39;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=48; tuple[1]=49; tuple[2]=47; tuple[3]=44;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=41; tuple[1]=45; tuple[2]=49; tuple[3]=48;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=29; tuple[1]=33; tuple[2]=45; tuple[3]=41;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=9; tuple[1]=13; tuple[2]=33; tuple[3]=29;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=35; tuple[1]=31; tuple[2]=11; tuple[3]=15;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=47; tuple[1]=43; tuple[2]=31; tuple[3]=35;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=49; tuple[1]=46; tuple[2]=43; tuple[3]=47;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=45; tuple[1]=42; tuple[2]=46; tuple[3]=49;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=33; tuple[1]=37; tuple[2]=42; tuple[3]=45;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=13; tuple[1]=17; tuple[2]=37; tuple[3]=33;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=31; tuple[1]=27; tuple[2]=7; tuple[3]=11;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=43; tuple[1]=38; tuple[2]=27; tuple[3]=31;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=46; tuple[1]=34; tuple[2]=38; tuple[3]=43;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=42; tuple[1]=30; tuple[2]=34; tuple[3]=46;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=37; tuple[1]=26; tuple[2]=30; tuple[3]=42;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=17; tuple[1]=21; tuple[2]=26; tuple[3]=37;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=27; tuple[1]=22; tuple[2]=3; tuple[3]=7;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=38; tuple[1]=18; tuple[2]=22; tuple[3]=27;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=34; tuple[1]=14; tuple[2]=18; tuple[3]=38;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=30; tuple[1]=10; tuple[2]=14; tuple[3]=34;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=26; tuple[1]=6; tuple[2]=10; tuple[3]=30;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      tuple[0]=21; tuple[1]=2; tuple[2]=6; tuple[3]=26;
      splitnum.push_back(std::make_pair(_quadrangle,tuple));
      return splitnum;
    default:
      return splitLagrange2DToQ1();
  }
  return splitnum;
}*/

/*! for a side of current element, return the list of (first order element number, side number) of the first order elements (P1)
  as a list of pair<P1 element number, P1 side number>

  \verbatim
          s2
        3----2      Side 1 :  Elt 2 (3 1 2), side 2 (1,2) -->  (2,2)
        |\   |      Side 2 :  Elt 2 (3 1 2), side 3 (2 3) -->  (2,3)
 Q1   s3|  \ |s1    Side 3 :  Elt 1 (1 3 4), side 2 (3 4) -->  (1,2)
        4----1      Side 4 :  Elt 1 (1 3 4), side 3 (4 1) -->  (1,3)
          s4
              s2
        3-----6-----2         Side 1 -> (4,2) (8,2)
        | \ 6 | \  8|
        | 5 \ | 7 \ |         Side 2 -> (6,3) (8,3)
 Q2  s3 7-----9-----5 s1
        | \  2| \  4|         Side 3 -> (1,2) (5,2)
        | 1 \ | 3 \ |
        4-----8-----1         Side 4 -> (1,3) (3,3)
              s4
  \endverbatim
*/
std::vector<std::pair<number_t,number_t> > LagrangeQuadrangle::splitP1Side(number_t side) const
{
  if(side<1 || side>4) { error("split_bad_side_num",side); }
  std::vector<std::pair<number_t,number_t> > sidenum;
  switch(interpolation_p->numtype)
    {
      case 0:
      case 1:
        if(side==1)      { sidenum.push_back(std::pair<number_t,number_t>(2,2)); }
        else if(side==2) { sidenum.push_back(std::pair<number_t,number_t>(2,3)); }
        else if(side==3) { sidenum.push_back(std::pair<number_t,number_t>(1,2)); }
        else              { sidenum.push_back(std::pair<number_t,number_t>(1,3)); }
        return sidenum;
      case 2:
        if(side==1)
          {
            sidenum.push_back(std::pair<number_t,number_t>(4,2));
            sidenum.push_back(std::pair<number_t,number_t>(8,2));
          }
        else if(side==2)
          {
            sidenum.push_back(std::pair<number_t,number_t>(6,3));
            sidenum.push_back(std::pair<number_t,number_t>(8,3));
          }
        else if(side==3)
          {
            sidenum.push_back(std::pair<number_t,number_t>(1,2));
            sidenum.push_back(std::pair<number_t,number_t>(5,2));
          }
        else
          {
            sidenum.push_back(std::pair<number_t,number_t>(1,3));
            sidenum.push_back(std::pair<number_t,number_t>(3,3));
          }
        return sidenum;
      default: error("split_low_deg_Lagrange_elt", words("shape",_triangle), 2, words("shape",_triangle));
    }
  return sidenum;
}

//! output of Lagrange D.o.f's numbers on underlying P1 mesh D.o.f's
void LagrangeQuadrangle::outputAsP1(std::ofstream& os, const int refNum, const std::vector<number_t>& ranks) const
{
  int interpNum = interpolation_p->numtype;
  /*
    Numbering of d.o.f is made starting with the outtermost layer of elements
    For instance first and second triangles are {13,12,1} and {5,13,1} for interpNum=3
    (edges of which are part of the edges of the original quadrangle)
    then recursively to the next inner layer
   */
  int pr(0), pr_1(-1), corner_1(1), corner, p1, p2, p3, p4;
  while(interpNum > 2)
    {
      // first outtermost lower right corner: 13 for interpNum=3, 17 for interpNum=4, 21 for interpNum=5
      corner_1 += 4 * interpNum;
      corner = corner_1 - pr;
      // part vertical layer parallel and close to { x_1 = 1 }
      p1 = corner_1 - 1; p2 = 1; p3 = p2 + 4; p4 = corner;
      for(int j = 1; j < interpNum; j++)
        {
          simplexVertexOutput(os, refNum, ranks[p1 + pr_1], ranks[p2 + pr_1], ranks[p4 + pr_1]);
          simplexVertexOutput(os, refNum, ranks[p3 + pr_1], ranks[p4 + pr_1], ranks[p2 + pr_1]);
          p1 = p4; p2 += 4; p3 = p2 + 4;
          if(j == interpNum - 2) { corner += 1; p4 = corner; }
          else { p4 += 4; }
        }

      // part vertical layer parallel and close to { x_2 = 1 }
      p1 = corner; p2 = p1 - 5; p3 = 2; p4 = p3 + 4;
      for(int j = 1; j < interpNum; j++)
        {
          simplexVertexOutput(os, refNum, ranks[p1 + pr_1], ranks[p2 + pr_1], ranks[p4 + pr_1]);
          simplexVertexOutput(os, refNum, ranks[p3 + pr_1], ranks[p4 + pr_1], ranks[p2 + pr_1]);
          p3 = p4; p2 = p1; p4 = p3 + 4;
          if(j == interpNum - 2) { corner += 1; p1 = corner; }
          else { p1 += 4; }
        }

      // part vertical layer parallel and close to { x_1 = 0 }
      p2 = corner; p3 = p2 - 5; p4 = 3; p1 = p4 + 4;
      for(int j = 1; j < interpNum; j++)
        {
          simplexVertexOutput(os, refNum, ranks[p1 + pr_1], ranks[p2 + pr_1], ranks[p4 + pr_1]);
          simplexVertexOutput(os, refNum, ranks[p3 + pr_1], ranks[p4 + pr_1], ranks[p2 + pr_1]);
          p3 = p2; p4 = p1; p1 = p4 + 4;
          if(j == interpNum - 2) { corner += 1; p2 = corner; }
          else { p2 += 4; }
        }

      // part vertical layer parallel and close to { x_2 = 0 }
      p1 = 4; p2 = p1 + 4; p3 = corner; p4 = p3 - 5;
      for(int j = 1; j < interpNum; j++)
        {
          simplexVertexOutput(os, refNum, ranks[p1 + pr_1], ranks[p2 + pr_1], ranks[p4 + pr_1]);
          simplexVertexOutput(os, refNum, ranks[p3 + pr_1], ranks[p4 + pr_1], ranks[p2 + pr_1]);
          p1 = p2; p4 = p3; p2 = p1 + 4;
          if(j == interpNum - 2) { corner = corner_1 - pr; p3 = corner; }
          else { p3 += 4; }
        }
      pr = corner_1 - 1; pr_1 = pr - 1; interpNum -= 2;
    }

  // finally consider the two cases for innermost quadrangle(s)
  if(interpNum == 1)
    {
      /*
        3-------2       3-------2
        |       |       | \  #2 |
        |       |  -->  |   \   |
        |       |       | #1  \ |
        4-------1       4-------1
       */
      simplexVertexOutput(os, refNum, ranks[4 + pr_1], ranks[1 + pr_1], ranks[3 + pr_1]);
      simplexVertexOutput(os, refNum, ranks[2 + pr_1], ranks[3 + pr_1], ranks[1 + pr_1]);
    }
  else if(interpNum == 2)
    {
      /*
        3-------6-------2       3-------6-------2
        |               |       | \  #5 | #4  / |
        |               |       |   \   |   /   |
        |               |       | #6  \ | /  #3 |
        7       9       5  -->  7-------9-------5
        |               |       | #7  / | \  #2 |
        |               |       |   /   |   \   |
        |               |       | /  #8 | #1  \ |
        4-------8-------1       4-------8-------1
       */
      simplexVertexOutput(os, refNum, ranks[8 + pr_1], ranks[1 + pr_1], ranks[9 + pr_1]);
      simplexVertexOutput(os, refNum, ranks[1 + pr_1], ranks[5 + pr_1], ranks[9 + pr_1]);
      simplexVertexOutput(os, refNum, ranks[5 + pr_1], ranks[2 + pr_1], ranks[9 + pr_1]);
      simplexVertexOutput(os, refNum, ranks[2 + pr_1], ranks[6 + pr_1], ranks[9 + pr_1]);
      simplexVertexOutput(os, refNum, ranks[6 + pr_1], ranks[3 + pr_1], ranks[9 + pr_1]);
      simplexVertexOutput(os, refNum, ranks[3 + pr_1], ranks[7 + pr_1], ranks[9 + pr_1]);
      simplexVertexOutput(os, refNum, ranks[7 + pr_1], ranks[4 + pr_1], ranks[9 + pr_1]);
      simplexVertexOutput(os, refNum, ranks[4 + pr_1], ranks[8 + pr_1], ranks[9 + pr_1]);
    }
  else
    {
      noSuchFunction("outputAsP1");
    }
}

/*!
  standard Lagrange Q_k quadrangle Reference Element shape functions ((k+1)^2 nodes)
  build using standard Lagrange P_k 1D Reference Element shape functions (k+1 nodes)
  at standard (equidistant on edges) points or Gauss-Lobatto abscissae.
  Second derivative NOT MANAGED !
 */
void LagrangeQuadrangle::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool der1, const bool der2) const
{
  int interpNum = interpolation_p->numtype;
  std::vector<real_t>::iterator it_w=shv.w.begin();
  std::vector< std::vector<real_t> >::iterator it_dwdx1,it_dwdx2;
  std::vector<real_t>::iterator it_dwdx1i, it_dwdx2i;
  if(der1)
  {
    it_dwdx1 =shv.dw.begin(); it_dwdx2=it_dwdx1 + 1;
    it_dwdx1i=(*it_dwdx1).begin(); it_dwdx2i=(*it_dwdx2).begin();
  }
  real_t x1 = *it_pt;
  real_t x2 = *(it_pt + 1);
  real_t x1m1 = x1 - 1;
  real_t x2m1 = x2 - 1;

  switch(interpNum)
    {
      case _P0:
        *it_w = 1.;
        if(der1) { *it_dwdx1i = 0.; *it_dwdx2i = 0.; }
        break;
      case _P1:
        // In the (x1, x2) reference plane, shape functions of the Lagrange
        // standard P1 quadrangle are given by
        // w_1(x1,x2) =   x1  (1-x2)  |  w_2(x1,x2) =   x1    x2
        // w_3(x1,x2) = (1-x1)  x2    |  w_4(x1,x2) = (1-x1)(1-x2)
        *it_w++ = -x1 * x2m1; // 0
        *it_w++ = x1 * x2;    // 1
        *it_w++ = -x1m1 * x2; // 2
        *it_w   = x1m1 * x2m1; // 3
        if(der1)
          {
            *it_dwdx1i++ = -x2m1; *it_dwdx2i++ = -x1; // 0
            *it_dwdx1i++ = x2;   *it_dwdx2i++ = x1;   // 1
            *it_dwdx1i++ = -x2;   *it_dwdx2i++ = -x1m1; // 2
            *it_dwdx1i   = x2m1; *it_dwdx2i   = x1m1; // 3
          }
        break;
      case _P2:
        {
          real_t dx1m1=2*x1-1, dx2m1=2*x2-1, ax1=x1*dx1m1, ax2=x2*dx2m1,
                 bx1=x1m1*dx1m1, bx2=x2m1*dx2m1, cx1=-x1*x1m1, cx2=-x2*x2m1;
          *it_w++ = ax1*bx2; *it_w++ = ax1*ax2; *it_w++ = bx1*ax2;
          *it_w++ = bx1*bx2; *it_w++ = 4*ax1*cx2; *it_w++ = 4*cx1*ax2;
          *it_w++ = 4*bx1*cx2; *it_w++ = 4*cx1*bx2; *it_w++ = 16*cx1*cx2;
          if(der1)
            {
              real_t dax1 = 4*x1-1, dax2 = 4*x2-1, dbx1 = 4*x1-3,
                            dbx2 = 4*x2-3, dcx1 = 1-2*x1, dcx2 = 1-2*x2;
              *it_dwdx1i++ = dax1*bx2; *it_dwdx2i++ =ax1*dbx2;
              *it_dwdx1i++ = dax1*ax2; *it_dwdx2i++ =ax1*dax2;
              *it_dwdx1i++ = dbx1*ax2; *it_dwdx2i++ =bx1*dax2;
              *it_dwdx1i++ = dbx1*bx2; *it_dwdx2i++ =bx1*dbx2;
              *it_dwdx1i++ = 4*dax1*cx2; *it_dwdx2i++ =4*ax1*dcx2;
              *it_dwdx1i++ = 4*dcx1*ax2; *it_dwdx2i++ =4*cx1*dax2;
              *it_dwdx1i++ = 4*dbx1*cx2; *it_dwdx2i++ =4*bx1*dcx2;
              *it_dwdx1i++ = 4*dcx1*bx2; *it_dwdx2i++ =4*cx1*dbx2;
              *it_dwdx1i++ = 16*dcx1*cx2; *it_dwdx2i++ =16*cx1*dcx2;
            }
        }
        break;
      default:
        // 1D shape functions
        RefElement* refSeg = sideRefElems_[0];
        ShapeValues shv0(*refSeg,der1,der2), shv1(shv0);
        refSeg->computeShapeValues(it_pt++, shv0, der1, der2);
        refSeg->computeShapeValues(it_pt  , shv1, der1, der2);
        std::vector<real_t>&  shape_0 = shv0.w;
        std::vector<real_t>& dshape_0 = shv0.dw[0];
        std::vector<real_t>&  shape_1 = shv1.w;
        std::vector<real_t>& dshape_1 = shv1.dw[0];

        // correspondence between segment and quadrangle local numbering
        std::vector<number_t> s2q(2 * (interpNum + 1) * (interpNum + 1));
        tensorNumberingQuadrangle(interpNum, s2q);

        // tensor product (square) of 1D shape functions
        std::vector<number_t>::iterator r_i(s2q.begin()), r_i_b;
        for(it_w = shv.w.begin(); it_w != shv.w.end(); it_w++)
          {
            *it_w = shape_0[*r_i++] * shape_1[*r_i++];
          }
        if(der1)
          {
            r_i = s2q.begin();
            for(it_dwdx1i = (*it_dwdx1).begin(); it_dwdx1i != (*it_dwdx1).end(); it_dwdx1i++, it_dwdx2i++)
              {
                r_i_b = r_i++;
                *it_dwdx1i = dshape_0[*r_i_b] *  shape_1[*r_i];
                *it_dwdx2i =  shape_0[*r_i_b] * dshape_1[*r_i++];
              }
          }
        break;
    }
}

//! Lagrange standard Qk quadrangle Reference Element
LagrangeStdQuadrangle::LagrangeStdQuadrangle(const Interpolation* interp_p)
  : LagrangeQuadrangle(interp_p)
{
  number_t kd = interp_p->numtype;
  name_ += "_" + tostring(kd);
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  // build O1 splitting scheme
  if (kd >0) splitO1Scheme = splitO1();

#ifdef FIG4TEX_OUTPUT
  // Draw a LagrangeStdQuadrangle using TeX and fig4TeX
  if(kd > 0) { fig4TeX(); }
#endif
}

//! Lagrange Gauss-Lobatto Qk quadrangle Reference Element
LagrangeGLQuadrangle::LagrangeGLQuadrangle(const Interpolation* interp_p)
  : LagrangeQuadrangle(interp_p)
{
  number_t kd = interp_p->numtype;
  name_ += "Gauss-Lobatto_" + tostring(kd);

  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  // build O1 splitting scheme
  splitO1Scheme = splitO1();

#ifdef FIG4TEX_OUTPUT
  // Draw a LagrangeGLQuadrangle using TeX and fig4TeX
  if(kd > 0) { fig4TeX(); }
#endif
}


//! tensorNumberingQuadrangle: correspondence between segment and quadrangle local numbering
void tensorNumberingQuadrangle(const int interpNum, std::vector<number_t>& s2q)
{
  /*
    Examples of local numbering of Lagrange Pk 1D elements

    2----1  2---3---1  2---4---3---1  2---5---4---3---1  2---6---5---4---3---1
    k=1      k=2            k=3              k=4                  k=5

    Examples of local numbering of Lagrange Qk quadrangles

    3----2  3---6---2  3--10---6---2  3--14--10---6---2  3--18--14--10---6---2
    |    |  |       |  |           |  |               |  |                   |
    4----1  7   9   5  7  15  14   9  7  19  22  18  13  7  23  30  26  22  17
     k=1    |       |  |           |  |               |  |                   |
    ......  4---8---1 11  16  13   5 11  23  25  21   9 11  27  35  34  29  13
               k=2     |           |  |               |  |                   |
    .................  4---8--12---1 15  20  24  17   5 15  31  36  33  25   9
                            k=3       |               |  |                   |
    ................................  4---8--12--16---1 19  24  28  32  21   5
                                             k=4         |                   |
    ...................................................  4---8--12--16--20---1
                                                                 k=5
   */

  std::vector<number_t>::iterator p = s2q.begin();
  int nk = interpNum;
  number_t q1 = 0, q2 = 1, q3 = 2, q4 = interpNum, q3_p, q4_m;

  while(nk > 0)
    {
      // 'current perimeter' vertices
      (*p++) = q1; (*p++) = q2;
      (*p++) = q1; (*p++) = q1;
      (*p++) = q2; (*p++) = q1;
      (*p++) = q2; (*p++) = q2;

      // non-vertex points on edges of current 'perimeter'
      q3_p = q3; q4_m = q4;
      for(dimen_t j = 2; j <= nk; j++)
        {
          (*p++) = q1; (*p++) = q4_m;
          (*p++) = q3_p; (*p++) = q1;
          (*p++) = q2; (*p++) = q3_p++;
          (*p++) = q4_m--; (*p++) = q2;
        }

      q1 += 1; q2--; q3 += 1; q4--;
      if(nk == interpNum) { q1++ ; q2 = interpNum; }
      nk -= 2;
    }

  // central point if any
  if(nk == 0) { (*p++) = q1; *p = q1; }

}

// // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //
// #ifdef FIG4TEX_OUTPUT
// //! fig4TeX builds a tex file to display local numbering and coordinates
// void LagrangeQuadrangle::fig4TeX() {
//  number_t nbVertices = geomRefElem_p->nbVertices();
//  char b=char(92);

//  std::ofstream os;
//  os.open((fig4TexFileName()).c_str(), ios::out);
//  os <<"%"<<b<< "input fig4tex.tex"<<b<<"input TeXnicolor.tex"<<b<<"input Fonts.tex";
//  os <<std::endl<<b<<"fourteenpoint"<<b<<"nopagenumbers";
//  os <<std::endl<<b<<"newbox"<<b<<"mybox";
//  if ( nbDofs(1, 1) < 6 ) os <<std::endl<<b<<"figinit{150pt}";
//  else os <<std::endl<<b<<"figinit{250pt}";
//  string longName = "Lagrange Quadrangle of degree ";

//  // Draw element and display vertices
//  number_t pts(0);

//  // define vertices
//  fig4TeXVertexPt(os, geomRefElem_p->vertices(), pts);

//  // start postscript file
//  os <<std::endl<<b<<"psbeginfig{}"<<std::endl<<b<<"pssetwidth{1}"<<b<<"psline[";
//  for ( size_t p = 1; p <= nbVertices; p++ ) os << p <<",";
//  os <<"1]";
//  os <<std::endl<<b<<"psendfig";  // end of postcript output

//  os <<std::endl<<b<<"figvisu{"<<b<<"mybox}{"<< longName << interpolation_p->number() <<"}{%"
//      <<std::endl<<b<<"figsetmark{$"<<b<<"bullet$}"<<b<<"figsetptname{{"<<b<<"bf#1}}";
//  // vertex nodes
//  os <<std::endl<<b<<"figwritese1:(5pt)"
//      <<std::endl<<b<<"figwritene2:(5pt)"
//      <<std::endl<<b<<"figwritenw3:(5pt)"
//      <<std::endl<<b<<"figwritesw4:(5pt)";

//  if ( nbDofs(1, 1) <= 7 ) {
//    // edge vertices
//    os <<std::endl<<b<<"figsetmark{}"
//      <<std::endl<<b<<"color{"<<b<<"Red}"
//      <<std::endl<<b<<"figwritegw1:${}_2$(1pt,7pt)"<<std::endl<<b<<"figwritegw1:${}_1$(7pt,1pt)"
//      <<std::endl<<b<<"figwritegw2:${}_1$(1pt,-7pt)"<<std::endl<<b<<"figwritegw2:${}_2$(7pt,-1pt)"
//      <<std::endl<<b<<"figwritege3:${}_2$(1pt,-7pt)"<<std::endl<<b<<"figwritege3:${}_1$(7pt,-1pt)"
//      <<std::endl<<b<<"figwritege4:${}_1$(1pt,7pt)"<<std::endl<<b<<"figwritege4:${}_2$(7pt,1pt)";
//      os <<std::endl<<b<<"endcolor";
//  }

//  // nodes on edge 1 (east), edge 2 (north), edge 3 (west), edge 4 (south)
//  number_t ed(0);
//  fig4TeXEdgePt(os , ed++, pts, "e", "w");
//  fig4TeXEdgePt(os , ed++, pts, "n", "s");
//  fig4TeXEdgePt(os , ed++, pts, "w", "e");
//  fig4TeXEdgePt(os , ed++, pts, "s", "n");

//  // internal nodes
//  if ( nbInternalDofs_ > 0 ) fig4TeXInternalPoints2d(os, pts);

//  // close figvisu and TeX file
//  os <<std::endl<<"}" <<b<<"par"<<b<<"centerline{"<<b<<"box"<<b<<"mybox}";
//  os <<std::endl<<b<<"bye";
//  os.close();
// }
// #endif  /* FIG4TEX_OUTPUT */
// // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //

} // end of namespace xlifepp
