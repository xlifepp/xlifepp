/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomRefQuadrangle.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 6 aug 2012

  \brief Definition of the xlifepp::GeomRefQuadrangle class

  xlifepp::GeomRefQuadrangle defines Reference Element geometric data on quadrangular elements

  Numbering of vertices and edges

  \verbatim
              II
        3------------2
        |            |
        |            |
    III |            | I
        |            |
        |            |
        4------------1
              IV
  \endverbatim

  sideNumbering local numbering of vertices on edges
 */

#ifndef GEOM_REF_QUADRANGLE_HPP
#define GEOM_REF_QUADRANGLE_HPP

#include "config.h"
#include "../GeomRefElement.hpp"

namespace xlifepp
{

/*!
  \class GeomRefQuadrangle
  child to class GeomRefElement
 */
class GeomRefQuadrangle : public GeomRefElement
{
  public:
    //! default constructor
    GeomRefQuadrangle();
    //! destructor
    ~GeomRefQuadrangle();

    real_t measure(const dimen_t dim, const number_t sideNum = 0) const; //!< returns edge length or element area
    // //! returns a quadrature rule built on edge sideNum (=1,2,3,4) from a 1d quadrature formula
    //  void sideQuadrature(const QuadratureRule&, QuadratureRule&, const number_t, const dimen_t = 1) const;

    //! returns tangent std::vector on edge sideNum (=1,2,3,4)
    void tangentVector(const std::vector<real_t>& jacobianMatrix, std::vector< std::vector<real_t> >& tgv, const number_t, const dimen_t = 1) const;
    //! returns a vertex opposite to edge number sideNum (=1,2, ...)
    number_t vertexOppositeSide(const number_t s) const { return 1+(s+1)%4;}
    //! returns edge or face opposite to vertex number v
    number_t sideOppositeVertex(const number_t) const { noSuchFunction("sideOppositeVertex");  return 0;}
    number_t edgeOppositeEdge(const number_t) const; //!< returns local number of edge opposite to edge given by its local number
    number_t sideWithVertices(const number_t, const number_t) const; //!< returns local number of edge bearing vertices with local numbers v1 and v2
    //! returns local number of edge bearing vertices with local numbers v1 and v2
    number_t sideWithVertices(const number_t, const number_t, const number_t, const number_t = 0) const { noSuchFunction("sideWithVertices");  return 0;}

    bool contains(std::vector<real_t>& p, real_t tol= theTolerance) const;  //!< test if a point belongs to current element
    std::vector<real_t> projection(const std::vector<real_t>& p, real_t& h) const; //!< return projection on ref quadrangle
    //! node numbers defining first simplex of ref element
    std::vector<number_t> simplexNodes() const
    {std::vector<number_t> sn(3,0); sn[0]=4;sn[1]=1;sn[2]=3;return sn;}

  private:
    void sideNumbering(); //!< local numbers of vertices on sides
    void sideOfSideNumbering(); //!< local numbers of vertices on side of sides
}; // end of class GeomRefQuadrangle

} // end of namespace xlifepp

#endif /* GEOM_REF_QUADRANGLE_HPP */
