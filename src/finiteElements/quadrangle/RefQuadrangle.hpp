/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RefQuadrangle.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 16 may 2012

  \brief Definition of the xlifepp::RefQuadrangle class

  xlifepp::RefQuadrangle defines Reference Element interpolation data on quadrangular elements
 */

#ifndef REF_QUADRANGLE_HPP
#define REF_QUADRANGLE_HPP

#include "config.h"
#include "../RefElement.hpp"

namespace xlifepp
{

class Interpolation;

/*!
  \class RefQuadrangle

  Parent class: RefElement
  Child classes: LagrangeQuadrangle
 */
class RefQuadrangle : public RefElement
{
  public:
    //! constructor by interpolation
    RefQuadrangle(const Interpolation* interp_p) : RefElement(_quadrangle, interp_p) {}
    //! virtual destructor
    virtual ~RefQuadrangle() {}

    virtual void interpolationData() = 0; //!< returns interpolation data
    virtual void outputAsP1(std::ofstream& os, const int, const std::vector<number_t>&) const { noSuchFunction("outputAsP1"); }
    const splitvec_t& getO1splitting() const // override
       { return RefElement::getO1splitting(); }

}; // end of class RefQuadrangle

} // end of namespace xlifepp

#endif /* REF_QUADRANGLE_HPP */
