/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RefQuadrangle.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 16 dec 2002
  \date 04 nov 2010

  \brief Implementation of xlifepp::RefQuadrangle class members and related functions
 */

#include "RefQuadrangle.hpp"
#include "LagrangeQuadrangle.hpp"
#include "NedelecEdgeQuadrangle.hpp"
#include "../Interpolation.hpp"
#include "utils.h"

namespace xlifepp
{

//! selectRefQuadrangle construction of a Reference Element  by interpolation type and interpolation subtype
RefElement* selectRefQuadrangle(const Interpolation* interp_p)
{
  switch ( interp_p->type)
  {
    case Lagrange:
      switch ( interp_p->subtype)
      {
        case _standard: return new LagrangeStdQuadrangle(interp_p); break;
        case _GaussLobattoPoints: return new LagrangeGLQuadrangle(interp_p); break;
        default: interp_p->badSubType(_quadrangle); break;
      }
      break;
      case _Nedelec:
      case _NedelecEdge:
      switch (interp_p->subtype)
      {
        case _firstFamily: return new NedelecEdgeFirstQuadranglePk(interp_p); break;
        default: interp_p->badSubType(_quadrangle); break;
      }
      break;
    case _RaviartThomas:  interp_p->badSubType(_quadrangle); break;
    default: break;
  }

  trace_p->push("selectRefQuadrangle");
  interp_p->badType(_quadrangle);
  trace_p->pop();
  return nullptr;
}

} // end of namespace xlifepp
