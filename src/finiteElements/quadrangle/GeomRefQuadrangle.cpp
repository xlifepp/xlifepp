/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomRefQuadrangle.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 15 dec 2002
  \date 6 aug 2012

  \brief Implementation of xlifepp::GeomRefQuadrangle class members and related functions
 */

#include "GeomRefQuadrangle.hpp"
#include "utils.h"

namespace xlifepp
{

//! GeomRefQuadrangle constructor for Geometric Reference Element using 2d base constructor with shape, surface , centroid coords, number of vertices
GeomRefQuadrangle::GeomRefQuadrangle()
  : GeomRefElement(_quadrangle, 1., 0.5, 4)
{
  trace_p->push("GeomRefQuadrangle::GeomRefQuadrangle");
  // coordinates of vertices
  std::vector<real_t>::iterator it_v(vertices_.begin());
  vertex(it_v, 1., 0.);
  vertex(it_v, 1., 1.);
  vertex(it_v, 0., 1.);
  vertex(it_v, 0., 0.);
  // vertex numbering on sides
  sideNumbering();
  // vertex numbering on side of sides
  sideOfSideNumbering();

  trace_p->pop();
}

GeomRefQuadrangle::~GeomRefQuadrangle() {}

//! sideNumbering defines Geometric Reference Element local numbering of vertices on sides
void GeomRefQuadrangle::sideNumbering()
{
  for (number_t i = 0; i < nbSides_; i++)
  {
    sideShapeTypes_[i] = _segment;
  }
  sideVertexNumbers_[0].push_back(1);
  sideVertexNumbers_[0].push_back(2);
  sideVertexNumbers_[1].push_back(2);
  sideVertexNumbers_[1].push_back(3);
  sideVertexNumbers_[2].push_back(3);
  sideVertexNumbers_[2].push_back(4);
  sideVertexNumbers_[3].push_back(4);
  sideVertexNumbers_[3].push_back(1);
}

//! sideOfSideNumbering defines Geometric Reference Element local numbering of vertices on side of sides
void GeomRefQuadrangle::sideOfSideNumbering()
{
  sideOfSideVertexNumbers_[0].push_back(2);
  sideOfSideVertexNumbers_[1].push_back(1);
  sideOfSideVertexNumbers_[2].push_back(3);
  sideOfSideVertexNumbers_[3].push_back(4);

  sideOfSideNumbers_[0].push_back(1);
  sideOfSideNumbers_[0].push_back(2);
  sideOfSideNumbers_[1].push_back(3);
  sideOfSideNumbers_[1].push_back(1);
  sideOfSideNumbers_[2].push_back(4);
  sideOfSideNumbers_[2].push_back(3);
  sideOfSideNumbers_[3].push_back(2);
  sideOfSideNumbers_[3].push_back(4);
}

//! Returns edge length or element area
real_t GeomRefQuadrangle::measure(const dimen_t d, const number_t sideNum) const
{
  real_t ms(0.);
  switch ( d )
  {
    case 0: ms = 1.; break;
    case 1: ms = 1.; break;
    case 2: ms = measure_;
    default: break;
  }
  return ms;
}
//
//! Returns a quadrature rule build on an edge form a 1d quadrature formula
//void GeomRefQuadrangle::sideQuadrature(const QuadratureRule& q1, QuadratureRule& qr, const number_t sideNum, const dimen_t d) const {
//   /*
//      q1 : input 1d  quadrature rule
//      q2 : ouput 2d quadrature rule set on the edge
//      sideNum: local number of edge (sideNum = 1,2,3,4)
//      d: = 1 !
//   */
//   std::vector<real_t>::const_iterator c_1(q1.point()), w_1(q1.weight());
//   std::vector<real_t>::iterator c_i(qr.point()), w_i(qr.weight());
//
//   switch ( d )
//   {
//      case 1:
//         switch ( sideNum )
//         {
//            case 1: // edge x = 1
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1++ )
//                  qr.point(c_i, 1., *c_1, w_i, *w_1 );
//               break;
//            case 2: // edge y = 1
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1++ )
//                  qr.point(c_i, *c_1, 1., w_i, *w_1 );
//               break;
//            case 3: // edge x = 0
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1++ )
//                  qr.point(c_i, 0., *c_1, w_i, *w_1 );
//               break;
//            case 4: // edge y = 0
//               for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1++ )
//                  qr.point(c_i, *c_1, 0., w_i, *w_1 );
//               break;
//            default: noSuchEdge(sideNum); break;
//         }
//         break;
//      default:
//         break;
//   }
//}

//! returns tangent std::vector on a edge number sideNum
void GeomRefQuadrangle::tangentVector(const std::vector<real_t>& jacobianMatrix, std::vector <std::vector<real_t> >& tgv, const number_t sideNum, const dimen_t d) const
{
  std::vector<real_t>::iterator tv_i(tgv[0].begin());
  std::vector<real_t>::const_iterator jm_i(jacobianMatrix.begin());

  switch ( d )
  {
    case 1:
      switch ( sideNum )
      {
        case 1: // edge x = 1
          for ( jm_i = jacobianMatrix.begin(); jm_i != jacobianMatrix.end(); jm_i += 2 ) { *tv_i++ = *(jm_i + 1); }
          break;
        case 2: // edge y = 1
          for ( jm_i = jacobianMatrix.begin(); jm_i != jacobianMatrix.end(); jm_i += 2 ) { *tv_i++ = -*jm_i;}
          break;
        case 3: // edge x = 0
          for ( jm_i = jacobianMatrix.begin(); jm_i != jacobianMatrix.end(); jm_i += 2 ) { *tv_i++ = -*(jm_i + 1);}
          break;
        case 4: // edge y = 0
          for ( jm_i = jacobianMatrix.begin(); jm_i != jacobianMatrix.end(); jm_i += 2 ) { *tv_i++ = *jm_i;}
          break;
        default: noSuchSide(sideNum); break;
      }
      break;
    default:
      *tv_i++ = 0; *tv_i = 0.;
      break;
  }
}

//! Returns the local number of an edge bearing 2 vertices given by their local numbers (>=1)
number_t GeomRefQuadrangle::sideWithVertices(const number_t vn1, const number_t vn2) const
{
  if(vn1==vn2) noSuchSide(vn1,vn2);
  number_t v1=vn1, v2=vn2;
  if(v1>v2) {v1=vn2;v2=vn1;}
  if(v1==1)
  {
      if(v2==2) return 1;
      if(v2==4) return 4;
      noSuchSide(vn1,vn2);
  }
  if(v1==2 && v2==3) return 2;
  if(v1==3 && v2==4) return 3;
  noSuchSide(vn1,vn2);
  return 0;
}

// //! Returns the local number of an edge opposed to an edge given by its local number
// number_t GeomRefQuadrangle::edgeOppositeEdge(const number_t edgeNum) const {
//   // here edgeNum is the local number of edge (edgeNum = 1,2,3)
//   if ( edgeNum < 3 ) return edgeNum+2; else return edgeNum-2;
// }

//! test if a point belongs to current element
bool GeomRefQuadrangle::contains(std::vector<real_t>& p, real_t tol) const
{
    real_t x=p[0], y=p[1];
    return (x >= -tol) && (x <= 1.+tol) && (y >= -tol) && (y <= 1.+tol);
}

//! return projection on ref hexahedron
std::vector<real_t> GeomRefQuadrangle::projection(const std::vector<real_t>& p, real_t& h) const
{
  real_t x=p[0], y=p[1];
  real_t tol=theTolerance;
  bool in = true;
  std::vector<real_t> q(2,0.);
  if(x<=-tol) {q[0]=0.; in=false;}
  else if(x>=1+tol) {q[0]=1.; in=false;}
  else q[0]=x;
  if(y<=-tol) {q[1]=0;; in=false;}
  else if(y>=1+tol) {q[1]=1;; in=false;}
  else q[1]=y;
  if(in) h=0; else h=norm(p-q);
  return q;
}

} // end of namespace xlifepp
