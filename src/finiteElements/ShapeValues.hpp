/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ShapeValues.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 01 jul 2005
  \date 10 may 2012

  \brief Definition of the xlifepp::ShapeValues class

  This class is used to create storage for finite element computation such as
    - Shape values at a given point (e.g. quadrature point)
    - Derivatives of Shape functions values stored in a vector<vector<real_t> dw as
      dw[0][i] stands for dx(wi), dw[1][i] stands for dy(wi) and dw[2][i] stands for dz(wi)
    - Second Derivatives of Shape functions values stored in a vector<vector<real_t> d2w as
      d2w[0][i] stands for dxx(wi), d2w[1][i] stands for dyy(wi) and d2w[2][i] stands for dxy(wi)
      d2w[3][i] stands for dzz(wi), d2w[4][i] stands for dxz(wi) and d2w[5][i] stands for dyz(wi)

       w = [w1_x w1_y w1_z w2_x w2_y w2_z ... wn_x wn1_y wn_z]
      dw = [dxw1_x dxw1_y dxw1_z dxw2_x dxw2_y dxw2_z ... dxwn_x dxwn_y dxwn_z] = dw[0]
           [dyw1_x dyw1_y dyw1_z dyw2_x dyw2_y dyw2_z ... dywn_x dywn_y dywn_z] = dw[1]
      (3D) [dzw1_x dzw1_y dzw1_z dzw2_x dzw2_y dzw2_z ... dzwn_x dzwn_y dzwn_z] = dw[2]
     d2w = [dxxw1_x dxxw1_y dxxw1_z dxxw2_x dxxw2_y dxxw2_z ... dxxwn_x dxxwn_y dxxwn_z] = d2w[0]
           [dyyw1_x dyyw1_y dyyw1_z dyyw2_x dyyw2_y dyyw2_z ... dyywn_x dyywn_y dyywn_z] = d2w[1]
           [dxyw1_x dxyw1_y dxyw1_z dxyw2_x dxyw2_y dxyw2_z ... dxywn_x dxywn_y dxywn_z] = d2w[2]
      (3D) [dzzw1_x dzzw1_y dzzw1_z dzzw2_x dzzw2_y dzzw2_z ... dzzwn_x dzzwn_y dzzwn_z] = d2w[3]
           [dxzw1_x dxzw1_y dxzw1_z dxzw2_x dxzw2_y dxzw2_z ... dxzwn_x dxzwn_y dxzwn_z] = d2w[4]
           [dyzw1_x dyzw1_y dyzw1_z dyzw2_x dyzw2_y dyzw2_z ... dyzwn_x dyzwn_y dyzwn_z] = d2w[5]

    - Mapping shape functions derivatives from Reference Element onto current element
*/

#ifndef SHAPE_VALUES_HPP
#define SHAPE_VALUES_HPP

#include "config.h"
#include "../geometry/GeomMapData.hpp"

namespace xlifepp
{

class RefElement;
class GeomMapData;

/*!
  \class ShapeValues
 */
class ShapeValues
{
  public:
    std::vector<real_t> w;                  //!< shape functions at a given point
    std::vector< std::vector<real_t> > dw;  //!< derivatives of shape functions at a given point
    std::vector< std::vector<real_t> > d2w; //!< 2nd derivatives of shape functions at a given point (dxx,dyy,dxy,[dzz,dxz, dyz])

    //--------------------------------------------------------------------------------
    // Constructors and Destructor
    //--------------------------------------------------------------------------------
    ShapeValues();                                 //!< default constructor
    ShapeValues(const RefElement&, bool der1=true, bool der2=false);  //!< constructor with associated ReferenceElement size
// Unused    ShapeValues(const RefElement&, const dimen_t); //!< constructor with associated ReferenceElement characteristics and space dimension

    //--------------------------------------------------------------------------------
    // Public member functions
    //--------------------------------------------------------------------------------
    //! returns number of shape functions
    dimen_t dim() const { return static_cast<dimen_t>(dw.size()); }
    //! returns number of dofs
    number_t nbDofs() const { return static_cast<number_t>(w.size());}
    //! returns true if empty
    bool isEmpty() const {return w.size()==0;}

// Unused    void resize(const RefElement&, const dimen_t);      //!< defines shape functions size
    void resize(const number_t, const dimen_t,bool der1, bool der2); //!< resize shape value vectors
    void set(const real_t);                             //!< set all shape functions to const value
    void extendToVector(dimen_t d);                     //!< extend scalar shape function values to vector shape function values
    void assign(const ShapeValues&);                    //!< fast assign of shapevalues into current shapevalues

    //shapevalues mapping
    void map(const ShapeValues&, const GeomMapData&, bool der1, bool der2);                   //!< maps Reference Element shape functions onto current Geometric Element
    void contravariantPiolaMap(const ShapeValues&, const GeomMapData&, bool der1, bool der2); //!< maps Reference Element shape functions onto current Geometric Element (contravariant Piola)
    void covariantPiolaMap(const ShapeValues&, const GeomMapData&, bool der1, bool der2);     //!< maps Reference Element shape functions onto current Geometric Element (covariant Piola)
    void Morley2dMap(const ShapeValues&, const GeomMapData&, bool der1, bool der2);           //!< maps Reference Element shape functions onto current Geometric Element (Morley)
    void Argyris2dMap(const ShapeValues& rsv, const GeomMapData& gd, bool der1, bool der2);   //!< maps Reference Element shape functions onto current Geometric Element (Argyris)
    void changeSign(const std::vector<real_t>&, dimen_t, bool der1, bool der2);               //!< change sign of shape functions according to a sign vector

    friend std::ostream& operator<<(std::ostream&, const ShapeValues&);

}; // end of class ShapeValues

std::ostream& operator<<(std::ostream&, const ShapeValues&); //!< print operator
void Argyris2dMap(const std::vector<real_t>& rw, std::vector<real_t>& w,
                  real_t j11, real_t j12, real_t j21, real_t j22,
                  real_t t11, real_t t12, real_t t13, real_t t21, real_t t22, real_t t23, real_t t31, real_t t32, real_t t33,
                  real_t a1, real_t a2, real_t a3, real_t b1, real_t b2, real_t b3, real_t c1, real_t c2, real_t c3, real_t d1, real_t d2, real_t d3,
                  real_t n1x, real_t n1y, real_t n2x, real_t n2y, real_t n3x, real_t n3y);

// fast assign of shape values into current shape values
inline void ShapeValues::assign(const ShapeValues& shv)
{
  const std::vector<real_t>& wc=shv.w;
  std::vector<real_t>::iterator it;
  if(w.size()==wc.size()) //same size
  {
    it=w.begin();
    std::vector<real_t>::const_iterator itc=wc.begin();
    for(;it!=w.end() && itc!=wc.end();++it, ++itc) *it=*itc;
  }
  else w=shv.w;

  const std::vector< std::vector<real_t> >& dwc=shv.dw;
  std::vector<real_t>::const_iterator itc;
  if(dw.size()==dwc.size()) //same size
  {
    std::vector< std::vector<real_t> >::iterator itd=dw.begin();
    std::vector< std::vector<real_t> >::const_iterator itdc=dwc.begin();
    for(;itd!=dw.end() && itdc!=dwc.end();++itd, ++itdc)
    {
        it=itd->begin(); itc=itdc->begin();
        for(;it!=itd->end();++it,++itc) *it=*itc;
    }
  }
  else dw=shv.dw;

  const std::vector< std::vector<real_t> >& d2wc=shv.d2w;
  if(d2w.size()==d2wc.size()) //same size
  {
    std::vector< std::vector<real_t> >::iterator itd=d2w.begin();
    std::vector< std::vector<real_t> >::const_iterator itdc=d2wc.begin();
    for(;itd!=d2w.end() && itdc!=d2wc.end();++itd, ++itdc)
    {
        it=itd->begin(); itc=itdc->begin();
        for(;it!=itd->end();++it,++itc) *it=*itc;
    }
  }
  else d2w=shv.d2w;
  return;
}

} // end of namespace xlifepp

#endif /* SHAPE_VALUES_HPP */
