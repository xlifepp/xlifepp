/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Interpolation.hpp
  \authors D. Martin, E. Lunéville
  \since 5  jun 2011
  \date 10 may 2012

  \brief Definition of the xlifepp::Interpolation class

  Class xlifepp::Interpolation carries finite element interpolation characteristics

  NOTE: if they do not already exist, the interpolations are created during the construction
  of a FE space (see class Space) by calling the external function:

  Interpolation* findInterpolation(FEType, FESubType, number_t, SpaceType)
*/


#ifndef INTERPOLATION_HPP
#define INTERPOLATION_HPP

#include "config.h"
#include "utils.h"
#include <vector>

namespace xlifepp
{

/*!
  \class Interpolation
 */
class Interpolation
{
  public:
    FEType type;              //!< interpolation type (Lagrange, Hermite, CrouzeixRaviart, Nedelec, RaviartThomas, ...)
    FESubType subtype;        //!< interpolation subtype (standard, GaussLobatto, firstFamily, ...)
    number_t numtype;         //!< conventional number defining the element interpolation (polynomial degree in case of Lagrange interpolation)
    SobolevType conformSpace; //!< conforming space type (L2, H1, Hdiv, Hcurl, H2)
    string_t name;            //!< one among "Lagrange", "Hermite", "Nedelec", "Crouzeix-Raviart", "Raviart-Thomas", ...
    string_t subname;         //!< interpolation subname
    string_t shortname;       //!< short name (see build)

    // Interpolation(const Interpolation&); //!< no copy constructor
    void build();                        //!< called by constructor

    static std::vector<Interpolation*> theInterpolations;    //!< unique list of run-time Interpolation pointers
    Interpolation(FEType typ, FESubType sub, number_t num, SobolevType spa); //!< main constructor
    ~Interpolation();                                        //!< destructor

    static void clearGlobalVector(); //!< delete all Interpolation objects
    static void printAllInterpolations(std::ostream&);  //!< print the list of Interpolation objects in memory
    static void printAllInterpolations(PrintStream& os) {printAllInterpolations(os.currentStream());}
    // Public Access functions (accessors)
    string_t conformSpaceName() const; //!< return the conforming space

    // Public members
    bool operator==(const Interpolation& i) const; //!< returns true if *this==i
    bool isContinuous() const;       //!< returns continuity of interpolation
    bool isScalar() const;           //!< true if DoFs are scalar
    number_t maximumDegree() const;  //!< returns maximum polynomial degree involved in interpolation space
    void print(std::ostream&) const; //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());} //!< print utility

    //@{
    //! Error handlers
    void badType() const;
    void badSubType() const;
    void badSpace() const;
    void badType(const number_t) const;
    void badSubType(const number_t) const;
    void badDegree(const number_t) const;
    //@}

    friend std::ostream& operator<<(std::ostream&, const Interpolation&);
}; // end of class Interpolation

// extern function to the class
std::ostream& operator<<(std::ostream&, const Interpolation&); //!< print operator

//! build interpolation data from InterpolationType
void buildInterpolationData(InterpolationType interpType, number_t dim, FEType& typ, FESubType& sub, number_t& num, SobolevType& spa);

Interpolation* findInterpolation(FEType typ, FESubType sub, number_t num, SobolevType spa); //!< main "constructor" by finding first if already exists
Interpolation* findInterpolation(InterpolationType interpType, number_t dim);               //!< main "constructor" by finding first if already exists
Interpolation& interpolation(FEType typ, FESubType sub, number_t num, SobolevType spa);     //!< main "constructor" by finding first if already exists

typedef PCollection<Interpolation> Interpolations;  //!< collection of Interpolation pointers

} // end of namespace xlifepp

#endif // INTERPOLATION_HPP
