/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file CrouzeixRaviartTriangle.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 23 nov 2004
  \date 6 aug 2012

  \brief Implementation of xlifepp::CrouzeixRaviartTriangle class members and related functions
 */

#include "CrouzeixRaviartTriangle.hpp"
#include "../Interpolation.hpp"
#include "utils.h"
#include <iostream>

namespace xlifepp
{

//! CrouzeixRaviartTriangle constructor for CrouzeixRaviart reference elements
CrouzeixRaviartTriangle::CrouzeixRaviartTriangle(const Interpolation* interp_p)
  : RefTriangle(interp_p)
{
  name_ += "_Crouzeix-Raviart";
  trace_p->push("CrouzeixRaviartTriangle::CrouzeixRaviartTriangle (" + name_ + ")");
  // build element interpolation data
  interpolationData();
  // local numbering of points on edges
  sideNumbering();
  // *NO* Reference Element on edges
  // side_interpolations_ = edge_interpolations_;
  maxDegree = 1;

  trace_p->pop();
}

CrouzeixRaviartTriangle::~CrouzeixRaviartTriangle() {}

//! interp defines Reference Element interpolation data
void CrouzeixRaviartTriangle::interpolationData()
{
  trace_p->push("CrouzeixRaviartTriangle::interpolationData");
  number_t intN = interpolation_p->numtype;

  switch ( intN )
  {
    case _P1:
      nbDofsInSides_ = nbDofs_ = 3;
      break;
    default:
      error("interp_order_unexpected",1, intN);
      break;
  }

  // Creating Degrees Of Freedom of reference element
  // in CrouzeixRaviart standard elements all D.o.F on edges are sharable
  // number of internal nodes (or D.o.F)

  refDofs.reserve(nbDofs_);
  for(number_t s = 1; s <= 3; s++ )
       refDofs.push_back(new RefDof(this, true, _onEdge, s, 1, 2, 0, s, 1, 0, _noProjection, _id, "nodal value"));
  //lagrangeRefDofs(1, 0, nbDofs_, 2);
  // compute number of shape functions
  nbshfcts_ = shapeValueSize();

  nbPts_ = nbDofs_;

  trace_p->pop();
}

//! sideNumbering defines Reference Element local numbers on sides
void CrouzeixRaviartTriangle::sideNumbering()
{
  trace_p->push("CrouzeixRaviartTriangle::sideNumbering");

  /*
    Local numbering of CrouzeixRaviart triangles

      *
      | \
      2   1
      |     \
      *---3---*
      .. k=1
   */
  number_t intN = interpolation_p->numtype;
  number_t nbSides = geomRefElem_p->nbSides();
  sideDofNumbers_.resize(nbSides);
  if ( intN == 1 )
  {
    //    number_t dof_Ed(1);
    for ( number_t side = 0; side < nbSides; side++ )
    {
      //      edge_interpolations_.push_back(new ReferenceSideInterpolation(interpolation_p, dof_Ed, dof_Ed));
      //      edge_interpolations_[ed]->type(NoInterp);
      sideDofNumbers_[side].push_back(side + 1);
    }
  }
  trace_p->pop();
}

//! CrouzeixRaviart standard P1 triangle Reference Element
CrouzeixRaviartStdTriangleP1::CrouzeixRaviartStdTriangleP1(const Interpolation* interp_p)
  : CrouzeixRaviartTriangle(interp_p)
{
  name_ += "_1";
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
}

CrouzeixRaviartStdTriangleP1::~CrouzeixRaviartStdTriangleP1() {}

//! pointCoordinates defines CrouzeixRaviart Reference Element point coordinates
void CrouzeixRaviartStdTriangleP1::pointCoordinates()
{
  trace_p->push("CrouzeixRaviartStdTriangleP1::pointCoordinates");
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();
  (*it_rd++)->coords(.5, .5);
  (*it_rd++)->coords(0., .5);
  (*it_rd)->coords(.5, 0.);
  trace_p->pop();
}

void CrouzeixRaviartStdTriangleP1::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x1 = *it_pt, x2 = *(it_pt + 1);
  std::vector<real_t>::iterator it_w(shv.w.begin());
  *it_w++ = 2.*(x1 + x2) - 1.; // w_1
  *it_w++ = 1. - 2.*x1; // w_2
  *it_w   = 1. - 2.*x2; // w_3

  if ( withDeriv )
  {
    std::vector< std::vector<real_t> >::iterator it_dwdx1(shv.dw.begin()), it_dwdx2(it_dwdx1 + 1);
    std::vector<real_t>::iterator it_dwdx1i((*it_dwdx1).begin()), it_dwdx2i((*it_dwdx2).begin());
    *it_dwdx1i++ = 2.; *it_dwdx2i++ = 2.; // grad w_1
    *it_dwdx1i++ = -2.; *it_dwdx2i++ = 0.; // grad w_2
    *it_dwdx1i   = 0.; *it_dwdx2i   = -2.; // grad w_3
  }
}

} // end of namespace xlifepp
