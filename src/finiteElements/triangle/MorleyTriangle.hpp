/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file MorleyTriangle.hpp
  \authors E. Lunéville
  \since 30 apr 2021
  \date  30 apr 2021

  \brief Definition of the xlifepp::MorleyTriangle class
         xlifepp::MorleyTriangle defines Morley non conforming H2 element interpolation data on triangular elements
           (Mi) vertices of triangle, ni normal vector to edge i (opposite to Mi), Ni middle of edge i
           space: P2 ; dofs: p(Mi), i=1,2,3,  grad(p)(Ni).ni i=1,2,3
           neither C1 nor C0 (C0 only at vertices)! Whereas is not, it is considered as a H1 comforming element

                  2
                  |\   /n1
             n2   | \ /
             -----4  6       (ordre Kirby)
                  |   \
                  |    \
                  3--5--1
                     |
                     |n3

   member functions
  ----------------
    - interp: interpolation data
    - sideNumbering: local numbering on edges (side)
    - sideDofsMap: side dofs mapping used when assembling
    - computeShapeValues: evaluate shape functions at a point
*/

#ifndef MORLEY_TRIANGLE_HPP
#define MORLEY_TRIANGLE_HPP

#include "config.h"
#include "RefTriangle.hpp"

namespace xlifepp
{
 class GeomMapData;  // forward class

/*!
  \class MorleyTriangle
  (non conforming H2 element)
  Parent class: RefTriangle
 */
class MorleyTriangle : public RefTriangle
{
  public:
    MorleyTriangle(const Interpolation* interp_p);
    MorleyTriangle();
    virtual ~MorleyTriangle();
    void interpolationData(); //!< builds interpolation data
    void sideNumbering();     //!< numbering of side D.O.F's
    void pointCoordinates();  //!< builds virtual coordinates of moment dofs
    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv = true, const bool with2Deriv=true) const;
    number_t nbShapeFcts() const
     {return 6;}
    Matrix<real_t> affineMap(const GeomMapData&) const; //!< create the affine transformation matrix mapping reference shape function to any triangle
}; // end of class MorleyTriangle

} // end of namespace xlifepp

#endif /* MORLEY_TRIANGLE_HPP */
