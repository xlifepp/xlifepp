/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RefTriangle.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 17 dec 2002
  \date 6 aug 2012

  \brief Definition of the xlifepp::RefTriangle class

  xlifepp::RefTriangle defines Reference Element interpolation data on triangular elements

  External class related functions
    - triangleLagrangeStd: construction of a Lagrange standard triangular Reference Element
    - triangleHermiteStd: construction of a Hermite standard triangular Reference Element
    - triangleCrouzeixRaviartStd: construction of a Crouzeix-Raviart standard triangular Reference Element
    - triangleNedelec: construction of a Hermite standard triangular Reference Element
    - triangleRaviartThomasStd: construction of a Hermite standard triangular Reference Element
*/

#ifndef REF_TRIANGLE_HPP
#define REF_TRIANGLE_HPP

#include "config.h"
#include "../RefElement.hpp"


namespace xlifepp
{

/*!
  \class RefTriangle

  Parent class: RefElement
  Child classes: LagrangeTriangle
                  HermiteTriangle
                  CrouzeixRaviartTriangle
                  NedelecTriangle
                  RaviartThomasTriangle
 */
class RefTriangle : public RefElement
{
  public:
    //! constructor from interpolation
    RefTriangle(const Interpolation* interp_p) : RefElement(_triangle, interp_p) {}
    //! destructor
    virtual ~RefTriangle() {}

    //! returns interpolation data
    virtual void interpolationData() = 0;
    virtual void outputAsP1(std::ofstream& os, const int, const std::vector<number_t>&) const { noSuchFunction("outputAsP1"); }
    const splitvec_t& getO1splitting() const // override
       { return RefElement::getO1splitting(); }

}; // end of class RefTriangle

//=================================================================================
// Extern class related functions and declarations
//=================================================================================
RefElement* triangleLagrangeStd(const Interpolation*);
RefElement* triangleHermiteStd(const Interpolation*);
RefElement* triangleCrouzeixRaviartStd(const Interpolation*);
RefElement* triangleNedelec(const Interpolation*);
RefElement* triangleRaviartThomasStd(const Interpolation*);
RefElement* triangleMorley(const Interpolation*);
RefElement* triangleArgyris(const Interpolation*);


} // end of namespace xlifepp

#endif /* REF_TRIANGLE_HPP */
