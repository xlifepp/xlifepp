/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file NedelecTriangle.hpp
  \authors D. Martin, N. Kielbasiewicz, E. Lunéville
  \since 11 jan 2003
  \date  27 mai 2015

  \brief Definition of the xlifepp::NedelecTriangle class

  xlifepp::NedelecTriangle defines Nedelec Hcurl-conforming Edge elements interpolation data on triangular elements

   member functions
  ----------------
    - interp: interpolation data
    - sideNumbering: local numbering on edges (side)
    - sideDofsMap: side dofs mapping used when assembling
    - computeShapeFunctions: compute shape functions as polynomials (only for any order family)
    - computeShapeValues: evaluate shape functions at a point

  Child classes:
    - xlifepp::NedelecStdTriangleP1 -> Nedelec P1 standard Reference Element
    - xlifepp::NedelecFirstTrianglePk -> Nedelec first family of any order on triangle
    - xlifepp::NedelecSecondTrianglePk -> Nedelec second family of any order on triangle
*/

#ifndef NEDELEC_TRIANGLE_HPP
#define NEDELEC_TRIANGLE_HPP

#include "config.h"
#include "RefTriangle.hpp"

namespace xlifepp
{

/*!
  \class NedelecTriangle
  (Hcurl-conforming Edge elements)

  Parent class: RefTriangle
  Child classes: NedelecFirstTriangleP1, NedelecFirstTrianglePk, NedelecSecondTrianglePk
 */
class NedelecTriangle : public RefTriangle
{
  public:
    NedelecTriangle(const Interpolation* interp_p);
    std::vector<number_t> dofsMap(const number_t& i, const number_t& j,
                                  const number_t& k=0) const; //!< dofs map
    //virtual Value operator()(const FeDof*,const Function& f, const Function& gradf, const Function& grad2f) const;
    virtual ~NedelecTriangle();
}; // end of class NedelecTriangle

/*!
  \class NedelecFirstTriangleP1
  (first family)
 */
class NedelecFirstTriangleP1: public NedelecTriangle
{
  public:
    NedelecFirstTriangleP1(const Interpolation* interp_p);
    ~NedelecFirstTriangleP1();
    void interpolationData();   //!< defines reference element interpolation data
    void sideNumbering();       //!< local numbering on edges (side)
    void pointCoordinates();    //!< builds virtual coordinates of  dofs
    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv = false) const;
    //Value operator()(const FeDof*,const Function& f, const Function& gradf, const Function& grad2f) const;
  private:
    number_t nbshfcts_;
    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class NedelecStdTriangle

/*!
  \class NedelecFirstTrianglePk
  Nedelec first family of any order k on triangle T (NT1k)
     space  Vk: P^2_(k-1) + S_k,  dim Vk = k(k+2)     where S_k={p in PH_k; p.x=0}, PHk homogeneous polynomials of order k)
     edge dofs: v-> int_e v.t q,  q in P_(k-1)[e]     k dofs by edge e
     triangle dofs: v-> int_t v.q,    q in P_(k-2)[t]^2   k(k-1) dofs  only for k>1
 */
class NedelecFirstTrianglePk: public NedelecTriangle
{
  public:
    NedelecFirstTrianglePk(const Interpolation* int_p);
    ~NedelecFirstTrianglePk();
    void interpolationData();  //!< defines reference element interpolation data
    void sideNumbering();      //!< local numbering on edges (side)
    void pointCoordinates();   //!< builds virtual coordinates of moment dofs
    number_t sideDofsMap(const number_t& n, const number_t& i,
                         const number_t& j, const number_t& k=0) const;  //!< side dofs mapping
    void computeShapeFunctions();                                        //!< compute shape functions as polynomials

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv=true, const bool with2Deriv=false) const; //!< compute shape values at a point
  private:
    number_t nbshfcts_;
    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class NedelecFirstTrianglePk


/*!
  \class NedelecSecondTrianglePk
  Nedelec second family of any order k on triangle T (NT2k)
     space  Vk: P^2_(k)                            dim Vk = (k+1)(k+2)
     edge dofs: v-> int_e v.t q, q in P_(k)[e]     k+1   dofs by edge e
     triangle dofs: v-> int_t v.q,   q in D_(k-1)[t]   k^2-1 dofs  only for k>1
                     where D_(k-1)[t] = P^2_(k-2) + PH_(k-2)*x of dimension k^2-1
 */
class NedelecSecondTrianglePk: public NedelecTriangle
{
  public:
    NedelecSecondTrianglePk(const Interpolation* int_p);
    ~NedelecSecondTrianglePk();
    void interpolationData();  //!< defines reference element interpolation data
    void sideNumbering();      //!< local numbering on edges (side)
    void pointCoordinates();   //!< builds virtual coordinates of moment dofs
    number_t sideDofsMap(const number_t& n, const number_t& i,
                         const number_t& j, const number_t& k=0) const;  //!< side dofs mapping
    void computeShapeFunctions();                                        //!< compute shape functions as polynomials

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv=true, const bool with2Deriv=false) const; //!< compute shape values at a point
  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class NedelecSecondTrianglePk


} // end of namespace xlifepp

#endif /* NEDELEC_TRIANGLE_HPP */
