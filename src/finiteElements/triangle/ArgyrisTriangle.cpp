/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ArgyrisTriangle.cpp
  \authors E. Lunéville
  \since 30 apr 2021
  \date  30 apr 2021

  \brief Implementation of xlifepp::ArgyrisTriangle class members and related functions
 */

#include "ArgyrisTriangle.hpp"
#include "geometry.h"
#include "utils.h"
#include <iostream>

namespace xlifepp
{

//! ArgyrisTriangle constructor for Argyris reference elements
ArgyrisTriangle::ArgyrisTriangle(const Interpolation* interp_p)
  : RefTriangle(interp_p)
{
  name_ += "_Argyris";
  trace_p->push("ArgyrisTriangle::ArgyrisTriangle (" + name_ + ")");
  interpolationData();
  sideNumbering();
  pointCoordinates();
  mapType = _ArgyrisMap;
  dofCompatibility=_signDofCompatibility;
  maxDegree = 5;
  setMatrices();
  trace_p->pop();
}
ArgyrisTriangle::ArgyrisTriangle(): RefTriangle(findInterpolation(_Argyris, _standard, 1, H2))
{
  name_ += "_Argyris";
  trace_p->push("ArgyrisTriangle::ArgyrisTriangle (" + name_ + ")");
  interpolationData();
  sideNumbering();
  pointCoordinates();
  mapType = _ArgyrisMap;
  dofCompatibility=_signDofCompatibility;
  maxDegree = 5;
  setMatrices();
  trace_p->pop();
}

ArgyrisTriangle::~ArgyrisTriangle() {}

//! interp defines Reference Element interpolation data
void ArgyrisTriangle::interpolationData()
{
  trace_p->push("ArgyrisTriangle::interpolationData");
  nbDofsInSides_ = 3;
  nbDofsOnVertices_ = 18;
  nbDofs_ = 21;
  // Creating Degrees Of Freedom of reference element
  // in Argyris standard elements all D.o.F on edges are sharable, no internal D.o.F
  refDofs.reserve(nbDofs_);
  for(number_t i = 1; i <= 3; i++ )
  {
    refDofs.push_back(new RefDof(this, true, _onVertex, i, 1, 2, 0, i, 1, 0, _noProjection,_id, "nodal value"));
    refDofs.push_back(new RefDof(this, true, _onVertex, i, 2, 2, 0, i, 1, 1, _noProjection,_dx, "nodal dx"));
    refDofs.push_back(new RefDof(this, true, _onVertex, i, 3, 2, 0, i, 1, 1, _noProjection,_dy, "nodal dy"));
    refDofs.push_back(new RefDof(this, true, _onVertex, i, 4, 2, 0, i, 1, 2, _noProjection,_dxx, "nodal dxx"));
    refDofs.push_back(new RefDof(this, true, _onVertex, i, 5, 2, 0, i, 1, 2, _noProjection,_dxy, "nodal dxy"));
    refDofs.push_back(new RefDof(this, true, _onVertex, i, 6, 2, 0, i, 1, 2, _noProjection,_dyy, "nodal dyy"));
  }
  for(number_t s = 1; s <= 3; s++ )
  {
    refDofs.push_back(new RefDof(this, true, _onEdge, (s<3?s+1:1), 1, 2, 0, s, 1, 1, _dotnProjection, _ndotgrad, "normal derivative"));
  }
  nbPts_ = nbDofs_;
  trace_p->pop();
}

//! sideNumbering defines Reference Element local numbers on sides
void ArgyrisTriangle::sideNumbering()
{
  trace_p->push("ArgyrisTriangle::sideNumbering");
  sideDofNumbers_.resize(3,std::vector<number_t>(13,0));
  for(number_t s=0;s<3;s++)
    for(number_t i=0;i<6;i++)
    {
      sideDofNumbers_[s][i]   = 1+6*s+i;
      sideDofNumbers_[s][i+6] = 1+6*(s<2?s+1:1)+i;
    }
  sideDofNumbers_[0][12] = 21; sideDofNumbers_[1][12] = 19; sideDofNumbers_[2][12] = 20;
  trace_p->pop();
}

//! pointCoordinates defines Argyris Reference Element point coordinates
void ArgyrisTriangle::pointCoordinates()
{
  trace_p->push("ArgyrisTriangle::pointCoordinates");
  std::vector<RefDof*>::iterator itrd = refDofs.begin();
  for(number_t i=0;i<6;i++, itrd++) (*itrd)->coords(1.,0.);
  for(number_t i=0;i<6;i++, itrd++) (*itrd)->coords(0.,1.);
  for(number_t i=0;i<6;i++, itrd++) (*itrd)->coords(0.,0.);
  (*itrd++)->coords(0.,0.5);
  (*itrd++)->coords(0.5,0);
  (*itrd)->coords(0.5,0.5);
  trace_p->pop();
}

/*! coefficient of polynomials (P5) defining shape functions stored in sparse matrices
    they come from ArgyrisPack developped by Erich Foster, Traian Iliescu, David Wells from Virginia Tech
    row (dof order) : P(Mi),dxP(Mi),dyP(Mi),dxxP(Mi),dxyP(Mi),dyyP(Mi) i=1,2,3 ; grad(P)(Ni).ni i=1,2,3
    col (monomial order) : 1 x y x^2 xy y^2 x^3 x^2y xy^2 y^3 x^4 x^3y x^2y^2 xy^3 y^4 x^5 x^4y x^3y^2 x^2y^3 xy^4 y^5
    NOTE: reverse sign of shape function w20, w21 of ArgyrisPack in order to deal with outward normals on every edge
*/
void ArgyrisTriangle::setMatrices()
{
    typedef std::vector<real_t> vect;
    mw.resize(21,21);mdx.resize(21,15);mdy.resize(21,15);mdxx.resize(21,10);mdyy.resize(21,10);mdxy.resize(21,10);
    std::vector<NumPair> dx(21,NumPair(0,0)),dy(21,NumPair(0,0));
    dx[1]=NumPair(1,0);dx[3]=NumPair(2,1);dx[4]=NumPair(1,2);dx[6]=NumPair(3,3);dx[7]=NumPair(2,4);dx[8]=NumPair(1,5);
    dx[10]=NumPair(4,6);dx[11]=NumPair(3,7);dx[12]=NumPair(2,8);dx[13]=NumPair(1,9);
    dx[15]=NumPair(5,10);dx[16]=NumPair(4,11);dx[17]=NumPair(3,12);dx[18]=NumPair(2,13);dx[19]=NumPair(1,14);
    dy[2]=NumPair(1,0);dy[4]=NumPair(1,1);dy[5]=NumPair(2,2);dy[7]=NumPair(1,3);dy[8]=NumPair(2,4);dy[9]=NumPair(3,5);
    dy[11]=NumPair(1,6);dy[12]=NumPair(2,7);dy[13]=NumPair(3,8);dy[14]=NumPair(4,9);
    dy[16]=NumPair(1,10);dy[17]=NumPair(2,11);dy[18]=NumPair(3,12);dy[19]=NumPair(4,13);dy[20]=NumPair(5,14);

    mw.setRow(1, Reals(0, 0, 0, 0, 0, 0, 10, 0, 0, 0, -15, 0, 15, 0, 0, 6, 0, -15, -15, 0, 0));
    mw.setRow(2, Reals(0, 0, 0, 0, 0, 0, -4, 0, 0, 0, 7, 0, -3.5, 0, 0, -3, 0, 3.5, 3.5, 0, 0));
    mw.setRow(3, Reals(0, 0, 0, 0, 0, 0, 0, -5, 0, 0, 0, 14, 18.5, 0, 0, 0, -8, -18.5, -13.5, 0, 0));
    mw.setRow(4, Reals(0, 0, 0, 0, 0, 0, 0.5, 0, 0, 0, -1, 0, 0.25, 0, 0, 0.5, 0, -0.25, -0.25, 0, 0));
    mw.setRow(5, Reals(0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, -3, -3.5, 0, 0, 0, 2, 3.5, 2.5, 0, 0));
    mw.setRow(6, Reals(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1.25, 0, 0, 0, 0, -0.75, -1.25, 0, 0));
    mw.setRow(7, Reals(0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 15, 0, -15, 0, 0, -15, -15, 0, 6));
    mw.setRow(8, Reals(0, 0, 0, 0, 0, 0, 0, 0, -5, 0, 0, 0, 18.5, 14, 0, 0, 0, -13.5, -18.5, -8, 0));
    mw.setRow(9, Reals(0, 0, 0, 0, 0, 0, 0, 0, 0, -4, 0, 0, -3.5, 0, 7, 0, 0, 3.5, 3.5, 0, -3));
    mw.setRow(10,Reals(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1.25, 0, 0, 0, 0, -1.25, -0.75, 0, 0));
    mw.setRow(11,Reals(0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, -3.5, -3, 0, 0, 0, 2.5, 3.5, 2, 0));
    mw.setRow(12,Reals(0, 0, 0, 0, 0, 0, 0, 0, 0, 0.5, 0, 0, 0.25, 0, -1, 0, 0, -0.25, -0.25, 0, 0.5));
    mw.setRow(13,Reals(1, 0, 0, 0, 0, 0, -10, 0, 0, -10, 15, 0, -30, 0, 15, -6, 0, 30, 30, 0, -6));
    mw.setRow(14,Reals(0, 1, 0, 0, 0, 0, -6, 0, -11, 0, 8, 0, 10, 18, 0, -3, 0, 1, -10, -8, 0));
    mw.setRow(15,Reals(0, 0, 1, 0, 0, 0, 0, -11, 0, -6, 0, 18, 10, 0, 8, 0, -8, -10, 1, 0, -3));
    mw.setRow(16,Reals(0, 0, 0, 0.5, 0, 0, -1.5, 0, 0, 0, 1.5, 0, -1.5, 0, 0, -0.5, 0, 1.5, 1, 0, 0));
    mw.setRow(17,Reals(0, 0, 0, 0, 1, 0, 0, -4, -4, 0, 0, 5, 10, 5, 0, 0, -2, -6, -6, -2, 0));
    mw.setRow(18,Reals(0, 0, 0, 0, 0, 0.5, 0, 0, 0, -1.5, 0, 0, -1.5, 0, 1.5, 0, 0, 1, 1.5, 0, -0.5));
    mw.setRow(19,Reals(0, 0, 0, 0, 0, 0, 0, 0, -16, 0, 0, 0, 32, 32, 0, 0, 0, -16, -32, -16, 0));
    mw.setRow(20,Reals(0, 0, 0, 0, 0, 0, 0, -16, 0, 0, 0, 32, 32, 0, 0, 0, -16, -32, -16, 0, 0));                    //change sign
    mw.setRow(21,Reals(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -8*sqrtOf2_, 0, 0, 0, 0, 8*sqrtOf2_, 8*sqrtOf2_, 0, 0));  //change sign

    vect w, g(15,0.), g2(10,0.);
    for(number_t k=1;k<=21;k++)
    {
       w=mw.getRow(k);
       g.assign(15,0.);g2.assign(10,0.);
       for(number_t i=0;i<21;i++)
           if(w[i]!=0 && dx[i].first!=0) g[dx[i].second]=dx[i].first*w[i];
       mdx.setRow(k,g);
       for(number_t i=0;i<15;i++)
           if(g[i]!=0 && dx[i].first!=0) g2[dx[i].second]=dx[i].first*g[i];
       mdxx.setRow(k,g2);
       g2.assign(10,0.);
       for(number_t i=0;i<15;i++)
           if(g[i]!=0 && dy[i].first!=0) g2[dy[i].second]=dy[i].first*g[i];
       mdxy.setRow(k,g2);
       g.assign(15,0.);g2.assign(10,0.);
       for(number_t i=0;i<21;i++)
           if(w[i]!=0 && dy[i].first!=0) g[dy[i].second]=dy[i].first*w[i];
       mdy.setRow(k,g);
       for(number_t i=0;i<15;i++)
           if(g[i]!=0 && dy[i].first!=0) g2[dy[i].second]=dy[i].first*g[i];
       mdyy.setRow(k,g2);
    }
}

/*! Argyris shape functions
    computed from matrix mw, mdx, mdy, mdxx, mdyy, mdxy
*/
void ArgyrisTriangle::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x = *it_pt,       x2=x*x, x3=x2*x, x4=x3*x, x5=x4*x;
  real_t y = *(it_pt + 1), y2=y*y, y3=y2*y, y4=y3*y, y5=y4*y;
  std::vector<real_t> mon = Reals(1,x,y,x2,x*y,y2,x3,x2*y,x*y2,y3,x4,x3*y,x2*y2,x*y3,y4,x5,x4*y,x3*y2,x2*y3,x*y4,y5);
  shv.w = mw*mon;
  if ( withDeriv )
  {
    mon.resize(15);
    shv.dw[0] = mdx*mon;
    shv.dw[1] = mdy*mon;
  }
  if ( with2Deriv )
  {
    mon.resize(10);
    shv.d2w[0] = mdxx*mon;
    shv.d2w[1] = mdyy*mon;
    shv.d2w[2] = mdxy*mon;
  }
}

/*!  create the affine transformation matrix mapping reference shape function to any triangle
     triangle and any related data are handled by the GeomMapData object (have to be set up before)
     2D Argyris transformation mapping ReferenceElement shape functions (s) unto current element,
      from Kirby R.C."A general approach to transforming finite elements".The SMAI journal of computational mathematics, Tome 4 (2018),197-224.doi:10.5802/smai-jcm.33.
     w1 = s1-a2s5-a3s6  w2 = s2-a1s4+a3s6  w3 = s3+a1s4+a2s5  w4=d1s4  w5=d2s5  w6=d3s6
     a1 = (Q1)_12/L1  a2 = (Q2)_12/L2  a3 = (Q3)_12/L3  di=(Qi)_11
     Qi = [ni ti]t Jt [nui taui]
          (ni, ti)   outward normal and tangent vector (pi/2 rotation of normal) on edge i of the reference triangle
          (nui taui) outward normal and tangent vector (pi/2 rotation of normal) on edge i of the current triangle
          Li length of edge i of the current triangle
     here edge i is the edge which does not contain vi vertex
     i=1 : n=(-1,0), t2=(0,-1) => Q12=J12*ny-J22*nx, Q11=-J21*ny-j11*nx
     i=2 : n=(0,-1), t2=(1,0)  => Q12=J11*ny-J21*nx, Q11=-J12*nx-j22*ny
     i=3 : n1=sqrt(2)/2(1,1), t1=0.5*sqrt(2)(-1,1) => Q12=sqrt(2)/2((J21+J22)*nx-(J11+J12)*ny), Q11=sqrt(2)/2(J11+J12)*nx+(J22+J21)*ny)
                                       |1 0 0  0  -a2 -a3|
                                       |0 1 0 -a1  0   a3|
      affine transformation matrix M = |0 0 1  a1  a2  0 |
                                       |0 0 0  d1  0   0 |
                                       |0 0 0  0   d2  0 |
                                       |0 0 0  0   0   d3|

     gd: geometric data (contains the inverse of jacobian and accessto length)
     out: matrix M
*/
Matrix<real_t> ArgyrisTriangle::affineMap(const GeomMapData& gd) const
{
   std::vector< Vector<real_t> >& ns=gd.sideNV();
   const Matrix<real_t>& J=gd.jacobianMatrix;
   real_t j11=J(1,1), j12=J(1,2),j21=J(2,1),j22=J(2,2);
   real_t l1=gd.measures(2), l2=gd.measures(3), l3=gd.measures(1);
   real_t n1x=ns[1][0],n1y=ns[1][1], n2x=ns[2][0], n2y=ns[2][1], n3x=ns[0][0], n3y=ns[0][1];
   real_t b1= (j11*n1y-j21*n1x), d1=-(j21*n1y+j11*n1x);
   real_t b2= (j22*n2x-j12*n2y), d2=-(j12*n2x+j22*n2y);
   real_t b3= 0.5*sqrtOf2_*((j21+j22)*n3x-(j11+j12)*n3y), d3= 0.5*sqrtOf2_*((j11+j12)*n3x+(j22+j21)*n3y);
   real_t a1=b1/l1, a2=b2/l2, a3=b3/l3, c1=b1*l1, c2=b2*l2, c3=b3*l3;
   real_t t11=j11*j11, t12=2*j11*j21, t13=j21*j21,
          t21=j12*j11, t22=j12*j21+j11*j22, t23=j21*j22,
          t31=j12*j12, t32=2*j22*j12, t33=j22*j22;
   real_t q8=15./8, s16=7./16, s32=1./32;
   a1*=q8;a2*=q8;a3*=q8;b1*=s16;b2*=s16;b3*=s16;c1*=s32;c2*=s32;c3*=s32;
   Matrix<real_t> M(21,_idMatrix);
   // Map shape functions
   M(1,20) =-a2; M(1,21) =-a3;
   M(7,20) = a3; M(7,21) =-a1;
   M(13,19)= a1; M(13,20)= a2;
   M(2,2)  = j11; M(2,3)  = j12; M(2,20) = -b2*n2y; M(2,21) = -b3*n3y;
   M(3,2)  = j21; M(3,3)  = j22; M(2,20) =  b2*n2x; M(2,21) = -b3*n3x;
   M(8,8)  = j11; M(8,9)  = j12; M(8,19) =  b1*n1y; M(8,21) = -b3*n3y;
   M(9,8)  = j21; M(9,9)  = j22; M(9,19) = -b1*n1x; M(9,21) = -b3*n3x;
   M(14,14)= j11; M(14,15)= j12; M(14,19)=  b1*n1y; M(14,20)= -b2*n2y;
   M(15,14)= j21; M(15,15)= j22; M(15,19)= -b1*n1x; M(15,20)=  b2*n2x;
   M(4,4) = t11; M(4,5) = t12; M(4,6) = t13; M(4,20) = -c2*n2y*n2y;   M(4,21)=-c3*n3y*n3y;
   M(5,4) = t21; M(5,5) = t22; M(5,6) = t23; M(5,20) =  2*c2*n2x*n2y; M(5,21)= 2*c3*n3x*n3y;
   M(6,4) = t31; M(6,5) = t32; M(6,6) = t33; M(6,20) = -c2*n2x*n2x;   M(6,21)=-c3*n3x*n3x;
   M(10,10) = t11; M(10,11) = t12; M(10,12) = t13; M(10,19) = -c1*n1y*n1y;   M(10,21)=-c3*n3y*n3y;
   M(11,10) = t21; M(11,11) = t22; M(11,12) = t23; M(11,19) =  2*c1*n1x*n1y; M(11,21)= 2*c3*n3x*n3y;
   M(12,10) = t31; M(12,11) = t32; M(12,12) = t33; M(12,19) = -c1*n1x*n1x;   M(12,21)= c3*n3x*n3x;
   M(16,16) = t11; M(16,17) = t12; M(16,18) = t13; M(16,19) =  c1*n1y*n1y;   M(16,20)= c2*n2y*n2y;
   M(17,16) = t21; M(17,17) = t22; M(17,18) = t23; M(17,19) = -2*c1*n1x*n1y; M(17,20)= -2*c2*n2x*n2y;
   M(18,16) = t31; M(18,17) = t32; M(18,18) = t33; M(18,19) =  c1*n1x*n1x;   M(18,20)= c2*n2x*n2x;
   M(19,19) = d1;
   M(20,20) = d2;
   M(21,21) = d3;
   return M;
}

} // end of namespace xlifepp
