/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LagrangeTriangle.cpp
  \authors D. Martin, N. Kielbasiewicz, E. Lunéville
  \since 17 dec 2002
  \date 12 feb 2015

  \brief Implementation of xlifepp::LagrangeTriangle class members and related functions
 */

#include "LagrangeTriangle.hpp"
#include "../Interpolation.hpp"
#include "utils.h"

#include <iostream>

namespace xlifepp
{

//! LagrangeTriangle constructor for Lagrange reference elements
LagrangeTriangle::LagrangeTriangle(const Interpolation* interp_p)
  : RefTriangle(interp_p)
{
  name_ += "_Lagrange";
  trace_p->push("LagrangeTriangle::LagrangeTriangle (" + name_ + ")");

  // build element interpolation data
  interpolationData();
  // local numbering of points on sides
  sideNumbering();
  // local numbering of points on sides
  sideOfSideNumbering();
  // Reference Element on edges
  sideRefElement();
  maxDegree = interpolation_p->numtype;

  trace_p->pop();
}

LagrangeTriangle::~LagrangeTriangle() {}
/*
--------------------------------------------------------------------------------
 interp defines Reference Element interpolation data
--------------------------------------------------------------------------------
*/
void LagrangeTriangle::interpolationData()
{
  trace_p->push("LagrangeTriangle::interpolationData");
  number_t interpNum = interpolation_p->numtype;

  switch(interpNum)
  {
    case _P0:       // Lagrange P_0 discontinuous
      nbDofs_ = nbInternalDofs_ = 1;
      break;
    case _P1BubbleP3: // Lagrange P_1 + P_3 bubble function
      nbDofs_ = 4;
      nbInternalDofs_ = 1;
      nbDofsOnVertices_ = 3;
      break;
    default:       // Lagrange standard triangle of degree > 0
      nbDofs_ = ((interpNum + 1)*(interpNum + 2))/2;
      nbInternalDofs_ = ((interpNum - 2)*(interpNum - 1))/2;
      nbDofsOnVertices_ = 3;
      nbDofsInSides_ = nbDofs_ - nbDofsOnVertices_ - nbInternalDofs_;
      break;
  }

  // Creating Degrees Of Freedom of reference element
  // in Lagrange standard (H1-conforming) elements all D.o.F on edges are sharable
  refDofs.reserve(nbDofs_);
  lagrangeRefDofs(2, nbDofsOnVertices_, nbInternalDofs_, 3, nbDofsInSides_);

  // compute number of shape functions
  nbshfcts_ = shapeValueSize();
  nbPts_ = nbDofs_;

  trace_p->pop();
}

//! create standard Lagrange numbering on sides
void LagrangeTriangle::sideNumbering()
{
  number_t interpNum = interpolation_p->numtype;
  number_t nbSides = geomRefElem_p->nbSides();
  sideDofNumbers_.resize(nbSides);
  if(interpNum==0)  //P0 interpolation
  {
       for(number_t side = 0; side < nbSides; side++)
       {
           sideDofNumbers_[side].resize(1);
           sideDofNumbers_[side][0]=1;
       }
       return;
  }

  if(interpNum > 0)
  {
    number_t intN = interpNum;
    if(interpNum == _P1BubbleP3) { intN = 1; }
    number_t nbVertices = geomRefElem_p->nbVertices();
    number_t more;
    number_t nbVerticesPerSide = geomRefElem_p->sideVertexNumbers()[0].size();
    number_t nbDofsPerSide = intN + 1;

    for(number_t side = 0; side < nbSides; side++)
    {
      sideDofNumbers_[side].resize(nbDofsPerSide);
      for(number_t j = 0; j < nbVerticesPerSide; j++)
      {
        sideDofNumbers_[side][j] = geomRefElem_p->sideVertexNumber(j + 1, side + 1);
      }
      if(intN > 1)  // order > 1
      {
        more = nbVertices + 1;
        for(number_t k = nbVerticesPerSide; k < nbDofsPerSide; k++, more += nbSides)
        {
          sideDofNumbers_[side][k] = side + more;
        }
      }
    }
  }
}

//! create standard Lagrange numbering on side of sides
void LagrangeTriangle::sideOfSideNumbering()
{
  number_t interpNum = interpolation_p->numtype;

  if(interpNum > 0)
  {
    number_t nbSideOfSides = geomRefElem_p->nbSideOfSides();
    number_t nbVerticesPerSideOfSide = geomRefElem_p->sideOfSideVertexNumbers()[0].size();

    sideOfSideDofNumbers_.resize(nbSideOfSides);
    for(number_t i = 0; i < nbSideOfSides; i++)
    {
      sideOfSideDofNumbers_[i].resize(nbVerticesPerSideOfSide);
      for(number_t j = 0; j < nbVerticesPerSideOfSide; j++)
      {
        sideOfSideDofNumbers_[i][j] = geomRefElem_p->sideOfSideVertexNumber(j + 1, i + 1);
      }
    }
  }
}

/*! compute shape functions coefficients in basis monoms by solving linear systems n=nbdofs=(k+1)*(k+2)/2
    | 1 y1 y1^2 ... x1 x1y1 x1y1^2 ... x1^k | | A11 A12 ... A1n |   | 1 0 ...  0 |
    | 1 y2 y2^2 ... x2 x2y2 x2y2^2 ... x2^k | | A21 A22 ... A2n |   | 0 1 ...  0 |
    |                ...                    | |       ...       |   |     ...    |
    | 1 yi yi^2 ... xi xiyi xiyi^2 ... xi^k | | Ai1 Ai2 ... Ain | = | 0 ...1...0 |
    |                ...                    | |       ...       |   |     ...    |
    | 1 yn yn^2 ... xn xnyn xnyn^2 ... xn^k | | An1 An2 ... Ann |   | 0 0 ...  1 |
*/

//! compute shape functions as polynomials using general algorithm (node coordinates has to be set)
// fill polynomials space Wk and dWk
void LagrangeTriangle::computeShapeFunctions()
{
  //compute Matrix Lij=pj(ni) for all pi in Vk, ni nodes
  number_t k=interpolation_p->numtype;
  Matrix<real_t> L(nbDofs_,nbDofs_);
  PolynomialBasis Pk(_Pk,2,k);             //Pk[X,Y] space equipped with canonical basis
  std::vector<RefDof*>::iterator itd=refDofs.begin();
  for (number_t i = 1; i <= nbDofs_; ++i, ++itd)   //loop on nodes
  {
    std::vector<real_t>::const_iterator itpt=(*itd)->coords();
    PolynomialBasis::iterator itp = Pk.begin();
    for (number_t j=1; j<=nbDofs_; ++j, ++itp) L(i,j)=itp->eval(*itpt, *(itpt+1));  //loop on monomials
  }
  //compute coefficients of shape functions by solving linear systems
  Matrix<real_t> sf(nbDofs_,_idMatrix);
  real_t eps=theZeroThreshold;
  number_t r;
  bool ok=gaussMultipleSolver(L, sf, nbDofs_, eps, r);
  if (!ok)
  {
    where("LagrangeTrianglePk::computeShapeFunctions()");
    error("mat_noinvert");
  }
  sf.roundToZero();
  Matrix<real_t>::iterator itm=sf.begin();
  Vector<real_t> v(nbDofs_);
  Vector<real_t>::iterator itv;
  Wk.dimVar=2;   Wk.dimVec=1;
  Wk.name="P"+tostring(k);
  for (number_t i=0; i<nbDofs_; i++)
  {
    for(itv=v.begin(); itv!=v.end(); ++itv, ++itm) *itv=*itm;
    Wk.add(combine(Pk,v));
  }
  // Wk.clean(0.000001); // to be used eventually for printing purpose
  dWk.resize(2);
  dWk[0]=dx(Wk); dWk[1]=dy(Wk);
}

//! return nodes numbers of first order triangle elements when splitting current element
std::vector<std::vector<number_t> > LagrangeTriangle::splitP1() const {
  std::vector<std::vector<number_t> > splitnum;
  std::vector<number_t> tuple(3,0);
  switch(interpolation_p->numtype) {
    case 0:
    case 1:
      tuple[0]=1; tuple[1]=2; tuple[2]=3;
      splitnum.push_back(tuple);
      return splitnum;
    case 2:
      tuple[0]=6; tuple[1]=5; tuple[2]=3;
      splitnum.push_back(tuple);
      tuple[0]=1; tuple[1]=4; tuple[2]=6;
      splitnum.push_back(tuple);
      tuple[0]=5; tuple[1]=6; tuple[2]=4;
      splitnum.push_back(tuple);
      tuple[0]=4; tuple[1]=2; tuple[2]=5;
      splitnum.push_back(tuple);
      return splitnum;
    case 3:
      tuple[0]=6; tuple[1]=8; tuple[2]=3;
      splitnum.push_back(tuple);
      tuple[0]=8; tuple[1]=6; tuple[2]=10;
      splitnum.push_back(tuple);
      tuple[0]=9; tuple[1]=10; tuple[2]=6;
      splitnum.push_back(tuple);
      tuple[0]=10; tuple[1]=9; tuple[2]=4;
      splitnum.push_back(tuple);
      tuple[0]=1; tuple[1]=4; tuple[2]=9;
      splitnum.push_back(tuple);
      tuple[0]=10; tuple[1]=5; tuple[2]=8;
      splitnum.push_back(tuple);
      tuple[0]=5; tuple[1]=10; tuple[2]=7;
      splitnum.push_back(tuple);
      tuple[0]=4; tuple[1]=7; tuple[2]=10;
      splitnum.push_back(tuple);
      tuple[0]=7; tuple[1]=2; tuple[2]=5;
      splitnum.push_back(tuple);
      return splitnum;
    case 4:
      tuple[0]=6; tuple[1]=11; tuple[2]=3;
      splitnum.push_back(tuple);
      tuple[0]=11; tuple[1]=6; tuple[2]=15;
      splitnum.push_back(tuple);
      tuple[0]=6; tuple[1]=9; tuple[2]=15;
      splitnum.push_back(tuple);
      tuple[0]=15; tuple[1]=9; tuple[2]=13;
      splitnum.push_back(tuple);
      tuple[0]=12; tuple[1]=13; tuple[2]=9;
      splitnum.push_back(tuple);
      tuple[0]=13; tuple[1]=12; tuple[2]=4;
      splitnum.push_back(tuple);
      tuple[0]=1; tuple[1]=4; tuple[2]=12;
      splitnum.push_back(tuple);
      tuple[0]=15; tuple[1]=8; tuple[2]=11;
      splitnum.push_back(tuple);
      tuple[0]=8; tuple[1]=15; tuple[2]=14;
      splitnum.push_back(tuple);
      tuple[0]=13; tuple[1]=14; tuple[2]=15;
      splitnum.push_back(tuple);
      tuple[0]=14; tuple[1]=13; tuple[2]=7;
      splitnum.push_back(tuple);
      tuple[0]=4; tuple[1]=7; tuple[2]=13;
      splitnum.push_back(tuple);
      tuple[0]=14; tuple[1]=5; tuple[2]=8;
      splitnum.push_back(tuple);
      tuple[0]=5; tuple[1]=14; tuple[2]=10;
      splitnum.push_back(tuple);
      tuple[0]=7; tuple[1]=10; tuple[2]=14;
      splitnum.push_back(tuple);
      tuple[0]=10; tuple[1]=2; tuple[2]=5;
      splitnum.push_back(tuple);
      return splitnum;
    case 5:
      tuple[0]=6; tuple[1]=14; tuple[2]=3;
      splitnum.push_back(tuple);
      tuple[0]=14; tuple[1]=6; tuple[2]=18;
      splitnum.push_back(tuple);
      tuple[0]=9; tuple[1]=18; tuple[2]=6;
      splitnum.push_back(tuple);
      tuple[0]=18; tuple[1]=9; tuple[2]=21;
      splitnum.push_back(tuple);
      tuple[0]=12; tuple[1]=21; tuple[2]=9;
      splitnum.push_back(tuple);
      tuple[0]=21; tuple[1]=12; tuple[2]=16;
      splitnum.push_back(tuple);
      tuple[0]=15; tuple[1]=16; tuple[2]=12;
      splitnum.push_back(tuple);
      tuple[0]=16; tuple[1]=15; tuple[2]=4;
      splitnum.push_back(tuple);
      tuple[0]=1; tuple[1]=4; tuple[2]=15;
      splitnum.push_back(tuple);
      tuple[0]=18; tuple[1]=11; tuple[2]=14;
      splitnum.push_back(tuple);
      tuple[0]=11; tuple[1]=18; tuple[2]=20;
      splitnum.push_back(tuple);
      tuple[0]=21; tuple[1]=20; tuple[2]=18;
      splitnum.push_back(tuple);
      tuple[0]=20; tuple[1]=21; tuple[2]=19;
      splitnum.push_back(tuple);
      tuple[0]=16; tuple[1]=19; tuple[2]=21;
      splitnum.push_back(tuple);
      tuple[0]=19; tuple[1]=16; tuple[2]=7;
      splitnum.push_back(tuple);
      tuple[0]=4; tuple[1]=7; tuple[2]=16;
      splitnum.push_back(tuple);
      tuple[0]=20; tuple[1]=8; tuple[2]=11;
      splitnum.push_back(tuple);
      tuple[0]=8; tuple[1]=20; tuple[2]=17;
      splitnum.push_back(tuple);
      tuple[0]=19; tuple[1]=17; tuple[2]=20;
      splitnum.push_back(tuple);
      tuple[0]=17; tuple[1]=19; tuple[2]=10;
      splitnum.push_back(tuple);
      tuple[0]=7; tuple[1]=10; tuple[2]=19;
      splitnum.push_back(tuple);
      tuple[0]=17; tuple[1]=5; tuple[2]=8;
      splitnum.push_back(tuple);
      tuple[0]=5; tuple[1]=17; tuple[2]=13;
      splitnum.push_back(tuple);
      tuple[0]=10; tuple[1]=13; tuple[2]=17;
      splitnum.push_back(tuple);
      tuple[0]=13; tuple[1]=2; tuple[2]=5;
      splitnum.push_back(tuple);
      return splitnum;
    case 6:
      tuple[0]=6; tuple[1]=17; tuple[2]=3;
      splitnum.push_back(tuple);
      tuple[0]=17; tuple[1]=6; tuple[2]=21;
      splitnum.push_back(tuple);
      tuple[0]=9; tuple[1]=21; tuple[2]=6;
      splitnum.push_back(tuple);
      tuple[0]=21; tuple[1]=9; tuple[2]=24;
      splitnum.push_back(tuple);
      tuple[0]=12; tuple[1]=24; tuple[2]=9;
      splitnum.push_back(tuple);
      tuple[0]=24; tuple[1]=12; tuple[2]=27;
      splitnum.push_back(tuple);
      tuple[0]=15; tuple[1]=27; tuple[2]=12;
      splitnum.push_back(tuple);
      tuple[0]=27; tuple[1]=15; tuple[2]=19;
      splitnum.push_back(tuple);
      tuple[0]=18; tuple[1]=19; tuple[2]=15;
      splitnum.push_back(tuple);
      tuple[0]=19; tuple[1]=18; tuple[2]=4;
      splitnum.push_back(tuple);
      tuple[0]=1; tuple[1]=4; tuple[2]=18;
      splitnum.push_back(tuple);
      tuple[0]=21; tuple[1]=14; tuple[2]=17;
      splitnum.push_back(tuple);
      tuple[0]=14; tuple[1]=21; tuple[2]=26;
      splitnum.push_back(tuple);
      tuple[0]=24; tuple[1]=26; tuple[2]=21;
      splitnum.push_back(tuple);
      tuple[0]=26; tuple[1]=24; tuple[2]=28;
      splitnum.push_back(tuple);
      tuple[0]=27; tuple[1]=28; tuple[2]=24;
      splitnum.push_back(tuple);
      tuple[0]=28; tuple[1]=27; tuple[2]=22;
      splitnum.push_back(tuple);
      tuple[0]=19; tuple[1]=22; tuple[2]=27;
      splitnum.push_back(tuple);
      tuple[0]=22; tuple[1]=19; tuple[2]=7;
      splitnum.push_back(tuple);
      tuple[0]=4; tuple[1]=7; tuple[2]=19;
      splitnum.push_back(tuple);
      tuple[0]=26; tuple[1]=11; tuple[2]=14;
      splitnum.push_back(tuple);
      tuple[0]=11; tuple[1]=26; tuple[2]=23;
      splitnum.push_back(tuple);
      tuple[0]=28; tuple[1]=23; tuple[2]=26;
      splitnum.push_back(tuple);
      tuple[0]=23; tuple[1]=28; tuple[2]=25;
      splitnum.push_back(tuple);
      tuple[0]=22; tuple[1]=25; tuple[2]=28;
      splitnum.push_back(tuple);
      tuple[0]=25; tuple[1]=22; tuple[2]=10;
      splitnum.push_back(tuple);
      tuple[0]=7; tuple[1]=10; tuple[2]=22;
      splitnum.push_back(tuple);
      tuple[0]=23; tuple[1]=8; tuple[2]=11;
      splitnum.push_back(tuple);
      tuple[0]=8; tuple[1]=23; tuple[2]=20;
      splitnum.push_back(tuple);
      tuple[0]=25; tuple[1]=20; tuple[2]=23;
      splitnum.push_back(tuple);
      tuple[0]=20; tuple[1]=25; tuple[2]=13;
      splitnum.push_back(tuple);
      tuple[0]=10; tuple[1]=13; tuple[2]=25;
      splitnum.push_back(tuple);
      tuple[0]=20; tuple[1]=5; tuple[2]=8;
      splitnum.push_back(tuple);
      tuple[0]=5; tuple[1]=20; tuple[2]=16;
      splitnum.push_back(tuple);
      tuple[0]=13; tuple[1]=16; tuple[2]=20;
      splitnum.push_back(tuple);
      tuple[0]=16; tuple[1]=2; tuple[2]=5;
      splitnum.push_back(tuple);
      return splitnum;
    default: return splitLagrange2DToP1();
  }
  return splitnum;
}

//! return nodes numbers of first order triangle elements when splitting current element
splitvec_t LagrangeTriangle::splitO1() const
{
  splitvec_t splitnum;
  std::vector<std::vector<number_t> > buf;
  buf=this->splitP1();
  for (number_t i=0; i< buf.size(); i++) { splitnum.push_back(std::make_pair(_triangle, buf[i])); }
  return splitnum;
}

/*! for a side of current element, return the list of (first order element number, side number) of the first order elements (P1)
   as a list of pair<P1 element number, P1 side number>
   example: P2 Lagrange
  \verbatim

     2                       2
     | \                     | \
  S2 5   4  S1       ==>     5---4
     |     \                 | \ | \
     3---6---1               3---6---1
         S3
 \endverbatim
 Side S1 :
   - Elt 2 (1 4 6), side (1,4) -->  (2,1)
   - Elt 4 (4 2 5), side (4,2) -->  (4,1)

 Side S2 :
   - Elt 1 (6 5 3), side (5 3) -->  (1,2)
   - Elt 4 (4 2 5), side (2,5) -->  (4,2)

 Side S3 :
   - Elt 1 (6 5 3), side (3 6) -->  (1,3)
   - Elt 2 (1 4 6), side (6,1) -->  (4,3)
*/

std::vector<std::pair<number_t,number_t> > LagrangeTriangle::splitP1Side(number_t side) const {
  if (side<1 || side>3) error("split_bad_side_num",side);
  std::vector<std::pair<number_t,number_t> > sidenum;
  switch(interpolation_p->numtype) {
    case 0:
    case 1:
      if (side==1)      { sidenum.push_back(std::pair<number_t,number_t>(1,1)); }
      else if (side==2) { sidenum.push_back(std::pair<number_t,number_t>(1,2)); }
      else              { sidenum.push_back(std::pair<number_t,number_t>(1,3)); }
      return sidenum;
    case 2:
      if (side==1) {
        sidenum.push_back(std::pair<number_t,number_t>(2,1));
        sidenum.push_back(std::pair<number_t,number_t>(4,1));
      }
      else if (side==2) {
        sidenum.push_back(std::pair<number_t,number_t>(1,2));
        sidenum.push_back(std::pair<number_t,number_t>(4,2));
      }
      else {
        sidenum.push_back(std::pair<number_t,number_t>(1,3));
        sidenum.push_back(std::pair<number_t,number_t>(4,3));
      }
      return sidenum;
    default: error("split_low_deg_Lagrange_elt", words("shape",_triangle) , 2, words("shape",_triangle));
  }
  return sidenum;
}

//! pointCoordinates defines Lagrange Reference Element point coordinates
std::vector<RefDof*>::iterator LagrangeTriangle::vertexCoordinates()
{
  std::vector<RefDof*>::iterator it_rd(refDofs.begin());
  (*it_rd++)->coords(1., 0.);
  (*it_rd++)->coords(0., 1.);
  (*it_rd++)->coords(0., 0.);
  return it_rd;
}

void LagrangeTriangle::pointCoordinates()
{
  //  trace_p->push("LagrangeTriangle::pointCoordinates");

  /*
    Local numbering of Lagrange Pk triangles
      2     2         2              2                  2
      | \   | \       | \            | \                | \
      3---1 5   4     5   7          5  10              5  13
       k=1  |     \   |     \        |     \            |     \
       .... 3---6---1 8  10   4      8  14   7          8  17  10
       ....... k=2    |         \    |         \        |         \
       .............. 3---6---9---1 11  15  13   4     11  20  19   7
       ................... k=3       |             \    |             \
       ............................. 3---6---9--12---1 14  18  21  16   4
       ..................................... k=4        |                 \
       ................................................ 3---6---9--12--15---1
       ......................................................... k=5
   */
  int interpNum = interpolation_p->numtype;
  std::vector<RefDof*>::iterator it_rd=refDofs.begin();
  switch (interpNum)
  {
    case 0: (*it_rd)->coords(over3_, over3_); break;
    case _P1BubbleP3:
      it_rd = vertexCoordinates();
      (*it_rd++)->coords(over3_, over3_);
      break;
    default: // Lagrange triangle of order > 0
      it_rd = vertexCoordinates();
      if (interpNum > 1)
      {
        // correspondence between segment and triangle local numbering
        std::vector<number_t> s2t(2*nbPts_);
        tensorNumberingTriangle(interpNum, s2t);

        RefElement* sideRef = sideRefElems_[0];
        // start after triangle last vertex
        number_t pt = 2*geomRefElem_p->nbVertices();
        while (it_rd != refDofs.end())
        {
          real_t x = *(sideRef->refDofs[s2t[pt++]]->coords());
          real_t y = *(sideRef->refDofs[s2t[pt++]]->coords());
          (*it_rd++)->coords(x, y);
        }
      }
      break;
  }
  //  trace_p->pop();
}

//! output of Lagrange D.o.f's numbers on underlying P1 mesh D.o.f's
void LagrangeTriangle::outputAsP1(std::ofstream& os, const int refNum, const std::vector<number_t>& ranks) const
{
  number_t interpNum = interpolation_p->numtype;

  /*
    Numbering of d.o.f is made starting with the outtermost layer of elements
    First three triangles are {1,4,12},  {13,12,4} and {4,7,13} for interpNum=3
    (edges of which are part of the edges of the original triangle)
    then recursively to the next inner layer
   */
  int pr(0), pr1(-1), corner_1(1), corner, p1, p2(1), p3, p4;
  while (interpNum > 3)
  {
    // first bottom right external "corner" bears number corner
    corner = corner_1 - pr;
    // first bottom right internal "corner" bears number corner_1
    // 13 for interpNum = 4, 16 for interpNum = 5, 19 for interpNum = 6
    corner_1 += int(3*interpNum);

    // layers of elements "parallel" to { x_1 + x_2 = 1 }
    p1 = corner++; p2 = p1 + 3; p4 = corner_1 - pr; p3 = p4 - 1;
    for (number_t j = 1; j < interpNum - 1; j++)
    {
      simplexVertexOutput(os, refNum, ranks[p1 + pr1], ranks[p2 + pr1], ranks[p3 + pr1]);
      simplexVertexOutput(os, refNum, ranks[p4 + pr1], ranks[p3 + pr1], ranks[p2 + pr1]);
      p1 = p2; p2 += 3; p3 = p4; p4 += 3;
      if (j == interpNum - 3) { corner_1 += 1; p4 = corner_1 - pr; }
    }

    simplexVertexOutput(os, refNum, ranks[p1 + pr1], ranks[p2 + pr1], ranks[corner_1 - 1]);

    // layers of elements "parallel" to { x_1 = 0 }
    p1 = p2; p2 = corner++; p3 = p2 + 3; p4 = corner_1 - pr;
    for (number_t j = 1; j < interpNum - 1; j++)
    {
      simplexVertexOutput(os, refNum, ranks[p1 + pr1], ranks[p2 + pr1], ranks[p3 + pr1]);
      simplexVertexOutput(os, refNum, ranks[p3 + pr1], ranks[p4 + pr1], ranks[p1 + pr1]);
      p1 = p4; p2 = p3; p3 += 3; p4 += 3;
      if (j == interpNum - 3) { corner_1 += 1; p4 = corner_1 - pr; }
    }
    simplexVertexOutput(os, refNum, ranks[p1 + pr1], ranks[p2 + pr1], ranks[p3 + pr1]);

    // layers of elements "parallel" to { x_2 = 0 }
    p4 = p1; p2 = p3; p3 = corner++; p1 = p3 + 3;
    for (number_t j = 1; j < interpNum - 1; j++)
    {
      simplexVertexOutput(os, refNum, ranks[p1 + pr1], ranks[p2 + pr1], ranks[p3 + pr1]);
      simplexVertexOutput(os, refNum, ranks[p2 + pr1], ranks[p1 + pr1], ranks[p4 + pr1]);
      p3 = p1; p1 += 3; p2 = p4; p4 += 3;
      if (j == interpNum - 3) { corner_1 -= 2; p4 = corner_1 - pr; }
    }

    simplexVertexOutput(os, refNum, ranks[p1 + pr1], ranks[p2 + pr1], ranks[p3 + pr1]);

    // initialize for next inner layer
    pr = corner_1 - 1; pr1 = pr - 1; interpNum -= 3;
  }

  pr = pr1;

  // finally consider the three cases for innermost triangles(s)
  if (interpNum == 1) { simplexVertexOutput(os, refNum, ranks[1 + pr], ranks[2 + pr], ranks[3 + pr]); }
  else if (interpNum == 2)
  {
    /*
        2                      3
        | \                    | \
        |   \                  |   \
        |     \                | #2  \
        5       4     ------>  5-------4
        |         \            | \  #4 | \
        |           \          |   \   |   \
        |             \        | #3  \ | #1  \
        3-------6-------1      3-------6-------1
     */
    simplexVertexOutput(os, refNum, ranks[1 + pr], ranks[4 + pr], ranks[6 + pr]);
    simplexVertexOutput(os, refNum, ranks[4 + pr], ranks[2 + pr], ranks[5 + pr]);
    simplexVertexOutput(os, refNum, ranks[6 + pr], ranks[5 + pr], ranks[3 + pr]);
    simplexVertexOutput(os, refNum, ranks[5 + pr], ranks[6 + pr], ranks[4 + pr]);
  }
  else if (interpNum == 3)
  {
    /*
        2                          2
        | \                        | \
        |   \                      |   \
        |     \                    | #4  \
        5      7                   5------7
        |       \                  | \ #5 | \
        |         \      ------->  |   \  |   \
        |           \              | #6  \| #3  \
        8      10     4            8------10-----4
        |               \          | \ #8 | \ #2 | \
        |                 \        |   \  |   \  |   \
        |                   \      |  #7 \| #9  \| #1  \
        3------6------9------1     3------6------9------1
     */

    simplexVertexOutput(os, refNum, ranks[1 + pr], ranks[4 + pr], ranks[9 + pr]);
    simplexVertexOutput(os, refNum, ranks[10 + pr], ranks[9 + pr], ranks[4 + pr]);
    simplexVertexOutput(os, refNum, ranks[4 + pr], ranks[7 + pr], ranks[10 + pr]);
    simplexVertexOutput(os, refNum, ranks[7 + pr], ranks[2 + pr], ranks[5 + pr]);
    simplexVertexOutput(os, refNum, ranks[5 + pr], ranks[10 + pr], ranks[7 + pr]);
    simplexVertexOutput(os, refNum, ranks[10 + pr], ranks[5 + pr], ranks[8 + pr]);
    simplexVertexOutput(os, refNum, ranks[6 + pr], ranks[8 + pr], ranks[3 + pr]);
    simplexVertexOutput(os, refNum, ranks[6 + pr], ranks[8 + pr], ranks[10 + pr]);
    simplexVertexOutput(os, refNum, ranks[9 + pr], ranks[10 + pr], ranks[6 + pr]);
  }
  else { noSuchFunction("outputAsP1"); }
} // end of outputAsP1

//! Lagrange standard P0 triangle Reference Element
template<>
LagrangeStdTriangle<_P0>::LagrangeStdTriangle(const Interpolation* interp_p)
  : LagrangeTriangle(interp_p)
{
  name_ += "_0";
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

template<>
LagrangeStdTriangle<_P0>::~LagrangeStdTriangle() {}

template<>
void LagrangeStdTriangle<_P0>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  std::vector<real_t>::iterator it_w=shv.w.begin();
  *it_w = 1.;

  if (withDeriv)
  {
    std::vector< std::vector<real_t> >::iterator it_dwdx1=shv.dw.begin(), it_dwdx2=it_dwdx1+1;
    std::vector<real_t>::iterator it_dwdx1i=(*it_dwdx1).begin(), it_dwdx2i=(*it_dwdx2).begin();
    *it_dwdx1i = 0.; *it_dwdx2i = 0.;
  }
}

//! Lagrange standard P1 triangle Reference Element
template<>
LagrangeStdTriangle<_P1>::LagrangeStdTriangle(const Interpolation* interp_p)
  : LagrangeTriangle(interp_p)
{
  name_ += "_1";
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  // build O1 splitting scheme
  splitO1Scheme = splitO1();

  //#ifdef FIG4TEX_OUTPUT
  // Draw a LagrangeStdTriangle using TeX and fig4TeX
  //  fig4TeX();
  //#endif
}

template<>
LagrangeStdTriangle<_P1>::~LagrangeStdTriangle() {}

template<>
void LagrangeStdTriangle<_P1>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  // In the (x1, x2) reference plane shape functions of the Lagrange P1 triangle
  // are given by (see local numbering)
  // w_1(x1,x2) = x1
  // w_2(x1,x2) = x2
  // w_3(x1,x2) = x3 = 1 - x1 - x2
  //
  real_t x1=*it_pt, x2=*(it_pt+1);

  std::vector<real_t>::iterator it_w=shv.w.begin();
  *it_w++ = x1;
  *it_w++ = x2;
  *it_w = 1.-x1-x2;

  if (withDeriv)
  {
    std::vector< std::vector<real_t> >::iterator it_dwdx1=shv.dw.begin(), it_dwdx2=it_dwdx1+1;
    std::vector<real_t>::iterator it_dwdx1i=(*it_dwdx1).begin(), it_dwdx2i=(*it_dwdx2).begin();
    *it_dwdx1i++ = 1.; *it_dwdx2i++ = 0.;
    *it_dwdx1i++ = 0.; *it_dwdx2i++ = 1.;
    *it_dwdx1i = -1.;  *it_dwdx2i = -1.;
  }
}

//! Lagrange standard Pk triangle Reference Element
template<number_t Pk>
LagrangeStdTriangle<Pk>::LagrangeStdTriangle(const Interpolation* interp_p)
  : LagrangeTriangle(interp_p)
{
  name_ += "_" + tostring(Pk);
  pointCoordinates();  // local coordinates of Points supporting D.o.F
  // build O1 splitting scheme
  splitO1Scheme = splitO1();

  //#ifdef FIG4TEX_OUTPUT
  // Draw a LagrangeStdTriangle using TeX and fig4TeX
  //  fig4TeX();
  //#endif
}

template<number_t Pk>
LagrangeStdTriangle<Pk>::~LagrangeStdTriangle() {}

//!  computeShapeValues defines Lagrange Pk Reference Element shape functions
template<>
void LagrangeStdTriangle<_P2>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  // In the (x1, x2) reference plane shape functions of the Lagrange P2 triangle
  // are given, with x3 = 1 - x1 - x2, by (see local numbering)
  // w_i(x1,x2) = xi(2xi-1)  (i = 1,2,3)
  // w_4(x1,x2) =  4 x1 x2
  // w_5(x1,x2) =  4 x2 x3
  // w_6(x1,x2) =  4 x3 x1
  //
  real_t x1=*it_pt, x2=*(it_pt+1), x3=1.-x1-x2, qx1=4.*x1, qx2=4.*x2, qx3=4.*x3;
  std::vector<real_t>::iterator it_w=shv.w.begin();
  *it_w++ = x1*(2.*x1-1.);
  *it_w++ = x2*(2.*x2-1.);
  *it_w++ = x3*(2.*x3-1.);
  *it_w++ = qx1*x2;
  *it_w++ = qx2*x3;
  *it_w = qx3*x1;

  if (withDeriv)
  {
    std::vector< std::vector<real_t> >::iterator it_dwdx1=shv.dw.begin(), it_dwdx2=it_dwdx1+1;
    std::vector<real_t>::iterator it_dwdx1i=(*it_dwdx1).begin(), it_dwdx2i=(*it_dwdx2).begin();
    *it_dwdx1i++ = qx1 - 1.;  *it_dwdx2i++ = 0.;
    *it_dwdx1i++ = 0.;        *it_dwdx2i++ = qx2 - 1.;
    *it_dwdx1i++ = -qx3 + 1.; *it_dwdx2i++ = -qx3 + 1.;
    *it_dwdx1i++ = qx2;       *it_dwdx2i++ = qx1;
    *it_dwdx1i++ = -qx2;      *it_dwdx2i++ = qx3 - qx2;
    *it_dwdx1i = qx3 - qx1;   *it_dwdx2i = -qx1;
  }
}

template<>
void LagrangeStdTriangle<_P3>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  // In the (x1, x2) reference plane shape functions of the Lagrange P_3 triangle
  // are given (with x3 = 1 - x1 - x2) by
  // w_i(x1,x2) = (1/2) xi(3xi-1)(3xi-2) (i = 1,2,3)
  // w_4(x1,x2) = (9/2) x1(2x1-1)x2
  // w_5(x1,x2) = (9/2) x2(2x2-1)x3
  // w_6(x1,x2) = (9/2) x3(2x3-1)x1
  // w_7(x1,x2) = (9/2) x2(2x2-1)x1
  // w_8(x1,x2) = (9/2) x3(2x3-1)x2
  // w_9(x1,x2) = (9/2) x1(2x1-1)x3
  // w_10(x1,x2) = 27 x1 x2 x3

  real_t x1=*it_pt, x2=*(it_pt+1), x3=1.-x1-x2, x1x3=x1*x3, x2x3=x2*x3,
         kx1m1=3.*x1-1., kx2m1=3.*x2-1., kx3m1=3.*x3-1.,
         x1kx1m=x1*kx1m1, x2kx2m=x2*kx2m1, x3kx3m=x3*kx3m1;
  real_t f9_2=9./2., f27_2=27./2.;

  std::vector<real_t>::iterator it_w=shv.w.begin();
  *it_w++ = x1kx1m*(kx1m1-1.)/2.;
  *it_w++ = x2kx2m*(kx2m1-1.)/2.;
  *it_w++ = x3kx3m*(kx3m1-1.)/2.;
  *it_w++ = f9_2*x1kx1m*x2;
  *it_w++ = f9_2*x2kx2m*x3;
  *it_w++ = f9_2*x3kx3m*x1;
  *it_w++ = f9_2*x2kx2m*x1;
  *it_w++ = f9_2*x3kx3m*x2;
  *it_w++ = f9_2*x1kx1m*x3;
  *it_w = 27.*x1*x2x3;

  if (withDeriv)
  {
    std::vector< std::vector<real_t> >::iterator it_dwdx1=shv.dw.begin(), it_dwdx2=it_dwdx1+1;
    std::vector<real_t>::iterator it_dwdx1i=(*it_dwdx1).begin(), it_dwdx2i=(*it_dwdx2).begin();
    *it_dwdx1i++ = 1. + (f27_2*x1-9.)*x1;        *it_dwdx2i++ = 0.;
    *it_dwdx1i++ = 0.;                           *it_dwdx2i++ = 1. + (f27_2*x2-9.)*x2;
    *it_dwdx1i++ = -1. - (f27_2*x3-9.)*x3;       *it_dwdx2i++ = -1. - (f27_2*x3-9.)*x3;
    *it_dwdx1i++ = -(f9_2-27.*x1)*x2;            *it_dwdx2i++ = f9_2*x1kx1m;
    *it_dwdx1i++ = -f9_2*x2kx2m;                 *it_dwdx2i++ = f9_2*((x3-x2)*kx2m1+3.*x2x3);
    *it_dwdx1i++ = f9_2*((x3-x1)*kx3m1-3.*x1x3); *it_dwdx2i++ = (f9_2-27.*x3)*x1;
    *it_dwdx1i++ = f9_2*x2kx2m;                  *it_dwdx2i++ = -(f9_2-27.*x2)*x1;
    *it_dwdx1i++ = (f9_2-27.*x3)*x2;             *it_dwdx2i++ = f9_2*((x3-x2)*kx3m1-3.*x2x3);
    *it_dwdx1i++ = f9_2*((x3-x1)*kx1m1+3.*x1x3); *it_dwdx2i++ = -f9_2*x1kx1m;
    *it_dwdx1i = 27.*x2*(x3-x1);                 *it_dwdx2i = 27.*x1*(x3-x2);
  }
}

template<>
void LagrangeStdTriangle<_P4>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  // In the (x1, x2) reference plane shape functions of the Lagrange P_4 triangle
  // are given (with x3 = 1 - x1 - x2) by
  //
  // ... well, they are not that given .....

  real_t x1=*it_pt, x2=*(it_pt+1), x3=1.-x1-x2, x1x2=x1*x2,
         kx1=4.*x1, kx1m1=kx1-1., kx1m2=kx1-2., x1kx1m=x1*kx1m1,
         kx2=4.*x2, kx2m1=kx2-1., kx2m2=kx2-2., x2kx2m=x2*kx2m1,
         kx3=4.*x3, kx2x3=kx2*x3;
  real_t t18=3.-kx1-kx2, t19=kx3*t18, t20=2.-kx1-kx2;
  real_t f08_3=8./3.;

  std::vector<real_t>::iterator it_w=shv.w.begin();
  *it_w++ = x1kx1m*kx1m2*(kx1-3.)/6.;
  *it_w++ = x2kx2m*kx2m2*(-3+kx2)/6.;
  *it_w++ = t19*t20*(1-kx1-kx2)/24.;
  *it_w++ = f08_3*x1kx1m*kx1m2*x2;
  *it_w++ = f08_3*x2kx2m*kx2m2*x3;
  *it_w++ = f08_3*x1*x3*t18*t20;
  *it_w++ = 4.*x1kx1m*x2kx2m;
  *it_w++ = x2kx2m*t19;
  *it_w++ = x1kx1m*t19;
  *it_w++ = f08_3*x1x2*kx2m1*kx2m2;
  *it_w++ = 2./3.*kx2x3*t18*t20;
  *it_w++ = f08_3*x1kx1m*kx1m2*x3;
  *it_w++ = 8.*x1kx1m*kx2x3;
  *it_w++ = 32.*x1x2*kx2m1*x3;
  *it_w = 8.*x1x2*t19;

  if (withDeriv)
  {
    std::vector< std::vector<real_t> >::iterator it_dwdx1=shv.dw.begin(), it_dwdx2=it_dwdx1+1;
    std::vector<real_t>::iterator it_dwdx1i=(*it_dwdx1).begin(), it_dwdx2i=(*it_dwdx2).begin();
    real_t t4_1=128./3.*x1, t4_2=128./3.*x2;
    real_t t4_12=128.*x2, t4_20=16./3.*x2, t4_21=64.*x2;
    real_t t4_23=128.*x1x2;
    real_t t4_32=f08_3*x2kx2m*kx2m2, t4_33=1.-x2, t4_34=3.-kx2, t4_38=256.*x2, t4_39=384.-t4_38;
    real_t t4_41=384.*x2, t4_42=512./3.*x1, t4_49=x1x2*kx2m1;
    real_t t4_50=32.*t4_49;
    real_t t4_67=(192.-t4_12)*x2;
    real_t t4_80=4.*x2*t4_33, t4_84=384.*x1x2;
    real_t t4_94=512.*x2;
    real_t t4_109=-25./3.+(140./3.+(-80.+t4_2)*x2)*x2+(140./3+(-160.+t4_12)*x2+(-80.+t4_12+t4_1)*x1)*x1;
    real_t t4_110=f08_3*x1*(4.*x1-1.)*(4.*x1-2.), t4_111=512./3.*x2;
    real_t t4_116=64.-t4_12, t4_121=128.*x1, t4_126=32.*x2, t4_128=(t4_12-16.)*x1;

    *it_dwdx1i++ = -1. + (44./3.+(-48.+t4_1)*x1)*x1;                   *it_dwdx2i++ = 0.;
    *it_dwdx1i++ = 0.;                                                 *it_dwdx2i++ = -1. + (44./3.+(-48.+t4_2)*x2)*x2;
    *it_dwdx1i++ = t4_109;                                             *it_dwdx2i++ = t4_109;
    *it_dwdx1i++ = t4_20 + (-t4_21+t4_23)*x1;                          *it_dwdx2i++ = t4_110;
    *it_dwdx1i++ = -t4_32;                                             *it_dwdx2i++ = 16./3. + (-224./3.+(224.-t4_111)*x2)*x2 + (-16./3.+t4_116*x2)*x1;
    *it_dwdx1i++ = -f08_3*t4_33*t4_34*kx2m2 + (-416./3.+t4_39*x2+(288.-384.*x2-t4_42)*x1)*x1; *it_dwdx2i++ = (-208./3.+t4_67+(192.-t4_38-t4_121)*x1)*x1;
    *it_dwdx1i++ = -4.*x2kx2m + t4_50;                                 *it_dwdx2i++ = (4.-t4_126+t4_128)*x1;
    *it_dwdx1i++ = (28.+(-144.+t4_12)*x2)*x2 + t4_50;                  *it_dwdx2i++ = -12. + (152.-t4_39*x2)*x2 + (28.+(-288.+384.*x2)*x2+t4_128)*x1;
    *it_dwdx1i++ = -4.*t4_33*t4_34 + (152.+(-288.+t4_12)*x2+(-384.+384.*x2+256.*x1)*x1)*x1; *it_dwdx2i++ = (28.-t4_126+(-144.+t4_12+t4_121)*x1)*x1;
    *it_dwdx1i++ = t4_32;                                              *it_dwdx2i++ = (16./3.-t4_116*x2)*x1;
    *it_dwdx1i++ = (-208./3.+t4_67)*x2 + ((192.-256.*x2)*x2-t4_23)*x1; *it_dwdx2i++ = 16. + (-416./3.+(288.-t4_111)*x2)*x2 + (-208./3.+384.*x2*t4_33+(96.-t4_38-t4_1)*x1)*x1;
    *it_dwdx1i++ = 16./3. - t4_20 + (-224./3.+t4_21+(224.-t4_12-t4_42)*x1)*x1; *it_dwdx2i++ = -t4_110;
    *it_dwdx1i++ = -8.*t4_80 + ((320.-256.*x2)*x2-t4_84)*x1;           *it_dwdx2i++ = (-32.+t4_21+(160.-t4_38-t4_121)*x1)*x1;
    *it_dwdx1i++ = 32.*x2kx2m*t4_33 - 64.*t4_49;                       *it_dwdx2i++ = (-32.+(320.-t4_41)*x2+(-t4_38+32.)*x1)*x1;
    *it_dwdx1i = 8.*t4_80*t4_34 + ((-448.+t4_94)*x2+t4_84)*x1;         *it_dwdx2i = (96.+(-448.+t4_41)*x2+(t4_94-224.+t4_121)*x1)*x1;
  }
}

template<>
void LagrangeStdTriangle<_P5>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x1=*it_pt, x2=*(it_pt+1), x3=1.-x1-x2, x1x2=x1*x2,
         kx1=5.*x1, kx1m1=kx1-1., kx1m2=kx1-2., kx1m3=kx1-3., kx1m4=kx1-4., x1kx1m=x1*kx1m1,
         kx2=5.*x2, kx2m1=kx2-1., kx2m2=kx2-2., kx2m3=kx2-3., kx2m4=kx2-4., x2kx2m=x2*kx2m1, kx2x3=kx2*x3;
  real_t f125_3=125./3., f25_4=25./4., f125_4=125./4., f25_6=25./6., f125_6=125./6.,
         f05_12=5./12., f25_12=25./12., f05_24=5./24., f25_24=25./24.;

  std::vector<real_t>::iterator it_w=shv.w.begin();
  real_t t5_22 = 4.-kx1-kx2;
  real_t t5_23 = 5.*x3*t5_22;
  real_t t5_24 = 3.-kx1-kx2;
  real_t t5_25 = 2.-kx1-kx2;
  real_t t5_26 = t5_22*t5_24*t5_25;
  real_t t5_27 = t5_23*t5_24;

  *it_w++ = x1kx1m*kx1m2*kx1m3*kx1m4/24.;
  *it_w++ = x2kx2m*kx2m2*kx2m3*kx2m4/24.;
  *it_w++ = t5_23*t5_24*t5_25*(1.-kx1-kx2)/120.;
  *it_w++ = f25_24*x1kx1m*kx1m2*kx1m3*x2;
  *it_w++ = f25_24*x2kx2m*kx2m2*kx2m3*x3;
  *it_w++ = f25_24*x1*x3*t5_26;
  *it_w++ = f25_12*x1kx1m*kx1m2*x2*kx2m1;
  *it_w++ = f25_12*x2kx2m*kx2m2*x3*t5_22;
  *it_w++ = f05_12*x1kx1m*t5_27;
  *it_w++ = f25_12*x1kx1m*x2kx2m*kx2m2;
  *it_w++ = f05_12*x2kx2m*t5_27;
  *it_w++ = f25_12*x1kx1m*kx1m2*x3*t5_22;
  *it_w++ = f25_24*x1x2*kx2m1*kx2m2*kx2m3;
  *it_w++ = f05_24*kx2x3*t5_26;
  *it_w++ = f25_24*x1kx1m*kx1m2*kx1m3*x3;
  *it_w++ = f125_6*x1kx1m*kx1m2*x2*x3;
  *it_w++ = f125_6*x1x2*kx2m1*kx2m2*x3;
  *it_w++ = f25_6*x1x2*t5_27;
  *it_w++ = f125_4*x1kx1m*x2kx2m*x3;
  *it_w++ = f125_4*x1x2*kx2m1*x3*t5_22;
  *it_w++ = f25_4*x1kx1m*kx2x3*t5_22;

  if (withDeriv)
  {
    std::vector< std::vector<real_t> >::iterator it_dwdx1=shv.dw.begin(), it_dwdx2=it_dwdx1+1;
    std::vector<real_t>::iterator it_dwdx1i=(*it_dwdx1).begin(), it_dwdx2i=(*it_dwdx2).begin();
    real_t T1 = 3125./24.*x1;
    real_t T2 = 3125./24.*x2;
    real_t T3 = 3125./6.*x2;
    real_t T4 = 3125./4.*x2;
    real_t T30 = -137./12. + (375./4.+(-2125./8.+(312.5-T2)*x2)*x2)*x2 + (375./4.+(-2125./4.+(1875./2.-T3)*x2)*x2+(-2125./8.+(1875./2.-T4)*x2+(312.5-T3-T1)*x1)*x1)*x1;
    real_t T6 = f25_4*x2;
    real_t T7 = 1375./12.*x2;
    real_t T8 = 1875./4.*x2;
    real_t T9 = 3125./6.*x1x2;
    real_t T48 = f25_24*x2kx2m*kx2m2*kx2m3;
    real_t T49 = 1.-x2;
    real_t T51 = -5.*T49*kx2m4;
    real_t T54 = 3125./3.*x2;
    real_t T59 = 9375./4.*x2;
    real_t T62 = 6250./3.*x2;
    real_t T63 = 15625./24.*x1;
    real_t T74 = 625./4.*x1x2*kx2m1;
    real_t T85 = x1x2*kx2m1*kx2m2;
    real_t T86 = f125_6*T85;
    real_t T97 = 3125.*x2;
    real_t T98 = 15625./12.*x1;
    real_t T123 = 312.5*x2;
    real_t T138 = (8875./12.+(-4375./4.+T3)*x2)*x2;
    real_t T141 = 1562.5*x2;
    real_t T142 = -4375./2.+T141;
    real_t T143 = T142*x2;
    real_t T160 = 5.*T49*x2;
    real_t T162 = 625.*x2;
    real_t T167 = 6250./3.*x1x2;
    real_t T185 = 9375./2.*x2;
    real_t T198 = 1875./4.*x1x2*kx2m1;
    real_t T238 = f25_24*x1*kx1m1*kx1m2*kx1m3;
    real_t T239 = 15625./24.*x2;
    real_t T246 = 1875./4.-T3;
    real_t T253 = 3125./6.*x1;
    real_t T260 = f125_3*x2;
    real_t T261 = T3-625./12.;
    real_t T267 = 15625./12.*x2;
    real_t T281 = (f125_6+(-312.5+T4)*x2)*x1;
    real_t T285 = 625./4.*x2;
    real_t T290 = 3125./4.*x1;
    real_t T340 = -4375./2.+T59;
    real_t T381 = -T141+625./4.;
    real_t T382 = f05_24*T51*kx2m2*kx2m3 + (-1925./6.+(8875./6.+(-4375./2.+T54)*x2)*x2+(8875./8.+(-13125./4.+T59)*x2+(-4375./3.+T62+T63)*x1)*x1)*x1;
    real_t T383 = -f25_12*T49*kx2m4*kx2m3 + (2675./6.+(-8875./6.+(1562.5-T3)*x2)*x2+(-7375./4.+(16875./4.-T59)*x2+(8125./3.-T97-T98)*x1)*x1)*x1;
    real_t T384 = (1175./12.+(-8875./12.+(5625./4.-T4)*x2)*x2)*x2 + ((-250.+1562.5*T49*x2)*x2-T74)*x1;
    real_t T385 = -f25_6*T49*kx2m4+(-325.+(3875./6.-T123)*x2+(6125./4.+(-9375./4.+T4)*x2+(-2500.+T62+T98)*x1)*x1)*x1;

    *it_dwdx1i++ = 1. + (-f125_6+(875./8.+(-625./3.+T1)*x1)*x1)*x1;   *it_dwdx2i++ = 0.;
    *it_dwdx1i++ = 0.;                                                *it_dwdx2i++ = 1. + (-f125_6+(875./8.+(-625./3.+T2)*x2)*x2)*x2;
    *it_dwdx1i++ = T30;                                               *it_dwdx2i++ = T30;
    *it_dwdx1i++ = -T6 + (T7 +(-T8+T9)*x1)*x1;                        *it_dwdx2i++ = T238;
    *it_dwdx1i++ = -T48;                                              *it_dwdx2i++ = -f25_4 + (1525./12.+(-5125./8.+(6875./6.-T239)*x2)*x2)*x2 + (f25_4+(-1375./12.+T246*x2)*x2)*x1;
    *it_dwdx1i++ = T382;                                              *it_dwdx2i++ = (-1925./12.+T138+(8875./12.+T143+(-4375./4.+T141+T253)*x1)*x1)*x1;
    *it_dwdx1i++ = f25_6*x2kx2m + (-62.5*x2kx2m+T74)*x1;              *it_dwdx2i++ = (T260-f25_6+(-T123+f125_4+T261*x1)*x1)*x1;
    *it_dwdx1i++ = (-37.5+(3875./12.+(-3125./4.+T3)*x2)*x2)*x2 + T86; *it_dwdx2i++ = 50./3. + (-325.+(6125./4.+(-2500.+T267)*x2)*x2)*x2 + (-37.5+(3875./6.+(-9375./4.+T62)*x2)*x2+T281)*x1;
    *it_dwdx1i++ = T383;                                              *it_dwdx2i++ = (1175./12.+(-250.+T285)*x2+(-8875./12.+(1562.5-T4)*x2+(5625./4.-T141-T290)*x1)*x1)*x1;
    *it_dwdx1i++ = -f25_12*x2kx2m*kx2m2 + T86;                        *it_dwdx2i++ = (-f25_6+(62.5-T285)*x2+T281)*x1;
    *it_dwdx1i++ = T384;                                              *it_dwdx2i++ = -25. + (2675./6.+(-7375./4.+(8125./3.-T267)*x2)*x2)*x2 + (1175./12.+(-8875./6.+(16875./4.-T97)*x2)*x2 + (-125.+(1562.5-T59)*x2-T261*x1)*x1)*x1;
    *it_dwdx1i++ = T385;                                              *it_dwdx2i++ = (-37.5+T260+(3875./12.-T123+(-3125./4.+T3+T253)*x1)*x1)*x1;
    *it_dwdx1i++ = T48;                                               *it_dwdx2i++ = (-f25_4+(1375./12.-T246*x2)*x2)*x1;
    *it_dwdx1i++ = (-1925./12.+T138)*x2 + ((8875./12.+T143)*x2+((-4375./4.+T141)*x2+T9)*x1)*x1; *it_dwdx2i++ = 25. + (-1925./6.+(8875./8.+(-4375./3.+T239)*x2)*x2)*x2 + (-1925./12.+(8875./6.+(-13125./4.+T62)*x2)*x2+(8875./24.+T340*x2+(T54-4375./12.+T1)*x1)*x1)*x1;
    *it_dwdx1i++ = -f25_4 + T6 + (1525./12.-T7+(-5125./8.+T8+(6875./6.-T3-T63)*x1)*x1)*x1; *it_dwdx2i++ = -T238;
    *it_dwdx1i++ = 25./3.*T160 + ((-2125./3.+T162)*x2+((2500.-T141)*x2-T167)*x1)*x1; *it_dwdx2i++ = (f125_3-250./3.*x2+(-2125./6.+T162+(2500./3.-T54-T253)*x1)*x1)*x1;
    *it_dwdx1i++ = f125_6*x2kx2m*kx2m2*T49 - f125_3*T85;              *it_dwdx2i++ = (f125_3+(-2125./3.+(2500.-T62)*x2)*x2+(-f125_3+(625.-T141)*x2)*x1)*x1;
    *it_dwdx1i++ = f25_6*T160*kx2m4*kx2m3 + ((-5875./3.+(5000.-T97)*x2)*x2+((3750.-T185)*x2-T167)*x1)*x1; *it_dwdx2i++ = (250.+(-5875./3.+(3750.-T62)*x2)*x2+(-5875./6.+(5000.-T185)*x2+(1250.-T97-T253)*x1)*x1)*x1;
    *it_dwdx1i++ = -f125_4*x2kx2m*T49 + ((-375.-T142*x2)*x2-T198)*x1; *it_dwdx2i++ = (f125_4+(-375.+T8)*x2+(-375./2.-T340*x2+T381*x1)*x1)*x1;
    *it_dwdx1i++ = f25_4*x2kx2m*T51 + ((1125./2.+(-6875./2.+T97)*x2)*x2+T198)*x1; *it_dwdx2i++ = (-125.+(3625./2.+(-9375./2.+T97)*x2)*x2+(1125./4.+(-6875./2.+T185)*x2-T381*x1)*x1)*x1;
    *it_dwdx1i = f25_4*T160*kx2m4 + ((3625./2.+(-6875./2.+T141)*x2)*x2+(-9375./2.*T49*x2+3125.*x1x2)*x1)*x1; *it_dwdx2i = (-125.+(1125./2.-T8)*x2+(3625./4.+(-6875./2.+T59)*x2+(-1562.5+T97+T290)*x1)*x1)*x1;
  }
}

template<>
void LagrangeStdTriangle<_P6>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x1=*it_pt, x2=*(it_pt+1);

  std::vector<real_t>::iterator it_w=shv.w.begin();
  real_t f3_10 = 0.3, f3_4 = 0.75;

  real_t T1 = 6.*x1;
  real_t T3 = x1*(T1-1.);
  real_t T5 = T3*(T1-2.);
  real_t T6 = T1-3.;
  real_t T8 = T6*(T1-4.);
  real_t T13 = 6.*x2;
  real_t T14 = T13-1.;
  real_t T15 = x2*T14;
  real_t T16 = T13-2.;
  real_t T17 = T15*T16;
  real_t T18 = T13-3.;
  real_t T19 = T13-4.;
  real_t T20 = T18*T19;
  real_t T25 = 1.-x1-x2;
  real_t T26 = 5.-T1-T13;
  real_t T28 = 4.-T1-T13;
  real_t T29 = 6.*T25*T26*T28;
  real_t T30 = 3.-T1-T13;
  real_t T31 = 2.-T1-T13;
  real_t T46 = T28*T30*T31;
  real_t T49 = T6*x2;
  real_t T59 = T26*T28*T30;
  real_t T67 = T3*x2;
  real_t T68 = T14*T16;
  real_t T72 = 6.*T15*T25;
  real_t T79 = x1*x2;
  real_t T80 = T79*T14;
  real_t T81 = T16*T18;
  real_t T86 = 6.*x2*T25*T26;

  *it_w++ = T5*T8*(T1-5.)/120.;
  *it_w++ = T17*T20*(T13-5.)/120.;
  *it_w++ = T29*T30*T31*(1.-T1-T13)/720.;
  *it_w++ = f3_10*T5*T8*x2;
  *it_w++ = f3_10*T17*T20*T25;
  *it_w++ = f3_10*x1*T25*T26*T46;
  *it_w++ = f3_4*T5*T49*T14;
  *it_w++ = f3_4*T17*T18*T25*T26;
  *it_w++ = f3_4*T3*T25*T59;
  *it_w++ = T5*T17;
  *it_w++ = T17*T29/6.;
  *it_w++ = T5*T29/6.;
  *it_w++ = f3_4*T67*T68*T18;
  *it_w++ = T72*T59/8.;
  *it_w++ = f3_4*T5*T6*T25*T26;
  *it_w++ = f3_10*T80*T81*T19;
  *it_w++ = T86*T46/20.;
  *it_w++ = f3_10*T5*T8*T25;
  *it_w++ = 9.*T5*T49*T25;
  *it_w++ = 9.*T80*T81*T25;
  *it_w++ = 9.*T79*T25*T59;
  *it_w++ = 3.*T5*T72;
  *it_w++ = 18.*T80*T16*T25*T26;
  *it_w++ = 3.*T67*T29;
  *it_w++ = 18.*T67*T68*T25;
  *it_w++ = 3.*T80*T29;
  *it_w++ = 3.*T5*T86;
  *it_w = 27.*T67*T14*T25*T26;

  if (withDeriv)
  {
    std::vector< std::vector<real_t> >::iterator it_dwdx1=shv.dw.begin(), it_dwdx2=it_dwdx1+1;
    std::vector<real_t>::iterator it_dwdx1i=(*it_dwdx1).begin(), it_dwdx2i=(*it_dwdx2).begin();
    real_t f1944_5=1944./5., f812_5=812./5., fm1323_2=-1323./2., f36_5=36./5.;
    real_t f11664_5 = 11664./5.;

    real_t T1 = f1944_5*x1;
    real_t T11 = f1944_5*x2;
    real_t T20 = 1944.*x2;
    real_t T27 = 3888.*x2;
    real_t T42 = -14.7 + (f812_5+(fm1323_2+(1260.+(-1134.+T11)*x2)*x2)*x2)*x2
               + (f812_5+(-1323.+(3780.+(-4536.+T20)*x2)*x2)*x2+(fm1323_2+(3780.+(-6804.+T27)*x2)*x2+(1260.+(-4536.+T27)*x2+(-1134.+T20+T1)*x1)*x1)*x1)*x1;
    real_t T43 = f36_5*x2;
    real_t T44 = 180.*x2;
    real_t T45 = 1134.*x2;
    real_t T46 = 2592.*x2;
    real_t T47 = x1*x2;
    real_t T48 = 1944.*T47;
    real_t T56 = 6.*x2;
    real_t T57 = T56 - 1.;
    real_t T58 = x2*T57;
    real_t T59 = T56 - 2.;
    real_t T60 = T56 - 3.;
    real_t T61 = T59*T60;
    real_t T62 = T56 - 4.;
    real_t T65 = f3_10*T58*T61*T62;
    real_t T66 = 1. - x2;
    real_t T67 = 5. - T56;
    real_t T68 = 6.*T66*T67;
    real_t T69 = T62*T60;
    real_t T74 = (10368.-T27)*x2;
    real_t T79 = 11664.*x2;
    real_t T84 = 15552.*x2;
    real_t T87 = 9720.*x2;
    real_t T88 = f11664_5*x1;
    real_t T101 = T47*T57;
    real_t T102 = 648.*T101;
    real_t T116 = T57*T59;
    real_t T118 = T47*T116*T60;
    real_t T119 = 9.*T118;
    real_t T134 = 23328.*x2;
    real_t T137 = 19440.*x2;
    real_t T138 = 5832.*x1;
    real_t T148 = T58*T59;
    real_t T151 = T47*T116;
    real_t T152 = 108.*T151;
    real_t T164 = 7776.*x2;
    real_t T209 = -19440. + T79;
    real_t T210 = T209*x2;
    real_t T226 = 594.*x2;
    real_t T248 = (2088.+(-5022.+(5184.-T20)*x2)*x2)*x2;
    real_t T252 = (15552.-T164)*x2;
    real_t T254 = (-10044.+T252)*x2;
    real_t T258 = (15552.-T79)*x2;
    real_t T279 = 6.*x2*T66;
    real_t T281 = 1188.*x2;
    real_t T284 = 5832.*x2;
    real_t T287 = 9720.*T47;
    real_t T300 = -T67*T62;
    real_t T304 = -34992. + T84;
    real_t T310 = 34992.*x2;
    real_t T315 = 31104.*x2;
    real_t T334 = 2592.*T101;
    real_t T340 = 6.*T59*T66;
    real_t T350 = 324.*T151;
    real_t T366 = 46656.*x2;
    real_t T369 = 19440.*T47;
    real_t T451 = 6.*x1;
    real_t T460 = f3_10*x1*(T451-1.)*(T451-2.)*(T451-3.)*(T451-4.);
    real_t T461 = f11664_5*x2;
    real_t T470 = 2592. - T20;
    real_t T479 = 1944.*x1;
    real_t T488 = 54.*x2;
    real_t T490 = (T20-162.)*x1;
    real_t T516 = (-27.+(594.+(-2916.+T27)*x2)*x2)*x1;
    real_t T520 = 648.*x2;
    real_t T529 = 3888.*x1;
    real_t T538 = 216.*x2;
    real_t T543 = -1296. + T27;
    real_t T714 = (-T164+648)*x1;
    real_t T731 = -3888. + T79;

    real_t f137_5=137./5., fm405_2=-405./2., fm3132_5=-3132./5., f9_2=9./2.,
           f99_2=99./2., fm8289_2=-8289./2., fm1197_2=-1197./2., fm12447_2=-12447./2.,
           f513_2=513./2., fm1566_5=-1566./5., fm972_5=-972./5., fm3_2=-3./2.,
           fm45_2=-45./2., fm1071_2=-1071./2.;

    *it_dwdx1i++ = -1. + (f137_5+(fm405_2+(612.+(-810.+T1)*x1)*x1)*x1)*x1;
    *it_dwdx1i++ = 0.;
    *it_dwdx1i++ = T42;
    *it_dwdx1i++ = T43 + (-T44+(T45+(-T46+T48)*x1)*x1)*x1;
    *it_dwdx1i++ = -T65;
    *it_dwdx1i++ = -T68*T69*T59/20. + (fm3132_5+(4176.+(-10044.+T74)*x2)*x2+(3132.+(-15066.+(23328.-T79)*x2)*x2+(-6696.+(20736.-T84)*x2+(6480.-T87-T88)*x1)*x1)*x1)*x1;
    *it_dwdx1i++ = -f9_2*T58 + (99.*T58+(-486.*T58+T102)*x1)*x1;
    *it_dwdx1i++ = (f99_2+(fm1197_2+(2376.+(-3726.+T20)*x2)*x2)*x2)*x2+T119;
    *it_dwdx1i++ = -f3_4*T66*T67*T69 + (1053.+(-5220.+(9342.+(-7128.+T20)*x2)*x2)*x2+(fm12447_2+(23652.+(-29160.+T79)*x2)*x2+(14796.+(-37584.+T134)*x2+(-15390.+T137+T138)*x1)*x1)*x1)*x1;
    *it_dwdx1i++ = 2.*T148 + (-36.*T148+T152)*x1;
    *it_dwdx1i++ = (-148.+(1692.+(-6120.+(8424.-T27)*x2)*x2)*x2)*x2 + ((360.+(-3672.+(10368.-T164)*x2)*x2)*x2-T152)*x1;
    *it_dwdx1i++ = -2.*T66*T67*T62 + (-1016.+(3384.+(-3672.+1296*x2)*x2)*x2+(6696.+(-18360.+(15552.-T27)*x2)*x2+(-17424.+(33696.-T84)*x2+(19440.-T137-7776.*x1)*x1)*x1)*x1)*x1;
    *it_dwdx1i++ = -f3_4*T58*T61 + T119;
    *it_dwdx1i++ = (f513_2+(-2610.+(7884.+(-9396.+T27)*x2)*x2)*x2)*x2 + ((-1071.+(9342.+T210)*x2)*x2+((1458.+(-10692.+T79)*x2)*x2+T102)*x1)*x1;
    *it_dwdx1i++ = -f9_2*T66*T67 + (594.+(-1197.+T226)*x2+(fm8289_2+(7128.-2916.*x2)*x2+(11556.+(-14904.+T27)*x2+(-13770.+T87+T138)*x1)*x1)*x1)*x1;
    *it_dwdx1i++ = T65;
    *it_dwdx1i++ = (fm1566_5+T248)*x2 + ((2088.+T254)*x2+((-5022.+T258)*x2+((5184.-T164)*x2-T48)*x1)*x1)*x1;
    *it_dwdx1i++ = f36_5 - T43 + (fm972_5+T44+(1404.-T45+(-4104.+T46+(5184.-T20-T88)*x1)*x1)*x1)*x1;
    *it_dwdx1i++ = -9.*T279 + ((1296.-T281)*x2+((-7614.+T284)*x2+(T252-T287)*x1)*x1)*x1;
    *it_dwdx1i++ = 9.*T58*T61*T66 - 18.*T118;
    *it_dwdx1i++ = fm3_2*T279*T300*T60 + ((-6156.+(25704.+T304*x2)*x2)*x2+((19278.+(-52488.+T310)*x2)*x2+((-23328.+T315)*x2+T287)*x1)*x1)*x1;
    *it_dwdx1i++ = 36.*T58*T66 + ((720.+(-4968.+T27)*x2)*x2+((-2916.-T209*x2)*x2-T334)*x1)*x1;
    *it_dwdx1i++ = 3.*T58*T340*T67 + ((-792.+(7992.+(-22032.+T84)*x2)*x2)*x2+T350)*x1;
    *it_dwdx1i++ = -3.*T279*T300 + ((6984.+(-22464.+(23328.-T164)*x2)*x2)*x2+((-28836.+(64152.-T310)*x2)*x2+((41472.-T366)*x2-T369)*x1)*x1)*x1;
    *it_dwdx1i++ = -3.*T58*T340 + ((504.+(-4968.+(12960.-T164)*x2)*x2)*x2-T350)*x1;
    *it_dwdx1i++ = -3.*T58*T68*T62 + ((2664.+(-22464.+(42768.-T134)*x2)*x2)*x2 + ((-4860.+34992.*x2*T66)*x2-T334)*x1)*x1;
    *it_dwdx1i++ = 6.*T279*T67 + ((-4032.+(7992.-T27)*x2)*x2+((21060.+(-33048.+T79)*x2)*x2+((-36288.+T315)*x2+T369)*x1)*x1)*x1;
    *it_dwdx1i = -f9_2*T58*T68 + ((-2214.+(17496.+(-27216.+T79)*x2)*x2)*x2+((5832.+(-40824.+T310)*x2)*x2+3888.*T101)*x1)*x1;

    *it_dwdx2i++ = 0.;
    *it_dwdx2i++ = -1. + (f137_5+(fm405_2+(612.+(-810.+T11)*x2)*x2)*x2)*x2;
    *it_dwdx2i++ = T42;
    *it_dwdx2i++ = T460;
    *it_dwdx2i++ = f36_5 + (fm972_5+(1404.+(-4104.+(5184.-T461)*x2)*x2)*x2)*x2 + (-f36_5+(180.+(-1134.+T470*x2)*x2)*x2)*x1;
    *it_dwdx2i++ = (fm1566_5+T248+(2088.+T254+(-5022.+T258+(5184.-T164-T479)*x1)*x1)*x1)*x1;
    *it_dwdx2i++ = (-T488+f9_2+(T226-f99_2+(-T20+162.+T490)*x1)*x1)*x1;
    *it_dwdx2i++ = fm45_2 + (594.+(fm8289_2+(11556.+(-13770.+T284)*x2)*x2)*x2)*x2 + (f99_2+(-1197.+(7128.+(-14904.+T87)*x2)*x2)*x2+T516)*x1;
    *it_dwdx2i++ = (f513_2+(-1071.+(1458.-T520)*x2)*x2+(-2610.+(9342.+(-10692.+T27)*x2)*x2+(7884.+T210+(-9396.+T79+T529)*x1)*x1)*x1)*x1;
    *it_dwdx2i++ = (4.+(-72.+T538)*x2+(-36.+(648.-T20)*x2+(72.+T543*x2)*x1)*x1)*x1;
    *it_dwdx2i++ = 40. + (-1016.+(6696+(-17424.+(19440.-T164)*x2)*x2)*x2)*x2 + (-148.+(3384.+(-18360.+(33696.-T137)*x2)*x2)*x2+(180.+(-3672.+15552.*x2*T66)*x2+(-72.-T543*x2)*x1)*x1)*x1;
    *it_dwdx2i++ = (-148.+ (360.-T538)*x2+(1692.+(-3672.+T20)*x2+(-6120.+T74+(8424.-T164-T529)*x1)*x1)*x1)*x1;
    *it_dwdx2i++ = (f9_2+(-99.+(486.-T520)*x2)*x2+T516)*x1;
    *it_dwdx2i++ = -45. + (1053.+(fm12447_2+(14796.+(-15390.+T284)*x2)*x2)*x2)*x2 + (f513_2+(-5220.+(23652.+(-37584.+T137)*x2)*x2)*x2+(fm1071_2+(9342.+(-29160.+T134)*x2)*x2+(486.+(-7128.+T79)*x2+T490)*x1)*x1)*x1;
    *it_dwdx2i++ = (f99_2-T488+(fm1197_2+T226+(2376.-T20+(-3726.+T20+T479)*x1)*x1)*x1)*x1;
    *it_dwdx2i++ = (f36_5+(-180.+(1134.-T470*x2)*x2)*x2)*x1;
    *it_dwdx2i++ = 36. + (fm3132_5+(3132.+(-6696.+(6480.-T461)*x2)*x2)*x2)*x2 + (fm1566_5+(4176.+(-15066.+(20736.-T87)*x2)*x2)*x2+(1044.+(-10044.+(23328.-T84)*x2)*x2+(-1674.+(10368.-T79)*x2+(1296.-T27-T1)*x1)*x1)*x1)*x1;
    *it_dwdx2i++ = -T460;
    *it_dwdx2i++ = (-54.+108.*x2+(-T281+648.+(T27-2538.+(-T27+3888.-T479)*x1)*x1)*x1)*x1;
    *it_dwdx2i++ = (-54.+(1296.+(-7614.+(15552.-T87)*x2)*x2)*x2+(54.+(-1188.+(5832.-T164)*x2)*x2)*x1)*x1;
    *it_dwdx2i++ = (540.+(-6156.+(19278.+(-23328.+T87)*x2)*x2)*x2+(-3078.+(25704.+(-52488.+T315)*x2)*x2+(6426.-34992*x2*T66+(T84-5832.+T479)*x1)*x1)*x1)*x1;
    *it_dwdx2i++ = (-36.+(504.-T520)*x2+(360.+(-4968.+T284)*x2+(-972.+(12960.-T79)*x2+T714)*x1)*x1)*x1;
    *it_dwdx2i++ = (180.+(-4032.+(21060.+(-36288.+T137)*x2)*x2)*x2+(-396.+(7992.+(-33048.+T315)*x2)*x2+(216.+T731*x2)*x1)*x1)*x1;
    *it_dwdx2i++ = (-360.+(2664.+(-4860.+T46)*x2)*x2+(3492.+(-22464.-T304*x2)*x2+(-9612.+(42768.-T310)*x2+(-T134+10368.-T529)*x1)*x1)*x1)*x1;
    *it_dwdx2i++ = (-36.+(720.+(-2916.+T46)*x2)*x2+(252.+(-4968.+(19440.-T84)*x2)*x2+(-216.-T731*x2)*x1)*x1)*x1;
    *it_dwdx2i++ = (-360.+(6984.+(-28836.+(41472.-T137)*x2)*x2)*x2+(1332.+(-22464.+(64152.-T366)*x2)*x2+(-1620.+(23328.-T310)*x2+T714)*x1)*x1)*x1;
    *it_dwdx2i++ = (180.+(-792.+T520)*x2+(-2016.+(7992.-T284)*x2+(7020.+(-22032.+T79)*x2+(T84-9072.+T529)*x1)*x1)*x1)*x1;
    *it_dwdx2i = (135.+(-2214.+(5832.-T27)*x2)*x2+(-1107.+(17496.+(-40824.+T134)*x2)*x2+(1944.+(-27216.+T310)*x2+(T79-972.)*x1)*x1)*x1)*x1;
  }
}

template class LagrangeStdTriangle<_P0>;
template class LagrangeStdTriangle<_P1>;
template class LagrangeStdTriangle<_P2>;
template class LagrangeStdTriangle<_P3>;
template class LagrangeStdTriangle<_P4>;
template class LagrangeStdTriangle<_P5>;
template class LagrangeStdTriangle<_P6>;

/*
--------------------------------------------------------------------------------
   Lagrange standard P1+bubbleP3 triangle Reference Element
--------------------------------------------------------------------------------
*/
template<>
LagrangeStdTriangle<_P1BubbleP3>::LagrangeStdTriangle(const Interpolation* interp_p)
  : LagrangeTriangle(interp_p)
{
  name_ += "_1+BubbleP3";
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

template<>
void LagrangeStdTriangle<_P1BubbleP3>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x1 = *it_pt, x2 = *(it_pt + 1), x3 = 1. - x1 - x2;

  std::vector<real_t>::iterator it_w(shv.w.begin());
  std::vector< std::vector<real_t> >::iterator it_dwdx1=shv.dw.begin(), it_dwdx2(it_dwdx1 + 1);
  std::vector<real_t>::iterator it_dwdx1i((*it_dwdx1).begin()), it_dwdx2i((*it_dwdx2).begin());
  real_t Ninex1x2x3 = 9.*x1*x2*x3;
  *it_w++ = x1 - Ninex1x2x3;
  *it_w++ = x2 - Ninex1x2x3;
  *it_w++ = x3 - Ninex1x2x3;
  *it_w++ = 3.*Ninex1x2x3;

  if(withDeriv)
  {
    real_t d1Ninex1x2x3 = 9.*x2*(x3 - x1), d2Ninex1x2x3 = 9.*x1*(x3 - x2);
    *it_dwdx1i++ = 1. - d1Ninex1x2x3; *it_dwdx2i++ =   -d2Ninex1x2x3;
    *it_dwdx1i++ =   -d1Ninex1x2x3; *it_dwdx2i++ = 1. - d2Ninex1x2x3;
    *it_dwdx1i++ = -1. - d1Ninex1x2x3; *it_dwdx2i++ = -1. - d2Ninex1x2x3;
    *it_dwdx1i   = 3.*d1Ninex1x2x3; *it_dwdx2i++ = 3.*d2Ninex1x2x3;
  }
}


/*!
  point number p has coordinates defined by ( x = coords[s2q[2*p]], y = coords[s2q[2*p+1]] ) where coords are coordinates of the associated 1D Lagrange reference element

  Examples of local numbering of Lagrange Pk 1D elements
   2---1  2---3---1  2---4---3---1  2---5---4---3---1  2---6---5---4---3---1  2---7---6---5---4---3---1
  . k=1      k=2          k=3              k=4                  k=5                      k=6
  Examples of local numbering of Lagrange Pk triangles

   2      2          2              2                  2                      2
   | \    | \        | \            | \                | \                    | \
   3---1  5   4      5   7          5  10              5  13                  5  16
  . k=1   |     \    |     \        |     \            |     \                |     \
  ....... 3---6---1  8  10   4      8  14   7          8  17  10              8  20  13
  .......... k=2     |         \    |  |  \   \        |  | \    \            |  | \    \
  .................. 3---6---9---1 11  15--13   4     11  20  19   7         11  23  25  10
  ........................ k=3      |             \    |  |     \    \        |  |     \    \
  ................................. 3---6---9--12---1 14  18--21--16   4     14  26  28  22   7
  .......................................... k=4       |                 \    |  |         \    \
  .................................................... 3---6---9--12--15---1 17  21--24--27--19   4
  ............................................................. k=5           |                     \
  ........................................................................... 3---6---9--12--15--18---1
  ...................................................................................... k=6
 */
void tensorNumberingTriangle(const int interpNum, std::vector<number_t>& s2t)
{
  std::vector<number_t>::iterator p=s2t.begin();
  int nk=interpNum;
  number_t q1 = 0, q2 = 1, q3 = 2, q4 = interpNum, q3_p, q4_m;

  while(nk > 0)
  {
    // 'current perimeter' vertices
    (*p++) = q1; (*p++) = q2;
    (*p++) = q2; (*p++) = q1;
    (*p++) = q2; (*p++) = q2;

    // non-vertex points on edges of current 'perimeter'
    q3_p = q3; q4_m = q4;
    for(dimen_t j = 2; j <= nk; j++)
      {
        (*p++) = q3_p; (*p++) = q4_m;
        (*p++) = q2; (*p++) = q3_p++;
        (*p++) = q4_m--; (*p++) = q2;
      }

    q1 += 2; q2--; q3 += 2 ; q4--;
    if(nk == interpNum) { q1++; q2 = interpNum; }
    nk -= 3;
  }
  // central point if any
  if(nk == 0) { (*p++) = q1; (*p) = q1; }
}

/*================================================================================================
                        Lagrange standard Pk triangle (any order)
  ================================================================================================*/

LagrangeStdTrianglePk::LagrangeStdTrianglePk(const Interpolation* interp_p)
  : LagrangeTriangle(interp_p)
{
  name_ += "_" + tostring(interp_p->numtype);
  pointCoordinates();      // local coordinates of Points supporting D.o.F
  computeShapeFunctions(); //construct shape functions as polynomials
  buildPolynomialTree();   //construct tree representation
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

LagrangeStdTrianglePk::~LagrangeStdTrianglePk() {}


// // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //
// #ifdef FIG4TEX_OUTPUT
// /*
// --------------------------------------------------------------------------------
//    fig4TeX builds a tex file to display local numbering and coordinates
// --------------------------------------------------------------------------------
// */
// void LagrangeTriangle::fig4TeX()
// {
//  Number nbVertices = geomRefElem_p->nbVertices();
//  char b = char(92);

//  std::ofstream os;
//  os.open((fig4TexFileName()).c_str(), ios::out);
//  os << "%" << b << "input fig4tex.tex" << b << "input TeXnicolor.tex" << b << "input Fonts.tex";
//  os << std::endl << b << "fourteenpoint" << b << "nopagenumbers";
//  os << std::endl << b << "newbox" << b << "mybox";
//  if ( nbDofs(1, 1) < 6 ) { os << std::endl << b << "figinit{150pt}"; }
//  else { os << std::endl << b << "figinit{250pt}"; }
//  string longName = "Lagrange Triangle of degree ";

//  // Draw element and display vertices
//  number_t pts(0);

//  // define vertices
//  fig4TeXVertexPt(os, geomRefElem_p->vertices(), pts);

//  // create postscript file
//  os << std::endl << b << "psbeginfig{}" << std::endl << b << "pssetwidth{1}" << b << "psline[";
//  for ( size_t p = 1; p <= nbVertices; p++ ) { os << p << ","; }
//  os << "1]";
//  os << std::endl << b << "psendfig"; // end of postcript output

//  os << std::endl << b << "figvisu{" << b << "mybox}{" << longName << interpolation_p->number() << "}{%"
//     << std::endl << b << "figsetmark{$" << b << "bullet$}" << b << "figsetptname{{" << b << "bf#1}}";
//  // vertex nodes
//  os << std::endl << b << "figwritese1:(5pt)"
//     << std::endl << b << "figwritenw2:(5pt)"
//     << std::endl << b << "figwritesw3:(5pt)";

//  // edge vertices
//  os << std::endl << b << "figsetmark{.}"
//     << std::endl << b << "color{" << b << "Red}"
//     << std::endl << b << "figwritegw1:${}_2$(7pt,7pt)" << std::endl << b << "figwritegw1:${}_1$(12pt,1pt)"
//     << std::endl << b << "figwritege2:${}_2$(1pt,-12pt)" << std::endl << b << "figwritege2:${}_1$(7pt,-7pt)"
//     << std::endl << b << "figwritege3:${}_1$(1pt,7pt)" << std::endl << b << "figwritege3:${}_2$(7pt,1pt)";
//  os << std::endl << b << "endcolor";
//  // nodes on edge 1 (north-east), edge 2 (west), edge 3 (south)
//  number_t ed(0);
//  fig4TeXEdgePt(os , ed++, pts, "ne", "sw");
//  fig4TeXEdgePt(os , ed++, pts, "w", "e");
//  fig4TeXEdgePt(os , ed++, pts, "s", "n");

//  // internal nodes
//  if ( nb_internal_dof_ > 0 ) { fig4TeXInternalPoints2d(os, pts); }

//  // close figvisu and TeX file
//  os << std::endl << "}" << b << "par" << b << "centerline{" << b << "box" << b << "mybox}";
//  os << std::endl << b << "bye";
//  os.close();
// }
// #endif /* FIG4TEX_OUTPUT */
// // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //

} // end of namespace xlifepp
