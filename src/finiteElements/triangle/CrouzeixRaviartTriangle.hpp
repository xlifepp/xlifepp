/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file CrouzeixRaviartTriangle.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 6 aug 2012

  \brief Definition of the xlifepp::CrouzeixRaviartTriangle class

  xlifepp::CrouzeixRaviartTriangle defines Crouzeix-Raviart non-conforming interpolation data

  member functions
    - interp   interpolation data
    - sideNumbering  local numbering on edges
    - pointCoordinates point coordinates

  child classes
    - xlifepp::CrouzeixRaviartStdTriangleP1 CrouzeixRaviart standard P1 Reference Element
*/

#ifndef CROUZEIX_RAVIART_TRIANGLE_HPP
#define CROUZEIX_RAVIART_TRIANGLE_HPP

#include "config.h"
#include "RefTriangle.hpp"

namespace xlifepp
{

/*!
  \class CrouzeixRaviartTriangle
  ( non-conforming elements )

  Parent class: RefTriangle
  Child classes: CrouzeixRaviartStdTriangleP1
 */
class CrouzeixRaviartTriangle : public RefTriangle
{
  public:
    CrouzeixRaviartTriangle(const Interpolation* interp_p); //!< constructor by interpolation
    virtual ~CrouzeixRaviartTriangle(); //!< destructor

  protected:
    void interpolationData(); //!< builds interpolation data
    void sideNumbering(); //!< builds side numbering

    virtual void pointCoordinates() = 0; //!< builds point coordinates

  private:
    number_t nbshfcts_;
    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class CrouzeixRaviartTriangle

/*!
  \class CrouzeixRaviartStdTriangleP1
  first order Crouzeix-Raviart element of triangle
 */
class CrouzeixRaviartStdTriangleP1: public CrouzeixRaviartTriangle
{
  public:
    CrouzeixRaviartStdTriangleP1(const Interpolation* interp_p); //!< constructor by interpolation
    ~CrouzeixRaviartStdTriangleP1(); //!< destructor
    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv=false) const;
    void pointCoordinates(); //!< builds point coordinates on reference element

}; // end of class CrouzeixRaviartStdTriangleP1

} // end of namespace xlifepp

#endif /* CROUZEIX_RAVIART_TRIANGLE_HPP */
