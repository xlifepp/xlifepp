/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ArgyrisTriangle.hpp
  \authors E. Lunéville
  \since 30 apr 2021
  \date  30 apr 2021

  \brief Definition of the xlifepp::ArgyrisTriangle class
         xlifepp::ArgyrisTriangle defines Argyris conforming H2 element (C1 continuity) on triangular elements
           (Mi) vertices of triangle, ni normal vector to edge i (opposite to Mi), Ni middle of edge i
           space: P5 ; dofs: {P(Mi),dxP(Mi),dyP(Mi),dxxP(Mi),xyP(Mi),dyyP(Mi) i=1,2,3 ; grad(P)(Ni).ni i=1,2,3}


                 7/12
                  |\   /n1    (XLiFE++ normal numbering following !!!)
             n2   | \ /
             ----19  21       (dofs follow Kirby order)
                  |   \
                  |    \
              13/18-20--1/6
                     |
                     |n3

   member functions
  ----------------
    - interp: interpolation data
    - sideNumbering: local numbering on edges (side)
    - sideDofsMap: side dofs mapping used when assembling
    - computeShapeValues: evaluate shape functions at a point
*/

#ifndef Argyris_TRIANGLE_HPP
#define Argyris_TRIANGLE_HPP

#include "config.h"
#include "RefTriangle.hpp"

namespace xlifepp
{
 class GeomMapData;  // forward class

/*!
  \class ArgyrisTriangle
  (non conforming H2 element)
  Parent class: RefTriangle
 */
class ArgyrisTriangle : public RefTriangle
{
  private:
    SparseMatrix<real_t> mw, mdx, mdy, mdxx, mdxy, mdyy;  //!< matrices of coefficients of shape functions
  public:
    ArgyrisTriangle(const Interpolation* interp_p);
    ArgyrisTriangle();
    virtual ~ArgyrisTriangle();
    void interpolationData(); //!< builds interpolation data
    void sideNumbering();     //!< numbering of side D.O.F's
    void pointCoordinates();  //!< builds virtual coordinates of moment dofs
    void setMatrices();       //!< build matrices matrices of coefficients of shape functions
    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv = true, const bool with2Deriv=true) const;
    number_t nbShapeFcts() const
     {return 6;}
    Matrix<real_t> affineMap(const GeomMapData&) const; //!< create the affine transformation matrix mapping reference shape function to any triangle
}; // end of class ArgyrisTriangle

} // end of namespace xlifepp

#endif /* Argyris_TRIANGLE_HPP */
