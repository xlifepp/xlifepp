/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file HermiteTriangle.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 19 nov 2004
  \date 6 aug 2012

  \brief Implementation of xlifepp::HermiteTriangle class members and related functions

  xlifepp::HermiteTriangle defines Hermite Reference Element interpolation data
 */

#include "HermiteTriangle.hpp"
#include "../Interpolation.hpp"
#include "utils.h"

namespace xlifepp
{

//! HermiteTriangle constructor for Hermite reference elements
HermiteTriangle::HermiteTriangle(const Interpolation* interp_p)
  : RefTriangle(interp_p)
{
  name_ += "_Her";
  trace_p->push("HermiteTriangle::HermiteTriangle (" + name_ + ")");
  interpolationData();
  // local numbering of points on sides
  sideNumbering();
  // Reference Element on sides
  sideRefElement();
  maxDegree = 3;

  trace_p->pop();
}

HermiteTriangle::~HermiteTriangle() {}

//! interpolationData defines Reference Element interpolation data
void HermiteTriangle::interpolationData()
{
  trace_p->push("HermiteTriangle::interpolationData");
  number_t i_n = interpolation_p->numtype;

  if ( i_n == 3 )  // Hermite triangle element of degree 3
  {
    nbDofs_ = 10;
    nbPts_ = 3; // 4 ?????
    nbInternalDofs_  = 1;
    nbDofsOnVertices_ = 9;
    // Creating Degrees Of Freedom of reference element
    refDofs.reserve(nbDofs_);
    for(number_t node = 1; node <= 3; node++ )
    {
        refDofs.push_back(new RefDof(this, true, _onVertex, node, 1, 1, 0, node, 1, 0, _noProjection,_id, "nodal value"));  // note ERIC:  I think is wrong !
        refDofs.push_back(new RefDof(this, true, _onVertex, node, 1, 2, 0, node, 1, 1, _noProjection,_dx, "derivative"));   // to be checked
        refDofs.push_back(new RefDof(this, true, _onVertex, node, 1, 3, 0, node, 1, 1, _noProjection,_dy, "derivative"));   // Hermite triangle has not been ever used !
    }
    refDofs.push_back(new RefDof(this, true, _onElement, 0, 1, 1, 0, 4, 1, 0, _noProjection,_id, "nodal value"));
    //hermiteRefDofs(1, 3, 2, 2);  lagrangeRefDofs(10, nbDofsOnVertices_, nbDofs_ - nbInternalDofs_, 2);
  }

  // compute number of shape functions
  nbshfcts_ = shapeValueSize();

  trace_p->pop();
}

//!  sideNumbering defines Reference Element local numbering on sides
void HermiteTriangle::sideNumbering()
{
  trace_p->push("HermiteTriangle::sideNumbering");
  number_t i_n = interpolation_p->numtype;

  number_t nbSides = geomRefElem_p->nbSides();
  number_t more = 1;

  switch (i_n)
  {
    case _P3: // Hermite triangle element of degree 3
      for ( number_t side = 0; side < nbSides; side++, more += nbSides)
      {
        sideDofNumbers_[side].push_back(more + 3);
        sideDofNumbers_[side].push_back(more + 4);
        sideDofNumbers_[side].push_back(more + 5);
        sideDofNumbers_[side].push_back(more);
        sideDofNumbers_[side].push_back(more + 1);
        sideDofNumbers_[side].push_back(more + 2);
      }
      // correction for first vertex of last edge (should be 'side+1 % nbSides')
      sideDofNumbers_[nbSides][0] = 1;
      sideDofNumbers_[nbSides][1] = 2;
      sideDofNumbers_[nbSides][2] = 3;
    default:
      break;
  }
  trace_p->pop();
}

//! HermiteTriangle constructor for Hermite standard reference elements
template<number_t Pk>
HermiteStdTriangle<Pk>::HermiteStdTriangle(const Interpolation* interp_p)
  : HermiteTriangle(interp_p)
{
  trace_p->push("HermiteTriangle::HermiteStdTriangle");
  name_ += "_" + tostring(Pk);
  // local coordinates of Points supporting D.o.F
  pointCoordinates();

  trace_p->pop();
}

template<number_t Pk>
HermiteStdTriangle<Pk>::~HermiteStdTriangle() {}

//! pointCoordinates defines Reference Element point coordinates
template<>
void HermiteStdTriangle<P3>::pointCoordinates()
{
  trace_p->push("HermiteStdTriangle<P3>::pointCoordinates");
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();
  // Hermite triangle element of degree 3
  (*it_rd++)->coords(1., 0.);
  (*it_rd++)->coords(1., 0.);
  (*it_rd++)->coords(1., 0.);
  (*it_rd++)->coords(0., 1.);
  (*it_rd++)->coords(0., 1.);
  (*it_rd++)->coords(0., 1.);
  (*it_rd++)->coords(0., 0.);
  (*it_rd++)->coords(0., 0.);
  (*it_rd++)->coords(0., 0.);
  (*it_rd)->coords(over3_, over3_);
  trace_p->pop();
}

//! computeShapeValues defines Hermite Reference Element shape functions
template<>
void HermiteStdTriangle<P3>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x1 = *it_pt, x2 = *(it_pt + 1), x3 = 1. - x1 - x2;
  real_t xx1 = x1 * (x3 - x2), xx2 = x2 * (x3 - x1), xx3 = x3 * (x2 - x1);
  real_t Twox1 = x1 + x1, Twox2 = x2 + x2, Twox3 = x3 + x3;
  real_t bubble = x1 * x2 * x3;

  std::vector<real_t>::iterator it_w(shv.w.begin());
  *it_w++ = x1 * x1 * (3. - Twox1) - 7 * bubble;
  *it_w++ = -x1 * xx3;
  *it_w++ = -x1 * xx2;
  *it_w++ = x2 * x2 * (3. - Twox2) - 7 * bubble;
  *it_w++ = -x2 * xx1;
  *it_w++ = x2 * xx3;
  *it_w++ = x3 * x3 * (3. - Twox3) - 7 * bubble;
  *it_w++ = x3 * xx2;
  *it_w++ = x3 * xx1;
  *it_w++ = 27 * bubble;

  if (withDeriv)
  {
    real_t Sevenxx1 = -7.*xx1;
    real_t Sevenxx2 = -7.*xx2;
    std::vector< std::vector<real_t> >::iterator it_dwdx1(shv.dw.begin()), it_dwdx2(it_dwdx1 + 1);
    std::vector<real_t>::iterator it_dwdx1i((*it_dwdx1).begin()), it_dwdx2i((*it_dwdx2).begin());
    *it_dwdx1i++ = 6.*x1 * (1. - x1) + Sevenxx2;  *it_dwdx2i++ = Sevenxx1;
    *it_dwdx1i++ = x1 * (Twox3 - x1) - xx2;       *it_dwdx2i++ = -x1 * x1 - xx1;
    *it_dwdx1i++ = x2 * (3.*x1 - x3);             *it_dwdx2i++ = x1 * x1 - xx1;
    *it_dwdx1i++ = Sevenxx2;                      *it_dwdx2i++ = 6.*x2 * (1. - x2) + Sevenxx1;
    *it_dwdx1i++ = x2 * x2 - xx2;                 *it_dwdx2i++ = x1 * (3 * x2 - x3);
    *it_dwdx1i++ = -x2 * x2 - xx2;                *it_dwdx2i++ = x2 * (Twox3 - x2) - xx1;
    *it_dwdx1i++ = -6.*x3 * (1. - x3) + Sevenxx2; *it_dwdx2i++ = -6.*x3 * (1. - x3) + Sevenxx1;
    *it_dwdx1i++ = x2 * (x1 - 3.*x3);             *it_dwdx2i++ = x3 * (x3 - Twox2) - xx1;
    *it_dwdx1i++ = x3 * (x3 - Twox1) - xx2;       *it_dwdx2i++ = x1 * (x2 - 3.*x3);
    *it_dwdx1i   = 27.*xx2;                       *it_dwdx2i++ = 27.*xx1;
  }
  /* ****
  {
     Twox3 = Twox3-1.;
     shapeValues.ddw[0][0]=14.*x2-12.*x1+6.;
     shapeValues.ddw[1][0]=-7.*Twox3;
     shapeValues.ddw[2][0]=14.*x1;
     shapeValues.ddw[0][1]=2.-6.*x1;
     shapeValues.ddw[1][1]=-Twox1-Twox3;
     shapeValues.ddw[2][1]=Twox2;
     shapeValues.ddw[0][2]=4.*x2;
     shapeValues.ddw[1][2]=Twox1-Twox3;
     shapeValues.ddw[2][2]=Twox1;
     shapeValues.ddw[0][3]=14*x2;
     shapeValues.ddw[1][3]=-7.*Twox3;
     shapeValues.ddw[2][3]=14.*x1-12.*x2+6.;
     shapeValues.ddw[0][4]=Twox2;
     shapeValues.ddw[1][4]=2*x2-Twox3;
     shapeValues.ddw[2][4]=4.*x1;
     shapeValues.ddw[0][5]=Twox2;
     shapeValues.ddw[1][5]=-Twox2-Twox3;
     shapeValues.ddw[2][5]=2.-6.*x2;
     shapeValues.ddw[0][6]=-6.*Twox3+14.*x2;
     shapeValues.ddw[1][6]=-13.*Twox3;
     shapeValues.ddw[2][6]=-6.*Twox3+14.*x1;
     shapeValues.ddw[0][7]=4.*x2;
     shapeValues.ddw[1][7]=x1+3.*(x2-x3);
     shapeValues.ddw[2][7]=2.-6.*x3;
     shapeValues.ddw[0][8]=2.-6.*x3;
     shapeValues.ddw[1][8]=x2+3.*(x1-x3);
     shapeValues.ddw[2][8]=4.*x1;
     shapeValues.ddw[0][9]=-54.*x2;
     shapeValues.ddw[1][9]=27.*Twox3;
     shapeValues.ddw[2][9]=-54.*x1;
  **** */
}

template class HermiteStdTriangle<_P3>;

} // end of namespace xlifepp
