/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RaviartThomasTriangle.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 06 aug 2012

  \brief Definition of the xlifepp::RaviartThomasTriangle class

  xlifepp::RaviartThomasTriangle defines Raviart-Thomas Hdiv-conforming Edge elements interpolation data on triangular elements

  member functions
  ----------------
    - interp: interpolation data
    - sideNumbering: local numbering on edges (side)s
    - sideDofsMap: side dofs mapping used when assembling
    - computeShapeFunctions: compute shape functions as polynomials
    - computeShapeValues: evaluate shape functions at a point

  child classes
  -------------
    - RaviartThomasStdTriangleP1 RaviartThomas standard P1 Reference Element
    - RaviartThomasStdTrianglePk RaviartThomas of any order

 */

#ifndef RAVIART_THOMAS_TRIANGLE_HPP
#define RAVIART_THOMAS_TRIANGLE_HPP

#include "config.h"
#include "RefTriangle.hpp"

namespace xlifepp
{

class FEinterpolation;

/*!
  \class RaviartThomasTriangle
  (Hdiv-conforming Edge elements)

  Parent class: RefTriangle
  Child classes: RaviartThomasStdTriangleP1
 */
class RaviartThomasTriangle : public RefTriangle
{
  public:
    RaviartThomasTriangle(const Interpolation* int_p);
    virtual ~RaviartThomasTriangle();
}; // end of class RaviartThomasTriangle

/*!
  \class RaviartThomasStdTriangleP1
 */
class RaviartThomasStdTriangleP1: public RaviartThomasTriangle
{
  public:
    RaviartThomasStdTriangleP1(const Interpolation* int_p);
    ~RaviartThomasStdTriangleP1();
    void interpolationData();        //!< defines reference element interpolation data
    void pointCoordinates();         //!< builds virtual coordinates of  dofs
    void sideNumbering();            //!< local numbering on edges (side)
    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv=true, const bool with2Deriv=false) const;
  private:
    number_t nbshfcts_;
    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class RaviartThomasStdTriangleP1


/*!
  \class RaviartThomasStdTrianglePk
  Raviart-Thomas element of any order k on triangle T  (RTtk, also known as Rao-Wilton-Glisson element)
     space  Vk: P^2_(k-1) + PH_(k-1)*(x1,x2),  dim Vk = k(k+2)  (PHk homogeneous polynomials of order k),
     edge dofs: v-> int_e v.n q, q in P_(k-1)[e]    k dofs by edge e
     triangle dofs: v-> int_t v.q,  q in P_(k-2)[t]^2   k(k-1) dofs  only for k>1
 */
class RaviartThomasStdTrianglePk : public RaviartThomasTriangle
{
  public:
    RaviartThomasStdTrianglePk(const Interpolation* int_p);
    ~RaviartThomasStdTrianglePk();
    void interpolationData();                    //!< defines reference element interpolation data
    void sideNumbering();                        //!< local numbering on edges (side)
    void pointCoordinates();                     //!< builds virtual coordinates of moment dofs
    number_t sideDofsMap(const number_t& n, const number_t& i,
                         const number_t& j, const number_t& k=0) const;  //!< side dofs mapping
    void computeShapeFunctions();                     //!< compute shape functions as polynomials
    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv=true, const bool with2Deriv=false) const;
  private:
    number_t nbshfcts_;
    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class RaviartThomasStdTrianglePk

//====================================================================================
//                        BuffaChristiansenRT class definition
//====================================================================================
/*!
    \class BuffaChristiansenRT
    ref element inherits from RT ref element but declare no shapevalues
    ShapeValues are directly computed from BCDof
*/
class BuffaChristiansenRT : public RaviartThomasStdTriangleP1
{
  public:
    BuffaChristiansenRT(const Interpolation* int_p)
    : RaviartThomasStdTriangleP1(int_p){name_="Buffa-Christiansen_1";hasShapeValues=false;dofCompatibility=_noDofCompatibility;}
    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv=true, const bool with2Deriv=false) const
    { error("not_handled","BuffaChristiansenRT::computeShapeValues(Reals::iterator, ShapeValues, bool, bool)"); }
}; // end of class BuffaChristiansenRT

} // end of namespace xlifepp

#endif /* RAVIART_THOMAS_TRIANGLE_HPP */
