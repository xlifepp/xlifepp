/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomRefTriangle.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 15 dec 2002
  \date 6 aug 2012

  \brief Implementation of xlifepp::GeomRefTriangle class members and related functions
 */

#include "GeomRefTriangle.hpp"
#include "utils.h"

namespace xlifepp
{

//! GeomRefTriangle constructor for Geometric Reference Element using 2d base constructor with shape, surface, centroid coords, number of vertices
GeomRefTriangle::GeomRefTriangle()
  : GeomRefElement(_triangle, 0.5, over3_, 3)
{
  trace_p->push("GeomRefTriangle::GeomRefTriangle");

  // coordinates of vertices
  std::vector<real_t>::iterator it_v(vertices_.begin());
  vertex(it_v, 1., 0.);
  vertex(it_v, 0., 1.);
  vertex(it_v, 0., 0.);
  // vertex numbering on sides
  sideNumbering();
  sideOfSideNumbering();

  trace_p->pop();
}

GeomRefTriangle::~GeomRefTriangle() {}

//! sideNumbering defines Geometric Reference Element local numbering of vertices on edges
void GeomRefTriangle::sideNumbering()
{
  for (number_t i = 0; i < nbSides_; i++)
  {
    sideShapeTypes_[i] = _segment;
  }
  sideVertexNumbers_[0].push_back(1);
  sideVertexNumbers_[0].push_back(2);
  sideVertexNumbers_[1].push_back(2);
  sideVertexNumbers_[1].push_back(3);
  sideVertexNumbers_[2].push_back(3);
  sideVertexNumbers_[2].push_back(1);
}

//! sideOfSideNumbering defines Geometric Reference Element local numbering of vertices on edges
void GeomRefTriangle::sideOfSideNumbering()
{
  sideOfSideVertexNumbers_[0].push_back(2);
  sideOfSideVertexNumbers_[1].push_back(1);
  sideOfSideVertexNumbers_[2].push_back(3);

  sideOfSideNumbers_[0].push_back(1);
  sideOfSideNumbers_[0].push_back(2);
  sideOfSideNumbers_[1].push_back(3);
  sideOfSideNumbers_[1].push_back(1);
  sideOfSideNumbers_[2].push_back(2);
  sideOfSideNumbers_[2].push_back(3);
}

//! returns edge length or element area
real_t GeomRefTriangle::measure(const dimen_t d, const number_t sideNum) const
{
  real_t ms(1.);
  switch ( d )
  {
    case 1:
      switch ( sideNum )
      {
        case 1: ms = sqrtOf2_; break;
        default: break;
      }
      break;
    case 2: ms = measure_;
    default: break;
  }
  return ms;
}

//! returns a quadrature rule build on an edge form a 1d quadrature formula
//void GeomRefTriangle::sideQuadrature(const QuadratureRule& q1, QuadratureRule& qr, const number_t sideNum, const dimen_t d) const {
//  /*
//    q1 : input 1d  quadrature rule
//    q2 : ouput 2d quadrature rule set on the edge
//    sideNum: local number of edge (sideNum = 1,2,3)
//    d: = 1 !
//   */
//
//  std::vector<real_t>::const_iterator c_1(q1.point()), w_1(q1.weight());
//  std::vector<real_t>::iterator c_i(qr.point()), w_i(qr.weight());
//
//  switch ( d ) {
//    case 1:
//      switch ( sideNum ) {
//        case 1: // edge x + y = 1 oriented from (1,0) -> (0,1)
//          for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1++ ) {
//            qr.point(c_i, *c_1, 1.-*c_1, w_i, *w_1 * sqrt_of_2_);
//          }
//          break;
//        case 2: // edge x = 0 oriented from (1,0) -> (0,0)
//          for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1++ ) {
//            qr.point(c_i, 0., *c_1, w_i, *w_1);
//          }
//          break;
//        case 3: // edge y = 0 oriented from (0,0) -> (1,0)
//          for ( size_t i = 0; i < q1.size(); i++, w_1++, c_1++ ) {
//            qr.point(c_i, *c_1, 0., w_i, *w_1);
//          }
//          break;
//        default: noSuchEdge(sideNum); break;
//      }
//      break;
//    default:
//      break;
//  }
//}

//! returns tangent vector(s) on edge sideNum (=1,2,3)
void GeomRefTriangle::tangentVector(const std::vector<real_t>& jacobianMatrix, std::vector<std::vector<real_t> >& tv, const number_t sideNum, const dimen_t d) const
{
  std::vector<real_t>::iterator it_tv(tv[0].begin());
  std::vector<real_t>::const_iterator it_jm(jacobianMatrix.begin());

  switch (d)
  {
    case 1:
      switch (sideNum)
      {
        case 1: // edge x + y = 1
          for ( it_jm = jacobianMatrix.begin(); it_jm != jacobianMatrix.end(); it_jm += 2 )
          {
            *it_tv++ = (*(it_jm + 1) - *it_jm) / sqrtOf2_;
          }
          break;
        case 2: // edge x = 0
          for ( it_jm = jacobianMatrix.begin(); it_jm != jacobianMatrix.end(); it_jm += 2 )
          {
            *it_tv++ = -*(it_jm + 1);
          }
          break;
        case 3: // edge y = 0
          for ( it_jm = jacobianMatrix.begin(); it_jm != jacobianMatrix.end(); it_jm += 2 )
          {
            *it_tv++ = *it_jm;
          }
          break;
        default: noSuchSide(sideNum); break;
      }
      break;
    default:
      *it_tv++ = 0; *it_tv = 0.;
      break;
  }
}

//! Returns the local number of an edge bearing 2 vertices given by their local numbers (>=1)
number_t GeomRefTriangle::sideWithVertices(const number_t vn1, const number_t vn2) const
{
  if(vn1==vn2) noSuchSide(vn1,vn2);
  number_t v1=vn1, v2=vn2;
  if(v1>v2) {v1=vn2;v2=vn1;}
  if(v1==1)
  {
      if(v2==2) return 1;
      if(v2==3) return 3;
      noSuchSide(vn1,vn2);
  }
  if(v1==2 && v2==3) return 2;
  noSuchSide(vn1,vn2);
  return 0;
}

//! test if a point belongs to current element
bool GeomRefTriangle::contains(std::vector<real_t>& p, real_t tol) const
{
  real_t x=p[0], y=p[1];
  return (x >= -tol) && (x <= 1.+tol) && (y >= -tol) && (y <= 1.+tol) && (x+y <= 1.+tol) && (x+y>=-tol);
}

//! return projection on triangle and distance to triangle (h)
std::vector<real_t> GeomRefTriangle::projection(const std::vector<real_t>& p, real_t& h) const
{
  return projectionOnTriangle(p,Point(0.,0.),Point(1.,0.),Point(0.,1.), h);
}

} // end of namespace xlifepp
