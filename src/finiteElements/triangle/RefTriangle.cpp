/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RefTriangle.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 15 dec 2002
  \date 06 aug 2012

  \brief Implementation of xlifepp::RefTriangle class members and related functions

  xlifepp::RefTriangle defines Reference Element interpolation data on triangular elements
 */

#include "RefTriangle.hpp"
#include "LagrangeTriangle.hpp"
#include "HermiteTriangle.hpp"
#include "CrouzeixRaviartTriangle.hpp"
#include "NedelecTriangle.hpp"
#include "RaviartThomasTriangle.hpp"
#include "MorleyTriangle.hpp"
#include "ArgyrisTriangle.hpp"

#include "../Interpolation.hpp"
#include "utils.h"

namespace xlifepp
{

//! selectReferenceTriangle construction of a Reference Element by interpolation type and interpolation subtype
RefElement* selectRefTriangle(const Interpolation* interp_p)
{
  switch (interp_p->type)
  {
    case _Lagrange:
      switch (interp_p->subtype)
      {
        case _standard: return triangleLagrangeStd(interp_p); break;
        default: interp_p->badSubType(_triangle); break;
      }
      break;
    case Hermite:
      switch (interp_p->subtype)
      {
        case _standard: return triangleHermiteStd(interp_p); break;
        default: interp_p->badSubType(_triangle); break;
      }
      break;
    case _CrouzeixRaviart:
      switch (interp_p->subtype)
      {
        case standard: return triangleCrouzeixRaviartStd(interp_p); break;
        default: interp_p->badSubType(_triangle); break;
      }
      break;
    case _Nedelec:
    case _NedelecEdge:
    case _NedelecFace:
      switch (interp_p->subtype)
      {
        case _firstFamily: return triangleNedelec(interp_p); break;
        case _secondFamily: return triangleNedelec(interp_p); break;
        default: interp_p->badSubType(_triangle); break;
      }
      break;
    case _RaviartThomas:
    case _BuffaChristiansen:  //Buffa-Christiansen inherits from Raviart-Thomas
      switch (interp_p->subtype)
      {
        case _standard: return triangleRaviartThomasStd(interp_p); break;
        default: interp_p->badSubType(_triangle); break;
      }
      break;
    case _Morley: return triangleMorley(interp_p); break;
    case _Argyris: return triangleArgyris(interp_p); break;
    default: break;
  }

  // Throw error messages
  trace_p->push("selectReferenceTriangle");
  interp_p->badType(_triangle);
  trace_p->pop();
  return nullptr;
}

//! triangleLagrangeStd construction of a Lagrange standard Reference Element by interpolation number
RefElement* triangleLagrangeStd(const Interpolation* interp_p)
{
  switch (interp_p->numtype)
  {
    case _P0: return new LagrangeStdTriangle<_P0>(interp_p); break;
    case _P1: return new LagrangeStdTriangle<_P1>(interp_p); break;
    case _P2: return new LagrangeStdTriangle<_P2>(interp_p); break;
    case _P3: return new LagrangeStdTriangle<_P3>(interp_p); break;
    case _P4: return new LagrangeStdTriangle<_P4>(interp_p); break;
    case _P5: return new LagrangeStdTriangle<_P5>(interp_p); break;
    case _P6: return new LagrangeStdTriangle<_P6>(interp_p); break;
    case _P1BubbleP3: return new LagrangeStdTriangle<_P1BubbleP3>(interp_p); break;
    default:  return new LagrangeStdTrianglePk(interp_p); break;
  }
  return nullptr;
}

//! triangleHermiteStd construction of a Hermite standard Reference Element by interpolation number
RefElement* triangleHermiteStd(const Interpolation* interp_p)
{
  switch (interp_p->numtype)
  {
    case _P3: return new HermiteStdTriangle<_P3>(interp_p); break;
    default:
      trace_p->push("triangleHermiteStd");
      interp_p->badDegree(_triangle);
      trace_p->pop();
      break;
  }
  return nullptr;
}

//! triangleCrouzeixRaviartStd construction of a Crouzeix_Raviart standard Reference Element by interpolation number
RefElement* triangleCrouzeixRaviartStd(const Interpolation* interp_p)
{
  switch ( interp_p->numtype )
  {
    case 1: return new CrouzeixRaviartStdTriangleP1(interp_p); break;
    default:
      trace_p->push("triangleCrouzeixRaviartStd");
      interp_p->badDegree(_triangle);
      trace_p->pop();
      break;
  }
  return nullptr;
}

//! triangleNedelec construction of a Nedelec standard Reference Element by interpolation number
RefElement* triangleNedelec(const Interpolation* interp_p)
{
  switch (interp_p->subtype)
  {
    case _firstFamily: if(interp_p->numtype == 1) return new NedelecFirstTriangleP1(interp_p);  //particular case
                     else return new NedelecFirstTrianglePk(interp_p);   //assume first family
    case _secondFamily: return new NedelecSecondTrianglePk(interp_p);
    default:
      trace_p->push("triangleNedelec");
      interp_p->badDegree(_triangle);
      trace_p->pop();
      break;
  }
  return nullptr;
}

//! triangleRaviart_ThomasStd construction of a Raviart_Thomas standard Reference Element by interpolation number
RefElement* triangleRaviartThomasStd(const Interpolation* interp_p)
{
  if(interp_p->type ==_BuffaChristiansen) return new BuffaChristiansenRT(interp_p);
  switch (interp_p->numtype)
  {
    case 1: return new RaviartThomasStdTriangleP1(interp_p); break;
    default: return new RaviartThomasStdTrianglePk(interp_p);
  }
  return nullptr;
}

//! triangleMorley construction of the Morley Reference Element
RefElement* triangleMorley(const Interpolation* interp_p)
{
   return new MorleyTriangle(interp_p);
}

//! triangleMorley construction of the Morley Reference Element
RefElement* triangleArgyris(const Interpolation* interp_p)
{
   return new ArgyrisTriangle(interp_p);
}

} // end of namespace xlifepp
