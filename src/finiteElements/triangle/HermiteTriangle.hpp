/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file HermiteTriangle.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 6 aug 2012

  \brief Definition of the xlifepp::HermiteTriangle class

  xlifepp::HermiteTriangle defines Hermite Reference Element interpolation data on triangular elements

  member functions
  ----------------
    - interpolationData interpolation data
    - sideNumbering  local numbering on edges
    - pointCoordinates  point coordinates

  child classes
  -------------
    - xlifepp::HermiteStdTriangle Lagrange standard Reference Element
 */

#ifndef HERMITE_TRIANGLE_HPP
#define HERMITE_TRIANGLE_HPP

#include "config.h"
#include "RefTriangle.hpp"

namespace xlifepp
{

class Interpolation;

/*!
  \class HermiteTriangle
  defines Hermite Reference Element interpolation data on triangular elements

  Parent class  : RefTriangle
  Child classes: HermiteStdTriangle
 */
class HermiteTriangle : public RefTriangle
{
  public:
    HermiteTriangle(const Interpolation* interp_p); //!< constructor by interpolation
    virtual ~HermiteTriangle(); //!< destructor

  protected:
    void interpolationData(); //!< builds interpolation data on reference element
    void sideNumbering(); //!< builds side numbering

    virtual void pointCoordinates() = 0; //!< builds point coordinates
  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }

}; // end of class HermiteTriangle

/*!
  \class HermiteStdTriangle
  template class child to class HermiteTriangle
 */
template<number_t Pk>
class HermiteStdTriangle : public HermiteTriangle
{
  public:
    HermiteStdTriangle(const Interpolation* interp_p); //!< constructor by interpolation
    ~HermiteStdTriangle(); //!< destructor

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv=false) const; //!< shape functions
    void pointCoordinates(); //!< builds point coordinates on reference element

}; // end of class HermiteStdTriangle

} // end of namespace xlifepp

#endif /* HERMITE_TRIANGLE_HPP */
