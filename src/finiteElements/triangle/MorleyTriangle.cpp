/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file MorleyTriangle.cpp
  \authors E. Lunéville
  \since 30 apr 2021
  \date  30 apr 2021

  \brief Implementation of xlifepp::MorleyTriangle class members and related functions
 */

#include "MorleyTriangle.hpp"
#include "geometry.h"
#include "utils.h"
#include <iostream>

namespace xlifepp
{

//! MorleyTriangle constructor for Morley reference elements
MorleyTriangle::MorleyTriangle(const Interpolation* interp_p)
  : RefTriangle(interp_p)
{
  name_ += "_Morley";
  trace_p->push("MorleyTriangle::MorleyTriangle (" + name_ + ")");
  interpolationData();
  sideNumbering();
  pointCoordinates();
  mapType = _MorleyMap;
  dofCompatibility=_signDofCompatibility;
  maxDegree = 2;
  trace_p->pop();
}
MorleyTriangle::MorleyTriangle(): RefTriangle(findInterpolation(_Morley, _standard, 1, H1))
{
  name_ += "_Morley";
  trace_p->push("MorleyTriangle::MorleyTriangle (" + name_ + ")");
  interpolationData();
  sideNumbering();
  pointCoordinates();
  mapType = _MorleyMap;
  dofCompatibility=_signDofCompatibility;
  maxDegree = 2;
  trace_p->pop();
}

MorleyTriangle::~MorleyTriangle() {}

//! interp defines Reference Element interpolation data
void MorleyTriangle::interpolationData()
{
  trace_p->push("MorleyTriangle::interpolationData");
  nbDofsInSides_ = 3;
  nbDofsOnVertices_ = 3;
  nbDofs_ = 6;
  // Creating Degrees Of Freedom of reference element
  // in Morley standard elements all D.o.F on edges are sharable, no internal D.o.F
  refDofs.reserve(nbDofs_);
  for(number_t i = 1; i <= 3; i++ )
       refDofs.push_back(new RefDof(this, true, _onVertex, i, 1, 2, 0, i, 1, 0, _noProjection, _id, "nodal value"));
  for(number_t s = 1; s <= 3; s++ )
       refDofs.push_back(new RefDof(this, true, _onEdge, (s<3?s+1:1), 1, 2, 0, s, 1, 1, _dotnProjection, _ndotgrad, "normal derivative"));
  nbPts_ = nbDofs_;
  trace_p->pop();
}

//! sideNumbering defines Reference Element local numbers on sides
void MorleyTriangle::sideNumbering()
{
  trace_p->push("MorleyTriangle::sideNumbering");
  sideDofNumbers_.resize(3,std::vector<number_t>(3,0));
  sideDofNumbers_[0][0] = 1; sideDofNumbers_[0][1] = 2; sideDofNumbers_[0][2] = 6;
  sideDofNumbers_[1][0] = 2; sideDofNumbers_[1][1] = 3; sideDofNumbers_[1][2] = 4;
  sideDofNumbers_[2][0] = 3; sideDofNumbers_[2][1] = 1; sideDofNumbers_[2][2] = 5;
  trace_p->pop();
}

//! pointCoordinates defines Morley Reference Element point coordinates
void MorleyTriangle::pointCoordinates()
{
  trace_p->push("MorleyTriangle::pointCoordinates");
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();
  (*it_rd++)->coords(1.,0.);
  (*it_rd++)->coords(0.,1.);
  (*it_rd++)->coords(0.,0.);
  (*it_rd++)->coords(0.,0.5);
  (*it_rd++)->coords(0.5,0);
  (*it_rd)->coords(0.5,0.5);
  trace_p->pop();
}

/*! Morley shape functions
   w1(s,y)=0.5(x+y-2xy+x^2-y^2) w2(s,y)=0.5(x+y-2xy+y^2-x^2) w3(x,y)=1-x-y+2xy
   w4(x,y)=x(x-1) w5(x,y)=y(y-1) w6(x,y)=1/sqr(2)((x+y)^2-(x+y))
*/
void MorleyTriangle::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x = *it_pt, y = *(it_pt + 1), xpy=x+y, xy=x*y, x2=x*x, y2=y*y;
  std::vector<real_t>::iterator it_w(shv.w.begin());
  *it_w++ = 0.5*(xpy-2*xy+x2-y2);      // w_1
  *it_w++ = 0.5*(xpy-2*xy+y2-x2);      // w_2
  *it_w++ = 1-xpy+2*xy;                // w_3
  *it_w++ = x2-x;                      // w_4
  *it_w++ = y2-y;                      // w_5
  *it_w   = 0.5*sqrtOf2_*(xpy*xpy-xpy);// w_6
  if ( withDeriv )
  {
    std::vector< std::vector<real_t> >::iterator it_dxw=shv.dw.begin(), it_dyw=it_dxw + 1;
    std::vector<real_t>::iterator it_dxwi=it_dxw->begin(), it_dywi=it_dyw->begin();
    real_t xmy=x-y, dx=2*x-1, dy=2*y-1;
    *it_dxwi++ = 0.5+xmy;            *it_dywi++ = 0.5-xpy;           // grad w_1
    *it_dxwi++ = 0.5-xpy;            *it_dywi++ = 0.5-xmy;           // grad w_2
    *it_dxwi++ = dy;                 *it_dywi++ = dx;                // grad w_3
    *it_dxwi++ = dx;                 *it_dywi++ = 0.;                // grad w_4
    *it_dxwi++ = 0.;                 *it_dywi++ = dy;                // grad w_5
    *it_dxwi   = sqrtOf2_*(xpy-0.5); *it_dywi   = sqrtOf2_*(xpy-0.5);// grad w_6
  }
  if ( with2Deriv )
  {
    std::vector< std::vector<real_t> >::iterator it_dxxw=shv.d2w.begin(), it_dyyw=it_dxxw + 1, it_dxyw=it_dxxw + 2;
    std::vector<real_t>::iterator it_dxxwi=it_dxxw->begin(), it_dyywi=it_dyyw->begin(), it_dxywi=it_dxyw->begin();
    *it_dxxwi++ = 1.;       *it_dyywi++ = -1.;      *it_dxywi++ = -1.;      // (d2x, d2y, dxy)w_1
    *it_dxxwi++ = -1.;      *it_dyywi++ = 1.;       *it_dxywi++ = -1.;      // (d2x, d2y, dxy)w_2
    *it_dxxwi++ = 0.;       *it_dyywi++ = 0.;       *it_dxywi++ = 2.;       // (d2x, d2y, dxy)w_3
    *it_dxxwi++ = 2.;       *it_dyywi++ = 0.;       *it_dxywi++ = 0.;       // (d2x, d2y, dxy)w_4
    *it_dxxwi++ = 0.;       *it_dyywi++ = 2.;       *it_dxywi++ = 0.;       // (d2x, d2y, dxy)w_5
    *it_dxxwi   = sqrtOf2_; *it_dyywi   = sqrtOf2_; *it_dxywi   = sqrtOf2_; // (d2x, d2y, dxy)w_6
  }
}

/*!  create the affine transformation matrix mapping reference shape function to any triangle
     triangle and any related data are handled by the GeomMapData object (have to be set up before)
     2D Morley transformation mapping ReferenceElement shape functions (s) unto current element,
      from Kirby R.C."A general approach to transforming finite elements".The SMAI journal of computational mathematics, Tome 4 (2018),197-224.doi:10.5802/smai-jcm.33.
     w1 = s1-a2s5-a3s6  w2 = s2-a1s4+a3s6  w3 = s3+a1s4+a2s5  w4=d1s4  w5=d2s5  w6=d3s6
     a1 = (Q1)_12/L1  a2 = (Q2)_12/L2  a3 = (Q3)_12/L3  di=(Qi)_11
     Qi = [ni ti]t Jt [nui taui]
          (ni, ti)   outward normal and tangent vector (pi/2 rotation of normal) on edge i of the reference triangle
          (nui taui) outward normal and tangent vector (pi/2 rotation of normal) on edge i of the current triangle
          Li length of edge i of the current triangle
     here edge i is the edge which does not contain vi vertex
     i=1 : n=(-1,0), t2=(0,-1) => Q12=J12*ny-J22*nx, Q11=-J21*ny-j11*nx
     i=2 : n=(0,-1), t2=(1,0)  => Q12=J11*ny-J21*nx, Q11=-J12*nx-j22*ny
     i=3 : n1=sqrt(2)/2(1,1), t1=0.5*sqrt(2)(-1,1) => Q12=sqrt(2)/2((J21+J22)*nx-(J11+J12)*ny), Q11=sqrt(2)/2(J11+J12)*nx+(J22+J21)*ny)
                                       |1 0 0  0  -a2 -a3|
                                       |0 1 0 -a1  0   a3|
      affine transformation matrix M = |0 0 1  a1  a2  0 |
                                       |0 0 0  d1  0   0 |
                                       |0 0 0  0   d2  0 |
                                       |0 0 0  0   0   d3|

     gd: geometric data (contains the inverse of jacobian and accessto length)
     out: matrix M
*/
Matrix<real_t> MorleyTriangle::affineMap(const GeomMapData& gd) const
{
   std::vector< Vector<real_t> >& ns=gd.sideNV();
   const Matrix<real_t>& J=gd.jacobianMatrix;
   real_t j11=J(1,1), j12=J(1,2),j21=J(2,1),j22=J(2,2);
   real_t a1= (j11*ns[1][1]-j21*ns[1][0])/gd.measures(2), d1=-(j21*ns[1][1]+j11*ns[1][0]);
   real_t a2= (j22*ns[2][0]-j12*ns[2][1])/gd.measures(3), d2=-(j12*ns[2][0]+j22*ns[2][1]);
   real_t a3= 0.5*sqrtOf2_*((j21+j22)*ns[0][0]-(j11+j12)*ns[0][1])/gd.measures(1),
          d3= 0.5*sqrtOf2_*((j11+j12)*ns[0][0]+(j22+j21)*ns[0][1]);
   Matrix<real_t> M(6,_idMatrix);
   M(4,4)=d1;M(5,5)=d2;M(6,6)=d3;
   M(1,5)=-a2;M(1,6)=-a3;
   M(2,4)=-a1;M(2,6)= a3;
   M(3,4)= a1;M(3,5)= a2;
   return M;
}

} // end of namespace xlifepp
