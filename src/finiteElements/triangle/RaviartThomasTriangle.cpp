/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RaviartThomasTriangle.cpp
  \authors D. Martin, N. Kielbasiewicz, E. Lunéville
  \since 23 nov 2004
  \date  31 jan 2015

  \brief Implementation of xlifepp::RaviartThomasTriangle class members and related functions
 */

#include "RaviartThomasTriangle.hpp"
#include "../Interpolation.hpp"
#include "../integration/Quadrature.hpp"
#include "../segment/LagrangeSegment.hpp"
#include "utils.h"

#include <iostream>

namespace xlifepp
{

//! RaviartThomasTriangle constructor for Raviart-Thomas reference elements
RaviartThomasTriangle::RaviartThomasTriangle(const Interpolation* interp_p)
    : RefTriangle(interp_p)
{
  name_ += "_Raviart-Thomas";
  mapType = _contravariantPiolaMap;
  dimShapeFunction=2;
  dofCompatibility=_signDofCompatibility;
  // *NO* Reference Element on edges   side_interpolations_ = edge_interpolations_
}

RaviartThomasTriangle::~RaviartThomasTriangle() {}

//! RaviartThomas standard P1 triangle Reference Element
RaviartThomasStdTriangleP1::RaviartThomasStdTriangleP1(const Interpolation* interp_p)
    : RaviartThomasTriangle(interp_p)
{
  name_ += "_1";
  interpolationData();  // build element interpolation data
  sideNumbering();      // local numbering of points on edges
  pointCoordinates();   // "virtual coordinates" of dofs
  maxDegree = 1;
}

RaviartThomasStdTriangleP1::~RaviartThomasStdTriangleP1() {}

//! interp defines Reference Element interpolation data
void RaviartThomasStdTriangleP1::interpolationData()
{
  trace_p->push("RaviartThomasTriangle::interpolationData");
  nbDofsInSides_ = nbDofs_ = 3;
  // Creating Degrees Of Freedom of reference element, in RaviartThomas standard elements all D.o.F on edges are sharable
  refDofs.reserve(nbDofs_);
  for (number_t s = 1; s <= 3; s++ )
    //refDofs.push_back(new RefDof(this, true, _onEdge, s, 1, 2, 1, 0, 2, 0, _dotnProjection, _ndot, "int_e u.nq")); //considered as integral dof
    refDofs.push_back(new RefDof(this, true, _onEdge, s, 1, 2, 0, 0, 2, 0, _dotnProjection, _ndot, "int_e u.nq"));   //considered as ponctual dof
  // compute number of shape functions
  nbshfcts_ = shapeValueSize();
  nbPts_ = nbDofs_;
  trace_p->pop();
}

/* builds virtual coordinates of moment dofs of order k
     for side dofs: {1/2}
*/
void RaviartThomasStdTriangleP1::pointCoordinates()
{
  std::vector<RefDof*>::iterator it_rd=refDofs.begin();
  (*it_rd++)->coords(0.5,0.5);
  (*it_rd++)->coords(0.,0.5);
  (*it_rd++)->coords(0.5,0.);
}

//! sideNumbering defines Reference Element local numbers on sides
void RaviartThomasStdTriangleP1::sideNumbering()
{
  trace_p->push("RaviartThomasTriangle::sideNumbering");
  /*
    Local numbering of RaviartThomas k=1
          *
          | \
          2   1
          |     \
          *---3---*
   */
  sideDofNumbers_.resize(3);
  for ( number_t s = 0; s < 3; s++ ) sideDofNumbers_[s].push_back(s + 1);
  trace_p->pop();
}


void RaviartThomasStdTriangleP1::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  /*
    w_1 = (x, y) , w_2 = (x-1, y) , w_3 = (x, y-1)
   */
  real_t x1 = *it_pt, x2 = *(it_pt + 1);
  std::vector<real_t>::iterator it_w=shv.w.begin();
  *it_w++ = x1;
  *it_w++ = x2; // w_1
//  *it_w++ = x1*sqrtOf2_;
//  *it_w++ = x2*sqrtOf2_; // w_1 (unitary normal)
  *it_w++ = x1 - 1;
  *it_w++ = x2; // w_2
  *it_w++ = x1;
  *it_w   = x2 - 1; // w_3
  if ( withDeriv )
  {
    std::vector<std::vector<real_t> >::iterator it_dwdx1=shv.dw.begin(), it_dwdx2=it_dwdx1 + 1;
    std::vector<real_t>::iterator it_dwdx1i=(*it_dwdx1).begin(), it_dwdx2i=(*it_dwdx2).begin();
    *it_dwdx1i++ = 1.;
    *it_dwdx2i++ = 0.;
    *it_dwdx1i++ = 0.;
    *it_dwdx2i++ = 1.; // grad w_1
//    *it_dwdx1i++ = sqrtOf2_;
//    *it_dwdx2i++ = 0.;
//    *it_dwdx1i++ = 0.;
//    *it_dwdx2i++ = sqrtOf2_; // grad w_1 (unitary normal)
    *it_dwdx1i++ = 1.;
    *it_dwdx2i++ = 0.;
    *it_dwdx1i++ = 0.;
    *it_dwdx2i++ = 1.; // grad w_2
    *it_dwdx1i++ = 1.;
    *it_dwdx2i++ = 0.;
    *it_dwdx1i   = 0.;
    *it_dwdx2i   = 1.; // grad w_3
  }
}

//=====================================================================================
/*! RaviartThomas Pk triangle Reference Element (any order)

        *             *
        | \           | \
        2   1       3 *   * 2
        |     \       |     \           dof's side numbering
        *---3---*   4 *  *7   * 1
          k=1         |    *8   \
                      *---*---*---*
                          5   6
                           k=2
        2
        | \
        2   1        edge orientation in dof's integral computation on edge
        |     \
        3---3---1         e1 : 1->2,  e2 : 1->3   e3 : 3->1
           ->
*/
//=====================================================================================
RaviartThomasStdTrianglePk::RaviartThomasStdTrianglePk(const Interpolation* interp_p)
    : RaviartThomasTriangle(interp_p)
{
  name_ += "_"+tostring(interp_p->numtype);
  interpolationData();  // build element interpolation data
  sideNumbering();      // local numbering of points on edges
  pointCoordinates();   // "virtual coordinates" of moment dofs
}

RaviartThomasStdTrianglePk::~RaviartThomasStdTrianglePk() {}

//! interp defines Reference Element interpolation data
void RaviartThomasStdTrianglePk::interpolationData()
{
  trace_p->push("RaviartThomasStdTrianglePk::interpolationData");

  number_t k = interpolation_p->numtype;
  nbDofs_= k*(k+2);
  nbDofsInSides_=3*k;
  nbInternalDofs_=k*(k-1);

  /* Creating Degrees Of Freedom of reference element
     in RaviartThomas order k elements, D.o.Fs are defined as moment D.o.Fs
        D.o.F on edges: v-> int_e v.n q, q in P_(k-1)[e]      k dofs by edge e
        internal D.o.F: : v-> int_t v.q,  q in P_(k-2)[t]^2   k(k-1) dofs  only for k>1
     numbering of D.oFs are given by the basis functions order of dual spaces defining D.o.Fs
     NOTE: moment D.o.Fs can be reinterpreted as punctual D.o.Fs using a well adapted quadrature formulae
            we do not use this representation here
  */
  number_t nbds=nbDofsInSides_/3;
  refDofs.reserve(nbDofs_);
  for (number_t e = 1; e <= 3; ++e )        //edge dofs (rank in polynoms basis is used to identify dof)
    for(number_t i=0; i<nbds; ++i)
      refDofs.push_back(new RefDof(this, true, _onEdge, e, i+1, 2, 1, 0, 2, 0, _dotnProjection, _ndot, "int_e u.n q"));
  for(number_t i=0; i<nbInternalDofs_; ++i) //element dofs (order = i is used to identify dof)
    refDofs.push_back(new RefDof(this, false, _onElement, 0, i+1, 2, 2, 0, 2, 0, _noProjection, _id, "int_k u.q"));

  // compute shape functions as polynomials
  computeShapeFunctions();
  buildPolynomialTree();   //construct tree representation
  maxDegree = Wk.degree();

  // compute number of shape functions
  nbshfcts_ = shapeValueSize();

  trace_p->pop();
}

//! compute shape functions as polynomials using general algorithm
void RaviartThomasStdTrianglePk::computeShapeFunctions()
{
  number_t k = interpolation_p->numtype;
  PolynomialsBasis::iterator itp;

  //compute Matrix Lij=l(pi,qj) for all pi in Vk, qj in P_(k-1)[e], edge D.o.Fs:  v-> int_e v.n q, q in P_(k-1)[e]
  PolynomialsBasis Vk(_Dk,2,k);       //Raviart-Thomas polynomials space
  LagrangeStdSegment segPk(findInterpolation(_Lagrange, _standard, k-1, H1));
  segPk.computeShapeFunctions();
  Ek=segPk.Wk;       //create P_(k-1)[x]
  PolynomialsBasis::iterator itq;
  dimen_t d=Vk.degree()+Ek.degree();
  QuadRule bestQR = Quadrature::bestQuadRule(_segment,d);
  Quadrature* quad=findQuadrature(_segment, bestQR, d);  //quadrature rule on segment of order d (integrates exactly x^d)
  number_t nbq= quad->numberOfPoints();
  Matrix<real_t> L(nbDofs_,nbDofs_);
  number_t j=1;
  real_t sq2=std::sqrt(2)/2.;
  std::vector<real_t>::const_iterator itpt, itw; //for quadrature rule
  for (itp=Vk.begin(); itp!=Vk.end(); ++itp, ++j)
  {
    number_t i=1;
    for (number_t e = 1; e <= 3; ++e)
    {
      Vector<real_t> n(2,-1.);
      if(e==1) n*=-sq2;
      if(e==2) n[1]=0;
      if(e==3) n[0]=0;
      Polynomial pn=dot(*itp,n);
      for (itq=Ek.begin(); itq!=Ek.end(); ++itq, ++i)
      {
        L(i,j)=0;
        itpt=quad->point();
        itw=quad->weight();
        Polynomial& pq=(*itq)[0];
        for (number_t q=0; q<nbq; ++q, ++itpt, ++itw)
        {
          real_t s=*itpt;
          if(e==1)  L(i,j)+= pn(1-s,s) * pq(s) **itw * 2* sq2;
          if(e==2)  L(i,j)+= pn(0,1-s) * pq(s) **itw ;
          if(e==3)  L(i,j)+= pn(s,0)   * pq(s) **itw ;
        }
      }
    }
  }
  if(k>1) //compute Matrix Lij=l(pi,qj) for all pi in Vk, qj in P_(k-2)[t]^2, element  D.o.Fs:  v-> int_t v.q,  q in P_(k-2)[t]^2
  {
    Kk=PolynomialsBasis(PolynomialBasis(_Pk,2,k-2),2); //P_(k-2)[t]^2
    PolynomialsBasis::iterator itr;
    dimen_t d=Vk.degree()+Kk.degree();
    bestQR = Quadrature::bestQuadRule(_triangle,d);
    quad=findQuadrature(_triangle,bestQR, d);  //quadrature rule on triangle of order d
    nbq= quad->numberOfPoints();
    j=1;
    for (itp=Vk.begin(); itp!=Vk.end(); ++itp,++j)
    {
      number_t i=3*k+1;

      for (itr=Kk.begin(); itr!=Kk.end(); ++itr,++i)
      {
        L(i,j)=0.;
        itpt=quad->point();
        itw=quad->weight();
        for (number_t q=0; q<nbq; ++q, itpt+=2, ++itw)
        {
          real_t x=*itpt, y=*(itpt+1);
          L(i,j)+= dot(eval(*itp,x,y),eval(*itr,x,y)) * *itw ;
        }
      }
    }
  }
  //compute coefficients of shape functions by solving linear systems
  Matrix<real_t> sf(nbDofs_,_idMatrix);
  Matrix<real_t> Lo=L;
  real_t eps=theZeroThreshold;
  number_t r;
  bool ok=gaussMultipleSolver(L, sf, nbDofs_, eps, r);
  if(!ok)
  {
    where("RaviartThomasStdTrianglePk::computeShapeFunctions()");
    error("mat_noinvert");
  }
  sf.roundToZero();
  Matrix<real_t>::iterator itm=sf.begin();
  Vector<real_t> v(nbDofs_);
  Vector<real_t>::iterator itv;
  Wk.dimVar=2;
  Wk.dimVec=2;
  Wk.name="RT"+tostring(k);
  for(number_t i=0; i<nbDofs_; i++)
  {
    for(itv=v.begin(); itv!=v.end(); ++itv, ++itm) *itv=*itm;
    Wk.push_back(combine(Vk,v));
  }
  //Wk.clean(0.000001);
  dWk.resize(2);
  dWk[0]=dx(Wk);
  dWk[1]=dy(Wk);
}

/* builds virtual coordinates of moment dofs of order k
     for side dofs: {1/k+1, 2/k+1, ..., k/k+1}
     for internal dofs: set to (1/3,1/3), to be improved in future
*/
void RaviartThomasStdTrianglePk::pointCoordinates()
{
  std::vector<RefDof*>::iterator it_rd=refDofs.begin();
  number_t nbds=nbDofsInSides_/3, k= interpolation_p->numtype, kp=k+1;
  for(number_t e = 1; e <= 3; ++e)              //edge dofs
    for(number_t i=1; i<=nbds; ++i, ++it_rd)
      {
        real_t a = real_t(i)/kp, b=1.-a;
        switch(e)
          {
            case 1: (*it_rd)->coords(b, a); break;
            case 2: (*it_rd)->coords(0, b); break;
            default: (*it_rd)->coords(a, 0);
          }
      }

  for(number_t i=0; i<nbInternalDofs_; ++i) //For the moment all are set to (1/3,1/3)
    (*it_rd++)->coords(1./3, 1./3);
}

//dof's side numbering
void RaviartThomasStdTrianglePk::sideNumbering()
{
  trace_p->push("RaviartThomasStdTrianglePk::sideNumbering");
  number_t k = interpolation_p->numtype;
  number_t n=1;
  sideDofNumbers_.resize(3,std::vector<number_t>(k,0));
  for (number_t s = 0; s < 3; ++s )        // k dofs by side (edge)
    for(number_t i=0; i<k; ++i, ++n)
      sideDofNumbers_[s][i] = n;
  trace_p->pop();
}

//tool for global dofs construction
//return new index from a given one n from local side numbers i,j (k not used)
number_t RaviartThomasStdTrianglePk::sideDofsMap(const number_t& n, const number_t& i, const number_t& j, const number_t& k) const
{
  number_t p=interpolation_p->numtype;
  if(p==1 || i<j) return n;              //no reverse
  if(n<=2) return 3-n;                   // 1<->2
  if(n<=4) return p+3-n;                 // p=3: 3<->3 ; p=4: 3<->4;
  error("not_yet_implemented","RaviartThomasStdTrianglePk::sideDofsMap(), k>4");
  return n;
}

//compute shape functions using polynomial representation
void RaviartThomasStdTrianglePk::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  computeShapeValuesFromShapeFunctions(it_pt,shv,withDeriv,with2Deriv);
}

} // end of namespace xlifepp
