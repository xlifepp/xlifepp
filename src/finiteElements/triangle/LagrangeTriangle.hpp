/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LagrangeTriangle.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 06 aug 2012

  \brief Definition of the xlifepp::LagrangeTriangle class

  LagrangeTriangle defines Lagrange Reference Element interpolation data on triangular elements

  child classes
    - LagrangeStdTriangle: Lagrange standard Reference Element (interpolation at "equidistant" nodes), templated by order up to 6
    - LagrangeStdTrianglePk: Lagrange standard Reference Element (interpolation at "equidistant" nodes), any order

  Member functions
    - interpolationData interpolation data
    - outputAsP1
    - pointCoordinates  point coordinates
    - computeShapeFunctions:  compute polynomials representation of shape functions if required
    - fig4TeX  TeX format output of local numbering & coordinates

  External class related functions
    - tensorNumberingTriangle builds correspondence between segment and triangle local numbering

  Local numbering of Lagrange Pk triangles, k=1, 2,.., 5
  \verbatim

    2     2         2              2                  2
    | \   | \       | \            | \                | \
    3---1 5   4     5   7          5  10              5  13
     k=1  |     \   |     \        |     \            |     \
          3---6---1 8  10   4      8  14   7          8  17  10
            k=2     |         \    |         \        |         \
                    3---6---9---1 11  15  13   4     11  20  19   7
                         k=3       |             \    |             \
                                   3---6---9--12---1 14  18  21  16   4
                                           k=4        |                 \
                                                      3---6---9--12--15---1
                                                                k=5
  \endverbatim
*/

#ifndef LAGRANGE_TRIANGLE_HPP
#define LAGRANGE_TRIANGLE_HPP

#include "config.h"
#include "RefTriangle.hpp"

namespace xlifepp
{

/*!
  \class LagrangeTriangle
  defines Lagrange Reference Element interpolation data on triangular elements

  Grand-Parent class: RefElement
  Parent class: RefTriangle
  Child classes: LagrangeStdTriangle
 */
class LagrangeTriangle : public RefTriangle
{
  public:
    LagrangeTriangle(const Interpolation* interp_p); //!< constructor by interpolation
    virtual ~LagrangeTriangle();  //!< destructor

  protected:
    void interpolationData();     //!< builds interpolation data on reference element
    void sideNumbering();         //!< numbering of side D.O.F's in standard Lagrange elements
    void sideOfSideNumbering();   //!< numbering of side of side D.O.F's in standard Lagrange elements
    void pointCoordinates();      //!< builds point coordinates on reference element
    void computeShapeFunctions(); //!< compute polynomials representation of shape functions
    std::vector<RefDof*>::iterator vertexCoordinates(); //!< returns vector of vertices dofs
    std::vector<std::vector<number_t> > splitP1() const; //!< return nodes numbers of first order triangle elements when splitting current element
    splitvec_t splitO1() const; //!< return nodes numbers of first order triangle elements when splitting current element
    std::vector<std::pair<number_t,number_t> > splitP1Side(number_t s) const;   //!< return side numbers of first order elements (P1) when splitting current element

    void outputAsP1(std::ofstream& os, const int , const std::vector<number_t>& ranks) const; //!< output of Lagrange D.o.f's numbers on underlyin P1 D.o.f's

    //internal side dofs mapping when vertices of side are permuted (k not used in 2d)
    //   standard i=1, j=2 :  [----1----2---- ... ----p-1----p----]     (p=nbDofsInSides_)
    //   permuted i=2, j=1 :  [----p----p-1---- ... ----2----1----]
    number_t sideDofsMap(const number_t& n, const number_t& i, const number_t& j, const number_t& k=0) const
    {
        if(i>j) return nbDofsInSides()/3-n+1;
        return n;
    }

  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }

    // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //
    //#ifdef FIG4TEX_OUTPUT
    //    void fig4TeX();
    //#endif /* FIG4TEX_OUTPUT */
    // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //

}; // end of class LagrangeTriangle

/*!
  \class LagrangeStdTriangle
  template class child to class LagrangeTriangle (up to P6)
 */
template<number_t Pk>
class LagrangeStdTriangle: public LagrangeTriangle
{
  public:
    LagrangeStdTriangle(const Interpolation* interp_p); //!< constructor by interpolation
    ~LagrangeStdTriangle(); //!< destructor

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv=false) const; //!< shape functions
    const splitvec_t& getO1splitting() const // override
     { return splitO1Scheme; }
  private:
    // storage of order 1 splitting scheme (intended to graphical softwares usage)
    splitvec_t splitO1Scheme;
}; // end of class LagrangeStdTriangle

/*!
  \class LagrangeStdTrianglePk
  any order LagrangeTriangle with general formulae
 */
class LagrangeStdTrianglePk: public LagrangeTriangle
{
  public:
    LagrangeStdTrianglePk(const Interpolation* interp_p); //!< constructor by interpolation
    ~LagrangeStdTrianglePk();    //!< destructor

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv=false) const //! shape functions values
      {computeShapeValuesFromShapeFunctions(it_pt, shv, withDeriv, with2Deriv);}
    const splitvec_t& getO1splitting() const // override
     { return splitO1Scheme; }
  private:
    // storage of order 1 splitting scheme (intended to graphical softwares usage)
    splitvec_t splitO1Scheme;
}; // end of class LagrangeStdTrianglePk

//================================================================================
// Extern class related functions
//================================================================================
void tensorNumberingTriangle(const int, std::vector<number_t>&); //!< correspondence between segment and triangle local numbering

} // end of namespace xlifepp

#endif /* LAGRANGE_TRIANGLE_HPP */
