/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file splitUtils.hpp
  \author N. Kielbasiewicz
  \since 11 jun 2013
  \date 12 jun 2013

  \brief Definition of splitting functions
  
  functions used in splitP1 and splitO1 member functions of xlifepp::RefElement child classes
 */

#ifndef SPLIT_UTILS_HPP
#define SPLIT_UTILS_HPP

#include "config.h"

namespace xlifepp
{
  //! return nodes numbers of first order tetrahedron elements when splitting first order hexahedron
  std::vector<std::vector<number_t> > splitHexahedronQ1ToTetrahedraP1(std::vector<number_t> nodeNumbers);
  //! return nodes numbers of first order tetrahedron elements when splitting second order hexahedron
  std::vector<std::vector<number_t> > splitHexahedronQ2ToTetrahedraP1(std::vector<number_t> nodeNumbers);
  //! return nodes numbers of first order tetrahedron elements when splitting third order hexahedron
  std::vector<std::vector<number_t> > splitHexahedronQ3ToTetrahedraP1(std::vector<number_t> nodeNumbers);
  //! return nodes numbers of first order hexahedron elements when splitting second order hexahedron
  std::vector<std::vector<number_t> > splitHexahedronQ2ToHexahedraQ1(std::vector<number_t> nodeNumbers);
  //! return nodes numbers of first order hexahedron elements when splitting third order hexahedron
  std::vector<std::vector<number_t> > splitHexahedronQ3ToHexahedraQ1(std::vector<number_t> nodeNumbers);
  //! return nodes numbers of first order tetrahedron elements when splitting second order tetrahedron
  std::vector<std::vector<number_t> > splitTetrahedronP2ToTetrahedraP1(std::vector<number_t> nodeNumbers);
  //! return nodes numbers of first order tetrahedron elements when splitting third order tetrahedron
  std::vector<std::vector<number_t> > splitTetrahedronP3ToTetrahedraP1(std::vector<number_t> nodeNumbers);
  //! return nodes numbers by removing duplicates
  bool splitNumbersFind(std::vector<std::vector<number_t> > splitnum, std::vector<number_t> num);
  std::vector<std::vector<number_t> > splitNumbersUnique(std::vector<std::vector<number_t> > splitnum);
  std::vector<std::vector<number_t> > splitNumbersMerge(std::vector<std::vector<number_t> > splitnum1, std::vector<std::vector<number_t> > splitnum2);

} // end of namespace xlifepp

#endif /* SPLIT_UTILS_HPP */
