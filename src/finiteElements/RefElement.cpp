/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RefElement.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 27 nov 2002
  \date 06 aug 2012

  \brief Implementation of xlifepp::RefElement class members and related functions
 */
#include <algorithm>
#include <fstream>
#include <iostream>

#include "RefElement.hpp"
#include "point/RefPoint.hpp"
#include "utils.h"

namespace xlifepp
{

std::ostream& operator<<(std::ostream& out, const FEMapType& femt)
{ out << words("FE map type", femt); return out; }

//------------------------------------------------------------------------------
// Constructors, destructor
//------------------------------------------------------------------------------

//! default constructor
RefElement::RefElement()
  : geomRefElem_p(nullptr), interpolation_p(nullptr), mapType(_standardMap), dofCompatibility(_noDofCompatibility), dimShapeFunction(1), rotateDof(false),
    name_(""), nbDofs_(0), nbPts_(0), nbDofsOnVertices_(0), nbDofsInSideOfSides_(0), nbDofsInSides_(0), nbInternalDofs_(0), reverseEdge(false)
{}


//! constructor by shape & interpolation
RefElement::RefElement(ShapeType sh, const Interpolation* interp_p)
  : geomRefElem_p(findGeomRefElement(sh)), interpolation_p(interp_p),
    nbDofs_(0), nbPts_(0), nbDofsOnVertices_(0), nbDofsInSideOfSides_(0), nbDofsInSides_(0), nbInternalDofs_(0)
{
  name_ = words("shape", sh);
  mapType = _standardMap;
  dofCompatibility=_noDofCompatibility;
  rotateDof=false;
  reverseEdge = false;
  dimShapeFunction=1;
  hasShapeValues=true;
  theRefElements.push_back(this);  // add new RefElement pointer to vector of RefElement*
}

//! destructor
RefElement::~RefElement()
{
  // remove pointer of current object from vector of RefElement*
  std::vector<RefElement*>::iterator it(find(theRefElements.begin(), theRefElements.end(), this));
  if (it != theRefElements.end()) { theRefElements.erase(it); }
}

// delete all RefElement objects
void RefElement::clearGlobalVector()
{
  while(RefElement::theRefElements.size() > 0) { delete RefElement::theRefElements[0]; }
}

// print the list of reference elements in memory
void RefElement::printAllRefElements(std::ostream& out)
{
  number_t vb=theVerboseLevel;
  verboseLevel(1);
  out<<"RefElements in memory: "<<eol;
  std::vector<RefElement*>::iterator it=theRefElements.begin();
  for(; it!=theRefElements.end(); ++it) out<<"  "<<(*it)<<": "<<(**it)<<eol;
  verboseLevel(vb);
}

//------------------------------------------------------------------------------
// accessor-like functions
//------------------------------------------------------------------------------
//! number of D.o.F's on sideDim side sideNum ( sideNum = 1, ...)
number_t RefElement::nbDofs(const number_t sideNum, const dimen_t sideDim) const
{
  if (sideNum == 0) return static_cast<number_t>(refDofs.size());
  //else if ( sideDim == 1 ) { return sideOfSideRefElems_[sideNum - 1]->nbDofs(); }
  if (dim()-sideDim == 2)
  {
    if (sideOfSideRefElems_.size()>0) return sideOfSideRefElems_[sideNum - 1]->nbDofs();
    error("is_void","sideOfSideRefElems_");
  }
  if (sideRefElems_.size()>0) return sideRefElems_[sideNum - 1]->nbDofs();
  error("is_void","sideRefElems_");
  return 0; // dummy return
}

//! number of internal D.o.F's on sideDim side sideNum ( sideNum = 1, ...)
number_t RefElement::nbInternalDofs(const number_t sideNum, const dimen_t sideDim) const
{
  if (sideNum == 0) return nbInternalDofs_;
  //else if ( sideDim == 1 ) { return sideOfSideRefElems_[sideNum - 1]->nbInternalDofs(); }
  if (dim()-sideDim == 2)
  {
    if (sideOfSideRefElems_.size()>0) return sideOfSideRefElems_[sideNum - 1]->nbInternalDofs();
    error("is_void","sideOfSideRefElems_");
  }
  if (sideRefElems_.size()>0) return sideRefElems_[sideNum - 1]->nbInternalDofs();
  error("is_void","sideRefElems_");
  return 0; // dummy return
}

ShapeType RefElement::shapeType() const //! returns shape of element
{ return geomRefElem_p->shapeType(); }

//! length of shape function storage
size_t RefElement::shapeValueSize() const
{
  size_t lsf(0);
  for (std::vector<RefDof*>::const_iterator it_rd = refDofs.begin(); it_rd != refDofs.end(); it_rd++)
  {
    lsf += (*it_rd)->dim();
  }
  return lsf;
}

//return side number of a dof located on a side (0 if not)
// expansive inverse map: side dof number -> side; in future do it in an other way
number_t RefElement::sideOf(number_t n) const
{
  std::vector<std::vector<number_t> >::const_iterator its=sideDofNumbers_.begin();
  number_t s=1;
  for (;its!=sideDofNumbers_.end(); ++its, ++s)
  {
    std::vector<number_t>::const_iterator itn=its->begin();
    for (;itn!=its->end(); ++itn)
      if (*itn == n) return s;
  }
  return 0;   //not on a side
}

//return side of side number of a dof located on a side of side (0 if not), only in 3D
// expansive inverse map: side of side dof number -> side; in future do it in an other way
number_t RefElement::sideOfSideOf(number_t n) const
{
  std::vector<std::vector<number_t> >::const_iterator its=sideOfSideDofNumbers_.begin();
  number_t s=1;
  for (;its!=sideDofNumbers_.end(); ++its, ++s)
  {
    std::vector<number_t>::const_iterator itn=its->begin();
    for (;itn!=its->end(); ++itn)
      if (*itn == n) return s;
  }
  return 0;   //not on a side of side
}


//------------------------------------------------------------------------------
// compute shape functions for given points of quadrature formula
//------------------------------------------------------------------------------
/* Unused
std::vector<ShapeValues> RefElement::getShapeValues(const std::vector<std::vector<real_t> >& x, const bool withDeriv) const
{
  trace_p->push("RefElement::getShapeValues");
  std::vector<ShapeValues> shp(static_cast<number_t>(x.size() / geomRefElem_p->dim()),ShapeValues(*this));
  std::vector<std::vector<real_t> >::const_iterator it_x(x.begin());
  std::vector<ShapeValues>::iterator it_shp;
  for (it_shp = shp.begin(); it_shp != shp.end(); it_shp++, it_x++) {
    computeShapeValues((*it_x).begin(), *it_shp, withDeriv);
  }
  trace_p->pop();
  return shp;
}
*/

//compute shape functions at a point using polynomial representation (general method)
void RefElement::computeShapeValuesFromShapeFunctions(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x1 =*it_pt, x2=1., x3=1.;
  number_t n=Wk.dimVar;
  if (n>1) x2 = *(it_pt + 1);
  if (n>2) x3 = *(it_pt + 2);
  std::vector<real_t>::iterator itw=shv.w.begin();
  PolynomialsBasis::const_iterator it;
  for (it=Wk.begin(); it!=Wk.end(); ++it)
    for (number_t i=0;i<Wk.dimVec;i++) *itw++ = (*it)[i].eval(x1,x2,x3);

  if ( withDeriv )
  {
    std::vector<std::vector<real_t> >::iterator itd=shv.dw.begin();
    std::vector<real_t>::iterator itdi;
    for (number_t j=0;j<dWk.size(); j++, ++itd)
    {
      itdi=itd->begin();
      for (it=dWk[j].begin(); it!=dWk[j].end(); ++it)
        for (number_t i=0;i<dWk[j].dimVec;i++) *itdi++ = (*it)[i].eval(x1,x2,x3);
    }
  }
  // if ( with2Deriv ) CASE TO BE DONE
}

//! find or create Reference Element for side of side elements ( an edge for a tetrahedron )
void RefElement::sideOfSideRefElement()
{
  // reference elements of edges
  number_t nbSideOfSides = geomRefElem_p->nbSideOfSides();
  sideOfSideRefElems_.resize(nbSideOfSides);

  // find or create Reference Element for element edges
  for (number_t i = 0; i < nbSideOfSides; i++)
  {
    sideOfSideRefElems_[i] = findRefElement(_segment, interpolation_p);
  }
}

//! find or create Reference Element for 3D element faces
void RefElement::sideRefElement()
{
  // reference elements of faces
  number_t nbSides = geomRefElem_p->nbSides();
  sideRefElems_.resize(nbSides);

  // find or create Reference Element for element faces
  for (number_t i = 0; i < nbSides; i++)
  {
    sideRefElems_[i] = findRefElement(geomRefElem_p->shapeType(i + 1), interpolation_p);
  }
}

/*! create Lagrange dofs with order: vertices, edges, faces, internals
    dimE: dimension of element
    nbV: number of vertices
    nbDI: number of dofs in element (excluding vertices, edges, faces)
    nbE: number of edges
    nbDE: number of dofs on all edges (excluding vertices)
    nbF: number of faces
    nbDF: number of dofs on all faces (excluding vertices, edges)
*/
void RefElement::lagrangeRefDofs(dimen_t dimE, number_t nbV, number_t nbDI,    //1D-2D-3D
                                 number_t nbE, number_t nbDE,                  //2D-3D
                                 number_t nbF, number_t nbDF)                  //3D
{
  number_t ne=0, nf=0;
  // works only if every side has same number of internal dofs
  if (nbE>0) ne=nbDE/nbE;
  if (nbF>0) nf=nbDF/nbF;
  number_t k=1;  //global dof number
  for (number_t n = 1; n <= nbV; n++, k++)
    refDofs.push_back(new RefDof(this, true, _onVertex, n, 1, dimE, 0, k, 1, 0, _noProjection, _id, "nodal value"));
  for (number_t n = 1; n <= ne; n++)  //dof on edge excluding vertices
    for (number_t e = 1; e <= nbE; e++, k++)
      refDofs.push_back(new RefDof(this, true, _onEdge, e, n, dimE, 0, k, 1, 0, _noProjection, _id, "nodal value"));
  for (number_t f = 1; f <= nbF; f++)
    for (number_t n = 1; n <= nf; n++, k++)  //dof on face excluding vertices, edge
      refDofs.push_back(new RefDof(this, true, _onFace, f, n, dimE, 0, k, 1, 0, _noProjection, _id, "nodal value"));
  for (number_t n = 1; n <= nbDI; n++, k++)  //internal dofs
    refDofs.push_back(new RefDof(this, false, _onElement, 0, n, dimE, 0, k, 1, 0, _noProjection, _id, "nodal value"));
}

void RefElement::lagrangeRefDofs(dimen_t dimE, number_t nbV, number_t nbDI,    //1D-2D-3D
                                 number_t nbE, number_t nbDE,                  //2D-3D
                                 std::vector<number_t> nbDF)                   //3D
{
  number_t nbF=nbDF.size();
  number_t ne=0;
  // works only if every side has same number of internal dofs
  if (nbE>0) ne=nbDE/nbE;
  number_t k=1;  //global dof number
  for (number_t n = 1; n <= nbV; n++, k++)
    { refDofs.push_back(new RefDof(this, true, _onVertex, n, 1, dimE, 0, k, 1, 0, _noProjection,_id, "nodal value")); }
  for (number_t n = 1; n <= ne; n++)  //dof on edge excluding vertices
  {
    for (number_t e = 1; e <= nbE; e++, k++)
      { refDofs.push_back(new RefDof(this, true, _onEdge, e, n, dimE, 0, k, 1, 0, _noProjection,_id, "nodal value")); }
  }
  for (number_t f = 0; f < nbF; f++)
  {
    for (number_t n = 1; n <= nbDF[f]; n++, k++)  //dof on face excluding vertices, edge
      { refDofs.push_back(new RefDof(this, true, _onFace, f, n, dimE, 0, k, 1, 0, _noProjection,_id, "nodal value")); }
  }
  for (number_t n = 1; n <= nbDI; n++, k++)  //internal dofs
    { refDofs.push_back(new RefDof(this, false, _onElement, 0, n, dimE, 0, k, 1, 0, _noProjection,_id, "nodal value")); }
}

// build tree representation of polynomial shape functions (if defined)
void RefElement::buildPolynomialTree()
{
  if (Wk.size()==0) return; //no polynomial representation
  Wk.buildTree();
  for (number_t i=0;i<dWk.size(); i++) dWk[i].buildTree();
}

/*! return nodes numbers of first order elements (P1) when splitting current element (virtual function)
    a void vector is returned when spliting is not handled
*/
std::vector<std::vector<number_t> > RefElement::splitP1() const
{
  warning("free_warning", "spliting to first order of RefElement "+name_+" is not handled");
  return std::vector<std::vector<number_t> >();
}

/*! return nodes numbers of first order elements of same shape when splitting current element (virtual function)
    a void vector is returned when splitting is not handled
*/
splitvec_t RefElement::splitO1() const
{
  warning("free_warning", "spliting to first order of RefElement "+name_+" is not handled");
  return splitvec_t();
}

const splitvec_t& RefElement::getO1splitting() const { // override
  noSuchFunction("getO1splitting");
  return * new splitvec_t(); // dummy return
}

/*! return side numbers of first order elements (P1) when splitting current element (virtual function)
    a void vector is returned when spliting is not handled
*/
std::vector<std::pair<number_t,number_t> > RefElement::splitP1Side(number_t s) const
{
  warning("free_warning", "spliting to first order of RefElement "+name_+" is not handled");
  return std::vector<std::pair<number_t,number_t> >();
}

/*! prints RefElement object to ostream
    if withDerivative is true, print derivatives of shape functions (only if formal representations exist)
*/
void RefElement::print(std::ostream& os, bool withDerivative) const
{
  if (theVerboseLevel < 1) return;
  os << words("reference element") << " " << name_;

  if (theVerboseLevel < 2) return;
  os << std::endl<<"- " << *(interpolation_p) << std::endl;
  os << "- " << *(geomRefElem_p) << std::endl;
  // D.o.F list
  os << "- " << words("reference DoFs") << ": " << refDofs.size();
  int i = 1;
  for (std::vector<RefDof*>::const_iterator it_rd = refDofs.begin(); it_rd != refDofs.end(); it_rd++, i++)
  {
    os << "\n  * " << i << " -> " << *(*it_rd);
  }

  //print formal representation of shape functions
  if (Wk.size()>0)
  {
    os << "\nshape functions: " << Wk;
    if (withDerivative)
    {
     for (number_t i=0;i<dWk.size(); i++)
     os << eol << "shape functions d" << tostring(i+1) << ": " << dWk[i];
    }
  }

  if (theVerboseLevel < 5) return;

  // local numbering of side of sides
  if (geomRefElem_p->dim() >= 2 && sideOfSideDofNumbers_.size()>0)
  {
    os << std::endl << "- Dof numbers on side of sides (RefElement " << name_ << ")";
    for (number_t sideOfSide = 0; sideOfSide < geomRefElem_p->nbSideOfSides(); sideOfSide++)
    {
      os << "\n . side of side " << sideOfSide + 1 << " -> ";
      for (number_t i=0; i< sideOfSideDofNumbers_[sideOfSide].size(); i++)
      {
        os << " " <<   sideOfSideDofNumbers_[sideOfSide][i];
      }
    }
  }

  if (geomRefElem_p->dim() >= 1 && sideDofNumbers_.size()>0)
  {
    os << "\n- Dof numbers on sides (RefElement " << name_ << ")";
    for (number_t side = 0; side < geomRefElem_p->nbSides(); side++)
    {
      os << "\n . side " << side + 1 << " ->";
      for (number_t i = 0; i < sideDofNumbers_[side].size(); i++) os << " " << sideDofNumbers_[side][i];
    }
  }
  else
    os << "\n- no Dof on sides (RefElement " << name_ << ")";

  if (!(sideOfSideRefElems_.empty()))
  {
    os << "\n- Side of side shapes: " << sideOfSideRefElems_.size() << std::endl;
    if (sideOfSideRefElems_.size() > 0) os << sideOfSideRefElems_[0]->name_;
    for (number_t i = 1; i < sideOfSideRefElems_.size(); i++)
    {
      os << ", " << sideOfSideRefElems_[i]->name_;
    }
  }
  if (!(sideRefElems_.empty()))
  {
    os << "\n- Side shapes: " << sideRefElems_.size() << std::endl;
    if (sideRefElems_.size() > 0) os << sideRefElems_[0]->name_;
    for (number_t i = 1; i < sideRefElems_.size(); i++)
    {
      os << ", " << sideRefElems_[i]->name_;
    }
  }
  os<<"\n";
}

std::ostream& operator<<(std::ostream& os, const RefElement& obj)
{
  obj.print(os);
  return os;
}

void RefElement::printShapeValues(std::ostream& os, std::vector<real_t>::const_iterator it_pt, const ShapeValues& shv) const
{
  os << std::endl << "   from " << trace_p->current() << " shape functions (" << name_ << ")" << " at point ( ";
  os.setf(std::ios::scientific);
  number_t prec=entryPrec;
  if (isTestMode) { prec=testPrec; }
  for (dimen_t d = 0; d < geomRefElem_p->dim(); d++)
  {
    os << std::setw(entryWidth) << std::setprecision(prec) << *(it_pt + d) << " ";
  }
  os.unsetf(std::ios::scientific);
  os << ")";
  os << std::endl << shv;
}

void RefElement::printShapeValues(std::vector<real_t>::const_iterator it_pt, const ShapeValues& shv) const
{ printShapeValues(thePrintStream, it_pt, shv); }

//------------------------------------------------------------------------------
// errors handlers
//------------------------------------------------------------------------------
void RefElement::noSuchFunction(const string_t& s) const //! function not yet implemented in a child class
{ error("undef_elem_member_fct", s, name_); }

//=================================================================================
// Extern class related functions
//=================================================================================

/*!
  definition of a Reference Element by shape number and interpolation
  use existing Reference Element object if one already exists
  otherwise create a new one, in both cases returns pointer to the object
 */
RefElement* findRefElement(ShapeType shape, const Interpolation* interp_p)
{
  // find reference element in vector theRefElements of class RefElement*
  // (do not create a new one when it already exists, return pointer)
  for (std::vector<RefElement*>::iterator it = RefElement::theRefElements.begin(); it != RefElement::theRefElements.end(); it++)
  {
    if (shape == (*it)->geomRefElem_p->shapeType() && *interp_p == *((*it)->interpolation_p))
    {
      return *it;
    }
  }

  // create new reference element:
  // this is a new one (a reference element never seen before)
  // to be created as a new RefElement object
  // in one of the following select"shape"Reference() functions
  switch(shape)
  {
    case _point: return new RefPoint(interp_p); break;
    case _segment: return selectRefSegment(interp_p); break;
    case _triangle: return selectRefTriangle(interp_p); break;
    case _quadrangle: return selectRefQuadrangle(interp_p); break;
    case _tetrahedron: return selectRefTetrahedron(interp_p); break;
    case _hexahedron: return selectRefHexahedron(interp_p); break;
    case _prism: return selectRefPrism(interp_p); break;
    case _pyramid: return selectRefPyramid(interp_p); break;
    default: error("unknown_elem_shape_num", shape); break;
  }
  return nullptr;
}

void simplexVertexOutput(std::ofstream& os, const int refNum, const int v1, const int v2, const int v3, const int v4)
{
  if (v4 == 0)
  {
    if (v3 == 0)
    {
      os << std::endl << " " << v1 << " " << v2;
    }
    else
    {
      os << std::endl << " " << v1 << " " << v2 << " " << v3;
    }
  }
  else
  {
    os << std::endl << " " << v1 << " " << v2 << " " << v3 << " " << v4;
  }
  os << " " << refNum;
}

//@{
/*! general splitting algorithm for Lagrange element (expansive method)
      based on an exhaustive analysis of all n-simplexes
      build from closed nodes i.e |N1_x-N2.x|<=1/k & |N1_y-N2.y|<=1/k & |N1_z-N2.z|<=1/k (k order of the element)
      and check if simplexes do not intersect together
    partial tests for segment, triangle and tetrahedron, seems to work
*/
// creates the list of nodes numbers of first order segment when splitting 1D Lagrange element
std::vector<std::vector<number_t> > RefElement::splitLagrange1DToP1() const
{
  if (interpolation_p->type!=_Lagrange)
  {
    where("RefElement::splitLagrange1DToP1()");
    error("refelt_not_lagrange");
  }
  number_t p=interpolation_p->numtype;
  if (p==0) error("not_handled","RefElement::splitLagrange1DToP1()");
  std::vector<std::vector<number_t> > splitNumbers;
  std::vector<number_t> elt(2);
  std::vector<Point> nodes(nbDofs_);
  std::vector<RefDof*>::const_iterator itd=refDofs.begin();
  std::vector<Point>::iterator itp=nodes.begin();
  for (; itd!=refDofs.end(); ++itd, ++itp) *itp = (*itd)->coords_; //coordinate of nodes
  real_t tol=1./p+theEpsilon;
  for (number_t i=0; i<nbDofs_; ++i)
  {
    Point& Pi=nodes[i];
    for (number_t j=i+1; j<nbDofs_; ++j)
    {
      Point& Pj=nodes[j];
      if (std::abs(Pi.x()-Pj.x()) <= tol)
      {
        // Pi and Pj are neighbors
        elt[0]=i+1; elt[1]=j+1;
        splitNumbers.push_back(elt);
      }
    }
  }
  return splitNumbers;
}

// creates the list of nodes numbers of first order triangle when splitting 2D Lagrange element
std::vector<std::vector<number_t> > RefElement::splitLagrange2DToP1() const
{
  if (interpolation_p->type!=_Lagrange)
  {
    where("RefElement::splitLagrange2DToP1()");
    error("refelt_not_lagrange");
  }
  number_t p=interpolation_p->numtype;
  if (p==0) error("not_handled","RefElement::splitLagrange2DToP1()");
  std::vector<std::vector<number_t> > splitNumbers;
  std::vector<std::vector<number_t> >::iterator its;
  std::vector<Point> nodes(nbDofs_);
  std::vector<RefDof*>::const_iterator itd=refDofs.begin();
  std::vector<Point>::iterator itp=nodes.begin();
  for (; itd!=refDofs.end(); ++itd, ++itp) *itp = (*itd)->coords_; //coordinates of nodes
  real_t tol=1./p+theEpsilon;
  for (number_t i=0; i<nbDofs_; ++i)
  {
    Point& Pi=nodes[i];
    for (number_t j=i+1; j<nbDofs_; ++j)
    {
      Point& Pj=nodes[j];
      if (areNeighbors2D(Pi,Pj,tol))
      {
        // Pi and Pj are neighbors
        for (number_t k=j+1; k<nbDofs_; ++k)
        {
          Point& Pk=nodes[k];
          real_t surf=std::abs(crossProduct(Pj-Pi,Pk-Pi)[2]);
          if ( surf>theEpsilon)
          {
            // triangle is not flat
            if (areNeighbors2D(Pi,Pk,tol) && areNeighbors2D(Pj,Pk,tol))
            {
              // Pi, Pj and Pk are neighbors => triangle is admissible
              std::set<number_t> elt;
              elt.insert(i+1); elt.insert(j+1); elt.insert(k+1);
              its=splitNumbers.begin();
              std::set<number_t>::iterator ite=elt.end();
              // test if current triangle i,j,k intersects one triangle already defined in splitNumbers
              bool insert=true;
              while (its!=splitNumbers.end() && insert)
              {
                std::vector<number_t> com;
                number_t d1=0, d2=0;
                for (number_t l=0; l<3; l++)
                {
                  number_t n =(*its)[l];
                  if (elt.find(n)!=elt.end()) com.push_back(n);
                  else d1=n;
                }
                if (com.size()<2) insert=true;
                else insert=false;
                if (com.size()==2) //only one common side, compare distance of points out of side
                {
                  ite=elt.begin();
                  while (d2==0 && ite!=elt.end())
                  {
                    if (*ite==com[0] || *ite==com[1]) ite++;
                    else d2=*ite;
                  }
                  Point& A=nodes[d1-1]; Point& B=nodes[d2-1];
                  if (A.distance(B)>tol) insert=true;
                }
                its++;
              }
              if (insert) splitNumbers.push_back(std::vector<number_t>(elt.begin(),elt.end()));
            }
          }
        } // end for k
      }
    } // end for j
  } // end for i
  return splitNumbers;
}

// creates the list of nodes numbers of first order tetrahedron  when splitting 3D Lagrange element
std::vector<std::vector<number_t> > RefElement::splitLagrange3DToP1() const
{
  if (interpolation_p->type!=_Lagrange)
  {
    where("RefElement::splitLagrange3DToP1");
    error("refelt_not_lagrange");
  }
  number_t p=interpolation_p->numtype;
  if (p==0) error("not_handled","RefElement::splitLagrange3DToP1()");
  std::vector<std::vector<number_t> > splitNumbers;
  std::vector<std::vector<number_t> >::iterator its;
  std::vector<Point> nodes(nbDofs_);
  std::vector<RefDof*>::const_iterator itd=refDofs.begin();
  std::vector<Point>::iterator itp=nodes.begin();
  for (; itd!=refDofs.end(); ++itd, ++itp) *itp = (*itd)->coords_; //coordinates of nodes
  real_t tol=1./p+theEpsilon;
  std::vector<Point> t1(4);
  for (number_t i=0; i<nbDofs_; i++)
  {
    Point& Pi=nodes[i];
    t1[0]=nodes[i];
    for (number_t j=i+1; j<nbDofs_; j++)
    {
      Point& Pj=nodes[j];
      t1[1]=nodes[j];
      if (areNeighbors3D(Pi,Pj,tol))
      {
        // Pi and Pj are neighbors
        for (number_t k=j+1; k<nbDofs_; k++)
        {
          Point& Pk=nodes[k];
          t1[2]=nodes[k];
          if (areNeighbors3D(Pi,Pk,tol) && areNeighbors3D(Pj,Pk,tol))
          {
            // Pi, Pj and Pk are neighbors
            for (number_t l=k+1; l<nbDofs_; l++)
            {
              Point& Pl=nodes[l];
              t1[3]=nodes[l];
              real_t vol=std::abs(dot(Pj-Pi, crossProduct(Pk-Pi,Pl-Pi)));
              if (vol>theEpsilon)
              {
                // tetrahedron is not flat
                if (areNeighbors3D(Pi,Pl,tol) && areNeighbors3D(Pj,Pl,tol) && areNeighbors3D(Pk,Pl,tol))
                {
                  // Pi, Pj, Pk and Pl are neighbors => tetrahedron i,j,k,l is admissible
                  std::set<number_t> elt;
                  elt.insert(i+1); elt.insert(j+1); elt.insert(k+1); elt.insert(l+1);
                  its=splitNumbers.begin();
                  // test if current tetrahedron i,j,k,l intersects one tetrahedron already defined in splitNumbers
                  bool insert=true;
                  while (its!=splitNumbers.end() && insert)
                  {
                    std::vector<number_t> com;
                    for (number_t q=0; q<4; q++)
                    {
                      if (elt.find((*its)[q])!=elt.end()) { com.push_back((*its)[q]); }
                    }
                    if (com.size()!=0) // tetrahedron have common vertices, test using coordinates if they strictly overlap
                    {
                      std::vector<Point> t2(4);
                      t2[0]=nodes[(*its)[0]-1];
                      t2[1]=nodes[(*its)[1]-1];
                      t2[2]=nodes[(*its)[2]-1];
                      t2[3]=nodes[(*its)[3]-1];
                      if (tetrahedraOverlap(t1,t2)) { insert=false; }
                    }
                    its++;
                  }
                  if (insert)
                  {
                    splitNumbers.push_back(std::vector<number_t>(elt.begin(),elt.end()));
                  }
                }
              }
            } // end for l
          }
        } // end for k
      }
    } // end for j
  } // end for i
  return splitNumbers;
}
//@}

/*
// creates the list of nodes numbers of first order quadrangle when splitting 2D Lagrange element
// Nota: works only for uniform distribution of points (the algorithm uses the points coordinates)
splitvec_t RefElement::splitLagrange2DToQ1() const
{
  if (interpolation_p->type!=_Lagrange)
  {
    where("RefElement::splitLagrange2DToP1()");
    error("refelt_not_lagrange");
  }
  number_t p=interpolation_p->numtype;
  if (p==0) error("not_handled","RefElement::splitLagrange2DToQ1()");
  splitvec_t splitNumbers;
  std::vector<Point> nodes(nbDofs_);
  std::vector<RefDof*>::const_iterator itd=refDofs.begin();
  std::vector<Point>::iterator itp=nodes.begin();
  for (; itd!=refDofs.end(); ++itd, ++itp) *itp = (*itd)->coords_; //coordinates of nodes
  real_t tol=1./p+theEpsilon;
  for (number_t i=0; i<nbDofs_; ++i)
  {
    Point& Pi=nodes[i];
    for (number_t j=i+1; j<nbDofs_; ++j)
    {
      Point& Pj=nodes[j];
      if (areNeighbors2D(Pi,Pj,tol))
      {
        // Pi and Pj are neighbors
        for (number_t k=j+1; k<nbDofs_; ++k)
        {
          Point& Pk=nodes[k];
          if (areNeighbors2D(Pi,Pk,tol) && areNeighbors2D(Pj,Pk,tol))
          {
            // Pi, Pj and Pk are neighbors
            for (number_t l=k+1; l<nbDofs_; ++l)
            {
              Point& Pl=nodes[l];
              if (areNeighbors2D(Pi,Pl,tol) && areNeighbors2D(Pj,Pl,tol) && areNeighbors2D(Pk,Pl,tol))
              {
                // Pi, Pj, Pk and Pl are neighbors => quadrangle is admissible
                std::vector<number_t> elt;
                elt.push_back(i+1); elt.push_back(j+1);

                // test if elt is not a crossed quadrangle
                if ((std::abs(Pj.x()-Pk.x()) > theEpsilon) && (std::abs(Pj.y()-Pk.y()) > theEpsilon))
                  { elt.push_back(l+1); elt.push_back(k+1); }
                else { elt.push_back(k+1); elt.push_back(l+1); }

                splitNumbers.push_back(std::make_pair(_quadrangle,elt));
              }
            } // end for l
          }
        } // end for k
      }
    } // end for j
  } // end for i
  return splitNumbers;
}
*/

// creates the list of nodes numbers of first order hexahedron  when splitting 3D Lagrange element
// Nota: works only for uniform distribution of points (the algorithm uses the points coordinates)
splitvec_t RefElement::splitLagrange3DToQ1() const
{
  if (interpolation_p->type!=_Lagrange)
  {
    where("RefElement::splitLagrange3DToQ1()");
    error("refelt_not_lagrange");
  }
  number_t p=interpolation_p->numtype;
  if (p==0) error("not_handled","RefElement::splitLagrange3DToQ1()");
  splitvec_t splitNumbers;
  std::vector<Point> nodes(nbDofs_);
  std::vector<RefDof*>::const_iterator itd=refDofs.begin();
  std::vector<Point>::iterator itp=nodes.begin();
  for (; itd!=refDofs.end(); ++itd, ++itp) *itp = (*itd)->coords_; //coordinates of nodes
  real_t tol=1./p+theEpsilon;
  for (number_t i=0; i<nbDofs_; i++)
  {
    const Point& Pi=nodes[i];
    for (number_t j=i+1; j<nbDofs_; j++)
    {
      const Point& Pj=nodes[j];
      if (areNeighbors3D(Pi,Pj,tol))
      {
        // Pi and Pj are neighbors
        for (number_t k=j+1; k<nbDofs_; k++)
        {
          const Point& Pk=nodes[k];
          if (areNeighbors3D(Pi,Pk,tol) && areNeighbors3D(Pj,Pk,tol))
          {
            // Pi, Pj and Pk are neighbors
            for (number_t l=k+1; l<nbDofs_; l++)
            {
              const Point& Pl=nodes[l];
              if (areNeighbors3D(Pi,Pl,tol) && areNeighbors3D(Pj,Pl,tol) && areNeighbors3D(Pk,Pl,tol))
              {
                // Pi, Pj, Pk and Pl are neighbors => tetrahedron i,j,k,l is admissible
                for (number_t ii=l+1; ii<nbDofs_; ++ii)
                {
                  const Point& Pii=nodes[ii];
                  if (areNeighbors3D(Pi,Pii,tol) && areNeighbors3D(Pj,Pii,tol) && areNeighbors3D(Pk,Pii,tol)
                   && areNeighbors3D(Pl,Pii,tol))
                  {
                    // Pi, Pj, Pk, Pl and Pii are neighbors
                    for (number_t jj=ii+1; jj<nbDofs_; ++jj)
                    {
                      const Point& Pjj = nodes[jj];
                      if (areNeighbors3D(Pi,Pjj,tol) && areNeighbors3D(Pj,Pjj,tol) && areNeighbors3D(Pk,Pjj,tol)
                       && areNeighbors3D(Pl,Pjj,tol) && areNeighbors3D(Pii,Pjj,tol))
                      {
                        // Pi, Pj, Pk, Pl, Pii and Pjj are neighbors
                        for (number_t kk=jj+1; kk<nbDofs_; ++kk)
                        {
                          const Point& Pkk=nodes[kk];
                          if (areNeighbors3D(Pi,Pkk,tol) && areNeighbors3D(Pj,Pkk,tol) && areNeighbors3D(Pk,Pkk,tol)
                           && areNeighbors3D(Pl,Pkk,tol) && areNeighbors3D(Pii,Pkk,tol) && areNeighbors3D(Pjj,Pkk,tol))
                          {
                            // Pi, Pj, Pk, Pl, Pii, Pjj and Pkk are neighbors
                            for (number_t ll=kk+1; ll<nbDofs_; ++ll)
                            {
                              const Point& Pll=nodes[ll];
                              if (areNeighbors3D(Pi,Pll,tol) && areNeighbors3D(Pj,Pll,tol) && areNeighbors3D(Pk,Pll,tol)
                               && areNeighbors3D(Pl,Pll,tol) && areNeighbors3D(Pii,Pll,tol) && areNeighbors3D(Pjj,Pll,tol)
                               && areNeighbors3D(Pkk,Pll,tol))
                              {
                                // Pi, Pj, Pk, Pl, Pii, Pjj, Pkk and Pll are neighbors => hexahedron is admissible
                                // we have to split nodes according to z coordinate and sort them according to x and y
                                std::set<std::pair<number_t,Point>,SortPointsByXAndY> face1, face2;
                                std::vector<number_t> facebelow, facetop, elt;
                                Point P1, P2;
                                face1.insert(std::make_pair(i+1,Pi));
                                P1 = Pi;
                                if (std::abs(Pj.z()-Pi.z()) <= theEpsilon) { face1.insert(std::make_pair(j+1,Pj)); }
                                else { face2.insert(std::make_pair(j+1,Pj)); P2=Pj; }
                                if (std::abs(Pk.z()-Pi.z()) <= theEpsilon) { face1.insert(std::make_pair(k+1,Pk)); }
                                else { face2.insert(std::make_pair(k+1,Pk)); P2=Pk;}
                                if (std::abs(Pl.z()-Pi.z()) <= theEpsilon) { face1.insert(std::make_pair(l+1,Pl)); }
                                else { face2.insert(std::make_pair(l+1,Pl)); P2=Pl;}
                                if (std::abs(Pii.z()-Pi.z()) <= theEpsilon) { face1.insert(std::make_pair(ii+1,Pii)); }
                                else { face2.insert(std::make_pair(ii+1,Pii)); P2=Pii; }
                                if (std::abs(Pjj.z()-Pi.z()) <= theEpsilon) { face1.insert(std::make_pair(jj+1,Pjj)); }
                                else { face2.insert(std::make_pair(jj+1,Pjj)); P2=Pjj; }
                                if (std::abs(Pkk.z()-Pi.z()) <= theEpsilon) { face1.insert(std::make_pair(kk+1,Pkk)); }
                                else { face2.insert(std::make_pair(kk+1,Pkk)); P2=Pkk; }
                                if (std::abs(Pll.z()-Pi.z()) <= theEpsilon) { face1.insert(std::make_pair(ll+1,Pll)); }
                                else { face2.insert(std::make_pair(ll+1,Pll)); P2=Pll; }

                                std::set<std::pair<number_t, Point>,SortPointsByXAndY>::const_iterator it;

                                if (P1.z() < P2.z())
                                {
                                  // face1 is below
                                  for (it=face1.begin(); it != face1.end(); ++it) { facebelow.push_back(it->first); }
                                  for (it=face2.begin(); it != face2.end(); ++it) { facetop.push_back(it->first); }
                                }
                                else
                                {
                                  // face2 is below
                                  for (it=face1.begin(); it != face1.end(); ++it) { facetop.push_back(it->first); }
                                  for (it=face2.begin(); it != face2.end(); ++it) { facebelow.push_back(it->first); }
                                }
                                elt.push_back(facebelow[0]);
                                elt.push_back(facebelow[1]);
                                elt.push_back(facebelow[3]);
                                elt.push_back(facebelow[2]);
                                elt.push_back(facetop[0]);
                                elt.push_back(facetop[1]);
                                elt.push_back(facetop[3]);
                                elt.push_back(facetop[2]);
                                splitNumbers.push_back(std::make_pair(_hexahedron,elt));
                              }
                            } // end for ll
                          }
                        } // end for kk
                      }
                    } // end for jj
                  }
                } // end for ii
              }
            } // end for l
          }
        } // end for k
      }
    } // end for j
  } // end for i
  return splitNumbers;
}

// creates the list of nodes numbers of first order hexahedron  when splitting 3D Lagrange element
splitvec_t RefElement::splitLagrange3DToPrismO1() const
{
  if (interpolation_p->type!=_Lagrange)
  {
    where("RefElement::splitLagrange3DToPrismO1()");
    error("refelt_not_lagrange");
  }
  number_t p=interpolation_p->numtype;
  if (p==0) error("not_handled","RefElement::splitLagrange3DToPrismO1()");
  splitvec_t splitNumbers;
  splitvec_t::iterator its;
  std::vector<Point> nodes(nbDofs_);
  std::vector<RefDof*>::const_iterator itd=refDofs.begin();
  std::vector<Point>::iterator itp=nodes.begin();
  for (; itd!=refDofs.end(); ++itd, ++itp) *itp = (*itd)->coords_; //coordinates of nodes
  real_t tol=1./p+theEpsilon;
  for (number_t i=0; i<nbDofs_; i++)
  {
    const Point& Pi=nodes[i];
    for (number_t j=i+1; j<nbDofs_; j++)
    {
      const Point& Pj=nodes[j];
      if (areNeighbors3D(Pi,Pj,tol))
      {
        // Pi and Pj are neighbors
        for (number_t k=j+1; k<nbDofs_; k++)
        {
          const Point& Pk=nodes[k];
          if (areNeighbors3D(Pi,Pk,tol) && areNeighbors3D(Pj,Pk,tol))
          {
            // Pi, Pj and Pk are neighbors
            for (number_t l=k+1; l<nbDofs_; l++)
            {
              const Point& Pl=nodes[l];
              if (areNeighbors3D(Pi,Pl,tol) && areNeighbors3D(Pj,Pl,tol) && areNeighbors3D(Pk,Pl,tol))
              {
                // Pi, Pj, Pk and Pl are neighbors => tetrahedron i,j,k,l is admissible
                for (number_t ii=l+1; ii<nbDofs_; ++ii)
                {
                  const Point& Pii=nodes[ii];
                  if (areNeighbors3D(Pi,Pii,tol) && areNeighbors3D(Pj,Pii,tol) && areNeighbors3D(Pk,Pii,tol)
                   && areNeighbors3D(Pl,Pii,tol))
                  {
                    // Pi, Pj, Pk, Pl and Pii are neighbors
                    for (number_t jj=ii+1; jj<nbDofs_; ++jj)
                    {
                      const Point& Pjj = nodes[jj];
                      if (areNeighbors3D(Pi,Pjj,tol) && areNeighbors3D(Pj,Pjj,tol) && areNeighbors3D(Pk,Pjj,tol)
                       && areNeighbors3D(Pl,Pjj,tol) && areNeighbors3D(Pii,Pjj,tol))
                      {
                        // Pi, Pj, Pk, Pl, Pii and Pjj are neighbors
                        // we have to split nodes according to z coordinate and sort them according to x and y
                        std::set<std::pair<number_t,Point>,SortPointsByXAndY> face1, face2;
                        std::vector<number_t> facebelow, facetop, elt;
                        std::vector<Point> coordsbelow, coordstop;
                        Point P1, P2;
                        face1.insert(std::make_pair(i+1,Pi));
                        P1 = Pi;
                        if (std::abs(Pj.z()-Pi.z()) <= theEpsilon) { face1.insert(std::make_pair(j+1,Pj)); }
                        else { face2.insert(std::make_pair(j+1,Pj)); P2=Pj; }
                        if (std::abs(Pk.z()-Pi.z()) <= theEpsilon) { face1.insert(std::make_pair(k+1,Pk)); }
                        else { face2.insert(std::make_pair(k+1,Pk)); P2=Pk;}
                        if (std::abs(Pl.z()-Pi.z()) <= theEpsilon) { face1.insert(std::make_pair(l+1,Pl)); }
                        else { face2.insert(std::make_pair(l+1,Pl)); P2=Pl;}
                        if (std::abs(Pii.z()-Pi.z()) <= theEpsilon) { face1.insert(std::make_pair(ii+1,Pii)); }
                        else { face2.insert(std::make_pair(ii+1,Pii)); P2=Pii; }
                        if (std::abs(Pjj.z()-Pi.z()) <= theEpsilon) { face1.insert(std::make_pair(jj+1,Pjj)); }
                        else { face2.insert(std::make_pair(jj+1,Pjj)); P2=Pjj; }

                        if (prismValid(face1,face2))
                        {
                          // face1.size + face2.size = 6 so face1.size = face2.size = 3
                          std::set<std::pair<number_t, Point>,SortPointsByXAndY>::const_iterator it;

                          if (P1.z() < P2.z())
                          {
                            // face1 is below
                            for (it=face1.begin(); it != face1.end(); ++it)
                              { facebelow.push_back(it->first); coordsbelow.push_back(it->second); }
                            for (it=face2.begin(); it != face2.end(); ++it)
                              { facetop.push_back(it->first); coordstop.push_back(it->second); }
                          }
                          else
                          {
                            // face2 is below
                            for (it=face1.begin(); it != face1.end(); ++it)
                              { facetop.push_back(it->first); coordstop.push_back(it->second); }
                            for (it=face2.begin(); it != face2.end(); ++it)
                              { facebelow.push_back(it->first); coordsbelow.push_back(it->second); }
                          }

                          // reorder vertices

                          elt.push_back(facebelow[0]);
                          elt.push_back(facebelow[1]);
                          elt.push_back(facebelow[2]);
                          elt.push_back(facetop[0]);
                          elt.push_back(facetop[1]);
                          elt.push_back(facetop[2]);

                          its=splitNumbers.begin();
                          // test if current prism intersects one prism already defined in splitNumbers
                          bool insert=true;
                          number_t d1=0, d2=0;
                          while (its != splitNumbers.end() && insert)
                          {
                            std::vector<number_t> com;
                            for (number_t q=0; q<3; q++)
                            {
                              bool found=false;
                              for (number_t qq=0; qq<3; qq++)
                              {
                                if (elt[qq]==its->second[q])
                                {
                                  com.push_back(its->second[q]);
                                  found=true;
                                }
                                if (!found) { d1 = its->second[q];}
                              }

                            }
                            if (com.size()<2) { insert=true; }
                            else { insert=false; }
                            if (com.size()==2) // prism triangular bases have common vertices, test using coordinates if they strictly overlap
                            {
                              number_t ite=0;
                              while (d2==0 && ite<3)
                              {
                                if (elt[ite]==com[0] || elt[ite]==com[1]) ite++;
                                else d2=elt[ite];
                              }
                              Point& A=nodes[d1-1]; Point& B=nodes[d2-1];
                              if (A.distance(B)>tol) { insert=true; }
                            }
                            its++;
                          }
                          if (insert)
                          {
                            splitNumbers.push_back(std::make_pair(_prism,elt));
                          }

                        }
                      }
                    } // end for jj
                  }
                } // end for ii
              }
            } // end for l
          }
        } // end for k
      }
    } // end for j
  } // end for i
  return splitNumbers;
}

bool areNeighbors2D(const Point& p, const Point& q, real_t tol)
{
 return ((std::abs(p.x()-q.x()) <= tol) && (std::abs(p.y()-q.y()) <= tol));
}

bool areNeighbors3D(const Point& p, const Point& q, real_t tol)
{
 return ((std::abs(p.x()-q.x()) <= tol) && (std::abs(p.y()-q.y()) <= tol) && (std::abs(p.z()-q.z()) <= tol));
}

bool SortPointsByXAndY::operator()(const std::pair<number_t,Point>& p, const std::pair<number_t,Point>& q) const
{
  if (p.second.y() < q.second.y()) { return true; }
  else
  {
    if (std::abs(p.second.y()-q.second.y()) < theEpsilon)
    {
      if (p.second.x() < q.second.x()) { return true; }
    }
  }
  return false;
}

/*
  test if plane defined by point p and normal vector n separates list of points or not
  returns 1 if every point if on the positive side of the plane
  or returns -1 if every point is on the negative side of the plane
  else returns 0
*/
int_t whichSide(const Point& p, const Point& n, const std::vector<Point>& pts)
{
  int_t positive=0, negative=0;
  for (number_t i = 0; i < pts.size(); ++i)
  {
    real_t t=dot(n,pts[i]-p);
    if (t > theEpsilon) { positive++; }
    else
    {
      if (t < -theEpsilon) { negative++; }
    }
    if (positive && negative) { return int_t(0); }
  }
  if (positive) { return int_t(1); }
  else { return int_t(-1); }
}

/*!
  implementation of the "Separating Axis Test" for a pair of tetrahedra

  The main idea is that if tetrahedra does not overlap, there exists an axis where their projections
  does not overlap
  This axis has one of the following properties:
    - the axis is orthogonal to one face of one of the tetrahedra
    - the axis is orthogonal to an edge on each tetrahedron

  overlapping means intersection has a non null measure

  The main idea of the method is to test each face of both tetrahedra then test each pair of edges (one per tetrahedron)
  If one of these elementary tests separates, then false is returned (no overlap), else their is overlapping
*/
bool tetrahedraOverlap(const std::vector<Point>& t1,const std::vector<Point>& t2)
{
  // **** test t1 faces **** //
  // test face t1[0] t1[1] t1[2]
  Point n=crossProduct(t1[1]-t1[0],t1[2]-t1[0]);
  real_t d=dot(n,t1[3]-t1[0]);
  if (d > theEpsilon) { n=-n; }
  if (whichSide(t1[0],n,t2) == 1) { return false; }

  // test face t1[0] t1[1] t1[3]
  n=crossProduct(t1[1]-t1[0],t1[3]-t1[0]);
  d=dot(n,t1[2]-t1[0]);
  if (d > theEpsilon) { n=-n; }
  if (whichSide(t1[0],n,t2) == 1) { return false; }

  // test face t1[0] t1[2] t1[3]
  n=crossProduct(t1[2]-t1[0],t1[3]-t1[0]);
  d=dot(n,t1[1]-t1[0]);
  if (d > theEpsilon) { n=-n; }
  if (whichSide(t1[0],n,t2) == 1) { return false; }

  // test face t1[1] t1[2] t1[3]
  n=crossProduct(t1[2]-t1[1],t1[3]-t1[1]);
  d=dot(n,t1[0]-t1[1]);
  if (d > theEpsilon) { n=-n; }
  if (whichSide(t1[1],n,t2) == 1) { return false; }

  // **** test t2 faces **** //
  // test face t2[0] t2[1] t2[2]
  n=crossProduct(t2[1]-t2[0],t2[2]-t2[0]);
  d=dot(n,t2[3]-t2[0]);
  if (d > theEpsilon) { n=-n; }
  if (whichSide(t2[0],n,t1) == 1) { return false; }

  // test face t2[0] t2[1] t2[3]
  n=crossProduct(t2[1]-t2[0],t2[3]-t2[0]);
  d=dot(n,t2[2]-t2[0]);
  if (d > theEpsilon) { n=-n; }
  if (whichSide(t2[0],n,t1) == 1) { return false; }

  // test face t2[0] t2[2] t2[3]
  n=crossProduct(t2[2]-t2[0],t2[3]-t2[0]);
  d=dot(n,t2[1]-t2[0]);
  if (d > theEpsilon) { n=-n; }
  if (whichSide(t2[0],n,t1) == 1) { return false; }

  // test face t2[1] t2[2] t2[3]
  n=crossProduct(t2[2]-t2[1],t2[3]-t2[1]);
  d=dot(n,t2[0]-t2[1]);
  if (d > theEpsilon) { n=-n; }
  if (whichSide(t2[1],n,t1) == 1) { return false; }

  // **** test pair of edges (one of t1, one of t2) **** //
  // test edges t1[0] t1[1] and t2[0] t2[1]
  n=crossProduct(t1[1]-t1[0],t2[1]-t2[0]);
  int_t side1=whichSide(t1[0],n,t2);
  int_t side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[1] and t2[0] t2[2]
  n=crossProduct(t1[1]-t1[0],t2[2]-t2[0]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[1] and t2[0] t2[3]
  n=crossProduct(t1[1]-t1[0],t2[3]-t2[0]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[1] and t2[1] t2[2]
  n=crossProduct(t1[1]-t1[0],t2[2]-t2[1]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[1] and t2[1] t2[3]
  n=crossProduct(t1[1]-t1[0],t2[3]-t2[1]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[1] and t2[2] t2[3]
  n=crossProduct(t1[1]-t1[0],t2[3]-t2[2]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[2] and t2[0] t2[1]
  n=crossProduct(t1[2]-t1[0],t2[1]-t2[0]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[2] and t2[0] t2[2]
  n=crossProduct(t1[2]-t1[0],t2[2]-t2[0]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[2] and t2[0] t2[3]
  n=crossProduct(t1[2]-t1[0],t2[3]-t2[0]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[2] and t2[1] t2[2]
  n=crossProduct(t1[2]-t1[0],t2[2]-t2[1]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[2] and t2[1] t2[3]
  n=crossProduct(t1[2]-t1[0],t2[3]-t2[1]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[2] and t2[2] t2[3]
  n=crossProduct(t1[2]-t1[0],t2[3]-t2[2]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[3] and t2[0] t2[1]
  n=crossProduct(t1[3]-t1[0],t2[1]-t2[0]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[3] and t2[0] t2[2]
  n=crossProduct(t1[3]-t1[0],t2[2]-t2[0]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[3] and t2[0] t2[3]
  n=crossProduct(t1[3]-t1[0],t2[3]-t2[0]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[3] and t2[1] t2[2]
  n=crossProduct(t1[3]-t1[0],t2[2]-t2[1]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[3] and t2[1] t2[3]
  n=crossProduct(t1[3]-t1[0],t2[3]-t2[1]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[0] t1[3] and t2[2] t2[3]
  n=crossProduct(t1[3]-t1[0],t2[3]-t2[2]);
  side1=whichSide(t1[0],n,t2); side2=whichSide(t1[0],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[1] t1[2] and t2[0] t2[1]
  n=crossProduct(t1[2]-t1[1],t2[1]-t2[0]);
  side1=whichSide(t1[1],n,t2); side2=whichSide(t1[1],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[1] t1[2] and t2[0] t2[2]
  n=crossProduct(t1[2]-t1[1],t2[2]-t2[0]);
  side1=whichSide(t1[1],n,t2); side2=whichSide(t1[1],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[1] t1[2] and t2[0] t2[3]
  n=crossProduct(t1[2]-t1[1],t2[3]-t2[0]);
  side1=whichSide(t1[1],n,t2); side2=whichSide(t1[1],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[1] t1[2] and t2[1] t2[2]
  n=crossProduct(t1[2]-t1[1],t2[2]-t2[1]);
  side1=whichSide(t1[1],n,t2); side2=whichSide(t1[1],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[1] t1[2] and t2[1] t2[3]
  n=crossProduct(t1[2]-t1[1],t2[3]-t2[1]);
  side1=whichSide(t1[1],n,t2); side2=whichSide(t1[1],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[1] t1[2] and t2[2] t2[3]
  n=crossProduct(t1[2]-t1[1],t2[3]-t2[2]);
  side1=whichSide(t1[1],n,t2); side2=whichSide(t1[1],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[1] t1[3] and t2[0] t2[1]
  n=crossProduct(t1[3]-t1[1],t2[1]-t2[0]);
  side1=whichSide(t1[1],n,t2); side2=whichSide(t1[1],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[1] t1[3] and t2[0] t2[2]
  n=crossProduct(t1[3]-t1[1],t2[2]-t2[0]);
  side1=whichSide(t1[1],n,t2); side2=whichSide(t1[1],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[1] t1[3] and t2[0] t2[3]
  n=crossProduct(t1[3]-t1[1],t2[3]-t2[0]);
  side1=whichSide(t1[1],n,t2); side2=whichSide(t1[1],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[1] t1[3] and t2[1] t2[2]
  n=crossProduct(t1[3]-t1[1],t2[2]-t2[1]);
  side1=whichSide(t1[1],n,t2); side2=whichSide(t1[1],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[1] t1[3] and t2[1] t2[3]
  n=crossProduct(t1[3]-t1[1],t2[3]-t2[1]);
  side1=whichSide(t1[1],n,t2); side2=whichSide(t1[1],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[1] t1[3] and t2[2] t2[3]
  n=crossProduct(t1[3]-t1[1],t2[3]-t2[2]);
  side1=whichSide(t1[1],n,t2); side2=whichSide(t1[1],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[2] t1[3] and t2[0] t2[1]
  n=crossProduct(t1[3]-t1[2],t2[1]-t2[0]);
  side1=whichSide(t1[2],n,t2); side2=whichSide(t1[2],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[2] t1[3] and t2[0] t2[2]
  n=crossProduct(t1[3]-t1[2],t2[2]-t2[0]);
  side1=whichSide(t1[2],n,t2); side2=whichSide(t1[2],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[2] t1[3] and t2[0] t2[3]
  n=crossProduct(t1[3]-t1[2],t2[3]-t2[0]);
  side1=whichSide(t1[2],n,t2); side2=whichSide(t1[2],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[2] t1[3] and t2[1] t2[2]
  n=crossProduct(t1[3]-t1[2],t2[2]-t2[1]);
  side1=whichSide(t1[2],n,t2); side2=whichSide(t1[2],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[2] t1[3] and t2[1] t2[3]
  n=crossProduct(t1[3]-t1[2],t2[3]-t2[1]);
  side1=whichSide(t1[2],n,t2); side2=whichSide(t1[2],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  // test edges t1[2] t1[3] and t2[2] t2[3]
  n=crossProduct(t1[3]-t1[2],t2[3]-t2[2]);
  side1=whichSide(t1[2],n,t2); side2=whichSide(t1[2],n,t1);
  if (side1*side2 < -theEpsilon) { return false; }

  return true;
}

/*!
  determines if a prism defined by its triangular faces is valid or not
  faces must coincide
  first point of each face has coordinates xmin and ymin
*/
bool prismValid(const std::set<std::pair<number_t,Point>,SortPointsByXAndY>& face1, const std::set<std::pair<number_t,Point>,SortPointsByXAndY>& face2)
{
  // vectors must have same size
  if (face1.size() != face2.size()) { return false; }
  std::set<std::pair<number_t,Point>,SortPointsByXAndY>::const_iterator it=face1.begin();
  const Point& P0=it->second; it++;
  const Point& P1=it->second; it++;
  const Point& P2=it->second;
  Point n1=crossProduct(P1-P0,P2-P0);

  it=face2.begin();
  const Point& PP0=it->second; it++;
  const Point& PP1=it->second; it++;
  const Point& PP2=it->second;
  Point n2=crossProduct(PP1-PP0,PP2-PP0);
  if (n1[2]*n2[2]<0.) { return false; }

  // not enough
  Point z0=PP0-P0;
  Point z1=PP1-P1;
  if (z0 != z1) { return false; }
  Point z2=PP2-P2;
  if (z0 != z2) { return false; }
  if (std::abs(z1.x()) > theEpsilon) { return false; }
  if (std::abs(z2.x()) > theEpsilon) { return false; }
  return true;
}

// // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //
// #ifdef FIG4TEX_OUTPUT
// string RefElement::fig4TexFileName()
// {
//   return name_+"-c++.tex";
// }
// void RefElement::fig4TeXVertexPt(ofstream& os, vector<real_t>::const_iterator coor, number_t& pts)
// {
//   const dimen_t d(geomRefElem_p->dim());
//   char b=char(92);
//   for ( number_t p = 1; p <= geomRefElem_p->numberOfVertices(); pts++, p++ )
//   {
//      os <<endl<<b<<"figpt "<< p <<":"<< p << "(";
//      for ( int i = 0; i < d-1 ; i++, coor++ ) os << setprecision(5) << *coor <<",";
//      os << setprecision(5) << *coor++ <<")";
//   }
// }
// void RefElement::fig4TeXEdgePt(ofstream& os, const number_t ed, number_t& pts,
//                                     const string& posit, const string& posit2)
// {
//   const dimen_t d(geomRefElem_p->dim());
//   char b=char(92);
//   number_t nb_P_Edge = edge_interpolations_[ed]->nbDofs();

//   os <<endl<<"% edge "<< ed+1;
//   if ( d == 2 ) os <<endl<<"{%";
//   else if ( d == 3 ) os <<endl<<"{"<<b<<"twelvepoint"<<b<<"bf%";
//   for ( number_t p = 2 ; p < nb_P_Edge; p++, pts++ )
//   {
//      number_t p_l = edge_interpolations_[ed]->DOF_numbers[p];
//      vector<real_t>::const_iterator coor = refDofs[p_l-1]->coords();
//      os <<endl<<b<<"figsetmark{$"<<b<<"figBullet$}"<<endl<<b<<"figpt0:" << p_l <<"(";
//      for ( int i = 0; i < d-1 ; i++, coor++ ) os << setprecision(5) << *coor <<",";
//      os << setprecision(5) << *coor++ <<")";
//      os <<b<<"figwrite"<<posit<<"0:(3pt)";
//      if ( d == 2 and !posit2.empty() )
//          show in Red local numbering on edges
//         os <<endl<<b<<"figsetmark{}"<<b<<"color{"<<b<<"Red}"<<b<<"figwrite"<<posit2<<"0:${}_{"<<p+1<<"}$(2pt)"<<b<<"endcolor";
//   }
//   os <<endl<< "}%";
// }
// void RefElement::fig4TeXInternalPoints2d(ofstream& os, number_t& pts)
// {
//   const dimen_t d(geomRefElem_p->dim());
//   char b=char(92);
//   os <<endl<<b<<"figsetmark{$"<<b<<"circ$}";
//   vector<RefDof*>::const_iterator refDofs_b(refDofs.begin()+pts), it_rd;
//   for ( it_rd = refDofs_b; it_rd != refDofs.end(); it_rd++, pts++ )
//   {
//      vector<real_t>::const_iterator coor = (*it_rd)->coords();
//      os <<endl<<b<<"figpt0:" << pts+1 <<"(";
//      for ( int i = 0; i < d-1 ; i++, coor++ ) os << setprecision(5) << *coor <<",";
//      os << setprecision(5) << *coor++ <<")";
//      os <<b<<"figwrites0:${}_{"<<pts+1<<"}$(3pt)";
//   }
// }
// void RefElement::fig4TeXFacePt(ofstream& os, const number_t fa,
//                                    const number_t pt1 , number_t& pts, const string& posit)
// {
//   const dimen_t d(geomRefElem_p->dim());
//   char b=char(92);
//   number_t nb_P = side_interpolations_[fa]->nbDofs();
//   os <<endl<<"% face "<< fa+1;
//   os <<endl<<"{"<<b<<"figsetmark{$"<<b<<"circ$}";
//   os <<endl<<b<<"tenpoint"<<b<<"it" <<b<<"color{"<<b<<"Blue}%";
//   for ( number_t p = pt1 ; p < nb_P; p++, pts++ )
//   {
//      number_t p_l = side_interpolations_[fa]->DOF_numbers[p];
//      vector<real_t>::const_iterator coor = refDofs[p_l-1]->coords();
//      os <<endl<<b<<"figpt0:" << p_l << "(";
//      for ( int i = 0; i < d-1 ; i++, coor++ ) os << setprecision(5) << *coor <<",";
//      os << setprecision(5) << *coor++ <<")";
//      os <<b<<"figwrite"<<posit<<"0:(4pt)";
//   }
//   os <<endl<<b<<"endcolor}%";
// }
// void RefElement::fig4TeXSmallFace(ofstream& os, const number_t fa, const number_t nbEdges, const number_t pt1)
// {
//    Draw a reduced face to show "inner perimeter of face"
//   const dimen_t d(geomRefElem_p->dim());
//   char b=char(92);
//   os <<endl;
//   number_t pp(11);
//   for ( number_t p = pt1 ; p < pt1+nbEdges; p++, pp++ )
//   {
//      number_t p_l = side_interpolations_[fa-1]->DOF_numbers[p];
//      vector<real_t>::const_iterator coor = refDofs[p_l-1]->coords();
//      os <<b<<"figpt"<<pp<<":(";
//      for ( int i = 0; i < d-1 ; i++, coor++ ) os << setprecision(5) << *coor <<",";
//      os << setprecision(5) << *coor++ <<")";
//   }
//   os <<endl<<b<<"pssetwidth{.4}"<<b<<"pssetdash{10}"<<b<<"psline[";
//   pp = 11;
//   for ( number_t p = 11; p < 11 + nbEdges; p++ ) os << p<<",";
//   os << pp<<"]";
// }

// #endif  /* FIG4TEX_OUTPUT */
// // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //

} // end of namespace xlifepp
