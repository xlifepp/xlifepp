/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Interpolation.cpp
  \authors D. Martin, E. Lunéville, N. Kielbasiewicz
  \since 5 jun 2011
  \date 6 aug 2012

  \brief implementation of xlifepp::Interpolation class members and related functions
 */

#include "Interpolation.hpp"
#include "utils.h"

#include <algorithm>

namespace xlifepp
{

//--------------------------------------------------------------------------------
// Interpolation constructor/destructor
//--------------------------------------------------------------------------------
Interpolation::Interpolation(FEType typ, FESubType sub, number_t num, SobolevType spa)
  : type(typ), subtype(sub), numtype(num), conformSpace(spa), name(""), subname(""), shortname("")
{
  build();
  // append new Interpolation pointer to vector Interpolation::Interpolations
  theInterpolations.push_back(this);
}

void Interpolation::build()
{
  name = words("FE name", type);

  switch (type)
  {
    case _Lagrange:
      if (subtype == _standard || subtype == _GaussLobattoPoints) subname = words("FE subname", subtype);
      else  badSubType();
      shortname="Lag";
      break;
    case _Hermite:
      if (subtype == _standard)  subname = words("FE subname", subtype);
      else  badSubType();
      shortname="Her";
      break;
    case _CrouzeixRaviart:
      if (subtype == _standard)  subname = words("FE subname", subtype);
      else badSubType();
      shortname="CR";
      break;
    case _Nedelec:
    case _NedelecFace:
    case _NedelecEdge:
      if (subtype == _firstFamily || subtype == _secondFamily) subname = words("FE subname", subtype);
      else badSubType();
      shortname="NE";
      break;
    case _RaviartThomas:
      if (subtype == _standard) subname = words("FE subname", subtype);
      else badSubType();
      shortname="RT";
      break;
    case _BuffaChristiansen:
      if (subtype == _standard) subname = words("FE subname", subtype);
      else badSubType();
      shortname="BC";
      break;
    case _Morley:
      if (subtype == _standard) subname = words("FE subname", subtype);
      else badSubType();
      shortname="Mor";
      break;
    case _Argyris:
      if (subtype == _standard) subname = words("FE subname", subtype);
      else badSubType();
      shortname="Arg";
      break;
    default:
      badType();
      break;
  }

  if(type==_Nedelec && conformSpace==_Hdiv) {type=_NedelecFace;shortname="NF";}
  if(type==_Nedelec && conformSpace==_Hrot) type=_NedelecEdge;
  if (subtype == _standard) subname = "";
  else
  {
      if(subtype ==_GaussLobattoPoints) shortname+="_GL";
      if(subtype ==_firstFamily)  shortname+="1";
      if(subtype ==_secondFamily) shortname+="2";
  }
  shortname+="_"+tostring(numtype);
}

// destructor
Interpolation::~Interpolation()
{
  // remove pointer of current object from vector of Interpolation*
  std::vector<Interpolation*>::iterator it;
  it = find(theInterpolations.begin(), theInterpolations.end(), this);
  if (it != theInterpolations.end()) {theInterpolations.erase(it);}
}

// delete all interpolation objects
void Interpolation::clearGlobalVector()
{
  while( Interpolation::theInterpolations.size() > 0 ) { delete Interpolation::theInterpolations[0]; }
}

// print the list of interpolations in memory
void Interpolation::printAllInterpolations(std::ostream& out)
{
    number_t vb=theVerboseLevel;
    verboseLevel(1);
    out<<"Interpolations in memory: "<<eol;
    std::vector<Interpolation*>::iterator it=theInterpolations.begin();
    for(; it!=theInterpolations.end(); ++it) out<<"  "<<(*it)<<": "<<(**it)<<eol;
    verboseLevel(vb);
}

//! conformSpaceName return space conformity in ad'hoc Space ( L2, H1, Hdiv, Hcurl, ...)
string_t Interpolation::conformSpaceName() const
{
  return words("Sobolev", conformSpace);
}

//! comparison operator between 2 Interpolation objects
bool Interpolation::operator==(const Interpolation& i) const {
	if (this==&i || (type == i.type && subtype==i.subtype && numtype==i.numtype && conformSpace==i.conformSpace && name==i.name && subname==i.subname)) { return true; }
	return false;
}

bool Interpolation::isContinuous() const
{
  switch (conformSpace)
  {
    case _L2:
      return false; // discontinuous interpolation
      break;
    case _H1:
      switch (type)
      {
        case _Lagrange:
          if (numtype > 0) {return true;} // H1-conforming interpolation
          else {return false;} // discontinuous (L2-conforming) interpolation
          break;
        case _Hermite:
          return true;  // H1-conforming interpolation
          break;
        default:
          break;
      }
      break;
    case _Hcurl:
      switch (type)
      {
        case _Nedelec:
          return true;  // Hcurl-conforming interpolation
          break;
        default:
          break;
      }
      break;
    case _Hdiv:
      switch (type)
      {
        case _RaviartThomas:
        case _CrouzeixRaviart:
          return true;  // Hdiv-conforming interpolation
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }
  return false;
}

//! maximumDegree return maximum polynomial degree involved in FE interpolation
number_t Interpolation::maximumDegree() const
{
  number_t maxDegree = numtype;

  switch (type)
  {
    case _Lagrange:
      switch (numtype)
      {
        case _P1BubbleP3:
          maxDegree = 3;
          break;
        default:
          break;
      }
    default:
      break;
  }
  return maxDegree;
}

/*!
  isScalar returns true when all D_O_Fs are scalar, otherwise false
  true for instance for Lagrange or Hermite interpolation
  false for instance for Nedelec or Raviart-Thomas interpolation
 */
bool Interpolation::isScalar() const
{
  switch (type)
  {
    case _Lagrange:
    case _Hermite:
    case _CrouzeixRaviart:
    case _Morley:
    case _Argyris:
      return true;
      break;
    default:
      break;
  }
  return false;
}

//--------------------------------------------------------------------------------
// Error handlers
//--------------------------------------------------------------------------------
void Interpolation::badType() const
{
  error("bad_interp_type", type);
}

void Interpolation::badSubType() const
{
  error("bad_interp_subtype", name, subtype);
}

void Interpolation::badSpace() const
{
  error("bad_interp_space", conformSpace);
}

void Interpolation::badType(const number_t sha) const
{
  error("bad_interp_type_for_shape", type, words("shape", sha));
}

void Interpolation::badSubType(const number_t sha) const
{
  error("bad_interp_subtype_for_shape", name, subtype, words("shape", sha));
}

void Interpolation::badDegree(const number_t sha) const
{
  error("bad_interp_degree", name, subname, numtype, words("shape", sha));
}

//print Interpolation data
void Interpolation::print(std::ostream& out) const
{
  out << "finite element interpolation: " << subname << " " << name << " of order " << numtype << " conforming in " << conformSpaceName();
}

std::ostream& operator<<(std::ostream& out, const Interpolation& intp)
{
  intp.print(out);
  return out;
}

//=================================================================================
// Extern class related functions
//=================================================================================


void buildInterpolationData(InterpolationType interpType, number_t dim, FEType& typ, FESubType& sub, number_t& num, SobolevType& spa)
{
  if (interpType < _P1BubbleP3)
  {
    typ=_Lagrange;
    sub=_standard;
    num=number_t(interpType)-number_t(_P0);
    spa=_H1;
  }
  else if (interpType == _P1BubbleP3)
  {
    typ=_Lagrange;
    sub=_standard;
    num=1;
    spa=_H1;

  }
  else if (interpType == _CR)
  {
    typ=_CrouzeixRaviart;
    sub=_standard;
    num=1;
    spa=_H1;  // not H1 conform but continuity on edge node
  }
  else if (interpType < _RT_1)
  {
    typ=_Lagrange;
    sub=_standard;
    num=number_t(interpType)-number_t(_Q0);
    spa=_H1;
  }
  else if (interpType < _N1_1)
  {
    if (dim == 1) { error("2D_or_3D_only","Space(..., interpType=BDM_i|NF2_i,, ...)"); }
    if (interpType > _RT_5) { error("not_yet_implemented","Space(..., interpType=BDM_i|NF2_i,, ...)"); }
    if (dim == 2)
    {
      typ=_RaviartThomas;
      sub=_standard;
    }
    else
    {
      typ=_NedelecFace;
      sub=_firstFamily;
    }
    num=number_t(interpType)-number_t(_RT_1)+1;
    spa=_Hdiv;
  }
  else
  {
    if (dim == 1) { error("2D_or_3D_only","Space(..., interpType=N2_i|NE2_i,, ...)"); }
    if (interpType > _N1_5) { error("not_yet_implemented","Space(..., interpType=N2_i|NE2_i,, ...)"); }
    if (dim == 2)
    {
      typ=_Nedelec;
      sub=_firstFamily; // instead of _standard
    }
    else
    {
      typ=_NedelecEdge;
      sub=_firstFamily;
    }
    num=number_t(interpType)-number_t(_N1_1)+1;
    spa=_Hcurl;
  }
}

/*!
  return Interpolation defined by its characteristics
  use existing Interpolation object if one already exists
  otherwise create a new one, in both cases returns pointer to the object
 */
Interpolation* findInterpolation(FEType typ, FESubType sub, number_t num, SobolevType spa)
{
  // if (sub == _secondFamily) { error("not_yet_implemented", "Interpolation(..., _secondFamily, ...)"); }
  // find interpolation in vector theInterpolations of Interpolation*
  // (one should not create a new if it already exists)
  std::vector<Interpolation*>::iterator it_FEI_p;
  for (it_FEI_p = Interpolation::theInterpolations.begin(); it_FEI_p != Interpolation::theInterpolations.end(); it_FEI_p++)
  {
    if (typ == (*it_FEI_p)->type && sub == (*it_FEI_p)->subtype
     && num == (*it_FEI_p)->numtype && spa == (*it_FEI_p)->conformSpace) {return *it_FEI_p;}
  }
  // this is a new (a Interpolation never seen before)
  // to be created as a new Interpolation objects and add to the vector theInterpolation
  return new Interpolation(typ, sub, num , spa);
}

Interpolation& interpolation(FEType typ, FESubType sub, number_t num, SobolevType spa)
{
  Interpolation* ip=findInterpolation(typ, sub, num, spa);
  if (ip==nullptr) error("null_pointer","interpolation");
  return *ip;
}

Interpolation* findInterpolation(InterpolationType interpType, number_t dim)
{
  FEType typ;
  FESubType sub;
  number_t num;
  SobolevType spa;
  buildInterpolationData(interpType, dim, typ, sub, num, spa);
  return findInterpolation(typ, sub, num, spa);
}

} // end of namespace xlifepp
