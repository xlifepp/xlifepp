/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RefElement.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 27 nov 2002
  \date 6 aug 2012

  \brief Definition of the xlifepp::RefElement class

  A xlifepp::RefElement object represents the standard FE reference element.
  It is mainly defined by a xlifepp::GeomRefElement and an interpolation which
  implies a predefined set of degrees of freedom (D.o.F)
  Only required xlifepp::RefElement objects are instantiated at run time.

  Inheriting classes:
    - xlifepp::RefSegment (1D)
    - xlifepp::RefTriangle, xlifepp::RefQuadrangle (2D)
    - xlifepp::RefTetrahedron, xlifepp::RefPrism, xlifepp::RefHexahedron (3D)

  IMPORTANT: A finite element involves a lot of arrays of numbering such as
  local numbers in an element, on a side ...

  These numbers always start with first item bearing number 1, though this value is
  stored in item 0 of an array or vector.

  IMPORTANT: Implicit local numbering of D.o.F

  D.o.F in an element are stored and referred to according to what is called
  "the local numbering of D.o.F's" which never appears explicitly in the code.
  Those local numbers of D.o.F can only be "seen" in the arrays of local numbering
  on edges or faces "numbering_of_DOF".
  Sketches of local numbering for 1d an 2d elements can be found in implementation
  of classes xlifepp::RefSegment, xlifepp::RefTriangle or xlifepp::RefQuadrangle.

  That "IMPLICIT" numbering of D.o.F's should obey the following rules which
  are used to create global numbering of D.o.F. in a Finite Element interpolation space:

  -#  D.o.F's supported by vertices of element should be numbered first in the order
  of (local) numbering of vertices, that is D.o.F supported by first vertex
  in element should bear first (or smallest) numbers, and so on.
  This set of D.o.F's can be empty, in which case nbDofsOnVertices_ = 0,
  otherwise nbDofsOnVertices_ is the number of such D.o.F's.
  -# each first D.o.F's supported by a node on an edge (excluding vertices) or
  supported by an edge should be numbered ascendingly according to local numbering
  of edges, then continue with second D.o.F's and so on
  that is first D.o.F with support in edge #1 should bear first number following
  the local numbers of D.o.F's on vertices, then come first D.o.F's on edge #2 and so on.
  This set of D.o.F's can be empty, in which case nbDofsInSideOfSides_ = 0,
  otherwise nbDofsInSideOfSides_ is the number of such D.o.F.s
  -# idem for D.o.F's on faces for 3d elements (replace nbDofsInSideOfSides_ with nbDofsInSides_)
  -# then come all other D.o.F's (for instance, internal or non-sharable D.o.F's)

  As an example, nodes of Pk-Lagrange triangles are numbered as follows

  \verbatim
  2      2          2              2                  2
  | \    | \        | \            | \                | \
  3---1  5   4      5   7          5  10              5  13
   k=1   |     \    |     \        |     \            |     \
         3---6---1  8  10   4      8  14   7          8  17  10
            k=2     |         \    |         \        |         \
                    3---6---9---1 11  15  13   4     11  20  19   7
                         k=3       |             \    |             \
                                   3---6---9--12---1 14  18  21  16   4
                                          k=4         |                 \
                                                      3---6---9--12--15--1
                                                              k=5
  \endverbatim
*/
#ifndef REF_ELEMENT_HPP
#define REF_ELEMENT_HPP

#include "config.h"
#include "utils.h"
#include "Interpolation.hpp"
#include "ShapeValues.hpp"
#include "RefDof.hpp"
#include "GeomRefElement.hpp"


namespace xlifepp
{
class Quadrature;   //forward class

/*! types of map applied to shape functions of reference element
    standard map          : J{-t}         Lagrange FE
    contravariantPiolaMap: J*J{-t}/|J|   div conforming FE
    covariantPiolaMap     : J{-t}*J{-t}   curl conforming FE
    MorleyMap             : see Morley element
    ArgyrisMap            : see Argyris element
*/
enum FEMapType {_standardMap,_contravariantPiolaMap,_covariantPiolaMap,_MorleyMap,_ArgyrisMap};
//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const FEMapType& femt);

/*!
   types of compatibility rule to applied to side dofs
     _noDofCompatibility   : no rule to apply (case of Lagrange dofs)
     _signDofCompatibility: sign correction due to normal/tangent vector orientation (case of Hcurl, Hdiv element)
*/
enum DofCompatibility {_noDofCompatibility,_signDofCompatibility};

/*!
  \class RefElement
  A RefElement object represents the standard FE reference element.
 */
typedef std::vector<std::pair<ShapeType, std::vector<number_t> > > splitvec_t;
class RefElement
{
public:
  // pointers to GeomRefElement and Interpolation
  GeomRefElement* geomRefElem_p;        //!< geometric data is gathered from geometric reference element
  const Interpolation* interpolation_p; //!< interpolation parameters
  std::vector<RefDof*> refDofs;         //!< vector of local reference Degrees of Freedom
  FEMapType mapType;                    //!< type of map applied to shape functions of reference element
  DofCompatibility dofCompatibility;    //!< compatibility rule to applied to side dofs (_noDofCompatibility,_signDofCompatibility)
  dimen_t dimShapeFunction;             //!< dimension of shape function
  bool rotateDof;                       //!< if true, apply rotation (given by childs) to shape values in order to insure correct matching on shared side
  number_t maxDegree;                   //!< maximum degree of shape functions
  bool reverseEdge;                     //!< reverse edge orientation when evaluating dof (default = false)

protected:
  string_t name_;   //!< reference element name (for documentation or debug purposes)
  number_t nbDofs_; //!< number of Degrees Of Freedom
  number_t nbPts_;  //!< number of Points (for local coordinates of points) (NEEDED only as dimension of local coordinates array ?)
  //  number_t nbNodes_; //!< number of Nodes (i.e. D.o.F support, might not be useful)


  // These are only needed to create global numbering of D.o.F's
  number_t nbDofsOnVertices_;    //!< sharable D.o.F on vertices are first D.o.F in element
  number_t nbDofsInSideOfSides_; //!< sharable D.o.F on side of sides (edges), excluding the previous ones, are next
  number_t nbDofsInSides_;       //!< sharable D.o.F on sides (faces or edges), excluding the previous ones, are next
  number_t nbInternalDofs_;      //!< finally come non-sharable D.o.F's

  /*
    These are only used (and should only be relied upon) for FE computation on sides
    They need not be defined for all kinds of elements, for instance for elements
    each side of which does not "naturally" define a finite element in lower dimension
    (e.g. Hcurl or Hdiv edge elements; also see actualRefElement() function)
   */
  std::vector<RefElement*> sideRefElems_;       //!< pointers to side reference elements (in 2d sideRefElems_ is set to sideOfSideRefElems_)
  std::vector<RefElement*> sideOfSideRefElems_; //!< pointers to side of side (1D) reference elements

  // vector<ReferenceSideInterpolation*> side_interpolations_;
  // interpolation data on sides (in 2d side_interpolations_ is set to edge_interpolations_)
  // vector<ReferenceSideInterpolation*> edge_interpolations_;
  // interpolation data on edges
  // Matrix<real_t> shapeFunCoeffs;    //!< matrix representing shape functions coefficients in polynoms basis

public:
  std::vector<std::vector<number_t> > sideDofNumbers_;       //!< list of dof numbers for each side
  std::vector<std::vector<number_t> > sideOfSideDofNumbers_; //!< list of dof numbers for each side of side (edge in 3D)
  //internal side dofs mapping when vertices of side are permuted (used when build global dofs)
  virtual std::vector<number_t> dofsMap(const number_t& i, const number_t& j, const number_t& k=0) const  //!< dofs permutation (NO LONGER USED)
  {return std::vector<number_t>();}  //no permutation
  virtual number_t sideDofsMap(const number_t & n, const number_t & i, const number_t & j, const number_t & k=0) const //face in 3d or edge in 2d
  {return n;}
  virtual number_t sideofsideDofsMap(const number_t & n, const number_t & i, const number_t & j=0) const //edge in 3d
  {return n;}
  number_t sideOf(number_t) const;         //return side number of a dof if it is located on a side (0 if not), 2D and 3D
  number_t sideOfSideOf(number_t) const;   //return side of side number of a dof if it is located on a side of side (0 if not), 3D only

  //data and tools to manage formal representation of shape values, say shape values as polynomials (optional)
public:
  PolynomialsBasis Wk;                             //!< shape functions basis
  std::vector<PolynomialsBasis> dWk;               //!< derivatives of shape functions basis
  PolynomialsBasis Ek;                             //!< auxiliary edge polynomial basis
  PolynomialsBasis Fk;                             //!< auxiliary face polynomial basis
  PolynomialsBasis Kk;                             //!< auxiliary element polynomial basis

public:
  bool hasShapeValues;                                               //!< tell that ref element has shapevalues, generally true
  mutable std::map<Quadrature*,std::vector<ShapeValues> > qshvs_;    //!< temporary structure to store shape values at some points of quadrature
  mutable std::map<Quadrature*,std::vector<ShapeValues> > qshvs_aux; //!< temporary structure to store auxiliary shape values at some points of quadrature

private:
  static const bool isSharable_ = true; //!< returns true is sharable (means that it can be a side of another element)

public:
  static std::vector<RefElement*> theRefElements; //!< vector to store run-time RefElement pointers
  static void clearGlobalVector();                //!< delete all RefElement objects
  static void printAllRefElements(std::ostream&);  //!< print the list of RefElement objects in memory
  static void printAllRefElements(PrintStream& os) {printAllRefElements(os.currentStream());}

  //--------------------------------------------------------------------------------
  // Constructors, Destructor
  // Warning* Constructor should not be invoked directly:
  //          use RefElementFind function to create or find such an object
  //--------------------------------------------------------------------------------
public:
  RefElement(); //!< default constructor
  RefElement(ShapeType, const Interpolation* ); //!< constructor by shape & interpolation
  virtual ~RefElement(); //!< destructor

private: // copy of a RefElement object is forbidden
  RefElement(const RefElement&); //!< no copy constructor
  void operator=(const RefElement&); //!< no assignment operator=

public:
  //--------------------------------------------------------------------------------
  // Public accessors
  //--------------------------------------------------------------------------------

  //! returns associated Geometric Reference Element
  const Interpolation& interpolation() const { return *interpolation_p; }
  //! returns element numtype (order for Lagrange element)
  number_t order() const {return interpolation_p->numtype;}
  bool isSimplex() const                           //! returns true for a segment or triangle or tetrahedron
  {return geomRefElem_p->isSimplex();}
  //! returns element dimension
  dimen_t dim() const { return geomRefElem_p->dim(); }
  //! returns (protected) reference element name
  string_t name() const { return name_; }
  //! returns (protected) number of points
  number_t nbPts() const  { return nbPts_; }
  //! returns (protected) number of D.o.F supported by vertices
  number_t nbDofsOnVertices() const { return nbDofsOnVertices_; }
  //! returns (protected) number of D.o.F supported by edges excluding D.o.F support by vertices
  number_t nbDofsInSideOfSides() const { return nbDofsInSideOfSides_; }
  //! returns (protected) number of D.o.F supported by faces excluding D.o.F supported by vertices or edges
  number_t nbDofsInSides() const { return nbDofsInSides_; }
  //! returns this if sideNum = 0 or reference element of sideDim side sideNum(sideNum = 1, ...)
  // Eric: I am not sure that it is longer true
  const RefElement* actualRefElement(const number_t sideNum = 0, const dimen_t sideDim = 0) const
  {
    if ( sideNum == 0 ) { return this; }
    else if ( sideDim == 1 ) { return sideOfSideRefElems_[sideNum - 1]; }
    else { return sideRefElems_[sideNum - 1];}
  }

  const std::vector<RefElement*>& sideRefElems() const
  {return sideRefElems_;}
  /*!
    returns this if sideNum = 0 or reference element of side sideNum (sideNum = 1, ...)
    non const version, to avoid a forbidden const to non const cast in Space class
  */
  RefElement* refElement(number_t sideNum = 0)
  {
    if ( sideNum == 0 ) { return this; }
    return sideRefElems_[sideNum - 1];
  }
  //! returns this if sideNum = 0 or reference element of side sideNum (sideNum = 1, ...)
  const RefElement* refElement(number_t sideNum = 0) const //!
  {
    if ( sideNum == 0 ) { return this; }
    return sideRefElems_[sideNum - 1];
  }
  //! return associated Geometric Reference Element (i=0) or GeomRefElement of its side i
  const GeomRefElement* geomRefElement(number_t sideNum = 0) const
  {
    if ( sideNum == 0 ) { return geomRefElem_p; }
    return sideRefElems_[sideNum - 1]->geomRefElem_p;
  }
  //   ReferenceSideInterpolation* sideInterpolation(const number_t sideNum, const dimen_t sideDim = 0) const
  //      //! return interpolation pointer if sideNum = 0
  //      //!        interpolation pointer of sideDim side sideNum (sideNum = 1, ...)
  //   {
  //      if ( sideDim == 1 ) return edge_interpolations_[sideNum-1];
  //      else return side_interpolations_[sideNum-1];
  //   }
  //! returns number of element D.o.F's (sideNum = 0) number of D.o.F's on a given sideDim side sideNum (sideNum = 1, ...)
  number_t nbDofs(const number_t sideNum = 0, const dimen_t sideDim = 0) const;
  //! returns number of element internal D.o.F (sideNum = 0 ) or number of internal D.o.F's on  sideDim side sideNum (sideNum = 1, ...)
  number_t nbInternalDofs(const number_t sideNum = 0, const dimen_t sideDim = 0) const;
  ShapeType shapeType() const;   //!<returns shape of element
  virtual number_t nbShapeFcts() const = 0;   //!<returns the number of shape functions

  //  const std::vector<number_t>& vertexDofNumbers()const                  //! dof numbers of dofs on vertices
  //                         {return vertexDofNumbers_;}

  //--------------------------------------------------------------------------------
  // Public Member Utility functions (declarations)
  //--------------------------------------------------------------------------------
  void buildPolynomialTree();                     //!< build tree representation of polynomial shape functions
  virtual void computeShapeFunctions()            //! compute shape functions
  { error("not_handled","computeShapeFunctions()"); }

  size_t shapeValueSize() const; //!< length of shape function storage computed from set of D.o.Fs
  // computes shape values
  virtual void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv = true, const bool with2Deriv=false) const = 0;
// Unused  std::vector<ShapeValues> getShapeValues(const std::vector< std::vector<real_t> >& xs, const bool withDeriv = true) const;
  void computeShapeValuesFromShapeFunctions(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv=false) const;
  virtual void rotateDofs(const std::vector<number_t>&, ShapeValues& shv, bool der1=false, bool der2=false) const {} //!< apply rotation to some shapevalues (rotation given by children)
  virtual Value evalEdgeDof(const Point& S1, const Point& S2,number_t e, number_t i, const Function& f, const Function& gradf, const Function& grad2f) const //! eval edge dof
   {error("not_handled_fe", "RefElement::evalEdgeDof", name());return Value(0);}
  virtual Value evalFaceDof(const FeDof& dof, const GeomElement& selt, const Function& f, const Function&gradf, const Function& grad2f) const //! eval face dof
   {error("not_handled_fe", "RefElement::evalEdgeDof", name());return Value(0);}
  virtual Value evalEltDof(const FeDof& dof, const Function& f, const Function& gradf, const Function& grad2f) const //! eval element dof
   {error("not_handled_fe", "RefElement::evalEdgeDof", name());return Value(0);}

  //split stuff
  virtual std::vector<std::vector<number_t> > splitP1() const; //!< return nodes numbers of first order elements (P1) when splitting current element
  //! return nodes numbers of first order elements of same shape when splitting current element
  //  (Lagrange elements only)
  virtual splitvec_t splitO1() const;
  //! returns reference to splitting scheme computed by splitO1
  virtual const splitvec_t& getO1splitting() const = 0;
  virtual std::vector<std::pair<number_t,number_t> > splitP1Side(number_t s) const;   //!< return side numbers of first order elements (P1) when splitting current element

  //print utility
  virtual void print(std::ostream&, bool withDerivative=false) const;                     //!< print RefElement
  virtual void print(PrintStream& os, bool withDerivative=false) const                    //!  print RefElement
  {print(os.currentStream(), withDerivative);}
  virtual void print(CoutStream& os, bool withDerivative=false) const                    //!  print RefElement
  {   print(std::cout,withDerivative);
      if(os.traceOnFile) print(os.printStream->currentStream(), withDerivative);
  }
  friend std::ostream& operator<<(std::ostream&, const RefElement&);
  void printShapeValues(std::ostream&, std::vector<real_t>::const_iterator it_pt, const ShapeValues& shv) const;  //!< print RefElement shape functions
  void printShapeValues(PrintStream& os, std::vector<real_t>::const_iterator it_pt, const ShapeValues& shv) const //!  print RefElement shape functions
  {printShapeValues(os.currentStream(), it_pt, shv);}
  void printShapeValues(CoutStream& os, std::vector<real_t>::const_iterator it_pt, const ShapeValues& shv) const //!  print RefElement shape functions
  {printShapeValues(std::cout, it_pt, shv); printShapeValues(os.printStream->currentStream(), it_pt, shv);}
  void printShapeValues(std::vector<real_t>::const_iterator it_pt, const ShapeValues& shv) const; //!< print RefElement shape functions
  //! print reference element as P1 reference
  virtual void outputAsP1(std::ofstream& os, const int, const std::vector<number_t>& ranks) const = 0;

protected:
  void sideOfSideRefElement(); //!< reference element constructors on element edges
  void sideRefElement();       //!< reference element constructors on element faces
  void lagrangeRefDofs(dimen_t dimE, number_t nbV, number_t nbDI=0,
                       number_t nbE=0, number_t nbDE=0,
                       number_t nbF=0, number_t nbDF=0);          //!< creates Reference D.O.F in Lagrange standard reference element
  void lagrangeRefDofs(dimen_t dimE, number_t nbV, number_t nbDI,
                       number_t nbE, number_t nbDE,
                       std::vector<number_t> nbDF);          //!< creates Reference D.O.F in Lagrange standard reference element
  //--------------------------------------------------------------------------------
  // errors handlers
  //--------------------------------------------------------------------------------
  void noSuchFunction(const string_t& s) const; //!< function not yet implemented in a child class

  friend class Element;

  // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //
  // #ifdef FIG4TEX_OUTPUT
  //    string fig4TexFileName();
  //    void fig4TeXVertexPt(ofstream&, vector<real_t>::const_iterator, number_t&);
  //    void fig4TeXEdgePt(ofstream&, const number_t, number_t&, const string_t&, const string_t& = "");
  //    void fig4TeXFacePt(ofstream&, const number_t, const number_t, number_t& pts, const string_t& posit);
  //    void fig4TeXInternalPoints2d(ofstream&, number_t&);
  //    void fig4TeXSmallFace(ofstream&, const number_t, const number_t, const number_t);
  // #endif  /* FIG4TEX_OUTPUT */
  // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //

public:
  // splitting tools
  std::vector<std::vector<number_t> > splitLagrange1DToP1() const;  //!< creates the list of nodes numbers of first order segment elements when splitting Lagrange element
  std::vector<std::vector<number_t> > splitLagrange2DToP1() const;  //!< creates the list of nodes numbers of first order triangle elements when splitting Lagrange element
  std::vector<std::vector<number_t> > splitLagrange3DToP1() const;  //!< creates the list of nodes numbers of first order tetrahedron elements when splitting Lagrange element
//  splitvec_t splitLagrange2DToQ1() const;  //!< creates the list of nodes numbers of first order quadrangle elements when splitting Lagrange element (unused)
  splitvec_t splitLagrange3DToQ1() const;  //!< creates the list of nodes numbers of first order hexahedron elements when splitting Lagrange element
  splitvec_t splitLagrange3DToPrismO1() const;  //!< creates the list of nodes numbers of first order prism elements when splitting Lagrange element

}; // end of class RefElement

//=================================================================================
// Extern class related functions
//=================================================================================
std::ostream& operator<<(std::ostream&, const RefElement&); //!< output operator
void simplexVertexOutput(std::ofstream& os, const int, const int v1, const int v2, const int v3 = 0, const int v4 = 0);
bool tetrahedraIntersect(const Point& A1,const Point& B1, const Point& C1, const Point& D1,
                         const Point& A2,const Point& B2, const Point& C2, const Point& D2); //utility

//=================================================================================
// Splitting utility functions
//=================================================================================

/*!
  \struct SortPointsByXAndY
  sort nodes according to coordinates
 */
struct SortPointsByXAndY
{
  bool operator() (const std::pair<number_t,Point>& p, const std::pair<number_t,Point>& q) const;
};

int_t whichSide(const Point& p, const Point& n, const std::vector<Point>& pts); //!< determines if a plane splits a set of points
bool tetrahedraOverlap(const std::vector<Point>& t1,const std::vector<Point>& t2); //!< determines if 2 tetrahedra overlap
bool prismValid(const std::set<std::pair<number_t,Point>, SortPointsByXAndY>& face1, const std::set<std::pair<number_t,Point>,SortPointsByXAndY>& face2); //! determines if a prism is valid
bool areNeighbors2D(const Point& p, const Point& q, real_t tol); //!< determines if 2D points are neighbors
bool areNeighbors3D(const Point& p, const Point& q, real_t tol); //!< determines if 3D points are neighbors

/*!
  RefElementFind
  definition of a Reference Element by shape number and interpolation
  use existing Reference Element object if one already exists
  otherwise create a new one, in both cases returns pointer to the object
 */
RefElement* findRefElement(ShapeType, const Interpolation*);
RefElement* selectRefSegment(const Interpolation*);
RefElement* selectRefTriangle(const Interpolation*);
RefElement* selectRefQuadrangle(const Interpolation*);
RefElement* selectRefTetrahedron(const Interpolation*);
RefElement* selectRefPrism(const Interpolation*);
RefElement* selectRefHexahedron(const Interpolation*);
RefElement* selectRefPyramid(const Interpolation*);

} // end of namespace xlifepp

#endif /* REF_ELEMENT_HPP */
