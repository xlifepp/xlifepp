/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomRefElement.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 27 dec 2002
  \date 6 aug 2012

  \brief Definition of the xlifepp::GeomRefElement class

  a xlifepp::GeomRefElement object represents the geometric support of a RefElement object.

  It is mainly defined by a list of "geometric characteristics".

  Inheriting classes:
    - xlifepp::GeomRefSegment (1D)
    - xlifepp::GeomRefTriangle, xlifepp::GeomRefQuadrangle (2D)
    - xlifepp::GeomRefTetrahedron, xlifepp::GeomRefPrism, xlifepp::GeomRefHexahedron (3D)
*/

#ifndef GEOM_REF_ELEMENT_HPP
#define GEOM_REF_ELEMENT_HPP

#include "config.h"
#include "utils.h"
#include <fstream>
#include <map>

namespace xlifepp
{
/*!
  \class GeomRefElement
 */
class GeomRefElement
{
  protected:
    ShapeType shapeType_;          //!< element shape
    const dimen_t dim_;            //!< element dimension
    const number_t nbVertices_;    //!< number of vertices
    const number_t nbSides_;       //!< number of sides
    const number_t nbSideOfSides_; //!< number of side of sides
    const real_t measure_;         //!< element length, area or volume according to dim
    std::vector<real_t> centroid_; //!< coordinates of element centroid
    std::vector<real_t> vertices_; //!< coordinates of element vertices
    std::vector<ShapeType> sideShapeTypes_;                       //!<  shape type of each side
    std::vector<std::vector<number_t> > sideVertexNumbers_;       //!< vertex numbers of each side
    std::vector<std::vector<number_t> > sideOfSideVertexNumbers_; //!< vertex numbers of each side of side
    std::vector<std::vector<int_t> > sideOfSideNumbers_;          //!< side of side numbers of each side

  public:
    static std::vector<GeomRefElement*> theGeomRefElements; //!< vector carrying all run time GeomRefElements
    static void clearGlobalVector();                        //!< delete all GeomRefElement objects
    static void printAllGeomRefElements(std::ostream&);  //!< print the list of GeomRefElement objects in memory
    static void printAllGeomRefElements(PrintStream& os) {printAllGeomRefElements(os.currentStream());}
    //--------------------------------------------------------------------------------
    // Constructors, Destructor
    // Warning* Constructor should not be invoked directly:
    //          use GeomReferenceElementFind function to create or find such an object
    //--------------------------------------------------------------------------------
  public:
    GeomRefElement(); //!< default constructor
    GeomRefElement(ShapeType, const real_t m = 1.0, const real_t c = 0.5); //!< constructor for 1D linear elements
    GeomRefElement(ShapeType, const real_t, const real_t, const number_t); //!< constructor for 2D polygonal elements with number of vertices
    GeomRefElement(ShapeType, const real_t, const real_t, const number_t, const number_t);  //!< constructor for 3D polyhedral elements with number of vertices and number of edges
    GeomRefElement(ShapeType, const dimen_t, const real_t, const real_t, const number_t, const number_t, const number_t); //!< constructor for any dimension elements, with number of vertices, edges and faces

    virtual ~GeomRefElement(); //!< destructor

  private: // no copy allowed
    GeomRefElement(const GeomRefElement&); //!< no copy constructor
    void operator=(const GeomRefElement&); //!< no assignment operator=

  public:
    //--------------------------------------------------------------------------------
    // public member functions
    //--------------------------------------------------------------------------------

    //! returns element dimension
    dimen_t dim() const { return dim_; }
    //! returns element number of side elements
    number_t nbSides() const { return nbSides_; }
    //! returns element number of side of side elements
    number_t nbSideOfSides() const { return nbSideOfSides_; }
    //! returns element length, area or volume
    real_t measure() const { return measure_; }
    //! returns sideOfSideNumbers
    const std::vector<std::vector<int_t> >& sideOfSideNumbers() const
    {return sideOfSideNumbers_;}

    //! returns centroid as iterator
    std::vector<real_t>::const_iterator centroid() const { return centroid_.begin(); }
    //! returns centroid as point
    const std::vector<real_t>& center() const { return centroid_;}
    //! returns vertices
    std::vector<real_t>::const_iterator vertices() const { return vertices_.begin(); }
    //! returns shape of sides
    std::vector<ShapeType> sideShapeTypes() {return sideShapeTypes_; }
    //! returns vertex numbers of sides
    const std::vector<std::vector<number_t> >& sideVertexNumbers() const { return sideVertexNumbers_; }
    //! returns vertex numbers of side of sides
    const std::vector<std::vector<number_t> >& sideOfSideVertexNumbers() const { return sideOfSideVertexNumbers_; }

    bool isSimplex() const                                   //! returns true for a segment, triangle and tetrahedron
      {return shapeType_==_point || shapeType_==_segment || shapeType_==_triangle || shapeType_==_tetrahedron;}
    string_t shape(const number_t s = 0) const;        //!< returns element shape (s = 0 ) or element side (s > 0) shape as a string
    ShapeType shapeType(const number_t s = 0) const;   //!< returns shape of element or shape of element side as a number (s = 1, ...)
    number_t nbVertices(const number_t s = 0) const;   //!< returns number of element vertices or number of vertices on a side (s = 1, ...)
    std::vector<real_t>::const_iterator vertex(const number_t v) const;         //!< returns coordinates of v-th vertex as a stl-vector ( vNum = 1,2, ...)
    number_t sideVertexNumber(const number_t v, const number_t s) const;        //!< returns local number of v-th vertex ( i = 1,2, ...) on side number s (s = 1,2,...)
    number_t sideOfSideVertexNumber(const number_t v, const number_t ss) const; //!< returns local number of v-th vertex ( i = 1,2, ...) on side of side number ss (ss = 1,2,...)
    int_t sideOfSideNumber(const number_t i, const number_t s) const;           //!< returns local number of i-th edge ( i = 1,2, ...) on face number s (s = 1,2,...)
    const std::vector<int_t>& sideOfSideNumbers(const number_t s) const         //! returns local numbers of edges on face number s (s = 1,2,...)
    {return sideOfSideNumbers_[s-1];}
    virtual std::vector<real_t> projection(const std::vector<real_t>&, real_t&) const;  //!< projection of a point onto ref element
    void rotateVertices(const number_t newFirst, std::vector<number_t>&) const;         //!< circular permutation of vertices such that new_first is first
    virtual bool contains(std::vector<real_t>& p, real_t tol= theTolerance) const;      //!< test if a point belongs to current element
    std::vector<real_t> sideToElt(number_t, std::vector<real_t>::const_iterator, const std::vector<number_t>& perm=std::vector<number_t>()) const; //!< return coordinates of a point on side defined in local coordinates
    friend std::ostream& operator<<(std::ostream&, const GeomRefElement&);

  protected:

    //! build vertex 1d coordinates
    void vertex(std::vector<real_t>::iterator& it, const real_t x1) { *it++ = x1; }
    //! build vertex 2d coordinates
    void vertex(std::vector<real_t>::iterator& it, const real_t x1, const real_t x2) { *it++ = x1; *it++ = x2; }
    //! build vertex 3d coordinates
    void vertex(std::vector<real_t>::iterator& it, const real_t x1, const real_t x2, const real_t x3) { *it++ = x1; *it++ = x2; *it++ = x3; }

    //--------------------------------------------------------------------------------
    // virtual functions (see child classes)
    //--------------------------------------------------------------------------------
  public:
    //! returns edge length or face area for any geometric reference element
    virtual real_t measure(const dimen_t dim, const number_t sideNum = 0) const = 0;
    //! returns local number of a vertex opposite to the side sideNum
    virtual number_t vertexOppositeSide(const number_t sideNum) const = 0;
    //! returns local number of edge bearing vertices with local numbers v1 and v2
    virtual number_t sideWithVertices(const number_t, const number_t) const
    { error("not_handled","GeomRefElement::sideWithVertices(Number,Number)"); return 0;}
    //! returns local number of face bearing vertices with local numbers v1, v2 and v3
    virtual number_t sideWithVertices(const number_t, const number_t, const number_t, const number_t=0) const
    { error("not_handled","GeomRefElement::sideWithVertices(Number,Number,Number)"); return 0;}
    //! node numbers defining first simplex of ref element
    virtual std::vector<number_t> simplexNodes() const
    { error("not_handled","GeomRefElement::simplexNodes()"); return std::vector<number_t>();}

    // //! Returns a quadrature rule built on edge sideNum (=1,2, ...) from a 1d quadrature formula
    // //! or a quadrature rule built on face sideNum (=1,2, ...) from a 2d quadrature formula
    // virtual void sideQuadrature(const QuadratureRule&, QuadratureRule&, const number_t, const dimen_t) const = 0;
    // //! returns tangent vector(s) on edge sideNum (=1,2, ...) or face sideNum (=1,2, ...)
    // virtual void tangentVector(const vector<real_t>& jacobian_matrix, vector< vector<real_t> >& tgv, const number_t, const dimen_t) const = 0;
    // //! returns local number of edge or face opposite to vertex number vertex_no
    // virtual number_t sideOppositeVertex(const number_t vertex_no) const = 0;
    // //! returns local number of edge opposite to edge with local number sideNum
    // virtual number_t edgeOppositeEdge(const number_t sideNum) const = 0;

    //--------------------------------------------------------------------------------
    // errors handlers
    //--------------------------------------------------------------------------------
    void noSuchSideOfSide(const number_t) const; //!< side of side number too large in a child class
    void noSuchSide(const number_t) const; //!< side number too large in a child class
    void noSuchSide(const number_t, const number_t, const number_t = 0, const number_t = 0) const; //!< vertex numbers not found on an side or a side of side
    void noSuchFunction(const string_t& s) const; //!< function not yet implemented in a child class

}; // end of class GeomRefElement

//=================================================================================
// Extern class related functions
//=================================================================================

std::ostream& operator<<(std::ostream&, const GeomRefElement&); //!< print operator

/*!
  definition of a Geom Reference Element by shape type
  use existing Geom Reference Element if one exists, otherwise create a new one,
  in both cases returns pointer to the object
 */
GeomRefElement* findGeomRefElement(ShapeType);

} // end of namespace xlifepp

#endif /* GEOM_REF_ELEMENT_HPP */
