/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file LagrangePyramid.cpp
	\author N. Kielbasiewicz
	\since 23 oct 2012
	\date 5 nov 2012

	\brief Implementation of xlifepp::LagrangePyramid class members and related functions
 */

#include "LagrangePyramid.hpp"
#include "../Interpolation.hpp"
#include "utils.h"

#include <iostream>

namespace xlifepp
{

//! LagrangePyramid constructor for Lagrange reference elements
LagrangePyramid::LagrangePyramid(const Interpolation* interp_p)
  : RefPyramid(interp_p)
{
  name_ += "_Lagrange";
  trace_p->push("LagrangePyramid::LagrangePyramid (" + name_ + ")");

  // build element interpolation data
  interpolationData();
  // define local numbering of points on edges
  sideOfSideNumbering();
  // find or create Reference Element for element edges
  sideOfSideRefElement();
  // define local numbering of points on faces
  sideNumbering();
  // find or create Reference Element for element faces
  sideRefElement();

  trace_p->pop();
}

LagrangePyramid::~LagrangePyramid() {}

//! interpolationData defines Reference Element interpolation data
void LagrangePyramid::interpolationData()
{
  trace_p->push("LagrangePyramid::interpolationData");

  number_t interpNum = interpolation_p->numtype;

  switch ( interpNum )
  {
    case _P0:
      nbDofs_ = nbInternalDofs_ = 1;
      break;
    case _P1:
      nbDofs_ = nbDofsOnVertices_ = 5;
      break;
    default: // Lagrange pyramid of degree > 1
      nbDofs_ = ((interpNum + 1) * (interpNum + 2) * (2*interpNum + 3)) / 6;
      nbInternalDofs_ = ((interpNum - 1) * (interpNum - 2) * (2*interpNum - 3)) / 6;
      nbDofsOnVertices_ = 5;
      nbDofsInSideOfSides_ = 8 * (interpNum - 1);
      nbDofsInSides_ = nbDofs_ - nbInternalDofs_ - nbDofsOnVertices_ - nbDofsInSideOfSides_;
      break;
  }

  // Creating Degrees Of Freedom of reference element
  // in Lagrange standard elements all D.o.F on faces are sharable
  // number of internal nodes (or D.o.F)

  refDofs.reserve(nbDofs_);
  std::vector<number_t> nbDofsPerSide(5,0);
  if (interpNum >= 2)
  {
    nbDofsPerSide[0]=(interpNum - 1) * (interpNum - 1);
    nbDofsPerSide[1]=((interpNum - 2) * (interpNum - 1)) / 2;
    nbDofsPerSide[2]=((interpNum - 2) * (interpNum - 1)) / 2;
    nbDofsPerSide[3]=((interpNum - 2) * (interpNum - 1)) / 2;
    nbDofsPerSide[4]=((interpNum - 2) * (interpNum - 1)) / 2;
  }
  lagrangeRefDofs(3, nbDofsOnVertices_, nbInternalDofs_, 8, nbDofsInSideOfSides_, nbDofsPerSide);
  //lagrangeRefDofs(1, nbDofsOnVertices_, nbDofs_ - nbInternalDofs_, 3);
  // compute number of shape functions
  nbshfcts_ = shapeValueSize();

  // Lagrange pyramid serendipity element of degree 2
  //   {
  //      nbDofs_=15;
  //   }
  nbPts_ = nbDofs_;

  trace_p->pop();
}

//! output of Lagrange D.o.f's numbers on underlying tetrahedron _P1 D.o.f's
void LagrangePyramid::outputAsP1(std::ofstream& os, const int refNum, const std::vector<number_t>& rks) const
{
	noSuchFunction("outputAsP1");
//  //const number_t w(9);
//  number_t interpNum = interpolation_p->numtype;
//
//  switch ( interpNum )
//  {
//    case _P0:
//    case _P1:
//      simplexVertexOutput(os, refNum, rks[0], rks[1], rks[2], rks[4]);
//      simplexVertexOutput(os, refNum, rks[2], rks[3], rks[4], rks[5]);
//      simplexVertexOutput(os, refNum, rks[0], rks[2], rks[3], rks[4]);
//      break;
//    case _P2:
//      // 8 _P1 pyramids split each into 3 _P1 tetrahedra
//      // 3 tetrahedra
//      simplexVertexOutput(os, refNum, rks[ 0], rks[ 6], rks[ 8], rks[15]);
//      simplexVertexOutput(os, refNum, rks[ 8], rks[12], rks[15], rks[17]);
//      simplexVertexOutput(os, refNum, rks[ 0], rks[ 8], rks[12], rks[15]);
//      // 3 tetrahedra
//      simplexVertexOutput(os, refNum, rks[ 6], rks[ 1], rks[ 7], rks[13]);
//      simplexVertexOutput(os, refNum, rks[ 7], rks[15], rks[14], rks[16]);
//      simplexVertexOutput(os, refNum, rks[ 6], rks[ 7], rks[15], rks[13]);
//      // 3 tetrahedra
//      simplexVertexOutput(os, refNum, rks[12], rks[15], rks[17], rks[ 9]);
//      simplexVertexOutput(os, refNum, rks[17], rks[ 3], rks[ 9], rks[11]);
//      simplexVertexOutput(os, refNum, rks[12], rks[17], rks[ 3], rks[ 9]);
//      // 3 tetrahedra
//      simplexVertexOutput(os, refNum, rks[15], rks[13], rks[16], rks[ 4]);
//      simplexVertexOutput(os, refNum, rks[16], rks[ 9], rks[ 9], rks[11]);
//      simplexVertexOutput(os, refNum, rks[12], rks[17], rks[ 4], rks[10]);
//      // 3 tetrahedra
//      simplexVertexOutput(os, refNum, rks[ 8], rks[ 7], rks[ 2], rks[16]);
//      simplexVertexOutput(os, refNum, rks[ 2], rks[17], rks[16], rks[14]);
//      simplexVertexOutput(os, refNum, rks[ 8], rks[ 2], rks[17], rks[16]);
//      // 3 tetrahedra
//      simplexVertexOutput(os, refNum, rks[17], rks[16], rks[14], rks[10]);
//      simplexVertexOutput(os, refNum, rks[14], rks[11], rks[10], rks[ 5]);
//      simplexVertexOutput(os, refNum, rks[17], rks[14], rks[11], rks[10]);
//      // 3 tetrahedra
//      simplexVertexOutput(os, refNum, rks[ 7], rks[ 8], rks[ 6], rks[17]);
//      simplexVertexOutput(os, refNum, rks[ 6], rks[16], rks[17], rks[15]);
//      simplexVertexOutput(os, refNum, rks[ 7], rks[ 6], rks[16], rks[17]);
//      // 3 tetrahedra
//      simplexVertexOutput(os, refNum, rks[16], rks[17], rks[15], rks[11]);
//      simplexVertexOutput(os, refNum, rks[15], rks[10], rks[12], rks[ 9]);
//      simplexVertexOutput(os, refNum, rks[16], rks[15], rks[10], rks[11]);
//      break;
//    default: noSuchFunction("outputAsP1"); break;
//  }
}

//! pointCoordinates defines Lagrange Reference Element point coordinates
std::vector<RefDof*>::iterator LagrangePyramid::vertexCoordinates()
{
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();
  (*it_rd++)->coords(0., 0., 0.);
  (*it_rd++)->coords(1., 0., 0.);
  (*it_rd++)->coords(1., 1., 0.);
  (*it_rd++)->coords(0., 1., 0.);
  (*it_rd++)->coords(0., 0., 1.);
  return it_rd;
}

void LagrangePyramid::pointCoordinates()
{
  trace_p->push("LagrangePyramid::pointCoordinates");

  // Local numbering of Lagrange Pk(1D) x Pk(2D) pyramid
  number_t interpNum = interpolation_p->numtype;
  std::vector<RefDof*>::iterator it_rd = refDofs.begin();

  switch ( interpNum )
  {
    case _P0:
      (*it_rd)->coords(0.4, 0.4, 0.2);
      break;
    case _P1:
      it_rd = vertexCoordinates();
      break;
    case _P2:
      it_rd = vertexCoordinates();
      (*it_rd++)->coords(0.5, 0., 0.); // 6
      (*it_rd++)->coords(0., 0.5, 0.); // 7
      (*it_rd++)->coords(0., 0., 0.5); // 8
      (*it_rd++)->coords(1., 0.5, 0.); // 9
      (*it_rd++)->coords(0.5, 0., 0.5); //10
      (*it_rd++)->coords(0.5, 1., 0.); //11
      (*it_rd++)->coords(0.5, 0.5, 0.5); //12
      (*it_rd++)->coords(0., 0.5, 0.5); //13
      (*it_rd++)->coords(0.5, 0.5, 0.); //14
      break;
    default:
      break;
  }
  trace_p->pop();
}

//! sideNumbering defines Lagrange Reference Element numbering on faces
void LagrangePyramid::sideNumbering()
{
  // Local numbering of Lagrange Pk(1D) x Qk (2D) pyramid
  trace_p->push("LagrangePyramid::sideNumbering");

  number_t interpNum = interpolation_p->numtype;
  number_t nbSides = geomRefElem_p->nbSides();
  sideDofNumbers_.resize(nbSides);

  if(interpNum==0)  //P0 interpolation
  {
       for(number_t side = 0; side < nbSides; side++)
       {
           sideDofNumbers_[side].resize(1);
           sideDofNumbers_[side][0]=1;
       }
       return;
  }

  if ( interpNum > 0 )
  {
    // Lagrange pyramidatic element (equidistant points) of degree k = 1, 2
    // local numbering of vertices
    number_t sideNum, nbDofsPerSide;

    // face #0 : x3 = 0 (quadrangular)
    sideNum = 0;
    nbDofsPerSide = (interpNum + 1) * (interpNum + 1);
    sideDofNumbers_[sideNum].resize(nbDofsPerSide);
    sideDofNumbers_[sideNum][0] = 1;
    sideDofNumbers_[sideNum][1] = 2;
    sideDofNumbers_[sideNum][2] = 3;
    sideDofNumbers_[sideNum][3] = 4;
    if (interpNum == 2)
    {
      sideDofNumbers_[sideNum][4] = 6; // dofs on side of side
      sideDofNumbers_[sideNum][5] = 9;
      sideDofNumbers_[sideNum][6] = 11;
      sideDofNumbers_[sideNum][7] = 7;
      sideDofNumbers_[sideNum][8] = 14; // internal dof
    }
    // face #1 : x2 = 0 (triangular)
    sideNum = 1;
    nbDofsPerSide = ((interpNum + 1) * (interpNum + 2)) / 2;
    sideDofNumbers_[sideNum].resize(nbDofsPerSide);
    sideDofNumbers_[sideNum][0] = 1;
    sideDofNumbers_[sideNum][1] = 2;
    sideDofNumbers_[sideNum][2] = 5;
    if (interpNum == 2)
    {
      sideDofNumbers_[sideNum][3] = 6; // dofs on side of side
      sideDofNumbers_[sideNum][4] = 10;
      sideDofNumbers_[sideNum][5] = 8;
    }
    // face #2 : x1+x3 = 1 (triangular)
    sideNum = 2;
    nbDofsPerSide = ((interpNum + 1) * (interpNum + 2)) / 2;
    sideDofNumbers_[sideNum].resize(nbDofsPerSide);
    sideDofNumbers_[sideNum][0] = 2;
    sideDofNumbers_[sideNum][1] = 3;
    sideDofNumbers_[sideNum][2] = 5;
    if (interpNum == 2)
    {
      sideDofNumbers_[sideNum][3] = 9; // dofs on side of side
      sideDofNumbers_[sideNum][4] = 12;
      sideDofNumbers_[sideNum][5] = 10;
    }
    // face #3 : x2+x3 = 1 (triangular)
    sideNum = 3;
    nbDofsPerSide = ((interpNum + 1) * (interpNum + 2)) / 2;
    sideDofNumbers_[sideNum].resize(nbDofsPerSide);
    sideDofNumbers_[sideNum][0] = 3;
    sideDofNumbers_[sideNum][1] = 4;
    sideDofNumbers_[sideNum][2] = 5;
    if (interpNum == 2)
    {
      sideDofNumbers_[sideNum][3] = 11; // dofs on side of side
      sideDofNumbers_[sideNum][4] = 13;
      sideDofNumbers_[sideNum][5] = 12;
    }
    // face #4 : x1 = 0 (triangular)
    sideNum = 4;
    nbDofsPerSide = ((interpNum + 1) * (interpNum + 2)) / 2;
    sideDofNumbers_[sideNum].resize(nbDofsPerSide);
    sideDofNumbers_[sideNum][0] = 4;
    sideDofNumbers_[sideNum][1] = 1;
    sideDofNumbers_[sideNum][2] = 5;
    if (interpNum == 2)
    {
      sideDofNumbers_[sideNum][3] = 7; // dofs on side of side
      sideDofNumbers_[sideNum][4] = 8;
      sideDofNumbers_[sideNum][5] = 13;
    }
  }
  trace_p->pop();
}

//! sideOfSideNumbering defines Lagrange Reference Element numbering on side of sides
void LagrangePyramid::sideOfSideNumbering()
{
  //	trace_p->push("LagrangePyramid::sideOfSideNumbering");
  number_t interpNum = interpolation_p->numtype;

  if ( interpNum > 0 )
  {
    number_t intN = interpNum;
    //if ( interpNum == _P1BubbleP3 ) { intN = 1; }
    number_t nbVertices = geomRefElem_p->nbVertices();
    number_t nbSideOfSides = geomRefElem_p->nbSideOfSides();
    number_t more;
    number_t nbVerticesPerSideOfSide = geomRefElem_p->sideOfSideVertexNumbers()[0].size();
    number_t nbDofsPerSideOfSide = intN + 1;

    sideOfSideDofNumbers_.resize(nbSideOfSides);
    for (number_t sideOfSide = 0; sideOfSide < nbSideOfSides; sideOfSide++)
    {
      sideOfSideDofNumbers_[sideOfSide].resize(nbDofsPerSideOfSide);
      for (number_t j = 0; j < nbVerticesPerSideOfSide; j++)
      {
        sideOfSideDofNumbers_[sideOfSide][j] = geomRefElem_p->sideOfSideVertexNumber(j + 1, sideOfSide + 1);
      }
      if (intN > 1)
      {
        more = nbVertices + 1;
        for (number_t k = nbVerticesPerSideOfSide; k < nbDofsPerSideOfSide; k++, more += nbSideOfSides)
        {
          sideOfSideDofNumbers_[sideOfSide][k] = sideOfSide + more;
        }
      }
    }
  }
  //	trace_p->pop();
}

//! return nodes numbers of first order tetrahedron elements when splitting current element
std::vector<std::vector<number_t> > LagrangePyramid::splitP1() const {
  std::vector<std::vector<number_t> > splitnum;
  std::vector<number_t> tuple(4,0);
  number_t k=interpolation_p->numtype;
  switch (k) {
    case 0:
    case 1:
      tuple[0]=2; tuple[1]=4; tuple[2]=5; tuple[3]=1;
      splitnum.push_back(tuple);
      tuple[0]=4; tuple[1]=2; tuple[2]=5; tuple[3]=3;
      splitnum.push_back(tuple);
      return splitnum;
    case 2:
      // O1 sub-pyramid 1 6 14 7 8
      tuple[0]=6; tuple[1]=7; tuple[2]=8; tuple[3]=1;
      splitnum.push_back(tuple);
      tuple[0]=7; tuple[1]=6; tuple[2]=8; tuple[3]=3;
      splitnum.push_back(tuple);
      // O1 sub-pyramid 6 2 9 14 10
      tuple[0]=2; tuple[1]=14; tuple[2]=10; tuple[3]=6;
      splitnum.push_back(tuple);
      tuple[0]=14; tuple[1]=2; tuple[2]=10; tuple[3]=9;
      splitnum.push_back(tuple);
      // O1 sub-pyramid 14 9 3 11 12
      tuple[0]=9; tuple[1]=11; tuple[2]=12; tuple[3]=14;
      splitnum.push_back(tuple);
      tuple[0]=11; tuple[1]=9; tuple[2]=12; tuple[3]=3;
      splitnum.push_back(tuple);
      // O1 sub-pyramid 7 14 11 4 13
      tuple[0]=14; tuple[1]=4; tuple[2]=13; tuple[3]=7;
      splitnum.push_back(tuple);
      tuple[0]=4; tuple[1]=14; tuple[2]=13; tuple[3]=11;
      splitnum.push_back(tuple);
      // O1 sub-pyramid 8 10 12 13 5
      tuple[0]=10; tuple[1]=13; tuple[2]=5; tuple[3]=8;
      splitnum.push_back(tuple);
      tuple[0]=13; tuple[1]=10; tuple[2]=5; tuple[3]=12;
      splitnum.push_back(tuple);
      // O1 sub-pyramid 12 13 8 10 14
      tuple[0]=13; tuple[1]=10; tuple[2]=14; tuple[3]=12;
      splitnum.push_back(tuple);
      tuple[0]=10; tuple[1]=13; tuple[2]=14; tuple[3]=8;
      splitnum.push_back(tuple);
      // P1 sub-tetrahedron
      tuple[0]=11; tuple[1]=12; tuple[2]=14; tuple[3]=13;
      splitnum.push_back(tuple);
      tuple[0]=10; tuple[1]=12; tuple[2]=14; tuple[3]=9;
      splitnum.push_back(tuple);
      tuple[0]=8; tuple[1]=10; tuple[2]=14; tuple[3]=6;
      splitnum.push_back(tuple);
      tuple[0]=13; tuple[1]=8; tuple[2]=14; tuple[3]=7;
      splitnum.push_back(tuple);
      return splitnum;
    default:
      return splitLagrange3DToP1();
  }
  return splitnum;
}

//! return nodes numbers of first order pyramid and tetrahedron elements when splitting current element
splitvec_t LagrangePyramid::splitO1() const
{
  splitvec_t splitnum;
  std::vector<number_t> tuple(5,0);
  number_t k=interpolation_p->numtype;
  switch (k)
  {
    case 1:
      tuple[0]=1; tuple[1]=2; tuple[2]=3; tuple[3]=4; tuple[4]=5;
      splitnum.push_back(std::pair<ShapeType, std::vector<number_t> > (_pyramid,tuple));
      return splitnum;
    case 2:
      tuple[0]=1; tuple[1]=6; tuple[2]=14; tuple[3]=7; tuple[4]=8;
      splitnum.push_back(std::pair<ShapeType, std::vector<number_t> > (_pyramid,tuple));
      tuple[0]=6; tuple[1]=2; tuple[2]=9; tuple[3]=14; tuple[4]=10;
      splitnum.push_back(std::pair<ShapeType, std::vector<number_t> > (_pyramid,tuple));
      tuple[0]=14; tuple[1]=9; tuple[2]=3; tuple[3]=11; tuple[4]=12;
      splitnum.push_back(std::pair<ShapeType, std::vector<number_t> > (_pyramid,tuple));
      tuple[0]=7; tuple[1]=14; tuple[2]=11; tuple[3]=4; tuple[4]=13;
      splitnum.push_back(std::pair<ShapeType, std::vector<number_t> > (_pyramid,tuple));
      tuple[0]=8; tuple[1]=10; tuple[2]=12; tuple[3]=13; tuple[4]=5;
      splitnum.push_back(std::pair<ShapeType, std::vector<number_t> > (_pyramid,tuple));
      tuple[0]=12; tuple[1]=13; tuple[2]=8; tuple[3]=10; tuple[4]=14;
      splitnum.push_back(std::pair<ShapeType, std::vector<number_t> > (_pyramid,tuple));
      tuple.resize(4);
      tuple[0]=11; tuple[1]=12; tuple[2]=14; tuple[3]=13;
      splitnum.push_back(std::pair<ShapeType, std::vector<number_t> > (_tetrahedron,tuple));
      tuple[0]=10; tuple[1]=12; tuple[2]=14; tuple[3]=9;
      splitnum.push_back(std::pair<ShapeType, std::vector<number_t> > (_tetrahedron,tuple));
      tuple[0]=8; tuple[1]=10; tuple[2]=14; tuple[3]=6;
      splitnum.push_back(std::pair<ShapeType, std::vector<number_t> > (_tetrahedron,tuple));
      tuple[0]=13; tuple[1]=8; tuple[2]=14; tuple[3]=7;
      splitnum.push_back(std::pair<ShapeType, std::vector<number_t> > (_tetrahedron,tuple));
      return splitnum;
    default: error("split_low_deg_Lagrange_elt", words("shape",_pyramid) , 2, words("shape",_tetrahedron)+" "+words("and")+words("shape",_pyramid));
  }
  return splitnum;
}

//! Lagrange standard Pk pyramid Reference Element
template<number_t Pk>
LagrangeStdPyramid<Pk>::LagrangeStdPyramid(const Interpolation* interp_p)
  : LagrangePyramid(interp_p)
{
  name_ += "_" + tostring(Pk);
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

//! Lagrange standard _P0 pyramid Reference Element
template<>
LagrangeStdPyramid<_P0>::LagrangeStdPyramid(const Interpolation* interp_p)
  : LagrangePyramid(interp_p)
{
  name_ += "_0";
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  maxDegree = 0;
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

//! Lagrange standard _P1 pyramid Reference Element
template<>
LagrangeStdPyramid<_P1>::LagrangeStdPyramid(const Interpolation* interp_p)
  : LagrangePyramid(interp_p)
{
  name_ += "_1";
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  maxDegree = 5;   //shape function are not polynomials, set 5 by default
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

//! Lagrange standard _P2 pyramid Reference Element
template<>
LagrangeStdPyramid<_P2>::LagrangeStdPyramid(const Interpolation* interp_p)
  : LagrangePyramid(interp_p)
{
  name_ += "_2";
  // local coordinates of Points supporting D.o.F
  pointCoordinates();
  maxDegree = 5;   //shape function are not polynomials, set 5 by default
  // build O1 splitting scheme
  splitO1Scheme = splitO1();
}

//! computeShapeValues defines Lagrange Pk Reference Element shape functions
template<>
void LagrangeStdPyramid<_P0>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  std::vector<real_t>::iterator sh0w_i(shv.w.begin());
  *sh0w_i = 1.;

  if ( withDeriv )
  {
    std::vector< std::vector<real_t> >::iterator sh1w(shv.dw.begin()), sh2w(sh1w + 1), sh3w(sh1w + 2);
    std::vector<real_t>::iterator sh1w_i((*sh1w).begin()), sh2w_i((*sh2w).begin()), sh3w_i((*sh3w).begin());
    *sh1w_i = 0.; *sh2w_i = 0.; *sh3w_i = 0.;
  }
}

template<>
void LagrangeStdPyramid<_P1>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x1 = *it_pt, x2 = *(it_pt + 1), x3 = *(it_pt + 2), x3m1 = x3 - 1.0, over1mx3 = 0.0;
  real_t d = x1*x1+x2*x2+(x3m1)*(x3m1);

  if (d > theEpsilon) { over1mx3 = -1.0/x3m1; }

  real_t x1x2over1mx3 = x1 * x2 * over1mx3;

  std::vector<real_t>::iterator sh0w_i = shv.w.begin();
  *sh0w_i++ = 1. - x1 - x2 - x3 + x1x2over1mx3;
  *sh0w_i++ = x1 - x1x2over1mx3;
  *sh0w_i++ = x1x2over1mx3;
  *sh0w_i++ = x2 - x1x2over1mx3;
  *sh0w_i = x3;

  if ( withDeriv )
  {
    std::vector< std::vector<real_t> >::iterator sh1w(shv.dw.begin()), sh2w(sh1w + 1), sh3w(sh1w + 2);
    std::vector<real_t>::iterator sh1w_i((*sh1w).begin()), sh2w_i((*sh2w).begin()), sh3w_i((*sh3w).begin());
    real_t x1over1mx3 = x1 * over1mx3;
    real_t x2over1mx3 = x2 * over1mx3;
    *sh1w_i++ = -1.0 + x2over1mx3; *sh2w_i++ = -1.0 + x1over1mx3; *sh3w_i++ = -1.0 + x1x2over1mx3*over1mx3;
    *sh1w_i++ = 1.0 - x2over1mx3;  *sh2w_i++ = -x1over1mx3;       *sh3w_i++ = -x1x2over1mx3*over1mx3;
    *sh1w_i++ = x2over1mx3;        *sh2w_i++ = x1*over1mx3;       *sh3w_i++ = x1x2over1mx3*over1mx3;
    *sh1w_i++ = -x2over1mx3;       *sh2w_i++ = 1.0-x1over1mx3;    *sh3w_i++ = -x1x2over1mx3*over1mx3;
    *sh1w_i = 0.0;                 *sh2w_i = 0.0;                 *sh3w_i = 1.0;
  }
}

template<>
void LagrangeStdPyramid<_P2>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x1 = *it_pt, x2 = *(it_pt + 1), x3 = *(it_pt + 2), x3m1 = x3 - 1.0, over1mx3=0.;
  real_t d = x1*x1+x2*x2+(x3m1)*(x3m1);

  if (d > theEpsilon) { over1mx3 = -1.0/x3m1; }

  real_t x1x2over1mx3 = x1 * x2 * over1mx3;
  real_t x1px2 = x1 + x2;
  real_t x1px2px3 = x1px2 + x3;
  real_t x1x2 = x1 * x2;
  real_t x1x3 = x1 * x3;
  real_t x2x3 = x2 * x3;
  real_t x1x1 = x1 * x1;
  real_t x2x2 = x2 * x2;
  real_t x3x3 = x3 * x3;

  std::vector<real_t>::iterator sh0w_i = shv.w.begin();
  *sh0w_i++ = 1.0 - 3*x1px2px3 + 2.0*x1px2px3*x1px2px3 + 4.0*x1x2 + 4.0*x1x2over1mx3*x1x2over1mx3 - 2.0*(1.0 + 2.0*x1px2)*x1x2over1mx3;
  *sh0w_i++ = -x1 + 2.0*x1x1 + 2.0*x1x2 + 4.0*x1x2over1mx3*x1x2over1mx3 + (1.0 - 6.0*x1 - 2.0*x2)*x1x2over1mx3;
  *sh0w_i++ = 2.0*x1x2 + 4.0*x1x2over1mx3*x1x2over1mx3 - (1.0 + 2.0*x1px2)*x1x2over1mx3;
  *sh0w_i++ = -x2 + 2.0*x2x2 + 2.0*x1x2 + 4.0*x1x2over1mx3*x1x2over1mx3 + (1.0 - 2.0*x1 - 6.0*x2)*x1x2over1mx3;
  *sh0w_i++ = -x3 + 2.0 * x3x3;
  *sh0w_i++ = 4.0*x1 - 4.0*x1x1 - 15.0*x1x2 -4.0*x1x3 - 14.0*x1x2over1mx3*x1x2over1mx3 +(15.0*x1 + 14.0*x2)*x1x2over1mx3;
  *sh0w_i++ = 4.0*x2 - 4.0*x2x2 - 15.0*x1x2 -4.0*x2x3 - 14.0*x1x2over1mx3*x1x2over1mx3 +(14.0*x1 + 15.0*x2)*x1x2over1mx3;
  *sh0w_i++ = 4.0*(x3 - x1x2 - x3*x1px2px3 + x1x2over1mx3);
  *sh0w_i++ = -4.0*x1x2 - 8.0*x1x2over1mx3*x1x2over1mx3 + (8.0*x1 + 4.0*x2)*x1x2over1mx3;
  *sh0w_i++ = x1x2 + 4.0*x1x3 - x1x2over1mx3;
  *sh0w_i++ = -4.0*x1x2 - 8.0*x1x2over1mx3*x1x2over1mx3 + (4.0*x1 + 8.0*x2)*x1x2over1mx3;
  *sh0w_i++ = 4.0*(-x1x2 + x1x2over1mx3);
  *sh0w_i++ = x1x2 + 4.0*x2x3 - x1x2over1mx3;
  *sh0w_i = 16.0*(x1x2 + x1x2over1mx3*x1x2over1mx3 - x1px2*x1x2over1mx3);

  if ( withDeriv )
  {
    std::vector< std::vector<real_t> >::iterator sh1w(shv.dw.begin()), sh2w(sh1w + 1), sh3w(sh1w + 2);
    std::vector<real_t>::iterator sh1w_i((*sh1w).begin()), sh2w_i((*sh2w).begin()), sh3w_i((*sh3w).begin());
    real_t x1over1mx3 = x1 * over1mx3;
    real_t x2over1mx3 = x2 * over1mx3;
    real_t x1x2over1mx3over1mx3 = x1x2over1mx3 * over1mx3;
    real_t x1x2x2over1mx3over1mx3 = x2 * x1x2over1mx3over1mx3;
    real_t x1x1x2over1mx3over1mx3 = x1*x1x2over1mx3over1mx3;
    real_t x1x1x2x2over1mx3over1mx3over1mx3 = x1x2 * x1x2over1mx3over1mx3 * over1mx3;
    // 1
    *sh1w_i++ = -3.0 + 4.0*x1px2px3 + 4.0*x2 + 8.0*x1x2x2over1mx3over1mx3 - 4.0*x1x2over1mx3 - 2.0*(1.0 + 2.0*x1px2)*x2over1mx3;
    *sh2w_i++ = -3.0 + 4.0*x1px2px3 + 4.0*x1 + 8.0*x1x1x2over1mx3over1mx3 - 4.0*x1x2over1mx3 - 2.0*(1.0 + 2.0*x1px2)*x1over1mx3;
    *sh3w_i++ = -3.0 + 4.0*x1px2px3 + 8.0*x1x1x2x2over1mx3over1mx3over1mx3 - 2.0*(1.0 + 2.0*x1px2)*x1x2over1mx3over1mx3;
    // 2
    *sh1w_i++ = -1.0 + 4.0*x1 + 2.0*x2 + 8.0*x1x2x2over1mx3over1mx3 - 6.0*x1x2over1mx3 + (1.0 - 6.0*x1 + 2.0*x2)*x2over1mx3;
    *sh2w_i++ = 2.0*x1 + 8.0*x1x1x2over1mx3over1mx3 - 6.0*x1x2over1mx3 + (1.0 - 6.0*x1 + 2.0*x2)*x1over1mx3;
    *sh3w_i++ = 8.0*x1x1x2x2over1mx3over1mx3over1mx3 + (1.0 - 6.0*x1 + 2.0*x2)*x1x2over1mx3over1mx3;
    // 3
    *sh1w_i++ = 2.0*x2 + 8.0*x1x2x2over1mx3over1mx3 - 2.0*x1x2over1mx3 - (1.0 + 2.0*x1 + 2.0*x2)*x2over1mx3;
    *sh2w_i++ = 2.0*x1 + 8.0*x1x1x2over1mx3over1mx3 - 2.0*x1x2over1mx3 - (1.0 + 2.0*x1 + 2.0*x2)*x1over1mx3;
    *sh3w_i++ = 8.0*x1x1x2x2over1mx3over1mx3over1mx3 - (1.0 + 2.0*x1 + 2.0*x2)*x1x2over1mx3over1mx3;
    // 4
    *sh1w_i++ = 2.0*x1 + 8.0*x1x2x2over1mx3over1mx3 - 2.0*x1x2over1mx3 + (1.0 - 2.0*x1 + 6.0*x2)*x2over1mx3;
    *sh2w_i++ = -1.0 + 2.0*x1 + 4.0*x2 + 8.0*x1x1x2over1mx3over1mx3 - 2.0*x1x2over1mx3 + (1.0 - 2.0*x1 + 6.0*x2)*x1over1mx3;
    *sh3w_i++ = 8.0*x1x1x2x2over1mx3over1mx3over1mx3 + (1.0 - 2.0*x1 + 6.0*x2)*x1x2over1mx3over1mx3;
    // 5
    *sh1w_i++ = 0.;
    *sh2w_i++ = 0.;
    *sh3w_i++ = -1.0 + 4.0*x3;
    // 6
    *sh1w_i++ = 4.0 - 8.0*x1 - 15.0*x2 - 4.0*x3 - 28.0*x1x2x2over1mx3over1mx3 + 15.0*x1x2over1mx3 + (15.0*x1 + 14.0*x2)*x2over1mx3;
    *sh2w_i++ = -15.0*x1 - 28.0*x1x1x2over1mx3over1mx3 + 14.0*x1x2over1mx3 + (15.0*x1 + 14.0*x2)*x1over1mx3;
    *sh3w_i++ = -4.0*x1 - 28.0*x1x1x2x2over1mx3over1mx3over1mx3 + (15.0*x1 + 14.0*x2)*x1x2over1mx3over1mx3;
    // 7
    *sh1w_i++ = -15.0*x2 - 28.0*x1x2x2over1mx3over1mx3 + 14.0*x1x2over1mx3 + (14.0*x1 + 15.0*x2)*x2over1mx3;
    *sh2w_i++ = 4.0 - 15.0*x1 - 8.0*x2 - 4.0*x3 - 28.0*x1x1x2over1mx3over1mx3 + 15.0*x1x2over1mx3 + (14.0*x1 + 15.0*x2)*x1over1mx3;
    *sh3w_i++ = -4.0*x2 - 28.0*x1x1x2x2over1mx3over1mx3over1mx3 + (14.0*x1 + 15.0*x2)*x1x2over1mx3over1mx3;
    // 8
    *sh1w_i++ = -4.0*x2 - 4.0*x3 + 4.0*x2over1mx3;
    *sh2w_i++ = -4.0*x1 - 4.0*x3 + 4.0*x1over1mx3;
    *sh3w_i++ = 4.0 - 8.0*x3 - 4.0*x1px2 + 4.0*x1x2over1mx3over1mx3;
    // 9
    *sh1w_i++ = -4.0*x2 - 16.0*x1x2x2over1mx3over1mx3 + 8.0*x1x2over1mx3 + (8.0*x1 + 4.0*x2)*x2over1mx3;
    *sh2w_i++ = -4.0*x1 - 16.0*x1x1x2over1mx3over1mx3 + 4.0*x1x2over1mx3 + (8.0*x1 + 4.0*x2)*x1over1mx3;
    *sh3w_i++ = -8.0*x1x1x2x2over1mx3over1mx3over1mx3 + (8.0*x1 + 4.0*x2)*x1x2over1mx3over1mx3;
    // 10
    *sh1w_i++ = x2 + 4.0*x3 - x2over1mx3;
    *sh2w_i++ = x1 - x1over1mx3;
    *sh3w_i++ = 4.0*x1 - x1x2over1mx3over1mx3;
    // 11
    *sh1w_i++ = -4.0*x2 - 16.0*x1x2x2over1mx3over1mx3 + 4.0*x1x2over1mx3 + (4.0*x1 + 8.0*x2)*x2over1mx3;
    *sh2w_i++ = -4.0*x1 - 16.0*x1x1x2over1mx3over1mx3 + 8.0*x1x2over1mx3 + (4.0*x1 + 8.0*x2)*x1over1mx3;
    *sh3w_i++ = -8.0*x1x1x2x2over1mx3over1mx3over1mx3 + (4.0*x1 + 8.0*x2)*x1x2over1mx3over1mx3;
    // 12
    *sh1w_i++ = -4.0*x2 + 4.0*x2over1mx3;
    *sh2w_i++ = -4.0*x1 + 4.0*x1over1mx3;
    *sh3w_i++ = 4.0*x1x2over1mx3over1mx3;
    // 13
    *sh1w_i++ = x2 - x2over1mx3;
    *sh2w_i++ = x1 + 4.0*x3 - x1over1mx3;
    *sh3w_i++ = 4.0*x2 - x1x2over1mx3over1mx3;
    // 14
    *sh1w_i = 16.0*x2 + 32.0*x1x2x2over1mx3over1mx3 - 16.0*x1x2over1mx3 - 16*x1px2*x2over1mx3;
    *sh2w_i = 16.0*x1 + 32.0*x1x1x2over1mx3over1mx3 - 16.0*x1x2over1mx3 - 16*x1px2*x1over1mx3;
    *sh3w_i = 32.0*x1x1x2x2over1mx3over1mx3over1mx3 - 16*x1px2*x1x2over1mx3over1mx3;
  }
}

} // end of namespace xlifepp
