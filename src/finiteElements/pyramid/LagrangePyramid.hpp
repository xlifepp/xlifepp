/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LagrangePyramid.hpp
  \author N. Kielbasiewicz
  \since 23 oct 2012
  \date 5 nov 2012

  \brief Definition of the xlifepp::LagrangePyramid class

  xlifepp::LagrangePyramid is child to xlifepp::RefPyramid and defines Lagrange Reference Element interpolation data
 */

#ifndef LAGRANGE_PYRAMID_HPP
#define LAGRANGE_PYRAMID_HPP

#include "config.h"
#include "RefPyramid.hpp"

namespace xlifepp
{

/*!
  \class LagrangePyramid
  defines Lagrange Reference Element interpolation data on pyramids
 */
class LagrangePyramid : public RefPyramid
{
  public:
    LagrangePyramid(const Interpolation* interp_p); //!< constructor by interpolation
    virtual ~LagrangePyramid(); //!< destructor
  protected:
    void sideNumbering(); //!< builds local numbers of D.o.F's on faces of reference element
    void sideOfSideNumbering(); //!< builds local numbers of D.o.F's on edges of reference element
    void interpolationData(); //!< builds interpolation data on reference element
    void pointCoordinates(); //!< builds point coordinates on reference element
    void outputAsP1(std::ofstream& os, const int , const std::vector<number_t>& ranks) const; //!< output of Lagrange D.o.f's numbers on underlyin P1 D.o.f's
    std::vector<RefDof*>::iterator vertexCoordinates(); //!< returns vector of vertices dofs
    std::vector<std::vector<number_t> > splitP1() const;  //!< return nodes numbers of first order tetrahedron elements when splitting current element
    splitvec_t splitO1() const;  //!< return nodes numbers of first order pyramid or tetrahedron elements when splitting current element
  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }

}; // end of class LagrangePyramid

/*!
  \class LagrangeStdPyramid
  template class child to class LagrangePyramid

  http://homepage.mac.com/danielmartin/melina++/doc/elements/pyramid/LagrangeStdPyramid1.pdf
  http://homepage.mac.com/danielmartin/melina++/doc/elements/pyramid/LagrangeStdPyramid2.pdf
 */
template<number_t Pk>
class LagrangeStdPyramid : public LagrangePyramid
{
  public:
    LagrangeStdPyramid(const Interpolation* interp_p); //!< constructor by interpolation
    ~LagrangeStdPyramid() {} //!< destructor

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv = true, const bool with2Deriv=false) const; //!< shape functions
    const splitvec_t& getO1splitting() const // override
     { return splitO1Scheme; }
  private:
    // storage of order 1 splitting scheme (intended to graphical softwares usage)
    splitvec_t splitO1Scheme;

}; // end of class LagrangeStdPyramid

} // end of namespace xlifepp

#endif /* LAGRANGE_PYRAMID_HPP */
