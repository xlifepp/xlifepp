/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file GeomRefPyramid.hpp
	\author N. Kielbasiewicz
	\since 23 oct 2012
	\date 5 nov 2012

  \brief Definition of the xlifepp::GeomRefPyramid class

  xlifepp::GeomRefPyramid defines Reference Element geometric data on pyramidal element

	Numbering of vertices, edges and faces

	\verbatim
	                         Vertex numbers on edges | Vertex numbers on faces
	                         Edge:   v1 v2  equation | Face:  v1  v2  v3  v4  eq.
	        5                   1:    1  2           |    1:   1   2   3   4  x3=0
	       .| \                 2:    1  4           |    2:   1   2   5      x2=0
	      / |   \               3:    1  5           |    3:   2   3   5      x1+x3=1
	      . |     \             4:    2  3           |    4:   3   4   5      x2+x3=1
	     /  |       \           5:    2  5           |    5:   4   1   5      x1=0
	     .  |         \         6:    3  4           | Edge numbers on faces (+)
	    /   1-----------4       7:    3  5           | Face:  e1  e2  e3  e4
	    .  /           /        8:    4  5           |    1:   1   4   6  -2
	   / /           /                               |    2:   1   5  -3
	   /           /                                 |    3:   4   7  -5
	  2-----------3                                  |    4:   6   8  -7
	                                                 |    5:  -2   3  -8

	(+) only required for interpolation involving more than 3 nodes on a edge
  \endverbatim

	sideOfSideNumbering local numbering of vertices on edges
	sideNumbering local numbering of vertices on faces and of edges on faces
*/

#ifndef GEOM_REF_PYRAMID_HPP
#define GEOM_REF_PYRAMID_HPP

#include "config.h"
#include "../GeomRefElement.hpp"

namespace xlifepp
{

/*!
	\class GeomRefPyramid
	child class to GeomRefElement
 */
class GeomRefPyramid : public GeomRefElement
{
  public:
    //! default constructor
    GeomRefPyramid();
    //! destructor
    ~GeomRefPyramid() {}

    real_t measure(const dimen_t dim, const number_t sideNum = 0) const; //!< returns edge length or face area

    // /*!
    //  returns a quadrature rule built on edge sideNum (=1,2,...9) from a 1d quadrature formula
    //  or a quadrature rule built on face sideNum (=1,2,...5) from a triangle or quadrangle quadrature formula
    // */
    //   void sideQuadrature(const QuadratureRule&, QuadratureRule&, const number_t, const dimen_t = 2) const;

    //! returns tangent vector on an edge or a face
    void tangentVector(const std::vector<real_t>& jacobianMatrix, std::vector<std::vector<real_t> >& tgv, const number_t, const dimen_t) const;

    number_t vertexOppositeSide(const number_t s) const //! returns vertex opposite to edge or face number sideNum (=1,2, ...)
    { if(s<3) return 6-s; else return s-2;}

    number_t sideOppositeVertex(const number_t) const //! returns edge or face opposite to vertex number v
    { noSuchFunction("sideOppositeVertex");  return 0;}

    number_t edgeOppositeEdge(const number_t) const //! returns edge opposite to edge number e
    { noSuchFunction("edgeOppositeEdge");  return 0;}

    number_t sideWithVertices(const number_t, const number_t) const; //!< returns local number of edge bearing vertices with local numbers v1 and v2
    number_t sideWithVertices(const number_t, const number_t, const number_t, const number_t = 0) const; //!< returns local number of face bearing vertices with local numbers v1, v2, v3

    bool contains(std::vector<real_t>& p, real_t tol= theTolerance) const;  //!< test if a point belongs to current element

    //! node numbers defining first simplex of ref element
    std::vector<number_t> simplexNodes() const
    {std::vector<number_t> sn(4,0); sn[0]=1;sn[1]=2;sn[2]=4;sn[3]=5; return sn;}

  private:

    void sideOfSideNumbering(); //!< local numbers of vertices on edges for pyramids
    void sideNumbering(); //!< local numbers of vertices and edges on faces for pyramids

};

} // end of namespace xlifepp

#endif /* GEOM_REF_PYRAMID_HPP */
