/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
	\file RefPyramid.cpp
	\author N. Kielbasiewicz
	\since 23 oct 2012
	\date 5 nov 2012

	\brief Implementation of xlifepp::RefPyramid class members and related functions
 */

#include "RefPyramid.hpp"
#include "LagrangePyramid.hpp"
#include "utils.h"

namespace xlifepp
{

//! RefPyramid constructor for reference elements
RefPyramid::RefPyramid(const Interpolation* interp_p) : RefElement(_pyramid, interp_p)
{
  trace_p->push("RefPyramid::RefPyramid");
  trace_p->pop();
}

RefPyramid::~RefPyramid() {}

//! selectRefPyramid construction of a pyramidatic Reference Element by interpolation subtype and number
RefElement* selectRefPyramid(const Interpolation* interp_p)
{
  switch ( interp_p->type)
  {
    case _Lagrange:
      switch ( interp_p->subtype)
      {
        case _standard: return pyramidLagrangeStd(interp_p); break;
        default: interp_p->badSubType(_pyramid); break;
      }
    default: break;
  }
  
  // Throw error messages
  trace_p->push("selectRefPyramid");
  interp_p->badType(_pyramid);
  trace_p->pop();
  return nullptr;
}

//! pyramidLagrangeStd construction of a pyramidatic Lagrange Reference Element by interpolation number
RefElement* pyramidLagrangeStd(const Interpolation* interp_p)
{
  switch ( interp_p->numtype )
  {
    case _P0: return new LagrangeStdPyramid<_P0>(interp_p);
      break;
    case _P1: return new LagrangeStdPyramid<_P1>(interp_p);
      break;
    case _P2: return new LagrangeStdPyramid<_P2>(interp_p);
      break;
    default:
      trace_p->push("pyramidLagrangeStd");
      interp_p->badDegree(_pyramid);
      trace_p->pop();
      break;
  }
  return nullptr;
}

} // end of namespace xlifepp
