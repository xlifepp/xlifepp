/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RefPyramid.hpp
  \author N. Kielbasiewicz
  \since 23 oct 2012
  \date 5 nov 2012

  \brief Definition of the xlifepp::RefPyramid class

  xlifepp::RefPyramid defines Reference Element interpolation data on pyramidal elements
 */

#ifndef REF_PYRAMID_HPP
#define REF_PYRAMID_HPP

#include "config.h"
#include "../RefElement.hpp"
#include "../Interpolation.hpp"

namespace xlifepp
{

/*!
  \class RefPyramid

  Parent class: RefElement
  Child classes: LagrangePyramid
 */
class RefPyramid : public RefElement
{
  public:
    RefPyramid(const Interpolation* interp_p); //!< constructor by interpolation
    virtual ~RefPyramid(); //!< virtual destructor

    virtual void interpolationData() = 0; //!< returns interpolation data

    virtual void outputAsP1(std::ofstream& os, const int, const std::vector<number_t>&) const { noSuchFunction("outputAsP1"); }
    virtual void sideNumbering() = 0; //!< returns side numbering
    const splitvec_t& getO1splitting() const // override
       { return RefElement::getO1splitting(); }

}; // end of class RefPyramid

//! Extern class related functions and declarations
RefElement* pyramidLagrangeStd(const Interpolation*);

} // end of namespace xlifepp

#endif /* REF_PYRAMID_HPP */
