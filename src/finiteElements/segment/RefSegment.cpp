/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file RefSegment.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 15 dec 2002
  \date 6 aug 2012

  \brief Implementation of xlifepp::RefSegment class members and related functions
 */

#include "RefSegment.hpp"
#include "LagrangeSegment.hpp"
#include "HermiteSegment.hpp"

#include "../Interpolation.hpp"
#include "utils.h"

namespace xlifepp
{

//! segment construction of a 1D Reference Element by interpolation type and interpolation subtype
RefElement* selectRefSegment(const Interpolation* interp_p)
{
  switch (interp_p->type)
  {
    case _Lagrange:
      switch (interp_p->subtype)
      {
        case _standard: return new LagrangeStdSegment(interp_p); break;
        case _GaussLobattoPoints:
          switch (interp_p->numtype)
          {
            case 0: interp_p->badDegree(_segment); break;
            default: return new LagrangeGLSegment(interp_p); break;
          }
          break;
        default: interp_p->badSubType(_segment); break;
      }
    case _Hermite:
      switch (interp_p->subtype)
      {
        case standard:
          switch (interp_p->numtype)
          {
            case 3: return new HermiteStdSegment<P3>(interp_p); break;
            default: interp_p->badDegree(_segment); break;
          }
        default: interp_p->badSubType(_segment); break;
      }
      break;
    default: break;
  }
  
  // Throw error messages
  trace_p->push("selectRefSegment");
  interp_p->badType(_segment);
  trace_p->pop();
  return nullptr;
}

} // end of namespace xlifepp
