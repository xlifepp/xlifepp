/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file HermiteSegment.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 6 aug 2012

  \brief Definition of the xlifepp::HermiteSegment class

  xlifepp::HermiteSegment defines Hermite Reference Element interpolation data on a segment

  child class
  -----------
    - xlifepp::HermiteStdSegment Hermite standard Reference Element

  member functions
  ----------------
    - interpolationData   interpolation data
    - outputAsP1
    - pointCoordinates    point coordinates

  Local D.o.F numbering of Hermite P_3 1D element
  \verbatim
  o-->-----<--o
  3  4     2  1
       k=3
  \endverbatim
 */

#ifndef HERMITE_SEGMENT_HPP
#define HERMITE_SEGMENT_HPP

#include "config.h"
#include "RefSegment.hpp"

namespace xlifepp
{

class Interpolation;

/*!
  \class HermiteSegment
  Grand-Parent class: ReferenceElement
  Parent class: ReferenceSegment
  Child class: HermiteStdSegment
 */
class HermiteSegment : public RefSegment
{
  public:
    HermiteSegment(const Interpolation* interp_p); //!< constructor
    virtual ~HermiteSegment(); //!< destructor

    virtual void pointCoordinates() = 0; //!< defines coordinates of points

  protected:
    void interpolationData(); //!< defines interpolation data

    //! output of D.o.f's numbers on underlyin P1 D.o.f's
    void outputAsP1(std::ofstream& os, const int, const std::vector<number_t>& ranks) const { noSuchFunction("outputAsP1"); }

  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }
}; // end of class HermiteSegment

/*!
  \class HermiteStdSegment
  template class (order is template parameter)
 */
template<number_t Pk>
class HermiteStdSegment : public HermiteSegment
{
  public:
    HermiteStdSegment(const Interpolation* interp_p); //!< constructor by interpolation
    ~HermiteStdSegment() {} //!< destructor

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv=false) const; //!< shape functions
    void pointCoordinates(); //!< point coordinates (!)

}; // end of class HermiteStdSegment

} // end of namespace xlifepp

#endif /* HERMITE_SEGMENT_HPP */
