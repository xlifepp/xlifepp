/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomRefSegment.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 15 dec 2002
  \date 6 aug 2012

  \brief Implementation of xlifepp::GeomRefSegment class members and related functions
 */

#include "GeomRefSegment.hpp"

namespace xlifepp
{

//! GeomRefSegment constructor for 1D Geometric Reference Element
GeomRefSegment::GeomRefSegment()
  : GeomRefElement(_segment)
{
  vertices_[0] = 1.;
  vertices_[1] = 0.;

  sideNumbering();
}

//! sideNumbering defines Geometric Reference Element local numbering of vertices on edges
void GeomRefSegment::sideNumbering()
{
  sideVertexNumbers_[0].push_back(2);
  sideVertexNumbers_[1].push_back(1);
  sideShapeTypes_[0] = _point;
  sideShapeTypes_[1] = _point;
}

//! Returns element length (1.)
real_t GeomRefSegment::measure(const dimen_t d, const number_t sideNum) const
{
  real_t ms(0.);
  switch (d)
  {
    case 1: case 0: ms = 1.;
    default: break;
  }
  return ms;
}

//! test if a point belongs to current element
bool GeomRefSegment::contains(std::vector<real_t>& p, real_t tol) const
{
    real_t x=p[0];
    return (x >=-tol) && (x <= 1.+tol);
}

//! projection of a point onto ref element [0,1]
std::vector<real_t> GeomRefSegment::projection(const std::vector<real_t>&p , real_t& h) const
 {
     h=0.;
     real_t x=p[0];
     std::vector<real_t> q(1,x);
     if(x>1.) {h=x-1;q[0]=1.;}
     if(x<0.) {h=-x;q[0]=0.;}
     return q;
 }

} // end of namespace xlifepp
