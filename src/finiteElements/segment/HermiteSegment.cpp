/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file HermiteSegment.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 19 nov 2004
  \date 6 aug 2012

  \brief Implementation of xlifepp::HermiteSegment class members and related functions
 */

#include "HermiteSegment.hpp"
#include "../Interpolation.hpp"
#include "utils.h"

namespace xlifepp
{

//! HermiteSegment constructor for Hermite reference elements
HermiteSegment::HermiteSegment(const Interpolation* interp_p)
  : RefSegment(interp_p)
{
  name_ += "_Hermite";
  trace_p->push("HermiteSegment::HermiteSegment (" + name_ + ")");

  // build element interpolation data
  interpolationData();
  maxDegree = 3;

  trace_p->pop();
}

HermiteSegment::~HermiteSegment() {}

//! interpolationData defines Reference Element interpolation data
void HermiteSegment::interpolationData()
{

  trace_p->push("HermiteSegment::interpolationData");

  number_t kd = interpolation_p->numtype;

  if ( kd == 3 )   // Hermite segment element of degree 3
  {
    nbDofsOnVertices_ = nbDofs_ = 4;
    nbPts_ = 2;
    refDofs.reserve(nbDofs_);
    //hermiteRefDofs(1, 2, 1, 1);
    //create dof, order:  v(1) dx(1) v(0) dx(0)
    refDofs.push_back(new RefDof(this, true, _onVertex, 1, 1, 1, 0, 1, 1, 1, _noProjection,_dx, "derivative"));
    refDofs.push_back(new RefDof(this, true, _onVertex, 1, 2, 1, 0, 1, 1, 0, _noProjection,_id, "nodal value"));
    refDofs.push_back(new RefDof(this, true, _onVertex, 2, 1, 1, 0, 2, 1, 1, _noProjection,_dx, "derivative"));
    refDofs.push_back(new RefDof(this, true, _onVertex, 2, 2, 1, 0, 2, 1, 0, _noProjection,_id, "nodal value"));
  }

  // compute number of shape functions
  nbshfcts_ = shapeValueSize();

  trace_p->pop();
}

//! HermiteSegment constructor for Hermite standard reference elements
template<number_t Pk>
HermiteStdSegment<Pk>::HermiteStdSegment(const Interpolation* interp_p)
  : HermiteSegment(interp_p)
{
  name_ += "_" + tostring(interp_p->numtype);

  // local coordinates of Points supporting D.o.F
  pointCoordinates();
}

//! pointCoordinates defines Reference Element point coordinates
template<number_t Pk>
void HermiteStdSegment<Pk>::pointCoordinates()
{
  number_t kd = interpolation_p->numtype;
  std::vector<RefDof*>::iterator it_rd(refDofs.begin());

  switch (kd)
  {
    case 3: // Hermite segment of degree 3
      (*it_rd++)->coords(1.);
      (*it_rd++)->coords(1.);
      (*it_rd++)->coords(0.);
      (*it_rd)->coords(0.);
    default:
      break;
  }
}

//! computeShapeValues defines Hermite Reference Element shape functions
// dof order: dof, order:  v(1) dx(1) v(0) dx(0)
template<>
void HermiteStdSegment<P3>::computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv, const bool withDeriv, const bool with2Deriv) const
{
  real_t x = *it_pt, x2 = x * x, x3 = x * x2;
  std::vector<real_t>::iterator it_w=shv.w.begin();
  *it_w++ = 3 * x2 - 2 * x3;
  *it_w++ = x3 - x2;
  *it_w = 2 * x3 - 3 * x2 + 1.;
  *it_w++ = x3 - 2 * x2 + x;

  if (withDeriv)
  {
    std::vector< std::vector<real_t> >::iterator it_dw=shv.dw.begin();
    std::vector<real_t>::iterator it_dwi=(*it_dw).begin();
    *it_dwi++ = 6 * x - 6 * x2;
    *it_dwi++ = 3 * x2 - 2 * x;
    *it_dwi = 6 * x2 - 6 * x;
    *it_dwi++ = 3 * x2 - 4 * x + 1.;
  }
}

template class HermiteStdSegment<P3>;

} // end of namespace xlifepp
