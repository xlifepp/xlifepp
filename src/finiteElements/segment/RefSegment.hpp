/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RefSegment.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 17 dec 2002
  \date 6 aug 2012

  \brief Definition of the xlifepp::RefSegment class

  xlifepp::RefSegment defines reference element interpolation data on a segment

  External class related functions
    - selectRefSegment: select a 1D Reference Element by interpolation
    - segmentLagrangeStd: construction of a 1D Lagrange standard Reference Element
    - segmentLagrangeGaussLobatto: construction of a 1D Lagrange Gauss-Lobatto Reference Element
    - segmentHermiteStd: construction of a 1D Hermite standard Reference Element
*/

#ifndef REF_SEGMENT_HPP
#define REF_SEGMENT_HPP

#include "config.h"
#include "../RefElement.hpp"

namespace xlifepp
{

class Interpolation;

/*!
  \class RefSegment
  Parent class: RefElement
  Child classes: LagrangeSegment, HermiteSegment
 */
class RefSegment : public RefElement
{
  public:
    //! constructor by interpolation
    RefSegment(const Interpolation* interp_p) : RefElement(_segment, interp_p) {}
    //! destructor
    virtual ~RefSegment() {}

    virtual void interpolationData() = 0; //!< returns interpolation data
    virtual void outputAsP1(std::ofstream& os, const int, const std::vector<number_t>& ranks) const { noSuchFunction("outputAsP1"); }
    const splitvec_t& getO1splitting() const // override
       { return RefElement::getO1splitting(); }
}; // end of class RefSegment

} // end of namespace xlifepp

#endif /* REF_SEGMENT_HPP */
