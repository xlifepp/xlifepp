/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomRefSegment.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 17 dec 2002
  \date 6 aug 2012

  \brief Definition of the xlifepp::GeomRefSegment class

  xlifepp::GeomRefSegment defines Reference Element geometric data on a segment
 */

#ifndef GEOM_REF_SEGMENT_HPP
#define GEOM_REF_SEGMENT_HPP

#include "config.h"
#include "../GeomRefElement.hpp"
//#include "../QuadratureRule.h++"

namespace xlifepp
{

/*!
  \class GeomRefSegment
 */
class GeomRefSegment : public GeomRefElement
{
  public:
    //! default constructor
    GeomRefSegment();
    //! destructor
    ~GeomRefSegment() {}

    real_t measure(const dimen_t dim, const number_t sideNum = 0) const; //!< returns 1.

    // void sideQuadrature(const QuadratureRule&, QuadratureRule&, const number_t, const dimen_t = 0) const { noSuchFunction("sideQuadrature"); }
    //! returns tangent vector on a side
    void tangentVector(const std::vector<real_t>& jacobianMatrix, std::vector<std::vector<real_t> >& tgv, const number_t, const dimen_t) const { noSuchFunction("tangentVector"); }
    //! returns vertex opposite to vertex number vNum (=1,2)
    number_t vertexOppositeSide(const number_t sideNum) const { return 1 + sideNum % 2; }
    //! returns vertex to vertex number vNum (=1,2)
    number_t sideOppositeVertex(const number_t vNum) const { return 1 + vNum % 2; }
    //! return edge oppsite to edge e
    number_t edgeOppositeEdge(const number_t) const { noSuchFunction("edgeOppositeEdge"); return 0; }
    //! returns side with vertices v1 and v2
    number_t sideWithVertices(const number_t, const number_t) const { noSuchFunction("sideWithVertices"); return 0;}
    //! returns side with vertices v1 and v2
    number_t sideWithVertices(const number_t, const number_t, const number_t, const number_t = 0) const { noSuchFunction("sideWithVertices"); return 0;}

    bool contains(std::vector<real_t>& p, real_t tol= theTolerance) const;              //!< test if a point belongs to current element
    virtual std::vector<real_t> projection(const std::vector<real_t>&, real_t&) const;  //!< projection of a point onto ref element
    //! node numbers defining first simplex of ref element
    std::vector<number_t> simplexNodes() const
    {std::vector<number_t> sn(2,0); sn[0]=2;sn[1]=1; return sn;}

  private:
    void sideNumbering(); //!< local numbers of vertices on sides
}; // end of class GeomRefSegment

} // end of namespace xlifepp

#endif /* GEOM_REF_SEGMENT_HPP */
