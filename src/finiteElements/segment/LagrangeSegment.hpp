/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LagrangeSegment.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 11 jan 2003
  \date 6 aug 2012

  \brief Definition of the xlifepp::LagrangeSegment class

  xlifepp::LagrangeSegment defines Lagrange Reference Element interpolation data on a segment

  child classes
    - xlifepp::LagrangeStdSegment
      Lagrange standard Reference Element (interpolation at equidistant nodes)
    - xlifepp::LagrangeGLSegment
      Lagrange Gauss-Lobatto Reference Element (interpolation at Gauss-Lobatto points)

  member functions
    - interpolationData   interpolation data
    - outputAsP1
    - pointCoordinates    point coordinates

  Local node numbering of Lagrange Pk 1D elements, k=0,1, 2,.., 5

  \verbatim
  -o-  o---o  o---o---o  o---o---o---o  o---o---o---o---o  o---o---o---o---o---o
   1   2   1  2   3   1  2   4   3   1  2   5   4   3   1  2   6   5   4   3   1
  k=0   k=1      k=2          k=3              k=4                  k=5
  \endverbatim
 */

#ifndef LAGRANGE_SEGMENT_HPP
#define LAGRANGE_SEGMENT_HPP

#include "config.h"
#include "RefSegment.hpp"

namespace xlifepp
{

class Interpolation;

/*!
  \class LagrangeSegment
  Grand-Parent class: ReferenceElement
  Parent class:       ReferenceSegment
  Child classes:      LagrangeStdSegment LagrangeGLSegment
 */
class LagrangeSegment : public RefSegment
{
  public:
    LagrangeSegment(const Interpolation* interp_p); //!< constructor by interpolation
    virtual ~LagrangeSegment(); //!< destructor
    virtual void pointCoordinates() = 0; //!< compute point coordinates, must exist il all subclasses
  protected:
    void interpolationData(); //!< build necessary interpolation data
    void sideNumbering(); //!< numbering of side D.O.F's in standard Lagrange elements
    std::vector<std::vector<number_t> > splitP1() const;  //!< return nodes numbers of first order segment elements when splitting current element
    splitvec_t splitO1() const;  //!< return nodes numbers of first order segment elements when splitting current element
    void outputAsP1(std::ofstream& os, const int, const std::vector<number_t>& ranks) const; //!< output of Lagrange D.o.f's numbers on underlyin P1 D.o.f's
  private:
    number_t nbshfcts_;

    number_t nbShapeFcts() const { return nbshfcts_; }

}; // end of class LagrangeSegment

/*!
  \class LagrangeStdSegment
  child class of LagrangeHexahedron with standard interpolation

 */
class LagrangeStdSegment : public LagrangeSegment
{
  public:
    LagrangeStdSegment(const Interpolation* interp_p); //!< constructor by interpolation
    ~LagrangeStdSegment() {} //!< destructor

    void pointCoordinates(); //!< point coordinates (!) (equidistant on [0,1])
    void computeShapeFunctions(); //!< compute polynomials representation
    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv=false) const; //!< compute shape functions at given point
    const splitvec_t& getO1splitting() const // override
     { return splitO1Scheme; }
  private:
    // storage of order 1 splitting scheme (intended to graphical softwares usage)
    splitvec_t splitO1Scheme;
}; // end of class LagrangeStdSegment

/*!
  \class LagrangeGLSegment
  child class of LagrangeHexahedron with Gauss-Lobatto interpolation
 */
class LagrangeGLSegment : public LagrangeSegment
{
  public:
    LagrangeGLSegment(const Interpolation* interp_p); //!< constructor by interpolation
    ~LagrangeGLSegment() {} //!< destructor

    void pointCoordinates(); //!< compute point coordinates (!) (from Gauss-Lobatto points on [-1,+1])

    void computeShapeValues(std::vector<real_t>::const_iterator it_pt, ShapeValues& shv,
                            const bool withDeriv = true, const bool with2Deriv=false) const; //!< compute  shape functions at given point
    const splitvec_t& getO1splitting() const // override
     { return splitO1Scheme; }
  private:
    // storage of order 1 splitting scheme (intended to graphical softwares usage)
    splitvec_t splitO1Scheme;
}; // end of class LagrangeGLSegment

} // end of namespace xlifepp

#endif /* LAGRANGE_SEGMENT_HPP */
