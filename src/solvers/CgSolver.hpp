/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file CgSolver.hpp
  \authors C. Chambeyron, D. Martin, Manh Ha NGUYEN
  \since 24 Nov 2004
  \date  01 Oct 2012

  \brief Definition of the xlifepp::CgSolver class

  Class xlifepp::CgSolver implements the Conjugate Gradient method for solving
  linear systems, with or without a preconditioner.
  CG is only suitable for symmetric real or complex linear systems.

  Base class: xlifepp::IterativeSolver

  For elementary explanation" about Conjugate Gradient method,
  see http://mathworld.wolfram.com/ConjugateGradientMethod.html
*/

#ifndef CG_SOLVER_HPP
#define CG_SOLVER_HPP

#include "IterativeSolver.hpp"
#include "Preconditioner.hpp"

namespace xlifepp
{
/*!
    \class CgSolver
    Implement Conjugate Gradient algorithm
*/
class CgSolver : public IterativeSolver
{
  public:
    //! Default constructor
    CgSolver(): IterativeSolver(_cg) {}

    //! Full constructor
    CgSolver(real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = theVerboseLevel)
    : IterativeSolver(_cg, maxOfIt, eps, vb) {}

    //@{
    //! contructors with key-value system
    CgSolver(const Parameter& p1) : IterativeSolver(_cg)
    {
      std::vector<Parameter> ps(1, p1);
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_cg;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    CgSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_cg)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_cg;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    CgSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_cg)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_cg;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    //@}

    ~CgSolver() {}

    //! Template Solvers without a preconditioner without argument "solType
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0)
    {
      if ( matA.valueType() == _real && vecB.valueType() == _real )
      {
        return (*this)( matA,  vecB,  vecX0,  _real );
      }
      else
      {
        return (*this)( matA,  vecB,  vecX0,  _complex );
      }
    }

    //! Template Solvers without a preconditioner
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, ValueType solType)
    {
      trace_p->push(name_ + "Solver");
      resetSolver();
      preconditioned_ = false;
      // Default value of maximum of iterations (when not set by user)
      maxOfIterations_ = maximumOfIterations(vecX0.size());
      // Print out size of unknow vector
      printHeader(vecX0.size());
      // Define the return value
      VecX x(vecX0);
      // Define initial residual vector according to Type
      VecX r;
      multMatrixVector(matA, x, r);
      r*=-1.; r+=vecB;
      //run algorithm
      if (_real == solType)
      {
        algorithm<real_t>(matA, vecB, x, r);
      }
      else
      {
        algorithm<complex_t>(matA, vecB, x, r);
      }
      //output
      printOutput(vecX0.size());
      printResult();
      trace_p->pop();
      return x;
    }

    //! Template Solvers without a preconditionner and X0 = vecB
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, ValueType solType)
    {
      return (*this)(matA, vecB, vecB, solType);
    }

    //! Template Solvers with a precondition without argument "solType
    template<class Mat, class VecB, class VecX, class VecPC>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, VecPC& pc)
    {
      if ( (_real == matA.valueType()) &&  (_real == vecB.valueType() ) )
      {
        return (*this)( matA,  vecB,  vecX0, pc, _real );
      }
      else {
        return (*this)( matA,  vecB,  vecX0, pc, _complex );
      }
    }

    //!  Template Solvers with a preconditionner
    template<class Mat, class VecB, class VecX, class VecPC>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, VecPC& pc, ValueType solType)
    {
      trace_p->push(name_ + "Solver");
      resetSolver();
      // Default value of maximum of iterations (when not set by user)
      maxOfIterations_ = maximumOfIterations(vecX0.size());
      printHeader(vecX0.size(), pc.name());
      if ( pc.valueType() == _complex ) solType = _complex;
      VecX x(vecX0);
      VecX r;
      multMatrixVector(matA, x, r);
      r*=-1.; r += vecB;
      //run algorithm
      if ( _real == solType )
      {
        algorithm<real_t>(matA, vecB , x , r, pc);
      }
      else
      {
        algorithm<complex_t>(matA, vecB , x , r, pc);
      }
      //output
      printOutput(vecX0.size());
      printResult();
      trace_p->pop();
      return x;
    }

  private:
    /*!
         Solve matA.X=B using CG method without preconditionner.
         Initial residue is an argument used to define the type (K: real or complex)
         of auxiliary vectors as matrices matA, B and vector vecX0 might not be of same type
    */
    template<class K, class Mat, class VecB, class VecX>
    void algorithm(Mat& matA, VecB& vecB, VecX& vecX, VecX& vecR)
    {
      trace_p->push(name_ + "algorithm");
      K alpha, beta, theta;
      //initialization
      real_t normOfB = vecB.norm2();
      if ( normOfB < theBreakdownThreshold ) { normOfB = 1.; }
      residue_ = vecR.norm2() / normOfB;
      VecX p(vecR);
      assign(theta, dotRC(vecR, vecR));  // theta_{0}=r_{0}.r_{0}
      VecX ap;
      // Iterative loop
      while ( ( numberOfIterations_ < maxOfIterations_ ) && (residue_  > epsilon_ ) )
      {
        multMatrixVector(matA, p, ap);         // ap = matA * p
        assign(alpha, theta / dotRC(ap, p));  // alpha_k: alpha{k} = theta_{k}/(Ap_{k},p_{k})
        VecX tmp(p);
        tmp *= alpha;
        vecX += tmp;    // update solution vector: X_{k+1} = X_{k} + alpha_{k} p_{k}
        ap *= (-alpha);
        vecR+=ap;       // residual vector: r_{k+1} = r_{k} - alpha_{k} Apap_{k}
        residue_ = (vecR.norm2() / normOfB );
        numberOfIterations_++;
        printIteration();
        if ( residue_  < epsilon_  ) { break; }  // Convergence when residual norm is small enough
        beta = 1. / theta;
        assign(theta, dotRC(vecR, vecR));   // theta_{k+1} = (r_{k+1}, r_{k+1})
        beta *= theta;                      // beta_{k+1} = theta_{k+1}/theta_{k}
        p*=beta;
        p += vecR;     // New direction vector  p_{k+1} = r_{k+1} + beta_{k+1} * p_{k}
      }
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
      trace_p->pop();
    }

    /*!
       Solve matA.X=B using CG method with a preconditioner.
           Typename K (K: real or complex) is the type of the initial residual vector
           and thus of auxiliary vectors as matrices matA, P, B
           and vector X0 might not share the same type
    */
    template<typename K, class Mat, class VecB, class VecX, class Prec>
    void algorithm(Mat& matA, VecB& vecB, VecX& vecX, VecX& vecR, Prec& pc)
    {
      trace_p->push(name_ + "Algorithm");
      real_t normOfB = vecB.norm2();
      if ( normOfB < theBreakdownThreshold ) { normOfB = 1.; }
      residue_ = vecR.norm2() / normOfB;
      K alpha, beta, theta;
      VecX pmr(vecX); pc.solve(vecR,pmr);
      VecX p(pmr);
      VecX ap;
      assign(theta, dotRC(vecR, pmr)); // theta_{0}=r_{0}.P^{-1}r_{0}
      // Iterative loop
      while ( ( numberOfIterations_ < maxOfIterations_)  && (residue_ > epsilon_ ) )
      {
        multMatrixVector(matA, p, ap);         // ap = matA * p
        assign(alpha, theta / dotRC(ap, p));   // alpha{k} = theta_{k}/(Ap_{k},p_{k})
        VecX tmp(p);
        tmp *= alpha;
        vecX += tmp;    // Update solution vector: X_{k+1} = X_{k} + alpha_{k} p_{k}
        ap*=-alpha;
        vecR+=ap;       // Residual vector: r_{k+1} = r_{k} - alpha_{k} Ap_{k}
        residue_ = vecR.norm2() / normOfB;
        numberOfIterations_++;
        printIteration();
        if ( residue_  < epsilon_  ) { break; }  // Convergence when residual norm is small enough
        // Next direction vector
        pc.solve(vecR, pmr);  // Compute P^{-1}r1_{k+1}
        if ( std::abs(theta) < theBreakdownThreshold ) { breakdown(std::abs(theta), "theta"); }
        beta = 1. / theta;
        assign(theta, dotRC(vecR, pmr));  // theta_{k+1} = (P^{-1}r_{k+1}, r_{k+1});
        beta *= theta;                    // beta_{k+1} = theta_{k+1}/theta_{k}
        p*=beta;
        p += pmr;       // p_{k+1} = P^{-1}r_{k+1} + beta_{k+1} * p_{k}
      }
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
      trace_p->pop();
    }
}; // end of class CgSolver

}  // namespace xlifepp

#endif /* CG_SOLVER_HPP */
