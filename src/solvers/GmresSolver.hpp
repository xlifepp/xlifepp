/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GmresSolver.hpp
  \authors C. Chambeyron, D. Martin, Manh Ha NGUYEN
  \since 24 nov 2004
  \date  16 Oct 2012

  \brief Definition of the xlifepp::GmresSolver class

  Class xlifepp::GmresSolver the Generalized Minimal Residual method for solving
  linear systems, with or without preconditioner.
  GMRes is suitable for non symmetric systems

  Base class: xlifepp::IterativeSolver

  For elementary explanation about Generalized Minimal Residual method,
  see http://mathworld.wolfram.com/GeneralizedMinimalResidualMethod.html
*/

#ifndef GMRES_SOLVER_HPP
#define GMRES_SOLVER_HPP

#include "IterativeSolver.hpp"
#include "Preconditioner.hpp"

namespace xlifepp
{
/*!
    \class GmresSolver
     Implement Generalized Minimal Residual algorithm
*/
class GmresSolver : public IterativeSolver
{
  private:
    number_t krylovDim_;  //!< Dimension of Krylov space

  public:
    //! Constructor with Krylov dimension
    GmresSolver(number_t kd=defaultKrylovDimension)
      : IterativeSolver(_gmres, defaultMaxIterations, theDefaultConvergenceThreshold),
        krylovDim_(kd) {}

    //! Full constructor
    GmresSolver(number_t kd, real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = theVerboseLevel)
    : IterativeSolver(_gmres, maxOfIt, eps, vb), krylovDim_(kd) {}

    //@{
    //! contructors with key-value system
    GmresSolver(const Parameter& p1) : IterativeSolver(_gmres)
    {
      std::vector<Parameter> ps(1, p1);
      real_t omega=1.;
      IterativeSolverType ist=_gmres;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim_, verboseLevel_, name_, ist);
    }
    GmresSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_gmres)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      real_t omega=1.;
      IterativeSolverType ist=_gmres;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim_, verboseLevel_, name_, ist);
    }
    GmresSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_gmres)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      real_t omega=1.;
      IterativeSolverType ist=_gmres;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim_, verboseLevel_, name_, ist);
    }
    GmresSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4) : IterativeSolver(_gmres)
    {
      std::vector<Parameter> ps(4);
      ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4;
      real_t omega=1.;
      IterativeSolverType ist=_gmres;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim_, verboseLevel_, name_, ist);
    }
    //@}

    ~GmresSolver() {} //!< destructor

    //@{
    //! print utility
    void print(std::ostream& out) const
    {
      out << words("Iterative solver") << " (" << name_ << ") " << words("of") << " " << words("type") << " " << words("iterative solver", type_);
      out << ", " << words("tolerance") << ": " << epsilon_;
      out << ", " << words("maximum number of iterations") << ": " << maxOfIterations_;
      out << ", " << words("krylov dimension") << ": " << krylovDim_;
      out << " " << words("and") << " " << words("verbose level") << ": " << verboseLevel_;
    }
    friend std::ostream& operator<<(std::ostream& out, const GmresSolver& gs)
    {
      gs.print(out);
      return out;
    }
    //@}

    /*!
        Return the maximum iteration as a function of linear system size
        \param nbRows number of row of vector unknown X
        \return Maximum number of iteration
    */
    number_t maximumOfIterations(const size_t nbRows)
    {
      return std::max(krylovDim_,IterativeSolver::maximumOfIterations(nbRows));
    }

    //! defines a default value for the Krylov dimension
    number_t krylovDimension(const size_t nbRows)
    {
      number_t kd=krylovDim_;
      if (kd == 0) { kd = nbRows; }
      else { kd = std::min(kd, nbRows); } // Krylov space dimension must be less than system dimension
      return kd;
    }

    //! Template Solvers without a precondition without argument "solType
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0)
    {
      if ( matA.valueType() == _real && vecB.valueType() == _real )
      {
        return (*this)( matA,  vecB,  vecX0,  _real );
      }
      else
      {
        return (*this)( matA,  vecB,  vecX0,  _complex );
      }
    }

    //! Template Solvers without a preconditioner
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, ValueType solType)
    {
      trace_p->push(name_ + "Solver");
      resetSolver();
      preconditioned_ = false;
      // Krylov space dimension must be less than system dimension
      krylovDim_=krylovDimension(vecX0.size());
      // Default value of maximum of iterations when not defined by user
      maxOfIterations_ = maximumOfIterations(vecX0.size());
      // Print out size of unknow vector
      printHeader(vecX0.size(), krylovDim_);
      // Define the return value
      VecX x(vecX0);
      // Define initial residual vector according to Type
      VecX r;
      multMatrixVector(matA, x, r);
      r*=-1.;
      r+=vecB; // r = vecB - r;
      //run algorithm
      if (_real == solType)
      {
        algorithm<real_t>(matA, vecB, x, r);
      }
      else
      {
        algorithm<complex_t>(matA, vecB, x, r);
      }
      //output
      printOutput(vecX0.size());
      printResult();
      trace_p->pop();
      return x;
    }

    //! Template Solvers without a preconditionner and X0 = vecB
    template <class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, ValueType solType)
    {
      return (*this)(matA, vecB, vecB, solType);
    }

    //! Template Solvers with a precondition without argument "solType
    template<class Mat, class VecB, class VecX, class VecPC>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, VecPC& pc)
    {
      if ( (_real == matA.valueType()) &&  (_real == vecB.valueType() ) )
      {
        return (*this)( matA,  vecB,  vecX0, pc, _real );
      }
      else
      {
        return (*this)( matA,  vecB,  vecX0, pc, _complex );
      }
    }

    //! Template Solvers with a preconditionner
    template<class Mat, class VecB, class VecX, class VecPC>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, VecPC& pc, ValueType solType)
    {
      trace_p->push(name_ + "Solver");
      resetSolver();
      // Krylov space dimension must be less than system dimension
      krylovDim_=krylovDimension(vecX0.size());
      // Default value of maximum of iterations when not defined by user
      maxOfIterations_ = maximumOfIterations(vecX0.size());
      if ( preconditioned_ )
      {
        printHeader(vecX0.size(), krylovDim_, pc.name());
        if ( pc.valueType() == _complex ) { solType = _complex; }
      }
      else { printHeader(vecX0.size(), krylovDim_); }
      //compute residue
      VecX x(vecX0);
      VecX r;
      multMatrixVector(matA, x, r);
      r*=-1.;
      r+=vecB; // r = vecB - r;
      //run algorithm
      if ( _real == solType )
      {
        algorithm<real_t>(matA, vecB , x , r, pc);
      }
      else
      {
        algorithm<complex_t>(matA, vecB , x , r, pc);
      }
      //output
      printOutput(vecX0.size());
      printResult();
      trace_p->pop();
      return x;
    }

  private:
    /*!
       Solve matA.vecX=vecB using GMRES method with initial guess vecX0 without a preconditionner.
       Typename K (K: real or complex) is the type of the initial residual Vector
           and thus of auxiliary vectors as matrices matA, vecB
           and Vector vecX0 might not share the same type
    */
    template<class K, class Mat, class VecB, class VecX>
    void algorithm(Mat& matA, VecB& vecB, VecX& vecX, VecX& vecR)
    {
      trace_p->push(name_ + "Algorithm");
      real_t normOfB = vecB.norm2();
      if ( normOfB < theBreakdownThreshold ) { normOfB = 1.; }
      // Givens rotations entries
      Vector<K> c(krylovDim_), s(krylovDim_);
      // Minimizing Vector in Krylov space
      Vector<K> rs(krylovDim_ + 1);
      // Hess is an Hessenberg matrix represented by a K-type Vector of vectors
      Vector< Vector<K> > hess(krylovDim_);
      // KrylovDim_+1 vectors of size vecX.size() and same type as vecR = vecB-matA*vecX0
      Vector<VecX> v(krylovDim_ + 1);
      v[0]=vecR;
      VecX ax;
      real_t rho = vecR.norm2();
      residue_ = rho / normOfB;

      while ( (residue_ > epsilon_) && (numberOfIterations_ < maxOfIterations_) )
      {
        real_t rhoTmp = normOfB;
        rs[0] = rho;
        v[0] *= (1. / rho);
        number_t i=0;
        while ( (i < krylovDim_) && (numberOfIterations_ < maxOfIterations_) && (residue_ > epsilon_) )
        {
          // Krylov orthogonal basis {V_i}
          // Hessenberg matrix H_{ki} = (matA*V_i, V_k)
          // Compute new Krylov Vector: v(i+1)=P^{-1}*matA*v(i)
          hess[i].resize(i+2);
          multMatrixVector(matA, v[i], v[i + 1]);
          arnoldiOrthogonalization<K>(i, v, hess[i]);
          // QR factorization of Hessenberg matrix
          rhoTmp = qrFactorization<K>(i, hess[i], c, s, rs);
          i++;
          residue_=rhoTmp/normOfB;
          numberOfIterations_++;
          printIteration();
        }
        // Least squares solution (solve upper triangular system) and update solution
        for ( number_t l = 0; l < i ; ++l )
        {
          number_t k = i - 1 - l;
          for ( number_t j = k + 1; j < i; j++ ) { rs[k] -= hess[j][k] * rs[j]; }
          rs[k] /= hess[k][k];
          v[k] *= rs[k];
          vecX+=v[k];
        }
        // update residue v[0] = vecB - matA*vecX;
        multMatrixVector(matA, vecX, ax);
        ax*=-1.; ax+=vecB;
        v[0]=ax;
        rho = ax.norm2();
        residue_ = rho / normOfB;
      }
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
      trace_p->pop();
    }

    /*!
       Solve matA.vecX=vecB using GMRES method with initial guess vecX0 and optional preconditionner.
           Typename K (K: real or complex) is the type of the initial residual Vector
           and thus of auxiliary vectors as matrices matA, P, vecB
           and Vector vecX0 might not share the same type
    */
    template<typename K, class Mat, class VecB, class VecX, class Prec>
    void algorithm(Mat& matA, VecB& vecB, VecX& vecX, VecX& vecR, Prec& pc)
    {
      trace_p->push(name_ + "Algorithm");
      real_t normOfB = vecB.norm2();
      if ( normOfB < theBreakdownThreshold ) { normOfB = 1.; }
      // Givens rotations entries
      Vector<K> c(krylovDim_), s(krylovDim_); // Minimizing Vector in Krylov space
      Vector<K> rs(krylovDim_ + 1); // hess is an Hessenberg matrix represented by a K-type Vector of vectors
      Vector< Vector<K> > hess(krylovDim_); // krylovDim_+1 vectors of size vecX.size() and same type as vecR = vecB-matA*vecX0
      VecB temp1(vecR),ax(vecR);
      Vector<VecX> v(krylovDim_ + 1, vecR);
      real_t rho= vecR.norm2(), rhoTmp;
      residue_ = rho / normOfB;
      while ( (residue_ > epsilon_) && (numberOfIterations_ < maxOfIterations_ ) )
      {
        pc.solve(v[0],ax);
        rho = ax.norm2(),
        rs[0] = rho;
        v[0]=ax;
        v[0] *= (1. / rho);
        number_t i=0;
        while ( (i < krylovDim_) && (numberOfIterations_ < maxOfIterations_) && (residue_ > epsilon_) )
        {
          // Krylov orthogonal basis {V_i}
          // Hessenberg matrix H_{ki} = (matA*V_i, V_k)
          // Compute new Krylov Vector: v(i+1)=P^{-1}*matA*v(i)
          hess[i].resize(i+2);
          multMatrixVector(matA, v[i], temp1);
          pc.solve(temp1,v[i+1]);
          arnoldiOrthogonalization<K>(i, v, hess[i]);
          // QR factorization of Hessenberg matrix
          rhoTmp = qrFactorization<K>(i, hess[i], c, s, rs);
          residue_=rhoTmp/normOfB;
          i++; numberOfIterations_++;
          printIteration();
        }
        // Least squares solution (solve upper triangular system) and update of solution Vector
        for ( number_t l = 0; l < i ; ++l )
        {
          number_t k = i - 1 - l ;
          for ( number_t j = k + 1; j < i; j++ ) { rs[k] -= hess[j][k] * rs[j]; }
          rs[k] /= hess[k][k];
          v[k] *= rs[k];
          vecX+= v[k];
        }
        // update residue v[0] = vecB - matA*vecX;
        multMatrixVector(matA, vecX, ax);
        ax*=-1.; ax+=vecB;
        rho = ax.norm2();
        residue_ = rho / normOfB;
        v[0]=ax;
      }
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
      trace_p->pop();
    }

    //! Compute Hessenberg matrix using Arnoldi's orthogonalization
    template<typename K, class VecX>
    void arnoldiOrthogonalization(const number_t i, Vector<VecX>& v, Vector<K>& hess)
    {
      number_t ip1 = i + 1;
      K hik;
      VecX tmp;
      typename Vector<K>::iterator it_hess=hess.begin();
      // Arnoldi's method, inner product coefficients are stored in upper Hessenberg matrix
      for ( number_t k = 0; k <= i; k++)
      {
    	tmp = v[k];
        assign(hik, hermitianProduct(v[ip1], v[k]));      //orthonormalisation using hermitian product
        *it_hess++=hik;
        tmp*=-hik;
        v[ip1]+= tmp;
      }
      hik = v[ip1].norm2();
      *it_hess=hik;
      if (std::abs(hik) > epsilon_) v[ip1] /= hik;
    }

    /*! Qr Factorization using complex Givens rotation

           |   c        -s    | |a| =  |       ca-sb       |
           | conj(s)  conj(c) | |b| =  | conj(s)a+conj(c)b |

           beta= sqrt(|a|^2 + |b|^2)   c = conj(a)/beta   s = -conj(b)/beta
    */
    template<typename K>
    real_t qrFactorization(const number_t i, Vector<K>& hess,
                           Vector<K>& c, Vector<K>& s, Vector<K>& rs)
    {
      real_t rhoTmp;
      K hik;
      // Previous Givens rotations acting on subdiagonal entries of current row
      typename Vector<K>::iterator it_h = hess.begin();
      typename Vector<K>::iterator cIt = c.begin();
      typename Vector<K>::iterator sIt = s.begin();
      if ( hess.size() > 2 )
      {
        for ( it_h = hess.begin(); it_h != hess.end() - 2; it_h++)
        {
          hik = *it_h;
          *it_h       = hik * *cIt - *(it_h + 1) * *sIt;
          *(it_h + 1) = hik * conj(*sIt) + *(it_h + 1) * conj(*cIt);
          sIt++; cIt++;
        }
      }
      // New Givens rotation acting on last 2 entries on current row
      K beta  = std::sqrt( *it_h * conj(*it_h) + * (it_h + 1) * conj(*(it_h + 1)) );
      *cIt = conj(*it_h )/ beta;
      *sIt = -conj(*(it_h + 1)) / beta;
      *it_h = beta;  // hess[i][i] = beta & hess[i+1][i] = 0.;
      rs[i + 1] = *sIt * rs[i];
      rs[i] *= *cIt;
      rhoTmp = std::abs(rs[i + 1]);
      return rhoTmp;
    }
}; // end of class GmresSolver

} // namespace xlifepp

#endif /* GMRES_SOLVER_HPP */
