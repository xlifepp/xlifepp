/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file IterativeSolver.cpp
  \authors C. Chambeyron, D. Martin
  \since 24 nov 2004
  \date  24 jan 2011

  \brief Implementation of xlifepp::IterativeSolver class members and related functions
*/

#include "IterativeSolver.hpp"
#include "config.h"
#include "utils.h"
#include <iostream>

namespace xlifepp
{

//! Default constructor by name
IterativeSolver::IterativeSolver(const string_t& name, number_t vb)
  : name_(name), type_(_noIterativeSolver), maxOfIterations_(defaultMaxIterations), numberOfIterations_(0),
    epsilon_(theDefaultConvergenceThreshold), residue_(1.), preconditioned_(true),
    verboseLevel_(vb), logged_(Trace::isLogged())
{
  // residues_.resize(3, 1.);
}

//! Default constructor by type
IterativeSolver::IterativeSolver(IterativeSolverType ist, number_t vb)
  : name_(words("iterative solver", ist)), type_(ist), maxOfIterations_(defaultMaxIterations), numberOfIterations_(0),
    epsilon_(theDefaultConvergenceThreshold), residue_(1.), preconditioned_(true),
    verboseLevel_(vb), logged_(Trace::isLogged())
{
  // residues_.resize(3, 1.);
}

//! Full constructor without type
IterativeSolver::IterativeSolver(const string_t& name, number_t maxOfIt, real_t eps, number_t vb, bool prec)
  : name_(name), type_(_noIterativeSolver), maxOfIterations_(maxOfIt), numberOfIterations_(0),
    epsilon_(eps), residue_(1.), preconditioned_(prec),
    verboseLevel_(vb), logged_(Trace::isLogged())
{
  // residues_.resize(3, 1.);
}

//! Full constructor without name
IterativeSolver::IterativeSolver(IterativeSolverType ist, number_t maxOfIt, real_t eps, number_t vb, bool prec)
  : name_(words("iterative solver", ist)), type_(ist), maxOfIterations_(maxOfIt), numberOfIterations_(0),
    epsilon_(eps), residue_(1.), preconditioned_(prec),
    verboseLevel_(vb), logged_(Trace::isLogged())
{
  // residues_.resize(3, 1.);
}

//! Full constructor
IterativeSolver::IterativeSolver(const string_t& name, IterativeSolverType ist, number_t maxOfIt, real_t eps, number_t vb, bool prec)
  : name_(name), type_(ist), maxOfIterations_(maxOfIt), numberOfIterations_(0),
    epsilon_(eps), residue_(1.), preconditioned_(prec),
    verboseLevel_(vb), logged_(Trace::isLogged())
{
  // residues_.resize(3, 1.);
}

IterativeSolver::~IterativeSolver() {}

/*!
     Return the maximum iteration as a function of linear system size
     \param nbRows number of row of vector unknown X
     \return Maximum number of iteration
 */
number_t IterativeSolver::maximumOfIterations(const size_t nbRows)
{
  number_t maxOfIt = maxOfIterations_;
  if ( maxOfIt == defaultMaxIterations )
       maxOfIt=nbRows;
  return maxOfIt;
}

/*!
    Reset Solver to its initial status
 */
void IterativeSolver::resetSolver()
{
  numberOfIterations_ = 0;
  // residues_.clear();
  // residues_.resize(3, 0.);
}

//==============================================================================
//
//     Messages and convergence error handlers
//
//==============================================================================
/*!
  Print out the header of this solver with the number of Row
*/
void IterativeSolver::printHeader(const size_t nbRows) const
{
  if (verboseLevel_>1) info("solver_header", name_, nbRows, maxOfIterations_, epsilon_);
}

/*!
  Print out the header of this solver with the number of row and PC name
*/
void IterativeSolver::printHeader(const size_t nbRows, const string_t& PCname) const
{
  if (verboseLevel_>1) info("solverPC_header", name_, PCname, nbRows, maxOfIterations_, epsilon_);
}

/*!
  Print out the header of this solver with the number of row and omega
*/
void IterativeSolver::printHeader(const size_t nbRows, const real_t omega) const
{
  if (verboseLevel_>1) info("solver_Omega", name_, nbRows, maxOfIterations_, epsilon_, omega);
}

/*!
  Print out the header of this solver with the number of row and krylov dimmension
*/
void IterativeSolver::printHeader(const size_t nbRows, const number_t kd) const
{
  if (verboseLevel_>1) info("solver_Krylov", name_, nbRows, maxOfIterations_, epsilon_, kd);
}

/*!
  Print out the header of this solver with the number of row, krylov dimension and PC name
*/
void IterativeSolver::printHeader(const size_t nbRows, const number_t kd, const string_t& PCname) const
{
  if (verboseLevel_>1) info("solverPC_Krylov", name_, PCname, nbRows, maxOfIterations_, epsilon_, kd);
}

//! Print out the final result of the solver
void IterativeSolver::printOutput(const size_t nbRows) const
{
  if ( logged_) { Trace::logOn(); }
  if (verboseLevel_>1)
  {
    info("solver_tail", name_, nbRows, numberOfIterations_, residue_);
    theCout << std::setw(11) << numberOfIterations_ << ": "
                   << std::setprecision(5) << residue_<<eol<<std::flush;
  }
  if (numberOfIterations_ >= maxOfIterations_ and residue_ > epsilon_)
     warning("solver_notconverge", name_, numberOfIterations_, residue_, epsilon_);
}

/*!
  Print out the iteration
*/
void IterativeSolver::printIteration() const
{
  if ( verboseLevel_ > 2 )
  {
    if (numberOfIterations_ == 0) { theCout << "  Iteration: Residue" << eol << std::flush; }
    number_t s = std::max(maxOfIterations_/verboseLevel_,number_t(1));
    if (numberOfIterations_% s == 0)
        theCout << std::setw(11) << numberOfIterations_ << ": "
                << std::setprecision(5) << residue_<< eol << std::flush;
  }
}

/*!
  Store the residue
*/
void IterativeSolver::storeResidue()
{
  Trace::logOff();
  std::vector<real_t>::iterator i_r = residues_.begin();
  while ( i_r != residues_.end() - 1 ) { *i_r = *(i_r + 1); i_r++; }
  *i_r = residue_;
}

/*!
  Print out error in case of break down
*/
void IterativeSolver::breakdown(const real_t v, const string_t& param) const
{
  error("solver_breakdown", name_, param, v, numberOfIterations_);
}

/*!
  Print out error in case of invalid Omega
*/
void IterativeSolver::invalidOmega(const real_t om) const
{
  error("solver_badOmega", name_, om);
}

/*!
  Print out error in case of no precondionned Matrix
*/
void IterativeSolver::noPrecondMatrix() const
{
  error("solver_noPCMatrix", name_);
}

//==============================================================================
/*!
      Print result of this solver
*/
//==============================================================================
void IterativeSolver::printResult() const
{
	if (residue_ < epsilon_)
  { theCout << name_ << " converges after " << numberOfIterations_ << " iterations. The residue is: " << residue_ << eol; }
	else
  { theCout << name_ << " does NOT converge after " << numberOfIterations_ << " iterations. The residue is: " << residue_ << eol; }
}

void IterativeSolver::print(std::ostream& out) const
{
  if (isTestMode) { out.precision(testPrec); }
  else { out.precision(fullPrec); }
  out << words("Iterative solver") << " (" << name_ << ") " << words("of") << " " << words("type") << " " << words("iterative solver", type_);
  out << ", " << words("tolerance") << ": " << epsilon_;
  out << ", " << words("maximum number of iterations") << ": " << maxOfIterations_;
  out << " " << words("and") << " " << words("verbose level") << ": " << verboseLevel_;
}

//! main routine to manage parameters of solver constructors
void buildSolverParams(const std::vector<Parameter>& ps, real_t& tol, number_t& maxIt, real_t& omega, number_t& krylovDim,
                       number_t& verboseLevel, string_t& name, IterativeSolverType& solverType)
{
  trace_p->push("buildSolverParams(...)");
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_tolerance);
  params.insert(_pk_maxIt);
  params.insert(_pk_krylovDim);
  params.insert(_pk_solver);
  params.insert(_pk_verbose);
  params.insert(_pk_omega);
  params.insert(_pk_name);

  bool nameAlreadySet = (name!=string_t());
  bool solverAlreadySet = (solverType!=_noIterativeSolver);

  for (number_t i=0; i < ps.size(); ++i)
  {
    ParameterKey key=ps[i].key();
    switch (key)
    {
      case _pk_tolerance:
      {
        switch(ps[i].type())
        {
          case _real:
            tol=ps[i].get_r();
            break;
          default:
            error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      }
      case _pk_maxIt:
      {
        switch(ps[i].type())
        {
          case _integer:
            maxIt=ps[i].get_n();
            break;
          default:
            error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      }
      case _pk_krylovDim:
      {
        switch(ps[i].type())
        {
          case _integer:
            krylovDim=ps[i].get_n();
            break;
          default:
            error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      }
      case _pk_solver:
      {
        switch(ps[i].type())
        {
          case _integer:
          case _enumIterativeSolverType:
            solverType=IterativeSolverType(ps[i].get_n());
            break;
          default:
            error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      }
      case _pk_omega:
      {
        switch(ps[i].type())
        {
          case _integer:
            omega=real_t(ps[i].get_i());
            break;
          case _real:
            omega=ps[i].get_r();
            break;
          default:
            error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      }
      case _pk_verbose:
      {
        switch(ps[i].type())
        {
          case _integer:
            verboseLevel=ps[i].get_n();
            break;
          default:
            error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      }
      case _pk_name:
      {
        switch(ps[i].type())
        {
          case _string:
            name=ps[i].get_s();
            break;
          default:
            error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      }
      default:
        error("solver_unexpected_param_key", words("param key",key), words("iterative solver", solverType));
    }
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
        { error("unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);
  }
  // _solver is necessary if not already set
  if (!solverAlreadySet)
    { if (params.find(_pk_solver) != params.end()) { error("param_missing","_solver"); } }
  if (solverType != _sor && solverType != _ssor && params.find(_pk_omega) == params.end())
    { error("solver_unexpected_param_key", words("param key",_pk_omega), words("iterative solver", solverType)); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p=params.begin(); it_p != params.end(); ++it_p)
  {
    switch (*it_p)
    {
      case _pk_tolerance:
        tol=theDefaultConvergenceThreshold;
        break;
      case _pk_maxIt:
        maxIt=defaultMaxIterations;
        break;
      case _pk_krylovDim:
        krylovDim=defaultKrylovDimension;
        break;
      case _pk_solver:
        if (!solverAlreadySet) solverType=_noIterativeSolver;
        break;
      case _pk_omega:
        omega=1.0;
        break;
      case _pk_verbose:
        verboseLevel=theVerboseLevel;
        break;
      case _pk_name:
        if (!nameAlreadySet) name="IS";
        break;
      default:
        error("solver_unexpected_param_key",words("param key",*it_p), words("iterative solver", solverType));
    }
  }
  trace_p->pop();
}
} // end of namespace xlifepp
