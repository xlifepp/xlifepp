/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SsorSolver.hpp
  \authors C. Chambeyron, D. Martin, Manh Ha NGUYEN
  \since 10 jan 2005
  \date 22 Oct 2012

  \brief Definition of the xlifepp::SsorSolver class

  Class xlifepp::SsorSolver implements the Symmetric Successive Over Relaxation method
  for solving symmetric positive definite linear systems

  Base class: xlifepp::IterativeSolver
*/

#ifndef SSOR_SOLVER_HPP
#define SSOR_SOLVER_HPP

#include "IterativeSolver.hpp"

namespace xlifepp
{
/*!
     \class SorSolver
      Implement Symmetric Successive Over Relaxation algorithm
*/
class SsorSolver : public IterativeSolver
{
  private:
    real_t omega_;  //!< relaxation factor of SOR method

  public:
    //! Constructor with omega
    SsorSolver(real_t w=1.) : IterativeSolver(_ssor), omega_(w) {}

    //! Full constructor
    SsorSolver(real_t w, real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = theVerboseLevel)
    : IterativeSolver(_ssor, maxOfIt, eps, vb), omega_(w) {}

    //@{
    //! contructors with key-value system
    SsorSolver(const Parameter& p1) : IterativeSolver(_ssor)
    {
      std::vector<Parameter> ps(1, p1);
      number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_ssor;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega_, krylovDim, verboseLevel_, name_, ist);
    }
    SsorSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_ssor)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_ssor;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega_, krylovDim, verboseLevel_, name_, ist);
    }
    SsorSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_ssor)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_ssor;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega_, krylovDim, verboseLevel_, name_, ist);
    }
    SsorSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4) : IterativeSolver(_ssor)
    {
      std::vector<Parameter> ps(4);
      ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4;
      number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_ssor;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega_, krylovDim, verboseLevel_, name_, ist);
    }
    //@}

    ~SsorSolver() {} //!< destructor

    //@{
    //! print utility
    void print(std::ostream& out) const
    {
      out << words("Iterative solver") << " (" << name_ << ") " << words("of") << " " << words("type") << " " << words("iterative solver", type_);
      out << " (" << words("relaxation parameter") << " " << omega_ << ")";
      out << ", " << words("tolerance") << ": " << epsilon_;
      out << ", " << words("maximum number of iterations") << ": " << maxOfIterations_;
      out << " " << words("and") << words("verbose level") << ": " << verboseLevel_;
    }
    friend std::ostream& operator<<(std::ostream& out, const SsorSolver& ss)
    {
      ss.print(out);
      return out;
    }
    //@}

    //! Template Solvers
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0)
    {
      trace_p->push(name_ + "Solver");

      // Default value of maximum of iterations when not defined by user
      maxOfIterations_ = maximumOfIterations(vecX0.size());

      printHeader(vecX0.size(), omega_);
      if ( omega_ <= 0 || omega_ >= 2 ) { invalidOmega(omega_); }

      // Define the return value
      VecX x(vecX0);

      // Define initial residual vector according to Type
      VecX r(x);
      multMatrixVector(matA, x, r);
      r*=-1.;
      r+=vecB;

      algorithm(matA, vecB, x, r);

      printOutput(vecX0.size());
      printResult();

      trace_p->pop();

      return x;
    }

    //! runs solver
    template<class Mat, class Vec>
    Vec operator()(Mat& matA, Vec& vecB) { return (*this)(matA, vecB, vecB); }

  private:
    /*!
     Symmetric Successive Over Relaxation algorithm
     Solve matA.vecX=vecB using SSOR method with initial guess vecX:
            X_{k+1} = [D+wU]^{-1}.[(1-w)D-wL].[D+wL]^{-1}.[(1-w)D-wU] X_{k}
                    + w(2-w).[D+wU]^{-1}.D.[D+wL]{^-1} vecB
                    = [D/w+U]^{-1}.(2/w-1)D.[D/w+L]{^-1} vecB
                    + [D/w+U]^{-1}.[(1-1/w)D+L].[D/w+L]^{-1}.[(1-1/w)D+U] X_{k}
     where matA = L + D + U and w denotes the relaxation parameter in ]0, 2[
     Initial residue is an argument used to define the type (real or complex)
     of auxiliary vectors as matA (and P), vecB and X0 might not be of same type
    */
    template<class Mat, class VecB, class VecX>
    void algorithm(Mat& matA, VecB& vecB, VecX& vecX, VecX& vecR)
    {
      trace_p->push(name_ + "Algorithm");
      real_t normOfB(vecB.norm2());
      if ( normOfB < theBreakdownThreshold ) { normOfB = 1.; }
      residue_ = vecR.norm2() / normOfB;
      // Compute tb = [D/w+U]^{-1}.(2/w-1)D.[D/w+L]{^-1}.vecB
      VecX t1(vecX), t2(vecX), tb(vecX), ax(vecX);
      matA.sorLowerSolve(vecB, t1, omega_);
      matA.sorDiagonalMatrixVector(t1, t2, (2./omega_) - 1.);
      matA.sorUpperSolve(t2, tb, omega_);
      while ( (residue_ > epsilon_) && (numberOfIterations_ < maxOfIterations_) )
      {
        // Compute X_{k+1} = [D/w+U]^{-1}.[(1-1/w)D+L].[D/w+L]^{-1}.[(1-1/w)D+U].X_{k}
        //                 + [D/w+U]^{-1}.(2/w-1)D.[D/w+L]{^-1}.vecB
        matA.sorUpperMatrixVector(vecX, t1, 1.- (1./omega_)); // t1 = [(1-1/w)D+U].X_{k}
        matA.sorLowerSolve(t1, t2, omega_);                   // t2 = [D/w+L]^{-1}.t1
        matA.sorLowerMatrixVector(t2, t1, 1.- (1./omega_));   // t1 = [(1-1/w)D+L].t2
        matA.sorUpperSolve(t1, vecX, omega_);                 // X_{k+1} = tb + [D/w+U]^{-1}.t1
        vecX+=tb;
        //compute residue
        multMatrixVector(matA, vecX, vecR);
        vecR*=-1; vecR+=vecB;
        residue_ = vecR.norm2() / normOfB;    // Convergence when r1.norm2() is small enough
        numberOfIterations_++;
        printIteration();
      }
      trace_p->pop();
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
    }

}; // end of class SsorSolver ===================================================

}
#endif /* SSOR_SOLVER_HPP */
