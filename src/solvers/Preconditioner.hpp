/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Preconditioner.hpp
  \author C. Chambeyron, D. Martin, Ha NGUYEN, N. Kielbasiewicz
  \since 6 Sept 2005
  \date  17 Nov 2016

  \brief Definition of the xlifepp::Preconditioner class
*/

#ifndef PRECONDITIONER_HPP
#define PRECONDITIONER_HPP

#include "config.h"
#include "utils.h"
#include <iostream>

namespace xlifepp
{

/*!
  \class Preconditioner
  Define the preconditioner for linear systems

  This is a main class that users can derive to deal with
  preconditioning by operators. In most of the cases, users will use
  xlifepp::PreconditionerTerm when it is a preconditioning with TermMatrix/TermVector
  level (or MatrixEntry/VectorEntry)
*/
class TermVector;
class VectorEntry;

class Preconditioner
{
  private:
    bool counter_; // internal counter

  protected:
    //! Type of preconditioner
    PreconditionerType type_;

    //! TermVector given by user. It is supposed to be initial guess X0
    const TermVector* termVector_p;

  public:

    //! connstructor with PreconditionerType
    Preconditioner(PreconditionerType pt=_userPrec)
      : counter_(0), type_(pt), termVector_p(nullptr) {}

    Preconditioner(const Preconditioner& p) : counter_(p.counter_), type_(p.type_), termVector_p(p.termVector_p)
    {}

    // virtual bool isEmpty() const;
    static string_t name(PreconditionerType pt)
    {
      string_t s("");
      switch (pt)
      {
        case _luPrec:       s = "LU"; break;
        case _ldltPrec:     s = "LDLt"; break;
        case _ldlstarPrec:  s = "LDL*"; break;
        case _iluPrec:       s = "ILU"; break;
        case _ildltPrec:     s = "ILDLt"; break;
        case _ildlstarPrec:  s = "ILDL*"; break;
        case _ssorPrec:     s = "SSOR"; break;
        case _diagPrec:     s = "Diagonal"; break;
        case _productPrec:  s = "Product"; break;
        case _userPrec:     s = "User Supplied"; break;
        default: break;
      }
      return s;
    }

    string_t name() const { return name(type_); } //!< returns name

    PreconditionerType kind() const { return type_; } //!< returns type of preconditioner
    void kind(PreconditionerType pt) { type_ = pt; } //!< sets type of preconditioner

    const TermVector* termVector() const { return termVector_p; } //!< returns termVector_p
    void termVector(TermVector& tv) { termVector_p=&tv; } //!< sets termVector_p

    virtual ValueType valueType() const;
    virtual void solve(TermVector& B, TermVector& X) const;
    virtual void solve(VectorEntry& B, VectorEntry& X) const;

    virtual void leftSolve(TermVector& B, TermVector& X) const;
    virtual void leftSolve(VectorEntry& B, VectorEntry& X) const;

    virtual void toScalar(bool keepEntries=false);
    virtual void toGlobal(StorageType st, AccessType at, SymType symt=_noSymmetry, bool keepSuTerms=false);

    //! destructor
    virtual ~Preconditioner() {}
}; // end of Preconditioner class

} // namespace xlifepp

#endif  /* PRECONDITIONER_HPP */
