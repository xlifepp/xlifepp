/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file QmrSolver.hpp
  \authors C. Chambeyron, D. Martin, Manh Ha NGUYEN
  \since 24 nov 2004
  \date  06 Oct 2012

  \brief Definition of the xlifepp::QmrSolver class

  Class xlifepp::QmrSolver implements the Quasi-Minimal Residual method for solving
  linear systems, with or without preconditioner.
  QMR is not suitable for non symmetric systems

  Base class: xlifepp::IterativeSolver

  For elementary explanation about Quasi-Minimal Residual method method,
  see http://mathworld.wolfram.com/Quasi-MinimalResidualMethod.html
*/

#ifndef QMR_SOLVER_HPP
#define QMR_SOLVER_HPP

#include "IterativeSolver.hpp"

namespace xlifepp
{

/*!
    \class QmrSolver
     Implement Quasi-Minimal Residual algorithm
*/
class QmrSolver : public IterativeSolver
{
  public:
    //! Constructor
    QmrSolver()
      : IterativeSolver(_qmr) {}

    //! Full Constructor
    QmrSolver(real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = theVerboseLevel)
    : IterativeSolver(_qmr, maxOfIt, eps, vb) {}

    //@{
    //! contructors with key-value system
    QmrSolver(const Parameter& p1) : IterativeSolver(_qmr)
    {
      std::vector<Parameter> ps(1, p1);
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_qmr;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    QmrSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_qmr)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_qmr;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    QmrSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_qmr)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_qmr;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    //@}

    ~QmrSolver() {}

    //! Template Solvers without a precondition without argument "solType
    template<class Mat, class VecB, class VecX>
    VecX operator()(const Mat& matA, const VecB& vecB, VecX& vecX0)
    {
      if (matA.valueType() == _real && vecB.valueType() == _real)
      {
        return (*this)(matA,  vecB,  vecX0,  _real);
      }
      else
      {
        return (*this)(matA,  vecB,  vecX0,  _complex);
      }
    }
    //! Template Solvers without a precondition
    template<class Mat, class VecB, class VecX>
    VecX operator()(const Mat& matA, const VecB& vecB, VecX& vecX0, ValueType solType)
    {
      trace_p->push(name_ + "Solver");
      resetSolver();
      preconditioned_ = false;
      // Default value of maximum of iterations when not defined by user
      maxOfIterations_ = maximumOfIterations(vecX0.size());
      printHeader(vecX0.size());
      // Define the return value
      VecX x(vecX0);
      // Define initial residual vector according to Type
      VecX r;
      multMatrixVector(matA, x, r);
      r*=-1.; r+=vecB;
      //run algorithm
      if (_real == solType)
      {
        algorithm<real_t>(matA, vecB, x, r);
      }
      else
      {
        algorithm<complex_t>(matA, vecB, x, r);
      }
      //output
      printOutput(vecX0.size());
      printResult();
      trace_p->pop();
      return x;
    }

    //! Template Solvers without a preconditionner and vecX0 = vecB
    template<class Mat, class VecB, class VecX>
    VecX operator()(const Mat& matA, const VecB& vecB, ValueType solType)
    {
      return (*this)(matA, vecB, vecB, solType);
    }

    //! Template Solvers with a precondition without argument "solType
    template<class Mat, class VecB, class VecX, class VecPC>
    VecX operator()(const Mat& matA, const VecB& vecB, VecX& vecX0, VecPC& pc)
    {
      if (matA.valueType() == _real && vecB.valueType() == _real)
      {
        return (*this)(matA,  vecB,  vecX0, pc,  _real);
      }
      else
      {
        return (*this)(matA,  vecB,  vecX0, pc,  _complex);
      }
    }

    //!  Template Solvers with a preconditioner
    template<class Mat, class VecB, class VecX, class VecPC>
    VecX operator()(const Mat& matA, const VecB& vecB, VecX& vecX0, VecPC& pc, ValueType solType)
    {
      trace_p->push(name_ + "Solver");
      resetSolver();
      // Default value of maximum of iterations when not defined by user
      maxOfIterations_ = maximumOfIterations(vecX0.size());
      printHeader(vecX0.size(), pc.name());
      if (pc.valueType() == _complex) {  solType = _complex; }
      // Redefine initial vector according to Type
      VecX x(vecX0);
      VecX r;
      multMatrixVector(matA, x, r);
      r*=-1.; r+=vecB; // r = vecB - r;
      //run algorithm
      if (_real == solType)
      {
        algorithm<real_t>(matA, vecB, x, r, pc);
      }
      else
      {
        algorithm<complex_t>(matA, vecB, x, r, pc);
      }
      //output
      printOutput(vecX0.size());
      printResult();
      trace_p->pop();
      return x;
    }

  private:
    /*!
         Quasi-Minimal Residual algorithm
         Solve matA.vecX=vecB using QMR method without preconditionner.
             Typename K (K: real or complex) is the type of the initial residual vector
             and thus of auxiliary vectors as matrices matA, P, vecB
             and vector vecX0 might not share the same type
    */
    template<class K, class Mat, class VecB, class VecX>
    void algorithm(const Mat& matA, const VecB& vecB, VecX& vecX, VecX& vecR)
    {
      trace_p->push(name_ + "Algorithm");
      real_t normOfB = vecB.norm2();
      if (normOfB < theBreakdownThreshold) { normOfB = 1.; }
      residue_ = vecR.norm2();
      // gamma_{0} = 1 , eta_{0} = -1
      real_t gamma0, gamma1(1.), theta1(0.), theta0, gamma2, yNorm0;
      K eta(-1.), alpha(1.), delta(1.), beta;
      real_t yNorm1(residue_);
      VecX y = vecR;
      if (yNorm1 > theEpsilon) y/=yNorm1;
      real_t zNorm1(residue_);
      VecX z = vecR;
      if (zNorm1 > theEpsilon) z/=zNorm1;
      assign(delta, dotRC(z, y));
      VecX p(y), q(z), ap(p);
      multMatrixVector(matA, p, ap); // ap = matA*p
      VecX qA;
      assign(alpha, dotRC(q, ap));
      VecX d(vecR); // d_{0}
      VecX s(vecR); // s_{0}
      VecX etap;    // s_{0}
      residue_*=1./normOfB;
      while ((numberOfIterations_ < maxOfIterations_) && (residue_ > epsilon_))
      {
        yNorm0 = yNorm1; theta0 = theta1; gamma0 = gamma1;
        beta = -alpha / delta;
        if (std::abs(beta) < theBreakdownThreshold) { breakdown(std::abs(beta), "beta"); }
        y*=beta; y+=ap;       //y = beta*y + ap;
        yNorm1 = y.norm2();
        if (yNorm1 < theBreakdownThreshold) { breakdown(yNorm1, "Ynorm"); }
        y /= yNorm1;
        multVectorMatrix(q, matA, qA);
        z*=beta; z+=qA; //      z = beta*z + q*matA;
        zNorm1 = z.norm2();
        if (zNorm1 < theBreakdownThreshold) { breakdown(zNorm1, "Znorm"); }
        z /= zNorm1;
        theta1 = yNorm1 / (gamma0 * std::abs(beta));
        gamma1 = 1. / std::sqrt(1. + theta1 * theta1);
        if (std::abs(gamma1) < theBreakdownThreshold) { breakdown(std::abs(beta), "gamma"); }
        // update and convergence test
        gamma2 = gamma1 / gamma0; gamma2 *= gamma2;
        eta *= yNorm0 * gamma2 / beta;
        gamma2 = theta0 * gamma1; gamma2 *= gamma2;
        // d = eta*p_ + theta*gamma*d
        d*=gamma2; etap = p;
        etap *= eta; d+=etap;
        vecX += d;
        // s = eta*matA.p + theta*gamma*s
        s*=gamma2;
        ap*=eta;  s+=ap;
        etap = s; etap*=-1.; vecR+=etap;
        //  Convergence when vecR.norm2()/normOfB is small enough
        residue_ = vecR.norm2() / normOfB;
        numberOfIterations_++;
        printIteration();
        if (residue_ < epsilon_) { break; }
        assign(delta, dotRC(z, y));
        if (std::abs(delta) < theBreakdownThreshold) { breakdown(std::abs(delta), "delta"); }
        // p = Y/||Y|| - (||Z||*delta/alpha) * p
        p*=-zNorm1 * delta / alpha;
        p+=y;
        // q = Z/||Z|| - (||Y||*delta/alpha) * p
        q*= -yNorm1 * delta / alpha;
        q += z;
        multMatrixVector(matA, p, ap); // matA.mat_Vec(p, ap); ap = matA*p;
        assign(alpha, dotRC(q, ap));
        if (std::abs(alpha) < theBreakdownThreshold) { breakdown(std::abs(alpha), "alpha"); }
      }
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
      trace_p->pop();
    }

    /*!
       Quasi-Minimal Residual algorithm
       Solve matA.vecX=vecB using QMR method with preconditioning.
           Initial residue is an argument used to define the type (real or complex)
           of auxiliary vectors as matA (and P), vecB and vecX0 might not be of same type
       Version from http://math.nist.gov/iml++/qmr.h.txt
       Note : in general form, preconditioning QMR works with a matrix M = M1*M2
              this particular version assumes M1=M and M2=I (left preconditioning)
              because, up to now,  only one preconditioning matrix is passed
    */
    template<typename K, class Mat, class VecB, class VecX, class Prec>
    void algorithm(const Mat& matA, const VecB& vecB, VecX& vecX, VecX& vecR, Prec& PC)
    {
      trace_p->push(name_ + "Algorithm");
      real_t normOfB = vecB.norm2();
      if ( normOfB < theBreakdownThreshold ) { normOfB = 1.; }
      residue_ = vecR.norm2()/normOfB;
      VecX X(vecX), V(vecR), W(V);             // V=R=b-AX, W=V
      VecX P(X), Q, Ptilda(P), Qtilda(Q);
      VecX D(vecX), S(X), R0(vecR), Tmp(vecX);
      real_t ksi, gamma0=1., eta=-1., rho0, rho1;
      real_t gamma1, delta, eps=0., beta, theta1, theta0=0., cstte;
      VecX Y(vecB), Yhat(vecB), Z(vecB), Zhat(vecX);
      PC.solve(V,Y);               // Y=PC.solver(V);            (M1*Y=V)
      //PC.leftSolve(W,Z);         // Z=PC.transposedSolver(W);  (M2t*Z=W)
      Z=W;//                       // case M2=I -> Z=W
      rho0=Y.norm2(), ksi=Z.norm2();
      while (residue_ > epsilon_ && numberOfIterations_ < maxOfIterations_)
      {
        if (rho0 < theBreakdownThreshold)  { breakdown(rho0, "rho0"); }
        if (ksi  < theBreakdownThreshold)  { breakdown(ksi, "ksi"); }
        V/=rho0; Y/=rho0;
        W/=ksi; Z/=ksi;
        assign(delta , dotRC(Z, Y));     //delta=Y.dot(Z)
        if (std::abs(delta) < theBreakdownThreshold)  {breakdown(std::abs(delta), "delta");}
        //PC.solve(Y,Yhat);            // Yhat=PC.solver(Y);         (M2*Yhat=Y)
        Yhat=Y;                        // case M2=I -> Yhat=Y
        PC.leftSolve(Z,Zhat);    // Zhat=PC.transposedSolver(Z); (M1t*Zhat=Z)
        if (numberOfIterations_ == 0)
        {
          P=Yhat, Q=Zhat;
        }
        else
        {
      	  cstte = -ksi*delta/eps;
      	  P*=(-ksi*delta/eps);  P+=Yhat;
      	  Q*=(-rho0*delta/eps); Q+=Zhat;
        }
        multMatrixVector(matA, P, Ptilda);  //Ptilda=A*P;
        assign(eps, dotRC(Q, Ptilda) );     //eps=Q.dot(Ptilda)
        if (std::abs(eps) < theBreakdownThreshold)  {breakdown(std::abs(eps), "epsilon"); }
        beta=eps/delta;
        if (std::abs(beta) < theBreakdownThreshold)  {breakdown(std::abs(beta), "epsilon"); }
        V*=(-beta); V+=Ptilda;
        multVectorMatrix(Q, matA, Qtilda);
        W*=(-beta); W+=Qtilda;
        PC.solve(V,Y);                     // (M1*Y=V)
        //PC.transposedSolve(W,Z);         // (M2t*Z=W)
        Z=W;//                             // case M2=I -> Z=W
        rho1=Y.norm2(); ksi=Z.norm2();
        theta1=rho1/(gamma0*std::abs(beta));
        gamma1=1./(std::sqrt(1.+theta1*theta1));
        if (std::abs(gamma1) < theBreakdownThreshold)  {breakdown(std::abs(gamma1), "gamma1"); }
        cstte = -rho0*gamma1*gamma1/(gamma0*gamma0)/beta; eta *= cstte;
        if (numberOfIterations_ == 0)
        {
          D = P;      D*=eta;
          S = Ptilda; S*=eta;
        }
        else
        {
      	  cstte = (theta0*gamma1)*(theta0*gamma1);
          D*=cstte; Tmp = P; Tmp*=eta; D+=Tmp;
          S*=cstte; Tmp = Ptilda; Tmp*=eta; S+=Tmp;
        }
        // update
        //X+=D; S*=-1.; R0-=S; S*=-1.;
        X+=D; R0-=S;
        rho0=rho1; gamma0=gamma1; theta0=theta1;
        numberOfIterations_++;
        residue_=R0.norm2()/normOfB;
        printIteration();
      }
      vecX=X;
      vecR=R0;
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
      trace_p->pop();
    }
}; // end of class QmrSolver ====================================================

} // namespace xlifepp

#endif /* QMR_SOLVER_HPP */
