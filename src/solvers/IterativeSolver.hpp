/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file IterativeSolver.hpp
  \authors C. Chambeyron, D. Martin, Ha NGUYEN
  \since 24 nov 2004
  \date  24 Sep 2012

  \brief Definition of the xlifepp::IterativeSolver class

  Base class for following classes of Iterative Solvers

  - xlifepp::BicgSolver       BiConjugate Gradient algorithm
  - xlifepp::BicgStabSolver   BiConjugate Gradient Stabilized method
  - xlifepp::CgSolver         Conjugate Gradient algorithm for real/complex symmetric matrices
  - xlifepp::CghSolver        Conjugate Gradient algorithm for complex hermitian matrices
  - xlifepp::CgsSolver        Conjugate Gradient Squared algorithm
  - xlifepp::GmresSolver      Generalized Minimal Residual algorithm
  - xlifepp::QmrSolver        Quasi-Minimal Residual algorithm
  - xlifepp::SorSolver        Successive OverRelaxation algorithm
  - xlifepp::Ssorsolver       Symmetric Successive OverRelaxation algorithm

  Solvers objects must be "constructed" before use (see constructors for each)
  and linear system solvers are implemented with the following template overloaded
  operator() with or without preconditionning or initial vector X0:
  \code
  template<class Mat, class Vec> Vec operator(Mat m, vecB)
  \endcode
  without preconditionning and without starting vector or
  \code
  template<class Mat, class Vec> Vec operator(Mat m, vecB, vecX0)
  \endcode
  without preconditionning and with given starting vector X0 or
  \code
  template<class Mat, class Vec, class Prec> Vec operator(Mat m, vecB, Prec p)
  \endcode
  with preconditionning and without starting vector or
  \code
  template<class Mat, class Vec, class Prec> Vec operator(Mat m, vecB, vecX0, Prec p)
  \endcode
  with preconditionning and with given starting vector X0

  The following operators, acting on matrices or vectors, are required:

  - operator*(Mat, Vec)   : matrix vector multiplication m x V
  - operator*(Vec, Mat)   : transposed matrix vector multiplication or V x m
  - operator+(Vec, Vec)   : vector addition
  - operator-(Vec, Vec)   : vector subtraction

  class Mat must implement the 2 following members:
    - valueType()                         : returning type (real_t or complex_t) of matrix m
    - multMatrixVector(Mat, Vec, Vec)     : returns matrix x vector into second vector
  and as extra requirements for some specific solvers (when used):
    - multVectorMatrix(Mat, Vec, Vec)    : returns matrix transposed x vector into second vector
    -                                              >| for Bicg and Qmr solvers
    - sorDiagonalMatrixVector: [w*D] * x          >|
    - sorLowerMatrixVector: [w*D + L] * x      >|
    - sorUpperMatrixVector: [w*D + U] * x      >| for SOR and SSOR solvers
    - sorLowerSolver: [D/w + L] x = b    >|
    - sorUpperSolver: [U/w + L] x = b    >|

  class Vec must implement the following members:
    - Vec(const Vec&)          : the standard copy constructor
    - operator*=               : multiplication by a scalar
    - operator+=               : addition of a vector to the current vector
    - add(Vec, Vec)            : alternate addition of vector to vector
    - operator-=               : subtraction of a vector from the current vector
    - function dotRC(Vec&, Vec&)  : scalar product for both real and complex vectors
    - function norm2()         : euclidian norm of vector

  class Prec, when used, must implement the following members:
    - precondMatrix_p: a pointer to an object of class Mat
    - Prec::solver(Vec& b)    : return the solution vector of p x = b

  members:
  --------
  name_
      name of Iterative Solver
  epsilon_
      convergence threshold (default: theDefaultConvergenceThreshold_ )
  maxOfIterations_
      maximum number of iterations (default: defaultMaxIterations_ )
  preconditioned_
      bool flag for preconditioned linear system solvers
*/

#ifndef ITERATIVE_SOLVER_HPP
#define ITERATIVE_SOLVER_HPP

#include "config.h"
#include "utils.h"
#include "Preconditioner.hpp"

namespace xlifepp
{
/*!
     Base class of other Solver classes
*/
class IterativeSolver
{
  public:
    //! Constructor by name
    IterativeSolver(const string_t& name, number_t vb = theVerboseLevel);
    //! Constructor by type
    IterativeSolver(IterativeSolverType ist, number_t vb = theVerboseLevel);
    //! Full constructor without type
    IterativeSolver(const string_t& name, number_t maxOfIt, real_t eps, number_t vb = theVerboseLevel, bool prec = true);
    //! Full constructor without name
    IterativeSolver(IterativeSolverType ist, number_t maxOfIt, real_t eps, number_t vb = theVerboseLevel, bool prec = true);
    //! Full constructor
    IterativeSolver(const string_t& name, IterativeSolverType ist, number_t maxOfIt, real_t eps, number_t vb = theVerboseLevel, bool prec = true);
    //! Destructor
    virtual ~IterativeSolver();
    //! Iterative solver name for documentation purposes
    string_t name() { return name_; }
    //! Reset Solver
    void resetSolver();
    void printOutput(const size_t nbRows) const; //!< print utility
    number_t nbIterations()   //! returns the number of iterations
    {return numberOfIterations_ ;}
    real_t residue() const    //! returns the residue
    {return residue_;}
  protected:
    //! Define a default value for the maximum number of iterations
    number_t maximumOfIterations(const size_t nbRows);
    //@{
    //! Print some useful information
    void printHeader(const size_t nbRows) const;
    void printHeader(const size_t nbRows, const string_t& PCname) const;
    void printHeader(const size_t nbRows, const real_t omega) const;
    void printHeader(const size_t nbRows, const number_t kd) const;
    void printHeader(const size_t nbRows, const number_t kd, const string_t& PCname) const;
//    void printOutput() const;
    void printResult() const;
    void printIteration() const;
    //@}

    //@{
    //! print utility
    void print(std::ostream& out) const;
    friend std::ostream& operator<<(std::ostream& out, const IterativeSolver& is)
    {
      is.print(out);
      return out;
    }
    //@}
    void storeResidue();
    void breakdown(const real_t v, const string_t& param = "") const;
    void invalidOmega(const real_t om) const;
    void noPrecondMatrix() const;

  protected:
    //! Name of Iterative Solver
    string_t name_;
    //! type of iterative solver
    IterativeSolverType type_;
    //! Maximum number of iterations
    number_t maxOfIterations_;
    //! current number of iterations
    number_t numberOfIterations_;
    //! Convergence threshold
    real_t epsilon_;
    //! current residue
    real_t residue_;
    //! Flag for preconditioned solvers
    bool preconditioned_;
    //! Vector to store the last values of residue (for a potential use in the future)
    Vector<real_t> residues_;
    //! Verbose level
    number_t verboseLevel_;
    //! true if logged
    bool logged_;
  public:
    static number_t theNumberOfIterations; //!< number of iterations (global scope)
    static real_t theResidue;              //!< residue (global scope)
}; // end of class IterativeSolver ==============================================

//! main routine to manage parameters of solver constructors
void buildSolverParams(const std::vector<Parameter>& ps, real_t& tol, number_t& iterMax, real_t& omega, number_t& krylovDim,
                       number_t& verboseLevel, string_t& name, IterativeSolverType& solverType);

//@{
/*!
  Assigns y to x for all combinations of a priori unknown types of x and y
  thus preventing from an invalid cast
*/
inline void assign(real_t& x, const real_t& y) { x = y; }
inline void assign(complex_t& x, const real_t& y) { x = y; }
inline void assign(complex_t& x, const complex_t& y) { x = y; }
inline void assign(real_t& x, const complex_t& y) { x = y.real(); }
//@}

} // end of namespace xlifepp

#endif  // ITERATIVE_SOLVER_HPP
