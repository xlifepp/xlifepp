/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file BicgSolver.hpp
  \authors C. Chambeyron, D. Martin, Manh Ha NGUYEN
  \since 24 Nov 2004
  \date  01 Oct 2012

  \brief Definition of the xlifepp::BicgSolver class

  Class xlifepp::BicgSolver implements the BiConjugate Gradient method for solving
  (non-symmetric) linear systems, with or without preconditioner.

  BiCG delivers about the same results as CG for symmetric linear systems
  at twice the cost (use CG instead)
  BiCG is not always suitable for non symetric linear systems (possible //breakdown)
  (use BiCGStab instead)
  In short one should never use BiCG !!! But IT WORKS ...

  Base class: xlifepp::IterativeSolver

  For elementary explanation about BiConjugate Gradient method,
  see e.g. http://mathworld.wolfram.com/BiconjugateGradientMethod.html
*/

#ifndef BICG_SOLVER_HPP
#define BICG_SOLVER_HPP

#include "IterativeSolver.hpp"
#include "Preconditioner.hpp"

namespace xlifepp
{

/*!
     \class BicgSolver
     Implement BiConjugate Gradient algorithm
*/
class BicgSolver : public IterativeSolver
{
  public:
    //! Default Constructor
    BicgSolver() : IterativeSolver(_bicg) {}

    //! Full constructor
    BicgSolver(real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = theVerboseLevel)
    : IterativeSolver(_bicg, maxOfIt, eps, vb) {}

    //@{
    //! contructors with key-value system
    BicgSolver(const Parameter& p1) : IterativeSolver(_bicg)
    {
      std::vector<Parameter> ps(1, p1);
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_bicg;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    BicgSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_bicg)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_bicg;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    BicgSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_bicg)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_bicg;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    //@}

    ~BicgSolver() {}

    //! Template Solvers without a precondition without argument "solType
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0)
    {
      if ( matA.valueType() == _real && vecB.valueType() == _real )
      {
        return (*this)( matA,  vecB,  vecX0,  _real );
      }
      else
      {
        return (*this)( matA,  vecB,  vecX0,  _complex );
      }
    }

    //! Template Solvers without a precondition
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, ValueType solType)
    {
      trace_p->push(name_ + "Solver");
      resetSolver();
      preconditioned_ = false;
      // Default value of maximum of iterations when not defined by user
      maxOfIterations_ = maximumOfIterations(vecX0.size());
      //initialize guess and residue
      printHeader(vecX0.size());
      VecX x(vecX0);
      VecX r;
      multMatrixVector(matA, x, r);
      r*=-1.;
      r+=vecB;
      //run algorithm
      if (_real == solType)
      {
        algorithm<real_t>(matA, vecB, x, r);
      }
      else
      {
        algorithm<complex_t>(matA, vecB, x, r);
      }
      //output
      printOutput(vecX0.size());
      printResult();
      trace_p->pop();
      return x;
    }

    //! Template Solvers without a preconditionner and vecX0 = vecB
    template<class Mat, class VecX>
    VecX operator()(Mat& matA, VecX& vecB, ValueType solType)
    {
      return (*this)(matA, vecB, vecB, solType);
    }

    //! Template Solvers with a precondition without argument "solType
    template<class Mat, class VecB, class VecX, class VecPC>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, VecPC& pc)
    {
      resetSolver();
      if ( (_real == matA.valueType()) &&  (_real == vecB.valueType() ) )
      {
        return (*this)( matA,  vecB,  vecX0, pc, _real );
      }
      else
      {
        return (*this)( matA,  vecB,  vecX0, pc, _complex );
      }
    }

    //! Template Solvers with a preconditioner
    template<class Mat, class VecB, class VecX, class Prec>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, Prec& pc, ValueType solType)
    {
      trace_p->push(name_ + "Solver");
      resetSolver();
      // Default value of maximum of iterations when not defined by user
      maxOfIterations_ = maximumOfIterations(vecX0.size());
      //print header
      if ( preconditioned_ )
      {
        printHeader(vecX0.size(), pc.name());
        if ( pc.valueType() == _complex ) { solType = _complex; }
      }
      else { printHeader(vecX0.size()); }

      // Define initial residual vector according to Type
      VecX x(vecX0);
      VecX r;
      multMatrixVector(matA, x, r);
      r*=-1.;
      r+=vecB;
      //run algorithm
      if ( _real == solType )
      {
        algorithm<real_t>(matA, vecB, x, r, pc);
      }
      else
      {
        algorithm<complex_t>(matA, vecB, x, r, pc);
      }
      //output
      printOutput(vecX0.size());
      printResult();
      trace_p->pop();
      return x;
    }

  private:
    /*!
        Solve matA.vecX=vecB using BiCG method with initial guess vecX without preconditioning.
          Typename K (K: real or complex) is the type of the initial residual vector
          and thus of auxiliary vectors as matrices matA, vecB
          and vector vecX0 might not share the same type
    */
template<typename K, class Mat, class VecB, class VecX>
    void algorithm(Mat& matA, VecB& vecB, VecX& vecX, VecX& vecR)
    {
      trace_p->push(name_ + "Algorithm");
      real_t normOfB = vecB.norm2();
      if ( normOfB < theBreakdownThreshold ) { normOfB = 1.; }
      VecX X(vecX), Q(vecX), Qt(vecX), P(vecX), Pt(vecX);
      VecX R (vecR);    //R = B - A*X0;
      residue_=R.norm2()/normOfB;
      VecX Rt(R),tmp(vecX);
      K beta, alpha, rhoold=0, rho;
      while ( residue_>epsilon_ && numberOfIterations_ < maxOfIterations_)
      {
        assign(rho,dotRC(R,Rt));   //  rho=dotRC(Z,Rt);
        if ( std::abs(rho) < theBreakdownThreshold ) breakdown(std::abs(rho), "rho");
        if (numberOfIterations_==0){ P=R; Pt=Rt;}
        else
        {
          beta=rho/rhoold;
          P*=beta;  P+=R;                    //P = R+beta*P
          Pt*=beta; Pt+=Rt;                  //Pt = Rt+beta*Pt
        }
        multMatrixVector(matA, P, Q);        //Q = A*P;
        multVectorMatrix(Pt, matA, Qt);      //Qt = Pt*A;
        assign(alpha, rho /dotRC(Pt, Q));    //alpha = rho/(Pt,Q);
        tmp = P; tmp*=alpha; X+=tmp;         //x += alpha*P
        Q*=-alpha;  R+=Q;                    //R -= alpha*Q;
        Qt*=-alpha; Rt+=Qt;                  //Rt -= alpha*Qt;
        rhoold=rho;
        numberOfIterations_++;
        residue_ = R.norm2()/normOfB;
        printIteration();
      }
      vecX = X;
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
      trace_p->pop();
    }

    /*!
      Solve matA.vecX=vecB using BiCG method with initial guess vecX  and an a preconditioner.
          Typename K (K: real or complex) is the type of the initial residual vector
          and thus of auxiliary vectors as matrices matA, P, vecB
          and vector vecX0 might not share the same type
    */
template<typename K, class Mat, class VecB, class VecX, class Prec>
    void algorithm(Mat& matA, VecB& vecB, VecX& vecX, VecX& vecR, Prec& M)
    {
      trace_p->push(name_ + "Algorithm");
      real_t normOfB = vecB.norm2();
      if ( normOfB < theBreakdownThreshold ) { normOfB = 1.; }
      VecX X(vecX), Q(vecX), Qt(vecX), P(vecX), Pt(vecX);
      VecX R(vecR);     //R = B - A*X;
      residue_=R.norm2()/normOfB;
      VecX Rt(R), Z(vecX), Zt(vecX), tmp(vecX);
      K beta, alpha, rhoold=0, rho;
      while ( residue_>epsilon_ && numberOfIterations_<maxOfIterations_)
      {
        M.solve(R,Z);              //Z = M^{-1}R
        M.leftSolve(Rt,Zt);        //Zt = M^{-t}Rt
        assign(rho,dotRC(Z,Rt));   //rho = dotRC(Z,Rt);
        if ( std::abs(rho) < theBreakdownThreshold ) breakdown(std::abs(rho), "rho");
        if (numberOfIterations_==0) { P=Z; Pt=Zt;}
        else
        {
          beta=rho/rhoold;
          P*=beta; P+=Z;                        //P = Z+beta*P
          Pt*=beta; Pt+=Zt;                     //Pt = Zt+beta*Pt
        }
        multMatrixVector(matA, P, Q);           //Q =A*P;
        multVectorMatrix(Pt, matA, Qt);         //Qt = Pt*A (A^t*Pt)
        assign(alpha, rho /dotRC(Pt, Q) );      //alpha = rho/(Pt,Q)
        tmp = P; tmp*=alpha; X+=tmp;            //X += alpha*P
        Q*=-alpha; R+=Q;                        //R -= alpha*Q;
        Qt*=-alpha; Rt+=Qt;                     //Rt -= alpha*Qt;
        rhoold=rho;
        residue_ = R.norm2()/normOfB;
        numberOfIterations_++;
        printIteration();
      }
      vecX = X;
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
      trace_p->pop();
    }


}; // end of class BicgSolver ===================================================

} // namespace xlifepp

#endif /* BICG_SOLVER_HPP */
