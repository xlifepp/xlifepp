/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file BicgStabSolver.hpp
  \authors C. Chambeyron, D. Martin, Manh Ha NGUYEN, E. Lunéville
  \since 24 Nov 2004
  \date  20 Nov 2015

  \brief Definition of the xlifepp::BicgStabSolver class

  Class xlifepp::BicgStabSolver implements thn BiConjugate Gradient Stabilized method for solving
  (non symmetric) linear systems, with or without preconditioner.
  Bicgs is suitable for non symmetric systems

  Base class: xlifepp::IterativeSolver

  For elementary explanation about BiConjugate Gradient Stabilized method,
  see e.g. http://mathworld.wolfram.com/BiconjugateGradientStabilizedMethod.html
*/

#ifndef BICGSTAB_SOLVER_HPP
#define BICGSTAB_SOLVER_HPP

#include "IterativeSolver.hpp"
#include "Preconditioner.hpp"

namespace xlifepp
{

/*!
   \class BicgStabSolver
   Implement BiConjugate Gradient Stabilized algorithm
*/
class BicgStabSolver : public IterativeSolver
{
  public:
    //! Default Constructor
    BicgStabSolver() : IterativeSolver(_bicgstab) {}

    //! Full Constructor
    BicgStabSolver(real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = theVerboseLevel)
    : IterativeSolver(_bicgstab, maxOfIt, eps, vb) {}

    //@{
    //! contructors with key-value system
    BicgStabSolver(const Parameter& p1) : IterativeSolver(_bicgstab)
    {
      std::vector<Parameter> ps(1, p1);
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_bicgstab;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }

    BicgStabSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_bicgstab)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_bicgstab;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }

    BicgStabSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_bicgstab)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_bicgstab;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    //@}

    ~BicgStabSolver() {}

    /*!
        Return the maximum iteration as a function of linear system size
        \param nbRows number of row of vector unknown X
        \return Maximum number of iteration
        BiCGStab is not guaranteed to converge in nbRows iterations so the default value is set to 2*nbRows
    */
    number_t maximumOfIterations(const size_t nbRows)
    {
      number_t maxOfIt = maxOfIterations_;
      if ( maxOfIt == defaultMaxIterations )
          maxOfIt=2*nbRows;
      return maxOfIt;
    }

    //! Template Solvers without a precondition without argument "solType
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0)
    {
      if ( matA.valueType() == _real && vecB.valueType() == _real )
      {
        return (*this)( matA,  vecB,  vecX0,  _real );
      }
      else
      {
        return (*this)( matA,  vecB,  vecX0,  _complex );
      }
    }

    //! Template Solvers without a precondition
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, ValueType solType)
    {
      trace_p->push(name_ + "Solver");
      resetSolver();
      preconditioned_ = false;
      // Default value of maximum of iterations when not defined by user
      maxOfIterations_ = maximumOfIterations(vecX0.size());
      printHeader(vecX0.size());
      VecX x(vecX0);
      //initialize residue
      VecX r;
      multMatrixVector(matA, x, r);
      r*=-1.;
      r+=vecB;     //r=b-Ax
      //run algorithm
      if (_real == solType)
      {
        algorithm<real_t>(matA, vecB, x, r);
      }
      else
      {
        algorithm<complex_t>(matA, vecB, x, r);
      }
      //output
      printOutput(vecX0.size());
      printResult();
      trace_p->pop();
      return x;
    }

    //! Template Solvers without a preconditionner and vecX0 = vecB
    template<class Mat, class VecX>
    VecX operator()(Mat& matA, VecX& vecB, ValueType solType)
    {
      return (*this)(matA, vecB, vecB, solType);
    }

    //! Template Solvers with a precondition without argument "solType
    template<class Mat, class VecB, class VecX, class VecPC>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, VecPC& pc)
    {
      if ( (_real == matA.valueType()) &&  (_real == vecB.valueType() ) )
      {
        return (*this)( matA,  vecB,  vecX0, pc, _real );
      }
      else
      {
        return (*this)( matA,  vecB,  vecX0, pc, _complex );
      }
    }

    //!  Template Solvers with a preconditioner
    template<class Mat, class VecB, class VecX,  class Prec>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, Prec& pc, ValueType solType)
    {
      trace_p->push(name_ + "Solver");
      resetSolver();
      // Default value of maximum of iterations when not defined by user
      maxOfIterations_ = maximumOfIterations(vecX0.size());
      if ( preconditioned_ )
      {
        printHeader(vecX0.size(), pc.name());
        if ( pc.valueType() == _complex ) { solType = _complex; }
      }
      else { printHeader(vecX0.size()); }
      VecX x(vecX0);
      //initialize residue
      VecX r;
      multMatrixVector(matA, x, r);
      r*=-1.;
      r+=vecB;  //r=b-Ax
      //run algorithm
      if ( _real == solType )
      {
        algorithm<real_t>(matA, vecB , x , r, pc);
      }
      else
      {
        algorithm<complex_t>(matA, vecB , x , r, pc);
      }
      //output
      printOutput(vecX0.size());
      printResult();
      trace_p->pop();
      return x;
    }

  private:
    /*!
       Solve Ax=b using BiCGs method with initial guess x=x0  without preconditioner.
           Typename K (K: real or complex) is the type of the initial residual vector
           and thus of auxiliary vectors as matrices A, M, b, x0 might not share the same type
    */


    template<typename K, class Mat, class VecB, class VecX>
    void algorithm(Mat& A, VecB& b, VecX& x, VecX& r)
    {
      trace_p->push(name_ + "algorithm");
      real_t normOfB = b.norm2();
      if ( normOfB < theBreakdownThreshold ) { normOfB = 1.; }
      // Some intermediate variables
      K rho_1, rho_2(0), alpha(0), beta, omega(0), sigma;
      VecX p(b), s(b), t(b), v(b), rtilde(b), q(b);
      //first residue r=b-Ax
      multMatrixVector(A, x, r);
      r*=-1.;  r+=b;
      residue_ = r.norm2() / normOfB;
      rtilde = r;
      // Iterative loop
      while ( (numberOfIterations_ < maxOfIterations_) && (residue_ > epsilon_) )  //main loop
      {
        assign(rho_1, hermitianProduct(r,rtilde));
        if (std::abs(rho_1) < theBreakdownThreshold) { breakdown(std::abs(rho_1), "rho_1"); }

        if (numberOfIterations_ == 0 ) p = r;
        else
        {
          if (std::abs(omega) < theBreakdownThreshold) { breakdown(std::abs(omega), "omega"); }
          // p= r + beta*(p-omega*v) where beta = rho_i/rho_{i-1} * alpha/omega
          beta = (rho_1 / rho_2) * (alpha / omega);
          v*=-omega;  // -omega*v
          p+=v;       // p-omega*v
          p*=beta;    // beta*(p-omega*v)
          p+=r;       // r + beta*(p-omega*v)
        }
        multMatrixVector(A, p, v);  //v =  A*p
        // s=r-alpha*v  where alpha = rho_i / (v,rtilde)
        assign(sigma, hermitianProduct(v,rtilde));
        if (std::abs(sigma) < theBreakdownThreshold) { breakdown(std::abs(sigma), "sigma"); }
        alpha = rho_1 / sigma;
        s=v;
        s*=-alpha;      // -alpha*v
        s+=r;           // r - alpha*v
        // first convergence test
        residue_= s.norm2() / normOfB;
        if (residue_ < epsilon_) // x=x+alpha*phat
        {
          p*=alpha;  x+=p;  break;
        }
        multMatrixVector(A, s, t);  // t= A*s
        // new iterate x=x+alpha*p+omega*s
        assign(omega, hermitianProduct(s, t) / hermitianProduct(t, t));
        q=p;
        q*=alpha;    x+=q;   //x + alpha*p
        q=s;
        q*=omega;    x+=q;   //x + alpha*p + omega*s
        // new residual r=s-omega*t
        r=s;
        t*=-omega;
        r+=t;
        residue_ = r.norm2() / normOfB;
        rho_2 = rho_1;
        ++numberOfIterations_;
        printIteration();
      }
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
      trace_p->pop();
    }


    /*!
         Solve Ax=b using BiCGs method with initial guess x=x0  and a preconditioner.
           Typename K (K: real or complex) is the type of the initial residual vector
           and thus of auxiliary vectors as matrices A, M, b, x0 might not share the same type
    */
    template<typename K, class Mat, class VecB, class VecX, class Prec>
    void algorithm(Mat& A, VecB& b, VecX& x, VecX& r, Prec& M)
    {
      trace_p->push(name_ + "algorithm");
      real_t normOfB = b.norm2();
      if ( normOfB < theBreakdownThreshold ) { normOfB = 1.; }
      // Some intermediate variables
      K rho_1, rho_2(0), alpha(0), beta, omega(0), sigma;
      VecX p(b), s(b), t(b), v(b), rtilde(b), phat(b), shat(b), q(b);
      //first residue r=b-Ax
      multMatrixVector(A, x, r);
      r*=-1.;  r+=b;
      residue_ = r.norm2() / normOfB;
      rtilde = r;
      while ( (numberOfIterations_ < maxOfIterations_) && (residue_ > epsilon_) )  //main loop
      {
        assign(rho_1, hermitianProduct(r,rtilde));
        if (std::abs(rho_1) < theBreakdownThreshold) { breakdown(std::abs(rho_1), "rho_1"); }
        if (numberOfIterations_ == 0 ) p = r;
        else
        {
          if (std::abs(omega) < theBreakdownThreshold) { breakdown(std::abs(omega), "omega"); }
          // p= r + beta*(p-omega*v) where beta = rho_i/rho_{i-1} * alpha/omega
          beta = (rho_1 / rho_2) * (alpha / omega);
          v*=-omega;  // -omega*v
          p+=v;       // p-omega*v
          p*=beta;    // beta*(p-omega*v)
          p+=r;       // r + beta*(p-omega*v)
        }
        M.solve(p, phat);              // preconditioning phat = M^{-1} * p
        multMatrixVector(A, phat, v);  //v = A*phat= A*M^{-1}*p
        // s=r-alpha*v  where alpha = rho_i / (v,rtilde)
        assign(sigma, hermitianProduct(v,rtilde));
        if (std::abs(sigma) < theBreakdownThreshold) { breakdown(std::abs(sigma), "sigma"); }
        alpha = rho_1 / sigma;
        s=v;
        s*=-alpha;      // -alpha*v
        s+=r;           // r - alpha*v
        // first convergence test
        residue_= s.norm2() / normOfB;
        if (residue_ < epsilon_) // x=x+alpha*phat
        {
          phat*=alpha;  x+=phat;  break;
        }
        M.solve(s, shat);              // preconditioning shat = M^{-1} s
        multMatrixVector(A, shat, t);  // t= A*shat = A*M^{-1}*s
        // new iterate x=x+alpha*p+omega*s
        assign(omega, hermitianProduct(s, t) / hermitianProduct(t, t));
        q=phat;
        q*=alpha;    x+=q;   //x + alpha*p
        q=shat;
        q*=omega;    x+=q;   //x + alpha*p + omega*s
        // new residual r=s-omega*t
        r=s;
        t*=-omega;
        r+=t;
        residue_ = r.norm2() / normOfB;
        rho_2 = rho_1;
        ++numberOfIterations_;
        printIteration();
      }
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
      trace_p->pop();
    }

}; // end of class BicgStabSolver

} // namespace xlifepp

#endif /* BICGSTAB_SOLVER_HPP */
