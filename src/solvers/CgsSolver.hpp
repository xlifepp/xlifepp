/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file CgsSolver.hpp
  \authors C. Chambeyron, D. Martin, Manh Ha NGUYEN
  \since 24 Nov 2004
  \date  16 Oct 2012

  \brief Definition of the xlifepp::CgsSolver class

  Class xlifepp::CgsSolver implements the Conjugate Gradient Squared method for solving
  linear systems, with or without preconditioner.
  CGS is suitable for non symmetric linear systems.
  CGS roughly behaves as BiCG but does not involves transposed matrix vector

  Base class: xlifepp::IterativeSolver

  For elementary explanation about Conjugate Gradient Squared method,
  see http://mathworld.wolfram.com/ConjugateGradientSquaredMethod.html
*/

#ifndef CGS_SOLVER_HPP
#define CGS_SOLVER_HPP

#include "IterativeSolver.hpp"
#include "Preconditioner.hpp"

namespace xlifepp
{

/*!
    \class CgsSolver
    Implement Conjugate Gradient Squared algorithm
*/
class CgsSolver : public IterativeSolver
{
  public:
    //! Default constructor
    CgsSolver()
      : IterativeSolver(_cgs) {}

    //! Full constructor
    CgsSolver(real_t eps, number_t maxOfIt = defaultMaxIterations, number_t vb = theVerboseLevel)
      : IterativeSolver(_cgs, maxOfIt, eps, vb) {}

    //@{
    //! contructors with key-value system
    CgsSolver(const Parameter& p1) : IterativeSolver(_cgs)
    {
      std::vector<Parameter> ps(1, p1);
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_cgs;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    CgsSolver(const Parameter& p1, const Parameter& p2) : IterativeSolver(_cgs)
    {
      std::vector<Parameter> ps(2);
      ps[0]=p1; ps[1]=p2;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_cgs;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    CgsSolver(const Parameter& p1, const Parameter& p2, const Parameter& p3) : IterativeSolver(_cgs)
    {
      std::vector<Parameter> ps(3);
      ps[0]=p1; ps[1]=p2; ps[2]=p3;
      real_t omega=1.; number_t krylovDim=defaultKrylovDimension;
      IterativeSolverType ist=_cgs;
      buildSolverParams(ps, epsilon_, maxOfIterations_, omega, krylovDim, verboseLevel_, name_, ist);
    }
    //@}

    ~CgsSolver() {}

    //! Template Solvers without a preconditioner without argument "solType
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0)
    {
      if ( matA.valueType() == _real && vecB.valueType() == _real )
      {
        return (*this)( matA,  vecB,  vecX0,  _real );
      }
      else
      {
        return (*this)( matA,  vecB,  vecX0,  _complex );
      }
    }

    //! Template solvers without a preconditioner
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, ValueType solType)
    {
      trace_p->push(name_ + "Solver");
      resetSolver();
      // Default value of maximum of iterations when not defined by user//
      maxOfIterations_ = maximumOfIterations(vecX0.size());
      printHeader(vecX0.size());
      VecX x(vecX0);
      //compute initial residue
      VecX r;
      multMatrixVector(matA, x, r);
      r*=-1.; r+=vecB;
      //run algorithm
      if (_real == solType)
      {
        algorithm<real_t>(matA, vecB, x, r);
      }
      else
      {
        algorithm<complex_t>(matA, vecB, x, r);
      }
      //output
      printOutput(vecX0.size());
      printResult();
      trace_p->pop();
      return x;
    }

    //! Template solvers without a preconditionner (vecB = vecX0)
    template<class Mat, class VecB, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, ValueType solType)
    {
      return (*this)(matA, vecB, vecB, solType);
    }

    //! Template Solvers with a preconditionner without argument "solType
    template<class Mat, class VecB, class VecX, class VecPC>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, VecPC& pc)
    {
      if ( matA.valueType() == _real && vecB.valueType() == _real )
      {
        return (*this)( matA,  vecB,  vecX0, pc, _real );
      }
      else
      {
        return (*this)( matA,  vecB,  vecX0, pc, _complex );
      }
    }

    //! Template solvers with a preconditionner
    template<class Mat, class VecB, class Prec, class VecX>
    VecX operator()(Mat& matA, VecB& vecB, VecX& vecX0, Prec& pc, ValueType solType)
    {
      trace_p->push(name_ + "Solver");
      resetSolver();
      // Default value of maximum of iterations when not defined by user
      maxOfIterations_ = maximumOfIterations(vecX0.size());
      printHeader(vecX0.size(), pc.name());
      if ( pc.valueType() == _complex ) { solType = _complex; }
      // Define initial residual vector according to Type
      VecX x(vecX0);
      VecX r;
      multMatrixVector(matA, x, r);
      r*=-1.; r+=vecB;
      //run algorithm
      if ( _real == solType )
      {
        algorithm<real_t>(matA, vecB , x , r, pc);
      }
      else
      {
        algorithm<complex_t>(matA, vecB , x , r, pc);
      }
      //output
      printOutput(vecX0.size());
      printResult();
      trace_p->pop();
      return x;
    }

  private:
    /*!
      Solve matA.vecX=vecB using CGS method without preconditioning.
      coming from Barrett et all. (1996)."Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods."
      Mathematics of Computation. 64. 10.2307/2153507.
      Typename K (K: real or complex) is the type of the initial residual vector
      and thus of auxiliary vectors as matrices matA, vecB
      and vector vecX0 might not share the same type
     */
    template<typename K, class Mat, class VecB, class VecX>
    void algorithm(Mat& A, VecB& b, VecX& x, VecX& r)
    {
      trace_p->push(name_ + "algorithm");
      K rho_1(1), rho_2(1), alpha(1), beta(1);
      VecX p(r), q(r) , u(r), v(r), t(r);
      real_t normOfB = b.norm2();
      if ( normOfB< theBreakdownThreshold ) { normOfB = 1.; }
      residue_ = r.norm2() / normOfB;
      if (residue_ < epsilon_){trace_p->pop();  return;}
      VecX rtilde = r;
      while ( numberOfIterations_ < maxOfIterations_  && residue_  > epsilon_)
      {
        assign(rho_1, dotRC(rtilde, r));
        if (std::abs(rho_1) < theBreakdownThreshold) breakdown(std::abs(rho_1),"rho_1");
        if (numberOfIterations_==0){ u = r; p = u; }
        else
        {
          beta = rho_1 / rho_2;
          u = q ;  u *= beta;  u += r;           // u = r + beta * q;
          p *= beta; p += q; p *= beta; p += u;  // p = u + beta* (q + beta * p);
        }
        multMatrixVector(A, p, v);    // v = A * p
        assign(alpha, rho_1 / dotRC(rtilde, v));
        q = v;  q *= -alpha; q += u;  // q = u - alpha * v;
        u += q;                       // u = u + q
        t = u; t *= alpha; x += t;    // x += alpha * u;
        multMatrixVector(A, u, t);    // t = A * u;
        t *= -alpha; r += t;          // r -= alpha * A * u;
        rho_2 = rho_1;
        residue_ = r.norm2() / normOfB;
        numberOfIterations_++;
        printIteration();
      }
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
      trace_p->pop();
    }

//    Version coming from Joly, Pascal & Meurant, Gérard. (1993). Complex Conjugate Gradient methods. Numerical Algorithms. 4. 379-406. 10.1007/BF02145754.
//    suggests using real(hermitianProduct(u,v)) in the calculation of rho, alpha, beta, but is less efficient than using the real scalar product (u,v) !?
//    template<typename K, class Mat, class VecB, class VecX>
//    void algorithm(Mat& A, VecB& b, VecX& x, VecX& r)
//    {
//      trace_p->push(name_ + "algorithm");
//      K rho(1.), alpha(1.), beta(1.);
//      VecX p(r), q(r) , u(r), v(r), t(r);
//
//      real_t normOfB = b.norm2();
//      if ( normOfB< theBreakdownThreshold ) { normOfB = 1.; }
//      residue_ = r.norm2() / normOfB;
//      if (residue_ < epsilon_)
//      {
//        trace_p->pop();
//        return;
//      }
//      VecX r0 = r;
//      //assign(rho, realPart(hermitianProduct(r, r0)));
//      assign(rho, dotRC(r, r0));
//      if (std::abs(rho) < theBreakdownThreshold) breakdown(std::abs(rho),"rho");
//      thePrintStream<<std::setprecision(10);
//      while ( numberOfIterations_ < maxOfIterations_  && residue_  > epsilon_)
//      {
//         multMatrixVector(A, q, v);    // v = A * q
//         //assign(alpha, rho / realPart(hermitianProduct(v, r0)));
//         assign(alpha, rho / dotRC(v, r0));
//         thePrintStream<<"iter "<<numberOfIterations_<<" : rho="<<rho<<" alpha="<<alpha;
//         u=v; u*=-alpha; u+=p;         // u=p-alpha*v=p-alpha*A*q
//         t = u; t+=p; t*=alpha; x+=t;  // x += alpha(p+u)
//         multMatrixVector(A, t, v);    // v = A * t
//         r-=v;
//         beta = 1. / rho;
//         //assign(rho, realPart(hermitianProduct(r, r0)));
//         assign(rho, dotRC(r, r0));
//         if (std::abs(rho) < theBreakdownThreshold) breakdown(std::abs(rho),"rho");
//         beta*=rho;
//         thePrintStream<<" new rho="<<rho<<" beta="<<beta;
//         p=u; p*=beta; p+=r;           // p=r+beta*u
//         q *= beta; q += u; q *= beta; q += p;  // q = p + beta* (u + beta * q);
//         residue_ = r.norm2() / normOfB;
//         thePrintStream<<" residue="<<residue_<<eol<<std::flush;
//         numberOfIterations_++;
//         printIteration();
//      }
//      trace_p->pop();
//    }


    /*!
       Solve matA.vecX=vecB using CGS method with an optional preconditioner.
       coming from Barrett et all. (1996)."Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods."
       Mathematics of Computation. 64. 10.2307/2153507.
       Typename K (K: real or complex) is the type of the initial residual vector
           and thus of auxiliary vectors as matrices A, P, b
           and vector x might not share the same type
    */
    template<typename K, class Mat, class VecB, class VecX, class Prec>
    void algorithm(Mat& A, VecB& b, VecX& x, VecX& r, Prec& M)
    {
      trace_p->push(name_ + "algorithm");
      K rho_1(1), rho_2(1), alpha(1), beta(1);
      VecX p(r), q(r) , u(r), v(r), phat(r), qhat(r), uhat(r), vhat(r);
      real_t normOfB = b.norm2();
      if ( normOfB< theBreakdownThreshold ) { normOfB = 1.; }
      residue_ = b.norm2() / normOfB;
      if ((residue_) < epsilon_){trace_p->pop(); return;}
      VecX rtilde = r;
      while ( ( numberOfIterations_ < maxOfIterations_ ) && (residue_  > epsilon_ ) )
      {
        assign(rho_1, dotRC(rtilde, r));
        if (std::abs(rho_1) < theBreakdownThreshold) breakdown(std::abs(rho_1),"rho_1");
        if (numberOfIterations_==0) { u = r;  p = u; }
        else
        {
          beta = rho_1 / rho_2;
          u = q ;  u *= beta;  u += r;           // u = r + beta * q;
          p *= beta; p += q; p *= beta; p += u;  // p = u + beta* (q + beta * p);
        }
        M.solve(p, phat);                  // preconditioner phat = M^{-1}p
        multMatrixVector(A, phat, vhat);   // vhat = A * phat
        assign(alpha, rho_1 / dotRC(rtilde, vhat));
        q = vhat;  q *= -alpha; q += u;    // q = u - alpha * vhat;
        u += q;                            // u = u + q
        M.solve(u, uhat);                  // preconditioner uhat = M^{-1}(u+q)
        uhat *= alpha; x += uhat;          // x += alpha * uhat;
        multMatrixVector(A, uhat, qhat);   // qhat = A * (alpha*uhat);
        r -= qhat;                         // r -= qhat;
        rho_2 = rho_1;
        residue_ = r.norm2() / normOfB;
        numberOfIterations_++;
        printIteration();
      }
      IterativeSolver::theNumberOfIterations=numberOfIterations_;
      IterativeSolver::theResidue=residue_;
      trace_p->pop();
    }
}; // end of class CgsSolver

} // namespace xlifepp

#endif /* CGS_SOLVER_HPP */
