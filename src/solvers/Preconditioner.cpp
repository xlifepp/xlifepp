/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Preconditioner.cpp
  \author C. Chambeyron, D. Martin, Ha NGUYEN, N. Kielbasiewicz
  \since 17 Nov 2016
  \date  17 Nov 2016

  \brief Implementation of xlifepp::Preconditioner class members and related functions
*/

#include "Preconditioner.hpp"

namespace xlifepp
{

ValueType Preconditioner::valueType() const { return _real; }
void Preconditioner::solve(TermVector& B, TermVector& X) const {}
void Preconditioner::solve(VectorEntry& B, VectorEntry& X) const {}
void Preconditioner::leftSolve(TermVector& B, TermVector& X) const {}
void Preconditioner::leftSolve(VectorEntry& B, VectorEntry& X) const {}
void Preconditioner::toScalar(bool keepEntries) {}
void Preconditioner::toGlobal(StorageType st, AccessType at, SymType symt, bool keepSuTerms) {}

} // namespace xlifepp
