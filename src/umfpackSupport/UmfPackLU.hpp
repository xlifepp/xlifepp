/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file UmfPackLU.hpp
  \authors Manh Ha NGUYEN
  \since 29 August 2013
  \date 13 Septembre 2013

  \brief Definition of xlifepp::UmfPackLU class

  allows to extract (LU) factorization and to solve system
  MatrixType has to be a class of matrices providing:
      ScalarType:  static member defining the type of values (real_t or complex_t)
      nbRows: member returning number of rows as number_t
      nbCols: member returning number of columns as number_t
      extract2UmfPack: member function giving colPtr_,rowIdx_,values_ (CSC storage like)
                        as std::vector<int>&, std::vector<int>&, std::vector<ScalarType>&
*/

#ifndef XLIFEPP_UMFPACKLU_HPP
#define XLIFEPP_UMFPACKLU_HPP

#include "utils.h"
#include "UmfPack.hpp"

namespace xlifepp
{

/*!
  \class UmfPackLU
  Wrapper class of Umfpack
*/
template<typename MatrixType>
class UmfPackLU
{
public:
    typedef typename MatrixType::ScalarType ScalarType;    //!< scalar type of matrix (real_t or Complex)

    //! structure to store matrix factors L and U
    struct LUMatrixType {
        std::vector<int> outerIdx;
        std::vector<int> innerIdx;
        std::vector<ScalarType> values;
    };

protected:
  // cached data to reduce reallocation, etc.
  mutable LUMatrixType lMatrix_;         //!< L factor
  mutable LUMatrixType uMatrix_;         //!< U factor
  mutable std::vector<int> pPerm_;       //!< row permutation vector
  mutable std::vector<int> qPerm_;       //!< col permutation vector
  mutable std::vector<real_t> rScaleFact_;

  //matrix to be factorized
  number_t nbRow_;                         //!< number of rows
  number_t nbCol_;                         //!< number of columns
  std::vector<int_t> colPtr_;              //!< col pointer in CSC storage
  std::vector<int_t> rowIdx_;              //!< row index  in CSC storage
  std::vector<ScalarType> values_;       //!< values of matrix to factorize

  //umfpack structure
  void* numeric_;                        //!< numeric structure in umfpack
  void* symbolic_;                       //!< symbolic structure in umfpack
  mutable ComputationInfo info_;         //!< computing info: _success, _numericalIssue, _noConvergence, _invalidInput
  bool isInitialized_;                   //!< true if initialized
  int factorizationIsOk_;                //!< true if factorized
  int analysisIsOk_;                     //!< true if analyzed
  mutable bool extractedDataAreDirty_;   //!< true if data have to be extracted
  mutable UMFPACK<int_t, ScalarType> umfpack_;  //!< UMFPACK object (wrapper to umfpack lib)
  real_t cond_;

public:
    UmfPackLU()                          //! void constructor
      { init(); }
    UmfPackLU(const MatrixType& matrix)  //! constructor from matrix, perform factorisation
      { init(); compute(matrix); }
    ~UmfPackLU()                         //! destructor (deallocate umfpack structure)
    {
      if(symbolic_) umfpack_.freeSymbolic(&symbolic_);
      if(numeric_)  umfpack_.freeNumeric(&numeric_);
    }

    inline number_t rows() const              //! accessor to number of matrix rows
    { return (nbRow_); }
    inline number_t cols() const              //! accessor to number of matrix columns
    { return (nbCol_); }

    /*! \brief Reports whether previous computation was successful.
      *
      * \returns \c Success if computation was succesful,
      *          \c NumericalIssue if the matrix.appears to be negative.
      */
    ComputationInfo info() const
    {
      UmfPackLUError(isInitialized_ , "Decomposition is not initialized.");
      return (info_);
    }
    real_t cond() const {return cond_;}

    inline const LUMatrixType& matrixL() const   //! access to L factor
    {
      if (extractedDataAreDirty_) extractData();
      return (lMatrix_);
    }

    inline const LUMatrixType& matrixU() const   //! access to U factor
    {
      if (extractedDataAreDirty_) extractData();
      return (uMatrix_);
    }

    inline const std::vector<int>& permutationP() const  //! access to row permutation vector
    {
      if (extractedDataAreDirty_) extractData();
      return (pPerm_);
    }

    inline const std::vector<int>& permutationQ() const  //! access to col permutation vector
    {
      if (extractedDataAreDirty_) extractData();
      return (qPerm_);
    }

    inline const std::vector<real_t>& scaleFactorR() const //! access to scale factors vector
    {
      if (extractedDataAreDirty_) extractData();
      return (rScaleFact_);
    }

    /*! Computes the sparse Cholesky decomposition of a matrix  */
    void compute(const MatrixType& matrix)
    {
      analyzePattern(matrix);  //symbolic factorisation
      factorize(matrix);       //numeric factorisation
    }

    /*! Performs a symbolic decomposition on the sparcity of \a matrix.
      * This function is particularly useful when solving several problems having the same structure.
      * \sa factorize(), compute()
      */
    void analyzePattern(const MatrixType& matrix)
    {
      if(symbolic_)  umfpack_.freeSymbolic(&symbolic_);
      if(numeric_)   umfpack_.freeNumeric(&numeric_);

      grapInput(matrix);

      int errorCode = 0;
      errorCode = umfpack_.symbolic(rows(), cols(), &colPtr_[0], &rowIdx_[0], &values_[0],
                                   &symbolic_, 0, 0);

      isInitialized_ = true;
      info_ = errorCode ? _invalidInput: _success;
      analysisIsOk_ = true;
      factorizationIsOk_ = false;
    }

    /*! Performs a numeric decomposition of \a matrix
      * The given matrix must has the same matrix on which the pattern analysis has been performed.
      * \sa analyzePattern(), compute()
      */
    void factorize(const MatrixType& matrix)
    {
      UmfPackLUError(analysisIsOk_, "UmfPackLU: you must first call analyzePattern()");
      if(numeric_) umfpack_.freeNumeric(&numeric_);
      int errorCode;
      int_t ni= UMFPACK<int,double>::infoSize;
      std::vector<double> info(ni);
      errorCode = umfpack_.numeric(&colPtr_[0], &rowIdx_[0], &values_[0], symbolic_, &numeric_, 0, &(info[0]));
      info_ = errorCode ? _numericalIssue: _success;
      factorizationIsOk_ = true;
      if(info[67]>0) cond_=1./info[67];else cond_=theRealMax;
    }

    template<typename VectorScalarType>
    void solve(const std::vector<VectorScalarType>& b,
               std::vector<typename Conditional<NumTraits<ScalarType>::IsComplex, ScalarType, VectorScalarType>::type >& x) const;

    void extractData() const;
    ScalarType determinant() const;

protected:
  void init()  //! initialize object at construction
  {
    info_ = _invalidInput;
    isInitialized_ = false;
    numeric_ = 0;
    symbolic_ = 0;
    colPtr_.resize(0);
    rowIdx_.resize(0);
    values_.resize(0);
    extractedDataAreDirty_ = true;
    cond_=0.;
  }

  void grapInput(const MatrixType& mat)  //! load matrix
  {
      nbRow_ = mat.nbRows;
      nbCol_ = mat.nbCols;
      mat.extract2UmfPack(colPtr_,rowIdx_,values_);
  }

private:
  template<typename VectorScalarType>
  void solveUmf(const std::vector<VectorScalarType>& b,
          std::vector<typename Conditional<NumTraits<ScalarType>::IsComplex, ScalarType, VectorScalarType>::type >& x,
          Int2Type<0>) const  //! solve LUx=b   L, U, b Real
  {
      (void)umfpack_.solve((int)0, &(colPtr_[0]), &(rowIdx_[0]),&(values_[0]), &x[0], &b[0], numeric_, 0, 0);
  }

  template<typename VectorScalarType>
  void solveUmf(const std::vector<VectorScalarType>& b,
          std::vector<typename Conditional<NumTraits<ScalarType>::IsComplex, ScalarType, VectorScalarType>::type >& x,
          Int2Type<1>) const  //! solve LUx=b   L, U Real, b Complex
  {
      typedef typename NumTraits<VectorScalarType>::RealScalar RealScalar;
      std::vector<RealScalar> xTmp(x.size());
      std::vector<RealScalar> bTmp(b.size());
      int size = b.size();
      for (int i = 0; i < size; i++) bTmp[i] = NumTraits<VectorScalarType>::realPart(b[i]);
      (void)umfpack_.solve((int)0, &(colPtr_[0]), &(rowIdx_[0]),&(values_[0]), &xTmp[0], &bTmp[0], numeric_, 0, 0);
      for (int i = 0; i < size; i++)
      {
	     x[i] = VectorScalarType(xTmp[i], x[i].imag());
	     bTmp[i] = NumTraits<VectorScalarType>::imagPart(b[i]);
      }
      (void)umfpack_.solve((int)0, &(colPtr_[0]), &(rowIdx_[0]),&(values_[0]), &xTmp[0], &bTmp[0], numeric_, 0, 0);
      for (int i = 0; i < size; i++)
      {
	    x[i]=VectorScalarType(x[i].real(),xTmp[i]);
      }
  }

  template<typename VectorScalarType>
  void solveUmf(const std::vector<VectorScalarType>& b,
          std::vector<typename Conditional<NumTraits<ScalarType>::IsComplex, ScalarType, VectorScalarType>::type >& x,
          Int2Type<2>) const  //! solve LUx=b  L, U Complex, b Real
  {
      std::vector<VectorScalarType> bImg(b.size());
      (void)umfpack_.solveCR((int)0, &(colPtr_[0]), &(rowIdx_[0]),&(values_[0]), &x[0], &b[0], &bImg[0], numeric_, 0, 0);
  }

  template<typename VectorScalarType>
  void solveUmf(const std::vector<VectorScalarType>& b,
          std::vector<typename Conditional<NumTraits<ScalarType>::IsComplex, ScalarType, VectorScalarType>::type >& x,
          Int2Type<3>) const  //! solve LUx=b  //!< solve LUx=b  L, U, b Complex
  {
      (void)umfpack_.solve((int)0, &(colPtr_[0]), &(rowIdx_[0]),&(values_[0]), &x[0], &b[0], numeric_, 0, 0);
  }

private:
  UmfPackLU(UmfPackLU&);                         //!< copy constructor forbidden
  const UmfPackLU& operator=(const UmfPackLU&);  //!< assign operator forbidden

  void UmfPackLUError(bool condition, const string_t& s) const  //! error managment
  { if (!condition) { error("free_error", s); } }

};

template<typename MatrixType>
void UmfPackLU<MatrixType>::extractData() const
{
  if (extractedDataAreDirty_)
  {
    // get size of the data
    int lnz, unz, rows, cols, nz_udiag, doRecip;
    umfpack_.getLunz(&lnz, &unz, &rows, &cols, &nz_udiag, numeric_);

    // allocate data
    lMatrix_.outerIdx.resize(rows+1);
    lMatrix_.innerIdx.resize(lnz);
    lMatrix_.values.resize(lnz);

    uMatrix_.outerIdx.resize(cols+1);
    uMatrix_.innerIdx.resize(unz);
    uMatrix_.values.resize(unz);

    pPerm_.resize(rows);
    qPerm_.resize(cols);

    /* The first value of rScaleFact_  defines how the scale factors Rs are to be interpreted.
        If it's one, then the scale factors rScaleFact_ [i+1] are to be used
        by multiplying row i by rScaleFact_ [i].  Otherwise, the entries in row i are to
        be divided by rScaleFact_ [i+1]. */
    rScaleFact_.resize(rows+1);

    umfpack_.getNumeric(&lMatrix_.outerIdx[0], &lMatrix_.innerIdx[0], &lMatrix_.values[0],
                        &uMatrix_.outerIdx[0], &uMatrix_.innerIdx[0], &uMatrix_.values[0],
                        &pPerm_[0], &qPerm_[0], 0, &doRecip, &rScaleFact_[1], numeric_);
    if (doRecip) rScaleFact_[0] = 1.0;
    else rScaleFact_[0] = 0.0;

    extractedDataAreDirty_ = false;
  }
}

template<typename MatrixType>
typename UmfPackLU<MatrixType>::ScalarType UmfPackLU<MatrixType>::determinant() const
{
  ScalarType det;
  umfpack_.getDeterminant(&det, 0, numeric_, 0);
  return (det);
}

template<typename MatrixType> template<typename VectorScalarType>
void UmfPackLU<MatrixType>::solve(const std::vector<VectorScalarType>& b,
                                  std::vector<typename Conditional<NumTraits<ScalarType>::IsComplex, typename UmfPackLU<MatrixType>::ScalarType, VectorScalarType >::type >& x) const
{
    if (numeric_) {
        Int2Type<SolverChoice<NumTraits<ScalarType>::IsComplex, NumTraits<VectorScalarType>::IsComplex>::choice> solveType;
        solveUmf(b,x,solveType);
    }

}


}
#endif /* XLIFEPP_UMFPACKLU_HPP */

