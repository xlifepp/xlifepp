/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file UmfPackSolver.hpp
  \authors Manh Ha NGUYEN
  \since 29 Aug 2013
  \date  29 Aug 2013

  \brief Definition of the xlifepp::UmfPackSolver class

  Class xlifepp::UmfPackSolver only plays a role of a wrapper of UMFPACK
*/

#ifndef UMFPACK_SOLVER_HPP
#define UMFPACK_SOLVER_HPP

#include "utils.h"
#include "UmfPack.hpp"

namespace xlifepp
{

#define UMFPACK_CONTROL 20

/*!
     \class UmfPackSolver
     This class solve AX=B with Umfpack library and it provides a same interface as iterative solvers
     User can invoke this solver with the operator()
*/
class UmfPackSolver
{
  public:
    // Constructor and destructor
    UmfPackSolver(void) {} //!< default constructor
    ~UmfPackSolver(){} //!< destructor

    /*!
      Solve AX=B with UmfPack
      \param[in] matA matrix A (matrix A should be largeMatrix and its storage can be overwritten)
      \param[in] vecB vector B (vector B should be std::vector)
      \param[in] vecX vector X (vector X should be std::vector)
      \param[in] stopOnAbnormal boolean needed by the solIntern::solve call
      return the reciprocal condition number rcond = 1/||A||*||inv(A)|| in norm 1
    */
    template<class Mat, class VecB, class VecX>
    double operator()(const Mat& matA, const VecB& vecB, VecX& vecX, bool stopOnAbnormal = true)
    {
        typedef typename Mat::ScalarType ScalarTypeMat;
        typedef typename VectorTrait<VecB>::Type ScalarTypeVec;
        typedef Int2Type<SolverChoice<NumTraits<ScalarTypeMat>::IsComplex, NumTraits<ScalarTypeVec>::IsComplex>::choice> SolverType;

        // Get storage and values of matrix A
        std::vector<int_t> colPointer, rowIdx;         //pointer to cs col storage
        const ScalarTypeMat *values;                   //pointer to first matrix value
        std::vector<ScalarTypeMat>* mat=nullptr;
        bool byPointer = matA.getCsColUmfPack(colPointer, rowIdx, values);  //try to get values by pointer (ok if already in cs col)
        if(!byPointer)  //extract values
        {
            mat = new std::vector<ScalarTypeMat>();
            matA.extract2UmfPack(colPointer, rowIdx, *mat);
            values = &(*mat)[0];
        }

        // Prepare some arguments for UMFPack
        int_t nRows = (int_t)matA.nbRows;
        int_t nCols = (int_t)matA.nbCols;
        UmfPackComputationMode sys = _A;               //solve Ax=B
        SolverType solType;

        SolverIntern<ScalarTypeMat,VecB,VecX> solIntern;
        double rcond = solIntern.solve((int_t)sys, nRows, nCols, colPointer, rowIdx, values, vecB, vecX, solType, stopOnAbnormal);
        if(!byPointer) delete mat;
        return rcond;
    }

    //! multiple right hand sides
    template<class Mat, class VecB, class VecX>
    double operator()(const Mat& matA, const std::vector<VecB*>& bs, std::vector<VecX*>& xs, bool stopOnAbnormal = true)
    {
        typedef typename Mat::ScalarType ScalarTypeMat;
        typedef typename VectorTrait<VecB>::Type ScalarTypeVec;
        typedef Int2Type<SolverChoice<NumTraits<ScalarTypeMat>::IsComplex, NumTraits<ScalarTypeVec>::IsComplex>::choice> SolverType;

        // Get storage and values of matrix A
        std::vector<int_t> colPointer, rowIdx;         //pointer to cs col storage
        const ScalarTypeMat *values;                   //pointer to first matrix value
        std::vector<ScalarTypeMat>* mat=nullptr;
        bool byPointer = matA.getCsColUmfPack(colPointer, rowIdx, values); //try to get values by pointer (ok if already in cs col)
        if(!byPointer)   //extract values (more expansive)
        {
            mat = new std::vector<ScalarTypeMat>();
            matA.extract2UmfPack(colPointer, rowIdx, *mat);
            values = &(*mat)[0];
        }

        // Prepare some arguments for UMFPack
        int_t nRows = (int_t)matA.nbRows;
        int_t nCols = (int_t)matA.nbCols;
        UmfPackComputationMode sys = _A;
        SolverType solType;

        SolverIntern<ScalarTypeMat,VecB,VecX> solIntern;
        double rcond = solIntern.solve((int_t)sys, nRows, nCols, colPointer, rowIdx, values, bs, xs, solType, stopOnAbnormal);
        if(!byPointer) delete mat;
        return rcond;
    }

    /*!
      Solve AX=B with UmfPack
      \param[in] size matrix size
      \param[in] colPointer column pointer of CSC-like Umfpack format (after calling function extract2UmfPack of largeMatrix)
      \param[in] rowIdx row index of CSC-like Umfpack format (after calling function extract2UmfPack of largeMatrix)
      \param[in] values values of CSC-like Umfpack format (after calling function extract2UmfPack of largeMatrix)
      \param[in] vecB vector B (vector B should be std::vector)
      \param[in] vecX vector X (vector X should be std::vector)
      \param[in] sys computation mode for UmfPack
      \param[in] stopOnAbnormal bollean needed by the solIntern::solve call
      return the reciprocal condition number rcond = 1/||A||*||inv(A)|| in norm 1
    */
    template<class ScalarTypeMat, class VecB, class VecX>
    double operator()(int_t size, const std::vector<int_t>& colPointer, const std::vector<int_t>& rowIdx,
                      const ScalarTypeMat* values, const VecB& vecB, VecX& vecX,
                      UmfPackComputationMode sys=_A, bool stopOnAbnormal = true)
    {
        typedef typename VectorTrait<VecB>::Type ScalarTypeVec;
        Int2Type<SolverChoice<NumTraits<ScalarTypeMat>::IsComplex, NumTraits<ScalarTypeVec>::IsComplex>::choice> solType;

        int_t nCols = size;
        int_t nRows = nCols;
        SolverIntern<ScalarTypeMat,VecB,VecX> solIntern;
        return solIntern.solve((int_t)sys, nRows, nCols, colPointer, rowIdx, values, vecB, vecX, solType, stopOnAbnormal);
    }

private:
    /*!
      Auxiliary internal class to process 4 cases of equation AX = B:
           + A:real, B: real
           + A:real, B:complex
           + A:complex, B: real
           + A:complex, B:complex
    */
    template<typename ScalarTypeMat, typename VecB, typename VecX>
    class SolverIntern
    {
    public:

    //error analysis
    void statusInfo(int_t status, double rcond, bool stopOnAbnormal)
    {
    switch(status)
        {
        case 1: //warning: singular matrix
            {
            string_t mes = "warning singular matrix, small reciprocal condition number = "+tostring(rcond)+" in umfpack";
            if(stopOnAbnormal) error("free_error",mes); else warning("free_warning",mes);
            }
            break;
        case 2: //warning determinant underflow
            {
            string_t mes = "warning determinant underflow, small reciprocal condition number = "+tostring(rcond)+" in umfpack";
            if(stopOnAbnormal) error("free_error",mes); else warning("free_warning",mes);
            }
            break;
        case 3: //warning determinant overflow
            {
            string_t mes = "warning determinant overflow, small reciprocal condition number = "+tostring(rcond)+" in umfpack";
            if(stopOnAbnormal) error("free_error",mes); else warning("free_warning",mes);
            }
            break;
        case -1: //out of memory
            {
            string_t mes = "out of memory in umfpack";
            if(stopOnAbnormal) error("free_error",mes); else warning("free_warning",mes);
            }
            break;
         default:
             if(status<0)
            {
            string_t mes = "abnormal factorisation in umfpack, error code = "+tostring(status);
            if(stopOnAbnormal) error("free_error",mes); else warning("free_warning",mes);
            }
        }
    }
    //@{
    //! useful typedefs for SolverIntern class
    typedef typename NumTraits<ScalarTypeMat>::magnitudeType MagType;
    typedef typename Conditional<NumTraits<ScalarTypeMat>::IsComplex, ScalarTypeMat, typename VectorTrait<VecB>::Type>::type ScalarType;
    //@}

    SolverIntern() {} //!< constructor

    //! Solve AX=B in case A Real, B Real
    double solve(int_t sys, int_t nRows, int_t nCols,
              const std::vector<int_t>& colPointer, const std::vector<int_t>& rowIdx, const ScalarTypeMat* values,
              const VecB& vecB, VecX& vecX,
              Int2Type<0>, bool stopOnAbnormal = true)
    {
        void* symbolic;
        void* numeric;
        double* null = (double*) nullptr;
        // double Mx=0, Ex=0;
        int_t status=0;
        UMFPACK<int_t, ScalarType> umfPack;
        int_t ni= UMFPACK<int, double>::infoSize, nc=UMFPACK<int, double>::controlSize;
        std::vector<double> info(ni), control(nc);

        //LU factorization
        status = umfPack.symbolic(nRows, nCols, &(colPointer[0]), &(rowIdx[0]), values, &symbolic, null, null);
        status = umfPack.numeric(&(colPointer[0]), &(rowIdx[0]), values, symbolic, &numeric, null, &(info[0]));
        double rcond=info[67];
        statusInfo(status, rcond, stopOnAbnormal);

        //solve system
        status = umfPack.solve((int_t)sys, &(colPointer[0]), &(rowIdx[0]), values, &vecX[0], &vecB[0], numeric, null, null);

        //free ressources
        umfPack.freeSymbolic(&symbolic);
        umfPack.freeNumeric(&numeric);

        return rcond;
    }

    //! Solve AX=B in case A Real, B Complex
    double solve(int_t sys, int_t nRows, int_t nCols,
              const std::vector<int_t>& colPointer, const std::vector<int_t>& rowIdx, const ScalarTypeMat* values,
              const VecB& vecB, VecX& vecX,
              Int2Type<1>, bool stopOnAbnormal = true)
    {
        void* symbolic;
        void* numeric;
        double* null = (double*) nullptr;
        // double Mx=0, Ex=0;
        int_t status = 0;

        // Use real version of UmfPack to solve A(Xr+iXi) = Br + iBi: solve AXr = Br and AXi = Bi;
        UMFPACK<int_t, ScalarTypeMat> umfPack;
        std::vector<ScalarTypeMat> B(vecB.size());
        std::vector<ScalarTypeMat> X(vecX.size());
        std::vector<double> info(UMFPACK<int, double>::infoSize), control(UMFPACK<int, double>::controlSize);

        //LU factorization
        status = umfPack.symbolic(nRows, nCols, &(colPointer[0]), &(rowIdx[0]), values, &symbolic, null, null);
        status = umfPack.numeric(&(colPointer[0]), &(rowIdx[0]), values, symbolic, &numeric, null, &(info[0]));
        double rcond=info[67];
        statusInfo(status, rcond, stopOnAbnormal);

        // Firstly, solve AXr = Br;
        extractPart(vecB, B, false);
        status = umfPack.solve((int_t)sys, &(colPointer[0]), &(rowIdx[0]),values, &X[0], &B[0], numeric, null, null);
        writePart(X,vecX, false);
        // Then, solve AXi = Bi;
        extractPart(vecB, B, true);
        status = umfPack.solve((int_t)sys, &(colPointer[0]), &(rowIdx[0]),values, &X[0], &B[0], numeric, null, null);
        writePart(X,vecX, true);

        //free ressources
        umfPack.freeSymbolic(&symbolic);
        umfPack.freeNumeric(&numeric);

        return rcond;
    }

    //! Solve AX=B in case A Complex, B Real
    double solve(int_t sys, int_t nRows, int_t nCols,
              const std::vector<int_t>& colPointer, const std::vector<int_t>& rowIdx, const ScalarTypeMat* values,
              const VecB& vecB, VecX& vecX,
              Int2Type<2>, bool stopOnAbnormal = true)
    {
        void* symbolic;
        void* numeric;
        double* null = (double*) nullptr;
        // double Ex=0;
        // complex_t Mx=0;
        int_t status=0;
        UMFPACK<int_t, ScalarType> umfPack;
        std::vector<typename VectorTrait<VecB>::Type> imagB(vecB.size());
        std::vector<double> info(UMFPACK<int, double>::infoSize), control(UMFPACK<int, double>::controlSize);

        //LU factorization
        status = umfPack.symbolic(nRows, nCols, &(colPointer[0]), &(rowIdx[0]),values,&symbolic, null, null);
        status = umfPack.numeric(&(colPointer[0]), &(rowIdx[0]), values, symbolic, &numeric, null, &(info[0]));
        double rcond=info[67];
        statusInfo(status, rcond, stopOnAbnormal);

        //solve system
        status = umfPack.solveCR((int_t)sys, &(colPointer[0]), &(rowIdx[0]),values, &vecX[0], &vecB[0], &imagB[0], numeric, null, null);

        //free ressources
        umfPack.freeSymbolic(&symbolic);
        umfPack.freeNumeric(&numeric);

        return rcond;
    }

    //! Solve AX=B in case A Complex, B Complex
    double solve(int_t sys, int_t nRows, int_t nCols,
              const std::vector<int_t>& colPointer, const std::vector<int_t>& rowIdx, const ScalarTypeMat* values,
              const VecB& vecB, VecX& vecX,
              Int2Type<3>, bool stopOnAbnormal = true)
    {
        void* symbolic;
        void* numeric;
        double* null = (double*) nullptr;
        // double Ex=0;
        // complex_t Mx=0;
        int_t status = 0;
        UMFPACK<int_t, ScalarType> umfPack;
        std::vector<double> info(UMFPACK<int, double>::infoSize), control(UMFPACK<int, double>::controlSize);

        //LU factorization
        status = umfPack.symbolic(nRows, nCols, &(colPointer[0]), &(rowIdx[0]),values,&symbolic, null, null);
        status = umfPack.numeric(&(colPointer[0]), &(rowIdx[0]), values, symbolic, &numeric, null, &(info[0]));
        double rcond=info[67];
        statusInfo(status, rcond, stopOnAbnormal);

        //solve system
        status = umfPack.solve((int_t)sys, &(colPointer[0]), &(rowIdx[0]),values, &vecX[0], &vecB[0], numeric, null, null);
        //cpuTime("solve in umfpack");
        //free ressources
        umfPack.freeSymbolic(&symbolic);
        umfPack.freeNumeric(&numeric);

        return rcond;
    }

    //! multiple right hand sides. Solve AX=B in case A Real, B Real
    double solve(int_t sys, int_t nRows, int_t nCols,
              const std::vector<int_t>& colPointer, const std::vector<int_t>& rowIdx, const ScalarTypeMat* values,
              const std::vector<VecB*>& bs, std::vector<VecX*>& xs,
              Int2Type<0>, bool stopOnAbnormal = true)
    {
        void* symbolic;
        void* numeric;
        double* null = (double*) nullptr;
        // double Mx=0, Ex=0;
        int_t status=0;
        UMFPACK<int_t, ScalarType> umfPack;
        int_t ni= UMFPACK<int, double>::infoSize, nc=UMFPACK<int, double>::controlSize;
        std::vector<double> info(ni), control(nc);

        //LU factorization
        status = umfPack.symbolic(nRows, nCols, &(colPointer[0]), &(rowIdx[0]),values,&symbolic, null, null);
        status = umfPack.numeric(&(colPointer[0]), &(rowIdx[0]), values, symbolic, &numeric, null, &(info[0]));
        double rcond=info[67];
        statusInfo(status, rcond, stopOnAbnormal);

        //solve systems
        int_t ns=bs.size();
        for(int_t k=0; k<ns; k++)
        {
           status = umfPack.solve((int_t)sys, &(colPointer[0]), &(rowIdx[0]),values, &(*xs[k])[0], &(*bs[k])[0], numeric, null, null);
        }

        //free ressources
        umfPack.freeSymbolic(&symbolic);
        umfPack.freeNumeric(&numeric);

        return rcond;
    }

    //! Solve AX=B in case A Real, B Complex
    double solve(int_t sys, int_t nRows, int_t nCols,
              const std::vector<int_t>& colPointer, const std::vector<int_t>& rowIdx, const ScalarTypeMat* values,
              const std::vector<VecB*>& bs, std::vector<VecX*>& xs,
              Int2Type<1>, bool stopOnAbnormal = true)
    {
        void* symbolic;
        void* numeric;
        double* null = (double*) nullptr;
        // double Mx=0, Ex=0;
        int_t status = 0;
        // Use real version of UmfPack to solve A(Xr+iXi) = Br + iBi: solve AXr = Br and AXi = Bi;
        UMFPACK<int_t, ScalarTypeMat> umfPack;
        std::vector<double> info(UMFPACK<int, double>::infoSize), control(UMFPACK<int, double>::controlSize);

        //LU factorization
        status = umfPack.symbolic(nRows, nCols, &(colPointer[0]), &(rowIdx[0]),values,&symbolic, null, null);
        status = umfPack.numeric(&(colPointer[0]), &(rowIdx[0]), values, symbolic, &numeric, null, &(info[0]));
        double rcond=info[67];
        statusInfo(status, rcond, stopOnAbnormal);

        //solve systems
        std::vector<ScalarTypeMat> B(bs[0]->size());
        std::vector<ScalarTypeMat> X(xs[0]->size());
        int_t ns=bs.size();
        for(int_t k=0; k<ns; k++)
        {
          // Firstly, solve AXr = Br;
          extractPart(*bs[k], B, false);
          status = umfPack.solve((int_t)sys, &(colPointer[0]), &(rowIdx[0]),values, &X[0], &B[0], numeric, null, null);
          writePart(X,*xs[k], false);
          // Then, solve AXi = Bi;
          extractPart(*bs[k], B, true);
          status = umfPack.solve((int_t)sys, &(colPointer[0]), &(rowIdx[0]),values, &X[0], &B[0], numeric, null, null);
          writePart(X,*xs[k],true);
        }

        //free ressources
        umfPack.freeSymbolic(&symbolic);
        umfPack.freeNumeric(&numeric);

        return rcond;
    }

    //! Solve AX=B in case A Complex, bs Real, xs should be of Complex type
    double solve(int_t sys, int_t nRows, int_t nCols,
              const std::vector<int_t>& colPointer, const std::vector<int_t>& rowIdx, const ScalarTypeMat* values,
              const std::vector<VecB*>& bs, std::vector<VecX*>& xs,
              Int2Type<2>, bool stopOnAbnormal = true)
    {
        void* symbolic;
        void* numeric;
        double* null = (double*) nullptr;
        // double Ex=0;
        // complex_t Mx=0;
        int_t status=0;
        UMFPACK<int_t, ScalarType> umfPack;
        std::vector<double> info(UMFPACK<int, double>::infoSize), control(UMFPACK<int, double>::controlSize);

        //LU factorization
        status = umfPack.symbolic(nRows, nCols, &(colPointer[0]), &(rowIdx[0]),values,&symbolic, null, null);
        status = umfPack.numeric(&(colPointer[0]), &(rowIdx[0]), values, symbolic, &numeric, null, &(info[0]));
        double rcond=info[67];
        statusInfo(status,rcond,stopOnAbnormal);

        //solve systems
        std::vector<typename VectorTrait<VecB>::Type> imagB(bs[0]->size());
        int_t ns=bs.size();
        for(int_t k=0; k<ns; k++)
        {
          status = umfPack.solveCR((int_t)sys, &(colPointer[0]), &(rowIdx[0]),values, &(*xs[k])[0], &(*bs[k])[0], &imagB[0], numeric, null, null);
        }

        //free ressources
        umfPack.freeSymbolic(&symbolic);
        umfPack.freeNumeric(&numeric);

        return rcond;
    }

    //! Solve AX=B in case A Complex, B Complex
    double solve(int_t sys, int_t nRows, int_t nCols,
              const std::vector<int_t>& colPointer, const std::vector<int_t>& rowIdx, const ScalarTypeMat* values,
              const std::vector<VecB*>& bs, std::vector<VecX*>& xs,
              Int2Type<3>, bool stopOnAbnormal = true)
    {
        void* symbolic;
        void* numeric;
        double* null = (double*) nullptr;
        // double Ex=0;
        // complex_t Mx=0;
        int_t status = 0;
        UMFPACK<int_t, ScalarType> umfPack;
        std::vector<double> info(UMFPACK<int, double>::infoSize), control(UMFPACK<int, double>::controlSize);

        //LU factorization
        status = umfPack.symbolic(nRows, nCols, &(colPointer[0]), &(rowIdx[0]),values,&symbolic, null, null);
        //cpuTime("umfPack.symbolic");
        status = umfPack.numeric(&(colPointer[0]), &(rowIdx[0]), values, symbolic, &numeric, null, &(info[0]));
        //cpuTime("umfPack.numeric");
        double rcond=info[67];
        statusInfo(status, rcond, stopOnAbnormal);

        //solve systems
        int_t ns=bs.size();
        for(int_t k=0; k<ns; k++)
        {
          status = umfPack.solve((int_t)sys, &(colPointer[0]), &(rowIdx[0]),values, &(*xs[k])[0], &(*bs[k])[0], numeric, null, null);
        }

        //free ressources
        umfPack.freeSymbolic(&symbolic);
        umfPack.freeNumeric(&numeric);

        return rcond;
    }

    private:
    /*!
      Extract real/imagine part of vector into a real vector
      \param[in] vec vector need extracting
      \param[in,out] extractedPart vector contains extracted values
      \param[in] imag flag to chose which part is extracted (real/imagine)
    */
    template<typename Vec>
    void extractPart(const Vec& vec, std::vector<double>& extractedPart, bool imag=true)
    {
        std::vector<double>::iterator itDest = extractedPart.begin();
        typename Vec::const_iterator itVec = vec.begin();
        if (imag)
            for (itVec = vec.begin(); itVec != vec.end(); itVec++, itDest++) {
                *itDest = (*itVec).imag();
            }
        else
            for (itVec = vec.begin(); itVec != vec.end(); itVec++, itDest++) {
                *itDest = (*itVec).real();
            }
    }

    /*!
      Write real/imagine part of vector with a real vector
      \param[in] sourcePart vector contains (real/imaginary) part to be written
      \param[in,out] vec vector to be written
      \param[in] imag flag to chose which part is extracted (real/imagine)
    */
    template<typename Vec>
    void writePart(const std::vector<double>& sourcePart, Vec& vec, bool imag=true)
    {
        std::vector<double>::const_iterator itSource;
        typename Vec::iterator itVec = vec.begin();
        if (imag)
            for (itSource = sourcePart.begin(); itSource != sourcePart.end(); itSource++, itVec++) {
	      (*itVec) = complex_t( (*itVec).real(), *itSource); //(*itVec).imag() = *itSource;
            }
        else
            for (itSource = sourcePart.begin(); itSource != sourcePart.end(); itSource++, itVec++) {
	      (*itVec) = complex_t(*itSource, (*itVec).imag());  //(*itVec).real() = *itSource;
            }
    }

    };
}; // end of class UmfPackSolver ===================================================

} // namespace xlifepp

#endif /* UMFPACK_SOLVER_HPP */

/* ------------------------------------------------------------------------------------------------------------------
                   UMFPACK INFO structure returned (currently not managed)
   ------------------------------------------------------------------------------------------------------------------
For numeric:
Info[UMFPACK_STATUS]: status code
  - UMFPACK_OK (0): Numeric factorization was successful. numeric computed a valid numeric factorization.
  - UMFPACK_WARNING_singular_matrix (1):  Numeric factorization was successful, but the matrix is singular. Numeric computed a valid numeric factorization, but you will get a divide by zero in solve.
  - UMFPACK WARNING_determinant_overflow (3): The determinant is larger in magnitude than the largest positive oating-point number (IEEE Inf)
  - UMFPACK_ERROR_out_of_memory (-1) : Insufficient memory to complete the numeric factorization.
  - UMFPACK ERROR_invalid_Numeric_object (-3): Routines that take a Numeric object as input check this object and return this error code if it is invalid.
  - UMFPACK ERROR_invalid_Symbolic_object(-4): Routines that take a Symbolic object as input  check this object and return this error code if it is invalid
  - UMFPACK_ERROR_argument_missing (-5) : Some arguments of some are optional (you can passa nullptr pointer instead of an array).
  - UMFPACK ERROR n nonpositive (-6): The number of rows or columns of the matrix must be greater than zero.
  - UMFPACK ERROR invalid matrix (-8): The matrix is invalid. For the column-oriented input,this error code will occur if the contents of Ap and/or Ai are invalid.
  - UMFPACK ERROR different pattern, (-11): The most common cause of this error is that the pattern of the matrix has changed between the symbolic and numeric factorization.
  - UMFPACK ERROR invalid system (-13): The sys argument provided to one of the solve routines is invalid.
  - UMFPACK ERROR invalid permutation (-15): The permutation vector provided as input is invalid.
  - UMFPACK ERROR file IO (-17): This error code is returned by the routines that save and load the Numeric or Symbolic objects to/from a file
  - UMFPACK ERROR ordering failed (-18): The ordering method failed.
  - UMFPACK ERROR internal error (-911): An internal error has occurred, of unknown cause.

Info[UMFPACK_NUMERIC_SIZE]:
  the actual final size (in Units) of the entire Numeric object, including the final size of the variable part of the object.
Info [UMFPACK_PEAK_MEMORY]:
  the actual peak memory usage (in Units) of both symbolic and numeric.
Info [UMFPACK_FLOPS]:
  the actual count of the (useful) floating-point operations performed excluding "useless" flops on zero values
Info [UMFPACK_LNZ]:
  the actual nonzero entries in final factor L,including the diagonal. This excludes any zero entries in L,although some of these are stored in the Numeric object. The
Info [UMFPACK_UNZ]:
  the actual nonzero entries in final factor U,including the diagonal. This excludes any zero entries in U,although some of these are stored in the Numeric object. The
Info [UMFPACK_NUMERIC_TIME]:
  the CPU time taken, in seconds.
  */


