/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file UmfPack.hpp
  \authors Manh Ha NGUYEN
  \since 29 August 2013
  \date 29 August 2013

  \brief Wrapper of some important functions of UMFPACK in order to solver (non)-symmetric problem
*/

#ifndef XLIFEPP_UMFPACK_HPP
#define XLIFEPP_UMFPACK_HPP

#include "utils.h"
#include "UmfPackTypeDefs.hpp"

namespace xlifepp
{
/*!
  \enum UmfPackComputationMode
  Enum for computation mode of UmfPack.
*/
enum UmfPackComputationMode
{
  //! Computation Ax[]=b.
  _A = 0,
  //! Computation A'x=b.
  _At = 1,
  //! Computation A.'x=b.
  _Aat = 2,
  //! Computation P'Lx=b.
  _Pt_L = 3,
  //! Computation Lx=b.
  _L = 4,
  //! Computation L'Px=b.
  _Lt_P = 5,
  //! Computation L.'Px=b.
  _Lat_P = 6,
  //! Computation L'x=b.
  _Lt = 7,
  //! Computation L.'x=b.
  _Lat = 8,
  //! Computation UQ'x=b.
  _U_Qt = 9,
  //! Computation Ux=b.
  _U = 10,
  //! Computation QU'x=b.
  _Q_Ut = 11,
  //! Computation QU.'x=b.
   _Q_Uat = 12,
  //! Computation U'x=b.
  _Ut = 13,
  //! Computation U.'x=b.
  _Uat = 14
};

template<class T>
struct UndefinedUMFPACKRoutine
{
  //! This function should not compile if there is an attempt to instantiate!
  static inline T notDefined() { return T::UMFPACK_routine_not_defined_for_this_type(); }
};

/*!
  \class UMFPACK
  This class plays a role of wrapper of Umfpack library. And only necessary functions are provided here.
  However, we we can easily extend this class with new methods wrapping functions of Umfpack library.
  Because Umfpack only works with int/double, (e.x umfpack_di_symbolic, etc) or long/double, (e.x umfpack_dl_symbolic)
  Class template specialization approach seems to be the best!
*/
template<typename OrdinalType, typename ScalarType>
class UMFPACK
{
  public:
    //! useful typedefs for UMFPACK class
    typedef typename NumTraits<ScalarType>::magnitudeType MagnitudeType;

    //! Default Constructor.
    inline UMFPACK(void) {}

    //! Destructor.
    inline ~UMFPACK(void) {}

    //! @name Primary routines
    //@{
    OrdinalType symbolic(OrdinalType nRow, OrdinalType nCol,
                         const OrdinalType Ap[], const OrdinalType Ai[],
                         const ScalarType Ax[], void** Symbolic,
                         const MagnitudeType Control[UMFPACK_CONTROL], MagnitudeType Info[UMFPACK_INFO]);

    OrdinalType numeric(const OrdinalType Ap[], const OrdinalType Ai[],
                        const ScalarType Ax[], void* Symbolic, void** Numeric,
                        const MagnitudeType Control[UMFPACK_CONTROL], MagnitudeType Info[UMFPACK_INFO]);

    void freeSymbolic(void** Symbolic);

    void freeNumeric(void** Numeric);

    OrdinalType solve(OrdinalType sys, const OrdinalType Ap[], const OrdinalType Ai[],
               const ScalarType Ax[], ScalarType X[], const ScalarType B[],
               void* Numeric, const MagnitudeType Control[UMFPACK_CONTROL], MagnitudeType Info[UMFPACK_INFO]);

    OrdinalType getLunz(OrdinalType* lnz, OrdinalType* unz, OrdinalType* nRow, OrdinalType* nCol, OrdinalType* nzUdiag, void* Numeric);

    OrdinalType getNumeric(OrdinalType Lp[], OrdinalType Lj[], ScalarType Lx[], OrdinalType Up[], OrdinalType Ui[], ScalarType Ux[],
            OrdinalType P[], OrdinalType Q[], ScalarType Dx[], OrdinalType* doRecip, ScalarType Rs[], void* Numeric);

    OrdinalType getDeterminant(ScalarType* Mx, ScalarType* Ex, void* NumericHandle, ScalarType UserInfo[UMFPACK_INFO]);
    //@}

  private:
    //! Copy Constructor.
    UMFPACK(const UMFPACK<OrdinalType, ScalarType>& umfpack);
    //! assignment operator
    UMFPACK<OrdinalType, ScalarType>& operator=(const UMFPACK<OrdinalType, ScalarType>& umfpack);
};

// End of Template declaration
// Begin of Template implementation

template<typename OrdinalType, typename ScalarType>
OrdinalType UMFPACK<OrdinalType, ScalarType>::symbolic(OrdinalType nRow, OrdinalType nCol,
        const OrdinalType Ap[], const OrdinalType Ai[],
        const ScalarType Ax[], void** Symbolic,
        const MagnitudeType Control[UMFPACK_CONTROL], MagnitudeType Info[UMFPACK_INFO])
{
    UndefinedUMFPACKRoutine<ScalarType>::notDefined();
}

template<typename OrdinalType, typename ScalarType>
OrdinalType UMFPACK<OrdinalType, ScalarType>::numeric(const OrdinalType Ap[], const OrdinalType Ai[],
        const ScalarType Ax[], void* Symbolic, void** Numeric,
        const MagnitudeType Control[UMFPACK_CONTROL], MagnitudeType Info[UMFPACK_INFO])
{
    UndefinedUMFPACKRoutine<ScalarType>::notDefined();
}

template<typename OrdinalType, typename ScalarType>
void UMFPACK<OrdinalType, ScalarType>::freeSymbolic(void** Symbolic)
{
    UndefinedUMFPACKRoutine<ScalarType>::notDefined();
}

template<typename OrdinalType, typename ScalarType>
void UMFPACK<OrdinalType, ScalarType>::freeNumeric(void** Numeric)
{
    UndefinedUMFPACKRoutine<ScalarType>::notDefined();
}

template<typename OrdinalType, typename ScalarType>
OrdinalType UMFPACK<OrdinalType, ScalarType>::solve(OrdinalType sys, const OrdinalType Ap[], const OrdinalType Ai[],
        const ScalarType Ax[], ScalarType X[], const ScalarType B[],
        void* Numeric, const MagnitudeType Control[UMFPACK_CONTROL], MagnitudeType Info[UMFPACK_INFO])
{
    UndefinedUMFPACKRoutine<ScalarType>::notDefined();
}

template<typename OrdinalType, typename ScalarType>
OrdinalType UMFPACK<OrdinalType, ScalarType>::getLunz(OrdinalType* lnz, OrdinalType* unz, OrdinalType* nRow, OrdinalType* nCol, OrdinalType* nzUdiag, void* Numeric)
{
    UndefinedUMFPACKRoutine<ScalarType>::notDefined();
}

template<typename OrdinalType, typename ScalarType>
OrdinalType UMFPACK<OrdinalType, ScalarType>::getNumeric(OrdinalType Lp[], OrdinalType Lj[], ScalarType Lx[], OrdinalType Up[], OrdinalType Ui[], ScalarType Ux[],
        OrdinalType P[], OrdinalType Q[], ScalarType Dx[], OrdinalType* doRecip, ScalarType Rs[], void* Numeric)
{
    UndefinedUMFPACKRoutine<ScalarType>::notDefined();
}

template<typename OrdinalType, typename ScalarType>
OrdinalType UMFPACK<OrdinalType, ScalarType>::getDeterminant(ScalarType* Mx, ScalarType* Ex, void* NumericHandle, ScalarType UserInfo[UMFPACK_INFO])
{
    UndefinedUMFPACKRoutine<ScalarType>::notDefined();
}

// End of general implementation

// Begin of specialization
#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<>
class UMFPACK<int, double>
{
  public:
    static number_t infoSize;                //size of info structure (UMFPACK_INFO)
    static number_t controlSize;             //size of control structure (UMFPACK_CONTROL)

    inline UMFPACK(void) {}
    inline ~UMFPACK(void) {}

    int symbolic(int nRow, int nCol,
            const int Ap[], const int Ai[],
            const double Ax[], void** Symbolic,
            const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    int numeric(const int Ap[], const int Ai[],
            const double Ax[], void* Symbolic, void** Numeric,
            const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    void freeSymbolic(void** Symbolic);

    void freeNumeric(void** Numeric);

    int solve(int sys, const int Ap[], const int Ai[],
            const double Ax[], double X[], const double B[],
            void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    int getLunz(int* lnz, int* unz, int* nRow, int* nCol, int* nzUdiag, void* Numeric);

    int getNumeric(int Lp[], int Lj[], double Lx[], int Up[], int Ui[], double Ux[],
            int P[], int Q[], double Dx[], int* doRecip, double Rs[], void* Numeric);

    int getDeterminant(double* Mx, double* Ex, void* NumericHandle, double UserInfo[UMFPACK_INFO]);
  private:
    UMFPACK(const UMFPACK<int, double>& umfpack);
    UMFPACK<int,double>& operator=(const UMFPACK<int, double>& umfpack);
};

template<>
class UMFPACK<int, std::complex<double> >
{
  public:
    static number_t infoSize;                //size of info structure (UMFPACK_INFO)
    static number_t controlSize;             //size of control structure (UMFPACK_CONTROL)

    inline UMFPACK(void) {}
    inline ~UMFPACK(void) {}

    int symbolic(int nRow, int nCol,
            const int Ap[], const int Ai[],
            const std::complex<double> Ax[], void** Symbolic,
            const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    int numeric(const int Ap[], const int Ai[],
            const std::complex<double> Ax[], void* Symbolic, void** Numeric,
            const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    void freeSymbolic(void** Symbolic);

    void freeNumeric(void** Numeric);

    int solve(int sys, const int Ap[], const int Ai[],
            const std::complex<double> Ax[], std::complex<double> X[], const std::complex<double> B[],
            void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    //! Solve in case AX=B with A complex, B real
    int solveCR(int sys, const int Ap[], const int Ai[],
                const std::complex<double> Ax[], std::complex<double> X[], const double Bx[], const double Bz[],
                void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    int getLunz(int* lnz, int* unz, int* nRow, int* nCol, int* nzUdiag, void* Numeric);

    int getNumeric(int Lp[], int Lj[], std::complex<double> Lx[], int Up[], int Ui[], std::complex<double> Ux[],
            int P[], int Q[], std::complex<double> Dx[], int *doRecip, double Rs[], void* Numeric);

    int getDeterminant(std::complex<double>* Mx, double* Ex, void* NumericHandle, double UserInfo[UMFPACK_INFO]);

  private:
    UMFPACK(const UMFPACK<int, std::complex<double> >& umfpack); //!< specialization of constructor (forbidden)
    UMFPACK<int, std::complex<double> >& operator=(const UMFPACK<int, std::complex<double> >& umfpack); //!< specialization of copy constructor (forbidden)
};

template<>
class UMFPACK<SuiteSparseLong, double>
{
  public:
    inline UMFPACK(void) {}
    inline UMFPACK(const UMFPACK<SuiteSparseLong, double>& umfpack) {}
    inline ~UMFPACK(void) {}

    SuiteSparseLong symbolic(SuiteSparseLong nRow, SuiteSparseLong nCol,
                         const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
                         const double Ax[], void** Symbolic,
                         const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    SuiteSparseLong numeric(const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
                        const double Ax[], void* Symbolic, void** Numeric,
                        const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    void freeSymbolic(void** Symbolic);

    void freeNumeric(void** Numeric);

    SuiteSparseLong solve(SuiteSparseLong sys, const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
               const double Ax[], double X[], const double B[],
               void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    SuiteSparseLong getLunz(SuiteSparseLong* lnz, SuiteSparseLong* unz, SuiteSparseLong* nRow, SuiteSparseLong* nCol, SuiteSparseLong* nzUdiag, void* Numeric);

    SuiteSparseLong getNumeric(SuiteSparseLong Lp[], SuiteSparseLong Lj[], double Lx[], SuiteSparseLong Up[], SuiteSparseLong Ui[], double Ux[],
            SuiteSparseLong P[], SuiteSparseLong Q[], double Dx[], SuiteSparseLong* doRecip, double Rs[], void* Numeric);

    SuiteSparseLong getDeterminant(double* Mx, double* Ex, void* NumericHandle, double UserInfo[UMFPACK_INFO]);

  private:
    UMFPACK(const UMFPACK<SuiteSparseLong, std::complex<double> >& umfpack);
    UMFPACK<SuiteSparseLong, std::complex<double> >& operator=(const UMFPACK<SuiteSparseLong, std::complex<double> >& umfpack);
};

template<>
class UMFPACK<SuiteSparseLong, std::complex<double> >
{
  public:
    inline UMFPACK(void) {}
    inline ~UMFPACK(void) {}

    SuiteSparseLong symbolic(SuiteSparseLong nRow, SuiteSparseLong nCol,
                         const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
                         const std::complex<double> Ax[], void** Symbolic,
                         const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    SuiteSparseLong numeric(const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
                const std::complex<double> Ax[],
                void* Symbolic, void** Numeric,
                const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    void freeSymbolic(void** Symbolic);

    void freeNumeric(void** Numeric);

    SuiteSparseLong solve(SuiteSparseLong sys, const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
               const std::complex<double> Ax[], std::complex<double> X[], const std::complex<double> B[],
               void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
    //! Solve in case AX=B with A complex, B real
    SuiteSparseLong solveCR(SuiteSparseLong sys, const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
                const std::complex<double> Ax[], std::complex<double> X[], const double Bx[], const double Bz[],
                void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);

    SuiteSparseLong getLunz(SuiteSparseLong* lnz, SuiteSparseLong* unz, SuiteSparseLong* nRow, SuiteSparseLong* nCol, SuiteSparseLong* nzUdiag, void* Numeric);

    SuiteSparseLong getNumeric(SuiteSparseLong Lp[], SuiteSparseLong Lj[], std::complex<double> Lx[], SuiteSparseLong Up[], SuiteSparseLong Ui[], std::complex<double> Ux[],
            SuiteSparseLong P[], SuiteSparseLong Q[], std::complex<double> Dx[], SuiteSparseLong *doRecip, double Rs[], void* Numeric);

    SuiteSparseLong getDeterminant(std::complex<double>* Mx, double* Ex, void* NumericHandle, double UserInfo[UMFPACK_INFO]);
  private:
    UMFPACK(const UMFPACK<SuiteSparseLong, double>& umfpack);
    UMFPACK<SuiteSparseLong,double>& operator=(const UMFPACK<SuiteSparseLong, double>& umfpack);
};

#endif

} // end of namespace xlifepp

#endif /* XLIFEPP_UMFPACK_HPP */

/* =====================================================================================================================
Exhaustive list of UMFPACK items, some may be useful

Control structure
0   UMFPACK_PRL
    UMFPACK_DENSE_ROW
    UMFPACK_DENSE_COL
    UMFPACK_BLOCK_SIZE
    UMFPACK_STRATEGY
5   UMFPACK_2BY2_TOLERANCE
    UMFPACK_FIXQ
    UMFPACK_AMD_DENSE
    UMFPACK_AGGRESSIVE
    UMFPACK_PIVOT_TOLERANCE
10  UMFPACK_ALLOC_INIT
    UMFPACK_SYM_PIVOT_TOLERANCE
    UMFPACK_SCALE
    UMFPACK_FRONT_ALLOC_INIT
    UMFPACK_DROPTOL
15  UMFPACK_IRSTEP
    UMFPACK_COMPILED_WITH_BLAS
    UMFPACK_COMPILED_FOR_MATLAB
    UMFPACK_COMPILED_WITH_GETRUSAGE
    UMFPACK_COMPILED_IN_DEBUG_MODE
20  UMFPACK_STRATEGY_AUTO
    UMFPACK_STRATEGY_UNSYMMETRIC
    UMFPACK_STRATEGY_2BY2
    UMFPACK_STRATEGY_SYMMETRIC
    UMFPACK_SCALE_NONE
25  UMFPACK_SCALE_SUM
    UMFPACK_SCALE_MAX

Info structure
0   UMFPACK_STATUS
    UMFPACK_NROW
    UMFPACK_NCOL
    UMFPACK_NZ
    UMFPACK_SIZE_OF_UNIT
5   UMFPACK_SIZE_OF_INT
    UMFPACK_SIZE_OF_LONG
    UMFPACK_SIZE_OF_POINTER
    UMFPACK_SIZE_OF_ENTRY
    UMFPACK_NDENSE_ROW
10  UMFPACK_NEMPTY_ROW
    UMFPACK_NDENSE_COL
    UMFPACK_NEMPTY_COL
    UMFPACK_SYMBOLIC_DEFRAG
    UMFPACK_SYMBOLIC_PEAK_MEMORY
15  UMFPACK_SYMBOLIC_SIZE
    UMFPACK_SYMBOLIC_TIME
    UMFPACK_SYMBOLIC_WALLTIME
    UMFPACK_STRATEGY_USED
    UMFPACK_ORDERING_USED
20  UMFPACK_QFIXED
    UMFPACK_DIAG_PREFERRED
    UMFPACK_PATTERN_SYMMETRY
    UMFPACK_NZ_A_PLUS_AT
    UMFPACK_NZDIAG
25  UMFPACK_SYMMETRIC_LUNZ
    UMFPACK_SYMMETRIC_FLOPS
    UMFPACK_SYMMETRIC_NDENSE
    UMFPACK_SYMMETRIC_DMAX
    UMFPACK_2BY2_NWEAK
30  UMFPACK_2BY2_UNMATCHED
    UMFPACK_2BY2_PATTERN_SYMMETRY
    UMFPACK_2BY2_NZ_PA_PLUS_PAT
    UMFPACK_2BY2_NZDIAG
    UMFPACK_COL_SINGLETONS
35  UMFPACK_ROW_SINGLETONS
    UMFPACK_N2
    UMFPACK_S_SYMMETRIC
    UMFPACK_NUMERIC_SIZE_ESTIMATE
    UMFPACK_PEAK_MEMORY_ESTIMATE
40  UMFPACK_FLOPS_ESTIMATE
    UMFPACK_LNZ_ESTIMATE
    UMFPACK_UNZ_ESTIMATE
    UMFPACK_VARIABLE_INIT_ESTIMATE
    UMFPACK_VARIABLE_PEAK_ESTIMATE
45  UMFPACK_VARIABLE_FINAL_ESTIMATE
    UMFPACK_MAX_FRONT_SIZE_ESTIMATE
    UMFPACK_MAX_FRONT_NROWS_ESTIMATE
    UMFPACK_MAX_FRONT_NCOLS_ESTIMATE
    UMFPACK_NUMERIC_SIZE
50  UMFPACK_PEAK_MEMORY
    UMFPACK_FLOPS
    UMFPACK_LNZ
    UMFPACK_UNZ
    UMFPACK_VARIABLE_INIT
55  UMFPACK_VARIABLE_PEAK
    UMFPACK_VARIABLE_FINAL
    UMFPACK_MAX_FRONT_SIZE
    UMFPACK_MAX_FRONT_NROWS
    UMFPACK_MAX_FRONT_NCOLS
60  UMFPACK_NUMERIC_DEFRAG
    UMFPACK_NUMERIC_REALLOC
    UMFPACK_NUMERIC_COSTLY_REALLOC
    UMFPACK_COMPRESSED_PATTERN
    UMFPACK_LU_ENTRIES
65  UMFPACK_NUMERIC_TIME
    UMFPACK_UDIAG_NZ
    UMFPACK_RCOND
    UMFPACK_WAS_SCALED
    UMFPACK_RSMIN
70  UMFPACK_RSMAX
    UMFPACK_UMIN
    UMFPACK_UMAX
    UMFPACK_ALLOC_INIT_USED
    UMFPACK_FORCED_UPDATES
75  UMFPACK_NUMERIC_WALLTIME
    UMFPACK_NOFF_DIAG
    UMFPACK_ALL_LNZ
    UMFPACK_ALL_UNZ
    UMFPACK_NZDROPPED
80  UMFPACK_IR_TAKEN
    UMFPACK_IR_ATTEMPTED
    UMFPACK_OMEGA1
    UMFPACK_OMEGA2
    UMFPACK_SOLVE_FLOPS
85  UMFPACK_SOLVE_TIME
    UMFPACK_SOLVE_WALLTIME
    UMFPACK_ORDERING_COLAMD
    UMFPACK_ORDERING_AMD
    UMFPACK_ORDERING_GIVEN

Status:
0   UMFPACK_OK
    UMFPACK_WARNING_singular_matrix
    UMFPACK_WARNING_determinant_underflow
    UMFPACK_WARNING_determinant_overflow
    UMFPACK_ERROR_out_of_memory
5   UMFPACK_ERROR_invalid_Numeric_object
    UMFPACK_ERROR_invalid_Symbolic_object
    UMFPACK_ERROR_argument_missing
    UMFPACK_ERROR_n_nonpositive
    UMFPACK_ERROR_invalid_matrix
10  UMFPACK_ERROR_different_pattern
    UMFPACK_ERROR_invalid_system
    UMFPACK_ERROR_invalid_permutation
    UMFPACK_ERROR_internal_error
    UMFPACK_ERROR_file_IO

Sys:
0   UMFPACK_A
    UMFPACK_At
    UMFPACK_Aat
    UMFPACK_Pt_L
    UMFPACK_L
5   UMFPACK_Lt_P
    UMFPACK_Lat_P
    UMFPACK_Lt
    UMFPACK_U_Qt
    UMFPACK_U
10  UMFPACK_Q_Ut
    UMFPACK_Q_Uat
    UMFPACK_Ut
    UMFPACK_Uat

*/
