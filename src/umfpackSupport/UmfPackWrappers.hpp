/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file UmfPackWrappers.hpp
  \authors Manh Ha NGUYEN
  \since 29 August 2013
  \date 04 September 2013

  \brief Wrapper of some important functions of UMFPACK in order to solver (non)-symmetric problem
*/

#ifndef XLIFEPP_UMFPACK_WRAPPERS_HPP
#define XLIFEPP_UMFPACK_WRAPPERS_HPP

#include "UmfPackTypeDefs.hpp"

#define UMFPACK_MANGLE(type,func) umfpack ## _ ## type ## _ ## func

// Primary functions of case int/double
#define DISYMBOLIC     UMFPACK_MANGLE(di,symbolic)
#define DINUMERIC      UMFPACK_MANGLE(di,numeric)
#define DISOLVE        UMFPACK_MANGLE(di,solve)
#define DIFREESYMBOLIC UMFPACK_MANGLE(di,free_symbolic)
#define DIFREENUMERIC  UMFPACK_MANGLE(di,free_numeric)
#define DIGETLUNZ      UMFPACK_MANGLE(di,get_lunz)
#define DIGETNUMERIC   UMFPACK_MANGLE(di,get_numeric)
#define DIGETDET       UMFPACK_MANGLE(di,get_determinant)

// Primary functions of case int/complex
#define ZISYMBOLIC     UMFPACK_MANGLE(zi,symbolic)
#define ZINUMERIC      UMFPACK_MANGLE(zi,numeric)
#define ZISOLVE        UMFPACK_MANGLE(zi,solve)
#define ZIFREESYMBOLIC UMFPACK_MANGLE(zi,free_symbolic)
#define ZIFREENUMERIC  UMFPACK_MANGLE(zi,free_numeric)
#define ZIGETLUNZ      UMFPACK_MANGLE(zi,get_lunz)
#define ZIGETNUMERIC   UMFPACK_MANGLE(zi,get_numeric)
#define ZIGETDET       UMFPACK_MANGLE(zi,get_determinant)

// Primary functions of case suitesparse long/double
#define DLSYMBOLIC     UMFPACK_MANGLE(dl,symbolic)
#define DLNUMERIC      UMFPACK_MANGLE(dl,numeric)
#define DLSOLVE        UMFPACK_MANGLE(dl,solve)
#define DLFREESYMBOLIC UMFPACK_MANGLE(dl,free_symbolic)
#define DLFREENUMERIC  UMFPACK_MANGLE(dl,free_numeric)
#define DLGETLUNZ      UMFPACK_MANGLE(dl,get_lunz)
#define DLGETNUMERIC   UMFPACK_MANGLE(dl,get_numeric)
#define DLGETDET       UMFPACK_MANGLE(dl,get_determinant)

// Primary functions of case suitesparse long/complex
#define ZLSYMBOLIC     UMFPACK_MANGLE(zl,symbolic)
#define ZLNUMERIC      UMFPACK_MANGLE(zl,numeric)
#define ZLSOLVE        UMFPACK_MANGLE(zl,solve)
#define ZLFREESYMBOLIC UMFPACK_MANGLE(zl,free_symbolic)
#define ZLFREENUMERIC  UMFPACK_MANGLE(zl,free_numeric)
#define ZLGETLUNZ      UMFPACK_MANGLE(zl,get_lunz)
#define ZLGETNUMERIC   UMFPACK_MANGLE(zl,get_numeric)
#define ZLGETDET       UMFPACK_MANGLE(zl,get_determinant)

#ifdef __cplusplus
extern "C" {
#endif

int DISYMBOLIC(int n_row, int n_col, const int Ap[], const int Ai[], const double Ax[], void** Symbolic, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
int DINUMERIC(const int Ap[], const int Ai[], const double Ax[], void* Symbolic, void** Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
int DISOLVE(int sys, const int Ap[], const int Ai[], const double Ax[], double X[], const double B[], void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
void DIFREESYMBOLIC(void** Symbolic);
void DIFREENUMERIC(void** Numeric);
int DIGETLUNZ(int* lnz, int* unz, int* n_row, int* n_col, int* nz_udiag, void* Numeric);
int DIGETNUMERIC(int Lp[], int Lj[], double Lx[], int Up[], int Ui[], double Ux[], int P[], int Q[], double Dx[], int* do_recip, double Rs[], void* Numeric);
int DIGETDET(double* Mx, double* Ex, void* NumericHandle, double User_Info[UMFPACK_INFO]);

int ZISYMBOLIC(int n_row, int n_col, const int Ap[], const int Ai[], const double Ax[], const double Az[], void** Symbolic, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
int ZINUMERIC(const int Ap[], const int Ai[], const double Ax[], const double Az[], void* Symbolic, void** Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
int ZISOLVE(int sys, const int Ap[], const int Ai[], const double Ax[], const double Az[], double Xx[], double Xz[], const double Bx[], const double Bz[], void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
void ZIFREESYMBOLIC(void** Symbolic);
void ZIFREENUMERIC(void** Numeric);
int ZIGETLUNZ(int* lnz, int* unz, int* n_row, int* n_col, int* nz_udiag, void* Numeric);
int ZIGETNUMERIC(int Lp[], int Lj[], double Lx[], double Lz[], int Up[], int Ui[], double Ux[], double Uz[], int P[], int Q[], double Dx[], double Dz[], int* do_recip, double Rs[], void* Numeric);
int ZIGETDET(double* Mx, double* Mz, double* Ex, void* NumericHandle, double User_Info[UMFPACK_INFO]);

SuiteSparseLong DLSYMBOLIC(SuiteSparseLong n_row, SuiteSparseLong n_col, const SuiteSparseLong Ap[], const SuiteSparseLong Ai[], const double Ax[], void** Symbolic, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
SuiteSparseLong DLNUMERIC(const SuiteSparseLong Ap[], const SuiteSparseLong Ai[], const double Ax[], void* Symbolic, void** Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
SuiteSparseLong DLSOLVE(SuiteSparseLong sys, const SuiteSparseLong Ap[], const SuiteSparseLong Ai[], const double Ax[], double X[], const double B[], void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
void DLFREESYMBOLIC(void** Symbolic);
void DLFREENUMERIC(void** Numeric);
SuiteSparseLong DLGETLUNZ(SuiteSparseLong* lnz, SuiteSparseLong* unz, SuiteSparseLong* n_row, SuiteSparseLong* n_col, SuiteSparseLong* nz_udiag, void* Numeric);
SuiteSparseLong DLGETNUMERIC(SuiteSparseLong Lp[], SuiteSparseLong Lj[], double Lx[], SuiteSparseLong Up[], SuiteSparseLong Ui[], double Ux[], SuiteSparseLong P[], SuiteSparseLong Q[], double Dx[], SuiteSparseLong* do_recip, double Rs[], void* Numeric);
SuiteSparseLong DLGETDET(double* Mx, double* Ex, void* NumericHandle, double User_Info[UMFPACK_INFO]);

SuiteSparseLong ZLSYMBOLIC(SuiteSparseLong n_row, SuiteSparseLong n_col, const SuiteSparseLong Ap[], const SuiteSparseLong Ai[], const double Ax[], const double Az[], void** Symbolic, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
SuiteSparseLong ZLNUMERIC(const SuiteSparseLong Ap[], const SuiteSparseLong Ai[], const double Ax[], const double Az[], void* Symbolic, void** Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
SuiteSparseLong ZLSOLVE(SuiteSparseLong sys, const SuiteSparseLong Ap[], const SuiteSparseLong Ai[], const double Ax[], const double Az[], double Xx[], double Xz[], const double Bx[], const double Bz[], void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO]);
void ZLFREESYMBOLIC(void** Symbolic);
void ZLFREENUMERIC(void** Numeric);
SuiteSparseLong ZLGETLUNZ(SuiteSparseLong* lnz, SuiteSparseLong* unz, SuiteSparseLong* n_row, SuiteSparseLong* n_col, SuiteSparseLong* nz_udiag, void* Numeric);
SuiteSparseLong ZLGETNUMERIC(SuiteSparseLong Lp[], SuiteSparseLong Lj[], double Lx[], double Lz[], SuiteSparseLong Up[], SuiteSparseLong Ui[], double Ux[], double Uz[], SuiteSparseLong P[], SuiteSparseLong Q[], double Dx[], double Dz[], SuiteSparseLong* do_recip, double Rs[], void* Numeric);
SuiteSparseLong ZLGETDET(double* Mx, double* Mz, double* Ex, void* NumericHandle, double User_Info[UMFPACK_INFO]);
#ifdef __cplusplus
}
#endif

#endif /* XLIFEPP_UMFPACK_WRAPPERS_HPP */
