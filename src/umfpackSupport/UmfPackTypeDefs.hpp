/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file UmfPackTypeDefs.hpp
  \authors Manh Ha NGUYEN
  \since 29 August 2013
  \date 29 August 2013

  \brief Some constant values of Umfpack library
*/

#ifndef XLIFEPP_UMFPACK_TYPEDEF_HPP
#define XLIFEPP_UMFPACK_TYPEDEF_HPP

#define UMFPACK_INFO 90
#define UMFPACK_CONTROL 20

#ifndef SuiteSparseLong
  #ifdef COMPILER_IS_32_BITS
    #define SuiteSparseLong long
    #define SuiteSparse_long_max LONG_MAX
    #define SuiteSparse_long_idd "ld"
  #else
    // #define SuiteSparseLong __int64
    // #define SuiteSparse_long_max _I64_MAX
    // #define SuiteSparse_long_idd "I64d"
    #define SuiteSparseLong long long
    #define SuiteSparse_long_max LONG_LONG_MAX
    #define SuiteSparse_long_idd "I64d"
  #endif
  #define SuiteSparse_long_id "%" SuiteSparse_long_idd
#endif

#endif /* XLIFEPP_UMFPACK_TYPEDEF_HPP */

