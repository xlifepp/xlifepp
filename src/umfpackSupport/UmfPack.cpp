/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file UmfPack.cpp
  \authors Manh Ha NGUYEN, Y. Lafranche
  \since 29 August 2013
  \date 29 August 2013

  \brief Wrapper of some important functions of UMFPACK in order to solver (non)-symmetric problem
*/

#include "config.h"
#ifdef XLIFEPP_WITH_UMFPACK
  #include "UmfPack.hpp"
  #include "UmfPackWrappers.hpp"

  namespace xlifepp
  {

  //
  // Specialization int/double
  //
  number_t UMFPACK<int, double>::infoSize = 90;
  number_t UMFPACK<int, double>::controlSize = 30;


  int UMFPACK<int, double>::symbolic(int nRow, int nCol,
                      const int Ap[], const int Ai[],
                      const double Ax[], void** Symbolic,
                      const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      return (DISYMBOLIC(nRow, nCol, Ap, Ai, Ax, Symbolic, Control, Info));
  }

  int UMFPACK<int, double>::numeric(const int Ap[], const int Ai[],
                      const double Ax[], void* Symbolic, void** Numeric,
                      const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      return (DINUMERIC(Ap, Ai, Ax, Symbolic, Numeric, Control, Info));
  }

  int UMFPACK<int, double>::solve(int sys, const int Ap[], const int Ai[],
          const double Ax[], double X[], const double B[], void* Numeric,
          const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      return (DISOLVE(sys, Ap, Ai, Ax, X, B, Numeric, Control, Info));
  }

  void UMFPACK<int, double>::freeSymbolic(void** Symbolic)
  {
      DIFREESYMBOLIC(Symbolic);
  }

  void UMFPACK<int, double>::freeNumeric(void** Numeric)
  {
      DIFREENUMERIC(Numeric);
  }

  int UMFPACK<int, double>::getLunz(int* lnz, int* unz, int* nRow, int* nCol, int* nzUdiag, void* Numeric)
  {
      return DIGETLUNZ(lnz, unz, nRow, nCol, nzUdiag, Numeric);
  }

  int UMFPACK<int, double>::getNumeric(int Lp[], int Lj[], double Lx[], int Up[], int Ui[], double Ux[],
          int P[], int Q[], double Dx[], int* doRecip, double Rs[], void* Numeric)
  {
      return DIGETNUMERIC(Lp, Lj, Lx, Up, Ui, Ux, P, Q, Dx, doRecip, Rs, Numeric);
  }

  int UMFPACK<int, double>::getDeterminant(double* Mx, double* Ex, void* NumericHandle, double UserInfo[UMFPACK_INFO])
  {
      return DIGETDET(Mx, Ex, NumericHandle, UserInfo);
  }

  //
  // Specialization int/complex<double>
  //

  int UMFPACK<int, std::complex<double> >::symbolic(int nRow, int nCol,
          const int Ap[], const int Ai[],
          const std::complex<double> Az[], void** Symbolic,
          const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      double* null = (double*)nullptr;
      const double* Axy=reinterpret_cast<const double*>(Az); //packed complex
      return (ZISYMBOLIC(nRow, nCol, Ap, Ai, Axy, null, Symbolic, Control, Info));
  }

  int UMFPACK<int, std::complex<double> >::numeric(const int Ap[], const int Ai[],
          const std::complex<double> Az[], void* Symbolic, void** Numeric,
          const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      double* null = (double*)nullptr;
      const double* Axy=reinterpret_cast<const double*>(Az);  //packed complex
      return ZINUMERIC(Ap, Ai, Axy, null, Symbolic, Numeric, Control, Info);
  }

  int UMFPACK<int, std::complex<double> >::solve(int sys, const int Ap[], const int Ai[],
          const std::complex<double> Az[], std::complex<double> X[], const std::complex<double> B[],
          void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      double* null = (double*)nullptr;
      const double* Axy=reinterpret_cast<const double*>(Az);  //packed complex
      const double* Bxy=reinterpret_cast<const double*>(B);   //packed complex
      double* Xxy=reinterpret_cast<double*>(X);               //packed complex
      return (ZISOLVE(sys, Ap, Ai, Axy, null, Xxy, null, Bxy, null, Numeric, Control, Info));
  }

  int UMFPACK<int, std::complex<double> >::solveCR(int sys, const int Ap[], const int Ai[],
          const std::complex<double> Az[], std::complex<double> X[], const double Bx[], const double Bz[],
          void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      double* null = (double*)nullptr;
      const double* Axy=reinterpret_cast<const double*>(Az);  //packed complex
      double* Xxy=reinterpret_cast<double*>(X);               //packed complex
      return (ZISOLVE(sys, Ap, Ai, Axy, null, Xxy, null, Bx, Bz, Numeric, Control, Info));
  }

  void UMFPACK<int, std::complex<double> >::freeSymbolic(void** Symbolic)
  {
      ZIFREESYMBOLIC(Symbolic);
  }

  void UMFPACK<int, std::complex<double> >::freeNumeric(void** Numeric)
  {
      ZIFREENUMERIC(Numeric);
  }

  int UMFPACK<int, std::complex<double> >::getLunz(int* lnz, int* unz, int* nRow, int* nCol, int* nzUdiag, void* Numeric)
  {
      return ZIGETLUNZ(lnz, unz, nRow, nCol, nzUdiag, Numeric);
  }

  int UMFPACK<int, std::complex<double> >::getNumeric(int Lp[], int Lj[], std::complex<double> Lz[], int Up[], int Ui[], std::complex<double> Uz[],
          int P[], int Q[], std::complex<double> Dz[], int *doRecip, double Rs[], void* Numeric)
  {
      double* null = (double*)nullptr;
      double* Lxy=reinterpret_cast<double*>(Lz);  //packed complex
      double* Uxy=reinterpret_cast<double*>(Uz);  //packed complex
      double* Dxy=reinterpret_cast<double*>(Dz);  //packed complex
      return ZIGETNUMERIC(Lp, Lj, Lxy, null, Up, Ui, Uxy, null, P, Q, Dxy, null, doRecip, Rs, Numeric);
  }

  int UMFPACK<int, std::complex<double> >::getDeterminant(std::complex<double>* Mx, double* Ex, void* NumericHandle, double UserInfo[UMFPACK_INFO])
  {
      double* null = (double*)nullptr;
      return ZIGETDET(reinterpret_cast<double*>(Mx), null, Ex, NumericHandle, UserInfo);
  }

  //
  // Specialization long long/double
  //

  SuiteSparseLong UMFPACK<SuiteSparseLong, double>::symbolic(SuiteSparseLong nRow, SuiteSparseLong nCol,
                      const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
                      const double Ax[], void** Symbolic,
                      const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      return (DLSYMBOLIC(nRow, nCol, Ap, Ai, Ax, Symbolic, Control, Info));
  }

  SuiteSparseLong UMFPACK<SuiteSparseLong, double>::numeric(const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
                      const double Ax[], void* Symbolic, void** Numeric,
                      const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      return (DLNUMERIC(Ap, Ai, Ax, Symbolic, Numeric, Control, Info));
  }

  SuiteSparseLong UMFPACK<SuiteSparseLong, double>::solve(SuiteSparseLong sys, const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
          const double Ax[], double X[], const double B[], void* Numeric,
          const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      return (DLSOLVE(sys, Ap, Ai, Ax, X, B, Numeric, Control, Info));
  }

  void UMFPACK<SuiteSparseLong, double>::freeSymbolic(void** Symbolic)
  {
      DLFREESYMBOLIC(Symbolic);
  }

  void UMFPACK<SuiteSparseLong, double>::freeNumeric(void** Numeric)
  {
      DLFREENUMERIC(Numeric);
  }

  SuiteSparseLong UMFPACK<SuiteSparseLong, double>::getLunz(SuiteSparseLong* lnz, SuiteSparseLong* unz, SuiteSparseLong* nRow, SuiteSparseLong* nCol, SuiteSparseLong* nzUdiag, void* Numeric)
  {
      return DLGETLUNZ(lnz, unz, nRow, nCol, nzUdiag, Numeric);
  }

  SuiteSparseLong UMFPACK<SuiteSparseLong, double>::getNumeric(SuiteSparseLong Lp[], SuiteSparseLong Lj[], double Lx[], SuiteSparseLong Up[], SuiteSparseLong Ui[], double Ux[],
          SuiteSparseLong P[], SuiteSparseLong Q[], double Dx[], SuiteSparseLong* doRecip, double Rs[], void* Numeric)
  {
      return DLGETNUMERIC(Lp, Lj, Lx, Up, Ui, Ux, P, Q, Dx, doRecip, Rs, Numeric);
  }

  SuiteSparseLong UMFPACK<SuiteSparseLong, double>::getDeterminant(double* Mx, double* Ex, void* NumericHandle, double UserInfo[UMFPACK_INFO])
  {
      return DLGETDET(Mx, Ex, NumericHandle, UserInfo);
  }

  //
  // Specialization long long/complex<double>
  //

  SuiteSparseLong UMFPACK<SuiteSparseLong, std::complex<double> >::symbolic(SuiteSparseLong nRow, SuiteSparseLong nCol,
          const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
          const std::complex<double> Az[], void** Symbolic,
          const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      double* null = (double*)nullptr;
      const double* Axy=reinterpret_cast<const double*>(Az); //packed complex
      return (ZLSYMBOLIC(nRow, nCol, Ap, Ai, Axy, null, Symbolic, Control, Info));
  }

  SuiteSparseLong UMFPACK<SuiteSparseLong, std::complex<double> >::numeric(const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
          const std::complex<double> Az[], void* Symbolic, void** Numeric,
          const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      double* null = (double*)nullptr;
      const double* Axy=reinterpret_cast<const double*>(Az);  //packed complex
      return ZLNUMERIC(Ap, Ai, Axy, null, Symbolic, Numeric, Control, Info);
  }

  SuiteSparseLong UMFPACK<SuiteSparseLong, std::complex<double> >::solve(SuiteSparseLong sys, const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
          const std::complex<double> Az[], std::complex<double> X[], const std::complex<double> B[],
          void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      double* null = (double*)nullptr;
      const double* Axy=reinterpret_cast<const double*>(Az);  //packed complex
      const double* Bxy=reinterpret_cast<const double*>(B);   //packed complex
      double* Xxy=reinterpret_cast<double*>(X);               //packed complex
      return (ZLSOLVE(sys, Ap, Ai, Axy, null, Xxy, null, Bxy, null, Numeric, Control, Info));
  }

  SuiteSparseLong UMFPACK<SuiteSparseLong, std::complex<double> >::solveCR(SuiteSparseLong sys, const SuiteSparseLong Ap[], const SuiteSparseLong Ai[],
          const std::complex<double> Az[], std::complex<double> X[], const double Bx[], const double Bz[],
          void* Numeric, const double Control[UMFPACK_CONTROL], double Info[UMFPACK_INFO])
  {
      double* null = (double*)nullptr;
      const double* Axy=reinterpret_cast<const double*>(Az);  //packed complex
      double* Xxy=reinterpret_cast<double*>(X);               //packed complex
      return (ZLSOLVE(sys, Ap, Ai, Axy, null, Xxy, null, Bx, Bz, Numeric, Control, Info));
  }

  void UMFPACK<SuiteSparseLong, std::complex<double> >::freeSymbolic(void** Symbolic)
  {
      ZLFREESYMBOLIC(Symbolic);
  }

  void UMFPACK<SuiteSparseLong, std::complex<double> >::freeNumeric(void** Numeric)
  {
      ZLFREENUMERIC(Numeric);
  }

  SuiteSparseLong UMFPACK<SuiteSparseLong, std::complex<double> >::getLunz(SuiteSparseLong* lnz, SuiteSparseLong* unz, SuiteSparseLong* nRow, SuiteSparseLong* nCol, SuiteSparseLong* nzUdiag, void* Numeric)
  {
      return ZLGETLUNZ(lnz, unz, nRow, nCol, nzUdiag, Numeric);
  }

  SuiteSparseLong UMFPACK<SuiteSparseLong, std::complex<double> >::getNumeric(SuiteSparseLong Lp[], SuiteSparseLong Lj[], std::complex<double> Lz[], SuiteSparseLong Up[], SuiteSparseLong Ui[], std::complex<double> Uz[],
          SuiteSparseLong P[], SuiteSparseLong Q[], std::complex<double> Dz[], SuiteSparseLong *doRecip, double Rs[], void* Numeric)
  {
      double* null = (double*)nullptr;
      double* Lxy=reinterpret_cast<double*>(Lz);  //packed complex
      double* Uxy=reinterpret_cast<double*>(Uz);  //packed complex
      double* Dxy=reinterpret_cast<double*>(Dz);  //packed complex
      return ZLGETNUMERIC(Lp, Lj, Lxy, null, Up, Ui, Uxy, null, P, Q, Dxy, null, doRecip, Rs, Numeric);
  }

  SuiteSparseLong UMFPACK<SuiteSparseLong, std::complex<double> >::getDeterminant(std::complex<double>* Mx, double* Ex, void* NumericHandle, double UserInfo[UMFPACK_INFO])
  {
      double* null = (double*)nullptr;
      return ZLGETDET(reinterpret_cast<double*>(Mx), null, Ex, NumericHandle, UserInfo);
  }


  } // end of namespace xlifepp
#endif  //XLIFEPP_WITH_UMFPACK

