/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file fft.hpp
  \author E. Lunéville
  \since 28 jan 2019
  \date  28 jan 2019

  \brief Implementation of the FFT from C++ Cookbook by Jeff Cogswell, Jonathan Turkanis, Christopher Diggins, D. Ryan Stephens
*/

#ifndef FFT_HPP_
#define FFT_HPP_

#include "utils.h"
#include "config.h"

namespace xlifepp
{

inline
number_t bitReverse(number_t x, int log2n)
{
  number_t n = 0;
  for (number_t i=0; i < log2n; i++)
    {
      n <<= 1;
      n |= (x & 1);
      x >>= 1;
    }
  return n;
}

/*! perform the discrete Fourier transform of the discrete vector a of length n=2^log2n
    the result is the vector b of length 2^log2n
            bk = sum_j=0,n-1 aj*exp(2*i*q*j*k/n)
    ita: iterator at the beginning of a
    itb: iterator at the beginning of b
    log2n: log_2(n)
    q: a parameter = -pi for direct fft and = pi for inverse fft
*/
template<typename IterA, typename IterB>
void ffta(IterA ita, IterB itb, number_t log2n, real_t q)
{
  number_t n = 1 << log2n;
  for (number_t i=0; i < n; ++i)
    {
      *(itb+bitReverse(i, log2n)) = *(ita+i);
    }
  for (number_t s = 1; s <= log2n; ++s)
    {
      number_t m = 1 << s;
      number_t m2 = m >> 1;
      complex_t w(1., 0);
      complex_t wm = std::exp(i_*(q/m2));
      for (number_t j=0; j < m2; ++j)
        {
          for (number_t k=j; k < n; k += m)
            {
              complex_t t = w * *(itb+k+m2);
              complex_t u = *(itb+k);
              *(itb+k) = u + t;
              *(itb+k+m2) = u - t;
            }
          w *= wm;
        }
    }
}

/*! perform the discrete Fourier transform of the discrete vector a of length n=2^log2n
    the result is the vector b of length 2^log2n
            bk = sum_j=0,n-1 aj*exp(-2*i*pi*j*k/n)
    ita: iterator at the beginning of a
    itb: iterator at the beginning of b
    log2n: log_2(n)
*/
template<typename IterA, typename IterB>
void fft(IterA ita, IterB itb, number_t log2n)
{
    ffta(ita,itb,log2n,-pi_);
}

/*! perform the inverse discrete Fourier transform of the discrete vector a of length n=2^log2n
    the result is the vector b of length 2^log2n
            bk = (1/n) sum_j=0,n-1 aj*exp(2*i*pi*j*k/n)
    ita: iterator at the beginning of a
    itb: iterator at the beginning of b
    log2n: log_2(n)
*/
template<typename IterA, typename IterB>
void ifft(IterA ita, IterB itb, number_t log2n)
{
    ffta(ita,itb,log2n,pi_);
    number_t n = 1 << log2n;
    for (number_t i=0; i < n; ++i) *(itb+i)/=n;
}

// fft from a real/complex vector, result is a complex vector
template<typename T>
std::vector<complex_t>& fft(const std::vector<T>& f, std::vector<complex_t>& g)
{
    number_t log2n=0, n=f.size();
    while( n>>=1 ) log2n++;  // shift by one bit to the right
    ffta(f.begin(),g.begin(),log2n,-pi_);
    return g;
}

template<typename T>
std::vector<complex_t> fft(const std::vector<T>& f)
{
    std::vector<complex_t> g(f.size());
    return fft(f,g);
}

// ifft from a real/complex vector, result is a complex vector
template<typename T>
std::vector<complex_t>& ifft(const std::vector<T>& f, std::vector<complex_t>& g)
{
    number_t log2n=0, n=f.size();
    while( n>>=1 ) log2n++;  // shift by one bit to the right
    ifft(f.begin(),g.begin(),log2n);
    return g;
}

template<typename T>
std::vector<complex_t> ifft(const std::vector<T>& f)
{
    std::vector<complex_t> g(f.size());
    return ifft(f,g);
}


} //end of namespace xlifepp
#endif // FTT_HPP
