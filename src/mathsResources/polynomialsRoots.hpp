/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file polynomialsRoots.hpp
  \author N. Kielbasiewicz
  \since 20 aug 2014
  \date 20 aug 2014

  \brief Definition of functions computing roots of polyniomals up to degree 4
 */

#ifndef POLYNOMIALS_ROOTS_HPP
#define POLYNOMIALS_ROOTS_HPP

#include "config.h"
#include "utils.h"

namespace xlifepp
{

std::vector<complex_t> ferrari(real_t a, real_t b, real_t c, real_t d, real_t e); //!< computes roots of degree 4 polynomial (Ferrari method)
std::vector<complex_t> cardan(real_t a, real_t b, real_t c, real_t d); //!< computes roots of degree 3 polynomial (real Cardan method)
std::vector<complex_t> cardan(complex_t a, complex_t b, complex_t c, complex_t d); //!< computes roots of degree 3 polynomial (complex Cardan method)
std::vector<complex_t> quadratic(real_t a, real_t b, real_t c); //!< computes roots of degree 2 real polynomial
std::vector<complex_t> quadratic(complex_t a, complex_t b, complex_t c); //!< computes roots of degree 2 complex polynomial

} // end of namespace xlifepp

#endif /* POLYNOMIALS_ROOTS_HPP */
