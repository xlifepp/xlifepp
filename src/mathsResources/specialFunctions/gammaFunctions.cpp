/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file gammaFunctions.cpp
  \author D. Martin
  \since 20 dec 2007
  \date 28 may 2010

  \brief Implementation of gamma functions
 */

#include "specialFunctions.hpp"
#include "utils.h"

#include <iostream>

namespace xlifepp
{

/*
--------------------------------------------------------------------------------
 Gamma and LogGamma functions
--------------------------------------------------------------------------------
*/
real_t gammaFunction(int_t n)
{
  // Gamma function for all positive integer n = (n-1)!
  //
  if ( n <= 0 )
  {
    where("gammaFunction(Int)");
    error("int_not_positive", n);
  }
  real_t g = 1.;
  for ( int_t k = 2; k < n; k++ ) { g *= k; }
  return g;
}

real_t gammaFunction(real_t x)
{
  // Gamma function for all real x which is not a negative integer
  //
  if ( std::abs(x - int_t(x)) <= theZeroThreshold ) { return gammaFunction(int_t(x)); } // use factorial (x-1)!!
  else if ( x > 0 )    { return std::exp(logGamma(x)); }
  else
  {
    // for -1 < x < 0 , use Gamma(x) = pi/(Gamma(1-x)*sin(pi*x))
    // for      x <-1 , use recursively Gamma(x) = Gamma(x+1)/x
    real_t x_p(x), x_f(1.);
    while ( x_p < -1. ) { x_f /= x_p++; }
    return x_f * pi_ / ( std::exp(logGamma(1. - x_p)) * std::sin(pi_ * x_p) );
  }
}

complex_t gammaFunction(const complex_t& z)
{
  // Gamma function defined for all complex z which is not a negative integer
  //
  real_t z_r(z.real());
  if ( std::abs(z.imag()) <= theZeroThreshold )
  { return complex_t(gammaFunction(z_r), 0.); }
  else
  {
    if ( z_r > 0.)
    { return exp(logGamma(z)); }
    else
      // Reflection formula
      // for Re(z) < 0, use Gamma(1-z) = pi / (Gamma(z)*sin(pi*z))
    { return pi_ / ( exp(logGamma(1. - z)) * sin(pi_ * z) ); }
  }
}

// Coefficients for C.Lanczos expansion of LogGamma function
// Coefficients for series expansion valid over the whole complex region Re(z) > 0
static const real_t lgg_coeff[11] =
{
  1.000000000000000174663,
  5716.400188274341379136, -14815.30426768413909044, 14291.49277657478554025,
  -6348.160217641458813289,  1301.608286058321874105, -108.1767053514369634679,
  2.605696505611755827729, -0.7423452510201416151527e-2, 0.5384136432509564062961e-7,
  -0.4023533141268236372067e-8
};

real_t logGamma(real_t x)
{
  //
  if ( x <= 0. )
  {
    where("logGamma(Real)");
    error("real_not_positive", x);
  }
  // Paul Godfrey's Lanczos implementation of the Gamma function
  // http://home.att.net/~numericana/answer/info/godfrey.htm
  //
  const int gam(9);
  real_t x_m(x + gam), ss(x_m - .5), ser(lgg_coeff[0]);
  for ( int k = gam + 1; k > 0; k--, x_m -= 1.) { ser += lgg_coeff[k] / x_m; }
  return (x - 0.5) * std::log(ss) - ss + std::log(ser * std::sqrt(2.*pi_));
}

complex_t LogGamma1(const complex_t& z)
{
  //BIG BUG: returns LogGamma(z) + 2i*Pi
  // for instance for z = 1 + y (3.5<y<7.6) or z = 1.1 + y (4.0<y<7.2)
  //
  complex_t z_p(z), lgam;
  if ( z.real() < 0. ) { z_p = -z; }

  const int gam(9);
  complex_t z_m(z_p + real_t(gam)), ss(z_m - .5), ser(0.);
  for ( int k = gam + 1; k > 0; k--, z_m -= 1.) { ser += lgg_coeff[k] / z_m; }
  lgam = (z_p - 0.5) * std::log(ss) - ss + std::log((ser + lgg_coeff[0]) * std::sqrt(2.*pi_));

  // Reflection formula
  // for Re(z) < 0, use Gamma(1-z) = pi / (sin(pi*z)*Gamma(z))
  if ( z.real() < 0. ) { lgam = -lgam + log(pi_ / (z_p * sin(pi_ * z))); }
  return lgam;
}

// Coefficients for C.Lanczos expansion of LogGamma function
// Coefficients for series expansion valid over complex region Re(z) > 7.
static const real_t lg_coef[10] =
{
  .8333333333333333e-1, -.2777777777777778e-2, .7936507936507937e-3,
  -.5952380952380952e-3, .8417508417508418e-3, -.1917526917526918e-02,
  .6410256410256410e-2, -.2955065359477124e-1, .1796443723688307, -1.39243221690590
};
// 1/12,-1/360, 1/1260,-1/1680, 1/1188,-691/360360, 1/156,-3617/122400, ?, ?

// Coefficients for C.Lanczos expansion of DiGamma function dg_coef[k] = - (2k+1) * lg_coef[k]
static const real_t dg_coeff[10] =
{
  -.83333333333333333e-1, .83333333333333333e-2, -.39682539682539683e-2,
  .41666666666666667e-2, -.75757575757575758e-2, .21092796092796093e-1,
  -.83333333333333333e-1, .4432598039215686    , -.3053954330270122e+1, .125318899521531e+2
};
//-1/12, 3/360,-5/1260, 7/1680,-9/1188, 11*691/360360,-13/156, 15*3617/122400, ? , ?

complex_t logGamma(const complex_t& z)
{
  // log[gamma(z)] for all complex z which is not a negative integer
  //
  real_t z_r = z.real(), z_ra(z_r);
  if ( z_ra > 171. ) { return complex_t(theRealMax, 0.); }
  if ( std::abs(z.imag()) <= theZeroThreshold && z_r <= 0.0 && std::abs(z_r - int_t(z_r)) <= theZeroThreshold )
  { return complex_t(theRealMax, 0.); }

  complex_t lgam, z_p(z);
  // transform z by symetry and translation such that modified z satisfies Re (z) > 7
  //
  if ( z_r < 0. ) { z_p = -z; z_ra = -z_r; }
  int n(0);
  complex_t z_m(z_p);
  if ( z_ra < 7. )
  {
    n = 7 - int(z_ra);
    z_m = z_p + complex_t(n, 0.);
  }
  // for | Re(z) | > 7, use C.Lanczos expansion (see Abramowitz&Stegun, formula 6.1.41)
  // log(Gamma(z)) = (z-1/2) log(z) - z + log(2pi)
  //               + 1/12z - 1/360z^3 + 1/1260z^5 - 1/1680z^7 + 1/1188z^9 + ...
  //
  complex_t overzk(1. / z_m), overz2(overzk * overzk);
  lgam = (z_m - 0.5) * log(z_m) - z_m + 0.5 * std::log(2.*pi_);
  for ( int k = 0; k < 10; k++, overzk *= overz2) { lgam += lg_coef[k] * overzk; }
  // Recurrence formula
  // for | Re(z) | < 7 , use recursively Gamma(z) = Gamma(z+1)/z
  for ( int k = 0; k < n; k++, z_p += 1. ) { lgam -= log(z_p); }
  // Reflection formula
  // for Re(z) < 0, use Gamma(1-z) = pi / (sin(pi*z)*Gamma(z))
  if ( z_r <= 0. ) { lgam = -lgam + log(pi_ / (z_p * sin(pi_ * z))); }

  return lgam;
}

/*
--------------------------------------------------------------------------------
 function Digamma(z) or Psi(z) : derivative of Log(Gamma(z))
--------------------------------------------------------------------------------
*/
real_t diGamma(int_t n)
{
  // Computation of Digamma(n) for positive integer n
  if ( n <= 0 )
  {
    where("diGamma(const Int)");
    error("int_not_positive", n);
  }
  real_t s(-theEulerConst);
  for ( int_t k = 1; k < n; k++ ) { s += 1. / k; }
  return s;
}

real_t diGamma(real_t x)
{
  real_t x_a=std::abs(x), dgam=0.;

  if ( std::abs(x_a - int(x_a)) <= theZeroThreshold )
  { return diGamma(int_t(x)); }
  else if ( std::abs(x_a + .5 - int(x_a + .5)) <= theZeroThreshold )
  {
    // For x = an integer + 1/2 use Abramowitz&Stegun (page 258 formula 6.3.4)
    int n = int(x_a - .5);
    for ( int k = 1; k < n; k++ ) { dgam += 1. / (k + k - 1.); }
    dgam += 2 * (dgam - logOf2_) - theEulerConst;
  }
  else
  {
    // Use formula for derivative of LogGamma(z)
    if ( x_a < 10 )
    {
      int n(10 - int(x_a));
      // for | x | < 10 , use recursively DiGamma(x) = DiGamma(x+1) - 1/x
      for ( int k = 0; k < n; k++ ) { dgam -= 1. / (k + x_a); }
      x_a += n;
    }
    real_t overx2(1. / (x_a * x_a)), overx2k(overx2);
    dgam += std::log(x_a) - .5 / x_a;
    for ( int k = 0; k < 10; k++, overx2k *= overx2 ) { dgam += dg_coeff[k] * overx2k; }
  }
  // Reflection formula
  // for x < 0, use Digamma(1-x) = Digamma(x) + pi/tan(pi*x);
  if ( x < 0 ) { dgam -= pi_ * (std::tan(pi_ * x)) + 1. / x; }
  return dgam;
}

complex_t diGamma(const complex_t& z)
{
  real_t z_r = z.real(), z_ra(z_r);

  if ( std::abs(z.imag()) <= theZeroThreshold )
  {
    if ( std::abs(z_r - int_t(z_r)) <= theZeroThreshold ) { return complex_t(diGamma(int_t(z_r)), 0.); }
    else { return complex_t(diGamma(z_r), 0.); }
  }
  else
  {
    // Use formula for derivative of LogGamma(z)
    //
    complex_t dgam(0.), z_p(z);
    if ( z_r < 0 ) { z_p = -z; z_ra = -z_r; }
    int n(0);
    complex_t z_m(z_p);
    if ( z_ra < 8. )
    {
      n = 8 - int(z_ra);
      z_m = z_p + complex_t(n, 0.);
    }
    // for | Re(z) | > 8, use derivative of C.Lanczos expansion for LogGamma
    // DiGamma(z) = log(z) - 1/(2z) - 1/12z^2 + 3/360z^4 - 5/1260z^6 + 7/1680z^8 - 9/1188z^10 + ...
    // (Abramowitz&Stegun, page 259, formula 6.3.18
    complex_t overz(1. / z_m), overz2(overz * overz), overzk(overz2);
    dgam += log(z_m) - .5 * overz;
    for ( int k = 0; k < 10; k++, overzk *= overz2 ) { dgam += dg_coeff[k] * overzk; }
    // Recurrence formula
    // for | Re(z) | < 8 , use recursively DiGamma(z) = DiGamma(z+1) - 1/z
    for ( int k = 0; k < n; k++, z_p += 1. ) { dgam -= 1. / z_p; }
    // Reflection formula
    // for Re(z) < 0, use Digamma(1-z) = Digamma(z) + pi/tan(pi*z);
    if ( z_r < 0. ) { dgam -= pi_ / tan(pi_ * z) + 1. / z; }

    return dgam;
  }
}
/*
--------------------------------------------------------------------------------
 Test of special functions: comparison with tables in pp. 267--287
 HANDBOOK Of MATHEMATICAL FUNCTIONS , Ed. M.ABRAMOWITZ & I.A. STEGUN
--------------------------------------------------------------------------------
*/
void gammaTest(std::ostream& out)
{
  // extra precision with respect to the A_S 6-digit precision
  // (results in A-S is obtained with ep = 0.)
  int ep(0), wd(15 + ep);
  out.setf(std::ios::scientific);
  real_t x(1.);
  // Gamma(x), Log(Gamma(x), Digamma(x)
  // x = 1.000, 1.005, 1.010, ... 2.000
  int nb_eval(0);
  for ( int page_number = 267; page_number < 271; page_number++ )
  {
    out << std::endl << std::endl << " Page " << page_number << std::endl
              << std::endl << string_t(20, ' ') << "GAMMA, DIGAMMA AND TRIGAMMA FUNCTIONS" << std::endl
              << std::endl << "  x     " << std::setw(wd) << std::left << "  Gamma(x)" << std::setw(wd) << std::left << "  LogGamma(x)" << std::setw(wd) << std::left << "  Digamma(x)" << std::setw(wd) << std::left << "  Trigamma(x)";
    for ( int i = 0; i < 51; i++, x += .005, nb_eval++)
      out << std::endl << int(x) << "." << int(10 * x) - 10 * int(x)
                << std::setw(wd) << std::setprecision(10 + ep) << gammaFunction(x)
                << std::setw(wd) << std::setprecision(10 + ep) << logGamma(x)
                << std::setw(wd) << std::setprecision(10 + ep) << diGamma(x);
    x -= .005;
  }
  out << std::endl << string_t(80, '-') << std::endl << "Gamma(x) Number of evaluations " << nb_eval << std::endl << string_t(80, '-') << std::endl;
  // real_t and imaginary parts of LogGamma( x + i*y ) with 12-digit precision

  // extra precision with respect to the A_S 12-digit precision
  // (results in A-S is obtained with ep = 0.)
  ep = 0;
  x = 1.;
  nb_eval = 0;
  for ( int page_number = 277; page_number < 288; page_number++, x += .1 )
  {
    out << std::endl << std::endl << " Page " << page_number
              << std::endl << string_t(25, ' ') << "GAMMA FUNCTION FOR COMPLEX ARGUMENTS"
              << std::endl << std::endl << string_t(40, ' ') << "x = " << std::setprecision(1) << int(x) << "." << int(10 * x) - 10 * int(x)
              << std::endl << std::endl << "  y     " << std::setw(wd) << std::left << "Re LogGamma(z)      Im LogGamma(z)"
              << "     y    " << std::setw(wd) << std::left << "Re LogGamma(z)      Im LogGamma(z)";
    real_t y(0.0);
    for ( int i = 0; i < 52; i++, y += 0.1, nb_eval += 2 )
    {
      complex_t ga(logGamma(complex_t(x, y))), ga5(logGamma(complex_t(x, y + 5.)));
      out << std::endl << " " << std::setw(1) << int(y) << "." << int(10 * y) - 10 * int(y) << "  "
                << std::setw(wd) << std::setprecision(12 + ep) << ga.real()
                << " " << std::setw(wd) << std::setprecision(12 + ep) << ga.imag()
                << "   " << std::setw(1) << int(y + 5.) << "." << int(10 * (y + 5.)) - 10 * int(y + 5.) << "  "
                << std::setw(wd) << std::setprecision(12 + ep) << ga5.real()
                << " " << std::setw(wd) << std::setprecision(12 + ep) << ga5.imag();
    }
  }
  out << std::endl << string_t(80, '-') << std::endl << "LogGamma(z) Number of evaluations " << nb_eval << std::endl << string_t(80, '-') << std::endl;
  //
  x = 1.;
  int cols(1);
  int page_number(288);
  nb_eval = 0;
  for ( int p = 0; p < 6; p++)
  {
    for ( int col = 0; col < cols; col++, x += .1 )
    {
      out << std::endl << std::endl << " Page " << page_number
                << std::endl << string_t(25, ' ') << "DIGAMMA FUNCTION FOR COMPLEX ARGUMENTS"
                << std::endl << std::endl << string_t(40, ' ') << "x = " << int(x) << "." << int(10 * x) - 10 * int(x)
                << std::endl << std::endl << "  y     " << std::setw(wd) << std::left << "Re DiGamma(z)       Im DiGamma(z) "
                << "     y    " << std::setw(wd) << std::left << "Re DiGamma(z)       Im DiGamma(z)";
      real_t y(0.0);
      for ( int i = 0; i < 52; i++, y += 0.1, nb_eval += 2 )
      {
        complex_t ga(diGamma(complex_t(x, y))), ga5(diGamma(complex_t(x, y + 5.)));
        out << std::endl << " " << std::setw(1) << int(y) << "." << int(10 * y) - 10 * int(y) << "  "
                  << std::setw(wd) << std::setprecision(12 + ep) << ga.real()
                  << " " << std::setw(wd) << std::setprecision(12 + ep) << ga.imag()
                  << "   " << std::setw(1) << int(y + 5.) << "." << int(10 * (y + 5.)) - 10 * int(y + 5.) << "  "
                  << std::setw(wd) << std::setprecision(12 + ep) << ga5.real()
                  << " " << std::setw(wd) << std::setprecision(12 + ep) << ga5.imag();
      }
    }
    cols = 2;
    page_number++;
  }
  out << std::endl << string_t(80, '-') << std::endl << "Digamma(z) Number of evaluations " << nb_eval << std::endl << string_t(80, '-') << std::endl;

  out.unsetf(std::ios::fixed);
}

} // end of namespace xlifepp
