/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file BesselFunctions.cpp
  \author D. Martin, N. Kielbasiewicz
  \since 14 feb 2007
  \date 5 mar 2018

  \brief Implementation of Bessel and Hankel functions
 */

#include "specialFunctions.hpp"
#include "utils.h"

#include <iostream>

namespace xlifepp
{

//@{
//! around pi
static const real_t pi_over4(pi_ / 4.);
static const real_t three_pi_over4(3 * pi_over4);
static const real_t two_over_pi(2. / pi_);
static const real_t sqrt2_over_pi(std::sqrt(two_over_pi));
//@}

//@{
//! first roots of J_0, J_1, Y_0, Y_1 and approximations
static const real_t J0_z[2]    = { 2.4048255576957727686e+0, 5.5200781102863106496e+0};
static const real_t Y0_z[3]    = { 8.9357696627916752158e-1, 3.9576784193148578684e+0, 7.0860510603017726976e+0};
static const real_t J1_z[2]    = { 3.8317059702075123156e+0, 7.0155866698156187535e+0};
static const real_t Y1_z[2]    = { 2.1971413260310170351e+0, 5.4296810407941351328e+0};
//@}

//@{
//! approximate fractional roots (multiples of 1/256)
static const real_t J0_a[2] = { 2.4062500000000000000e+0, 5.5195312500000000000e+0};
static const real_t Y0_a[3] = { 0.8906250000000000000e+0, 3.9570312500000000000e+0, 7.085937500000000000e+0};
static const real_t J1_a[2] = { 3.8320312500000000000e+0, 7.0156250000000000000e+0};
static const real_t Y1_a[2] = { 2.1953125000000000000e+0, 5.4296875000000000000e+0};
//@}

//@{
//! accurate values for approximate roots above
static const real_t J0_v[2] = { -1.42444230422723137837e-3, 5.46860286310649596604e-4};
static const real_t Y0_v[3] = { 2.9519662791675215849e-3,  6.4716931485786837568e-4, 1.1356030177269762362e-4};
static const real_t J1_v[2] = { -3.2527979248768438556e-4, -3.8330184381246462950e-5};
static const real_t Y1_v[2] = { 1.8288260310170351490e-3, -6.4592058648672279948e-6};
//@}

//@{
// data for rational approximations of Bessel functions J_O, Y_0 for large arguments
static const real_t jy0_p0 [6] =
{
  8.8961548424210455236e-1, 1.5376201909008354296e+2, 3.4806486443249270347e+3,
  2.1170523380864944322e+4, 4.1345386639580765797e+4, 2.2779090197304684302e+4
};
static const real_t jy0_q0 [5] =
{
  1.5711159858080893649e+2, 3.5028735138235608207e+3, 2.1215350561880115730e+4,
  4.1370412495510416640e+4, 2.2779090197304684318e+4
};
static const real_t jy0_p1 [6] =
{
  -8.8033303048680751817e-3, -1.2441026745835638459e+0, -2.2300261666214198472e+1,
  -1.1183429920482737611e+2, -1.8591953644342993800e+2, -8.9226600200800094098e+1
};
static const real_t jy0_q1 [5] =
{
  9.0593769594993125859e+1, 1.4887231232283756582e+3, 7.2642780169211018836e+3,
  1.1951131543434613647e+4, 5.7105024128512061905e+3
};
//@}

//@{
//! data for rational approximations of Bessel functions J_1, Y_1 for large arguments
static const real_t jy1_p0 [6] =
{
  -1.6116166443246101165e+3, -1.0982405543459346727e+5, -1.5235293511811373833e+6,
  -6.6033732483649391093e+6, -9.9422465050776411957e+6, -4.4357578167941278571e+6
};
static const real_t jy1_q0 [6] =
{
  -1.4550094401904961825e+3, -1.0726385991103820119e+5, -1.5118095066341608816e+6,
  -6.5853394797230870728e+6, -9.9341243899345856590e+6, -4.4357578167941278568e+6
};
static const real_t jy1_p1 [6] =
{
  3.5265133846636032186e+1, 1.7063754290207680021e+3, 1.8494262873223866797e+4,
  6.6178836581270835179e+4, 8.5145160675335701966e+4, 3.3220913409857223519e+4
};
static const real_t jy1_q1 [6] =
{
  8.6383677696049909675e+2, 3.7890229745772202641e+4, 4.0029443582266975117e+5,
  1.4194606696037208929e+6, 1.8194580422439972989e+6, 7.0871281941028743574e+5
};
//@}

//@{
//! data for Chebyshev interpolation of Struve functions H_0 and H_1
static const real_t ch0[13] = {.182311992692574,
                             -.68661765315081e-1, .388759121580854   , -.267648939655143   , .794413767405257e-1,
                             -.13647452878064e-1, .155298216531296e-2, -.126637763099949e-3, .779608642052048e-5,
                             -.37611407660050e-6, .1462633271602e-7  , -.46873653930e-9    , .1260241570e-10
                            };
static const real_t ch0minusy0[13] = {.992837275764239,
                                    -.696891281138625e-2, .182051037870371e-3, -.106325825284416e-4, .98198294286525e-6,
                                    -.12250645444977e-6 , .1894083311800e-7  , -.344358225604e-8   , .71119101711e-9,
                                    -.16288744137e-9    , .4065680728e-10    , -.1091504796e-10    , .312005243e-11
                                   };
static const real_t ch1[13] = {.557889144648160,
                             -.111883257265698   , -.163379581252009   , .322569320724059   , -.145816323672442,
                             .329267739937403e-1, -.460372142093572e-2, .443470616331396e-3, -.314209952934117e-4,
                             .171237199380035e-5, -.7416987005204e-7  , .261837670705e-8   , -.7685839395e-10
                            };
static const real_t ch1minusy1[13] = {1.0075764293865,
                                    .750316051248257e-2, -.704393326451905e-4, .266205393382266e-5, -.18841157753405e-6,
                                    .1949014958394e-7  , -.261261989905e-8   , .42362690104e-9    , -.7955155531e-10,
                                    .1679973006e-10    , -.390719821e-11     , .98543090e-12      , -.26635794e-12
                                   };
//@}

//@{
//! Specific integerss as real numbers
static const real_t zero=real_t(0.);
static const real_t one=real_t(1.);
static const real_t four=real_t(4.);
static const real_t eight=real_t(8.);
//@}

/*
--------------------------------------------------------------------------------
   Bessel functions of order 0, 1 of the first kind: J_0, J_1
--------------------------------------------------------------------------------
*/
real_t besselJ0(real_t x)
{
  // Returns the Bessel function of the first kind and order 0, J_0(x) for real x.
  // Adapted into c++ from William Cody's CALJY0 fortran routine
  real_t j0, x2, absx(std::abs(x));
  if ( absx < theTolerance ) { j0 = one; }
  else if ( absx < four )
  {
    // coefficients for rational approximation of J_0(x) / (x**2 - J0_z[0]**2)
    static const real_t j0_s_n [7] =
    {
      -1.2117036164593528341e-1, 1.0344222815443188943e+2, -3.6629814655107086448e+04,
      6.6302997904833794242e+6, -6.2140700423540120665e+8, 2.7282507878605942706e+10,
      -4.1298668500990866786e+11
    };
    static const real_t j0_s_d [5] =
    {
      9.3614022392337710626e+2, 4.5612696224219938200e+5, 1.3985097372263433271e+8,
      2.6328198300859648632e+10, 2.3883787996332290397e+12
    };
    x2 = x * x;
    real_t p = hornerAlgorithmTpl(x2, j0_s_n, j0_s_n + 7, zero);
    real_t q = hornerAlgorithmTpl(x2, j0_s_d, j0_s_d + 5, one);
    j0 = (absx - J0_a[0] - J0_v[0]) * (absx + J0_z[0]) * p / q;
  }
  else if ( absx < eight )
  {
    // coefficients for rational approximation of J_0(x) / (x**2 - J0_z[1]**2)
    static const real_t j0_b_n [8] =
    {
      4.8591703355916499363e+1, 7.4321196680624245801e+2, 4.4176707025325087628e+3,
      1.1725046279757103576e+4, 1.0341910641583726701e+4, -7.2879702464464618998e+3,
      -1.2254078161378989535e+4, -1.8319397969392084011e+3
    };
    static const real_t j0_b_d [7] =
    {
      -2.5258076240801555057e+1, 3.3307310774649071172e+2, -2.9458766545509337327e+3,
      1.8680990008359188352e+4, -8.4055062591169562211e+4, 2.4599102262586308984e+5,
      -3.5783478026152301072e+5
    };
    x2 = one - x * x / 64.;
    real_t p = hornerAlgorithmTpl(x2, j0_b_n, j0_b_n + 8, zero);
    real_t q = hornerAlgorithmTpl(x2, j0_b_d, j0_b_d + 7, one);
    j0 = (absx - J0_a[1] - J0_v[1]) * (absx + J0_z[1]) * p / q;
  }
  else
  {
    real_t t = absx - pi_over4;
    real_t eight_over_x = eight / absx;
    x2 = eight_over_x * eight_over_x;
    real_t r0 = hornerAlgorithmTpl(x2, jy0_p0, jy0_p0 + 6, zero) / hornerAlgorithmTpl(x2, jy0_q0, jy0_q0 + 5, one);
    real_t r1 = hornerAlgorithmTpl(x2, jy0_p1, jy0_p1 + 6, zero) / hornerAlgorithmTpl(x2, jy0_q1, jy0_q1 + 5, one);
    j0 = sqrt2_over_pi / std::sqrt(absx) * (r0 * std::cos(t) - r1 * eight_over_x * std::sin(t));
  }
  return j0;
}

real_t besselJ1(real_t x)
{
  // Returns the Bessel function of the first kind and order 1, J_1(x) for real x.
  // Adapted into c++ from William Cody's CALJY1 fortran routine
  real_t j1, x2, absx(std::abs(x));
  if ( absx < theTolerance ) { j1 = .5 * x; }
  else if ( absx < four)
  {
    static const real_t j1_s_n [7] =
    {
      -1.0767857011487300348e-2, 1.0650724020080236441e+1, -4.4615792982775076130e+3,
      9.8062904098958257677e+5, -1.1548696764841276794e+8, 6.6781041261492395835e+9,
      -1.4258509801366645672e+11
    };
    static const real_t j1_s_d [5] =
    {
      1.0742272239517380498e+3 , 5.9117614494174794095e+5, 2.0228375140097033958e+8,
      4.2091902282580133541e+10, 4.1868604460820175290e+12
    };
    x2 = x * x;
    real_t p = hornerAlgorithmTpl(x2, j1_s_n, j1_s_n + 7, zero);
    real_t q = hornerAlgorithmTpl(x2, j1_s_d, j1_s_d + 5, one);
    j1 = x * (absx - J1_a[0] - J1_v[0]) * (absx + J1_z[0]) * p / q;
  }
  else if ( absx < eight)
  {
    static const real_t j1_b_n [8] =
    {
      4.6179191852758252280e+0, -7.1329006872560947377e+3,  4.5039658105749078904e+6,
      -1.4437717718363239107e+9, 2.3569285397217157313e+11, -1.6324168293282543629e+13,
      1.1357022719979468624e+14, 1.0051899717115285432e+15
    };
    static const real_t j1_b_d [7] =
    {
      1.3886978985861357615e+3, 1.1267125065029138050e+6, 6.4872502899596389593e+8,
      2.7622777286244082666e+11, 8.4899346165481429307e+13, 1.7128800897135812012e+16,
      1.7253905888447681194e+18
    };
    x2 = x * x;
    real_t p = hornerAlgorithmTpl(x2, j1_b_n, j1_b_n + 6, zero);
    p = p * (x2 - 64.) + j1_b_n[6];
    p = p * (x2 - 16.) + j1_b_n[7];
    real_t q = hornerAlgorithmTpl(x2, j1_b_d, j1_b_d + 7, one);
    j1 = x * (absx - J1_a[1] - J1_v[1]) * (absx + J1_z[1]) * p / q;
  }
  else
  {
    real_t t = absx - three_pi_over4;
    real_t eight_over_x = eight / absx;
    x2 = eight_over_x * eight_over_x;
    real_t r0 = hornerAlgorithmTpl(x2, jy1_p0, jy1_p0 + 6, zero) / hornerAlgorithmTpl(x2, jy1_q0, jy1_q0 + 6, one);
    real_t r1 = hornerAlgorithmTpl(x2, jy1_p1, jy1_p1 + 6, zero) / hornerAlgorithmTpl(x2, jy1_q1, jy1_q1 + 6, one);
    j1 = sqrt2_over_pi / std::sqrt(absx) * (r0 * std::cos(t) - r1 * eight_over_x * std::sin(t));
    if ( x < 0 ) { j1 = -j1; }
  }
  return j1;
}

/*
--------------------------------------------------------------------------------
 Bessel functions of order 0..N of the first kind: J_n
--------------------------------------------------------------------------------
*/
std::vector<real_t> besselJ0N(real_t x, number_t N)
{
  // Returns the Bessel functions of the first kind and orders 0..N, for positive x
  // Backwards recursive computation
  // Initialize result with J_0[0] = 1, J_k[0] = 0, 0 < k <= N
  std::vector<real_t> j0n(N + 1, 0.);
  j0n.front() = one;
  number_t low_bound;
  if ( x > 1.e-15 )
  {
    // Estimate a lower bound low_bound such J_{low_bound}(x) is pretty close to 0
    if (x < 5.)  { low_bound = static_cast<number_t>(4.*x + 21.); }
    else if (x < 10) { low_bound = static_cast<number_t>(2.4 * x + 28.); }
    else if (x < 20.) { low_bound = static_cast<number_t>(2.*x + 32.); }
    else if (x < 50.) { low_bound = static_cast<number_t>(1.5 * x + 42.); }
    else { low_bound = static_cast<number_t>(1.3 * x + 52.); }
    
    real_t jn(zero), jnm1, Two_i(low_bound + low_bound);
    std::vector<real_t>::reverse_iterator jn_i = j0n.rbegin();
    
    if(N > low_bound) jn_i=jn_i+(N-low_bound); // Correction if N > low_bound
    
    for ( number_t i = low_bound; i > 0; i-- )
    {
      // Use recurrence relation starting with low_bound
      // build a sequence proportional to the sequence j_n(x)
      jnm1 = one / (Two_i / x - jn);
      Two_i -= 2.;
      jn = jnm1;
      if ( i <= N ) { *jn_i++ = jn; } // store only required range of values
    }
    j0n.front() = besselJ0(x);
    for ( std::vector<real_t>::iterator jjn_i = j0n.begin() + 1; jjn_i != j0n.end(); jjn_i++ )
    { *jjn_i *= *(jjn_i - 1); }
  }
  return j0n;
}

/*
--------------------------------------------------------------------------------
 Bessel functions of the second kind: Y_0, Y_1
--------------------------------------------------------------------------------
*/
real_t besselY0withoutSingularity(real_t x)
{
  // Returns part of Bessel function of the second kind and order 0.
  // Adapted into c++ from William Cody's CALJY0 fortran routine
  real_t y0, x2;
  if ( x < 3.)
  {
    // coefficients for rational approximation of
    // ( Y_0(x) -2 Log(x/Y0_z[0]) ) / (x**2 - Y0_z[0]**2)
    static const real_t y0_s_n [6] =
    {
      -1.8402381979244993524e+1, 1.0102532948020907590e+4, -2.1287548474401797963e+6,
      2.0422274357376619816e+8, -8.3716255451260504098e+9, 1.0723538782003176831e+11
    };
    static const real_t y0_s_d [5] =
    {
      6.6475986689240190091e+2, 2.3889393209447253406e+5, 5.5662956624278251596e+7,
      8.1617187777290363573e+9, 5.8873865738997033405e+11
    };
    x2 = x * x;
    real_t p = hornerAlgorithmTpl(x2, y0_s_n, y0_s_n + 6, zero);
    real_t q = hornerAlgorithmTpl(x2, y0_s_d, y0_s_d + 5, one);
    y0 = (x - Y0_a[0] - Y0_v[0]) * (x + Y0_z[0]) * p / q;
  }
  else if ( x < 5.5)
  {
    // coefficients for rational approximation of
    // ( Y_0(x) -2 Log(x/Y0_z[1]) ) / (x**2 - Y0_z[1]**2)
    static const real_t y0_m_n [7] =
    {
      1.7427031242901594547e+1, -1.4566865832663635920e+4,  4.6905288611678631510e+6,
      -6.9590439394619619534e+8, 4.3600098638603061642e+10, -5.5107435206722644429e+11,
      -2.2213976967566192242e+13
    };
    static const real_t y0_m_d [6] =
    {
      8.3030857612070288823e+2, 4.0669982352539552018e+5, 1.3960202770986831075e+8,
      3.4015103849971240096e+10, 5.4266824419412347550e+12, 4.3386146580707264428e+14
    };
    x2 = x * x;
    real_t p = hornerAlgorithmTpl(x2, y0_m_n, y0_m_n + 7, zero);
    real_t q = hornerAlgorithmTpl(x2, y0_m_d, y0_m_d + 6, one);
    y0 = (x - Y0_a[1] - Y0_v[1]) * (x + Y0_z[1]) * p / q;
  }
  else if ( x < eight)
  {
    // coefficients for rational approximation of
    // ( Y_0(x) -2 Log(x/Y0_z[2]) ) / (x**2 - Y0_z[2]**2)
    static const real_t y0_b_n [8] =
    {
      -1.7439661319197499338e+1 , 2.1363534169313901632e+4, -1.0085539923498211426e+7,
      2.1958827170518100757e+9 , -1.9363051266772083678e+11, -1.2829912364088687306e+11,
      6.7016641869173237784e+14, -8.0728726905150210443e+15
    };
    static const real_t y0_b_d [7] =
    {
      8.7903362168128450017e+2,  5.3924739209768057030e+5,  2.4727219475672302327e+8,
      8.6926121104209825246e+10, 2.2598377924042897629e+13, 3.9272425569640309819e+15,
      3.4563724628846457519e+17
    };
    x2 = x * x;
    real_t p = hornerAlgorithmTpl(x2, y0_b_n, y0_b_n + 8, zero);
    real_t q = hornerAlgorithmTpl(x2, y0_b_d, y0_b_d + 7, one);
    y0 = (x - Y0_a[2] - Y0_v[2]) * (x + Y0_z[2]) * p / q;
  }
  else { y0 = 0; }
  return y0;
}

real_t besselY0(real_t x)
{
  // Returns the Bessel function of the second kind and order 0, Y_0(x) for positive x.
  real_t y0;
  if ( x <= 0 )
  { y0 = -theRealMax; }
  else if ( x < theTolerance )
  { y0 = two_over_pi * (std::log(x) + std::log(.5) + theEulerConst); }
  else if ( x < 3.)
  { y0 = besselY0withoutSingularity(x) + two_over_pi * std::log(x / Y0_z[0]) * besselJ0(x); }
  else if ( x < 5.5)
  { y0 = besselY0withoutSingularity(x) + two_over_pi * std::log(x / Y0_z[1]) * besselJ0(x); }
  else if ( x < eight)
  { y0 = besselY0withoutSingularity(x) + two_over_pi * std::log(x / Y0_z[2]) * besselJ0(x); }
  else
  {
    real_t t = x - pi_over4;
    real_t eight_over_x = eight / x;
    real_t x2 = eight_over_x * eight_over_x;
    real_t r0 = hornerAlgorithmTpl(x2, jy0_p0, jy0_p0 + 6, zero) / hornerAlgorithmTpl(x2, jy0_q0, jy0_q0 + 5, one);
    real_t r1 = hornerAlgorithmTpl(x2, jy0_p1, jy0_p1 + 6, zero) / hornerAlgorithmTpl(x2, jy0_q1, jy0_q1 + 5, one);
    y0 = sqrt2_over_pi / std::sqrt(x) * (r0 * std::sin(t) + r1 * eight_over_x * std::cos(t));
  }
  return y0;
}

real_t besselY1withoutSingularity(real_t x)
{
  // Returns the Bessel function of the second kind and order 1, Y_1(x) for positive x.
  // Adapted into c++ from William Cody's CALJY1 fortran routine
  real_t y1, x2;
  if ( x < four)
  {
    static const real_t y1_s_n [7] =
    {
      -3.1714424660046133456e+2, 2.2157953222280260820e+5, -5.9157479997408395984e+7,
      7.2144548214502560419e+9, -3.7595974497819597599e+11, 5.4708611716525426053e+12,
      4.0535726612579544093e+13
    };
    static const real_t y1_s_d [6] =
    {
      8.2079908168393867438e+2,  3.8136470753052572164e+5,  1.2250435122182963220e+8,
      2.7800352738690585613e+10, 4.1272286200406461981e+12, 3.0737873921079286084e+14
    };
    x2 = x * x;
    real_t p = hornerAlgorithmTpl(x2, y1_s_n, y1_s_n + 7, zero);
    real_t q = hornerAlgorithmTpl(x2, y1_s_d, y1_s_d + 6, one);
    y1 = (x - Y1_a[0] - Y1_v[0]) * (x + Y1_z[0]) * p / (x * q);
  }
  else if ( x < eight)
  {
    static const real_t y1_b_n [9] =
    {
      -1.2337180442012953128e+3,  1.9153806858264202986e+6, -1.1957961912070617006e+9,
      3.7453673962438488783e+11, -5.9530713129741981618e+13, 4.0686275289804744814e+15,
      -2.3638408497043134724e+16, -5.6808094574724204577e+18, 1.1514276357909013326e+19
    };
    static const real_t y1_b_d [8] =
    {
      1.2855164849321609336e+3, 1.0453748201934079734e+6, 6.3550318087088919566e+8,
      3.0221766852960403645e+11, 1.1187010065856971027e+14, 3.0837179548112881950e+16,
      5.6968198822857178911e+18, 5.3321844313316185697e+20
    };
    x2 = x * x;
    real_t p = hornerAlgorithmTpl(x2, y1_b_n, y1_b_n + 9, zero);
    real_t q = hornerAlgorithmTpl(x2, y1_b_d, y1_b_d + 8, one);
    y1 = (x - Y1_a[1] - Y1_v[1]) * (x + Y1_z[1]) * p / (x * q);
  }
  else { y1 = zero; }
  return y1;
}

real_t besselY1(real_t x)
{
  // Returns the Bessel function of the second kind and order 1, Y_1(x) for positive x.
  real_t y1;
  if ( x <= 0 )
  { y1 = -theRealMax; }
  else if ( x < theTolerance )
  { y1 = -two_over_pi / x; }
  else if ( x < four)
  { y1 = besselY1withoutSingularity(x) + two_over_pi * std::log(x / Y1_z[0]) * besselJ1(x); }
  else if ( x < eight)
  { y1 = besselY1withoutSingularity(x) + two_over_pi * std::log(x / Y1_z[1]) * besselJ1(x); }
  else
  {
    real_t t = x - three_pi_over4;
    real_t eight_over_x = eight / x;
    real_t x2 = eight_over_x * eight_over_x;
    real_t r0 = hornerAlgorithmTpl(x2, jy1_p0, jy1_p0 + 6, zero) / hornerAlgorithmTpl(x2, jy1_q0, jy1_q0 + 6, one);
    real_t r1 = hornerAlgorithmTpl(x2, jy1_p1, jy1_p1 + 6, zero) / hornerAlgorithmTpl(x2, jy1_q1, jy1_q1 + 6, one);
    y1 = sqrt2_over_pi / std::sqrt(x) * (r0 * std::sin(t) + r1 * eight_over_x * std::cos(t));
  }
  return y1;
}

/*
--------------------------------------------------------------------------------
 Bessel functions of order 0..N of the second kind: Y_k
--------------------------------------------------------------------------------
*/
std::vector<real_t> besselY0N(real_t x, number_t N)
{
  // Returns the Bessel functions of the second kind and orders 0..N, for positive x
  // Forward recursive computation
  std::vector<real_t> y0n(N+1, 0);
  std::vector<real_t>::iterator yn_i(y0n.begin());
  real_t yn_minus2 = besselY0(x), yn_minus1, yn;
  *yn_i++ = yn_minus2;
  if ( N > 0 )
  {
    *yn_i++ = yn_minus1 = besselY1(x);
    real_t Two_i_minus2(2.);
    while ( yn_i != y0n.end() )
    {
      // Use recurrence relation Y_{n}(x) = 2(n-1)/x * Y_{n-1}(x) - Y_{n-2}(x)
      *yn_i++ = yn = Two_i_minus2 * yn_minus1 / x - yn_minus2;
      yn_minus2 = yn_minus1;
      yn_minus1 = yn;
      Two_i_minus2 += 2.;
    }
  }
  return y0n;
}

/*
--------------------------------------------------------------------------------
   Modified Bessel functions of order 0, 1 of the first kind: I_0, I_1
--------------------------------------------------------------------------------
*/
real_t besselI0(real_t x)
{
  real_t y,p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7,q8,q9,ax,bx;
  p1=1.0; p2=3.5156229; p3=3.0899424; p4=1.2067429;
  p5=0.2659732; p6=0.360768e-1; p7=0.45813e-2;
  q1=0.39894228; q2=0.1328592e-1; q3=0.225319e-2; q4=-0.157565e-2;
  q5=0.916281e-2; q6=-0.2057706e-1; q7=0.2635537e-1;
  q8=-0.1647633e-1; q9=0.392377e-2;
  if (std::fabs(x) < 3.75)
  {
    y=std::pow((x/3.75),2);
    return (p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7))))));
  }
  else {
    ax=std::fabs(x);
    y=3.75/ax;
    bx=std::exp(ax)/std::sqrt(ax);
    ax=q1+y*(q2+y*(q3+y*(q4+y*(q5+y*(q6+y*(q7+y*(q8+y*q9)))))));
    return ax*bx;
  }
}

real_t besselI1(real_t x)
{
  real_t y,p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7,q8,q9,ax,bx;
  p1=0.5; p2=0.87890594; p3=0.51498869; p4=0.15084934;
  p5=0.2658733e-1; p6=0.301532e-2; p7=0.32411e-3;
  q1=0.39894228; q2=-0.3988024e-1; q3=-0.362018e-2;
  q4=0.163801e-2;q5=-0.1031555e-1; q6= 0.2282967e-1;
  q7=-0.2895312e-1; q8=0.1787654e-1; q9=-0.420059e-2;
  if (std::fabs(x) < 3.75)
  {
    y=std::pow((x/3.75),2);
    return (x*(p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7)))))));
  }
  else
  {
    ax=std::fabs(x);
    y=3.75/ax;
    bx=std::exp(ax)/std::sqrt(ax);
    ax=q1+y*(q2+y*(q3+y*(q4+y*(q5+y*(q6+y*(q7+y*(q8+y*q9)))))));
    return ax*bx;
  }
}

/*
--------------------------------------------------------------------------------
 Modified Bessel functions of order 0..N of the first kind: I_k
--------------------------------------------------------------------------------
*/
std::vector<real_t> besselI0N(real_t x, number_t n)
{
  std::vector<real_t> i0n(n+1, 0);
  std::vector<real_t>::iterator in_i(i0n.begin());
  real_t in_minus_2=besselI0(x);
  *in_i++=in_minus_2;
  if (n > 0)
  {
    real_t in_minus_1=besselI1(x), tox=2./x, bip;
    *in_i++=in_minus_1;
    number_t j=1;
    while (in_i != i0n.end())
    {
      // Use recurrence relation I_{n}(x) = I_{n-2}(x) - 2*(n-1)/x * I_{n-1}(x)
      *in_i++=bip=in_minus_2-j*tox*in_minus_1;
      in_minus_2=in_minus_1;
      in_minus_1=bip;
      j++;
    }
  }
  return i0n;
}

/*
--------------------------------------------------------------------------------
   Modified Bessel functions of order 0, 1 of the second kind: K_0, K_1
--------------------------------------------------------------------------------
*/
real_t besselK0(real_t x)
{
  real_t y,ax,p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7,tmp;
  p1=-0.57721566; p2= 0.42278420; p3=0.23069756; p4=0.3488590e-1;
  p5= 0.262698e-2; p6=0.10750e-3; p7=0.74e-5;
  q1= 1.25331414; q2=-0.7832358e-1; q3=0.2189568e-1; q4=-0.1062446e-1;
  q5= 0.587872e-2; q6=-0.251540e-2; q7=0.53208e-3;
  if (x == 0.0) return 1.e30;  //arbitrary big value
  if (x <= 2.0)
  {
    y=x*x/4.0;
    ax=-std::log(x/2.0)*besselI0(x);
    tmp = ax+(p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7))))));
    return tmp;
  }
  else
  {
    y=2.0/x;
    ax=std::exp(-x)/std::sqrt(x);
    tmp = ax*(q1+y*(q2+y*(q3+y*(q4+y*(q5+y*(q6+y*q7))))));
    return tmp;
  }
}

real_t besselK1(real_t x)
{
  real_t y,ax,p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7;
  p1=1.0; p2=0.15443144; p3=-0.67278579; p4=-0.18156897;
  p5=-0.1919402e-1; p6=-0.110404e-2; p7=-0.4686e-4;
  q1=1.25331414; q2=0.23498619; q3=-0.3655620e-1; q4=0.1504268e-1;
  q5=-0.780353e-2; q6=0.325614e-2; q7=-0.68245e-3;
  if (x == 0.0) return 1.e30;
  if (x <= 2.0)
  {
    y=x*x/4.0;
    ax=std::log(x/2.0)*besselI1(x);
    return (ax+(1.0/x)*(p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7)))))));
  }
  else
  {
    y=2.0/x;
    ax=std::exp(-x)/std::sqrt(x);
    return (ax*(q1+y*(q2+y*(q3+y*(q4+y*(q5+y*(q6+y*q7)))))));
  }
}

/*
--------------------------------------------------------------------------------
 Modified Bessel functions of order 0..N of the second kind: K_k
--------------------------------------------------------------------------------
*/
std::vector<real_t> besselK0N(real_t x, number_t n)
{
  if (x == 0.) return std::vector<real_t>(n+1,1.e30);
  std::vector<real_t> k0n(n+1, 0);
  std::vector<real_t>::iterator kn_i(k0n.begin());
  real_t kn_minus_2=besselK0(x);
  *kn_i++=kn_minus_2;
  if (n > 0)
  {
    real_t kn_minus_1=besselK1(x), tox=2./x, bkp;
    *kn_i++=kn_minus_1;
    number_t j=1;
    while (kn_i != k0n.end())
    {
      // Use recurrence relation K_{n}(x) = K_{n-2}(x) + 2*(n-1)/x * K_{n-1}(x)
      *kn_i++=bkp=kn_minus_2+j*tox*kn_minus_1;
      kn_minus_2=kn_minus_1;
      kn_minus_1=bkp;
      j++;
    }
  }
  return k0n;
}

/*
--------------------------------------------------------------------------------
 Hankel functions of order 0 and 1 of the first kind: H1_0, H1_1
--------------------------------------------------------------------------------
*/
complex_t hankelH10(real_t x)
{
  // Returns the Hankel function of order 0, H^(1)_0(x) = J_0(x) + i*Y_0(x) for any positive x.
  real_t j0, y0;
  if ( x < eight)
  {
    j0 = besselJ0(x);
    if ( x <= 0 )
    { y0 = -theRealMax; }
    else if ( x < theTolerance )
    { y0 = two_over_pi * (std::log(x) + std::log(.5) + theEulerConst); }
    else if ( x < 3.)
    { y0 = besselY0withoutSingularity(x) + two_over_pi * std::log(x / Y0_z[0]) * j0; }
    else if ( x < 5.5)
    { y0 = besselY0withoutSingularity(x) + two_over_pi * std::log(x / Y0_z[1]) * j0; }
    else
    { y0 = besselY0withoutSingularity(x) + two_over_pi * std::log(x / Y0_z[2]) * j0; }
  }
  else
  {
    real_t t = x - pi_over4;
    real_t cost = std::cos(t); real_t sint = std::sin(t);
    real_t eight_over_x = eight / x;
    real_t x2 = eight_over_x * eight_over_x;
    real_t r0 = hornerAlgorithmTpl(x2, jy0_p0, jy0_p0 + 6, zero) / hornerAlgorithmTpl(x2, jy0_q0, jy0_q0 + 5, one);
    real_t r1 = hornerAlgorithmTpl(x2, jy0_p1, jy0_p1 + 6, zero) / hornerAlgorithmTpl(x2, jy0_q1, jy0_q1 + 5, one);
    j0 = sqrt2_over_pi / std::sqrt(x) * (r0 * cost - r1 * eight_over_x * sint);
    y0 = sqrt2_over_pi / std::sqrt(x) * (r0 * sint + r1 * eight_over_x * cost);
  }
  return complex_t(j0, y0);
}

complex_t hankelH11(real_t x)
{
  // Returns the Hankel function of order 1, H^(1)_1(x) = J_1(x) + i*Y_1(x) for any positive x.
  real_t j1, y1;
  if ( x < eight)
  {
    j1 = besselJ1(x);
    if ( x <= 0 )
    { y1 = -theRealMax; }
    else if ( x < theTolerance )
    { y1 = -two_over_pi / x; }
    else if ( x < four)
    { y1 = besselY1withoutSingularity(x) + two_over_pi * std::log(x / Y1_z[0]) * j1; }
    else
    { y1 = besselY1withoutSingularity(x) + two_over_pi * std::log(x / Y1_z[1]) * j1; }
  }
  else
  {
    real_t t = x - three_pi_over4;
    real_t cost = std::cos(t); real_t sint = std::sin(t);
    real_t eight_over_x = eight / x;
    real_t x2 = eight_over_x * eight_over_x;
    real_t r0 = hornerAlgorithmTpl(x2, jy1_p0, jy1_p0 + 6, zero) / hornerAlgorithmTpl(x2, jy1_q0, jy1_q0 + 6, one);
    real_t r1 = hornerAlgorithmTpl(x2, jy1_p1, jy1_p1 + 6, zero) / hornerAlgorithmTpl(x2, jy1_q1, jy1_q1 + 6, one);
    j1 = sqrt2_over_pi / std::sqrt(x) * (r0 * cost - r1 * eight_over_x * sint);
    y1 = sqrt2_over_pi / std::sqrt(x) * (r0 * sint + r1 * eight_over_x * cost);
  }
  return complex_t(j1, y1);
}

/*
--------------------------------------------------------------------------------
 Hankel functions of order 0..N of the first kind: H1_k
--------------------------------------------------------------------------------
*/
std::vector<complex_t> hankelH10N(real_t x, number_t N)
{
  // Returns the Hankel functions up to order n: H^(1)_k(x) = J_k(x) + i*Y_k(x) for any positive x.
  if (std::abs(real_t(N)/x)>1)
  {
    real_t Nr=real_t(N);
    real_t b=acosh(Nr/x);
    if (std::log(0.5*theRealMax)< (std::log(std::sqrt(2./(pi_*x*std::sinh(b))))+Nr*(b-std::tanh(b))))
    {
      where("hankelH10N");
      error("too_large", "N");
    }
  }
  std::vector<real_t> j0n=besselJ0N(x,N);
  std::vector<real_t> y0n=besselY0N(x,N);
  std::vector<complex_t> h10n(N+1,complex_t(0.));
  std::vector<real_t>::iterator itj=j0n.begin(), ity=y0n.begin();
  std::vector<complex_t>::iterator ith=h10n.begin();
  for(;ith!=h10n.end(); ++ith, ++itj, ++ity) *ith = complex_t(*itj,*ity);
  return h10n;
}

/*
--------------------------------------------------------------------------------
 Hankel functions of order 0..N of the second kind: H2_k
--------------------------------------------------------------------------------
*/
std::vector<complex_t> hankelH20N(real_t x, number_t N)
{
  std::vector<complex_t> h20n(N+1,complex_t(0.));
  for (number_t i=0; i <= N; ++i)
  {
    h20n[i]=zhankel(complex_t(x, 0.), 2, i);
  }
  return h20n;
}

/*
--------------------------------------------------------------------------------
   Spherical Bessel functions of the first (j_n) and second kind (y_n)
--------------------------------------------------------------------------------
*/
std::vector<real_t> sphericalbesselJ0N(real_t x, number_t NN)
{
  // Returns the Spherical Bessel functions of the first kind and orders 0..N, for positive x
  std::vector<real_t> j0n(NN + 1, 0);
  std::vector<real_t>::iterator jn_i = j0n.begin();
  int N(NN);
  if ( N > 0 )
  {
    if ( x < 1 )
    {
      // Use ascending series for small argument
      // http://www.math.sfu.ca/~cbm/aands/page_437.htm
      // initialize (2i+1) and x^i / ( 1*3*5...(2i+1))
      real_t Two_i_plus1(one), x_i_slash(one), x2over2(-x * x / 2.);
      for ( jn_i = j0n.begin(); jn_i != j0n.end(); jn_i++ )
      {
        *jn_i = one;
        int k(0);
        real_t term_k(one);
        real_t Two_i_plus_2k_plus1(Two_i_plus1);
        while ( std::abs(x_i_slash * term_k) > theEpsilon )
        {
          // compute (-x*x/2)^k / ( k! (2i+3)(2i+5)...(2i+2k+1) )
          k++; Two_i_plus_2k_plus1 += 2;
          term_k = x2over2 * term_k / (k * Two_i_plus_2k_plus1);
          *jn_i += term_k;
        }
        // multiply by x^i / ( 1*3*5...(2i+1))
        *jn_i *= x_i_slash;
        Two_i_plus1 += 2;
        x_i_slash *= x / Two_i_plus1;
      }
    }
    else
    {
      // Use backwards recursive computation
      // Miller's method:  http://www.math.sfu.ca/~cbm/aands/page_452.htm
      // Estimate a lower bound low_bound such J_{low_bound}(x) is pretty close to 0
      int low_bound(21);
      if (x < 5.)       { low_bound = static_cast<number_t>(21 + 4.0 * x); }
      else if (x < 10.) { low_bound = static_cast<number_t>(28 + 2.4 * x); }
      else if (x < 20.) { low_bound = static_cast<number_t>(32 + 2.0 * x); }
      else if (x < 50.) { low_bound = static_cast<number_t>(42 + 1.5 * x); }
      else { low_bound = static_cast<number_t>(52 + 1.3 * x); }

      real_t jnp2(zero), jnp1(1.e-30), jn;
      if (low_bound <= N) { j0n[low_bound] = jnp1; }
      real_t Two_i_minus1 = low_bound + low_bound + 1.;
      for ( int i = low_bound - 1; i >= 0 ; i-- )
      {
        // Use recurrence relation j_{n}(x) = (2n-1)/x * j_{n-1}(x) - j_{n-2}(x)
        jn = Two_i_minus1 * jnp1 / x - jnp2;
        jnp2 = jnp1; jnp1 = jn;
        if ( i <= N ) { j0n[i] = jn; } // store only required range of values
        if ( std::abs(jn) > 1.e+40 )
        {
          // Recurrence might yield big numbers, in this case we renormalize
          real_t factor = 1.e+40 / jn;
          jnp2 *= factor; jnp1 *= factor;
          for ( jn_i = j0n.begin() + i ; jn_i != j0n.end(); jn_i++) { *jn_i *= factor; }
        }
        Two_i_minus1 -= 2.;
      }
      real_t j0 = std::sin(x) / x;
      jn_i = j0n.begin();
      real_t factor = j0 / (*jn_i);
      *jn_i = j0;
      for ( jn_i = j0n.begin() + 1; jn_i != j0n.end(); jn_i++ ) { *jn_i *= factor; }
    }
  }
  return j0n;
}

std::vector<real_t> sphericalbesselY0N(real_t x, number_t N)
{
  // Returns the Spherical Bessel functions of the second kind and orders 0..N, for positive x
  // Forward recursive computation
  std::vector<real_t> y0n(N + 1, 0);
  std::vector<real_t>::iterator yn_i = y0n.begin();
  real_t yn_minus2 = -std::cos(x) / x, yn_minus1, yn;
  *yn_i++ = yn_minus2;
  if ( N > 0 )
  {
    *yn_i++ = yn_minus1 = (yn_minus2 - std::sin(x)) / x;
    real_t Two_i_minus1(3.);
    for ( std::vector<real_t>::iterator it = y0n.begin() + 2; it != y0n.end(); it++, yn_i++ )
    {
      // Use recurrence relation y_{n}(x) = (2n-1)/x * y_{n-1}(x) - y_{n-2}(x)
      *yn_i = yn = Two_i_minus1 * yn_minus1 / x - yn_minus2;
      yn_minus2 = yn_minus1;
      yn_minus1 = yn;
      Two_i_minus1 += 2.;
    }
  }
  return y0n;
}

/*
--------------------------------------------------------------------------------
   Struve functions of ordre 0 and 1 : H_0, H_1
--------------------------------------------------------------------------------
*/
real_t struveNotH0(real_t x)
{
  // Struve function of order 0 : returns H_0(x) for any |x|<8.
  //                              returns H_0(x)-Y_0(x) for any |x|>8.
  real_t h0, absx(std::abs(x)), t0(one), t1, t2, t, two_t;
  if ( absx < 8 )
  {
    t1 = t = x / eight; two_t = t + t;
    h0 = ch0[0] * t;
    for ( int i = 1; i < 13; i++ )
    {
      t2 = two_t * t1 - t0; t1 = two_t * t2 - t1; t0 = t2;
      h0 += ch0[i] * t1;
    }
  }
  else
  {
    t1 = t = eight / x; two_t = t + t;
    h0 = ch0minusy0[0];
    for ( int i = 1; i < 13; i++ )
    {
      t2 = two_t * t1 - t0; t1 = two_t * t2 - t1; t0 = t2;
      h0 += ch0minusy0[i] * t2;
    }
    h0 = two_over_pi * h0 / x;
  }
  return h0;
}

real_t struveNotH1(real_t x)
{
  // Struve function of order 1, H_1(x) : returns H_1(x) for any |x|<8.
  //                                      returns H_1(x)-Y_1(x) for any |x|>8.
  real_t h1, absx(std::abs(x)), t0(one), t1, t2, t, two_t;
  if ( absx < eight )
  {
    t1 = t = x / eight; two_t = t + t;
    h1 = ch1[0];
    for ( int i = 1; i < 13; i++ )
    {
      t2 = two_t * t1 - t0; t1 = two_t * t2 - t1; t0 = t2;
      h1 += ch1[i] * t2;
    }
  }
  else
  {
    t1 = t = eight / x; two_t = t + t;
    h1 = ch1minusy1[0];
    for ( int i = 1; i < 13; i++ )
    {
      t2 = two_t * t1 - t0; t1 = two_t * t2 - t1; t0 = t2;
      h1 += ch1minusy1[i] * t2;
    }
    h1 = two_over_pi * h1;
  }
  return h1;
}

std::pair<real_t, real_t> struveNotH01(real_t x)
{
  // Compute Struve functions H0 and H1 for any real x
  // returns H0(x) and H1(x) for |x| < 8
  // returns H0(x)-Y0(x) and H1(x)-Y1(x) for |x| > 8.
  real_t h0, h1, absx(std::abs(x)), t0(one), t1, t2, t, two_t;
  if ( absx < eight )
  {
    t1 = t = x / eight; two_t = t + t;
    h0 = ch0[0] * t;
    h1 = ch1[0];
    for ( int i = 1; i < 13; i++ )
    {
      t2 = two_t * t1 - t0; t1 = two_t * t2 - t1; t0 = t2;
      h0 += ch0[i] * t1; h1 += ch1[i] * t2;
    }
  }
  else
  {
    t1 = t = eight / x; two_t = t + t;
    h0 = ch0minusy0[0];
    h1 = ch1minusy1[0];
    for ( int i = 1; i < 13; i++ )
    {
      t2 = two_t * t1 - t0; t1 = two_t * t2 - t1; t0 = t2;
      h0 += ch0minusy0[i] * t2; h1 += ch1minusy1[i] * t2;
    }
    h0 *= two_over_pi / x; h1 *= two_over_pi;
  }
  return std::pair<real_t, real_t>(h0, h1);
}

/*
--------------------------------------------------------------------------------
 Test of special functions: comparison with tables in pp. 390-397 & pp. 457--458
 HANDBOOK Of MATHEMATICAL FUNCTIONS , Ed. M.ABRAMOWITZ & I.A. STEGUN
--------------------------------------------------------------------------------
*/
void besselJY01Test(std::ostream& out)
{
  // Abramowitz & Stegun pp. 390-397
  int page(390), pagejump(51);
  real_t x(0.), xx;
  out.setf(std::ios::scientific);
  out << std::endl << std::endl << "     Page " << page << "                                     Page " << page + 1 << std::endl
            << std::endl << "     x   J0(x)               J1(x)             "
            << "     x   Y0(x)               Y1(x)" << std::endl;
  out << std::endl << "    0.0  1.000000000000e+00  0.000000000000e+00"
            << "    0.0 -inf                -inf";
  for ( int i = 1; i < 204; i++ )
  {
    x += 0.1;
    if ( i == pagejump )
    {
      page += 2;
      out << std::endl << std::endl << "     Page " << page << "                                     Page " << page + 1 << std::endl
                << std::endl << "     x   J0(x)               J1(x)             "
                << "     x   Y0(x)               Y1(x)" << std::endl;
      x -= 0.1;
      pagejump += 51;
    }
    xx = x + .0001;
    real_t J0 = besselJ0(x), J1 = besselJ1(x);
    real_t Y0 = besselY0(x), Y1 = besselY1(x);
    out << std::endl << std::setw(5) << int(xx) << "." << int(10 * xx) - 10 * int(xx)
              << std::setw(20) << std::setprecision(12) << J0 << std::setw(20) << std::setprecision(12) << J1
              << std::setw(5) << int(xx) << "." << int(10 * xx) - 10 * int(xx)
              << std::setw(20) << std::setprecision(12) << Y0 << std::setw(20) << std::setprecision(12) << Y1;
  }
}

void sphericalbesselJ0NTest(std::ostream& out)
{
  // Abramowitz & Stegun pp. 457--458
  std::vector<real_t> jn(3);
  std::vector<real_t>::const_iterator jn_i;
  out.setf(std::ios::scientific);
  out << std::endl << std::endl << "     Page 457" << std::endl
            << std::endl << "     x     j0(x)               j1(x)               j2(x)" << std::endl;
  real_t x(0.1), xx;
  for ( int i = 0; i < 50; i++ )
  {
    xx = x + .0001;
    out << std::endl << std::setw(5) << int(xx) << "." << int(10 * xx) - 10 * int(xx);
    jn = sphericalbesselJ0N(x, 2);
    for ( jn_i = jn.begin(); jn_i != jn.end(); jn_i++ )
    { out << std::setw(20) << std::setprecision(12) << *jn_i; }
    x += 0.1;
  }
  out << std::endl << std::endl << "     Page 458" << std::endl
            << std::endl << "     x     j0(x)               j1(x)               j2(x)";
  x = 5.0;
  for ( int i = 0; i < 51; i++ )
  {
    xx = x + .0001;
    out << std::endl << std::setw(5) << int(xx) << "." << int(10 * xx) - 10 * int(xx);
    jn = sphericalbesselJ0N(x, 2);
    for ( jn_i = jn.begin(); jn_i != jn.end(); jn_i++ )
    { out << std::setw(20) << std::setprecision(12) << *jn_i; }
    x += 0.1;
  }
}

} // end of namespace xlifepp
