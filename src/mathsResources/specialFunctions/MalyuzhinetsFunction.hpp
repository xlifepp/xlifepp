/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file MalyuzhinetsFunction.hpp
  \author E. Lunéville
  \since 17 dec 2019
  \date  17 dec 2019

  \brief Malyuzhinets function stuff
*/
/*
-------------------------------------------------------------------------------------
 Malyuzhinets function involved in exact solutions of accoustic diffraction by a wedge

      0<= Phi <pi the wedge half angle
      Phi = pi corresponds to the crack, Phi=pi/2 to an half plane

             -----\
             ------\   Phi angle
             wedge  \____________
             -------/
             ------/
             -----/

                /     /+inf                              \
                |   1 |    1      cosh(z.t)-1            |
    psi(z)= exp | - - |    - ----------------------- dt  |     for |Re z|<2Phi+pi/2
                |   2 |    t cosh(pi.t/2)sinh(2Phi.t)    |
                \    /0                                  /

   psi(z+2Phi) = psi(z-2phi)cot(z/2+pi/4)                      for |Re z|>=2Phi+pi/2
                                                                                   _   ______
   main properties: analytic in the strip |Re z|<2Phi+pi/2,  psi(-z)=psi(z)   psi(z)= psi(z)
                     has zeros at znm+- = +-[2Phi + pi/2 +2n.Phi + 2m.pi]  m even
                     has poles at znm+- = +-[2Phi + pi/2 +2n.Phi + 2m.pi]  m odd  (may be balanced regarding Phi)

   * Gamma factorization  psi(z)=psiN(z)/psiN(0)
                                                  m+1                            m+1
                         / 2Phi+pi/2+ m.pi+z \(-1)      / 2Phi+pi/2+ m.pi-z \(-1)
   psiN(z)= Product gamma| ----------------- |     gamma|------------------ |
             m=0,N       \       4Phi        /          \       4Phi        /

                  /      /+inf                                             \
                  |    N |    exp(-(N+1)pi.t)        cosh(z.t)-1           |
           * exp  |(-1)  |    --------------- ------------------------- dt |     for |Re z|<2Phi+pi/2+(N+1)pi
                  |      |          t          cosh(pi.t/2)sinh(2Phi.t)    |
                  \     /0                                                 /
Note: the integrand is more exponentially decreasing
       and poles and zeros |znm+-|<=2Phi+pi/2+ N.pi have been removed from integrand

The Malyuzhinets class provides the computation of the gamma factorized Malyuzhinets function
by choosing the number of gamma functions (0 no gamma function so compute the original form)
the integral involved may be approximated in 5 ways
    - trapeze on  ]0,tf[ using nq points (tf is a priori estimated) : _trapezeCal_ and adaptive=false
    - laguerre on ]0,+inf[ using nq quadrature points: _laguerreCal_ and adaptive=false
    - adaptative trapeze on ]0,tf[ with tolerance eps: _defaultCal_ and adaptive=true
    - adaptive trapeze on ]0,1/2[ with tolerance eps,
      and trapeze on ]1/2,tf[ using nq quadrature points: _trapezeCal_ and adaptive=true
    - adaptive trapeze on ]0,1/2[ with tolerance eps,
      and laguerre on ]1/2,+inf[ using nq quadrature points: _laguerreCal_ and adaptive=true

References:
 Babich V., Lyalinov M., Grikurov V., Diffraction theory, The Sommerfeld-Malyuzhinets Technique, Alpha Science (2008)
 Bernard J.M.L., About field expressions for the diffraction by an anisotropic impedance wedge at skew incidence
                 and relations with Gamma function, URSI EM Theory Symposium, EMTS 2019

---------------------------------------------------------------------------------------------------------------
*/

#ifndef MALYUZHINETS_FUNCTION_HPP
#define MALYUZHINETS_FUNCTION_HPP

#include "utils.h"
#include "config.h"
#include "specialFunctions.hpp"

namespace xlifepp
{
    enum FieldPart{_totalField,_diffractedField,_wedgeField};

/*!
  \class Malyuzhinets
  describes the class providing the computation of Malyuzhinets function
  involved in wedge diffraction
*/
class Malyuzhinets
{
  public:
    real_t Phi;         //!< wedge angle
    number_t ngamma;    //!< number of Gamma function (default 2)
    CalType quad;       //!< quadrature method (_trapezeCal, _laguerreCal*,_defaultCal),
    bool adaptive;      //!< true* if local or global adaptive quadrature must be used
    number_t nq;        //!< number of quadrature points (default 32)
    real_t eps;         //!< relative tolerance when using adaptive quadrature (default 1E-4)
    Parameters pars;    //!< internal parameters
    mutable complex_t z_;  //!< current z
    mutable std::vector<real_t> lagPoints; //!< to store Laguerre points
    mutable std::vector<real_t> lagWeights;//!< to store Laguerre weights

    Malyuzhinets(real_t wa, int_t ng=2, CalType qr=_laguerreCal, bool adapt=true); //!< constructor

    complex_t compute(complex_t z) const;                           //!< compute Malyuzhinets function at a point z
    std::vector<complex_t> compute(const std::vector<complex_t>& zs,
                                   bool parallel=true) const;       //!< compute Malyuzhinets function at points zs
    complex_t operator()(complex_t z) const                         //!< compute Malyuzhinets function at a point z
      {return compute(z);}
    std::vector<complex_t> operator()(const std::vector<complex_t>& zs,
                                      bool parallel=true) const     //!< compute Malyuzhinets function at points zs
      {return compute(zs,parallel);}
    complex_t integrand(real_t t, const complex_t& z) const;            //!< integrand at (t,z)
    std::vector<complex_t> integrand(const std::vector<real_t>& ts, const complex_t& z) const; //!< integrand at (ts,z)
    complex_t integral(real_t t0, real_t tf, const complex_t& z) const;                            //!< integral at z
    std::vector<complex_t> integral(real_t t0, real_t tf, const std::vector<complex_t>& zs) const; //!< integral at zs
    void print(std::ostream& out) const;            //print class
    void print(PrintStream& os) const {print(os.currentStream());}
};

inline std::ostream& operator<<(std::ostream& out, const Malyuzhinets& mal)
{
    mal.print(out);
    return out;
}

complex_t mal_integrand(real_t t, Parameters& pars=defaultParameters);  //! integrand at (t,z), extern call

//stable function sinh(x)/x (used by Malyuzhinets::integrand)
template <typename T> T sinhs(const T& x)
  {return (std::abs(x) <= 1E-10) ? 1. : std::sinh(x)/x;}


/*! compute wedge diffraction at (kr,phi) using either Somerfeld paths or Steepest Decent paths
    Phi: wedge angle , phi0 : incidence angle, tau=pi/2Phi,

                1    /                         cos tau.phi0
 u_(kr,phi) = -----  | exp(-ikr.cos z)  ----------------------------- dz    (Dirichlet on both sides)
              4i.Phi /path              sin tau(phi+z) - sin tau.phi0

                1    /                        cos tau(phi+z)
 u_(kr,phi) = -----  | exp(-ikr.cos z) ----------------------------- dz    (Neumann on both sides)
             4i.Phi /path              sin tau(phi+z) - sin tau.phi0


                1    /                 PsiP(z+phi)PsiM(z+phi)        cos tau.phi_0
 u_(kr,phi) = -----  | exp(-ikr.cos z) ---------------------- ----------------------------- dz   ( Fourier dn(u) - ik.sin thetaP/M u =0 on SigmaP/M)
             4i.Phi /path                PsiP(phi0)PsiM(phi0) sin tau(phi+z) - sin tau.phi0

 with   psiP(z)=psi(Phi+z+thetaP-pi/2)psi(Phi+z-thetaP+pi/2)      psiM(z)=psi(-Phi+z-thetaM+pi/2)psi(-Phi+z+thetaM-\pi/2)

 important notes:
  - To deal with Neuman and Dirichlet condition, use Fourier with tethaP=0 and thetaM = -100i for instance
  - Computation is hazardous for wedge angle Phi < pi/2 (multiple reflexion)
  - Re(thetaMP)<0 (reactive material) induces exponentially growing solutions
  - heavy computation when Malyuzhinets function is involved

  There are many ways to compute the wedge diffraction depending on
      - the paths chosen,
      - the Malyuzineths object is passed or not (Fourier mode)
      - the number of kr or phi passed (one or few)
  Note: if Malyuzhinets object is not passed but required, it will be construct internally at each call (additional cost)
         if more than point given, point loop is parallelized else quadrature loop is parallelized (if OMP available)
*/

//Dirichlet kernel for 2D Helmholtz wedge diffraction u=0
inline
complex_t wedge_dir(const complex_t& z, real_t phi, real_t phi0, const complex_t& thetaP, const complex_t& thetaM,
                    const complex_t& coeffP, const complex_t& coeffM, bool istd, const Malyuzhinets& mal)
{
    real_t Phi=mal.Phi, tau=pi_/(2*Phi);
    return std::cos(tau*phi0)/(std::sin(tau*(phi+z))-std::sin(tau*phi0));
}

inline
complex_t wedge_dndirP(const complex_t& z, real_t Phi, real_t phi0)
{
    //dn = -dphi (wedge outward normal)
    real_t tau=pi_/(2*Phi);
    complex_t d=std::cos(tau*z)-std::sin(tau*phi0);
    return -tau*std::cos(tau*phi0)*std::sin(tau*z)/(d*d);
}

inline
complex_t wedge_dndirM(const complex_t& z, real_t Phi, real_t phi0)
{
    //dn = dphi (wedge outward normal)
    real_t tau=pi_/(2*Phi);
    complex_t d=std::cos(tau*z)+std::sin(tau*phi0);
    return -tau*std::cos(tau*phi0)*std::sin(tau*z)/(d*d);
}

//Neumann kernel for 2D Helmholtz wedge diffraction du/dn=0
inline
complex_t wedge_neu(const complex_t& z, real_t phi, real_t phi0, const complex_t& thetaP, const complex_t& thetaM,
                    const complex_t& coeffP, const complex_t& coeffM, bool istd, const Malyuzhinets& mal)
{
    real_t Phi=mal.Phi, tau=pi_/(2*Phi);
    return std::cos(tau*(phi+z))/(std::sin(tau*(phi+z))-std::sin(tau*phi0));
}

//Fourier kernel for 2D Helmholtz wedge diffraction du/dn-iksin(theta)u=0
inline
complex_t wedge_fou(const complex_t& z, real_t phi, real_t phi0, const complex_t& thetaP, const complex_t& thetaM,
                    const complex_t& coeffP, const complex_t& coeffM, bool istd,const Malyuzhinets& mal)
{
    real_t Phi=mal.Phi, tau=pi_/(2*Phi), pis2=pi_/2;
    complex_t zp=z+phi+Phi, zm=z+phi-Phi, sp=thetaP-pis2, sm=thetaM-pis2;
    complex_t G=std::cos(tau*phi0)/(std::sin(tau*(phi+z))-std::sin(tau*phi0));
    if(!istd) // non standard mode
    {
        complex_t tp=thetaP+pis2, tm=thetaM+pis2;
        if(real(thetaP)<0) G *= coeffP/(mal(zp-tp)*mal(zp+tp));
        else               G *= mal(zp+sp)*mal(zp-sp)/coeffP;
        if(real(thetaM)<0) G *= coeffM/(mal(zm-tm)*mal(zm+tm));
        else               G *= mal(zm+sm)*mal(zm-sm)/coeffM;
    }
    else // standard mode
   {
       G*=mal(zp+sp)*mal(zp-sp)*mal(zm+sm)*mal(zm-sm)/(coeffP*coeffM);
   }
   return G;
}

//diffraction functions using SD path
void checkBC(BcType& bcP, BcType& bcM, complex_t& thetaP, complex_t& thetaM);
std::vector<complex_t> wedgeDiffractionSD(const std::vector<real_t>& krs, const std::vector<real_t>& phis, real_t Phi, real_t phi0, FieldPart fp,
                                          real_t kra, BcType bcP, BcType bcM=_undefEcType, complex_t thetaP=0, complex_t thetaM=0);
std::vector<complex_t> wedgeDiffractionSD_par(const std::vector<real_t>& krs, const std::vector<real_t>& phis, real_t Phi, real_t phi0, FieldPart fp,
                                          real_t kra, BcType bcP, BcType bcM=_undefEcType, complex_t thetaP=0, complex_t thetaM=0);

inline complex_t wedgeDiffractionSD(real_t kr, real_t phi, real_t Phi, real_t phi0, FieldPart fp, real_t kra, BcType bcP, BcType bcM=_undefEcType,
                                    complex_t thetaP=0, complex_t thetaM=0)
{
    std::vector<real_t> phis(1,phi), krs(1,kr);
    return wedgeDiffractionSD(krs,phis,Phi,phi0,fp,kra,bcP,bcM,thetaP,thetaM)[0];
}
inline complex_t wedgeDiffractionSD_par(real_t kr, real_t phi, real_t Phi, real_t phi0, FieldPart fp, real_t kra, BcType bcP, BcType bcM=_undefEcType,
                                    complex_t thetaP=0, complex_t thetaM=0)
{
    std::vector<real_t> phis(1,phi), krs(1,kr);
    return wedgeDiffractionSD_par(krs,phis,Phi,phi0,fp,kra,bcP,bcM,thetaP,thetaM)[0];
}

std::vector<complex_t> wedgeCurrentSD(const std::vector<real_t>& krs, real_t Phi, int uplow, real_t phi0, FieldPart fp, real_t kra, BcType bcP, BcType bcM=_undefEcType,
                                    complex_t thetaP=0, complex_t thetaM=0);
std::vector<complex_t> wedgeCurrentSD_par(const std::vector<real_t>& krs, real_t Phi, int uplow, real_t phi0, FieldPart fp, real_t kra, BcType bcP, BcType bcM=_undefEcType,
                                    complex_t thetaP=0, complex_t thetaM=0);

std::vector<complex_t> wedgeDirCurrentSD(const std::vector<real_t>& krs, real_t Phi, real_t phi0, FieldPart fp, int uplow);

//diffraction functions using Sommerfeld path
std::vector<complex_t> wedgeDiffractionSOM(const std::vector<real_t>& krs, const std::vector<real_t>& phis, real_t Phi, real_t phi0, FieldPart fp,
                                           BcType bcP, BcType bcM=_undefEcType, complex_t thetaP=0, complex_t thetaM=0);

inline complex_t wedgeDiffractionSOM(real_t kr, real_t phi, real_t Phi, real_t phi0, FieldPart fp, BcType bcP, BcType bcM=_undefEcType,
                                     complex_t thetaP=0, complex_t thetaM=0)
{
    std::vector<real_t> phis(1,phi), krs(1,kr);
    return wedgeDiffractionSOM(krs,phis,Phi,phi0,fp,bcP,bcM,thetaP,thetaM)[0];
}

std::vector<complex_t> wedgeCurrentSOM(const std::vector<real_t>& krs, real_t Phi, real_t phi0, FieldPart fp, BcType bcP, BcType bcM=_undefEcType,
                                    complex_t thetaP=0, complex_t thetaM=0);

std::vector<complex_t> wedgeDirCurrentSOM(const std::vector<real_t>& krs, real_t Phi, real_t phi0, FieldPart fp, BcType bcP, BcType bcM=_undefEcType);


} // end of namespace xlifepp

#endif
