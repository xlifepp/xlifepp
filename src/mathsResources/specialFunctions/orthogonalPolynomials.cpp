/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file orthogonalPolynomials.cpp
  \author D. Martin
  \since 24 nov 2004
  \date 18 jun 2011

  \brief Implementation of orthogonal polynomials functions
 */

#include "specialFunctions.hpp"

namespace xlifepp
{

/*
-------------------------------------------------------------------------------
.Chebyshev polynomials (of the first kind) on [-1, 1] up to order n
.  Tn = cos( n acos(x))
. Recurrence relation:
.   T_0(x) = 1, T_1(x) = x
.   T_n(x) = 2 x T_{n-1}(x) - T_{n-1}(x) , n > 1
-------------------------------------------------------------------------------
*/
void chebyshevPolynomials(real_t x, std::vector<real_t>& val)
{
  real_t p_n(x), p_n1(1.), p_n2, Two_x(x + x);

  std::vector<real_t>::iterator val_i(val.begin());
  *val_i++ = p_n1;
  if ( val.size() > 1 ) { *val_i = p_n; }
  for ( val_i = val.begin() + 2; val_i < val.end(); val_i++)
  {
    p_n2 = p_n1; p_n1 = p_n; *val_i = p_n = Two_x * p_n1 - p_n2;
  }
  // alternately: using Jacobi polynomials
  // JacobiPolynomials(-0.5,-0.5, x, val);
}
/*
-------------------------------------------------------------------------------
.Gegenbauer ultraspherical polynomials on [-1, 1], with parameter lambda up to order n
. Recurrence relation:
.  P_0 = 1, P_1 = 2*lambda*x
.  n*P_n(x) = 2*(n+lambda-1)*x*P_{n-1}(x) - (n+2*lambda-2)*P_{n-2}(x) , n > 1
. P_n is Jacobi polynomial with a = b = lambda - 1/2
-------------------------------------------------------------------------------
*/
void gegenbauerPolynomials(real_t lambda, real_t x, std::vector<real_t>& val)
{
  real_t p_n(1.), p_n1(0.), p_n2, Lambdapn_1(lambda), TwoLambdapn_2(2 * lambda - 1.);

  std::vector<real_t>::iterator val_i(val.begin());
  *val_i = p_n;
  int n(1);
  for ( val_i = val.begin() + 1; val_i < val.end(); val_i++, n++ )
  {
    p_n2 = p_n1; p_n1 = p_n;
    *val_i = p_n = ( 2.*Lambdapn_1 * x * p_n1 - TwoLambdapn_2 * p_n2) / n;
    Lambdapn_1 += 1.; TwoLambdapn_2 += 2.;
  }
  // alternately: using Jacobi polynomials
  // JacobiPolynomials(lambda-0.5,lambda-0.5, n, x, val);
} //@end GegenbauerPolynomials
/*
-------------------------------------------------------------------------------
.Jacobi polynomials on [-1, 1] up to order n (cf. Abramowitz & Stegun p.382)
.  Orthogonal polynomials with weight (1-x)^a (1+x)^b
. Recurrence relation:
.  P_0 = 1, P_1 = (2*(a+1)+a+b+2)*(x-1) / 2
.  P_{n+1}*{ 2*(n+1)(n+a+b+1)(2*n+a+b) } =
.    { (2*n+1+a+b)(a^2-b^2) + (2*n+a+b)(2*n+1+a+b)(2*n+2+a+b)*x } * P_{n}
.   -{ 2*(n+a)*(n+b)(2*n+2+a+b) } * P_{n-1} , n > 0
-------------------------------------------------------------------------------
*/
void jacobiPolynomials(real_t a, real_t b, real_t x,
                       std::vector<real_t>& val)
{
  real_t p_n(1), p_n1, p_n2, a2_b2(a * a - b * b), apb(a + b), Two_npapb(a + b + 2);

  std::vector<real_t>::iterator val_i(val.begin());
  *val_i++ = p_n1 = 1.;
  if ( val.size() > 1 ) { p_n = *val_i = .5 * (2 * (a + 1) + a + b + 2) * (x - 1); }
  int n(1);
  for ( val_i = val.begin() + 2; val_i < val.end(); val_i++, n++ )
  {
    p_n2 = p_n1; p_n1 = p_n;
    *val_i = p_n =
               ( ( (Two_npapb + 1) * (a2_b2) + Two_npapb * (Two_npapb + 1) * (Two_npapb + 2) * x ) * p_n1
                 - 2 * (n + a) * (n + b) * (Two_npapb + 2) * p_n2 ) / (2 * Two_npapb * (n + 1) * (n + apb + 1) );
    Two_npapb += 2;
  }
} //@end JacobiPolynomials
/*
-------------------------------------------------------------------------------
.Jacobi polynomials on [0, 1] up to order n
-------------------------------------------------------------------------------
*/
void jacobiPolynomials01(real_t a, real_t b, real_t x,
                         std::vector<real_t>& val)
{
  jacobiPolynomials(a, b, 2 * x - 1, val);
}
/*
--------------------------------------------------------------------------------
.Legendre polynomials on [-1, 1] up to order n: val[n]=P_n(x)
. Recurrence relation:
.   P_0(x) = 1, P_1(x) = x
. n*P_n(x) = (2*n-1) x*P_{n-1}(x) - (n-1) P_{n-2}(x) , n > 1
. P_n is Gegenbauer ultraspherical polynomial with lambda = 1/2
. or     Jacobi polynomial with a = b = 0
--------------------------------------------------------------------------------
*/
void legendrePolynomials(real_t x, std::vector<real_t>& val)
{
  std::vector<real_t>::iterator val_i(val.begin());
  real_t p_n(x), p_n1(1.), p_n2;
  int n(1), nm1(0), nx2m1(1);
  // P_0(x) = 1. , P_1(x) = x
  *val_i++ = p_n1;
  *val_i++ = p_n;
  for ( val_i = val.begin() + 2; val_i < val.end(); n++, nm1++, nx2m1 += 2, val_i++ )
  {
    // compute P_{n-2}, P_{n-1}, P_{n} by recursion:
    // k*P_k(x) = (2*k-1)*x P_{k-1}(x) - (k-1) P_{k-2}(x)
    p_n2 = p_n1; p_n1 = p_n; *val_i = p_n  = ( nx2m1 * x * p_n1 - nm1 * p_n2 ) / n;
  }
  // alternately using Gegenbauer polynomials
  // GegenbauerPolynomials(0.5, x, val);
  // alternately using Jacobi polynomials
  // JacobiPolynomials(0.,0., x, val);
} // @end LegendrePolynomials
/*
--------------------------------------------------------------------------------
.Derivative of Legendre polynomials: val[n]= d/dx P_{n+1}(x)
. Recurrence relation
.  P'n satisfies:
.   P'_0(x) = 0, P'_1(x) = 1
.   (1-x^2) P'_n(x) = - n x P_n(x) + n P_{n-1}(x) , n > 1
. P'_n is Gegenbauer ultraspherical polynomial with lambda = 3/2
--------------------------------------------------------------------------------
*/
void legendrePolynomialsDerivative(real_t x, std::vector<real_t>& val)
{
  std::vector<real_t>::iterator val_i(val.begin());
  real_t p_n(1.), p_n1(0.), p_n2;
  int n(1), nm1(0), nx2m1(1);
  *val_i = 1.;
  for ( val_i = val.begin() + 1; val_i < val.end(); n++, nm1++, nx2m1 += 2, val_i++ )
  {
    // compute P_{n-2}, P_{n-1}, P_{n} by recursion:
    // k*P_k(x) = (2*k-1)*x P_{k-1}(x) - (k-1) P_{k-2}(x)
    p_n2 = p_n1; p_n1 = p_n;
    p_n = ( nx2m1 * x * p_n1 - nm1 * p_n2 ) / n;
    // compute P'_n
    // (1-x^2)*P'_k(x) = k P_{k-1}(x) - k*x P_k(x)
    *val_i = n * (p_n1 - x * p_n) / (1 - x * x);
  }
  // alternately:
  // GegenbauerPolynomials(1.5, x, val);
} //@end LegendrePolynomialsDerivative
/*
-------------------------------------------------------------------------------
 Associated Legendre functions up to degree n for any real x:
        P_{l}^{m}(x) = (-1)^{m} * (1-x^2)^{m/2} * d^m/dx^m{P_l(x)}
  where P_l is Legendre polynomial of degree l.

- Recurrence relation ( for |x| <= 1 )
.  P_{0}^{0}(x) = 1
.  P_{l}^{l}(x) = (2l-1)!! * (-\sqrt{1-x^2})^{l}  // for l >= 1
.       where  the double factorial (2l-1)!! is defined by
.       (2l-1)!! = \Prod_{i=1,..,k}{2i-1) = (2l)! / 2^{l}*l!
.  P_{l+1}^{l}  = (2l+1) * x * P_{l}^{l}(x)       // for l >= 0
.    and for l = m+1,...
.  (l-m+1) P_{l+1}^{l}(x) = (2*l+1) * x * P_{l}^m(x) - (l+m) * P_{l-1}^l(x)

- Examples
. P_0^0=1            P_1^0=x               P_2^0=(3x^2-1)/2               P_3^0=(5x^3-3x)/2
. P_1^1=-sqrt(1-x^2) P_2^1=-3x*sqrt(1-x^2) P_3^1=-3/2(5x^2-1)sqrt(1-x^2)
. P_2^2=3(1-x^2)     P_3^2=15x*(1-x^2)
. P_3^3(x)=-15(1-x^2)sqrt(1-x^2)

- Argument Plm is a "triangular 2d array" defined as a std::vector of std::vectors
. the first std::vector carry the values: P_{l}^{0} for l = 0,..,n  ( =Plm[0][l] )
. the second one         the values: P_{l}^{1} for l = 1,..,n  ( =Plm[1][l-1] )
. the last carries only one value: P_{n}^{n}                 ( =Plm[n][0] )
. thus for given l in [0, n], P_l^m(x) is in Plm[m][l-m] for m = 0,...,l

- Some values for x = 0.5 (as stored in the Plm triangular array above)

 m |     l-m = 0     |    l-m = 1      |    l-m = 2      |    l-m = 3     |    l-m = 4
 --+-----------------|-----------------|-----------------|----------------|----------------
 0 | P_0^0= 1.        P_1^0=  0.5       P_2^0=-0.125      P_3^0=-0.4375    P_4^0=-0.2890625
 1 | P_1^1=-0.8660254 P_2^1= -1.2990381 P_3^1=-0.32475953 P_4^1= 1.3531647
 2 | P_2^2= 2.25      P_3^2=  5.625     P_4^2= 4.21875
 3 | P_3^3=-9.7427858 P_4^3=-34.0997503
 4 | P_4^4=59.0625
 -------------------------------------------------------------------------------
*/
void legendreFunctions(real_t x, std::vector<std::vector<real_t> >& Plm)
{
  // compute all Legendre functions up to degree n: P_{l}^{m}(x) for any real x
  // where n equals Plm.size()-1 and l = 0,.., n and m = 0,..,n;
  // for given l in [0, n], P_l^m(x) is stored in Plm[m][l-m] for m = 0,...,l
  // size_t n(Plm.size()-1);
  //
  std::vector< std::vector<real_t> >::iterator pl_i(Plm.begin());
  std::vector<real_t>::iterator pmm_i, plm_i, plm_b;
  // compute 1-x^2 and \sqrt{1-x^2}
  // testing the magnitude of x: |x| <= 1 or |x| > 1
  int xle1(1);
  if ( std::abs(x) > 1. ) { xle1 = -1; }
  real_t one_x2(xle1 * (1. - x * x)), sq_1_x2(sqrt(one_x2));
  // initialize P_{0}^{0} = 1., then P^{0}_{1}=x;
  real_t Pmm(1.);
  int m(0), twom_1(-1);
  for ( pl_i = Plm.begin(); pl_i != Plm.end() - 1; pl_i++, m++ )
  {
    pmm_i = (*pl_i).begin();
    // store P_{m}^{m} from previous value of m
    // Plm[m][0]
    *pmm_i = Pmm;
    // compute P_{m+1}^{m}(x) = (2m+1) * x * P^{m}_{m}(x)
    twom_1 += 2;
    // Plm[m][1]
    *(pmm_i + 1) = twom_1 * x * Pmm;
    // compute P_{l}^{m}, l = m+2,... using recursion formula
    // (l-m) P_{l}^{m}(x) = (2*l-1) * x * P_{l-1}^m(x) - (l+m-1) * P_{l-2}^m(x)
    int l = m + 2, l_m(2), twol_1(2 * l - 1);
    plm_b = pmm_i + 2;
    for ( plm_i = plm_b; plm_i != (*pl_i).end(); l++, l_m++, twol_1 += 2, plm_i++, pmm_i++ )
    {
      // Plm[m][l-m] for l = m+2,.., n
      *plm_i = ( twol_1 * x * *(pmm_i + 1) - (l + m - 1) * *pmm_i ) / l_m;
    }
    // compute P_{m}^{m}(x) = (2m-1)!! * (-\sqrt{1-x^2})^{m}
    // double factorial is (2m-1)!! = \Prod_{i=1,..,m}{2i-1) = (2m)! / 2^{m}*m!
    Pmm *= -xle1 * twom_1 * sq_1_x2;
  }
  // finally store P_{n}^{n}
  plm_i = (*(Plm.end() - 1)).begin(); // Plm[n][0]
  *plm_i = Pmm;
}

void legendreFunctionsDerivative(real_t x, const std::vector<std::vector<real_t> >& Plm, std::vector<std::vector<real_t> >& dPlm)
{
  // compute all Legendre functions derivatives up to degree n: P'_{l}^{m}(x) for any real x
  // where n equals dPlm.size()-1 and l = 0,.., n and m = 0,..,n;
  // this function requires that the Legendre functions are already computed and transmitted: Plm.
  //
  // for given l in [0, n], P_l^m(x) is stored in Plm[m][l-m] for m = 0,...,l
  //
  // Argument dPlm is a "triangular 2d array" defined as a std::vector of std::vectors
  // the 1st std::vector carries the values: P'_{0}^{0}
  // the 2nd one    carries the values: P'_{1}^{0} P'_{1}^{1}
  // and for l = 2,..,n
  // the l-th std::vector carries the values: P'_{l}^{0} P'_{l}^{1} .. P'_{l}^{l}
  // and the size of std::vector dPlm[l] is l+1.
  //
  // For a given int n, container dPlm can be declared (and defined) prior
  // to call to this function by
  //    std::vector< std::vector<real_t> > dPlm(n+1);
  //    for ( int l = 0; l <= n; l++ ) dPlm[l] = std::vector<real_t>(l+1);
  //
  //
  real_t sq_1_x2 = sqrt(1 - x * x);
  //
  // Compute Legendre derivative functions: P'_l^m (m=0,..,l; l=0,..,n)
  //
  // int n(Plm.size()-1);
  std::vector< std::vector<real_t> >::iterator dPl_i(dPlm.begin());
  std::vector<real_t>::iterator dPlm_i;
  //
  if (std::abs(sq_1_x2) < theEpsilon)
  {
    // dPlm.zero(); ASK DANIEL
    *((*dPl_i).begin()) = 0.;
    int l(1);
    real_t costl = - 0.5 * x;
    for ( dPl_i = dPlm.begin() + 1; dPl_i != dPlm.end(); dPl_i++, l++ )
    {
      *((*dPl_i).begin()) = 0.;
      // compute P'_{l}^{1} -- the other are null
      // P'_{l}^{1} = - x^l l(l+1) / 2
      *((*dPl_i).begin() + 1) = costl * l * (l + 1);
      costl *= x;
      for ( dPlm_i = (*dPl_i).begin() + 2; dPlm_i != (*dPl_i).end(); dPlm_i++ )
      {
        *dPlm_i = 0.;    // ASK DANIEL
      }
    }
  }
  else
  {
    *((*dPl_i).begin()) = 0.;  // Case l=0 where P'_l^m is equal to 0. ASK DANIEL
    int l(1);
    for ( dPl_i = dPlm.begin() + 1; dPl_i != dPlm.end(); dPl_i++, l++ )
    {
      // compute P'_{l}^{0} = P_{l}^{1} = Plm[1][l-1]
      *((*dPl_i).begin()) = Plm[1][l - 1];
      int m(1), lpm(l + 1), l_mp1(l), m_1(0), lp1_m(l), l_m(l - 1);
      for ( dPlm_i = (*dPl_i).begin() + 1; dPlm_i != (*dPl_i).end(); m++, dPlm_i++, lpm++, l_mp1--, m_1++, lp1_m--, l_m-- )
      {
        // compute P'_{l}^{m} for m = 1,..., l
        // P'_{l}^{m} = - (l-m+1) (l+m) P_{l}^{m-1} - x/sqrt(1-x^2) * m * P_{l}^{m}.
        *dPlm_i = - l_mp1 * lpm * Plm[m_1][lp1_m] - (x / sq_1_x2) * m * Plm[m][l - m];
      }
    }
  }
}

void legendreFunctionsTest(real_t x, number_t n, std::ostream& out)
{
  // Output function for Legendre Functions
  //
  // define 2d triangular array container for Legendre functions
  std::vector< std::vector<real_t> > Plm(n + 1);
  for ( number_t m = 0; m <= n; m++ ) { Plm[m] = std::vector<real_t>(n + 1 - m); }
  // compute associated Legendre functions
  legendreFunctions(x, Plm);

  out << std::endl << std::endl << " Legendre functions P_l^m up to order " << n << " (m=0,..,l; l=0,..," << n << ")"
            << " for x = " << x;
  out.setf(std::ios::scientific);
  number_t m(0), l;
  for ( l = 0; l <= n; l++ )
  {
    out << std::endl << " l = " << l;
    for ( m = 0; m <= l; m++ )
    { out << std::endl << "       P_" << l << "^" << m << " = " << std::setw(15) << std::setprecision(8) << Plm[m][l - m]; }
  }
  out.unsetf(std::ios::scientific);
}
void legendreFunctionsDerivativeTest(real_t x, number_t n, std::ostream& out)
{
  // Output function for Legendre functions derivatives
  //
  std::vector< std::vector<real_t> > Plm(n + 1);
  for ( number_t m = 0; m <= n; m++ ) { Plm[m] = std::vector<real_t>(n + 1 - m); }
  // compute associated Legendre functions
  legendreFunctions(x, Plm);

  std::vector< std::vector<real_t> > dPml(n + 1);
  for ( number_t l = 0; l <= n; l++ )
  {
    dPml[l] = std::vector<real_t>(l + 1);
  }
  legendreFunctionsDerivative(x, Plm, dPml);

  out << std::endl << std::endl << " Legendre functions derivative dP_l^m up to order " << dPml.size() - 1 << " (m=0,..,l; l=0,..," << dPml.size() - 1 << ")";
  out << " for real " << x;
  out.setf(std::ios::scientific);
  int l = 0;
  for ( std::vector< std::vector<real_t> >::iterator dPl_i = dPml.begin(); dPl_i != dPml.end(); dPl_i++, l++ )
  {
    int m = 0;
    out << std::endl << " l = " << l;
    for ( std::vector<real_t>::iterator dPml_i = (*dPl_i).begin(); dPml_i != (*dPl_i).end(); dPml_i++, m++ )
    { out << std::endl << "       P'_" << l << "^" << m << " = " << std::setw(19) << std::setprecision(12) << *dPml_i; }
  }
  out.unsetf(std::ios::scientific);
}

} // end of namespace xlifepp
