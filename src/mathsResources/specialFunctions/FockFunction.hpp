/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file FockFunction.hpp
  \author E. Lunéville
  \since 23 mars 2019
  \date  23 mars 2019

  \brief Fock function stuff
*/
/*
--------------------------------------------------------------------------------
 Fock function stuff
--------------------------------------------------------------------------------

                        1     /+inf     exp(-ixt)             1     /+inf
       fock_s(x,q) = -------  |      --------------dt =    -------  |     j_s(t,x)dt
                     sqrt(pi) /-inf   w2'(t)-qw2(t)        sqrt(pi) /-inf

                   w2(t) = 2sqrt(pi)exp(-ipi/6)Ai(texp(-2ipi/3))

        may be computed using either Filon or trapeze methods on ]-inf,0]and[0,+inf[ or combining several approximated methods regarding x:
                           a                        b
        -------------------|------------------------|---------------------> x
         stationary phase     integration by parts      residue (only 1)
                                     + Filon

        when x in [a,b],

        /+inf             /c                /d                 /+inf
        |   j_s(t,x)dt =  |   j_s(t,x)dt +  |   j_s(t,x)dt  +  |   j_s(t,x)dt
        /-inf             /-inf             /c                /d
                          use  IPP          use Filon         use IPP


*/

#ifndef FOCK_FUNCTION_HPP
#define FOCK_FUNCTION_HPP

#include "utils.h"
#include "config.h"
#include "../../finiteElements/integration/FilonIM.hpp"
#include "specialFunctions.hpp"

namespace xlifepp
{

class Fock
{
  public:
    complex_t q;       //!< impedance factor, (default q = 0)
    bool qis0;         //!< true if q = 0
    number_t ord;      //!< stationary phase order (0 or 1)
    number_t nbr;      //!< number of residue used by the residue method
    real_t a, b;       //!< bounds defining methods to be used (default a=-2.55, b=2)
    real_t c, d;       //!< bounds of integral computed by quadrature (default c=-15, d = 5)
    number_t nf;       //!< number of points used by Filon method
    FilonIM fimAP, fimAM;     //!< Filon object to compute approximated Fock function
    FilonIM fimM , fimP;      //!< Filon objects to compute Fock function by Filon methods
    mutable bool rotate;      //!< use rotation of negative real axe in quadrature methods (default = true)
    Vector<complex_t> poles;  //!< poles zk
    Vector<complex_t> vpoles; //!< values (zk-q*q)*w2_Ai(zk)
    Tabular<complex_t>* table;//!< tabulated Fock values

    Fock(): ord(0), nbr(0), nf(0), q(0.), a(-2.55), b(2), c(-10), d(5){}  //!< default constructor
    Fock(number_t o, number_t nr, number_t vnf, const complex_t& vq = 0., real_t va=-2.55, real_t vb=2, real_t vc=-15, real_t vd=5, bool rot=true); //!< constructor
    Fock(const string_t& filename); //!< constructor loading table
    void clear();                   //!< clear object
    void initFilon(real_t c, real_t d, number_t nf, bool rot=true);                  //!< initialize Filon object fimAP, fimAM used to compute Fock approximation
    void initFilonMP(real_t a, real_t b, number_t nfM, number_t nfP, bool rot=true); //!< initialize Filon objects fimM, fimP used to compute "exact" Fock function
    void initResidue(number_t nbp=1, bool withNewton=false);                         //!< initialize pole stuff for residue computation
    void newton(complex_t& r0, number_t kmax=10, real_t tol=theTolerance) const;     //!< improve roots of w2'-q.w2 using Newton's method

    complex_t Fock_s_Filon(real_t x) const;                //!< compute "exact" surface Fock function using Filon methods
    complex_t Fock_s_trapz(real_t x, real_t a=-100, real_t b=10, number_t nt=2200) const; //!< compute "exact" surface Fock function using trapeze method
    complex_t Fock_s_app(real_t x) const;                  //!< compute surface Fock function using approximation
    complex_t resFock_s(real_t x) const;                   //!< compute surface Fock function using residue (x>b)
    complex_t spFock_s(real_t x) const;                    //!< compute surface Fock function using stationnary phase (x<a)
    complex_t ippFock_s(real_t x, real_t c, int_t p) const;//!< compute surface Fock function using ipp (a<x<b)

    void createTable(real_t x0, real_t xf, number_t nx);   //!< create tabulated Fock values
    void saveTable(real_t x0, real_t xf, number_t nx, const string_t& filename); //!< create and save to file tabulated Fock values
    void saveTable(const string_t& filename) const;        //!< save to file tabulated Fock values
    void loadTable(const string_t& filename);              //!< load from file tabulated Fock values
    complex_t Fock_s_interp(real_t x) const;               //!< compute surface Fock function from tabulated values

    complex_t operator()(real_t x, CalType ct=_defaultCal) const //!< compute Fock main function
    {
        switch(ct)

        { case _filonCal: return Fock_s_Filon(x);
          case _trapezeCal: return Fock_s_trapz(x);
          case _interpCal: return Fock_s_interp(x);
          case _approxCal: return Fock_s_app(x);
          default: // priority: tabulated, approximated, Filon, trapeze
            {
                if(table!=nullptr) return Fock_s_interp(x);
                if(fimAP.size()>0) return Fock_s_app(x);
                if(fimM.size()>0) return Fock_s_Filon(x);
                return Fock_s_trapz(x);
            }
        }
        return complex_t(0.);  // fake return
    }
};

// extern function miming Fock::Fock_s_app used by Tabular that does not accept member function
complex_t ext_Fock_s_app(real_t x, Parameters & pars);
complex_t ext_Fock_s(real_t x, Parameters & pars);

//! 2*sqrt(pi)exp(i*pi/6)Ai(t*exp(2i*pi/3))
inline complex_t w1_Ai(const complex_t& z, DiffOpType d=_id)
{
  if(d==_id) return 2*sqrtOfpi_*sqrt(expipiover3_)*airy(expipiover3_*expipiover3_*z);
  return 2*sqrtOfpi_*sqrt(expipiover3_)*expipiover3_*expipiover3_*airy(expipiover3_*expipiover3_*z,_dt);
}

//! 2*sqrt(pi)exp(-i*pi/6)Ai(t*exp(-2i*pi/3))
inline complex_t w2_Ai(const complex_t& z, DiffOpType d=_id)
{
  if(d==_id) return 2*sqrtOfpi_*airy(-expipiover3_*z)/sqrt(expipiover3_);
  return -2*sqrtOfpi_*sqrt(expipiover3_)*airy(-expipiover3_*z,_dt);
}

inline complex_t j_s(const complex_t& z, real_t x, const complex_t& q) //!< exp(-ixz) /(w2'(z)-qw2(z))
    {
      complex_t d=w2_Ai(z,_dt);
      if(q!=complex_t(0.)) d-=q*w2_Ai(z);
      return std::exp(-i_*x*z)/d;
    }

//! function used in Filon: 1/(w2'(r*(t+s))-qw2(r*(t+s))) where s is a shift and r a complex rotation
inline complex_t filon_f(real_t t, Parameters& pa = defaultParameters)
    {
      complex_t q= pa(1);
      real_t s=pa(2);
      complex_t r= pa(3);
      if(r==complex_t(1.))
       {
           complex_t d=w2_Ai(t+s,_dt);
           if(q!=complex_t(0.)) d-=q*w2_Ai(t+s);
           return 1./d;
       }
      complex_t z=r*(t+s);
      complex_t d=w2_Ai(z,_dt);
      if(q!=complex_t(0.)) d-=q*w2_Ai(z);
      return 1./d;
    }

//wrapper to Airy's functions used by Newton's algorithm to find roots of Airy's functions
inline real_t   airyR (const real_t& x){return real(airy(x));}
inline real_t  airyRp(const real_t& x){return real(airy(x,_dt));}
inline real_t airyRpp(const real_t& x){return x*real(airy(x));}

complex_t fockCurvatureTransition_N(real_t x);   // Fock function involved in curvature transition (Neumann case)
complex_t fockCurvatureTransition_D(real_t x);   // Fock function involved in curvature transition (Dirichlet case)

} // end of namespace xlifepp

#endif
