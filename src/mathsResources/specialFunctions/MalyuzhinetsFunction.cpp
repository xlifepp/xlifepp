/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file MalyuzhinetsFunction.cpp
  \author E. Lunéville
  \since 17 dec 2019
  \date  17 dec 2019

  \brief function related to Malyuzhinets class
*/

#include "MalyuzhinetsFunction.hpp"
#include "../quadratureMethods.hpp"

namespace xlifepp
{

Malyuzhinets::Malyuzhinets(real_t wa, int_t ng, CalType qr, bool adapt)
  : Phi(wa), ngamma(ng), quad(qr), adaptive(adapt)
{
  if(qr!=_trapezeCal && qr!=_laguerreCal && qr!=_defaultCal)
    error("free_error","only _laguerreCal or _trapezeCal or _defaultCal are available in Malyuzhinets computation");
  if(Phi<=0)
    error("free_error","the wedge angle Phi should be greater than 0 in Malyuzhinets computation");
  if(Phi<=pi_/2)
    warning("free_warning","Malyuzhinets computation with Phi<pi/2 is hazardous");
  pars<<Parameter(reinterpret_cast<const void *>(this),"object");
  eps=1E-5;
  nq=32;
  if(qr==_laguerreCal)
  {
    lagPoints.resize(nq);
    lagWeights.resize(nq);
    LaguerreTable(nq, lagPoints, lagWeights);
  }
}

void Malyuzhinets::print(std::ostream& out) const
{
  out<<"Malyuzhinets function: Phi="<<Phi<<" ngamma="<<ngamma<<" quadrature:";
  if(adaptive)
    out<<" adaptive trapeze and";
  switch(quad)
  {
    case _trapezeCal:
      out<< " trapeze";
      break;
    case _laguerreCal:
      out<< " Laguerre";
      break;
    default:
      break;
  }
  out<<" nq="<<nq<<" eps="<<eps<<" z="<<z_<<" pars:"<<pars;
}

// Malyuzhinets integrand f=(cosh(zt)-1)/(t.cosh(pit/2)sinh(2Phit))
complex_t Malyuzhinets::integrand(real_t t, const complex_t& z) const
{
  complex_t f;
  if(std::abs(t)<=1E-4) //stable form close to 0, sinhs(x)=sinh(x)/x
  {
    complex_t s=sinhs(0.5*z*t);
    f=z*z*s*s/(4*Phi*std::cosh(0.5*pi_*t)*sinhs(2*Phi*t));
  }
  else
  {
    real_t ep=t*(1-std::exp(-4*Phi*t));
    complex_t ez=std::exp(-z*t);
    f=2*std::exp((z-0.5*pi_-2*Phi)*t)*(1+ez*ez-2*ez)/((1+std::exp(-pi_*t))*ep);   // stable form for large t, Re z >0
  }
  if(ngamma>0)
  {
    f*=std::exp(-pi_*t*ngamma);
    if(ngamma%2==0)
      f=-f;
  }
  return f;
}

std::vector<complex_t> Malyuzhinets::integrand(const std::vector<real_t>& ts, const complex_t& z) const
{
  std::vector<complex_t> fs(ts.size());
  std::vector<complex_t>::iterator itf=fs.begin();
  std::vector<real_t>::const_iterator it=ts.begin();
  for(; it!=ts.end(); ++it,++itf)
    *itf=integrand(*it,z);
  return fs;
}

complex_t Malyuzhinets::integral(real_t t0, real_t tf, const complex_t& zin) const
{
  complex_t res(0.,0.);
  z_=zin;
  real_t a=t0;
  if(adaptive) //use adaptiveTrapz on [t0,a] or [t0,tf] if defaultCal
  {
    a=tf;
    if(quad!=_defaultCal)
      a=std::min(t0+0.5,tf);
    res=adaptiveTrapz(mal_integrand,const_cast<Parameters&>(pars), t0, a, eps);
  }
  if(a>=tf)
    return res;
  switch(quad)
  {
    case _trapezeCal:
      res+=trapz(mal_integrand,const_cast<Parameters&>(pars),a,tf,nq);
      break;
    case _laguerreCal:
      res+=laguerre(mal_integrand,const_cast<Parameters&>(pars),a,2*Phi+(ngamma+.5)*pi_-real(z_),nq,lagPoints,lagWeights);
      break;
    default:
      break;
  }
  return res;
}

//!< compute Malyuzhinets function at point z
complex_t Malyuzhinets::compute(complex_t z) const
{
  complex_t res;
  if(real(z)<0)
    z=-z;  // move to the right half plane because psi(-z)=psi(z)
  real_t z0=0.5*pi_ + 2*Phi;
  int_t m=std::ceil((real(z)-z0)/(4*Phi));
  z=z-4*Phi*m;        // move to the analytic strip z0-4Phi < real(z) < z0

  if(ngamma==0)        //original form (no gamma function)
  {
    real_t tf=20./(z0-real(z));
    res=integral(0,tf,z);
    res=std::exp(-res);
  }
  else                 // gamma factorized form
  {
    complex_t f=1, f0=1, g, g0;
    real_t p4=0.25/Phi, a;
    for(number_t k=0; k<ngamma; k++)
    {
      a=0.5+(k+0.5)*pi_*p4;
      g=gammaFunction(a+p4*z)*gammaFunction(a-p4*z);
      g0= gammaFunction(a);
      g0*=g0;
      if(k%2==0) {
        f/=g;
        f0/=g0;
      }
      else       {
        f*=g;
        f0*=g0;
      }
    }
    f/=f0;
    real_t tf=20./(z0-real(z)+ngamma*pi_);
    res=integral(0,tf,z);
    res=f*std::exp(0.5*res);
  }
  //back to original z
  for(number_t k=1; k<=m; k++, z+=4*Phi)
    res/=std::tan(0.5*z+Phi+0.25*pi_);
  return res;
}


//!< compute Malyuzhinets function at points zs
std::vector<complex_t> Malyuzhinets::compute(const std::vector<complex_t>& zs, bool parallel) const
{
  std::vector<complex_t> res(zs.size());
  if(parallel)
  {
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t k=0; k<zs.size(); k++)
      res[k]= compute(zs[k]);
  }
  else
  {
    std::vector<complex_t>::iterator itr=res.begin();
    std::vector<complex_t>::const_iterator itz=zs.begin();
    for(; itz!=zs.end(); ++itz,++itr)
      *itr=compute(*itz);
  }
  return res;
}

void checkBC(BcType& bcP, BcType& bcM, complex_t& thetaP, complex_t& thetaM)
{
  if (bcM == _undefEcType)
    bcM=bcP;
  if (bcP == _NeumannEc && bcM == _DirichletEc)
  {
    bcP=_FourierEc;
    thetaP=complex_t(0,0);
    thetaM=-100*i_;
  }
  if (bcP == _DirichletEc && bcM == _NeumannEc)
  {
    bcP=_FourierEc;
    thetaP=-100*i_;
    thetaM=complex_t(0,0);
  }
  if (bcP == _FourierEc && thetaM == 0. && thetaP == 0.)
    bcP=_NeumannEc;
}

complex_t mal_integrand(real_t t, Parameters& pars)  //! integrand at (t,z), extern call
{
  const Malyuzhinets* mal = reinterpret_cast<const Malyuzhinets*>(pars("object").get_p());
  return mal->integrand(t, mal->z_);
}

// main wegde diffraction computation function using SD path (parallel computing)
std::vector<complex_t> wedgeDiffractionSD_par(const std::vector<real_t>& krs, const std::vector<real_t>& phis,
    real_t Phi, real_t phi0,  FieldPart fp, real_t krasym, BcType bcP, BcType bcM,
    complex_t thetaP, complex_t thetaM)
{
  typedef complex_t (*wg_fun)(const complex_t&, real_t, real_t, const complex_t&, const complex_t&, const complex_t&, const complex_t&,
                              bool, const Malyuzhinets&);

  checkBC(bcP, bcM, thetaP, thetaM);
  Malyuzhinets mal(Phi);
  mal.quad=_defaultCal;
  number_t ng=mal.ngamma;

  // initialization
  real_t eps=pi_/1000, epspi=eps; // truncature parameters in integrals
  number_t nq=100;                // number of quadrature points
  real_t dx=(pi_/2-epspi-eps)/nq; // step in trapeze method
  complex_t idx=dx/(4*i_*Phi);    // coefficient in integrals
  wg_fun g = wedge_dir;           // kernel
  complex_t sP=-1, sM=-1;         // sign of reflection
  real_t psip= 2*Phi-phi0-pi_;    // upper reflection angle
  real_t psim=-2*Phi-phi0+pi_;    // lower refection angle
  complex_t coeffP=1., coeffM=1.; // Fourier coefficient
  bool pole=false;
  real_t pis2=pi_/2;
  bool istd = true;   // use standard mode
  complex_t ep4=std::exp(-i_*pi_/4);
  real_t tau=pi_/(2*Phi);

  switch(bcP)
  {
    case _NeumannEc:
      g=wedge_neu;
      sM=1;
      sP=1.;
      break;
    case _FourierEc:
      g=wedge_fou;
      sP=1-2*std::sin(thetaP)/(-std::sin(phi0-Phi)+std::sin(thetaP));
      sM=1-2*std::sin(thetaM)/(std::sin(phi0+Phi)+std::sin(thetaM));
      if(!istd)
      {
        if(real(thetaP)<0)
          coeffP = mal(phi0+Phi-thetaP-pis2) * mal(phi0+Phi+thetaP+pis2);
        else
          coeffP = mal(phi0+Phi+thetaP-pis2) * mal(phi0+Phi-thetaP+pis2);
        if(real(thetaM)<0)
          coeffM = mal(phi0-Phi-thetaM-pis2) * mal(phi0-Phi+thetaM+pis2);
        else
          coeffM = mal(phi0-Phi+thetaM-pis2) * mal(phi0-Phi-thetaM+pis2);
      }
      else
      {
        coeffP = mal(phi0+Phi+thetaP-pis2) * mal(phi0+Phi-thetaP+pis2);
        coeffM = mal(phi0-Phi+thetaM-pis2) * mal(phi0-Phi-thetaM+pis2);
      }
      pole=true;
    default: break;
  }

  //duplicate mal for multithreading
  std::vector<Malyuzhinets*> mals(numberOfThreads());
  for(number_t i=0; i<numberOfThreads(); i++) // update internal parameters
  {
    mals[i]=new Malyuzhinets(mal);
    mals[i]->pars("object")=static_cast<const void*>(mals[i]);
  }

  number_t nphis=phis.size(), nkrs=krs.size();
  std::vector<complex_t> vs(nphis*nkrs,0.);

  bool par = nkrs>1 ;

  //integral over [pi/2+eps, pi-epspi] using trapeze method
  real_t x0=pi_/2+eps;
  number_t nx=round((0.5*pi_-eps-epspi)/dx);
  std::vector<complex_t> a(nx+1), b(nx+1), c(nx+1), gs(nx+1);
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t i=0; i<nx+1; i++)
  {
    real_t x=x0+i*dx;
    a[i]=x+acosh(1./std::abs(std::cos(x)))*i_;
    b[i]=std::cos(a[i]);
    c[i]=1+i_/std::cos(x);
  }

  std::vector<complex_t>::iterator itv=vs.begin(), itvb;
  for(number_t p=0; p<nphis; p++)
  {
    if(std::abs(phis[p])<= Phi+theTolerance)
    {
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t i=0; i<nx+1; i++)
        gs[i] = g(a[i], phis[p], phi0, thetaP, thetaM, coeffP, coeffM, istd, *mals[currentThread()])
                - g(a[i]-2*pi_, phis[p], phi0, thetaP, thetaM, coeffP, coeffM, istd,*mals[currentThread()]);
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t q=0; q<nkrs; q++)
      {
        if(krs[q]<=krasym)
        {
            complex_t v=0., ikr=krs[q]*i_;
            std::vector<complex_t>::iterator itc=c.begin(), itb=b.begin(), itg=gs.begin();
            v+=0.5**itg**itc*std::exp(-ikr**itb);
            itg++; itc++; itb++;
            for(number_t k=1; k<nx; k++, ++itg, ++itc, ++itb)
              v+=*itg**itc*std::exp(-ikr**itb);
            v+=0.5**itg**itc*std::exp(-ikr**itb);
            vs[p*nkrs+q]=v*idx;
        }
      }
    }
  }
  // integral over [pi+epsi, 3pi/2-eps] using trapeze method
  x0=pi_+epspi;
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t i=0; i<nx+1; i++)
  {
    real_t x=x0+i*dx;
    a[i]=x-acosh(1./std::abs(std::cos(x)))*i_;
    b[i]=std::cos(a[i]);
    c[i]=1+i_/std::cos(x);
  }
  for(number_t p=0; p<nphis; p++)
  {
    if(std::abs(phis[p])<= Phi+theTolerance)
    {
      complex_t gmp = g(-pi_, phis[p], phi0, thetaP, thetaM, coeffP, coeffM, istd, mal)
                    - g( pi_, phis[p], phi0, thetaP, thetaM, coeffP, coeffM, istd, mal);
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t i=0; i<nx+1; i++)
        gs[i] = g(a[i], phis[p], phi0, thetaP, thetaM, coeffP, coeffM, istd, *mals[currentThread()])
                - g(a[i]-2*pi_, phis[p], phi0, thetaP, thetaM, coeffP, coeffM, istd, *mals[currentThread()]);
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t q=0; q<nkrs; q++)
      {
        if(krs[q]<=krasym)
        {
            complex_t v=0., ikr=krs[q]*i_;
            std::vector<complex_t>::iterator itc=c.begin(), itb=b.begin(), itg=gs.begin();
            v+=0.5**itg**itc*std::exp(-ikr**itb);
            itg++; itc++; itb++;
            for(number_t k=1; k<nx; k++, ++itg, ++itc, ++itb)
               v+=*itg**itc*std::exp(-ikr**itb);
            v+=0.5**itg**itc*std::exp(-ikr**itb);
            vs[p*nkrs+q]+=v*idx;
        }
        else //use asymptotic expansion
        {
            vs[p*nkrs+q]+=i_*tau*exp(i_*krs[q])*ep4*(gmp)/std::sqrt(2*pi_*krs[q]);
        }
      }
      if(fp!=_wedgeField)
      {
        if(phi0>Phi-pi_ && phis[p]>=psip) //add upper reflected field
        {
          #ifdef XLIFEPP_WITH_OMP
          #pragma omp parallel for
          #endif // XLIFEPP_WITH_OMP
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]+=sP*exp(i_*krs[q]*std::cos(phis[p]-psip));
        }
        if(phi0<pi_-Phi && phis[p]<=psim) //add lower reflected field
        {
          #ifdef XLIFEPP_WITH_OMP
          #pragma omp parallel for
          #endif // XLIFEPP_WITH_OMP
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]+=sM*exp(i_*krs[q]*std::cos(phis[p]-psim));
        }
        if(std::abs(phi0)<=pi_-Phi || (phi0>pi_-Phi && phis[p]>=phi0-pi_) || (phi0<Phi-pi_ && phis[p]<=phi0+pi_))//add incident field in light zone
        {
          #ifdef XLIFEPP_WITH_OMP
          #pragma omp parallel for
          #endif // XLIFEPP_WITH_OMP
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]+=exp(-i_*krs[q]*std::cos(phis[p]-phi0));
        }
        if(fp==_diffractedField) // remove incident field everywhere
        {
          #ifdef XLIFEPP_WITH_OMP
          #pragma omp parallel for
          #endif // XLIFEPP_WITH_OMP
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]-=exp(-i_*krs[q]*std::cos(phis[p]-phi0));
        }
      }
    }
  }
  if(pole) // add Fourrier pole contribution (only available for standard mode)
  {
    complex_t coeff=coeffP*coeffM;
    pole=false;
    real_t tau=pi_/(2*Phi), phicp, phicm;
    if(imag(thetaP)>0) phicp=Phi+pi_+real(thetaP)-std::acos(-1/std::cosh(imag(thetaP)));
    else               phicp=Phi-pi_+real(thetaP)+std::acos(-1/std::cosh(imag(thetaP)));
    if(imag(thetaM)>0) phicm=-Phi-pi_-real(thetaM)+std::acos(-1/std::cosh(imag(thetaM)));
    else               phicm=-Phi+pi_-real(thetaM)-std::acos(-1/std::cosh(imag(thetaM)));
    real_t zp= 2*Phi+3*pi_/2; // bp01+phi+Phi-thetaP+pi/2
    real_t T=10./pi_;
    complex_t intg = exp(mal.integral(0,T,zp));
    real_t gam = gammaFunction(1+3*pi_/(4*Phi)) /(gammaFunction(1+pi_/(2*Phi))*gammaFunction(-pi_/(4*Phi)));
    real_t psin0 = gammaFunction(0.5+3*pi_/(8*Phi)) /gammaFunction(0.5+pi_/(8*Phi));
    psin0=psin0*psin0;
    complex_t resp = gam * mal(2*Phi+2*thetaP+pi_/2) * intg / psin0 ;
    complex_t cresp= 2*pi_*resp*mal(thetaP-thetaM+3*pi_/2) * mal(thetaP+thetaM+pi_/2)/coeff;
    cresp= cresp*std::cos(tau*phi0)/(std::sin(tau*(Phi+thetaP+pi_))-std::sin(tau*phi0));
    complex_t resm = gam * mal(2*Phi+2*thetaM+pi_/2) * intg / psin0 ;
    complex_t cresm= 2*pi_*resm*mal(thetaM-thetaP+3*pi_/2) * mal(thetaP+thetaM+pi_/2)/coeff;
    cresm= cresm*std::cos(tau*phi0)/(std::sin(tau*(Phi+thetaM+pi_))+std::sin(tau*phi0));
    for(number_t p=0; p<nphis; p++)
    {
      if(std::abs(phis[p])<= Phi)
      {
        complex_t bp01=Phi+pi_-phis[p]+thetaP;
        real_t rb=real(bp01);
        if(abs(thetaP)>eps && rb<3*pi_/2 && imag(bp01) < sign(pi_-rb)*acosh(1/std::abs(std::cos(rb))))
        {
          pole=true;
          #ifdef XLIFEPP_WITH_OMP
          #pragma omp parallel for
          #endif // XLIFEPP_WITH_OMP
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]+=exp(-i_*krs[q]*std::cos(bp01))*cresp;
        }
        complex_t cm01=-(Phi+pi_+phis[p]+thetaM);
        real_t rc=real(cm01);
        if(abs(thetaM)>eps && rc>-3*pi_/2 && imag(cm01) > -sign(pi_+rc)*acosh(1/std::abs(std::cos(rc))))
        {
          pole=true;
          #ifdef XLIFEPP_WITH_OMP
          #pragma omp parallel for
          #endif // XLIFEPP_WITH_OMP
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]+=exp(-i_*krs[q]*std::cos(cm01))*cresm;
        }
      }
    }
  }
  for(number_t i=0; i<numberOfThreads(); i++) delete mals[i];
  //end of computation
  return vs;
}

// main wegde diffraction computation function using SD path (non parallel computing)
std::vector<complex_t> wedgeDiffractionSD(const std::vector<real_t>& krs, const std::vector<real_t>& phis,
    real_t Phi, real_t phi0,  FieldPart fp, real_t krasym, BcType bcP, BcType bcM,
    complex_t thetaP, complex_t thetaM)
{
  typedef complex_t (*wg_fun)(const complex_t&, real_t, real_t, const complex_t&, const complex_t&, const complex_t&, const complex_t&,
                              bool, const Malyuzhinets&);

  checkBC(bcP, bcM, thetaP, thetaM);
  Malyuzhinets mal(Phi);
  mal.quad=_defaultCal;
  number_t ng=mal.ngamma;

  // initialization
  real_t eps=pi_/1000, epspi=eps; // truncature parameters in integrals
  number_t nq=100;                // number of quadrature points
  real_t dx=(pi_/2-epspi-eps)/nq; // step in trapeze method
  complex_t idx=dx/(4*i_*Phi);    // coefficient in integrals
  wg_fun g = wedge_dir;           // kernel
  complex_t sP=-1, sM=-1;         // sign of reflection
  real_t psip= 2*Phi-phi0-pi_;    // upper reflection angle
  real_t psim=-2*Phi-phi0+pi_;    // lower refection angle
  complex_t coeffP=1., coeffM=1.; // Fourier coefficient
  bool pole=false;
  real_t pis2=pi_/2;
  bool istd = true;   // use standard mode
  complex_t ep4=std::exp(-i_*pi_/4);
  real_t tau=pi_/(2*Phi);

  switch(bcP)
  {
    case _NeumannEc:
      g=wedge_neu;
      sM=1;
      sP=1.;
      break;
    case _FourierEc:
      g=wedge_fou;
      sP=1-2*std::sin(thetaP)/(-std::sin(phi0-Phi)+std::sin(thetaP));
      sM=1-2*std::sin(thetaM)/(std::sin(phi0+Phi)+std::sin(thetaM));
      if(!istd)
      {
        if(real(thetaP)<0)
          coeffP = mal(phi0+Phi-thetaP-pis2) * mal(phi0+Phi+thetaP+pis2);
        else
          coeffP = mal(phi0+Phi+thetaP-pis2) * mal(phi0+Phi-thetaP+pis2);
        if(real(thetaM)<0)
          coeffM = mal(phi0-Phi-thetaM-pis2) * mal(phi0-Phi+thetaM+pis2);
        else
          coeffM = mal(phi0-Phi+thetaM-pis2) * mal(phi0-Phi-thetaM+pis2);
      }
      else
      {
        coeffP = mal(phi0+Phi+thetaP-pis2) * mal(phi0+Phi-thetaP+pis2);
        coeffM = mal(phi0-Phi+thetaM-pis2) * mal(phi0-Phi-thetaM+pis2);
      }
      pole=true;
    default: break;
  }


  number_t nphis=phis.size(), nkrs=krs.size();
  std::vector<complex_t> vs(nphis*nkrs,0.);

  bool par = nkrs>1 ;

  //integral over [pi/2+eps, pi-epspi] using trapeze method
  real_t x0=pi_/2+eps;
  number_t nx=round((0.5*pi_-eps-epspi)/dx);
  std::vector<complex_t> a(nx+1), b(nx+1), c(nx+1), gs(nx+1);
  for(number_t i=0; i<nx+1; i++)
  {
    real_t x=x0+i*dx;
    a[i]=x+acosh(1./std::abs(std::cos(x)))*i_;
    b[i]=std::cos(a[i]);
    c[i]=1+i_/std::cos(x);
  }

  std::vector<complex_t>::iterator itv=vs.begin(), itvb;
  for(number_t p=0; p<nphis; p++)
  {
    if(std::abs(phis[p])<= Phi+theTolerance)
    {
      for(number_t i=0; i<nx+1; i++)
        gs[i] = g(a[i], phis[p], phi0, thetaP, thetaM, coeffP, coeffM, istd, mal)
                - g(a[i]-2*pi_, phis[p], phi0, thetaP, thetaM, coeffP, coeffM, istd,mal);
      for(number_t q=0; q<nkrs; q++)
      {
        if(krs[q]<=krasym)
        {
            complex_t v=0., ikr=krs[q]*i_;
            std::vector<complex_t>::iterator itc=c.begin(), itb=b.begin(), itg=gs.begin();
            v+=0.5**itg**itc*std::exp(-ikr**itb);
            itg++; itc++; itb++;
            for(number_t k=1; k<nx; k++, ++itg, ++itc, ++itb)
              v+=*itg**itc*std::exp(-ikr**itb);
            v+=0.5**itg**itc*std::exp(-ikr**itb);
            vs[p*nkrs+q]=v*idx;
        }
      }
    }
  }
  // integral over [pi+epsi, 3pi/2-eps] using trapeze method
  x0=pi_+epspi;
  for(number_t i=0; i<nx+1; i++)
  {
    real_t x=x0+i*dx;
    a[i]=x-acosh(1./std::abs(std::cos(x)))*i_;
    b[i]=std::cos(a[i]);
    c[i]=1+i_/std::cos(x);
  }
  for(number_t p=0; p<nphis; p++)
  {
    if(std::abs(phis[p])<= Phi+theTolerance)
    {
      complex_t gmp = g(-pi_, phis[p], phi0, thetaP, thetaM, coeffP, coeffM, istd, mal)
                    - g( pi_, phis[p], phi0, thetaP, thetaM, coeffP, coeffM, istd, mal);
      for(number_t i=0; i<nx+1; i++)
        gs[i] = g(a[i], phis[p], phi0, thetaP, thetaM, coeffP, coeffM, istd, mal)
              - g(a[i]-2*pi_, phis[p], phi0, thetaP, thetaM, coeffP, coeffM, istd, mal);
      for(number_t q=0; q<nkrs; q++)
      {
        if(krs[q]<=krasym)
        {
            complex_t v=0., ikr=krs[q]*i_;
            std::vector<complex_t>::iterator itc=c.begin(), itb=b.begin(), itg=gs.begin();
            v+=0.5**itg**itc*std::exp(-ikr**itb);
            itg++; itc++; itb++;
            for(number_t k=1; k<nx; k++, ++itg, ++itc, ++itb)
               v+=*itg**itc*std::exp(-ikr**itb);
            v+=0.5**itg**itc*std::exp(-ikr**itb);
            vs[p*nkrs+q]+=v*idx;
        }
        else //use asymptotic expansion
        {
            vs[p*nkrs+q]+=i_*tau*exp(i_*krs[q])*ep4*(gmp)/std::sqrt(2*pi_*krs[q]);
        }
      }
      if(fp!=_wedgeField)
      {
        if(phi0>Phi-pi_ && phis[p]>=psip) //add upper reflected field
        {
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]+=sP*exp(i_*krs[q]*std::cos(phis[p]-psip));
        }
        if(phi0<pi_-Phi && phis[p]<=psim) //add lower reflected field
        {
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]+=sM*exp(i_*krs[q]*std::cos(phis[p]-psim));
        }
        if(std::abs(phi0)<=pi_-Phi || (phi0>pi_-Phi && phis[p]>=phi0-pi_) || (phi0<Phi-pi_ && phis[p]<=phi0+pi_))//add incident field in light zone
        {
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]+=exp(-i_*krs[q]*std::cos(phis[p]-phi0));
        }
        if(fp==_diffractedField) // remove incident field everywhere
        {
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]-=exp(-i_*krs[q]*std::cos(phis[p]-phi0));
        }
      }
    }
  }
  if(pole) // add Fourrier pole contribution (only available for standard mode)
  {
    complex_t coeff=coeffP*coeffM;
    pole=false;
    real_t tau=pi_/(2*Phi), phicp, phicm;
    if(imag(thetaP)>0) phicp=Phi+pi_+real(thetaP)-std::acos(-1/std::cosh(imag(thetaP)));
    else               phicp=Phi-pi_+real(thetaP)+std::acos(-1/std::cosh(imag(thetaP)));
    if(imag(thetaM)>0) phicm=-Phi-pi_-real(thetaM)+std::acos(-1/std::cosh(imag(thetaM)));
    else               phicm=-Phi+pi_-real(thetaM)-std::acos(-1/std::cosh(imag(thetaM)));
    real_t zp= 2*Phi+3*pi_/2; // bp01+phi+Phi-thetaP+pi/2
    real_t T=10./pi_;
    complex_t intg = exp(mal.integral(0,T,zp));
    real_t gam = gammaFunction(1+3*pi_/(4*Phi)) /(gammaFunction(1+pi_/(2*Phi))*gammaFunction(-pi_/(4*Phi)));
    real_t psin0 = gammaFunction(0.5+3*pi_/(8*Phi)) /gammaFunction(0.5+pi_/(8*Phi));
    psin0=psin0*psin0;
    complex_t resp = gam * mal(2*Phi+2*thetaP+pi_/2) * intg / psin0 ;
    complex_t cresp= 2*pi_*resp*mal(thetaP-thetaM+3*pi_/2) * mal(thetaP+thetaM+pi_/2)/coeff;
    cresp= cresp*std::cos(tau*phi0)/(std::sin(tau*(Phi+thetaP+pi_))-std::sin(tau*phi0));
    complex_t resm = gam * mal(2*Phi+2*thetaM+pi_/2) * intg / psin0 ;
    complex_t cresm= 2*pi_*resm*mal(thetaM-thetaP+3*pi_/2) * mal(thetaP+thetaM+pi_/2)/coeff;
    cresm= cresm*std::cos(tau*phi0)/(std::sin(tau*(Phi+thetaM+pi_))+std::sin(tau*phi0));
    for(number_t p=0; p<nphis; p++)
    {
      if(std::abs(phis[p])<= Phi)
      {
        complex_t bp01=Phi+pi_-phis[p]+thetaP;
        real_t rb=real(bp01);
        if(abs(thetaP)>eps && rb<3*pi_/2 && imag(bp01) < sign(pi_-rb)*acosh(1/std::abs(std::cos(rb))))
        {
          pole=true;
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]+=exp(-i_*krs[q]*std::cos(bp01))*cresp;
        }
        complex_t cm01=-(Phi+pi_+phis[p]+thetaM);
        real_t rc=real(cm01);
        if(abs(thetaM)>eps && rc>-3*pi_/2 && imag(cm01) > -sign(pi_+rc)*acosh(1/std::abs(std::cos(rc))))
        {
          pole=true;
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]+=exp(-i_*krs[q]*std::cos(cm01))*cresm;
        }
      }
    }
  }
  //end of computation
  return vs;
}

// compute wegde diffraction current using SD path (n=krs.size())
//   return current vector (diffracted field) containing the current
//         if uplow= 0 : on phi=Phi (first values) and phi=-Phi (last values)
//         if uplow= 1 : on phi=Phi
//         if uplow=-1 : on phi=-Phi
//   for Neumann or Fourier problem return u and for the Dirichlet problem return du/dn
//   if kr >=krasym use asymptotic expansion in SD method
std::vector<complex_t> wedgeCurrentSD(const std::vector<real_t>& krs, real_t Phi, int uplow, real_t phi0, FieldPart fp, real_t krasym, BcType bcP, BcType bcM,
                                      complex_t thetaP, complex_t thetaM)
{
  checkBC(bcP, bcM, thetaP, thetaM);
  std::vector<complex_t> cs, ds;
  if(uplow>=0)
  {
      std::vector<real_t> phis(1,Phi);
      if(bcP!=_DirichletEc)
        cs = wedgeDiffractionSD(krs,phis,Phi,phi0,fp,krasym,bcP,bcM,thetaP,thetaM);
      else
        cs = wedgeDirCurrentSD(krs,Phi,phi0,fp,1);
      if(uplow>0) return cs;
  }
  if(uplow<=0)
  {
      std::vector<real_t> phis(1,-Phi);
      if(bcM!=_DirichletEc)
        ds = wedgeDiffractionSD(krs,phis,Phi,phi0,fp,krasym,bcP,bcM,thetaP,thetaM);
      else
        ds = wedgeDirCurrentSD(krs,Phi,phi0,fp,-1);
      if(uplow<0) return ds;
  }
  cs.insert(cs.end(),ds.begin(),ds.end());
  return cs;
}

// compute wegde diffraction current using SD path (n=krs.size())
//   return current vector (diffracted field) containing the current
//         if uplow= 0 : on phi=Phi (first values) and phi=-Phi (last values)
//         if uplow= 1 : on phi=Phi
//         if uplow=-1 : on phi=-Phi
//   for Neumann or Fourier problem return u and for the Dirichlet problem return du/dn
//   if kr >=krasym use asymptotic expansion in SD method
std::vector<complex_t> wedgeCurrentSD_par(const std::vector<real_t>& krs, real_t Phi, int uplow, real_t phi0, FieldPart fp, real_t krasym, BcType bcP, BcType bcM,
                                      complex_t thetaP, complex_t thetaM)
{
  checkBC(bcP, bcM, thetaP, thetaM);
  std::vector<complex_t> cs, ds;
  if(uplow>=0)
  {
      std::vector<real_t> phis(1,Phi);
      if(bcP!=_DirichletEc)
        cs = wedgeDiffractionSD_par(krs,phis,Phi,phi0,fp,krasym,bcP,bcM,thetaP,thetaM);
      else
        cs =wedgeDirCurrentSD(krs,Phi,phi0,fp,1);
      if(uplow>0) return cs;
  }
  if(uplow<=0)
  {
      std::vector<real_t> phis(1,-Phi);
      if(bcM!=_DirichletEc)
        ds = wedgeDiffractionSD_par(krs,phis,Phi,phi0,fp,krasym,bcP,bcM,thetaP,thetaM);
      else
        ds = wedgeDirCurrentSD(krs,Phi,phi0,fp,-1);
      if(uplow<0) return ds;
  }
  cs.insert(cs.end(),ds.begin(),ds.end());
  return cs;
}

// compute du/dn = 1/r du/dphi in case of Dirichlet problem, using SD path
// here we return 1/kr du/dphi, to retrieve du/dn multiply by k !
//   when uplow =1 compute current on upper boundary
//   when uplow=-1 compute current on lower boundary
std::vector<complex_t> wedgeDirCurrentSD(const std::vector<real_t>& krs, real_t Phi, real_t phi0, FieldPart fp, int uplow)
{
  number_t nr=0, nkrs=krs.size();
  if(nr==0) return std::vector<complex_t>();   // nothing to do, return void vector
  std::vector<complex_t> res(nkrs,0.);

  real_t psip= 2*Phi-phi0-pi_;    // upper reflection angle
  real_t psim=-2*Phi-phi0+pi_;    // lower refection angle
  real_t eps=pi_/1000, epspi=eps; // truncature parameters in integrals
  number_t nq=100;                // number of quadrature points
  real_t dx=(pi_/2-epspi-eps)/nq; // step in trapeze method
  complex_t idx=dx/(4*i_*Phi);    // coefficient in integrals
  real_t x0=pi_/2+eps;
  number_t nx=round((0.5*pi_-eps-epspi)/dx);
  std::vector<complex_t> a(nx+1), b(nx+1), c(nx+1), gs(nx+1);
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t i=0; i<nx+1; i++)
  {
    real_t x=x0+i*dx;
    a[i]=x+acosh(1./std::abs(std::cos(x)))*i_;
    b[i]=std::cos(a[i]);
    c[i]=1+i_/std::cos(x);
  }
  if(uplow==1)
  {
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t i=0; i<nx+1; i++)
      gs[i] = wedge_dndirP(a[i], Phi, phi0)-wedge_dndirP(a[i]-2*pi_, Phi, phi0);
  }
  else
  {
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t i=0; i<nx+1; i++)
      gs[i] = wedge_dndirM(a[i], Phi, phi0)-wedge_dndirM(a[i]-2*pi_, Phi, phi0);
  }
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t q=0; q<nkrs; q++)
  {
    complex_t ikr=krs[q]*i_;
    complex_t v=0.;
    std::vector<complex_t>::iterator itc=c.begin(), itb=b.begin(), itg=gs.begin();
    v+=0.5**itg**itc*std::exp(-ikr**itb);
    itg++; itc++; itb++;
    for(number_t j=1; j<nx; j++, ++itg, ++itc, ++itb) v+=*itg**itc*std::exp(-ikr**itb);
    v+=0.5**itg**itc*std::exp(-ikr**itb);
    res[q]+= v*idx;
  }
  x0=pi_+epspi;
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t i=0; i<nx+1; i++)
  {
    real_t x=x0+i*dx;
    a[i]=x-acosh(1./std::abs(std::cos(x)))*i_;
    b[i]=std::cos(a[i]);
    c[i]=1+i_/std::cos(x);
  }
  if(uplow==1)
  {
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t i=0; i<nx+1; i++)
      gs[i] = wedge_dndirP(a[i], Phi, phi0)-wedge_dndirP(a[i]-2*pi_, Phi, phi0);
  }
  else
  {
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t i=0; i<nx+1; i++)
      gs[i] = wedge_dndirM(a[i], Phi, phi0)-wedge_dndirM(a[i]-2*pi_, Phi, phi0);
  }

  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t q=0; q<nkrs; q++)
  {
    complex_t ikr=krs[q]*i_;
    complex_t v=0.;
    std::vector<complex_t>::iterator itc=c.begin(), itb=b.begin(), itg=gs.begin();
    v+=0.5**itg**itc*std::exp(-ikr**itb);
    itg++; itc++; itb++;
    for(number_t j=1; j<nx; j++, ++itg, ++itc, ++itb) v+=*itg**itc*std::exp(-ikr**itb);
    v+=0.5**itg**itc*std::exp(-ikr**itb);
    res[q]+= v*idx;
    res[q]/=krs[q];  // normalization: divide final result by krs[q]
    if(uplow==1)  // dn = -dphi (wedge outward normal)
    {
      if(fp!=_wedgeField)
      {
        if(phi0>Phi-pi_ && Phi>=psip) //add upper reflected field
          res[q]-=i_*exp(ikr*std::cos(Phi-psip))*std::sin(Phi-psip);
        if(fp==_totalField && phi0>=Phi-pi_) //add incident field
          res[q]-=i_*exp(-ikr*std::cos(Phi-phi0))*std::sin(Phi-phi0);
      }
    }
    else // dn = dphi (wedge outward normal)
    {
      if(fp!=_wedgeField)
      {
        if(phi0<pi_-Phi && Phi<=psim) //add lower reflected field
          res[q]-=i_*exp(ikr*std::cos(Phi+psim))*std::sin(Phi+psim);
        if(fp==_totalField && phi0<=pi_-Phi) //add incident field
          res[q]-=i_*exp(-ikr*std::cos(Phi+phi0))*std::sin(Phi+phi0);
      }
    }
  }
  return res;
}

/* main wegde diffraction computation function using Sommerfeld path
     path gammam0 : zm0 (t) = t(1-am)+tbm + i*cm       t=0,1  am<-pi-eps, bm>eps>0
     path gammamM: zmM (t) = am + i*(1-t)cm+i*t*dm    t=0,1  dm>cm>0  with dm large enough
     path gammamP: zmP (t) = bm + i*(1-t)cm+i*t*dm    t=0,1
     path gammap0 : zp0 (t) = t*(1-ap)+t*bp + i*cp     t=0,1  ap<-eps, bp>pi+eps>0
     path gammapM: zpM (t) = ap + i*(1-t)*cp+i*t*dp   t=0,1  dp<cp<0 with dp large enough
     path gammapP: zpP (t) = bp + i*(1-t)*cp+i*t*dp   t=0,1
     compute int_gammamP + int_gammam0 - int_gammamM f(z)dz + int_gammapM - int_gammap0 + int_gammapM f(z)dz
           = int_[0,1] f(zm0(t))zm0'(t)dt +int_[0,1]( f(zmP(t))zmP'(t) - f(zmM(t))zmM'(t) ) dt
           - int_[0,1] f(zp0(t))zp0'(t)dt +int_[0,1]( f(zpM(t))zpM'(t) - f(zpP(t))zpP'(t) ) dt
*/
std::vector<complex_t> wedgeDiffractionSOM(const std::vector<real_t>& krs, const std::vector<real_t>& phis,
    real_t Phi, real_t phi0,  FieldPart fp, BcType bcP, BcType bcM,
    complex_t thetaP, complex_t thetaM)
{
  typedef complex_t (*wg_fun)(const complex_t&, real_t, real_t, const complex_t&, const complex_t&, const complex_t&, const complex_t&,  bool, const Malyuzhinets&);

  checkBC(bcP, bcM, thetaP, thetaM);
  Malyuzhinets mal(Phi);
  mal.quad=_defaultCal;
  number_t ng=mal.ngamma;

  // initialization
  real_t psip= 2*Phi-phi0-pi_;    // upper reflection angle
  real_t psim=-2*Phi-phi0+pi_;    // lower refection angle
  wg_fun g = wedge_dir;           // kernel
  complex_t sP=-1, sM=-1;         // sign of reflection
  complex_t coeffP=1., coeffM=1.; // Fourier coefficient
  real_t pis2=pi_/2;
  bool istd = true;              // use standard mode
  real_t eps=0.13;               // bound parameters in integrals
  real_t am=-pi_-eps, bm=eps, cm=eps, dm=cm+4*Phi;
  real_t ap=-eps, bp=pi_+eps, cp=-cm, dp=-dm;

  switch(bcP)
  {
    case _NeumannEc:
      g=wedge_neu;
      sM=1;
      sP=1.;
      break;
    case _FourierEc:
      g=wedge_fou;
      sP=1-2*std::sin(thetaP)/(-std::sin(phi0-Phi)+std::sin(thetaP));
      sM=1-2*std::sin(thetaM)/(std::sin(phi0+Phi)+std::sin(thetaM));
      if(!istd)
      {
        if(real(thetaP)<0)
          coeffP = mal(phi0+Phi-thetaP-pis2) * mal(phi0+Phi+thetaP+pis2);
        else
          coeffP = mal(phi0+Phi+thetaP-pis2) * mal(phi0+Phi-thetaP+pis2);
        if(real(thetaM)<0)
          coeffM = mal(phi0-Phi-thetaM-pis2) * mal(phi0-Phi+thetaM+pis2);
        else
          coeffM = mal(phi0-Phi+thetaM-pis2) * mal(phi0-Phi-thetaM+pis2);
      }
      else
      {
        coeffP = mal(phi0+Phi+thetaP-pis2) * mal(phi0+Phi-thetaP+pis2);
        coeffM = mal(phi0-Phi+thetaM-pis2) * mal(phi0-Phi-thetaM+pis2);
      }
    default:
      break;
  }

  //duplicate mal for multithreading
  std::vector<Malyuzhinets*> mals(numberOfThreads());
  for(number_t i=0; i<numberOfThreads(); i++) // update internal parameters
  {
    mals[i]=new Malyuzhinets(mal);
    mals[i]->pars("object")=static_cast<const void*>(mals[i]);
  }

  number_t nphis=phis.size(), nkrs=krs.size();
  std::vector<complex_t> vs(nphis*nkrs,0.);
  real_t krmax= *xlifepp::maxElementTpl(krs.begin(),krs.end());

  // integral on bounded arc  [am,bm] + i.cm and [ap,bp] + i.cp
  number_t n0=std::max(int_t(50),round(5*krmax*std::cosh(cm)/pi_));
  real_t dt=1./n0;
  std::vector<complex_t> zM(n0+1), zP(n0+1), cM(n0+1), cP(n0+1), gsM(n0+1), gsP(n0+1);
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t i=0; i<n0+1; i++)
  {
    real_t t= i*dt;
    zM[i]=(1-t)*am+t*bm+i_*cm;
    zP[i]=(1-t)*ap+t*bp+i_*cp;
    cM[i]=std::cos(zM[i]);
    cP[i]=std::cos(zP[i]);
  }
  complex_t idxM=(bm-am)*dt/(4*i_*Phi), idxP=(bp-ap)*dt/(4*i_*Phi);
  for(number_t p=0; p<nphis; p++)
  {
    if(std::abs(phis[p])<= Phi+theTolerance)
    {
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t i=0; i<n0+1; i++)
      {
        gsM[i] = g(zM[i], phis[p], phi0, thetaP, thetaM, coeffP,coeffM, istd, *mals[currentThread()]);
        gsP[i] = g(zP[i], phis[p], phi0, thetaP, thetaM, coeffP,coeffM, istd, *mals[currentThread()]);
      }
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t q=0; q<nkrs; q++)
      {
        complex_t vM=0., vP=0., ikr=i_*krs[q];
        vM=0.5*gsM[0]*std::exp(-ikr*cM[0]);
        vP=0.5*gsP[0]*std::exp(-ikr*cP[0]);
        for(number_t k=1; k<n0; k++)
        {
          vM+=gsM[k]*std::exp(-ikr*cM[k]);
          vP+=gsP[k]*std::exp(-ikr*cP[k]);
        }
        vM+=0.5*gsM[n0]*std::exp(-ikr*cM[n0]);
        vP+=0.5*gsP[n0]*std::exp(-ikr*cP[n0]);
        vs[p*nkrs+q] = idxP*vP-idxM*vM;
      }
    }
  }
  // integral on 'infinite' arcs am + i.[cm,dm] and bm + i.[cm,dm]
  number_t ninf=round(10*dm);
  dt=1./ninf;
  zM.resize(ninf+1);
  zP.resize(ninf+1);
  cM.resize(ninf+1);
  cP.resize(ninf+1);
  gsM.resize(ninf+1);
  gsP.resize(ninf+1);
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t i=0; i<ninf+1; i++)
  {
    real_t t= i*dt;
    zM[i]=am+i_*((1-t)*cm+t*dm);
    zP[i]=bm+i_*((1-t)*cm+t*dm);
    cM[i]=std::cos(zM[i]);
    cP[i]=std::cos(zP[i]);
  }
  idxM=(dm-cm)*dt/(4*Phi);
  for(number_t p=0; p<nphis; p++)
  {
    if(std::abs(phis[p])<= Phi+theTolerance)
    {
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t i=0; i<ninf+1; i++)
      {
        gsM[i] = g(zM[i], phis[p], phi0, thetaP, thetaM, coeffP,coeffM, istd, *mals[currentThread()]);
        gsP[i] = g(zP[i], phis[p], phi0, thetaP, thetaM, coeffP,coeffM, istd, *mals[currentThread()]);
      }
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t q=0; q<nkrs; q++)
      {
        complex_t vM=0., vP=0., ikr=i_*krs[q];
        vM=0.5*gsM[0]*std::exp(-ikr*cM[0]);
        vP=0.5*gsP[0]*std::exp(-ikr*cP[0]);
        for(number_t k=1; k<ninf; k++)
        {
          vM+=gsM[k]*std::exp(-ikr*cM[k]);
          vP+=gsP[k]*std::exp(-ikr*cP[k]);
        }
        vM+=0.5*gsM[ninf]*std::exp(-ikr*cM[ninf]);
        vP+=0.5*gsP[ninf]*std::exp(-ikr*cP[ninf]);
        vs[p*nkrs+q] += idxM*(vM-vP);
      }
    }
  }
  // integral on 'infinite' arcs ap + i.[cp,dp] and bp + i.[cp,dp]
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel
  #endif // XLIFEPP_WITH_OMP
  for(number_t i=0; i<ninf+1; i++)
  {
    real_t t= i*dt;
    zM[i]=ap+i_*((1-t)*cp+t*dp);
    zP[i]=bp+i_*((1-t)*cp+t*dp);
    cM[i]=std::cos(zM[i]);
    cP[i]=std::cos(zP[i]);
  }
  idxP=(dp-cp)*dt/(4*Phi);
  for(number_t p=0; p<nphis; p++)
  {
    if(std::abs(phis[p])<= Phi+theTolerance)
    {
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t i=0; i<ninf+1; i++)
      {
        gsM[i] = g(zM[i], phis[p], phi0, thetaP, thetaM, coeffP,coeffM, istd, *mals[currentThread()]);
        gsP[i] = g(zP[i], phis[p], phi0, thetaP, thetaM, coeffP,coeffM, istd, *mals[currentThread()]);
      }
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for
      #endif // XLIFEPP_WITH_OMP
      for(number_t q=0; q<nkrs; q++)
      {
        complex_t vM=0., vP=0., ikr=i_*krs[q];
        vM=0.5*gsM[0]*std::exp(-ikr*cM[0]);
        vP=0.5*gsP[0]*std::exp(-ikr*cP[0]);
        for(number_t k=1; k<ninf; k++)
        {
          vM+=gsM[k]*std::exp(-ikr*cM[k]);
          vP+=gsP[k]*std::exp(-ikr*cP[k]);
        }
        vM+=0.5*gsM[ninf]*std::exp(-ikr*cM[ninf]);
        vP+=0.5*gsP[ninf]*std::exp(-ikr*cP[ninf]);
        vs[p*nkrs+q] += idxP*(vP-vM);
      }
    }
  }

  if(fp!=_totalField)  // remove some inc/ref field
  {
    for(number_t p=0; p<nphis; p++)
    {
      if(std::abs(phis[p])<= Phi+theTolerance)
      {
        if(fp==_diffractedField) //remove incident field everywhere
        {
          #ifdef XLIFEPP_WITH_OMP
          #pragma omp parallel for
          #endif // XLIFEPP_WITH_OMP
          for(number_t q=0; q<nkrs; q++)
            vs[p*nkrs+q]-=exp(-i_*krs[q]*std::cos(phis[p]-phi0));
        }
        else
        {
          if(std::abs(phi0)<=pi_-Phi || (phi0>pi_-Phi && phis[p]>=phi0-pi_) || (phi0<Phi-pi_ && phis[p]<=phi0+pi_))  // remove incident field in light zone
          {
            #ifdef XLIFEPP_WITH_OMP
            #pragma omp parallel for
            #endif // XLIFEPP_WITH_OMP
            for(number_t q=0; q<nkrs; q++)
              vs[p*nkrs+q]-=exp(-i_*krs[q]*std::cos(phis[p]-phi0));
          }
          if(phi0>Phi-pi_ && phis[p]>=psip) //remove upper reflected field
          {
            #ifdef XLIFEPP_WITH_OMP
            #pragma omp parallel for
            #endif // XLIFEPP_WITH_OMP
            for(number_t q=0; q<nkrs; q++)
              vs[p*nkrs+q]-=sP*exp(i_*krs[q]*std::cos(phis[p]-psip));
          }
          if(phi0<pi_-Phi && phi0<=pi_-Phi && phis[p]<=psim) //remove lower reflected field
          {
            #ifdef XLIFEPP_WITH_OMP
            #pragma omp parallel for
            #endif // XLIFEPP_WITH_OMP
            for(number_t q=0; q<nkrs; q++)
              vs[p*nkrs+q]-=sM*exp(i_*krs[q]*std::cos(phis[p]-psim));
          }
        }
      }
    }
  }

  for(number_t i=0; i<numberOfThreads(); i++)
    delete mals[i];
  //end of computation
  return vs;
}

std::vector<complex_t> wedgeCurrentSOM(const std::vector<real_t>& krs, real_t Phi, real_t phi0, FieldPart fp, BcType bcP, BcType bcM,
                                       complex_t thetaP, complex_t thetaM)
{
  checkBC(bcP, bcM, thetaP, thetaM);
  if(bcP!=_DirichletEc && bcM!=_DirichletEc)
  {
    std::vector<real_t> phis(2,Phi);
    phis[1]=-Phi;
    return wedgeDiffractionSOM(krs,phis,Phi,phi0,fp,bcP,bcM,thetaP,thetaM);
  }
  std::vector<complex_t> ds = wedgeDirCurrentSOM(krs,Phi,phi0,fp,bcP,bcM);
  if(bcP!=_DirichletEc)  // so bcM = _DirichletEC
  {
    std::vector<real_t> phis(1,Phi);
    std::vector<complex_t> cs = wedgeDiffractionSOM(krs,phis,Phi,phi0,fp,bcP,bcM,thetaP,thetaM);
    cs.insert(cs.end(),ds.begin(),ds.end());
    return cs;
  }
  if(bcM!=_DirichletEc)  // so bcP = _DirichletEC
  {
    std::vector<real_t> phis(1,-Phi);
    std::vector<complex_t> cs = wedgeDiffractionSOM(krs,phis,Phi,phi0,fp,bcP,bcM,thetaP,thetaM);
    ds.insert(ds.end(),cs.begin(),cs.end());
    return ds;
  }
  //bcM and bcP = _DirichletEC
  return ds;
}

// compute du/dn  (diffracted field) in case of Dirichlet problem, using Sommerfeld path
std::vector<complex_t> wedgeDirCurrentSOM(const std::vector<real_t>& krs, real_t Phi, real_t phi0, FieldPart fp, BcType bcP, BcType bcM)
{
  number_t nr=0, nkrs=krs.size();
  if(bcP==_DirichletEc)
    nr+=nkrs;
  if(bcM==_DirichletEc)
    nr+=nkrs;
  if(nr==0)
    return std::vector<complex_t>();   // nothing to do, return void vector
  std::vector<complex_t> res(nr,0.);
  // initialization
  real_t psip= 2*Phi-phi0-pi_;    // upper reflection angle
  real_t psim=-2*Phi-phi0+pi_;    // lower refection angle
  complex_t sP=-1, sM=-1;         // sign of reflection
  complex_t coeffP=1., coeffM=1.; // Fourier coefficient
  real_t pis2=pi_/2;
  bool istd = true;              // use standard mode
  real_t eps=0.13;               // bound parameters in integrals
  real_t am=-pi_-eps, bm=eps, cm=eps, dm=cm+4*Phi;
  real_t ap=-eps, bp=pi_+eps, cp=-cm, dp=-dm;
  real_t krmax= *xlifepp::maxElementTpl(krs.begin(),krs.end());

  // integral on bounded arc  [am,bm] + i.cm and [ap,bp] + i.cp
  number_t n0=std::max(int_t(50),round(5*krmax*std::cosh(cm)/pi_));
  real_t dt=1./n0;
  std::vector<complex_t> zM(n0+1), zP(n0+1), cM(n0+1), cP(n0+1), gsM(n0+1), gsP(n0+1);
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t i=0; i<n0+1; i++)
  {
    real_t t= i*dt;
    zM[i]=(1-t)*am+t*bm+i_*cm;
    zP[i]=(1-t)*ap+t*bp+i_*cp;
    cM[i]=std::cos(zM[i]);
    cP[i]=std::cos(zP[i]);
  }
  complex_t idxM=(bm-am)*dt/(4*i_*Phi), idxP=(bp-ap)*dt/(4*i_*Phi);
  number_t k=0;
  if(bcP==_DirichletEc)
  {
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t i=0; i<n0+1; i++)
    {
      gsM[i] = wedge_dndirP(zM[i], Phi, phi0);
      gsP[i] = wedge_dndirP(zP[i], Phi, phi0);
    }
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t q=0; q<nkrs; q++)
    {
      complex_t vM=0., vP=0., ikr=i_*krs[q];
      vM=0.5*gsM[0]*std::exp(-ikr*cM[0]);
      vP=0.5*gsP[0]*std::exp(-ikr*cP[0]);
      for(number_t i=1; i<n0; i++)
      {
        vM+=gsM[i]*std::exp(-ikr*cM[i]);
        vP+=gsP[i]*std::exp(-ikr*cP[i]);
      }
      vM+=0.5*gsM[n0]*std::exp(-ikr*cM[n0]);
      vP+=0.5*gsP[n0]*std::exp(-ikr*cP[n0]);
      res[q] = idxP*vP-idxM*vM;
    }
    k=nkrs;
  }
  if(bcM==_DirichletEc)
  {
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t i=0; i<n0+1; i++)
    {
      gsM[i] = wedge_dndirM(zM[i], Phi, phi0);
      gsP[i] = wedge_dndirM(zP[i], Phi, phi0);
    }
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t q=0; q<nkrs; q++)
    {
      complex_t vM=0., vP=0., ikr=i_*krs[q];
      vM=0.5*gsM[0]*std::exp(-ikr*cM[0]);
      vP=0.5*gsP[0]*std::exp(-ikr*cP[0]);
      for(number_t i=1; i<n0; i++)
      {
        vM+=gsM[i]*std::exp(-ikr*cM[i]);
        vP+=gsP[i]*std::exp(-ikr*cP[i]);
      }
      vM+=0.5*gsM[n0]*std::exp(-ikr*cM[n0]);
      vP+=0.5*gsP[n0]*std::exp(-ikr*cP[n0]);
      res[k+q] = idxP*vP-idxM*vM;
    }
  }

  // integral on 'infinite' arcs am + i.[cm,dm] and bm + i.[cm,dm]
  number_t ninf=round(10*dm);
  dt=1./ninf;
  zM.resize(ninf+1);
  zP.resize(ninf+1);
  cM.resize(ninf+1);
  cP.resize(ninf+1);
  gsM.resize(ninf+1);
  gsP.resize(ninf+1);
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t i=0; i<ninf+1; i++)
  {
    real_t t= i*dt;
    zM[i]=am+i_*((1-t)*cm+t*dm);
    zP[i]=bm+i_*((1-t)*cm+t*dm);
    cM[i]=std::cos(zM[i]);
    cP[i]=std::cos(zP[i]);
  }
  idxM=(dm-cm)*dt/(4*Phi);
  k=0;
  if(bcP==_DirichletEc)
  {
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t i=0; i<ninf+1; i++)
    {
      gsM[i] = wedge_dndirP(zM[i], Phi, phi0);
      gsP[i] = wedge_dndirP(zP[i], Phi, phi0);
    }
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t q=0; q<nkrs; q++)
    {
      complex_t vM=0., vP=0., ikr=i_*krs[q];
      vM=0.5*gsM[0]*std::exp(-ikr*cM[0]);
      vP=0.5*gsP[0]*std::exp(-ikr*cP[0]);
      for(number_t i=1; i<ninf; i++)
      {
        vM+=gsM[i]*std::exp(-ikr*cM[i]);
        vP+=gsP[i]*std::exp(-ikr*cP[i]);
      }
      vM+=0.5*gsM[ninf]*std::exp(-ikr*cM[ninf]);
      vP+=0.5*gsP[ninf]*std::exp(-ikr*cP[ninf]);
      res[q] += idxM*(vM-vP);
    }
    k=nkrs;
  }
  if(bcM==_DirichletEc)
  {
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t i=0; i<ninf+1; i++)
    {
      gsM[i] = wedge_dndirM(zM[i], Phi, phi0);
      gsP[i] = wedge_dndirM(zP[i], Phi, phi0);
    }
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t q=0; q<nkrs; q++)
    {
      complex_t vM=0., vP=0., ikr=i_*krs[q];
      vM=0.5*gsM[0]*std::exp(-ikr*cM[0]);
      vP=0.5*gsP[0]*std::exp(-ikr*cP[0]);
      for(number_t i=1; i<ninf; i++)
      {
        vM+=gsM[i]*std::exp(-ikr*cM[i]);
        vP+=gsP[i]*std::exp(-ikr*cP[i]);
      }
      vM+=0.5*gsM[ninf]*std::exp(-ikr*cM[ninf]);
      vP+=0.5*gsP[ninf]*std::exp(-ikr*cP[ninf]);
      res[k+q] += idxM*(vM-vP);
    }
  }

  // integral on 'infinite' arcs ap + i.[cp,dp] and bp + i.[cp,dp]
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t i=0; i<ninf+1; i++)
  {
    real_t t= i*dt;
    zM[i]=ap+i_*((1-t)*cp+t*dp);
    zP[i]=bp+i_*((1-t)*cp+t*dp);
    cM[i]=std::cos(zM[i]);
    cP[i]=std::cos(zP[i]);
  }
  idxP=(dp-cp)*dt/(4*Phi);
  k=0;
  if(bcP==_DirichletEc)
  {
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t i=0; i<ninf+1; i++)
    {
      gsM[i] = wedge_dndirP(zM[i], Phi, phi0);
      gsP[i] = wedge_dndirP(zP[i], Phi, phi0);
    }
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t q=0; q<nkrs; q++)
    {
      complex_t vM=0., vP=0., ikr=i_*krs[q];
      vM=0.5*gsM[0]*std::exp(-ikr*cM[0]);
      vP=0.5*gsP[0]*std::exp(-ikr*cP[0]);
      for(number_t i=1; i<ninf; i++)
      {
        vM+=gsM[i]*std::exp(-ikr*cM[i]);
        vP+=gsP[i]*std::exp(-ikr*cP[i]);
      }
      vM+=0.5*gsM[ninf]*std::exp(-ikr*cM[ninf]);
      vP+=0.5*gsP[ninf]*std::exp(-ikr*cP[ninf]);
      res[q] += idxP*(vP-vM);
      res[q]/=krs[q];  // normalization: divide final result by krs[q]
    }
    k=nkrs;
  }
  if(bcM==_DirichletEc)
  {
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t i=0; i<ninf+1; i++)
    {
      gsM[i] = wedge_dndirM(zM[i], Phi, phi0);
      gsP[i] = wedge_dndirM(zP[i], Phi, phi0);
    }
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for
    #endif // XLIFEPP_WITH_OMP
    for(number_t q=0; q<nkrs; q++)
    {
      complex_t vM=0., vP=0., ikr=i_*krs[q];
      vM=0.5*gsM[0]*std::exp(-ikr*cM[0]);
      vP=0.5*gsP[0]*std::exp(-ikr*cP[0]);
      for(number_t i=1; i<ninf; i++)
      {
        vM+=gsM[i]*std::exp(-ikr*cM[i]);
        vP+=gsP[i]*std::exp(-ikr*cP[i]);
      }
      vM+=0.5*gsM[ninf]*std::exp(-ikr*cM[ninf]);
      vP+=0.5*gsP[ninf]*std::exp(-ikr*cP[ninf]);
      res[k+q] += idxP*(vP-vM);
      res[k+q]/=krs[q];  // normalization: divide final result by krs[q]
    }
  }

  if(fp!=_totalField) // remove some inc/ref field
  {
    if(fp==_diffractedField) //remove incident field everywhere
    {
      k=0;
      if(bcP==_DirichletEc) //dn = -dphi (wedge outward normal)
      {
        #ifdef XLIFEPP_WITH_OMP
        #pragma omp parallel for
        #endif // XLIFEPP_WITH_OMP
        for(number_t q=0; q<nkrs; q++)
          res[q]+=i_*exp(-i_*krs[q]*std::cos(Phi-phi0))*std::sin(Phi-phi0);
        k=nkrs;
      }
      if(bcM==_DirichletEc) //dn = dphi (wedge outward normal)
      {
        #ifdef XLIFEPP_WITH_OMP
        #pragma omp parallel for
        #endif // XLIFEPP_WITH_OMP
        for(number_t q=0; q<nkrs; q++)
          res[k+q]+=i_*exp(-i_*krs[q]*std::cos(Phi+phi0))*std::sin(Phi+phi0);
      }
    }
    else
    {
        k=0;
        if(bcP==_DirichletEc) //dn = -dphi (wedge outward normal)
        {
          if(phi0>=Phi-pi_)  // remove incident field in light zone
          {
            #ifdef XLIFEPP_WITH_OMP
            #pragma omp parallel for
            #endif // XLIFEPP_WITH_OMP
            for(number_t q=0; q<nkrs; q++)
                res[q]+=i_*exp(-i_*krs[q]*std::cos(Phi-phi0))*std::sin(Phi-phi0);
          }
          if(phi0>=Phi-pi_) //remove upper reflected field: dn = -dphi (wedge outward normal)
          {
            #ifdef XLIFEPP_WITH_OMP
            #pragma omp parallel for
            #endif // XLIFEPP_WITH_OMP
            for(number_t q=0; q<nkrs; q++)
              res[q]-=i_*exp(i_*krs[q]*std::cos(Phi-psip))*std::sin(Phi-psip);;
          }
         k=nkrs;
        }
        if(bcM==_DirichletEc) //dn = dphi (wedge outward normal)
        {
          if(phi0<=pi_-Phi) // remove incident field in light zone
          {
            #ifdef XLIFEPP_WITH_OMP
            #pragma omp parallel for
            #endif // XLIFEPP_WITH_OMP
            for(number_t q=0; q<nkrs; q++)
                res[k+q]+=i_*exp(-i_*krs[q]*std::cos(Phi+phi0))*std::sin(Phi+phi0);
          }
          if(phi0<=pi_-Phi) //remove lower reflected field: dn = dphi (wedge outward normal)
          {
            #ifdef XLIFEPP_WITH_OMP
            #pragma omp parallel for
            #endif // XLIFEPP_WITH_OMP
            for(number_t q=0; q<nkrs; q++)
              res[k+q]-=i_*exp(i_*krs[q]*std::cos(Phi+psip))*std::sin(Phi+psip);;
          }
        }
    }
  }
  //end of computation
  return res;
}


}//end namespace xlifpp
