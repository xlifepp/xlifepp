/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file FockFunction.cpp
  \author E. Lunéville
  \since 24 Mar 2019
  \date  24 Mar 2019

  \brief Implementation of Fock functions stuff

                        1     /+inf     exp(-ixt)
       fock_s(x,q) = -------  |      --------------dt
                     sqrt(pi) /-inf   w2'(t)-qw2(t)

            w2(t) = 2sqrt(pi)exp(-ipi/6)Ai(texp(-2ipi/3))

        computed using several methods:

                           a                        b
        -------------------|------------------------|---------------------> x
         stationary phase     integration by parts      residue (only 1)
                              + Filon quadrature
 */

#include "FockFunction.hpp"
#include "../quadratureMethods.hpp"
#include "utils.h"
#include <iostream>

namespace xlifepp
{
// constructor
Fock::Fock(number_t o, number_t nr, number_t vnf, const complex_t& vq, real_t va, real_t vb, real_t vc, real_t vd, bool rot)
  : q(vq), ord(o), nbr(nr), a(va), b(vb), c(vc), d(vd), nf(vnf), rotate(rot), table(nullptr)
{
  qis0 = (q==complex_t(0));
  initFilon(c,d,nf,rot);
  initResidue(nbr, !qis0);
}

// constructor loading a Fock table
Fock::Fock(const string_t& filename)
  : q(0), qis0(true), ord(0), nbr(1), a(0), b(0), c(0), d(0), nf(0), table(nullptr)
{
  loadTable(filename);
}

void Fock::initFilonMP(real_t a, real_t b, number_t nfM, number_t nfP, bool rot)
{
  rotate = rot;
  Parameters pars(q,"q");
  pars.add("s",0.);
  pars.add("r",complex_t(1.));
  fimP=FilonIM(1,nfP,b,filon_f,pars);
  if(rot) pars("r")=-expipiover3_; // exp(-2*i_*pi_/3);
  fimM=FilonIM(1,nfM,-a,filon_f,pars);
}

void Fock::initFilon(real_t vc, real_t vd, number_t vnf, bool rot)
{
    c=vc; d=vd; nf=vnf; rotate=rot;
    Parameters pars(q,"q");
    if(!rotate)
    {
        pars.add("s",c);
        pars.add("r",complex_t(1.));
        fimAP=FilonIM(1,nf,d-c,filon_f,pars);
    }
    else
    {
        pars.add("s",0.);
        pars.add("r",complex_t(1.));
        number_t nfp=number_t(d*nf/(d-c));
        fimAP=FilonIM(1,nfp,d,filon_f,pars);
        pars("r")=-expipiover3_; // exp(-2*i_*pi_/3);
        fimAM=FilonIM(1,nf-nfp,-c,filon_f,pars);
    }
}

// reset object to void
void Fock::clear()
{
  q=0; a=0; b=0; c=0; d=0; nf=0; ord=0; nbr=1;
  fimAP.clear();
  fimAM.clear();
  fimM.clear();
  fimP.clear();
  delete table;
  table=nullptr;
}

/* compute surface Fock function using exact integrand and Filon methods (a<0, b>0)

                               /0              /b
       sqrt(pi)*fock_s(x,q) =  |   j_s(t)dt +  |   j_s(t)dt
                               /a              /0
                                 /-a                       /b
                            = -r |   exp(-ixrt).f(rt)dt +  |   exp(-ixt).f(t)dt  with r=exp(-2ipi/3)
                                 /0                        /0
                            = -r*fimM(x*r)+fimP(x)
*/
complex_t Fock::Fock_s_Filon(real_t x) const
{
  if(fimP.size()==0 || fimP.size()==0)
    error("free_error", "in Fock::Fock_s, FilonIM objects are not correctly initialized, use initFilonMP");
  if(rotate)
  {
      complex_t r=-expipiover3_; // exp(-2ipi/3)
      return (fimP(x)-r*fimM(x*r))/sqrtOfpi_;
  }
  return (fimP(x)-fimM(x))/sqrtOfpi_;
}

/* compute surface Fock function using exact integrand trapeze method

                               /b             /b              /-a
       sqrt(pi)*fock_s(x,q) =  |   j_s(t)dt = |   j_s(t)dt - r|   j_s(rt)dt  with r=exp(-2ipi/3)
                               /a             /0              /0
*/
complex_t Fock::Fock_s_trapz(real_t x, real_t a, real_t b, number_t nt) const
{
  number_t ntp=std::floor(nt*b/(b-a)), ntm=nt-ntp;
  std::vector<complex_t> vj(ntp+1);
  real_t dt=b/ntp;
  #pragma omp for
  for(number_t n=0;n<ntp+1; n++)
  {
      vj[n] = j_s(n*dt,x,q);
  }
  complex_t ip=trapz(vj,dt);
  vj.resize(ntm+1); dt=-a/ntm;
  complex_t r=exp(-2*i_*pi_/3);
  #pragma omp for
  for(number_t n=0;n<ntm+1; n++)
  {
      vj[n] = j_s(n*dt*r,x,q);
  }
  complex_t im=trapz(vj,dt);
  return (ip-r*im)/sqrtOfpi_;
}

//main Fock computation function
complex_t Fock::Fock_s_app(real_t x) const
{
  if(x<= a) return spFock_s(x)/sqrtOfpi_;    // use stationary phase approximation
  if(x>= b) return resFock_s(x)/sqrtOfpi_;   // use residue method
  if(!rotate) return (ippFock_s(x,-c,-1) + std::exp(-i_*x*c)*fimAP(x) + ippFock_s(x,d,1))/sqrtOfpi_; // use ipp and Filon
  complex_t r=-expipiover3_; // exp(-2ipi/3)
  return (ippFock_s(x,-c,-2) + fimAP(x)-r*fimAM(r*x) + ippFock_s(x,d,1))/sqrtOfpi_; // use ipp and Filon
}

/*! precomputation of poles zk of w2'-q.w2 and values zk.w2(zk)-q.w2'(zk) = (zk-q^2)w2(zk)
     q = 0 : zk = exp(2ipi/3)ak' with ak' the zeros of Ai' that are tabulated (up to 10)
     q=inf: zk = exp(2ipi/3)ak  with ak  the zeros of Ai that are tabulated (up to 10)
     when q!=0, zk is approximated from zeros of ak' (q small) or ak (q large)
     if withNewton is on, Newton's method is used to get a better approximation
*/
void Fock::initResidue(number_t nbp, bool withNewton)
{
    if(nbp==0) error("free_error"," number of poles in Fock computation must be >0");
    nbr=nbp;
    poles.resize(nbp); vpoles.resize(nbp);
    number_t nbe=std::min(number_t(10),nbp);
    real_t aq = std::abs(q);
    if(aq<3)  // zeros of w2' (= exp(2ipi/3) * zeros of Ai')
    {
      switch(nbe)
      {
          case 10: poles[9] = -12.3847883718;
          case  9: poles[8] = -11.4750566335;
          case  8: poles[7] = -10.5276603970;
          case  7: poles[6] =  -9.5354490524;
          case  6: poles[5] =  -8.4884867340;
          case  5: poles[4] =  -7.3721772550;
          case  4: poles[3] =  -6.1633073556;
          case  3: poles[2] =  -4.8200992112;
          case  2: poles[1] =  -3.2481975822;
          case  1: poles[0] =  -1.0187929716;
          default: break;
      }
      if(nbp>10) // compute roots using newton
      {
          for(number_t n=11;n<=nbp;n++)
          {
              real_t r =-std::pow((1.5*pi_*(n-0.75)),2./3.);
              xlifepp::newton(airyRp,airyRpp,r);
              poles[n-1]=r;
          }
      }
    }
    else // zeros of Ai
    {
        switch(nbe)
      {
          case 10: poles[9] = -12.8287767529;
          case  9: poles[8] = -11.9360155632;
          case  8: poles[7] = -11.0085243037;
          case  7: poles[6] = -10.0401743416;
          case  6: poles[5] =  -9.0226508533;
          case  5: poles[4] =  -7.9441335871;
          case  4: poles[3] =  -6.7867080901;
          case  3: poles[2] =  -5.5205598281;
          case  2: poles[1] =  -4.0879494441;
          case  1: poles[0] =  -2.3381074105;
          default: break;
      }
      if(nbp>10) // compute roots using newton
      {
          for(number_t n=11;n<=nbp;n++)
          {
              real_t r=-std::pow((1.5*pi_*(n-0.25)),2./3.);
              xlifepp::newton(airyR,airyRp,r);
              poles[n-1]=r;
          }
      }
    }
    complex_t e23= expipiover3_*expipiover3_;  // exp(2ipi/3)
    for(number_t k=0;k<nbp; k++) poles[k]*=e23;
    if(aq < theEpsilon) // case q=0: values vk = zk.w2(zk)
    {
       for(number_t k=0;k<nbp; k++) vpoles[k]=poles[k]*w2_Ai(poles[k]);
       return;
    }
    if(aq >= theRealMax) // case q=inf: values vk = w2'(zk)
    {
        for(number_t k=0;k<nbp; k++) vpoles[k]=w2_Ai(poles[k],_dt);
        return;
    }

    // poles zk are zeros of w2'-q.w2, values are vk=zk.w2(zk)-q.w2'(zk)=(zk-q*q)w2(zk)
    number_t reject=0;
    if(aq<3) // "small" q
    {
        complex_t s=q, s2=s*s, s3=s*s2, s4=s2*s2, s5=s3*s2, root;
        for(number_t k=0;k<nbp;k++)
        {
            complex_t r = 1./poles[k], r2=r*r, r3=r2*r, r5=r2*r3, r6=r3*r3;
            root = poles[k] + r*s - 0.5*r3*s2 + (r2/3.+0.5*r5)*s3 - (7.*r2*r2/12.+5.*r5*r2/8.)*s4
                            + (r3/5. +21*r6/20.+35*r3*r6/40.)*s5-(29*r5/45.+231*r6*r2/120.+63*r6*r5/48.)*s4*s2;
            if(withNewton) newton(root);  //improve roots using Newton's method
            poles[k]= root;
            if(imag(root)<0) vpoles[k]=(root-q*q)*w2_Ai(root); else {vpoles[k]=0;reject++;}
        }
    }
    else // "large" q
    {
        complex_t s=1./q, s2=s*s, s3=s2*s, s4=s3*s, s5=s4*s, s6=s5*s, root;
        for(number_t k=0;k<nbp;k++)
        {
            complex_t r = poles[k], r2=r*r, r3=r2*r;
            root = r + s + r*s3/3 + 0.25*s4 + r2*s5/5. + 7.*r*s6/18.+(5./28.+r3/7)*s6*s + 29*r2*s6*s2/60.;
            if(withNewton) newton(root);  //improve roots using Newton's method
            poles[k]= root;
            if(std::imag(root)<0.) vpoles[k]=(root-q*q)*w2_Ai(root); else {vpoles[k]=0;reject++;}
        }
    }

    if(reject>0) //removes rejected poles from  poles and vpoles vectors
    {
        nbr=nbp-reject;
        if(nbr==0) error("free_error", "in Fock::initResidue, no pole with imag <0 found");
        else warning("free_warning", "in Fock::initResidue, only "+tostring(nbr)+" pole with imag <0 found from the "+tostring(nbp)+" first");
        number_t j=0;
        for(number_t k=0;k<nbp;k++)
           {
               if(vpoles[k] != complex_t(0.))
                {
                    poles[j]=poles[k];
                    vpoles[j]=vpoles[k];
                    j++;
                }
           }
        poles.resize(nbr); vpoles.resize(nbr);
    }

    //reject multiple roots
    number_t j=1;
    real_t eps=1.e-3;
    for(number_t k=1;k<nbr;++k)
    {
       number_t i=0;
       while (std::abs(poles[k]-poles[i])> eps && i<k) i++;
       if(i==k) {poles[j]=poles[k]; vpoles[j]=vpoles[k];j++;}
    }
    if(j<nbr) {nbr=j; poles.resize(nbr); vpoles.resize(nbr);}
}

// improve roots of w2'-q.w2 using Newton's method r_k+1= r_k - h(r_k)/h'(r_k)
// with h(z)=w2'(z)-q.w2(z) and h'(z)=w2"(z)-q.w2'(z) = z.w2(z)-q.w2'(z)
// emit a warning if no convergence
void Fock::newton(complex_t& r0, number_t kmax, real_t tol) const
{
    complex_t r=r0, bestr=r0;
    complex_t w=w2_Ai(r), wp=w2_Ai(r,_dt), h=wp-q*w, hp=r*w-q*wp;
    real_t e = std::abs(h), besth=e;
    number_t k=0;
    while(e>tol && k<kmax)
    {
        r-=h/hp;
        w=w2_Ai(r); wp=w2_Ai(r,_dt);
        h=wp-q*w; hp=r*w-q*wp;
        e = std::abs(h);
        if(e<besth) {besth=e;r0=r;}  // update root
        k++;
    }
    if(e>tol)
      warning("free_warning","Newton's method for finding roots does not converge: "+tostring(besth)+">"+tostring(tol));
}

// compute Fock_s by residue requires x > 0, 1 residue is useful if x > 2
complex_t Fock::resFock_s(real_t x) const
{
   complex_t c(0.,-2*pi_);  //-2ipi
   complex_t res = 0.;
   if(imag(poles[0])<0) res=exp(-i_*x*poles[0])/vpoles[0];
   if(nbr==1) return c*res;
   for(number_t k=1; k<nbr; k++)
        if(imag(poles[k])<0) res+= exp(-i_*x*poles[k])/vpoles[k];
   return c*res;
}

// compute Fock_s2 using stationnary phase approximation, requires x < -2.
complex_t Fock::spFock_s(real_t x) const
{
  complex_t e4(0.5*sqrtOf2_,0.5*sqrtOf2_); // exp(i*pi/4);
  real_t ax=std::abs(x), rx=std::sqrt(ax), x2=x*x, x3=x2*x;
  complex_t gamma(1,7/(48*x3));
  complex_t d =e4*rx*gamma, gamma_q;
  if(q!=complex_t(0.))
    {
      gamma_q = complex_t(1,-5/(48*x3));
      d-=q*gamma_q/(e4*rx);
    }
  complex_t a=1./d;
  if(ord==0) return 2*std::exp(i_*x3/3)*e4*sqrtOfpi_*rx*a; // order 0
  //order 1
  complex_t x5=x3*x2, gamma1=-7.*i_/(32*x5), gamma2=35*i_/(64*x5*x2);
  complex_t d1=e4*(rx*gamma/(4*x2)+rx*gamma1),
            d2=e4*(-3*rx*gamma/(16*x2*x2)+rx*gamma1/(2*x2)+rx*gamma2);
  if(q!=complex_t(0.))
    {
      gamma1=5.*i_/(32*x5); gamma2=-25*i_/(64*x5*x2);
      d1+=(q/e4)*(gamma_q/(4*x2*rx)-gamma1/rx);
      d2+=(q/e4)*(-5*gamma_q/(16*x2*x2*rx)+gamma1/(x2*rx*2)-gamma2/rx);
    }
  complex_t a1=-d1/(d*d), a2=(2*d1*d1-d*d2)/(d*d*d);
  a=a-i_*(a2*x+a1/(2*x)-a/(12*x3));
  return 2*std::exp(i_*x3/3)*e4*sqrtOfpi_*rx*a;
}

/*! compute part of Fock_s using integration by parts approximation (two first terms of double ipp), c>0

            / +inf                                 / c                  / +inf
 (p=1) :    |      j_s(x,q,t)dt  or   (p=-1) :     |   j_s(x,q,t)dt =   |   j_s(x,q,-t)dt
           /c                                     /-inf                /-c

                / +inf
 or (p=-2) :  r |   j_s(x,q,r*t)dt   with r=exp(-2ipi/3)  (rotated integral)
               /c
*/
complex_t Fock::ippFock_s(real_t x, real_t c, int_t p) const
{
  real_t sc=std::sqrt(c), sc3 =sc*sc*sc, ssc=std::sqrt(sc), ssc3=std::sqrt(sc3);  // c^1/2, c^3/2, c^1/4, c^3/4
  real_t gamma=1-7./(48*sc3), gamma_q;
  if (q != 0.) gamma_q=1+5./(48*sc3);
  complex_t g, gp;
  complex_t phi, phip, phipp, p2;
  if (p==1)
  {
    phi=-i_*x*c-2*sc3/3, phip = -i_*x-sc, phipp= -i_/(sc*2);
    g=ssc*gamma;
    if (q != 0.) g-=q*gamma_q/ssc;
    gp = 0.25*gamma/ssc3+7*ssc/(32*c*sc3);
    if (q != 0.) gp+=q*(0.25*gamma_q/(c*ssc)+5./(32*ssc*c*sc3));
    gp = -gp/(g*g);
    g=1./g;
    p2=phip*phip;
  }
  else if (p==-1)
    {
      phi=i_*(x*c+2*sc3/3), phip = i_*(x+sc), phipp= i_/(sc*2);
      g=ssc*gamma;
      if (q != 0.)  g+=i_*q*gamma_q/ssc;
      gp = 0.25*gamma/ssc3+7*i_*ssc/(32*c*sc3);
      if (q != 0.)  gp-=i_*q*(0.25*gamma_q/(c*ssc)+5.*i_/(32*ssc*c*sc3));
      complex_t e4(0.5*sqrtOf2_, -0.5*sqrtOf2_); // exp(-i_*pi_/4)
      gp = -e4*gp/(g*g);
      g=e4/g;
      p2=phip*phip;
    }
  else //p=-2 (rotated integral)
  {
    complex_t r=1./expipiover3_, r2=r*r; //exp(-i*pi/3);
    phi=-i_*x*c*r2-2*sc3/3, phip = -i_*x*r2+sc, phipp=-i_/(sc*2);
    g=ssc*gamma;
    if (q != 0.)  g-=q*r*gamma_q/ssc;
    gp = 0.25*gamma/ssc3+7*ssc/(32*c*sc3);
    if (q != 0.) gp += q*r*(0.25*gamma_q/(c*ssc)+5./(32*ssc*c*sc3));
    gp = gp/(g*g);
    g=-1./g;
    p2=phip*phip;
  }
  return std::exp(phi)*(gp/p2-g*(1.+ phipp/p2)/phip);
}

//! create tabulated Fock values
void Fock::createTable(real_t x0, real_t xf, number_t nx)
{
  real_t dx = (xf-x0)/nx;
  Parameters pars(this,"Fock pointer");
  if(fimAP.size()>0) table = new Tabular<complex_t>(x0,dx,nx,ext_Fock_s_app,pars,"x");
  else if(fimM.size()>0 && fimP.size()>0) table = new Tabular<complex_t>(x0,dx,nx,ext_Fock_s,pars,"x");
  else error("free_error"," no computation available in Fock::createTable");
  table->comment="surface Fock table, q= "+tostring(real(q))+" "+tostring(imag(q));
}

//! save to file tabulated Fock values
void Fock::saveTable(const string_t& filename) const
{
  if(table!=nullptr) table->saveToFile(filename);
}

//! create and save to file tabulated Fock values
void Fock::saveTable(real_t x0, real_t xf, number_t nx, const string_t& filename)
{
  createTable(x0, xf, nx);
  saveTable(filename);
}

//! load tabulated Fock values from file
void Fock::loadTable(const string_t& filename)
{
  if(table!=nullptr) delete table;
  table = new Tabular<complex_t>(filename);
  // locate q= x y in comment
  string_t c=table->comment;
  q=complex_t(0);
  number_t p=c.find("q= ");
  if(p==std::string::npos) warning("free_warning",filename+" seems not to be a Fock table file, q is not set!");
  c.erase(0,p+3);
  std::stringstream ss(c); real_t x, y;
  ss>>x>>y;
  q=complex_t(x,y);
}

/*! compute surface Fock using linear interpolation from values in table
      x0 <= x <= x0+(n-1)*dx  (n=table.size()) NO CHECK
*/
complex_t Fock::Fock_s_interp(real_t x) const
{
  return (*table)(x);
}

// extern function miming Fock::Fock_s_app used by Tabular that does not accept member function
complex_t ext_Fock_s_app(real_t x, Parameters & pars)
{
    const Fock* fo = static_cast<const Fock*>(pars(1).get_p()); //recast void* parameter to Fock object pointer
    return fo->Fock_s_app(x);
}
// extern function miming Fock::Fock_s used by Tabular that does not accept member function
complex_t ext_Fock_s(real_t x, Parameters & pars)
{
    const Fock* fo = static_cast<const Fock*>(pars(1).get_p()); //recast void* parameter to Fock object pointer
    return fo->Fock_s_Filon(x);
}

/* Fock function involved in curvature transition from curved line to straight line (Neumann case)
                              /+inf  w1(r1-z)
   F(s) = i/sqrt(pi) s^{-1/2} |      --------- exp{i.z^2/(4.s)}dz      (r1 first root of w1')
                              /0       w1(r1)

  when s->0 F(s)-> w1(r1-z)/w1(r1-z)
*/
//complex_t fockCurvatureTransition_N(real_t x)
} // end of namespace xlifepp

