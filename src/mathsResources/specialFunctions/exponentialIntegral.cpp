/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file exponentialIntegral.cpp
  \author D. Martin
  \since 20 dec 2007
  \date 18 jan 2008

  \brief Implementation of exponential integral functions
 */

#include "specialFunctions.hpp"

#include <iostream>

namespace xlifepp
{
/*
--------------------------------------------------------------------------------
 complex Exponential Integral E1(z) = \int_z^{\infty} t^{-1} exp(-t) dt
--------------------------------------------------------------------------------
*/
complex_t e1z(const complex_t& z)
{
  complex_t rs;
  real_t zr=z.real(), za=std::abs(z);
  if ( za < 10. || ( zr < 0 && za < 20. ) )
  {
    // Ascending series  E1(z) = -\gamma - log(z) + \sum_{n>0} (-z)^n / n n!
    rs = -theEulerConst - log(z) + ascendingSeriesOfE1(z);
  }
  else
  {
    // Continued fraction
    rs = exp(-z) / continuedFractionOfE1(z);
    if ( zr <= 0 && z.imag() == 0 ) { rs -= pi_ * i_; }
  }
  return rs;
}

/*
--------------------------------------------------------------------------------
 Function Ein(z) = E1(z) + \gamma + log(z) = \sum_{n>0} (-z)^n / n n!
--------------------------------------------------------------------------------
*/
complex_t eInz(const complex_t& z)
{
  complex_t rs;
  real_t zr=z.real(), za=std::abs(z);
  if ( za < 10. || ( zr < 0 && za < 20 ) )
  {
    rs = ascendingSeriesOfE1(z);
  }
  else
  {
    rs = exp(-z) / continuedFractionOfE1(z);
    if ( zr <= 0 && z.imag() == 0 ) { rs -= pi_ * i_; }
    rs += theEulerConst + log(z);
  }
  return rs;
}
/*
--------------------------------------------------------------------------------
 complex_t Integral Function multiplied by exp(z) : exp(z)*E1(z)
--------------------------------------------------------------------------------
 */
complex_t expzE1z(const complex_t& z)
{
  complex_t rs;
  real_t zr=z.real(), za=std::abs(z);
  if ( za < 10. || ( zr < 0 && za < 20 ) )
  {
    rs = (-theEulerConst - log(z) + ascendingSeriesOfE1(z)) * exp(z);
  }
  else
  {
    rs = 1. / continuedFractionOfE1(z);
    if ( zr <= 0 && z.imag() == 0 ) { rs -= pi_ * i_ * exp(z); }
  }
  return rs;
}

complex_t zexpzE1z(const complex_t& z)
{
  complex_t rs;
  real_t zr=z.real(), za=std::abs(z);
  if ( za < theZeroThreshold )
  {
    rs = (-theEulerConst) * z * exp(z);
  }
  else if ( za < 10. || ( zr < 0 && za < 20 ) )
  {
    rs = (-theEulerConst - log(z) + ascendingSeriesOfE1(z)) * z * exp(z);
  }
  else
  {
    rs = z / continuedFractionOfE1(z);
    if ( zr <= 0 && z.imag() == 0 ) { rs -= pi_ * i_ * z * exp(z); }
  }
  return rs;
}

/*
--------------------------------------------------------------------------------
- compute series \sum_{n>0} (-z)^n / n n!
-               = z * \sum_{n>1} (n-1)! (-z)^(n-1) / (n!)^2
--------------------------------------------------------------------------------
*/
complex_t ascendingSeriesOfE1(const complex_t& z)
{
  complex_t rs(complex_t(1., 0.)), tr(rs);
  int k(2);
  while ( std::abs(tr) > theEpsilon * abs(rs) )
  {
    real_t rk(k++);
    tr *= (1 / rk - 1.) * (z / rk);
    rs += tr;
  }
  return z * rs;
}


/*
--------------------------------------------------------------------------------
 compute continued fraction in E1
--------------------------------------------------------------------------------
*/
complex_t continuedFractionOfE1(const complex_t& z)
{
  complex_t cf(0., 0.);
  int k(120);
  while ( k > 0 )
  {
    real_t rk(k--);
    cf = rk / (1. + rk / (z + cf));
  }
  return z + cf;
}

/*
--------------------------------------------------------------------------------
 Test of special functions: comparison with tables in pp. 249-251
 HANDBOOK Of MATHEMATICAL FUNCTIONS , Ed. M.ABRAMOWITZ & I.A. STEGUN
--------------------------------------------------------------------------------
*/
void e1Test(std::ostream& out)
{
  // extra precision with respect to the A_S 6-digit precision
  // results in A-S is obtained with ep = 6.
  int ep(6);
  real_t x1(-19), x(x1), y(0.);
  complex_t ce1;
  int page_number(249);
  out.setf(std::ios::scientific);
  for ( int i = 1; i < 9; i++, x1 += 5 )
  {
    if ( i % 3 == 1 ) out << std::endl << std::endl << " Page " << page_number++ << std::endl
                                  << std::endl << string_t(30, ' ') << "EXPONENTIAL INTEGRAL FOR COMPLEX ARGUMENTS"
                                  << std::endl << string_t(45, ' ') << "Z*EXP(Z)*E1(Z)";
    out << std::endl << "    y / x" << string_t(2 + ep, ' ');
    for ( int j = 0; j < 5; j++ ) { out << std::setw(5) << int(x1 + j) << string_t(12 + 2 * ep, ' '); }
    y = 0.;
    for ( int jy = 0; jy < 21; jy++ , y += 1.)
    {
      //if ( jy > 0 and jy%5 == 0 ) out <<std::endl;
      x = x1;
      out << std::endl << std::setw(5) << int(y);
      for ( int j = 1; j < 6; j++, x += 1. )
      {
        // ce1 = complex_t(x,y) * exp(complex_t(x,y)) * E1z(complex_t(x,y))<<" ";
        ce1 = zexpzE1z(complex_t(x, y));
        out << "  " << std::setw(6 + ep) << std::setprecision(ep) << ce1.real() << "+i*" << std::setw(6 + ep) << std::setprecision(ep) << ce1.imag();
      }
    }
  }
  x1 = -4.;
  out << std::endl << std::endl << string_t(27, ' ') << "EXPONENTIAL INTEGRAL FOR SMALL COMPLEX ARGUMENTS"
            << std::endl << string_t(45, ' ') << "EXP(Z)*E1(Z)";
  out << std::endl << "  y / x" << string_t(4 + ep, ' ');
  for ( int j = 0; j < 5; j++ )
  { out << std::setw(5) << int(x1 + .5 * j) << "." << abs(int(10 * (x1 + .5 * j)) - 10 * int(x1 + .5 * j)) << string_t(12 + 2 * ep, ' '); }
  y = 0.;
  for ( int jy = 0; jy < 6; jy++, y += .2)
  {
    x = x1;
    out << std::endl << int(y) << "." << int(10 * y) - 10 * int(y);
    for ( int j = 1; j < 6; j++, x += .5 )
    {
      // ce1 =  exp(complex_t(x,y)) * E1z(complex_t(x,y))<<" ";
      ce1 = expzE1z(complex_t(x, y));
      out << "  " << std::setw(6 + ep) << std::setprecision(ep) << ce1.real() << "+i*" << std::setw(6 + ep) << std::setprecision(ep) << ce1.imag();
    }
  }
  x1 = -2.;
  out << std::endl << std::endl << string_t(45, ' ') << "E1(Z)+LOG(Z)";
  for ( int i = 1; i < 3; i++ )
  {
    out << std::endl << "  y / x" << string_t(4 + ep, ' ');
    for ( int j = 0; j < 5; j++ )
    { out << std::setw(5) << int(x1 + .5 * j) << "." << abs(int(10 * (x1 + .5 * j)) - 10 * int(x1 + .5 * j)) << string_t(12 + 2 * ep, ' '); }
    y = 0.;
    for ( int jy = 0; jy < 6; jy++, y += .2 )
    {
      x = x1;
      out << std::endl << int(y) << "." << int(10 * y) - 10 * int(y);
      for ( int j = 1; j < 6; j++, x += .5 )
      {
        ce1 = log(complex_t(x, y)) + e1z(complex_t(x, y));
        out << "  " << std::setw(6 + ep) << std::setprecision(ep) << ce1.real() << "+i*" << std::setw(6 + ep) << std::setprecision(ep) << ce1.imag();
      }
    }
    x1 = 0.5;
  }
}

} // end of namespace xlifepp
