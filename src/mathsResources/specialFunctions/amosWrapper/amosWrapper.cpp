/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file amosWrapper.cpp
  \author N. Kielbasiewicz
  \since 5 mar 2018
  \date 5 mar 2018

  \brief Implementation of wrapper functions to amos library
 */

#include "amosWrapper.hpp"
#include "../../../utils/Messages.hpp"

namespace xlifepp
{

/*
-------------------------------------------------------------------------------
.Chebyshev polynomials (of the first kind) on [-1, 1] up to order n
.  Tn = cos( n acos(x))
. Recurrence relation:
.   T_0(x) = 1, T_1(x) = x
.   T_n(x) = 2 x T_{n-1}(x) - T_{n-1}(x) , n > 1
-------------------------------------------------------------------------------
*/
std::complex<double> zbesselJ(const std::complex<double>& z, double order)
{
  #ifdef XLIFEPP_WITH_AMOS
    // Input values for Fortran subroutines.
    double zr = z.real();
    double zi = z.imag();
    double nu = std::abs(order);
    int kode = 1;
    int N = 1;

    // Output values.
    double cyr,cyi;
    int nz,ierr;

    // External function call
    FTNAME(zbesj)(&zr, &zi, &nu, &kode, &N, &cyr, &cyi, &nz, &ierr);    // Call Fortran subroutine.

    // If the input is real, then the output should be real as well.
    if (zi == 0.0 && zr >= 0.0) cyi = 0.0;
    std::complex<double> res(cyr, cyi);               // Placeholder for output.

    // If order is negative, then we must apply the reflection formula.
    if (order < 0.0)
    {
      // We prepare the rotation coefficients.
      double c = cos(pi_*nu);
      // test if nu is an integer and a half (so that cos(pi*nu)=0.0)
      double nuPlusHalf = nu + 0.5;
      if (std::floor(nuPlusHalf) == nuPlusHalf && std::abs(nu) < 1e14) c=0.0;
      double s = sin(pi_*nu);
      // test if nu is an integer (so that sin(pi*nu)=0.0)
      if (std::floor(nu) == nu && std::abs(nu) < 1e14) s=0.0;

      // We compute the Bessel Y function.
      double cyrY, cyiY, cwrkr, cwrki;
      int nzY, ierrY, kodeY(1), NY(1);

      // External function call
      FTNAME(zbesy)(&zr, &zi, &nu, &kodeY, &NY, &cyrY, &cyiY, &nzY, &cwrkr, &cwrki, &ierrY);
      std::complex<double> resY(cyrY,cyiY);
      res = c*res - s*resY;
    }

    // If the return code is not normal, we print the error code.
    if (ierr!=0) std::cout << "besselJ: Error code " << ierr << "." << std::endl;

    return res;
  #else
    error("xlifepp_without_amos",__func__);
    return std::complex<double>(); // dummy return
  #endif // XLIFEPP_WITH_AMOS
}

std::complex<double> zbesselY(const std::complex<double>& z, double order)
{
  #ifdef XLIFEPP_WITH_AMOS
    // Input values for Fortran subroutines
    double zr = z.real();
    double zi = z.imag();
    double nu = std::abs(order);
    int kode = 1;
    int N = 1;

    // Output and temporary varibles
    double cyr, cyi,cwrkr,cwrki;
    int nz, ierr;

    // External function call
    FTNAME(zbesy)(&zr, &zi, &nu, &kode, &N, &cyr, &cyi, &nz, &cwrkr, &cwrki, &ierr); // Call Fortran subroutine.

    // In passing from C++ to FORTRAN, the exact zero becomes the numerical zero (10^(-14)).
    // The limiting form of Y_nu(z) for high order, -Gamma(nu)/pi*Re(z)^(-nu)*(1-i*nu*Im(z)/Re(z)),
    // leads to product of the form zero*infinity, which destroys numerical precision. We hence
    // manually set the imaginary part of the answer to zero is the imaginary part of the input
    // is zero.
    if (zi == 0.0 && zr >= 0.0) cyi=0.0;
    std::complex<double> res(cyr,cyi);                           // Placeholder for output

    // If order is negative, we must apply the reflection formula.
    if (order < 0.0)
    {
      // We prepare the rotation coefficients.
      double c = cos(pi_*nu);
      // test if nu is an integer and a half (so that cos(pi*nu)=0.0)
      double nuPlusHalf = nu + 0.5;
      if (std::floor(nuPlusHalf) == nuPlusHalf && std::abs(nu) < 1e14) c=0.0;
      double s = sin(pi_*nu);
      // test if nu is an integer (so that sin(pi*nu)=0.0)
      if (std::floor(nu) == nu && std::abs(nu) < 1e14) s=0.0;

      // We compute the Bessel J function.
      double cyrJ,cyiJ;
      int nzJ, ierrJ, kodeJ(1), NJ(1);

      FTNAME(zbesj)(&zr, &zi, &nu, &kodeJ, &NJ, &cyrJ, &cyiJ, &nzJ, &ierrJ);
      std::complex<double> resJ(cyrJ,cyiJ);
      res = s*resJ + c*res;
    }

    // If the return code is not normal, we print the error code.
    if (ierr!=0) std::cout << "besselY: Error code " << ierr << "." << std::endl;

    return res;
  #else
    error("xlifepp_without_amos",__func__);
    return std::complex<double>(); // dummy return
  #endif // XLIFEPP_WITH_AMOS
}

std::complex<double> zbesselI(const std::complex<double>& z, double order)
{
  #ifdef XLIFEPP_WITH_AMOS
    // Input values for Fortran subroutines.
    double zr = z.real();
    double zi = z.imag();
    double nu = std::abs(order);
    int kode = 1;
    int N = 1;

    // Output and temporary variables.
    double cyr,cyi;
    int nz, ierr;

    // External function call.
    FTNAME(zbesi)(&zr, &zi, &nu, &kode, &N, &cyr, &cyi, &nz, &ierr); // Call Fortran subroutine.

    // Enforcing some conditions on the output as a function of the output.
    if (zi == 0.0 && zr >= 0.0) cyi = 0.0;
    std::complex<double> res(cyr,cyi);

    // We apply the reflection formula is order is negative.
    if (order < 0.0)
    {
      // We prepare the reflection coefficients.
      double s = sin(pi_*nu);
      // test if nu is an integer (so that sin(pi*nu)=0.0)
      if (std::floor(nu) == nu && std::abs(nu) < 1e14) s=0.0;

      // We evaluate the besselK function.
      int nzK, ierrK;
      double cyrK, cyiK;

      // External function call.
      FTNAME(zbesk)(&zr, &zi, &nu, &kode, &N, &cyrK, &cyiK, &nzK, &ierrK);

      if (zi == 0.0 && zr >= 0.0) cyiK = 0.0;
      std::complex<double> resK(cyrK,cyiK);
      res += 2.0/pi_*s*resK;
    }

    // In case of error, we print the error code.
    if (ierr!=0) std::cout << "besselI: Error code " << ierr << "." << std::endl;

    return res;
  #else
    error("xlifepp_without_amos",__func__);
    return std::complex<double>(); // dummy return
  #endif // XLIFEPP_WITH_AMOS
}

std::complex<double> zbesselK(const std::complex<double>& z, double order)
{
  #ifdef XLIFEPP_WITH_AMOS
    // Input values for Fortran subroutines.
    double zr = z.real();
    double zi = z.imag();
    double nu = std::abs(order);
    int kode = 1;
    int N = 1;

    // Output and temporary variables.
    double cyr, cyi;
    int nz, ierr;

    // External function call.
    FTNAME(zbesk)(&zr, &zi, &nu, &kode, &N, &cyr, &cyi, &nz, &ierr); // Call Fortran subroutine.

    // In passing from C++ to FORTRAN, the exact zero becomes the numerical zero (10^(-14)).
    // The limiting form of K_nu(z) for high order, Gamma(nu)/2*(z/2)^(-nu),
    // leads to product of the form zero*infinity for the imaginary part, which destroys numerical precision. We hence
    // manually set the imaginary part of the answer to zero is the imaginary part of the input
    // is zero.
    if (zi == 0.0 && zr >= 0.0) cyi = 0.0;
    std::complex<double> res(cyr,cyi);

    // In case of error, we print the error code.
    if (ierr!=0) std::cout << "besselK: Error code " << ierr << "." << std::endl;

    return res;
  #else
    error("xlifepp_without_amos",__func__);
    return std::complex<double>(); // dummy return
  #endif // XLIFEPP_WITH_AMOS
}

std::complex<double> zhankel(const std::complex<double>& z, int kind, double order)
{
  #ifdef XLIFEPP_WITH_AMOS
    // Input values.
    double zr = z.real();
    double zi = z.imag();
    double nu = std::abs(order);
    int kode = 1;
    int N = 1;

    // Output values
    double cyr,cyi;
    int nz,ierr;

    // External function call.
    FTNAME(zbesh)(&zr, &zi, &nu, &kode, &kind, &N, &cyr, &cyi, &nz, &ierr);
    std::complex<double> res(cyr,cyi);

    // Reflection formula if order is negative.
    if (order < 0.0)
    {
      double phase = 1.;
      if (kind == 2) phase = -1.;
      res *= std::exp(phase*i_*pi_*nu);
    }

    return res;
  #else
    error("xlifepp_without_amos",__func__);
    return std::complex<double>(); // dummy return
  #endif // XLIFEPP_WITH_AMOS
}

std::complex<double> zairy(const std::complex<double>& z, int id)
{
  #ifdef XLIFEPP_WITH_AMOS
    // Input values.
    double zr = z.real();
    double zi = z.imag();
    int kode = 1;

    // Output values.
    double air, aii;
    int nz, ierr=0;

    // External function call.
    FTNAME(zairy)(&zr, &zi, &id, &kode, &air, &aii, &nz, &ierr);
    if(ierr!=0) airyError(ierr,z,"A");
    return std::complex<double>(air,aii);
  #else
    error("xlifepp_without_amos",__func__);
    return std::complex<double>(); // dummy return
  #endif // XLIFEPP_WITH_AMOS
}

std::complex<double> zbiry(const std::complex<double>& z, int id)
{
  #ifdef XLIFEPP_WITH_AMOS
    // Input values.
    double zr = z.real();
    double zi = z.imag();
    int kode = 1;

    // Output values.
    double bir, bii;
    int nz, ierr;

    // External function call.
    FTNAME(zbiry)(&zr, &zi, &id, &kode, &bir, &bii, &nz, &ierr);
    if(ierr!=0) airyError(ierr,z,"Ba");
    return std::complex<double>(bir,bii);
  #else
    error("xlifepp_without_amos",__func__);
    return std::complex<double>(); // dummy return
  #endif // XLIFEPP_WITH_AMOS
}

void airyError(int err, const complex_t& z, const string_t& pr)
{
    switch (err)
    {
        case 0: return;
        case 1: error("free_error"," abnormal call of "+pr+"iry function, no computation"); return;
        case 2: error("free_error"," overflow in "+pr+"iry function (z="+tostring(z)+")"); return;
        case 3: warning("free_warning"," |z| large in "+pr+"iry function, loss of precision (z="+tostring(z)+")"); return;
        case 4: error("free_error"," |z| too large in "+pr+"iry function, no computation (z="+tostring(z)+")"); return;
        case 5: error("free_error"," in "+pr+"iry function, algorithm cannot stop properly (z="+tostring(z)+")"); return;
        default: error("free_error"," unknown error code in "+pr+"iry function (z="+tostring(z)+")"); return;
    }
}

} // end of namespace xlifepp
