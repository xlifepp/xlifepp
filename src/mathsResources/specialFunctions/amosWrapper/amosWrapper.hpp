/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file amosWrapper.hpp
  \author N. Kielbasiewicz
  \since 5 mar 2018
  \date 5 mar 2018

  \brief Declaration of wrapper functions to amos library

  The amos library is the reference library to use complex Bessel functions
*/

#ifndef AMOS_WRAPPER_HPP
#define AMOS_WRAPPER_HPP

#include "config.h"

namespace xlifepp {

#ifdef XLIFEPP_WITH_AMOS
  extern "C" {
    //@{
    //! fortran routines of amos library
    void FTNAME(cairy)(std::complex<float>* z, int* id, int* kode, std::complex<float>* ai, int* nz, int* ierr);
    void FTNAME(cbesh)(std::complex<float>* z, float* nu, int* kode, int* M, int* N, std::complex<float>* cy, int* nz, int* ierr);
    void FTNAME(cbesi)(std::complex<float>* z, float* nu, int* kode, int* N, std::complex<float>* cy, int* nz, int* ierr);
    void FTNAME(cbesj)(std::complex<float>* z, float* nu, int* kode, int* N, std::complex<float>* cy, int* nz, int* ierr);
    void FTNAME(cbesk)(std::complex<float>* z, float* nu, int* kode, int* N, std::complex<float>* cy, int* nz, int* ierr);
    void FTNAME(cbesy)(std::complex<float>* z, float* nu, int* kode, int* N, std::complex<float>* cy, int* nz,
                      std::complex<float>* cwrk, int* ierr);
    void FTNAME(cbiry)(std::complex<float>* z, int* id, int* kode, std::complex<float>* bi, int* ierr);
    void FTNAME(zairy)(double* zr, double* zi, int* id, int* kode, double* air, double* aii, int* nz, int* ierr);
    void FTNAME(zbesh)(double* zr, double* zi, double* nu, int* kode, int* M, int* N, double* cyr, double* cyi, int* nz, int* ierr);
    void FTNAME(zbesi)(double* zr, double* zi, double* nu, int* kode, int* N, double* cyr, double* cyi, int* nz, int* ierr);
    void FTNAME(zbesj)(double* zr, double* zi, double* nu, int* kode, int* N, double* cyr, double* cyi, int* nz, int* ierr);
    void FTNAME(zbesk)(double* zr, double* zi, double* nu, int* kode, int* N, double* cyr, double* cyi, int* nz, int* ierr);
    void FTNAME(zbesy)(double* zr, double* zi, double* nu, int* kode, int* N, double* cyr, double* cyi, int* nz,
                      double* cwrkr, double* cwrki, int* ierr);
    void FTNAME(zbiry)(double* zr, double* zi, int* id, int* kode, double* bir, double* bii, int* nz, int* ierr);
    //@}
  }
#endif // XLIFEPP_WITH_AMOS

//--------------------------------------------------------------------------------
// Bessel functions
//--------------------------------------------------------------------------------
std::complex<double> zbesselJ(const std::complex<double>& z, double order); //!< Bessel function of the first kind: J_N(z)
std::complex<double> zbesselY(const std::complex<double>& z, double order); //!< Bessel function of the second kind: Y_N(z)
std::complex<double> zbesselI(const std::complex<double>& z, double order); //!< Modified Bessel function of the first kind: I_N(z)
std::complex<double> zbesselK(const std::complex<double>& z, double order); //!< Modified Bessel function of the second kind: K_N(z)
std::complex<double> zhankel(const std::complex<double>& z, int kind, double order); //!< Hankel functions of the i-th kind: Hi_N(z)
std::complex<double> zairy(const std::complex<double>& z, int id); //!< Airy function: Ai(z)
std::complex<double> zbiry(const std::complex<double>& z, int id); //!< Biry function: Bi(z)
void airyError(int err, const complex_t &z, const string_t& pr="A");

} // end of namespace xlifepp

#endif /* AMOS_WRAPPER_HPP */
