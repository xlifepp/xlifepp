/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file specialFunctions.hpp
  \author D. Martin
  \since 10 jan 2003
  \date 9 apr 2007

  \brief Definition of Bessel, Hankel and Struve functions

  -- Bessel functions
  -     HANDBOOK Of MATHEMATICAL FUNCTIONS, Ed. M.ABRAMOWITZ & I.A. STEGUN
  -     Chap. 9 : Bessel Functions of Integer Order (p.355-456)
  -      http://www.math.sfu.ca/~cbm/aands/page_355.htm
  -
    bessel_J0   Bessel function of the first kind and order 0           J_0(x)
    bessel_J1   Bessel function of the first kind and order 1           J_1(x)
    bessel_J0N  Bessel functions of the first kind and orders 0..N      J_0(x)..J_N(x)
    bessel_Y0   Bessel function of the second kind and order 0          Y_0(x)
    bessel_Y1   Bessel function of the second kind and order 1          Y_1(x)
    bessel_Y0N  Bessel functions of the second kind and orders 0..N     Y_0(x)..Y_N(x)
    hankel_H10  Hankel function of order 0 (J_0(x) + i * Y_0(x))        H_0^{1}(x)
    hankel_H11  Hankel function of order 1 (J_1(x) + i * Y_1(x))        H_1^{1}(x)
   *bessel_I0   Modified Bessel function of the first kind and order 0  I_0(x)
   *bessel_I1   Modified Bessel function of the first kind and order 1  I_1(x)
   *bessel_K0   Modified Bessel function of the second kind and order 0 K_0(x)
   *bessel_K1   Modified Bessel function of the second kind and order 1 K_1(x)

  -- Spherical Bessel functions
  -     HANDBOOK Of MATHEMATICAL FUNCTIONS, Ed. M.ABRAMOWITZ & I.A. STEGUN
  -     Chap. 10 : Bessel Functions of Fractional Order (p.437-494)
  -      http://www.math.sfu.ca/~cbm/aands/page_437.htm
  -
    sphericalbessel_j0N   Spherical Bessel functions of the first kind and orders 0..N      j_0(x)..j_N(x)
    sphericalbessel_y0N   Spherical Bessel functions of the second kind and orders 0..N     y_0(x)..y_N(x)

  -- Struve functions
  -     HANDBOOK Of MATHEMATICAL FUNCTIONS , Ed. M.ABRAMOWITZ & I.A. STEGUN
  -     Chap. 12 : Struve Functions and Related Functions (p.495-502)
  -      http://www.math.sfu.ca/~cbm/aands/page_495.htm
  -     MATHEMATICAL FUNCTIONS AND THEIR APPROXIMATIONS, Yudell L.LUKE
  -     Chap. X: Lommel and Struve functions (pp. 416--..)
  -
    struveNot_H0    Struve function of order 0                              H_0(x)
    struveNot_H1    Struve function of order 1                              H_1(x)
    struveNot_H01   Nearly a Struve function
                          returning H_0(x) and H_1(x) for |x| < 8 H_0(x),H_1(x)
                          returning H_0(x)-Y_0(x) and H_1(x)-Y_1(x) for |x| > 8
*/

#ifndef SPECIAL_FUNCTIONS_HPP
#define SPECIAL_FUNCTIONS_HPP

#include "config.h"
#include "amosWrapper/amosWrapper.hpp"

namespace xlifepp
{

//--------------------------------------------------------------------------------
// Bessel functions
//--------------------------------------------------------------------------------
real_t besselJ0(real_t x);                    //!< Bessel function of the first kind and order 0 : J_0(x)
inline complex_t besselJ0(const complex_t& z) //! Bessel function of the first kind and order 0 : J_0(z) (complex case)
{ return zbesselJ(z, 0); }

real_t besselJ1(real_t x);                    //!< Bessel function of the first kind and order 1 : J_1(x)
inline complex_t besselJ1(const complex_t& z) //! Bessel function of the first kind and order 1 : J_1(z) (complex case)
{ return zbesselJ(z, 1); }

std::vector<real_t> besselJ0N(real_t x, number_t N);   //!< Bessel functions of the first kind and order 0..N

template <class T_>// For signed or unsigned integral types
real_t besselJ(real_t x, T_ N) { //! Bessel function of the first kind and order N: J_N(x)
  int mN = 1;
  if (N < 0) { N = -N;   mN = 1 - 2*(N%2); } // J_{-n} = (-1)^n J_{n}
  real_t res;
  switch (N) {
    case  0: res = besselJ0(x); break;
    case  1: res = besselJ1(x); break;
    default: res = besselJ0N(x, N)[N]; break;
  }
  return mN * res;
}
template <>
inline real_t besselJ(real_t x, real_t N) //! Bessel function of the first kind and real order N: J_N(x)
{ return zbesselJ(x, N).real(); }

inline complex_t besselJ(const complex_t& z, real_t N) //! Bessel function of the first kind and order N: J_N(z) (complex case)
{ return zbesselJ(z, N); }

//- - - - - - - - - - - - - - - - - - - - - - -
real_t besselY0(real_t x);                    //!< Bessel function of the second kind and order 0 : Y_0(x)
inline complex_t besselY0(const complex_t& z) //! Bessel function of the second kind and order 0 : Y_0(z) (complex case)
{ return zbesselY(z, 0); }

real_t besselY1(real_t x);                    //!< Bessel function of the second kind and order 0 : Y_1(x)
inline complex_t besselY1(const complex_t& z) //! Bessel function of the second kind and order 0 : Y_1(z) (complex case)
{ return zbesselY(z, 1); }

std::vector<real_t> besselY0N(real_t x, number_t N); //!< Bessel functions of the second kind and order 0..N

template <class T_>// For signed or unsigned integral types
real_t besselY(real_t x, T_ N) { //! Bessel function of the second kind and order N: Y_N(x)
  int mN = 1;
  if (N < 0) { N = -N;   mN = 1 - 2*(N%2); } // Y_{-n} = (-1)^n Y_{n}
  real_t res;
  switch (N) {
    case  0: res = besselY0(x); break;
    case  1: res = besselY1(x); break;
    default: res = besselY0N(x, N)[N]; break;
  }
  return mN * res;
}
template <>
inline real_t besselY(real_t x, real_t N) //! Bessel function of the second kind and real order N: Y_N(x)
{ return zbesselY(x, N).real(); }

inline complex_t besselY(const complex_t& z, real_t N) //! Bessel function of the second kind and order N: Y_N(z) (complex case)
{ return zbesselY(z, N); }

//- - - - - - - - - - - - - - - - - - - - - - -
real_t besselI0(real_t x);                    //!< Modified Bessel function of the first kind and order 0 : I_0(x)
inline complex_t besselI0(const complex_t& z) //! Modified Bessel function of the first kind and order 0 : I_0(z) (complex case)
{ return zbesselI(z, 0); }

real_t besselI1(real_t x);                    //!< Modified Bessel function of the first kind and order 1 : I_1(x)
inline complex_t besselI1(const complex_t& z) //! Modified Bessel function of the first kind and order 1 : I_1(z) (complex case)
{ return zbesselI(z, 1); }

std::vector<real_t> besselI0N(real_t x, number_t n); //!< Modified Bessel functions of the first kind and order 0..N

#ifdef XLIFEPP_WITH_AMOS
  // Use Amos library for BesselI and BesselK functions of integral order instead of
  // internal procedures since those ones provide results with limited precision.
  template <class T_>// For signed or unsigned integral types
  inline real_t besselI(real_t x, T_ N) //! Modified Bessel function of the first kind and order N: I_N(x)
  { return zbesselI(x, N).real(); }
  #else
  // Use internal procedures when Amos is not available, but accuracy is only single precision.
  template <class T_>// For signed or unsigned integral types
  real_t besselI(real_t x, T_ N) { //! Modified Bessel function of the first kind and order N: I_N(x)
    if (N < 0) { N = -N; } // I_{-n} = I_{n}
    real_t res;
    switch (N) {
      case  0: res = besselI0(x); break;
      case  1: res = besselI1(x); break;
      default: res = besselI0N(x, N)[N]; break;
    }
    return res;
  }
#endif // XLIFEPP_WITH_AMOS
template <>
inline real_t besselI(real_t x, real_t N) //! Modified Bessel function of the first kind and real order N: I_N(x)
{ return zbesselI(x, N).real(); }

inline complex_t besselI(const complex_t& z, real_t N) //! Modified Bessel function of the first kind and order N: I_N(z) (complex case)
{ return zbesselI(z, N); }

//- - - - - - - - - - - - - - - - - - - - - - -
real_t besselK0(real_t x);                    //!< Modified Bessel function of the second kind and order 0 : K_0(x)
inline complex_t besselK0(const complex_t& z) //! Modified Bessel function of the second kind and order 0 : K_0(z) (complex case)
{ return zbesselK(z, 0); }

real_t besselK1(real_t x);                    //!< Modified Bessel function of the second kind and order 1 : K_1(x)
inline complex_t besselK1(const complex_t& z) //! Modified Bessel function of the second kind and order 1 : K_1(z) (complex case)
{ return zbesselK(z, 1); }

std::vector<real_t> besselK0N(real_t x, number_t n); //!< Modified Bessel functions of the second kind and order 0..N

  #ifdef XLIFEPP_WITH_AMOS
  // Use Amos library for BesselI and BesselK functions of integral order instead of
  // internal procedures since those ones provide results with limited precision.
  template <class T_>// For signed or unsigned integral types
  inline real_t besselK(real_t x, T_ N) //! Modified Bessel function of the first kind and order N: I_N(x)
  { return zbesselK(x, N).real(); }
#else
  // Use internal procedures when Amos is not available, but accuracy is only single precision.
  template <class T_>// For signed or unsigned integral types
  real_t besselK(real_t x, T_ N) { //! Modified Bessel function of the second kind and order N: K_N(x)
    if (N < 0) { N = -N; } // K_{-n} = K_{n}
    real_t res;
    switch (N) {
      case  0: res = besselK0(x); break;
      case  1: res = besselK1(x); break;
      default: res = besselK0N(x, N)[N]; break;
    }
    return res;
  }
#endif // XLIFEPP_WITH_AMOS
template <>
inline real_t besselK(real_t x, real_t N) //! Modified Bessel function of the second kind and real order N: K_N(x)
{ return zbesselK(x, N).real(); }

inline complex_t besselK(const complex_t& z, real_t N) //! Modified Bessel function of the second kind and order N: K_N(z) (complex case)
{ return zbesselK(z, N); }

//- - - - - - - - - - - - - - - - - - - - - - -
complex_t hankelH10(real_t x);                 //!< Hankel function of the first kind and order 0 : H1_0(x) = J_0(x) + i Y_0(x)
inline complex_t hankelH10(const complex_t& z) //! Hankel function of the first kind and order 0 : H1_0(z) = J_0(z) + i Y_0(z) (complex case)
{ return zhankel(z, 1, 0); }

complex_t hankelH11(real_t x);                 //!< Hankel function of the first kind and order 1 : H1_1(x) = J_1(x) + i Y_1(x)
inline complex_t hankelH11(const complex_t& z) //! Hankel function of the first kind and order 1 : H1_1(z) = J_1(z) + i Y_1(z) (complex case)
{ return zhankel(z, 1, 1); }

std::vector<complex_t> hankelH10N(real_t x, number_t N); //!< Hankel functions of the first kind and order 0 ... N: H1_k(x) = J_k(x) + i Y_k(x)

template <class T_>// For signed or unsigned integral types
complex_t hankelH1(real_t x, T_ N) { //! Hankel function of the first kind and order N: H1_N(x) = J_N(x) + i Y_N(x)
  int mN = 1;
  if (N < 0) { N = -N;   mN = 1 - 2*(N%2); } // H1_{-n} = (-1)^n H1_{n}
  complex_t res;
  switch (N) {
    case  0: res = hankelH10(x); break;
    case  1: res = hankelH11(x); break;
    default: res = hankelH10N(x, N)[N]; break;
  }
  return real_t(mN) * res;
}
template <>
inline complex_t hankelH1(real_t x, real_t N) //! Hankel function of the first kind and real order N: H1_N(x) = J_N(x) + i Y_N(x)
{ return zhankel(complex_t(x, 0.), 1, N); }
inline complex_t hankelH1(const complex_t& z, real_t N) //! Hankel function of the first kind and order N: H1_N(z) = J_N(z) + i Y_N(z) (complex case)
{ return zhankel(z, 1, N); }

//- - - - - - - - - - - - - - - - - - - - - - -
inline complex_t hankelH20(real_t x)           //! Hankel function of the second kind and order 0 : H2_0(x) = J_0(x) - i Y_0(x)
{ return complex_t(besselJ0(x), -besselY0(x)); }//{ return zhankel(complex_t(x, 0.), 2, 0); }
inline complex_t hankelH20(const complex_t& z) //! Hankel function of the second kind and order 0 : H2_0(z)  = J_0(z) + i Y_0(z) (complex case)
{ return zhankel(z, 2, 0); }

inline complex_t hankelH21(real_t x)           //! Hankel function of the second kind and order 1 : H2_1(x) = J_1(x) - i Y_1(x)
{ return complex_t(besselJ1(x), -besselY1(x)); }//{ return zhankel(complex_t(x, 0.), 2, 1); }
inline complex_t hankelH21(const complex_t& z) //! Hankel function of the second kind and order 1 : H2_1(z) = J_1(z) - i Y_1(z) (complex case)
{ return zhankel(z, 2, 1); }

std::vector<complex_t> hankelH20N(real_t x,number_t N); //!< Hankel functions of the second kind and order 0 ... N: H2_k(x) = J_k(x) - i Y_k(x)

template <class T_>// For signed or unsigned integral types
complex_t hankelH2(real_t x, T_ N) { //! Hankel function of the second kind and order N: H2_N(x) = J_N(x) - i Y_N(x)
  int mN = 1;
  if (N < 0) { N = -N;   mN = 1 - 2*(N%2); } // H2_{-n} = (-1)^n H2_{n}
  complex_t res;
  switch (N) {
    case  0: res = hankelH20(x); break;
    case  1: res = hankelH21(x); break;
    default: res = complex_t(besselJ0N(x, N)[N], -besselY0N(x, N)[N]); break; // hankelH20N(x, N)[N];
  }
  return real_t(mN) * res;
}
template <>
inline complex_t hankelH2(real_t x, real_t N) //! Hankel function of the second kind and real order N: H2_N(x) = J_N(x) - i Y_N(x)
{ return zhankel(complex_t(x, 0.), 2, N); }
inline complex_t hankelH2(const complex_t& z, real_t N) //! Hankel function of the second kind and order N: H2_N(z) = J_N(z) - i Y_N(z) (complex case)
{ return zhankel(z, 2, N); }

//- - - - - - - - - - - - - - - - - - - - - - -
inline complex_t airy(real_t x, DiffOpType d=_id)           //! Airy function: Ai(x) or Ai'(x)
{ if(d==_id) return zairy(complex_t(x, 0.), 0); else return zairy(complex_t(x, 0.), 1); }
inline complex_t airy(const complex_t& z, DiffOpType d=_id) //! Airy function: Ai(z) or Ai'(z)
{ if(d==_id) return zairy(z, 0); else return zairy(z, 1); }

inline complex_t biry(real_t x, DiffOpType d=_id)           //! Airy function: Bi(x) or Bi'(x)
{ if(d==_id) return zbiry(complex_t(x, 0.), 0); else return zbiry(complex_t(x, 0.), 1); }
inline complex_t zbiry(const complex_t& z, DiffOpType d=_id)//! Airy function: Bi(z) or Bi'(z)
{ if(d==_id) return zbiry(z, 0); else return zbiry(z, 1); }

/*
--------------------------------------------------------------------------------
   Struve functions
--------------------------------------------------------------------------------
*/

/*! Struve function of order 0, H_0(x)
    returns H_0(x) for any |x|<=8, H_0(x)-Y_0(x) for any |x|>8.
*/
real_t struveNotH0(real_t x);

/*! Struve function of order 1, H_1(x)
    returns H_1(x) for any |x|<=‡8, H_1(x)-Y_1(x) for any |x|>8.
*/
real_t struveNotH1(real_t x);

/*! returns both H_0(x) and H_1(x) for any |x|<8,
    returns H_0(x)-Y_0(x) and H_1(x)-Y_1(x) for any |x|>8.
*/
std::pair<real_t, real_t> struveNotH01(real_t x);

/*
--------------------------------------------------------------------------------
   spherical Bessel functions
--------------------------------------------------------------------------------
*/
//! spherical Bessel function of the first kind and orders 0..N
std::vector<real_t> sphericalbesselJ0N(real_t x, number_t N);

//! spherical Bessel function of the second kind and orders 0..N
std::vector<real_t> sphericalbesselY0N(real_t x, number_t N);

/*
--------------------------------------------------------------------------------
   For tests
--------------------------------------------------------------------------------
*/
void besselJY01Test(std::ostream&); // compare with Table 9.1 of Abramowitz & Stegun
/*
 390                            BESSEL FUNCTIONS OF INTEGER ORDER                            391

 Table 9.1                   BESSEL FUNCTIONS--ORDER 0, 1 AND 2

 x   J0(x)               J1(x)                  x   Y0(x)               Y1(x)

 0.0  1.000000000000e+00  0.000000000000e+00    0.0 -inf                -inf
 0.1  9.975015620660e-01  4.993752603624e-02    0.1 -1.534238651350e+00 -6.458951094702e+00
 0.2  9.900249722396e-01  9.950083263924e-02    0.2 -1.081105322372e+00 -3.323824988112e+00
 0.3  9.776262465383e-01  1.483188162731e-01    0.3 -8.072735778045e-01 -2.293105138389e+00
 0.4  9.603982266596e-01  1.960265779553e-01    0.4 -6.060245684270e-01 -1.780872044270e+00
 0.5  9.384698072408e-01  2.422684576749e-01    0.5 -4.445187335067e-01 -1.471472392670e+00
 0.6  9.120048634972e-01  2.867009880639e-01    0.6 -3.085098701156e-01 -1.260391347177e+00
 0.7  8.812008886074e-01  3.289957415401e-01    0.7 -1.906649293374e-01 -1.103249871908e+00
 0.8  8.462873527505e-01  3.688420460942e-01    0.8 -8.680227965661e-02 -9.781441766834e-01
 0.9  8.075237981225e-01  4.059495460788e-01    0.9  5.628306635205e-03 -8.731265824563e-01
 1.0  7.651976865580e-01  4.400505857449e-01    1.0  8.825696421568e-02 -7.812128213003e-01
 1.1  7.196220185275e-01  4.709023948663e-01    1.1  1.621632029269e-01 -6.981195600677e-01
 1.2  6.711327442644e-01  4.982890575672e-01    1.2  2.280835032272e-01 -6.211363797488e-01
 1.3  6.200859895615e-01  5.220232474147e-01    1.3  2.865353571656e-01 -5.485197299808e-01
 1.4  5.668551203743e-01  5.419477139309e-01    1.4  3.378951296797e-01 -4.791469742328e-01
 1.5  5.118276717359e-01  5.579365079101e-01    1.5  3.824489237978e-01 -4.123086269739e-01
 1.6  4.554021676394e-01  5.698959352617e-01    1.6  4.204268964157e-01 -3.475780082651e-01
 1.7  3.979848594461e-01  5.777652315290e-01    1.7  4.520270001816e-01 -2.847262450641e-01
 1.8  3.399864110426e-01  5.815169517312e-01    1.8  4.774317149005e-01 -2.236648681735e-01
 1.9  2.818185593744e-01  5.811570727134e-01    1.9  4.968199712838e-01 -1.644057723316e-01
 2.0  2.238907791412e-01  5.767248077569e-01    2.0  5.103756726497e-01 -1.070324315409e-01
 2.1  1.666069803320e-01  5.682921357570e-01    2.1  5.182937375138e-01 -5.167861213042e-02
 2.2  1.103622669222e-01  5.559630498191e-01    2.2  5.207842853880e-01  1.487789289764e-03
 2.3  5.553978444560e-02  5.398725326043e-01    2.3  5.180753962076e-01  5.227731584423e-02
 2.4  2.507683297243e-03  5.201852681819e-01    2.4  5.104147486657e-01  1.004889383311e-01
 2.5 -4.838377646820e-02  4.970941024643e-01    2.5  4.980703596152e-01  1.459181379668e-01
 2.6 -9.680495439704e-02  4.708182665176e-01    2.6  4.813305905870e-01  1.883635443502e-01
 2.7 -1.424493700460e-01  4.416013791183e-01    2.7  4.605035490754e-01  2.276324458709e-01
 2.8 -1.850360333644e-01  4.097092468523e-01    2.8  4.359159856397e-01  2.635453936351e-01
 2.9 -2.243115457920e-01  3.754274818131e-01    2.9  4.079117692362e-01  2.959400546077e-01
 3.0 -2.600519549019e-01  3.390589585259e-01    3.0  3.768500100128e-01  3.246744247918e-01
 3.1 -2.920643476507e-01  3.009211331011e-01    3.1  3.431028893545e-01  3.496294822677e-01
 3.2 -3.201881696571e-01  2.613432487805e-01    3.2  3.070532501324e-01  3.707113384413e-01
 3.3 -3.442962603989e-01  2.206634529852e-01    3.3  2.690919950545e-01  3.878529310237e-01
 3.4 -3.642955967620e-01  1.792258516815e-01    3.4  2.296153372096e-01  4.010152921085e-01
 3.5 -3.801277399873e-01  1.373775273623e-01    3.5  1.890219439208e-01  4.101884178875e-01
 3.6 -3.917689837008e-01  9.546554717788e-02    3.6  1.477100126112e-01  4.153917621114e-01
 3.7 -3.992302033712e-01  5.383398774546e-02    3.7  1.060743153204e-01  4.166743726838e-01
 3.8 -4.025564101786e-01  1.282100292673e-02    3.8  6.450324665499e-02  4.141146893078e-01
 3.9 -4.018260148876e-01 -2.724403962078e-02    3.9  2.337590819872e-02  4.078200195265e-01
 4.0 -3.971498098638e-01 -6.604332802355e-02    4.0 -1.694073932507e-02  3.979257105571e-01
 4.1 -3.886696798359e-01 -1.032732577473e-01    4.1 -5.609462660635e-02  3.845940348189e-01
 4.2 -3.765570543676e-01 -1.386469421260e-01    4.2 -9.375120131443e-02  3.680128078542e-01
 4.3 -3.610111172365e-01 -1.718965602215e-01    4.3 -1.295959028909e-01  3.483937583140e-01
 4.4 -3.422567900039e-01 -2.027755219231e-01    4.4 -1.633364628042e-01  3.259706707535e-01
 4.5 -3.205425089851e-01 -2.310604319234e-01    4.5 -1.947050086295e-01  3.009973230697e-01
 4.6 -2.961378165741e-01 -2.565528360974e-01    4.6 -2.234599525536e-01  2.737452414709e-01
 4.7 -2.693307894198e-01 -2.790807358433e-01    4.7 -2.493876472450e-01  2.445012968471e-01
 4.8 -2.404253272912e-01 -2.984998580996e-01    4.8 -2.723037944566e-01  2.135651672662e-01
 4.9 -2.097383275853e-01 -3.146946710152e-01    4.9 -2.920545942440e-01  1.812466920450e-01
 5.0 -1.775967713143e-01 -3.275791375915e-01    5.0 -3.085176252490e-01  1.478631433912e-01

 392                            BESSEL FUNCTIONS OF INTEGER ORDER                            393

 Table 9.1                   BESSEL FUNCTIONS--ORDER 0, 1 AND 2

 x   J0(x)               J1(x)                  x   Y0(x)               Y1(x)

 5.0 -1.775967713143e-01 -3.275791375915e-01    5.0 -3.085176252490e-01  1.478631433912e-01
 5.1 -1.443347470605e-01 -3.370972020182e-01    5.1 -3.216024491249e-01  1.137364419775e-01
 5.2 -1.102904397910e-01 -3.432230058719e-01    5.2 -3.312509348199e-01  7.919034298209e-02
 5.3 -7.580311158559e-02 -3.459608338012e-01    5.3 -3.374373010937e-01  4.454761908761e-02
 5.4 -4.121010124499e-02 -3.453447907796e-01    5.4 -3.401678782759e-01  1.012726668250e-02
 5.5 -6.843869417820e-03 -3.414382154290e-01    5.5 -3.394805928819e-01 -2.375823895639e-02
 5.6  2.697088468511e-02 -3.343328362910e-01    5.6 -3.354441812453e-01 -5.680561439948e-02
 5.7  5.992000972404e-02 -3.241476802229e-01    5.7 -3.281571407937e-01 -8.872334050660e-02
 5.8  9.170256757481e-02 -3.110277443039e-01    5.8 -3.177464299633e-01 -1.192341134563e-01
 5.9  1.220333545928e-01 -2.951424447290e-01    5.9 -3.043659299972e-01 -1.480771525310e-01
 6.0  1.506452572510e-01 -2.766838581276e-01    6.0 -2.881946839816e-01 -1.750103443004e-01
 6.1  1.772914222427e-01 -2.558647725584e-01    6.1 -2.694349304312e-01 -1.998122044893e-01
 6.2  2.017472229489e-01 -2.329165670732e-01    6.2 -2.483099505160e-01 -2.222836406201e-01
 6.3  2.238120061322e-01 -2.080869402072e-01    6.3 -2.250617496168e-01 -2.422495004633e-01
 6.4  2.433106048234e-01 -1.816375090242e-01    6.4 -1.999485952920e-01 -2.595598933986e-01
 6.5  2.600946055816e-01 -1.538413014100e-01    6.5 -1.732424349190e-01 -2.740912739593e-01
 6.6  2.740433606241e-01 -1.249801651606e-01    6.6 -1.452262172341e-01 -2.857472790945e-01
 6.7  2.850647377106e-01 -9.534211804139e-02    6.7 -1.161911427261e-01 -2.944593130029e-01
 6.8  2.930956031043e-01 -6.521866340169e-02    6.8 -8.643386833578e-02 -3.001868757586e-01
 6.9  2.981020354048e-01 -3.490209610462e-02    6.9 -5.625369217089e-02 -3.029176343352e-01
 7.0  3.000792705196e-01 -4.682823482349e-03    7.0 -2.594974396721e-02 -3.026672370242e-01
 7.1  2.990513805016e-01  2.515327425391e-02    7.1  4.181793191708e-03 -2.994788746010e-01
 7.2  2.950706914010e-01  5.432742022236e-02    7.2  3.385040483617e-02 -2.934225939199e-01
 7.3  2.882169476350e-01  8.257043049326e-02    7.3  6.277388637403e-02 -2.845943718681e-01
 7.4  2.785962326575e-01  1.096250948540e-01    7.4  9.068088018003e-02 -2.731149597811e-01
 7.5  2.663396578804e-01  1.352484275797e-01    7.5  1.173132861482e-01 -2.591285104861e-01
 7.6  2.516018338500e-01  1.592137683964e-01    7.6  1.424285246603e-01 -2.428010020793e-01
 7.7  2.345591395865e-01  1.813127153246e-01    7.7  1.658016324239e-01 -2.243184743430e-01
 7.8  2.154078077463e-01  2.013568727559e-01    7.8  1.872271733096e-01 -2.038850953513e-01
 7.9  1.943618448413e-01  2.191793999217e-01    7.9  2.065209481444e-01 -1.817210772806e-01
 8.0  1.716508071376e-01  2.346363468539e-01    8.0  2.235214893876e-01 -1.580604617313e-01
 8.1  1.475174540444e-01  2.476077669816e-01    8.1  2.380913287022e-01 -1.331487959525e-01
 8.2  1.222153017841e-01  2.579985976487e-01    8.2  2.501180276191e-01 -1.072407222508e-01
 8.3  9.600610089501e-02  2.657393020419e-01    8.3  2.595149637545e-01 -8.059750353385e-02
 8.4  6.915726165699e-02  2.707862682768e-01    8.4  2.662218673639e-01 -5.348450839742e-02
 8.5  4.193925184294e-02  2.731219636741e-01    8.5  2.702051053658e-01 -2.616867939854e-02
 8.6  1.462299127875e-02  2.727548445459e-01    8.6  2.714577123380e-01  1.083991830187e-03
 8.7 -1.252273244966e-02  2.697190240921e-01    8.7  2.699991703467e-01  2.801095917680e-02
 8.8 -3.923380317654e-02  2.640737032397e-01    8.8  2.658749417912e-01  5.435556333494e-02
 8.9 -6.525324685124e-02  2.559023714440e-01    8.9  2.591557617217e-01  7.986939739413e-02
 9.0 -9.033361118287e-02  2.453117865733e-01    9.0  2.499366982850e-01  1.043145751967e-01
 9.1 -1.142392326832e-01  2.324307450059e-01    9.1  2.383359920541e-01  1.274658819640e-01
 9.2 -1.367483707649e-01  2.174086549605e-01    9.2  2.244936869905e-01  1.491127879223e-01
 9.3 -1.576551899434e-01  2.004139278437e-01    9.3  2.085700676452e-01  1.690613070614e-01
 9.4 -1.767715727515e-01  1.816322040070e-01    9.4  1.907439189130e-01  1.871356847183e-01
 9.5 -1.939287476874e-01  1.612644307575e-01    9.5  1.712106262027e-01  2.031798993872e-01
 9.6 -2.089787183689e-01  1.395248117407e-01    9.6  1.501801352559e-01  2.170589659890e-01
 9.7 -2.217954820317e-01  1.166386479002e-01    9.7  1.278747920242e-01  2.286600297761e-01
 9.8 -2.322760275794e-01  9.284009111281e-02    9.8  1.045270839992e-01  2.378932420862e-01
 9.9 -2.403411055348e-01  6.836983228370e-02    9.9  8.037730516278e-02  2.446924112612e-01
10.0 -2.459357644513e-01  4.347274616887e-02   10.0  5.567116728360e-02  2.490154242070e-01

 394                            BESSEL FUNCTIONS OF INTEGER ORDER                            395

 Table 9.1                   BESSEL FUNCTIONS--ORDER 0, 1 AND 2

 x   J0(x)               J1(x)                  x   Y0(x)               Y1(x)

 10.0 -2.459357644513e-01  4.347274616887e-02   10.0  5.567116728360e-02  2.490154242070e-01
 10.1 -2.490296505809e-01  1.839551545758e-02   10.1  3.065738063332e-02  2.508444362565e-01
 10.2 -2.496170698541e-01 -6.615743297719e-03   10.2  5.585227315442e-03  2.501858291982e-01
 10.3 -2.477168134822e-01 -3.131782947600e-02   10.3 -1.929784969275e-02  2.470699395159e-01
 10.4 -2.433717507142e-01 -5.547276184899e-02   10.4 -4.374861899939e-02  2.415505610460e-01
 10.5 -2.366481944623e-01 -7.885001422733e-02   10.5 -6.753037249787e-02  2.337042283573e-01
 10.6 -2.276350476207e-01 -1.012286625856e-01   10.6 -9.041515478014e-02  2.236292891956e-01
 10.7 -2.164427399238e-01 -1.223994239272e-01   10.7 -1.121858896683e-01  2.114447762747e-01
 10.8 -2.032019671120e-01 -1.421665682987e-01   10.8 -1.326383843902e-01  1.972890905317e-01
 10.9 -1.880622459633e-01 -1.603496866808e-01   10.9 -1.515831932230e-01  1.813185096742e-01
 11.0 -1.711903004072e-01 -1.767852989567e-01   11.0 -1.688473238921e-01  1.637055374149e-01
 11.1 -1.527682954357e-01 -1.913282877750e-01   11.1 -1.842757716215e-01  1.446371102063e-01
 11.2 -1.329919368596e-01 -2.038531458647e-01   11.2 -1.977328674821e-01  1.243126795321e-01
 11.3 -1.120684561098e-01 -2.142550262075e-01   11.3 -2.091034295385e-01  1.029421888862e-01
 11.4 -9.021450024752e-02 -2.224505864150e-01   11.4 -2.182937072596e-01  8.074396544663e-02
 11.5 -6.765394811167e-02 -2.283786206653e-01   11.5 -2.252321116912e-01  5.794254714301e-02
 11.6 -4.461567409444e-02 -2.320004746198e-01   11.6 -2.298697259857e-01  3.476646630071e-02
 11.7 -2.133128138851e-02 -2.333002408314e-01   11.7 -2.321805930194e-01  1.144601132753e-02
 11.8  1.967173306734e-03 -2.322847342674e-01   11.8 -2.321617789782e-01 -1.178901201396e-02
 11.9  2.504944169958e-02 -2.289832496619e-01   11.9 -2.298332139434e-01 -3.471149833402e-02
 12.0  4.768931079683e-02 -2.234471044906e-01   12.0 -2.252373126344e-01 -5.709921826089e-02
 12.1  6.966677360680e-02 -2.157489733769e-01   12.1 -2.184383805509e-01 -7.873693145139e-02
 12.2  9.077012317050e-02 -2.059820216996e-01   12.2 -2.095218127752e-01 -9.941841713893e-02
 12.3  1.107979503076e-01 -1.942588480406e-01   12.3 -1.985930946350e-01 -1.189484032993e-01
 12.4  1.295610265175e-01 -1.807102468827e-01   12.4 -1.857766152672e-01 -1.371443765986e-01
 12.5  1.468840547004e-01 -1.654838046148e-01   12.5 -1.712143068447e-01 -1.538382565375e-01
 12.6  1.626072717455e-01 -1.487423434220e-01   12.6 -1.550641238173e-01 -1.688779186029e-01
 12.7  1.765878885615e-01 -1.306622290042e-01   12.7 -1.374983779633e-01 -1.821285527798e-01
 12.8  1.887013547807e-01 -1.114315592776e-01   12.8 -1.187019463284e-01 -1.934738454315e-01
 12.9  1.988424371363e-01 -9.124825224995e-02   12.9 -9.887037024150e-02 -2.028169743237e-01
 13.0  2.069261023771e-01 -7.031805212178e-02   13.0 -7.820786452788e-02 -2.100814084207e-01
 13.1  2.128881975221e-01 -4.885247333423e-02   13.1 -5.692525678130e-02 -2.152115060050e-01
 13.2  2.166859222586e-01 -2.706670276479e-02   13.2 -3.523787710230e-02 -2.181729066455e-01
 13.3  2.182980903193e-01 -5.177480554678e-03   13.3 -1.336341905865e-02 -2.189527145477e-01
 13.4  2.177251787312e-01  1.659901986400e-02   13.4  8.480207231244e-03 -2.175594728370e-01
 13.5  2.149891658804e-01  3.804929208599e-02   13.5  3.007700904678e-02 -2.140229303400e-01
 13.6  2.101331613693e-01  5.896455724873e-02   13.6  5.121501145646e-02 -2.083936044154e-01
 13.7  2.032208326330e-01  7.914276510011e-02   13.7  7.168830401567e-02 -2.007421453278e-01
 13.8  1.943356352156e-01  9.839051665835e-02   13.8  9.129901427859e-02 -1.911585095362e-01
 13.9  1.835798554579e-01  1.165248903691e-01   13.9  1.098591894595e-01 -1.797509510696e-01
 14.0  1.710734761105e-01  1.333751546988e-01   14.0  1.271925685822e-01 -1.666448418562e-01
 14.1  1.569528770326e-01  1.487843512974e-01   14.1  1.431362286225e-01 -1.519813334678e-01
 14.2  1.413693846571e-01  1.626107342002e-01   14.2  1.575420894708e-01 -1.359158741907e-01
 14.3  1.244876852839e-01  1.747290520132e-01   14.3  1.702782639962e-01 -1.186165966565e-01
 14.4  1.064841184903e-01  1.850316616146e-01   14.4  1.812302410821e-01 -1.002625924273e-01
 14.5  8.754486801038e-02  1.934294635960e-01   14.5  1.903018911878e-01 -8.104209092875e-02
 14.6  6.786406832339e-02  1.998526514474e-01   14.6  1.974162857774e-01 -6.115056094904e-02
 14.7  4.764184590153e-02  2.042512683299e-01   14.7  2.025163238107e-01 -4.078875357125e-02
 14.8  2.708231458588e-02  2.065955671798e-01   14.8  2.055651604029e-01 -2.016070586319e-02
 14.9  6.391544890861e-03  2.068761718099e-01   14.9  2.065464347070e-01  5.282750764142e-04
 15.0 -1.422447282677e-02  2.051040386135e-01   15.0  2.054642960389e-01  2.107362803687e-02

 396                            BESSEL FUNCTIONS OF INTEGER ORDER                            397

 Table 9.1                   BESSEL FUNCTIONS--ORDER 0, 1 AND 2

 x   J0(x)               J1(x)                  x   Y0(x)               Y1(x)

 15.0 -1.422447282677e-02  2.051040386135e-01   15.0  2.054642960389e-01  2.107362803687e-02
 15.1 -3.456185145556e-02  2.013102204085e-01   15.1  2.023432292287e-01  4.127353400948e-02
 15.2 -5.442079684403e-02  1.955454358660e-01   15.2  1.972276821250e-01  6.093087362448e-02
 15.3 -7.360754495112e-02  1.878794498324e-01   15.3  1.901815000860e-01  7.985512692091e-02
 15.4 -9.193622786231e-02  1.784002716552e-01   15.4  1.812871741344e-01  9.786419732092e-02
 15.5 -1.092306509000e-01  1.672131803518e-01   15.5  1.706449112294e-01  1.147861425133e-01
 15.6 -1.253259640225e-01  1.544395870857e-01   15.6  1.583715367894e-01  1.304607959476e-01
 15.7 -1.400702118290e-01  1.402157469423e-01   15.7  1.445992411700e-01  1.447412637878e-01
 15.8 -1.533257477607e-01  1.246913333884e-01   15.8  1.294741832575e-01  1.574952834674e-01
 15.9 -1.649704994857e-01  1.080278900631e-01   15.9  1.131549656518e-01  1.686064314007e-01
 16.0 -1.748990739836e-01  9.039717566131e-02   16.0  9.581099708072e-02  1.779751689394e-01
 16.1 -1.830236924653e-01  7.197941862246e-02   16.1  7.762075870139e-02  1.855197172915e-01
 16.2 -1.892749469779e-01  5.296149912684e-02   16.2  5.876999178350e-02  1.911767538284e-01
 16.3 -1.936023723284e-01  3.353507651579e-02   16.3  3.944982494208e-02  1.949019239839e-01
 16.4 -1.959748287910e-01  1.389468068626e-02   16.4  1.985485957386e-02  1.966701647698e-01
 16.5 -1.963806929369e-01 -5.764213735624e-03   16.5  1.812324575481e-04  1.964758377859e-01
 16.6 -1.948278558056e-01 -2.524711156840e-02   16.6 -1.937532540317e-02  1.943326714661e-01
 16.7 -1.913435295252e-01 -4.436240083665e-02   16.7 -3.862141468187e-02  1.902735141580e-01
 16.8 -1.859738653476e-01 -6.292321765586e-02   16.8 -5.736785961672e-02  1.843499014676e-01
 16.9 -1.787833878912e-01 -8.074925425014e-02   16.9 -7.543154755580e-02  1.766314430901e-01
 17.0 -1.698542521512e-01 -9.766849275778e-02   17.0 -9.263719844232e-02  1.672050360772e-01
 17.1 -1.592853315323e-01 -1.135188482914e-01   17.1 -1.088190473004e-01  1.561739131484e-01
 17.2 -1.471911467660e-01 -1.281497056872e-01   17.2 -1.238224237217e-01  1.436565362143e-01
 17.3 -1.337006470758e-01 -1.414233354920e-01   17.3 -1.375052134435e-01  1.297853467391e-01
 17.4 -1.189558563364e-01 -1.532161759880e-01   17.4 -1.497391883399e-01  1.147053859034e-01
 17.5 -1.031103982287e-01 -1.634199694258e-01   17.5 -1.604111925050e-01  9.857279873422e-02
 17.6 -8.632791549801e-02 -1.719427421176e-01   17.6 -1.694241735779e-01  8.155323742984e-02
 17.7 -6.878039938214e-02 -1.787096196184e-01   17.7 -1.766980500287e-01  6.382018001233e-02
 17.8 -5.064644607112e-02 -1.836634698715e-01   17.8 -1.821704067768e-01  4.555318118908e-02
 17.9 -3.210945768656e-02 -1.867653689135e-01   17.9 -1.857970132314e-01  2.693607288059e-02
 18.0 -1.335580572199e-02 -1.879948854881e-01   18.0 -1.875521596114e-01  8.155132278224e-03
 18.1  5.427024838490e-03 -1.873501827064e-01   18.1 -1.874288092000e-01 -1.060276447553e-02
 18.2  2.405229240961e-02 -1.848479366856e-01   18.2 -1.854385660060e-01 -2.915198483965e-02
 18.3  4.233583847140e-02 -1.805230738865e-01   18.3 -1.816114591091e-01 -4.730996064035e-02
 18.4  6.009789204225e-02 -1.744283306318e-01   18.4 -1.759955467582e-01 -6.489896933080e-02
 18.5  7.716482142255e-02 -1.666336400100e-01   18.5 -1.686563450403e-01 -8.174785849681e-02
 18.6  9.337081627746e-02 -1.572253530294e-01   18.6 -1.596760876317e-01 -9.769369699823e-02
 18.7  1.085594838932e-01 -1.463053024741e-01   18.7 -1.491528247654e-01 -1.125833369325e-01
 18.8  1.225853443627e-01 -1.339897194140e-01   18.8 -1.371993710854e-01 -1.262748715543e-01
 18.9  1.353152105223e-01 -1.204080137110e-01   18.9 -1.239421134900e-01 -1.386389753721e-01
 19.0  1.466294396597e-01 -1.057014311424e-01   19.0 -1.095196913853e-01 -1.495601138627e-01
 19.1  1.564230453336e-01 -9.002160090899e-02   19.1 -9.408156295987e-02 -1.589376115760e-01
 19.2  1.646066590768e-01 -7.352898830426e-02   19.2 -7.778647214352e-02 -1.666865688437e-01
 19.3  1.711073332708e-01 -5.639126817890e-02   19.3 -6.080083181602e-02 -1.727386188313e-01
 19.4  1.758691780863e-01 -3.878163553798e-02   19.4 -4.329703957743e-02 -1.770425182737e-01
 19.5  1.788538270402e-01 -2.087707014810e-02   19.5 -2.545174297615e-02 -1.795645668963e-01
 19.6  1.800407274325e-01 -2.856572403404e-03   19.6 -7.444071505223e-03 -1.802888522206e-01
 19.7  1.794272536588e-01  1.510061209776e-02   19.7  1.054614707836e-02 -1.792173181821e-01
 19.8  1.770286431451e-01  3.281676178388e-02   19.8  2.834016848615e-02 -1.763696577181e-01
 19.9  1.728777563926e-01  5.011742480738e-02   19.9  4.576209415939e-02 -1.717830312105e-01
 20.0  1.670246643406e-01  6.683312417585e-02   20.0  6.264059680939e-02 -1.655116143625e-01
*/
void sphericalbesselJ0NTest(std::ostream&); // compare with Table 10.1 of Abramowitz & Stegun
/*
 457                          BESSEL FUNCTIONS OF FRACTIONAL ORDER

 Table 10.1                SPHERICAL BESSEL FUNCTIONS--ORDER 0, 1 AND 2

 x       j0(x)               j1(x)               j2(x)

 0.0  1.000000000000e+00  0.000000000000e+00  0.000000000000e+00
 0.1  9.983341664683e-01  3.330001190256e-02  6.661906084456e-04
 0.2  9.933466539753e-01  6.640038067032e-02  2.659056079527e-03
 0.3  9.850673555378e-01  9.910288804064e-02  5.961524868620e-03
 0.4  9.735458557716e-01  1.312121544219e-01  1.054530239227e-02
 0.5  9.588510772084e-01  1.625370306361e-01  1.637110660799e-02
 0.6  9.410707889917e-01  1.928919568034e-01  2.338899502534e-02
 0.7  9.203109817681e-01  2.220982778338e-01  3.153878037661e-02
 0.8  8.966951136244e-01  2.499855053465e-01  4.075053142515e-02
 0.9  8.703632329194e-01  2.763925162764e-01  5.094515466858e-02
 1.0  8.414709848079e-01  3.011686789398e-01  6.203505201137e-02
 1.1  8.101885091468e-01  3.241748979283e-01  7.392484883964e-02
 1.2  7.766992383060e-01  3.452845698578e-01  8.651218633845e-02
 1.3  7.411986041671e-01  3.643844427250e-01  9.968857135213e-02
 1.4  7.038926642775e-01  3.813753724123e-01  1.133402766060e-01
 1.5  6.649966577360e-01  3.961729707122e-01  1.273492836884e-01
 1.6  6.247335019009e-01  4.087081401264e-01  1.415942608360e-01
 1.7  5.833322414426e-01  4.189274916107e-01  1.559515672821e-01
 1.8  5.410264615990e-01  4.267936423845e-01  1.702962757085e-01
 1.9  4.980526777302e-01  4.322853918914e-01  1.845032042036e-01
 2.0  4.546487134128e-01  4.353977749800e-01  1.984479490571e-01
 2.1  4.110520793566e-01  4.361419923602e-01  2.120079097294e-01
 2.2  3.674983653725e-01  4.345452193763e-01  2.250632974133e-01
 2.3  3.242196574681e-01  4.306502951078e-01  2.374981187594e-01
 2.4  2.814429918963e-01  4.245152947656e-01  2.492011265607e-01
 2.5  2.393888576416e-01  4.162129892754e-01  2.600667294889e-01
 2.6  1.982697583929e-01  4.058301968315e-01  2.699958533357e-01
 2.7  1.582888445310e-01  3.934670320549e-01  2.788967466410e-01
 2.8  1.196386250557e-01  3.792360591873e-01  2.866857240735e-01
 2.9  8.249976869448e-02  3.632613564980e-01  2.932878414758e-01
 3.0  4.704000268662e-02  3.456774997624e-01  2.986374970757e-01
 3.1  1.341311691396e-02  3.266284732862e-01  3.026789540082e-01
 3.2 -1.824191982112e-02  3.062665174918e-01  3.053667799696e-01
 3.3 -4.780172549795e-02  2.847509225488e-01  3.066662005423e-01
 3.4 -7.515914765495e-02  2.622467779190e-01  3.065533634658e-01
 3.5 -1.002237793399e-01  2.389236879860e-01  3.050155118993e-01
 3.6 -1.229223453597e-01  2.149544641596e-01  3.020510654927e-01
 3.7 -1.431989570023e-01  1.905138039752e-01  2.976696088741e-01
 3.8 -1.610152344586e-01  1.657769677515e-01  2.918917879467e-01
 3.9 -1.763502972267e-01  1.409184633265e-01  2.847491151701e-01
 4.0 -1.892006238270e-01  1.161107492592e-01  2.762836857714e-01
 4.1 -1.995797831864e-01  9.152296666996e-02  2.665478075791e-01
 4.2 -2.075180410509e-01  6.731970959282e-02  2.556035479029e-01
 4.3 -2.130618457557e-01  4.365984333123e-02  2.435222015682e-01
 4.4 -2.162731986113e-01  2.069537985617e-02  2.303836848768e-01
 4.5 -2.172289150367e-01 -1.429581245757e-03  2.162758608729e-01
 4.6 -2.160197833986e-01 -2.257983836164e-02  2.012938018584e-01
 4.7 -2.127496292690e-01 -4.262999272469e-02  1.855389956149e-01
 4.8 -2.075342935075e-01 -6.146526603061e-02  1.691185022383e-01
 4.9 -2.005005331886e-01 -7.898222502270e-02  1.521440688890e-01
 5.0 -1.917848549326e-01 -9.508940807917e-02  1.347312100851e-01

 458                          BESSEL FUNCTIONS OF FRACTIONAL ORDER

  Table 10.1                SPHERICAL BESSEL FUNCTIONS--ORDER 0, 1 AND 2

 x   j0(x)               j1(x)               j2(x)
 5.0 -1.917848549326e-01 -9.508940807917e-02  1.347312100851e-01
 5.1 -1.815322906525e-01 -1.097078496795e-01  1.169982614293e-01
 5.2 -1.698951261000e-01 -1.227714995001e-01  9.906541484998e-02
 5.3 -1.570315928724e-01 -1.342275337833e-01  8.105374356112e-02
 5.4 -1.431045347326e-01 -1.440365575324e-01  6.308422499234e-02
 5.5 -1.282800591946e-01 -1.521726969974e-01  4.527676992329e-02
 5.6 -1.127261853343e-01 -1.586235828294e-01  2.774926596146e-02
 5.7 -9.661149870134e-02 -1.633902251825e-01  1.061664334211e-02
 5.8 -8.010382403686e-02 -1.664867829273e-01 -6.010029201389e-03
 5.9 -6.336892624241e-02 -1.679402299977e-01 -2.202441104455e-02
 6.0 -4.656924969982e-02 -1.677899227250e-01 -3.732571166269e-02
 6.1 -2.986270561838e-02 -1.660870727969e-01 -5.181946133091e-02
 6.2 -1.340151658347e-02 -1.628941312269e-01 -6.541822433277e-02
 6.3  2.668873092753e-03 -1.582840894112e-01 -7.804224900286e-02
 6.4  1.821081325789e-02 -1.523397039844e-01 -8.962004950059e-02
 6.5  3.309538278274e-02 -1.451526527608e-01 -1.000889148262e-01
 6.6  4.720323689597e-02 -1.368226295549e-01 -1.093953412391e-01
 6.7  6.042536128606e-02 -1.274563861118e-01 -1.174953849182e-01
 6.8  7.266372810862e-02 -1.171667297414e-01 -1.243549324063e-01
 6.9  8.383184991133e-02 -1.060714855383e-01 -1.299498871019e-01
 7.0  9.385522838840e-02 -9.429243227927e-02 -1.342662707938e-01
 7.1  1.026716957924e-01 -8.195422121837e-02 -1.373002399692e-01
 7.2  1.102316477568e-01 -6.918328705214e-02 -1.390580173619e-01
 7.3  1.164981672094e-01 -5.610676029750e-02 -1.395557399344e-01
 7.4  1.214470399745e-01 -4.285139021620e-02 -1.388192251973e-01
 7.5  1.250666635700e-01 -2.954248723534e-02 -1.368836584641e-01
 7.6  1.273578515831e-01 -1.630289355252e-02 -1.337932043012e-01
 7.7  1.283335368671e-01 -3.251990281913e-03 -1.296005460679e-01
 7.8  1.280183776121e-01  9.495250903778e-03 -1.243663580338e-01
 7.9  1.264482711190e-01  2.182916414664e-02 -1.181587151139e-01
 8.0  1.236697808279e-01  3.364622682957e-02 -1.110524457668e-01
 8.1  1.197394828204e-01  4.484983167360e-02 -1.031284340524e-01
 8.2  1.147232386195e-01  5.535098775650e-02 -9.447287724516e-02
 8.3  1.086954016574e-01  6.506894537687e-02 -8.517650573804e-02
 8.4  1.017379652486e-01  7.393174040063e-02 -7.533377224838e-02
 8.5  9.393966030865e-02  8.187665446982e-02 -6.504201755459e-02
 8.6  8.539501138071e-02  8.885058822101e-02 -5.440062014082e-02
 8.7  7.620335977956e-02  9.481034544563e-02 -4.351013721210e-02
 8.8  6.646786282861e-02  9.972282691935e-02 -3.247144456065e-02
 8.9  5.629447825370e-02  1.035651334264e-01 -2.138488271671e-02
 9.0  4.579094280464e-02  1.063245782988e-01 -1.034941670504e-02
 9.1  3.506575410433e-02  1.079986105753e-01  5.381834479782e-04
 9.2  2.422716457612e-02  1.085946506500e-01  1.118413454889e-02
 9.3  1.338219607603e-02  1.081298410246e-01  2.149839780287e-02
 9.4  2.635683558870e-03  1.066307154887e-01  3.139539585243e-02
 9.5 -7.910644259136e-03  1.041327907302e-01  4.079468343711e-02
 9.6 -1.815903971073e-02  1.006800850087e-01  4.962156627594e-02
 9.7 -2.801655942381e-02  9.632456911418e-02  5.780766327355e-02
 9.8 -3.739582951550e-02  9.112555536625e-02  6.529140768884e-02
 9.9 -4.621574684599e-02  8.514903088681e-02  7.201848347836e-02
10.0 -5.440211108894e-02  7.846694179875e-02  7.794219362856e-02

 458bis

 10.0 -5.440211108894e-02  7.846694179875e-02  7.794219362856e-02
 10.1 -6.188818305870e-02  7.115643535747e-02  8.302375791736e-02
 10.2 -6.861516545035e-02  6.329906731146e-02  8.723253818901e-02
 10.3 -7.453260288967e-02  5.497998201023e-02  9.054618988294e-02
 10.4 -7.959869895054e-02  4.628707293045e-02  9.295073921894e-02
 10.5 -8.378054856873e-02  3.731013137403e-02  9.444058610417e-02
 10.6 -8.705428505781e-02  2.813999110510e-02  9.501843348378e-02
 10.7 -8.940514170749e-02  1.886767663681e-02  9.469514450286e-02
 10.8 -9.082742870986e-02  9.583562754213e-03  9.348952947492e-02
 10.9 -9.132442690884e-02  3.765526690027e-04  9.142806525811e-02
 11.0 -9.090820059552e-02 -8.666718053050e-03  8.854455021741e-02
 11.1 -8.959933227063e-02 -1.746264496723e-02  8.487969849570e-02
 11.2 -8.742658295994e-02 -2.593137917667e-02  8.048067782333e-02
 11.3 -8.442648229222e-02 -3.399750668754e-02  7.540059556102e-02
 11.4 -8.064285312848e-02 -4.159067714705e-02  6.969793808978e-02
 11.5 -7.612627605986e-02 -4.864617694025e-02  6.343596903197e-02
 11.6 -7.093349956627e-02 -5.510544218398e-02  5.668209210489e-02
 11.7 -6.512680204436e-02 -6.091650775198e-02  4.950718467206e-02
 11.8 -5.877331226925e-02 -6.603438908984e-02  4.198490826336e-02
 11.9 -5.194429514597e-02 -7.042139422580e-02  3.419100248400e-02
 12.0 -4.471440983337e-02 -7.404736404715e-02  2.620256882158e-02
 12.1 -3.716094748220e-02 -7.688983958608e-02  1.809735089061e-02
 12.2 -2.936305592105e-02 -7.893415573735e-02  9.953017624984e-03
 12.3 -2.140095864763e-02 -8.017346150685e-02  1.846455841080e-03
 12.4 -1.335517543938e-02 -8.060866755792e-02 -6.146921550438e-03
 12.5 -5.305751788098e-03 -8.024832247733e-02 -1.395384560646e-02
 12.6  2.668495811199e-03 -7.910841981665e-02 -2.150383386278e-02
 12.7  1.049071192283e-02 -7.721213857413e-02 -2.872979977498e-02
 12.8  1.808670508606e-02 -7.458952036026e-02 -3.556862392049e-02
 12.9  2.538561543703e-02 -7.127708703278e-02 -4.196168218884e-02
 13.0  3.232054129436e-02 -6.731740308891e-02 -4.785532662257e-02
 13.1  3.882911941774e-02 -6.275858756066e-02 -5.320131504231e-02
 13.2  4.485405414448e-02 -5.765378056788e-02 -5.795718609173e-02
 13.3  5.034359114260e-02 -5.206057004219e-02 -6.208657686640e-02
 13.4  5.525193208600e-02 -4.604038443787e-02 -6.555948084075e-02
 13.5  5.953958715197e-02 -3.965785749318e-02 -6.835244437268e-02
 13.6  6.317366285709e-02 -3.298017129383e-02 -7.044870064250e-02
 13.7  6.612808338018e-02 -2.607638401975e-02 -7.183824046479e-02
 13.8  6.838374416262e-02 -1.901674882511e-02 -7.251781999416e-02
 13.9  6.992859722266e-02 -1.187203031059e-02 -7.249090592279e-02
 14.0  7.075766826392e-02 -4.712824995996e-03 -7.176755933449e-02
 14.1  7.087300629418e-02  2.391107908037e-03 -7.036425993077e-02
 14.2  7.028356709270e-02  9.371499296954e-03 -6.830367287504e-02
 14.3  6.900503246676e-02  1.616220062702e-02 -6.561436100654e-02
 14.4  6.705956781592e-02  2.269978639095e-02 -6.233044565114e-02
 14.5  6.447552107067e-02  2.892412330064e-02 -5.849121969812e-02
 14.6  6.128706658497e-02  3.477890123269e-02 -5.414071701661e-02
 14.7  5.753379803694e-02  4.021212140313e-02 -4.932724264854e-02
 14.8  5.326027482266e-02  4.517653768766e-02 -4.410286853462e-02
 14.9  4.851552681289e-02  4.963004748537e-02 -3.852289980241e-02
 15.0  4.335252267714e-02  5.353602903573e-02 -3.264531687000e-02

458ter

 15.0  4.335252267714e-02  5.353602903573e-02 -3.264531687000e-02
 15.1  3.782760726225e-02  5.686362263278e-02 -2.653019879217e-02
 15.2  3.199991374038e-02  5.958795373365e-02 -2.023913339822e-02
 15.3  2.593075641377e-02  6.169029652471e-02 -1.383461984030e-02
 15.4  1.968301017829e-02  6.315817708217e-02 -7.379469188261e-03
 15.5  1.332048270567e-02  6.398541584029e-02 -9.362086720615e-04
 15.6  6.907285403813e-03  6.417210965305e-02  5.433504914082e-03
 15.7  5.072091583423e-04  6.372455429985e-02  1.166945726838e-02
 15.8 -5.816889254914e-03  6.265510883661e-02  1.771342890743e-02
 15.9 -1.200368436315e-02  6.098200372599e-02  2.350972280202e-02
 16.0 -1.799395729156e-02  5.872909518949e-02  2.900566263959e-02
 16.1 -2.373114392447e-02  5.592556870489e-02  3.415205734774e-02
 16.2 -2.916185101225e-02  5.260559502161e-02  3.890362786810e-02
 16.3 -3.423633566176e-02  4.880794247907e-02  4.321939255975e-02
 16.4 -3.890894404561e-02  4.457554978637e-02  4.706300803092e-02
 16.5 -4.313850559813e-02  3.995506375179e-02  5.040306264391e-02
 16.6 -4.688867942978e-02  3.499634673594e-02  5.321332040615e-02
 16.7 -5.012825018082e-02  2.975195883913e-02  5.547291344534e-02
 16.8 -5.283137104652e-02  2.427662002172e-02  5.716648176468e-02
 16.9 -5.497775223280e-02  1.862665749292e-02  5.828425948007e-02
 17.0 -5.655279363997e-02  1.285944378892e-02  5.882210724978e-02
 17.1 -5.754766111589e-02  7.032830994576e-03  5.878149111493e-02
 17.2 -5.795930616521e-02  1.204586544573e-03  5.816940846949e-02
 17.3 -5.779042954179e-02 -4.568164029793e-03  5.699826236900e-02
 17.4 -5.704938968222e-02 -1.022948216153e-02  5.528568586127e-02
 17.5 -5.575005745532e-02 -1.572514403810e-02  5.305431847736e-02
 17.6 -5.391161919989e-02 -2.100314625677e-02  5.033153745158e-02
 17.7 -5.155833049668e-02 -2.601418473986e-02  4.714914664246e-02
 17.8 -4.871922356661e-02 -3.071210260421e-02  4.354302649848e-02
 17.9 -4.542777160120e-02 -3.505430301694e-02  3.955274874920e-02
 18.0 -4.172151370954e-02 -3.900212344187e-02  3.522115980256e-02
 18.1 -3.764164450583e-02 -4.252116808369e-02  3.059393708864e-02
 18.2 -3.323257265969e-02 -4.558159576657e-02  2.571912280805e-02
 18.3 -2.854145298507e-02 -4.815836093530e-02  2.064663971699e-02
 18.4 -2.361769685173e-02 -5.023140593677e-02  1.542779370987e-02
 18.5 -1.851246586322e-02 -5.178580322224e-02  1.011476804340e-02
 18.6 -1.327815385681e-02 -5.281184660193e-02  4.760114082303e-03
 18.7 -7.967862343005e-03 -5.330509117846e-02 -5.837565626296e-04
 18.8 -2.634874514807e-03 -5.326634207989e-02 -5.865073689432e-03
 18.9  2.667867079726e-03 -5.270159260126e-02 -1.103319923866e-02
 19.0  7.888274192787e-03 -5.162191284178e-02 -1.603910253623e-02
 19.1  1.297561298340e-02 -5.004329038868e-02 -2.083581566225e-02
 19.2  1.788098587604e-02 -4.798642504275e-02 -2.537886478897e-02
 19.3  2.255779069290e-02 -4.547648000282e-02 -2.962667359489e-02
 19.4  2.696215284318e-02 -4.254279232074e-02 -3.354093516082e-02
 19.5  3.105332665229e-02 -3.921854580316e-02 -3.708694908354e-02
 19.6  3.479406224837e-02 -3.554040986720e-02 -4.023392090152e-02
 19.7  3.815093478945e-02 -3.154814815147e-02 -4.295522130998e-02
 19.8  4.109463320743e-02 -2.728420093975e-02 -4.522860304679e-02
 19.9  4.360020606240e-02 -2.279324566878e-02 -4.703637375116e-02
 20.0  4.564726253638e-02 -1.812173996385e-02 -4.836552353096e-02
*/

/*
  complex Exponential Integral E1(z) = \f$\int_z^{\infty} t^{-1} exp(-t) dt\f$
                                     = \f$-\gamma - log(z) + \sum_{n>0} (-z)^n / n n!\f$
*/
complex_t e1z(const complex_t& z); //!< return E1(z)

complex_t eInz(const complex_t& z); //!< return \f$E1(z) + gamma + log(z) = \sum_{n>0} (-z)^n / n n!\f$
//  http://www.math.sfu.ca/~cbm/aands/page_228.htm (see footnote)

complex_t expzE1z(const complex_t&); //!< return exp(z)*E1(z)

complex_t zexpzE1z(const complex_t&); //!< return z*exp(z)*E1(z)

complex_t ascendingSeriesOfE1(const complex_t& z); //!< ascending series in E1 formula (used for 'small' z)
//  http://www.math.sfu.ca/~cbm/aands/page_229.htm (Formula 5.1.11)

complex_t continuedFractionOfE1(const complex_t& z); //!< continued fraction in E1 formula (used for 'large' z)
//  http://www.math.sfu.ca/~cbm/aands/page_229.htm (Formula 5.1.22)

//! For tests
void e1Test(std::ostream&);
/*
.                  EXPONENTIAL INTEGRAl AND RELATED FUNCTIONS                 249
.
.                  EXPONENTIAL INTEGRAL FOR COMPLEX ARGUMENTS      Table 5.6
.                            Z*EXP(Z)*E1(Z)

  y / x            -19                          -18                          -17                          -16                          -15
  0  1.059305e+00+i*3.344324e-07  1.063087e+00+i*8.612351e-07  1.067394e+00+i*2.211020e-06  1.072345e+00+i*5.656635e-06  1.078103e+00+i*1.441531e-05
  1  1.059090e+00+i*3.539294e-03  1.062827e+00+i*4.010195e-03  1.067073e+00+i*4.584363e-03  1.071942e+00+i*5.295878e-03  1.077584e+00+i*6.195111e-03
  2  1.058456e+00+i*6.999918e-03  1.062061e+00+i*7.917903e-03  1.066135e+00+i*9.031762e-03  1.070775e+00+i*1.040265e-02  1.076102e+00+i*1.211804e-02
  3  1.057431e+00+i*1.031039e-02  1.060829e+00+i*1.163254e-02  1.064636e+00+i*1.322636e-02  1.068925e+00+i*1.517151e-02  1.073783e+00+i*1.757854e-02
  4  1.056058e+00+i*1.341001e-02  1.059190e+00+i*1.507895e-02  1.062657e+00+i*1.707464e-02  1.066508e+00+i*1.948587e-02  1.070793e+00+i*2.243201e-02
  5  1.054391e+00+i*1.625223e-02  1.057215e+00+i*1.820220e-02  1.060297e+00+i*2.051241e-02  1.063659e+00+i*2.327231e-02  1.067318e+00+i*2.659819e-02
  6  1.052490e+00+i*1.880591e-02  1.054981e+00+i*2.096854e-02  1.057655e+00+i*2.350489e-02  1.060510e+00+i*2.649860e-02  1.063538e+00+i*3.005443e-02
  7  1.050413e+00+i*2.105479e-02  1.052565e+00+i*2.336398e-02  1.054829e+00+i*2.604354e-02  1.057187e+00+i*2.916706e-02  1.059610e+00+i*3.282308e-02
  8  1.048217e+00+i*2.299577e-02  1.050037e+00+i*2.539119e-02  1.051905e+00+i*2.814072e-02  1.053795e+00+i*3.130579e-02  1.055664e+00+i*3.495705e-02
  9  1.045956e+00+i*2.463653e-02  1.047458e+00+i*2.706578e-02  1.048958e+00+i*2.982395e-02  1.050421e+00+i*3.295981e-02  1.051797e+00+i*3.652667e-02
 10  1.043672e+00+i*2.599276e-02  1.044880e+00+i*2.841236e-02  1.046045e+00+i*3.113026e-02  1.047129e+00+i*3.418330e-02  1.048081e+00+i*3.760927e-02
 11  1.041402e+00+i*2.708569e-02  1.042345e+00+i*2.946107e-02  1.043212e+00+i*3.210162e-02  1.043967e+00+i*3.503363e-02  1.044559e+00+i*3.828184e-02
 12  1.039177e+00+i*2.793973e-02  1.039882e+00+i*3.024464e-02  1.040490e+00+i*3.278131e-02  1.040965e+00+i*3.556711e-02  1.041259e+00+i*3.861632e-02
 13  1.037018e+00+i*2.858068e-02  1.037515e+00+i*3.079624e-02  1.037901e+00+i*3.321138e-02  1.038140e+00+i*3.583627e-02  1.038192e+00+i*3.867717e-02
 14  1.034942e+00+i*2.903433e-02  1.035259e+00+i*3.114789e-02  1.035456e+00+i*3.343107e-02  1.035501e+00+i*3.588844e-02  1.035359e+00+i*3.852032e-02
 15  1.032959e+00+i*2.932546e-02  1.033123e+00+i*3.132946e-02  1.033162e+00+i*3.347589e-02  1.033049e+00+i*3.576510e-02  1.032754e+00+i*3.819324e-02
 16  1.031076e+00+i*2.947720e-02  1.031110e+00+i*3.136810e-02  1.031019e+00+i*3.337723e-02  1.030780e+00+i*3.550190e-02  1.030365e+00+i*3.773545e-02
 17  1.029296e+00+i*2.951065e-02  1.029222e+00+i*3.128800e-02  1.029025e+00+i*3.316238e-02  1.028685e+00+i*3.512899e-02  1.028180e+00+i*3.717942e-02
 18  1.027620e+00+i*2.944477e-02  1.027456e+00+i*3.111039e-02  1.027174e+00+i*3.285471e-02  1.026756e+00+i*3.467154e-02  1.026183e+00+i*3.655150e-02
 19  1.026046e+00+i*2.929630e-02  1.025809e+00+i*3.085365e-02  1.025459e+00+i*3.247400e-02  1.024981e+00+i*3.415034e-02  1.024360e+00+i*3.587287e-02
 20  1.024570e+00+i*2.907988e-02  1.024275e+00+i*3.053353e-02  1.023872e+00+i*3.203684e-02  1.023349e+00+i*3.358240e-02  1.022695e+00+i*3.516042e-02
  y / x            -14                          -13                          -12                          -11                          -10
  0  1.084892e+00+i*3.657254e-05  1.093027e+00+i*9.231345e-05  1.102975e+00+i*2.316313e-04  1.115431e+00+i*5.771693e-04  1.131470e+00+i*1.426281e-03
  1  1.084200e+00+i*7.359438e-03  1.092067e+00+i*8.912820e-03  1.101566e+00+i*1.106249e-02  1.113230e+00+i*1.416878e-02  1.127796e+00+i*1.887846e-02
  2  1.082276e+00+i*1.430627e-02  1.089498e+00+i*1.716091e-02  1.098025e+00+i*2.098137e-02  1.108171e+00+i*2.624109e-02  1.120286e+00+i*3.369962e-02
  3  1.079313e+00+i*2.060362e-02  1.085635e+00+i*2.447099e-02  1.092873e+00+i*2.950686e-02  1.101137e+00+i*3.618934e-02  1.110462e+00+i*4.521747e-02
  4  1.075560e+00+i*2.607524e-02  1.080853e+00+i*3.063731e-02  1.086686e+00+i*3.642205e-02  1.093013e+00+i*4.384283e-02  1.099666e+00+i*5.345062e-02
  5  1.071279e+00+i*3.064152e-02  1.075522e+00+i*3.559918e-02  1.079985e+00+i*4.172443e-02  1.084526e+00+i*4.933578e-02  1.088877e+00+i*5.881666e-02
  6  1.066708e+00+i*3.430290e-02  1.069960e+00+i*3.940455e-02  1.073184e+00+i*4.555214e-02  1.076197e+00+i*5.296734e-02  1.078701e+00+i*6.188566e-02
  7  1.062046e+00+i*3.711662e-02  1.064412e+00+i*4.216911e-02  1.066578e+00+i*4.811513e-02  1.068350e+00+i*5.509292e-02  1.069450e+00+i*6.322454e-02
  8  1.057448e+00+i*3.917375e-02  1.059054e+00+i*4.404105e-02  1.060352e+00+i*4.964407e-02  1.061159e+00+i*5.605661e-02  1.061235e+00+i*6.332208e-02
  9  1.053020e+00+i*4.058040e-02  1.053997e+00+i*4.517557e-02  1.054606e+00+i*5.035874e-02  1.054687e+00+i*5.615779e-02  1.054046e+00+i*6.256630e-02
 10  1.048834e+00+i*4.144439e-02  1.049303e+00+i*4.571913e-02  1.049380e+00+i*5.045196e-02  1.048933e+00+i*5.564043e-02  1.047807e+00+i*6.124942e-02
 11  1.044928e+00+i*4.186683e-02  1.044997e+00+i*4.580119e-02  1.044674e+00+i*5.008416e-02  1.043853e+00+i*5.469478e-02  1.042416e+00+i*5.958359e-02
 12  1.041320e+00+i*4.193773e-02  1.041080e+00+i*4.553131e-02  1.040464e+00+i*4.938399e-02  1.039389e+00+i*5.346469e-02  1.037766e+00+i*5.771874e-02
 13  1.038010e+00+i*4.173433e-02  1.037537e+00+i*4.499932e-02  1.036713e+00+i*4.845189e-02  1.035473e+00+i*5.205641e-02  1.033752e+00+i*5.575846e-02
 14  1.034989e+00+i*4.132112e-02  1.034344e+00+i*4.427721e-02  1.033378e+00+i*4.736467e-02  1.032040e+00+i*5.054696e-02  1.030282e+00+i*5.377284e-02
 15  1.032241e+00+i*4.075091e-02  1.031474e+00+i*4.342161e-02  1.030414e+00+i*4.618016e-02  1.029026e+00+i*4.899121e-02  1.027274e+00+i*5.180814e-02
 16  1.029747e+00+i*4.006622e-02  1.028895e+00+i*4.247646e-02  1.027781e+00+i*4.494124e-02  1.026377e+00+i*4.742760e-02  1.024658e+00+i*4.989400e-02
 17  1.027486e+00+i*3.930087e-02  1.026579e+00+i*4.147542e-02  1.025438e+00+i*4.367931e-02  1.024043e+00+i*4.588251e-02  1.022375e+00+i*4.804852e-02
 18  1.025437e+00+i*3.848144e-02  1.024499e+00+i*4.044397e-02  1.023352e+00+i*4.241699e-02  1.021981e+00+i*4.437356e-02  1.020375e+00+i*4.628190e-02
 19  1.023580e+00+i*3.762865e-02  1.022627e+00+i*3.940119e-02  1.021489e+00+i*4.117033e-02  1.020155e+00+i*4.291210e-02  1.018617e+00+i*4.459902e-02
 20  1.021896e+00+i*3.675846e-02  1.020942e+00+i*3.836120e-02  1.019824e+00+i*3.995041e-02  1.018533e+00+i*4.150499e-02  1.017066e+00+i*4.300118e-02
  y / x             -9                           -8                           -7                           -6                           -5
  0  1.152759e+00+i*3.489330e-03  1.181848e+00+i*8.431095e-03  1.222408e+00+i*2.005333e-02  1.278884e+00+i*4.672338e-02  1.353831e+00+i*1.058394e-01
  1  1.146232e+00+i*2.637602e-02  1.169677e+00+i*3.884079e-02  1.199049e+00+i*6.021873e-02  1.233798e+00+i*9.733053e-02  1.268723e+00+i*1.608255e-01
  2  1.134679e+00+i*4.457874e-02  1.151385e+00+i*6.081350e-02  1.169639e+00+i*8.533460e-02  1.186778e+00+i*1.221620e-01  1.196351e+00+i*1.756458e-01
  3  1.120694e+00+i*5.759502e-02  1.131255e+00+i*7.470077e-02  1.140733e+00+i*9.825884e-02  1.146266e+00+i*1.300051e-01  1.142853e+00+i*1.706715e-01
  4  1.106249e+00+i*6.594813e-02  1.111968e+00+i*8.215578e-02  1.115404e+00+i*1.028610e-01  1.114273e+00+i*1.284402e-01  1.105376e+00+i*1.581339e-01
  5  1.092564e+00+i*7.059178e-02  1.094817e+00+i*8.505484e-02  1.094475e+00+i*1.024110e-01  1.089952e+00+i*1.223967e-01  1.079407e+00+i*1.438790e-01
  6  1.080246e+00+i*7.251962e-02  1.080188e+00+i*8.498655e-02  1.077672e+00+i*9.918770e-02  1.071684e+00+i*1.146377e-01  1.061236e+00+i*1.302800e-01
  7  1.069494e+00+i*7.258002e-02  1.067987e+00+i*8.312034e-02  1.064339e+00+i*9.461831e-02  1.057935e+00+i*1.065680e-01  1.048279e+00+i*1.181160e-01
  8  1.060276e+00+i*7.142463e-02  1.057920e+00+i*8.024960e-02  1.053778e+00+i*8.953705e-02  1.047493e+00+i*9.883986e-02  1.038838e+00+i*1.075084e-01
  9  1.052450e+00+i*6.952255e-02  1.049645e+00+i*7.688449e-02  1.045382e+00+i*8.440509e-02  1.039464e+00+i*9.171665e-02  1.031806e+00+i*9.833700e-02
 10  1.045832e+00+i*6.719704e-02  1.042834e+00+i*7.334001e-02  1.038659e+00+i*7.946209e-02  1.033205e+00+i*8.527126e-02  1.026459e+00+i*9.041253e-02
 11  1.040241e+00+i*6.466378e-02  1.037209e+00+i*6.980337e-02  1.033231e+00+i*7.482090e-02  1.028260e+00+i*7.948827e-02  1.022317e+00+i*8.354394e-02
 12  1.035508e+00+i*6.206271e-02  1.032539e+00+i*6.638071e-02  1.028808e+00+i*7.052392e-02  1.024300e+00+i*7.431533e-02  1.019052e+00+i*7.756114e-02
 13  1.031490e+00+i*5.948197e-02  1.028638e+00+i*6.312794e-02  1.025171e+00+i*6.657575e-02  1.021090e+00+i*6.968811e-02  1.016439e+00+i*7.232025e-02
 14  1.028065e+00+i*5.697507e-02  1.025359e+00+i*6.007042e-02  1.022152e+00+i*6.296166e-02  1.018458e+00+i*6.554199e-02  1.014319e+00+i*6.770224e-02
 15  1.025132e+00+i*5.457264e-02  1.022583e+00+i*5.721535e-02  1.019626e+00+i*5.965799e-02  1.016277e+00+i*6.181710e-02  1.012577e+00+i*6.360948e-02
 16  1.022608e+00+i*5.229047e-02  1.020218e+00+i*5.455946e-02  1.017494e+00+i*5.663786e-02  1.014452e+00+i*5.846006e-02  1.011130e+00+i*5.996204e-02
 17  1.020426e+00+i*5.013480e-02  1.018192e+00+i*5.209370e-02  1.015681e+00+i*5.387418e-02  1.012912e+00+i*5.542432e-02  1.009915e+00+i*5.669434e-02
 18  1.018530e+00+i*4.810593e-02  1.016444e+00+i*4.980617e-02  1.014129e+00+i*5.134120e-02  1.011600e+00+i*5.266963e-02  1.008887e+00+i*5.375239e-02
 19  1.016874e+00+i*4.620052e-02  1.014929e+00+i*4.768385e-02  1.012790e+00+i*4.901522e-02  1.010476e+00+i*5.016141e-02  1.008009e+00+i*5.109149e-02
 20  1.015422e+00+i*4.441312e-02  1.013607e+00+i*4.571355e-02  1.011629e+00+i*4.687480e-02  1.009505e+00+i*4.787001e-02  1.007254e+00+i*4.867446e-02


 250                         EXPONENTIAL INTEGRAl AND RELATED FUNCTIONS
.
.                             EXPONENTIAL INTEGRAL FOR COMPLEX ARGUMENTS
.                                             Z*EXP(Z)*E1(Z)

  y / x             -4                           -3                           -2                           -1                            0
  0  1.438208e+00+i*2.301611e-01  1.483729e+00+i*4.692321e-01  1.340965e+00+i*8.503367e-01  6.971749e-01+i*1.155727e+00  0.000000e+00+i*0.000000e+00
  1  1.287244e+00+i*2.637045e-01  1.251069e+00+i*4.104125e-01  1.098808e+00+i*5.619160e-01  8.134864e-01+i*5.786973e-01  6.214496e-01+i*3.433780e-01
  2  1.185758e+00+i*2.473563e-01  1.136171e+00+i*3.284389e-01  1.032990e+00+i*3.884283e-01  8.964185e-01+i*3.788375e-01  7.980420e-01+i*2.890906e-01
  3  1.123282e+00+i*2.178354e-01  1.080316e+00+i*2.628143e-01  1.013205e+00+i*2.893658e-01  9.362826e-01+i*2.809059e-01  8.758731e-01+i*2.376646e-01
  4  1.085153e+00+i*1.890034e-01  1.051401e+00+i*2.151178e-01  1.006122e+00+i*2.283987e-01  9.574456e-01+i*2.226117e-01  9.167703e-01+i*1.987126e-01
  5  1.061263e+00+i*1.644661e-01  1.035185e+00+i*1.804866e-01  1.003172e+00+i*1.878565e-01  9.698086e-01+i*1.839629e-01  9.407139e-01+i*1.694811e-01
  6  1.045719e+00+i*1.443912e-01  1.025396e+00+i*1.547459e-01  1.001788e+00+i*1.591888e-01  9.775822e-01+i*1.565106e-01  9.558333e-01+i*1.471289e-01
  7  1.035205e+00+i*1.280734e-01  1.019109e+00+i*1.350792e-01  1.001077e+00+i*1.379386e-01  9.827555e-01+i*1.360424e-01  9.659370e-01+i*1.296464e-01
  8  1.027834e+00+i*1.147320e-01  1.014861e+00+i*1.196603e-01  1.000684e+00+i*1.215989e-01  9.863563e-01+i*1.202182e-01  9.729943e-01+i*1.156778e-01
  9  1.022501e+00+i*1.037111e-01  1.011869e+00+i*1.072943e-01  1.000453e+00+i*1.086649e-01  9.889550e-01+i*1.076338e-01  9.781027e-01+i*1.043030e-01
 10  1.018534e+00+i*9.450185e-02  1.009688e+00+i*9.718095e-02  1.000312e+00+i*9.818387e-02  9.908873e-01+i*9.739625e-02  9.819104e-01+i*9.488539e-02
 11  1.015513e+00+i*8.671835e-02  1.008052e+00+i*8.876989e-02  1.000221e+00+i*8.952494e-02  9.923606e-01+i*8.891135e-02  9.848192e-01+i*8.697465e-02
 12  1.013163e+00+i*8.006921e-02  1.006795e+00+i*8.167276e-02  1.000160e+00+i*8.225494e-02  9.935080e-01+i*8.176857e-02  9.870883e-01+i*8.024517e-02
 13  1.011303e+00+i*7.433307e-02  1.005809e+00+i*7.560891e-02  1.000119e+00+i*7.606702e-02  9.944180e-01+i*7.567555e-02  9.888906e-01+i*7.445680e-02
 14  1.009806e+00+i*6.934018e-02  1.005022e+00+i*7.037112e-02  1.000090e+00+i*7.073795e-02  9.951514e-01+i*7.041856e-02  9.903445e-01+i*6.942905e-02
 15  1.008585e+00+i*6.495894e-02  1.004384e+00+i*6.580341e-02  1.000070e+00+i*6.610163e-02  9.957505e-01+i*6.583787e-02  9.915336e-01+i*6.502399e-02
 16  1.007577e+00+i*6.108619e-02  1.003859e+00+i*6.178629e-02  1.000055e+00+i*6.203197e-02  9.962461e-01+i*6.181179e-02  9.925179e-01+i*6.113463e-02
 17  1.006735e+00+i*5.764015e-02  1.003423e+00+i*5.822682e-02  1.000043e+00+i*5.843158e-02  9.966606e-01+i*5.824600e-02  9.933416e-01+i*5.767679e-02
 18  1.006025e+00+i*5.455533e-02  1.003057e+00+i*5.505167e-02  1.000035e+00+i*5.522412e-02  9.970105e-01+i*5.506631e-02  9.940375e-01+i*5.458342e-02
 19  1.005420e+00+i*5.177870e-02  1.002747e+00+i*5.220227e-02  1.000028e+00+i*5.234885e-02  9.973086e-01+i*5.221359e-02  9.946306e-01+i*5.180052e-02
 20  1.004902e+00+i*4.926699e-02  1.002481e+00+i*4.963127e-02  1.000023e+00+i*4.975691e-02  9.975645e-01+i*4.964014e-02  9.951401e-01+i*4.928413e-02
  y / x              1                            2                            3                            4                            5
  0  5.963474e-01+i*0.000000e+00  7.226572e-01+i*0.000000e+00  7.862512e-01+i*0.000000e+00  8.253826e-01+i*0.000000e+00  8.521109e-01+i*0.000000e+00
  1  6.733212e-01+i*1.478639e-01  7.470123e-01+i*7.566082e-02  7.970360e-01+i*4.568600e-02  8.311262e-01+i*3.061932e-02  8.555441e-01+i*2.198492e-02
  2  7.775143e-01+i*1.865704e-01  7.969645e-01+i*1.182282e-01  8.230547e-01+i*7.875296e-02  8.460965e-01+i*5.549408e-02  8.648796e-01+i*4.099909e-02
  3  8.474683e-01+i*1.812256e-01  8.443610e-01+i*1.322519e-01  8.531756e-01+i*9.665867e-02  8.655213e-01+i*7.217976e-02  8.778601e-01+i*5.534102e-02
  4  8.914598e-01+i*1.652071e-01  8.810360e-01+i*1.316856e-01  8.805838e-01+i*1.034034e-01  8.853082e-01+i*8.140837e-02  8.921432e-01+i*6.482487e-02
  5  9.198259e-01+i*1.482714e-01  9.078731e-01+i*1.251357e-01  9.031515e-01+i*1.035771e-01  9.032310e-01+i*8.518728e-02  9.060575e-01+i*7.020916e-02
  6  9.388274e-01+i*1.329855e-01  9.273841e-01+i*1.166564e-01  9.210060e-01+i*1.003572e-01  9.185274e-01+i*8.545965e-02  9.187081e-01+i*7.254384e-02
  7  9.520321e-01+i*1.198069e-01  9.417224e-01+i*1.079899e-01  9.349580e-01+i*9.559814e-02  9.312090e-01+i*8.366602e-02  9.297649e-01+i*7.279171e-02
  8  9.615119e-01+i*1.085892e-01  9.524347e-01+i*9.982963e-02  9.458679e-01+i*9.030246e-02  9.415938e-01+i*8.075486e-02  9.392208e-01+i*7.169964e-02
  9  9.685124e-01+i*9.904509e-02  9.605815e-01+i*9.240757e-02  9.544569e-01+i*8.498561e-02  9.500717e-01+i*7.731270e-02  9.472192e-01+i*6.979846e-02
 10  9.738096e-01+i*9.088790e-02  9.668852e-01+i*8.575822e-02  9.612831e-01+i*7.989763e-02  9.570069e-01+i*7.368796e-02  9.539552e-01+i*6.744669e-02
 11  9.779038e-01+i*8.387054e-02  9.718424e-01+i*7.983585e-02  9.667655e-01+i*7.514726e-02  9.627077e-01+i*7.008034e-02  9.596264e-01+i*6.487834e-02
 12  9.811269e-01+i*7.779040e-02  9.757989e-01+i*7.456687e-02  9.712156e-01+i*7.076943e-02  9.674232e-01+i*6.659917e-02  9.644116e-01+i*6.224206e-02
 13  9.837057e-01+i*7.248439e-02  9.789995e-01+i*6.987272e-02  9.748652e-01+i*6.676167e-02  9.713510e-01+i*6.329980e-02  9.684639e-01+i*5.962967e-02
 14  9.857986e-01+i*6.782190e-02  9.816205e-01+i*6.567935e-02  9.778877e-01+i*6.310360e-02  9.746461e-01+i*6.020579e-02  9.719112e-01+i*5.709578e-02
 15  9.875187e-01+i*6.369816e-02  9.837908e-01+i*6.192064e-02  9.804140e-01+i*5.976732e-02  9.774301e-01+i*5.732235e-02  9.748584e-01+i*5.467101e-02
 16  9.889486e-01+i*6.002876e-02  9.856060e-01+i*5.853909e-02  9.825436e-01+i*5.672283e-02  9.797985e-01+i*5.464444e-02  9.773910e-01+i*5.237061e-02
 17  9.901491e-01+i*5.674524e-02  9.871381e-01+i*5.548534e-02  9.843531e-01+i*5.394080e-02  9.818266e-01+i*5.216158e-02  9.795786e-01+i*5.020021e-02
 18  9.911665e-01+i*5.379171e-02  9.884422e-01+i*5.271726e-02  9.859021e-01+i*5.139390e-02  9.835744e-01+i*4.986072e-02  9.814779e-01+i*4.815948e-02
 19  9.920356e-01+i*5.112223e-02  9.895607e-01+i*5.019898e-02  9.872371e-01+i*4.905726e-02  9.850894e-01+i*4.772796e-02  9.831349e-01+i*4.624451e-02
 20  9.927838e-01+i*4.869875e-02  9.905266e-01+i*4.789993e-02  9.883949e-01+i*4.690860e-02  9.864099e-01+i*4.574944e-02  9.845874e-01+i*4.444941e-02
  y / x              6                            7                            8                            9                           10
  0  8.716058e-01+i*0.000000e+00  8.864877e-01+i*0.000000e+00  8.982371e-01+i*0.000000e+00  9.077576e-01+i*0.000000e+00  9.156333e-01+i*0.000000e+00
  1  8.738266e-01+i*1.657045e-02  8.880094e-01+i*1.294731e-02  8.993265e-01+i*1.040125e-02  9.085650e-01+i*8.542458e-03  9.162487e-01+i*7.143197e-03
  2  8.800226e-01+i*3.145396e-02  8.923265e-01+i*2.486638e-02  9.024530e-01+i*2.014019e-02  9.109012e-01+i*1.663901e-02  9.180401e-01+i*1.397515e-02
  3  8.890290e-01+i*4.351690e-02  8.987929e-01+i*3.499514e-02  9.072361e-01+i*2.869295e-02  9.145310e-01+i*2.392055e-02  9.208560e-01+i*2.022951e-02
  4  8.994841e-01+i*5.237988e-02  9.065911e-01+i*4.296734e-02  9.131673e-01+i*3.575449e-02  9.191270e-01+i*3.014473e-02  9.244789e-01+i*2.571647e-02
  5  9.102418e-01+i*5.825896e-02  9.149522e-01+i*4.877954e-02  9.197292e-01+i*4.124174e-02  9.243364e-01+i*3.520748e-02  9.286641e-01+i*3.033439e-02
  6  9.205338e-01+i*6.167553e-02  9.232825e-01+i*5.266669e-02  9.264810e-01+i*4.524182e-02  9.298362e-01+i*3.912266e-02  9.331750e-01+i*3.406280e-02
  7  9.299448e-01+i*6.322038e-02  9.311926e-01+i*5.497073e-02  9.330955e-01+i*4.794215e-02  9.353646e-01+i*4.198566e-02  9.378069e-01+i*3.694383e-02
  8  9.383126e-01+i*6.342479e-02  9.384687e-01+i*5.604653e-02  9.393594e-01+i*4.956954e-02  9.407312e-01+i*4.393612e-02  9.423984e-01+i*3.905997e-02
  9  9.456287e-01+i*6.271438e-02  9.450233e-01+i*5.621112e-02  9.451535e-01+i*5.034896e-02  9.458115e-01+i*4.512821e-02  9.468331e-01+i*4.051390e-02
 10  9.519648e-01+i*6.140810e-02  9.508496e-01+i*5.572482e-02  9.504272e-01+i*5.048129e-02  9.505347e-01+i*4.571108e-02  9.510346e-01+i*4.141323e-02
 11  9.574268e-01+i*5.973504e-02  9.559865e-01+i*5.479032e-02  9.551756e-01+i*5.013466e-02  9.548699e-01+i*4.581814e-02  9.549587e-01+i*4.186055e-02
 12  9.621284e-01+i*5.785504e-02  9.604951e-01+i*5.355999e-02  9.594210e-01+i*4.944388e-02  9.588136e-01+i*4.556283e-02  9.585857e-01+i*4.194796e-02
 13  9.661780e-01+i*5.587714e-02  9.644445e-01+i*5.214554e-02  9.632008e-01+i*4.851388e-02  9.623793e-01+i*4.503822e-02  9.619129e-01+i*4.175474e-02
 14  9.696733e-01+i*5.387419e-02  9.679030e-01+i*5.062727e-02  9.665585e-01+i*4.742454e-02  9.655908e-01+i*4.431875e-02  9.649490e-01+i*4.134718e-02
 15  9.726987e-01+i*5.189373e-02  9.709347e-01+i*4.906194e-02  9.695390e-01+i*4.623569e-02  9.684768e-01+i*4.346284e-02  9.677098e-01+i*4.077949e-02
 16  9.753264e-01+i*4.996582e-02  9.735970e-01+i*4.748902e-02  9.721854e-01+i*4.499147e-02  9.710674e-01+i*4.251567e-02  9.702145e-01+i*4.009527e-02
 17  9.776172e-01+i*4.810861e-02  9.759404e-01+i*4.593538e-02  9.745377e-01+i*4.372399e-02  9.733925e-01+i*4.151169e-02  9.724841e-01+i*3.932908e-02
 18  9.796222e-01+i*4.633214e-02  9.780086e-01+i*4.441884e-02  9.766318e-01+i*4.245632e-02  9.754805e-01+i*4.047692e-02  9.745398e-01+i*3.850806e-02
 19  9.813837e-01+i*4.464105e-02  9.798394e-01+i*4.295077e-02  9.784996e-01+i*4.120470e-02  9.773573e-01+i*3.943076e-02  9.764021e-01+i*3.765328e-02
 20  9.829375e-01+i*4.303640e-02  9.814648e-01+i*4.153798e-02  9.801692e-01+i*3.998033e-02  9.790468e-01+i*3.838752e-02  9.780902e-01+i*3.678094e-02

 .                  EXPONENTIAL INTEGRAl AND RELATED FUNCTIONS                 251
 .
 .                  EXPONENTIAL INTEGRAL FOR COMPLEX ARGUMENTS      Table 5.6
 .                            Z*EXP(Z)*E1(Z)

  y / x             11                           12                           13                           14                           15
  0  9.222596e-01+i*0.000000e+00  9.279136e-01+i*0.000000e+00  9.327959e-01+i*0.000000e+00  9.370553e-01+i*0.000000e+00  9.408042e-01+i*0.000000e+00
  1  9.227395e-01+i*6.063064e-03  9.282952e-01+i*5.211591e-03  9.331045e-01+i*4.528313e-03  9.373084e-01+i*3.971556e-03  9.410144e-01+i*3.511823e-03
  2  9.241432e-01+i*1.190225e-02  9.294157e-01+i*1.025800e-02  9.340131e-01+i*8.932070e-03  9.380553e-01+i*7.847404e-03  9.416359e-01+i*6.948885e-03
  3  9.263697e-01+i*1.732110e-02  9.312053e-01+i*1.499143e-02  9.354725e-01+i*1.309811e-02  9.392607e-01+i*1.153952e-02  9.426427e-01+i*1.024173e-02
  4  9.292701e-01+i*2.217111e-02  9.335600e-01+i*1.929513e-02  9.374082e-01+i*1.693392e-02  9.408699e-01+i*1.497395e-02  9.439942e-01+i*1.333071e-02
  5  9.326718e-01+i*2.636133e-02  9.363555e-01+i*2.309076e-02  9.397293e-01+i*2.037321e-02  9.428156e-01+i*1.809503e-02  9.456395e-01+i*1.616919e-02
  6  9.364004e-01+i*2.985697e-02  9.394622e-01+i*2.633925e-02  9.423383e-01+i*2.337750e-02  9.450235e-01+i*2.086702e-02  9.475216e-01+i*1.872496e-02
  7  9.402971e-01+i*3.267012e-02  9.427571e-01+i*2.903635e-02  9.451399e-01+i*2.593412e-02  9.474193e-01+i*2.327343e-02  9.495821e-01+i*2.098029e-02
  8  9.442291e-01+i*3.484671e-02  9.461324e-01+i*3.120500e-02  9.480470e-01+i*2.805186e-02  9.499329e-01+i*2.531451e-02  9.517646e-01+i*2.293048e-02
  9  9.480934e-01+i*3.645322e-02  9.494999e-01+i*3.288683e-02  9.509854e-01+i*2.975554e-02  9.525023e-01+i*2.700388e-02  9.540175e-01+i*2.458175e-02
 10  9.518159e-01+i*3.756554e-02  9.527915e-01+i*3.413423e-02  9.538946e-01+i*3.108052e-02  9.550752e-01+i*2.836471e-02  9.562961e-01+i*2.594869e-02
 11  9.553474e-01+i*3.826070e-02  9.559582e-01+i*3.500395e-02  9.567287e-01+i*3.206787e-02  9.576095e-01+i*2.942626e-02  9.585627e-01+i*2.705173e-02
 12  9.586593e-01+i*3.861152e-02  9.589675e-01+i*3.555239e-02  9.594541e-01+i*3.276061e-02  9.600731e-01+i*3.022082e-02  9.607874e-01+i*2.791481e-02
 13  9.617386e-01+i*3.868357e-02  9.618002e-01+i*3.583260e-02  9.620487e-01+i*3.320089e-02  9.624426e-01+i*3.078145e-02  9.629472e-01+i*2.856350e-02
 14  9.645833e-01+i*3.853397e-02  9.644472e-01+i*3.589257e-02  9.644990e-01+i*3.342826e-02  9.647023e-01+i*3.114025e-02  9.650256e-01+i*2.902355e-02
 15  9.671991e-01+i*3.821118e-02  9.669071e-01+i*3.577447e-02  9.667988e-01+i*3.347864e-02  9.668428e-01+i*3.132729e-02  9.670112e-01+i*2.931980e-02
 16  9.695966e-01+i*3.775559e-02  9.691837e-01+i*3.551456e-02  9.689470e-01+i*3.338384e-02  9.688595e-01+i*3.136997e-02  9.688972e-01+i*2.947551e-02
 17  9.717894e-01+i*3.720030e-02  9.712847e-01+i*3.514349e-02  9.709463e-01+i*3.317151e-02  9.707517e-01+i*3.129276e-02  9.706801e-01+i*2.951196e-02
 18  9.737924e-01+i*3.657216e-02  9.732195e-01+i*3.468684e-02  9.728020e-01+i*3.286534e-02  9.725212e-01+i*3.111710e-02  9.723593e-01+i*2.944826e-02
 19  9.756209e-01+i*3.589271e-02  9.749990e-01+i*3.416572e-02  9.745211e-01+i*3.248539e-02  9.741719e-01+i*3.086159e-02  9.739364e-01+i*2.930131e-02
 20  9.772898e-01+i*3.517907e-02  9.766344e-01+i*3.359739e-02  9.761116e-01+i*3.204846e-02  9.757090e-01+i*3.054215e-02  9.754141e-01+i*2.908589e-02
  y / x             16                           17                           18                           19                           20
  0  9.441297e-01+i*0.000000e+00  9.470998e-01+i*0.000000e+00  9.497690e-01+i*0.000000e+00  9.521808e-01+i*0.000000e+00  9.543709e-01+i*0.000000e+00
  1  9.443062e-01+i*3.127757e-03  9.472495e-01+i*2.803583e-03  9.498970e-01+i*2.527437e-03  9.522912e-01+i*2.290262e-03  9.544667e-01+i*2.085040e-03
  2  9.448289e-01+i*6.196262e-03  9.476933e-01+i*5.559594e-03  9.502771e-01+i*5.016238e-03  9.526191e-01+i*4.548817e-03  9.547517e-01+i*4.143816e-03
  3  9.456783e-01+i*9.150002e-03  9.484165e-01+i*8.223154e-03  9.508977e-01+i*7.429737e-03  9.531557e-01+i*6.745425e-03  9.552187e-01+i*6.151167e-03
  4  9.468238e-01+i*1.194046e-02  9.493954e-01+i*1.075445e-02  9.517406e-01+i*9.734998e-03  9.538865e-01+i*8.852629e-03  9.558563e-01+i*8.084041e-03
  5  9.482263e-01+i*1.452847e-02  9.505999e-01+i*1.312053e-02  9.527822e-01+i*1.190419e-02  9.547929e-01+i*1.084683e-02  9.566496e-01+i*9.922327e-03
  6  9.498418e-01+i*1.688556e-02  9.519954e-01+i*1.529640e-02  9.539950e-01+i*1.391549e-02  9.558529e-01+i*1.270899e-02  9.575809e-01+i*1.164944e-02
  7  9.516239e-01+i*1.899412e-02  9.535450e-01+i*1.726537e-02  9.553494e-01+i*1.575344e-02  9.570425e-01+i*1.442503e-02  9.586307e-01+i*1.325266e-02
  8  9.535270e-01+i*2.084675e-02  9.552117e-01+i*1.901870e-02  9.568153e-01+i*1.740886e-02  9.583371e-01+i*1.598581e-02  9.597786e-01+i*1.472319e-02
  9  9.555085e-01+i*2.244490e-02  9.569601e-01+i*2.055483e-02  9.583632e-01+i*1.887832e-02  9.597121e-01+i*1.738685e-02  9.610042e-01+i*1.605601e-02
 10  9.575301e-01+i*2.379715e-02  9.587578e-01+i*2.187820e-02  9.599657e-01+i*2.016343e-02  9.611444e-01+i*1.862785e-02  9.622877e-01+i*1.724957e-02
 11  9.595589e-01+i*2.491742e-02  9.605761e-01+i*2.299793e-02  9.615980e-01+i*2.126988e-02  9.626124e-01+i*1.971201e-02  9.636107e-01+i*1.830531e-02
 12  9.615675e-01+i*2.582319e-02  9.623905e-01+i*2.392662e-02  9.632382e-01+i*2.220650e-02  9.640970e-01+i*2.064537e-02  9.649563e-01+i*1.922715e-02
 13  9.635343e-01+i*2.653403e-02  9.641807e-01+i*2.467905e-02  9.648680e-01+i*2.298435e-02  9.655815e-01+i*2.143606e-02  9.663095e-01+i*2.002098e-02
 14  9.654425e-01+i*2.707037e-02  9.659307e-01+i*2.527127e-02  9.664722e-01+i*2.361591e-02  9.670518e-01+i*2.209368e-02  9.676576e-01+i*2.069410e-02
 15  9.672799e-01+i*2.745250e-02  9.676281e-01+i*2.571970e-02  9.680385e-01+i*2.411435e-02  9.684964e-01+i*2.262874e-02  9.689896e-01+i*2.125479e-02
 16  9.690382e-01+i*2.769994e-02  9.692639e-01+i*2.604054e-02  9.695578e-01+i*2.449303e-02  9.699060e-01+i*2.305212e-02  9.702966e-01+i*2.171191e-02
 17  9.707124e-01+i*2.783095e-02  9.708318e-01+i*2.624932e-02  9.710231e-01+i*2.476501e-02  9.712734e-01+i*2.337475e-02  9.715713e-01+i*2.207451e-02
 18  9.723000e-01+i*2.786231e-02  9.723280e-01+i*2.636060e-02  9.724299e-01+i*2.494279e-02  9.725936e-01+i*2.360730e-02  9.728084e-01+i*2.235162e-02
 19  9.738003e-01+i*2.780916e-02  9.737506e-01+i*2.638776e-02  9.737750e-01+i*2.503810e-02  9.738627e-01+i*2.375992e-02  9.740038e-01+i*2.255199e-02
 20  9.752147e-01+i*2.768500e-02  9.750992e-01+i*2.634296e-02  9.750572e-01+i*2.506177e-02  9.750786e-01+i*2.384219e-02  9.751545e-01+i*2.268399e-02

.                               EXPONENTIAL INTEGRAL FOR SMALL COMPLEX ARGUMENTS
.                                                EXP(Z)*E1(Z)

 y / x             -4.0                           -3.5                           -3.0                           -2.5                           -2.0
 0.0 -3.595520e-01-i*5.754028e-02 -4.205093e-01-i*9.486788e-02 -4.945764e-01-i*1.564107e-01 -5.806501e-01-i*2.578776e-01 -6.704827e-01-i*4.251683e-01
 0.2 -3.471790e-01-i*7.828255e-02 -4.005965e-01-i*1.199269e-01 -4.624933e-01-i*1.855729e-01 -5.289874e-01-i*2.890090e-01 -5.875585e-01-i*4.512253e-01
 0.4 -3.333728e-01-i*9.664773e-02 -3.792781e-01-i*1.412209e-01 -4.295541e-01-i*2.087998e-01 -4.783030e-01-i*3.108836e-01 -5.105427e-01-i*4.631926e-01
 0.6 -3.185556e-01-i*1.126328e-01 -3.572020e-01-i*1.588904e-01 -3.967301e-01-i*2.265753e-01 -4.299777e-01-i*3.247735e-01 -4.411279e-01-i*4.641632e-01
 0.8 -3.031094e-01-i*1.263012e-01 -3.349227e-01-i*1.731693e-01 -3.647850e-01-i*2.395000e-01 -3.849405e-01-i*3.320471e-01 -3.800126e-01-i*4.570879e-01
 1.0 -2.873689e-01-i*1.377684e-01 -3.128943e-01-i*1.843550e-01 -3.342796e-01-i*2.482307e-01 -3.437194e-01-i*3.340435e-01 -3.271401e-01-i*4.445281e-01

 .                                                E1(Z)+LOG(Z)

 y / x             -2.0                           -1.5                           -1.0                            0.5                            0.0
 0.0 -4.261087e+00+i*0.000000e+00 -2.895820e+00+i*0.000000e+00 -1.895118e+00+i*0.000000e+00 -1.147367e+00+i*0.000000e+00 -5.77216 e+00+i*0.000000e+00
 0.2 -4.219228e+00+i*6.367788e-01 -2.867070e+00+i*4.628041e-01 -1.875155e+00+i*3.426999e-01 -1.133341e+00+i*2.588397e-01 -5.672323e-01+i*1.995561e-01
 0.4 -4.094686e+00+i*1.260867e+00 -2.781497e+00+i*9.171266e-01 -1.815717e+00+i*6.796906e-01 -1.091560e+00+i*5.138060e-01 -5.374814e-01+i*3.964615e-01
 0.6 -3.890531e+00+i*1.859922e+00 -2.641121e+00+i*1.354712e+00 -1.718135e+00+i*1.005410e+00 -1.022911e+00+i*7.611217e-01 -4.885549e-01+i*5.881288e-01
 0.8 -3.611783e+00+i*2.422284e+00 -2.449241e+00+i*1.767749e+00 -1.584591e+00+i*1.314586e+00 -9.288420e-01+i*9.971998e-01 -4.214222e-01+i*7.720958e-01
 1.0 -3.265262e+00+i*2.937296e+00 -2.210344e+00+i*2.149077e+00 -1.418052e+00+i*1.602372e+00 -8.113274e-01+i*1.218731e+00 -3.374039e-01+i*9.460831e-01
 y / x              0.5                            1.0                            1.5                            2.0                            2.5
 0.0 -1.333736e-01+i*0.000000e+00  2.193839e-01+i*0.000000e+00  5.054847e-01+i*0.000000e+00  7.420477e-01+i*0.000000e+00  9.412056e-01+i*0.000000e+00
 0.2 -1.261685e-01+i*1.570812e-01  2.246612e-01+i*1.262102e-01  5.094099e-01+i*1.034318e-01  7.450141e-01+i*8.635880e-02  9.434838e-01+i*7.335542e-02
 0.4 -1.046871e-01+i*3.123312e-01  2.404021e-01+i*2.511426e-01  5.211237e-01+i*2.059620e-01  7.538706e-01+i*1.720741e-01  9.502886e-01+i*1.462458e-01
 0.6 -6.932772e-02+i*4.639611e-01  2.663367e-01+i*3.735472e-01  5.404416e-01+i*3.067078e-01  7.684901e-01+i*2.565150e-01  9.615317e-01+i*2.182147e-01
 0.8 -2.074303e-02+i*6.102641e-01  3.020225e-01+i*4.922290e-01  5.670609e-01+i*4.048231e-01  7.886640e-01+i*3.390751e-01  9.770680e-01+i*2.888224e-01
 1.0  4.017706e-02+i*7.496549e-01  3.468552e-01+i*6.060736e-01  6.005685e-01+i*4.995155e-01  8.141071e-01+i*4.191846e-01  9.966987e-01+i*3.576536e-01
*/

/*!
 Function \f$Gamma(z) = \int_0^{\infty} t^{z-1} exp(-t) dt for Re(z) > 0\f$
-         Gamma(1-z) = Pi / ( sin(Pi z) Gamma(z) )
-         Gamma(z+1) = z Gamma(z)
-         Gamma(n+1) = n! for integer n > 0
*/
real_t gammaFunction(int_t n); //!< return Gamma(n) = (n-1)!

real_t gammaFunction(real_t); //!< return \f$\int_0^t dt  t^{x-1} \exp(-t)\f$ for x > 0

complex_t gammaFunction(const complex_t&); //!< return \f$\int_0^t dt  t^{x-1} \exp(-t)\f$ for x > 0

real_t logGamma(real_t); //!< return Log(Gamma(x))

complex_t logGamma1(const complex_t&); //!< return Log(Gamma(z))
complex_t logGamma(const complex_t&); //!< Paul Godfrey's Lanczos implementation of the Gamma function

/*
--------------------------------------------------------------------------------
 Function DiGamma(z) = d/dz(Log(Gamma(z)))
--------------------------------------------------------------------------------
*/
//@{
/*! return \f$-gamma + \sum_1^{n-1} 1/n\f$
    where gamma is Euler-Mascheroni constant
*/
real_t diGamma(int_t n);
real_t diGamma(real_t);
complex_t diGamma(const complex_t&);
//@}

/*
--------------------------------------------------------------------------------
 For tests
--------------------------------------------------------------------------------
*/
void gammaTest(std::ostream&);

//--------------------------------------------------------------------------------
// OrthogonalPolynomials: some orthogonal polynomials on [-1 , 1]
//
// - ChebyshevPolynomials  (of the first kind) on [-1, 1] up to order n
//  .  Tn = cos( n acos(x))
//  . Recurrence relation:
//  .   T_0(x) = 1, T_1(x) = x
//  .   T_n(x) = 2 x T_{n-1}(x) - T_{n-1}(x) , n > 1
//  . http://en.wikipedia.org/wiki/Chebyshev_polynomials
//
//
// - GegenbauerPolynomials Gegenbauer ultraspherical polynomials on [-1, 1], with parameter lambda up to order n
//  . Recurrence relation:
//  .  P_0 = 1, P_1 = 2*lambda*x
//  .  n*P_n(x) = 2*(n+lambda-1)*x*P_{n-1}(x) - (n+2*lambda-2)*P_{n-2}(x) , n > 1
//  . P_n is Jacobi polynomial with a = b = lambda - 1/2
//  . http://en.wikipedia.org/wiki/Gegenbauer_polynomials
//
// - JacobiPolynomials on [-1, 1] up to order n (cf. Abramowitz & Stegun p.382)
//  .  Orthogonal polynomials with weight (1-x)^a (1+x)^b
//  . Recurrence relation:
//  .  P_0 = 1, P_1 = (2*(a+1)+a+b+2)*(x-1) / 2
//  .  P_{n+1}*{ 2*(n+1)(n+a+b+1)(2*n+a+b) } =
//  .    { (2*n+1+a+b)(a^2-b^2) + (2*n+a+b)(2*n+1+a+b)(2*n+2+a+b)*x } * P_{n}
//  .   -{ 2*(n+a)*(n+b)(2*n+2+a+b) } * P_{n-1} , n > 0
//  . http://en.wikipedia.org/wiki/Jacobi_polynomials
//
// - LegendrePolynomials  on [-1, 1] up to order n
//  . Recurrence relation:
//  .   P_0(x) = 1, P_1(x) = x
//  . n*P_n(x) = (2*n-1) x P_{n-1}(x) - (n-1) P_{n-2}(x) , n > 1
//  . P_n is Gegenbauer ultraspherical polynomial with lambda = 1/2
//  . or     Jacobi polynomial with a = b = 0
//  . http://en.wikipedia.org/wiki/Legendre_polynomials
//
// - LegendrePolynomialsDerivative
//  . Recurrence relation
//  .  P'n satisfies:
//  .   P'_0(x) = 0, P'_1(x) = 1
//  .   (1-x^2) P'_n(x) = - n x P_n(x) + n P_{n-1}(x) , n > 1
//  . P'_n is Gegenbauer ultraspherical polynomial with lambda = 3/2
//
// - LegendreFunctions: Associated Legendre functions on [-1, 1] up to order n
//  .            P^m_l(x) = (-1)^{m} * (1-x^2)^{m/2} d^m/dx^m[ P_l(x)
//  . where P_l is Legendre polynomial of degree l
//  . Recurrence relation:
//  .   P^{k}_{k}(x) = (2k-1)!! * (-\sqrt{1-x^2})^{l}
//  .         where  (2k-1)!! = \Prod_{i=1,..,k}{2i-1) = (2k)! / 2^{k}*k!
//  .   P^{k}_{k+1}  = (2k+1) * x * P^{k}_{k}(x)
//  .   and for l = k+2, ..,n
//  .   (l-k+1) P^k_{l+1}(x) = (2*l+1)*x*P^k_{l}(x) - (l+k)*P^k_{l-1}(x)
//  . http://en.wikipedia.org/wiki/Associated_Legendre_polynomials
//
// - LegendreFunctionsDerivative: Associated Legendre functions derivatives on [-1, 1] up to order n
//  . Case (x^2 = 1) :
//  .   P'_{l}^{1}(x) = - x^l l(l+1) / 2
//  .   P'_{l}^{m}(x) = 0   if m != 1
//  . Case (x^2 != 1) :
//  .   P'_{0}^{0}(x) = 0
//  .   for l > 0 :
//  .     P'_{l}^{0} = P_{l}^{1}
//  .     for m = 1,..., l
//  .       P'_{l}^{m} = - (l-m+1) (l+m) P_{l}^{m-1} - x/sqrt(1-x^2) * m * P_{l}^{m}
//--------------------------------------------------------------------------------
/*!
  Chebyshev polynomials (of the first kind) on [-1, 1] up to order n
  T_0(x) = 1, T_1(x) = x, T_n(x) = 2 x T_{n-1}(x) - T_{n-1}(x) , n > 1
 */
void chebyshevPolynomials(real_t, std::vector<real_t>&);
/*!
  Gegenbauer ultraspherical polynomials on [-1, 1], with parameter lambda up to order n
  P_0 = 1, P_1 = 2*lambda*x
  n*P_n(x) = 2*(n+lambda-1)*x*P_{n-1}(x) - (n+2*lambda-2)*P_{n-2}(x) , n > 1
 */
void gegenbauerPolynomials(real_t lambda, real_t, std::vector<real_t>&);

/*!
  Jacobi polynomials on [-1, 1] up to order n
  P_0 = 1, P_1 = (2*(a+1)+a+b+2)*(x-1) / 2
  P_{n+1}*{ 2*(n+1)(n+a+b+1)(2*n+a+b) } = { (2*n+1+a+b)(a^2-b^2) + (2*n+a+b)(2*n+1+a+b)(2*n+2+a+b)*x } * P_{n}
                                           -{ 2*(n+a)*(n+b)(2*n+2+a+b) } * P_{n-1} , n > 0
 */
void jacobiPolynomials(real_t a, real_t b, real_t, std::vector<real_t>&);
void jacobiPolynomials01(real_t a, real_t b, real_t, std::vector<real_t>&); //!< Jacobi polynomials on [0, 1] up to order n

/*!
  Legendre polynomials on [-1, 1] up to order n
  P_0(x) = 1, P_1(x) = x,
  n*P_n(x) = (2*n-1) x P_{n-1}(x) - (n-1) P_{n-2}(x) , n > 1
 */
void legendrePolynomials(real_t, std::vector<real_t>&);

/*!
  derivatives of Legendre polynomials on [-1, 1] up to order n
  P'_0(x) = 0, P'_1(x) = 1
  (1-x^2) P'_n(x) = - n x P_n(x) + n P_{n-1}(x) , n > 1
 */
void legendrePolynomialsDerivative(real_t, std::vector<real_t>&);

/*!
  compute all Legendre functions up to order n for any real x: P^m_l(x)
  where n = Pml.size()-1 and l = 0,.., n and m = 0,..,n
        P^m_l(x) = (-1)^{m} * (1-x^2)^{m/2} d^m/dx^m[ P_l(x) ]
  where P_l is Legendre polynomial of degree l
 */
void legendreFunctions(real_t, std::vector<std::vector<real_t> >& Pml);

/*!
  compute all Legendre functions derivatives up to order n for any real x: P'^m_l(x)
  from given Legendre functions up to order n for any real x: P^m_l(x)
  where n = Pml.size()-1 and l = 0,.., n and m = 0,..,n
 */
void legendreFunctionsDerivative(real_t, const std::vector<std::vector<real_t> >&, std::vector<std::vector<real_t> >&);

void legendreFunctionsTest(real_t x, number_t n, std::ostream&);       //!< output function for test of Legendre Functions
void LegendreFunctionsDerivativeTest(real_t, number_t, std::ostream&); //!< output function for test of Legendre Functions derivatives

} // end of namespace xlifepp

#endif /* SPECIAL_FUNCTIONS_HPP */
