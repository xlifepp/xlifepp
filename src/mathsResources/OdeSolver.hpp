/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file OdeSolver.hpp
  \author E. Lunéville
  \since 16 jun 2021
  \date  16 jun 2021

  \brief OdeSolver class and childs Euler, Ode45
         to solve differential system y'(t)=F(t,y(t)), y(t0) = y0, t0<t<tf
         templated by the type T of y (T = real, complex, vector<real>, vector<complex>, ...)
         T requires only operator += and +
         F is a user fonction with prototype T& F(real_t t, const T& y, T& fty)
  usage:
    real_t& f(real_t t, const real_t& y, real_t& fty){... return fty;}
    Euler eu(f,0.,1.,0.01,0.); //build and compute
    eu.ys();  // access to states
    eu.ts();  // access to times
    eu.tys(); // access to times and states as a list<pair<real,T> >

    NOTE: Euler and Ode45 are acronym for real first order ode
           for other state type specify the type, for instance:
           Vector<real_t>& f(real_t t, const Vector<real_t>& y, Vector<real_t>& fty){...return fty;}
           EulerT<Vector<real_t> eu(f,0.,1.,0.01,Vector<Real>(2,0.)); //build and compute second order system

*/

#ifndef ODESOLVER_HPP
#define ODESOLVER_HPP

#include "utils.h"
#include "config.h"

namespace xlifepp
{
/*==============================================================================
    Base class
    f: function of ode y'=f(t,y)
    t0  : initial time
    tf: final time
    dt: initial time step
    y0  : initial step
    name: method name
==============================================================================*/
template <class T> class OdeSolver
{
  protected:
    T& (*f_)(real_t, const T&, T&);    // f(t,x) : R x K -> K (K=R, C, Rn, Cn, ...)
    real_t t0_,tf_;       // initial and final time
    real_t dt_;           // time step
    T y0_;                // initial state
    std::list<std::pair<real_t,T> > tys_;    // list of (time step, state)
    string_t name_;
  public:
    OdeSolver(T& (*f)(real_t, const T&, T&),real_t t0, real_t tf, real_t dt, const T& y0, const string_t& na="")
      : f_(f), t0_(t0), tf_(tf), dt_(dt), y0_(y0), name_(na)
    {
      if(this->tf_<=this->t0_) {std::cout<<"tf must be greater than t0"; exit(-1);}
      this->dt_=std::max(10*theEpsilon, std::min(this->dt_,this->tf_-this->t0_));
    }

    const std::list<std::pair<real_t,T> >& tys() const
    {return tys_;}

    std::vector<T> states() const  //! access to states y
    {
      std::vector<T> r(tys_.size());
      typename std::list<std::pair<real_t,T> >::const_iterator it=this->tys_.begin();
      typename std::vector<T>::iterator itr=r.begin();
      for(; it!=this->tys_.end(); ++it) *itr=it->second;
      return r;
    }
    std::vector<T> times() const //! access to time step t
    {
      std::vector<real_t> r(tys_.size());
      typename std::list<std::pair<real_t,T> >::const_iterator it=this->tys_.begin();
      typename std::vector<real_t>::iterator itr=r.begin();
      for(; it!=this->tys_.end(); ++it) *itr=it->first;
      return r;
    }
    void saveToFile(const string_t& fn) const
    {
      std::ofstream of(fn.c_str());
      typename std::list<std::pair<real_t,T> >::const_iterator it=this->tys_.begin();
      for(; it!=this->tys_.end(); ++it) of<<it->first<<" "<<it->second<<std::endl;
      of.close();
    }
};

/*==============================================================================
    Euler class (standard Euler method)
    f: function of ode y'=f(t,y)
    t0  : initial time
    tf: final time
    dt: initial time step
    y0  : initial step
    tol: relative tolerance error
  ==============================================================================*/
template <class T>
class EulerT: public OdeSolver<T>
{
  public:
    EulerT(T& (*f)(real_t, const T&, T&), real_t t0, real_t tf, real_t dt,const T& y0)
      : OdeSolver<T>(f,t0,tf,dt,y0,"Euler")
      {compute();}

    void compute()
    {
      this->tys_.clear();
      this->tys_.push_back(std::make_pair(this->t0_,this->y0_));
      real_t dt=this->dt_, t=this->t0_;
      T y = this->y0_, yy;
      if(t+dt>this->tf_) dt=this->tf_-t;
      while(t<this->tf_)
      {
        this->f_(t,y,yy);
        y+=dt*yy;
        t+=dt;
        this->tys_.push_back(std::make_pair(t,y));
      }
    }
};
typedef EulerT<real_t> Euler;

/*==============================================================================
    RK4 class (Runge-Kutta 4)
    f: function of ode y'=f(t,y)
    t0  : initial time
    tf: final time
    dt: initial time step
    y0  : initial step
  ==============================================================================*/
template <class T>
class RK4T: public OdeSolver<T>
{
  public:
    RK4T(T& (*f)(real_t, const T&, T&), real_t t0, real_t tf, real_t dt,const T& y0)
      : OdeSolver<T>(f,t0,tf,dt,y0,"RK4")
      {compute();}

    void compute()
    {
      T k1, k2, k3, k4;
      real_t dt=this->dt_, dt2=0.5*dt, t=this->t0_, tf=this->tf_;
      T y = this->y0_, yy;
      this->tys_.clear();
      this->tys_.push_back(std::make_pair(t,y));
      if(t+dt>tf) dt = tf-t;
      while(t<tf)
      {
      this->f_(t, y, k1);
      this->f_(t+dt2, y + dt2*k1, k2);
      this->f_(t+dt2, y + dt2*k2, k3);
      this->f_(t+dt , y + dt*k3,  k4);
      y+=dt*(k1+2.*k2+2.*k3+k4)/6.;
      t+=dt;
      this->tys_.push_back(std::make_pair(t,y));
      }
    }
};
typedef RK4T<real_t> RK4;

/*==============================================================================
    Ode45 class (adaptative scheme based on Runge-Kutta 4 and 5)
    f: function of ode y'=f(t,y)
    t0  : initial time
    tf: final time
    dt: initial time step
    y0  : initial step
    prec: relative error precision
  ==============================================================================*/
template <class T>
class Ode45T: public OdeSolver<T>
{
  public:
    real_t a,b,c,d,s;  //coefficients of RK45
    number_t nbTry_;
    real_t minScale_, maxScale_;
    real_t prec_;

    Ode45T(T& (*f)(real_t, const T&, T&), real_t t0, real_t tf, real_t dt,const T& y0, real_t prec=1E-6)
      : OdeSolver<T>(f,t0,tf,dt,y0,"ode45")
    {
      a = 2./3.; b = 1./729.; c = 1./2970.; d = 1./5940.;  s = 1./2520.;
      nbTry_=12; minScale_=0.125; maxScale_=4;
      prec_= prec;
      compute();
    }
    Ode45T(T& (*f)(real_t, const T&, T&), real_t t0, real_t tf, real_t dt,const T& y0,
           number_t nbtry, real_t minscale, real_t maxscale,real_t prec=1E-6)
      : OdeSolver<T>(f,t0,tf,dt,y0,"ode45")
    {
      a = 2./3.;  b = 1./729.;  c = 1./2970.; d = 1./5940.; s = 1./2520.;
      nbTry_=nbtry; minScale_= minscale; maxScale_= maxscale;
      prec_= prec;
      compute();
    }
    void compute() // Integrate the y'=f(t,y) from t0 to tf trying to maintain an error less than prec*(tf-t0)
    {
      this->tys_.clear();
      this->tys_.push_back(std::make_pair(this->t0_,this->y0_));
      real_t scale, dt=this->dt_, dtmax=dt, t=this->t0_, err, yy;
      T ty0, ty1;
      bool last_interval = false;
      number_t i;
      prec_ /= (this->tf_-this->t0_);
      ty0 = this->y0_;
      while(t < this->tf_) {
        scale = 1.0;
        for(i= 0; i < nbTry_; i++) {
          err = rk45(t, ty0, ty1, dt);
          if(err == 0.) { scale = maxScale_; break; }
          yy = (xlifepp::norm(ty0) == 0.0) ? prec_ : xlifepp::norm(ty0);
          scale = 0.8*std::sqrt(std::sqrt(prec_*yy/err));
          scale = std::min(std::max(scale,minScale_),maxScale_);
          if(err<(prec_*yy)) break;
          dt *= scale;
          if(dtmax>0 && dt>dtmax) {dt=dtmax;break;}
          if(t+dt >this->tf_)dt = this->tf_ - t;
          else if(t+dt+0.5*dt > this->tf_) dt = 0.5*dt;
        }
        if(i>=nbTry_) {std::cout<<"ode45 fails"; exit(-1);};
        ty0 = ty1;
        t+= dt;
        dt *= scale;
        if(dtmax>0 && dt>dtmax) dt=dtmax;
        this->tys_.push_back(std::make_pair(t,ty1));
        if(last_interval) break;
        if(t + dt > this->tf_) { last_interval = true; dt = this->tf_ - t; }
        else if(t + dt + 0.5*dt > this->tf_) dt = 0.5*dt;
      }
    }

    real_t rk45(real_t t, T& y, T& yy, real_t dt)  //one step of rk45
    {
      T k1, k2, k3, k4, k5, k6;
      real_t dt5 = 0.2 * dt;
      this->f_(t, y, k1);
      this->f_(t+dt5,    y + dt5*k1,k2);
      this->f_(t+0.3*dt, y + dt*(0.075*k1 + 0.225*k2),k3);
      this->f_(t+0.6*dt, y + dt*(0.3*k1 - 0.9*k2 + 1.2*k3),k4);
      this->f_(t+a*dt,   y + b*dt*(226.*k1 - 675.*k2 + 880.*k3 + 55.*k4),k5);
      this->f_(t+dt,     y + c*dt*(-1991.*k1 + 7425.*k2 - 2660.*k3 - 10010.*k4 + 10206.*k5),k6);
      yy = y + d*dt*(341.*k1 + 3800.*k3 - 7975.*k4 + 9477.*k5 + 297.*k6);
      return norm(s*(77.*k1 - 400.0*k3 + 1925.*k4 - 1701.*k5 + 99.*k6));
    }
};
typedef Ode45T<real_t> Ode45;

//functions wrapping ode objects
template <typename T>
Vector<T> euler(T& (*f)(real_t, const T& y, T& fty), real_t a, real_t b, real_t dt, const T& y0)
{
   EulerT<T> sys(f,a,b,dt,y0);
   Vector<T> r(sys.tys().size());
   typename std::list<std::pair<real_t,T> >::const_iterator it=sys.tys().begin();
   typename std::vector<T>::iterator itr=r.begin();
   for(; it!=sys.tys().end(); ++it, ++itr) *itr=it->second;
   return r;
}

template <typename T>
Vector<T> rk4(T& (*f)(real_t, const T& y, T& fty), real_t a, real_t b, real_t dt, const T& y0)
{
   RK4T<T> sys(f,a,b,dt,y0);
   Vector<T> r(sys.tys().size());
   typename std::list<std::pair<real_t,T> >::const_iterator it=sys.tys().begin();
   typename std::vector<T>::iterator itr=r.begin();
   for(; it!=sys.tys().end(); ++it, ++itr) *itr=it->second;
   return r;
}

template <typename T>
std::pair<Vector<real_t>,Vector<T> > ode45(T& (*f)(real_t, const T& y, T& fty), real_t a, real_t b, real_t dt, const T& y0, real_t prec=1.E-6)
{
   Ode45T<T> sys(f,a,b,dt,y0,prec);
   std::pair<Vector<real_t>,Vector<T> > r(std::make_pair(Vector<real_t>(sys.tys().size(),0.), Vector<T>(sys.tys().size(),T())));
   typename std::list<std::pair<real_t,T> >::const_iterator it=sys.tys().begin();
   typename std::vector<real_t>::iterator itt=r.first.begin();
   typename std::vector<T>::iterator its=r.second.begin();
   for(; it!=sys.tys().end(); ++it,++its,++itt)
   {
       *itt=it->first;
       *its=it->second;
   }
   return r;
}

template <typename T>
std::pair<Vector<real_t>,Vector<T> > ode45(T& (*f)(real_t, const T& y, T& fty), real_t a, real_t b, real_t dt, const T& y0,
                                           number_t nbt, real_t mins, real_t maxs, real_t prec=1.E-6)
{
   Ode45T<T> sys(f,a,b,dt,y0,nbt, mins, maxs,prec);
   std::pair<Vector<real_t>,Vector<T> > r(std::make_pair(Vector<real_t>(sys.tys().size(),0.), Vector<T>(sys.tys().size(),T())));
   typename std::list<std::pair<real_t,T> >::const_iterator it=sys.tys().begin();
   typename std::vector<real_t>::iterator itt=r.first.begin();
   typename std::vector<T>::iterator its=r.second.begin();
   for(; it!=sys.tys().end(); ++it,++its,++itt)
   {
       *itt=it->first;
       *its=it->second;
   }
   return r;
}

} //end of namespace xlifepp
#endif // ODESOLVER_HPP
