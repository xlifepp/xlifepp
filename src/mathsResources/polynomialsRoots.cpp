/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file polynomialsRoots.cpp
  \authors N. Kielbasiewicz
  \since 20 aug 2014
  \date 20 aug 2014

  \brief Implementation of functions computing roots of polynomials up to degree 4
 */

#include "polynomialsRoots.hpp"

#include <iostream>

namespace xlifepp
{

std::vector<complex_t> ferrari(real_t a, real_t b, real_t c, real_t d, real_t e)
{
  // equation to solve is a * x^4 + b * x^3 + c * x^2 + d * x + e = 0
  real_t b2=b/2.;
  real_t b4=b/4.;
  real_t a2=a*a;
  real_t a3=a2*a;
  real_t a4=a3*a;

  // reduction of equation to the form z^4 + p * z^2 + q * z + r = 0 with x = z - b / ( 4 * a )
  real_t p=c/a-3.*b2*b2/(2.*a2);
  real_t q=(b2*b2*b2)/a3-b2*c/a2+d/a;
  real_t r=-3.*(b4*b4*b4*b4)/a4+c*b4*b4/a3-d*b4/a2+e/a;

  // z^4 + p * z^2 + q * z + r = ( z^2 + y )^2 - 2 * y * z^2 - y^2 + p * z^2 + q * z + r
  //                           = ( z^2 + y )^2 - [ (2 * y - p) * z^2 -q * z + y^2 - r ]
  // second term is a polynom of order 2 in z. It can be written as a square if and only if discriminant is zero
  // It is equivalent as finding y solution of 8 * y^3 - 4 * p * y^2 - 8 * r * y + 4 * r * p - q^2 = 0
  std::vector<complex_t> y = cardan(8., -4.*p, -8.*r, 4.*r*p-q*q);
  real_t y0=0;
  for (number_t i=0; i < y.size(); ++i)
  {
    // we stop when we have found the real solution (it always exists)
    if (y[i].imag() == 0.)
    {
      y0=y[i].real();
      break;
    }
  }

  // we now have z^4 + p * z^2 + q * z + r = ( z^2 + y0 )^2 - ( alpha0 * z + beta0 )^2
  complex_t alpha0;
  if (2.*y0 > p) { alpha0=std::sqrt(2.*y0-p); }
  else { alpha0=complex_t(0.,std::sqrt(p-2.*y0)); }
  complex_t beta0;
  if (2.*y0 == p) { beta0=-q/(2.*alpha0); }
  else
  {
    if (y0*y0 > r) { beta0=std::sqrt(y0*y0-r); }
    else { beta0=complex_t(0.,std::sqrt(r-y0*y0)); }
  }
  // we solve z^2 + y0 + alpha0 z + beta0 = 0
  std::vector<complex_t> res1=quadratic(complex_t(1.),alpha0,y0+beta0);
  // we solve z^2 + y0 - alpha0 z - beta0 = 0
  std::vector<complex_t> res2=quadratic(complex_t(1.),-alpha0,y0-beta0);
  std::vector<complex_t> res;
  for (number_t i=0; i < res1.size(); ++i) { res.push_back(res1[i]-b4/a); }
  for (number_t i=0; i < res2.size(); ++i) { res.push_back(res2[i]-b4/a); }
  return res;
}

std::vector<complex_t> cardan(real_t a, real_t b, real_t c, real_t d)
{
  // equation to solve is a * x^3 + b * x^2 + c * x + d = 0
  real_t a2=a*a;
  real_t a3=a2*a;
  real_t b2=b*b;
  real_t b3=b2*b;
  // reduction of equation to the form z^3 + 3 * p * z + 2 * q = 0 with x = z - b / ( 3 * a )
  real_t p = c/(3.*a)-b2/(9.*a2);
  real_t q = b3/(27.*a3)-9.*b*c/(54.*a2) + d/(2.*a);
  real_t delta = - q*q - p*p*p;
  complex_t j=std::polar(1.,2.*pi_/3.);
  complex_t j2=j*j;

  std::vector<complex_t> res(3);
  if (delta < 0)
  {
    // one real root and 2 conjugate complex roots
    real_t u=xlifepp::cbrt(-q+std::sqrt(-delta));
    real_t v=xlifepp::cbrt(-q+std::sqrt(-delta));
    res[0]=u+v;
    res[1]=j*u + j2*v;
    res[2]=j2*u + j*v;
  }
  else
  {
    // 3 real roots
    complex_t u=xlifepp::cbrt(-q+complex_t(0.,std::sqrt(delta)));
    complex_t ju=j*u;
    complex_t j2u=j2*u;
    res[0]=2.*u.real();
    res[1]=2.*ju.real();
    res[2]=2.*j2u.real();
  }
  return res;
}

std::vector<complex_t> cardan(complex_t a, complex_t b, complex_t c, complex_t d)
{
  // equation to solve is a * x^3 + b * x^2 + c * x + d = 0
  complex_t a2=a*a;
  complex_t a3=a2*a;
  complex_t b2=b*b;
  complex_t b3=b2*b;
  // reduction of equation to the form z^3 + 3 * p * z + 2 * q = 0 with x = z - b / ( 3 * a )
  complex_t p = c/(3.*a)-b2/(9.*a2);
  complex_t q = b3/(27.*a3)-9.*b*c/(54.*a2) + d/(2.*a);
  complex_t delta = - q*q - p*p*p;
  complex_t j=std::polar(1.,2.*pi_/3.);
  complex_t j2=j*j;
  complex_t u = xlifepp::cbrt(-q+std::sqrt(-delta));
  complex_t v = xlifepp::cbrt(-q-std::sqrt(-delta));
  std::vector<complex_t> res(3);
  res[0]=u+v;
  res[1]=j*u + j2*v;
  res[2]=j2*u + j*v;
  return res;
}

std::vector<complex_t> quadratic(real_t a, real_t b, real_t c)
{
  std::vector<complex_t> res(2);
  real_t delta=b*b-4.*a*c;
  if (delta >=0)
  {
    res[0]=(-b-std::sqrt(delta))/(2.*a);
    res[1]=(-b+std::sqrt(delta))/(2.*a);
  }
  else
  {
    res[0]=complex_t(-b,-std::sqrt(-delta))/(2.*a);
    res[1]=complex_t(-b,+std::sqrt(-delta))/(2.*a);
  }
  return res;
}

std::vector<complex_t> quadratic(complex_t a, complex_t b, complex_t c)
{
  std::vector<complex_t> res(2);
  complex_t delta=b*b-4.*a*c;
  // pas bon sqrt ne fonctione qu'avec les réels ??????
  res[0]=(-b-std::sqrt(delta))/(2.*a);
  res[1]=(-b+std::sqrt(delta))/(2.*a);
  return res;
}

} // end of namespace xlifepp
