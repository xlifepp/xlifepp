/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Maxwell3dKernel.hpp
  \author R. Rais, E. Lunéville
  \since 08 fev 2017
  \date  08 fev 2017

  \brief Definition of Maxwell 3D kernel
*/
#ifndef MAXWELL3DKERNEL_HPP
#define MAXWELL3DKERNEL_HPP

#include "utils.h"

namespace xlifepp
{
// Kernel like constructor
Kernel Maxwell3dKernel(Parameters& = defaultParameters);     //!< construct a Maxwell3d kernel from parameters
Kernel Maxwell3dKernel(const real_t& k, const real_t& t=0);        //!< construct a Maxwell3d kernel from real k
Kernel Maxwell3dKernel(const complex_t& k, const real_t& t=0);     //!< construct a Maxwell3d kernel from complex k
void initMaxwell3dKernel(Kernel&, Parameters&); //!< initialize kernel data

// computation functions
Matrix<complex_t> Maxwell3d(const Point&, const Point&, Parameters& = defaultParameters);           //!< value
Matrix<complex_t> Maxwell3dCurlx(const Point&, const Point&, Parameters& = defaultParameters);      //!< curlx
Matrix<complex_t> Maxwell3dCurly(const Point&, const Point&, Parameters& = defaultParameters);      //!< curly
Matrix<complex_t> Maxwell3dCurlxy(const Point&, const Point&, Parameters& = defaultParameters);     //!< curlx.curly
Vector<complex_t> Maxwell3dDivx(const Point&, const Point&, Parameters& = defaultParameters);       //!< divx
Vector<complex_t> Maxwell3dDivy(const Point&, const Point&, Parameters& pa = defaultParameters);    //!< divy
complex_t Maxwell3dDivxy(const Point&, const Point&, Parameters& pa = defaultParameters);           //!< divx.divy


} // end of namespace xlifepp

#endif // MAXWELL3DKERNEL_HPP
