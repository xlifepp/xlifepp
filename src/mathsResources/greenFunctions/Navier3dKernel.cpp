/*
 XLiFE++ is an extended library of finite elements written in C++
 Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*!
 \file Navier3dKernel.cpp
 \author R. Rais
 \since 30 sep 2021
 \date  30 sep 2021

 \brief Implementation of Navier 3D kernels

 Navier3D kernel  K(k; x, y)=1/mu(G(k_s; x, y)*I_3 + 1/k_s^2 * Hess(G(k_s; x, y)-G(k_p; x, y))),

 where G(k; x, y)=(1/4 pi*|x-y|) exp(ik|x-y|)
 */

#include "Navier3dKernel.hpp"
#include "Helmholtz3dKernel.hpp"
#include "utils.h"

namespace xlifepp
{
//---------------------------------------------------------------------------------------------------------
//construct Navier3d Kernel : Phi(k_s; x, y)=1/mu(G(k_s; x, y)*I_3 + 1/k_s^2 * Hess(G(k_s; x, y)-G(k_p; x, y)))
//---------------------------------------------------------------------------------------------------------

//construct a Navier3d Kernel
Kernel Navier3dKernel(Parameters& pars)
{
  Kernel K;
  initNavier3dKernel(K,pars);
  return K;
}

Kernel Navier3dKernel(const real_t& k, const real_t& lambda, const real_t& mu)
{
  std::cout << "Navier3dKernel "<<eol;
  Kernel K;
  Parameters pars;
  pars<<Parameter(k,"k");
  pars<<Parameter(lambda,"lambda");
  pars<<Parameter(mu,"mu");

  initNavier3dKernel(K,pars);
  return K;
}

void initNavier3dKernel(Kernel& K, Parameters& pars)
{
  K.name="Navier 3D kernel";
  K.shortname="Navier3D";
  K.singularType =_r_;
  K.singularOrder = -1;
  K.singularCoefficient = over4pi_;
  K.symmetry=_symmetric;
  const void * p;
  K.userData<<Parameter(p,"_nxp")<<Parameter(p,"_nyp");
  K.userData.push(pars);
  K.kernel = Function(Navier3d, K.userData);
  K.tracx = Function(Navier3dTracx, K.userData);
  K.tracy = Function(Navier3dTracy, K.userData);
  K.tracxy = Function(Navier3dTracxy, K.userData);
  K.nxtensornxtimesNGr = Function(Navier3dnxtensornxtimesNGr, K.userData);
  K.nxtensornxtimestracy = Function(Navier3dnxtensornxtimestracy, K.userData);
}
//! kernel computation : Phi(k_s; x, y)=1/mu(G(k_s; x, y)*I_3 + 1/k_s^2 * Hess(G(k_s; x, y)-G(k_p; x, y)))
Matrix<complex_t> Navier3d(const Point& x, const Point& y, Parameters& pa)
{
  real_t k = pa("k");
  complex_t ukk = 1./(k*k);
  complex_t Gk= Helmholtz3d(x,y,pa);
  Matrix<complex_t> Gradxyk=Helmholtz3dGradxy(x,y,pa);
  real_t lambda = pa("lambda");
  real_t mu = pa("mu");
  real_t overmu=1/mu;
  Parameters pakp(k * std::sqrt(mu/(lambda+2*mu)),"k");
  Gradxyk-=Helmholtz3dGradxy(x,y,pakp);
  Matrix<complex_t> mIG(3,3);
  std::vector<complex_t>::iterator itm=mIG.begin(), itg=Gradxyk.begin();
//Row 1
  *itm++=Gk-ukk* *itg++;
  *itm++=-ukk* *itg++;
  *itm++=-ukk* *itg++;
//Row 2
  *itm++=mIG(1,2); itg++;
  *itm++=Gk-ukk* *itg++;
  *itm++=-ukk* *itg++;
//Row 3
  *itm++=mIG(1,3); itg++;
  *itm++=mIG(2,3); itg++;
  *itm++=Gk-ukk* *itg;
  return overmu*mIG;
 }
//! kernel computation : Tracx(Phi)
Matrix<complex_t> Navier3dTracx(const Point& x, const Point& y, Parameters& pa)
{
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  real_t r3 = r * r2;
  real_t k = pa("k");
  real_t k2 = k * k;
  complex_t ik = i_ * k;
  real_t overk2r2 = 2/(k2 * r2);
  real_t overk2r3 = overk2r2 / r;
  real_t lambda = pa("lambda");
  real_t mu = pa("mu");
  real_t kp = k * std::sqrt(mu/(lambda+2*mu));
  real_t kp2 = kp * kp;
  complex_t ikp = i_ * kp;
  Point x_y=x-y;
  Vector<real_t> nxp=getNx();
  real_t nxdotXminusY= inner_product(nxp.begin(), nxp.end(), x_y.begin(), real_t(0.));
  Matrix<complex_t> Maux1(3,3);
  tensorProductTpl(x_y.begin(), x_y.end(), nxp.begin(), nxp.end(), Maux1.begin());
  Matrix<complex_t> Maux2(3,3);
  tensorProductTpl(nxp.begin(), nxp.end(),x_y.begin(), x_y.end(), Maux2.begin());
  Matrix<complex_t> Maux3(3,3);
  tensorProductTpl(x_y.begin(), x_y.end(), x_y.begin(),x_y.end(),Maux3.begin());
  Matrix<complex_t> Maux4(3,3,0);
  std::vector<complex_t>::iterator it_aux4=Maux4.begin();
  *it_aux4++ = nxdotXminusY;
  *it_aux4++;
  *it_aux4++;
  *it_aux4++;
  *it_aux4++ = nxdotXminusY;
  *it_aux4++;
  *it_aux4++;
  *it_aux4++;
  *it_aux4++ = nxdotXminusY;
  Matrix<complex_t> SumM1(3,3);
  SumM1 = Maux2 + Maux4;
  Matrix<complex_t> SumM2(3,3);
  SumM2 =  overk2r2 * (Maux1 + SumM1);
  Matrix<complex_t> Maux5(3,3);
  Maux5 = (overk2r3 * nxdotXminusY) * Maux3;
  complex_t Gk= Helmholtz3d(x,y,pa);
  Parameters pakp(kp,"k");
  complex_t Gkp= Helmholtz3d(x,y,pakp);
  Matrix<complex_t> mTracx(3,3);
  mTracx = tran(((Gk * (ik- 1/r)) / r ) * SumM1
  - (Gk * (k2+3*ik /r - 3/r2) - Gkp * (kp2+3*ikp /r - 3/r2)) *   SumM2
  + lambda/(lambda+2*mu) * Gkp/r * (ikp- 1/r) * Maux1
  - (Gk * (ik*k2 - 6*k2 /r - 15*ik/r2 + 15/r3)
      - Gkp * (ikp*kp2 - 6*kp2 /r - 15*ikp/r2 + 15/r3)) * Maux5);

  return mTracx;
}
//! kernel computation : Tracy(Phi)
Matrix<complex_t> Navier3dTracy(const Point& x, const Point& y, Parameters& pa)
{
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  real_t r3 = r * r2;
  real_t k = pa("k");
  real_t k2 = k * k;
  complex_t ik = i_ * k;
  real_t overk2r2 = 2/(k2 * r2);
  real_t overk2r3 = overk2r2 / r;
  real_t lambda = pa("lambda");
  real_t mu = pa("mu");
  real_t kp = k * std::sqrt(mu/(lambda+2*mu));
  real_t kp2 = kp * kp;
  complex_t ikp = i_ * kp;
  Point x_y=x-y;
  Vector<real_t> nyp=getNy();
  real_t nydotXminusY = inner_product(nyp.begin(), nyp.end(), x_y.begin(), real_t(0.));
  Matrix<complex_t> Maux1(3,3);
  tensorProductTpl(x_y.begin(), x_y.end(), nyp.begin(), nyp.end(), Maux1.begin());
  Matrix<complex_t> Maux2(3,3);
  tensorProductTpl(nyp.begin(), nyp.end(),x_y.begin(), x_y.end(), Maux2.begin());
  Matrix<complex_t> Maux3(3,3);
  tensorProductTpl(x_y.begin(), x_y.end(), x_y.begin(),x_y.end(),Maux3.begin());
  Matrix<complex_t> Maux4(3,3,0);
  std::vector<complex_t>::iterator it_aux4=Maux4.begin();
  *it_aux4++ = nydotXminusY;
  *it_aux4++;
  *it_aux4++;
  *it_aux4++;
  *it_aux4++ = nydotXminusY;
  *it_aux4++;
  *it_aux4++;
  *it_aux4++;
  *it_aux4++ = nydotXminusY;
  Matrix<complex_t> SumM1(3,3);
  SumM1 = Maux2 + Maux4;
  Matrix<complex_t> SumM2(3,3);
  SumM2 = overk2r2 * (Maux1 + SumM1);
  Matrix<complex_t> Maux5(3,3);
  Maux5 = (overk2r3 * nydotXminusY) * Maux3;
  complex_t Gk= Helmholtz3d(x,y,pa);
  Parameters pakp(kp,"k");
  complex_t Gkp= Helmholtz3d(x,y,pakp);
  Matrix<complex_t> mTracy(3,3);
  mTracy = -((Gk * (ik- 1/r)) / r ) * SumM1
  + (Gk * (k2+3*ik /r - 3/r2) - Gkp * (kp2+3*ikp /r - 3/r2)) *   SumM2
  - lambda/(lambda+2*mu) * Gkp/r * (ikp- 1/r) * Maux1
  + (Gk * (ik*k2 - 6*k2 /r - 15*ik/r2 + 15/r3)
     - Gkp * (ikp*kp2 - 6*kp2 /r - 15*ikp/r2 + 15/r3)) * Maux5;

  return mTracy;
}
//! kernel computation : Tracxy(Phi)
Matrix<complex_t> Navier3dTracxy(const Point& x, const Point& y, Parameters& pa)
{
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  real_t r3 = r * r2;
  real_t k = pa("k");
  real_t k2 = k * k;
  complex_t ik = i_ * k;
  real_t overk2r2 = 2/(k2 * r2);
  real_t overk2r3 = overk2r2 / r;
  real_t lambda = pa("lambda");
  real_t mu = pa("mu");
  real_t kp = k * std::sqrt(mu/(lambda+2*mu));
  real_t kp2 = kp * kp;
  complex_t ikp = i_ * kp;
  Point x_y=x-y;
  Vector<real_t> nxp=getNx();
  real_t nxdotXminusY= inner_product(nxp.begin(), nxp.end(), x_y.begin(), real_t(0.));
  complex_t S1 = k2 + 3*ik/r - 3/r2;
  complex_t S2 = kp2 + 3*ikp/r - 3/r2;
  complex_t S3 = ik*k2 - 6*k2/r - 15*ik/r2 +15/r3;
  complex_t S4 = ikp*kp2 - 6*kp2/r - 15*ikp/r2 +15/r3;
  complex_t S5 = -k2*k2 - 10*ik*k2/r + 45*k2/r2 + 105*ik/r3 - 105/(r3*r);
  complex_t S6 = -kp2*kp2 - 10*ikp*kp2/r + 45*kp2/r2 + 105*ikp/r3 - 105/(r3*r);
  real_t overk2r4 = overk2r2 / r2;
  Vector<real_t> nyp=getNy();
  real_t nxdotny =  inner_product(nxp.begin(), nxp.end(), nyp.begin(), real_t(0.));
  real_t nydotXminusY = inner_product(nyp.begin(), nyp.end(), x_y.begin(), real_t(0.));
  Matrix<complex_t> M_Id(3,3,0);
  std::vector<complex_t>::iterator it_Id = M_Id.begin();
  *it_Id++ = 1;
  *it_Id++;
  *it_Id++;
  *it_Id++;
  *it_Id++ = 1;
  *it_Id++;
  *it_Id++;
  *it_Id++;
  *it_Id++ = 1;
  Matrix<complex_t> Maux1(3,3);
  tensorProductTpl(x_y.begin(), x_y.end(), nxp.begin(), nxp.end(), Maux1.begin());
  Matrix<complex_t> Maux2(3,3);
  tensorProductTpl(nxp.begin(), nxp.end(),x_y.begin(), x_y.end(), Maux2.begin());
  Matrix<complex_t> Maux3(3,3);
  tensorProductTpl(nxp.begin(), nxp.end(), nyp.begin(),nyp.end(),Maux3.begin());
  Matrix<complex_t> Maux4(3,3);
  tensorProductTpl(nyp.begin(), nyp.end(), nxp.begin(),nxp.end(),Maux4.begin());
  Matrix<complex_t> Maux5(3,3);
  tensorProductTpl(nyp.begin(), nyp.end(), x_y.begin(),x_y.end(),Maux5.begin());
  Matrix<complex_t> Maux6(3,3);
  tensorProductTpl(x_y.begin(), x_y.end(), nyp.begin(),nyp.end(),Maux6.begin());
  Matrix<complex_t> Maux7(3,3);
  tensorProductTpl(x_y.begin(), x_y.end(), x_y.begin(),x_y.end(),Maux7.begin());
  Matrix<complex_t> Maux8(3,3);
  Maux8 = nxdotny * Maux7;
  Matrix<complex_t> Maux9(3,3);
  Maux9 = nxdotXminusY * Maux5;
  Matrix<complex_t> Maux10(3,3);
  Maux10 = nydotXminusY * Maux2;
  Matrix<complex_t> Maux11(3,3);
  Maux11 = mu * nxdotXminusY * Maux6;
  real_t lambdar2 = lambda * r2;
  Matrix<complex_t> Maux12(3,3);
  Maux12 = lambdar2 * Maux3;
  Matrix<complex_t> Maux13(3,3);
  Maux13 = lambdar2 * Maux2;
  Matrix<complex_t> Maux14(3,3);
  complex_t Gk= Helmholtz3d(x,y,pa);
  Parameters pakp(kp,"k");
  complex_t Gkp= Helmholtz3d(x,y,pakp);
  Matrix<complex_t> mTracxy(3,3);
  mTracxy = Gk/r2 * S1 * (nydotXminusY * (2 * lambda * Maux2 + mu * (Maux1 + nxdotXminusY * M_Id))+ mu * (Maux8 + Maux9))
    - Gk/r * (ik-1/r) * (2 * lambda * Maux3 + 2 * mu * (Maux4 + nxdotny * M_Id))
    + overk2r3 * (S3 * Gk - S4 * Gkp ) * (2 * lambda * Maux10 + mu * (nydotXminusY * Maux1 + Maux8 +
                                                                               Maux9 + nydotXminusY * nxdotXminusY * M_Id)+ 2 * Maux11 + Maux12)
    + overk2r2 * (S1 * Gk - S2 * Gkp) * ((5 * lambda + 2 * mu) * Maux3 + 2 * mu * (Maux4 + (nxdotny * M_Id)))
    - lambda/(lambda+2 * mu) * Gkp/r2 * (-S2 * (Maux12 + 2 * Maux11) + (ikp*r-1) * (3 * lambda +2 * mu) * Maux3)
    + overk2r4 * (S5 * Gk-S6 * Gkp) * nydotXminusY * (Maux13 + 2 * mu * nxdotXminusY * Maux7)
    + overk2r3 * (S3 * Gk - S4 * Gkp) * (lambda * Maux10 + mu * (Maux9 + Maux8)
                                         + nydotXminusY * ((4 * lambda + 2 * mu) * Maux2 + mu * (nxdotXminusY * M_Id + Maux1)));
  return mTracxy;
}
//! kernel computation : NxtensorNx(Phi)
Matrix<complex_t> Navier3dnxtensornxtimesNGr(const Point& x, const Point& y, Parameters& pa)
{
  real_t k = pa("k");
  real_t lambda = pa("lambda");
  real_t mu = pa("mu");
  Matrix<complex_t> mGr= Navier3d(x,y,pa);
  Vector<real_t>& nxp=getNx();
  Matrix<real_t> NxtensorNx(3,3);
  tensorProductTpl(nxp.begin(), nxp.end(), nxp.begin(), nxp.end(), NxtensorNx.begin());
  Matrix<complex_t> mNxtensorNxGr(3,3);
  mNxtensorNxGr = NxtensorNx * mGr;
  return mNxtensorNxGr;
}
//! kernel computation : NxtensorNxTracy(Phi)
Matrix<complex_t> Navier3dnxtensornxtimestracy(const Point& x, const Point& y, Parameters& pa)
{
  real_t k = pa("k");
  real_t lambda = pa("lambda");
  real_t mu = pa("mu");
  Matrix<complex_t> mTracy= Navier3dTracy(x,y,pa);
  Vector<real_t>& nxp=getNx();
  Matrix<real_t> NxtensorNx(3,3);
  tensorProductTpl(nxp.begin(), nxp.end(), nxp.begin(), nxp.end(), NxtensorNx.begin());
  Matrix<complex_t> mNxtensorNxTracy(3,3);
  mNxtensorNxTracy = NxtensorNx * mTracy;
  return mNxtensorNxTracy;
}

} // end of namespace xlifepp



