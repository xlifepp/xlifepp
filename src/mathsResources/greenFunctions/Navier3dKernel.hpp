/*
 XLiFE++ is an extended library of finite elements written in C++
 Copyright (C) 2014  LunÈville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*!
 \file Navier3dKernel.hpp
 \author R. Rais
 \since 30 sep 2021
 \date  30 sep 2021
 
 \brief Definition of Navier 3D kernel
 */
#ifndef NAVIER3DKERNEL_HPP
#define NAVIER3DKERNEL_HPP

#include "utils.h"

namespace xlifepp
{
// Kernel like constructor
Kernel Navier3dKernel(Parameters& = defaultParameters);     //!< construct a Navier3d kernel from parameters
Kernel Navier3dKernel(const real_t& k, const real_t& lambda, const real_t& mu);   //!< construct a Navier3d kernel
void initNavier3dKernel(Kernel&, Parameters&); //!< initialize kernel data
    
// computation functions
Matrix<complex_t> Navier3d(const Point&, const Point&, Parameters& = defaultParameters);           //!< value
Matrix<complex_t> Navier3dTracx(const Point&, const Point&, Parameters& = defaultParameters);      //!< tracx
Matrix<complex_t> Navier3dTracy(const Point&, const Point&, Parameters& = defaultParameters);      //!< tracy
Matrix<complex_t> Navier3dTracxy(const Point&, const Point&, Parameters& = defaultParameters);     //!< tracx.tracy
Matrix<complex_t> Navier3dnxtensornxtimestracy(const Point&, const Point&, Parameters& = defaultParameters);     //!< NxtensorNxTracy
Matrix<complex_t> Navier3dnxtensornxtimesNGr(const Point&, const Point&, Parameters& = defaultParameters);     //!< NxtensorNxGr        
} // end of namespace xlifepp

#endif // NAVIER3DKERNEL_HPP

