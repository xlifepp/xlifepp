/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Laplace3dKernel.cpp
  \author E. Lunéville
  \since 17 jul 2014
  \date  17 jul 2014

  \brief Implementation of Laplace 3D kernels

  Laplace3D kernel  K(k; x, y)=(1/4 pi*|x-y|)
*/

#include "Laplace3dKernel.hpp"
#include "utils.h"

namespace xlifepp
{


//--------------------------------------------------------------------------------------------
//construct Laplace3d Kernel: G(k; x, y)=(1/4 pi*|x-y|)
//--------------------------------------------------------------------------------------------

//construct a Laplace3d Kernel
Kernel Laplace3dKernel(Parameters& pars)
{
    Kernel K;
    K.name="Laplace 3D kernel";
    K.shortname="Lap3D";
    K.singularType =_r_;
    K.singularOrder = -1;
    K.singularCoefficient = over4pi_;
    K.symmetry=_symmetric;
    K.userData.push(pars);
    K.kernel = Function(Laplace3d, K.userData);
    K.gradx = Function(Laplace3dGradx, K.userData);
    K.grady = Function(Laplace3dGrady, K.userData);
    K.ndotgradx = Function(Laplace3dNxdotGradx, K.userData);
    K.ndotgrady = Function(Laplace3dNydotGrady, K.userData);
    return K;
}

real_t Laplace3d(const Point& x, const Point& y, Parameters& pars)
{
  real_t r = x.distance(y);
  return over4pi_ / r;
}

//derivatives
Vector<real_t> Laplace3dGradx(const Point& x, const Point& y,Parameters& pars)
{
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  Vector<real_t> g1(3);
  scaledVectorTpl(-over4pi_ / (r*r2), x.begin(), x.end(), y.begin(), g1.begin());
  return g1;
}

Vector<real_t> Laplace3dGrady(const Point& x, const Point& y,Parameters& pars)
{
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  Vector<real_t> g1(3);
  scaledVectorTpl(over4pi_ / (r*r2), x.begin(), x.end(), y.begin(), g1.begin());
  return g1;
}

real_t Laplace3dNxdotGradx(const Point& x, const Point& y, Parameters& pars)
{
  Vector<real_t>& nxp = getNx();
  std::vector<real_t>::const_iterator itx=x.begin(),ity=y.begin(), itn=nxp.begin();
  real_t d1=(*itx++ - *ity++), d2=(*itx++ - *ity++), d3=(*itx - *ity);
  real_t r = d1* *itn++; r+= d2* *itn++; r+=d3* *itn;
  real_t r3= d1*d1+d2*d2+d3*d3; r3*=std::sqrt(r3);
  return -r*over4pi_/r3;
}

real_t Laplace3dNydotGrady(const Point& x, const Point& y, Parameters& pars)
{
   Vector<real_t>& nyp = getNy();
    std::vector<real_t>::const_iterator itx=x.begin(),ity=y.begin(), itn=nyp.begin();
    real_t d1=(*itx++ - *ity++), d2=(*itx++ - *ity++), d3=(*itx - *ity);
    real_t r = d1* *itn++; r+= d2* *itn++; r+=d3* *itn;
    real_t r3= d1*d1+d2*d2+d3*d3; r3*=std::sqrt(r3);
    return r*over4pi_/r3;
}

}
