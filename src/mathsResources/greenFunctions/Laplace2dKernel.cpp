/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Laplace2dKernel.cpp
  \author E. Lunéville
  \since 17 may 2016
  \date  17 may 2016

  \brief Implementation of Laplace 2D kernels

  Laplace2D kernel  K(k; x, y)=(-1/2 pi)*log(|x-y|)
*/

#include "Laplace2dKernel.hpp"
#include "utils.h"

namespace xlifepp
{


//--------------------------------------------------------------------------------------------
//construct Laplace2d Kernel: G(k; x, y)=-(1/2 pi)*log(|x-y|)
//--------------------------------------------------------------------------------------------

//construct a Laplace2d Kernel
Kernel Laplace2dKernel(Parameters& pars)
{
    Kernel K;
    K.dimPoint=2;
    K.name="Laplace 2D kernel";
    K.shortname="Lap2D";
    K.singularType =_logr;
    K.singularOrder = 1;
    K.singularCoefficient = -over2pi_;
    K.symmetry=_symmetric;
    K.userData.push(pars);
    K.kernel = Function(Laplace2d, 2, K.userData);
    K.gradx = Function(Laplace2dGradx, 2, K.userData);
    K.grady = Function(Laplace2dGrady, 2, K.userData);
    K.ndotgradx = Function(Laplace2dNxdotGradx, 2, K.userData);
    K.ndotgrady = Function(Laplace2dNydotGrady, 2, K.userData);
    K.gradxy = Function(Laplace2dGradxy, 2, K.userData);
    return K;
}

real_t Laplace2d(const Point& x, const Point& y, Parameters& pars)
{
  return -over2pi_*std::log(x.distance(y));
}

//derivatives
Vector<real_t> Laplace2dGradx(const Point& x, const Point& y,Parameters& pars)
{
  real_t r = x.distance(y);
  Vector<real_t> g(2);
  real_t a = -over2pi_ / (r*r);
  g[0]=a*(x[0]-y[0]);
  g[1]=a*(x[1]-y[1]);
  return g;
}

Vector<real_t> Laplace2dGrady(const Point& x, const Point& y,Parameters& pars)
{
  real_t r = x.distance(y);
  Vector<real_t> g(2);
  real_t a = over2pi_ / (r*r);
  g[0]=a*(x[0]-y[0]);
  g[1]=a*(x[1]-y[1]);
  return g;
}

real_t Laplace2dNxdotGradx(const Point& x, const Point& y, Parameters& pars)
{
  Vector<real_t>& nxp = getNx();
  std::vector<real_t>::const_iterator itx=x.begin(),ity=y.begin(), itn=nxp.begin();
  real_t d1=(*itx++ - *ity++), d2=(*itx - *ity);
  real_t r=x.distance(y);
  real_t rn = d1* *itn++; rn+= d2* *itn;
  return -rn*over2pi_/(r*r);
}

real_t Laplace2dNydotGrady(const Point& x, const Point& y, Parameters& pars)
{
   Vector<real_t>& nyp = getNy();
   std::vector<real_t>::const_iterator itx=x.begin(),ity=y.begin(), itn=nyp.begin();
   real_t d1=(*itx++ - *ity++), d2=(*itx - *ity);
   real_t r=x.distance(y);
   real_t rn = d1* *itn++; rn+= d2* *itn;
   return rn*over2pi_/(r*r);
}

//  gradxy stored as the matrix  | dx1dy1   dx2dy1 |
//                               | dx1dy2   dx2dy2 |
Matrix<real_t> Laplace2dGradxy(const Point& x, const Point& y, Parameters& pa)
{
  Matrix<real_t> m(2,2);
  real_t z1=x[0]-y[0], z2=x[1]-y[1];
  real_t r2=z1*z1+z2*z2;
  real_t a=over2pi_/r2, b=2*a/r2;
  m(1,1)=a-b*z1*z1; m(1,2)=-b*z1*z2;
  m(2,1)=-b*z1*z2;  m(2,2)=a-b*z2*z2;
  return m;
}


}
