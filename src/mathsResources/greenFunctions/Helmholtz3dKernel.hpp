/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Helmholtz3dKernel.hpp
  \author E. Lunéville
  \since 19 jun 2014
  \date  19 jun 2014

  \brief Definition of Helmholtz 3D kernels

  Header to deal with Helmholtz 3D kernel: K(k; x, y)=(1/4 pi*|x-y|) exp(ik|x-y|)
*/
#ifndef HELMHOLTZ3DKERNEL_HPP
#define HELMHOLTZ3DKERNEL_HPP

#include "utils.h"

namespace xlifepp
{

Kernel Helmholtz3dKernel(Parameters& = defaultParameters);     //!< construct a Helmholtz3d kernel from parameters
Kernel Helmholtz3dKernelReg(Parameters& = defaultParameters);  //!< construct a Helmholtz3d kernel from parameters
Kernel Helmholtz3dKernelSing(Parameters& = defaultParameters); //!< construct a Helmholtz3d kernel from parameters
Kernel Helmholtz3dKernel(const real_t& k);        //!< construct a Helmholtz3d kernel from real k
Kernel Helmholtz3dKernel(const complex_t& k);     //!< construct a Helmholtz3d kernel from complex k
void initHelmholtz3dKernel(Kernel&, Parameters&); //!< initialize kernel data

//main computation functions
complex_t Helmholtz3d(const Point& x, const Point& y, Parameters& pa = defaultParameters);                   //!< value
Vector<complex_t> Helmholtz3dGradx(const Point& x, const Point& y, Parameters& pa = defaultParameters);      //!< gradx
Vector<complex_t> Helmholtz3dGrady(const Point& x, const Point& y, Parameters& pa = defaultParameters);      //!< grady
complex_t Helmholtz3dNxdotGradx(const Point& x, const Point&y, Parameters& pa = defaultParameters);          //!< nx.gradx
complex_t Helmholtz3dNydotGrady(const Point& x, const Point&y, Parameters& pa = defaultParameters);          //!< ny.grady
Matrix<complex_t> Helmholtz3dGradxy(const Point& x, const Point& y, Parameters& pa = defaultParameters);     //!< gradxy
complex_t Helmholtz3dReg(const Point& x, const Point& y, Parameters& pa = defaultParameters);                //!< value
Vector<complex_t> Helmholtz3dGradxReg(const Point& x, const Point& y, Parameters& pa = defaultParameters);   //!< gradx
Vector<complex_t> Helmholtz3dGradyReg(const Point& x, const Point& y, Parameters& pa = defaultParameters);   //!< grady
Matrix<complex_t> Helmholtz3dGradxyReg(const Point& x, const Point& y, Parameters& pa = defaultParameters);  //!< gradxy
  complex_t Helmholtz3dNydotGradyReg(const Point& x, const Point& y, Parameters& pa);
complex_t Helmholtz3dSing(const Point& x, const Point& y, Parameters& pa = defaultParameters);               //!< value
Vector<complex_t> Helmholtz3dGradxSing(const Point& x, const Point& y, Parameters& pa = defaultParameters);  //!< gradx
Vector<complex_t> Helmholtz3dGradySing(const Point& x, const Point& y, Parameters& pa = defaultParameters);  //!< grady
Matrix<complex_t> Helmholtz3dGradxySing(const Point& x, const Point& y, Parameters& pa = defaultParameters); //!< gradxy

//utilities
void over4pir(const Point&, const Point&, real_t&); //!< \f$1/(4\pi r)\f$
void gradxover4pir(const Point&, const Point&, Vector<real_t>&); //!< \f$grad_x/(4\pi r)\f$
void gradxgradyover4pir(const Point&, const Point&, Vector<Vector<real_t> >&); //!< \f$grad_xgrad_y/(4\pi r)\f$

} // end of namespace xlifepp

#endif // HELMHOLTZ3DKERNEL_HPP
