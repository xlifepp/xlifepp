/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Helmholtz3dKernel.cpp
  \author E. Lunéville
  \since 19 jun 2014
  \date  06 jan 2017

  \brief Implementation of Helmholtz 3D kernels

  Helmholtz3D kernel  K(k; x, y)=(1/4 pi*|x-y|) exp(ik|x-y|)
*/

#include "Helmholtz3dKernel.hpp"
#include "utils.h"

namespace xlifepp
{


//--------------------------------------------------------------------------------------------
//construct Helmholtz3d Kernel: G(k; x, y)=(1/4 pi*|x-y|) exp(ik|x-y|)
//--------------------------------------------------------------------------------------------

//construct a Helmholtz3d Kernel
Kernel Helmholtz3dKernel(Parameters& pars)
{
  Kernel K;
  initHelmholtz3dKernel(K,pars);
  return K;
}

Kernel Helmholtz3dKernel(const real_t& k)
{
  Kernel K;
  Parameters pars;
  pars<<Parameter(k,"k");
  initHelmholtz3dKernel(K,pars);
  return K;
}

Kernel Helmholtz3dKernel(const complex_t& k)
{
  Kernel K;
  Parameters pars;
  pars<<Parameter(k,"k");
  initHelmholtz3dKernel(K,pars);
  return K;
}

void initHelmholtz3dKernel(Kernel& K, Parameters& pars)
{
  K.name="Helmholtz 3D kernel";
  K.shortname="Helmz3D";
  K.singularType =_r_;
  K.singularOrder = -1;
  K.singularCoefficient = over4pi_;
  K.symmetry=_symmetric;
  K.userData.push(pars);
  K.kernel = Function(Helmholtz3d, K.userData);
  K.gradx = Function(Helmholtz3dGradx, K.userData);
  K.grady = Function(Helmholtz3dGrady, K.userData);
  K.ndotgradx = Function(Helmholtz3dNxdotGradx, K.userData);
  K.ndotgrady = Function(Helmholtz3dNydotGrady, K.userData);
  K.gradxy = Function(Helmholtz3dGradxy, K.userData);  //DOES NOT WORK FOR THE MOMENT  (adapt in Matrix)
  K.singPart = new Kernel(Helmholtz3dKernelSing(K.userData));
  K.regPart = new Kernel(Helmholtz3dKernelReg(K.userData));
}

complex_t Helmholtz3d(const Point& x, const Point& y, Parameters& pa)
{
  complex_t k = pa("k");
  real_t r = x.distance(y);
  complex_t ikr = i_ * k * r;
  return over4pi_ * std::exp(ikr) / r;
}

//derivatives
Vector<complex_t> Helmholtz3dGradx(const Point& x, const Point& y, Parameters& pa)
{
  complex_t k = pa("k");
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  complex_t ikr = i_ * k * r;
  complex_t dr = (ikr - 1.) / r2;
  Vector<complex_t> g1(3);
  scaledVectorTpl(over4pi_ * exp(ikr)*dr / r, x.begin(), x.end(), y.begin(), g1.begin());
  return g1;
}

Vector<complex_t> Helmholtz3dGrady(const Point& x, const Point& y,Parameters& pa)
{
  complex_t k = pa("k");
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  complex_t ikr = i_ * k * r;
  complex_t dr = (ikr - 1.) / r2;
  Vector<complex_t> g1(3);
  scaledVectorTpl(-over4pi_ * exp(ikr)*dr / r, x.begin(), x.end(), y.begin(), g1.begin());
  return g1;
}

complex_t Helmholtz3dNxdotGradx (const Point& x, const Point& y, Parameters& pa)
{
  complex_t k = pa("k");
  Vector<real_t>& nxp = getNx();
  std::vector<real_t>::const_iterator itx=x.begin(),ity=y.begin(), itn=nxp.begin();
  real_t d1=(*itx++ - *ity++), d2=(*itx++ - *ity++), d3=(*itx - *ity);
  real_t rn = d1* *itn++; rn+= d2* *itn++; rn+=d3* *itn;
  real_t r2= d1*d1+d2*d2+d3*d3; //r3*=r2*std::sqrt(r2);
  real_t r=std::sqrt(r2);
  complex_t ikr = i_ * k * r;
  complex_t dr = (ikr - 1.) / r2;
  return rn*over4pi_*exp(ikr)*dr/r;
}

complex_t Helmholtz3dNydotGrady(const Point& x, const Point& y, Parameters& pa)
{
  complex_t k = pa("k");
  Vector<real_t>& nyp = getNy();
  std::vector<real_t>::const_iterator itx=x.begin(),ity=y.begin(), itn=nyp.begin();
  real_t d1=(*itx++ - *ity++), d2=(*itx++ - *ity++), d3=(*itx - *ity);
  real_t rn = d1* *itn++; rn+= d2* *itn++; rn+=d3* *itn;
  real_t r2= d1*d1+d2*d2+d3*d3; //r3*=r2*std::sqrt(r2);
  real_t r=std::sqrt(r2);
  complex_t ikr = i_ * k * r;
  complex_t dr = (ikr - 1.) / r2;
  return -rn*over4pi_*exp(ikr)*dr/r;
}

Matrix<complex_t> Helmholtz3dGradxy(const Point& x, const Point& y, Parameters& pa )
{
  Vector<Vector<complex_t> > g2(3,Vector<complex_t>(3,complex_t(0.)));
  complex_t k = pa("k");
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  complex_t ik = i_ * k;
  complex_t ikr = ik * r;
  complex_t eikr = std::exp(ikr);
  complex_t d2ijr = (ikr - 1.) * eikr;
  complex_t dijr2 = ik * ik * eikr * r * r2 / over4pi_;
  Vector<real_t> gradxLap(3);
  gradxover4pir(x, y, gradxLap);
  Vector<Vector<real_t> >grad2xyLap(3, gradxLap);
  gradxgradyover4pir(x, y, grad2xyLap);

  Vector<Vector<real_t> >::const_iterator it_l2 = grad2xyLap.begin();
  Vector<real_t>::const_iterator li = gradxLap.begin(), lj, it_l2j;
  for (Vector<Vector<complex_t> >::iterator it_g2 = g2.begin(); it_g2 != g2.end(); it_g2++, it_l2++, li++)
  {
    it_l2j = (*it_l2).begin();
    lj = gradxLap.begin();
    for (Vector<complex_t>::iterator it_g2_j = (*it_g2).begin(); it_g2_j != (*it_g2).end(); it_g2_j++, it_l2j++, lj++)
    {
      *it_g2_j = d2ijr** it_l2j - dijr2** li** lj;
    }
  }

  //move g2 to Matrix (to change in futur)
  Matrix<complex_t> m2(3,3);
  Vector<Vector<complex_t> >::iterator it_g2 = g2.begin();
  Vector<complex_t>::iterator it_v;
  Matrix<complex_t>::iterator it_m=m2.begin();
  for(;it_g2!=g2.end();it_g2++)
    for(it_v=it_g2->begin(); it_v!=it_g2->end();it_v++, it_m++) *it_m=*it_v;  //travel row
  return m2;
}

//--------------------------------------------------------------------------------------------
//construct Helmholtz3d Kernel regular part: G_reg(k; x, y)=(exp(i*k*r)-1)/(4*pi*r)
//--------------------------------------------------------------------------------------------

//construct a Helmholtz3d Kernel regular part
Kernel Helmholtz3dKernelReg(Parameters& pars)
{
    Kernel K;
    K.name="Helmholtz 3D kernel, regular part";
    K.shortname="Helmz3D_reg";
    K.singularType =_notsingular;
    K.singularOrder = 0;
    K.singularCoefficient = over4pi_;
    K.symmetry=_symmetric;
    K.userData = pars;
    K.kernel = Function(Helmholtz3dReg, K.userData);
    K.gradx = Function(Helmholtz3dGradxReg, K.userData);
    K.grady = Function(Helmholtz3dGradyReg, K.userData);
    K.ndotgrady = Function(Helmholtz3dNydotGradyReg, K.userData);
    K.gradxy = Function(Helmholtz3dGradxyReg, K.userData);  //DOES NOT WORK FOR THE MOMENT  (adapt in Matrix)
    K.singPart = nullptr;
    K.regPart = nullptr;
    return K;
}


complex_t Helmholtz3dReg(const Point& x, const Point& y, Parameters& pa)
{
  complex_t g;
  complex_t k = pa("k");
  real_t r = x.distance(y);
  complex_t ikr = i_ * k * r;
  if (std::abs(ikr) < 1.e-04)
  {
    int n=4; // for abs(kr)<1.e-4 this is a good choice for n (checked)
    g = 1 + ikr / n--;
    while (n > 1) {g = 1 + g * ikr / n--;}
    g *= i_ * over4pi_ * k;
  }
  else g = over4pi_ * (std::exp(ikr) - 1.) / r;
  return g;
}

Vector<complex_t> Helmholtz3dGradxReg(const Point& x, const Point& y, Parameters& pa)
{
  complex_t k = pa("k");
  real_t r = x.distance(y);
  complex_t ikr = i_ * k * r;
  complex_t t = over4pi_ * (1. + std::exp(ikr)*(ikr - 1.))/ (r*r);              //grad_x of the regular part
  //complex_t t = over4pi_ * (1.-0.5*ikr*ikr + std::exp(ikr)*(ikr - 1.))/(r*r); //regular part of grad_x(H)
  Vector<complex_t> g(3);
  scaledVectorTpl(t/ r, x.begin(), x.end(), y.begin(), g.begin());
  return g;
}

Vector<complex_t> Helmholtz3dGradyReg(const Point& x, const Point& y, Parameters& pa)
{
  complex_t k = pa("k");
  real_t r = x.distance(y);
  complex_t ikr = i_ * k * r;
  complex_t t = - over4pi_ * (1. + std::exp(ikr)*(ikr - 1.))/(r*r);               //grad_y of the regular part
  //complex_t t = - over4pi_ * (1.-0.5*ikr*ikr + std::exp(ikr)*(ikr - 1.))/(r*r); //regular part of grad_y(H)
  Vector<complex_t> g(3);
  scaledVectorTpl(t/ r, x.begin(), x.end(), y.begin(), g.begin());
  return g;
}

complex_t Helmholtz3dNydotGradyReg(const Point& x, const Point& y, Parameters& pa)
{
  complex_t k = pa("k");
  Vector<real_t>& nyp = getNy();
  std::vector<real_t>::const_iterator itx=x.begin(),ity=y.begin(), itn=nyp.begin();
  real_t d1=(*itx++ - *ity++), d2=(*itx++ - *ity++), d3=(*itx - *ity);
  real_t rn = d1* *itn++; rn+= d2* *itn++; rn+=d3* *itn;
  real_t r2= d1*d1+d2*d2+d3*d3;
  real_t r=std::sqrt(r2);
  complex_t ikr = i_ * k * r;
  return -rn*over4pi_*(1. + std::exp(ikr)*(ikr - 1.))/(r2*r);
  //return  -rn*over4pi_*((1.+0.5*k*k*r2) + (ikr - 1.)*std::exp(ikr))/(r2*r); //regular part of ny.grad_y(H)
}

Matrix<complex_t> Helmholtz3dGradxyReg(const Point& x, const Point& y, Parameters& pa )
{
  Matrix<complex_t> m(3,3);
  //warning("free_warning","Helmholtz3dGradxyReg not implemented");
  return m;
}

//--------------------------------------------------------------------------------------------
//construct Helmholtz3d Kernel singular part: G_sing(k; x, y)=1/(4*pi*r)
//--------------------------------------------------------------------------------------------

//construct a Helmholtz3d Kernel singular part
Kernel Helmholtz3dKernelSing(Parameters& pars)
{
    Kernel K;
    K.name="Helmholtz 3D kernel, singular part";
    K.shortname="Lap3D";
    K.singularType =_r_;
    K.singularOrder = -1;
    K.singularCoefficient = over4pi_;
    K.symmetry=_symmetric;
    K.userData = pars;
    K.kernel = Function(Helmholtz3dSing, K.userData);
    K.gradx = Function(Helmholtz3dGradxSing, K.userData);
    K.grady = Function(Helmholtz3dGradySing, K.userData);
    K.gradxy = Function(Helmholtz3dGradxySing, K.userData);  //DOES NOT WORK FOR THE MOMENT  (adapt in Matrix)
    K.singPart = nullptr;
    K.regPart = nullptr;
    return K;
}


complex_t Helmholtz3dSing(const Point& x, const Point& y, Parameters& pa)
{
  real_t r = x.distance(y);
  return over4pi_/r;
}

Vector<complex_t> Helmholtz3dGradxSing(const Point& x, const Point& y, Parameters& pa)
{
  real_t r = x.distance(y);
  complex_t t = -over4pi_ / (r*r*r);
  Vector<complex_t> g(3);
  scaledVectorTpl(t, x.begin(), x.end(), y.begin(), g.begin());
  return g;
}

Vector<complex_t> Helmholtz3dGradySing(const Point& x, const Point& y, Parameters& pa)
{
  real_t r = x.distance(y);
  complex_t t = over4pi_ / (r*r*r);
  Vector<complex_t> g(3);
  scaledVectorTpl(t, x.begin(), x.end(), y.begin(), g.begin());
  return g;
}

Matrix<complex_t> Helmholtz3dGradxySing(const Point& x, const Point& y, Parameters& pa )
{
  Matrix<complex_t> m(3,3);
  //warning("free_warning","Helmholtz3dGradxySing not implemented");
  return m;
}

//usefull functions

void over4pir(const Point& x, const Point& y, real_t& g)
{
  g = over4pi_ / x.distance(y);
}

void gradxover4pir(const Point& x, const Point& y, Vector<real_t>& g1)
{
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  real_t dr = -over4pi_ / r2;
  scaledVectorTpl(dr / r, x.begin(), x.end(), y.begin(), g1.begin());
}

void gradxgradyover4pir(const Point& x, const Point& y, Vector<Vector<real_t> >& g2)
{
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  real_t r3 = r * r2;
  real_t over4pir3 = over4pi_ / r3;
  real_t threeover4pir5 = 3.*over4pir3 / r2;
  Vector<Vector<real_t> >::iterator it_g2 = g2.begin();
  // change g2 storage to matrix ??? then use line below
  // tensorProductTpl_scaled(threeover4pir5, x.begin(), x.end(), y.begin(), y.end(), g2.begin());
  // and don't forget diagonal extras coeff. "over4pir3"
  Vector<real_t>::const_iterator y_i(y.begin());
  size_t i(0);
  for (Vector<real_t>::const_iterator x_i = x.begin(); x_i != x.end(); x_i++, y_i++, it_g2++, i++)
  {
    Vector<real_t>::iterator it_g2_j = (*(it_g2)).begin();
    Vector<real_t>::const_iterator y_j(y.begin());
    for (Vector<real_t>::const_iterator x_j = x.begin(); x_j != x.end(); x_j++, y_j++, it_g2_j++)
    {
      *it_g2_j = threeover4pir5 * (*x_i - *y_i) * (*x_j - *y_j);
    }
    *((*it_g2).begin() + i) -= over4pir3;
  }
}

// //--------------------------------------------------------------------------------
// // Kernel decomposition according to singularity exponents:
// // G(k; x, y)=\sum_i F_i(x,y)/|x-y|^{\alpha_0_i}
// //     with F_0(x,y)=(1/4 pi) exp(ikr) & alpha_0_0=1
// //--------------------------------------------------------------------------------
// Vector<int> GreenFunctionHelmholtz3d::selfAlpha()
// {
//  Vector<int> alpha(1); alpha[0] = 1; return alpha;
// }

// void GreenFunctionHelmholtz3d::selfF(const Point& x, const Point& y, Vector<complex_t>& F)
// {
//  real_t k = real((*params_p)("k"));
//  real_t r = x.distance(y);
//  complex_t ikr = complex_t(0., 1.) * k * r;
//  F[0] = over4pi * std::exp(ikr);
// }

// /*
// Kernel gradient decomposition according to singularity exponents:
// grad_x G(k; x, y)=\sum_i GxF_i(x,y)(x-y)/|x-y|^{\alpha_x_i}
// where \sum_i GxF_i(x,y)=(1/4 pi) (ikr-1) exp(ikr)/r^3
//    with GxF_0(x,y)= (1/4 pi) ik exp(ikr) & alpha_x_0=2
//         GxF_1(x,y)=-(1/4 pi) exp(ikr)    & alpha_x_1=3
// */
// Vector<int> GreenFunctionHelmholtz3d::gradxAlpha()
// {
//  Vector<int> alpha(2);
//  alpha[0] = 2; alpha[1] = 3;
//  return alpha;
// }

// void GreenFunctionHelmholtz3d::gradxF(const Point& x, const Point& y, Vector<complex_t>& GxF)
// {
//  real_t k = real((*params_p)("k"));
//  real_t r = x.distance(y);
//  complex_t ik = complex_t(0, k);
//  complex_t over4pieikr = over4pi * std::exp(ik * r);
//  GxF[0] = ik * over4pieikr;
//  GxF[1] = -over4pieikr;
// }

// /*
// Kernel gradient decomposition according to singularity exponents:
// grad_y G(k; x, y)=\sum_i GxF_i(x,y)(x-y)/|x-y|^{\alpha_x_i}
// where \sum_i GxF_i(x,y)=(1/4 pi) (ikr-1) exp(ikr)/r^3
//    with GxF_0(x,y)=-(1/4 pi) ik exp(ikr) & alpha_x_0=2
//         GxF_1(x,y)= (1/4 pi) exp(ikr)    & alpha_x_1=3
// */
// Vector<int> GreenFunctionHelmholtz3d::gradyAlpha() { return gradxAlpha(); }

// void GreenFunctionHelmholtz3d::gradyF(const Point& x, const Point& y, Vector<complex_t>& GyF)
// {
//  real_t k = real((*params_p)("k"));
//  real_t r = x.distance(y);
//  complex_t ik = complex_t(0, k);
//  complex_t over4pieikr = over4pi * std::exp(ik * r);
//  GyF[0] = -ik * over4pieikr;
//  GyF[1] = over4pieikr;
// }

// /*
// Kernel second gradient decomposition according to singularity exponents:
// grad_xgrad_y G(k; x, y)=\sum_i GxyF_i(x,y)(x-y)(x-y)^T/|x-y|^{\alpha_xy_i}
//                  +\sum_i GxyFDiag_i(x,y)/|x-y|^{\alpha_xyDiag_i}*Id

//    with GxyF_0(x,y)   =k^2 *(1/4 pi)exp(ikr) & alpha_xy_0=3
//     "   GxyF_1(x,y)   =3i k*(1/4 pi)exp(ikr) & alpha_xy_1=4
//     "   GxyF_2(x,y)   =-3  *(1/4 pi)exp(ikr) & alpha_xy_2=5
//     " GxyFDiag_0(x,y)=-i k*(1/4 pi)exp(ikr) & alpha_xy_ diag_0=2
//     " GxyFDiag_1(x,y)=     (1/4 pi)exp(ikr) & alpha_xy_ diag_1=3
// */
// Vector<int> GreenFunctionHelmholtz3d::grad2xyAlpha()
// {
//  Vector<int> alpha(3);
//  alpha[0] = 3; alpha[1] = 4; alpha[2] = 5;
//  return alpha;
// }

// void GreenFunctionHelmholtz3d::grad2xyF(const Point& x, const Point& y, Vector<complex_t>& GxyF)
// {
//  real_t k = real((*params_p)("k"));
//  real_t r = x.distance(y);
//  complex_t ik = complex_t(0, k);
//  complex_t over4pieikr = over4pi * exp(ik * r);
//  GxyF[0] = k * k * over4pieikr;
//  GxyF[1] = 3.*ik * over4pieikr;
//  GxyF[2] = -3.*over4pieikr;
// }

// Vector<int> GreenFunctionHelmholtz3d::grad2xyAlphaDiag()
// {
//  Vector<int> alpha(2);
//  alpha[0] = 2; alpha[1] = 3;
//  return alpha;
// }

// void  GreenFunctionHelmholtz3d::grad2xyFDiag(const Point& x, const Point& y, Vector<complex_t>& GxyFDiag)
// {
//  real_t k = real((*params_p)("k"));
//  real_t r = x.distance(y);
//  complex_t ik = complex_t(0, k);
//  complex_t over4pieikr = over4pi * exp(ik * r);
//  GxyFDiag[0] = -ik * over4pieikr;
//  GxyFDiag[1] = over4pieikr;
// }

// /*
//  "Regular part of" kernel second gradient decomposition according to singularity exponents:
//  grad_xgrad_y [ G(k; x, y)-G(0; x, y) ]
//                  =\sum_i GxyF_i(x,y)(x-y)(x-y)^T/|x-y|^{\alpha_xy_i}
//                  +\sum_i GxyFDiag_i(x,y)/|x-y|^{\alpha_xyDiag_i}*Id

//    with GxyF_0(x,y)   =k^2 *(1/4 pi)exp(ikr)              & alpha_xy_0=3
//     "   GxyF_1(x,y)   =3i k*(1/4 pi)exp(ikr)              & alpha_xy_1=4
//     "   GxyF_2(x,y)   =-3  *(1/4 pi) [ exp(ikr)-1 ]/r & alpha_xy_2=4
//     " GxyFDiag_0(x,y)=-i k*(1/4 pi)exp(ikr)              & alpha_xy_ diag_0=2
//     " GxyFDiag_1(x,y)=     (1/4 pi) [ exp(ikr)-1 ]/r & alpha_xy_ diag_1=2
// */
// Vector<int> GreenFunctionHelmholtz3d::grad2xyRegAlpha()
// {
//  Vector<int> alpha(3);
//  alpha[0] = 3; alpha[1] = 4; alpha[2] = 4;
//  return alpha;
// }

// void GreenFunctionHelmholtz3d::grad2xyRegF(const Point& x, const Point& y, Vector<complex_t>& GxyF)
// {
//  real_t k = real((*params_p)("k"));
//  real_t r = x.distance(y);
//  complex_t ik = complex_t(0, k);
//  complex_t over4pieikr = over4pi * exp(ik * r);
//  GxyF[0] = k * k * over4pieikr;
//  GxyF[1] = 3.*ik * over4pieikr;
//  selfReg(x, y, GxyF[2]);
//  GxyF[2] *= -3;
// }

// Vector<int> GreenFunctionHelmholtz3d::grad2xyRegAlphaDiag()
// {
//  Vector<int> alpha(2);
//  alpha[0] = 2; alpha[1] = 2;
//  return alpha;
// }

// void GreenFunctionHelmholtz3d::grad2xyRegFDiag(const Point& x, const Point& y, Vector<complex_t>& GxyFDiag)
// {
//  real_t k = real((*params_p)("k"));
//  real_t r = x.distance(y);
//  complex_t ik = complex_t(0, k);
//  GxyFDiag[0] = -ik * over4pi * exp(ik * r);
//  selfReg(x, y, GxyFDiag[1]);
// }

} // end of namespace xlifepp
