/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Helmholtz2dKernel.hpp
  \author E. Lunéville
  \since 19 jun 2014
  \date  19 jun 2014

  \brief Definition of Helmholtz 2D kernels

  Header to deal with Helmholtz 2D kernel:
         in free space:
            K(x,y)=i/4 H^{(1)}_0(k*|x-y|)
            where H^{(1)}_0 is Hankel function of the first kind of order 0 : H_0^{(1)}(z)=J_0(z) + i Y_0(z)
         in the strip ]-inf,inf[x]0,h[ :
            using either modal expansion or images expansion

*/
#ifndef HELMHOLTZ2DKERNEL_HPP
#define HELMHOLTZ2DKERNEL_HPP

#include "utils.h"

namespace xlifepp
{

//==========================================================================================================
// free space kernel
void initHelmholtz2dKernel(Kernel&, Parameters&); //!< initialize kernel data
Kernel Helmholtz2dKernel(Parameters& = defaultParameters);     //!< construct a Helmholtz2d kernel
Kernel Helmholtz2dKernelReg(Parameters& = defaultParameters);  //!< construct a Helmholtz2d kernel, regular part
Kernel Helmholtz2dKernelSing(Parameters& = defaultParameters); //!< construct a Helmholtz2d kernel, singular part
Kernel Helmholtz2dKernel(const real_t& k);        //!< construct a Helmholtz2d kernel from real k
//Kernel Helmholtz2dKernel(const complex_t& k);     //!< construct a Helmholtz2d kernel from complex k
//main computation functions
complex_t Helmholtz2d(const Point& x, const Point& y, Parameters& pa = defaultParameters);                  //!< value
Vector<complex_t> Helmholtz2dGradx(const Point& x, const Point& y, Parameters& pa = defaultParameters);     //!< gradx
Vector<complex_t> Helmholtz2dGrady(const Point& x, const Point& y, Parameters& pa = defaultParameters);     //!< grady
Matrix<complex_t> Helmholtz2dGradxy(const Point& x, const Point& y, Parameters& pa = defaultParameters);    //!< gradxy
complex_t Helmholtz2dNxdotGradx(const Point& x, const Point&y, Parameters& pa = defaultParameters);         //!< nx.gradx
complex_t Helmholtz2dNydotGrady(const Point& x, const Point&y, Parameters& pa = defaultParameters);         //!< ny.grady
complex_t Helmholtz2dReg(const Point& x, const Point& y, Parameters& pa = defaultParameters);               //!< reg value
Vector<complex_t> Helmholtz2dGradxReg(const Point& x, const Point& y, Parameters& pa = defaultParameters);  //!< reg gradx
Vector<complex_t> Helmholtz2dGradyReg(const Point& x, const Point& y, Parameters& pa = defaultParameters);  //!< reg grady
Matrix<complex_t> Helmholtz2dGradxyReg(const Point& x, const Point& y, Parameters& pa = defaultParameters); //!< reg gradxy
complex_t Helmholtz2dSing(const Point& x, const Point& y, Parameters& pa = defaultParameters);              //!< sing value
Vector<complex_t> Helmholtz2dGradxSing(const Point& x, const Point& y, Parameters& pa = defaultParameters); //!< sing gradx
Vector<complex_t> Helmholtz2dGradySing(const Point& x, const Point& y, Parameters& pa = defaultParameters); //!< sing grady
Matrix<complex_t> Helmholtz2dGradxySing(const Point& x, const Point& y, Parameters& pa = defaultParameters);//!< sing gradxy

//==========================================================================================================
/*!Helmholtz kernel in the strip ]-inf,+inf[x]0,h[ with either Dirichlet or Neumann boundary condition
  computed using modal expansion far from xs axis and accelerated image expansion close to the xs axis
  manage following parameters
    number_t bc: boundary type on both sides (0=Dirichlet,1=Neumann, ...)
    real_t k: wave number
    real_t h: strip height (default 1)
    number_t N: maximum of terms in expansion (default 1000, must be greater than the number of propagative modes!)
    real_t l: abcissa separating  image expansion and modal expansion (default h/10)
    real_t eps: threshold used to cut expansions , say |an|<eps, (default 1E-6)
*/
enum BoundaryCondionType{_Dirichlet, _Neumann};

complex_t Helmholtz2dStrip(const Point& x, const Point& y, Parameters& pa = defaultParameters);
complex_t Helmholtz2dStripDir(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps);
complex_t Helmholtz2dStripNeu(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps);
Vector<complex_t> Helmholtz2dStripGradx(const Point& x, const Point& y, Parameters& pa = defaultParameters);
Vector<complex_t> Helmholtz2dStripGradxDir(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps);
Vector<complex_t> Helmholtz2dStripGradxNeu(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps);
Vector<complex_t> Helmholtz2dStripGrady(const Point& x, const Point& y, Parameters& pa = defaultParameters);
Vector<complex_t> Helmholtz2dStripGradyDir(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps);
Vector<complex_t> Helmholtz2dStripGradyNeu(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps);
Matrix<complex_t> Helmholtz2dStripGradxy(const Point& x, const Point& y, Parameters& pa = defaultParameters);
Matrix<complex_t> Helmholtz2dStripGradxyDir(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps);
Matrix<complex_t> Helmholtz2dStripGradxyNeu(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps);
complex_t Helmholtz2dStripNxdotGradx(const Point& x, const Point&y, Parameters& pa = defaultParameters);
complex_t Helmholtz2dStripNydotGrady(const Point& x, const Point&y, Parameters& pa = defaultParameters);
void initHelmholtz2dStripKernel(Kernel&, Parameters&);          //!< initialize kernel data
Kernel Helmholtz2dStripKernel(Parameters& = defaultParameters); //!< construct a Helmholtz2d kernel
Kernel Helmholtz2dStripKernel(BoundaryCondionType bct, real_t k, real_t h=1.,
                              number_t n=1000, real_t l=-1., real_t e=1.E-6);  //!< construct a Helmholtz2dStrip kernel from k, h, n, ...

//==========================================================================================================
/*!Helmholtz kernel in a half-plane with either Dirichlet or Neumann boundary condition
   half plane P is defined by AY.n >0 where A(a1,a2) is a point of a line L, t = (t1,t2) the tangent vector and n=(-t2,t1)
   kernel is built from source image Ys = 2A - Y +2*(AY.t)t/|t|^2

        Hd(X,Y) = H(X,Y) - H(X,Ys)   satisfy Hd(X,Y) =0 for Y in L (say Dirichlet condition)
        Hd(X,Y) = H(X,Y) + H(X,Ys)   satisfy grad(Hd(X,Y)).n =0 for Y in L (say Neumann condition)

   Note that dy1(Ys1) = -1+2*t1^2/|t|^2    dy2(Ys1) = 2*t1*t2/|t|^2
             dy1(Ys2) =  2*t1^2/|t|^2      dy2(Ys2) = -1 + 2*t2^2/|t|^2

  This kernel manage the following parameters
    bc: boundary condition type on line L (_Dirichlet (default),_Neumann)
    k: wave number
    a,b: origin of the line (default (0,0))
    t1,t2  : tangent vector of the line (default (1,0))
*/

complex_t Helmholtz2dHalfPlane(const Point& x, const Point& y, Parameters& pa = defaultParameters);
Vector<complex_t> Helmholtz2dHalfPlaneGradx(const Point& x, const Point& y, Parameters& pa = defaultParameters);
Vector<complex_t> Helmholtz2dHalfPlaneGrady(const Point& x, const Point& y, Parameters& pa = defaultParameters);
Matrix<complex_t> Helmholtz2dHalfPlaneGradxy(const Point& x, const Point& y, Parameters& pa = defaultParameters);
complex_t Helmholtz2dHalfPlaneNxdotGradx(const Point& x, const Point&y, Parameters& pa = defaultParameters);
complex_t Helmholtz2dHalfPlaneNydotGrady(const Point& x, const Point&y, Parameters& pa = defaultParameters);
void initHelmholtz2dHalfPlaneKernel(Kernel&, Parameters&);          //!< initialize kernel data
Kernel Helmholtz2dHalfPlaneKernel(Parameters& = defaultParameters); //!< construct a Helmholtz2dHalfPlane kernel
Kernel Helmholtz2dHalfPlaneKernel(real_t k, const std::vector<real_t>& t=Point(1.,0.),
                                  const Point& A=Point(0.,0.),BoundaryCondionType bct=_Dirichlet);  //!< construct a Helmholtz2dHalfPlane kernel from k, point, vector, bc
Kernel Helmholtz2dHalfPlaneKernel(real_t k, real_t t1=1., real_t t2=0., real_t a=0., real_t b=0.,
                                  BoundaryCondionType bct=_Dirichlet);  //!< construct a Helmholtz2dHalfPlane kernel from from k, point, vector, bc
} // end of namespace xlifepp
#endif // HELMHOLTZ2DKERNEL_HPP
