/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Helmholtz2dKernel.cpp
  \author E. Lunéville
  \since 19 jun 2014
  \date  19 jun 2014

  \brief Implementation of Helmholtz 2D kernels

  Helmholtz2D kernel  K(x,y)=i/4 H^{(1)}_0(k*|x-y|)

  where H^{(1)}_0 is Hankel function of the first kind of order 0 :
               H_0^{(1)}(z)=J_0(z) + i Y_0(z)
*/

#include "Helmholtz2dKernel.hpp"
#include "../specialFunctions/specialFunctions.hpp"
#include "utils.h"
#include <numeric>


namespace xlifepp
{

//--------------------------------------------------------------------------------------------
//construct Helmholtz2d Kernel
//--------------------------------------------------------------------------------------------

Kernel Helmholtz2dKernel(const real_t& k)
{
  Kernel K;
  Parameters pars;
  pars<<Parameter(k,"k");
  initHelmholtz2dKernel(K,pars);
  return K;
}

Kernel Helmholtz2dKernel(Parameters& pars)
{
  Kernel K;
  if(!pars.contains("k")) error("free_error"," no parameter ""k"" in Helmholtz2dKernel parameters");
  if(pars("k").type_!=_real) error("free_error"," parameter ""k"" has to be a real for Helmholtz2dKernel");
  initHelmholtz2dKernel(K,pars);
  return K;
}

void initHelmholtz2dKernel(Kernel& K, Parameters& pars)
{
  K.dimPoint=2;
  K.name="Helmholtz 2D kernel";
  K.shortname="Helmz2D";
  K.singularType = _logr;
  K.singularOrder = 1;
  K.singularCoefficient= -over2pi_;
  K.symmetry=_symmetric;
  K.userData.push(pars);
  K.kernel = Function(Helmholtz2d, 2,K.userData);
  K.gradx = Function(Helmholtz2dGradx, 2, K.userData);
  K.grady = Function(Helmholtz2dGrady, 2, K.userData);
  K.gradxy = Function(Helmholtz2dGradxy, 2, K.userData);
  K.ndotgradx = Function(Helmholtz2dNxdotGradx, 2, K.userData);
  K.ndotgrady = Function(Helmholtz2dNydotGrady, 2, K.userData);
  K.singPart = new Kernel(Helmholtz2dKernelSing(pars));
  K.regPart = new Kernel(Helmholtz2dKernelReg(pars));
}

complex_t Helmholtz2d(const Point& x, const Point& y, Parameters& pa)
{
  real_t k = pa("k").get_r();
  real_t r = x.distance(y);
  return .25 * i_ * hankelH10(k * r);
}

//derivatives
Vector<complex_t> Helmholtz2dGradx(const Point& x, const Point& y, Parameters& pa)
{
  real_t k = pa("k").get_r();
  real_t r = x.distance(y);
  complex_t dr = -.25 * i_ * k * hankelH11(k * r);
  Vector<complex_t> gx(2,complex_t(0.));
  gx(1)=dr*(x(1)-y(1))/r;
  gx(2)=dr*(x(2)-y(2))/r;
  return gx;
}

Vector<complex_t> Helmholtz2dGrady(const Point& x, const Point& y,Parameters& pa)
{
  real_t k = pa("k").get_r();
  real_t r = x.distance(y);
  complex_t dr = .25 * i_ * k * hankelH11(k * r);
  Vector<complex_t> gy(2,complex_t(0.));
  gy(1)=dr*(x(1)-y(1))/r;
  gy(2)=dr*(x(2)-y(2))/r;
  return gy;
}

complex_t Helmholtz2dNxdotGradx(const Point& x, const Point& y, Parameters& pa)
{
  Vector<complex_t> gx = Helmholtz2dGradx(x,y,pa);
  Vector<real_t>& nxp = getNx();
  std::vector<real_t>::iterator itn=nxp.begin();
  std::vector<complex_t>::const_iterator itgx=gx.begin();
  complex_t r = (*itn++ * *itgx++);
  return   r += (*itn * *itgx);
}

complex_t Helmholtz2dNydotGrady(const Point& x, const Point& y, Parameters& pa)
{
  Vector<real_t>& nyp = getNy();
  std::vector<real_t>::iterator itn=nyp.begin();
  real_t k = pa("k").get_r();
  real_t r= x.distance(y);
  complex_t dr= 0.25* i_ * k /r *hankelH11(k*r);
  complex_t res=(x(1)-y(1))* *itn++;
  res+=(x(2)-y(2))* *itn;
  res*=dr;
  return res;
}

// complex_t Helmholtz2dNydotGrady(const Point& x, const Point& y, Parameters& pa)
// {
//   Vector<complex_t> gy = Helmholtz2dGrady(x,y,pa);
//   const std::vector<real_t>* nyp=static_cast<const std::vector<real_t>*>(pa.list_[1]->p_);
//   std::vector<complex_t>::const_iterator itgy=gy.begin();
//   std::vector<real_t>::const_iterator itn=nyp->begin();
//   complex_t r = (*itn++ * *itgy++);
//   return   r += (*itn * *itgy);
// }

/*
  gradxy stored as the matrix  | dx1dy1   dx2dy1 |
                               | dx1dy2   dx2dy2 |
*/
Matrix<complex_t> Helmholtz2dGradxy(const Point& x, const Point& y, Parameters& pa)
{
  Matrix<complex_t> m(2,2);
  real_t k = pa("k").get_r();
  Point z=x-y;
  real_t r = norm2(z);
  z/=r;
  real_t kr=k*r, ksr=k/r;
  complex_t e =-.25 * i_ * hankelH10(kr),
            ep=.25 * i_ * ksr * hankelH11(kr),
            es=k*k*e+2*ep;
  m(1,1)=ep-z(1)*z(1)*es;
  m(2,2)=ep-z(2)*z(2)*es;
  m(1,2)=-z(1)*z(2)*es;
  m(2,1)=m(1,2);
  return m;
//  m(1,1)=z(1)*z(1)*es-ep*(1-z(1)*z(1));
//  m(2,2)=z(2)*z(2)*es-ep*(1-z(2)*z(2));
//  m(1,2)=z(1)*z(2)*es+ep*z(1)*z(2);
//  m(2,1)=m(1,2);
//  return -m;
}

// Vector<complex_t> Helmholtz2ddNxdNyGradxy(const Point& x, const Point& y, Parameters& pa)
// {
//   Matrix<complex_t> m(2,2) = Helmholtz2dGradxy(x,y,pa);
//   m = -m;

//   complex_t res;

//   const std::vector<real_t>* nxp=static_cast<const std::vector<real_t>*>(pa.list_[0]->p_);
//   const std::vector<real_t>* nyp=static_cast<const std::vector<real_t>*>(pa.list_[1]->p_);
//   std::vector<real_t>::const_iterator itn=nyp->begin();
//   std::vector<real_t>::const_iterator itn=nyp->begin();

//   complex_t res=(x(1)-y(1))* *itn++;
//   res+=(x(2)-y(2))* *itn;
//   res*=dr;
//    return r;
// }

//--------------------------------------------------------------------------------------------
//construct singular part of Helmholtz2d Kernel as a Kernel
//--------------------------------------------------------------------------------------------
Kernel Helmholtz2dKernelSing(Parameters& pars)
{
  Kernel K;
  K.name="Helmholtz 2D kernel, singular part";
  K.shortname="Lap2D";
  K.singularType = _logr;
  K.singularOrder = 1;
  K.singularCoefficient=-over2pi_;
  K.symmetry=_symmetric;
  K.dimPoint=2;
  K.userData.push(pars);
  K.kernel = Function(Helmholtz2dSing, 2, K.userData);
  K.gradx  = Function(Helmholtz2dGradxSing, 2, K.userData);
  K.grady  = Function(Helmholtz2dGradySing, 2, K.userData);
  K.gradxy = Function(Helmholtz2dGradxySing, 2, K.userData);  //DOES NOT WORK FOR THE MOMENT  (adapt to Matrix)
  K.singPart = nullptr;
  K.regPart = nullptr;
  return K;
}

//! Singular part of Helmholtz2d function - log(r)/(2*pi)
complex_t Helmholtz2dSing(const Point& x, const Point& y, Parameters& pa)
{
  real_t r = x.distance(y);
  return -over2pi_ * std::log(r);
}

//derivatives
Vector<complex_t> Helmholtz2dGradxSing(const Point& x, const Point& y, Parameters& pa)
{
  real_t r = x.distance(y);
  complex_t dr = -over2pi_ / (r*r);
  Vector<complex_t> gx(2,complex_t(0.));
  gx(1)=dr*(x[0]-y[0]);
  gx(2)=dr*(x[1]-y[1]);
  return gx;
}

Vector<complex_t> Helmholtz2dGradySing(const Point& x, const Point& y,Parameters& pa)
{
  real_t r = x.distance(y);
  complex_t dr = over2pi_ / (r*r);
  Vector<complex_t> gy(2,complex_t(0.));
  gy(1)=dr*(x[0]-y[0]);
  gy(2)=dr*(x[1]-y[1]);
  return gy;
}

Matrix<complex_t> Helmholtz2dGradxySing(const Point& x, const Point& y, Parameters& pa)
{
  Matrix<complex_t> m(2,2);
  //warning("free_warning","Helmholtz2dGradxySing not implemented");
  return m;
}

//--------------------------------------------------------------------------------------------
//construct regular part of Helmholtz2d Kernel as a Kernel
//   for the moment, it is encoded as K - Ksing
//   to be improved in future
//--------------------------------------------------------------------------------------------
Kernel Helmholtz2dKernelReg(Parameters& pars)
{
  Kernel K;
  K.name="Helmholtz 2D kernel, regular part";
  K.shortname="Helmz2D reg";
  K.singularType = _notsingular;
  K.singularOrder = 0;
  K.singularCoefficient = 1.;
  K.symmetry=_symmetric;
  K.dimPoint=2;
  K.userData.push(pars);
  K.kernel = Function(Helmholtz2dReg, 2, K.userData);
  K.gradx = Function(Helmholtz2dGradxReg, 2, K.userData);
  K.grady = Function(Helmholtz2dGradyReg, 2, K.userData);
  K.gradxy = Function(Helmholtz2dGradxyReg, 2, K.userData);  //DOES NOT WORK FOR THE MOMENT  (adapt in Matrix)
  K.singPart = nullptr;
  K.regPart = nullptr;
  return K;
}

//! Regular part of Helmholtz2d function i/4 H^{(1)}_0(k*r) - log(k*r)/(2*pi)
complex_t Helmholtz2dReg(const Point& x, const Point& y, Parameters& pa)
{
  return Helmholtz2d(x,y,pa)-Helmholtz2dSing(x,y,pa);
}

//derivatives
Vector<complex_t> Helmholtz2dGradxReg(const Point& x, const Point& y, Parameters& pa)
{
  return Helmholtz2dGradx(x,y,pa)-Helmholtz2dGradxSing(x,y,pa);
}

Vector<complex_t> Helmholtz2dGradyReg(const Point& x, const Point& y,Parameters& pa)
{
  return Helmholtz2dGrady(x,y,pa)-Helmholtz2dGradySing(x,y,pa);
}

Matrix<complex_t> Helmholtz2dGradxyReg(const Point& x, const Point& y, Parameters& pa)
{
  Matrix<complex_t> m(2,2);
  //warning("free_warning","Helmz2dGradxyReg not implemented");
  return m;
}

// // Kernel decomposition according to singularity exponents:
// //   G(x,y)=\sum_i F_i(x,y) / |x-y|^{\alpha_i}
// //         =-1/2pi Log(r) + i/4 ( H^{(1)}_0(k*r) - 2i/pi Log(r) )  : alpha_0_0=-1
// std::vector<int> GreenFunctionHelmholtz2d::selfAlpha()
// {
//  std::vector<int> alpha;
//  alpha.push_back(-1);
//  return alpha;
// }

// void GreenFunctionHelmholtz2d::selfF(const Point& x, const Point& y, std::vector<complex_t>& F)
// {
//  Parameter kp = (*params_p)("k");
//  real_t k = real(kp);
//  real_t r = x.distance(y);
//  complex_t fake = 0 * k * r;
//  F[0] = fake;
// }

// // Kernel gradient decomposition according to singularity exponents:
// //   grad_x G(x,y)=\sum_i GF_i(x,y)(x-y) / |x-y|^{\alpha_x_i}
// std::vector<int> GreenFunctionHelmholtz2d::gradxAlpha()
// {
//  std::vector<int> alpha;
//  alpha.push_back(1); alpha.push_back(2);
//  return alpha;
// }

// void GreenFunctionHelmholtz2d::gradxF(const Point& x, const Point& y, std::vector<complex_t>& GxF)
// {
//  Parameter kp = (*params_p)("k");
//  real_t k = real(kp);
//  real_t r = x.distance(y);
//  complex_t fake = 0 * k * r;
//  GxF[0] = fake;
//  GxF[1] = fake;
// }

// std::vector<int> GreenFunctionHelmholtz2d::gradyAlpha()
// {
//  std::vector<int> alpha;
//  alpha.push_back(1); alpha.push_back(2);
//  return alpha;
// }

// void GreenFunctionHelmholtz2d::gradyF(const Point& x, const Point& y, std::vector<complex_t>& GyF)
// {
//  Parameter kp = (*params_p)("k");
//  real_t k = real(kp);
//  real_t r = x.distance(y);
//  complex_t fake = 0 * k * r;
//  GyF[0] = -fake;
//  GyF[1] = -fake;
// }

//===============================================================================
//  Helmholtz2d in the strip ]-inf,+inf[ x ]0,h[
//  with Dirichlet or Neumann condition
//  based on
//    - modal expansion far away the source axis
//    - accelerated images expansion close to the source axis
//===============================================================================
Kernel Helmholtz2dStripKernel(BoundaryCondionType bc, real_t k, real_t h, number_t n, real_t l, real_t eps)
{
  Kernel K;
  Parameters pars;
  pars<<Parameter(number_t(bc),"bc")<<Parameter(k,"k")<<Parameter(h,"h")
      <<Parameter(n,"N") <<Parameter(l,"l")<<Parameter(eps,"eps");
  initHelmholtz2dStripKernel(K,pars);
  return K;
}

Kernel Helmholtz2dStripKernel(Parameters& pars)
{
  Kernel K;
  initHelmholtz2dStripKernel(K,pars);
  return K;
}

void initHelmholtz2dStripKernel(Kernel& K, Parameters& pars)
{
  number_t bc=0, N=1000;
  real_t k=1, h=1, l=-1, eps=1E-6;
  if(pars.contains("bc")) pars.get("bc",bc);
  else pars<<Parameter(bc,"bc");
  if(pars.contains("k")) pars.get("k",k);
  else pars<<Parameter(k,"k");
  if(pars.contains("h")) pars.get("h",h);
  else pars<<Parameter(h,"h");
  if(pars.contains("N")) pars.get("N",N);
  else pars<<Parameter(N,"N");
  if(pars.contains("l")) pars.get("l",l);
  else pars<<Parameter(l,"l");
  if(pars.contains("eps")) pars.get("eps",eps);
  else pars<<Parameter(eps,"eps");
  if(l==-1)
    {
      l=h/10;
      pars("l")=l;
    }
  K.dimPoint=2;
  K.name="Helmholtz 2D kernel in a strip";
  K.shortname="Helmz2D_Strip";
  K.singularType = _logr;
  K.singularOrder = 1;
  K.singularCoefficient= -over2pi_;
  K.symmetry=_symmetric;
  K.userData.push(pars);
  K.kernel = Function(Helmholtz2dStrip, 2, K.userData);
  K.gradx = Function(Helmholtz2dStripGradx, 2, K.userData);
  K.grady = Function(Helmholtz2dStripGrady, 2, K.userData);
  K.gradxy = Function(Helmholtz2dStripGradxy, 2, K.userData);
  K.ndotgradx = Function(Helmholtz2dStripNxdotGradx, 2, K.userData);
  K.ndotgrady = Function(Helmholtz2dStripNydotGrady, 2, K.userData);
  K.singPart = new Kernel(Helmholtz2dKernelSing(pars));
  K.regPart = new Kernel(Helmholtz2dKernelReg(pars));
}

complex_t Helmholtz2dStrip(const Point& x, const Point& y, Parameters& pa)
{
  number_t bc=integer(pa("bc")), N = integer(pa("N"));
  real_t k=real(pa("k")), h=real(pa("h")), l=real(pa("l")), eps=real(pa("eps"));
  if(bc==0) return Helmholtz2dStripDir(x,y,k,h,l,N,eps);
  return Helmholtz2dStripNeu(x,y,k,h,l,N,eps);
}

Vector<complex_t> Helmholtz2dStripGradx(const Point& x, const Point& y, Parameters& pa)
{
  number_t bc=integer(pa("bc")), N = integer(pa("N"));
  real_t k=real(pa("k")), h=real(pa("h")), l=real(pa("l")), eps=real(pa("eps"));
  if(bc==0) return Helmholtz2dStripGradxDir(x,y,k,h,l,N,eps);
  return Helmholtz2dStripGradxNeu(x,y,k,h,l,N,eps);
}

Vector<complex_t> Helmholtz2dStripGrady(const Point& x, const Point& y, Parameters& pa)
{
  number_t bc=integer(pa("bc")), N = integer(pa("N"));
  real_t k=real(pa("k")), h=real(pa("h")), l=real(pa("l")), eps=real(pa("eps"));
  if(bc==0) return Helmholtz2dStripGradyDir(x,y,k,h,l,N,eps);
  return Helmholtz2dStripGradyNeu(x,y,k,h,l,N,eps);
}

complex_t Helmholtz2dStripNxdotGradx(const Point& x, const Point& y, Parameters& pa)
{
  Vector<complex_t> gx = Helmholtz2dStripGradx(x,y,pa);
  std::vector<real_t>& nxp=getNx();
  std::vector<complex_t>::const_iterator itgx=gx.begin();
  std::vector<real_t>::const_iterator itn=nxp.begin();
  complex_t r = (*itn++ * *itgx++);
  return   r += (*itn * *itgx);
}

complex_t Helmholtz2dStripNydotGrady(const Point& x, const Point& y, Parameters& pa)
{
  Vector<complex_t> gy = Helmholtz2dStripGrady(x,y,pa);
  std::vector<real_t>& nyp=getNy();
  std::vector<complex_t>::const_iterator itgy=gy.begin();
  std::vector<real_t>::const_iterator itn=nyp.begin();
  complex_t r = (*itn++ * *itgy++);
  return   r += (*itn * *itgy);
}

Matrix<complex_t> Helmholtz2dStripGradxy(const Point& x, const Point& y, Parameters& pa)
{
  number_t bc=integer(pa("bc")), N = integer(pa("N"));
  real_t k=real(pa("k")), h=real(pa("h")), l=real(pa("l")), eps=real(pa("eps"));
  if(bc==0) return Helmholtz2dStripGradxyDir(x,y,k,h,l,N,eps);
  return Helmholtz2dStripGradxyNeu(x,y,k,h,l,N,eps);
}

complex_t Helmholtz2dStripDir(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps)
{
  complex_t g=0;
  real_t x1=x[0], x2=x[1], y1=y[0], y2=y[1];

  if(std::abs(y2)< 1E-10 || std::abs(h-y2)< 1E-10) return g;  //g=0 on boundary!

  if(std::abs(y1-x1)>l) // modal expansion far away source axis
    {
      number_t Nc=k*h/pi_;  //cut-off
      number_t n=1, cont=0;
      while(n<=Nc ||(n<=N && cont<5)) // stop when 5 subsequent coefficients < eps
        {
          real_t npish=n*pi_/h;
          complex_t betan = std::sqrt(complex_t(npish*npish-k*k));
          if(npish<k) betan=-betan;
          complex_t a=std::sin(npish*y2)*std::sin(npish*x2)*std::exp(-betan*std::abs(x1-y1))/(betan*h);
          g+=a;
          if(std::abs(a)<eps) cont=cont+1; else cont=0;
          n=n+1;
        }
      //theCout<<"|y1-x1|="<<std::abs(y1-x1)<<" n="<<n<<eol;
      return g;
    }
  // accelerated image expansion when close to the source axis
  number_t M=N/2+1;
  complex_t is4=i_/4;
  Vector<complex_t> S(M,complex_t(0.));
  real_t r2=(y1-x1)*(y1-x1);
  real_t rmm,rpm,rmp,rpp;
  complex_t s0,s1,t1;
  number_t n=1;
  real_t er=2*eps;
  rmm=k*std::sqrt((y2-x2)*(y2-x2)+r2);
  rpp=k*std::sqrt((y2+x2)*(y2+x2)+r2);
  S[0] = is4*(hankelH10(rmm)-hankelH10(rpp));
  while(n<M && er>=eps)
    {
      s0=0;
      s1=S[0];
      rmm=y2-x2-2*n*h; rmm=k*std::sqrt(rmm*rmm+r2);
      rpp=y2+x2+2*n*h; rpp=k*std::sqrt(rpp*rpp+r2);
      rmp=y2-x2+2*n*h; rmp=k*std::sqrt(rmp*rmp+r2);
      rpm=y2+x2-2*n*h; rpm=k*std::sqrt(rpm*rpm+r2);
      t1=s1+is4*(hankelH10(rmm)-hankelH10(rpp)+ hankelH10(rmp)-hankelH10(rpm));
      for(number_t q=1; q<=n; ++q)
        {
          g=s0+1./(t1-s1);
          s0=s1; s1=S[q];
          S[q-1]=t1; t1=g;
        }
      S[n]=g;
      if(n>1 && (n+1)%2==1) er=std::abs(S[n]-S[n-2]);
      n=n+1;
    }
  if(n==M+1) warning("free_warning","in Helmholtz2dStripDir, max number of iterations reached");
  //theCout<<"|y1-x1|="<<std::abs(y1-x1)<<" n="<<n<<eol;
  return g;
}

Vector<complex_t> Helmholtz2dStripGradxDir(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps)
{
  Vector<complex_t> g=Helmholtz2dStripGradyDir(x,y,k,h,l,N,eps);
  g[0]=-g[0];
  return g;
}

Vector<complex_t> Helmholtz2dStripGradyDir(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps)
{
  real_t x1=x[0], x2=x[1], y1=y[0], y2=y[1];
  Vector<complex_t> g(2,complex_t(0.));

  if(std::abs(y1-x1)>l) // modal expansion far away source axis
    {
      number_t n, cont=0;
      number_t Nc=k*h/pi_;  //cut-off
      if(std::abs(y2)>1E-10 && std::abs(1-y2)>1E-10) // dG/dy1 (=0 on boundary)
        {
          cont=0; n=1;
          while(n<=Nc ||(n<=N && cont<5)) // 5 subsequent coefficients < eps
            {
              real_t npish=n*pi_/h;
              complex_t betan = std::sqrt(complex_t(npish*npish-k*k));
              if(npish<k) betan=-betan;
              complex_t a=std::sin(npish*x2)*std::sin(npish*y2)*exp(-betan*std::abs(x1-y1))/h;
              g[0]+=a;
              if(std::abs(a)<eps) cont=cont+1;
              else cont=0;
              n=n+1;
            }
          if(y1>x1) g(1)=-g(1);
        }
      n=1; cont=0; // dG/dy2
      while(n<=Nc ||(n<=N && cont<5)) //5 subsequent coefficients < eps
        {
          real_t npish=n*pi_/h;
          complex_t betan = std::sqrt(complex_t(npish*npish-k*k));
          if(npish<k) betan=-betan;
          complex_t a=npish*std::sin(npish*x2)*std::cos(npish*y2)*exp(-betan*std::abs(x1-y1))/(betan*h);
          g[1]+=a;
          if(std::abs(a)<eps) cont=cont+1;
          else cont=0;
          n=n+1;
        }
      return g;
    }
  // accelerated image method
  number_t M=N/2+1;
  complex_t is4=i_/4;
  Vector<complex_t> S(M+1,complex_t(0.));
  real_t r2=(y1-x1)*(y1-x1);
  real_t rm,rp;
  complex_t s0,s1,t1;
  number_t n=1;
  real_t er=2*eps;

  if(std::abs(y2)>1E-10 && std::abs(1-y2)>1E-10 && std::abs(y1-x1)>1E-10) // dG/dy1 (=0 on boundary)
    {
      rm=std::sqrt((y2-x2)*(y2-x2)+r2); rp=std::sqrt((y2+x2)*(y2+x2)+r2);
      S(1) = is4*(y1-x1)*(hankelH11(k*rm)/rm-hankelH11(k*rp)/rp);
      while(n<M && er>=eps)
        {
          s0=0;
          s1=S(1);
          rm = y2-x2-2*n*h;  rm=std::sqrt(rm*rm+r2);
          rp = y2+x2+2*n*h;  rp=std::sqrt(rp*rp+r2);
          t1 = s1+is4*(y1-x1)*(hankelH11(k*rm)/rm-hankelH11(k*rp)/rp);
          rm = y2-x2+2*n*h;  rm=std::sqrt(rm*rm+r2);
          rp = y2+x2-2*n*h;  rp=std::sqrt(rp*rp+r2);
          t1+=is4*(y1-x1)*(hankelH11(k*rm)/rm-hankelH11(k*rp)/rp);
          for(number_t q=1; q<=n; q++)
            {
              g[0]=s0+1./(t1-s1);
              s0=s1;
              s1=S(q+1);
              S(q)=t1;
              t1=g[0];
            }
          S(n+1)=g[0];
          if(n>1 && (n+1)%2==1) er=std::abs(S(n+1)-S(n-1));
          n=n+1;
        }
      if(n==M+1) warning("free_warning","in Helmholtz2dStripGradyDir, max number of iterations reached");
    }

  n=1; er=2*eps;  // dG/dy
  rm = y2-x2-2*n*h;  rm=std::sqrt(rm*rm+r2);
  rp = y2+x2+2*n*h;  rp=std::sqrt(rp*rp+r2);
  S(1) = is4*(hankelH11(k*rm)*(y2-x2)/rm-hankelH11(k*rp)*(y2+x2)/rp);
  while(n<M && er>=eps)
    {
      s0=0;
      s1=S(1);
      rm = y2-x2-2*n*h;  rm=std::sqrt(rm*rm+r2);
      rp = y2+x2+2*n*h;  rp=std::sqrt(rp*rp+r2);
      t1=s1+is4*(hankelH11(k*rm)*(y2-x2-2*n*h)/rm-hankelH11(k*rp)*(y2+x2+2*n*h)/rp);
      rm = y2-x2+2*n*h;  rm=std::sqrt(rm*rm+r2);
      rp = y2+x2-2*n*h;  rp=std::sqrt(rp*rp+r2);
      t1+=is4*(hankelH11(k*rm)*(y2-x2+2*n*h)/rm-hankelH11(k*rp)*(y2+x2-2*n*h)/rp);
      for(number_t q=1; q<=n; q++)
        {
          g[1]=s0+1./(t1-s1);
          s0=s1;
          s1=S(q+1);
          S(q)=t1;
          t1=g[1];
        }
      S(n+1)=g[1];
      if(n>1 && (n+1)%2==1) er=std::abs(S(n+1)-S(n-1));
      n=n+1;
    }
  if(n==M+1) warning("free_warning","in Helmholtz2dStripGradyDir, max number of iterations reached");

  return g;
}

Matrix<complex_t> Helmholtz2dStripGradxyDir(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps)
{
  warning("free_warning","Helmholtz2dStripGradxyDir not yet available");
  return Matrix<complex_t>(2,2);
}


complex_t Helmholtz2dStripNeu(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps)
{
  complex_t g=0;
  real_t x1=x[0], x2=x[1], y1=y[0], y2=y[1];

  if(std::abs(y1-x1)>l) // modal expansion far away the source axis
    {
      Vector<complex_t> beta(N);
      for(number_t n=1; n<=N; ++n)
        {
          real_t npish=(n-1)*pi_/h;
          beta(n) = std::sqrt(complex_t(npish*npish-k*k));
          if(npish<k) beta(n)=-beta(n);
        }
      real_t Nc=k*h/pi_;  // cut-off
      complex_t g=0;
      number_t n=1, cont=0;
      while(n<=Nc ||(n<=N && cont<5)) // 5 subsequent coefficients < eps
        {
          real_t npish=(n-1)*pi_/h;
          complex_t a=std::cos(npish*y2)*std::cos(npish*x2)*exp(-beta(n)*std::abs(y1-x1))/(beta(n)*h);
          if(n==1) a*=0.5;
          g+=a;
          if(std::abs(a)<eps) cont=cont+1;
          else cont=0;
          n=n+1;
        }
      return g;
    }
  // accelerated image method
  number_t M=N/2+1;
  Vector<complex_t> S(M+1,complex_t(0.));
  complex_t is4=i_/4;
  real_t r2=(y1-x1)*(y1-x1);
  real_t rmm,rpm,rmp,rpp;
  complex_t s0,s1,t1;
  number_t n=1;
  real_t er=2*eps;
  rmm=k*std::sqrt((y2-x2)*(y2-x2)+r2);
  rpp=k*std::sqrt((y2+x2)*(y2+x2)+r2);
  S[0] = is4*(hankelH10(rmm)+hankelH10(rpp));
  while(n<M && er>=eps)
    {
      s0=0;
      s1=S[0];
      rmm=y2-x2-2*n*h; rmm=k*std::sqrt(rmm*rmm+r2);
      rpp=y2+x2+2*n*h; rpp=k*std::sqrt(rpp*rpp+r2);
      rmp=y2-x2+2*n*h; rmp=k*std::sqrt(rmp*rmp+r2);
      rpm=y2+x2-2*n*h; rpm=k*std::sqrt(rpm*rpm+r2);
      t1=s1+is4*(hankelH10(rmm)+hankelH10(rpp)+hankelH10(rmp)+hankelH10(rpm));
      for(number_t q=1; q<=n; ++q)
        {
          g=s0+1./(t1-s1);
          s0=s1; s1=S[q];
          S[q-1]=t1; t1=g;
        }
      S[n]=g;
      if(n>1 && (n+1)%2==1) er=std::abs(S[n]-S[n-2]);
      n=n+1;
    }
  if(n==M+1) warning("free_warning","in Helmholtz2dStripNeu, max number of iterations reached");
  return g;
}

Vector<complex_t> Helmholtz2dStripGradxNeu(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps)
{
  Vector<complex_t> g=Helmholtz2dStripGradyNeu(x,y,k,h,l,N,eps);
  g[0]=-g[0];
  return g;
}

Vector<complex_t> Helmholtz2dStripGradyNeu(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps)
{
  real_t x1=x[0], x2=x[1], y1=y[0], y2=y[1];
  Vector<complex_t> g(2,complex_t(0.));

  if(std::abs(y1-x1)>=l) // modal expansion far away source axis
    {
      Vector<complex_t> beta(N+1);
      number_t n, cont=0;
      for(n=1; n<=N; ++n)
        {
          real_t npish=n*pi_/h;
          beta(n) = std::sqrt(complex_t(npish*npish-k*k));
          if(npish<k) beta(n)=-beta(n);
        }
      number_t Nc=real_t(k)*h/pi_;  //cut-off
      cont=0; n=1;
      while(n<=Nc ||(n<=N && cont<5)) // 5 subsequent coefficients < eps
        {
          real_t npish=n*pi_/h;
          complex_t a=std::cos(npish*x2)*std::cos(npish*y2)*exp(-beta(n)*std::abs(x1-y1))/h;
          if(n==1) a*=0.5;
          g[0]+=a;
          if(std::abs(a)<eps) cont=cont+1;
          else cont=0;
          n=n+1;
        }
      if(y1>x1) g[0]*=-1;

      if(std::abs(y2)>1E-10 && std::abs(1-y2)>1E-10) // dG/dy2=0 on boundary
        {
          n=1; cont=0; // dG/dy2
          while(n<=Nc ||(n<=N && cont<5)) //5 subsequent coefficients < eps
            {
              real_t npish=n*pi_/h;
              complex_t a=npish*std::cos(npish*x2)*std::sin(npish*y2)*exp(-beta(n)*std::abs(x1-y1))/(beta(n)*h);
              if(n==1) a*=0.5;
              g[1]+=a;
              if(std::abs(a)<eps) cont=cont+1;
              else cont=0;
              n=n+1;
            }
        }
      return g;
    }
  // accelerated image method
  number_t M=N/2+1;
  complex_t is4=i_/4;
  Vector<complex_t> S(M+1,complex_t(0.));
  real_t z1=x1-y1;
  real_t r2=z1*z1;
  real_t rm,rp;
  complex_t s0,s1,t1;
  number_t n=1;
  real_t er=2*eps;
  if(std::abs(z1)>1E-10) // dG/dy1  (0 on x1 axis)
    {
      rm=std::sqrt((y2-x2)*(y2-x2)+r2); rp=std::sqrt((y2+x2)*(y2+x2)+r2);
      S[0] = is4*(y1-x1)*(hankelH11(k*rm)/rm+hankelH11(k*rp)/rp);
      while(n<M && er>=eps)
        {
          s0=0;
          s1=S[0];
          rm = y2-x2-2*n*h;  rm=std::sqrt(rm*rm+r2);
          rp = y2+x2+2*n*h;  rp=std::sqrt(rp*rp+r2);
          t1 = s1+is4*(y1-x1)*(hankelH11(k*rm)/rm+hankelH11(k*rp)/rp);
          rm = y2-x2+2*n*h;  rm=std::sqrt(rm*rm+r2);
          rp = y2+x2-2*n*h;  rp=std::sqrt(rp*rp+r2);
          t1+=is4*(y1-x1)*(hankelH11(k*rm)/rm+hankelH11(k*rp)/rp);
          for(number_t q=1; q<=n; q++)
            {
              g[0]=s0+1./(t1-s1);
              s0=s1;
              s1=S[q];
              S[q-1]=t1;
              t1=g[0];
            }
          S[n]=g[0];
          if(n>1 && (n+1)%2==1) er=std::abs(S[n]-S[n-2]);
          n=n+1;
        }
      if(n==M+1) warning("free_warning","in Helmholtz2dStripGradyNeu, max number of iterations reached");
    }
  if(std::abs(y2)>1E-10 && std::abs(1-y2)>1E-10) // dG/dy2 (=0 on boundary)
    {
      n=1; er=2*eps;
      rm = y2-x2-2*n*h;  rm=std::sqrt(rm*rm+r2);
      rp = y2+x2+2*n*h;  rp=std::sqrt(rp*rp+r2);
      S[0] = is4*(hankelH11(k*rm)*(y2-x2)/rm+hankelH11(k*rp)*(y2+x2)/rp);
      while(n<M && er>=eps)
        {
          s0=0;
          s1=S[0];
          rm = y2-x2-2*n*h;  rm=std::sqrt(rm*rm+r2);
          rp = y2+x2+2*n*h;  rp=std::sqrt(rp*rp+r2);
          t1=s1+is4*(hankelH11(k*rm)*(y2-x2-2*n*h)/rm+hankelH11(k*rp)*(y2+x2+2*n*h)/rp);
          rm = y2-x2+2*n*h;  rm=std::sqrt(rm*rm+r2);
          rp = y2+x2-2*n*h;  rp=std::sqrt(rp*rp+r2);
          t1+=is4*(hankelH11(k*rm)*(y2-x2+2*n*h)/rm+hankelH11(k*rp)*(y2+x2-2*n*h)/rp);
          for(number_t q=1; q<=n; q++)
            {
              g[1]=s0+1./(t1-s1);
              s0=s1;
              s1=S[q];
              S[q-1]=t1;
              t1=g[1];
            }
          S[n]=g[1];
          if(n>1 && (n+1)%2==1) er=std::abs(S[n]-S[n-2]);
          n=n+1;
        }
      if(n==M+1) warning("free_warning","in Helmholtz2dStripGradyNeu, max number of iterations reached");
    }
  return g;
}

Matrix<complex_t> Helmholtz2dStripGradxyNeu(const Point& x, const Point& y, real_t k,real_t h,real_t l,number_t N, real_t eps)
{
  warning("free_warning","Helmholtz2dStripGradxyNeu not yet available");
  return Matrix<complex_t>(2,2);
}

/*==========================================================================================================
Helmholtz kernel in a half-plane with either Dirichlet or Neumann boundary condition
   half plane P is defined by AY.n >0 where A(a1,a2) is a point of a line L, t = (t1,t2) the tangent vector and n=(-t2,t1)
   kernel is built from source image Ys = 2A - Y +2*(AY.t)t/|t|^2

        Hd(X,Y) = H(X,Y) - H(X,Ys)   satisfy Hd(X,Y) =0 for Y in L (say Dirichlet condition)
        Hd(X,Y) = H(X,Y) + H(X,Ys)   satisfy grad(Hd(X,Y)).n =0 for Y in L (say Neumann condition)

   Note that dy1(Ys1) = -1+2*t1^2/|t|^2    dy2(Ys1) = 2*t1*t2/|t|^2
             dy1(Ys2) =  2*t1*t2/|t|^2     dy2(Ys2) = -1 + 2*t2^2/|t|^2

  This kernel manage the following parameters
    bc: boundary condition type on line L (_Dirichlet (default),_Neumann)
    k: wave number
    a,b: origin of the line (default (0,0))
    t1,t2  : tangent vector of the line (default (1,0))
==========================================================================================================*/
Kernel Helmholtz2dHalfPlaneKernel(real_t k, real_t t1, real_t t2, real_t a, real_t b, BoundaryCondionType bc)
{
  Kernel K;
  Parameters pars;
  pars<<Parameter(number_t(bc),"bc")<<Parameter(k,"k")<<Parameter(t1,"t1")
      <<Parameter(t2,"t2") <<Parameter(a,"a")<<Parameter(b,"b");
  initHelmholtz2dHalfPlaneKernel(K,pars);
  return K;
}

Kernel Helmholtz2dHalfPlaneKernel(Parameters& pars)
{
  Kernel K;
  initHelmholtz2dHalfPlaneKernel(K,pars);
  return K;
}

Kernel Helmholtz2dHalfPlaneKernel(real_t k, const std::vector<real_t>& t, const Point& A, BoundaryCondionType bc)
{
  Kernel K;
  Parameters pars;
  pars<<Parameter(number_t(bc),"bc")<<Parameter(k,"k")<<Parameter(t[0],"t1")
      <<Parameter(t[1],"t2") <<Parameter(A(1),"a")<<Parameter(A(2),"b");
  initHelmholtz2dHalfPlaneKernel(K,pars);
  return K;

}

void initHelmholtz2dHalfPlaneKernel(Kernel& K, Parameters& pars)
{
  number_t bc=number_t(_Dirichlet);
  real_t k=1, a=0., b=0., t1=1., t2=0.;

  if(pars.contains("bc")) bc=integer(pars("bc"));
  else pars<<Parameter(bc,"bc");
  if(pars.contains("k")) pars.get("k",k);
  else pars<<Parameter(k,"k");
  if(pars.contains("a")) pars.get("a",a);
  else pars<<Parameter(a,"a");
  if(pars.contains("b")) pars.get("b",b);
  else pars<<Parameter(b,"b");
  if(pars.contains("t1")) pars.get("t1",t1);
  else pars<<Parameter(t1,"t1");
  if(pars.contains("t2")) pars.get("t2",t2);
  else pars<<Parameter(t2,"t2");

  K.dimPoint=2;
  string_t sbc=" Dirichlet condition", shbc="Dir";
  if(bc!=0) {sbc=" Neumann condition"; shbc="Neu";}
  K.name="Helmholtz 2D kernel in a half-plane with "+sbc;;
  K.shortname="Helmz2D_HalfPlane_"+shbc;
  K.singularType = _logr;
  K.singularOrder = 1;
  K.singularCoefficient= -over2pi_;
  K.symmetry=_noSymmetry;
  K.userData.push(pars);
  K.kernel = Function(Helmholtz2dHalfPlane, 2, K.userData);
  K.gradx = Function(Helmholtz2dHalfPlaneGradx, 2, K.userData);
  K.grady = Function(Helmholtz2dHalfPlaneGrady, 2, K.userData);
  K.gradxy = Function(Helmholtz2dHalfPlaneGradxy, 2, K.userData);
  K.ndotgradx = Function(Helmholtz2dHalfPlaneNxdotGradx, 2, K.userData);
  K.ndotgrady = Function(Helmholtz2dHalfPlaneNydotGrady, 2, K.userData);
  K.singPart = new Kernel(Helmholtz2dKernelSing(pars));
  K.regPart = new Kernel(Helmholtz2dKernelReg(pars));
}


complex_t Helmholtz2dHalfPlane(const Point& x, const Point& y, Parameters& pa)
{
  number_t bc=integer(pa("bc"));
  real_t k=real(pa("k")), a=real(pa("a")), b=real(pa("b")), t1=real(pa("t1")), t2=real(pa("t2"));
  real_t y1=y(1), y2=y(2), s=2*((y1-a)*t1+(y2-b)*t2)/(t1*t1+t2*t2);
  Point ys(2*a-y1+s*t1, 2*b-y2+s*t2);
  if(bc==1) return Helmholtz2d(x,y,pa)+Helmholtz2d(x,ys,pa);
  return Helmholtz2d(x,y,pa)-Helmholtz2d(x,ys,pa);
}

Vector<complex_t> Helmholtz2dHalfPlaneGradx(const Point& x, const Point& y, Parameters& pa)
{
  number_t bc=integer(pa("bc"));
  real_t k=real(pa("k")), a=real(pa("a")), b=real(pa("b")), t1=real(pa("t1")), t2=real(pa("t2"));
  real_t y1=y(1), y2=y(2), s=2*((y1-a)*t1+(y2-b)*t2)/(t1*t1+t2*t2);
  Point ys(2*a-y1+s*t1, 2*b-y2+s*t2);
  if(bc==1) return Helmholtz2dGradx(x,y,pa)+Helmholtz2dGradx(x,ys,pa);
  return Helmholtz2dGradx(x,y,pa)-Helmholtz2dGradx(x,ys,pa);
}

Vector<complex_t> Helmholtz2dHalfPlaneGrady(const Point& x, const Point& y, Parameters& pa)
{
  number_t bc=integer(pa("bc"));
  real_t k=real(pa("k")), a=real(pa("a")), b=real(pa("b")), t1=real(pa("t1")), t2=real(pa("t2"));
  real_t tt = 1./(t1*t1+t2*t2);
  real_t y1=y(1), y2=y(2), s=2*((y1-a)*t1+(y2-b)*t2)*tt;
  Point ys(2*a-y1+s*t1, 2*b-y2+s*t2);
  real_t d11 = -1+2*t1*t1*tt, d12 = 2*t1*t2*tt, d22 = -1 + 2*t2*t2*tt;
  Vector<complex_t> dH=Helmholtz2dGrady(x,y,pa), dHs=Helmholtz2dGrady(x,ys,pa);
  if(bc==1)
  {
      dH(1)+=d11*dHs(1)+d12*dHs(2);
      dH(2)+=d12*dHs(1)+d22*dHs(2);
  }
  else
   {
      dH(1)-=d11*dHs(1)+d12*dHs(2);
      dH(2)-=d12*dHs(1)+d22*dHs(2);
  }
  return dH;
}

complex_t Helmholtz2dHalfPlaneNxdotGradx(const Point& x, const Point& y, Parameters& pa)
{
  Vector<complex_t> gx = Helmholtz2dHalfPlaneGradx(x,y,pa);
  std::vector<real_t>& nxp=getNx();
  std::vector<complex_t>::const_iterator itgx=gx.begin();
  std::vector<real_t>::const_iterator itn=nxp.begin();
  complex_t r = (*itn++ * *itgx++);
  return   r += (*itn * *itgx);
}

complex_t Helmholtz2dHalfPlaneNydotGrady(const Point& x, const Point& y, Parameters& pa)
{
  Vector<complex_t> gy = Helmholtz2dHalfPlaneGrady(x,y,pa);
  std::vector<real_t>& nyp=getNy();
  std::vector<complex_t>::const_iterator itgy=gy.begin();
  std::vector<real_t>::const_iterator itn=nyp.begin();
  complex_t r = (*itn++ * *itgy++);
  return   r += (*itn * *itgy);
}


Matrix<complex_t> Helmholtz2dHalfPlaneGradxy(const Point& x, const Point& y, Parameters& pa)
{
  number_t bc=integer(pa("bc"));
  real_t k=real(pa("k")), a=real(pa("a")), b=real(pa("b")), t1=real(pa("t1")), t2=real(pa("t2"));
  real_t tt = 1./(t1*t1+t2*t2);
  real_t y1=y(1), y2=y(2), s=2*((y1-a)*t1+(y2-b)*t2)*tt;
  Point ys(2*a-y1+s*t1, 2*b-y2+s*t2);
  Vector<complex_t> dHs = Helmholtz2dGrady(x,ys,pa);
  real_t d11 = -1+2*t1*t1*tt, d12 = 2*t1*t2*tt, d22 = -1 + 2*t2*t2*tt;
  Matrix<complex_t> d2H=Helmholtz2dGradxy(x,y,pa), d2Hs=Helmholtz2dGradxy(x,ys,pa);
  if(bc==1)
  {
      d2H(1,1)+=d11*d2Hs(1,1)+d12*d2Hs(2,1);
      d2H(1,2)+=d11*d2Hs(1,2)+d12*d2Hs(2,2);
      d2H(2,1)+=d12*d2Hs(1,1)+d22*d2Hs(2,1);
      d2H(2,2)+=d12*d2Hs(1,2)+d22*d2Hs(2,2);
  }
  else
   {
      d2H(1,1)-=d11*d2Hs(1,1)+d12*d2Hs(2,1);
      d2H(1,2)-=d11*d2Hs(1,2)+d12*d2Hs(2,2);
      d2H(2,1)-=d12*d2Hs(1,1)+d22*d2Hs(2,1);
      d2H(2,2)-=d12*d2Hs(1,2)+d22*d2Hs(2,2);
  }
  return d2H;
}


} //end of namespace xlifepp
