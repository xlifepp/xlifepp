/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Laplace3dKernel.hpp
  \author E. Lunéville
  \since 17 jul 2014
  \date  17 jul 2014

  \brief Definition of Laplace 3D kernels

  Header to deal with Laplace 3D kernel: K(x, y)=(1/4 pi*|x-y|)
*/
#ifndef LAPLACE3DKERNEL_HPP
#define LAPLACE3DKERNEL_HPP

#include "utils.h"

namespace xlifepp
{

Kernel Laplace3dKernel(Parameters& pars = defaultParameters ); //!< construct a Laplace3d kernel

//main computation functions
real_t Laplace3d(const Point& x, const Point& y,Parameters& pars); //!< value
Vector<real_t> Laplace3dGradx(const Point& x, const Point& y,Parameters& pars); //!< gradx
Vector<real_t> Laplace3dGrady(const Point& x, const Point& y,Parameters& pars); //!< grady
real_t Laplace3dNxdotGradx(const Point& x, const Point& y, Parameters& pars);    //!< nx dot grad_x
real_t Laplace3dNydotGrady(const Point& x, const Point& y, Parameters& pars);    //!< ny dot grad_y

} // end of namespace xlifepp

#endif // LAPLACE3DKERNEL_HPP
