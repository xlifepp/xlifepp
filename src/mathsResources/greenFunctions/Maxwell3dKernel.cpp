/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 \file Maxwell3dKernel.cpp
 \author R. Rais, E. Lunéville
 \since 08 fev 2017
 \date  08 fev 2017

 \brief Implementation of Maxwell 3D kernels

 Maxwell3D kernel  K(k; x, y)=G(k_s; x, y)*I_3 + 1/k_s^2 * Hess(G(k_s; x, y)-G(k_p; x, y)),

 where G(k; x, y)=(1/4 pi*|x-y|) exp(ik|x-y|)
 */

#include "Maxwell3dKernel.hpp"
#include "Helmholtz3dKernel.hpp"
#include "utils.h"

namespace xlifepp
{


//---------------------------------------------------------------------------------------------------------
//construct Maxwell3d Kernel: IG_t(k_s; x, y)=G(k_s; x, y)*I_3 + 1/k_s^2 * Hess(G(k_s; x, y)-G(k_p; x, y))
//---------------------------------------------------------------------------------------------------------

//construct a Maxwell3d Kernel
Kernel Maxwell3dKernel(Parameters& pars)
{
  Kernel K;
  initMaxwell3dKernel(K,pars);
  return K;
}

Kernel Maxwell3dKernel(const real_t& k, const real_t& t)
{
  Kernel K;
  Parameters pars;
  pars<<Parameter(k,"k");
  pars<<Parameter(t,"RegParam");
  initMaxwell3dKernel(K,pars);
  return K;
}

Kernel Maxwell3dKernel(const complex_t& k, const real_t& t)
{
  Kernel K;
  Parameters pars;
  pars<<Parameter(k,"k");
  pars<<Parameter(t,"RegParam");
  initMaxwell3dKernel(K,pars);
  return K;
}

void initMaxwell3dKernel(Kernel& K, Parameters& pars)
{
  K.name="Maxwell 3D kernel";
  K.shortname="Maxw3D";
  K.singularType =_r_;
  K.singularOrder = -1;
  K.singularCoefficient = over4pi_;
  K.symmetry=_symmetric;
  K.userData.push(pars);
  K.kernel = Function(Maxwell3d, K.userData);
  K.curlx = Function(Maxwell3dCurlx, K.userData);
  K.curly = Function(Maxwell3dCurly, K.userData);
  K.curlxy = Function(Maxwell3dCurlxy, K.userData);
  K.divx = Function(Maxwell3dDivx, K.userData);
  K.divy = Function(Maxwell3dDivy, K.userData);
  K.divxy = Function(Maxwell3dDivxy, K.userData);
}
//! kernel computation: IG_t(k_s; x, y)=G(k_s; x, y)*I_3 + 1/k_s^2 * Hess(G(k_s; x, y)-G(k_p; x, y))
Matrix<complex_t> Maxwell3d(const Point& x, const Point& y, Parameters& pa)
{
  complex_t k = pa("k");
  complex_t ukk = 1./(k*k);
  complex_t Gk= Helmholtz3d(x,y,pa);
  Matrix<complex_t> Gradxyk=Helmholtz3dGradxy(x,y,pa);
  real_t RegParam = pa("RegParam");
  if(RegParam!=0)
  {
     Parameters pakp(k * std::sqrt(RegParam),"k");
     Gradxyk-=Helmholtz3dGradxy(x,y,pakp);
  }
  Matrix<complex_t> mIG(3,3);
  std::vector<complex_t>::iterator itm=mIG.begin(), itg=Gradxyk.begin();
  //Row 1
  *itm++=Gk-ukk* *itg++;
  *itm++=-ukk* *itg++;
  *itm++=-ukk* *itg++;
  //Row 2
  *itm++=mIG(1,2); itg++;
  *itm++=Gk-ukk* *itg++;
  *itm++=-ukk* *itg++;
  //Row 3
  *itm++=mIG(1,3); itg++;
  *itm++=mIG(2,3); itg++;
  *itm++=Gk-ukk* *itg;
  return mIG;
}
//! kernel computation: Curlx(IG_t)
Matrix<complex_t> Maxwell3dCurlx(const Point& x, const Point& y, Parameters& pa)
{
  complex_t k = pa("k");
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  real_t r3 = r * r2;
  complex_t ikr = i_ * k * r;
  complex_t eikr = std::exp(ikr);
  complex_t eik4pir3 = (ikr-1) * eikr * over4pi_ /r3;
  Matrix<complex_t> mCurlxG(3,3,0);
  std::vector<complex_t>::iterator itm=mCurlxG.begin();
  *itm++;
  *itm++ =-eik4pir3*(x(3)-y(3));
  *itm++ =eik4pir3*(x(2)-y(2));
  *itm++ =-mCurlxG(1,2);
  *itm++;
  *itm++ =-eik4pir3*(x(1)-y(1));
  *itm++ =-mCurlxG(1,3);
  *itm++ =-mCurlxG(2,3);
  return mCurlxG;
 }
//! kernel computation: Curly(IG_t)
Matrix<complex_t> Maxwell3dCurly(const Point& x, const Point& y, Parameters& pa)
{
  complex_t k = pa("k");
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  real_t r3 = r * r2;
  complex_t ikr = i_ * k * r;
  complex_t eikr = std::exp(ikr);
  complex_t eik4pir3 = (1.-ikr) * eikr * over4pi_ /r3;
  Matrix<complex_t> mCurlyG(3,3,0);
  std::vector<complex_t>::iterator itm=mCurlyG.begin();
  *itm++;
  *itm++ =-eik4pir3*(x(3)-y(3));
  *itm++ =eik4pir3*(x(2)-y(2));
  *itm++ =-mCurlyG(1,2);
  *itm++;
  *itm++ =-eik4pir3*(x(1)-y(1));
  *itm++ =-mCurlyG(1,3);
  *itm++ =-mCurlyG(2,3);
  return mCurlyG;
}
//! kernel computation: CurlxCurly(IG_t)
Matrix<complex_t> Maxwell3dCurlxy(const Point& x, const Point& y, Parameters& pa)
{
  Vector< Vector<complex_t> > curlxyG(3,Vector<complex_t>(3,complex_t(0.)));
  complex_t k = pa("k");
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  real_t r3 = r * r2;
  real_t r5 = r3 * r2;
  complex_t ikr = i_ * k * r;
  complex_t eikr = std::exp(ikr);
  complex_t eik4pir5 = (k*k*r2+3*ikr-3) * eikr*over4pi_ / r5;
  Matrix<complex_t> mCurlxyG(3,3);
  real_t xy1=x(1)-y(1), xy2=x(2)-y(2), xy3=x(3)-y(3);
  complex_t alpha = 2* (ikr-1.)*eikr *over4pi_ /r3;
  std::vector<complex_t>::iterator itm=mCurlxyG.begin();
  // Row 1
  *itm++ = alpha - eik4pir5 * (xy2 * xy2 + xy3 * xy3);
  *itm++ = eik4pir5 * (xy1 * xy2);
  *itm++ = eik4pir5 * (xy1 * xy3);
  // Row 2
  *itm++ = eik4pir5 * (xy1 * xy2);
  *itm++ = alpha- eik4pir5 * (xy1 * xy1 + xy3 * xy3);
  *itm++ = eik4pir5 * (xy2 * xy3);
  // Row 3
  *itm++ = eik4pir5 * (xy1 * xy3);
  *itm++ = eik4pir5 * (xy2 * xy3);
  *itm++ = alpha- eik4pir5 * (xy1 * xy1 + xy2 * xy2);
  return mCurlxyG;
}
//! kernel computation: Divx(IG_t)
Vector<complex_t> Maxwell3dDivx(const Point& x, const Point& y, Parameters& pa)
{
  real_t RegParam = pa("RegParam");
  if(RegParam==0) return Vector<complex_t>(3,complex_t(0.));   //return 0 (classical Maxwell)
  //regularized Maxwell
  complex_t k = pa("k");
  complex_t kp = k * std::sqrt(RegParam);
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  complex_t ikpr = i_ * kp * r;
  complex_t dr = (ikpr-1.) / r2;
  Vector<complex_t> divxG(3,complex_t(0.));
  scaledVectorTpl(RegParam * over4pi_ * std::exp(ikpr)*dr / r, x.begin(), x.end(), y.begin(), divxG.begin());
  return divxG;
}
//! kernel computation: Divy(IG_t)
Vector<complex_t> Maxwell3dDivy(const Point& x, const Point& y, Parameters& pa)
{
  real_t RegParam = pa("RegParam");
  if(RegParam==0) return Vector<complex_t>(3,complex_t(0.));   //return 0 (classical Maxwell)
  //regularized Maxwell
  complex_t k = pa("k");
  complex_t kp = k * std::sqrt(RegParam);
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  complex_t ikpr = i_ * kp * r;
  complex_t dr = (ikpr-1.) / r2;
  Vector<complex_t> divyG(3,complex_t(0.));
  scaledVectorTpl(-RegParam * over4pi_ * std::exp(ikpr)*dr / r, x.begin(), x.end(), y.begin(), divyG.begin());
  return divyG;
}

//! kernel computation: DivxDivy(IG_t)
complex_t Maxwell3dDivxy(const Point& x, const Point& y, Parameters& pa)
{
  real_t RegParam = pa("RegParam");
  if(RegParam==0) return complex_t(0.);   //return 0 (classical Maxwell)
  //regularized Maxwell
  complex_t k = pa("k");
  complex_t kp = k * std::sqrt(RegParam);
  real_t r2 = x.squareDistance(y);
  real_t r = std::sqrt(r2);
  complex_t ikpr = i_ * kp * r;
  return RegParam * over4pi_ * std::exp(ikpr) * kp * kp / r;
}
} // end of namespace xlifepp



