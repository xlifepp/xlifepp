/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file randomGenerators.cpp
  \since 12 jul 2016
  \date 12 jul 2016

  Implementation of some basic random generators
 */

#include "randomGenerators.hpp"
#include <cstdlib>

#if __cplusplus >= 201103L
  #include <random>
#endif // __cplusplus

namespace xlifepp
{

// ==========================================================================================
//  uniform distributions
// ==========================================================================================
//front end to get one sample from uniform distribution on [a,b(, random generator has to be initialized before
// if c++11 is enabled it uses std::random else it uses C random generator
real_t uniformDistribution(real_t a, real_t b)
{
#if __cplusplus >= 201103L    //c++11 enabled, use std::random
    std::uniform_real_distribution<double> ud(a,b);
    return ud(theRandomEngine);
#else //use C style generator
    return uniformDistributionC(a,b);
#endif
}

real_t uniformDistributionC(real_t a, real_t b)
{
    real_t u = std::rand() * (1.0 / RAND_MAX);
    return a+ (b-a)*u;
}

//generate a real random uniform matrix (mxn, row major access), using uniform distribution on [a,b[
// if c++11 is enabled it uses std::random else it uses std::rand()
void uniformDistribution(real_t* mat, real_t a, real_t b, number_t n, number_t m)
{
#if __cplusplus >= 201103L    //c++11 enabled, use std::random
  std::uniform_real_distribution<double> ud(a,b);
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat)
        *mat=ud(theRandomEngine);
#else //use C style generator
  uniformDistributionC(mat,a,b,n,m);
#endif
}

void uniformDistributionC(real_t* mat, real_t a, real_t b, number_t n, number_t m)
{
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat)
        *mat=uniformDistributionC(a,b);
}

//generate a real random uniform matrix (mxn, row major access), using uniform distribution on [0,1[
// if c++11 is enabled it uses std::random else it uses std::rand()
void uniformDistribution(real_t* mat, number_t n, number_t m)
{
 uniformDistribution(mat,0.,1.,n,m);
}

void uniformDistributionC(real_t* mat, number_t n, number_t m)
{
 uniformDistributionC(mat,0.,1.,n,m);
}

//generate a complex random uniform matrix (row major access), using uniform distribution on [a,b[
// if c++11 is enabled it uses std::random else it uses std::rand()
void uniformDistribution(complex_t* mat, real_t a, real_t b, number_t n, number_t m)
{
complex_t zi(0.,1.);
#if __cplusplus >= 201103L    //c++11 enabled, use std::random
  std::uniform_real_distribution<double> ud(a,b);
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat)
        *mat=ud(theRandomEngine)+zi*ud(theRandomEngine);
#else //use internal generator
   uniformDistributionC(mat,a,b,n,m);
#endif
}

void uniformDistributionC(complex_t* mat, real_t a, real_t b, number_t n, number_t m)
{
complex_t zi(0.,1.);
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat)
        *mat=uniformDistributionC(a,b)+zi*uniformDistributionC(a,b);
}

//generate a complex random uniform matrix (mxn,, row major access), using uniform distribution on [0,1[
// if c++11 is enabled it uses std::random else it uses std::rand()
void uniformDistribution(complex_t* mat, number_t n, number_t m)
{
 uniformDistribution(mat,0.,1.,n,m);
}

void uniformDistributionC(complex_t* mat, number_t n, number_t m)
{
 uniformDistributionC(mat,0.,1.,n,m);
}

// ==========================================================================================
//  normal distributions
// ==========================================================================================
//front end to get one sample from normal distribution on [a,b(, random generator has to be initialized before
// if c++11 is enabled it uses std::random else it uses internal generator: BoxMuller or Marsaglia (default)

//Box-Muller method for normal distribution, random generator has to be initialized before
real_t normalBoxMuller(real_t mu, real_t sigma)
{
  real_t epsilon = std::numeric_limits<real_t>::min();
  real_t two_pi = 2.0*3.14159265358979323846;
  real_t u, v, s;
  do
    {
      u = std::rand() * (1.0 / RAND_MAX);
      v = std::rand() * (1.0 / RAND_MAX);
    }
  while ( u <= epsilon );
  s = std::sqrt(-2.0 * std::log(u)) * std::cos(two_pi * v);
  return mu + s * sigma;
}

//Marsaglia method for normal distribution (Box Muller improvement), random generator has to be initialized before
double normalMarsaglia(real_t mu, real_t sigma)
{
  real_t u, v, s;
  do
    {
      u = (std::rand() / ((double) RAND_MAX)) * 2.0 - 1.0;
      v = (std::rand() / ((double) RAND_MAX)) * 2.0 - 1.0;
      s = u * u + v * v;
    }
  while( (s >= 1.0) || (s == 0.0) );
  s = std::sqrt(-2.0 * std::log(s) / s);
  return mu + sigma * u * s;
}

//front end to get one sample from normal distribution, random generator has to be initialized before
// if c++11 is enabled it uses std::random else it uses internal generators
real_t normalDistribution(real_t mu, real_t sigma, GaussianGenerator gg)
{
#if __cplusplus >= 201103L    //c++11 enabled, use std::random
  std::normal_distribution<real_t> nd(mu,sigma);
  return nd(theRandomEngine);
#else //use internal generators
  return normalDistributionC(mu,sigma,gg);
#endif
}

real_t normalDistributionC(real_t mu, real_t sigma, GaussianGenerator gg)
{
  switch(gg)
    {
      case _BoxMullerGenerator: return normalBoxMuller(mu, sigma);
      case _MarsagliaGenerator:
      default:                  return normalMarsaglia(mu, sigma);
    }
  return 0.;
}

//generate a random Gaussian matrix (mxn, row major access), using normal distribution (mu,sigma)
// if c++11 is enabled it uses std::random else it uses internal generators
void normalDistribution(real_t* mat, real_t mu, real_t sigma, number_t n, number_t m)
{

#if __cplusplus >= 201103L    //c++11 enabled, use std::random
  std::normal_distribution<real_t> nd(mu,sigma);
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat) *mat=nd(theRandomEngine);
#else //use internal generator
  normalDistributionC(mat,mu,sigma,n,m);
#endif
}

void normalDistributionC(real_t* mat, real_t mu, real_t sigma, number_t n, number_t m)
{
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat) *mat=normalDistributionC(mu,sigma);
}

void normalDistributionC(real_t* mat, real_t mu, real_t sigma, GaussianGenerator gg, number_t n, number_t m)
{
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat) *mat=normalDistributionC(mu,sigma,gg);
}

//generate a random Gaussian matrix (mxn, row major access), mu=0, sigma=1.
// if c++11 is enabled it uses std::random else it uses internal generators
void normalDistribution(real_t* mat, number_t m, number_t n)
{
  normalDistribution(mat,0.,1.,m,n);
}

void normalDistributionC(real_t* mat, number_t m, number_t n)
{
  normalDistributionC(mat,0.,1.,m,n);
}

void normalDistributionC(real_t* mat, GaussianGenerator gg, number_t m, number_t n)
{
  normalDistributionC(mat,0.,1.,gg,m,n);
}

//generate a complex random Gaussian matrix (mxn, row major access), using normal distribution (mu,sigma)
// if c++11 is enabled it uses std::random else it uses internal generators
void normalDistribution(complex_t* mat, real_t mu, real_t sigma, number_t n, number_t m)
{
#if __cplusplus >= 201103L   //c++11 enabled, use std::random
  std::normal_distribution<real_t> nd(0.,1.);
  complex_t zi(0.,1.);
  real_t s=std::sqrt(sigma*sigma/2.);
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat)
        *mat = mu+s*(nd(theRandomEngine)+zi*nd(theRandomEngine));
#else //use internal generator
  normalDistributionC(mat,mu,sigma,n,m);
#endif
}

void normalDistributionC(complex_t* mat, real_t mu, real_t sigma, number_t n, number_t m)
{
  complex_t zi(0.,1.);
  real_t s=std::sqrt(sigma*sigma/2.);
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat)
        *mat=mu+s*(normalDistributionC()+zi*normalDistributionC());
}

void normalDistributionC(complex_t* mat, real_t mu, real_t sigma, GaussianGenerator gg, number_t n, number_t m)
{
  complex_t zi(0.,1.);
  real_t s=std::sqrt(sigma*sigma/2.);
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat)
        *mat=mu+s*(normalDistributionC(0.,1.,gg)+zi*normalDistributionC(0.,1.,gg));
}

//generate a complex random Gaussian matrix (mxn, row major access), using normal distribution (0,1)
// if c++11 is enabled it uses std::random else it uses internal generators
void normalDistribution(complex_t* mat, number_t n, number_t m)
{
#if __cplusplus >= 201103L   //c++11 enabled, use std::random
  std::normal_distribution<real_t> nd(0.,1.);
  complex_t zi(0.,1.);
  real_t s=std::sqrt(0.5);
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat)
        *mat = s*(nd(theRandomEngine)+zi*nd(theRandomEngine));
#else //use internal generator
  normalDistributionC(mat,n,m);
#endif
}

void normalDistributionC(complex_t* mat, number_t n, number_t m)
{
  complex_t zi(0.,1.);
  real_t s=std::sqrt(0.5);
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat)
        *mat=s*(normalDistributionC()+zi*normalDistributionC());
}

void normalDistributionC(complex_t* mat, GaussianGenerator gg, number_t n, number_t m)
{
  complex_t zi(0.,1.);
  real_t s=std::sqrt(0.5);
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++mat)
        *mat=s*(normalDistributionC(0,1.,gg)+zi*normalDistributionC(0,1.,gg));
}


} // end of namespace xlifepp
