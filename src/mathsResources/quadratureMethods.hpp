/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file quadratureMethods.hpp
  \authors E. Lunéville
  \since 19 jan 2019
  \date  1 dec 2019

  \brief Implementation of various quadrature methods (rectangle, trapeze, Simpson, adaptive trapeze) to compute 1D integrals

 */

#ifndef QUADRATUREMETHODS_HPP_INCLUDED
#define QUADRATUREMETHODS_HPP_INCLUDED

#include "utils.h"
#include "config.h"

namespace xlifepp
{
//----------------------------------------------------------------
//  integral using rectangle method
//----------------------------------------------------------------
/*! uniform rectangle method from a list of n values uniformaly distributed (step h)
    n: number of values
    h: step
    itb: first position in the list of values
    intg: value of integral: h sum k=1,n v_k
*/
template<typename T, typename Iterator>
T rectangle(number_t n, real_t h, Iterator itb, T& intg)
{
  intg=T();
  for (number_t k=0;k<n; k++, ++itb) intg+=*itb;
  return intg*=h;
}

template<typename T>
T rectangle(const std::vector<T>& f, real_t h)
{
  T intg=T();
  return rectangle(f.size(),h,f.begin(),intg);
}

//! uniform rectangle method on [a,b] interval from a function and a number of subdivisions
template<typename T>
T rectangle(T(*f)(real_t), real_t a, real_t b, number_t n)
{
  real_t h=(b-a)/n;
  real_t x=a;
  T intg=T(0);
  for (number_t k=0;k<n;k++,x+=h) intg+=f(x);
  return intg*=h;
}

//! uniform rectangle method on [a,b] interval from a function with parameters and a number of subdivisions
template<typename T>
T rectangle(T(*f)(real_t, Parameters&), Parameters& pars, real_t a, real_t b, number_t n)
{
  real_t h=(b-a)/n;
  real_t x=a;
  T intg=T(0);
  for (number_t k=0;k<n;k++,x+=h) intg+=f(x,pars);
  return intg*=h;
}

//----------------------------------------------------------------
//  integral using trapeze method
//----------------------------------------------------------------
/*! uniform trapeze method from a list of n values uniformaly distributed (step h)
    n: number of values
    h: step
    itb: first position in the list of values
    intg: value of integral
*/
template<typename T, typename Iterator>
T trapz(number_t n, real_t h, Iterator itb, T& intg)
{
  intg=T();
  intg+=0.5* *itb;itb++;
  for (number_t k=1;k<n-1; k++, ++itb) intg+=*itb;
  intg+=0.5* *itb;
  intg*=h;
  return intg;
}

template<typename T>
T trapz(const std::vector<T>& f, real_t h)
{
  T intg=T();
  return trapz(f.size(),h,f.begin(),intg);
}

//! uniform trapeze method on [a,b] interval from a function and a number of points
template<typename T>
T trapz(T(*f)(real_t), real_t a, real_t b, number_t n)
{
  real_t h=(b-a)/n;
  real_t x=a+h;
  T res=T(0);
  for (number_t k=1;k<n;k++,x+=h) res+=f(x);
  res=0.5*h*(2*res+f(a)+f(b));
  return res;
}

//! uniform trapeze method on [a,b] interval from a function with parameters and a number of points
template<typename T>
T trapz(T(*f)(real_t, Parameters&), Parameters& pars, real_t a, real_t b, number_t n)
{
  real_t h=(b-a)/n;
  real_t x=a+h;
  T res=T(0);
  for (number_t k=1;k<n;k++,x+=h) res+=f(x,pars);
  res=0.5*h*(2*res+f(a,pars)+f(b,pars));
  return res;
}

//----------------------------------------------------------------
//  integral using Simpson method
//----------------------------------------------------------------
/*! uniform Simpson method from a list of n (odd) values uniformaly distributed (step h)
    n: number of values
    h: step
    itb: first position in the list of values
    intg: value of integral
*/
template<typename T, typename Iterator>
T simpson(number_t n, real_t h, Iterator itb, T& intg)
{
  if ((n%2)==0) { n=n-1; }
  intg=T();
  intg+=*itb; itb++;
  for (number_t k=1;k<n-2; k+=2)
  {
    intg+=4* *itb; ++itb;
    intg+=2* *itb; ++itb;
  }
  intg+=4* *itb; itb++; intg+=*itb;
  intg*=(h/3);
  return intg;
}

template<typename T>
T simpson(const std::vector<T>& f, real_t h)
{
  T intg=T();
  return simpson(f.size(),h,f.begin(),intg);
}

//! uniform Simpson method on [a,b] interval from a function and a number of intervals
template<typename T>
T simpson(T(*f)(real_t), real_t a, real_t b, number_t n)
{
  real_t h=(b-a)/n;
  std::vector<T> vf(n+1);
  typename std::vector<T>::iterator itv=vf.begin();
  real_t x=a;
  for (;itv!=vf.end();++itv, x+=h) *itv=f(x);
  return simpson(vf,h);
}

//! uniform Simpson method on [a,b] interval from a function with parameters and a number of intervals
template<typename T>
T simpson(T(*f)(real_t, Parameters&), Parameters& pars, real_t a, real_t b, number_t n)
{
  real_t h=(b-a)/n;
  std::vector<T> vf(n+1);
  typename std::vector<T>::iterator itv=vf.begin();
  real_t x=a;
  for (;itv!=vf.end();++itv, x+=h) *itv=f(x,pars);
  return simpson(vf,h);
}

//----------------------------------------------------------------
//  integral using adaptative trapeze method
//----------------------------------------------------------------
/*! adaptative trapeze method from a function and a given tolerance eps
    compute int_[a,b] f(x)dx using trapeze method I1=0.5*(b-a)*[f(a)+f(b)]
    and the modified trapeze method I2=0.25*(b-a)*[f(a)+2*f(0.5*(a+b))+f(b)]
    error estimator: |I1-I2|< 3*eps*(b-a)
*/
template<typename T>
class TrapzInterval
{
  public:
    real_t x1,x2;
    T f1, f2;
    TrapzInterval(real_t a, real_t b, const T& fa, const T& fb)
    : x1(a), x2(b), f1(fa), f2(fb) {}
};

template<typename T>
T adaptiveTrapz(T(*f)(real_t), real_t a, real_t b, real_t eps=1E-6)
{
  T res=T(), I1, I2;
  std::list<TrapzInterval<T> > Is;
  Is.push_back(TrapzInterval<T>(a,b,f(a),f(b)));
  typename std::list<TrapzInterval<T> >::iterator it;
  bool first=true;
  while (Is.size()>0)
  {
    it=Is.begin();
    real_t x1=it->x1, x2=it->x2, x3=0.5*(x2+x1), dx=x2-x1;
    T f2=it->f2, f3=f(x3);
    I1=0.5*dx*(it->f1+f2);
    I2=0.5*(I1+dx*f3); //= 0.25*dx*(f1+2*f3+f2);
    if (first || norm(I1-I2)>3*eps*dx) //split interval
    {
      it->x2=x3; it->f2=f3;
      Is.push_back(TrapzInterval<T>(x3,x2,f3,f2));
      first = false;
    }
    else // error ok, add contribution
    {
      res+=I2;
      it= Is.erase(it);
    }
  }
  return res;
}

template<typename T>
T adaptiveTrapz(T(*f)(real_t,Parameters&), Parameters& pars, real_t a, real_t b, real_t eps=1E-6)
{
  T res=T(), I1, I2;
  std::list<TrapzInterval<T> > Is;
  Is.push_back(TrapzInterval<T>(a,b,f(a,pars),f(b,pars)));
  typename std::list<TrapzInterval<T> >::iterator it;
  bool first=true;
  while (Is.size()>0)
  {
    it=Is.begin();
    real_t x1=it->x1, x2=it->x2, x3=0.5*(x2+x1), dx=x2-x1;
    T f2=it->f2, f3=f(x3,pars);
    I1=0.5*dx*(it->f1+f2);
    I2=0.5*(I1+dx*f3); //= 0.25*dx*(f1+2*f3+f2);
    if (first || norm(I1-I2)>3*eps*dx) //split interval
    {
      it->x2=x3; it->f2=f3;
      Is.push_back(TrapzInterval<T>(x3,x2,f3,f2));
      first = false;
    }
    else // error ok, add contribution
    {
      res+=I2;
      it= Is.erase(it);
    }
  }
  return res;
}

//-----------------------------------------------------------------
//  integral using Laguerre quadrature points for f(t)~exp(-at)g(t)
//-----------------------------------------------------------------
/*         /+inf              /+inf
  compute  |    f(t)dt = 1/a |   exp(-t)[exp(t)f(t0+t/a)]dt
           /t0               /0
                       ~ 1/a sum  wi.exp(xi)f(t0+xi/a)
                            i=1,nq
  xi, wi points and weights of Laguerre

*/

void LaguerreTable(number_t n, std::vector<real_t>& quadpoints, std::vector<real_t>& quadweights);

// compute integral,
// if quadpoints or quadweights are not of size nq, they are reloaded from the function LaguerreTable
template<typename T>
T laguerre(T(*f)(real_t), real_t t0, real_t a, number_t nq,
           std::vector<real_t>& quadpoints, std::vector<real_t>& quadweights)
{
  if (quadpoints.size()!=nq || quadpoints.size()!=nq ) LaguerreTable(nq,quadpoints,quadweights);
  if (a<=0) error("laguerre_exp_dec");
  T res=T(0);
  std::vector<real_t>::iterator itp=quadpoints.begin();
  std::vector<real_t>::iterator itw=quadweights.begin();
  for (;itp!=quadpoints.end(); ++itp, ++itw)
  {
    real_t t=t0+*itp/a;
    res+=f(t)**itw;
  }
  return res/a;
}

template<typename T>
T laguerre(T(*f)(real_t,Parameters&), Parameters& pars, real_t t0, real_t a, number_t nq,
           std::vector<real_t>& quadpoints, std::vector<real_t>& quadweights)
{
  if (quadpoints.size()!=nq || quadpoints.size()!=nq ) LaguerreTable(nq,quadpoints,quadweights);
  if (a<=0) error("laguerre_exp_dec");
  T res=T(0);
  std::vector<real_t>::iterator itp=quadpoints.begin();
  std::vector<real_t>::iterator itw=quadweights.begin();
  for (;itp!=quadpoints.end(); ++itp, ++itw)
  {
    real_t t=t0+*itp/a;
    res+=f(t,pars)**itw;
  }
  return res/a;
}

}
#endif // QUADRATUREMETHODS_HPP_INCLUDED
