/*
  earcut algorithm to triangulate polygons
  from https://github.com/mapbox/earcut.hpp
  ISC License
  Copyright (c) 2015, Mapbox
  Permission to use, copy, modify, and/or distribute this software for any purpose
  with or without fee is hereby granted, provided that the above copyright notice
  and this permission notice appear in all copies.
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
  REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
  FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
  INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
  OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
  TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
  THIS SOFTWARE.

  From an indexed list of points (polygon vertices) create a list of triangles slicing the polygon

    std::vector<Number> indices = earcut<Number>(polygon);

  input: Polygon class supporting size(), empty() and operator[]
  output: list of triangle numbers (3 subsequent)

  polygon should be a vector<vector<Point> > with polygon[0] the exterior boundary and polygon[1], polygon[2], ... some hole boundaries
         there exist a short cut adressing polygons with no hole (N the indexing type, P a Point class)
             std::vector<std::vector<N> > triangulate(const std::vector<P>& vertices)

  Note: WORKS ONLY IN 2D
  Note: move original C++11 to C98 (remove template N: replaced everywhere by size_t, change C++11 syntax to C98 in particular in ObjectPool class)

*/

#ifndef EARCUT_HPP
#define EARCUT_HPP

#include <algorithm>
#include <cassert>
#include <cmath>
#include <memory>
#include <vector>
#include <limits>

namespace xlifepp
{

class Earcut
{
public:
    std::vector<size_t> indices;
    std::size_t vertices;

    Earcut() : vertices(0), inv_size(0) {}

    template <typename Polygon>
    void operator()(const Polygon& points);

    struct Node {
        Node(const size_t& i_=0, const double& x_=0., const double& y_=0.)
          : i(i_), x(x_), y(y_), prev(nullptr), next(nullptr), z(0), prevZ(nullptr), nextZ(nullptr), steiner(false) {}

        size_t i;
        double x;
        double y;

        // previous and next vertice nodes in a polygon ring
        Node* prev;
        Node* next;

        // z-order curve value
        int z;

        // previous and next nodes in z-order
        Node* prevZ;
        Node* nextZ;

        // indicates whether this is a steiner point
        bool steiner;
    };

    template <typename Ring> Node* linkedList(const Ring& points, const bool clockwise);
    Node* filterPoints(Node* start, Node* end = nullptr);
    void earcutLinked(Node* ear, int pass = 0);
    bool isEar(Node* ear);
    bool isEarHashed(Node* ear);
    Node* cureLocalIntersections(Node* start);
    void splitEarcut(Node* start);
    template <typename Polygon> Node* eliminateHoles(const Polygon& points, Node* outerNode);
    void eliminateHole(Node* hole, Node* outerNode);
    Node* findHoleBridge(Node* hole, Node* outerNode);
    bool sectorContainsSector(const Node* m, const Node* p);
    void indexCurve(Node* start);
    Node* sortLinked(Node* list);
    int zOrder(const double x_, const double y_);
    Node* getLeftmost(Node* start);
    bool isPointInTriangle(double ax, double ay, double bx, double by, double cx, double cy, double px, double py) const;
    bool isValidDiagonal(Node* a, Node* b);
    double area(const Node* p, const Node* q, const Node* r) const;
    bool equals(const Node* p1, const Node* p2);
    bool intersects(const Node* p1, const Node* q1, const Node* p2, const Node* q2);
    bool onSegment(const Node* p, const Node* q, const Node* r);
    int sign(double val);
    bool intersectsPolygon(const Node* a, const Node* b);
    bool locallyInside(const Node* a, const Node* b);
    bool middleInside(const Node* a, const Node* b);
    Node* splitPolygon(Node* a, Node* b);
    template <typename Point> Node* insertNode(std::size_t i, const Point& p, Node* last);
    void removeNode(Node* p);

    bool hashing;
    double minX, maxX;
    double minY, maxY;
    double inv_size;

    template <typename T, typename Alloc = std::allocator<T> >
    class ObjectPool {
    public:
        ObjectPool() : currentBlock(nullptr), currentIndex(1), blockSize (1) { }
        ObjectPool(std::size_t blockSize_) {reset(blockSize_);}
        ~ObjectPool() {clear();}
        T* construct(const size_t& i, const double& x, const double& y)
        {
            if (currentIndex >= blockSize) {
                currentBlock = alloc.allocate(blockSize);
                allocations.push_back(currentBlock);
                currentIndex = 0;
            }
            T* object = &currentBlock[currentIndex++];
            Earcut::Node node(i,x,y);
            #if __cplusplus > 201703L
              std::construct_at(object, node);
            #else
              alloc.construct(object, node); // std::allocator::construct deprecated in C++17, removed in C++20
            #endif
            return object;
        }
        void reset(std::size_t newBlockSize)
        {
            typename std::vector<T*>::iterator it=allocations.begin();
            for(;it!=allocations.end();++it) alloc.deallocate(*it, blockSize);
            allocations.clear();
            blockSize = std::max<std::size_t>(1, newBlockSize);
            currentBlock = 0;
            currentIndex = blockSize;
        }
        void clear() { reset(blockSize); }
    private:
        T* currentBlock;
        std::size_t currentIndex;
        std::size_t blockSize;
        std::vector<T*> allocations;
        Alloc alloc;
    };

    ObjectPool<Node> nodes;
};

template <typename Polygon>
void Earcut::operator()(const Polygon& points)
{
    // reset
    indices.clear();
    vertices = 0;

    if (points.empty()) return;

    double x;
    double y;
    int threshold = 80;
    std::size_t len = 0;

    for (size_t i = 0; threshold >= 0 && i < points.size(); i++) {
        threshold -= static_cast<int>(points[i].size());
        len += points[i].size();
    }

    //estimate size of nodes and indices
    nodes.reset(len * 3 / 2);
    indices.reserve(len + points[0].size());

    Node* outerNode = linkedList(points[0], true);
    if (!outerNode || outerNode->prev == outerNode->next) return;

    if (points.size() > 1) outerNode = eliminateHoles(points, outerNode);

    // if the shape is not too simple, we'll use z-order curve hash later; calculate polygon bbox
    hashing = threshold < 0;
    if (hashing) {
        Node* p = outerNode->next;
        minX = maxX = outerNode->x;
        minY = maxY = outerNode->y;
        do {
            x = p->x;
            y = p->y;
            minX = std::min<double>(minX, x);
            minY = std::min<double>(minY, y);
            maxX = std::max<double>(maxX, x);
            maxY = std::max<double>(maxY, y);
            p = p->next;
        } while (p != outerNode);

        // minX, minY and size are later used to transform coords into integers for z-order calculation
        inv_size = std::max<double>(maxX - minX, maxY - minY);
        inv_size = inv_size != .0 ? (1. / inv_size) : .0;
    }

    earcutLinked(outerNode);

    nodes.clear();
}

// create a circular doubly linked list from polygon points in the specified winding order
template <typename Ring>
Earcut::Node* Earcut::linkedList(const Ring& points, const bool clockwise)
{
    double sum = 0;
    const std::size_t len = points.size();
    std::size_t i, j;
    Node* last = nullptr;

    // calculate original winding order of a polygon ring
    for (i = 0, j = len > 0 ? len - 1 : 0; i < len; j = i++)
    {
        double p20 = points[j][0];
        double p10 = points[i][0];
        double p11 = points[i][1];
        double p21 = points[j][1];
        sum += (p20 - p10) * (p11 + p21);
    }

    // link points into circular doubly-linked list in the specified winding order
    if (clockwise == (sum > 0)) {
        for (i = 0; i < len; i++) last = insertNode(vertices + i, points[i], last);
    } else {
        for (i = len; i-- > 0;) last = insertNode(vertices + i, points[i], last);
    }

    if (last && equals(last, last->next)) {
        removeNode(last);
        last = last->next;
    }

    vertices += len;

    return last;
}


inline bool sortxNode(const Earcut::Node* a, const Earcut::Node* b) {return a->x < b->x;}

// link every hole into the outer loop, producing a single-ring polygon without holes
template <typename Polygon>
Earcut::Node* Earcut::eliminateHoles(const Polygon& points, Node* outerNode)
{
    const size_t len = points.size();

    std::vector<Node*> queue;
    for (size_t i = 1; i < len; i++) {
        Node* list = linkedList(points[i], false);
        if (list) {
            if (list == list->next) list->steiner = true;
            queue.push_back(getLeftmost(list));
        }
    }
    std::sort(queue.begin(), queue.end(), sortxNode);

    // process holes from left to right
    for (size_t i = 0; i < queue.size(); i++) {
        eliminateHole(queue[i], outerNode);
        outerNode = filterPoints(outerNode, outerNode->next);
    }

    return outerNode;
}

// create a node and util::optionally link it with previous one (in a circular doubly linked list)
template <typename Point>
Earcut::Node* Earcut::insertNode(std::size_t i, const Point& pt, Node* last)
{
    Node* p = nodes.construct(static_cast<size_t>(i), pt[0], pt[1]);  // Eric adaptation
    if (!last) {
        p->prev = p;
        p->next = p;

    } else {
        assert(last);
        p->next = last->next;
        p->prev = last;
        last->next->prev = p;
        last->next = p;
    }
    return p;
}

template <typename Polygon>
std::vector<size_t> earcut(const Polygon& poly)
{
    Earcut earcut;
    earcut(poly);
    return earcut.indices;
}

// create list of triangles from vertices of polygon (with no holes)
template <typename P>
std::vector<std::vector<size_t> > earcutTriangulation(const std::vector<P>& vertices)
{
  std::vector<std::vector<P> > polygon;
  polygon.push_back(vertices);
  Earcut ears; ears(polygon);
  size_t nbt=ears.indices.size()/3;
  std::vector<std::vector<size_t> > tris(nbt);
  typename std::vector<size_t>::iterator it=ears.indices.begin();
  for(size_t i=0;i<nbt; i++)
    {
        tris[i] = std::vector<size_t>(3,0);
        std::vector<size_t>& tri = tris[i];
        tri[0]=*it++;tri[1]=*it++;tri[2]=*it++;
    }
  return tris;
}

} //end namespace xlifepp

#endif // EARCUT_HPP
