/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file combinatorics.hpp
  \author N. Kielbasiewicz
  \since 10 jan 2003
  \date 26 avr 2012

  \brief Definition of combinatorics functions

  Combinatorics functions:
    - binomialCoefficient     compute binomial coeff. C_n^k
    - binomialCoefficients    compute a row of Pascal's triangle
 */

#ifndef COMBINATORICS_HPP
#define COMBINATORICS_HPP

#include "config.h"
#include "utils.h"

namespace xlifepp
{

//--------------------------------------------------------------------------------
// arithmetics: compute binomial coefficients
//--------------------------------------------------------------------------------
number_t binomialCoefficient(int n, int k); //!< compute binomial coefficient C_n^k = = n! / ( k! * (n-k)! )
void binomialCoefficients(std::vector<number_t>& row); //!< compute n-th row of Pascal binomial coefficients where n = row.size()-1
void binomialCoefficientsScaled(std::vector<real_t>& row); //!< compute n-th row of Pascal binomial coefficients scaled by (n-1)! where n = row.size()-1

} // end of namespace xlifepp

#endif /* COMBINATORICS_HPP */
