/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file randomGenerators.hpp
  \author E. Lunéville
  \since 12 jul 2016
  \date 12 jul 2016

  \brief Definition of some basic random generators (based on std::random if C++11 available else use std::rand() function)
*/

#ifndef RANDOM_GENERATORS_HPP
#define RANDOM_GENERATORS_HPP

#include "config.h"
#include <iostream>

namespace xlifepp
{
 enum GaussianGenerator{_BoxMullerGenerator,_MarsagliaGenerator};

 // uniform distribution using <random> if C++11 available else using rand(); front end user
 real_t uniformDistribution(real_t a=0., real_t b=1.);                                          //!< return a sample from an uniform distribution on [a,b[ (using <random> if C11)
 void uniformDistribution(real_t* mat, real_t a, real_t b,number_t n=1, number_t m=1);          //!< compute a matrix with real coefficient following an uniform distribution on [a,b[ (using <random> if C11)
 void uniformDistribution(real_t* mat,number_t n=1, number_t m=1);                              //!< compute a matrix with real coefficient following an uniform distribution on [0,1[ (using <random> if C11)
 void uniformDistribution(complex_t* mat, real_t a, real_t b, number_t n=1, number_t m=1);      //!< compute a matrix with complex coefficient following an uniform distribution on [a,b[ (using <random> if C11)
 void uniformDistribution(complex_t* mat, number_t n=1, number_t m=1);                          //!< compute a matrix with complex coefficient following an uniform distribution on [0,1[ (using <random> if C11)

 // uniform distribution using C style rand() function
 real_t uniformDistributionC(real_t a=0., real_t b=1.);                                          //!< return a sample from an uniform distribution on [a,b[ (using rand())
 void uniformDistributionC(real_t* mat, real_t a, real_t b,number_t n=1, number_t m=1);          //!< compute a matrix with real coefficient following an uniform distribution on [a,b[ (using rand())
 void uniformDistributionC(real_t* mat,number_t n=1, number_t m=1);                              //!< compute a matrix with real coefficient following an uniform distribution on [0,1[ (using rand())
 void uniformDistributionC(complex_t* mat, real_t a, real_t b, number_t n=1, number_t m=1);      //!< compute a matrix with complex coefficient following an uniform distribution on [a,b[ (using rand())
 void uniformDistributionC(complex_t* mat, number_t n=1, number_t m=1);                          //!< compute a matrix with complex coefficient following an uniform distribution on [0,1[ (using rand())

 // normal distribution using <random> if C++11 available else using rand(); front end user
 real_t normalDistribution(real_t mu=0., real_t sigma=1.,GaussianGenerator gg=_MarsagliaGenerator); //!< return a sample from normal distribution (mu,sigma) (using <random> if C11)
 void normalDistribution(real_t* mat, real_t mu, real_t sigma, number_t n=1, number_t m=1);     //!< compute a Gaussian matrix normal distribution (mu,sigma) (using <random> if C11)
 void normalDistribution(real_t* mat, number_t n=1, number_t m=1);                              //!< compute a Gaussian matrix normal distribution (0,1) (using <random> if C11)
 void normalDistribution(complex_t* mat, real_t mu, real_t sigma, number_t n=1, number_t m=1);  //!< compute a complex Gaussian matrix normal distribution (mu,sigma) (using <random> if C11)
 void normalDistribution(complex_t* mat, number_t n=1, number_t m=1);                           //!< compute a complex Gaussian matrix normal distribution (0,1) (using <random> if C11)

 // normal distribution based on C style rand() function
 real_t normalBoxMuller(real_t mu=0., real_t sigma=1.); //!< normal distribution (mu,sigma) using Box Muller method (using rand())
 real_t normalMarsaglia(real_t mu=0., real_t sigma=1.); //!< normal distribution (mu,sigma) using Marsaglia method (using rand())
 real_t normalDistributionC(real_t mu=0., real_t sigma=1.,GaussianGenerator gg=_MarsagliaGenerator); //!< return a sample from normal distribution (mu,sigma) (using rand())
 void normalDistributionC(real_t* mat, real_t mu, real_t sigma, number_t n=1, number_t m=1);     //!< compute a Gaussian matrix normal distribution (mu,sigma) (using rand())
 void normalDistributionC(real_t* mat, number_t n=1, number_t m=1);                              //!< compute a Gaussian matrix normal distribution (0,1) (using rand())
 void normalDistributionC(complex_t* mat, real_t mu, real_t sigma, number_t n=1, number_t m=1);  //!< compute a complex Gaussian matrix normal distribution (mu,sigma) (using rand())
 void normalDistributionC(complex_t* mat, number_t n=1, number_t m=1);                           //!< compute a complex Gaussian matrix normal distribution (0,1) (using rand())
 void normalDistributionC(real_t* mat, real_t mu, real_t sigma, GaussianGenerator gg, number_t n=1, number_t m=1);     //!< compute a Gaussian matrix normal distribution (mu,sigma) (using rand())
 void normalDistributionC(real_t* mat, GaussianGenerator gg, number_t n=1, number_t m=1);                              //!< compute a Gaussian matrix normal distribution (0,1) (using rand())
 void normalDistributionC(complex_t* mat, real_t mu, real_t sigma, GaussianGenerator gg,number_t n=1, number_t m=1);  //!< compute a complex Gaussian matrix normal distribution (mu,sigma) (using rand())
 void normalDistributionC(complex_t* mat, GaussianGenerator gg, number_t n=1, number_t m=1);                           //!< compute a complex Gaussian matrix normal distribution (0,1) (using rand())

//same version with std::vector (only front end functions), user friendly shortcut
template <typename T>
void uniformDistribution(std::vector<T>& v, real_t a=0., real_t b=1.)
 {
     uniformDistribution(&v[0],a,b,v.size(),1);
 }

template <typename T>
void uniformDistribution(std::vector<T>& mat, number_t n, number_t m, real_t a=0., real_t b=1.)
 {
     uniformDistribution(&mat[0],a,b,n,m);
 }

template <typename T>
std::vector<T> uniformDistribution(number_t n, real_t a=0., real_t b=1.)
 {
     std::vector<T> us(n);
     uniformDistribution(&us[0],a,b,n,1);
     return us;
 }

template <typename T>
std::vector<T> uniformDistribution(number_t n, number_t m, real_t a=0., real_t b=1.)
 {
     std::vector<T> us(n*m);
     uniformDistribution(&us[0],a,b,n,m);
     return us;
 }

template <typename T>
void normalDistribution(std::vector<T>& v, real_t mu=0., real_t sigma=1.)
 {
     normalDistribution(&v[0],mu,sigma,v.size(),1);
 }

template <typename T>
void normalDistribution(std::vector<T>& mat, number_t n, number_t m, real_t mu=0., real_t sigma=1.)
 {
     normalDistribution(&mat[0],mu,sigma,n,m);
 }

template <typename T>
std::vector<T> normalDistribution(number_t n, real_t mu=0., real_t sigma=1.)
 {
     std::vector<T> ns(n);
     normalDistribution(&ns[0],mu,sigma,n,1);
     return ns;
 }

template <typename T>
std::vector<T> normalDistribution(number_t n, number_t m, real_t mu=0., real_t sigma=1.)
 {
     std::vector<T> ns(n*m);
     normalDistribution(&ns[0],mu,sigma,n,m);
     return ns;
 }

} // end of namespace xlifepp

#endif /* GAUSS_FORMULAE_HPP */

