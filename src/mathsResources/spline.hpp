/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file spline.hpp
  \author E. Lunéville
  \since 16 jul 2019
  \date  16 jul 2019

  \brief Spline class definition

  The spline class is the abstract base class of C2Spline, CatmullRomSpline and BSpline classes
  C2Spline and CatmullRomSpline are interpolation splines while BSpline corresponds to approximation splines
*/

#ifndef SPLINE_HPP_
#define SPLINE_HPP_

#include "utils.h"
#include "config.h"

namespace xlifepp
{

// forward class
class C2Spline;
class CatmullRomSpline;
class BSpline;
class BezierSpline;
class Nurbs;

//============================================================================================
/*! abstract Spline class
    type_, subtype_: _C2Spline,_CatmullRomSpline, _BSpline, BezierSpline, Nurbs and _SplineInterpolation, _SplineApproximation
    degree_: degree of polynoms used by spline (called spline degree)
    controlPoints, knots: internal arrays common to any splines but handling different stuff
    splinePar_: type of spline parametrization (change the derivatives but not the tangent)
    yps_, ype_: tangent vector at end points (required by some boundary conditions) (1D)
    bces_, bce_: boundary condition at start point and end point
    parameterBounds_: real bounds (a,b) of spline parametrization: parametrization are always set on [0,1]
                       but the spline parametrization functions does the mapping s = a + (b-a)t
*/
//============================================================================================
class Spline
{
  protected:
    mutable std::vector<Point> controlPoints_;  //!< list of control points
    std::multimap<real_t,number_t> knots_;      //!< ordered list of knots
    number_t degree_;                   //!< spline degree (default 3)
    bool isClosed_;                     //!< true if first and last control points are the same
    SplineType type_;                   //!< type of the spline (_C2Spline,_CatmullRomSpline, _BSpline)
    SplineSubtype subtype_;             //!< subtype of spline (_noSplineSubtype,_SplineInterpolation,_SplineApproximation)
    SplineBC bcs_,bce_;                 //!< type of boundary condition at start and end points
    SplineParametrization splinePar_;   //!< type of knots parametrization (_xParametrization, _uniformParametrization, ...)
    std::vector<real_t> yps_, ype_;     //!< tangent vector at bounds required by clamped conditions

  public:
    Parametrization* parametrization_;  //!< pointer to parametrization if not 0

    Spline() {}
    Spline(const Spline&);              //!<copy constructor
    virtual ~Spline() {}
    void checkBC(SplineBC defBC);       //!< boundary conditions of spline

    const std::vector<Point>& controlPoints() const {return controlPoints_;}
    std::vector<Point>& controlPoints() {return controlPoints_;}
    SplineType type() const {return type_;}
    SplineSubtype subtype() const {return subtype_;}
    number_t degree() const {return degree_;}
    number_t nbcp() const {return controlPoints_.size();}
    number_t dim()const {return controlPoints_[0].size();}
    bool isClosed() const {return isClosed_;}
    bool isPeriodic() const {return bcs_==_periodicBC;}
    std::pair<SplineBC,SplineBC> boundaryConditions() const {return std::make_pair(bcs_,bce_);}
    const std::vector<real_t>& yps() const {return yps_;}
    std::vector<real_t>& yps() {return yps_;}
    const std::vector<real_t>& ype() const {return ype_;}
    std::vector<real_t>& ype() {return ype_;}
    const Parametrization& parametrization() const
      {if(parametrization_==nullptr) error("free_error","undefined spline parametrization");
       return *parametrization_;}
    const std::multimap<real_t,number_t>& knots()const {return knots_;}
    RealPair knotsBounds()const;
    virtual RealPair parameterBounds() const;             //!< return parameter bounds
    virtual std::vector<Point> boundNodes() const;        //!< return bound nodes

    virtual Spline* clone() const =0; //!< virtual copy like constructor

    virtual std::multimap<real_t,number_t>::const_iterator locate(real_t t) const;    //!< locate parameter interval
    virtual Point evaluate(real_t t, DiffOpType d=_id) const                          //!< evaluate spline or its derivative at x
    {error("not_handled","Spline::evaluate(x)"); return Point();}
    virtual Vector<Point> evaluate(const std::vector<real_t>& ts, DiffOpType d=_id) const; //!< evaluate spline or its derivative at a list of x
    Point operator()(real_t t, DiffOpType d=_id) const
    {return evaluate(t,d);}
    Vector<Point> operator()(const std::vector<real_t>& ts, DiffOpType d=_id) const
    {return evaluate(ts,d);}

    virtual void print(std::ostream&,bool nocpt=false) const; //!< print utility
    virtual void print(PrintStream& os) const {print(os.currentStream());}

    //@{
    //! downcast to child
    virtual C2Spline* c2Spline() {error("free_error","cannot downcast spline to C2Spline"); return nullptr;}
    virtual const C2Spline* c2Spline() const  {error("free_error","cannot downcast spline to C2Spline"); return nullptr;}
    virtual CatmullRomSpline* catmullRomSpline() {error("free_error","cannot downcast spline to CatmullRomSpline"); return nullptr;}
    virtual const CatmullRomSpline* catmullRomSpline() const  {error("free_error","cannot downcast spline to CatmullRomSpline"); return nullptr;}
    virtual BSpline* bSpline() {error("free_error","cannot downcast spline to BSpline"); return nullptr;}
    virtual const BSpline* bSpline() const  {error("free_error","cannot downcast spline to BSpline"); return nullptr;}
    virtual BezierSpline* bezierSpline() {error("free_error","cannot downcast spline to BezierSpline"); return nullptr;}
    virtual const BezierSpline* bezierSpline() const  {error("free_error","cannot downcast spline to BezierSpline"); return nullptr;}
    virtual Nurbs* nurbs() {error("free_error","cannot downcast spline to Nurbs"); return nullptr;}
    virtual const Nurbs* nurbs() const  {error("free_error","cannot downcast spline to Nurbs"); return nullptr;}
    //@}
};

inline std::ostream& operator<< (std::ostream& out, const Spline& sp)
{sp.print(out); return out;}

//============================================================================================
/*! C2Spline class */
//============================================================================================
class C2Spline: public Spline
{
  protected:
     std::vector<Vector<real_t> > P_;  //!< polynomial coefficients i=1,n-1, k=1,dim: P_[i][4*(k-1)]+P_[i][4*(k-1)+1]x+P_[i][4*(k-1)+2]x^2+P_[i][4*(k-1)+3]x^3
  public:
    void init();
    //! general constructor from a list of control points
    C2Spline(const std::vector<Point>& cpts, SplineParametrization sp=_xParametrization, SplineBC bcs=_naturalBC, SplineBC bce=_naturalBC,
             const std::vector<real_t>& yp0=std::vector<real_t>(3,0.), const std::vector<real_t>& yp1=std::vector<real_t>(3,0.));
    //! constructor from a segment and y values
    C2Spline(real_t a, real_t b, const std::vector<real_t>& ys, SplineParametrization sp=_xParametrization, SplineBC bcs=_naturalBC, SplineBC bce=_naturalBC,
             const std::vector<real_t>& yp0=std::vector<real_t>(3,0.), const std::vector<real_t>& yp1=std::vector<real_t>(3,0.));
    //! copy constructor
    C2Spline(const C2Spline&);
    //! copy C2Spline data in a cleaned C2Spline (re-allocate parametrization_ pointer)
    void copy(const C2Spline&);
    //! assign C2Spline to current one
    C2Spline& operator=(const C2Spline&);
    //! destructor
    virtual ~C2Spline();
    //! clone (copy constructor like)
    virtual C2Spline* clone() const { return new C2Spline(*this);}

    virtual C2Spline* c2Spline() {return this;}
    virtual const C2Spline* c2Spline() const  {return this;}

    virtual Point evaluate(real_t t, DiffOpType d=_id) const;  //!< evaluate spline or its derivative at x
    virtual std::vector<Point> boundNodes() const;       //!< return bound nodes

    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const; //!< parametrization c+(a-c)cos(t)+(b-c)sin(t)
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const; //!< inverse of parametrization c+(a-c)cos(t)+(b-c)sin(t)

    virtual void print(std::ostream&) const; //!< print utility
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

inline std::ostream& operator<< (std::ostream& out, const C2Spline& sp)
{sp.print(out); return out;}
inline Vector<real_t> parametrization_C2Spline(const Point& pt, Parameters& pars, DiffOpType d=_id)    //! extern parametrization call
{return reinterpret_cast<const C2Spline*>(pars("spline").get_p())->funParametrization(pt,pars,d);}
inline Vector<real_t> invParametrization_C2Spline(const Point& pt, Parameters& pars, DiffOpType d=_id) //! extern invParametrization call
{return reinterpret_cast<const C2Spline*>(pars("spline").get_p())->invParametrization(pt,pars,d);}

//============================================================================================
/*! CatmullRomSpline class */
//============================================================================================
class CatmullRomSpline : public Spline
{
    protected:
     real_t tau_;                        //!< tension factor , default 0.5 (centripetal)
    public:
     void init();
     CatmullRomSpline(const std::vector<Point>& cpts, SplineParametrization sp=_chordalParametrization, real_t tau=0.5,
                      SplineBC bcs=_naturalBC, SplineBC bce=_naturalBC,
                      const std::vector<real_t>& yp0=std::vector<real_t>(3,0.), const std::vector<real_t>& yp1=std::vector<real_t>(3,0.));
     //! copy constructor
     CatmullRomSpline(const CatmullRomSpline&);
     //! copy CatmullRomSpline data in a cleaned CatmullRomSpline (re-allocate parametrization_ pointer)
     void copy(const CatmullRomSpline&);
     //! assign CatmullRomSpline to current one
     CatmullRomSpline& operator=(const CatmullRomSpline&);
     virtual CatmullRomSpline* clone() const { return new CatmullRomSpline(*this); }
     virtual ~CatmullRomSpline();

     virtual CatmullRomSpline* catmullRomSpline() {return this;}
     virtual const CatmullRomSpline* catmullRomSpline() const  {return this;}


     virtual Point evaluate(real_t t, DiffOpType d=_id) const;      //!< evaluate spline or its derivative at x
     virtual std::vector<Point> boundNodes() const;           //!< return bound nodes

     Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const;   //!< parametrization
     Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const;   //!< inverse of parametrization

     virtual void print(std::ostream&) const; //!< print utility
     virtual void print(PrintStream& os) const {print(os.currentStream());}
};

inline std::ostream& operator<< (std::ostream& out, const CatmullRomSpline& sp)
{sp.print(out); return out;}
inline Vector<real_t> parametrization_CatmullRomSpline(const Point& pt, Parameters& pars, DiffOpType d=_id)    //! extern parametrization call
{return reinterpret_cast<const CatmullRomSpline*>(pars("spline").get_p())->funParametrization(pt,pars,d);}
inline Vector<real_t> invParametrization_CatmullRomSpline(const Point& pt, Parameters& pars, DiffOpType d=_id) //! extern invParametrization call
{return reinterpret_cast<const CatmullRomSpline*>(pars("spline").get_p())->invParametrization(pt,pars,d);}

//============================================================================================
/*! BSpline class (rational B-Spline) */
//============================================================================================
class BSpline : public Spline
{
    protected:
     std::vector<Point> interpolationPoints_; //!< list of interpolation points if interpolating BSpline is used
     std::vector<real_t> weights_;            //!< weights related to control points (default 1)
     bool noWeight_;                          //!< true if weight=(1,1, ...,1)
     real_t t0_, tf_;                         //!< parameter bounds
     std::map<real_t,number_t> paramMap_;     //!< additional map used by non uniform parametrization to store s0,s1,s2, ...

    public:
     void init();
     void initInterp();
     BSpline(const std::vector<Point>& cpts, number_t degree=3,
             SplineBC bcs=_undefBC, SplineBC bce=_undefBC,
             const std::vector<real_t>& weights=std::vector<real_t>());   //!< create B-Spline approximation
     BSpline(SplineSubtype sbt, const std::vector<Point>& cpts, number_t degree=3,
             SplineParametrization spar = _uniformParametrization,
             SplineBC bcs=_undefBC, SplineBC bce=_undefBC,
             const std::vector<real_t>& yp0=std::vector<real_t>(), const std::vector<real_t>& yp1=std::vector<real_t>(),
             const std::vector<real_t>& weights=std::vector<real_t>());  //!< create Bspline approximation or interpolation

     //! copy constructor
     BSpline(const BSpline&);
     //! copy BSpline data in a cleaned BSpline (re-allocate parametrization_ pointer)
     void copy(const BSpline&);
     //! assign BSpline to current one
     BSpline& operator=(const BSpline&);

     virtual BSpline* clone() const { return new BSpline(*this);}
     virtual ~BSpline();

     virtual BSpline* bSpline() {return this;}
     virtual const BSpline* bSpline() const  {return this;}

     const std::vector<Point>& interpolationPoints() const {return interpolationPoints_;}
     std::vector<Point>& interpolationPoints() {return interpolationPoints_;}
     const std::vector<real_t>& weights() const {return weights_;}

     void computeB(real_t t, DiffOpType dif, number_t& j, Vector<real_t>& B,
                   Vector<real_t>& dB, Vector<real_t>& d2B) const;                    //!< compute tool called by evaluate
     virtual Point evaluate(real_t t, DiffOpType d=_id) const;                        //!< evaluate spline or its derivative at t
     virtual Point evaluate2(real_t t, DiffOpType d=_id) const;                       //!< evaluate spline or its derivative at t
     virtual std::vector<Point> boundNodes() const;                                   //!< return bound nodes
     RealPair parameterBounds() const                                                 //!< real parameter bounds
       {return RealPair(t0_,tf_);}

     Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const; //!< parametrization
     Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const; //!< inverse of parametrization
     real_t toKnotsParameter(real_t t) const //! map t in [0,1] to knots parameter
       {return t0_+t*(tf_-t0_);}

     virtual void print(std::ostream&, bool nocpt=false) const; //!< print utility
     virtual void print(PrintStream& os) const {print(os.currentStream());}
};

inline std::ostream& operator<< (std::ostream& out, const BSpline& sp)
{sp.print(out); return out;}
inline Vector<real_t> parametrization_BSpline(const Point& pt, Parameters& pars, DiffOpType d=_id)    //! extern parametrization call
{return reinterpret_cast<const BSpline*>(pars("spline").get_p())->funParametrization(pt,pars,d);}
inline Vector<real_t> invParametrization_BSpline(const Point& pt, Parameters& pars, DiffOpType d=_id) //! extern invParametrization call
{return reinterpret_cast<const BSpline*>(pars("spline").get_p())->invParametrization(pt,pars,d);}

//============================================================================================
/*! BezierSpline class  */
//============================================================================================
class BezierSpline : public Spline
{
    public:
     BezierSpline(const std::vector<Point>& cpts);
     //! copy constructor
     BezierSpline(const BezierSpline&);
     //! copy BezierSpline data in a cleaned BezierSpline (re-allocate parametrization_ pointer)
     void copy(const BezierSpline&);
     //! assign BezierSpline to current one
     BezierSpline& operator=(const BezierSpline&);

     virtual BezierSpline* clone() const { return new BezierSpline(*this);}
     virtual ~BezierSpline();

     virtual BezierSpline* bezierSpline() {return this;}
     virtual const BezierSpline* bezierSpline() const  {return this;}

     RealPair parameterBounds() const{return RealPair(0.,1.);}                                      //! return parameter bounds

     virtual Point evaluate(real_t t, DiffOpType d=_id) const;                                      //!< evaluate spline or its derivative at t
     Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const;  //!< parametrization
     Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const;  //!< inverse of parametrization

     virtual void print(std::ostream&) const; //!< print utility
     virtual void print(PrintStream& os) const {print(os.currentStream());}
};

inline std::ostream& operator<< (std::ostream& out, const BezierSpline& sp)
{sp.print(out); return out;}
inline Vector<real_t> parametrization_BezierSpline(const Point& pt, Parameters& pars, DiffOpType d=_id)    //! extern parametrization call
{return reinterpret_cast<const BezierSpline*>(pars("spline").get_p())->funParametrization(pt,pars,d);}
inline Vector<real_t> invParametrization_BezierSpline(const Point& pt, Parameters& pars, DiffOpType d=_id) //! extern invParametrization call
{return reinterpret_cast<const BezierSpline*>(pars("spline").get_p())->invParametrization(pt,pars,d);}


//============================================================================================
/*! Nurbs class implements non uniform rational Bspline for 3D surface
    it is built from BSpline's in two parameter directions (u,v), may be seen as a cross product of two BSpline
    it inherits from Spline class but some Spline data are not used or replaced
    it may deal with approximation or interpolation subtype. When not specified, use interpolation
*/
//============================================================================================
class Nurbs : public Spline
{
    protected:
     mutable std::vector<std::vector<Point> > controlPointsM_;  //!< list of control points (matrix)
     std::vector<std::vector<real_t> > weightsM_;               //!< list of weights (matrix)
     BSpline* bs_u, *bs_v;                                      //!< u/v-Bspline
     bool noWeight_;                                            //!< true if weightsM_ is empty (same as weights= (1,1,...))

    public:
     //!constructor from  control points and weights given as vectors of vectors ( (p11,... p1n), (p21, ... p2n), ..., (pm1,... pmn) )
     Nurbs(const std::vector<std::vector<Point> >& cpts, number_t degree_u=3,number_t degree_v=3,
           SplineBC bcs_u=_clampedBC, SplineBC bcs_v=_clampedBC, SplineBC bce_u=_undefBC, SplineBC bce_v=_undefBC,
           const std::vector<std::vector<real_t> >& w=std::vector<std::vector<real_t> >());
     Nurbs(SplineSubtype sbt, const std::vector<std::vector<Point> >& cpts, number_t degree_u=3,number_t degree_v=3,
           SplineBC bcs_u=_clampedBC, SplineBC bcs_v=_clampedBC, SplineBC bce_u=_undefBC, SplineBC bce_v=_undefBC,
           const std::vector<std::vector<real_t> >& w=std::vector<std::vector<real_t> >());
     //!constructor from  control points and weights given as vectors (p11,... p1n,p21, ... p2n, ..., pm1,... pmn)
     Nurbs(const std::vector<Point>& cpts, number_t ncpu, number_t degree_u=3,number_t degree_v=3,
           SplineBC bcs_u=_clampedBC, SplineBC bcs_v=_clampedBC, SplineBC bce_u=_undefBC, SplineBC bce_v=_undefBC,
           const std::vector<real_t>& w=std::vector<real_t>());
     Nurbs(SplineSubtype sbt, const std::vector<Point>& cpts, number_t ncpu, number_t degree_u=3,number_t degree_v=3,
           SplineBC bcs_u=_clampedBC, SplineBC bcs_v=_clampedBC, SplineBC bce_u=_undefBC, SplineBC bce_v=_undefBC,
           const std::vector<real_t>& w=std::vector<real_t>());
     //!< build controlPointsM_ and weightsM_ matrices from vectors
     void initMatrices(number_t ncpu, const std::vector<Point>& cpts, const std::vector<real_t>& w);
     //! initialize Nurbs
     void init(number_t degree_u,number_t degree_v, SplineBC bcs_u, SplineBC bcs_v, SplineBC bce_u, SplineBC bce_v);
     //! additional initialization when interpolated nurbs
     void initInterp();
     //! copy constructor
     Nurbs(const Nurbs&);
     //! copy Nurbs data in a cleaned Nurbs (re-allocate parametrization_ pointer)
     void copy(const Nurbs&);
     //! assign Nurbs to current one
     Nurbs& operator=(const Nurbs&);

     virtual Nurbs* clone() const { return new Nurbs(*this);}
     virtual ~Nurbs();
     virtual Nurbs* nurbs() {return this;}
     virtual const Nurbs* nurbs() const  {return this;}

    const std::vector<std::vector<Point> >& controlPointsM() const {return controlPointsM_;}
    std::vector<std::vector<Point> >& controlPointsM() {return controlPointsM_;}
    const std::vector<std::vector<real_t> >& weightsM() const {return weightsM_;};
    std::vector<std::vector<real_t> >& weightsM() {return weightsM_;};
    const BSpline* bsu() const {return bs_u;}
    const BSpline* bsv() const {return bs_v;}
    const bool noWeight() const {return noWeight_;}
    number_t nbcp() const {return controlPointsM_.size()*controlPointsM_[0].size();}
    number_t dim()const {return controlPointsM_[0][0].size();}
    const Parametrization& parametrization() const
     {if(parametrization_==nullptr) error("free_error","undefined spline parametrization");
      return *parametrization_;}

    RealPair uBounds() const {return bs_u->parameterBounds();}         //!< return u-parameter bounds
    RealPair vBounds() const {return bs_v->parameterBounds();}         //!< return v-parameter bounds

    Point evaluate(real_t u, real_t v, DiffOpType d=_id) const;        //!< evaluate nurbs or its derivative at (u,v)
    Point operator()(real_t u,real_t v,DiffOpType d=_id) const         //!< evaluate nurbs or its derivative at (u,v)
     {return evaluate(u,v,d);}

    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const;       //!< parametrization
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const;       //!< inverse of parametrization

    void print(std::ostream&) const; //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());}
};

inline std::ostream& operator<< (std::ostream& out, const Nurbs& sp)
{sp.print(out); return out;}
inline Vector<real_t> parametrization_Nurbs(const Point& pt, Parameters& pars, DiffOpType d=_id)    //! extern parametrization call
{return reinterpret_cast<const Nurbs*>(pars("spline").get_p())->funParametrization(pt,pars,d);}
inline Vector<real_t> invParametrization_Nurbs(const Point& pt, Parameters& pars, DiffOpType d=_id) //! extern invParametrization call
{return reinterpret_cast<const Nurbs*>(pars("spline").get_p())->invParametrization(pt,pars,d);}

} //end of namespace xlifepp
#endif // SPLINE_HPP
