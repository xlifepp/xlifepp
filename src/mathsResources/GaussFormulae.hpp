/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file GaussFormulae.hpp
  \author N. Kielbasiewicz
  \since 10 jan 2003
  \date 26 avr 2012

  \brief Definition of Gauss formulae functions

  Gauss quadrature formulae:
    - GaussLegendreRule, GaussLegendreRuleComputed
    - GaussLobattoRule, GaussLobattoRuleComputed, GaussLobattoPoints
*/

#ifndef GAUSS_FORMULAE_HPP
#define GAUSS_FORMULAE_HPP

#include "config.h"
#include "utils.h"
#include <iostream>

namespace xlifepp
{

//--------------------------------------------------------------------------------
// Gauss-Legendre 1D quadrature points and weights on [-1 , 1]
// - Abscissas x_j are roots of Legendre Polynomial P_n of ordre n
// - Weights are 2/( (1-x_j^2) (P'_n(x_j)^2) )
// Note: the (n+1)/2 first positive points in ascending order only are returned.
//--------------------------------------------------------------------------------
void gaussLegendreRule(number_t, std::vector<real_t>&, std::vector<real_t>&); //!< returns Gauss-Legendre rule with computed or tabulated
void gaussLegendreRuleComputed(number_t , std::vector<real_t>&, std::vector<real_t>&); //!< returns Gauss-Legendre rule for any number of points
void gaussLegendreOutput(number_t, std::ostream&); //!< "tabulated output" of Gauss-Lobatto rules up to a given number of points

//--------------------------------------------------------------------------------
// Gauss-Lobatto 1D quadrature points and weights on [-1 , 1]
// - Abscissas x_j are
// -        roots of (1-x^2)*P'_{n-1} with P'_{n-1} derivative of Legendre Polynomial of ordre n-1
// - Weights are
// -        2./(n*(n-1)) for end points -1 and +1
// -        2./(n*(n-1)*{P_{n-1}(x_j)}^2) for roots of P'_{n-1}
// Note: the (n-2)/2 first positive points in ascending order only are returned
// that is excluding end point 1 and corresponding weight.
//--------------------------------------------------------------------------------
void gaussLobattoRule(number_t, std::vector<real_t>&, std::vector<real_t>&); //!< returns Gauss-Lobatto rule with computed or tabulated
void gaussLobattoRuleComputed(number_t, std::vector<real_t>&, std::vector<real_t>&); //!< returns Gauss-Lobatto rule for any number of points
void gaussLobattoPoints(number_t, std::vector<real_t>&); //!< returns Gauss-Lobatto points for any number of points
void gaussLobattoOutput(number_t, std::ostream&); //!< "tabulated output" of Gauss-Lobatto rules up to a given number of points

//--------------------------------------------------------------------------------
// Gauss-Jacobi 1D quadrature points and weights on [-1 , 1]
// - Abscissas x_j are roots of Jacobi Polynomial of ordre n
// - Weights are
//--------------------------------------------------------------------------------
void gaussJacobiRule(number_t, real_t, real_t, std::vector<real_t>&, std::vector<real_t>&); //!< returns Gauss-Jacobi rule with tabulated
void gaussJacobi20Rule(number_t, std::vector<real_t>&, std::vector<real_t>&); //!< returns Gauss-Jacobi (2,0) rule with tabulated
void gaussJacobiOutput(number_t, real_t, real_t, std::ostream&); //!< "tabulated output" of Gauss-Jacobi rules up to a given number of points
void gaussJacobi20Output(number_t, std::ostream&); //!< "tabulated output" of Gauss-Jacobi (2,0) rules up to a given number of points

} // end of namespace xlifepp

#endif /* GAUSS_FORMULAE_HPP */
