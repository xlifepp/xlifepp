/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file helmholtz2d.cpp
  \author N. Salles
  \since 17 august 2016
  \date 17 august 2016

  \brief Implementation of exact solutions of Helmholtz2d problems
 */

#include "exactSolutions.hpp"
#include "../specialFunctions/specialFunctions.hpp"

namespace xlifepp
{

/*!
  Exact solution to the Helmholtz 2D problem outside a sphere:
  Field scattered by solid sphere of radius Rad of an incoming field Phi_w= exp^{(i*K*x}).
  with Dirichlet boundary condition on the sphere
 */
complex_t scatteredFieldDiskDirichlet(const Point& p, Parameters& param)
{

  real_t K(1.), Rad(1.);
  number_t nmax=50;
  if (param.contains("k")) K = param("k").get_r();
  if (param.contains("radius")) Rad = param("radius").get_r();
  if(param.contains("nmax")) nmax=param("nmax");

  const real_t krad=K * Rad;

  complex_t z(p[0],p[1]);
  real_t r=std::abs(z);
  z/=r;
  complex_t res(0.,0.);
  real_t Nr=real_t(nmax);
  real_t b=acosh(Nr/krad);
  if(std::log(0.5*theRealMax)<(std::log(std::sqrt(2./(pi_*krad*std::sinh(b))))+Nr*(b-std::tanh(b))))
    {
      bool Ntoolarge=true;
      while(Ntoolarge)
        {
          nmax--;
          Nr=real_t(nmax);
          b=acosh(Nr/krad);
          Ntoolarge=(std::log(0.5*theRealMax)<(std::log(std::sqrt(2./(pi_*krad*std::sinh(b))))+Nr*(b-std::tanh(b))));
        }
      warning("free_warning","nmax is too large in scatteredFieldDiskDirichlet, new nmax = "+str(nmax));
    }
  std::vector<real_t> bjr=besselJ0N(krad,nmax);
  std::vector<complex_t> hkr=hankelH10N(krad,nmax);
  std::vector<complex_t> hk=hankelH10N(K*r, nmax);

  real_t cosnt=0.;
  complex_t it(1.,0.);
  for(number_t n=1; n<=nmax; n++)
    {
      it*=i_;
      cosnt=real(pow(z,real_t(n)));
      res-=cosnt*bjr[n]*(it*hk[n]/hkr[n]);
    }
  res*=2.;
  res-=bjr[0]*(hk[0]/hkr[0]);
  return res;
}

complex_t scatteredFieldDiskNeumann(const Point& p, Parameters& param)
{
  real_t k=1., R=1.;
  if (param.contains("k")) k = param("k").get_r();
  if (param.contains("radius")) R = param("radius").get_r();
  real_t x=p[0], y=p[1];
  real_t r = std::sqrt(x*x+y*y);
  const real_t kR =k*R, kr=k*r;
  real_t t = std::atan2(y,x);
  number_t nmax=50;
  if(param.contains("nmax")) nmax=param("nmax");
  else nmax = std::floor(k*r+10*std::log(k*r+pi_));

  std::vector<real_t>    jkR=besselJ0N(kR,nmax);
  std::vector<complex_t> hkR=hankelH10N(kR,nmax);
  std::vector<complex_t> hkr=hankelH10N(kr,nmax);
  complex_t it(1.,0.);
  complex_t res(0.,0.);
  for(number_t n=1; n<=nmax; n++)
  {
      it*=i_;
      real_t jnkRd=jkR[n-1]-(n/kR)*jkR[n];
      complex_t hnkRd=hkR[n-1]-(n/kR)*hkR[n];
      res-=it*std::cos(n*t)*jnkRd*hkr[n]/hnkRd;
  }
  res*=2;
  res-=jkR[1]*hkr[0]/hkR[1];
  return res;
}
} // end of namespace xlifepp
