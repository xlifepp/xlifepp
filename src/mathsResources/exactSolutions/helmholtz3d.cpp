/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file helmholtz3d.cpp
  \author D. Martin
  \since 09 oct 2010
  \date 15 jun 2011

  \brief Implementation of exact solutions of Helmholtz3d problems
 */

#include "exactSolutions.hpp"
#include "../specialFunctions/specialFunctions.hpp"

namespace xlifepp
{

/*!
  Exact solution to the Helmholtz 3D problem outside a sphere:
  Field scattered by solid sphere of radius Rad of an incoming field Phi_w= exp^{(i*K*x}).
  with Neumann boundary condition on the sphere
 */
complex_t scatteredFieldSphereNeumann(const Point& p, Parameters& param)
{
  // We first recover the current value of the wave number K and radius of sphere
  real_t K(1.), Rad(1.);
  if (param.contains("k")) K=real(param("k"));
  if (param.contains("radius")) Rad = real(param("radius"));
  const real_t kr= K * Rad;
  int nmax= 50;
  if (param.contains("nmax")) nmax=param("nmax");

  // spherical Bessel functions up to order nmax for argument K*Rad
  std::vector<real_t> js(nmax + 1), ys(nmax + 1);
  js = sphericalbesselJ0N(kr, nmax);
  ys = sphericalbesselY0N(kr, nmax);

  // computation of coefficients an(K*Rad) until a minimum n is found such that
  // | an(K*Rad) | <epsilon (as the series is known to converge quickly)
  //
  // initialize n, 2n+1, i^n
  const complex_t i1(0., 1.);
  int n=0, nm1=-1, np1=1;
  real_t nx2p1=1.;
  complex_t hnp, in=1.;
  std::vector<complex_t> a(nmax);
  // Use recurrence relation for n=0 : d/dz[H_{0}](z)=- H_{1}(z)
  a[0] = - js[1] / complex_t(js[1], ys[1]);
  while (std::abs(a[n]) > theZeroThreshold && n < nmax)
  {
    in *= i1; n++; nx2p1 += 2; nm1++; np1++;
    // Use recurrence relation to compute derivative: d/dz[H_{n}](z)
    //   (2n+1) d/dz[H_{n}](z)=n*H_{n-1}(z)-(n+1)*H_{n+1}(z)
    //   http://www.math.sfu.ca/~cbm/aands/page_439.htm (formula 10.1.20)
    // d/dz[H_{n}](K*Rad)
    hnp = n * complex_t(js[nm1], ys[nm1]) - np1 * complex_t(js[np1], ys[np1]);
    a[n] = - in * nx2p1 * hnp.real() / hnp;
  }
  int nf=n;
  if (nf >= nmax)
  {
		where("scatteredField_SphereNeuman");
		error("conv_failed",nmax);
  }
  // spherical coordinates rho and phi
  real_t rho= std::sqrt(p[0]*p[0] + p[1]*p[1] + p[2]*p[2]);
  real_t cosp= p[0] / rho;

  // Compute partial sum of the series (including Legendre polynomials)
  // spherical Bessel functions up to order nf for argument K*rho
  js = sphericalbesselJ0N(K * rho, nf);
  ys = sphericalbesselY0N(K * rho, nf);

  // initialize computation of Legendre polynomials and (2n+1)*cosp
  real_t pcnm1=1., pcn=cosp, pcnp1, n2p1cosp=cosp, twocosp= 2 * cosp;

  // initialize Phi series:
  complex_t Phi= a[0]*complex_t(js[0], ys[0]);
  for (int n = 1; n < nf; n++)
  {
    Phi += a[n] * complex_t(js[n], ys[n]) * pcn;
    // Recurrence relation for Legendre polynomials
    // (n+1) P_{n+1}(cosp)=(2n+1)*cosp*P_{n}(cosp)-n*P_{n-1}(cosp)
    //   http://en.wikipedia.org/wiki/Legendre_polynomials
    n2p1cosp += twocosp;
    pcnp1 = (n2p1cosp * pcn - n * pcnm1) / (n + 1);
    pcnm1 = pcn;
    pcn = pcnp1;
  }
  return Phi;
}

/*!
  Exact solution to the Helmholtz 3D problem outside a sphere:
  Field scattered by solid sphere of radius Rad of an incoming field Phi_w= exp^{(i*K*x}).
  with Dirichlet boundary condition on the sphere
 */
complex_t scatteredFieldSphereDirichlet(const Point& p, Parameters& param)
{
  // We first recover the current value of the wave number K and radius of sphere
  real_t K(1.), Rad(1.);
  if (param.contains("k"))  { K = real((param)("k")); }
  if (param.contains("radius")) { Rad = real(param("radius"));}
  int nmax= 50;
  if (param.contains("nmax")) nmax=param("nmax");
  const real_t kr=K * Rad;

  // spherical Bessel functions up to order nmax for argument K*Rad
  std::vector<real_t> js(nmax + 1), ys(nmax + 1);
  js = sphericalbesselJ0N(kr, nmax);
  ys = sphericalbesselY0N(kr, nmax);

  // computation of coefficients an(K*Rad) until a minimum n is found such that
  // | an(K*Rad) | <epsilon (as the series is known to converge quickly)
  // initialize n, 2n+1, i^n
  const complex_t i1(0., 1.);
  int n=0;
  real_t nx2p1=1.;
  complex_t in=1.;
  std::vector<complex_t> a(nmax);
  a[0] = js[0] / complex_t(js[0], ys[0]);
  while (std::abs(a[n]) > theZeroThreshold && n < nmax)
  {
    in *= i1; n++; nx2p1 += 2;
    a[n] = in * nx2p1 *js[n] / complex_t(js[n], ys[n]);
  }
  int nf=n;
  if (nf >= nmax)
  {
		where("scatteredField_SphereDirichlet");
		error("conv_failed",nmax);
  }
  // spherical coordinates rho and phi
  real_t rho=std::sqrt(p[0]*p[0] + p[1]*p[1] + p[2]*p[2]);
  real_t cosp = p[0] / rho;

  // Compute partial sum of the series (including Legendre polynomials)
  // spherical Bessel functions up to order nf for argument K*rho
  js = sphericalbesselJ0N(K * rho, nf);
  ys = sphericalbesselY0N(K * rho, nf);

  // initialize computation of Legendre polynomials and (2n+1)*cosp
  real_t pcnm1=1., pcn=cosp, pcnp1, n2p1cosp=cosp, twocosp=2 * cosp;

  // initialize Phi series:
  complex_t Phi=-a[0]*complex_t(js[0], ys[0]);
  for (int n = 1; n < nf; n++)
  {
    Phi += -a[n] * complex_t(js[n], ys[n]) * pcn;
    // Recurrence relation for Legendre polynomials
    // (n+1) P_{n+1}(cosp)=(2n+1)*cosp*P_{n}(cosp)-n*P_{n-1}(cosp)
    //   http://en.wikipedia.org/wiki/Legendre_polynomials
    n2p1cosp += twocosp;
    pcnp1 = (n2p1cosp * pcn - n * pcnm1) / (n + 1);
    pcnm1 = pcn;
    pcn = pcnp1;
  }
  return Phi;
}
} // end of namespace xlifepp

