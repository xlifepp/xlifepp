/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file exactSolutions.hpp
  \author N. Kielbasiewicz
  \since 10 jan 2003
  \date 26 avr 2013

  \brief Definition of some functions computing exact solutions for Helmholtz 3d problems

  Some exact solutions for Helmholtz 3d problems:
    - scatteredFieldSphereNeumann: scattered field of incoming wave on sphere for Helmholtz equation
    - scatteredFieldMaxwellExn: scattered field of incoming wave on sphere for Maxwell's equation
    - sphericalHarmonics: spherical harmonics Y_l^m
    - sphericalHarmonicsSurfaceGrad: surface gradient of spherical harmonics: grad_S(Y_l^m)
 */

#ifndef EXACT_SOLUTIONS_HPP
#define EXACT_SOLUTIONS_HPP

#include "config.h"
#include "utils.h"

namespace xlifepp
{

/*!
 -------------------------------------------------------------------------------
   'Exact solutions' to the Helmholtz 3D problem outside a sphere
 -------------------------------------------------------------------------------

 Field scattered by solid sphere of radius Rad of an incoming field Phi_w= exp^{(i*k*x}).
 with Neumann boundary condition on the sphere:
 Series representation of Bessel spherical functions and Legendre polynomials

 \f$\Phi(\rho,\phi) = \sum_{n=0}^\infty A\_n(K*Rad) h_n^{(1)}(K*\rho) P_n(cos(\phi))\f$

 where \f$A_n(r) = - i^n (2*n+1) { d/dr[j_n](r) / d/dr[h_n^{(1)}](r) }\f$
 and
 - h_n^{(1)} = H_{n} = j_n + i*y_n is the spherical hankel function of first kind and order n
 - j_n: spherical bessel function of the first  kind and order n
 - y_n: spherical bessel function of the second kind and order n

 -- for spherical Bessel functions, see
 -    HANDBOOK Of MATHEMATICAL FUNCTIONS, Ed. M.ABRAMOWITZ & I.A. STEGUN
 -    Chap. 10 : Bessel Functions of Fractional Order (p.437-494)
 -    http://www.math.sfu.ca/~cbm/aands/page_437.htm
 -- for Legendre polynomials, see
 -    http://en.wikipedia.org/wiki/Legendre_polynomials
 */
complex_t scatteredFieldSphereNeumann(const Point&, Parameters&);

/*!Field scattered by solid sphere of radius Rad of an incoming field Phi_w= exp^{(i*k*x}).
 with Dirichlet boundary condition on the sphere:
 Series representation of Bessel spherical functions and Legendre polynomials

 \f$\Phi(\rho,\phi) = \sum_{n=0}^\infty A\_n(K*Rad) h_n^{(1)}(K*\rho) P_n(cos(\phi))\f$

 where \f$A_n(r) = i^n (2*n+1) { j_n(kR)/ h_n^{(1)}(kR)}\f$
 */
complex_t scatteredFieldSphereDirichlet(const Point&, Parameters&);

// These functions ***REQUIRE*** an associated Parameters as second argument "containing"
// - a Parameter bearing the name "k" and holding the value of the wave number k.
// - a Parameter bearing the name "radius" and holding the value of the sphere radius .

  /*!
 -------------------------------------------------------------------------------
   'Exact solutions' to the Helmholtz 2D problem outside a disk
 -------------------------------------------------------------------------------
  Field scattered by solid disk of radius Rad of an incoming field Phi_w= exp^{(i*k*x}).
  */
complex_t scatteredFieldDiskDirichlet(const Point&, Parameters&);

complex_t scatteredFieldDiskNeumann(const Point&, Parameters&);
// These functions ***REQUIRE*** an associated Parameters as second argument "containing"
// - a Parameter bearing the name "k" and holding the value of the wave number k.
// - a Parameter bearing the name "radius" and holding the value of the sphere radius .

/*!
--------------------------------------------------------------------------------
   'Exact solutions' to the Maxwell 3D problem outside a sphere
--------------------------------------------------------------------------------

 Field scattered by solid sphere of radius R of an incoming field Phi_w= exp^{(i*k*z}).
 with tangential boundary condition Exn = 0 (electric field) on the sphere:

 \f$\Phi(\rho,\theta \phi) = \sum_{n=1..\infty} i^n * (2n+1) *
         cos\phi*e_rho*c_rho[n] + cos\phi*e_theta*c_theta[n] + sin\phi*c_phi[n]\f$

 with
 -        [ \f$\sin\theta*\cos\phi\f$ ]          [ \f$\cos\theta*\cos\phi\f$ ]        [ \f$-\sin\phi\f$ ]
 - e_rho =[ \f$\sin\theta*\sin\phi\f$ ] e_theta =[ \f$\cos\theta*\sin\phi\f$ ] e_phi =[ \f$\cos\phi\f$  ]
 -        [ \f$\cos\theta\f$          ]          [ \f$-\sin\theta\f$         ]        [        0        ]
 and
 - \f$c_rho[n]   = - A_n(K*R) * H_{n}(K*rho)/K*rho ) * Ps_n(\cos\theta)\f$
 - c_theta[n] =
 - c–phi[n]   =

 where
 -  A_n(r) = -i^{n-1} (2*n+1) real(hnp(r)) / hnp(r)      where hnp(r) = d/dr[H_{n}](r) + H_{n}(r)
 -  B_n(r) = -i^{n-1} (2*n+1) real(H_{n}(r)) / H_{n}(r)
 and
 - H_{n} = h_n^{(1)} = j_n + i*y_n is the spherical hankel function of first kind and order n
 - j_n: spherical bessel function of the first  kind and order n
 - y_n: spherical bessel function of the second kind and order n
 */
Vector<complex_t> scatteredFieldMaxwellExn(const Point&, Parameters&);
// This function ***REQUIRES*** an associated Parameters as second argument "containing"
// a Parameter bearing the name "k" and holding the value of the wave number k.

/*!
--------------------------------------------------------------------------------
   Spherical harmonics and derivatives
--------------------------------------------------------------------------------

 Spherical harmonics Y^{m}_l up to order n (l=0,..,n; m=0,..,l) on the unit sphere
 We use here the normalized definition, in which harmonics form an orthonormal
 basis in L^2(unit_sphere).
 In spherical coordinates
 - \f$r = \sqrt{x*^2+y^2+z^2} , \cos\theta = z/r, \tan\phi = y/x\f$
 - with \f$\theta in [0, Pi], \phi in [0, 2*Pi]\f$, we have the following formula:

 \f$Y^{m}_{l} = \sqrt{(2*l+1)/4Pi * (l-m)!/(l+m)!} * P^{m}_{l}(\cos\theta) * \exp(i*m*\phi)\f$

 where \f$P^{m}_{l}\f$ is the m-th associated Legendre polynomial of order l.

 \f$Y^{m}_{l}\f$ satisfies for negative m: \f$Y^{m}_{l} = (-1)^{m} Y^{-m}_{l}*\f$
 where (terminal) * denotes the complex conjugate.

 For spherical harmonics see
 - http://en.wikipedia.org/wiki/Spherical_harmonics
 and for associated Legendre functions
 - http://en.wikipedia.org/wiki/Associated_Legendre_polynomials
 */
void sphericalHarmonics(const Point& x, std::vector< std::vector<complex_t> >& Yml);
// Spherical harmonics Y_{l}^{m} up to order n on the unit sphere
// the order n is computed by n = Yml.size()-1 and spherical harmonics Yml
// are computed for l = 0,..,n and m = 0,..,l,
// assuming that the size of std::vector Yml[l] is l+1, which can be done as follows
//   std::vector< std::vector<complex_t> > Yml(n+1);
//   for ( int l = 0; l <= Yml.size()-1; l++ ) Yml[l] = std::vector<complex_t>(l+1);

/*!
 Spherical harmonics surface gradient gradS_Y^{m}_l up to order n (l=0,..,n; m=0,..,l) on the unit sphere
- Case (\f$\sin(\theta) = 0\f$) :
.   gradS_Y_{l}^{1} = \f$\sqrt{ (2l+1)/4\pi * (l-1)! / (l+1)! } * e^{i \phi} *
.                     (P_{l}^{'1}(\cos(\theta)) e_\theta - i (\cos(\theta))^{l+1} \cos(\phi) l(l+1)/2 * e_\phi)\f$
.   where \f$e_\theta\f$ and \f$e_\phi\f$ are the unit tangent std::vector on the unit sphere.
.   gradS_Y_{l}^{m}(x) = 0   if m =/= 1
.
- Case (\f$\sin(\theta) /= 0\f$) :
.   gradS_Y_{0}^{0}(x) = 0
.   For l > 0 :
.      gradS_Y_{l}^{m} = \f$\sqrt{ (2l+1)/4\pi * (l-m)! / (l+m)! } * e^{im \phi} *
.                        ( P_{l}^{'|m|}(\cos(\theta)) e_\theta + im * P_{l}^{|m|}(\cos(\theta)) e_\phi)\f$
.      where \f$e_\theta\f$ and \f$e_\phi\f$ are the unit tangent std::vector on the unit sphere.
 */
void sphericalHarmonicsSurfaceGrad(const Point&, std::vector<std::vector<Vector<complex_t> > >&);
// surface gradient of Spherical harmonics

// FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //
void sphericalHarmonicsTest(const Point&, const number_t, std::ostream&); //!< output function for test of Spherical harmonics
void sphericalHarmonicsSurfaceGradTest(const Point&, const number_t, std::ostream&); //!< output function for test of Spherical harmonics and derivatives
// FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST // FOR TEST //

} // end of namespace xlifepp

#endif /* EXACT_SOLUTIONS_HPP */

