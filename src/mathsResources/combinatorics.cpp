/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file combinatorics.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 15 jun 2007
  \date 12 dec 2008

  \brief Implementation of combinatorics functions
 */

#include "combinatorics.hpp"

#include <iostream>

namespace xlifepp
{

//! ompute binomial coefficient C_n^k = n! / ( k! * (n-k)! )
number_t binomialCoefficient(int n, int k)
{
  number_t cnk(1);
  int mn = std::min(k, n - k);
  if (mn < 0) {cnk = 0;}
  else if (mn > 0)
  {
    int mx = std::max(k, n - k);
    cnk = mx + 1;
    for (int i = 2; i <= mn; i++ ) {cnk = (cnk * (mx + i)) / i;}
  }
  return cnk;
}

/*!
  compute n-th row in Pascal's triangle of binomial coefficients

-      k = 0  1  2  3  4  5
- n = 1 |  1  1
- n = 2 |  1  2  1              C^n_k = C^{n-1}_{k-1} + C^{n-1}_k , 0 <= k <= n
- n = 3 |  1  3  3  1
- n = 4 |  1  4  6  4  1
- n = 5 |  1  5 10  10  5  1
- n = ......................
--------------------------------------------------------------------------------
 */
void binomialCoefficients(std::vector<number_t>& row)
{
  // compute n-th row of Pascal binomial coefficients
  // where n = row.size()-1
  if (row.size() > 1)
  {
    // Lo and behold ! we use both forward iterators and reverse iterators
    // --------------- to compute 'in place' coefficients
    // start on first row 'i' = 1 (row.begin())
    std::vector<number_t>::iterator it_r(row.begin()), it_r2;
    // and on second column on row (row.rend()-1)
    std::vector<number_t>::reverse_iterator rit_r(row.rend() - 1), rit_r2(rit_r), rit_r3;
    // first column in triangle is set to 1
    *it_r = 1;
    // forward iteration on rows i = 2 ... n
    for (it_r2 = row.begin() + 1; it_r2 != row.end(); it_r2++)
    {
      // initialize last coefficient on row, row is thus 1 .... 1
      *it_r2 = *it_r;
      // reverse iteration from next-to-last (row.rbegin() + n-'i-1') coefficient
      //                   down to second (row.rend()+1) coefficient on row
      // for each column j compute C^i_j = C^{i-1}_j + C^{i-1}_{j-1}, i.e. *j << *j + *(j+1)
      for (rit_r3 = rit_r2--; rit_r3 != rit_r; rit_r3++) {*rit_r3 += *(rit_r3 + 1);}
    }
  }
}

//! ompute n-th row in Pascal's triangle of binomial coefficients scaled by 1/(n-1)!
void binomialCoefficientsScaled(std::vector<real_t>& row)
{
  // compute n-th row of Pascal binomial coefficients scaled by 1/(n-1)!
  // where n = row.size()-1
  if ( row.size() > 1 )
  {
    std::vector<real_t>::iterator it_r(row.begin()), it_r2, it_rl;
    std::vector<real_t>::reverse_iterator rit_r(row.rend() - 1), rit_r2(rit_r - 1), rit_r3;
    *it_r = 1.;
    if (row.size() > 1) {*(it_r + 1) = *it_r;}
    int n(1);
    for (it_r2 = row.begin() + 2; it_r2 != row.end(); it_r2++, n++)
    {
      *it_r2 = *it_r;
      for (rit_r3 = rit_r2--; rit_r3 != rit_r; rit_r3++) {*rit_r3 += *(rit_r3 + 1);}
      // scaled n-th row by 1/(n-1)!!
      for (it_rl = it_r; it_rl != it_r2 + 1; it_rl++) {*it_rl /= n;}
    }
  }
}

} // end of namespace xlifepp

