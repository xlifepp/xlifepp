/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file spline.cpp
  \author E. Lunéville
  \since 20 aug 2019
  \date  20 aug 2019

  \brief Implementation of spline classes and functionalities
*/

#include "spline.hpp"
#include "../geometry/Geometry.hpp"
#include "../geometry/Parametrization.hpp"

namespace xlifepp
{
//=====================================================================================
//             Spline functions
//=====================================================================================
//!copy constructor (deep copy)
Spline::Spline(const Spline& sp)
: controlPoints_(sp.controlPoints_), knots_(sp.knots_), degree_(sp.degree_), isClosed_(sp.isClosed_),
  type_(sp.type_), subtype_(sp.subtype_), bcs_(sp.bcs_), bce_(sp.bce_), splinePar_(sp.splinePar_), yps_(sp.yps_), ype_(sp.ype_)
{
    parametrization_=nullptr;
    if(sp.parametrization_!=nullptr) parametrization_=new Parametrization(*sp.parametrization_); //deep copy
}
/*!
  check boundary condition bcs_, bcse with the following rules
     bcs_= undef -> bcs_=defBC
     bcs_!= undef & bce_= undef -> bce_=bcs_
     bcs_= periodic | bce_= periodic -> bce_=bcs_=periodic
*/
void Spline::checkBC(SplineBC defBC)
{
    if(bcs_==_undefBC)
        {
            bcs_=defBC;
            if(bce_==_undefBC) bce_=defBC;
        }
    else if (bce_==_undefBC) bce_=bcs_;
    if(bcs_==_periodicBC || bce_==_periodicBC) {bcs_=bce_=_periodicBC;}
}

RealPair Spline::knotsBounds()const
{
    if(knots_.size()==0) return RealPair(0.,0.); // no knots !
    return RealPair(knots_.begin()->first,knots_.rbegin()->first);
}

//! return parameter bounds same as knot bounds!
RealPair Spline::parameterBounds()const
{
//    if(parametrization_==nullptr) error("free_error","undefined spline parametrization");
//    return parametrization_->bounds(_x);
      return knotsBounds();
}

std::vector<Point> Spline::boundNodes() const
{
  std::vector<Point> res(2);
  res[0]=evaluate(0.);
  res[1]=evaluate(1.);
  return res;
}

std::multimap<real_t,number_t>::const_iterator Spline::locate(real_t t) const
{
  if(t < knots_.begin()->first-theTolerance || t > knots_.rbegin()->first+theTolerance)
  {
    theCout<<"delta="<<t - knots_.rbegin()->first-theTolerance<<eol;
    warning("free_warning", "parameter "+tostring(t)+" is outside the interval definition of the "+words("spline type",type_)+" ["
            +tostring(knots_.begin()->first)+","+tostring(knots_.rbegin()->first)+"]");
    if(t<knots_.begin()->first)  return knots_.begin();
    else return (--(--knots_.end()));
  }
  std::multimap<real_t,number_t>::const_iterator itm=knots_.lower_bound(t);
  if(itm==knots_.end()) {itm--;itm--;}
  else
  {
    if(itm->first > t) itm--;
    if(itm==(--knots_.end())) itm--;
  }
  return itm;
}

Vector<Point> Spline::evaluate(const std::vector<real_t>& ts, DiffOpType dif) const
{
  Vector<Point> Qs(ts.size());
  std::vector<real_t>::const_iterator its=ts.begin();
  Vector<Point>::iterator itq= Qs.begin();
  for(; its!=ts.end(); ++its,++itq) *itq = evaluate(*its,dif);
  return Qs;
}

//! print utility
void Spline::print(std::ostream& out, bool nocpt) const
{
  if(theVerboseLevel==0) return;
  out<<words("spline type",type_);
  if(subtype_!=_noSplineSubtype) out<<"_"<<words("spline subtype",subtype_);
  out<<", degree "<<degree_<<", "<<words("spline parametrization",splinePar_)<<", "<<words("spline BC",bcs_);
  if(bcs_!=_periodicBC && bce_!=bcs_) out<<" & "<< words("spline BC",bce_);
  out<<eol;
  if(theVerboseLevel<5) return;
  if (!nocpt)
  {
    out<<"   "<<words("control points")<<": ";
    if (isTestMode) out<<roundToZero(controlPoints_)<<eol;
    else out<<controlPoints_<<eol;
  }
  out<<"   "<<words("knots")<<": "<<knots_<<eol;
  out<<"   parameter bounds: "<<parameterBounds()<<eol;
}

//special function declared in Parameters.hpp
void Parameter::setToSpline(const Spline& par)
{
  if (p_!=nullptr) deletePointer();
  Spline* np= par.clone();
  p_ = static_cast<void*>(np);
  type_=_pointerSpline;
}

void deleteSpline(void *p)
{
  if (p!=nullptr) delete reinterpret_cast<Spline*>(p);
}

void* cloneSpline(const void* p)
{
  return reinterpret_cast<void*>((reinterpret_cast<const Spline*>(p))->clone());
}

//=====================================================================================
//             C2Spline functions
//=====================================================================================
//general constructor
C2Spline::C2Spline(const std::vector<Point>& cpts, SplineParametrization sp, SplineBC bcs, SplineBC bce,
                   const std::vector<real_t>& yps, const std::vector<real_t>& ype)
{
  degree_=3;
  type_=_C2Spline;
  subtype_=_noSplineSubtype;
  bcs_=bcs;
  bce_=bce;
  yps_=yps;
  ype_=ype;
  controlPoints_=cpts;
  if(sp==_undefParametrization) splinePar_=_xParametrization;
  else splinePar_=sp;
  std::vector<Point>::iterator it=controlPoints_.begin();
  init();
}

C2Spline::C2Spline(real_t a, real_t b, const std::vector<real_t>& ys, SplineParametrization sp, SplineBC bcs, SplineBC bce,
                   const std::vector<real_t>& yps, const std::vector<real_t>& ype)
{
  number_t n=ys.size()-1;
  real_t dx=(b-a)/n;
  controlPoints_.resize(n+1);
  real_t x=a;
  std::vector<Point>::iterator it=controlPoints_.begin();
  std::vector<real_t>::const_iterator ity=ys.begin();
  for(; it!=controlPoints_.end(); ++it, ++ity, x+=dx)
    *it=Point(x,*ity);
  degree_=3;
  type_=_C2Spline;
  subtype_=_noSplineSubtype;
  bcs_=bcs; bce_=bce;
  checkBC(_clampedBC);
  yps_=yps;
  ype_=ype;
  if(sp==_undefParametrization) splinePar_=_xParametrization;
  else splinePar_=sp;
  init();
}

//! copy constructor
C2Spline::C2Spline(const C2Spline& c2s)
{
    copy(c2s);
}

//! copy C2Spline data in a cleaned C2Spline (re-allocate parametrization_ pointer)
void C2Spline::copy(const C2Spline& c2s)
{
    controlPoints_=c2s.controlPoints_;
    knots_=c2s.knots_;
    degree_=c2s.degree_;;
    isClosed_=c2s.isClosed_;
    type_=c2s.type_;
    subtype_=c2s.subtype_;
    bcs_=c2s.bcs_;
    bce_=c2s.bce_;
    splinePar_=c2s.splinePar_;
    yps_=c2s.yps_;
    ype_=c2s.ype_;
    P_=c2s.P_;
    //instead copy of parametrization, re-create a new parametrization of C2Spline
    Parameters pars(reinterpret_cast<const void *>(this),"spline");
//    real_t a=knots_.begin()->first, b=knots_.rbegin()->first;
//    parametrization_ = new Parametrization(a, b, parametrization_C2Spline, pars,"C2Spline parametrization",dimen_t(controlPoints_[0].size()));
    parametrization_ = new Parametrization(0., 1., parametrization_C2Spline, pars,"C2Spline parametrization",dimen_t(controlPoints_[0].size()));
    parametrization_->setinvParametrization(invParametrization_C2Spline);
}

//! assign C2Spline to current one
C2Spline& C2Spline::operator=(const C2Spline& c2s)
{
    delete parametrization_;
    copy(c2s);
    return *this;
}

//! init C2Spline from main data ( build knots_ and p_)
void C2Spline::init()
{
  number_t n=controlPoints_.size()-1;
  real_t t=0;
  switch(splinePar_)
  {
    case _xParametrization:
      for(number_t i=0; i<=n; i++) knots_.insert(std::make_pair(controlPoints_[i][0],i));
      break;
    case _uniformParametrization:
      for(number_t i=0; i<=n; i++) knots_.insert(std::make_pair(real_t(i),i));
      break;
    case _chordalParametrization:
      knots_.insert(std::make_pair(t,0));
      for(number_t i=1; i<=n; i++)
      {
        t+=dist(controlPoints_[i-1],controlPoints_[i]);
        knots_.insert(std::make_pair(t,i));
      }
      break;
    case _centripetalParametrization:
      knots_.insert(std::make_pair(t,0));
      for(number_t i=1; i<=n; i++)
      {
        t+=std::sqrt(dist(controlPoints_[i-1],controlPoints_[i]));
        knots_.insert(std::make_pair(t,i));
      }
      break;
    default:  error("free_error"," unknown spline parametrization in C2Spline::init");
  }

  real_t dse = dist(controlPoints_[n],controlPoints_[0]);
  isClosed_= dse < theTolerance;

  //check periodicity
  bool isPer=false;
  if(splinePar_==_xParametrization) isPer = std::abs(controlPoints_[n][1]-controlPoints_[0][1])<theTolerance;
  else isPer = isClosed_;
  if(bcs_==_periodicBC && !isPer)
     warning("free_warning","C2 spline may be hazardous because start and end points are not consistant with a periodic condition");

  std::multimap<real_t,number_t>::iterator itm=knots_.begin(), itmp=itm;
  itmp++;
  Vector<real_t> h(n);
  for(number_t i=1; i<=n; i++, ++itm, ++itmp) h(i)=itmp->first-itm->first;
  Matrix<real_t> S(n+1,n+1);
  number_t d=1, s=1;
  if(splinePar_!=_xParametrization) {d=dim();s=0;}
  Vector<real_t> F(d*(n+1));
  //build spline system
  for(number_t i=2; i<=n; i++)
  {
    real_t h1=h(i-1), h2=h(i);
    S(i,i-1)=h2;
    S(i,i+1)=h1;
    S(i,i)=2*(h1+h2);
    for(number_t k=0; k<d; k++)
      F(k*(n+1)+i)=3*(h1*h1*(controlPoints_[i][k+s]-controlPoints_[i-1][k+s])+h2*h2*(controlPoints_[i-1][k+s]-controlPoints_[i-2][k+s]))/(h1*h2);
  }
  //take into account boundary conditions
  switch(bcs_)
  {
    case _undefBC:
    case _naturalBC:
      S(1,1)=2*h(1);
      S(1,2)=h(1);
      for(number_t k=0; k<d; k++) F(k*(n+1)+1)=3*(controlPoints_[1][k+s]-controlPoints_[0][k+s]);
      break;
    case _clampedBC:
      S(1,1)=1;
      for(number_t k=0; k<d; k++) F(k*(n+1)+1)=yps_[k];
      break;
    case _periodicBC:
      S(1,1)=2*h(n);
      S(1,2)=h(n);
      S(1,n)=h(1);
      S(1,n+1)=2*h(1);
      S(n+1,1)=1;
      S(n+1,n+1)=-1;
      for(number_t k=0; k<d; k++)
        F(k*(n+1)+1)=3*h(n)*(controlPoints_[1][k+s]-controlPoints_[0][k+s])/h(1)+ 3*h(1)*(controlPoints_[n][k+s]-controlPoints_[n-1][k+s])/h(n);
      break;
    default:error("free_error","unknown or illegal spline boundary condition");
  }
  if(bce_==_naturalBC || bce_==_undefBC)
  {
    S(n+1,n+1)=2*h(n);
    S(n+1,n)=h(n);
    for(number_t k=0; k<d; k++) F(k*(n+1)+n+1)=3*(controlPoints_[n][k+s]-controlPoints_[n-1][k+s]);
  }
  else if(bce_==_clampedBC)
  {
    S(n+1,n+1)=1;
    for(number_t k=0; k<d; k++) F(k*(n+1)+n+1)=ype_[k+s];
  }

  // solve linear system using Gauss for dense matrix (to be improved in future) and build ploynomials
  real_t minPivot=theTolerance;
  number_t row=0;
  if(!gaussMultipleSolver(S,F,d,minPivot,row)) error("free_error", "Gauss solver fails in C2Spline::init");
  P_.resize(n,Vector<real_t>(4*d));
  for(number_t i=0; i<n; i++)
  {
    Vector<real_t>& p=P_[i];
    for(number_t k=0; k<d; k++)
    {
      number_t kn=k*(n+1), k4=4*k;
      real_t a=1./h[i], a2=a*a, dy=controlPoints_[i+1][k+s]-controlPoints_[i][k+s];
      p[k4]  = controlPoints_[i][k+s];
      p[k4+1]= F[kn+i];
      p[k4+2]= 3*a2*dy-2*a*F[kn+i]-a*F[kn+i+1];
      p[k4+3]= a2*(F[kn+i]+F[kn+i+1])-2*a2*a*dy;
    }
  }

  // associate parametrization
  Parameters pars(reinterpret_cast<const void *>(this),"spline");
//  real_t a=knots_.begin()->first, b=knots_.rbegin()->first;
//  parametrization_ = new Parametrization(a, b, parametrization_C2Spline, pars,"C2Spline parametrization",dimen_t(controlPoints_[0].size()));
  parametrization_ = new Parametrization(0., 1., parametrization_C2Spline, pars,"C2Spline parametrization",dimen_t(controlPoints_[0].size()));
  parametrization_->setinvParametrization(invParametrization_C2Spline);
}

//! C2Spline destructor
C2Spline::~C2Spline()
{
    if(parametrization_!=nullptr) delete parametrization_;
}

Point C2Spline::evaluate(real_t x, DiffOpType dif) const
{
  // map x in [0,1] to knots parameter
  real_t a=knots_.begin()->first, b=knots_.rbegin()->first;
  real_t l=b-a;
  x=a+x*l;

  // locate interval
  std::multimap<real_t,number_t>::const_iterator itm=locate(x);
  number_t i=itm->second;
  real_t t=x-itm->first;

  // compute spline
  number_t d=1;
  if(splinePar_ != _xParametrization) d=dim();
  Point Q; Q.resize(d);
  const Vector<real_t>& Pi=P_[i];
  switch(dif)
  {
    case _id:
      for(number_t k=0; k<d; k++)
      {
        number_t k4=4*k;
        Q[k]=Pi[k4]+Pi[k4+1]*t+Pi[k4+2]*t*t+Pi[k4+3]*t*t*t;
      }
      break;
    case _dx:
      for(number_t k=0; k<d; k++)
      {
        number_t k4=4*k;
        Q[k]=l*(Pi[k4+1]+2*Pi[k4+2]*t+3*Pi[k4+3]*t*t);
      }
      break;
    case _dxx:
      for(number_t k=0; k<d; k++)
      {
        number_t k4=4*k;
        Q[k]=(l*l)*(2*Pi[k4+2]*t+6*Pi[k4+3]*t);
      }
      break;
    case _dxxx:
      for(number_t k=0; k<d; k++)
        Q[k]=l*l*l*6*Pi[4*k+3];
      break;
    default:
      error("free_error", "differential operator not handled in C2Spline::evaluate");
  }
  return Q;
}

std::vector<Point> C2Spline::boundNodes() const
{
  std::vector<Point> res(2);
  res[0]=controlPoints_[0];
  res[1]=controlPoints_[controlPoints_.size()-1];
  return res;
}

//! print utility
void C2Spline::print(std::ostream& out) const
{
  if(theVerboseLevel==0) return;
  Spline::print(out);
  if(theVerboseLevel<5)  return;
  out<<"    polynomial coefs: "<<P_<<eol;
}

//! C2 spline parametrization s(t)
Vector<real_t> C2Spline::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  Point Q = this->evaluate(pt[0],d);
  if(splinePar_==_xParametrization)
  {
      Vector<real_t> R(2,pt[0]); R[1]=Q[0];
      return R;
  }
  return Vector<real_t>(Q.begin(),Q.end());
}

Vector<real_t> C2Spline::invParametrization(const Point& pt, Parameters& pars, DiffOpType d) const //!< inverse of parametrization
{
  Point Q = evaluate(pt[0],_id);
  if(dist(pt,Q)>theTolerance) error("free_error","point seems no to be on C2Spline curve");
  return Vector<real_t>(1,pt[0]);
}

//============================================================================================
//      CatmullRomSpline functions
//============================================================================================
CatmullRomSpline::CatmullRomSpline(const std::vector<Point>& cpts, SplineParametrization sp, real_t tau,
                                   SplineBC bcs, SplineBC bce, const std::vector<real_t>& yps, const std::vector<real_t>& ype)
{
    if(cpts.size()<2) error("free_error","give at least 2 points for buiding Catmull-Rom spline");
    if(tau<0) error("free_error","When buiding Catmull-Rom spline, tension factor tau must be greater than 0");
    controlPoints_=cpts;
    tau_=tau;
    if(tau_==theRealMax) tau_=0.5;
    type_=_CatmullRomSpline;
    subtype_=_noSplineSubtype;
    degree_=3;
    if(sp==_undefParametrization) splinePar_=_chordalParametrization;
    else splinePar_=sp;
    bcs_=bcs;bce_=bce;
    checkBC(_naturalBC);
    yps_=yps; ype_=ype;
    number_t d=controlPoints_[0].size();
    yps_.resize(d);ype_.resize(d);
    init();
}

//! copy constructor
CatmullRomSpline::CatmullRomSpline(const CatmullRomSpline& crs)
{
    copy(crs);
}

//! copy CatmullRomSpline data in a cleaned CatmullRomSpline (re-allocate parametrization_ pointer)
void CatmullRomSpline::copy(const CatmullRomSpline& crs)
{
    controlPoints_=crs.controlPoints_;
    knots_=crs.knots_;
    degree_=crs.degree_;;
    isClosed_=crs.isClosed_;
    type_=crs.type_;
    subtype_=crs.subtype_;
    bcs_=crs.bcs_;
    bce_=crs.bce_;
    splinePar_=crs.splinePar_;
    yps_=crs.yps_;
    ype_=crs.ype_;
    tau_=crs.tau_;
    //instead copy of parametrization, re-create a new parametrization of CatmullRomSpline
    Parameters pars(reinterpret_cast<const void *>(this),"spline");
//    real_t a=knots_.begin()->first, b=knots_.rbegin()->first;
//    parametrization_ = new Parametrization(a, b, parametrization_CatmullRomSpline, pars,"CatmullRomSpline parametrization",dimen_t(controlPoints_[0].size()));
    parametrization_ = new Parametrization(0., 1., parametrization_CatmullRomSpline, pars,"CatmullRomSpline parametrization",dimen_t(controlPoints_[0].size()));
    parametrization_->setinvParametrization(invParametrization_CatmullRomSpline);
}

//! assign CatmullRomSpline to current one
CatmullRomSpline& CatmullRomSpline::operator=(const CatmullRomSpline& crs)
{
    delete parametrization_;
    copy(crs);
    return *this;
}

void CatmullRomSpline::init()
{
  number_t m = controlPoints_.size()-1;  //shoud be greater than 1
  isClosed_=dist(controlPoints_[m],controlPoints_[0])<theTolerance;
  if(isClosed_) // add controlPoints to build "periodic" spline
    {
        controlPoints_[m]=controlPoints_[0];
        Point P0 = controlPoints_[1], Pm = controlPoints_[m-1];
        controlPoints_.resize(m+3);
        for(number_t k=m+1;k!=0;k--) controlPoints_[k]=controlPoints_[k-1];
        controlPoints_[0]=Pm; controlPoints_[m+2]=P0;
        bcs_=_periodicBC;bce_=_periodicBC;
    }
    if(bcs_==_clampedBC) // add at the beginning a point to enforce derivative yps_
    {
        Point P0 = controlPoints_[0]-tau_*Point(yps_);
        controlPoints_.insert(controlPoints_.begin(),P0);
    }
    else if(bcs_==_naturalBC) // add a symmetric point at the beginning
    {
        Point P0 = 2*controlPoints_[0]-controlPoints_[1];
        controlPoints_.insert(controlPoints_.begin(),P0);
    }
    if(bce_==_clampedBC) // add at the end a point to enforce derivative ype_
    {
        Point Pm = controlPoints_[m+1]+tau_*Point(ype_);
        controlPoints_.push_back(Pm);
    }
    else if(bce_==_naturalBC) // add a symmetric point at the end
    {
        Point Pm = 2*controlPoints_[m+1]-controlPoints_[m];
        controlPoints_.push_back(Pm);
    }

    m = controlPoints_.size();
    real_t t=0;
    knots_.insert(std::make_pair(t,1));
    switch(splinePar_)
    {
      case _xParametrization:
       for(number_t i=2; i<=m-2; i++)
         {
          t+=controlPoints_[i][0];
          knots_.insert(std::make_pair(t,i));
         }
         break;
      case _uniformParametrization:
        for(number_t i=2; i<=m-2; i++)
         {
          t+=1;
          knots_.insert(std::make_pair(t,i));
         }
         break;
      default: // chordal parametrization
        for(number_t i=2; i<=m-2; i++)
          {
            t+=std::pow(dist(controlPoints_[i],controlPoints_[i-1]),tau_);
            knots_.insert(std::make_pair(t,i));
          }
    }

  Parameters pars(reinterpret_cast<const void *>(this),"spline");
//  real_t a=knots_.begin()->first, b=knots_.rbegin()->first;
//  parametrization_ = new Parametrization(a, b, parametrization_CatmullRomSpline, pars,"CatmullRomSpline parametrization",dimen_t(controlPoints_[0].size()));
  parametrization_ = new Parametrization(0., 1., parametrization_CatmullRomSpline, pars,"CatmullRomSpline parametrization",dimen_t(controlPoints_[0].size()));
  parametrization_->setinvParametrization(invParametrization_CatmullRomSpline);
}

CatmullRomSpline::~CatmullRomSpline()
{
    if(parametrization_!=nullptr) delete parametrization_;
}

Point CatmullRomSpline::evaluate(real_t x, DiffOpType dif) const
{
  // map x in [0,1] to knots parameter
  real_t a=knots_.begin()->first, b=knots_.rbegin()->first;
  real_t l=b-a;
  x=a+x*l;
  x=std::max(a,std::min(x,b));
  // locate interval
  std::multimap<real_t,number_t>::const_iterator it=locate(x);
  number_t i=it->second;
  real_t ti=it->first;
  it++;
  real_t tip=(it++)->first, t=(x-ti)/(tip-ti), t2=t*t;
  // compute Catmull-Rom spline
  number_t d=dim();
  Point Q;Q.resize(d,0.);
  real_t a1,a2,a3,a4;
  switch(dif)
  {
    case _id: a1=tau_*t*(2*t-t2-1); a2=1+t2*((tau_-3)+(2-tau_)*t);
               a3=t*(tau_+(3-2*tau_)*t+(tau_-2)*t2); a4=tau_*t2*(t-1);
               l=1.;
               break;
    case _dx:
    case _dt: a1=tau_*(4*t-3*t2-1); a2=t*(2*(tau_-3)+3*t*(2-tau_));
               a3=(tau_+2*(3-2*tau_)*t+3*(tau_-2)*t2); a4=tau_*t*(3*t-2);
               break;
    case _dxx:
    case _dt2: a1=tau_*(4-6*t); a2=2*(tau_-3)+6*t*(2-tau_);          // discontinuous
               a3=2*(3-2*tau_)+3*(tau_-2)*t; a4=tau_*(6*t-2);
               l=l*l;
               break;
    case _dxxx:
    case _dt3: a1=-6*tau_; a2=6*(2-tau_); a3=6*(tau_-2); a4=6*tau_; // discontinuous
                l=l*l*l;
                break;
    default:
      error("free_error", "differential operator not handled in CatmullRomSpline::evaluate");
  }
  std::vector<real_t>::iterator itq=Q.begin();
  Vector<real_t>::const_iterator  it1=controlPoints_[i-1].begin(), it2=controlPoints_[i].begin(),
                                  it3=controlPoints_[i+1].begin(), it4=controlPoints_[i+2].begin();
  for(;itq!=Q.end();++itq,++it1,++it2,++it3,++it4) *itq=l*(a1**it1+a2**it2+a3**it3+a4**it4);
  return Q;
}

std::vector<Point> CatmullRomSpline::boundNodes() const
{
  std::vector<Point> res(2);
  res[0]=controlPoints_[1];
  res[1]=controlPoints_[controlPoints_.size()-2];
  return res;
}

//! print utility
void CatmullRomSpline::print(std::ostream& out) const
{
  if(theVerboseLevel==0) return;
  Spline::print(out);
  if(theVerboseLevel<5)  return;
  out<<"    tau = "<<tau_;
  if(tau_==0.5) out<<" (centripetal)";
  if(tau_==0.) out<<" (standard)";
  if(tau_==1.) out<<" (chordal)";
  out<<eol;
}

Vector<real_t> CatmullRomSpline::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const //!< parametrization s(t)
{
  Point Q = evaluate(pt[0],d);
  return Vector<real_t>(Q.begin(),Q.end());
}

Vector<real_t> CatmullRomSpline::invParametrization(const Point& pt, Parameters& pars, DiffOpType d) const //!< inverse of parametrization
{
  error("free_error","inv parametrization of CatmullRomSpline is not available");
  return Vector<real_t>(1,pt[0]);
}

//============================================================================================
//      BSpline functions
//============================================================================================
BSpline::BSpline(const std::vector<Point>& cpts, number_t degree,
                 SplineBC bcs, SplineBC bce, const std::vector<real_t>& weights)
{
    t0_=0;
    tf_=0;
    if(cpts.size()<degree+1)
        error("free_error","give at least "+tostring(degree+1)+" points for buiding B-spline of degree "+tostring(degree));
    controlPoints_=cpts;
    weights_=weights;
    type_=_BSpline;
    subtype_=_SplineApproximation;
    degree_=degree;
    splinePar_=_uniformParametrization;
    bcs_=bcs;bce_=bce;
    checkBC(_clampedBC);
    number_t d=controlPoints_[0].size();
    yps_.resize(d);ype_.resize(d);
    init();
}

//!< approximation or interpolation B-spline
BSpline::BSpline(SplineSubtype sbt, const std::vector<Point>& cpts, number_t degree, SplineParametrization spar,
                 SplineBC bcs, SplineBC bce,  const std::vector<real_t>& yps, const std::vector<real_t>& ype, const std::vector<real_t>& weights)
{
    t0_=0;
    tf_=0;
    if(cpts.size()<degree+1)
        error("free_error","give at least "+tostring(degree+1)+" points for buiding B-spline of degree "+tostring(degree));
    weights_=weights;
    type_=_BSpline;
    subtype_=_SplineInterpolation;
    degree_=degree;
    splinePar_=spar;
    if(splinePar_==_undefParametrization) splinePar_=_uniformParametrization;
    bcs_=bcs;bce_=bce;
    checkBC(_clampedBC);
    controlPoints_=cpts;
    number_t d=controlPoints_[0].size();
    yps_=yps;ype_=ype;
    yps_.resize(d);ype_.resize(d);
    subtype_=sbt;
    if(sbt==_SplineInterpolation) // construct control points (Ci)i=0,n in order to interpolate some given points (Pj)j=0,n
    {
        interpolationPoints_=controlPoints_;
//      add virtual points to impose derivatives at ends
        if(bcs_==_clampedBC)
          {
              controlPoints_.insert(controlPoints_.begin(), controlPoints_[0]);
              if(weights_.size()>0) weights_.insert(weights_.begin(),weights_[0]);
          }
        if(bce_==_clampedBC)
        {
            controlPoints_.push_back(*controlPoints_.rbegin());
            if(weights_.size()>0) weights_.push_back(*weights_.rbegin());
        }
        init();
        initInterp();
    }
    else init();
}

//! copy constructor
BSpline::BSpline(const BSpline& bs)
{
    copy(bs);
}

//! copy BSpline data in a cleaned BSpline (re-allocate parametrization_ pointer)
void BSpline::copy(const BSpline& bs)
{
    controlPoints_=bs.controlPoints_;
    knots_=bs.knots_;
    degree_=bs.degree_;;
    isClosed_=bs.isClosed_;
    type_=bs.type_;
    subtype_=bs.subtype_;
    bcs_=bs.bcs_;
    bce_=bs.bce_;
    splinePar_=bs.splinePar_;
    yps_=bs.yps_;
    ype_=bs.ype_;
    interpolationPoints_=bs.interpolationPoints_;
    weights_=bs.weights_;
    noWeight_=bs.noWeight_;
    t0_=bs.t0_;
    tf_=bs.tf_;
    paramMap_= bs.paramMap_;
    //instead copy of parametrization, re-create a new parametrization of BSpline
    Parameters pars(reinterpret_cast<const void *>(this),"spline");
//    parametrization_ = new Parametrization(t0_, tf_, parametrization_BSpline, pars,"BSpline parametrization",dimen_t(controlPoints_[0].size()));
    parametrization_ = new Parametrization(0., 1., parametrization_BSpline, pars,"BSpline parametrization",dimen_t(controlPoints_[0].size()));
    parametrization_->setinvParametrization(invParametrization_BSpline);
}

//! assign BSpline to current one
BSpline& BSpline::operator=(const BSpline& bs)
{
    delete parametrization_;
    copy(bs);
    return *this;
}

void BSpline::init()
{
    number_t m = controlPoints_.size()-1;  //shoud be greater than 1
    if(weights_.size()==0)
    {
        noWeight_=true;
        if(noWeight_) weights_ = std::vector<real_t>(m+1,1.);
    }
    else
    {
        if(weights_.size()!=controlPoints_.size())
           error("free_error","weights size must be equal to the number of control points");
        else noWeight_=false;
    }
    //build knots
    isClosed_=dist(controlPoints_[m],controlPoints_[0])<theTolerance;
    if(!isClosed_ && bcs_==_periodicBC) //enforce closed data
    {
        controlPoints_.push_back(controlPoints_[0]);
        weights_.push_back(weights_[0]);
        m+=1;
        isClosed_=true;
    }
    if(bcs_==_periodicBC)  // add the first degree_ points to the end of points
    {
        bcs_=_periodicBC; bce_=_periodicBC;
        controlPoints_[m]=controlPoints_[0];  //force equality
        std::vector<Point>::iterator itp=controlPoints_.begin()+1;
        controlPoints_.insert(controlPoints_.end(),itp,itp+degree_);
        m=controlPoints_.size();
        for(number_t i=0;i<m+degree_+1;i++) knots_.insert(std::make_pair(i,i));
        t0_=degree_; tf_=m;
        weights_.resize(m,1.);
        number_t kp=m-degree_-1;
        for(number_t k=1;k<degree_;k++) weights_[kp+k]=weights_[k];
    }
    else
    {
        number_t n=controlPoints_.size();
        number_t k=0,kc=0;
        if(bcs_==_clampedBC)
        {
            for(;k<degree_;k++) knots_.insert(std::make_pair(real_t(kc),k));
            t0_=0;
        }
        else
        {
            for(;k<degree_;k++,kc++) knots_.insert(std::make_pair(real_t(kc),k));
            t0_=k;
        }
        for(number_t i=0;i<=n-degree_;i++,k++,kc++) knots_.insert(std::make_pair(real_t(kc),k));
        if(bce_==_clampedBC)
        {
            for(number_t i=0;i<degree_;i++,k++) knots_.insert(std::make_pair(real_t(kc-1),k));
            tf_=kc-1;
        }
        else
        {
            for(number_t i=0;i<degree_;i++,k++,kc++) knots_.insert(std::make_pair(real_t(kc),k));
            tf_=m+1;
        }
    }

  Parameters pars(reinterpret_cast<const void *>(this),"spline");
//  parametrization_ = new Parametrization(t0_, tf_, parametrization_BSpline, pars,"BSpline parametrization",dimen_t(controlPoints_[0].size()));
  parametrization_ = new Parametrization(0., 1., parametrization_BSpline, pars,"BSpline parametrization",dimen_t(controlPoints_[0].size()));
  parametrization_->setinvParametrization(invParametrization_BSpline);
}

/*! initialization when interpolation is selected
    cpts (in)  : list of interpolated points Q_i
    cpts (out) : list of control points P_j leading to B-spline interpolating Q_i
    this function solve the linear system
         sum_j=0,m w_j*B_jk(s_i)P_j = sum_j=0,m w_j*B_jk(s_i) Q_i    0 <= i,j <=m
    <=>  M*P=D*Q  with M_ij = w_j*B_jk(s_i)  D_ii = sum_j=0,m w_j*B_jk(s_i) =sum_j=0,m M_ij
    when B-spline is clamped at ends, first (resp. last) two rows of system have to be modified
        P0 = Q0                                  k*w_m/w_m-1(t_m+1-t_m)*(Pm-Pm-1) = ype
        k*w_1/w_0(t_k+1-t_k)*(P1-P0) = yps       Pm=Qm
    s_i is given by t(r_i)=t0+r_i*(tf-t0) where r_i are chosen using one of the rules
        uniform: r_i=i/m
        chordal: r_i=r_i-1 + |Q_i-Q_i-1|/ sum_j=1,m |Q_j-Q_i-j|  , r_0=0
        centripetal: r_i=r_i-1 + sqrt(|Q_i-Q_i-1|)/ sum_j=1,m sqrt(|Q_j-Q_i-j|)  , r_0=0
        ONLY UNIFORM IS AVAILABLE

*/
void BSpline::initInterp()
{
    std::vector<Point>  Q = controlPoints_;   // copy controlPoints to interpolated points
    std::vector<Point>& P = controlPoints_;   // new control points
    number_t m = Q.size()-1;  //shoud be greater than 1

    //create paramMap_ if non uniform parametrization
    if(splinePar_==_chordalParametrization || splinePar_==_centripetalParametrization)
    {
            std::vector<Point>& P=interpolationPoints_;
            number_t n=P.size();
            std::vector<real_t> ds(n,0.);
            real_t s=0.;
            if(splinePar_==_chordalParametrization)
               for(number_t i=1;i<n;i++) ds[i]= (s+=dist(P[i],P[i-1]));
            else
               for(number_t i=1;i<n;i++) ds[i]= (s+=std::sqrt(dist(P[i],P[i-1])));
            for(number_t i=0;i<n;i++) paramMap_.insert(std::make_pair(ds[i]/s,i));
    }

    //build matrix M (up to now as dense matrix, to be improved in future)
    Matrix<real_t> B(m+1,m+1);
    Vector<real_t> D;
    if(!noWeight_) D.resize(m+1,0.);
    Vector<real_t> bik, dbik, d2bik;
    number_t ij;
    bool useParMap=(splinePar_==_chordalParametrization || splinePar_==_centripetalParametrization) && paramMap_.size()>0;
    std::map<real_t,number_t>::iterator itm;
    if(useParMap) itm=paramMap_.begin();
    for(number_t i=0;i<=m;i++)
    {
        real_t ri=real_t(i)/m;       // uniform
        if(useParMap) {ri=itm->first; itm++;}
        real_t si = t0_+ri*(tf_-t0_); // map to [t0,tf]
        computeB(si,_id,ij,bik,dbik,d2bik);
        Vector<real_t>::iterator itb=bik.begin();
        if(noWeight_)  // B-spline
        {
          for(number_t k=0;k<=degree_; k++, ++itb)
             B(i+1,ij-degree_+k+1) = *itb;
        }
        else //rational B-Spline
        {
         std::vector<real_t>::const_iterator itw=weights_.begin()+(ij-degree_);
         real_t w=0., wb;
         for(number_t k=0;k<=degree_; k++, ++itb, ++itw)
           {
             wb=*itw * *itb;
             B(i+1,ij-degree_+k+1) = wb;
             D[i]+=wb;
           }
         }
    }

    // modify system if clamped condition
    if(bcs_==_clampedBC)
    {
        for(number_t j=0;j<=m;j++) {B(1,j+1)=0;B(2,j+1)=0.;}
        real_t a=1.;
        if(!noWeight_) a=weights_[1]/weights_[0];
        std::multimap<real_t,number_t>::iterator itn=knots_.begin();
        for(number_t k=0;k<degree_;k++) itn++;
        real_t td=itn->first; itn++;
        real_t tp=itn->first;
        a*=-(degree_/(tp-td));
        a*=tf_-t0_; // derivative normalisation
        B(1,1)=1.; B(2,1)=a; B(2,2)=-a;
    }
    if(bce_==_clampedBC)
    {
        for(number_t j=0;j<=m;j++) {B(m,j+1)=0;B(m+1,j+1)=0.;}

        real_t a=1.;
        if(!noWeight_) a=weights_[m]/weights_[m-1];
        std::multimap<real_t,number_t>::iterator itn=knots_.begin();
        for(number_t k=0;k<m;k++) itn++;
        real_t td=itn->first; itn++;
        real_t tp=itn->first;
        a*=-(degree_/(tp-td));
        a*=tf_-t0_; // derivative normalisation
        B(m+1,m+1)=1; B(m,m+1)=-a; B(m,m)=a;
    }
    // compute inverse of B
    Matrix<real_t> invB=inverse(B);
    // compute control points P=invB*D*Q
    number_t d=Q[0].size();
    Vector<real_t> rhs(m+1), x(m+1);
    for(number_t k=0;k<d;k++)
    {
        for(number_t j=0;j<=m;j++)
        {
            rhs[j]=Q[j][k];
            if(!noWeight_) rhs[j]*=D[j];
        }
        if(bcs_==_clampedBC) {rhs[0]=Q[0][k];rhs[1]=yps_[k];}
        if(bce_==_clampedBC) {rhs[m]=Q[m][k];rhs[m-1]=ype_[k];}
        multMatrixVector(invB, rhs, x);
        for(number_t j=0;j<=m;j++) P[j][k]=x[j];
    }
}

BSpline::~BSpline()
{
    if(parametrization_!=nullptr) delete parametrization_;
}

void BSpline::computeB(real_t t, DiffOpType dif, number_t& j, Vector<real_t>& B, Vector<real_t>& dB, Vector<real_t>& d2B) const
{
  // locate interval
  std::multimap<real_t,number_t>::const_iterator itj=knots_.begin();
  bool loc=false;
  j=0;
  if(t<t0_-theTolerance || t> tf_+theTolerance)
    error("free_error","in BSpline::evaluate, parameter t="+tostring(t)+" is outside the parameter interval["+tostring(t0_)+","+tostring(tf_)+"]");
  if(t<t0_) t=t0_; else if(t>=tf_) t=tf_-theTolerance;
  while (!loc && itj!=knots_.end())
  {
      if(t < (++itj)->first) loc=true; else j++;
  }
  if(!loc)
    error("free_error","in BSpline::evaluate, no localization of parameter t="+tostring(t)+", outside the parameter interval ["+tostring(t0_)+","+tostring(tf_)+"]");
  itj--;

  //set Bi0(t)
  number_t d=degree_+2;
  std::multimap<real_t,number_t>::const_iterator it,itk;
  B.resize(d); dB.resize(d);d2B.resize(d);
  B*=0; dB*=0; d2B*=0;
  B[degree_]=1.;
  for(number_t l=0;l<degree_;l++) itj--;

  //compute Bik at t
  real_t a,ap,ti,tip,bi,bip,dbi,dbip,d2bi,d2bip;
  for(number_t k=1;k<=degree_;k++)
    {
      it=itj;
      itk=itj;
      for(number_t l=0;l<k;l++) itk++;
      a = itk->first-it->first;
      for(number_t i=0;i<=degree_;i++)
        {
           if(i>=degree_-k)
           {
             bi=B[i];bip=B[i+1];
             ti=t-it->first;
             if(a!=0 && bi!=0) B[i]*=ti/a; else B[i]=0;
             ap = (++itk)->first - (++it)->first;
             tip = itk->first-t;
             if(ap!=0 && bip!=0) B[i]+=bip*tip/ap;
             if(dif>_id) // compute first derivative
             {
               dbi=dB[i]; dbip=dB[i+1];
               if(a!=0)  dB[i]=(bi+ti*dbi)/a;
               if(ap!=0) dB[i]+=(tip*dbip-bip)/ap;
             }
             if(dif>=_dt2) // compute second derivative
             {
               d2bi=d2B[i]; d2bip=d2B[i+1];
               if(a!=0)  d2B[i]=(dbi+ti*d2bi)/a;
               if(ap!=0) d2B[i]+=(tip*d2bip-dbip)/ap;
             }
             a=ap;
           }
           else a = (++itk)->first - (++it)->first;
        }
    }

    real_t dt= tf_-t0_;
    if(dif==_d1 || dif==_dt) dB*=dt;
    if(dif==_d11 || dif==_dt2) d2B*=dt*dt;
}

/*! evaluate B-Spline using Bernstein polynomials
    Q(s)= sum_{i=0,n} wi Bi,k(t) Pi /sum_{i=0,n} wi Bi,k(t)   with t=t0_+s*(tf_-t0_)
    with Bi,k the B-spline function
    NEW VERSION: separate the computation of B from the construction of Q
                  computeB may be reused, probably a little less efficient
    BSpline parameter t in [0,1]
    derivative order up to 2
*/
Point BSpline::evaluate(real_t t, DiffOpType dif) const
{
  if(dif==_dt) dif=_d1;
  if(dif==_dt2)dif=_d11;
  if(dif!=_id && dif!=_d1 && dif!=_d11)
    error("free_error","BSpline::evaluate only handles derivative of BSpline up to 2");
  number_t j=0;
  number_t di=dim();
  Vector<real_t> B, dB, d2B;
  computeB(toKnotsParameter(t),dif,j,B,dB,d2B);
  Point Q; Q.resize(di,0.);
  if(dif==_id || noWeight_)
  {
     if(dif==_d1 ) B=dB;
     if(dif==_d11) B=d2B;
     Vector<real_t>::iterator itb=B.begin();
     std::vector<Point>::const_iterator itp=controlPoints_.begin()+(j-degree_);
     if(noWeight_)  // B-spline
     {
      for(number_t i=0;i<=degree_; i++, ++itp, ++itb)
        for(number_t k=0;k<di;k++) Q[k]+=*itb* (*itp)[k];
     }
     else //rational B-Spline
     {
       std::vector<real_t>::const_iterator itw=weights_.begin()+(j-degree_);
       real_t w=0., wb;
       for(number_t i=0;i<=degree_; i++, ++itp, ++itb, ++itw)
       {
          wb=*itw * *itb;
          for(number_t k=0;k<di;k++) Q[k]+= wb * (*itp)[k];
          w+=wb;
       }
       Q/=w;
     }
  }
  else  // first or second derivative of rational BSpline
  {
      Point Q0, Q1;
      Q0.resize(di,0.); Q1.resize(di,0.);
      number_t j=0;
      computeB(toKnotsParameter(t),dif,j,B,dB,d2B);
      std::vector<Point>::const_iterator itp=controlPoints_.begin()+(j-degree_);
      std::vector<real_t>::const_iterator itw=weights_.begin()+(j-degree_);
      Vector<real_t>::iterator itb0=B.begin();
      Vector<real_t>::iterator itb1=dB.begin();
      real_t w0=0., w1=0., wb0, wb1;
      if(dif==_d1)
      {
        for(number_t i=0;i<=degree_; i++, ++itp, ++itb0, ++itb1, ++itw)
        {
          wb0=*itw * *itb0;
          wb1=*itw * *itb1;
          for(number_t k=0;k<di;k++)
          {
              Q0[k]+= wb0 * (*itp)[k];
              Q1[k]+= wb1 * (*itp)[k];
          }
          w0+=wb0;
          w1+=wb1;
        }
        Q=(w0*Q1-w1*Q0)/(w0*w0);
      }
      else
      {
          Point Q2; Q2.resize(di,0.);
          Vector<real_t>::iterator itb2=d2B.begin();
          real_t w2=0., wb2;
          for(number_t i=0;i<=degree_; i++, ++itp, ++itb0, ++itb1, ++itb2, ++itw)
          {
            wb0=*itw * *itb0;
            wb1=*itw * *itb1;
            wb2=*itw * *itb2;
            for(number_t k=0;k<di;k++)
            {
              Q0[k]+= wb0 * (*itp)[k];
              Q1[k]+= wb1 * (*itp)[k];
              Q2[k]+= wb2 * (*itp)[k];
            }
            w0+=wb0;
            w1+=wb1;
            w2+=wb2;
          }
          Q=(w0*(w0*Q2-w2*Q0)+2*w1*(w0*Q1-w1*Q0))/(w0*w0*w0);
      }
  }
  return Q;
}

/*! evaluate B-Spline using Bernstein polynomials
    Q(s)= sum_{i=0,n} wi Bi,k(t) Pi(t) /sum_{i=0,n} wi Bi,k(t)
    with Bi,k the B-spline function
    OLD version
*/
Point BSpline::evaluate2(real_t t, DiffOpType dif) const
{
  // locate interval
  t=toKnotsParameter(t);
  std::multimap<real_t,number_t>::const_iterator itj=knots_.begin();
  bool loc=false;
  number_t j=0;
  if(t<t0_-theTolerance || t> tf_+theTolerance) error("free_error","in BSpline::evaluate, parameter t="+tostring(t)+" is outside the parameter interval");
  if(t<t0_) t=t0_; else if(t>tf_) t=tf_-theTolerance;
  while (!loc && itj!=knots_.end())
  {
      if(t < (++itj)->first) loc=true; else j++;
  }
  if(!loc) error("free_error","in BSpline::evaluate, parameter t="+tostring(t)+" is outside the parameter interval");
  itj--;

  //set Bi0(t)
  number_t d=degree_+2;
  std::multimap<real_t,number_t>::const_iterator it,itk;
  Vector<real_t> B(d,0.), dB, d2B;
  B[degree_]=1.;
  if(dif>_id) dB.resize(d,0.);
  if(dif>=_dt2) d2B.resize(d,0.);
  for(number_t l=0;l<degree_;l++) itj--;

  //compute Bik at t
  real_t a,ap,ti,tip,bi,bip,dbi,dbip,d2bi,d2bip;
  for(number_t k=1;k<=degree_;k++)
    {
      it=itj;
      itk=itj;
      for(number_t l=0;l<k;l++) itk++;
      a = itk->first-it->first;
      for(number_t i=0;i<=degree_;i++)
        {
           if(i>=degree_-k)
           {
             bi=B[i];bip=B[i+1];
             ti=t-it->first;
             if(a!=0 && bi!=0) B[i]*=ti/a; else B[i]=0;
             ap = (++itk)->first - (++it)->first;
             tip = itk->first-t;
             if(ap!=0 && bip!=0) B[i]+=bip*tip/ap;
             if(dif>_id) // compute first derivative
             {
               dbi=dB[i]; dbip=dB[i+1];
               if(a!=0)  dB[i]=(bi+ti*dbi)/a;
               if(ap!=0) dB[i]+=(tip*dbip-bip)/ap;
             }
             if(dif>=_dt2) // compute second derivative
             {
               d2bi=d2B[i]; d2bip=d2B[i+1];
               if(a!=0)  d2B[i]=(dbi+ti*d2bi)/a;
               if(ap!=0) d2B[i]+=(tip*d2bip-dbip)/ap;
             }
             a=ap;
           }
           else a = (++itk)->first - (++it)->first;
        }
    }
  // compute BSpline
  number_t di=dim();
  Point Q(0.,0.,0.);
  if(di<3) Q.resize(di);
  Vector<real_t>::iterator itb;
  switch(dif)
  {
      case _id: itb=B.begin();break;
      case _dt:
      case _dx: itb=dB.begin();break;
      case _dt2:
      case _dxx: itb=d2B.begin();break;
      default: error("free_error"," differential operator not handled in BSpline::evaluate");
  }

  std::vector<Point>::const_iterator itp=controlPoints_.begin()+(j-degree_);
  if(noWeight_)  // B-spline
  {
      for(number_t i=0;i<=degree_; i++, ++itp, ++itb)
        for(number_t k=0;k<di;k++) Q[k]+=*itb* (*itp)[k];
  }
  else //rational B-Spline
  {
      std::vector<real_t>::const_iterator itw=weights_.begin()+(j-degree_);
      real_t w=0., wb;
      for(number_t i=0;i<=degree_; i++, ++itp, ++itb, ++itw)
        {
            wb=*itw * *itb;
            for(number_t k=0;k<di;k++) Q[k]+= wb * (*itp)[k];
            w+=wb;
        }
        Q/=w;
  }
  return Q;
}

std::vector<Point> BSpline::boundNodes() const
{
  std::vector<Point> res(2);
  if(subtype_==_SplineInterpolation)
  {
    res[0]=interpolationPoints_[0];
    res[1]=interpolationPoints_[interpolationPoints_.size()-1];
  }
  else
  {
    res[0]=controlPoints_[0];
    res[1]=controlPoints_[controlPoints_.size()-1];
  }
  return res;
}

//! print utility
void BSpline::print(std::ostream& out,bool nocpt) const
{
  if(theVerboseLevel==0) return;
  Spline::print(out,nocpt);
  if(noWeight_) out<<"    uniform weight"; else out<<"    non uniform weight";
  if(theVerboseLevel<5 || noWeight_)  {out<<eol;return;}
  out<<"  = "<<weights_<<eol;
}

Vector<real_t> BSpline::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const //!< parametrization s(t)
{
  Point Q = evaluate(pt[0],d);
  return Vector<real_t>(Q.begin(),Q.end());
}

Vector<real_t> BSpline::invParametrization(const Point& pt, Parameters& pars, DiffOpType d) const //!< inverse of parametrization
{
  error("free_error","inv parametrization of BSpline is not available");
  return Vector<real_t>(1,pt[0]);
}

//============================================================================================
//      BezierSpline functions
//============================================================================================
BezierSpline::BezierSpline(const std::vector<Point>& cpts)
{
    number_t n=cpts.size();
    if(n<2) error("free_error","give at least 2 points for buiding a Bezier spline");
    controlPoints_=cpts;
    type_=_BezierSpline;
    subtype_=_noSplineSubtype;
    degree_=n-1;
    splinePar_=_uniformParametrization;
    bcs_=_clampedBC;bce_=_clampedBC;
    isClosed_=dist(controlPoints_[degree_],controlPoints_[0])<theTolerance;
    Parameters pars(reinterpret_cast<const void *>(this),"spline");
    parametrization_ = new Parametrization(0., 1., parametrization_BezierSpline, pars,"BezierSpline parametrization",dimen_t(controlPoints_[0].size()));
    parametrization_->setinvParametrization(invParametrization_BezierSpline);
}

//! copy constructor
BezierSpline::BezierSpline(const BezierSpline& bs)
{
    copy(bs);
}

//! copy BezierSpline data in a cleaned BezierSpline (re-allocate parametrization_ pointer)
void BezierSpline::copy(const BezierSpline& bs)
{
    controlPoints_=bs.controlPoints_;
    knots_=bs.knots_;
    degree_=bs.degree_;;
    isClosed_=bs.isClosed_;
    type_=bs.type_;
    subtype_=bs.subtype_;
    bcs_=bs.bcs_;
    bce_=bs.bce_;
    splinePar_=bs.splinePar_;
    yps_=bs.yps_;
    ype_=bs.ype_;
    //instead copy of parametrization, re-create a new parametrization of BezierSpline
    Parameters pars(reinterpret_cast<const void *>(this),"spline");
    parametrization_ = new Parametrization(0., 1., parametrization_BezierSpline, pars,"BezierSpline parametrization",dimen_t(controlPoints_[0].size()));
    parametrization_->setinvParametrization(invParametrization_BezierSpline);
}

//! assign BezierSpline to current one
BezierSpline& BezierSpline::operator=(const BezierSpline& bs)
{
    delete parametrization_;
    copy(bs);
    return *this;
}


Point BezierSpline::evaluate(real_t t, DiffOpType dif) const
{
    if(t==0) return controlPoints_[0];
    if(t==1) return controlPoints_[degree_];
    number_t d=controlPoints_[0].size();
    number_t der=0;
    real_t b=std::pow(1-t,degree_),db=0.,d2b=0.;
    real_t a=t/(1.-t),a1,a2,*c=&b;
    if(dif==_dt || dif==_dx)
        {
            der=1;
            db=degree_*std::pow(1-t,degree_-1);
            a1=1./((1-t)*(1-t));
            c=&db;
        }
    if(dif==_dt2 || dif==_dxx)
        {
            der=2;
            if(degree_>1) d2b=degree_*(degree_-1)*std::pow(1-t,degree_-2);
            a1=1./((1-t)*(1-t));
            a2 =2*a1/(1-t);
            c=&d2b;
        }
    Point Q=controlPoints_[0]; Q*=b;
    number_t j=degree_;
    for(number_t i=1;i<=degree_;i++,j--)
    {
        if(der>=2) {d2b*=a; d2b+=a1*db; d2b+=a2*b;}
        if(der>=1) {db*=a; db+=a1*b;}
        b*=(a*j)/i;
        for(number_t k=0;k<d;k++) Q[k]+=*c*controlPoints_[i][k];
    }
    return Q;
}

BezierSpline::~BezierSpline()
{
    if(parametrization_!=nullptr) delete parametrization_;
}

//! print utility
void BezierSpline::print(std::ostream& out) const
{
  if(theVerboseLevel==0) return;
  Spline::print(out);
}

Vector<real_t> BezierSpline::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const //!< parametrization s(t)
{
  Point Q = evaluate(pt[0],d);
  return Vector<real_t>(Q.begin(),Q.end());
}

Vector<real_t> BezierSpline::invParametrization(const Point& pt, Parameters& pars, DiffOpType d) const //!< inverse of parametrization
{
  error("free_error","inv parametrization of BezierSpline is not available");
  return Vector<real_t>(1,pt[0]);
}

//============================================================================================
//      Nurbs functions
//============================================================================================
// controlPoints and weights given as vectors of vectors
Nurbs::Nurbs(const std::vector<std::vector<Point> >& cpts, number_t degree_u,number_t degree_v,
             SplineBC bcs_u, SplineBC bcs_v, SplineBC bce_u, SplineBC bce_v,
             const std::vector<std::vector<real_t> >& w)
             : Spline()
{
  controlPointsM_=cpts;
  weightsM_=w;
  init(degree_u,degree_v,bcs_u,bcs_v,bce_u,bce_v);
  initInterp();
}

Nurbs::Nurbs(SplineSubtype sbt, const std::vector<std::vector<Point> >& cpts, number_t degree_u,number_t degree_v,
             SplineBC bcs_u, SplineBC bcs_v, SplineBC bce_u, SplineBC bce_v, const std::vector<std::vector<real_t> >& w)
             : Spline()
{
  controlPointsM_=cpts;
  weightsM_=w;
  init(degree_u,degree_v,bcs_u,bcs_v,bce_u,bce_v);
  if(sbt==_SplineInterpolation) initInterp();
}

// controlPoints and weights given as vectors (p11,... p1n,p21, ... p2n, ..., pm1,... pmn)
Nurbs::Nurbs(SplineSubtype sbt, const std::vector<Point>& cpts, number_t ncpu, number_t degree_u,number_t degree_v,
             SplineBC bcs_u, SplineBC bcs_v, SplineBC bce_u, SplineBC bce_v,
             const std::vector<real_t>& w)
             : Spline()
{
   initMatrices(ncpu,cpts,w);
   init(degree_u,degree_v,bcs_u,bcs_v,bce_u,bce_v);
   if(sbt==_noSplineSubtype) sbt=_SplineInterpolation;
   if(sbt==_SplineInterpolation) initInterp();
}

Nurbs::Nurbs(const std::vector<Point>& cpts, number_t ncpu, number_t degree_u,number_t degree_v,
             SplineBC bcs_u, SplineBC bcs_v, SplineBC bce_u, SplineBC bce_v,
             const std::vector<real_t>& w)
             : Spline()
{
   initMatrices(ncpu,cpts,w);
   init(degree_u,degree_v,bcs_u,bcs_v,bce_u,bce_v);
   initInterp();  //interpolation if subtype is undef
}

// build controlPointsM_ and weightsM_ matrices from vectors
void Nurbs::initMatrices(number_t ncpu, const std::vector<Point>& cpts,const std::vector<real_t>& w)
{
    //check data consistancy and //move vectors to vectors of vectors
    number_t np=cpts.size(), nw=w.size();
    if(cpts.size()<ncpu) error("free_error", "the number of points in u direction ("+tostring(ncpu)
                               +") should be greater or equal to the number of control points ("+tostring(np)+")");
    number_t ncpv=np/ncpu;
    if(ncpu*ncpv<np) warning("free_warning"," number of control points is not a multiple of number of points in u direction, some last control points will be omitted");
    if(nw>0 && nw<np) error("free_error", "the number of weights should be greater or equal to the number of points");
    controlPointsM_.resize(ncpu);
    std::vector<Point> ptsv(ncpv);
    std::vector<Point>::const_iterator itp=cpts.begin();
    for(number_t i=0;i<ncpu; i++)
    {
       for(number_t j=0;j<ncpv; j++, ++itp) ptsv[j]=*itp;
       controlPointsM_[i]=ptsv;
    }
    if(nw>0)
    {
       weightsM_.resize(ncpu);
       std::vector<real_t> wv(ncpv);
       std::vector<real_t>::const_iterator itw=w.begin();
       for(number_t i=0;i<ncpu; i++)
       {
         for(number_t j=0;j<ncpv; j++, ++itw) wv[j]=*itw;
         weightsM_[i]=wv;
       }
    }
}

void Nurbs::init( number_t degree_u,number_t degree_v, SplineBC bcs_u, SplineBC bcs_v, SplineBC bce_u, SplineBC bce_v)
{
  // Spline data members
  type_=_Nurbs;
  subtype_=_SplineApproximation;
  degree_=degree_u; // not used
  isClosed_=false;  // not used yet
  bcs_=bcs_u;       // not used
  bce_=bce_u;       // not used
  splinePar_=_uniformParametrization;
  // Nurbs data members
  noWeight_=weightsM_.size()==0;
  number_t n=controlPointsM_.size();
  std::vector<Point> cptu(n);
  for(number_t i=0;i<n;i++) cptu[i]=controlPointsM_[i][0];
  bs_u=new BSpline(cptu,degree_u,bcs_u,bce_u);   // cptu is passed only to build the u-Bspline. It is not used when evaluating nurbs
  bs_v=new BSpline(controlPointsM_[0],degree_v,bcs_v,bce_v);// controlPointsM_[0] is passed only to build the v-Bspline. It is not used when evaluating nurbs
  // set parametrization
  Parameters pars(reinterpret_cast<const void *>(this),"spline");
  parametrization_ = new Parametrization(0.,1.,0.,1., parametrization_Nurbs, pars,"Nurbs parametrization",dimen_t(controlPointsM_[0].size()));
  parametrization_->setinvParametrization(invParametrization_Nurbs);
}

/*! initialization when interpolation is selected
    controlPointsM_ (in)  : list of interpolated points Q_ij
    controlPointsM_ (out) : list of control points P_ij leading to nurbs interpolating Q_ij
    this function solve the linear system
         sum_i=0,m sum_j=0,n w_ij*B_i(u_k)B_j(v_l)P_ij = sum_i=0,m sum_j=0,n w_ij*B_i(u_k)B_j(v_l) Q_kl    0 <= k <= m, 0<= l <= n
    <=>  M*P=D*Q  with M_ij_kl= w_ij*B_j(u_k)B_j(v_l)  D_kl = sum_i=0,m sum_j=0,n w_ij*B_i(u_k)B_j(v_l) =  sum_i=0,m sum_j=0,n  M_ij
    when w_ij=1 for all i,j (no weight) ==> M*P=Q

    u_k, v_l are given by  u_k = u0+r_k*(uf-u0)  and v_l = v0 + s_l*(vf-v0)  (u0,uf u knot bounds, v0,vf v-knot bounds)
    where r_k, v_l  are chosen using one of the rules
        uniform: r_k=k/m
                      s_l=l/n
        chordal: r_k = r_k-1 + |Q_k,0-Q_k-1,0|/ sum_i=1,m |Q_i,0-Q_i-1,0|  , r_0 = 0
                      s_l = s_l-1 + |Q_0,l-Q_0,l-1|/ sum_i=1,m |Q_0,i-Q_0,i-1|  , v_0 = 0
        centripetal: r_k = r_k-1 + sqrt|Q_k,0-Q_k-1,0|/ sum_i=1,m sqrt|Q_i,0-Q_i-1,0|  , r_0 = 0
                      s_l = s_l-1 + sqrt|Q_0,l-Q_0,l-1|/ sum_i=1,m sqrt|Q_0,i-Q_0,i-1|  , v_0 = 0
        ONLY UNIFORM IS AVAILABLE

    NOTE: M is a sparse matrix because B_i(u_k) != 0 if ku-deg <= i <= ku and B_j(v_l) != 0 if kv-deg <= j <= kv (iu, iv knots index related to uk and vl)
           as a result M(kl)(ij) = wij B_i(u_k) B_j(v_l) !=0  for ku-deg <= i <= ku and kv-deg <= j <= kv
           for row k*m+l, non zero cols: (ku-deg)*m+kv-deg -> (ku-deg)*m+kv,...,  (ku-deg+1)*m+kv-deg -> (ku-deg+1 )*m+kv, ...ku*m+kv-deg -> ku*m+kv,
*/
void Nurbs::initInterp()
{
    subtype_=_SplineInterpolation;
    std::vector<std::vector<Point> >  Q = controlPointsM_;   // copy controlPoints to interpolated points
    std::vector<std::vector<Point> >& P = controlPointsM_;   // new control points
    number_t m = Q.size(), n=Q[0].size(), mn=m*n;
    RealPair u0f=bs_u->parameterBounds(), v0f=bs_v->parameterBounds();
    number_t degu=bs_u->degree(), degv=bs_v->degree();
    //build matrix M (up to now as dense matrix, to be improved in future)
    Matrix<real_t> M(mn,mn);
    Vector<real_t> D;
    if(!noWeight_) D.resize(m,0.);
    Vector<real_t> buk, dbuk, d2buk, bvl, dbvl, d2bvl ;
    number_t ku,kv;
    for(number_t k=0;k<m;k++)
    {
      real_t rk = real_t(k)/(m-1);       // uniform
      real_t uk = u0f.first+rk*(u0f.second-u0f.first); // map to [u0,uf]
      bs_u->computeB(uk, _id, ku, buk, dbuk, d2buk);
      for(number_t l=0;l<n;l++)
      {
        real_t sl = real_t(l)/(n-1);       // uniform
        real_t vl = v0f.first+sl*(v0f.second-v0f.first); // map to [v0,vf]
        bs_v->computeB(vl, _id, kv, bvl, dbvl, d2bvl);
        if(noWeight_)  // non rational
        {
          Vector<real_t>::iterator itu=buk.begin();
          number_t pu=ku-degu;
          for(number_t du=0; du<=degu; du++, pu++, ++itu)
          {
             Vector<real_t>::iterator itv=bvl.begin();
             number_t pv=kv-degv;
             for(number_t dv=0;dv<=degv; dv++, pv++, ++itv)
             {
                 M(k*n+l+1,pu*n+pv+1) = *itu * *itv;
             }
          }
        }
        else //rational B-Spline
        {
          real_t wb;
          Vector<real_t>::iterator itu=buk.begin();
          number_t pu=ku-degu;
          for(number_t du=0; du<=degu; du++,pu++, ++itu)
          {
             Vector<real_t>::iterator itv=bvl.begin();
             number_t pv=kv-degv;
             for(number_t dv=0;dv<=degv; dv++, pv++, ++itv)
             {
                 wb=weightsM_[pu][pv]* *itu * *itv;
                 M(k*n+l+1,pu*n+pv+1) = wb;
                 D[k*n+l]+=wb;
             }
          }
        }
      }
    }

    // modify system if clamped conditions (TO DO)

    // solve system MP=DQ using Gauss
    number_t dim=Q[0][0].size();  //point dimension (should be 3)
    Vector<real_t> rhs(dim*mn);
    number_t i=0, j=0;
    for(number_t d=0;d<dim;d++) // build rhs's
    {
      j=0;
      for(number_t k=0;k<m;k++)
        for(number_t l=0;l<n;l++, i++)
        {
          rhs[i]=Q[k][l][d];
          if(!noWeight_) {rhs[i]*=D[j]; j++;}
        }
    }
    number_t row;
    real_t minPivot=1E-12;
    if(!gaussMultipleSolver(M, rhs, dim, minPivot,row))
       error("free_error","in Nurbs::initInterp, fails to solve linear system");
    i=0;
    for(number_t d=0; d<dim; d++)
      for(number_t k=0; k<m; k++)
        for(number_t l=0; l<n; l++, i++)
          P[k][l][d]=rhs[i];
}

//! copy constructor
Nurbs::Nurbs(const Nurbs& nu)
{
    copy(nu);
}

//! copy Nurbs data in a cleaned Nurbs (re-allocate parametrization_ pointer)
void Nurbs::copy(const Nurbs& nu)
{
    controlPointsM_=nu.controlPointsM_;
    weightsM_=nu.weightsM_;
    noWeight_=nu.noWeight_;
    bs_u=new BSpline(*nu.bs_u);
    bs_v=new BSpline(*nu.bs_v);
    Parameters pars(reinterpret_cast<const void *>(this),"spline");
    parametrization_ = new Parametrization(0.,1.,0.,1., parametrization_Nurbs, pars,"Nurbs parametrization",dimen_t(controlPointsM_[0].size()));
    parametrization_->setinvParametrization(invParametrization_Nurbs);
    type_=nu.type_;
    subtype_=nu.subtype_;
    degree_=nu.degree_;
    isClosed_=nu.isClosed_;
    bcs_=nu.bcs_;
    bce_=nu.bce_;
    splinePar_=nu.splinePar_;
}

//! assign Nurbs to current one
Nurbs& Nurbs::operator=(const Nurbs& nu)
{
    if(bs_u!=nullptr) delete bs_u;
    if(bs_v!=nullptr) delete bs_v;
    if(parametrization_!=nullptr) delete parametrization_;
    copy(nu);
    return *this;
}

Nurbs::~Nurbs()
{
    if(bs_u!=nullptr) delete bs_u;
    if(bs_v!=nullptr) delete bs_v;
    if(parametrization_!=nullptr) delete parametrization_;
}

//! print utility
void Nurbs::print(std::ostream& out) const
{
  if(theVerboseLevel==0) return;
  out<<"Nurbs, "<<controlPointsM_.size()*controlPointsM_[0].size()<<" points"<<eol;
  if(theVerboseLevel<5) return;
  out<<"  "<<words("control points")<<": "<<controlPointsM_<<eol;
  if(weightsM_.size()>0) out<<"    "<<words("weights")<<": "<<weightsM_<<eol;
  if(bs_u!=nullptr) {out<<"  u parameter: ";bs_u->print(out,true);}
  if(bs_v!=nullptr) {out<<"  v parameter: ";bs_v->print(out,true);}
}

/*! evaluate nurbs or its derivative at (u,v)
    Q(u,v)= sum{i=0,m} sum{j=0,n} wij Bi,k(s) Bi,k(t) Pij / sum{i=0,m} sum{j=0,n} wij Bi,k(s) Bi,k(t)
    with s = s0+u*(sf-s0) ,  t = t0+v*(tf-t0)  with (s0,sf) and (t0,tf) bounds of u/v knot parameters
    and Bi,k B-spline functions
    When wij=1 for all i,j: Q(u,v)= sum{i=0,m} sum{j=0,n} Bi,k(s) Bi,k(t) Pij  (not rational)
*/
// controlPointsM is m x n matrix of points, m= controlPointsM.size() stands for u parameter while n=controlPointsM[0].size() stands for v parameter

Point Nurbs::evaluate(real_t u, real_t v, DiffOpType dif) const
{
    Vector<real_t> Bu, Bv, dBu, dBv, d2Bu, d2Bv;
    number_t ju,jv;
    DiffOpType difu=_id, difv=_id;
    switch(dif)
    {
        case _id: break;
        case _dx:
        case _dt: difu=_dt; break;
        case _dxx:
        case _dt2: difu=_dt2; break;
        case _dy: difv=_dt; break;
        case _dyy: difv=_dt2; break;
        case _dxy: difu=_dt; difv=_dt;break;
        default: error("free_error"," differential operator not handled in Nurbs::evaluate");
    }
    bs_u->computeB(bs_u->toKnotsParameter(u),difu,ju,Bu,dBu,d2Bu);
    bs_v->computeB(bs_v->toKnotsParameter(v),difv,jv,Bv,dBv,d2Bv);
    number_t di=dim();
    Point Q(0.,0.,0.);
    if(di<3) Q.resize(di);
    number_t degu=bs_u->degree(), degv=bs_v->degree();
    std::vector<std::vector<Point> >::const_iterator itpu=controlPointsM_.begin()+(ju-degu);
    std::vector<Point>::const_iterator itpv;
    Vector<real_t>::iterator itbu=Bu.begin(),itbv;
    if(weightsM_.size()==0)
    {
       real_t wb;
       for(number_t i=0;i<=degu;i++,++itbu,++itpu)
      {
        itbv=Bv.begin();
        itpv=itpu->begin()+(jv-degv);
        for(number_t j=0;j<=degv;j++,++itbv,++itpv)
        {
            wb=*itbu**itbv;
            for(number_t k=0;k<di;k++) Q[k]+=wb*(*itpv)[k];
        }
      }
    }
    else // TO BE CORRECTED
    {
      real_t w=0., wb;
      std::vector<std::vector<real_t> >::const_iterator itwu=weightsM_.begin()+(ju-degu);
      std::vector<real_t>::const_iterator itwv;
      for(number_t i=0;i<degu;i++,++itbu,++itpu)
      {
        itbv=Bv.begin();
        itpv=itpu->begin()+(jv-degv);
        itwv=itwu->begin()+(jv-degv);
        for(number_t j=0;j<degv;j++,++itbv,++itpv, ++itwv)
        {
            wb = *itwv * *itbu * *itbv;
            for(number_t k=0;k<di;k++) Q[k]+=wb*(*itpv)[k];
            w+=wb;
        }

      }
      Q/=w;
    }
  return Q;
}

Vector<real_t> Nurbs::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const //!< parametrization s(t)
{
  Point Q = evaluate(pt[0],pt[1],d);
  return Vector<real_t>(Q.begin(),Q.end());
}

Vector<real_t> Nurbs::invParametrization(const Point& pt, Parameters& pars, DiffOpType d) const //!< inverse of parametrization
{
  error("free_error","inv parametrization of Nurbs is not available");
  return Vector<real_t>(1,pt[0]);
}


}// end of namespace xlifepp
