/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file init.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 08 jan 2005
  \date 4 may 2012

  \brief Definition of init function

  Initialisation procedure for error/message handling:

  It creates the XLiFE++ Messages classe loading ascii files of messages,
  see Messages.hpp for a description of these files.
  Those classes have global scope.

  To instantiate an error message, see function msg in Messages.cpp

  To add a new error class:
    - make a new error file (one for each language) : error_file
    - create a new Messages class:
      \code
      my_errors=new Messages(error_file,out_stream,out_file,error_type);
      \endcode
    - or append the new error file in the unique Messages object
      \code
      theMessages_p->appendFromFile(error_file);
      \endcode

  To throw some me_errors call:
    \code
    msg(id_message,data,my_errors)
    msg(id_message,data)
    \endcode

  messages file names are [en | fr]/xxxxx.txt
  where xxxxx is the error family, same as internal Messages object
  for example: for internal error types,
  object name is internal and messages file is [en | fr]/internal.txt

  format of format message in file is of the form:
    \verbatim
    # int_id string_id type_error type_status format_string
    \endverbatim
    where format_string any sequence of strings (excluding character '#') and any combination of %i, %r, %c, %b, %s where:
      - %i stands for an integer parameter,
      - %r stands for a real parameter,
      - %c stands for a complex parameter,
      - %b stands for a boolean parameter,
      - %s stands for a string parameter
  Eol can be used in format_string and is preserved on output
 */
#ifndef INIT_HPP
#define INIT_HPP

#include "config.hpp"

namespace xlifepp
{

//================================================================================
// init functions
//================================================================================

//@{
//! initializes execution of XLiFE++
void init();
void init(const Parameter& p1);
void init(const Parameter& p1, const Parameter& p2);
void init(const Parameter& p1, const Parameter& p2, const Parameter& p3);
void init(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4);
void init(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5);
void init(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6);
void init(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
          const Parameter& p7);
void init(int argc, char** argv);
void init(int argc, char** argv, const Parameter& p1);
void init(int argc, char** argv, const Parameter& p1, const Parameter& p2);
void init(int argc, char** argv, const Parameter& p1, const Parameter& p2, const Parameter& p3);
void init(int argc, char** argv, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4);
void init(int argc, char** argv, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5);
void init(int argc, char** argv, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
          const Parameter& p6);
void init(int argc, char** argv, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
          const Parameter& p6, const Parameter& p7);
void init(const std::vector<Parameter>& ps, int argc = 0, char** argv = nullptr);

void initBuild(Language lang, number_t verboseLevel, int_t nbThreads, bool trackingMode, bool disablePushPop, bool traceMemory,
               bool isLogged, const std::vector<string_t>& syskeys, const std::vector<string_t>& authorizedExtensions);
//@}

void timerInit(); //!< initialization of engine for time handling
void traceInit(); //!< initialization of engine for trace handling
void initRandomGenerators(int); //!<  initialize random generators from a seed
void msgInit(const string_t&, std::ofstream&); //!< initialization of engine for messages handling

//@{
//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const EcType& et);
std::ostream& operator<<(std::ostream& out, const ReductionMethodType& rmt);
std::ostream& operator<<(std::ostream& out, const InterpolationType& it);
std::ostream& operator<<(std::ostream& out, const FESubType& fest);
std::ostream& operator<<(std::ostream& out, const FEType& fet);
std::ostream& operator<<(std::ostream& out, const IntegrationMethodType& imt);
std::ostream& operator<<(std::ostream& out, const QuadRule& qr);
std::ostream& operator<<(std::ostream& out, const SobolevType& st);
std::ostream& operator<<(std::ostream& out, const LinearFormType& lft);
std::ostream& operator<<(std::ostream& out, const ComputationType& ct);
std::ostream& operator<<(std::ostream& out, const TransformType& tt);
std::ostream& operator<<(std::ostream& out, const IOFormat& iof);
std::ostream& operator<<(std::ostream& out, const OrientationType& ot);
std::ostream& operator<<(std::ostream& out, const ShapeType& sh);
std::ostream& operator<<(std::ostream& out, const ShapesType& sh);
std::ostream& operator<<(std::ostream& out, const OCShapeType& ocst);
std::ostream& operator<<(std::ostream& out, const AccessType& at);
std::ostream& operator<<(std::ostream& out, const FactorizationType& ft);
std::ostream& operator<<(std::ostream& out, const StorageType& st);
std::ostream& operator<<(std::ostream& out, const StorageBuildType& sbt);
std::ostream& operator<<(std::ostream& out, const StrucType& st);
std::ostream& operator<<(std::ostream& out, const SymType& st);
std::ostream& operator<<(std::ostream& out, const DiffOpType& dot);
std::ostream& operator<<(std::ostream& out, const ContinuityOrder& co);
std::ostream& operator<<(std::ostream& out, const UnitaryVector& uv);
std::ostream& operator<<(std::ostream& out, const DofType& dt);
std::ostream& operator<<(std::ostream& out, const SpaceType& st);
std::ostream& operator<<(std::ostream& out, const SupportType& st);
std::ostream& operator<<(std::ostream& out, const UnknownType& ut);
std::ostream& operator<<(std::ostream& out, const EigenSolverType& est);
std::ostream& operator<<(std::ostream& out, const IterativeSolverType& ist);
std::ostream& operator<<(std::ostream& out, const HMApproximationMethod& hmam);
std::ostream& operator<<(std::ostream& out, const FuncFormType& fft);
std::ostream& operator<<(std::ostream& out, const FunctType& ft);
std::ostream& operator<<(std::ostream& out, const Language& l);
std::ostream& operator<<(std::ostream& out, const ValueType& vt);
std::ostream& operator<<(std::ostream& out, const SplineType& st);
std::ostream& operator<<(std::ostream& out, const SplineSubtype& sst);
std::ostream& operator<<(std::ostream& out, const SplineParametrization& sp);
std::ostream& operator<<(std::ostream& out, const SplineBC& sbc);
//@}

} // end of namespace xlifepp

#endif /* INIT_HPP */
