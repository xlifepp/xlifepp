/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file user_typedefs.hpp
  \author N. Kielbasiewicz
  \since 12 dec 2014
  \date 12 dec 2014

  \brief typedefs definition for users
  typedefs to hide references and template parameters for user
 */

#ifndef USER_TYPEDEFS_HPP
#define USER_TYPEDEFS_HPP

#include <iosfwd>
#include <string>
#include <iostream>

namespace xlifepp
{

//@{
//! alias for basic type
typedef int_t Int;
typedef real_t Real;
typedef complex_t Complex;
typedef number_t Number;
typedef dimen_t Dimen;
typedef string_t String;
//@}

typedef Parameters Options;

typedef GeomDomain& Domain;

typedef Interpolation& FEInterpolation;

//@{
//! alias for vector type
typedef Reals RealVector;
typedef Complexes ComplexVector;
typedef Vector<Reals> RealVectors;
typedef Vector<Complexes> ComplexVectors;
//@}

//@{
//! alias for matrix type
typedef Matrix<Real> RealMatrix;
typedef Matrix<Complex> ComplexMatrix;
typedef Matrix<Matrix<Real> > RealMatrices;
typedef Matrix<Matrix<Complex> > ComplexMatrices;
//@}

} // end of namespace xlifepp

#endif /* USER_TYPEDEFS_HPP */
