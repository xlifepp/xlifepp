/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file config.hpp
  \authors D. Martin, E. Lunéville, M.-H. Nguyen, N. Kielbasiewicz
  \since 10 jun 2011
  \date 17 oct 2014

  \brief config "compile configuration" header file (pre-compiler macros)
 */

#ifndef CONFIG_HPP
#define CONFIG_HPP

// Include most commonly used C++ definition files
#include <cfloat>
#include <climits>
#include <complex>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <iosfwd>
#include <string>
#include <set>
#include <vector>
#include <map>
#include <utility>
#include <algorithm>
#include <limits>
#include <iostream>
#include <list>
#include <iterator>

#if __cplusplus >= 201103L
  #include <random>
#endif // __cplusplus

// setup: define OS, precision type, default language, debug parameter, ...
#include "setup.hpp"

//! main namespace of XLiFE++ library
namespace xlifepp
{

//------------------------------------------------------------------------------
// Definition of scalars
//------------------------------------------------------------------------------
#if defined(STD_TYPES)
//! typedef for real
typedef float real_t;
//! typedef for complex
typedef std::complex<float> complex_t;
#elif defined(LONG_TYPES)
//! typedef for real
typedef double real_t;
//! typedef for complex
typedef std::complex<double> complex_t;
#elif defined(LONGLONG_TYPES)
//! typedef for real
typedef long double real_t;
//! typedef for complex
typedef std::complex<long double> complex_t;
#endif

#if defined(COMPILER_IS_32_BITS)
//! typedef for integer
typedef long int int_t;
#else
//! typedef for integer
typedef long long int_t;
typedef unsigned long long uint_t;
#endif
//------------------------------------------------------------------------------
// String choice
//------------------------------------------------------------------------------
#if defined(STD_STRING)
//! typedef for string
typedef std::string string_t;
#elif defined(WIDE_STRING)
//! typedef for string
typedef std::wstring string_t;
#endif

//------------------------------------------------------------------------------
// Shortcuts (type aliases)
//------------------------------------------------------------------------------
//! typedef for short unsigned int
typedef short unsigned int dimen_t;
//! typedef for size_t
typedef size_t number_t;

typedef std::pair<dimen_t, dimen_t> dimPair; //!< useful alias
typedef std::pair<number_t, number_t> NumPair;
typedef std::pair<real_t, real_t> RealPair;

//------------------------------------------------------------------------------
// Output format parameters and extremal values
//------------------------------------------------------------------------------
extern const number_t entriesPerRow;
// unused: extern const number_t addrPerRow;
// unused: extern const number_t numberPerRow;
extern const number_t entryWidth;
extern const number_t entryPrec;
extern const number_t addrWidth;
extern const number_t numberWidth;
extern const number_t fullPrec; // for numerical data that could be reused
extern const number_t testPrec; // for test programs output

extern const number_t theNumberMax;
extern const size_t theSizeMax;
extern const dimen_t theDimMax;
extern const int_t theIntMax;
extern const int_t theIntMin;
extern const real_t theRealMax;
extern const real_t theEpsilon;
extern const real_t theTolerance;
extern const real_t theZeroThreshold;
extern const number_t theLanguage;
extern number_t theGlobalVerboseLevel;
extern number_t theVerboseLevel;
extern bool trackingObjects;
extern bool isTestMode;
extern real_t theDefaultCharacteristicLength; // default value of local step for points definition in a geo script (gmsh)
extern real_t theLocateToleranceFactor; // tolerance factor of points located outside of each element, but near enough to be projected

extern number_t defaultMaxIterations;
extern real_t theRatioOfIterations2Rows;
extern real_t theBreakdownThreshold;
extern real_t theDefaultConvergenceThreshold;
extern number_t defaultKrylovDimension;

extern std::string eol;

//------------------------------------------------------------------------------
// Some math constants
//------------------------------------------------------------------------------
// 1/3, 1/6, pi, 1/pi, 1/2pi, 1/4pi, sqrt(2), sqrt(3), sqrt(pi), log(2), exp(i.pi/3)
extern const real_t over3_;
extern const real_t over6_;
extern const real_t pi_;
extern const complex_t i_;
extern const real_t overpi_;
extern const real_t over2pi_;
extern const real_t over4pi_;
extern const real_t sqrtOf2_;
extern const real_t sqrtOf3_;
extern const real_t sqrtOfpi_;
extern const real_t logOf2_;
extern const complex_t expipiover3_;
//Euler-Mascheroni gamma constant =-\int_0^\infty exp(-x)log(x)dx = .577215664901532860606512090082402431042...;
extern const real_t theEulerConst;

//------------------------------------------------------------------------------
// default random seed and generator
//------------------------------------------------------------------------------
extern unsigned int theRandomSeed;
#if __cplusplus >= 201103L    //c++11 enabled, use std::random
extern std::mt19937 theRandomEngine;  // Mersenne Twister 19937 generator
#endif

//------------------------------------------------------------------------------
// Declaration of default global objects
//------------------------------------------------------------------------------
class Parameters;
extern Parameters defaultParameters;
extern Parameters* defaultParameters_p;

class Messages;
extern Messages* theMessages_p;

class Trace;
extern Trace* trace_p;

class MsgData;
extern MsgData theMessageData;
extern std::string theWhereData;

class Environment;
extern Environment* theEnvironment_p;

#ifndef DOXYGEN_SHOULD_SKIP_THIS

class Parameter;
extern Parameter _lang, _verbose, _nbThreads, _trackingMode, _pushpop, _traceMemory, _isLogged;
extern Parameter _varnames;
extern Parameter _vertices, _faces;
extern Parameter _center, _center1, _center2, _apogee, _origin, _apex, _v1, _v2, _v3, _v4, _v5, _v6, _v7, _v8;
extern Parameter _xmin, _xmax, _ymin, _ymax, _zmin, _zmax;
extern Parameter _spline, _splineType, _splineSubtype, _degree, _splineBC, _tensionFactor, _splineParametrization, _weights, _tangent0, _tangent1, _nbu;
extern Parameter _tmin, _tmax, _parametrization, _partitioning, _nbParts, _shapePartition;
extern Parameter _partmesh, _ncommon, _ptype, _objtype, _ctype, _iptype, _rtype, _savedoms, _repsortie;
extern Parameter _length, _xlength, _ylength, _zlength, _radius, _radius1, _radius2, _xradius, _yradius, _zradius;
extern Parameter _basis, _scale, _direction, _axis, _normal;
extern Parameter _end_shape, _end1_shape, _end2_shape, _end_distance, _end1_distance, _end2_distance;
extern Parameter _nnodes, _hsteps;
extern Parameter _base_names, _domain_name, _side_names, _face_names, _edge_names, _vertex_names;
extern Parameter _init_transformation, _naming_domain, _naming_section, _naming_side;
extern Parameter _layers, _nbsubdomains;
extern Parameter _nboctants, _angle, _angle1, _angle2, _type;
extern Parameter _shape, _generator, _pattern, _split_direction, _refinement_depth, _default_hstep;
extern Parameter _dim, _domain, _extension_domain, _extension_domain_v, _FE_type, _FE_subtype, _Sobolev_type, _conformity, _order, _interpolation;
extern Parameter _optimizeNumbering, _notOptimizeNumbering, _withLocateData, _withoutLocateData;
extern Parameter _basis_dim;
extern Parameter _rank;
extern Parameter _method, _quad, _quad1, _quad2, _order1, _order2, _bound, _isogeo, _symmetry, _computation, _functionPart;
extern Parameter _cluster, _row_cluster, _col_cluster, _min_block_size, _clustering_method, _approximation_method, _max_rank, _threshold, _eta;
extern Parameter _compute, _notCompute, _assembled, _unassembled, _storage, _nodal, _reduction;
extern Parameter _pseudoReductionMethod, _realReductionMethod, _penalizationReductionMethod;
extern Parameter _maxIt, _krylovDim, _solver, _tolerance;
extern Parameter _convToStd, _forceNonSym, _ncv, _nev, _mode, _sigma, _sort, _which;
extern Parameter _omega, _name;
extern Parameter _format, _data_name, _aFilePerDomain, _aUniqueFile, _encodingFileName, _noEncodingFileName;
extern Parameter _vizir4_solType;
extern Parameter _transformation, _suffix;

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

extern Parameter _residue, _nbIterations;

class ArpackProb;
extern ArpackProb _ARprob;

class SymbolicFunction;
extern SymbolicFunction x_1, x_2, x_3, x_4, r_, theta_, phi_;

class ThreadData;
extern ThreadData theThreadData;

template <typename T> class Vector;
extern Vector<real_t> voidVector;

enum AngleUnitType {_deg, _rad};
class AngleUnit;
extern AngleUnit deg_;  // to convert deg to rad
extern AngleUnit rad_;  // to convert deg to rad

//------------------------------------------------------------------------------
// Unique output file stream
//------------------------------------------------------------------------------

//extern std::ofstream thePrintStream;
class PrintStream;
class CoutStream;
extern PrintStream thePrintStream;
extern std::stringstream theStringStream;
extern PrintStream theLogStream;
extern CoutStream theCout;
extern string_t thePrintFile;
extern string_t theLogFile;
extern string_t theLogGmshFile;

//------------------------------------------------------------------------------
// enum's used as types or as consts in user's main or functions
// by default items of enum begin by _ except for end user's enum
// enum are listed by library alphabetically
//------------------------------------------------------------------------------

enum KeepStatus { _keep, _nokeep };

// --------- arpackSupport ---------

// --------- eigenSolvers ----------

// ----- essentialConditions -------

//! type of essential condition
enum EcType { _undefEcType,_DirichletEc, _transmissionEc, _crackEc, _periodicEc, _meanEc, _lfEc, _NeumannEc, _FourierEc };
typedef EcType BcType;

//! method to translate essential condition to constraints
enum ECMethod { _undefECMethod, _dofEC, _internalNodeEC, _momentEC };

//! reduction method to deal with essential conditions
enum ReductionMethodType
{
  _noReduction = 0,
  _pseudoReduction,
  _realReduction,
  _penalizationReduction,
  _dualReduction
};

// ----------- finalize ------------

// -------- finiteElements ---------

/*!
    interpolation type :
    - P0, ... P10, Q0, ... Q10 stands for Lagrange FE interpolation
    - name nomenclature of edge/face element
                              2D  (triangle)                     3D (tetrahedron)
      FE face type (Hdiv)  Raviart-Thomas(RT)            Nedelec Face first family (NF1)
                           Brezzi-Douglas-Marini(BDM)    Nedelec Face second family(NF2)
      FE edge type (Hrot)  Nedelec first family (N1)     Nedelec Edge first family (NE1)
                           Nedelec second family(N2)     Nedelec Edge second family(NE2)

    \note according to the previous table, there is an equivalence between 2D and 3D shortname:
          for instance NF_1 in 2D is understood as RT_1 and RT_1 in 3D is understood as NF_1
*/
enum InterpolationType
{
  _P0 = 0, P0 = _P0,
  _P1, P1 = _P1,
  _P2, P2 = _P2,
  _P3, P3 = _P3,
  _P4, P4 = _P4,
  _P5, P5 = _P5,
  _P6, P6 = _P6,
  _P7, P7 = _P7,
  _P8, P8 = _P8,
  _P9, P9 = _P9,
  _P10, P10 = _P10,
  _P1BubbleP3, P1BubbleP3 = _P1BubbleP3,

  _CR, CR=_CR,

  _Q0, Q0 = _Q0,
  _Q1, Q1 = _Q1,
  _Q2, Q2 = _Q2,
  _Q3, Q3 = _Q3,
  _Q4, Q4 = _Q4,
  _Q5, Q5 = _Q5,
  _Q6, Q6 = _Q6,
  _Q7, Q7 = _Q7,
  _Q8, Q8 = _Q8,
  _Q9, Q9 = _Q9,
  _Q10, Q10 = _Q10,

  _RT_1, RT_1 = _RT_1, _NF1_1 = _RT_1, NF1_1 = _RT_1,
  _RT_2, RT_2 = _RT_2, _NF1_2 = _RT_2, NF1_2 = _RT_2,
  _RT_3, RT_3 = _RT_3, _NF1_3 = _RT_3, NF1_3 = _RT_3,
  _RT_4, RT_4 = _RT_4, _NF1_4 = _RT_4, NF1_4 = _RT_4,
  _RT_5, RT_5 = _RT_5, _NF1_5 = _RT_5, NF1_5 = _RT_5,
  _BDM_1, BDM_1 = _BDM_1, _NF2_1 = _BDM_1, NF2_1 = _BDM_1,
  _BDM_2, BDM_2 = _BDM_2, _NF2_2 = _BDM_2, NF2_2 = _BDM_2,
  _BDM_3, BDM_3 = _BDM_3, _NF2_3 = _BDM_3, NF2_3 = _BDM_3,
  _BDM_4, BDM_4 = _BDM_4, _NF2_4 = _BDM_4, NF2_4 = _BDM_4,
  _BDM_5, BDM_5 = _BDM_5, _NF2_5 = _BDM_5, NF2_5 = _BDM_5,

  _N1_1, N1_1 = _N1_1, _NE1_1 = _N1_1, NE1_1 = _N1_1,
  _N1_2, N1_2 = _N1_2, _NE1_2 = _N1_2, NE1_2 = _N1_2,
  _N1_3, N1_3 = _N1_3, _NE1_3 = _N1_3, NE1_3 = _N1_3,
  _N1_4, N1_4 = _N1_4, _NE1_4 = _N1_4, NE1_4 = _N1_4,
  _N1_5, N1_5 = _N1_5, _NE1_5 = _N1_5, NE1_5 = _N1_5,
  _N2_1, N2_1 = _N2_1, _NE2_1 = _N2_1, NE2_1 = _N2_1,
  _N2_2, N2_2 = _N2_2, _NE2_2 = _N2_2, NE2_2 = _N2_2,
  _N2_3, N2_3 = _N2_3, _NE2_3 = _N2_3, NE2_3 = _N2_3,
  _N2_4, N2_4 = _N2_4, _NE2_4 = _N2_4, NE2_4 = _N2_4,
  _N2_5, N2_5 = _N2_5, _NE2_5 = _N2_5, NE2_5 = _N2_5
};

//! finite element subfamily
enum FESubType
{
  _standard = 0, standard = _standard,
  _GaussLobattoPoints, GaussLobattoPoints = _GaussLobattoPoints,
  _firstFamily, firstFamily = _firstFamily,
  _secondFamily, secondFamily = _secondFamily
};

//! finite element family
enum FEType
{
  _Lagrange, Lagrange = _Lagrange,
  _Hermite, Hermite = _Hermite,
  _CrouzeixRaviart, CrouzeixRaviart = _CrouzeixRaviart,
  _Nedelec, Nedelec = _Nedelec,
  _RaviartThomas, RaviartThomas = _RaviartThomas,
  _NedelecFace, NedelecFace = _NedelecFace,
  _NedelecEdge, NedelecEdge = _NedelecEdge,
  _BuffaChristiansen, BuffaChristiansen=_BuffaChristiansen,
  _Morley, Morley = _Morley, _Argyris, Argyris = _Argyris
};

//! regular/singular part
enum FunctionPart {_allFunction=0, allFunction=_allFunction, _regularPart, regularPart=_regularPart, _singularPart, singularPart=_singularPart};

//! integration methods
enum IntegrationMethodType
{
  _undefIM, _quadratureIM, _polynomialIM, _productIM,
  _LenoirSalles2dIM, LenoirSalles2d =_LenoirSalles2dIM, _LenoirSalles3dIM, LenoirSalles3d= _LenoirSalles3dIM, _LenoirSalles2dIR, _LenoirSalles3dIR,
  _SauterSchwabIM, SauterSchwab=_SauterSchwabIM, _SauterSchwabSymIM, SauterSchwabSym=_SauterSchwabSymIM,
  _DuffyIM, Duffy = _DuffyIM, _DuffySymIM, Duffy_sym = _DuffySymIM, _HMatrixIM, H_Matrix= _HMatrixIM, _CollinoIM, _FilonIM
};

//! quadrature rules
enum QuadRule
{
  _defaultRule = 0, defaultQuadrature = _defaultRule,
  _GaussLegendreRule, GaussLegendre = _GaussLegendreRule,
  _symmetricalGaussRule, symmetricalGauss = _symmetricalGaussRule,
  _GaussLobattoRule, GaussLobatto = _GaussLobattoRule,
  _nodalRule, nodalQuadrature = _nodalRule,
  _miscRule, miscQuadrature = _miscRule,
  _GrundmannMollerRule, GrundmannMoller = _GrundmannMollerRule,
  _doubleQuadrature,
  _evenGaussLegendreRule, _evenGaussLobattoRule
};

//! Sobolev space
enum SobolevType
{
  _L2 = 0, L2 = _L2,
  _H1, H1 = _H1,
  _Hdiv, Hdiv = _Hdiv,
  _Hcurl, Hcurl = _Hcurl, _Hrot = _Hcurl, Hrot = _Hrot,
  _H2, H2 = _H2,
  _Hinf, Hinf = _Hinf, Linf=_Hinf
};

// ------------- form --------------

//! type of linear form
enum LinearFormType
{
  _undefLf = 0,
  _intg,
  _doubleIntg,
  _bilinearAsLinear,
  _linearCombination,
  _composedLf,
  _explicitLf,
  _userLf
};

//! type of computation
enum ComputationType
{
  _undefComputation=0,
  _FEComputation, FEComputation=_FEComputation,
  _IEComputation, IEComputation=_IEComputation,
  _SPComputation, SPComputation=_SPComputation,
  _FESPComputation, FESPComputation=_FESPComputation,
  _IESPComputation, IESPComputation=_IESPComputation,
  _FEextComputation, FEextComputation=_FEextComputation,
  _IEextComputation, IEextComputation=_IEextComputation,
  _IEHmatrixComputation, IEHmatrixComputation=_IEHmatrixComputation,
  _DGComputation, DGComputation=_DGComputation
};

// ----------- geometry ------------

//! geometrical transformations
enum TransformType
{ _noTransform, _translation, _rotation2d, _rotation3d, _homothety, _scaling, _ptReflection, _reflection2d, _reflection3d, _composition, _explicitLinear };

//! type of crack
enum CrackType
{
  _noCrack,
  _openCrack,
  _closedCrack
};

//! used for RevTrunk class in lib geometry
enum GeometricEndShape
{
  _gesNone, gesNone=_gesNone,
  _gesFlat, gesFlat=_gesFlat,
  _gesCone, gesCone=_gesCone,
  _gesEllipsoid, gesEllipsoid=_gesEllipsoid,
  _gesSphere, gesSphere=_gesSphere
};

//! enum to describe the relative position of 2 geometries
enum GeometryRelationType
{
  _inside=0,
  _contains,
  _intersects,
  _outside
};

//! I/O mesh format
enum IOFormat
{
  _undefFormat=0,
  _vtk, vtk=_vtk,
  _vtu, vtu=_vtu,
  _msh, msh=_msh,
  _geo, geo=_geo,
  _mel, mel=_mel,
  _ply, ply=_ply,
  _medit, medit=_medit,
  _vizir4, vizir=_vizir4,
  _matlab, matlab=_matlab,
  _raw, raw=_raw,
  _xyzv, xyzv=_xyzv
};

//! enum to select the mesh generator/algorithm
enum MeshGenerator
{
  _defaultGenerator=0, _defaultPattern=_defaultGenerator,
  _structured, structured=_structured,
  _subdiv, subdiv=_subdiv,
  _gmsh, gmsh=_gmsh, _unstructured=_gmsh, unstructured=_gmsh,
  _gmshOC, gmshOC=_gmshOC,
  _fromParametrization, fromParametrization=_fromParametrization
};
typedef MeshGenerator MeshPattern;

//! enum to select the mesh generator/algorithm
enum MeshOption
{
  _defaultMeshOption=0,
  _unstructuredMesh, unstructuredMesh=_unstructuredMesh,
  _structuredMesh, structuredMesh=_structuredMesh,
  _leftSplit, leftSplit=_leftSplit,
  _rightSplit, rightSplit=_rightSplit,
  _randomSplit, randomSplit=_randomSplit,
  _alternateSplit, alternateSplit=_alternateSplit,
  _crossSplit, crossSplit = _crossSplit

};

//! enum to select the mesh generator/algorithm
enum StructuredMeshSplitRule
{
  _noSplitRule=0,
  _left, left=_left,
  _right, right=_right,
  _random, random=_random,
  _alternate, alternate=_alternate,
  _cross, cross = _cross
};

//! orientation type for a spacedim-1 domain
enum OrientationType
{
  _undefOrientationType,
  _towardsInfinite,towardsInfinite=_towardsInfinite,
  _outwardsInfinite, outwardsInfinite=_outwardsInfinite,
  _towardsDomain, towardsDomain=_towardsDomain,
  _outwardsDomain, outwardsDomain=_outwardsDomain
};

//! geometrical shapes
enum ShapeType
{
  _noShape=0,
  _fromFile,
  _point,
  _segment, segment=_segment,
  _triangle, triangle=_triangle,
  _quadrangle, quadrangle=_quadrangle,
  _tetrahedron, tetrahedron=_tetrahedron,
  _hexahedron, hexahedron=_hexahedron,
  _prism, prism=_prism,
  _pyramid, pyramid=_pyramid,
  _ellArc, _circArc, _splineArc, _parametrizedArc, _parametrizedSurface, _splineSurface,
  _polygon, _parallelogram, _rectangle, _square, _ellipse, _disk, _ellipticSector, _circularSector, _ellipsoidSidePart, _sphereSidePart,
  _setofpoints, _setofelems, _trunkSidePart, _cylinderSidePart, _coneSidePart,
  _polyhedron, _parallelepiped, _cuboid, _cube, _ellipsoid, _ball, _trunk,
  _revTrunk, _cylinder, _revCylinder, _cone, _revCone,
  _composite, _loop, _extrusion, _ocShape
};

//! geometrical shapes
enum ShapesType
{
  _noShapes=0,
  _fromFiles,
  _points,
  _segments, segments=_segments,
  _triangles, triangles=_triangles,
  _quadrangles, quadrangles=_quadrangles,
  _tetrahedra, tetrahedra=_tetrahedra,
  _hexahedra, hexahedra,
  _prisms, prisms=_prisms,
  _pyramids, pyramids=_pyramids,
  _ellArcs, _circArcs, _parametrizedArcs,
  _polygons, _parallelograms, _rectangles, _squares, _ellipses, _disks, _ellipticSectors, _circularSectors, _ellipsoidSideParts, _sphereSideParts,
  _setsofpoints, _setsofelems, _trunkSideParts, _cylinderSideParts, _coneSideParts,
  _polyhedra, _parallelepipeds, _cuboids, _cubes, _ellipsoids, _balls, _trunks,
  _revTrunks, _cylinders, _revCylinders, _cones, _revCones,
  _composites, _loops, _extrusions
};

//! Open Cascade shape
enum OCShapeType
{
  _undefOCShape, _OCSolid, _solid=_OCSolid, _OCShell, _shell=_OCShell, _OCFace, _face=_OCFace,
  _OCWire, _wire=_OCWire, _OCEdge, _edge=_OCEdge, _OCVertex, _vertex=_OCVertex
};

// ------------- hierarchicalMatrix --------------

enum ClusteringMethod
{
  _noClusteringMethod=0,
  _regularBisection, regularBisection=_regularBisection,
  _boundingBoxBisection, boundingBoxBisection=_boundingBoxBisection,
  _cardinalityBisection, cardinalityBisection=_cardinalityBisection,
  _uniformKdtree, uniformKdtree=_uniformKdtree,
  _nonuniformKdtree, nonuniformKdtree=_nonuniformKdtree
};

// ------------- init --------------

enum DataAccess { _copy = 0 , _view };

/*!
  Enumerated list of matrix properties regarding invertibility
*/
enum MatrixConditioning
{
  _well_conditioned_matrix =0,
  _bad_conditioned_matrix ,
  _non_invertible_matrix
};

// ---------- largeMatrix ----------

//! access type of storage
enum AccessType
{
  _noAccess = 0,
  _sym,
  _row,
  _col,
  _dual
};

//! factorization of matrices
enum FactorizationType {_noFactorization, _lu, _ldlt, _ldlstar, _llt, _llstar, _qr,
                                         _ilu, _ildlt, _ildlstar, _illt, _illstar, _umfpack};

//! part of matrices
enum MatrixPart
{
  _all = 0,
  _lower,
  _upper
};

//! storage of matrices
enum StorageType
{
  _noStorage = 0,
  _dense, dense=_dense,
  _cs, cs=_cs,
  _skyline, skyline=_skyline,
  _coo, coo=_coo,
  _hmatrix, hmatrix=_hmatrix
};

//! short names of storage and access
enum StorageAccessType {
  _csRow=0, csRow=_csRow,
  _csCol, csCol=_csCol,
  _csDual, csDual=_csDual,
  _csSym, csSym=_csSym,
  _denseRow, denseRow=_denseRow,
  _denseCol, denseCol=_denseCol,
  _denseDual, denseDual=_denseDual,
  _skylineSym, skylineSym=_skylineSym,
  _skylineDual, skylineDual=_skylineDual
};

//! storage build type
enum StorageBuildType
{
  _undefBuild=0,
  _feBuild,
  _dgBuild,
  _diagBuild,
  _ecBuild,
  _globalBuild,
  _otherBuild
};

//! structure of matrices and vectors
enum StrucType {_scalar, _vector, _matrix, _vectorofvector, _vectorofmatrix, _matrixofmatrix, _undefStrucType};

//! symmetry of matrices
enum SymType {
  _noSymmetry = 0, noSymmetry=_noSymmetry,
  _symmetric, symmetric=_symmetric,
  _skewSymmetric, skewSymmetric=_skewSymmetric,
  _selfAdjoint, selfAdjoint=_selfAdjoint,
  _skewAdjoint, skewAdjoint=_skewAdjoint,
  _diagonal, diagonal=_diagonal,
  _undefSymmetry, undefSymmetry=_undefSymmetry};

// -------- mathsResources ---------

// ----------- operator ------------

//! type fo differential operator
enum DiffOpType
{
  _id, _d0, _dt = _d0, _d1, _dx = _d1, _d2, _dy = _d2, _d3, _dz = _d3,
  _grad, _nabla = _grad, _div, _curl, _rot = _curl,
  _gradS, _nablaS = _gradS, _divS, _curlS, _rotS = _curlS, _scurlS, _srotS =_scurlS,
  _ntimes, _timesn, _ndot, _ncross, _ncrossncross, _ndotgrad, _ndiv,
  _ncrosscurl, _ncrossgrad, _ncrossntimes, _timesncrossn, _ntimesndot,
  _divG, _gradG, _nablaG = _gradG, _curlG, _rotG = _curlG, _epsilon, _epsilonG, _epsilonR, _voigtToM,
  _grad_x, _nabla_x = _grad_x,
  _grad_y, _nabla_y = _grad_y,
  _grad_xy, _nabla_xy =_grad_xy,
  _div_x, _div_y, _div_xy,
  _curl_x, _rot_x = _curl_x,
  _curl_y, _rot_y = _curl_y,
  _curl_xy, _rot_xy = _curl_xy,
  _trac_x, _trac_y, _trac_xy, _nxtensornxtimesNGr, _nxtensornxtimestrac_y,
  _ntimes_x, _timesn_x, _ndot_x, _ncross_x, _ncrossncross_x, _ncrossntimes_x, _timesncrossn_x, _ndotgrad_x, _ndiv_x, _ncrosscurl_x,
  _ntimes_y, _timesn_y, _ndot_y, _ncross_y, _ncrossncross_y, _ncrossntimes_y, _timesncrossn_y, _ndotgrad_y, _ndiv_y, _ncrosscurl_y,
  _ndotgrad_xy,
  _nxdotny_times, _nxcrossny_dot, _nycrossnx_dot, _nxcrossny_cross, _nycrossnx_cross, _nxcrossny_times, _nycrossnx_times,
  _dt2,_d11,_dxx=_d11, _d22, _dyy=_d22, _d33, _dzz=_d33, _d12, _dxy=_d12, _d21, _dyx=_d21,_d13, _dxz=_d13, _d31, _dzx=_d31,_d23,
  _dyz=_d23, _d32, _dzy=_d32,_lap, _lapG, _d2G,
  _dt3,_d111, _dxxx=_d111, _d222, _dyyy=_d222, _d333,_dzzz=_d333,_d112,_dxxy=_d112, _d113, _dxxz=_d113, _d122, _dxyy=_d122,_d123, _dxyz=_d123,
  _d133, _dxzz=_d133, _d223, _dyyz=_d223, _d233, _dyzz=_d223
};

//! continuity order
//! C0 : only geometric continuity,
//! C1 : continuity of the first derivative all along the Curve,
//! C2 : continuity of the second derivative all along the Curve,
//! C3 : continuity of the third derivative all along the Curve,
//! G1 : tangency continuity all along the Curve,
//! G2 : curvature continuity all along the Curve,
//! Cinf: the order of continuity is infinite.
enum ContinuityOrder { _notRegular=0, _regC0, _regC1, _regC2, _regC3, _regG1, _regG2, _regCinf };

//! type of unitary vector
enum UnitaryVector { _n, _nx, _ny, _nxdotny, _nxcrossny, _nycrossnx, _ncrossn, _tau, _taux, _tauy, _btau, _btaux, _btauy };

//! type of jump
enum JumpType { _nojump, _jump, _mean };

//! variable symbolic name
enum VariableName { _varUndef=0,_x,_x1=_x,_y,_x2=_y, _z, _x3=_z, _t, _x4=_t,_xy, _yx, _xx, _yy, _r,_theta, _phi };

//! symbolic operations
enum SymbolicOperation
{
  _idop=0, _plus, _minus, _multiply, _divide, _power, _atan2,                          //binary op
  _equal, _different, _less, _lessequal, _greater, _greaterequal, _and, _or,           //binary op
  _abs, _sign, _realPart, _imagPart, _sqrt, _squared, _sin, _cos, _tan, _asin, _acos,  //unary op
  _atan, _sinh, _cosh, _tanh, _asinh, _acosh, _atanh, _exp, _log, _log10, _pow, _not,
  _conj, _adj, _tran, _inv
};

// ----------- solvers -------------

//! preconditioner
enum PreconditionerType { _noPrec, _luPrec, _ldltPrec, _ldlstarPrec, _ssorPrec, _diagPrec, _productPrec,
                                   _iluPrec, _ildltPrec, _ildlstarPrec, _illtPrec, _userPrec };

// ------------ space --------------

//! DoF type
enum DofType { _feDof, _spDof, _otherDof };

//! space type
enum SpaceType { _feSpace, _spSpace, _subSpace, _prodSpace };

//! DoF support type
enum SupportType { _undefSupport = 0, _pointSupport, _faceSupport, _edgeSupport, _elementSupport };

//! type of unknown
enum UnknownType { _feUnknown, _spUnknown, _mixedUnknown };

// ------------ term ---------------

//! Enum for reporting the status of a computation
enum ComputationInfo
{
  //! Computation was successful.
  _success = 0,
  //! The provided data did not satisfy the prerequisites.
  _numericalIssue = 1,
  //! Iterative procedure did not converge.
  _noConvergence = 2,
  /*! The inputs are invalid, or the algorithm has been improperly called.
    * When assertions are enabled, such errors trigger an assert. */
  _invalidInput = 3
};

//! computational mode for eigen solvers
enum EigenComputationalMode { _davidson, _krylovSchur,                    // for intern solver
                              _buckling, _cayley, _cshiftRe, _cshiftIm }; // for Arpack

//! Eigen solvers
enum EigenSolverType { _intern, _arpack };

//! Possible choices to sort the eigenvalues
enum EigenSortKind { _decr_module, _decr_realpart, _decr_imagpart, _incr_module, _incr_realpart, _incr_imagpart };

//! Iterative solvers
enum IterativeSolverType { _noIterativeSolver, _cg, _cgs, _qmr, _bicg, _bicgstab, _gmres, _sor, _ssor };

//! type of space projectors
enum ProjectorType
{
  _noProjectorType=0,
  _userProjector, userProjector=_userProjector,
  _L2Projector, L2Projector=_L2Projector,
  _H1Projector, H1Projector=_H1Projector,
  _H10Projector, H10Projector=_H10Projector
};

enum vizir4SolType
{
  _SolAtVertices=0, SolAtVertices=_SolAtVertices,
  _SolAtEdges, SolAtEdges=_SolAtEdges,
  _SolAtTriangles=0, SolAtTriangles=_SolAtTriangles, 
  _SolAtQuadrilaterals, SolAtQuadrilaterals=_SolAtQuadrilaterals,
  _SolAtTetrahedra, SolAtTetrahedra=_SolAtTetrahedra, _SolAtPyramids, _SolAtPrisms,_SolAtHexahedra,
  _HOSolAtEdgesP1, _HOSolAtEdgesP2, _HOSolAtEdgesP3, _HOSolAtEdgesP4,
  _HOSolAtTrianglesP1, _HOSolAtTrianglesP2, _HOSolAtTrianglesP3, _HOSolAtTrianglesP4,
  _HOSolAtQuadrilateralsQ1, _HOSolAtQuadrilateralsQ2, _HOSolAtQuadrilateralsQ3, _HOSolAtQuadrilateralsQ4,
  _HOSolAtTetrahedraP1, _HOSolAtTetrahedraP2, _HOSolAtTetrahedraP3, _HOSolAtTetrahedraP4,
  _HOSolAtPyramidsP1, _HOSolAtPyramidsP2, _HOSolAtPyramidsP3, _HOSolAtPyramidsP4,
  _HOSolAtPrismsP1, _HOSolAtPrismsP2, _HOSolAtPrismsP3, _HOSolAtPrismsP4,
  _HOSolAtHexahedraQ1, _HOSolAtHexahedraQ2, _HOSolAtHexahedraQ3, _HOSolAtHexahedraQ4
};

//! SOR solver type
enum SorSolverType { _diagSorS, _upperSorS, _lowerSorS, _matrixVectorSorS };

//! Enumerated list of available HMatrix approximation method
enum HMApproximationMethod
{
  _noHMApproximation,
  _svdCompression, svdCompression=_svdCompression,
  _rsvdCompression, rsvdCompression=_rsvdCompression,
  _r3svdCompression, r3svdCompression=_r3svdCompression,
  _acaFull, acaFull=_acaFull,
  _acaPartial, acaPartial=_acaPartial,
  _acaPlus, acaPlus=_acaPlus
};

// -------- umfpackSupport ---------

// ------------ utils --------------

//! space or mesh dimension
enum DimensionType { _undefDim=0, _1D, _2D, _3D, _nD };

//! type of function (C++ function or TermVector)
enum FuncFormType { _analytical, _interpolated };

//! type of functions (one Point variable, or two)
enum FunctType  { _function, _kernel };

//! multilingual messages
enum Language
{
  _en=0, en = _en, english = _en,
  _fr, fr = _fr, francais = _fr,
  _de, de = _de, deutsch = _de,
  _es, es = _es, espanol = _es,
  _nbLanguages
};

//! Ptype
enum Ptype
{
  _rec=0, rec = _rec, recursive = _rec,
  _kway, kway = _kway
};
//! Objtype
enum Objtype
{
  _cut=0, cut = _cut,
  _vol, vol = _vol
};
enum Ctype
{
  _rm=0, rm = _rm,
  _shem, shem = _shem
};
enum Iptype
{
  _grow=0, grow = _grow,
  _iprandom, iprandom = _iprandom
};
enum Rtype
{
  _fm=0, fm = _fm,
  _greedy, greedy = _greedy,
  _sep2sided, sep2sided = _sep2sided,
  _sep1sided, sep1sided = _sep1sided
};

enum Gtype
{
   dual = 0,
   nodal
};

//! used for GeomParameter class in lib geometry
enum ParameterKey
{
  _pk_none,
  _pk_lang, _pk_verbose, _pk_nbThreads, _pk_trackingMode, _pk_pushpop, _pk_traceMemory, _pk_isLogged,
  _pk_varnames,
  _pk_vertices, _pk_faces,
  _pk_center, _pk_center1, _pk_center2, _pk_apogee, _pk_origin, _pk_apex,
  _pk_v1, _pk_v2, _pk_v3, _pk_v4, _pk_v5, _pk_v6, _pk_v7, _pk_v8,
  _pk_xmin, _pk_xmax, _pk_ymin, _pk_ymax, _pk_zmin, _pk_zmax,
  _pk_spline, _pk_spline_type, _pk_spline_subtype, _pk_degree, _pk_spline_BC, _pk_tension, _pk_spline_parametrization, _pk_weights,_pk_tangent_0, _pk_tangent_1, _pk_nbu,
  _pk_tmin, _pk_tmax, _pk_parametrization, _pk_partitioning, _pk_nbParts, _pk_shapePartition,
  _pk_partmesh, _pk_ncommon, _pk_ptype, _pk_objtype, _pk_ctype, _pk_iptype, _pk_rtype, _pk_savedoms, _pk_repsortie,
  _pk_length, _pk_xlength, _pk_ylength, _pk_zlength, _pk_radius, _pk_radius1, _pk_radius2, _pk_xradius, _pk_yradius, _pk_zradius,
  _pk_basis, _pk_scale, _pk_dir, _pk_axis, _pk_normal,
  _pk_end_shape, _pk_end1_shape, _pk_end2_shape, _pk_end_distance, _pk_end1_distance, _pk_end2_distance,
  _pk_nnodes, _pk_hsteps,
  _pk_base_names, _pk_domain_name, _pk_side_names, _pk_face_names, _pk_edge_names, _pk_vertex_names, _pk_naming_domain, _pk_naming_section, _pk_naming_side, _pk_init_transformation,
  _pk_layers, _pk_nbsubdomains,
  _pk_nboctants, _pk_angle, _pk_angle1, _pk_angle2, _pk_type,
  _pk_shape, _pk_generator, _pk_pattern, _pk_split_direction, _pk_refinement_depth, _pk_default_hstep,
  _pk_dim, _pk_domain, _pk_extension_domain, _pk_extension_domain_u = _pk_extension_domain, _pk_extension_domain_v, _pk_FE_type, _pk_FE_subtype, _pk_Sobolev_type, _pk_order, _pk_interpolation,
  _pk_optimizeNumbering, _pk_notOptimizeNumbering, _pk_withLocateData, _pk_withoutLocateData,
  _pk_basis_dim,
  _pk_rank,
  _pk_method, _pk_quad, _pk_quad1, _pk_quad2, _pk_order1, _pk_order2, _pk_bound, _pk_isogeo, _pk_symmetry,
  _pk_computation, _pk_function_part,
  _pk_cluster, _pk_row_cluster, _pk_col_cluster, _pk_min_block_size, _pk_clustering_method, _pk_approximation_method, _pk_max_rank, _pk_threshold, _pk_eta,
  _pk_compute, _pk_notCompute, _pk_assembled, _pk_unassembled, _pk_storage, _pk_nodal,
  _pk_reduction, _pk_pseudoReductionMethod, _pk_realReductionMethod, _pk_penalizationReductionMethod,
  _pk_maxIt, _pk_krylovDim, _pk_solver, _pk_tolerance,
  _pk_convToStd, _pk_forceNonSym, _pk_ncv, _pk_nev, _pk_mode, _pk_sigma, _pk_sort, _pk_which,
  _pk_omega, _pk_name,
  _pk_format, _pk_data_name, _pk_aFilePerDomain, _pk_aUniqueFile, _pk_encodingFileName, _pk_noEncodingFileName,
  _pk_vizir4_solType,
  _pk_transformation, _pk_suffix
};

//! types of values of matrices and vectors
enum ValueType
{
  _none, _integer, _bool, _real, _complex, _string, _pt, _pointer,
  _integerVector, _boolVector, _realVector, _complexVector, _stringVector, _ptVector,
  _integerMatrix, _boolMatrix, _realMatrix, _complexMatrix, _stringMatrix, _ptMatrix,
  _pointerClusterTreeFeDof, _pointerGeomDomain, _pointerFunction, _pointerIntegrationMethod, _pointerIntegrationMethods, _pointerParametrization, _pointerSpline, _pointerTermVectors, _pointerTransformation,
  _enumClusteringMethod, _enumComputationType, _enumCtype, _enumDimensionType, _enumEigenComputationalMode, _enumEigenSolverType, _enumEigenSortKind, _enumFESubType, _enumFEType, _enumFunctionPart, _enumGeometricEndShape, _enumGtype, _enumHMApproximationMethod, _enumIntegrationMethodType, _enumInterpolationType, _enumIOFormat, _enumIptype, _enumIterativeSolverType, _enumLanguage, _enumMeshGenerator, _enumObjtype, _enumPartitioning, _enumPtype, _enumQuadRule, _enumReductionMethodType, _enumRtype, _enumShapeType, _enumSobolevType, _enumSplineBC, _enumSplineParametrization, _enumSplineSubtype, _enumSplineType, _enumStorageAccessType, _enumStructuredMeshSplitRule, _enumStorageType, _enumSymType,
  _enumTransformType
};

//! type of partitioning
enum Partitioning
{
  _nonePartition, _linearPartition, _splinePartition
};

//! type of spline
enum SplineType
{
  _noSpline, _C2Spline, _CatmullRomSpline, _BSpline, _BezierSpline, _Nurbs
};

//! subtype of spline
enum SplineSubtype
{
  _noSplineSubtype, _SplineInterpolation, _SplineApproximation
};

//! spline parametrization type
enum SplineParametrization
{
  _undefParametrization, _xParametrization, _uniformParametrization,_chordalParametrization, _centripetalParametrization
};

//! spline boundary condition
enum SplineBC
{
  _undefBC, _naturalBC, _clampedBC, _periodicBC
};

//! type of calculation in Fock, Malyuzhinets function
enum CalType
{
  _defaultCal, _filonCal, _trapezeCal, _approxCal, _interpCal, _laguerreCal, _adaptiveCal
};
//------------------------------------------------------------------------------
// macro definition for decorated Fortran routines name called in C/C++
//------------------------------------------------------------------------------
#ifdef NO_TRAILING_UNDERSCORE
  // IBM xlf compiler version under AIX / Mac OS X(Darwin)
  #define FTNAME(x) x
#else
  // Almost all other compilers append an underscore to Fortran routine name
  #define FTNAME(x) x ## _
#endif /* NO_TRAILING_UNDERSCORE */

} //end of namespace xlifepp

#endif /* CONFIG_HPP */
