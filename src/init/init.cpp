/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file init.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 10 dec 2002
  \date 23 dec 2011

  \brief Implementation of init function
 */

#include "init.hpp"
#include "utils.h"

#include <fstream>
#include <iostream>
#include <cstdio>
#include <ctime>

namespace xlifepp
{

//! initialize execution of XLiFE++
void initBuild(Language lang, number_t verboseLevel, int_t nbThreads, bool trackingMode, bool pushpop, bool traceMemory, bool isLogged,
               const std::vector<string_t>& syskeys, const std::vector<string_t>& authorizedExtensions)
{
  // Rebuild environment and localized strings
  theEnvironment_p->language(lang);
  theEnvironment_p->rebuild();
  if (syskeys.size() > 0) theEnvironment_p->systemKeys(syskeys);
  if (authorizedExtensions.size() > 0) theEnvironment_p->authorizedSaveToFileExtensions(authorizedExtensions);

  // Initialize function tracing
  Trace::trackingMode=trackingMode;
  if (isLogged) { Trace::logOn(); }
  else { Trace::logOff(); }
  Trace::disablePushPop=!pushpop;
  Trace::traceMemory=traceMemory;

  // Initialize time
  timerInit();

  //initialize thread global vectors
  number_t nt=numberOfThreads(nbThreads);  //current number of threads
  theThreadData.resize(nt);

  // Initialize output messages files for info/warning/error handling
  msgInit(theEnvironment_p->msgFilePath(), thePrintStream[0]);

  // Initialize static variables
  //Point::tolerance = theTolerance;
  Point::tolerance =theEpsilon;

  // initialize verbose level
  theVerboseLevel=verboseLevel;

  // Initialize RTI names of function types
  Function::initReturnTypes();

  // Initialize RTI names of value types (in lib operator for the moment)
  Value::valueTypeRTINamesInit();

  // Display logo (!) and header information on console
  theEnvironment_p->printHeader(*theStartTime_p);

  // Open Print File and write header information
  theEnvironment_p->printHeader(thePrintStream[0]);

  //initialize random generator
  initRandomGenerators(time(nullptr));

  trace_p->pop();
#ifdef XLIFEPP_WITH_OPENCASCADE
  info("opencascade_compatibility");
#endif
}

void initRandomGenerators(int seed)
{
  theRandomSeed = seed;
  srand(theRandomSeed);
  #if __cplusplus >= 201103L
  theRandomEngine.seed(theRandomSeed);  // initilize Mersenne Twister 19937 generator
  #endif
}

void init(const std::vector<Parameter>& ps, int argc, char** argv)
{
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_lang);
  params.insert(_pk_verbose);
  params.insert(_pk_trackingMode);
  params.insert(_pk_traceMemory);
  params.insert(_pk_pushpop);
  params.insert(_pk_nbThreads);
  params.insert(_pk_isLogged);
  Language lang;
  number_t verboseLevel=1;
  int_t nbThreads=-1;
  bool trackingMode=false, traceMemory=false, pushpop=true, isLogged=false;

  // Don't init twice !
  if ( Environment::running() ) { return; }

  // Pre-initialize environment and english messages
  theEnvironment_p = new Environment(_en);
  thePrintStream.open(thePrintFile.c_str());
  theLogStream.open(theLogFile.c_str());
  theCout.open(thePrintStream, theStringStream);
  std::remove(theLogGmshFile.c_str());
  traceInit();
  msgInit(theEnvironment_p->msgFilePath(), thePrintStream[0]);

  for (number_t i=0; i < ps.size(); ++i)
  {
    ParameterKey key=ps[i].key();
    switch (key)
    {
      case _pk_lang:
      {
        switch (ps[i].type())
        {
          case _integer:
          case _enumLanguage:
            lang=Language(ps[i].get_n());
            break;
          default: error("param_badtype", "Language", "_lang");
        }
        break;
      }
      case _pk_verbose:
      {
        switch (ps[i].type())
        {
          case _integer: verboseLevel=ps[i].get_n(); break;
          default: error("param_badtype", "int", "_verbose");
        }
        break;
      }
      case _pk_trackingMode:
      {
        switch (ps[i].type())
        {
          case _integer: trackingMode=bool(ps[i].get_n()); break;
          case _bool: trackingMode=ps[i].get_b(); break;
          default: error("param_badtype", "bool/int", "_trackingMode");
        }
        break;
      }
      case _pk_traceMemory:
      {
        switch (ps[i].type())
        {
          case _integer: traceMemory=bool(ps[i].get_n()); break;
          case _bool: traceMemory=ps[i].get_b(); break;
          default: error("param_badtype", "bool/int", "_traceMemory");
        }
        break;
      }
      case _pk_pushpop:
      {
        switch (ps[i].type())
        {
          case _integer: pushpop=bool(ps[i].get_n()); break;
          case _bool: pushpop=ps[i].get_b(); break;
          default: error("param_badtype", "bool/int", "_pushpop");
        }
        break;
      }
      case _pk_nbThreads:
      {
        switch (ps[i].type())
        {
          case _integer: nbThreads=ps[i].get_i(); break;
          default: error("param_badtype", "int", "_nbThreads");
        }
        break;
      }
      case _pk_isLogged:
      {
        switch (ps[i].type())
        {
          case _integer: isLogged=bool(ps[i].get_n()); break;
          case _bool: isLogged=ps[i].get_b(); break;
          default: error("param_badtype", "bool", "_isLogged");
        }
        break;
      }
      default: error("init_unexpected_parameter");
    }
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) error("unexpected_parameter", words("param key", key));
      else error("param_already_used", words("param key", key));
    }
    usedParams.insert(key);
  }
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p=params.begin(); it_p != params.end(); ++it_p)
  {
    switch (*it_p)
    {
      case _pk_lang: lang=_en; break;
      case _pk_verbose: verboseLevel=1; break;
      case _pk_trackingMode: trackingMode=false; break;
      case _pk_traceMemory: traceMemory=false; break;
      case _pk_pushpop: pushpop=true; break;
      case _pk_nbThreads: nbThreads=-1; break;
      case _pk_isLogged: isLogged=trackingMode; break;
      default: error("unexpected_parameter", words("param key", *it_p));
    }
  }
  std::vector<string_t> syskeys;

  if (argc > 1)
  {
    // management of command line options
    // -l <lang>, -lang <lang>, --lang <lang>
    // -vl <num>, -verbose-level <num>, --verbose-level <num>
    // -v, -vv, -vvv
    // -j <num>, -jobs <num>, --jobs <num>
    std::vector<string_t> clargs(argc-1);
    for (number_t i=1; i < argc; ++i) { clargs[i-1]=argv[i]; }

    while (clargs.size() > 0)
    {
      if (clargs[0] == "-h")
      {
        syskeys.push_back("-h");
        std::cout << "Command line options for XLiFE++ executables:" << std::endl
                  << "  -h                               prints the current help" << std::endl
                  << "  -j <num>, -jobs <num>,           define the number of threads. Default is -1," << std::endl
                  << "  --jobs <num>                     meaning automatical behavior." << std::endl
                  << "  -l <string>, -lang <string>,     define the language used for messages." << std::endl
                  << "  --lang <string>                  Default is en." << std::endl
                  << "  -vl <num>, -verbose-level <num>, define the verbose level." << std::endl
                  << "  --verbose-level <num>            Default is 0." << std::endl
                  << "  -v                               set the verbose level to 1." << std::endl
                  << "  -vv                              set the verbose level to 2." << std::endl
                  << "  -vvv                             set the verbose level to 3." << std::endl;
        exit(0);
      }
      else if (clargs[0] == "-l" || clargs[0] == "-lang" || clargs[0] == "--lang")
      {
        syskeys.push_back("-l");
        syskeys.push_back("-lang");
        syskeys.push_back("--lang");
        bool langFound=false;
        for (number_t i=0; i < _nbLanguages; ++i)
        {
          if (words("language", i) == clargs[1])
          {
            langFound=true;
            lang=Language(i);
          }
        }
        if (!langFound) error("unexpected_parameter", clargs[1]);
        clargs.erase(clargs.begin(), clargs.begin()+2);
      }
      else if (clargs[0] == "-vl" || clargs[0] == "-verbose-level" || clargs[0] == "--verbose-level")
      {
        syskeys.push_back("-vl");
        syskeys.push_back("-verbose-level");
        syskeys.push_back("--verbose-level");
        verboseLevel=atoi(clargs[1].c_str());
        clargs.erase(clargs.begin(), clargs.begin()+2);
      }
      else if (clargs[0] == "-v")
      {
        syskeys.push_back("-v");
        verboseLevel=1;
        clargs.erase(clargs.begin());
      }
      else if (clargs[0] == "-vv")
      {
        syskeys.push_back("-vv");
        verboseLevel=2;
        clargs.erase(clargs.begin());
      }
      else if (clargs[0] == "-vvv")
      {
        syskeys.push_back("-vvv");
        verboseLevel=3;
        clargs.erase(clargs.begin());
      }
      else if (clargs[0] == "-j" || clargs[0] == "-jobs" || clargs[0] == "--jobs")
      {
        syskeys.push_back("-j");
        syskeys.push_back("-jobs");
        syskeys.push_back("--jobs");
        nbThreads=atoi(clargs[1].c_str());
        clargs.erase(clargs.begin(), clargs.begin()+2);
      }
      else
      {
        // user program may deal with other options directly, so we just scan next argv
        clargs.erase(clargs.begin());
        //error("unexpected_parameter", clargs[0]);
      }
    }
  }
  std::vector<string_t> authorizedExtensions={"m", "msh", "geo", "ply", "mel", "mesh", "vtk", "vtu", "xyzv", "sol","txt"};
  initBuild(lang, verboseLevel, nbThreads, trackingMode, pushpop, traceMemory, isLogged, syskeys, authorizedExtensions);
}

void init()
{
  std::vector<Parameter> ps(0);
  init(ps);
}

void init(const Parameter& p1)
{
  std::vector<Parameter> ps(1,p1);
  init(ps);
}

void init(const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps(2);
  ps[0]=p1; ps[1]=p2;
  init(ps);
}

void init(const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps(3);
  ps[0]=p1; ps[1]=p2; ps[2]=p3;
  init(ps);
}

void init(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps(4);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4;
  init(ps);
}

void init(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps(5);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5;
  init(ps);
}

void init(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  std::vector<Parameter> ps(6);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5; ps[5]=p6;
  init(ps);
}

void init(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
          const Parameter& p7)
{
  std::vector<Parameter> ps(7);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5; ps[5]=p6; ps[6]=p7;
  init(ps);
}

void init(int argc, char** argv)
{
  std::vector<Parameter> ps(0);
  init(ps, argc, argv);
}

void init(int argc, char** argv, const Parameter& p1)
{
  std::vector<Parameter> ps(1,p1);
  init(ps, argc, argv);
}

void init(int argc, char** argv, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps(2);
  ps[0]=p1; ps[1]=p2;
  init(ps, argc, argv);
}

void init(int argc, char** argv, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps(3);
  ps[0]=p1; ps[1]=p2; ps[2]=p3;
  init(ps, argc, argv);
}

void init(int argc, char** argv, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps(4);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4;
  init(ps, argc, argv);
}

void init(int argc, char** argv, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps(5);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5;
  init(ps, argc, argv);
}

void init(int argc, char** argv, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  std::vector<Parameter> ps(6);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5; ps[5]=p6;
  init(ps, argc, argv);
}

void init(int argc, char** argv, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
          const Parameter& p7)
{
  std::vector<Parameter> ps(7);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5; ps[5]=p6; ps[6]=p7;
  init(ps, argc, argv);
}

//! initialization procedure for time handling
void timerInit()
{
  theStartTime_p = new Timer();
  theStartTime_p->update();
  theLastTime_p = new Timer();
  theLastTime_p->update();
}

//! initialization procedure for trace handling
void traceInit()
{
  trace_p = new Trace();
  trace_p->push("init");
}

//! initialization procedure for messages handling
void msgInit(const string_t& msgPath, std::ofstream& out)
{
  // Create message object
  if (theMessages_p != nullptr) { delete theMessages_p; }
  theMessages_p = new Messages(msgPath + "messages.txt", out, thePrintFile);
  theMessageData = MsgData();
}

std::ostream& operator<<(std::ostream& out, const EcType& et)
{ out << words("essential condition", et); return out; }
std::ostream& operator<<(std::ostream& out, const ReductionMethodType& rmt)
{ out << words("reduction method", rmt); return out; }
std::ostream& operator<<(std::ostream& out, const InterpolationType& it)
{ out << words("interpolation", it); return out; }
std::ostream& operator<<(std::ostream& out, const FESubType& fest)
{ out << words("FE subname", fest); return out; }
std::ostream& operator<<(std::ostream& out, const FEType& fet)
{ out << words("FE name", fet); return out; }
std::ostream& operator<<(std::ostream& out, const IntegrationMethodType& imt)
{ out << words("imtype", imt); return out; }
std::ostream& operator<<(std::ostream& out, const QuadRule& qr)
{ out << words("quadrule", qr); return out; }
std::ostream& operator<<(std::ostream& out, const SobolevType& st)
{ out << words("Sobolev", st); return out; }
std::ostream& operator<<(std::ostream& out, const LinearFormType& lft)
{ out << words("form type", lft); return out; }
std::ostream& operator<<(std::ostream& out, const ComputationType& ct)
{ out << words("computation type", ct); return out; }
std::ostream& operator<<(std::ostream& out, const TransformType& tt)
{ out << words("transform", tt); return out; }
std::ostream& operator<<(std::ostream& out, const IOFormat& iof)
{ out << words("ioformat", iof); return out; }
std::ostream& operator<<(std::ostream& out, const OrientationType& ot)
{ out << words("orientation type", ot); return out; }
std::ostream& operator<<(std::ostream& out, const ShapeType& sh)
{ out << words("shape", sh); return out; }
std::ostream& operator<<(std::ostream& out, const ShapesType& sh)
{ out << words("shapes", sh); return out; }
std::ostream& operator<<(std::ostream& out, const OCShapeType& ocst)
{ out << words("OC shape", ocst); return out; }
std::ostream& operator<<(std::ostream& out, const AccessType& at)
{ out << words("access type", at); return out; }
std::ostream& operator<<(std::ostream& out, const FactorizationType& ft)
{ out << words("factorization type", ft); return out; }
std::ostream& operator<<(std::ostream& out, const StorageType& st)
{ out << words("storage type", st); return out; }
std::ostream& operator<<(std::ostream& out, const StorageBuildType& sbt)
{ out << words("storage build type", sbt); return out; }
std::ostream& operator<<(std::ostream& out, const StrucType& st)
{ out << words("structure", st); return out; }
std::ostream& operator<<(std::ostream& out, const SymType& st)
{ out << words("symmetry", st); return out; }
std::ostream& operator<<(std::ostream& out, const DiffOpType& dot)
{ out << words("diffop", dot); return out; }
std::ostream& operator<<(std::ostream& out, const ContinuityOrder& co)
{ out << words("ContinuityOrder", co); return out; }
std::ostream& operator<<(std::ostream& out, const UnitaryVector& uv)
{ out << words("normal", uv); return out; }
std::ostream& operator<<(std::ostream& out, const DofType& dt)
{ out << words("dof", dt); return out; }
std::ostream& operator<<(std::ostream& out, const SpaceType& st)
{ out << words("space", st); return out; }
std::ostream& operator<<(std::ostream& out, const SupportType& st)
{ out << words("support type", st); return out; }
std::ostream& operator<<(std::ostream& out, const UnknownType& ut)
{ out << words("unknown", ut); return out; }
std::ostream& operator<<(std::ostream& out, const EigenSolverType& est)
{ out << words("eigen solver", est); return out; }
std::ostream& operator<<(std::ostream& out, const IterativeSolverType& ist)
{ out << words("iterative solver", ist); return out; }
std::ostream& operator<<(std::ostream& out, const HMApproximationMethod& hmam)
{ out << words("HMatrix approximation", hmam); return out; }
std::ostream& operator<<(std::ostream& out, const FuncFormType& fft)
{ out << words("function type", fft); return out; }
std::ostream& operator<<(std::ostream& out, const FunctType& ft)
{ out << words("function", ft); return out; }
std::ostream& operator<<(std::ostream& out, const Language& l)
{ out << words("language", l); return out; }
std::ostream& operator<<(std::ostream& out, const ValueType& vt)
{ out << words("value", vt); return out; }
std::ostream& operator<<(std::ostream& out, const SplineType& st)
{ out << words("spline type", st); return out; }
std::ostream& operator<<(std::ostream& out, const SplineSubtype& sst)
{ out << words("spline subtype", sst); return out; }
std::ostream& operator<<(std::ostream& out, const SplineParametrization& sp)
{ out << words("spline parametrization", sp); return out; }
std::ostream& operator<<(std::ostream& out, const SplineBC& sbc)
{ out << words("spline BC", sbc); return out; }

} // end of namespace xlifepp
