/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file globalScopeData.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 3 jun 2011
  \date 02 mars 2012

  \brief global scope variable and object definitions
 */

#ifndef GLOBAL_SCOPE_DATA_HPP
#define GLOBAL_SCOPE_DATA_HPP

#include <iosfwd>
#include <string>
#include <iostream>

#if __cplusplus >= 201103L    //c++11 enabled, use std::random
#include <random>
#endif

namespace xlifepp
{

//------------------------------------------------------------------------------
// Output format parameters and extremal values
//------------------------------------------------------------------------------
const number_t entriesPerRow = 10;
// unused: const number_t addrPerRow = 10;
// unused: const number_t numberPerRow  = 10;

#if defined(STD_TYPES)
const real_t theRealMax = FLT_MAX; // 3.40e+38F;
const real_t theEpsilon = FLT_EPSILON; // 1.19209290e-07F;
const number_t theEpsilonFactor = 1000;
const number_t theNumberMax = UINT_MAX;
const size_t theSizeMax = ULONG_MAX;
const int_t theIntMax = LONG_MAX;
const int_t theIntMin = LONG_MIN;
const number_t entryWidth = 15;
const number_t entryPrec = 7;
const number_t addrWidth = 8;
const number_t fullPrec = 9; // conforms to IEEE floating point numbers representation
const number_t testPrec = 5;

#elif defined(LONG_TYPES)
const real_t theRealMax = DBL_MAX; // 1.79769e+308;
const real_t theEpsilon = DBL_EPSILON; // 2.22045e-16;
const number_t theEpsilonFactor = 10000;
const number_t theNumberMax = SIZE_MAX;
const size_t theSizeMax = SIZE_MAX;
const int_t theIntMax = LONG_MAX;
const int_t theIntMin = LONG_MIN;
const number_t entryWidth = 19;
const number_t entryPrec = 11;
const number_t addrWidth = 11;
const number_t numberWidth = 11;
const number_t fullPrec = 17; // conforms to IEEE floating point numbers representation
const number_t testPrec = 9;

#elif defined(LONGLONG_TYPES)
const real_t theRealMax = LDBL_MAX;
const real_t theEpsilon = LDBL_EPSILON;
const number_t theEpsilonFactor = 1000000;
const number_t theNumberMax = ULONG_LONG_MAX;
const size_t theSizeMax = ULONG_LONG_MAX;
const int_t theIntMax = LLONG_MAX;
const int_t theIntMin = LLONG_MIN;
const number_t entryWidth = 19;
const number_t entryPrec = 11;
const number_t addrWidth = 11;
const number_t numberWidth = 11;
const number_t fullPrec = 21; // conforms to IEEE floating point numbers representation
const number_t testPrec = 11;
#endif

const dimen_t theDimMax = USHRT_MAX;
const real_t theTolerance = theEpsilonFactor* theEpsilon;
const real_t theZeroThreshold = 1.e-30;

number_t defaultMaxIterations = 0;
real_t theRatioOfIterations2Rows;
real_t theBreakdownThreshold = 1.e-30;
real_t theDefaultConvergenceThreshold = std::sqrt(theEpsilon);
number_t defaultKrylovDimension = 0;
real_t theDefaultCharacteristicLength=0.1;
real_t theLocateToleranceFactor=0.1;

number_t theGlobalVerboseLevel = UINT_MAX;
number_t theVerboseLevel = 1;
bool trackingObjects = true;
bool isTestMode = false;

//------------------------------------------------------------------------------
// Math constants
//------------------------------------------------------------------------------
const real_t over3_ = 1. / (real_t(3.));
const real_t over6_ = 1. / (real_t(6.));
/*
  pi with 79 digits (about 1,241,100,000,000 were known in Dec. 2002)
  3.141592653589793238462643383279502884197169399375105820974944592307816406286208
  3.1415926535897932 (double, 16 digits)
  3.1415927 (float, 7 digits)
 */
const real_t pi_ = 4.*std::atan(real_t(1.));
const complex_t i_ = complex_t(0.,1.);
const real_t overpi_  = 1. / pi_;
const real_t over2pi_ = overpi_ / 2.;
const real_t over4pi_ = overpi_ / 4.;
const real_t sqrtOfpi_ = std::sqrt(pi_);
const complex_t expipiover3_=std::exp(i_*pi_/3.);

/*
  sqrt(2)
  1.414213562373095048801688724209698078569671875376948073176679737990732478462107
  1.4142135623730951 (double, 16 digits)
  1.4142135 (float, 7 digits)
 */
const real_t sqrtOf2_ = std::sqrt(real_t(2.));
const real_t sqrtOf3_ = std::sqrt(real_t(3.));
const real_t logOf2_  = std::log(real_t(2.));
// EulerMascheroni gamma constant = .577215664901532860606512090082402431042....
const real_t theEulerConst = .5772156649015328606065;

//------------------------------------------------------------------------------
// angle unit management
//------------------------------------------------------------------------------
AngleUnitType defaultAngleUnit=_rad;
AngleUnit deg_(_deg,defaultAngleUnit,0.);  // to convert deg to default angle unit, tolerance 0
AngleUnit rad_(_rad,defaultAngleUnit,0.);  // to convert rad to default angle unit, tolerance 0

//------------------------------------------------------------------------------
// default random seed and generator
//------------------------------------------------------------------------------
unsigned int theRandomSeed;
#if __cplusplus >= 201103L    //c++11 enabled, use std::random
    std::mt19937 theRandomEngine;  // Mersenne Twister 19937 generator
#endif

//------------------------------------------------------------------------------
// Default objects
//------------------------------------------------------------------------------
Parameters defaultParameters(0);                      //!< default ParameterList object used as default argument in Function constructor
Parameters* defaultParameters_p = &defaultParameters; //!< default ParameterList object used as default argument in Function constructor

//------------------------------------------------------------------------------
// Languages and localized strings
//------------------------------------------------------------------------------

std::map<string_t, std::vector<std::string> > Environment::enumWords_;
std::map<string_t, std::string> Environment::words_;

//------------------------------------------------------------------------------
// Language constant
//------------------------------------------------------------------------------
#if defined(LANG_EN)
const number_t theLanguage = _en;
#elif defined(LANG_FR)
const number_t theLanguage = _fr ;
#elif defined(LANG_DE)
const number_t theLanguage = _de ;
#elif defined(LANG_ES)
const number_t theLanguage = _es ;
#endif /* LANG_ */

//------------------------------------------------------------------------------
// Miscellaneous initialization
//------------------------------------------------------------------------------
bool Environment::running_(false);
bool Environment::parallelOn_(true);
std::vector<string_t> Environment::systemKeys_;
real_t Point::tolerance;
string_t eol="\n";

//------------------------------------------------------------------------------
// Log, Errors and Print files
//------------------------------------------------------------------------------
const string_t Environment::theInstallPath_ = INSTALL_PATH;
const string_t Environment::theVisuTermVecMPath_ = VISUTERMVECM_PATH;
const string_t Environment::theGmshExe_ = GMSH_EXECUTABLE;
const string_t Environment::theParaviewExe_ = PARAVIEW_EXECUTABLE;
std::vector<string_t> Environment::authorizedSaveToFileExtensions_;
const number_t Trace::maxPos_ = 36;
string_t theLogFile = "log.txt";
string_t theLogGmshFile = "log_gmsh.txt";
PrintStream theLogStream;
bool Trace::isLogged_=false;

string_t thePrintFile = "print.txt";
PrintStream thePrintStream;
std::stringstream theStringStream;
CoutStream theCout;

//--------------------------------------------------------------------------------
// Tracing, Error and Time functions
//--------------------------------------------------------------------------------
//! runtime Trace object
Trace* trace_p;

//! runtime Environment object
Environment* theEnvironment_p;

//! start and previous Timer objects
Timer* theStartTime_p;
Timer* theLastTime_p;

//! error and msg objects
Messages* theMessages_p;
MsgData theMessageData; // global message object
string_t theWhereData = "";
bool Trace::trackingMode=false;
bool Trace::traceMemory=false;
bool Trace::disablePushPop=false;

#ifndef DOXYGEN_SHOULD_SKIP_THIS

Parameter _lang(_pk_lang, "lang"), _verbose(_pk_verbose, "verbose");
Parameter _trackingMode(_pk_trackingMode, "trackingMode"), _traceMemory(_pk_traceMemory, "traceMemory"),
          _pushpop(_pk_pushpop, "pushpop");
Parameter _nbThreads(_pk_nbThreads, "nbThreads"), _isLogged(_pk_isLogged, "isLogged");
Parameter _varnames(_pk_varnames, "varnames");
Parameter _vertices(_pk_vertices, "vertices"), _faces(_pk_faces, "faces");
Parameter _center(_pk_center, "center"), _center1(_pk_center1, "center1"), _center2(_pk_center2, "center2");
Parameter _apogee(_pk_apogee, "apogee"), _origin(_pk_origin, "origin"), _apex(_pk_apex, "apex");
Parameter _v1(_pk_v1, "v1"), _v2(_pk_v2, "v2"), _v3(_pk_v3, "v3"), _v4(_pk_v4, "v4");
Parameter _v5(_pk_v5, "v5"), _v6(_pk_v6, "v6"), _v7(_pk_v7, "v7"), _v8(_pk_v8, "v8");
Parameter _xmin(_pk_xmin, "xmin"), _xmax(_pk_xmax, "xmax"), _ymin(_pk_ymin, "ymin"), _ymax(_pk_ymax, "ymax");
Parameter _zmin(_pk_zmin, "zmin"), _zmax(_pk_zmax, "zmax");
Parameter _tmin(_pk_tmin,"tmin"), _tmax(_pk_tmax,"tmax");
Parameter _spline(_pk_spline,"spline"),_splineType(_pk_spline_type,"spline_type"),
          _splineSubtype(_pk_spline_subtype,"spline_subtype"), _degree(_pk_degree,"degree"),
          _splineBC(_pk_spline_BC,"spline BC"),_tensionFactor(_pk_tension,"tension factor"),
          _splineParametrization(_pk_spline_parametrization,"spline parametrization"),
          _weights(_pk_weights,"weights"), _nbu(_pk_nbu,"nbu");
Parameter _tangent0(_pk_tangent_0,"tangent 0"), _tangent1(_pk_tangent_1,"tangent 1");
Parameter _parametrization(_pk_parametrization,"parametrization"), _partitioning(_pk_partitioning,"partitioning");
Parameter _nbParts(_pk_nbParts,"nbParts"), _shapePartition(_pk_shapePartition,"shapePartition");
Parameter _partmesh(_pk_partmesh,"partmesh"), _ncommon(_pk_ncommon,"ncommon");
Parameter _ptype(_pk_ptype,"ptype"), _objtype(_pk_objtype,"objtype");
Parameter _ctype(_pk_ctype,"ctype"), _iptype(_pk_iptype,"iptype");
Parameter _rtype(_pk_rtype,"rtype");
Parameter _savedoms(_pk_savedoms,"savedoms"), _repsortie(_pk_repsortie,"repsortie");
Parameter _length(_pk_length, "length");
Parameter _xlength(_pk_xlength, "xlength"), _ylength(_pk_ylength, "ylength"), _zlength(_pk_zlength, "zlength");
Parameter _xradius(_pk_xradius, "xradius"), _yradius(_pk_yradius, "yradius"), _zradius(_pk_zradius, "zradius");
Parameter _radius(_pk_radius, "radius"), _radius1(_pk_radius1, "radius1"), _radius2(_pk_radius2, "radius2");
Parameter _basis(_pk_basis, "basis"), _scale(_pk_scale, "scale"), _direction(_pk_dir, "direction"),
          _axis(_pk_axis, "axis"), _normal(_pk_normal, "normal");
Parameter _end_shape(_pk_end_shape,"end_shape"), _end1_shape(_pk_end1_shape,"end1_shape"),
          _end2_shape(_pk_end2_shape,"end2_shape");
Parameter _end_distance(_pk_end_distance, "end_distance"), _end1_distance(_pk_end1_distance, "end1_distance");
Parameter _end2_distance(_pk_end2_distance, "end2_distance");
Parameter _nnodes(_pk_nnodes, "nnodes"), _hsteps(_pk_hsteps, "hsteps");
Parameter _base_names(_pk_base_names, "base names"), _domain_name(_pk_domain_name, "domain name"),
          _side_names(_pk_side_names, "side names"), _face_names(_pk_face_names, "face names"),
          _edge_names(_pk_edge_names, "edge names"), _vertex_names(_pk_vertex_names, "vertex names");
Parameter _init_transformation(_pk_init_transformation,"init transformation"), _naming_domain(_pk_naming_domain,"naming domain"), _naming_section(_pk_naming_section,"naming section"),
          _naming_side(_pk_naming_side,"naming side");
Parameter _layers(_pk_layers, "layers"), _nbsubdomains(_pk_nbsubdomains, "nbsubdomains");
Parameter _nboctants(_pk_nboctants, "nboctants"), _angle(_pk_angle, "angle"),
          _angle1(_pk_angle1, "angle1"), _angle2(_pk_angle2, "angle2"),
          _type(_pk_type, "type");
Parameter _shape(_pk_shape, "shape"), _generator(_pk_generator, "generator"), _pattern(_pk_pattern, "pattern"),
          _split_direction(_pk_split_direction, "split_direction"), _refinement_depth(_pk_refinement_depth, "refinement_depth"),
          _default_hstep(_pk_default_hstep, "default_hstep");
Parameter _dim(_pk_dim, "dim"), _domain(_pk_domain, "domain"), _extension_domain(_pk_extension_domain, "extension_domain"),
           _extension_domain_v(_pk_extension_domain_v, "extension_domain_v"),
          _FE_type(_pk_FE_type, "FEtype"), _FE_subtype(_pk_FE_subtype, "FEsubtype"),
          _Sobolev_type(_pk_Sobolev_type, "Sobolev_type"), _order(_pk_order, "order"), _interpolation(_pk_interpolation, "_interpolation");
Parameter _optimizeNumbering(_pk_optimizeNumbering, "optimizeNumbering"),
          _notOptimizeNumbering(_pk_notOptimizeNumbering, "notOptimizeNumbering"),
          _withLocateData(_pk_withLocateData, "withLocateData"), _withoutLocateData(_pk_withoutLocateData, "withoutLocateData");
Parameter _basis_dim(_pk_basis_dim, "basis_dim");
Parameter _rank(_pk_rank, "rank");
Parameter _method(_pk_method, "method"), _bound(_pk_bound, "bound"),
          _quad(_pk_quad, "quad"), _quad1(_pk_quad1, "quad1"), _quad2(_pk_quad2, "quad2"),
          _order1(_pk_order1, "order1"), _order2(_pk_order2, "order2");
Parameter _isogeo(_pk_isogeo, "isogeo"), _symmetry(_pk_symmetry, "symmetry"), _computation(_pk_computation, "computation");
Parameter _functionPart(_pk_function_part, "functionPart");
Parameter _cluster(_pk_cluster, "cluster"), _row_cluster(_pk_row_cluster, "row_cluster"),
          _col_cluster(_pk_col_cluster, "col_cluster"), _min_block_size(_pk_min_block_size, "min_block_size"),
          _clustering_method(_pk_clustering_method, "clustering_method"),
          _approximation_method(_pk_approximation_method, "approximation_method"), _max_rank(_pk_max_rank, "max_rank"),
          _threshold(_pk_threshold, "threshold"), _eta(_pk_eta, "eta");
Parameter _compute(_pk_compute, "compute"), _notCompute(_pk_notCompute, "notCompute"),
          _assembled(_pk_assembled, "assembled"), _unassembled(_pk_unassembled, "unassembled"),
          _storage(_pk_storage, "storage"), _nodal(_pk_nodal, "nodal"), _reduction(_pk_reduction, "reduction"),
          _pseudoReductionMethod(_pk_pseudoReductionMethod, "pseudoReductionMethod"),
          _realReductionMethod(_pk_realReductionMethod, "realReductionMethod"),
          _penalizationReductionMethod(_pk_penalizationReductionMethod, "penalizationReductionMethod");
Parameter _maxIt(_pk_maxIt, "iterMax"), _krylovDim(_pk_krylovDim, "krylovDim"), _solver(_pk_solver, "solver"),
          _tolerance(_pk_tolerance, "tolerance");
Parameter _convToStd(_pk_convToStd, "convToStd"), _forceNonSym(_pk_forceNonSym, "forceNonSym"),
          _ncv(_pk_ncv, "ncv"), _nev(_pk_nev, "nev"), _mode(_pk_mode, "shift_mode"),
          _sigma(_pk_sigma, "sigma"), _sort(_pk_sort, "sort"), _which(_pk_which, "which");
Parameter _omega(_pk_omega, "omega"), _name(_pk_name, "name");
Parameter _format(_pk_format, "format"), _data_name(_pk_data_name, "data_name"), _aFilePerDomain(_pk_aFilePerDomain, "aFilePerDomain"), _aUniqueFile(_pk_aUniqueFile, "aUniqueFile");
Parameter _vizir4_solType(_pk_vizir4_solType, "Vizir4_sol_type");
Parameter _transformation(_pk_transformation, "transformation"), _suffix(_pk_suffix, "suffix");

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

// global variables
Parameter _residue(0.,"residue"), _nbIterations(0, "nbiterations");

#ifdef XLIFEPP_WITH_ARPACK
  ArpackProb _ARprob(&theDefaultTermMatrix,0);
#endif

//--------------------------------------------------------------------------------
// Initialisation of static variables
//--------------------------------------------------------------------------------
std::vector<Interpolation*> Interpolation::theInterpolations;                      //!< unique list of FE interpolations
std::vector<Space*>  Space::theSpaces;                                             //!< unique list of defined spaces
number_t FeSpace::lastEltIndex = 0;                                                //!< for unique elements numbering
std::vector<Unknown*> Unknown::theUnknowns;                                        //!< list of pointers of all Unknowns
std::vector<DifferentialOperator*> DifferentialOperator::theDifferentialOperators; //!< list of pointers of all DifferentialOperator
std::map<string_t, std::pair<ValueType, StrucType> > Function::returnTypes;        //!< list of RTI names of return types of function
std::map<string_t, std::pair<ValueType, StrucType> > Function::returnArgs;         //!< list of RTI names of return args of function
std::map<string_t, std::pair<ValueType, StrucType> > Value::theValueTypeRTInames;  //!< list of RTI names of valuetypes (real, complex, Vector<>, ...)
std::vector<const GeomDomain*> GeomDomain::theDomains;                             //!< list of pointers of all geometric domains
std::vector<GeomRefElement*> GeomRefElement::theGeomRefElements;                   //!< list of pointers of all geometric reference elements
std::vector<RefElement*> RefElement::theRefElements;                               //!< list of pointers of all reference elements
std::vector<Quadrature*> Quadrature::theQuadratures;                               //!< list of pointers of all quadrature rules
std::vector<Term*> Term::theTerms;                                                 //!< list of pointers of all terms
std::vector<MatrixStorage*> MatrixStorage::theMatrixStorages;                      //!< list of pointers of all matrix storages
std::map<string_t,DomainMap*> DomainMap::theDomainMaps;                            //!< list of pointers of all domain maps indexed by name
std::vector<Projector *> Projector::theProjectors;                                 //!< list of pointers of all projectors
bool Geometry::checkInclusion=true;                                                //!< to manage inclusion checking in composite algorithms
number_t IterativeSolver::theNumberOfIterations=0;                                 //!< to store the number of iterations of iterative solvers
real_t IterativeSolver::theResidue=0.;                                             //!< to store the residue of iterative solvers

// void real Vector
Vector<real_t> voidVector;
// Constant TermMatrix:
TermMatrix theDefaultTermMatrix;
// Constant TermVector:
const TermVector theDefaultTermVector;
// Constant PreconditionerTerm:
Preconditioner theDefaultPreconditioner(_noPrec);
// Constant SymbolicFunction
SymbolicFunction x_1(_x1),x_2(_x2),x_3(_x3), x_4(_x4), r_(_r), theta_(_theta), phi_(_phi);

//--------------------------------------------------------------------------------
// list of objects that may depend on thread
//--------------------------------------------------------------------------------
ThreadData theThreadData;

#ifdef XLIFEPP_WITHOUT_OPENCASCADE
  typedef SquareGeo Square;
#endif

} // end of namespace xlifepp

#endif /* GLOBAL_SCOPE_DATA_HPP */
