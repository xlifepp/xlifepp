/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RowCsStorage.hpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Definition of the xlifepp::RowCsStorage class

  xlifepp::RowCsStorage class is the child class of xlifepp::CsStorage class dealing with Compressed Row Storage
  NOTE: the first entry is stored at adress 1 in values vector (not at adress 0!)

  colIndex_: vector of column index of non zero value ordered row by row (as values_ vector)
  rowPointer_: vector of positions of begining of rows in previous vector

  Example: The following 5 x 6 non symmetric matrix
  -------
  \verbatim
  0  d x . x . x
  1  . d x . . x
  2  x x d . . .     colIndex_   = [ 0 1 3 5  1 2 5  0 1 2  2 3  0 2 3 4 ]
  3  . . x d . .     rowPointer_ = [ 0 4 7 10 12 16]  (last gives the number of non zeros)
  4  x . x x d .
  \endverbatim

  Note: this is the true CSR, contrary to Daniel's choice
       : for an empty row, rowPointer_ is equal to the previous one: length of row i = rowPointer_[i+1]-rowPointer_[i]

  Only _sym access type storage can be used for a matrix with a symmetry
  property ( _symmetric, _skewSymmetric, _selfAdjoint or _skewAdjoint)
*/

#ifndef ROW_CS_STORAGE_HPP
#define ROW_CS_STORAGE_HPP

#include "config.h"
#include "CsStorage.hpp"

namespace xlifepp
{

/*!
   \class CsStorage
   child class of row compressed sparse storage
 */
class RowCsStorage : public CsStorage
{
  protected:
    std::vector<number_t> colIndex_;   //!< vector of column index of non zero value
    std::vector<number_t> rowPointer_; //!< vector of positions of begining of rows

  public:
    // constructor, destructor
    //! default constructor
    RowCsStorage(number_t nr = 0, number_t nc = 0, string_t id="RowCsStorage");
    //! destructor
    ~RowCsStorage() {}
    //! constructor by a pair of global numerotation vectors
    RowCsStorage(number_t, number_t, const std::vector< std::vector<number_t> >&, const std::vector< std::vector<number_t> >&, string_t id="RowCsStorage");

    //! constructor by the list of col index by rows
    template<class L>
    RowCsStorage(number_t, number_t, const std::vector<L>&, string_t id="RowCsStorage");

    //! create a clone (virtual copy constructor, covariant)
    RowCsStorage* clone() const
    {return new RowCsStorage(*this);}
    //! create a new scalar RowCs storage from current RowCs storage and submatrix sizes
    RowCsStorage* toScalar(dimen_t nbr, dimen_t nbc);
     //! clear storage vectors
    virtual void clear()
    {rowPointer_.clear();colIndex_.clear();}

    // utilities
    number_t size() const  {return colIndex_.size();}         //!< number of non zero elements stored
    bool sameStorage(const MatrixStorage&) const;             //!< check if two storages have the same structures

    // access operator
    number_t pos(number_t i, number_t j, SymType s = _noSymmetry) const;     //!< returns adress of entry (i,j)
    void positions(const std::vector<number_t>&, const std::vector<number_t>&,
                   std::vector<number_t>&, bool errorOn = true,SymType s = _noSymmetry)  const;                    //!< access to submatrix positions
    std::vector<std::pair<number_t, number_t> > getCol(SymType s,number_t c, number_t r1=1, number_t r2=0) const;
       //!< get (col indices, adress) of col c in set [r1,r2]
    std::vector<std::pair<number_t, number_t> > getRow(SymType s,number_t r, number_t c1=1, number_t c2=0) const;
    //!< get (col indices, adress) of row r in set [c1,c2]
    std::set<number_t> getCols(number_t r, number_t c1=1, number_t c2=0) const;                            //!< get col indices of row r in set [c1,c2] (no getRows!)
    void getColsV(std::vector<number_t>&, number_t&, number_t r, number_t c1=1, number_t c2=0) const;      //!< get col indices of row r in set [c1,c2] (no allocation!)

    std::vector<number_t> skylineRowPointer() const;  //!< return skyline row pointer from current RowCsStorage
    std::vector<number_t> skylineColPointer() const;  //!< return skyline col pointer from current RowCsStorage
    // fill current values (csval)in skyline val (skyval), using the function skysto->pos(i,j)
    void fillSkylineValues(const std::vector<real_t>& csval, std::vector<real_t>& skyval, SymType sym, MatrixStorage* skysto) const
      {fillSkylineValuesT(csval,skyval,sym,skysto);}
    void fillSkylineValues(const std::vector<complex_t>& csval, std::vector<complex_t>& skyval, SymType sym, MatrixStorage* skysto) const
      {fillSkylineValuesT(csval,skyval,sym,skysto);}
    void fillSkylineValues(const std::vector<Matrix<real_t>>& csval, std::vector<Matrix<real_t>>& skyval, SymType sym, MatrixStorage* skysto) const
      {fillSkylineValuesT(csval,skyval,sym,skysto);}
    void fillSkylineValues(const std::vector<Matrix<complex_t>>& csval, std::vector<Matrix<complex_t>>& skyval, SymType sym, MatrixStorage* skysto) const
      {fillSkylineValuesT(csval,skyval,sym,skysto);}
    template <typename T>
    void fillSkylineValuesT(const std::vector<T>& csval, std::vector<T>& skyval, SymType sym, MatrixStorage* skysto) const;

    //modifying tools by adding set of row/column indices
    void addSubMatrixIndices(const std::vector<number_t>& ,const std::vector<number_t>& ); //!< add dense submatrix indices to storage
    void addIndices(const std::vector<number_t>&, const std::vector<number_t>&);           //!< add indexes (i,j) given by rows (i) and columns (j) (i,j >= 1)
    void addRow(number_t, const std::set<number_t>&, MatrixPart);                          //!< add row-cols (r,c1), (r,c2), ... given by a cols set (r,c>=1)
    //@{
     //! delete rows r1,..., r2 (may be expansive for some storage)
    void deleteRows(number_t r1, number_t r2, std::vector<real_t>& v, SymType s)
    {deleteColsT(rowPointer_,colIndex_,nbRows_,nbCols_,r1, r2, v);}
    void deleteRows(number_t r1, number_t r2, std::vector<complex_t>& v, SymType s)
    {deleteColsT(rowPointer_,colIndex_,nbRows_,nbCols_,r1, r2, v);}
    void deleteRows(number_t r1, number_t r2, std::vector<Matrix<real_t> >& v, SymType s)
    {deleteColsT(rowPointer_,colIndex_,nbRows_,nbCols_,r1, r2, v);}
    void deleteRows(number_t r1, number_t r2, std::vector<Matrix<complex_t> >& v, SymType s)
    {deleteColsT(rowPointer_,colIndex_,nbRows_,nbCols_,r1, r2, v);}
    //@}
    //@{
    //! delete rows c1,..., c2 (may be expansive for some storage)
    void deleteCols(number_t c1, number_t c2, std::vector<real_t>& v, SymType s)
    {deleteRowsT(rowPointer_,colIndex_,nbRows_,nbCols_,c1, c2, v);}
    void deleteCols(number_t c1, number_t c2, std::vector<complex_t>& v, SymType s)
    {deleteRowsT(rowPointer_,colIndex_,nbRows_,nbCols_,c1, c2, v);}
    void deleteCols(number_t c1, number_t c2, std::vector<Matrix<real_t> >& v, SymType s)
    {deleteRowsT(rowPointer_,colIndex_,nbRows_,nbCols_,c1, c2, v);}
    void deleteCols(number_t c1, number_t c2, std::vector<Matrix<complex_t> >& v, SymType s)
    {deleteRowsT(rowPointer_,colIndex_,nbRows_,nbCols_,c1, c2, v);}
    //@}

    // print utilities
    void print(std::ostream&) const;                                   //!< print storage pointers
    void printEntries(std::ostream&, const std::vector<real_t>&,
                      number_t vb, const SymType sym) const;             //!< print real scalar matrix
    void printEntries(std::ostream&, const std::vector<complex_t>&,
                      number_t vb, const SymType sym) const;             //!< print complex scalar matrix
    void printEntries(std::ostream&, const std::vector<Matrix<real_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of real matrices
    void printEntries(std::ostream&, const std::vector<Matrix<complex_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of complex matrices
    void printEntries(std::ostream&, const std::vector<Vector<real_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of real vectors (not available)
    void printEntries(std::ostream&, const std::vector<Vector<complex_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of complex vectors (not available)
    void printCooMatrix(std::ostream&, const std::vector<real_t>&,
                        SymType s = _noSymmetry) const;       //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector<complex_t>&,
                        SymType s = _noSymmetry) const;       //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector< Matrix<real_t> >&,
                        SymType s = _noSymmetry) const;       //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector< Matrix<complex_t> >&,
                        SymType s = _noSymmetry) const;       //!< output matrix of complex scalars in coordinate form

    void print(PrintStream& os) const
      {print(os.currentStream());}
    void printEntries(PrintStream& os, const std::vector<real_t>& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<complex_t>& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Matrix<real_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Matrix<complex_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Vector<real_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Vector<complex_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printCooMatrix(PrintStream& os, const std::vector<real_t>& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector<complex_t>& m,SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector< Matrix<real_t> >& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector< Matrix<complex_t> >& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}

   //! load file utilities
    void loadFromFileDense(std::istream& ifs, std::vector<real_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileDense(ifs, mat, colIndex_, rowPointer_, sym, rAsC);}
    void loadFromFileDense(std::istream& ifs, std::vector<complex_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileDense(ifs, mat, colIndex_, rowPointer_, sym, rAsC);}
    void loadFromFileCoo(std::istream& ifs, std::vector<real_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileCoo(ifs, mat, colIndex_, rowPointer_, sym, rAsC);}
    void loadFromFileCoo(std::istream& ifs, std::vector<complex_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileCoo(ifs, mat, colIndex_, rowPointer_, sym, rAsC);}

    // template Matrix x Vector & Vector x Matrix multiplications and specializations (see below)
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< templated row dense Matrix x Vector
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< templated Vector x row dense Matrix
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>& m, V* vp, R* rp) const;                         //!<templated rowcs Matrix x Vector (pointer)
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>& m, V* vp, R* rp) const;                         //!<templated rowcs Vector x Matrix (pointer)

    //@{
    //! Matrix (Scalar) * Vector (Scalar)
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    //@}

    //@{
    //! Vector (Scalar) * Matrix (Scalar)
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    //@}

    //! Matrix * Vector (pointer form)
    void multMatrixVector(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }

    //! Vector*Matrix (pointer form)
    void multVectorMatrix(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }

    // template Matrix + Matrix addition and specializations (see below)
    template<typename M1, typename M2, typename R>
    void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const; //!< templated row dense Matrix + Matrix

    //@{
    //! Matrix+Matrix
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv) const
    { addMatrixMatrix<>(m, v, rv);}
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    //@}

    //! conversion to umfpack format
    template<typename M, typename OrdinalType>
    void toUmfPack(const std::vector<M>& values, std::vector<OrdinalType>& colPointer, std::vector<OrdinalType>& rowIndex, std::vector<M>& mat) const;

    //@{
    //! specializations of umfpack conversion
    void toUmfPack(const std::vector<real_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<real_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat);}
    void toUmfPack(const std::vector<complex_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<complex_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat);}
    //@}


    //! Incomplete matrix factorization with ILU
    template<typename M>
    void ilu(std::vector<M>& m, std::vector<M>& fa) const;

    template<typename M>
    void iluParallel(std::vector<M>& m, std::vector<M>& fa) const;

    //@{
    //! specializations of template ILU
    void ilu(std::vector<real_t>& m, std::vector<real_t>& fa, const SymType sym = _noSymmetry) const
    {
// #ifdef XLIFEPP_WITH_OMP
//         iluParallel<>(m, fa);
// #else
        ilu<>(m, fa);
// #endif
    }

    void ilu(std::vector<complex_t>& m, std::vector<complex_t>& fa, const SymType sym = _noSymmetry) const
    {
// #ifdef XLIFEPP_WITH_OMP
//         iluParallel<>(m, fa);
// #else
        ilu<>(m, fa);
// #endif
    }
    //@}

};

/*! constructor from the colNumbers (column index in cols starts at 1)
   cols[k] of type L stores the indices of column of non zero element on row k (index starts at 1)
   only requirement on L are the definition of forward const iterator named const_iterator
                             and the size
   works for L = vector<number_t>, list<number_t>, set<number_t> */
template<class L>
RowCsStorage::RowCsStorage(number_t nr, number_t nc, const std::vector<L>& cols, string_t id)
  : CsStorage(nr, nc, _row, id)
{
  trace_p->push("RowCsStorage constructor");
  if(cols.size()==nbRows_) buildCsStorage(cols, colIndex_, rowPointer_); //construct colIndex_ and rowPointer_
  else //adjust cols vector to fit nbRows_
  {
      std::vector<L> fitcols(nbRows_, L());
      typename std::vector<L>::iterator itf=fitcols.begin();
      typename std::vector<L>::const_iterator itc=cols.begin();
      number_t i=0;
      for(;itc!=cols.end() && i<nbRows_; ++itc, ++itf, ++i) *itf=*itc;
      buildCsStorage(fitcols, colIndex_, rowPointer_); //construct colIndex_ and rowPointer_
  }
  trace_p->pop();
}

// fill current values (csval)in skyline val (skyval), using the function skysto->pos(i,j)
// skysto has to be allocated with a symSkyline or a dualSkyline (not checked here)
template <typename T>
void RowCsStorage::fillSkylineValuesT(const std::vector<T>& csval, std::vector<T>& skyval, SymType sym, MatrixStorage* skysto) const
{
  auto itc=colIndex_.begin();
  auto itv=csval.begin()+1;
  bool lower = skysto->upperPartSize()==0;
  for(number_t r=0;r<nbRows_;r++)
  {
    for(number_t k=0;k<rowPointer_[r+1]-rowPointer_[r];k++,itc++, itv++)
    {
      number_t c=*itc;
      if(r>=c || (r<c && !lower) )
      {
        number_t p=skysto->pos(r+1,c+1);
        skyval[p]=*itv;
      }
    }
  }
}

} // end of namespace xlifepp

#endif // ROW_CS_STORAGE_HPP
