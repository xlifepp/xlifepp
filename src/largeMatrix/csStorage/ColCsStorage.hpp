/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ColCsStorage.hpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Definition of the xlifepp::ColCsStorage class

  xlifepp::ColCsStorage class is the child class of xlifepp::CsStorage class dealing with Compressed Column Storage
  NOTE: the first entry is stored at adress 1 in values vector (not at adress 0!)

  - rowIndex_: vector of row index of non zero value ordered col by col (as values_ vector)
  - ColPointer_: vector of positions of begining of columns in previous vector

  Example: The following 5 x 6 non symmetric matrix
  -------
  \verbatim
  0  d x . x . x
  1  . d x . . x
  2  x x d . . .     rowIndex_   = [ 0 2 4  0 1 2  1 2 3 4  0 3 4  4  0 1 ]
  3  . . x d . .     colPointer_ = [ 0 3 6 10 13 14 16 ] (last gives the number of non zeros)
  4  x . x x d .
  \endverbatim

  Note: this is the true CSC, contrary to Daniel's choice
         for an empty col, colPointer_ is equal to the previous one: length of column i = colPointer_[i+1]-colPointer_[i]

  Only _sym access type storage can be used for a matrix with a symmetry
  property ( _symmetric, _skewSymmetric, _selfAdjoint or _skewAdjoint)
*/

#ifndef COL_CS_STORAGE_HPP
#define COL_CS_STORAGE_HPP

#include "config.h"
#include "CsStorage.hpp"

namespace xlifepp
{

/*!
   \class ColCsStorage
   child class of col compressed sparse storage
 */

class ColCsStorage : public CsStorage
{
  protected:
    std::vector<number_t> rowIndex_;   //!< vector of row index of non zero value
    std::vector<number_t> colPointer_; //!< vector of positions of begining of cols

  public:
    // constructor, destructor
    ColCsStorage(number_t nr = 0, number_t nc = 0, string_t id="ColCsStorage"); //!< default constructor
    ~ColCsStorage() {}                                                          //!< destructor
    //! constructor by a pair of global numbering vectors
    ColCsStorage(number_t, number_t, const std::vector< std::vector<number_t> >&, const std::vector< std::vector<number_t> >&, string_t id="ColCsStorage");
    //! constructor by the list of row index by cols
    template<class L>
    ColCsStorage(number_t, number_t, const std::vector<L>&, string_t id="ColCsStorage");
    ColCsStorage* clone() const                                 //! create a clone (virtual copy constructor, covariant)
    {return new ColCsStorage(*this);}
    ColCsStorage* toScalar(dimen_t, dimen_t);                   //!< create a new scalar ColCs storage from current ColCs storage and submatrix sizes

    // utilities
    number_t size() const  {return rowIndex_.size();}           //!< number of non zero elements stored
    virtual void clear()                                        //! clear storage vectors
    {rowIndex_.clear();colPointer_.clear();}
    bool sameStorage(const MatrixStorage&) const;               //!< check if two storages have the same structures

    // accessor
    std::vector<number_t>& rowIndex()               //! vector of row index of non zero value
        {return rowIndex_;}
    std::vector<number_t>& colPointer()             //! vector of positions of beginning of cols
        {return colPointer_;}
    const std::vector<number_t>& rowIndex() const   //! vector of row index of non zero value
        {return rowIndex_;}
    const std::vector<number_t>& colPointer() const //! vector of positions of beginning of cols
        {return colPointer_;}

    //! access to submatrix positions
    number_t pos(number_t i, number_t j, SymType s = _noSymmetry) const;                                           //!< returns adress of entry (i,j)
    void positions(const std::vector<number_t>&, const std::vector<number_t>&, std::vector<number_t>&, bool errorOn = true, SymType s = _noSymmetry) const;
    std::vector<std::pair<number_t, number_t> > getCol(SymType s, number_t c, number_t r1=1, number_t r2=0) const; //!< get (row indices, adress) of col c in set [r1,r2]
    std::vector<std::pair<number_t, number_t> > getRow(SymType s, number_t r, number_t c1=1, number_t c2=0) const; //!< get (col indices, adress) of row r in set [c1,c2]
    std::set<number_t> getRows(number_t c, number_t r1=1, number_t r2=0) const;                                    //!< get row indices of col c in set [r1,r2] (no getCols)
    void getRowsV(std::vector<number_t>&, number_t&,
                  number_t c, number_t r1=1, number_t r2=0) const;  //!< get row indices of col c in set [r1,r2] (no allocation)

    std::vector<number_t> skylineRowPointer() const; //!< return skyline row pointer from current ColCsStorage
    std::vector<number_t> skylineColPointer() const; //!< return skyline col pointer from current ColCsStorage

    // fill current values (csval)in skyline val (skyval), using the function skysto->pos(i,j)
    void fillSkylineValues(const std::vector<real_t>& csval, std::vector<real_t>& skyval, SymType sym, MatrixStorage* skysto) const
      {fillSkylineValuesT(csval,skyval,sym,skysto);}
    void fillSkylineValues(const std::vector<complex_t>& csval, std::vector<complex_t>& skyval, SymType sym, MatrixStorage* skysto) const
      {fillSkylineValuesT(csval,skyval,sym,skysto);}
    void fillSkylineValues(const std::vector<Matrix<real_t>>& csval, std::vector<Matrix<real_t>>& skyval, SymType sym, MatrixStorage* skysto) const
      {fillSkylineValuesT(csval,skyval,sym,skysto);}
    void fillSkylineValues(const std::vector<Matrix<complex_t>>& csval, std::vector<Matrix<complex_t>>& skyval, SymType sym, MatrixStorage* skysto) const
      {fillSkylineValuesT(csval,skyval,sym,skysto);}
    template <typename T>
    void fillSkylineValuesT(const std::vector<T>& csval, std::vector<T>& skyval, SymType sym, MatrixStorage* skysto) const;

    //modifying tools
    void addSubMatrixIndices(const std::vector<number_t>& ,const std::vector<number_t>& ); //!< add dense submatrix indices to storage
    void addIndices(const std::vector<number_t>&, const std::vector<number_t>&);           //!< add indexes (i,j) given by rows (i) and columns (j) (i,j >= 1)
    void addCol(number_t, const std::set<number_t>&, MatrixPart=_all);                     //!< add col-rows (r,c1), (r,c2), ... given by a rows set (r,c>=1)

    //@{
    //! delete rows r1,..., r2 (may be expansive for some storage)
    void deleteRows(number_t r1, number_t r2, std::vector<real_t>& v, SymType s)
    {deleteRowsT(colPointer_,rowIndex_,nbCols_,nbRows_,r1, r2, v);}
    void deleteRows(number_t r1, number_t r2, std::vector<complex_t>& v, SymType s)
    {deleteRowsT(colPointer_,rowIndex_,nbCols_,nbRows_,r1, r2, v);}
    void deleteRows(number_t r1, number_t r2, std::vector<Matrix<real_t> >& v, SymType s)
    {deleteRowsT(colPointer_,rowIndex_,nbCols_,nbRows_,r1, r2, v);}
    void deleteRows(number_t r1, number_t r2, std::vector<Matrix<complex_t> >& v, SymType s)
    {deleteRowsT(colPointer_,rowIndex_,nbCols_,nbRows_,r1, r2, v);}
    //@}

    //@{
    //! delete rows c1,..., c2 (may be expansive for some storage)
    void deleteCols(number_t c1, number_t c2, std::vector<real_t>& v, SymType s)
    {deleteColsT(colPointer_,rowIndex_,nbCols_,nbRows_,c1, c2, v);}
    void deleteCols(number_t c1, number_t c2, std::vector<complex_t>& v, SymType s)
    {deleteColsT(colPointer_,rowIndex_,nbCols_,nbRows_,c1, c2, v);}
    void deleteCols(number_t c1, number_t c2, std::vector<Matrix<real_t> >& v, SymType s)
    {deleteColsT(colPointer_,rowIndex_,nbCols_,nbRows_,c1, c2, v);}
    void deleteCols(number_t c1, number_t c2, std::vector<Matrix<complex_t> >& v, SymType s)
    {deleteColsT(colPointer_,rowIndex_,nbCols_,nbRows_,c1, c2, v);}
    //@}

    //! move from current storage to newstorage, return false if not available
    template<typename T>
    bool toStorageT(const MatrixStorage& newsto, std::vector<T>& newval, const std::vector<T>& oldval) const;
    //!< move matrix from from current storage to an other one, return false if not available
    bool toStorage(const MatrixStorage& newsto, std::vector<real_t>& newval, const std::vector<real_t>& oldval) const
    {return toStorageT(newsto, newval, oldval);}
    bool toStorage(const MatrixStorage& newsto, std::vector<complex_t>& newval, const std::vector<complex_t>& oldval) const
    {return toStorageT(newsto, newval, oldval);}
    bool toStorage(const MatrixStorage& newsto, std::vector<Matrix<real_t> >& newval, const std::vector<Matrix<real_t> >& oldval) const
    {return toStorageT(newsto, newval, oldval);}
    bool toStorage(const MatrixStorage& newsto, std::vector<Matrix<complex_t> >& newval, const std::vector<Matrix<complex_t> >& oldval) const
    {return toStorageT(newsto, newval, oldval);}

    // print utilities
    void print(std::ostream&) const;                                     //!< print storage pointers
    void printEntries(std::ostream&, const std::vector<real_t>&,
                      number_t vb, const SymType sym) const;             //!< print real scalar matrix
    void printEntries(std::ostream&, const std::vector<complex_t>&,
                      number_t vb, const SymType sym) const;             //!< print complex scalar matrix
    void printEntries(std::ostream&, const std::vector<Matrix<real_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of real matrices
    void printEntries(std::ostream&, const std::vector<Matrix<complex_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of complex matrices
    void printEntries(std::ostream&, const std::vector<Vector<real_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of real vectors (not available)
    void printEntries(std::ostream&, const std::vector<Vector<complex_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of complex vectors (not available)
    void printCooMatrix(std::ostream&, const std::vector<real_t>&,
                        SymType s = _noSymmetry) const;       //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector<complex_t>&,
                        SymType s = _noSymmetry) const;       //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector< Matrix<real_t> >&,
                        SymType s = _noSymmetry) const;       //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector< Matrix<complex_t> >&,
                        SymType s = _noSymmetry) const;       //!< output matrix of complex scalars in coordinate form

    void print(PrintStream& os) const
      {print(os.currentStream());}
    void printEntries(PrintStream& os, const std::vector<real_t>& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<complex_t>& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Matrix<real_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Matrix<complex_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Vector<real_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Vector<complex_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printCooMatrix(PrintStream& os, const std::vector<real_t>& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector<complex_t>& m,SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector< Matrix<real_t> >& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector< Matrix<complex_t> >& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}

    // load from file utilities
    void loadFromFileDense(std::istream& ifs, std::vector<real_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileDense(ifs, mat, rowIndex_, colPointer_, sym, rAsC);}
    void loadFromFileDense(std::istream& ifs, std::vector<complex_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileDense(ifs, mat, rowIndex_, colPointer_, sym, rAsC);}
    void loadFromFileCoo(std::istream& ifs, std::vector<real_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileCoo(ifs, mat, rowIndex_, colPointer_, sym, rAsC);}
    void loadFromFileCoo(std::istream& ifs, std::vector<complex_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileCoo(ifs, mat, rowIndex_, colPointer_, sym, rAsC);}

    // template Matrix x Vector & Vector x Matrix multiplications and specializations (see below)
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< templated col cs Matrix x Vector
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< templated Vector x col cs Matrix
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>& m, V* vp, R* rp) const;                         //!< templated Vector x col cs Matrix (pointer form)
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>& m, V* vp, R* rp) const;                         //!< templated Vector x col cs Matrix (pointer form)

    //@{
    // Matrix (Scalar) * Vector (Scalar)
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv); }
    //@}

    //@{
    // Matrix (Matrix) * Vector (Vector)
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    //@}

    //@{
    // Vector (Scalar) * Matrix (Scalar)
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    //@}

    //@{
    // Matrix (Matrix) * Vector (Vector)
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    //@}

    //@{
    //Matrix * Vector (pointer form)
    void multMatrixVector(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    //@}

    //@{
    //Vector * Matrix  (pointer form)
    void multVectorMatrix(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    //@}

    // template Matrix + Matrix addition and specializations (see below)
    template<typename M1, typename M2, typename R>
    void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const; //!< templated row dense Matrix + Matrix

    // Matrix+Matrix
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv) const
    { addMatrixMatrix<>(m, v, rv);}
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }

    //! conversion to umfpack format
    template<typename M, typename OrdinalType>
    void toUmfPack(const std::vector<M>& values, std::vector<OrdinalType>& colPointer, std::vector<OrdinalType>& rowIndex, std::vector<M>& mat) const;

    void toUmfPack(const std::vector<real_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<real_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat);}
    void toUmfPack(const std::vector<complex_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<complex_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat);}


    //! Incomplete matrix factorization with ILU
    template<typename M>
    void ilu(std::vector<M>& m, std::vector<M>& fa) const;

    template<typename M>
    void iluParallel(std::vector<M>& m, std::vector<M>& fa) const;

    //@{
    //! specializations of template ILU
    void ilu(std::vector<real_t>& m, std::vector<real_t>& fa, const SymType sym = _noSymmetry) const
    {
// #ifdef XLIFEPP_WITH_OMP
//         iluParallel<>(m, fa);
// #else
        ilu<>(m, fa);
// #endif
    }

    void ilu(std::vector<complex_t>& m, std::vector<complex_t>& fa, const SymType sym = _noSymmetry) const
    {
// #ifdef XLIFEPP_WITH_OMP
//         iluParallel<>(m, fa);
// #else
        ilu<>(m, fa);
// #endif
    }
    //@}

};

// constructor from the colNumbers (column index in cols starts at 1)
//   cols[k] of type L stores the indices of column of non zero element on row k (index starts at 1)
//   only requirement on L are the definition of forward const iterator named::const_iterator
//                             and the size
//   works for L = vector<number_t>, list<number_t>, set<number_t>
template<class L>
ColCsStorage::ColCsStorage(number_t nr, number_t nc, const std::vector<L>& rows, string_t id)
  : CsStorage(nr, nc, _col, id)
{
  trace_p->push("ColCsStorage constructor");
  if(rows.size()==nbCols_) buildCsStorage(rows, rowIndex_, colPointer_); //construct rowIndex_ and colPointer_
  else //adjust rows vector to fit nbCols_
  {
      std::vector<L> fitrows(nbCols_, L());
      typename std::vector<L>::iterator itf=fitrows.begin();
      typename std::vector<L>::const_iterator itc=rows.begin();
      number_t i=0;
      for(;itc!=rows.end() && i<nbRows_; ++itc, ++itf, ++i) *itf=*itc;
      buildCsStorage(fitrows, rowIndex_, colPointer_); //construct colIndex_ and rowPointer_
  }
  trace_p->pop();
}

//!< move matrix from from current storage (_csCol) to an other one (only csCol for the moment), return false if not available
// current storage and new storage have to address matrix with same sizes and current storage must be included in new storage
template<typename T>
bool ColCsStorage::toStorageT(const MatrixStorage& newsto, std::vector<T>& newval, const std::vector<T>& oldval) const
{
    if(this==&newsto) return true; //same storage, nothing to do !
    if(newsto.accessType()!=_col || newsto.storageType()!=_cs) return false;
    const ColCsStorage& newcscol = reinterpret_cast<const ColCsStorage&>(newsto);
    number_t nbcol=colPointer_.size()-1;
    if(colPointer_.size()!=newcscol.colPointer_.size())
        error("free_error"," ColCsStorage::toStorage: old and new CsCol storage are not consistent");
    trace_p->push("ColCsStorage::toStorage");
    if(newval.size() != newsto.size() + 1) newval.assign(newsto.size() + 1, 0.*oldval[0]);  // safe reallocation

    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for schedule(dynamic)
    #endif
    for(number_t c=0; c<nbcol; c++)  // loop on column of current storage
    {
      number_t cb = colPointer_[c], ncb = newcscol.colPointer_[c];
      std::vector<number_t>::const_iterator itb=rowIndex_.begin() + cb,
                                            ite=rowIndex_.begin() + colPointer_[c+1],
                                            it=itb;
      std::vector<number_t>::const_iterator nitb=newcscol.rowIndex_.begin() + ncb,
                                            nite=newcscol.rowIndex_.begin() + newcscol.colPointer_[c+1],
                                            nit=nitb;
      number_t k=0, nk=0;
      while(it != ite)  //loop on rows of current storage
      {
          number_t r=*it;
          bool found=false;
          while(!found && nit!=nite) // loop on rows of new storage
          {
            if(*nit==r) found=true;
            nit++; nk++;
          }
          newval[ncb+nk]=oldval[cb+k+1];
          it++; k++;
      }
    }
    trace_p->pop();
    return true;
}

// fill current values (csval)in skyline val (skyval), using the function skysto->pos(i,j)
// skysto has to be allocated with a symSkyline or a dualSkyline (not checked here)
template <typename T>
void ColCsStorage::fillSkylineValuesT(const std::vector<T>& csval, std::vector<T>& skyval, SymType sym, MatrixStorage* skysto) const
{
  auto itr=rowIndex_.begin();
  auto itv=csval.begin()+1;
  bool lower = skysto->upperPartSize()==0;
  for(number_t c=0;c<nbCols_;c++)
  {
    for(number_t k=0;k<colPointer_[c+1]-colPointer_[c];k++,itr++, itv++)
    {
      number_t r=*itr;
      if(r>=c || (r<c && !lower) )
      {
        number_t p=skysto->pos(r+1,c+1);
        skyval[p]=*itv;
      }
    }
  }
}

} // end of namespace xlifepp

#endif // COL_CS_STORAGE_HPP


