/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DualCsStorage.hpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Definition of the xlifepp::DualCsStorage class

  xlifepp::DualCsStorage class is the child class of xlifepp::CsStorage class dealing with Compressed Dual Row/Col Storage

  The diagonal is always first stored, then the lower triangular part in CSR, then the upper triangular part in CSC
  NOTE: the first entry (say (1,1)) is stored at address 1 in values vector (not at adress 0!)

  - colIndex_: for lower part, vector of col index of non zero value by stored row-wise
  - rowPointer_: for lower part, vector of positions of beginning of rows in previous vector
  - rowIndex_: for upper part, vector of row index of non zero value stored column-wise
  - colPointer_: for upper part, vector of positions of beginning of columns in previous vector


  Example: The following 5 x 6 non symmetric matrix
  -------
  \verbatim
  0  d x . x . x     ndiag       = 5
  1  . d x . . x     colIndex_   = [ 0 1  2  0 2 3 ]
  2  x x d . . .     rowPointer_ = [ 0 0 0 2 3 6 ]    (last gives the number of non zeros in the strict lower part)
  3  . . x d . .     rowIndex_   = [ 0  1  0  0 1 ]
  4  x . x x d .     colPointer_ = [ 0 0 1 2 2 3 5 ]  (last gives the number of non zeros in the strict upper part)
  \endverbatim

  Note: this is based on true CSR/CSC, contrary to Daniel's choice
       : for an empty row, rowPointer_is equal to the previous one: length of row i = rowPointer_[i+1]-rowPointer_[i]
       : for an empty col, colPointer_is equal to the previous one: length of col j = colPointer_[j+1]-colPointer_[j]
*/

#ifndef DUAL_CS_STORAGE_HPP
#define DUAL_CS_STORAGE_HPP

#include "config.h"
#include "CsStorage.hpp"

namespace xlifepp
{

/*!
   \class DualCsStorage
   child class for dual row/col compressed sparse storage
 */

class DualCsStorage : public CsStorage
{
  protected:
    std::vector<number_t> colIndex_;   //!< vector of column index of non zero value (lower part)
    std::vector<number_t> rowPointer_; //!< vector of positions of begining of rows
    std::vector<number_t> rowIndex_;   //!< vector of row index of non zero value (upper part)
    std::vector<number_t> colPointer_; //!< vector of positions of begining of cols

  public:
    // constructor, destructor
    DualCsStorage(number_t nr = 0, number_t nc = 0, string_t id="DualCsStorage"); //!< default constructor
    DualCsStorage(number_t, number_t,
                  const std::vector<number_t>&, const std::vector<number_t>&,
                  const std::vector<number_t>&, const std::vector<number_t>&,
                  string_t id="DualCsStorage");                               //!< explicit constructor
    ~DualCsStorage() {}                                                       //!< destructor


    DualCsStorage(number_t, number_t, const std::vector< std::vector<number_t> >&,
                  const std::vector< std::vector<number_t> >&, string_t id="DualCsStorage"); //!< constructor by a pair of global numerotation vectors
    template<class L>
    DualCsStorage(number_t, number_t, const std::vector<L>&, string_t id="DualCsStorage"); //!< constructor by the list of col index by rows
    template<class L>
    void buildStorage(const std::vector<L>&);                      //!< construct storage vectors from column indices
    DualCsStorage* clone() const                                   //! create a clone (virtual copy constructor, covariant)
    {return new DualCsStorage(*this);}
    DualCsStorage* toScalar(dimen_t, dimen_t);                     //!< create a new scalar DualCs storage from current DualCs storage and submatrix sizes
    virtual void clear()                                           //! clear storage vectors
    {rowPointer_.clear(); colIndex_.clear(); rowPointer_.clear(); colPointer_.clear();}

    // utilities
    number_t size() const                                        //! number of non zero elements stored
    {return std::min(nbRows_, nbCols_) + rowIndex_.size() + colIndex_.size();}
    number_t lowerPartSize() const {return colIndex_.size();}    //!< returns number of entries of lower triangular part
    number_t upperPartSize() const {return rowIndex_.size();}    //!< returns number of entries of upper triangular part
    bool sameStorage(const MatrixStorage&) const;                //!< check if two storages have the same structures

    // access operator
    number_t pos(number_t i, number_t j, SymType s = _noSymmetry) const;       //!< returns adress of entry (i,j)
    void positions(const std::vector<number_t>&, const std::vector<number_t>&,
                   std::vector<number_t>&, bool errorOn = true,
                   SymType s = _noSymmetry) const;                       //!< access to submatrix positions
    std::vector<std::pair<number_t, number_t> > getCol(SymType s, number_t c, number_t r1=1, number_t r2=0) const; //!< get (row indices, adress) of col c in set [r1,r2]
    std::vector<std::pair<number_t, number_t> > getRow(SymType s, number_t r, number_t c1=1, number_t c2=0) const; //!< get (col indices, adress) of row r in set [c1,c2]
    std::set<number_t> getCols(number_t r, number_t c1=1, number_t c2=0) const;                       //!< get col indices of row r in set [c1,c2]
    std::set<number_t> getRows(number_t c, number_t r1=1, number_t r2=0) const;                       //!< get row indices of col c in set [r1,r2]
    void getRowsV(std::vector<number_t>&, number_t&, number_t c, number_t r1=1, number_t r2=0) const;   //!< get col indices of row r in set [c1,c2] (no allocation)
    void getColsV(std::vector<number_t>&, number_t&, number_t r, number_t c1=1, number_t c2=0) const;   //!< get row indices of col c in set [r1,r2] (no allocation)
    //modifying tools
    void addSubMatrixIndices(const std::vector<number_t>&,const std::vector<number_t>&);   //!< add dense submatrix indices in storage
    void addRow(number_t, const std::set<number_t>&, MatrixPart);                          //!< add row-cols (r,c1), (r,c2), ... given by a cols set (r,c>=1)
    void addCol(number_t, const std::set<number_t>&, MatrixPart);                          //!< add col-rows (r1,c), (r2,c), ... given by a cols set (r,c>=1)

    //skyline conversion tools
    std::vector<number_t> skylineRowPointer() const; //!< return skyline row pointer from current DualCsStorage pointers
    std::vector<number_t> skylineColPointer() const; //!< return skyline col pointer from current DualCsStorage pointers
    void fillSkylineValues(const std::vector<real_t>&, std::vector<real_t>&,SymType,MatrixStorage*) const;                         //!< fill real values of current storage as a skyline storage
    void fillSkylineValues(const std::vector<complex_t>&, std::vector<complex_t>&,SymType,MatrixStorage*) const;                   //!< fill complex values of current storage as a skyline storage
    void fillSkylineValues(const std::vector<Matrix<real_t> >&, std::vector<Matrix<real_t> >&,SymType,MatrixStorage*) const;       //!< fill values of current storage as a skyline storage
    void fillSkylineValues(const std::vector<Matrix<complex_t> >&, std::vector<Matrix<complex_t> >&,SymType,MatrixStorage*) const; //!< fill values of current storage as a skyline storage

    // print utilities
    void print(std::ostream&) const;                                   //!< print storage pointers
    void printEntries(std::ostream&, const std::vector<real_t>&,
                      number_t vb, const SymType sym) const;             //!< print real scalar matrix
    void printEntries(std::ostream&, const std::vector<complex_t>&,
                      number_t vb, const SymType sym) const;             //!< print complex scalar matrix
    void printEntries(std::ostream&, const std::vector<Matrix<real_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of real matrices
    void printEntries(std::ostream&, const std::vector<Matrix<complex_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of complex matrices
    void printEntries(std::ostream&, const std::vector<Vector<real_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of real vectors (not available)
    void printEntries(std::ostream&, const std::vector<Vector<complex_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of complex vectors (not available)
    void printCooMatrix(std::ostream&, const std::vector<real_t>&,
                        SymType s = _noSymmetry) const;       //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector<complex_t>&,
                        SymType s = _noSymmetry) const;       //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector< Matrix<real_t> >&,
                        SymType s = _noSymmetry) const;       //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector< Matrix<complex_t> >&,
                        SymType s = _noSymmetry) const;       //!< output matrix of complex scalars in coordinate form

    void print(PrintStream& os) const
      {print(os.currentStream());}
    void printEntries(PrintStream& os, const std::vector<real_t>& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<complex_t>& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Matrix<real_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Matrix<complex_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Vector<real_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Vector<complex_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printCooMatrix(PrintStream& os, const std::vector<real_t>& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector<complex_t>& m,SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector< Matrix<real_t> >& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector< Matrix<complex_t> >& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}

    // load file utilities
    //! load from dense matrix in file
    void loadFromFileDense(std::istream& ifs, std::vector<real_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileDense(ifs, mat, colIndex_, rowPointer_, rowIndex_, colPointer_, sym, rAsC);}
    //! load from dense matrix in file
    void loadFromFileDense(std::istream& ifs, std::vector<complex_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileDense(ifs, mat, colIndex_, rowPointer_, rowIndex_, colPointer_, sym, rAsC);}
    //! load from coordinate matrix in file
    void loadFromFileCoo(std::istream& ifs, std::vector<real_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileCoo(ifs, mat, colIndex_, rowPointer_, rowIndex_, colPointer_, sym, rAsC);}
    //! load from coordinate matrix in file
    void loadFromFileCoo(std::istream& ifs, std::vector<complex_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileCoo(ifs, mat, colIndex_, rowPointer_, rowIndex_, colPointer_, sym, rAsC);}

    // template Matrix x Vector & Vector x Matrix multiplications and specializations (see below)
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< templated dualcs Matrix x Vector
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< templated Vector x dualcs Matrix
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>& m, V*, R*) const;       //!< templated dualcs Matrix x Vector (pointer)
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>& m, V*, R*) const;       //!< templated Vector x dualcs Matrix (pointer)

    //@{
    //! Matrix (Scalar) * Vector (Scalar)
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    //@}

    //@{
    //! Vector (Scalar) * Matrix (Scalar)
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    //@}

    //@{
    //! Matrix * Vector (pointer form)
    void multMatrixVector(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const         //! templated dualcs Matrix x Vector (pointer)
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const   //! templated dualcs Matrix x Vector (pointer)
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const   //! templated dualcs Matrix x Vector (pointer)
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const//! templated dualcs Matrix x Vector (pointer)
    { multMatrixVector<>(m, vp, rp); }
    //@}

    //@{
    //! Vector * Matrix (pointer form)
    void multVectorMatrix(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const         //! templated Vector x dualcs Matrix (pointer)
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const   //! templated Vector x dualcs Matrix (pointer)
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const   //! templated Vector x dualcs Matrix (pointer)
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const//! templated Vector x dualcs Matrix (pointer)
    { multVectorMatrix<>(m, vp, rp); }
    //@}

    // template Matrix + Matrix addition and specializations (see below)
    template<typename M1, typename M2, typename R>
    void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const; //!< templated row dense Matrix + Matrix

    //@{
    //! Matrix+Matrix
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv) const
    { addMatrixMatrix<>(m, v, rv);}
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    //@}

    //! Incomplete matrix factorization with ILU
    template<typename M>
    void ilu(std::vector<M>& m, std::vector<M>& fa) const;

    template<typename M>
    void iluParallel(std::vector<M>& m, std::vector<M>& fa) const;

    //@{
    //! specializations of template LU
    void ilu(std::vector<real_t>& m, std::vector<real_t>& fa, const SymType sym = _noSymmetry) const
    {
// #ifdef XLIFEPP_WITH_OMP
//         iluParallel<>(m, fa);
// #else
        ilu<>(m, fa);
// #endif
    }

    void ilu(std::vector<complex_t>& m, std::vector<complex_t>& fa, const SymType sym = _noSymmetry) const
    {
// #ifdef XLIFEPP_WITH_OMP
//         iluParallel<>(m, fa);
// #else
        ilu<>(m, fa);
// #endif
    }
    //@}
    /* ----------------------------------------------------------------------------------------------------------------------------
                                                     SOR and SSOR stuff
    ----------------------------------------------------------------------------------------------------------------------------*/
    /*! special template partial Matrix x Vector multiplications [w*D] v used in SSOR algorithm
      for D + L + U matrix splitting (D diagonal, L & U strict lower & upper trangular parts) */
    template<typename M, typename V, typename R>
    void sorDiagonalMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, const real_t w) const
    {
      // SOR-like [w*D] v diagonal Matrix x Vector multiplication
      typename std::vector<M>::const_iterator it_m = m.begin() + 1;
      typename std::vector<V>::const_iterator it_v = v.begin();
      typename std::vector<R>::iterator it_r = r.begin();
      bzSorDiagonalMatrixVector(it_m, it_v, it_r, w);
    }

    //@{
    //! specializations of partial matrix std::vector multiplications
    void sorDiagonalMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, const real_t w) const
    { sorDiagonalMatrixVector<>(m, v, rv, w); }
    void sorDiagonalMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w) const
    { sorDiagonalMatrixVector<>(m, v, rv, w); }
    void sorDiagonalMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, const real_t w) const
    { sorDiagonalMatrixVector<>(m, v, rv, w); }
    void sorDiagonalMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w) const
    { sorDiagonalMatrixVector<>(m, v, rv, w); }
    //@}

    /*! special template partial Matrix x Vector multiplications [w*D+L] v used in SSOR algorithm
            for D + L + U matrix splitting (D diagonal, L & U strict lower & upper trangular parts)  */
    template<typename M, typename V, typename R>
    void sorLowerMatrixVector(const std::vector<M>& m, const std::vector<V>& v,
                              std::vector<R>& r, const real_t w, const SymType sym) const
    {
      typename std::vector<M>::const_iterator it_m = m.begin() + 1;
      typename std::vector<V>::const_iterator it_vb = v.begin();
      typename std::vector<R>::iterator it_rb = r.begin();
      bzSorDiagonalMatrixVector(it_m, it_vb, it_rb, w); // it_m now equals it_l
      lowerMatrixVector(colIndex_, rowPointer_, it_m, it_vb, it_rb, sym);
    }

    //@{
    //! specializations of partial matrix vector multiplications
    void sorLowerMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, const real_t w, const SymType sym) const
    { sorLowerMatrixVector<>(m, v, rv, w, sym); }
    void sorLowerMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorLowerMatrixVector<>(m, v, rv, w, sym); }
    void sorLowerMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorLowerMatrixVector<>(m, v, rv, w, sym); }
    void sorLowerMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorLowerMatrixVector<>(m, v, rv, w, sym); }
    //@}

  /*! special template partial Matrix x Vector multiplications [w*D+L] v used in SSOR algorithm
            for D + L + U matrix splitting (D diagonal, L & U strict lower & upper trangular parts)  */
    template<typename M, typename V, typename R>
    void lowerD1MatrixVector(const std::vector<M>& m, const std::vector<V>& v,
                              std::vector<R>& r, const SymType sym) const
    {
      typename std::vector<M>::const_iterator it_m = m.begin() + 1;
      typename std::vector<V>::const_iterator it_vb = v.begin(), itv = it_vb;
      typename std::vector<R>::iterator it_rb = r.begin(),itr=it_rb;
//       bzSorDiagonalMatrixVector(it_m, it_vb, it_rb, w); // it_m now equals it_l
    for(number_t i=0;i<std::min(nbRows_,nbCols_); i++, ++itv,++itr) *itr = *itv; //diag part
//     itmb = m.begin() + 1 + diagonalSize();
      it_m += rowPointer_.size()-1;
      CsStorage::lowerMatrixVector(colIndex_, rowPointer_, it_m, it_vb, it_rb, sym);
    }
//@{
    //! specializations of partial matrix vector multiplications
    void lowerD1MatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, const SymType sym) const
    {lowerD1MatrixVector<>(m, v, rv, sym); }
    void lowerD1MatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const SymType sym) const
    { lowerD1MatrixVector<>(m, v, rv, sym); }
    void lowerD1MatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, const SymType sym) const
    { lowerD1MatrixVector<>(m, v, rv, sym); }
    void lowerD1MatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const SymType sym) const
    { lowerD1MatrixVector<>(m, v, rv, sym); }
    //@}

    /*! special template partial Matrix x Vector multiplications [w*D+U] v used in SSOR algorithm
        for D + L + U matrix splitting (D diagonal, L & U strict lower & upper trangular parts) */
    template<typename M, typename V, typename R>
    void sorUpperMatrixVector(const std::vector<M>& m, const std::vector<V>& v,
                              std::vector<R>& r, const real_t w, const SymType sym) const
    {
      typename std::vector<M>::const_iterator it_m = m.begin() + 1;
      typename std::vector<V>::const_iterator it_vb = v.begin();
      typename std::vector<R>::iterator it_rb = r.begin();
      bzSorDiagonalMatrixVector(it_m, it_vb, it_rb, w); // it_m now equals it_l
      it_m += colIndex_.size(); //move to it_u
      CsStorage::upperMatrixVector(rowIndex_, colPointer_, it_m, it_vb, it_rb, sym);
    }

    //@{
    //! specializations of partial matrix vector multiplications
    void sorUpperMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, const real_t w, const SymType sym) const
    { sorUpperMatrixVector<>(m, v, rv, w, sym); }
    void sorUpperMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorUpperMatrixVector<>(m, v, rv, w, sym); }
    void sorUpperMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorUpperMatrixVector<>(m, v, rv, w, sym); }
    void sorUpperMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorUpperMatrixVector<>(m, v, rv, w, sym); }
    //@}

 template<typename M, typename V, typename R>
    void upperMatrixVector(const std::vector<M>& m, const std::vector<V>& v,
                              std::vector<R>& r, const SymType sym) const
    {
      typename std::vector<M>::const_iterator it_m = m.begin() + 1;
      typename std::vector<V>::const_iterator it_vb = v.begin();
      typename std::vector<R>::iterator it_rb = r.begin();
      bzSorDiagonalMatrixVector(it_m, it_vb, it_rb, 1.); // it_m now equals it_l
      it_m += colIndex_.size(); //move to it_u
      CsStorage::upperMatrixVector(rowIndex_, colPointer_, it_m, it_vb, it_rb, sym);
    }

    //@{
    //! specializations of partial matrix vector multiplications
    void upperMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, const SymType sym) const
    { upperMatrixVector<>(m, v, rv, sym); }
    void upperMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const SymType sym) const
    { upperMatrixVector<>(m, v, rv, sym); }
    void upperMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, const SymType sym) const
    { upperMatrixVector<>(m, v, rv, sym); }
    void upperMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const SymType sym) const
    { upperMatrixVector<>(m, v, rv, sym); }
    //@}

    /*! special template diagonal and triangular solvers D/w x = b used in SSOR algorithm
        for D + L + U matrix splitting (D diagonal, L & U strict lower & upper triangular parts) */
    template<typename M, typename V, typename R>
    void sorDiagonalSolver(const std::vector<M>& m, const std::vector<R>& b,
                           std::vector<V>& x, const real_t w) const
    {
      typename std::vector<M>::const_iterator it_m = m.begin() + 1;
      typename std::vector<R>::const_iterator it_b = b.begin();
      typename std::vector<V>::iterator it_xb = x.begin(), it_xe = x.end();
      bzSorDiagonalSolver(it_m, it_b, it_xb, it_xe, w);
    }

    //@{
    //! specializations of diagonal solvers
    void sorDiagonalSolver(const std::vector<real_t>& m, const std::vector<real_t>& b, std::vector<real_t>& x, const real_t w) const
    { sorDiagonalSolver<>(m, b, x, w); }
    void sorDiagonalSolver(const std::vector<real_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorDiagonalSolver<>(m, b, x, w); }
    void sorDiagonalSolver(const std::vector<complex_t>& m, const std::vector<real_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorDiagonalSolver<>(m, b, x, w); }
    void sorDiagonalSolver(const std::vector<complex_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorDiagonalSolver<>(m, b, x, w); }
    //@}

    /*! special template diagonal and triangular solvers (D/w+L) x = b used in SSOR algorithm
          for D + L + U matrix splitting (D diagonal, L & U strict lower & upper triangular parts) */
    template<typename M, typename V, typename R>
    void sorLowerSolver(const std::vector<M>& m, const std::vector<R>& b,
                        std::vector<V>& x, const real_t w) const
    {
      typename std::vector<M>::const_iterator it_m = m.begin() + 1, it_l = it_m + diagonalSize();
      typename std::vector<R>::const_iterator it_b = b.begin();
      typename std::vector<V>::iterator it_xb = x.begin(), it_xe = x.end();
      bzSorLowerSolver(it_m, it_l, it_b, it_xb, it_xe, colIndex_, rowPointer_, w);
    }

    //@{
    //! specializations of lower triangular part solvers
    void sorLowerSolver(const std::vector<real_t>& m, const std::vector<real_t>& b, std::vector<real_t>& x, const real_t w) const
    { sorLowerSolver<>(m, b, x, w); }
    void sorLowerSolver(const std::vector<real_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorLowerSolver<>(m, b, x, w); }
    void sorLowerSolver(const std::vector<complex_t>& m, const std::vector<real_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorLowerSolver<>(m, b, x, w); }
    void sorLowerSolver(const std::vector<complex_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorLowerSolver<>(m, b, x, w); }
    //@}
    /*!
       special template diagonal and triangular solvers
       (D/w+U) x = b used in SSOR algorithm
       for D + L + U matrix splitting (D diagonal, L & U strict lower & upper triangular parts)
    */
    template<typename M, typename V, typename R>
    void sorUpperSolver(const std::vector<M>& m, const std::vector<R>& b,
                        std::vector<V>& x, const real_t w, const SymType sym) const
    {
      // *** LO AND BEHOLD we use reverse_iterators here
      typename std::vector<M>::const_reverse_iterator it_ru = m.rbegin(), it_rd = it_ru + upperPartSize() + lowerPartSize();
      typename std::vector<R>::const_reverse_iterator it_rbb = b.rbegin();
      typename std::vector<V>::reverse_iterator it_rxb = x.rbegin(), it_rxe = x.rend();
      bzSorUpperSolver(it_rd, it_ru, it_rbb, it_rxb, it_rxe, rowIndex_, colPointer_, w);
    }
    //@{
    //! specializations of upper triangular part solvers
    void sorUpperSolver(const std::vector<real_t>& m, const std::vector<real_t>& b, std::vector<real_t>& x, const real_t w, const SymType sym) const
    { sorUpperSolver<>(m, b, x, w, sym); }
    void sorUpperSolver(const std::vector<real_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w, const SymType sym) const
    { sorUpperSolver<>(m, b, x, w, sym); }
    void sorUpperSolver(const std::vector<complex_t>& m, const std::vector<real_t>& b, std::vector<complex_t>& x, const real_t w, const SymType sym) const
    { sorUpperSolver<>(m, b, x, w, sym); }
    void sorUpperSolver(const std::vector<complex_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w, const SymType sym) const
    { sorUpperSolver<>(m, b, x, w, sym); }
    //@}

    /* ----------------------------------------------------------------------------------------------------------------------------
                                                         Direct Solve after factorization L.D.Lt, L.D.U, LLt
       ----------------------------------------------------------------------------------------------------------------------------*/
     /*!
       Template  triangular part solvers (after factorization)
       Lower triangular with unit diagonal linear system solver: (I+L) x = b
     */
    template<typename M, typename V, typename X>
    void lowerD1Solver(const std::vector<M>& m,  std::vector<V>& v, std::vector<X>& x) const;
    //@{
    //! Specializations of lower triangular part with unit diagonal linear solvers (I + L) x = v
    void lowerD1Solver(const std::vector<real_t>& m,  std::vector<real_t>& v, std::vector<real_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<real_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<complex_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    //@}

    /*!
      Template  triangular part solvers (after factorization)
      Lower triangular with unit diagonal linear system solver: (D+L) x = b
    */
    template<typename M, typename V, typename X>
    void lowerSolver(const std::vector<M>& m,  std::vector<V>& v, std::vector<X>& x) const;
    //@{
    //! Specializations of lower triangular part with diagonal linear solvers (D + L) x = v
    void lowerSolver(const std::vector<real_t>& m,  std::vector<real_t>& v, std::vector<real_t>& x) const
    { lowerSolver<>(m, v, x); }
    void lowerSolver(const std::vector<real_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerSolver<>(m, v, x); }
    void lowerSolver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x) const
    { lowerSolver<>(m, v, x); }
    void lowerSolver(const std::vector<complex_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerSolver<>(m, v, x); }
    //@}
    /*!
      Template  triangular part solvers (after factorization)
      Upper triangular with diagonal linear system solver: (D+U) x = b
    */
    template<typename M, typename V, typename X>
    void upperSolver(const std::vector<M>& m,  std::vector<V>& v, std::vector<X>& x, SymType sym) const;
    //@{
    //! Specializations of upper triangular part with  diagonal linear solvers (D + U) x = v
    void upperSolver(const std::vector<real_t>& m,  std::vector<real_t>& v, std::vector<real_t>& x, SymType sym) const
    { upperSolver<>(m, v, x,  sym); }
    void upperSolver(const std::vector<real_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x, SymType sym) const
    { upperSolver<>(m, v, x,  sym); }
    void upperSolver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x, SymType sym) const
    { upperSolver<>(m, v, x,  sym); }
    void upperSolver(const std::vector<complex_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x, SymType sym) const
    { upperSolver<>(m, v, x,  sym); }
    //@}
    /*!
       Template  triangular part solvers (after factorization)
       Lower triangular with unit diagonal transposed linear system solver: (I+L)t x = b
     */
    template<typename M, typename V, typename X>
    void lowerD1LeftSolver(const std::vector<M>& m,  std::vector<V>& v, std::vector<X>& x) const;
    //@{
    //! Specializations of lower triangular part with unit diagonal linear solvers (I + L)t x = v
    void lowerD1LeftSolver(const std::vector<real_t>& m,  std::vector<real_t>& v, std::vector<real_t>& x) const
    { lowerD1LeftSolver<>(m, v, x); }
    void lowerD1LeftSolver(const std::vector<real_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerD1LeftSolver<>(m, v, x); }
    void lowerD1LeftSolver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x) const
    { lowerD1LeftSolver<>(m, v, x); }
    void lowerD1LeftSolver(const std::vector<complex_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerD1LeftSolver<>(m, v, x); }
    //@}
    /*!
      Template  triangular part solvers (after factorization)
      Upper triangular with diagonal transposed linear system solver: (D+U)t x = b
    */
    template<typename M, typename V, typename X>
    void upperLeftSolver(const std::vector<M>& m,  std::vector<V>& v, std::vector<X>& x, SymType sym=_noSymmetry) const;
    //@{
    //! Specializations of upper triangular part with diagonal linear solvers (D + U)t x = v
    void upperLeftSolver(const std::vector<real_t>& m,  std::vector<real_t>& v, std::vector<real_t>& x, SymType sym=_noSymmetry) const
    { upperLeftSolver<>(m, v, x,  sym); }
    void upperLeftSolver(const std::vector<real_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x, SymType sym=_noSymmetry) const
    { upperLeftSolver<>(m, v, x,  sym); }
    void upperLeftSolver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x, SymType sym=_noSymmetry) const
    { upperLeftSolver<>(m, v, x,  sym); }
    void upperLeftSolver(const std::vector<complex_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x, SymType sym=_noSymmetry) const
    { upperLeftSolver<>(m, v, x,  sym); }
    //@}


    /* ----------------------------------------------------------------------------------------------------------------------------
                                                         UMFPACK stuff
       ----------------------------------------------------------------------------------------------------------------------------*/
    //! conversion to toUmfPack format
    template<typename M, typename OrdinalType>
    void toUmfPack(const std::vector<M>& values, std::vector<OrdinalType>& colPointer, std::vector<OrdinalType>& rowIndex, std::vector<M>& mat) const;
    //@{
    //! specialization of umfpack conversion
    void toUmfPack(const std::vector<real_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<real_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat);}
    void toUmfPack(const std::vector<complex_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<complex_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat);}
    //@}
};

// constructor from the list of col index of non zero for each row
//   cols[k] of type L stores the indices of column of non zero element on row k (index starts at 1)
//   only requirement on L are the definition of forward const iterator named::const_iterator
//                             and the size
//   works for L = vector<number_t>, list<number_t>, set<number_t>
template<class L>
DualCsStorage::DualCsStorage(number_t nr, number_t nc, const std::vector<L>& cols, string_t id)
  : CsStorage(nr, nc, _dual, id)
{
  trace_p->push("DualCsStorage constructor");
  buildStorage(cols);
  trace_p->pop();
}

// constructor from the colNumbers (column index in cols starts at 1)
//   cols[k] of type L stores the indices of column of non zero element on row k (index starts at 1)
//   only requirement on L are the definition of forward const iterator named::const_iterator
//                             and the size
//   works for L = vector<number_t>, list<number_t>, set<number_t>
template<class L>
void DualCsStorage::buildStorage(const std::vector<L>& cols)
{
  trace_p->push("DualCsStorage constructor");
  // construct set of column index for strict lower part
  std::vector<std::vector<number_t> > lowup(nbRows_);
  typename std::vector<L>::const_iterator citvn;
  std::vector<std::vector<number_t> >::iterator itvn = lowup.begin();
  typename L::const_iterator cits;

  number_t r = 1;
  for(citvn = cols.begin(); citvn != cols.end(); citvn++, itvn++, r++)
    for(cits = citvn->begin(); cits != citvn->end(); cits++)
      if(*cits < r) { itvn->push_back(*cits); }
  buildCsStorage(lowup, colIndex_, rowPointer_); // construct rowIndex_ and colPointer_

  // construct set of column index for strict upper part
  lowup.clear();
  lowup.resize(nbCols_);
  itvn = lowup.begin();
  r = 1;
  for(citvn = cols.begin(); citvn != cols.end(); citvn++, itvn++, r++)
    for(cits = citvn->begin(); cits != citvn->end(); cits++)
      if(*cits > r) { lowup[*cits - 1].push_back(r); }
  buildCsStorage(lowup, rowIndex_, colPointer_); // construct rowIndex_ and colPointer_
  trace_p->pop();
}

} // end of namespace xlifepp

#endif // DUAL_CS_STORAGE_HPP
