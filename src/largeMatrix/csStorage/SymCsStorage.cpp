/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SymCsStorage.cpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Implementation of xlifepp::SymCsStorage class and functionnalities
*/



#include "SymCsStorage.hpp"
#include <list>
#include <functional>


namespace xlifepp
{
/*--------------------------------------------------------------------------------
  Constructors, Destructor
--------------------------------------------------------------------------------*/
//default constructor
SymCsStorage::SymCsStorage(number_t n, string_t id)
  : CsStorage(n, n, _sym, id)
{
  trace_p->push("SymCsStorage constructor");
  std::vector<std::vector<number_t> > cols(n);  // no index
  buildCsStorage(cols, colIndex_, rowPointer_); //construct rowIndex_ and colPointer_
  trace_p->pop();
}

// constructor by a pair of numbering vectors rowNumbers and colNumbers (same size)
//   rowNumbers[k] is the row numbers of submatrix k
//   colNumbers[k] is the col numbers of submatrix k
// this constructor is well adapted to FE matrix where submatrix is linked to an element

SymCsStorage::SymCsStorage(number_t n, const std::vector<std::vector<number_t> >& rowNumbers, const std::vector<std::vector<number_t> >& colNumbers, string_t id)
  : CsStorage(n, n, _sym, id)
{
  trace_p->push("SymCsStorage constructor");
  //build list of col index for each rows, only for lower part
  std::vector<std::set<number_t> > colset(n);  //temporary vector of col index
  std::vector<std::vector<number_t> >::const_iterator itrs, itcs = colNumbers.begin();
  std::vector<number_t>::const_iterator itr, itc;
  for(itrs = rowNumbers.begin(); itrs != rowNumbers.end(); itrs++, itcs++)
    {
      for(itr = itrs->begin(); itr != itrs->end(); itr++)
        for(itc = itcs->begin(); itc != itcs->end(); itc++)
          if(*itr > *itc) { colset[*itr - 1].insert(*itc); }
    }

  //convert set to vector
  std::vector<std::vector<number_t> > cols(nbRows_);   //temporary vector of col index
  std::vector<std::set<number_t> >::iterator itset;
  std::vector<std::vector<number_t> >::iterator itcols = cols.begin();
  for(itset = colset.begin(); itset != colset.end(); itset++, itcols++)
    { *itcols = std::vector<number_t>(itset->begin(), itset->end()); }
  buildCsStorage(cols, colIndex_, rowPointer_);        //construct storage
  trace_p->pop();
}

// create a new scalar SymCs storage from current SymCs storage and submatrix sizes (non optimal)
SymCsStorage* SymCsStorage::toScalar(dimen_t nbr, dimen_t nbc)
{
  if(nbr!=nbc) warning("free_warning"," in SymCsStorage::toScalar(nbr, nbc), nbr is different from nbc");
  MatrixStorage* nsto=findMatrixStorage(stringId, _cs, _sym, buildType_, true, nbr*nbRows_, nbc*nbCols_);
  if(nsto!=nullptr) return static_cast<SymCsStorage*>(nsto);
  std::vector<std::vector<number_t> > cols=scalarColIndices(nbr, nbc);
  SymCsStorage* ss = new SymCsStorage(nbr*nbRows_, cols, _all, stringId);
  ss->buildType() = buildType_;
  ss->scalarFlag()=true;
  return ss;
}

/*--------------------------------------------------------------------------------
  check if two storages have the same structures
--------------------------------------------------------------------------------*/
bool SymCsStorage::sameStorage(const MatrixStorage& sto) const
{
  if(!(sto.storageType()==storageType_ &&
       sto.accessType()==accessType_ &&
       sto.nbOfRows()==nbRows_ &&
       sto.nbOfColumns()==nbCols_&&
       sto.size()==size()))
    return false;
  const SymCsStorage& csto=static_cast<const SymCsStorage&>(sto);
  //compare structure pointers
  if(rowPointer_!=csto.rowPointer_) return false;
  if(colIndex_!=csto.colIndex_) return false;
  return true;
}

/*--------------------------------------------------------------------------------
 add submatrix indices given by its row and column indices (>= 1)
--------------------------------------------------------------------------------*/
void SymCsStorage::addSubMatrixIndices(const std::vector<number_t>& rows,const std::vector<number_t>& cols)
{
  addCsSubMatrixIndices(rowPointer_,colIndex_,rows,cols,true,false);
}

/*--------------------------------------------------------------------------------
  add row-cols (r,c1), (r,c2), ... given by a cols set (r,c>=1)
  MatrixPart not managed
-------------------------------------------------------------------------------*/
void SymCsStorage::addRow(number_t r, const std::set<number_t>& cols, MatrixPart mp)
{
  if(cols.size()==0) return;  //nothing to add
  number_t rb=rowPointer_[r-1], re=rowPointer_[r];
  std::set<number_t> allcols;
  //if(re>rb) allcols.insert(colIndex_.begin()+rb, colIndex_.begin()+re-1);
  if(re>rb) allcols.insert(colIndex_.begin()+rb, colIndex_.begin()+re);
  number_t nbc0=allcols.size();
  std::set<number_t>::iterator its=cols.begin();
  for(; its!=cols.end(); its++)
    if(*its < r) allcols.insert(*its -1); //add current cols (-1 shift)
  number_t nc=allcols.size()-nbc0;
  if(nc>0) //new cols added
    {
      std::vector<number_t>::iterator itc=colIndex_.begin()+re;
      colIndex_.insert(itc, nc, 0);          //insert nc element initialized to 0
      its=allcols.begin();
      itc=colIndex_.begin()+rb;
      for(; its!=allcols.end(); its++,itc++) *itc=*its; //copy new columns set
      std::vector<number_t>::iterator itr=rowPointer_.begin()+r;
      for(; itr!=rowPointer_.end(); itr++) *itr+=nc; //shift rowPointer
    }
}


/*--------------------------------------------------------------------------------
 access operator to position of (i,j)>0 coefficients (return 0 if outside)
 This DIRECT access to (i,j) pointer SHOULD NOT be used in matrix computation
--------------------------------------------------------------------------------*/
number_t SymCsStorage::pos(number_t i, number_t j, SymType s) const
{
  if(i == 0 || i > nbRows_ || j == 0 || j > nbCols_) { return 0; }
  if(i == j) { return i; }
  if(i > j) //in lower triangular part
    {
      for(number_t k = rowPointer_[i - 1]; k < rowPointer_[i]; k++) //find in list of col index of row i
        if(colIndex_[k] == j - 1) { return nbRows_ + k + 1; }     //add 1 because values begin at adress 1
      return 0;
    }
  //in upper triangular part
  for(number_t k = rowPointer_[j - 1]; k < rowPointer_[j]; k++) //find in list of col index of row i
    if(colIndex_[k] == i - 1)
      {
        if(s != _noSymmetry) { return nbRows_ + k + 1; }         //symmetric matrix
        else      { return nbRows_ + colIndex_.size() + k + 1; } //non symmetric matrix
      }

  return 0;
}

/*--------------------------------------------------------------------------------
 access to submatrix adresses, useful for assembling matrices
 rows and cols are vector of index (>0) of row and column, no index ordering is assumed !!
 return the vector of adresses (relative number >0 in storage) in a row storage (as Matrix)
 if a row or a col index is out of storage
     an error is raised if errorOn is true (default)
     position is set to 0 else
 if sy!=_noSymmetry: symmetric matrix, positions of lower part and upper part are returned
 be care when using it in matrix assembling
 remark: since the vector storage indexCol_ is ordered for each line, if the indices of columns
          of the submatrix were also, one could write a faster algorithm
--------------------------------------------------------------------------------*/
void SymCsStorage::positions(const std::vector<number_t>& rows, const std::vector<number_t>& cols,
                             std::vector<number_t>& adrs, bool errorOn, SymType sy) const
{
  number_t nba = rows.size() * cols.size();
  if(adrs.size() != nba) { adrs.resize(nba, 0); }    //reset size
  std::vector<number_t>::iterator itp = adrs.begin();
  std::vector<number_t>::const_iterator itr, itc;

  for(itr = rows.begin(); itr != rows.end(); itr++)  //loop on row index
    for(itc = cols.begin(); itc != cols.end(); itc++, itp++) //loop on col indices
      {
        *itp = pos(*itr, *itc, sy); //non optimal method
        if(*itp == 0 && errorOn) error("storage_outofstorage","SymCs", *itr, *itc);
      }
}

// get (col indices, address) of row r in set [c1,c2]
//   SymType != noSymmetry means that the upper part is not stored
//              so an upper part coefficient address is the address of its symmetric coefficient in lower part
//   SymType == noSymmetry  means that the upper part is stored after diagonal part and lower part
std::vector<std::pair<number_t, number_t> > SymCsStorage::getRow(SymType s, number_t r, number_t c1, number_t c2) const
{
  number_t nbc=c2;
  if(nbc==0) nbc=nbCols_;
  std::vector<std::pair<number_t, number_t> > cols(nbc-c1+1);
  std::vector<std::pair<number_t, number_t> >::iterator itcol=cols.begin();
  number_t l=0;
  if(c1 < r) //lower part
    {
      for(number_t k = rowPointer_[r-1]; k < rowPointer_[r]; k++) // find in list of col index of row i
        {
          number_t c=colIndex_[k]+1;
          if(c >= c1 && c < r) {*itcol = std::make_pair(c,nbRows_+k+1); l++; itcol++;}   // add 1 because values begin at address 1
        }
    }

  if(nbc >=r && r>=c1) {*itcol++=std::make_pair(r,r); l++;}    //diag coeff

  for(number_t c=std::max(r+1,c1); c<=nbc; c++) //upper part
    {
      number_t a=pos(r,c,s);
      if(a!=0) {*itcol=std::make_pair(c,a); l++; itcol++; }
    }
  cols.resize(l);
  return cols;
}

// get (row indices, adress) of col c in set [r1,r2]
//   SymType != noSymmetry means that the upper part is not stored
//              so an upper part coefficient address is the address of its symmetric coefficient in lower part
//   SymType == noSymmetry  means that the upper part is stored after diagonal part and lower part
std::vector<std::pair<number_t, number_t> > SymCsStorage::getCol(SymType s, number_t c, number_t r1, number_t r2) const
{
  number_t nbr=r2;
  if(nbr==0) nbr=nbRows_;
  std::vector<std::pair<number_t, number_t> > rows(nbr-r1+1);
  std::vector<std::pair<number_t, number_t> >::iterator itrow=rows.begin();
  number_t l=0;
  number_t shift = nbRows_+1;  // add 1 because values begin at address 1
  if(s==_noSymmetry) shift+=lowerPartSize();
  if(r1 < c) //upper part
    {
      for(number_t k = rowPointer_[c-1]; k < rowPointer_[c]; k++) // find in list of col index of row i
        {
          number_t r=colIndex_[k]+1;
          if(r >= r1 && r < c) {*itrow = std::make_pair(r,k+shift); l++; itrow++;}
        }
    }

  if(nbr >=c && c>=r1) {*itrow++=std::make_pair(c,c); l++;}     //diag coeff

  for(number_t r=std::max(c+1,r1); r<=nbr; r++) //lower part
    {
      number_t a=pos(r,c,s);
      if(a!=0) {*itrow=std::make_pair(r,a); l++; itrow++;}
    }
  rows.resize(l);
  return rows;
}

//! get col indices of row r>=1 in set [c1,c2], use colIndex pointer for lower part (efficient for lower part)
// if c2>r, return all indices in the lower and upper part
std::set<number_t> SymCsStorage::getCols(number_t r, number_t c1, number_t c2) const
{
  std::set<number_t> colset;
  number_t nbc=c2;
  if(nbc==0) nbc=nbCols_;
  if(nbc<c1) return colset; //void set

  //deal with lower part
  number_t d=std::min(r-1,nbc);
  number_t rb=rowPointer_[r-1], re=rowPointer_[r];
  for(number_t k = rb; k < re; k++) // find in list of col index of row i
    {
      number_t c=colIndex_[k]+1;
      if( c >= c1 && c<=d) colset.insert(c);
    }
  if(nbc>=r) colset.insert(r);  //add diag index
  if(nbc>r)  //deal with upper part
    {
      std::set<number_t> colup=CsStorage::getCols(r,r+1,nbc); //call generic algorithm
      if(colup.size()>0) colset.insert(colup.begin(),colup.end());  //add upper index
    }
  return colset;
}

//! get row indices of col c in set [r1,r2], use getCols
// if r2>c, return all indices in the lower and upper part
std::set<number_t> SymCsStorage::getRows(number_t c, number_t r1, number_t r2) const
{
  return getCols(c,r1,r2);
}

/*--------------------------------------------------------------------------------
 return skyline row/col pointer from current symmetric cs storage
 --------------------------------------------------------------------------------*/
std::vector<number_t> SymCsStorage::skylineRowPointer() const
{
  return skylinePointer(rowPointer_,colIndex_);
}
std::vector<number_t> SymCsStorage::skylineColPointer() const   //same as skylineRowPointer
{
  return skylinePointer(rowPointer_,colIndex_);
}


/*--------------------------------------------------------------------------------
 fill values of current storage as a skyline storage
 does not required explicit skyline storage !
 WARNING: skyval has to be previously correctly sized and initialized at 0 !!!
 --------------------------------------------------------------------------------*/
void SymCsStorage::fillSkylineValues(const std::vector<real_t>& csval, std::vector<real_t>& skyval,SymType sym,MatrixStorage* skysto) const
{
  std::vector<real_t>::const_iterator itm = csval.begin() + 1;
  std::vector<real_t>::iterator itms = skyval.begin() + 1;
  fillSkylineDiagonalPart(itm,itms);
  fillSkylineTriangularPart(rowPointer_,colIndex_, itm, itms);  //copy lower part
  if(sym==_noSymmetry) fillSkylineTriangularPart(rowPointer_,colIndex_,itm,itms);  //copy upper part if there is no symmetry
}

void SymCsStorage::fillSkylineValues(const std::vector<complex_t>& csval, std::vector<complex_t>& skyval,SymType sym,MatrixStorage* skysto) const
{
  std::vector<complex_t>::const_iterator itm = csval.begin() + 1;
  std::vector<complex_t>::iterator itms = skyval.begin() + 1;
  fillSkylineDiagonalPart(itm,itms);
  fillSkylineTriangularPart(rowPointer_,colIndex_, itm, itms);  //copy lower part
  if(sym==_noSymmetry) fillSkylineTriangularPart(rowPointer_,colIndex_,itm,itms);  //copy upper part if there is no symmetry
}

void SymCsStorage::fillSkylineValues(const std::vector<Matrix<real_t> >& csval, std::vector<Matrix<real_t> >& skyval,SymType sym,MatrixStorage* skysto) const
{
  std::vector<Matrix<real_t> >::const_iterator itm = csval.begin() + 1;
  std::vector<Matrix<real_t> >::iterator itms = skyval.begin() + 1;
  fillSkylineDiagonalPart(itm,itms);
  fillSkylineTriangularPart(rowPointer_,colIndex_, itm, itms);  //copy lower part
  if(sym==_noSymmetry) fillSkylineTriangularPart(rowPointer_,colIndex_,itm,itms);  //copy upper part if there is no symmetry
}

void SymCsStorage::fillSkylineValues(const std::vector<Matrix<complex_t> >& csval, std::vector<Matrix<complex_t> >& skyval,SymType sym,MatrixStorage* skysto) const
{
  std::vector<Matrix<complex_t> >::const_iterator itm = csval.begin() + 1;
  std::vector<Matrix<complex_t> >::iterator itms = skyval.begin() + 1;
  fillSkylineDiagonalPart(itm,itms);
  fillSkylineTriangularPart(rowPointer_,colIndex_, itm, itms);  //copy lower part
  if(sym==_noSymmetry) fillSkylineTriangularPart(rowPointer_,colIndex_,itm,itms);  //copy upper part if there is no symmetry
}

// print storage pointers
void SymCsStorage::print(std::ostream& os) const
{
  printHeader(os);
  if (theVerboseLevel < 2) { return; }
  os<<"row pointer = "<<rowPointer_<<eol;
  os<<"column index = "<<colIndex_<<eol;
}

/*--------------------------------------------------------------------------------
 print SymCs Storage matrix to output stream
--------------------------------------------------------------------------------*/
//print real scalar matrix
void SymCsStorage::printEntries(std::ostream& os, const std::vector<real_t>& m,
                                number_t vb, const SymType sym) const
{
  std::vector<real_t>::const_iterator itm = m.begin() + 1, itml = itm + nbRows_, itmu;
  printEntriesTriangularPart(_scalar, itm, itml, colIndex_, rowPointer_,
                             entriesPerRow, entryWidth, entryPrec, "row", vb, os);
  if(sym != _noSymmetry) return;
  //print upper part if a non "symmetric" matrix
  itm = m.begin() + 1;
  itmu = itm + nbRows_ + colIndex_.size();
  printEntriesTriangularPart(_scalar, itm, itmu, colIndex_, rowPointer_,
                             entriesPerRow, entryWidth, entryPrec, "col", vb, os);
}

//print complex scalar matrix
void SymCsStorage::printEntries(std::ostream& os, const std::vector<complex_t>& m,
                                number_t vb, SymType sym) const
{
  std::vector<complex_t>::const_iterator itm = m.begin() + 1, itml = itm + nbRows_,itmu;
  printEntriesTriangularPart(_scalar, itm, itml, colIndex_, rowPointer_,
                             entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "row", vb, os);
  if(sym != _noSymmetry) return;
  //print upper part if a non "symmetric" matrix
  itm = m.begin() + 1;
  itmu = itm + nbRows_ + colIndex_.size();
  printEntriesTriangularPart(_scalar, itm, itmu, colIndex_, rowPointer_,
                             entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "col", vb, os);
}

//large matrix of vectors are not available for the moment, use Matrix n x 1 or 1 x n
void SymCsStorage::printEntries(std::ostream& os, const std::vector< Vector<real_t> >& m,
                                number_t vb, SymType sym) const
{error("storage_novector", "SymCsStorage::printEntries");}

void SymCsStorage::printEntries(std::ostream& os, const std::vector< Vector<complex_t> >& m,
                                number_t vb, SymType sym) const
{error("storage_novector", "SymCsStorage::printEntries");}

//print matrix of real matrices
void SymCsStorage::printEntries(std::ostream& os, const std::vector< Matrix<real_t> >& m,
                                number_t vb, SymType sym) const
{
  std::vector< Matrix<real_t> >::const_iterator itm = m.begin() + 1, itml = itm + nbRows_, itmu;
  printEntriesTriangularPart(_matrix, itm, itml, colIndex_, rowPointer_,
                             entriesPerRow, entryWidth, entryPrec, "row", vb, os);
  if(sym != _noSymmetry) return;
  //print upper part if a non "symmetric" matrix
  itm = m.begin() + 1;
  itmu = itm + nbRows_ + colIndex_.size();
  printEntriesTriangularPart(_matrix, itm, itmu, colIndex_, rowPointer_,
                             entriesPerRow, entryWidth, entryPrec, "col", vb, os);
}

//print matrix of complex matrices
void SymCsStorage::printEntries(std::ostream& os, const std::vector< Matrix<complex_t> >& m,
                                number_t vb, SymType sym) const
{
  std::vector< Matrix<complex_t> >::const_iterator itm = m.begin() + 1, itml = itm + nbRows_,
                                                   itmu = itm + nbRows_ + colIndex_.size();
  printEntriesTriangularPart(_matrix, itm, itml, colIndex_, rowPointer_,
                             entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "row", vb, os);
  if(sym != _noSymmetry) return;
  itm = m.begin() + 1;
  itmu = itm + nbRows_ + colIndex_.size();
  //print upper part if a non "symmetric" matrix
  printEntriesTriangularPart(_matrix, itm, itmu, colIndex_, rowPointer_,
                             entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "col", vb, os);
}

/*
--------------------------------------------------------------------------------
 print matrix to ostream in coordinate form: i j M_ij
 according to type of values of matrix (overload the generic method in MatrixStorage)
 sym is not used
--------------------------------------------------------------------------------
*/
void SymCsStorage::printCooMatrix(std::ostream& os, const std::vector<real_t>& m, SymType sym) const
{
  std::vector<real_t>::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml;
  if(sym == _noSymmetry) { itmu += lowerPartSize(); }
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, colIndex_, rowPointer_, true);
  printCooTriangularPart(os, itmu, colIndex_, rowPointer_, false, sym);
}

void SymCsStorage::printCooMatrix(std::ostream& os, const std::vector<complex_t>& m, SymType sym) const
{
  std::vector<complex_t>::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml;
  if(sym == _noSymmetry) { itmu += lowerPartSize(); }
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, colIndex_, rowPointer_, true);
  printCooTriangularPart(os, itmu, colIndex_, rowPointer_, false, sym);
}

void SymCsStorage::printCooMatrix(std::ostream& os, const std::vector< Matrix<real_t> >& m, SymType sym) const
{
  std::vector< Matrix<real_t> >::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml;
  if(sym == _noSymmetry) { itmu += lowerPartSize(); }
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, colIndex_, rowPointer_, true);
  printCooTriangularPart(os, itmu, colIndex_, rowPointer_, false, sym);
}

void SymCsStorage::printCooMatrix(std::ostream& os, const std::vector< Matrix<complex_t> >& m, SymType sym) const
{
  std::vector< Matrix<complex_t> >::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml;
  if(sym == _noSymmetry) { itmu += lowerPartSize(); }
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, colIndex_, rowPointer_, true);
  printCooTriangularPart(os, itmu, colIndex_, rowPointer_, false, sym);
}

/*--------------------------------------------------------------------------------
 template Matrix x Vector & Vector x Matrix multiplications and specializations
 result vector has to be sized before
--------------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void SymCsStorage::multMatrixVector(const std::vector<M>& m, V* vp, R* rp, SymType sym) const
{
  trace_p->push("SymCsStorage::multMatrixVector (pointer form)");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  R* rpe= rp + diagonalSize();
  diagonalMatrixVector(itm, vp, rp, rpe);   // product by diagonal
  itl = m.begin() + 1 + nbRows_;
  lowerMatrixVector(colIndex_, rowPointer_, itl, vp, rp, _noSymmetry);  // lower part
  itl = m.begin() + 1 + nbRows_;
  if(sym == _noSymmetry) { itl += colIndex_.size(); }           //case of a non symmetric matrix with symmetric storage
  upperMatrixVector(colIndex_, rowPointer_, itl, vp, rp, sym);  //upper part
  trace_p->pop();
}

template<typename M, typename V, typename R>
void SymCsStorage::multMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, SymType sym) const
{
  trace_p->push("SymCsStorage::multMatrixVector");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = r.end();
  diagonalMatrixVector(itm, itvb, itrb, itre);   //product by diagonal
  itl = m.begin() + 1 + nbRows_;
  lowerMatrixVector(colIndex_, rowPointer_, itl, itvb, itrb, _noSymmetry);  //lower part
  itl = m.begin() + 1 + nbRows_;
  if(sym == _noSymmetry) { itl += colIndex_.size(); }               //case of a non symmetric matrix with symmetric storage
  upperMatrixVector(colIndex_, rowPointer_, itl, itvb, itrb, sym);  //upper part
  trace_p->pop();
}

template<typename M, typename V, typename R>
void SymCsStorage::multVectorMatrix(const std::vector<M>& m, V* vp, R* rp, SymType sym) const
{
  trace_p->push("SymCsStorage::multVectorMatrix (pointer form)");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  R* rpe= rp + diagonalSize();
  diagonalVectorMatrix(itm, vp, rp, rpe);   // product by diagonal
  itl = m.begin() + 1 + nbRows_;
  lowerVectorMatrix(colIndex_, rowPointer_, itl, vp, rp, _noSymmetry);  // lower part
  itl = m.begin() + 1 + nbRows_;
  if(sym == _noSymmetry) { itl += colIndex_.size(); }           //case of a non symmetric matrix with symmetric storage
  upperVectorMatrix(colIndex_, rowPointer_, itl, vp, rp, sym);  //upper part
  trace_p->pop();
}

template<typename M, typename V, typename R>
void SymCsStorage::multVectorMatrix(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, SymType sym) const
{
  trace_p->push("SymCsStorage::multVectorMatrix");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = r.end();
  diagonalVectorMatrix(itm, itvb, itrb, itre);   //product by diagonal
  itl = m.begin() + 1 + nbRows_;
  SymType symt=_noSymmetry;
  //when M is a Matrix, set symt=symmetric to force transposition because lowerVectorMatrix calls upperMatrixVector in omp
  #ifdef XLIFEPP_WITH_OMP
    if (dimsOf(*itm)!=dimPair(1,1)) symt=_symmetric;
  #endif // XLIFEPP_WITH_OMP
  lowerVectorMatrix(colIndex_, rowPointer_, itl, itvb, itrb, symt);  //lower part
  itl = m.begin() + 1 + nbRows_;
  if(sym == _noSymmetry) { itl += colIndex_.size(); }               //case of a non symmetric matrix with symmetric storage
  upperVectorMatrix(colIndex_, rowPointer_, itl, itvb, itrb, sym);  //upper part
  trace_p->pop();
}

/*!
    Add two matrices
    \param m1 vector values_ of first matrix
    \param m2 vector values_ of second matrix
    \param r vector values_ of result matrix
*/
template<typename M1, typename M2, typename R>
void SymCsStorage::addMatrixMatrix(const std::vector<M1>& m1, const std::vector<M2>& m2, std::vector<R>& r) const
{
  trace_p->push("SymCsStorage::addMatrixMatrix");
  typename std::vector<M1>::const_iterator itm1 = m1.begin() + 1;
  typename std::vector<M2>::const_iterator itm2 = m2.begin() + 1;
  typename std::vector<R>::iterator itrb = r.begin() + 1, itre = r.end();
  sumMatrixMatrix(itm1, itm2, itrb, itre);
  trace_p->pop();
}

/*!
   Extract and convert matrix storage to UMFPack format (Matlab sparse matrix)
   \param m1 vector values_ current matrix
   \param colPtUmf vector column Pointer of UMFPack format
   \param rowIdxUmf vector row Index of UMFPack format
   \param resultUmf vector values of UMFPack format
   \param sym type of symmetry
 */
template<typename M1, typename Idx>
void SymCsStorage::toUmfPack(const std::vector<M1>& m1, std::vector<Idx>& colPtUmf, std::vector<Idx>& rowIdxUmf, std::vector<M1>& resultUmf, const SymType sym) const
{
  trace_p->push("SymCsStorage::toUmfPack");
  // Reserve memory for resultUmf and set its size to 0
  resultUmf.reserve(m1.size());
  resultUmf.clear();

  // Reserve memory for rowIdxUmf and set its size to 0
  rowIdxUmf.reserve(m1.size());
  rowIdxUmf.clear();

  // Resize column pointer as dualCs's column pointer size
  colPtUmf.clear();
  colPtUmf.resize(rowPointer_.size(), Idx(0));

  // Because colPointer always begins with 0, so need to to count from the second position
  typename std::vector<Idx>::iterator itColPtUmf = colPtUmf.begin()+1;

  std::vector<number_t>::const_iterator itfColPt = rowPointer_.begin();
  std::vector<number_t>::const_iterator itsColPt = rowPointer_.begin()+1;

  std::vector<number_t>::const_iterator itRowIdx = colIndex_.begin();

  std::vector<number_t>::const_iterator itbColIdx = colIndex_.begin(), itColIdx = itbColIdx;
  std::vector<number_t>::const_iterator iteColIdx = colIndex_.end();

  std::vector<number_t>::const_iterator itRowPt = rowPointer_.begin();

  typename std::vector<M1>::const_iterator itmDiag = m1.begin()+1;
  typename std::vector<M1>::const_iterator itmLow  = itmDiag + diagonalSize();
  typename std::vector<M1>::const_iterator itmUp   = itmLow + lowerPartSize();
  if (_noSymmetry != sym) itmUp = itmLow;
  typename std::vector<M1>::const_iterator itval;

  Idx nzCol = Idx(0); // Number of non-zero in each "working" column
  number_t diagIdx = 0; // Index of diagonal elements
  number_t colIdx = 0; // Index of current column
  for(; itsColPt != rowPointer_.end(); ++itsColPt,++itfColPt,++itColPtUmf)
    {
      nzCol = *itsColPt - *itfColPt; // Number of non-zero value in current column of upper part
      *itColPtUmf += nzCol; // Update column pointer

      while(nzCol>0)
        {
          // Update values
          switch (sym)
            {
            case _skewSymmetric:
              resultUmf.push_back(-(*itmUp));
              break;
            case _selfAdjoint:
              resultUmf.push_back(conj(*itmUp));
              break;
            case _skewAdjoint:
              resultUmf.push_back(-conj(*itmUp));
              break;
            default:
              resultUmf.push_back(*itmUp);
              break;
            }
          ++itmUp;
          --nzCol;

          // Update rowIdxUmf
          rowIdxUmf.push_back(*itRowIdx);
          ++itRowIdx;
        }


      // Process diagonal part
      if(diagIdx < diagonalSize())
        {
          if(M1(0) != *itmDiag)
            {
              // Update values
              resultUmf.push_back(*itmDiag);

              // Update colPtUmf with number of non-zero value
              ++nzCol;

              // Update rowIdxUmf
              rowIdxUmf.push_back(diagIdx);
            }
        }
      ++itmDiag;
      ++diagIdx;

      // Process lower part
      itColIdx = itbColIdx;
      while(itColIdx != iteColIdx)
        {
          itColIdx = std::find(itColIdx,iteColIdx, colIdx);
          if(itColIdx != iteColIdx)
            {
              itRowPt = std::find_if(rowPointer_.begin(), rowPointer_.end(),std::bind(std::greater_equal<number_t>(), std::placeholders::_1, itColIdx - itbColIdx+1));

              // Update values
              itval = (itColIdx-itbColIdx) + itmLow;
              resultUmf.push_back(*itval);

              // Update colPtUmf with number of non-zero value
              ++nzCol;

              // Update rowIdxUmf
              rowIdxUmf.push_back(itRowPt-rowPointer_.begin()-1);

              // Increase iterator to make a search in new range
              ++itColIdx;
            }
        }

      // Update colPtUmf with number of non-zero value
      *itColPtUmf += *(itColPtUmf-1) + nzCol;

      // Move to next column
      ++colIdx;
    }

  trace_p->pop();
}

/*!
   Incomplete factorization of matrix M stored as matrix F = L D Lt where
   - L is a lower triangular matrix with unit diagonal and is stored as
   the strict lower triangular part of F
  */
template<typename M>
void SymCsStorage::ildlt(std::vector<M>& m, std::vector<M>& f) const
{
  trace_p->push("SymCsStorage::ildlt");
  typename std::vector<M>::iterator d_j=f.begin()+1, l_jb=d_j+nbRows_;
  number_t p,q;
  for (number_t j=0; j < nbRows_; ++j)
  {
    // row j: *(d_j+j) = djj -= sum_k=1:j-1 Ljk*Ljk*Dj;
    for ( number_t k=rowPointer_[j]; k<rowPointer_[j+1]; ++k )
    {
      if (colIndex_[k] < j) { *(d_j+j) -= *(l_jb+k) * *(l_jb+k) * *(d_j+colIndex_[k] ) ; }
      else { break;}
    }
    // Lij -= sum_k<j (Lik*Lkj*Dk)/Dj
    for (number_t i=j+1;  i<nbRows_; ++i)
    {
      for ( number_t k=rowPointer_[i]; k < rowPointer_[i+1]; ++k)
      {
        if(colIndex_[k] == j)
        {//Lij trouv�
          p=rowPointer_[i];
          q=rowPointer_[j];
          while ( ((colIndex_[p]<j) && (colIndex_[q]<i)) && ((p<rowPointer_[i+1]) && (q<rowPointer_[j+1]) ) )
          {
            if ( colIndex_[p] == colIndex_[q] )
            {
              *(l_jb+k) -= (*(l_jb+p) * *(l_jb+q)  *   *(d_j+colIndex_[p])  );
              q++;
              p++;
            }
           else if ( colIndex_[q] < colIndex_[p] )  {q++;}
           else if ( colIndex_[q] > colIndex_[p] )  {p++;}
          }
          if(std::abs(*(d_j+j)) < theZeroThreshold) { error("small_pivot"); }
          *(l_jb+k) /= *(d_j+j);
        }
        else if (colIndex_[k] > j) break;
      }
    }
  }// end loop j
  trace_p->pop();
}

// template<typename M>
// void SymCsStorage::ildlt(std::vector<M>& m, std::vector<M>& f) const
// {
//  /*
//   Incomplete factorization of matrix M stored as matrix F = L D Lt where
//   - L is a lower triangular matrix with unit diagonal and is stored as
//   the strict lower triangular part of F
//  */
//  trace_p->push("SymCsStorage::ildlt");
//  typename std::vector<M>::iterator d_j=f.begin()+1, l_jb=d_j+nbRows_, l_jk, l_jp, l_jq, d_jj;
//  std::vector<number_t>::const_iterator itc, itp, itq, itcb=colIndex_.begin();
//  number_t rj=rowPointer_[0], rj1, p, q;

//  for (number_t j=0; j < nbRows_; ++j)
//    {
//      // row j: *(d_j+j) = djj -= sum_k=1:j-1 Ljk*Ljk*Dj;
//      rj1=rowPointer_[j+1];
//      itc = itcb+ rj;
//      d_jj = d_j+j;
//      l_jk = l_jb+rj;
//      for (number_t k=rj; k<rj1 && *itc<j; ++k, ++itc, ++l_jk )
//        *d_jj -= *l_jk* *l_jk * *(d_j+ *itc);

//      // Lij -= sum_k<j (Lik*Lkj*Dk)/Dj
//      number_t ri=rj1, ri1;
//      for (number_t i=j+1;  i<nbRows_; ++i)
//        {
//          ri1=rowPointer_[i+1];
//          itc = itcb+ ri;
//          number_t k=ri;
//          while(k<ri1 && *itc<j) {++k; ++itc;}
//          if(*itc==j)
//            {
//              l_jk = l_jb+k;
//              p=ri; q=rj;
//              itp = itcb+rj; itq = itcb+ri;
//              while (*itp<j && *itq<i && p<ri1 && q<rj1)
//                {
//                  if (*itp == *itq)
//                    {
//                      *l_jk -= *(l_jb+p) * *(l_jb+q) *  *(d_j+ *itp);
//                      ++q, ++p; ++itp; ++itq;
//                    }
//                  else if (*itq<*itp) {++p; ++itp;}
//                  else {++q; ++itq;}
//                }
//              if(std::abs(*d_jj) < theZeroThreshold) { error("small_pivot"); }
//              *l_jk /= *d_jj;
//            }
//            ri=ri1;
//        }
//        rj=rj1;
//    }// end loop j
//  trace_p->pop();
// }

template<typename M>
void SymCsStorage::ilu(std::vector<M>& m, std::vector<M>& f) const
{
  trace_p->push("SymCsStorage::ilu");

  typename std::vector<M>::iterator d_i=f.begin()+1, l_ib=d_i+nbRows_, u_ib=l_ib+colIndex_.size();
  typename std::vector<M>::iterator ind_ik, ind_kj, ind_kl, ind_ki, ind_lk;
  for (number_t k=0; k < nbRows_; ++k)
    {
      M pivot = *(d_i+k);  // pivot=Akk
      if(std::abs(pivot) < theZeroThreshold) {  error("small_pivot"); }
      ind_ik = l_ib;
      ind_ki = u_ib;
      for (number_t i=k+1; i<nbRows_; i++)
        {
          // A ik/= pivot
          M Aik=0;
          M Aki = 0;
          for ( number_t p=rowPointer_[i]; p<rowPointer_[i+1]; p++ )
            {
              if (colIndex_[p] == k)
                {
                  ind_ik = l_ib+p;
                  *ind_ik /= pivot;
                  Aik=*ind_ik;
                  ind_ki = u_ib+p ;
                  Aki = *ind_ki;
                  // k<j<i -> L
                  // Aij -= Aik*Akj
                  for ( number_t j=k+1; j<i ; j++)   // < -> <=
                    {
                      // Aij
                      for ( number_t p=rowPointer_[i]; p<rowPointer_[i+1]; p++ )
                        if (colIndex_[p] == j)
                          {
                            //Akj
                            for ( number_t q=rowPointer_[j]; q<rowPointer_[j+1]; q++ )
                              if (colIndex_[q] == k)   //Akj exists
                                {
                                  ind_kj = u_ib+q;
                                  *(l_ib+p) -= *ind_ik * (*ind_kj);
                                  break;
                                }
                            break;
                          }
                    }
//                      k<l<i -> U
//                      Ali -= Alk*Aki
                  for (number_t l=k+1; l<i    ; l++)
                    {
                      // Alk
                      for ( number_t p=rowPointer_[l]; p<rowPointer_[l+1]; ++p )
                        if (colIndex_[p] == k)
                          {
                            ind_lk = l_ib+p ;
                            // Ali
                            for ( number_t p=rowPointer_[i]; p<rowPointer_[i+1]; ++p )
                              if (colIndex_[p] == l)
                                {
                                  *(u_ib+p) -= *ind_lk * (*ind_ki);
                                  break;
                                }
                            break;
                          }
                    }
                  break;
                }
            }
          //Aii -= Aik*Aki
          *(d_i+i) -= Aik*Aki;   //*ind_ik * (*ind_ki)
        } // end loop i
    }// end loop k



  trace_p->pop();
}

template<typename M>
void SymCsStorage::illt(std::vector<M>& m, std::vector<M>& f) const
{
  /*
   Incomplete factorization of matrix M sdp stored as matrix F = L Lt where
   - L is a lower triangular matrix with unit diagonal and is stored as
   the strict lower triangular part of F
  */
  trace_p->push("SymCsStorage::illt");
  typename std::vector<M>::iterator d_j=f.begin()+1, l_jb=d_j+nbRows_ ;
  number_t p,q;
  for (number_t j=0; j < nbRows_; ++j)
    {
      // row j: *(d_j+j) = djj -= sum_k=1:j-1 Ljk*Ljk;
      for ( number_t k=rowPointer_[j]; k<rowPointer_[j+1]; ++k )
        {
          if (colIndex_[k] < j)
            { *(d_j+j) -= *(l_jb+k) * *(l_jb+k)  ; }
          else { break;}
        }
      // Dj=sqrt(Dj)
      if ( smallPivot(*(d_j+j)) ) {  error("small_pivot") ;  }
      *(d_j+j) =  std::sqrt( (*(d_j+j)) );
//    Lij -= sum_k<j (Lik*Lkj)/Dj
      for (number_t i=j+1;  i<nbRows_; ++i)
        {
          for ( number_t k=rowPointer_[i]; k < rowPointer_[i+1]; ++k)
            {
              if(colIndex_[k] == j)
                {
                  //Lij trouv�
                  p=rowPointer_[i];
                  q=rowPointer_[j];
                  while ( ((colIndex_[p]<j) && (colIndex_[q]<i)) && ((p<rowPointer_[i+1]) && (q<rowPointer_[j+1]) ) )
                    {
                      if ( colIndex_[p] == colIndex_[q] )
                        {
                          *(l_jb+k) -= *(l_jb+p) * *(l_jb+q);
                          q++;
                          p++;
                        }
                      else if ( colIndex_[q] < colIndex_[p] )  {q++;}
                      else if ( colIndex_[q] > colIndex_[p] )  {p++;}
                    }
                  if(std::abs(*(d_j+j)) < theZeroThreshold) { error("small_pivot"); }
                  *(l_jb+k) /= *(d_j+j);
                }
              else if (colIndex_[k] > j) break;
            }
        }
    }// end loop j
  trace_p->pop();
}

template<typename M>
void SymCsStorage::ildlstar(std::vector<M>& m, std::vector<M>& f) const
{
  /*
   Incomplete factorization of matrix M stored as matrix F = L D L* where
   - L is a lower triangular matrix with unit diagonal and is stored as
   the strict lower triangular part of F
  */
  trace_p->push("SymCsStorage::ildlstar");
  typename std::vector<M>::iterator d_j=f.begin()+1, l_jb=d_j+nbRows_;
  number_t p,q;
  for (number_t j=0; j < nbRows_; ++j)
    {
      // row j: *(d_j+j) = djj -= sum_k=1:j-1 Ljk*Ljk*Dj;
      for ( number_t k=rowPointer_[j]; k<rowPointer_[j+1]; ++k )
        {
          if (colIndex_[k] < j)
            { *(d_j+j) -= *(l_jb+k) * conj(*(l_jb+k)) * *(d_j+colIndex_[k] ) ;  }
          else { break;}
        }
//    Lij -= sum_k<j (Lik*Lkj*Dk)/Dj
      for (number_t i=j+1;  i<nbRows_; ++i)
        {
          for ( number_t k=rowPointer_[i]; k < rowPointer_[i+1]; ++k)
            {
              if(colIndex_[k] == j)
                {
                  //Lij trouv�
                  p=rowPointer_[i];
                  q=rowPointer_[j];
                  while ( ((colIndex_[p]<j) && (colIndex_[q]<i)) && ((p<rowPointer_[i+1]) && (q<rowPointer_[j+1]) ) )
                    {
                      if ( colIndex_[p] == colIndex_[q] )
                        {
                          *(l_jb+k) -= (*(l_jb+p) * conj(*(l_jb+q))  *   *(d_j+colIndex_[p])  );
                          q++;
                          p++;
                        }
                      else if ( colIndex_[q] < colIndex_[p] )  {q++;}
                      else if ( colIndex_[q] > colIndex_[p] )  {p++;}
                    }
                  if(std::abs(*(d_j+j)) < theZeroThreshold) { error("small_pivot"); }
                  *(l_jb+k) /= *(d_j+j);
                }
              else if (colIndex_[k] > j) break;
            }
        }


    }// end loop j
  trace_p->pop();
}


template<typename M>
void SymCsStorage::illstar(std::vector<M>& m, std::vector<M>& f) const
{
  /*
   Incomplete factorization of matrix M sdp stored as matrix F = L L* where
   - L is a lower triangular matrix with unit diagonal and is stored as
   the strict lower triangular part of F
  */
  trace_p->push("SymCsStorage::illstar");
  typename std::vector<M>::iterator d_j=f.begin()+1, l_jb=d_j+nbRows_ ;
  number_t p,q;
  for (number_t j=0; j < nbRows_; ++j)
    {
      // row j: *(d_j+j) = djj -= sum_k=1:j-1 Ljk*Ljk;
      for ( number_t k=rowPointer_[j]; k<rowPointer_[j+1]; ++k )
        {
          if (colIndex_[k] < j)
            {
              *(d_j+j) -= *(l_jb+k) * conj(*(l_jb+k) ) ;
            }
          else { break;}
        }
      // Dj=sqrt(Dj)
      if((realPart(*(d_j+j)) <= 0) ) { error("small_pivot") ; }
      *(d_j+j) =  std::sqrt( (*(d_j+j)) );
//    Lij -= sum_k<j (Lik*Lkj)/Dj
      for (number_t i=j+1;  i<nbRows_; ++i)
        {
          for ( number_t k=rowPointer_[i]; k < rowPointer_[i+1]; ++k)
            {
              if(colIndex_[k] == j)
                {
                  //Lij trouv�
                  p=rowPointer_[i];
                  q=rowPointer_[j];
                  while ( ((colIndex_[p]<j) && (colIndex_[q]<i)) && ((p<rowPointer_[i+1]) && (q<rowPointer_[j+1]) ) )
                    {
                      if ( colIndex_[p] == colIndex_[q] )
                        {
                          *(l_jb+k) -= *(l_jb+p) * conj(*(l_jb+q));
                          q++;
                          p++;
                        }
                      else if ( colIndex_[q] < colIndex_[p] )  {q++;}
                      else if ( colIndex_[q] > colIndex_[p] )  {p++;}
                    }
                  if(std::abs(*(d_j+j)) < theZeroThreshold) { error("small_pivot"); }
                  *(l_jb+k) /= *(d_j+j);
                }
              else if (colIndex_[k] > j) break;
            }
        }
    }// end loop j
  trace_p->pop();
}


template<typename M, typename V, typename X>
void SymCsStorage::lowerD1Solver(const std::vector<M>& m,  std::vector<V>& v, std::vector<X>& x) const
{
  /*
     solver for a lower triangular part linear system with unit diagonal
  */
  // no const v here as v might coincide with x
  trace_p->push("SymCsStorage::lowerD1Solver");
  CsStorage::bzLowerD1Solver(m.begin() + 1 + v.size(), v.begin(), x.begin(), x.end(), colIndex_, rowPointer_);
  trace_p->pop();
}

template<typename M, typename V, typename X>
void SymCsStorage::diagonalSolver(const std::vector<M>& m,  std::vector<V>& v, std::vector<X>& x) const
{
  /*
     solver for a lower triangular part linear system with unit diagonal
  */
  // no const v here as v might coincide with x
  trace_p->push("SymCsStorage::diagonalSolver");
  typename std::vector<M>::const_iterator it_m = m.begin() + 1;
  typename std::vector<V>::const_iterator it_vb = v.begin();
  typename std::vector<X>::iterator it_xb = x.begin();
  typename std::vector<X>::iterator it_xe = x.end();
  CsStorage::bzSorDiagonalSolver(it_m, it_vb, it_xb, it_xe, 1.);
  trace_p->pop();
}

/*
--------------------------------------------------------------------------------
     Template upper triangular part  solver (after factorization)
--------------------------------------------------------------------------------
*/
template<typename M, typename V, typename X>
void SymCsStorage::upperD1Solver(const std::vector<M>& m,  std::vector<V>& b, std::vector<X>& x, SymType sym) const
{
  /*
     solver for a upper triangular part linear system: U=D+U
  */
  // no const v here as v might coincide with x
  trace_p->push("SymCsStorage::upperD1Solver");
  typename std::vector<M>::const_reverse_iterator it_ru = m.rbegin(), it_rd = it_ru + upperPartSize() + lowerPartSize();
  typename std::vector<V>::const_reverse_iterator it_rbb = b.rbegin();
  typename std::vector<X>::reverse_iterator it_rxb = x.rbegin(), it_rxe = x.rend();
  bzUpperD1Solver(it_rd, it_ru, it_rbb, it_rxb, it_rxe, colIndex_, rowPointer_,  1.,  sym) ;
  trace_p->pop();
}
/*
--------------------------------------------------------------------------------
     Template upper triangular part  solver (after factorization)
--------------------------------------------------------------------------------
*/
template<typename M, typename V, typename X>
void SymCsStorage::upperSolver(const std::vector<M>& m,  std::vector<V>& b, std::vector<X>& x, SymType sym) const
{
  /*
     solver for a upper triangular part linear system: U=D+U
  */
  // no const v here as v might coincide with x
  trace_p->push("SymCsStorage::upperSolver");
  typename std::vector<M>::const_reverse_iterator it_ru = m.rbegin(), it_rd = it_ru + upperPartSize() + lowerPartSize();
  typename std::vector<V>::const_reverse_iterator it_rbb = b.rbegin();
  typename std::vector<X>::reverse_iterator it_rxb = x.rbegin(), it_rxe = x.rend();
  bzSorUpperSolver(it_rd, it_ru, it_rbb, it_rxb, it_rxe, colIndex_, rowPointer_,  1.,  sym) ;
  trace_p->pop();
}

} // end of namespace xlifepp

