/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ColCsStorage.cpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Implementation of xlifepp::ColCsStorage class and functionnalities
*/

#include "ColCsStorage.hpp"

namespace xlifepp
{
/*--------------------------------------------------------------------------------
  Constructors, Destructor
--------------------------------------------------------------------------------*/
// default constructor
ColCsStorage::ColCsStorage(number_t nr, number_t nc, string_t id)
  : CsStorage(nr, nc, _col, id)
{
  trace_p->push("ColCsStorage constructor");
  std::vector<std::vector<number_t> > rows(nc);
  buildCsStorage(rows, rowIndex_, colPointer_); //construct rowIndex_ and colPointer_
  trace_p->pop();
}

// constructor by a pair of numbering vectors rowNumbers and colNumbers (same size)
//   rowNumbers[k] is the row numbers of submatrix k
//   colNumbers[k] is the col numbers of submatrix k
// indices start from 1 !!!
// this constructor is well adapted to FE matrix where submatrix is linked to an element
ColCsStorage::ColCsStorage(number_t nr, number_t nc, const std::vector<std::vector<number_t> >& rowNumbers, const std::vector<std::vector<number_t> >& colNumbers, string_t id)
  : CsStorage(nr, nc, _col, id)
{
  trace_p->push("ColCsStorage constructor");
  // build list of col index for each rows
  std::vector<std::set<number_t> > rowset(nbCols_);  //temporary vector of row index
  std::vector<std::vector<number_t> >::const_iterator itrs, itcs = colNumbers.begin();
  std::vector<number_t>::const_iterator itr, itc;
  for(itrs = rowNumbers.begin(); itrs != rowNumbers.end(); itrs++, itcs++)
    {
      for(itc = itcs->begin(); itc != itcs->end(); itc++)
        for(itr = itrs->begin(); itr != itrs->end(); itr++)
          { rowset[*itc - 1].insert(*itr); }
    }
  // convert set to vector
  std::vector<std::vector<number_t> > rows(nbCols_);   //temporary vector of col index
  std::vector<std::set<number_t> >::iterator itset;
  std::vector<std::vector<number_t> >::iterator itrows = rows.begin();
  for(itset = rowset.begin(); itset != rowset.end(); itset++, itrows++)
    { *itrows = std::vector<number_t>(itset->begin(), itset->end()); }
  buildCsStorage(rows, rowIndex_, colPointer_); //construct rowIndex_ and colPointer_
  trace_p->pop();
}

// create a new scalar ColCs storage from current ColCs storage and submatrix sizes
ColCsStorage* ColCsStorage::toScalar(dimen_t nbr, dimen_t nbc)
{
  MatrixStorage* nsto=findMatrixStorage(stringId, _cs, _col, buildType_, true, nbr*nbRows_, nbc*nbCols_);
  if(nsto!=nullptr) return static_cast<ColCsStorage*>(nsto);
  ColCsStorage* cs = new ColCsStorage(nbr*nbRows_, nbc*nbCols_, stringId);
  cs->buildType() = buildType_;
  cs->scalarFlag() = true;
  cs->rowIndex_.resize(nbr*nbc*rowIndex_.size());
  toScalarCs(colPointer_, rowIndex_, nbr, nbc, cs->colPointer_, cs->rowIndex_);
  return cs;
}


/*--------------------------------------------------------------------------------
 add submatrix indices given by its row and column indices (>= 1)
--------------------------------------------------------------------------------*/
void ColCsStorage::addSubMatrixIndices(const std::vector<number_t>& rows,const std::vector<number_t>& cols)
{
  addCsSubMatrixIndices(colPointer_,rowIndex_,cols, rows, false,true);
}

/*--------------------------------------------------------------------------------
 add in storage indexes (i,j) given by rows (i) and columns (j) (i,j >= 1)
 rows and cols are of same size
--------------------------------------------------------------------------------*/
void ColCsStorage::addIndices(const std::vector<number_t>& rows, const std::vector<number_t>& cols)
{
  if(rows.size()==0) return;  //nothing to do

  std::vector<number_t>::const_iterator itr = rows.begin(),itc=cols.end();
  std::map<number_t, std::set<number_t> > colsmap;
  std::map<number_t, std::set<number_t> >::iterator itm;
  //collect row indices by col
  for(; itc!=cols.end(); itc++, itr++)
    {
      itm=colsmap.find(*itc);
      if(itm==colsmap.end()) colsmap[*itc]=std::set<number_t>();
      colsmap[*itc].insert(*itr);
    }
  //modify current colpointer and rowindex (expansive operation)
  for(itm=colsmap.begin(); itm!=colsmap.end(); itm++)
    if(itm->second.size()>0) addCol(itm->first,itm->second);  //insert row at col c in storage
}

/*--------------------------------------------------------------------------------
//insert in storage indexes (r,c1), (r,c2), .... c1,c2, ... given by cols (r,c>=1)
  expansive operation !
--------------------------------------------------------------------------------*/
void ColCsStorage::addCol(number_t c, const std::set<number_t>& rows, MatrixPart mpart)
{
  if(rows.size()==0) return;
  number_t cb=colPointer_[c-1], ce=colPointer_[c];
  std::set<number_t> allrows(rowIndex_.begin()+cb, rowIndex_.begin()+ce);
  std::set<number_t>::iterator its=rows.begin();
  for(; its!=rows.end(); its++) allrows.insert(*its - 1); //add current cols (-1 shift)
  number_t nr=allrows.size()- (ce -cb);
  if(nr==0) return;                      //nothing to do , no new rows

  std::vector<number_t>::iterator itr=rowIndex_.begin()+ce;
  rowIndex_.insert(itr, nr, 0);          //insert nr element initialized to 0
  its=allrows.begin();
  itr=rowIndex_.begin()+cb;
  for(; its!=allrows.end(); its++,itr++) *itr=*its; //copy new columns set
  std::vector<number_t>::iterator itc=colPointer_.begin()+c;
  for(; itc!=colPointer_.end(); itc++) *itc+=nr; //shift colPointer
}

/*--------------------------------------------------------------------------------
 access operator to position of (i,j)>0 coefficients (return 0 if outside)
 This DIRECT access to (i,j) pointer SHOULD NOT be used in matrix computation
--------------------------------------------------------------------------------*/
number_t ColCsStorage::pos(number_t i, number_t j, SymType s) const
{
  if(i == 0 || i > nbRows_ || j == 0 || j > nbCols_) { return 0; }
  for(number_t k = colPointer_[j - 1]; k < colPointer_[j]; k++) // find in list of row index of col j
    if(rowIndex_[k] == i - 1) { return k + 1; }             // add 1 because values begin at address 1
  return 0;
}

/*--------------------------------------------------------------------------------
 access to submatrix adresses, useful for assembling matrices
 rows and cols are vector of index (>0) of row and column, no index ordering is assumed !!
 return the vector of adresses (relative number >0 in storage) in a row storage (as Matrix)
 if a row or a col index is out of storage
     an error is raised if errorOn is true (default)
     position is set to 0 else
 if sy!=_noSymmetry only positions of lower part are requested (not used here)
 remark: since the vector storage indexCol_ is ordered for each line, if the indices of columns
          of the submatrix were also, one could write a faster algorithm
--------------------------------------------------------------------------------*/
void ColCsStorage::positions(const std::vector<number_t>& rows, const std::vector<number_t>& cols, std::vector<number_t>& adrs, bool errorOn, SymType sy) const
{
  number_t nba = rows.size() * cols.size();
  if(adrs.size() != nba) { adrs.resize(nba,0); }             // resizment of result vector
  std::vector<number_t>::iterator itp = adrs.begin();
  std::vector<number_t>::const_iterator itr, itc, itb, ite, it;
  number_t c=0, nbc=cols.size();
  for(itc = cols.begin(); itc != cols.end(); itc++, c++)  // loop on column of submatrix
    {
      number_t j = *itc, cb = colPointer_[j - 1], ce = colPointer_[j];
      itb = rowIndex_.begin() + cb;
      ite = rowIndex_.begin() + ce;
      number_t r=0;
      for(itr = rows.begin(); itr != rows.end(); itr++, r++) // loop on row of submatrix
        {
          number_t ir = *itr - 1;
          itp=adrs.begin()+ (r*nbc+c);
          number_t k=0;
          it=itb;
          *itp=0;
          while(it != ite && *itp == 0) // find in list of col index of row i
            {
              if(*it == ir) { *itp = cb + k + 1; }
              it++; k++;
            }
          if(errorOn && *itp == 0) {error("storage_outofstorage","ColCs", *itr, *itc); }
        }
    }
}

// get (col indices, adress) of row r in set [c1,c2]; ## SymType not used ##
std::vector<std::pair<number_t, number_t> > ColCsStorage::getRow(SymType s, number_t r, number_t c1, number_t c2) const
{
  number_t nbc=c2;
  if(nbc==0) nbc=nbCols_;
  std::vector<std::pair<number_t, number_t> > cols(nbc-c1+1);
  std::vector<std::pair<number_t, number_t> >::iterator itcol=cols.begin();
  number_t l=0;
  for(number_t j=c1; j<=nbc; j++)
    {
      number_t a=pos(r,j);
      if(a!=0)
        {
          *itcol=std::make_pair(j,a);
          l++; itcol++;
        }
    }
  cols.resize(l);
  return cols;
}

// get (row indices, adress) of col c in set [r1,r2] ## SymType not used ##
std::vector<std::pair<number_t, number_t> > ColCsStorage::getCol(SymType s,number_t c, number_t r1, number_t r2) const
{
  number_t nbr=r2;
  if(nbr==0) nbr=nbRows_;
  number_t cb=colPointer_[c-1], ce=colPointer_[c];
  number_t l=ce-cb;
  std::vector<std::pair<number_t, number_t> > rows(l);
  std::vector<std::pair<number_t, number_t> >::iterator itrow=rows.begin();
  l=0;
  for(number_t k = cb; k < ce; k++)
    {
      number_t r=rowIndex_[k]+1;
      if(r >= r1 && r <= nbr)
        {
          *itrow = std::make_pair(r,k+1);             // add 1 because values begin at address 1
          l++; itrow++;
        }
    }
  rows.resize(l);
  return rows;
}

//! get row indices of col c in range [r1,r2], specific CsCol algorithm (no getCols, use generic algorithm)
std::set<number_t> ColCsStorage::getRows(number_t c, number_t r1, number_t r2) const
{
  std::set<number_t> rowset;
  number_t nbr=r2;
  if(nbr==0) nbr=nbRows_;
  if(nbr<r1) return rowset;
  number_t cb=colPointer_[c-1], ce=colPointer_[c];
  for(number_t k = cb; k < ce; k++) // find in list of row index of row i
    {
      number_t r=rowIndex_[k]+1;
      if(r >= r1 && r<=nbr) rowset.insert(r);
    }
  return rowset;
}

//! get row indices of col c in range [r1,r2], specific CsCol algorithm (no getCols, use generic algorithm)
// no allocation, no check (faster)
void ColCsStorage::getRowsV(std::vector<number_t>& rowsv, number_t& nbrows, number_t c, number_t r1, number_t r2) const
{
  nbrows=0;
  number_t nbr=r2;
  if(nbr==0) nbr=nbCols_;
  if(nbr<r1) return;
  std::vector<number_t>::iterator itv=rowsv.begin();
  number_t cb=colPointer_[c-1], ce=colPointer_[c];
  for(number_t k = cb; k < ce; k++) // find in list of col index of row i
    {
      number_t r=rowIndex_[k]+1;
      if(r >= r1 && r<=nbr) {*itv=r; itv++; nbrows++;}
    }
}

//return skyline row/col pointer from current ColCsStorage
// skyline row : p(r) = min{0<=c<r, st a_rc!=nullptr}, =r if empty set
// skyline col : p(c) = min{0<=r<c, st a_rc!=nullptr}, =c if empty set
std::vector<number_t> ColCsStorage::skylineRowPointer() const
{
  std::vector<number_t> sky(nbRows_+1);
  number_t r=0;
  sky[0]=0;
  for(;r<nbRows_;r++) sky[r+1]=r;
  auto itr=rowIndex_.begin();
  for(number_t c=0;c<nbCols_;c++)
  {
    for(number_t k=0;k<colPointer_[c+1]-colPointer_[c];k++,itr++)
    {
      r=*itr;
      if(r>c) sky[r+1]=std::min(sky[r+1],c);
    }
  }
  for(r=1;r<=nbRows_;r++) sky[r]=sky[r-1]+r-1-sky[r];
  return sky;
}
std::vector<number_t> ColCsStorage::skylineColPointer() const
{
  std::vector<number_t> sky(nbCols_+1);
  number_t c=0;
  sky[0]=0;
  for(;c<nbCols_;c++) sky[c+1]=c;
  auto itr=rowIndex_.begin();
  for(c=0;c<nbCols_-1;c++)
  {
    for(number_t k=0;k<colPointer_[c+1]-colPointer_[c];k++,itr++)
    {
      if(c>*itr) sky[c+1]=std::min(sky[c+1],*itr);
    }
  }
  for(c=1;c<=nbCols_;c++) sky[c]=sky[c-1]+c-1-sky[c];
  return sky;
}

// check if two storages have the same structures
bool ColCsStorage::sameStorage(const MatrixStorage& sto) const
{
  if(!(sto.storageType()==storageType_ &&
       sto.accessType()==accessType_ &&
       sto.nbOfRows()==nbRows_ &&
       sto.nbOfColumns()==nbCols_&&
       sto.size()==size()))
    return false;
  const ColCsStorage& csto=static_cast<const ColCsStorage&>(sto);
  if(colPointer_!=csto.colPointer_) return false;
  if(rowIndex_!=csto.rowIndex_) return false;
  return true;
}

// print storage pointers
void ColCsStorage::print(std::ostream& os) const
{
  printHeader(os);
  if (theVerboseLevel < 2) { return; }
  os<<"column pointer = "<<colPointer_<<eol;
  os<<"row index = "<<rowIndex_<<eol;
}

/*--------------------------------------------------------------------------------
 print ColCs Storage matrix to output stream
--------------------------------------------------------------------------------*/
// print real scalar matrix
void ColCsStorage::printEntries(std::ostream& os, const std::vector<real_t>& m, number_t vb, const SymType sym) const
{
  std::vector<real_t>::const_iterator itm = m.begin() + 1;
  printEntriesAll(_scalar, itm, rowIndex_, colPointer_, entriesPerRow, entryWidth, entryPrec, "col", vb, os);
}

// print complex scalar matrix
void ColCsStorage::printEntries(std::ostream& os, const std::vector<complex_t>& m, number_t vb, SymType sym) const
{
  std::vector<complex_t>::const_iterator itm = m.begin() + 1;
  printEntriesAll(_scalar, itm, rowIndex_, colPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "col", vb, os);
}

// large matrix of vectors are not available for the moment, use Matrix n x 1 or 1 x n
void ColCsStorage::printEntries(std::ostream& os, const std::vector< Vector<real_t> >& m, number_t vb, SymType sym) const
{error("storage_novector", "ColCsStorage::printEntries");}

void ColCsStorage::printEntries(std::ostream& os, const std::vector< Vector<complex_t> >& m, number_t vb, SymType sym) const
{error("storage_novector", "ColCsStorage::printEntries");}

// print matrix of real matrices
void ColCsStorage::printEntries(std::ostream& os, const std::vector< Matrix<real_t> >& m, number_t vb, SymType sym) const
{
  std::vector< Matrix<real_t> >::const_iterator itm = m.begin() + 1;
  printEntriesAll(_matrix, itm, rowIndex_, colPointer_, entriesPerRow, entryWidth, entryPrec, "col", vb, os);
}

// print matrix of complex matrices
void ColCsStorage::printEntries(std::ostream& os, const std::vector< Matrix<complex_t> >& m, number_t vb, SymType sym) const
{
  std::vector< Matrix<complex_t> >::const_iterator itm = m.begin() + 1;
  printEntriesAll(_matrix, itm, rowIndex_, colPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "col", vb, os);
}

/*
--------------------------------------------------------------------------------
 print matrix to ostream in coordinate form: i j M_ij
 according to type of values of matrix (overload the generic method in MatrixStorage)
 sym is not used
--------------------------------------------------------------------------------
*/
void ColCsStorage::printCooMatrix(std::ostream& os, const std::vector<real_t>& m, SymType sym) const
{
  std::vector<real_t>::const_iterator itm = m.begin() + 1;
  printCooTriangularPart(os, itm, rowIndex_, colPointer_, false);
}

void ColCsStorage::printCooMatrix(std::ostream& os, const std::vector<complex_t>& m, SymType sym) const
{
  std::vector<complex_t>::const_iterator itm = m.begin() + 1;
  printCooTriangularPart(os, itm, rowIndex_, colPointer_, false);
}

void ColCsStorage::printCooMatrix(std::ostream& os, const std::vector< Matrix<real_t> >& m, SymType sym) const
{
  std::vector< Matrix<real_t> >::const_iterator itm = m.begin() + 1;
  printCooTriangularPart(os, itm, rowIndex_, colPointer_, false);
}

void ColCsStorage::printCooMatrix(std::ostream& os, const std::vector< Matrix<complex_t> >& m, SymType sym) const
{
  std::vector< Matrix<complex_t> >::const_iterator itm = m.begin() + 1;
  printCooTriangularPart(os, itm, rowIndex_, colPointer_, false);
}

// /*
// --------------------------------------------------------------------------------
// print matrix to ostream in coordinate form: i j M_ij
// according to type of values of matrix (overload the generic method in MatrixStorage)
// --------------------------------------------------------------------------------
// */
// void ColCsStorage::printCooMatrix(std::ostream& os, const std::vector<real_t>& m, SymType sym) const
// {
//  std::vector<real_t>::const_iterator itm = m.begin()+1;
//  std::vector<number_t>::const_iterator itc=colPointer_.begin();
//  std::vector<number_t>::const_iterator itr=rowIndex_.begin();
//  for(number_t j = 1; j <= nbCols_; j++, itc++)      //loop on cols
//    {
//      number_t nnz=*(itc+1)-*itc;
//      for(number_t i=0; i<nnz; i++, itr++, itm++)    //loop on row indices
//        os << *itr+1 << "  " << j << "  " << *itm << eol:
// }

// void ColCsStorage::printCooMatrix(std::ostream& os, const std::vector<complex_t>& m, SymType sym) const
// {
//  std::vector<complex_t>::const_iterator itm = m.begin()+1;
//  std::vector<number_t>::const_iterator itc=colPointer_.begin();
//  std::vector<number_t>::const_iterator itr=rowIndex_.begin();
//  for(number_t j = 1; j <= nbCols_; j++, itc++)      //loop on cols
//    {
//      number_t nnz=*(itc+1)-*itc;
//      for(number_t i=0; i<nnz; i++, itr++, itm++)    //loop on row indices
//        os << *itr+1 << "  " << j << "  " << itm->real() << "  " << itm->imag() << eol;
//    }
// }

// void ColCsStorage::printCooMatrix(std::ostream& os, const std::vector< Matrix<real_t> >& m, SymType sym) const
// {
//  std::vector< Matrix<real_t> >::const_iterator itm = m.begin()+1;
//  std::vector<number_t>::const_iterator itc=colPointer_.begin();
//  std::vector<number_t>::const_iterator itr=rowIndex_.begin();
//  std::vector<real_t>::const_iterator it;
//  number_t nr = m[0].numberOfRows(), nc = m[0].numberOfColumns();
//  for(number_t j = 1; j <= nbCols_; j++, itc++)      //loop on cols
//    {
//      number_t nnz=*(itc+1)-*itc;
//      for(number_t i=0; i<nnz; i++, itr++, itm++)    //loop on row indices
//        {
//          it=itm->begin();
//          for(number_t k = 1; k <= nr; k++)          //loop on subrows
//            for(number_t l = 1; l <= nc; l++, it++)  //loop on subcols
//              os << *itc * nr + k  << "  " << (j-1)* nc + l << "  " << *it << eol;
//        }
//    }
// }

// void ColCsStorage::printCooMatrix(std::ostream& os, const std::vector< Matrix<complex_t> >& m, SymType sym) const
// {
//  std::vector< Matrix<complex_t> >::const_iterator itm = m.begin()+1;
//  std::vector<number_t>::const_iterator itc=colPointer_.begin();
//  std::vector<number_t>::const_iterator itr=rowIndex_.begin();
//  std::vector<complex_t>::const_iterator it;
//  number_t nr = m[0].numberOfRows(), nc = m[0].numberOfColumns();
//  for(number_t j = 1; j <= nbCols_; j++, itc++)      //loop on cols
//    {
//      number_t nnz=*(itc+1)-*itc;
//      for(number_t i=0; i<nnz; i++, itr++, itm++)    //loop on row indices
//        {
//          it=itm->begin();
//          for(number_t k = 1; k <= nr; k++)          //loop on subrows
//            for(number_t l = 1; l <= nc; l++, it++)  //loop on subcols
//              os << *itc * nr + k  << "  " << (j-1)* nc + l << "  " << it->real() << "  " << it->imag() << eol;
//        }
//    }
// }

/*--------------------------------------------------------------------------------
 template Matrix x Vector & Vector x Matrix multiplications and specializations
 result vector has to be sized before
--------------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void ColCsStorage::multMatrixVector(const std::vector<M>& m, V* vp, R* rp) const
{
  trace_p->push("ColCsStorage::multMatrixVector (pointer form)");
  typename std::vector<M>::const_iterator itm = m.begin() + 1;
  columnMatrixVector(rowIndex_, colPointer_, itm, vp, rp);
  trace_p->pop();
}

template<typename M, typename V, typename R>
void ColCsStorage::multMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  trace_p->push("ColCsStorage::multMatrixVector");
  typename std::vector<M>::const_iterator itm = m.begin() + 1;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin();
  //std::fill(r.begin(), r.end(), R((m[0]*v[0])*0.));
  columnMatrixVector(rowIndex_, colPointer_, itm, itvb, itrb);
  trace_p->pop();
}

template<typename M, typename V, typename R>
void ColCsStorage::multVectorMatrix(const std::vector<M>& m, V* vp, R* rp) const
{
  trace_p->push("ColCsStorage::multVectorMatrix (pointer form)");
  typename std::vector<M>::const_iterator itm = m.begin() + 1;
  columnVectorMatrix(rowIndex_, colPointer_, itm, vp, rp);
  trace_p->pop();
}

template<typename M, typename V, typename R>
void ColCsStorage::multVectorMatrix(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  trace_p->push("ColCsStorage::multVectorMatrix");
  typename std::vector<M>::const_iterator itm = m.begin() + 1;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin();
  //std::fill(r.begin(), r.end(), R((m[0]*v[0])*0.));
  columnVectorMatrix(rowIndex_, colPointer_, itm, itvb, itrb);
  trace_p->pop();
}

/*!
   Add two matrices
   \param m1 vector values_ of first matrix
   \param m2 vector values_ of second matrix
   \param r vector values_ of result matrix
 */
template<typename M1, typename M2, typename R>
void ColCsStorage::addMatrixMatrix(const std::vector<M1>& m1, const std::vector<M2>& m2, std::vector<R>& r) const
{
  trace_p->push("ColCsStorage::addMatrixMatrix");
  typename std::vector<M1>::const_iterator itm1 = m1.begin() + 1;
  typename std::vector<M2>::const_iterator itm2 = m2.begin() + 1;
  typename std::vector<R>::iterator itrb = r.begin() + 1, itre = r.end();
  sumMatrixMatrix(itm1, itm2, itrb, itre);
  trace_p->pop();
}


/*!
   Extract and convert matrix storage to UMFPack format (Matlab sparse matrix)
   \param m1 vector values_ current matrix
   \param colPt vector column Pointer of UMFPack format
   \param rowIdx vector row Index of UMFPack format
   \param result vector values of UMFPack format
 */
template<typename M1, typename Idx>
void ColCsStorage::toUmfPack(const std::vector<M1>& m1, std::vector<Idx>& colPt, std::vector<Idx>& rowIdx, std::vector<M1>& result) const
{
  colPt.resize(colPointer_.size());
  rowIdx.resize(rowIndex_.size());
  std::copy(colPointer_.begin(),colPointer_.end(),colPt.begin());
  std::copy(rowIndex_.begin(),rowIndex_.end(),rowIdx.begin());

  result.resize(m1.size()-1);
  std::copy(m1.begin()+1,m1.end(),result.begin());

}

template<typename M>
void ColCsStorage::ilu(std::vector<M>& m, std::vector<M>& f) const
{
  /*
   incomplete factorization of matrix M stored as matrix F = L U where
   - L is a lower triangular matrix with unit diagonal and is stored as
   the strict lower triangular part of F
   - U is a upper triangular matrix with diagonal stored as diagonal part of F
   and strict upper triangular part stored as the strict upper triangular part of F:
  */
  trace_p->push("ColCsStorage::ilu");
  typename std::vector<M>::iterator f_ib=f.begin()+1;
  std::vector<std::pair<number_t, number_t> > Ri;
  std::vector<M> Diag(nbCols_);
  number_t k;
  Diag[0]=*f_ib;
  number_t c,l;
  for (number_t i=2; i <= nbCols_; i++) // boucle sur les lignes
  {
       Ri=getRow(_noSymmetry,i,1,nbCols_);
       for (number_t j=0; j < Ri.size(); j++)
         {//Rj[p].first-1 ligne du coef non nul  col j
                 c  =Ri[j].first-1; //colonne du jieme non zero de ligne i
                 l = colPointer_[c]; // position du 1er coef non nul de colonne c
                 k = 0;
              while ( (Ri[k].first-1 < c) && (rowIndex_[l] < i-1) )
              {
                  if (rowIndex_[l] == Ri[k].first-1)
                       {
                          *(f_ib+Ri[j].second-1) -=*(f_ib+Ri[k].second-1) * (*(f_ib+l));
                           l++;k++;
                       }
                  else if (rowIndex_[l] < Ri[k].first-1)   l++;
                  else /*if  (rowIndex_[c] > Rj[k].first-1) */    k++;
              }
              if (Ri[j].first-1 == i-1)
                {
                   Diag[i-1]=*(f_ib+Ri[j].second-1);
                   if(std::abs(Diag[i-1]) < theZeroThreshold) {error("small_pivot");}
                }
              else if (Ri[j].first-1 <i-1) *(f_ib+Ri[j].second-1)/=Diag[Ri[j].first-1];
         }
   }
  trace_p->pop();
}

} // end of namespace xlifepp

