/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DualCsStorage.cpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Implementation of xlifepp::DualCsStorage class and functionnalities
*/

#include "DualCsStorage.hpp"
#include "../skylineStorage/SymSkylineStorage.hpp"
#include "../skylineStorage/DualSkylineStorage.hpp"
#include <functional>

namespace xlifepp
{

/*--------------------------------------------------------------------------------
  Constructors, Destructor
--------------------------------------------------------------------------------*/
// default constructor
DualCsStorage::DualCsStorage(number_t nr, number_t nc, string_t id)
  : CsStorage(nr, nc, _dual, id)
{
  trace_p->push("DualCsStorage constructor");
  std::vector<std::vector<number_t> > cols(nr);
  buildStorage(cols);
  trace_p->pop();
}

//explicit constructor
DualCsStorage::DualCsStorage(number_t nr, number_t nc,
                             const std::vector<number_t>& cI, const std::vector<number_t>& rP,
                             const std::vector<number_t>& rI, const std::vector<number_t>& cP,
                             string_t id)
  : CsStorage(nr, nc, _dual, id), colIndex_(cI), rowPointer_(rP), rowIndex_(rI), colPointer_(cP) {}


// constructor by a pair of numbering vectors rowNumbers and colNumbers (same size)
//   rowNumbers[k] is the row numbers of submatrix k (index starts at 1)
//   colNumbers[k] is the col numbers of submatrix k (index starts at 1)
// this constructor is well adapted to FE matrix where submatrix is linked to an element
DualCsStorage::DualCsStorage(number_t nr, number_t nc, const std::vector<std::vector<number_t> >& rowNumbers, const std::vector<std::vector<number_t> >& colNumbers, string_t id)
  : CsStorage(nr, nc, _dual, id)
{
  trace_p->push("DualCsStorage constructor");
  // build list of col index for each rows
  std::vector<std::set<number_t> > colset(nbRows_);  // temporary vector of col index of lower part
  std::vector<std::set<number_t> > rowset(nbCols_);  // temporary vector of row index of upper part
  std::vector<std::vector<number_t> >::const_iterator itrs, itcs = colNumbers.begin();
  std::vector<number_t>::const_iterator itr, itc;
  for(itrs = rowNumbers.begin(); itrs != rowNumbers.end(); itrs++, itcs++)
    {
      for(itr = itrs->begin(); itr != itrs->end(); itr++)
        for(itc = itcs->begin(); itc != itcs->end(); itc++)
          {
            if(*itr > *itc) { colset[*itr - 1].insert(*itc); }
            if(*itr < *itc) { rowset[*itc - 1].insert(*itr); }
          }
    }
//  // convert set to vector
//  std::vector<std::set<number_t> >::iterator itset;
//  std::vector<std::vector<number_t> > cols(nbRows_);   // temporary vector of col index
//  std::vector<std::vector<number_t> >::iterator itrc = cols.begin();
//  for(itset = colset.begin(); itset != colset.end(); itset++, itrc++)
//    { *itrc = std::vector<number_t>(itset->begin(), itset->end()); }

  //now buildCsStorage works also with std::vector<std::set<number_t> >, convert to vector is useless
  buildCsStorage(colset, colIndex_, rowPointer_); // construct colIndex_ and rowPointer_

  std::vector<std::set<number_t> >::iterator itset;
  std::vector<std::vector<number_t> > rows(nbCols_);   // temporary vector of row index
  std::vector<std::vector<number_t> >::iterator itrc = rows.begin();
  for(itset = rowset.begin(); itset != rowset.end(); itset++, itrc++)
    { *itrc = std::vector<number_t>(itset->begin(), itset->end()); }

  buildCsStorage(rows, rowIndex_, colPointer_); // construct rowIndex_ and colPointer_
  trace_p->pop();
}

/*--------------------------------------------------------------------------------
   check if two storages have the same structures
--------------------------------------------------------------------------------*/
bool DualCsStorage::sameStorage(const MatrixStorage& sto) const
{
  if(!(sto.storageType()==storageType_ &&
       sto.accessType()==accessType_ &&
       sto.nbOfRows()==nbRows_ &&
       sto.nbOfColumns()==nbCols_&&
       sto.size()==size()))
    return false;
  const DualCsStorage& csto=static_cast<const DualCsStorage&>(sto);
  //compare structure pointers
  if(colPointer_!=csto.colPointer_) return false;
  if(rowPointer_!=csto.rowPointer_) return false;
  if(rowIndex_!=csto.rowIndex_) return false;
  if(colIndex_!=csto.colIndex_) return false;
  return true;
}

/*--------------------------------------------------------------------------------
 create a new scalar DualCs storage from  current DualCs storage and submatrix sizes (non optimal)
--------------------------------------------------------------------------------*/
DualCsStorage* DualCsStorage::toScalar(dimen_t nbr, dimen_t nbc)
{
  MatrixStorage* nsto=findMatrixStorage(stringId, _cs, _dual, buildType_, true, nbr*nbRows_, nbc*nbCols_);
  if(nsto!=nullptr) return static_cast<DualCsStorage*>(nsto);
  std::vector<std::vector<number_t> > cols=scalarColIndices(nbr, nbc);
  DualCsStorage* ds=  new DualCsStorage(nbr*nbRows_, nbc*nbCols_, cols, stringId);
  ds->buildType() = buildType_;
  ds->scalarFlag()=true;
  return ds;
}


/*--------------------------------------------------------------------------------
 add submatrix indices given by its row and column indices (>= 1)
--------------------------------------------------------------------------------*/
void DualCsStorage::addSubMatrixIndices(const std::vector<number_t>& rows,const std::vector<number_t>& cols)
{
  addCsSubMatrixIndices(rowPointer_,colIndex_,rows,cols,true,false);
  addCsSubMatrixIndices(colPointer_,rowIndex_,cols,rows,true,false);
}

/*--------------------------------------------------------------------------------
  add row-cols (r,c1), (r,c2), ... given by a cols set (r,c>=1)
--------------------------------------------------------------------------------*/
void DualCsStorage::addRow(number_t r, const std::set<number_t>& cols, MatrixPart mp)
{
  if(cols.size()==0) return;  //nothing to add
  if(mp!=_upper) //lower part
    {
      number_t rb=rowPointer_[r-1], re=rowPointer_[r];
      std::set<number_t> allcols;
      //if(re>rb) allcols.insert(colIndex_.begin()+rb, colIndex_.begin()+re-1);
      if(re>rb) allcols.insert(colIndex_.begin()+rb, colIndex_.begin()+re);
      number_t nbc0=allcols.size();
      std::set<number_t>::iterator its=cols.begin();
      for(; its!=cols.end(); its++)
        if(*its < r) allcols.insert(*its -1); //add current cols (-1 shift)
      number_t nc=allcols.size()-nbc0;
      if(nc>0) //new cols added
        {
          std::vector<number_t>::iterator itc=colIndex_.begin()+re;
          colIndex_.insert(itc, nc, 0);          //insert nc element initialized to 0
          its=allcols.begin();
          itc=colIndex_.begin()+rb;
          for(; its!=allcols.end(); its++,itc++) *itc=*its; //copy new columns set
          std::vector<number_t>::iterator itr=rowPointer_.begin()+r;
          for(; itr!=rowPointer_.end(); itr++) *itr+=nc; //shift rowPointer
        }
    }

  if(mp!=_lower) //upper part (quite memory expansive)
    {
      std::map<number_t, std::set<number_t> > mrowindex;
      std::set<number_t>::iterator its;
      for(its=cols.begin(); its!=cols.end(); its++)
        if(*its > r)
          {
            number_t c= *its;
            number_t cb=colPointer_[c-1], ce=colPointer_[c];
            if(ce>cb)
              {
                std::set<number_t> allrows(rowIndex_.begin()+cb, rowIndex_.begin()+ce-1);
                number_t nr=allrows.size();
                allrows.insert(r-1);
                if(allrows.size() > nr) mrowindex[c -1]=allrows; //new row index
              }
          }
      number_t nr=mrowindex.size();
      if(nr==0)  return;  //nothing added

      nr+=rowIndex_.size();
      std::vector<number_t> ncolPointer(nbOfColumns()+1), nrowIndex(nr);
      std::vector<number_t>::iterator itnc=ncolPointer.begin(), itc=colPointer_.begin(),
                                      itnri = nrowIndex.begin(), itri = rowIndex_.begin(), it;
      std::map<number_t, std::set<number_t> >::iterator itms;
      number_t c=0;
      *itc=0; itc++;
      *itnc=0; itnc++;
      for(; c< nbOfColumns(); itc++, itnc++, c++)
        {
          itms=mrowindex.find(c);
          if(itms!=mrowindex.end()) //changed column
            {
              for(its=itms->second.begin(); its!=itms->second.end(); its++,itnri++) *itnri=*its;
              *itnc = *(itnc-1) + itms->second.size();
            }
          else // unchanged column
            {
              for(it=itri + *(itc-1); it!=itri + *itc; it++, itnri++) *itnri=*it;
              *itnc = *(itnc-1) + *itc - *(itc-1);
            }
        }
      colPointer_=ncolPointer;
      rowIndex_ = nrowIndex;
    }
}

/*--------------------------------------------------------------------------------
  add col-rows (r1,c), (r2,c), ... given by a cols set (r,c>=1)
--------------------------------------------------------------------------------*/
void DualCsStorage::addCol(number_t c, const std::set<number_t>& rows, MatrixPart mp)
{
  if(rows.size()==0) return;  //nothing to add
  if(mp!=_lower) //upper part
    {
      std::set<number_t> allrows;
      number_t cb=colPointer_[c-1], ce=colPointer_[c];
      //if(ce>cb) allrows.insert(rowIndex_.begin()+cb, rowIndex_.begin()+ce-1);
      if(ce>cb) allrows.insert(rowIndex_.begin()+cb, rowIndex_.begin()+ce);
      number_t nbr0=allrows.size();
      std::set<number_t>::iterator its=rows.begin();
      for(; its!=rows.end(); its++)
        if(*its < c) allrows.insert(*its -1); //add current cols (-1 shift)
      number_t nr=allrows.size()- nbr0;
      if(nr>0) //new rows added
        {
          std::vector<number_t>::iterator itr=rowIndex_.begin()+ce;
          rowIndex_.insert(itr, nr, 0);          //insert nr element initialized to 0
          its=allrows.begin();
          itr=rowIndex_.begin()+cb;
          for(; its!=allrows.end(); its++,itr++) *itr=*its; //copy new rows set
          std::vector<number_t>::iterator itc=colPointer_.begin()+c;
          for(; itc!=colPointer_.end(); itc++) *itc+=nr; //shift colPointer
        }
    }
  if(mp!=_upper) //lower part (quite memory expansive)
    {
      std::map<number_t, std::set<number_t> > mcolindex;
      std::set<number_t>::iterator its;
      for(its=rows.begin(); its!=rows.end(); its++)
        if(*its > c)
          {
            number_t r= *its;
            number_t rb=rowPointer_[r-1], re=rowPointer_[r];
            if(re>rb)
              {
                std::set<number_t> allcols(colIndex_.begin()+rb, colIndex_.begin()+ re-1);
                number_t nc=allcols.size();
                allcols.insert(c-1);
                if(allcols.size() > nc) mcolindex[r -1]=allcols; //new col index
              }

          }
      number_t nc=mcolindex.size();
      if(nc==0)  return;  //nothing added
      nc+=colIndex_.size();
      std::vector<number_t> nrowPointer(nbOfRows()+1), ncolIndex(nc);
      std::vector<number_t>::iterator itnr=nrowPointer.begin(), itr=rowPointer_.begin(),
                                      itnci = ncolIndex.begin(), itci = colIndex_.begin(), it;
      std::map<number_t, std::set<number_t> >::iterator itms;
      number_t r=0;
      *itr=0; itr++;
      *itnr=0; itnr++;
      for(; r< nbOfRows(); itr++, itnr++, r++)
        {
          itms=mcolindex.find(r);
          if(itms!=mcolindex.end()) //changed column
            {
              for(its=itms->second.begin(); its!=itms->second.end(); its++,itnci++) *itnci=*its;
              *itnr = *(itnr-1) + itms->second.size();
            }
          else // unchanged column
            {
              number_t rb=*(itr-1), re=*itr;
              for(it=itci + rb ; it!=itci + re ; it++, itnci++) *itnci=*it;
              *itnr = *(itnr-1) + re - rb;
            }
        }
      rowPointer_=nrowPointer;
      colIndex_ = ncolIndex;
    }
}


/*--------------------------------------------------------------------------------
 access operator to position of (i,j)>0 coefficients (return 0 if outside)
 This DIRECT access to (i,j) pointer SHOULD NOT be used in matrix computation
--------------------------------------------------------------------------------*/
number_t DualCsStorage::pos(number_t i, number_t j, SymType s) const
{
  if(i == 0 || i > nbRows_ || j == 0 || j > nbCols_) { return 0; }
  if(i == j) { return i; } // diagonal coeff.
  if(i > j) // in lower triangular part
    {
      for(number_t k = rowPointer_[i - 1]; k < rowPointer_[i]; k++) // find in list of col index of row i
        if(colIndex_[k] == j - 1) { return diagonalSize() + k + 1; }     // add 1 because values begin at address 1
      return 0;
    }
  if(j > i) // in upper triangular part
    {
      for(number_t k = colPointer_[j - 1]; k < colPointer_[j]; k++) //find in list of col index of row i
        if(rowIndex_[k] == i - 1) { return diagonalSize() + colIndex_.size() + k + 1; }
    }
  return 0;
}

/*--------------------------------------------------------------------------------
 access to submatrix adresses, useful for assembling matrices
 rows and cols are vector of index (>0) of row and column, no index ordering is assumed !!
 return the vector of adresses (relative number >0 in storage) in a row storage (as Matrix)
 if a row or a col index is out of storage
     an error is raised if errorOn is true (default)
     position is set to 0 else
 if sy!=_noSymmetry: symmetric matrix, only positions of lower part are returned (not used here)
 remark: since the vector storage indexCol_ is ordered for each line, if the indices of columns
          of the submatrix were also, one could write a faster algorithm
--------------------------------------------------------------------------------*/
void DualCsStorage::positions(const std::vector<number_t>& rows, const std::vector<number_t>& cols, std::vector<number_t>& adrs, bool errorOn, SymType sy) const
{
  number_t nba = rows.size() * cols.size();
  if(adrs.size() != nba) { adrs.resize(nba, 0); }      // reset size
  std::vector<number_t>::iterator itp = adrs.begin();
  std::vector<number_t>::const_iterator itr, itc;

  for(itr = rows.begin(); itr != rows.end(); itr++)  // loop on row index
    for(itc = cols.begin(); itc != cols.end(); itc++, itp++) // loop on col indices
      {
        *itp = pos(*itr, *itc);  // non optimal method
        if(*itp == 0 && errorOn) { error("storage_outofstorage","DualCs", *itr, *itc); }
      }
}


// get (col indices, adress) of row r in set [c1,c2] ## SymType not used ##
std::vector<std::pair<number_t, number_t> > DualCsStorage::getRow(SymType s, number_t r, number_t c1, number_t c2) const
{
  number_t nbc=c2;
  if(nbc==0) nbc=nbCols_;
  std::vector<std::pair<number_t, number_t> > cols(nbc-c1+1);
  std::vector<std::pair<number_t, number_t> >::iterator itcol=cols.begin();
  number_t l=0, shift= diagonalSize()+1;
  if(c1 < r) //lower part
    {
      for(number_t k = rowPointer_[r-1]; k< rowPointer_[r]; k++) // find in list of col index of row i
        {
          number_t c=colIndex_[k]+1;
          if(c >= c1 && c <= nbc && c<r) { *itcol = std::make_pair(c, shift+k); l++; itcol++;}
        }
    }
  if(nbc >=r && r>=c1) {*itcol++=std::make_pair(r,r); l++;}    //diag coeff
  for(number_t c=std::max(r+1,c1); c<=nbc; c++)                //upper part
    {
      number_t a=pos(r,c);
      if(a!=0)  {*itcol=std::make_pair(c,a); l++; itcol++;}
    }
  cols.resize(l);
  return cols;
}

// get (row indices, adress) of col c in set [r1,r2] ## SymType not used ##
std::vector<std::pair<number_t, number_t> > DualCsStorage::getCol(SymType s,number_t c, number_t r1, number_t r2) const
{
  number_t nbr=r2;
  if(nbr==0) nbr=nbRows_;
  std::vector<std::pair<number_t, number_t> > rows(nbr-r1+1);
  std::vector<std::pair<number_t, number_t> >::iterator itrow=rows.begin();
  number_t l=0;
  number_t shift=diagonalSize() + colIndex_.size() +1;
  if(r1 < c) //upper part
    {
      for(number_t k = colPointer_[c-1]; k < colPointer_[c]; k++) // find in list of col index of row i
        {
          number_t r=rowIndex_[k]+1;
          if(r >= r1 && r <= nbr && r<c) {*itrow = std::make_pair(r,shift+k); l++; itrow++;}
        }
    }

  if(nbr >=c && c>=r1) {*itrow++=std::make_pair(c,c); l++;}     //diag coeff
  for(number_t r=std::max(c+1,r1); r<=nbr; r++)        //lower part
    {
      number_t a=pos(r,c);
      if(a!=0)
        {
          *itrow=std::make_pair(r,a);
          l++;
          itrow++;
        }
    }
  rows.resize(l);
  return rows;
}

//! get col indices of row r>=1 in set [c1,c2], use colIndex pointer for lower part (efficient for lower part)
std::set<number_t> DualCsStorage::getCols(number_t r, number_t c1, number_t c2) const
{
  std::set<number_t> colset;
  number_t nbc=c2;
  if(nbc==0) nbc=nbCols_;
  if(nbc<c1) return colset; //void set

  //deal with lower part
  number_t d=std::min(r-1,nbc);
  number_t rb=rowPointer_[r-1], re=rowPointer_[r];
  for(number_t k = rb; k < re; k++) // find in list of col index of row i
    {
      number_t c=colIndex_[k]+1;
      if(c >= c1 && c<=d) colset.insert(c);
    }
  if(r>nbCols_) return colset; //no diag, no upper part
  if(nbc>=r ) colset.insert(r);  //add diag index
  if(nbc>r)  //deal with upper part
    {
      //std::set<number_t> colup=CsStorage::getCols(r,r+1,nbc); //call generic algorithm
      //if(colup.size()>0) colset.insert(colup.begin(),colup.end());  //add upper index
      number_t k;
      std::vector<number_t>::const_iterator itr=rowIndex_.begin()+colPointer_[r], itre;
      for(number_t j=r+1; j<=nbc; ++j)
        {
          k=(*itr)+1;
          itre = rowIndex_.begin() + colPointer_[j];
          while(k!=r && itr<itre)  {++itr; k=(*itr)+1;}
          if(itr<itre && k==r) colset.insert(j);
          itr=itre;
        }
    }
  return colset;
}

//! get row indices of col c in set [r1,r2], use rowIndex pointer for upper part (efficient for upper part)
std::set<number_t> DualCsStorage::getRows(number_t c, number_t r1, number_t r2) const
{
  std::set<number_t> rowset;
  number_t nbr=r2;
  if(nbr==0) nbr=nbRows_;
  if(nbr<r1) return rowset;

  //deal with upper part
  number_t d=std::min(c-1,nbr);
  number_t cb=colPointer_[c-1], ce=colPointer_[c];
  for(number_t k = cb; k < ce; k++) // find in list of row index of row i
    {
      number_t r=rowIndex_[k]+1;
      if(r >= r1 && r<=d) rowset.insert(r);
    }
  if(c>nbRows_) return rowset; //no diag, no lower part
  if(nbr>=c) rowset.insert(c);  //add diag index
  if(nbr>c)  //deal with lower part
    {
      std::set<number_t> rowlow=CsStorage::getRows(c,c+1,nbr); //call generic algorithm
      if(rowlow.size()>0) rowset.insert(rowlow.begin(),rowlow.end());  //add upper index
    }
  return rowset;
}

//! get col indices of row r in set [c1,c2], use colIndex pointer for lower part (efficient for lower part)
void DualCsStorage::getColsV(std::vector<number_t>& colsv, number_t& nbcols, number_t r, number_t c1, number_t c2) const
{
  nbcols=0;
  number_t nbc=c2;
  if(nbc==0) nbc=nbCols_;
  if(nbc<c1) return; //void set

  //deal with lower part
  number_t d=std::min(r-1,nbc);
  number_t rb=rowPointer_[r-1], re=rowPointer_[r];
  std::vector<number_t>::iterator itv=colsv.begin();
  for(number_t k = rb; k < re; k++) // find in list of col index of row i
    {
      number_t c=colIndex_[k]+1;
      if(c >= c1 && c<=d) {*itv=c; itv++; nbcols++;}
    }
  if(nbc>=r) {*itv=r; itv++; nbcols++;} //add diag index
  if(nbc>r)  //deal with upper part
    {
      std::set<number_t> colup=CsStorage::getCols(r,r+1,nbc); //call generic algorithm
      std::set<number_t>::iterator its=colup.begin();
      for(; its!=colup.end(); its++)
        if(*its!=0) {*itv=*its; itv++; nbcols++;}
    }
}

//! get row indices of col c in set [r1,r2], use rowIndex pointer for upper part (efficient for upper part)
void DualCsStorage::getRowsV(std::vector<number_t>& rowsv, number_t& nbrows, number_t c, number_t r1, number_t r2) const
{
  nbrows=0;
  number_t nbr=r2;
  if(nbr==0) nbr=nbRows_;
  if(nbr<r1) return;

  //deal with upper part
  number_t d=std::min(c-1,nbr);
  number_t cb=colPointer_[c-1], ce=colPointer_[c];
  std::vector<number_t>::iterator itv=rowsv.begin();
  for(number_t k = cb; k < ce; k++) // find in list of row index of row i
    {
      number_t r=rowIndex_[k]+1;
      if(r >= r1 && r<=d) {*itv=r; itv++; nbrows++;}
    }
  if(nbr>=c) {*itv=c; itv++; nbrows++;}; //add diag index
  if(nbr>c)  //deal with lower part
    {
      std::set<number_t> rowlow=CsStorage::getRows(c,c+1,nbr); //call generic algorithm
      std::set<number_t>::iterator its=rowlow.begin();
      for(; its!=rowlow.end(); its++)
        if(*its!=0) {*itv=*its; itv++; nbrows++;}
    }
}

/*--------------------------------------------------------------------------------
 return skyline row/col pointer from current dual cs storage
 --------------------------------------------------------------------------------*/
std::vector<number_t> DualCsStorage::skylineRowPointer() const
{
  return skylinePointer(rowPointer_,colIndex_);
}
std::vector<number_t> DualCsStorage::skylineColPointer() const   //same as skylineRowPointer
{
  return skylinePointer(colPointer_,rowIndex_);
}
/*--------------------------------------------------------------------------------
 fill values of current storage as a skyline storage
 does not required explicit skyline storage !
 WARNING: skyval has to be previously correctly sized and initialized at 0 !!!
 --------------------------------------------------------------------------------*/
void DualCsStorage::fillSkylineValues(const std::vector<real_t>& csval, std::vector<real_t>& skyval,SymType sym, MatrixStorage* skysto) const
{
  std::vector<real_t>::const_iterator itm = csval.begin() + 1;
  std::vector<real_t>::iterator itms = skyval.begin() + 1;
  fillSkylineDiagonalPart(itm,itms);
  fillSkylineTriangularPart(rowPointer_,colIndex_, itm, itms);  //copy lower part
  if(sym==_noSymmetry) fillSkylineTriangularPart(colPointer_,rowIndex_,itm,itms);  //copy upper part if there is no symmetry
}

void DualCsStorage::fillSkylineValues(const std::vector<complex_t>& csval, std::vector<complex_t>& skyval,SymType sym, MatrixStorage* skysto) const
{
  std::vector<complex_t>::const_iterator itm = csval.begin() + 1;
  std::vector<complex_t>::iterator itms = skyval.begin() + 1;
  fillSkylineDiagonalPart(itm,itms);
  fillSkylineTriangularPart(rowPointer_,colIndex_, itm, itms);  //copy lower part
  if(sym==_noSymmetry) fillSkylineTriangularPart(colPointer_,rowIndex_,itm,itms);  //copy upper part if there is no symmetry
}

void DualCsStorage::fillSkylineValues(const std::vector<Matrix<real_t> >& csval, std::vector<Matrix<real_t> >& skyval,SymType sym, MatrixStorage* skysto) const
{
  std::vector<Matrix<real_t> >::const_iterator itm = csval.begin() + 1;
  std::vector<Matrix<real_t> >::iterator itms = skyval.begin() + 1;
  fillSkylineDiagonalPart(itm,itms);
  fillSkylineTriangularPart(rowPointer_,colIndex_, itm, itms);  //copy lower part
  if(sym==_noSymmetry) fillSkylineTriangularPart(colPointer_,rowIndex_,itm,itms);  //copy upper part if there is no symmetry
}

void DualCsStorage::fillSkylineValues(const std::vector<Matrix<complex_t> >& csval, std::vector<Matrix<complex_t> >& skyval,SymType sym, MatrixStorage* skysto) const
{
  std::vector<Matrix<complex_t> >::const_iterator itm = csval.begin() + 1;
  std::vector<Matrix<complex_t> >::iterator itms = skyval.begin() + 1;
  fillSkylineDiagonalPart(itm,itms);
  fillSkylineTriangularPart(rowPointer_,colIndex_, itm, itms);  //copy lower part
  if(sym==_noSymmetry) fillSkylineTriangularPart(colPointer_,rowIndex_,itm,itms);  //copy upper part if there is no symmetry
}

// print storage pointers
void DualCsStorage::print(std::ostream& os) const
{
  printHeader(os);
  if (theVerboseLevel < 2) { return; }
  os<<"row pointer = "<<rowPointer_<<eol;
  os<<"column index = "<<colIndex_<<eol;
  os<<"column pointer = "<<colPointer_<<eol;
  os<<"row index = "<<rowIndex_<<eol;
}


/*--------------------------------------------------------------------------------
 print SymCs Storage matrix to output stream
--------------------------------------------------------------------------------*/
// print real scalar matrix
void DualCsStorage::printEntries(std::ostream& os, const std::vector<real_t>& m, number_t vb, const SymType sym) const
{
  std::vector<real_t>::const_iterator itm = m.begin() + 1, itml = itm + diagonalSize(),
                                      itmu = itml + colIndex_.size();
  os << "lower triangular part ";
  printEntriesTriangularPart(_scalar, itm, itml, colIndex_, rowPointer_, entriesPerRow, entryWidth, entryPrec, "row", vb, os);
  os << "upper triangular part";
  itm = m.begin() + 1;
  printEntriesTriangularPart(_scalar, itm, itmu, rowIndex_, colPointer_, entriesPerRow, entryWidth, entryPrec, "col", vb, os);
}

// print complex scalar matrix
void DualCsStorage::printEntries(std::ostream& os, const std::vector<complex_t>& m, number_t vb, SymType sym) const
{
  std::vector<complex_t>::const_iterator itm = m.begin() + 1, itml = itm + diagonalSize(),
                                         itmu = itml + colIndex_.size();
  os << "lower triangular part ";
  printEntriesTriangularPart(_scalar, itm, itml, colIndex_, rowPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "row", vb, os);
  os << "upper triangular part";
  itm = m.begin() + 1;
  printEntriesTriangularPart(_scalar, itm, itmu, rowIndex_, colPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "col", vb, os);
}

// large matrix of vectors are not available for the moment, use Matrix n x 1 or 1 x n
void DualCsStorage::printEntries(std::ostream& os, const std::vector< Vector<real_t> >& m, number_t vb, SymType sym) const
{error("storage_novector", "DualCsStorage::printEntries");}

void DualCsStorage::printEntries(std::ostream& os, const std::vector< Vector<complex_t> >& m, number_t vb, SymType sym) const
{error("storage_novector", "DualCsStorage::printEntries");}

// print matrix of real matrices
void DualCsStorage::printEntries(std::ostream& os, const std::vector< Matrix<real_t> >& m, number_t vb, SymType sym) const
{
  std::vector< Matrix<real_t> >::const_iterator itm = m.begin() + 1, itml = itm + diagonalSize(),
                                                itmu = itml + colIndex_.size();
  os << "lower triangular part ";
  printEntriesTriangularPart(_matrix, itm, itml, colIndex_, rowPointer_, entriesPerRow, entryWidth, entryPrec, "row", vb, os);
  os << "upper triangular part";
  itm = m.begin() + 1;
  printEntriesTriangularPart(_matrix, itm, itmu, rowIndex_, colPointer_, entriesPerRow, entryWidth, entryPrec, "col", vb, os);
}

// print matrix of complex matrices
void DualCsStorage::printEntries(std::ostream& os, const std::vector< Matrix<complex_t> >& m, number_t vb, SymType sym) const
{
  std::vector< Matrix<complex_t> >::const_iterator itm = m.begin() + 1, itml = itm + diagonalSize(),
                                                   itmu = itml + colIndex_.size();
  os << "lower triangular part ";
  printEntriesTriangularPart(_matrix, itm, itml, colIndex_, rowPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "row", vb, os);
  os << "upper triangular part";
  itm = m.begin() + 1;
  printEntriesTriangularPart(_matrix, itm, itmu, rowIndex_, colPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "col", vb, os);
}

/*
--------------------------------------------------------------------------------
 print matrix to ostream in coordinate form: i j M_ij
 according to type of values of matrix (overload the generic method in MatrixStorage)
 sym is not used
--------------------------------------------------------------------------------
*/
void DualCsStorage::printCooMatrix(std::ostream& os, const std::vector<real_t>& m, SymType sym) const
{
  std::vector<real_t>::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml + lowerPartSize();
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, colIndex_, rowPointer_, true);
  printCooTriangularPart(os, itmu, rowIndex_, colPointer_, false);
}

void DualCsStorage::printCooMatrix(std::ostream& os, const std::vector<complex_t>& m, SymType sym) const
{
  std::vector<complex_t>::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml + lowerPartSize();
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, colIndex_, rowPointer_, true);
  printCooTriangularPart(os, itmu, rowIndex_, colPointer_, false);
}

void DualCsStorage::printCooMatrix(std::ostream& os, const std::vector< Matrix<real_t> >& m, SymType sym) const
{
  std::vector< Matrix<real_t> >::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml + lowerPartSize();
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, colIndex_, rowPointer_, true);
  printCooTriangularPart(os, itmu, rowIndex_, colPointer_, false);
}

void DualCsStorage::printCooMatrix(std::ostream& os, const std::vector< Matrix<complex_t> >& m, SymType sym) const
{
  std::vector< Matrix<complex_t> >::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml + lowerPartSize();
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, colIndex_, rowPointer_, true);
  printCooTriangularPart(os, itmu, rowIndex_, colPointer_, false);
}

/*--------------------------------------------------------------------------------
 template Matrix x Vector & Vector x Matrix multiplications and specializations
 result vector has to be sized before
--------------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void DualCsStorage::multMatrixVector(const std::vector<M>& m, V* vp, R* rp) const
{
  trace_p->push("DualCsStorage::multMatrixVector (pointer form)");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  R* rpe= rp + diagonalSize();
  diagonalMatrixVector(itm, vp, rp, rpe);   // product by diagonal
  itl = m.begin() + 1 + diagonalSize();
  lowerMatrixVector(colIndex_, rowPointer_, itl, vp, rp, _noSymmetry);  // lower part
  itl = m.begin() + 1 + diagonalSize() + colIndex_.size();
  CsStorage::upperMatrixVector(rowIndex_, colPointer_, itl, vp, rp, _noSymmetry);  // upper part
  trace_p->pop();
}

template<typename M, typename V, typename R>
void DualCsStorage::multMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  trace_p->push("DualCsStorage::multMatrixVector");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = itrb + diagonalSize();
  diagonalMatrixVector(itm, itvb, itrb, itre);   // product by diagonal
  itl = m.begin() + 1 + diagonalSize();
  CsStorage::lowerMatrixVector(colIndex_, rowPointer_, itl, itvb, itrb, _noSymmetry);  // lower part
  itl = m.begin() + 1 + diagonalSize() + colIndex_.size();
  CsStorage::upperMatrixVector(rowIndex_, colPointer_, itl, itvb, itrb, _noSymmetry);  // upper part
  trace_p->pop();
}

template<typename M, typename V, typename R>
void DualCsStorage::multVectorMatrix(const std::vector<M>& m, V* vp, R* rp) const
{
  trace_p->push("DualCsStorage::multVectorMatrix (pointer form)");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  R* rpe= rp + diagonalSize();
  diagonalVectorMatrix(itm, vp, rp, rpe);   // product by diagonal
  itl = m.begin() + 1 + diagonalSize();
  lowerVectorMatrix(colIndex_, rowPointer_, itl, vp, rp, _noSymmetry);  // lower part
  itl = m.begin() + 1 + diagonalSize() + colIndex_.size();
  upperVectorMatrix(rowIndex_, colPointer_, itl, vp, rp, _noSymmetry);  // upper part
  trace_p->pop();
}

template<typename M, typename V, typename R>
void DualCsStorage::multVectorMatrix(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  trace_p->push("DualCsStorage::multVectorMatrix");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = itrb + diagonalSize();
  diagonalVectorMatrix(itm, itvb, itrb, itre);   // product by diagonal
  itl = m.begin() + 1 + diagonalSize();
  lowerVectorMatrix(colIndex_, rowPointer_, itl, itvb, itrb, _noSymmetry);  // lower part
  itl = m.begin() + 1 + diagonalSize() + colIndex_.size();
  upperVectorMatrix(rowIndex_, colPointer_, itl, itvb, itrb, _noSymmetry);  // upper part
  trace_p->pop();
}

/*!
   Add two matrices
   \param m1 vector values_ of first matrix
   \param m2 vector values_ of second matrix
   \param r vector values_ of result matrix
 */
template<typename M1, typename M2, typename R>
void DualCsStorage::addMatrixMatrix(const std::vector<M1>& m1, const std::vector<M2>& m2, std::vector<R>& r) const
{
  trace_p->push("DualCsStorage::addMatrixMatrix");
  typename std::vector<M1>::const_iterator itm1 = m1.begin() + 1;
  typename std::vector<M2>::const_iterator itm2 = m2.begin() + 1;
  typename std::vector<R>::iterator itrb = r.begin() + 1, itre = r.end();
  sumMatrixMatrix(itm1, itm2, itrb, itre);
  trace_p->pop();
}

/*!
   Extract and convert matrix storage to UMFPack format (Matlab sparse matrix)
   \param m1 vector values_ current matrix
   \param colPtUmf vector column Pointer of UMFPack format
   \param rowIdxUmf vector row Index of UMFPack format
   \param resultUmf vector values of UMFPack format
 */
template<typename M1, typename Idx>
void DualCsStorage::toUmfPack(const std::vector<M1>& m1, std::vector<Idx>& colPtUmf, std::vector<Idx>& rowIdxUmf, std::vector<M1>& resultUmf) const
{
  // Reserve memory for resultUmf and set its size to 0
  resultUmf.clear();
  resultUmf.reserve(m1.size());
  // Reserve memory for rowIdxUmf and set its size to 0
  rowIdxUmf.clear();
  rowIdxUmf.reserve(m1.size());
  // Resize column pointer as dualCs's column pointer size
  colPtUmf.clear();
  colPtUmf.resize(colPointer_.size());

  // Because colPointer always begins with 0, so need to count from the second position
  typename std::vector<Idx>::iterator itColPtUmf = colPtUmf.begin()+1;
  std::vector<number_t>::const_iterator itfColPt = colPointer_.begin();
  std::vector<number_t>::const_iterator itsColPt = colPointer_.begin()+1;
  std::vector<number_t>::const_iterator itRowIdx = rowIndex_.begin();
  std::vector<number_t>::const_iterator itbColIdx = colIndex_.begin(), itColIdx = itbColIdx;
  std::vector<number_t>::const_iterator iteColIdx = colIndex_.end();
  std::vector<number_t>::const_iterator itRowPt = rowPointer_.begin();
  typename std::vector<M1>::const_iterator itmDiag = m1.begin()+1;
  typename std::vector<M1>::const_iterator itmLow  = itmDiag + diagonalSize();
  typename std::vector<M1>::const_iterator itmUp   = itmLow + lowerPartSize();
  typename std::vector<M1>::const_iterator itval;

  Idx nzCol = Idx(0);   // Number of non-zero in each "working" column
  number_t diagIdx = 0; // Index of diagonal elements
  number_t colIdx = 0;  // Index of current column
  for(; itsColPt != colPointer_.end(); ++itsColPt,++itfColPt,++itColPtUmf)
    {
      nzCol = *itsColPt - *itfColPt; // Number of non-zero value in current column of upper part
      *itColPtUmf += nzCol;          // Update column pointer

      // Always process upper part first
      while(nzCol>0)
        {
          resultUmf.push_back(*itmUp);    // Update values
          ++itmUp; --nzCol;
          rowIdxUmf.push_back(*itRowIdx); // Update rowIdxUmf
          ++itRowIdx;
        }

      // Process diagonal part
      if(diagIdx < diagonalSize())
        {
          if(M1(0) != *itmDiag)
            {
              resultUmf.push_back(*itmDiag); // Update values
              ++nzCol;                       // Update colPtUmf with number of non-zero value
              rowIdxUmf.push_back(diagIdx);  // Update rowIdxUmf
            }
        }
      ++itmDiag; ++diagIdx;

      // Process lower part
      itColIdx = itbColIdx;
      while(itColIdx != iteColIdx)
        {
          itColIdx = std::find(itColIdx,iteColIdx, colIdx);
          if(itColIdx != iteColIdx)
            {
              itRowPt = std::find_if(rowPointer_.begin(), rowPointer_.end(),std::bind(std::greater_equal<number_t>(), std::placeholders::_1, itColIdx - itbColIdx+1));
              itval = (itColIdx-itbColIdx) + itmLow;              // Update values
              resultUmf.push_back(*itval);
              ++nzCol;                                            // Update colPtUmf with number of non-zero value
              rowIdxUmf.push_back(itRowPt-rowPointer_.begin()-1); // Update rowIdxUmf
              ++itColIdx;                                         // Increase iterator to make a search in new range
            }
        }

      *itColPtUmf += *(itColPtUmf-1) + nzCol;  // Update colPtUmf with number of non-zero value
      ++colIdx;   // Move to next column
    }
}

template<typename M>
void DualCsStorage::ilu(std::vector<M>& m, std::vector<M>& f) const
{
  /*
   Factorization of matrix M stored as matrix F = L U where
   - L is a lower triangular matrix with unit diagonal and is stored as
   the strict lower triangular part of F
   - U is a upper triangular matrix with diagonal stored as diagonal part of F
   and strict upper triangular part stored as the strict upper triangular part of F:

  */
  trace_p->push("DualCsStorage::ilu");
  typename std::vector<M>::iterator d_i=f.begin()+1, l_ib=d_i+nbRows_ , u_ib=l_ib+colIndex_.size();
  typename std::vector<M>::iterator ind_ik, ind_kj, ind_kl, ind_ki, ind_lk;
  for (number_t k=0; k < nbRows_; ++k)
  {
       M pivot = *(d_i+k);  // pivot=Akk
       if(std::abs(pivot) < theZeroThreshold) { error("small_pivot"); }
       ind_ik = l_ib;
       ind_ki = u_ib;
       for (number_t i=k+1; i<nbRows_; i++)
       {
           // A ik/= pivot
           M Aik=0;
           for ( number_t p=rowPointer_[i]; p<rowPointer_[i+1]; p++ )
             {
                  if (colIndex_[p] == k)
                       {
                          ind_ik = l_ib+p;
                         *ind_ik /= pivot;
                          Aik=*ind_ik;
                    // k<j<i -> L
                    // Aij -= Aik*Akj
                          for ( number_t j=k+1; j<i ; j++)   // < -> <=
                          { // Aij
                              for ( number_t p=rowPointer_[i]; p<rowPointer_[i+1]; p++ )
                                     if (colIndex_[p] == j)
                                       {  //Akj
                                         for ( number_t q=colPointer_[j]; q<colPointer_[j+1]; q++ )
                                               if (rowIndex_[q] == k)   //Akj exists
                                                 {
                                                     ind_kj = u_ib+q;
                                                    *(l_ib+p) -= *ind_ik * (*ind_kj);
                                                     break;
                                                  }
                                         break;
                                       }
                          }
                         break;
                       }
              }
           // k<l<i -> U
           // Ali -= Alk*Aki
           // Aki
           M Aki = 0;
           for ( number_t p=colPointer_[i]; p<colPointer_[i+1] ; p++ )
            {
                 if (rowIndex_[p] == k)
                    {
                        ind_ki = u_ib+p ;
                        Aki = *ind_ki;
                        for (number_t l=k+1; l<i    ; l++)
                          {// Alk
                              for ( number_t p=rowPointer_[l]; p<rowPointer_[l+1]; ++p )
                                     if (colIndex_[p] == k)
                                       {
                                          ind_lk = l_ib+p ;
                                         // Ali
                                          for ( number_t p=colPointer_[i]; p<colPointer_[i+1]; ++p )
                                               if (rowIndex_[p] == l)
                                                 {
                                                    *(u_ib+p) -= *ind_lk * (*ind_ki);
                                                     break;
                                                  }
                                          break;
                                        }
                          }
                        break;
                     }
             }// end for
             //Aii -= Aik*Aki
             *(d_i+i) -= Aik*Aki;   //*ind_ik * (*ind_ki)
        }// end loop i
  }// end loop k

  trace_p->pop();
}

/*
--------------------------------------------------------------------------------
     Template lower triangular part with unit diagonal solver (after factorization)
--------------------------------------------------------------------------------
*/
template<typename M, typename V, typename X>
void DualCsStorage::lowerD1Solver(const std::vector<M>& m,  std::vector<V>& v, std::vector<X>& x) const
{
  /*
     solver for a lower triangular part linear system with unit diagonal (I+L)*X=b
  */
  // no const v here as v might coincide with x
  trace_p->push("DualCsStorage::lowerD1Solver");
  CsStorage::bzLowerD1Solver(m.begin() + 1 + v.size(), v.begin(), x.begin(), x.end(), colIndex_, rowPointer_);
  trace_p->pop();
}

template<typename M, typename V, typename X>
void DualCsStorage::lowerD1LeftSolver(const std::vector<M>& m,  std::vector<V>& v, std::vector<X>& x) const
{
  /*
     solver for a transposed lower triangular part linear system with unit diagonal (I+L)t*X=b
  */
  // no const v here as v might coincide with x
  trace_p->push("DualCsStorage::lowerD1LeftSolver");
  typename std::vector<M>::const_reverse_iterator it_ru = m.rbegin() + upperPartSize(), it_rd = it_ru + lowerPartSize();
  typename std::vector<V>::const_reverse_iterator it_rbb = v.rbegin();
  typename std::vector<X>::reverse_iterator it_rxb = x.rbegin(), it_rxe = x.rend();
  bzUpperD1Solver(it_rd, it_ru, it_rbb, it_rxb, it_rxe, rowIndex_, colPointer_, 1.) ;
  trace_p->pop();
}
/*
--------------------------------------------------------------------------------
     Template lower triangular part  solver (after factorization)
--------------------------------------------------------------------------------
*/
template<typename M, typename V, typename X>
void DualCsStorage::lowerSolver(const std::vector<M>& m,  std::vector<V>& v, std::vector<X>& x) const
{
  /*
     solver for a lower triangular part linear system (D+L)*X=B
  */
  // no const v here as v might coincide with x
  trace_p->push("DualCsStorage::lowerSolver");
  typename std::vector<M>::const_iterator itmDiag = m.begin()+1;
  typename std::vector<M>::const_iterator itmLow  = itmDiag + diagonalSize();
  typename std::vector<V>::iterator it_vb = v.begin();
  typename std::vector<X>::iterator it_xb = x.begin(), it_xe = x.end();
  bzSorLowerSolver(itmDiag, itmLow, it_vb, it_xb, it_xe, colIndex_, rowPointer_, 1.);
  trace_p->pop();
}
/*
--------------------------------------------------------------------------------
     Template upper triangular part  solver (after factorization)
--------------------------------------------------------------------------------
*/
template<typename M, typename V, typename X>
void DualCsStorage::upperSolver(const std::vector<M>& m,  std::vector<V>& b, std::vector<X>& x, SymType sym) const
{
  /*
     solver for a upper triangular part linear system:(D+U)*X=B
  */
  // no const v here as v might coincide with x
  trace_p->push("DualCsStorage::upperSolver");
  typename std::vector<M>::const_reverse_iterator it_ru = m.rbegin(), it_rd = it_ru + upperPartSize() + lowerPartSize();
  typename std::vector<V>::const_reverse_iterator it_rbb = b.rbegin();
  typename std::vector<X>::reverse_iterator it_rxb = x.rbegin(), it_rxe = x.rend();
  bzSorUpperSolver(it_rd, it_ru, it_rbb, it_rxb, it_rxe, colIndex_, rowPointer_,  1.,  sym) ;
  trace_p->pop();
}

template<typename M, typename V, typename X>
void DualCsStorage::upperLeftSolver(const std::vector<M>& m,  std::vector<V>& b, std::vector<X>& x, SymType sym) const
{
  /*
     solver for a transposed upper triangular part linear system:(D+U)t*X=B
  */
  // no const v here as v might coincide with x
  trace_p->push("DualCsStorage::upperLeftSolver");
  typename std::vector<M>::const_iterator itdb = m.begin()+1, itlb = itdb + diagonalSize() + lowerPartSize();
  typename std::vector<V>::const_iterator itbb = b.begin();
  typename std::vector<X>::iterator itxb = x.begin(), itxe = x.end();
  bzSorLowerSolver(itdb, itlb, itbb, itxb, itxe, rowIndex_, colPointer_, 1.);
  trace_p->pop();
}


} // end of namespace xlifepp

