/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file CsStorage.cpp
  \authors D. Martin, E. Lunéville, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Implementation of xlifepp::CsStorage class and functionnalities
*/

#include "CsStorage.hpp"
#include "SymCsStorage.hpp"
#include "DualCsStorage.hpp"
#include "RowCsStorage.hpp"
#include "ColCsStorage.hpp"
#include <list>

namespace xlifepp
{
/*=======================================================================================
  Constructors, destructor
  =======================================================================================*/
// constructor with access
CsStorage::CsStorage(AccessType ac, string_t id) : MatrixStorage(_cs, ac, id) {}

// constructor of a square matrix n x n
CsStorage::CsStorage(number_t n, AccessType ac, string_t id) : MatrixStorage(_cs, ac, n, n, id) {}

// constructor of a matrix m x n
CsStorage::CsStorage(number_t m, number_t n, AccessType ac, string_t id) : MatrixStorage(_cs, ac, m, n, id) {}

// construct row (resp. col) storage vectors (index and pointer)  from the indices of columns (resp rows)
// stored in a vector<vector<number_t> > (rcIndex), called by CsStorage child class                          ###MOVE IN TEMPLATE FORM SEE.HPP
// note: rcIndex starts at 1 !!!
//void CsStorage::buildCsStorage(const std::vector<std::vector<number_t> >& rcIndex, std::vector<number_t>& index, std::vector<number_t>& pointer)
//{
//  trace_p->push("CsStorage::buildCsStorage");
//  //update row/colPointer_
//  std::vector<std::vector<number_t> >::const_iterator itvs;
//  number_t nnz = 0;
//  pointer.resize(rcIndex.size() + 1,0);    //+1 to store the size of stored values (more convenient)
//  std::vector<number_t>::iterator itvn = pointer.begin();
//  for(itvs = rcIndex.begin(); itvs != rcIndex.end(); itvs++, itvn++)
//    {
//      *itvn = nnz;
//      nnz += itvs->size();
//    }
//  *itvn = nnz; // last of rowPointers_ is the number of non zeros
//  // update col/rowIndex_
//  index.resize(nnz);
//  itvn = index.begin();
//  std::vector<number_t>::const_iterator its;
//  for(itvs = rcIndex.begin(); itvs != rcIndex.end(); itvs++)
//    for(its = itvs->begin(); its != itvs->end(); its++, itvn++) { *itvn = *its -1; }
//  // end of construction
//  trace_p->pop();
//}

/*! go to scalar cs storage from non scalar cs storage (generic routine for all cs storage)
    crpointer: col/rowpointer of non scalar cs storage
    rcindex: row/colindex of non scalar cs storage
    nbrc: row/col size of submatrix
    nbcr: col/row size of submatrix
    scrpointer: col/rowpointer of scalar cs storage to be set (already sized)
    srcindex: row/colindex of scalar cs storage to be set (already sized)
*/
void CsStorage::toScalarCs(const std::vector<number_t>& crpointer, const std::vector<number_t>& rcindex,
                           dimen_t nbrc, dimen_t nbcr,
                           std::vector<number_t>& scrpointer, std::vector<number_t>& srcindex)
{
  std::vector<number_t>::const_iterator itcr=crpointer.begin();
  std::vector<number_t>::const_iterator itrc=rcindex.begin();
  std::vector<number_t>::iterator itcrs=scrpointer.begin();
  std::vector<number_t>::iterator itrcs=srcindex.begin();
  *itcrs=0;
  for(; itcr!=crpointer.end()-1; itcr++)
    {
      for(dimen_t j=0; j<nbcr; j++, itcrs++)
        {
          number_t rcb=*itcr, rce=*(itcr+1);
          itrc=rcindex.begin()+rcb;
          *(itcrs +1) = *itcrs + nbrc*(rce - rcb);
          for(number_t rc=rcb; rc<rce; rc++, itrc++)
            for(dimen_t k=0; k< nbrc; k++, itrcs++) *itrcs= nbrc* *itrc + k;
        }
    }
}

//create a new dual cs storage from sym cs storage
MatrixStorage* CsStorage::toDual()
{
  if(accessType_!=_sym)
  {
    where("CsStorage::toDual");
    error("symmetric_only"); return nullptr;
  }
  SymCsStorage* scs=dynamic_cast<SymCsStorage*>(this);  //downcast
  if(scs==nullptr)
  {
    where("CsStorage::toDual");
    error("downcast_failure", "SymCsStorage"); return nullptr;
  }
  return new DualCsStorage(nbRows_, nbCols_,scs->colIndex_,scs->rowPointer_,scs->colIndex_,scs->rowPointer_);
}

/*!--------------------------------------------------------------------------------
 add submatrix indices given by its row and column indices (>= 1)
 row/col algorithm,
 if lower=true only strict lower/upper part of submatrix is "added"
 if diag=true diagonal coefficient are also added (case of csRow and csCol)
--------------------------------------------------------------------------------*/
void CsStorage::addCsSubMatrixIndices(std::vector<number_t>& rowPointer, std::vector<number_t>& colIndex,
                                    const std::vector<number_t>& rows,const std::vector<number_t>& cols,
                                    bool lower, bool diag)
{
  trace_p->push("CsStorage::addSubMatrixIndices");

  std::vector<number_t>::const_iterator itr,itc;
  std::vector<number_t> newRowPointer(rowPointer.size(),0);
  std::list<number_t> newColIndex;
  std::vector<number_t>::iterator itrp=newRowPointer.begin(), itp=rowPointer.begin(), itcol=colIndex.begin();

  std::map<number_t,number_t> rowsMap; //to speed up algorithm
  number_t r=1;
  for(itr=rows.begin(); itr!=rows.end(); itr++,r++)  rowsMap[*itr]=r;
  number_t nbrows=rowPointer.size()-1;
  for(r=1; r<=nbrows; itrp++, itp++, r++)
    {
      //build colIndex/rowIndex of row/col r
      std::set<number_t> scol;
      number_t k1=*itp, k2=*(itp+1);
      if(k2>k1) scol.insert(itcol+k1,itcol+k2); //insert existing cols in list scol
      if(rowsMap.find(r)!=rowsMap.end())        //if row to add, insert new cols in list scol
        {
          for(itc=cols.begin(); itc!=cols.end(); itc++)
            if( r >*itc || (diag && r==*itc) || (!lower && r<*itc)) scol.insert(*itc-1);  //insert column index in set
        }
      //update new storage
      number_t s=scol.size();
      *(itrp+1)=*itrp+s;
      if(s>0) newColIndex.insert(newColIndex.end(),scol.begin(),scol.end());
    }
  rowPointer=newRowPointer;
  colIndex.assign(newColIndex.begin(),newColIndex.end());
  trace_p->pop();
}

//create the skyline row/col pointer from cs row/col pointer and cs col/row index
std::vector<number_t> skylinePointer(const std::vector<number_t>& csPointer,const std::vector<number_t>& csIndex)
{
  trace_p->push("skylinePointer");
  std::vector<number_t> skyPointer(csPointer.size(),0);
  std::vector<number_t>::const_iterator itr, itc=csIndex.begin();
  std::vector<number_t>::iterator its=skyPointer.begin();
  number_t i=0;
  for(itr=csPointer.begin(); itr!=csPointer.end()-1; itr++,its++,i++)
    {
      number_t cmin=i;
      for(number_t k=*itr; k<*(itr+1); k++) cmin=std::min(*itc++,cmin); //first column number (do not assume that column indexes are ordered)
      *(its+1)=*its + i-cmin;
    }
  trace_p->pop();
  return skyPointer;
}

//balance matrix coefficients on numThread threads
//   create the two vectors itThreadLower and itThreadUpper containing lower_bound and upper_bound iterators of coefficient ranges
//   Note: numThread may be changed to fill well
void CsStorage::extractThreadIndex(const std::vector<number_t>& pointer, const std::vector<number_t>& index,
                                   number_t &numThread,
                                   std::vector<std::vector<number_t>::const_iterator>& itThreadLower,
                                   std::vector<std::vector<number_t>::const_iterator>& itThreadUpper) const
{
  std::vector<number_t>::const_iterator itpb = pointer.begin(), itpe = pointer.end()-1, itp = itpb;
  std::vector<number_t>::const_iterator itpLower, itpUpper;

  number_t nnz = index.size(); // Number of non-zero entries

  // Assign each thread an approximately equal number of non-zero
  number_t nnzEachThread = number_t(nnz/numThread);
  number_t nnzRangeIdx = nnzEachThread;
  itp = itpb;
  number_t i = 0;
  for (; i < numThread && itp!=itpe; i++)
    {
      itThreadLower[i] = itp;
      nnzRangeIdx = *itp + nnzEachThread;
      itpLower = std::lower_bound(itp, itpe, nnzRangeIdx);
      itpUpper = std::upper_bound(itpLower, itpe,nnzRangeIdx);
      itp = ((nnzRangeIdx - *(itpUpper -1))< (*itpUpper-nnzRangeIdx)) ? itpUpper-1 : itpUpper;
      itThreadUpper[i] = itp;
    }
  itThreadUpper[i-1] = itpe;
}

/*!--------------------------------------------------------------------------------
  return a col/row access storage from current one
--------------------------------------------------------------------------------*/
CsStorage* CsStorage::toColStorage()
{
    if(accessType_ == _col) return this; //already a col storage
    return new ColCsStorage(nbRows_, nbCols_, scalarColIndices(1,1), stringId);
}

CsStorage* CsStorage::toRowStorage()
{
    if(accessType_ == _row) return this; //already a col storage
    return new RowCsStorage(nbRows_, nbCols_, scalarColIndices(1,1), stringId);
}

} // end of namespace xlifepp

