/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SymCsStorage.hpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Definition of the xlifepp::SymCsStorage class

  xlifepp::SymCsStorage class is the child class of xlifepp::CsStorage class dealing with Symmetric Compressed Row Storage for square matrix
  The diagonal is always first stored (even zero coefficients), then the lower triangular part in CSR
  NOTE: the first entry (say (1,1)) is stored at address 1 in values vector (not at address 0!)
  the matrix may not have symmetry, in that case the upper part is stored in CSC like CSR

  - colIndex_: vector of column index of non zero value ordered row by row (as values_ vector)
  - rowPointer_: vector of positions of beginning of rows in previous vector

  Example: The following 5 x 5 non symmetric matrix
  -------
  \verbatim
  0  d.x.x
  1  .dx..      (row            2   3    4    )
  2  xxdxx      colIndex_  = [ 0 1  2  0 2 3 ] (without diagonal coeffs)
  3  ..xdx     rowPointer_ = [ 0 0 0 2 3 6]    (last gives the number of non zeros of the lower part)
  4  x.xxd      (row           0 1 2 3 4      )
  \endverbatim

  Note: this is the true CSR, contrary to Daniel's choice
       : for an empty row, rowPointer_ is equal to the previous one: length of row i = rowPointer_[i+1]-rowPointer_[i]
       : storing positons
                      diag          lower        upper if required
                    0 ... n-1    n ... n+nnz   n+nnz+1 ... n+2*nnz+1

  Only _sym access type storage can be used for a matrix with a symmetry
  property ( _symmetric, _skewSymmetric, _selfAdjoint or _skewAdjoint)
*/

#ifndef SYM_CS_STORAGE_HPP
#define SYM_CS_STORAGE_HPP

#include "config.h"
#include "CsStorage.hpp"

namespace xlifepp
{

/*!
   \class SymCsStorage
   child class for row compressed storage of matrix with symmetrty
 */
class SymCsStorage : public CsStorage
{
  friend class CsStorage;

  protected:
    std::vector<number_t> colIndex_;   //!< vector of column index of non zero value
    std::vector<number_t> rowPointer_; //!< vector of positions of beginning of rows

  public:
    // constructor, destructor
    SymCsStorage(number_t n = 0, string_t id="SymCsStorage"); //!< default constructor
    ~SymCsStorage() {}                                    //!< destructor

   //! constructor by a pair of global numerotation vectors
    SymCsStorage(number_t, const std::vector< std::vector<number_t> >&, const std::vector< std::vector<number_t> >&, string_t id="SymCsStorage");
    //! constructor by the list of col index by rows
    template <typename L>
    SymCsStorage(number_t, const std::vector<L>&, MatrixPart, string_t id="SymCsStorage");
    SymCsStorage* clone() const                                   //! create a clone (virtual copy constructor, covariant)
    {return new SymCsStorage(*this);}
    SymCsStorage* toScalar(dimen_t, dimen_t);                         //!< create a new scalar SymCs storage from current SymCs storage and submatrix sizes
    virtual void clear()                                          //! clear storage vectors
    {rowPointer_.clear();colIndex_.clear();}

    //utilities
    number_t size() const {return nbRows_ + colIndex_.size();}      //!< number of non zero elements stored in lower triangular part
    number_t lowerPartSize() const {return colIndex_.size();}       //!< returns number of entries of lower triangular part
    number_t upperPartSize() const {return colIndex_.size();}       //!< returns number of entries of upper triangular part
    bool sameStorage(const MatrixStorage&) const;                   //!< check if two storages have the same structures

    //access operator
    std::set<number_t> colIndex(number_t r) const;                     //!< returns column indices of row r>0 (strict lower part)
    number_t pos(number_t i, number_t j, SymType s = _noSymmetry) const; //!< returns adress of entry (i,j)
    void positions(const std::vector<number_t>&, const std::vector<number_t>&, std::vector<number_t>&, bool errorOn = true,
                   SymType = _noSymmetry) const;                   //!< access to submatrix positions
    std::vector<std::pair<number_t, number_t> > getCol(number_t c, number_t r1=1, number_t r2=0) const; //!< get (row indices, adress) of col c in set [r1,r2]
    std::vector<std::pair<number_t, number_t> > getRow(number_t r, number_t c1=1, number_t c2=0) const; //!< get (col indices, adress) of row r in set [c1,c2]
    std::vector<std::pair<number_t, number_t> > getCol(SymType s, number_t c, number_t r1=1, number_t r2=0) const; //!< get (row indices, adress) of col c in set [r1,r2]
    std::vector<std::pair<number_t, number_t> > getRow(SymType s, number_t r, number_t c1=1, number_t c2=0) const; //!< get (col indices, adress) of row r in set [c1,c2]
    std::set<number_t> getCols(number_t r, number_t c1=1, number_t c2=0) const;                       //!< get col indices of row r in set [c1,c2]
    std::set<number_t> getRows(number_t c, number_t r1=1, number_t r2=0) const;                       //!< get row indices of col c in set [r1,r2]

    //modifying tools
    void addSubMatrixIndices(const std::vector<number_t>& ,const std::vector<number_t>& ); //!< add dense submatrix indices in storage
    void addRow(number_t, const std::set<number_t>&, MatrixPart);                          //!< add row-cols (r,c1), (r,c2), ... given by a cols set (r,c>=1)

    //skyline conversion tools
    std::vector<number_t> skylineRowPointer() const; //!< return skyline row pointer from current SymCsStorage pointers
    std::vector<number_t> skylineColPointer() const; //!< return skyline col pointer from current SymCsStorage pointers
    void fillSkylineValues(const std::vector<real_t>&, std::vector<real_t>&,SymType,MatrixStorage*) const;                         //!< fill real values of current storage as a skyline storage
    void fillSkylineValues(const std::vector<complex_t>&, std::vector<complex_t>&,SymType,MatrixStorage*) const;                   //!< fill complex values of current storage as a skyline storage
    void fillSkylineValues(const std::vector<Matrix<real_t> >&, std::vector<Matrix<real_t> >&,SymType,MatrixStorage*) const;       //!< fill values of current storage as a skyline storage
    void fillSkylineValues(const std::vector<Matrix<complex_t> >&, std::vector<Matrix<complex_t> >&,SymType,MatrixStorage*) const; //!< fill values of current storage as a skyline storage

    //print utilities
    void print(std::ostream&) const;                                   //!< print storage pointers
    void printEntries(std::ostream&, const std::vector<real_t>&,
                      number_t vb, const SymType sym) const;             //!< print real scalar matrix
    void printEntries(std::ostream&, const std::vector<complex_t>&,
                      number_t vb, const SymType sym) const;             //!< print complex scalar matrix
    void printEntries(std::ostream&, const std::vector<Matrix<real_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of real matrices
    void printEntries(std::ostream&, const std::vector<Matrix<complex_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of complex matrices
    void printEntries(std::ostream&, const std::vector<Vector<real_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of real vectors (not available)
    void printEntries(std::ostream&, const std::vector<Vector<complex_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of complex vectors (not available)
    void printCooMatrix(std::ostream&, const std::vector<real_t>&, SymType) const;              //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector<complex_t>&, SymType) const;           //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector< Matrix<real_t> >&, SymType) const;    //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector< Matrix<complex_t> >&, SymType) const; //!< output matrix of complex scalars in coordinate form

    void print(PrintStream& os) const
      {print(os.currentStream());}
    void printEntries(PrintStream& os, const std::vector<real_t>& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<complex_t>& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Matrix<real_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Matrix<complex_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Vector<real_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Vector<complex_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printCooMatrix(PrintStream& os, const std::vector<real_t>& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector<complex_t>& m,SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector< Matrix<real_t> >& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector< Matrix<complex_t> >& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}

    //load file utilities
    void loadFromFileDense(std::istream& ifs, std::vector<real_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileDense(ifs, mat, colIndex_, rowPointer_, colIndex_, rowPointer_, sym, rAsC);}
    void loadFromFileDense(std::istream& ifs, std::vector<complex_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileDense(ifs, mat, colIndex_, rowPointer_, colIndex_, rowPointer_, sym, rAsC);}
    void loadFromFileCoo(std::istream& ifs, std::vector<real_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileCoo(ifs, mat, colIndex_, rowPointer_, colIndex_, rowPointer_, sym, rAsC);}
    void loadFromFileCoo(std::istream& ifs, std::vector<complex_t>& mat, SymType sym, bool rAsC)
    {loadCsFromFileCoo(ifs, mat, colIndex_, rowPointer_, colIndex_, rowPointer_, sym, rAsC);}

    //template Matrix x Vector & Vector x Matrix multiplications and specializations (see below)
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&, SymType sym) const; //!< templated symcs Matrix x Vector
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, std::vector<R>&, SymType sym) const; //!< templated Vector x symcs Matrix
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>& m, V*, R*, SymType sym) const;       //!< templated symcs Matrix x Vector (pointer form)
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>& m, V*, R*, SymType sym) const;       //!< templated Vector x symcs Matrix (pointer form)

    //@{
    //! Matrix (Scalar) * Vector (Scalar)
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    //@}

    //@{
    //! Vector (Scalar) * Matrix (Scalar)
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    //@}

    //@{
    //! Matrix * Vector (pointer form)
    void multMatrixVector(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const         //! templated symcs Matrix x Vector (pointer)
    { multMatrixVector<>(m, vp, rp, sym); }
    void multMatrixVector(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const   //! templated symcs Matrix x Vector (pointer)
    { multMatrixVector<>(m, vp, rp, sym); }
    void multMatrixVector(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const   //! templated symcs Matrix x Vector (pointer)
    { multMatrixVector<>(m, vp, rp, sym); }
    void multMatrixVector(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const//! templated symcs Matrix x Vector (pointer)
    { multMatrixVector<>(m, vp, rp, sym); }
    //@}

    //! template Matrix + Matrix addition and specializations (see below)
    template<typename M1, typename M2, typename R>
    void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const; //!< templated Sym Cs Matrix + Matrix

    //@{
    //! Matrix+Matrix
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv) const
    { addMatrixMatrix<>(m, v, rv);}
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    //@}

    template<typename M>
    void ilu(std::vector<M>& m, std::vector<M>& fa) const;
void ilu(std::vector<real_t>& m, std::vector<real_t>& fa, const SymType sym = _noSymmetry) const
    {
        ilu<>(m, fa);
    }

    void ilu(std::vector<complex_t>& m, std::vector<complex_t>& fa, const SymType sym = _noSymmetry) const
    {
// #ifdef XLIFEPP_WITH_OMP
//         ildltParallel<>(m, fa);
// #else
        ilu<>(m, fa);
// #endif
    }


    //! Incomplete matrix factorization with ILDLt
    template<typename M>
    void ildlt(std::vector<M>& m, std::vector<M>& fa) const;

    template<typename M>
    void ildltParallel(std::vector<M>& m, std::vector<M>& fa) const;

    //@{
    //! specializations of template ILDLt
    void ildlt(std::vector<real_t>& m, std::vector<real_t>& fa, const SymType sym = _noSymmetry) const
    {
        ildlt<>(m, fa);
    }

    void ildlt(std::vector<complex_t>& m, std::vector<complex_t>& fa, const SymType sym = _noSymmetry) const
    {
// #ifdef XLIFEPP_WITH_OMP
//         ildltParallel<>(m, fa);
// #else
        ildlt<>(m, fa);
// #endif
    }
    //@}

    //! Incomplete matrix factorization with ILLt
    template<typename M>
    void illt(std::vector<M>& m, std::vector<M>& fa) const;

    template<typename M>
    void illtParallel(std::vector<M>& m, std::vector<M>& fa) const;

    //@{
    //! specializations of template ILLt
    void illt(std::vector<real_t>& m, std::vector<real_t>& fa, const SymType sym = _noSymmetry) const
    {
        illt<>(m, fa);
    }

    void illt(std::vector<complex_t>& m, std::vector<complex_t>& fa, const SymType sym = _noSymmetry) const
    {
// #ifdef XLIFEPP_WITH_OMP
//         ildltParallel<>(m, fa);
// #else
        illt<>(m, fa);
// #endif
    }
    //@}


    //! Incomplete matrix factorization with ILDLstar
    template<typename M>
    void ildlstar(std::vector<M>& m, std::vector<M>& fa) const;

    template<typename M>
    void ildlstarParallel(std::vector<M>& m, std::vector<M>& fa) const;

    //@{
    //! specializations of template iLDLstar
    void ildlstar(std::vector<real_t>& m, std::vector<real_t>& fa, const SymType sym = _noSymmetry) const
    {
        ildlstar<>(m, fa);
    }

    void ildlstar(std::vector<complex_t>& m, std::vector<complex_t>& fa, const SymType sym = _noSymmetry) const
    {
// #ifdef XLIFEPP_WITH_OMP
//         ildltParallel<>(m, fa);
// #else
        ildlstar<>(m, fa);
// #endif
    }
    //@}

    //! Incomplete matrix factorization with ILLstar
    template<typename M>
    void illstar(std::vector<M>& m, std::vector<M>& fa) const;

    template<typename M>
    void illstarParallel(std::vector<M>& m, std::vector<M>& fa) const;

    //@{
    //! specializations of template ILLstar
    void illstar(std::vector<real_t>& m, std::vector<real_t>& fa, const SymType sym = _selfAdjoint) const
    {
        illstar<>(m, fa);
    }

    void illstar(std::vector<complex_t>& m, std::vector<complex_t>& fa, const SymType sym = _selfAdjoint) const
    {
// #ifdef XLIFEPP_WITH_OMP
//         ildltParallel<>(m, fa);
// #else
        illstar<>(m, fa);
// #endif
    }
    //@}
/* ----------------------------------------------------------------------------------------------------------------------------
                                                     SOR and SSOR stuff
   ----------------------------------------------------------------------------------------------------------------------------*/
    /*! special template partial Matrix x Vector multiplications [w*D] v used in SSOR algorithm
	    for D + L + U matrix splitting (D diagonal, L & U strict lower & upper trangular parts) */
    template<typename M, typename V, typename R>
    void sorDiagonalMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, const real_t w) const
      { typename std::vector<M>::const_iterator it_m = m.begin() + 1;
		typename std::vector<V>::const_iterator it_vb = v.begin();
		typename std::vector<R>::iterator it_rb = r.begin();
		bzSorDiagonalMatrixVector(it_m, it_vb, it_rb, w);
      }

    //@{
    //! specializations of partial matrix std::vector multiplications
    void sorDiagonalMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, const real_t w) const
      { sorDiagonalMatrixVector<>(m, v, rv, w); }
    void sorDiagonalMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w) const
      { sorDiagonalMatrixVector<>(m, v, rv, w); }
    void sorDiagonalMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, const real_t w) const
      { sorDiagonalMatrixVector<>(m, v, rv, w); }
    void sorDiagonalMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w) const
      { sorDiagonalMatrixVector<>(m, v, rv, w); }
    //@}

    /*! special template partial Matrix x Vector multiplications [w*D+L] v used in SSOR algorithm
        for D + L + U matrix splitting (D diagonal, L & U strict lower & upper trangular parts) */
		template<typename M, typename V, typename R>
		void sorLowerMatrixVector(const std::vector<M>& m, const std::vector<V>& v,
		                          std::vector<R>& r, const real_t w, const SymType sym) const
		{
		  typename std::vector<M>::const_iterator it_m = m.begin() + 1;
		  typename std::vector<V>::const_iterator it_vb = v.begin();
		  typename std::vector<R>::iterator it_rb = r.begin();
		  bzSorDiagonalMatrixVector(it_m, it_vb, it_rb, w);  // it_m now equals it_l
		  lowerMatrixVector(colIndex_, rowPointer_, it_m, it_vb, it_rb, sym);
		}

    //@{
    //! specializations of partial matrix vector multiplications
    void sorLowerMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, const real_t w, const SymType sym) const
    { sorLowerMatrixVector<>(m, v, rv, w, sym); }
    void sorLowerMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorLowerMatrixVector<>(m, v, rv, w, sym); }
    void sorLowerMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorLowerMatrixVector<>(m, v, rv, w, sym); }
    void sorLowerMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorLowerMatrixVector<>(m, v, rv, w, sym); }
    //@}

    /*! special template partial Matrix x Vector multiplications [w*D+U] v used in SSOR algorithm
        for D + L + U matrix splitting (D diagonal, L & U strict lower & upper trangular parts) */
		template<typename M, typename V, typename R>
		void sorUpperMatrixVector(const std::vector<M>& m, const std::vector<V>& v,
		                          std::vector<R>& r, const real_t w, const SymType sym) const
		{
		  typename std::vector<M>::const_iterator it_m = m.begin() + 1;
		  typename std::vector<V>::const_iterator it_vb = v.begin();
		  typename std::vector<R>::iterator it_rb = r.begin();
		  bzSorDiagonalMatrixVector(it_m, it_vb, it_rb, w);       // it_m now equals it_l
		  if(sym == _noSymmetry) { it_m += (colIndex_).size(); }  // it_m = u_i
		  upperMatrixVector(colIndex_, rowPointer_, it_m, it_vb, it_rb, sym);
		}

    //@{
    //! specializations of partial matrix vector multiplications
    void sorUpperMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, const real_t w, const SymType sym) const
    { sorUpperMatrixVector<>(m, v, rv, w, sym); }
    void sorUpperMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorUpperMatrixVector<>(m, v, rv, w, sym); }
    void sorUpperMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorUpperMatrixVector<>(m, v, rv, w, sym); }
    void sorUpperMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorUpperMatrixVector<>(m, v, rv, w, sym); }
    //@}

    /*! special template diagonal and triangular solvers  D/w x = b used in SSOR algorithm
        for D + L + U matrix splitting (D diagonal, L & U strict lower & upper triangular parts) */
		template<typename M, typename V, typename R>
		void sorDiagonalSolver(const std::vector<M>& m, const std::vector<R>& b,
		                       std::vector<V>& x, const real_t w) const
		{
		  typename std::vector<M>::const_iterator it_m = m.begin() + 1;
		  typename std::vector<R>::const_iterator it_b = b.begin();
		  typename std::vector<V>::iterator it_xb = x.begin(), it_xe = x.end();
		  bzSorDiagonalSolver(it_m, it_b, it_xb, it_xe, w);
		}

    //@{
    //! specializations of diagonal solvers
    void sorDiagonalSolver(const std::vector<real_t>& m, const std::vector<real_t>& b, std::vector<real_t>& x, const real_t w) const
    { sorDiagonalSolver<>(m, b, x, w); }
    void sorDiagonalSolver(const std::vector<real_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorDiagonalSolver<>(m, b, x, w); }
    void sorDiagonalSolver(const std::vector<complex_t>& m, const std::vector<real_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorDiagonalSolver<>(m, b, x, w); }
    void sorDiagonalSolver(const std::vector<complex_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorDiagonalSolver<>(m, b, x, w); }
    //@}


   template<typename M, typename V, typename R>
    void diagonalSolver(const std::vector<M>& m,  std::vector<V>& v, std::vector<R>& x) const;
 //! Specializations of diagonal linear solvers D x = v */
    void diagonalSolver(const std::vector<real_t>& m,  std::vector<real_t>& v, std::vector<real_t>& x) const
    { diagonalSolver<>(m, v, x); }
    void diagonalSolver(const std::vector<real_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { diagonalSolver<>(m, v, x); }
    void diagonalSolver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x) const
    { diagonalSolver<>(m, v, x); }
    void diagonalSolver(const std::vector<complex_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { diagonalSolver<>(m, v, x); }
	  /*! special template diagonal and triangular solvers (D/w+L) x = b used in SSOR algorithm
          for D + L + U matrix splitting (D diagonal, L & U strict lower & upper triangular parts) */
		template<typename M, typename V, typename R>
		void sorLowerSolver(const std::vector<M>& m, const std::vector<R>& b,
		                                  std::vector<V>& x, const real_t w) const
		{
		  //typename std::vector<M>::const_iterator it_m = m.begin() + 1, it_l = it_m + colIndex_.size();  ??? ERIC
		  typename std::vector<M>::const_iterator it_m = m.begin() + 1, it_l = it_m + diagonalSize();
		  typename std::vector<R>::const_iterator it_b = b.begin();
		  typename std::vector<V>::iterator it_xb = x.begin(), it_xe = x.end();
		  bzSorLowerSolver(it_m, it_l, it_b, it_xb, it_xe, colIndex_, rowPointer_, w);
		}

    //@{
    //! specializations of lower triangular part solvers
    void sorLowerSolver(const std::vector<real_t>& m, const std::vector<real_t>& b, std::vector<real_t>& x, const real_t w) const
    { sorLowerSolver<>(m, b, x, w); }
    void sorLowerSolver(const std::vector<real_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorLowerSolver<>(m, b, x, w); }
    void sorLowerSolver(const std::vector<complex_t>& m, const std::vector<real_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorLowerSolver<>(m, b, x, w); }
    void sorLowerSolver(const std::vector<complex_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorLowerSolver<>(m, b, x, w); }
    //@}


//     /*!
//       Template  triangular part solvers (after factorization)
//       Lower triangular with unit diagonal linear system solver: (I+L) x = b
//     */
    template<typename M, typename V, typename X>
    void lowerD1Solver(const std::vector<M>& m,  std::vector<V>& v, std::vector<X>& x) const;
    //@{
    //! Specializations of lower triangular part with unit diagonal linear solvers (I + L) x = v */
    void lowerD1Solver(const std::vector<real_t>& m,  std::vector<real_t>& v, std::vector<real_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<real_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<complex_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    //@}


	  /*! special template diagonal and triangular solvers (D/w+U) x = b used in SSOR algorithm
          for D + L + U matrix splitting (D diagonal, L & U strict lower & upper triangular parts) */
		template<typename M, typename V, typename R>
		void sorUpperSolver(const std::vector<M>& m, const std::vector<R>& b,
		                                  std::vector<V>& x, const real_t w, const SymType sym) const
		{
		  // *** LO AND BEHOLD we use reverse_iterators here
		  typename std::vector<M>::const_reverse_iterator it_ru = m.rbegin(), it_rd = it_ru + colIndex_.size();
		  typename std::vector<R>::const_reverse_iterator it_rbb = b.rbegin();
		  typename std::vector<V>::reverse_iterator it_rxb = x.rbegin(), it_rxe = x.rend();
		  if(sym==_noSymmetry) it_rd = it_ru + 2*colIndex_.size(); // move back colIndex_.size() more because values= [diagonal, lower part, upper part]
          bzSorUpperSolver(it_rd, it_ru, it_rbb, it_rxb, it_rxe, colIndex_, rowPointer_, w, sym);
		}

    //@{
    //! specializations of upper triangular part solvers
    void sorUpperSolver(const std::vector<real_t>& m, const std::vector<real_t>& b, std::vector<real_t>& x, const real_t w, const SymType sym) const
    { sorUpperSolver<>(m, b, x, w, sym); }
    void sorUpperSolver(const std::vector<real_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w, const SymType sym) const
    { sorUpperSolver<>(m, b, x, w, sym); }
    void sorUpperSolver(const std::vector<complex_t>& m, const std::vector<real_t>& b, std::vector<complex_t>& x, const real_t w, const SymType sym) const
    { sorUpperSolver<>(m, b, x, w, sym); }
    void sorUpperSolver(const std::vector<complex_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w, const SymType sym) const
    { sorUpperSolver<>(m, b, x, w, sym); }
    //@}


 template<typename M, typename V, typename X>
    void upperSolver(const std::vector<M>& m,  std::vector<V>& v, std::vector<X>& x, SymType sym) const;
    //@{
    //! Specializations of lower triangular part with unit diagonal linear solvers (I + L) x = v */
    void upperSolver(const std::vector<real_t>& m,  std::vector<real_t>& v, std::vector<real_t>& x, SymType sym) const
    { upperSolver<>(m, v, x,  sym); }
    void upperSolver(const std::vector<real_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x, SymType sym) const
    { upperSolver<>(m, v, x,  sym); }
    void upperSolver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x, SymType sym) const
    { upperSolver<>(m, v, x,  sym); }
    void upperSolver(const std::vector<complex_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x, SymType sym) const
    { upperSolver<>(m, v, x,  sym); }
    //@}


template<typename M, typename V, typename X>
void upperD1Solver(const std::vector<M>& m,  std::vector<V>& b, std::vector<X>& x, SymType sym) const;
//@{
    //! Specializations of lower triangular part with unit diagonal linear solvers (I + L) x = v */
    void upperD1Solver(const std::vector<real_t>& m,  std::vector<real_t>& v, std::vector<real_t>& x, SymType sym) const
    { upperD1Solver<>(m, v, x,  sym); }
    void upperD1Solver(const std::vector<real_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x, SymType sym) const
    { upperSolver<>(m, v, x,  sym); }
    void upperD1Solver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x, SymType sym) const
    { upperD1Solver<>(m, v, x,  sym); }
    void upperD1Solver(const std::vector<complex_t>& m,  std::vector<complex_t>& v, std::vector<complex_t>& x, SymType sym) const
    { upperD1Solver<>(m, v, x,  sym); }
    //@}

/* ----------------------------------------------------------------------------------------------------------------------------
                                                     UMFPACK stuff
   ----------------------------------------------------------------------------------------------------------------------------*/

    //! conversion to umfpack format
    template<typename M, typename OrdinalType>
    void toUmfPack(const std::vector<M>& values, std::vector<OrdinalType>& colPointer, std::vector<OrdinalType>& rowIndex, std::vector<M>& mat, const SymType sym=_noSymmetry) const;

    //@{
    //! conversion to umfpack format (specializations)
    void toUmfPack(const std::vector<real_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<real_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat,sym);}
    void toUmfPack(const std::vector<complex_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<complex_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat,sym);}
    //@}
};

// constructor from the list of col index of non zero for each row
//   cols[k] of type L stores the indices of column of non zero element on row k (index starts at 1)
//   only requirement on L are the definition of forward const iterator named::const_iterator
//                             and the size
//   works for L = vector<number_t>, list<number_t>, set<number_t>
//   if mp==_lower it is assumed that only strict lower part indices are given
//   if mp!=_lower it is assumed that all indices are given, in that case lower part indices are extracted from (more expansive)
template <typename L>
SymCsStorage::SymCsStorage(number_t n, const std::vector<L>& cols, MatrixPart mp, string_t id)
   : CsStorage(n, n, _sym, id)
{
   trace_p->push("SymCsStorage constructor");
   if(mp==_lower)
   {
       buildCsStorage(cols, colIndex_, rowPointer_);  //construct rowIndex_ and colPointer_
       trace_p->pop();
       return;
    }

   //construct set of column index for strict lower part
   std::vector<std::vector<number_t> > lowup(nbRows_);
   typename std::vector<L>::const_iterator cit_vn;
   std::vector<std::vector<number_t> >::iterator it_vn = lowup.begin();
   typename L::const_iterator cit_s;
   number_t r = 1;
   std::vector<number_t>::iterator it;
   for(cit_vn = cols.begin(); cit_vn != cols.end(); ++cit_vn, ++it_vn, r++)
   {
      it_vn->resize(cit_vn->size());
      it = it_vn->begin();
      number_t l=0;
      for(cit_s = cit_vn->begin(); cit_s != cit_vn->end(); ++cit_s)
         if(*cit_s < r) { *it = *cit_s;  ++it;  l++; }
      it_vn->resize(l);
   }
   buildCsStorage(lowup, colIndex_, rowPointer_); //construct rowIndex_ and colPointer_
   trace_p->pop();
}

} // end of namespace xlifepp

#endif // SYM_CS_STORAGE

