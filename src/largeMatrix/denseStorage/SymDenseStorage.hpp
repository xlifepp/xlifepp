/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SymDenseStorage.hpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Definition of the xlifepp::SymDenseStorage class

  The xlifepp::SymDenseStorage class deals with dense storage of a symmetric matrix M (square matrix)
  storing main diagonal, then lower triangular part by rows
            diagonal: M11, M22...Mnn,
            lower triangular part: M21, M31, M32 ..., Mi1, ..., Mii-1, Mn1, ..., Mnn-1
  it inherits from xlifepp::DenseStorage
*/

#ifndef SYM_DENSE_STORAGE_HPP
#define SYM_DENSE_STORAGE_HPP

#include "config.h"
#include "DenseStorage.hpp"

namespace xlifepp
{

/*!
   \class SymDenseStorage
   handles dense storage of "symetric" matrix storing
   the lower "triangular" part row by row
   the diagonal is stored first, then the strict lower part
 */

class SymDenseStorage : public DenseStorage {
  public:

    // Constructors, Destructor
    SymDenseStorage(string_t id="SymmDenseStorage");         //!< default constructor
    SymDenseStorage(number_t, string_t id="SymmDenseStorage"); //!< constructor by access type, number of columns and rows (square matrix)
    virtual ~SymDenseStorage() {}                          //!< virtual destructor
    SymDenseStorage* clone() const                         //! create a clone (virtual copy constructor, covariant)
    {return new SymDenseStorage(*this);}
    SymDenseStorage* toScalar(dimen_t, dimen_t);               //!< create a new scalar SymDense storage from current SymDense storage and submatrix sizes

    // size utilities
    number_t lowerPartSize() const           //! returns number of matrix entries in lower triangular part of matrix
    { return (nbRows_ * (nbRows_ - 1)) / 2; }
    number_t upperPartSize() const           //! returns number of matrix entries in upper triangular part of matrix
    { return (nbRows_ * (nbRows_ - 1)) / 2; }
    number_t size() const                    //! returns number of stored entries of matrix
    { return (nbRows_ * (nbRows_ + 1)) / 2; }

    // access operator
    number_t pos(number_t i, number_t j, SymType s = _noSymmetry) const;          //!< overloaded pos returns adress of entry (i,j)
    void positions(const std::vector<number_t>&, const std::vector<number_t>&,
    std::vector<number_t>&, bool errorOn = true, SymType = _noSymmetry ) const;  //!< access to submatrix positions
    std::vector<std::pair<number_t, number_t> > getCol(SymType s, number_t c, number_t r1=1, number_t r2=0) const; //!< get (row indices, adress) of col c in set [r1,r2]
    std::vector<std::pair<number_t, number_t> > getRow(SymType s, number_t r, number_t c1=1, number_t c2=0) const; //!< get (col indices, adress) of row r in set [c1,c2]

    /* template specializations
       This rather COMPLICATED implementation of multMatrixVector is made
       to simulate "template virtual functions" in abstract base class MatrixStorage
       and thus to avoid "switch...case:" programming and "downcasting" in Matrix*Vector
       overloaded operator* of class LargeMatrix.
       Note that the call to template functions above is forced with the "<>" template
       descriptor in the following specialized virtual functions (this is not recursion) */

    //----------------------------------------------------------------------------------------------
    //                         set diagonal and Matrix + Matrix
    //----------------------------------------------------------------------------------------------
    //! Set value of Diagonal
    template<typename T>
    void setDiagValueSymDense(std::vector<T>& m, const T k);
    //@{
    //! sets values of diagonal matrix (specializations)
    void setDiagValue(std::vector<real_t>& m, const real_t k)
    { setDiagValueSymDense<>(m, k);}
    void setDiagValue(std::vector<complex_t>& m, const complex_t k)
    { setDiagValueSymDense<>(m, k);}
    //@}

    // template Matrix + Matrix addition and specializations (see below)
    template<typename M1, typename M2, typename R>
    void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const; //!< templated row dense Matrix + Matrix
    //@{
    //! Matrix + Matrix
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv) const
    { addMatrixMatrix<>(m, v, rv);}
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    //@}

    //-----------------------------------------------------------------------------------------------
    //                                     print stuff
    //-----------------------------------------------------------------------------------------------
    void printEntries(std::ostream&, const std::vector<real_t>&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of real scalars
    void printEntries(std::ostream&, const std::vector<complex_t>&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of complex scalars
    void printEntries(std::ostream&, const std::vector< Vector<real_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of real vectors
    void printEntries(std::ostream&, const std::vector< Vector<complex_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of complex vectors
    void printEntries(std::ostream&, const std::vector< Matrix<real_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of real matrices
    void printEntries(std::ostream&, const std::vector< Matrix<complex_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of complex matrices

    //----------------------------------------------------------------------------------------------
    //        template Matrix x Vector & Vector x Matrix multiplications and specializations
    //----------------------------------------------------------------------------------------------
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&, SymType) const; //!< templated sym dense Matrix x Vector
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, std::vector<R>&, SymType) const; //!< templated Vector x sym dense Matrix
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>&, V*, R*, SymType) const;                   //!< templated Vector x sym dense Matrix (pointer form)
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>&, V*, R*, SymType) const;                   //!< templated Vector x sym dense Matrix (pointer form)

    //@{
    //! Matrix (Scalar) * Vector (Scalar)
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    //@}

    //@{
    //! Vector (Scalar) * Matrix (Scalar)
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    //@}

    //@{
    //! Matrix * Vector (pointer form)
    void multMatrixVector(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp, sym); }
    void multMatrixVector(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp, sym); }
    void multMatrixVector(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp, sym); }
    void multMatrixVector(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp, sym); }
    //@}

    //@{
    //! Vector * Matrix (pointer form)
    void multVectorMatrix(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp, sym); }
    void multVectorMatrix(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp, sym); }
    void multVectorMatrix(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp, sym); }
    void multVectorMatrix(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp, sym); }
    //@}

    /*---------------------------------------------------------------------------------------------------------------------
                                       triangular part matrix * vector
       ------------------------------------------------------------------------------------------------------------------*/
    template<typename M, typename V, typename R>
    void lowerMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! lower Matrix (Scalar) * Vector (Scalar)
    virtual void lowerMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    //@}

    template<typename M, typename V, typename R>
    void lowerD1MatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! lower Matrix diag 1 (Scalar) * Vector (Scalar)
    virtual void lowerD1MatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    //@}

    template<typename M, typename V, typename R>
    void upperMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&, SymType) const;
    //@{
    //! upper Matrix (Scalar) * Vector (Scalar)
    virtual void upperMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv,s);}
    virtual void upperMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv,s);}
    virtual void upperMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv,s);}
    virtual void upperMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv,s);}
    //@}

    template<typename M, typename V, typename R>
    void upperD1MatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&, SymType s) const;
    //@{
    //! upper Matrix diag 1 (Scalar) * Vector (Scalar)
    virtual void upperD1MatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv,s);}
    virtual void upperD1MatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv,s);}
    virtual void upperD1MatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv,s);}
    virtual void upperD1MatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv,s);}
    //@}

    template<typename M, typename V, typename R>
    void diagonalMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! diag Matrix * Vector (Scalar)
    virtual void diagonalMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    //@}

    //----------------------------------------------------------------------------------------------
    //                             ldlt factorization stuff
    //----------------------------------------------------------------------------------------------
    // template ldlt
    template<typename T>
    void ldlt(std::vector<T>& A, std::vector<T>& LD) const;      //!< LDLt factorization with no pivoting (omp)
    //@{
    //! LDLt specializations
    void ldlt(std::vector<real_t>& A, std::vector<real_t>& LD, const SymType sym = _noSymmetry) const
    {ldlt<>(A, LD);}
    void ldlt(std::vector<complex_t>& A, std::vector<complex_t>& LD, const SymType sym = _noSymmetry) const
    {ldlt<>(A, LD);}
    //@}

    //! Diagonal linear system solver: D x = b
    template<typename M, typename V, typename X>
    void diagonalSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const;
    //@{
    //! Specializations of diagonal linear solvers D x = v
    void diagonalSolver(const std::vector<real_t>& m, std::vector<real_t>& v, std::vector<real_t>& x) const
    { diagonalSolver<>(m, v, x); }
    void diagonalSolver(const std::vector<real_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { diagonalSolver<>(m, v, x); }
    void diagonalSolver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x) const
    { diagonalSolver<>(m, v, x); }
    void diagonalSolver(const std::vector<complex_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { diagonalSolver<>(m, v, x); }
    //@}

    template<typename M, typename V, typename X>
    void lowerD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const;
    //@{
    //! Specializations of lower triangular part with unit diagonal linear solvers (I + L) x = v */
    void lowerD1Solver(const std::vector<real_t>& m, std::vector<real_t>& v, std::vector<real_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<real_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<complex_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    //@}

    //! Upper triangular with unit diagonal linear system solver: (I+U) x = b
    template<typename M, typename V, typename X>
    void upperD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x, const SymType sym = _noSymmetry) const;
    //@{
    //! Specializations of upper triangular part with unit diagonal linear solvers (I + U) x = v
    void upperD1Solver(const std::vector<real_t>& m, std::vector<real_t>& v, std::vector<real_t>& x, const SymType sym = _noSymmetry) const
    { upperD1Solver<>(m, v, x, sym); }
    void upperD1Solver(const std::vector<real_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperD1Solver<>(m, v, x, sym); }
    void upperD1Solver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperD1Solver<>(m, v, x, sym); }
    void upperD1Solver(const std::vector<complex_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperD1Solver<>(m, v, x, sym); }
    //@}

    //! upper triangular linear system solver: (D+U) x = b
    template<typename M, typename V, typename X>
    void upperSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x, const SymType sym = _noSymmetry) const;
    //@{
    //! specializations of upper triangular part linear solvers (D + U) x = v
    void upperSolver(const std::vector<real_t>& m, std::vector<real_t>& v, std::vector<real_t>& x, const SymType sym = _noSymmetry) const
    { upperSolver<>(m, v, x, sym); }
    void upperSolver(const std::vector<real_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperSolver<>(m, v, x, sym); }
    void upperSolver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperSolver<>(m, v, x, sym); }
    void upperSolver(const std::vector<complex_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperSolver<>(m, v, x, sym); }
    //@}

    //----------------------------------------------------------------------------------------------
    //                                         UMFpack stuff
    //----------------------------------------------------------------------------------------------
    //! conversion to umfpack format
    template<typename M, typename OrdinalType>
    void toUmfPack(const std::vector<M>& values, std::vector<OrdinalType>& colPointer, std::vector<OrdinalType>& rowIndex, std::vector<M>& mat, const SymType sym = _noSymmetry) const;
    //@{
    //! specializations od umfpack conversion
    void toUmfPack(const std::vector<real_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<real_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat, sym);}
    void toUmfPack(const std::vector<complex_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<complex_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat, sym);}
    //@}
};

} // end of namespace xlifepp

#endif // SYM_DENSE_STORAGE_HPP

