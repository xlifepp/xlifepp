/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file RowDenseStorage.hpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Definition of the xlifepp::RowDenseStorage class

  The xlifepp::RowDenseStorage class deals with dense storage of a matrix M stored row by row:
            M11, ..., M1n, M21, ..., M2n, ... , Mm1,..., Mmn
  it inherits from xlifepp::DenseStorage
*/

#ifndef ROW_DENSE_STORAGE_HPP
#define ROW_DENSE_STORAGE_HPP

#include "config.h"
#include "DenseStorage.hpp"

namespace xlifepp
{

/*!
   \class RowDenseStorage
   handles dense storage of matrix stored row by row
 */

class RowDenseStorage : public DenseStorage
{
  public:

    // Constructors, Destructor
    RowDenseStorage(string_t id="RowDenseStorage");                     //!< default constructor
    RowDenseStorage(number_t, string_t id="RowDenseStorage");           //!< constructor by access type, number of columns and rows (square matrix)
    RowDenseStorage(number_t, number_t, string_t id="RowDenseStorage"); //!< constructor by access type, number of columns and rows
    // RowDenseStorage(const SymDenseStorage&);   //!< constructor from a sym dense storage
    // RowDenseStorage(const DualDenseStorage&);  //!< constructor from a dual dense storage
    virtual ~RowDenseStorage() {}                                  //!< virtual destructor
    RowDenseStorage* clone() const                                 //! create a clone (virtual copy constructor, covariant)
    {return new RowDenseStorage(*this);}
    RowDenseStorage* toScalar(dimen_t, dimen_t);                       //!< create a new scalar RowDense storage from current RowDense storage and submatrix sizes

    // access operator
    number_t pos(number_t i, number_t j, SymType s = _noSymmetry) const; //!< overloaded pos returns adress of entry (i,j)
    number_t pos_(number_t i, number_t j, SymType s = _noSymmetry) const //! overloaded pos fast returns adress of entry (i,j)
    { return ((i - 1) * nbCols_ + j); }
    void positions(const std::vector<number_t>&, const std::vector<number_t>&,
                   std::vector<number_t>&, bool errorOn = true, SymType = _noSymmetry) const;   //!< access to submatrix positions
    std::vector<std::pair<number_t, number_t> > getCol(SymType s, number_t c, number_t r1=1, number_t r2=0) const; //!< get (row indices, adress) of col c in set [r1,r2]
    std::vector<std::pair<number_t, number_t> > getRow(SymType s, number_t r, number_t c1=1, number_t c2=0) const; //!< get (col indices, adress) of row r in set [c1,c2]


    /* template specializations
       This rather COMPLICATED implementation of multMatrixVector is made
       to simulate "template virtual functions" in abstract base class MatrixStorage
       and thus to avoid "switch...case:" programming and "downcasting" in Matrix*Vector
       overloaded operator* of class LargeMatrix.
       Note that the call to template functions above is forced with the "<>" template
       descriptor in the following specialized virtual functions (this is not recursion) */

    //----------------------------------------------------------------------------------------------------------------
    //                                     print stuff
    //----------------------------------------------------------------------------------------------------------------
    void printEntries(std::ostream&, const std::vector<real_t>&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of real scalars
    void printEntries(std::ostream&, const std::vector<complex_t>&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of complex scalars
    void printEntries(std::ostream&, const std::vector< Vector<real_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of real vectors
    void printEntries(std::ostream&, const std::vector< Vector<complex_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of complex vectors
    void printEntries(std::ostream&, const std::vector< Matrix<real_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of real matrices
    void printEntries(std::ostream&, const std::vector< Matrix<complex_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of complex matrices

    //----------------------------------------------------------------------------------------------------------------
    //                                     basic algebra stuff
    //----------------------------------------------------------------------------------------------------------------
    //! Set value of Diagonal
    template<typename T>
    void setDiagValueRowDense(std::vector<T>& m, const T k);

    void setDiagValue(std::vector<real_t>& m, const real_t k)
    { setDiagValueRowDense<>(m, k);}
    void setDiagValue(std::vector<complex_t>& m, const complex_t k)
    { setDiagValueRowDense<>(m, k);}

    //! template Matrix + Matrix addition and specializations (see below)
    template<typename M1, typename M2, typename R>
    void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const; //!< templated row dense Matrix + Matrix
    //@{
    //! Matrix + Matrix
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv) const
    { addMatrixMatrix<>(m, v, rv);}
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    //@}
    //@{
    //! transpose matrix
    template<typename T> MatrixStorage* transpose(const std::vector<T>& m, std::vector<T>& mt) const;
    MatrixStorage* transpose(const std::vector<real_t>& m, std::vector<real_t>& mt) const
    { return transpose<>(m, mt); }
    MatrixStorage* transpose(const std::vector<complex_t>& m, std::vector<complex_t>& mt) const
    { return transpose<>(m, mt); }
    MatrixStorage* transpose(const std::vector<Matrix<real_t> >& m, std::vector<Matrix<real_t> >& mt) const
    { return transpose<>(m, mt); }
    MatrixStorage* transpose(const std::vector<Matrix<complex_t> >& m, std::vector<Matrix<complex_t> >& mt) const
    { return transpose<>(m, mt); }
    //@}

    //----------------------------------------------------------------------------------------------------------------
    //                                     matrix * vector stuff
    //----------------------------------------------------------------------------------------------------------------
    //template Matrix x Vector & Vector x Matrix multiplications and specializations (see below)
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< templated row dense Matrix x Vector
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< templated Vector x row dense Matrix
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>& m, V* vp, R* rp) const;           //!< templated row dense Matrix x Vector (pointer form)
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>& m, V* vp, R* rp) const;           //!< templated row dense Vector x Matrix (pointer form)

    // parallel Vector * Matrix
    template<typename M, typename V, typename R>
    void parallelMultVectorMatrix(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;

    //@{
    //! Matrix (Scalar) * Vector (Scalar)
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    //@}

    //@{
    //! Matrix * Vector (pointer form)
    void multMatrixVector(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    //@}

    //@{
    //! Vector * Matrix (pointer form)
    void multVectorMatrix(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    //@}

    //@{
    //! Vector (Scalar) * Matrix (Scalar)
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType) const
    {
      #ifdef XLIFEPP_WITH_OMP
        parallelMultVectorMatrix<>(m, v, rv);
      #else
        multVectorMatrix<>(m, v, rv);
      #endif // XLIFEPP_WITH_OMP
    }
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    {
      #ifdef XLIFEPP_WITH_OMP
        parallelMultVectorMatrix<>(m, v, rv);
      #else
        multVectorMatrix<>(m, v, rv);
      #endif // XLIFEPP_WITH_OMP
    }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType) const
    {
      #ifdef XLIFEPP_WITH_OMP
        parallelMultVectorMatrix<>(m, v, rv);
      #else
        multVectorMatrix<>(m, v, rv);
      #endif // XLIFEPP_WITH_OMP
    }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    {
      #ifdef XLIFEPP_WITH_OMP
        parallelMultVectorMatrix<>(m, v, rv);
      #else
        multVectorMatrix<>(m, v, rv);
      #endif // XLIFEPP_WITH_OMP
    }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    {
      #ifdef XLIFEPP_WITH_OMP
        parallelMultVectorMatrix<>(m, v, rv);
      #else
        multVectorMatrix<>(m, v, rv);
      #endif // XLIFEPP_WITH_OMP
    }
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    {
      #ifdef XLIFEPP_WITH_OMP
        parallelMultVectorMatrix<>(m, v, rv);
      #else
        multVectorMatrix<>(m, v, rv);
      #endif // XLIFEPP_WITH_OMP
    }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    {
      #ifdef XLIFEPP_WITH_OMP
        parallelMultVectorMatrix<>(m, v, rv);
      #else
        multVectorMatrix<>(m, v, rv);
      #endif // XLIFEPP_WITH_OMP
    }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    {
      #ifdef XLIFEPP_WITH_OMP
        parallelMultVectorMatrix<>(m, v, rv);
      #else
        multVectorMatrix<>(m, v, rv);
      #endif // XLIFEPP_WITH_OMP
    }
    //@}

     /*----------------------------------------------------------------------------------------------------------------------------
                                       virtual triangular part matrix * vector
       ----------------------------------------------------------------------------------------------------------------------------*/
    template<typename M, typename V, typename R>
    void lowerMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! lower Matrix (Scalar) * Vector (Scalar)
    virtual void lowerMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void lowerD1MatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! lower Matrix diag 1 (Scalar) * Vector (Scalar)
    virtual void lowerD1MatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void upperMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! upper Matrix (Scalar) * Vector (Scalar)
    virtual void upperMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv);}
    virtual void upperMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv);}
    virtual void upperMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv);}
    virtual void upperMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void upperD1MatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! upper Matrix diag 1 (Scalar) * Vector (Scalar)
   virtual void upperD1MatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv);}
    virtual void upperD1MatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv);}
    virtual void upperD1MatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv);}
    virtual void upperD1MatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void diagonalMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! diag Matrix * Vector (Scalar)
    virtual void diagonalMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    //@}

    //----------------------------------------------------------------------------------------------------------------
    //                                     direct solver stuff
    //----------------------------------------------------------------------------------------------------------------
    template<typename T>
    void gaussSolver(std::vector<T>& A, std::vector<std::vector<T> >& b) const;   //!< Gauss elimination with pivoting (omp); n rhs
    template<typename T>
    void gaussSolver(std::vector<T>& A, std::vector<T>& b) const;                 //!< Gauss elimination with pivoting (omp); 1 rhs
    template<typename T>
    void lu(std::vector<T>& A, std::vector<T>& LU, std::vector<number_t>& p) const;//!< LU factorization with row pivoting (omp)
    template<typename T>
    void lu(std::vector<T>& A, std::vector<T>& LU) const;                          //!< LU factorization with no pivoting (omp)

    //specialization
    virtual void gaussSolver(std::vector<real_t>& A, std::vector<std::vector<real_t> >& bs) const
    {gaussSolver<>(A,bs);}
    virtual void gaussSolver(std::vector<complex_t>& A, std::vector<std::vector<complex_t> >& bs) const
    {gaussSolver<>(A,bs);}
    virtual void gaussSolver(std::vector<real_t>& A, std::vector<real_t>& b) const
    {gaussSolver<>(A,b);}
    virtual void gaussSolver(std::vector<complex_t>& A, std::vector<complex_t>& b) const
    {gaussSolver<>(A,b);}
    void lu(std::vector<real_t>& A, std::vector<real_t>& LU, std::vector<number_t>& p, const SymType sym = _noSymmetry) const
    {lu<>(A, LU, p);}
    void lu(std::vector<complex_t>& A, std::vector<complex_t>& LU, std::vector<number_t>& p, const SymType sym = _noSymmetry) const
    {lu<>(A, LU, p);}
    void lu(std::vector<real_t>& A, std::vector<real_t>& LU, const SymType sym = _noSymmetry) const
    {lu<>(A, LU);}
    void lu(std::vector<complex_t>& A, std::vector<complex_t>& LU, const SymType sym = _noSymmetry) const
    {lu<>(A, LU);}

    /*!
      Template diagonal and triangular part solvers (after factorization)
      Lower triangular with unit diagonal linear system solver: (I+L) x = b
    */
    template<typename M, typename V, typename X>
    void lowerD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const;
    //@{
    //! Specializations of lower triangular part with unit diagonal linear solvers (I + L) x = v */
    void lowerD1Solver(const std::vector<real_t>& m, std::vector<real_t>& v, std::vector<real_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<real_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<complex_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    //@}

    //! Diagonal linear system solver: D x = b
    template<typename M, typename V, typename X>
    void diagonalSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const;
    //@{
    //! Specializations of diagonal linear solvers D x = v
    void diagonalSolver(const std::vector<real_t>& m, std::vector<real_t>& v, std::vector<real_t>& x) const
    { diagonalSolver<>(m, v, x); }
    void diagonalSolver(const std::vector<real_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { diagonalSolver<>(m, v, x); }
    void diagonalSolver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x) const
    { diagonalSolver<>(m, v, x); }
    void diagonalSolver(const std::vector<complex_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { diagonalSolver<>(m, v, x); }
    //@}

    //! Upper triangular with unit diagonal linear system solver: (I+U) x = b
    template<typename M, typename V, typename X>
    void upperD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const;
    //@{
    //! Specializations of upper triangular part with unit diagonal linear solvers (I + U) x = v
    void upperD1Solver(const std::vector<real_t>& m, std::vector<real_t>& v, std::vector<real_t>& x, const SymType sym = _noSymmetry) const
    { upperD1Solver<>(m, v, x); }
    void upperD1Solver(const std::vector<real_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperD1Solver<>(m, v, x); }
    void upperD1Solver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperD1Solver<>(m, v, x); }
    void upperD1Solver(const std::vector<complex_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperD1Solver<>(m, v, x); }
    //@}

    //! upper triangular linear system solver: (D+U) x = b
    template<typename M, typename V, typename X>
    void upperSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const;
    //@{
    //! specializations of upper triangular part linear solvers (D + U) x = v
    void upperSolver(const std::vector<real_t>& m, std::vector<real_t>& v, std::vector<real_t>& x, const SymType sym) const
    { upperSolver<>(m, v, x); }
    void upperSolver(const std::vector<real_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x, const SymType) const
    { upperSolver<>(m, v, x); }
    void upperSolver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x, const SymType sym) const
    { upperSolver<>(m, v, x); }
    void upperSolver(const std::vector<complex_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x, const SymType sym) const
    { upperSolver<>(m, v, x); }
    //@}

    //! conversion to umfpack
    template<typename M, typename OrdinalType>
    void toUmfPack(const std::vector<M>& values, std::vector<OrdinalType>& colPointer, std::vector<OrdinalType>& rowIndex, std::vector<M>& mat) const;
    //@{
    //! specializations of umfpack conversion
    void toUmfPack(const std::vector<real_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<real_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat);}
    void toUmfPack(const std::vector<complex_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<complex_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat);}
    //@}
};


//create the transposed storage in row dense
template<typename T>
MatrixStorage* RowDenseStorage::transpose(const std::vector<T>& m, std::vector<T>& mt) const
{
  RowDenseStorage* stot = new RowDenseStorage(nbCols_, nbRows_);
  mt.resize(nbCols_*nbRows_+1, 0.*m[0]);
  typename std::vector<T>::const_iterator itm=m.begin()+1;
  for(number_t i=0; i<nbRows_; ++i)
    for(number_t j=0; j<nbCols_; ++j, ++itm)
      {
        mt[j*nbRows_+i+1]=*itm;
      }
  return stot;
}

/* -------------------------------------------------------------------------------------------------------
                                    Gauss solver with pivoting strategy
   -------------------------------------------------------------------------------------------------------*/
/* Gauss solver with virtual row pivoting strategy  A*x1=b1, A*x2=b2, ...
   A: square matrix stored as row dense
   b: right hand sides and solutions
*/
template<typename T>
void RowDenseStorage::gaussSolver(std::vector<T>& A, std::vector<std::vector<T> >& b) const
{
  std::vector<number_t> P(nbRows_);  //store indices and addresses of pivoting row i
  std::vector<number_t>::iterator itp=P.begin();
  for(number_t i=0; i<nbRows_; i++,++itp) *itp=i;
  typename std::vector<std::vector<T> >::iterator itb, itx;
  typename std::vector<T>::iterator itri, itrk, itAb=A.begin()+1;
  T piv, v;
  real_t s,t;
  number_t q;
  bool show_status = (nbRows_ > 1000  && theVerboseLevel>0);
  if(show_status) std::cout<<"   in row dense Gauss solver, "<<numberOfThreads()<<" threads: "<<std::flush;

  //Gauss elimination
  for(number_t k=0; k<nbRows_-1; k++) //main Gauss loop
    {
      number_t pk=P[k];
      itrk = itAb+pk*nbCols_+k;    //Akk
      s=std::abs(*itrk);
      number_t l=k;
      for(number_t j=k+1; j<nbRows_; j++)
        {
          t=std::abs(*(itAb+P[j]*nbCols_+k));
          if(t > s) {l=j; s=t;}
        }
      if(s<theTolerance) error("mat_noinvert");
      if(l!=k)  //swap rows k and l
        {
          q=P[l]; P[l]=P[k]; P[k]=q; pk=q;
        }
      piv=*(itAb+pk*nbCols_+k);

      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for firstprivate(v, itb, itri, itrk)  schedule(dynamic)
      #endif // XLIFEPP_WITH_OMP
      for(number_t i=k+1; i<nbRows_; i++)
        {
          number_t pi=P[i];
          itri=itAb+pi*nbCols_+k;
          v=*itri/piv;
          *itri++=0;
          itrk=itAb+pk*nbCols_+k+1;
          for(number_t j=k+1; j<nbRows_; j++, ++itri, ++itrk)  *itri-=v* *itrk;
          for(itb=b.begin(); itb!=b.end(); itb++)(*itb)[pi]-=v*(*itb)[pk];
        }  //end loop i

      if(show_status && nbRows_>10) //progress status
        {
          number_t  p = k % (nbRows_/10);
          if(p == 0) std::cout<<(k/(nbRows_/10))<<"0% "<<std::flush;
        }
    }//end loop k

  //solve triangular system
  for(int_t i=nbRows_-1; i>=0; i--)
    {
      number_t pi=P[i], pj;
      itri=itAb+pi*nbCols_+i+1;
      for(number_t j=i+1; j<nbRows_; j++, ++itri)
        {
          v=*itri;
          pj=P[j];
          for(itb=b.begin(); itb!=b.end(); itb++)(*itb)[pi]-=v*(*itb)[pj];
        }
      itri=itAb+pi*nbCols_+i;
      v=*itri;
      for(itb=b.begin(); itb!=b.end(); itb++)(*itb)[pi]/=v;
    }

  //apply inverse permutation to rhs's
  typename std::vector<T>::iterator itv;
  for(itb=b.begin(); itb!=b.end(); itb++)
    {
      std::vector<T> bc=*itb;   //copy B
      itp=P.begin();
      for(itv=itb->begin(); itv!=itb->end(); ++itv, ++itp) *itv = bc[*itp];
    }

  if(show_status) std::cout<<"done "<<eol<<std::flush; //progress status
}

// generic Gauss solver with partial pivoting strategy  A*x=b, call multiple rhs function
template<typename T>
void RowDenseStorage::gaussSolver(std::vector<T>& A, std::vector<T>& b) const
{
  std::vector<std::vector<T> > bs(1,b);
  gaussSolver<>(A,bs);
  b=bs[0];
}

/* -------------------------------------------------------------------------------------------------------
                                    LU factorization with row pivoting strategy
   -------------------------------------------------------------------------------------------------------*/
/*! LU factorization with real row pivoting
    LU is a row dense matrix where its rows have been permuted according to the row permutation vector (LU=PA)
    be care: solving A*X=B is equivalent to solving P*A*X=P*B <=> L*U*X=PB, the product by P has to be done before calling triangular part solvers!
              product B=A*X is equivalent to product B=inv(P)*L*U*X, the product by inv(P) has to be done after calling triangular part products!
              product B=X*A is equivalent to product B=X*inv(P)*L*U=tr(tr(U)*tr(L)*P*tr(X)), the product by P has to be done before calling triangular part products!
              this products are not taken into account by storage class !
*/
template<typename T>
void RowDenseStorage::lu(std::vector<T>& A, std::vector<T>& LU, std::vector<number_t>& P) const
{
  if(&A!=&LU) LU=A;   //recopy A to LU to work only with LU matrix
  P.resize(nbRows_);
  std::vector<number_t>::iterator itp=P.begin();
  for(number_t i=0; i<nbRows_; i++, ++itp) *itp=i;
  typename std::vector<T>::iterator itri, itrk, itlub = LU.begin()+1;
  number_t q;
  T piv, v;
  real_t s,t;
  bool show_status = (nbRows_ > 1000  && theVerboseLevel>0);
  if(show_status) std::cout<<"   in row dense LU factorization  with row permutation, "<<numberOfThreads()<<" threads: "<<std::flush;

  for(number_t k=0; k<nbRows_-1; k++) //main Gauss loop
    {
      s=std::abs(LU[k*nbCols_+k+1]);
      number_t l=k;
      for(number_t i=k+1; i<nbRows_; i++)
        {
          t=std::abs(LU[i*nbCols_+k+1]);
          if(t > s) {l=i; s=t;}
        }
      if(s<theTolerance) error("mat_noinvert");
      if(l!=k)  //swap rows k and l
        {
          q=P[l]; P[l]=P[k]; P[k]=q;                        //virtual swap
          itrk=itlub+k*nbCols_;
          itri=itlub+l*nbCols_;
          for(number_t j=0; j<nbCols_; j++, itrk++, itri++) //real swap
          {
              v=*itri; *itri=*itrk; *itrk=v;
          }
        }
      piv=LU[k*nbCols_+k+1];
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for firstprivate(q, v, itri, itrk) schedule(dynamic)
      #endif // XLIFEPP_WITH_OMP
      for(number_t i=k+1; i<nbRows_; i++)
        {
          itri=itlub+i*nbCols_+k;
          v=*itri/=piv;
          itri++;
          itrk=itlub+k*nbCols_+k+1;
          for(number_t j=k+1; j<nbCols_; j++, ++itri, ++itrk)  *itri-=v* *itrk;
        }  //end loop i

      if(show_status && nbRows_>10) //progress status
        {
          number_t  p = k % (nbRows_/10);
          if(p == 0) std::cout<<(k/(nbRows_/10))<<"0% "<<std::flush;
        }
    }//end loop k
}

/* -------------------------------------------------------------------------------------------------------
                                    LU factorization with no pivoting strategy
   -------------------------------------------------------------------------------------------------------*/
//! LU factorization with no pivoting
template<typename T>
void RowDenseStorage::lu(std::vector<T>& A, std::vector<T>& LU) const
{
  if (&A!=&LU) LU=A;   //copy A
  typename std::vector<T>::iterator itri, itrk, itlub = LU.begin()+1;
  T piv, v;
  bool show_status = (nbRows_ > 1000  && theVerboseLevel>0);
  if (show_status) std::cout<<"   in row dense LU factorization with no permutation, "<<numberOfThreads()<<" threads: "<<std::flush;
  for (number_t k=0; k<nbRows_-1; k++) //main Gauss loop
  {
    piv=LU[k*nbCols_+k+1];
    if (std::abs(piv)<theTolerance)
    {
      where("RowDenseStorage::lu");
      error("small_pivot");
    }
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for firstprivate(v, itri, itrk) schedule(dynamic)
    #endif // XLIFEPP_WITH_OMP
    for (number_t i=k+1; i<nbRows_; i++)
    {
      itri=itlub+i*nbCols_+k;
      v=*itri/=piv;
      itri++;
      itrk=itlub+k*nbCols_+k+1;
      for (number_t j=k+1; j<nbCols_; j++, ++itri, ++itrk)  *itri-=v* *itrk;
    }  //end loop i

    if (show_status && nbRows_>10) //progress status
    {
      number_t  p = k % (nbRows_/10);
      if (p == 0) std::cout<<(k/(nbRows_/10))<<"0% "<<std::flush;
    }
  }//end loop k
}

/* -------------------------------------------------------------------------------------------------------
                                   lower and upper solvers
   -------------------------------------------------------------------------------------------------------*/
template<typename M, typename V, typename X>
void RowDenseStorage::lowerD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const
{
    typename std::vector<M>::const_iterator itmb = m.begin()+1, itm;   //values start at 1
    typename std::vector<V>::iterator itv=v.begin();
    typename std::vector<X>::iterator itxb=x.begin(), itx;
    number_t n=x.size();
    for(number_t k=0;k<n;++k, ++itv, itmb+=n)
    {
        X t=*itv;
        itx=itxb;
        itm=itmb;
        for(number_t j=0;j<k;++j, ++itm, ++itx)
        {
            t -= *itm * *itx;
        }
        *itx = t;
    }
}

template<typename M, typename V, typename X>
void RowDenseStorage::upperSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const
{
    number_t n=x.size();
    typename std::vector<M>::const_reverse_iterator itmb = m.rbegin()++, itm;
    typename std::vector<V>::reverse_iterator itv=v.rbegin()++;
    typename std::vector<X>::reverse_iterator itxb=x.rbegin()++, itx;
    for(number_t k=0;k<n;++k, ++itv, itmb+=n)
    {
        X t=*itv;
        itx=itxb;
        itm=itmb;
        for(number_t j=0;j<k;++j, ++itm, ++itx)
        {
           t -= *itm * *itx;
        }
        *itx = t / *itm;
    }
}

template<typename M, typename V, typename X>
void RowDenseStorage::upperD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const
{
    number_t n=x.size();
    typename std::vector<M>::const_reverse_iterator itmb = m.rbegin()++, itm;
    typename std::vector<V>::reverse_iterator itv=v.rbegin()++;
    typename std::vector<X>::reverse_iterator itxb=x.rbegin()++, itx;
    for(number_t k=0;k<n;++k, ++itv, itmb+=n)
    {
        X t=*itv;
        itx=itxb;
        itm=itmb;
        for(number_t j=0;j<k;++j, ++itm, ++itx)
        {
           t -= *itm * *itx;
        }
        *itx = t;
    }
}

template<typename M, typename V, typename X>
void RowDenseStorage::diagonalSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const
{
    typename std::vector<M>::const_iterator itm = m.begin()+1;   //values start at 1
    typename std::vector<V>::iterator itv=v.begin();
    typename std::vector<X>::iterator itx=x.begin();
    number_t n=x.size();
    for(number_t k=0;k<n;++k, ++itv, ++itx, itm+=n)
    {
        *itx = *itv / *itm;
    }
}

} // end of namespace xlifepp

#endif // ROW_DENSE_STORAGE_HPP
