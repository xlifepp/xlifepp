/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DualDenseStorage.hpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Definition of the xlifepp::DualDenseStorage class

  The xlifepp::DualDenseStorage class deals with dense storage of a matrix M
  storing main diagonal, then lower triangular part by rows and upper triangular part by columns
            diagonal: M11, M22...Mpp,   with p=min(m,n)
            lower triangular part: M21, M31, M32 ..., Mi1, ..., Mii-1, Mm1, ..., Mmn-1
            upper triangular part: M12, M13, M23 ..., M1j, ..., Mj-1j, M1n, ..., Mm-1n
  it inherits from xlifepp::DenseStorage
*/

#ifndef DUAL_DENSE_STORAGE_HPP
#define DUAL_DENSE_STORAGE_HPP

#include "config.h"
#include "DenseStorage.hpp"

namespace xlifepp
{

/*!
   \class DualDenseStorage
   handles dense storage of matrix stored
   row by row for the lower "triangular" part and column by column for the upper "triangular"
 */

class DualDenseStorage : public DenseStorage
{
  public:
    // Constructors, Destructor
    DualDenseStorage(string_t id="DualDenseStorage");                 //!< default constructor
    DualDenseStorage(number_t, string_t id="DualDenseStorage");         //!< constructor by access type, number of columns and rows (square matrix)
    DualDenseStorage(number_t, number_t, string_t id="DualDenseStorage"); //!< constructor by access type, number of columns and rows
    virtual ~DualDenseStorage() {}                                  //!< virtual destructor
    DualDenseStorage* clone() const                                 //! create a clone (virtual copy constructor, covariant)
    {return new DualDenseStorage(*this);}
    DualDenseStorage* toScalar(dimen_t, dimen_t);                      //!< create a new scalar DualDense storage from current DualDense storage and submatrix sizes


    // size utilities
    number_t lowerPartSize() const;      //!< returns number of matrix entries in lower triangular part of matrix
    number_t upperPartSize() const;      //!< returns number of matrix entries in upper triangular part of matrix

    // access operator
    number_t pos(number_t i, number_t j, SymType s = _noSymmetry) const;    //!< overloaded pos returns adress of entry (i,j)
    void positions(const std::vector<number_t>&, const std::vector<number_t>&,
                   std::vector<number_t>&, bool errorOn = true, SymType = _noSymmetry) const;  //!< access to submatrix positions
    std::vector<std::pair<number_t, number_t> > getCol(SymType s, number_t c, number_t r1=1, number_t r2=0) const; //!< get (row indices, adress) of col c in set [r1,r2]
    std::vector<std::pair<number_t, number_t> > getRow(SymType s, number_t r, number_t c1=1, number_t c2=0) const; //!< get (col indices, adress) of row r in set [c1,c2]


    /* template specializations
       This rather COMPLICATED implementation of multMatrixVector is made
       to simulate "template virtual functions" in abstract base class MatrixStorage
       and thus to avoid "switch...case:" programming and "downcasting" in Matrix*Vector
       overloaded operator* of class LargeMatrix.
       Note that the call to template functions above is forced with the "<>" template
       descriptor in the following specialized virtual functions (this is not recursion) */

    /*------------------------------------------------------------------------------------------------------------------
                                        input and output stuff
      ------------------------------------------------------------------------------------------------------------------*/
    void printEntries(std::ostream&, const std::vector<real_t>&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output dual dense matrix of real scalars
    void printEntries(std::ostream&, const std::vector<complex_t>&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output dual dense matrix of complex scalars
    void printEntries(std::ostream&, const std::vector< Vector<real_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output dual dense matrix of real vectors
    void printEntries(std::ostream&, const std::vector< Vector<complex_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output dual dense matrix of complex vectors
    void printEntries(std::ostream&, const std::vector< Matrix<real_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output dual dense matrix of real matrices
    void printEntries(std::ostream&, const std::vector< Matrix<complex_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output dual dense matrix of complex matrices

    /*------------------------------------------------------------------------------------------------------------------
                                         basic algebra stuff
      ------------------------------------------------------------------------------------------------------------------*/
    //! Set value of Diagonal
    template<typename T>
    void setDiagValueDualDense(std::vector<T>& m, const T k);

    //@{
    //! set value of diagonal
    void setDiagValue(std::vector<real_t>& m, const real_t k)
    { setDiagValueDualDense<>(m, k);}
    void setDiagValue(std::vector<complex_t>& m, const complex_t k)
    { setDiagValueDualDense<>(m, k);}
    //@}

    // template Matrix + Matrix addition and specializations (see below)
    template<typename M1, typename M2, typename R>
    void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const; //!< templated row dense Matrix + Matrix

    //@{
    //! Matrix+Matrix
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv) const
    { addMatrixMatrix<>(m, v, rv);}
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    //@}


    /*------------------------------------------------------------------------------------------------------------------
             template Matrix x Vector & Vector x Matrix multiplications and specializations
      ------------------------------------------------------------------------------------------------------------------*/
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< templated row dense Matrix x Vector
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< templated Vector x row dense Matrix
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>& m, V* vp, R* rp) const;               //!< templated dual dense Matrix x Vector (pointer)
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>& m, V* vp, R* rp) const;               //!< templated Vector x dual dense Matrix (pointer)

    //@{
    //! Matrix (Scalar) * Vector (Scalar)
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv); }
    //@}

    //@{
    //! Vector (Scalar) * Matrix (Scalar)
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    //@}

    //@{
    //! Matrix * Vector (pointer form)
    void multMatrixVector(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    //@}

    //@{
    //! Vector * Matrix (pointer form)
    void multVectorMatrix(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    //@}

    /*------------------------------------------------------------------------------------------------------------------
                                       triangular part matrix * vector
     ------------------------------------------------------------------------------------------------------------------*/
    template<typename M, typename V, typename R>
    void lowerMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! lower Matrix (Scalar) * Vector (Scalar)
    virtual void lowerMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void lowerD1MatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! lower Matrix diag 1 (Scalar) * Vector (Scalar)
    virtual void lowerD1MatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void upperMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! upper Matrix (Scalar) * Vector (Scalar)
    virtual void upperMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv);}
    virtual void upperMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv);}
    virtual void upperMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv);}
    virtual void upperMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void upperD1MatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! upper Matrix diag 1 (Scalar) * Vector (Scalar)
   virtual void upperD1MatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv);}
    virtual void upperD1MatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv);}
    virtual void upperD1MatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv);}
    virtual void upperD1MatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void diagonalMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! diag Matrix * Vector (Scalar)
    virtual void diagonalMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    //@}

    /* -------------------------------------------------------------------------------------------------------------------------
                                                     SOR and SSOR stuff
    ----------------------------------------------------------------------------------------------------------------------------*/
    /*! special template partial Matrix x Vector multiplications [w*D] v used in SSOR algorithm
      for D + L + U matrix splitting (D diagonal, L & U strict lower & upper trangular parts) */
    template<typename M, typename V, typename R>
    void sorDiagonalMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, const real_t w) const
    {
      typename std::vector<M>::const_iterator it_m = m.begin() + 1;
      typename std::vector<V>::const_iterator it_vb = v.begin();
      typename std::vector<R>::iterator it_rb = r.begin(), it_re=r.end();
      bzSorDiagonalMatrixVector(it_m, it_vb, it_rb, it_re, w);
    }
    //@{
    //! specializations of partial matrix std::vector multiplications
    void sorDiagonalMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, const real_t w) const
    { sorDiagonalMatrixVector<>(m, v, rv, w); }
    void sorDiagonalMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w) const
    { sorDiagonalMatrixVector<>(m, v, rv, w); }
    void sorDiagonalMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, const real_t w) const
    { sorDiagonalMatrixVector<>(m, v, rv, w); }
    void sorDiagonalMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w) const
    { sorDiagonalMatrixVector<>(m, v, rv, w); }
    //@}

    /*! special template partial Matrix x Vector multiplications [w*D+L] v used in SSOR algorithm
        for D + L + U matrix splitting (D diagonal, L & U strict lower & upper trangular parts) */
    template<typename M, typename V, typename R>
    void sorLowerMatrixVector(const std::vector<M>& m, const std::vector<V>& v,
                              std::vector<R>& r, const real_t w, const SymType sym) const
    {
      typename std::vector<M>::const_iterator it_m = m.begin() + 1, it_l=it_m+diagonalSize();
      typename std::vector<V>::const_iterator it_vb = v.begin(), it_ve=v.end();
      typename std::vector<R>::iterator it_rb = r.begin(), it_re=r.end();
      bzSorDiagonalMatrixVector(it_m, it_vb, it_rb, it_re, w);
      DenseStorage::lowerMatrixVector(it_l, it_vb, it_ve, it_rb, it_re, sym);
    }
    //@{
    //! specializations of partial matrix vector multiplications
    void sorLowerMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, const real_t w, const SymType sym) const
    { sorLowerMatrixVector<>(m, v, rv, w, sym); }
    void sorLowerMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorLowerMatrixVector<>(m, v, rv, w, sym); }
    void sorLowerMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorLowerMatrixVector<>(m, v, rv, w, sym); }
    void sorLowerMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorLowerMatrixVector<>(m, v, rv, w, sym); }
    //@}

    /*! special template partial Matrix x Vector multiplications [w*D+U] v used in SSOR algorithm
        for D + L + U matrix splitting (D diagonal, L & U strict lower & upper trangular parts) */
    template<typename M, typename V, typename R>
    void sorUpperMatrixVector(const std::vector<M>& m, const std::vector<V>& v,
                              std::vector<R>& r, const real_t w, const SymType sym) const
    {
      typename std::vector<M>::const_iterator it_m = m.begin() + 1, it_u=it_m+diagonalSize()+lowerPartSize();
      typename std::vector<V>::const_iterator it_vb = v.begin(), it_ve=v.end();
      typename std::vector<R>::iterator it_rb = r.begin(), it_re=r.end();
      bzSorDiagonalMatrixVector(it_m, it_vb, it_rb, it_re, w);
      DenseStorage::upperMatrixVector(it_u, it_vb, it_ve, it_rb, it_re, sym);
    }
    //@{
    //! specializations of partial matrix vector multiplications
    void sorUpperMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, const real_t w, const SymType sym) const
    { sorUpperMatrixVector<>(m, v, rv, w, sym); }
    void sorUpperMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorUpperMatrixVector<>(m, v, rv, w, sym); }
    void sorUpperMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorUpperMatrixVector<>(m, v, rv, w, sym); }
    void sorUpperMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, const real_t w, const SymType sym) const
    { sorUpperMatrixVector<>(m, v, rv, w, sym); }
    //@}

    /*! special template diagonal and triangular solvers  D/w x = b used in SSOR algorithm
        for D + L + U matrix splitting (D diagonal, L & U strict lower & upper triangular parts) */
    template<typename M, typename V, typename R>
    void sorDiagonalSolver(const std::vector<M>& m, const std::vector<R>& b,
                           std::vector<V>& x, const real_t w) const
    {
      typename std::vector<M>::const_iterator it_m = m.begin() + 1;
      typename std::vector<R>::const_iterator it_b = b.begin();
      typename std::vector<V>::iterator it_xb = x.begin(), it_xe = x.end();
      bzSorDiagonalSolver(it_m, it_b, it_xb, it_xe, w);
    }
    //@{
    //! specializations of diagonal solvers
    void sorDiagonalSolver(const std::vector<real_t>& m, const std::vector<real_t>& b, std::vector<real_t>& x, const real_t w) const
    { sorDiagonalSolver<>(m, b, x, w); }
    void sorDiagonalSolver(const std::vector<real_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorDiagonalSolver<>(m, b, x, w); }
    void sorDiagonalSolver(const std::vector<complex_t>& m, const std::vector<real_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorDiagonalSolver<>(m, b, x, w); }
    void sorDiagonalSolver(const std::vector<complex_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorDiagonalSolver<>(m, b, x, w); }
    //@}

    /*! special template diagonal and triangular solvers (D/w+L) x = b used in SSOR algorithm
          for D + L + U matrix splitting (D diagonal, L & U strict lower & upper triangular parts) */
    template<typename M, typename V, typename R>
    void sorLowerSolver(const std::vector<M>& m, const std::vector<R>& b,
                        std::vector<V>& x, const real_t w) const
    {
      typename std::vector<M>::const_iterator it_d = m.begin() + 1, it_l = it_d + diagonalSize();
      typename std::vector<R>::const_iterator it_b = b.begin();
      typename std::vector<V>::iterator it_xb = x.begin(), it_xe = x.end();
      bzSorLowerSolver(it_d, it_l, it_b, it_xb, it_xe, w);
    }
    //@{
    //! specializations of lower triangular part solvers
    void sorLowerSolver(const std::vector<real_t>& m, const std::vector<real_t>& b, std::vector<real_t>& x, const real_t w) const
    { sorLowerSolver<>(m, b, x, w); }
    void sorLowerSolver(const std::vector<real_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorLowerSolver<>(m, b, x, w); }
    void sorLowerSolver(const std::vector<complex_t>& m, const std::vector<real_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorLowerSolver<>(m, b, x, w); }
    void sorLowerSolver(const std::vector<complex_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w) const
    { sorLowerSolver<>(m, b, x, w); }
    //@}

    /*! special template diagonal and triangular solvers (D/w+U) x = b used in SSOR algorithm
          for D + L + U matrix splitting (D diagonal, L & U strict lower & upper triangular parts) */
    template<typename M, typename V, typename R>
    void sorUpperSolver(const std::vector<M>& m, const std::vector<R>& b,
                        std::vector<V>& x, const real_t w, const SymType sym) const
    {
      // *** LO AND BEHOLD we use reverse_iterators here
      typename std::vector<M>::const_reverse_iterator it_ru = m.rbegin(), it_rd = it_ru + upperPartSize() + lowerPartSize();
      typename std::vector<R>::const_reverse_iterator it_rbb = b.rbegin();
      typename std::vector<V>::reverse_iterator it_rxb = x.rbegin(), it_rxe = x.rend();
      bzSorUpperSolver(it_rd, it_ru, it_rbb, it_rxb, it_rxe, w);
    }
    //@{
    //! specializations of upper triangular part solvers
    void sorUpperSolver(const std::vector<real_t>& m, const std::vector<real_t>& b, std::vector<real_t>& x, const real_t w, const SymType sym) const
    { sorUpperSolver<>(m, b, x, w, sym); }
    void sorUpperSolver(const std::vector<real_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w, const SymType sym) const
    { sorUpperSolver<>(m, b, x, w, sym); }
    void sorUpperSolver(const std::vector<complex_t>& m, const std::vector<real_t>& b, std::vector<complex_t>& x, const real_t w, const SymType sym) const
    { sorUpperSolver<>(m, b, x, w, sym); }
    void sorUpperSolver(const std::vector<complex_t>& m, const std::vector<complex_t>& b, std::vector<complex_t>& x, const real_t w, const SymType sym) const
    { sorUpperSolver<>(m, b, x, w, sym); }
    //@}

    /* ----------------------------------------------------------------------------------------------------------------------------
                                                         UMFPACK stuff
       ----------------------------------------------------------------------------------------------------------------------------*/
    //! conversion to umfpack format
    template<typename M, typename OrdinalType>
    void toUmfPack(const std::vector<M>& values, std::vector<OrdinalType>& colPointer, std::vector<OrdinalType>& rowIndex, std::vector<M>& mat) const;
    //@{
    //! specialization of umfpack conversion
    void toUmfPack(const std::vector<real_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<real_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat);}
    void toUmfPack(const std::vector<complex_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<complex_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat);}
    //@}
};

} // end of namespace xlifepp

#endif // DUAL_DENSE_STORAGE_HPP
