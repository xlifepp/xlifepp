/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ColDenseStorage.hpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Definition of the xlifepp::ColDenseStorage class

  The xlifepp::ColDenseStorage class deals with dense storage of a matrix M stored column by column:
             M11, ..., Mm1, M12, ..., Mm2, ... , M1n,..., Mmn
  it inherits from xlifepp::DenseStorage
*/

#ifndef COL_DENSE_STORAGE_HPP
#define COL_DENSE_STORAGE_HPP

#include "config.h"
#include "DenseStorage.hpp"

namespace xlifepp
{

/*!
   \class ColDenseStorage
   handles dense storage of matrix stored column by column
 */
class ColDenseStorage : public DenseStorage
{
  public:

    // Constructors, Destructor
    ColDenseStorage(string_t id="ColDenseStorage");                 //!< default constructor
    ColDenseStorage(number_t, string_t id="ColDenseStorage");         //!< constructor by access type, number of columns and rows (square matrix)
    ColDenseStorage(number_t, number_t, string_t id="ColDenseStorage"); //!< constructor by access type, number of columns and rows
    // ColDenseStorage(const SymmDenseStorage&);   //!< constructor from a sym dense storage
    // ColDenseStorage(const DualDenseStorage&);   //!< constructor from a dual dense storage
    virtual ~ColDenseStorage() {}                                  //!< virtual destructor
    ColDenseStorage* clone() const                                 //! create a clone (virtual copy constructor, covariant)
    {return new ColDenseStorage(*this);}
    ColDenseStorage* toScalar(dimen_t, dimen_t);                  //!< create a new scalar ColDense storage from current ColDense storage and submatrix sizes

    // access operator
    number_t pos(number_t i, number_t j, SymType s = _noSymmetry) const; //!< overloaded pos returns adress of entry (i,j)
    number_t pos_(number_t i, number_t j, SymType s = _noSymmetry) const //! overloaded pos fast returns adress of entry (i,j)
     { return ((j - 1) * nbRows_ + i); }
    void positions(const std::vector<number_t>&, const std::vector<number_t>&,
                   std::vector<number_t>&, bool errorOn = true, SymType = _noSymmetry) const; //!< access to submatrix positions
    std::vector<std::pair<number_t, number_t> > getCol(SymType s, number_t c, number_t r1=1, number_t r2=0) const; //!< get (row indices, adress) of col c in set [r1,r2]
    std::vector<std::pair<number_t, number_t> > getRow(SymType s, number_t r, number_t c1=1, number_t c2=0) const; //!< get (col indices, adress) of row r in set [c1,c2]

       /* template specializations
       This rather COMPLICATED implementation of multMatrixVector is made
       to simulate "template virtual functions" in abstract base class MatrixStorage
       and thus to avoid "switch...case:" programming and "downcasting" in Matrix*Vector
       overloaded operator* of class LargeMatrix.
       Note that the call to template functions above is forced with the "<>" template
       descriptor in the following specialized virtual functions (this is not recursion) */

    //----------------------------------------------------------------------------------------------------------------
    //                                     print stuff
    //----------------------------------------------------------------------------------------------------------------
    void printEntries(std::ostream&, const std::vector<real_t>&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of real scalars
    void printEntries(std::ostream&, const std::vector<complex_t>&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of complex scalars
    void printEntries(std::ostream&, const std::vector< Vector<real_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of real vectors
    void printEntries(std::ostream&, const std::vector< Vector<complex_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of complex vectors
    void printEntries(std::ostream&, const std::vector< Matrix<real_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of real matrices
    void printEntries(std::ostream&, const std::vector< Matrix<complex_t> >&,
                      number_t vb = 0, const SymType sym = _noSymmetry) const;    //!< output row dense matrix of complex matrices

    //----------------------------------------------------------------------------------------------------------------
    //                                     basic algebra stuff
    //----------------------------------------------------------------------------------------------------------------
     //! Set value of Diagonal
    template<typename T>
    void setDiagValueColDense(std::vector<T>& m, const T k);

    //@{
    //! set value of diagonal
    void setDiagValue(std::vector<real_t>& m, const real_t k)
    { setDiagValueColDense<>(m, k);}
    void setDiagValue(std::vector<complex_t>& m, const complex_t k)
    { setDiagValueColDense<>(m, k);}
    //@}

    // template Matrix + Matrix addition and specializations (see below)
    template<typename M1, typename M2, typename R>
    void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const; //!< templated row dense Matrix + Matrix

    //@{
    //! Matrix+Matrix
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv) const
    { addMatrixMatrix<>(m, v, rv);}
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    //@}
    //@{
    //! transpose matrix
    template<typename T> MatrixStorage* transpose(const std::vector<T>& m, std::vector<T>& mt) const;
    MatrixStorage* transpose(const std::vector<real_t>& m, std::vector<real_t>& mt) const
    { return transpose<>(m, mt); }
    MatrixStorage* transpose(const std::vector<complex_t>& m, std::vector<complex_t>& mt) const
    { return transpose<>(m, mt); }
    MatrixStorage* transpose(const std::vector<Matrix<real_t> >& m, std::vector<Matrix<real_t> >& mt) const
    { return transpose<>(m, mt); }
    MatrixStorage* transpose(const std::vector<Matrix<complex_t> >& m, std::vector<Matrix<complex_t> >& mt) const
    { return transpose<>(m, mt); }
    //@}

    //----------------------------------------------------------------------------------------------------------------
    //                                  matrix * vector  and vector * mat stuff
    //----------------------------------------------------------------------------------------------------------------
    // template Matrix x Vector & Vector x Matrix multiplications and specializations (see below)
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< templated col dense Matrix x Vector
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< templated Vector x col dense Matrix
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>& m, V* vp, R* rp) const;           //!< templated col dense Matrix x Vector (pointer form)
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>& m, V* vp, R* rp) const;           //!< templated Vector x col dense Matrix (pointer form)

    /* template specializations
       This rather COMPLICATED implementation of multMatrixVector is made
       to simulate "template virtual functions" in abstract base class MatrixStorage
       and thus to avoid "switch...case:" programming and "downcasting" in Matrix*Vector
       overloaded operator* of class LargeMatrix.
       Note that the call to template functions above is forced with the "<>" template
       descriptor in the following specialized virtual functions (this is not recursion) */

    //@{
    //! Matrix (Scalar) * Vector (Scalar)
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType) const
    { multMatrixVector<>(m, v, rv);}
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    {multMatrixVector<>(m, v, rv);}
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType) const
    {multMatrixVector<>(m, v, rv);}
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    {multMatrixVector<>(m, v, rv);}
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    {multMatrixVector<>(m, v, rv);}
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    {multMatrixVector<>(m, v, rv);}
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    {multMatrixVector<>(m, v, rv);}
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    {multMatrixVector<>(m, v, rv);}
    //@}

    //@{
    //! Matrix * Vector (pointer form)
    void multMatrixVector(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    void multMatrixVector(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp); }
    //@}

    //@{
    //! Vector * Matrix (pointer form)
    void multVectorMatrix(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    void multVectorMatrix(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp); }
    //@}

    //@{
    //! Vector (Scalar) * Matrix (Scalar)
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType) const
    { multVectorMatrix<>(m, v, rv); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv); }
    //@}

    // parallel Matrix * Vector
    template<typename M, typename V, typename R>
    void parallelMultMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const; //!< parallel templated col dense Matrix x Vector

    /*---------------------------------------------------------------------------------------------------------------------
                                       triangular part matrix * vector
       ------------------------------------------------------------------------------------------------------------------*/
    template<typename M, typename V, typename R>
    void lowerMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! lower Matrix (Scalar) * Vector (Scalar)
    virtual void lowerMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void lowerD1MatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! lower Matrix diag 1 (Scalar) * Vector (Scalar)
    virtual void lowerD1MatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void upperMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! upper Matrix (Scalar) * Vector (Scalar)
    virtual void upperMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv);}
    virtual void upperMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv);}
    virtual void upperMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv);}
    virtual void upperMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void upperD1MatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! upper Matrix diag 1 (Scalar) * Vector (Scalar)
   virtual void upperD1MatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv);}
    virtual void upperD1MatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv);}
    virtual void upperD1MatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv);}
    virtual void upperD1MatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void diagonalMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! diag Matrix * Vector (Scalar)
    virtual void diagonalMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    //@}

/* ----------------------------------------------------------------------------------------------------------------------------
                                                         UMFPACK stuff
   ----------------------------------------------------------------------------------------------------------------------------*/
    //! conversion to umfpack format
    template<typename M, typename OrdinalType>
    void toUmfPack(const std::vector<M>& values, std::vector<OrdinalType>& colPointer, std::vector<OrdinalType>& rowIndex, std::vector<M>& mat) const;

    //@{
    //! specialization of umfpack conversion
    void toUmfPack(const std::vector<real_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<real_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat);}
    void toUmfPack(const std::vector<complex_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<complex_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat);}
    //@}
};

//create the transposed storage in col dense
template<typename T>
MatrixStorage* ColDenseStorage::transpose(const std::vector<T>& m, std::vector<T>& mt) const
{
  ColDenseStorage* stot = new ColDenseStorage(nbCols_, nbRows_);
  mt.resize(nbCols_*nbRows_+1, 0.*m[0]);
  typename std::vector<T>::const_iterator itm=m.begin()+1;
  for(number_t j=0; j<nbCols_; ++j)
    for(number_t i=0; i<nbRows_; ++i, ++itm)
      {
        mt[i*nbCols_+j+1]=*itm;
      }
  return stot;
}

} // end of namespace xlifepp

#endif // COL_DENSE_STORAGE_HPP
