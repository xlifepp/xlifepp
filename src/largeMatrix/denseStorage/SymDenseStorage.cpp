/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SymDenseStorage.cpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 3 apr 2013

  \brief Implementation of xlifepp::SymDenseStorage class and functionnalities
*/

#include "SymDenseStorage.hpp"
#include <functional>

namespace xlifepp
{
/*=======================================================================================
  Constructors, destructor
  =======================================================================================*/
//default constructor
SymDenseStorage::SymDenseStorage(string_t id)
  : DenseStorage(_sym, id) {}

// constructor for square structure (number of rows)
SymDenseStorage::SymDenseStorage(number_t nr, string_t id)
  : DenseStorage(_sym, nr, id) {}

/*--------------------------------------------------------------------------------
 access operator to position of (i,j) coefficients (return 0 if outside)
 This DIRECT access to (i,j) pointer SHOULD NOT be used in matrix computation
 where SEQUENTIAL access MUST ALWAYS be used and is only provided for tests.
--------------------------------------------------------------------------------*/
number_t SymDenseStorage::pos(number_t i, number_t j, SymType s) const
{

  if (i == 0 || i > nbRows_ || j == 0 || j > nbRows_) { return 0; }
  if (i == j) { return i; }
  if (i > j)  { return (nbRows_ + ((i - 2) * (i - 1)) / 2 + j); }
  if(s != _noSymmetry)   { return (nbRows_ + ((j - 2) * (j - 1)) / 2 + i); }
  return (nbRows_ + lowerPartSize() + ((j - 2) * (j - 1)) / 2 + i);
}

/* access to submatrix positions, useful for matrix assembling
   return the vector of adresses (relative number in storage) stored by row
   when sym!=_noSymmetry only adresses of the lower part is returned
*/
void SymDenseStorage::positions(const std::vector<number_t>& rows, const std::vector<number_t>& cols,
                                std::vector<number_t>& adrs, bool errorOn, SymType sym) const
{
  number_t nba = rows.size() * cols.size();
  if(adrs.size() != nba) { adrs.resize(nba, 0); }    // reset size
  std::vector<number_t>::iterator itp = adrs.begin();
  std::vector<number_t>::const_iterator itr, itc, itb, ite;

  for(itr = rows.begin(); itr != rows.end(); itr++)  // loop on row index
  {
    for(itc = cols.begin(); itc != cols.end(); itc++, itp++) // loop on col indices
    {
      *itp = pos(*itr, *itc, sym);
      if(*itp == 0 && errorOn) { error("storage_outofstorage", "SymDense", *itr, *itc); }
    }
  }
}

// get (row indices, adress) of col c in set [r1,r2]
//   SymType != noSymmetry means that the upper part is not stored
//              so an upper part coefficient address is the address of its symmetric coefficient in lower part
//   SymType == noSymmetry  means that the upper part is stored after diagonal part and lower part
std::vector<std::pair<number_t, number_t> > SymDenseStorage::getCol(SymType s,number_t c, number_t r1, number_t r2) const
{
    number_t nbr=r2;
    if(nbr==0) nbr=nbRows_;
    std::vector<std::pair<number_t, number_t> > rows(nbr-r1+1);
    std::vector<std::pair<number_t, number_t> >::iterator itrow=rows.begin();
    //retry rows in upper part, same as cols of row c
    number_t a= nbRows_ + ((c - 2) * (c - 1)) / 2 + r1;
    if(s==_noSymmetry) a+=lowerPartSize();
    number_t rmax=std::min(c,nbr+1);
    for(number_t i=r1;i<rmax; i++, a++, itrow++) *itrow=std::make_pair(i,a);
    //retry diag
    if(c>=r1 && c<=nbr) *itrow++=std::make_pair(c,c);
    //retry rows in lower part
    for(number_t i=c+1;i<=nbr;i++, itrow++)
        *itrow=std::make_pair(i,nbRows_ + ((i - 2) * (i - 1)) / 2 + c);
    return rows;
}

// get (col indices, adress) of row r in set [c1,c2]
//   SymType != noSymmetry means that the upper part is not stored
//              so an upper part coefficient address is the address of its symmetric coefficient in lower part
//   SymType == noSymmetry  means that the upper part is stored after diagonal part and lower part
std::vector<std::pair<number_t, number_t> > SymDenseStorage::getRow(SymType s, number_t r, number_t c1, number_t c2) const
{
    number_t nbc=c2;
    if(nbc==0) nbc=nbCols_;
    std::vector<std::pair<number_t, number_t> > cols(nbc-c1+1);
    std::vector<std::pair<number_t, number_t> >::iterator itcol=cols.begin();
    //retry cols in lower part
    number_t a= nbRows_ + ((r - 2) * (r - 1)) / 2 + c1;
    number_t cmax=std::min(r,nbc+1);
    for(number_t j=c1;j<cmax; j++, a++, itcol++) *itcol=std::make_pair(j,a);
    //retry diag
    if(r>=c1 && r<=nbc) *itcol++=std::make_pair(r,r);
    //retry cols in upper part
    a=nbRows_;
    if(s==_noSymmetry) a+=lowerPartSize();
    for(number_t j=r+1;j<=nbc;j++, itcol++)
        *itcol=std::make_pair(j, a + ((j - 2) * (j - 1)) / 2 + r);
    return cols;
}

// create a new scalar SymDense storage from current SymDense storage and submatrix sizes
SymDenseStorage* SymDenseStorage::toScalar(dimen_t nbr, dimen_t nbc)
{
    MatrixStorage* nsto=findMatrixStorage(stringId, _dense, _sym, buildType_, true, nbr*nbRows_, nbc*nbCols_);
    if(nsto!=nullptr) return static_cast<SymDenseStorage*>(nsto);
    SymDenseStorage* sd = new SymDenseStorage(nbr*nbRows_, stringId);
    sd->buildType() = buildType_;
    sd->scalarFlag()=true;
    return sd;
}

/*
--------------------------------------------------------------------------------
 print SymmDense matrix to default print file or ostream
--------------------------------------------------------------------------------
*/
void SymDenseStorage::printEntries(std::ostream& os, const std::vector<real_t>& m, number_t vb, SymType sym) const
{
  number_t nbDiag = nbRows_, nbLow = nbRows_ * (nbRows_ - 1) / 2;
  std::vector<real_t>::const_iterator itm = m.begin() + 1, itl = itm + nbDiag;
  os << eol << words("lower triangular part") << " (" << nbDiag + nbLow << " " << words("entries") << ",";
  printScalarEntriesTriangularPart(itm, itl, nbRows_, nbCols_, entriesPerRow, entryWidth, entryPrec,
                                   "row", vb, os);
  if(sym == _noSymmetry)
  {
    itm = m.begin() + 1;
    itl = itm + nbDiag;
    os << eol << words("upper triangular part") << " (" << nbDiag + nbLow << " " << words("entries") << ",";
    printScalarEntriesTriangularPart(itm, itl, nbCols_, nbRows_, entriesPerRow, entryWidth, entryPrec,
                                     "col", vb, os);
  }
}

void SymDenseStorage::printEntries(std::ostream& os, const std::vector<complex_t>& m, number_t vb, SymType sym) const
{
  number_t nbDiag = nbRows_, nbLow = nbRows_ * (nbRows_ - 1) / 2;
  std::vector<complex_t>::const_iterator itm = m.begin() + 1, itl = itm + nbDiag;
  os << eol << words("lower triangular part") << " (" << nbDiag + nbLow << " " << words("entries") << ",";
  printScalarEntriesTriangularPart(itm, itl, nbRows_, nbCols_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec,
                                   "row", vb, os);
  if(sym == _noSymmetry)
  {
    itm = m.begin() + 1;
    itl = itm + nbDiag;
    os << eol << words("upper triangular part") << " (" << nbDiag + nbLow << " " << words("entries") << ",";
    printScalarEntriesTriangularPart(itm, itl, nbCols_, nbRows_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec,
                                     "col", vb, os);
  }
}

void SymDenseStorage::printEntries(std::ostream& os, const std::vector< Vector<real_t> >& m, number_t vb, SymType sym) const
{error("storage_novector", "SymDenseStorage::printEntries");}

void SymDenseStorage::printEntries(std::ostream& os, const std::vector< Vector<complex_t> >& m, number_t vb, SymType sym) const
{error("storage_novector", "SymDenseStorage::printEntries");}

void SymDenseStorage::printEntries(std::ostream& os, const std::vector< Matrix<real_t> >& m, number_t vb, SymType sym) const
{
  number_t nbDiag = nbRows_, nbLow = nbRows_ * (nbRows_ - 1) / 2;
  std::vector< Matrix<real_t> >::const_iterator itm = m.begin() + 1, itl = itm + nbDiag;
  os << eol << words("lower triangular part") << " (" << nbDiag + nbLow << " " << words("entries") << ",";
  printMatrixEntriesTriangularPart(itm, itl, nbRows_, nbCols_, "row", vb, os);
  if(sym == _noSymmetry)
  {
    itm = m.begin() + 1;
    itl = itm + nbDiag;
    os << eol << words("upper triangular part") << " (" << nbDiag + nbLow << " " << words("entries") << ",";
    printMatrixEntriesTriangularPart(itm, itl, nbCols_, nbRows_, "col", vb, os);
  }
}

void SymDenseStorage::printEntries(std::ostream& os, const std::vector< Matrix<complex_t> >& m, number_t vb, SymType sym) const
{
  number_t nbDiag = nbRows_, nbLow = nbRows_ * (nbRows_ - 1) / 2;
  std::vector< Matrix<complex_t> >::const_iterator itm = m.begin() + 1, itl = itm + nbDiag;
  os << eol << words("lower triangular part") << " (" << nbDiag + nbLow << " " << words("entries") << ",";
  printMatrixEntriesTriangularPart(itm, itl, nbRows_, nbCols_, "row", vb, os);
  if(sym == _noSymmetry)
  {
    itm = m.begin() + 1;
    itl = itm + nbDiag;
    os << eol << words("upper triangular part") << " (" << nbDiag + nbLow << " " << words("entries") << ",";
    printMatrixEntriesTriangularPart(itm, itl, nbCols_, nbRows_, "col", vb, os);
  }
}

/*--------------------------------------------------------------------------------
 template Matrix x Vector & Vector x Matrix multiplications and specializations
 result vector has to be sized before
--------------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void SymDenseStorage::multMatrixVector(const std::vector<M>& m, V* vp, R* rp, SymType sym) const
{
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  V* vpe = vp + nbCols_;
  R* rpe = rp + nbRows_;
  DenseStorage::diagonalMatrixVector(itm, vp, rp, rpe);   // D*v
  itl = itm;
  DenseStorage::lowerMatrixVector(itm, vp, vpe, rp, rpe); // L*V
  if(sym == _noSymmetry) {DenseStorage::upperMatrixVector(itm, vp, vpe, rp, rpe); }      // U * v
  else                   {DenseStorage::lowerVectorMatrix(itl, vp, vpe, rp, rpe, sym); } // matrix has a symmetry property: U = L^t, -L^t, L* or -L*, perform L^t * v = v * L
}

template<typename M, typename V, typename R>
void SymDenseStorage::multMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, SymType sym) const
{
  trace_p->push("SymDenseStorage::multMatrixVector");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl=itm+diagonalSize();
  typename std::vector<V>::const_iterator itvb = v.begin(), itve = v.end();
  typename std::vector<R>::iterator it_rb = r.begin(), it_re = r.end();
  DenseStorage::diagonalMatrixVector(itm, itvb, it_rb, it_re);   // D*v

  #ifndef XLIFEPP_WITH_OMP
  DenseStorage::lowerMatrixVector(itm, itvb, itve, it_rb, it_re); // L*V
  if(sym == _noSymmetry) {DenseStorage::upperMatrixVector(itm, itvb, itve, it_rb, it_re); }      // U * v
  else                   {DenseStorage::lowerVectorMatrix(itl, itvb, itve, it_rb, it_re, sym); } // matrix has a symmetry property: U = L^t, -L^t, L* or -L*, perform L^t * v = v * L
  #else
  if(Environment::parallelOn())
   {
       DenseStorage::parallelLowerMatrixVector(_lower, itm, v, r);
       typename std::vector<M>::const_iterator itu=itl+lowerPartSize();
       if(sym == _noSymmetry) {DenseStorage::parallelUpperMatrixVector(_lower, itu, v, r, sym); }      // U * v
       else                   {DenseStorage::parallelLowerVectorMatrix(_lower, itl, v, r, sym); }      // matrix has a symmetry property: U = L^t, -L^t, L* or -L*, perform L^t * v = v * L
   }
   else
   {
       DenseStorage::lowerMatrixVector(itm, itvb, itve, it_rb, it_re); // L*V
       if(sym == _noSymmetry) {DenseStorage::upperMatrixVector(itm, itvb, itve, it_rb, it_re); }      // U * v
       else                   {DenseStorage::lowerVectorMatrix(itl, itvb, itve, it_rb, it_re, sym); } // matrix has a symmetry property: U = L^t, -L^t, L* or -L*, perform L^t * v = v * L
   }
  #endif
  trace_p->pop();
}

template<typename M, typename V, typename R>
void SymDenseStorage::multVectorMatrix(const std::vector<M>& m, V* vp, R* rp, SymType sym) const
{
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl=itm+diagonalSize(), itu=itl+lowerPartSize();
  V* vpe = vp + nbCols_;
  R* rpe = rp + nbRows_;
  DenseStorage::diagonalMatrixVector(itm, vp, rp, rpe);   // D*v
  itl = itm;
  DenseStorage::lowerVectorMatrix(itm, vp, vpe, rp, rpe); // L*V
  if(sym == _noSymmetry) {DenseStorage::upperVectorMatrix(itu, vp, vpe, rp, rpe); }      // U * v
  else                   {DenseStorage::lowerMatrixVector(itl, vp, vpe, rp, rpe, sym); } // matrix has a symmetry property: U = L^t, -L^t, L* or -L*, perform L^t * v = v * L
}

template<typename M, typename V, typename R>
void SymDenseStorage::multVectorMatrix(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, SymType sym) const
{
  trace_p->push("SymDenseStorage::multMatrixVector");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl=itm+diagonalSize(), itu=itl+lowerPartSize();
  typename std::vector<V>::const_iterator itvb = v.begin(), itve = v.end();
  typename std::vector<R>::iterator it_rb = r.begin(), it_re = r.end();
  DenseStorage::diagonalMatrixVector(itm, itvb, it_rb, it_re);    // D*v
  #ifndef XLIFEPP_WITH_OMP
  DenseStorage::lowerVectorMatrix(itm, itvb, itve, it_rb, it_re); // L*V
  if(sym == _noSymmetry) { DenseStorage::upperVectorMatrix(itu, itvb, itve, it_rb, it_re); }      // U * v
  else                   { DenseStorage::lowerMatrixVector(itl, itvb, itve, it_rb, it_re, sym); } // matrix has a symmetry property: U = L^t, -L^t, L* or -L*, perform L^t * v = v * L
  #else
  if(Environment::parallelOn())
   {
       parallelLowerVectorMatrix(_lower, itm, v, r); // L*V
       if(sym == _noSymmetry) {DenseStorage::parallelUpperVectorMatrix(_lower, itu, v, r, sym); } // U * v
       else                   {DenseStorage::parallelLowerMatrixVector(_lower, itl, v, r, sym); } // matrix has a symmetry property: U = L^t, -L^t, L* or -L*, perform L^t * v = v * L
   }
   else
   {
       lowerVectorMatrix(itm, itvb, itve, it_rb, it_re); // L*V
       if(sym == _noSymmetry) {DenseStorage::upperVectorMatrix(itu, itvb, itve, it_rb, it_re, sym); } // U * v
       else                   {DenseStorage::lowerMatrixVector(itl, itvb, itve, it_rb, it_re, sym); } // matrix has a symmetry property: U = L^t, -L^t, L* or -L*, perform L^t * v = v * L
   }
  #endif
  trace_p->pop();
}

// Set the Diagonal with a specific value of DualDense and SymDense
template<typename T>
void SymDenseStorage::setDiagValueSymDense(std::vector<T>& v, const T m)
{
  typename std::vector<T>::iterator it = v.begin() + 1;
  number_t diagSize = diagonalSize();
  for (number_t r = 0; r < diagSize; r++, it++) {*it = m;}
}

/*!
  Add two matrices
  \param m1 vector values_ of first matrix
  \param m2 vector values_ of second matrix
  \param r vector values_ of result matrix
 */
template<typename M1, typename M2, typename R>
void SymDenseStorage::addMatrixMatrix(const std::vector<M1>& m1, const std::vector<M2>& m2, std::vector<R>& r) const
{
  trace_p->push("SymDenseStorage::addMatrixMatrix");
  typename std::vector<M1>::const_iterator itm1 = m1.begin() + 1;
  typename std::vector<M2>::const_iterator itm2 = m2.begin() + 1;
  typename std::vector<R>::iterator it_rb = r.begin() + 1, it_re = r.end();
  sumMatrixMatrix(itm1, itm2, it_rb, it_re);
  trace_p->pop();
}

/* -------------------------------------------------------------------------------------------------------
                                    LDLT factorization with no pivoting strategy
   -------------------------------------------------------------------------------------------------------*/
/*! LDLt factorization with no pivoting
    LDLt is a sym dense matrix where its rows have been permuted according to the row permutation vector (LDLt=PA)
    works only for symmetric matrix
    be care: solving A*X=B is equivalent to solving P*A*X=P*B <=> L*D*Lt*X=PB, the product by P has to be done before calling triangular part solvers!
              product B=A*X is equivalent to product B=inv(P)*L*D*Lt*X, the product by inv(P) has to be done after calling triangular part products!
              product B=X*A is equivalent to product B=X*inv(P)*L*D*Lt=tr(L*D*Lt*P*Xt), the product by P has to be done before calling triangular part products!
              this products are not taken into account by storage class !
*/
template<typename T>
void SymDenseStorage::ldlt(std::vector<T>& A, std::vector<T>& LD) const
{
  if (&A!=&LD) LD=A;   //copy A
  std::vector<number_t> adrow(nbRows_);  //address of LDi0
  std::vector<number_t>::iterator ita=adrow.begin();
  for(number_t i=0; i<nbRows_; i++, ++ita) *ita = pos(i+1,1);
  typename std::vector<T>::iterator itij, itkj, itik, itlub = LD.begin();
  T piv;
  bool show_status = (nbRows_ > 1000  && theVerboseLevel>0);
  if (show_status) std::cout<<"   in row dense LD factorization with no permutation, "<<numberOfThreads()<<" threads: "<<std::flush;
  for (number_t k=0; k<nbRows_; k++) //main Gauss loop
  {
    piv=LD[k+1];
    itkj=itlub+adrow[k];       //LDk0
    for(number_t j=0; j<k; j++, ++itkj)  piv-= *itkj * *itkj * LD[j+1];
    LD[k+1]=piv;
    if (std::abs(piv)<theTolerance)
    {
      where("SymDenseStorage::ldlt(...)");
      error("small_pivot");
    }
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for firstprivate(itij, itkj, itik) schedule(dynamic)
    #endif // XLIFEPP_WITH_OMP
    for (number_t i=k+1; i<nbRows_; i++)
    {
      itij=itlub+adrow[i];   //LDi0
      itkj=itlub+adrow[k];   //LDk0
      itik=itij+k;           //LDik
      T Lik=*itik;
      for(number_t j=0; j<k; j++, ++itij,++itkj)  Lik-=*itij * *itkj;
      *itik=Lik/piv;
    }  //end loop i

    if (show_status && nbRows_>10) //progress status
    {
      number_t  p = k % (nbRows_/10);
      if(p == 0) std::cout<<(k/(nbRows_/10))<<"0% "<<std::flush;
    }
  }//end loop k
}
/*-----------------------------------------------------------------------
              triangular part matrix * vector
--------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void SymDenseStorage::diagonalMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
    else r.resize(nbRows_);
    typename std::vector<M>::const_iterator itm=m.begin()+1;
    typename std::vector<V>::const_iterator itv=v.begin();
    typename std::vector<R>::iterator itr=r.begin();
    for(number_t i=0;i<std::min(nbRows_,nbCols_);i++, ++itv,++itr, itm++) *itr = *itm * *itv;
}

template<typename M, typename V, typename R>
void SymDenseStorage::lowerMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    diagonalMatrixVector(m,v,r);
    typename std::vector<M>::const_iterator itm=m.begin()+diagonalSize()+1;
    typename std::vector<V>::const_iterator itvb=v.begin(), itve=v.end();
    typename std::vector<R>::iterator itrb=r.begin(), itre=r.end();
  #ifndef XLIFEPP_WITH_OMP
    DenseStorage::lowerMatrixVector(itm, itvb, itve, itrb, itre);
  #else
    if(Environment::parallelOn()) DenseStorage::parallelLowerMatrixVector(_lower, itm, v, r, _noSymmetry);
    else  DenseStorage::lowerMatrixVector(itm, itvb, itve, itrb, itre);
  #endif // XLIFEPP_WITH_OMP
}

template<typename M, typename V, typename R>
void SymDenseStorage::lowerD1MatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
    else r.resize(nbRows_);
    typename std::vector<V>::const_iterator itvb=v.begin(), itve=v.end(), itv=itvb;
    typename std::vector<R>::iterator itrb=r.begin(), itre=r.end(), itr=itrb;
    for(number_t i=0;i<std::min(nbRows_,nbCols_);i++, ++itv,++itr) *itr = *itv; //diag part
    typename std::vector<M>::const_iterator itm=m.begin()+diagonalSize()+1;
  #ifndef XLIFEPP_WITH_OMP
    DenseStorage::lowerMatrixVector(itm, itvb, itve, itrb, itre);
  #else
    if(Environment::parallelOn()) DenseStorage::parallelLowerMatrixVector(_lower, itm, v, r, _noSymmetry);
    else  DenseStorage::lowerMatrixVector(itm, itvb, itve, itrb, itre);
  #endif // XLIFEPP_WITH_OMP
}

template<typename M, typename V, typename R>
void SymDenseStorage::upperMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, SymType s) const
{
    diagonalMatrixVector(m,v,r);
    typename std::vector<V>::const_iterator itvb=v.begin(), itve=v.end();
    typename std::vector<R>::iterator itrb=r.begin(), itre=r.end();
    typename std::vector<M>::const_iterator itm=m.begin()+diagonalSize()+1;
  #ifndef XLIFEPP_WITH_OMP
    DenseStorage::lowerVectorMatrix(itm, itvb, itve, itrb, itre, s);
  #else
    if(Environment::parallelOn()) DenseStorage::parallelLowerVectorMatrix(_lower, itm, v, r, s);
    else  DenseStorage::lowerVectorMatrix(itm, itvb, itve, itrb, itre, s);
  #endif // XLIFEPP_WITH_OMP
}

template<typename M, typename V, typename R>
void SymDenseStorage::upperD1MatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, SymType s) const
{
    if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
    else r.resize(nbRows_);
    typename std::vector<V>::const_iterator itvb=v.begin(), itve=v.end(), itv=itvb;
    typename std::vector<R>::iterator itrb=r.begin(), itre=r.end(), itr=itrb;
    for(number_t i=0;i<std::min(nbRows_,nbCols_); i++, ++itv,++itr) *itr = *itv; //diag part
    typename std::vector<M>::const_iterator itm=m.begin()+diagonalSize()+1;
    #ifndef XLIFEPP_WITH_OMP
    DenseStorage::lowerVectorMatrix(itm, itvb, itve, itrb, itre, s);
  #else
    if(Environment::parallelOn()) DenseStorage::parallelLowerVectorMatrix(_lower, itm, v, r, s);
    else  DenseStorage::lowerVectorMatrix(itm, itvb, itve, itrb, itre, s);
  #endif // XLIFEPP_WITH_OMP
}

/* -------------------------------------------------------------------------------------------------------
                                   lower and upper solvers
   -------------------------------------------------------------------------------------------------------*/
template<typename M, typename V, typename X>
void SymDenseStorage::lowerD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const
{
    typename std::vector<V>::iterator itv=v.begin();
    typename std::vector<X>::iterator itxb=x.begin(), itx;
    typename std::vector<M>::const_iterator itmb=m.begin(), itm;
    number_t n=x.size();
    for(number_t k=1; k<=n; ++k, ++itv)
    {
        X t=*itv;
        itx=itxb;
        itm=itmb + pos(k,1);
        for(number_t j=1; j<k; ++j,++itx, ++itm) t -= *itm * *itx;
        *itx = t;
    }
}

template<typename M, typename V, typename X>
void SymDenseStorage::upperSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x, const SymType sym) const
{
    typename std::vector<X>::iterator itxb=x.begin(), itx=itxb;
    typename std::vector<V>::iterator itv;
    typename std::vector<M>::const_iterator itmb=m.begin(), itm;
    for(itv=v.begin();itv!=v.end(); ++itv, ++itx) *itx=*itv; //initialize x
    number_t n=x.size();
    for(number_t k=n; k>0; k--)
    {
        X t=*(itxb+k-1); itx=itxb; itm=itmb + pos(k,1);  //column k
        switch(sym)
        {
            case _skewSymmetric: for(number_t j=1; j<k; j++, ++itx, ++itm) *itx += *itm * t;
            break;
            case _selfAdjoint:   for(number_t j=1; j<k; j++, ++itx, ++itm) *itx -= conj(*itm) * t;
            break;
            case _skewAdjoint:   for(number_t j=1; j<k; j++, ++itx, ++itm) *itx += conj(*itm) * t;
            break;
            default:  for(number_t j=1; j<k; j++, ++itx, ++itm) *itx -= *itm * t;
        }
        *itx/=*itm;
    }
}

template<typename M, typename V, typename X>
void SymDenseStorage::upperD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x, const SymType sym) const
{
    typename std::vector<X>::iterator itxb=x.begin(), itx=itxb;
    typename std::vector<V>::iterator itv;
    typename std::vector<M>::const_iterator itmb=m.begin(), itm;
    for(itv=v.begin();itv!=v.end(); ++itv, ++itx) *itx=*itv; //initialize x
    number_t n=x.size();
    for(number_t k=n; k>0; k--)
    {
        X t=*(itxb+k-1); itx=itxb; itm=itmb + pos(k,1);  //column k
        switch(sym)
        {
            case _skewSymmetric: for(number_t j=1; j<k; j++, ++itx, ++itm) *itx += *itm * t;
            break;
            case _selfAdjoint:   for(number_t j=1; j<k; j++, ++itx, ++itm) *itx -= conj(*itm) * t;
            break;
            case _skewAdjoint:   for(number_t j=1; j<k; j++, ++itx, ++itm) *itx += conj(*itm) * t;
            break;
            default:  for(number_t j=1; j<k; j++, ++itx, ++itm) *itx -= *itm * t;
        }
    }
}

template<typename M, typename V, typename X>
void SymDenseStorage::diagonalSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const
{
    typename std::vector<V>::iterator itv=v.begin();
    typename std::vector<X>::iterator itx=x.begin();
    typename std::vector<M>::const_iterator itm=m.begin()+1;  //values really start at index 1
    for(; itx!=x.end(); ++itv, ++itx, ++itm) *itx = *itv / *itm ;
}

/* -------------------------------------------------------------------------------------------------------
                                        UMFPack stuff
   -------------------------------------------------------------------------------------------------------*/
/*!
   Extract and convert matrix storage to UMFPack format (Matlab sparse matrix)
   \param m1 vector values_ current matrix
   \param colPtUmf vector column Pointer of UMFPack format
   \param rowIdxUmf vector row Index of UMFPack format
   \param resultUmf vector values of UMFPack format
   \param sym type of symmetry
 */
template<typename M1, typename Idx>
void SymDenseStorage::toUmfPack(const std::vector<M1>& m1, std::vector<Idx>& colPtUmf, std::vector<Idx>& rowIdxUmf, std::vector<M1>& resultUmf, const SymType sym) const
{
    // Reserve memory for resultUmf and set its size to 0
    resultUmf.reserve(m1.size());
    resultUmf.clear();

    // Reserve memory for rowIdxUmf and set its size to 0
    rowIdxUmf.reserve(m1.size());
    rowIdxUmf.clear();

    // Resize column pointer as dualCs's column pointer size
    colPtUmf.clear();
    colPtUmf.resize(nbCols_+1, Idx(0));
    // Because colPointer always begins with 0, so need to to count from the second position
    typename std::vector<Idx>::iterator itColPtUmf = colPtUmf.begin()+1;
    colPtUmf[0] = Idx(0);

    typename std::vector<M1>::const_iterator itbval = m1.begin()+1, itmDiag = itbval;
    typename std::vector<M1>::const_iterator itmLow = itbval + diagonalSize();
    typename std::vector<M1>::const_iterator itmUp  = itmLow + lowerPartSize();
    if (_noSymmetry != sym) itmUp = itmLow;
    typename std::vector<M1>::const_iterator itbUpval, iteUpval, itUpval, itbLowval, itLowval;

    itbUpval = iteUpval = itmUp;
    itLowval = itbLowval =  itmLow;
    for (number_t colIdx = 0; colIdx < nbCols_; ++colIdx) {
        number_t nzCol = 0;

        // Process upper part
        if (0 != colIdx) {
            itbUpval = iteUpval;
            iteUpval = itbUpval + colIdx;

            itUpval = std::find_if(itbUpval, iteUpval, std::bind(std::not_equal_to<M1>(), std::placeholders::_1, M1(0)));
            if (itUpval != iteUpval) {
                switch (sym) {
                case _skewSymmetric:
                    resultUmf.push_back(-(*itUpval));
                    break;
                case _selfAdjoint:
                    resultUmf.push_back(conj(*itUpval));
                    break;
                case _skewAdjoint:
                    resultUmf.push_back(-conj(*itUpval));
                    break;
                default:
                    resultUmf.push_back(*itUpval);
                    break;
                }
                rowIdxUmf.push_back(itUpval - itbUpval);
                ++nzCol;
                ++itUpval;
                while (itUpval != iteUpval) {
                    itUpval = std::find_if(itUpval, iteUpval, std::bind(std::not_equal_to<M1>(), std::placeholders::_1, M1(0)));
                    if (itUpval != iteUpval) {
                        switch (sym) {
                        case _skewSymmetric:
                            resultUmf.push_back(-(*itUpval));
                            break;
                        case _selfAdjoint:
                            resultUmf.push_back(conj(*itUpval));
                            break;
                        case _skewAdjoint:
                            resultUmf.push_back(-conj(*itUpval));
                            break;
                        default:
                            resultUmf.push_back(*itUpval);
                            break;
                        }
                        rowIdxUmf.push_back(itUpval - itbUpval);
                        ++nzCol;
                        ++itUpval;
                    }
                }
            }
        }
        // Process diagonal part
        if (M1(0) != *itmDiag) {
            resultUmf.push_back(*itmDiag);
            rowIdxUmf.push_back(itmDiag - itbval);
            ++nzCol;
        }
        ++itmDiag;

        if ((nbCols_-1) != (colIdx)) {

            for (number_t rowIdx = colIdx; rowIdx < nbRows_-1; rowIdx++)
            {
                itLowval += rowIdx;
                if (M1(0) != *(itLowval) ) {
                    resultUmf.push_back(*itLowval);
                    rowIdxUmf.push_back(rowIdx+1);
                    ++nzCol;
                }

            }
            itLowval = itbLowval + colIdx + 1;
            itbLowval = itLowval;

        }

        // Update colPtUmf with number of non-zero value
        *itColPtUmf += *(itColPtUmf-1) + nzCol;
        ++itColPtUmf;
    }
}

} // end of namespace xlifepp

