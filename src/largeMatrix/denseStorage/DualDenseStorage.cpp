/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DualDenseStorage.cpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Implementation of xlifepp::DualDenseStorage class and functionnalities
*/

#include "DualDenseStorage.hpp"
#include <functional>

namespace xlifepp
{
/*=======================================================================================
  Constructors, destructor
  =======================================================================================*/
// default constructor
DualDenseStorage::DualDenseStorage(string_t id)
  : DenseStorage(_dual, id) {}

// constructor for square structure (number of rows)
DualDenseStorage::DualDenseStorage(number_t nr, string_t id)
  : DenseStorage(_dual, nr, id) {}

// constructor for rectangular structure (number of rows/columns)
DualDenseStorage::DualDenseStorage(number_t nr, number_t nc, string_t id)
  : DenseStorage(_dual, nr, nc, id) {}

/*-------------------------------------------------------------------------------
                           size utilities
  -------------------------------------------------------------------------------*/
number_t DualDenseStorage::lowerPartSize() const
{
  if(nbCols_ >= nbRows_) { return (nbRows_ * (nbRows_ - 1)) / 2; }
  return nbCols_ * (nbRows_ - nbCols_) + (nbCols_ * (nbCols_ - 1)) / 2;
}

number_t DualDenseStorage::upperPartSize() const
{
  if(nbRows_ >= nbCols_) { return (nbCols_ * (nbCols_ - 1)) / 2; }
  return nbRows_ * (nbCols_ - nbRows_) + (nbRows_ * (nbRows_ - 1)) / 2;
}

number_t DualDenseStorage::pos(number_t i, number_t j, SymType s) const
{
  if(i == 0 || i > nbRows_ || j == 0 || j > nbCols_) { return 0; }
  number_t nbDiag = diagonalSize();
  if(i == j && i <= nbDiag) { return i; }
  else if(i > j)
  {
    number_t baseAddr = nbDiag;
    if(i <= nbDiag) { return (baseAddr + ((i - 2) * (i - 1)) / 2 + j); }
    else            { return (baseAddr + ((nbCols_ - 1) * nbCols_) / 2 + (i - nbCols_-1) * nbCols_ + j); }
  }
  else
  {
    number_t baseAddr = nbDiag + ((nbDiag - 1) * (nbDiag) / 2);
    if(nbRows_ > nbCols_) { baseAddr += nbCols_ * (nbRows_ - nbCols_); }
    if(j <= nbDiag) { return (baseAddr + ((j - 2) * (j - 1)) / 2 + i); }
    else            { return (baseAddr + ((nbRows_ - 1) * nbRows_ ) / 2 + (j - nbRows_-1) * nbRows_ + i); }
  }
}

// access to submatrix positions, useful for assembling matrices
// return the vector of adresses (relative number in storage) stored by row
// note: index of given rows and cols  begins at 1
//        the returned adresses begin at 0
void DualDenseStorage::positions(const std::vector<number_t>& rows, const std::vector<number_t>& cols, std::vector<number_t>& pos, bool errorOn, SymType sym) const
{
  number_t nbDiag = diagonalSize();
  //number_t nbLow=nbDiag+((nbDiag-1)*(nbDiag-2))/2+(nbRows_-nbDiag)*nbCols_;  //Daniel's formulae ?
  number_t nbLow = nbDiag + lowerPartSize();
  number_t nbp = rows.size() * cols.size();
  if(pos.size() != nbp) { pos.resize(nbp); }
  std::vector<number_t>::iterator it_p = pos.begin();
  for(std::vector<number_t>::const_iterator it_r = rows.begin(); it_r != rows.end(); it_r++)
  {
    number_t adr1 = (*it_r - 1) * (*it_r - 2) / 2;
    for(std::vector<number_t>::const_iterator itc = cols.begin(); itc != cols.end(); itc++, it_p++)
    {
      if(*itc < *it_r)  { *it_p = nbDiag + adr1 + *itc; }   // lower part
      else if(*itc == *it_r) { *it_p = *it_r; }              // diagonal
      else { *it_p = nbLow + (*itc - 1) * (*itc - 2) / 2 + *it_r; } // upper part
    }
  }
}

// get (row indices, adress) of col c in set [r1,r2] ## SymType not used ##
std::vector<std::pair<number_t, number_t> > DualDenseStorage::getCol(SymType s, number_t c, number_t r1, number_t r2) const
{
    number_t nbr=r2;
    if(nbr==0) nbr=nbRows_;
    std::vector<std::pair<number_t, number_t> > rows(nbr-r1+1);
    std::vector<std::pair<number_t, number_t> >::iterator itrow=rows.begin();
    for(number_t i=r1;i<=nbr;i++, itrow++)  *itrow=std::make_pair(i,pos(i,c));
    return rows;
}

// get (col indices, adress) of row r in set [c1,c2] ## SymType not used ##
std::vector<std::pair<number_t, number_t> > DualDenseStorage::getRow(SymType s, number_t r, number_t c1, number_t c2) const
{
    number_t nbc=c2;
    if(nbc==0) nbc=nbCols_;
    std::vector<std::pair<number_t, number_t> > cols(nbc-c1+1);
    std::vector<std::pair<number_t, number_t> >::iterator itcol=cols.begin();
    for(number_t j=c1;j<=nbc;j++, itcol++)  *itcol=std::make_pair(j,pos(r,j));
    return cols;
}

// create a new scalar DualDense storage from current DualDense storage and submatrix sizes
DualDenseStorage* DualDenseStorage::toScalar(dimen_t nbr, dimen_t nbc)
{
    MatrixStorage* nsto=findMatrixStorage(stringId, _dense, _dual, buildType_, true, nbr*nbRows_, nbc*nbCols_);
    if(nsto!=nullptr) return static_cast<DualDenseStorage*>(nsto);
    DualDenseStorage* dd = new DualDenseStorage(nbr*nbRows_, nbc*nbCols_, stringId);
    dd->buildType() = buildType_;
    dd->scalarFlag()=true;
    return dd;
}

/*-------------------------------------------------------------------------------
                       print DualDense matrix to ostream
 -------------------------------------------------------------------------------*/
void DualDenseStorage::printEntries(std::ostream& os, const std::vector<real_t>& m, number_t vb, SymType sym) const
{
  number_t nbDiag = nbRows_, nbLow = nbRows_ * (nbRows_ - 1) / 2, nbUp = nbLow;
  if(nbRows_ > nbCols_)
  {
    nbDiag = nbCols_;
    nbUp = (nbCols_ * (nbCols_ - 1)) / 2;
    nbLow = nbUp + nbCols_ * (nbRows_ - nbCols_);
  }
  else if(nbRows_ < nbCols_)
  {
    nbDiag = nbRows_;
    nbLow = (nbRows_ * (nbRows_ - 1)) / 2;
    nbUp = nbLow + nbRows_ * (nbCols_ - nbRows_);
  }
  std::vector<real_t>::const_iterator itm = m.begin() + 1, itl = itm + nbDiag;
  os<<eol;
  os << words("lower triangular part") << " (" << nbDiag + nbLow << " " << words("entries") << ",";
  printScalarEntriesTriangularPart(itm, itl, nbRows_, nbCols_, entriesPerRow,
                                   entryWidth, entryPrec, "row", vb, os);

    itm = m.begin() + 1;
    itl = itm + nbDiag + nbLow;
    os << words("upper triangular part") << " (" << nbDiag + nbUp << " " << words("entries") << ",";
    printScalarEntriesTriangularPart(itm, itl, nbCols_, nbRows_, entriesPerRow,
                                     entryWidth, entryPrec, "col", vb, os);
}

void DualDenseStorage::printEntries(std::ostream& os, const std::vector<complex_t>& m, number_t vb, SymType sym) const
{
  number_t nbDiag = nbRows_, nbLow = nbRows_ * (nbRows_ - 1) / 2, nbUp = nbLow;
  if(nbRows_ > nbCols_)
  {
    nbDiag = nbCols_;
    nbUp = nbCols_ * (nbCols_ - 1) / 2;
    nbLow = nbUp + nbCols_ * (nbRows_ - nbCols_);
  }
  else if(nbRows_ < nbCols_)
  {
    nbDiag = nbRows_;
    nbLow = nbRows_ * (nbRows_ - 1) / 2;
    nbUp = nbLow + nbRows_ * (nbCols_ - nbRows_);
  }
  std::vector<complex_t>::const_iterator itm = m.begin() + 1, itl = itm + nbDiag;
  os << eol ;
  os << words("lower triangular part") << " (" << nbDiag + nbLow << " " << words("entries") << ",";
  printScalarEntriesTriangularPart(itm, itl, nbRows_, nbCols_, entriesPerRow / 2,
                                   2 * entryWidth + 1, entryPrec, "row", vb, os);

    itm = m.begin() + 1;
    itl = itm + nbDiag + nbLow;
    os << words("upper triangular part") << " (" << nbDiag + nbUp << " " << words("entries") << ",";
    printScalarEntriesTriangularPart(itm, itl, nbCols_, nbRows_, entriesPerRow / 2,
                                     2 * entryWidth + 1, entryPrec, "col", vb, os);
}

void DualDenseStorage::printEntries(std::ostream& os, const std::vector< Vector<real_t> >& m, number_t vb, SymType sym) const
{error("storage_novector", "ColDenseStorage::printEntries");}

void DualDenseStorage::printEntries(std::ostream& os, const std::vector< Vector<complex_t> >& m, number_t vb, SymType sym) const
{error("storage_novector", "ColDenseStorage::printEntries");}

void DualDenseStorage::printEntries(std::ostream& os, const std::vector< Matrix<real_t> >& m, number_t vb, SymType sym) const
{
  number_t nbDiag = nbRows_, nbLow = nbRows_ * (nbRows_ - 1) / 2, nbUp = nbLow;
  if(nbRows_ > nbCols_)
  {
    nbDiag = nbCols_;
    nbUp = (nbCols_ * (nbCols_ - 1)) / 2;
    nbLow = nbUp + nbCols_ * (nbRows_ - nbCols_);
  }
  else if(nbRows_ < nbCols_)
  {
    nbDiag = nbRows_;
    nbLow = nbRows_ * (nbRows_ - 1) / 2;
    nbUp = nbLow + nbRows_ * (nbCols_ - nbRows_);
  }
  std::vector< Matrix<real_t> >::const_iterator itm = m.begin() + 1, itl = itm + nbDiag;

  os << eol;
  os << words("lower triangular part") << " (" << nbDiag + nbLow << " " << words("entries") << ",";
  printMatrixEntriesTriangularPart(itm, itl, nbRows_, nbCols_, "row", vb, os);

    itm = m.begin() + 1;
    itl = itm + nbDiag + nbLow;
    os << words("upper triangular part") << " (" << nbDiag + nbUp << " " << words("entries") << ",";
    printMatrixEntriesTriangularPart(itm, itl, nbCols_, nbRows_, "col", vb, os);
}

void DualDenseStorage::printEntries(std::ostream& os, const std::vector< Matrix<complex_t> >& m, number_t vb, SymType sym) const
{
  number_t nbDiag = nbRows_, nbLow = nbRows_ * (nbRows_ - 1) / 2, nbUp = nbLow;
  if(nbRows_ > nbCols_)
  {
    nbDiag = nbCols_;
    nbUp = nbCols_ * (nbCols_ - 1) / 2;
    nbLow = nbUp + nbCols_ * (nbRows_ - nbCols_);
  }
  else if(nbRows_ < nbCols_)
  {
    nbDiag = nbRows_;
    nbLow = nbRows_ * (nbRows_ - 1) / 2;
    nbUp = nbLow + nbRows_ * (nbCols_ - nbRows_);
  }
  std::vector< Matrix<complex_t> >::const_iterator itm = m.begin() + 1, itl = itm + nbDiag;

  os << eol;
  os << words("lower triangular part") << " (" << nbDiag + nbLow << " " << words("entries") << ",";
  printMatrixEntriesTriangularPart(itm, itl, nbRows_, nbCols_, "row", vb, os);

    itm = m.begin() + 1;
    itl = itm + nbDiag + nbLow;
    os << words("upper triangular part") << " (" << nbDiag + nbUp << " " << words("entries") << ",";
    printMatrixEntriesTriangularPart(itm, itl, nbCols_, nbRows_, "col", vb, os);
}

/*-------------------------------------------------------------------------------
 template transposed Matrix x Vector or Vector x Matrix multiplication
 -------------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void DualDenseStorage::multMatrixVector(const std::vector<M>& m, V* vp, R* rp) const
{
  typename std::vector<M>::const_iterator itm = m.begin() + 1;
  V* vpe=vp+nbCols_;
  R* rpe=rp+nbRows_;
  DenseStorage::diagonalMatrixVector(itm, vp, rp, rpe);
  DenseStorage::lowerMatrixVector(itm, vp, vpe, rp, rpe);
  DenseStorage::upperMatrixVector(itm, vp, vpe, rp, rpe);
}


template<typename M, typename V, typename R>
void DualDenseStorage::multMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  trace_p->push("DualDenseStorage::multMatrixVector");
  typename std::vector<M>::const_iterator itm = m.begin() + 1;
  typename std::vector<V>::const_iterator itvb = v.begin(), itve = v.end();
  typename std::vector<R>::iterator it_rb = r.begin(), it_re = r.end();
  DenseStorage::diagonalMatrixVector(itm, itvb, it_rb, it_re);
  #ifndef XLIFEPP_WITH_OMP
  DenseStorage::lowerMatrixVector(itm, itvb, itve, it_rb, it_re);
  DenseStorage::upperMatrixVector(itm, itvb, itve, it_rb, it_re, _noSymmetry);
  #else
  if(Environment::parallelOn())
  {
      typename std::vector<M>::const_iterator itl = m.begin() + 1 + diagonalSize(), itu = itl + lowerPartSize();
      parallelLowerMatrixVector(_lower, itl, v, r);
      parallelUpperMatrixVector(_upper, itu, v, r, _noSymmetry);
  }
  else
  {
      DenseStorage::lowerMatrixVector(itm, itvb, itve, it_rb, it_re);
      DenseStorage::upperMatrixVector(itm, itvb, itve, it_rb, it_re, _noSymmetry);
  }
  #endif // XLIFEPP_WITH_OMP
  trace_p->pop();
}

template<typename M, typename V, typename R>
void DualDenseStorage::multVectorMatrix(const std::vector<M>& m, V* vp, R* rp) const
{
  typename std::vector<M>::const_iterator itm = m.begin() + 1;
  V* vpe=vp+nbCols_;
  R* rpe=rp+nbRows_;
  DenseStorage::diagonalVectorMatrix(itm, vp, rp, rpe);
  DenseStorage::lowerVectorMatrix(itm, vp, vpe, rp, rpe);
  DenseStorage::upperVectorMatrix(itm, vp, vpe, rp, rpe);
}

template<typename M, typename V, typename R>
void DualDenseStorage::multVectorMatrix(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  // Vector x Matrix or Matrix transposed x Vector multiplication
  trace_p->push("DualDenseStorage::multVectorMatrix");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl = itm + diagonalSize(), itu = itl + lowerPartSize();
  typename std::vector<V>::const_iterator itvb = v.begin(), itve = v.end();
  typename std::vector<R>::iterator it_rb = r.begin(), it_re = r.end();
  DenseStorage::diagonalMatrixVector(itm, itvb, it_rb, it_re);
  #ifndef XLIFEPP_WITH_OMP
  DenseStorage::upperMatrixVector(itl, itvb, itve, it_rb, it_re); // tL*tV = t(V*L)
  DenseStorage::lowerMatrixVector(itu, itvb, itve, it_rb, it_re); // tU*tV = t(V*U)
  #else
  if(Environment::parallelOn())
  {
      parallelUpperMatrixVector(_lower, itl, v, r); // tL*tV = t(V*L)
      parallelLowerMatrixVector(_upper, itu, v, r); // tU*tV = t(V*U)
  }
  else
  {
      DenseStorage::upperMatrixVector(itl, itvb, itve, it_rb, it_re); // tL*tV = t(V*L)
      DenseStorage::lowerMatrixVector(itu, itvb, itve, it_rb, it_re); // tU*tV = t(V*U)
  }
  #endif // XLIFEPP_WITH_OMP
  trace_p->pop();
}

/*-------------------------------------------------------------------------------
 template add matrix matrix
 -------------------------------------------------------------------------------*/
// Set the Diagonal with a specific value of DualDense
template<typename T>
void DualDenseStorage::setDiagValueDualDense(std::vector<T>& v, const T m)
{
  typename std::vector<T>::iterator it = v.begin() + 1;
  number_t diagSize = diagonalSize() + 1;
  for (number_t r = 1; r < diagSize; r++, it++) {*it = m;}
}

/*!
   Add two matrices
   \param m1 vector values_ of first matrix
   \param m2 vector values_ of second matrix
   \param r vector values_ of result matrix
 */
template<typename M1, typename M2, typename R>
void DualDenseStorage::addMatrixMatrix(const std::vector<M1>& m1, const std::vector<M2>& m2, std::vector<R>& r) const
{
  trace_p->push("DualDenseStorage::addMatrixMatrix");
  typename std::vector<M1>::const_iterator itm1 = m1.begin() + 1;
  typename std::vector<M2>::const_iterator itm2 = m2.begin() + 1;
  typename std::vector<R>::iterator it_rb = r.begin() + 1, it_re = r.end();
  sumMatrixMatrix(itm1, itm2, it_rb, it_re);
  trace_p->pop();
}

/*-----------------------------------------------------------------------
              triangular part matrix * vector
--------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void DualDenseStorage::diagonalMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
    else r.resize(nbRows_);
    typename std::vector<M>::const_iterator itm=m.begin()+1;
    typename std::vector<V>::const_iterator itv=v.begin();
    typename std::vector<R>::iterator itr=r.begin();
    for(number_t i=0;i<std::min(nbRows_,nbCols_);i++, ++itv,++itr, ++itm) *itr = *itm * *itv;
}

template<typename M, typename V, typename R>
void DualDenseStorage::lowerMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    diagonalMatrixVector(m,v,r);
    typename std::vector<M>::const_iterator itm=m.begin()+diagonalSize()+1;
    typename std::vector<V>::const_iterator itvb=v.begin(), itve=v.end();
    typename std::vector<R>::iterator itrb=r.begin(), itre=r.end();
  #ifndef XLIFEPP_WITH_OMP
    DenseStorage::lowerMatrixVector(itm, itvb, itve, itrb, itre);
  #else
    if(Environment::parallelOn()) DenseStorage::parallelLowerMatrixVector(_lower, itm, v, r, _noSymmetry);
    else  DenseStorage::lowerMatrixVector(itm, itvb, itve, itrb, itre);
  #endif // XLIFEPP_WITH_OMP
}

template<typename M, typename V, typename R>
void DualDenseStorage::lowerD1MatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
    else r.resize(nbRows_);
    typename std::vector<M>::const_iterator itmb=m.begin()+1, itm;
    typename std::vector<V>::const_iterator itvb=v.begin(), itve=v.end(), itv=itvb;
    typename std::vector<R>::iterator itrb=r.begin(), itre=r.end(), itr=itrb;
    for(number_t i=0;i<std::min(nbRows_,nbCols_); i++, ++itv, ++itr) *itr = *itv; //diag part
    itm=itmb+diagonalSize();
  #ifndef XLIFEPP_WITH_OMP
    DenseStorage::lowerMatrixVector(itm, itvb, itve, itrb, itre);
  #else
    if(Environment::parallelOn()) DenseStorage::parallelLowerMatrixVector(_lower, itm, v, r, _noSymmetry);
    else  DenseStorage::lowerMatrixVector(itm, itvb, itve, itrb, itre);
  #endif // XLIFEPP_WITH_OMP
}

template<typename M, typename V, typename R>
void DualDenseStorage::upperMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    diagonalMatrixVector(m,v,r);
    typename std::vector<M>::const_iterator itm = m.begin()+diagonalSize()+lowerPartSize()+1;
    typename std::vector<V>::const_iterator itvb=v.begin(), itve=v.end();
    typename std::vector<R>::iterator itrb=r.begin(), itre=r.end();
  #ifndef XLIFEPP_WITH_OMP
    DenseStorage::upperMatrixVector(itm, itvb, itve, itrb, itre);
  #else
    if(Environment::parallelOn()) DenseStorage::parallelUpperMatrixVector(_upper, itm, v, r, _noSymmetry);
    else  DenseStorage::upperMatrixVector(itm, itvb, itve, itrb, itre);
  #endif // XLIFEPP_WITH_OMP
}

template<typename M, typename V, typename R>
void DualDenseStorage::upperD1MatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
    else r.resize(nbRows_);
    typename std::vector<M>::const_iterator itmb=m.begin()+1, itm;
    typename std::vector<V>::const_iterator itvb=v.begin(), itve=v.end(), itv=itvb;
    typename std::vector<R>::iterator itrb=r.begin(), itre=r.end(), itr=itrb;
    for(number_t i=0;i<std::min(nbRows_,nbCols_); i++, ++itv, ++itr) *itr = *itv; //diag part
    itm=itmb+diagonalSize()+lowerPartSize();
  #ifndef XLIFEPP_WITH_OMP
    DenseStorage::upperMatrixVector(itm, itvb, itve, itrb, itre);
  #else
    if(Environment::parallelOn()) DenseStorage::parallelUpperMatrixVector(_upper, itm, v, r, _noSymmetry);
    else  DenseStorage::upperMatrixVector(itm, itvb, itve, itrb, itre);
  #endif // XLIFEPP_WITH_OMP
}

/*-----------------------------------------------------------------------
                              UMFPack stuff
--------------------------------------------------------------------------*/
 /*!
    Extract and convert matrix storage to UMFPack format (Matlab sparse matrix)
    \param m1 vector values_ current matrix
    \param colPtUmf vector column Pointer of UMFPack format
    \param rowIdxUmf vector row Index of UMFPack format
    \param resultUmf vector values of UMFPack format
  */
 template<typename M1, typename Idx>
 void DualDenseStorage::toUmfPack(const std::vector<M1>& m1, std::vector<Idx>& colPtUmf, std::vector<Idx>& rowIdxUmf, std::vector<M1>& resultUmf) const
 {
     // Reserve memory for resultUmf and set its size to 0
     resultUmf.reserve(m1.size());
     resultUmf.clear();

     // Reserve memory for rowIdxUmf and set its size to 0
     rowIdxUmf.reserve(m1.size());
     rowIdxUmf.clear();

     // Resize column pointer as dualCs's column pointer size
     colPtUmf.clear();
     colPtUmf.resize(nbCols_+1, Idx(0));
     // Because colPointer always begins with 0, so need to to count from the second position
     typename std::vector<Idx>::iterator itColPtUmf = colPtUmf.begin()+1;
     colPtUmf[0] = Idx(0);

     typename std::vector<M1>::const_iterator itbval = m1.begin()+1, itmDiag = itbval;
     typename std::vector<M1>::const_iterator itmLow = itbval + diagonalSize();
     typename std::vector<M1>::const_iterator itmUp  = itmLow + lowerPartSize();
     typename std::vector<M1>::const_iterator itbUpval, iteUpval, itUpval, itbLowval, itLowval;

     itbUpval = iteUpval = itmUp;
     itLowval = itbLowval =  itmLow;
     for (number_t colIdx = 0; colIdx < nbCols_; ++colIdx) {
         number_t nzCol = 0;

         // Process upper part
         if (0 != colIdx) {
             itbUpval = iteUpval;
             iteUpval = itbUpval + colIdx;

             itUpval = std::find_if(itbUpval, iteUpval, std::bind(std::not_equal_to<M1>(), std::placeholders::_1, M1(0)));
             if (itUpval != iteUpval) {
                 resultUmf.push_back(*itUpval);
                 rowIdxUmf.push_back(itUpval - itbUpval);
                 ++nzCol;
                 ++itUpval;
                 while (itUpval != iteUpval) {
                     itUpval = std::find_if(itUpval, iteUpval, std::bind(std::not_equal_to<M1>(), std::placeholders::_1, M1(0)));
                     if (itUpval != iteUpval) {
                         resultUmf.push_back(*itUpval);
                         rowIdxUmf.push_back(itUpval - itbUpval);
                         ++nzCol;
                         ++itUpval;
                     }
                 }
             }
         }

         // Process diagonal part
         if (M1(0) != *itmDiag) {
             resultUmf.push_back(*itmDiag);
             rowIdxUmf.push_back(itmDiag - itbval);
             ++nzCol;
         }
         ++itmDiag;

         if ((nbCols_-1) != (colIdx)) {

             for (number_t rowIdx = colIdx; rowIdx < nbRows_-1; rowIdx++)
             {
                 itLowval += rowIdx;
                 if (M1(0) != *(itLowval) ) {
                     resultUmf.push_back(*itLowval);
                     rowIdxUmf.push_back(rowIdx+1);
                     ++nzCol;
                 }

             }
             itLowval = itbLowval + colIdx + 1;
             itbLowval = itLowval;

         }

         // Update colPtUmf with number of non-zero value
         *itColPtUmf += *(itColPtUmf-1) + nzCol;
         ++itColPtUmf;
     }
 }

} // end of namespace xlifepp

