/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ColDenseStorage.cpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Implementation of xlifepp::ColDenseStorage class and functionnalities
*/

#include "ColDenseStorage.hpp"
#include <functional>

namespace xlifepp
{
/*=======================================================================================
  Constructors, destructor
  =======================================================================================*/
// default constructor
ColDenseStorage::ColDenseStorage(string_t id)
  : DenseStorage(_col, id) {}

// constructor for square structure (number of rows)
ColDenseStorage::ColDenseStorage(number_t nr, string_t id)
  : DenseStorage(_col, nr, id) {}

// constructor for rectangular structure (number of rows/columns)
ColDenseStorage::ColDenseStorage(number_t nr, number_t nc, string_t id)
  : DenseStorage(_col, nr, nc, id) {}

// // constructor from a SymmDenseStorage
// ColDenseStorage::ColDenseStorage(const SymmDenseStorage& sd)
// : DenseStorage(_col, sd.numberOfCols()) {}

// // constructor from a SymmDenseStorage
// ColDenseStorage::ColDenseStorage(const DualDenseStorage& dd)
// : DenseStorage(_col, dd.numberOfCols(), dd.numberOfColumns()) {}

/*--------------------------------------------------------------------------------
 access operator to position of (i,j) coefficients (return 0 if outside)
 This DIRECT access to (i,j) pointer SHOULD NOT be used in matrix computation
 where SEQUENTIAL access MUST ALWAYS be used and is only provided for tests.
--------------------------------------------------------------------------------*/
number_t ColDenseStorage::pos(number_t i, number_t j, SymType s) const
{
  if(i == 0 || i > nbRows_ || j == 0 || j > nbCols_) { return 0; }
  else { return ((j - 1) * nbRows_ + i); }
}

// access to submatrix positions, useful for assembling matrices
// return the vector of adresses (relative number in storage) stored by col
void ColDenseStorage::positions(const std::vector<number_t>& rows, const std::vector<number_t>& cols,
                                std::vector<number_t>& pos, bool errorOn, SymType sym) const
{
  number_t nbp = rows.size() * cols.size();
  if(pos.size() != nbp) { pos.resize(nbp); }
  std::vector<number_t>::iterator itp = pos.begin();
  for(std::vector<number_t>::const_iterator itr = rows.begin(); itr != rows.end(); itr++)
    for(std::vector<number_t>::const_iterator itc = cols.begin(); itc != cols.end(); itc++, itp++)
    { *itp = nbRows_ * (*itc - 1) + *itr; }
}

// get (row indices, adress) of col c in set [r1,r2] ## SymType not used ##
std::vector<std::pair<number_t, number_t> > ColDenseStorage::getCol(SymType s, number_t c, number_t r1, number_t r2) const
{
    number_t nbr=r2;
    if(nbr==0) nbr=nbRows_;
    std::vector<std::pair<number_t, number_t> > rows(nbr-r1+1);
    std::vector<std::pair<number_t, number_t> >::iterator itrow=rows.begin();
    number_t a=(c - 1) * nbRows_+r1;
    for(number_t i=r1;i<=nbr;i++, a++, itrow++) *itrow=std::make_pair(i,a);
    return rows;
}

// get (col indices, adress) of row r in set [c1,c2] ## SymType not used ##
std::vector<std::pair<number_t, number_t> > ColDenseStorage::getRow(SymType s,number_t r, number_t c1, number_t c2) const
{
    number_t nbc=c2;
    if(nbc==0) nbc=nbCols_;
    std::vector<std::pair<number_t, number_t> > cols(nbc-c1+1);
    std::vector<std::pair<number_t, number_t> >::iterator itcol=cols.begin();
    for(number_t j=c1;j<=nbc;j++, itcol++) *itcol=std::make_pair(j,(j - 1) * nbRows_+r);
    return cols;
}

// create a new scalar ColDense storage from current ColDense storage and submatrix sizes
ColDenseStorage* ColDenseStorage::toScalar(dimen_t nbr, dimen_t nbc)
{
    MatrixStorage* nsto=findMatrixStorage(stringId, _dense, _col, buildType_, true, nbr*nbRows_, nbc*nbCols_);
    if(nsto!=nullptr) return static_cast<ColDenseStorage*>(nsto);
    ColDenseStorage* cd = new ColDenseStorage(nbr*nbRows_, nbc*nbCols_, stringId);
    cd->buildType() = buildType_;
    cd->scalarFlag()=true;
    return cd;
}

/*--------------------------------------------------------------------------------
 template Matrix x Vector & Vector x Matrix multiplications and specializations
--------------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void ColDenseStorage::multMatrixVector(const std::vector<M>& m, V* vp, R* rp) const
{
  typename std::vector<M>::const_iterator itm = m.begin() + 1;
  V* vpe=vp+nbCols_;
  R* rpe=rp+nbRows_;
  columnMatrixVector(itm, vp, vpe, rp, rpe);
}

template<typename M, typename V, typename R>
void ColDenseStorage::multMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  trace_p->push("ColDenseStorage::multMatrixVector");
  typename std::vector<M>::const_iterator itm = m.begin() + 1;
  typename std::vector<V>::const_iterator itvb = v.begin(), itve = v.end();
  typename std::vector<R>::iterator itrb = r.begin(), itre = r.end();
  #ifndef XLIFEPP_WITH_OMP
  columnMatrixVector(itm, itvb, itve, itrb, itre);
  #else
  if(Environment::parallelOn()) parallelMultMatrixVector(m,v,r);
  else columnMatrixVector(itm, itvb, itve, itrb, itre);
  #endif // XLIFEPP_WITH_OMP
  trace_p->pop();
}

template<typename M, typename V, typename R>
void ColDenseStorage::multVectorMatrix(const std::vector<M>& m, V* vp, R* rp) const
{
  typename std::vector<M>::const_iterator itm = m.begin() + 1;
  V* vpe=vp+nbCols_;
  R* rpe=rp+nbRows_;
  columnVectorMatrix(itm, vp, vpe, rp, rpe);
}

template<typename M, typename V, typename R>
void ColDenseStorage::multVectorMatrix(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  trace_p->push("ColDenseStorage::multMatrixVector");
  typename std::vector<M>::const_iterator itm = m.begin() + 1;
  typename std::vector<V>::const_iterator itvb = v.begin(), itve = v.end();
  typename std::vector<R>::iterator itrb = r.begin(), itre = r.end();
  #ifndef XLIFEPP_WITH_OMP
  columnVectorMatrix(itm, itvb, itve, itrb, itre);
  #else
  if(Environment::parallelOn()) parallelColumnVectorMatrix(itm, itvb, itve, itrb, itre);
  else columnVectorMatrix(itm, itvb, itve, itrb, itre);
  #endif // XLIFEPP_WITH_OMP
  trace_p->pop();
}

// parallel Matrix * Vector
template<typename M, typename V, typename R>
void ColDenseStorage::parallelMultMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  #ifdef XLIFEPP_WITH_OMP
    trace_p->push("ColDenseStorage::parallelMultMatrixVector");
    number_t nr=nbOfRows(), nc=nbOfColumns();
    number_t nt = 1;
    #pragma omp parallel for lastprivate(nt)
    for (number_t i = 0; i < 1; i++)
      {nt = omp_get_num_threads();}

    number_t dc = nc/nt;
    if(dc==0 || nt==1 || !Environment::parallelOn()) //do not use parallel computation
    {
        typename std::vector<M>::const_iterator itm = m.begin() + 1;
        typename std::vector<V>::const_iterator itvb = v.begin(), itve = v.end();
        typename std::vector<R>::iterator itrb = r.begin(), itre = r.end();
        columnMatrixVector(itm, itvb, itve, itrb, itre);
        trace_p->pop();
        return;
    }
    std::vector<std::vector<R> > res(nt, std::vector<R>(nr,0.*(m[1]*v[0])));  //nt temporary res vector
    //partial product in parallel
    #pragma omp parallel for
    for(number_t t=0; t<nt; ++t)
    {
        typename std::vector<M>::const_iterator itm = m.begin() + ( 1 + t*dc*nr);
        typename std::vector<R>::iterator itrb = res[t].begin(), itre = res[t].end(), itr;
        typename std::vector<V>::const_iterator itvb = v.begin()+ t*dc, itve = itvb + dc, itv;
        if(t==nt-1) itve=v.end();
        for(itv = itvb; itv != itve; ++itv)
          for(itr = itrb; itr != itre; ++itr, ++itm)  *itr += *itm * *itv;
    }
    //accumulate in serial
    typename std::vector<R>::iterator itrb = r.begin(), itre = r.end(), itr;
    typename std::vector<R>::iterator itrp;
    for(itr = itrb; itr != itre; ++itr) { *itr *= 0; }
    for(number_t t=0; t<nt; ++t)
    {
        itrp = res[t].begin();
        for(itr=itrb; itr!=itre; ++itr, ++itrp) *itr+=*itrp;
    }
    trace_p->pop();
  #endif // XLIFEPP_WITH_OMP
}
/*
--------------------------------------------------------------------------------
 print ColDense matrix to ostream, according to type of values of matrix
 printScalarEntries is defined in DenseStorage class
--------------------------------------------------------------------------------
*/
void ColDenseStorage::printEntries(std::ostream& os, const std::vector<real_t>& m, number_t vb, const SymType sym) const
{
  std::vector<real_t>::const_iterator itm = m.begin() + 1;
  printScalarEntries(itm, nbCols_, nbRows_, entriesPerRow,
                     entryWidth, entryPrec, "col", vb, os);
}

void ColDenseStorage::printEntries(std::ostream& os, const std::vector<complex_t>& m, number_t vb, const SymType sym) const
{
  std::vector<complex_t>::const_iterator itm = m.begin() + 1;
  printScalarEntries(itm, nbCols_, nbRows_, entriesPerRow / 2,
                     2 * entryWidth + 1, entryPrec, "col", vb, os);
}

void ColDenseStorage::printEntries(std::ostream& os, const std::vector< Vector<real_t> >& m, number_t vb, const SymType sym) const
{error("storage_novector", "ColDenseStorage::printEntries");}

void ColDenseStorage::printEntries(std::ostream& os, const std::vector< Vector<complex_t> >& m, number_t vb, const SymType sym) const
{error("storage_novector", "ColDenseStorage::printEntries");}

void ColDenseStorage::printEntries(std::ostream& os, const std::vector< Matrix<real_t> >& m, number_t vb, const SymType sym) const
{
  std::vector< Matrix<real_t> >::const_iterator itm = m.begin() + 1;
  printMatrixEntries(itm, nbCols_, nbRows_, "col", vb, os);
}
void ColDenseStorage::printEntries(std::ostream& os, const std::vector< Matrix<complex_t> >& m, number_t vb, const SymType sym) const
{
  std::vector< Matrix<complex_t> >::const_iterator itm = m.begin() + 1;
  printMatrixEntries(itm, nbCols_, nbRows_, "col", vb, os);
}

// Set the Diagonal with a specific value
template<typename T>
void ColDenseStorage::setDiagValueColDense(std::vector<T>& v, const T m)
{
  typename std::vector<T>::iterator it = v.begin() + 1;
  number_t diagSize = diagonalSize() + 1;
  *it = m;
  for (number_t k = 1; k < (diagSize - 1); k++) { it += diagSize; *it = m; }
}

/*!
   Add two matrices
   \param m1 vector values_ of first matrix
   \param m2 vector values_ of second matrix
   \param r vector values_ of result matrix
 */
template<typename M1, typename M2, typename R>
void ColDenseStorage::addMatrixMatrix(const std::vector<M1>& m1, const std::vector<M2>& m2, std::vector<R>& r) const
{
  trace_p->push("ColDenseStorage::addMatrixMatrix");
  typename std::vector<M1>::const_iterator itm1 = m1.begin() + 1;
  typename std::vector<M2>::const_iterator itm2 = m2.begin() + 1;
  typename std::vector<R>::iterator itrb = r.begin() + 1, itre = r.end();
  sumMatrixMatrix(itm1, itm2, itrb, itre);
  trace_p->pop();
}

/*-----------------------------------------------------------------------
              triangular part matrix * vector
--------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void ColDenseStorage::lowerMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    r.assign(nbRows_,R(0));  //to reset to 0
    typename std::vector<M>::const_iterator itmb=m.begin()+1, itm;
    typename std::vector<R>::iterator itr;
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for firstprivate(itr,itm) schedule(dynamic)
    #endif // XLIFEPP_WITH_OMP
    for(number_t j=0;j<std::min(nbRows_,nbCols_); j++)
       {
           V vj=v[j];
           itr=r.begin()+j;
           itm=itmb+j*nbRows_+j;
           for(number_t i=j;i<nbRows_;i++, ++itr, ++itm) *itr+= *itm * vj;
       }
}

template<typename M, typename V, typename R>
void ColDenseStorage::lowerD1MatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    r.assign(nbRows_,R(0));  //to reset to 0
    typename std::vector<M>::const_iterator itmb=m.begin()+1, itm;
    typename std::vector<R>::iterator itr;
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for firstprivate(itr,itm) schedule(dynamic)
    #endif // XLIFEPP_WITH_OMP
    for(number_t j=0;j<std::min(nbRows_,nbCols_); j++)
       {
           V vj=v[j];
           itr=r.begin()+j;
           *itr+=vj; itr++;
           itm=itmb+j*nbRows_+j+1;
           for(number_t i=j+1;i<nbRows_;i++, ++itr, ++itm) *itr+= *itm * vj;
       }
}

template<typename M, typename V, typename R>
void ColDenseStorage::upperMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    r.assign(nbRows_,R(0));  //to reset to 0
    typename std::vector<M>::const_iterator itmb=m.begin()+1, itm;
    typename std::vector<R>::iterator itr;
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for firstprivate(itr,itm) schedule(dynamic)
    #endif // XLIFEPP_WITH_OMP
    for(number_t j=0;j<nbCols_; j++)
       {
           V vj=v[j];
           itr=r.begin();
           itm=itmb+j*nbRows_;
           for(number_t i=0;i<=std::min(j,nbRows_-1);i++, ++itr, ++itm) *itr+= *itm * vj;
       }
}

template<typename M, typename V, typename R>
void ColDenseStorage::upperD1MatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    r.assign(nbRows_,R(0));  //to reset to 0
    typename std::vector<M>::const_iterator itmb=m.begin()+1, itm;
    typename std::vector<R>::iterator itr;
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for firstprivate(itr,itm) schedule(dynamic)
    #endif // XLIFEPP_WITH_OMP
    for(number_t j=0;j<nbCols_; j++)
       {
           V vj=v[j];
           itr=r.begin();
           itm=itmb+j*nbRows_;
           for(number_t i=0;i<std::min(j,nbRows_);i++, ++itr, ++itm) *itr+= *itm * vj;
           if(j<nbRows_) *itr+=vj;
       }
}

template<typename M, typename V, typename R>
void ColDenseStorage::diagonalMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
    else r.resize(nbRows_);
    typename std::vector<M>::const_iterator itm=m.begin()+1;
    typename std::vector<V>::const_iterator itv=v.begin();
    typename std::vector<R>::iterator itr=r.begin();
    for(number_t i=0;i<std::min(nbRows_,nbCols_);i++, ++itv,++itr, itm+=nbRows_) *itr = *itm * *itv;
}

/*-----------------------------------------------------------------------
                              UMFPack stuff
--------------------------------------------------------------------------*/
/*!
   Extract and convert matrix storage to UMFPack format (Matlab sparse matrix)
   \param m1 vector values_ current matrix
   \param colPtUmf vector column Pointer of UMFPack format
   \param rowIdxUmf vector row Index of UMFPack format
   \param resultUmf vector values of UMFPack format
 */
template<typename M1, typename Idx>
void ColDenseStorage::toUmfPack(const std::vector<M1>& m1, std::vector<Idx>& colPtUmf, std::vector<Idx>& rowIdxUmf, std::vector<M1>& resultUmf) const
{
    // Reserve memory for resultUmf and set its size to 0
    resultUmf.reserve(m1.size());
    resultUmf.clear();

    // Reserve memory for rowIdxUmf and set its size to 0
    rowIdxUmf.reserve(m1.size());
    rowIdxUmf.clear();

    // Resize column pointer as dualCs's column pointer size
    colPtUmf.clear();
    colPtUmf.resize(nbCols_+1, Idx(0));
    // Because colPointer always begins with 0, so need to to count from the second position
    typename std::vector<Idx>::iterator itColPtUmf = colPtUmf.begin()+1;
    colPtUmf[0] = Idx(0);

    typename std::vector<M1>::const_iterator itbval = m1.begin()+1;
    typename std::vector<M1>::const_iterator itval, itbrange, iterange;

    for (number_t colIdx = 0; colIdx < nbCols_; ++colIdx) {
        number_t nzCol = 0;
        itval = itbrange = itbval + colIdx*nbRows_;
        iterange = itbrange + nbRows_;
        itval = std::find_if(itbrange, iterange, std::bind(std::not_equal_to<M1>(), std::placeholders::_1, M1(0)));
        if (itval != iterange) {
            resultUmf.push_back(*itval);
            rowIdxUmf.push_back(itval - itbrange);
            ++nzCol;
            ++itval;
            while (itval != iterange) {
                itval = std::find_if(itval, iterange, std::bind(std::not_equal_to<M1>(), std::placeholders::_1, M1(0)));
                if (itval != iterange){
                    resultUmf.push_back(*itval);
                    rowIdxUmf.push_back(itval - itbrange);
                    ++nzCol;
                    ++itval;
                }

            }
        }

        // Update colPtUmf with number of non-zero value
        *itColPtUmf += *(itColPtUmf-1) + nzCol;
        ++itColPtUmf;
    }
}

} // end of namespace xlifepp

