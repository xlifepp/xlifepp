/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DenseStorage.cpp
  \authors D. Martin, E. Lunéville, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Implementation of xlifepp::DenseStorage class and functionnalities
*/

#include "DenseStorage.hpp"
#include "SymDenseStorage.hpp"
#include "DualDenseStorage.hpp"

namespace xlifepp
{
/*=======================================================================================
  Constructors, destructor
  =======================================================================================*/
// default constructor
DenseStorage::DenseStorage(string_t id) : MatrixStorage(_dense, _noAccess, id) {}

// construct DenseStorage giving access type
DenseStorage::DenseStorage(AccessType at, string_t id)
  : MatrixStorage(_dense, at, id) {}

// construct DenseStorage giving access type and size (square matrix)
DenseStorage::DenseStorage(AccessType at, number_t nr, string_t id)
  : MatrixStorage(_dense, at, nr, nr, id) {}

// construct DenseStorage giving access type and sizes (rectangular matrix)
DenseStorage::DenseStorage(AccessType at, number_t nr, number_t nc, string_t id)
  : MatrixStorage(_dense, at, nr, nc, id) {}

//create a new dual dense storage from sym dense storage
MatrixStorage* DenseStorage::toDual()
{
  if(accessType_!=_sym)
  {
    where("DenseStorage::DenseStorage");
    error("symmetric_only"); return nullptr;
  }
  SymDenseStorage* scs=dynamic_cast<SymDenseStorage*>(this);  //downcast
  if(scs==nullptr)
  {
    where("DenseStorage::DenseStorage");
    error("downcast_failure", "SymDenseStorage"); return nullptr;
  }
  return new DualDenseStorage(nbRows_, nbCols_);
}

// check if two storages have the same structures
bool DenseStorage::sameStorage(const MatrixStorage& sto) const
{
   return (sto.storageType()==storageType_ &&
           sto.accessType()==accessType_ &&
           sto.nbOfRows()==nbRows_ &&
           sto.nbOfColumns()==nbCols_&&
           sto.size()==size());
}

/*--------------------------------------------------------------------------------
  load a real or complex scalar matrix from file in dense or coordinate format
  general code for any dense storage, use pos(i,j) function (not optimized)
  matrix of matrices are not taken into account! may be in future ...
--------------------------------------------------------------------------------*/
void DenseStorage::loadFromFileDense(std::istream& ifs, std::vector<real_t>& mat, SymType sym, bool realAsCmplx)
{
  // read reals, realAsCmplx not used
  mat.resize(nbRows_ * nbCols_ + 1, 0.);
  real_t r;
  for(number_t i = 1; i <= nbRows_; i++)
    for(number_t j = 1; j <= nbCols_; j++)
    {
      ifs >> r;
      if(sym == _noSymmetry || (sym != _noSymmetry && i >= j)) { mat[pos(i, j)] = r; }
    }
}

void DenseStorage::loadFromFileDense(std::istream& ifs, std::vector<complex_t>& mat, SymType sym, bool realAsCmplx)
{
  // casting from real to complex if realAsCmplx is true
  mat.resize(nbRows_ * nbCols_ + 1, complex_t(0., 0.));
  real_t r1, r2 = 0.;
  for(number_t i = 1; i <= nbRows_; i++)
    for(number_t j = 1; j <= nbCols_; j++)
    {
      ifs >> r1;
      if(!realAsCmplx) { ifs >> r2; }
      if(sym == _noSymmetry || (sym != _noSymmetry && i >= j)) { mat[pos(i, j)] = complex_t(r1, r2); }
    }
}

void DenseStorage::loadFromFileCoo(std::istream& ifs, std::vector<real_t>& mat, SymType sym, bool realAsCmplx)
{
  // read reals, realAsCmplx not used
  mat.resize(nbRows_ * nbCols_ + 1, 0.);
  number_t i, j;
  real_t r;
  while(!ifs.eof())
  {
    ifs >> i >> j >> r;
    if(sym == _noSymmetry || (sym != _noSymmetry && i >= j)) { mat[pos(i, j)] = r; }
  }
}

void DenseStorage::loadFromFileCoo(std::istream& ifs, std::vector<complex_t>& mat, SymType sym, bool realAsCmplx)
{
  // casting from real to complex if realAsCmplx is true
  mat.resize(nbRows_ * nbCols_ + 1, complex_t(0., 0.));
  number_t i, j;
  real_t r1, r2 = 0.;
  while(!ifs.eof())
  {
    ifs >> i >> j >> r1;
    if(!realAsCmplx) { ifs >> r2; }
    if(sym == _noSymmetry || (sym != _noSymmetry && i >= j)) { mat[pos(i, j)] = complex_t(r1, r2); }
  }
}

//! get row indices of col c in range [r1,r2] (present for consitancy)
std::set<number_t> DenseStorage::getRows(number_t c, number_t r1, number_t r2) const
{
    std::set<number_t> rowset;
    number_t nbr=r2;
    if(nbr==0) nbr=nbRows_;
    if(nbr<r1) return rowset;
    for(number_t r = r1; r<=nbr; r++) rowset.insert(r);
    return rowset;
}

//! get col indices of row r in set [c1,c2], (present for consitancy)
std::set<number_t> DenseStorage::getCols(number_t r, number_t c1, number_t c2) const
{
    std::set<number_t> colset;
    number_t nbc=c2;
    if(nbc==0) nbc=nbCols_;
    if(nbc<c1) return colset;
    for(number_t c = c1; c<=nbc; c++) colset.insert(c);
    return colset;
}

//! get col indices of row r in set [c1,c2], faster (no allocation, no check)
void DenseStorage::getColsV(std::vector<number_t>& colsv, number_t& nbcols, number_t r, number_t c1, number_t c2 ) const
{
    number_t cf=c2;
    if(cf==0) cf=nbCols_;
    nbcols=0;
    if(cf<c1) return;
    std::vector<number_t>::iterator itv=colsv.begin();
    for(number_t c = c1; c<=cf; c++, itv++, nbcols++) *itv=c;
}

//! get row indices of col c in set [c1,c2], faster (no allocation, no check)
void DenseStorage::getRowsV(std::vector<number_t>& rowsv, number_t& nbrows, number_t c, number_t r1, number_t r2 ) const
{
    number_t rf=r2;
    if(rf==0) rf=nbRows_;
    nbrows=0;
    if(rf<r1) return;
    std::vector<number_t>::iterator itv=rowsv.begin();
    for(number_t r = r1; r<=rf; r++, itv++, nbrows++) *itv=r;
}

//regarding the number of threads, produce a vector of row/col index [0,i_1,...i_n]
//to balance rows or cols of the lower/upper triangular part of dense matrix
//if the matrix is too small or maximum of threads is 1, nothing is done
// Note it is assumed hat the matrix is a square matrix (nbOfRows=nbOfColumns)
void DenseStorage::extractThreadIndex(MatrixPart lowup, number_t& nt, std::vector<number_t>& threadIndex ) const
{
  #ifdef XLIFEPP_WITH_OMP
    number_t numt = 1;
    #pragma omp parallel for lastprivate(numt)
    for (number_t i = 0; i < 1; i++) {numt = omp_get_num_threads();}
    number_t granularity = 1;
    numt*=granularity;
    nt=numt;
    //balance rows
    number_t nr, nc, nnz;
    if(lowup == _lower) { nr=nbOfRows(); nc=nbOfColumns(); nnz= lowerPartSize();}
    else                { nr=nbOfColumns();  nc=nbOfRows(); nnz= upperPartSize();}
    number_t d = nnz/nt;
    if(d==0 || nt==1) {nt=1;return;} //two small problem, do not use parallel computation

    //split row/col index in pieces of size roughly equal to d
    threadIndex.resize(nt+1);
    number_t k=0;
    number_t row=1;
    bool cont=true;
    while(k<=nt && cont)
    {
        number_t n=0;
        while(n<d && row<nr) {n+=std::min(row,nc);++row;} //find next row giving a set of rows/cols of size r d
        threadIndex[++k]=row-1;
        if(row==nr) cont=false;
    }
    nt=k;
    threadIndex.resize(nt+1); //fit threadIndex
    threadIndex[k]= nr-1;    //force the last row to be the last row/col index
  #endif
}

/* -------------------------------------------------------------------------------------------------------
                               generic Gauss solver (not fast)
   -------------------------------------------------------------------------------------------------------*/
/*! generic Gauss solver with row pivoting strategy  A*x1=b1, A*x2=b2, ...
   A: square matrix
   b: right hand sides and solutions
   ### does not work for symmetric dense storage ###
*/
template<typename T>
void DenseStorage::gaussSolverG(std::vector<T>& A, std::vector<std::vector<T> >& b) const
{
  if (accessType_==_sym)
  {
    where("DenseStorage:::gaussSolverG(...)");
    error("access_unexpected", words("access type",_dual), words("access type",_sym));
  }
  std::vector<number_t> P(nbRows_);
  std::vector<number_t>::iterator itp=P.begin();
  for(number_t i=1; i<=nbRows_; i++,itp++) *itp=i;
  typename std::vector<std::vector<T> >::iterator itb, itx;
  number_t q;
  T piv, v;
  real_t s,t;
  bool show_status = (nbRows_ > 1000 && theVerboseLevel > 0);
  if (show_status) std::cout<<"   in generic dense Gauss solver, "<<numberOfThreads()<<" threads: "<<std::flush;

  //Gauss elimination
  for (number_t k=1; k<nbRows_; k++) //main loop
  {
    q=pos(P[k-1],k);
    s=std::abs(A[q]);
    number_t l=k;
    for(number_t j=k+1; j<=nbRows_; j++)
      {
        q=pos(P[j-1],k); t=std::abs(A[q]);
        if(t > s) {l=j; s=t;}
      }
    if (s<theTolerance)
    {
      where("DenseStorage:::gaussSolverG(...)");
      error("small_pivot");
    }
    if (l!=k) {q=P[l-1]; P[l-1]=P[k-1]; P[k-1]=q;} //pivoting rows k and l

    piv=A[pos_(P[k-1],k)];
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for firstprivate(q, v, itb) schedule(dynamic)
    #endif // XLIFEPP_WITH_OMP
    for (number_t i=k+1; i<=nbRows_; i++)
    {
      q=pos_(P[i-1],k); v=A[q]/piv;
      A[q]=0;
      number_t pi=P[i-1], pk=P[k-1];
      for(number_t j=k+1; j<=nbRows_; j++)  A[pos_(pi,j)]-=v*A[pos_(pk,j)];
      for(itb=b.begin(); itb!=b.end(); itb++)(*itb)[pi-1]-=v*(*itb)[pk-1];
    }  //end loop i

    if (show_status && nbRows_>10) //progress status
    {
      number_t  p = k % (nbRows_/10);
      if(p == 0) std::cout<<(k/(nbRows_/10))<<"0% "<<std::flush;
    }
  }//end loop k

  //solve triangular system
  for (number_t i=nbRows_; i>=1; i--)
  {
    number_t pi=P[i-1];
    for (number_t j=i+1; j<=nbRows_; j++)
    {
      v=A[pos_(pi,j)];
      for(itb=b.begin(); itb!=b.end(); itb++)(*itb)[pi-1]-=v*(*itb)[P[j-1]-1];
    }
    v=A[pos_(P[i-1],i)];
    for (itb=b.begin(); itb!=b.end(); itb++)(*itb)[P[i-1]-1]/=v;
  }

  //apply inverse permutation to rhs's
  typename std::vector<T>::iterator itv;
  for (itb=b.begin(); itb!=b.end(); itb++)
  {
    std::vector<T> bc=*itb;   //copy B
    itp=P.begin();
    for (itv=itb->begin(); itv!=itb->end(); ++itv, ++itp) *itv = bc[*itp-1];
  }

  if (show_status) std::cout<<"done "<<eol<<std::flush; //progress status
}

/*! generic Gauss solver with row pivoting strategy  A*x=b
    A: square matrix
    b: right hand sides and solutions
    ### does not work for symmetric dense storage ###
*/
template<typename T>
void DenseStorage::gaussSolverG(std::vector<T>& A, std::vector<T>& b) const
{
  std::vector<std::vector<T> > bs(1,b);
  gaussSolverG(A,bs);
  b=bs[0];
}

/* -------------------------------------------------------------------------------------------------------
                               generic LU factorization (not fast)
   -------------------------------------------------------------------------------------------------------*/
/*! generic LU factorisation with with real row pivoting strategy  PA = LU
    ### does not work for symmetric dense storage ###
    A: square matrix
    LU: the factorized matrix
    P: the row permutation vector (index starts from 0)
    be care: solving A*X=B is equivalent to solving P*A*X=P*B <=> L*U*X=PB, the product by P has to be done before calling triangular part solvers!
             product B=A*X is equivalent to product B=inv(P)*L*U*X, the product by inv(P) has to be done after calling triangular part product!
             product B=X*A is equivalent to product B=X*inv(P)*L*U=tr(tr(U)*tr(L)*P*X), the product by P has to be done before calling triangular part product!
            this products are not taken into account by storage class !
*/
template<typename T>
void DenseStorage::luG(std::vector<T>& A, std::vector<T>& LU, std::vector<number_t>& p) const
{
  if (accessType_==_sym)
  {
    where("DenseStorage::luG(...)");
    error("access_unexpected", words("access type",_dual), words("access type",_sym));
  }

  if (&A!=&LU) LU=A;   //recopy A to LU to work only with LU matrix
  p.resize(nbRows_);
  std::vector<number_t>::iterator itp=p.begin();
  for (number_t i=0; i<nbRows_; i++,itp++) *itp=i;
  number_t q;
  T piv, v;
  real_t s,t;
  bool show_status = (nbRows_ > 1000 && theVerboseLevel > 0);
  if (show_status) std::cout<<"   in generic dense pivoting LU factorization, "<<numberOfThreads()<<" threads: "<<std::flush;

  //Gauss elimination
  for (number_t k=1; k<nbRows_; k++) //main loop
  {
    s=std::abs(LU[pos(k,k)]);
    number_t l=k;
    for (number_t i=k+1; i<=nbRows_; i++)
    {
      t=std::abs(LU[pos(i,k)]);
      if(t > s) {l=i; s=t;}
    }
    if (s<theTolerance)
    {
      where("DenseStorage::luG(...)");
      error("small_pivot");
    }
    if (l!=k) //pivoting rows k and l
    {
      q=p[l-1]; p[l-1]=p[k-1]; p[k-1]=q;
      number_t pk, pl;
      for (number_t j=1; j<=nbCols_; j++) //real swap
      {
        pk=pos(k,j); pl=pos(l,j);
        v=LU[pk]; LU[pk]=LU[pl]; LU[pl]=v;
      }
    }
    piv=LU[pos_(k,k)];
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for firstprivate(q, v) schedule(dynamic)
    #endif // XLIFEPP_WITH_OMP
    for (number_t i=k+1; i<=nbRows_; i++)
    {
      q=pos(i,k); v=LU[q]/=piv;
      for(number_t j=k+1; j<=nbCols_; j++) LU[pos_(i,j)]-=v*LU[pos_(k,j)];
    }  //end loop i

    if (show_status && nbRows_>10) //progress status
    {
      number_t  p = k % (nbRows_/10);
      if (p == 0) std::cout<<(k/(nbRows_/10))<<"0% "<<std::flush;
    }
  }//end loop k

  if (show_status) std::cout<<"done "<<eol<<std::flush; //progress status
}

/*! generic LU factorisation with with no pivoting strategy  A = LU
   A: square matrix
   LU: the factorized matrix
   ### does not work for symmetric storage ###
*/
template<typename T>
void DenseStorage::luG(std::vector<T>& A, std::vector<T>& LU) const
{
  if (accessType_==_sym)
  {
    where("DenseStorage::luG(...)");
    error("access_unexpected", words("access type",_dual), words("access type",_sym));
  }

  if (&A!=&LU) LU=A;   //recopy A to LU to work only with LU matrix
  number_t q;
  T piv, v;
  bool show_status = (nbRows_ > 1000 && theVerboseLevel > 0);
  if (show_status) std::cout<<"   in generic dense pivoting LU factorization, "<<numberOfThreads()<<" threads: "<<std::flush;

  //Gauss elimination
  for (number_t k=1; k<nbRows_; k++) //main loop
  {
    piv=LU[pos_(k,k)];
    if (std::abs(piv)<theTolerance)
    {
      where("DenseStorage::luG(...)");
      error("small_pivot");
    }

    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for firstprivate(q, v) schedule(dynamic)
    #endif // XLIFEPP_WITH_OMP
    for (number_t i=k+1; i<=nbRows_; i++)
    {
      q=pos_(i,k); v=LU[q]/=piv;
      for(number_t j=k+1; j<=nbCols_; j++) LU[pos_(i,j)]-=v*LU[pos_(k,j)];
    }  //end loop i

    if (show_status && nbRows_>10) //progress status
    {
      number_t  p = k % (nbRows_/10);
      if(p == 0) std::cout<<(k/(nbRows_/10))<<"0% "<<std::flush;
    }
  }//end loop k

  if (show_status) std::cout<<"done "<<eol<<std::flush; //progress status
}
    //! specializations of gaussSolver and LU factorization thats call generic functions
void DenseStorage::gaussSolver(std::vector<real_t>& A, std::vector<std::vector<real_t> >& bs) const
    {gaussSolverG<>(A,bs);}
void DenseStorage::gaussSolver(std::vector<complex_t>& A, std::vector<std::vector<complex_t> >& bs) const
    {gaussSolverG<>(A,bs);}
void DenseStorage::gaussSolver(std::vector<real_t>& A, std::vector<real_t>& b) const
    {gaussSolverG<>(A,b);}
void DenseStorage::gaussSolver(std::vector<complex_t>& A, std::vector<complex_t>& b) const
    {gaussSolverG<>(A,b);}
void DenseStorage::lu(std::vector<real_t>& A, std::vector<real_t>& LU, std::vector<number_t>& p, const SymType sym) const
    {luG<>(A, LU, p);}
void DenseStorage::lu(std::vector<complex_t>& A, std::vector<complex_t>& LU, std::vector<number_t>& p, const SymType sym) const
    {luG<>(A, LU, p);}
void DenseStorage::lu(std::vector<real_t>& A, std::vector<real_t>& LU, const SymType sym) const
    {luG<>(A, LU);}
void DenseStorage::lu(std::vector<complex_t>& A, std::vector<complex_t>& LU, const SymType sym) const
    {luG<>(A, LU);}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
template void DenseStorage::gaussSolverG(std::vector<real_t>& A, std::vector<real_t>& b) const;
template void DenseStorage::gaussSolverG(std::vector<complex_t>& A, std::vector<complex_t>& b) const;
template void DenseStorage::luG(std::vector<real_t>& A, std::vector<real_t>& LU, std::vector<number_t>& p) const;
template void DenseStorage::luG(std::vector<complex_t>& A, std::vector<complex_t>& LU, std::vector<number_t>& p) const;
template void DenseStorage::luG(std::vector<real_t>& A, std::vector<real_t>& LU) const;
template void DenseStorage::luG(std::vector<complex_t>& A, std::vector<complex_t>& LU) const;
#endif

} // end of namespace xlifepp

