/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file LargeMatrixAdapterInverseGeneralized.hpp
  \author Manh Ha NGUYEN
  \since 23 April 2014
  \date  23 April 2014

  \brief Definition of the xlifepp::LargeMatrixAdapterInverseGeneralized class
  
  xlifepp::LargeMatrixAdapterInverseGeneralized which is used in EigenSolver. This class is inherited from the virtual class xlifepp::Operator.
  This class, in fact, is an interface between the class LargeMatrix and the class xlifepp::Operator
  This class contains function multInverMatrixVector in order to do: A^-1*X=Y
*/

#ifndef LARGEMATRIX_ADAPTER_INVERSE_GENERALIZED_HPP
#define LARGEMATRIX_ADAPTER_INVERSE_GENERALIZED_HPP

#include "utils.h"
#include "MultiVectorAdapter.hpp"
#include "../../eigenSolvers/eigenSparse/XlifeppMultiVec.hpp"
#include "../../eigenSolvers/eigenSparse/XlifeppOperator.hpp"

namespace xlifepp {

  /*!
    \class LargeMatrixAdapterInverseGeneralized
    \brief Template specialization of Operator class using the Operator virtual base class and
    LargeMatrix class.

    This interface will ensure that any Operator and MultVec will be accepted by the templated solvers.
  */
  template<typename MatrixType, typename ScalarType>
  class LargeMatrixAdapterInverseGeneralized : public Operator<ScalarType>
  {
  private:
      typedef const MatrixType LMatrix;
  public:
      LargeMatrixAdapterInverseGeneralized(LMatrix* pA, LMatrix* pB, FactorizationType fac) : lMatrixA_p(pA), lMatrixB_p(pB), fac_(fac) {}

//      LargeMatrixAdapterInverseGeneralized& operator=(LMatrix matrix) { lMatrix_p = &matrix; fac_ = _noFactorization; return (*this); }


      //! Destructor.
      virtual ~LargeMatrixAdapterInverseGeneralized() {}

      void apply(const MultiVec<ScalarType>& x, MultiVec<ScalarType>& y) const
      {
          Vector<ScalarType> vecTemp(x.getVecLength());
          for (int i = 0; i< x.getNumberVecs(); ++i) {
              multMatrixVector(*lMatrixB_p, *(x[i]), vecTemp);
              multInverMatrixVector(*lMatrixA_p, vecTemp, *(y[i]), fac_);
          }
      }

  private:
      LMatrix*  lMatrixA_p;
      LMatrix*  lMatrixB_p;
      FactorizationType fac_;
  };

} // end of xlifepp namespace

#endif /* LARGEMATRIX_ADAPTER_INVERSE_GENERALIZED_HPP */

