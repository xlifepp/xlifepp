/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file LargeMatrixAdapter.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  03 April 2014

  \brief Definition of the xlifepp::LargeMatrixAdapter class
  
  xlifepp::LargeMatrixAdapter which is used in xlifepp::EigenSolver. This class is inherited from the virtual class xlifepp::Operator.
  This class, in fact, is an interface between the class xlifepp::LargeMatrix and the class xlifepp::Operator
*/

#ifndef LARGEMATRIX_ADAPTER_HPP
#define LARGEMATRIX_ADAPTER_HPP

#include "utils.h"
#include "MultiVectorAdapter.hpp"
#include "../../eigenSolvers/eigenSparse/XlifeppMultiVec.hpp"
#include "../../eigenSolvers/eigenSparse/XlifeppOperator.hpp"

namespace xlifepp {

  /*!
    \class LargeMatrixAdapter
    \brief Template specialization of Operator class using the Operator virtual base class and
    LargeMatrix class.

    This interface will ensure that any Operator and MultVec will be accepted by the templated solvers.
  */
  template<typename MatrixType, typename ScalarType>
  class LargeMatrixAdapter : public Operator<ScalarType>
  {
  private:
      typedef const MatrixType LMatrix;
  public:
//      LargeMatrixAdapter() { lMatrix_p = nullptr; }
      LargeMatrixAdapter(LMatrix* p) { lMatrix_p = p; }

      LargeMatrixAdapter& operator=(LMatrix matrix) { lMatrix_p = &matrix; return (*this); }


      //! Destructor.
      virtual ~LargeMatrixAdapter() {}

      void apply(const MultiVec<ScalarType>& x, MultiVec<ScalarType>& y) const
      {
          for (int i = 0; i< x.getNumberVecs(); ++i) {
              //multMatrixVector(*lMatrix_p, *(x[i]), *(y[i]));
              *(y[i]) = *lMatrix_p * *(x[i]);
          }
      }

      // This function exists for the testing purpose
      template<typename Scalar,typename ScalarTypeX, typename MatrixTypeX>
      friend void multMatVecLargeMatrixAdapter(const LargeMatrixAdapter<MatrixTypeX, Scalar>& m,
              const MultiVec<ScalarTypeX>& x,
              MultiVec<typename Conditional<NumTraits<Scalar>::IsComplex, Scalar, ScalarTypeX>::type >& y);

  private:
      LMatrix*  lMatrix_p;
  };
  
  template<typename Scalar,typename ScalarTypeX, typename MatrixType>
  void multMatVecLargeMatrixAdapter(const LargeMatrixAdapter<MatrixType, Scalar>& m,
                                    const MultiVec<ScalarTypeX>& x,
                                    MultiVec<typename Conditional<NumTraits<Scalar>::IsComplex, Scalar, ScalarTypeX>::type >& y)
  {
      for (int i = 0; i< x.getNumberVecs(); ++i) {
//          multMatrixVector(*(m.lMatrix_p), *(x[i]), *(y[i]));
          *(y[i]) = *(m.lMatrix_p) * *(x[i]);
      }
  }

} // end of xlifepp namespace

#endif /* LARGEMATRIX_ADAPTER_HPP */

