/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file MultiVectorAdapter.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  03 June 2013

  \brief Definiiton of the xlifepp::MultiVecAdapter class
  
  Multivector which is used in EigenSolver. This class is inherited from the virtual class xlifepp::MultiVec.
  Multivector, as its name, it's a collection of std::vector.
*/

#ifndef MULTI_VECTOR_HPP
#define MULTI_VECTOR_HPP

#include "utils.h"
#include "../../eigenSolvers/eigenSparse/EigenTestError.hpp"
#include "../../eigenSolvers/eigenSparse/XlifeppMultiVec.hpp"

namespace xlifepp
{

/*!
 * \class MultiVecAdapter
 *  This class implements all the virtual functions of its ancestor \class MultiVec
 *  By using the pointer to each vector in MultiVecAdapter, we can create objects
 *  which share a same view of memory (share same storage, like Storage in library LargeMatrix)
 *  However, these implementation aren't counted for parallel programming and maybe we need to change them in the future
 *  to make the class work with MPI?
 *
 */
template <class ScalarType>
class MultiVecAdapter : public MultiVec<ScalarType>
{
public:
    typedef std::vector<ScalarType>* VectorPointer;

  //! Constructor for a \c numberVecs vectors of length \c length.
  MultiVecAdapter(const number_t length, const dimen_t numberVecs) :
    length_(length),
    numberVecs_(numberVecs)
  {
    check();

    data_.resize(numberVecs);
    ownership_.resize(numberVecs);

    // Allocates memory to store the vectors.
    for (dimen_t v = 0 ; v < numberVecs_ ; ++v)
    {
      data_[v] = new std::vector<ScalarType>(length);
      ownership_[v] = true;
    }

    // Initializes all elements to zero.
    mvInit(0.0);
  }

  //! Constructor with already allocated memory
  MultiVecAdapter(const number_t length, const std::vector<VectorPointer>& rhs) :
    length_(length),
    numberVecs_(rhs.size())
  {
    check();

    data_.resize(numberVecs_);
    ownership_.resize(numberVecs_);

    // Copies pointers from input array, set ownership so that
    // this memory is not free'd in the destructor
    for (dimen_t v = 0 ; v < numberVecs_ ; ++v)
    {
      data_[v] = rhs[v];
      ownership_[v] = false;
    }
  }

  //! Copy constructor, performs a deep copy.
  MultiVecAdapter(const MultiVecAdapter& rhs) :
    length_(rhs.getVecLength()),
    numberVecs_(rhs.numberVecs_)
  {
    check();

    data_.resize(numberVecs_);
    ownership_.resize(numberVecs_);

    for (dimen_t v = 0 ; v < numberVecs_ ; ++v)
    {
      data_[v] = new std::vector<ScalarType>(length_);
      ownership_[v] = true;
    }

    for (dimen_t v = 0 ; v < numberVecs_ ; ++v)
    {
      for (number_t i = 0 ; i < length_ ; ++i)
        (*this)(i, v) = rhs(i, v);
    }
  }

  //! Destructor
  ~MultiVecAdapter()
  {
    for (number_t v = 0 ; v < numberVecs_ ; ++v)
      if (ownership_[v])
        delete data_[v];
  }

   /*!
     Returns a clone of the current mult-vector with a number of vector. If numberVecs is less than
     the number of current multi-vector then its first numberVecs vector will be clone.

     \param[in] numberVecs number of vector in multivector
     \return cloned multi-vector
   */
  MultiVecAdapter* clone(const dimen_t numberVecs) const
  {
    MultiVecAdapter* tmp = new MultiVecAdapter(length_, numberVecs);

    // FIXME Do we need to copy all values??
    //   for (number_t v = 0 ; v < numberVecs ; ++v)
    //         for (number_t i = 0 ; i < length_ ; ++i)
    //           (*tmp)(i, v) = (*this)(i, v);

    return(tmp);
  }

  //! Returns a clone of the current multi-vector.
  MultiVecAdapter* cloneCopy() const
  {
    return(new MultiVecAdapter(*this));
  }

  /*!
    Returns a clone copy of specified vectors by their index. Vector index must have size equal or less than
    the number of vector of the current multi-vector.

    \param[in] index index of vectors of the current multi-vector which are cloned
    \return cloned multi-vector
  */
  MultiVecAdapter* cloneCopy(const std::vector<int>& index) const
  {
    number_t size = index.size();
    MultiVecAdapter* tmp = new MultiVecAdapter(length_, size);

    for (number_t v = 0 ; v < index.size() ; ++v) {
      for (number_t i = 0 ; i < length_ ; ++i) {
        (*tmp)(i, v) = (*this)(i, index[v]);
      }
    }

    return(tmp);
  }

  /*!
    Returns a view of specified vectors (shallow copy) by their index.
    Vector index must have size equal or less than the number of vector of the current multi-vector.

    \param[in] index index of vectors of the current multi-vector which are cloned
    \return cloned multi-vector
  */
  MultiVecAdapter* cloneViewNonConst(const std::vector<int>& index)
  {
    number_t size = index.size();
    std::vector<VectorPointer> values(size);

    for (number_t v = 0 ; v < index.size() ; ++v)
      values[v] = data_[index[v]];

    return(new MultiVecAdapter(length_, values));
  }

  //! Returns a view of current vector (shallow copy), const version.
  const MultiVecAdapter* cloneView(const std::vector<int>& index) const
  {
    number_t size = index.size();
    std::vector<VectorPointer> values(size);

    for (number_t v = 0 ; v < index.size() ; ++v)
      values[v] = data_[index[v]];

    return(new MultiVecAdapter(length_, values));
  }

  //! Return length of vector in multi-vector
  number_t getVecLength () const
  {
    return(length_);
  }

  //! Return the number of vector in multi-vector
  dimen_t getNumberVecs () const
  {
    return(numberVecs_);
  }

  /*!
    Update *this with alpha * A * B + beta * (*this).
    In this case, multi-vector is consider a (dense) matrix whose column is formed by a vector

    \param[in] alpha alpha
    \param[in] A multi-vector A
    \param[in] B dense matrix B
    \param[in] beta beta
  */
  void mvTimesMatAddMv (ScalarType alpha, const MultiVec<ScalarType>& A,
                        const MatrixEigenDense<ScalarType>& B,
                        ScalarType beta)
  {

      internalEigenSolver::testErrorEigenProblemMultVec(length_ != A.getVecLength(), " mvTimesMatAddMv: Two multivector must have the same vector length");
      internalEigenSolver::testErrorEigenProblemMultVec(B.numOfRows() != A.getNumberVecs(), "mvTimesMatAddMv: Row of matrix must be equal to number of vector");
      internalEigenSolver::testErrorEigenProblemMultVec(B.numOfCols() > numberVecs_, "mvTimesMatAddMv: Column of matrix must be less than number of vector");

    MultiVecAdapter* MyA;
    MyA = dynamic_cast<MultiVecAdapter*>(&const_cast<MultiVec<ScalarType>&>(A));
    if (MyA == nullptr) error("constructor", "mvTimesMatAddMv: Unable to allocate memory");

    if ((*this)[0] == (*MyA)[0]) {
      // If this == A, then need additional storage ...
      // This situation is a bit peculiar but it may be required by
      // certain algorithms.

      std::vector<ScalarType> tmp(numberVecs_);

      for (number_t i = 0 ; i < length_ ; ++i) {
        for (number_t v = 0; v < A.getNumberVecs() ; ++v) {
          tmp[v] = (*MyA)(i, v);
        }

        for (number_t v = 0 ; v < B.numOfCols() ; ++v) {
          (*this)(i, v) *= beta;
          ScalarType res = NumTraits<ScalarType>::zero();

          for (number_t j = 0 ; j < A.getNumberVecs() ; ++j) {
            res +=  tmp[j] * B.coeff(j, v);
          }

          (*this)(i, v) += alpha * res;
        }
      }
    }
    else {
      for (number_t i = 0 ; i < length_ ; ++i) {
        for (number_t v = 0 ; v < B.numOfCols() ; ++v) {
          (*this)(i, v) *= beta;
          ScalarType res = 0.0;
          for (number_t j = 0 ; j < A.getNumberVecs() ; ++j) {
            res +=  (*MyA)(i, j) * B.coeff(j, v);
          }

          (*this)(i, v) += alpha * res;
        }
      }
    }
  }

  /*!
    Replace *this with alpha * A + beta * B.

    \param[in] alpha alpha
    \param[in] A multi-vector A
    \param[in] B multi-vector B
    \param[in] beta beta
  */
  void mvAddMv (ScalarType alpha, const MultiVec<ScalarType>& A,
                ScalarType beta,  const MultiVec<ScalarType>& B)
  {
    MultiVecAdapter* MyA;
    MyA = dynamic_cast<MultiVecAdapter*>(&const_cast<MultiVec<ScalarType> &>(A));
    if (MyA == nullptr) error("constructor", "mvAddMv: Unable to allocate memory");

    MultiVecAdapter* MyB;
    MyB = dynamic_cast<MultiVecAdapter*>(&const_cast<MultiVec<ScalarType> &>(B));
    if (MyB == nullptr) error("constructor", "mvAddMv: Unable to allocate memory");

    internalEigenSolver::testErrorEigenProblemMultVec(numberVecs_ != A.getNumberVecs(),":mvAddMv, Two multi-vectors must have the same number vector" );
    internalEigenSolver::testErrorEigenProblemMultVec(numberVecs_ != B.getNumberVecs(),":mvAddMv, Two multi-vectors must have the same number vector" );

    internalEigenSolver::testErrorEigenProblemMultVec(length_ != A.getVecLength(),":mvAddMv, Two multi-vectors must have the same vector length" );
    internalEigenSolver::testErrorEigenProblemMultVec(length_ != B.getVecLength(),":mvAddMv, Two multi-vectors must have the same vector length" );

    for (dimen_t v = 0 ; v < numberVecs_ ; ++v) {
      for (number_t i = 0 ; i < length_ ; ++i) {
        (*this)(i, v) = alpha * (*MyA)(i, v) + beta * (*MyB)(i, v);
      }
    }
  }

  /*!
     Compute a dense matrix B through the matrix-matrix multiply alpha * A^H * (*this).

    \param[in] alpha alpha
    \param[in] A multi-vector A
    \param[out] B dense matrix B
  */
  void mvTransMv (ScalarType alpha, const MultiVec<ScalarType>& A,
                  MatrixEigenDense<ScalarType>& B
                 ) const
  {
    MultiVecAdapter* MyA;
    MyA = dynamic_cast<MultiVecAdapter*>(&const_cast<MultiVec<ScalarType> &>(A));

    if (MyA == nullptr) error("constructor", "mvTransMv: Unable to allocate memory");

    internalEigenSolver::testErrorEigenProblemMultVec(length_ != A.getVecLength(),":mvTransMv, Two multi-vectors must have the same vector length" );
    internalEigenSolver::testErrorEigenProblemMultVec(B.numOfCols() < numberVecs_, "mvTransMv: Column of matrix must be greater than number of vector");
    internalEigenSolver::testErrorEigenProblemMultVec(B.numOfRows() < A.getNumberVecs(), "mvTransMv: Row of matrix must be greater than number of vector");

      for (dimen_t v = 0 ; v < A.getNumberVecs() ; ++v) {
        for (dimen_t w = 0 ; w < numberVecs_ ; ++w) {
          ScalarType value = 0.0;
          for (number_t i = 0 ; i < length_ ; ++i) {
            value += NumTraits<ScalarType>::conjugate((*MyA)(i, v)) * (*this)(i, w);
          }
          B.coeffRef(v, w) = alpha * value;
        }
      }

  }

  /*!
     Compute a vector b whose components are the individual dot-products, i.e.b[i] = A[i]^H*this[i] where A[i] is the i-th column of A.

    \param[in] A multi-vector A
    \param[out] b std::vector b
  */
  void mvDot (const MultiVec<ScalarType>& A, std::vector<ScalarType>& b) const
  {
    MultiVecAdapter* MyA;
    MyA = dynamic_cast<MultiVecAdapter*>(&const_cast<MultiVec<ScalarType> &>(A));
    if (MyA == nullptr) error("constructor", "mvTransMv: Unable to allocate memory");

    internalEigenSolver::testErrorEigenProblemMultVec(numberVecs_ != A.getNumberVecs(),":mvDot, Two multi-vectors must have the same number vector" );
    internalEigenSolver::testErrorEigenProblemMultVec(length_ != A.getVecLength(),":mvDot, Two multi-vectors must have the same vector length" );
    internalEigenSolver::testErrorEigenProblemMultVec(numberVecs_ > (dimen_t)b.size(), ":mvDot, Multi-vectors must have less number of vector than the size of result" );

      for (dimen_t v = 0 ; v < numberVecs_ ; ++v) {
        ScalarType value = 0.0;
        for (number_t i = 0 ; i < length_ ; ++i) {
          value += (*this)(i, v) * NumTraits<ScalarType>::conjugate((*MyA)(i, v));
        }
        b[v] = value;
      }
  }

  /*!
      Compute square norm of each vector of multivector in a result vector
      \param[out] normvec vector contains result of normalization
   */
  void mvNorm ( std::vector<typename NumTraits<ScalarType>::magnitudeType>& normvec ) const
  {
      internalEigenSolver::testErrorEigenProblemMultVec(numberVecs_ > ((dimen_t)normvec.size()),":mvNorm, Multi-vectors must have less number of vector than the size of result");

    typedef typename NumTraits<ScalarType>::magnitudeType MagnitudeType;

    for (dimen_t v = 0 ; v < numberVecs_ ; ++v) {
      MagnitudeType value = NumTraits<MagnitudeType>::zero();
      for (number_t i = 0 ; i < length_ ; ++i) {
        MagnitudeType val = NumTraits<ScalarType>::magnitude((*this)(i, v));
        value += val * val;
      }
      normvec[v] = NumTraits<MagnitudeType>::squareroot(value);
    }
  }

  /*!
      Copy the vectors in A to a set of vectors in *this. The numvecs vectors in
      A are copied to a subset of vectors in *this indicated by the indices given
      in index.
      \param[in] A multi-vector whose vectors are copied to a subset of vectors in *this
      \param[in] index index specifying the subset of vectors in *this to be copied
  */
  // FIXME: not so clear what the size of A and index.size() are...
  void setBlock (const MultiVec<ScalarType>& A,
                 const std::vector<int>& index)
  {
    MultiVecAdapter* MyA;
    MyA = dynamic_cast<MultiVecAdapter*>(&const_cast<MultiVec<ScalarType> &>(A));
    if (MyA == nullptr) error("constructor", "setBlock: Unable to allocate memory");

    internalEigenSolver::testErrorEigenProblemMultVec((number_t)index.size() > A.getNumberVecs(),":setBlock, Multi-vector must have the greater number vector than size of vector index" );
    internalEigenSolver::testErrorEigenProblemMultVec(length_ != A.getVecLength(),":setBlock, Two multi-vectors must have the same vector length" );


    for (number_t v = 0 ; v < index.size() ; ++v) {
      for (number_t i = 0 ; i < length_ ; ++i) {
        (*this)(i, index[v])  = (*MyA)(i, v);
      }
    }
  }

  // Scale the vectors by alpha
  void mvScale( ScalarType alpha )
  {
    for (dimen_t v = 0 ; v < numberVecs_ ; ++v) {
      for (number_t i = 0 ; i < length_ ; ++i) {
        (*this)(i, v) *= alpha;
      }
    }
  }

  // Scale the i-th vector by alpha[i]
  void mvScale( const std::vector<ScalarType>& alpha )
  {
    for (dimen_t v = 0 ; v < numberVecs_ ; ++v) {
      for (number_t i = 0 ; i < length_ ; ++i) {
        (*this)(i, v) *= alpha[v];
      }
    }
  }

  // Fill the vectors in *this with random numbers.
  void  mvRandom ()
  {
    for (dimen_t v = 0 ; v < numberVecs_ ; ++v) {
      for (number_t i = 0 ; i < length_ ; ++i) {
        (*this)(i, v) = NumTraits<ScalarType>::random();
      }
    }
  }

  // Replace each element of the vectors in *this with alpha.
  void  mvInit (ScalarType alpha)
  {
    for (dimen_t v = 0 ; v < numberVecs_ ; ++v) {
      for (number_t i = 0 ; i < length_ ; ++i) {
        (*this)(i, v) = alpha;
      }
    }
  }

  void mvPrint (std::ostream &os) const
  {
    os << "Object MultiVecAdapter" << std::endl;
    os << "Number of rows = " << length_ << std::endl;
    os << "Number of vecs = " << numberVecs_ << std::endl;

    for (number_t i = 0 ; i < length_ ; ++i)
      {
        for (dimen_t v = 0 ; v < numberVecs_ ; ++v)
          os << (*this)(i, v) << " ";
        os << std::endl;
      }
  }

  inline ScalarType& operator()(const int i, const int j)
  {
    if (j < 0 || j >= numberVecs_) error("index_out_of_range", "MultiVector: Number Vector", 0, numberVecs_);
    if (i < 0 || i >= (int)length_) error("index_out_of_range", "MultiVector: Vector length", 0, length_);

    return ((*(data_[j]))[i]);
  }

  inline const ScalarType& operator()(const int i, const int j) const
  {
    if (j < 0 || j >= numberVecs_) error("index_out_of_range", "MultiVector: Number Vector", 0, numberVecs_);
    if (i < 0 || i >= (int)length_) error("index_out_of_range", "MultiVector: Vector length", 0, length_);

    return ((*(data_[j]))[i]);
  }

  std::vector<ScalarType>* operator[](int v)
  {
    if (v < 0 || v >= numberVecs_) error("index_out_of_range", "MultiVector: Number Vector", 0, numberVecs_);
    return (data_[v]);
  }

  std::vector<ScalarType>* operator[](int v) const
  {
      if (v < 0 || v >= numberVecs_) error("index_out_of_range", "MultiVector: Number Vector", 0, numberVecs_);
      return (data_[v]);
  }

  /*
  Read from file a dense matrix, formated as:
  \verbatim
  nbRows nb_Cols
  a11 a12 ........
  a21 a22 ........
  ....
  \endverbatim
  */
 void mvLoadFromFile(const char* f)
 {
   trace_p->push("MultiVectorAdapter::loadFromFile");
   std::ifstream in(f);

   if(!in.is_open())    // file is not found
   {
     error("mat_badfile", f);
   }

   number_t cols, rows;
   in >> rows >> cols;
   if ((length_ != rows) || (numberVecs_ != cols)) {
       error("mat_badfile", f);
   }

   for (number_t r = 0; r < length_; ++r) {
       for (int c = 0; c < numberVecs_; ++c) {
           if(in.eof())    // error end of file reached
           {
             in.close();
             error("mat_badeof", f, rows, cols, r, c);
           }
           in >> ((*(data_[c]))[r]);
       }
   }

   in.close();
   trace_p->pop();
 }

private:
  void check()
  {
    if (length_ <= 0)
        error("index_out_of_range", "MultiVector: Vector length must be positive", 0, length_);

    if (numberVecs_ <= 0)
        error("index_out_of_range", "MultiVector: Number vector must be positive", 0, numberVecs_);
  }

  //! length of the vectors
  const number_t length_;
  //! Number of multi-vectors
  const dimen_t numberVecs_;
  //! Pointers to the storage of the vectors.
  std::vector<VectorPointer > data_;
  //! If \c true, then this object owns the vectors and must free them in dtor.
  std::vector<bool> ownership_;
};

/*! \relates MultiVecAdapter
    Output stream operator for handling the printing of MultiVecAdapter.
*/
template <typename ScalarType>
std::ostream& operator<<(std::ostream& os, const MultiVecAdapter<ScalarType>& obj)
{
  obj.mvPrint(os);
  return os;
}
}
#endif /* MULTI_VECTOR_HPP */

