/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DualSkylineStorage.cpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 6 Juin 2014

  \brief Implementation of xlifepp::DualSkylineStorage class and functionnalities
*/

#include "DualSkylineStorage.hpp"

namespace xlifepp
{

     using std::abs;


/*--------------------------------------------------------------------------------
  Constructors, Destructor
--------------------------------------------------------------------------------*/
// default constructor
DualSkylineStorage::DualSkylineStorage(number_t nr, number_t nc, string_t id)
  : SkylineStorage(nr, nc, _dual, id) {}

DualSkylineStorage::DualSkylineStorage(const std::vector<number_t>& rowp, const std::vector<number_t>& colp, string_t id)
  : SkylineStorage(rowp.size()-1,colp.size()-1, _dual, id) {rowPointer_=rowp; colPointer_=colp;}

// constructor by a pair of numbering vectors rowNumbers and colNumbers (same size)
//   rowNumbers[k] is the row numbers of submatrix k
//   colNumbers[k] is the col numbers of submatrix k
// this constructor is well adapted to FE matrix where submatrix is linked to an element
DualSkylineStorage::DualSkylineStorage(number_t nr, number_t nc, const std::vector<std::vector<number_t> >& rowNumbers, const std::vector<std::vector<number_t> >& colNumbers, string_t id)
  : SkylineStorage(nr, nc, _dual, id)
{
  trace_p->push("DualSkylineStorage constructor");

  // build the row skyline and col skyline
  rowPointer_.resize(nbRows_ + 1);
  std::vector<number_t>::iterator it = rowPointer_.begin();
  for(number_t r = 1; r <= nbRows_; r++, it++) { *it = r; }
  colPointer_.resize(nbCols_ + 1);
  it = colPointer_.begin();
  for(number_t c = 1; c <= nbCols_; c++, it++) { *it = c; }

  std::vector<std::vector<number_t> >::const_iterator itrs, itcs = colNumbers.begin();
  std::vector<number_t>::const_iterator itr, itc;
  // compute min row and min col indices
  for(itrs = rowNumbers.begin(); itrs != rowNumbers.end(); itrs++, itcs++)
    {
      for(itr = itrs->begin(); itr != itrs->end(); itr++)
        {
          for(itc = itcs->begin(); itc != itcs->end(); itc++)
            {
              if(*itr > *itc) { rowPointer_[*itr - 1] = std::min(rowPointer_[*itr - 1], *itc); }
              if(*itc > *itr) { colPointer_[*itc - 1] = std::min(colPointer_[*itc - 1], *itr); }
            }
        }
    }
  // build rowPointer_ and colPointer_
  *(rowPointer_.begin()) = 0;
  number_t r = 2, c = 1, l = 0;
  for(it = rowPointer_.begin() + 1; it != rowPointer_.end(); it++, r++)
    {
      c = *it;
      *it = *(it - 1) + l;
      l = r - c;
    }
  *(colPointer_.begin()) = 0;
  c = 2; l = 0;
  for(it = colPointer_.begin() + 1; it != colPointer_.end(); it++, c++)
    {
      r = *it;
      *it = *(it - 1) + l;
      l = c - r;
    }
  trace_p->pop();
}
/*--------------------------------------------------------------------------------
  create a new scalar DualSkyline storage from current DualSkyline storage and submatrix sizes
  --------------------------------------------------------------------------------*/
DualSkylineStorage* DualSkylineStorage::toScalar(dimen_t nbr, dimen_t nbc)
{
    MatrixStorage* nsto=findMatrixStorage(stringId, _skyline, _dual, buildType_, true, nbr*nbRows_, nbc*nbCols_);
    if(nsto!=nullptr) return static_cast<DualSkylineStorage*>(nsto);
    std::vector<std::vector<number_t> > cols=scalarColIndices(nbr, nbc);
    DualSkylineStorage* ds = new DualSkylineStorage(nbr*nbRows_, nbc*nbCols_, cols, stringId);
    ds->buildType() = buildType_;
    ds->scalarFlag()=true;
    return ds;
}

  /*--------------------------------------------------------------------------------
    check if two storages have the same structures
  --------------------------------------------------------------------------------*/
  bool DualSkylineStorage::sameStorage(const MatrixStorage& sto) const
  {
    if(!(sto.storageType()==storageType_ &&
    sto.accessType()==accessType_ &&
    sto.nbOfRows()==nbRows_ &&
    sto.nbOfColumns()==nbCols_&&
    sto.size()==size()))
      return false;
    const DualSkylineStorage& csto=static_cast<const DualSkylineStorage&>(sto);
    //compare structure pointers
    if(rowPointer_!=csto.rowPointer_) return false;
    if(colPointer_!=csto.colPointer_) return false;
    return true;
  }

/*--------------------------------------------------------------------------------
 add submatrix indices given by its row and column indices (>= 1)
--------------------------------------------------------------------------------*/
void DualSkylineStorage::addSubMatrixIndices(const std::vector<number_t>& rows,const std::vector<number_t>& cols)
{
  addSkylineSubMatrixIndices(rowPointer_,rows,cols);
  addSkylineSubMatrixIndices(colPointer_,cols,rows);
}

/*--------------------------------------------------------------------------------
 access operator to position of (i,j)>0 coefficients (return 0 if outside)
 This DIRECT access to (i,j) pointer SHOULD NOT be used in matrix computation
 symmetry info is not used
--------------------------------------------------------------------------------*/
number_t DualSkylineStorage::pos(number_t i, number_t j, SymType s) const
{
  if(i == 0 || i > nbRows_ || j == 0 || j > nbCols_) { return 0; }
  if(i == j) { return i; } // diagonal coeff.
  if(i > j) // lower triangular part
    {
      number_t l = rowPointer_[i] - rowPointer_[i - 1];
      if(i <= l + j)  { return diagonalSize() + rowPointer_[i] + j - i + 1; } // add 1 because values begin at address 1
      else { return 0; }
    }
  // upper triangular part
  number_t l = colPointer_[j] - colPointer_[j - 1];
  if(j <= l + i) { return diagonalSize() + rowPointer_[nbRows_] + colPointer_[j] + i - j + 1; }
  return 0;
}

/*--------------------------------------------------------------------------------
 access to submatrix adresses, useful for assembling matrices
 rows and cols are vector of index (>0) of row and column, no index ordering is assumed !!
 return the vector of adresses (relative number >0 in storage) in a row storage (as Matrix)
 if a row or a col index is out of storage
     an error is raised if errorOn is true (default)
     position is set to 0 else
 if sy!=_noSymmetry: symmetric matrix, only positions of lower part are returned (not used here)
--------------------------------------------------------------------------------*/
void DualSkylineStorage::positions(const std::vector<number_t>& rows, const std::vector<number_t>& cols, std::vector<number_t>& adrs, bool errorOn, SymType sy) const
{
  number_t nba = rows.size() * cols.size();
  if(adrs.size() != nba) { adrs.resize(nba, 0); }      // reset size
  std::vector<number_t>::iterator itp = adrs.begin();
  std::vector<number_t>::const_iterator itr, itc;

  for(itr = rows.begin(); itr != rows.end(); itr++)  // loop on row index
    for(itc = cols.begin(); itc != cols.end(); itc++, itp++) // loop on col indices
      {
        *itp = pos(*itr, *itc);  // non optimal method
        if(*itp == 0 && errorOn) { error("storage_outofstorage", "DualSkyline", *itr, *itc); }
      }
}

// get (col indices, adress) of row r in set [c1,c2] ## SymType not used ##
std::vector<std::pair<number_t, number_t> > DualSkylineStorage::getRow(SymType s, number_t r, number_t c1, number_t c2) const
{
  number_t nbc=c2;
  if(nbc==0) nbc=nbCols_;
  std::vector<std::pair<number_t, number_t> > cols(nbc-c1+1);
  std::vector<std::pair<number_t, number_t> >::iterator itcol=cols.begin();
  number_t l=0;
  if(c1 < r) //lower part
    {
      number_t rb=rowPointer_[r-1], re=rowPointer_[r];
      for(number_t k = rb; k < re; k++) // find in list of col index of row i
        {
          number_t c=k+r-re;
          if(c >= c1 && c <= nbc && c<r) {*itcol = std::make_pair(c,nbRows_+k+1);l++; itcol++;}   // add 1 because values begin at address 1
        }
    }
  if(nbc >=r && r>=c1) {*itcol++=std::make_pair(r,r);l++;} //diag coeff
  for(number_t c=std::max(r+1,c1); c<=nbc; c++)            //upper part
    {
      number_t a=pos(r,c);
      if(a!=0) {*itcol=std::make_pair(c,a); l++; itcol++;}
    }
  cols.resize(l);
  return cols;
}

// get (row indices, adress) of col c in set [r1,r2] ## SymType not used ##
std::vector<std::pair<number_t, number_t> > DualSkylineStorage::getCol(SymType s,number_t c, number_t r1, number_t r2) const
{
  number_t nbr=r2;
  if(nbr==0) nbr=nbRows_;
  std::vector<std::pair<number_t, number_t> > rows(nbr-r1+1);
  std::vector<std::pair<number_t, number_t> >::iterator itrow=rows.begin();
  number_t l=0;
  if(r1 < c) //upper part
    {
      number_t cb=colPointer_[c-1], ce=colPointer_[c];
      number_t shift=diagonalSize() + rowPointer_[nbRows_];
      for(number_t k = cb; k < ce; k++) // find in list of col index of row i
        {
          number_t r=k+c-ce;
          if(r >= r1 && r <= nbr && r<c) { *itrow = std::make_pair(r,shift+k+1); itrow++; l++;};  // add 1 because values begin at address 1
        }
    }
  if(nbr >=c && c>=r1) {*itrow++=std::make_pair(c,c);l++;}  //diag coeff
  for(number_t r=std::max(c+1,r1); r<=nbr; r++) //lower part
    {
      number_t a=pos(r,c);
      if(a!=0){*itrow=std::make_pair(r,a); l++; itrow++;}
    }
  rows.resize(l);
  return rows;
}

// print storage pointers
void DualSkylineStorage::print(std::ostream& os) const
{
    printHeader(os);
    os<<"row pointer = "<<rowPointer_<<eol;
    os<<"col pointer = "<<colPointer_<<eol;
}

/*--------------------------------------------------------------------------------
 print DualSkyline Storage matrix to output stream
--------------------------------------------------------------------------------*/
// print real scalar matrix
void DualSkylineStorage::printEntries(std::ostream& os, const std::vector<real_t>& m, number_t vb, const SymType sym) const
{
  std::vector<real_t>::const_iterator itm = m.begin() + 1, itml = itm + diagonalSize(),
                                    itmu = itml + lowerPartSize();
  os << "lower triangular part ";
  printEntriesTriangularPart(_scalar, itm, itml,  rowPointer_, entriesPerRow, entryWidth, entryPrec, "row", vb, os);
  itm = m.begin() + 1;
  os << "upper triangular part";
  printEntriesTriangularPart(_scalar, itm, itmu, colPointer_, entriesPerRow, entryWidth, entryPrec, "col", vb, os);
}

// print complex scalar matrix
void DualSkylineStorage::printEntries(std::ostream& os, const std::vector<complex_t>& m, number_t vb, SymType sym) const
{
    std::vector<complex_t>::const_iterator itm = m.begin() + 1, itml = itm + diagonalSize(),
                                       itmu = itml + lowerPartSize();
  printEntriesTriangularPart(_scalar, itm, itml, rowPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "row", vb, os);
  itm = m.begin() + 1;
  printEntriesTriangularPart(_scalar, itm, itmu, colPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "col", vb, os);
}

// large matrix of vectors are not available for the moment, use Matrix n x 1 or 1 x n
void DualSkylineStorage::printEntries(std::ostream& os, const std::vector< Vector<real_t> >& m, number_t vb, SymType sym) const
{error("storage_novector", "DualSkylineStorage::printEntries");}

void DualSkylineStorage::printEntries(std::ostream& os, const std::vector< Vector<complex_t> >& m, number_t vb, SymType sym) const
{error("storage_novector", "DualSkylineStorage::printEntries");}

// print matrix of real matrices
void DualSkylineStorage::printEntries(std::ostream& os, const std::vector< Matrix<real_t> >& m, number_t vb, SymType sym) const
{
  std::vector< Matrix<real_t> >::const_iterator itm = m.begin() + 1, itml = itm + diagonalSize(),
                                              itmu = itml + lowerPartSize();
  printEntriesTriangularPart(_matrix, itm, itml, rowPointer_, entriesPerRow, entryWidth, entryPrec, "row", vb, os);
  itm = m.begin() + 1;
  printEntriesTriangularPart(_matrix, itm, itmu, colPointer_, entriesPerRow, entryWidth, entryPrec, "col", vb, os);
}

// print matrix of complex matrices
void DualSkylineStorage::printEntries(std::ostream& os, const std::vector< Matrix<complex_t> >& m, number_t vb, SymType sym) const
{
  std::vector< Matrix<complex_t> >::const_iterator itm = m.begin() + 1, itml = itm + diagonalSize(),
                                                 itmu = itml + lowerPartSize();
  printEntriesTriangularPart(_matrix, itm, itml, rowPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "row", vb, os);
  itm = m.begin() + 1;
  printEntriesTriangularPart(_matrix, itm, itmu, colPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "col", vb, os);
}

/*
--------------------------------------------------------------------------------
 print matrix to ostream in coordinate form: i j M_ij
 according to type of values of matrix (overload the general method in MatrixStorage)
 sym is not used
--------------------------------------------------------------------------------
*/
void DualSkylineStorage::printCooMatrix(std::ostream& os, const std::vector<real_t>& m, SymType sym) const
{
  std::vector<real_t>::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml + lowerPartSize();
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, rowPointer_, nbRows_, nbCols_, true);
  printCooTriangularPart(os, itmu, colPointer_, nbCols_, nbRows_, false);
}

void DualSkylineStorage::printCooMatrix(std::ostream& os, const std::vector<complex_t>& m, SymType sym) const
{
  std::vector<complex_t>::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml + lowerPartSize();
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, rowPointer_, nbRows_, nbCols_, true);
  printCooTriangularPart(os, itmu, colPointer_, nbCols_, nbRows_, false);
}

void DualSkylineStorage::printCooMatrix(std::ostream& os, const std::vector< Matrix<real_t> >& m, SymType sym) const
{
  std::vector< Matrix<real_t> >::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml + lowerPartSize();
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, rowPointer_, nbRows_, nbCols_, true);
  printCooTriangularPart(os, itmu, colPointer_, nbCols_, nbRows_, false);
}

void DualSkylineStorage::printCooMatrix(std::ostream& os, const std::vector< Matrix<complex_t> >& m, SymType sym) const
{
  std::vector< Matrix<complex_t> >::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml + lowerPartSize();
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, rowPointer_, nbRows_, nbCols_, true);
  printCooTriangularPart(os, itmu, colPointer_, nbCols_, nbRows_, false);
}

/*--------------------------------------------------------------------------------
 template Matrix x Vector & Vector x Matrix multiplications and specializations
 result vector has to be sized before
--------------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void DualSkylineStorage::multMatrixVector(const std::vector<M>& m, V* vp, R* rp) const
{
  trace_p->push("DualSkylineStorage::multMatrixVector (pointer form)");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  R* rpe= rp + diagonalSize();
  SkylineStorage::diagonalMatrixVector(itm, vp, rp, rpe);   // product by diagonal
  itl = m.begin() + 1 + diagonalSize();
  SkylineStorage::lowerMatrixVector(rowPointer_, itl, vp, rp, _noSymmetry);  // lower part
  itl = m.begin() + 1 + diagonalSize() + lowerPartSize();
  SkylineStorage::upperMatrixVector(colPointer_, itl, vp, rp, _noSymmetry);  // upper part
  trace_p->pop();
}

template<typename M, typename V, typename R>
void DualSkylineStorage::multMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  trace_p->push("DualSkylineStorage::multMatrixVector");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = itrb + diagonalSize();
  SkylineStorage::diagonalMatrixVector(itm, itvb, itrb, itre);   //product by diagonal
  itl = m.begin() + 1 + diagonalSize();
  SkylineStorage::lowerMatrixVector(rowPointer_, itl, itvb, itrb, _noSymmetry);  //lower part
  itl = m.begin() + 1 + diagonalSize() + lowerPartSize();
  SkylineStorage::upperMatrixVector(colPointer_, itl, itvb, itrb, _noSymmetry);  //upper part
  trace_p->pop();
}

template<typename M, typename V, typename R>
void DualSkylineStorage::multVectorMatrix(const std::vector<M>& m, V* vp, R* rp) const
{
  trace_p->push("DualSkylineStorage::multVectorMatrix (pointer form)");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  R* rpe= rp + diagonalSize();
  SkylineStorage::diagonalVectorMatrix(itm, vp, rp, rpe);   // product by diagonal
  itl = m.begin() + 1 + diagonalSize();
  SkylineStorage::lowerVectorMatrix(rowPointer_, itl, vp, rp, _noSymmetry);  // lower part
  itl = m.begin() + 1 + diagonalSize() + lowerPartSize();
  SkylineStorage::upperVectorMatrix(colPointer_, itl, vp, rp, _noSymmetry);  // upper part
  trace_p->pop();
}

template<typename M, typename V, typename R>
void DualSkylineStorage::multVectorMatrix(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  trace_p->push("DualSkylineStorage::multVectorMatrix");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = itrb + diagonalSize();
  SkylineStorage::diagonalVectorMatrix(itm, itvb, itrb, itre);   //product by diagonal
  itl = m.begin() + 1 + diagonalSize();
  SkylineStorage::lowerVectorMatrix(rowPointer_, itl, itvb, itrb, _noSymmetry);  //lower part
  itl = m.begin() + 1 + diagonalSize() + lowerPartSize();
  SkylineStorage::upperVectorMatrix(colPointer_, itl, itvb, itrb, _noSymmetry);  //upper part
  trace_p->pop();
}

/*-----------------------------------------------------------------------
              triangular part matrix * vector
--------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void DualSkylineStorage::diagonalMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    if(nbRows_>nbCols_) r.assign(nbRows_,R(0)); else r.resize(nbRows_);  //to reset to 0
    typename std::vector<M>::const_iterator itmb = m.begin() + 1;
    typename std::vector<V>::const_iterator itvb = v.begin();
    typename std::vector<R>::iterator itrb = r.begin(), itre = itrb + diagonalSize();
    SkylineStorage::diagonalVectorMatrix(itmb, itvb, itrb, itre);   //product by diagonal
}

template<typename M, typename V, typename R>
void DualSkylineStorage::lowerMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
    else r.resize(nbRows_);
    typename std::vector<M>::const_iterator itmb = m.begin() + 1;
    typename std::vector<V>::const_iterator itvb = v.begin();
    typename std::vector<R>::iterator itrb = r.begin(), itre = itrb + diagonalSize();
    SkylineStorage::diagonalMatrixVector(itmb, itvb, itrb, itre);   //product by diagonal
    itmb = m.begin() + 1 + diagonalSize();
    SkylineStorage::lowerMatrixVector(rowPointer_, itmb, itvb, itrb, _noSymmetry);  //lower part
}

template<typename M, typename V, typename R>
void DualSkylineStorage::lowerD1MatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
    else r.resize(nbRows_);
    typename std::vector<M>::const_iterator itmb=m.begin()+1;
    typename std::vector<V>::const_iterator itvb = v.begin(), itv=itvb;
    typename std::vector<R>::iterator itrb = r.begin(), itr=itrb;
    for(number_t i=0;i<std::min(nbRows_,nbCols_); i++, ++itv,++itr) *itr = *itv; //diag part
    itmb = m.begin() + 1 + diagonalSize();
    SkylineStorage::lowerMatrixVector(rowPointer_, itmb, itvb, itrb, _noSymmetry);  //lower part
}

template<typename M, typename V, typename R>
void DualSkylineStorage::upperMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    if(nbRows_>nbCols_) r.assign(nbRows_,R(0)); else r.resize(nbRows_); //to reset to 0
    typename std::vector<M>::const_iterator itmb = m.begin() + 1;
    typename std::vector<V>::const_iterator itvb = v.begin();
    typename std::vector<R>::iterator itrb = r.begin(), itre = itrb + diagonalSize();
    SkylineStorage::diagonalMatrixVector(itmb, itvb, itrb, itre);   //product by diagonal
    itmb = m.begin() + 1 + diagonalSize() + lowerPartSize();
    SkylineStorage::upperMatrixVector(colPointer_, itmb, itvb, itrb, _noSymmetry);  //upper part
}

template<typename M, typename V, typename R>
void DualSkylineStorage::upperD1MatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
    if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
    else r.resize(nbRows_);
    typename std::vector<M>::const_iterator itmb=m.begin()+1;
    typename std::vector<V>::const_iterator itvb=v.begin(), itv=itvb;
    typename std::vector<R>::iterator itrb=r.begin(), itr=itrb;
    for(number_t i=0;i<std::min(nbRows_,nbCols_);i++, ++itv,++itr) *itr = *itv; //diag part
    itmb=m.begin()+1+diagonalSize()+lowerPartSize();
    SkylineStorage::upperMatrixVector(colPointer_, itmb, itvb, itrb, _noSymmetry);  //upper part
}

/*
--------------------------------------------------------------------------------
     Template diagonal solver (after factorization)
--------------------------------------------------------------------------------
*/
template<typename M, typename V, typename X>
void DualSkylineStorage::diagonalSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const
{
  /*
     diagonal solver
  */
  // no const v here as v might coincide with x
  trace_p->push("DualSkylineStorage::diagonalSolver");
  bzDiagonalSolver(m.begin() + 1, v.begin(), x.begin(), x.end());
  trace_p->pop();
}

/*
--------------------------------------------------------------------------------
     Template lower triangular part with unit diagonal solver (after factorization)
--------------------------------------------------------------------------------
*/
template<typename M, typename V, typename X>
void DualSkylineStorage::lowerD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const
{
  /*
     solver for a lower triangular part linear system with unit diagonal
  */
  // no const v here as v might coincide with x
  trace_p->push("DualSkylineStorage::lowerD1Solver");
  bzLowerD1Solver(m.begin() + 1 + v.size(), v.begin(), x.begin(), x.end(), rowPointer_.begin());
  trace_p->pop();
}

/*
--------------------------------------------------------------------------------
     Template upper triangular part with unit diagonal solver (after factorization)
--------------------------------------------------------------------------------
*/
template<typename M, typename V, typename X>
void DualSkylineStorage::upperD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x, const SymType sym) const
{
  /*
      solver for an upper triangular part linear system with unit diagonal
   */
  // no const v here as v might coincide with x
  // LO AND BEHOLD *** we use reverse_iterators here ***/
  trace_p->push("DualSkylineStorage::upperD1Solver");
  bzUpperD1Solver(m.rbegin(), v.rbegin(), x.rbegin(), x.rend(), colPointer_.rbegin());
  trace_p->pop();
}

/*
 --------------------------------------------------------------------------------
     Template upper triangular part solver (after factorization)
 --------------------------------------------------------------------------------
 */
template<typename M, typename V, typename X>
void DualSkylineStorage::upperSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const
{
  /*
     solver for an upper triangular part linear system
  */
  // no const v here as v might coincide with x
  // LO AND BEHOLD *** we use reverse_iterators here */
  // Need to subtract 1 to make sure pointing to the first element of vector m
  trace_p->push("DualSkylineStorage::upperSolver");
  bzUpperSolver(m.rend() - 1 - v.size(), m.rbegin(), v.rbegin(), x.rbegin(), x.rend(), colPointer_.rbegin());
  trace_p->pop();
}

/*
--------------------------------------------------------------------------------
     Template L.D.U factorization - original one -
--------------------------------------------------------------------------------
*/
//template<typename M>
//void DualSkylineStorage::lu(std::vector<M>& m, std::vector<M>& f) const
//{
//  /*
//   Factorization of matrix M stored as matrix F = L U where
//   - L is a lower triangular matrix with unit diagonal and is stored as
//   the strict lower triangular part of F
//   - U is a upper triangular matrix with diagonal stored as diagonal part of F
//   and strict upper triangular part stored as the strict upper triangular part of F:
//
//           \sum_{ max(C1(i), L1(j)) <= k <= j } L_{ik} U{kj} = M_{ij}
//
//   where C1(i) is the column index of first non zero entry on row i
//         L1(j) and the row index of first non zero entry on column j
//  */
//  trace_p->push("DualSkylineStorage::lu");
//
//  // iterators on diagonal (md), strict lower (ml) and strict upper (mu) triangular part of M
//  typename std::vector<M>::iterator md_i=m.begin() + 1, ml_i=md_i + nbRows_, mu_i=ml_i + * (rowPointer_.end() - 1);
//
//  // iterators on diagonal (d), strict lower (l) and strict upper (u) triangular part
//  // of non symmetric matrix F
//  typename std::vector<M>::iterator d_i=f.begin() + 1, l_ib=d_i + nbRows_, l_i_nx=l_ib, l_i,
//                                    u_ib=l_ib + * (rowPointer_.end() - 1), u_i_nx=u_ib, u_i;
//
//  // iterators on row adresses starting with 2nd row (first row is row 0)
//  std::vector<number_t>::const_iterator row_i_nx=rowPointer_.begin() + 1, row_i
//  ,                                     col_i_nx=colPointer_.begin() + 1, col_i;
//
//  // first diagonal entry D_{11} = U_{11} = M_{11}
//  if(std::abs(*md_i) < NumTraits<M>::epsilon()) { isSingular("L.U", 0); }
//  *d_i++ = *md_i++;
//
//  for(number_t row = 1; row < nbRows_; ++d_i, ++md_i, ++row)
//    {
//      // initialize D_{ii} with M_{ii}
//      *d_i = *md_i;
//
//      // iterator to first entry on current row
//      // and number of entries on current row ( row_entries = i - C1(i) )
//      row_i = row_i_nx++;
//      number_t row_entries = *row_i_nx - *row_i;
//      l_i = l_i_nx; l_i_nx = l_ib + *row_i_nx;
//
//      thePrintStream<<"DualSkylineStorage::lu row = "<<row<<" row_entries="<<row_entries
//                    <<" row_i="<<*row_i<<" row_i_nx="<<*row_i_nx<< eol<<std::flush;
//      if(row_entries > 0)
//        {
//          /*
//           compute sub-diagonal entries of factorized matrix on current row (say i)
//           by solving the following sub-system:
//           \sum_{k <= j}  L_{ik} U_{kj} = M_{ij}  ( for i > j )
//           where U_{kj}'s are computed on previous columns
//           */
//          bzLowerSolver(d_i - row_entries, u_ib, ml_i + *row_i, l_i, l_i_nx, row_i - row_entries);
//        }
//
//      // iterator to first entry on current column
//      // and number of entries on current column ( col_entries = i - L1(i) )
//      col_i = col_i_nx++;
//      number_t col_entries = *col_i_nx - *col_i;
//      u_i = u_i_nx; u_i_nx = u_ib + *col_i_nx;
//      if(col_entries > 0)
//        {
//          /*
//           compute super-diagonal entries of factorized matrix on current column (say j)
//           by solving the following sub-system:
//           \sum_{k <= j}  L{ik} U_{kj} = M_{ij}  ( for i < j )
//           where L_{ik}'s are computed on previous rows
//           */
//          bzLowerD1Solver(l_ib, mu_i + *col_i, u_i, u_i_nx, col_i - col_entries);
//        }
//      if(std::min(row_entries, col_entries) > 0)
//        {
//          /*
//           compute diagonal entry
//           D_{ii} = M_{ii} - \sum_{max(C1(i),L1(i) <= k < i} L_{ik} * U_{ki}
//          */
//          if(row_entries > col_entries)
//            for(typename std::vector<M>::iterator u_ki = u_i; u_ki != u_i_nx; ++u_ki)
//              {  *d_i -= *l_i++ * *u_ki; }
//          else
//            for(typename std::vector<M>::iterator l_ki = l_i; l_ki != l_i_nx; ++l_ki)
//              {  *d_i -= *l_ki * *u_i++; }
//        }
//      if(std::abs(*d_i) < NumTraits<M>::epsilon()) { isSingular("L.U", row); }
//    }
//  trace_p->pop();
//}

template<typename M>
void DualSkylineStorage::lu(std::vector<M>& m, std::vector<M>& f) const
{
  /*
   Factorization of matrix M stored as matrix F = L U where
   - L is a lower triangular matrix with unit diagonal and is stored as
   the strict lower triangular part of F
   - U is a upper triangular matrix with diagonal stored as diagonal part of F
   and strict upper triangular part stored as the strict upper triangular part of F:

           \sum_{ max(C1(i), L1(j)) <= k <= j } L_{ik} U{kj} = M_{ij}

   where C1(i) is the column index of first non zero entry on row i
         L1(j) and the row index of first non zero entry on column j
  */
  trace_p->push("DualSkylineStorage::lu");

  // iterators on diagonal (md), strict lower (ml) and strict upper (mu) triangular part of M
  typename std::vector<M>::iterator md_i=m.begin() + 1, ml_i=md_i + nbRows_, mu_i=ml_i + * (rowPointer_.end() - 1);

  // iterators on diagonal (d), strict lower (l) and strict upper (u) triangular part
  // of non symmetric matrix F
  typename std::vector<M>::iterator d_i=f.begin() + 1, l_ib=d_i + nbRows_, l_i_nx=l_ib, l_i,
                                    u_ib=l_ib + * (rowPointer_.end() - 1), u_i_nx=u_ib, u_i;

  // iterators on row adresses starting with 2nd row (first row is row 0)
  std::vector<number_t>::const_iterator row_i_nx=rowPointer_.begin() + 1, row_i,
                                        col_i_nx=colPointer_.begin() + 1, col_i;

  // first diagonal entry D_{11} = U_{11} = M_{11}
  if(abs(*md_i) < NumTraits<M>::epsilon()) { isSingular("L.U", 0); }
  *d_i++ = *md_i++;

  for(number_t row = 1; row < nbRows_; ++d_i, ++md_i, ++row)
    {
      // initialize D_{ii} with M_{ii}
      *d_i = *md_i;
      // iterator to first entry on current row
      // and number of entries on current row ( row_entries = i - C1(i) )
      row_i = row_i_nx++;
      number_t row_entries = *row_i_nx - *row_i;
      l_i = l_i_nx; l_i_nx = l_ib + *row_i_nx;
      // iterator to first entry on current column
      // and number of entries on current column ( col_entries = i - L1(i) )
      col_i = col_i_nx++;
      number_t col_entries = *col_i_nx - *col_i;
      u_i = u_i_nx; u_i_nx = u_ib + *col_i_nx;
      if(row_entries > 0)
        {
          /*
           compute sub-diagonal entries of factorized matrix on current row (say i)
           by solving the following sub-system:
           \sum_{k <= j}  L_{ik} U_{kj} = M_{ij}  ( for i > j )
           where U_{kj}'s are computed on previous columns
           */
          bzLowerSolver(d_i - row_entries, u_ib, ml_i + *row_i, l_i, l_i_nx, col_i - row_entries);
        }
      if(col_entries > 0)
        {
          /*
           compute super-diagonal entries of factorized matrix on current column (say j)
           by solving the following sub-system:
           \sum_{k <= j}  L{ik} U_{kj} = M_{ij}  ( for i < j )
           where L_{ik}'s are computed on previous rows
           */
          bzLowerD1Solver(l_ib, mu_i + *col_i, u_i, u_i_nx, row_i - col_entries);
        }
      if(std::min(row_entries, col_entries) > 0)
        {
          /*
           compute diagonal entry
           D_{ii} = M_{ii} - \sum_{max(C1(i),L1(i) <= k < i} L_{ik} * U_{ki}
          */
          if(row_entries > col_entries)
            {
                l_i+=row_entries-col_entries;
                for(typename std::vector<M>::iterator u_ki = u_i; u_ki != u_i_nx; ++u_ki, ++l_i)
                  *d_i -= *l_i * *u_ki;
            }
          else
            {
                u_i+=col_entries-row_entries;
                for(typename std::vector<M>::iterator l_ki = l_i; l_ki != l_i_nx; ++l_ki, ++u_i)
                  *d_i -= *l_ki * *u_i;
            }
        }
      if(abs(*d_i) < NumTraits<M>::epsilon()) { isSingular("L.U", row); }
    }
  trace_p->pop();
}
/*!
   Add two matrices
   \param m1 vector values_ of first matrix
   \param m2 vector values_ of second matrix
   \param r vector values_ of result matrix
 */
template<typename M1, typename M2, typename R>
void DualSkylineStorage::addMatrixMatrix(const std::vector<M1>& m1, const std::vector<M2>& m2, std::vector<R>& r) const
{
  trace_p->push("DualSkylineStorage::addMatrixMatrix");
  typename std::vector<M1>::const_iterator itm1 = m1.begin();
  typename std::vector<M2>::const_iterator itm2 = m2.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = r.end();
  sumMatrixMatrix(itm1, itm2, itrb, itre);
  trace_p->pop();
}


/*!
   Addition two skyline matrices
   \param[in,out] m1 vector values_ of the first matrix
   \param[in] st1 symmetric type of the first matrix
   \param[in] rowPtr2 rowPointer of storage of the second matrix
   \param[in] colPtr2 colPointer of storage of the second matrix
   \param[in] m2 vector values_ of the second matrix
   \param[in] st2 symmetric type of the second matrix
 */
template<typename M1, typename M2>
void DualSkylineStorage::addTwoMatrixDualSkyline(std::vector<M1>& m1, SymType st1,
                         const std::vector<number_t>& rowPtr2, const std::vector<number_t>& colPtr2, const std::vector<M2>& m2, SymType st2)
{
  trace_p->push("DualSkylineStorage::addTwoMatrixDualSkyline");
  typename std::vector<M1>::const_iterator itm1 = m1.begin()+1, itm1Tmp;
  typename std::vector<M2>::const_iterator itm2 = m2.begin()+1;
  std::vector<M1> nValues(diagonalSize()+1, M1());
  typename std::vector<M1>::iterator itv = nValues.begin()+1, itev = nValues.end();

  for (;itv != itev; ++itv, ++itm1, ++itm2) *itv = *(itm1) + *(itm2);

  std::vector<number_t>::iterator itbrP1 = rowPointer_.begin() + 1, itrP1, iterP1 = rowPointer_.end();
  std::vector<number_t>::const_iterator itbrP2 = rowPtr2.begin() + 1, itrP2;

  itrP1 = itbrP1;
  itrP2 = itbrP2;
  number_t distance1 = 0, distance2 = 0;

  // Update the lower part
  for (; itrP1 != (iterP1-1); ++itrP1, ++itrP2) {
      distance1 = *(itrP1+1) - *itrP1;
      distance2 = *(itrP2+1) - *itrP2;
      if (distance1 <= distance2) {
          for (number_t i = 0; i < (distance2 - distance1); ++i) nValues.push_back(*(itm2++));
          for (number_t i = 0; i < distance1; ++i) nValues.push_back(*(itm1++) + *(itm2++));

      } else {
          for (number_t i = 0; i < (distance1 - distance2); ++i) nValues.push_back(*(itm1++));
          for (number_t i = 0; i < distance2; ++i) nValues.push_back(*(itm1++) + *(itm2++));
      }
  }

  // Update the rowPointer
  itrP1 = itbrP1;
  itrP2 = itbrP2;
  number_t updatePrev = 0, updateNext = 0;
  for (; itrP1 != (iterP1-1); ++itrP1, ++itrP2) {
      distance1 = *(itrP1+1) - *itrP1;
      distance2 = *(itrP2+1) - *itrP2;
      updateNext = std::max(distance1, distance2);
      if (*itrP1 != *itrP2) *itrP1 = updatePrev + *(itrP1-1);
      updatePrev = updateNext;
  }
  if (*itrP1 != *itrP2) *itrP1 = updatePrev + *(itrP1-1);

  itrP1 = itbrP1 = colPointer_.begin()+1; iterP1 = colPointer_.end();
  itrP2 = itbrP2 = colPtr2.begin()+1;
  // Update the upper part
  if (_noSymmetry != st2) itm2 = m2.begin()+diagonalSize()+1;
  switch (st2) {
  case _skewSymmetric:
      for (; itrP1 != (iterP1-1); ++itrP1, ++itrP2) {
          distance1 = *(itrP1+1) - *itrP1;
          distance2 = *(itrP2+1) - *itrP2;
          if (distance1 <= distance2) {
              for (number_t i = 0; i < (distance2 - distance1); ++i) nValues.push_back(-*(itm2++));
              for (number_t i = 0; i < distance1; ++i) nValues.push_back(*(itm1++) - *(itm2++));

          } else {
              for (number_t i = 0; i < (distance1 - distance2); ++i) nValues.push_back(*(itm1++));
              for (number_t i = 0; i < distance2; ++i) nValues.push_back(*(itm1++) - *(itm2++));
          }
      }
      break;
  case _selfAdjoint:
      for (; itrP1 != (iterP1-1); ++itrP1, ++itrP2) {
          distance1 = *(itrP1+1) - *itrP1;
          distance2 = *(itrP2+1) - *itrP2;
          if (distance1 <= distance2) {
              for (number_t i = 0; i < (distance2 - distance1); ++i) nValues.push_back(-*(itm2++));
              for (number_t i = 0; i < distance1; ++i) nValues.push_back(*(itm1++) + conj(*(itm2++)));
          } else {
              for (number_t i = 0; i < (distance1 - distance2); ++i) nValues.push_back(*(itm1++));
              for (number_t i = 0; i < distance2; ++i) nValues.push_back(*(itm1++) + conj(*(itm2++)));
          }
      }
      break;

  case _skewAdjoint:
      for (; itrP1 != (iterP1-1); ++itrP1, ++itrP2) {
          distance1 = *(itrP1+1) - *itrP1;
          distance2 = *(itrP2+1) - *itrP2;
          if (distance1 <= distance2) {
              for (number_t i = 0; i < (distance2 - distance1); ++i) nValues.push_back(-*(itm2++));
              for (number_t i = 0; i < distance1; ++i) nValues.push_back(*(itm1++) - conj(*(itm2++)));
          } else {
              for (number_t i = 0; i < (distance1 - distance2); ++i) nValues.push_back(*(itm1++));
              for (number_t i = 0; i < distance2; ++i) nValues.push_back(*(itm1++) - conj(*(itm2++)));
          }
      }
      break;
  default:
      for (; itrP1 != (iterP1-1); ++itrP1, ++itrP2) {
          distance1 = *(itrP1+1) - *itrP1;
          distance2 = *(itrP2+1) - *itrP2;
          if (distance1 <= distance2) {
              for (number_t i = 0; i < (distance2 - distance1); ++i) nValues.push_back(*(itm2++));
              for (number_t i = 0; i < distance1; ++i) nValues.push_back(*(itm1++) + (*(itm2++)));
          } else {
              for (number_t i = 0; i < (distance1 - distance2); ++i) nValues.push_back(*(itm1++));
              for (number_t i = 0; i < distance2; ++i) nValues.push_back(*(itm1++) + (*(itm2++)));
          }
      }
      break;
  }

  if (m1.size() != nValues.size()) m1.resize(nValues.size());
  std::copy(nValues.begin(), nValues.end(), m1.begin());

  // Update the and colPointer
  // Update the colPointer
  itrP1 = itbrP1;
  itrP2 = itbrP2;
  updatePrev = 0;
  updateNext = 0;
  for (; itrP1 != (iterP1-1); ++itrP1, ++itrP2) {
      distance1 = *(itrP1+1) - *itrP1;
      distance2 = *(itrP2+1) - *itrP2;
      updateNext = std::max(distance1, distance2);
      if (*itrP1 != *itrP2) *itrP1 = updatePrev + *(itrP1-1);
      updatePrev = updateNext;
  }
  if (*itrP1 != *itrP2) *itrP1 = updatePrev + *(itrP1-1);

  trace_p->pop();
}

/*!
   Extract and convert matrix storage to UMFPack format (Matlab sparse matrix)
   \param m1 vector values_ current matrix
   \param colPtUmf vector column Pointer of UMFPack format
   \param rowIdxUmf vector row Index of UMFPack format
   \param resultUmf vector values of UMFPack format
 */
template<typename M1, typename Idx>
void DualSkylineStorage::toUmfPack(const std::vector<M1>& m1, std::vector<Idx>& colPtUmf, std::vector<Idx>& rowIdxUmf, std::vector<M1>& resultUmf) const
{
  // Reserve memory for resultUmf and set its size to 0
  resultUmf.reserve(m1.size());
  resultUmf.clear();

  // Reserve memory for rowIdxUmf and set its size to 0
  rowIdxUmf.reserve(m1.size());
  rowIdxUmf.clear();

  // Resize column pointer as dualCs's column pointer size
  colPtUmf.clear();
  colPtUmf.resize(colPointer_.size(), Idx(0));

  // Because colPointer always begins with 0, so need to to count from the second position
  typename std::vector<Idx>::iterator itColPtUmf = colPtUmf.begin()+1;
  colPtUmf[0] = Idx(0);

  std::vector<number_t>::const_iterator itfColPt = colPointer_.begin();
  std::vector<number_t>::const_iterator itsColPt = colPointer_.begin()+1;

  std::vector<number_t>::const_iterator itbRowPt = rowPointer_.begin(), itfRowPt, itsRowPt;
  std::vector<number_t>::const_iterator iteRowPt = rowPointer_.end();

  typename std::vector<M1>::const_iterator itmDiag = m1.begin()+1;
  typename std::vector<M1>::const_iterator itmLow  = itmDiag + diagonalSize();
  typename std::vector<M1>::const_iterator itmUp   = itmLow + lowerPartSize();
  typename std::vector<M1>::const_iterator itval;

  Idx nvCol = Idx(0); // Number of non-zero in each "working" column
  number_t diagIdx = 0; // Index of diagonal elements
  int colIdx  = 0; // Index of current column

  for(; itsColPt != colPointer_.end(); ++itsColPt,++itfColPt,++itColPtUmf)
    {
      nvCol = *itsColPt - *itfColPt; // Number of value appearing in current column of upper part

      Idx nzCol = Idx(0); // Number of non-zero in each "working" column

      // Always process upper part first
      while(nvCol>0)
        {
          // Update non-zero values
          if(M1(0) != (*itmUp))
            {
              resultUmf.push_back(*itmUp);
              rowIdxUmf.push_back(colIdx-nvCol);
              ++nzCol;
            }
          ++itmUp;
          --nvCol;
        }

      // Process diagonal part
      if(diagIdx < diagonalSize())
        {
          if(M1(0) != *itmDiag)
            {
              // Update values
              resultUmf.push_back(*itmDiag);

              // Update colPtUmf with number of non-zero value
              ++nzCol;

              // Update rowIdxUmf
              rowIdxUmf.push_back(diagIdx);
            }
          ++itmDiag;
          ++diagIdx;
        }

      // Process lower part
      int rowIdx = 0;
      itfRowPt = itbRowPt;
      itsRowPt = itfRowPt+1;
      while(itsRowPt != iteRowPt)
        {
          int nzRow = *itsRowPt - *itfRowPt; // Number of "non-zero" values in a row
          if((int(0) < nzRow) && (colIdx < rowIdx))    // Only consider the row contains values in colIdx column)
            {
              int firstNzPos = rowIdx - nzRow; // Position (in column) of the first non zero value in a row
              if(firstNzPos <= colIdx)
                {
                  itval = itmLow + (*itfRowPt);
                  while(firstNzPos != colIdx) {++itval; ++firstNzPos;}
                  if(M1(0) != (*itval))
                    {
                      resultUmf.push_back(*itval); // Update values
                      rowIdxUmf.push_back(rowIdx); // Update row index


                      // Update colPtUmf with number of non-zero value
                      ++nzCol;
                    }
                }
            }
          ++itfRowPt; ++itsRowPt; ++rowIdx;
        }

      // Update colPtUmf with number of non-zero value
      *itColPtUmf += *(itColPtUmf-1) + nzCol;

      // Move to next column
      ++colIdx;
    }
}

#ifdef XLIFEPP_WITH_OMP
  /*!
  --------------------------------------------------------------------------------
    Template L.U factorization (Parallel version)
      Factorization of matrix M stored as matrix F = L U where
      - L is a lower triangular matrix with unit diagonal and is stored as
      the strict lower triangular part of F
      - U is a upper triangular matrix with diagonal stored as diagonal part of F
      and strict upper triangular part stored as the strict upper triangular part of F:

              \sum_{ max(C1(i),L1(j)) <= k <= j } L_{ik} U{kj} = M_{ij}

      where C1(i) is the column index of first non zero entry on row i
      -     L1(j) and the row index of first non zero entry on column j

      CAUTION Matrix M may have a symmetry property, but
              Matrix F ***MUST HAVE NO*** symmetry property since L and U are computed
              with a different set of rules (THIS IS NOT CHECKED HERE)
  --------------------------------------------------------------------------------
  */
  template<typename M>
  void DualSkylineStorage::luParallel(std::vector<M>& m, std::vector<M>& f) const
  {
    trace_p->push("DualSkylineStorage::luParallel");

    // iterators on diagonal (md), strict lower (ml) and strict upper (mu) triangular part of M
    typename std::vector<M>::iterator it_md(m.begin() + 1), it_ml(it_md + nbRows_), it_mu(it_ml);
    it_mu += *(rowPointer_.end() - 1);

    // iterators on diagonal (d), strict lower (l) and strict upper (u) triangular part
    // of non symmetric matrix F
    typename std::vector<M>::iterator it_fd(f.begin() + 1), it_fl(it_fd + nbRows_); //it_lnx(it_lb), it_l;
    typename std::vector<M>::iterator it_fu(it_fl +  * (rowPointer_.end() - 1));

    // iterators on row adresses starting with 2nd row and 2nd column (first row is row 0)
    std::vector<number_t>::const_iterator it_rownx(rowPointer_.begin()+1), it_row (it_rownx);
    std::vector<number_t>::const_iterator it_colnx(colPointer_.begin()+1), it_col (it_colnx);

    // first diagonal entry D_{11} = U_{11} = M_{11}
    if ( abs(*it_md) < theZeroThreshold ) { isSingular("L.U", 0); }

    number_t numThread = 1;
    #pragma omp parallel for lastprivate(numThread)
    for (number_t i = 0; i < 1; i++) {
      numThread = omp_get_num_threads();
    }

    const number_t BLOCKFACTOR = 0.05*std::min(nbOfRows(),nbOfColumns()); //1024;//2048; //1536; //1024; //128; //256; 512
    number_t numBlockMin = BLOCKFACTOR; //numThread * BLOCKFACTOR;
    if(numBlockMin==0) numBlockMin=1;
    number_t blockSize  = std::floor(real_t(diagonalSize())/numBlockMin);
    number_t nbBlockRow = std::ceil(real_t(nbOfRows())/blockSize);
    number_t nbBlockCol = std::ceil(real_t(nbOfColumns())/blockSize);
    number_t numBlock = std::min(nbBlockRow, nbBlockCol);

    std::vector<number_t> blockSizeRow(nbBlockRow, blockSize), blockSizeCol(nbBlockCol, blockSize);
    blockSizeRow[nbBlockRow-1] = nbRows_ - (nbBlockRow-1)*blockSize;
    blockSizeCol[nbBlockCol-1] = nbCols_ - (nbBlockCol-1)*blockSize;

    number_t k = 0, jj = 0, ii = 0;
    #pragma omp parallel private(k)
    for (k=0; k < numBlock;++k) {
      #pragma omp single
      diagBlockSolverParallel(k*blockSize, blockSizeRow[k], it_rownx,
                              k*blockSize, blockSizeCol[k], it_colnx,
                              it_fd, it_fl, it_fu,
                              it_md, it_ml, it_mu);
      #pragma omp for nowait
      for (jj = k+1; jj < nbBlockCol; ++jj) {
        #pragma omp task untied firstprivate(k, jj) \
                      shared(blockSize, blockSizeRow, blockSizeCol, it_rownx, it_colnx, it_fl, it_fu, it_mu)
        upperBlockSolverParallel(k*blockSize, blockSizeRow[k],it_rownx,
                            jj*blockSize, blockSizeCol[jj], it_colnx,
                            it_fl, it_fu, it_mu);
      }

      #pragma omp for
      for (ii = k+1; ii < nbBlockRow; ++ii) {
        #pragma omp task untied firstprivate(k, ii) \
                    shared(blockSize, blockSizeRow, blockSizeCol, it_rownx, it_colnx, it_fd, it_fl, it_fu, it_ml)
        lowerBlockSolverParallel(ii*blockSize, blockSizeRow[ii], it_rownx,
                              k*blockSize, blockSizeCol[k], it_colnx,
                              it_fd, it_fl, it_fu, it_ml);
      }
    }

    trace_p->pop();
  }
#endif


} // end of namespace xlifepp
