/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SkylineStorage.hpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 24 juin 2011
  \date 21 feb 2013

  \brief Definition of the xlifepp::SkylineStorage class

  xlifepp::SkylineStorage class is the abstract mother class of all skyline storage methods for large matrices
  \verbatim
  SkylineStorage --------> | DualSkylineStorage
                           | SkylineStorage
  \endverbatim

  Only _sym access type storage can be used for a matrix with a symmetry
  property ( _symmetric, _skewSymmetric, _selfAdjoint or _skewAdjoint)
*/

#ifndef SKYLINE_STORAGE_HPP
#define SKYLINE_STORAGE_HPP

#include "config.h"
#include "../MatrixStorage.hpp"

#ifdef XLIFEPP_WITH_OMP
  #include <omp.h>
#endif

namespace xlifepp {

/*!
   \class SkylineStorage
   abstract base class of all skyline storage classes
 */
class SkylineStorage : public MatrixStorage
{
public:

  // Constructors, Destructor
  SkylineStorage(AccessType = _dual, string_t id="SkylineStorage");                 //!< constructor by access type
  SkylineStorage(number_t, AccessType = _dual, string_t id="SkylineStorage");         //!< constructor by access type, number of columns and rows (same)
  SkylineStorage(number_t, number_t, AccessType = _dual, string_t id="SkylineStorage"); //!< constructor by access type, number of columns and rows
  virtual ~SkylineStorage() {}                        //!< virtual destructor

  // public utilities
  virtual number_t size() const = 0;                       //!< storage size
  virtual number_t lowerPartSize() const = 0;              //!< size of lower triangular part except for Dual/SkylineStorage
  virtual number_t upperPartSize() const {return 0;}       //!< size of upper triangular part except for Dual/SkylineStorage
  virtual MatrixStorage* toSkyline(AccessType at=_dual); //!< create skyline storage from current storage
  virtual const std::vector<number_t>& rowPointer() const = 0; //!< rowPointer of skyline storage
  virtual const std::vector<number_t>& colPointer() const = 0; //!< colPointer of skyline storage (return rowPointer in _sym access)
  MatrixStorage* toDual();                               //!< create a new skyline storage from sym skyline storage

protected:

  void addSkylineSubMatrixIndices(std::vector<number_t>&, const std::vector<number_t>&,
                            const std::vector<number_t>&);  //!< add to storage dense submatrix indices given by its row and column indices (>= 1)

  // print utilites for child class
  template<typename Iterator>
  void printEntriesTriangularPart(StrucType, Iterator&, Iterator&, const std::vector<number_t>&,
                                  number_t, number_t, number_t, const string_t&, number_t, std::ostream&) const;  //!< print lower/upper part of matrix in sym/dual compressed storage
  template<typename Iterator>
  void printCooTriangularPart(std::ostream&, Iterator&, const std::vector<number_t>&,
                              number_t, number_t, bool, SymType sym = _noSymmetry) const;                   //!< print lower/upper part of matrix in coordinate format
  template<typename Iterator>
  void printCooDiagonalPart(std::ostream&, Iterator&, number_t) const;                                    //!< print diagonal of matrix in coordinate format

  //! load matrix from file in dense format
  template <typename T>
  void loadSkylineFromFileDense(std::istream&, std::vector<T>&, std::vector<number_t>&,
                                std::vector<number_t>&, SymType, bool);

  //! load matrix from file in coordinate format
  template <typename T>
  void loadSkylineFromFileCoo(std::istream&, std::vector<T>&, std::vector<number_t>&,
                              std::vector<number_t>&, SymType, bool);

  //@{
  //! product of any part of Skyline matrix and vector common to different Skyline storages (child classes)
  template<typename MatIterator, typename VecIterator, typename ResIterator>
  void diagonalMatrixVector(MatIterator&, VecIterator&, ResIterator&, ResIterator&) const;
  template<typename MatIterator, typename VecIterator, typename ResIterator>
  void diagonalVectorMatrix(MatIterator&, VecIterator&, ResIterator&, ResIterator&) const;
  template<typename MatIterator, typename VecIterator, typename ResIterator>
  void lowerMatrixVector(const std::vector<number_t>& pointer,
                         MatIterator& itm, VecIterator& itvb, ResIterator& itrb, SymType sym = _noSymmetry) const;
  template<typename MatIterator, typename VecIterator, typename ResIterator>
  void lowerVectorMatrix(const std::vector<number_t>& pointer,
                         MatIterator& itm, VecIterator& itvb, ResIterator& itrb, SymType sym = _noSymmetry) const;
  template<typename MatIterator, typename VecIterator, typename ResIterator>
  void upperMatrixVector(const std::vector<number_t>& pointer,
                         MatIterator& itm, VecIterator& itvb, ResIterator& itrb, SymType sym = _noSymmetry) const;
  template<typename MatIterator, typename VecIterator, typename ResIterator>
  void upperVectorMatrix(const std::vector<number_t>& pointer,
                         MatIterator& itm, VecIterator& itvb, ResIterator& itrb, SymType sym = _noSymmetry) const;
  template<typename MatIterator, typename VecIterator, typename XIterator>
  void bzLowerSolver(const MatIterator&, const MatIterator&, const VecIterator&, const XIterator&, const XIterator&, const std::vector<number_t>::const_iterator) const;

  template<typename MatIterator, typename VecIterator, typename XIterator>
  void bzLowerD1Solver(const MatIterator&, const VecIterator&, const XIterator&, const XIterator&, const std::vector<number_t>::const_iterator) const;

  template<typename MatIterator, typename VecIterator, typename XIterator>
  void bzLowerConjD1Solver(const MatIterator&, const VecIterator&, const XIterator&, const XIterator&, const std::vector<number_t>::const_iterator) const;

  template<typename MatIterator, typename VecIterator, typename XIterator>
  void bzDiagonalSolver(const MatIterator&, const VecIterator&, const XIterator&, const XIterator&) const;

  template<typename MatRevIterator, typename VecRevIterator, typename XRevIterator>
  void bzUpperSolver(const MatRevIterator&, const MatRevIterator&, const VecRevIterator&, const XRevIterator&, const XRevIterator&, const std::vector<number_t>::const_reverse_iterator) const;

  template<typename MatRevIterator, typename VecRevIterator, typename XRevIterator>
  void bzUpperD1Solver(const MatRevIterator&, const VecRevIterator&, const XRevIterator&, const XRevIterator&, const std::vector<number_t>::const_reverse_iterator) const;

  template<typename MatRevIterator, typename VecRevIterator, typename XRevIterator>
  void bzUpperConjD1Solver(const MatRevIterator&, const VecRevIterator&, const XRevIterator&, const XRevIterator&, const std::vector<number_t>::const_reverse_iterator) const;
  //@}

  //! sum of any part of dense matrix and dense matrix
  template<typename Mat1Iterator, typename Mat2Iterator, typename ResIterator>
  void sumMatrixMatrix(Mat1Iterator&, Mat2Iterator&, ResIterator&, ResIterator&) const;

  //! Extract thread index for block of column or row
  void extractThreadIndex(const std::vector<number_t>& pointer, number_t& numThread,
                          std::vector<std::vector<number_t>::const_iterator>& itLowerThread,
                          std::vector<std::vector<number_t>::const_iterator>& itUpperThread) const;

  #ifdef XLIFEPP_WITH_OMP
    template<typename MatIterator, typename VecIterator, typename ResIterator>
    void parallelLowerMatrixVector(const std::vector<number_t>& pointer,
                                    MatIterator& itm, VecIterator& itvb, ResIterator& itrb,
                                    number_t numThread,
                                    std::vector<std::vector<number_t>::const_iterator>& itThreadLower,
                                    std::vector<std::vector<number_t>::const_iterator>& itThreadUpper,
                                    SymType sym) const;

    template<typename MatIterator, typename VecIterator, typename ResIterator>
    void parallelUpperMatrixVector(const std::vector<number_t>& pointer,
                                    MatIterator& itm, VecIterator& itvb, ResIterator& itrb,
                                    number_t numThread,
                                    std::vector<std::vector<number_t>::const_iterator>& itThreadLower,
                                    std::vector<std::vector<number_t>::const_iterator>& itThreadUpper,
                                    number_t numRes,
                                    SymType sym) const;

    template<typename MatIterator, typename NumberIterator>
    void upperSolverParallel(number_t row, number_t rowEntries, MatIterator it_fl,
                              number_t col, MatIterator it_fu, NumberIterator itcol_f,
                              MatIterator it_mu) const;

    template<typename MatIterator, typename NumberIterator>
    void lowerSolverParallel(number_t col, number_t colEntries, MatIterator it_fu, MatIterator it_fd,
                              number_t row, MatIterator it_fl, NumberIterator itrow_f,
                              MatIterator it_ml) const;

    template<typename MatIterator, typename NumberIterator>
    void lowerSymSolverParallel(number_t col, number_t colEntries, MatIterator it_fu, MatIterator it_fd,
                                number_t row, MatIterator it_fl, NumberIterator itrow_f,
                                MatIterator it_ml) const;

    template<typename MatIterator, typename NumberIterator>
    void lowerSymConjSolverParallel(number_t col, number_t colEntries, MatIterator it_fu, MatIterator it_fd,
                                    number_t row, MatIterator it_fl, NumberIterator itrow_f,
                                    MatIterator it_ml) const;

    template<typename MatIterator, typename NumberIterator>
    void diagBlockSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
                                  number_t colIdx, number_t blockColSize, NumberIterator colPointer,
                                  MatIterator it_fd, MatIterator it_fl, MatIterator it_fu,
                                  MatIterator it_md, MatIterator it_ml, MatIterator it_mu) const;

    template<typename MatIterator, typename NumberIterator>
    void diagBlockSymSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
                                    number_t colIdx, number_t blockColSize, NumberIterator colPointer,
                                    MatIterator it_fd, MatIterator it_fl, MatIterator it_fu,
                                    MatIterator it_md, MatIterator it_ml, MatIterator it_mu) const;

    template<typename MatIterator, typename NumberIterator>
    void diagBlockSymConjSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
                                        number_t colIdx, number_t blockColSize, NumberIterator colPointer,
                                        MatIterator it_fd, MatIterator it_fl, MatIterator it_fu,
                                        MatIterator it_md, MatIterator it_ml, MatIterator it_mu) const;

    template<typename MatIterator, typename NumberIterator>
    void upperBlockSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
                                  number_t colIdx, number_t blockColSize, NumberIterator colPointer,
                                  MatIterator it_fl, MatIterator it_fu, MatIterator it_mu) const;

    template<typename MatIterator, typename NumberIterator>
    void lowerBlockSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
                                  number_t colIdx, number_t blockColSize, NumberIterator colPointer,
                                  MatIterator it_fd, MatIterator it_fl, MatIterator it_fu, MatIterator it_ml) const;

    template<typename MatIterator, typename NumberIterator>
    void lowerBlockSymSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
                                      number_t colIdx, number_t blockColSize, NumberIterator colPointer,
                                      MatIterator it_fd, MatIterator it_fl, MatIterator it_fu, MatIterator it_ml) const;

    template<typename MatIterator, typename NumberIterator>
    void lowerBlockSymConjSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
                                          number_t colIdx, number_t blockColSize, NumberIterator colPointer,
                                          MatIterator it_fd, MatIterator it_fl, MatIterator it_fu, MatIterator it_ml) const;
  #endif
};

/*
--------------------------------------------------------------------------------
  Template functions
--------------------------------------------------------------------------------
*/

// print matrix of scalars for sym or dual cs strorage
// itd: iterator on matrix diagonal entries
// itlu: iterator on matrix lower or upper triangular part entries
//  index: numbers of cols (resp. rows) on rows (resp. cols)
//  pointer: adresses of the beginning of rows (resp. cols)
template<typename Iterator>
void SkylineStorage::printEntriesTriangularPart(StrucType st, Iterator& itd, Iterator& itlu, const std::vector<number_t>& pointer, number_t perRow, number_t width, number_t prec, const string_t& rowOrcol, number_t vb, std::ostream& os) const
{
  number_t rcmin = std::min(vb, pointer.size() - 1); // number of rows or cols to print
  string_t f = "firste";
  if(rcmin > 1) { f = "firstes"; }
  os << "(" << words(f) << " " << rcmin << " " << words(rowOrcol) << "s.)";
  os.setf(std::ios::scientific);
  string_t colOrrow;
  if(rowOrcol == "row") { colOrrow = "col"; }
  else { colOrrow = "row"; }
  for(number_t rc = 0; rc < rcmin; rc++)
  {
    number_t nnz = pointer[rc + 1] - pointer[rc]; // number of non zeros on row/col rc
    os << eol << "  " << words(rowOrcol) << " " << rc + 1;
    if(nnz == 0)  // only diagonal coef.
    {
      os << " (1 " << words("entry") << ", " << words(colOrrow) << " : " << rc + 1 << ")";
      if(st == _scalar) { printRowWise(os, "   ", perRow - 1, width, prec, itd , itd + 1); }
      else { os << std::setw(width) << std::setprecision(prec) << *itd; }
      itd++;
    }
    if(nnz > 0)   // non zero coefs. and diagonal coef.
    {
      os << " (" << nnz + 1 << " " << words("entries") << ", " << words(colOrrow) << " : ";
      for(number_t i = rc - nnz; i < rc; i++)  { os << " " << i + 1; }
      os << " " << (rc + 1) << ")";
      if(st == _scalar)  { printRowWise(os, "   ", perRow - 1, width, prec, itlu , itlu + nnz); }
      else for(Iterator it = itlu; it < itlu + nnz; it++) { os << *it; }
      os << std::setw(width) << std::setprecision(prec) << *itd++;
      itlu += nnz;
    }
  }
  os.unsetf(std::ios::scientific);
  os << std::endl;
}

/*--------------------------------------------------------------------------------
 print "triangular" part of matrix to ostream in coordinate form  i j A_ij:
    1 1 A11
    1 2 A12
    ...
 a null value or a value less than a tolerance in module is not displayed
 The coordinates are not ordered
--------------------------------------------------------------------------------*/
template<typename Iterator>
void SkylineStorage::printCooTriangularPart(std::ostream& os, Iterator& itm, const std::vector<number_t>& pointer, number_t nbr, number_t nbc, bool byRow, SymType sym) const
{
  std::vector<number_t>::const_iterator itr = pointer.begin();
  number_t k, l;

  for(number_t i = 1; i <= nbr; i++, itr++)      // loop on rows/cols
  {
    number_t nnz = *(itr + 1) - *itr, c = std::min(i, nbc) - nnz;
    for(number_t j = c; j < c + nnz; j++, itm++) // loop on cols/rows
    {
      if(byRow) {k = i; l = j;}
      else {k = j; l = i;}
      switch(sym)
      {
        case _skewSymmetric: printCoo(os, -*itm, k, l, 0.); break;
        case _skewAdjoint: printCoo(os, -conj(*itm), k, l, 0.); break;
        case _selfAdjoint: printCoo(os, conj(*itm), k, l, 0.); break;
        default: printCoo(os, *itm, k, l, 0.);
      }
    }
  }
}

template<typename Iterator>
void SkylineStorage::printCooDiagonalPart(std::ostream& os, Iterator& itm, number_t n) const
{
  for(number_t i = 1; i <= n; i++, itm++)   { printCoo(os, *itm, i, i, 0.); }
}


/*--------------------------------------------------------------------------------
 load matrix from file including a dense matrix
    A11 A12  ...                            Re(A11) Im(A11) Re(A12) IM(A12) ....
    A21 A22  ...    or if complex values    Re(A21) Im(A21) Re(A22) IM(A22) ....
    ...                                     ...
 values in module less than the tolerance are considered to be outside the storage
 note: algorithms use pos(i,j) function; they are not optimal!
 arguments:
   ifs: istream for reading
   mat: values of matrix
   colPointer: col index of non zero coefficients (for lower triangular part)
   rowPointer: row or col pointer in crIndex (for upper triangular part)
   sym: symmetry property of the matrix (_noSymmetry, _symmetric, ...)
--------------------------------------------------------------------------------*/
//for dual or sym skyline storage
template <typename T>
void SkylineStorage::loadSkylineFromFileDense(std::istream& ifs, std::vector<T>& mat, std::vector<number_t>& rowPointer, std::vector<number_t>& colPointer, SymType sym, bool realAsCmplx)
{
  trace_p->push("SkylineStorage::loadSkylineFromFileDense");
  if(accessType_ != _dual && accessType_ != _sym) { error("storage_not_handled", words("storage type",_skyline), words("access type",accessType_)); }

  // initialisation
  bool upper = (accessType_ == _dual) || (accessType_ == _sym && sym == _noSymmetry);
  std::vector<std::vector<T> > lowValues, upValues;
  std::vector<T> diagValues;
  diagValues.resize(diagonalSize());
  rowPointer.resize(nbRows_ + 1, theNumberMax); lowValues.resize(nbRows_);
  std::vector<number_t> colPointerSym;
  std::vector<number_t>* colPointer_p = &colPointerSym;
  if(accessType_ == _dual) { colPointer_p = &colPointer; }
  if(upper) { colPointer_p->resize(nbCols_ + 1, theNumberMax); upValues.resize(nbCols_);}
  // read the file and construct the column and row indices of "non zeros"
  std::vector<number_t>::iterator itr = rowPointer.begin()+1, itc;
  typename std::vector<std::vector<T> >::iterator itlv = lowValues.begin(), ituv;
  typename std::vector<T>::iterator itd = diagValues.begin();
  for(number_t i = 1; i <= nbRows_; i++, itr++, itlv++, itd++)
  {
    number_t cmax = std::min(i - 1, nbCols_);
    for(number_t j = 1; j <= cmax; j++)            // strict lower part
    {
      T v; readItem(ifs, v, realAsCmplx);
      if(*itr == theNumberMax &&  std::abs(v) > theTolerance) { *itr = j - 1; } // set rowpointer for row i
      if(*itr != theNumberMax) { itlv->push_back(v); }
    }
    if(i <= nbCols_)   { readItem(ifs, *itd, realAsCmplx); }  // diagonal part
    for(number_t j = cmax + 2; j <= nbCols_; j++)           // strict upper part
    {
      T v; readItem(ifs, v, realAsCmplx);
      if(upper)
      {
        itc = colPointer_p->begin() + j;// - 1;
        ituv = upValues.begin() + j - 1;
        if(*itc == theNumberMax &&  std::abs(v) > theTolerance) { *itc = i - 1; } // set rowpointer for col j
        if(*itc != theNumberMax) { ituv->push_back(v); }
      }
    }
  }

  // update skyline storage vectors
  itr = rowPointer.begin();
  *itr = 0; itr++;
  number_t l = 0, r;
  for(number_t i = 1; i <= nbRows_; i++, itr++)
  {
    if (theNumberMax == *itr) *itr = *(itr-1);
    else
    {
      r = *itr;
      l = std::min(i-1, nbCols_) - r;
      *itr = *(itr - 1) + l;
    }
  }
  if(upper)
  {
    itc = colPointer_p->begin();
    *itc = 0; itc++;
    l = 0; r = 0;
    for(number_t j = 1; j <= nbCols_; j++, itc++)
    {
      if (theNumberMax == *itc) *itc = *(itc-1);
      else
      {
        r = *itc;
        l = std::min(j-1, nbRows_) - r;
        *itc = *(itc - 1) + l;
      }
    }
  }

  // if matrix declared _noSymmetry with symmetric storage, check if the read matrix have a symetric storage
  if(accessType_ == _sym && sym == _noSymmetry)
  {
    itc = colPointer_p->begin();
    for(itr = rowPointer.begin(); itr != rowPointer.end(); itr++, itc++)
      if(*itr != *itc) { error("largematrix_nosymstorage", "loadSkylineFromFileDense"); }
  }
  // assign matrix values
  if(upper) { mat.resize(diagonalSize() + lowerPartSize() + upperPartSize() + 1); }
  else      { mat.resize(diagonalSize() + lowerPartSize() + 1); }
  // load values in "matrix"
  mat[0] = T();
  typename std::vector<T>::iterator itm = mat.begin() + 1;
  typename std::vector<std::vector<T> >::iterator itvv;
  typename std::vector<T>::iterator itv;
  std::vector<std::vector<number_t> >::iterator itrc;
  for(itv = diagValues.begin(); itv != diagValues.end(); itv++, itm++) { *itm = *itv; } // diagonal part
  for(itvv = lowValues.begin(); itvv != lowValues.end(); itvv++)      // lower triangular part
    for(itv = itvv->begin(); itv != itvv->end(); itv++, itm++)  { *itm = *itv; }
  if(upper)  // upper triangular part
  {
    for(itvv = upValues.begin(); itvv != upValues.end(); itvv++)    // upper triangular part
      for(itv = itvv->begin(); itv != itvv->end(); itv++, itm++)  { *itm = *itv; }
  }
  trace_p->pop();
}

/*--------------------------------------------------------------------------------
 load matrix from file including a matrix in coordinate format
    i j Aij      or if complex values    i j Re(Aij) Im(Aij)
    ...                                     ...
 contrary to dense matrix file, zero values are kept in compressed storage
   ifs: istream for reading
   mat: values of matrix
   colPointer: col index of non zero coefficients (for lower triangular part)
   rowPointer: row or col pointer in crIndex (for upper triangular part)
   sym: symmetry property of the matrix (_noSymmetry, _symmetric, ...)
--------------------------------------------------------------------------------*/
template <typename T>
void SkylineStorage::loadSkylineFromFileCoo(std::istream& ifs, std::vector<T>& mat, std::vector<number_t>& rowPointer, std::vector<number_t>& colPointer, SymType sym, bool realAsCmplx)
{
  trace_p->push("CsStorage::loadSkylineFromFileCoo");
  if(accessType_ != _dual && accessType_ != _sym) { error("storage_not_handled", words("storage type",_skyline), words("access type",accessType_)); }

  // initialisation
  bool upper = (accessType_ == _dual) || (accessType_ == _sym && sym == _noSymmetry);
  number_t i, j;
  std::map<std::pair<number_t, number_t>, T> values;         // to store matrix values with coordinates access
  while(!ifs.eof())  // read values and store them in a map indexed by coordinates matrix
  {
    ifs >> i >> j;
    T v; readItem(ifs, v, realAsCmplx);
    if((i >= j) || ((i < j) && upper))
    { values[std::pair<number_t, number_t>(i, j)] = v; }
  }
  // build storage
  rowPointer.resize(nbRows_ + 1, theNumberMax);
  if(accessType_ == _dual) { colPointer.resize(nbCols_ + 1, theNumberMax); }
  typename std::map<std::pair<number_t, number_t>, T>::iterator itmv;
  for(itmv = values.begin(); itmv != values.end(); itmv++)  //first non zero column index
  {
    number_t i = itmv->first.first, j = itmv->first.second, c = std::min(i, nbCols_);
    if(j < c) { rowPointer[i-1] = std::min(rowPointer[i-1], j - 1); }
    if(true == upper)
    {
      c = std::min(j, nbRows_);
      if(i < c) { colPointer[j - 1] = std::min(colPointer[j - 1], i - 1); }
    }
  }
  //update row pointer
  std::vector<number_t>::iterator itb=rowPointer.begin(), ite=rowPointer.end(), it;
  number_t temp = 0, c = 0;
  *itb = 0;
  for (it = itb+1; it != ite; it++)
  {
    if (theNumberMax == *it)
    {
      *it = *(it-1);
      c = 0;
    }
    else
    {
      i = it - itb;
      temp = i - *it;
      *(it) = *(it-1) + c;
      c = temp;
    }
  }
  *(ite-1) = *(ite-2)+temp;
  if(upper) //update col pointer
  {
    itb = colPointer.begin();
    ite = colPointer.end();
    *itb = 0;
    number_t c = 0;
    for (it = itb+1; it != ite; it++)
    {
      if (theNumberMax == *it)
      {
        *it = *(it-1);
        c = 0;
      }
      else
      {
        i = it - itb;
        temp = i - *it;
        *(it) = *(it-1) + c;
        c = temp;
      }
    }
    *(ite-1) = *(ite-2)+temp;
  }

  if(upper) { mat.resize(diagonalSize() + lowerPartSize() + upperPartSize() + 1); }
  else      { mat.resize(diagonalSize() + lowerPartSize() + 1); }

  // assign matrix values
  for(itmv = values.begin(); itmv != values.end(); itmv++)
  {
    i = itmv->first.first;
    j = itmv->first.second;
    mat[pos(i, j)] = itmv->second;
  }
  trace_p->pop();
}

/*
--------------------------------------------------------------------------------
   template Matrix x Vector partial multiplications used in child classes
   CAUTION ! We use non-commutative * operations here as we may deal
             with overloaded matrix-vector left or right products
   in functions, iterators have the following meaning
     itd: iterator on matrix diagonal entries (begin)
     itm: iterator on matrix entries (lower "triangular" part)
     itvb: iterator on given vector entries (begin)
     itve: iterator on given vector entries (end)
     itrb: iterator on result vector entries (begin)
     itre: iterator on result vector entries (end)
     pointer: adresses in index of beginning of rows or cols
   Result vector res *** NEED NOT BE INITIALIZED HERE ***
--------------------------------------------------------------------------------
*/
// partial multiplication by diagonal M*v, matrix need not be square
// should be called first in a product matrix*vector (result vector initialisation)
template<typename MatIterator, typename VecIterator, typename ResIterator>
void SkylineStorage::diagonalMatrixVector(MatIterator& itd, VecIterator& itvb, ResIterator& itrb, ResIterator& itre) const
{
  VecIterator itv = itvb;
  ResIterator itr;
  for(itr = itrb; itr != itrb + diagonalSize(); itr++, itd++, itv++) { *itr = *itd **itv; }
  for(itr = itrb + diagonalSize(); itr != itre; itr++) { *itr *= 0; }     // *itr=0 : ambiguous resolution in Vector<K>::operator = ?!
}

// partial multiplication by diagonal v*M, matrix need not be square
template<typename MatIterator, typename VecIterator, typename ResIterator>
void SkylineStorage::diagonalVectorMatrix(MatIterator& itd, VecIterator& itvb, ResIterator& itrb, ResIterator& itre) const
{
  VecIterator itv = itvb;
  ResIterator itr;
  for(itr = itrb; itr != itrb + diagonalSize(); itr++, itd++, itv++) { *itr = *itv **itd; }
  for(itr = itrb + diagonalSize(); itr != itre; itr++) { *itr *= 0; }     // *itr=0 : ambiguous resolution in Vector<K>::operator = ?!
}

// partial multiplication by lower triangular part
// also used in multiplication of upper part of tranposed matrix by vector
// should be called after Diagonal multiplication
template<typename MatIterator, typename VecIterator, typename ResIterator>
void SkylineStorage::lowerMatrixVector(const std::vector<number_t>& pointer,
                                       MatIterator& itm, VecIterator& itvb, ResIterator& itrb, SymType sym) const
{
  #ifdef XLIFEPP_WITH_OMP
    // Get the number of thread to execute code
    number_t numThread = 4;
    #pragma omp parallel for lastprivate(numThread)
    for (number_t i = 0; i < 1; i++)
    {
      numThread = omp_get_num_threads();
    }

    const number_t GRANULARITY = 16;
    numThread *= GRANULARITY;

    // Lower and upper bound position for each thread
    std::vector<std::vector<number_t>::const_iterator> itThreadLower(numThread);
    std::vector<std::vector<number_t>::const_iterator> itThreadUpper(numThread);
    extractThreadIndex(pointer, numThread, itThreadLower, itThreadUpper);
    parallelLowerMatrixVector(pointer, itm, itvb, itrb, numThread, itThreadLower, itThreadUpper, sym);

  #else
    std::vector<number_t>::const_iterator itpb = pointer.begin(), itpe = pointer.end() - 1, itp;
    ResIterator itr = itrb;
    VecIterator itv;
    number_t r = 0;

    switch(sym)
    {
      case _skewSymmetric:
        for(itp = itpb; itp != itpe; ++itp, ++itr, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itv = itvb + r - nnz;
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itv) { *itr -= *itm **itv; }
        }
        break;
      case _selfAdjoint:
        for(itp = itpb; itp != itpe; ++itp, ++itr, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itv = itvb + r - nnz;
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itv) { *itr += conj(*itm) **itv; }
        }
        break;
      case _skewAdjoint:
        for(itp = itpb; itp != itpe; ++itp, ++itr, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itv = itvb + r - nnz;
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itv) { *itr -= conj(*itm) **itv; }
        }
        break;
      default: // symmetric matrix or non symmetric
        for(itp = itpb; itp != itpe; ++itp, ++itr, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itv = itvb + r - nnz;
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itv) { *itr += *itm **itv; }
        }
    }
  #endif
}

template<typename MatIterator, typename VecIterator, typename ResIterator>
void SkylineStorage::lowerVectorMatrix(const std::vector<number_t>& pointer,
                                       MatIterator& itm, VecIterator& itvb, ResIterator& itrb, SymType sym) const
{
  #ifdef XLIFEPP_WITH_OMP
    // Get the number of thread to execute code
    number_t numThread = 4;
    #pragma omp parallel for lastprivate(numThread)
    for (number_t i = 0; i < 1; i++)
    {
      numThread = omp_get_num_threads();
    }

    const number_t GRANULARITY = 16;
    numThread *= GRANULARITY;

    // Lower and upper bound position for each thread
    std::vector<std::vector<number_t>::const_iterator> itThreadLower(numThread);
    std::vector<std::vector<number_t>::const_iterator> itThreadUpper(numThread);
    extractThreadIndex(pointer, numThread, itThreadLower, itThreadUpper);
    parallelUpperMatrixVector(pointer, itm, itvb, itrb, numThread, itThreadLower, itThreadUpper, MatrixStorage::nbCols_, sym);

  #else
    std::vector<number_t>::const_iterator itpb = pointer.begin(), itpe = pointer.end() - 1, itp;
    VecIterator itv = itvb;
    ResIterator itr;
    number_t r = 0;

    switch(sym)
    {
      case _skewSymmetric:
        for(itp = itpb; itp != itpe; ++itp, ++itv, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itr = itrb + r - nnz;
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itr) { *itr -= *itv **itm; }
        }
        break;
      case _selfAdjoint:
        for(itp = itpb; itp != itpe; ++itp, ++itv, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itr = itrb + r - nnz;
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itr) { *itr += *itv * conj(*itm); }
        }
        break;
      case _skewAdjoint:
        for(itp = itpb; itp != itpe; ++itp, ++itv, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itr = itrb + r - nnz;
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itr) { *itr -= *itv * conj(*itm); }
        }
        break;
      default: // symmetric matrix or non symmetric
        for(itp = itpb; itp != itpe; ++itp, ++itv, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itr = itrb + r - nnz;
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itr) { *itr += *itv **itm; }
        }
    }
  #endif
}

// partial multiplication by upper triangular part
// also used in multiplication of upper part of tranposed matrix by vector
// should be called after Diagonal multiplication
template<typename MatIterator, typename VecIterator, typename ResIterator>
void SkylineStorage::upperMatrixVector(const std::vector<number_t>& pointer,
                                       MatIterator& itm, VecIterator& itvb, ResIterator& itrb, SymType sym) const
{
  #ifdef XLIFEPP_WITH_OMP
    // Get the number of thread to execute code
    number_t numThread = 4;
    #pragma omp parallel for lastprivate(numThread)
    for (number_t i = 0; i < 1; i++)
    {
      numThread = omp_get_num_threads();
    }

    const number_t GRANULARITY = 16;
    numThread *= GRANULARITY;

    // Lower and upper bound position for each thread
    std::vector<std::vector<number_t>::const_iterator> itThreadLower(numThread);
    std::vector<std::vector<number_t>::const_iterator> itThreadUpper(numThread);
    extractThreadIndex(pointer, numThread, itThreadLower, itThreadUpper);
    parallelUpperMatrixVector(pointer, itm, itvb, itrb, numThread, itThreadLower, itThreadUpper, MatrixStorage::nbRows_, sym);

  #else
    std::vector<number_t>::const_iterator itpb = pointer.begin(), itpe = pointer.end() - 1, itp;
    VecIterator itv = itvb;
    ResIterator itr;
    number_t r = 0;

    switch(sym)
    {
      case _skewSymmetric:
        for(itp = itpb; itp != itpe; ++itp, ++itv, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itr = itrb + r - nnz;
          //for(number_t c = 0; c < nnz; c++, itm++, itr++) { *itr -= *itm * *itv; }
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itr) { *itr -= *itv **itm; }  //correction for block matrix
        }
        break;
      case _selfAdjoint:
        for(itp = itpb; itp != itpe; ++itp, ++itv, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itr = itrb + r - nnz;
          //for(number_t c = 0; c < nnz; c++, itm++, itr++) { *itr += conj(*itm) * *itv; }
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itr) { *itr += *itv * conj(*itm); } //correction for block matrix
        }
        break;
      case _skewAdjoint:
        for(itp = itpb; itp != itpe; ++itp, ++itv, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itr = itrb + r - nnz;
          //for(number_t c = 0; c < nnz; c++, itm++, itr++) { *itr -= conj(*itm) * *itv; }
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itr) { *itr -= *itv * conj(*itm); } //correction for block matrix
        }
        break;
      case _symmetric:
        for(itp = itpb; itp != itpe; ++itp, ++itv, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itr = itrb + r - nnz;
          //for(number_t c = 0; c < nnz; c++, itm++, itr++) { *itr += *itm** itv; }
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itr) { *itr += *itv **itm; } //correction for block matrix
        }
        break;
      default: // no symmetry
        for(itp = itpb; itp != itpe; ++itp, ++itv, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itr = itrb + r - nnz;
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itr) { *itr += *itv **itm; }
        }
    }
  #endif
}

template<typename MatIterator, typename VecIterator, typename ResIterator>
void SkylineStorage::upperVectorMatrix(const std::vector<number_t>& pointer,
                                       MatIterator& itm, VecIterator& itvb, ResIterator& itrb, SymType sym) const
{
  #ifdef XLIFEPP_WITH_OMP
    // Get the number of thread to execute code
    number_t numThread = 4;
    #pragma omp parallel for lastprivate(numThread)
    for (number_t i = 0; i < 1; i++)
    {
      numThread = omp_get_num_threads();
    }

    const number_t GRANULARITY = 16;
    numThread *= GRANULARITY;

    // Lower and upper bound position for each thread
    std::vector<std::vector<number_t>::const_iterator> itThreadLower(numThread);
    std::vector<std::vector<number_t>::const_iterator> itThreadUpper(numThread);
    extractThreadIndex(pointer, numThread, itThreadLower, itThreadUpper);
    parallelLowerMatrixVector(pointer, itm, itvb, itrb, numThread, itThreadLower, itThreadUpper, sym);

  #else
    std::vector<number_t>::const_iterator itpb = pointer.begin(), itpe = pointer.end() - 1, itp;
    ResIterator itr = itrb;
    VecIterator itv;
    number_t r = 0;

    switch(sym)
    {
      case _skewSymmetric:
        for(itp = itpb; itp != itpe; ++itp, ++itr, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itv = itvb + r - nnz;
          //for(number_t c = 0; c < nnz; c++, itm++, itv++) { *itr -= *itv * *itm; }
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itv) { *itr -= *itm **itv; }
        }
        break;
      case _selfAdjoint:
        for(itp = itpb; itp != itpe; ++itp, ++itr, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itv = itvb + r - nnz;
          //for(number_t c = 0; c < nnz; c++, itm++, itv++) { *itr += *itv * conj(*itm); }
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itv) { *itr += conj(*itm) **itv; }
        }
        break;
      case _skewAdjoint:
        for(itp = itpb; itp != itpe; ++itp, ++itr, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itv = itvb + r - nnz;
          //for(number_t c = 0; c < nnz; c++, itm++, itv++) { *itr -= *itv * conj(*itm); }
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itv) { *itr -= conj(*itm) **itv; }
        }
        break;
      case _symmetric: // symmetric matrix
        for(itp = itpb; itp != itpe; ++itp, ++itr, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itv = itvb + r - nnz;
          //for(number_t c = 0; c < nnz; c++, itm++, itv++) { *itr += *itv * *itm; }
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itv) { *itr += *itm **itv; }
        }
        break;
      default: // no symmetry
        for(itp = itpb; itp != itpe; ++itp, ++itr, ++r)
        {
          number_t nnz = *(itp + 1) - *itp;
          itv = itvb + r - nnz;
          for(number_t c = 0; c < nnz; ++c, ++itm, ++itv) { *itr += *itv **itm; }
        }
    }
  #endif
}

/*!
  Template solver for lower triangular matrix
  (used for factorization or as downwards solver for Symm & Dual Skyline Storages)
  *** Matrix is assumed INVERTIBLE here ***, no check on division by zero is done
*/
template<typename MatIterator, typename VecIterator, typename XIterator>
void SkylineStorage::bzLowerSolver(const MatIterator& itdb, const MatIterator& itlb, const VecIterator& itvb,
                                   const XIterator& itxb, const XIterator& itxe, const std::vector<number_t>::const_iterator itrow) const
{
  /*
    lower triangular part solver (for sub-system)
    used in L.U factorization algorithm or for solving L x = b after L.Lt factorization
    *** Caution here as itxb might coincide with itvb

      - itrow is a (forward) iterator on (sub-)matrix addresses of first non-zero entries on rows
      - itdb is a (forward) iterator on (sub-)matrix diagonal entries (D)
      - itlb is a (forward) iterator on (sub-)matrix strict lower triangular part entries (L)
  */
  MatIterator it_d=itdb, it_l; VecIterator it_b=itvb; XIterator it_xx;
  std::vector<number_t>::const_iterator it_rowCurrent, it_rowNext=itrow;

  // row index (relative to sub-matrix)
  number_t row=0;

  // forward loop on rows
  for ( XIterator it_x = itxb; it_x != itxe; ++it_x, ++row )
  {
    // iterator on current row
    it_rowCurrent = it_rowNext; ++it_rowNext;
    // number of entries on row
    // min is taken to allow use of solver for a sub-matrix, see LDLt factorization ***
    number_t nbEntries = std::min(row, *it_rowNext - *it_rowCurrent);
    it_l = itlb + *it_rowNext;
    // x[i] = ( b[i] - \sum_{j<i} L[i,j] * x[j] ) / D[i,i]
    *it_x = *it_b; ++it_b;
    it_xx = itxb + row - nbEntries;
    for ( MatIterator it_ll = it_l - nbEntries; it_ll != it_l; ++it_ll, ++it_xx)
        *it_x -= *it_ll * *it_xx;
    *it_x /= *it_d; ++it_d;
  }
}

/*!
    Template solvers for lower triangular matrix with unit diagonal
    (used for factorization or as downwards solver for Symm & Dual Skyline Storages)
*/
template<typename MatIterator, typename VecIterator, typename XIterator>
void SkylineStorage::bzLowerD1Solver(const MatIterator& itmb, const VecIterator& itvb, const XIterator& itxb,
                                     const XIterator& itxe, const std::vector<number_t>::const_iterator itrow) const
{
  /*
    lower triangular part solver with unit diagonal (for subsystem)
    use to solve L x = b after L.D.Lt, L.D.U or L.U factorization
    *** Caution here as itxb might coincide with itvb

     itrow is a (forward) iterator on (sub-)matrix addresses of first non-zero entries on rows
     itmb is a (forward) iterator on (sub-)matrix strict lower triangular part entries (L)
  */
  MatIterator itm; VecIterator itv=itvb; XIterator itx;
  std::vector<number_t>::const_iterator itrcurrent, itrnext=itrow;
  number_t row=0;

  for ( XIterator it_x = itxb; it_x != itxe; ++it_x, ++row )
  {
    itrcurrent = itrnext; ++itrnext;
    // <<  Min is taken to allow use of solver for a sub-matrix, see LDLt factorization ***
    number_t nbEntries = std::min(row, *itrnext - *itrcurrent);
    itm = itmb + *itrnext;
    // x[i] = ( b[i] - \sum_{j<i} L[i,j] * x[j] )
    *it_x = *itv; ++itv;
    itx = itxb + row - nbEntries;
    /*for ( MatIterator it_ll = itm - nbEntries; it_ll != itm; ++it_ll, ++itx )
         *it_x -= *it_ll **itx;*/
  MatIterator it_ll = itm - nbEntries;
  for (; it_ll != itm; ++it_ll, ++itx)
    *it_x -= *it_ll * *itx;
  }
}

/*!
    Template solvers for lower triangular complex matrix
    (used for factorization or as downwards solver for Symm & Dual Skyline Storages)
*/
template<typename MatIterator, typename VecIterator, typename XIterator>
void SkylineStorage::bzLowerConjD1Solver(const MatIterator& itlb, const VecIterator& itbb, const XIterator& itxb, const XIterator& itxe, const std::vector<number_t>::const_iterator itrow) const
{
  /*
    lower triangular part solver with unit diagonal (for sub-system)
    used to solve conj(L) x = b during L.D.L* factorization
    This is adapted from bz_lowerD1Solver in the hermitian case, since
    L_ij entries are stored and conj(L_ij) are used in solver
  */
  MatIterator it_l; VecIterator it_b(itbb); XIterator it_xx;
  std::vector<number_t>::const_iterator it_rowCurrent, it_rowNext(itrow);
  number_t row(0);

  for ( XIterator it_x = itxb; it_x != itxe; ++it_x, ++row )
  {
    it_rowCurrent = it_rowNext; ++it_rowNext;
    number_t nbEntries = std::min(row, *it_rowNext - *it_rowCurrent);
    it_l = itlb + *it_rowNext;
    *it_x = *it_b; ++it_b;
    it_xx = itxb + row - nbEntries;
    for ( MatIterator it_ll = it_l - nbEntries; it_ll != it_l; ++it_ll, ++it_xx )
    { *it_x -= conj(*it_ll) **it_xx; }
  }
}

/*!
   Template diagonal solver (used after factorization for Symm & Dual Skyline Storages)
   Matrix is assumed INVERTIBLE here ***, no check on division by zero is done
*/
template<typename MatIterator, typename VecIterator, typename XIterator>
void SkylineStorage::bzDiagonalSolver(const MatIterator& itdb, const VecIterator& itbb, const XIterator& itxb, const XIterator& itxe) const
{
  /*
    diagonal solver
    *** Caution here as itxb might coincide with itbb

    - itdb is a (forward) iterator on matrix diagonal entries
  */
  MatIterator itd(itdb);
  VecIterator itb(itbb);
 for ( XIterator itx = itxb; itx != itxe; itx++ ) { *itx = *itb++ / *itd++; }
}

/*!
   Template upper triangular solver
   (used for factorization or as upwards solver for Symm & Dual Skyline Storages)
   Matrix is assumed INVERTIBLE here ***, no check on division by zero is done
*/
template<typename MatRevIterator, typename VecRevIterator, typename XRevIterator>
void SkylineStorage::bzUpperSolver(const MatRevIterator& it_drb, const MatRevIterator& it_u, const VecRevIterator& it_brb, const XRevIterator& it_xrb, const XRevIterator& it_xre, const std::vector<number_t>::const_reverse_iterator it_col) const
{
  /*
    upper triangular part solver (for sub-system)
    used in L.U factorization algorithm or for solving Lt x = b after L.Lt factorization
    or U x = b after L.U factorization
    *** Caution here as it_xrb might coincide with it_brb

    *** LO AND BEHOLD *** we are using reverse_iterator syntax here
    *** loops are thus reversed from end to begin though iterators are "++"ed

    - it_col is a (backward) iterator on (sub-)matrix addresses of first non-zero
      entry on columns starting on last column
    - it_drb is a (backward) iterator on (sub-)matrix diagonal entries (D)
      starting on last diagonal matrix entry
    - it_u is a (backward) iterator on (sub-)matrix strict upper triangular part entries (U)
      starting on last matrix entry
  */
  MatRevIterator it_d(it_drb);
  std::vector<number_t>::const_reverse_iterator it_colNext(it_col), it_colCurrent;
  int_t col(-1); // column index relative to sub-matrix starting at last entry

  // initialize solution vector with right hand side vector and count columns
  VecRevIterator it_b(it_brb);
  for ( XRevIterator it_x = it_xrb; it_x != it_xre; ++it_x, ++col ) { *it_x = *it_b; ++it_b; }

  // backward loop on columns
  MatRevIterator it_uu = it_u;
  for ( XRevIterator it_x = it_xrb; it_x != it_xre; ++it_x, --col )
  {
    // divide by diagonal entry
    // x[i] = x[i] / D[i,i]
    *it_x /= *it_d; ++it_d;

    // iterator on current column
    it_colCurrent = it_colNext; ++it_colNext;

    // number of entries on column
    int_t nbEntries = *it_colCurrent - *it_colNext;
    XRevIterator xx_i = it_x;

    // x[j] = x[j] - U[j,i] x[i] for j < i
    // *** min is taken to allow use of solver for a sub-matrix, see LU factorization ***
    for ( MatRevIterator uuu_i = it_uu; uuu_i != it_uu + std::min(col, nbEntries) ; ++uuu_i )
    { *(++xx_i) -= *uuu_i **it_x; }

    // skip to previous column
    it_uu += nbEntries;
  }
}

/*!
  Template upper triangular solvers
  (used for factorization or as upwards solver for Symm & Dual Skyline Storages)
*/
template<typename MatRevIterator, typename VecRevIterator, typename XRevIterator>
void SkylineStorage::bzUpperD1Solver(const MatRevIterator& itu, const VecRevIterator& itbrb, const XRevIterator& itxrb, const XRevIterator& itxre, const std::vector<number_t>::const_reverse_iterator itcol) const
{
  /*
    upper triangular part with unit diagonal solver use to solve Lt x = b
    after L.D.Lt factorization or U x = b after L.D.U factorization
    *** Caution here as itxrb might coincide with itbrb

    *** LO AND BEHOLD *** we are using reverse_iterator syntax here
  */
  std::vector<number_t>::const_reverse_iterator itcolNext(itcol), itcolCurrent;
  int_t col(-1); // column index relative to sub-matrix starting at last

  // initialize solution vector with right hand side vector and count columns
  VecRevIterator itb(itbrb);
  for ( XRevIterator itx = itxrb; itx != itxre; ++itx, ++col ) { *itx = *itb; ++itb; }

  // backward loop on columns
  MatRevIterator ituu = itu;
  for ( XRevIterator itx = itxrb; itx != itxre; ++itx, --col )
  {
    itcolCurrent = itcolNext; ++itcolNext;
    int_t nbEntries = *itcolCurrent - *itcolNext;
    XRevIterator itxx = itx;
    // x[j] = x[j] - U[j,i] x[i] for j < i
    // *** min is taken to allow use of solver for a sub-matrix, see LDU factorization ***
    for ( MatRevIterator ituuu = ituu; ituuu != ituu + std::min(col, nbEntries) ; ++ituuu )
    { *(++itxx) -= *ituuu **itx; }
    ituu += nbEntries;
  }
}

/*!
    Template upper triangular solvers
    (used for factorization or as upwards solver for Symm & Dual Skyline Storages)
*/
template<typename MatRevIterator, typename VecRevIterator, typename XRevIterator>
void SkylineStorage::bzUpperConjD1Solver(const MatRevIterator& itu, const VecRevIterator& itbrb, const XRevIterator& itxrb, const XRevIterator& itxre, const std::vector<number_t>::const_reverse_iterator itcol) const
{
  /*
    upper triangular part with unit diagonal solver use to solve L* x = b
    after L.D.L* factorization
    This is adapted from bz_upperD1Solver in the hermitian case, since
    L_ij coefficients are stored and U_ij=conj(L_ji) are used in solver
  */
  std::vector<number_t>::const_reverse_iterator itcolNext(itcol), itcolCurrent;
  int_t col(-1);

  VecRevIterator itb(itbrb);
  for ( XRevIterator itx = itxrb; itx != itxre; ++itx, ++col ) { *itx = *itb; ++itb; }

  MatRevIterator ituu = itu;
  for ( XRevIterator itx = itxrb; itx != itxre; ++itx, --col )
  {
    itcolCurrent = itcolNext; ++itcolNext;
    int_t nbEntries = *itcolCurrent - *itcolNext;
    XRevIterator itxx = itx;
    for ( MatRevIterator ituuu = ituu; ituuu != ituu + std::min(col, nbEntries); ++ituuu )
    { *(++itxx) -= conj(*ituuu) **itx; }
    ituu += nbEntries;
  }
}

template<typename Mat1Iterator, typename Mat2Iterator, typename ResIterator>
void SkylineStorage::sumMatrixMatrix(Mat1Iterator& itm1, Mat2Iterator& itm2, ResIterator& itrb, ResIterator& itre) const
{
  for (ResIterator itr = itrb; itr != itre; ++itr, ++itm1, ++itm2)
  {
    *itr = *itm1 + *itm2;
  }
}

#ifdef XLIFEPP_WITH_OMP
  /*!
    Parallelize triangular lower part of matrix-vector multiplication
        (as well as triangular upper part of vector-matrix multiplication)
        The switch-case statement inside the parallel region can decrease the performance
        a little bit; however, in this way, it helps avoiding redundant codes

    \param [in] pointer vector row index
    \param [in] itm iterator pointing to first non zero element in the part
    \param [in] itvb iterator pointing to first element of source vector
    \param [in,out] itrb iterator pointing to first element of vector result
    \param [in] numThread number of thread. It's not necessary to be the same number of OpenMp thread
                    it can be even more depending on the GRANULARITY factor
    \param [in] itThreadLower vector containing lower bound of index for each thread
    \param [in] itThreadUpper vector containing upper bound of index for each thread
    \param [in] sym symmetric type of matrix
  */
  template<typename MatIterator, typename VecIterator, typename ResIterator>
  void SkylineStorage::parallelLowerMatrixVector(const std::vector<number_t>& pointer,
        MatIterator& itm, VecIterator& itvb, ResIterator& itrb,
        number_t numThread,
        std::vector<std::vector<number_t>::const_iterator>& itThreadLower,
        std::vector<std::vector<number_t>::const_iterator>& itThreadUpper,
        SymType sym) const
  {
    std::vector<number_t>::const_iterator itpb = pointer.begin();
    std::vector<number_t>::const_iterator itpLower, itpUpper;
    ResIterator itr = itrb;
    MatIterator itim = itm;
    VecIterator itv = itvb;

    std::vector<std::vector<number_t>::const_iterator>::const_iterator itbLower, itbUpper;
    itbLower = itThreadLower.begin();
    itbUpper = itThreadUpper.begin();

    number_t i = 0;
    #pragma omp parallel \
    private(i, itr, itv, itim, itpLower, itpUpper) \
    shared(numThread, itbLower, itbUpper, itpb, itm, itvb, itrb, sym)
    {
      number_t nnz = 0;
      switch(sym)
      {
        case _skewSymmetric:
          #pragma omp for schedule(dynamic,1)
          for(i = 0; i < numThread; ++i)
          {
            itpLower = *(itbLower+i);
            itpUpper = *(itbUpper+i);
            for (; itpLower != itpUpper; ++itpLower)
            {
              itr = itrb + (itpLower - itpb);
              nnz = *(itpLower+1) - *itpLower;
              itv = itvb + (itpLower - itpb) - nnz;
              itim = itm + *(itpLower);

              while(nnz>0)
              {
                *itr -= (*itim) **itv;
                ++itim; ++itv;
                --nnz;
              }
            }
          }
          break;

        case _selfAdjoint:
          #pragma omp for schedule(dynamic,1)
          for(i = 0; i < numThread; ++i)
          {
            itpLower = *(itbLower+i);
            itpUpper = *(itbUpper+i);
            for (; itpLower != itpUpper; ++itpLower)
            {
              itr = itrb + (itpLower - itpb);
              nnz = *(itpLower+1) - *itpLower;
              itv = itvb + (itpLower - itpb) - nnz;
              itim = itm + *(itpLower);

              while(nnz>0)
              {
                *itr += conj(*itim) **itv;
                ++itim; ++itv;
                --nnz;
              }
            }
          }
          break;

        case _skewAdjoint:
          #pragma omp for schedule(dynamic,1)
          for(i = 0; i < numThread; ++i)
          {
            itpLower = *(itbLower+i);
            itpUpper = *(itbUpper+i);
            for (; itpLower != itpUpper; ++itpLower)
            {
              itr = itrb + (itpLower - itpb);
              nnz = *(itpLower+1) - *itpLower;
              itv = itvb + (itpLower - itpb) - nnz;
              itim = itm + *(itpLower);

              while(nnz>0)
              {
                *itr -= conj(*itim) **itv;
                ++itim; ++itv;
                --nnz;
              }
            }
          }
          break;

        default: // symmetric matrix and non-symmetric
          #pragma omp for schedule(dynamic,1)
          for(i = 0; i < numThread; i++)
          {
            itpLower = *(itbLower+i);
            itpUpper = *(itbUpper+i);
            for (; itpLower != itpUpper; itpLower++)
            {
              itr = itrb + (itpLower - itpb);
              nnz = *(itpLower+1) - *itpLower;
              itv = itvb + (itpLower - itpb) - nnz;
              itim = itm + *(itpLower);

              while(nnz>0)
              {
                *itr += (*itim) **itv;
                ++itim; ++itv;
                --nnz;
              }
            }
          }
          break;
      }
    }
  }

  /*!
    Parallelize matrix-vector multiplication of triangular upper part
        (as well as vector-matrix multiplication of triangular lower part)
        The switch-case statement inside the parallel region can decrease the performance
        a little bit; however, in this way, it helps avoiding redundant codes

    \param [in] pointer vector column index
    \param [in] itm iterator pointing to first non zero element in the part
    \param [in] itvb iterator pointing to first element of source vector
    \param [in,out] itrb iterator pointing to first element of vector result
    \param [in] numThread number of thread. It's not necessary to be the same number of OpenMp thread
                    it can be even more depending on the GRANULARITY factor
    \param [in] itThreadLower vector containing lower bound of index for each thread
    \param [in] itThreadUpper vector containing upper bound of index for each thread
    \param [in] numRes size of vector result
    \param [in] sym symmetric type of matrix
  */
  template<typename MatIterator, typename VecIterator, typename ResIterator>
  void SkylineStorage::parallelUpperMatrixVector(const std::vector<number_t>& pointer,
        MatIterator& itm, VecIterator& itvb, ResIterator& itrb,
        number_t numThread,
        std::vector<std::vector<number_t>::const_iterator>& itThreadLower,
        std::vector<std::vector<number_t>::const_iterator>& itThreadUpper,
        number_t numRes,
        SymType sym) const
  {
    typedef typename IterationVectorTrait<ResIterator>::Type ResType;

    std::vector<number_t>::const_iterator itpb = pointer.begin();
    std::vector<number_t>::const_iterator itpLower, itpUpper;
    VecIterator itv = itvb;
    ResIterator itr = itrb;
    MatIterator itim = itm;

    std::vector<std::vector<number_t>::const_iterator>::const_iterator itbLower, itbUpper;
    itbLower = itThreadLower.begin();
    itbUpper = itThreadUpper.begin();

    number_t i = 0;
    #pragma omp parallel  default(none) \
    private(i, itr, itim, itpLower, itpUpper, itv) \
    shared(numThread, itbLower, itbUpper, itpb, itm, itvb, itrb, numRes, sym)
    {
      std::vector<ResType> resTemp(numRes, *itrb * 0.);
      typename std::vector<ResType>::iterator itbResTemp = resTemp.begin(), iteResTemp = resTemp.end(), itResTemp = itbResTemp;

      number_t nnz = 0;
      switch(sym)
      {
        case _skewSymmetric:
          #pragma omp for nowait schedule(dynamic,1)
          for(i = 0; i < numThread; ++i)
          {
            itpLower = *(itbLower+i);
            itpUpper = *(itbUpper+i);
            for (; itpLower != itpUpper; ++itpLower)
            {
              itv = itvb + (itpLower - itpb);
              nnz = *(itpLower+1) - *itpLower;
              itim = itm + *(itpLower);
              itResTemp = itbResTemp + (itpLower - itpb) - nnz;

              while(nnz>0)
              {
                *itResTemp -= (*itim) **itv;
                ++itim; ++itResTemp;
                --nnz;
              }
            }
          }
          break;
        case _selfAdjoint:
          #pragma omp for nowait schedule(dynamic,1)
          for(i = 0; i < numThread; i++)
          {
            itpLower = *(itbLower+i);
            itpUpper = *(itbUpper+i);
            for (; itpLower != itpUpper; itpLower++)
            {
              itv = itvb + (itpLower - itpb);
              nnz = *(itpLower+1) - *itpLower;
              itim = itm + *(itpLower);
              itResTemp = itbResTemp + (itpLower - itpb) - nnz;

              while(nnz>0)
              {
                *itResTemp += conj(*itim) **itv;
                ++itim; ++itResTemp;
                --nnz;
              }
            }
          }
          break;
        case _skewAdjoint:
          #pragma omp for nowait schedule(dynamic,1)
          for(i = 0; i < numThread; i++)
          {
            itpLower = *(itbLower+i);
            itpUpper = *(itbUpper+i);
            for (; itpLower != itpUpper; ++itpLower)
            {
              itv = itvb + (itpLower - itpb);
              nnz = *(itpLower+1) - *itpLower;
              itim = itm + *(itpLower);
              itResTemp = itbResTemp + (itpLower - itpb) - nnz;

              while(nnz>0)
              {
                *itResTemp -= conj(*itim) **itv;
                ++itim; ++itResTemp;
                --nnz;
              }
            }
          }
          break;
        case _symmetric:
          #pragma omp for nowait schedule(dynamic,1)
          for(i = 0; i < numThread; i++)
          {
            itpLower = *(itbLower+i);
            itpUpper = *(itbUpper+i);
            for (; itpLower != itpUpper; itpLower++)
            {
              itv = itvb + (itpLower - itpb);
              nnz = *(itpLower+1) - *itpLower;
              itim = itm + *(itpLower);
              itResTemp = itbResTemp + (itpLower - itpb) - nnz;

              while(nnz>0)
              {
                *itResTemp += (*itim) **itv;
                ++itim; ++itResTemp;
                --nnz;
              }
            }
          }
          break;
        default: // no symmetry
          #pragma omp for nowait schedule(dynamic,1)
          for(i = 0; i < numThread; i++)
          {
            itpLower = *(itbLower+i);
            itpUpper = *(itbUpper+i);
            for (; itpLower != itpUpper; itpLower++)
            {
              itv = itvb + (itpLower - itpb);
              nnz = *(itpLower+1) - *itpLower;
              itim = itm + *(itpLower);
              itResTemp = itbResTemp + (itpLower - itpb) - nnz;

              while(nnz>0)
              {
                *itResTemp += (*itim) **itv;
                ++itim; ++itResTemp;
                --nnz;
              }
            }
          }
          break;

      }
      #pragma omp critical (updateResult)
      {
        for (itResTemp = itbResTemp; itResTemp != iteResTemp; itResTemp++)
        {
          itr = (itrb + (itResTemp - itbResTemp));
          *itr += *itResTemp;
        }
      }
    }
  }


  /*!
    Solver diagonal block of LU factorization (version parallel)
    \param[in] rowIdx beginning row index of the current block
    \param[in] blockRowSize number of rows of this block
    \param[in] rowPointer rowPointer pointing to the first element of the lower part
    \param[in] colIdx beginning column index of the current block
    \param[in] blockColSize number of columns of this block
    \param[in] colPointer colPointer pointing to the first element of the lower part
    \param[in,out] it_fd iterator (destination) pointing to the first diagonal value of the block
    \param[in,out] it_fl iterator (destination) pointing to the first lower value of the block
    \param[in,out] it_fu iterator (destination) pointing to the first upper value of the block
    \param[in] it_md iterator (source) pointing to the first diagonal value of the block
    \param[in] it_ml iterator (source) pointing to the first lower value of the block
    \param[in] it_mu iterator (source) pointing to the first upper value of the block
  */
  template<typename MatIterator, typename NumberIterator>
  void SkylineStorage::diagBlockSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
        number_t colIdx, number_t blockColSize, NumberIterator colPointer,
        MatIterator it_fd, MatIterator it_fl, MatIterator it_fu,
        MatIterator it_md, MatIterator it_ml, MatIterator it_mu) const
  {
    number_t j = 0;
    NumberIterator itrowb = rowPointer + rowIdx;
    NumberIterator itcolb = colPointer + colIdx;
    NumberIterator itrow, itcol;

    for (number_t i = 0; i < blockRowSize; ++i)
    {
      itrow = itrowb + i;
      itcol = itcolb + i;
      number_t rowEntries = (*itrow - *(itrow-1));
      number_t firstZeroIdxRow = (rowIdx+i) - rowEntries;
      number_t colEntries = (*itcol - *(itcol-1));
      number_t firstZeroIdxCol = (colIdx+i) - colEntries;

      // Compute the diagonal value
      if ( std::abs(*it_md+rowIdx+i) < theZeroThreshold ) { isSingular("L.U", 0); }
      *(it_fd+rowIdx+i) = *(it_md+rowIdx+i);
      if ((rowEntries > 0) && (colEntries > 0))
      {
        number_t firstZeroIdx = std::max(firstZeroIdxRow, firstZeroIdxCol);
        MatIterator it_flx = it_fl + *(itrow-1) + (rowEntries-1) ;
        MatIterator it_fux = it_fu + *(itcol-1) + (colEntries-1);
        for (j = 0; j < ((rowIdx+i) - firstZeroIdx); ++j)
        {
          *(it_fd+rowIdx+i) -= *(it_flx-j) **(it_fux-j);
        }
      }
      //Compute the upper part
      for (j = colIdx + i + 1; j < (colIdx+blockColSize); ++j)
      {
        upperSolverParallel(i+rowIdx, rowEntries, it_fl + *(itrow-1), j, it_fu, colPointer, it_mu);
      }

      // Compute the lower part
      for (j = rowIdx + i + 1; j < (rowIdx+blockRowSize); ++j)
      {
        lowerSolverParallel(i+colIdx, colEntries, it_fu + *(itcol-1), it_fd+i+rowIdx, j, it_fl, rowPointer, it_ml);
      }
    }
  }

  /*!
    Solver diagonal block of LDLt factorization (version parallel)
    \param[in] rowIdx beginning row index of the current block
    \param[in] blockRowSize number of rows of this block
    \param[in] rowPointer rowPointer pointing to the first element of the lower part
    \param[in] colIdx beginning column index of the current block
    \param[in] blockColSize number of columns of this block
    \param[in] colPointer colPointer pointing to the first element of the lower part
    \param[in,out] it_fd iterator (destination) pointing to the first diagonal value of the block
    \param[in,out] it_fl iterator (destination) pointing to the first lower value of the block
    \param[in,out] it_fu iterator (destination) pointing to the first upper value of the block
    \param[in] it_md iterator (source) pointing to the first diagonal value of the block
    \param[in] it_ml iterator (source) pointing to the first lower value of the block
    \param[in] it_mu iterator (source) pointing to the first upper value of the block
  */
  template<typename MatIterator, typename NumberIterator>
  void SkylineStorage::diagBlockSymSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
        number_t colIdx, number_t blockColSize, NumberIterator colPointer,
        MatIterator it_fd, MatIterator it_fl, MatIterator it_fu,
        MatIterator it_md, MatIterator it_ml, MatIterator it_mu) const
  {
    number_t j = 0;
    NumberIterator itrowb = rowPointer + rowIdx;
    NumberIterator itcolb = colPointer + colIdx;
    NumberIterator itrow, itcol;

    for (number_t i = 0; i < blockRowSize; ++i)
    {
      itrow = itrowb + i;
      itcol = itcolb + i;
      number_t rowEntries = (*itrow - *(itrow-1));
      number_t firstZeroIdxRow = (rowIdx+i) - rowEntries;
      number_t colEntries = (*itcol - *(itcol-1));
      number_t firstZeroIdxCol = (colIdx+i) - colEntries;

      // Compute the diagonal value
      if ( std::abs(*it_md+rowIdx+i) < theZeroThreshold ) { isSingular("L.D.L.T", 0); }
      *(it_fd+rowIdx+i) = *(it_md+rowIdx+i);
      if ((rowEntries > 0) && (colEntries > 0))
      {
        number_t firstZeroIdx = std::max(firstZeroIdxRow, firstZeroIdxCol);
        MatIterator it_flx = it_fl + *(itrow-1) + (rowEntries-1) ;
        MatIterator it_fux = it_fu + *(itcol-1) + (colEntries-1);
        MatIterator it_fdx = it_fd+rowIdx+i-1;
        for (j = 0; j < ((rowIdx+i) - firstZeroIdx); ++j)
        {
          *(it_fd+rowIdx+i) -= *(it_flx-j) **(it_fux-j) **(it_fdx-j);
        }
      }

      // Compute the lower part
      for (j = rowIdx + i + 1; j < (rowIdx+blockRowSize); ++j)
      {
        lowerSymSolverParallel(i+colIdx, colEntries, it_fu + *(itcol-1), it_fd+i+rowIdx, j, it_fl, rowPointer, it_ml);
      }
    }
  }


  /*!
    Solver diagonal block of LDL* factorization (version parallel)
    \param[in] rowIdx beginning row index of the current block
    \param[in] blockRowSize number of rows of this block
    \param[in] rowPointer rowPointer pointing to the first element of the lower part
    \param[in] colIdx beginning column index of the current block
    \param[in] blockColSize number of columns of this block
    \param[in] colPointer colPointer pointing to the first element of the lower part
    \param[in,out] it_fd iterator (destination) pointing to the first diagonal value of the block
    \param[in,out] it_fl iterator (destination) pointing to the first lower value of the block
    \param[in,out] it_fu iterator (destination) pointing to the first upper value of the block
    \param[in] it_md iterator (source) pointing to the first diagonal value of the block
    \param[in] it_ml iterator (source) pointing to the first lower value of the block
    \param[in] it_mu iterator (source) pointing to the first upper value of the block
  */
  template<typename MatIterator, typename NumberIterator>
  void SkylineStorage::diagBlockSymConjSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
        number_t colIdx, number_t blockColSize, NumberIterator colPointer,
        MatIterator it_fd, MatIterator it_fl, MatIterator it_fu,
        MatIterator it_md, MatIterator it_ml, MatIterator it_mu) const
  {
    number_t j = 0;
    NumberIterator itrowb = rowPointer + rowIdx;
    NumberIterator itcolb = colPointer + colIdx;
    NumberIterator itrow, itcol;

    for (number_t i = 0; i < blockRowSize; ++i)
    {
      itrow = itrowb + i;
      itcol = itcolb + i;
      number_t rowEntries = (*itrow - *(itrow-1));
      number_t firstZeroIdxRow = (rowIdx+i) - rowEntries;
      number_t colEntries = (*itcol - *(itcol-1));
      number_t firstZeroIdxCol = (colIdx+i) - colEntries;

      // Compute the diagonal value
      if ( std::abs(*it_md+rowIdx+i) < theZeroThreshold ) { isSingular("L.D.L.T", 0); }
      *(it_fd+rowIdx+i) = *(it_md+rowIdx+i);
      if ((rowEntries > 0) && (colEntries > 0))
      {
        number_t firstZeroIdx = std::max(firstZeroIdxRow, firstZeroIdxCol);
        MatIterator it_flx = it_fl + *(itrow-1) + (rowEntries-1) ;
        MatIterator it_fux = it_fu + *(itcol-1) + (colEntries-1);
        MatIterator it_fdx = it_fd+rowIdx+i-1;
        for (j = 0; j < ((rowIdx+i) - firstZeroIdx); ++j)
        {
          *(it_fd+rowIdx+i) -= *(it_flx-j) *  conj(*(it_fux-j)) **(it_fdx-j);
        }
      }

      // Compute the lower part
      for (j = rowIdx + i + 1; j < (rowIdx+blockRowSize); ++j)
      {
        lowerSymConjSolverParallel(i+colIdx, colEntries, it_fu + *(itcol-1), it_fd+i+rowIdx, j, it_fl, rowPointer, it_ml);
      }
    }
  }

  /*!
    Solver upper block of LU factorization (version parallel)
    \param[in] rowIdx beginning row index of the current block
    \param[in] blockRowSize number of rows of this block
    \param[in] rowPointer rowPointer pointing to the first element of the lower part
    \param[in] colIdx beginning column index of the current block
    \param[in] blockColSize number of columns of this block
    \param[in] colPointer colPointer pointing to the first element of the lower part
    \param[in,out] it_fl iterator (destination) pointing to the first lower value of the block
    \param[in,out] it_fu iterator (destination) pointing to the first upper value of the block
    \param[in] it_mu iterator (source) pointing to the first upper value of the block
  */
  template<typename MatIterator, typename NumberIterator>
  void SkylineStorage::upperBlockSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
        number_t colIdx, number_t blockColSize, NumberIterator colPointer,
        MatIterator it_fl, MatIterator it_fu, MatIterator it_mu) const
  {
    number_t j = 0;
    NumberIterator itrowb = rowPointer + rowIdx;
    NumberIterator itcolb = colPointer + colIdx;
    NumberIterator itrow, itcol;

    for (number_t i = 0; i < blockColSize; ++i)
    {
      itcol = itcolb + i;
      number_t colEntries = (*itcol - *(itcol-1));
      number_t firstZeroIdxCol = (colIdx+i) - colEntries;
      if (firstZeroIdxCol < (rowIdx+blockRowSize))
      {
        for (j = 0; j < blockRowSize; ++j)
        {
          if (firstZeroIdxCol <= (rowIdx+j))
          {
            itrow = itrowb + j;
            number_t rowEntries = (*(itrow) - *((itrow)-1));
            number_t firstZeroIdxRow = (rowIdx+j) - rowEntries;
            number_t firstZeroIdx = std::max(firstZeroIdxRow, firstZeroIdxCol);

            number_t pos = *(itcol-1) + (rowIdx+j)-firstZeroIdxCol;
            MatIterator it_fuu = it_fu+pos;
            *it_fuu = *(it_mu+pos);
            MatIterator it_flx = it_fl + *(itrow-1) + (firstZeroIdx - firstZeroIdxRow) ;
            MatIterator it_fux = it_fu + *(itcol-1) + (firstZeroIdx - firstZeroIdxCol);

            for (number_t k = 0; k < ((rowIdx+j) - firstZeroIdx); ++k)
            {
              *(it_fuu) -= *(it_fux) **(it_flx);
              ++it_flx; ++it_fux;
            }
          }
        }
      }
    }
  }

  /*!
    Solver lower block of LU factorization (version parallel)
    \param[in] rowIdx beginning row index of the current block
    \param[in] blockRowSize number of rows of this block
    \param[in] rowPointer rowPointer pointing to the first element of the lower part
    \param[in] colIdx beginning column index of the current block
    \param[in] blockColSize number of columns of this block
    \param[in] colPointer colPointer pointing to the first element of the lower part
    \param[in,out] it_fd iterator (destination) pointing to the first diagonal value of the block
    \param[in,out] it_fl iterator (destination) pointing to the first lower value of the block
    \param[in,out] it_fu iterator (destination) pointing to the first upper value of the block
    \param[in] it_ml iterator (source) pointing to the first lower value of the block
  */
  template<typename MatIterator, typename NumberIterator>
  void SkylineStorage::lowerBlockSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
        number_t colIdx, number_t blockColSize, NumberIterator colPointer,
        MatIterator it_fd, MatIterator it_fl, MatIterator it_fu, MatIterator it_ml) const
  {
    number_t j = 0;
    NumberIterator itrowb = rowPointer + rowIdx;
    NumberIterator itcolb = colPointer + colIdx;
    NumberIterator itrow, itcol;

    for (number_t i = 0; i < blockRowSize; ++i)
    {
      itrow = itrowb + i;
      number_t rowEntries = (*itrow - *(itrow-1));
      number_t firstZeroIdxRow = (rowIdx+i) - rowEntries;
      if (firstZeroIdxRow < (colIdx+blockColSize))
      {
        for (j = 0; j < blockColSize; ++j)
        {
          itcol = itcolb + j;
          if (firstZeroIdxRow <= (colIdx+j))
          {
            number_t colEntries = (*(itcol) - *((itcol)-1));
            number_t firstZeroIdxCol = (colIdx+j) - colEntries;
            number_t firstZeroIdx = std::max(firstZeroIdxRow, firstZeroIdxCol);

            number_t offset = (colIdx+j)-firstZeroIdxRow;
            number_t pos = *(itrow-1) + offset;
            MatIterator it_fll = it_fl+pos;
            *it_fll = *(it_ml+pos);

            if ((colEntries > 0) && (offset>0))
            {
              MatIterator it_flx = it_fl + pos -1 ;
              MatIterator it_fux = it_fu + *(itcol-1) + (colEntries-1);

              for (number_t k = 0; k < ((colIdx+j) - firstZeroIdx); ++k)
              {
                *(it_fll) -= *(it_fux) **(it_flx);
                --it_flx; --it_fux;
              }
            }
            *it_fll /= *(it_fd+colIdx+j);
          }
        }
      }
    }
  }


  /*!
    Solver lower block of LDL.t factorization (version parallel)
    \param[in] rowIdx beginning row index of the current block
    \param[in] blockRowSize number of rows of this block
    \param[in] rowPointer rowPointer pointing to the first element of the lower part
    \param[in] colIdx beginning column index of the current block
    \param[in] blockColSize number of columns of this block
    \param[in] colPointer colPointer pointing to the first element of the lower part
    \param[in,out] it_fd iterator (destination) pointing to the first diagonal value of the block
    \param[in,out] it_fl iterator (destination) pointing to the first lower value of the block
    \param[in,out] it_fu iterator (destination) pointing to the first upper value of the block
    \param[in] it_ml iterator (source) pointing to the first lower value of the block
  */
  template<typename MatIterator, typename NumberIterator>
  void SkylineStorage::lowerBlockSymSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
        number_t colIdx, number_t blockColSize, NumberIterator colPointer,
        MatIterator it_fd, MatIterator it_fl, MatIterator it_fu, MatIterator it_ml) const
  {
    number_t j = 0;
    NumberIterator itrowb = rowPointer + rowIdx;
    NumberIterator itcolb = colPointer + colIdx;
    NumberIterator itrow, itcol;

    for (number_t i = 0; i < blockRowSize; ++i)
    {
      itrow = itrowb + i;
      number_t rowEntries = (*itrow - *(itrow-1));
      number_t firstZeroIdxRow = (rowIdx+i) - rowEntries;
      if (firstZeroIdxRow < (colIdx+blockColSize))
      {
        for (j = 0; j < blockColSize; ++j)
        {
          itcol = itcolb + j;
          if (firstZeroIdxRow <= (colIdx+j))
          {
            number_t colEntries = (*(itcol) - *((itcol)-1));
            number_t firstZeroIdxCol = (colIdx+j) - colEntries;
            number_t firstZeroIdx = std::max(firstZeroIdxRow, firstZeroIdxCol);

            number_t offset = (colIdx+j)-firstZeroIdxRow;
            number_t pos = *(itrow-1) + offset;
            MatIterator it_fll = it_fl+pos;
            *it_fll = *(it_ml+pos);

            if ((colEntries > 0) && (offset>0))
            {
              MatIterator it_flx = it_fl + pos -1 ;
              MatIterator it_fux = it_fu + *(itcol-1) + (colEntries-1);
              MatIterator it_fdx = it_fd + colIdx + j -1;

              for (number_t k = 0; k < ((colIdx+j) - firstZeroIdx); ++k)
              {
                *(it_fll) -= *(it_fux) **(it_flx) * (*it_fdx);
                --it_flx; --it_fux; --it_fdx;
              }
            }
            *it_fll /= *(it_fd+colIdx+j);
          }
        }
      }
    }
  }


  /*!
    Solver lower block of LDL* factorization (version parallel)
    \param[in] rowIdx beginning row index of the current block
    \param[in] blockRowSize number of rows of this block
    \param[in] rowPointer rowPointer pointing to the first element of the lower part
    \param[in] colIdx beginning column index of the current block
    \param[in] blockColSize number of columns of this block
    \param[in] colPointer colPointer pointing to the first element of the lower part
    \param[in,out] it_fd iterator (destination) pointing to the first diagonal value of the block
    \param[in,out] it_fl iterator (destination) pointing to the first lower value of the block
    \param[in,out] it_fu iterator (destination) pointing to the first upper value of the block
    \param[in] it_ml iterator (source) pointing to the first lower value of the block
  */
  template<typename MatIterator, typename NumberIterator>
  void SkylineStorage::lowerBlockSymConjSolverParallel(number_t rowIdx, number_t blockRowSize, NumberIterator rowPointer,
        number_t colIdx, number_t blockColSize, NumberIterator colPointer,
        MatIterator it_fd, MatIterator it_fl, MatIterator it_fu, MatIterator it_ml) const
  {
    number_t j = 0;
    NumberIterator itrowb = rowPointer + rowIdx;
    NumberIterator itcolb = colPointer + colIdx;
    NumberIterator itrow, itcol;

    for (number_t i = 0; i < blockRowSize; ++i)
    {
      itrow = itrowb + i;
      number_t rowEntries = (*itrow - *(itrow-1));
      number_t firstZeroIdxRow = (rowIdx+i) - rowEntries;
      if (firstZeroIdxRow < (colIdx+blockColSize))
      {
        for (j = 0; j < blockColSize; ++j)
        {
          itcol = itcolb + j;
          if (firstZeroIdxRow <= (colIdx+j))
          {
            number_t colEntries = (*(itcol) - *((itcol)-1));
            number_t firstZeroIdxCol = (colIdx+j) - colEntries;
            number_t firstZeroIdx = std::max(firstZeroIdxRow, firstZeroIdxCol);

            number_t offset = (colIdx+j)-firstZeroIdxRow;
            number_t pos = *(itrow-1) + offset;
            MatIterator it_fll = it_fl+pos;
            *it_fll = *(it_ml+pos);

            if ((colEntries > 0) && (offset>0))
            {
              MatIterator it_flx = it_fl + pos -1 ;
              MatIterator it_fux = it_fu + *(itcol-1) + (colEntries-1);
              MatIterator it_fdx = it_fd + colIdx + j -1;

              for (number_t k = 0; k < ((colIdx+j) - firstZeroIdx); ++k)
              {
                *(it_fll) -= conj(*(it_fux)) **(it_flx) * (*it_fdx);
                --it_flx; --it_fux; --it_fdx;
              }
            }
            *it_fll /= *(it_fd+colIdx+j);
          }
        }
      }
    }
  }


  /*!
    Solver upper part of LU factorization (version parallel)
    \param row row of upper part on which we need to find values
    \param rowEntries number of entries on row of lower part
    \param it_fl iterator points to first non-zero entry of row of lower part
    \param col column position of entry on upper part need finding value
    \param it_fu iterator points to first non-zero entry of column of upper part
    \param itcol_f column pointer (row pointer in case of symmetric)
    \param it_mu iterator points to first entry of upper part
  */
  template<typename MatIterator, typename NumberIterator>
  void SkylineStorage::upperSolverParallel(number_t row, number_t rowEntries, MatIterator it_fl,
        number_t col, MatIterator it_fu, NumberIterator itcol_f,
        MatIterator it_mu) const
  {
    // Column pointer (upper part) to col(th)
    NumberIterator itcol = itcol_f+col;

    // Position of the first non-zero entries on column col
    number_t rowIdx = col - (*itcol - *(itcol-1));
    if (row >= rowIdx)
    {
      // Position (row,col) needs finding the value
      number_t pos = *(itcol-1) + (row - rowIdx);
      MatIterator it_fuu = it_fu + pos;
      *it_fuu = *(it_mu+pos);
      if (0 != rowEntries)
      {
        // Position of the first non-zero entries on row row
        number_t colIdx = row - rowEntries;
        number_t nonZeroIdx = std::max(colIdx, rowIdx);
        MatIterator it_fll = it_fl + (rowEntries - 1);
        for (number_t i = 0; i < (row-nonZeroIdx); ++i)
        {
          *(it_fuu) -= *(it_fuu-i-1) **(it_fll-i);
        }
      }
    }
  }

  /*!
    Solver lower part of LU factorization (version parallel)
    \param col column position of entry on upper part need finding value
    \param colEntries number of entries on column of upper part
    \param it_fl iterator points to first non-zero entry of row of lower part
    \param row row of lower part on which we need to find values
    \param it_fu iterator points to first non-zero entry of column of upper part
    \param it_fd iterator points to the first diagonal entry
    \param itrow_f row pointer pointing to first element of the lower part
    \param it_ml iterator points to first entry of lower part
  */
  template<typename MatIterator, typename NumberIterator>
  void SkylineStorage::lowerSolverParallel(number_t col, number_t colEntries, MatIterator it_fu, MatIterator it_fd,
        number_t row, MatIterator it_fl, NumberIterator itrow_f,
        MatIterator it_ml) const
  {
    // Row pointer (lower part) to rows(th)
    NumberIterator itrow = itrow_f+row;
    number_t rowEntries = (*itrow) - *(itrow - 1);

    // Position of the first non-zero entries on row (th)
    number_t colIdx = row - rowEntries;
    if (col >= colIdx)
    {
      // Position (row,col) needs finding the value
      number_t pos = *(itrow-1) + (col - colIdx);
      MatIterator it_fll = it_fl + pos;
      *it_fll = *(it_ml+pos);

      if (0 != colEntries)
      {
        // Position of the first non-zero entries on column col
        number_t rowIdx = col - colEntries;
        number_t nonZeroIdx = std::max(colIdx, rowIdx);
        MatIterator it_fuu = it_fu + (colEntries - 1);
        for (number_t i = 0; i < (col-nonZeroIdx); ++i)
        {
          *(it_fll) -= *(it_fuu-i) **(it_fll-i-1);
        }
      }
      *it_fll /= *it_fd;
    }
  }

  /*!
    Solver lower part of LDL.t factorization (version parallel)
    \param col column position of entry on upper part need finding value
    \param colEntries number of entries on column of upper part
    \param it_fl iterator points to first non-zero entry of row of lower part
    \param row row of lower part on which we need to find values
    \param it_fu iterator points to first non-zero entry of column of upper part
    \param it_fd iterator points to the first diagonal entry
    \param itrow_f row pointer pointing to first element of the lower part
    \param it_ml iterator points to first entry of lower part
  */
  template<typename MatIterator, typename NumberIterator>
  void SkylineStorage::lowerSymSolverParallel(number_t col, number_t colEntries, MatIterator it_fu, MatIterator it_fd,
        number_t row, MatIterator it_fl, NumberIterator itrow_f,
        MatIterator it_ml) const
  {
    // Row pointer (lower part) to rows(th)
    NumberIterator itrow = itrow_f+row;
    number_t rowEntries = (*itrow) - *(itrow - 1);

    // Position of the first non-zero entries on row (th)
    number_t colIdx = row - rowEntries;
    if (col >= colIdx)
    {
      // Position (row,col) needs finding the value
      number_t pos = *(itrow-1) + (col - colIdx);
      MatIterator it_fll = it_fl + pos;
      *it_fll = *(it_ml+pos);

      if (0 != colEntries)
      {
        // Position of the first non-zero entries on column col
        number_t rowIdx = col - colEntries;
        number_t nonZeroIdx = std::max(colIdx, rowIdx);
        MatIterator it_fuu = it_fu + (colEntries - 1);
        for (number_t i = 0; i < (col-nonZeroIdx); ++i)
        {
          *(it_fll) -= *(it_fuu-i) **(it_fll-i-1) **(it_fd-i-1);
        }
      }
      *it_fll /= *it_fd;
    }
  }

  /*!
    Solver lower part of LDL* factorization (version parallel)
    \param col column position of entry on upper part need finding value
    \param colEntries number of entries on column of upper part
    \param it_fl iterator points to first non-zero entry of row of lower part
    \param row row of lower part on which we need to find values
    \param it_fu iterator points to first non-zero entry of column of upper part
    \param it_fd iterator points to the first diagonal entry
    \param itrow_f row pointer pointing to first element of the lower part
    \param it_ml iterator points to first entry of lower part
  */
  template<typename MatIterator, typename NumberIterator>
  void SkylineStorage::lowerSymConjSolverParallel(number_t col, number_t colEntries, MatIterator it_fu, MatIterator it_fd,
        number_t row, MatIterator it_fl, NumberIterator itrow_f,
        MatIterator it_ml) const
  {
    // Row pointer (lower part) to rows(th)
    NumberIterator itrow = itrow_f+row;
    number_t rowEntries = (*itrow) - *(itrow - 1);

    // Position of the first non-zero entries on row (th)
    number_t colIdx = row - rowEntries;
    if (col >= colIdx)
    {
      // Position (row,col) needs finding the value
      number_t pos = *(itrow-1) + (col - colIdx);
      MatIterator it_fll = it_fl + pos;
      *it_fll = *(it_ml+pos);

      if (0 != colEntries)
      {
        // Position of the first non-zero entries on column col
        number_t rowIdx = col - colEntries;
        number_t nonZeroIdx = std::max(colIdx, rowIdx);
        MatIterator it_fuu = it_fu + (colEntries - 1);
        for (number_t i = 0; i < (col-nonZeroIdx); ++i)
        {
          *(it_fll) -= conj(*(it_fuu-i)) **(it_fll-i-1) **(it_fd-i-1);
        }
      }
      *it_fll /= *it_fd;
    }
  }

#endif // end of XLIFEPP_WITH_OMP

} // end of namespace xlifepp

#endif // SKYLINE_STORAGE_HPP
