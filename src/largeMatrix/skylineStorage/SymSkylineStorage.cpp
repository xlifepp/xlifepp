/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SymSkylineStorage.cpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 6 Juin 2014

  \brief Implementation of xlifepp::SymSkylineStorage class and functionnalities
*/

#include "SymSkylineStorage.hpp"

namespace xlifepp
{
using std::abs;

/*--------------------------------------------------------------------------------
  Constructors, Destructor
--------------------------------------------------------------------------------*/
// default constructor
SymSkylineStorage::SymSkylineStorage(number_t n, string_t id)
  : SkylineStorage(n, n, _sym, id) {}

SymSkylineStorage::SymSkylineStorage(const std::vector<number_t>& rowp, string_t id)
  : SkylineStorage(rowp.size()-1,rowp.size()-1,_sym, id) {rowPointer_=rowp;}

// constructor by a pair of numbering vectors rowNumbers and colNumbers (same size)
//   rowNumbers[k] is the row numbers of submatrix k (>0)
//   colNumbers[k] is the col numbers of submatrix k (>0)
// this constructor is well adapted to FE matrix where submatrix is linked to an element
SymSkylineStorage::SymSkylineStorage(number_t n, const std::vector<std::vector<number_t> >& rowNumbers, const std::vector<std::vector<number_t> >& colNumbers, string_t id)
  : SkylineStorage(n, n, _sym, id)
{
  trace_p->push("SymSkylineStorage constructor");
  // find the skyline length of rows in lower part of matrix
  std::vector<number_t> rowsize(nbRows_, 0);             // temporary vector of row lengths
  std::vector<std::vector<number_t> >::const_iterator itrs, itcs = colNumbers.begin();
  std::vector<number_t>::const_iterator itr, itc;
  for(itrs = rowNumbers.begin(); itrs != rowNumbers.end(); itrs++, itcs++)
    {
      for(itr = itrs->begin(); itr != itrs->end(); itr++)
        for(itc = itcs->begin(); itc != itcs->end(); itc++)
          {
            int_t l = *itr - *itc;
            if(l > int_t(rowsize[*itr - 1])) { rowsize[*itr - 1] = number_t(l); }
          }
    }
  // build rowPointer_
  rowPointer_.resize(nbRows_ + 1);
  std::vector<number_t>::iterator its, it = rowPointer_.begin();
  *it = 0; it++;
  for(its = rowsize.begin(); its != rowsize.end(); its++, it++) { *it = *(it - 1) + *its; }

  trace_p->pop();
}
/*--------------------------------------------------------------------------------
  create a new scalar SymSkyline storage from current SymSkyline storage and submatrix sizes
  (non optimal)
--------------------------------------------------------------------------------*/
SymSkylineStorage* SymSkylineStorage::toScalar(dimen_t nbr, dimen_t nbc)
{
  if(nbr!=nbc) warning("free_warning"," in SymSkylineStorage::toScalar(nbr, nbc), nbr is different from nbc");
  MatrixStorage* nsto=findMatrixStorage(stringId, _skyline, _sym, buildType_, true, nbr*nbRows_, nbc*nbCols_);
  if(nsto!=nullptr) return static_cast<SymSkylineStorage*>(nsto);
  std::vector<std::vector<number_t> > cols=scalarColIndices(nbr, nbc);
  SymSkylineStorage* ss = new SymSkylineStorage(nbr*nbRows_, cols, stringId);
  ss->buildType() = buildType_;
  ss->scalarFlag()=true;
  return ss;
}

/*--------------------------------------------------------------------------------
   check if two storages have the same structures
--------------------------------------------------------------------------------*/
bool SymSkylineStorage::sameStorage(const MatrixStorage& sto) const
{
  if(!(sto.storageType()==storageType_ &&
       sto.accessType()==accessType_ &&
       sto.nbOfRows()==nbRows_ &&
       sto.nbOfColumns()==nbCols_&&
       sto.size()==size()))
    return false;
  const SymSkylineStorage& csto=static_cast<const SymSkylineStorage&>(sto);
  //compare structure pointers
  if(rowPointer_!=csto.rowPointer_) return false;
  return true;
}

/*--------------------------------------------------------------------------------
 add submatrix indices given by its row and column indices (>= 1)
--------------------------------------------------------------------------------*/
void SymSkylineStorage::addSubMatrixIndices(const std::vector<number_t>& rows,const std::vector<number_t>& cols)
{
  addSkylineSubMatrixIndices(rowPointer_,rows,cols);
}

/*--------------------------------------------------------------------------------
 access operator to position of (i,j)>0 coefficients (return 0 if outside)
 This DIRECT access to (i,j) pointer SHOULD NOT be used in matrix computation
--------------------------------------------------------------------------------*/
number_t SymSkylineStorage::pos(number_t i, number_t j, SymType s) const
{
  if(i == 0 || i > nbRows_ || j == 0 || j > nbCols_) { return 0; }
  if(i == j) { return i; }
  if(i > j) // lower triangular part
    {
      number_t l = rowPointer_[i] - rowPointer_[i - 1];
      if(i <= l + j)  { return nbRows_ + rowPointer_[i] + j - i + 1; } // add 1 because values begin at address 1
      else { return 0; }
    }
  // upper triangular part
  number_t l = rowPointer_[j] - rowPointer_[j - 1];
  if(j <= i + l)
    {
      if(s != _noSymmetry) { return nbRows_ + rowPointer_[j] + i - j + 1; }         // symmetric matrix
      else      { return nbRows_ + lowerPartSize() + rowPointer_[j] + i - j + 1; }  // non symmetric matrix
    }
  return 0;
}

/*--------------------------------------------------------------------------------
 access to submatrix adresses, useful for assembling matrices
 rows and cols are vector of index (>0) of row and column, no index ordering is assumed !!
 return the vector of adresses (relative number >0 in storage) in a row storage (as Matrix)
 if a row or a col index is out of storage
     an error is raised if errorOn is true (default)
     position is set to 0 else
--------------------------------------------------------------------------------*/
void SymSkylineStorage::positions(const std::vector<number_t>& rows, const std::vector<number_t>& cols,
                                  std::vector<number_t>& adrs, bool errorOn, SymType sy) const
{
  number_t nba = rows.size() * cols.size();
  if(adrs.size() != nba) { adrs.resize(nba, 0); }    // reset size
  std::vector<number_t>::iterator itp = adrs.begin();
  std::vector<number_t>::const_iterator itr, itc, itb, ite;

  for(itr = rows.begin(); itr != rows.end(); itr++)  // loop on row index
    {
      for(itc = cols.begin(); itc != cols.end(); itc++, itp++) // loop on col indices
        {
          *itp = pos(*itr, *itc, sy);
          if(*itp == 0 && errorOn) { error("storage_outofstorage", "SymSkyline", *itr, *itc); }
        }
    }
}

// get (col indices, adress) of row r in set [c1,c2]
//   SymType != noSymmetry means that the upper part is not stored
//              so an upper part coefficient address is the address of its symmetric coefficient in lower part
//   SymType == noSymmetry  means that the upper part is stored after diagonal part and lower part
std::vector<std::pair<number_t, number_t> > SymSkylineStorage::getRow(SymType s, number_t r, number_t c1, number_t c2) const
{
  number_t nbc=c2;
  if(nbc==0) nbc=nbCols_;
  std::vector<std::pair<number_t, number_t> > cols(nbc-c1+1);
  std::vector<std::pair<number_t, number_t> >::iterator itcol=cols.begin();
  number_t l=0;
  if(c1 < r) //lower part
    {
      number_t rb=rowPointer_[r-1], re=rowPointer_[r];
      for(number_t k = rb; k < re; k++, itcol++, l++) // find in list of col index of row i
        {
          number_t c=k+r-re;
          if(c >= c1) *itcol = std::make_pair(c,nbRows_+k+1);   // add 1 because values begin at address 1
        }
    }

  if(c1 <= r && r<=nbc) {*itcol++=std::make_pair(r,r); l++;} //diag coeff
//upper part
  for(number_t c=r+1; c<=nbc; c++)
    {
      number_t a=pos(r,c,s);
      if(a!=0)
        {
          *itcol=std::make_pair(c,a);
          l++; itcol++;
        }
    }
  if(l>0) cols.resize(l);
  else cols.clear();
  return cols;
}

// get (row indices, adress) of col c in set [r1,r2]
//   SymType != noSymmetry means that the upper part is not stored
//              so an upper part coefficient address is the address of its symmetric coefficient in lower part
//   SymType == noSymmetry  means that the upper part is stored after diagonal part and lower part
std::vector<std::pair<number_t, number_t> > SymSkylineStorage::getCol(SymType s, number_t c, number_t r1, number_t r2) const
{
  number_t nbr=r2;
  if(nbr==0) nbr=nbRows_;
  std::vector<std::pair<number_t, number_t> > rows(nbr-r1+1);
  std::vector<std::pair<number_t, number_t> >::iterator itrow=rows.begin();
  number_t l=0;
  if(r1 < c) //upper part
    {
      number_t cb=rowPointer_[c-1], ce=rowPointer_[c];
      number_t shift=diagonalSize() + rowPointer_[nbRows_];
      if(s==_noSymmetry) shift+=lowerPartSize();
      for(number_t k = cb; k < ce; k++, itrow++, l++) // find in list of col index of row i
        {
          number_t r=k+c-ce;
          if(r >= r1) *itrow = std::make_pair(r,shift+k+1);  // add 1 because values begin at address 1
        }
    }
  if(r1 <= c && c<=nbr) {*itrow++=std::make_pair(c,c); l++;} //diag coeff
  for(number_t r=c+1; r<=nbr; r++)             //lower part
    {
      number_t a=pos(r,c);
      if(a!=0)
        {
          *itrow=std::make_pair(r,a);
          l++; itrow++;
        }
    }
  if(l>0) rows.resize(l);
  else rows.clear();
  return rows;
}

// print storage pointers
void SymSkylineStorage::print(std::ostream& os) const
{
  printHeader(os);
  os<<"row pointer = "<<rowPointer_<<eol;
}

/*--------------------------------------------------------------------------------
 print SymSkyline Storage matrix to output stream
--------------------------------------------------------------------------------*/
// print real scalar matrix
void SymSkylineStorage::printEntries(std::ostream& os, const std::vector<real_t>& m, number_t vb, const SymType sym) const
{
  std::vector<real_t>::const_iterator itm = m.begin() + 1, itml = itm + nbRows_,
                                      itmu = itm + nbRows_ + lowerPartSize();
  printEntriesTriangularPart(_scalar, itm, itml, rowPointer_, entriesPerRow, entryWidth, entryPrec, "row", vb, os);
  itm = m.begin() + 1;
  if(sym == _noSymmetry)  // print upper part if a non "symmetric" matrix
    {
      printEntriesTriangularPart(_scalar, itm, itmu, rowPointer_, entriesPerRow, entryWidth, entryPrec, "col", vb, os);
    }
}

// print complex scalar matrix
void SymSkylineStorage::printEntries(std::ostream& os, const std::vector<complex_t>& m, number_t vb, SymType sym) const
{
  std::vector<complex_t>::const_iterator itm = m.begin() + 1, itml = itm + nbRows_;
//                                       itmu = itm + nbRows_ + lowerPartSize();
  printEntriesTriangularPart(_scalar, itm, itml, rowPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "row", vb, os);
  itm = m.begin() + 1;
  if(sym == _noSymmetry)    // print upper part if a non "symmetric" matrix
    {
      printEntriesTriangularPart(_scalar, itm, itml, rowPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "col", vb, os);
    }
}

// large matrix of vectors are not available for the moment, use Matrix n x 1 or 1 x n
void SymSkylineStorage::printEntries(std::ostream& os, const std::vector< Vector<real_t> >& m, number_t vb, SymType sym) const
{error("storage_novector", "SymSkylineStorage::printEntries");}

void SymSkylineStorage::printEntries(std::ostream& os, const std::vector< Vector<complex_t> >& m, number_t vb, SymType sym) const
{error("storage_novector", "SymSkylineStorage::printEntries");}

// print matrix of real matrices
void SymSkylineStorage::printEntries(std::ostream& os, const std::vector< Matrix<real_t> >& m, number_t vb, SymType sym) const
{
  std::vector< Matrix<real_t> >::const_iterator itm = m.begin() + 1, itml = itm + nbRows_,
                                                itmu = itm + nbRows_ + lowerPartSize();
  printEntriesTriangularPart(_matrix, itm, itml, rowPointer_, entriesPerRow, entryWidth, entryPrec, "row", vb, os);
  itm = m.begin() + 1;
  if(sym == _noSymmetry)    // print upper part if a non "symmetric" matrix
    {
      printEntriesTriangularPart(_matrix, itm, itmu, rowPointer_, entriesPerRow, entryWidth, entryPrec, "col", vb, os);
    }
}

// print matrix of complex matrices
void SymSkylineStorage::printEntries(std::ostream& os, const std::vector< Matrix<complex_t> >& m, number_t vb, SymType sym) const
{
  std::vector< Matrix<complex_t> >::const_iterator itm = m.begin() + 1, itml = itm + nbRows_,
                                                   itmu = itm + nbRows_ + lowerPartSize();
  printEntriesTriangularPart(_matrix, itm, itml, rowPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "row", vb, os);
  itm = m.begin() + 1;
  if(sym == _noSymmetry)    // print upper part if a non "symmetric" matrix
    {
      printEntriesTriangularPart(_matrix, itm, itmu, rowPointer_, entriesPerRow / 2, 2 * entryWidth + 1, entryPrec, "col", vb, os);
    }
}

/*
--------------------------------------------------------------------------------
 print matrix to ostream in coordinate form: i j M_ij
 according to type of values of matrix (overload the general method in MatrixStorage)
 sym is not used
--------------------------------------------------------------------------------
*/
void SymSkylineStorage::printCooMatrix(std::ostream& os, const std::vector<real_t>& m, SymType sym) const
{
  std::vector<real_t>::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml;
  if(sym == _noSymmetry) { itmu += lowerPartSize(); }
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, rowPointer_, nbRows_, nbCols_, true);
  printCooTriangularPart(os, itmu, rowPointer_, nbCols_, nbRows_, false, sym);
}

void SymSkylineStorage::printCooMatrix(std::ostream& os, const std::vector<complex_t>& m, SymType sym) const
{
  std::vector<complex_t>::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml;
  if(sym == _noSymmetry) { itmu += lowerPartSize(); }
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, rowPointer_, nbRows_, nbCols_, true);
  printCooTriangularPart(os, itmu, rowPointer_, nbCols_, nbRows_, false, sym);
}

void SymSkylineStorage::printCooMatrix(std::ostream& os, const std::vector< Matrix<real_t> >& m, SymType sym) const
{
  std::vector< Matrix<real_t> >::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml;
  if(sym == _noSymmetry) { itmu += lowerPartSize(); }
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, rowPointer_, nbRows_, nbCols_, true);
  printCooTriangularPart(os, itmu, rowPointer_, nbCols_, nbRows_, false, sym);
}

void SymSkylineStorage::printCooMatrix(std::ostream& os, const std::vector< Matrix<complex_t> >& m, SymType sym) const
{
  std::vector< Matrix<complex_t> >::const_iterator itmb = m.begin() + 1, itml = itmb + diagonalSize(), itmu = itml;
  if(sym == _noSymmetry) { itmu += lowerPartSize(); }
  printCooDiagonalPart(os, itmb, diagonalSize());
  printCooTriangularPart(os, itml, rowPointer_, nbRows_, nbCols_, true);
  printCooTriangularPart(os, itmu, rowPointer_, nbCols_, nbRows_, false, sym);
}

/*--------------------------------------------------------------------------------
 template Matrix x Vector & Vector x Matrix multiplications and specializations
 result vector has to be sized before
--------------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void SymSkylineStorage::multMatrixVector(const std::vector<M>& m, V* vp, R* rp, SymType sym) const
{
  trace_p->push("SymSkylineStorage::multMatrixVector (pointer form)");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  R* rpe= rp + nbRows_;
  SkylineStorage::diagonalMatrixVector(itm, vp, rp, rpe);   // product by diagonal
  itl = m.begin() + 1 + nbRows_;
  SkylineStorage::lowerMatrixVector(rowPointer_, itl, vp, rp, _noSymmetry);  // lower part
  itl = m.begin() + 1 + nbRows_;
  if(sym == _noSymmetry) { itl += lowerPartSize(); }                   // case of a non symmetric matrix with symmetric storage
  SkylineStorage::upperMatrixVector(rowPointer_, itl, vp, rp, sym);    // upper part
  trace_p->pop();
}

template<typename M, typename V, typename R>
void SymSkylineStorage::multMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, SymType sym) const
{
  trace_p->push("SymSkylineStorage::multMatrixVector");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = r.end();
  SkylineStorage::diagonalMatrixVector(itm, itvb, itrb, itre);   // product by diagonal
  itl = m.begin() + 1 + nbRows_;
  SkylineStorage::lowerMatrixVector(rowPointer_, itl, itvb, itrb, _noSymmetry);  // lower part
  itl = m.begin() + 1 + nbRows_;
  if(sym == _noSymmetry) { itl += lowerPartSize(); }                     // case of a non symmetric matrix with symmetric storage
  SkylineStorage::upperMatrixVector(rowPointer_, itl, itvb, itrb, sym);  // upper part
  trace_p->pop();
}

template<typename M, typename V, typename R>
void SymSkylineStorage::multVectorMatrix(const std::vector<M>& m, V* vp, R* rp, SymType sym) const
{
  trace_p->push("SymSkylineStorage::multVectorMatrix (pointer form");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  R* rpe= rp + nbRows_;
  SkylineStorage::diagonalVectorMatrix(itm, vp, rp, rpe);   // product by diagonal
  itl = m.begin() + 1 + nbRows_;
  SkylineStorage::lowerVectorMatrix(rowPointer_, itl, vp, rp, _noSymmetry);  // lower part
  itl = m.begin() + 1 + nbRows_;
  if(sym == _noSymmetry) { itl += lowerPartSize(); }                   // case of a non symmetric matrix with symmetric storage
  SkylineStorage::upperVectorMatrix(rowPointer_, itl, vp, rp, sym);    // upper part
  trace_p->pop();
}

template<typename M, typename V, typename R>
void SymSkylineStorage::multVectorMatrix(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, SymType sym) const
{
  trace_p->push("SymSkylineStorage::multVectorMatrix");
  typename std::vector<M>::const_iterator itm = m.begin() + 1, itl;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = r.end();
  SkylineStorage::diagonalVectorMatrix(itm, itvb, itrb, itre);   // product by diagonal
  itl = m.begin() + 1 + nbRows_;
  SkylineStorage::lowerVectorMatrix(rowPointer_, itl, itvb, itrb, _noSymmetry);  // lower part
  itl = m.begin() + 1 + nbRows_;
  if(sym == _noSymmetry) { itl += lowerPartSize(); }                     // case of a non symmetric matrix with symmetric storage
  SkylineStorage::upperVectorMatrix(rowPointer_, itl, itvb, itrb, sym);  // upper part
  trace_p->pop();
}

/*-----------------------------------------------------------------------
                         matrix + matrix
--------------------------------------------------------------------------*/
/*!
   Addition two skyline matrices
   \param[in,out] m1 vector values_ of the first matrix
   \param[in] st1 symmetric type of the first matrix
   \param[in] rowPtr2 rowPointer of storage of the second matrix
   \param[in] colPtr2 colPointer of storage of the second matrix
   \param[in] m2 vector values_ of the second matrix
   \param[in] st2 symmetric type of the second matrix
 */
template<typename M1, typename M2>
void SymSkylineStorage::addTwoMatrixSymSkyline(std::vector<M1>& m1, SymType st1,
    const std::vector<number_t>& rowPtr2, const std::vector<number_t>& colPtr2, const std::vector<M2>& m2, SymType st2)
{
  trace_p->push("DualSkylineStorage::addTwoMatrixDualSkyline");
  typename std::vector<M1>::const_iterator itm1 = m1.begin()+1, itm1Tmp;
  typename std::vector<M2>::const_iterator itm2 = m2.begin()+1;
  std::vector<M1> nValues(diagonalSize()+1, M1());
  typename std::vector<M1>::iterator itv = nValues.begin()+1, itev = nValues.end();

  for(; itv != itev; ++itv, ++itm1, ++itm2) *itv = *(itm1) + *(itm2);

  std::vector<number_t>::iterator itbrP1 = rowPointer_.begin() + 1, itrP1, iterP1 = rowPointer_.end();
  std::vector<number_t>::const_iterator itbrP2 = rowPtr2.begin() + 1, itrP2;

  itrP1 = itbrP1;
  itrP2 = itbrP2;
  number_t distance1 = 0, distance2 = 0;

  // Update the lower part
  for(; itrP1 != (iterP1-1); ++itrP1, ++itrP2)
    {
      distance1 = *(itrP1+1) - *itrP1;
      distance2 = *(itrP2+1) - *itrP2;
      if(distance1 <= distance2)
        {
          for(number_t i = 0; i < (distance2 - distance1); ++i) nValues.push_back(*(itm2++));
          for(number_t i = 0; i < distance1; ++i) nValues.push_back(*(itm1++) + *(itm2++));

        }
      else
        {
          for(number_t i = 0; i < (distance1 - distance2); ++i) nValues.push_back(*(itm1++));
          for(number_t i = 0; i < distance2; ++i) nValues.push_back(*(itm1++) + *(itm2++));
        }
    }

  itrP1 = itbrP1 = rowPointer_.begin()+1; iterP1 = rowPointer_.end();
  itrP2 = itbrP2 = colPtr2.begin()+1;
  number_t distVal = std::distance(nValues.begin(), nValues.end());

  // Update the upper part
  if(_noSymmetry != st2) itm2 = m2.begin()+diagonalSize()+1;
  if(_noSymmetry != st1) itm1 = m1.begin()+diagonalSize()+1;

  switch(st2)
    {
      case _skewSymmetric:
        for(; itrP1 != (iterP1-1); ++itrP1, ++itrP2)
          {
            distance1 = *(itrP1+1) - *itrP1;
            distance2 = *(itrP2+1) - *itrP2;
            if(distance1 <= distance2)
              {
                for(number_t i = 0; i < (distance2 - distance1); ++i) nValues.push_back(-*(itm2++));
                for(number_t i = 0; i < distance1; ++i) nValues.push_back(*(itm1++) - *(itm2++));

              }
            else
              {
                for(number_t i = 0; i < (distance1 - distance2); ++i) nValues.push_back(*(itm1++));
                for(number_t i = 0; i < distance2; ++i) nValues.push_back(*(itm1++) - *(itm2++));
              }
          }
        break;
      case _selfAdjoint:
        for(; itrP1 != (iterP1-1); ++itrP1, ++itrP2)
          {
            distance1 = *(itrP1+1) - *itrP1;
            distance2 = *(itrP2+1) - *itrP2;
            if(distance1 <= distance2)
              {
                for(number_t i = 0; i < (distance2 - distance1); ++i) nValues.push_back(-*(itm2++));
                for(number_t i = 0; i < distance1; ++i) nValues.push_back(*(itm1++) + conj(*(itm2++)));
              }
            else
              {
                for(number_t i = 0; i < (distance1 - distance2); ++i) nValues.push_back(*(itm1++));
                for(number_t i = 0; i < distance2; ++i) nValues.push_back(*(itm1++) + conj(*(itm2++)));
              }
          }
        break;

      case _skewAdjoint:
        for(; itrP1 != (iterP1-1); ++itrP1, ++itrP2)
          {
            distance1 = *(itrP1+1) - *itrP1;
            distance2 = *(itrP2+1) - *itrP2;
            if(distance1 <= distance2)
              {
                for(number_t i = 0; i < (distance2 - distance1); ++i) nValues.push_back(-*(itm2++));
                for(number_t i = 0; i < distance1; ++i) nValues.push_back(*(itm1++) - conj(*(itm2++)));
              }
            else
              {
                for(number_t i = 0; i < (distance1 - distance2); ++i) nValues.push_back(*(itm1++));
                for(number_t i = 0; i < distance2; ++i) nValues.push_back(*(itm1++) - conj(*(itm2++)));
              }
          }
        break;
      default:
        for(; itrP1 != (iterP1-1); ++itrP1, ++itrP2)
          {
            distance1 = *(itrP1+1) - *itrP1;
            distance2 = *(itrP2+1) - *itrP2;
            if(distance1 <= distance2)
              {
                for(number_t i = 0; i < (distance2 - distance1); ++i) nValues.push_back(*(itm2++));
                for(number_t i = 0; i < distance1; ++i) nValues.push_back(*(itm1++) + (*(itm2++)));
              }
            else
              {
                for(number_t i = 0; i < (distance1 - distance2); ++i) nValues.push_back(*(itm1++));
                for(number_t i = 0; i < distance2; ++i) nValues.push_back(*(itm1++) + (*(itm2++)));
              }
          }
        break;
    }


  // Duplicate and stupid code but it works, it needs improving
  if(_noSymmetry != st1) itm1 = m1.begin() + diagonalSize() + 1;
  itv = nValues.begin() + distVal;
  itrP1 = itbrP1;
  itrP2 = itbrP2;

  switch(st1)
    {
      case _skewSymmetric:
        for(; itrP1 != (iterP1-1); ++itrP1, ++itrP2)
          {
            distance1 = *(itrP1+1) - *itrP1;
            distance2 = *(itrP2+1) - *itrP2;
            if(distance1 <= distance2)
              {
                for(number_t i = 0; i < (distance2 - distance1); ++i) ++itv;  // nValues.push_back(-*(itm2++));
                for(number_t i = 0; i < distance1; ++i) *(itv++) -= 2 * *(itm1++);  //nValues.push_back(*(itm1++) - *(itm2++));
              }
            else
              {
                for(number_t i = 0; i < (distance1 - distance2); ++i) *(itv++) -= 2 * *(itm1++);  //nValues.push_back(*(itm1++));
                for(number_t i = 0; i < distance2; ++i) *(itv++) -= 2 * *(itm1++);  //nValues.push_back(*(itm1++) - *(itm2++));
              }
          }
        break;
      case _selfAdjoint:
        for(; itrP1 != (iterP1-1); ++itrP1, ++itrP2)
          {
            distance1 = *(itrP1+1) - *itrP1;
            distance2 = *(itrP2+1) - *itrP2;
            if(distance1 <= distance2)
              {
                for(number_t i = 0; i < (distance2 - distance1); ++i) ++itv;  //nValues.push_back(-*(itm2++));
                for(number_t i = 0; i < distance1; ++i) { *(itv) -= *(itm1); *(itv++) += conj(*(itm1++)); } //;nValues.push_back(*(itm1++) + conj(*(itm2++)));

              }
            else
              {
                for(number_t i = 0; i < (distance1 - distance2); ++i) { *(itv) -= *(itm1); *(itv++) += conj(*(itm1++)); }  //nValues.push_back(*(itm1++));
                for(number_t i = 0; i < distance2; ++i) { *(itv) -= *(itm1); *(itv++) += conj(*(itm1++)); }  //nValues.push_back(*(itm1++) + conj(*(itm2++)));
              }
          }
        break;

      case _skewAdjoint:
        for(; itrP1 != (iterP1-1); ++itrP1, ++itrP2)
          {
            distance1 = *(itrP1+1) - *itrP1;
            distance2 = *(itrP2+1) - *itrP2;
            if(distance1 <= distance2)
              {
                for(number_t i = 0; i < (distance2 - distance1); ++i) ++itv;  //nValues.push_back(-*(itm2++));
                for(number_t i = 0; i < distance1; ++i) { *(itv) -= *(itm1); *(itv++) -= conj(*(itm1++)); }  //nValues.push_back(*(itm1++) - conj(*(itm2++)));
              }
            else
              {
                for(number_t i = 0; i < (distance1 - distance2); ++i) { *(itv) -= *(itm1); *(itv++) -= conj(*(itm1++)); }  //nValues.push_back(*(itm1++));
                for(number_t i = 0; i < distance2; ++i) { *(itv) -= *(itm1); *(itv++) -= conj(*(itm1++)); }  //nValues.push_back(*(itm1++) - conj(*(itm2++)));
              }
          }
        break;
      default:
        break;
    }


  if(m1.size() != nValues.size()) m1.resize(nValues.size());
  std::copy(nValues.begin(), nValues.end(), m1.begin());

  // Update the and colPointer
  // Update the colPointer
  itrP1 = itbrP1;
  itrP2 = itbrP2;
  number_t updatePrev = 0, updateNext = 0;
  for(; itrP1 != (iterP1-1); ++itrP1, ++itrP2)
    {
      distance1 = *(itrP1+1) - *itrP1;
      distance2 = *(itrP2+1) - *itrP2;
      updateNext = std::max(distance1, distance2);
      if(*itrP1 != *itrP2) *itrP1 = updatePrev + *(itrP1-1);
      updatePrev = updateNext;
    }
  if(*itrP1 != *itrP2) *itrP1 = updatePrev + *(itrP1-1);

  trace_p->pop();
}
/*-----------------------------------------------------------------------
              triangular part matrix * vector
--------------------------------------------------------------------------*/
template<typename M, typename V, typename R>
void SymSkylineStorage::diagonalMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  if(nbRows_>nbCols_) r.assign(nbRows_,R(0));
  else r.resize(nbRows_);  //to reset to 0
  typename std::vector<M>::const_iterator itmb = m.begin() + 1;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = itrb + diagonalSize();
  SkylineStorage::diagonalVectorMatrix(itmb, itvb, itrb, itre);   //product by diagonal
}

template<typename M, typename V, typename R>
void SymSkylineStorage::lowerMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
  else r.resize(nbRows_);
  typename std::vector<M>::const_iterator itmb = m.begin() + 1;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = itrb + diagonalSize();
  SkylineStorage::diagonalMatrixVector(itmb, itvb, itrb, itre);   //product by diagonal
  itmb = m.begin() + 1 + diagonalSize();
  SkylineStorage::lowerMatrixVector(rowPointer_, itmb, itvb, itrb, _noSymmetry);  //lower part
}

template<typename M, typename V, typename R>
void SymSkylineStorage::lowerD1MatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r) const
{
  if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
  else r.resize(nbRows_);
  typename std::vector<M>::const_iterator itmb=m.begin()+1;
  typename std::vector<V>::const_iterator itvb = v.begin(), itv=itvb;
  typename std::vector<R>::iterator itrb = r.begin(), itr=itrb;
  for(number_t i=0; i<std::min(nbRows_,nbCols_); i++, ++itv,++itr) *itr = *itv; //diag part
  itmb = m.begin() + 1 + diagonalSize();
  SkylineStorage::lowerMatrixVector(rowPointer_, itmb, itvb, itrb, _noSymmetry);  //lower part
}

template<typename M, typename V, typename R>
void SymSkylineStorage::upperMatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, SymType s) const
{
  if(nbRows_>nbCols_) r.assign(nbRows_,R(0));
  else r.resize(nbRows_); //to reset to 0
  typename std::vector<M>::const_iterator itmb = m.begin() + 1;
  typename std::vector<V>::const_iterator itvb = v.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = itrb + diagonalSize();
  SkylineStorage::diagonalMatrixVector(itmb, itvb, itrb, itre);   //product by diagonal
  itmb = m.begin() + 1 + diagonalSize();
  if(s == _noSymmetry) { itmb += lowerPartSize(); }     // case of a non symmetric matrix with symmetric storage
  SkylineStorage::upperMatrixVector(rowPointer_, itmb, itvb, itrb, s);  // upper part
}

template<typename M, typename V, typename R>
void SymSkylineStorage::upperD1MatrixVector(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r, SymType s) const
{
  if(nbRows_>nbCols_) r.assign(nbRows_,R(0));  //to reset to 0
  else r.resize(nbRows_);
  typename std::vector<M>::const_iterator itmb=m.begin()+1;
  typename std::vector<V>::const_iterator itvb=v.begin(), itv=itvb;
  typename std::vector<R>::iterator itrb=r.begin(), itr=itrb;
  for(number_t i=0; i<std::min(nbRows_,nbCols_); i++, ++itv,++itr) *itr = *itv; //diag part
  itmb = m.begin() + 1 +diagonalSize();
  if(s == _noSymmetry) { itmb += lowerPartSize(); }     // case of a non symmetric matrix with symmetric storage
  SkylineStorage::upperMatrixVector(rowPointer_, itmb, itvb, itrb, s);  // upper part
}

/*!
--------------------------------------------------------------------------------
   Template L.D.Lt factorization

    Factorization of matrix M as matrix F = L D Lt
    where L is a lower triangular matrix with unit diagonal
            and is stored as the strict lower triangular part of F
      and D is a diagonal matrix stored as diagonal of matrix F:

    \f$\sum_{ C1(i) <= k <= j } L_{ik} D_{kk} L_{jk} = M_{ij} for ( j <= i )\f$

    where C1(i) is the column index of first non zero entry on row i
--------------------------------------------------------------------------------
*/
template<typename M>
void SymSkylineStorage::ldlt(std::vector<M>& m, std::vector<M>& f, const SymType sym) const
{
  trace_p->push("SymSkylineStorage::ldlt");

  // iterators on diagonal (md) and strict lower triangular part (ml) of input matrix M
  typename std::vector<M>::iterator it_md=m.begin() + 1, it_ml=it_md + nbRows_;
  // iterators on diagonal (d) and strict lower triangular part (l) of 'matrix' F
  typename std::vector<M>::iterator it_d=f.begin() + 1, it_lb=it_d + nbRows_, it_l, it_lnx=it_lb;
  // iterators on row adresses starting with 1st row (row 0)
  std::vector<number_t>::const_iterator itRowNx=rowPointer_.begin(), it_row;

  // iterators on row adresses starting with 2nd row (first row is row 0)
  // std::vector<number_t>::iterator itRowNx(rowPointer_.begin()+1), it_row;
  // first diagonal entry D_{11}
  if(abs(*it_md) < theZeroThreshold) { isSingular("L.D.Lt", 0); }

  //for ( number_t row = 1; row < nbRows_; row++, it_d++, it_md++ )
  for(number_t row = 0; row < nbRows_; ++row, ++it_d, ++it_md)
    {
      // initialize D_{ii} with M_{ii}
      *it_d = *it_md;

      // iterator to first entry on current row (say row i)
      // and number of entries on current row ( C1(i) = i - rowEntries )
      it_row = itRowNx++;
      number_t rowEntries = *itRowNx - *it_row;
      it_l = it_lnx; it_lnx = it_lb + *itRowNx;
      if(rowEntries > 0)
        {
          /*
             compute sub-diagonal entries of factorized matrix on current row (say row i)
             by solving sub-system consisting of previous rows:
             \sum_{C1(i)<=k<=j}  L_{jk} (LD)_{ik} = M_{ij}  ( for C1(i) <= j < i )
             where L_{jk}'s are computed on previous rows and (LD)_{ik}'s are temporary unknowns
          */
          bzLowerD1Solver(it_lb, it_ml + *it_row, it_l, it_lnx, it_row - rowEntries);

          /*
             compute ( with D_{kk} known from previous rows )
                 L_{ik} = (LD)_{ik} / D_{kk} ( k < i )
             and D_{ii} = M_{ii} - \sum_{C1(i)<=k<i} L_{ik} * L_{ik}
          */
          typename std::vector<M>::iterator d_k = it_d - rowEntries, l_ik;
          for(l_ik = it_l; l_ik != it_lnx; ++l_ik, ++d_k)
            {
              *l_ik /= *d_k; *it_d -= *l_ik * *l_ik * *d_k;
            }
        }
      if(abs(*it_d) < theZeroThreshold) { isSingular("L.D.Lt", row); }
    }
  trace_p->pop();
}

/*
--------------------------------------------------------------------------------
  Template L.U factorization
    Factorization of matrix M stored as matrix F = L U where
    - L is a lower triangular matrix with unit diagonal and is stored as
    the strict lower triangular part of F
    - U is a upper triangular matrix with diagonal stored as diagonal part of F
    and strict upper triangular part stored as the strict upper triangular part of F:

            \sum_{ max(C1(i),L1(j)) <= k <= j } L_{ik} U{kj} = M_{ij}

    where C1(i) is the column index of first non zero entry on row i
    -     L1(j) and the row index of first non zero entry on column j

    CAUTION Matrix M may have a symmetry property, but
            Matrix F ***MUST HAVE NO*** symmetry property since L and U are computed
            with a different set of rules (THIS IS NOT CHECKED HERE)
--------------------------------------------------------------------------------
*/
template<typename M>
void SymSkylineStorage::lu(std::vector<M>& m, std::vector<M>& f, const SymType sym) const
{
  trace_p->push("SymSkylineStorage::lu");

  // iterators on diagonal (md), strict lower (ml) and strict upper (mu) triangular part of M
  typename std::vector<M>::iterator it_md(m.begin() + 1), it_ml(it_md + nbRows_), it_mu(it_ml);
  if(sym == _noSymmetry) { it_mu += *(rowPointer_.end() - 1); }

  // iterators on diagonal (d), strict lower (l) and strict upper (u) triangular part
  // of non symmetric matrix F
  typename std::vector<M>::iterator it_d(f.begin() + 1), it_lb(it_d + nbRows_), it_lnx(it_lb), it_l;
  typename std::vector<M>::iterator it_ub(it_lb + * (rowPointer_.end() - 1)), it_unx(it_ub), it_u;

  // iterators on row adresses starting with 2nd row (first row is row 0)
  std::vector<number_t>::const_iterator it_rownx(rowPointer_.begin()), it_row;

  // first diagonal entry D_{11} = U_{11} = M_{11}
  if(abs(*it_md) < NumTraits<M>::epsilon()) { isSingular("L.U", 0); }

  for(number_t row = 0; row < nbRows_; ++it_d, ++it_md, ++row)
    {
      // initialize D_{ii} with M_{ii}
      *it_d = *it_md;

      // iterator to first entry on current row
      // and number of entries on current row ( rowEntries = i - C1(i) )
      it_row = it_rownx; ++it_rownx;
      number_t rowEntries = *it_rownx - *it_row;

      it_l = it_lnx; it_lnx = it_lb + *it_rownx;
      it_u = it_unx; it_unx = it_ub + *it_rownx;

      if(rowEntries > 0)
        {
          /*
           compute sub-diagonal entries of factorized matrix on current row (say i)
           by solving the following sub-system:
           \sum_{k <= j}  L_{ik} U_{kj} = M_{ij}  ( for i > j )
           where U_{kj}'s are computed on previous columns
          */

          // Need one new function bzLowerSolverT
          bzLowerSolver(it_d - rowEntries, it_ub, it_ml + *it_row, it_l, it_lnx, it_row - rowEntries);

          /*
           compute super-diagonal entries of factorized matrix on current column (say j)
           by solving the following sub-system:
           \sum_{k <= j}  L{ik} U_{kj} = M_{ij}  ( for i < j )
           where L_{ik}'s are computed on previous rows
          */
          bzLowerD1Solver(it_lb, it_mu + *it_row, it_u, it_unx, it_row - rowEntries);

          /*
           compute diagonal entry
           D_{ii} = M_{ii} - \sum_{C1(i) <= k < i} L_{ik} * U_{ki}
          */
          for(typename std::vector<M>::iterator u_ki = it_u; u_ki != it_unx; ++u_ki)
            {  *it_d -= *it_l * *u_ki; ++it_l; }
        }
      if(abs(*it_d) < NumTraits<M>::epsilon()) { isSingular("L.U", row); }
    }
  trace_p->pop();
}

/*
--------------------------------------------------------------------------------
    Template L.D.L* factorization (complex only)

    Factorization of matrix M as matrix F = L D L*
    where L is a lower triangular matrix with unit diagonal
     and is stored as the strict lower triangular part of F
     and D is a diagonal matrix stored as diagonal of matrix F:

    \sum_{ C1(i) <= k <= j } L_{ik} D_{kk} conj(L_{jk}) = M_{ij} for ( j <= i )

    where C1(i) is the column index of first non zero entry on row i
--------------------------------------------------------------------------------
 */
template<typename M>
void SymSkylineStorage::ldlstar(std::vector<M>& m, std::vector<M>& f) const
{
  trace_p->push("SymSkylineStorage::ldlstar");

  // iterators on diagonal (md) and strict lower triangular part (ml) of input matrix M
  typename std::vector<M>::iterator it_md(m.begin() + 1), it_ml(it_md + nbRows_);

  // iterators on diagonal (d) and strict lower triangular part (l) of 'matrix' F
  typename std::vector<M>::iterator it_d(f.begin() + 1), it_lb(it_d + nbRows_), it_l, it_lnx(it_lb);

  // iterators on row adresses starting with 1st row (row 0)
  std::vector<number_t>::const_iterator it_rowNx(rowPointer_.begin()), it_row;

  for(number_t row = 0; row < nbRows_; ++row, ++it_d, ++it_md)
    {
      // initialize D_{ii} with M_{ii}
      *it_d = *it_md;

      // iterator to first entry on current row (say row i)
      // and number of entries on current row ( C1(i) = i - rowEntries )
      it_row = it_rowNx; ++it_rowNx;
      number_t rowEntries = *it_rowNx - *it_row;
      it_l = it_lnx; it_lnx = it_lb + *it_rowNx;
      if(rowEntries > 0)
        {
          /*
           compute sub-diagonal entries of factorized matrix on current row (say row i)
           by solving sub-system consisting of previous rows:
           \sum_{C1(i) <= k <= j}  conj(L_{jk}) (LD)_{ik} = M_{ij}  ( for C1(i) <= j < i )
           where L_{jk}'s are computed on previous rows and (LD)_{ik}'s are temporary unknowns
          */
          bzLowerConjD1Solver(it_lb, it_ml + *it_row, it_l, it_lnx, it_row - rowEntries);  // Need to change to bwLowerD1Solver

          /*
              compute ( with D_{kk} known from previous rows )
              L_{ik} = (LD)_{ik} / D_{kk} ( k < i )
              and D_{ii} = M_{ii} - \sum_{C1(i) <= k < i} L_{ik} * L_{ik}
          */
          typename std::vector<M>::iterator d_k = it_d - rowEntries, l_ik;
          for(l_ik = it_l; l_ik != it_lnx; ++l_ik, ++d_k)
            {
              *l_ik /= *d_k; *it_d -= *l_ik * conj(*l_ik) * *d_k;
            }
        }
      if(abs(*it_d) < theZeroThreshold) { isSingular("L.D.L*", row); }
    }
  trace_p->pop();
}

/*
---------------------------------------------------------------------------------
     Template lower triangular part with unit diagonal solver (after factorization)
--------------------------------------------------------------------------------
*/
template<typename M, typename V, typename X>
void SymSkylineStorage::lowerD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const
{
  /*
     solver for an lower triangular part linear system  with unit diagonal
     This is used in LDLt, LDL*, LDU and LU solvers for lower triangular systems
  */
  // no const v here as v might coincide with x
  trace_p->push("SymSkylineStorage::lowerD1Solver");
  bzLowerD1Solver(m.begin() + 1 + v.size(), v.begin(), x.begin(), x.end(), rowPointer_.begin());
  trace_p->pop();
}

/*
--------------------------------------------------------------------------------
     Template diagonal solver (after factorization)
--------------------------------------------------------------------------------
*/
template<typename M, typename V, typename X>
void SymSkylineStorage::diagonalSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const
{
  // diagonal solver
  // no const v here as v might coincide with x
  trace_p->push("SymSkylineStorage::diagonalSolver");
  bzDiagonalSolver(m.begin() + 1, v.begin(), x.begin(), x.end());
  trace_p->pop();
}

/*
--------------------------------------------------------------------------------
     Template upper triangular part with unit diagonal solver (after factorization)
--------------------------------------------------------------------------------
*/
template<typename M, typename V, typename X>
void SymSkylineStorage::upperD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x, const SymType sym) const
{
  /*
     solver for an upper triangular part linear system with unit diagonal
     This is used in LDLt, LDL* and LDU solvers for upper triangular systems
  */
  // no const v here as v might coincide with x
  // LO AND BEHOLD *** we use reverse_iterators here
  //
  // THIS SHOULD NOT BE RESTRICTED TO THE SYMMETRIC CASE
  // BUT WAIT UNTIL OTHER CASES ARE NECESSARY
  //
  trace_p->push("SymSkylineStorage::upperD1Solver");
  switch(sym)
    {
      case _symmetric:
      case _noSymmetry:
        bzUpperD1Solver(m.rbegin(), v.rbegin(), x.rbegin(), x.rend(), rowPointer_.rbegin());
        break;
      case _skewSymmetric:
        error("storage_not_implemented", "SymSkylineStorage::upperD1Solver", "SkewSymmetric");
        break;

      // It suffices to take opposite of right hand side
      // Use _symmetric case solver
      // Then take opposite of right hand side and solution (which may coincide)
      case _selfAdjoint:
        bzUpperConjD1Solver(m.rbegin(), v.rbegin(), x.rbegin(), x.rend(), rowPointer_.rbegin());
        break;
      case _skewAdjoint:
        error("storage_not_implemented", "SymSkylineStorage::upperD1Solver", "SkewAdjoint");
        break;

      // It suffices to take opposite conjugate of right hand side
      // Use _symmetric case solver
      // Then change take opposite conjugate of solution
      default:
        error("storage_not_implemented", "SymSkylineStorage::upperD1Solver", "Unknown");
        break;
    }
  trace_p->pop();
}

/*
--------------------------------------------------------------------------------
 template upper triangular part solver (after factorization)
      Solver for an upper triangular part linear system
      This is used in LU solver for upper triangular systems
--------------------------------------------------------------------------------
*/
template<typename M, typename V, typename X>
void SymSkylineStorage::upperSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x, const SymType sym) const
{
  // no const v here as v might coincide with x
  // ** LO AND BEHOLD *** we use reverse_iterators here ***/
  //
  // THIS SHOULD NOT BE RESTRICTED TO THE NON-SYMMETRIC CASE
  // BUT WAIT UNTIL OTHER CASES ARE NECESSARY
  //
  trace_p->push("SymSkylineStorage::upperSolver");
  switch(sym)
    {
      // Need to subtract from 1 to make sure poiting to first element of vector m
      case _symmetric: case _noSymmetry:
        bzUpperSolver(m.rend() - 1 - v.size(), m.rbegin(), v.rbegin(), x.rbegin(), x.rend(), rowPointer_.rbegin());
        break;
      case _skewSymmetric:
        error("storage_not_implemented", "SymSkylineStorage::upperSolver", "SkewSymmetric");
        break;

      // It suffices to take opposite of right hand side
      // tpl_opposite(v.begin(), v.end(), v.end());
      // Use _symmetric case solver
      // bz_upperSolver(m.rend() - v.size(), m.rbegin(), v.rbegin(), x.rbegin(), x.rend(), rowPointer_.rbegin());
      // Then take opposite of right hand side and solution (which may coincide !!)
      // tpl_opposite(v.begin(), v.end(), v.end());
      // if ( !(&v == &x) ) tpl_opposite(x.begin(), x.end(), x.end());
      //   BUT How do we compare vectors v and x
      case _selfAdjoint:
        error("storage_not_implemented", "SymSkylineStorage::upperSolver", "SelfAdjoint");
        break;

      // It suffices to take conjugate of right hand side
      // Use _symmetric case solver
      // Then change conjugate of solution
      case _skewAdjoint:
        error("storage_not_implemented", "SymSkylineStorage::upperSolver", "SkewAdjoint ");
        break;

      // It suffices to take opposite conjugate of right hand side
      // Use _symmetric case solver
      // Then change take opposite conjugate of solution
      default:
        theMessageData << "SymSkylineStorage::upperSolver " << "Unknown ";
        error("storage_not_implemented", "SymSkylineStorage::upperSolver", "Unknown");
        break;
    }
  trace_p->pop();
}

/*!
   Add two matrices
   \param m1 vector values_ of first matrix
   \param m2 vector values_ of second matrix
   \param r vector values_ of result matrix
 */
template<typename M1, typename M2, typename R>
void SymSkylineStorage::addMatrixMatrix(const std::vector<M1>& m1, const std::vector<M2>& m2, std::vector<R>& r) const
{
  trace_p->push("SkylineStorage::addMatrixMatrix");
  typename std::vector<M1>::const_iterator itm1 = m1.begin();
  typename std::vector<M2>::const_iterator itm2 = m2.begin();
  typename std::vector<R>::iterator itrb = r.begin(), itre = r.end();
  sumMatrixMatrix(itm1, itm2, itrb, itre);
  trace_p->pop();
}

/*!
   Extract and convert matrix storage to UMFPack format (Matlab sparse matrix)
   \param m1 vector values_ current matrix
   \param colPtUmf vector column Pointer of UMFPack format
   \param rowIdxUmf vector row Index of UMFPack format
   \param resultUmf vector values of UMFPack format
   \param sym type of symmetry
 */
template<typename M1, typename Idx>
void SymSkylineStorage::toUmfPack(const std::vector<M1>& m1, std::vector<Idx>& colPtUmf, std::vector<Idx>& rowIdxUmf, std::vector<M1>& resultUmf, const SymType sym) const
{
  // Reserve memory for resultUmf and set its size to 0
  resultUmf.reserve(m1.size());
  resultUmf.clear();

  // Reserve memory for rowIdxUmf and set its size to 0
  rowIdxUmf.reserve(m1.size());
  rowIdxUmf.clear();

  // Resize column pointer as dualCs's column pointer size
  colPtUmf.clear();
  colPtUmf.resize(rowPointer_.size());

  // Because colPointer always begins with 0, so need to to count from the second position
  typename std::vector<Idx>::iterator itColPtUmf = colPtUmf.begin()+1;
  colPtUmf[0] = Idx(0);

  std::vector<number_t>::const_iterator itfColPt = rowPointer_.begin();
  std::vector<number_t>::const_iterator itsColPt = rowPointer_.begin()+1;

  std::vector<number_t>::const_iterator itbRowPt = rowPointer_.begin(), itfRowPt, itsRowPt;
  std::vector<number_t>::const_iterator iteRowPt = rowPointer_.end();

  typename std::vector<M1>::const_iterator itmDiag = m1.begin()+1;
  typename std::vector<M1>::const_iterator itmLow  = itmDiag + diagonalSize();
  typename std::vector<M1>::const_iterator itmUp   = itmLow + lowerPartSize();
  if(_noSymmetry != sym) itmUp = itmLow;
  typename std::vector<M1>::const_iterator itval;

  Idx nvCol = Idx(0); // Number of non-zero in each "working" column
  number_t diagIdx = 0; // Index of diagonal elements
  int colIdx  = 0; // Index of current column

  for(; itsColPt != rowPointer_.end(); ++itsColPt,++itfColPt,++itColPtUmf)
    {
      nvCol = *itsColPt - *itfColPt; // Number of value appearing in current column of upper part

      Idx nzCol = Idx(0); // Number of non-zero in each "working" column

      // Always process upper part first
      while(nvCol>0)
        {
          // Update non-zero values
          if(M1(0) != (*itmUp))
            {
              switch(sym)
                {
                  case _skewSymmetric:
                    resultUmf.push_back(-(*itmUp));
                    break;
                  case _selfAdjoint:
                    resultUmf.push_back(conj(*itmUp));
                    break;
                  case _skewAdjoint:
                    resultUmf.push_back(-conj(*itmUp));
                    break;
                  default:
                    resultUmf.push_back(*itmUp);
                    break;
                }
              rowIdxUmf.push_back(colIdx-nvCol);
              ++nzCol;
            }
          ++itmUp;
          --nvCol;
        }


      // Process diagonal part
      if(diagIdx < diagonalSize())
        {
          if(M1(0) != *itmDiag)
            {
              // Update values
              resultUmf.push_back(*itmDiag);

              // Update colPtUmf with number of non-zero value
              ++nzCol;

              // Update rowIdxUmf
              rowIdxUmf.push_back(diagIdx);
            }
          ++itmDiag;
          ++diagIdx;
        }

      // Process lower part
      int rowIdx = 0;
      itfRowPt = itbRowPt;
      itsRowPt = itfRowPt+1;
      while(itsRowPt != iteRowPt)
        {
          int nzRow = *itsRowPt - *itfRowPt; // Number of "non-zero" values in a row
          if((int(0) < nzRow) && (colIdx < rowIdx))    // Only consider the row contains values in colIdx column)
            {
              int firstNzPos = rowIdx - nzRow; // Position (in column) of the first non zero value in a row
              if(firstNzPos <= colIdx)
                {
                  itval = itmLow + (*itfRowPt);
                  while(firstNzPos != colIdx) {++itval; ++firstNzPos;}
                  if(M1(0) != (*itval))
                    {
                      resultUmf.push_back(*itval); // Update values
                      rowIdxUmf.push_back(rowIdx); // Update row index


                      // Update colPtUmf with number of non-zero value
                      ++nzCol;
                    }
                }
            }
          ++itfRowPt; ++itsRowPt; ++rowIdx;
        }

      // Update colPtUmf with number of non-zero value
      *itColPtUmf += *(itColPtUmf-1) + nzCol;

      // Move to next column
      ++colIdx;
    }
}

#ifdef XLIFEPP_WITH_OMP
  /*!
  --------------------------------------------------------------------------------
    Template L.U factorization (parallel version)
      Factorization of matrix M stored as matrix F = L U where
      - L is a lower triangular matrix with unit diagonal and is stored as
      the strict lower triangular part of F
      - U is a upper triangular matrix with diagonal stored as diagonal part of F
      and strict upper triangular part stored as the strict upper triangular part of F:

              \sum_{ max(C1(i),L1(j)) <= k <= j } L_{ik} U{kj} = M_{ij}

      where C1(i) is the column index of first non zero entry on row i
      -     L1(j) and the row index of first non zero entry on column j

      CAUTION Matrix M may have a symmetry property, but
              Matrix F ***MUST HAVE NO*** symmetry property since L and U are computed
              with a different set of rules (THIS IS NOT CHECKED HERE)
  --------------------------------------------------------------------------------
  */
  template<typename M>
  void SymSkylineStorage::luParallel(std::vector<M>& m, std::vector<M>& f, const SymType sym) const
  {
    trace_p->push("SymSkylineStorage::lu");

    // iterators on diagonal (md), strict lower (ml) and strict upper (mu) triangular part of M
    typename std::vector<M>::iterator it_md(m.begin() + 1), it_ml(it_md + nbRows_), it_mu(it_ml);
    typename std::vector<M>::iterator it_fd(f.begin() + 1), it_fl(it_fd + nbRows_); //it_lnx(it_lb), it_l;
    typename std::vector<M>::iterator it_fu(it_fl);
    if(sym == _noSymmetry) { it_mu += *(rowPointer_.end() - 1); it_fu += *(rowPointer_.end() - 1);  }

    // iterators on row adresses starting with 2nd row (first row is row 0)
    std::vector<number_t>::const_iterator it_rownx(rowPointer_.begin()+1), it_row(it_rownx);

    // first diagonal entry D_{11} = U_{11} = M_{11}
    if(abs(*it_md) < theZeroThreshold) { isSingular("L.U", 0); }

    number_t numThread = 1;
    #pragma omp parallel for lastprivate(numThread)
    for(number_t i = 0; i < 1; i++)
      {
        numThread = omp_get_num_threads();
      }

    const number_t BLOCKFACTOR = 0.05*std::min(nbOfRows(),nbOfColumns());
    number_t numBlock = numThread * BLOCKFACTOR;
    if(numBlock==0) numBlock=1;
    std::vector<number_t> blockSizeRow(numBlock), blockSizeCol(numBlock);
    number_t rowSize = std::floor(nbRows_/numBlock);
    number_t colSize = std::floor(nbCols_/numBlock);
    for(number_t i = 0; i < (numBlock-1); ++i)
      {
        blockSizeRow[i] = rowSize;
        blockSizeCol[i] = colSize;
      }
    blockSizeRow[numBlock-1] = nbRows_ - (numBlock-1)*rowSize;
    blockSizeCol[numBlock-1] = nbCols_ - (numBlock-1)*colSize;

    number_t k = 0, jj = 0, ii = 0;
    #pragma omp parallel private(k)
    for(k=0; k < numBlock; ++k)
      {
        #pragma omp single
        diagBlockSolverParallel(k*rowSize, blockSizeRow[k],it_rownx,
                                k*colSize, blockSizeCol[k], it_rownx,
                                it_fd, it_fl, it_fu,
                                it_md, it_ml, it_mu);
        #pragma omp for nowait
        for(jj = k+1; jj < numBlock; ++jj)
          {
            #pragma omp task untied firstprivate(k, jj) \
            shared(rowSize, blockSizeRow, colSize, blockSizeCol, it_rownx, it_fl, it_fu, it_mu)
            upperBlockSolverParallel(k*rowSize, blockSizeRow[k],it_rownx,
                                    jj*colSize, blockSizeCol[jj], it_rownx,
                                    it_fl, it_fu, it_mu);
          }
        #pragma omp for
        for(ii = k+1; ii < numBlock; ++ii)
          {
            #pragma omp task untied firstprivate(k, ii) \
            shared(rowSize, blockSizeRow, colSize, blockSizeCol, it_rownx, it_fd, it_fl, it_fu, it_ml)
            lowerBlockSolverParallel(ii*rowSize, blockSizeRow[ii], it_rownx,
                                    k*colSize, blockSizeCol[k], it_rownx,
                                    it_fd, it_fl, it_fu, it_ml);
          }
      }
    trace_p->pop();
  }


  /*!
  --------------------------------------------------------------------------------
    Template L.D.Lt factorization (parallel version)
      Factorization of matrix M as matrix F = L D Lt
      where L is a lower triangular matrix with unit diagonal
              and is stored as the strict lower triangular part of F
        and D is a diagonal matrix stored as diagonal of matrix F:

      \f$\sum_{ C1(i) <= k <= j } L_{ik} D_{kk} L_{jk} = M_{ij} for ( j <= i )\f$

      where C1(i) is the column index of first non zero entry on row i
  --------------------------------------------------------------------------------
  */
  template<typename M>
  void SymSkylineStorage::ldltParallel(std::vector<M>& m, std::vector<M>& f, const SymType sym) const
  {
    trace_p->push("SymSkylineStorage::ldlt");

    // iterators on diagonal (md), strict lower (ml) and strict upper (mu) triangular part of M
    typename std::vector<M>::iterator it_md(m.begin() + 1), it_ml(it_md + nbRows_), it_mu(it_ml);
    typename std::vector<M>::iterator it_fd(f.begin() + 1), it_fl(it_fd + nbRows_);
    typename std::vector<M>::iterator it_fu(it_fl);

    // iterators on row adresses starting with 2nd row (first row is row 0)
    std::vector<number_t>::const_iterator it_rownx(rowPointer_.begin()+1), it_row(it_rownx);

    // first diagonal entry D_{11} = U_{11} = M_{11}
    if(abs(*it_md) < theZeroThreshold) { isSingular("L.U", 0); }

  //  number_t numThread = numberOfThreads();
    const number_t BLOCKFACTOR = 0.05*std::min(nbOfRows(),nbOfColumns());
    number_t numBlock = BLOCKFACTOR ; //numThread * BLOCKFACTOR;
    if(numBlock==0) numBlock=1;
    std::vector<number_t> blockSizeRow(numBlock), blockSizeCol(numBlock);
    number_t rowSize = std::floor(real_t(nbRows_)/numBlock);
    number_t colSize = std::floor(real_t(nbCols_)/numBlock);
    for(number_t i = 0; i < (numBlock-1); ++i)
      {
        blockSizeRow[i] = rowSize;
        blockSizeCol[i] = colSize;
      }
    blockSizeRow[numBlock-1] = nbRows_ - (numBlock-1)*rowSize;
    blockSizeCol[numBlock-1] = nbCols_ - (numBlock-1)*colSize;

    number_t k = 0, ii = 0;
  //  #pragma omp parallel private(k)
  //  for(k=0; k < numBlock; ++k)
  //    {
  //      #pragma omp single
  //      diagBlockSymSolverParallel(k*rowSize, blockSizeRow[k],it_rownx,
  //                                 k*colSize, blockSizeCol[k], it_rownx,
  //                                 it_fd, it_fl, it_fu,
  //                                 it_md, it_ml, it_mu);
  //      #pragma omp for
  //      for(ii = k+1; ii < numBlock; ++ii)
  //        {
  //          #pragma omp task untied firstprivate(k, ii) \
  //          shared(rowSize, blockSizeRow, colSize, blockSizeCol, it_rownx, it_fd, it_fl, it_fu, it_ml)
  //          lowerBlockSymSolverParallel(ii*rowSize, blockSizeRow[ii], it_rownx,
  //                                      k*colSize, blockSizeCol[k], it_rownx,
  //                                      it_fd, it_fl, it_fu, it_ml);
  //        }
  //    }

    for(k=0; k < numBlock; ++k)
      {
        diagBlockSymSolverParallel(k*rowSize, blockSizeRow[k],it_rownx,
                                  k*colSize, blockSizeCol[k], it_rownx,
                                  it_fd, it_fl, it_fu,
                                  it_md, it_ml, it_mu);
        #pragma omp parallel for firstprivate(k) \
        shared(rowSize, blockSizeRow, colSize, blockSizeCol, it_rownx, it_fd, it_fl, it_fu, it_ml) \
        schedule(static)
        for(ii = k+1; ii < numBlock; ++ii)
          {
            lowerBlockSymSolverParallel(ii*rowSize, blockSizeRow[ii], it_rownx,
                                        k*colSize, blockSizeCol[k], it_rownx,
                                        it_fd, it_fl, it_fu, it_ml);
          }
      }
    trace_p->pop();
  }


  /*!
  --------------------------------------------------------------------------------
    Template L.D.L* factorization (parallel version)
      Factorization of matrix M as matrix F = L D L*
      where L is a lower triangular matrix with unit diagonal
      and is stored as the strict lower triangular part of F
      and D is a diagonal matrix stored as diagonal of matrix F:

      \f$\sum_{ C1(i) <= k <= j } L_{ik} D_{kk} conj(L_{jk}) = M_{ij} for ( j <= i )\f$

      where C1(i) is the column index of first non zero entry on row i
  --------------------------------------------------------------------------------
  */
  template<typename M>
  void SymSkylineStorage::ldlstarParallel(std::vector<M>& m, std::vector<M>& f) const
  {
    trace_p->push("SymSkylineStorage::ldlstar");

    // iterators on diagonal (md), strict lower (ml) and strict upper (mu) triangular part of M
    typename std::vector<M>::iterator it_md(m.begin() + 1), it_ml(it_md + nbRows_), it_mu(it_ml);
    typename std::vector<M>::iterator it_fd(f.begin() + 1), it_fl(it_fd + nbRows_);
    typename std::vector<M>::iterator it_fu(it_fl);

    // iterators on row adresses starting with 2nd row (first row is row 0)
    std::vector<number_t>::const_iterator it_rownx(rowPointer_.begin()+1), it_row(it_rownx);

    // first diagonal entry D_{11} = U_{11} = M_{11}
    if(abs(*it_md) < theZeroThreshold) { isSingular("L.D.L*", 0); }

    number_t numThread = 1;
    #pragma omp parallel for lastprivate(numThread)
    for(number_t i = 0; i < 1; ++i)
      {
        numThread = omp_get_num_threads();
      }

    const number_t BLOCKFACTOR = 0.05*std::min(nbOfRows(),nbOfColumns());
    number_t numBlock = BLOCKFACTOR ; //numThread * BLOCKFACTOR;
    if(numBlock==0)  numBlock=1;
    std::vector<number_t> blockSizeRow(numBlock), blockSizeCol(numBlock);
    number_t rowSize = std::floor(real_t(nbRows_)/numBlock);
    number_t colSize = std::floor(real_t(nbCols_)/numBlock);
    for(number_t i = 0; i < (numBlock-1); ++i)
      {
        blockSizeRow[i] = rowSize;
        blockSizeCol[i] = colSize;
      }
    blockSizeRow[numBlock-1] = nbRows_ - (numBlock-1)*rowSize;
    blockSizeCol[numBlock-1] = nbCols_ - (numBlock-1)*colSize;

    number_t k = 0, jj = 0, ii = 0;
    #pragma omp parallel private(k)
    for(k=0; k < numBlock; ++k)
      {
        #pragma omp single
        diagBlockSymConjSolverParallel(k*rowSize, blockSizeRow[k],it_rownx,
                                      k*colSize, blockSizeCol[k], it_rownx,
                                      it_fd, it_fl, it_fu,
                                      it_md, it_ml, it_mu);
        #pragma omp for
        for(ii = k+1; ii < numBlock; ++ii)
          {
            #pragma omp task untied firstprivate(k, ii) \
            shared(rowSize, blockSizeRow, colSize, blockSizeCol, it_rownx, it_fd, it_fl, it_fu, it_ml)
            lowerBlockSymConjSolverParallel(ii*rowSize, blockSizeRow[ii], it_rownx,
                                            k*colSize, blockSizeCol[k], it_rownx,
                                            it_fd, it_fl, it_fu, it_ml);
          }
      }

    trace_p->pop();
  }
#endif // end of XLIFEPP_WITH_OMP

} // end of namespace xlifepp

