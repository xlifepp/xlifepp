/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SymSkylineStorage.hpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 juin 2011
  \date 6 Juin 2014

  \brief Definition of the xlifepp::SymSkylineStorage class

  xlifepp::SymSkylineStorage class is the child class of xlifepp::SkylineStorage class dealing with Symmetric Skyline Storage for square matrix
  The diagonal is always first stored (even zero coefficients), then the lower triangular part
  the matrix may not have symmetry, in that case the upper part is stored
  NOTE: the first entry (say (1,1)) is stored at address 1 in values vector (not at address 0!)

  - rowPointer_: vector of positions of beginning of rows

  Example: The following 5 x 5 non symmetric matrix with symmetric storage
  -------
  0  d . x . x
  1  . d x . 0
  2  x x d x x    rowPointer_  = [ 0 0 0 2 3 7 ] (without diagonal coeffs)
  3  . . x d x       row           0 1 2 3 4
  4  x 0 x x d

  Note: for an empty row, rowPointer_ is equal to the previous one: length of row i = rowPointer_[i+1]-rowPointer_[i]
         the last value of rowPointer_ gives the number of stored values of the strict lower triangular part
         be careful: rowPointer_ addresses in strict lower triangular part, to get the right address in values vector,
                      nbRows_ has to be added to the rowPointer_
*/

#ifndef SYM_SKYLINE_STORAGE_HPP
#define SYM_SKYLINE_STORAGE_HPP

#include "config.h"
#include "SkylineStorage.hpp"

namespace xlifepp
{

/*!
   \class SymSkylineStorage
   child class dealing with skyline storage of matrix with symmetry
 */
class SymSkylineStorage : public SkylineStorage
{
  friend class SkylineStorage;

  protected:
    std::vector<number_t> rowPointer_; //!< vector of positions of beginning of rows

  public:
    // constructor, destructor
    SymSkylineStorage(number_t n = 0, string_t id="SymmSkylineStorage"); //!< default constructor
    SymSkylineStorage(const std::vector<number_t>&, string_t);           //!< explicit constructor
    ~SymSkylineStorage() {}                                         //!< destructor
    //! constructor by a pair of global numerotation vectors
    SymSkylineStorage(number_t, const std::vector<std::vector<number_t> >&, const std::vector< std::vector<number_t> >&, string_t id="SymmSkylineStorage");
    //! constructor by the list of col index by rows
    template<class L>
    SymSkylineStorage(number_t, const std::vector<L>&, string_t id="SymmSkylineStorage");
    SymSkylineStorage* clone() const                                 //! create a clone (virtual copy constructor, covariant)
    {return new SymSkylineStorage(*this);}
    SymSkylineStorage* toScalar(dimen_t, dimen_t);                       //!< create a new scalar SymSkyline storage from current SymSkyline storage and submatrix sizes
    virtual void clear()                                             //! clear storage vectors
    {rowPointer_.clear();}

    // utilities
    number_t size() const {return nbRows_ + rowPointer_[nbRows_];}  //!< number of non zero elements stored
    number_t lowerPartSize() const {return rowPointer_[nbRows_];}   //!< returns number of entries of lower triangular part
    number_t upperPartSize() const {return rowPointer_[nbRows_];}   //!< returns number of entries of upper triangular part
    const std::vector<number_t>& rowPointer() const                 //! returns rowPointer_ (const)
    {return rowPointer_;}
    const std::vector<number_t>& colPointer() const                 //! colPointer of skyline storage (return rowPointer)
    {return rowPointer_;}
    bool sameStorage(const MatrixStorage&) const;                   //!< check if two storages have the same structures

    // access operator
    number_t pos(number_t i, number_t j, SymType s = _noSymmetry) const; //!< returns adress of entry (i,j)
    void positions(const std::vector<number_t>&, const std::vector<number_t>&,
                    std::vector<number_t>&, bool errorOn = true,
                    SymType = _noSymmetry) const;                   //!< access to submatrix positions
    std::vector<std::pair<number_t, number_t> > getCol(SymType s, number_t c, number_t r1=1, number_t r2=0) const; //!< get (row indices, adress) of col c in set [r1,r2]
    std::vector<std::pair<number_t, number_t> > getRow(SymType s, number_t r, number_t c1=1, number_t c2=0) const; //!< get (col indices, adress) of row r in set [c1,c2]
    void addSubMatrixIndices(const std::vector<number_t>&,const std::vector<number_t>&); //!< add dense submatrix indices to storage

    /* template specializations
        This rather COMPLICATED implementation of multMatrixVector is made
        to simulate "template virtual functions" in abstract base class MatrixStorage
        and thus to avoid "switch...case:" programming and "downcasting" in Matrix*Vector
        overloaded operator* of class LargeMatrix.
        Note that the call to template functions above is forced with the "<>" template
        descriptor in the following specialized virtual functions (this is not recursion) */

    /*------------------------------------------------------------------------------------------------------------------
                                        input and output stuff
      ------------------------------------------------------------------------------------------------------------------*/
    void print(std::ostream&) const;                                   //!< print storage pointers
    void printEntries(std::ostream&, const std::vector<real_t>&,
                      number_t vb, const SymType sym) const;             //!< print real scalar matrix
    void printEntries(std::ostream&, const std::vector<complex_t>&,
                      number_t vb, const SymType sym) const;             //!< print complex scalar matrix
    void printEntries(std::ostream&, const std::vector<Matrix<real_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of real matrices
    void printEntries(std::ostream&, const std::vector<Matrix<complex_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of complex matrices
    void printEntries(std::ostream&, const std::vector<Vector<real_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of real vectors (not available)
    void printEntries(std::ostream&, const std::vector<Vector<complex_t> >&,
                      number_t vb, const SymType sym) const;             //!< print matrix of complex vectors (not available)
    void printCooMatrix(std::ostream&, const std::vector<real_t>&,
                        SymType s = _noSymmetry) const;       //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector<complex_t>&,
                        SymType s = _noSymmetry) const;       //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector< Matrix<real_t> >&,
                        SymType s = _noSymmetry) const;       //!< output matrix of real scalars in coordinate form
    void printCooMatrix(std::ostream&, const std::vector< Matrix<complex_t> >&,
                        SymType s = _noSymmetry) const;       //!< output matrix of complex scalars in coordinate form

    void print(PrintStream& os) const
      {print(os.currentStream());}
    void printEntries(PrintStream& os, const std::vector<real_t>& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<complex_t>& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Matrix<real_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Matrix<complex_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Vector<real_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printEntries(PrintStream& os, const std::vector<Vector<complex_t> >& m, number_t vb, const SymType sym) const
      {printEntries(os.currentStream(), m, vb, sym);}
    void printCooMatrix(PrintStream& os, const std::vector<real_t>& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector<complex_t>& m,SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector< Matrix<real_t> >& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}
    void printCooMatrix(PrintStream& os, const std::vector< Matrix<complex_t> >& m, SymType s = _noSymmetry) const
      {printCooMatrix(os.currentStream(), m, s);}

    // load from file utilities
    //! load a dense real matrix from file
    void loadFromFileDense(std::istream& ifs, std::vector<real_t>& mat, SymType sym, bool realAsCmplx)
    {loadSkylineFromFileDense(ifs, mat, rowPointer_, rowPointer_, sym, realAsCmplx);}
    //! load a dense complex matrix from file
    void loadFromFileDense(std::istream& ifs, std::vector<complex_t>& mat, SymType sym, bool realAsCmplx)
    {loadSkylineFromFileDense(ifs, mat, rowPointer_, rowPointer_, sym, realAsCmplx);}
    //! load a coordinate real matrix from file
    void loadFromFileCoo(std::istream& ifs, std::vector<real_t>& mat, SymType sym, bool realAsCmplx)
    {loadSkylineFromFileCoo(ifs, mat, rowPointer_, rowPointer_, sym, realAsCmplx);}
    //! load a coordinate complex matrix from file
    void loadFromFileCoo(std::istream& ifs, std::vector<complex_t>& mat, SymType sym, bool realAsCmplx)
    {loadSkylineFromFileCoo(ifs, mat, rowPointer_, rowPointer_, sym, realAsCmplx);}

    /*------------------------------------------------------------------------------------------------------------------
                                          basic algebra stuff
      ------------------------------------------------------------------------------------------------------------------*/
    //! Set the Diagonal with a specific value
    template<typename T>
    void setDiagValueSymSkyline(std::vector<T>& v, const T m)
    {
        typename std::vector<T>::iterator it = v.begin() + 1;
        number_t diagSize = diagonalSize() + 1;
        for (number_t k = 1; k < diagSize; k++, it++) {(*it) = m;}
    }
    void setDiagValue(std::vector<real_t>& m, const real_t k) //! Set value of Diagonal (real)
    { setDiagValueSymSkyline<>(m, k);}
    void setDiagValue(std::vector<complex_t>& m, const complex_t k) //! Set value of Diagonal (complex)
    { setDiagValueSymSkyline<>(m, k);}

    // template Matrix + Matrix addition and specializations (see below)
    template<typename M1, typename M2, typename R>
    void addMatrixMatrix(const std::vector<M1>&, const std::vector<M2>&, std::vector<R>&) const; //!< templated row dense Matrix + Matrix

    //@{
    //! specializations of Matrix + Matrix
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv) const
    { addMatrixMatrix<>(m, v, rv);}
    void addMatrixMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    void addMatrixMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv) const
    { addMatrixMatrix<>(m, v, rv); }
    //@}

    //! addition of 2 skyline matrices
    template<typename M1, typename M2>
    void addTwoMatrixSymSkyline(std::vector<M1>&, SymType, const std::vector<number_t>&, const std::vector<number_t>&, const std::vector<M2>&, SymType);

    //@{
    //! specializations of addition of skyline matrices
    void addTwoMatrix(std::vector<real_t>& m1, SymType st1, const std::vector<number_t>& rowPtr2, const std::vector<number_t>& colPtr2, const std::vector<real_t>& m2, SymType st2)
    { addTwoMatrixSymSkyline<>(m1, st1, rowPtr2, colPtr2, m2, st2); }
    void addTwoMatrix(std::vector<complex_t>& m1, SymType st1, const std::vector<number_t>& rowPtr2, const std::vector<number_t>& colPtr2, const std::vector<real_t>& m2, SymType st2)
    { addTwoMatrixSymSkyline<>(m1, st1, rowPtr2, colPtr2, m2, st2); }
    void addTwoMatrix(std::vector<complex_t>& m1, SymType st1, const std::vector<number_t>& rowPtr2, const std::vector<number_t>& colPtr2, const std::vector<complex_t>& m2, SymType st2)
    { addTwoMatrixSymSkyline<>(m1, st1, rowPtr2, colPtr2, m2, st2); }
    //@}


    /*------------------------------------------------------------------------------------------------------------------
              template Matrix x Vector & Vector x Matrix multiplications and specializations
      ------------------------------------------------------------------------------------------------------------------*/
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&, SymType sym) const; //!< templated sym_skyline Matrix x Vector
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>&, const std::vector<V>&, std::vector<R>&, SymType sym) const; //!< templated Vector x sym_skyline Matrix
    template<typename M, typename V, typename R>
    void multMatrixVector(const std::vector<M>& m, V* vp, R* rp, SymType sym) const;           //!< templated sym_skyline Matrix x Vector (pointer form)
    template<typename M, typename V, typename R>
    void multVectorMatrix(const std::vector<M>& m, V* vp, R* rp, SymType sym) const;           //!< templated Vector x sym_skyline Matrix (pointer form)

    //@{
    //! Matrix (Scalar) * Vector (Scalar)
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    void multMatrixVector(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multMatrixVector<>(m, v, rv, sym); }
    //@}

    //@{
    //! Vector (Scalar) * Matrix (Scalar)
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<real_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector< Matrix<real_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<real_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    void multVectorMatrix(const std::vector< Matrix<complex_t> >& m, const std::vector< Vector<complex_t> >& v, std::vector< Vector<complex_t> >& rv, SymType sym) const
    { multVectorMatrix<>(m, v, rv, sym); }
    //@}

    //@{
    //! Matrix * Vector (pointer form)
    void multMatrixVector(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp, sym); }
    void multMatrixVector(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp, sym); }
    void multMatrixVector(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp, sym); }
    void multMatrixVector(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multMatrixVector<>(m, vp, rp, sym); }
    //@}

    //@{
    //! Vector * Matrix (pointer form)
    void multVectorMatrix(const std::vector<real_t>& m, real_t* vp, real_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp, sym); }
    void multVectorMatrix(const std::vector<real_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp, sym); }
    void multVectorMatrix(const std::vector<complex_t>& m, real_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp, sym); }
    void multVectorMatrix(const std::vector<complex_t>& m, complex_t* vp, complex_t* rp, SymType sym) const
    { multVectorMatrix<>(m, vp, rp, sym); }
    //@}

    /*---------------------------------------------------------------------------------------------------------------------
                                       triangular part matrix * vector
     ------------------------------------------------------------------------------------------------------------------*/
    template<typename M, typename V, typename R>
    void diagonalMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! diag Matrix * Vector (Scalar)
    virtual void diagonalMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    virtual void diagonalMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {diagonalMatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void lowerMatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! lower Matrix (Scalar) * Vector (Scalar)
    virtual void lowerMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    virtual void lowerMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {lowerMatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void lowerD1MatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&) const;
    //@{
    //! lower Matrix diag 1 (Scalar) * Vector (Scalar)
    virtual void lowerD1MatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v, std::vector<real_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    virtual void lowerD1MatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v, std::vector<complex_t>& rv, SymType s) const
    {lowerD1MatrixVector<>(m,v,rv);}
    //@}
    template<typename M, typename V, typename R>
    void upperMatrixVector(const std::vector<M>&m, const std::vector<V>&, std::vector<R>&, SymType) const;
    //@{
    //! upper Matrix (Scalar) * Vector (Scalar)
    virtual void upperMatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv,s);}
    virtual void upperMatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv,s);}
    virtual void upperMatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv,s);}
    virtual void upperMatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperMatrixVector<>(m,v,rv,s);}
    //@}
    template<typename M, typename V, typename R>
    void upperD1MatrixVector(const std::vector<M>&, const std::vector<V>&, std::vector<R>&, SymType) const;
    //@{
    //! upper Matrix diag 1 (Scalar) * Vector (Scalar)
    virtual void upperD1MatrixVector(const std::vector<real_t>&m, const std::vector<real_t>&v, std::vector<real_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv,s);}
    virtual void upperD1MatrixVector(const std::vector<real_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv,s);}
    virtual void upperD1MatrixVector(const std::vector<complex_t>&m, const std::vector<real_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv,s);}
    virtual void upperD1MatrixVector(const std::vector<complex_t>&m, const std::vector<complex_t>&v, std::vector<complex_t>&rv, SymType s) const
    {upperD1MatrixVector<>(m,v,rv,s);}
    //@}

    //----------------------------------------------------------------------------------------------------------------
    //                                     direct solver stuff
    //----------------------------------------------------------------------------------------------------------------
    //! Matrix factorization with LDLt
    template<typename M>
    void ldlt(std::vector<M>& m, std::vector<M>& fa, const SymType sym = _symmetric) const;

    //! Matrix factorization with LDLt
    template<typename M>
    void ldltParallel(std::vector<M>& m, std::vector<M>& fa, const SymType sym = _symmetric) const;

    //@{
    //! specializations of template LDLT
    void ldlt(std::vector<real_t>& m, std::vector<real_t>& fa, const SymType sym = _symmetric) const
    {
      #ifdef XLIFEPP_WITH_OMP
        if(numberOfThreads()==1) ldlt<>(m, fa, sym);
        else ldltParallel<>(m, fa, sym);
      #else
        ldlt<>(m, fa, sym);
      #endif
    }
    void ldlt(std::vector<complex_t>& m, std::vector<complex_t>& fa, const SymType sym = _symmetric) const
    {
      #ifdef XLIFEPP_WITH_OMP
        if(numberOfThreads()==1)ldlt<>(m, fa, sym);
        else ldltParallel<>(m, fa, sym);
      #else
        ldlt<>(m, fa, sym);
      #endif
    }
    //@}

    //! Matrix factorization with LU
    template<typename M>
    void lu(std::vector<M>& m, std::vector<M>& fa, const SymType sym = _noSymmetry) const;

    //! Matrix factorization with LU
    template<typename M>
    void luParallel(std::vector<M>& m, std::vector<M>& fa, const SymType sym = _noSymmetry) const;

    //@{
    //! specializations of template LU
    void lu(std::vector<real_t>& m, std::vector<real_t>& fa, const SymType sym = _noSymmetry) const
    {
      #ifdef XLIFEPP_WITH_OMP
        if(numberOfThreads()==1)lu<>(m, fa, sym);
        else luParallel<>(m, fa, sym);
      #else
        lu<>(m, fa, sym);
      #endif
    }

    void lu(std::vector<complex_t>& m, std::vector<complex_t>& fa, const SymType sym = _noSymmetry) const
    {
      #ifdef XLIFEPP_WITH_OMP
        if(numberOfThreads()==1)lu<>(m, fa, sym);
        else luParallel<>(m, fa, sym);
      #else
        lu<>(m, fa, sym);
      #endif
    }
    //@}

    //! Matrix factorization with LDL*
    template<typename M>
    void ldlstar(std::vector<M>& m, std::vector<M>& fa) const;

    //! Matrix factorization with LDL*
    template<typename M>
    void ldlstarParallel(std::vector<M>& m, std::vector<M>& fa) const;

    //@{
    //! specializations of template LDL*
    void ldlstar(std::vector<complex_t>& m, std::vector<complex_t>& fa) const
    {
      #ifdef XLIFEPP_WITH_OMP
        if(numberOfThreads()==1)ldlstar<>(m, fa);
        else ldlstarParallel<>(m, fa);
      #else
        ldlstar<>(m, fa);
      #endif
    }
    //@}

    // Template diagonal and triangular part solvers (after factorization)
    //! Lower triangular with unit diagonal linear system solver: (I+L) x = b
    template<typename M, typename V, typename X>
    void lowerD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const;

    //@{
    //! Specializations of lower triangular part with unit diagonal linear solvers (I + L) x = v */
    void lowerD1Solver(const std::vector<real_t>& m, std::vector<real_t>& v, std::vector<real_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<real_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    void lowerD1Solver(const std::vector<complex_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { lowerD1Solver<>(m, v, x); }
    //@}

    //! Diagonal linear system solver: D x = b
    template<typename M, typename V, typename X>
    void diagonalSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x) const;

    //@{
    //! Specializations of diagonal linear solvers D x = v
    void diagonalSolver(const std::vector<real_t>& m, std::vector<real_t>& v, std::vector<real_t>& x) const
    { diagonalSolver<>(m, v, x); }
    void diagonalSolver(const std::vector<real_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { diagonalSolver<>(m, v, x); }
    void diagonalSolver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x) const
    { diagonalSolver<>(m, v, x); }
    void diagonalSolver(const std::vector<complex_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x) const
    { diagonalSolver<>(m, v, x); }
    //@}

    //! Upper triangular with unit diagonal linear system solver: (I+U) x = b
    template<typename M, typename V, typename X>
    void upperD1Solver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x, const SymType sym) const;

    //@{
    //! Specializations of upper triangular part with unit diagonal linear solvers (I + U) x = v
    void upperD1Solver(const std::vector<real_t>& m, std::vector<real_t>& v, std::vector<real_t>& x, const SymType sym = _noSymmetry) const
    { upperD1Solver<>(m, v, x, sym); }
    void upperD1Solver(const std::vector<real_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperD1Solver<>(m, v, x, sym); }
    void upperD1Solver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperD1Solver<>(m, v, x, sym); }
    void upperD1Solver(const std::vector<complex_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperD1Solver<>(m, v, x, sym); }
    //@}

    //! upper triangular linear system solver: (D+U) x = b
    template<typename M, typename V, typename X>
    void upperSolver(const std::vector<M>& m, std::vector<V>& v, std::vector<X>& x, const SymType sym) const;

    //@{
    //! specializations of upper triangular part linear solvers (D + U) x = v
    void upperSolver(const std::vector<real_t>& m, std::vector<real_t>& v, std::vector<real_t>& x, const SymType sym = _noSymmetry) const
    { upperSolver<>(m, v, x, sym); }
    void upperSolver(const std::vector<real_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperSolver<>(m, v, x, sym); }
    void upperSolver(const std::vector<complex_t>& m, std::vector<real_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperSolver<>(m, v, x, sym); }
    void upperSolver(const std::vector<complex_t>& m, std::vector<complex_t>& v, std::vector<complex_t>& x, const SymType sym = _noSymmetry) const
    { upperSolver<>(m, v, x, sym); }
    //@}

    /* ----------------------------------------------------------------------------------------------------------------------------
                                                 UMFpack stuff
    ----------------------------------------------------------------------------------------------------------------------------
    */
    //! conversion to umfpack format
    template<typename M, typename OrdinalType>
    void toUmfPack(const std::vector<M>& values, std::vector<OrdinalType>& colPointer, std::vector<OrdinalType>& rowIndex, std::vector<M>& mat, const SymType sym = _noSymmetry) const;

    //@{
    //! specializations of umfpack conversions
    void toUmfPack(const std::vector<real_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<real_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat,sym);}
    void toUmfPack(const std::vector<complex_t>& values, std::vector<int_t>& colPointer, std::vector<int_t>& rowIndex, std::vector<complex_t>& mat, const SymType sym) const
    { toUmfPack<>(values,colPointer,rowIndex,mat,sym);}
    //@}
}; //end of class definition

// constructor from the list of col index of non zero for each row
//   cols[k] of type L stores the indices of column of non zero element on row k (index starts at 1!)
//   only requirement on L are the definition of forward const iterator named::const_iterator
//                             and the size
//   works for L = vector<number_t>, list<number_t>, set<number_t>
template<class L>
SymSkylineStorage::SymSkylineStorage(number_t n, const std::vector<L>& cols, string_t id)
   : SkylineStorage(n, n, _sym, id)
{
   trace_p->push("SymSkylineStorage constructor");
   // construct set of rowAdress for strict lower part
   typename std::vector<L>::const_iterator itvn;
   typename L::const_iterator its;
   rowPointer_.resize(nbRows_ + 1);
   std::vector<number_t>::iterator itrp = rowPointer_.begin();
   number_t r = 2, l = 0;
   *itrp = 0; itrp++;
   for(itvn = cols.begin()+1 ; itvn != cols.end(); itvn++, r++, itrp++)
   {
      *itrp = *(itrp - 1) + l;
      l = 0;
      if(itvn->size()>0)
      {
         number_t s = *std::min_element(itvn->begin(), itvn->end());
         if(s<r) l = r-s;
      }
   }
   *itrp = *(itrp - 1) + l;
   trace_p->pop();
}

}
#endif // SYM_CS_STORAGE_HPP

