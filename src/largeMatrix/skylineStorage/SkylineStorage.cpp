/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SkylineStorage.cpp
  \authors D. Martin, E. Lunéville, N. Kielbasiewicz
  \since 21 jun 2011
  \date 21 feb 2013

  \brief Implementation of xlifepp::SkylineStorage class and functionnalities
*/

#include "SkylineStorage.hpp"
#include "DualSkylineStorage.hpp"
#include "SymSkylineStorage.hpp"

namespace xlifepp
{
/*=======================================================================================
  Constructors, destructor
  =======================================================================================*/
// constructor with access
SkylineStorage::SkylineStorage(AccessType ac, string_t id) : MatrixStorage(_skyline, ac, id) {}

// constructor of a square matrix n x n
SkylineStorage::SkylineStorage(number_t n, AccessType ac, string_t id) : MatrixStorage(_skyline, ac, n, n, id) {}

// constructor of a matrix m x n
SkylineStorage::SkylineStorage(number_t m, number_t n, AccessType ac, string_t id) : MatrixStorage(_skyline, ac, m, n, id) {}

// create skyline storage from current storage
MatrixStorage* SkylineStorage::toSkyline(AccessType at)
{
    if(accessType_==at) return static_cast<MatrixStorage*>(this);  //storage is already a skyline one
    if(nbRows_!=nbCols_) error("mat_nonsquare","SkylineStorage::toSkyline",nbRows_,nbCols_);
    if(accessType_==_sym && at==_dual)
    {
      return static_cast<MatrixStorage*>(new DualSkylineStorage(rowPointer(),rowPointer(), stringId));
    }
    if(accessType_==_dual && at==_sym)  //keep only lower part of matrix
    {
      return static_cast<MatrixStorage*>(new SymSkylineStorage(rowPointer(), stringId));
    }
    return nullptr;
}

//create a new dual skyline storage from sym skyline storage
MatrixStorage* SkylineStorage::toDual()
{
  if(accessType_!=_sym)
  {
    where("SkylineStorage::toDual");
    error("symmetric_only"); return nullptr;
  }
  SymSkylineStorage* scs=dynamic_cast<SymSkylineStorage*>(this);  //downcast
  if(scs==nullptr)
  {
    where("SkylineStorage::toDual");
    error("downcast_failure", "SymSkylineStorage"); return nullptr;
  }
  return new DualSkylineStorage(scs->rowPointer_,scs->rowPointer_);
}

/*--------------------------------------------------------------------------------
 add submatrix indices given by its row and column indices (>= 1)
 row/col algorithm,
--------------------------------------------------------------------------------*/
void SkylineStorage::addSkylineSubMatrixIndices(std::vector<number_t>& rowPointer, const std::vector<number_t>& rows,
                                         const std::vector<number_t>& cols)
{
  trace_p->push("SkylineStorage::addSubMatrixIndices");

  std::vector<number_t>::const_iterator itr,itc;
  std::vector<number_t> newRowPointer(rowPointer.size(),0);
  std::vector<number_t>::iterator itrp=newRowPointer.begin(), itp=rowPointer.begin();

  std::map<number_t,number_t> rowsMap; //to speed up algorithm
  number_t r=1;
  for(itr=rows.begin(); itr!=rows.end(); itr++,r++)  rowsMap[*itr]=r;
  number_t nbrows=rowPointer.size()-1;
  for(r=1; r<=nbrows; itrp++, itp++, r++)
    {
      //build colIndex/rowIndex of row/col r
      number_t k1=*itp, k2=*(itp+1);
      number_t cmin=r-k2+k1;
      if(rowsMap.find(r)!=rowsMap.end())      //add new cols if row to add
        {
          for(itc=cols.begin(); itc!=cols.end(); itc++)
            if( *itc < cmin ) cmin=*itc;
        }
      //update new storage
      *(itrp+1)=*itrp+r-cmin;
    }
  rowPointer=newRowPointer;
  trace_p->pop();
}

// try to split the pointer vector in numThread parts, output numThread may be less than input numThread
void SkylineStorage::extractThreadIndex(const std::vector<number_t>& pointer,
        number_t& numThread,
        std::vector<std::vector<number_t>::const_iterator>& itThreadLower,
        std::vector<std::vector<number_t>::const_iterator>& itThreadUpper) const
{
    std::vector<number_t>::const_iterator itpb = pointer.begin(), itpe = pointer.end()-1, itp = itpb;
    std::vector<number_t>::const_iterator itpLower, itpUpper;
    number_t nnz = *itpe;

    // Assign each thread an approximately equal number of non-zero
    number_t nnzEachThread = nnz/numThread;
    number_t nnzRangeIdx = nnzEachThread;
    itp = itpb;
    itThreadLower[0] = itpb;
    //for (number_t i = 0; i < numThread; i++) {
    number_t i=0;
    for (; i < numThread && itp!=itpe; i++) {
        itThreadLower[i] = itp;
        nnzRangeIdx = *itp + nnzEachThread;
        itpLower = std::lower_bound(itp, itpe, nnzRangeIdx);
        itpUpper = std::upper_bound(itpLower, itpe,nnzRangeIdx);
        itp = ((nnzRangeIdx - *(itpUpper -1))< (*itpUpper-nnzRangeIdx)) ? itpUpper-1 : itpUpper;
        itThreadUpper[i] = itp;
    }
    //itThreadUpper[numThread-1] = itpe;
    if(itp!=itpe) itThreadUpper[numThread-1] = itpe; //all vectors fulfilled
    else // shorter vectors
      {
       numThread=i;
       itThreadLower.resize(numThread);
       itThreadUpper.resize(numThread);
      }
}
} // end of namespace xlifepp

