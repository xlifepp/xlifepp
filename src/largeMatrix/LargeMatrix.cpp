/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LargeMatrix.cpp
  \author E. Lunéville, M.-H. Nguyen
  \since 28 jun 2012
  \date  20 Dec 2012

  \brief Implementation of xlifepp::LargeMatrix class and functionnalities
*/

#include "LargeMatrix.hpp"

namespace xlifepp
{
// specialized version of product matrix x vector
// mat<real_t> * vec<complex_t>
void multMatrixVector(const LargeMatrix<real_t>& mat, const std::vector<complex_t>& vec, std::vector<complex_t>& res)
{
  trace_p->push("multMatrixVector");
  if(vec.size() != mat.nbCols) { error("largematrix_mismatch_dim"); }
  if(res.size() < mat.nbRows) { res.resize(mat.nbRows, 0); }
  if(mat.factorization_==_noFactorization) mat.storage_p->multMatrixVector(mat.values_, vec, res, mat.sym);
  else multFactMatrixVector(mat, vec, res);
  trace_p->pop();
}

std::vector<complex_t> operator*(const LargeMatrix<real_t>& mat, const std::vector<complex_t>& vec)
{
  std::vector<complex_t> res(mat.nbRows, complex_t(0));
  multMatrixVector(mat, vec, res);
  return res;
}
// mat<complex_t> * vec<real_t>
void multMatrixVector(const LargeMatrix<complex_t>& mat, const std::vector<real_t>& vec, std::vector<complex_t>& res)
{
  trace_p->push("multMatrixVector");
  if(vec.size() != mat.nbCols) { error("largematrix_mismatch_dim"); }
  if(res.size() < mat.nbRows) { res.resize(mat.nbRows, 0); }
  if(mat.factorization_==_noFactorization) mat.storage_p->multMatrixVector(mat.values_, vec, res, mat.sym);
  else multFactMatrixVector(mat, vec, res);
  trace_p->pop();
}

std::vector<complex_t> operator*(const LargeMatrix<complex_t>& mat, const std::vector<real_t>& vec)
{
  std::vector<complex_t> res(mat.nbRows, complex_t(0));
  multMatrixVector(mat, vec, res);
  return res;
}

// mat<Matrix<real_t>> * vec<Vector<complex_t>>
void multMatrixVector(const LargeMatrix<Matrix<real_t> >& mat, const std::vector<Vector<complex_t> >& vec, std::vector<Vector<complex_t> >& res)
{
  trace_p->push("multMatrixVector");
  if(vec.size() != mat.nbCols) { error("largematrix_mismatch_dim"); }
  if(vec.begin()->size() != mat.values_.begin()->numberOfColumns()) { error("largematrix_mismatch_dim"); }
  if(res.size() < mat.nbRows) { res.resize(mat.nbRows, Vector<complex_t>(mat.nbRowsSub, complex_t(0.))); }
  if(mat.factorization_==_noFactorization) mat.storage_p->multMatrixVector(mat.values_, vec, res, mat.sym);
  else //factorized matrix
  {
      error("largematrix_factorized", mat.name);
  }
  trace_p->pop();
}

std::vector<Vector<complex_t> > operator*(const LargeMatrix<Matrix<real_t> >& mat, const std::vector<Vector<complex_t> >& vec)
{
  std::vector<Vector<complex_t> > res(mat.nbRows, Vector<complex_t>(mat.nbRowsSub, complex_t(0.)));
  multMatrixVector(mat, vec, res);
  return res;
}

// mat<Matrix<complex_t>> * vec<Vector<real_t>>
void multMatrixVector(const LargeMatrix<Matrix<complex_t> >& mat, const std::vector<Vector<real_t> >& vec, std::vector<Vector<complex_t> >& res)
{
  trace_p->push("multMatrixVector");
  if(vec.size() != mat.nbCols) { error("largematrix_mismatch_dim"); }
  if(vec.begin()->size() != mat.values_.begin()->numberOfColumns()) { error("largematrix_mismatch_dim"); }
  if(res.size() < mat.nbRows) { res.resize(mat.nbRows, Vector<complex_t>(mat.nbRowsSub, complex_t(0.))); }
  if(mat.factorization_==_noFactorization) mat.storage_p->multMatrixVector(mat.values_, vec, res, mat.sym);
  else //factorized matrix
  {
      error("largematrix_factorized", mat.name);
  }
  trace_p->pop();
}

std::vector<Vector<complex_t> > operator*(const LargeMatrix<Matrix<complex_t> >& mat, const std::vector<Vector<real_t> >& vec)
{
  std::vector<Vector<complex_t> > res(mat.nbRows, Vector<complex_t>(mat.nbRowsSub, complex_t(0.)));
  multMatrixVector(mat, vec, res);
  return res;
}

// vec<complex_t> * mat<real_t>
void multVectorMatrix(const LargeMatrix<real_t>& mat, const std::vector<complex_t>& vec, std::vector<complex_t>& res)
{
  multVectorMatrix(vec, mat, res);
}

// vec<complex_t> * mat<real_t>
void multVectorMatrix(const std::vector<complex_t>& vec, const LargeMatrix<real_t>& mat, std::vector<complex_t>& res)
{
  trace_p->push("multVectorMatrix");
  if(vec.size() != mat.nbRows) { error("largematrix_mismatch_dim"); }
  if(res.size() < mat.nbCols) { res.resize(mat.nbCols, 0); }
  if(mat.factorization_==_noFactorization)  mat.storage_p->multVectorMatrix(mat.values_, vec, res, mat.sym);
  else multVectorFactMatrix(mat, vec, res);
  trace_p->pop();
}

std::vector<complex_t> operator*(const std::vector<complex_t>& vec, const LargeMatrix<real_t>& mat)
{
  std::vector<complex_t> res(mat.nbCols, complex_t(0));
  multVectorMatrix(vec, mat, res);
  return res;
}

// mat<complex_t> * vec<real_t>
void multVectorMatrix(const LargeMatrix<complex_t>& mat, const std::vector<real_t>& vec, std::vector<complex_t>& res)
{
  multVectorMatrix(vec, mat, res);
}

// mat<complex_t> * vec<real_t>
void multVectorMatrix(const std::vector<real_t>& vec, const LargeMatrix<complex_t>& mat, std::vector<complex_t>& res)
{
  trace_p->push("multVectorMatrix");
  if(vec.size() != mat.nbRows) { error("largematrix_mismatch_dim"); }
  if(res.size() < mat.nbCols) { res.resize(mat.nbCols, 0); }
  if(mat.factorization_==_noFactorization) mat.storage_p->multVectorMatrix(mat.values_, vec, res, mat.sym);
  else multVectorFactMatrix(mat, vec, res);
  trace_p->pop();
}

std::vector<complex_t> operator*(const std::vector<real_t>& vec, const LargeMatrix<complex_t>& mat)
{
  std::vector<complex_t> res(mat.nbCols, complex_t(0));
  multVectorMatrix(vec, mat, res);
  return res;
}

// mat<Matrix<real_t>> * vec<Vector<complex_t>>
void multVectorMatrix(const LargeMatrix<Matrix<real_t> >& mat, const std::vector<Vector<complex_t> >& vec, std::vector<Vector<complex_t> >& res)
{
  multVectorMatrix(vec, mat, res);
}

// mat<Matrix<real_t>> * vec<Vector<complex_t>>
void multVectorMatrix(const std::vector<Vector<complex_t> >& vec, const LargeMatrix<Matrix<real_t> >& mat, std::vector<Vector<complex_t> >& res)
{
  trace_p->push("multVectorMatrix");
  if(vec.size() != mat.nbRows) { error("largematrix_mismatch_dim"); }
  if(vec.begin()->size() != mat.values_.begin()->numberOfRows()) { error("largematrix_mismatch_dim"); }
  if(res.size() < mat.nbCols) { res.resize(mat.nbCols, Vector<complex_t>(mat.nbColsSub, complex_t(0.))); }
  if(mat.factorization_==_noFactorization) mat.storage_p->multVectorMatrix(mat.values_, vec, res, mat.sym);
  else error("free_error","product of a factorized matrix and a vector is not available");
  trace_p->pop();
}

std::vector<Vector<complex_t> > operator*(const std::vector<Vector<complex_t> >& vec, const LargeMatrix<Matrix<real_t> >& mat)
{
  std::vector<Vector<complex_t> > res(mat.nbCols, Vector<complex_t>(mat.nbColsSub, complex_t(0.)));
  multVectorMatrix(vec, mat, res);
  return res;
}

// mat<Matrix<complex_t>> * vec<Vector<real_t>>
void multVectorMatrix(const LargeMatrix<Matrix<complex_t> >& mat, const std::vector<Vector<real_t> >& vec, std::vector<Vector<complex_t> >& res)
{
  multVectorMatrix(vec, mat, res);
}

// mat<Matrix<complex_t>> * vec<Vector<real_t>>
void multVectorMatrix(const std::vector<Vector<real_t> >& vec, const LargeMatrix<Matrix<complex_t> >& mat, std::vector<Vector<complex_t> >& res)
{
  trace_p->push("multVectorMatrix");
  if(vec.size() != mat.nbRows) { error("largematrix_mismatch_dim"); }
  if(vec.begin()->size() != mat.values_.begin()->numberOfRows()) { error("largematrix_mismatch_dim"); }
  if(res.size() < mat.nbCols) { res.resize(mat.nbCols, Vector<complex_t>(mat.nbColsSub, complex_t(0.))); }
  if(mat.factorization_==_noFactorization) mat.storage_p->multVectorMatrix(mat.values_, vec, res, mat.sym);
  else error("largematrix_factorized", mat.name);
  trace_p->pop();
}

std::vector<Vector<complex_t> > operator*(const std::vector<Vector<real_t> >& vec, const LargeMatrix<Matrix<complex_t> >& mat)
{
  std::vector<Vector<complex_t> > res(mat.nbCols, Vector<complex_t>(mat.nbColsSub, complex_t(0.)));
  multVectorMatrix(vec, mat, res);
  return res;
}

/*!
    Add two different type largeMatrix and store result into the third
       The two matrices must share the same storage.
       The result matrix will point to the same storage after the summation
    \param matA complex matrix
    \param matB real matrix
    \param matC complex matrix which share the same storage.
*/
void addMatrixMatrix(const LargeMatrix<complex_t>& matA, const LargeMatrix<real_t>& matB, LargeMatrix<complex_t>& matC)
{
  trace_p->push("addMatrixMatrix");
  if ((matA.nbRows != matB.nbRows) || (matA.nbCols != matB.nbCols) ||
      (matA.nbRows != matC.nbRows) || (matA.nbCols != matC.nbCols) )
  {
    error("largematrix_mismatch_dim");
  }
  else if ((matA.storage_p != matB.storage_p) ||
           (matA.storage_p->accessType() != matC.storage_p->accessType()) ||
           (matA.storage_p->storageType() != matC.storage_p->storageType()))
  {
    error("largematrix_mismatch_storage");
  }
  else if (matA.storage_p != matC.storage_p)
  {
    if (matA.values_.size() != matC.values_.size()) { matC.values_.resize(matA.values_.size()); }
    matA.storage_p->addMatrixMatrix(matA.values_, matB.values_, matC.values_);

    // Decrease number of object using matrix storage of matrix C
    matC.storage_p->objectMinus();
    matC.storage_p = matA.storage_p;

    // Increase number of object using matrix storage of matrix A (and B, of course)
    matA.storage_p->objectPlus();
  }
  else
  {
    if (matA.values_.size() != matC.values_.size()) { matC.values_.resize(matA.values_.size()); }
    matA.storage_p->addMatrixMatrix(matA.values_, matB.values_, matC.values_);
  }

  if ((matA.sym == matB.sym) || ((_noSymmetry != matB.sym) && ((_selfAdjoint == matA.sym) || (_skewAdjoint == matA.sym))))
  {
    matC.sym = matA.sym;
  }
  else
  {
    matC.sym = _noSymmetry;
  }

  trace_p->pop();
}

/*!
   Add two different type largeMatrix and store result into the third
   The two matrices must share the same storage.
   The result matrix will point to the same storage after the summation
   \param matA real matrix
   \param matB complex matrix
   \param matC complex matrix which share the same storage.
 */
void addMatrixMatrix(const LargeMatrix<real_t>& matA, const LargeMatrix<complex_t>& matB, LargeMatrix<complex_t>& matC)
{
  trace_p->push("addMatrixMatrix");
  if ((matA.nbRows != matB.nbRows) || (matA.nbCols != matB.nbCols) ||
      (matA.nbRows != matC.nbRows) || (matA.nbCols != matC.nbCols) )
  {
    error("largematrix_mismatch_dim");
  }
  else if ((matA.storage_p != matB.storage_p) ||
           (matA.storage_p->accessType() != matC.storage_p->accessType()) ||
           (matA.storage_p->storageType() != matC.storage_p->storageType()))
  {
    error("largematrix_mismatch_storage");
  }
  else if (matA.storage_p != matC.storage_p)
  {
    if (matA.values_.size() != matC.values_.size()) { matC.values_.resize(matA.values_.size()); }
    matA.storage_p->addMatrixMatrix(matA.values_, matB.values_, matC.values_);

    // Decrease number of object using matrix storage of matrix C
    matC.storage_p->objectMinus();
    matC.storage_p = matA.storage_p;

    // Increase number of object using matrix storage of matrix A (and B, of course)
    matA.storage_p->objectPlus();
  }
  else
  {
    if (matA.values_.size() != matC.values_.size()) { matC.values_.resize(matA.values_.size()); }
    matA.storage_p->addMatrixMatrix(matA.values_, matB.values_, matC.values_);
  }

  if ((matA.sym == matB.sym) || ((_noSymmetry != matA.sym) && ((_selfAdjoint == matB.sym) || (_skewAdjoint == matB.sym))))
  {
    matC.sym = matB.sym;
  }
  else
  {
    matC.sym = _noSymmetry;
  }

  trace_p->pop();
}

// Specialization operator+: real_t + complex_t
LargeMatrix<complex_t> operator+(const LargeMatrix<real_t>& matA, const LargeMatrix<complex_t>& matB)
{
  LargeMatrix<complex_t> matC;
  addMatrixMatrix(matA, matB, matC);
  return matC;
}

// Specialization operator-: complex_t - real_t
LargeMatrix<complex_t> operator+(const LargeMatrix<complex_t>& matA, const LargeMatrix<real_t>& matB)
{
  LargeMatrix<complex_t> matC;
  addMatrixMatrix(matA, matB, matC);
  return matC;
}

// Specialization operator+: real_t - complex_t
LargeMatrix<complex_t> operator-(const LargeMatrix<real_t>& matA, const LargeMatrix<complex_t>& matB)
{
  LargeMatrix<complex_t> matC(matA);
  return matC-=matB;
}

// Specialization operator+: complex_t - real_t
LargeMatrix<complex_t> operator-(const LargeMatrix<complex_t>& matA, const LargeMatrix<real_t>& matB)
{
  LargeMatrix<complex_t> matC(matB);
  matC-=matA;
  return matC*=complex_t(-1.);
}

/*!
    Multiple a complex Matrix with real scalar
       The result matrix will point to the same storage
    \param mat complex matrix
    \param v real scalar
    \return result matrix which share the same storage.
*/
LargeMatrix<complex_t> multMatrixScalar(const LargeMatrix<complex_t>& mat, const real_t v)
{
  trace_p->push("multMatrixScalar");
  LargeMatrix<complex_t> result(mat);
  std::vector<complex_t>::iterator itb = result.values_.begin(), it;
  for (it = itb; it != result.values_.end(); it++) { *it *= cmplx(v); }
  trace_p->pop();
  return result;
}

// Specialized operator*: complex_t matrix * real scalar
LargeMatrix<complex_t> operator*(const LargeMatrix<complex_t>& mat, const real_t v)
{
  return multMatrixScalar(mat, v);
}

// Specialized operator*:  real scalar * complex_t matrix
LargeMatrix<complex_t> operator*(const real_t v, const LargeMatrix<complex_t>& mat)
{
  return multMatrixScalar(mat, v);
}

/*!
    Multiple a real Matrix with complex scalar
       The result matrix will point to the same storage
    \param mat real matrix
    \param v complex scalar
    \return complex matrix which share the same storage.
*/
LargeMatrix<complex_t> multMatrixScalar(const LargeMatrix<real_t>& mat, const complex_t v)
{
  trace_p->push("multMatrixScalar");
  LargeMatrix<complex_t> result(mat.storage_p, complex_t(0.0), mat.sym);
  std::vector<complex_t>::iterator itbc = result.values_.begin(), itc;
  std::vector<real_t>::const_iterator itr = mat.values_.begin();
  for (itc = itbc; itc != result.values_.end(); itc++, itr++) { *itc = cmplx(*itr) * v; }
  trace_p->pop();
  return result;
}

// Specialized operator*: real matrix * complex scalar
LargeMatrix<complex_t> operator*(const LargeMatrix<real_t>& mat, const complex_t v)
{
  return multMatrixScalar(mat, v);
}

// Specialized operator*: complex scalar * real matrix
LargeMatrix<complex_t> operator*(const complex_t v, const LargeMatrix<real_t>& mat)
{
  return multMatrixScalar(mat, v);
}


// Specialized real/complex matrices in matrix * matrix
LargeMatrix<complex_t> operator*(const LargeMatrix<complex_t>& mA, const LargeMatrix<real_t>& mB)
{
    LargeMatrix<complex_t> mR;
    multMatrixMatrix(mA,mB,mR);
    return mR;
}
LargeMatrix<complex_t> operator*(const LargeMatrix<real_t>& mA, const LargeMatrix<complex_t>& mB)
{
    LargeMatrix<complex_t> mR;
    multMatrixMatrix(mA,mB,mR);
    return mR;
}
LargeMatrix<Matrix<complex_t> > operator*(const LargeMatrix<Matrix<complex_t> >& mA, const LargeMatrix<Matrix<real_t> >& mB)
{
    LargeMatrix<Matrix<complex_t> > mR;
    multMatrixMatrix(mA,mB,mR);
    return mR;
}
LargeMatrix<Matrix<complex_t> > operator*(const LargeMatrix<Matrix<real_t> >& mA, const LargeMatrix<Matrix<complex_t> >& mB)
{
    LargeMatrix<Matrix<complex_t> > mR;
    multMatrixMatrix(mA,mB,mR);
    return mR;
}

} // end of namespace xlifepp

