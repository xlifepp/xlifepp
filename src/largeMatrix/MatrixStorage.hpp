/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file MatrixStorage.hpp
  \authors D. Martin, E. Lunéville, NGUYEN Manh Ha, N. Kielbasiewicz
  \since 21 jun 2011
  \date 3 apr 2013

  \brief Definition of the xlifepp::MatrixStorage class

  xlifepp::MatrixStorage class is the mother class of all matrix storage methods

  \verbatim
                 |                        | RowDenseStorage
                 | DenseStorage --------> | ColDenseStorage
                 |                        | DualDenseStorage
                 |                        | SymDenseStorage
                 |
                 |
  MatrixStorage -------> | SkylineStorage ------> | DualSkylineStorage
                 |                        | SymSkylineStorage
                 |
                 |
                 |                        | RowCsStorage  (CSR)
                 | CsStorage -----------> | ColCsStorage  (CSC)
                 |                        | DualCsStorage (CSR/CSC)
                 |                        | SymCsStorage  (CSR)
  \endverbatim


  The different child storages handle their specific algorithms (algebraic operations, product by a vector, ...)
  note that xlifepp::DenseStorage and its childs have no member attribute.

  A storage may be identified by
    - its storage type (_noStorage,_dense, _cs, _skyline, _coo)
    - its access type  (_noAccess, _sym, _dual, _row, _col)
    - its build type   (_undefBuild, _feBuild, _dgBuild, diagBuild, _ecBuild, _globalBuild, _otherBuild)
    - its stringId encoding row/col space pointers or other pointers
    - its number of rows or columns
  the function findMatrixStorage(...) uses these characteristics to find an existing storage

  template Matrix x Vector multiplications (with non-commutative algebra)

  Information on multMatrixVector and multVectorMatrix;

  In all what follows Matrix A is decomposed as  A = L + D + U
  where   D=diagonal of A     (d_i = iterator on D entries)
       L=strict lower part (l_i = iterator on L entries)
       U=strict upper part (u_i = iterator on U entries)
  Moreover, if matrix A has a symmetry property we either have U = L^t, = -L^t, = L* or = -L*.
  thus u_i can be a iterator on L entries if a symmetry proprety is involved.

  - The notation A^T means transpose(A) in the usual sense: (A^T)_ij = (A_ji)^T
   and we will use the following notations for block matrices:
  - "^TB" means the block-transpose of the matrix, in other terms, there is no
   transposition inside the matrix blocks.
  - "^bt" means transposition inside blocks only, the matrix blocks keep their position.

  * multMatrixVector computes the product Matrix x Vector for vector unknown types.
   It decomposes the product into Diagonal, Upper and Lower parts:
       A * v = D * v + L * v + U * v   if A has no symmetry property,
       A * v = D * v + L * v + L^T * v if A is symmetric,
       A * v = D * v + L * v - L^T * v if A is skewsymmetric ...
   ... diagonalMatrixVector(d_i), lowerMatrixVector(l_i), upperMatrixVector(u_i)
   ...                                                    upperVectorMatrix(l_i).

  * multVectorMatrix computes the product Vector x Matrix for vector unknown types.
   It gives effectively a way to compute Transpose(Matrix) x Vector.
   It decomposes the product into Diagonal, Upper and Lower parts:
       v * A = v * D + v * L + v * U                           if A has no symmetry property,
       v * A = v * D + v * L + v * L^T = v * D + v * L + L * v if A is symmetric,
       v * A = v * D + v * L - v * L^T = v * D + v * L - L * v if A is skewsymmetric ...
   ... diagonalVectorMatrix(d_i), lowerVectorMatrix(l_i), upperVectorMatrix(u_i)
   ...                                                    lowerMatrixVector(l_i).

  * The tools involved realize the computation of the result defined in the following table
   but can also be used to get the equivalent result given on the right-most column.

   tool                      |  result   |  equivalent result
   --------------------------|-----------|-------------------
   diagonalMatrixVector(d_i) |  D * v    |  v * D^T
   diagonalVectorMatrix(d_i) |  v * D    |  D^T * v
   lowerMatrixVector(l_i)    |  L * v    |  v * L^T
   upperMatrixVector(u_i)    |  U * v    |  v * U^T
   lowerVectorMatrix(l_i)    |  v * L    |  L^T * v
   upperVectorMatrix(u_i)    |  v * U    |  U^T * v

   We have also the following results, but, note the transpose operator is
   not taken in the usual sense (as indicated above)

   tool                      |  result   |  equivalent result
   --------------------------|-----------|-------------------
   lowerMatrixVector(u_i)    |  U^TB * v |  v * U^bt
   upperMatrixVector(l_i)    |  L^TB * v |  v * L^bt
   lowerVectorMatrix(l_i)    |  v * L^TB |  L^bt * v
   upperVectorMatrix(u_i)    |  v * U^TB |  U^bt * v
*/

#ifndef MATRIX_STORAGE_HPP
#define MATRIX_STORAGE_HPP

#include "config.h"
#include "utils.h"

namespace xlifepp
{

/*!
  \class MatrixStorage
  abstract base class of all matrix storage methods
 */
class MatrixStorage
{
  protected:
    StorageType storageType_;       //!< storage type (_noStorage,_dense, _cs, _skyline, _coo)
    AccessType accessType_;         //!< access type (_noAccess, _sym, _dual, _row, _col)
    StorageBuildType buildType_;    //!< storage build type (_undefBuild, _feBuild, _dgBuild, diagBuild, _ecBuild, _globalBuild, _otherBuild)
    bool scalarFlag_;               //!< true means a storage coming from a to scalar conversion (default = false)
    number_t nbRows_;               //!< number of rows
    number_t nbCols_;               //!< number of columns
    number_t nbObjectsSharingThis_; //!< number of matrices sharing "this" storage

  public:
    string_t stringId;                                    //!< string identifier based on characteristic pointers (e.g row/col space pointers)
    static std::vector<MatrixStorage*> theMatrixStorages; //!< list of all matrix storages
    static void clearGlobalVector();                      //!< delete all MatrixStorage objects
    static void printAllStorages(std::ostream&);  //!< print the list of MatrixStorage objects in memory
    static void printAllStorages(PrintStream& os) {printAllStorages(os.currentStream());}

    // Constructors, Destructor
    MatrixStorage(string_t id="");           //!< default constructor
    virtual ~MatrixStorage();                //!< virtual destructor
    MatrixStorage(StorageType, AccessType at = _noAccess, string_t id="");      //!< constructor by storage and access types
    MatrixStorage(StorageType, AccessType, number_t, number_t, string_t id=""); //!< constructor by storage and access types, number of columns an rows
    MatrixStorage(const MatrixStorage&);     //!< copy constructor
    virtual MatrixStorage* clone() const=0;  //!< create a clone (like a virtual copy constructor)
    virtual void clear() {}                  //!< clear storage vectors
    MatrixStorage* extract(const std::vector<number_t>& rowIndex,
                           const std::vector<number_t>& colIndex, const string_t& id=""); //!< extract storage of submatrix

    // Public Inline Access functions
    string_t name() const;                    //! returns type of storage as a string
    StorageType storageType() const           //! access to type of storage
    { return storageType_; }
    AccessType accessType() const             //! access to type of access
    { return accessType_ ;}
    StorageBuildType buildType() const        //! access to storage build type
    { return buildType_ ;}
    StorageBuildType& buildType()             //! write access to storage build type
    { return buildType_ ;}
    bool scalarFlag() const                   //! access to scalarFlag
    { return scalarFlag_; }
    bool& scalarFlag()                        //! write access to scalarFlag
    { return scalarFlag_; }
    number_t nbOfRows() const                 //! returns number of rows
    { return nbRows_; }
    number_t nbOfColumns() const              //! returns number of columns
    { return nbCols_; }
    number_t& nbOfRows()                      //! returns number of rows
    { return nbRows_; }
    number_t& nbOfColumns()                   //! returns number of columns
    { return nbCols_; }
    virtual number_t size() const = 0;        //!< returns number of entries
    number_t diagonalSize() const             //! returns number of diagonal entries
    { return std::min(nbRows_, nbCols_); }
    virtual number_t lowerPartSize() const = 0; //!< returns number of entries of lower triangular part
    virtual number_t upperPartSize() const = 0; //!< returns number of entries of upper triangular part
    virtual bool isDiagonal() const             //! true if matrix is diagonal
    {return lowerPartSize()==0 && upperPartSize()==0;}
    number_t numberOfObjects() const            //! number of objects using this matrix Storage
    { return nbObjectsSharingThis_; }
    void objectPlus()                           //! increase number of objects using this matrix Storage
    { nbObjectsSharingThis_++; }
    void objectMinus()                          //! decrease number of objects using this matrix Storage
    {if(nbObjectsSharingThis_ > 0) nbObjectsSharingThis_--;}

    virtual bool sameStorage(const MatrixStorage&) const=0;  //!< check if two storages have the same structures

    //modifying storage tools by adding set of indices or other matrix storage
    virtual void addSubMatrixIndices(const std::vector<number_t>&,const std::vector<number_t>&)   //! add dense submatrix indices in storage
    {error("storage_not_implemented","addSubMatrixIndices",name());}
    virtual void addIndices(const std::vector<number_t>&, const std::vector<number_t>&) //! add indices (i,j) given by rows (i) and columns (j) (i,j >= 1)
    {error("storage_not_implemented","addIndices",name());}
    virtual void addRow(number_t, const std::set<number_t>&, MatrixPart)                //! add row-cols (r,c1), (r,c2), ... given by a cols set (r,c>=1)
    {error("storage_not_implemented","addRow",name());}
    virtual void addCol(number_t, const std::set<number_t>&, MatrixPart)                //! add row-cols (r,c1), (r,c2), ... given by a cols set (r,c>=1)
    {error("storage_not_implemented","addCol",name());}
    void add(const MatrixStorage&, const std::vector<number_t>& rowmap=std::vector<number_t>(),
             const std::vector<number_t>& colmap=std::vector<number_t>());            //!< add storage in current storage with row/col mapping
    virtual std::vector<std::vector<number_t> > scalarColIndices(dimen_t nbr=1, dimen_t nbc=1); //!< create scalar col indices for each row from current storage and submatrix sizes

    //skyline conversion tools
    virtual std::vector<number_t> skylineRowPointer() const  //! return skyline row pointer from current storage
    {
      error("storage_wrong_ptr", "skyline","row", words("storage type",storageType_));
      return std::vector<number_t>();
    }
    virtual std::vector<number_t> skylineColPointer() const  //! return skyline col pointer from current storage
    {
      error("storage_wrong_ptr", "skyline","col",words("storage type",storageType_));
      return std::vector<number_t>();
    }
    //! fill real values of current storage as a skyline storage
    virtual void fillSkylineValues(const std::vector<real_t>&,std::vector<real_t>&, SymType, MatrixStorage*) const
    {error("storage_not_implemented", "fillSkylineValues",name());}
    //! fill complex values of current storage as a skyline storage
    virtual void fillSkylineValues(const std::vector<complex_t>&,std::vector<complex_t>&, SymType, MatrixStorage*) const
    {error("storage_not_implemented", "fillSkylineValues",name());}
    //! fill values of current storage as a skyline storage
    virtual void fillSkylineValues(const std::vector<Matrix<real_t> >&,std::vector<Matrix<real_t> >&, SymType, MatrixStorage*) const
    {error("storage_not_implemented", "fillSkylineValues",name());}
    //! fill values of current storage as a skyline storage
    virtual void fillSkylineValues(const std::vector<Matrix<complex_t> >&,std::vector<Matrix<complex_t> >&, SymType, MatrixStorage*) const
    {error("storage_not_implemented", "fillSkylineValues",name());}

    //!< move matrix from from current storage to an other one, return false if not available
    virtual bool toStorage(const MatrixStorage& newsto, std::vector<real_t>& newval, const std::vector<real_t>& oldval) const
    {return false;}
    virtual bool toStorage(const MatrixStorage& newsto, std::vector<complex_t>& newval, const std::vector<complex_t>& oldval) const
    {return false;}
    virtual bool toStorage(const MatrixStorage& newsto, std::vector<Matrix<real_t> >& newval, const std::vector<Matrix<real_t> >& oldval) const
    {return false;}
    virtual bool toStorage(const MatrixStorage& newsto, std::vector<Matrix<complex_t> >& newval, const std::vector<Matrix<complex_t> >& oldval) const
    {return false;}

    //accesstype conversion tools
    virtual MatrixStorage* toColStorage()   //! return new col access storage from current storage
    {error("not_handled", "toColStorage()"); return nullptr;}
    virtual MatrixStorage* toRowStorage()   //! return new row access storage from current storage
    {error("not_handled", "toRowStorage()"); return nullptr;}

    //print utilities
    void printHeader(std::ostream&) const;   //!< print header of matrixstorage (name, size, ...)
    virtual void visual(std::ostream& os = thePrintStream[0]) const; //!< visualize storage on ostream
    virtual void print (std::ostream& os = thePrintStream[0]) const;  //!< visualize storage on ostream
    virtual void visual(PrintStream& os = thePrintStream) const //! visualize storage on ostream
      {visual(os.currentStream());}
    virtual void print (PrintStream& os = thePrintStream) const  //! visualize storage on ostream
      {visual(os.currentStream());}
    friend std::ostream& operator<<(std::ostream&, const MatrixStorage&);

    // error handlers
    void noFactorization(const string_t& = "") const; //!< error handler when no factorization
    void noSolver(const string_t& = "") const; //!< error handler when no solver

    //access operation
    virtual number_t pos(number_t i, number_t j, SymType sym = _noSymmetry) const //! returns adress of entry (i,j)
    {error("not_yet_implemented", "operator (i,j,sym) for storage " + name()); return 0;}

    virtual void positions(const std::vector<number_t>&, const std::vector<number_t>&,
                    std::vector<number_t>&, bool errorOn = true, SymType = _noSymmetry) const    //! access to submatrix positions
    {error("not_yet_implemented", "adresses() for storage " + name());}

    //! get (row indices, adress) of col c in set [r1,r2], see child for better algorithm
    virtual std::vector<std::pair<number_t, number_t> > getCol(SymType, number_t c, number_t r1=1, number_t r2=0) const;
    //! get (col indices, adress) of row r in set [c1,c2], see child for better algorithm
    virtual std::vector<std::pair<number_t, number_t> > getRow(SymType, number_t r, number_t c1=1, number_t c2=0) const;
    //! get row indices of col c in set [r1,r2], generic algorithm using position, see children for better algorithms
    virtual std::set<number_t> getRows(number_t c, number_t r1=1, number_t r2=0) const;
    //! get col indices of row r in set [c1,c2], generic algorithm using position, see children for better algorithms
    virtual std::set<number_t> getCols(number_t r, number_t c1=1, number_t c2=0) const;
    //! get diagonal adresses, generic algorithm using position, see children for better algorithms
    virtual std::vector<number_t> getDiag() const;
    //! get col indices of row r in set [c1,c2], faster generic algorithm using position, see children for better algorithms
    virtual void getColsV(std::vector<number_t>&, number_t&, number_t r, number_t c1=1, number_t c2=0) const;
    //! get row indices of col c in set [r1,r2], faster generic algorithm using position, see children for better algorithms
    virtual void getRowsV(std::vector<number_t>&, number_t&, number_t c, number_t r1=1, number_t r2=0) const;

    virtual MatrixStorage* toScalar(dimen_t, dimen_t) //! create a new matrixstorage for scalar storage from non scalar storage
    {error("not_yet_implemented", "toScalar(dimen_t, dimen_t) for storage " + name()); return nullptr;}
    virtual MatrixStorage* toDual()               //! create a new dual storage from sym storage
    {error("symmetric_only"); return nullptr;}

    //print functions and save functions
    //! output matrix of real scalars
    virtual void printEntries(std::ostream&, const std::vector<real_t>&, number_t vb = 0, const SymType sym = _noSymmetry) const
    {warning("storage_noprint", name());}
            void printEntries(PrintStream& os, const std::vector<real_t>& m, number_t vb = 0, const SymType sym = _noSymmetry) const
    {printEntries(os.currentStream(),m,vb,sym);}
    //! output matrix of complex scalars
    virtual void printEntries(std::ostream&, const std::vector<complex_t>&, number_t vb = 0, const SymType sym = _noSymmetry) const
    {warning("storage_noprint", name());}
            void printEntries(PrintStream& os, const std::vector<complex_t>& m, number_t vb = 0, const SymType sym = _noSymmetry) const
    {printEntries(os.currentStream(),m,vb,sym);}
    //! output matrix of real matrices
    virtual void printEntries(std::ostream&, const std::vector< Matrix<real_t> >&, number_t vb = 0, const SymType sym = _noSymmetry) const
    {warning("storage_noprint", name());}
            void printEntries(PrintStream& os, const std::vector< Matrix<real_t> >& m, number_t vb = 0, const SymType sym = _noSymmetry) const
    {printEntries(os.currentStream(),m,vb,sym);}
    //! output matrix of complex matrices
    virtual void printEntries(std::ostream&, const std::vector< Matrix<complex_t> >&, number_t vb = 0, const SymType sym = _noSymmetry) const
    {warning("storage_noprint", name());}
            void printEntries(PrintStream& os, const std::vector< Matrix<complex_t> >& m, number_t vb = 0, const SymType sym = _noSymmetry) const
    {printEntries(os.currentStream(),m,vb,sym);}

    template<typename T>
    void printDenseMatrix(std::ostream& os, const std::vector<T>& m, SymType s = _noSymmetry) const;  //!< output matrix in dense format
    template<typename T>
    void printDenseMatrix(PrintStream& os, const std::vector<T>& m, SymType s = _noSymmetry) const  //! output matrix in dense format
    {printDenseMatrix(os.currentStream(),m,s);}
    template<typename T>
    void printCooMatrix(std::ostream& os, const std::vector<T>& m, SymType s = _noSymmetry, real_t tol=theTolerance) const;  //!< output matrix in coordinate format
    template<typename T>
    void printCooMatrix(PrintStream& os, const std::vector<T>& m, SymType s = _noSymmetry, real_t tol=theTolerance) const  //! output matrix in coordinate format
    {printCooMatrix(os.currentStream(),m,s, tol);}

    /*---------------------------------------------------------------------------------------------------
      virtual type dependant functions
      as virtual template function are not allowed in C++, all specialisations have to be declared here
      it is a quite boring programming but other solutions are finally more intricate
      when child class does not implement functions, a generic function is called or an error/warning message is raised
    ---------------------------------------------------------------------------------------------------*/

    //removing tools
    //! delete rows r1,..., r2 (may be expansive for some storage)
    virtual void deleteRows(number_t r1, number_t r2, std::vector<real_t>& v, SymType s)
    {error("not_yet_implemented", "deleteRows for storage " + name());}
    //! delete rows r1,..., r2 (may be expansive for some storage)
    virtual void deleteRows(number_t r1, number_t r2, std::vector<complex_t>& v, SymType s)
    {error("not_yet_implemented", "deleteRows for storage " + name());}
    //! delete rows r1,..., r2 (may be expansive for some storage)
    virtual void deleteRows(number_t r1, number_t r2, std::vector<Matrix<real_t> >& v, SymType s)
    {error("not_yet_implemented", "deleteRows for storage " + name());}
    //! delete rows r1,..., r2 (may be expansive for some storage)
    virtual void deleteRows(number_t r1, number_t r2, std::vector<Matrix<complex_t> >& v, SymType s)
    {error("not_yet_implemented", "deleteRows for storage " + name());}

    //! delete cols c1,..., c2 (may be expansive for some storage)
    virtual void deleteCols(number_t c1, number_t c2, std::vector<real_t>& v, SymType s)
    {error("not_yet_implemented", "deleteCols for storage " + name());}
    //! delete cols c1,..., c2 (may be expansive for some storage)
    virtual void deleteCols(number_t c1, number_t c2, std::vector<complex_t>& v, SymType s)
    {error("not_yet_implemented", "deleteCols for storage " + name());}
    //! delete cols c1,..., c2 (may be expansive for some storage)
    virtual void deleteCols(number_t c1, number_t c2, std::vector<Matrix<real_t> >& v, SymType s)
    {error("not_yet_implemented", "deleteCols for storage " + name());}
    //! delete cols c1,..., c2 (may be expansive for some storage)
    virtual void deleteCols(number_t c1, number_t c2, std::vector<Matrix<complex_t> >& v, SymType s)
    {error("not_yet_implemented", "deleteCols for storage " + name());}

    // load from file functions
    //! load a real dense matrix from file
    virtual void loadFromFileDense(std::istream&, std::vector<real_t>&, SymType, bool)
    {error("not_yet_implemented", "loadFromFileDense for storage " + name());}
    //! load a complex dense matrix from file
    virtual void loadFromFileDense(std::istream&, std::vector<complex_t>&, SymType, bool)
    {error("not_yet_implemented", "loadFromFileDense for storage " + name());}
    //! load a dense matrix of real matrices from file
    virtual void loadFromFileDense(std::istream&, std::vector<Matrix<real_t> >&, SymType, bool)
    {error("not_yet_implemented", "loadFromFileDense for storage " + name());}
    //! load a dense matrix of complex matrices from file
    virtual void loadFromFileDense(std::istream&, std::vector<Matrix<complex_t> >&, SymType, bool)
    {error("not_yet_implemented", "loadFromFileDense for storage " + name());}
    //! load a real coordinate matrix
    virtual void loadFromFileCoo(std::istream&, std::vector<real_t>&, SymType, bool)
    {error("not_yet_implemented", "loadFromFileCoo for storage " + name());}
    //! load a complex coordinate matrix
    virtual void loadFromFileCoo(std::istream&, std::vector<complex_t>&, SymType, bool)
    {error("not_yet_implemented", "loadFromFileCoo for storage " + name());}
    //! load a coordinate matrix of real matrices
    virtual void loadFromFileCoo(std::istream&, std::vector<Matrix<real_t> >&, SymType, bool)
    {error("not_yet_implemented", "loadFromFileCoo for storage " + name());}
    //! load a coordinate matrix of complex matrices
    virtual void loadFromFileCoo(std::istream&, std::vector<Matrix<complex_t> >&, SymType, bool)
    {error("not_yet_implemented", "loadFromFileCoo for storage " + name());}

    //!transpose matrix
    virtual MatrixStorage* transpose(const std::vector<real_t>&, std::vector<real_t>&) const
    {error("not_yet_implemented", "transpose for storage " + name()); return nullptr;}
    virtual MatrixStorage* transpose(const std::vector<complex_t>&, std::vector<complex_t>&) const
    {error("not_yet_implemented", "transpose for storage " + name());return nullptr;}
    virtual MatrixStorage* transpose(const std::vector<Matrix<real_t> >&, std::vector<Matrix<real_t> >&) const
    {error("not_yet_implemented", "transpose for storage " + name()); return nullptr;}
    virtual MatrixStorage* transpose(const std::vector<Matrix<complex_t> >&, std::vector<Matrix<complex_t> >&) const
    {error("not_yet_implemented", "transpose for storage " + name()); return nullptr;}

    /* ----------------------------------------------------------------------------------------------------------------------------
                                    virtual matrix * vector and vector * matrix
       ----------------------------------------------------------------------------------------------------------------------------*/
    //@{
    //! Matrix (Scalar) * Vector (Scalar)
    virtual void multMatrixVector(const std::vector<real_t>&, const std::vector<real_t>&, std::vector<real_t>&, SymType) const
    {error("not_yet_implemented", "multMatrixVector for real matrix, storage " + name());}
    virtual void multMatrixVector(const std::vector<real_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "multMatrixVector for real matrix, storage " + name());}
    virtual void multMatrixVector(const std::vector<complex_t>&, const std::vector<real_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "multMatrixVector for complex matrix, storage " + name());}
    virtual void multMatrixVector(const std::vector<complex_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "multMatrixVector for complex matrix, storage " + name());}

    virtual void multMatrixVector(const std::vector<real_t>&, real_t*, real_t*, SymType) const
    {error("not_yet_implemented", "multMatrixVector for real matrix, storage " + name());}
    virtual void multMatrixVector(const std::vector<real_t>&, complex_t*, complex_t*, SymType) const
    {error("not_yet_implemented", "multMatrixVector for real matrix, storage " + name());}
    virtual void multMatrixVector(const std::vector<complex_t>&, real_t*, complex_t*, SymType) const
    {error("not_yet_implemented", "multMatrixVector for complex matrix, storage " + name());}
    virtual void multMatrixVector(const std::vector<complex_t>&, complex_t*, complex_t*, SymType) const
    {error("not_yet_implemented", "multMatrixVector for complex matrix, storage " + name());}
    //@}

    //@{
    //! Matrix (Matrix) * Vector (Vector)
    virtual void multMatrixVector(const std::vector< Matrix<real_t> >&, const std::vector< Vector<real_t> >&, std::vector< Vector<real_t> >&, SymType) const
    {error("not_yet_implemented", "multMatrixVector for matrix of real matrices, storage " + name());}
    virtual void multMatrixVector(const std::vector< Matrix<real_t> >&, const std::vector< Vector<complex_t> >&, std::vector< Vector<complex_t> >&, SymType) const
    {error("not_yet_implemented", "multMatrixVector for matrix of real matrices, storage " + name());}
    virtual void multMatrixVector(const std::vector< Matrix<complex_t> >&, const std::vector< Vector<real_t> >&, std::vector< Vector<complex_t> >&, SymType) const
    {error("not_yet_implemented", "multMatrixVector for matrix of complex matrices, storage " + name());}
    virtual void multMatrixVector(const std::vector< Matrix<complex_t> >&, const std::vector< Vector<complex_t> >&, std::vector< Vector<complex_t> >&, SymType) const
    {error("not_yet_implemented", "multMatrixVector for matrix of complex matrices, storage " + name());}
    //@}

    //@{
    //! Vector (Scalar) * Matrix (Scalar)
    virtual void multVectorMatrix(const std::vector<real_t>&, const std::vector<real_t>&, std::vector<real_t>&, SymType) const
    {error("not_yet_implemented", "multVectorMatrix for real matrix, storage " + name());}
    virtual void multVectorMatrix(const std::vector<real_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "multVectorMatrix for real matrix, storage " + name());}
    virtual void multVectorMatrix(const std::vector<complex_t>&, const std::vector<real_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "multVectorMatrix for complex matrix, storage " + name());}
    virtual void multVectorMatrix(const std::vector<complex_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "multVectorMatrix for real matrix, storage " + name());}

    virtual void multVectorMatrix(const std::vector<real_t>&, real_t*, real_t*, SymType) const
    {error("not_yet_implemented", "multVectorMatrix for real matrix, storage " + name());}
    virtual void multVectorMatrix(const std::vector<real_t>&, complex_t*, complex_t*, SymType) const
    {error("not_yet_implemented", "multVectorMatrix for real matrix, storage " + name());}
    virtual void multVectorMatrix(const std::vector<complex_t>&, real_t*, complex_t*, SymType) const
    {error("not_yet_implemented", "multVectorMatrix for complex matrix, storage " + name());}
    virtual void multVectorMatrix(const std::vector<complex_t>&, complex_t*, complex_t*, SymType) const
    {error("not_yet_implemented", "multVectorMatrix for complex matrix, storage " + name());}
    //@}

    //@{
    //! Vector (Vector) * Matrix (Matrix)
    virtual void multVectorMatrix(const std::vector< Matrix<real_t> >&, const std::vector< Vector<real_t> >&, std::vector< Vector<real_t> >&, SymType) const
    {error("not_yet_implemented", "multVectorMatrix for matrix of real matrices, storage " + name());}
    virtual void multVectorMatrix(const std::vector< Matrix<real_t> >&, const std::vector< Vector<complex_t> >&, std::vector< Vector<complex_t> >&, SymType) const
    {error("not_yet_implemented", "multVectorMatrix for matrix of real matrices, storage " + name());}
    virtual void multVectorMatrix(const std::vector< Matrix<complex_t> >&, const std::vector< Vector<real_t> >&, std::vector< Vector<complex_t> >&, SymType) const
    {error("not_yet_implemented", "multVectorMatrix for matrix of complex matrices, storage " + name());}
    virtual void multVectorMatrix(const std::vector< Matrix<complex_t> >&, const std::vector< Vector<complex_t> >&, std::vector< Vector<complex_t> >&, SymType) const
    {error("not_yet_implemented", "multVectorMatrix for matrix of complex matrices, storage " + name());}
    //@}

     /* ----------------------------------------------------------------------------------------------------------------------------
                                       virtual triangular part matrix * vector
       ----------------------------------------------------------------------------------------------------------------------------*/
    //@{
    //! lower Matrix (Scalar) * Vector (Scalar)
    virtual void lowerMatrixVector(const std::vector<real_t>&, const std::vector<real_t>&, std::vector<real_t>&, SymType) const
    {error("not_yet_implemented", "lowerMatrixVector for real matrix, storage " + name());}
    virtual void lowerMatrixVector(const std::vector<real_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "lowerMatrixVector for real matrix, storage " + name());}
    virtual void lowerMatrixVector(const std::vector<complex_t>&, const std::vector<real_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "lowerMatrixVector for complex matrix, storage " + name());}
    virtual void lowerMatrixVector(const std::vector<complex_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "lowerMatrixVector for complex matrix, storage " + name());}
    //@}

    //@{
    //! lower Matrix diag 1 (Scalar) * Vector (Scalar)
    virtual void lowerD1MatrixVector(const std::vector<real_t>&, const std::vector<real_t>&, std::vector<real_t>&, SymType) const
    {error("not_yet_implemented", "lowerD1MatrixVector for real matrix, storage " + name());}
    virtual void lowerD1MatrixVector(const std::vector<real_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "lowerD1MatrixVector for real matrix, storage " + name());}
    virtual void lowerD1MatrixVector(const std::vector<complex_t>&, const std::vector<real_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "lowerD1MatrixVector for complex matrix, storage " + name());}
    virtual void lowerD1MatrixVector(const std::vector<complex_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "lowerD1MatrixVector for complex matrix, storage " + name());}
    //@}

    //@{
    //! upper Matrix (Scalar) * Vector (Scalar)
    virtual void upperMatrixVector(const std::vector<real_t>&, const std::vector<real_t>&, std::vector<real_t>&, SymType) const
    {error("not_yet_implemented", "upperMatrixVector for real matrix, storage " + name());}
    virtual void upperMatrixVector(const std::vector<real_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "upperMatrixVector for real matrix, storage " + name());}
    virtual void upperMatrixVector(const std::vector<complex_t>&, const std::vector<real_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "upperMatrixVector for complex matrix, storage " + name());}
    virtual void upperMatrixVector(const std::vector<complex_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "upperMatrixVector for complex matrix, storage " + name());}
    //@}

    //@{
    //! upper Matrix diag 1 (Scalar) * Vector (Scalar)
    virtual void upperD1MatrixVector(const std::vector<real_t>&, const std::vector<real_t>&, std::vector<real_t>&, SymType) const
    {error("not_yet_implemented", "upperD1MatrixVector for real matrix, storage " + name());}
    virtual void upperD1MatrixVector(const std::vector<real_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "upperD1MatrixVector for real matrix, storage " + name());}
    virtual void upperD1MatrixVector(const std::vector<complex_t>&, const std::vector<real_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "upperD1MatrixVector for complex matrix, storage " + name());}
    virtual void upperD1MatrixVector(const std::vector<complex_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "upperD1MatrixVector for complex matrix, storage " + name());}
    //@}

    //@{
    //! diagonal Matrix * Vector (Scalar)
    virtual void diagonalMatrixVector(const std::vector<real_t>&, const std::vector<real_t>&, std::vector<real_t>&, SymType) const
    {error("not_yet_implemented", "diagonalMatrixVector for real matrix, storage " + name());}
    virtual void diagonalMatrixVector(const std::vector<real_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "diagonalMatrixVector for real matrix, storage " + name());}
    virtual void diagonalMatrixVector(const std::vector<complex_t>&, const std::vector<real_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "diagonalMatrixVector for complex matrix, storage " + name());}
    virtual void diagonalMatrixVector(const std::vector<complex_t>&, const std::vector<complex_t>&, std::vector<complex_t>&, SymType) const
    {error("not_yet_implemented", "diagonalMatrixVector for complex matrix, storage " + name());}
    //@}

    /* ----------------------------------------------------------------------------------------------------------------------------
                                          virtual matrix + matrix
       ----------------------------------------------------------------------------------------------------------------------------*/
    //@{
    //! Matrix + Matrix
    virtual void addMatrixMatrix(const std::vector<real_t>&, const std::vector<real_t>&, std::vector<real_t>&) const
    {error("not_yet_implemented", "addMatrixMatrix for matrix of real matrices, storage " + name());}
    virtual void addMatrixMatrix(const std::vector<complex_t>&, const std::vector<complex_t>&, std::vector<complex_t>&) const
    {error("not_yet_implemented", "addMatrixMatrix for matrix of complex matrices, storage " + name());}
    virtual void addMatrixMatrix(const std::vector<complex_t>&, const std::vector<real_t>&, std::vector<complex_t>&) const
    {error("not_yet_implemented", "addMatrixMatrix for matrix of complex and real matrices, storage " + name());}
    virtual void addMatrixMatrix(const std::vector<real_t>&, const std::vector<complex_t>&, std::vector<complex_t>&) const
    {error("not_yet_implemented", "addMatrixMatrix for matrix of real and complex matrices, storage " + name());}

    virtual void addTwoMatrix(std::vector<real_t>& m1, SymType st1, const std::vector<number_t>& rowPtr2, const std::vector<number_t>& colPtr2, const std::vector<real_t>& m2, SymType st2)
    {error("not_yet_implemented", "addTwoMatrix for matrix of real and real matrices, storage " + name());}
    virtual void addTwoMatrix(std::vector<complex_t>& m1, SymType st1, const std::vector<number_t>& rowPtr2, const std::vector<number_t>& colPtr2, const std::vector<real_t>& m2, SymType st2)
    {error("not_yet_implemented", "addTwoMatrix for matrix of complex and real matrices, storage " + name());}
    virtual void addTwoMatrix(std::vector<complex_t>& m1, SymType st1, const std::vector<number_t>& rowPtr2, const std::vector<number_t>& colPtr2, const std::vector<complex_t>& m2, SymType st2)
    {error("not_yet_implemented", "addTwoMatrix for matrix of complex and complex matrices, storage " + name());}
    //@}

    /* ----------------------------------------------------------------------------------------------------------------------------
                                          virtual matrix * matrix
       ----------------------------------------------------------------------------------------------------------------------------*/
    //@{
    //! Matrix * Matrix (result in row dense storage), no virtual template
    virtual void multMatrixMatrix(const std::vector<real_t>&, const MatrixStorage&, const std::vector<real_t>&, std::vector<real_t>&,
                                  SymType = _noSymmetry, SymType = _noSymmetry) const;
    virtual void multMatrixMatrix(const std::vector<real_t>&, const MatrixStorage&, const std::vector<complex_t>&, std::vector<complex_t>&,
                                  SymType = _noSymmetry, SymType = _noSymmetry) const;
    virtual void multMatrixMatrix(const std::vector<complex_t>&, const MatrixStorage&, const std::vector<real_t>&, std::vector<complex_t>&,
                                  SymType = _noSymmetry, SymType = _noSymmetry) const;
    virtual void multMatrixMatrix(const std::vector<complex_t>&, const MatrixStorage&, const std::vector<complex_t>&, std::vector<complex_t>&,
                                  SymType = _noSymmetry, SymType = _noSymmetry) const;
    virtual void multMatrixMatrix(const std::vector<Matrix<real_t> >&, const MatrixStorage&, const std::vector<Matrix<real_t> >&, std::vector<Matrix<real_t> >&,
                                  SymType = _noSymmetry, SymType = _noSymmetry) const;
    virtual void multMatrixMatrix(const std::vector<Matrix<real_t> >&, const MatrixStorage&, const std::vector<Matrix<complex_t> >&, std::vector<Matrix<complex_t> >&,
                                  SymType = _noSymmetry, SymType = _noSymmetry) const;
    virtual void multMatrixMatrix(const std::vector<Matrix<complex_t> >&, const MatrixStorage&, const std::vector<Matrix<real_t> >&, std::vector<Matrix<complex_t> >&,
                                  SymType = _noSymmetry, SymType = _noSymmetry) const;
    virtual void multMatrixMatrix(const std::vector<Matrix<complex_t> >&, const MatrixStorage&, const std::vector<Matrix<complex_t> >&, std::vector<Matrix<complex_t> >&,
                                  SymType = _noSymmetry, SymType = _noSymmetry) const;
    //@}

    template<typename IteratorA,typename IteratorB, typename IteratorR>
    void multMatrixMatrixGeneric(IteratorA, const MatrixStorage&, IteratorB, IteratorR,
                                 SymType = _noSymmetry, SymType = _noSymmetry) const;    //!< generic Matrix * Matrix

    //@{
    //! Matrix * diag Matrix (result in current storage), no virtual template
    virtual void multMatrixDiagMatrix(const std::vector<real_t>&, const std::vector<real_t>&, std::vector<real_t>&)const;
    virtual void multMatrixDiagMatrix(const std::vector<real_t>&, const std::vector<complex_t>&, std::vector<complex_t>&)const;
    virtual void multMatrixDiagMatrix(const std::vector<complex_t>&, const std::vector<real_t>&, std::vector<complex_t>&)const;
    virtual void multMatrixDiagMatrix(const std::vector<complex_t>&, const std::vector<complex_t>&, std::vector<complex_t>&)const;
    virtual void multMatrixDiagMatrix(const std::vector<Matrix<real_t> >&, const std::vector<Vector<real_t> >&, std::vector<Matrix<real_t> >&)const;
    virtual void multMatrixDiagMatrix(const std::vector<Matrix<real_t> >&, const std::vector<Vector<complex_t> >&, std::vector<Matrix<complex_t> >&)const;
    virtual void multMatrixDiagMatrix(const std::vector<Matrix<complex_t> >&, const std::vector<Vector<real_t> >&, std::vector<Matrix<complex_t> >&) const;
    virtual void multMatrixDiagMatrix(const std::vector<Matrix<complex_t> >&, const std::vector<Vector<complex_t> >&, std::vector<Matrix<complex_t> >&)const;
    //@}

    template<typename IteratorA,typename IteratorB, typename IteratorR>
    void multMatrixDiagMatrixGeneric(IteratorA, IteratorB, IteratorR)const;  //!< generic Matrix * diag Matrix

    //@{
    //! diag Matrix * Matrix (result in current storage), no virtual template
    virtual void multDiagMatrixMatrix(const std::vector<real_t>&, const std::vector<real_t>&, std::vector<real_t>&)const;
    virtual void multDiagMatrixMatrix(const std::vector<real_t>&, const std::vector<complex_t>&, std::vector<complex_t>&)const;
    virtual void multDiagMatrixMatrix(const std::vector<complex_t>&, const std::vector<real_t>&, std::vector<complex_t>&)const;
    virtual void multDiagMatrixMatrix(const std::vector<complex_t>&, const std::vector<complex_t>&, std::vector<complex_t>&)const;
    virtual void multDiagMatrixMatrix(const std::vector<Vector<real_t> >&, const std::vector<Matrix<real_t> >&, std::vector<Matrix<real_t> >&)const;
    virtual void multDiagMatrixMatrix(const std::vector<Vector<real_t> >&, const std::vector<Matrix<complex_t> >&, std::vector<Matrix<complex_t> >&)const;
    virtual void multDiagMatrixMatrix(const std::vector<Vector<complex_t> >&, const std::vector<Matrix<real_t> >&, std::vector<Matrix<complex_t> >&) const;
    virtual void multDiagMatrixMatrix(const std::vector<Vector<complex_t> >&, const std::vector<Matrix<complex_t> >&, std::vector<Matrix<complex_t> >&)const;
    //@}

    template<typename IteratorA,typename IteratorB, typename IteratorR>
    void multDiagMatrixMatrixGeneric(IteratorA, IteratorB, IteratorR)const;  //!< generic diag Matrix * Matrix

    /* ----------------------------------------------------------------------------------------------------------------------------
                               virtual matrix factorizations (Skyline and Dense child classes only)
       ----------------------------------------------------------------------------------------------------------------------------*/
    //@{
    //! virtual LDLt factorizations (Skyline child classes only)
    virtual void ldlt(std::vector<real_t>&, std::vector<real_t>&, const SymType sym = _symmetric) const
    { noFactorization("L*D*Lt"); }
    virtual void ldlt(std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _symmetric) const
    { noFactorization("L*D*Lt"); }
    //@}

    //@{
    //! virtual incomplete LDLt factorizations (DualCs classes only)
    virtual void ildlt(std::vector<real_t>&, std::vector<real_t>&, const SymType sym = _symmetric) const
    { noFactorization("incomplete L*D*Lt"); }
    virtual void ildlt(std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _symmetric) const
    { noFactorization("incomplete L*D*Lt"); }
    //@}

    //@{
    //! virtual incomplete LLt factorizations (DualCs classes only)
    virtual void illt(std::vector<real_t>&, std::vector<real_t>&, const SymType sym = _symmetric) const
    { noFactorization("incomplete L*Lt"); }
    virtual void illt(std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _symmetric) const
    { noFactorization("incomplete L*Lt"); }
    //@}

    //@{
    //! virtual LU factorizations (Skyline and Dense child classes only)
    virtual void lu(std::vector<real_t>&, std::vector<real_t>&, const SymType sym = _noSymmetry) const
    { noFactorization("L*U"); }
    virtual void lu(std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _noSymmetry) const
    { noFactorization("L*U"); }
    virtual void lu(std::vector<real_t>&, std::vector<real_t>&, std::vector<number_t>&, const SymType sym = _noSymmetry) const
    { noFactorization("L*U with pivoting"); }
    virtual void lu(std::vector<complex_t>&, std::vector<complex_t>&, std::vector<number_t>&, const SymType sym = _noSymmetry) const
    { noFactorization("L*U with pivoting"); }
    //@}

    //@{
    //! virtual ILU factorizations ( CS child classes only)
    virtual void ilu(std::vector<real_t>&, std::vector<real_t>&, const SymType sym = _noSymmetry) const
    {  noFactorization("iL*UU"); }
    virtual void ilu(std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _noSymmetry) const
    {  noFactorization("iL*Ucomplexe"); }
    //@}

    //@{
    //! virtual LDL* factorizations (Skyline child classes only)
    virtual void ldlstar(std::vector<real_t>&, std::vector<real_t>&)  const
    { noFactorization("L*D*(L*) SelfAdjoint"); }
    virtual void ldlstar(std::vector<complex_t>&, std::vector<complex_t>&) const
    { noFactorization("L*D*(L*) SelfAdjoint"); }
    //@}

    //@{
    //! virtual iLDL* factorizations (Cs child classes only)
    virtual void ildlstar(std::vector<real_t>&, std::vector<real_t>&, const SymType sym = _selfAdjoint) const
    { noFactorization("L*D*(L*) SelfAdjoint"); }
    virtual void ildlstar(std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _selfAdjoint) const
    { noFactorization("L*D*(L*) SelfAdjoint"); }
    //@}

    //@{
    //! virtual iLL* factorizations (Cs child classes only)
    virtual void illstar(std::vector<real_t>&, std::vector<real_t>&, const SymType sym = _selfAdjoint) const
    { noFactorization("L*(L*) SelfAdjoint"); }
    virtual void illstar(std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _selfAdjoint) const
    { noFactorization("L*(L*) SelfAdjoint"); }
    //@}

    //@{
    // virtual Gauss elimination (Dense child classes only)
    virtual void gaussSolver(std::vector<real_t>&, std::vector<real_t>&) const
    { noFactorization("Gauss elimination"); }
    virtual void gaussSolver(std::vector<complex_t>&, std::vector<complex_t>&) const
    { noFactorization("Gauss elimination"); }
    virtual void gaussSolver(std::vector<real_t>&, std::vector<std::vector<real_t> >&) const
    { noFactorization("Gauss elimination"); }
    virtual void gaussSolver(std::vector<complex_t>&, std::vector<std::vector<complex_t> >&) const
    { noFactorization("Gauss elimination"); }
    //@}

    // Virtual diagonal and triangular linear system solvers after factorizations (Skyline child classes only)
    //@{
    //! Lower + diag(1) part factorizations
    virtual void lowerD1Solver(const std::vector<real_t>&, std::vector<real_t>&, std::vector<real_t>&) const
    { error("not_yet_implemented", "lowerD1Solver, storage " + name()); }
    virtual void lowerD1Solver(const std::vector<real_t>&, std::vector<complex_t>&, std::vector<complex_t>&) const
    { error("not_yet_implemented", "lowerD1Solver, storage " + name()); }
    virtual void lowerD1Solver(const std::vector<complex_t>&, std::vector<real_t>&, std::vector<complex_t>&) const
    { error("not_yet_implemented", "lowerD1Solver, storage " + name()); }
    virtual void lowerD1Solver(const std::vector<complex_t>&, std::vector<complex_t>&, std::vector<complex_t>&) const
    { error("not_yet_implemented", "lowerD1Solver, storage " + name()); }
    //@}

    //@{
    //! Diagonal part factorizations
    virtual void diagonalSolver(const std::vector<real_t>&, std::vector<real_t>&, std::vector<real_t>&) const
    { error("not_yet_implemented", "diagonalSolver, storage " + name()); }
    virtual void diagonalSolver(const std::vector<real_t>&, std::vector<complex_t>&, std::vector<complex_t>&) const
    { error("not_yet_implemented", "diagonalSolver, storage " + name()); }
    virtual void diagonalSolver(const std::vector<complex_t>&, std::vector<real_t>&, std::vector<complex_t>&) const
    { error("not_yet_implemented", "diagonalSolver, storage " + name()); }
    virtual void diagonalSolver(const std::vector<complex_t>&, std::vector<complex_t>&, std::vector<complex_t>&) const
    { error("not_yet_implemented", "diagonalSolver, storage " + name()); }
    //@}

    //@{
    //! Upper + diag(1) part factorizations
    virtual void upperD1Solver(const std::vector<real_t>&, std::vector<real_t>&, std::vector<real_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "upperD1Solver, storage " + name(), sym); }
    virtual void upperD1Solver(const std::vector<real_t>&, std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "upperD1Solver, storage " + name(), sym); }
    virtual void upperD1Solver(const std::vector<complex_t>&, std::vector<real_t>&, std::vector<complex_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "upperD1Solver, storage " + name(), sym); }
    virtual void upperD1Solver(const std::vector<complex_t>&, std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "upperD1Solver, storage " + name(), sym); }
    //@}

    //@{
    //! upper part factorizations
    virtual void upperSolver(const std::vector<real_t>&, std::vector<real_t>&, std::vector<real_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "upperSolver, storage " + name(), sym); }
    virtual void upperSolver(const std::vector<real_t>&, std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "upperSolver, storage " + name(), sym); }
    virtual void upperSolver(const std::vector<complex_t>&, std::vector<real_t>&, std::vector<complex_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "upperSolver, storage " + name(), sym); }
    virtual void upperSolver(const std::vector<complex_t>&, std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "upperSolver, storage " + name(), sym); }
    //@}

    //@{
    //! Lower + diag(1) part left factorizations
    virtual void lowerD1LeftSolver(const std::vector<real_t>&, std::vector<real_t>&, std::vector<real_t>&) const
    { error("not_yet_implemented", "lowerD1Solver, storage " + name()); }
    virtual void lowerD1LeftSolver(const std::vector<real_t>&, std::vector<complex_t>&, std::vector<complex_t>&) const
    { error("not_yet_implemented", "lowerD1Solver, storage " + name()); }
    virtual void lowerD1LeftSolver(const std::vector<complex_t>&, std::vector<real_t>&, std::vector<complex_t>&) const
    { error("not_yet_implemented", "lowerD1Solver, storage " + name()); }
    virtual void lowerD1LeftSolver(const std::vector<complex_t>&, std::vector<complex_t>&, std::vector<complex_t>&) const
    { error("not_yet_implemented", "lowerD1Solver, storage " + name()); }
    //@}

    //@{
    //! upper part left factorizations
    virtual void upperLeftSolver(const std::vector<real_t>&, std::vector<real_t>&, std::vector<real_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "upperSolver, storage " + name(), sym); }
    virtual void upperLeftSolver(const std::vector<real_t>&, std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "upperSolver, storage " + name(), sym); }
    virtual void upperLeftSolver(const std::vector<complex_t>&, std::vector<real_t>&, std::vector<complex_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "upperSolver, storage " + name(), sym); }
    virtual void upperLeftSolver(const std::vector<complex_t>&, std::vector<complex_t>&, std::vector<complex_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "upperSolver, storage " + name(), sym); }
    //@}

    /* ----------------------------------------------------------------------------------------------------------------------------
                                        SOR and SSOR stuff
       ----------------------------------------------------------------------------------------------------------------------------*/
    /*! Generic SOR methods, not fast , see child class for best algorithms */
    template<typename M, typename V, typename R>
    void sorDiagonalMatrixVectorG(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r,
                        const real_t w) const;                 //!< SOR operation w*D*v
    template<typename M, typename V, typename R>
    void sorLowerMatrixVectorG(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r,
                      const real_t w) const;                     //!< SOR operation [w*D+L]*v
    template<typename M, typename V, typename R>
    void sorUpperMatrixVectorG(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r,
                      const real_t w, const SymType sym) const;  //!< SOR operation [w*D+U]*v
    template<typename M, typename V, typename R>
    void sorDiagonalSolverG(const std::vector<M>& m, const std::vector<R>& b, std::vector<V>& x,
                    const real_t w) const;                        //!< SOR solver D/w*x = b
    template<typename M, typename V, typename R>
    void sorLowerSolverG(const std::vector<M>& m, const std::vector<V>& b, std::vector<R>& x,
                  const real_t w) const;                           //!< SOR solver (D/w+L)*x = b
    template<typename M, typename V, typename R>
    void sorUpperSolverG(const std::vector<M>& m, const std::vector<V>& b, std::vector<R>& x,
                  const real_t w, const SymType sym) const;         //!< SOR solver (D/w+U)*x = b
    //@{
    //! virtual matrix vector products w*Dv for [S]SOR algorithms
    virtual void sorDiagonalMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v,
                             std::vector<real_t>& r, const real_t w) const
    {sorDiagonalMatrixVectorG(m,v,r,w);}
    virtual void sorDiagonalMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v,
                             std::vector<complex_t>& r, const real_t w) const
    {sorDiagonalMatrixVectorG(m,v,r,w);}
    virtual void sorDiagonalMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v,
                             std::vector<complex_t>& r, const real_t w) const
    {sorDiagonalMatrixVectorG(m,v,r,w);}
    virtual void sorDiagonalMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v,
                             std::vector<complex_t>& r, const real_t w) const
    {sorDiagonalMatrixVectorG(m,v,r,w);}
    //@}

    //@{
    //! virtual matrix vector products [w*D+L]v for [S]SOR algorithms
    virtual void sorLowerMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v,
                           std::vector<real_t>& r, const real_t w, const SymType sym = _noSymmetry) const
    {sorLowerMatrixVectorG(m,v,r,w);}
    virtual void sorLowerMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v,
                           std::vector<complex_t>& r, const real_t w, const SymType sym = _noSymmetry) const
    {sorLowerMatrixVectorG(m,v,r,w);}
    virtual void sorLowerMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v,
                           std::vector<complex_t>& r, const real_t w, const SymType sym = _noSymmetry) const
    {sorLowerMatrixVectorG(m,v,r,w);}
    virtual void sorLowerMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v,
                           std::vector<complex_t>& r, const real_t w, const SymType sym = _noSymmetry) const
    { sorLowerMatrixVectorG(m,v,r,w);}
    //@}

    //@{
    //! virtual matrix vector products [w*D+U]v for [S]SOR algorithms
    virtual void sorUpperMatrixVector(const std::vector<real_t>& m, const std::vector<real_t>& v,
                           std::vector<real_t>& r, const real_t w, const SymType sym = _noSymmetry) const
    {sorUpperMatrixVectorG(m,v,r,w,sym);}
    virtual void sorUpperMatrixVector(const std::vector<real_t>& m, const std::vector<complex_t>& v,
                           std::vector<complex_t>& r, const real_t w, const SymType sym = _noSymmetry) const
    {sorUpperMatrixVectorG(m,v,r,w,sym);}
    virtual void sorUpperMatrixVector(const std::vector<complex_t>& m, const std::vector<real_t>& v,
                           std::vector<complex_t>& r, const real_t w, const SymType sym = _noSymmetry) const
    {sorUpperMatrixVectorG(m,v,r,w,sym);}
    virtual void sorUpperMatrixVector(const std::vector<complex_t>& m, const std::vector<complex_t>& v,
                           std::vector<complex_t>& r,const real_t w, const SymType sym = _noSymmetry) const
    {sorUpperMatrixVectorG(m,v,r,w,sym);}
    //@}

    //@{
    //! virtual  diagonal and triangular linear system solvers [D/w] x = b for [S]SOR algorithms
    virtual void sorDiagonalSolver(const std::vector<real_t>& m, const std::vector<real_t>& b,
                         std::vector<real_t>& x, const real_t w) const
    {sorDiagonalSolverG(m,b,x,w);}
    virtual void sorDiagonalSolver(const std::vector<real_t>& m, const std::vector<complex_t>& b,
                         std::vector<complex_t>& x, const real_t w) const
    {sorDiagonalSolverG(m,b,x,w);}
    virtual void sorDiagonalSolver(const std::vector<complex_t>& m, const std::vector<real_t>& b,
                         std::vector<complex_t>& x, const real_t w) const
    {sorDiagonalSolverG(m,b,x,w);}
    virtual void sorDiagonalSolver(const std::vector<complex_t>& m, const std::vector<complex_t>& b,
                         std::vector<complex_t>& x, const real_t w) const
    {sorDiagonalSolverG(m,b,x,w);}
    //@}

    //@{
    //! virtual diagonal and triangular linear system solvers [D/w + L] x = b for [S]SOR algorithms
    virtual void sorLowerSolver(const std::vector<real_t>& m, const std::vector<real_t>& b,
                       std::vector<real_t>& x, const real_t w) const
    {sorLowerSolverG(m,b,x,w);}
    virtual void sorLowerSolver(const std::vector<real_t>& m, const std::vector<complex_t>& b,
                       std::vector<complex_t>& x, const real_t w) const
    {sorLowerSolverG(m,b,x,w);}
    virtual void sorLowerSolver(const std::vector<complex_t>& m, const std::vector<real_t>& b,
                       std::vector<complex_t>& x, const real_t w) const
    {sorLowerSolverG(m,b,x,w);}
    virtual void sorLowerSolver(const std::vector<complex_t>& m, const std::vector<complex_t>& b,
                       std::vector<complex_t>& x, const real_t w) const
    {sorLowerSolverG(m,b,x,w);}
    //@}

    //@{
    //! virtual diagonal and triangular linear system solvers [D/w+U] x = b for [S]SOR algorithms
    virtual void sorUpperSolver(const std::vector<real_t>& m, const std::vector<real_t>& b,
                       std::vector<real_t>& x, const real_t w, const SymType sym = _noSymmetry) const
    {sorUpperSolverG(m,b,x,w,sym);}
    virtual void sorUpperSolver(const std::vector<real_t>& m, const std::vector<complex_t>& b,
                       std::vector<complex_t>& x, const real_t w, const SymType sym = _noSymmetry) const
    {sorUpperSolverG(m,b,x,w,sym);}
    virtual void sorUpperSolver(const std::vector<complex_t>& m, const std::vector<real_t>& b,
                       std::vector<complex_t>& x, const real_t w, const SymType sym = _noSymmetry) const
    {sorUpperSolverG(m,b,x,w,sym);}
    virtual void sorUpperSolver(const std::vector<complex_t>& m, const std::vector<complex_t>& b,
                       std::vector<complex_t>& x, const real_t w, const SymType sym = _noSymmetry) const
    {sorUpperSolverG(m,b,x,w,sym);}
    //@}


    //@{
    //! conversion to umfpack format
    virtual void toUmfPack(const std::vector<real_t>&, std::vector<int_t>&, std::vector<int_t>&, std::vector<real_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "toUmfPack (real_t Matrix)"); }
    virtual void toUmfPack(const std::vector<complex_t>&, std::vector<int_t>&, std::vector<int_t>&, std::vector<complex_t>&, const SymType sym = _noSymmetry) const
    { error("not_yet_implemented", "toUmfPack (complex_t Matrix)"); }
    //@}

    //@{
    //! set value of diagonal
    virtual void setDiagValue(std::vector<real_t>&, const real_t)
    { error("not_yet_implemented", "diagValue (real_t Matrix)"); }
    virtual void setDiagValue(std::vector<complex_t>&, const complex_t)
    { error("not_yet_implemented", "diagValue (complex_t Matrix)"); }
    //@}

  protected:
    void isSingular(const string_t&, const number_t) const; //!< error handler when singular matrix
};

std::ostream& operator<<(std::ostream&, const MatrixStorage&); //!< print operator

//! find matrix storage in vector theMatrixStorages of class MatrixStorage
MatrixStorage* findMatrixStorage(const string_t& id, StorageType st, AccessType at); // old version
MatrixStorage* findMatrixStorage(const string_t& id, StorageType st, AccessType at, StorageBuildType sb, bool scalar=false, number_t nbr=0, number_t nbc=0);
//! create matrix storage from indices (vector of column indices for each row)
MatrixStorage* createMatrixStorage(StorageType st, AccessType at, StorageBuildType sb, number_t, number_t,
                                   const std::vector<std::vector<number_t> >&, const string_t&);
//! print list of MatrixStorage's
void printMatrixStorages(std::ostream&);
inline void printMatrixStorages(PrintStream& os)
{printMatrixStorages(os.currentStream());}

/*--------------------------------------------------------------------------------
 print matrix to ostream in dense form:
    A11 A12 ... A1n
    A21 A22 ... A2n
    ...
 This algorithm uses the function pos(i,j), it is general but not optimal
 note that the matrix may have symmetry
--------------------------------------------------------------------------------*/

void printDense(std::ostream&, const real_t&, number_t);            //!< print real scalar in dense format
void printDense(std::ostream&, const complex_t&, number_t);         //!< print complex scalar in dense format

template<typename T>
void printDense(std::ostream& os, const Matrix<T>& v, number_t k) //! print row of Matrix in dense format
{
  number_t nc = v.numberOfColumns();
  typename Matrix<T>::const_iterator it = v.begin() + (k - 1) * nc;

  printDense(os, *it, 1);
  ++it;
  for(number_t l = 2; l <= nc; l++, it++) // loop on subcols
  {
    os << " ";
    printDense(os, *it, 1);
  }
}

// number of rows and cols of any value type of matrix
template<typename T>
number_t numberOfRows(const Matrix<T>& m) {return m.numberOfRows();}
template<typename T>
number_t numberOfCols(const Matrix<T>& m) {return m.numberOfCols();}
number_t numberOfRows(const real_t&);
number_t numberOfRows(const complex_t&);
number_t numberOfCols(const real_t&);
number_t numberOfCols(const complex_t&);

template<typename T>
void MatrixStorage::printDenseMatrix(std::ostream& os, const std::vector<T>& m, SymType sym) const
{
  typename std::vector<T>::const_iterator itmb = m.begin(), itm;
  number_t nr = numberOfRows(*(itmb + 1));   // at least one entry in matrix
  for(number_t i = 1; i <= nbRows_; i++)     // loop on rows
    for(number_t k = 1; k <= nr; k++)        // loop on subrows
    {
      for(number_t j = 1; j <= nbCols_; j++) // loop on cols
      {
        if (j > 1) { os << " "; }
        itm = itmb + pos(i, j, sym);
        switch(sym)
        {
          case _skewSymmetric: printDense(os, -*itm, k); break;
          case _skewAdjoint: printDense(os, -conj(*itm), k); break;
          case _selfAdjoint: printDense(os, conj(*itm), k); break;
          default:             printDense(os, *itm, k);
        }
      }
      os << std::endl;
    }
}

/*--------------------------------------------------------------------------------
 print matrix to ostream in coordinate form  i j A_ij:
   1 1 A11
   1 2 A12
   ...
 a null value or a value less than a tolerance in module is not displayed
 The coordinates are not necessarly ordered !
 This algorithm uses the function pos(i,j), it is general but not optimal
 note that the matrix may have symmetry
--------------------------------------------------------------------------------*/
void printCoo(std::ostream& os, const real_t& v, number_t i, number_t j, real_t tol = theTolerance);
void printCoo(std::ostream& os, const complex_t& v, number_t i, number_t j, real_t tol = theTolerance);

template<typename T>
void printCoo(std::ostream& os, const Matrix<T>& v, number_t i, number_t j, real_t tol = theTolerance)
{
  number_t nr = v.numberOfRows(), nc = v.numberOfColumns();
  typename Matrix<T>::const_iterator it = v.begin();
  for(number_t k = 1; k <= nr; k++)          // loop on subrows
    for(number_t l = 1; l <= nc; l++, it++)  // loop on subcols
    { printCoo(os, *it, (i - 1)* nr + k, (j - 1)* nc + l, tol); }
}

template<typename T>
void MatrixStorage::printCooMatrix(std::ostream& os, const std::vector<T>& m, SymType sym, real_t tol) const
{
  typename std::vector<T>::const_iterator itmb = m.begin(), itm;
  number_t p;
  for (number_t i = 1; i <= nbRows_; i++)    // loop on rows
  {
    for (number_t j = 1; j <= nbCols_; j++)  // loop on cols
    {
      p = pos(i, j, sym);
      if( p > 0 ) // print only values in storage
      {
        itm = itmb + p;
        switch(sym)
        {
          case _skewSymmetric: printCoo(os, -*itm, i, j, tol); break;
          case _skewAdjoint:   printCoo(os, -conj(*itm), i, j, tol); break;
          case _selfAdjoint:   printCoo(os, conj(*itm), i, j, tol); break;
         default:              printCoo(os, *itm, i, j, tol);
        }
      }
    }
  }
}

void readItem(std::istream&, real_t&, bool isreal = true);   //!< read real item from istream
void readItem(std::istream&, complex_t&, bool isreal = false); //!< read complex item from istream


/*! template Matrix * Matrix, generic algorithm (see child for better ones)
   itmA: iterator on A matrix values
   itmB: iterator on B matrix values
   itmR: iterator on result matrix values
*/
template<typename IteratorA, typename IteratorB, typename IteratorR>
void MatrixStorage::multMatrixMatrixGeneric(IteratorA itmA, const MatrixStorage& stB, IteratorB itmB, IteratorR itmR, SymType symA, SymType symB) const
{
  number_t nbr=nbRows_, nbc=stB.nbCols_;
  std::vector<std::pair<number_t, number_t> >::iterator itrow, itcol, itk;
  itmR++;   //first value is reserved
  for(number_t i=1; i<=nbr; i++)
  {
    std::vector<std::pair<number_t, number_t> > rowi=getRow(symA,i);
    for(number_t j=1; j<=nbc; j++,itmR++)
    {
      std::vector<std::pair<number_t, number_t> > colj=stB.getCol(symB,j);
      itrow=rowi.begin(); itcol=colj.begin(); itk=itrow;
      *itmR=0;
      bool endrow=false;
      for(; itcol!=colj.end() && !endrow; itcol++)
      {
        number_t c=itcol->first;
        while(itk->first<c && itk!=rowi.end()) itk++;
        if(itk==rowi.end()) endrow=true;
        else if(itk->first==c) *itmR+=*(itmA+itk->second)** (itmB+itcol->second);
      }
    }
  }
}

/*! template Diag Matrix * Matrix, generic algorithm (see child for better ones)
   itA: iterator on matrix values
   itB: iterator on diag matrix values
   itR: iterator on result matrix values
*/
template<typename IteratorA,typename IteratorB, typename IteratorR>
void MatrixStorage::multMatrixDiagMatrixGeneric(IteratorA itA, IteratorB itB, IteratorR itR) const
{
  std::vector<std::pair<number_t, number_t> >::iterator itcol;
  for(number_t j=1; j<=nbCols_; j++, itB++)  //column loop
  {
    std::vector<std::pair<number_t, number_t> > colj=getCol(_noSymmetry,j); //potential bug, symmetry of matrix A not used !
    itcol=colj.begin();
    for(; itcol!=colj.end(); itcol++)
    {
      number_t a=itcol->second;
      *(itR+a)= *(itA+a)** itB;
    }
  }
}

/*! template Diag Matrix * Matrix, generic algorithm (see child for better ones)
   itA: iterator on diag matrix values
   itB: iterator on matrix values
   itR: iterator on result matrix values
*/
template<typename IteratorA,typename IteratorB, typename IteratorR>
void MatrixStorage::multDiagMatrixMatrixGeneric(IteratorA itA, IteratorB itB, IteratorR itR) const
{
  std::vector<std::pair<number_t, number_t> >::iterator itrow;
  for(number_t i=1; i<=nbRows_; i++, itA++)  //row loop
  {
    std::vector<std::pair<number_t, number_t> > rowi=getRow(_noSymmetry,i);//potential bug, symmetry of matrix A not used !
    itrow=rowi.begin();
    for(; itrow!=rowi.end(); itrow++)
    {
      number_t a=itrow->second;
      *(itR+a)= *itA** (itB+a);
    }
  }
}

/* -------------------------------------------------------------------------------
      SOR and SSOR stuff
      Generic SOR methods using position function -> not fast
      see child class for faster methods
 ---------------------------------------------------------------------------------*/
//! SOR operation w*D*v
template<typename M, typename V, typename R>
void MatrixStorage::sorDiagonalMatrixVectorG(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r,
    const real_t w) const
{
  number_t nr=nbOfRows(), nc=nbOfColumns();
  r.resize(nr,M(0));
  typename std::vector<M>::const_iterator it_m= m.begin();
  typename std::vector<V>::const_iterator it_v = v.begin();
  typename std::vector<R>::iterator it_r = r.begin();
  for(number_t i=1; i<=std::min(nr,nc); ++i, ++it_r, ++it_v)
  {
    number_t q=pos(i,i);
    if(q>0) *it_r = w** (it_m + q)* (*it_v);
  }
}

//! SOR operation [w*D+L]*v
template<typename M, typename V, typename R>
void MatrixStorage::sorLowerMatrixVectorG(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r,
    const real_t w) const
{
  sorDiagonalMatrixVectorG(m,v,r,w);  //diagonal part
  //lower part
  std::vector<std::pair<number_t, number_t> >::iterator ita;
  number_t nr=nbOfRows(), nc=nbOfColumns();
  typename std::vector<R>::iterator it_r = r.begin();
  typename std::vector<V>::const_iterator it_v = v.begin();
  typename std::vector<M>::const_iterator it_m= m.begin();
  if(accessType()!=_col) //row algorithm
  {
    it_r++;
    for(number_t r=2; r<=nr; ++r, ++it_r)
    {
      std::vector<std::pair<number_t, number_t> > rowadr=getRow(_noSymmetry, r,1,r-1);
      it_v= v.begin();
      for(ita=rowadr.begin(); ita!=rowadr.end(); ++ita)
        *it_r += *(it_m + ita->second)** (it_v + (ita->first -1));
    }
  }
  else //col algorithm
  {
    it_v= v.begin();
    for(number_t c=1; c<=nc; ++c, ++it_v)
    {
      std::vector<std::pair<number_t, number_t> > coladr=getCol(_noSymmetry, c,c+1,nr);
      for(ita=coladr.begin(); ita!=coladr.end(); ++ita)
        *(it_r + (ita->first -1)) += *(it_m + ita->second)** it_v;
    }
  }
}

//! SOR operation [w*D+U]*v
template<typename M, typename V, typename R>
void MatrixStorage::sorUpperMatrixVectorG(const std::vector<M>& m, const std::vector<V>& v, std::vector<R>& r,
    const real_t w, const SymType sym) const
{
  //if(theVerboseLevel>0) thePrintStream<<"use generic sorUpperMatrixVector for "<<name()<<" storage"<<eol;
  sorDiagonalMatrixVectorG(m,v,r,w);  //diagonal part
  //upper part
  std::vector<std::pair<number_t, number_t> >::iterator ita;
  number_t nr=nbOfRows(), nc=nbOfColumns();
  typename std::vector<R>::iterator it_r = r.begin();
  typename std::vector<V>::const_iterator it_v = v.begin();
  typename std::vector<M>::const_iterator it_m = m.begin();
  if(accessType_!=_col) //row algorithm
  {
    for(number_t r=1; r<=nr; ++r, ++it_r)
    {
      std::vector<std::pair<number_t, number_t> > rowadr=getRow(sym,r,r+1,nc);
      it_v= v.begin();
      switch(sym)
      {
        case _selfAdjoint:
          for(ita=rowadr.begin(); ita!=rowadr.end(); ++ita)
            *it_r += conj(*(it_m + ita->second))** (it_v + (ita->first -1));
          break;
        case _skewSymmetric:
          for(ita=rowadr.begin(); ita!=rowadr.end(); ++ita)
            *it_r += -(*(it_m + ita->second))** (it_v + (ita->first -1));
          break;
        case _skewAdjoint:
          for(ita=rowadr.begin(); ita!=rowadr.end(); ++ita)
            *it_r += -conj(*(it_m + ita->second))** (it_v + (ita->first -1));
          break;
        default:
          for(ita=rowadr.begin(); ita!=rowadr.end(); ++ita)
            *it_r += *(it_m + ita->second)** (it_v + (ita->first -1));
      }
    }
  }
  else //col algorithm
  {
    it_v= v.begin()+1;
    for(number_t c=2; c<=nc; ++c, ++it_v)
    {
      std::vector<std::pair<number_t, number_t> > coladr=getCol(sym,c,1,c-1);
      switch(sym)
      {
        case _selfAdjoint:
          for(ita=coladr.begin(); ita!=coladr.end(); ++ita)
            *(it_r + (ita->first -1)) += conj(*(it_m + ita->second))** it_v;
          break;
        case _skewSymmetric:
          for(ita=coladr.begin(); ita!=coladr.end(); ++ita)
            *(it_r + (ita->first -1)) += -(*(it_m + ita->second))** it_v;
          break;
        case _skewAdjoint:
          for(ita=coladr.begin(); ita!=coladr.end(); ++ita)
            *(it_r + (ita->first -1)) += -conj(*(it_m + ita->second))** it_v;
          break;
        default:
          for(ita=coladr.begin(); ita!=coladr.end(); ++ita)
            *(it_r + (ita->first -1)) += *(it_m + ita->second)** it_v;
      }
    }
  }
}

//! SOR solver D/w*x = b
template<typename M, typename V, typename R>
void MatrixStorage::sorDiagonalSolverG(const std::vector<M>& m, const std::vector<R>& b, std::vector<V>& x,
                          const real_t w) const
{
  //if(theVerboseLevel>0) thePrintStream<<"use generic sorDiagonalSolver for "<<name()<<" storage"<<eol;
  number_t nr=nbOfRows();
  x.resize(nr);
  typename std::vector<M>::const_iterator it_m= m.begin();
  typename std::vector<R>::const_iterator it_b = b.begin();
  typename std::vector<V>::iterator it_x = x.begin();
  for(number_t i=1; i<=nr; ++i, ++it_x, ++it_b)
  {
    number_t q=pos(i,i);
    if(q>0) *it_x = w** it_b / *(it_m + q);
  }
}

//! SOR solver (D/w+L)*x = b
template<typename M, typename V, typename R>
void MatrixStorage::sorLowerSolverG(const std::vector<M>& m, const std::vector<V>& b, std::vector<R>& x,
                        const real_t w) const
{
  //if(theVerboseLevel>0) thePrintStream<<"use generic sorLowerSolver for "<<name()<<" storage"<<eol;
  number_t nr=nbOfRows();
  x.resize(nr);
  typename std::vector<R>::iterator it_xb=x.begin(), it_x=it_xb;
  typename std::vector<V>::const_iterator it_b = b.begin();
  typename std::vector<M>::const_iterator it_m= m.begin();
  std::vector<std::pair<number_t, number_t> >::iterator ita;
  for(it_x=it_xb; it_x != x.end(); ++it_x, ++it_b) *it_x=*it_b;  //x=b

  number_t r=1;
  if(accessType()!=_col) //row algorithm
  {
    for(it_x=it_xb; it_x != x.end(); ++it_x, ++r)
    {
      if(r>1)
      {
        std::vector<std::pair<number_t, number_t> > rowadr=getRow(_noSymmetry, r,1,r-1);
        for(ita=rowadr.begin(); ita!=rowadr.end(); ++ita)
          *it_x -= *(it_m + ita->second)** (it_xb + (ita->first - 1));
      }
      *it_x *= w / *(it_m + pos(r,r));  // zero diag is not checked !
    }
  }
  else //col algorithm
  {
    for(it_x=it_xb; it_x != x.end(); ++it_x, ++r)
    {
      *it_x *= w / *(it_m + pos(r,r));  // zero diag is not checked !
      std::vector<std::pair<number_t, number_t> > coladr=getCol(_noSymmetry,r,r+1,nr);
      for(ita=coladr.begin(); ita!=coladr.end(); ++ita)
        *(it_xb + (ita->first -1)) -= *(it_m + ita->second)** it_x;
    }
  }
}

//! SOR solver (D/w+U)*x = b
template<typename M, typename V, typename R>
void MatrixStorage::sorUpperSolverG(const std::vector<M>& m, const std::vector<V>& b, std::vector<R>& x,
                        const real_t w, const SymType sym) const
{
  //if(theVerboseLevel>0) thePrintStream<<"use generic sorUpperSolver for "<<name()<<" storage"<<eol;
  number_t nr=nbOfRows(), nc=nbOfColumns();
  x.resize(nr);
  typename std::vector<R>::reverse_iterator it_xb=x.rbegin(), it_x=it_xb;  //use reverse iterator !!!
  typename std::vector<V>::const_reverse_iterator it_b = b.rbegin();
  typename std::vector<M>::const_iterator it_m= m.begin();
  std::vector<std::pair<number_t, number_t> >::iterator ita;
  for(it_x=it_xb; it_x != x.rend(); ++it_x, ++it_b) *it_x=*it_b;  //x=b

  number_t r=nr;
  if(accessType()!=_col) //row algorithm
  {
    for(it_x=it_xb; it_x != x.rend(); ++it_x, --r)
    {
      std::vector<std::pair<number_t, number_t> > rowadr=getRow(sym,r,r+1,nc);
      switch(sym)
      {
        case _selfAdjoint:
          for(ita=rowadr.begin(); ita!=rowadr.end(); ++ita)
            *it_x -= conj(*(it_m + ita->second))** (it_xb + (nr-ita->first));
          break;
        case  _skewSymmetric:
          for(ita=rowadr.begin(); ita!=rowadr.end(); ++ita)
            *it_x -= -(*(it_m + ita->second))** (it_xb + (nr-ita->first));
          break;
        case _skewAdjoint:
          for(ita=rowadr.begin(); ita!=rowadr.end(); ++ita)
            *it_x -= -conj(*(it_m + ita->second))** (it_xb + (nr-ita->first));
          break;
        default:
          for(ita=rowadr.begin(); ita!=rowadr.end(); ++ita)
            *it_x -= *(it_m + ita->second)** (it_xb + (nr-ita->first));
      }
      *it_x *= w / *(it_m + pos(r,r));  // zero diag is not checked !
    }
  }
  else //col algorithm
  {
    for(it_x=it_xb; it_x != x.rend(); ++it_x, --r)
    {
      *it_x *= w / *(it_m + pos(r,r));  // zero diag is not checked !
      if(r>1)
      {
        std::vector<std::pair<number_t, number_t> > coladr=getCol(sym,r,1,r-1);
        switch(sym)
        {
          case _selfAdjoint:
            for(ita=coladr.begin(); ita!=coladr.end(); ++ita)
              *(it_xb + (nr -ita->first)) -= conj(*(it_m + ita->second))** it_x;
            break;
          case _skewSymmetric:
            for(ita=coladr.begin(); ita!=coladr.end(); ++ita)
              *(it_xb + (nr -ita->first)) -= -(*(it_m + ita->second))** it_x;
            break;
          case _skewAdjoint:
            for(ita=coladr.begin(); ita!=coladr.end(); ++ita)
              *(it_xb + (nr -ita->first)) -= -conj(*(it_m + ita->second))** it_x;
            break;
          default:
            for(ita=coladr.begin(); ita!=coladr.end(); ++ita)
              *(it_xb + (nr -ita->first)) -= *(it_m + ita->second)** it_x;
        }
      }
    }
  }
}

/* =====================================================================================
  colamd/symamd - sparse matrix column ordering algorithm adapt from SuiteSparse colamd
  ====================================================================================== */
bool colAmd(number_t nbr, number_t nbc, const std::vector<number_t>& rowIndices,
            const std::vector<number_t>& colPointer, std::vector<number_t>& colPerm);
bool symAmd(number_t nbr, number_t nbc, const std::vector<number_t>& rowIndices,
            const std::vector<number_t>& colPointer, std::vector<number_t>& colPerm);

bool sameStorage(const MatrixStorage&,const MatrixStorage&); //!< test if two storages are similar

} // end of namespace xlifepp

#endif // MATRIX_STORAGE_HPP

