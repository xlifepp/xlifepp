/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file MatrixEntry.hpp
  \author E. Lunéville
  \since 01 nov 2013
  \date 01 nov 2013

  \brief Definition of the xlifepp::MatrixEntry class
*/

#ifndef MATRIX_ENTRY_HPP
#define MATRIX_ENTRY_HPP


#include "config.h"
#include "largeMatrix.h"
#include "utils.h"

namespace xlifepp{

/*!
  \class MatrixEntry
  small class handling current types of LargeMatrix
  shadowing template parameter of LargeMatrix
 */
class MatrixEntry
{
  public:
    ValueType valueType_;                             //!< value type of the entries (_real,_complex)
    StrucType strucType_;                             //!< structure type of the entries (_scalar,_matrix)
    LargeMatrix<real_t>* rEntries_p;                  //!< pointer to real LargeMatrix
    LargeMatrix<complex_t>* cEntries_p;               //!< pointer to complex LargeMatrix
    LargeMatrix<Matrix<real_t> >* rmEntries_p;        //!< pointer to LargeMatrix of real matrices
    LargeMatrix<Matrix<complex_t> >* cmEntries_p;     //!< pointer to LargeMatrix of complex matrices
    dimPair nbOfComponents;                           //!< number of line and columns of matrix values, (1,1) in scalar case

    //constructors/destructor
    MatrixEntry();                                  //!< default constructor
    MatrixEntry(ValueType, StrucType, MatrixStorage*, dimPair valdim= dimPair(1,1), SymType sy = _noSymmetry); //!< constructor from a storage pointer
    MatrixEntry(SpecialMatrix, StorageType, AccessType, number_t, number_t, const real_t&); //!< construct special matrix (constant diagonal)
    MatrixEntry(SpecialMatrix, StorageType, AccessType, number_t, number_t, const complex_t&); //!< construct special matrix (constant diagonal)
    MatrixEntry(SpecialMatrix, StorageType, AccessType, number_t, number_t, const Matrix<real_t>&); //!< construct special matrix (constant diagonal)
    MatrixEntry(SpecialMatrix, StorageType, AccessType, number_t, number_t, const Matrix<complex_t>&); //!< construct special matrix (constant diagonal)
    MatrixEntry(const MatrixEntry&, bool storageCopy=false);  //!< copy constructor (full copy) with option to force the copy of storage
    MatrixEntry& operator=(const MatrixEntry&);     //!< assign operator (full copy)
    ~MatrixEntry();                                 //!< destructor
    void clear();                                   //!< deallocate memory used by a MatrixEntry

    //accessors and properties
    ValueType valueType() const                     //! return valueType (const)
    {return valueType_;}
    ValueType& valueType()                          //! return valueType (non const)
    {return valueType_;}
    StrucType strucType() const                     //! return strucType (const)
    {return strucType_;}
    StrucType& strucType()                          //! return strucType (non const)
    {return strucType_;}
    number_t nbOfRows() const;                      //!< return the number of rows
    number_t nbOfCols() const;                      //!< return the number of columns
    void setNbOfRows(number_t n);                   //!< set the number of rows
    void setNbOfCols(number_t n);                   //!< set the number of columns
    StorageType storageType() const;                //!< return the storage type
    AccessType accessType() const;                  //!< return the access type
    SymType symmetry() const;                       //!< return symmetry of matrix
    SymType& symmetry();                            //!< return symmetry of matrix (r/w)
    MatrixStorage* storagep() const;                //!< return storage pointer (const)
    MatrixStorage* storagep();                      //!< return storage pointer (non const)
    bool isDiagonal() const;                        //!< true if matrix is a diagonal one
    bool isId(const double& tol=0.) const;          //!< true if matrix is a diagonal one
    bool isvoid() const                             //! true if matrix is void
    {return (storagep()->size()==0 || nbOfCols()==0 || nbOfRows()==0);}
    number_t size() const                           //! size of matrixentry, counted in dofs
    {return storagep()->size();}
    number_t scalarSize() const                     //! size of matrixentry, counted in scalar
    {return storagep()->size()*nbOfComponents.first*nbOfComponents.second;}
    FactorizationType factorization() const;        //!< return factorisation type
    template <typename T>
    LargeMatrix<T>& getLargeMatrix() const;         //!< return a reference to LargeMatrix object

    //value managment
    void setEntry(number_t, number_t, const real_t &, bool errorOn=true);            //!< set (i,j) real value (indices start from 1)
    void setEntry(number_t, number_t, const complex_t &, bool errorOn=true);         //!< set (i,j) complex value (indices start from 1)
    void setEntry(number_t, number_t, const Matrix<real_t> &, bool errorOn=true);    //!< set (i,j) real matrix value (indices start from 1)
    void setEntry(number_t, number_t, const Matrix<complex_t> &, bool errorOn=true); //!< set (i,j) complex matrix value (indices start from 1)
    void setEntry(number_t, number_t, const Value&, dimen_t k=0, dimen_t l=0, bool errorOn=true);  //!< set (i,j) value (indices start from 1)
    void setColToZero(number_t, number_t);                                           //!< set c1 to c2 cols to 0 (index start from 1)
    void setRowToZero(number_t, number_t);                                           //!< set r1 to r2 rows to 0 (index start from 1)
    void setCol(const Value& val, number_t c1, number_t c2);                         //!< set c1 to c2 rows to val (index start from 1), no storage modification!
    void setRow(const Value& val, number_t r1, number_t r2);                         //!< set r1 to r2 rows to val (index start from 1), no storage modification!
    real_t getEntry(number_t, number_t, real_t &) const;                             //!< get (i,j) real value (indices start from 1)
    complex_t getEntry(number_t, number_t, complex_t &) const ;                      //!< get (i,j) complex value (indices start from 1)
    Matrix<real_t> getEntry(number_t, number_t, Matrix<real_t> &) const ;            //!< get (i,j) real matrix value (indices start from 1)
    Matrix<complex_t> getEntry(number_t, number_t, Matrix<complex_t> &) const;       //!< get (i,j) complex matrix value (indices start from 1)
    Value getEntry(number_t i, number_t j, dimen_t k=0, dimen_t l=0) const;          //!< get (i,j) as a Value (indices start from 1)
    VectorEntry getRowCol(number_t, AccessType) const;                               //!< return a row or a col as VectorEntry

    //conversion tools
    void toSkyline();                               //!< convert current matrix storage to skyline storage (useful for factorization)
    void toStorage(MatrixStorage*);                 //!< convert current matrix storage to an other matrix storage
    MatrixEntry* toScalar();                        //!< create new scalar matrix entry from non scalar matrix entry
    void toUnsymmetric(AccessType at=_sym);         //!< convert current matrix to unsymmetric representation if it has a symmetric one
    void roundToZero(real_t aszero=10*theEpsilon);  //!< round to zero all coefficients close to 0 (|.| < aszero)
    MatrixEntry& toComplex();                       //!< change type of entries rEntries-> cEntries or rmEntries -> cmEntries
    MatrixEntry& toReal(bool realPart=true);        //!< change type of entries cEntries-> rEntries or cmEntries -> rmEntries taking real or imag part
    MatrixEntry& toConj();                          //!< modify current entries to conjugate entries if they are not real
    void toMatrix(const std::vector<dimen_t>&, const std::vector<dimen_t>&); //!< change rEntries-> rmEntries or cEntries -> rcEntries

    //various algebraic operations
    MatrixEntry& operator*=(const real_t&);         //!< MatrixEntry *= r
    MatrixEntry& operator*=(const complex_t&);      //!< MatrixEntry *= c
    MatrixEntry& operator/=(const real_t&);         //!< MatrixEntry /= r
    MatrixEntry& operator/=(const complex_t&);      //!< MatrixEntry /= c
    MatrixEntry& operator+=(const MatrixEntry&);    //!< MatrixEntry += MatrixEntry
    MatrixEntry& operator-=(const MatrixEntry&);    //!< MatrixEntry -= MatrixEntry
    void add(const MatrixEntry&, const std::vector<number_t>&,
             const std::vector<number_t>&, complex_t);  //!< add MatrixEntry to current MatrixEntry with scalar multiplication
    template<typename iterator>
    void add(iterator&, iterator&);                 //!< add entries to current MatrixEntry
    void addColToCol(number_t c1, number_t c2, complex_t a, bool updateStorage = false); //!< combine cols of matrix: c2 = c2 + a *c1 (indices start from 1)
    void addRowToRow(number_t r1, number_t r2, complex_t a, bool updateStorage = false); //!< combine rows of matrix: r2 = r2 + a *r1 (indices start from 1)
    void copyVal(const MatrixEntry&, const std::vector<number_t>&,const std::vector<number_t>&); //!< copy in current matrix, values of mat located at mat_adrs at position cur_adrs
    void assign(const MatrixEntry&, const std::vector<int_t>&, const std::vector<int_t>&); //!< assign entries to current MatrixEntry

    //factorization tools
    void luFactorize(bool withPermutation=true);         //!< factorize matrix entry as LU
    void ldltFactorize();                                //!< factorize matrix entry as LDLt
    void ldlstarFactorize();                             //!< factorize matrix entry as LDL*
    void iluFactorize();                                 //!< factorize matrix entry as ILU
    void ildltFactorize();                               //!< factorize matrix entry as ILDLt
    void illtFactorize();                                //!< factorize matrix entry as ILLt
    void ildlstarFactorize();                            //!< factorize matrix entry as ILDL*
    void illstarFactorize();                             //!< factorize matrix entry as ILL*
    void umfpackFactorize();                             //!< factorize matrix using umfpack

    void ldltSolve(const VectorEntry&, VectorEntry&);    //!< solve linear system using LDLt factorization
    void ldlstarSolve(const VectorEntry&, VectorEntry&); //!< solve linear system using LDL* factorization
    void luSolve(const VectorEntry&, VectorEntry&);      //!< solve linear system using LU factorization
    void lltSolve(const VectorEntry&, VectorEntry&);     //!< solve linear system using LLt factorization
    void umfluSolve(const VectorEntry&, VectorEntry&);   //!< solve linear system using umfpack LU factorization
    void luLeftSolve(const VectorEntry&, VectorEntry&);    //!< solve transposed linear system using LU factorization
    void umfluLeftSolve(const VectorEntry&, VectorEntry&); //!< solve transposed linear system using umfpack LU factorization

    void sorDiagonalSolve(const VectorEntry&,  VectorEntry&, const real_t); //!< solve linear system using SOR diagonal solver
    void sorDiagonalMatrixVector(const VectorEntry&,  VectorEntry&, const real_t); //!< solve linear system using SOR block diagonal solver
    void sorUpperSolve(const VectorEntry&,  VectorEntry&, const real_t); //!< solve linear system using SOR upper part solver
    void sorLowerSolve(const VectorEntry&,  VectorEntry&, const real_t); //!< solve linear system using SOR lower part solver

    //@{
    //! eigen solvers
    std::vector<std::pair<complex_t, VectorEntry* > > eigenSolve(EigenComputationalMode, number_t nev, real_t tol, string_t which,
                                                                 complex_t sigma = complex_t(), bool isRealShift = true,
                                                                 bool isShift=false);
    std::vector<std::pair<complex_t, VectorEntry* > > eigenSolve(const MatrixEntry* pB, EigenComputationalMode, number_t nev, real_t tol,
                                                                 string_t which, complex_t sigma = complex_t(), bool isRealShift = true,
                                                                 bool isShift=false);
    //@}
    //@{
    //! arpack solver
    std::vector<std::pair<complex_t, VectorEntry* > > arpackSolve(number_t nev, real_t tol, string_t which,
                                                                  EigenComputationalMode eigCompMode, complex_t sigma, const char mode,
                                                                  bool isRealShift);
    std::vector<std::pair<complex_t, VectorEntry* > > arpackSolve(const MatrixEntry* pB, number_t nev, real_t tol, string_t which,
                                                                  EigenComputationalMode eigCompMode, complex_t sigma, const char mode,
                                                                  bool isRealShift);
    //@}

    //special operation
    real_t partialNormOfCol(number_t c, number_t r1, number_t r2) const;  //!< partial norm of column c, only components in range [r1,r2]
    void deleteRows(number_t, number_t);                      //!< delete rows r1,..., r2 in matrix
    void deleteCols(number_t, number_t);                      //!< delete cols c1,..., c2 in matrix

    friend MatrixEntry operator*(const MatrixEntry&,const MatrixEntry&);

    real_t norm2() const;                                     //!< Frobenius norm
    real_t norminfty() const;                                 //!< infinite norm

    //in/out utilities
    void print(std::ostream&) const;                //!< print entry on stream
    void printHeader(std::ostream&) const;
    void viewStorage(std::ostream&) const;          //!< print storage on stream
    void saveToFile(const string_t& fn, StorageType st, number_t prec=fullPrec, bool encode=false, real_t tol=theTolerance) const; //!< save matrix entries to dense or sparse Matlab format (_coo)
    void saveToFile(const string_t& fn, StorageType st, bool encode, real_t tol=theTolerance) const //! save matrix entries to dense or sparse Matlab format (_coo)
    { saveToFile(fn, st, fullPrec, encode, tol); }

  private:
    void copy(const MatrixEntry&, bool storageCopy=false);    //!<internal copy tool
};

MatrixEntry operator*(const MatrixEntry&,const MatrixEntry&); //!< product of MatrixEntry
std::ostream& operator<<(std::ostream&, const MatrixEntry&);     //!< print on ostream

//algebraic operation and solver
MatrixEntry conj(const MatrixEntry &);                           //!< return the conjugate of a matrix entry
VectorEntry operator*(const MatrixEntry&, const VectorEntry&);   //!< matrix * vector
VectorEntry operator*(const VectorEntry&, const MatrixEntry&);   //!< vector * matrix
void multMatrixVector(const MatrixEntry&, const VectorEntry&, VectorEntry&);   //!< matrix * vector
void multVectorMatrix(const VectorEntry&, const MatrixEntry&, VectorEntry&);   //!< vector * matrix
void multVectorMatrix(const MatrixEntry&, const VectorEntry&, VectorEntry&);   //!< vector * matrix
void QR(const MatrixEntry&, MatrixEntry&, bool, MatrixEntry&, VectorEntry*, bool&, std::vector<number_t>*&, number_t&, real_t =100*theEpsilon);   //!< QR factorisation
void QRSolve(const MatrixEntry&, MatrixEntry*, VectorEntry*);         //!< QR solver      (only in scalar representation)
void gaussSolve(MatrixEntry&, VectorEntry&, VectorEntry&);            //!< Gauss solver   (only in scalar representation)
void umfpackSolve(MatrixEntry&, VectorEntry&, VectorEntry&, real_t&); //!< umfpack solver (only in scalar representation)
void umfpackSolve(MatrixEntry&, std::vector<VectorEntry*>&, std::vector<VectorEntry*>&, real_t&); //!< umfpack solver with multiple right hand side (only in scalar representation)
void factorize(MatrixEntry&, MatrixEntry&, FactorizationType ft=_noFactorization,
               bool withPermutation=true);                                         //!< factorize matrix as LU or LDLt or LDL*
void factorize(MatrixEntry&, FactorizationType ft=_noFactorization,
               bool withPermutation=true);                                         //!< factorize matrix as LU or LDLt or LDL*
void iFactorize(MatrixEntry&, MatrixEntry&, FactorizationType ft=_noFactorization);//!< factorize matrix as iLU (later or iLDLt or iLDL*)
void iFactorize(MatrixEntry&, FactorizationType ft=_noFactorization);              //!< factorize matrix as iLU (later or iLDLt or iLDL*)
VectorEntry factSolve(MatrixEntry&, const VectorEntry&);                           //!< solve linear system after factorization method
VectorEntry factLeftSolve(MatrixEntry&, const VectorEntry&);                     //!< solve transposed linear system after factorization method
//---------------------------------------------------------------------------------------------------
//  inline functions
//---------------------------------------------------------------------------------------------------
/*! set (i,j) entries, be care: unallocated pointers are not checked !
  these functions use pos(i,j) functions of storage, it may be expansive
  if onError is true then stops on indices out of storage
*/
inline void MatrixEntry::setEntry(number_t i, number_t j, const real_t & r, bool errorOn)
{(*rEntries_p)(i,j,errorOn)=r;}
inline void MatrixEntry::setEntry(number_t i, number_t j, const complex_t &c, bool errorOn)
{(*cEntries_p)(i,j,errorOn)=c;}
inline void MatrixEntry::setEntry(number_t i, number_t j, const Matrix<real_t> &rm, bool errorOn)
{(*rmEntries_p)(i,j,errorOn)=rm;}
inline void MatrixEntry::setEntry(number_t i, number_t j, const Matrix<complex_t> &cm, bool errorOn)
{(*cmEntries_p)(i,j,errorOn)=cm;}

/*! get (i,j) entries, these functions use pos(i,j) functions of storage */
inline real_t MatrixEntry::getEntry(number_t i, number_t j, real_t & r) const
{return r=rEntries_p->get(i,j);}
inline complex_t  MatrixEntry::getEntry(number_t i, number_t j, complex_t & c) const
{return c=cEntries_p->get(i,j);}
inline Matrix<real_t> MatrixEntry::getEntry(number_t i, number_t j, Matrix<real_t> &rm) const
{return rm=rmEntries_p->get(i,j);}
inline Matrix<complex_t> MatrixEntry::getEntry(number_t i, number_t j, Matrix<complex_t> &cm) const
{return cm=cmEntries_p->get(i,j);}

} //end of namespace xlifepp

#endif // MATRIX_ENTRY_HPP
