/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file ComplexEigenSolver.hpp
  \author Manh Ha NGUYEN
  \since 20 June 2013
  \date  20 June 2013

  \brief Direct eigen solver for complex non-symmetric (dense) matrix
*/

// This file is adapted from Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2009 Claire Maurice
// Copyright (C) 2009 Gael Guennebaud <gael.guennebaud@inria.fr>
// Copyright (C) 2010 Jitse Niesen <jitse@maths.leeds.ac.uk>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_COMPLEX_EIGEN_SOLVER_HPP
#define EIGEN_COMPLEX_EIGEN_SOLVER_HPP

#include "./decomposition/ComplexSchur.hpp"

namespace xlifepp {

namespace internalEigenSolver
{
template<typename MatrixType>
void doComputeEigenvectorsComplexSolverInPlace(const real_t matrixnorm, const MatrixType& schurT, const MatrixType& schurU, MatrixType& eigVec);
}

/*!
  \class ComplexEigenSolver
  
  \brief Computes eigenvalues and eigenvectors of general complex matrices
  
  \tparam _MatrixType the type of the matrix of which we are
  computing the eigendecomposition; this is expected to be an
  instantiation of the Matrix class template.
  
  The eigenvalues and eigenvectors of a matrix \f$ A \f$ are scalars
  \f$ \lambda \f$ and vectors \f$ v \f$ such that \f$ Av = \lambda v
  \f$.  If \f$ D \f$ is a diagonal matrix with the eigenvalues on
  the diagonal, and \f$ V \f$ is a matrix with the eigenvectors as
  its columns, then \f$ A V = V D \f$. The matrix \f$ V \f$ is
  almost always invertible, in which case we have \f$ A = V D V^{-1}
  \f$. This is called the eigendecomposition.
  
  The main function in this class is compute(), which computes the
  eigenvalues and eigenvectors of a given function. The
  documentation for that function contains an example showing the
  main features of the class.
  
  \sa class EigenSolver, class SelfAdjointEigenSolver
*/
template<typename _MatrixType> class ComplexEigenSolver
{
  public:

    //! \brief Synonym for the template parameter \p _MatrixType.
    typedef _MatrixType MatrixType;

//    enum {
//      RowsAtCompileTime = MatrixType::RowsAtCompileTime,
//      ColsAtCompileTime = MatrixType::ColsAtCompileTime,
//      Options = MatrixType::Options,
//      MaxRowsAtCompileTime = MatrixType::MaxRowsAtCompileTime,
//      MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime
//    };

    //! \brief Scalar type for matrices of type MatrixType.
    typedef typename MatrixType::Scalar Scalar;
    typedef typename NumTraits<Scalar>::RealScalar RealScalar;
    typedef typename NumTraits<Scalar>::ComplexScalar ComplexScalar;

    /*!
      \brief Type for vector of eigenvalues as returned by eigenvalues().
      
      This is a column vector with entries of type ComplexScalar.
      The length of the vector is the size of MatrixType.
    */
    typedef VectorEigenDense<ComplexScalar> EigenvalueType;

    /*!
      \brief Type for matrix of eigenvectors as returned by eigenvectors().
      
      This is a square matrix with entries of type ComplexScalar.
      The size is the same as the size of MatrixType.
    */
    typedef MatrixEigenDense<ComplexScalar> EigenvectorType;

    /*!
      \brief Default constructor.
      
      The default constructor is useful in cases in which the user intends to
      perform decompositions via compute().
    */
    ComplexEigenSolver()
    : eivec_(), eivalues_(), schur_(), isInitialized_(false), eigenvectorsOk_(false), matX_()
    {}

    /*!
      \brief Default Constructor with memory preallocation
      
      Like the default constructor but with preallocation of the internal data
      according to the specified problem \a size.
      \sa ComplexEigenSolver()
    */
    ComplexEigenSolver(Index size)
    : eivec_(size, size), eivalues_(size), schur_(size), isInitialized_(false), eigenvectorsOk_(false), matX_(size, size)
    {}

    /*!
      \brief Constructor; computes eigendecomposition of given matrix.
      
      \param[in]  matrix  Square matrix whose eigendecomposition is to be computed.
      \param[in]  computeEigenvectors  If true, both the eigenvectors and the
         eigenvalues are computed; if false, only the eigenvalues are
         computed.
      \param sorted
      
      This constructor calls compute() to compute the eigendecomposition.
    */
    ComplexEigenSolver(const MatrixType& matrix, bool computeEigenvectors = true, bool sorted = false)
    : eivec_(matrix.rows(), matrix.cols()), eivalues_(matrix.cols()), schur_(matrix.rows()), isInitialized_(false),
      eigenvectorsOk_(false), matX_(matrix.rows(),matrix.cols())
    {
      compute(matrix, computeEigenvectors, sorted);
    }

    /*!
      \brief Returns the eigenvectors of given matrix.
      
      \returns  A const reference to the matrix whose columns are the eigenvectors.
      
      \pre Either the constructor
      ComplexEigenSolver(const MatrixType& matrix, bool) or the member
      function compute(const MatrixType& matrix, bool) has been called before
      to compute the eigendecomposition of a matrix, and
      \p computeEigenvectors was set to true (the default).
      
      This function returns a matrix whose columns are the eigenvectors. Column
      \f$ k \f$ is an eigenvector corresponding to eigenvalue number \f$ k
      \f$ as returned by eigenvalues().  The eigenvectors are normalized to
      have (Euclidean) norm equal to one. The matrix returned by this
      function is the matrix \f$ V \f$ in the eigendecomposition \f$ A = V D
      V^{-1} \f$, if it exists.
      
      Example: \\include ComplexEigenSolver_eigenvectors.cpp
      Output: \\verbinclude ComplexEigenSolver_eigenvectors.out
    */
    const EigenvectorType& eigenvectors() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "ComplexEigenSolver"); }
      if (!eigenvectorsOk_) { error("eigenvectors_not_computed"); }
      return eivec_;
    }

    /*!
      \brief Returns the eigenvalues of given matrix.
      
      \returns A const reference to the column vector containing the eigenvalues.
      
      \pre Either the constructor
      ComplexEigenSolver(const MatrixType& matrix, bool) or the member
      function compute(const MatrixType& matrix, bool) has been called before
      to compute the eigendecomposition of a matrix.
      
      This function returns a column vector containing the
      eigenvalues. Eigenvalues are repeated according to their
      algebraic multiplicity, so there are as many eigenvalues as
      rows in the matrix. The eigenvalues are not sorted in any particular
      order.
      
      Example: \\include ComplexEigenSolver_eigenvalues.cpp
      Output: \\verbinclude ComplexEigenSolver_eigenvalues.out
    */
    const EigenvalueType& eigenvalues() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "ComplexEigenSolver"); }
      return eivalues_;
    }

    /*!
      \brief Computes eigendecomposition of given matrix.
      
      \param[in]  matrix  Square matrix whose eigendecomposition is to be computed.
      \param[in]  computeEigenvectors  If true, both the eigenvectors and the
         eigenvalues are computed; if false, only the eigenvalues are
         computed.
      \param sorted
      \returns    Reference to \c *this
      
      This function computes the eigenvalues of the complex matrix \p matrix.
      The eigenvalues() function can be used to retrieve them.  If
      \p computeEigenvectors is true, then the eigenvectors are also computed
      and can be retrieved by calling eigenvectors().
      
      The matrix is first reduced to Schur form using the
      ComplexSchur class. The Schur decomposition is then used to
      compute the eigenvalues and eigenvectors.
      
      The cost of the computation is dominated by the cost of the
      Schur decomposition, which is \f$ O(n^3) \f$ where \f$ n \f$
      is the size of the matrix.
      
      Example: \\include ComplexEigenSolver_compute.cpp
      Output: \\verbinclude ComplexEigenSolver_compute.out
    */
    ComplexEigenSolver& compute(const MatrixType& matrix, bool computeEigenvectors = true, bool sorted = false);

    /*!
      \brief Reports whether previous computation was successful.
      
      \returns \c _success if computation was succesful, \c NoConvergence otherwise.
    */
    ComputationInfo info() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "ComplexEigenSolver"); }
      return schur_.info();
    }

  protected:
    EigenvectorType eivec_; //!< type of eigen vector
    EigenvalueType eivalues_; //!< type of eigen value
    ComplexSchur<MatrixType> schur_; //!< schur matrix
    bool isInitialized_; //!< true if initialized
    bool eigenvectorsOk_; //!< true if computation of eigen vectors
    EigenvectorType matX_;

  private:
    //! computes eigen vectors
    void doComputeEigenvectors(const RealScalar matrixnorm);
    //! sorts eigen values
    void sortEigenvalues(bool computeEigenvectors);
};


template<typename MatrixType>
ComplexEigenSolver<MatrixType>& ComplexEigenSolver<MatrixType>::compute(const MatrixType& matrix, bool computeEigenvectors, bool sorted)
{
  // this code is inspired from Jampack
  if (matrix.numOfCols() != matrix.numOfRows()) { matrix.nonSquare("Computing real matrix", matrix.numOfRows(), matrix.numOfCols()); }

  // Do a complex Schur decomposition, A = U T U^*
  // The eigenvalues are on the diagonal of T.
  schur_.compute(matrix, computeEigenvectors);

  if( _success == schur_.info())
  {
    eivalues_ = schur_.matrixT().diagonal();
    if(computeEigenvectors) {
        matX_.reshape(matrix.numOfRows(),matrix.numOfCols());
        eivec_.reshape(matrix.numOfRows(),matrix.numOfCols());
        doComputeEigenvectors(matrix.normFrobenius());
    }
    if (sorted) sortEigenvalues(computeEigenvectors);
  }

  isInitialized_ = true;
  eigenvectorsOk_ = computeEigenvectors;
  return *this;
}


template<typename MatrixType>
void ComplexEigenSolver<MatrixType>::doComputeEigenvectors(const RealScalar matrixnorm)
{
  const Index n = eivalues_.size();

  // Compute X such that T = X D X^(-1), where D is the diagonal of T.
  // The matrix X is unit triangular.
  matX_.putScalar(NumTraits<ComplexScalar>::zero());

  for(Index k=n-1 ; k>=0 ; k--)
  {
    matX_.coeffRef(k,k) = ComplexScalar(1.0,0.0);
    // Compute X(i,k) using the (i,k) entry of the equation X T = D X
    for(Index i=k-1 ; i>=0 ; i--)
    {
      matX_.coeffRef(i,k) = schur_.matrixT().coeff(i,k); //-schur_.matrixT().coeff(i,k);
      if(k-i-1>0) {
        //matX_.coeffRef(i,k) -= (schur_.matrixT().row(i).segment(i+1,k-i-1) * matX_.col(k).segment(i+1,k-i-1)).value();
        EigenvalueType tmpCol = matX_.blockCol(i+1,k,k-i-1);
        EigenvalueType tmpRow = schur_.matrixT().blockRow(i,i+1,k-i-1);
        ComplexScalar value(NumTraits<ComplexScalar>::zero());
        for (Index j = 0; j < k-i-1;j++) value += tmpCol[j]*tmpRow[j];
        matX_.coeffRef(i,k) += value;
      }
      //ComplexScalar z = schur_.matrixT().coeff(i,i) - schur_.matrixT().coeff(k,k);
      ComplexScalar z = eivalues_[k] - eivalues_[i];
      if(z == ComplexScalar(0))
      {
        // If the i-th and k-th eigenvalue are equal, then z equals 0.
        // Use a small value instead, to prevent division by zero.
        //internal::real_ref(z) = NumTraits<RealScalar>::epsilon() * matrixnorm;
//      z.real() = NumTraits<RealScalar>::epsilon() * matrixnorm;
        z=complex_t(NumTraits<RealScalar>::epsilon() * matrixnorm,z.imag());
      }
      matX_.coeffRef(i,k) = matX_.coeff(i,k) / z;
    }
  }

  // Compute V as V = U X; now A = U T U^* = U X D X^(-1) U^* = V D V^(-1)
  //eivec_.noalias() = schur_.matrixU() * matX_;
  multMatMat(schur_.matrixU(), matX_, eivec_);

  // .. and normalize the eigenvectors
  EigenvalueType tmpCol;
  for(Index k=0 ; k<n ; k++)
  {
    //eivec_.col(k).normalize();
    tmpCol = eivec_.columnVector(k);
    tmpCol.normalize();
    eivec_.columnVector(k,tmpCol);
  }
}

template<typename MatrixType>
void ComplexEigenSolver<MatrixType>::sortEigenvalues(bool computeEigenvectors)
{
  const Index n =  eivalues_.size();
  for (Index i=0; i<n; i++)
  {
    Index k = i;
    //eivalues_.cwiseAbs().tail(n-i).minCoeff(&k);
    for(Index j = i; j < n; j++) { if (std::abs(eivalues_[j]) < std::abs(eivalues_[k])) k = j; }

    if (k != 0)
    {
      //k += i;
      std::swap(eivalues_[k],eivalues_[i]);
      if(computeEigenvectors) eivec_.swapCols(i,k);
    }
  }
}

namespace internalEigenSolver
{
template<typename MatrixType>
void doComputeEigenvectorsComplexSolverInPlace(const real_t matrixnorm, const MatrixType& schurT, const MatrixType& schurU, MatrixType& eigVec)
{
  VectorEigenDense<complex_t> eigvalues = schurT.diagonal();
  const Index n = eigvalues.size();

  MatrixType matX(schurT.numOfRows());

  // Compute X such that T = X D X^(-1), where D is the diagonal of T.
  // The matrix X is unit triangular.
  //matX.putScalar(NumTraits<ComplexScalar>::zero());

  for(Index k=n-1 ; k>=0 ; k--)
  {
    matX.coeffRef(k,k) = complex_t(1.0,0.0);
    // Compute X(i,k) using the (i,k) entry of the equation X T = D X
    for(Index i=k-1 ; i>=0 ; i--)
    {
      matX.coeffRef(i,k) = schurT.coeff(i,k); //-schurT.coeff(i,k);
      if(k-i-1>0) {
        //matX.coeffRef(i,k) -= (schurT.row(i).segment(i+1,k-i-1) * matX.col(k).segment(i+1,k-i-1)).value();
        VectorEigenDense<complex_t> tmpCol = matX.blockCol(i+1,k,k-i-1);
        VectorEigenDense<complex_t> tmpRow = schurT.blockRow(i,i+1,k-i-1);
        complex_t value(NumTraits<complex_t>::zero());
        for (Index j = 0; j < k-i-1;j++) value += tmpCol[j]*tmpRow[j];
        matX.coeffRef(i,k) += value;
      }
      //complex_t z = schurT.coeff(i,i) - schurT.coeff(k,k);
      complex_t z = eigvalues[k] - eigvalues[i];
      if(z == complex_t(0))
      {
        // If the i-th and k-th eigenvalue are equal, then z equals 0.
        // Use a small value instead, to prevent division by zero.
        //internal::real_ref(z) = NumTraits<RealScalar>::epsilon() * matrixnorm;
        // z.real() = NumTraits<RealScalar>::epsilon() * matrixnorm;
        z=complex_t(NumTraits<real_t>::epsilon() * matrixnorm,0);
      }
      matX.coeffRef(i,k) = matX.coeff(i,k) / z;
    }
  }

  // Compute V as V = U X; now A = U T U^* = U X D X^(-1) U^* = V D V^(-1)
  //eigVec.noalias() = schur_.matrixU() * matX;
  multMatMat(schurU, matX, eigVec);

  // .. and normalize the eigenvectors
  VectorEigenDense<complex_t> tmpCol;
  for(Index k=0 ; k<n ; k++)
  {
    //eigVec.col(k).normalize();
    tmpCol = eigVec.columnVector(k);
    tmpCol.normalize();
    eigVec.columnVector(k,tmpCol);
  }
}

} // end of namespace internalEigenSolver

} // end namespace xlifepp

#endif // EIGEN_COMPLEX_EIGEN_SOLVER_HPP
