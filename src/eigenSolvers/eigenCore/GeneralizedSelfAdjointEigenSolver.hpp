/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file GeneralizedSelfAdjointEigenSolver.hpp
  \author Manh Ha NGUYEN
  \since 22 Jan 2013
  \date  7 Mars 2013

  \brief Direct eigen solver for generalized selfadjoint (dense) matrix
*/

// This file is adapted from Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2009 Claire Maurice
// Copyright (C) 2009 Gael Guennebaud <gael.guennebaud@inria.fr>
// Copyright (C) 2010 Jitse Niesen <jitse@maths.leeds.ac.uk>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_GENERALIZED_SELFADJOINT_EIGENSOLVER_HPP
#define EIGEN_GENERALIZED_SELFADJOINT_EIGENSOLVER_HPP

#include "./decomposition/Tridiagonalization.hpp"

namespace xlifepp
{

/*!
  \class GeneralizedSelfAdjointEigenSolver
  
  \brief Computes eigenvalues and eigenvectors of the generalized selfadjoint eigen problem
  
  \tparam _MatrixType the type of the matrix of which we are computing the
  eigendecomposition; this is expected to be an instantiation of the Matrix
  class template.
  
  This class solves the generalized eigenvalue problem
  \f$ Av = \lambda Bv \f$. In this case, the matrix \f$ A \f$ should be
  selfadjoint and the matrix \f$ B \f$ should be positive definite.
  
  Only the \b lower \b triangular \b part of the input matrix is referenced.
  
  Call the function compute() to compute the eigenvalues and eigenvectors of
  a given matrix. Alternatively, you can use the
  GeneralizedSelfAdjointEigenSolver(const MatrixType&, const MatrixType&, int)
  constructor which computes the eigenvalues and eigenvectors at construction time.
  Once the eigenvalue and eigenvectors are computed, they can be retrieved with the eigenvalues()
  and eigenvectors() functions.
  
  The documentation for GeneralizedSelfAdjointEigenSolver(const MatrixType&, const MatrixType&, int)
  contains an example of the typical use of this class.
  
  \sa class SelfAdjointEigenSolver, class EigenSolver
*/
template<typename _MatrixType>
class GeneralizedSelfAdjointEigenSolver : public SelfAdjointEigenSolver<_MatrixType>
{
    typedef SelfAdjointEigenSolver<_MatrixType> Base;
  public:
    typedef _MatrixType MatrixType;
    
    /*! \brief Default constructor for fixed-size matrices.
      
      The default constructor is useful in cases in which the user intends to
      perform decompositions via compute().
    */
    GeneralizedSelfAdjointEigenSolver() : Base() {}
    
    /*! \brief Constructor, pre-allocates memory for dynamic-size matrices.
      
      \param[in]  size  Positive integer, size of the matrix whose
      eigenvalues and eigenvectors will be computed.
      
      \sa compute() for an example
    */
    GeneralizedSelfAdjointEigenSolver(Index size)
      : Base(size)
    {}
    
    /*! \brief Constructor; computes generalized eigendecomposition of given matrix pencil.
      
      \param[in]  matA  Selfadjoint matrix in matrix pencil.
                        Only the lower triangular part of the matrix is referenced.
      \param[in]  matB  Positive-definite matrix in matrix pencil.
                        Only the lower triangular part of the matrix is referenced.
      \param[in]  options A or-ed set of flags {ComputeEigenvectors,EigenvaluesOnly} | {Ax_lBx,ABx_lx,BAx_lx}.
                          Default is ComputeEigenvectors|Ax_lBx.
      
      This constructor calls compute(const MatrixType&, const MatrixType&, int)
      to compute the eigenvalues and (if requested) the eigenvectors of the
      generalized eigenproblem \f$ Ax = \lambda B x \f$ with \a matA the
      selfadjoint matrix \f$ A \f$ and \a matB the positive definite matrix
      \f$ B \f$. Each eigenvector \f$ x \f$ satisfies the property
      \f$ x^* B x = 1 \f$. The eigenvectors are computed if
      \a options contains ComputeEigenvectors.
      
      In addition, the two following variants can be solved via \p options:
        - \c ABx_lx: \f$ ABx = \lambda x \f$
        - \c BAx_lx: \f$ BAx = \lambda x \f$
      
      \sa compute(const MatrixType&, const MatrixType&, int)
    */
    GeneralizedSelfAdjointEigenSolver(const MatrixType& matA, const MatrixType& matB,
                                      int options = _computeEigenVector | _Ax_lBx)
      : Base(matA.numOfCols())
    {
      compute(matA, matB, options);
    }
    
    /*! \brief Computes generalized eigendecomposition of given matrix pencil.
      
      \param[in]  matA  Selfadjoint matrix in matrix pencil.
                        Only the lower triangular part of the matrix is referenced.
      \param[in]  matB  Positive-definite matrix in matrix pencil.
                        Only the lower triangular part of the matrix is referenced.
      \param[in]  options A or-ed set of flags {#_computeEigenVector,#_eigenValueOnly} | {#_Ax_lBx,#_ABx_lx,#_BAx_lx}.
                          Default is #_computeEigenVector|#_Ax_lBx.
      
      \returns    Reference to \c *this
      
      Accoring to \p options, this function computes eigenvalues and (if requested)
      the eigenvectors of one of the following three generalized eigenproblems:
        - \c Ax_lBx: \f$ Ax = \lambda B x \f$
        - \c ABx_lx: \f$ ABx = \lambda x \f$
        - \c BAx_lx: \f$ BAx = \lambda x \f$
      with \a matA the selfadjoint matrix \f$ A \f$ and \a matB the positive definite
      matrix \f$ B \f$.
      In addition, each eigenvector \f$ x \f$ satisfies the property \f$ x^* B x = 1 \f$.
      
      The eigenvalues() function can be used to retrieve
      the eigenvalues. If \p options contains _computeEigenVector, then the
      eigenvectors are also computed and can be retrieved by calling
      eigenvectors().
      
      The implementation uses LLT to compute the Cholesky decomposition
      \f$ B = LL^* \f$ and computes the classical eigendecomposition
      of the selfadjoint matrix \f$ L^{-1} A (L^*)^{-1} \f$ if \p options contains _Ax_lBx
      and of \f$ L^{*} A L \f$ otherwise. This solves the
      generalized eigenproblem, because any solution of the generalized
      eigenproblem \f$ Ax = \lambda B x \f$ corresponds to a solution
      \f$ L^{-1} A (L^*)^{-1} (L^* x) = \lambda (L^* x) \f$ of the
      eigenproblem for \f$ L^{-1} A (L^*)^{-1} \f$. Similar statements
      can be made for the two other variants.
      
      \sa GeneralizedSelfAdjointEigenSolver(const MatrixType&, const MatrixType&, int)
    */
    GeneralizedSelfAdjointEigenSolver& compute(const MatrixType& matA, const MatrixType& matB,
        int options = _computeEigenVector | _Ax_lBx);
};


template<typename MatrixType>
GeneralizedSelfAdjointEigenSolver<MatrixType>& GeneralizedSelfAdjointEigenSolver<MatrixType>::
compute(const MatrixType& matA, const MatrixType& matB, int options)
{
  trace_p->push("GeneralizedSelfAdjointEigenSolver::compute");
  if (matA.numOfCols() != matA.numOfRows()) { matA.nonSquare("Computing generalized selfadjoint matrix", matA.numOfRows(), matA.numOfCols()); }
  if (matB.numOfCols() != matB.numOfRows()) { matB.nonSquare("Computing generalized selfadjoint matrix", matB.numOfRows(), matB.numOfCols()); }
  if (matA.numOfCols() != matB.numOfCols()) { matA.mismatchDims("Computing generalized selfadjoint matrix", matA.numOfCols(), matB.numOfCols()); }
  bool test=(options&~(_eigVecMask | _genEigMask)) == 0 && (options & _eigVecMask) != _eigVecMask
             && ((options & _genEigMask) == 0 || (options & _genEigMask) == _Ax_lBx
             || (options & _genEigMask) == _ABx_lx || (options & _genEigMask) == _BAx_lx);
  if (!test) { error("invalid_option"); }
                                    
  bool computeEigVecs = ((options & _eigVecMask) == 0) || ((options & _eigVecMask) == _computeEigenVector);
  
  // Compute the cholesky decomposition of matB = L L' = U'U
  MatrixType cholB = matB.cholesky();
  
  int type = (options & _genEigMask);
  if(type == 0)
  { type = _Ax_lBx; }
  
  if(type == _Ax_lBx)
  {
    // compute C = inv(L) A inv(L')
    MatrixType matC(matA.numOfRows(), matA.numOfCols());
    cholB.solveCholeskyInplaceLower(matA, matC);
    cholB.solveCholeskyInplaceLower(adjoint(matC), matC);
    
    Base::compute(matC, computeEigVecs ? _computeEigenVector : _eigenValueOnly );
    
    // transform back the eigen vectors: evecs = inv(U) * evecs
    if(computeEigVecs)
    { cholB.solveCholeskyInplaceUpper(Base::eivec_, Base::eivec_); }
  } //TODO In the future, we will improve these functions
  else if(type == _ABx_lx)
  {
    // compute C = L' A L
    MatrixType matC(matA.numOfRows(), matA.numOfCols());
    MatrixType temp(matC);
    
    //MatrixType matC = matA.template selfadjointView<Lower>();
    //matC = matC * cholB.matrixL();
    //matC = cholB.matrixU() * matC;
    multMatMat(matA, cholB, temp);
    multMatMat(adjoint(cholB), temp, matC);
    
    Base::compute(matC, computeEigVecs ? _computeEigenVector : _eigenValueOnly );
    
    // transform back the eigen vectors: evecs = inv(U) * evecs
    if(computeEigVecs)
      //cholB.matrixU().solveInPlace(Base::m_eivec);
    { cholB.solveCholeskyInplaceUpper(Base::eivec_, Base::eivec_); }
  }
  else if(type == _BAx_lx)
  {
    // compute C = L' A L
    //    MatrixType matC = matA.template selfadjointView<Lower>();
    //    matC = matC * cholB.matrixL();
    //    matC = cholB.matrixU() * matC;
    MatrixType matC(matA.numOfRows(), matA.numOfCols());
    MatrixType temp(matC);
    multMatMat(matA, cholB, temp);
    multMatMat(adjoint(cholB), temp, matC);
    
    Base::compute(matC, computeEigVecs ? _computeEigenVector : _eigenValueOnly );
    
    // transform back the eigen vectors: evecs = L * evecs
    if(computeEigVecs)
    {
      //      Base::m_eivec = cholB.matrixL() * Base::m_eivec;
      MatrixType temp2(matA.numOfRows(), matA.numOfCols());
      multMatMat(cholB, Base::eivec_, temp2);
      Base::eivec_ = temp2;
    }
  }
  
  trace_p->pop();
  return *this;
}

} // end namespace xlifepp

#endif // EIGEN_GENERALIZED_SELFADJOINT_EIGENSOLVER_HPP
