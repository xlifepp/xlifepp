/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file HouseHolderSequence.hpp
  \author Manh Ha NGUYEN
  \since 22 Jan 2013
  \date  7 Mars 2013

  \brief Definition of the xlifepp::HouseholderSequence class

  Class deals with householder algorithm
*/

// This file is adapted from Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2009 Claire Maurice
// Copyright (C) 2009 Gael Guennebaud <gael.guennebaud@inria.fr>
// Copyright (C) 2010 Jitse Niesen <jitse@maths.leeds.ac.uk>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_HOUSEHOLDER_SEQUENCE_HPP
#define EIGEN_HOUSEHOLDER_SEQUENCE_HPP

namespace xlifepp
{

/*!
  \class HouseholderSequence
  \brief Sequence of Householder reflections acting on subspaces with decreasing size
  \tparam VectorsType type of matrix containing the Householder vectors
  \tparam CoeffsType  type of vector containing the Householder coefficients
  \tparam Side        either OnTheLeft (the default) or OnTheRight
  
  This class represents a product sequence of Householder reflections where the first Householder reflection
  acts on the whole space, the second Householder reflection leaves the one-dimensional subspace spanned by
  the first unit vector invariant, the third Householder reflection leaves the two-dimensional subspace
  spanned by the first two unit vectors invariant, and so on up to the last reflection which leaves all but
  one dimensions invariant and acts only on the last dimension. Such sequences of Householder reflections
  are used in several algorithms to zero out certain parts of a matrix. Indeed, the methods
  HessenbergDecomposition::matrixQ(), Tridiagonalization::matrixQ(), HouseholderQR::householderQ(),
  and ColPivHouseholderQR::householderQ() all return a %HouseholderSequence.
  
  More precisely, the class %HouseholderSequence represents an \f$ n \times n \f$ matrix \f$ H \f$ of the
  form \f$ H = \prod_{i=0}^{n-1} H_i \f$ where the i-th Householder reflection is \f$ H_i = I - h_i v_i
  v_i^* \f$. The i-th Householder coefficient \f$ h_i \f$ is a scalar and the i-th Householder vector \f$
  v_i \f$ is a vector of the form
  \f[
  v_i = [\underbrace{0, \ldots, 0}_{i-1\mbox{ zeros}}, 1, \underbrace{*, \ldots,*}_{n-i\mbox{ arbitrary entries}} ].
  \f]
  The last \f$ n-i \f$ entries of \f$ v_i \f$ are called the essential part of the Householder vector.
  
  Typical usages are listed below, where H is a HouseholderSequence:
  \code
  A.applyOnTheRight(H);             // A = A * H
  A.applyOnTheLeft(H);              // A = H * A
  A.applyOnTheRight(H.adjoint());   // A = A * H^*
  A.applyOnTheLeft(H.adjoint());    // A = H^* * A
  MatrixXd Q = H;                   // conversion to a dense matrix
  \endcode
  In addition to the adjoint, you can also apply the inverse (=adjoint), the transpose, and the conjugate operators.
  */
template<class MatrixType, class VectorType>
class HouseholderSequence
{
  public:
    typedef typename MatrixType::type_t Scalar;
    typedef MatrixType VectorsType;
    typedef VectorType EssentialVectorType;
    typedef VectorType CoeffsType;
    typedef HouseholderSequence<MatrixType, VectorType> ConjugateReturnType;
    
  public:
    /*!
      \brief Constructor.
      \param[in]  v      %Matrix containing the essential parts of the Householder vectors
      \param[in]  h      Vector containing the Householder coefficients
      
      Constructs the Householder sequence with coefficients given by \p h and vectors given by \p v. The
      i-th Householder coefficient \f$ h_i \f$ is given by \p h(i) and the essential part of the i-th
      Householder vector \f$ v_i \f$ is given by \p v(k,i) with \p k > \p i (the subdiagonal part of the
      i-th column). If \p v has fewer columns than rows, then the Householder sequence contains as many
      Householder reflections as there are columns.
      
      \note The %HouseholderSequence object stores \p v and \p h by reference.
      \sa setLength(), setShift()
      */
    HouseholderSequence(const VectorsType& v, const CoeffsType& h)
      : vectors_(v), coeffs_(h), trans_(false), length_(v.numOfCols()),
        shift_(0)
    {
    }
    
    /*! \brief Copy constructor. */
    HouseholderSequence(const HouseholderSequence& other)
      : vectors_(other.vectors_),
        coeffs_(other.coeffs_),
        trans_(other.trans_),
        length_(other.length_),
        shift_(other.shift_)
    {
    }
    
    /*!
      \brief Number of rows of transformation viewed as a matrix.
      \returns Number of rows
      \details This equals the dimension of the space that the transformation acts on.
      */
    Index rows() const { return vectors_.numOfRows(); }
    
    /*!
      \brief Number of columns of transformation viewed as a matrix.
      \returns Number of columns
      \details This equals the dimension of the space that the transformation acts on.
      */
    Index cols() const { return vectors_.numOfCols(); }
    
    /*!
      \brief Essential part of a Householder vector.
      \param[in]  k  Index of Householder reflection
      \returns    Vector containing non-trivial entries of k-th Householder vector
      
      This function returns the essential part of the Householder vector \f$ v_i \f$. This is a vector of
      length \f$ n-i \f$ containing the last \f$ n-i \f$ entries of the vector
      \f[
      v_i = [\underbrace{0, \ldots, 0}_{i-1\mbox{ zeros}}, 1, \underbrace{*, \ldots,*}_{n-i\mbox{ arbitrary entries}} ].
      \f]
      The index \f$ i \f$ equals \p k + shift(), corresponding to the k-th column of the matrix \p v
      passed to the constructor.
      
      \sa setShift(), shift()
      */
    const EssentialVectorType essentialVector(Index k) const
    {
      if ((k < 0) || (k >= length_)) { vectors_.indexOutOfRange("index of essential vector", k, length_); }
      Index start = k + 1 + shift_;
      VectorType temp = vectors_.columnVector(k);
      return temp.tail(rows() - start);
    }
    
    //! \brief %Transpose of the Householder sequence.
    HouseholderSequence transpose() const
    {
      return HouseholderSequence(*this).setTrans(!trans_);
    }
    
    //! \brief Complex conjugate of the Householder sequence.
    ConjugateReturnType conjugate() const
    {
      return ConjugateReturnType(vectors_, coeffs_.conjugate())
             .setTrans(trans_)
             .setLength(length_)
             .setShift(shift_);
    }
    
    //! \brief Adjoint (conjugate transpose) of the Householder sequence.
    ConjugateReturnType adjoint() const
    {
      return conjugate().setTrans(!trans_);
    }
    
    //! \brief Inverse of the Householder sequence (equals the adjoint).
    ConjugateReturnType inverse() const { return adjoint(); }
    
    MatrixType unitaryMatrix()
    {
      Index vecs = length_;
      MatrixType& dst = vectors_;
      
      dst.diagonal(NumTraits<Scalar>::one());
      dst.setZeroStrictUpper();
      for(int_t k = vecs - 1; k >= 0; --k)
      {
        Index cornerSize = rows() - k - shift_;
        MatrixType cornerMat;
        if(trans_)
        {
          //              dst.bottomRightCorner(cornerSize, cornerSize)
          //                    .applyHouseholderOnTheRight(essentialVector(k), coeffs_.coeff(k), workspace.data());
          cornerMat = dst.bottomRightCorner(cornerSize, cornerSize);
          cornerMat.applyHouseholderOnTheRight(essentialVector(k), coeffs_.coeff(k));
          dst.bottomRightCorner(cornerSize, cornerSize, cornerMat);
          
        }
        else
        {
          cornerMat = dst.bottomRightCorner(cornerSize, cornerSize);
          cornerMat.applyHouseholderOnTheLeft(essentialVector(k), coeffs_.coeff(k));
          dst.bottomRightCorner(cornerSize, cornerSize, cornerMat);
        }
        
        // clear the off diagonal vector
        //dst.col(k).tail(rows()-k-1).setZero();
        // Not smart code but it works!!!
        VectorType colVec = dst.columnVector(k);
        VectorType tail = colVec.tail(rows() - k - 1);
        tail.setZero();
        colVec.tail(rows() - k - 1, tail);
        dst.columnVector(k, colVec);
      }
      
      // clear the remaining columns if needed
      for(Index k = 0; k < cols() - vecs ; ++k)
      {
        //dst.col(k).tail(rows()-k-1).setZero();
        VectorType colVec = dst.columnVector(k);
        VectorType tail = colVec.tail(rows() - k - 1);
        tail.setZero();
        colVec.tail(rows() - k - 1, tail);
        dst.columnVector(k, colVec);
      }
      
      return dst;
    }
    
    MatrixType matrixQ()
    {
      //Index vecs = length_;
      Index vecs = coeffs_.size();
      MatrixType dst(rows(),rows());
      eyeMatrix(dst);

      for(int_t k = vecs - 1; k >= 0; --k)
      {
        Index cornerSize = rows() - k - shift_;
        MatrixType cornerMat;
        if(trans_) {
          //              dst.bottomRightCorner(cornerSize, cornerSize)
          //                    .applyHouseholderOnTheRight(essentialVector(k), coeffs_.coeff(k), workspace.data());
          cornerMat = dst.bottomRightCorner(cornerSize, cornerSize);
          cornerMat.applyHouseholderOnTheRight(essentialVector(k), coeffs_.coeff(k));
          dst.bottomRightCorner(cornerSize, cornerSize, cornerMat);

        } else {
          cornerMat = dst.bottomRightCorner(cornerSize, cornerSize);
          cornerMat.applyHouseholderOnTheLeft(essentialVector(k), coeffs_.coeff(k));
          dst.bottomRightCorner(cornerSize, cornerSize, cornerMat);
        }
      }
//        for(Index k = vecs-1; k >= 0; --k)
//        {
//          Index cornerSize = rows() - k - m_shift;
//          if(m_trans)
//            dst.bottomRightCorner(cornerSize, cornerSize)
//               .applyHouseholderOnTheRight(essentialVector(k), m_coeffs.coeff(k), &workspace.coeffRef(0));
//          else
//            dst.bottomRightCorner(cornerSize, cornerSize)
//               .applyHouseholderOnTheLeft(essentialVector(k), m_coeffs.coeff(k), &workspace.coeffRef(0));
//        }
        return dst;
    }



    /*!
      \brief Sets the length of the Householder sequence.
      \param [in]  length  New value for the length.
      
      By default, the length \f$ n \f$ of the Householder sequence \f$ H = H_0 H_1 \ldots H_{n-1} \f$ is set
      to the number of columns of the matrix \p v passed to the constructor, or the number of rows if that
      is smaller. After this function is called, the length equals \p length.
      
      \sa length()
    */
    HouseholderSequence& setLength(Index length)
    {
      length_ = length;
      return *this;
    }
    
    /*! \brief Sets the shift of the Householder sequence.
      \param [in]  shift  New value for the shift.
      
      By default, a %HouseholderSequence object represents \f$ H = H_0 H_1 \ldots H_{n-1} \f$ and the i-th
      column of the matrix \p v passed to the constructor corresponds to the i-th Householder
      reflection. After this function is called, the object represents 
      \f$ H = H_{\mathrm{shift}} * H_{\mathrm{shift}+1} \ldots H_{n-1} \f$ and the i-th column of \p
      v corresponds to the (shift+i)-th
      Householder reflection.
      
      \sa shift()
      */
    HouseholderSequence& setShift(Index shift)
    {
      shift_ = shift;
      return *this;
    }
    
    Index length() const { return length_; }  /*!< \brief Returns the length of the Householder sequence. */
    Index shift() const { return shift_; }    /*!< \brief Returns the shift of the Householder sequence. */
    
  protected:
  
    /*! \brief Sets the transpose flag.
      \param[in]  trans  New value of the transpose flag.
      
      By default, the transpose flag is not set. If the transpose flag is set, then this object represents
      \f$ H^T = H_{n-1}^T \ldots H_1^T H_0^T \f$ instead of \f$ H = H_0 H_1 \ldots H_{n-1} \f$.
      
      \sa trans()
      */
    HouseholderSequence& setTrans(bool trans)
    {
      trans_ = trans;
      return *this;
    }
    
    //! \brief Returns the transpose flag.
    bool trans() const { return trans_; }
    
    VectorsType vectors_;
    CoeffsType coeffs_;
    bool trans_;
    Index length_;
    Index shift_;
};

} // end namespace xlifepp

#endif // EIGEN_HOUSEHOLDER_SEQUENCE_HPP
