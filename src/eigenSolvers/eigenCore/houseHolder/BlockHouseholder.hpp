/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file BlockHouseholder.hpp
  \author Manh Ha NGUYEN
  \since 22 Jan 2013
  \date  7 Mars 2013
  
  \brief Definition of functions delaing with block Householder algorithm
*/

// This file is adapted from Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2009 Claire Maurice
// Copyright (C) 2009 Gael Guennebaud <gael.guennebaud@inria.fr>
// Copyright (C) 2010 Jitse Niesen <jitse@maths.leeds.ac.uk>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_BLOCK_HOUSEHOLDER_HPP
#define EIGEN_BLOCK_HOUSEHOLDER_HPP

// This file contains some helper function to deal with block householder reflectors

#include "utils.h"

namespace xlifepp {

namespace internalEigenSolver {

template<typename TriangularFactorType,typename VectorsType,typename CoeffsType>
void makeBlockHouseholderTriangularFactor(TriangularFactorType& triFactor, VectorsType& vectors, const CoeffsType& hCoeffs)
{
  typedef typename VectorsType::type_t Scalar;
  const Index nbVecs = vectors.numOfCols();
  if (!(triFactor.numOfRows() == nbVecs && triFactor.numOfCols() == nbVecs && vectors.numOfRows()>=nbVecs))
      triFactor.sizeMisMatch("makeblockhouseholder wrong size", nbVecs, triFactor.numOfCols());
  //eigen_assert(triFactor.numOfRows() == nbVecs && triFactor.numOfCols() == nbVecs && vectors.numOfRows()>=nbVecs);

  for(Index i = 0; i < nbVecs; i++)
  {
    Index rs = vectors.numOfRows() - i;
    Scalar Vii = vectors.coeff(i,i);
    (vectors.coeffRef(i,i)) = NumTraits<Scalar>::one();
//    triFactor.col(i).head(i).noalias() = -hCoeffs(i) * vectors.block(i, 0, rs, i).adjoint()
//                                       * vectors.col(i).tail(rs);
    CoeffsType colTriFactor = triFactor.columnVector(i);
    CoeffsType headtriFactor = colTriFactor.head(i);
    VectorsType blockVectors(vectors, i, 0, rs, i);
    Indexing idx(4);
    idx[0] = idx[1] = 0;
    idx[2] = rs; idx[3] = i;
    (adjoint(blockVectors)).multSubMatVecVec(idx, vectors.columnVector(i).tail(rs), headtriFactor);
    headtriFactor *= -hCoeffs.coeff(i);
    colTriFactor.head(i, headtriFactor);
    triFactor.columnVector(i, colTriFactor);

    vectors.coeffRef(i, i) = Vii;
    // FIXME add .noalias() once the triangular product can work inplace
//    triFactor.col(i).head(i) = triFactor.block(0,0,i,i).template triangularView<Upper>()
//                             * triFactor.col(i).head(i);
//    triFactor(i,i) = hCoeffs(i);

    CoeffsType headTemp(headtriFactor);
    VectorsType blockTri(triFactor, 0, 0, i, i);
    blockTri.multSubMatVecVec(idx, headtriFactor, headTemp);
    colTriFactor.head(i, headTemp);
    triFactor.columnVector(i, colTriFactor);
    triFactor.coeffRef(i,i) = hCoeffs.coeff(i);
  }
}

template<typename MatrixType,typename VectorsType,typename CoeffsType>
void applyBlockHouseholderOnTheLeft(MatrixType& mat, VectorsType& vectors, const CoeffsType& hCoeffs)
{
//  typedef typename MatrixType::Index Index;
//  enum { TFactorSize = MatrixType::ColsAtCompileTime };
  Index nbVecs = vectors.numOfCols();
  //Matrix<typename MatrixType::Scalar, TFactorSize, TFactorSize> T(nbVecs,nbVecs);
  MatrixType T(nbVecs, nbVecs);
  makeBlockHouseholderTriangularFactor(T, vectors, hCoeffs);

  //const TriangularView<const VectorsType, UnitLower>& V(vectors);

  // A -= V T V^* A
//  Matrix<typename MatrixType::Scalar,VectorsType::ColsAtCompileTime,MatrixType::ColsAtCompileTime,0,
//         VectorsType::MaxColsAtCompileTime,MatrixType::MaxColsAtCompileTime> tmp = V.adjoint() * mat;
  MatrixType tmp(vectors.numOfCols(), mat.numOfCols()), tmpMat(mat);
  multMatMat(adjoint(vectors),mat, tmp);

  // FIXME add .noalias() once the triangular product can work inplace
//  tmp = T.template triangularView<Upper>().adjoint() * tmp;
//  mat.noalias() -= V * tmp;

  multMatMat(adjoint(T), tmp, tmp);
  multMatMat(vectors, tmp, tmpMat);
  mat -= tmpMat;
}

} // end namespace internalEigenSolver

} // end namespace xlifepp

#endif // EIGEN_BLOCK_HOUSEHOLDER_HPP
