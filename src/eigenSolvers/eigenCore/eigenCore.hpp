/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file eigenCore.hpp
  \author Manh-Ha Nguyen
  \since 4 apr 2013
  \date 4 apr 2013

  \brief Main header file for intern eigen solver classes
 */

#ifndef EIGENCORE_HPP
#define EIGENCORE_HPP

#include "utils.h"
#include "./utils/MatrixEigenDense.hpp"
#include "./utils/VectorEigenDense.hpp"
#include "./smartPointer/SmartPtr.hpp"

#endif /* EIGENCORE_HPP */
