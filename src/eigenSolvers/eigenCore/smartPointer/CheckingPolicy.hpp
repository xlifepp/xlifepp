/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file CheckingPolicy.hpp
  \author Manh Ha NGUYEN
  \since 8 April 2013
  \date  8 April 2013

  \brief Checking policy for smart pointer class
*/

#ifndef CHECKING_POLICY_HPP
#define CHECKING_POLICY_HPP

namespace xlifepp
{
/*!
  \struct NoCheck
  \ingroup  SmartPointerCheckingGroup
  Implementation of the CheckingPolicy used by SmartPtr
  Well, it's clear what it does :o)
*/
template <class P>
struct NoCheck
{
  NoCheck() //! default constructor
  {}

  template <class P1>
  NoCheck(const NoCheck<P1>&) //! copy constructor
  {}

  static void onDefault(const P&)
  {}

  static void onInit(const P&)
  {}

  static void onDereference(const P&)
  {}

  static void swap(NoCheck&)
  {}
};

} // end namespace xlifepp

#endif // CHECKING_POLICY_HPP
