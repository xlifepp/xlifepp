/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file StoragePolicy.hpp
  \author Manh Ha NGUYEN
  \since 8 April 2013
  \date  8 April 2013

  \brief Storage policy for smart pointer class
*/

#ifndef STORAGE_POLICY_HPP
#define STORAGE_POLICY_HPP

namespace xlifepp
{
/*!
  \class DefaultSPStorage
  Implementation of the StoragePolicy used by SmartPtr
*/
template <class T>
class DefaultSPStorage
{
  public:
    typedef T* StoredType;    //!< the type of the pointee_ object
    typedef T* InitPointerType; //!< type used to declare OwnershipPolicy type.
    typedef T* PointerType;   //!< type returned by operator->
    typedef T& ReferenceType; //!< type returned by operator*

    //! default constructor
    DefaultSPStorage() : pointee_(Default())
    {}

    /*!
      The storage policy doesn't initialize the stored pointer
      which will be initialized by the OwnershipPolicy's clone fn
    */
    DefaultSPStorage(const DefaultSPStorage&) : pointee_(nullptr)
    {}

    //! copy constructor
    template <class U>
    DefaultSPStorage(const DefaultSPStorage<U>&) : pointee_(nullptr)
    {}

    //! constructor by stored type
    DefaultSPStorage(const StoredType& p) : pointee_(p) {}

    //! pointer operator
    PointerType operator->() const { return pointee_; }

    //! refenrce operator
    ReferenceType operator*() const { return *pointee_; }

    void swap(DefaultSPStorage& rhs)
    { std::swap(pointee_, rhs.pointee_); }

    // Better implementation???
//    template<typename K>
//    void swap(DefaultSPStorage<K>& rhs)
//    {
//        if (nullptr == pointee_) {
//            pointee_ = rhs.pointee_;
//        }
//        else {
//            typename DefaultSPStorage<K>::StoredType temp;
//            temp = rhs.pointee_;
//            rhs.pointee_ = pointee_;
//            pointee_ = temp;
//        }
//    }


    // Accessors
    template <class F>
    friend typename DefaultSPStorage<F>::PointerType getImpl(const DefaultSPStorage<F>& sp);

    template <class F>
    friend const typename DefaultSPStorage<F>::StoredType& getImplConstRef(DefaultSPStorage<F>& sp);

    template <class F>
    friend typename DefaultSPStorage<F>::StoredType& getImplRef(DefaultSPStorage<F>& sp);

    template <class F>
    friend const typename DefaultSPStorage<F>::StoredType& getImplRef(const DefaultSPStorage<F>& sp);

  protected:
    /*!
      Destroys the data stored
      (Destruction might be taken over by the OwnershipPolicy)
     
      If your compiler gives you a warning in this area while
      compiling the tests, it is on purpose, please ignore it.
    */
    void destroy()
    { delete pointee_; }

    //! Default value to initialize the pointer
    static StoredType Default()
    { return nullptr; }

  private:
    //! Data
    StoredType pointee_;
};

template <class T>
inline typename DefaultSPStorage<T>::PointerType getImpl(const DefaultSPStorage<T>& sp)
{ return sp.pointee_; }

template <class T>
inline const typename DefaultSPStorage<T>::StoredType& getImplConstRef(DefaultSPStorage<T>& sp)
{ return sp.pointee_; }

template <class T>
inline typename DefaultSPStorage<T>::StoredType& getImplRef(DefaultSPStorage<T>& sp)
{ return sp.pointee_; }

template <class T>
inline const typename DefaultSPStorage<T>::StoredType& getImplRef(const DefaultSPStorage<T>& sp)
{ return sp.pointee_; }

} // end namespace xlifepp

#endif // STORAGE_POLICY_HPP
