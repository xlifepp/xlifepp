/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file RefToValue.hpp
  \author Manh Ha NGUYEN
  \since 8 April 2013
  \date  8 April 2013

  \brief Utility tool for smart pointer class
*/

#ifndef REF_TO_VALUE_HPP
#define REF_TO_VALUE_HPP

namespace xlifepp
{
/*!
  \class RefToValue
  \ingroup SmartPointerGroup
  Transports a reference as a value
  Serves to implement the Colvin/Gibbons trick for SmartPtr/ScopeGuard
*/
template <class T>
class RefToValue
{
  public:
  RefToValue(T& ref) : ref_(ref) {} //!< constructor

  RefToValue(const RefToValue& rhs) : ref_(rhs.ref_) {} //!< copy constructor

  //! access operator
  operator T& () const
  {
    return ref_;
  }

  private:
    // Disable - not implemented
    RefToValue(); //!< default constructor forbidden
    RefToValue& operator=(const RefToValue&); //!< assignment operator forbidden

    T& ref_; //!< reference
};

////////////////////////////////////////////////////////////////////////////////
///  \ingroup ExceptionGroup
///  RefToValue creator.
////////////////////////////////////////////////////////////////////////////////

template <class T>
inline RefToValue<T> ByRef(T& t)
{
  return RefToValue<T>(t);
}

} // end namespace xlifepp

#endif // REF_TO_VALUE_HPP
