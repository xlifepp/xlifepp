/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SmartPtr.hpp
  \author Manh Ha NGUYEN
  \since 8 April 2013
  \date  8 April 2013

  \brief Simple smart pointer class
*/

#ifndef SMART_POINTER_HPP
#define SMART_POINTER_HPP

#include "utils.h"
#include "../eigenCore.hpp"
#include "StoragePolicy.hpp"
#include "OwnershipPolicy.hpp"
#include "ConversionPolicy.hpp"
#include "CheckingPolicy.hpp"
#include "ConstantPolicy.hpp"
#include "RefToValue.hpp"

namespace xlifepp
{

// Forward declaration
template<typename T,
        template <class> class OwnershipPolicy = RefCounted,
        class ConversionPolicy = DisallowConversion,
        template <class> class CheckingPolicy = NoCheck,
        template <class> class StoragePolicy = DefaultSPStorage,
        template<class> class ConstnessPolicy = DontPropagateConst
        >
 class SmartPtr;

/*!
  \struct SmartPtrDef
  this class added to unify the usage of SmartPtr.
  instead of writing SmartPtr<T,OP,CP,KP,SP> write SmartPtrDef<T,OP,CP,KP,SP>::type
*/
template<typename T,
        template <class> class OwnershipPolicy = RefCounted,
        class ConversionPolicy = DisallowConversion,
        template <class> class CheckingPolicy = NoCheck,
        template <class> class StoragePolicy = DefaultSPStorage,
        template<class> class ConstnessPolicy = DontPropagateConst
        >
struct SmartPtrDef
{
  typedef SmartPtr<T, OwnershipPolicy, ConversionPolicy, CheckingPolicy, StoragePolicy, ConstnessPolicy> type;
};

/*!
  \class SmartPtr
  Smart pointers are objects which store pointers to dynamically allocated (heap) objects.
  They behave much like built-in C++ pointers except that they automatically delete the object pointed to at the appropriate time.
  Conceptually, smart pointers are seen as owning the object pointed to,
  and thus responsible for deletion of the object when it is no longer needed.

  \note The implementation is adapted from
  Modern C++ Design: Generic Programming and Design Patterns Applied
*/
template <typename T,
        template <class> class OwnershipPolicy,
        class ConversionPolicy,
        template <class> class CheckingPolicy,
        template <class> class StoragePolicy,
        template <class> class ConstnessPolicy
        >
class SmartPtr : public StoragePolicy<T>,
                 public OwnershipPolicy<typename StoragePolicy<T>::InitPointerType>,
                 public CheckingPolicy<typename StoragePolicy<T>::StoredType>,
                 public ConversionPolicy
{
    typedef StoragePolicy<T> SP;
    typedef OwnershipPolicy<typename StoragePolicy<T>::InitPointerType> OP;
    typedef CheckingPolicy<typename StoragePolicy<T>::StoredType> KP;
    typedef ConversionPolicy CP;

  public:
    typedef typename ConstnessPolicy<T>::Type* ConstPointerType;
    typedef typename ConstnessPolicy<T>::Type& ConstReferenceType;

    typedef typename SP::PointerType PointerType;
    typedef typename SP::StoredType StoredType;
    typedef typename SP::ReferenceType ReferenceType;

    typedef typename Conditional<OP::destructiveCopy,SmartPtr, const SmartPtr>::type CopyArg;

  private:
    struct NeverMatched {};

    typedef const StoredType& ImplicitArg;
    typedef typename Conditional<false, const StoredType&, NeverMatched>::type ExplicitArg;

  public:
    //! default constructor
    SmartPtr() : OP(_smPtrNull)
    {
      KP::onDefault(getImpl(*this));
    }

    //! constructor from explicit argument
    explicit
    SmartPtr(ExplicitArg p) : SP(p)
    {
      KP::onInit(getImpl(*this));
    }

    //! basic constructor from implicit argument
    SmartPtr(ImplicitArg p) : SP(p)
    {
      KP::onInit(getImpl(*this));
    }

    //! constructor from implicit argument
    SmartPtr(ImplicitArg p, bool isAllocated) : SP(p)
    {
      KP::onInit(getImpl(*this));
      OP::allocateMemory(isAllocated);
    }

    //! constructor frm copy argument
    SmartPtr(CopyArg& rhs) : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
    {
      getImplRef(*this) = OP::clone(getImplRef(rhs));
    }

    //! constructor from null pointer
    SmartPtr(SmartPointerNullType smNull) : OP(smNull) { }

    template<typename T1,
            template <class> class OP1,
            class CP1,
            template <class> class KP1,
            template <class> class SP1,
            template <class> class CNP1
            >
    SmartPtr(const SmartPtr<T1, OP1, CP1, KP1, SP1, CNP1>& rhs)   //! copy constructor
        : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
    {
      getImplRef(*this) = OP::clone(getImplRef(rhs));
    }

    template<typename T1,
            template <class> class OP1,
            class CP1,
            template <class> class KP1,
            template <class> class SP1,
            template <class> class CNP1
            >
    SmartPtr(SmartPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs) //! copy constructor (non const arg)
        : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
    {
      getImplRef(*this) = OP::clone(getImplRef(rhs));
    }

    //! constructor
    SmartPtr(RefToValue<SmartPtr> rhs)
    : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
    {}

    operator RefToValue<SmartPtr>()
    { return RefToValue<SmartPtr>(*this); }

    //! assignment operator from copy argument
    SmartPtr& operator=(CopyArg& rhs)
    {
      SmartPtr temp(rhs);
      temp.swap(*this);
      return *this;
    }
    //! assignment operator from null pointer
    SmartPtr& operator=(const SmartPointerNullType&)
    {
      if (_smPtrNull == *this) return *this;
      else {
        SmartPtr temp(_smPtrNull);
        temp.swap(*this);
        return *this;
      }
    }

    //! assignment operator
    template<typename T1,
            template <class> class OP1,
            class CP1,
            template <class> class KP1,
            template <class> class SP1,
            template <class> class CNP1
            >
    SmartPtr& operator=(const SmartPtr<T1, OP1, CP1, KP1, SP1, CNP1>& rhs)
    {
      SmartPtr temp(rhs);
      temp.swap(*this);
      return *this;
    }
    //! assignment operator (non const arg)
    template<typename T1,
            template <class> class OP1,
            class CP1,
            template <class> class KP1,
            template <class> class SP1,
            template <class> class CNP1
            >
    SmartPtr& operator=(SmartPtr<T1, OP1, CP1, KP1, SP1, CNP1>& rhs)
    {
      SmartPtr temp(rhs);
      temp.swap(*this);
      return *this;
    }

    void swap(SmartPtr& rhs)
    {
      OP::swap(rhs);
      CP::swap(rhs);
      KP::swap(rhs);
      SP::swap(rhs);
    }

    //! destructor
    ~SmartPtr()
    {
      if (OP::release(getImpl(*static_cast<SP*>(this))))
      {
        SP::destroy();
      }
    }

    template<typename T1,
            template <class> class OP1,
            class CP1,
            template <class> class KP1,
            template <class> class SP1,
            template <class> class CNP1
            >
    friend void release(SmartPtr<T1, OP1, CP1, KP1, SP1, CNP1>& sp,
                        typename SP1<T1>::StoredType& p);

    template<typename T1,
            template <class> class OP1,
            class CP1,
            template <class> class KP1,
            template <class> class SP1,
            template <class> class CNP1
            >
    friend void reset(SmartPtr<T1, OP1, CP1, KP1, SP1, CNP1>& sp,
                      typename SP1<T1>::StoredType p);

    template<typename T1>
    friend SmartPtr<typename RemoveConst<T1>::type>  smartPtrConstCast(SmartPtr<T1>& rhs);

    template<typename T1>
    friend const SmartPtr<typename RemoveConst<T1>::type>  smartPtrConstCast(const SmartPtr<T1>& rhs);

// Comment by Eric, because hazardous syntax (line 282) and function never used in XliFE++
//    template<typename T1,
//            template <class> class OP1,
//            class CP1,
//            template <class> class KP1,
//            template <class> class SP1,
//            template <class> class CNP1
//            >
//    bool merge(SmartPtr< T1, OP1, CP1, KP1, SP1, CNP1>& rhs)
//    {
//      if (getImpl(*this) != getImpl(rhs))  return false;
//      return OP::template merge(rhs);  // ???????
//    }

    PointerType operator->()
    {
      KP::onDereference(getImplRef(*this));
      return SP::operator->();
    }

    ConstPointerType operator->() const
    {
      KP::onDereference(getImplRef(*this));
      return SP::operator->();
    }

    ReferenceType operator*()
    {
      KP::onDereference(getImplRef(*this));
      return SP::operator*();
    }

    ConstReferenceType operator*() const
    {
      KP::onDereference(getImplRef(*this));
      return SP::operator*();
    }

    bool operator!() const // Enables "if (!sp) ..."
    { return getImpl(*this) == 0; }

    static inline T* getPointer(const SmartPtr& sp)
    { return getImpl(sp); }

    // Ambiguity buster
    template<typename T1,
            template <class> class OP1,
            class CP1,
            template <class> class KP1,
            template <class> class SP1,
            template <class> class CNP1
            >
    bool operator==(const SmartPtr<T1, OP1, CP1, KP1, SP1, CNP1>& rhs) const
    { return getImpl(*this) == getImpl(rhs); }

    // Ambiguity buster
    template<typename T1,
            template <class> class OP1,
            class CP1,
            template <class> class KP1,
            template <class> class SP1,
            template <class> class CNP1
            >
    bool operator!=(const SmartPtr<T1, OP1, CP1, KP1, SP1, CNP1>& rhs) const
    { return !(*this == rhs); }

    // Ambiguity buster
    template<typename T1,
            template <class> class OP1,
            class CP1,
            template <class> class KP1,
            template <class> class SP1,
            template <class> class CNP1
            >
    bool operator<(const SmartPtr<T1, OP1, CP1, KP1, SP1, CNP1>& rhs) const
    { return getImpl(*this) < getImpl(rhs); }

    // Ambiguity buster
    template<typename T1,
            template <class> class OP1,
            class CP1,
            template <class> class KP1,
            template <class> class SP1,
            template <class> class CNP1
            >
    inline bool operator > (const SmartPtr< T1, OP1, CP1, KP1, SP1, CNP1>& rhs)
    { return (getImpl(rhs) < getImpl(*this)); }

    // Ambiguity buster
    template<typename T1,
            template <class> class OP1,
            class CP1,
            template <class> class KP1,
            template <class> class SP1,
            template <class> class CNP1
            >
    inline bool operator <= (const SmartPtr< T1, OP1, CP1, KP1, SP1, CNP1>& rhs)
    { return !(getImpl(rhs) < getImpl(*this)); }

    // Ambiguity buster
    template<typename T1,
            template <class> class OP1,
            class CP1,
            template <class> class KP1,
            template <class> class SP1,
            template <class> class CNP1
            >
    inline bool operator >= (const SmartPtr< T1, OP1, CP1, KP1, SP1, CNP1>& rhs)
    { return !(getImpl(*this) < getImpl(rhs)); }

private:
    // Helper for enabling 'if (sp)'
    struct Tester
    {
      Tester(int) {}
      void dummy() {}
    };

    typedef void (Tester::*unspecified_boolean_type_)();

    typedef typename Conditional<CP::allow, Tester, unspecified_boolean_type_>::type
        unspecified_boolean_type;

public:
    // enable 'if (sp)'
    operator unspecified_boolean_type() const
    { return !*this ? 0 : &Tester::dummy; }

private:
    // Helper for disallowing automatic conversion
    struct Insipid
    {
      Insipid(PointerType) {}
    };

    typedef typename Conditional<CP::allow, PointerType, Insipid>::type
        AutomaticConversionResult;

public:
    operator AutomaticConversionResult() const
    { return getImpl(*this); }
};


//==============================================================================
// friends
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP
        >
inline void release(SmartPtr<T, OP, CP, KP, SP, CNP>& sp,
                    typename SP<T>::StoredType& p)
{
  p = getImplRef(sp);
  getImplRef(sp) = SP<T>::Default();
}

template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP
        >
inline void reset(SmartPtr<T, OP, CP, KP, SP, CNP>& sp,
                  typename SP<T>::StoredType p)
{ SmartPtr<T, OP, CP, KP, SP, CNP>(p).swap(sp); }

//==============================================================================
// free comparison operators for class template SmartPtr
//==============================================================================

//==============================================================================
//  operator== for lhs = SmartPtr, rhs = raw pointer
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP,
        typename U
        >
inline bool operator==(const SmartPtr<T, OP, CP, KP, SP, CNP>& lhs, U* rhs)
{ return getImpl(lhs) == rhs; }

//==============================================================================
//  operator== for lhs = raw pointer, rhs = SmartPtr
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP,
        typename U
        >
inline bool operator==(U* lhs, const SmartPtr<T, OP, CP, KP, SP, CNP >& rhs)
{ return rhs == lhs; }

//==============================================================================
//  operator!= for lhs = SmartPtr, rhs = raw pointer
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP,
        typename U
        >
inline bool operator!=(const SmartPtr<T, OP, CP, KP, SP, CNP>& lhs, U* rhs)
{ return !(lhs == rhs); }

//==============================================================================
//  operator!= for lhs = raw pointer, rhs = SmartPtr
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP,
        typename U
        >
inline bool operator!=(U* lhs, const SmartPtr<T, OP, CP, KP, SP, CNP >& rhs)
{ return rhs != lhs; }

//==============================================================================
//  operator< for lhs = SmartPtr, rhs = raw pointer
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP,
        typename U
        >
inline bool operator<(const SmartPtr<T, OP, CP, KP, SP, CNP >& lhs, U* rhs)
{ return (getImpl(lhs) < rhs); }

//==============================================================================
//  operator< for lhs = raw pointer, rhs = SmartPtr
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP,
        typename U
        >
inline bool operator<(U* lhs, const SmartPtr<T, OP, CP, KP, SP, CNP >& rhs)
{ return (getImpl(rhs) < lhs); }

//==============================================================================
//  operator> for lhs = SmartPtr, rhs = raw pointer
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP,
        typename U
        >
inline bool operator>(const SmartPtr<T, OP, CP, KP, SP, CNP >& lhs, U* rhs)
{ return rhs < lhs; }

//==============================================================================
//  operator> for lhs = raw pointer, rhs = SmartPtr
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP,
        typename U
        >
inline bool operator>(U* lhs, const SmartPtr<T, OP, CP, KP, SP, CNP >& rhs)
{ return rhs < lhs; }

//==============================================================================
//  operator<= for lhs = SmartPtr, rhs = raw pointer
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP,
        typename U
        >
inline bool operator<=(const SmartPtr<T, OP, CP, KP, SP, CNP >& lhs, U* rhs)
{ return !(rhs < lhs); }

//==============================================================================
//  operator<= for lhs = raw pointer, rhs = SmartPtr
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP,
        typename U
        >
inline bool operator<=(U* lhs, const SmartPtr<T, OP, CP, KP, SP, CNP >& rhs)
{ return !(rhs < lhs); }

//==============================================================================
//  operator>= for lhs = SmartPtr, rhs = raw pointer
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP,
        typename U
        >
inline bool operator>=(const SmartPtr<T, OP, CP, KP, SP, CNP >& lhs, U* rhs)
{ return !(lhs < rhs); }

//==============================================================================
//  operator>= for lhs = raw pointer, rhs = SmartPtr
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP,
        typename U
        >
inline bool operator>=(U* lhs, const SmartPtr<T, OP, CP, KP, SP, CNP>& rhs)
{ return !(lhs < rhs); }

//==============================================================================
//  operator== for lhs = SmartPtr, rhs = _smPtrNull
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP
        >
inline bool operator==(const SmartPtr<T, OP, CP, KP, SP, CNP>& lhs, SmartPointerNullType rhs)
{ return getImpl(lhs) == 0; }

//==============================================================================
//  operator== for lhs = _smPtrNull, rhs = SmartPtr
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP
        >
inline bool operator==(SmartPointerNullType lhs, const SmartPtr<T, OP, CP, KP, SP, CNP>& rhs)
{ return getImpl(rhs) == 0; }

//==============================================================================
//  operator!= for lhs = SmartPtr, rhs = _smPtrNull
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP
        >
inline bool operator!=(const SmartPtr<T, OP, CP, KP, SP, CNP>& lhs, SmartPointerNullType rhs)
{ return !(lhs == rhs); }

//==============================================================================
//  operator!= for lhs = _smPtrNull, rhs = SmartPtr
//==============================================================================
template<typename T,
        template <class> class OP,
        class CP,
        template <class> class KP,
        template <class> class SP,
        template <class> class CNP
        >
inline bool operator!=(SmartPointerNullType lhs, const SmartPtr<T, OP, CP, KP, SP, CNP>& rhs)
{ return !(lhs == rhs); }

// Cast constant pointee to non-constant one (constant version)
template<typename T>
const SmartPtr<typename RemoveConst<T>::type> smartPtrConstCast(const SmartPtr<T>& rhs) //, OP, CP, KP, SP, CNP>& rhs)
{
    typedef typename RemoveConst<T>::type* typePointerNonConst;
    typePointerNonConst tmpPointer = (const_cast<typePointerNonConst>(getImplRef(rhs)));

    return smartPtr(tmpPointer);
}

// Cast constant pointee to non-constant one.
template<typename T>
SmartPtr<typename RemoveConst<T>::type>  smartPtrConstCast(SmartPtr<T>& rhs)
{
    typedef typename RemoveConst<T>::type* typePointerNonConst;
    typePointerNonConst tmpPointer = (const_cast<typePointerNonConst>(getImplRef(rhs)));

    return smartPtr(tmpPointer);
}

//==============================================================================
//  operator!= for lhs = SmartPtr, rhs = _smPtrNull
//==============================================================================
template<typename T>
inline SmartPtr<T> smartPtr(T* p)
{
    return SmartPtr<T>(p);
}

//==============================================================================
//  operator!= for lhs = SmartPtr, rhs = _smPtrNull
//==============================================================================
template<typename T>
inline SmartPtr<T> smartPtrFromRef(T* p)
{
    return SmartPtr<T>(p, false);
}

} // end namespace xlifepp

#endif // SMART_POINTER_HPP
