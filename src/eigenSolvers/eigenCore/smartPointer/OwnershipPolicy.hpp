/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file OwnershipPolicy.hpp
  \author Manh Ha NGUYEN
  \since 8 April 2013
  \date  8 April 2013

  \brief Ownership policy for smart pointer class
*/

#ifndef OWNERSHIP_POLICY_HPP
#define OWNERSHIP_POLICY_HPP

#include "utils.h"
#include <stdint.h>

namespace xlifepp
{

enum SmartPointerNullType {
  _smPtrNull = 0
};

enum ConstCastStorage { _constCastSt = 1};

/*!
  \class RefCounted
  \ingroup  SmartPointerOwnershipGroup
  Implementation of the OwnershipPolicy used by SmartPtr
  Provides a classic external reference counting implementation
*/
template <class P>
class RefCounted
{
  public:
    typedef uintptr_t* CountType;
    RefCounted()
    : count_p(new uintptr_t) //! default constructor
    {
      if (nullptr == count_p) error("constructor");
      *count_p = 1;
      isAllocated_ = true;
    }

    RefCounted(const RefCounted& rhs) //! copy constructor
    : count_p(rhs.count_p)
    {
      isAllocated_ = (rhs.isAllocated_);
    }

    // MWCW lacks template friends, hence the following kludge
    template <typename P1>
    RefCounted(const RefCounted<P1>& rhs) //! template copy constructor
    : count_p(reinterpret_cast<const RefCounted&>(rhs).count_p),
      isAllocated_(reinterpret_cast<const RefCounted&>(rhs).isAllocated_)
    {}


    //! Initialization for special smart pointer reference count
    RefCounted(SmartPointerNullType nullType)
    {
      count_p = (nullptr);
      isAllocated_ = (true);
    }

    P clone(const P& val)
    {
      if (nullptr == count_p) return val;
      else {
        ++*count_p;
        return val;
      }
    }

    void allocateMemory(bool isAlloc)  { isAllocated_ = isAlloc; }

    bool release(const P&)
    {
      // Special case for "null" smart pointer
      // This way ensure pointee-object be released.
      if (nullptr == count_p) return false;

      if (!--*count_p)
      {
        delete count_p;
        count_p = nullptr;
        if (isAllocated_) return true;
        else return false;
      }
      return false;
    }

    void swap(RefCounted& rhs)
    {
      std::swap(count_p, rhs.count_p);
      std::swap(isAllocated_, rhs.isAllocated_);
    }

    enum { destructiveCopy = false };

    template<typename T>
    friend const typename RefCounted<T>::CountType& getRefCounted(const RefCounted<T>& ref);

    template<typename T>
    friend typename RefCounted<T>::CountType& getRefCounted(RefCounted<T>& ref);

    template<typename T>
    friend bool getRefCountedAlloc(RefCounted<T>& ref);

    template<typename T>
    friend void setRefCountedAlloc(bool isAlloc, RefCounted<T>& ref);
  private:
    //! Data
    CountType count_p;
    /*!
      This flag ensures only freeing in-the-fly memory (created by new keyword),
      not freeing memory in case of SmartPointer pointing to an already-allocated memory, e.x passing to SmartPointer by reference.
      true if memory is allocated in the fly, otherwise false
    */
    bool isAllocated_;
};

template<typename T>
inline const typename RefCounted<T>::CountType& getRefCounted(const RefCounted<T>& ref)
{
  return (ref.count_p);
}

template<typename T>
inline typename RefCounted<T>::CountType& getRefCounted(RefCounted<T>& ref)
{
  return (ref.count_p);
}

template<typename T>
bool getRefCountedAlloc(RefCounted<T>& ref)
{
  return ref.isAllocated_;
}

template<typename T>
void setRefCountedAlloc(bool isAlloc, RefCounted<T>& ref)
{
  ref.allocateMemory(isAlloc);
}

} // end namespace xlifepp

#endif // OWNERSHIP_POLICY_HPP
