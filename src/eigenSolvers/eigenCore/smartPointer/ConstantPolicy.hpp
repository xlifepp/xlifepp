/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file ConstantPolicy.hpp
  \author Manh Ha NGUYEN
  \since 8 April 2013
  \date  8 April 2013

  \brief Constant policy for smart pointer class
*/

#ifndef CONSTANT_POLICY_HPP
#define CONSTANT_POLICY_HPP

namespace xlifepp
{

////////////////////////////////////////////////////////////////////////////////
/// @note These policy classes are used in SmartPtr to define
///  how const is propagated from the pointee.
////////////////////////////////////////////////////////////////////////////////

/*!
  \struct DontPropagateConst
  \ingroup ConstGroup
  Don't propagate constness of pointed or referred object.
*/
template< class T >
struct DontPropagateConst
{
  typedef T Type;
};

/*!
  \struct PropagateConst
  \ingroup ConstGroup
  Propagate constness of pointed or referred object.
*/
template< class T >
struct PropagateConst
{
  typedef const T Type;
};

} // end namespace xlifepp

#endif // CONSTANT_POLICY_HPP
