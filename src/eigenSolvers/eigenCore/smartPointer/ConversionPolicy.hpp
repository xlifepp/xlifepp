/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file ConversionPolicy.hpp
  \author Manh Ha NGUYEN
  \since 8 April 2013
  \date  8 April 2013
  
  \brief Conversion policy for smart pointer class
*/

#ifndef CONVERSION_POLICY_HPP
#define CONVERSION_POLICY_HPP

namespace xlifepp
{

/*!
  \struct AllowConversion
  Implementation of the ConversionPolicy used by SmartPtr
  Allows implicit conversion from SmartPtr to the pointee type
*/
struct AllowConversion
{
  enum { allow = true };

  void swap(AllowConversion&)
  {}
};

/*!
  \struct DisallowConversion
  Implementation of the ConversionPolicy used by SmartPtr
  Does not allow implicit conversion from SmartPtr to the pointee type
  You can initialize a DisallowConversion with an AllowConversion
*/
struct DisallowConversion
{
  DisallowConversion()
  {}

  DisallowConversion(const AllowConversion&)
  {}

  enum { allow = false };

  void swap(DisallowConversion&)
  {}
};

} // end namespace xlifepp

#endif // CONVERSION_POLICY_HPP
