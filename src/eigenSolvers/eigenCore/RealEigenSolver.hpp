/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file RealEigenSolver.hpp
  \author Manh Ha NGUYEN
  \since 22 Fev 2013
  \date  06 August 2013

  \brief Direct eigen solver for real non-symmetric (dense) matrix
*/

// This file is adapted from Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2009 Claire Maurice
// Copyright (C) 2009 Gael Guennebaud <gael.guennebaud@inria.fr>
// Copyright (C) 2010 Jitse Niesen <jitse@maths.leeds.ac.uk>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_REAL_EIGENSOLVER_HPP
#define EIGEN_REAL_EIGENSOLVER_HPP

#include "./utils/VectorEigenDense.hpp"
#include "./utils/MatrixEigenDense.hpp"
#include "./decomposition/RealSchur.hpp"

namespace xlifepp
{

namespace internalEigenSolver
{
template<typename MatrixType>
void doComputeEigenvectorsRealSolverInPlace(const real_t matrixnorm, const MatrixType& schurT, const MatrixType& schurU, MatrixType& eigVec);
}
/*!
  \class RealEigenSolver
  
  \brief Computes eigenvalues and eigenvectors of general matrices
  
  \tparam _MatrixType the type of the matrix of which we are computing the
  eigendecomposition; this is expected to be an instantiation of the Matrix
  class template. Currently, only real matrices are supported.
  
  The eigenvalues and eigenvectors of a matrix \f$ A \f$ are scalars
  \f$ \lambda \f$ and vectors \f$ v \f$ such that \f$ Av = \lambda v \f$.  If
  \f$ D \f$ is a diagonal matrix with the eigenvalues on the diagonal, and
  \f$ V \f$ is a matrix with the eigenvectors as its columns, then \f$ A V =
  V D \f$. The matrix \f$ V \f$ is almost always invertible, in which case we
  have \f$ A = V D V^{-1} \f$. This is called the eigendecomposition.
  
  The eigenvalues and eigenvectors of a matrix may be complex, even when the
  matrix is real. However, we can choose real matrices \f$ V \f$ and \f$ D
  \f$ satisfying \f$ A V = V D \f$, just like the eigendecomposition, if the
  matrix \f$ D \f$ is not required to be diagonal, but if it is allowed to
  have blocks of the form
  \f[ \left(\begin{array}{cc} u & v \\ -v & u \end{array}\right) \f]
  (where \f$ u \f$ and \f$ v \f$ are real numbers) on the diagonal.  These
  blocks correspond to complex eigenvalue pairs \f$ u \pm iv \f$. We call
  this variant of the eigendecomposition the pseudo-eigendecomposition.
  
  Call the function compute() to compute the eigenvalues and eigenvectors of
  a given matrix. Alternatively, you can use the
  RealEigenSolver(const MatrixType&, bool) constructor which computes the
  eigenvalues and eigenvectors at construction time. Once the eigenvalue and
  eigenvectors are computed, they can be retrieved with the eigenvalues() and
  eigenvectors() functions. The pseudoEigenvalueMatrix() and
  pseudoEigenvectors() methods allow the construction of the
  pseudo-eigendecomposition.
  
  The documentation for RealEigenSolver(const MatrixType&, bool) contains an
  example of the typical use of this class.
  
  \note The implementation is adapted from
  <a href="http://math.nist.gov/javanumerics/jama/">JAMA</a> (public domain).
  Their code is based on EISPACK.
  
  \sa MatrixBase::eigenvalues(), class ComplexEigenSolver, class SelfAdjointEigenSolver
*/
template<typename _MatrixType>
class RealEigenSolver
{
  public:
  
    //! \brief Synonym for the template parameter \p _MatrixType.
    typedef _MatrixType MatrixType;
    
    //! \brief Scalar type for matrices of type #MatrixType.
    typedef typename MatrixType::type_t Scalar;
    typedef typename NumTraits<Scalar>::RealScalar RealScalar;
    typedef VectorEigenDense<Scalar> ColumnVectorType;
    typedef VectorEigenDense<Scalar> VectorType;
    
    /*!
      \brief Complex scalar type for #MatrixType.
      
      This is \c std::complex<Scalar> if #Scalar is real (e.g.,
      \c float or \c double) and just \c Scalar if #Scalar is
      complex.
    */
    typedef typename NumTraits<Scalar>::ComplexScalar ComplexScalar;
    typedef VectorEigenDense<ComplexScalar> ComplexVectorType;
    
    /*!
      \brief Type for vector of eigenvalues as returned by eigenvalues().
      
      This is a column vector with entries of type #ComplexScalar.
      The length of the vector is the size of #MatrixType.
    */
    typedef VectorEigenDense<ComplexScalar> EigenvalueType;
    
    /*!
      \brief Type for matrix of eigenvectors as returned by eigenvectors().
      
      This is a square matrix with entries of type #ComplexScalar.
      The size is the same as the size of #MatrixType.
    */
    typedef MatrixEigenDense<ComplexScalar> EigenvectorsType;
    
    /*!
      \brief Default constructor.
      
      The default constructor is useful in cases in which the user intends to
      perform decompositions via RealEigenSolver::compute(const MatrixType&, bool).
      
      \sa compute() for an example.
    */
    RealEigenSolver() : eivec_(), eivalues_(), isInitialized_(false), realSchur_(), matT_() {}
    
    /*!
      \brief Default constructor with memory preallocation
      
      Like the default constructor but with preallocation of the internal data
      according to the specified problem \a size.
      \sa RealEigenSolver()
    */
    RealEigenSolver(Index size)
      : eivec_(size, size),
        eivalues_(size),
        isInitialized_(false),
        eigenvectorsOk_(false),
        realSchur_(size),
        matT_(size, size)
    {}
    
    /*!
      \brief Constructor; computes eigendecomposition of given matrix.
      
      \param[in]  matrix  Square matrix whose eigendecomposition is to be computed.
      \param[in]  computeEigenvectors  If true, both the eigenvectors and the
         eigenvalues are computed; if false, only the eigenvalues are
         computed.
      
      This constructor calls compute() to compute the eigenvalues
      and eigenvectors.
      
      \sa compute()
    */
    RealEigenSolver(const MatrixType& matrix, bool computeEigenvectors = true)
      : eivec_(matrix.numOfRows(), matrix.numOfCols()),
        eivalues_(matrix.numOfCols()),
        isInitialized_(false),
        eigenvectorsOk_(false),
        realSchur_(matrix.numOfCols()),
        matT_(matrix.numOfRows(), matrix.numOfCols())
    {
      compute(matrix, computeEigenvectors);
    }
    
    /*!
      \brief Returns the eigenvectors of given matrix.
      
      \returns  %Matrix whose columns are the (possibly complex) eigenvectors.
      
      \pre Either the constructor
      RealEigenSolver(const MatrixType&,bool) or the member function
      compute(const MatrixType&, bool) has been called before, and
      \p computeEigenvectors was set to true (the default).
      
      Column \f$ k \f$ of the returned matrix is an eigenvector corresponding
      to eigenvalue number \f$ k \f$ as returned by eigenvalues().  The
      eigenvectors are normalized to have (Euclidean) norm equal to one. The
      matrix returned by this function is the matrix \f$ V \f$ in the
      eigendecomposition \f$ A = V D V^{-1} \f$, if it exists.
      
      \sa eigenvalues(), pseudoEigenvectors()
    */
    EigenvectorsType eigenvectors() const;
    
    /*!
      \brief Returns the pseudo-eigenvectors of given matrix.
      
      \returns  Const reference to matrix whose columns are the pseudo-eigenvectors.
      
      \pre Either the constructor
      RealEigenSolver(const MatrixType&,bool) or the member function
      compute(const MatrixType&, bool) has been called before, and
      \p computeEigenvectors was set to true (the default).
      
      The real matrix \f$ V \f$ returned by this function and the
      block-diagonal matrix \f$ D \f$ returned by pseudoEigenvalueMatrix()
      satisfy \f$ AV = VD \f$.
      
      \sa pseudoEigenvalueMatrix(), eigenvectors()
    */
    const MatrixType& pseudoEigenvectors() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized","RealEigenSolver"); }
      if (!eigenvectorsOk_) { error("eigenvectors_not_computed"); }
      return eivec_;
    }
    
    /*!
      \brief Returns the block-diagonal matrix in the pseudo-eigendecomposition.
      
      \returns  A block-diagonal matrix.
      
      \pre Either the constructor
      RealEigenSolver(const MatrixType&,bool) or the member function
      compute(const MatrixType&, bool) has been called before.
      
      The matrix \f$ D \f$ returned by this function is real and
      block-diagonal. The blocks on the diagonal are either 1-by-1 or 2-by-2
      blocks of the form
      \f$\left(\begin{array}{cc} u & v \\ -v & u \end{array}\right) \f$.
      These blocks are not sorted in any particular order.
      The matrix \f$ D \f$ and the matrix \f$ V \f$ returned by
      pseudoEigenvectors() satisfy \f$ AV = VD \f$.
      
      \sa pseudoEigenvectors() for an example, eigenvalues()
    */
    MatrixType pseudoEigenvalueMatrix() const;
    
    /*!
      \brief Returns the eigenvalues of given matrix.
      
      \returns A const reference to the column vector containing the eigenvalues.
      
      \pre Either the constructor
      RealEigenSolver(const MatrixType&,bool) or the member function
      compute(const MatrixType&, bool) has been called before.
      
      The eigenvalues are repeated according to their algebraic multiplicity,
      so there are as many eigenvalues as rows in the matrix. The eigenvalues
      are not sorted in any particular order.
      
      \sa eigenvectors(), pseudoEigenvalueMatrix(),
          MatrixBase::eigenvalues()
    */
    const EigenvalueType& eigenvalues() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized","RealEigenSolver"); }
      return eivalues_;
    }
    
    /*!
      \brief Computes eigendecomposition of given matrix.
      
      \param[in]  matrix  Square matrix whose eigendecomposition is to be computed.
      \param[in]  computeEigenvectors  If true, both the eigenvectors and the
         eigenvalues are computed; if false, only the eigenvalues are
         computed.
      \returns    Reference to \c *this
      
      This function computes the eigenvalues of the real matrix \p matrix.
      The eigenvalues() function can be used to retrieve them.  If
      \p computeEigenvectors is true, then the eigenvectors are also computed
      and can be retrieved by calling eigenvectors().
      
      The matrix is first reduced to real Schur form using the RealSchur
      class. The Schur decomposition is then used to compute the eigenvalues
      and eigenvectors.
      
      The cost of the computation is dominated by the cost of the
      Schur decomposition, which is very approximately \f$ 25n^3 \f$
      (where \f$ n \f$ is the size of the matrix) if \p computeEigenvectors
      is true, and \f$ 10n^3 \f$ if \p computeEigenvectors is false.
      
      This method reuses of the allocated data in the RealEigenSolver object.
    */
    RealEigenSolver<MatrixType>& compute(const MatrixType& matrix, bool computeEigenvectors = true);
    
    ComputationInfo info() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized","RealEigenSolver"); }
      return realSchur_.info();
    }
    
  private:
    void doComputeEigenvectors(); //!< computes eigen vectors
  protected:
    MatrixType eivec_; //!< type of eigen vectors
    EigenvalueType eivalues_; //!< type of eigen values
    bool isInitialized_; //!< true if initialized
    bool eigenvectorsOk_; //!< true for computation of eigen vectors
    RealSchur<MatrixType> realSchur_; //!< schur matrix
    MatrixType matT_;
};

template<typename MatrixType>
MatrixType RealEigenSolver<MatrixType>::pseudoEigenvalueMatrix() const
{
  if (!isInitialized_) { error("eigensolver_not_initialized","RealEigenSolver"); }
  Index n = eivalues_.size();
  MatrixType matD(n);
  for (Index i=0; i<n; ++i)
  {
    //if (internal::isMuchSmallerThan(internal::imag(eivalues_.coeff(i)), internal::real(eivalues_.coeff(i))))
    if (NumTraits<Scalar>::abs2(eivalues_.coeff(i).imag()) < NumTraits<Scalar>::abs2(eivalues_.coeff(i).real())*NumTraits<Scalar>::prec()*NumTraits<Scalar>::prec())
      matD.coeffRef(i,i) = eivalues_.coeff(i).real();
    else
    {
        matD.coeffRef(i,i)     = eivalues_.coeff(i).real();
        matD.coeffRef(i,i+1)   = eivalues_.coeff(i).imag();
        matD.coeffRef(i+1,i)   = -eivalues_.coeff(i).imag();
        matD.coeffRef(i+1,i+1) = eivalues_.coeff(i).real();
//      matD.template block<2,2>(i,i) <<  internal::real(eivalues_.coeff(i)), internal::imag(eivalues_.coeff(i)),
//                                       -internal::imag(eivalues_.coeff(i)), internal::real(eivalues_.coeff(i));
      ++i;
    }
  }
  return matD;
}

template<typename MatrixType>
typename RealEigenSolver<MatrixType>::EigenvectorsType RealEigenSolver<MatrixType>::eigenvectors() const
{
  if (!isInitialized_) { error("eigensolver_not_initialized","RealEigenSolver"); }
  if (!eigenvectorsOk_) { error("eigenvectors_not_computed"); }
  Index n = eivec_.numOfCols();
  EigenvectorsType matV(n, n);
  
  for (Index j = 0; j < n; ++j)
  {
    if ((imag(eivalues_.coeff(j)) <= (theEpsilon * real(eivalues_.coeff(j)))) || j + 1 == n)
    {
      // we have a real eigen value
      //matV.col(j) = eivec_.col(j).template cast<ComplexScalar>();
      //matV.col(j).normalize();
      ComplexVectorType col = cmplx(eivec_.columnVector(j));
      col.normalize();
      matV.columnVector(j, col);
    }
    else
    {
      // we have a pair of complex eigen values
      for (Index i = 0; i < n; ++i)
      {
        matV.coeffRef(i, j)   = ComplexScalar(eivec_.coeff(i, j),  eivec_.coeff(i, j + 1));
        matV.coeffRef(i, j + 1) = ComplexScalar(eivec_.coeff(i, j), -eivec_.coeff(i, j + 1));
      }
      //      matV.col(j).normalize();
      //      matV.col(j+1).normalize();
      ComplexVectorType col = matV.columnVector(j);
      col.normalize();
      matV.columnVector(j, col);
      col = matV.columnVector(j + 1);
      col.normalize();
      matV.columnVector(j + 1, col);
      ++j;
    }
  }
  return matV;
}

template<typename MatrixType>
RealEigenSolver<MatrixType>& RealEigenSolver<MatrixType>::compute(const MatrixType& matrix, bool computeEigenvectors)
{
  trace_p->push("RealEigenSolver::compute");
  if (matrix.numOfCols() != matrix.numOfRows()) { matrix.nonSquare("Computing real matrix", matrix.numOfRows(), matrix.numOfCols()); }
  
  // Reduce to real Schur form.
  realSchur_.compute(matrix, computeEigenvectors);
  if (realSchur_.info() == _success)
  {
    matT_ = realSchur_.matrixT();
    if (computeEigenvectors)
    { eivec_ = realSchur_.matrixU(); }
    
    // Compute eigenvalues from matT
    eivalues_.resize(matrix.numOfCols());
    Index i = 0;
    while (i < matrix.numOfCols())
    {
      if (i == matrix.numOfCols() - 1 || matT_.coeff(i + 1, i) == Scalar(0))
      {
        eivalues_.coeffRef(i) = matT_.coeff(i, i);
        ++i;
      }
      else
      {
        Scalar p = Scalar(0.5) * (matT_.coeff(i, i) - matT_.coeff(i + 1, i + 1));
        Scalar z = std::sqrt(std::abs(p * p + matT_.coeff(i + 1, i) * matT_.coeff(i, i + 1)));
        eivalues_.coeffRef(i)   = ComplexScalar(matT_.coeff(i + 1, i + 1) + p, z);
        eivalues_.coeffRef(i + 1) = ComplexScalar(matT_.coeff(i + 1, i + 1) + p, -z);
        i += 2;
      }
    }
    
    // Compute eigenvectors.
    if (computeEigenvectors)
    { doComputeEigenvectors(); }
  }
  
  isInitialized_ = true;
  eigenvectorsOk_ = computeEigenvectors;
  
  trace_p->pop();
  return *this;
}

// Complex scalar division.
template<typename Scalar>
complex_t cdiv(Scalar xr, Scalar xi, Scalar yr, Scalar yi)
{
  Scalar r, d;
  if (std::abs(yr) > std::abs(yi))
  {
    r = yi / yr;
    d = yr + r * yi;
    return complex_t((xr + r * xi) / d, (xi - r * xr) / d);
  }
  else
  {
    r = yr / yi;
    d = yi + r * yr;
    return complex_t((r * xr + xi) / d, (r * xi - xr) / d);
  }
}


template<typename MatrixType>
void RealEigenSolver<MatrixType>::doComputeEigenvectors()
{
  const Index size = eivec_.numOfCols();
  const Scalar eps = NumTraits<Scalar>::epsilon();
  
  // inefficient! this is already computed in RealSchur
  Scalar norm(0);
  for (Index j = 0; j < size; ++j)
  {
    //norm += matT_.row(j).segment((std::max)(j-1,Index(0)), size-(std::max)(j-1,Index(0))).cwiseAbs().sum();
    norm += matT_.blockRow(j, (std::max)(j - 1, Index(0)), size - (std::max)(j - 1, Index(0))).sumAbs();
  }
  
  // Backsubstitute to find vectors of upper triangular form
  if (norm == 0.0)
  {
    return;
  }
  
  for (Index n = size - 1; n >= 0; n--)
  {
    Scalar p = eivalues_.coeff(n).real();
    Scalar q = eivalues_.coeff(n).imag();
    
    // Scalar vector
    if (q == Scalar(0))
    {
      Scalar lastr(0), lastw(0);
      Index l = n;
      
      matT_.coeffRef(n, n) = 1.0;
      for (Index i = n - 1; i >= 0; i--)
      {
        Scalar w = matT_.coeff(i, i) - p;
        //Scalar r = matT_.row(i).segment(l,n-l+1).dot(matT_.col(n).segment(l, n-l+1));
        VectorType tmpCol = matT_.blockCol(l, n, n - l + 1);
        Scalar r = matT_.blockRow(i, l, n - l + 1).dotProduct(tmpCol);
        
        if (eivalues_.coeff(i).imag() < 0.0)
        {
          lastw = w;
          lastr = r;
        }
        else
        {
          l = i;
          if (eivalues_.coeff(i).imag() == 0.0)
          {
            if (w != 0.0)
            { matT_.coeffRef(i, n) = -r / w; }
            else
            { matT_.coeffRef(i, n) = -r / (eps * norm); }
          }
          else // Solve real equations
          {
            Scalar x = matT_.coeff(i, i + 1);
            Scalar y = matT_.coeff(i + 1, i);
            Scalar denom = (eivalues_.coeff(i).real() - p) * (eivalues_.coeff(i).real() - p) + eivalues_.coeff(i).imag() * eivalues_.coeff(i).imag();
            Scalar t = (x * lastr - lastw * r) / denom;
            matT_.coeffRef(i, n) = t;
            if (std::abs(x) > std::abs(lastw))
            { matT_.coeffRef(i + 1, n) = (-r - w * t) / x; }
            else
            { matT_.coeffRef(i + 1, n) = (-lastr - y * t) / lastw; }
          }
          
          // Overflow control
          Scalar t = std::abs(matT_.coeff(i, n));
          if ((eps * t) * t > Scalar(1))
          {
            VectorType col = matT_.columnVector(n);
            VectorType tail = col.tail(size - i);
            tail /= t;
            col.tail(size - i, tail);
            matT_.columnVector(n, col);
            //matT_.col(n).tail(size-i) /= t;
          }
        }
      }
    }
    else if (q < Scalar(0) && n > 0) // Complex vector
    {
      Scalar lastra(0), lastsa(0), lastw(0);
      Index l = n - 1;
      
      // Last vector component imaginary so matrix is triangular
      if (std::abs(matT_.coeff(n, n - 1)) > std::abs(matT_.coeff(n - 1, n)))
      {
        matT_.coeffRef(n - 1, n - 1) = q / matT_.coeff(n, n - 1);
        matT_.coeffRef(n - 1, n) = -(matT_.coeff(n, n) - p) / matT_.coeff(n, n - 1);
      }
      else
      {
        std::complex<Scalar> cc = cdiv<Scalar>(0.0, -matT_.coeff(n - 1, n), matT_.coeff(n - 1, n - 1) - p, q);
        matT_.coeffRef(n - 1, n - 1) = real(cc);
        matT_.coeffRef(n - 1, n) = imag(cc);
      }
      matT_.coeffRef(n, n - 1) = 0.0;
      matT_.coeffRef(n, n) = 1.0;
      for (Index i = n - 2; i >= 0; i--)
      {
        //        Scalar ra = matT_.row(i).segment(l, n-l+1).dot(matT_.col(n-1).segment(l, n-l+1));
        VectorType tmpCol = matT_.blockCol(l, n - 1, n - l + 1);
        Scalar ra = matT_.blockRow(i, l, n - l + 1).dotProduct(tmpCol);
        //        Scalar sa = matT_.row(i).segment(l, n-l+1).dot(matT_.col(n).segment(l, n-l+1));
        tmpCol = matT_.blockCol(l, n, n - l + 1);
        Scalar sa = matT_.blockRow(i, l, n - l + 1).dotProduct(tmpCol);
        Scalar w = matT_.coeff(i, i) - p;
        
        if (eivalues_.coeff(i).imag() < 0.0)
        {
          lastw = w;
          lastra = ra;
          lastsa = sa;
        }
        else
        {
          l = i;
          if (eivalues_.coeff(i).imag() == RealScalar(0))
          {
            std::complex<Scalar> cc = cdiv(-ra, -sa, w, q);
            matT_.coeffRef(i, n - 1) = real(cc);
            matT_.coeffRef(i, n) = imag(cc);
          }
          else
          {
            // Solve complex equations
            Scalar x = matT_.coeff(i, i + 1);
            Scalar y = matT_.coeff(i + 1, i);
            Scalar vr = (eivalues_.coeff(i).real() - p) * (eivalues_.coeff(i).real() - p) + eivalues_.coeff(i).imag() * eivalues_.coeff(i).imag() - q * q;
            Scalar vi = (eivalues_.coeff(i).real() - p) * Scalar(2) * q;
            if ((vr == 0.0) && (vi == 0.0))
            { vr = eps * norm * (std::abs(w) + std::abs(q) + std::abs(x) + std::abs(y) + std::abs(lastw)); }
            
            complex_t cc = cdiv(x * lastra - lastw * ra + q * sa, x * lastsa - lastw * sa - q * ra, vr, vi);
            matT_.coeffRef(i, n - 1) = real(cc);
            matT_.coeffRef(i, n) = imag(cc);
            if (std::abs(x) > (std::abs(lastw) + std::abs(q)))
            {
              matT_.coeffRef(i + 1, n - 1) = (-ra - w * matT_.coeff(i, n - 1) + q * matT_.coeff(i, n)) / x;
              matT_.coeffRef(i + 1, n) = (-sa - w * matT_.coeff(i, n) - q * matT_.coeff(i, n - 1)) / x;
            }
            else
            {
              cc = cdiv(-lastra - y * matT_.coeff(i, n - 1), -lastsa - y * matT_.coeff(i, n), lastw, q);
              matT_.coeffRef(i + 1, n - 1) = real(cc);
              matT_.coeffRef(i + 1, n) = imag(cc);
            }
          }
          
          // Overflow control
          
          Scalar t = (std::max)(std::abs(matT_.coeff(i, n - 1)), std::abs(matT_.coeff(i, n)));
          if ((eps * t) * t > Scalar(1))
          {
            //matT_.block(i, n-1, size-i, 2) /= t;
            MatrixType block(matT_, i, n - 1, size - i, 2);
            block /= t;
            matT_.replace(block, i, n - 1, size - i, 2);
          }
          
        }
      }
      
      // We handled a pair of complex conjugate eigenvalues, so need to skip them both
      n--;
    }
    else
    {
      error("abnormal_failure"); // this should not happen
    }
  }
  
  ColumnVectorType m_tmp(eivec_.numOfRows());
  // Back transformation to get eigenvectors of original matrix
  for (Index j = size - 1; j >= 0; j--)
  {
    //MatrixType leftCols(eivec_, 0, 0, eivec_.numOfRows(), j+1);
    //m_tmp.noalias() = eivec_.leftCols(j+1) * matT_.col(j).segment(0, j+1);
    Indexing idx(4); idx[0] = idx[1] = 0; idx[2] = eivec_.numOfRows(); idx[3] = j + 1;
    eivec_.multSubMatVecVec(idx, matT_.blockCol(0, j, j + 1), m_tmp);
    //eivec_.col(j) = m_tmp;
    eivec_.columnVector(j, m_tmp);
    
  }
}

namespace internalEigenSolver
{
// This function is a copy-paste code of function doCompute above and only serves for BlockSchurKrylov
// We can consider it redundant and maybe combine it with doCompute above in the future!
template<typename MatrixType>
void doComputeEigenvectorsRealSolverInPlace(const real_t matrixnorm, const MatrixType& schurT, const MatrixType& schurU, MatrixType& eigVec)
{
  typedef typename MatrixType::type_t Scalar;
  typedef VectorEigenDense<Scalar> VectorType;
  MatrixType matT = schurT;
  eigVec = schurU;
  const Index size = eigVec.numOfCols();
  const Scalar eps = NumTraits<Scalar>::epsilon();

  VectorEigenDense<complex_t> eivalues(size);

  Index i = 0;
  // Counting number of real eigenvalues, in symmetric case, we don't need to compute eigVec, it's equal to schurU
  Index count = 1;
  while (i < size)
  {
    if (i == size - 1 || matT.coeff(i + 1, i) == Scalar(0))
    {
      eivalues.coeffRef(i) = complex_t(matT.coeff(i, i),0);
      ++i;
      ++count;
    }
    else
    {
      Scalar p = Scalar(0.5) * (matT.coeff(i, i) - matT.coeff(i + 1, i + 1));
      Scalar z = std::sqrt(std::abs(p * p + matT.coeff(i + 1, i) * matT.coeff(i, i + 1)));
      eivalues.coeffRef(i)   = complex_t(matT.coeff(i + 1, i + 1) + p, z);
      eivalues.coeffRef(i + 1) = complex_t(matT.coeff(i + 1, i + 1) + p, -z);
      i += 2;
    }
  }

  if (count < size) {
    // inefficient! this is already computed in RealSchur
    Scalar norm(0);
    for (Index j = 0; j < size; ++j)
    {
      //norm += matT.row(j).segment((std::max)(j-1,Index(0)), size-(std::max)(j-1,Index(0))).cwiseAbs().sum();
      norm += matT.blockRow(j, (std::max)(j - 1, Index(0)), size - (std::max)(j - 1, Index(0))).sumAbs();
    }

    // Backsubstitute to find vectors of upper triangular form
    if (norm == 0.0)
    {
      return;
    }

    for (Index n = size - 1; n >= 0; n--)
    {
      Scalar p = eivalues.coeff(n).real();
      Scalar q = eivalues.coeff(n).imag();

      // Scalar vector
      if (q == Scalar(0))
      {
        Scalar lastr(0), lastw(0);
        Index l = n;

        matT.coeffRef(n, n) = 1.0;
        for (Index i = n - 1; i >= 0; i--)
        {
          Scalar w = matT.coeff(i, i) - p;
          //Scalar r = matT.row(i).segment(l,n-l+1).dot(matT.col(n).segment(l, n-l+1));
          VectorType tmpCol = matT.blockCol(l, n, n - l + 1);
          Scalar r = matT.blockRow(i, l, n - l + 1).dotProduct(tmpCol);

          if (eivalues.coeff(i).imag() < 0.0)
          {
            lastw = w;
            lastr = r;
          }
          else
          {
            l = i;
            if (eivalues.coeff(i).imag() == 0.0)
            {
              if (w != 0.0)
              { matT.coeffRef(i, n) = -r / w; }
              else
              { matT.coeffRef(i, n) = -r / (eps * norm); }
            }
            else // Solve real equations
            {
              Scalar x = matT.coeff(i, i + 1);
              Scalar y = matT.coeff(i + 1, i);
              Scalar denom = (eivalues.coeff(i).real() - p) * (eivalues.coeff(i).real() - p) + eivalues.coeff(i).imag() * eivalues.coeff(i).imag();
              Scalar t = (x * lastr - lastw * r) / denom;
              matT.coeffRef(i, n) = t;
              if (std::abs(x) > std::abs(lastw))
              { matT.coeffRef(i + 1, n) = (-r - w * t) / x; }
              else
              { matT.coeffRef(i + 1, n) = (-lastr - y * t) / lastw; }
            }

            // Overflow control
            Scalar t = std::abs(matT.coeff(i, n));
            if ((eps * t) * t > Scalar(1))
            {
              VectorType col = matT.columnVector(n);
              VectorType tail = col.tail(size - i);
              tail /= t;
              col.tail(size - i, tail);
              matT.columnVector(n, col);
              //matT.col(n).tail(size-i) /= t;
            }
          }
        }
      }
      else if (q < Scalar(0) && n > 0) // Complex vector
      {
        Scalar lastra(0), lastsa(0), lastw(0);
        Index l = n - 1;

        // Last vector component imaginary so matrix is triangular
        if (std::abs(matT.coeff(n, n - 1)) > std::abs(matT.coeff(n - 1, n)))
        {
          matT.coeffRef(n - 1, n - 1) = q / matT.coeff(n, n - 1);
          matT.coeffRef(n - 1, n) = -(matT.coeff(n, n) - p) / matT.coeff(n, n - 1);
        }
        else
        {
          std::complex<Scalar> cc = cdiv<Scalar>(0.0, -matT.coeff(n - 1, n), matT.coeff(n - 1, n - 1) - p, q);
          matT.coeffRef(n - 1, n - 1) = real(cc);
          matT.coeffRef(n - 1, n) = imag(cc);
        }
        matT.coeffRef(n, n - 1) = 0.0;
        matT.coeffRef(n, n) = 1.0;
        for (Index i = n - 2; i >= 0; i--)
        {
          //        Scalar ra = matT.row(i).segment(l, n-l+1).dot(matT.col(n-1).segment(l, n-l+1));
          VectorType tmpCol = matT.blockCol(l, n - 1, n - l + 1);
          Scalar ra = matT.blockRow(i, l, n - l + 1).dotProduct(tmpCol);
          //        Scalar sa = matT.row(i).segment(l, n-l+1).dot(matT.col(n).segment(l, n-l+1));
          tmpCol = matT.blockCol(l, n, n - l + 1);
          Scalar sa = matT.blockRow(i, l, n - l + 1).dotProduct(tmpCol);
          Scalar w = matT.coeff(i, i) - p;

          if (eivalues.coeff(i).imag() < 0.0)
          {
            lastw = w;
            lastra = ra;
            lastsa = sa;
          }
          else
          {
            l = i;
            if (eivalues.coeff(i).imag() == real_t(0))
            {
              std::complex<Scalar> cc = cdiv(-ra, -sa, w, q);
              matT.coeffRef(i, n - 1) = real(cc);
              matT.coeffRef(i, n) = imag(cc);
            }
            else
            {
              // Solve complex equations
              Scalar x = matT.coeff(i, i + 1);
              Scalar y = matT.coeff(i + 1, i);
              Scalar vr = (eivalues.coeff(i).real() - p) * (eivalues.coeff(i).real() - p) + eivalues.coeff(i).imag() * eivalues.coeff(i).imag() - q * q;
              Scalar vi = (eivalues.coeff(i).real() - p) * Scalar(2) * q;
              if ((vr == 0.0) && (vi == 0.0))
              { vr = eps * norm * (std::abs(w) + std::abs(q) + std::abs(x) + std::abs(y) + std::abs(lastw)); }

              complex_t cc = cdiv(x * lastra - lastw * ra + q * sa, x * lastsa - lastw * sa - q * ra, vr, vi);
              matT.coeffRef(i, n - 1) = real(cc);
              matT.coeffRef(i, n) = imag(cc);
              if (std::abs(x) > (std::abs(lastw) + std::abs(q)))
              {
                matT.coeffRef(i + 1, n - 1) = (-ra - w * matT.coeff(i, n - 1) + q * matT.coeff(i, n)) / x;
                matT.coeffRef(i + 1, n) = (-sa - w * matT.coeff(i, n) - q * matT.coeff(i, n - 1)) / x;
              }
              else
              {
                cc = cdiv(-lastra - y * matT.coeff(i, n - 1), -lastsa - y * matT.coeff(i, n), lastw, q);
                matT.coeffRef(i + 1, n - 1) = real(cc);
                matT.coeffRef(i + 1, n) = imag(cc);
              }
            }

            // Overflow control

            Scalar t = (std::max)(std::abs(matT.coeff(i, n - 1)), std::abs(matT.coeff(i, n)));
            if ((eps * t) * t > Scalar(1))
            {
              //matT.block(i, n-1, size-i, 2) /= t;
              MatrixType block(matT, i, n - 1, size - i, 2);
              block /= t;
              matT.replace(block, i, n - 1, size - i, 2);
            }

          }
        }

        // We handled a pair of complex conjugate eigenvalues, so need to skip them both
        n--;
      }
      else
      {
        where("doComputeEigenvectorsRealSolverInPlace(...)");
        error("abnormal_failure"); // this should not happen
      }
    }

    VectorType m_tmp(eigVec.numOfRows());
    // Back transformation to get eigenvectors of original matrix
    for (Index j = size - 1; j >= 0; j--)
    {
      //MatrixType leftCols(eigVec, 0, 0, eigVec.numOfRows(), j+1);
      //m_tmp.noalias() = eigVec.leftCols(j+1) * matT.col(j).segment(0, j+1);
      Indexing idx(4); idx[0] = idx[1] = 0; idx[2] = eigVec.numOfRows(); idx[3] = j + 1;
      eigVec.multSubMatVecVec(idx, matT.blockCol(0, j, j + 1), m_tmp);
      //eigVec.col(j) = m_tmp;
      eigVec.columnVector(j, m_tmp);

    }
  }
}

}

} // end namespace xlifepp

#endif // EIGEN_REAL_EIGENSOLVER_HPP
