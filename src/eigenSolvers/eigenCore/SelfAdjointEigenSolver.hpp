/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file SelfAdjointEigenSolver.hpp
  \author Manh Ha NGUYEN
  \since 22 Jan 2013
  \date  7 Mars 2013

  \brief Direct eigen solver for selfadjoint (dense) matrix
*/

// This file is adapted from Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2009 Claire Maurice
// Copyright (C) 2009 Gael Guennebaud <gael.guennebaud@inria.fr>
// Copyright (C) 2010 Jitse Niesen <jitse@maths.leeds.ac.uk>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_SELF_ADJOINT_EIGENSOLVER_HPP
#define EIGEN_SELF_ADJOINT_EIGENSOLVER_HPP

#include "./utils/VectorEigenDense.hpp"
#include "./decomposition/Tridiagonalization.hpp"

namespace xlifepp
{

template<typename _MatrixType>
class GeneralizedSelfAdjointEigenSolver;

/*!
  \class SelfAdjointEigenSolver
  
  \brief Computes eigenvalues and eigenvectors of selfadjoint matrices
  
  \tparam _MatrixType the type of the matrix of which we are computing the
  eigendecomposition; this is expected to be an instantiation of the Matrix
  class template.
  
  A matrix \f$ A \f$ is selfadjoint if it equals its adjoint. For real
  matrices, this means that the matrix is symmetric: it equals its
  transpose. This class computes the eigenvalues and eigenvectors of a
  selfadjoint matrix. These are the scalars \f$ \lambda \f$ and vectors
  \f$ v \f$ such that \f$ Av = \lambda v \f$.  The eigenvalues of a
  selfadjoint matrix are always real. If \f$ D \f$ is a diagonal matrix with
  the eigenvalues on the diagonal, and \f$ V \f$ is a matrix with the
  eigenvectors as its columns, then \f$ A = V D V^{-1} \f$ (for selfadjoint
  matrices, the matrix \f$ V \f$ is always invertible). This is called the
  eigendecomposition.
  
  The algorithm exploits the fact that the matrix is selfadjoint, making it
  faster and more accurate than the general purpose eigenvalue algorithms
  implemented in RealEigenSolver.
  
  Only the \b lower \b triangular \b part of the input matrix is referenced.
  
  Call the function compute() to compute the eigenvalues and eigenvectors of
  a given matrix. Alternatively, you can use the
  SelfAdjointEigenSolver(const MatrixType&, int) constructor which computes
  the eigenvalues and eigenvectors at construction time. Once the eigenvalue
  and eigenvectors are computed, they can be retrieved with the eigenvalues()
  and eigenvectors() functions.
  
  To solve the \em generalized eigenvalue problem \f$ Av = \lambda Bv \f$ and
  the likes, see the class GeneralizedSelfAdjointEigenSolver.
  
  \sa MatrixBase::eigenvalues(), class EigenSolver
*/
template<typename _MatrixType>
class SelfAdjointEigenSolver
{
  public:
  
    typedef _MatrixType MatrixType;
    
    //! \brief Scalar type for matrices of type \p _MatrixType.
    typedef typename MatrixType::type_t Scalar;
    
    /*!
      \brief Real scalar type for \p _MatrixType.
      
      This is just \c Scalar if #Scalar is real (e.g., \c float or
      \c double), and the type of the real part of \c Scalar if #Scalar is
      complex.
    */
    typedef typename NumTraits<Scalar>::RealScalar RealScalar;
    
    /*!
      \brief Type for vector of eigenvalues as returned by eigenvalues().
      
      This is a column vector with entries of type #RealScalar.
      The length of the vector is the size of \p _MatrixType.
    */
    typedef VectorEigenDense<RealScalar> RealVectorType;
    typedef Tridiagonalization<MatrixType> TridiagonalizationType;
    
    /*!
      \brief Default constructor for fixed-size matrices.
      
      The default constructor is useful in cases in which the user intends to
      perform decompositions via compute().
    */
    SelfAdjointEigenSolver()
      : eivec_(),
        eivalues_(),
        subdiag_(),
        maxIterations_(30),
        info_(_noConvergence),
        eigenvectorsOk_(false),
        isInitialized_(false)
    { }
    
    /*!
      \brief Constructor, pre-allocates memory for dynamic-size matrices.
      
      \param[in] size Positive integer, size of the matrix whose
      eigenvalues and eigenvectors will be computed.
      
      This constructor is useful for dynamic-size matrices, when the user
      intends to perform decompositions via compute(). The \p size
      parameter is only used as a hint. It is not an error to give a wrong
      \p size, but it may impair performance.
      
      \sa compute() for an example
    */
    SelfAdjointEigenSolver(Index size)
      : eivec_(size, size),
        eivalues_(size),
        subdiag_(size > 1 ? size - 1 : 1),
        maxIterations_(30),
        info_(_noConvergence),
        eigenvectorsOk_(false),
        isInitialized_(false)
    {}
    
    /*!
      \brief Constructor; computes eigendecomposition of given matrix.
      
      \param[in] matrix  Selfadjoint matrix whose eigendecomposition is to
         be computed. Only the lower triangular part of the matrix is referenced.
      \param[in] options Can be ComputeEigenvectors (default) or EigenvaluesOnly.
      
      This constructor calls compute(const MatrixType&, int) to compute the
      eigenvalues of the matrix \p matrix. The eigenvectors are computed if
      \p options equals ComputeEigenvectors.
      
      \sa compute(const MatrixType&, int)
    */
    SelfAdjointEigenSolver(const MatrixType& matrix, int_t options = _computeEigenVector)
      : eivec_(matrix.numOfRows(), matrix.numOfCols()),
        eivalues_(matrix.numOfCols()),
        subdiag_(matrix.numOfRows() > 1 ? matrix.numOfRows() - 1 : 1),
        maxIterations_(30),
        info_(_noConvergence),
        eigenvectorsOk_(false),
        isInitialized_(false)
    {
      compute(matrix, options);
    }
    
    /*!
      \brief Computes eigendecomposition of given matrix.
      
      \param[in] matrix Selfadjoint matrix whose eigendecomposition is to
         be computed. Only the lower triangular part of the matrix is referenced.
      \param[in] options Can be ComputeEigenvectors (default) or EigenvaluesOnly.
      \returns   Reference to \c *this
      
      This function computes the eigenvalues of \p matrix.  The eigenvalues()
      function can be used to retrieve them.  If \p options equals ComputeEigenvectors,
      then the eigenvectors are also computed and can be retrieved by
      calling eigenvectors().
      
      This implementation uses a symmetric QR algorithm. The matrix is first
      reduced to tridiagonal form using the Tridiagonalization class. The
      tridiagonal matrix is then brought to diagonal form with implicit
      symmetric QR steps with Wilkinson shift. Details can be found in
      Section 8.3 of Golub \& Van Loan, <i>%Matrix Computations</i>.
      
      The cost of the computation is about \f$ 9n^3 \f$ if the eigenvectors
      are required and \f$ 4n^3/3 \f$ if they are not required.
      
      This method reuses the memory in the SelfAdjointEigenSolver object that
      was allocated when the object was constructed, if the size of the
      matrix does not change.
      
      \sa SelfAdjointEigenSolver(const MatrixType&, int_t)
    */
    SelfAdjointEigenSolver& compute(const MatrixType& matrix, int_t options = _computeEigenVector);
    
    /*!
      \brief Returns the eigenvectors of given matrix.
      
      \returns  A const reference to the matrix whose columns are the eigenvectors.
      
      \pre The eigenvectors have been computed before.
      
      Column \f$ k \f$ of the returned matrix is an eigenvector corresponding
      to eigenvalue number \f$ k \f$ as returned by eigenvalues().  The
      eigenvectors are normalized to have (Euclidean) norm equal to one. If
      this object was used to solve the eigenproblem for the selfadjoint
      matrix \f$ A \f$, then the matrix returned by this function is the
      matrix \f$ V \f$ in the eigendecomposition \f$ A = V D V^{-1} \f$.
      
      \sa eigenvalues()
    */
    const MatrixType& eigenvectors() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "SelfAdjointEigenSolver"); }
      if (!eigenvectorsOk_) { error("eigenvectors_not_computed"); }
      return eivec_;
    }
    
    /*!
      \brief Returns the eigenvalues of given matrix.
      
      \returns A const reference to the column vector containing the eigenvalues.
      
      \pre The eigenvalues have been computed before.
      
      The eigenvalues are repeated according to their algebraic multiplicity,
      so there are as many eigenvalues as rows in the matrix. The eigenvalues
      are sorted in increasing order.
      
      \sa eigenvectors(), MatrixBase::eigenvalues()
    */
    const RealVectorType& eigenvalues() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "SelfAdjointEigenSolver"); }
      return eivalues_;
    }
    
    /*!
      \brief Reports whether previous computation was successful.
      
      \returns \c Success if computation was succesful, \c NoConvergence otherwise.
    */
    ComputationInfo info() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "SelfAdjointEigenSolver"); }
      return info_;
    }

  protected:
    /*!
      Performs a QR step on a tridiagonal symmetric matrix represented as a
      pair of two vectors \a diag and \a subdiag.
    */
    void tridiagonalQRstep(RealVectorType& diag, RealVectorType& subdiag, number_t start, number_t end, MatrixType& matrixQ, number_t n, bool computeEigenvectors);
  protected:
    MatrixType eivec_; //!< type of eigen vectors
    RealVectorType eivalues_; //!< type of eigen values
    typename TridiagonalizationType::SubDiagonalType subdiag_;

    /*!
      \brief Maximum number of iterations.
    
      The algorithm terminates if it does not converge within maxIterations_ * n iterations, where n
      denotes the size of the matrix. This value is currently set to 30 (copied from LAPACK).
    */
    int_t maxIterations_;

    ComputationInfo info_;
    bool eigenvectorsOk_; //!< true if computation of eigen vectors
    bool isInitialized_; //!< true if initialized
};

template<typename MatrixType>
SelfAdjointEigenSolver<MatrixType>& SelfAdjointEigenSolver<MatrixType>
::compute(const MatrixType& matrix, int_t options)
{
  trace_p->push("SelfAdjointEigenSolver::compute");
  typedef typename MatrixType::type_t Scalar;
  if (matrix.numOfCols() != matrix.numOfRows()) { matrix.nonSquare("Computing selfadjoint matrix", matrix.numOfRows(), matrix.numOfCols()); }
  bool test=(options&~(_eigVecMask | _genEigMask)) == 0 && (options & _eigVecMask) != _eigVecMask;
  if (!test) { error("invalid_option"); }
  
  bool computeEigenvectors = (options & _computeEigenVector) == _computeEigenVector;
  Index n = matrix.numOfCols();
  eivalues_.resize(n, 1);
  
  if(n == 1)
  {
    eivalues_.coeffRef(0) = NumTraits<Scalar>::real(matrix.coeff(0, 0));
    if(computeEigenvectors)
    { eivec_ = MatrixType(n, n, NumTraits<Scalar>::one()); }
    info_ = _success;
    isInitialized_ = true;
    eigenvectorsOk_ = computeEigenvectors;
    trace_p->pop();
    return *this;
  }
  
  // declare some aliases
  RealVectorType& diag = eivalues_;
  MatrixType& mat = eivec_;
  
  // map the matrix coefficients to [-1:1] to avoid over- and underflow. Maybe after because it causes numerical unstability
  RealScalar scale = matrix.maxCoeff();
  if(scale == RealScalar(0)) { scale = RealScalar(1); }
  //FIXME Round-off error
  //mat = matrix / scale;
  mat = matrix;
  subdiag_.resize(n - 1);
  //Tridiagonalization<MatrixType> tridiag(n);
  transformMatrix(mat);
  internalEigenSolver::tridiagonalizationInplace(mat, diag, subdiag_, computeEigenvectors);
  transformMatrix(mat);

  // Just keep these stupid codes here for the case of UpperTriangular
  // TODO Remove them soon
  transformVectorDense(diag);
  transformVectorDense(subdiag_);

  Index end = n - 1;
  Index start = 0;
  Index iter = 0; // total number of iterations
  
  while (end > 0)
  {
    for (Index i = start; i < end; ++i)
      if ((std::fabs(subdiag_[i]) <= (NumTraits<RealScalar>::prec() * ((std::fabs(diag[i]) + std::fabs(diag[i + 1]))))))
      //if ((std::fabs(subdiag_[i]) <= (theEpsilon * ((std::fabs(diag[i]) + std::fabs(diag[i + 1]))))))
      { subdiag_[i] = 0; }
      
    // find the largest unreduced block
    while (end > 0 && subdiag_[end - 1] == 0)
    {
      end--;
    }
    if (end <= 0)
    { break; }
    
    // if we spent too many iterations, we give up
    iter++;
    if(iter > maxIterations_ * n) { break; }
    
    start = end - 1;
    while (start > 0 && subdiag_[start - 1] != 0)
    { start--; }
    
    tridiagonalQRstep(diag, subdiag_, start, end, mat, n, computeEigenvectors);
  }
  
  if (iter <= maxIterations_ * n)
  { info_ = _success; }
  else
  { info_ = _noConvergence; }
  
  // Sort eigenvalues and corresponding vectors.
  // TODO make the sort optional ?
  // TODO use a better sort algorithm !!
  if (info_ == _success)
  {
    for (Index i = 0; i < n - 1; ++i)
    {
      Index k;
      k = eivalues_.minElement(i, n - 1);
      if (k > 0)
      {
        std::swap(eivalues_[i], eivalues_[k]);
        if(computeEigenvectors)
        { eivec_.swapCols(i, k); }
      }
    }
  }
  
  // scale back the eigen values
  //eivalues_ *= scale;
  isInitialized_ = true;
  eigenvectorsOk_ = computeEigenvectors;
  trace_p->pop();
  return *this;
}

/*!
  * Performs a QR step on a tridiagonal symmetric matrix represented as a
  * pair of two vectors \a diag and \a subdiag.
  *
  * \param diag
  * \param subdiag
  * \param start
  * \param end
  * \param matrixQ
  * \param n
  * \param computeEigenvectors true to compute eigen vectors
  *
  * For compilation efficiency reasons, this procedure does not use eigen expression
  * for its arguments.
  *
  * Implemented from Golub's "Matrix Computations", algorithm 8.3.2:
  * "implicit symmetric QR step with Wilkinson shift"
  */
template<class Matrix>
void SelfAdjointEigenSolver<Matrix>::tridiagonalQRstep(RealVectorType& diag, RealVectorType& subdiag, number_t start, number_t end, Matrix& matrixQ, number_t n, bool computeEigenvectors)
{
  real_t td = (diag[end - 1] - diag[end]) * real_t(0.5);
  real_t e = subdiag[end - 1];
  // Note that thanks to scaling, e^2 or td^2 cannot overflow, however they can still
  // underflow thus leading to inf/NaN values when using the following commented code:
  //   real_t e2 = abs2(subdiag[end-1]);
  //   real_t mu = diag[end] - e2 / (td + (td>0 ? 1 : -1) * sqrt(td*td + e2));
  // This explain the following, somewhat more complicated, version:
  real_t mu = diag[end];
  if(td == 0)
  { mu -= std::abs(e); }
  else
  {
    real_t e2 = std::pow(subdiag[end - 1], 2);
    real_t h = hypot(td, e);
    if(e2 == 0)  { mu -= (e / (td + (td > 0 ? 1 : -1))) * (e / h); }
    else       { mu -= e2 / (td + (td > 0 ? h : -h)); }
  }
  
  real_t x = diag[start] - mu;
  real_t z = subdiag[start];
  for (number_t k = start; k < end; ++k)
  {
    JacobiRotation<real_t> rot;
    rot.makeGivens(x, z);
    
    // do T = G' T G
    real_t sdk = rot.s() * diag[k] + rot.c() * subdiag[k];
    real_t dkp1 = rot.s() * subdiag[k] + rot.c() * diag[k + 1];
    
    diag[k] = rot.c() * (rot.c() * diag[k] - rot.s() * subdiag[k]) - rot.s() * (rot.c() * subdiag[k] - rot.s() * diag[k + 1]);
    diag[k + 1] = rot.s() * sdk + rot.c() * dkp1;
    subdiag[k] = rot.c() * sdk - rot.s() * dkp1;
    
    
    if (k > start)
    { subdiag[k - 1] = rot.c() * subdiag[k - 1] - rot.s() * z; }
    
    x = subdiag[k];
    
    if (k < end - 1)
    {
      z = -rot.s() * subdiag[k + 1];
      subdiag[k + 1] = rot.c() * subdiag[k + 1];
    }
    
    // apply the givens rotation to the unit matrix Q = Q * G
    if (computeEigenvectors)
    {
        matrixQ.applyOnTheRight(k, k + 1, rot);
    }
  }
}

} // end namespace xlifepp

#endif // EIGEN_SELF_ADJOINT_EIGENSOLVER_HPP
