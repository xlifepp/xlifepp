/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file  VectorEigenDense.hpp
  \author Manh Ha NGUYEN
  \since 22 Jan 2013
  \date  7 Mars 2013

  \brief Definition of the xlifepp::VectorEigenDense class
*/

#ifndef VECTOR_EIGEN_DENSE_HPP
#define VECTOR_EIGEN_DENSE_HPP

#include "utils.h"

namespace xlifepp
{

//! Trait to select corresponding algorithm
template<typename K>
struct VectorAlgorithmSelector
{
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<>
struct VectorAlgorithmSelector<real_t>
{
  typedef std::vector<real_t>::const_iterator cit_vk;
  static real_t dot(cit_vk itb, cit_vk ite, cit_vk it2b) { return std::inner_product(itb, ite, it2b, 0.0);}
};

template<>
struct VectorAlgorithmSelector<complex_t>
{
  typedef std::vector<complex_t>::const_iterator cit_vk;
  static complex_t dot(cit_vk itb, cit_vk ite, cit_vk it2b) { return hermitianInnerProductTpl(itb, ite, it2b);}
};

#endif

/*!
  \class VectorEigenDense
 
  \brief Class (row) dense vector to deal with direct eigen problem solver
 
  This class provides some basic methods to support calculation of eigen problem dense matrix.
 
  \tparam Type of scalar (real_t, complex_t)
 
  //TODO Improve performance by reusing vector data
*/
template<typename K>
class VectorEigenDense : public Vector<K>
{
  public:
    //@{
    //! useful typedefs to VectorEigenDense class
    typedef typename VectorEigenDense::type_t Scalar;
    typedef typename NumTraits<Scalar>::RealScalar RealScalar;
    typedef typename VectorEigenDense::it_vk it_vk;
    typedef typename VectorEigenDense::cit_vk cit_vk;
    //@}

    //! default constructor
    VectorEigenDense() : Vector<K>(), acType_(_col) {}
    //! constructor from length
    VectorEigenDense(const dimen_t l) : Vector<K>(l, K()), acType_(_col) {}
    
    /*!
      Full constructor
      \param[in] l Size of vector (number of row or number of column)
      \param[in] v initial value
      \param[in] ac type of vector (column or row)
    */
    VectorEigenDense(const dimen_t l, const K v, AccessType ac = _col) : Vector<K>(l, v),  acType_(ac) { }
    
    /*!
      Copy constructor
    */
    VectorEigenDense(const VectorEigenDense<K>& v) : Vector<K>(v.size())
    {
      cit_vk itb = v.begin(), ite = v.end(), itv;
      it_vk it = this->begin();
      acType_ = v.acType_;
      for (itv = itb; itv != ite; itv++, it++)
      {
        *it = *itv;
      }
    }
    
    template<typename T>
    friend VectorEigenDense<T> adjointVec(const VectorEigenDense<T>& v);
    template<typename T>
    friend VectorEigenDense<T> transposeVec(const VectorEigenDense<T>& v);
    template<typename T>
    friend VectorEigenDense<T> conj(const VectorEigenDense<T>& v);
    
    //--------------------------------------------------------------------------------
    // Utilities for Householder
    //--------------------------------------------------------------------------------
    /*!
      Computes the elementary reflector H such that:
      \f$ H *this = [ beta 0 ... 0]^T \f$
      where the transformation H is:
      \f$ H = I - tau v v^*\f$
      and the vector v is:
      \f$ v^T = [1 essential^T] \f$
    
      The essential part of the vector \c v is stored in *this.
    
      On output:
      \param tau the scaling factor of the Householder transformation
      \param beta the result of H * \c *this
    
      TODO This implementation is not so good.
           In deed, we can use directly one part of vector as the essential.
    */
    void makeHouseHolderInPlace(K& tau, real_t& beta)
    {
      VectorEigenDense<K> v(this->size() - 1, K(0.0), acType_);
      copy(v, 1, this->size());

     // if (v.norm2() <= NumTraits<K>::epsilon()) {
     //     tau = NumTraits<K>::zero();
     //     beta = real_t(0.0);
     // }
     // else {
          makeHouseHolder(v, tau, beta);
          replace(v, 1, this->size());
     // }
    }
    
    /*!
      Computes the elementary reflector H only for the tail part of vector:
      \f$ H *tail = [ beta 0 ... 0]^T \f$
      where the transformation H is:
      \f$ H = I - tau v v^*\f$
      and the vector v is:
      \f$ v^T = [1 essential^T] \f$
    
      The essential part of the vector \c v is stored in *this.
    
      On output:
      \param tau the scaling factor of the Householder transformation
      \param beta the result of H * \c *this
      On Input:
      \param tail size of the tail of vector on which the house holder transformation is taken place
    
      TODO This implementation is not so good.
           In deed, we can use directly one part of vector as the essential.
    */
    void makeHouseHolderInPlace(K& tau, real_t& beta, number_t tail)
    {
      if (tail > this->size()) { overSize("Tail over size", tail, this->size()); }
      VectorEigenDense<K> v(tail, K(0.0), acType_);
      copy(v, this->size() - tail, this->size());
     // if (v.norm2() <= NumTraits<K>::epsilon()) {
     //     tau = NumTraits<K>::zero();
     //     beta = real_t(0.0);
     // }
     // else {
          v.makeHouseHolderInPlace(tau, beta);
          replace(v, this->size() - tail, this->size());
     // }
    }
    
    /*!
      Computes the elementary reflector H such that:
      \f$ H *this = [ beta 0 ... 0]^T \f$
      where the transformation H is:
      \f$ H = I - tau v v^*\f$
      and the vector v is:
      \f$ v^T = [1 essential^T] \f$
    
      On output:
      \param essential the essential part of the vector \c v
      \param tau the scaling factor of the Householder transformation
      \param beta the result of H * \c *this
    */
    void makeHouseHolder(VectorEigenDense<K>& essential, K& tau, real_t& beta) const
    {
      //VectorBlock<const Derived, EssentialPart::SizeAtCompileTime> tail(derived(), 1, size()-1);
      typedef NumTraits<K> TraitType;
      RealScalar anorm = RealScalar(0.0);
      for (cit_vk it = this->begin()+1; it !=this->end(); ++it) {
          anorm += TraitType::abs2(*it);
      }

      
      //RealScalar tailSqNorm = size()==1 ? RealScalar(0) : tail.squaredNorm();
      //Scalar c0 = coeff(0);
      //RealScalar tailSqNorm = (1 == this->size()) ? real_t(0.0) : std::sqrt(hermitianInnerProductTpl(this->begin() + 1, this->end(), this->begin() + 1).real());
      RealScalar tailSqNorm = (1 == this->size()) ? real_t(0.0) : std::sqrt(anorm);
      K coeff0 = coeff(0);
      
      if(real_t(0.0) == tailSqNorm && TraitType::isReal())
      {
        tau = TraitType::zero();
        beta = TraitType::real(coeff0);
        essential.set(K(0.0));
      }
      else if (real_t(0.0) == tailSqNorm && TraitType::isComplex() && TraitType::zero() == coeff0) {
        tau = TraitType::zero();
        beta = this->norm2();
        essential.set(K(0.0));
      }
      else
      {
        //beta = internal::sqrt(internal::abs2(c0) + tailSqNorm);
        beta = this->norm2();
        
        //if (internal::real(c0)>=RealScalar(0))
        if (TraitType::real(coeff0) >= real_t(0.0))
        { beta = -beta; }
        tau = conj((beta - coeff0) / beta);
        cit_vk it = (this)->begin() + 1;
        
        //essential = tail / (c0 - beta);
        //tau = internal::conj((beta - c0) / beta);
        K temp = (coeff0 - beta);
        for (it_vk itEss = essential.begin(); itEss != essential.end(); it++, itEss++)
        {
          *itEss = (*it) / temp;
        }
      }
    }
    
    /*!
      Extract head a vector, return result into a new vector
    
      \param[in] headSize size of the extracted vector
      \return extracted heading vector
    */
    VectorEigenDense<K> head(number_t headSize)
    {
      if ((headSize < 0) || (headSize > this->size())) { overSize("Head over size", headSize, this->size()); }

      VectorEigenDense<K> v(headSize, K(), acType_);
      copy(v, 0, this->size() - headSize);

      return v;
    }

    /*!
      Replace tail of a vector with a vector.
      \param[in] headSize size of the new vector
      \param[in] v Input vector
    */
    void head(number_t headSize, VectorEigenDense<K>& v)
    {
      if ((headSize < 0) || (headSize > this->size())) { overSize("Head over size", headSize, this->size()); }
      if (headSize != v.size()) { overSize("Vector over head", headSize, v.size()); }
      replace(v, 0, this->size() - headSize);
    }

    /*!
      Tail a vector, return result into a new vector
    
      \param[in] tailSize size of the new vector
      \return extracted tailing vector
    */
    VectorEigenDense<K> tail(number_t tailSize)
    {
      if (tailSize > this->size()) { overSize("Tail over size", tailSize, this->size()); }
      
      VectorEigenDense<K> v(tailSize, K(), acType_);
      copy(v, this->size() - tailSize, this->size());
      
      return v;
    }
    
    /*!
      Replace tail of a vector with a vector.
      \param[in] tailSize size of the new vector
      \param[in] v Input vector
    */
    void tail(number_t tailSize, VectorEigenDense<K>& v)
    {
      if (tailSize > this->size()) { overSize("Tail over size", tailSize, this->size()); }
      if (tailSize != v.size()) { overSize("Vector over tail", tailSize, v.size()); }
      replace(v, this->size() - tailSize, this->size());
    }
    
    /*!
      Extract segment of a vector
      \param[in] index beginning position of segment in vector
      \param[in] segmentLength segment length
    
      \return segment vector
    */
    VectorEigenDense<K> segment(const int_t index, const int_t segmentLength)
    {
      if ((index < 0) || (index > (int_t)this->size()) || ((index+segmentLength) > (int_t)this->size())) { indexOutOfRange("Tail over size", index, this->size()); }
      VectorEigenDense<K> seg(segmentLength, K(0.0), accessType());
      copy(seg, index, index+segmentLength);

      return seg;
    }

    //! compute Frobenius norm of a vector
    RealScalar normFrobenius() const
    {
      RealScalar anorm = RealScalar(0.0);
      for (cit_vk it = this->begin(); it !=this->end(); ++it) {
          anorm += NumTraits<K>::abs2(*it);
      }
      return std::sqrt(anorm);
    }

    /*!
      Fill segment of a vector with a vector
      \param[in] index beginning position of segment in vector
      \param[in] segmentLength segment length
      \param[in,out] seg vector to be modified
      
      \return segment vector
    */
    void segment(VectorEigenDense<K>& seg, const int_t index, const int_t segmentLength)
    {
      if ((index < 0) || (index > (int_t)this->size()) || ((index+segmentLength) > (int_t)this->size())) { indexOutOfRange("Tail over size", index, this->size()); }
      replace(seg, index, index+segmentLength);
    }

    /*!
      Calculate inner product (hermitian product) of two vectors:
      inner product = vectorX * vectorY
      \param[in] v the second vector (Y)
      \return inner product
    */
    K dotProduct(VectorEigenDense<K>& v) const
    {
      if ((v.size() != this->size())) { overSize("No same size", v.size(), this->size()); }
      cit_vk itb = this->begin(), it2b = v.begin(), it2e = v.end();
      return VectorAlgorithmSelector<K>::dot(it2b, it2e, itb);
    }
    
    //! Real part of vector
    VectorEigenDense<real_t> real()
    {
      VectorEigenDense<real_t> v(this->size(), real_t(0.0), acType_);
      cit_vk it_v = this->begin();
      typedef std::vector<real_t>::iterator it_Real;
      
      for(it_Real it = v.begin(); it != v.end(); it_v++, it++)
      {
        *it = NumTraits<K>::real(*it_v);
      }
      return v;
    }
    
    //! Minimum element within a range
    number_t minElement(number_t firstPos, number_t lastPos)
    {
      number_t first, last;
      if (firstPos >= lastPos)
      {
        first = lastPos;
        last = firstPos;
      }
      else
      {
        first = firstPos;
        last = lastPos;
      }
      if (NumTraits<K>::isReal())
      {
        if (last > (this->size() - 1)) { indexOutOfRange("Last index", last, this->size() - 1); }
        cit_vk itb = this->begin() + first, ite = this->begin() + last + 1, it;
        it = std::min_element(itb, ite);
        
        return (it - (this->begin()));
      }
      else
      { return first; }
    }
    
    //! Calculate sum of absolute value of all elements.
    real_t sumAbs()
    {
      real_t sum = 0.0;
      for (cit_vk it = this->begin(); it != this->end(); it++)
      {
        sum += std::abs(*it);
      }
      
      return sum;
    }
    
    //! set a vector to zero
    void setZero()
    {
      this->set(NumTraits<K>::zero());
    }
    
    //! Set a vector with a value
    void set(const K v)
    {
      for (it_vk it = this->begin(); it != this->end(); it++ )
      {
        *it = v;
      }
    }
    
    //! Normalize vector
    void normalize()
    {
      typedef typename NumTraits<K>::RealScalar RealScalar;
      RealScalar norm = this->norm2();
      *this /= norm;
    }
    
    //! Coefficient of vector
    K coeff(const number_t idx) const
    {
      if (idx >= this->size()) { indexOutOfRange("vector", idx, this->size()); }
      return (*this)[idx];
    }
    
    //! Reference of coefficient of vector
    K& coeffRef(const number_t idx)
    {
      if (idx >= this->size()) { indexOutOfRange("vector", idx, this->size()); }
      it_vk it = this->begin() + idx;
      return (*it);
    }
    
    //! return access type
    AccessType accessType() const
    {
      return acType_;
    }
    
    //! error message when incompatible sizes
    void overSize(const string_t& s, const size_t r, const size_t c) const
    {
      where(s);
      error("bad_dim", r, c);
    }
    
    //! error message when index out of range
    void indexOutOfRange(const string_t& s, const size_t index, const size_t range) const
    {
      where(s);
      error("index_out_of_range", index, range);
    }
    
  protected:
    /*!
      Copy a part of vector to another (smaller) vector:
      X = [x1 x2 x3 x4 x5]
      X.copy(Y, 0, 2)
      Y = [x1 x2 x3]
    
      \param[in,out] v destination vector
      \param[in] firstPos first position in range
      \param[in] lastPos last position in range
    */
    void copy(VectorEigenDense<K>& v, number_t firstPos, number_t lastPos)
    {
      cit_vk itb = this->begin() + firstPos;
      cit_vk ite = this->begin() + lastPos;
      it_vk itv = v.begin();
      std::copy(itb, ite, itv);
    }
    
    /*!
      Replace a part of vector by another (smaller) vector:
      X = [x1 x2 x3 x4 x5]
      Y = [y1 y2 y3]
      X.replace(Y, 0, 2)
      X = [y1 y2 y3 x4 x5]
    
      \param[in] v source vector
      \param[in] firstPos first position in range
      \param[in] lastPos last position in range
    */
    void replace(const VectorEigenDense<K>& v, number_t firstPos, number_t lastPos)
    {
      it_vk it = this->begin() + firstPos;
      cit_vk itb = v.begin(), ite = v.end();
      std::copy(itb, ite, it);
    }
    
  private:
    /*!
      adjoint a vector itself.
      This function can be used directly by changing into public, in this case, maybe some performances can be gained
    */
    void adjoint()
    {
      transpose();
      conjugate();
    }
    
    /*!
      Transpose a vector itself.
      This function can be used directly by changing into public, in this case, maybe some performances can be gained
    */
    void transpose()
    {
      if (_col == acType_)  { acType_ = _row; }
      else { acType_ = _col; }
    }
    
    /*!
      Conjugate vector itself.
      This function can be used directly by changing into public, in this case, maybe some performances can be gained
    */
    void conjugate()
    {
      typedef NumTraits<K> TraitType;
      if (!TraitType::isReal())
      {
        it_vk itb = this->begin();
        it_vk ite = this->end();
        conjTpl(itb, ite, itb);
      }
    }
    
  private:
    //! Vector is rowMajor or colMajor (column or row vector)
    AccessType acType_;
};

/*!
  Adjoint a vector

  \param[in] v source vector
  \return adjointed vector
*/
template<typename K>
VectorEigenDense<K> adjointVec(const VectorEigenDense<K>& v)
{
  VectorEigenDense<K> vec(v);
  vec.adjoint();
  return vec;
}

/*!
  Transpose a vector
  
  \param[in] v source vector
  \return transposed vector
*/
template<typename K>
VectorEigenDense<K> transposeVec(const VectorEigenDense<K>& v)
{
  VectorEigenDense<K> vec(v);
  vec.transpose();
  return vec;
}

/*!
  Conjugate a vector
  
  \param[in] v source vector
  \return conjugated vector
*/
template<typename K>
VectorEigenDense<K> conj(const VectorEigenDense<K>& v)
{
  VectorEigenDense<K> vec(v);
  vec.conjugate();
  return vec;
}

template<typename K, typename KK>
VectorEigenDense<K> operator*(const KK& k, const VectorEigenDense<K>& vec)
{
  VectorEigenDense<K> v(vec);
  typename VectorEigenDense<K>::it_vk it;
  
  for(it = v.begin(); it < v.end(); it++)
  {
    *it *= k;
  }
  return v;
}

template<typename K, typename KK>
VectorEigenDense<K> operator*(const VectorEigenDense<K>& vec, const KK& k)
{
  return k * vec;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<typename K>
VectorEigenDense<complex_t> cmplx(const VectorEigenDense<K>& v)
{
  VectorEigenDense<complex_t> cplVec(v.size(), complex_t(0, 0), v.accessType());
  typename VectorEigenDense<K>::cit_vk cit;
  VectorEigenDense<complex_t>::it_vk it = cplVec.begin();
  
  for (cit = v.begin(); cit != v.end(); cit++, it++)
  {
    *it = cmplx(*cit);
  }
  
  return cplVec;
}

#endif

template<typename K>
void transformVectorDense(VectorEigenDense<K>& v)
{
    std::reverse(v.begin(),v.end());
}

} 
#endif /* VECTOR_EIGEN_DENSE_HPP */
