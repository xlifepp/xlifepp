/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file Jacobi.hpp
  \author Manh Ha NGUYEN
  \since 22 Jan 2013
  \date  7 Mars 2013

  \brief Definition of the xlifepp::JacobiRotation class

  Class deals with Jacobi rotations.
*/

#ifndef EIGEN_JACOBI_HPP
#define EIGEN_JACOBI_HPP

#include "utils.h"

namespace xlifepp
{

/*!
  \class JacobiRotation
  \brief Rotation given by a cosine-sine pair.
  
  This class represents a Jacobi or Givens rotation.
  This is a 2D rotation in the plane \c J of angle \f$ \theta \f$ defined by
  its cosine \c c and sine \c s as follow:
  \f$ J = \left ( \begin{array}{cc} c & \overline s \\ -s  & \overline c \end{array} \right ) \f$
  
  You can apply the respective counter-clockwise rotation to a column vector \c v by
  applying its adjoint on the left: \f$ v = J^* v \f$ that translates to the following code:
  \code
  v.applyOnTheLeft(J.adjoint());
  \endcode
*/
template<typename Scalar>
class JacobiRotation
{
  public:
    typedef typename NumTraits<Scalar>::RealScalar RealScalar;
    
    //! Default constructor without any initialization.
    JacobiRotation() {}
    
    //! Construct a planar rotation from a cosine-sine pair (\a c, \c s).
    JacobiRotation(const Scalar& c, const Scalar& s) : c_(c), s_(s) {}
    
    Scalar& c() { return c_; } //!< returns cosine (non const)
    Scalar c() const { return c_; } //!< returns cosine (const)
    Scalar& s() { return s_; } //!< returns sine (non const)
    Scalar s() const { return s_; } //!< returns sine (const)
    
    //! Concatenates two planar rotation
    JacobiRotation operator*(const JacobiRotation& other)
    {
      return JacobiRotation(c_ * other.c_ - conj(s_) * other.s_,
                            conj(c_ * conj(other.s_) + conj(s_) * conj(other.c_)));
    }
    
    //! Returns the transposed transformation
    JacobiRotation transpose() const { return JacobiRotation(c_, -conj(s_)); }
    
    //! Returns the adjoint transformation
    JacobiRotation adjoint() const { return JacobiRotation(conj(c_), -s_); }
    
    bool makeJacobi(RealScalar x, Scalar y, RealScalar z); //!< computes Jacobi rotation
    
    void makeGivens(const Scalar& p, const Scalar& q, Scalar* z = 0); //!< computes Givens rotation
    
  protected:
    void makeGivens(const Scalar& p, const Scalar& q, Scalar* z, TrueType); //!< computes Givens rotation
    void makeGivens(const Scalar& p, const Scalar& q, Scalar* z, FalseType); //!< computes Givens rotation
    
    Scalar c_; //!< cosine of rotation angle
    Scalar s_; //!< sine of rotation angle
};

/*!
  Makes \c *this as a Jacobi rotation \a J such that applying \a J on both the right and left sides of the selfadjoint 2x2 matrix
  \f$ B = \left ( \begin{array}{cc} x & y \\ \overline y & z \end{array} \right )\f$ yields a diagonal matrix \f$ A = J^* B J \f$
*/
template<typename Scalar>
bool JacobiRotation<Scalar>::makeJacobi(RealScalar x, Scalar y, RealScalar z)
{
  typedef typename NumTraits<Scalar>::Real RealScalar;
  if(y == Scalar(0))
  {
    c_ = Scalar(1);
    s_ = Scalar(0);
    return false;
  }
  else
  {
    RealScalar tau = (x - z) / (RealScalar(2) * std::abs(y));
    RealScalar w = std::sqrt(NumTraits<Scalar>::abs2(tau) + RealScalar(1));
    RealScalar t;
    if(tau > RealScalar(0))
    {
      t = RealScalar(1) / (tau + w);
    }
    else
    {
      t = RealScalar(1) / (tau - w);
    }
    RealScalar sign_t = t > RealScalar(0) ? RealScalar(1) : RealScalar(-1);
    RealScalar n = RealScalar(1) / std::sqrt(NumTraits<Scalar>::abs2(t) + RealScalar(1));
    s_ = - sign_t * (conj(y) / std::abs(y)) * std::abs(t) * n;
    c_ = n;
    return true;
  }
}

/*!
  Makes \c *this as a Givens rotation \c G such that applying \f$ G^* \f$ to the left of the vector
  \f$ V = \left ( \begin{array}{c} p \\ q \end{array} \right )\f$ yields:
  \f$ G^* V = \left ( \begin{array}{c} r \\ 0 \end{array} \right )\f$.
  
  The value of \a z is returned if \a z is not null (the default is null).
  Also note that G is built such that the cosine is always real.
  
  This function implements the continuous Givens rotation generation algorithm
  found in Anderson (2000), Discontinuous Plane Rotations and the Symmetric Eigenvalue Problem.
  LAPACK Working Note 150, University of Tennessee, UT-CS-00-454, December 4, 2000.
*/
template<typename Scalar>
void JacobiRotation<Scalar>::makeGivens(const Scalar& p, const Scalar& q, Scalar* z)
{
  makeGivens(p, q, z, typename Conditional<NumTraits<Scalar>::IsComplex, TrueType, FalseType>::type());
}


//! specialization for complexes
template<typename Scalar>
void JacobiRotation<Scalar>::makeGivens(const Scalar& p, const Scalar& q, Scalar* r, TrueType)
{
  if(q == Scalar(0))
  {
    c_ = NumTraits<Scalar>::real(p) < 0 ? Scalar(-1) : Scalar(1);
    s_ = 0;
    if(r) { *r = c_ * p; }
  }
  else if(p == Scalar(0))
  {
    c_ = 0;
    s_ = -q / std::abs(q);
    if(r) { *r = std::abs(q); }
  }
  else
  {
    RealScalar p1 = norm1(p);
    RealScalar q1 = norm1(q);
    if(p1 >= q1)
    {
      Scalar ps = p / p1;
      RealScalar p2 = NumTraits<Scalar>::abs2(ps);
      Scalar qs = q / p1;
      RealScalar q2 = NumTraits<Scalar>::abs2(qs);
      
      RealScalar u = std::sqrt(RealScalar(1) + q2 / p2);
      if(NumTraits<Scalar>::real(p) < RealScalar(0))
      { u = -u; }
      
      c_ = Scalar(1) / u;
      s_ = -qs * conj(ps) * (c_ / p2);
      if(r) { *r = p * u; }
    }
    else
    {
      Scalar ps = p / q1;
      RealScalar p2 = NumTraits<Scalar>::abs2(ps);
      Scalar qs = q / q1;
      RealScalar q2 = NumTraits<Scalar>::abs2(qs);
      
      RealScalar u = q1 * std::sqrt(p2 + q2);
      if(NumTraits<Scalar>::real(p) < RealScalar(0))
      { u = -u; }
      
      p1 = std::abs(p);
      ps = p / p1;
      c_ = p1 / u;
      s_ = -conj(ps) * (q / u);
      if(r) { *r = ps * u; }
    }
  }
}

//! specialization for reals
template<typename Scalar>
void JacobiRotation<Scalar>::makeGivens(const Scalar& p, const Scalar& q, Scalar* r, FalseType)
{

  if(q == Scalar(0))
  {
    c_ = p < Scalar(0) ? Scalar(-1) : Scalar(1);
    s_ = Scalar(0);
    if(r) { *r = std::abs(p); }
  }
  else if(p == Scalar(0))
  {
    c_ = Scalar(0);
    s_ = q < Scalar(0) ? Scalar(1) : Scalar(-1);
    if(r) { *r = std::abs(q); }
  }
  else if(std::abs(p) > std::abs(q))
  {
    Scalar t = q / p;
    Scalar u = std::sqrt(Scalar(1) + NumTraits<Scalar>::abs2(t));
    if(p < Scalar(0))
    { u = -u; }
    c_ = Scalar(1) / u;
    s_ = -t * c_;
    if(r) { *r = p * u; }
  }
  else
  {
    Scalar t = p / q;
    Scalar u = std::sqrt(Scalar(1) + NumTraits<Scalar>::abs2(t));
    if(q < Scalar(0))
    { u = -u; }
    s_ = -Scalar(1) / u;
    c_ = -t * s_;
    if(r) { *r = q * u; }
  }
  
}

} // end namespace xlifepp

#endif // EIGEN_JACOBI_HPP
