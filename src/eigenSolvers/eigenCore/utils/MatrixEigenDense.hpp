/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file  MatrixEigenDense.hpp
  \author Manh Ha NGUYEN
  \since 22 Jan 2013
  \date  7 Mars 2013
  
  \brief Definition of the xlifepp::MatrixEigenDense
 */

#ifndef MATRIX_EIGEN_DENSE_HPP
#define MATRIX_EIGEN_DENSE_HPP

#include "utils.h"
#include "VectorEigenDense.hpp"
#include "Jacobi.hpp"

namespace xlifepp
{

template<typename Scalar>
class JacobiRotation;

/*!
  \class MatrixEigenDense
  \brief Class (row) dense matrix to deal with direct eigen problem solver
 
  This class provides some basic methods to support calculation of eigen problem dense matrix.
 
  \tparam Type of scalar (real_t, complex_t)
*/
template<typename K>
class MatrixEigenDense : public Matrix<K>
{
  public:
    typedef typename MatrixEigenDense<K>::type_t Scalar;
    typedef typename NumTraits<Scalar>::RealScalar RealScalar;
    typedef typename std::vector<K>::iterator it_vk;
    typedef typename std::vector<K>::const_iterator cit_vk;
    
    MatrixEigenDense() : Matrix<K>(), col_(1) {} //!< default constructor
    MatrixEigenDense(const dimen_t r) : Matrix<K>(r, r), col_(r) {} //!< constructor from number of rows
    MatrixEigenDense(const dimen_t r, const dimen_t c) : Matrix<K>(r, c), col_(c) {} //!< constructor from dimensions
    MatrixEigenDense(const dimen_t r, const dimen_t c, const K v) : Matrix<K>(r, c, v), col_(c) {} //!< ocnstructor from dimensions and value
    MatrixEigenDense(const MatrixEigenDense<K>& mat) : Matrix<K>() //! copy constructor
    {
      this->resize(mat.numOfRows()*mat.numOfCols(), K(0.0));
      col_ = mat.col_;
      this->rows_ = mat.numOfRows();
      it_vk itMat = this->begin();
      for (cit_vk it = mat.begin(); it != mat.end(); it++, itMat++)
      {
        *itMat = *it;
      }
    }
    
    MatrixEigenDense<K>& operator=(const MatrixEigenDense<K>& mat) //! assignment operator
    {
      this->resize(mat.numOfRows()*mat.numOfCols(), K(0.0));
      col_ = mat.col_;
      this->rows_ = mat.numOfRows();
      it_vk itMat = this->begin();
      for (cit_vk it = mat.begin(); it != mat.end(); it++, itMat++)
      {
        *itMat = *it;
      }
      
      return *this;
    }
    
    /*!
      Construct matrix from an existing matrix (Extract a sub-matrix from an existing one)
      This is a very simple version which uses copy operation, and needs optimizing
      
      \param[in] mat Existing matrix from which we extract sub-matrix
      \param[in] rIdx Row position (top left corner) of matrix at which sub-matrix is extracted
      \param[in] cIdx Column position (top left corner) of matrix at which sub-matrix is extracted
      \param[in] rSize Number of row of sub-matrix
      \param[in] cSize Number of column sub-matrix
    */
    MatrixEigenDense(const MatrixEigenDense<K>& mat, const int_t rIdx, const int_t cIdx, const dimen_t rSize, const dimen_t cSize) : Matrix<K>(rSize, cSize), col_(cSize)
    {
      int_t r = mat.numOfRows();
      int_t c = mat.numOfCols();
      if (rSize > r) { this->mismatchDims("Row mismatch", rSize, r); }
      if (cSize > c) { this->mismatchDims("Column mismatch", cSize, c); }
      if ((rSize - rIdx) > r) { indexOutOfRange("Row matrix", rIdx, r); }
      if ((cSize - cIdx) > c) { indexOutOfRange("Column matrix", cIdx, c); }
      cit_vk itMatb = mat.begin();
      cit_vk itMat = itMatb + (c * rIdx + cIdx);
      it_vk it = (this)->begin();
      
      for (int_t i = 0; i < rSize; i++)
      {
        for (int_t j = 0; j < cSize; j++)
        {
          *it = *itMat;
          it++;
          itMat++;
        }
        itMat += (c - cSize);
      }
    }
    
  public:
    //    In the future, we should find a way use a temporary memory workspace (globally) to gain performance.
    //    But now, we use temporary object (locally) and it costs rather expensive.
    //    void applyHouseholderOnTheLeft(const VectorEigenDense<K>& essential,
    //            const K& tau,
    //            K* workspace)
    
    /*!
      Apply the elementary reflector H given by
      \f$ H = I - tau v v^*\f$
      with
      \f$ v^T = [1 essential^T] \f$
      from the left to a matrix.
      
      On input:
      \param essential the essential part of the vector \c v
      \param tau the scaling factor of the Householder transformation
    */
    void applyHouseholderOnTheLeft(const VectorEigenDense<K>& essential, const K& tau)
    {
      if (NumTraits<K>::zero() != tau) {
        if(1 == numOfRows())
        {
          *this *= (K(1) - tau);
        }
        else
        {
          Indexing idx(4);
          idx[0] = 1; idx[1] = 0;
          idx[2] = numOfRows() - 1; idx[3] = numOfCols();
          //MatrixEigenDense<K> bottom(*this, 1, 0, *numOfRows()()-1, *numOfCols);
          //Map<typename internal::plain_row_type<PlainObject>::type> tmp(workspace,cols());
          VectorEigenDense<K> temp(numOfCols(), K(0.0), _row);

          //Block<Derived, EssentialPart::SizeAtCompileTime, Derived::ColsAtCompileTime> bottom(derived(), 1, 0, rows()-1, cols());
          //temp = essential.adjointVector() * bottom;
          multVecSubMatVec(idx, adjointVec(essential), temp);
          //this->row(0) -= tau * temp;
          temp += rowVector(0);
          addAssignRow(0, temp, -tau);

          //bottom.noalias() -= tau * essential * tmp;
          multVecVecSubMatAdditionAssign(idx, essential, temp, -tau);
        }
      }
    }
    
    /*!
      Apply the elementary reflector H given by
      \f$ H = I - tau v v^*\f$
      with
      \f$ v^T = [1 essential^T] \f$
      from the right to a matrix.
      
      On input:
      \param essential the essential part of the vector \c v
      \param tau the scaling factor of the Householder transformation
    */
    void applyHouseholderOnTheRight(const VectorEigenDense<K>& essential, const K& tau)
    {
      if (NumTraits<K>::zero() != tau) {
        if(1 == numOfCols())
        {
          *this *= (K(1.0) - tau);
        }
        else
        {
          Indexing idx(4);
          idx[0] = 0; idx[1] = 1;
          idx[2] = numOfRows(); idx[3] = numOfCols() - 1;
          //Map<typename internal::plain_col_type<PlainObject>::type> tmp(workspace,rows());
          VectorEigenDense<K> temp(numOfRows(), K(0.0));
          //Block<Derived, Derived::RowsAtCompileTime, EssentialPart::SizeAtCompileTime> right(derived(), 0, 1, rows(), cols()-1);
          //tmp.noalias() = right * essential.conjugate();
          multSubMatVecVec(idx, conj(essential), temp);//

          temp += columnVector(0);
          //this->col(0) -= tau * tmp;
          addAssignCol(0, temp, -tau);
          //right.noalias() -= tau * tmp * essential.transpose();
          multVecVecSubMatAdditionAssign(idx, temp, transposeVec(essential), -tau);
        }
      }
    }
    
    /*!
      Computes the product of a sub-matrix and a vector with:
      Matrix
            A | B
            C | D
      Vector X, Y
      Product: D x X = Y
     
      \param[in] idx index specify block of sub-matrix inside the main matrix.
                 idx[0]: row position of top-left point
                 idx[1]: col position of top-left point
                 idx[2]: number of row of sub-matrix
                 idx[3]: number of column of sub-matrix
      \param[in] vec vector X
      \param[out] res vector Y: the result of D * X
    */
    template<typename T>
    void multSubMatVecVec(const Indexing& idx, const VectorEigenDense<T>& vec, VectorEigenDense<T>& res)
    {
      number_t c = numOfCols();
      verifySize(idx);
      if ((idx[3] != vec.size()) || (_col != vec.accessType())) { this->mismatchDims("multSubMatVecVec:Column mismatch", idx[3], vec.size()); }
      if ((idx[2] != res.size()) || (_col != res.accessType())) { this->mismatchDims("multSubMatVecVec:Row mismatch", idx[2], res.size()); }
      
      cit_vk itMat = this->begin() + (c * idx[0] + idx[1]);
      typename VectorEigenDense<T>::cit_vk itVec = vec.begin();
      typename VectorEigenDense<T>::it_vk itRes = res.begin();
      
      for(itRes = res.begin(); itRes != res.end(); itRes++)
      {
        *itRes = T(0.0);
        *itRes = inner_product(itMat, itMat + idx[3], itVec, *itRes);
        itMat += c;
      }
    }
    
    /*!
      Computes the product of a vector and sub-matrix with:
      Matrix
            A | B
            C | D
      Vector X, Y
      Product: X * D = Y
     
      \param[in] idx index specify block of sub-matrix inside the main matrix.
                 idx[0]: row position of top-left point
                 idx[1]: column position of top-left point
                 idx[2]: number of row of sub-matrix
                 idx[3]: number of column of sub-matrix
      \param[in] vec vector X
      \param[out] res vector Y: the result of X * D
    */
    template<typename T>
    void multVecSubMatVec(const Indexing& idx, const VectorEigenDense<T>& vec, VectorEigenDense<T>& res)
    {
      number_t c = numOfCols();
      verifySize(idx);
      if ((idx[2] != vec.size()) || (_row != vec.accessType())) { this->mismatchDims("multVecSubMatVec:Row mismatch", idx[2], vec.size()); }
      if ((idx[3] != res.size()) || (_row != res.accessType())) { this->mismatchDims("multVecSubMatVec:Column mismatch", idx[3], res.size()); }
      
      cit_vk itMat = this->begin() + (c * idx[0] + idx[1]), itMatIdx, itVec;
      typename VectorEigenDense<T>::it_vk itRes;
      
      for(itRes = res.begin(); itRes != res.end(); itRes++, itMat++)
      {
        *itRes = T(0.0);
        itMatIdx = itMat;
        
        for(itVec = vec.begin(); itVec != vec.end(); itVec++, itMatIdx += c)
        {
          *itRes += *itVec * *itMatIdx;
        }
      }
    }
    
    
    /*!
      Computes the product of a vector and a vector, result is returned into a sub-matrix with:
      Matrix
            A | B
            C | D
      Vector X, Y
      Product: D= X * Y * scale
      The content of sub-matrix will be replaced with the new one!!!!
      
      \param[in] idx index specify block of sub-matrix inside the main matrix.
                 idx[0]: row position of top-left point
                 idx[1]: column position of top-left point
                 idx[2]: number of row of sub-matrix
                 idx[3]: number of column of sub-matrix
      \param[in] vec1 vector X
      \param[in] vec2 vector Y
    */
    void multVecVecSubMat(const Indexing& idx, const VectorEigenDense<K>& vec1, const VectorEigenDense<K>& vec2)
    {
      number_t c = numOfCols();
      verifySize(idx);
      if ((idx[2] != vec1.size()) || (_col != vec1.accessType())) { this->mismatchDims("multVecVecSubMat:Row mismatch", idx[2], vec1.size()); }
      if ((idx[3] != vec2.size()) || (_row != vec2.accessType())) { this->mismatchDims("multVecVecSubMat:Column mismatch", idx[3], vec2.size()); }
      
      it_vk itMat = this->begin() + (c * idx[0] + idx[1]), itMatIdx;
      cit_vk itVec1 = vec1.begin(), itVec2;
      
      for(number_t i = 0; i < idx[2]; i++)
      {
        itMatIdx = itMat + i * c;
        itVec2 = vec2.begin();
        for (number_t j = 0; j < idx[3]; j++, itMatIdx++, itVec2++)
        {
          *itMatIdx = *itVec1 * *itVec2;
        }
        itVec1++;
      }
    }
    
    /*!
      Computes the product of a vector and a vector, then addition assignment
          and the result is returned into a sub-matrix with:
      Matrix
            A | B
            C | D
      Vector X, Y
      Product: D += X * Y * scale
      The content of sub-matrix will be replaced with a new one!!!!
      
      \param[in] idx index specify block of sub-matrix inside the main matrix.
                 idx[0]: row position of top-left point
                 idx[1]: column position of top-left point
                 idx[2]: number of row of sub-matrix
                 idx[3]: number of column of sub-matrix
      \param[in] vec1 vector X
      \param[in] vec2 vector Y
      \param[in] scale scale factor
    */
    void multVecVecSubMatAdditionAssign(const Indexing& idx, const VectorEigenDense<K>& vec1, const VectorEigenDense<K>& vec2, const K& scale)
    {
      number_t c = numOfCols();
      verifySize(idx);
      if ((idx[2] != vec1.size()) || (_col != vec1.accessType())) { this->mismatchDims("multVecVecSubMatAdditionAssign:Row mismatch", idx[2], vec1.size()); }
      if ((idx[3] != vec2.size()) || (_row != vec2.accessType())) { this->mismatchDims("multVecVecSubMatAdditionAssign:Column mismatch", idx[3], vec2.size()); }
      
      it_vk itMat = this->begin() + (c * idx[0] + idx[1]), itMatIdx;
      cit_vk itVec1 = vec1.begin(), itVec2;
      
      for(number_t i = 0; i < idx[2]; i++)
      {
        itMatIdx = itMat + i * c;
        itVec2 = vec2.begin();
        for (number_t j = 0; j < idx[3]; j++, itMatIdx++, itVec2++)
        {
          *itMatIdx += scale * *itVec1 * *itVec2;
        }
        itVec1++;
      }
    }
    
    /*!
      Computes the submatrix += scale * vec1*vec2^T + conj(scale)*vec2*vec1^T
      Matrix
            A | B
            C | D
      Vector X, Y
      Product: D += scaleX * X * Y^T + conjugate(scale) * Y * X^T
      The content of sub-matrix will be replaced with a new one!!!!
      This implementation can be optimized!!!
      
      \param[in] idx index specify block of sub-matrix inside the main matrix.
                 idx[0]: row position of top-left point
                 idx[1]: column position of top-left point
                 idx[2]: number of row of sub-matrix
                 idx[3]: number of column of sub-matrix
      \param[in] vec1 colum vector X
      \param[in] vec2 colum vector Y
      \param[in] scale scale factor
    */
    void rankUpdate(const Indexing& idx, const VectorEigenDense<K>& vec1, const VectorEigenDense<K>& vec2, const K& scale)
    {
      number_t c = numOfCols();
      verifySize(idx);
      if (vec1.size() != vec2.size()) { this->mismatchDims("rankUpdate::VectorMisMatch", vec1.size(), vec2.size()); }
      if (idx[2] != vec1.size()) { this->mismatchDims("rankUpdate::Row mismatch", idx[2], vec1.size()); }
      
      it_vk itMat = this->begin() + (c * idx[0] + idx[1]), itMatIdx;
      cit_vk itVec1 = vec1.begin(), itVec1Extra;
      cit_vk itVec2 = vec2.begin(), itVec2Extra;
      
      for(number_t i = 0; i < idx[2]; i++)
      {
        itMatIdx = itMat + i * c;
        itVec1Extra = vec1.begin();
        itVec2Extra = vec2.begin();
        for (number_t j = 0; j < idx[3]; j++, itMatIdx++, itVec1Extra++, itVec2Extra++)
        {
          *itMatIdx += (scale) * (*itVec1) * conj(*itVec2Extra) + conj(scale) * (*itVec2) * conj(*itVec1Extra);
        }
        itVec1++;
        itVec2++;
      }
    }
    
    MatrixEigenDense<K>& operator-=(const MatrixEigenDense<K>& mat)
    {
      if (mat.numOfRows() != numOfRows()) { this->mismatchDims("Row mismatch", mat.numOfRows(), numOfRows()); }
      if (mat.numOfCols() != numOfCols()) { this->mismatchDims("Column mismatch", mat.numOfCols(), numOfCols()); }
      
      cit_vk itMat = mat.begin();
      for (it_vk it = this->begin(); it != this->end(); it++, itMat++)
      {
        *it -= *itMat;
      }
      
      return *this;
    }
    
    /*!
      Extract the bottom right corner sub-matrix
      
      \param[in] rowSize number of row of sub-matrix
      \param[in] colSize number of row of sub-matrix
      \return sub-matrix
    */
    MatrixEigenDense bottomRightCorner(const Index rowSize, const Index colSize)
    {
      if (rowSize > numOfRows()) { indexOutOfRange("bottomRightCorner:Row", rowSize, numOfRows()); }
      if (colSize > numOfCols()) { indexOutOfRange("bottomRightCorner:Column", colSize, numOfCols()); }
      MatrixEigenDense<K> mat(*this, numOfRows() - rowSize, numOfCols() - colSize, rowSize, colSize);
      
      return mat;
    }
    
    /*!
      Replace the bottom right corner of a matrix with a matrix
      
      \param[in] rowSize number of row of sub-matrix
      \param[in] colSize number of row of sub-matrix
      \param[in] subMat matrix to replace
    */
    void bottomRightCorner(const Index rowSize, const Index colSize, MatrixEigenDense<K>& subMat)
    {
      if (rowSize > numOfRows()) { indexOutOfRange("bottomRightCorner:Row", rowSize, numOfRows()); }
      if (colSize > numOfCols()) { indexOutOfRange("bottomRightCorner:Column", colSize, numOfCols()); }
      
      dimen_t r = this->numOfRows();
      dimen_t c = this->numOfCols();
      dimen_t rIdx = r - rowSize;
      dimen_t cIdx = c - colSize;
      
      it_vk itMat = this->begin() + (c * rIdx + cIdx);
      cit_vk it = subMat.begin();
      
      for (Index i = 0; i < rowSize; i++)
      {
        for (Index j = 0; j < colSize; j++)
        {
          *itMat = *it;
          it++;
          itMat++;
        }
        itMat += cIdx;
      }
    }
    
    /*!
      Replace content of a block of a matrix with another matrix
      
      \param[in] mat replacing matrix
      \param[in] rIdx row top left corner index
      \param[in] cIdx column top left corner index
      \param[in] rSize row of replaced block (and replacing matrix)
      \param[in] cSize column of replaced block (and replacing matrix)
    */
    void replace(const MatrixEigenDense<K>& mat, const int_t rIdx, const int_t cIdx, const int_t rSize, const int_t cSize)
    {
      int_t r = numOfRows();
      int_t c = numOfCols();
      if (rSize > r) { this->mismatchDims("Row mismatch", rSize, r); }
      if (cSize > c) { this->mismatchDims("Column mismatch", cSize, c); }
      if ((rSize - rIdx) > r) { indexOutOfRange("Row matrix", rIdx, r); }
      if ((cSize - cIdx) > c) { indexOutOfRange("Column matrix", cIdx, c); }
      
      it_vk it = this->begin() + (c * rIdx + cIdx);
      cit_vk itMat = mat.begin();
      
      for (int_t i = 0; i < rSize; i++)
      {
        for (int_t j = 0; j < cSize; j++)
        {
          *it = *itMat;
          it++;
          itMat++;
        }
        it += (c - cSize);
      }
    }
    
    /*!
      Zeroing the lower part of a matrix
      TODO Change to large matrix to have better performance?
    */
    void setZeroLowerTriangle()
    {
      int_t r = std::min(numOfRows(), numOfCols());
      int_t c = numOfCols();
      
      it_vk it = this->begin();
      for (int_t i = 0; i < r; i++)
      {
        for (int_t j = 0; j < i + 1; j++)
        {
          *it = NumTraits<K>::zero();
          it++;
        }
        it += (c - i - 1);
      }
    }
    
    /*!
      Zeroing the strict upper of a matrix
      TODO Change to large matrix to have better performance?
    */
    void setZeroStrictUpper()
    {
      dimen_t r = numOfRows();
      dimen_t c = numOfCols();
      
      it_vk it = this->begin() + 1;
      for (number_t i = 0; i < r; i++)
      {
        for (number_t j = i + 1; j < c; j++)
        {
          *it = NumTraits<K>::zero();
          it++;
        }
        it += (i + 2);
      }
    }
    
    /*!
      Computes the addition assignment a row r of matrix A and the (row) vector X
      row(r) += scale * X
      Content of the row will be replaced with a new one!!!!
      
      \param[in] r row index
      \param[in] row (row) vector X
      \param[in] scale scale factor
    */
    void addAssignRow(const dimen_t r, const VectorEigenDense<K>& row, K scale)
    {
      if (r >= numOfRows()) { indexOutOfRange("row of matrix", r, numOfRows()); }
      if ((numOfCols() != row.size())) { sizeMisMatch("Matrix column and vector size", numOfCols(), row.size()); }
      it_vk itMat = this->begin() + (r) * numOfCols();
      cit_vk itVec;
      
      for (itVec = row.begin(); itVec != row.end(); itVec++, itMat++)
      {
        *itMat += scale * *itVec;
      }
    }
    
    /*!
      Computes the addition assignment of column c of matrix A and (column) vector X
      column(c) += scale * X
      Content of the column will be replaced with a new one!!!!
      
      \param[in] c column index
      \param[in] col (column) vector X
      \param[in] scale scale factor
    */
    void addAssignCol(const dimen_t c, const VectorEigenDense<K>& col, K scale)
    {
      if (c >= numOfCols()) { indexOutOfRange("column of matrix", c, numOfCols()); }
      if ((numOfRows() != col.size())) { sizeMisMatch("Matrix row and vector size", numOfRows(), col.size()); }
      
      int cols = numOfCols();
      it_vk it = this->begin() + c;
      
      for(cit_vk it_v = col.begin(); it_v != col.end(); it += cols, it_v++)
      {
        *it += scale * *it_v;
      }
    }
    
    /*!
      Return a row of matrix (row index begins from 0)
      \param[in] r row to return
      \return r-th row of matrix
    */
    VectorEigenDense<K> rowVector(const dimen_t r) const
    {
      if (r >= numOfRows()) { indexOutOfRange("row of matrix", r, numOfRows()); }
      
      VectorEigenDense<K> v(numOfCols(), K(0.0), _row);
      cit_vk itMat = this->begin() + (r) * this->hsize();
      it_vk itVec;
      for (itVec = v.begin(); itVec != v.end(); itVec++, itMat++)
      {
        *itVec = *itMat;
      }
      
      return v;
    }
    
    
    /*!
      Set a row of matrix (beginning from 0) with a row vector
      \param[in] r row to be set
      \param[in] v row vector
    */
    void rowVector(const dimen_t r, VectorEigenDense<K>& v)
    {
      if (r >= numOfRows()) { indexOutOfRange("row of matrix", r, numOfRows()); }
      if(v.size() != numOfCols()) { sizeMisMatch("rowVector: Vector size and matrix column", v.size(), numOfCols()); }
      
      it_vk it = this->begin() + (r) * numOfCols();
      for(cit_vk it_v = v.begin(); it_v != v.end(); it_v++, it++)
      {
        *it = *it_v;
      }
    }
    
    /*!
      Return a column of matrix (column index begins from 0)
      \param[in] c column to return
      \return c-th column of matrix
    */
    VectorEigenDense<K> columnVector(const dimen_t c) const
    {
      if (c >= numOfCols()) { indexOutOfRange("column of matrix", c, numOfCols()); }
      
      VectorEigenDense<K> v(numOfRows());
      int_t cols = numOfCols();
      cit_vk it = this->begin() + c;
      
      for(it_vk it_v = v.begin(); it_v != v.end(); it += cols, it_v++)
      {
        *it_v = *it;
      }
      
      return v;
    }
    
    /*!
      Set a column of matrix with a column vector
      \param[in] c column to be set
      \param[in] v column vector
    */
    void columnVector(const dimen_t c, VectorEigenDense<K>& v)
    {
      if (c >= numOfCols()) { indexOutOfRange("column of matrix", c, numOfCols()); }
      if ((numOfRows() != v.size())) { sizeMisMatch("columnVector: Matrix row and vector size", numOfRows(), v.size()); }
      
      int_t cols = numOfCols();
      it_vk it = this->begin() + c;
      
      for(cit_vk it_v = v.begin(); it_v != v.end(); it += cols, it_v++)
      {
        *it = *it_v;
      }
    }
    
    /*!
      Swap two rows: row1 <--> row2
      \param[in] row1 first row to swap
      \param[in] row2 second row to swap
    */
    void swapRows(number_t row1, number_t row2)
    {
      if (row1 >= numOfRows()) { indexOutOfRange("row of matrix", row1, numOfRows()); }
      if (row2 >= numOfRows()) { indexOutOfRange("row of matrix", row2, numOfRows()); }
      if (row1 != row2)
      {
        it_vk itRow1First = this->begin() + row1 * numOfCols();
        it_vk itRow1Last = itRow1First + numOfCols();
        it_vk itRow2First = this->begin() + row2 * numOfCols();
        
        std::swap_ranges(itRow1First, itRow1Last, itRow2First);
      }
    }
    
    /*!
      Swap two columns: col1 <--> col2
      \param[in] col1 first column to swap
      \param[in] col2 second column to swap
    */
    void swapCols(number_t col1, number_t col2)
    {
      if (col1 >= numOfCols()) { indexOutOfRange("column of matrix", col1, numOfCols()); }
      if (col2 >= numOfCols()) { indexOutOfRange("column of matrix", col2, numOfCols()); }
      if (col1 != col2)
      {
        it_vk itCol1 = this->begin() + col1;
        it_vk itCol2 = this->begin() + col2;
        K temp;
        for (number_t i = 0; i < numOfRows(); ++i)
        {
          temp = *itCol1;
          *itCol1 = *itCol2;
          *itCol2 = temp;
          itCol1 += numOfCols();
          itCol2 += numOfCols();
        }
      }
    }
    
    /*!
      Set diagonal of a matrix with a value.
      \param[in] v value to set
    */
    void diagonal(K v)
    {
      if(numOfCols() != numOfRows()) { this->nonSquare("Setting diagonal", numOfRows(), numOfCols()); }
      
      number_t rows = numOfRows();
      number_t cols = numOfCols();
      it_vk it_v = this->begin();
      
      for(number_t i = 0; i < rows; ++i)
      {
        *(it_v + i) = v;
        it_v += cols;
      }
    }
    
    /*!
      Return diagonal of a matrix in a column vector.
     */
    VectorEigenDense<K> diagonal() const
    {
      if(numOfCols() != numOfRows()) { this->nonSquare("Extracting diagonal", numOfRows(), numOfCols()); }
      VectorEigenDense<K> diag(numOfRows());
      
      number_t rows = numOfRows();
      number_t cols = numOfCols();
      cit_vk it_v = this->begin();
      it_vk it = diag.begin();
      
      for(number_t i = 0; i < rows; ++i)
      {
        *it = *(it_v + i);
        it++;
        it_v += cols;
      }
      
      return diag;
    }
    
    /*!
      Return (lower) sub-diagonal of a matrix in a column vector.
    */
    VectorEigenDense<K> subDiagonal() const
    {
      if(numOfCols() != numOfRows()) { this->nonSquare("Extracting (lower) sub-diagonal", numOfRows(), numOfCols()); }
      VectorEigenDense<K> subdiag(numOfRows() - 1);
      
      int_t rows = numOfRows() - 1;
      int_t cols = numOfCols();
      cit_vk it_v = this->begin() + cols;
      it_vk it = subdiag.begin();
      
      for(int_t i = 0; i < rows; ++i)
      {
        *it = *(it_v + i);
        it++;
        it_v += cols;
      }
      
      return subdiag;
    }
    
    RealScalar normFrobenius() const
    {
      RealScalar anorm = RealScalar(0.0);
      for (cit_vk it = this->begin(); it !=this->end(); ++it) {
          anorm += NumTraits<K>::abs2(*it);
      }
      return std::sqrt(anorm);
    }

    RealScalar normOne()
    {
      RealScalar anorm = NumTraits<RealScalar>::zero();
      for (number_t i = 0; i < numOfCols(); ++i) {
          RealScalar sum = NumTraits<RealScalar>::zero();
          for (number_t j = 0; j < numOfRows(); ++j) {
              sum += NumTraits<K>::magnitude(coeff(j,i));
          }
          if (sum > anorm) anorm = sum;
      }

      return anorm;
    }

    /*!
      Return maximum absolute coefficient of a matrix
    */
    real_t maxCoeff() const
    {
      cit_vk itb = this->begin(), ite = this->end(), it;
      it = maxElementTpl(itb, ite);
      
      return real_t(std::abs(*it));
    }
    
    /*!
      Return minimum absolute coefficient of a matrix
    */
    real_t minCoeff() const
    {
      cit_vk itb = this->begin(), ite = this->end(), it;
      it = minElementTpl(itb, ite);
      
      return real_t(std::abs(*it));
    }
    
    /*!
      Extract a block of a row of a matrix
      \param rowIdx row index (beginning position of block)
      \param colIdx column index (beginning position of block)
      \param size size of (row) block
     
      \return (row) vector
    */
    VectorEigenDense<K> blockRow(int_t rowIdx, int_t colIdx, int_t size) const
    {
      if ((rowIdx < 0) || (rowIdx >= numOfRows())) { indexOutOfRange("row of matrix", rowIdx, numOfRows()); }
      if ((colIdx < 0) || (colIdx >= numOfCols())) { indexOutOfRange("column of matrix", colIdx, numOfCols()); }
      if ((colIdx + size) > numOfCols()) { sizeMisMatch("block size", (colIdx + size), numOfCols()); }
      
      VectorEigenDense<K> v(size, 0, _row);
      cit_vk itMat = this->begin() + rowIdx * numOfCols() + colIdx;
      
      for (it_vk it = v.begin(); it != v.end(); it++, itMat++)
      {
        *it = *itMat;
      }
      
      return v;
    }
    
    /*!
      Extract a block of a column of a matrix
      \param rowIdx row index (beginning position of block)
      \param colIdx column index (beginning position of block)
      \param size size of (column) block
     
      \return (column) vector
    */
    VectorEigenDense<K> blockCol(int_t rowIdx, int_t colIdx, int_t size) const
    {
      if ((rowIdx < 0) || (rowIdx >= numOfRows())) { indexOutOfRange("row of matrix", rowIdx, numOfRows()); }
      if ((colIdx < 0) || (colIdx >= numOfCols())) { indexOutOfRange("column of matrix", colIdx, numOfCols()); }
      if ((rowIdx + size) > numOfRows()) { sizeMisMatch("block size column", (rowIdx + size), numOfRows()); }
      
      VectorEigenDense<K> v(size);
      cit_vk itMat = this->begin() + rowIdx * numOfCols() + colIdx;
      
      for (it_vk it = v.begin(); it != v.end(); it++)
      {
        *it = *itMat;
        itMat += numOfCols();
      }
      
      return v;
    }
    
    /*!
      \brief Standard Cholesky decomposition (LL^T) of a matrix and associated features
     
      This function performs a LL^T Cholesky decomposition of a symmetric, positive definite
      matrix A such that A = LL^* = U^*U, where L is lower triangular.
     
      While the Cholesky decomposition is particularly useful to solve selfadjoint problems like  D^*D x = b,
      Nevertheless, this standard Cholesky decomposition remains useful in many other
      situations like generalised eigen problems with hermitian matrices.
     
      Remember that Cholesky decompositions are not rank-revealing. This LLT decomposition is only stable on positive definite matrices,
      Also, do not use a Cholesky decomposition to determine whether a system of equations has a solution.
     
      \return Lower triangular matrix
    */
    MatrixEigenDense<K> cholesky() const
    {
      if (this->numOfRows() != this->numOfCols()) { this->nonSquare("Computing cholesky decomposition", this->numOfRows(), this->numOfCols()); }
      MatrixEigenDense<K> chol(numOfRows(), numOfCols(), NumTraits<K>::zero());
      number_t n = numOfRows();
      
      for (number_t i = 0; i < n; i++)
        for (number_t j = 0; j < (i + 1); j++)
        {
          K s = NumTraits<K>::zero();
          for (number_t k = 0; k < j; k++)
          { s += chol[i * n + k] * conj(chol[j * n + k]); }
          chol[i * n + j] = (i == j) ? std::sqrt(NumTraits<K>::real((*this)[i * n + i] - s)) :  (1.0 / (chol[j * n + j]) * ((*this)[i * n + j] - s));
        }
        
      return chol;
    }
    
    /*!
      \brief Linear solver with Cholesky decomposed matrix
       Solve linear equation: Lx = b with L: lower triangular matrix
       !!!This function should only be called after the function cholesky()
     
       \param[in] mat right-hand matrix (b)
       \param[out] res result matrix (x)
    */
    template<typename T>
    void solveCholeskyInplaceLower(const MatrixEigenDense<T>& mat, MatrixEigenDense<K>& res)
    {
      if (numOfRows() != res.numOfRows()) { this->mismatchDims("Row mismatch", numOfRows(), res.numOfRows()); }
      if (mat.numOfCols() != res.numOfCols()) { this->mismatchDims("Column mismatch", mat.numOfCols(), res.numOfCols()); }
      
      int_t nbRows(res.numOfRows()), nbCols(res.numOfCols());
      int_t i, j, k;
      
      for (i = 0; i < nbRows; ++i)
      {
        for (k = 0; k < nbCols; ++k)
        {
          res[nbCols * i + k] = mat[nbCols * i + k];
        }
        for (j = 0; j < i; ++j)
        {
          for (k = 0; k < nbCols; ++k)
          {
            res[nbCols * i + k] = res[nbCols * i + k] - (*this)[nbCols * i + j] * res[nbCols * j + k];
          }
        }
        for (k = 0; k < nbCols; ++k)
        {
          res[nbCols * i + k] /= (*this)[i * nbCols + i];
        }
      }
    }
    
    /*!
      \brief Linear solver with Cholesky decomposed matrix
      Solve linear equation: XU = b with U: upper triangular matrix
      !!!This function should only be called after the function cholesky()
    
      \param[in] mat right-hand matrix (b)
      \param[out] res result matrix (x)
    */
    template<typename T>
    void solveCholeskyInplaceUpper(const MatrixEigenDense<T>& mat, MatrixEigenDense<K>& res)
    {
      if (numOfRows() != res.numOfRows()) { this->mismatchDims("Row mismatch", numOfRows(), res.numOfRows()); }
      if (mat.numOfCols() != res.numOfCols()) { this->mismatchDims("Column mismatch", mat.numOfCols(), res.numOfCols()); }
      
      int_t nbRows(res.numOfRows()), nbCols(res.numOfCols());
      int_t i, j, k;
      
      for (i = nbRows; i > 0; --i)
      {
        for (k = 0; k < nbCols; ++k)
        {
          res[nbCols * (i - 1) + k] = mat[nbCols * (i - 1) + k];
        }
        for (j = i; j < nbCols; ++j)
        {
          for (k = 0; k < nbCols; ++k)
          {
            res[nbCols * (i - 1) + k] = res[nbCols * (i - 1) + k] - conj((*this)[nbCols * j + i - 1]) * res[nbCols * j + k];
          }
        }
        for (k = 0; k < nbCols; ++k)
        {
          res[nbCols * (i - 1) + k] /= conj((*this)[(i - 1) * nbCols + i - 1]);
        }
      }
    }
    
    void reshape(const dimen_t row, const dimen_t col)
    {
      this->resize(row*col);
      this->rows_ = row;
      col_ = col;
    }

    MatrixEigenDense<K>& normalizeEigenVectors();

    void putScalar(const K scalar = NumTraits<K>::zero())
    {
      for (it_vk it = this->begin(); it != this->end(); ++it) { *it = scalar;}
    }

    void random()
    {
      for (it_vk it = this->begin(); it != this->end(); ++it) { *it = NumTraits<K>::random();}
    }

    /*!
      Return coefficient of a matrix
    
      \param rowIdx row index of coefficient
      \param colIdx column index of coefficient
     
      \return coefficient
    */
    K coeff(const number_t rowIdx, const number_t colIdx) const
    {
      if (rowIdx >= numOfRows()) { indexOutOfRange("row of matrix", rowIdx, numOfRows()); }
      if (colIdx >= numOfCols()) { indexOutOfRange("column of matrix", colIdx, numOfCols()); }
      number_t c = numOfCols();
      return (*this)[c * rowIdx + colIdx];
    }
    
    /*!
      Return reference of a coefficient of matrix
    
      \param rowIdx row index of coefficient
      \param colIdx column index of coefficient
    
      \return reference of coefficient
    */
    K& coeffRef(const number_t rowIdx, const number_t colIdx)
    {
      if (rowIdx >= numOfRows()) { indexOutOfRange("row of matrix", rowIdx, numOfRows()); }
      if (colIdx >= numOfCols()) { indexOutOfRange("column of matrix", colIdx, numOfCols()); }
      number_t c = numOfCols();
      it_vk it = this->begin() + c * rowIdx + colIdx;
      return (*it);
    }
    
    /*!
      Scale a matrix
      \param s scaling factor
      \return scaled matrix
      TODO need to do with round-off error. Limit using this function!!!
    */
    template<typename T>
    MatrixEigenDense<K>& scale(const T s) const
    {
      if (NumTraits<K>::isRealType() && NumTraits<T>::isComplex()) { error("c_to_r"); }
      else
      {
        MatrixEigenDense<K> m(numOfRows(), numOfCols());
        it_vk itm = m.begin();
        for (cit_vk it = this->begin(); it != this->end(); it++, itm++) { *itm = *it * s;}
        
        return m;
      }
    }
    
    void sizeMisMatch(const string_t& s, const size_t r, const size_t c) const //! error: in matrix operation s dimensions disagree dim(op1)= ... dim(op2)=...
    {
      where(s);
      error("bad_dim", r, c);
    }
    
    void indexOutOfRange(const string_t& s, const size_t index, const size_t range) const
    {
      where(s);
      error("index_out_of_range", index, range);
    }
    
    /*!
      Applies the rotation in the plane \a j to the rows \a p and \a q of \c *this, i.e., it computes B = J * B,
      * with \f$ B = \left ( \begin{array}{cc} \mbox{*this.row(p)} \\ \mbox{*this.row(q)} \end{array} \right ) \f$.
      *
      */
    template<typename OtherScalar>
    inline void applyOnTheLeft(Index p, Index q, const JacobiRotation<OtherScalar>& j)
    {
      VectorEigenDense<K> x = rowVector(p);
      VectorEigenDense<K> y = rowVector(q);
      applyRotationInThePlane(x, y, j);
      rowVector(p, x); rowVector(q, y);
    }
    
    /*!
      Applies the rotation in the plane \a j to the columns \a p and \a q of \c *this, i.e., it computes B = B * J
      with \f$ B = \left ( \begin{array}{cc} \mbox{*this.col}(p) & \mbox{*this.col}(q) \end{array} \right ) \f$.
    */
    template<typename OtherScalar>
    inline void applyOnTheRight(Index p, Index q, const JacobiRotation<OtherScalar>& j)
    {
      VectorEigenDense<K> x = columnVector(p);
      VectorEigenDense<K> y = columnVector(q);
      applyRotationInThePlane(x, y, j.transpose());
      columnVector(p, x); columnVector(q, y);
    }
    
    //! Return number of rows or columns of matrix
    dimen_t numOfRows() const { return this->numberOfRows(); }
    dimen_t numOfCols() const { return col_;}
  protected:
    template<typename VectorX, typename VectorY, typename OtherScalar>
    void applyRotationInThePlane(VectorX& x, VectorY& y, const JacobiRotation<OtherScalar>& j)
    {
      typedef number_t Index;
      typedef typename VectorX::type_t Scalar;
      Index size = x.size();
      Index incrx = 1; //_x.innerStride();
      Index incry = 1; //_y.innerStride();
      
      it_vk itx = x.begin();
      it_vk ity = y.begin();
      for(Index i = 0; i < size; ++i)
      {
        Scalar xi = *itx;
        Scalar yi = *ity;
        *itx =  j.c() * xi + conj(j.s()) * yi;
        *ity = -j.s() * xi + conj(j.c()) * yi;
        itx += incrx;
        ity += incry;
      }
    }
    
  private:
    /*!
      Check the validity of info of sub-matrix
      \param[in] idx index specify block of sub-matrix inside the main matrix.
                 idx[0]: row position of top-left point
                 idx[1]: column position of top-left point
                 idx[2]: number of row of sub-matrix
                 idx[3]: number of column of sub-matrix
    */
    void verifySize(const Indexing& idx)
    {
      dimen_t r = numOfRows();
      dimen_t c = numOfCols();
      if ((idx[2] > r) || (idx[3] > c)) { this->mismatchDims("Index out of range", idx[2], idx[3]); }
      if (((idx[2] + idx[0]) > r) || ((idx[3] + idx[1]) > c)) { this->mismatchDims("Sub-matrix over size", idx[2] + idx[0], idx[3] + idx[1]); }
    }
  private:
    dimen_t col_;
  
  // specialization for real and complex matrices
  friend std::ostream& operator<<(std::ostream& os, const MatrixEigenDense<real_t>& m);
  friend std::ostream& operator<<(std::ostream& os, const MatrixEigenDense<complex_t>& m);
};

std::ostream& operator<<(std::ostream& os, const MatrixEigenDense<real_t>& m);    //!< flux insertion (write)
std::ostream& operator<<(std::ostream& os, const MatrixEigenDense<complex_t>& m); //!< flux insertion (write)

template<typename K1, typename K2>
void multMatMat(const MatrixEigenDense<K1>& mat1, const MatrixEigenDense<K2>& mat2, MatrixEigenDense<typename Conditional<NumTraits<K1>::IsComplex, K1, K2>::type>& res)
{
  dimen_t nbk = mat1.numOfCols(), nbr = mat1.numOfRows(), nbc = mat2.numOfCols();
  if (mat1.numOfCols() != mat2.numOfRows()) { mat1.mismatchDims("Matrix multiplication wrong dim (col -row)", mat1.numOfCols(), mat2.numOfRows()); }
  if (mat1.numOfRows() != res.numOfRows()) { mat1.mismatchDims("Matrix multiplication wrong dim (col -row)", mat1.numOfRows(), res.numOfRows()); }
  if (mat2.numOfCols() != res.numOfCols()) { mat2.mismatchDims("Matrix multiplication wrong dim", mat2.numOfCols(), res.numOfCols()); }
  
  res.putScalar(NumTraits<typename Conditional<NumTraits<K1>::IsComplex, K1, K2>::type>::zero());
  for (number_t i = 0; i < nbr; ++i) {
    for (number_t j = 0; j < nbc; ++j) {
      for (number_t k = 0; k < nbk; ++k) {
        res.coeffRef(i,j) += mat1.coeff(i,k)*mat2.coeff(k,j);
      }
    }
  }
}

template<typename K>
MatrixEigenDense<K> transpose(const MatrixEigenDense<K>& mat)
{
  MatrixEigenDense<K> v(mat.numOfCols(), mat.numOfRows());
  for (number_t i = 0; i < mat.numOfCols(); ++i) {
    for (number_t j = 0; j < mat.numOfRows(); ++j) {
      v.coeffRef(i,j) = mat.coeff(j,i);
    }
  }
  
  return v;
}

template<typename K>
MatrixEigenDense<K> adjoint(const MatrixEigenDense<K>& mat)
{
  //if (mat.numOfRows() != mat.numOfCols()) { mat.nonSquare("Adjoint matrix", mat.numOfRows(), mat.numOfCols()); }
  MatrixEigenDense<K> v(mat.numOfCols(), mat.numOfRows());
  typename MatrixEigenDense<K>::it_vk itV = v.begin();
  typename MatrixEigenDense<K>::cit_vk itM = mat.begin();
  adjoint(v.numOfRows(), v.numOfCols(), itM, itV);
  //adjoint(v.numOfCols(), v.numOfRows(), itM, itV);
  
  return v;
}

template<typename K>
MatrixEigenDense<complex_t> cmplx(const MatrixEigenDense<K>& mat)
{
  //if (mat.numOfRows() != mat.numOfCols()) { mat.nonSquare("Adjoint matrix", mat.numOfRows(), mat.numOfCols()); }
  MatrixEigenDense<complex_t> v(mat.numOfRows(), mat.numOfCols());
  typename MatrixEigenDense<complex_t>::it_vk itV = v.begin();
  typename MatrixEigenDense<K>::cit_vk itMb = mat.begin(), itMe = mat.end(), itM;
  for (itM = itMb; itM != itMe; itM++, itV++) *itV = cmplx(*itM);

  return v;
}

template<typename K>
MatrixEigenDense<K> conj(const MatrixEigenDense<K>& mat)
{
  MatrixEigenDense<K> v(mat.numOfRows(), mat.numOfCols());
  typename MatrixEigenDense<K>::it_vk itV = v.begin();
  typename MatrixEigenDense<K>::cit_vk itM = mat.begin();
  for (; itV != v.end(); ++itV, ++itM) {
    *itV = conj(*itM);
  }

  return v;
}

template<typename K>
void removeSmallValue(MatrixEigenDense<K>& mat)
{
  typename MatrixEigenDense<K>::it_vk itM = mat.begin();
  for (; itM != mat.end(); ++itM) {
    *itM = NumTraits<K>::compare2epsilon(*itM);
  }
}

//! Reverse all elements of a matrix
template<typename K>
void eyeMatrix(MatrixEigenDense<K>& mat)
{
  mat.diagonal(NumTraits<K>::one());
}

//! Reverse all elements of a matrix
template<typename K>
void transformMatrix(MatrixEigenDense<K>& mat)
{
    std::reverse(mat.begin(),mat.end());
}

template<typename K>
MatrixEigenDense<K> kroneckerProduct(MatrixEigenDense<K>& mat1, MatrixEigenDense<K>& mat2)
{
  dimen_t r1 = mat1.numOfRows();
  dimen_t c1 = mat1.numOfCols();
  dimen_t r2 = mat2.numOfRows();
  dimen_t c2 = mat2.numOfCols();

  MatrixEigenDense<K> tmp(r1*r2, c1*c2);
  for (int i = 0; i < r1; ++i)
    for (int j = 0; j < c1; ++j)
      for (int m = 0; m < r2; ++m)
        for (int k = 0; k < c2; ++k)
          tmp.coeffRef(i*r2+m,j*c2+k) = mat1.coeff(i,j)*mat2.coeff(m,k);

  return tmp;
}

template <typename T>
MatrixEigenDense<T> normalizeEigenVectors(const MatrixEigenDense<T>& eigVecs)
{
  MatrixEigenDense<T> res=eigVecs;
  res.normalizeEigenVectors();
  return res;
}

} // end of namespace xlifepp

#endif /* MATRIX_EIGEN_DENSE_HPP */

