/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file  MatrixEigenDense.cpp
  \author N. Kielbasiewicz
  \since 10 dec 2020
  \date 10 dec 2020
  
  \brief Implementation of the xlifepp::MatrixEigenDense object members
 */

#include "MatrixEigenDense.hpp"

namespace xlifepp
{
  
std::ostream& operator<<(std::ostream& os, const MatrixEigenDense<real_t>& m) //! real matrix flux insertion (write)
{
  dimen_t rows=m.numberOfRows(), cols=m.numberOfColumns();
  if (rows == 0) // empty matrix
  {
    os << "[ ]";
    return os;
  }
  // printRowWise(os, "Allo", rows, testPrec, m.begin(), m.end());
  std::vector<real_t>::const_iterator it_m = m.begin();
  os << "[";
  for ( dimen_t r = 0; r < rows; r++ )
    {
      for ( dimen_t c = 0; c < cols; c++, it_m++ )
          os << " " <<  std::setprecision(testPrec) << roundToZero(*it_m);
      if ( rows - r > 1 ) os << eol;
    }
  os << " ]";
  return os;
}

std::ostream& operator<<(std::ostream& os, const MatrixEigenDense<complex_t>& m) //! complex matrix flux insertion (write)
{
  dimen_t rows=m.numberOfRows(), cols=m.numberOfColumns();
  if (rows == 0) // empty matrix
  {
    os << "[ ]";
    return os;
  }
  // printRowWise(os, "A l'huile", rows, testPrec, m.begin(), m.end());
  std::vector<complex_t>::const_iterator it_m = m.begin();
  os << "[";
  for (dimen_t r = 0; r < rows; r++ )
    {
      for ( dimen_t c = 0; c < cols; c++, it_m++)
          os << " " << std::setprecision(testPrec) << roundToZero(*it_m);
      if ( rows - r > 1 )  os << eol;
    }
  os << " ]";
  return os;
}

template <>
MatrixEigenDense<real_t>& MatrixEigenDense<real_t>::normalizeEigenVectors()
{
  it_vk it = begin(), c1;
  number_t cols=size()/rows_;
  for (dimen_t c = 0; c < cols; ++c, ++it)
  {
    bool toNormalize = false;
    c1 = it;
    for (dimen_t r = 0; r < rows_; ++r, c1+=cols)
    {
      if (*c1 > theEpsilon) { break; }
      if (*c1 < -theEpsilon) { toNormalize=true; break; }
    }
    if (toNormalize)
    {
      c1 = it;
      for (dimen_t r = 0; r < rows_; ++r, c1+=cols)
      {
        (*c1)*=-1;
      }
    }
  }
  return *this;
}

template <>
MatrixEigenDense<complex_t>& MatrixEigenDense<complex_t>::normalizeEigenVectors()
{
  it_vk it = begin(), c1;
  number_t cols=size()/rows_;
  for (dimen_t c = 0; c < cols; ++c, ++it)
  {
    bool toNormalize = false;
    c1 = it;
    for (dimen_t r = 0; r < rows_; ++r, c1+=cols)
    {
      if (std::abs(*c1) > theEpsilon)
      {
        if (c1->real() > theEpsilon) { break; }
        if (c1->real() < -theEpsilon) { toNormalize=true; break; }
        if (c1->imag() > theEpsilon) { break; }
        if (c1->imag() < -theEpsilon) { toNormalize=true; break; }
      }
    }
    if (toNormalize)
    {
      c1 = it;
      for (dimen_t r = 0; r < rows_; ++r, c1+=cols)
      {
        (*c1)*=-1;
      }
    }
  }   
  return *this;
}

} // end of namespace xlifepp
