/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file ComplexSchur.hpp
  \author Manh Ha NGUYEN
  \since 19 June 2013
  \date  19 June 2013

  \brief Definition of the xlifepp::ComplexSchur class

  Class deals with complex Schur decomposition.
*/

// This file is adapted from Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2009 Claire Maurice
// Copyright (C) 2009 Gael Guennebaud <gael.guennebaud@inria.fr>
// Copyright (C) 2010 Jitse Niesen <jitse@maths.leeds.ac.uk>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_COMPLEX_SCHUR_HPP
#define EIGEN_COMPLEX_SCHUR_HPP

#include "../utils/Jacobi.hpp"
#include "../utils/VectorEigenDense.hpp"
#include "../utils/MatrixEigenDense.hpp"
#include "./HessenbergDecomposition.hpp"

namespace xlifepp {

namespace internalEigenSolver {
template<typename MatrixType, bool IsComplex> struct ComplexSchurReduceToHessenberg;
template<typename MatrixType> void swapComplexSchurInPlace(MatrixType& matrixT, MatrixType& matrixQ, const std::vector<int>& order);
template<typename MatrixType> void swapComplexSchurInPlace(MatrixType& matrixT, const std::vector<int>& order);
}

/*!
  \class ComplexSchur
  
  \brief Performs a complex Schur decomposition of a real or complex square matrix
  
  \tparam _MatrixType the type of the matrix of which we are
  computing the Schur decomposition; this is expected to be an
  instantiation of the Matrix class template.
  
  Given a real or complex square matrix A, this class computes the
  Schur decomposition: \f$ A = U T U^*\f$ where U is a unitary
  complex matrix, and T is a complex upper triangular matrix.  The
  diagonal of the matrix T corresponds to the eigenvalues of the
  matrix A.
  
  Call the function compute() to compute the Schur decomposition of
  a given matrix. Alternatively, you can use the 
  ComplexSchur(const MatrixType&, bool) constructor which computes
  the Schur decomposition at construction time. Once the
  decomposition is computed, you can use the matrixU() and matrixT()
  functions to retrieve the matrices U and V in the decomposition.
  
  \note This code is inspired from Jampack
  
  \sa class RealSchur, class EigenSolver, class ComplexEigenSolver
*/
template<typename _MatrixType> class ComplexSchur
{
  public:
    typedef _MatrixType MatrixType;

    //! Scalar type for matrices of type \p _MatrixType.
    typedef typename MatrixType::type_t Scalar;
    typedef typename NumTraits<Scalar>::RealScalar RealScalar;
    typedef typename NumTraits<Scalar>::ComplexScalar ComplexScalar;

    /*!
      \brief Type for the matrices in the Schur decomposition.
      
      This is a square matrix with entries of type ComplexScalar. 
      The size is the same as the size of \p _MatrixType.
    */
    typedef MatrixEigenDense<ComplexScalar> ComplexMatrixType;

    /*!
      \brief Default constructor.
      
      \param [in] size  Positive integer, size of the matrix whose Schur decomposition will be computed.
      
      The default constructor is useful in cases in which the user
      intends to perform decompositions via compute().  The \p size
      parameter is only used as a hint. It is not an error to give a
      wrong \p size, but it may impair performance.
      
      \sa compute() for an example.
    */
    ComplexSchur(Index size)
    : matT_(size,size), matU_(size,size), hess_(size), isInitialized_(false), matUisUptodate_(false)
    {}

    /*!
      \brief Constructor; computes Schur decomposition of given matrix. 
       
      \param[in]  matrix    Square matrix whose Schur decomposition is to be computed.
      \param[in]  computeU  If true, both T and U are computed; if false, only T is computed.
      
      This constructor calls compute() to compute the Schur decomposition.
      
      \sa matrixT() and matrixU() for examples.
    */
    ComplexSchur(const MatrixType& matrix, bool computeU = true)
    : matT_(matrix.numOfRows(),matrix.numOfCols()), matU_(matrix.numOfRows(),matrix.numOfCols()),
      hess_(matrix.numOfRows()), isInitialized_(false), matUisUptodate_(false)
    { compute(matrix, computeU); }

    /*!
      \brief Returns the unitary matrix in the Schur decomposition. 
      
      \returns A const reference to the matrix U.
      
      It is assumed that either the constructor
      ComplexSchur(const MatrixType& matrix, bool computeU) or the
      member function compute(const MatrixType& matrix, bool computeU)
      has been called before to compute the Schur decomposition of a
      matrix, and that \p computeU was set to true (the default
      value).
      
      Example: \\include test_EigenSolverDecomposition.cpp
    */
    const ComplexMatrixType& matrixU() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "ComplexSchur"); }
      if (!matUisUptodate_) { error("eigensolver_matrix_not_updated", "U",  "Complex"); }
      return matU_;
    }

    /*!
      \brief Returns the triangular matrix in the Schur decomposition. 
      
      \returns A const reference to the matrix T.
      
      It is assumed that either the constructor
      ComplexSchur(const MatrixType& matrix, bool computeU) or the
      member function compute(const MatrixType& matrix, bool computeU)
      has been called before to compute the Schur decomposition of a
      matrix.
      
      Note that this function returns a plain square matrix. If you want to reference
      only the upper triangular part, use:
      \code schur.matrixT().triangularView<Upper>() \endcode 
      
      Example: \\include test_EigenSolverDecomposition.cpp
    */
    const ComplexMatrixType& matrixT() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "ComplexSchur"); }
      return matT_;
    }

    /*!
      \brief Reorder the Schur decomposition according to permutation vector
      
      It is assumed that either the constructor
      ComplexSchur(const MatrixType& matrix, bool computeU) or the
      member function compute(const MatrixType& matrix, bool computeU)
      has been called before to compute the Schur decomposition of a
      matrix.
      
      Note that this function change upper triangular matrix T and unitary matrix U
      
      Example: \\include test_EigenSolverDecomposition.cpp
    */
    void swapSchur(const std::vector<int>& order, bool computeU = true);

    /*!
      \brief Computes Schur decomposition of given matrix. 
       
      \param[in]  matrix  Square matrix whose Schur decomposition is to be computed.
      \param[in]  computeU  If true, both T and U are computed; if false, only T is computed.
      \returns    Reference to \c *this
      
      The Schur decomposition is computed by first reducing the
      matrix to Hessenberg form using the class
      HessenbergDecomposition. The Hessenberg matrix is then reduced
      to triangular form by performing QR iterations with a single
      shift. The cost of computing the Schur decomposition depends
      on the number of iterations; as a rough guide, it may be taken
      on the number of iterations; as a rough guide, it may be taken
      to be \f$25n^3\f$ complex flops, or \f$10n^3\f$ complex flops
      if \a computeU is false.
      
      Example: \\include test_EigenSolverDecomposition.cpp
    */
    ComplexSchur& compute(const MatrixType& matrix, bool computeU = true);

    /*!
      \brief Reports whether previous computation was successful.
      
      \returns \c _success if computation was succesful, \c _noConvergence otherwise.
    */
    ComputationInfo info() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "ComplexSchur"); }
      return info_;
    }

    /*!
      \brief Maximum number of iterations.
      
      Maximum number of iterations allowed for an eigenvalue to converge. 
    */
    static const int maxIterations_ = 30;

  protected:
    ComplexMatrixType matT_, matU_;
    HessenbergDecomposition<MatrixType> hess_; //!< hessenberg decomposition
    ComputationInfo info_;
    bool isInitialized_; //!< true if initialized
    bool matUisUptodate_;

  private:  
    bool subdiagonalEntryIsNeglegible(Index i);
    ComplexScalar computeShift(Index iu, Index iter);
    void reduceToTriangularForm(bool computeU);
    friend struct internalEigenSolver::ComplexSchurReduceToHessenberg<MatrixType, NumTraits<Scalar>::IsComplex>;
};

/*! If matT_(i+1,i) is neglegible in floating point arithmetic
  * compared to matT_(i,i) and matT_(j,j), then set it to zero and
  * return true, else return false. */
template<typename MatrixType>
inline bool ComplexSchur<MatrixType>::subdiagonalEntryIsNeglegible(Index i)
{
  RealScalar d = NumTraits<ComplexScalar>::norm1(matT_.coeff(i,i)) + NumTraits<ComplexScalar>::norm1(matT_.coeff(i+1,i+1));
  RealScalar sd = NumTraits<ComplexScalar>::norm1(matT_.coeff(i+1,i));
  if ( sd < d*NumTraits<RealScalar>::epsilon()*NumTraits<RealScalar>::epsilon())
  //if (internalEigenSolver::isMuchSmallerThan(sd, d, NumTraits<RealScalar>::epsilon()))
  {
    matT_.coeffRef(i+1,i) = ComplexScalar(0);
    return true;
  }
  return false;
}


/*! Compute the shift in the current QR iteration. */
template<typename MatrixType>
typename ComplexSchur<MatrixType>::ComplexScalar ComplexSchur<MatrixType>::computeShift(Index iu, Index iter)
{
  if (iter == 10 || iter == 20) 
  {
    // exceptional shift, taken from http://www.netlib.org/eispack/comqr.f
    return std::fabs(real(matT_.coeff(iu,iu-1))) + std::fabs(real(matT_.coeff(iu-1,iu-2)));
  }

  // compute the shift as one of the eigenvalues of t, the 2x2
  // diagonal block on the bottom of the active submatrix
  //Matrix<ComplexScalar,2,2> t = matT_.template block<2,2>(iu-1,iu-1);
  ComplexMatrixType t(matT_,iu-1,iu-1,2,2);
  //RealScalar normt = t.cwiseAbs().sum();
  RealScalar normt = t.normOne();
  t /= normt;     // the normalization by sf is to avoid under/overflow

  ComplexScalar b = t.coeff(0,1) * t.coeff(1,0);
  ComplexScalar c = t.coeff(0,0) - t.coeff(1,1);
  ComplexScalar disc = sqrt(c*c + RealScalar(4)*b);
  ComplexScalar det = t.coeff(0,0) * t.coeff(1,1) - b;
  ComplexScalar trace = t.coeff(0,0) + t.coeff(1,1);
  ComplexScalar eival1 = (trace + disc) / RealScalar(2);
  ComplexScalar eival2 = (trace - disc) / RealScalar(2);

  if(NumTraits<ComplexScalar>::norm1(eival1) > NumTraits<ComplexScalar>::norm1(eival2))
    eival2 = det / eival1;
  else
    eival1 = det / eival2;

  // choose the eigenvalue closest to the bottom entry of the diagonal
  if(NumTraits<ComplexScalar>::norm1(eival1-t.coeff(1,1)) < NumTraits<ComplexScalar>::norm1(eival2-t.coeff(1,1)))
    return normt * eival1;
  else
    return normt * eival2;
}


template<typename MatrixType>
ComplexSchur<MatrixType>& ComplexSchur<MatrixType>::compute(const MatrixType& matrix, bool computeU)
{
  matUisUptodate_ = false;
  if (matrix.numOfCols() != matrix.numOfRows())
  { error("mat_nonsquare", "ComplexSchur::compute", matrix.numOfRows(), matrix.numOfCols()); }

  if(matrix.numOfCols() == 1)
  {
    matT_ = cmplx(matrix) ; //matrix.template cast<ComplexScalar>();
    if(computeU)  matU_ = ComplexMatrixType(1,1,NumTraits<ComplexScalar>::one());//::Identity(1,1);
    info_ = _success;
    isInitialized_ = true;
    matUisUptodate_ = computeU;
    return *this;
  }

  internalEigenSolver::ComplexSchurReduceToHessenberg<MatrixType, NumTraits<Scalar>::IsComplex>::run(*this, matrix, computeU);
  reduceToTriangularForm(computeU);
  return *this;
}

// Reduce the Hessenberg matrix matT_ to triangular form by QR iteration.
template<typename MatrixType>
void ComplexSchur<MatrixType>::reduceToTriangularForm(bool computeU)
{  
    const Index size = matT_.numOfCols();
    const Index row = matT_.numOfRows();
  // The matrix matT_ is divided in three parts.
  // Rows 0,...,il-1 are decoupled from the rest because matT_(il,il-1) is zero.
  // Rows il,...,iu is the part we are working on (the active submatrix).
  // Rows iu+1,...,end are already brought in triangular form.
  Index iu = matT_.numOfCols() - 1;
  Index il;
  Index iter = 0; // number of iterations we are working on the (iu,iu) element
  Index totalIter = 0; // number of iterations for whole matrix

  while(true)
  {
    // find iu, the bottom row of the active submatrix
    while(iu > 0)
    {
      if(!subdiagonalEntryIsNeglegible(iu-1)) break;
      iter = 0;
      --iu;
    }

    // if iu is zero then we are done; the whole matrix is triangularized
    if(iu==0) break;

    // if we spent too many iterations, we give up
    iter++;
    totalIter++;
    if(totalIter > maxIterations_ * matT_.numOfCols()) break;

    // find il, the top row of the active submatrix
    il = iu-1;
    while(il > 0 && !subdiagonalEntryIsNeglegible(il-1))
    {
      --il;
    }

    /* perform the QR step using Givens rotations. The first rotation
       creates a bulge; the (il+2,il) element becomes nonzero. This
       bulge is chased down to the bottom of the active submatrix. */

    ComplexScalar shift = computeShift(iu, iter);
    JacobiRotation<ComplexScalar> rot;
    rot.makeGivens(matT_.coeff(il,il) - shift, matT_.coeff(il+1,il));
    //matT_.rightCols(matT_.numOfCols()-il).applyOnTheLeft(il, il+1, rot.adjoint());
    ComplexMatrixType rightCols(matT_,0,il,matT_.numOfRows(),matT_.numOfCols()-il);
    rightCols.applyOnTheLeft(il, il+1, rot.adjoint());
    matT_.replace(rightCols,0,il,row,matT_.numOfCols()-il);
    //matT_.topRows((std::min)(il+2,iu)+1).applyOnTheRight(il, il+1, rot);
    ComplexMatrixType topRows(matT_,0,0,(std::min)(il+2,iu)+1,size);
    topRows.applyOnTheRight(il, il+1, rot);
    matT_.replace(topRows,0,0,(std::min)(il+2,iu)+1,size);
    if(computeU) matU_.applyOnTheRight(il, il+1, rot);

    for(Index i=il+1 ; i<iu ; i++)
    {
      rot.makeGivens(matT_.coeffRef(i,i-1), matT_.coeffRef(i+1,i-1), &matT_.coeffRef(i,i-1));
      matT_.coeffRef(i+1,i-1) = ComplexScalar(0);
      //matT_.rightCols(matT_.numOfCols()-i).applyOnTheLeft(i, i+1, rot.adjoint());
      ComplexMatrixType rightColsTmp(matT_,0,i,row,matT_.numOfCols()-i);
      rightColsTmp.applyOnTheLeft(i, i+1, rot.adjoint());
      matT_.replace(rightColsTmp,0,i,row,matT_.numOfCols()-i);

      //matT_.topRows((std::min)(i+2,iu)+1).applyOnTheRight(i, i+1, rot);
      ComplexMatrixType topRowsTmp(matT_,0,0,(std::min)(i+2,iu)+1,size);
      topRowsTmp.applyOnTheRight(i, i+1, rot);
      matT_.replace(topRowsTmp,0,0,(std::min)(i+2,iu)+1,size);
      if(computeU) matU_.applyOnTheRight(i, i+1, rot);
    }
  }

  if(totalIter <= maxIterations_ * matT_.numOfCols())
    info_ = _success;
  else
    info_ = _noConvergence;

  isInitialized_ = true;
  matUisUptodate_ = computeU;
}

template<typename MatrixType>
void ComplexSchur<MatrixType>::swapSchur(const std::vector<int>& order, bool computeU)
{
  if (computeU) internalEigenSolver::swapComplexSchurInPlace(matT_,matU_,order);
  else internalEigenSolver::swapComplexSchurInPlace(matT_,order);
}

namespace internalEigenSolver {

/* Reduce given matrix to Hessenberg form */
template<typename MatrixType, bool IsComplex>
struct ComplexSchurReduceToHessenberg
{
  // this is the implementation for the case IsComplex = true
  static void run(ComplexSchur<MatrixType>& thisMatrix, const MatrixType& matrix, bool computeU)
  {
    thisMatrix.hess_.compute(matrix);
    thisMatrix.matT_ = thisMatrix.hess_.matrixH();
    if(computeU)  thisMatrix.matU_ = thisMatrix.hess_.matrixQ();
  }
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<typename MatrixType>
struct ComplexSchurReduceToHessenberg<MatrixType, false>
{
  static void run(ComplexSchur<MatrixType>& thisMatrix, const MatrixType& matrix, bool computeU)
  {
    // Note: hess_ is over RealScalar; matT_ and matU_ is over ComplexScalar
    thisMatrix.hess_.compute(matrix);
    thisMatrix.matT_ = cmplx(thisMatrix.hess_.matrixH()); //.template cast<ComplexScalar>();
    if(computeU)
    {
      // This may cause an allocation which seems to be avoidable
      MatrixType Q = thisMatrix.hess_.matrixQ();
      thisMatrix.matU_ = cmplx(Q); //.template cast<ComplexScalar>();
    }
  }
};

#endif

// This function sorts eigenvalues on diagonal of Schur triangular matrix T with a specific order
// \param matrixT[in/out] triangular Schur matrix T
// \param matrixQ[in/out] orthogonal matrix Q
// \param order[in] sorting order
template<typename MatrixType>
void swapComplexSchurInPlace(MatrixType& matrixT, MatrixType& matrixQ, const std::vector<int>& order)
{
    typedef typename MatrixType::type_t Scalar;
    typedef typename NumTraits<Scalar>::RealScalar RealScalar;
    typedef typename NumTraits<Scalar>::ComplexScalar ComplexScalar;

    std::vector<int> sortOrder(order);
    const Index size = matrixT.numOfCols();
    const Index length = sortOrder.size();
    const Index kk = (std::min)(length,size-1);
    Index j = 0; while ((j <= kk) && (sortOrder[j] == j)) j++;

    while (j <= kk) {
      JacobiRotation<ComplexScalar> rot;
      Index i = sortOrder[j];
      for (Index k = i-1; k >= j; k--) {
        if (NumTraits<RealScalar>::epsilon() < (std::abs(matrixT.coeff(k+1,k+1)- matrixT.coeff(k,k)))) {
          rot.makeGivens(matrixT.coeff(k,k+1), (matrixT.coeff(k+1,k+1) - matrixT.coeff(k,k)));
          matrixT.applyOnTheRight(k,k+1,rot);
          matrixT.applyOnTheLeft(k,k+1,rot.adjoint());
          matrixQ.applyOnTheRight(k,k+1,rot);
        }
        matrixT.coeffRef(k+1,k) = ComplexScalar(0);
      }
      for (std::vector<int>::iterator it = sortOrder.begin(); it != sortOrder.end(); ++it) {
        if (*it < i) (*it)++;
      }
      ++j; while ((j <= kk) && (sortOrder[j] == j)) j++;
    }
}

template<typename MatrixType>
void swapComplexSchurInPlace(MatrixType& matrixT, const std::vector<int>& order)
{
    typedef typename MatrixType::type_t Scalar;
    typedef typename NumTraits<Scalar>::RealScalar RealScalar;
    typedef typename NumTraits<Scalar>::ComplexScalar ComplexScalar;

    std::vector<int> sortOrder(order);
    const Index size = matrixT.numOfCols();
    const Index length = sortOrder.size();
    const Index kk = (std::min)(length,size-1);
    Index j = 0; while ((j <= kk) && (sortOrder[j] == j)) j++;

    while (j <= kk) {
      JacobiRotation<ComplexScalar> rot;
      Index i = sortOrder[j];
      for (Index k = i-1; k >= j; k--) {
        if (NumTraits<RealScalar>::epsilon() < (std::abs(matrixT.coeff(k+1,k+1)- matrixT.coeff(k,k)))) {
          rot.makeGivens(matrixT.coeff(k,k+1), (matrixT.coeff(k+1,k+1) - matrixT.coeff(k,k)));
          matrixT.applyOnTheRight(k,k+1,rot);
          matrixT.applyOnTheLeft(k,k+1,rot.adjoint());
        }
        matrixT.coeffRef(k+1,k) = ComplexScalar(0);
      }
      for (std::vector<int>::iterator it = sortOrder.begin(); it != sortOrder.end(); ++it) {
        if (*it < i) (*it)++;
      }
      ++j; while ((j <= kk) && (sortOrder[j] == j)) j++;
    }
}

} // end namespace internalEigenSolver

} // end namespace xlifepp

#endif // EIGEN_COMPLEX_SCHUR_HPP

