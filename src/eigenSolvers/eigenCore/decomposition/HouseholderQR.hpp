/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file HouseholderQR.hpp
  \author Manh Ha NGUYEN
  \since 22 Jan 2013
  \date  7 Aug 2013
  
  \brief Definition of the xlifepp::HouseholderQR class

  Class deals with Householder QR decomposition.
*/

// This file is adapted from Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2009 Claire Maurice
// Copyright (C) 2009 Gael Guennebaud <gael.guennebaud@inria.fr>
// Copyright (C) 2010 Jitse Niesen <jitse@maths.leeds.ac.uk>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_QR_HPP
#define EIGEN_QR_HPP

#include "../utils/VectorEigenDense.hpp"
#include "../houseHolder/HouseHolderSequence.hpp"
#include "../houseHolder/BlockHouseholder.hpp"

namespace xlifepp {

/*!
  \class HouseholderQR
  
  \brief Householder QR decomposition of a matrix
  
  \param MatrixType the type of the matrix of which we are computing the QR decomposition
  
  This class performs a QR decomposition of a matrix \b A into matrices \b Q and \b R
  such that 
  \f[
   \mathbf{A} = \mathbf{Q} \, \mathbf{R}
  \f]
  by using Householder transformations. Here, \b Q a unitary matrix and \b R an upper triangular matrix.
  The result is stored in a compact way compatible with LAPACK.
  
  Note that no pivoting is performed. This is \b not a rank-revealing decomposition.
*/
template<typename _MatrixType>
class HouseholderQR
{
  public:

    typedef _MatrixType MatrixType;

    typedef typename MatrixType::type_t Scalar;
    typedef typename NumTraits<Scalar>::RealScalar RealScalar;
    //typedef typename MatrixType::Index Index;
    //typedef Matrix<Scalar, RowsAtCompileTime, RowsAtCompileTime, (MatrixType::Flags&RowMajorBit) ? RowMajor : ColMajor, MaxRowsAtCompileTime, MaxRowsAtCompileTime> MatrixQType;
    typedef VectorEigenDense<Scalar> HCoeffsType;
    typedef VectorEigenDense<Scalar> RowVectorType;
    typedef typename HouseholderSequence<MatrixType,HCoeffsType>::ConjugateReturnType HouseholderSequenceType;

    /*!
      \brief Default Constructor.
    
      The default constructor is useful in cases in which the user intends to
      perform decompositions via HouseholderQR::compute(const MatrixType&).
    */
    HouseholderQR() : qr_(), hCoeffs_(), isInitialized_(false) {}

    /*!
      \brief Default Constructor with memory preallocation
      
      Like the default constructor but with preallocation of the internalEigenSolver data
      according to the specified problem \a size.
      \sa HouseholderQR()
    */
    HouseholderQR(Index rows, Index cols)
      : qr_(rows, cols),
        hCoeffs_((std::min)(rows,cols)),
        isInitialized_(false) {}

    //! constructor by type of matrix
    HouseholderQR(const MatrixType& matrix)
      : qr_(matrix.numOfRows(), matrix.numOfCols()),
        hCoeffs_((std::min)(matrix.numOfRows(),matrix.numOfCols())),
        isInitialized_(false)
    {
      compute(matrix);
    }

    /* This method finds a solution x to the equation Ax=b, where A is the matrix of which
      * *this is the QR decomposition, if any exists.
      *
      * \param b the right-hand-side of the equation to solve.
      *
      * \returns a solution.
      *
      * \note The case where b is a matrix is not yet implemented. Also, this
      *       code is space inefficient.
      *
      * \note_about_checking_solutions
      *
      * \note_about_arbitrary_choice_of_solution
      *
      * Example: \\include HouseholderQR_solve.cpp
      * Output: \\verbinclude HouseholderQR_solve.out
      */
//    template<typename Rhs>
//    inline const internalEigenSolver::solve_retval<HouseholderQR, Rhs>
//    solve(const MatrixBase<Rhs>& b) const
//    {
//      eigen_assert(isInitialized_ && "HouseholderQR is not initialized.");
//      return internalEigenSolver::solve_retval<HouseholderQR, Rhs>(*this, b.derived());
//    }

    HouseholderSequenceType householderQ() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "HouseholderQR"); }
      return HouseholderSequenceType(qr_, conj(hCoeffs_));
    }

    MatrixType matrixQ()
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "HouseholderQR"); }
      return HouseholderSequenceType(qr_, conj(hCoeffs_))
             .setLength(qr_.numOfRows()-1)
             .setShift(0)
             .matrixQ();
    }

    /*!
      \returns a reference to the matrix where the Householder QR decomposition is stored
      in a LAPACK-compatible way.
    */
    const MatrixType& matrixQR() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "HouseholderQR"); }
      return qr_;
    }

    HouseholderQR& compute(const MatrixType& matrix);

    /*!
      \returns the absolute value of the determinant of the matrix of which
      *this is the QR decomposition. It has only linear complexity
      (that is, O(n) where n is the dimension of the square matrix)
      as the QR decomposition has already been computed.
      
      \note This is only for square matrices.
      
      \warning a determinant can be very big or small, so for matrices
      of large enough dimension, there is a risk of overflow/underflow.
      One way to work around that is to use logAbsDeterminant() instead.
      
      \sa logAbsDeterminant(), MatrixBase::determinant()
    */
    typename MatrixType::RealScalar absDeterminant() const;

    /*!
      \returns the natural log of the absolute value of the determinant of the matrix of which
      *this is the QR decomposition. It has only linear complexity
      (that is, O(n) where n is the dimension of the square matrix)
      as the QR decomposition has already been computed.
      
      \note This is only for square matrices.
      
      \note This method is useful to work around the risk of overflow/underflow that's inherent
      to determinant computation.
      
      \sa absDeterminant(), MatrixBase::determinant()
    */
    typename MatrixType::RealScalar logAbsDeterminant() const;

    inline Index numOfRows() const { return qr_.numOfRows(); }
    inline Index numOfCols() const { return qr_.numOfCols(); }
    const HCoeffsType& hCoeffs() const { return hCoeffs_; }

  protected:
    MatrixType qr_;
    HCoeffsType hCoeffs_;
    bool isInitialized_;
};

//template<typename MatrixType>
//typename MatrixType::RealScalar HouseholderQR<MatrixType>::absDeterminant() const
//{
//  eigen_assert(isInitialized_ && "HouseholderQR is not initialized.");
//  eigen_assert(qr_.numOfRows() == qr_.numOfCols() && "You can't take the determinant of a non-square matrix!");
//  return internalEigenSolver::abs(qr_.diagonal().prod());
//}
//
//template<typename MatrixType>
//typename MatrixType::RealScalar HouseholderQR<MatrixType>::logAbsDeterminant() const
//{
//  eigen_assert(isInitialized_ && "HouseholderQR is not initialized.");
//  eigen_assert(qr_.numOfRows() == qr_.numOfCols() && "You can't take the determinant of a non-square matrix!");
//  return qr_.diagonal().cwiseAbs().array().log().sum();
//}

namespace internalEigenSolver {

template<typename MatrixQR, typename HCoeffs>
void householderQRinplaceUnblocked(MatrixQR& mat, HCoeffs& hCoeffs)
{
  typedef typename MatrixQR::type_t Scalar;
  typedef typename NumTraits<Scalar>::RealScalar RealScalar;
  Index rows = mat.numOfRows();
  Index cols = mat.numOfCols();
  Index size = (std::min)(rows,cols);

  if (!((Index)hCoeffs.size() == size)) mat.sizeMisMatch("Householder vector mismatch", hCoeffs.size(), size);

  typedef VectorEigenDense<Scalar> TempType;
  //typedef Matrix<Scalar,MatrixQR::ColsAtCompileTime,1> TempType;
  TempType tempVector;

  for(Index k = 0; k < size; ++k)
  {
    Index remainingRows = rows - k;
    Index remainingCols = cols - k - 1;

    RealScalar beta;
    //mat.col(k).tail(remainingRows).makeHouseholderInPlace(hCoeffs.coeffRef(k), beta);
    TempType col = mat.columnVector(k);
    col.makeHouseHolderInPlace(hCoeffs.coeffRef(k), beta, remainingRows);
    mat.columnVector(k, col);
    mat.coeffRef(k,k) = beta;

    // apply H to remaining part of qr_ from the left
//    mat.bottomRightCorner(remainingRows, remainingCols)
//        .applyHouseholderOnTheLeft(mat.col(k).tail(remainingRows-1), hCoeffs.coeffRef(k), tempData+k+1);
    MatrixQR bottomRightCorner = mat.bottomRightCorner(remainingRows, remainingCols);
    col = mat.columnVector(k);
    TempType colTail = col.tail(remainingRows -1);
    bottomRightCorner.applyHouseholderOnTheLeft(colTail, hCoeffs.coeffRef(k));
    mat.bottomRightCorner(remainingRows, remainingCols, bottomRightCorner);
  }
}

template<typename MatrixQR, typename HCoeffs>
void householderQRinplaceBlocked(MatrixQR& mat, HCoeffs& hCoeffs, Index maxBlockSize=32)
{
    typedef typename MatrixQR::type_t Scalar;
    typedef MatrixQR BlockType;
  //typedef Block<MatrixQR,Dynamic,Dynamic> BlockType;

  Index rows = mat.numOfRows();
  Index cols = mat.numOfCols();
  Index size = (std::min)(rows, cols);

  //typedef Matrix<Scalar,Dynamic,1,ColMajor,MatrixQR::MaxColsAtCompileTime,1> TempType;
  typedef VectorEigenDense<Scalar> TempType;
  TempType tempVector;

  Index blockSize = (std::min)(maxBlockSize,size);

  Index k = 0;
  for (k = 0; k < size; k += blockSize)
  {
    Index bs = (std::min)(size-k,blockSize);  // actual size of the block
    Index tcols = cols - k - bs;            // trailing columns
    Index brows = rows-k;                   // rows of the block

    // partition the matrix:
    //        A00 | A01 | A02
    // mat  = A10 | A11 | A12
    //        A20 | A21 | A22
    // and performs the qr dec of [A11^T A12^T]^T
    // and update [A21^T A22^T]^T using level 3 operations.
    // Finally, the algorithm continue on A22

    //BlockType A11_21 = mat.block(k,k,brows,bs);
    BlockType A11_21(mat, k, k, brows,bs);
    //Block<HCoeffs,Dynamic,1> hCoeffsSegment = hCoeffs.segment(k,bs);
    TempType hCoeffsSegment = hCoeffs.segment(k,bs);
    householderQRinplaceUnblocked(A11_21, hCoeffsSegment);
    mat.replace(A11_21, k, k, brows,bs);

    if(tcols)
    {
      //BlockType A21_22 = mat.block(k,k+bs,brows,tcols);
      BlockType A21_22(mat,k,k+bs,brows,tcols);
      applyBlockHouseholderOnTheLeft(A21_22,A11_21,adjointVec(hCoeffsSegment));
      mat.replace(A21_22, k,k+bs,brows,tcols);
    }
    hCoeffs.segment(hCoeffsSegment, k, bs);
  }
}

//template<typename _MatrixType, typename Rhs>
//struct solve_retval<HouseholderQR<_MatrixType>, Rhs>
//  : solve_retval_base<HouseholderQR<_MatrixType>, Rhs>
//{
//  EIGEN_MAKE_SOLVE_HELPERS(HouseholderQR<_MatrixType>,Rhs)
//
//  template<typename Dest> void evalTo(Dest& dst) const
//  {
//    const Index rows = dec().numOfRows(), cols = dec().numOfCols();
//    const Index rank = (std::min)(rows, cols);
//    eigen_assert(rhs().numOfRows() == rows);
//
//    typename Rhs::PlainObject c(rhs());
//
//    // Note that the matrix Q = H_0^* H_1^*... so its inverse is Q^* = (H_0 H_1 ...)^T
//    c.applyOnTheLeft(householderSequence(
//      dec().matrixQR().leftCols(rank),
//      dec().hCoeffs().head(rank)).transpose()
//    );
//
//    dec().matrixQR()
//       .topLeftCorner(rank, rank)
//       .template triangularView<Upper>()
//       .solveInPlace(c.topRows(rank));
//
//    dst.topRows(rank) = c.topRows(rank);
//    dst.bottomRows(cols-rank).setZero();
//  }
//};

} // end namespace internalEigenSolver

template<typename MatrixType>
HouseholderQR<MatrixType>& HouseholderQR<MatrixType>::compute(const MatrixType& matrix)
{
  Index rows = matrix.numOfRows();
  Index cols = matrix.numOfCols();
  Index size = (std::min)(rows,cols);

  qr_ = matrix;
  hCoeffs_.resize(size);

  internalEigenSolver::householderQRinplaceBlocked(qr_, hCoeffs_, 48);

  isInitialized_ = true;
  return *this;
}

/*! \return the Householder QR decomposition of \c *this.
  *
  * \sa class HouseholderQR
  */
//template<typename Derived>
//const HouseholderQR<typename MatrixBase<Derived>::PlainObject>
//MatrixBase<Derived>::householderQr() const
//{
//  return HouseholderQR<PlainObject>(eval());
//}

} // end namespace xlifepp

#endif // EIGEN_QR_HPP
