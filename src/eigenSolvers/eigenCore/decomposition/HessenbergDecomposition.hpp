/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file HessenbergDecomposition.hpp
  \author Manh Ha NGUYEN
  \since 22 Jan 2013
  \date  7 Mars 2013

  \brief Definition of the xlifepp::HessenbergDecomposition

  Class deals with Hessenber decomposition.
*/

// This file is adapted from Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2009 Claire Maurice
// Copyright (C) 2009 Gael Guennebaud <gael.guennebaud@inria.fr>
// Copyright (C) 2010 Jitse Niesen <jitse@maths.leeds.ac.uk>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_HESSENBERGDECOMPOSITION_HPP
#define EIGEN_HESSENBERGDECOMPOSITION_HPP

#include "../eigenCore.hpp"

namespace xlifepp
{

/*!
  \class HessenbergDecomposition
  
  \brief Reduces a square matrix to Hessenberg form by an orthogonal similarity transformation
  
  \tparam _MatrixType the type of the matrix of which we are computing the Hessenberg decomposition
  
  This class performs an Hessenberg decomposition of a matrix \f$ A \f$. In
  the real case, the Hessenberg decomposition consists of an orthogonal
  matrix \f$ Q \f$ and a Hessenberg matrix \f$ H \f$ such that \f$ A = Q H
  Q^T \f$. An orthogonal matrix is a matrix whose inverse equals its
  transpose (\f$ Q^{-1} = Q^T \f$). A Hessenberg matrix has zeros below the
  subdiagonal, so it is almost upper triangular. The Hessenberg decomposition
  of a complex matrix is \f$ A = Q H Q^* \f$ with \f$ Q \f$ unitary (that is,
  \f$ Q^{-1} = Q^* \f$).
  
  Call the function compute() to compute the Hessenberg decomposition of a
  given matrix. Alternatively, you can use the
  HessenbergDecomposition(const MatrixType&) constructor which computes the
  Hessenberg decomposition at construction time. Once the decomposition is
  computed, you can use the matrixH() and matrixQ() functions to construct
  the matrices H and Q in the decomposition.
*/
template<typename _MatrixType>
class HessenbergDecomposition
{
  public:
  
    //! \brief Synonym for the template parameter \p _MatrixType.
    typedef _MatrixType MatrixType;
    
    //! \brief Scalar type for matrices of type #MatrixType.
    typedef typename MatrixType::type_t Scalar;
    
    /*!
      \brief Type for vector of Householder coefficients.
      
      This is column vector with entries of type #Scalar. The length of the
      vector is one less than the size of #MatrixType,
    */
    typedef VectorEigenDense<Scalar> CoeffVectorType;
    
    //! \brief Return type of matrixQ()
    typedef typename HouseholderSequence<MatrixType, CoeffVectorType>::ConjugateReturnType HouseholderSequenceType;
    
    /*!
      \brief Default constructor; the decomposition will be computed later.
      
      \param [in] size  The size of the matrix whose Hessenberg decomposition will be computed.
      
      The default constructor is useful in cases in which the user intends to
      perform decompositions via compute().  The \p size parameter is only
      used as a hint. It is not an error to give a wrong \p size, but it may
      impair performance.
      
      \sa compute() for an example.
    */
    HessenbergDecomposition(number_t size)
      : matrix_(size, size),
        isInitialized_(false)
    {
      if(size > 1)
      { hCoeffs_.resize(size - 1); }
    }
    
    /*!
      \brief Constructor; computes Hessenberg decomposition of given matrix.
      
      \param[in]  matrix  Square matrix whose Hessenberg decomposition is to be computed.
      
      This constructor calls compute() to compute the Hessenberg
      decomposition.
      
      \sa matrixH() for an example.
    */
    HessenbergDecomposition(const MatrixType& matrix)
      : matrix_(matrix),
        isInitialized_(false)
    {
      if(matrix.numOfRows() < 2)
      {
        isInitialized_ = true;
        return;
      }
      hCoeffs_.resize(matrix.numOfRows() - 1, 1);
      computeInplace(matrix_, hCoeffs_);
      isInitialized_ = true;
    }
    
    /*!
      \brief Computes Hessenberg decomposition of given matrix.
      
      \param[in]  matrix  Square matrix whose Hessenberg decomposition is to be computed.
      \returns    Reference to \c *this
      
      The Hessenberg decomposition is computed by bringing the columns of the
      matrix successively in the required form using Householder reflections
      (see, e.g., Algorithm 7.4.2 in Golub \& Van Loan, <i>%Matrix
      Computations</i>). The cost is \f$ 10n^3/3 \f$ flops, where \f$ n \f$
      denotes the size of the given matrix.
      
      This method reuses of the allocated data in the HessenbergDecomposition
      object.
    */
    HessenbergDecomposition& compute(const MatrixType& matrix)
    {
      matrix_ = matrix;
      if(matrix.numOfRows() < 2)
      {
        isInitialized_ = true;
        return *this;
      }
      hCoeffs_.resize(matrix.numOfRows() - 1, 1);
      computeInplace(matrix_, hCoeffs_);
      isInitialized_ = true;
      return *this;
    }
    
    /*!
      \brief Returns the Householder coefficients.
      
      \returns a const reference to the vector of Householder coefficients
      
      \pre Either the constructor HessenbergDecomposition(const MatrixType&)
      or the member function compute(const MatrixType&) has been called
      before to compute the Hessenberg decomposition of a matrix.
      
      The Householder coefficients allow the reconstruction of the matrix
      \f$ Q \f$ in the Hessenberg decomposition from the packed data.
    */
    const CoeffVectorType& householderCoefficients() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "HessenbergDecomposition"); }
      return hCoeffs_;
    }
    
    /*!
      \brief Returns the internal representation of the decomposition
      
      \returns a const reference to a matrix with the internal representation
               of the decomposition.
      
      \pre Either the constructor HessenbergDecomposition(const MatrixType&)
      or the member function compute(const MatrixType&) has been called
      before to compute the Hessenberg decomposition of a matrix.
      
      The returned matrix contains the following information:
        - the upper part and lower sub-diagonal represent the Hessenberg matrix H
        - the rest of the lower part contains the Householder vectors that, combined with
          Householder coefficients returned by householderCoefficients(),
          allows to reconstruct the matrix Q as
             \f$ Q = H_{N-1} \ldots H_1 H_0 \f$.
          Here, the matrices \f$ H_i \f$ are the Householder transformations
             \f$ H_i = (I - h_i v_i v_i^T) \f$
          where \f$ h_i \f$ is the \f$ i \f$th Householder coefficient and
          \f$ v_i \f$ is the Householder vector defined by
             \f$ v_i = [ 0, \ldots, 0, 1, M(i+2,i), \ldots, M(N-1,i) ]^T \f$
          with M the matrix returned by this function.
      
      See LAPACK for further details on this packed storage.
    */
    const MatrixType& packedMatrix() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "HessenbergDecomposition"); }
      return matrix_;
    }
    
    /*!
      \brief Reconstructs the orthogonal matrix Q in the decomposition
      
      \returns object representing the matrix Q
      
      \pre Either the constructor HessenbergDecomposition(const MatrixType&)
      or the member function compute(const MatrixType&) has been called
      before to compute the Hessenberg decomposition of a matrix.
      
      This function returns a light-weight object of template class
      HouseholderSequence. You can either apply it directly to a matrix or
      you can convert it to a matrix of type #MatrixType.
    */
    MatrixType matrixQ()
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "HessenbergDecomposition"); }
      return HouseholderSequenceType(matrix_, conj(hCoeffs_))
             .setLength(matrix_.numOfRows() - 1)
             .setShift(1)
             .unitaryMatrix();
    }
    
    /*!
      \brief Constructs the Hessenberg matrix H in the decomposition
      
      \returns expression object representing the matrix H
      
      \pre Either the constructor HessenbergDecomposition(const MatrixType&)
      or the member function compute(const MatrixType&) has been called
      before to compute the Hessenberg decomposition of a matrix.
      
      The object returned by this function constructs the Hessenberg matrix H
      when it is assigned to a matrix or otherwise evaluated. The matrix H is
      constructed from the packed matrix as returned by packedMatrix(): The
      upper part (including the subdiagonal) of the packed matrix contains
      the matrix H. It may sometimes be better to directly use the packed
      matrix instead of constructing the matrix H.
      
      Example: \\include test_EigenSolverDecomposition.cpp
      
      \sa matrixQ(), packedMatrix()
    */
    MatrixType matrixH() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "HessenbergDecomposition"); }
      number_t n = matrix_.numOfRows();
      if (n <= 2) { return matrix_; }
      else
      {
        MatrixType result(matrix_);
        MatrixType temp(result, 2, 0, n - 2, n - 2);
        temp.setZeroLowerTriangle();
        result.replace(temp, 2, 0, n - 2, n - 2);
        return result;
      }
    }
    
    static void computeInplace(MatrixType& matA, CoeffVectorType& hCoeffs);
  private:
    typedef typename NumTraits<Scalar>::RealScalar RealScalar;
    
  protected:
    MatrixType matrix_;
    CoeffVectorType hCoeffs_;
    bool isInitialized_; //!< returns true if initialized
};

/*!
  \internal
  Performs a tridiagonal decomposition of \a matA in place.
  
  \param matA the input selfadjoint matrix
  \param hCoeffs returned Householder coefficients
  
  The result is written in the lower triangular part of \a matA.
  
  Implemented from Golub's "%Matrix Computations", algorithm 8.3.1.
  
  \sa packedMatrix()
*/
template<typename MatrixType>
void HessenbergDecomposition<MatrixType>::computeInplace(MatrixType& matA, CoeffVectorType& hCoeffs)
{
  if (matA.numOfRows() != matA.numOfCols()) { matA.nonSquare("Computing hessenberg decomposition", matA.numOfRows(), matA.numOfCols()); }
  number_t n = matA.numOfRows();
  for (number_t i = 0; i < n - 1; ++i)
  {
    // let's consider the vector v = i-th column starting at position i+1
    number_t remainingSize = n - i - 1;
    RealScalar beta;
    Scalar h;
    
    //    matA.col(i).tail(remainingSize).makeHouseholderInPlace(h, beta);
    //    matA.col(i).coeffRef(i+1) = beta;
    //    hCoeffs.coeffRef(i) = h;
    CoeffVectorType col = matA.columnVector(i);
    col.makeHouseHolderInPlace(h, beta, remainingSize);
    col[i + 1] = Scalar(beta);
    
    // Apply similarity transformation to remaining columns,
    // i.e., compute A = H A H'
    MatrixType cornerMat, rightCols;
    // A = H A
    //    matA.bottomRightCorner(remainingSize, remainingSize)
    //        .applyHouseholderOnTheLeft(matA.col(i).tail(remainingSize-1), h, &temp.coeffRef(0));
    CoeffVectorType colTemp = col.tail(remainingSize - 1);
    cornerMat = matA.bottomRightCorner(remainingSize, remainingSize);
    cornerMat.applyHouseholderOnTheLeft(colTemp, h);
    matA.bottomRightCorner(remainingSize, remainingSize, cornerMat);
    
    //    // A = A H'
    //    matA.rightCols(remainingSize)
    //        .applyHouseholderOnTheRight(matA.col(i).tail(remainingSize-1).conjugate(), internal::conj(h), &temp.coeffRef(0));
    rightCols = matA.bottomRightCorner(n, remainingSize);
    rightCols.applyHouseholderOnTheRight(conj(colTemp), conj(h));
    matA.bottomRightCorner(n, remainingSize, rightCols);
    
    col.tail(remainingSize - 1, colTemp);
    matA.columnVector(i, col);
    hCoeffs[i] = h;
  }
}

} // end namespace xlifepp

#endif // EIGEN_HESSENBERGDECOMPOSITION_HPP
