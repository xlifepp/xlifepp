/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file Tridiagonalization.hpp
  \author Manh Ha NGUYEN
  \since 22 Jan 2013
  \date  7 Mars 2013

  \brief Definition of the xlifepp::Tridiagonalization class

  Class deals with Matrix Tridiagonalization.
*/

// This file is adapted from Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2009 Claire Maurice
// Copyright (C) 2009 Gael Guennebaud <gael.guennebaud@inria.fr>
// Copyright (C) 2010 Jitse Niesen <jitse@maths.leeds.ac.uk>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_TRIDIAGONALIZATION_HPP
#define EIGEN_TRIDIAGONALIZATION_HPP

#include "../eigenCore.hpp"
#include "../houseHolder/HouseHolderSequence.hpp"

namespace xlifepp
{

namespace internalEigenSolver
{
template<typename MatrixType, typename CoeffVectorType>
void tridiagonalizationInplace(MatrixType& matA, CoeffVectorType& hCoeffs);
}
/*!
  \class Tridiagonalization
  
  \brief Tridiagonal decomposition of a selfadjoint matrix
  
  \tparam _MatrixType the type of the matrix of which we are computing the
  tridiagonal decomposition; this is expected to be an instantiation of the
  Matrix class template.
  
  This class performs a tridiagonal decomposition of a selfadjoint matrix \f$ A \f$ such that:
  \f$ A = Q T Q^* \f$ where \f$ Q \f$ is unitary and \f$ T \f$ a real symmetric tridiagonal matrix.
  
  A tridiagonal matrix is a matrix which has nonzero elements only on the
  main diagonal and the first diagonal below and above it. The Hessenberg
  decomposition of a selfadjoint matrix is in fact a tridiagonal
  decomposition. This class is used in SelfAdjointEigenSolver to compute the
  eigenvalues and eigenvectors of a selfadjoint matrix.
  
  Call the function compute() to compute the tridiagonal decomposition of a
  given matrix. Alternatively, you can use the Tridiagonalization(const MatrixType&)
  constructor which computes the tridiagonal Schur decomposition at
  construction time. Once the decomposition is computed, you can use the
  matrixQ() and matrixT() functions to retrieve the matrices Q and T in the
  decomposition.
*/
template<typename _MatrixType>
class Tridiagonalization
{
  public:
  
    //! Synonym for the template parameter \p _MatrixType.
    typedef _MatrixType MatrixType;
    
    typedef typename MatrixType::type_t Scalar;
    typedef typename NumTraits<Scalar>::RealScalar RealScalar;
    typedef VectorEigenDense<Scalar> CoeffVectorType;
    typedef VectorEigenDense<RealScalar> DiagonalType;
    typedef VectorEigenDense<RealScalar> SubDiagonalType;
    
    typedef typename Conditional<NumTraits<Scalar>::IsComplex, CoeffVectorType, DiagonalType>::type DiagonalReturnType;
    typedef typename Conditional<NumTraits<Scalar>::IsComplex, CoeffVectorType, SubDiagonalType>::type SubDiagonalReturnType;
    
    //! Return type of matrixQ()
    typedef typename HouseholderSequence<MatrixType, CoeffVectorType>::ConjugateReturnType HouseholderSequenceType;
    
    /*!
      \brief Default constructor.
      
      \param[in]  size  Positive integer, size of the matrix whose tridiagonal
      decomposition will be computed.
      
      The default constructor is useful in cases in which the user intends to
      perform decompositions via compute().  The \p size parameter is only
      used as a hint. It is not an error to give a wrong \p size, but it may
      impair performance.
      
      \sa compute() for an example.
    */
    Tridiagonalization(dimen_t size)
      : matrix_(size, size),
        hCoeffs_(size > 1 ? size - 1 : 1),
        isInitialized_(false)
    {}
    
    /*!
      \brief Constructor; computes tridiagonal decomposition of given matrix.
      
      \param[in]  matrix  Selfadjoint matrix whose tridiagonal decomposition
      is to be computed.
      
      This constructor calls compute() to compute the tridiagonal decomposition.
    */
    Tridiagonalization(const MatrixType& matrix)
      : matrix_(matrix),
        hCoeffs_(matrix.numOfCols() > 1 ? matrix.numOfCols() - 1 : 1),
        isInitialized_(false)
    {
      internalEigenSolver::tridiagonalizationInplace(matrix_, hCoeffs_);
      isInitialized_ = true;
    }
    
    /*!
      \brief Computes tridiagonal decomposition of given matrix.
      
      \param[in]  matrix  Selfadjoint matrix whose tridiagonal decomposition
      is to be computed.
      \returns    Reference to \c *this
      
      The tridiagonal decomposition is computed by bringing the columns of
      the matrix successively in the required form using Householder
      reflections. The cost is \f$ 4n^3/3 \f$ flops, where \f$ n \f$ denotes
      the size of the given matrix.
      
      This method reuses of the allocated data in the Tridiagonalization
      object, if the size of the matrix does not change.
      
      Example: \\include test_EigenSolverDecomposition.cpp
    */
    Tridiagonalization& compute(const MatrixType& matrix)
    {
      matrix_ = matrix;
      hCoeffs_.resize(matrix.numOfRows() - 1, 1);
      internalEigenSolver::tridiagonalizationInplace(matrix_, hCoeffs_);
      isInitialized_ = true;
      return *this;
    }
    
    /*!
      \brief Returns the Householder coefficients.
      
      \returns a const reference to the vector of Householder coefficients
      
      \pre Either the constructor Tridiagonalization(const MatrixType&) or
      the member function compute(const MatrixType&) has been called before
      to compute the tridiagonal decomposition of a matrix.
      
      The Householder coefficients allow the reconstruction of the matrix
      \f$ Q \f$ in the tridiagonal decomposition from the packed data.
    */
    inline CoeffVectorType householderCoefficients() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "Tridiagonalization"); }
      return hCoeffs_;
    }
    
    /*!
      \brief Returns the internal representation of the decomposition
      
      \returns a const reference to a matrix with the internal representation
               of the decomposition.
      
      \pre Either the constructor Tridiagonalization(const MatrixType&) or
      the member function compute(const MatrixType&) has been called before
      to compute the tridiagonal decomposition of a matrix.
      
      The returned matrix contains the following information:
        - the strict upper triangular part is equal to the input matrix A.
        - the diagonal and lower sub-diagonal represent the real tridiagonal
          symmetric matrix T.
        - the rest of the lower part contains the Householder vectors that,
          combined with Householder coefficients returned by
          householderCoefficients(), allows to reconstruct the matrix Q as
             \f$ Q = H_{N-1} \ldots H_1 H_0 \f$.
          Here, the matrices \f$ H_i \f$ are the Householder transformations
             \f$ H_i = (I - h_i v_i v_i^T) \f$
          where \f$ h_i \f$ is the \f$ i \f$th Householder coefficient and
          \f$ v_i \f$ is the Householder vector defined by
             \f$ v_i = [ 0, \ldots, 0, 1, M(i+2,i), \ldots, M(N-1,i) ]^T \f$
          with M the matrix returned by this function.
      
      See LAPACK for further details on this packed storage.
      
      \sa householderCoefficients()
    */
    inline const MatrixType& packedMatrix() const
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "Tridiagonalization"); }
      return matrix_;
    }
    
    /*!
      \brief Returns the unitary matrix Q in the decomposition
      
      \returns object representing the matrix Q
      
      \pre Either the constructor Tridiagonalization(const MatrixType&) or
      the member function compute(const MatrixType&) has been called before
      to compute the tridiagonal decomposition of a matrix.
      
      This function returns a light-weight object of template class
      HouseholderSequence. You can either apply it directly to a matrix or
      you can convert it to a matrix of type #MatrixType.      *
    */
    MatrixType matrixQ()
    {
      if (!isInitialized_) { error("eigensolver_not_initialized", "Tridiagonalization"); }
      return HouseholderSequenceType(matrix_, conj(hCoeffs_))
             .setLength(matrix_.numOfRows() - 1)
             .setShift(1)
             .unitaryMatrix();
    }
    
    /*!
      \brief Returns the diagonal of the tridiagonal matrix T in the decomposition.
      
      \returns expression representing the diagonal of T
      
      \pre Either the constructor Tridiagonalization(const MatrixType&) or
      the member function compute(const MatrixType&) has been called before
      to compute the tridiagonal decomposition of a matrix.
      
      \sa subDiagonal()
    */
    DiagonalReturnType diagonal() const;
    
    /*!
      \brief Returns the subdiagonal of the tridiagonal matrix T in the decomposition.
      
      \returns expression representing the subdiagonal of T
      
      \pre Either the constructor Tridiagonalization(const MatrixType&) or
      the member function compute(const MatrixType&) has been called before
      to compute the tridiagonal decomposition of a matrix.
    */
    SubDiagonalReturnType subDiagonal() const;
    
  private:
    //    static void tridiagonalizationInplace(MatrixType& matA, CoeffVectorType& hCoeffs);
    //    static void tridiagonalizationInplace(MatrixType& mat, DiagonalType& diag, SubDiagonalType& subdiag, bool extractQ);
    
  protected:
    MatrixType matrix_;
    CoeffVectorType hCoeffs_;
    bool isInitialized_;
};

namespace internalEigenSolver
{
/*!
  Performs a tridiagonal decomposition of the selfadjoint matrix \a matA in-place.
  
  \param[in,out] matA On input the selfadjoint matrix. Only the \b lower triangular part is referenced.
                      On output, the strict upper part is left unchanged, and the lower triangular part
                      represents the T and Q matrices in packed format has detailed below.
  \param[out]    hCoeffs returned Householder coefficients (see below)
  
  On output, the tridiagonal selfadjoint matrix T is stored in the diagonal
  and lower sub-diagonal of the matrix \a matA.
  The unitary matrix Q is represented in a compact way as a product of
  Householder reflectors \f$ H_i \f$ such that:
        \f$ Q = H_{N-1} \ldots H_1 H_0 \f$.
  The Householder reflectors are defined as
        \f$ H_i = (I - h_i v_i v_i^T) \f$
  where \f$ h_i = hCoeffs[i]\f$ is the \f$ i \f$th Householder coefficient and
  \f$ v_i \f$ is the Householder vector defined by
        \f$ v_i = [ 0, \ldots, 0, 1, matA(i+2,i), \ldots, matA(N-1,i) ]^T \f$.
  
  Implemented from Golub's "Matrix Computations", algorithm 8.3.1.
  
  \sa Tridiagonalization::packedMatrix()
*/
template<typename MatrixType, typename CoeffVectorType>
void tridiagonalizationInplace(MatrixType& matA, CoeffVectorType& hCoeffs)
{
  typedef typename MatrixType::type_t Scalar;
  typedef typename NumTraits<Scalar>::RealScalar RealScalar;
  number_t n = matA.numOfRows();
  if (n != matA.numOfCols()) { matA.nonSquare("Tridiagonalization", n, matA.numOfCols()); }
  if (n != (hCoeffs.size() + 1)) { matA.sizeMisMatch("House holder wrong size", hCoeffs.size() + 1, n); }
  
  for (number_t i = 0; i < n - 1; ++i)
  {
    number_t remainingSize = n - i - 1;
    RealScalar beta;
    Scalar h;
    //    matA.col(i).tail(remainingSize).makeHouseholderInPlace(h, beta);
    CoeffVectorType col = matA.columnVector(i);
    col.makeHouseHolderInPlace(h, beta, remainingSize);
    
    // Apply similarity transformation to remaining columns,
    // i.e., A = H A H' where H = I - h v v' and v = matA.col(i).tail(n-i-1)
    //matA.col(i).coeffRef(i+1) = 1;
    col.coeffRef(i + 1) = 1;
    matA.columnVector(i, col);
    
    //    hCoeffs.tail(n-i-1).noalias() = (matA.bottomRightCorner(remainingSize,remainingSize).template selfadjointView<Lower>()
    //                                  * (conj(h) * matA.col(i).tail(remainingSize)));
    CoeffVectorType hCoeffTemp = hCoeffs.tail(remainingSize);
    CoeffVectorType colTemp = col.tail(remainingSize);
    
    Indexing idx(4);
    idx[0] = idx[1] = i + 1;
    idx[2] = idx[3] = remainingSize;
    matA.multSubMatVecVec(idx, conj(h)*colTemp, hCoeffTemp);
    
    // hCoeffs.tail(n-i-1) += (conj(h)*Scalar(-0.5)*(hCoeffs.tail(remainingSize).dot(matA.col(i).tail(remainingSize)))) * matA.col(i).tail(n-i-1);
    
    hCoeffTemp += (conj(h)) * Scalar(-0.5) * (hCoeffTemp.dotProduct(colTemp)) * colTemp;
    
    
    //    matA.bottomRightCorner(remainingSize, remainingSize).template selfadjointView<Lower>()
    //      .rankUpdate(matA.col(i).tail(remainingSize), hCoeffs.tail(remainingSize), -1);
    matA.rankUpdate(idx, colTemp, hCoeffTemp, -1);
    col.coeffRef(i + 1) = Scalar(beta);
    matA.columnVector(i, col);
    hCoeffs.tail(remainingSize, hCoeffTemp);
    hCoeffs.coeffRef(i) = h;
  }
}

template<typename MatrixType, typename CoeffVectorType>
void tridiagonalizationUpperInplace(MatrixType& matA, CoeffVectorType& hCoeffs)
{
  typedef typename MatrixType::type_t Scalar;
  typedef typename NumTraits<Scalar>::RealScalar RealScalar;
  number_t n = matA.numOfRows();
  if (n != matA.numOfCols()) { matA.nonSquare("Tridiagonalization", n, matA.numOfCols()); }
  if (n != (hCoeffs.size() + 1)) { matA.sizeMisMatch("House holder wrong size", hCoeffs.size() + 1, n); }

  //for (number_t i = 0; i < n - 1; ++i)
  for (number_t i = n-1; i > 0; --i)
  {
    number_t remainingSize = n - i - 1;
    RealScalar beta;
    Scalar h;
    //    matA.col(i).tail(remainingSize).makeHouseholderInPlace(h, beta);
    CoeffVectorType col = matA.columnVector(i);
    col.makeHouseHolderInPlace(h, beta, remainingSize);

    // Apply similarity transformation to remaining columns,
    // i.e., A = H A H' where H = I - h v v' and v = matA.col(i).tail(n-i-1)
    //matA.col(i).coeffRef(i+1) = 1;
    col.coeffRef(i + 1) = 1;
    matA.columnVector(i, col);

    //    hCoeffs.tail(n-i-1).noalias() = (matA.bottomRightCorner(remainingSize,remainingSize).template selfadjointView<Lower>()
    //                                  * (conj(h) * matA.col(i).tail(remainingSize)));
    CoeffVectorType hCoeffTemp = hCoeffs.tail(remainingSize);
    CoeffVectorType colTemp = col.tail(remainingSize);

    Indexing idx(4);
    idx[0] = idx[1] = i + 1;
    idx[2] = idx[3] = remainingSize;
    matA.multSubMatVecVec(idx, conj(h)*colTemp, hCoeffTemp);

    // hCoeffs.tail(n-i-1) += (conj(h)*Scalar(-0.5)*(hCoeffs.tail(remainingSize).dot(matA.col(i).tail(remainingSize)))) * matA.col(i).tail(n-i-1);

    hCoeffTemp += (conj(h)) * Scalar(-0.5) * (hCoeffTemp.dotProduct(colTemp)) * colTemp;


    //    matA.bottomRightCorner(remainingSize, remainingSize).template selfadjointView<Lower>()
    //      .rankUpdate(matA.col(i).tail(remainingSize), hCoeffs.tail(remainingSize), -1);
    matA.rankUpdate(idx, colTemp, hCoeffTemp, -1);
    col.coeffRef(i + 1) = Scalar(beta);
    matA.columnVector(i, col);
    hCoeffs.tail(remainingSize, hCoeffTemp);
    hCoeffs.coeffRef(i) = h;
  }
}

/*!
  \brief Performs a full tridiagonalization in place
  
  \param[in,out]  mat  On input, the selfadjoint matrix whose tridiagonal
     decomposition is to be computed. Only the lower triangular part referenced.
     The rest is left unchanged. On output, the orthogonal matrix Q
     in the decomposition if \p extractQ is true.
  \param[out]  diag  The diagonal of the tridiagonal matrix T in the
     decomposition.
  \param[out]  subdiag  The subdiagonal of the tridiagonal matrix T in
     the decomposition.
  \param[in]  extractQ  If true, the orthogonal matrix Q in the
     decomposition is computed and stored in \p mat.
  
  Computes the tridiagonal decomposition of the selfadjoint matrix \p mat in place
  such that \f$ mat = Q T Q^* \f$ where \f$ Q \f$ is unitary and \f$ T \f$ a real
  symmetric tridiagonal matrix.
  
  The tridiagonal matrix T is passed to the output parameters \p diag and \p subdiag. If
  \p extractQ is true, then the orthogonal matrix Q is passed to \p mat. Otherwise the lower
  part of the matrix \p mat is destroyed.
  
  The vectors \p diag and \p subdiag are not resized. The function
  assumes that they are already of the correct size. The length of the
  vector \p diag should equal the number of rows in \p mat, and the
  length of the vector \p subdiag should be one left.
  
  \note Currently, it requires two temporary vectors to hold the intermediate
  Householder coefficients, and to reconstruct the matrix Q from the Householder
  reflectors.
  
  \sa class Tridiagonalization
*/
template<typename MatrixType, typename DiagonalType, typename SubDiagonalType>
void tridiagonalizationInplace(MatrixType& mat, DiagonalType& diag, SubDiagonalType& subdiag, bool extractQ)
{
  typedef typename Tridiagonalization<MatrixType>::CoeffVectorType CoeffVectorType;
  typedef typename Tridiagonalization<MatrixType>::HouseholderSequenceType HouseholderSequenceType;

  CoeffVectorType hCoeffs(mat.numOfCols() - 1);
  tridiagonalizationInplace(mat, hCoeffs);
  diag = mat.diagonal().real();
  subdiag = mat.subDiagonal().real();
  if(extractQ)
  {
    mat = HouseholderSequenceType(mat, conj(hCoeffs))
          .setLength(mat.numOfRows() - 1)
          .setShift(1)
          .unitaryMatrix();
  }
}

} // end namespace internalEigenSolver

} // end namespace xlifepp

#endif // EIGEN_TRIDIAGONALIZATION_HPP
