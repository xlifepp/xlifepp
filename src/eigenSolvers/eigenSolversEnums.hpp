/*
XLiFE++ is an extended library of finite elements written in C++
    Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file eigenSolversEnums.hpp
  \author Yvon Lafranche
  \since 27 June 2016
  \date  27 June 2016

  \brief Collect enumerations internally used in eigenSolvers directory
*/

#ifndef EIGENSOLVERS_ENUMS_HPP
#define EIGENSOLVERS_ENUMS_HPP

//! main namespace of XLiFE++ library
namespace xlifepp {

//! type of eigen problems
enum EigenSolverMode
{
  /*! Used in SelfAdjointEigenSolver and GeneralizedSelfAdjointEigenSolver to specify
    * that only the eigenvalues are to be computed and not the eigenvectors. */
  _eigenValueOnly      = 0x40,
  /*! Used in SelfAdjointEigenSolver and GeneralizedSelfAdjointEigenSolver to specify
    * that both the eigenvalues and the eigenvectors are to be computed. */
  _computeEigenVector  = 0x80,
  _eigVecMask = _eigenValueOnly | _computeEigenVector,
  /*! Used in GeneralizedSelfAdjointEigenSolver to indicate that it should
    * solve the generalized eigenproblem \f$ Ax = \lambda B x \f$. */
  _Ax_lBx              = 0x100,
  /*! Used in GeneralizedSelfAdjointEigenSolver to indicate that it should
    * solve the generalized eigenproblem \f$ ABx = \lambda x \f$. */
  _ABx_lx              = 0x200,
  /*! Used in GeneralizedSelfAdjointEigenSolver to indicate that it should
    * solve the generalized eigenproblem \f$ BAx = \lambda x \f$. */
  _BAx_lx              = 0x400,
  _genEigMask = _Ax_lBx | _ABx_lx | _BAx_lx
};

/*!
  Enumerated list of available message types recognized by the eigensolvers.
  These message types are also used with verboseLevel to print out warning (debugging) info.
*/
enum MsgEigenType
{
  _errorsEigen = 0,               //!< Errors [ always printed ]
  _warningsEigen = 0x1,           //!< Internal warnings
  _iterationDetailsEigen = 0x2,   //!< Approximate eigenvalues, errors
  _orthoDetailsEigen = 0x4,       //!< Orthogonalization/orthonormalization details
  _finalSummaryEigen = 0x8,       //!< Final computational summary
  _timingDetailsEigen = 0x10,     //!< Timing details
  _statusTestDetailsEigen = 0x20, //!< Status test details
  _debugEigen = 0x40              //!< Debugging information
};

//! Enumerated type used to pass back information from a StatusTest
enum TestStatus {
  _passed    = 0x1,    //!< The solver passed the test
  _failed    = 0x2,    //!< The solver failed the test
  _undefined = 0x4     //!< The test has not been evaluated on the solver
};

} //end of namespace xlifepp

#endif /* EIGENSOLVERS_ENUMS_HPP */
