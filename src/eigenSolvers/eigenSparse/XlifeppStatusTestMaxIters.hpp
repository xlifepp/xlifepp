/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file XlifeppStatusTestMaxIters.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013

  \brief Status test for testing the number of iterations.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_STATUS_TEST_MAXITER_HPP
#define XLIFEPP_STATUS_TEST_MAXITER_HPP

#include "XlifeppStatusTest.hpp"

namespace xlifepp {

/*!
  \class StatusTestMaxIters
  \brief A status test for testing the number of iterations.

  xlifepp::StatusTestMaxIters will test true when an eigensolver has reached some number
  of iterations. Specifically,
  <pre>
                   { _passed,  if solver->getNumIters() >= maxIter
  status(solver) = {
                   { _failed,  if solver->getNumIters()  < maxIter
  </pre>
  where maxIter is the parameter given to the status tester.

  This status test also supports negation, so that it negates the need for a
  StatusTestMinIters status tester. In this way, all tests on the range of iterations
  can be constructed through the appropriate use of StatusTestMaxIters and StatusTestCombo.
*/
template <class ScalarType, class MV, class OP>
class StatusTestMaxIters : public StatusTest<ScalarType,MV,OP> {

 public:
  //! @name Constructors/destructors
  //@{

  //! Constructor
  StatusTestMaxIters(int maxIter, bool negate = false) : state_(_undefined), negate_(negate) {
    setMaxIters(maxIter);
  };

  //! Destructor
  virtual ~StatusTestMaxIters() {}
  //@}

  //! @name Status methods
  //@{

  /*! \brief Check status as defined by test.
    \return TestStatus indicating whether the test passed or failed.
  */
  TestStatus checkStatus(EigenSolver<ScalarType,MV,OP>* solver) {
    state_ = (solver->getNumIters() >= maxIters_) ? _passed : _failed;
    if (negate_) {
      if (state_ == _passed) state_ = _failed;
      else state_ = _passed;
    }
    return state_;
  }

  //! \brief Return the result of the most recent checkStatus call.
  TestStatus getStatus() const {
    return state_;
  }

  //! Get the indices for the vectors that passed the test.
  std::vector<int> whichVecs() const {
    return std::vector<int>(0);
  }

  //! Get the number of vectors that passed the test.
  int howMany() const {
    return 0;
  }

  //@}

  //! @name Accessor methods
  //@{

  /*! \brief Set the maximum number of iterations.
   *  \note This also resets the test status to ::_undefined.
   */
  void setMaxIters(int maxIters) {
    state_ = _undefined;
    maxIters_ = maxIters;
  }

  //! \brief Get the maximum number of iterations.
  int getMaxIters() {return maxIters_;}

  /*! \brief Set the negation policy for the status test.
   *  \note This also reset the test status to ::_undefined.
   */
  void setNegate(bool negate) {
    state_ = _undefined;
    negate_ = negate;
  }

  //! \brief Get the negation policy for the status test.
  bool getNegate() const {
    return negate_;
  }

  //@}

  //! @name Reset methods
  //@{
  //! Informs the status test that it should reset its internal configuration to the uninitialized state.
  /*! The StatusTestMaxIters class has no internal state, so this call is equivalent to calling clearStatus().
    eigenvalue problem. The status test may have information that pertains to a particular problem or solver
    state. The internal information will be reset back to the uninitialized state. The user specified information
    that the convergence test uses will remain.
  */
  void reset() {
    state_ = _undefined;
  }

  //! \brief Clears the results of the last status test.
  /*! This should be distinguished from the reset() method, as it only clears the cached result from the last
   * status test, so that a call to getStatus() will return ::_undefined. This is necessary for the SEQOR and SEQAND
   * tests in the StatusTestCombo class, which may short circuit and not evaluate all of the StatusTests contained
   * in them.
  */
  void clearStatus() {
    state_ = _undefined;
  }

  //@}

  //! @name Print methods
  //@{

  //! Output formatted description of stopping test to output stream.
  std::ostream& print(std::ostream& os, int indent = 0) const {
    std::string ind(indent,' ');
    os << ind << "- StatusTestMaxIters: ";
    switch (state_) {
    case _passed:
      os << "_passed" << std::endl;
      break;
    case _failed:
      os << "_failed" << std::endl;
      break;
    case _undefined:
      os << "_undefined" << std::endl;
      break;
    }
    os << ind << "  MaxIters: " << maxIters_ << std::endl;
    return os;
  }

   PrintStream& print(PrintStream& os, int indent = 0) const
   { print(os.currentStream(),indent); return os;}

  //@}
  private:
  int maxIters_;
  TestStatus state_;
  bool negate_;

};

} // end of xlifepp namespace

#endif /* XLIFEPP_STATUS_TEST_MAXITER_HPP */
