/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file XlifeppBlockKrylovSchurSolMgr.hpp
  \author Manh Ha NGUYEN
  \since 13 June 2013
  \date 24 July 2013

  \brief The xlifepp::BlockKrylovSchurSolMgr class provides a solver manager for the BlockKrylovSchur eigensolver.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_BLOCK_KRYLOV_SCHUR_SOLMGR_HPP
#define XLIFEPP_BLOCK_KRYLOV_SCHUR_SOLMGR_HPP

#include "config.h"
#include "XlifeppEigenTypes.hpp"
#include "EigenTestError.hpp"
#include "XlifeppEigenProblem.hpp"
#include "XlifeppSolverManager.hpp"
#include "XlifeppSolverUtils.hpp"

#include "XlifeppBlockKrylovSchur.hpp"
#include "XlifeppBasicSort.hpp"
#include "XlifeppSVQBOrthoManager.hpp"
#include "XlifeppBasicOrthoManager.hpp"
#include "XlifeppStatusTestResNorm.hpp"
#include "XlifeppStatusTestWithOrdering.hpp"
#include "XlifeppStatusTestCombo.hpp"
#include "XlifeppStatusTestOutput.hpp"
#include "XlifeppBasicOutputManager.hpp"

namespace xlifepp {

/*! \class BlockKrylovSchurSolMgr
 *
 *  \brief The BlockKrylovSchurSolMgr provides a flexible solver manager over the BlockKrylovSchur eigensolver.
 *
 * The solver manager provides to the solver a StatusTestCombo object constructed as follows:<br>
 *    &nbsp;&nbsp;&nbsp;<tt>combo = globaltest _OR debugtest</tt><br>
 * where
 *    - \c globaltest terminates computation when global convergence has been detected.<br>
 *      It is encapsulated in a StatusTestWithOrdering object, to ensure that computation is terminated
 *      only after the most significant eigenvalues/eigenvectors have met the convergence criteria.<br>
 *      If not specified via setGlobalStatusTest(), this test is a StatusTestResNorm object which tests the
 *      2-norms of the Ritz residuals relative to the Ritz values.
 *    - \c debugtest allows a user to specify additional monitoring of the iteration, encapsulated in a StatusTest object<br>
 *      If not specified via setDebugStatusTest(), \c debugtest is ignored.<br>
 *      In most cases, it should return ::_failed; if it returns ::_passed, solve() will throw an XlifeppError exception.
 *
 * Additionally, the solver manager will terminate solve() after a specified number of restarts.
 *
 * Much of this behavior is controlled via parameters and options passed to the
 * solver manager. For more information, see BlockKrylovSchurSolMgr().
 */
template<class ScalarType, class MV, class OP>
class BlockKrylovSchurSolMgr : public SolverManager<ScalarType,MV,OP> {

  private:
    typedef MultiVecTraits<ScalarType,MV> MVT;
    typedef OperatorTraits<ScalarType,MV,OP> OPT;
    typedef NumTraits<ScalarType> SCT;
    typedef typename NumTraits<ScalarType>::magnitudeType MagnitudeType;
    typedef NumTraits<MagnitudeType> MT;

  public:

  //! @name Constructors/Destructor
  //@{
  /*! \brief Basic constructor for BlockKrylovSchurSolMgr.
   *
   * This constructor accepts the EigenProblem to be solved in addition
   * to a parameter list of options for the solver manager. These options include the following:
   *   - Solver parameters
   *      - \c "Which" - a \c string specifying the desired eigenvalues: SM, LM, SR or LR. Default: "LM"
   *      - \c "Block Size" - a \c int specifying the block size to be used by the underlying block Krylov-Schur solver. Default: 1
   *      - \c "Num Blocks" - a \c int specifying the number of blocks allocated for the Krylov basis. Default: 3*nev
   *      - \c "Extra NEV Blocks" - a \c int specifying the number of extra blocks the solver should keep in addition to those
             required to compute the number of eigenvalues requested.  Default: 0
   *      - \c "Maximum Restarts" - a \c int specifying the maximum number of restarts the underlying solver is allowed to perform. Default: 20
   *      - \c "Orthogonalization" - a \c string specifying the desired orthogonalization:  DGKS and SVQB. Default: "SVQB"
   *      - \c "Verbosity" - a sum of MsgType specifying the verbosity. Default: xlifepp::_errorsEigen
   *   - Convergence parameters
   *      - \c "Convergence Tolerance" - a \c MagnitudeType specifying the level that residual norms must reach to decide convergence. Default: machine precision.
   *      - \c "Relative Convergence Tolerance" - a \c bool specifying whether residuals norms should be scaled by their eigenvalues for the purposing of deciding convergence. Default: true
   */
  BlockKrylovSchurSolMgr(const SmartPtr<EigenProblem<ScalarType,MV,OP> >& problem, Parameters& pl);

  //! Destructor.
  virtual ~BlockKrylovSchurSolMgr() {}
  //@}

  //! @name Accessor methods
  //@{
  //! Return the eigenvalue problem.
  const EigenProblem<ScalarType,MV,OP>& getProblem() const {
    return *problem_;
  }

  //! Get the iteration count for the most recent call to \c solve().
  int getNumIters() const {
    return numIters_;
  }

  /*! \brief Return the Ritz values from the most recent solve.
   */
  std::vector<ValueEigenSolver<ScalarType> > getRitzValues() const {
    std::vector<ValueEigenSolver<ScalarType> > ret(ritzValues_);
    return ret;
  }
  //@}

  //! @name Solver application methods
  //@{
  /*! \brief This method performs possibly repeated calls to the underlying eigensolver's iterate() routine
   * until the problem has been solved (as decided by the solver manager) or the solver manager decides to
   * quit.
   *
   * This method calls BlockKrylovSchur::iterate(), which will return either because a specially constructed status test evaluates to ::_passed
   * or an exception is thrown.
   *
   * A return from BlockKrylovSchur::iterate() signifies one of the following scenarios:
   *    - the maximum number of restarts has been exceeded. In this scenario, the solver manager will place\n
   *      all converged eigenpairs into the eigenproblem and return ::_noConvergence.
   *    - global convergence has been met. In this case, the most significant NEV eigenpairs in the solver and locked storage  \n
   *      have met the convergence criterion. (Here, NEV refers to the number of eigenpairs requested by the EigenProblem.)    \n
   *      In this scenario, the solver manager will return ::_success.
   *
   * \returns ::ComputationInfo specifying:
   *     - ::_success: the eigenproblem was solved to the specification required by the solver manager.
   *     - ::_noConvergence: the eigenproblem was not solved to the specification desired by the solver manager.
  */
  ComputationInfo solve();

  //! Set the status test defining global convergence.
  void setGlobalStatusTest(const SmartPtr< StatusTest<ScalarType,MV,OP> >& global);

  //! Get the status test defining global convergence.
  const SmartPtr< StatusTest<ScalarType,MV,OP> > & getGlobalStatusTest() const;

  //! Set the status test for debugging.
  void setDebugStatusTest(const SmartPtr< StatusTest<ScalarType,MV,OP> >& debug);

  //! Get the status test for debugging.
  const SmartPtr< StatusTest<ScalarType,MV,OP> >& getDebugStatusTest() const;
  //@}

  private:
  SmartPtr<EigenProblem<ScalarType,MV,OP> > problem_;
  SmartPtr<SortManager<MagnitudeType> > sort_;

  std::string whch_, ortho_;
  MagnitudeType orthoKappa_;

  MagnitudeType convtol_;
  int_t maxRestarts_;
  bool relconvtol_,conjSplit_;
  int_t blockSize_, numBlocks_, stepSize_, nevBlocks_, xtraNevBlocks_;
  int_t numIters_;
  MsgEigenType verbosity_;
  bool inSituRestart_;

  std::vector<ValueEigenSolver<ScalarType> > ritzValues_;

  SmartPtr<StatusTest<ScalarType,MV,OP> > globalTest_;
  SmartPtr<StatusTest<ScalarType,MV,OP> > debugTest_;

  int_t printNum_;

  SmartPtr<BasicOutputManager<ScalarType> > printer_;
};


// Constructor
template<class ScalarType, class MV, class OP>
BlockKrylovSchurSolMgr<ScalarType,MV,OP>::BlockKrylovSchurSolMgr(
        const SmartPtr<EigenProblem<ScalarType,MV,OP> >& problem,
        Parameters& pl) :
  problem_(problem),
  whch_("LM"),
  ortho_("SVQB"),
  orthoKappa_(-1.0),
  convtol_(0),
  maxRestarts_(20),
  relconvtol_(true),
  conjSplit_(false),
  blockSize_(0),
  numBlocks_(0),
  stepSize_(0),
  nevBlocks_(0),
  xtraNevBlocks_(0),
  numIters_(0),
  verbosity_(xlifepp::_errorsEigen),
  inSituRestart_(false),
  printNum_(-1)
{
  internalEigenSolver::testErrorEigenProblem(problem_ == _smPtrNull, "Problem not given to solver manager.");
  internalEigenSolver::testErrorEigenProblem(!problem_->isProblemSet(), "Problem not set.");
  internalEigenSolver::testErrorEigenProblem(problem_->getInitVec() == _smPtrNull, "Problem does not contain initial vectors to clone from.");

  const int_t nev = problem_->getNEV();

  // convergence tolerance
  convtol_ = pl.get("Convergence Tolerance",MT::prec());
  relconvtol_ = pl.get("Relative Convergence Tolerance",relconvtol_);

  // maximum number of restarts
  maxRestarts_ = pl.get("Maximum Restarts",maxRestarts_);

  // block size: default is 1
  blockSize_ = pl.get("Block Size",int_t(1));
  internalEigenSolver::testErrorEigenProblem(blockSize_ <= 0,
                     "xlifepp::BlockKrylovSchurSolMgr: \"Block Size\" must be strictly positive.");

  // set the number of blocks we need to save to compute the nev eigenvalues of interest.
  xtraNevBlocks_ = pl.get("Extra NEV Blocks",int_t(0));
  if (nev%blockSize_) {
    nevBlocks_ = nev/blockSize_ + xtraNevBlocks_ + 1;
  } else {
    nevBlocks_ = nev/blockSize_ + xtraNevBlocks_;
  }

  numBlocks_ = pl.get("Num Blocks",3*nevBlocks_);
  internalEigenSolver::testErrorEigenProblem(numBlocks_ <= nevBlocks_,
                     "xlifepp::BlockKrylovSchurSolMgr: \"Num Blocks\" must be strictly positive and large enough to compute the requested eigenvalues.");

  internalEigenSolver::testErrorEigenProblem(numBlocks_*blockSize_ > MVT::getVecLength(*problem_->getInitVec()),

                     "xlifepp::BlockKrylovSchurSolMgr: Potentially impossible orthogonality requests. Reduce basis size.");

  // step size: the default is maxRestarts_*numBlocks_, so that Ritz values are only computed every restart.
  if (maxRestarts_) {
    stepSize_ = pl.get("Step Size", (maxRestarts_+1)*(numBlocks_+1));
  } else {
    stepSize_ = pl.get("Step Size", numBlocks_+1);
  }
  internalEigenSolver::testErrorEigenProblem(stepSize_ < 1,
                     "xlifepp::BlockKrylovSchurSolMgr: \"Step Size\" must be strictly positive.");

  // get the sort manager
  if (pl.contains("Sort Manager")) {
    // sort_ = Teuchos::getParameter<SmartPtr<xlifepp::SortManager<MagnitudeType> > >(pl,"Sort Manager");
  } else {
    // which values to solve for
    whch_ = pl.get("Which",whch_);
    internalEigenSolver::testErrorEigenProblem(whch_ != "SM" && whch_ != "LM" && whch_ != "SR" && whch_ != "LR" && whch_ != "SI" && whch_ != "LI",
                        "Invalid sorting string.");
    sort_ = smartPtr(new BasicSort<MagnitudeType>(whch_));
  }

  // which orthogonalization to use
  ortho_ = pl.get("Orthogonalization",ortho_);
  if (ortho_ != "DGKS" && ortho_ != "SVQB") {
    ortho_ = "SVQB";
  }

  // which orthogonalization constant to use
  orthoKappa_ = pl.get("Orthogonalization Constant",orthoKappa_);

  // verbosity level
  if (pl.contains("Verbosity")) {
    verbosity_ = (MsgEigenType)pl.get("Verbosity", (int_t)verbosity_);
  }
    //    if (Teuchos::isParameterType<int>(pl,"Verbosity")) {
    //      verbosity_ = pl.get("Verbosity", verbosity_);
    //    } else {
    //      verbosity_ = (int)Teuchos::getParameter<xlifepp::MsgType>(pl,"Verbosity");
    //    }
    //  }

  // restarting technique: V*Q or applyHouse(V,H,tau)
  if (pl.contains("In Situ Restarting")) {
    inSituRestart_ = pl.get("In Situ Restarting",inSituRestart_);
  }
//    if (Teuchos::isParameterType<bool>(pl,"In Situ Restarting")) {
//      inSituRestart_ = pl.get("In Situ Restarting",inSituRestart_);
//    } else {
//      inSituRestart_ = (Teuchos::getParameter<int>(pl,"In Situ Restarting") != 0);
//    }
//  }

  printNum_ = pl.get("Print Number of Ritz Values",int_t(-1));

  // output stream
  string_t fntemplate = "";
  bool allProcs = false;
//  if (pl.isParameter("Output on all processors")) {
//    if (Teuchos::isParameterType<bool>(pl,"Output on all processors")) {
//      allProcs = pl.get("Output on all processors",allProcs);
//    } else {
//      allProcs = (Teuchos::getParameter<int>(pl,"Output on all processors") != 0);
//    }
//  }
//  fntemplate = pl.get("Output filename template",fntemplate);
  int myPID;
# ifdef HAVE_MPI
  // Initialize MPI
  int mpiStarted = 0;
  MPI_Initialized(&mpiStarted);
  if (mpiStarted) MPI_Comm_rank(MPI_COMM_WORLD, &myPID);
  else myPID=0;
# else
  myPID = 0;
# endif
  if (fntemplate != "") {
    std::ostringstream myPIDstr;
    myPIDstr << myPID;
    // replace %d in fntemplate with myPID
    int pos, start=0;
    while ((pos = fntemplate.find("%d",start)) != -1) {
      fntemplate.replace(pos,2,myPIDstr.str());
      start = pos+2;
    }
  }

  const void* myStrVoid = nullptr;
  SmartPtr<std::ostream> osp;

  if (pl.contains("Write Out Result")) {
    myStrVoid = pl.get("Write Out Result", myStrVoid);
    osp = SmartPtr<std::ostream>((std::ostream*)myStrVoid, false);
  }
  else if (fntemplate != "") {
    osp = SmartPtr<std::ostream>(new std::ofstream(fntemplate.c_str(),std::ios::out | std::ios::app));
    if (!*osp) {
      osp = SmartPtr<std::ostream>(&std::cout, false);
      std::cout << "xlifepp::BlockDavidsonSolMgr::constructor(): Could not open file for write: " << fntemplate << std::endl;
    }
  }
  else {
    osp = SmartPtr<std::ostream>(&std::cout, false);
  }

  // Output manager
  MsgEigenType verbosity = _errorsEigen;
  if (pl.contains("Verbosity")) {
    verbosity = (MsgEigenType)pl.get("Verbosity", (int_t)verbosity);
  }

  if (allProcs) {
    // print on all procs
    printer_ = SmartPtr<BasicOutputManager<ScalarType> >(new BasicOutputManager<ScalarType>(verbosity,osp,myPID));
  }
  else {
    // print only on proc 0
    printer_ = SmartPtr<BasicOutputManager<ScalarType> >(new BasicOutputManager<ScalarType>(verbosity,osp,0));
  }
}

// solve()
template<class ScalarType, class MV, class OP>
ComputationInfo BlockKrylovSchurSolMgr<ScalarType,MV,OP>::solve() {

  const int nev = problem_->getNEV();
  ScalarType one = NumTraits<ScalarType>::one();
  ScalarType zero = NumTraits<ScalarType>::zero();

  typedef SolverUtils<ScalarType,MV,OP> msutils;

  //=============================================================================================
  // Output manager
  //SmartPtr<BasicOutputManager<ScalarType> > printer_ = smartPtr(new BasicOutputManager<ScalarType>(verbosity_));

  //=============================================================================================
  // Status tests
  //
  // convergence
  SmartPtr<StatusTest<ScalarType,MV,OP> > convtest;
  if (globalTest_ == _smPtrNull) {
    convtest = smartPtr(new StatusTestResNorm<ScalarType,MV,OP>(convtol_,nev,StatusTestResNorm<ScalarType,MV,OP>::_ritzRes2Norm,relconvtol_));
  }
  else {
    convtest = globalTest_;
  }
  SmartPtr<StatusTestWithOrdering<ScalarType,MV,OP> > ordertest
    = smartPtr(new StatusTestWithOrdering<ScalarType,MV,OP>(convtest,sort_,nev));
  // for a non-short-circuited _OR test, the order doesn't matter
  std::vector<SmartPtr<StatusTest<ScalarType,MV,OP> > > alltests;
  alltests.push_back(ordertest);

  if (debugTest_ != _smPtrNull) alltests.push_back(debugTest_);

  SmartPtr<StatusTestCombo<ScalarType,MV,OP> > combotest
    = smartPtr(new StatusTestCombo<ScalarType,MV,OP>(StatusTestCombo<ScalarType,MV,OP>::_OR, alltests));
  // printing StatusTest
  SmartPtr<StatusTestOutput<ScalarType,MV,OP> > outputtest;
  if (printer_->isVerbosity(_debugEigen)) {
    outputtest = smartPtr(new StatusTestOutput<ScalarType,MV,OP>(printer_,combotest,1,_passed+_failed+_undefined));
  }
  else {
    outputtest = smartPtr(new StatusTestOutput<ScalarType,MV,OP>(printer_,combotest,1,_passed));
  }

  //=============================================================================================
  // Orthomanager
  SmartPtr<OrthoManager<ScalarType,MV> > ortho;
  if (ortho_=="SVQB") {
    ortho = smartPtr(new SVQBOrthoManager<ScalarType,MV,OP>(problem_->getM(), false));
  } else if (ortho_=="DGKS") {
    if (orthoKappa_ <= 0) {
      ortho = smartPtr(new BasicOrthoManager<ScalarType,MV,OP>(problem_->getM()));
    }
    else {
      ortho = smartPtr(new BasicOrthoManager<ScalarType,MV,OP>(problem_->getM(),orthoKappa_));
    }
  } else {
    internalEigenSolver::testErrorEigenProblem(ortho_!="SVQB"&& ortho_!="DGKS","xlifepp::BlockKrylovSchurSolMgr::solve(): Invalid orthogonalization type.");
  }

  //=============================================================================================
  // Parameter list
  Parameters plist;
  plist.add("Block Size",blockSize_);
  plist.add("Num Blocks",numBlocks_);
  plist.add("Step Size",stepSize_);
  if (printNum_ == -1) {
    plist.add("Print Number of Ritz Values",nevBlocks_*blockSize_);
  }
  else {
    plist.add("Print Number of Ritz Values",printNum_);
  }

  //=============================================================================================
  // BlockKrylovSchur solver
  SmartPtr<BlockKrylovSchur<ScalarType,MV,OP> > bksSolver
    = smartPtr(new BlockKrylovSchur<ScalarType,MV,OP>(problem_,sort_,printer_,outputtest,ortho,plist));
  // set any auxiliary vectors defined in the problem
  SmartPtr< const MV > probauxvecs = problem_->getAuxVecs();
  if (probauxvecs != _smPtrNull) {
    //bksSolver->setAuxVecs(Teuchos::tuple< SmartPtr<const MV> >(probauxvecs));
    bksSolver->setAuxVecs(std::vector<SmartPtr<const MV> >(1,probauxvecs));
  }

  // Create workspace for the Krylov basis generated during a restart
  // Need at most (nevBlocks_*blockSize_+1) for the updated factorization and another block for the current factorization residual block (F).
  //  ---> (nevBlocks_*blockSize_+1) + blockSize_
  // If Hermitian, this becomes nevBlocks_*blockSize_ + blockSize_
  // we only need this if there is the possibility of restarting, ex situ
  SmartPtr<MV> workMV;
  if (maxRestarts_ > 0) {
    if (inSituRestart_==true) {
      // still need one work vector for applyHouse()
      workMV = MVT::clone(*problem_->getInitVec(), 1);
    }
    else { // inSituRestart == false
      if (problem_->isHermitian()) {
        workMV = MVT::clone(*problem_->getInitVec(), nevBlocks_*blockSize_ + blockSize_);
      } else {
        workMV = MVT::clone(*problem_->getInitVec(), nevBlocks_*blockSize_+1 + blockSize_);
      }
    }
  } else {
    workMV = _smPtrNull;
  }

  // go ahead and initialize the solution to nothing in case we throw an exception
  EigenSolverSolution<ScalarType,MV> sol;
  sol.numVecs = 0;
  problem_->setSolution(sol);

  int numRestarts = 0;
  int curNevBlocks = 0;

  // enter solve() iterations
  {
    // tell bksSolver to iterate
    while (1) {
      bksSolver->iterate();

      //=============================================================================================
      //
      // check convergence first
      //
      //=============================================================================================
      if (ordertest->getStatus() == _passed) {
        // we have convergence
        // ordertest->whichVecs() tells us which vectors from solver state are the ones we want
        // ordertest->howMany() will tell us how many
        break;
      }
      //=============================================================================================
      //
      // check for restarting, i.e. the subspace is full
      //
      //=============================================================================================
      // this is for the Hermitian case, or non-Hermitian conjugate split situation.
      // --> for the Hermitian case the current subspace dimension needs to match the maximum subspace dimension
      // --> for the non-Hermitian case:
      //     --> if a conjugate pair was detected in the previous restart then the current subspace dimension needs to match the
      //         maximum subspace dimension (the BKS solver keeps one extra vector if the problem is non-Hermitian).
      //     --> if a conjugate pair was not detected in the previous restart then the current subspace dimension will be one less
      //         than the maximum subspace dimension.
      else if ((bksSolver->getCurSubspaceDim() == bksSolver->getMaxSubspaceDim()) ||
                (!problem_->isHermitian() && !conjSplit_ && (bksSolver->getCurSubspaceDim()+1 == bksSolver->getMaxSubspaceDim()))) {

        // Update the Schur form of the projected eigenproblem, then sort it.
        if (!bksSolver->isSchurCurrent()) {
          bksSolver->computeSchurForm(true);

          // Check for convergence, just in case we wait for every restart to check
          outputtest->checkStatus(&*bksSolver);
        }

        // Don't bother to restart if we've converged or reached the maximum number of restarts
        if (numRestarts >= maxRestarts_ || ordertest->getStatus() == _passed) {
          break; // break from while(1){bksSolver->iterate()}
        }

        // Start restarting timer and increment counter
        numRestarts++;

        if (printer_->isVerbosity(_iterationDetailsEigen)) {
            printer_->stream(_iterationDetailsEigen) << " Performing restart number " << numRestarts << " of " << maxRestarts_ << std::endl << std::endl;
        }

        // Get the most current Ritz values before we continue.
        ritzValues_ = bksSolver->getRitzValues();

        // Get the state.
        BlockKrylovSchurState<ScalarType,MV> oldState = bksSolver->getState();

        // Get the current dimension of the factorization
        int curDim = oldState.curDim;

        // Determine if the storage for the nev eigenvalues of interest splits a complex conjugate pair.
        std::vector<int> ritzIndex = bksSolver->getRitzIndex();
        if (ritzIndex[nevBlocks_*blockSize_-1]==1) {
          conjSplit_ = true;
          curNevBlocks = nevBlocks_*blockSize_+1;
        } else {
          conjSplit_ = false;
          curNevBlocks = nevBlocks_*blockSize_;
        }

        // Update the Krylov-Schur decomposition

        // Get a view of the Schur vectors of interest.
        // MatrixEigenDense<ScalarType> Qnev(Teuchos::View, *(oldState.Q), curDim, curNevBlocks);
        MatrixEigenDense<ScalarType> Qnev(*(oldState.Q), 0, 0, curDim, curNevBlocks);

        // Get a view of the current Krylov basis.
        std::vector<int> curind(curDim);
        for (int i=0; i<curDim; i++) { curind[i] = i; }
        SmartPtr<const MV> basistemp = MVT::cloneView(*(oldState.V), curind);

        // Compute the new Krylov basis: Vnew = V*Qnev
        //
        // this will occur ex situ in workspace allocated for this purpose (tmpMV)
        // or in situ in the solver's memory space.
        //
        // we will also set a pointer for the location that the current factorization residual block (F),
        // currently located after the current basis in oldstate.V, will be moved to
        //
        SmartPtr<MV> newF;
        if (inSituRestart_) {
          //
          // get non-const pointer to solver's basis so we can work in situ
          SmartPtr<MV> solverbasis = smartPtrConstCast<const MV>(oldState.V);
          //
          // perform Householder QR of copyQnev = Q [D;0], where D is unit diag. We will want D below.
          // lapack.GEQRF(curDim,curNevBlocks,copyQnev.values(),copyQnev.stride(),&tau[0],&work[0],work.size(),&info);
          std::vector<ScalarType> tau(curNevBlocks), work(curNevBlocks);
          HouseholderQR<MatrixEigenDense<ScalarType> > geqrf(Qnev);
          MatrixEigenDense<ScalarType> copyQnev = geqrf.matrixQR();
          tau = geqrf.hCoeffs();

          // we need to get the diagonal of D
          std::vector<ScalarType> d(curNevBlocks);
          for (int j=0; j<copyQnev.numOfCols(); j++) {
            d[j] = copyQnev.coeff(j,j);
          }
          if (printer_->isVerbosity(_debugEigen)) {
            MatrixEigenDense<ScalarType> R(copyQnev,0,0,curNevBlocks,curNevBlocks);
            for (int j=0; j<R.numOfCols(); j++) {
              R.coeffRef(j,j) = SCT::magnitude(R.coeff(j,j)) - 1.0;
              for (int i=j+1; i<R.numOfRows(); i++) {
                R.coeffRef(i,j) = zero;
              }
            }
            printer_->stream(_debugEigen) << "||Triangular factor of Su - I||: " << R.normFrobenius() << std::endl;
          }
          //
          // perform implicit V*Qnev
          // this actually performs V*[Qnev Qtrunc*M] = [newV truncV], for some unitary M
          // we are interested in only the first curNevBlocks vectors of the result
          curind.resize(curDim);
          for (int i=0; i<curDim; i++) curind[i] = i;
          {
            SmartPtr<MV> oldV = MVT::cloneViewNonConst(*solverbasis,curind);
            msutils::applyHouse(curNevBlocks,*oldV,copyQnev,tau,workMV);
          }
          // multiply newV*D
          // get pointer to new basis
          curind.resize(curNevBlocks);
          for (int i=0; i<curNevBlocks; i++) { curind[i] = i; }
          {
            SmartPtr<MV> newV = MVT::cloneViewNonConst(*solverbasis, curind);
            MVT::mvScale(*newV,d);
          }
          // get pointer to new location for F
          curind.resize(blockSize_);
          for (int i=0; i<blockSize_; i++) { curind[i] = curNevBlocks + i; }
          newF = MVT::cloneViewNonConst(*solverbasis, curind);
        }
        else {
          // get pointer to first part of work space
          curind.resize(curNevBlocks);
          for (int i=0; i<curNevBlocks; i++) { curind[i] = i; }
          SmartPtr<MV> tmp_newV = MVT::cloneViewNonConst(*workMV, curind);
          // perform V*Qnev
          MVT::mvTimesMatAddMv(one, *basistemp, Qnev, zero, *tmp_newV);
          tmp_newV = _smPtrNull;
          // get pointer to new location for F
          curind.resize(blockSize_);
          for (int i=0; i<blockSize_; i++) { curind[i] = curNevBlocks + i; }
          newF = MVT::cloneViewNonConst(*workMV, curind);
        }

        // Move the current factorization residual block (F) to the last block of newV.
        curind.resize(blockSize_);
        for (int i=0; i<blockSize_; i++) { curind[i] = curDim + i; }
        SmartPtr<const MV> oldF = MVT::cloneView(*(oldState.V), curind);
        for (int i=0; i<blockSize_; i++) { curind[i] = i; }
        MVT::setBlock(*oldF, curind, *newF);
        newF = _smPtrNull;

        // Update the Krylov-Schur quasi-triangular matrix.
        //
        // Create storage for the new Schur matrix of the Krylov-Schur factorization
        // Copy over the current quasi-triangular factorization of oldState.H which is stored in oldState.S.
        // MatrixEigenDense<ScalarType> oldS(Teuchos::View, *(oldState.S), curNevBlocks+blockSize_, curNevBlocks);
        MatrixEigenDense<ScalarType> oldS(*(oldState.S), 0, 0, curNevBlocks+blockSize_, curNevBlocks);
        SmartPtr<MatrixEigenDense<ScalarType> > newH =
          smartPtr(new MatrixEigenDense<ScalarType>(oldS));
        //
        // Get a view of the B block of the current factorization
        // MatrixEigenDense<ScalarType> oldB(Teuchos::View, *(oldState.H), blockSize_, blockSize_, curDim, curDim-blockSize_);
        MatrixEigenDense<ScalarType> oldB(*(oldState.H), curDim, curDim-blockSize_, blockSize_, blockSize_);
        //
        // Get a view of the a block row of the Schur vectors.
        // MatrixEigenDense<ScalarType> subQ(Teuchos::View, *(oldState.Q), blockSize_, curNevBlocks, curDim-blockSize_);
        MatrixEigenDense<ScalarType> subQ(*(oldState.Q), curDim-blockSize_, 0, blockSize_, curNevBlocks);
        //
        // Get a view of the new B block of the updated Krylov-Schur factorization
        // MatrixEigenDense<ScalarType> newB(Teuchos::View, *newH,  blockSize_, curNevBlocks, curNevBlocks);
        MatrixEigenDense<ScalarType> newB(*newH, curNevBlocks,  0, blockSize_, curNevBlocks);
        //
        // Compute the new B block.
        // blas.GEMM(Teuchos::NO_TRANS, Teuchos::NO_TRANS, blockSize_, curNevBlocks, blockSize_, one,
        //              oldB.values(), oldB.stride(), subQ.values(), subQ.stride(), zero, newB.values(), newB.stride());
        multMatMat(oldB, subQ, newB);
        newH->replace(newB, curNevBlocks,  0, blockSize_, curNevBlocks);

        //
        // Set the new state and initialize the solver.
        BlockKrylovSchurState<ScalarType,MV> newstate;
        if (inSituRestart_) {
          newstate.V = oldState.V;
        } else {
          newstate.V = workMV;
        }
        newstate.H = newH;
        newstate.curDim = curNevBlocks;
        bksSolver->initialize(newstate);

      } // end of restarting
      
      //=============================================================================================
      //
      // we returned from iterate(), but none of our status tests _passed.
      // something is wrong, and it is probably our fault.
      //
      //=============================================================================================
      else {
        internalEigenSolver::testErrorEigenProblem(true,"xlifepp::BlockKrylovSchurSolMgr::solve(): Invalid return from bksSolver::iterate().");
      }
    }

    //
    // free temporary space
    workMV = _smPtrNull;

    // Get the most current Ritz values before we return
    ritzValues_ = bksSolver->getRitzValues();

    sol.numVecs = ordertest->howMany();
    if (printer_->isVerbosity(_debugEigen)) {
        printer_->stream(_debugEigen) << "ordertest->howMany() : " << sol.numVecs << std::endl;
    }

    std::vector<int> whichVecs = ordertest->whichVecs();

    // Place any converged eigenpairs in the solution container.
    if (sol.numVecs > 0) {

      // Next determine if there is a conjugate pair on the boundary and resize.
      std::vector<int> tmpIndex = bksSolver->getRitzIndex();
      for (int i=0; i<(int)ritzValues_.size(); ++i) {
        if (printer_->isVerbosity(_debugEigen)) {
            printer_->stream(_debugEigen) << ritzValues_[i].realpart << " + i " << ritzValues_[i].imagpart << ", Index = " << tmpIndex[i] << std::endl;
        }
      }
      if (printer_->isVerbosity(_debugEigen)) {
          printer_->stream(_debugEigen) << "Number of converged eigenpairs (before) = " << sol.numVecs << std::endl;
      }
      for (int i=0; i<sol.numVecs; ++i) {
        if (printer_->isVerbosity(_debugEigen)) {
            printer_->stream(_debugEigen) << "whichVecs[" << i << "] = " << whichVecs[i] << ", tmpIndex[" << whichVecs[i] << "] = " << tmpIndex[whichVecs[i]] << std::endl;
        }
      }
      if (tmpIndex[whichVecs[sol.numVecs-1]]==1) {
        if (printer_->isVerbosity(_debugEigen)) {
            printer_->stream(_debugEigen) << "There is a conjugate pair on the boundary, resizing sol.numVecs" << std::endl;
        }
        whichVecs.push_back(whichVecs[sol.numVecs-1]+1);
        sol.numVecs++;
        for (int i=0; i<sol.numVecs; ++i) {
          if (printer_->isVerbosity(_debugEigen)) {
              printer_->stream(_debugEigen) << "whichVecs[" << i << "] = " << whichVecs[i] << ", tmpIndex[" << whichVecs[i] << "] = " << tmpIndex[whichVecs[i]] << std::endl;
          }
        }
      }

      bool keepMore = false;
      int numEvecs = sol.numVecs;
      if (printer_->isVerbosity(_debugEigen)) {
          printer_->stream(_debugEigen) << "Number of converged eigenpairs (after) = " << sol.numVecs << std::endl;
          printer_->stream(_debugEigen) << "whichVecs[sol.numVecs-1] > sol.numVecs-1 : " << whichVecs[sol.numVecs-1] << " > " << sol.numVecs-1 << std::endl;
      }
      if (whichVecs[sol.numVecs-1] > (sol.numVecs-1)) {
        keepMore = true;
        numEvecs = whichVecs[sol.numVecs-1]+1;  // Add 1 to fix zero-based indexing
        if (printer_->isVerbosity(_debugEigen)) {
            printer_->stream(_debugEigen) << "keepMore = true; numEvecs = " << numEvecs << std::endl;
        }
      }

      // Next set the number of Ritz vectors that the iteration must compute and compute them.
      bksSolver->setNumRitzVectors(numEvecs);
      bksSolver->computeRitzVectors();

      // If the leading Ritz pairs are the converged ones, get the information
      // from the iteration to the solution container. Otherwise copy the necessary
      // information using 'whichVecs'.
      if (!keepMore) {
        sol.index = bksSolver->getRitzIndex();
        sol.evals = bksSolver->getRitzValues();
        sol.evecs = MVT::cloneCopy(*(bksSolver->getRitzVectors()));
      }

      // Resize based on the number of solutions being returned and set the number of Ritz
      // vectors for the iteration to compute.
      sol.evals.resize(sol.numVecs);
      sol.index.resize(sol.numVecs);

      // If the converged Ritz pairs are not the leading ones, copy over the information directly.
      if (keepMore) {
        std::vector<xlifepp::ValueEigenSolver<ScalarType> > tmpEvals = bksSolver->getRitzValues();
        for (int vec_i=0; vec_i<sol.numVecs; ++vec_i) {
          sol.index[vec_i] = tmpIndex[whichVecs[vec_i]];
          sol.evals[vec_i] = tmpEvals[whichVecs[vec_i]];
        }
        sol.evecs = MVT::cloneCopy(*(bksSolver->getRitzVectors()), whichVecs);
      }

      // Set the solution space to be the Ritz vectors at this time.
      sol.espace = sol.evecs;
    }
  }

  // print final summary
  if (printer_->isVerbosity(_finalSummaryEigen)) {
      bksSolver->currentStatus(printer_->stream(_finalSummaryEigen));
  }

  // print timing information
  //Teuchos::TimeMonitor::summarize(printer_->stream(_timingDetailsEigen));

  problem_->setSolution(sol);
  if (printer_->isVerbosity(_debugEigen)) {
    printer_->stream(_debugEigen) << "Returning " << sol.numVecs << " eigenpairs to eigenproblem." << std::endl;
  }

  // get the number of iterations performed during this solve.
  numIters_ = bksSolver->getNumIters();

  if (sol.numVecs < nev) {
    return _noConvergence; // return from BlockKrylovSchurSolMgr::solve()
  }
  return _success; // return from BlockKrylovSchurSolMgr::solve()
}


template <class ScalarType, class MV, class OP>
void
BlockKrylovSchurSolMgr<ScalarType,MV,OP>::setGlobalStatusTest(
    const SmartPtr< StatusTest<ScalarType,MV,OP> >& global)
{
  globalTest_ = global;
}

template <class ScalarType, class MV, class OP>
const SmartPtr< StatusTest<ScalarType,MV,OP> >&
BlockKrylovSchurSolMgr<ScalarType,MV,OP>::getGlobalStatusTest() const
{
  return globalTest_;
}

template <class ScalarType, class MV, class OP>
void
BlockKrylovSchurSolMgr<ScalarType,MV,OP>::setDebugStatusTest(
    const SmartPtr< StatusTest<ScalarType,MV,OP> >& debug)
{
  debugTest_ = debug;
}

template <class ScalarType, class MV, class OP>
const SmartPtr< StatusTest<ScalarType,MV,OP> >&
BlockKrylovSchurSolMgr<ScalarType,MV,OP>::getDebugStatusTest() const
{
  return debugTest_;
}

} // end xlifepp namespace

#endif /* XLIFEPP_BLOCK_KRYLOV_SCHUR_SOLMGR_HPP */
