/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppStatusTestDecl.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013
  
  \brief Forward declaration of pure virtual base class xlifepp::StatusTest.
*/

#ifndef XLIFEPP_STATUS_TEST_DECL_HPP
#define XLIFEPP_STATUS_TEST_DECL_HPP

#include "XlifeppEigenTypes.hpp"

namespace xlifepp {

/*! 
  \class StatusTest
  \brief A pure virtual class for defining the status tests for the xlifepp iterative solvers
  
  xlifepp::StatusTest is an interface that can be implemented to create convergence tests for
  all xlifepp solvers.  Almost any kind of test can be expressed using this mechanism,
  including composite tests (see StatusTestCombo). 
*/
template <class ScalarType, class MV, class OP>
class StatusTest;

}

#endif /*XLIFEPP_STATUS_TEST_DECL_HPP */
