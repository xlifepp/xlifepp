/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppBlockKrylovSchur.hpp
  \author Manh Ha NGUYEN
  \since 13 June 2013
  \date 20 August 2013

  \brief Implementation of a block Krylov-Schur eigensolver.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_BLOCK_KRYLOV_SCHUR_HPP
#define XLIFEPP_BLOCK_KRYLOV_SCHUR_HPP

#include "EigenTestError.hpp"
#include "XlifeppEigenTypes.hpp"
#include "XlifeppEigenSolver.hpp"
#include "XlifeppMultiVecTraits.hpp"
#include "XlifeppOperatorTraits.hpp"
#include "XlifeppHelperTraits.hpp"
#include "XlifeppOrthoManager.hpp"

namespace xlifepp {

namespace internalEigenSolver {
/* Compute SchurForm */
template<typename MatrixType, bool IsComplex> struct ComputeSchurForm;
/* Sort (Real/Complex) Schur form */
template<typename MatrixType, bool IsComplex> struct SortSchurForm;
/* Compute eigenvector of dense matrix from Schur decomposition*/
template<typename MatrixType, bool IsComplex> struct EigenVectorComputation;
/* Compute eigenvalues of dense matrix from Schur decomposition*/
template<typename MatrixType, bool IsComplex> struct EigenValueComputation;
}

//! @name BlockKrylovSchur Structures 
//@{ 
/*! \brief Structure to contain pointers to BlockKrylovSchur state variables.
 *
 * This struct is utilized by BlockKrylovSchur::initialize() and BlockKrylovSchur::getState().
 */
template <class ScalarType, class MulVec>
struct BlockKrylovSchurState {
  /*! \brief The current dimension of the reduction.
   *
   * This should always be equal to BlockKrylovSchur::getCurSubspaceDim()
   */
  int curDim;
  /*! \brief The current Krylov basis. */
  SmartPtr<const MulVec> V;
  /*! \brief The current Hessenberg matrix. 
   *
   * The \c curDim by \c curDim leading submatrix of H is the 
   * projection of problem->getOperator() by the first \c curDim vectors in V. 
   */
  SmartPtr<const MatrixEigenDense<ScalarType> > H;
  /*! \brief The current Schur form reduction of the valid part of H. */
  SmartPtr<const MatrixEigenDense<ScalarType> > S;
  /*! \brief The current Schur vectors of the valid part of H. */
  SmartPtr<const MatrixEigenDense<ScalarType> > Q;
  BlockKrylovSchurState() : curDim(0), V(_smPtrNull),
                            H(_smPtrNull), S(_smPtrNull),
                            Q(_smPtrNull) {}
};
  
/*!
  \class BlockKrylovSchur

   \brief This class implements the block Krylov-Schur iteration,
   for solving linear eigenvalue problems.

   This method is a block version of the iteration presented by G.W. Stewart 
   in "A Krylov-Schur Algorithm for Large Eigenproblems", 
   SIAM J. Matrix Anal. Appl., Vol 23(2001), No. 3, pp. 601-614.
*/
template <class ScalarType, class MV, class OP>
class BlockKrylovSchur : public EigenSolver<ScalarType,MV,OP> { 
  public:
    //! @name Constructor/Destructor
    //@{ 
    /*! \brief %BlockKrylovSchur constructor with eigenproblem, solver utilities, and parameter list of solver options.
     *
     * This constructor takes pointers required by the eigensolver, in addition
     * to a parameter list of options for the eigensolver. These options include the following:
     *   - "Block Size" - an \c int specifying the block size used by the algorithm. This can also be specified using the setBlockSize() method. Default: 1
     *   - "Num Blocks" - an \c int specifying the maximum number of blocks allocated for the solver basis. Default: 3*problem->getNEV()
     *   - "Step Size"  - an \c int specifying how many iterations are performed between computations of eigenvalues and eigenvectors.\n
     *     \note This parameter is mandatory.
     *   - "Number of Ritz Vectors" - an \c int specifying how many Ritz vectors are computed on calls to getRitzVectors(). Default: 0
     *   - "Print Number of Ritz ValueEigenSolvers" - an \c int specifying how many Ritz values are printed on calls to currentStatus(). Default: "Block Size"
     */
    BlockKrylovSchur(const SmartPtr<EigenProblem<ScalarType,MV,OP> >& problem,
                      const SmartPtr<SortManager<typename NumTraits<ScalarType>::magnitudeType> >& sorter,
                      const SmartPtr<OutputManager<ScalarType> >& printer,
                      const SmartPtr<StatusTest<ScalarType,MV,OP> >& tester,
                      const SmartPtr<OrthoManager<ScalarType,MV> >& ortho,
                      Parameters& params
                   );
    
    //! %BlockKrylovSchur destructor.
    virtual ~BlockKrylovSchur() {}
    //@}


    //! @name Solver methods
    //@{
    /*! \brief This method performs Block Krylov-Schur iterations until the status
     * test indicates the need to stop or an error occurs (in which case, an
     * exception is thrown).
     *
     * iterate() will first determine whether the solver is inintialized; if
     * not, it will call initialize() using default arguments. After
     * initialization, the solver performs Block Krylov-Schur iterations until the
     * status test evaluates as ::_passed, at which point the method returns to
     * the caller. 
     *
     * The Block Krylov-Schur iteration proceeds as follows:
     * -# The operator problem->getOperator() is applied to the newest \c blockSize vectors in the Krylov basis.
     * -# The resulting vectors are orthogonalized against the auxiliary vectors and the previous basis vectors, and made orthonormal.
     * -# The Hessenberg matrix is updated.
     * -# If we have performed \c stepSize iterations since the last update, update the Ritz values and Ritz residuals.
     *
     * The status test is queried at the beginning of the iteration.
     *
     */
    void iterate();

    /*! \brief Initialize the solver to an iterate, providing a Krylov basis and Hessenberg matrix.
     *
     * The %BlockKrylovSchur eigensolver contains a certain amount of state,
     * consisting of the current Krylov basis and the associated Hessenberg matrix.
     *
     * initialize() gives the user the opportunity to manually set these,
     * although this must be done with caution, abiding by the rules given
     * below. All notions of orthogonality and orthonormality are derived from
     * the inner product specified by the orthogonalization manager.
     *
     * \post 
     * isInitialized() == \c true (see post-conditions of isInitialize())
     *
     * The user has the option of specifying any component of the state using
     * initialize(). However, these arguments are assumed to match the
     * post-conditions specified under isInitialized(). Any necessary component of the
     * state not given to initialize() will be generated.
     *
     * \note for any pointer in \c newstate which directly points to the multivectors in 
     * the solver, the data is not copied.
     */
    void initialize(BlockKrylovSchurState<ScalarType,MV> state);

    /*! \brief Initialize the solver with the initial vectors from the eigenproblem
     *  or random data.
     */
    void initialize();

    /*! \brief Indicates whether the solver has been initialized or not.
     *
     * \return bool indicating the state of the solver.
     * \post
     * If isInitialized() == \c true:
     *    - the first getCurSubspaceDim() vectors of V are orthogonal to auxiliary vectors and have orthonormal columns
     *    - the principal Hessenberg submatrix of of H contains the Hessenberg matrix associated with V
     */
    bool isInitialized() const { return initialized_; }

    /*! \brief Get the current state of the eigensolver.
     * 
     * The data is only valid if isInitialized() == \c true. 
     *
     * \returns A BlockKrylovSchurState object containing const pointers to the current
     * solver state.
     */
    BlockKrylovSchurState<ScalarType,MV> getState() const {
      BlockKrylovSchurState<ScalarType,MV> state;
      state.curDim = curDim_;
      state.V = V_;
      state.H = H_;
      state.Q = Q_;
      state.S = schurH_;
      return state;
    }
    //@}

    //! @name Status methods
    //@{
    //! \brief Get the current iteration count.
    int getNumIters() const { return(iter_); }

    //! \brief Reset the iteration count.
    void resetNumIters() { iter_=0; }

    /*! \brief Get the Ritz vectors.
     *
     *  \return A multivector of columns not exceeding the maximum dimension of the subspace
     *  containing the Ritz vectors from the most recent call to computeRitzVectors().
     *
     *  \note To see if the returned Ritz vectors are current, call isRitzVecsCurrent().
     */
    SmartPtr<const MV> getRitzVectors() { return ritzVectors_; }

    /*! \brief Get the Ritz values.
     *
     *  \return A vector of length not exceeding the maximum dimension of the subspace 
     *  containing the Ritz values from the most recent Schur form update.
     *
     *  \note To see if the returned Ritz values are current, call isRitzValsCurrent().
     */
    std::vector<ValueEigenSolver<ScalarType> > getRitzValues() {
      std::vector<ValueEigenSolver<ScalarType> > ret = ritzValues_;
      ret.resize(ritzIndex_.size());
      return ret;
    }

    /*! \brief Get the Ritz index vector.
     *
     *  \return A vector of length not exceeding the maximum dimension of the subspace
     *  containing the index vector for the Ritz values and Ritz vectors, if they are computed.
     */ 
    std::vector<int> getRitzIndex() { return ritzIndex_; }

    /*! \brief Get the current residual norms.
     *
     *  \note Block Krylov-Schur cannot provide this so a zero length vector will be returned.
     */
    std::vector<typename NumTraits<ScalarType>::magnitudeType> getResNorms() {
      std::vector<typename NumTraits<ScalarType>::magnitudeType> ret(0);
      return ret;
    }

    /*! \brief Get the current residual 2-norms
     *
     *  \note Block Krylov-Schur cannot provide this so a zero length vector will be returned.
     */
    std::vector<typename NumTraits<ScalarType>::magnitudeType> getRes2Norms() {
      std::vector<typename NumTraits<ScalarType>::magnitudeType> ret(0);
      return ret;
    }

    /*! \brief Get the current Ritz residual 2-norms
     *
     *  \return A vector of length blockSize containing the 2-norms of the Ritz residuals.
     */
    std::vector<typename NumTraits<ScalarType>::magnitudeType> getRitzRes2Norms() {
      std::vector<typename NumTraits<ScalarType>::magnitudeType> ret = ritzResiduals_;
      ret.resize(ritzIndex_.size());
      return ret;
    }
    //@}

    //! @name Accessor routines
    //@{
    //! Set a new StatusTest for the solver.
    void setStatusTest(SmartPtr<StatusTest<ScalarType,MV,OP> > test);

    //! Get the current StatusTest used by the solver.
    SmartPtr<StatusTest<ScalarType,MV,OP> > getStatusTest() const;

    //! Get a constant reference to the eigenvalue problem.
    const EigenProblem<ScalarType,MV,OP>& getProblem() const { return(*problem_); };

    /*! \brief Set the blocksize and number of blocks to be used by the
     * iterative solver in solving this eigenproblem.
     *  
     *  Changing either the block size or the number of blocks will reset the
     *  solver to an uninitialized state.
     */
    void setSize(int blockSize, int numBlocks);

    //! \brief Set the blocksize. 
    void setBlockSize(int blockSize);

    //! \brief Set the step size. 
    void setStepSize(int stepSize);

    //! \brief Set the number of Ritz vectors to compute.
    void setNumRitzVectors(int numRitzVecs);

    //! \brief Get the step size. 
    int getStepSize() const { return(stepSize_); }

    //! Get the blocksize to be used by the iterative solver in solving this eigenproblem.
    int getBlockSize() const { return(blockSize_); }

    //! \brief Get the number of Ritz vectors to compute.
    int getNumRitzVectors() const { return(numRitzVecs_); }

    /*! \brief Get the dimension of the search subspace used to generate the current eigenvectors and eigenvalues.
     *
     *  \return An integer specifying the rank of the Krylov subspace currently in use by the eigensolver. If isInitialized() == \c false, 
     *  the return is 0.
     */
    int getCurSubspaceDim() const {
      if (!initialized_) return 0;
      return curDim_;
    }

    //! Get the maximum dimension allocated for the search subspace. 
    int getMaxSubspaceDim() const { return (problem_->isHermitian()?blockSize_*numBlocks_:blockSize_*numBlocks_+1); }


    /*! \brief Set the auxiliary vectors for the solver.
     *
     *  Because the current Krylov subspace cannot be assumed
     *  orthogonal to the new auxiliary vectors, a call to setAuxVecs() will
     *  reset the solver to the uninitialized state. This happens only in the
     *  case where the new auxiliary vectors have a combined dimension of 
     *  greater than zero.
     *
     *  In order to preserve the current state, the user will need to extract
     *  it from the solver using getState(), orthogonalize it against the
     *  new auxiliary vectors, and reinitialize using initialize().
     */
    void setAuxVecs(const std::vector<SmartPtr<const MV> >& auxvecs);

    //! Get the auxiliary vectors for the solver.
    std::vector<SmartPtr<const MV> > getAuxVecs() const {return auxVecs_;}
    //@}

    //! @name Output methods
    //@{
    //! This method requests that the solver print out its current status to screen.
    void currentStatus(std::ostream& os);
    //@}

    //! @name Block-Krylov Schur status routines
    //@{
    //! Get the status of the Ritz vectors currently stored in the eigensolver.
    bool isRitzVecsCurrent() const { return ritzVecsCurrent_; }

    //! Get the status of the Ritz values currently stored in the eigensolver.
    bool isRitzValsCurrent() const { return ritzValsCurrent_; }
    
    //! Get the status of the Schur form currently stored in the eigensolver.
    bool isSchurCurrent() const { return schurCurrent_; }
    //@}

    //! @name Block-Krylov Schur compute routines
    //@{
    //! Compute the Ritz vectors using the current Krylov factorization.
    void computeRitzVectors();

    //! Compute the Ritz values using the current Krylov factorization.
    void computeRitzValueEigenSolvers();
    
    //! Compute the Schur form of the projected eigenproblem from the current Krylov factorization.
    void computeSchurForm(const bool sort = true);
    //@}

  private:
    //
    // Convenience typedefs
    //
    typedef MultiVecTraits<ScalarType,MV> MVT;
    typedef OperatorTraits<ScalarType,MV,OP> OPT;
    typedef NumTraits<ScalarType> SCT;
    typedef typename SCT::magnitudeType MagnitudeType;
    typedef typename std::vector<ScalarType>::iterator STiter;
    typedef typename std::vector<MagnitudeType>::iterator MTiter;
    const MagnitudeType MT_ONE;  
    const MagnitudeType MT_ZERO; 
    const MagnitudeType NANVAL;
    const ScalarType ST_ONE;
    const ScalarType ST_ZERO;
    //
    // Internal structs
    //
    /*!
      \struct CheckList
      utility class to store check data
    */
    struct CheckList {
      bool checkV;
      bool checkArn;
      bool checkAux;
      CheckList() : checkV(false), checkArn(false), checkAux(false) {}
    };
    //
    // Internal methods
    //
    std::string accuracyCheck(const CheckList& chk, const std::string& where) const;
    void sortSchurForm(MatrixEigenDense<ScalarType>& H,
                        MatrixEigenDense<ScalarType>& Q,
                        std::vector<int>& order);
    //
    // Classes inputed through constructor that define the eigenproblem to be solved.
    //
    const SmartPtr<EigenProblem<ScalarType,MV,OP> >     problem_;
    const SmartPtr<SortManager<typename NumTraits<ScalarType>::magnitudeType> > sm_;
    const SmartPtr<OutputManager<ScalarType> >          om_;
    SmartPtr<StatusTest<ScalarType,MV,OP> >             tester_;
    const SmartPtr<OrthoManager<ScalarType,MV> >        orthman_;
    //
    // Information obtained from the eigenproblem
    //
    SmartPtr<const OP> Op_;
    //
    // Internal timers
    //
   // SmartPtr<Teuchos::Time> timerOp_, timerSortRitzVal_,
   //                                     timerCompSF_, timerSortSF_,
   //                                     timerCompRitzVec_, timerOrtho_;
    //
    // Counters
    //
    int countApplyOp_;

    //
    // Algorithmic parameters.
    //
    // blockSize_ is the solver block size; it controls the number of eigenvectors that 
    // we compute, the number of residual vectors that we compute, and therefore the number
    // of vectors added to the basis on each iteration.
    int blockSize_;
    // numBlocks_ is the size of the allocated space for the Krylov basis, in blocks.
    int_t numBlocks_; 
    // stepSize_ dictates how many iterations are performed before eigenvectors and eigenvalues
    // are computed again
    int stepSize_;
    
    // 
    // Current solver state
    //
    // initialized_ specifies that the basis vectors have been initialized and the iterate() routine
    // is capable of running; _initialize is controlled  by the initialize() member method
    // For the implications of the state of initialized_, please see documentation for initialize()
    bool initialized_;
    //
    // curDim_ reflects how much of the current basis is valid 
    // NOTE: for Hermitian, 0 <= curDim_ <= blockSize_*numBlocks_
    //   for non-Hermitian, 0 <= curDim_ <= blockSize_*numBlocks_ + 1
    // this also tells us how many of the values in _theta are valid Ritz values
    int curDim_;
    //
    // State Multivecs
    SmartPtr<MV> ritzVectors_, V_;
    int numRitzVecs_;
    //
    // Projected matrices
    // H_: Projected matrix from the Krylov-Schur factorization AV = VH + FB^T
    //
    SmartPtr<MatrixEigenDense<ScalarType> > H_;
    // 
    // Schur form of Projected matrices (these are only updated when the Ritz values/vectors are updated).
    // schurH_: Schur form reduction of H
    // Q_: Schur vectors of H
    SmartPtr<MatrixEigenDense<ScalarType> > schurH_;
    SmartPtr<MatrixEigenDense<ScalarType> > Q_;
    // 
    // Auxiliary vectors
    std::vector<SmartPtr<const MV> > auxVecs_;
    int numAuxVecs_;
    //
    // Number of iterations that have been performed.
    int iter_;
    //
    // State flags
    bool ritzVecsCurrent_, ritzValsCurrent_, schurCurrent_;
    // 
    // Current eigenvalues, residual norms
    std::vector<ValueEigenSolver<ScalarType> > ritzValues_;
    std::vector<MagnitudeType> ritzResiduals_;
    // 
    // Current index vector for Ritz values and vectors
    std::vector<int> ritzIndex_;  // computed by BKS
    std::vector<int> ritzOrder_;  // returned from sort manager
    //
    // Number of Ritz pairs to be printed upon output, if possible
    int numRitzPrint_;

    friend struct internalEigenSolver::SortSchurForm<MatrixEigenDense<ScalarType>, NumTraits<ScalarType>::IsComplex>;
};
//@}

//=============================================================================================
// Constructor
template <class ScalarType, class MV, class OP>
BlockKrylovSchur<ScalarType,MV,OP>::BlockKrylovSchur(
      const SmartPtr<EigenProblem<ScalarType,MV,OP> >& problem,
      const SmartPtr<SortManager<typename NumTraits<ScalarType>::magnitudeType> >& sorter,
      const SmartPtr<OutputManager<ScalarType> >& printer,
      const SmartPtr<StatusTest<ScalarType,MV,OP> >& tester,
      const SmartPtr<OrthoManager<ScalarType,MV> >& ortho,
      Parameters& params
     ) :
  MT_ONE(NumTraits<MagnitudeType>::one()),
  MT_ZERO(NumTraits<MagnitudeType>::zero()),
  NANVAL(NumTraits<MagnitudeType>::nan()),
  ST_ONE(NumTraits<ScalarType>::one()),
  ST_ZERO(NumTraits<ScalarType>::zero()),
  // problem, tools
  problem_(problem), 
  sm_(sorter),
  om_(printer),
  tester_(tester),
  orthman_(ortho),
  // timers, counters
  countApplyOp_(0),
  // internal data
  blockSize_(0),
  numBlocks_(0),
  stepSize_(0),
  initialized_(false),
  curDim_(0),
  numRitzVecs_(0),
  auxVecs_(std::vector<SmartPtr<const MV> >(0)),
  numAuxVecs_(0),
  iter_(0),
  ritzVecsCurrent_(false),
  ritzValsCurrent_(false),
  schurCurrent_(false),
  numRitzPrint_(0)
{     
  internalEigenSolver::testErrorEigenProblem(problem_ == _smPtrNull,
                     "xlifepp::BlockKrylovSchur::constructor: user specified null problem pointer.");
  internalEigenSolver::testErrorEigenProblem(sm_ == _smPtrNull,
                     "xlifepp::BlockKrylovSchur::constructor: user passed null sort manager pointer.");
  internalEigenSolver::testErrorEigenProblem(om_ == _smPtrNull,
                     "xlifepp::BlockKrylovSchur::constructor: user passed null output manager pointer.");
  internalEigenSolver::testErrorEigenProblem(tester_ == _smPtrNull,
                     "xlifepp::BlockKrylovSchur::constructor: user passed null status test pointer.");
  internalEigenSolver::testErrorEigenProblem(orthman_ == _smPtrNull,
                     "xlifepp::BlockKrylovSchur::constructor: user passed null orthogonalization manager pointer.");
  internalEigenSolver::testErrorEigenProblem(problem_->isProblemSet() == false,
                     "xlifepp::BlockKrylovSchur::constructor: user specified problem is not set.");
  internalEigenSolver::testErrorEigenProblem(sorter == _smPtrNull,
                     "xlifepp::BlockKrylovSchur::constructor: user specified null sort manager pointer.");
  internalEigenSolver::testErrorEigenProblem(printer == _smPtrNull,
                     "xlifepp::BlockKrylovSchur::constructor: user specified null output manager pointer.");
  internalEigenSolver::testErrorEigenProblem(tester == _smPtrNull,
                     "xlifepp::BlockKrylovSchur::constructor: user specified null status test pointer.");
  internalEigenSolver::testErrorEigenProblem(ortho == _smPtrNull,
                     "xlifepp::BlockKrylovSchur::constructor: user specified null ortho manager pointer.");

  // Get problem operator
  Op_ = problem_->getOperator();

  // get the step size
  internalEigenSolver::testErrorEigenProblem(!params.contains("Step Size"),
                     "xlifepp::BlockKrylovSchur::constructor: mandatory parameter 'Step Size' is not specified.");
  int_t ss = params.get("Step Size",numBlocks_);
  setStepSize(ss);

  // set the block size and allocate data
  int_t bs = params.get("Block Size", int_t(1));
  int_t nb = params.get("Num Blocks", int_t(3)*problem_->getNEV());
  setSize(bs,nb);

  // get the number of Ritz vectors to compute and allocate data.
  // --> if this parameter is not specified in the parameter list, then it's assumed that no Ritz vectors will be computed.
  int_t numRitzVecs = params.get("Number of Ritz Vectors", int_t(0));
  setNumRitzVectors(numRitzVecs);

  // get the number of Ritz values to print out when currentStatus is called.
  numRitzPrint_ = params.get("Print Number of Ritz ValueEigenSolvers", bs);
}

//=============================================================================================
// Set the block size
// This simply calls setSize(), modifying the block size while retaining the number of blocks.
template <class ScalarType, class MV, class OP>
void BlockKrylovSchur<ScalarType,MV,OP>::setBlockSize (int blockSize) 
{
  setSize(blockSize,numBlocks_);
}

//=============================================================================================
// Set the step size.
template <class ScalarType, class MV, class OP>
void BlockKrylovSchur<ScalarType,MV,OP>::setStepSize (int stepSize)
{
  internalEigenSolver::testErrorEigenProblem(stepSize <= 0,  "xlifepp::BlockKrylovSchur::setStepSize(): new step size must be positive and non-zero.");
  stepSize_ = stepSize;
}

//=============================================================================================
// Set the number of Ritz vectors to compute.
template <class ScalarType, class MV, class OP>
void BlockKrylovSchur<ScalarType,MV,OP>::setNumRitzVectors (int numRitzVecs)
{
  // This routine only allocates space; it doesn't not perform any computation
  // any change in size will invalidate the state of the solver.

  internalEigenSolver::testErrorEigenProblem(numRitzVecs < 0,  "xlifepp::BlockKrylovSchur::setNumRitzVectors(): number of Ritz vectors to compute must be positive.");

  // Check to see if the number of requested Ritz vectors has changed.
  if (numRitzVecs != numRitzVecs_) {
    if (numRitzVecs) {
      ritzVectors_ = _smPtrNull;
      ritzVectors_ = MVT::clone(*V_, numRitzVecs);
    } else {
      ritzVectors_ = _smPtrNull;
    }
    numRitzVecs_ = numRitzVecs;
    ritzVecsCurrent_ = false;
  }      
}

//=============================================================================================
// Set the block size and make necessary adjustments.
template <class ScalarType, class MV, class OP>
void BlockKrylovSchur<ScalarType,MV,OP>::setSize (int blockSize, int numBlocks) 
{
  // This routine only allocates space; it doesn't not perform any computation
  // any change in size will invalidate the state of the solver.

  internalEigenSolver::testErrorEigenProblem(numBlocks <= 0 || blockSize <= 0,  "xlifepp::BlockKrylovSchur::setSize was passed a non-positive argument.");
  internalEigenSolver::testErrorEigenProblem(numBlocks < 3,  "xlifepp::BlockKrylovSchur::setSize(): numBlocks must be at least three.");
  if (blockSize == blockSize_ && numBlocks == numBlocks_) {
    // do nothing
    return;
  }

  blockSize_ = blockSize;
  numBlocks_ = numBlocks;

  SmartPtr<const MV> tmp;
  // grab some Multivector to clone
  // in practice, getInitVec() should always provide this, but it is possible to use a 
  // EigenProblem with nothing in getInitVec() by manually initializing with initialize(); 
  // in case of that strange scenario, we will try to clone from V_; first resort to getInitVec(),
  // because we would like to clear the storage associated with V_ so we have room for the new V_
  if (problem_->getInitVec() != _smPtrNull) {
    tmp = problem_->getInitVec();
  }
  else {
    tmp = V_;
    internalEigenSolver::testErrorEigenProblem(tmp == _smPtrNull,
        "xlifepp::BlockKrylovSchur::setSize(): eigenproblem did not specify initial vectors to clone from.");
  }

  // blockSize*numBlocks dependent
  //
  int newsd;
  if (problem_->isHermitian()) {
    newsd = blockSize_*numBlocks_;
  } else {
    newsd = blockSize_*numBlocks_+1;
  }
  // check that new size is valid
  internalEigenSolver::testErrorEigenProblem(newsd > MVT::getVecLength(*tmp),
      "xlifepp::BlockKrylovSchur::setSize(): maximum basis size is larger than problem dimension.");

  ritzValues_.resize(newsd);
  ritzResiduals_.resize(newsd,MT_ONE);
  ritzOrder_.resize(newsd);
  V_ = _smPtrNull;
  V_ = MVT::clone(*tmp,newsd+blockSize_);
  H_ = SmartPtr<MatrixEigenDense<ScalarType> >(new MatrixEigenDense<ScalarType>(newsd+blockSize_,newsd));
  Q_ = SmartPtr<MatrixEigenDense<ScalarType> >(new MatrixEigenDense<ScalarType>(newsd,newsd));

  initialized_ = false;
  curDim_ = 0;
}

// Set the auxiliary vectors
template <class ScalarType, class MV, class OP>
void BlockKrylovSchur<ScalarType,MV,OP>::setAuxVecs(const std::vector<SmartPtr<const MV> >& auxvecs) {
  typedef typename std::vector<SmartPtr<const MV> >::iterator tarcpmv;

  // set new auxiliary vectors
  auxVecs_ = auxvecs;
  
  if (om_->isVerbosity(_debugEigen)) {
    // Check almost everything here
    CheckList chk;
    chk.checkAux = true;
    om_->print(_debugEigen, accuracyCheck(chk, ": in setAuxVecs()"));
  }

  numAuxVecs_ = 0;
  for (tarcpmv i=auxVecs_.begin(); i != auxVecs_.end(); i++) {
    numAuxVecs_ += MVT::getNumberVecs(**i);
  }
  
  // If the solver has been initialized, X and P are not necessarily orthogonal to new auxiliary vectors
  if (numAuxVecs_ > 0 && initialized_) {
    initialized_ = false;
  }
}

/* Initialize the state of the solver
 * 
 * POST-CONDITIONS:
 *
 * V_ is orthonormal, orthogonal to auxVecs_, for first curDim_ vectors
 *
 */
template <class ScalarType, class MV, class OP>
void BlockKrylovSchur<ScalarType,MV,OP>::initialize(BlockKrylovSchurState<ScalarType,MV> newstate)
{
  // NOTE: memory has been allocated by setBlockSize(). Use setBlock below; do not clone

  std::vector<int> bsind(blockSize_);
  for (int i=0; i<blockSize_; i++) bsind[i] = i;

  // in BlockKrylovSchur, V and H are required.  
  // if either doesn't exist, then we will start with the initial vector.
  //
  // inconsistent multivectors widths and lengths will not be tolerated, and
  // will be treated with exceptions.
  //
  std::string errstr("xlifepp::BlockKrylovSchur::initialize(): specified multivectors must have a consistent length and width.");

  // set up V,H: if the user doesn't specify both of these these, 
  // we will start over with the initial vector.
  if (newstate.V != _smPtrNull && newstate.H != _smPtrNull) {

    // initialize V_,H_, and curDim_

    internalEigenSolver::testErrorEigenProblem(MVT::getVecLength(*newstate.V) != MVT::getVecLength(*V_),
                         errstr);
    if (newstate.V != V_) {
      internalEigenSolver::testErrorEigenProblem(MVT::getNumberVecs(*newstate.V) < blockSize_,
           errstr);
      internalEigenSolver::testErrorEigenProblem(MVT::getNumberVecs(*newstate.V) > getMaxSubspaceDim(),
           errstr);
    }
    internalEigenSolver::testErrorEigenProblem(newstate.curDim > getMaxSubspaceDim(),
                         errstr);

    curDim_ = newstate.curDim;
    int lclDim = MVT::getNumberVecs(*newstate.V);

    // check size of H
    internalEigenSolver::testErrorEigenProblem(newstate.H->numOfRows() < curDim_ || newstate.H->numOfCols() < curDim_,  errstr);
    
    if (curDim_ == 0 && lclDim > blockSize_) {
      om_->stream(_warningsEigen) << "xlifepp::BlockKrylovSchur::initialize(): the solver was initialized with a kernel of " << lclDim << std::endl
                                << "The block size however is only " << blockSize_ << std::endl
                                << "The last " << lclDim - blockSize_ << " vectors of the kernel will be overwritten on the first call to iterate()." << std::endl;
    }


    // copy basis vectors from newstate into V
    if (newstate.V != V_) {
      std::vector<int> nevind(lclDim);
      for (int i=0; i<lclDim; i++) nevind[i] = i;
      MVT::setBlock(*newstate.V,nevind,*V_);
    }

    // put data into H_, make sure old information is not still hanging around.
    if (newstate.H != H_) {
      H_->putScalar(ST_ZERO);
      //MatrixEigenDense<ScalarType> newH(Teuchos::View,*newstate.H,curDim_+blockSize_,curDim_);
      MatrixEigenDense<ScalarType> newH(*newstate.H,0,0,curDim_+blockSize_,curDim_);
      H_->replace(newH,0,0,curDim_+blockSize_,curDim_);
    }

  }
  else {
    // user did not specify a basis V
    // get vectors from problem or generate something, projectAndNormalize, call initialize() recursively
    SmartPtr<const MV> ivec = problem_->getInitVec();
    internalEigenSolver::testErrorEigenProblem(ivec == _smPtrNull,
                       "xlifepp::BlockKrylovSchur::initialize(): eigenproblem did not specify initial vectors to clone from.");

    int lclDim = MVT::getNumberVecs(*ivec);
    bool userand = false;
    if (lclDim < blockSize_) {
      // we need at least blockSize_ vectors
      // use a random multivec
      userand = true;
    }
            
    if (userand) {
      // make an index
      std::vector<int> dimind2(lclDim);
      for (int i=0; i<lclDim; i++) { dimind2[i] = i; }

      // alloc newV as a view of the first block of V
      SmartPtr<MV> newV1 = MVT::cloneViewNonConst(*V_,dimind2);

      // copy the initial vectors into the first lclDim vectors of V
      MVT::setBlock(*ivec,dimind2,*newV1);

      // resize / reinitialize the index vector        
      dimind2.resize(blockSize_-lclDim);
      for (int i=0; i<blockSize_-lclDim; i++) { dimind2[i] = lclDim + i; }

      // initialize the rest of the vectors with random vectors
      SmartPtr<MV> newV2 = MVT::cloneViewNonConst(*V_,dimind2);
      MVT::mvRandom(*newV2);
    }
    else {
      // alloc newV as a view of the first block of V
      SmartPtr<MV> newV1 = MVT::cloneViewNonConst(*V_,bsind);
     
      // get a view of the first block of initial vectors
      SmartPtr<const MV> ivecV = MVT::cloneView(*ivec,bsind);

      // assign ivec to first part of newV
      MVT::setBlock(*ivecV,bsind,*newV1);
    }

    // get pointer into first block of V
    SmartPtr<MV> newV = MVT::cloneViewNonConst(*V_,bsind);

    // remove auxVecs from newV and normalize newV
    if (auxVecs_.size() > 0) {
      std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > dummy;
      int rank = orthman_->projectAndNormalize(*newV,auxVecs_);
      internalEigenSolver::testErrorEigenProblem(rank != blockSize_,
                          "xlifepp::BlockKrylovSchur::initialize(): couldn't generate initial basis of full rank.");
    }
    else {
      int rank = orthman_->normalize(*newV);
      internalEigenSolver::testErrorEigenProblem(rank != blockSize_,
                          "xlifepp::BlockKrylovSchur::initialize(): couldn't generate initial basis of full rank.");
    }

    // set curDim
    curDim_ = 0;

    // clear pointer
    newV = _smPtrNull;
  }

  // The Ritz vectors/values and Schur form are no longer current.
  ritzVecsCurrent_ = false;
  ritzValsCurrent_ = false;
  schurCurrent_ = false;

  // the solver is initialized
  initialized_ = true;

  if (om_->isVerbosity(_debugEigen)) {
    // Check almost everything here
    CheckList chk;
    chk.checkV = true;
    chk.checkArn = true;
    chk.checkAux = true;
    om_->print(_debugEigen, accuracyCheck(chk, ": after initialize()"));
  }

  // Print information on current status
  if (om_->isVerbosity(_debugEigen)) {
    currentStatus(om_->stream(_debugEigen));
  }
  else if (om_->isVerbosity(_iterationDetailsEigen)) {
    currentStatus(om_->stream(_iterationDetailsEigen));
  }
}

//=============================================================================================
// initialize the solver with default state
template <class ScalarType, class MV, class OP>
void BlockKrylovSchur<ScalarType,MV,OP>::initialize()
{
  BlockKrylovSchurState<ScalarType,MV> empty;
  initialize(empty);
}

//=============================================================================================
// Perform BlockKrylovSchur iterations until the StatusTest tells us to stop.
template <class ScalarType, class MV, class OP>
void BlockKrylovSchur<ScalarType,MV,OP>::iterate()
{
  //
  // Allocate/initialize data structures
  //
  if (initialized_ == false) {
    initialize();
  }

  // Compute the current search dimension. 
  // If the problem is non-Hermitian and the blocksize is one, let the solver use the extra vector.
  int searchDim = blockSize_*numBlocks_;
  if (problem_->isHermitian() == false) {
    searchDim++;
  } 

  // iterate until the status test tells us to stop.
  //
  // also break if our basis is full
  //
  while (tester_->checkStatus(this) != _passed && curDim_+blockSize_ <= searchDim) {

    iter_++;

    // F can be found at the curDim_ block, but the next block is at curDim_ + blockSize_.
    int lclDim = curDim_ + blockSize_; 

    // Get the current part of the basis.
    std::vector<int> curind(blockSize_);
    for (int i=0; i<blockSize_; i++) { curind[i] = lclDim + i; }
    SmartPtr<MV> Vnext = MVT::cloneViewNonConst(*V_,curind);

    // Get a view of the previous vectors
    // this is used for orthogonalization and for computing V^H K H
    for (int i=0; i<blockSize_; i++) { curind[i] = curDim_ + i; }
    SmartPtr<const MV> Vprev = MVT::cloneView(*V_,curind);
    // om_->stream(_debugEigen) << "Vprev: " << std::endl;
    // MVT::mvPrint(*Vprev,om_->stream(_debugEigen));

    // Compute the next vector in the Krylov basis:  Vnext = Op*Vprev
    {
      OPT::apply(*Op_,*Vprev,*Vnext);
      countApplyOp_ += blockSize_;
    }
    // om_->stream(_debugEigen) << "Vnext: " << std::endl;
    // MVT::mvPrint(*Vnext,om_->stream(_debugEigen));

    Vprev = _smPtrNull;
    
    // Remove all previous Krylov-Schur basis vectors and auxVecs from Vnext
    {
      // Get a view of all the previous vectors
      std::vector<int> prevind(lclDim);
      for (int i=0; i<lclDim; i++) { prevind[i] = i; }
      Vprev = MVT::cloneView(*V_,prevind);
      std::vector<SmartPtr<const MV> > AVprev(1, Vprev);
      
      // Get a view of the part of the Hessenberg matrix needed to hold the ortho coeffs.
      SmartPtr<MatrixEigenDense<ScalarType> >
      // subH = Teuchos::rcp(new MatrixEigenDense<ScalarType> (Teuchos::View,*H_,lclDim,blockSize_,0,curDim_));
      subH = smartPtr(new MatrixEigenDense<ScalarType> (*H_,0,curDim_,lclDim,blockSize_));

      std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > AsubH;
      AsubH.push_back(subH); //AsubH.append(subH);
      
      // Add the auxiliary vectors to the current basis vectors if any exist
      if (auxVecs_.size() > 0) {
        for (dimen_t i=0; i<auxVecs_.size(); i++) {
          AVprev.push_back(auxVecs_[i]); //AVprev.append(auxVecs_[i]);
          AsubH.push_back(_smPtrNull); //AsubH.append(_smPtrNull);
        }
      }
      
      // Get a view of the part of the Hessenberg matrix needed to hold the norm coeffs.
//         om_->stream(_debugEigen) << "V before ortho: " << std::endl;
//         MVT::mvPrint(*Vprev,om_->stream(_debugEigen));
      SmartPtr<MatrixEigenDense<ScalarType> >
         // subR = Teuchos::rcp(new MatrixEigenDense<ScalarType>
         //                      (Teuchos::View,*H_,blockSize_,blockSize_,lclDim,curDim_));
        subR = smartPtr(new MatrixEigenDense<ScalarType>
                           (*H_,lclDim,curDim_,blockSize_,blockSize_));

      int rank = orthman_->projectAndNormalize(*Vnext,AVprev,AsubH,subR);
      // om_->stream(_debugEigen) << "Vnext after ortho: " << std::endl;
      // MVT::mvPrint(*Vnext,om_->stream(_debugEigen));

      internalEigenSolver::testErrorEigenProblem(rank != blockSize_,
                         "xlifepp::BlockKrylovSchur::iterate(): couldn't generate basis of full rank.");
      H_->replace(*subH,0,curDim_,lclDim,blockSize_);
      H_->replace(*subR,lclDim,curDim_,blockSize_,blockSize_);
       // om_->stream(_debugEigen) << "subH: " << std::endl << *subH << std::endl;
       // om_->stream(_debugEigen) << "subR: " << std::endl << *subR << std::endl;
       // om_->stream(_debugEigen) << "H:    " << std::endl << *H_ << std::endl;

    }
    //
    // V has been extended, and H has been extended. 
    //
    // Update basis dim and release all pointers.
    Vnext = _smPtrNull;
    curDim_ += blockSize_;
    // The Ritz vectors/values and Schur form are no longer current.
    ritzVecsCurrent_ = false;
    ritzValsCurrent_ = false;
    schurCurrent_ = false;
    //
    // Update Ritz values and residuals if needed
    if (!(iter_%stepSize_)) {
      computeRitzValueEigenSolvers();
    }
    
    // When required, monitor some orthogonalities
    if (om_->isVerbosity(_debugEigen)) {
      // Check almost everything here
      CheckList chk;
      chk.checkV = true;
      chk.checkArn = true;
      om_->print(_debugEigen, accuracyCheck(chk, ": after local update"));
    }
    else if (om_->isVerbosity(_orthoDetailsEigen)) {
      CheckList chk;
      chk.checkV = true;
      om_->print(_orthoDetailsEigen, accuracyCheck(chk, ": after local update"));
    }
    
    // Print information on current iteration
    if (om_->isVerbosity(_debugEigen)) {
      currentStatus(om_->stream(_debugEigen));
    }
    else if (om_->isVerbosity(_iterationDetailsEigen)) {
      currentStatus(om_->stream(_iterationDetailsEigen));
    }
    
  } // end while (statusTest == false)
  
} // end of iterate()

//=============================================================================================
// Check accuracy, orthogonality, and other debugging stuff
// 
// bools specify which tests we want to run (instead of running more than we actually care about)
//
// checkV: V orthonormal
//          orthogonal to auxvecs
// checkAux: check that auxiliary vectors are actually orthonormal
//
// checkArn: check the Arnoldi factorization
//
// NOTE:  This method needs to check the current dimension of the subspace, since it is possible to
//        call this method when curDim_ = 0 (after initialization).
template <class ScalarType, class MV, class OP>
std::string BlockKrylovSchur<ScalarType,MV,OP>::accuracyCheck(const CheckList& chk, const std::string& where) const
{
  std::stringstream os;
  os.precision(2);
  os.setf(std::ios::scientific, std::ios::floatfield);
  MagnitudeType tmp;

  os << " _debugEigenging checks: iteration " << iter_ << where << std::endl;

  // index vectors for V and F
  std::vector<int> lclind(curDim_);
  for (int i=0; i<curDim_; i++) lclind[i] = i;
  std::vector<int> bsind(blockSize_);
  for (int i=0; i<blockSize_; i++) { bsind[i] = curDim_ + i; }
  
  SmartPtr<const MV> lclV,lclF;
  SmartPtr<MV> lclAV;
  if (curDim_)
    lclV = MVT::cloneView(*V_,lclind);
  lclF = MVT::cloneView(*V_,bsind);

  if (chk.checkV) {
    if (curDim_) {
      tmp = orthman_->orthonormError(*lclV);
      os << " >> Error in V^H M V == I  : " << tmp << std::endl;
    }
    tmp = orthman_->orthonormError(*lclF);
    os << " >> Error in F^H M F == I  : " << tmp << std::endl;
    if (curDim_) {
      tmp = orthman_->orthogError(*lclV,*lclF);
      os << " >> Error in V^H M F == 0  : " << tmp << std::endl;
    }
    for (dimen_t i=0; i<auxVecs_.size(); i++) {
      if (curDim_) {
        tmp = orthman_->orthogError(*lclV,*auxVecs_[i]);
        os << " >> Error in V^H M Aux[" << i << "] == 0 : " << tmp << std::endl;
      }
      tmp = orthman_->orthogError(*lclF,*auxVecs_[i]);
      os << " >> Error in F^H M Aux[" << i << "] == 0 : " << tmp << std::endl;
    }
  }
  
  if (chk.checkArn) {

    if (curDim_) {
      // Compute AV      
      lclAV = MVT::clone(*V_,curDim_);
      {
        OPT::apply(*Op_,*lclV,*lclAV);
      }
      
      // Compute AV - VH
      //**MatrixEigenDense<ScalarType>subH(Teuchos::View,*H_,curDim_,curDim_);
      MatrixEigenDense<ScalarType> subH(*H_,0,0,curDim_,curDim_);
      MVT::mvTimesMatAddMv(-ST_ONE, *lclV, subH, ST_ONE, *lclAV);
      
      // Compute FB_k^T - (AV-VH)
       // MatrixEigenDense<ScalarType> curB(Teuchos::View,*H_,
       //                                                 blockSize_,curDim_, curDim_);
      MatrixEigenDense<ScalarType> curB(*H_, curDim_, 0, blockSize_,curDim_);
      MVT::mvTimesMatAddMv(-ST_ONE, *lclF, curB, ST_ONE, *lclAV);

      // Compute || FE_k^T - (AV-VH) ||
      std::vector<MagnitudeType> arnNorms(curDim_);
      orthman_->norm(*lclAV, arnNorms);
      
      for (int i=0; i<curDim_; i++) {        
      os << " >> Error in Krylov-Schur factorization (R = AV-VS-FB^H), ||R[" << i << "]|| : " << arnNorms[i] << std::endl;
      }
    }
  }

  if (chk.checkAux) {
    for (dimen_t i=0; i<auxVecs_.size(); i++) {
      tmp = orthman_->orthonormError(*auxVecs_[i]);
      os << " >> Error in Aux[" << i << "]^H M Aux[" << i << "] == I: " << tmp << std::endl;
      for (dimen_t j=i+1; j<auxVecs_.size(); j++) {
        tmp = orthman_->orthogError(*auxVecs_[i],*auxVecs_[j]);
        os << " >> Error in Aux[" << i << "]^H M Aux[" << j << "] == 0 : " << tmp << std::endl;
      }
    }
  }

  os << std::endl;

  return os.str();
}

//=============================================================================================
/* Get the current approximate eigenvalues, i.e. Ritz values.
 * 
 * POST-CONDITIONS:
 *
 * ritzValues_ contains Ritz w.r.t. V, H
 * Q_ contains the Schur vectors w.r.t. H
 * schurH_ contains the Schur matrix w.r.t. H
 * ritzOrder_ contains the current ordering from sort manager
 */

template <class ScalarType, class MV, class OP>  
void BlockKrylovSchur<ScalarType,MV,OP>::computeRitzValueEigenSolvers()
{
  // Can only call this if the solver is initialized
  if (initialized_) {

    // This just updates the Ritz values and residuals.
    // --> ritzValsCurrent_ will be set to 'true' by this method.
    if (!ritzValsCurrent_) {
      // Compute the current Ritz values, through computing the Schur form
      //   without updating the current projection matrix or sorting the Schur form.
      computeSchurForm(false);
    }
  }
}

//=============================================================================================
/* Get the current approximate eigenvectors, i.e. Ritz vectors.
 * 
 * POST-CONDITIONS:
 *
 * ritzValues_ contains Ritz w.r.t. V, H
 * ritzVectors_ is first blockSize_ Ritz vectors w.r.t. V, H
 * Q_ contains the Schur vectors w.r.t. H
 * schurH_ contains the Schur matrix w.r.t. H
 * ritzOrder_ contains the current ordering from sort manager
 */

template <class ScalarType, class MV, class OP>  
void BlockKrylovSchur<ScalarType,MV,OP>::computeRitzVectors()
{
  internalEigenSolver::testErrorEigenProblem(numRitzVecs_==0,
                     "xlifepp::BlockKrylovSchur::computeRitzVectors(): no Ritz vectors were required from this solver.");

  internalEigenSolver::testErrorEigenProblem(curDim_ < numRitzVecs_,
                     "xlifepp::BlockKrylovSchur::computeRitzVectors(): the current subspace is not large enough to compute the number of requested Ritz vectors.");


  // Check to see if the current subspace dimension is non-trivial and the solver is initialized
  if (curDim_ && initialized_) {

    // Check to see if the Ritz vectors are current.
    if (!ritzVecsCurrent_) {
      
      // Check to see if the Schur factorization of H (schurH_, Q) is current and sorted.
      if (!schurCurrent_) {
        // Compute the Schur factorization of the current H, which will not directly change H,
        // the factorization will be sorted and placed in (schurH_, Q)
        computeSchurForm(true);
      }
      
      // After the Schur form is computed, then the Ritz values are current.
      // Thus, I can check the Ritz index vector to see if I have enough space for the Ritz vectors requested.
      internalEigenSolver::testErrorEigenProblem(ritzIndex_[numRitzVecs_-1]==1,
                         "xlifepp::BlockKrylovSchur::computeRitzVectors(): the number of required Ritz vectors splits a complex conjugate pair.");

      // Compute the Ritz vectors.
      // --> For a Hermitian problem this is simply the current basis times the first numRitzVecs_ Schur vectors
      //     
      // --> For a non-Hermitian problem, this involves solving the projected eigenproblem, then
      //     placing the product of the current basis times the first numRitzVecs_ Schur vectors times the
      //     eigenvectors of interest into the Ritz vectors.

      // Get a view of the current Krylov-Schur basis vectors and Schur vectors
      std::vector<int> curind(curDim_);
      for (int i=0; i<curDim_; i++) { curind[i] = i; }
      SmartPtr<const MV> Vtemp = MVT::cloneView(*V_, curind);
      if (problem_->isHermitian()) {
        // Get a view into the current Schur vectors
        // MatrixEigenDense<ScalarType> subQ(Teuchos::View, *Q_, curDim_, numRitzVecs_);
        MatrixEigenDense<ScalarType> subQ(*Q_, 0, 0, curDim_, numRitzVecs_);

        // Compute the current Ritz vectors      
        MVT::mvTimesMatAddMv(ST_ONE, *Vtemp, subQ, ST_ZERO, *ritzVectors_);
        
      } else {

        // Get a view into the current Schur vectors.
        // MatrixEigenDense<ScalarType>subQ(Teuchos::View, *Q_, curDim_, curDim_);
        MatrixEigenDense<ScalarType> subQ(*Q_, 0, 0, curDim_, curDim_);
        
        // Get a set of work vectors to hold the current Ritz vectors.
        // SmartPtr<MV> tmpritzVectors_ = MVT::clone(*V_, curDim_);
        std::vector<int> curindDim(curDim_);
        for (int i=0; i<curDim_; i++) { curindDim[i] = i; }
        SmartPtr<MV> tmpritzVectors_ = MVT::cloneCopy(*V_, curindDim);

        // Compute the current Krylov-Schur vectors.
        // MVT::mvTimesMatAddMv(ST_ONE, *Vtemp, subQ, ST_ZERO, *tmpritzVectors_);

        //  Now compute the eigenvectors of the Schur form
        //  Reset the dense matrix and compute the eigenvalues of the Schur form.
        //
        // Allocate the work space. This space will be used below for calls to:
        // * TREVC (requires 3*N for real, 2*N for complex) 

        // MatrixEigenDense<ScalarType>copyQ(Teuchos::Copy, *Q_, curDim_, curDim_);
        MatrixEigenDense<ScalarType> matT(*schurH_, 0, 0, curDim_, curDim_);
        real_t matNorm = matT.normFrobenius();
        MatrixEigenDense<ScalarType> S(curDim_, curDim_);

         // lapack.TREVC(side, curDim_, schurH_->values(), schurH_->stride(), vl, ldvl,
         //               copyQ.values(), copyQ.stride(), curDim_, &mm, &work[0], &rwork[0], &info);
        internalEigenSolver::EigenVectorComputation<MatrixEigenDense<ScalarType>, NumTraits<ScalarType>::IsComplex>::run(matNorm, matT, subQ, S);

        // Get a view into the eigenvectors of the Schur form
        // MatrixEigenDense<ScalarType> subCopyQ(Teuchos::View, copyQ, curDim_, numRitzVecs_);
        MatrixEigenDense<ScalarType> subCopyQ(S, 0, 0, curDim_, numRitzVecs_);
        
        // Convert back to Ritz vectors of the operator.
        curind.resize(numRitzVecs_);  // This is already initialized above
        SmartPtr<MV> view_ritzVectors = MVT::cloneViewNonConst(*ritzVectors_, curind);
        MVT::mvTimesMatAddMv(ST_ONE, *tmpritzVectors_, subCopyQ, ST_ZERO, *view_ritzVectors);

        // Compute the norm of the new Ritz vectors
        std::vector<MagnitudeType> ritzNrm(numRitzVecs_);
        MVT::mvNorm(*view_ritzVectors, ritzNrm);

        // Release memory used to compute Ritz vectors before scaling the current vectors.
        tmpritzVectors_ = _smPtrNull;
        view_ritzVectors = _smPtrNull;
        
        // Scale the Ritz vectors to have Euclidean norm.
        ScalarType ritzScale = ST_ONE;
        for (int i=0; i<numRitzVecs_; i++) {
          
          // If this is a conjugate pair then normalize by the real and imaginary parts.
          if (ritzIndex_[i] == 1) {
            // ritzScale = ST_ONE/lapack_mag.LAPY2(ritzNrm[i],ritzNrm[i+1]);
            ritzScale = ST_ONE/(NumTraits<real_t>::lapy2(ritzNrm[i],ritzNrm[i+1]));
            std::vector<int> newind(2);
            newind[0] = i; newind[1] = i+1;
            tmpritzVectors_ = MVT::cloneCopy(*ritzVectors_, newind);
            view_ritzVectors = MVT::cloneViewNonConst(*ritzVectors_, newind);
            MVT::mvAddMv(ritzScale, *tmpritzVectors_, ST_ZERO, *tmpritzVectors_, *view_ritzVectors);

            // Increment counter for imaginary part
            i++;
          } else {

            // This is a real Ritz value, normalize the vector
            std::vector<int> newind(1);
            newind[0] = i;
            tmpritzVectors_ = MVT::cloneCopy(*ritzVectors_, newind);
            view_ritzVectors = MVT::cloneViewNonConst(*ritzVectors_, newind);
            MVT::mvAddMv(ST_ONE/ritzNrm[i], *tmpritzVectors_, ST_ZERO, *tmpritzVectors_, *view_ritzVectors);
          }              
        }
        
      } // if (problem_->isHermitian()) 
      
      // The current Ritz vectors have been computed.
      ritzVecsCurrent_ = true;
      
    } // if (!ritzVecsCurrent_)      
  } // if (curDim_)    
} // computeRitzVectors()

//=============================================================================================
// Set a new StatusTest for the solver.
template <class ScalarType, class MV, class OP>
void BlockKrylovSchur<ScalarType,MV,OP>::setStatusTest(SmartPtr<StatusTest<ScalarType,MV,OP> > test) {
  internalEigenSolver::testErrorEigenProblem(test == _smPtrNull,
      "xlifepp::BlockKrylovSchur::setStatusTest() was passed a null StatusTest.");
  tester_ = test;
}

//=============================================================================================
// Get the current StatusTest used by the solver.
template <class ScalarType, class MV, class OP>
SmartPtr<StatusTest<ScalarType,MV,OP> > BlockKrylovSchur<ScalarType,MV,OP>::getStatusTest() const {
  return tester_;
}

//=============================================================================================
/* Get the current approximate eigenvalues, i.e. Ritz values.
 * 
 * POST-CONDITIONS:
 *
 * ritzValues_ contains Ritz w.r.t. V, H
 * Q_ contains the Schur vectors w.r.t. H
 * schurH_ contains the Schur matrix w.r.t. H
 * ritzOrder_ contains the current ordering from sort manager
 * schurCurrent_ = true if sort = true; i.e. the Schur form is sorted according to the index
 *  vector returned by the sort manager.
 */
template <class ScalarType, class MV, class OP>
void BlockKrylovSchur<ScalarType,MV,OP>::computeSchurForm(const bool sort)
{
  // Check to see if the dimension of the factorization is greater than zero.
  if (curDim_) {

    // Check to see if the Schur factorization is current.
    if (!schurCurrent_) {
      
      // Check to see if the Ritz values are current
      // --> If they are then the Schur factorization is current but not sorted.
      if (!ritzValsCurrent_) {
        // Get a view into Q, the storage for H's Schur vectors.
        // MatrixEigenDense<ScalarType> subQ(Teuchos::View, *Q_, curDim_, curDim_);
        MatrixEigenDense<ScalarType> subQ(*Q_, 0, 0, curDim_, curDim_);
        
        // Get a copy of H to compute/sort the Schur form.
        // schurH_ = Teuchos::rcp(new MatrixEigenDense<ScalarType>(Teuchos::Copy, *H_, curDim_, curDim_));
        schurH_ = SmartPtr<MatrixEigenDense<ScalarType> >(new MatrixEigenDense<ScalarType>(*H_, 0, 0, curDim_, curDim_));
        //
        //---------------------------------------------------
        // Compute the Schur factorization of subH
        // ---> Use driver GEES to first reduce to upper Hessenberg 
        //         form and then compute Schur form, outputting Ritz values
        //---------------------------------------------------
        //
        // Allocate the work space. This space will be used below for calls to:
        // * GEES  (requires 3*N for real, 2*N for complex)
        // * TREVC (requires 3*N for real, 2*N for complex) 
        // * TREXC (requires N for real, none for complex)
        // Furthermore, GEES requires a real array of length curDim_ (for complex datatypes)
        //
        std::vector<MagnitudeType> tmp_rRitzValueEigenSolvers(curDim_);
        std::vector<MagnitudeType> tmp_iRitzValueEigenSolvers(curDim_);

//          char jobvs = 'V';
//          lapack.GEES(jobvs,curDim_, schurH_->values(), schurH_->stride(), &sdim, &tmp_rRitzValueEigenSolvers[0],
//                       &tmp_iRitzValueEigenSolvers[0], subQ.values(), subQ.stride(), &work[0], lwork,
//                      &rwork[0], &bwork[0], &info);
        
//          internalEigenSolver::testErrorEigenProblem(info != 0,
//                             "xlifepp::BlockKrylovSchur::computeSchurForm(): GEES(n==" << curDim_ << ") returned info " << info << " != 0.");
        //
        //---------------------------------------------------
        // Use the Krylov-Schur factorization to compute the current Ritz residuals 
        // for ALL the eigenvalues estimates (Ritz values)
        //           || Ax - x\theta || = || U_m+1*B_m+1^H*Q*s || 
        //                              = || B_m+1^H*Q*s ||
        //
        // where U_m+1 is the current Krylov-Schur basis, Q are the Schur vectors, and x = U_m+1*Q*s
        // NOTE: This means that s = e_i if the problem is hermitian, else the eigenvectors
        //       of the Schur form need to be computed.
        //
        // First compute H_{m+1,m}*B_m^T, then determine what 's' is.
        //---------------------------------------------------
        //
        // Get current B_m+1
//          MatrixEigenDense<ScalarType>curB(Teuchos::View, *H_,
//                                                          blockSize_, curDim_, curDim_);
        MatrixEigenDense<ScalarType> curB(*H_, curDim_, 0,
                                                        blockSize_, curDim_);
        //
        // Compute B_m+1^H*Q
        MatrixEigenDense<ScalarType> subB(blockSize_, curDim_);
//          blas.GEMM(Teuchos::NO_TRANS, Teuchos::NO_TRANS, blockSize_, curDim_, curDim_, ST_ONE,
//                     curB.values(), curB.stride(), subQ.values(), subQ.stride(),
//                     ST_ZERO, subB.values(), subB.stride());

        internalEigenSolver::ComputeSchurForm<MatrixEigenDense<ScalarType>, NumTraits<ScalarType>::IsComplex> schur;
        schur.run(*schurH_);
        subQ = schur.matrixU();
        MatrixEigenDense<ScalarType> matT = schur.matrixT();
        Q_->replace(subQ, 0, 0, curDim_, curDim_);
        schurH_->replace(matT,0,0,curDim_,curDim_);
        real_t matNorm = matT.normFrobenius();
        internalEigenSolver::EigenValueComputation<MatrixEigenDense<ScalarType>, NumTraits<ScalarType>::IsComplex>::run(matT,tmp_rRitzValueEigenSolvers,tmp_iRitzValueEigenSolvers);

        //
        // Determine what 's' is and compute Ritz residuals.
        //
        //**ScalarType* b_ptr = subB.values();
        if (problem_->isHermitian()) {
          //
          // 's' is the i-th canonical basis vector.
          //
          multMatMat(curB, subQ, subB);
          for (int i=0; i<curDim_ ; i++) {
            //**ritzResiduals_[i] = blas.NRM2(blockSize_, b_ptr + i*blockSize_, 1);
            ritzResiduals_[i] = subB.columnVector(i).norm2();
          }
        } else {
          //
          //  Compute S: the eigenvectors of the block upper triangular, Schur matrix.
          //
          MatrixEigenDense<ScalarType> S(curDim_, curDim_);
//            lapack.TREVC(side, curDim_, schurH_->values(), schurH_->stride(), vl, ldvl,
//                          S.values(), S.stride(), curDim_, &mm, &work[0], &rwork[0], &info);
          internalEigenSolver::EigenVectorComputation<MatrixEigenDense<ScalarType>, NumTraits<ScalarType>::IsComplex>::run(matNorm, matT, subQ, S);

          //
          // Scale the eigenvectors so that their Euclidean norms are all one.
          //
          //HelperTraits<typename NumTraits<ScalarType>::ComplexScalar>::scaleRitzVectors(tmp_iRitzValueEigenSolvers, &S);

          //
          // Compute ritzRes = *B_m+1^H*Q*S where the i-th column of S is 's' for the i-th Ritz-value
          //
          MatrixEigenDense<ScalarType> ritzRes(blockSize_, curDim_);
          multMatMat(curB, S, ritzRes);
//            blas.GEMM(Teuchos::NO_TRANS, Teuchos::NO_TRANS, blockSize_, curDim_, curDim_, ST_ONE,
//                       subB.values(), subB.stride(), S.values(), S.stride(),
//                       ST_ZERO, ritzRes.values(), ritzRes.stride());

          /* TO DO:  There's be an incorrect assumption made in the computation of the Ritz residuals.
                     This assumption is that the next vector in the Krylov subspace is Euclidean orthonormal.
                     It may not be normalized using Euclidean norm. */

//            SmartPtr<MV> ritzResVecs = MVT::clone(*V_, curDim_);
//            std::vector<int> curind(blockSize_);
//            for (int i=0; i<blockSize_; i++) { curind[i] = curDim_ + i; }
//            SmartPtr<const MV> Vtemp = MVT::cloneView(*V_,curind);
//
//            MVT::mvTimesMatAddMv(ST_ONE, *Vtemp, ritzRes, ST_ZERO, *ritzResVecs);
//            std::vector<MagnitudeType> ritzResNrms(curDim_);
//            //orthman_->norm(*ritzResVecs, ritzResNrms);
//            MVT::mvNorm(*ritzResVecs, ritzResNrms);
//            int i = 0;
//            while(i < curDim_) {
//              //if (tmp_ritzValueEigenSolvers[curDim_+i] != MT_ZERO) {
//              if (tmp_iRitzValueEigenSolvers[curDim_+i] != MT_ZERO) {
//                ritzResiduals_[i] = lapack_mag.LAPY2(ritzResNrms[i], ritzResNrms[i+1]);
//                ritzResiduals_[i+1] = ritzResiduals_[i];
//                i = i+2;
//              } else {
//                ritzResiduals_[i] = ritzResNrms[i];
//                i++;
//              }
//                ritzResiduals_[i] = ritzResNrms[i];
//                i++;
//            }

          //
          // Compute the Ritz residuals for each Ritz value.
          //
          HelperTraits<ScalarType>::computeRitzResiduals(tmp_iRitzValueEigenSolvers, ritzRes, &ritzResiduals_);
        }
        //
        // Sort the Ritz values.
        //
        {
          int i=0;
          if (problem_->isHermitian()) {
            //
            // Sort using just the real part of the Ritz values.
            sm_->sort(tmp_rRitzValueEigenSolvers, smartPtrFromRef(&ritzOrder_), curDim_); // don't catch exception
            ritzIndex_.clear();
            while (i < curDim_) {
              // The Ritz value is not complex.
              ritzValues_[i].set(tmp_rRitzValueEigenSolvers[i], MT_ZERO);
              ritzIndex_.push_back(0);
              i++;
            }
          }
          else {
            //
            // Sort using both the real and imaginary parts of the Ritz values.
            sm_->sort(tmp_rRitzValueEigenSolvers, tmp_iRitzValueEigenSolvers, smartPtrFromRef(&ritzOrder_) , curDim_);
            HelperTraits<ScalarType>::sortRitzValueEigenSolvers(tmp_rRitzValueEigenSolvers, tmp_iRitzValueEigenSolvers, &ritzValues_, &ritzOrder_, &ritzIndex_);
          }
          //
          // Sort the ritzResiduals_ based on the ordering from the Sort Manager.
          std::vector<MagnitudeType> ritz2(curDim_);
          for (i=0; i<curDim_; i++) { ritz2[i] = ritzResiduals_[ ritzOrder_[i] ]; }
          //**blas_mag.COPY(curDim_, &ritz2[0], 1, &ritzResiduals_[0], 1);
          std::copy(ritz2.begin(),ritz2.end(),ritzResiduals_.begin());
          
          // The Ritz values have now been updated.
          ritzValsCurrent_ = true;
        }

      } // if (!ritzValsCurrent_) ...
      // 
      //---------------------------------------------------
      // * The Ritz values and residuals have been updated at this point.
      // 
      // * The Schur factorization of the projected matrix has been computed,
      //   and is stored in (schurH_, Q_).
      //
      // Now the Schur factorization needs to be sorted.
      //---------------------------------------------------
      //
      // Sort the Schur form using the ordering from the Sort Manager.
      if (sort) {
        sortSchurForm(*schurH_, *Q_, ritzOrder_);
        //
        // Indicate the Schur form in (schurH_, Q_) is current and sorted
        schurCurrent_ = true;
      }
    } // if (!schurCurrent_) ...

  } // if (curDim_) ...

} // computeSchurForm(...)

//=============================================================================================
// Sort the Schur form of H stored in (H,Q) using the ordering vector.
template <class ScalarType, class MV, class OP>
void BlockKrylovSchur<ScalarType,MV,OP>::sortSchurForm(MatrixEigenDense<ScalarType>& H,
                                                        MatrixEigenDense<ScalarType>& Q,
                                                        std::vector<int>& order)
{
    if (H.numOfCols()<Q.numOfCols()) {
        MatrixEigenDense<ScalarType> tmpQ(Q,0,0,H.numOfRows(),H.numOfCols());
        internalEigenSolver::SortSchurForm<MatrixEigenDense<ScalarType>, NumTraits<ScalarType>::IsComplex>::run(H, tmpQ, order, curDim_, ritzIndex_);
        Q.replace(tmpQ,0,0,H.numOfRows(),H.numOfCols());
    } else {
        internalEigenSolver::SortSchurForm<MatrixEigenDense<ScalarType>, NumTraits<ScalarType>::IsComplex>::run(H, Q, order, curDim_, ritzIndex_);
    }
}

//=============================================================================================
// Print the current status of the solver
template <class ScalarType, class MV, class OP>
void BlockKrylovSchur<ScalarType,MV,OP>::currentStatus(std::ostream& os)
{
  using std::endl;

  os.setf(std::ios::scientific, std::ios::floatfield);
  os.precision(6);
  os <<"================================================================================" << endl;
  os << endl;
  os <<"                         BlockKrylovSchur Solver Status" << endl;
  os << endl;
  os <<"The solver is "<<(initialized_ ? "initialized." : "not initialized.") << endl;
  os <<"The number of iterations performed is " <<iter_<<endl;
  os <<"The block size is         " << blockSize_<<endl;
  os <<"The number of blocks is   " << numBlocks_<<endl;
  os <<"The current basis size is " << curDim_<<endl;
  os <<"The number of auxiliary vectors is " << numAuxVecs_ << endl;
  os <<"The number of operations Op*x   is "<<countApplyOp_<<endl;

  os.setf(std::ios_base::right, std::ios_base::adjustfield);

  os << endl;
  if (initialized_) {
    os <<"CURRENT RITZ VALUES             "<<endl;
    if (ritzIndex_.size() != 0) {
      int numPrint = (curDim_ < numRitzPrint_? curDim_: numRitzPrint_);
      if (problem_->isHermitian()) {
        os << std::setw(20) << "Ritz ValueEigenSolver"
           << std::setw(20) << "Ritz Residual"
           << endl;
        os <<"--------------------------------------------------------------------------------"<<endl;
        for (int i=0; i<numPrint; i++) {
          os << std::setw(20) << ritzValues_[i].realpart
             << std::setw(20) << ritzResiduals_[i] 
             << endl;
        }
      } else {
        os << std::setw(24) << "Ritz ValueEigenSolver"
           << std::setw(30) << "Ritz Residual"
           << endl;
        os <<"--------------------------------------------------------------------------------"<<endl;
        for (int i=0; i<numPrint; i++) {
          // Print out the real eigenvalue.
          os << std::setw(15) << ritzValues_[i].realpart;
          if (ritzValues_[i].imagpart < MT_ZERO) {
            os << " - i" << std::setw(15) << NumTraits<MagnitudeType>::magnitude(ritzValues_[i].imagpart);
          } else {
            os << " + i" << std::setw(15) << ritzValues_[i].imagpart;
          }              
          os << std::setw(20) << ritzResiduals_[i] << endl;
        }
      }
    } else {
      os << std::setw(20) << "[ NONE COMPUTED ]" << endl;
    }
  }
  os << endl;
  os <<"================================================================================" << endl;
  os << endl;
}
  
namespace internalEigenSolver {
/* Compute (Real/Complex) Schur form */
// this is the implementation for the case IsComplex = true
template<typename MatrixType, bool IsComplex>
struct ComputeSchurForm
{
ComputeSchurForm() : cplSchur(1) {}
void run(MatrixType& matrix, bool computeU = true)
{
    cplSchur.compute(matrix,computeU);
}
const MatrixType& matrixT() { return cplSchur.matrixT(); }
const MatrixType& matrixU() { return cplSchur.matrixU(); }
ComplexSchur<MatrixType> cplSchur;
};

# ifndef DOXYGEN_SHOULD_SKIP_THIS

// this is the implementation for the case IsComplex = false (RealSchur)
template<typename MatrixType>
struct ComputeSchurForm<MatrixType, false>
{
ComputeSchurForm():realSchur(1) {}
void run(MatrixType& matrix, bool computeU = true)
{
    realSchur.compute(matrix,computeU);
}
const MatrixType& matrixT() { return realSchur.matrixT(); }
const MatrixType& matrixU() { return realSchur.matrixU(); }

RealSchur<MatrixType> realSchur;
};

#endif

/* Sort (Real/Complex) Schur form */
// Sorting Schur form, the (quasi) triangular matrix T and orthogonal matrix Q with a order
// \param matrixT[in/out] (quasi) triangular matrix
// \param matriQ[in/out] orthogonal matrix
// \param sortOrder[in] sorting order
// \param curDim[in] size of matrixT and matrixQ (it seems to be redundant here but it's necessary)
// \param ritIndex[in] index of ritzValues, in fact, it serves only for real non-symmetric case
template<typename MatrixType, bool IsComplex>
struct SortSchurForm
{
  // this is the implementation for the case IsComplex = true
  static void run(MatrixType& matrixT, MatrixType& matrixQ, std::vector<int>& sortOrder, int curDim, std::vector<int>& ritIndex)
  {
      swapComplexSchurInPlace(matrixT, matrixQ, sortOrder);
  }
};

# ifndef DOXYGEN_SHOULD_SKIP_THIS

// The function run is implemented in according to stack-like manner of swapRealSchurInPlace
template<typename MatrixType>
struct SortSchurForm<MatrixType, false>
{
  // this is the implementation for the case IsComplex = true
  static void run(MatrixType& matrixT, MatrixType& matrixQ, std::vector<int>& sortOrder, int curDim, std::vector<int>& ritIndex)
  {
    int i = 0, nevtemp = 0;
    std::vector<int> offset2(curDim);
    std::vector<int> order2(curDim);

    while (i < curDim) {
      if (ritIndex[i] != 0) { // This is the first value of a complex conjugate pair
        offset2[nevtemp] = 0;
        int j = i+2;
        while (j < curDim) {
          if (sortOrder[j] > sortOrder[i]) { offset2[nevtemp]++; }
          ++j;
        }
        order2[nevtemp] = sortOrder[i];
        i = i+2;
      } else {
        offset2[nevtemp] = 0;
        for (int j=i; j<curDim; j++) {
          if (sortOrder[j] > sortOrder[i]) { offset2[nevtemp]++; }
        }
        order2[nevtemp] = sortOrder[i];
        i++;
      }
      nevtemp++;
    }

    for (i=nevtemp-1; i>=0; i--) {
        swapRealSchurInPlace(matrixT,matrixQ,order2[i]+offset2[i], 0);
    }
  }
};

#endif

/* Compute EigenVectors */
// this is the implementation for the case IsComplex = true
// Calculating eigenvector from Schur form, the (quasi) triangular matrix T and orthogonal matrix Q
// \param matrixnorm[in] a value prevents the calculation from overflow
// \param schurT[in/out] (quasi) triangular matrix
// \param schurU[in] orthogonal matrix
// \param eigVec[in/out] computed eigen vector
template<typename MatrixType, bool IsComplex>
struct EigenVectorComputation
{
  static void run(const real_t matrixnorm, const MatrixType& schurT, const MatrixType& schurU, MatrixType& eigVec)
  {
    doComputeEigenvectorsComplexSolverInPlace(matrixnorm, schurT, schurU, eigVec);
  }
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<typename MatrixType>
struct EigenVectorComputation<MatrixType, false>
{
  static void run(const real_t matrixnorm, const MatrixType& schurT, const MatrixType& schurU, MatrixType& eigVec)
  {
    doComputeEigenvectorsRealSolverInPlace(matrixnorm, schurT, schurU, eigVec);
  }
};

#endif

/* Compute EigenValues */
// this is the implementation for the case IsComplex = true
// Calculating eigenvalue from Schur form, the (quasi) triangular matrix T
// \param schurT[in/out] (quasi) triangular matrix
// \param rRitzValueEigenSolvers[in/out] real part of eigenvalue
// \param iRitzValueEigenSolvers[in/out] imagine part of eigenvalue
template<typename MatrixType, bool IsComplex>
struct EigenValueComputation
{
  static void run(const MatrixType& schurT, std::vector<real_t>& rRitzValueEigenSolvers, std::vector<real_t>& iRitzValueEigenSolvers)
  {
    Index i = 0;
    while (i < schurT.numOfCols())
    {
      rRitzValueEigenSolvers[i] = NumTraits<typename MatrixType::type_t>::realPart(schurT.coeff(i, i));
      iRitzValueEigenSolvers[i] = NumTraits<typename MatrixType::type_t>::imagPart(schurT.coeff(i, i));
      ++i;
    }
  }
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<typename MatrixType>
struct EigenValueComputation<MatrixType, false>
{
  static void run(const MatrixType& schurT, std::vector<real_t>& rRitzValueEigenSolvers, std::vector<real_t>& iRitzValueEigenSolvers)
  {
    Index i = 0;
    while (i < schurT.numOfCols())
    {
      if (i == schurT.numOfCols() - 1 || schurT.coeff(i + 1, i) == real_t(0))
      {
          rRitzValueEigenSolvers[i] = schurT.coeff(i, i);
          iRitzValueEigenSolvers[i] = real_t(0);
          ++i;
      }
      else
      {
        real_t p = real_t(0.5) * (schurT.coeff(i, i) - schurT.coeff(i + 1, i + 1));
        real_t z = std::sqrt(std::abs(p * p + schurT.coeff(i + 1, i) * schurT.coeff(i, i + 1)));
        rRitzValueEigenSolvers[i] = rRitzValueEigenSolvers[i+1] = schurT.coeff(i + 1, i + 1) + p;
        iRitzValueEigenSolvers[i] = z;
        iRitzValueEigenSolvers[i+1] = -z;
        i += 2;
      }
    }
  }
};

#endif

}// end of namespace internalEigenSolver

} // End of namespace xlifepp

#endif /*XLIFEPP_BLOCK_KRYLOV_SCHUR_HPP */
