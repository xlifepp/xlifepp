/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file XlifeppStatusTestResNorm.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013

  \brief A status test for testing the norm of the eigenvectors residuals.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_STATUS_TEST_RESNORM_HPP
#define XLIFEPP_STATUS_TEST_RESNORM_HPP

#include "utils.h"
#include "XlifeppStatusTestDecl.hpp"

namespace xlifepp {

//! @name LOBPCG Exceptions
//@{
/*!
  \class StatusTestResNorm
  \brief A status test for testing the norm of the eigenvectors residuals.

  StatusTestResNorm was designed to be used as a test for
  convergence. The tester compares the norms of the residual vectors against
  a user specified tolerance.

  In addition to specifying the tolerance, the user may specify:
  <ul>
  <li> the norm to be used: 2-norm or OrthoManager::norm() or EigenSolver::getRitzRes2Norms()
  <li> the scale: absolute or relative to magnitude of Ritz value
  <li> the quorum: the number of vectors required for the test to
  evaluate as ::_passed.
  </ul>
  */
template <class ScalarType, class MV, class OP>
class StatusTestResNorm : public StatusTest<ScalarType,MV,OP>
{
  typedef typename NumTraits<ScalarType>::RealScalar MagnitudeType;

  public:

    /*! \enum ResType
      Enumerated type used to specify which residual norm used by this status test.
    */
    enum ResType {
      _resOrth,
      _res2Norm,
      _ritzRes2Norm
    };

    //! @name Constructors/destructors
    //@{
    //! Constructor
    StatusTestResNorm(typename NumTraits<ScalarType>::RealScalar tol, int quorum = -1, ResType whichNorm = _resOrth, bool scaled = true, bool throwExceptionOnNaN = true);

    //! Destructor
    virtual ~StatusTestResNorm() {}
    //@}

    //! @name Status methods
    //@{
    /*!
      Check status as defined by test.
      \return TestStatus indicating whether the test passed or failed.
    */
    TestStatus checkStatus( EigenSolver<ScalarType,MV,OP>* solver );

    //! Return the result of the most recent checkStatus call, or undefined if it has not been run.
    TestStatus getStatus() const { return state_; }

    //! Get the indices for the vectors that passed the test.
    std::vector<int> whichVecs() const {
      return ind_;
    }

    //! Get the number of vectors that passed the test.
    int howMany() const {
      return ind_.size();
    }
    //@}

    //! @name Accessor methods
    //@{
    /*! \brief Set quorum.
     *
     *  Setting quorum to -1 signifies that all residuals from the solver must meet the tolerance.
     *  This also resets the test status to ::_undefined.
     */
    void setQuorum(int quorum) {
      state_ = _undefined;
      quorum_ = quorum;
    }

    /*! \brief Get quorum.
    */
    int getQuorum() const {
      return quorum_;
    }

    /*! \brief Set tolerance.
     *  This also resets the test status to ::_undefined.
     */
    void setTolerance(typename NumTraits<ScalarType>::RealScalar tol) {
      state_ = _undefined;
      tol_ = tol;
    }

    //! Get tolerance.
    typename NumTraits<ScalarType>::RealScalar getTolerance() {return tol_;}

    /*! \brief Set the residual norm to be used by the status test.
     *
     *  This also resets the test status to ::_undefined.
     */
    void setWhichNorm(ResType whichNorm) {
      state_ = _undefined;
      whichNorm_ = whichNorm;
    }

    //! Return the residual norm used by the status test.
    ResType getWhichNorm() {return whichNorm_;}

    /*! \brief Instruct test to scale norms by eigenvalue estimates (relative scale).
     *  This also resets the test status to ::_undefined.
     */
    void setScale(bool relscale) {
      state_ = _undefined;
      scaled_ = relscale;
    }

    //! Returns true if the test scales the norms by the eigenvalue estimates (relative scale).
    bool getScale() {return scaled_;}
    //@}

    //! @name Reset methods
    //@{
    //! Informs the status test that it should reset its internal configuration to the uninitialized state.
    /*! This is necessary for the case when the status test is being reused by another solver or for another
      eigenvalue problem. The status test may have information that pertains to a particular problem or solver
      state. The internal information will be reset back to the uninitialized state. The user specified information
      that the convergence test uses will remain.
      */
    void reset() {
      ind_.resize(0);
      state_ = _undefined;
    }

    //! Clears the results of the last status test.
    /*! This should be distinguished from the reset() method, as it only clears the cached result from the last
     * status test, so that a call to getStatus() will return ::_undefined. This is necessary for the SEQOR and SEQAND
     * tests in the StatusTestCombo class, which may short circuit and not evaluate all of the StatusTests contained
     * in them.
     */
    void clearStatus() {
      ind_.resize(0);
      state_ = _undefined;
    }
    //@}

    //! @name Print methods
    //@{
    //! Output formatted description of stopping test to output stream.
    std::ostream& print(std::ostream& os, int indent = 0) const;

    PrintStream& print(PrintStream& os, int indent = 0) const
    { print(os.currentStream(),indent); return os;}
    //@}

  private:
    TestStatus state_;
    MagnitudeType tol_;
    std::vector<int> ind_;
    int quorum_;
    bool scaled_;
    ResType whichNorm_;
    bool throwExceptionOnNaN_;
};
//@}

template <class ScalarType, class MV, class OP>
StatusTestResNorm<ScalarType,MV,OP>::StatusTestResNorm(typename NumTraits<ScalarType>::RealScalar tol, int quorum, ResType whichNorm, bool scaled, bool throwExceptionOnNaN)
: state_(_undefined), tol_(tol), quorum_(quorum), scaled_(scaled), whichNorm_(whichNorm), throwExceptionOnNaN_(throwExceptionOnNaN)
{}

template <class ScalarType, class MV, class OP>
TestStatus StatusTestResNorm<ScalarType,MV,OP>::checkStatus( EigenSolver<ScalarType,MV,OP>* solver )
{
  typedef NumTraits<MagnitudeType> MT;

  std::vector<MagnitudeType> res;

  // get the eigenvector/ritz residuals norms (using the appropriate norm)
  // get the eigenvalues/ritzvalues and ritz index as well
  std::vector<ValueEigenSolver<ScalarType> > vals = solver->getRitzValues();
  switch (whichNorm_) {
    case _res2Norm:
      res = solver->getRes2Norms();
      // we want only the ritz values corresponding to our eigenvector residuals
      vals.resize(res.size());
      break;
    case _resOrth:
      res = solver->getResNorms();
      // we want only the ritz values corresponding to our eigenvector residuals
      vals.resize(res.size());
      break;
    case _ritzRes2Norm:
      res = solver->getRitzRes2Norms();
      break;
  }

  // if appropriate, scale the norms by the magnitude of the eigenvalue estimate
  if (scaled_) {
    for (unsigned int i=0; i<res.size(); i++) {
        MagnitudeType tmp = NumTraits<MagnitudeType>::lapy2(vals[i].realpart,vals[i].imagpart);
      // scale by the newly computed magnitude of the ritz values
      if ( tmp != MT::zero() ) {
        res[i] /= tmp;
      }
    }
  }

  // test the norms
  int have = 0;
  ind_.resize(res.size());
  for (unsigned int i=0; i<res.size(); i++) {
    if (res[i] < tol_) {
      ind_[have] = i;
      have++;
    }
  }
  ind_.resize(have);
  int need = (quorum_ == -1) ? res.size() : quorum_;
  state_ = (have >= need) ? _passed : _failed;
  return state_;
}


template <class ScalarType, class MV, class OP>
std::ostream& StatusTestResNorm<ScalarType,MV,OP>::print(std::ostream& os, int indent) const
{
  std::string ind(indent,' ');
  os << ind << "- StatusTestResNorm: ";
  switch (state_) {
    case _passed:
      os << "_passed" << std::endl;
      break;
    case _failed:
      os << "_failed" << std::endl;
      break;
    case _undefined:
      os << "_undefined" << std::endl;
      break;
  }
  os << ind << "  (Tolerance,WhichNorm,Scaled,Quorum): "
    << "(" << tol_;
  switch (whichNorm_) {
    case _resOrth:
      os << ",_resOrth";
      break;
    case _res2Norm:
      os << ",_res2Norm";
      break;
    case _ritzRes2Norm:
      os << ",_ritzRes2Norm";
      break;
  }
  os        << "," << (scaled_   ? "true" : "false")
    << "," << quorum_
    << ")" << std::endl;

  if (state_ != _undefined) {
    os << ind << "  Which vectors: ";
    if (ind_.size() > 0) {
      for (unsigned int i=0; i<ind_.size(); i++) os << ind_[i] << " ";
      os << std::endl;
    }
    else {
      os << "[empty]" << std::endl;
    }
  }
  return os;
}

} // end of xlifepp namespace

#endif /* XLIFEPP_STATUS_TEST_RESNORM_HPP */
