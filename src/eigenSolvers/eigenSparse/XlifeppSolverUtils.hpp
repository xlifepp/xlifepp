/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppSolverUtils.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013
  
  \brief Class which provides internal utilities for the xlifepp solvers.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_SOLVER_UTILS_HPP
#define XLIFEPP_SOLVER_UTILS_HPP

#include "utils.h"
#include "../eigenCore/utils/MatrixEigenDense.hpp"
#include "EigenTestError.hpp"
#include "XlifeppMultiVecTraits.hpp"
#include "XlifeppOperatorTraits.hpp"
#include "XlifeppOutputManager.hpp"


namespace xlifepp {

/*!    \class SolverUtils
       \brief xlifepp's templated, static class providing utilities for
       the solvers.

       This class provides concrete, templated implementations of utilities necessary
       for the solvers.  These utilities include
       sorting, orthogonalization, projecting/solving local eigensystems, and sanity
       checking.  These are internal utilties, so the user should not alter this class.
*/
template<class ScalarType, class MV, class OP>
class SolverUtils 
{  
  public:
    typedef typename NumTraits<ScalarType>::RealScalar MagnitudeType;
    typedef typename NumTraits<ScalarType>::Scalar  SCT;
    typedef MatrixEigenDense<SCT> MatrixDense;
    
    //! @name Constructor/Destructor
    //@{ 

    //! Constructor.  
    SolverUtils();
    
    //! Destructor.
    virtual ~SolverUtils() {}
    
    //@}
    
    //! @name Sorting Methods
    //@{ 
    
    //! Permute the vectors in a multivector according to the permutation vector \c perm, and optionally the residual vector \c resids
    static void permuteVectors(const int n, const std::vector<int>& perm, MV& Q, std::vector< MagnitudeType >* resids = nullptr);

    //! Permute the columns of a Teuchos::SerialDenseMatrix according to the permutation vector \c perm
    static void permuteVectors(const std::vector<int>& perm, MatrixDense& Q);

    //@} 

    //! @name Basis update methods
    //@{

    //! apply a sequence of Householder reflectors (from \c GEQRF) to a multivector, using minimal workspace.
    /*! 
      @param k [in] the number of Householder reflectors composing the product
      @param V [in/out] the multivector to be modified, with \f$n\f$ columns
      @param H [in] a \f$n \times k\f$ matrix containing the encoded Householder vectors, as returned from \c GEQRF (see below)
      @param tau [in] the \f$n\f$ coefficients for the Householder reflects, as returned from \c GEQRF
      @param workMV [work] (optional) a multivector used for workspace. it need contain only a single vector; it if contains more, only the first vector will be modified.

      This routine applies a sequence of Householder reflectors, \f$H_1 H_2 \cdots H_k\f$, to a multivector \f$V\f$. The 
      reflectors are applied individually, as rank-one updates to the multivector. The benefit of this is that the only 
      required workspace is a one-column multivector. This workspace can be provided by the user. If it is not, it will
      be allocated locally on each call to applyHouse.

      Each \f$H_i\f$ (\f$i=1,\ldots,k \leq n\f$) has the form<br>
      \f$ H_i = I - \tau_i v_i v_i^T \f$ <br>
      where \f$\tau_i\f$ is a scalar and \f$v_i\f$ is a vector with
      \f$v_i(1:i-1) = 0\f$ and \f$e_i^T v_i = 1\f$; \f$v(i+1:n)\f$ is stored below <tt>H(i,i)</tt>
      and \f$\tau_i\f$ in <tt>tau[i-1]</tt>.
      \note zero-based indexing used for data structures \c H and \c tau, while one-based indexing used for mathematic object \f$v_i\f$.

      If the multivector is \f$m \times n\f$ and we apply \f$k\f$ Householder reflectors, the total cost of the method is
      \f$4mnk - 2m(k^2-k)\f$ flops. For \f$k=n\f$, this becomes \f$2mn^2\f$, the same as for a matrix-matrix multiplication by the accumulated Householder reflectors.
     */
    static void applyHouse(int k, MV& V, const MatrixDense& H, const std::vector<ScalarType>& tau, SmartPtr<MV> workMV = _smPtrNull);

    //@}

    //! @name Eigensolver Projection Methods
    //@{ 

    //! Routine for computing the first NEV generalized eigenpairs of the Hermitian pencil <tt>(KK, MM)</tt>
    /*!
      @param size [in] Dimension of the eigenproblem (KK, MM)
      @param KK [in] Hermitian "stiffness" matrix 
      @param MM [in] Hermitian positive-definite "mass" matrix
      @param EV [in] Dense matrix to store the nev eigenvectors 
      @param theta [in] Array to store the eigenvalues (Size = nev)
      @param nev [in/out] Number of the smallest eigenvalues requested (in) / computed (out)
      @param esType [in] Flag to select the algorithm
      <ul>
      <li> esType =  0  (default) (Cholesky factorization of MM)
                        with deflation of MM to get orthonormality of 
                        eigenvectors (\f$S^TMMS = I\f$)
      <li> esType =  1  (Cholesky factorization of MM)
                        (no check of orthonormality)
      <li> esType = 10  Simple eigenproblem on KK
                        (MM is not referenced in this case)
      </ul>

      \note The code accesses only the upper triangular part of KK and MM.
      \return Integer \c info on the status of the computation
      // Return the integer info on the status of the computation
      <ul>
      <li> info = 0 >> Success
      <li> info = - 20 >> Failure in LAPACK routine
      </ul>
    */
    static int directSolver(int size, const MatrixDense& KK,
                     SmartPtr<const MatrixDense> MM,
                     MatrixDense& EV,
                     std::vector< MagnitudeType >& theta,
                     int& nev, int esType = 0);
    //@}

    //! @name Sanity Checking Methods
    //@{ 

    //! Return the maximum coefficient of the matrix \f$M * X - MX\f$ scaled by the maximum coefficient of \c MX.
    /*! \note When \c M is not specified, the identity is used.
     */
    static typename NumTraits<ScalarType>::RealScalar errorEquality(const MV& X, const MV& MX, SmartPtr<const OP> M = _smPtrNull);
    
    //@}
    
  private:

    //! @name Internal Typedefs
    //@{ 

    typedef MultiVecTraits<ScalarType,MV> MVT;
    typedef OperatorTraits<ScalarType,MV,OP> OPT;

    //@}
};

//-----------------------------------------------------------------------------
// 
//  CONSTRUCTOR
//
//-----------------------------------------------------------------------------  

template<class ScalarType, class MV, class OP>
SolverUtils<ScalarType, MV, OP>::SolverUtils() {}


//-----------------------------------------------------------------------------
// 
//  SORTING METHODS
//
//-----------------------------------------------------------------------------

//=============================================================================
// permuteVectors for MV
template<class ScalarType, class MV, class OP>
void SolverUtils<ScalarType, MV, OP>::permuteVectors(
            const int n,
            const std::vector<int>& perm,
            MV& Q,
            std::vector< MagnitudeType >* resids)
{
  // Permute the vectors according to the permutation vector \c perm, and
  // optionally the residual vector \c resids
  
  int i, j;
  std::vector<int> permcopy(perm), swapvec(n-1);
  std::vector<int> index(1);
  ScalarType one = NumTraits<ScalarType>::one();
  ScalarType zero = NumTraits<ScalarType>::zero();

  internalEigenSolver::testErrorEigenProblem(n > MVT::getNumberVecs(Q),  "xlifepp::SolverUtils::permuteVectors(): argument n larger than width of input multivector.");

  // We want to recover the elementary permutations (individual swaps) 
  // from the permutation vector. Do this by constructing the inverse
  // of the permutation, by sorting them to {1,2,...,n}, and recording
  // the elementary permutations of the inverse.
  for (i=0; i<n-1; i++) {
    //
    // find i in the permcopy vector
    for (j=i; j<n; j++) {
      if (permcopy[j] == i) {
        // found it at index j
        break;
      }
      internalEigenSolver::testErrorEigenProblem(j == n-1,  "xlifepp::SolverUtils::permuteVectors(): permutation index invalid.");
    }
    //
    // Swap two scalars
    std::swap(permcopy[j], permcopy[i]);

    swapvec[i] = j;
  }
    
  // now apply the elementary permutations of the inverse in reverse order
  for (i=n-2; i>=0; i--) {
    j = swapvec[i];
    //
    // Swap (i,j)
    //
    // Swap residuals (if they exist)
    if (resids) {
      std::swap( (*resids)[i], (*resids)[j]);
    }
    //
    // Swap corresponding vectors
    index[0] = j;
    SmartPtr<MV> tmpQ = MVT::cloneCopy(Q, index);
    SmartPtr<MV> tmpQj = MVT::cloneViewNonConst(Q, index);
    index[0] = i;
    SmartPtr<MV> tmpQi = MVT::cloneViewNonConst(Q, index);
    MVT::mvAddMv(one, *tmpQi, zero, *tmpQi, *tmpQj);
    MVT::mvAddMv(one, *tmpQ, zero, *tmpQ, *tmpQi);
  }
}


//==========================================================================
// permuteVectors for MV
template<class ScalarType, class MV, class OP>
void SolverUtils<ScalarType, MV, OP>::permuteVectors(
            const std::vector<int>& perm,
            MatrixDense& Q)
{
  // Permute the vectors in Q according to the permutation vector \c perm, and
  // optionally the residual vector \c resids
  const int n = perm.size();
  const int m = Q.numOfRows();
  
  internalEigenSolver::testErrorEigenProblem(n != Q.numOfCols(),  "xlifepp::SolverUtils::permuteVectors(): size of permutation vector not equal to number of columns.");

  // Sort the primitive ritz vectors
  MatrixDense copyQ(Q);
  for (int i=0; i< n; i++) {
    for (int j = 0; j < m; ++j) {
      Q.coeffRef(j,i) = copyQ.coeff(j, perm[i]);
    }
  }
}


//-----------------------------------------------------------------------------
// 
//  BASIS UPDATE METHODS
//
//-----------------------------------------------------------------------------

// apply householder reflectors to multivector
template<class ScalarType, class MV, class OP>
void SolverUtils<ScalarType, MV, OP>::applyHouse(int k, MV& V, const MatrixDense& H, const std::vector<ScalarType>& tau, SmartPtr<MV> workMV) {

  const int n = MVT::getNumberVecs(V);
  const ScalarType ONE = NumTraits<SCT>::one();
  const ScalarType ZERO = NumTraits<SCT>::zero();

  // early exit if V has zero-size or if k==0
  if (MVT::getNumberVecs(V) == 0 || MVT::getVecLength(V) == 0 || k == 0) {
    return;
  }

  if (workMV == _smPtrNull) {
    // user did not give us any workspace; allocate some
    workMV = MVT::clone(V,1);
  }
  else if (MVT::getNumberVecs(*workMV) > 1) {
    std::vector<int> first(1);
    first[0] = 0;
    workMV = MVT::cloneViewNonConst(*workMV,first);
  }
  else {
    internalEigenSolver::testErrorEigenProblem(MVT::getNumberVecs(*workMV) < 1,"xlifepp::SolverUtils::applyHouse(): work multivector was empty.");
  }
  // Q = H_1 ... H_k is square, with as many rows as V has vectors
  // however, H need only have k columns, one each for the k reflectors.
  internalEigenSolver::testErrorEigenProblem(H.numOfCols() != k, "xlifepp::SolverUtils::applyHouse(): H must have at least k columns.");
  internalEigenSolver::testErrorEigenProblem((int)tau.size() != k, "xlifepp::SolverUtils::applyHouse(): tau must have at least k entries.");
  internalEigenSolver::testErrorEigenProblem(H.numOfRows() != MVT::getNumberVecs(V), "xlifepp::SolverUtils::applyHouse(): Size of H,V are inconsistent.");

  // perform the loop
  // flops: Sum_{i=0:k-1} 4 m (n-i) == 4mnk - 2m(k^2- k)
  for (int i=0; i<k; i++) {
    // apply V H_i+1 = V - tau_i+1 (V v_i+1) v_i+1^T
    // because of the structure of v_i+1, this transform does not affect the first i columns of V
    std::vector<int> activeind(n-i);
    for (int j=0; j<n-i; j++) activeind[j] = j+i;
    SmartPtr<MV> actV = MVT::cloneViewNonConst(V,activeind);

    // note, below H_i, v_i and tau_i are mathematical objects which use 1-based indexing
    // while H, v and tau are data structures using 0-based indexing

    // get v_i+1: i-th column of H
    MatrixDense v(H,i,i,n-i,1);
    // v_i+1(1:i) = 0: this isn't part of v
    // e_i+1^T v_i+1 = 1 = v(0)

    v.coeffRef(0,0) = ONE;

    // compute -tau_i V v_i
    // tau_i+1 is tau[i]
    // flops: 2 m n-i
    MVT::mvTimesMatAddMv(-tau[i],*actV,v,ZERO,*workMV);// Replaced by this code to adapted with our eigen core

    // perform V = V + workMV v_i^T
    // flops: 2 m n-i
    MatrixDense vT = conj(transpose(v)); //Not a smart way to do this but we can improve later //(v,Teuchos::CONJ_TRANS);
    MVT::mvTimesMatAddMv(ONE,*workMV,vT,ONE,*actV);

    actV = _smPtrNull;
  }
}


//-----------------------------------------------------------------------------
// 
//  EIGENSOLVER PROJECTION METHODS
//
//-----------------------------------------------------------------------------
template<class ScalarType, class MV, class OP>
int SolverUtils<ScalarType, MV, OP>::directSolver(
    int size,
    const MatrixDense& KK,
    SmartPtr<const MatrixDense> MM,
    MatrixDense& EV,
    std::vector< MagnitudeType >& theta,
    int& nev, int esType)
{
  // Routine for computing the first NEV generalized eigenpairs of the symmetric pencil (KK, MM)
  //
  // Parameter variables:
  //
  // size: Dimension of the eigenproblem (KK, MM)
  //
  // KK: Hermitian "stiffness" matrix 
  //
  // MM: Hermitian positive-definite "mass" matrix
  //
  // EV: Matrix to store the nev eigenvectors 
  //
  // theta: Array to store the eigenvalues (Size = nev)
  //
  // nev: Number of the smallest eigenvalues requested (input)
  //       Number of the smallest computed eigenvalues (output)
  //       Routine may compute and return more or less eigenvalues than requested.
  //
  // esType: Flag to select the algorithm
  //
  // esType =  0 (default) Uses LAPACK routine (Cholesky factorization of MM)
  //                       with deflation of MM to get orthonormality of 
  //                       eigenvectors (S^T MM S = I)
  //
  // esType =  1           Uses LAPACK routine (Cholesky factorization of MM)
  //                       (no check of orthonormality)
  //
  // esType = 10           Uses LAPACK routine for simple eigenproblem on KK
  //                       (MM is not referenced in this case)
  //
  // Note: The code accesses only the upper triangular part of KK and MM.
  //
  // Return the integer info on the status of the computation
  //
  // info = 0 >> Success
  //
  // info < 0 >> error in the info-th argument
  // info = - 20 >> Failure in LAPACK routine

  // Define local arrays

  // Create blas/lapack objects.
  //Teuchos::LAPACK<int,ScalarType> lapack;
  //Teuchos::BLAS<int,ScalarType> blas;

  int rank = 0;
  ComputationInfo info = _noConvergence;
  
  if (size < nev || size < 0) {
    return -1;
  }
  if (KK.numOfCols() < size || KK.numOfRows() < size) {
    return -2;
  }
  if ((esType == 0 || esType == 1)) {
    if (_smPtrNull == MM) {
      return -3;
    }
    else if (MM->numOfCols() < size || MM->numOfRows() < size) {
      return -3;
    }
  }
  if (EV.numOfCols() < size || EV.numOfRows() < size) {
    return -4;
  }
  if ((int)theta.size() < size) {
    return -5;
  }
  if (nev <= 0) {
    return -6;
  }

  MagnitudeType tol = NumTraits<SCT>::magnitude(std::sqrt(NumTraits<SCT>::epsilon()));
  ScalarType zero = NumTraits<ScalarType>::zero();
  ScalarType one = NumTraits<ScalarType>::one();

  SmartPtr<MatrixDense> KKcopy, MMcopy, U;

  GeneralizedSelfAdjointEigenSolver<MatrixDense> hegv;
  MatrixDense eigenVec;
  int idxTemp = 0;

  switch (esType) {
    default:
    case 0:
      //
      // Use eigenCore to compute the generalized eigenvectors
      //
      for (rank = size; rank > 0; --rank) {
          //
          // Copy KK&  MM
          //
        KKcopy = SmartPtr<MatrixDense>(new MatrixDense(KK, 0, 0, rank, rank));
        MMcopy = SmartPtr<MatrixDense>(new MatrixDense((*MM), 0, 0, rank, rank));
        U = SmartPtr<MatrixDense>(new MatrixDense(rank,rank));

        //
        // Solve the generalized eigenproblem with eigenCore
        //
        hegv.compute(*KKcopy, *MMcopy, _computeEigenVector | _Ax_lBx);
        info = hegv.info();
        if (_success != info) { rank = (int)(rank/2); continue; }
        eigenVec = hegv.eigenvectors();

        //
        // Check the quality of eigenvectors (using mass-orthonormality)
        //
        for (int i = 0; i < rank; ++i) {
          for (int j = 0; j < i; ++j) {
            (MMcopy)->coeffRef(i,j) = conj((MM)->coeff(j,i));
          }
        }

        // U = MMcopy * eigenVec;
        multMatMat((*MMcopy), eigenVec, *U);
        // MMcopy = eigenVec^H*U = eigenVec^H * MMcopy * eigenVec
        multMatMat(adjoint(eigenVec), *U, (*MMcopy));
        // Check whether MMcopy satisfies orthogonalization
        MagnitudeType maxNorm = NumTraits<SCT>::magnitude(zero);
        MagnitudeType maxOrth = NumTraits<SCT>::magnitude(zero);
        for (int i = 0; i < rank; ++i) {
          for (int j = i; j < rank; ++j) {
            if (j == i)
              maxNorm = NumTraits<SCT>::magnitude((*MMcopy).coeff(i,j) - one) > maxNorm
                ? NumTraits<SCT>::magnitude((*MMcopy).coeff(i,j) - one) : maxNorm;
            else 
              maxOrth = NumTraits<SCT>::magnitude((*MMcopy).coeff(i,j)) > maxOrth
                ? NumTraits<SCT>::magnitude((*MMcopy).coeff(i,j)) : maxOrth;
          }
        }
        if ((maxNorm <= tol) && (maxOrth <= tol)) {
          break;
        }
      } // for (rank = size; rank > 0; --rank)

      //
      // Copy the computed eigenvectors and eigenvalues
      // (they may be less than the number requested because of deflation)
      //
      idxTemp = 0;
      for (number_t i = 0; i < eigenVec.numOfCols(); ++i) {
        if (0 != hegv.eigenvalues().coeff(i)) {
          for (number_t j = 0; j < eigenVec.numOfRows(); ++j) {
              EV.coeffRef(j,idxTemp) = eigenVec.coeff(j,i);
          }
          theta[idxTemp] = hegv.eigenvalues().coeff(i);
          ++idxTemp;
        }
      }
      break;

    case 1:
      //
      // Use the Cholesky factorization of MM to compute the generalized eigenvectors
      //
      // Copy KK&  MM
      //
      KKcopy = SmartPtr<MatrixDense>(new MatrixDense(KK, 0, 0, size, size));
      MMcopy = SmartPtr<MatrixDense>(new MatrixDense(*MM, 0, 0, size, size));
      //
      // Solve the generalized eigenproblem with eigenCore. Keep LAPACK function as reference
      //

      // lapack.HEGV(1, 'V', 'U', size, KKcopy->values(), KKcopy->stride(),
      //           MMcopy->values(), MMcopy->stride(),& tt[0],& work[0], lwork,
      //           & rwork[0],& info);
      hegv.compute(KK, *MM, _computeEigenVector | _Ax_lBx);
      info = hegv.info();

      //
      // Treat error messages
      //
      if (_success != info) {break;}

      EV = hegv.eigenvectors();
      theta = hegv.eigenvalues();
      break;

    case 10:
      //
      // Simple eigenproblem
      // Solve the generalized eigenproblem with eigenCore
      //
      SelfAdjointEigenSolver<MatrixDense> heev;

      MatrixDense tmpKK(KK, 0, 0, size, size);
      heev.compute(tmpKK, _computeEigenVector);
      info = heev.info();

      //
      // Treat error messages
      if (_success != info) {
        internalEigenSolver::testWarningEigenProblem(_noConvergence == info,
                "xlifepp::SolverUtils::directSolver(): In SelfAdjointEigenSolver, the algorithm failed to converge");
        internalEigenSolver::testWarningEigenProblem(_invalidInput == info,
                "xlifepp::SolverUtils::directSolver(): In SelfAdjointEigenSolver, has an invalid input");
        break;
      }

      //
      // Copy the eigenvectors
      //
      for (int i = 0; i < size; ++i) {
          theta[i] = heev.eigenvalues()[i];
          for (int j = 0; j < size; ++j) {
              EV.coeffRef(j,i) = heev.eigenvectors().coeff(j,i);
          }
      }
      break;
  }

  return info;
}


//-----------------------------------------------------------------------------
// 
//  SANITY CHECKING METHODS
//
//-----------------------------------------------------------------------------

template<class ScalarType, class MV, class OP>
typename NumTraits<ScalarType>::RealScalar
SolverUtils<ScalarType, MV, OP>::errorEquality(const MV& X, const MV& MX, SmartPtr<const OP> M)
{
  // Return the maximum coefficient of the matrix M * X - MX
  // scaled by the maximum coefficient of MX.
  // When M is not specified, the identity is used.
  
  MagnitudeType maxDiff = NumTraits<SCT>::magnitude(NumTraits<SCT>::zero());
  
  int xc = MVT::getNumberVecs(X);
  int mxc = MVT::getNumberVecs(MX);
  
  internalEigenSolver::testErrorEigenProblem(xc != mxc,"xlifepp::SolverUtils::errorEquality(): input multivecs have different number of columns.");
  if (xc == 0) {
    return maxDiff;
  }
  
  MagnitudeType maxCoeffX = NumTraits<SCT>::magnitude(NumTraits<SCT>::zero());
  std::vector<MagnitudeType> tmp(xc);
  MVT::mvNorm(MX, tmp);

  for (int i = 0; i < xc; ++i) {
    maxCoeffX = (tmp[i] > maxCoeffX) ? tmp[i] : maxCoeffX;
  }

  std::vector<int> index(1);
  SmartPtr<MV> MtimesX;
  if (M != _smPtrNull) {
    MtimesX = MVT::clone(X, xc);
    OPT::apply(*M, X, *MtimesX);
  }
  else {
    MtimesX = MVT::cloneCopy(X);
  }
  MVT::mvAddMv(-1.0, MX, 1.0, *MtimesX, *MtimesX);
  MVT::mvNorm(*MtimesX, tmp);
 
  for (int i = 0; i < xc; ++i) {
    maxDiff = (tmp[i] > maxDiff) ? tmp[i] : maxDiff;
  }
  
  return (maxCoeffX == 0.0) ? maxDiff : maxDiff/maxCoeffX;
  
}  
  
} // end of namespace xlifepp

#endif /* XLIFEPP_SOLVER_UTILS_HPP */

