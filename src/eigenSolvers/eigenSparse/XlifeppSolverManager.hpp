/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppSolverManager.hpp
  \author Manh Ha NGUYEN
  \since 26 Mars 2013
  \date  12 June  2013
  
  \brief Pure virtual base class which describes the basic interface for a solver manager.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_SOLVERMANAGER_HPP
#define XLIFEPP_SOLVERMANAGER_HPP

#include "XlifeppEigenTypes.hpp"
#include "XlifeppEigenProblem.hpp"

namespace xlifepp {

/*!
  \class SolverManager

  \brief The SolverManager is a templated virtual base class that defines the
    basic interface that any solver manager will support.
*/
template<class ScalarType, class MV, class OP>
class SolverManager {
    
public:

  //!@name Constructors/Destructor 
  //@{ 

  //! Empty constructor.
  SolverManager() {}

  //! Destructor.
  virtual ~SolverManager() {}
  //@}
  
  //! @name Accessor methods
  //@{ 

  //! Return the eigenvalue problem.
  virtual const EigenProblem<ScalarType,MV,OP>& getProblem() const = 0;

  //! Get the iteration count for the most recent call to \c solve().
  virtual int getNumIters() const = 0;

  //@}

  //! @name Solver application methods
  //@{ 
    
  /*! \brief This method performs possibly repeated calls to the underlying eigensolver's iterate() routine
   * until the problem has been solved (as decided by the solver manager) or the solver manager decides to 
   * quit.
   *
   * \returns ::ComputationInfo specifying:
   *    - ::_success: the eigenproblem was solved to the specification required by the solver manager.
   *    - ::_noConvergence: the eigenproblem was not solved to the specification desired by the solver manager
  */
  virtual ComputationInfo solve() = 0;
  //@}
  
};

} // end of namespace xlifepp

#endif /* XLIFEPP_SOLVERMANAGER_HPP */
