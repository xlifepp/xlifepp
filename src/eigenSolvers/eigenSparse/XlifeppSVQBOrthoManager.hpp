/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppSVQBOrthoManager.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  05 June 2013
  
  \brief Orthogonalization manager based on the SVQB technique described in
  "A Block Orthogonalization Procedure With Constant Synchronization Requirements", A. Stathapoulos and K. Wu
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 xlifepp: Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************
// @HEADER


#ifndef XLIFEPP_SVQB_ORTHOMANAGER_HPP
#define XLIFEPP_SVQB_ORTHOMANAGER_HPP

#include "utils.h"
#include "EigenTestError.hpp"
#include "XlifeppMultiVecTraits.hpp"
#include "XlifeppOperatorTraits.hpp"
#include "XlifeppMatOrthoManager.hpp"

namespace xlifepp {

/*!   \class SVQBOrthoManager
      \brief An implementation of the xlifepp::MatOrthoManager that performs orthogonalization
      using the SVQB iterative orthogonalization technique described by Stathapoulos and Wu. This orthogonalization routine,
      while not returning the upper triangular factors of the popular Gram-Schmidt method, has a communication 
      cost (measured in number of communication calls) that is independent of the number of columns in the basis.
*/
template<class ScalarType, class MV, class OP>
class SVQBOrthoManager : public MatOrthoManager<ScalarType,MV,OP> {

  private:
    typedef typename NumTraits<ScalarType>::RealScalar MagnitudeType;
    typedef NumTraits<ScalarType>  SCT;
    typedef NumTraits<MagnitudeType>  SCTM;
    typedef MultiVecTraits<ScalarType,MV>      MVT;
    typedef OperatorTraits<ScalarType,MV,OP>   OPT;
    std::string dbgstr;


  public:

    //! @name Constructor/Destructor
    //@{ 
    //! Constructor specifying re-orthogonalization tolerance.
    SVQBOrthoManager(SmartPtr<const OP> Op = _smPtrNull, bool debug = false);


    //! Destructor
    ~SVQBOrthoManager() {}
    //@}



    //! @name Methods implementing xlifepp::MatOrthoManager
    //@{ 


    /*! \brief Given a list of mutually orthogonal and internally orthonormal bases \c Q, this method
     * projects a multivector \c X onto the space orthogonal to the individual <tt>Q[i]</tt>, 
     * optionally returning the coefficients of \c X for the individual <tt>Q[i]</tt>. All of this is done with respect
     * to the inner product innerProd().
     *
     * After calling this routine, \c X will be orthogonal to each of the <tt>Q[i]</tt>.
     *
     @param[in,out] X The multivector to be modified.<br>
       On output, the columns of \c X will be orthogonal to each <tt>Q[i]</tt>, satisfying
       \f[
       X_{out} = X_{in} - \sum_i Q[i] \langle Q[i], X_{in} \rangle
       \f]

     @param[in,out] MX The image of \c X under the inner product operator \c Op. 
       If \f$ MX != 0\f$: On input, this is expected to be consistent with \c Op . X. On output, this is updated consistent with updates to \c X.
       If \f$ MX == 0\f$ or \f$ Op == 0\f$: \c MX is not referenced.

     @param[out] C The coefficients of \c X in the bases <tt>Q[i]</tt>. If <tt>C[i]</tt> is a non-null pointer 
       and <tt>C[i]</tt> matches the dimensions of \c X and <tt>Q[i]</tt>, then the coefficients computed during the orthogonalization
       routine will be stored in the matrix <tt>C[i]</tt>, similar to calling
       \code
          innerProd(Q[i], X, C[i]);
       \endcode
       If <tt>C[i]</tt> points to a MatrixEigenDense with size
       inconsistent with \c X and \c <tt>Q[i]</tt>, then a std::invalid_argument
       exception will be thrown. Otherwise, if <tt>C.size() < i</tt> or
       <tt>C[i]</tt> is a null pointer, the caller will not have access to the
       computed coefficients.

     @param[in] Q A list of multivector bases specifying the subspaces to be orthogonalized against, satisfying 
     \f[
        \langle Q[i], Q[j] \rangle = I \quad\textrm{if}\quad i=j
     \f]
     and
     \f[
        \langle Q[i], Q[j] \rangle = 0 \quad\textrm{if}\quad i \neq j\ .
     \f]
    
     @param MQ
    */
    void projectMat (
              MV& X,
              std::vector<SmartPtr<const MV> >  Q,
              std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C,
                  //= Teuchos::tuple(SmartPtr<MatrixEigenDense<ScalarType> >(_smPtrNull)),
              SmartPtr<MV> MX,
              std::vector<SmartPtr<const MV> > MQ
                  //= Teuchos::tuple(SmartPtr<const MV>(_smPtrNull))
           ) const;


    /*! \brief This method takes a multivector \c X and attempts to compute an orthonormal basis for \f$colspan(X)\f$, with respect to innerProd().
     *
     * This method does not compute an upper triangular coefficient matrix \c B.
     *
     * This routine returns an integer \c rank stating the rank of the computed basis. If \c X does not have full rank and the normalize() routine does 
     * not attempt to augment the subspace, then \c rank may be smaller than the number of columns in \c X. In this case, only the first \c rank columns of 
     * output \c X and first \c rank rows of \c B will be valid.
     *  
     * The method attempts to find a basis with dimension equal to the number of columns in \c X. It does this by augmenting linearly dependent 
     * vectors in \c X with random directions. A finite number of these attempts will be made; therefore, it is possible that the dimension of the 
     * computed basis is less than the number of vectors in \c X.
     *
     @param[in,out] X The multivector to be modified.<br>
       On output, the first \c rank columns of \c X satisfy
       \f[
          \langle X[i], X[j] \rangle = \delta_{ij}\ .
       \f]
       Also, 
       \f[
          X_{in}(1:m,1:n) = X_{out}(1:m,1:rank) B(1:rank,1:n)
       \f]
       where \c m is the number of rows in \c X and \c n is the number of columns in \c X.

     @param[in,out] MX The image of \c X under the inner product operator \c Op. 
       If \f$ MX != 0\f$: On input, this is expected to be consistent with \c Op . X. On output, this is updated consistent with updates to \c X.
       If \f$ MX == 0\f$ or \f$ Op == 0\f$: \c MX is not referenced.

     @param[out] B The coefficients of the original \c X with respect to the computed basis. If \c B is a non-null pointer and \c B matches the dimensions of \c B, then the
     coefficients computed during the orthogonalization routine will be stored in \c B, similar to calling 
       \code
          innerProd(Xout, Xin, B);
       \endcode
     If \c B points to a MatrixEigenDense with size inconsistent with \c X, then a std::invalid_argument exception will be thrown. Otherwise, if \c B is null, the caller will not have
     access to the computed coefficients. This matrix is not necessarily triangular (as in a QR factorization); see the documentation of specific orthogonalization managers.<br>
     In general, \c B has no non-zero structure.

     @return Rank of the basis computed by this method, less than or equal to the number of columns in \c X. This specifies how many columns in the returned \c X and rows in the returned \c B are valid.
    */
    int normalizeMat (
              MV& X,
              SmartPtr<MatrixEigenDense<ScalarType> > B = _smPtrNull,
              SmartPtr<MV> MX  = _smPtrNull
           ) const;

    /*! \brief Given a set of bases <tt>Q[i]</tt> and a multivector \c X, this method computes an orthonormal basis for \f$colspan(X) - \sum_i colspan(Q[i])\f$.
     *
     *  This routine returns an integer \c rank stating the rank of the computed basis. If the subspace \f$colspan(X) - \sum_i colspan(Q[i])\f$ does not 
     *  have dimension as large as the number of columns of \c X and the orthogonalization manager doe not attempt to augment the subspace, then \c rank 
     *  may be smaller than the number of columns of \c X. In this case, only the first \c rank columns of output \c X and first \c rank rows of \c B will 
     *  be valid.
     *
     * The method attempts to find a basis with dimension the same as the number of columns in \c X. It does this by augmenting linearly dependent 
     * vectors with random directions. A finite number of these attempts will be made; therefore, it is possible that the dimension of the 
     * computed basis is less than the number of vectors in \c X.
     *
     @param[in,out] X The multivector to be modified.<br>
       On output, the first \c rank columns of \c X satisfy
       \f[
            \langle X[i], X[j] \rangle = \delta_{ij} \quad \textrm{and} \quad \langle X, Q[i] \rangle = 0\ .
       \f]
       Also, 
       \f[
          X_{in}(1:m,1:n) = X_{out}(1:m,1:rank) B(1:rank,1:n) + \sum_i Q[i] C[i]
       \f]
       where \c m is the number of rows in \c X and \c n is the number of columns in \c X.

     @param[in,out] MX The image of \c X under the inner product operator \c Op. 
       If \f$ MX != 0\f$: On input, this is expected to be consistent with \c Op . X. On output, this is updated consistent with updates to \c X.
       If \f$ MX == 0\f$ or \f$ Op == 0\f$: \c MX is not referenced.

     @param[out] C The coefficients of \c X in the <tt>Q[i]</tt>. If <tt>C[i]</tt> is a non-null pointer 
       and <tt>C[i]</tt> matches the dimensions of \c X and <tt>Q[i]</tt>, then the coefficients computed during the orthogonalization
       routine will be stored in the matrix <tt>C[i]</tt>, similar to calling
       \code
          innerProd(Q[i], X, C[i]);
       \endcode
       If <tt>C[i]</tt> points to a MatrixEigenDense with size
       inconsistent with \c X and \c <tt>Q[i]</tt>, then a std::invalid_argument
       exception will be thrown. Otherwise, if <tt>C.size() < i</tt> or
       <tt>C[i]</tt> is a null pointer, the caller will not have access to the
       computed coefficients.

     @param[out] B The coefficients of the original \c X with respect to the computed basis. If \c B is a non-null pointer and \c B matches the dimensions of \c B, then the coefficients computed during the orthogonalization routine will be stored in \c B, similar to calling 
       \code
          innerProd(Xout, Xin, B);
       \endcode
     If \c B points to a MatrixEigenDense with size inconsistent with \c X, then a std::invalid_argument exception will be thrown. Otherwise, if \c B is null, the caller will not have
     access to the computed coefficients. This matrix is not necessarily triangular (as in a QR factorization); see the documentation of specific orthogonalization managers.<br>
     In general, \c B has no non-zero structure.

     @param[in] Q A list of multivector bases specifying the subspaces to be orthogonalized against, satisfying 
     \f[
        \langle Q[i], Q[j] \rangle = I \quad\textrm{if}\quad i=j
     \f]
     and
     \f[
        \langle Q[i], Q[j] \rangle = 0 \quad\textrm{if}\quad i \neq j\ .
     \f]

     @param MQ
              
     @return Rank of the basis computed by this method, less than or equal to the number of columns in \c X. This specifies how many columns in the returned \c X and rows in the returned \c B are valid.

    */
    int projectAndNormalizeMat (
              MV& X,
              std::vector<SmartPtr<const MV> >  Q,
              std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C,
                  //= Teuchos::tuple(SmartPtr<MatrixEigenDense<ScalarType> >(_smPtrNull)),
                  SmartPtr<MatrixEigenDense<ScalarType> > B, //                  = _smPtrNull,
              SmartPtr<MV> MX  = _smPtrNull,
              std::vector<SmartPtr<const MV> > MQ = std::vector<SmartPtr<const MV> >(1,_smPtrNull)
                  //= Teuchos::tuple(SmartPtr<const MV>(_smPtrNull))
           ) const;

    //@}

    //! @name Error methods
    //@{ 

    /*! \brief This method computes the error in orthonormality of a multivector, measured
     * as the Frobenius norm of the difference <tt>innerProd(X,Y) - I</tt>.
     *  The method has the option of exploiting a caller-provided \c MX.
     */
    typename NumTraits<ScalarType>::RealScalar
    orthonormErrorMat(const MV& X, SmartPtr<const MV> MX = _smPtrNull) const;

    /*! \brief This method computes the error in orthogonality of two multivectors, measured
     * as the Frobenius norm of <tt>innerProd(X,Y)</tt>.
     *  The method has the option of exploiting a caller-provided \c MX.
     */
    typename NumTraits<ScalarType>::RealScalar
    orthogErrorMat(
          const MV& X,
          const MV& Y,
          SmartPtr<const MV> MX = _smPtrNull,
          SmartPtr<const MV> MY = _smPtrNull
       ) const;

    //@}

  private:
    
    MagnitudeType eps_;
    bool debug_;
  
    //! Routine to find an orthogonal/orthonormal basis
    int findBasis(MV& X, SmartPtr<MV> MX,
                   std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C,
                   SmartPtr<MatrixEigenDense<ScalarType> > B,
                   std::vector<SmartPtr<const MV> > Q,
                   bool normalize_in) const;
  };


  //=============================================================================================
  // Constructor
  template<class ScalarType, class MV, class OP>
  SVQBOrthoManager<ScalarType,MV,OP>::SVQBOrthoManager(SmartPtr<const OP> Op, bool debug)
    : MatOrthoManager<ScalarType,MV,OP>(Op), dbgstr("                    *** "), debug_(debug) {

      eps_ = theEpsilon;
    if (debug_) {
      std::cout << "eps_ == " << eps_ << std::endl;
    }
  }

  //=============================================================================================
  // Compute the distance from orthonormality
  template<class ScalarType, class MV, class OP>
  typename NumTraits<ScalarType>::RealScalar
  SVQBOrthoManager<ScalarType,MV,OP>::orthonormErrorMat(const MV& X, SmartPtr<const MV> MX) const {
    const ScalarType ONE = SCT::one();
    int rank = MVT::getNumberVecs(X);
    MatrixEigenDense<ScalarType> xTx(rank,rank);
    MatOrthoManager<ScalarType,MV,OP>::innerProdMat(X,X,xTx,MX,MX);
    for (int i=0; i<rank; i++) {
      xTx.coeffRef(i,i) -= ONE;
    }
    return xTx.normFrobenius();
  }

  //=============================================================================================
  // Compute the distance from orthogonality
  template<class ScalarType, class MV, class OP>
  typename NumTraits<ScalarType>::RealScalar
  SVQBOrthoManager<ScalarType,MV,OP>::orthogErrorMat(
          const MV& X,
          const MV& Y,
          SmartPtr<const MV> MX,
          SmartPtr<const MV> MY
     ) const {
    int r1 = MVT::getNumberVecs(X);
    int r2 = MVT::getNumberVecs(Y);
    MatrixEigenDense<ScalarType> xTx(r1,r2);
    MatOrthoManager<ScalarType,MV,OP>::innerProdMat(X,Y,xTx,MX,MY);
    return xTx.normFrobenius();
  }

  //=============================================================================================
  template<class ScalarType, class MV, class OP>
  void SVQBOrthoManager<ScalarType, MV, OP>::projectMat(
          MV& X,
          std::vector<SmartPtr<const MV> >  Q,
          std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C,
          SmartPtr<MV> MX,
          std::vector<SmartPtr<const MV> > MQ) const {
    (void)MQ;
    findBasis(X,MX,C,_smPtrNull,Q,false);
  }

  //=============================================================================================
  // Find an Op-orthonormal basis for span(X), with rank numvectors(X)
  template<class ScalarType, class MV, class OP>
  int SVQBOrthoManager<ScalarType, MV, OP>::normalizeMat(
          MV& X,
          SmartPtr<MatrixEigenDense<ScalarType> > B,
          SmartPtr<MV> MX) const {
    std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C;
    std::vector<SmartPtr<const MV> > Q;
    return findBasis(X,MX,C,B,Q,true);
  }

  //=============================================================================================
  // Find an Op-orthonormal basis for span(X) - span(W)
  template<class ScalarType, class MV, class OP>
  int SVQBOrthoManager<ScalarType, MV, OP>::projectAndNormalizeMat(
          MV& X,
          std::vector<SmartPtr<const MV> >  Q,
          std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C,
          SmartPtr<MatrixEigenDense<ScalarType> > B,
          SmartPtr<MV> MX,
          std::vector<SmartPtr<const MV> > MQ) const {
    (void)MQ;
    return findBasis(X,MX,C,B,Q,true);
  }


  //===============================================================================================
  // Find an Op-orthonormal basis for span(X), with the option of extending the subspace so that 
  // the rank is numvectors(X)
  // 
  // Tracking the coefficients (C[i] and B) for this code is complicated by the fact that the loop
  // structure looks like
  // do
  //    project
  //    do
  //       ortho
  //    end
  // end
  // However, the recurrence for the coefficients is not complicated:
  // B = I
  // C = 0
  // do 
  //    project yields newC
  //    C = C + newC*B
  //    do 
  //       ortho yields newR
  //       B = newR*B
  //    end
  // end
  // This holds for each individual C[i] (which correspond to the list of bases we are orthogonalizing
  // against).
  //===============================================================================================
  template<class ScalarType, class MV, class OP>
  int SVQBOrthoManager<ScalarType, MV, OP>::findBasis(
                MV& X, SmartPtr<MV> MX,
                std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C,
                SmartPtr<MatrixEigenDense<ScalarType> > B,
                std::vector<SmartPtr<const MV> > Q,
                bool normalize_in) const {

    const ScalarType ONE  = SCT::one();
    const MagnitudeType MONE = SCTM::one();
    const MagnitudeType ZERO = SCTM::zero();

    int numGS = 0,
        numSVQB = 0,
        numRand = 0;

    // get sizes of X,MX
    int xc = MVT::getNumberVecs(X);
    int xr = MVT::getVecLength(X);

    // get sizes of Q[i]
    int nq = Q.size();
    int qr = (nq == 0) ? 0 : MVT::getVecLength(*Q[0]);
    int qsize = 0;
    std::vector<int> qcs(nq);
    for (int i=0; i<nq; i++) {
      qcs[i] = MVT::getNumberVecs(*Q[i]);
      qsize += qcs[i];
    }

    if (normalize_in == true &&  qsize + xc > xr) {
      // not well-posed
      internalEigenSolver::testErrorEigenProblem(true,
                          "xlifepp::SVQBOrthoManager::findBasis(): Orthogonalization constraints not feasible");
    }

    // try to short-circuit as early as possible
    if (normalize_in == false &&  (qsize == 0 || xc == 0)) {
      // nothing to do
      return 0;
    }
    else if (normalize_in == true &&  (xc == 0 || xr == 0)) {
      // normalize requires X not empty
      internalEigenSolver::testErrorEigenProblem(true,
                          "xlifepp::SVQBOrthoManager::findBasis(): X must be non-empty");
    }

    // check that Q matches X
    internalEigenSolver::testErrorEigenProblem(qsize != 0 &&  qr != xr ,
                        "xlifepp::SVQBOrthoManager::findBasis(): Size of X not consistant with size of Q");

    /* If we don't have enough C, expanding it creates null references
     * If we have too many, resizing just throws away the later ones
     * If we have exactly as many as we have Q, this call has no effect
     */
    C.resize(nq);
    std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > newC(nq);
    // check the size of the C[i] against the Q[i] and consistency between Q[i]
    for (int i=0; i<nq; i++) {
      // check size of Q[i]
      internalEigenSolver::testErrorEigenProblem(MVT::getVecLength(*Q[i]) != qr,
                          "xlifepp::SVQBOrthoManager::findBasis(): Size of Q not mutually consistant");
      internalEigenSolver::testErrorEigenProblem(qr < qcs[i],
                          "xlifepp::SVQBOrthoManager::findBasis(): Q has less rows than columns");
      // check size of C[i]
      if (C[i] == _smPtrNull) {
        C[i] = SmartPtr<MatrixEigenDense<ScalarType> >(new MatrixEigenDense<ScalarType>(qcs[i],xc));
      }
      else {
        internalEigenSolver::testErrorEigenProblem(C[i]->numOfRows() != qcs[i] || C[i]->numOfCols() != xc,
                            "xlifepp::SVQBOrthoManager::findBasis(): Size of Q not consistant with C");
      }
      // clear C[i]
      C[i]->putScalar(ZERO);
      newC[i] = SmartPtr<MatrixEigenDense<ScalarType> >(new MatrixEigenDense<ScalarType>(*C[i]));
    }


    //========================================================================
    // Allocate necessary storage
    // C were allocated above
    // Allocate MX and B (if necessary)
    // Set B = I
    if (normalize_in == true) {
      if (B == _smPtrNull) {
        B = SmartPtr<MatrixEigenDense<ScalarType> >(new MatrixEigenDense<ScalarType>(xc,xc));
      }
      internalEigenSolver::testErrorEigenProblem(B->numOfRows() != xc || B->numOfCols() != xc,
                          "xlifepp::SVQBOrthoManager::findBasis(): Size of B not consistant with X");
      // set B to I
      B->putScalar(ZERO);
      for (int i=0; i<xc; i++) {
        B->coeffRef(i,i) = MONE;
      }
    }
    /* *****************************************
     *  If hasOp_ == false, DO NOT MODIFY MX  *
     ****************************************** 
     * if Op==null, MX == X (via pointer)
     * Otherwise, either the user passed in MX or we will allocate and compute it
     *
     * workX will be a multivector of the same size as X, used to perform X*S when normalizing
     */
    SmartPtr<MV> workX;
    if (normalize_in) {
      workX = MVT::clone(X,xc);
    }
    if (this->hasOp_) {
      if (MX == _smPtrNull) {
        // we need to allocate space for MX
        MX = MVT::clone(X,xc);
        OPT::apply(*(this->Op_),X,*MX);
        this->OpCounter_ += MVT::getNumberVecs(X);
      }
    }
    else {
        MX = smartPtrFromRef(&X);
    }
    std::vector<ScalarType> normX(xc), invnormX(xc);
    MatrixEigenDense<ScalarType> XtMX(xc,xc), workU(1,1), eigvec(xc,xc);

    /**********************************************************************
     * allocate storage for eigenvectors,eigenvalues of X^T Op X, and for
     * the work space needed to compute this xc-by-xc eigendecomposition
     **********************************************************************/
    std::vector<ScalarType> work;
    std::vector<MagnitudeType> lambda, lambdahi, rwork;
    if (normalize_in) {
      // size of lambda is xc
      lambda.resize(xc);
      lambdahi.resize(xc);
      workU.reshape(xc,xc);
    }

    // test sizes of X,MX
    int mxc = (this->hasOp_) ? MVT::getNumberVecs(*MX) : xc;
    int mxr = (this->hasOp_) ? MVT::getVecLength(*MX)  : xr;
    internalEigenSolver::testErrorEigenProblem(xc != mxc || xr != mxr,
                        "xlifepp::SVQBOrthoManager::findBasis(): Size of X not consistant with MX");

    // sentinel to continue the outer loop (perform another projection step)
    bool doGramSchmidt = true;          
    // variable for testing orthonorm/orthog 
    MagnitudeType tolerance = MONE/SCTM::squareroot(eps_);

    // outer loop
    while (doGramSchmidt)
    {
      //=================================================================================
      // perform projection
      if (qsize > 0) {

        numGS++;

        // Compute the norms of the vectors
        {
          std::vector<MagnitudeType> normX_mag(xc);
          MatOrthoManager<ScalarType,MV,OP>::normMat(X,normX_mag,MX);
          for (int i=0; i<xc; ++i) {
            normX[i] = normX_mag[i];
            invnormX[i] = (normX_mag[i] == ZERO) ? ZERO : MONE/normX_mag[i]; 
          }
        }
        // normalize the vectors
        MVT::mvScale(X,invnormX);
        if (this->hasOp_) {
          MVT::mvScale(*MX,invnormX);
        }
        // check that vectors are normalized now
        if (debug_) {
          std::vector<MagnitudeType> nrm2(xc);
          std::cout << dbgstr << "max post-scale norm: (with/without MX) : ";
          MagnitudeType maxpsnw = ZERO, maxpsnwo = ZERO;
          MatOrthoManager<ScalarType,MV,OP>::normMat(X,nrm2,MX);
          for (int i=0; i<xc; i++) {
            maxpsnw = (nrm2[i] > maxpsnw ? nrm2[i] : maxpsnw);
          }
          this->norm(X,nrm2);
          for (int i=0; i<xc; i++) {
            maxpsnwo = (nrm2[i] > maxpsnwo ? nrm2[i] : maxpsnwo);
          }
          std::cout << "(" << maxpsnw << "," << maxpsnwo << ")" << std::endl;
        }
        // project the vectors onto the Qi
        for (int i=0; i<nq; i++) {
          MatOrthoManager<ScalarType,MV,OP>::innerProdMat(*Q[i],X,*newC[i],_smPtrNull,MX);
        }
        // remove the components in Qi from X
        for (int i=0; i<nq; i++) {
          MVT::mvTimesMatAddMv(-ONE,*Q[i],*newC[i],ONE,X);
        }
        // un-scale the vectors 
        MVT::mvScale(X,normX);
        // Recompute the vectors in MX
        if (this->hasOp_) {
          OPT::apply(*(this->Op_),X,*MX);
          this->OpCounter_ += MVT::getNumberVecs(X);
        }

        //          
        // Compute largest column norm of 
        //            ( C[0]  )
        //        C = ( ....  )
        //            (C[nq-1])
        MagnitudeType maxNorm = ZERO;
        for  (int j=0; j<xc; j++) {
          MagnitudeType sum = ZERO;
          for (int k=0; k<nq; k++) {
            for (int i=0; i<qcs[k]; i++) {
              sum += SCT::magnitude((*newC[k]).coeff(i,j))*SCT::magnitude((*newC[k]).coeff(i,j));
            }
          }
          maxNorm = (sum > maxNorm) ? sum : maxNorm;
        }
              
        // do we perform another GS?
        if (maxNorm < 0.36) {
          doGramSchmidt = false;
        }

        // unscale newC to reflect the scaling of X
        for (int k=0; k<nq; k++) {
          for (int j=0; j<xc; j++) {
            for (int i=0; i<qcs[k]; i++) {
              (*newC[k]).coeffRef(i,j) *= normX[j];
            }
          }
        }
        // accumulate into C
        if (normalize_in) {
          // we are normalizing
          for (int i=0; i<nq; i++) {
            //info = C[i]->multiply(Teuchos::NO_TRANS,Teuchos::NO_TRANS,ONE,*newC[i],*B,ONE);
              MatrixEigenDense<ScalarType> tmpCi(C[i]->numOfRows(),C[i]->numOfCols());
              multMatMat(*newC[i], *B, tmpCi);
              (*C[i]) += tmpCi;
          }
        }
        else {
          // not normalizing
          for (int i=0; i<nq; i++) {
            (*C[i]) += *newC[i];
          }
        }
      }
      else { // qsize == 0... don't perform projection
        // don't do any more outer loops; all we need is to call the normalize code below
        doGramSchmidt = false;
      }

      //=============================================================================
      // perform normalization
      if (normalize_in) {

        MagnitudeType condT = tolerance;
        
        while (condT >= tolerance) {

          numSVQB++;

          // compute X^T Op X
          MatOrthoManager<ScalarType,MV,OP>::innerProdMat(X,X,XtMX,MX,MX);

          // compute scaling matrix for XtMX: D^{.5} and D^{-.5} (D-half  and  D-half-inv)
          std::vector<MagnitudeType> Dh(xc), Dhi(xc);
          for (int i=0; i<xc; i++) {
            Dh[i]  = SCT::magnitude(SCT::squareroot(XtMX.coeff(i,i)));
            Dhi[i] = (Dh[i] == ZERO ? ZERO : MONE/Dh[i]);
          }
          // scale XtMX:   S = D^{-.5} * XtMX * D^{-.5}
          for (int i=0; i<xc; i++) {
            for (int j=0; j<xc; j++) {
              XtMX.coeffRef(i,j) *= Dhi[i]*Dhi[j];
            }
          }

          // compute the eigenvalue decomposition of S=U*Lambda*U^T (using upper part)
          ComputationInfo info = _noConvergence;
          SelfAdjointEigenSolver<MatrixEigenDense<ScalarType> > heev(XtMX);
          //lapack.HEEV('V', 'U', xc, XtMX.values(), XtMX.stride(),& lambda[0],& work[0], work.size(),& rwork[0],& info);
          info = heev.info();
          std::stringstream os;
          os << "xlifepp::SVQBOrthoManager::findBasis(): Error code " << info << " from SelfAdjointEigenSolver";
          if (info != _success) { // Stop projection and normalization
            internalEigenSolver::testWarningEigenProblem(info != _success,os.str());
            break;
          }

          // remember, HEEV orders the eigenvalues from smallest to largest
          // examine condition number of Lambda, compute Lambda^{-.5}
//          MagnitudeType maxLambda = lambda[xc-1],
//                        minLambda = lambda[0];
          lambda = heev.eigenvalues();
          eigvec = heev.eigenvectors();
          workU = heev.eigenvectors();

          if (debug_) {
            std::cout << dbgstr << "eigenvalues of XtMX: (";
            for (int i=0; i<xc-1; i++) {
              std::cout << lambda[i] << ",";
            }
            std::cout << lambda[xc-1] << ")" << std::endl;
          }

          MagnitudeType maxLambda = lambda[xc-1],
                        minLambda = lambda[0];
          int iZeroMax = -1;
          for (int i=0; i<xc; i++) {
            if (lambda[i] <= 10*eps_*maxLambda) {  // 10*eps_*maxLambda // finish: this was eps_*eps_*maxLambda
              iZeroMax = i;
              lambda[i]  = ZERO;
              lambdahi[i] = ZERO;
            }
            /*
            else if (lambda[i] < eps_*maxLambda) {
              lambda[i]  = SCTM::squareroot(eps_*maxLambda);
              lambdahi[i] = MONE/lambda[i];
            }
            */
            else {
              lambda[i] = SCTM::squareroot(lambda[i]);
              lambdahi[i] = MONE/lambda[i];
            }
          }

          // compute X * D^{-.5} * U * Lambda^{-.5} and new Op*X
          //
          // copy X into workX
          std::vector<int> ind(xc);
          for (int i=0; i<xc; i++) {ind[i] = i;}
          MVT::setBlock(X,ind,*workX);
          //
          // compute D^{-.5}*U*Lambda^{-.5} into workU
          for (int j=0; j<xc; j++) {
            for (int i=0; i<xc; i++) {
              workU.coeffRef(i,j) *= Dhi[i]*lambdahi[j];
            }
          }

          // compute workX * workU into X
          MVT::mvTimesMatAddMv(ONE,*workX,workU,ZERO,X);
          //
          // note, it seems important to apply Op exactly for large condition numbers.
          // for small condition numbers, we can update MX "implicitly"
          // this trick reduces the number of applications of Op
          if (this->hasOp_) {
            if (maxLambda >= tolerance * minLambda) {
              // explicit update of MX
              OPT::apply(*(this->Op_),X,*MX);
              this->OpCounter_ += MVT::getNumberVecs(X);
            }
            else {
              // implicit update of MX
              // copy MX into workX
              MVT::setBlock(*MX,ind,*workX);
              //
              // compute workX * workU into MX
              MVT::mvTimesMatAddMv(ONE,*workX,workU,ZERO,*MX);
            }
          }

          // accumulate new B into previous B
          // B = Lh * U^H * Dh * B
          for (int j=0; j<xc; j++) {
            for (int i=0; i<xc; i++) {
              workU.coeffRef(i,j) = Dh[i] * B->coeff(i,j);
            }
          }

          //info = B->multiply(Teuchos::CONJ_TRANS,Teuchos::NO_TRANS,ONE,XtMX,workU,ZERO);
          multMatMat(conj(transpose(eigvec)), workU, *B);

          for (int j=0; j<xc ;j++) {
            for (int i=0; i<xc; i++) {
              B->coeffRef(i,j) *= lambda[i];
            }
          }

          // check iZeroMax (rank indicator)
          if (iZeroMax >= 0) {
            if (debug_) {
              std::cout << dbgstr << "augmenting multivec with " << iZeroMax+1 << " random directions" << std::endl;
            }

            numRand++;
            // put random info in the first iZeroMax+1 vectors of X,MX
            std::vector<int> ind2(iZeroMax+1);
            for (int i=0; i<iZeroMax+1; i++) {
              ind2[i] = i;
            }
            SmartPtr<MV> Xnull,MXnull;
            Xnull = MVT::cloneViewNonConst(X,ind2);
            MVT::mvRandom(*Xnull);
            if (this->hasOp_) {
              MXnull = MVT::cloneViewNonConst(*MX,ind2);
              OPT::apply(*(this->Op_),*Xnull,*MXnull);
              this->OpCounter_ += MVT::getNumberVecs(*Xnull);
              MXnull = _smPtrNull;
            }
            Xnull = _smPtrNull;
            condT = tolerance;
            doGramSchmidt = true;
            break; // break from while(condT > tolerance)
          }

          condT = SCTM::magnitude(maxLambda / minLambda);
          if (debug_) {
            std::cout << dbgstr << "condT: " << condT << ", tolerance = " << tolerance << std::endl;
          }
      
          // if multiple passes of SVQB are necessary, then pass through outer GS loop again    
          if ((doGramSchmidt == false) &&  (condT > SCTM::squareroot(tolerance))) {
            doGramSchmidt = true;
          }

        } // end while (condT >= tolerance)
      }
      // end if(normalize_in)
       
    } // end while (doGramSchmidt)

    if (debug_) {
      std::cout << dbgstr << "(numGS,numSVQB,numRand)                : " 
           << "(" << numGS 
           << "," << numSVQB 
           << "," << numRand 
           << ")" << std::endl;
    }
    return xc;
  }
} // namespace xlifepp

#endif /* XLIFEPP_SVQB_ORTHOMANAGER_HPP */
