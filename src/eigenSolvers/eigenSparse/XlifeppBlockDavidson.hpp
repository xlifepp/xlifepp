/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppBlockDavidson.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013

  \brief Implementation of the block Davidson method
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_BLOCK_DAVIDSON_HPP
#define XLIFEPP_BLOCK_DAVIDSON_HPP

#include "utils.h"
#include "../eigenCore/eigenCore.hpp"
#include "EigenTestError.hpp"
#include "XlifeppEigenTypes.hpp"

#include "XlifeppEigenSolver.hpp"
#include "XlifeppMultiVecTraits.hpp"
#include "XlifeppOperatorTraits.hpp"

#include "XlifeppSortManager.hpp"
#include "XlifeppMatOrthoManager.hpp"
#include "XlifeppSolverUtils.hpp"
#include "XlifeppBasicOutputManager.hpp"

namespace xlifepp {

//! @name BlockDavidson Structures 
//@{ 
/*! \brief Structure to contain pointers to BlockDavidson state variables.
 *
 * This struct is utilized by BlockDavidson::initialize() and BlockDavidson::getState().
 */
template <class ScalarType, class MV>
struct BlockDavidsonState {
  /*! \brief The current dimension of the solver.
   *
   * This should always be equal to BlockDavdison::getCurSubspaceDim()
   */
  int curDim;
  /*! \brief The basis for the Krylov space.
   *
   * V has BlockDavidson::getMaxSubspaceDim() vectors, but only the first \c curDim are valid.
   */
  SmartPtr<const MV> V;
  //! The current eigenvectors.
  SmartPtr<const MV> X;
  //! The image of the current eigenvectors under K.
  SmartPtr<const MV> KX;
  //! The image of the current eigenvectors under M, or _smPtrNull if M was not specified.
  SmartPtr<const MV> MX;
  //! The current residual vectors
  SmartPtr<const MV> R;
  /*! \brief The current preconditioned residual vectors.
   *
   *  H is a pointer into V, and is only useful when BlockDavidson::iterate() throw a BlockDavidsonOrthoFailure exception.
   */
  SmartPtr<const MV> H;
  //! The current Ritz values. This vector is a copy of the internal data.
  SmartPtr<const std::vector<typename NumTraits<ScalarType>::RealScalar> >  T;
  /*! \brief The current projected K matrix.
   *
   * KK is of order BlockDavidson::getMaxSubspaceDim(), but only the principal submatrix of order \c curDim is meaningful. It is Hermitian in memory.
   *
   */
  SmartPtr<const MatrixEigenDense<ScalarType> > KK;
  BlockDavidsonState() : curDim(0), V(_smPtrNull),
                         X(_smPtrNull), KX(_smPtrNull), MX(_smPtrNull),
                         R(_smPtrNull), H(_smPtrNull),
                         T(_smPtrNull), KK(_smPtrNull) {}
};
//@}


/*!
\class BlockDavidson

\brief This class implements a Block Davidson iteration, a preconditioned iteration for solving linear Hermitian eigenproblems.

This method is described in <em>A Comparison of EigenSolvers for
Large-scale 3D Modal Analysis Using AMG-Preconditioned Iterative
Methods</em>, P. Arbenz, U. L. Hetmaniuk, R. B. Lehoucq, R. S.
Tuminaro, Internat. J. for Numer. Methods Engrg., 64, pp. 204-236
(2005)

*/
template <class ScalarType, class MV, class OP>
class BlockDavidson : public EigenSolver<ScalarType,MV,OP> {
public:
typedef typename NumTraits<ScalarType>::Scalar Scalar;
typedef MatrixEigenDense<Scalar> MatrixDense;

  //! @name Constructor/Destructor
  //@{   
  /*! \brief BlockDavidson constructor with eigenproblem, solver utilities, and parameter list of solver options.
   *
   * This constructor takes pointers required by the eigensolver, in addition
   * to a parameter list of options for the eigensolver. These options include the following:
   *   - "Block Size" - an \c int specifying the block size used by the algorithm. This can also be specified using the setBlockSize() method.
   *   - "Num Blocks" - an \c int specifying the maximum number of blocks allocated for the solver basis.
   */
  BlockDavidson(const SmartPtr<EigenProblem<ScalarType,MV,OP> >& problem,
                 const SmartPtr<SortManager<typename NumTraits<ScalarType>::RealScalar> >& sorter,
                 const SmartPtr<OutputManager<ScalarType> >&      printer,
                 const SmartPtr<StatusTest<ScalarType,MV,OP> >&   tester,
                 const SmartPtr<MatOrthoManager<ScalarType,MV,OP> >&       ortho,
                 Parameters &params
              );
  
  //! BlockDavidson destructor.
  virtual ~BlockDavidson();
  //@}


  //! @name Solver methods
  //@{ 
  /*! \brief This method performs BlockDavidson iterations until the status
   * test indicates the need to stop or an error occurs (in which case, an 
   * appropriate exception is thrown).
   *
   * iterate() will first determine whether the solver is uninitialized; if
   * not, it will call initialize(). After
   * initialization, the solver performs block Davidson iterations until the
   * status test evaluates as ::_passed, at which point the method returns to
   * the caller. 
   *
   * The block Davidson iteration proceeds as follows:
   * -# The current residual (R) is preconditioned to form H
   * -# H is orthogonalized against the auxiliary vectors and the previous basis vectors, and made orthonormal.
   * -# The current basis is expanded with H and used to project the problem matrix.
   * -# The projected eigenproblem is solved, and the desired eigenvectors and eigenvalues are selected.
   * -# These are used to form the new eigenvector estimates (X).
   * -# The new residual (R) is formed.
   *
   * The status test is queried at the beginning of the iteration.
   */
  void iterate();

  /*! \brief Initialize the solver to an iterate, optionally providing the
   * current basis and projected problem matrix, the current Ritz vectors and values,
   * and the current residual.
   *
   * The BlockDavidson eigensolver contains a certain amount of state,
   * including the current Krylov basis, the current eigenvectors, 
   * the current residual, etc. (see getState())
   *
   * initialize() gives the user the opportunity to manually set these,
   * although this must be done with caution, as the validity of the
   * user input will not be checked.
   *
   * Only the first <tt>newstate.curDim</tt> columns of <tt>newstate.V</tt>
   * and <tt>newstate.KK</tt> and the first <tt>newstate.curDim</tt> rows of 
   * <tt>newstate.KK</tt> will be used.
   *
   * If <tt>newstate.V == getState().V</tt>, then the data is not copied. The
   * same holds for <tt>newstate.KK</tt>, <tt>newstate.X</tt>,
   * <tt>newstate.KX</tt>, <tt>newstate.MX</tt>, and <tt>newstate.R</tt> Only the
   * upper triangular half of <tt>newstate.KK</tt> is used to initialize the
   * state of the solver.
   *
   * \post 
   * isInitialized() == \c true (see post-conditions of isInitialize())
   *
   * The user has the option of specifying any component of the state using
   * initialize(). However, these arguments are assumed to match the
   * post-conditions specified under isInitialized(). Any component of the
   * state (i.e., KX) not given to initialize() will be generated.
   *
   * \note for any pointer in \c newstate which directly points to the multivectors in 
   * the solver, the data is not copied.
   */
  void initialize(BlockDavidsonState<ScalarType,MV> newstate);

  /*! \brief Initialize the solver with the initial vectors from the eigenproblem
   *  or random data.
   */
  void initialize();

  /*! \brief Indicates whether the solver has been initialized or not.
   *
   * \return bool indicating the state of the solver.
   * \post
   * If isInitialized() == \c true:
   *    - getCurSubspaceDim() > 0 and is a multiple of getBlockSize()
   *    - the first getCurSubspaceDim() vectors of V are orthogonal to auxiliary vectors and have orthonormal columns
   *    - the principal submatrix of order getCurSubspaceDim() of KK contains the project eigenproblem matrix
   *    - X contains the Ritz vectors with respect to the current Krylov basis
   *    - T contains the Ritz values with respect to the current Krylov basis
   *    - KX == Op*X
   *    - MX == M*X if M != _smPtrNull\n
   *      Otherwise, MX == _smPtrNull
   *    - R contains the residual vectors with respect to X
   */
  bool isInitialized() const;

  /*! \brief Get access to the current state of the eigensolver.
   * 
   * The data is only valid if isInitialized() == \c true. 
   *
   * The data for the preconditioned residual is only meaningful in the
   * scenario that the solver throws a BlockDavidsonRitzFailure exception
   * during iterate().
   *
   * \returns A BlockDavidsonState object containing const pointers to the current
   * solver state. Note, these are direct pointers to the multivectors; they are not
   * pointers to views of the multivectors.
   */
  BlockDavidsonState<ScalarType,MV> getState() const;
  //@}


  //! @name Status methods
  //@{
  //! \brief Get the current iteration count.
  int getNumIters() const;

  //! \brief Reset the iteration count.
  void resetNumIters();

  /*! \brief Get access to the current Ritz vectors.
    
      \return A multivector with getBlockSize() vectors containing 
      the sorted Ritz vectors corresponding to the most significant Ritz values. 
      The i-th vector of the return corresponds to the i-th Ritz vector; there is no need to use
      getRitzIndex().
   */
  SmartPtr<const MV> getRitzVectors();

  /*! \brief Get the Ritz values for the previous iteration.
   *
   *  \return A vector of length getCurSubspaceDim() containing the Ritz values from the
   *  previous projected eigensolve.
   */
  std::vector<ValueEigenSolver<ScalarType> > getRitzValues();

  /*! \brief Get the index used for extracting individual Ritz vectors from getRitzVectors().
   *
   * Because BlockDavidson is a Hermitian solver, all Ritz values are real and all Ritz vectors can be represented in a 
   * single column of a multivector. Therefore, getRitzIndex() is not needed when using the output from getRitzVectors().
   *
   * \return An \c int vector of size getCurSubspaceDim() composed of zeros.
   */
  std::vector<int> getRitzIndex();


  /*! \brief Get the current residual norms, computing the norms if they are not up-to-date with the current residual vectors.
   *
   *  \return A vector of length getCurSubspaceDim() containing the norms of the
   *  residuals, with respect to the orthogonalization manager's norm() method.
   */
  std::vector<typename NumTraits<ScalarType>::RealScalar> getResNorms();


  /*! \brief Get the current residual 2-norms, computing the norms if they are not up-to-date with the current residual vectors.
   *
   *  \return A vector of length getCurSubspaceDim() containing the 2-norms of the
   *  current residuals. 
   */
  std::vector<typename NumTraits<ScalarType>::RealScalar> getRes2Norms();


  /*! \brief Get the 2-norms of the residuals.
   * 
   * The Ritz residuals are not defined for the %LOBPCG iteration. Hence, this method returns the 
   * 2-norms of the direct residuals, and is equivalent to calling getRes2Norms().
   *
   *  \return A vector of length getBlockSize() containing the 2-norms of the direct residuals.
   */
  std::vector<typename NumTraits<ScalarType>::RealScalar> getRitzRes2Norms();

  /*! \brief Get the dimension of the search subspace used to generate the current eigenvectors and eigenvalues.
   *
   *  \return An integer specifying the rank of the Krylov subspace currently in use by the eigensolver. If isInitialized() == \c false, 
   *  the return is 0. Otherwise, it will be some strictly positive multiple of getBlockSize().
   */
  int getCurSubspaceDim() const;

  //! Get the maximum dimension allocated for the search subspace. For %BlockDavidson, this always returns numBlocks*blockSize.
  int getMaxSubspaceDim() const;
  //@}


  //! @name Accessor routines from EigenSolver
  //@{ 
  //! Set a new StatusTest for the solver.
  void setStatusTest(SmartPtr<StatusTest<ScalarType,MV,OP> > test);

  //! Get the current StatusTest used by the solver.
  SmartPtr<StatusTest<ScalarType,MV,OP> > getStatusTest() const;

  //! Get a constant reference to the eigenvalue problem.
  const EigenProblem<ScalarType,MV,OP>& getProblem() const;

  /*! \brief Set the blocksize. 
   *
   * This method is required to support the interface provided by EigenSolver. However, the preferred method
   * of setting the allocated size for the BlockDavidson eigensolver is setSize(). In fact, setBlockSize() 
   * simply calls setSize(), maintaining the current number of blocks.
   *
   * The block size determines the number of Ritz vectors and values that are computed on each iteration, thereby
   * determining the increase in the Krylov subspace at each iteration.
   */
  void setBlockSize(int blockSize);

  //! Get the blocksize used by the iterative solver.
  int getBlockSize() const;

  /*! \brief Set the auxiliary vectors for the solver.
   *
   *  Because the current basis V cannot be assumed
   *  orthogonal to the new auxiliary vectors, a call to setAuxVecs() will
   *  reset the solver to the uninitialized state. This happens only in the
   *  case where the new auxiliary vectors have a combined dimension of 
   *  greater than zero.
   *
   *  In order to preserve the current state, the user will need to extract
   *  it from the solver using getState(), orthogonalize it against the
   *  new auxiliary vectors, and reinitialize using initialize().
   */
  void setAuxVecs(const std::vector<SmartPtr<const MV> >& auxvecs);

  //! Get the auxiliary vectors for the solver.
  std::vector<SmartPtr<const MV> > getAuxVecs() const;
  //@}

  //! @name BlockDavidson-specific accessor routines
  //@{ 
  /*! \brief Set the blocksize and number of blocks to be used by the
   * iterative solver in solving this eigenproblem.
   *  
   *  Changing either the block size or the number of blocks will reset the
   *  solver to an uninitialized state.
   *
   *  The requested block size must be strictly positive; the number of blocks must be 
   *  greater than one. Invalid arguments will result in a std::invalid_argument exception.
   */
  void setSize(int blockSize, int numBlocks);
  //@}

  //! @name Output methods
  //@{ 
  //! This method requests that the solver print out its current status to the given output stream.
  void currentStatus(std::ostream& os);
  //@}

private:
  //
  // Convenience typedefs
  //
  typedef SolverUtils<ScalarType,MV,OP> Utils;
  typedef MultiVecTraits<ScalarType,MV> MVT;
  typedef OperatorTraits<ScalarType,MV,OP> OPT;
  typedef NumTraits<ScalarType> SCT;
  typedef typename SCT::RealScalar MagnitudeType;
  const MagnitudeType ONE;  
  const MagnitudeType ZERO; 
  const MagnitudeType NANVAL;
  //
  // Internal structs
  //
  struct CheckList {
    bool checkV;
    bool checkX, checkMX, checkKX;
    bool checkH, checkMH, checkKH;
    bool checkR, checkQ;
    bool checkKK;
    CheckList() : checkV(false),
                  checkX(false),checkMX(false),checkKX(false),
                  checkH(false),checkMH(false),checkKH(false),
                  checkR(false),checkQ(false),checkKK(false) {}
  };
  
  //
  // Internal methods
  //
  std::string accuracyCheck(const CheckList& chk, const std::string& where) const;
  //
  // Classes inputed through constructor that define the eigenproblem to be solved.
  //
  const SmartPtr<EigenProblem<ScalarType,MV,OP> >    problem_;
  const SmartPtr<SortManager<typename NumTraits<ScalarType>::RealScalar> > sm_;
  const SmartPtr<OutputManager<ScalarType> >         om_;
  SmartPtr<StatusTest<ScalarType,MV,OP> >            tester_;
  const SmartPtr<MatOrthoManager<ScalarType,MV,OP> > orthman_;
  //
  // Information obtained from the eigenproblem
  //
  SmartPtr<const OP> Op_;
  SmartPtr<const OP> MOp_;
  SmartPtr<const OP> Prec_;
  bool hasM_;

  //
  // Counters
  //
  int countApplyOp_, countApplyM_, countApplyPrec_;

  //
  // Algorithmic parameters.
  //
  // blockSize_ is the solver block size; it controls the number of eigenvectors that 
  // we compute, the number of residual vectors that we compute, and therefore the number
  // of vectors added to the basis on each iteration.
  int blockSize_;
  // numBlocks_ is the size of the allocated space for the Krylov basis, in blocks.
  int numBlocks_; 
  
  // 
  // Current solver state
  //
  // initialized_ specifies that the basis vectors have been initialized and the iterate() routine
  // is capable of running; _initialize is controlled  by the initialize() member method
  // For the implications of the state of initialized_, please see documentation for initialize()
  bool initialized_;
  //
  // curDim_ reflects how much of the current basis is valid 
  // NOTE: 0 <= curDim_ <= blockSize_*numBlocks_
  // this also tells us how many of the values in theta_ are valid Ritz values
  int curDim_;
  //
  // State Multivecs
  // H_,KH_,MH_ will not own any storage
  // H_ will occasionally point at the current block of vectors in the basis V_
  // MH_,KH_ will occasionally point at MX_,KX_ when they are used as temporary storage
  SmartPtr<MV> X_, KX_, MX_, R_,
                   H_, KH_, MH_,
                   V_;
  //
  // Projected matrices
  //
  SmartPtr<MatrixEigenDense<ScalarType> > KK_;
  // 
  // auxiliary vectors
  std::vector<SmartPtr<const MV> > auxVecs_;
  int numAuxVecs_;
  //
  // Number of iterations that have been performed.
  int iter_;
  // 
  // Current eigenvalues, residual norms
  std::vector<MagnitudeType> theta_, Rnorms_, R2norms_;
  // 
  // are the residual norms current with the residual?
  bool RnormsCurrent_, R2normsCurrent_;

};

//=============================================================================================
// Implementations
//=============================================================================================

//=============================================================================================
// Constructor
template <class ScalarType, class MV, class OP>
BlockDavidson<ScalarType,MV,OP>::BlockDavidson(
      const SmartPtr<EigenProblem<ScalarType,MV,OP> >&    problem,
      const SmartPtr<SortManager<typename NumTraits<ScalarType>::RealScalar> >& sorter,
      const SmartPtr<OutputManager<ScalarType> >&         printer,
      const SmartPtr<StatusTest<ScalarType,MV,OP> >&      tester,
      const SmartPtr<MatOrthoManager<ScalarType,MV,OP> >& ortho,
      Parameters& params
     ) :
  ONE(NumTraits<MagnitudeType>::one()),
  ZERO(NumTraits<MagnitudeType>::zero()),
  NANVAL(NumTraits<MagnitudeType>::nan()),
  // problem, tools
  problem_(problem), 
  sm_(sorter),
  om_(printer),
  tester_(tester),
  orthman_(ortho),
  // counters
  countApplyOp_(0),
  countApplyM_(0),
  countApplyPrec_(0),
  // internal data
  blockSize_(0),
  numBlocks_(0),
  initialized_(false),
  curDim_(0),
  auxVecs_(std::vector<SmartPtr<const MV> >(0)),
  numAuxVecs_(0),
  iter_(0),
  RnormsCurrent_(false),
  R2normsCurrent_(false)
{     
  internalEigenSolver::testErrorEigenProblem(problem_ == _smPtrNull,
                     "xlifepp::BlockDavidson::constructor: user passed null problem pointer.");
  internalEigenSolver::testErrorEigenProblem(sm_ == _smPtrNull,
                     "xlifepp::BlockDavidson::constructor: user passed null sort manager pointer.");
  internalEigenSolver::testErrorEigenProblem(om_ == _smPtrNull,
                     "xlifepp::BlockDavidson::constructor: user passed null output manager pointer.");
  internalEigenSolver::testErrorEigenProblem(tester_ == _smPtrNull,
                     "xlifepp::BlockDavidson::constructor: user passed null status test pointer.");
  internalEigenSolver::testErrorEigenProblem(orthman_ == _smPtrNull,
                     "xlifepp::BlockDavidson::constructor: user passed null orthogonalization manager pointer.");
  internalEigenSolver::testErrorEigenProblem(problem_->isProblemSet() == false,
                     "xlifepp::BlockDavidson::constructor: problem is not set.");
  internalEigenSolver::testErrorEigenProblem(problem_->isHermitian() == false,
                     "xlifepp::BlockDavidson::constructor: problem is not hermitian.");

  // get the problem operators
  Op_   = problem_->getOperator();
  internalEigenSolver::testErrorEigenProblem(Op_ == _smPtrNull,
                     "xlifepp::BlockDavidson::constructor: problem provides no operator.");
  MOp_  = problem_->getM();
  Prec_ = problem_->getPrec();
  hasM_ = (MOp_ != _smPtrNull);

  // set the block size and allocate data
  int_t bs = params.get("Block Size", problem_->getNEV());
  int_t nb = params.get("Num Blocks", int_t(2));
  setSize(bs,nb);
}

//=============================================================================================
// Destructor
template <class ScalarType, class MV, class OP>
BlockDavidson<ScalarType,MV,OP>::~BlockDavidson() {}

//=============================================================================================
// Set the block size
// This simply calls setSize(), modifying the block size while retaining the number of blocks.
template <class ScalarType, class MV, class OP>
void BlockDavidson<ScalarType,MV,OP>::setBlockSize (int blockSize) 
{
  setSize(blockSize,numBlocks_);
}

//=============================================================================================
// Return the current auxiliary vectors
template <class ScalarType, class MV, class OP>
std::vector<SmartPtr<const MV> > BlockDavidson<ScalarType,MV,OP>::getAuxVecs() const {
  return auxVecs_;
}

//=============================================================================================
// return the current block size
template <class ScalarType, class MV, class OP>
int BlockDavidson<ScalarType,MV,OP>::getBlockSize() const {
  return(blockSize_); 
}

//=============================================================================================
// return eigenproblem
template <class ScalarType, class MV, class OP>
const EigenProblem<ScalarType,MV,OP>& BlockDavidson<ScalarType,MV,OP>::getProblem() const { 
  return(*problem_); 
}

//=============================================================================================
// return max subspace dim
template <class ScalarType, class MV, class OP>
int BlockDavidson<ScalarType,MV,OP>::getMaxSubspaceDim() const {
  return blockSize_*numBlocks_;
}

//=============================================================================================
// return current subspace dim
template <class ScalarType, class MV, class OP>
int BlockDavidson<ScalarType,MV,OP>::getCurSubspaceDim() const {
  if (!initialized_) return 0;
  return curDim_;
}

//=============================================================================================
// return ritz residual 2-norms
template <class ScalarType, class MV, class OP>
std::vector<typename NumTraits<ScalarType>::RealScalar>
BlockDavidson<ScalarType,MV,OP>::getRitzRes2Norms() {
  return this->getRes2Norms();
}

//=============================================================================================
// return ritz index
template <class ScalarType, class MV, class OP>
std::vector<int> BlockDavidson<ScalarType,MV,OP>::getRitzIndex() {
  std::vector<int> ret(curDim_,0);
  return ret;
}

//=============================================================================================
// return ritz values
template <class ScalarType, class MV, class OP>
std::vector<ValueEigenSolver<ScalarType> > BlockDavidson<ScalarType,MV,OP>::getRitzValues() {
  std::vector<ValueEigenSolver<ScalarType> > ret(curDim_);
  for (int i=0; i<curDim_; ++i) {
    ret[i].realpart = theta_[i];
    ret[i].imagpart = ZERO;
  }
  return ret;
}

//=============================================================================================
// return pointer to ritz vectors
template <class ScalarType, class MV, class OP>
SmartPtr<const MV> BlockDavidson<ScalarType,MV,OP>::getRitzVectors() {
  return X_;
}

//=============================================================================================
// reset number of iterations
template <class ScalarType, class MV, class OP>
void BlockDavidson<ScalarType,MV,OP>::resetNumIters() { 
  iter_=0; 
}

//=============================================================================================
// return number of iterations
template <class ScalarType, class MV, class OP>
int BlockDavidson<ScalarType,MV,OP>::getNumIters() const { 
  return(iter_); 
}

//=============================================================================================
// return state pointers
template <class ScalarType, class MV, class OP>
BlockDavidsonState<ScalarType,MV> BlockDavidson<ScalarType,MV,OP>::getState() const {
  BlockDavidsonState<ScalarType,MV> state;
  state.curDim = curDim_;
  state.V = V_;
  state.X = X_;
  state.KX = KX_;
  if (hasM_) {
    state.MX = MX_;
  }
  else {
    state.MX = _smPtrNull;
  }
  state.R = R_;
  state.H = H_;
  state.KK = KK_;
  if (curDim_ > 0) {
    state.T = SmartPtr<std::vector<MagnitudeType> >(new std::vector<MagnitudeType>(theta_.begin(),theta_.begin()+curDim_));
  } else {

    state.T = SmartPtr<std::vector<MagnitudeType> >(new std::vector<MagnitudeType>(0));
  }
  return state;
}

//=============================================================================================
// Return initialized state
template <class ScalarType, class MV, class OP>
bool BlockDavidson<ScalarType,MV,OP>::isInitialized() const { return initialized_; }

//=============================================================================================
// Set the block size and make necessary adjustments.
template <class ScalarType, class MV, class OP>
void BlockDavidson<ScalarType,MV,OP>::setSize (int blockSize, int numBlocks) 
{
  // This routine only allocates space; it doesn't not perform any computation
  // any change in size will invalidate the state of the solver.
  internalEigenSolver::testErrorEigenProblem(blockSize < 1,  "xlifepp::BlockDavidson::setSize(blocksize,numblocks): blocksize must be strictly positive.");
  internalEigenSolver::testErrorEigenProblem(numBlocks < 2,  "xlifepp::BlockDavidson::setSize(blocksize,numblocks): numblocks must be greater than one.");
  if (blockSize == blockSize_ && numBlocks == numBlocks_) {
    // do nothing
    return;
  }

  blockSize_ = blockSize;
  numBlocks_ = numBlocks;

  SmartPtr<const MV> tmp;
  // grab some Multivector to clone
  // in practice, getInitVec() should always provide this, but it is possible to use a 
  // EigenProblem with nothing in getInitVec() by manually initializing with initialize(); 
  // in case of that strange scenario, we will try to clone from X_ first, then resort to getInitVec()
  if (X_ != _smPtrNull) { // this is equivalent to blockSize_ > 0
    tmp = X_;
  }
  else {
    tmp = problem_->getInitVec();
    internalEigenSolver::testErrorEigenProblem(tmp == _smPtrNull,
                       "xlifepp::BlockDavidson::setSize(): eigenproblem did not specify initial vectors to clone from.");
  }

  internalEigenSolver::testErrorEigenProblem(numAuxVecs_+blockSize*numBlocks > MVT::getVecLength(*tmp),
                     "xlifepp::BlockDavidson::setSize(): max subspace dimension and auxilliary subspace too large.");


  //
  // blockSize dependent
  //
  // grow/allocate vectors
  Rnorms_.resize(blockSize_,NANVAL);
  R2norms_.resize(blockSize_,NANVAL);
  //
  // clone multivectors off of tmp
  //
  // free current allocation first, to make room for new allocation
  X_ = _smPtrNull;
  KX_ = _smPtrNull;
  MX_ = _smPtrNull;
  R_ = _smPtrNull;
  V_ = _smPtrNull;

  om_->print(_debugEigen," >> Allocating X_\n");
  X_ = MVT::clone(*tmp,blockSize_);
  om_->print(_debugEigen," >> Allocating KX_\n");
  KX_ = MVT::clone(*tmp,blockSize_);
  if (hasM_) {
    om_->print(_debugEigen," >> Allocating MX_\n");
    MX_ = MVT::clone(*tmp,blockSize_);
  }
  else {
    MX_ = X_;
  }
  om_->print(_debugEigen," >> Allocating R_\n");
  R_ = MVT::clone(*tmp,blockSize_);

  //
  // blockSize*numBlocks dependent
  //
  int newsd = blockSize_*numBlocks_;
  theta_.resize(blockSize_*numBlocks_,NANVAL);
  om_->print(_debugEigen," >> Allocating V_\n");
  V_ = MVT::clone(*tmp,newsd);
  KK_ = SmartPtr<MatrixEigenDense<ScalarType> >(new MatrixEigenDense<ScalarType>(newsd,newsd));

  om_->print(_debugEigen," >> done allocating.\n");

  initialized_ = false;
  curDim_ = 0;
}

//=============================================================================================
// Set the auxiliary vectors
template <class ScalarType, class MV, class OP>
void BlockDavidson<ScalarType,MV,OP>::setAuxVecs(const std::vector<SmartPtr<const MV> >& auxvecs) {
  typedef typename std::vector<SmartPtr<const MV> >::iterator tarcpmv;

  // set new auxiliary vectors
  auxVecs_ = auxvecs;
  numAuxVecs_ = 0;
  for (tarcpmv i=auxVecs_.begin(); i != auxVecs_.end(); ++i) {
    numAuxVecs_ += MVT::getNumberVecs(**i);
  }

  // If the solver has been initialized, V is not necessarily orthogonal to new auxiliary vectors
  if (numAuxVecs_ > 0 && initialized_) {
    initialized_ = false;
  }

  if (om_->isVerbosity(_debugEigen)) {
    CheckList chk;
    chk.checkQ = true;
    om_->print(_debugEigen, accuracyCheck(chk, ": in setAuxVecs()"));
  }
}

//=============================================================================================
/* Initialize the state of the solver
 * 
 * POST-CONDITIONS:
 *
 * V_ is orthonormal, orthogonal to auxVecs_, for first curDim_ vectors
 * theta_ contains Ritz w.r.t. V_(1:curDim_)
 * X is Ritz vectors w.r.t. V_(1:curDim_)
 * KX = Op*X
 * MX = M*X if hasM_
 * R = KX - MX*diag(theta_)
 *
 */
template <class ScalarType, class MV, class OP>
void BlockDavidson<ScalarType,MV,OP>::initialize(BlockDavidsonState<ScalarType,MV> newstate)
{
  // NOTE: memory has been allocated by setBlockSize(). Use setBlock below; do not clone
  std::vector<int> bsind(blockSize_);
  for (int i=0; i<blockSize_; ++i) bsind[i] = i;

  // in BlockDavidson, V is primary
  // the order of dependence follows like so.
  // --init->               V,KK
  //    --ritz analysis->   theta,X  
  //       --op apply->     KX,MX  
  //          --compute->   R
  // 
  // if the user specifies all data for a level, we will accept it.
  // otherwise, we will generate the whole level, and all subsequent levels.
  //
  // the data members are ordered based on dependence, and the levels are
  // partitioned according to the amount of work required to produce the
  // items in a level.
  //
  // inconsistent multivectors widths and lengths will not be tolerated, and
  // will be treated with exceptions.
  //
  // for multivector pointers in newstate which point directly (as opposed to indirectly, via a view) to 
  // multivectors in the solver, the copy will not be affected.

  // set up V and KK: get them from newstate if user specified them
  // otherwise, set them manually
  SmartPtr<MV> lclV;
  SmartPtr<MatrixEigenDense<ScalarType> > lclKK;
  // A temporary smart pointer for matrix KK. We use it to extract a part of KK, like "view" of KK
  SmartPtr<MatrixEigenDense<ScalarType> > tmpKK;

  if (newstate.V != _smPtrNull && newstate.KK != _smPtrNull) {
    internalEigenSolver::testErrorEigenProblem(MVT::getVecLength(*newstate.V) != MVT::getVecLength(*V_),
                        "xlifepp::BlockDavidson::initialize(newstate): Vector length of V not correct.");
    internalEigenSolver::testErrorEigenProblem(newstate.curDim < blockSize_,
                        "xlifepp::BlockDavidson::initialize(newstate): Rank of new state must be at least blockSize().");
    internalEigenSolver::testErrorEigenProblem(newstate.curDim > blockSize_*numBlocks_,
                        "xlifepp::BlockDavidson::initialize(newstate): Rank of new state must be less than getMaxSubspaceDim().");
    internalEigenSolver::testErrorEigenProblem(MVT::getNumberVecs(*newstate.V) < newstate.curDim,
                        "xlifepp::BlockDavidson::initialize(newstate): Multivector for basis in new state must be as large as specified state rank.");

    curDim_ = newstate.curDim;
    // pick an integral amount
    curDim_ = (int)(curDim_ / blockSize_)*blockSize_;

    internalEigenSolver::testErrorEigenProblem(curDim_ != newstate.curDim,
                        "xlifepp::BlockDavidson::initialize(newstate): Rank of new state must be a multiple of getBlockSize().");

    // check size of KK
    internalEigenSolver::testErrorEigenProblem(newstate.KK->numOfRows() < curDim_ || newstate.KK->numOfCols() < curDim_,
                        "xlifepp::BlockDavidson::initialize(newstate): Projected matrix in new state must be as large as specified state rank.");

    // put data in V
    std::vector<int> nevind(curDim_);
    for (int i=0; i<curDim_; ++i) nevind[i] = i;
    if (newstate.V != V_) {
      if (curDim_ < MVT::getNumberVecs(*newstate.V)) {
        newstate.V = MVT::cloneView(*newstate.V,nevind);
      }
      MVT::setBlock(*newstate.V,nevind,*V_);
    }
    lclV = MVT::cloneViewNonConst(*V_,nevind);

    // put data into KK_
    //lclKK = SmartPtr(new MatrixEigenDense<ScalarType>(Teuchos::View,*KK_,curDim_,curDim_));
    tmpKK = SmartPtr<MatrixDense>(new MatrixDense(*KK_,0,0,curDim_,curDim_));
    lclKK = smartPtrFromRef(&(*tmpKK));
    if (newstate.KK != KK_) {
      if (newstate.KK->numOfRows() > curDim_ || newstate.KK->numOfCols() > curDim_) {
        //newstate.KK = Teuchos::rcp(new MatrixEigenDense<ScalarType>(Teuchos::View,*newstate.KK,curDim_,curDim_));
        newstate.KK = SmartPtr<MatrixDense>(new MatrixEigenDense<ScalarType>(*(newstate.KK), 0, 0, curDim_, curDim_));
      }
      //lclKK->assign(*newstate.KK);
      lclKK->replace(*newstate.KK,0,0,curDim_,curDim_);  // FIXME Need to replace in the future
    }

  }
  else {
    // user did not specify one of V or KK
    // get vectors from problem or generate something, projectAndNormalize
    SmartPtr<const MV> ivec = problem_->getInitVec();
    internalEigenSolver::testErrorEigenProblem(ivec == _smPtrNull,
                       "xlifepp::BlockDavdison::initialize(newstate): EigenProblem did not specify initial vectors to clone from.");
    // clear newstate so we won't use any data from it below
    newstate.X      = _smPtrNull;
    newstate.MX     = _smPtrNull;
    newstate.KX     = _smPtrNull;
    newstate.R      = _smPtrNull;
    newstate.H      = _smPtrNull;
    newstate.T      = _smPtrNull;
    newstate.KK     = _smPtrNull;
    newstate.V      = _smPtrNull;
    newstate.curDim = 0;

    curDim_ = MVT::getNumberVecs(*ivec);
    // pick the largest multiple of blockSize_
    curDim_ = (int)(curDim_ / blockSize_)*blockSize_;
    if (curDim_ > blockSize_*numBlocks_) {
      // user specified too many vectors... truncate
      // this produces a full subspace, but that is okay
      curDim_ = blockSize_*numBlocks_;
    }
    bool userand = false;
    if (curDim_ == 0) {
      // we need at least blockSize_ vectors
      // use a random multivec: ignore everything from InitVec
      userand = true;
      curDim_ = blockSize_;
    }

    // get pointers into V,KV,MV
    // tmpVecs will be used below for M*V and K*V (not simultaneously)
    // lclV has curDim vectors
    // if there is space for lclV and tmpVecs in V_, point tmpVecs into V_
    // otherwise, we must allocate space for these products
    //
    // get pointer to first curDim vector in V_
    std::vector<int> dimind(curDim_);
    for (int i=0; i<curDim_; ++i) dimind[i] = i;
    lclV = MVT::cloneViewNonConst(*V_,dimind);
    if (userand) {
      // generate random vector data
      MVT::mvRandom(*lclV);
    }
    else {
      if (MVT::getNumberVecs(*ivec) > curDim_) {
        ivec = MVT::cloneView(*ivec,dimind);
      }
      // assign ivec to first part of lclV
      MVT::setBlock(*ivec,dimind,*lclV);
    }
    SmartPtr<MV> tmpVecs;
    if (curDim_*2 <= blockSize_*numBlocks_) {
      // partition V_ = [lclV tmpVecs _leftover_]
      std::vector<int> block2(curDim_);
      for (int i=0; i<curDim_; ++i) block2[i] = curDim_+i;
      tmpVecs = MVT::cloneViewNonConst(*V_,block2);
    }
    else {
      // allocate space for tmpVecs
      tmpVecs = MVT::clone(*V_,curDim_);
    }

    // compute M*lclV if hasM_
    if (hasM_) {
      OPT::apply(*MOp_,*lclV,*tmpVecs);
      countApplyM_ += curDim_;
    }

    // remove auxVecs from lclV and normalize it
    if (auxVecs_.size() > 0) {
      std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > dummyC;
      int rank = orthman_->projectAndNormalizeMat(*lclV,auxVecs_,dummyC,_smPtrNull,tmpVecs);
      internalEigenSolver::testErrorEigenProblem(rank != curDim_,
                         "xlifepp::BlockDavidson::initialize(): Couldn't generate initial basis of full rank.");
    }
    else {
      int rank = orthman_->normalizeMat(*lclV,_smPtrNull,tmpVecs);
      internalEigenSolver::testErrorEigenProblem(rank != curDim_,
                         "xlifepp::BlockDavidson::initialize(): Couldn't generate initial basis of full rank.");
    }

    // compute K*lclV: we are re-using tmpVecs to store the result
    {
      OPT::apply(*Op_,*lclV,*tmpVecs);
      countApplyOp_ += curDim_;
    }

    // generate KK
    //lclKK = Teuchos::rcp(new MatrixEigenDense<ScalarType>(Teuchos::View,*KK_,curDim_,curDim_));
    tmpKK = SmartPtr<MatrixDense>(new MatrixDense(*KK_,0,0,curDim_,curDim_));
    lclKK = smartPtrFromRef(&(*tmpKK));
    MVT::mvTransMv(ONE,*lclV,*tmpVecs,*lclKK);

    // clear tmpVecs
    tmpVecs = _smPtrNull;
  }
  KK_->replace(*lclKK, 0, 0, curDim_, curDim_); // FIXME Need to replace in the future


  // X,theta require Ritz analysis; if we have to generate one of these, we might as well generate both
  if (newstate.X != _smPtrNull && newstate.T != _smPtrNull) {
    internalEigenSolver::testErrorEigenProblem(MVT::getNumberVecs(*newstate.X) != blockSize_ || MVT::getVecLength(*newstate.X) != MVT::getVecLength(*X_),
                         "xlifepp::BlockDavidson::initialize(newstate): Size of X must be consistent with block size and length of V.");
    internalEigenSolver::testErrorEigenProblem((signed int)(newstate.T->size()) != curDim_,
                         "xlifepp::BlockDavidson::initialize(newstate): Size of T must be consistent with dimension of V.");

    if (newstate.X != X_) {
      MVT::setBlock(*newstate.X,bsind,*X_);
    }

    std::copy(newstate.T->begin(),newstate.T->end(),theta_.begin());
  }
  else {
    // compute ritz vecs/vals
    MatrixEigenDense<ScalarType> S(curDim_,curDim_);
    {
      int rank = curDim_;
      Utils::directSolver(curDim_, *lclKK, _smPtrNull, S, theta_, rank, 10);
      // we want all ritz values back
      internalEigenSolver::testErrorEigenProblem(rank != curDim_,
                         "xlifepp::BlockDavidson::initialize(newstate): Not enough Ritz vectors to initialize algorithm.");
    }
    // sort ritz pairs
    {
      std::vector<int> order(curDim_);
      //
      // sort the first curDim_ values in theta_
      sm_->sort(theta_, smartPtrFromRef(&order), curDim_);
      //
      // apply the same ordering to the primitive ritz vectors
      Utils::permuteVectors(order,S);
    }

    // compute eigenvectors
    //MatrixEigenDense<ScalarType> S1(Teuchos::View,S,curDim_,blockSize_);
    MatrixEigenDense<ScalarType> S1(S, 0, 0, curDim_, blockSize_); // FIXME Need to replace in the future
    {
      // X <- lclV*S
      MVT::mvTimesMatAddMv(ONE, *lclV, S1, ZERO, *X_);
      S.replace(S1, 0, 0, curDim_, blockSize_); // FIXME Need to replace in the future
    }
    // we generated theta,X so we don't want to use the user's KX,MX
    newstate.KX = _smPtrNull;
    newstate.MX = _smPtrNull;
  }

  // done with local pointers
  lclV = _smPtrNull;
  lclKK = _smPtrNull;

  // set up KX
  if (newstate.KX != _smPtrNull) {
    internalEigenSolver::testErrorEigenProblem(MVT::getNumberVecs(*newstate.KX) != blockSize_,
                         "xlifepp::BlockDavidson::initialize(newstate): vector length of newstate.KX not correct.");
    internalEigenSolver::testErrorEigenProblem(MVT::getVecLength(*newstate.KX) != MVT::getVecLength(*X_),
                         "xlifepp::BlockDavidson::initialize(newstate): newstate.KX must have at least block size vectors.");
    if (newstate.KX != KX_) {
      MVT::setBlock(*newstate.KX,bsind,*KX_);
    }
  }
  else {
    // generate KX
    {
      OPT::apply(*Op_,*X_,*KX_);
      countApplyOp_ += blockSize_;
    }
    // we generated KX; we will generate R as well
    newstate.R = _smPtrNull;
  }

  // set up MX
  if (hasM_) {
    if (newstate.MX != _smPtrNull) {
      internalEigenSolver::testErrorEigenProblem(MVT::getNumberVecs(*newstate.MX) != blockSize_,
                           "xlifepp::BlockDavidson::initialize(newstate): vector length of newstate.MX not correct.");
      internalEigenSolver::testErrorEigenProblem(MVT::getVecLength(*newstate.MX) != MVT::getVecLength(*X_),
                           "xlifepp::BlockDavidson::initialize(newstate): newstate.MX must have at least block size vectors.");
      if (newstate.MX != MX_) {
        MVT::setBlock(*newstate.MX,bsind,*MX_);
      }
    }
    else {
      // generate MX
      {
        OPT::apply(*MOp_,*X_,*MX_);
        countApplyOp_ += blockSize_;
      }
      // we generated MX; we will generate R as well
      newstate.R = _smPtrNull;
    }
  }
  else {
    // the assignment MX_==X_ would be redundant; take advantage of this opportunity to debug a little
    internalEigenSolver::testErrorEigenProblem(MX_ != X_,  "xlifepp::BlockDavidson::initialize(): solver invariant not satisfied (MX==X).");
  }

  // set up R
  if (newstate.R != _smPtrNull) {
    internalEigenSolver::testErrorEigenProblem(MVT::getNumberVecs(*newstate.R) != blockSize_,
                         "xlifepp::BlockDavidson::initialize(newstate): vector length of newstate.R not correct.");
    internalEigenSolver::testErrorEigenProblem(MVT::getVecLength(*newstate.R) != MVT::getVecLength(*X_),
                         "xlifepp::BlockDavidson::initialize(newstate): newstate.R must have at least block size vectors.");
    if (newstate.R != R_) {
      MVT::setBlock(*newstate.R,bsind,*R_);
    }
  }
  else {
    // form R <- KX - MX*T
    MVT::mvAddMv(ZERO,*KX_,ONE,*KX_,*R_);
    MatrixEigenDense<ScalarType> T(blockSize_,blockSize_);
    T.putScalar(ZERO);
    for (int i=0; i<blockSize_; ++i) T.coeffRef(i,i) = theta_[i];
    MVT::mvTimesMatAddMv(-ONE,*MX_,T,ONE,*R_);
  }

  // R has been updated; mark the norms as out-of-date
  RnormsCurrent_ = false;
  R2normsCurrent_ = false;

  // finally, we are initialized
  initialized_ = true;

  if (om_->isVerbosity(_debugEigen)) {
    // Check almost everything here
    CheckList chk;
    chk.checkV = true;
    chk.checkX = true;
    chk.checkKX = true;
    chk.checkMX = true;
    chk.checkR = true;
    chk.checkQ = true;
    chk.checkKK = true;
    om_->print(_debugEigen, accuracyCheck(chk, ": after initialize()"));
  }

  // Print information on current status
  // not now
  if (om_->isVerbosity(_debugEigen)) {
    currentStatus(om_->stream(_debugEigen));
  }
  else if (om_->isVerbosity(_iterationDetailsEigen)) {
    currentStatus(om_->stream(_iterationDetailsEigen));
  }
}

//=============================================================================================
// initialize the solver with default state
template <class ScalarType, class MV, class OP>
void BlockDavidson<ScalarType,MV,OP>::initialize()
{
  BlockDavidsonState<ScalarType,MV> empty;
  initialize(empty);
}

//=============================================================================================
// Perform BlockDavidson iterations until the StatusTest tells us to stop.
template <class ScalarType, class MV, class OP>
void BlockDavidson<ScalarType,MV,OP>::iterate ()
{
  // Initialize solver state
  if (initialized_ == false) {
    initialize();
  }

  // as a data member, this would be redundant and require synchronization with
  // blockSize_ and numBlocks_; we'll use a constant here.
  const int searchDim = blockSize_*numBlocks_;

  // The projected matrices are part of the state, but the eigenvectors are defined locally.
  //    S = Local eigenvectors         (size: searchDim * searchDim
  MatrixEigenDense<ScalarType> S(searchDim, searchDim);

  // iterate until the status test tells us to stop.
  // also break if our basis is full
  while (tester_->checkStatus(this) != _passed && curDim_ < searchDim)
  {
    // Print information on current iteration
    if (om_->isVerbosity(_debugEigen)) {
      currentStatus(om_->stream(_debugEigen));
    }
    else if (om_->isVerbosity(_iterationDetailsEigen)) {
      currentStatus(om_->stream(_iterationDetailsEigen));
    }

    ++iter_;
    // get the current part of the basis
    std::vector<int> curind(blockSize_);
    for (int i=0; i<blockSize_; ++i) curind[i] = curDim_ + i;
    H_ = MVT::cloneViewNonConst(*V_,curind);
    
    // apply the preconditioner on the residuals: H <- Prec*R
    // H = Prec*R
    if (Prec_ != _smPtrNull) {
      OPT::apply(*Prec_, *R_, *H_);   // don't catch the exception
      countApplyPrec_ += blockSize_;
    }
    else {
      std::vector<int> bsind(blockSize_);
      for (int i=0; i<blockSize_; ++i) { bsind[i] = i; }
      MVT::setBlock(*R_,bsind,*H_);
    }

    // apply the mass matrix on H
    if (hasM_) {
      // use memory at MX_ for temporary storage
      MH_ = MX_;
      OPT::apply(*MOp_, *H_, *MH_);    // don't catch the exception
      countApplyM_ += blockSize_;
    }
    else  {
      MH_ = H_;
    }

    // Get a view of the previous vectors
    // this is used for orthogonalization and for computing V^H K H
    std::vector<int> prevind(curDim_);
    for (int i=0; i<curDim_; ++i) prevind[i] = i;
    SmartPtr<const MV> Vprev = MVT::cloneView(*V_,prevind);

    // Orthogonalize H against the previous vectors and the auxiliary vectors, and normalize
    {
      std::vector<SmartPtr<const MV> > against = auxVecs_;
      against.push_back(Vprev);
      std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > dummyC;
      int rank = orthman_->projectAndNormalizeMat(*H_,against,
                                          dummyC,
                                          _smPtrNull,MH_);
      internalEigenSolver::testErrorEigenProblem(rank != blockSize_,
                         "xlifepp::BlockDavidson::iterate(): unable to compute orthonormal basis for H.");
    }

    // apply the stiffness matrix to H
    {
      // use memory at KX_ for temporary storage
      KH_ = KX_;
      OPT::apply(*Op_, *H_, *KH_);    // don't catch the exception
      countApplyOp_ += blockSize_;
    }


    if (om_->isVerbosity(_debugEigen)) {
      CheckList chk;
      chk.checkH = true;
      chk.checkMH = true;
      chk.checkKH = true;
      om_->print(_debugEigen, accuracyCheck(chk, ": after ortho H"));
    }
    else if (om_->isVerbosity(_orthoDetailsEigen)) {
      CheckList chk;
      chk.checkH = true;
      chk.checkMH = true;
      chk.checkKH = true;
      om_->print(_orthoDetailsEigen, accuracyCheck(chk,": after ortho H"));
    }

    // compute next part of the projected matrices
    // this this in two parts
    SmartPtr<MatrixEigenDense<ScalarType> > nextKK;
    // Vprev*K*H
    // nextKK = Teuchos::rcp(new MatrixEigenDense<ScalarType>(Teuchos::View,*KK_,curDim_,blockSize_,0,curDim_));
    nextKK = SmartPtr<MatrixDense>(new MatrixEigenDense<ScalarType>(*KK_,0,curDim_, curDim_,blockSize_));
    MVT::mvTransMv(ONE,*Vprev,*KH_,*nextKK);
    // A little bit strange code here. In fact, it decrease performance, it's necessary to have a better MatrixEigenDense.
    KK_->replace(*nextKK, 0,curDim_, curDim_,blockSize_); // FIXME Need to replace in the future

    // H*K*H
    // nextKK = Teuchos::rcp(new MatrixEigenDense<ScalarType>(Teuchos::View,*KK_,blockSize_,blockSize_,curDim_,curDim_));
    nextKK = SmartPtr<MatrixDense>(new MatrixEigenDense<ScalarType>(*KK_,curDim_,curDim_,blockSize_,blockSize_));
    MVT::mvTransMv(ONE,*H_,*KH_,*nextKK);
    KK_->replace(*nextKK, curDim_,curDim_,blockSize_,blockSize_);

    // make sure that KK_ is Hermitian in memory
    nextKK = _smPtrNull;

    for (int i=curDim_; i<curDim_+blockSize_; ++i) {
      for (int j=0; j<i; ++j) {
        (*KK_).coeffRef(i,j) = SCT::conjugate((*KK_).coeff(j,i));
      }
    }

    // V has been extended, and KK has been extended. Update basis dim and release all pointers.
    curDim_ += blockSize_;
    H_ = KH_ = MH_ = _smPtrNull;
    Vprev = _smPtrNull;

    if (om_->isVerbosity(_debugEigen)) {
      CheckList chk;
      chk.checkKK = true;
      om_->print(_debugEigen, accuracyCheck(chk, ": after expanding KK"));
    }

    // Get pointer to complete basis
    curind.resize(curDim_);
    for (int i=0; i<curDim_; ++i) curind[i] = i;
    SmartPtr<const MV> curV = MVT::cloneView(*V_,curind);

    // Perform spectral decomposition
    {
      int nevlocal = curDim_;
      int info = Utils::directSolver(curDim_,*KK_,_smPtrNull,S,theta_,nevlocal,10);
      internalEigenSolver::testErrorEigenProblem(info != _success,"xlifepp::BlockDavidson::iterate(): direct solve returned error code.");
      // we did not ask directSolver to perform deflation, so nevLocal better be curDim_
      internalEigenSolver::testErrorEigenProblem(nevlocal != curDim_,"xlifepp::BlockDavidson::iterate(): direct solve did not compute all eigenvectors."); // this should never happen
    }

    // Sort ritz pairs
    { 
      std::vector<int> order(curDim_);
      // sort the first curDim_ values in theta_
      sm_->sort(theta_, smartPtrFromRef(&order), curDim_);
      // apply the same ordering to the primitive ritz vectors
      //MatrixEigenDense<ScalarType> curS(Teuchos::View,S,curDim_,curDim_);
      MatrixEigenDense<ScalarType> curS(S, 0 , 0, curDim_, curDim_); // FIXME Need to replace in the future
      Utils::permuteVectors(order,curS);
      S.replace(curS,  0 , 0, curDim_, curDim_); // FIXME Need to replace in the future
    }

    // Create a view matrix of the first blockSize_ vectors
    //MatrixEigenDense<ScalarType> S1(Teuchos::View, S, curDim_, blockSize_);
    MatrixEigenDense<ScalarType> S1(S, 0 , 0, curDim_, blockSize_); // FIXME Need to replace in the future

    // Compute the new Ritz vectors
    {
      MVT::mvTimesMatAddMv(ONE,*curV,S1,ZERO,*X_);
    }

    // apply the stiffness matrix for the Ritz vectors
    {
      OPT::apply(*Op_, *X_, *KX_);    // don't catch the exception
      countApplyOp_ += blockSize_;
    }

    // apply the mass matrix for the Ritz vectors
    if (hasM_) {
      OPT::apply(*MOp_,*X_,*MX_);
      countApplyM_ += blockSize_;
    }
    else {
      MX_ = X_;
    }

    // Compute the residual
    // R = KX - MX*diag(theta)
    {
      MVT::mvAddMv(ONE, *KX_, ZERO, *KX_, *R_);
      MatrixEigenDense<ScalarType> T(blockSize_, blockSize_);
      for (int i = 0; i < blockSize_; ++i) {
        T.coeffRef(i,i) = theta_[i];
      }
      MVT::mvTimesMatAddMv(-ONE, *MX_, T, ONE, *R_);
    }

    // R has been updated; mark the norms as out-of-date
    RnormsCurrent_ = false;
    R2normsCurrent_ = false;

    // When required, monitor some orthogonalities
    if (om_->isVerbosity(_debugEigen)) {
      // Check almost everything here
      CheckList chk;
      chk.checkV = true;
      chk.checkX = true;
      chk.checkKX = true;
      chk.checkMX = true;
      chk.checkR = true;
      om_->print(_debugEigen, accuracyCheck(chk, ": after local update"));
    }
    else if (om_->isVerbosity(_orthoDetailsEigen)) {
      CheckList chk;
      chk.checkX = true;
      chk.checkKX = true;
      chk.checkMX = true;
      chk.checkR = true;
      om_->print(_orthoDetailsEigen, accuracyCheck(chk, ": after local update"));
    }
  } // end while (statusTest == false)

} // end of iterate()

//=============================================================================================
// compute/return residual M-norms
template <class ScalarType, class MV, class OP>
std::vector<typename NumTraits<ScalarType>::RealScalar>
BlockDavidson<ScalarType,MV,OP>::getResNorms() {
  if (RnormsCurrent_ == false) {
    // Update the residual norms
    orthman_->norm(*R_,Rnorms_);
    RnormsCurrent_ = true;
  }
  return Rnorms_;
}

//=============================================================================================
// compute/return residual 2-norms
template <class ScalarType, class MV, class OP>
std::vector<typename NumTraits<ScalarType>::RealScalar>
BlockDavidson<ScalarType,MV,OP>::getRes2Norms() {
  if (R2normsCurrent_ == false) {
    // Update the residual 2-norms 
    MVT::mvNorm(*R_,R2norms_);
    R2normsCurrent_ = true;
  }
  return R2norms_;
}

//=============================================================================================
// Set a new StatusTest for the solver.
template <class ScalarType, class MV, class OP>
void BlockDavidson<ScalarType,MV,OP>::setStatusTest(SmartPtr<StatusTest<ScalarType,MV,OP> > test) {
  internalEigenSolver::testErrorEigenProblem(test == _smPtrNull,
      "xlifepp::BlockDavidson::setStatusTest() was passed a null StatusTest.");
  tester_ = test;
}

//=============================================================================================
// Get the current StatusTest used by the solver.
template <class ScalarType, class MV, class OP>
SmartPtr<StatusTest<ScalarType,MV,OP> > BlockDavidson<ScalarType,MV,OP>::getStatusTest() const {
  return tester_;
}

//=============================================================================================
// Check accuracy, orthogonality, and other debugging stuff
// 
// bools specify which tests we want to run (instead of running more than we actually care about)
//
// we don't bother checking the following because they are computed explicitly:
//    H == Prec*R
//   KH == K*H
//
// 
// checkV: V orthonormal
//          orthogonal to auxvecs
// checkX: X orthonormal
//          orthogonal to auxvecs
// checkMX: check MX == M*X
// checkKX: check KX == K*X
// checkH: H orthonormal 
//          orthogonal to V and H and auxvecs
// checkMH: check MH == M*H
// checkR: check R orthogonal to X
// checkQ: check that auxiliary vectors are actually orthonormal
// checkKK: check that KK is symmetric in memory 
//
// TODO: 
//  add checkTheta
template <class ScalarType, class MV, class OP>
std::string BlockDavidson<ScalarType,MV,OP>::accuracyCheck(const CheckList& chk, const std::string& where) const
{
  std::stringstream os;
  os.precision(2);
  os.setf(std::ios::scientific, std::ios::floatfield);

  os << " Debugging checks: iteration " << iter_ << where << std::endl;

  // V and friends
  std::vector<int> lclind(curDim_);
  for (int i=0; i<curDim_; ++i) lclind[i] = i;
  SmartPtr<const MV> lclV;
  if (initialized_) {
    lclV = MVT::cloneView(*V_,lclind);
  }
  if (chk.checkV && initialized_) {
    MagnitudeType err = orthman_->orthonormError(*lclV);
    os << " >> Error in V^H M V == I  : " << err << std::endl;
    for (number_t i=0; i<auxVecs_.size(); ++i) {
      err = orthman_->orthogError(*lclV,*auxVecs_[i]);
      os << " >> Error in V^H M Q[" << i << "] == 0 : " << err << std::endl;
    }
    MatrixEigenDense<ScalarType> curKK(curDim_,curDim_);
    SmartPtr<MV> lclKV = MVT::clone(*V_,curDim_);
    OPT::apply(*Op_,*lclV,*lclKV);
    MVT::mvTransMv(ONE,*lclV,*lclKV,curKK);
    MatrixEigenDense<ScalarType> subKK(*KK_, 0, 0, curDim_, curDim_);
    curKK -= subKK;
    // dup the lower tri part
    for (int j=0; j<curDim_; ++j) {
      for (int i=j+1; i<curDim_; ++i) {
        curKK.coeffRef(i,j) = curKK.coeff(j,i);
      }
    }
    os << " >> Error in V^H K V == KK: " << curKK.normFrobenius() << std::endl;
  }

  // X and friends
  if (chk.checkX && initialized_) {
    MagnitudeType err = orthman_->orthonormError(*X_);
    os << " >> Error in X^H M X == I  : " << err << std::endl;
    for (number_t i=0; i<auxVecs_.size(); ++i) {
      err = orthman_->orthogError(*X_,*auxVecs_[i]);
      os << " >> Error in X^H M Q[" << i << "] == 0 : " << err << std::endl;
    }
  }
  if (chk.checkMX && hasM_ && initialized_) {
    MagnitudeType err = Utils::errorEquality(*X_, *MX_, MOp_);
    os << " >> Error in MX == M*X     : " << err << std::endl;
  }
  if (chk.checkKX && initialized_) {
    MagnitudeType err = Utils::errorEquality(*X_, *KX_, Op_);
    os << " >> Error in KX == K*X     : " << err << std::endl;
  }

  // H and friends
  if (chk.checkH && initialized_) {
    MagnitudeType err = orthman_->orthonormError(*H_);
    os << " >> Error in H^H M H == I  : " << err << std::endl;
    err = orthman_->orthogError(*H_,*lclV);
    os << " >> Error in H^H M V == 0  : " << err << std::endl;
    err = orthman_->orthogError(*H_,*X_);
    os << " >> Error in H^H M X == 0  : " << err << std::endl;
    for (number_t i=0; i<auxVecs_.size(); ++i) {
      err = orthman_->orthogError(*H_,*auxVecs_[i]);
      os << " >> Error in H^H M Q[" << i << "] == 0 : " << err << std::endl;
    }
  }
  if (chk.checkKH && initialized_) {
    MagnitudeType err = Utils::errorEquality(*H_, *KH_, Op_);
    os << " >> Error in KH == K*H     : " << err << std::endl;
  }
  if (chk.checkMH && hasM_ && initialized_) {
    MagnitudeType err = Utils::errorEquality(*H_, *MH_, MOp_);
    os << " >> Error in MH == M*H     : " << err << std::endl;
  }

  // R: this is not M-orthogonality, but standard Euclidean orthogonality
  if (chk.checkR && initialized_) {
    MatrixEigenDense<ScalarType> xTx(blockSize_,blockSize_);
    MVT::mvTransMv(ONE,*X_,*R_,xTx);
    MagnitudeType err = xTx.normFrobenius();
    os << " >> Error in X^H R == 0    : " << err << std::endl;
  }

  // KK
  if (chk.checkKK && initialized_) {
    //MatrixEigenDense<ScalarType> SDMerr(curDim_,curDim_), lclKK(Teuchos::View,*KK_,curDim_,curDim_);
    MatrixEigenDense<ScalarType> SDMerr(curDim_,curDim_), lclKK(*KK_,0,0,curDim_,curDim_);
    for (int j=0; j<curDim_; ++j) {
      for (int i=0; i<curDim_; ++i) {
        SDMerr.coeffRef(i,j) = lclKK.coeff(i,j) - conj(lclKK.coeff(j,i));
      }
    }
    os << " >> Error in KK - KK^H == 0 : " << SDMerr.normFrobenius() << std::endl;
  }

  // Q
  if (chk.checkQ) {
    for (number_t i=0; i<auxVecs_.size(); ++i) {
      MagnitudeType err = orthman_->orthonormError(*auxVecs_[i]);
      os << " >> Error in Q[" << i << "]^H M Q[" << i << "] == I: " << err << std::endl;
      for (number_t j=i+1; j<auxVecs_.size(); ++j) {
        err = orthman_->orthogError(*auxVecs_[i],*auxVecs_[j]);
        os << " >> Error in Q[" << i << "]^H M Q[" << j << "] == 0 : " << err << std::endl;
      }
    }
  }

  os << std::endl;

  return os.str();
}

//=============================================================================================
// Print the current status of the solver
template <class ScalarType, class MV, class OP>
void 
BlockDavidson<ScalarType,MV,OP>::currentStatus(std::ostream& os)
{
  os.setf(std::ios::scientific, std::ios::floatfield);
  os.precision(6);
  os <<std::endl;
  os <<"================================================================================" << std::endl;
  os << std::endl;
  os <<"                          BlockDavidson Solver Status" << std::endl;
  os << std::endl;
  os <<"The solver is "<<(initialized_ ? "initialized." : "not initialized.") << std::endl;
  os <<"The number of iterations performed is " <<iter_<<std::endl;
  os <<"The block size is         " << blockSize_<<std::endl;
  os <<"The number of blocks is   " << numBlocks_<<std::endl;
  os <<"The current basis size is " << curDim_<<std::endl;
  os <<"The number of auxiliary vectors is "<< numAuxVecs_ << std::endl;
  os <<"The number of operations Op*x   is "<<countApplyOp_<<std::endl;
  os <<"The number of operations M*x    is "<<countApplyM_<<std::endl;
  os <<"The number of operations Prec*x is "<<countApplyPrec_<<std::endl;

  os.setf(std::ios_base::right, std::ios_base::adjustfield);

  if (initialized_) {
    os << std::endl;
    os <<"CURRENT EIGENVALUE ESTIMATES             "<<std::endl;
    os << std::setw(20) << "Eigenvalue" 
       << std::setw(20) << "Residual(M)"
       << std::setw(20) << "Residual(2)"
       << std::endl;
    os <<"--------------------------------------------------------------------------------"<<std::endl;
    for (int i=0; i<blockSize_; ++i) {
      os << std::setw(20) << theta_[i];
      if (RnormsCurrent_) os << std::setw(20) << Rnorms_[i];
      else os << std::setw(20) << "not current";
      if (R2normsCurrent_) os << std::setw(20) << R2norms_[i];
      else os << std::setw(20) << "not current";
      os << std::endl;
    }
  }
  os <<"================================================================================" << std::endl;
  os << std::endl;
}
  
} // End of namespace xlifepp

#endif /*XLIFEPP_BLOCK_DAVIDSON_HPP*/
