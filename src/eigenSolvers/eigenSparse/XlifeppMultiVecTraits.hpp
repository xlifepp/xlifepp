/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppMultiVecTraits.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013

  \brief Declaration of basic traits for the multivector type

  xlifepp::MultiVecTraits declares basic traits for the multivector
  type MV used in xlifepp's orthogonalizations and solvers.  A
  specialization of xlifepp::MultiVecTraits that defines all the traits must
  be made for each specific multivector type.  Here, we only provide
  default definitions that fail at compile time if no specialization
  of xlifepp::MultiVecTraits exists for the given combination of scalar type
  (ScalarType) and multivector type (MV).
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_MULTI_VEC_TRAITS_HPP
#define XLIFEPP_MULTI_VEC_TRAITS_HPP

#include "XlifeppEigenTypes.hpp"

namespace xlifepp {

/*! \class UndefinedMultiVecTraits
    \brief Used by MultiVecTraits to report lack of a specialization.
   
    MultiVecTraits<ScalarType, MV> uses this struct to produce a
    compile-time error when no specialization exists for the scalar
    type ScalarType and multivector type MV.
*/
template< class ScalarType, class MV >
struct UndefinedMultiVecTraits
{
  /*! \brief Any attempt to compile this method will result in a compile-time error.
     
      If you see compile errors referring to this method, then
      either no specialization of MultiVecTraits exists for the
      scalar type ScalarType and multivector type MV, or the
      specialization for ScalarType and MV is not complete.
  */
  static inline ScalarType notDefined() { return MV::this_type_is_missing_a_specialization(); };
};


/*!
    \class MultiVecTraits
    \tparam ScalarType The type of the entries in the multivectors.
    \tparam MV The type of the multivectors themselves.

    \brief Traits class which defines basic operations on multivectors.

    This traits class tells xlifepp's solvers how to perform
    multivector operations for the multivector type MV.  These
    operations include creating copies or views, finding the number
    of rows or columns (i.e., vectors) in a given multivector, and
    computing inner products, norms, and vector sums.  (xlifepp's
    solvers use the OperatorTraits traits class to apply operators
    to multivectors.)
   
    xlifepp gives users two different ways to tell its solvers how
    to compute with multivectors of a given type MV.  The first and
    preferred way is for users to specialize MultiVecTraits, this
    traits class, for their given MV type. The second way
    is for users to make their multivector type (or a wrapper
    thereof) inherit from MultiVec.  This works because xlifepp
    provides a specialization of MultiVecTraits for MultiVec.
    Specializing MultiVecTraits is more flexible because it does not
    require a multivector type to inherit from MultiVec; this is
    possible even if you do not have control over the interface of a
    class.

    If you have a different multivector type MV that you would like
    to use with xlifepp, and if that type does not inherit from
    MultiVec, then you must implement a specialization of
    MultiVecTraits for MV.  Otherwise, this traits class will report
    a compile-time error (relating to UndefinedMultiVecTraits).
    Specializing MultiVecTraits for your MV type is not hard.
*/
template<class ScalarType, class MV>
class MultiVecTraits {
public:
  //! @name Creation methods
  //@{

  /*! \brief Creates a new empty \c MV containing \c numvecs columns.

  \return Reference-counted pointer to the new multivector of type \c MV.
  */
  static SmartPtr<MV> clone(const MV& mv, const int numvecs)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); return _smPtrNull; }

  /*! \brief Creates a new \c MV and copies contents of \c mv into the new vector (deep copy).

    \return Reference-counted pointer to the new multivector of type \c MV.
  */
  static SmartPtr<MV> cloneCopy(const MV& mv)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); return _smPtrNull; }

  /*! \brief Creates a new \c MV and copies the selected contents of \c mv into the new vector (deep copy).

    The copied vectors from \c mv are indicated by the \c index.size() indices in \c index.
    \return Reference-counted pointer to the new multivector of type \c MV.
  */
  static SmartPtr<MV> cloneCopy(const MV& mv, const std::vector<int>& index)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); return _smPtrNull; }

  /*! \brief Creates a new \c MV that shares the selected contents of \c mv (shallow copy).

  The index of the \c numvecs vectors shallow copied from \c mv are indicated by the indices given in \c index.
  \return Reference-counted pointer to the new multivector of type \c MV.
  */
  static SmartPtr<MV> cloneViewNonConst(MV& mv, const std::vector<int>& index)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); return _smPtrNull; }

  /*! \brief Creates a new const \c MV that shares the selected contents of \c mv (shallow copy).

  The index of the \c numvecs vectors shallow copied from \c mv are indicated by the indices given in \c index.
  \return Reference-counted pointer to the new const multivector of type \c MV.
  */
  static SmartPtr<const MV> cloneView(const MV& mv, const std::vector<int>& index)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); return _smPtrNull; }

  //@}

  //! @name Attribute methods
  //@{

  //! Obtain the vector length of \c mv.
  static int getVecLength(const MV& mv)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); return 0; }

  //! Obtain the number of vectors in \c mv
  static int getNumberVecs(const MV& mv)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); return 0; }

  //@}

  //! @name Update methods
  //@{

  /*! \brief Update \c mv with \f$ \alpha AB + \beta mv \f$.
   */
  static void mvTimesMatAddMv(const ScalarType alpha, const MV& A,
                               const MatrixEigenDense<ScalarType>& B,
                               const ScalarType beta, MV& mv)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }

  /*! \brief Replace \c mv with \f$\alpha A + \beta B\f$.
   */
  static void mvAddMv(const ScalarType alpha, const MV& A, const ScalarType beta, const MV& B, MV& mv)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }

  /*! \brief Scale each element of the vectors in \c mv with \c alpha.
   */
  static void mvScale (MV& mv, const ScalarType alpha)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }

  /*! \brief Scale each element of the \c i-th vector in \c mv with \c alpha[i].
   */
  static void mvScale (MV& mv, const std::vector<ScalarType>& alpha)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }

  /*! \brief Compute a dense matrix \c B through the matrix-matrix multiply \f$ \alpha A^Hmv \f$.
  */
  static void mvTransMv(const ScalarType alpha, const MV& A, const MV& mv, MatrixEigenDense<ScalarType>& B)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }

  /*! \brief Compute a vector \c b where the components are the individual dot-products of the \c i-th columns of \c A and \c mv, i.e.\f$b[i] = A[i]^Hmv[i]\f$.
   */
  static void mvDot (const MV& mv, const MV& A, std::vector<ScalarType>& b)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }

  //@}
  //! @name Norm method
  //@{

  /*! \brief Compute the 2-norm of each individual vector of \c mv.
    Upon return, \c normvec[i] holds the value of \f$||mv_i||_2\f$, the \c i-th column of \c mv.
  */
  static void mvNorm(const MV& mv, std::vector<typename NumTraits<ScalarType>::magnitudeType>& normvec)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }

  //@}

  //! @name Initialization methods
  //@{
  /*! \brief Copy the vectors in \c A to a set of vectors in \c mv indicated by the indices given in \c index.

  The \c numvecs vectors in \c A are copied to a subset of vectors in \c mv indicated by the indices given in \c index,
  i.e.<tt> mv[index[i]] = A[i]</tt>.
  */
  static void setBlock(const MV& A, const std::vector<int>& index, MV& mv)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }

  /*!
    \brief mv := A
    assign (deep copy) A into mv.
  */
  static void assign(const MV& A, MV& mv)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }

  /*! \brief Replace the vectors in \c mv with random vectors.
   */
  static void mvRandom(MV& mv)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }

  /*! \brief Replace each element of the vectors in \c mv with \c alpha.
   */
  static void mvInit(MV& mv, const ScalarType alpha = NumTraits<ScalarType>::zero())
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }
  //@}

  //! @name Print method
  //@{
  /*! \brief Print the \c mv multi-vector to the \c os output stream.
   */
  static void mvPrint(const MV& mv, std::ostream& os)
  { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }
  //@}
};

} // namespace xlifepp

#endif /* XLIFEPP_MULTI_VEC_TRAITS_HPP */

