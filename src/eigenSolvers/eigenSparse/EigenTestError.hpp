/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file EigenTestError.hpp
  \author Manh Ha NGUYEN
  \since 09 April 2013
  \date  03 June 2013

  \brief This file provides some functions to control errors and warnings for eigen solver
*/

#ifndef EIGEN_SPARSE_TEST_ERROR_HPP
#define EIGEN_SPARSE_TEST_ERROR_HPP

#include "utils.h"

namespace xlifepp
{

//! namespace dedicated to internal eigensolver of XLiFE++
namespace internalEigenSolver
{

/*!
  \struct EigenSolverVerbose
  struct to handle verbose level
*/
struct EigenSolverVerbose
{
  //! returns true if verbose level is greater than debug level
  static inline bool isDebug() { return (theVerboseLevel >= (unsigned int)_debugEigen); }
};

inline void testErrorEigenProblem(bool condition, const string_t& s) { if (condition) error("eigen_eigenproblem", s); }

inline void testWarningEigenProblem(bool condition, const string_t& s) {  if (condition) warning("eigen_eigenproblem_warning", s); }

inline void testErrorEigenProblemMultVec(bool condition, const string_t& s) { if (condition) error("eigen_eigenproblem", "MultiVector" + s); }

inline void printOutDebugInfoEigenProblem(const string_t& nameOfClass, const string_t& message)
{
  if (theVerboseLevel >= (unsigned int)_debugEigen) info("eigen_eigenproblem_debug", nameOfClass + " : " + message);
}

} // end of namespace internalEigenSOlver

} // end of namespace xlifepp

#endif //EIGEN_SPARSE_TEST_ERROR_HPP
