/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file XlifeppBlockDavidsonSolMgr.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013

  \brief The xlifepp::BlockDavidsonSolMgr provides a solver manager for the BlockDavidson eigensolver.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_BLOCKDAVIDSON_SOLMGR_HPP
#define XLIFEPP_BLOCKDAVIDSON_SOLMGR_HPP

#include "utils.h"
#include "../eigenCore/eigenCore.hpp"
#include "EigenTestError.hpp"
#include "XlifeppEigenTypes.hpp"
#include "XlifeppEigenProblem.hpp"
#include "XlifeppSolverManager.hpp"
#include "XlifeppSolverUtils.hpp"
#include "XlifeppBlockDavidson.hpp"
#include "XlifeppBasicSort.hpp"
#include "XlifeppSVQBOrthoManager.hpp"
#include "XlifeppBasicOrthoManager.hpp"
#include "XlifeppStatusTestResNorm.hpp"
#include "XlifeppStatusTestWithOrdering.hpp"
#include "XlifeppStatusTestCombo.hpp"
#include "XlifeppStatusTestOutput.hpp"
#include "XlifeppStatusTestMaxIters.hpp"

#ifdef HAVE_MPI
  #include <mpi.h>
#endif

namespace xlifepp {


/*! \class BlockDavidsonSolMgr
 *
 *  \brief The BlockDavidsonSolMgr provides a powerful solver manager over the BlockDavidson eigensolver.
 *
 * This solver manager implements a hard-locking mechanism, whereby eigenpairs designated to be locked are moved from the eigensolver and placed in
 * auxilliary storage. The eigensolver is then restarted and continues to iterate, orthogonal to the locked eigenvectors.
 *
 * The solver manager provides to the solver a StatusTestCombo object constructed as follows:<br>
 *    &nbsp;&nbsp;&nbsp;<tt>combo = globaltest OR lockingtest OR debugtest</tt><br>
 * where
 *    - \c globaltest terminates computation when global convergence has been detected.<br>
 *      It is encapsulated in a StatusTestWithOrdering object, to ensure that computation is terminated
 *      only after the most significant eigenvalues/eigenvectors have met the convergence criteria.<br>
 *      If not specified via setGlobalStatusTest(), \c globaltest is a StatusTestResNorm object which tests the
 *      M-norms of the direct residuals relative to the Ritz values.
 *    - \c lockingtest halts BlockDavidson::iterate() in order to deflate converged eigenpairs for locking.<br>
 *      It will query the underlying BlockDavidson eigensolver to determine when eigenvectors should be locked.<br>
 *      If not specified via setLockingStatusTest(), \c lockingtest is a StatusTestResNorm object.
 *    - \c debugtest allows a user to specify additional monitoring of the iteration, encapsulated in a StatusTest object<br>
 *      If not specified via setDebugStatusTest(), \c debugtest is ignored.<br>
 *      In most cases, it should return ::_failed; if it returns ::_passed, solve() will throw an XlifeppError exception.
 *
 * Additionally, the solver manager will terminate solve() after a specified number of restarts.
 *
 * Much of this behavior is controlled via parameters and options passed to the
 * solver manager. For more information, see BlockDavidsonSolMgr().
 */
template<class ScalarType, class MV, class OP>
class BlockDavidsonSolMgr : public SolverManager<ScalarType,MV,OP> {

  private:
    typedef MultiVecTraits<ScalarType,MV> MVT;
    typedef OperatorTraits<ScalarType,MV,OP> OPT;
    typedef NumTraits<ScalarType> SCT;
    typedef typename NumTraits<ScalarType>::RealScalar MagnitudeType;
    typedef NumTraits<MagnitudeType> MT;
    typedef MatrixEigenDense<ScalarType> MatrixDense;

  public:

  //! @name Constructors/Destructor
  //@{
  /*! \brief Basic constructor for BlockDavidsonSolMgr.
   *
   * This constructor accepts the EigenProblem to be solved in addition
   * to a parameter list of options for the solver manager. These options include the following:
   *   - Solver parameters
   *      - \c "Which" - a \c string specifying the desired eigenvalues: SM, LM, SR or LR. Default: "SR"
   *      - \c "Block Size" - a \c int specifying the block size to be used by the underlying block Davidson solver. Default: problem->getNEV()
   *      - \c "Num Blocks" - a \c int specifying the number of blocks allocated for the Krylov basis. Default: 2
   *      - \c "Maximum Restarts" - a \c int specifying the maximum number of restarts the underlying solver is allowed to perform. Default: 20
   *      - \c "Verbosity" - a sum of MsgType specifying the verbosity. Default: Errors
   *   - Convergence parameters (if using default convergence test; see setGlobalStatusTest())
   *      - \c "Convergence Tolerance" - a \c MagnitudeType specifying the level that residual norms must reach to decide convergence. Default: machine precision.
   *      - \c "Relative Convergence Tolerance" - a \c bool specifying whether residuals norms should be scaled by their eigenvalues for the purposing of deciding convergence. Default: true
   *      - \c "Convergence Norm" - a \c string specifying the norm for convergence testing: "2" or "M"
   *   - Locking parameters (if using default locking test; see setLockingStatusTest())
   *      - \c "Use Locking" - a \c bool specifying whether the algorithm should employ locking of converged eigenpairs. Default: false
   *      - \c "Max Locked" - a \c int specifying the maximum number of eigenpairs to be locked. Default: problem->getNEV()
   *      - \c "Locking Quorum" - a \c int specifying the number of eigenpairs that must meet the locking criteria before locking actually occurs. Default: 1
   *      - \c "Locking Tolerance" - a \c MagnitudeType specifying the level that residual norms must reach to decide locking. Default: 0.1*convergence tolerance
   *      - \c "Relative Locking Tolerance" - a \c bool specifying whether residuals norms should be scaled by their eigenvalues for the purposing of deciding locking. Default: true
   *      - \c "Locking Norm" - a \c string specifying the norm for locking testing: "2" or "M"
   */
  BlockDavidsonSolMgr(const SmartPtr<EigenProblem<ScalarType,MV,OP> >& problem,
                            Parameters& pl);

  //! Destructor.
  virtual ~BlockDavidsonSolMgr() {}
  //@}

  //! @name Accessor methods
  //@{
  //! Return the eigenvalue problem.
  const EigenProblem<ScalarType,MV,OP>& getProblem() const {
    return *problem_;
  }

  //! Get the iteration count for the most recent call to \c solve().
  int getNumIters() const {
    return numIters_;
  }

  /*! \brief Return the timers for this object.
   *
   * The timers are ordered as follows:
   *   - time spent in solve() routine
   *   - time spent restarting
   *   - time spent locking converged eigenvectors
   */
  // std::vector<Teuchos::Time> getTimers() const {
  //   return Teuchos::tuple(_timerSolve, _timerRestarting, _timerLocking);
  // }
  //@}

  //! @name Solver application methods
  //@{
  /*! \brief This method performs possibly repeated calls to the underlying eigensolver's iterate() routine
   * until the problem has been solved (as decided by the solver manager) or the solver manager decides to
   * quit.
   *
   * This method calls BlockDavidson::iterate(), which will return either because a specially constructed status test evaluates to ::_passed
   * or an exception is thrown.
   *
   * A return from BlockDavidson::iterate() signifies one of the following scenarios:
   *    - the maximum number of restarts has been exceeded. In this scenario, the solver manager will place\n
   *      all converged eigenpairs into the eigenproblem and return ::_noConvergence.
   *    - the locking conditions have been met. In this scenario, some of the current eigenpairs will be removed\n
   *      from the eigensolver and placed into auxiliary storage. The eigensolver will be restarted with the remaining part of the Krylov subspace\n
   *      and some random information to replace the removed subspace.
   *    - global convergence has been met. In this case, the most significant NEV eigenpairs in the solver and locked storage  \n
   *      have met the convergence criterion. (Here, NEV refers to the number of eigenpairs requested by the EigenProblem.)    \n
   *      In this scenario, the solver manager will return ::_success.
   *
   * \returns ReturnType specifying:
   *     - _success: the eigenproblem was solved to the specification required by the solver manager.
   *     - _noConvergence: the eigenproblem was not solved to the specification desired by the solver manager.
  */
  ComputationInfo solve();

  //! Set the status test defining global convergence.
  void setGlobalStatusTest(const SmartPtr<StatusTest<ScalarType,MV,OP> >& global);

  //! Get the status test defining global convergence.
  const SmartPtr<StatusTest<ScalarType,MV,OP> >& getGlobalStatusTest() const;

  //! Set the status test defining locking.
  void setLockingStatusTest(const SmartPtr<StatusTest<ScalarType,MV,OP> >& locking);

  //! Get the status test defining locking.
  const SmartPtr<StatusTest<ScalarType,MV,OP> >& getLockingStatusTest() const;

  //! Set the status test for debugging.
  void setDebugStatusTest(const SmartPtr<StatusTest<ScalarType,MV,OP> >& debug);

  //! Get the status test for debugging.
  const SmartPtr<StatusTest<ScalarType,MV,OP> >& getDebugStatusTest() const;
  //@}

  private:
  SmartPtr<EigenProblem<ScalarType,MV,OP> > problem_;

  string_t whch_, ortho_;

  MagnitudeType convtol_, locktol_;
  int_t maxRestarts_;
  bool useLocking_;
  bool relconvtol_, rellocktol_;
  int_t blockSize_, numBlocks_, numIters_;
  int_t maxLocked_;
  int_t lockQuorum_;
  bool inSituRestart_;
  int_t numRestartBlocks_;
  enum StatusTestResNorm<ScalarType,MV,OP>::ResType convNorm_, lockNorm_;

  SmartPtr<StatusTest<ScalarType,MV,OP> > globalTest_;
  SmartPtr<StatusTest<ScalarType,MV,OP> > lockingTest_;
  SmartPtr<StatusTest<ScalarType,MV,OP> > debugTest_;

  SmartPtr<BasicOutputManager<ScalarType> > printer_;
};


// Constructor
template<class ScalarType, class MV, class OP>
BlockDavidsonSolMgr<ScalarType,MV,OP>::BlockDavidsonSolMgr(
        const SmartPtr<EigenProblem<ScalarType,MV,OP> >& problem,
        Parameters& pl) :
  problem_(problem),
  whch_("SR"),
  ortho_("SVQB"),
  convtol_(MT::prec()),
  maxRestarts_(20),
  useLocking_(false),
  relconvtol_(true),
  rellocktol_(true),
  blockSize_(0),
  numBlocks_(0),
  numIters_(0),
  maxLocked_(0),
  lockQuorum_(1),
  inSituRestart_(false),
  numRestartBlocks_(1)
{
  internalEigenSolver::testErrorEigenProblem(_smPtrNull == problem_, "Problem not given to solver manager.");
  internalEigenSolver::testErrorEigenProblem(!problem_->isProblemSet(), "Problem not set.");
  internalEigenSolver::testErrorEigenProblem(!problem_->isHermitian(),  "Problem not symmetric.");
  internalEigenSolver::testErrorEigenProblem(problem_->getInitVec() == _smPtrNull, "Problem does not contain initial vectors to clone from.");

  string_t strtmp;

  // which values to solve for
  whch_ = pl.get("Which",whch_);
  internalEigenSolver::testErrorEigenProblem(whch_ != "SM" && whch_ != "LM" && whch_ != "SR" && whch_ != "LR", "Invalid sorting string.");

  // which orthogonalization to use
  ortho_ = pl.get("Orthogonalization",ortho_);
  if (ortho_ != "DGKS" && ortho_ != "SVQB") {
    ortho_ = "SVQB";
  }

  // convergence tolerance
  convtol_ = pl.get("Convergence Tolerance",convtol_);
  relconvtol_ = pl.get("Relative Convergence Tolerance",relconvtol_);
  strtmp = pl.get("Convergence Norm", string_t("2"));
  if (strtmp == "2") {
    convNorm_ = StatusTestResNorm<ScalarType,MV,OP>::_res2Norm;
  }
  else if (strtmp == "M") {
    convNorm_ = StatusTestResNorm<ScalarType,MV,OP>::_resOrth;
  }
  else {
    internalEigenSolver::testErrorEigenProblem(true,
        "xlifepp::BlockDavidsonSolMgr: Invalid Convergence Norm.");
  }

  // locking tolerance
  useLocking_ = pl.get("Use Locking",useLocking_);
  rellocktol_ = pl.get("Relative Locking Tolerance",rellocktol_);
  // default: should be less than convtol_
  locktol_ = convtol_/10;
  locktol_ = pl.get("Locking Tolerance",locktol_);
  strtmp = pl.get("Locking Norm",string_t("2"));
  if (strtmp == "2") {
    lockNorm_ = StatusTestResNorm<ScalarType,MV,OP>::_res2Norm;
  }
  else if (strtmp == "M") {
    lockNorm_ = StatusTestResNorm<ScalarType,MV,OP>::_resOrth;
  }
  else {
    internalEigenSolver::testErrorEigenProblem(true,
        "xlifepp::BlockDavidsonSolMgr: Invalid Locking Norm.");
  }

  // maximum number of restarts
  maxRestarts_ = pl.get("Maximum Restarts",maxRestarts_);

  // block size: default is nev()
  blockSize_ = pl.get("Block Size",problem_->getNEV());
  internalEigenSolver::testErrorEigenProblem(blockSize_ <= 0,
                     "xlifepp::BlockDavidsonSolMgr: \"Block Size\" must be strictly positive.");
  numBlocks_ = pl.get("Num Blocks", int_t(2));
  internalEigenSolver::testErrorEigenProblem(numBlocks_ <= 1,
                     "xlifepp::BlockDavidsonSolMgr: \"Num Blocks\" must be >= 1.");

  // max locked: default is nev(), must satisfy maxLocked_ + blockSize_ >= nev
  if (useLocking_) {
    maxLocked_ = pl.get("Max Locked",problem_->getNEV());
  }
  else {
    maxLocked_ = 0;
  }
  if (maxLocked_ == 0) {
    useLocking_ = false;
  }
  internalEigenSolver::testErrorEigenProblem(maxLocked_ < 0,
                     "xlifepp::BlockDavidsonSolMgr: \"Max Locked\" must be positive.");
  internalEigenSolver::testErrorEigenProblem(maxLocked_ + blockSize_ < problem_->getNEV(),

                     "xlifepp::BlockDavidsonSolMgr: Not enough storage space for requested number of eigenpairs.");
  internalEigenSolver::testErrorEigenProblem(numBlocks_*blockSize_ + maxLocked_ > MVT::getVecLength(*(problem_->getInitVec())),

                     "xlifepp::BlockDavidsonSolMgr: Potentially impossible orthogonality requests. Reduce basis size or locking size.");

  if (useLocking_) {
    lockQuorum_ = pl.get("Locking Quorum",lockQuorum_);
    internalEigenSolver::testErrorEigenProblem(lockQuorum_ <= 0,
                       "xlifepp::BlockDavidsonSolMgr: \"Locking Quorum\" must be strictly positive.");
  }

  // restart size
  numRestartBlocks_ = pl.get("Num Restart Blocks",numRestartBlocks_);
  internalEigenSolver::testErrorEigenProblem(numRestartBlocks_ <= 0,
                     "xlifepp::BlockDavidsonSolMgr: \"Num Restart Blocks\" must be strictly positive.");
  internalEigenSolver::testErrorEigenProblem(numRestartBlocks_ >= numBlocks_,
                     "xlifepp::BlockDavidsonSolMgr: \"Num Restart Blocks\" must be strictly less than \"Num Blocks\".");

  // restarting technique: V*Q or applyHouse(V,H,tau)
  if (pl.contains("In Situ Restarting")) {
      inSituRestart_ = pl.get("In Situ Restarting",inSituRestart_);
  }

  // output stream
  string_t fntemplate = "";
  bool allProcs = false;
//  if (pl.isParameter("Output on all processors")) {
//    if (Teuchos::isParameterType<bool>(pl,"Output on all processors")) {
//      allProcs = pl.get("Output on all processors",allProcs);
//    } else {
//      allProcs = (Teuchos::getParameter<int>(pl,"Output on all processors") != 0);
//    }
//  }
//  fntemplate = pl.get("Output filename template",fntemplate);
  int myPID;
# ifdef HAVE_MPI
    // Initialize MPI
    int mpiStarted = 0;
    MPI_Initialized(&mpiStarted);
    if (mpiStarted) MPI_Comm_rank(MPI_COMM_WORLD, &myPID);
    else myPID=0;
# else
    myPID = 0;
# endif
  if (fntemplate != "") {
    std::ostringstream myPIDstr;
    myPIDstr << myPID;
    // replace %d in fntemplate with myPID
    int pos, start=0;
    while ((pos = fntemplate.find("%d",start)) != -1) {
      fntemplate.replace(pos,2,myPIDstr.str());
      start = pos+2;
    }
  }

  const void* myStrVoid = nullptr;
  SmartPtr<std::ostream> osp;

  if (pl.contains("Write Out Result")) {
      myStrVoid = pl.get("Write Out Result", myStrVoid);
      osp = SmartPtr<std::ostream>((std::ostream*)myStrVoid, false);
  }
  else if (fntemplate != "") {
    osp = SmartPtr<std::ostream>(new std::ofstream(fntemplate.c_str(),std::ios::out | std::ios::app));
    if (!*osp) {
      osp = SmartPtr<std::ostream>(&std::cout, false);
      std::cout << "xlifepp::BlockDavidsonSolMgr::constructor(): Could not open file for write: " << fntemplate << std::endl;
    }
  }
  else {
    osp = SmartPtr<std::ostream>(&std::cout, false);
  }

  // Output manager
  int_t verbosity = _errorsEigen;
  if (pl.contains("Verbosity")) {
      verbosity = pl.get("Verbosity", verbosity);
  }

  if (allProcs) {
    // print on all procs
    printer_ = SmartPtr<BasicOutputManager<ScalarType> >(new BasicOutputManager<ScalarType>(verbosity,osp,myPID));
  }
  else {
    // print only on proc 0
    printer_ = SmartPtr<BasicOutputManager<ScalarType> >(new BasicOutputManager<ScalarType>(verbosity,osp,0));
  }
}

// solve()
template<class ScalarType, class MV, class OP>
ComputationInfo BlockDavidsonSolMgr<ScalarType,MV,OP>::solve()
{
  typedef SolverUtils<ScalarType,MV,OP> msutils;

  const int nev = problem_->getNEV();

  //=============================================================================================
  // Sort manager
  SmartPtr<BasicSort<MagnitudeType> > sorter = SmartPtr<BasicSort<MagnitudeType> >(new BasicSort<MagnitudeType>(whch_));

  //=============================================================================================
  // Status tests
  //
  // convergence
  SmartPtr<StatusTest<ScalarType,MV,OP> > convtest(_smPtrNull);

  if ( _smPtrNull == globalTest_) {
    convtest = SmartPtr<StatusTestResNorm<ScalarType,MV,OP> >(new StatusTestResNorm<ScalarType,MV,OP>(convtol_,nev,convNorm_,relconvtol_));
  }
  else {
    convtest = globalTest_;
  }

  SmartPtr<StatusTestWithOrdering<ScalarType,MV,OP> > ordertest(new StatusTestWithOrdering<ScalarType,MV,OP>(convtest,sorter,nev));
  // locking
  SmartPtr<StatusTest<ScalarType,MV,OP> > locktest(_smPtrNull);
  if (useLocking_) {
    if (_smPtrNull == lockingTest_) {
      locktest = SmartPtr<StatusTestResNorm<ScalarType,MV,OP> >(new StatusTestResNorm<ScalarType,MV,OP>(locktol_,lockQuorum_,lockNorm_,rellocktol_));
    }
    else {
      locktest = lockingTest_;
    }
  }
  // for a non-short-circuited OR test, the order doesn't matter
  std::vector<SmartPtr<StatusTest<ScalarType,MV,OP> > > alltests;
  alltests.push_back(ordertest);
  if (locktest != _smPtrNull) alltests.push_back(locktest);
  if (debugTest_ != _smPtrNull) alltests.push_back(debugTest_);

  SmartPtr<StatusTestCombo<ScalarType,MV,OP> > combotest
      = SmartPtr<StatusTestCombo<ScalarType,MV,OP> >(new StatusTestCombo<ScalarType,MV,OP>(StatusTestCombo<ScalarType,MV,OP>::_OR, alltests));

  // printing StatusTest
  SmartPtr<StatusTestOutput<ScalarType,MV,OP> > outputtest;
  if (printer_->isVerbosity(_debugEigen)) {
    outputtest = SmartPtr<StatusTestOutput<ScalarType,MV,OP> >(new StatusTestOutput<ScalarType,MV,OP>(printer_,combotest,1,_passed+_failed+_undefined));
  }
  else {
    outputtest = SmartPtr<StatusTestOutput<ScalarType,MV,OP> >(new StatusTestOutput<ScalarType,MV,OP>(printer_,combotest,1,_passed));
  }

  //=============================================================================================
  // Orthomanager
  SmartPtr<MatOrthoManager<ScalarType,MV,OP> > ortho(_smPtrNull);
  if ("SVQB" == ortho_) {
    ortho = SmartPtr<SVQBOrthoManager<ScalarType,MV,OP> >(new SVQBOrthoManager<ScalarType,MV,OP>(problem_->getM()));
  } else if (ortho_=="DGKS") {
    ortho = SmartPtr<BasicOrthoManager<ScalarType,MV,OP> >(new BasicOrthoManager<ScalarType,MV,OP>(problem_->getM()));
  } else {
    internalEigenSolver::testErrorEigenProblem(ortho_!="SVQB"&& ortho_!="DGKS","xlifepp::BlockDavidsonSolMgr::solve(): Invalid orthogonalization type.");
  }

  //=============================================================================================
  // Parameter list
  Parameters plist;
  plist.add("Block Size",blockSize_);
  plist.add("Num Blocks",numBlocks_);

  //=============================================================================================
  // BlockDavidson solver
  SmartPtr<BlockDavidson<ScalarType,MV,OP> > bdSolver
      = SmartPtr<BlockDavidson<ScalarType,MV,OP> >(new BlockDavidson<ScalarType,MV,OP>(problem_,sorter,printer_,outputtest,ortho,plist));

  // set any auxiliary vectors defined in the problem
  SmartPtr<const MV>  probauxvecs = problem_->getAuxVecs();
  if (probauxvecs != _smPtrNull) {
    //bdSolver->setAuxVecs(Teuchos::tuple< SmartPtr<const MV> >(probauxvecs));
    bdSolver->setAuxVecs(std::vector<SmartPtr<const MV> >(1, probauxvecs));
  }

  //=============================================================================================
  // Storage
  //
  // lockvecs will contain eigenvectors that have been determined "locked" by the status test
  int curNumLocked = 0;
  SmartPtr<MV> lockvecs;
  // lockvecs is used to hold the locked eigenvectors, as well as for temporary storage when locking.
  // when locking, we will lock some number of vectors numnew, where numnew <= maxlocked - curlocked
  // we will produce numnew random vectors, which will go into the space with the new basis.
  // we will also need numnew storage for the image of these random vectors under A and M;
  // columns [curlocked+1,curlocked+numnew] will be used for this storage
  if (maxLocked_ > 0) {
    lockvecs = MVT::clone(*problem_->getInitVec(),maxLocked_);
  }
  std::vector<MagnitudeType> lockvals;
  //
  // Restarting occurs under two scenarios: when the basis is full and after locking.
  //
  // For the former, a new basis of size blockSize*numRestartBlocks is generated using the current basis
  // and the most significant primitive Ritz vectors (projected eigenvectors).
  //     [S,L] = eig(KK)
  //     S = [Sr St]   // some for "r"estarting, some are "t"runcated
  //     newV = V*Sr
  //     KK_new = newV'*K*newV = Sr'*V'*K*V*Sr = Sr'*KK*Sr
  //  Therefore, the only multivector operation needed is for the generation of newV.
  //
  //  * If the multiplication is explicit, it requires a workspace of blockSize*numRestartBlocks vectors.
  //    This space must be specifically allocated for that task, as we don't have any space of that size.
  //    It (workMV) will be allocated at the beginning of solve()
  //  * Optionally, the multiplication can be performed implicitly, via a Householder QR factorization of
  //    Sr. This can be done in situ, using the basis multivector contained in the solver. This requires
  //    that we cast away the const on the multivector returned from getState(). Workspace for this approach
  //    is a single vector. the solver's internal storage must be preserved (X,MX,KX,R), requiring us to
  //    allocate this vector.
  //
  // For the latter (restarting after locking), the new basis is the same size as existing basis. If numnew
  // vectors are locked, they are deflated from the current basis and replaced with randomly generated
  // vectors.
  //     [S,L] = eig(KK)
  //     S = [Sl Su]  // partitioned: "l"ocked and "u"nlocked
  //     newL = V*Sl = X(locked)
  //     defV = V*Su
  //     augV = rand(numnew)  // orthogonal to oldL,newL,defV,auxvecs
  //     newV = [defV augV]
  //     Kknew = newV'*K*newV = [Su'*KK*Su    defV'*K*augV]
  //                            [augV'*K*defV augV'*K*augV]
  //     locked = [oldL newL]
  // Clearly, this operation is more complicated than the previous.
  // Here is a list of the significant computations that need to be performed:
  // - newL will be put into space in lockvecs, but will be copied from getState().X at the end
  // - defV,augV will be stored in workspace the size of the current basis.
  //   - If inSituRestart==true, we compute defV in situ in bdSolver::V_ and
  //     put augV at the end of bdSolver::V_
  //   - If inSituRestart==false, we must have curDim vectors available for
  //     defV and augV; we will allocate a multivector (workMV) at the beginning of solve()
  //     for this purpose.
  // - M*augV and K*augV are needed; they will be stored in lockvecs. As a result, newL will
  //   not be put into lockvecs until the end.
  //
  // Therefore, we must allocate workMV when ((maxRestarts_ > 0) || (useLocking_ == true)) && inSituRestart == false
  // It will be allocated to size (numBlocks-1)*blockSize
  //
  SmartPtr<MV> workMV;
  if (inSituRestart_ == false) {
    // we need storage space to restart, either if we may lock or if may restart after a full basis
    if (useLocking_== true || maxRestarts_ > 0) {
      workMV = MVT::clone(*problem_->getInitVec(),(numBlocks_-1)*blockSize_);
    }
    else {
      // we will never need to restart.
      workMV = _smPtrNull;
    }
  }
  else { // inSituRestart_ == true
    // we will restart in situ, if we need to restart
    // three situation remain:
    // - never restart                                       => no space needed
    // - only restart for locking (i.e., never restart full) => no space needed
    // - restart for full basis                              => need one vector
    if (maxRestarts_ > 0) {
      workMV = MVT::clone(*problem_->getInitVec(),1);
    }
    else {
      workMV = _smPtrNull;
    }
  }

  // some consts and utils
  const ScalarType ONE = SCT::one();
  const ScalarType ZERO = SCT::zero();

  // go ahead and initialize the solution to nothing in case we throw an exception
  EigenSolverSolution<ScalarType,MV> sol;
  sol.numVecs = 0;
  problem_->setSolution(sol);

  int numRestarts = 0;

  // enter solve() iterations
  {
    // tell bdSolver to iterate
    while (1) {
      bdSolver->iterate();

      //=============================================================================================
      //
      // check user-specified debug test; if it passed, return an exception
      //
      //=============================================================================================
      // Not this time!!!!!!!!
      if (debugTest_ != _smPtrNull && debugTest_->getStatus() == _passed) {
         internalEigenSolver::testErrorEigenProblem(debugTest_ != _smPtrNull && debugTest_->getStatus() == _passed, "xlifepp::BlockDavidsonSolMgr::solve(): User-specified debug status test returned _passed.");
      }
      //=============================================================================================
      //
      // check convergence next
      //
      //=============================================================================================
      else if (_passed == ordertest->getStatus()) {
        // we have convergence
        // ordertest->whichVecs() tells us which vectors from lockvecs and solver state are the ones we want
        // ordertest->howMany() will tell us how many
        break;
      }
      //=============================================================================================
      //
      // check for restarting before locking: if we need to lock, it will happen after the restart
      //
      //=============================================================================================

      else if (bdSolver->getCurSubspaceDim() == bdSolver->getMaxSubspaceDim()) {
        if (numRestarts >= maxRestarts_) {
          break; // break from while(1){bdSolver->iterate()}
        }
        numRestarts++;

        if (printer_->isVerbosity(_iterationDetailsEigen)) {
            printer_->stream(_iterationDetailsEigen) << " Performing restart number " << numRestarts << " of " << maxRestarts_ << std::endl << std::endl;
        }

        BlockDavidsonState<ScalarType,MV> state = bdSolver->getState();
        int curdim = state.curDim;
        int newdim = numRestartBlocks_*blockSize_;

/*      // For the debugging purpose
        {
          std::vector<ValueEigenSolver<ScalarType> ritzvalues = bdSolver->getRitzValues();
          std::cout << "Ritz values from solver:\n";
          for (unsigned int i=0; i<ritzvalues.size(); i++) {
            std::cout << ritzvalues[i].realpart << " ";
          }
          std::cout << "\n";
        }
*/

        // compute eigenvectors of the projected problem
        MatrixEigenDense<ScalarType> S(curdim,curdim);
        std::vector<MagnitudeType> theta(curdim);
        int rank = curdim;

/*      // For the debugging purpose
        {
          std::stringstream os;
          os << "KK before HEEV...\n"
            << *state.KK << "\n";
          std::cout << os.str();
        }
*/
        int info = msutils::directSolver(curdim,*(state.KK),_smPtrNull,S,theta,rank,10);
        internalEigenSolver::testErrorEigenProblem(info != 0     ,
            "xlifepp::BlockDavidsonSolMgr::solve(): error calling SolverUtils::directSolver.");       // this should never happen
        internalEigenSolver::testErrorEigenProblem(rank != curdim,
            "xlifepp::BlockDavidsonSolMgr::solve(): direct solve did not compute all eigenvectors."); // this should never happen

        // For the debugging purpose
       // {
       //   std::stringstream os;
       //   std::cout << "Ritz values from heev(KK):\n";
       //   for (unsigned int i=0; i<theta.size(); i++) std::cout << theta[i] << " ";
       //   os << "\nRitz vectors from heev(KK):\n"
       //                                        << S << "\n";
       //   std::cout << os.str();
       // }

        //
        // sort the eigenvalues (so that we can order the eigenvectors)
        {
          std::vector<int> order(curdim);
          sorter->sort(theta,smartPtrFromRef(&order),curdim);

          // apply the same ordering to the primitive ritz vectors
          msutils::permuteVectors(order,S);
        }

/* // For the debugging purpose
        {
          std::stringstream os;
          std::cout << "Ritz values from heev(KK) after sorting:\n";
          std::copy(theta.begin(), theta.end(), std::ostream_iterator<ScalarType>(std::cout, " "));
          os << "\nRitz vectors from heev(KK) after sorting:\n"
            << S << "\n";
          std::cout << os.str();
        }
*/

        //
        // select the significant primitive ritz vectors
        //Teuchos::SerialDenseMatrix<int,ScalarType> Sr(Teuchos::View,S,curdim,newdim);
        MatrixEigenDense<ScalarType> Sr(S, 0, 0, curdim, newdim);

/* // For the debugging purpose
        {
          std::stringstream os;
          os << "Significant primitive Ritz vectors:\n"
            << Sr << "\n";
          std::cout << os.str();
        }
*/
        //
        // generate newKK = Sr'*KKold*Sr
        MatrixEigenDense<ScalarType> newKK(newdim,newdim);
        {
          MatrixEigenDense<ScalarType> KKtmp(curdim,newdim);
          //KKold(Teuchos::View,*state.KK,curdim,curdim);
          MatrixEigenDense<ScalarType> KKold(*(state.KK), 0, 0, curdim, curdim);

          // KKtmp = KKold*Sr
          // teuchosRet = KKtmp.multiply(Teuchos::NO_TRANS,Teuchos::NO_TRANS,ONE,KKold,Sr,ZERO);
          multMatMat(KKold,Sr,KKtmp);

          // newKK = Sr'*KKtmp = Sr'*KKold*Sr
          //teuchosRet = newKK.multiply(Teuchos::CONJ_TRANS,Teuchos::NO_TRANS,ONE,Sr,KKtmp,ZERO);
          multMatMat(conj(transpose(Sr)), KKtmp, newKK);
        }

/* // For the debugging purpose
        {
          std::stringstream os;
          os << "Sr'*KK*Sr:\n"
            << newKK << "\n";
          std::cout << os.str();
        }
*/

        // prepare new state
        BlockDavidsonState<ScalarType,MV> rstate;
        rstate.curDim = newdim;
        rstate.KK = smartPtrFromRef(&newKK);

        //
        // we know that newX = newV*Sr(:,1:bS) = oldV*S(:1:bS) = oldX
        // the restarting preserves the Ritz vectors and residual
        // for the Ritz values, we want all of the values associated with newV.
        // these have already been placed at the beginning of theta
        rstate.X  = state.X;
        rstate.KX = state.KX;
        rstate.MX = state.MX;
        rstate.T  = SmartPtr<std::vector<MagnitudeType> >(new std::vector<MagnitudeType>(theta.begin(),theta.begin()+newdim));

        if (inSituRestart_ == true) {
          //
          // get non-const pointer to solver's basis so we can work in situ
          SmartPtr<MV> solverbasis = smartPtrConstCast<const MV>(state.V);

          //
          // perform Householder QR of Sr = Q [D;0], where D is unit diag.
          // WARNING: this will overwrite Sr; however, we do not need Sr anymore after this
          std::vector<ScalarType> tau(newdim), work(newdim);
          // lapack.GEQRF(curdim,newdim,Sr.values(),Sr.stride(),&tau[0],&work[0],work.size(),&geqrf_info);
          HouseholderQR<MatrixDense> geqrf(Sr);
          Sr = geqrf.matrixQR();

          if (printer_->isVerbosity(_debugEigen)) {
            MatrixEigenDense<ScalarType> R(Sr,0,0,newdim,newdim);
            for (int j=0; j<newdim; j++) {
              R.coeffRef(j,j) = SCT::magnitude(R.coeff(j,j)) - 1.0;
              for (int i=j+1; i<newdim; i++) {
                R.coeffRef(i,j) = ZERO;
              }
            }
            printer_->stream(_debugEigen) << "||Triangular factor of Sr - I||: " << R.normFrobenius() << std::endl;
          }

          //
          // perform implicit oldV*Sr
          // this actually performs oldV*[Sr Su*M] = [newV truncV], for some unitary M
          // we are actually interested in only the first newdim vectors of the result
          {
            std::vector<int> curind(curdim);
            for (int i=0; i<curdim; i++) curind[i] = i;
            SmartPtr<MV> oldV = MVT::cloneViewNonConst(*solverbasis,curind);
            msutils::applyHouse(newdim,*oldV,Sr,tau,workMV);
          }
          //
          // put the new basis into the state for initialize()
          // the new basis is contained in the the first newdim columns of solverbasis
          // initialize() will recognize that pointer bdSolver.V_ == pointer rstate.V, and will neglect the copy.
          rstate.V = solverbasis;
        }
        else { // inSituRestart == false)
          // newV = oldV*Sr, explicitly. workspace is in workMV
          std::vector<int> curind(curdim), newind(newdim);
          for (int i=0; i<curdim; i++) curind[i] = i;
          for (int i=0; i<newdim; i++) newind[i] = i;
          SmartPtr<const MV> oldV = MVT::cloneView(*state.V,curind);
          SmartPtr<MV>       newV = MVT::cloneViewNonConst(*workMV ,newind);

          MVT::mvTimesMatAddMv(ONE,*oldV,Sr,ZERO,*newV);
          //
          // put the new basis into the state for initialize()
          rstate.V = newV;
        }

        //
        // send the new state to the solver
        bdSolver->initialize(rstate);
      } // end of restarting
      //=============================================================================================
      //
      // check locking if we didn't converge or restart
      //
      //=============================================================================================
      else if (locktest != _smPtrNull && locktest->getStatus() == _passed) {
        //
        // get current state
        BlockDavidsonState<ScalarType,MV> state = bdSolver->getState();
        const int curdim = state.curDim;

        //
        // get number,indices of vectors to be locked
        internalEigenSolver::testErrorEigenProblem(locktest->howMany() <= 0,
            "xlifepp::BlockDavidsonSolMgr::solve(): status test mistake: howMany() non-positive.");
        internalEigenSolver::testErrorEigenProblem(locktest->howMany() != (int)locktest->whichVecs().size(),
            "xlifepp::BlockDavidsonSolMgr::solve(): status test mistake: howMany() not consistent with whichVecs().");
        // we should have room to lock vectors; otherwise, locking should have been deactivated
        internalEigenSolver::testErrorEigenProblem(curNumLocked == maxLocked_,
            "xlifepp::BlockDavidsonSolMgr::solve(): status test mistake: locking not deactivated.");
        //
        // don't lock more than maxLocked_; we didn't allocate enough space.
        std::vector<int> tmpVectorInt;
        if (curNumLocked + locktest->howMany() > maxLocked_) {
          // just use the first of them
          tmpVectorInt.resize(maxLocked_-curNumLocked);
          std::vector<int> tmp(locktest->whichVecs());
          std::copy(tmp.begin(), tmp.begin()+(maxLocked_-curNumLocked), tmpVectorInt.begin());
        }
        else {
          tmpVectorInt = locktest->whichVecs();
        }
        const std::vector<int> lockind(tmpVectorInt);
        const int numNewLocked = lockind.size();
        //
        // generate indices of vectors left unlocked
        // curind = [0,...,curdim-1] = UNION(lockind, unlockind)
        const int numUnlocked = curdim-numNewLocked;
        tmpVectorInt.resize(curdim);
        for (int i=0; i<curdim; i++) tmpVectorInt[i] = i;
        const std::vector<int> curind(tmpVectorInt);       // curind = [0 ... curdim-1]
        tmpVectorInt.resize(numUnlocked);
        std::set_difference(curind.begin(),curind.end(),lockind.begin(),lockind.end(),tmpVectorInt.begin());
        const std::vector<int> unlockind(tmpVectorInt);    // unlockind = [0 ... curdim-1] - lockind
        tmpVectorInt.clear();

        //
        // debug printing
        if (printer_->isVerbosity(_debugEigen)) {
          printer_->print(_debugEigen,"Locking vectors: ");
          for (unsigned int i=0; i<lockind.size(); i++) {printer_->stream(_debugEigen) << " " << lockind[i];}
          printer_->print(_debugEigen,"\n");
          printer_->print(_debugEigen,"Not locking vectors: ");
          for (unsigned int i=0; i<unlockind.size(); i++) {printer_->stream(_debugEigen) << " " << unlockind[i];}
          printer_->print(_debugEigen,"\n");
        }

        //
        // we need primitive ritz vectors/values:
        // [S,L] = eig(oldKK)
        //
        // this will be partitioned as follows:
        //   locked: Sl = S(lockind)      // we won't actually need Sl
        // unlocked: Su = S(unlockind)
        //
        MatrixEigenDense<ScalarType> S(curdim,curdim);
        std::vector<MagnitudeType> theta(curdim);
        {
          int rank = curdim;
          int info = msutils::directSolver(curdim,(*state.KK),(_smPtrNull),S,theta,rank,10);
          internalEigenSolver::testErrorEigenProblem(info != 0     ,
              "xlifepp::BlockDavidsonSolMgr::solve(): error calling SolverUtils::directSolver.");       // this should never happen
          internalEigenSolver::testErrorEigenProblem(rank != curdim,
              "xlifepp::BlockDavidsonSolMgr::solve(): direct solve did not compute all eigenvectors."); // this should never happen
          //
          // sort the eigenvalues (so that we can order the eigenvectors)
          std::vector<int> order(curdim);
          sorter->sort(theta,smartPtrFromRef(&order),curdim);
          //
          // apply the same ordering to the primitive ritz vectors
          msutils::permuteVectors(order,S);
        }
        //
        // select the unlocked ritz vectors
        // the indexing in unlockind is relative to the ordered primitive ritz vectors
        // (this is why we ordered theta,S above)
        MatrixEigenDense<ScalarType> Su(curdim,numUnlocked);
        for (int i=0; i<numUnlocked; i++) {
          for (int j = 0; j < curdim; ++j) {
            Su.coeffRef(j,i) = S.coeff(j,unlockind[i]);
          }
        }


        //
        // newV has the following form:
        // newV = [defV augV]
        // - defV will be of size curdim - numNewLocked, and contain the generated basis: defV = oldV*Su
        // - augV will be of size numNewLocked, and contain random directions to make up for the lost space
        //
        // we will need a pointer to defV below to generate the off-diagonal block of newKK
        // go ahead and setup pointer to augV
        //
        SmartPtr<MV> defV, augV;
        if (true == inSituRestart_) {
          //
          // get non-const pointer to solver's basis so we can work in situ
          SmartPtr<MV> solverbasis = smartPtrConstCast<const MV>(state.V);

          //
          // perform Householder QR of Su = Q [D;0], where D is unit diag.
          // work on a copy of Su, since we need Su below to build newKK
          MatrixEigenDense<ScalarType> copySu; //(Su);
          std::vector<ScalarType> tau(numUnlocked), work(numUnlocked);
          // lapack.GEQRF(curdim,numUnlocked,copySu.values(),copySu.stride(),&tau[0],&work[0],work.size(),&info);
          HouseholderQR<MatrixDense> geqrf(Su);
          copySu = geqrf.matrixQR();

          if (printer_->isVerbosity(_debugEigen)) {
            MatrixEigenDense<ScalarType> R(copySu,0,0,numUnlocked,numUnlocked);
            for (int j=0; j<numUnlocked; j++) {
              R.coeffRef(j,j) = SCT::magnitude(R.coeff(j,j)) - 1.0;
              for (int i=j+1; i<numUnlocked; i++) {
                R.coeffRef(i,j) = ZERO;
              }
            }
            printer_->stream(_debugEigen) << "||Triangular factor of Su - I||: " << R.normFrobenius() << std::endl;
          }

          //
          // perform implicit oldV*Su
          // this actually performs oldV*[Su Sl*M] = [defV lockV], for some unitary M
          // we are actually interested in only the first numUnlocked vectors of the result
          {
            SmartPtr<MV> oldV = MVT::cloneViewNonConst(*solverbasis,curind);
            msutils::applyHouse(numUnlocked,*oldV,copySu,tau,workMV);
          }
          std::vector<int> defind(numUnlocked), augind(numNewLocked);
          for (int i=0; i<numUnlocked ; i++) defind[i] = i;
          for (int i=0; i<numNewLocked; i++) augind[i] = numUnlocked+i;
          defV = MVT::cloneViewNonConst(*solverbasis,defind);
          augV = MVT::cloneViewNonConst(*solverbasis,augind);
        }
        else { // inSituRestart == false)
          // defV = oldV*Su, explicitly. workspace is in workMV
          std::vector<int> defind(numUnlocked), augind(numNewLocked);
          for (int i=0; i<numUnlocked ; i++) defind[i] = i;
          for (int i=0; i<numNewLocked; i++) augind[i] = numUnlocked+i;
          SmartPtr<const MV> oldV = MVT::cloneView(*state.V,curind);
          defV = MVT::cloneViewNonConst(*workMV,defind);
          augV = MVT::cloneViewNonConst(*workMV,augind);

          MVT::mvTimesMatAddMv(ONE,*oldV,Su,ZERO,*defV);
        }

        //
        // lockvecs will be partitioned as follows:
        // lockvecs = [curlocked augTmp ...]
        // - augTmp will be used for the storage of M*augV and K*augV
        //   later, the locked vectors (stored in state.X and referenced via const SmartPtr<MV> view newLocked)
        //   will be moved into lockvecs on top of augTmp when it is no longer needed as workspace.
        // - curlocked will be used in orthogonalization of augV
        //
        // newL is the new locked vectors; newL = oldV*Sl = RitzVectors(lockind)
        // we will not produce them, but instead retrieve them from RitzVectors
        //
        SmartPtr<const MV> curlocked, newLocked;
        SmartPtr<MV> augTmp;
        {
          // setup curlocked
          if (curNumLocked > 0) {
            std::vector<int> curlockind(curNumLocked);
            for (int i=0; i<curNumLocked; i++) curlockind[i] = i;
            curlocked = MVT::cloneView(*lockvecs,curlockind);
          }
          else {
            curlocked = _smPtrNull;
          }
          // setup augTmp
          std::vector<int> augtmpind(numNewLocked);
          for (int i=0; i<numNewLocked; i++) augtmpind[i] = curNumLocked+i;
          augTmp = MVT::cloneViewNonConst(*lockvecs,augtmpind);
          // setup newLocked
          newLocked = MVT::cloneView(*bdSolver->getRitzVectors(),lockind);
        }

        //
        // generate augV and perform orthogonalization
        //
        MVT::mvRandom(*augV);
        //
        // orthogonalize it against auxvecs, defV, and all locked vectors (new and current)
        // use augTmp as storage for M*augV, if hasM
        {
          std::vector<SmartPtr<const MV> > against;
          std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > dummyC;
          if (probauxvecs != _smPtrNull) against.push_back(probauxvecs);
          if (curlocked != _smPtrNull)   against.push_back(curlocked);
          against.push_back(newLocked);
          against.push_back(defV);
          if (problem_->getM() != _smPtrNull) {
            OPT::apply(*problem_->getM(),*augV,*augTmp);
          }
          ortho->projectAndNormalizeMat(*augV,against,dummyC,(_smPtrNull),augTmp);
        }

        //
        // form newKK
        //
        // newKK = newV'*K*newV = [Su'*KK*Su    defV'*K*augV]
        //                        [augV'*K*defV augV'*K*augV]
        //
        // first, generate the principal submatrix, the projection of K onto the unlocked portion of oldV
        //
        MatrixEigenDense<ScalarType> newKK(curdim,curdim);
        {
          MatrixEigenDense<ScalarType> KKtmp(curdim,numUnlocked);
          // KKold(Teuchos::View,*state.KK,curdim,curdim),
          MatrixEigenDense<ScalarType> KKold(*state.KK, 0, 0, curdim,curdim);
          // KK11(Teuchos::View,newKK,numUnlocked,numUnlocked);
          MatrixEigenDense<ScalarType> KK11(newKK, 0, 0, numUnlocked,numUnlocked);

          // KKtmp = KKold*Su
          // teuchosRet = KKtmp.multiply(Teuchos::NO_TRANS,Teuchos::NO_TRANS,ONE,KKold,Su,ZERO);
          multMatMat(KKold, Su, KKtmp);

          // KK11 = Su'*KKtmp = Su'*KKold*Su
          // teuchosRet = KK11.multiply(Teuchos::CONJ_TRANS,Teuchos::NO_TRANS,ONE,Su,KKtmp,ZERO);
          multMatMat(conj(transpose(Su)), KKtmp, KK11);
          newKK.replace(KK11, 0, 0, numUnlocked,numUnlocked); // FIXME Need to replace in the future
        }

        //
        // project the stiffness matrix on augV
        {
          OPT::apply(*problem_->getOperator(),*augV,*augTmp);
          //KK12(Teuchos::View,newKK,numUnlocked,numNewLocked,0,numUnlocked),
          MatrixEigenDense<ScalarType> KK12(newKK,0,numUnlocked,numUnlocked,numNewLocked),
            //KK22(Teuchos::View,newKK,numNewLocked,numNewLocked,numUnlocked,numUnlocked);
                                       KK22(newKK,numUnlocked,numUnlocked,numNewLocked,numNewLocked);
          MVT::mvTransMv(ONE,*defV,*augTmp,KK12);
          MVT::mvTransMv(ONE,*augV,*augTmp,KK22);
          newKK.replace(KK12,0,numUnlocked,numUnlocked,numNewLocked);
          newKK.replace(KK22,numUnlocked,numUnlocked,numNewLocked,numNewLocked);
        }

        //
        // done with defV,augV
        defV = _smPtrNull;
        augV = _smPtrNull;
        //
        // make it hermitian in memory (fill in KK21)
//          for (int j=0; j<curdim; ++j) {
//            for (int i=j+1; i<curdim; ++i) {
//              newKK.coeffRef(i,j) = SCT::conjugate(newKK.coeff(j,i));
//            }
//          }

        // Better way to fill in KK21?
        for (int j=numUnlocked; j<curdim; ++j) {
          for (int i=0; i<numUnlocked; ++i) {
            newKK.coeffRef(j,i) = SCT::conjugate(newKK.coeff(i,j));
          }
        }

        //
        // we are done using augTmp as storage
        // put newLocked into lockvecs, new values into lockvals
        augTmp = _smPtrNull;
        {
          std::vector<ValueEigenSolver<ScalarType> > allvals = bdSolver->getRitzValues();
          for (int i=0; i<numNewLocked; i++) {
            lockvals.push_back(allvals[lockind[i]].realpart);
          }

          std::vector<int> indlock(numNewLocked);
          for (int i=0; i<numNewLocked; i++) indlock[i] = curNumLocked+i;
          MVT::setBlock(*newLocked,indlock,*lockvecs);
          newLocked = _smPtrNull;

          curNumLocked += numNewLocked;
          std::vector<int> curlockind(curNumLocked);
          for (int i=0; i<curNumLocked; i++) curlockind[i] = i;
          curlocked = MVT::cloneView(*lockvecs,curlockind);
        }
        // add locked vecs as aux vecs, along with aux vecs from problem
        // add lockvals to ordertest
        // disable locktest if curNumLocked == maxLocked
        {
          ordertest->setAuxVals(lockvals);

          std::vector< SmartPtr<const MV> > aux;
          if (probauxvecs != _smPtrNull) aux.push_back(probauxvecs);
          aux.push_back(curlocked);
          bdSolver->setAuxVecs(aux);

          if (curNumLocked == maxLocked_) {
            // disabled locking now
            combotest->removeTest(locktest);
          }
        }

        //
        // prepare new state
        BlockDavidsonState<ScalarType,MV> rstate;
        rstate.curDim = curdim;
        if (inSituRestart_) {
          // data is already in the solver's memory
          rstate.V = state.V;
        }
        else {
          // data is in workspace and will be copied to solver memory
          rstate.V = workMV;
        }
        rstate.KK = smartPtrFromRef(&newKK);

        //
        // pass new state to the solver
        bdSolver->initialize(rstate);
      } // end of locking
      //=============================================================================================
      //
      // we returned from iterate(), but none of our status tests _passed.
      // something is wrong, and it is probably our fault.
      //
      //=============================================================================================
      else {
        internalEigenSolver::testErrorEigenProblem(true,"xlifepp::BlockDavidsonSolMgr::solve(): Invalid return from bdSolver::iterate().");
      }
    }

    // clear temp space
    workMV = _smPtrNull;

    sol.numVecs = ordertest->howMany();
    if (sol.numVecs > 0) {
      sol.evecs = MVT::clone(*(problem_->getInitVec()),sol.numVecs);
      sol.espace = sol.evecs;
      sol.evals.resize(sol.numVecs);
      std::vector<MagnitudeType> vals(sol.numVecs);

      // copy them into the solution
      std::vector<int> which = ordertest->whichVecs();
      // indices between [0,blockSize) refer to vectors/values in the solver
      // indices between [-curNumLocked,-1] refer to locked vectors/values [0,curNumLocked)
      // everything has already been ordered by the solver; we just have to partition the two references
      std::vector<int> inlocked(0), insolver(0);
      for (unsigned int i=0; i<which.size(); i++) {
        if (which[i] >= 0) {
          internalEigenSolver::testErrorEigenProblem(which[i] >= blockSize_,"xlifepp::BlockDavidsonSolMgr::solve(): positive indexing mistake from ordertest.");
          insolver.push_back(which[i]);
        }
        else {
          // sanity check
          internalEigenSolver::testErrorEigenProblem(which[i] < -curNumLocked,"xlifepp::BlockDavidsonSolMgr::solve(): negative indexing mistake from ordertest.");
          inlocked.push_back(which[i] + curNumLocked);
        }
      }

      internalEigenSolver::testErrorEigenProblem(insolver.size() + inlocked.size() != (unsigned int)sol.numVecs,"xlifepp::BlockDavidsonSolMgr::solve(): indexing mistake.");

      // set the vecs,vals in the solution
      if (insolver.size() > 0) {
        // set vecs
        int lclnum = insolver.size();
        std::vector<int> tosol(lclnum);
        for (int i=0; i<lclnum; i++) tosol[i] = i;
        SmartPtr<const MV> v = MVT::cloneView(*bdSolver->getRitzVectors(),insolver);
        MVT::setBlock(*v,tosol,*(sol.evecs));
        // set vals
        std::vector<ValueEigenSolver<ScalarType> > fromsolver = bdSolver->getRitzValues();
        for (unsigned int i=0; i<insolver.size(); i++) {
          vals[i] = fromsolver[insolver[i]].realpart;
        }
      }

      // get the vecs,vals from locked storage
      if (inlocked.size() > 0) {
        int solnum = insolver.size();
        // set vecs
        int lclnum = inlocked.size();
        std::vector<int> tosol(lclnum);
        for (int i=0; i<lclnum; i++) tosol[i] = solnum + i;
        SmartPtr<const MV> v = MVT::cloneView(*lockvecs,inlocked);
        MVT::setBlock(*v,tosol,*(sol.evecs));
        // set vals
        for (unsigned int i=0; i<inlocked.size(); i++) {
          vals[i+solnum] = lockvals[inlocked[i]];
        }
      }

      // sort the eigenvalues and permute the eigenvectors appropriately
      {
        std::vector<int> order(sol.numVecs);
        //sorter->sort(vals,SmartPtrFromRef(order),sol.numVecs);
        sorter->sort(vals,smartPtrFromRef(&order),sol.numVecs);
        // store the values in the EigenSolverSolution
        for (int i=0; i<sol.numVecs; i++) {
          sol.evals[i].realpart = vals[i];
          sol.evals[i].imagpart = MT::zero();
        }
        // now permute the eigenvectors according to order
        msutils::permuteVectors(sol.numVecs,order,*(sol.evecs));
      }

      // setup sol.index, remembering that all eigenvalues are real so that index = {0,...,0}
      sol.index.resize(sol.numVecs,0);
    }
  }

  // print final summary
  if (printer_->isVerbosity(_finalSummaryEigen)) {
      bdSolver->currentStatus(printer_->stream(_finalSummaryEigen));
  }

  problem_->setSolution(sol);
  if (printer_->isVerbosity(_debugEigen)) {
      printer_->stream(_debugEigen) << "Returning " << sol.numVecs << " eigenpairs to eigenproblem." << std::endl;
  }

  // get the number of iterations taken for this call to solve().
  numIters_ = bdSolver->getNumIters();

  if (sol.numVecs < nev) {
    return _noConvergence; // return from BlockDavidsonSolMgr::solve()
  } else return _success; // return from BlockDavidsonSolMgr::solve()
}


template <class ScalarType, class MV, class OP>
void
BlockDavidsonSolMgr<ScalarType,MV,OP>::setGlobalStatusTest(
    const SmartPtr<StatusTest<ScalarType,MV,OP> >& global)
{
  globalTest_ = global;
}

template <class ScalarType, class MV, class OP>
const SmartPtr<StatusTest<ScalarType,MV,OP> >&
BlockDavidsonSolMgr<ScalarType,MV,OP>::getGlobalStatusTest() const
{
  return globalTest_;
}

template <class ScalarType, class MV, class OP>
void
BlockDavidsonSolMgr<ScalarType,MV,OP>::setDebugStatusTest(
    const SmartPtr< StatusTest<ScalarType,MV,OP> >& debug)
{
  debugTest_ = debug;
}

template <class ScalarType, class MV, class OP>
const SmartPtr< StatusTest<ScalarType,MV,OP> >&
BlockDavidsonSolMgr<ScalarType,MV,OP>::getDebugStatusTest() const
{
  return debugTest_;
}

template <class ScalarType, class MV, class OP>
void
BlockDavidsonSolMgr<ScalarType,MV,OP>::setLockingStatusTest(
    const SmartPtr< StatusTest<ScalarType,MV,OP> >& locking)
{
  lockingTest_ = locking;
}

template <class ScalarType, class MV, class OP>
const SmartPtr< StatusTest<ScalarType,MV,OP> >&
BlockDavidsonSolMgr<ScalarType,MV,OP>::getLockingStatusTest() const
{
  return lockingTest_;
}

} // end xlifepp namespace

#endif /* XLIFEPP_BLOCKDAVIDSON_SOLMGR_HPP */
