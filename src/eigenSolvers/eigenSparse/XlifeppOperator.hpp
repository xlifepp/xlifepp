/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppOperator.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013
  
  \brief Templated virtual class for creating operators that can interface with xlifepp::OperatorTraits class
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_OPERATOR_HPP
#define XLIFEPP_OPERATOR_HPP

#include "config.h"
#include "XlifeppOperatorTraits.hpp"
#include "XlifeppMultiVec.hpp"

namespace xlifepp {
  
/*!	
	\class Operator
	\brief xlifepp's templated virtual class for constructing an operator that can interface with the
	OperatorTraits class used by the eigensolvers.
	
	A concrete implementation of this class is necessary.  The user can create their own implementation
	if those supplied are not suitable for their needs.
*/
template <class ScalarType>
class Operator {
  public:
    //! @name Constructor/Destructor
    //@{ 
    //! Default constructor.
    Operator() {}
    
    //! Destructor.
    virtual ~Operator() {}
    //@}
    
    //! @name Operator application method
    //@{ 
    
    /*! \brief This method takes the MultiVec \c x and
      applies the operator to it resulting in the MultiVec \c y.
    */

    virtual void apply (const MultiVec<ScalarType>& x, MultiVec<ScalarType>& y) const = 0;

    //@}
};
  
//==================================================================
// Implementation of the OperatorTraits for Operator and MultiVec.
//==================================================================
#ifndef DOXYGEN_SHOULD_SKIP_THIS

/*
  \brief Template specialization of OperatorTraits class using Operator and MultiVec virtual
  base classes.

  Any class that inherits from Operator will be accepted by the xlifepp templated solvers due to this
  interface to the OperatorTraits class.
*/
template <class ScalarType> 
class OperatorTraits < ScalarType, MultiVec<ScalarType>, Operator<ScalarType> > 
{
public:

  //! @name Operator application method
  //@{ 
  /*! \brief This method takes the MultiVec \c x and
    applies the Operator \c Op to it resulting in the MultiVec \c y.
  */
  static void apply (const Operator<ScalarType>& Op,
		      const MultiVec<ScalarType>& x, 
		      MultiVec<ScalarType>& y)
  { Op.apply(x, y); }
  //@}
  
};

#endif
  
} // end of namespace xlifepp

#endif /* XLIFEPP_OPERATOR_HPP */
