/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppBasicOutputManager.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013
  
  \brief Basic output manager for sending information of select verbosity levels to the appropriate output stream
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************

#ifndef XLIFEPP_BASIC_OUTPUT_MANAGER_HPP
#define XLIFEPP_BASIC_OUTPUT_MANAGER_HPP

#include "utils.h"
#include "XlifeppOutputManager.hpp"

#ifdef HAVE_MPI
  #include <mpi.h>
#endif

namespace xlifepp {

/*!
  \class BasicOutputManager
  \brief xlifepp's basic output manager for sending information of select verbosity levels
  to the appropriate output stream.

*/
template <class ScalarType>
class BasicOutputManager : public OutputManager<ScalarType> {

public:

  //! @name Constructors/Destructor
  //@{
  //! Default constructor
  BasicOutputManager(int vb = 1,
                     SmartPtr<std::ostream> os = SmartPtr<std::ostream>(&std::cout, false),
                     int printingRank = 0);

  //! Destructor.
  virtual ~BasicOutputManager() {}
  //@}

  //! @name Set/Get methods
  //@{
  //! Set the output stream for this manager.
  void setOStream(SmartPtr<std::ostream> os);

  //! Get the output stream for this manager.
  SmartPtr<std::ostream> getOStream();
  //@}

  //! Find out whether we need to print out information for this message type.
  /*! This method is used by the solver to determine whether computations are
    necessary for this message type.
    */
  bool isVerbosity(MsgEigenType type) const;

  //! Send some output to this output stream.
  void print(MsgEigenType type, const std::string output);

  //! Return a stream for outputting to.
  std::ostream& stream(MsgEigenType type);

private:
  //! Copy constructor.
  BasicOutputManager(const OutputManager<ScalarType>& OM);

  //! Assignment operator.
  BasicOutputManager<ScalarType>& operator=(const OutputManager<ScalarType>& OM);

  //! Output stream
  SmartPtr<std::ostream> myOS_;

  bool iPrint_;
};

template<class ScalarType>
BasicOutputManager<ScalarType>::BasicOutputManager(int vb, SmartPtr<std::ostream> os, int printingRank)
  : OutputManager<ScalarType>(vb), myOS_(os)
{
  // print only on proc 0
  // Keep this variable in order to do with MPI in the future
  int myPID;
  #ifdef HAVE_MPI
    // Initialize MPI
    int mpiStarted = 0;
    MPI_Initialized( &mpiStarted);
    if (mpiStarted) MPI_Comm_rank(MPI_COMM_WORLD, & myPID);
    else myPID=0;
  #else 
    myPID = 0;
  #endif
  iPrint_ = (myPID == printingRank);
  }

template<class ScalarType>
void BasicOutputManager<ScalarType>::setOStream(SmartPtr<std::ostream> os)
{
  myOS_ = os; 
}

template<class ScalarType>
SmartPtr<std::ostream> BasicOutputManager<ScalarType>::getOStream()
{
  return myOS_; 
}

template<class ScalarType>
bool BasicOutputManager<ScalarType>::isVerbosity(MsgEigenType type) const
{
  if ((type & this->vb_) == type) {
    return true;
  }
  return false;
}

template<class ScalarType>
void BasicOutputManager<ScalarType>::print(MsgEigenType type, const std::string output)
{
  if ((type & this->vb_) == type  && iPrint_) {
    *myOS_ << output;
  }
}

template<class ScalarType>
std::ostream& BasicOutputManager<ScalarType>::stream(MsgEigenType type)
{
  if ((type & this->vb_) == type  && iPrint_) {
    return *myOS_;
  } else {
    *myOS_ << " ";
    return *myOS_;
  }
}

} // end xlifepp namespace

#endif /*XLIFEPP_BASIC_OUTPUT_MANAGER_HPP*/
