/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppSortManager.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013
 
  \brief Virtual base class which defines the interface between an eigensolver and a class whose 
  job is the sorting of the computed eigenvalues
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************

#ifndef XLIFEPP_SORTMANAGER_HPP
#define XLIFEPP_SORTMANAGER_HPP

#include "utils.h"
#include "../eigenCore/eigenCore.hpp"

namespace xlifepp {

/*!    \class SortManager

       \brief xlifepp's templated pure virtual class for managing the sorting
       of approximate eigenvalues computed by the eigensolver. A concrete
       implementation of this class is necessary. 
*/
template<class MagnitudeType>
class SortManager {
    
  public:

    //! Default constructor.
    SortManager() {}
    
    //! Constructor accepting a Parameters. This is the default mode for instantiating a SortManager.
    SortManager(Parameters& pl) {}

    //! Destructor
    virtual ~SortManager() {}

    /*! \brief Sort real eigenvalues, optionally returning the permutation vector.

       @param evals [in/out] Vector of length at least \c n containing the eigenvalues to be sorted.  <br>
                     On output, the first \c n eigenvalues will be sorted. The rest will be unchanged.

       @param perm [out] Vector of length at least \c n to store the permutation index (optional).  <br>
       If specified, on output the first \c n eigenvalues will contain the permutation indices, in the range <tt>[0,n-1]</tt>, such that <tt>evals_out[i] = evals_in[perm[i]]</tt>

       @param n [in] Number of values in evals to be sorted. If <tt>n == -1</tt>, all values will be sorted.
    */
    virtual void sort(std::vector<MagnitudeType>& evals, SmartPtr<std::vector<int> > perm = _smPtrNull, int n = -1) const = 0;

    /*! \brief Sort complex eigenvalues, optionally returning the permutation vector.

       This routine takes two vectors, one for each part of a complex
       eigenvalue. This is helpful for solving real, non-symmetric eigenvalue
       problems.

       @param rEvals [in/out] Vector of length at least \c n containing the real part of the eigenvalues to be sorted.  <br>
                     On output, the first \c n eigenvalues will be sorted. The rest will be unchanged.

       @param iEvals [in/out] Vector of length at least \c n containing the imaginary part of the eigenvalues to be sorted.  <br>
                     On output, the first \c n eigenvalues will be sorted. The rest will be unchanged.

       @param perm [out] Vector of length at least \c n to store the permutation index (optional).  <br>
       If specified, on output the first \c n eigenvalues will contain the permutation indices, in the range <tt>[0,n-1]</tt>, such that <tt>r_evals_out[i] = r_evals_in[perm[i]]</tt>
       and similarly for \c iEvals.

       @param n [in] Number of values in \c rEvals, \c iEvals to be sorted. If <tt>n == -1</tt>, all values will be sorted.
    */
    virtual void sort(std::vector<MagnitudeType>& rEvals,
                      std::vector<MagnitudeType>& iEvals,
                      SmartPtr<std::vector<int> > perm = _smPtrNull,
                      int n = -1) const = 0;
  };
  
} // namespace xlifepp

#endif /* XLIFEPP_SORTMANAGER_HPP */
