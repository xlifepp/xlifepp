/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file XlifeppStatusTestCombo.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013

  \brief Status test for forming logical combinations of other status tests.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************

#ifndef XLIFEPP_STATUS_TEST_COMBO_HPP
#define XLIFEPP_STATUS_TEST_COMBO_HPP

#include "utils.h"
#include "config.h"
#include "XlifeppEigenTypes.hpp"
#include "XlifeppStatusTest.hpp"

namespace xlifepp {

/*!
  \class StatusTestCombo
  \brief Status test for forming logical combinations of other status tests.

  Test types include StatusTestCombo::_OR, StatusTestCombo::_AND, StatusTestCombo::_SEQOR and StatusTestCombo::_SEQAND.  The StatusTestCombo::_OR and StatusTestCombo::_AND tests
  evaluate all of the tests, in the order they were passed to the
  StatusTestCombo.  The StatusTestCombo::_SEQOR and StatusTestCombo::_SEQAND run only the tests necessary to
  determine the final outcome, short-circuiting on the first test that
  conclusively decides the outcome. More formally, StatusTestCombo::_SEQAND runs the tests in
  the order they were given to the StatusTestCombo class and stops after the
  first test that evaluates ::_failed. StatusTestCombo::_SEQOR run the tests in the order they
  were given to the StatusTestCombo class and stops after the first test that
  evaluates ::_passed.
*/
template <class ScalarType, class MV, class OP>
class StatusTestCombo : public StatusTest<ScalarType,MV,OP> {

 private:
   typedef std::vector< SmartPtr< StatusTest<ScalarType,MV,OP> > > STPArray;

 public:

 //!  \brief Enumerated type to list the types of StatusTestCombo combo types.
 enum ComboType
 {
   _OR,           /*!< Logical _OR which evaluates all tests */
   _AND,          /*!< Logical _AND which evaluates all tests */
   _SEQOR,        /*!< Short-circuited logical _OR */
   _SEQAND        /*!< Short-circuited logical _AND */
 };


#ifndef DOXYGEN_SHOULD_SKIP_THIS

  typedef std::vector< SmartPtr< StatusTest<ScalarType,MV,OP> > > t_arr;
  typedef std::vector< SmartPtr< StatusTest<ScalarType,MV,OP> > > st_vector;
  typedef typename st_vector::iterator                 iterator;
  typedef typename st_vector::const_iterator     const_iterator;

#endif // DOXYGEN_SHOULD_SKIP_THIS

  //! @name Constructors/destructors
  //@{

  //! Constructor
  //! \brief Default constructor has no tests and initializes to StatusTestCombo::ComboType StatusTestCombo::_OR.
  StatusTestCombo() : state_(_undefined) {}

  //! Constructor
  //! \brief Constructor specifying the StatusTestCombo::ComboType and the tests.
  StatusTestCombo(ComboType type, std::vector< SmartPtr< StatusTest<ScalarType,MV,OP> > > tests) :
    state_(_undefined),
    type_(type)
  {
    setTests(tests);
  };

  //! Destructor
  virtual ~StatusTestCombo() {}
  //@}

  //! @name Status methods
  //@{
  /*! Check status as defined by test.

    \return TestStatus indicating whether the test passed or failed.
  */
  TestStatus checkStatus( EigenSolver<ScalarType,MV,OP>* solver );

  //! Return the result of the most recent checkStatus call.
  TestStatus getStatus() const {
    return state_;
  }

  //! Get the indices for the vectors that passed the test.
  /*!
   * This returns some combination of the passing vectors from the
   * tests comprising the StatusTestCombo. The nature of the combination depends on the
   * StatusTestCombo::ComboType:
   *    - StatusTestCombo::_SEQOR, StatusTestCombo::_OR - whichVecs() returns the union of whichVecs() from all evaluated constituent tests
   *    - StatusTestCombo::_SEQAND, StatusTestCombo::_AND - whichVecs() returns the intersection of whichVecs() from all evaluated constituent tests
   */
  std::vector<int> whichVecs() const {
    return ind_;
  }

  //! Get the number of vectors that passed the test.
  //
  // See whichVecs()
  int howMany() const {
    return ind_.size();
  }

  //@}

  //! @name Accessor methods
  //@{

  /*! \brief Set the maximum number of iterations.
   *  This also resets the test status to ::_undefined.
   */
  void setComboType(ComboType type) {
    type_ = type;
    state_ = _undefined;
  }

  //! Get the maximum number of iterations.
  ComboType getComboType() const {return type_;}

  /*! \brief Set the tests
   *  This also resets the test status to ::_undefined.
   */
  void setTests(std::vector<SmartPtr<StatusTest<ScalarType,MV,OP> > > tests) {
    tests_ = tests;
    state_ = _undefined;
  }

  //! Get the tests
  std::vector<SmartPtr<StatusTest<ScalarType,MV,OP> > > getTests() const {return tests_;}

  /*! \brief Add a test to the combination.
   *
   *  This also resets the test status to ::_undefined.
   */
  void addTest(SmartPtr<StatusTest<ScalarType,MV,OP> > test) {
    tests_.push_back(test);
    state_ = _undefined;
  }

  /*! \brief Removes a test from the combination, if it exists in the tester.
   *
   * This also resets the test status to ::_undefined, if a test was removed.
   */
  void removeTest(const SmartPtr<StatusTest<ScalarType,MV,OP> >& test);

  //@}

  //! @name Reset methods
  //@{
  //! \brief Informs the status test that it should reset its internal configuration to the uninitialized state.
  /*! The StatusTestCombo class has no internal state, but children classes might, so this method will call
     reset() on all child status tests. It also resets the test status to ::_undefined.
  */
  void reset();

  //! \brief Clears the results of the last status test.
  /*! This should be distinguished from the reset() method, as it only clears the cached result from the last
   * status test, so that a call to getStatus() will return ::_undefined. This is necessary for the StatusTestCombo::_SEQOR and StatusTestCombo::_SEQAND
   * tests in the StatusTestCombo class, which may short circuit and not evaluate all of the StatusTests contained
   * in them.
  */
  void clearStatus();

  //@}

  //! @name Print methods
  //@{

  //! Output formatted description of stopping test to output stream.
  std::ostream& print(std::ostream& os, int indent = 0) const;
  PrintStream& print(PrintStream& os, int indent = 0) const;

  //@}
  private:

  TestStatus evalOR(EigenSolver<ScalarType,MV,OP>* solver);
  TestStatus evalAND(EigenSolver<ScalarType,MV,OP>* solver);
  TestStatus evalSEQOR(EigenSolver<ScalarType,MV,OP>* solver);
  TestStatus evalSEQAND(EigenSolver<ScalarType,MV,OP>* solver);

  TestStatus state_;
  ComboType type_;
  STPArray tests_;
  std::vector<int> ind_;

};


template <class ScalarType, class MV, class OP>
void StatusTestCombo<ScalarType,MV,OP>::removeTest(const SmartPtr<StatusTest<ScalarType,MV,OP> > &test)
{
  typename STPArray::iterator iter1;
  iter1 = std::find(tests_.begin(),tests_.end(),test);
  if (iter1 != tests_.end()) {
    tests_.erase(iter1);
    state_ = _undefined;
  }
}


template <class ScalarType, class MV, class OP>
TestStatus StatusTestCombo<ScalarType,MV,OP>::checkStatus( EigenSolver<ScalarType,MV,OP>* solver ) {
  clearStatus();
  switch (type_) {
    case _OR:
      state_ = evalOR(solver);
      break;
    case _AND:
      state_ = evalAND(solver);
      break;
    case _SEQOR:
      state_ = evalSEQOR(solver);
      break;
    case _SEQAND:
      state_ = evalSEQAND(solver);
      break;
  }
  return state_;
}


template <class ScalarType, class MV, class OP>
void StatusTestCombo<ScalarType,MV,OP>::reset() {
  ind_.resize(0);
  state_ = _undefined;
  typedef typename STPArray::iterator iter;
  for (iter i=tests_.begin(); i != tests_.end(); i++) {
    (*i)->reset();
  }
}

template <class ScalarType, class MV, class OP>
void StatusTestCombo<ScalarType,MV,OP>::clearStatus() {
  ind_.resize(0);
  state_ = _undefined;
  typedef typename STPArray::iterator iter;
  for (iter i=tests_.begin(); i != tests_.end(); i++) {
    (*i)->clearStatus();
  }
}

template <class ScalarType, class MV, class OP>
std::ostream& StatusTestCombo<ScalarType,MV,OP>::print(std::ostream& os, int indent) const {
  std::string ind(indent,' ');
  os << ind << "- StatusTestCombo: ";
  switch (state_) {
  case _passed:
    os << "_passed" << std::endl;
    break;
  case _failed:
    os << "_failed" << std::endl;
    break;
  case _undefined:
    os << "_undefined" << std::endl;
    break;
  }
  // print children, with extra indention
  typedef typename STPArray::const_iterator const_iter;
  for (const_iter i=tests_.begin(); i != tests_.end(); i++) {
    (*i)->print(os,indent+2);
  }
  return os;
}

template <class ScalarType, class MV, class OP>
PrintStream& StatusTestCombo<ScalarType,MV,OP>::print(PrintStream& os, int indent) const
{ print(os.currentStream(),indent); return os;}

template <class ScalarType, class MV, class OP>
TestStatus StatusTestCombo<ScalarType,MV,OP>::evalOR( EigenSolver<ScalarType,MV,OP>* solver ) {
  state_ = _failed;
  typedef typename STPArray::iterator iter;
  for (iter i=tests_.begin(); i != tests_.end(); i++) {
    TestStatus r = (*i)->checkStatus(solver);
    if (i == tests_.begin()) {
      ind_ = (*i)->whichVecs();
      // sort ind_ for use below
      std::sort(ind_.begin(),ind_.end());
    }
    else {
      // to use set_union, ind_ must have room for the result, which will have size() <= end.size() + iwv.size()
      // also, ind and iwv must be in ascending order; only ind_ is
      // lastly, the return from set_union points to the last element in the union, which tells us how big the union is
      std::vector<int> iwv = (*i)->whichVecs();
      std::sort(iwv.begin(),iwv.end());
      std::vector<int> tmp(ind_.size() + iwv.size());
      std::vector<int>::iterator end;
      end = std::set_union(ind_.begin(),ind_.end(),iwv.begin(),iwv.end(),tmp.begin());
      tmp.resize(end - tmp.begin());
      // ind_ will be sorted coming from set_union
      ind_ = tmp;
    }
    if (r == _passed) {
      state_ = _passed;
    }
    else {
      internalEigenSolver::testErrorEigenProblem(r != _failed,
                         "xlifepp::StatusTestCombo::evalOR(): child test gave invalid return");
    }
  }
  return state_;
}

template <class ScalarType, class MV, class OP>
TestStatus StatusTestCombo<ScalarType,MV,OP>::evalSEQOR( EigenSolver<ScalarType,MV,OP>* solver ) {
  state_ = _failed;
  typedef typename STPArray::iterator iter;
  for (iter i=tests_.begin(); i != tests_.end(); i++) {
    TestStatus r = (*i)->checkStatus(solver);
    if (i == tests_.begin()) {
      ind_ = (*i)->whichVecs();
      // sort ind_ for use below
      std::sort(ind_.begin(),ind_.end());
    }
    else {
      // to use set_union, ind_ must have room for the result, which will have size() <= end.size() + iwv.size()
      // also, ind and iwv must be in ascending order; only ind_ is
      // lastly, the return from set_union points to the last element in the union, which tells us how big the union is
      std::vector<int> iwv = (*i)->whichVecs();
      std::sort(iwv.begin(),iwv.end());
      std::vector<int> tmp(ind_.size() + iwv.size());
      std::vector<int>::iterator end;
      end = std::set_union(ind_.begin(),ind_.end(),iwv.begin(),iwv.end(),tmp.begin());
      tmp.resize(end - tmp.begin());
      // ind_ will be sorted coming from set_union
      ind_ = tmp;
    }
    if (r == _passed) {
      state_ = _passed;
      break;
    }
    else {
      internalEigenSolver::testErrorEigenProblem(r != _failed,
                         "xlifepp::StatusTestCombo::evalSEQOR(): child test gave invalid return");
    }
  }
  return state_;
}

template <class ScalarType, class MV, class OP>
TestStatus StatusTestCombo<ScalarType,MV,OP>::evalAND( EigenSolver<ScalarType,MV,OP>* solver ) {
  state_ = _passed;
  typedef typename STPArray::iterator iter;
  for (iter i=tests_.begin(); i != tests_.end(); i++) {
    TestStatus r = (*i)->checkStatus(solver);
    if (i == tests_.begin()) {
      ind_ = (*i)->whichVecs();
      // sort ind_ for use below
      std::sort(ind_.begin(),ind_.end());
    }
    else {
      // to use set_intersection, ind_ must have room for the result, which will have size() <= end.size() + iwv.size()
      // also, ind and iwv must be in ascending order; only ind_ is
      // lastly, the return from set_intersection points to the last element in the intersection, which tells us how big the intersection is
      std::vector<int> iwv = (*i)->whichVecs();
      std::sort(iwv.begin(),iwv.end());
      std::vector<int> tmp(ind_.size() + iwv.size());
      std::vector<int>::iterator end;
      end = std::set_intersection(ind_.begin(),ind_.end(),iwv.begin(),iwv.end(),tmp.begin());
      tmp.resize(end - tmp.begin());
      // ind_ will be sorted coming from set_intersection
      ind_ = tmp;
    }
    if (r == _failed) {
      state_ = _failed;
    }
    else {
      internalEigenSolver::testErrorEigenProblem(r != _passed,
                         "xlifepp::StatusTestCombo::evalAND(): child test gave invalid return");
    }
  }
  return state_;
}

template <class ScalarType, class MV, class OP>
TestStatus StatusTestCombo<ScalarType,MV,OP>::evalSEQAND( EigenSolver<ScalarType,MV,OP>* solver ) {
  state_ = _passed;
  typedef typename STPArray::iterator iter;
  for (iter i=tests_.begin(); i != tests_.end(); i++) {
    TestStatus r = (*i)->checkStatus(solver);
    if (i == tests_.begin()) {
      ind_ = (*i)->whichVecs();
      // sort ind_ for use below
      std::sort(ind_.begin(),ind_.end());
    }
    else {
      // to use set_intersection, ind_ must have room for the result, which will have size() <= end.size() + iwv.size()
      // also, ind and iwv must be in ascending order; only ind_ is
      // lastly, the return from set_intersection points to the last element in the intersection, which tells us how big the intersection is
      std::vector<int> iwv = (*i)->whichVecs();
      std::sort(iwv.begin(),iwv.end());
      std::vector<int> tmp(ind_.size() + iwv.size());
      std::vector<int>::iterator end;
      end = std::set_intersection(ind_.begin(),ind_.end(),iwv.begin(),iwv.end(),tmp.begin());
      tmp.resize(end - tmp.begin());
      // ind_ will be sorted coming from set_intersection
      ind_ = tmp;
    }
    if (r == _failed) {
      state_ = _failed;
      break;
    }
    else {
      internalEigenSolver::testErrorEigenProblem(r != _passed,
                         "xlifepp::StatusTestCombo::evalAND(): child test gave invalid return");
    }
  }
  return state_;
}

} // end of xlifepp namespace

#endif /* XLIFEPP_STATUS_TEST_COMBO_HPP */
