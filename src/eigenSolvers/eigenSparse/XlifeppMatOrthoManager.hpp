/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppMatOrthoManager.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013

  \brief Templated virtual class for providing orthogonalization/orthonormalization methods with matrix-based inner products.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_MATORTHOMANAGER_HPP
#define XLIFEPP_MATORTHOMANAGER_HPP

#include "utils.h"
#include "../eigenCore/eigenCore.hpp"
#include "XlifeppEigenTypes.hpp"
#include "XlifeppOrthoManager.hpp"
#include "XlifeppMultiVecTraits.hpp"
#include "XlifeppOperatorTraits.hpp"

namespace xlifepp {

/*! \class MatOrthoManager
  
  \brief xlifepp's templated virtual class for providing routines for
  orthogonalization and orthonormalization of multivectors using matrix-based
  inner products.

  This class extends xlifepp::OrthoManager by providing extra calling arguments
  to orthogonalization routines, to reduce the cost of applying the inner
  product in cases where the user already has the image of target multivectors
  under the inner product matrix.

  A concrete implementation of this class is necessary. The user can create
  their own implementation if those supplied are not suitable for their needs.

*/
template <class ScalarType, class MV, class OP>
class MatOrthoManager : public OrthoManager<ScalarType,MV> {
  public:
    //! @name Constructor/Destructor
    //@{ 
    //! Default constructor.
    MatOrthoManager(SmartPtr<const OP> op = _smPtrNull);

    //! Destructor.
    virtual ~MatOrthoManager() {}
    //@}

    //! @name Accessor routines
    //@{ 

    //! Set operator used for inner product.
    virtual void setOp(SmartPtr<const OP> op);

    //! Get operator used for inner product.
    virtual SmartPtr<const OP> getOp() const;

    //! Retrieve operator counter.
    /*! This counter returns the number of applications of the operator specifying the inner 
     * product. When the operator is applied to a multivector, the counter is incremented by the
     * number of vectors in the multivector. If the operator is not specified, the counter is never 
     * incremented.
     */
    int getOpCounter() const;

    //! Reset the operator counter to zero.
    /*! See getOpCounter() for more details.
     */
    void resetOpCounter();

    //@}

    //! @name Matrix-based Orthogonality Methods 
    //@{ 

    /*! \brief Provides a matrix-based inner product.
     *
     * Provides the inner product 
     * \f[
     *    \langle x, y \rangle = x^H M y
     * \f]
     * Optionally allows the provision of \f$M y\f$ and/or \f$M x\f$. See OrthoManager::innerProd() for more details.
     *
     */
    void innerProdMat(
          const MV& X, const MV& Y, 
          MatrixEigenDense<ScalarType>& Z,
          SmartPtr<const MV> MX = _smPtrNull,
          SmartPtr<const MV> MY = _smPtrNull
       ) const;

    /*! \brief Provides the norm induced by the matrix-based inner product.
     *
     *  Provides the norm:
     *  \f[
     *     \|x\|_M = \sqrt{x^H M y}
     *  \f]
     *  Optionally allows the provision of \f$M x\f$. See OrthoManager::norm() for more details.
     */
    void normMat(
          const MV& X, 
          std::vector< typename NumTraits<ScalarType>::RealScalar >& normvec,
          SmartPtr<const MV> MX = _smPtrNull
       ) const;

    /*! \brief Provides matrix-based projection method.
     *
     * This method optionally allows the provision of \f$M X\f$ and/or the \f$M Q[i]\f$. See OrthoManager::project() for more details.
     @param X, Q, C [in/out] As in OrthoManager::project()

     @param MX [in/out] If specified by the user, on input \c MX is required to be the image of \c X under the operator getOp(). 
     On output, \c MX will be updated to reflect the changes in \c X.
     
     @param MQ [in] If specified by the user, on \c MQ[i] is required to be the image of <tt>Q[i]</tt> under the operator getOp().
     */
    virtual void projectMat (
          MV& X,
          std::vector<SmartPtr<const MV> >  Q,
          std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C,
              //= Teuchos::tuple(SmartPtr<MatrixEigenDense<ScalarType> >(_smPtrNull)),
          SmartPtr<MV> MX = _smPtrNull,
          std::vector<SmartPtr<const MV> > MQ = std::vector<SmartPtr<const MV> >(1,_smPtrNull)
              //= Teuchos::tuple(SmartPtr<const MV>(_smPtrNull))
       ) const = 0;

    /*! \brief Provides matrix-based orthonormalization method.
     *
     * This method optionally allows the provision of \f$M X\f$. See orthoManager::normalize() for more details.
     @param X, B [in/out] As in OrthoManager::normalize()

     @param MX [in/out] If specified by the user, on input \c MX is required to be the image of \c X under the operator getOp(). 
     On output, \c MX will be updated to reflect the changes in \c X.

     @return Rank of the basis computed by this method, less than or equal to
       the number of columns in \c X. This specifies how many columns in the
       returned \c X and \c MX and rows in the returned \c B are valid.
    */
    virtual int normalizeMat (
          MV& X,
          SmartPtr<MatrixEigenDense<ScalarType> > B = _smPtrNull,
          SmartPtr<MV> MX  = _smPtrNull
       ) const = 0;


    /*! \brief Provides matrix-based projection/orthonormalization method.
     *
     * This method optionally allows the provision of \f$M X\f$ and/or the \f$M Q[i]\f$. See orthoManager::projectAndNormalize() for more details.
     @param X, Q, C, B [in/out] As in OrthoManager::projectAndNormalize()

     @param MX [in/out] If specified by the user, on input \c MX is required to be the image of \c X under the operator getOp(). 
     On output, \c MX will be updated to reflect the changes in \c X.
     
     @param MQ [in] If specified by the user, on \c MQ[i] is required to be the image of <tt>Q[i]</tt> under the operator getOp().

     @return Rank of the basis computed by this method, less than or equal to
       the number of columns in \c X. This specifies how many columns in the
       returned \c X and \c MX and rows in the returned \c B are valid.
    */
    virtual int projectAndNormalizeMat (
          MV& X,
          std::vector<SmartPtr<const MV> >  Q,
          std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C = std::vector<SmartPtr<MatrixEigenDense<ScalarType> > >(1, _smPtrNull),
              //= Teuchos::tuple(SmartPtr<MatrixEigenDense<ScalarType> >(_smPtrNull)),
          SmartPtr<MatrixEigenDense<ScalarType> > B                   = _smPtrNull,
          SmartPtr<MV> MX  = _smPtrNull,
          std::vector<SmartPtr<const MV> > MQ = std::vector<SmartPtr<const MV> >(1,_smPtrNull)
              //= Teuchos::tuple(SmartPtr<const MV>(_smPtrNull))
       ) const = 0;

    /*! \brief This method computes the error in orthonormality of a multivector.
     *
     *  This method optionally allows optionally exploits a caller-provided \c MX.
     */
    virtual typename NumTraits<ScalarType>::RealScalar
    orthonormErrorMat(const MV& X, SmartPtr<const MV> MX = _smPtrNull) const = 0;

    /*! \brief This method computes the error in orthogonality of two multivectors.
     *
     *  This method optionally allows optionally exploits a caller-provided \c MX and/or \c MY.
     */
    virtual typename NumTraits<ScalarType>::RealScalar
    orthogErrorMat(
          const MV& X,
          const MV& Y,
          SmartPtr<const MV> MX = _smPtrNull,
          SmartPtr<const MV> MY = _smPtrNull
       ) const = 0;

    //@}

    //! @name Methods implementing xlifepp::OrthoManager
    //@{ 

    /*! \brief Implements the interface OrthoManager::innerProd(). 
     *
     * This method calls 
     * \code
     * innerProdMat(X,Y,Z);
     * \endcode
     */
    void innerProd(const MV& X, const MV& Y, MatrixEigenDense<ScalarType>& Z) const;

    /*! \brief Implements the interface OrthoManager::norm(). 
     *
     * This method calls 
     * \code
     * normMat(X,normvec);
     * \endcode
     */
    void norm(const MV& X, std::vector< typename NumTraits<ScalarType>::RealScalar >& normvec) const;
    
    /*! \brief Implements the interface OrthoManager::project(). 
     *
     * This method calls 
     * \code
     * projectMat(X,Q,C);
     * \endcode
     */
    void project (
          MV& X,
          std::vector<SmartPtr<const MV> > Q,
          std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C
              //= Teuchos::tuple(SmartPtr<MatrixEigenDense<ScalarType> >(_smPtrNull))
       ) const;

    /*! \brief Implements the interface OrthoManager::normalize(). 
     *
     * This method calls 
     * \code
     * normalizeMat(X,B);
     * \endcode
     */
    int normalize (MV& X, SmartPtr<MatrixEigenDense<ScalarType> > B = _smPtrNull) const;

    /*! \brief Implements the interface OrthoManager::projectAndNormalize(). 
     *
     * This method calls 
     * \code
     * projectAndNormalizeMat(X,Q,C,B);
     * \endcode
     */
    int projectAndNormalize (
          MV& X,
          std::vector<SmartPtr<const MV> > Q,
          std::vector<SmartPtr<MatrixEigenDense<ScalarType> > >C,
              //= Teuchos::tuple(SmartPtr<MatrixEigenDense<ScalarType> >(_smPtrNull)),
              SmartPtr<MatrixEigenDense<ScalarType> > B = _smPtrNull
       ) const;

    /*! \brief Implements the interface OrthoManager::orthonormError(). 
     *
     * This method calls 
     * \code
     * orthonormErrorMat(X);
     * \endcode
     */
    typename NumTraits<ScalarType>::RealScalar
    orthonormError(const MV& X) const;

    /*! \brief Implements the interface OrthoManager::orthogError(). 
     *
     * This method calls 
     * \code
     * orthogErrorMat(X1,X2);
     * \endcode
     */
    typename NumTraits<ScalarType>::RealScalar
    orthogError(const MV& X1, const MV& X2) const;

    //@}

  protected:
    SmartPtr<const OP> Op_;
    bool hasOp_;
    mutable int OpCounter_;

};

template <class ScalarType, class MV, class OP>
MatOrthoManager<ScalarType,MV,OP>::MatOrthoManager(SmartPtr<const OP> op)
    : Op_(op), hasOp_(op !=_smPtrNull), OpCounter_(0) {}

template <class ScalarType, class MV, class OP>
void MatOrthoManager<ScalarType,MV,OP>::setOp(SmartPtr<const OP> op)
{ 
  Op_ = op;
  hasOp_ = (Op_ != _smPtrNull);
}

template <class ScalarType, class MV, class OP>
SmartPtr<const OP> MatOrthoManager<ScalarType,MV,OP>::getOp() const
{ 
  return Op_;
} 

template <class ScalarType, class MV, class OP>
int MatOrthoManager<ScalarType,MV,OP>::getOpCounter() const 
{
  return OpCounter_;
}

template <class ScalarType, class MV, class OP>
void MatOrthoManager<ScalarType,MV,OP>::resetOpCounter() 
{
  OpCounter_ = 0;
}

template <class ScalarType, class MV, class OP>
void MatOrthoManager<ScalarType,MV,OP>::innerProd(
    const MV& X, const MV& Y, MatrixEigenDense<ScalarType>& Z) const
{
  typedef NumTraits<ScalarType> SCT;
  typedef MultiVecTraits<ScalarType,MV>     MVT;
  typedef OperatorTraits<ScalarType,MV,OP>  OPT;

  SmartPtr<const MV> P,Q;
  SmartPtr<MV> R;

  if (hasOp_) {
    // attempt to minimize the amount of work in applying 
    if (MVT::getNumberVecs(X) < MVT::getNumberVecs(Y)) {
      R = MVT::clone(X,MVT::getNumberVecs(X));
      OPT::apply(*Op_,X,*R);
      OpCounter_ += MVT::getNumberVecs(X);
      P = R;
      Q = SmartPtr<const MV>(&Y, false);
    }
    else {
      P = SmartPtr<const MV>(&X, false);
      R = MVT::clone(Y,MVT::getNumberVecs(Y));
      OPT::apply(*Op_,Y,*R);
      OpCounter_ += MVT::getNumberVecs(Y);
      Q = R;
    }
  }
  else {
    P = SmartPtr<const MV>(&X, false);
    Q = SmartPtr<const MV>(&Y, false);
  }

  MVT::mvTransMv(SCT::one(),*P,*Q,Z);
}

template <class ScalarType, class MV, class OP>
void MatOrthoManager<ScalarType,MV,OP>::innerProdMat(
    const MV& X, const MV& Y, MatrixEigenDense<ScalarType>& Z, SmartPtr<const MV> MX, SmartPtr<const MV> MY) const
{
  (void)MX;
  typedef NumTraits<ScalarType> SCT;
  typedef MultiVecTraits<ScalarType,MV> MVT;

  SmartPtr<MV> P,Q;

  if (_smPtrNull == MY) {
    innerProd(X,Y,Z);
  }
  else if (hasOp_) {
    // the user has done the matrix vector for us
    MVT::mvTransMv(SCT::one(),X,*MY,Z);
  }
  else {
    // there is no matrix vector
    MVT::mvTransMv(SCT::one(),X,Y,Z);
  }
}

template <class ScalarType, class MV, class OP>
void MatOrthoManager<ScalarType,MV,OP>::norm(
    const MV& X, std::vector< typename NumTraits<ScalarType>::RealScalar >& normvec) const
{
  this->normMat(X,normvec);
}

template <class ScalarType, class MV, class OP>
void MatOrthoManager<ScalarType,MV,OP>::normMat(
    const MV& X, 
    std::vector< typename NumTraits<ScalarType>::RealScalar >& normvec,
    SmartPtr<const MV> MX) const
{
  typedef NumTraits<ScalarType> SCT;
  typedef NumTraits<typename SCT::RealScalar> MT;
  typedef MultiVecTraits<ScalarType,MV>     MVT;
  typedef OperatorTraits<ScalarType,MV,OP>  OPT;

  if (!hasOp_) {
    MX = SmartPtr<const MV>(&X,false);
  }
  else if (_smPtrNull == MX) {
      SmartPtr<MV> R = MVT::clone(X,MVT::getNumberVecs(X));
    OPT::apply(*Op_,X,*R);
    OpCounter_ += MVT::getNumberVecs(X);
    MX = R;
  }

  MatrixEigenDense<ScalarType> z(1,1);
  SmartPtr<const MV> Xi, MXi;
  std::vector<int> ind(1);
  for (int i=0; i<MVT::getNumberVecs(X); i++) {
    ind[0] = i;
    Xi = MVT::cloneView(X,ind);
    MXi = MVT::cloneView(*MX,ind);
    MVT::mvTransMv(SCT::one(),*Xi,*MXi,z);
    normvec[i] = MT::squareroot(SCT::magnitude(z.coeff(0,0)));
  }
}


template <class ScalarType, class MV, class OP>
void MatOrthoManager<ScalarType,MV,OP>::project (
      MV& X,
      std::vector<SmartPtr<const MV> > Q,
      std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C
   ) const
{
  this->projectMat(X,Q,C);
}

template <class ScalarType, class MV, class OP>
int MatOrthoManager<ScalarType,MV,OP>::normalize (
    MV& X, SmartPtr<MatrixEigenDense<ScalarType> > B) const
{
  return this->normalizeMat(X,B);
}

template <class ScalarType, class MV, class OP>
int MatOrthoManager<ScalarType,MV,OP>::projectAndNormalize (
      MV& X,
      std::vector<SmartPtr<const MV> > Q,
      std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C,
      SmartPtr<MatrixEigenDense<ScalarType> > B
   ) const
{
  return this->projectAndNormalizeMat(X,Q,C,B);
}

template <class ScalarType, class MV, class OP>
typename NumTraits<ScalarType>::RealScalar
MatOrthoManager<ScalarType,MV,OP>::orthonormError(const MV& X) const
{
  return this->orthonormErrorMat(X,_smPtrNull);
}

template <class ScalarType, class MV, class OP>
typename NumTraits<ScalarType>::RealScalar
MatOrthoManager<ScalarType,MV,OP>::orthogError(const MV& X1, const MV& X2) const
{
  return this->orthogErrorMat(X1,X2);
}

} // end of xlifepp namespace

#endif /* XLIFEPP_MATORTHOMANAGER_HPP */

