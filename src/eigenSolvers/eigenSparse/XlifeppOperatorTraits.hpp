/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppOperatorTraits.hpp
  \author Manh Ha NGUYEN
  \since 26 Mars 2013
  \date  12 June  2013
  
  \brief Virtual base class which defines basic traits for the operator type
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_OPERATOR_TRAITS_HPP
#define XLIFEPP_OPERATOR_TRAITS_HPP

#include "utils.h"
#include "XlifeppEigenTypes.hpp"

namespace xlifepp {

/*! \brief This is the default struct used by OperatorTraits<ScalarType, MV, OP> class to produce a
    compile time error when the specialization does not exist for operator type <tt>OP</tt>.
*/
template< class ScalarType, class MV, class OP >
struct UndefinedOperatorTraits
{
  //! This function should not compile if there is an attempt to instantiate!
  /*! \note Any attempt to compile this function results in a compile time error.  This means
    that the template specialization of xlifepp::OperatorTraits class does not exist for type
    <tt>OP</tt>, or is not complete.
  */
  static inline void notDefined() { return OP::this_type_is_missing_a_specialization(); };
};


/*!  \brief Virtual base class which defines basic traits for the operator type.

     An adapter for this traits class must exist for the <tt>MV</tt> and <tt>OP</tt> types.
     If not, this class will produce a compile-time error.

     \ingroup anasazi_opvec_interfaces
*/
template <class ScalarType, class MV, class OP>
class OperatorTraits 
{
public:
  
  //! @name Operator application method.
  //@{ 
  
  //! Application method which performs operation <b>y = Op*x</b>. An OperatorError exception is thrown if there is an error.
  static void apply (const OP& Op,
                      const MV& x, 
                      MV& y)
  {
    for (int i = 0; i< x.numVecs(); ++i) {
      multMatrixVector(Op,x(i),y(i));
    }
  };
  
  //@}
  
};
  
} // end xlifepp namespace

#endif /* XLIFEPP_OPERATOR_TRAITS_HPP */
