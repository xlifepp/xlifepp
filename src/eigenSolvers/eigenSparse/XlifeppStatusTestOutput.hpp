/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file XlifeppStatusTestOutput.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013

  \brief Special xlifepp::StatusTest for printing status tests.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************

#ifndef XLIFEPP_STATUS_TEST_OUTPUT_HPP
#define XLIFEPP_STATUS_TEST_OUTPUT_HPP

#include "config.h"
#include "EigenTestError.hpp"
#include "XlifeppEigenTypes.hpp"
#include "XlifeppEigenSolver.hpp"
#include "XlifeppStatusTest.hpp"

namespace xlifepp {

/*!
  \class StatusTestOutput
  \brief A special StatusTest for printing other status tests.

  StatusTestOutput is a wrapper around another StatusTest that calls
  StatusTest::print() on the underlying object on calls to StatusTestOutput::checkStatus().
  The frequency and occasion of the printing can be dictated according to some parameters passed to
  StatusTestOutput::StatusTestOutput().
*/
template <class ScalarType, class MV, class OP>
class StatusTestOutput : public StatusTest<ScalarType,MV,OP> {

 public:
  //! @name Constructors/destructors
  //@{

  /*! \brief Constructor
   *
   * The StatusTestOutput requires an OutputManager for printing the underlying StatusTest on
   * calls to checkStatus(), as well as an underlying StatusTest.
   *
   * StatusTestOutput can be initialized with a null pointer for argument \c test. However, calling checkStatus() with a null child pointer
   * will result in a StatusTestError exception being thrown. See checkStatus() for more information.
   *
   * The last two parameters, described below, in addition to the verbosity level of the OutputManager, control when printing is
   * called. When both the \c mod criterion and the \c printStates criterion are satisfied, the status test will be printed to the
   * OutputManager with ::MsgType of ::_statusTestDetailsEigen.
   *
   * @param[in] mod A positive number describes how often the output should be printed. On every call to checkStatus(), an internal counter
   *                is incremented. Printing may only occur when this counter is congruent to zero modulo \c mod. Default: 1 (attempt to print on every call to checkStatus())
   * @param[in] printStates A combination of ::TestStatus values for which the output may be printed. Default: ::_passed (attempt to print whenever checkStatus() will return ::_passed)
   * @param printer dedicated to print the underlying StatusTest
   * @param test child pointer
   *
   */
  StatusTestOutput(const SmartPtr<OutputManager<ScalarType> >& printer,
                   SmartPtr<StatusTest<ScalarType,MV,OP> > test,
                   int mod = 1,
                   int printStates = _passed)
    : printer_(printer), test_(test), state_(_undefined), stateTest_(printStates), modTest_(mod), numCalls_(0)
    { }

  //! Destructor
  virtual ~StatusTestOutput() {}
  //@}

  //! @name Status methods
  //@{
  /*! Check and return status of underlying StatusTest.

    This method calls checkStatus() on the StatusTest object passed in the constructor. If appropriate, the
    method will follow this call with a call to print() on the underlying object, using the OutputManager passed via the constructor
    with verbosity level ::_statusTestDetailsEigen.

    The internal counter will be incremented during this call, but only after
    performing the tests to decide whether or not to print the underlying
    StatusTest. This way, the very first call to checkStatus() following
    initialization or reset() will enable the underlying StatusTest to be
    printed, regardless of the mod parameter, as the current number of calls
    will be zero.

    If the specified SmartPtr for the child class is _smPtrNull, then calling checkStatus() will result in a StatusTestError exception being thrown.

    \return ::TestStatus indicating whether the underlying test passed or failed.
  */
  TestStatus checkStatus( EigenSolver<ScalarType,MV,OP>* solver ) {
    internalEigenSolver::testErrorEigenProblem(test_ == _smPtrNull,"StatusTestOutput::checkStatus(): child pointer is null.");
    state_ = test_->checkStatus(solver);

    if (numCalls_++ % modTest_ == 0) {
      if ( (state_ & stateTest_) == state_) {
        if ( printer_->isVerbosity(_statusTestDetailsEigen) ) {
          print( printer_->stream(_statusTestDetailsEigen) );
        }
        else if ( printer_->isVerbosity(_debugEigen) ) {
          print( printer_->stream(_debugEigen) );
        }
      }
    }

    return state_;
  }

  //! Return the result of the most recent checkStatus call, or undefined if it has not been run.
  TestStatus getStatus() const {
    return state_;
  }

  //! Get the indices for the vectors that passed the test.
  std::vector<int> whichVecs() const {
    return std::vector<int>(0);
  }

  //! Get the number of vectors that passed the test.
  int howMany() const {
    return 0;
  }

  //@}


  //! @name Accessor methods
  //@{

  /*! \brief Set child test.
   *
   *  \note This also resets the test status to ::_undefined.
   */
  void setChild(SmartPtr<StatusTest<ScalarType,MV,OP> > test) {
    test_ = test;
    state_ = _undefined;
  }

  //! \brief Get child test.
  SmartPtr<StatusTest<ScalarType,MV,OP> > getChild() const {
    return test_;
  }

  //@}


  //! @name Reset methods
  //@{
  /*! \brief Informs the status test that it should reset its internal configuration to the uninitialized state.
   *
   *  This resets the cached state to an ::_undefined state and calls reset() on the underlying test. It also
   *  resets the counter for the number of calls to checkStatus().
   */
  void reset() {
    state_ = _undefined;
    if (test_ != _smPtrNull) {
      test_->reset();
    }
    numCalls_ = 0;
  }

  //! Clears the results of the last status test.
  //! This resets the cached state to an ::_undefined state and calls clearStatus() on the underlying test.
  void clearStatus() {
    state_ = _undefined;
    if (test_ != _smPtrNull) {
      test_->clearStatus();
    }
  }

  //@}

  //! @name Print methods
  //@{

  //! Output formatted description of stopping test to output stream.
  std::ostream& print(std::ostream& os, int indent = 0) const {
    std::string ind(indent,' ');
    os << ind << "- StatusTestOutput: ";
    switch (state_) {
    case _passed:
      os << "_passed" << std::endl;
      break;
    case _failed:
      os << "_failed" << std::endl;
      break;
    case _undefined:
      os << "_undefined" << std::endl;
      break;
    }
    os << ind << "  (Num calls,Mod test,State test): " << "(" << numCalls_ << ", " << modTest_ << ",";
    if (stateTest_ == 0) {
      os << " none )" << std::endl;
    }
    else {
      if ( (stateTest_ & _passed) == _passed ) os << " _passed";
      if ( (stateTest_ & _failed) == _failed ) os << " _failed";
      if ( (stateTest_ & _undefined) == _undefined ) os << " _undefined";
      os << " )" << std::endl;
    }
    // print child, with extra indention
    test_->print(os,indent+3);
    return os;
  }

   PrintStream& print(PrintStream& os, int indent = 0) const
   { print(os.currentStream(),indent); return os;}

  //@}

  private:
    SmartPtr<OutputManager<ScalarType> > printer_;
    SmartPtr<StatusTest<ScalarType,MV,OP> > test_;
    TestStatus state_;
    int stateTest_;
    int modTest_;
    int numCalls_;
};

} // end of xlifepp namespace

#endif /* XLIFEPP_STATUS_TEST_OUTPUT_HPP */
