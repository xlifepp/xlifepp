/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppBasicOrthoManager.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013

  \brief Implementation of the xlifepp::BasicOrthoManager class
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_BASIC_ORTHOMANAGER_HPP
#define XLIFEPP_BASIC_ORTHOMANAGER_HPP


#include "utils.h"
#include "../eigenCore/eigenCore.hpp"
#include "EigenTestError.hpp"
#include "XlifeppMultiVecTraits.hpp"
#include "XlifeppOperatorTraits.hpp"
#include "XlifeppMatOrthoManager.hpp"

namespace xlifepp {

/*!   \class BasicOrthoManager
      \brief An implementation of the MatOrthoManager that performs orthogonalization
      using (potentially) multiple steps of classical Gram-Schmidt.
*/
template<class ScalarType, class MV, class OP>
class BasicOrthoManager : public MatOrthoManager<ScalarType,MV,OP> {

  private:
    typedef typename NumTraits<ScalarType>::magnitudeType MagnitudeType;
    typedef NumTraits<ScalarType>  SCT;
    typedef MultiVecTraits<ScalarType,MV>      MVT;
    typedef OperatorTraits<ScalarType,MV,OP>   OPT;

  public:
    //! @name Constructor/Destructor
    //@{
    //! Constructor specifying re-orthogonalization tolerance.
    BasicOrthoManager(SmartPtr<const OP> Op = _smPtrNull,
                       typename NumTraits<ScalarType>::RealScalar kappa = sqrtOf2_ /* sqrt(2) */,
                       typename NumTraits<ScalarType>::RealScalar eps = 0.0,
                       typename NumTraits<ScalarType>::RealScalar tol = 0.20);


    //! Destructor
    ~BasicOrthoManager() {}
    //@}


    //! @name Methods implementing MatOrthoManager
    //@{

    /*! \brief Given a list of mutually orthogonal and internally orthonormal bases \c Q, this method
     * projects a multivector \c X onto the space orthogonal to the individual <tt>Q[i]</tt>,
     * optionally returning the coefficients of \c X for the individual <tt>Q[i]</tt>. All of this is done with respect
     * to the inner product innerProd().
     *
     * After calling this routine, \c X will be orthogonal to each of the <tt>Q[i]</tt>.
     *
     @param[in,out] X The multivector to be modified.<br>
       On output, the columns of \c X will be orthogonal to each <tt>Q[i]</tt>, satisfying
       \f[
       X_{out} = X_{in} - \sum_i Q[i] \langle Q[i], X_{in} \rangle
       \f]

     @param[in,out] MX The image of \c X under the inner product operator \c Op.
       If \f$ MX != 0\f$: On input, this is expected to be consistent with \c Op . X. On output, this is updated consistent with updates to \c X.
       If \f$ MX == 0\f$ or \f$ Op == 0\f$: \c MX is not referenced.

     @param[out] C The coefficients of \c X in the bases <tt>Q[i]</tt>. If <tt>C[i]</tt> is a non-null pointer
       and <tt>C[i]</tt> matches the dimensions of \c X and <tt>Q[i]</tt>, then the coefficients computed during the orthogonalization
       routine will be stored in the matrix <tt>C[i]</tt>, similar to calling
       \code
          innerProd(Q[i], X, C[i]);
       \endcode
       If <tt>C[i]</tt> points to a MatrixEigenDense with size
       inconsistent with \c X and \c <tt>Q[i]</tt>, then a std::invalid_argument
       exception will be thrown. Otherwise, if <tt>C.size() < i</tt> or
       <tt>C[i]</tt> is a null pointer, the caller will not have access to the
       computed coefficients.

     @param[in] Q A list of multivector bases specifying the subspaces to be orthogonalized against, satisfying
     \f[
        \langle Q[i], Q[j] \rangle = I \quad\textrm{if}\quad i=j
     \f]
     and
     \f[
        \langle Q[i], Q[j] \rangle = 0 \quad\textrm{if}\quad i \neq j\ .
     \f]
     @param MQ
    */
    void projectMat (
          MV& X,
          std::vector<SmartPtr<const MV> > Q,
          std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C, // = std::vector<SmartPtr>(_smPtrNull),
              //= Teuchos::tuple(SmartPtr<MatrixEigenDense<ScalarType> >(_smPtrNull)),
          SmartPtr<MV> MX, //                        = _smPtrNull,
          std::vector<SmartPtr<const MV> > MQ // = std::vector<SmartPtr>(_smPtrNull)// = Teuchos::tuple(SmartPtr<const MV>(_smPtrNull))
       ) const;


    /*! \brief This method takes a multivector \c X and attempts to compute an orthonormal basis for \f$colspan(X)\f$, with respect to innerProd().
     *
     * The method uses classical Gram-Schmidt with selective reorthogonalization. As a result, the coefficient matrix \c B is upper triangular.
     *
     * This routine returns an integer \c rank stating the rank of the computed basis. If \c X does not have full rank and the normalize() routine does
     * not attempt to augment the subspace, then \c rank may be smaller than the number of columns in \c X. In this case, only the first \c rank columns of
     * output \c X and first \c rank rows of \c B will be valid.
     *
     * The method attempts to find a basis with dimension equal to the number of columns in \c X. It does this by augmenting linearly dependent
     * vectors in \c X with random directions. A finite number of these attempts will be made; therefore, it is possible that the dimension of the
     * computed basis is less than the number of vectors in \c X.
     *
     @param[in,out] X The multivector to be modified.<br>
       On output, the first \c rank columns of \c X satisfy
       \f[
          \langle X[i], X[j] \rangle = \delta_{ij}\ .
       \f]
       Also,
       \f[
          X_{in}(1:m,1:n) = X_{out}(1:m,1:rank) B(1:rank,1:n)
       \f]
       where \c m is the number of rows in \c X and \c n is the number of columns in \c X.

     @param[in,out] MX The image of \c X under the inner product operator \c Op.
       If \f$ MX != 0\f$: On input, this is expected to be consistent with \c Op . X. On output, this is updated consistent with updates to \c X.
       If \f$ MX == 0\f$ or \f$ Op == 0\f$: \c MX is not referenced.

     @param[out] B The coefficients of the original \c X with respect to the computed basis. If \c B is a non-null pointer and \c B matches the dimensions of \c B, then the
     coefficients computed during the orthogonalization routine will be stored in \c B, similar to calling
       \code
          innerProd(Xout, Xin, B);
       \endcode
     If \c B points to a MatrixEigenDense with size inconsistent with \c X, then a std::invalid_argument exception will be thrown. Otherwise, if \c B is null, the caller will not have
     access to the computed coefficients. This matrix is not necessarily triangular (as in a QR factorization); see the documentation of specific orthogonalization managers.<br>
     The first rows in \c B corresponding to the valid columns in \c X will be upper triangular.

     @return Rank of the basis computed by this method, less than or equal to the number of columns in \c X. This specifies how many columns in the returned \c X and rows in the returned \c B are valid.
    */
    int normalizeMat (
          MV& X,
          SmartPtr<MatrixEigenDense<ScalarType> > B = _smPtrNull,
          SmartPtr<MV> MX = _smPtrNull
       ) const;


    /*! \brief Given a set of bases <tt>Q[i]</tt> and a multivector \c X, this method computes an orthonormal basis for \f$colspan(X) - \sum_i colspan(Q[i])\f$.
     *
     *  This routine returns an integer \c rank stating the rank of the computed basis. If the subspace \f$colspan(X) - \sum_i colspan(Q[i])\f$ does not
     *  have dimension as large as the number of columns of \c X and the orthogonalization manager doe not attempt to augment the subspace, then \c rank
     *  may be smaller than the number of columns of \c X. In this case, only the first \c rank columns of output \c X and first \c rank rows of \c B will
     *  be valid.
     *
     * The method attempts to find a basis with dimension the same as the number of columns in \c X. It does this by augmenting linearly dependent
     * vectors with random directions. A finite number of these attempts will be made; therefore, it is possible that the dimension of the
     * computed basis is less than the number of vectors in \c X.
     *
     @param[in,out] X The multivector to be modified.<br>
       On output, the first \c rank columns of \c X satisfy
       \f[
            \langle X[i], X[j] \rangle = \delta_{ij} \quad \textrm{and} \quad \langle X, Q[i] \rangle = 0\ .
       \f]
       Also,
       \f[
          X_{in}(1:m,1:n) = X_{out}(1:m,1:rank) B(1:rank,1:n) + \sum_i Q[i] C[i]
       \f]
       where \c m is the number of rows in \c X and \c n is the number of columns in \c X.

     @param[in,out] MX The image of \c X under the inner product operator \c Op.
       If \f$ MX != 0\f$: On input, this is expected to be consistent with \c Op . X. On output, this is updated consistent with updates to \c X.
       If \f$ MX == 0\f$ or \f$ Op == 0\f$: \c MX is not referenced.

     @param[out] C The coefficients of \c X in the <tt>Q[i]</tt>. If <tt>C[i]</tt> is a non-null pointer
       and <tt>C[i]</tt> matches the dimensions of \c X and <tt>Q[i]</tt>, then the coefficients computed during the orthogonalization
       routine will be stored in the matrix <tt>C[i]</tt>, similar to calling
       \code
          innerProd(Q[i], X, C[i]);
       \endcode
       If <tt>C[i]</tt> points to a MatrixEigenDense with size
       inconsistent with \c X and \c <tt>Q[i]</tt>, then a std::invalid_argument
       exception will be thrown. Otherwise, if <tt>C.size() < i</tt> or
       <tt>C[i]</tt> is a null pointer, the caller will not have access to the
       computed coefficients.

     @param[out] B The coefficients of the original \c X with respect to the computed basis. If \c B is a non-null pointer and \c B matches the dimensions of \c B, then the
     coefficients computed during the orthogonalization routine will be stored in \c B, similar to calling
       \code
          innerProd(Xout, Xin, B);
       \endcode
     If \c B points to a MatrixEigenDense with size inconsistent with \c X, then a std::invalid_argument exception will be thrown. Otherwise, if \c B is null, the caller will not have
     access to the computed coefficients. This matrix is not necessarily triangular (as in a QR factorization); see the documentation of specific orthogonalization managers.<br>
     The first rows in \c B corresponding to the valid columns in \c X will be upper triangular.

     @param[in] Q A list of multivector bases specifying the subspaces to be orthogonalized against, satisfying
     \f[
        \langle Q[i], Q[j] \rangle = I \quad\textrm{if}\quad i=j
     \f]
     and
     \f[
        \langle Q[i], Q[j] \rangle = 0 \quad\textrm{if}\quad i \neq j\ .
     \f]
     @param MQ
     
     @return Rank of the basis computed by this method, less than or equal to the number of columns in \c X. This specifies how many columns in the returned \c X and rows in the returned \c B are valid.

    */
    int projectAndNormalizeMat (
          MV& X,
          std::vector<SmartPtr<const MV> >  Q,
          std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C, // = std::vector<SmartPtr>(_smPtrNull),
              //= Teuchos::tuple(SmartPtr<MatrixEigenDense<ScalarType> >(_smPtrNull)),
              SmartPtr<MatrixEigenDense<ScalarType> > B, //                  = _smPtrNull,
              SmartPtr<MV> MX = _smPtrNull,
          std::vector<SmartPtr<const MV> > MQ = std::vector<SmartPtr<const MV> >(1,_smPtrNull)
       ) const;
    //@}

    //! @name Error methods
    //@{

    /*! \brief This method computes the error in orthonormality of a multivector, measured
     * as the Frobenius norm of the difference <tt>innerProd(X,Y) - I</tt>.
     *  The method has the option of exploiting a caller-provided \c MX.
     */
    typename NumTraits<ScalarType>::RealScalar
    orthonormErrorMat(const MV& X, SmartPtr<const MV> MX = _smPtrNull) const;

    /*! \brief This method computes the error in orthogonality of two multivectors, measured
     * as the Frobenius norm of <tt>innerProd(X,Y)</tt>.
     *  The method has the option of exploiting a caller-provided \c MX.
     */
    typename NumTraits<ScalarType>::RealScalar
    orthogErrorMat(const MV& X1, const MV& X2, SmartPtr<const MV> MX1, SmartPtr<const MV> MX2) const;
    //@}

    //! @name Accessor routines
    //@{
    //! Set parameter for re-orthogonalization threshold.
    void setKappa(typename NumTraits<ScalarType>::RealScalar kappa) { kappa_ = kappa; }

    //! Return parameter for re-orthogonalization threshold.
    typename NumTraits<ScalarType>::RealScalar getKappa() const { return kappa_; }
    //@}

  private:
    //! Parameter for re-orthogonalization.
    MagnitudeType kappa_;
    MagnitudeType eps_;
    MagnitudeType tol_;

    // ! Routine to find an orthonormal basis
    int findBasis(MV& X, SmartPtr<MV> MX,
                  MatrixEigenDense<ScalarType>& B,
                  bool completeBasis, int howMany = -1) const;
};


//=============================================================================================
// Constructor
template<class ScalarType, class MV, class OP>
BasicOrthoManager<ScalarType,MV,OP>::BasicOrthoManager(SmartPtr<const OP> Op,
                                                      typename NumTraits<ScalarType>::RealScalar kappa,
                                                      typename NumTraits<ScalarType>::RealScalar eps,
                                                      typename NumTraits<ScalarType>::RealScalar tol) :
                                                      MatOrthoManager<ScalarType,MV,OP>(Op), kappa_(kappa), eps_(eps), tol_(tol)
{
  internalEigenSolver::testErrorEigenProblem(eps_ < SCT::magnitude(SCT::zero()),
      "xlifepp::BasicOrthoManager::BasicOrthoManager(): argument \"eps\" must be non-negative.");
  if (eps_ == 0) {
    eps_ = NumTraits<MagnitudeType>::pow(theEpsilon,.75);
  }
  internalEigenSolver::testErrorEigenProblem(tol_ < SCT::magnitude(SCT::zero()) || tol_ > SCT::magnitude(SCT::one()),
      "xlifepp::BasicOrthoManager::BasicOrthoManager(): argument \"tol\" must be in [0,1].");
}

//=============================================================================================
// Compute the distance from orthonormality
template<class ScalarType, class MV, class OP>
typename NumTraits<ScalarType>::RealScalar
BasicOrthoManager<ScalarType,MV,OP>::orthonormErrorMat(const MV& X, SmartPtr<const MV> MX) const
{
  const ScalarType ONE = SCT::one();
  int rank = MVT::getNumberVecs(X);
  MatrixEigenDense<ScalarType> xTx(rank,rank);
  MatOrthoManager<ScalarType,MV,OP>::innerProdMat(X,X,xTx,MX,MX);
  for (int i=0; i<rank; i++) {
    xTx.coeffRef(i,i) -= ONE;
  }
  return xTx.normFrobenius();
}

//=============================================================================================
// Compute the distance from orthogonality
template<class ScalarType, class MV, class OP>
typename NumTraits<ScalarType>::RealScalar
BasicOrthoManager<ScalarType,MV,OP>::orthogErrorMat(const MV& X1, const MV& X2, SmartPtr<const MV> MX1, SmartPtr<const MV> MX2) const
{
  int r1 = MVT::getNumberVecs(X1);
  int r2  = MVT::getNumberVecs(X2);
  MatrixEigenDense<ScalarType> xTx(r1,r2);
  MatOrthoManager<ScalarType,MV,OP>::innerProdMat(X1,X2,xTx,MX1,MX2);
  return xTx.normFrobenius();
}

//=============================================================================================
template<class ScalarType, class MV, class OP>
void BasicOrthoManager<ScalarType, MV, OP>::projectMat(
      MV& X,
      std::vector<SmartPtr<const MV> > Q,
      std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C,
      SmartPtr<MV> MX,
      std::vector<SmartPtr<const MV> > MQ
 ) const
{
  // For the inner product defined by the operator Op or the identity (Op == 0)
  //   -> Orthogonalize X against each Q[i]
  // Modify MX accordingly
  //
  // Note that when Op is 0, MX is not referenced
  //
  // Parameter variables
  //
  // X: Vectors to be transformed
  //
  // MX: Image of the block vector X by the mass matrix
  //
  // Q: Bases to orthogonalize against. These are assumed orthonormal, mutually and independently.
  //

  if (internalEigenSolver::EigenSolverVerbose::isDebug()){
      std::cout << "Entering xlifepp::BasicOrthoManager::projectMat(...)\n";
  }

  ScalarType ONE  = SCT::one();

  int xc = MVT::getNumberVecs(X);
  int xr = MVT::getVecLength(X);
  int nq = Q.size();
  std::vector<int> qcs(nq);
  // short-circuit
  if (nq == 0 || xc == 0 || xr == 0) {
    if (internalEigenSolver::EigenSolverVerbose::isDebug()){
        std::cout << "Leaving xlifepp::BasicOrthoManager::projectMat(...)\n";
    }
    return;
  }
  int qr = MVT::getVecLength (*Q[0]);
  // if we don't have enough C, expand it with null references
  // if we have too many, resize to throw away the latter ones
  // if we have exactly as many as we have Q, this call has no effect
  C.resize(nq);

  /* *****   DO NO MODIFY *MX IF hasOp_ == false   ***** */
  if (this->hasOp_) {
    if (MX == _smPtrNull) {
      // we need to allocate space for MX
      MX = MVT::clone(X,MVT::getNumberVecs(X));
      OPT::apply(*(this->Op_),X,*MX);
      this->OpCounter_ += MVT::getNumberVecs(X);
    }
  }
  else {
    // Op == I  -->  MX = X (ignore it if the user passed it in)
    MX = smartPtrFromRef(&X);
  }
  int mxc = MVT::getNumberVecs(*MX);
  int mxr = MVT::getVecLength(*MX);

  // check size of X and Q w.r.t. common sense
  internalEigenSolver::testErrorEigenProblem(xc<0 || xr<0 || mxc<0 || mxr<0,
                      "xlifepp::BasicOrthoManager::projectMat(): MVT returned negative dimensions for X,MX");
  // check size of X w.r.t. MX and Q
  internalEigenSolver::testErrorEigenProblem(xc!=mxc || xr!=mxr || xr!=qr,
                      "xlifepp::BasicOrthoManager::projectMat(): Size of X not consistent with MX,Q");

  // tally up size of all Q and check/allocate C
  int baslen = 0;
  for (int i=0; i<nq; i++) {
    internalEigenSolver::testErrorEigenProblem(MVT::getVecLength(*Q[i]) != qr,
                        "xlifepp::BasicOrthoManager::projectMat(): Q lengths not mutually consistent");
    qcs[i] = MVT::getNumberVecs(*Q[i]);
    internalEigenSolver::testErrorEigenProblem(qr < qcs[i],
                        "xlifepp::BasicOrthoManager::projectMat(): Q has less rows than columns");
    baslen += qcs[i];

    // check size of C[i]
    if (C[i] == _smPtrNull) {
      C[i] = SmartPtr<MatrixEigenDense<ScalarType> >(new MatrixEigenDense<ScalarType>(qcs[i],xc));
    }
    else {
      internalEigenSolver::testErrorEigenProblem(C[i]->numOfRows() != qcs[i] || C[i]->numOfCols() != xc ,
                         "xlifepp::BasicOrthoManager::projectMat(): Size of Q not consistent with size of C");
    }
  }

  // Perform the Gram-Schmidt transformation for a block of vectors
  // Compute the initial Op-norms
  std::vector<ScalarType> oldDot(xc);
  MVT::mvDot(X, *MX, oldDot);
  MQ.resize(nq);

  // Define the product Q^T * (Op*X)
  for (int i=0; i<nq; i++) {
    // Multiply Q' with MX
    MatOrthoManager<ScalarType,MV,OP>::innerProdMat(*Q[i],X,*C[i],MQ[i],MX);

    // Multiply by Q and subtract the result in X
    MVT::mvTimesMatAddMv(-ONE, *Q[i], *C[i], ONE, X);

    // Update MX, with the least number of applications of Op as possible
    // Update MX. If we have MQ, use it. Otherwise, just multiply by Op
    if (this->hasOp_) {
      if (MQ[i] == _smPtrNull) {
        OPT::apply(*(this->Op_), X, *MX);
        this->OpCounter_ += MVT::getNumberVecs(X);
      }
      else {
        MVT::mvTimesMatAddMv(-ONE, *MQ[i], *C[i], ONE, *MX);
      }
    }
  }

  // Compute new Op-norms
  std::vector<ScalarType> newDot(xc);
  MVT::mvDot(X, *MX, newDot);
  // determine (individually) whether to do another step of classical Gram-Schmidt
  for (int j = 0; j < xc; ++j) {

    if (SCT::magnitude(kappa_*newDot[j]) < SCT::magnitude(oldDot[j])) {
      for (int i=0; i<nq; i++) {
        MatrixEigenDense<ScalarType> C2(*C[i]);

        // apply another step of classical Gram-Schmidt
        MatOrthoManager<ScalarType,MV,OP>::innerProdMat(*Q[i],X,C2,MQ[i],MX);
        *C[i] += C2;
        MVT::mvTimesMatAddMv(-ONE, *Q[i], C2, ONE, X);

        // Update MX as above
        if (this->hasOp_) {
          if (MQ[i] == _smPtrNull) {
            OPT::apply(*(this->Op_), X, *MX);
            this->OpCounter_ += MVT::getNumberVecs(X);
          }
          else {
            MVT::mvTimesMatAddMv(-ONE, *MQ[i], C2, ONE, *MX);
          }
        }
      }
      break;
    } // if (kappa_*newDot[j] < oldDot[j])
  } // for (int j = 0; j < xc; ++j)

  internalEigenSolver::printOutDebugInfoEigenProblem("xlifepp::BasicOrthoManager", "Leaving xlifepp::BasicOrthoManager::projectMat(...)\n");
}

//=============================================================================================
// Find an Op-orthonormal basis for span(X), with rank numvectors(X)
template<class ScalarType, class MV, class OP>
int BasicOrthoManager<ScalarType, MV, OP>::normalizeMat(
        MV& X,
        SmartPtr<MatrixEigenDense<ScalarType> > B,
        SmartPtr<MV> MX) const
{
  // call findBasis(), with the instruction to try to generate a basis of rank numvecs(X)
  // findBasis() requires MX

  int xc = MVT::getNumberVecs(X);
  int xr = MVT::getVecLength(X);

  // if Op==null, MX == X (via pointer)
  // Otherwise, either the user passed in MX or we will allocated and compute it
  if (this->hasOp_) {
    if (_smPtrNull == MX) {
      // we need to allocate space for MX
      MX = MVT::clone(X,xc);
      OPT::apply(*(this->Op_),X,*MX);
      this->OpCounter_ += MVT::getNumberVecs(X);
    }
  }

  // if the user doesn't want to store the coefficients,
  // allocate some local memory for them
  if (_smPtrNull == B) {
    B = SmartPtr<MatrixEigenDense<ScalarType> >(new MatrixEigenDense<ScalarType>(xc,xc));
  }

  int mxc = (this->hasOp_) ? MVT::getNumberVecs(*MX) : xc;
  int mxr = (this->hasOp_) ? MVT::getVecLength(*MX)  : xr;

  // check size of C, B
  internalEigenSolver::testErrorEigenProblem(xc == 0 || xr == 0,
                      "xlifepp::BasicOrthoManager::normalizeMat(): X must be non-empty");
  internalEigenSolver::testErrorEigenProblem(B->numOfRows() != xc || B->numOfCols() != xc,
                      "xlifepp::BasicOrthoManager::normalizeMat(): Size of X not consistent with size of B");
  internalEigenSolver::testErrorEigenProblem(xc != mxc || xr != mxr,
                      "xlifepp::BasicOrthoManager::normalizeMat(): Size of X not consistent with size of MX");
  internalEigenSolver::testErrorEigenProblem(xc > xr,
                      "xlifepp::BasicOrthoManager::normalizeMat(): Size of X not feasible for normalization");

  return findBasis(X, MX, *B, true);
}

  //=============================================================================================
  // Find an Op-orthonormal basis for span(X) - span(W)
  template<class ScalarType, class MV, class OP>
  int BasicOrthoManager<ScalarType, MV, OP>::projectAndNormalizeMat(
          MV& X,
          std::vector<SmartPtr<const MV> >  Q,
          std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > C,
          SmartPtr<MatrixEigenDense<ScalarType> > B,
          SmartPtr<MV> MX,
          std::vector<SmartPtr<const MV> > MQ
     ) const {

  internalEigenSolver::printOutDebugInfoEigenProblem("xlifepp::BasicOrthoManager", "Entering xlifepp::BasicOrthoManager::projectAndNormalizeMat(...)\n");

  int nq = Q.size();
  int xc = MVT::getNumberVecs(X);
  int xr = MVT::getVecLength(X);
  int rank;

  /* if the user doesn't want to store the coefficients,
   * allocate some local memory for them
   */
  if ( _smPtrNull == B) {
    B = SmartPtr<MatrixEigenDense<ScalarType> >(new MatrixEigenDense<ScalarType>(xc,xc));
  }

  /* *****   DO NO MODIFY *MX IF hasOp_ == false   ***** */
  if (this->hasOp_) {
    if (_smPtrNull == MX) {
      // we need to allocate space for MX
      MX = MVT::clone(X,MVT::getNumberVecs(X));
      OPT::apply(*(this->Op_),X,*MX);
      this->OpCounter_ += MVT::getNumberVecs(X);
    }
  }
  else {
    // Op == I  -->  MX = X (ignore it if the user passed it in)
    MX = smartPtrFromRef(&X);
  }

  int mxc = MVT::getNumberVecs(*MX);
  int mxr = MVT::getVecLength(*MX);

  internalEigenSolver::testErrorEigenProblem(xc == 0 || xr == 0,  "xlifepp::BasicOrthoManager::projectAndNormalizeMat(): X must be non-empty");

  int numbas = 0;
  for (int i=0; i<nq; i++) {
    numbas += MVT::getNumberVecs(*Q[i]);
  }

  // check size of B
  internalEigenSolver::testErrorEigenProblem(B->numOfRows() != xc || B->numOfCols() != xc,
                      "xlifepp::BasicOrthoManager::projectAndNormalizeMat(): Size of X must be consistent with size of B");
  // check size of X and MX
  internalEigenSolver::testErrorEigenProblem(xc<0 || xr<0 || mxc<0 || mxr<0,
                      "xlifepp::BasicOrthoManager::projectAndNormalizeMat(): MVT returned negative dimensions for X,MX");
  // check size of X w.r.t. MX
  internalEigenSolver::testErrorEigenProblem(xc!=mxc || xr!=mxr,
                      "xlifepp::BasicOrthoManager::projectAndNormalizeMat(): Size of X must be consistent with size of MX");
  // check feasibility
  internalEigenSolver::testErrorEigenProblem(numbas+xc > xr,
                      "xlifepp::BasicOrthoManager::projectAndNormalizeMat(): Orthogonality constraints not feasible");

  // orthogonalize all of X against Q
  projectMat(X,Q,C,MX,MQ);

  MatrixEigenDense<ScalarType> oldCoeff(xc,1);
  // start working
  rank = 0;
  int numTries = 10;   // each vector in X gets 10 random chances to escape degeneracy
  int oldrank = -1;
  do {
    int curxsize = xc - rank;

    // orthonormalize X, but quit if it is rank deficient
    // we can't let findBasis generated random vectors to complete the basis,
    // because it doesn't know about Q; we will do this ourselves below
    rank = findBasis(X,MX,*B,false,curxsize);

    if (oldrank != -1 && rank != oldrank) {
      // we had previously stopped before, after operating on vector oldrank
      // we saved its coefficients, augmented it with a random vector, and
      // then called findBasis() again, which proceeded to add vector oldrank
      // to the basis.
      // now, restore the saved coefficients into B
      for (int i=0; i<xc; i++) {
        B->coeffRef(i,oldrank) = oldCoeff.coeff(i,0);
      }
    }

    if (rank < xc) {
      if (rank != oldrank) {
        // we quit on this vector and will augment it with random below
        // this is the first time that we have quit on this vector
        // therefor, (*B)(:,rank) contains the actual coefficients of the
        // input vectors with respect to the previous vectors in the basis
        // save these values, as (*B)(:,rank) will be overwritten by our next
        // call to findBasis()
        // we will restore it after we are done working on this vector
        for (int i=0; i<xc; i++) {
          oldCoeff.coeffRef(i,0) = (B)->coeff(i,rank);
        }
      }
    }

    if (rank == xc) {
      // we are done
      break;
    }
    else {
      internalEigenSolver::testErrorEigenProblem(rank < oldrank,
                          "xlifepp::BasicOrthoManager::projectAndNormalizeMat(): basis lost rank; this shouldn't happen");

      if (rank != oldrank) {
        // we added a vector to the basis; reset the chance counter
        numTries = 10;
        // store old rank
        oldrank = rank;
      }
      else {
        // has this vector run out of chances to escape degeneracy?
        if (numTries <= 0) {
          break;
        }
      }
      // use one of this vector's chances
      numTries--;

      // randomize troubled direction
      SmartPtr<MV> curX, curMX;
      std::vector<int> ind(1);
      ind[0] = rank;
      curX  = MVT::cloneViewNonConst(X,ind);
      MVT::mvRandom(*curX);
      if (this->hasOp_) {
        curMX = MVT::cloneViewNonConst(*MX,ind);
        OPT::apply(*(this->Op_), *curX, *curMX);
        this->OpCounter_ += MVT::getNumberVecs(*curX);
      }

      // orthogonalize against Q
      // if !this->hasOp_, the curMX will be ignored.
      // we don't care about these coefficients
      // on the contrary, we need to preserve the previous coeffs
      {
        std::vector<SmartPtr<MatrixEigenDense<ScalarType> > > dummyC(0);
        projectMat(*curX,Q,dummyC,curMX,MQ);
      }
    }
  } while (1);

  // this should never raise an exception; but our post-conditions oblige us to check
  internalEigenSolver::testErrorEigenProblem(rank > xc || rank < 0,
                      "xlifepp::BasicOrthoManager::projectAndNormalizeMat(): Debug error in rank variable.");
  internalEigenSolver::printOutDebugInfoEigenProblem("xlifepp::BasicOrthoManager", "Leaving xlifepp::BasicOrthoManager::projectAndNormalizeMat(...)\n");
  return rank;
}

//=============================================================================================
// Find an Op-orthonormal basis for span(X), with the option of extending the subspace so that
// the rank is numvectors(X)
template<class ScalarType, class MV, class OP>
int BasicOrthoManager<ScalarType, MV, OP>::findBasis(
              MV& X, SmartPtr<MV> MX,
              MatrixEigenDense<ScalarType>& B,
              bool completeBasis, int howMany) const {

  // For the inner product defined by the operator Op or the identity (Op == 0)
  //   -> Orthonormalize X
  // Modify MX accordingly
  //
  // Note that when Op is 0, MX is not referenced
  //
  // Parameter variables
  //
  // X: Vectors to be orthonormalized
  //
  // MX: Image of the multivector X under the operator Op
  //
  // Op: Pointer to the operator for the inner product
  //
  internalEigenSolver::printOutDebugInfoEigenProblem("xlifepp::BasicOrthoManager", "Entering xlifepp::BasicOrthoManager::findBasis(...)\n");

  const ScalarType ONE  = SCT::one();
  const MagnitudeType ZERO = SCT::magnitude(SCT::zero());

  int xc = MVT::getNumberVecs(X);

  if (howMany == -1) {
    howMany = xc;
  }

  /* ******************************************************
   *  If hasOp_ == false, we will not reference MX below *
   ****************************************************** */
  internalEigenSolver::testErrorEigenProblem(this->hasOp_ == true && MX == _smPtrNull,
      "xlifepp::BasicOrthoManager::findBasis(): calling routine did not specify MS.");
  internalEigenSolver::testErrorEigenProblem(howMany < 0 || howMany > xc,
                      "xlifepp::BasicOrthoManager::findBasis(): Invalid howMany parameter");

  /* xstart is which column we are starting the process with, based on howMany
   * columns before xstart are assumed to be Op-orthonormal already
   */
  int xstart = xc - howMany;

  for (int j = xstart; j < xc; j++) {

    // numX represents the number of currently orthonormal columns of X
    int numX = j;
    // j represents the index of the current column of X
    // these are different interpretations of the same value

    //
    // set the lower triangular part of B to zero
    for (int i=j+1; i<xc; ++i) {
      B.coeffRef(i,j) = ZERO;
    }

    // Get a view of the vector currently being worked on.
    std::vector<int> index(1);
    index[0] = j;
    SmartPtr<MV> Xj = MVT::cloneViewNonConst(X, index);
    SmartPtr<MV> MXj;
    if ((this->hasOp_)) {
      // MXj is a view of the current vector in MX
      MXj = MVT::cloneViewNonConst(*MX, index);
    }
    else {
      // MXj is a pointer to Xj, and MUST NOT be modified
      MXj = Xj;
    }

    // Get a view of the previous vectors.
    std::vector<int> prevIdx(numX);
    SmartPtr<const MV> prevX, prevMX;

    if (numX > 0) {
      for (int i=0; i<numX; ++i) prevIdx[i] = i;
      prevX = MVT::cloneViewNonConst(X, prevIdx);
      if (this->hasOp_) {
        prevMX = MVT::cloneViewNonConst(*MX, prevIdx);
      }
    }

    bool rankDef = true;
    /* numTrials>0 will denote that the current vector was randomized for the purpose
     * of finding a basis vector, and that the coefficients of that vector should
     * not be stored in B
     */
    for (int numTrials = 0; numTrials < 10; numTrials++) {
      // Make storage for these Gram-Schmidt iterations.
      MatrixEigenDense<ScalarType> product(numX, 1);
      std::vector<MagnitudeType> origNorm(1), newNorm(1), newNorm2(1);

      //
      // Save old MXj vector and compute Op-norm
      //
      SmartPtr<MV> oldMXj = MVT::cloneCopy(*MXj);
      MatOrthoManager<ScalarType,MV,OP>::normMat(*Xj,origNorm,MXj);

      if (numX > 0) {
        // apply the first step of Gram-Schmidt

        // product <- prevX^T MXj
        MatOrthoManager<ScalarType,MV,OP>::innerProdMat(*prevX,*Xj,product,_smPtrNull,MXj);

        // Xj <- Xj - prevX prevX^T MXj
        //     = Xj - prevX product
        MVT::mvTimesMatAddMv(-ONE, *prevX, product, ONE, *Xj);

        // Update MXj
        if (this->hasOp_) {
          // MXj <- Op*Xj_new
          //      = Op*(Xj_old - prevX prevX^T MXj)
          //      = MXj - prevMX product
          MVT::mvTimesMatAddMv(-ONE, *prevMX, product, ONE, *MXj);
        }

        // Compute new Op-norm
        MatOrthoManager<ScalarType,MV,OP>::normMat(*Xj,newNorm,MXj);
        MagnitudeType product_norm = product.normOne();

        // Check if a correction is needed.
        if (product_norm/newNorm[0] >= tol_ || newNorm[0] < eps_*origNorm[0]) {
          // apply the second step of Gram-Schmidt
          // This is the same as above
          MatrixEigenDense<ScalarType> P2(numX,1);
          MatOrthoManager<ScalarType,MV,OP>::innerProdMat(*prevX,*Xj,P2,_smPtrNull,MXj);
          product += P2;
          MVT::mvTimesMatAddMv(-ONE, *prevX, P2, ONE, *Xj);
          if ((this->hasOp_)) {
            MVT::mvTimesMatAddMv(-ONE, *prevMX, P2, ONE, *MXj);
          }
          // Compute new Op-norms
          MatOrthoManager<ScalarType,MV,OP>::normMat(*Xj,newNorm2,MXj);
          product_norm = P2.normOne();

          if (product_norm/newNorm2[0] >= tol_ || newNorm2[0] < eps_*origNorm[0]) {
            // we don't do another GS, we just set it to zero.
            MVT::mvInit(*Xj,ZERO);
            if ((this->hasOp_)) {
              MVT::mvInit(*MXj,ZERO);
            }
          }
        }
      } // if (numX > 0) do GS

      // save the coefficients, if we are working on the original vector and not a randomly generated one
      if (numTrials == 0) {
        for (int i=0; i<numX; i++) {
          B.coeffRef(i,j) = product.coeff(i,0);
        }
      }

      // Check if Xj has any directional information left after the orthogonalization.
      MatOrthoManager<ScalarType,MV,OP>::normMat(*Xj,newNorm,MXj);
      if (newNorm[0] != ZERO && newNorm[0] > SCT::sfmin()) {
        // Normalize Xj.
        // Xj <- Xj / norm
        MVT::mvScale(*Xj, ONE/newNorm[0]);
        if (this->hasOp_) {
          // Update MXj.
          MVT::mvScale(*MXj, ONE/newNorm[0]);
        }

        // save it, if it corresponds to the original vector and not a randomly generated one
        if (numTrials == 0) {
          B.coeffRef(j,j) = newNorm[0];
        }

        // We are not rank deficient in this vector. Move on to the next vector in X.
        rankDef = false;
        break;
      }
      else {
        // There was nothing left in Xj after orthogonalizing against previous columns in X.
        // X is rank deficient.
        // reflect this in the coefficients
        B.coeffRef(j,j) = ZERO;

        if (completeBasis) {
          // Fill it with random information and keep going.
          MVT::mvRandom(*Xj);
          if (this->hasOp_) {
            OPT::apply(*(this->Op_), *Xj, *MXj);
            this->OpCounter_ += MVT::getNumberVecs(*Xj);
          }
        }
        else {
          rankDef = true;
          break;
        }
      }
    }  // for (numTrials = 0; numTrials < 10; ++numTrials)

    // if rankDef == true, then quit and notify user of rank obtained
    if (rankDef == true) {
      internalEigenSolver::testWarningEigenProblem(rankDef && completeBasis,
                          "xlifepp::BasicOrthoManager::findBasis(): Unable to complete basis");
      if (internalEigenSolver::EigenSolverVerbose::isDebug()){
        std::cout << "Returning early, rank " << j << " from xlifepp::BasicOrthoManager::findBasis(...)\n";
      }
      return j;
    }

  } // for (j = 0; j < xc; ++j)

  if (internalEigenSolver::EigenSolverVerbose::isDebug()){
      std::cout << "Returning " << xc << " from xlifepp::BasicOrthoManager::findBasis(...)\n";
  }
  return xc;
}

} // namespace xlifepp

#endif // XLIFEPP_BASIC_ORTHOMANAGER_HPP


