/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppEigenProblem.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013

  \brief Base class which defines the interface required by an eigensolver and
  status test class to compute solutions to an eigenproblem
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_EIGENPROBLEM_HPP
#define XLIFEPP_EIGENPROBLEM_HPP

#include "XlifeppEigenProblem.hpp"
#include "XlifeppMultiVecTraits.hpp"
#include "XlifeppOperatorTraits.hpp"

namespace xlifepp {

/*!
   \class EigenProblem
   \brief This provides a basic implementation for defining standard or 
   generalized eigenvalue problems.
*/
template<class ScalarType, class MV, class OP>
class EigenProblem
{
  public:
    
    //! @name Constructors/Destructor
    //@{ 
    
    //! Empty constructor - allows xlifepp::EigenProblem to be described at a later time through "Set Methods".
    EigenProblem();
    
    //! Standard Eigenvalue Problem Constructor.
    EigenProblem(const SmartPtr<const OP>& Op, const SmartPtr<MV>& initVec);
    
    //! Generalized Eigenvalue Problem Constructor.
    EigenProblem(const SmartPtr<const OP>& Op, const SmartPtr<const OP>& B, const SmartPtr<MV>& initVec);
    
    //! Copy Constructor.
    EigenProblem(const EigenProblem<ScalarType, MV, OP>& Problem);
    
    //! Destructor.
    ~EigenProblem() {}
    //@}
    
    //! @name Set Methods
    //@{ 
    
    /*! \brief Set the operator for which eigenvalues will be computed.  

    \note This may be different from the \c A if a spectral transformation is employed. 
    For example, this operator may apply the operation \f$(A-\sigma I)^{-1}\f$ if you are
    looking for eigenvalues of \c A around \f$\sigma\f$.  
    */
    void setOperator(const SmartPtr<const OP>& Op) { Op_ = Op; isSet_=false; };
    
    /*! \brief Set the operator \c A of the eigenvalue problem \f$Ax=Mx\lambda\f$.
    */
    void setA(const SmartPtr<const OP>& A) { AOp_ = A; isSet_=false; };
    
    /*! \brief Set the operator \c M of the eigenvalue problem \f$Ax = Mx\lambda\f$.
     */
    void setM(const SmartPtr<const OP>& M) { MOp_ = M; isSet_=false; };
    
    /*! \brief Set the preconditioner for this eigenvalue problem \f$Ax = Mx\lambda\f$.
     */
    void setPrec(const SmartPtr<const OP>& prec) { prec_ = prec; isSet_=false; };
    
    /*! \brief Set the initial guess.  

    This vector is required to create all the space needed 
    by xlifepp to solve the eigenvalue problem.

    \note Even if an initial guess is not known by the user, an initial vector must be passed in.  
    */
    void setInitVec(const SmartPtr<MV>& initVec) { initVec_ = initVec; isSet_=false; };
    
    /*! \brief Set auxiliary vectors.

    \note This multivector can have any number of columns, and most likely will contain vectors that
    will be used by the eigensolver to orthogonalize against.
    */
    void setAuxVecs(const SmartPtr<const MV>& auxVecs) { auxVecs_ = auxVecs; isSet_=false; };

    //! Specify the number of eigenvalues (NEV) that are requested.
    void setNEV(int_t nev){ nev_ = nev; isSet_ = false; };

    //! Specify the symmetry of this eigenproblem.
    /*! This knowledge may allow the solver to take advantage of the eigenproblems' symmetry.
      Some computational work can be avoided by setting this properly.
    */
    void setHermitian(bool isSym){ isSym_ = isSym; isSet_ = false; };

    /*! \brief Specify that this eigenproblem is fully defined.
     *
     * This routine serves multiple purpose:
     *    - sanity check that the eigenproblem has been fully and consistently defined
     *    - opportunity for the eigenproblem to allocate internal storage for eigenvalues
     * and eigenvectors (to be used by eigensolvers and solver managers)
     *
     * This method reallocates internal storage, so that any previously retrieved references to 
     * internal storage (eigenvectors or eigenvalues) are invalidated.
     *
     * \note The user MUST call this routine before they send the eigenproblem to any solver or solver manager.
     *
     * \returns \c true signifies success, \c false signifies error.
     */
    bool setProblem();

    /*! \brief Set the solution to the eigenproblem.
     *
     * This mechanism allows an EigenSolverSolution struct to be associated with an EigenProblem object.
     * setSolution() is usually called by a solver manager at the end of its SolverManager::solve() 
     * routine.
     */
    void setSolution(const EigenSolverSolution<ScalarType,MV>& sol) {sol_ = sol;}

    //@}
    
    //! @name Accessor Methods
    //@{ 
    
    //! Get a pointer to the operator for which eigenvalues will be computed.
    SmartPtr<const OP> getOperator() const { return(Op_); };
    
    //! Get a pointer to the operator \c A of the eigenproblem \f$Ax=\lambda Mx\f$.
    SmartPtr<const OP> getA() const { return(AOp_); };
    
    //! Get a pointer to the operator \c M of the eigenproblem \f$Ax=\lambda Mx\f$.
    SmartPtr<const OP> getM() const { return(MOp_); };
    
    //! Get a pointer to the preconditioner of the eigenproblem \f$Ax=\lambda Mx\f$.
    SmartPtr<const OP> getPrec() const { return(prec_); };
    
    //! Get a pointer to the initial vector
    SmartPtr<const MV> getInitVec() const { return (initVec_); };
    
    //! Get a pointer to the auxiliary vector
    SmartPtr<const MV> getAuxVecs() const { return (auxVecs_); };
    
    //! Get the number of eigenvalues (NEV) that are required by this eigenproblem.
    int_t getNEV() const { return (nev_); }

    //! Get the symmetry information for this eigenproblem.
    bool isHermitian() const { return (isSym_); }
    
    //! If the problem has been set, this method will return true.
    bool isProblemSet() const { return (isSet_); }

    /*! \brief Get the solution to the eigenproblem.
     *
     * There is no computation associated with this method. It only provides a 
     * mechanism for associating an EigenSolverSolution with a EigenProblem.
     */
    const EigenSolverSolution<ScalarType,MV>& getSolution() const { return(sol_); }

    //@}
    
  protected:
    
    //! Reference-counted pointer for \c A of the eigenproblem \f$Ax=\lambda Mx\f$
    SmartPtr<const OP> AOp_;

    //! Reference-counted pointer for \c M of the eigenproblem \f$Ax=\lambda Mx\f$
    SmartPtr<const OP> MOp_;

    //! Reference-counted pointer for the operator of the eigenproblem \f$Ax=\lambda Mx\f$
    SmartPtr<const OP> Op_;

    //! Reference-counted pointer for the preconditioner of the eigenproblem \f$Ax=\lambda Mx\f$
    SmartPtr<const OP> prec_;

    //! Reference-counted pointer for the initial vector of the eigenproblem \f$Ax=\lambda Mx\f$
    SmartPtr<MV> initVec_;

    //! Reference-counted pointer for the auxiliary vector of the eigenproblem \f$Ax=\lambda Mx\f$
    SmartPtr<const MV> auxVecs_;

    //! Number of eigenvalues requested
    int_t nev_;

    //! Symmetry of the eigenvalue problem
    /*! \note A generalized eigenvalue problem \f$Ax= \lambda Mx\f$ is considered symmetric
      if the operator \c M is positive (semi) definite.
    */
    bool isSym_;

    //! Sanity Check Flag
    bool isSet_;

    //! Type-definition for the MultiVecTraits class corresponding to the \c MV type
    typedef MultiVecTraits<ScalarType,MV> MVT;
    //! Type-definition for the OperatorTraits class corresponding to the \c OP type
    typedef OperatorTraits<ScalarType,MV,OP> OPT;

    //! Solution to problem
    EigenSolverSolution<ScalarType,MV> sol_;
};


//=============================================================================
//     Implementations (Constructors / Destructors)
//=============================================================================
template <class ScalarType, class MV, class OP>
EigenProblem<ScalarType, MV, OP>::EigenProblem() :
  nev_(0),
  isSym_(false),
  isSet_(false)
{
}


//=============================================================================
template <class ScalarType, class MV, class OP>
EigenProblem<ScalarType, MV, OP>::EigenProblem(const SmartPtr<const OP>& Op, const SmartPtr<MV>& initVec) :
  Op_(Op),
  initVec_(initVec),
  nev_(0),
  isSym_(false),
  isSet_(false)
{
}


//=============================================================================
template <class ScalarType, class MV, class OP>
EigenProblem<ScalarType, MV, OP>::EigenProblem(const SmartPtr<const OP>& Op, const SmartPtr<const OP>& M,
                                                          const SmartPtr<MV>& initVec) :
  MOp_(M),
  Op_(Op),
  initVec_(initVec),
  nev_(0),
  isSym_(false),
  isSet_(false)
{
}


//=============================================================================
template <class ScalarType, class MV, class OP>
EigenProblem<ScalarType, MV, OP>::EigenProblem(const EigenProblem<ScalarType,MV,OP>& Problem) :
  AOp_(Problem.AOp_),
  MOp_(Problem.MOp_),
  Op_(Problem.Op_),
  prec_(Problem.prec_),
  initVec_(Problem.initVec_),
  nev_(Problem.nev_),
  isSym_(Problem.isSym_),
  isSet_(Problem.isSet_),
  sol_(Problem.sol_)
{
}


//=============================================================================
//    SetProblem (sanity check method)
//=============================================================================
template <class ScalarType, class MV, class OP>
bool EigenProblem<ScalarType, MV, OP>::setProblem()
{
  //----------------------------------------------------------------
  // Sanity Checks
  //----------------------------------------------------------------
  // If there is no operator, then we can't proceed.
  if (!getImpl(AOp_) && !getImpl(Op_)) { return false; }

  // If there is no initial vector, then we don't have anything to clone workspace from.
  if (!getImpl(initVec_)) { return false; }

  // If we don't need any eigenvalues, we don't need to continue.
  if (nev_ == 0) { return false; }

  // If there is an A, but no operator, we can set them equal.
  if (getImpl(AOp_) && !getImpl(Op_)) { Op_ = AOp_; }
  
  // Clear the storage from any previous call to setSolution()
  EigenSolverSolution<ScalarType,MV> emptysol;
  sol_ = emptysol;
  
  // mark the problem as set and return no-error
  isSet_=true;
  return true;
}        
  
} // end xlifepp namespace

#endif /* XLIFEPP_EIGENPROBLEM_HPP */
