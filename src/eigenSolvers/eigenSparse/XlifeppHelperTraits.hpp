/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file XlifeppHelperTraits.hpp
  \author Manh Ha NGUYEN
  \since 13 June 2013
  \date 20 August 2013

  \brief Class which processes ritValue and ritVector
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_HELPER_TRAITS_HPP
#define XLIFEPP_HELPER_TRAITS_HPP

#include "utils.h"
#include "XlifeppEigenTypes.hpp"

namespace xlifepp {

/*!  \brief Class which defines basic traits for working with different scalar types.

  An adapter for this traits class must exist for the <tt>ScalarType</tt>.
  If not, this class will produce a compile-time error.  */
template <class ScalarType>
class HelperTraits
{
  public:

    //! Helper function for correctly storing the Ritz values when the eigenproblem is non-Hermitian
    /*! This allows us to use template specialization to compute the right index vector and correctly
     *  handle complex-conjugate pairs.
     */
    static void sortRitzValueEigenSolvers(
        const std::vector<typename NumTraits<ScalarType>::magnitudeType>& rRV,
        const std::vector<typename NumTraits<ScalarType>::magnitudeType>& iRV,
        std::vector<ValueEigenSolver<ScalarType> >* RV, std::vector<int>* RO, std::vector<int>* RI );

    //! Helper function for correctly scaling the eigenvectors of the projected eigenproblem.
    /*! This allows us to use template specialization to compute the right scaling so the
     * Ritz residuals are correct.
     */
    static void scaleRitzVectors(
        const std::vector<typename NumTraits<ScalarType>::magnitudeType>& iRV,
        MatrixEigenDense<ScalarType>* S );

    //! Helper function for correctly computing the Ritz residuals of the projected eigenproblem.
    /*! This allows us to use template specialization to ensure the Ritz residuals are correct.
    */
    static void computeRitzResiduals(
        const std::vector<typename NumTraits<ScalarType>::magnitudeType>& iRV,
        const MatrixEigenDense<ScalarType>& S,
        std::vector<typename NumTraits<ScalarType>::magnitudeType>* RR);

};


template<class ScalarType>
void HelperTraits<ScalarType>::sortRitzValueEigenSolvers(
    const std::vector<typename NumTraits<ScalarType>::magnitudeType>& rRV,
    const std::vector<typename NumTraits<ScalarType>::magnitudeType>& iRV,
    std::vector<ValueEigenSolver<ScalarType> >* RV, std::vector<int>* RO, std::vector<int>* RI )
{
  typedef typename NumTraits<ScalarType>::magnitudeType MagnitudeType;
  MagnitudeType MT_ZERO = NumTraits<MagnitudeType>::zero();

  int curDim = (int)rRV.size();
  int i = 0;

  // Clear the current index.
  RI->clear();

  // Place the Ritz values from rRV and iRV into the RV container.
  while( i < curDim ) {
    if ( iRV[i] != MT_ZERO ) {
      //
      // We will have this situation for real-valued, non-Hermitian matrices.
      (*RV)[i].set(rRV[i], iRV[i]);
      (*RV)[i+1].set(rRV[i+1], iRV[i+1]);

      // Make sure that complex conjugate pairs have their positive imaginary part first.
      if ( (*RV)[i].imagpart < MT_ZERO ) {
        // The negative imaginary part is first, so swap the order of the ritzValueEigenSolvers and ritzOrders.
        xlifepp::ValueEigenSolver<ScalarType> tmp_ritz( (*RV)[i] );
        (*RV)[i] = (*RV)[i+1];
        (*RV)[i+1] = tmp_ritz;

        int tmp_order = (*RO)[i];
        (*RO)[i] = (*RO)[i+1];
        (*RO)[i+1] = tmp_order;

      }
      RI->push_back(1); RI->push_back(-1);
      i = i+2;
    } else {
      //
      // The Ritz value is not complex.
      (*RV)[i].set(rRV[i], MT_ZERO);
      RI->push_back(0);
      i++;
    }
  }
}


template<class ScalarType>
void HelperTraits<ScalarType>::scaleRitzVectors(
    const std::vector<typename NumTraits<ScalarType>::magnitudeType>& iRV,
    MatrixEigenDense<ScalarType>* S )
{
  ScalarType ST_ONE = NumTraits<ScalarType>::one();

  typedef typename NumTraits<ScalarType>::magnitudeType MagnitudeType;
  MagnitudeType MT_ZERO = NumTraits<MagnitudeType>::zero();

  int i = 0, curDim = S->numOfRows();
  ScalarType temp;
  ScalarType* s_ptr = S->values();
  while( i < curDim ) {
    if ( iRV[i] != MT_ZERO ) {
//                temp = lapack_mag.LAPY2( blas.NRM2( curDim, s_ptr+i*curDim, 1 ),
//                        blas.NRM2( curDim, s_ptr+(i+1)*curDim, 1 ) );
      temp = NumTraits<MagnitudeType>::lapy2(S->columnVector(i).norm2(), S->columnVector(i+1).norm2());
//                blas.SCAL( curDim, ST_ONE/temp, s_ptr+i*curDim, 1 );
//                blas.SCAL( curDim, ST_ONE/temp, s_ptr+(i+1)*curDim, 1 );
      for (int j = 0; j < curDim; j++) {
        S->coeffRef(j,i) *= ST_ONE/temp;
        S->coeffRef(j,i+1) *= ST_ONE/temp;
      }
      i = i+2;
    } else {
      //temp = blas.NRM2( curDim, s_ptr+i*curDim, 1 );
      temp = S->columnVector(i).norm2();
      //blas.SCAL( curDim, ST_ONE/temp, s_ptr+i*curDim, 1 );
      for (int j = 0; j < curDim; j++) {
        S->coeffRef(j,i) *= ST_ONE/temp;
      }
      i++;
    }
  }
}

template<class ScalarType>
void HelperTraits<ScalarType>::computeRitzResiduals(
    const std::vector<typename NumTraits<ScalarType>::magnitudeType>& iRV,
    const MatrixEigenDense<ScalarType>& S,
    std::vector<typename NumTraits<ScalarType>::magnitudeType>* RR )
{
  typedef typename NumTraits<ScalarType>::magnitudeType MagnitudeType;
  MagnitudeType MT_ZERO = NumTraits<MagnitudeType>::zero();

  int i = 0;
  // int s_stride = S.stride();
  // ScalarType* s_ptr = S.values();
  int s_cols = S.numOfCols();


  while( i < s_cols ) {
    if ( iRV[i] != MT_ZERO ) {
//                (*RR)[i] = lapack_mag.LAPY2( blas.NRM2(s_rows, s_ptr + i*s_stride, 1),
//                                             blas.NRM2(s_rows, s_ptr + (i+1)*s_stride, 1) );
      (*RR)[i] = NumTraits<MagnitudeType>::lapy2(S.columnVector(i).norm2(), S.columnVector(i+1).norm2());
      (*RR)[i+1] = (*RR)[i];
      i = i+2;
    } else {
      // (*RR)[i] = blas.NRM2(s_rows, s_ptr + i*s_stride, 1);
      (*RR)[i] = S.columnVector(i).norm2();
      i++;
    }
  }
}

// Partial template specializations for the complex scalar type.

#ifndef DOXYGEN_SHOULD_SKIP_THIS

/*!  \brief Class which defines basic traits for working with different scalar types.

  An adapter for this traits class must exist for the <tt>ScalarType</tt>.
  If not, this class will produce a compile-time error.   */
template<typename T>
class HelperTraits<std::complex<T> >
{
  public:
    static void sortRitzValueEigenSolvers(
      const std::vector<real_t>& rRV,
      const std::vector<real_t>& iRV,
      std::vector<ValueEigenSolver<std::complex<T> > >* RV,
      std::vector<int>* RO, std::vector<int>* RI );

    static void scaleRitzVectors(
      const std::vector<real_t>& iRV,
      MatrixEigenDense<std::complex<T> >* S );

    static void computeRitzResiduals(
      const std::vector<real_t>& iRV,
      const MatrixEigenDense<std::complex<T> >& S,
      std::vector<real_t>* RR );
};

template<typename T>
void HelperTraits<std::complex<T> >::sortRitzValueEigenSolvers(
    const std::vector<real_t>& rRV,
    const std::vector<real_t>& iRV,
    std::vector<ValueEigenSolver<std::complex<T> > >* RV,
    std::vector<int>* RO, std::vector<int>* RI )
{
  (void)RO;
  int curDim = (int)rRV.size();
  int i = 0;

  // Clear the current index.
  RI->clear();

  // Place the Ritz values from rRV and iRV into the RV container.
  while( i < curDim ) {
    (*RV)[i].set(rRV[i], iRV[i]);
    //RI->push_back(0);
    RI->push_back(2);   // change by Eric
    i++;
  }
}

template<typename T>
void HelperTraits<std::complex<T> >::scaleRitzVectors(
    const std::vector<real_t>& iRV,
    MatrixEigenDense<std::complex<T> >* S )
{
  (void)iRV;
  typedef complex_t ST;
  ST ST_ONE = NumTraits<ST>::one();

  //Teuchos::BLAS<int,ST> blas;

  int i = 0, curDim = S->numOfRows();
  real_t temp;
  //ST* s_ptr = S->values();
  while( i < curDim ) {
//          temp = blas.NRM2( curDim, s_ptr+i*curDim, 1 );
//          blas.SCAL( curDim, ST_ONE/temp, s_ptr+i*curDim, 1 );
    temp = S->columnVector(i).norm2();
    for (int j = 0; j < curDim; j++) {
      S->coeffRef(j,i) *= ST_ONE/temp;
    }
  i++;
  }
}

template<typename T>
void HelperTraits<std::complex<T> >::computeRitzResiduals(
    const std::vector<real_t>& iRV,
    const MatrixEigenDense<std::complex<T> >& S,
    std::vector<real_t>* RR )
{
  (void)iRV;

  //int s_stride = S.stride();
  //int s_rows = S.numOfRows();
  int s_cols = S.numOfCols();
  //ANSZI_CPLX_CLASS<T>* s_ptr = S.values();

  for (int i=0; i<s_cols; ++i ) {
    //(*RR)[i] = blas.NRM2(s_rows, s_ptr + i*s_stride, 1);
    (*RR)[i] = S.columnVector(i).norm2();
  }
}

#endif

} // end xlifepp namespace

#endif /* XLIFEPP_HELPER_TRAITS_HPP */
