/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppMultiVec.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013

  \brief Interface for multivectors used by xlifepp' linear solvers.

  We provide two options for letting xlifepp' linear solvers use
  arbitrary multivector types.  One is via compile-time
  polymorphism, by specializing xlifepp::MultiVecTraits.  The other
  is via run-time polymorphism, by implementing MultiVec
  (the interface defined in this header file).  xlifepp ultimately
  only uses xlifepp::MultiVecTraits (it uses MultiVec via a
  specialization of xlifepp::MultiVecTraits for xlifepp::MultiVec),
  so the preferred way to tell xlifepp how to use your multivector
  class is via an xlifepp::MultiVecTraits specialization.  However,
  some users find a run-time polymorphic interface useful, so we
  provide it as a service to them.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************

#ifndef XLIFEPP_MULTI_VEC_HPP
#define XLIFEPP_MULTI_VEC_HPP


#include "XlifeppMultiVecTraits.hpp"

namespace xlifepp {

/*!
  \class MultiVec
 \brief Interface for multivectors used by xlifepp's linear solvers.
 \tparam ScalarType The type of entries of the multivector.

 xlifepp accesses multivectors through a traits interface called
 MultiVecTraits.  If you want to use xlifepp with your own
 multivector class MV, you may either specialize MultiVecTraits for
 MV, or you may wrap MV in your own class that implements MultiVec.
 Specializing MultiVecTraits works via compile-time polymorphism,
 whereas implementing the MultiVec interface works via run-time
 polymorphism.  You may pick whichever option you like.  However,
 specializing MultiVecTraits is the preferred method.  This is
 because xlifepp's linear solvers always use a specialization of
 MultiVecTraits to access multivector operations.  They only use
 MultiVec through a specialization of the MultiVecTraits traits
 class, which is implemented below in this header file.

 If you want your multivector class (or a wrapper thereof) to
 implement the MultiVec interface, you should inherit from
 MultiVec<ScalarType>, where ScalarType is the type of entries in
 the multivector.  For example, a multivector with entries of type
 double would inherit from MultiVec<double>.
 */
template <class ScalarType>
class MultiVec {
public:
  //! @name Constructor/Destructor
  //@{
  //! Default constructor.
  MultiVec() {}
  //! Destructor (virtual for memory safety of derived classes).
  virtual ~MultiVec () {}
  //@}

  //! @name Creation methods
  //@{ 
  //! \brief Create a new MultiVec with \c numvecs columns.
  //! \return Pointer to the new multivector with uninitialized values.
  virtual MultiVec<ScalarType>* clone (const dimen_t numvecs) const = 0;

  //! \brief Create a new MultiVec and copy contents of \c *this into it (deep copy).
  //! \return Pointer to the new multivector	
  virtual MultiVec<ScalarType>* cloneCopy () const = 0;
	
  /*! \brief Creates a new MultiVec and copies the selected contents of \c *this
    into the new vector (deep copy).  The copied 
    vectors from \c *this are indicated by the \c index.size() indices in \c index.

    \return Pointer to the new multivector	
  */
  virtual MultiVec<ScalarType>* cloneCopy (const std::vector<int>& index) const = 0;

  /*! \brief Creates a new MultiVec that shares the selected contents of \c *this.
    The index of the \c numvecs vectors shallow copied from \c *this are indicated by the
    indices given in \c index.

    \return Pointer to the new multivector	
  */
  virtual MultiVec<ScalarType>* cloneViewNonConst(const std::vector<int>& index) = 0;
	
  /*! \brief Creates a new MultiVec that shares the selected contents of \c *this.
    The index of the \c numvecs vectors shallow copied from \c *this are indicated by the
    indices given in \c index.

    \return Pointer to the new multivector	
  */
  virtual const MultiVec<ScalarType>* cloneView (const std::vector<int>& index) const = 0;
  //@}

  //! @name Dimension information methods	
  //@{ 
  //! The number of rows in the multivector.
  virtual number_t getVecLength () const = 0;
  //! The number of vectors (i.e., columns) in the multivector.
  virtual dimen_t getNumberVecs () const = 0;
  //@}

  //! @name Update methods
  //@{ 
  //! Update \c *this with \c alpha * \c A * \c B + \c beta * (\c *this).
  virtual void 
  mvTimesMatAddMv (ScalarType alpha,
		   const MultiVec<ScalarType>& A, 
		   const MatrixEigenDense<ScalarType>& B, ScalarType beta) = 0;

  //! Replace \c *this with \c alpha * \c A + \c beta * \c B.
  virtual void mvAddMv (ScalarType alpha, const MultiVec<ScalarType>& A, ScalarType beta, const MultiVec<ScalarType>& B) = 0;

  //! Scale each element of the vectors in \c *this with \c alpha.
  virtual void mvScale (ScalarType alpha) = 0;

  //! Scale each element of the <tt>i</tt>-th vector in \c *this with <tt>alpha[i]</tt>.
  virtual void mvScale (const std::vector<ScalarType>& alpha) = 0;

  /*! \brief Compute a dense matrix \c B through the matrix-matrix multiply 
    \c alpha * \c A^T * (\c *this).
  */
  virtual void mvTransMv (ScalarType alpha, const MultiVec<ScalarType>& A, MatrixEigenDense<ScalarType>& B) const = 0;

  /*! \brief Compute the dot product of each column of *this with the corresponding column of A.
     
      Compute a vector \c b whose entries are the individual
      dot-products.  That is, <tt>b[i] = A[i]^H * (*this)[i]</tt>
      where <tt>A[i]</tt> is the i-th column of A.
  */
  virtual void mvDot (const MultiVec<ScalarType>& A, std::vector<ScalarType>& b) const = 0;
  //@}
  
  //! @name Norm method
  //@{ 
  /*! \brief Compute the 2-norm of each vector in \c *this.  
     
      \param normvec [out] On output, normvec[i] holds the 2-norm of the
        \c i-th vector of \c *this.
  */
  virtual void mvNorm (std::vector<typename NumTraits<ScalarType>::RealScalar>& normvec) const = 0;
  //@}

  //! @name Initialization methods
  //@{ 
  /*! \brief Copy the vectors in \c A to a set of vectors in \c *this.  
     
      The \c numvecs vectors in \c A are copied to a subset of vectors
      in \c *this indicated by the indices given in \c index.
  */
  virtual void setBlock (const MultiVec<ScalarType>& A, const std::vector<int>& index) = 0;
	
  //! Fill all the vectors in \c *this with random numbers.
  virtual void mvRandom () = 0;

  //! Replace each element of the vectors in \c *this with \c alpha.
  virtual void mvInit (ScalarType alpha) = 0;
  //@}

  //! @name Print method
  //@{ 
  //! Print \c *this multivector to the \c os output stream.
  virtual void mvPrint (std::ostream& os) const = 0;
  //@}

  virtual std::vector<ScalarType>* operator[](int v) = 0;

  virtual std::vector<ScalarType>* operator[](int v) const = 0;

  virtual ScalarType& operator()(const int i, const int j) = 0;

  virtual const ScalarType& operator()(const int i, const int j) const = 0;

  virtual void mvLoadFromFile(const char* f) = 0;
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

/* \brief Specialization of MultiVecTraits for Belos::MultiVec.
   
    xlifepp interfaces to every multivector implementation through a
    specialization of MultiVecTraits.  Thus, we provide a
    specialization of MultiVecTraits for the MultiVec run-time
    polymorphic interface above.
   
    \tparam ScalarType The type of entries in the multivector; the
      template parameter of MultiVec.
*/
template<class ScalarType>
class MultiVecTraits<ScalarType, MultiVec<ScalarType> > {
  public:
    //! @name Creation methods
    //@{
    //! \brief Create a new empty \c MultiVec containing \c numvecs columns.
    //! \return Reference-counted pointer to the new \c MultiVec.
    static SmartPtr<MultiVec<ScalarType> >
    clone (const MultiVec<ScalarType>& mv, const int numvecs) {
      return SmartPtr<MultiVec<ScalarType> >(const_cast<MultiVec<ScalarType>&> (mv).clone (numvecs));
    }

    /*! \brief Creates a new \c MultiVec and copies contents of \c mv into the new vector (deep copy).

      \return Reference-counted pointer to the new \c MultiVec.
    */
    static SmartPtr<MultiVec<ScalarType> > cloneCopy(const MultiVec<ScalarType>& mv)
    { return SmartPtr<MultiVec<ScalarType> >(const_cast<MultiVec<ScalarType>&>(mv).cloneCopy()); }

    /*! \brief Creates a new \c MultiVec and copies the selected contents of \c mv into the new vector (deep copy).

      The copied vectors from \c mv are indicated by the \c index.size() indices in \c index.
      \return Reference-counted pointer to the new \c MultiVec.
    */
    static SmartPtr<MultiVec<ScalarType> > cloneCopy(const MultiVec<ScalarType>& mv, const std::vector<int>& index)
    { return SmartPtr<MultiVec<ScalarType> >(const_cast<MultiVec<ScalarType>&>(mv).cloneCopy(index)); }

    /*! \brief Creates a new \c MultiVec that shares the selected contents of \c mv (shallow copy).

    The index of the \c numvecs vectors shallow copied from \c mv are indicated by the indices given in \c index.
    \return Reference-counted pointer to the new \c MultiVec.
    */
    static SmartPtr<MultiVec<ScalarType> > cloneViewNonConst(MultiVec<ScalarType>& mv, const std::vector<int>& index)
    { return SmartPtr<MultiVec<ScalarType> >(mv.cloneViewNonConst(index)); }

    /*! \brief Creates a new const \c MultiVec that shares the selected contents of \c mv (shallow copy).

    The index of the \c numvecs vectors shallow copied from \c mv are indicated by the indices given in \c index.
    \return Reference-counted pointer to the new const \c MultiVec.
    */
    static SmartPtr<const MultiVec<ScalarType> > cloneView(const MultiVec<ScalarType>& mv, const std::vector<int>& index)
    { return SmartPtr<const MultiVec<ScalarType> >(const_cast<MultiVec<ScalarType>&>(mv).cloneView(index)); }
    //@}

    //! @name Attribute methods
    //@{
    //! Obtain the vector length of \c mv.
    static int getVecLength(const MultiVec<ScalarType>& mv)
    { return mv.getVecLength(); }

    //! Obtain the number of vectors in \c mv
    static int getNumberVecs(const MultiVec<ScalarType>& mv)
    { return mv.getNumberVecs(); }
    //@}

    //! @name Update methods
    //@{
    /*! \brief Update \c mv with \f$ \alpha AB + \beta mv \f$.
     */
    static void mvTimesMatAddMv(ScalarType alpha, const MultiVec<ScalarType>& A,
				 const MatrixEigenDense<ScalarType>& B,
				 ScalarType beta, MultiVec<ScalarType>& mv)
    { mv.mvTimesMatAddMv(alpha, A, B, beta); }

    /*! \brief Replace \c mv with \f$\alpha A + \beta B\f$.
     */
    static void mvAddMv(ScalarType alpha, const MultiVec<ScalarType>& A, ScalarType beta, const MultiVec<ScalarType>& B, MultiVec<ScalarType>& mv)
    { mv.mvAddMv(alpha, A, beta, B); }

    /*! \brief Compute a dense matrix \c B through the matrix-matrix multiply \f$ \alpha A^Tmv \f$.
     */
    static void mvTransMv(ScalarType alpha, const MultiVec<ScalarType>& A, const MultiVec<ScalarType>& mv, MatrixEigenDense<ScalarType>& B)
    { mv.mvTransMv(alpha, A, B); }

    /*! \brief Compute a vector \c b where the components are the individual dot-products of the \c i-th columns of \c A and \c mv, i.e.\f$b[i] = A[i]^H mv[i]\f$.
     */
    static void mvDot(const MultiVec<ScalarType>& mv, const MultiVec<ScalarType>& A, std::vector<ScalarType> & b)
    { mv.mvDot(A, b); }

    //! Scale each element of the vectors in \c *this with \c alpha.
    static void mvScale (MultiVec<ScalarType>& mv, ScalarType alpha)
    { mv.mvScale(alpha); }

    //! Scale each element of the \c i-th vector in \c *this with \c alpha[i].
    static void mvScale (MultiVec<ScalarType>& mv, const std::vector<ScalarType>& alpha)
    { mv.mvScale(alpha); }
    //@}

    //! @name Norm method
    //@{
    /*! \brief Compute the 2-norm of each individual vector of \c mv.
      Upon return, \c normvec[i] holds the value of \f$||mv_i||_2\f$, the \c i-th column of \c mv.
    */
    static void mvNorm(const MultiVec<ScalarType>& mv, std::vector<typename NumTraits<ScalarType>::RealScalar> & normvec)
    { mv.mvNorm(normvec); }
    //@}

    //! @name Initialization methods
    //@{
    /*! \brief Copy the vectors in \c A to a set of vectors in \c mv indicated by the indices given in \c index.

    The \c numvecs vectors in \c A are copied to a subset of vectors in \c mv indicated by the indices given in \c index,
    i.e.<tt> mv[index[i]] = A[i]</tt>.
    */
    static void setBlock(const MultiVec<ScalarType>& A, const std::vector<int>& index, MultiVec<ScalarType>& mv)
    { mv.setBlock(A, index); }

    /*! \brief Replace the vectors in \c mv with random vectors.
     */
    static void mvRandom(MultiVec<ScalarType>& mv)
    { mv.mvRandom(); }

    /*! \brief Replace each element of the vectors in \c mv with \c alpha.
     */
    static void mvInit(MultiVec<ScalarType>& mv, ScalarType alpha = NumTraits<ScalarType>::zero())
    { mv.mvInit(alpha); }
    //@}

    //! @name Print method
    //@{
    //! Print the \c mv multi-vector to the \c os output stream.
    static void mvPrint(const MultiVec<ScalarType>& mv, std::ostream& os)
    { mv.mvPrint(os); }
    //@}
};

#endif

} // end of namespace xlifepp

#endif /* XLIFEPP_MULTI_VEC_HPP */
