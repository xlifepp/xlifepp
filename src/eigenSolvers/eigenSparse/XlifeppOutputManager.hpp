/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppOutputManager.hpp
  \author Manh Ha NGUYEN
  \since 05 April 2013
  \date  12 June 2013
 
  \brief Abstract class definition for xlifepp Output Managers.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************

#ifndef XLIFEPP_OUTPUT_MANAGER_HPP
#define XLIFEPP_OUTPUT_MANAGER_HPP

#include "config.h"
#include "XlifeppEigenTypes.hpp"


namespace xlifepp {

/*!  \class OutputManager

  \brief Output managers remove the need for the eigensolver to know any information
  about the required output.  Calling isVerbosity(MsgEigenType type) informs the solver if
  it is supposed to output the information corresponding to the message type.
*/
template <class ScalarType>
class OutputManager {

public:

  //!@name Constructors/Destructor 
  //@{ 

  //! Default constructor
  OutputManager(int vb = 0) : vb_(vb) {}

  //! Destructor.
  virtual ~OutputManager() {}
  //@}
  
  //! @name Set/Get methods
  //@{ 

  //! Set the message output types for this manager.
  virtual void setVerbosity(int vb) { vb_ = vb; }

  //! Get the message output types for this manager.
  virtual int getVerbosity() const { return vb_; }

  //@}

  //! @name Output methods
  //@{ 

  //! Find out whether we need to print out information for this message type.
  /*! This method is used by the solver to determine whether computations are
      necessary for this message type.
  */
  virtual bool isVerbosity(MsgEigenType type) const = 0;

  //! Send output to the output manager.
  virtual void print(MsgEigenType type, const std::string output) = 0;

  //! Create a stream for outputting to.
  virtual std::ostream& stream(MsgEigenType type) = 0;

  //@}

  private:

  //! @name Undefined methods
  //@{ 

  //! Copy constructor.
  OutputManager(const OutputManager<ScalarType>& OM);

  //! Assignment operator.
  OutputManager<ScalarType>& operator=(const OutputManager<ScalarType>& OM);

  //@}

  protected:
  int vb_;
};

} // end of namespace xlifepp

#endif /* XLIFEPP_OUTPUT_MANAGER_HPP*/
