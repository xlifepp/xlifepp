/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file XlifeppEigenTypes.hpp
  \author Manh Ha NGUYEN
  \since 26 Mars 2013
  \date  26 Mars 2013

  \brief Types and exceptions used within xlifepp solvers and interfaces.
*/

// This file is adapted from Anasazi, an extensible and interoperable framework
// for large-scale eigenvalue algorithms
// ***********************************************************************
//
//                 Block Eigensolvers Package
//                 Copyright (2004) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA
//
// ***********************************************************************


#ifndef XLIFEPP_EIGEN_TYPES_HPP
#define XLIFEPP_EIGEN_TYPES_HPP

#include "../eigenCore/eigenCore.hpp"

namespace xlifepp {

//! @name xlifepp Structs
//@{

//!  This struct is used for storing eigenvalues and Ritz values, as a pair of real values.
template <class ScalarType>
struct ValueEigenSolver {
  //! The real component of the eigenvalue.
  typename NumTraits<ScalarType>::RealScalar realpart;
  //! The imaginary component of the eigenvalue.
  typename NumTraits<ScalarType>::RealScalar imagpart;
  void set(const typename NumTraits<ScalarType>::RealScalar& rp, const typename NumTraits<ScalarType>::RealScalar& ip){
    realpart=rp;imagpart=ip;
  }
  ValueEigenSolver<ScalarType>& operator=(const ValueEigenSolver<ScalarType>& rhs) {
    realpart=rhs.realpart;imagpart=rhs.imagpart;
    return *this;
  }
};

//!  Struct for storing an eigenproblem solution.
template <class ScalarType, class MV>
struct EigenSolverSolution {
  //! The computed eigenvectors
  SmartPtr<MV> evecs;
  //! An orthonormal basis for the computed eigenspace
  SmartPtr<MV> espace;
  //! The computed eigenvalues
  std::vector<ValueEigenSolver<ScalarType> >  evals;
  /*! \brief An index into evecs to allow compressed storage of eigenvectors for real, non-Hermitian problems.
   *
   *  index has length numVecs, where each entry is 0, +1, or -1. These have the following interpretation:
   *     - index[i] == 0: signifies that the corresponding eigenvector is stored as the i column of evecs. This will usually be the
   *       case when ScalarType is complex, an eigenproblem is Hermitian, or a real, non-Hermitian eigenproblem has a real eigenvector.
   *     - index[i] == +1: signifies that the corresponding eigenvector is stored in two vectors: the real part in the i column of evecs and the <i><b>positive</b></i> imaginary part in the i+1 column of evecs.
   *     - index[i] == -1: signifies that the corresponding eigenvector is stored in two vectors: the real part in the i-1 column of evecs and the <i><b>negative</b></i> imaginary part in the i column of evecs
   */
  std::vector<int>         index;
  //! The number of computed eigenpairs
  int numVecs;
  
  EigenSolverSolution() : evecs(),espace(),evals(0),index(0),numVecs(0) {}
};
//@}

} // end of namespace xlifepp

#endif /*XLIFEPP_EIGEN_TYPES_HPP */
