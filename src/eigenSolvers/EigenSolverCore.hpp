/*
XLiFE++ is an extended library of finite elements written in C++
    Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file EigenSolverCore.hpp
  \author Manh Ha NGUYEN
  \since 27 Mars 2013
  \date  27 June 2016

  \brief Header file to eigen solver intern of Xlife++
*/

#ifndef EIGENSOLVER_CORE_HPP
#define EIGENSOLVER_CORE_HPP

#include "eigenCore/decomposition/Tridiagonalization.hpp"
#include "eigenCore/decomposition/HessenbergDecomposition.hpp"
#include "eigenCore/decomposition/RealSchur.hpp"
#include "eigenCore/decomposition/ComplexSchur.hpp"
#include "eigenCore/decomposition/HouseholderQR.hpp"

#include "eigenCore/utils/VectorEigenDense.hpp"
#include "eigenCore/utils/MatrixEigenDense.hpp"
#include "eigenCore/SelfAdjointEigenSolver.hpp"
#include "eigenCore/GeneralizedSelfAdjointEigenSolver.hpp"
#include "eigenCore/RealEigenSolver.hpp"
#include "eigenCore/ComplexEigenSolver.hpp"
#include "eigenCore/smartPointer/SmartPtr.hpp"

#endif

