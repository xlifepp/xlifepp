/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LinearForm.cpp
  \author E. Lunéville
  \since 23 mar 2012
  \date  02 apr 2012

  \brief Implementation of xlifepp::LinearForm classes member functions and related utilities
*/

#include "LinearForm.hpp"
#include "utils.h"

namespace xlifepp
{
//-------------------------------------------------------------------------------
//member functions of BasicLinearForm class
//-------------------------------------------------------------------------------

//downcast
IntgLinearForm* BasicLinearForm::asIntgForm()
{
  if (type()!=_intg) error("downcast_failure","IntgLinearForm");
  return static_cast<IntgLinearForm*>(this);
}

const IntgLinearForm* BasicLinearForm::asIntgForm() const
{
  if (type()!=_intg) error("downcast_failure","IntgLinearForm");
  return static_cast<const IntgLinearForm*>(this);
}

DoubleIntgLinearForm* BasicLinearForm::asDoubleIntgForm()
{
  if (type()!=_doubleIntg) error("downcast_failure","DoubleIntgLinearForm");
  return static_cast<DoubleIntgLinearForm*>(this);
}

const DoubleIntgLinearForm* BasicLinearForm::asDoubleIntgForm() const
{
  if (type()!=_doubleIntg) error("downcast_failure","DoubleIntgLinearForm");
  return static_cast<const DoubleIntgLinearForm*>(this);
}

BilinearFormAsLinearForm* BasicLinearForm::asBilinearAsLinearForm()
{
  if (type()!=_bilinearAsLinear) error("downcast_failure","BilinearAsLinearForm");
  return reinterpret_cast<BilinearFormAsLinearForm*>(this);
}

const BilinearFormAsLinearForm* BasicLinearForm::asBilinearAsLinearForm() const
{
  if (type()!=_bilinearAsLinear) error("downcast_failure","BilinearAsLinearForm");
  return reinterpret_cast<const BilinearFormAsLinearForm*>(this);
}

//-------------------------------------------------------------------------------
//member functions of IntgLinearForm class
//-------------------------------------------------------------------------------
//constructors
IntgLinearForm::IntgLinearForm(const GeomDomain& dom, const OperatorOnUnknown& opu, const IntegrationMethods& ims)
{
  opu_p = &opu;
  u_p = opu.unknown();
  domain_p = &dom;
  intgMethod_p=nullptr;
  intgMethods = ims;
  setIntegrationMethods();
  setComputationType();
}

IntgLinearForm::IntgLinearForm(const GeomDomain& dom, const OperatorOnUnknown& opu, const IntegrationMethod& im)
{
  if (!im.isSingleIM())
  {
    where("LinearForm::LinearForm");
    error("im_not_single");
  }
  opu_p = &opu;
  u_p = opu.unknown();
  domain_p = &dom;
  intgMethod_p=&im;  //pointer copy !!!
  setComputationType();
}

IntgLinearForm::IntgLinearForm(const GeomDomain& dom, const OperatorOnUnknown& opu, QuadRule qr, number_t qo)
{
  if (dom.domType() != _meshDomain)
  {
    where("IntgLinearForm::IntgLinearForm");
    error("domain_notmesh",dom.name(), words("domain type",dom.domType()));
  }
  opu_p = &opu;
  u_p = opu.unknown();
  domain_p = &dom;
  intgMethod_p=nullptr;
  //create quadrature from qr, qo and shapetypes
  const MeshDomain* mdom=dom.meshDomain();
  number_t ord = opu.degree(); //degree of polynoms to be integrated
  number_t mord = mdom->order();
  if (mord>1) ord+=mord; // increase order when curved element
  if (ord == 0) ord = 1;
  if (qr != _defaultRule) ord=std::max(ord,qo);  //keep user choice if enough
  intgMethod_p=static_cast<IntegrationMethod*>(new QuadratureIM(mdom->shapeTypes,qr,ord));
  setComputationType();
}

//clone of the linear form
BasicLinearForm* IntgLinearForm::clone() const
{
  return new IntgLinearForm(*this);
}

//set integration methods
void IntgLinearForm::setIntegrationMethods()
{
  const MeshDomain* mdom = domain_p->meshDomain();
  number_t ord = opu_p->degree(), ordu = opu_p->unknownDegree(); //degree of polynoms to be integrated
  if (opu_p->hasFunction()) ord += ordu; // operator involves user functions, the order is increased by degree of shape functions
  number_t mord = mdom->order();
  if (mord>1) ord+=mord; // increase order when curved element
  if (ord == 0) ord = 1;
  if (ord < 2 && opu_p->hasFunction()) ord=2; // at least order 2 if there is a function
  if (intgMethods.size()==0) //no method, define default one
  {
    intgMethods = IntegrationMethods(static_cast<IntegrationMethod&>(*new QuadratureIM(mdom->shapeTypes, _defaultRule, ord)),_allFunction, theRealMax);
    return;
  }
  //check that intg methods are single intg method and find best quad rule if required
  std::vector<IntgMeth>::iterator iti=intgMethods.begin();
  for (; iti!=intgMethods.end(); ++iti)  //loop on integration methods
  {
    const IntegrationMethod* im =iti->intgMeth;
    if (!im->isSingleIM())
    {
      where("IntgLinearForm::setIntegrationMethods()");
      error("im_not_single");
    }
    IntegrationMethodType imt=im->imType;
    if (imt==_quadratureIM) //check if default rule
    {
      const QuadratureIM* qim = static_cast<const QuadratureIM*>(im);
      if (qim->quadRule==_defaultRule)
      {
        if (qim->degree==0) iti->intgMeth = new QuadratureIM(mdom->shapeTypes, _defaultRule, ord); //find best quadrules at degree ord
        else iti->intgMeth = new QuadratureIM(mdom->shapeTypes, _defaultRule, qim->degree);       //find best quadrules at degree qim->degree
      }
      else qim->setQuadratures(mdom->shapeTypes);
    }
    else if (imt==_LenoirSalles2dIR || imt==_LenoirSalles3dIR)  //Lenoir-Salles method, check if method is designed for integrand
    {
      dimen_t d=mdom->dim();
      if ((imt==_LenoirSalles2dIR && d!=1) || (imt==_LenoirSalles3dIR && d!=2)) //check dimension
      {
        where("IntgLinearForm::setIntegrationMethods()");
        error("im_not_handled",words("integration", imt));
      }
      const Interpolation* intp = opu_p->unknown()->space()->interpolation();
      if (intp->type!=_Lagrange) // restricted to P0/P1 interpolation
      {
        where("IntgLinearForm::setIntegrationMethods()");
        error("refelt_not_lagrange");
      }
      if (intp->numtype>1) // restricted to P0/P1 interpolation
      {
        where("IntgLinearForm::setIntegrationMethods()");
        error("quadrature_order_not_handled",intp->numtype);
      }
      if (!opu_p->hasKernel()) //check kernel
      {
        where("IntgLinearForm::setIntegrationMethods()");
        error("im_not_double");
      }
      const Kernel* ker = opu_p->kernelp();
      if (iti->functionPart==_singularPart) ker = ker->singPart;
      if (iti->functionPart==_regularPart) ker = ker->regPart;
      if (ker->shortname!="Lap3D" && ker->shortname!="Lap2D")
      {
        where("IntgLinearForm::setIntegrationMethods()");
        error("ker_bad_family","Laplace");
      }
      const OperatorOnKernel* opker=opu_p->opkernelp();
      if (!opker->isId() && opker->ydifOp().type()!=_ndotgrad_y && opker->xdifOp().type()!=_grad_x )
      {
        where("IntgLinearForm::setIntegrationMethods()");
        error("opker_not_supported","opker = "+opker->asString(),im->name);
      }
    }
    else
    {
      where("IntgLinearForm::setIntegrationMethods()");
      error("im_not_handled",words("integration", im->type()));
    }
  }
}

//set the computation type
void IntgLinearForm::setComputationType()
{
  if (compuType!=_undefComputation) return; // compuType set by user
  compuType = _FEComputation;
  if (domain_p->meshDomain()!=nullptr && domain_p->meshDomain()->isSideDomain())
  {
    //no tangent derivatives or unknown space with no trace on boundary
    if (opu_p->extensionRequired()) compuType = _FEextComputation;
  }
  //warning: boundary terms involving non tangential derivatives with spectral unknown are not handled for the moment
  if (u_p->isSpectral()) compuType = _SPComputation;
}

//check if normal is required by one of integration methods related to the intg linear form
bool IntgLinearForm::intgMethodsRequireNormal() const
{
  if (intgMethod_p!=nullptr && intgMethod_p->requireNormal) return true;
  for (std::vector<IntgMeth>::const_iterator itm=intgMethods.begin(); itm!=intgMethods.end(); ++itm)
  {
    if (itm->intgMeth->requireNormal) return true;
  }
  return false;
}

//print stuff
void IntgLinearForm::print(std::ostream& os) const
{
  if (theVerboseLevel == 0) return;
  os << message("form_intginfo", words("value", valueType()), domain_p->name(), u_p->name());
  os <<": "<< asString();
  os<<", "<<words("computation type",compuType);
  if (isoGeometric) os<<", iso-geometric";
  if (theVerboseLevel < 2) return;
  if (intgMethod_p != nullptr) os <<", "<< *intgMethod_p;
  if (!intgMethods.empty())
  {
    if (intgMethods.intgMethods.size()==1) os <<", "<< intgMethods;
    else os <<eol<<"   Quadratures: "<< intgMethods;
  }
  os << message("form_info2") << "\n   " << *opu_p;
  if (intgMethod_p!=nullptr) os<<"   using "<<*intgMethod_p;
  os<<eol;
}


//-------------------------------------------------------------------------------
//member functions of DoubleIntgLinearForm class
//-------------------------------------------------------------------------------
//constructors
DoubleIntgLinearForm::DoubleIntgLinearForm(const GeomDomain& domx, const GeomDomain& domy, const OperatorOnUnknown& opu, QuadRule qr, number_t qo)
{
  opu_p = &opu;
  u_p = opu.unknown();
  domainx_p = &domx;
  domainy_p = &domy;
  //setQuadrature(); to be updated
}

//clone of the linear form
BasicLinearForm* DoubleIntgLinearForm::clone() const
{
  return new DoubleIntgLinearForm(*this);
}

void DoubleIntgLinearForm::print(std::ostream& os) const
{
  if (theVerboseLevel > 0) { os << message("form_intg2info", words("value", valueType()), domainx_p->name(), domainy_p->name(), u_p->name()); }
  os <<": "<< asString();
  os<<", "<<words("computation type",compuType);
  if (isoGeometric) os<<", iso-geometric";
  if (theVerboseLevel < 2) return;
  os << message("form_info2") << "\n   " << *opu_p << "\n";
}

//-------------------------------------------------------------------------------
//member functions of SuLinearForm class
//-------------------------------------------------------------------------------
//constructors, destructor and assignment operator
//to be safe, basic linear forms are copied

//full constructor from a list of BasicLinearForm
SuLinearForm::SuLinearForm(const std::vector<lfPair>& lfs)
{
  for (number_t n = 0; n < lfs.size(); n++)
  { lfs_.push_back(lfPair(lfs[n].first->clone(), lfs[n].second)); }
}

//full copy of BasicLinearForms
SuLinearForm::SuLinearForm(const SuLinearForm& sulf)
{
  for (number_t n = 0; n < sulf.lfs_.size(); n++)
  { lfs_.push_back(lfPair(sulf.lfs_[n].first->clone(), sulf.lfs_[n].second)); }
}

//full copy of BasicLinearForms
SuLinearForm& SuLinearForm::operator=(const SuLinearForm& sulf)
{
  if (&sulf == this) { return *this; } //same object
  for (number_t n = 0; n < lfs_.size(); n++) //delete current linear forms
  { delete lfs_[n].first; }
  lfs_.clear();
  for (number_t n = 0; n < sulf.lfs_.size(); n++) //copy of sulf
  { lfs_.push_back(lfPair(sulf.lfs_[n].first->clone(), sulf.lfs_[n].second)); }
  return *this;
}

SuLinearForm::~SuLinearForm()
{
  for (number_t n = 0; n < lfs_.size(); n++) //delete current linear forms
  {
    BasicLinearForm* bl_p=lfs_[n].first;
    if (bl_p!=nullptr) delete bl_p;
  }
}

//return the type of the linear form
LinearFormType SuLinearForm::type() const
{
  if (lfs_.size() == 0) { return _undefLf; }
  if (lfs_.size() > 1)  { return _linearCombination; }
  return lfs_[0].first->type();
}

//return the value type (real or complex) of the linear form
ValueType SuLinearForm::valueType() const
{
  for (number_t n = 0; n < lfs_.size(); n++)
  {
    if (lfs_[n].first->valueType() == _complex || lfs_[n].second.imag() != 0) { return _complex; }
  }
  return _real;
}

//return the structure type (scalar, vector, matrix) of the linear form
//currently, return the structure of the first basic linear form
StrucType SuLinearForm::strucType() const
{
  if (lfs_.size()>0) return lfs_[0].first->strucType();
  return _scalar;
}

//return the unknown of the linear form, given by the unknown of the first item
const Unknown* SuLinearForm::unknown() const
{
  if (lfs_.size() == 0) { return nullptr; }
  const Unknown* up = &(lfs_[0].first->up());
  for (number_t n = 1; n < lfs_.size(); n++)
  {
    const Unknown* un=&(lfs_[n].first->up());
    if (un != up) error("free_error","in SuLinearForm::unknown(), inconsistent unknowns: "+un->name()+" and "+up->name());
  }
  return up;
}

//return the space of the linear form
const Space* SuLinearForm::space() const
{
  if (unknown() == nullptr) { return nullptr; }
  return unknown()->space();
}

//check consitancy of Unknown
bool SuLinearForm::checkConsistancy(const SuLinearForm& sulf) const
{
  const Unknown* u1 = unknown(), *u2 = sulf.unknown();
  if (u1 == nullptr || u2 == nullptr) { return true; }
  if (u1->parent() != u2->parent()) { error("form_badlc"); }
  return true;
}

// true if normal is required
bool SuLinearForm::normalRequired() const
{
  for (cit_vlfp it=lfs_.begin(); it!=lfs_.end(); ++it)
  {
    IntgLinearForm* ilf=it->first->asIntgForm();
    if (ilf!=nullptr && ilf->normalRequired()) return true;
  }
  return false;
}

// true if x-normal is required
bool SuLinearForm::xnormalRequired() const
{
  for (cit_vlfp it=lfs_.begin(); it!=lfs_.end(); ++it)
  {
    IntgLinearForm* ilf=it->first->asIntgForm();
    if (ilf!=nullptr && ilf->xnormalRequired()) return true;
  }
  return false;
}

// true if y-normal is required
bool SuLinearForm::ynormalRequired() const
{
  for (cit_vlfp it=lfs_.begin(); it!=lfs_.end(); ++it)
  {
    IntgLinearForm* ilf=it->first->asIntgForm();
    if (ilf!=nullptr && ilf->ynormalRequired()) return true;
  }
  return false;
}

// true if x-normal is required
bool SuLinearForm::xnormalRequiredByOpKernel() const
{
  for (cit_vlfp it=lfs_.begin(); it!=lfs_.end(); ++it)
  {
    IntgLinearForm* ilf=it->first->asIntgForm();
    if (ilf!=nullptr && ilf->xnormalRequiredByOpKernel()) return true;
  }
  return false;
}

// true if y-normal is required
bool SuLinearForm::ynormalRequiredByOpKernel() const
{
  for (cit_vlfp it=lfs_.begin(); it!=lfs_.end(); ++it)
  {
    IntgLinearForm* ilf=it->first->asIntgForm();
    if (ilf!=nullptr && ilf->ynormalRequiredByOpKernel()) return true;
  }
  return false;
}

SuLinearForm& SuLinearForm::operator+=(const SuLinearForm& sulf)
{
  checkConsistancy(sulf);  //check unknown compatibility
  for (number_t n = 1; n <= sulf.lfs_.size(); n++) //add linear forms from sulf
  { lfs_.push_back(lfPair(sulf(n).first->clone(), sulf(n).second)); }
  return *this;
}

SuLinearForm& SuLinearForm::operator-=(const SuLinearForm& sulf)
{
  checkConsistancy(sulf);  //check unknown compatibility
  for (number_t n = 1; n <= sulf.lfs_.size(); n++) //add linear forms from sulf
  { lfs_.push_back(lfPair(sulf(n).first->clone(), -sulf(n).second)); }
  return *this;
}

SuLinearForm& SuLinearForm::operator *=(const complex_t& c)
{
  for (number_t n = 0; n < lfs_.size(); n++)  { lfs_[n].second *= c; }
  return *this;
}

SuLinearForm& SuLinearForm::operator /=(const complex_t& c)
{
  if (std::abs(c) < theZeroThreshold) { error("form_divideby0", c, "SuLinearForm::operator /="); }
  for (number_t n = 0; n < lfs_.size(); n++)  { lfs_[n].second /= c; }
  return *this;
}

string_t SuLinearForm::asString() const //! as symbolic string
{
  string_t s="";
  for (number_t n = 0; n < lfs_.size(); n++)
  {
    s+= coefAsString(n==0,lfs_[n].second);
    s+= lfs_[n].first->asString();
  }
  return s;
}
//-------------------------------------------------------------------------------
//external functions of SuLinearForm class
//-------------------------------------------------------------------------------
//general algebraic operations on linear form
SuLinearForm operator-(const SuLinearForm& sulf)
{
  SuLinearForm nsulf(sulf);
  for (number_t n = 1; n <= nsulf.size(); n++) { nsulf(n).second *= -1; }
  return nsulf;
}

SuLinearForm operator+(const SuLinearForm& sulf1, const SuLinearForm& sulf2)
{
  sulf1.checkConsistancy(sulf2); //check unknown compatibility
  SuLinearForm nsulf(sulf1);       //create a copy of sulf1
  return nsulf += sulf2;
}

SuLinearForm operator-(const SuLinearForm& sulf1, const SuLinearForm& sulf2)
{
  sulf1.checkConsistancy(sulf2); //check unknown compatibility
  SuLinearForm nsulf(sulf1);       //create a copy of sulf1
  return nsulf -= sulf2;
}

SuLinearForm operator*(const complex_t& c, const SuLinearForm& sulf)
{
  SuLinearForm nsulf(sulf);
  return nsulf *= c;
}

SuLinearForm operator*(const SuLinearForm& sulf, const complex_t& c)
{
  SuLinearForm nsulf(sulf);
  return nsulf *= c;
}

SuLinearForm operator/(const SuLinearForm& sulf, const complex_t& c)
{
  SuLinearForm nsulf(sulf);
  return nsulf /= c;
}

//print SuLinearForm on stream
std::ostream& operator<<(std::ostream& os, const SuLinearForm& sulf)
{
  sulf.print(os);
  return os;
}

void SuLinearForm::print(std::ostream& os) const
{
  if (theVerboseLevel > 0)
  {
    if (lfs_.size() == 0)
    {
      os << message("form_isempty");
      return;
    }
    if (lfs_.size() > 1) { os << message("form_lcinfo", words("value", valueType()), unknown()->parent()->name()) << "\n"; }
  }
  if (theVerboseLevel > 1)
  {
    for (number_t n = 0; n < lfs_.size(); n++)
    {
      os << "   " << lfs_[n].second << " * ";
      lfs_[n].first->print(os);
    }
  }
}

//-------------------------------------------------------------------------------
//member functions of LinearForm class
//-------------------------------------------------------------------------------
//constructors
//from a linear combination
LinearForm::LinearForm(const SuLinearForm& lc)
{
  mlclf_[lc.unknown()->parent()] = lc;
  //note: unknown indexation uses parent unknown
  //       in order to component unknown be associated to its parent item
}

//various functionalities
bool LinearForm::isEmpty() const
{
  return (mlclf_.size() == 0);
}

bool LinearForm::singleUnknown() const
{
  return (mlclf_.size() < 2);
}

number_t LinearForm::nbOfUnknowns() const
{
  return mlclf_.size();
}

const Unknown* LinearForm::unknown() const
{
  if (mlclf_.size()>0) return mlclf_.begin()->first;
  return nullptr;
}

std::set<const Unknown*> LinearForm::unknowns() const
{
  std::set<const Unknown*> us;
  for (cit_mulc it = mlclf_.begin(); it != mlclf_.end(); it++)
  { us.insert(it->first); }
  return us;
}

//accessors
//to the linear form associated to an unknown
SuLinearForm& LinearForm::operator[](const Unknown* u)
{
  if (mlclf_.find(u->parent()) == mlclf_.end()) { error("form_nolf", "", "LinearForm::operator[]"); }
  return mlclf_[u->parent()];
}
const SuLinearForm& LinearForm::operator[](const Unknown* u) const
{
  cit_mulc it = mlclf_.find(u->parent());
  if (it == mlclf_.end()) { error("form_nolf", "", "LinearForm::operator[]"); }
  return it->second;
}

//to the n-th basic linear form associated to an unknown
BasicLinearForm& LinearForm::operator()(const Unknown& u, number_t n)
{
  if (mlclf_.find(u.parent()) == mlclf_.end()) { error("form_nolf", "", "LinearForm::operator()"); }
  if (n < 1 || n > mlclf_[u.parent()].size()) { error("form_outbounds", "", n, mlclf_[&u].size(), "LinearForm::operator()"); }
  return *(mlclf_[u.parent()](n).first);
}
const BasicLinearForm& LinearForm::operator()(const Unknown& u, number_t n) const
{
  cit_mulc it = mlclf_.find(u.parent());
  if (it == mlclf_.end()) { error("form_nolf", "", "LinearForm::operator()"); }
  const SuLinearForm& sulf = it->second;
  if (n < 1 || n > sulf.size()) { error("form_outbounds", "", n, sulf.size(), "LinearForm::operator()"); }
  return *(sulf(n).first);
}

//return first linear combination
const SuLinearForm& LinearForm::first() const
{
  if (isEmpty()) { error("form_nolf", "", "LinearForm::first()"); }
  cit_mulc it = mlclf_.begin();
  return it->second;
}

// access to SuLinearForm pointers related to unknown
const SuLinearForm* LinearForm::subLfp(const Unknown* u) const
{
  cit_mulc it=mlclf_.find(u);
  if (it!=mlclf_.end()) return &(it->second);
  return nullptr;
}

// access to SuLinearForm pointers related to unknown
SuLinearForm* LinearForm::subLfp(const Unknown* u)
{
  it_mulc it=mlclf_.find(u);
  if (it!=mlclf_.end()) return &(it->second);
  return nullptr;
}

// true if normal is required
bool LinearForm::normalRequired() const
{
  for (cit_mulc it = mlclf_.begin(); it != mlclf_.end(); it++) //loop on linear combinations of lf
  { if (it->second.normalRequired()) return true; }
  return false;
}

// true if x-normal is required
bool LinearForm::xnormalRequired() const
{
  for (cit_mulc it = mlclf_.begin(); it != mlclf_.end(); it++) //loop on linear combinations of lf
  { if (it->second.xnormalRequired()) return true; }
  return false;
}

// true if y-normal is required
bool LinearForm::ynormalRequired() const
{
  for (cit_mulc it = mlclf_.begin(); it != mlclf_.end(); it++) //loop on linear combinations of lf
  { if (it->second.ynormalRequired()) return true; }
  return false;
}

// true if x-normal is required by operator on kernel
bool LinearForm::xnormalRequiredByOpKernel() const
{
  for (cit_mulc it = mlclf_.begin(); it != mlclf_.end(); it++) //loop on linear combinations of lf
  { if (it->second.xnormalRequiredByOpKernel()) return true; }
  return false;
}

// true if y-normal is required by operator on kernel
bool LinearForm::ynormalRequiredByOpKernel() const
{
  for (cit_mulc it = mlclf_.begin(); it != mlclf_.end(); it++) //loop on linear combinations of lf
  { if (it->second.ynormalRequiredByOpKernel()) return true; }
  return false;
}

//return a copy of the (lc)linear form associated to an unknown
//different from operator[]
LinearForm LinearForm::operator()(const Unknown& u) const
{
  cit_mulc it = mlclf_.find(u.parent());
  if (it == mlclf_.end()) { error("form_nolf", "", "LinearForm::operator()"); }
  return LinearForm(it->second);
}

//internal algebraic operations
LinearForm& LinearForm::operator +=(const LinearForm& lf)
{
  for (cit_mulc it = lf.mlclf_.begin(); it != lf.mlclf_.end(); it++) //loop on linear combinations of lf
  {
    const SuLinearForm& lc = it->second;
    //  it_mulc it2 = mlclf_.find(lc.unknown()->parent());       //use parent to associate lc to its parent item
    //  if (it2 == mlclf_.end()) { mlclf_[lc.unknown()->parent()] = lc; } //no linear combination with such unknown, create a new entry in map
    it_mulc it2 = mlclf_.find(lc.unknown());
    if (it2 == mlclf_.end()) { mlclf_[lc.unknown()] = lc; } //no linear combination with such unknown, create a new entry in map
    else { it2->second += lc; } //add linear combination to item it2
  }
  return *this;
}
LinearForm& LinearForm::operator -=(const LinearForm& lf)
{
  for (cit_mulc it = lf.mlclf_.begin(); it != lf.mlclf_.end(); it++) //loop on linear combinations of lf
  {
    const SuLinearForm& lc = it->second;
    // it_mulc it2 = mlclf_.find(lc.unknown()->parent());        //use parent to associate lc to its parent item
    // if (it2 == mlclf_.end()) { mlclf_[lc.unknown()->parent()] = -lc; } //no linear combination with such unknown, create a new entry in map
    it_mulc it2 = mlclf_.find(lc.unknown());
    if (it2 == mlclf_.end()) { mlclf_[lc.unknown()] = -lc; } //no linear combination with such unknown, create a new entry in map
    else { it2->second -= lc; } //add linear combination to item it2
  }
  return *this;
}
LinearForm& LinearForm::operator *=(const complex_t& c)
{
  for (it_mulc it = mlclf_.begin(); it != mlclf_.end(); it++) { it->second *= c; }
  return *this;
}
LinearForm& LinearForm::operator /=(const complex_t& c)
{
  for (it_mulc it = mlclf_.begin(); it != mlclf_.end(); it++) { it->second /= c; }
  return *this;
}

// return the value type of the linear form
ValueType LinearForm::valueType() const
{
  ValueType vt=_real;
  for (cit_mulc it = mlclf_.begin(); it != mlclf_.end() && vt==_real; it++)  vt=it->second.valueType();
  return vt;
}

//return the structure type (scalar, vector, matrix) of the linear form
//currently, return the structure of the first su linear form
StrucType LinearForm::strucType() const
{
  if (!isEmpty()) return first().strucType();
  return _scalar;
}


//print utility
std::ostream& operator<<(std::ostream& os, const LinearForm& lf)
{
  lf.print(os);
  return os;
}

void LinearForm::print(std::ostream& os)const
{
  if (theVerboseLevel < 1) { return; }
  if (isEmpty())
  {
    os << message("form_isempty") << "\n";
    return;
  }
  if (singleUnknown())
  {
    first().print(os);
    return;
  }
  os << message("form_mulinfo") << "\n";
  if (isTestMode)
  {
    std::map<string_t, SuLinearForm> mlclf_sorted;
    for (cit_mulc it = mlclf_.begin(); it != mlclf_.end(); it++)
    {
      mlclf_sorted[it->first->name()]=it->second;
    }
    for (std::map<string_t, SuLinearForm>::const_iterator it = mlclf_sorted.begin(); it != mlclf_sorted.end(); it++)
    {
      os << "   " << words("unknown") << " " << it->first << ": ";
      it->second.print(os);
    }
  }
  else
  {
    for (cit_mulc it = mlclf_.begin(); it != mlclf_.end(); it++)
    {
      os << "   " << words("unknown") << " " << it->second.unknown()->name() << ": ";
      it->second.print(os);
    }
  }
}

//-------------------------------------------------------------------------------
//external functions of LinearForm class
//-------------------------------------------------------------------------------

void intgLfBuildParam(const Parameter& p, real_t& bound, bool& isogeom, IntegrationMethod*& meth,
                      IntegrationMethods& meths, QuadRule& qr, number_t& qo, SymType& st,
                      ComputationType& ct, const GeomDomain*& extdom)
{
  ParameterKey key=p.key();
  switch (key)
  {
    case _pk_bound:
    {
      switch (p.type())
      {
        case _integer:
        {
          bound=real_t(p.get_n());
          break;
        }
        case _real:
        {
          bound=p.get_r();
          break;
        }
        default:
          error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_isogeo:
    {
      isogeom=true;
      break;
    }
    case _pk_method:
    {
      if (p.type() == _pointerIntegrationMethod) { meth=const_cast<IntegrationMethod*>(reinterpret_cast<const IntegrationMethod*>(p.get_p())); }
      else if (p.type() == _pointerIntegrationMethods) { meths=*reinterpret_cast<const IntegrationMethods*>(p.get_p()); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      break;
    }
    case _pk_order:
    {
      if (p.type() == _integer) { qo=p.get_n(); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      break;
    }
    case _pk_quad:
    {
      switch (p.type())
      {
        case _integer:
        case _enumQuadRule:
          qr=QuadRule(p.get_n());
          break;
        default: error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_symmetry:
    {
      switch (p.type())
      {
        case _integer:
        case _enumSymType:
          st=SymType(p.get_n());
          break;
        default: error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_computation:
    {
      switch (p.type())
      {
        case _integer:
        case _enumComputationType:
          ct=ComputationType(p.get_n());
          break;
        default: error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_extension_domain:
    {
      if (p.type() == _pointerGeomDomain) { extdom=static_cast<const GeomDomain*>(p.get_p()); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      break;
    }
    default:
      error("lform_unexpected_param_key",words("param key",key));
  }
}

void intgLfParamCompatibility(const ParameterKey& key, std::set<ParameterKey>& usedParams)
{
  // user must use _quad or _method, not both
  if (key==_pk_quad && usedParams.find(_pk_method) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_method)); }
  if (key==_pk_method && usedParams.find(_pk_quad) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_quad)); }
  // user must use _order or _method, not both
  if (key==_pk_order && usedParams.find(_pk_method) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_method)); }
  if (key==_pk_method && usedParams.find(_pk_order) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_order)); }
}

LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const std::vector<Parameter>& ps)
{
  trace_p->push("intg(Domain, OperatorOnUnknown, Parameters)");
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_method);
  params.insert(_pk_quad);
  params.insert(_pk_order);
  params.insert(_pk_symmetry);
  params.insert(_pk_isogeo);
  params.insert(_pk_computation);

  real_t bound=0.;
  bool isogeom=false;
  IntegrationMethod* meth=nullptr;
  IntegrationMethods meths;
  QuadRule qr=_defaultRule;
  number_t qo=0;
  SymType st=_undefSymmetry;
  ComputationType ct=_undefComputation;
  const GeomDomain* extdom=nullptr;

  for (number_t i=0; i < ps.size(); ++i)
  {
    intgLfBuildParam(ps[i], bound, isogeom, meth, meths, qr, qo, st, ct, extdom);

    ParameterKey key=ps[i].key();
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("lform_unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);

    intgLfParamCompatibility(key, usedParams);
  }
  // (quad, order) has to be set (or method)
  if (params.find(_pk_quad) != params.end() && params.find(_pk_order) == params.end()) { error("param_missing","order"); }
  if (params.find(_pk_order) != params.end() && params.find(_pk_quad) == params.end()) { error("param_missing","quad"); }

  BasicLinearForm* blf;
  if (usedParams.find(_pk_method) != usedParams.end())
  {
    // key _method is given
    if (meth!=nullptr)
    {
      if (opu.hasKernel())
      {
        // we force building of meths
        meths=IntegrationMethods(*meth);
        blf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, opu, meths));
      }
      else
      {
        // IntegrationMethod is given
        blf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, opu, *meth));
      }
    }
    else
    {
      // IntegrationMethods is given
      blf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, opu, meths));
    }
  }
  else
  {
    if (opu.hasKernel())
    {
      // we force building of meths
      //meths=IntegrationMethods(QuadratureIM(qr, qo));
      meths=IntegrationMethods(_quad=qr, _order=qo);
      blf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, opu, meths));
    }
    else
    {
      // key _method was not given, we consider _quad and _order (possibly with their default values)
      blf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, opu, qr, qo));
    }
  }
  blf->isoGeometric = isogeom;
  if (ct!=_undefComputation) { blf->compuType = ct; }
  if(extdom!=nullptr)
  {
    if(!dom.isSideDomain() || extdom->dim() != dom.dim() + 1)
    {
       warning("free_warning","extension_domain key is not used, "+dom.name()+" is not a side domain");
    }
    else
    {
       blf->extDom_p() = const_cast<const GeomDomain*>(&dom.extendDomain(false,*extdom));
    }
  }
  SuLinearForm sulf;
  sulf.lfs().push_back(lfPair(blf, 1.));
  trace_p->pop();
  return LinearForm(sulf);
}

LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu)
{
  std::vector<Parameter> ps(0);
  return intg(dom, opu, ps);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  return intg(dom, opu, ps);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps={p1, p2};
  return intg(dom, opu, ps);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3)
{
  std::vector<Parameter> ps={p1, p2, p3};
  return intg(dom, opu, ps);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  return intg(dom, opu, ps);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  return intg(dom, opu, ps);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  return intg(dom, opu, ps);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  return intg(dom, opu, ps);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  return intg(dom, opu, ps);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  return intg(dom, opu, ps);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9, const Parameter& p10)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  return intg(dom, opu, ps);
}

LinearForm intg(const GeomDomain& dom, const Unknown& u)
{
  std::vector<Parameter> ps(0);
  return intg(dom, id(u), ps);
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  return intg(dom, id(u), ps);
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps={p1, p2};
  return intg(dom, id(u), ps);
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3)
{
  std::vector<Parameter> ps={p1, p2, p3};
  return intg(dom, id(u), ps);
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  return intg(dom, id(u), ps);
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  return intg(dom, id(u), ps);
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  return intg(dom, id(u), ps);
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  return intg(dom, id(u), ps);
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  return intg(dom, id(u), ps);
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  return intg(dom, id(u), ps);
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9, const Parameter& p10)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  return intg(dom, id(u), ps);
}

LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = lcopu.begin(); cit_Lcopu != lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first);
  }
  return lf;
}

LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = lcopu.begin(); cit_Lcopu != lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, p1);
  }
  return lf;
}

LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = lcopu.begin(); cit_Lcopu != lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, p1, p2);
  }
  return lf;
}

LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = lcopu.begin(); cit_Lcopu != lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, p1, p2, p3);
  }
  return lf;
}

LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = lcopu.begin(); cit_Lcopu != lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, p1, p2, p3, p4);
  }
  return lf;
}

LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = lcopu.begin(); cit_Lcopu != lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, p1, p2, p3, p4, p5);
  }
  return lf;
}

LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = lcopu.begin(); cit_Lcopu != lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, p1, p2, p3, p4, p5, p6);
  }
  return lf;
}

LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = lcopu.begin(); cit_Lcopu != lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, p1, p2, p3, p4, p5, p6, p7);
  }
  return lf;
}

LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = lcopu.begin(); cit_Lcopu != lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, p1, p2, p3, p4, p5, p6, p7, p8);
  }
  return lf;
}

LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = lcopu.begin(); cit_Lcopu != lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, p1, p2, p3, p4, p5, p6, p7, p8, p9);
  }
  return lf;
}

LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9, const Parameter& p10)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = lcopu.begin(); cit_Lcopu != lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
  }
  return lf;
}

LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus)
{
  OperatorOnUnknown* opu= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  if (opu->strucType()==_matrix)
  {
    where("intg(Domain, KernelOperatorOnUnknowns, ...)");
    error("scalar_or_vector");
  }
  return intg(dom, *opu);
}

LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1)
{
  OperatorOnUnknown* opu= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  if (opu->strucType()==_matrix)
  {
    where("intg(Domain, KernelOperatorOnUnknowns, ...)");
    error("scalar_or_vector");
  }
  return intg(dom, *opu, p1);
}

LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2)
{
  OperatorOnUnknown* opu= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  if (opu->strucType()==_matrix)
  {
    where("intg(Domain, KernelOperatorOnUnknowns, ...)");
    error("scalar_or_vector");
  }
  return intg(dom, *opu, p1, p2);
}

LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3)
{
  OperatorOnUnknown* opu= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  if (opu->strucType()==_matrix)
  {
    where("intg(Domain, KernelOperatorOnUnknowns, ...)");
    error("scalar_or_vector");
  }
  return intg(dom, *opu, p1, p2, p3);
}

LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4)
{
  OperatorOnUnknown* opu= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  if (opu->strucType()==_matrix)
  {
    where("intg(Domain, KernelOperatorOnUnknowns, ...)");
    error("scalar_or_vector");
  }
  return intg(dom, *opu, p1, p2, p3, p4);
}

LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  OperatorOnUnknown* opu= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  if (opu->strucType()==_matrix)
  {
    where("intg(Domain, KernelOperatorOnUnknowns, ...)");
    error("scalar_or_vector");
  }
  return intg(dom, *opu, p1, p2, p3, p4, p5);
}

LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  OperatorOnUnknown* opu= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  if (opu->strucType()==_matrix)
  {
    where("intg(Domain, KernelOperatorOnUnknowns, ...)");
    error("scalar_or_vector");
  }
  return intg(dom, *opu, p1, p2, p3, p4, p5, p6);
}

LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
{
  OperatorOnUnknown* opu= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  if (opu->strucType()==_matrix)
  {
    where("intg(Domain, KernelOperatorOnUnknowns, ...)");
    error("scalar_or_vector");
  }
  return intg(dom, *opu, p1, p2, p3, p4, p5, p6, p7);
}

LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8)
{
  OperatorOnUnknown* opu= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  if (opu->strucType()==_matrix)
  {
    where("intg(Domain, KernelOperatorOnUnknowns, ...)");
    error("scalar_or_vector");
  }
  return intg(dom, *opu, p1, p2, p3, p4, p5, p6, p7, p8);
}

LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9)
{
  OperatorOnUnknown* opu= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  if (opu->strucType()==_matrix)
  {
    where("intg(Domain, KernelOperatorOnUnknowns, ...)");
    error("scalar_or_vector");
  }
  return intg(dom, *opu, p1, p2, p3, p4, p5, p6, p7, p8, p9);
}

LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9, const Parameter& p10)
{
  OperatorOnUnknown* opu= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  if (opu->strucType()==_matrix)
  {
    where("intg(Domain, KernelOperatorOnUnknowns, ...)");
    error("scalar_or_vector");
  }
  return intg(dom, *opu, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
}

LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const std::vector<Parameter>& ps)
{
  trace_p->push("intg(Domain, Domain, OperatorOnUnknown, Parameters)");
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_method);
  params.insert(_pk_quad);
  params.insert(_pk_order);
  params.insert(_pk_symmetry);
  params.insert(_pk_isogeo);

  real_t bound=0.;
  bool isogeom=false;
  IntegrationMethod* meth=nullptr;
  IntegrationMethods meths;
  QuadRule qr=_defaultRule;
  number_t qo=0;
  SymType st=_undefSymmetry;
  ComputationType ct=_undefComputation;
  const GeomDomain* extdom=nullptr;

  for (number_t i=0; i < ps.size(); ++i)
  {
    intgLfBuildParam(ps[i], bound, isogeom, meth, meths, qr, qo, st, ct, extdom);

    ParameterKey key=ps[i].key();
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("form_unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);

    intgLfParamCompatibility(key, usedParams);
  }

  // (quad, order) has to be set (or method)
  if (params.find(_pk_quad) != params.end() && params.find(_pk_order) == params.end()) { error("param_missing","order"); }
  if (params.find(_pk_order) != params.end() && params.find(_pk_quad) == params.end()) { error("param_missing","quad"); }

  BasicLinearForm* bf;
  if (usedParams.find(_pk_method) != usedParams.end())
  {
    // key _method was given
    if (meth!=nullptr)
    {
      // IntegrationMethod is given
      bf = static_cast<BasicLinearForm*>(new DoubleIntgLinearForm(domv, domu, opu, qr, qo));
    }
    else
    {
      // IntegrationMethods is given
      bf = static_cast<BasicLinearForm*>(new DoubleIntgLinearForm(domv, domu, opu, qr, qo));
    }
  }
  else
  {
    // key _method was not given, we consider _quad and _order (possibly with their default values)
    bf = static_cast<BasicLinearForm*>(new DoubleIntgLinearForm(domv, domu, opu, qr, qo));
  }
  bf->isoGeometric = isogeom;
  if (ct!=_undefComputation) { bf->compuType = ct; }
  SuLinearForm sulf;
  sulf.lfs().push_back(lfPair(bf, 1.));
  trace_p->pop();
  return LinearForm(sulf);
}

LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu)
{
  std::vector<Parameter> ps(0);
  return intg(domv, domu, opu, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  return intg(domv, domu, opu, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2)
{
  std::vector<Parameter> ps={p1, p2};
  return intg(domv, domu, opu, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps={p1, p2, p3};
  return intg(domv, domu, opu, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  return intg(domv, domu, opu, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  return intg(domv, domu, opu, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  return intg(domv, domu, opu, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  return intg(domv, domu, opu, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7, const Parameter& p8)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  return intg(domv, domu, opu, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7, const Parameter& p8, const Parameter& p9)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  return intg(domv, domu, opu, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  return intg(domv, domu, opu, ps);
}

LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u)
{
  std::vector<Parameter> ps(0);
  return intg(domv, domu, id(u), ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  return intg(domv, domu, id(u), ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2)
{
  std::vector<Parameter> ps={p1, p2};
  return intg(domv, domu, id(u), ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps={p1, p2, p3};
  return intg(domv, domu, id(u), ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  return intg(domv, domu, id(u), ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  return intg(domv, domu, id(u), ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  return intg(domv, domu, id(u), ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  return intg(domv, domu, id(u), ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7, const Parameter& p8)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  return intg(domv, domu, id(u), ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7, const Parameter& p8, const Parameter& p9)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  return intg(domv, domu, id(u), ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  return intg(domv, domu, id(u), ps);
}

//general algebraic operations on linear form
LinearForm operator-(const LinearForm& lf)
{
  LinearForm nlf(lf);
  return nlf *= -1;
}

LinearForm operator+(const LinearForm& lf1, const LinearForm& lf2)
{
  LinearForm nlf(lf1);
  return nlf += lf2;
}

LinearForm operator-(const LinearForm& lf1, const LinearForm& lf2)
{
  LinearForm nlf(lf1);
  return nlf -= lf2;
}

LinearForm operator*(const int_t& i, const LinearForm& lf)
{
  LinearForm nlf(lf);
  return nlf *= real_t(i);
}

LinearForm operator*(const int& i, const LinearForm& lf)
{
  LinearForm nlf(lf);
  return nlf *= real_t(i);
}

LinearForm operator*(const number_t& n, const LinearForm& lf)
{
  LinearForm nlf(lf);
  return nlf *= real_t(n);
}

LinearForm operator*(const real_t& r, const LinearForm& lf)
{
  LinearForm nlf(lf);
  return nlf *= r;
}

LinearForm operator*(const complex_t& c, const LinearForm& lf)
{
  LinearForm nlf(lf);
  return nlf *= c;
}

LinearForm operator*(const LinearForm& lf, const int_t& i)
{
  LinearForm nlf(lf);
  return nlf *= real_t(i);
}

LinearForm operator*(const LinearForm& lf, const int& i)
{
  LinearForm nlf(lf);
  return nlf *= real_t(i);
}

LinearForm operator*(const LinearForm& lf, const number_t& n)
{
  LinearForm nlf(lf);
  return nlf *= real_t(n);
}

LinearForm operator*(const LinearForm& lf, const real_t& r)
{
  LinearForm nlf(lf);
  return nlf *= r;
}

LinearForm operator*(const LinearForm& lf, const complex_t& c)
{
  LinearForm nlf(lf);
  return nlf *= c;
}

LinearForm operator/(const LinearForm& lf, const int_t& i)
{
  if (std::abs(real_t(i)) < theZeroThreshold) { error("form_divideby0", complex_t(real_t(i),0.), "SuLinearForm::operator /"); }
  LinearForm nlf(lf);
  return nlf /= real_t(i);
}

LinearForm operator/(const LinearForm& lf, const int& i)
{
  if (std::abs(real_t(i)) < theZeroThreshold) { error("form_divideby0", complex_t(real_t(i),0.), "SuLinearForm::operator /"); }
  LinearForm nlf(lf);
  return nlf /= real_t(i);
}

LinearForm operator/(const LinearForm& lf, const number_t& n)
{
  if (n==0) { error("form_divideby0", complex_t(real_t(n),0.), "SuLinearForm::operator /"); }
  LinearForm nlf(lf);
  return nlf /= real_t(n);
}

LinearForm operator/(const LinearForm& lf, const real_t& r)
{
  if (std::abs(r) < theZeroThreshold) { error("form_divideby0", complex_t(r,0.), "SuLinearForm::operator /"); }
  LinearForm nlf(lf);
  return nlf /= r;
}

LinearForm operator/(const LinearForm& lf, const complex_t& c)
{
  if (std::abs(c) < theZeroThreshold) { error("form_divideby0", c, "SuLinearForm::operator /"); }
  LinearForm nlf(lf);
  return nlf /= c;
}

//----------------------------------------
// DEPRECATED (to be removed)
//----------------------------------------

LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const IntegrationMethods& ims, bool isogeo)
{
  warning("deprecated", "intg(Domain, ..., IntegrationMethods, bool)", "intg(Domain, ..., _method=?, _isogeo)");
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, opu, ims));
  bf->isoGeometric=isogeo;
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return LinearForm(lc);
}
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& Lcopu, const IntegrationMethods& ims, bool isogeo)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = Lcopu.begin(); cit_Lcopu != Lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, ims, isogeo);
  }
  return lf;
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, const IntegrationMethods& ims, bool isogeo)
{
  return intg(dom, id(u), ims, isogeo);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const IntegrationMethod& im, bool isogeo)
{
  warning("deprecated", "intg(Domain, ..., IntegrationMethod, bool)", "intg(Domain, ..., _method=? , _isogeo)");
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, opu, im));
  bf->isoGeometric=isogeo;
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return LinearForm(lc);
}
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& Lcopu, const IntegrationMethod& im, bool isogeo)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = Lcopu.begin(); cit_Lcopu != Lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, im, isogeo);
  }
  return lf;
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, const IntegrationMethod& im, bool isogeo)
{
  return intg(dom, id(u), im, isogeo);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, QuadRule qr, number_t qo, bool isogeo)
{
  warning("deprecated", "intg(Domain, ..., QuadRule, Number, bool)", "intg(Domain, ..., _quad=?, _order=?, _isogeo)");
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, opu, qr, qo));
  bf->isoGeometric=isogeo;
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return LinearForm(lc);
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, bool isogeo)
{
  return intg(dom, id(u), _defaultRule, u.degree(), isogeo);
}
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& Lcopu, QuadRule qr, number_t qo, bool isogeo)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = Lcopu.begin(); cit_Lcopu != Lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, qr, qo, isogeo);
  }
  return lf;
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, QuadRule qr, number_t qo, bool isogeo)
{
  return intg(dom, id(u), qr, qo, isogeo);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, ComputationType ct, const IntegrationMethods& ims, bool isogeo)
{
  warning("deprecated", "intg(Domain, ..., ComputationType, IntegrationMethods, bool)", "intg(Domain, ..., _computation=?, _method=?, _isogeo)");
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, opu, ims));
  bf->isoGeometric = isogeo;
  bf->compuType = ct;
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return LinearForm(lc);
}
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& Lcopu, ComputationType ct, const IntegrationMethods& ims, bool isogeo)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = Lcopu.begin(); cit_Lcopu != Lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, ct, ims, isogeo);
  }
  return lf;
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, ComputationType ct, const IntegrationMethods& ims, bool isogeo)
{
  return intg(dom, id(u), ct, ims, isogeo);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, ComputationType ct, const IntegrationMethod& im, bool isogeo)
{
  warning("deprecated", "intg(Domain, ..., ComputationType, IntegrationMethod, bool)", "intg(Domain, ..., _computation=?, _method=?, _isogeo)");
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, opu, im));
  bf->compuType = ct;
  bf->isoGeometric = isogeo;
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return LinearForm(lc);
}
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& Lcopu, ComputationType ct, const IntegrationMethod& im, bool isogeo)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = Lcopu.begin(); cit_Lcopu != Lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, ct, im, isogeo);
  }
  return lf;
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, ComputationType ct, const IntegrationMethod& im, bool isogeo)
{
  return intg(dom, id(u), ct, im, isogeo);
}
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, ComputationType ct, QuadRule qr, number_t qo, bool isogeo)
{
  warning("deprecated", "intg(Domain, ..., ComputationType, QuadRule, Number, bool)", "intg(Domain, ..., _computation=?,_quad=?, _order=?, _isogeo)");
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, opu, qr, qo));
  bf->compuType = ct;
  bf->isoGeometric = isogeo;
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return LinearForm(lc);
}
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& Lcopu, ComputationType ct, QuadRule qr, number_t qo, bool isogeo)
{
  LinearForm lf;
  for (cit_opuval cit_Lcopu = Lcopu.begin(); cit_Lcopu != Lcopu.end(); cit_Lcopu++)
  {
    lf += cit_Lcopu->second * intg(dom, *cit_Lcopu->first, ct, qr, qo, isogeo);
  }
  return lf;
}
LinearForm intg(const GeomDomain& dom, const Unknown& u, ComputationType ct, QuadRule qr, number_t qo, bool isogeo)
{
  return intg(dom, id(u), ct, qr, qo, isogeo);
}
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const IntegrationMethods& ims, bool isogeo)
{
  warning("deprecated", "intg(Domain, ..., IntegrationMethods, bool)", "intg(Domain, ..., _method=?, _isogeo)");
  OperatorOnUnknown* opv= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  if (opv->strucType()==_matrix)
  {
    where("intg(Domain, KernelOperatorOnUnknowns, IntegrationMethods)");
    error("scalar_or_vector");
  }
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, *opv, ims));
  bf->isoGeometric = isogeo;
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return LinearForm(lc);
}
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const IntegrationMethod& im, bool isogeo)
{
  warning("deprecated", "intg(Domain, ..., IntegrationMethod, bool)", "intg(Domain, ..., _method=?, _isogeo)");
  OperatorOnUnknown* opv= new OperatorOnUnknown(toOperatorOnUnknown(kopus));  //move to OperatorOnUnknown
  IntegrationMethods ims(im);
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, *opv, ims));
  bf->isoGeometric = isogeo;
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return LinearForm(lc);
}
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, QuadRule  qr, number_t qo, bool isogeo)
{
  warning("deprecated", "intg(Domain, ..., QuadRule, Number, bool)", "intg(Domain, ..., _quad=?, _order=?, _isogeo)");
  OperatorOnUnknown* opv= new OperatorOnUnknown(toOperatorOnUnknown(kopus));
  IntegrationMethods ims(QuadratureIM(qr,qo));
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, *opv, ims));
  bf->isoGeometric = isogeo;
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return LinearForm(lc);
}
LinearForm intg(const GeomDomain& domx, const GeomDomain& domy, const OperatorOnUnknown& opu, QuadRule qr, number_t qo, bool isogeo)
{
  warning("deprecated", "intg(Domain, ..., QuadRule, Number, bool)", "intg(Domain, ..., _quad=?, _order=?, _isogeo)");
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new DoubleIntgLinearForm(domx, domy, opu, qr, qo));
  bf->isoGeometric = isogeo;
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return LinearForm(lc);
}
LinearForm intg(const GeomDomain& domx, const GeomDomain& domy, const Unknown& u, QuadRule qr, number_t qo, bool isogeo)
{
  return intg(domx, domy, id(u), qr, qo, isogeo);
}

} //end of namespace xlifepp
