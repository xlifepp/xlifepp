/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file BilinearForm.cpp
  \author E. Lunéville
  \since 23 mar 2012
  \date  31 mar 2012

  \brief Implementation of xlifepp::BilinearForm classes member functions and related utilities
*/

#include "BilinearForm.hpp"
#include "utils.h"

namespace xlifepp
{

//! return the u subspace of the bilinear form, construct it if does not exist
Space* BasicBilinearForm::subSpace_up()
{
  if (u_p==nullptr)
  {
    where("BasicBilinearForm::subSpace_up");
    error("null_pointer","unknown");
  }
  if (domainu_p==nullptr)
  {
    where("BasicBilinearForm::subSpace_up");
    error("null_pointer","domain");
  }

  Space* sp = Space::findSubSpace(domainu_p,u_p->space());
  if (sp==nullptr) sp=new Space(*domainu_p, *u_p->space(),u_p->space()->name()+" on "+domainu_p->name());
  return sp;
}

//! return the v subspace of the bilinear form, construct it if does not exist
Space* BasicBilinearForm::subSpace_vp()
{
  if (u_p==nullptr)
  {
    where("BasicBilinearForm::subSpace_vp");
    error("null_pointer","unknown");
  }
  if (domainu_p==nullptr)
  {
    where("BasicBilinearForm::subSpace_vp");
    error("null_pointer","domain");
  }

  Space* sp = Space::findSubSpace(domainv_p,v_p->space());
  if (sp==nullptr) sp=new Space(*domainv_p, *v_p->space(),v_p->space()->name()+" on "+domainv_p->name());
  return sp;
}

//check status of unknown and testfunction
//   u_p should be a real unknown
//   v_p should be a test function
//   raise a warning
void BasicBilinearForm::checkUnknowns() const
{
  if (u_p->isTestFunction())
  {
    warning("free_warning"," in "+asString()+" using TestFunction in place of Unknown may be hazardous");
    return;
  }
  if (!v_p->isTestFunction())
  {
    warning("free_warning"," in "+asString()+" using Unknown in place of TestFunction may be hazardous");
    return;
  }
}

//-------------------------------------------------------------------------------
// member functions of IntgBilinearForm class
//-------------------------------------------------------------------------------
// constructors

IntgBilinearForm::IntgBilinearForm(const GeomDomain& dom, const OperatorOnUnknowns& opus, const IntegrationMethod& im,
                                   SymType st)
{
  const OperatorOnUnknown& opu = opus.opu();
  const OperatorOnUnknown& opv = opus.opv();
  opus_p = new OperatorOnUnknowns(opus); //hard copy of opu/v to avoid memory trap (to be improved)
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &dom;
  domainv_p = &dom;
  extDomainu_p=nullptr;
  extDomainv_p=nullptr;
  if (!im.isSingleIM())
    {
      where("IntgBilinearForm::IntgBilinearForm");
      error("im_not_single");
    }
  intgMethod_p = &im; //pointer copy !!!
  setComputationType();
  if (st==_undefSymmetry) setSymType();
  else symmetry=st;
  checkUnknowns();
}

IntgBilinearForm::IntgBilinearForm(const GeomDomain& dom, const OperatorOnUnknowns& opus, QuadRule qr, number_t qo, SymType st)
{
  const OperatorOnUnknown& opu = opus.opu();
  const OperatorOnUnknown& opv = opus.opv();
  opus_p = new OperatorOnUnknowns(opus); //hard copy of opu/v to avoid memory trap (to be improved)
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &dom;
  domainv_p = &dom;
  extDomainu_p=nullptr;
  extDomainv_p=nullptr;
  setIntegrationMethod(dom, opus, qr, qo);
  setComputationType();
  if (st==_undefSymmetry) setSymType();
  else symmetry=st;
  checkUnknowns();
}

IntgBilinearForm::IntgBilinearForm(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                                   const OperatorOnUnknown& opv, const IntegrationMethod& im, SymType st)
{
  if (!checkConsistancy(opu, aop, opv)) {error("opu_badopus", words("algop", aop), "IntgBilinearForm");}
  opus_p = new OperatorOnUnknowns(opu, opv, aop);
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &dom;
  domainv_p = &dom;
  extDomainu_p=nullptr;
  extDomainv_p=nullptr;
  if (!im.isSingleIM())
  {
    where("IntgBilinearForm::IntgBilinearForm");
    error("im_not_single");
  }

  intgMethod_p = &im; //pointer copy !!!
  setComputationType();
  if (st==_undefSymmetry) setSymType();
  else symmetry=st;
  checkUnknowns();
}

IntgBilinearForm::IntgBilinearForm(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                                   const OperatorOnUnknown& opv, QuadRule qr, number_t qo, SymType st)
{
  if (!checkConsistancy(opu, aop, opv)) {error("opu_badopus", words("algop", aop), "IntgBilinearForm");}
  compuType = _FEComputation;
  opus_p = new OperatorOnUnknowns(opu, opv, aop);
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &dom;
  domainv_p = &dom;
  extDomainu_p=nullptr;
  extDomainv_p=nullptr;
  setIntegrationMethod(dom, *opus_p, qr, qo);
  setComputationType();
  symmetry=st;
  if (st==_undefSymmetry) setSymType();
  else symmetry=st;
  checkUnknowns();
}

// copy constructor
IntgBilinearForm::IntgBilinearForm(const IntgBilinearForm& ibf)
  : BasicBilinearForm(ibf)
{
  intgMethod_p=ibf.intgMethod_p;
  opus_p=new OperatorOnUnknowns(*ibf.opus_p);
}

// destructor
IntgBilinearForm::~IntgBilinearForm()
{
  if (opus_p!=nullptr) delete opus_p;
}

//assign operator
IntgBilinearForm& IntgBilinearForm::operator=(const IntgBilinearForm& ibf)
{
  if (this == &ibf) return *this;
  if (opus_p!=nullptr) delete opus_p;
  opus_p = new OperatorOnUnknowns(*ibf.opus_p);
  intgMethod_p=ibf.intgMethod_p;
  compuType = ibf.compuType;
  u_p = ibf.u_p;
  v_p = ibf.v_p;
  domainu_p = ibf.domainu_p;
  domainv_p = ibf.domainv_p;
  extDomainu_p=ibf.extDomainu_p;
  extDomainv_p=ibf.extDomainv_p;
  symmetry = ibf.symmetry;
  isoGeometric=ibf.isoGeometric;
  return *this;
}

//set integration method from operator on unknowns and user quadrature parameters
void IntgBilinearForm::setIntegrationMethod(const GeomDomain& dom, const OperatorOnUnknowns& opus,
                                            QuadRule qr, number_t qo)
{
  intgMethod_p = nullptr;
  if (dom.domType() != _meshDomain) { error("domain_notmesh",dom.name(),words("domain type",dom.domType())); }
  const OperatorOnUnknown& opu=opus_p->opu();
  const OperatorOnUnknown& opv=opus_p->opv();
  const MeshDomain* mdom = dom.meshDomain();
  number_t ordopu = opu.degree(); //degree of u-polynoms to be integrated
  number_t ordopv = opv.degree(); //degree of v-polynoms to be integrated
  number_t ord = ordopu + ordopv; //degree of polynom to be integrated
  number_t mord = mdom->order();  //mesh order
  if (mord>1) ord+=mord;
  if (ord == 0) ord = 1;
  if (ord < 2 && (opu.hasFunction() || opu.hasFunction())) ord=2; // at least order 2 if there is a function
  if (qr != _defaultRule && qo > ord) ord = qo; //keep user choice
  intgMethod_p = new QuadratureIM(mdom->shapeTypes, qr, ord);
}

void IntgBilinearForm::setComputationType()
{
  compuType = _FEComputation;
  Space* spu = opus_p->opu().unknown()->space();
  Space* spv = opus_p->opv().unknown()->space();
  number_t dsu=spu->dimDomain(), dsv=spv->dimDomain(), dd =domainu_p->dim();
  const MeshDomain* mdom=domainu_p->meshDomain();
  if (mdom!=nullptr  && (dsu > dd || dsv > dd))  // side domain
  {
    if (mdom->isSidesOf_p!=nullptr) // domain made of sides of elements (DG computation)
    {
      compuType = _DGComputation;
      return;
    }
    if ((mdom->isSideDomain() && opus_p->extensionRequired()) //boundary domain and non tangent derivative
        || (dsu > dd && !mdom->isSideOf(*spu->domain()->meshDomain()) )
        || (dsv > dd && !mdom->isSideOf(*spv->domain()->meshDomain()) ) )
    { compuType = _FEextComputation; return; }
  }
  if (spu->domain()->mesh()!= spv->domain()->mesh()) {compuType = _FEextComputation; return;}

  //warning: boundary terms involving non tangential derivatives with spectral unknown are not handled for the moment
  if (u_p->isSpectral() || v_p->isSpectral()) compuType = _FESPComputation;
  if (u_p->isSpectral() && v_p->isSpectral()) compuType = _SPComputation;
}

// set (change) the unknowns of IntgBilinearForm
void IntgBilinearForm::setUnknowns(const Unknown& u, const Unknown& v)
{
  u_p=&u; v_p=&v;
  const_cast<OperatorOnUnknowns*>(opus_p)->setUnknowns(u,v);
}

// clone of the linear form
BasicBilinearForm* IntgBilinearForm::clone() const
{
  return new IntgBilinearForm(*this);
}

bool IntgBilinearForm::hasJump() const
{
  return (opus_p->opu().jumpType()!=_nojump || opus_p->opu().jumpType()!=_nojump);
}

/*! split IntgBilinearForm involving particular operators (jump : u1-u2, mean : (u1+u2)/2
    for instance, if G is one side of a crack with Gd the opposite side of gamma
         intg(G,[u]*v)= intg(G,(u|G-u|Gd)*v) is split as  intg(G,u*v)-intg(G,u|Gd*v)  (v continuous)
         intg(G,u*[v])= intg(G,u*(v|G-v|Gd)) is split as  intg(G,u*v)-intg(G,u*v|Gd)  (u continuous)
         intg(G,{u}*v)= intg(G,(u|G+u|Gd)*v) is split as  intg(G,u*v)+intg(G,u|Gd*v)  (v continuous)
         intg(G,u*{v})= intg(G,u*(v|G+v|Gd)) is split as  intg(G,u*v)+intg(G,u*v|Gd)  (u continuous)
         intg(G,[u]*[v])= intg(G,(u|G-u|Gd)*(v|G-v|Gd)) is split as  intg(G,u*v)+intg(Gd,u*v)-intg(G,u*v|Gd)-intg(G,u|Gd*v)
         intg(G,{u}*{v})= intg(G,(u|G+u|Gd)*(v|G+v|Gd)) is split as  intg(G,u*v)+intg(Gd,u*v)+intg(G,u*v|Gd)+intg(G,u|Gd*v)
         intg(G,{u}*[v])= intg(G,(u|G+u|Gd)*(v|G-v|Gd)) is split as  intg(G,u*v)-intg(Gd,u*v)-intg(G,u*v|Gd)+intg(G,u|Gd*v)
         intg(G,[u]*{v})= intg(G,(u|G-u|Gd)*(v|G+v|Gd)) is split as  intg(G,u*v)-intg(Gd,u*v)+intg(G,u*v|Gd)-intg(G,u|Gd*v)

         gives the following table intg(G,u*v) intg(Gd,u*v) intg(G,u*v|Gd) intg(G,u|Gd*v)
                      [u]*v             1            0             0            -1
                      u*[v]             1            0            -1             0
                      {u}*v             1            0             0             1
                      u*{v}             1            0             1             0
                     [u]*[v]            1            1            -1            -1
                     {u}*{v}            1            1             1             1
                     {u}*[v]            1           -1            -1             1
                     [u]*{v}            1           -1             1            -1
                                                     a             b             c
          all coefficient are multiplied by the factor f = 1 if no mean operator, f = 0.5 if one mean operator, f = 0.25 if two mean operators
*/
std::vector<bfPair> IntgBilinearForm::split() const
{
  if (!domainu_p->meshDomain()->isCrack()) //no copy, shared pointer !
    return std::vector<bfPair>(1,bfPair(const_cast<BasicBilinearForm*>(static_cast<const BasicBilinearForm*>(this)),1.));
  JumpType jtu=opus_p->opu().jumpType(), jtv=opus_p->opv().jumpType();
  real_t f=1., a=0, b=0, c=0;
  number_t n=1;
  if (jtu==_jump)
  {
    c=-1; n=2;
    if (jtv==_jump) {a=1; b=-1; n=4;}
    else if (jtv==_mean) {a=-1; b=1; n=4; f*=0.5;}
  }
  else if (jtu==_mean)
  {
    c=1;
    n=2;
    f*=0.5;
    if (jtv==_jump) {a=-1; b=-1; n=4;}
    else if (jtv==_mean) {a=1; b=1; n=4; f*=0.5;}
  }
  else
  {
    if (jtv==_jump) {b=-1; n=2;}
    else if (jtv==_mean) {b=1; n=2;}
  }
  if (n==1) //no copy, shared pointer !
    return std::vector<bfPair>(1,bfPair(const_cast<BasicBilinearForm*>(static_cast<const BasicBilinearForm*>(this)),1.));
  std::vector<bfPair> bls(n);
  number_t k=0;
  OperatorOnUnknown opu(opus_p->opu()); opu.jumpType()=_nojump;
  OperatorOnUnknown opv(opus_p->opv()); opv.jumpType()=_nojump;
  MeshDomain* dualDom=const_cast<MeshDomain*>(domainu_p->meshDomain()->dualCrackDomain_p);
  bls[k++]=bfPair(new IntgBilinearForm(*domainu_p, opu, opus_p->algop(), opv, *intgMethod_p, _undefSymmetry),f);
  if (a!=0)
  {
    IntgBilinearForm* ilf=new IntgBilinearForm(*dualDom, opu, opus_p->algop(), opv, *intgMethod_p, _undefSymmetry);
    bls[k++]=bfPair(static_cast<BasicBilinearForm*>(ilf),f*a);
  }
  if (b!=0)
  {
    OperatorOnUnknown opv2(opv); opv2.domain()=dualDom;
    bls[k++]=bfPair(static_cast<BasicBilinearForm*>(new IntgBilinearForm(*domainu_p, opu, opus_p->algop(), opv2, *intgMethod_p, _undefSymmetry)),f*b);
  }
  if (c!=0)
  {
    OperatorOnUnknown opu2(opu); opu2.domain()=dualDom;
    bls[k]=bfPair(static_cast<BasicBilinearForm*>(new IntgBilinearForm(*domainu_p, opu2, opus_p->algop(), opv, *intgMethod_p, _undefSymmetry)),f*c);
  }
  return bls;
}

void IntgBilinearForm::print(std::ostream& os) const
{
  if (theVerboseLevel > 0)
  {
    os << message("form_intgbinfo", words("value", valueType()), domainu_p->name(), u_p->name(), v_p->name());
    os << ": " << asString();
    os << ", " << words("computation type",compuType);
    if (isoGeometric) os << ", iso-geometric";
    if (intgMethod_p != nullptr) os << ", " << *intgMethod_p;
  }
  if (theVerboseLevel > 1) { os << eol << "   "<<message("form_binfo2") << "\n     " << opu() << "     " << opv(); }
}

string_t IntgBilinearForm::asString() const
{
  string_t s="intg_"+domainu_p->name()+" "+opus_p->asString();
  return s;
}

// find symmetry property (_noSymmetry , _symmetric, _skewSymmetric, _selfAdjoint) of bilinear form, used to set symmetry parameter
// Here only simple cases. In more complex cases, the user may enforce symmetry by specifying sym argument in constructors of bilinear form
SymType IntgBilinearForm::setSymType() const
{
  symmetry=_noSymmetry;
  if (u_p->space() != v_p->space()) return symmetry;
  if (u_p != v_p && v_p !=u_p->dual_p()) return symmetry;
  const OperatorOnUnknown& ou=opu();
  const OperatorOnUnknown& ov=opv();
  if (ou.difOp().name() != ov.difOp().name()) return symmetry;
  number_t nc=ou.coefs().size();
  if (nc!=0) //operator with coefficients
  {
    if (ov.coefs().size()!=nc) return symmetry;
    for (number_t i=0;i<nc;i++)
    { if (ou.coefs()[i]!=ov.coefs()[i]) return symmetry; }
  }
  if (ou.strucType() == _scalar && ov.strucType() == _scalar)  {symmetry=_symmetric; return symmetry;}
  if (ou.leftOperand()==nullptr && ov.leftOperand()==nullptr) //no operand
  {
    if (ou.strucType() == _vector && ov.strucType() == _vector && algop() == _innerProduct) symmetry=_symmetric;
    if (ou.strucType() == _matrix && ov.strucType() == _matrix && algop() == _contractedProduct) symmetry=_symmetric;
  }
  return symmetry;
}

//-------------------------------------------------------------------------------
// member functions of DoubleIntgBilinearForm class
//-------------------------------------------------------------------------------
// copy constructor
DoubleIntgBilinearForm::DoubleIntgBilinearForm(const DoubleIntgBilinearForm& dibf)
  : BasicBilinearForm(dibf)
{
  intgMethod_p=dibf.intgMethod_p;  //shared pointer
  intgMethods=dibf.intgMethods;    //full copy
  kopus_p=nullptr; lckopus_p=nullptr;
  if (dibf.kopus_p!=nullptr) kopus_p=new KernelOperatorOnUnknowns(*dibf.kopus_p); //full copy
  else if (dibf.lckopus_p!=nullptr) lckopus_p=new LcKernelOperatorOnUnknowns(*dibf.lckopus_p); //full copy
}

DoubleIntgBilinearForm::~DoubleIntgBilinearForm()
{
  if (kopus_p!=nullptr) delete kopus_p;
  if (lckopus_p!=nullptr) delete lckopus_p;
}

//assign operator
DoubleIntgBilinearForm& DoubleIntgBilinearForm::operator=(const DoubleIntgBilinearForm& dibf)
{
  if (this == &dibf) return *this;
  if (kopus_p!=nullptr) delete kopus_p;
  if (lckopus_p!=nullptr) delete lckopus_p;
  if (dibf.kopus_p!=nullptr) kopus_p=new KernelOperatorOnUnknowns(*dibf.kopus_p); //full copy
  else if (dibf.lckopus_p!=nullptr) lckopus_p=new LcKernelOperatorOnUnknowns(*dibf.lckopus_p); //full copy
  intgMethod_p=dibf.intgMethod_p;  //shared pointer
  intgMethods=dibf.intgMethods;    //full copy
  compuType = dibf.compuType;
  u_p = dibf.u_p;
  v_p = dibf.v_p;
  domainu_p = dibf.domainu_p;
  domainv_p = dibf.domainv_p;
  symmetry = dibf.symmetry;
  isoGeometric=dibf.isoGeometric;
  return *this;
}

// constructors from IntegrationMethod
DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv,
                                               const KernelOperatorOnUnknowns& kopus, const IntegrationMethod& im, SymType st)
{
  const OperatorOnUnknown& opu = kopus.opu();
  const OperatorOnUnknown& opv = kopus.opv();
  kopus_p = new KernelOperatorOnUnknowns(kopus); //hard copy of opu/v to avoid memory trap (to be improved)
  lckopus_p = nullptr;
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  if (!im.isDoubleIM())
  {
    where("DoubleIntgBilinearForm::DoubleIntgBilinearForm");
    error("im_not_double");
  }
  intgMethod_p = &im; //pointer copy !!!
  if (im.type()==_HMatrixIM) setHMIntegrationMethods(); //check and update quadrature pointers
  setComputationType();
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv,
                                               const LcKernelOperatorOnUnknowns& lckopus, const IntegrationMethod& im,
                                               SymType st)
{
  lckopus_p = new LcKernelOperatorOnUnknowns(lckopus); //hard copy of opu/v to avoid memory trap (to be improved)
  kopus_p = nullptr;
  KernelOperatorOnUnknowns* kopus=lckopus_p->begin()->first;
  u_p = kopus->opu().unknown();
  v_p = kopus->opv().unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  if (!im.isDoubleIM())
  {
    where("DoubleIntgBilinearForm::DoubleIntgBilinearForm");
    error("im_not_double");
  }
  intgMethod_p = &im; //pointer copy !!!
  if (im.type()==_HMatrixIM) setHMIntegrationMethods(); //check and update quadrature pointers
  setComputationType();
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                                               AlgebraicOperator aop, const OperatorOnUnknown& opv,
                                               const IntegrationMethod& im, SymType st)
{
  OperatorOnKernel opk = OperatorOnKernel();
  kopus_p = new KernelOperatorOnUnknowns(opu, aop, opk, _product, opv);
  lckopus_p=nullptr;
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  if (!im.isDoubleIM())
  {
    where("DoubleIntgBilinearForm::DoubleIntgBilinearForm");
    error("im_not_double");
  }
  intgMethod_p = &im; //pointer copy !!!
  if (im.type()==_HMatrixIM) setHMIntegrationMethods(); //check and update quadrature pointers
  setComputationType();
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                                               AlgebraicOperator aopu, const OperatorOnKernel& opk, AlgebraicOperator aopv,
                                               const OperatorOnUnknown& opv, const IntegrationMethod& im, SymType st)
{
  kopus_p = new KernelOperatorOnUnknowns(opu, aopu, opk, aopv, opv);
  lckopus_p=nullptr;
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  if (!im.isDoubleIM())
  {
    where("DoubleIntgBilinearForm::DoubleIntgBilinearForm");
    error("im_not_double");
  }
  intgMethod_p = &im; //pointer copy !!!
  if (im.type()==_HMatrixIM) setHMIntegrationMethods(); //check and update quadrature pointers
  setComputationType();
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                                               AlgebraicOperator aopu, const Kernel& ker, AlgebraicOperator aopv, const
                                               OperatorOnUnknown& opv, const IntegrationMethod& im, SymType st)
{
  OperatorOnKernel opk = OperatorOnKernel(&ker, _id, _id, ker.valueType(), ker.strucType());
  kopus_p = new KernelOperatorOnUnknowns(opu, aopu, opk, aopv, opv);
  lckopus_p=nullptr;
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  if (!im.isDoubleIM())
  {
    where("DoubleIntgBilinearForm::DoubleIntgBilinearForm");
    error("im_not_double");
  }
  intgMethod_p = &im; //pointer copy !!!
  if (im.type()==_HMatrixIM) setHMIntegrationMethods(); //check and update quadrature pointers
  setComputationType();
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

// constructors from IntegrationMethods
DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv,
                                               const KernelOperatorOnUnknowns& kopus, const IntegrationMethods& ims,
                                               SymType st)
{
  kopus_p = new KernelOperatorOnUnknowns(kopus); //hard copy of opu/v to avoid memory trap (to be improved)
  lckopus_p=nullptr;
  u_p = kopus.opu().unknown();
  v_p = kopus.opv().unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  intgMethod_p = nullptr;
  intgMethods = ims;
  setIntegrationMethods();   //check and set integration methods
  setComputationType();
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv,
                                               const LcKernelOperatorOnUnknowns& lckopus, const IntegrationMethods& ims,
                                               SymType st)
{
  kopus_p=nullptr;
  lckopus_p = new LcKernelOperatorOnUnknowns(lckopus); //hard copy of opu/v to avoid memory trap (to be improved)
  KernelOperatorOnUnknowns* kopus=lckopus_p->begin()->first;
  u_p = kopus->opu().unknown();
  v_p = kopus->opv().unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  intgMethod_p = nullptr;
  intgMethods = ims;
  setIntegrationMethods();   //check and set integration methods
  setComputationType();
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                                               AlgebraicOperator aop, const OperatorOnUnknown& opv,
                                               const IntegrationMethods& ims, SymType st)
{
  OperatorOnKernel opk = OperatorOnKernel();
  kopus_p = new KernelOperatorOnUnknowns(opu, aop, opk, _product, opv);
  lckopus_p = nullptr;
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  intgMethod_p = nullptr;
  intgMethods = ims;
  setIntegrationMethods();   //check and set integration methods
  setComputationType();
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                                               AlgebraicOperator aopu, const OperatorOnKernel& opk, AlgebraicOperator aopv,
                                               const OperatorOnUnknown& opv, const IntegrationMethods& ims, SymType st)
{
  kopus_p = new KernelOperatorOnUnknowns(opu, aopu, opk, aopv, opv);
  lckopus_p = nullptr;
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  intgMethod_p = nullptr;
  intgMethods = ims;
  setIntegrationMethods();   //check and set integration methods
  setComputationType();
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                                               AlgebraicOperator aopu, const Kernel& ker, AlgebraicOperator aopv, const
                                               OperatorOnUnknown& opv, const IntegrationMethods& ims, SymType st)
{
  OperatorOnKernel opk = OperatorOnKernel(&ker, _id, _id, ker.valueType(), ker.strucType());
  kopus_p = new KernelOperatorOnUnknowns(opu, aopu, opk, aopv, opv);
  lckopus_p = nullptr;
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  intgMethod_p = nullptr;
  intgMethods = ims;
  setIntegrationMethods();   //check and set integration methods
  setComputationType();
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

// constructors from QuadRule
DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv,
                                               const KernelOperatorOnUnknowns& kopus,
                                               QuadRule qr1, number_t qo1, QuadRule qr2, number_t qo2, SymType st)
{
  kopus_p = new KernelOperatorOnUnknowns(kopus);
  lckopus_p = nullptr;
  u_p = kopus_p->opu().unknown();
  v_p = kopus_p->opv().unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  setIntegrationMethod(domu, domv, kopus, qr1, qo1, qr2, qo2);
  setComputationType();
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv,
                                               const LcKernelOperatorOnUnknowns& lckopus,
                                               QuadRule qr1, number_t qo1, QuadRule qr2, number_t qo2, SymType st)
{
  lckopus_p = new LcKernelOperatorOnUnknowns(lckopus);
  kopus_p = nullptr;
  KernelOperatorOnUnknowns* kopus=lckopus_p->begin()->first;
  u_p = kopus->opu().unknown();
  v_p = kopus->opv().unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  setIntegrationMethod(domu, domv, *kopus, qr1, qo1, qr2, qo2);
  //setComputationType();
  compuType = _IEComputation; // setComputationType() has to be generalized to deal with LcKernelOperatorOnUnknowns
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

// no explicit kernel, may be into opu or opv
DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                                               AlgebraicOperator aop, const OperatorOnUnknown& opv, QuadRule qr1, number_t
                                               qo1, QuadRule qr2, number_t qo2, SymType st)
{
  if (!checkConsistancy(opu, aop, opv)) {error("opu_badopus", words("algop", aop), "DoubleIntgBilinearForm");}
  OperatorOnKernel opker(0); // null pointer means K(x,y)=Id
  kopus_p = new KernelOperatorOnUnknowns(opu, aop, opker, _product, opv);
  lckopus_p = nullptr;
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  compuType = _IEComputation;
  setIntegrationMethod(domu, domv, *kopus_p, qr1, qo1, qr2, qo2);
  setComputationType();
  symmetry = st;
  if (symmetry==_undefSymmetry) setSymType();
  checkUnknowns();
}

//with Kernel operator
DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                                               AlgebraicOperator aopu, const OperatorOnKernel& opk, AlgebraicOperator aopv,
                                               const OperatorOnUnknown& opv, QuadRule qr1, number_t qo1, QuadRule qr2,
                                               number_t qo2, SymType st)
{
  //if (!checkConsistancy(opu, aop, opv)) {error("opu_badopus", words("algop", aop), "DoubleIntgBilinearForm");}
  kopus_p = new KernelOperatorOnUnknowns(opu, aopu, opk, aopv, opv);
  lckopus_p = nullptr;
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  setIntegrationMethod(domu, domv, *kopus_p, qr1, qo1, qr2, qo2);
  setComputationType();
  symmetry = st;
  if (st==_undefSymmetry) setSymType();
  checkUnknowns();
}

//with Kernel
DoubleIntgBilinearForm::DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                                               AlgebraicOperator aopu, const Kernel& ker, AlgebraicOperator aopv,
                                               const OperatorOnUnknown& opv, QuadRule qr1, number_t qo1, QuadRule qr2,
                                               number_t qo2, SymType st)
{
  //if (!checkConsistancy(opu, aop, opv)) {error("opu_badopus", words("algop", aop), "DoubleIntgBilinearForm");}
  OperatorOnKernel opk = OperatorOnKernel(&ker, _id, _id, ker.valueType(), ker.strucType());
  kopus_p = new KernelOperatorOnUnknowns(opu, aopu, opk, aopv, opv);
  lckopus_p = nullptr;
  u_p = opu.unknown();
  v_p = opv.unknown();
  domainu_p = &domu;
  domainv_p = &domv;
  setIntegrationMethod(domu, domv, *kopus_p, qr1, qo1, qr2, qo2);
  setComputationType();
  symmetry = st;
  if (st==_undefSymmetry) setSymType();
  checkUnknowns();
}

//set computation type one of
//   _IEComputation (standard Kernel)
//   _IESPComputation (TensorKernel)
//   _IEextComputation (standard Kernel and extended domain)
//   _IEHmatrixComputation (HMatrix storage)
void DoubleIntgBilinearForm::setComputationType()
{
  compuType = _IEComputation;
  if (kopus_p!=nullptr && kopus_p->opker().kernelp()!=nullptr && kopus_p->opker().kernelp()->type() == _tensorKernel) compuType = _IESPComputation;
  if (intgMethod_p!=nullptr && intgMethod_p->imType==_HMatrixIM) compuType = _IEHmatrixComputation;
  bool ext = false;
  if (domainu_p->meshDomain()!=nullptr && domainu_p->meshDomain()->isSideDomain() && kopus_p->opu().extensionRequired()) ext=true;
  if (domainv_p->meshDomain()!=nullptr && domainv_p->meshDomain()->isSideDomain() && kopus_p->opv().extensionRequired()) ext=true;
  if (ext)
  {
    if (compuType == _IEComputation) compuType = _IEextComputation;
    else
    {
      string_t mes = "bilinear form "+asString()+" requiring ";
      if (compuType == _IESPComputation) mes+= "spectral computation";
      else mes+= " HMatrix computation ";
      mes+=" on extended domain is not yet available";
      error("free_error",mes);
    }
  }
}

// set quadrature order regarding order of interpolation and user quadrature parameters
// convention: x lives in domain of v, y lives in domain of u
void DoubleIntgBilinearForm::setIntegrationMethod(const GeomDomain& domu, const GeomDomain& domv,
                                                  const KernelOperatorOnUnknowns& kopus,
                                                  QuadRule qr2, number_t qo2, QuadRule qr1, number_t qo1)
{
  intgMethod_p = nullptr;
  if (domu.domType() != _meshDomain)
  {
    where("DoubleIntgBilinearForm::setIntegrationMethod");
    error("domain_notmesh", domu.name(), words("domain type",domu.domType()));
    return;   //no integration method set
  }
  if (domv.domType() != _meshDomain)
  {
    where("DoubleIntgBilinearForm::setIntegrationMethod");
    error("domain_notmesh", domv.name(), words("domain type",domv.domType()));
    return;   //no integration method set
  }
  const  MeshDomain* mdomv=domv.meshDomain(), *mdomu=domu.meshDomain();
  number_t ordx=0, ordy=0;
  if (kopus_p!=nullptr)
  {
    const OperatorOnUnknown& opu = kopus_p->opu();
    const OperatorOnUnknown& opv = kopus_p->opv();
    ordy=opu.degree();
    ordx=opv.degree();
  }
  else if (lckopus_p!=nullptr)
  {
    for (cit_opkuvval ito=lckopus_p->begin(); ito!=lckopus_p->end(); ++ito)
    {
      KernelOperatorOnUnknowns* kopus = ito->first;
      ordy=std::max(ordy,kopus->opu().degree());
      ordx=std::max(ordx,kopus->opv().degree());
    }
  }
  ordx+=2; ordy+=2; //add two to take into account Kernel
  // add order of geometric element (>0)
  ordx+=domv.order()-1; ordy+=domu.order()-1; // no increment for 'linear' element because the jacobian is constant
  if (qr1 != _defaultRule && qo1 > ordx) ordx = qo1; // keep user choice
  if (qr2 != _defaultRule && qo2 > ordy) ordy = qo2; // keep user choice
  intgMethod_p = nullptr;
  if (&domu==&domv && kopus_p!=nullptr && kopus_p->opker().kernelp()!=nullptr && kopus_p->opker().kernelp()->singularType!=_notsingular)
  {
    where("DoubleIntgBilinearForm::setIntegrationMethod");
    warning("singular_quad_expected_w");
  }
  SingleIM* imx = new QuadratureIM(mdomv->shapeTypes, qr1, ordx);  //create single quadrature for x (v-domain)
  SingleIM* imy = new QuadratureIM(mdomu->shapeTypes, qr2, ordy);  //create single quadrature for y (u-domain)
  intgMethod_p = new ProductIM(imx, imy);  //create quadrature product
  intgMethods.push_back(IntgMeth(*intgMethod_p,_allFunction, theRealMax));
}

/*
void DoubleIntgBilinearForm::setIntegrationMethods() ...
implemented in termUtils.cpp because of a cyclic dependance of libraries
*/

//! return the value type of the bilinear form
ValueType DoubleIntgBilinearForm::valueType() const
{
  if (kopus_p!=nullptr) return kopus_p->valueType();
  if (lckopus_p!=nullptr)
  {
    for (cit_opkuvval ito=lckopus_p->begin(); ito!=lckopus_p->end(); ++ito)
    { if (ito->first->valueType()==_complex || ito->second.imag()!=0.) return _complex; }
  }
  return _real;
}


/*! find symmetry property (_noSymmetry , _symmetric, _skewSymmetric, _selfAdjoint) of bilinear form, used to set symmetry parameter
   Here only simple cases. In more complex cases, the user may enforce symmetry by specifying sym argument in constructors of bilinear form
   To be improved...
*/
// to be improved
SymType DoubleIntgBilinearForm::setSymType() const
{
  symmetry=_noSymmetry;
  if (u_p->space() != v_p->space()) return symmetry;
  if (kopus_p!=nullptr)
  {
    const OperatorOnUnknown& opu = kopus_p->opu();
    const OperatorOnUnknown& opv = kopus_p->opv();
    if (opu.difOp().name() != opv.difOp().name()) return symmetry;
    const OperatorOnKernel& opker=kopus_p->opker();
    if (opker.kernelp()!=nullptr)
    {
      if (opker.kernelp()->symmetry == _noSymmetry) return symmetry;
      if (opker.xdifOp().name() != opker.ydifOp().name()) return symmetry;
      if (opker.xydifOp()!=_id && opker.xydifOp()!=_nxdotny_times) return symmetry;
    }
    if (opu.strucType() == _scalar && opv.strucType() == _scalar)  symmetry=_symmetric;
  }
  return symmetry;
}

//clone of the linear form
BasicBilinearForm* DoubleIntgBilinearForm::clone() const
{
  return new DoubleIntgBilinearForm(*this);
}

// set (change) the unknowns of IntgBilinearForm
void DoubleIntgBilinearForm::setUnknowns(const Unknown& u, const Unknown& v)
{
  u_p=&u; v_p=&v;
  const_cast<KernelOperatorOnUnknowns*>(kopus_p)->setUnknowns(u,v);
}

void DoubleIntgBilinearForm::print(std::ostream& os) const
{
  if (theVerboseLevel== 0) return;
  os << message("form_intg2binfo", words("value", valueType()), domainu_p->name(), domainv_p->name(), u_p->name(), v_p->name());
  os <<": "<< asString();
  os<<", "<<words("computation type",compuType);
  if (isoGeometric) os<<", iso-geometric";
  if (theVerboseLevel < 2) return;
  if (intgMethod_p != nullptr) os <<", "<< *intgMethod_p;
  if (!intgMethods.empty())
  {
    if (intgMethods.intgMethods.size()==1) os <<", "<< intgMethods;
    else os <<eol<<"   Quadratures: "<< intgMethods;
  }
  os << eol;
  if (kopus_p!=nullptr)
  {
    os << "   "<<message("form_binfo2") << "\n" ;
    os << "    " << opu();
    os << "    operation " << words("algop", algopu()) << "\n";
    if (!opker().voidKernel())
      {
        os << "    " << opker();
        os << "    operation " << words("algop", algopv()) << "\n";
      }
    else os << "    no kernel\n";
    os << "    " << opv();
    if (kopus().priorityIsRight()) os << "  right priority: opu op (opker op opv)\n";
    else os << "  left priority: (opu op opker) op opv\n";
  }
  else if (lckopus_p!=nullptr)
  {

  }
}

string_t DoubleIntgBilinearForm::asString() const
{
  string_t s="intg_"+domainv_p->name()+" intg_"+domainu_p->name()+" ";
  if (kopus_p!=nullptr) s+=kopus_p->asString();
  else if (lckopus_p!=nullptr) s+=lckopus_p->asString();
  return s;
}

//-------------------------------------------------------------------------------
// member functions of MatrixBilinearForm class
//-------------------------------------------------------------------------------

MatrixBilinearForm::~MatrixBilinearForm()
{
  if (newMatrixEntry_ && matrix_p != nullptr) delete matrix_p;
}

//-------------------------------------------------------------------------------
// member functions of SuBilinearForm class
//-------------------------------------------------------------------------------
// constructors, destructor and assignment operator
// to be safe, bilinear forms are copied
SuBilinearForm::SuBilinearForm(const SuBilinearForm& subf)
{
  for (number_t n = 0; n < subf.bfs_.size(); n++)
    { bfs_.push_back(bfPair(subf.bfs_[n].first->clone(), subf.bfs_[n].second)); }
  symmetry =subf.symmetry;
}

SuBilinearForm& SuBilinearForm::operator=(const SuBilinearForm& subf)
{
  if (&subf == this) { return *this; } //same object
  for (number_t n = 0; n < bfs_.size(); n++) //delete current linear forms
  { delete bfs_[n].first; }
  bfs_.clear();
  for (number_t n = 0; n < subf.bfs_.size(); n++) //copy of lf
  { bfs_.push_back(bfPair(subf.bfs_[n].first->clone(), subf.bfs_[n].second)); }
  symmetry =subf.symmetry;
  return *this;
}

SuBilinearForm::~SuBilinearForm()
{
  for (number_t n = 0; n < bfs_.size(); n++) //delete current linear forms
  { delete bfs_[n].first; }
}

// return the type of the bilinear form
LinearFormType SuBilinearForm::type() const
{
  if (bfs_.size() == 0) { return _undefLf; }
  if (bfs_.size() > 1)  { return _linearCombination; }
  return bfs_[0].first->type();
}

// return the value type (real or complex) of the linear form
ValueType SuBilinearForm::valueType() const
{
  for (number_t n = 0; n < bfs_.size(); n++)
  { if (bfs_[n].first->valueType() == _complex || bfs_[n].second.imag() != 0) { return _complex; } }
  return _real;
}

// return the unknown of the bilinear form, given by the unknown of the first item
const Unknown* SuBilinearForm::up() const
{
  if (bfs_.size() == 0) { return nullptr; }
  return &(bfs_[0].first->up());
}

const Unknown* SuBilinearForm::vp() const
{
  if (bfs_.size() == 0) { return nullptr; }
  return &(bfs_[0].first->vp());
}

// return the domain of the FIRST basic bilinear form in SuBilinearForm, be cautious with this function !
const GeomDomain* SuBilinearForm::dom_up() const
{
  if (bfs_.size() == 0) { return nullptr; }
  return &(bfs_[0].first->dom_up());
}

const GeomDomain* SuBilinearForm::dom_vp() const
{
  if (bfs_.size() == 0) { return nullptr; }
  return &(bfs_[0].first->dom_vp());
}

// return the space of the linear form
const Space* SuBilinearForm::uSpace() const
{
  if (up() == nullptr) { return nullptr; }
  return up()->space();
}

// return the space of the linear form
const Space* SuBilinearForm::vSpace() const
{
  if (vp() == nullptr) { return nullptr; }
  return vp()->space();
}

// return symmetry property (_noSymmetry , _symmetric, _skewSymmetric, _selfAdjoint) of bilinear form
// and update symmetry property
SymType SuBilinearForm::setSymType() const
{
  symmetry=_noSymmetry;
  if (bfs_.size()==0) return symmetry;
  symmetry = bfs_[0].first->symmetry;
  if (symmetry==_noSymmetry) return symmetry;
  for (number_t n = 1; n < bfs_.size(); n++)
  { if (bfs_[n].first->symmetry != symmetry) {symmetry=_noSymmetry; return _noSymmetry;} }
  return symmetry;
}

// true if at least one basic bilinear form has jump like operator (useful to split it)
bool SuBilinearForm::hasJump() const
{
  for (number_t n = 0; n < bfs_.size(); n++)
  { if (bfs_[n].first->hasJump()) return true; }
  return false;
}

// true if at least one basic bilinear form is a UserBilinearForm of type _DGComputation
bool SuBilinearForm::hasDGUserBF() const
{
  for (number_t n = 0; n < bfs_.size(); n++)
  {
    UserBilinearForm* ubf=bfs_[n].first->asUserForm();
    if (ubf!=nullptr && ubf->computationType()==_DGComputation) return true;
  }
  return false;
}

// true if a DG SubilinearForm (defined on a sides domain and has jump/mean operator or has a UserBilinearForm of type _DGComputation)
bool SuBilinearForm::isDG() const
{
  return (dom_up()->isSidesDomain() && (hasJump() || hasDGUserBF()));
}


// set the unknowns of subilinear form
void SuBilinearForm::setUnknowns(const Unknown& u, const Unknown& v)
{
  std::vector<bfPair>::iterator it= bfs_.begin();
  for (; it!=bfs_.end(); ++it) it->first->setUnknowns(u,v);
}

// check consitancy of Unknown
bool SuBilinearForm::checkConsistancy(const SuBilinearForm& subf) const
{
  const Unknown* u1 = up(), *u2 = subf.up();
  if (u1 != nullptr && u2 != nullptr)
  { if (u1->parent() != u2->parent()) {error("form_badlc"); return false;} }
  u1 = vp();
  u2 = subf.vp();
  if (u1 != nullptr && u2 != nullptr)
  { if (u1->parent() != u2->parent()) {error("form_badlc"); return false;} }
  return true;
}

SuBilinearForm& SuBilinearForm::operator+=(const SuBilinearForm& subf)
{
  checkConsistancy(subf); // check unknown compatibility
  for (number_t n = 1; n <= subf.bfs_.size(); n++) // add linear forms from lf
  { bfs_.push_back(bfPair(subf(n).first->clone(), subf(n).second)); }
  setSymType();  //update symmetry property
  return *this;
}

SuBilinearForm& SuBilinearForm::operator-=(const SuBilinearForm& subf)
{
  checkConsistancy(subf); // check unknown compatibility
  for (number_t n = 1; n <= subf.bfs_.size(); n++) // add linear forms from lf
  { bfs_.push_back(bfPair(subf(n).first->clone(), -subf(n).second)); }
  setSymType();  //update symmetry property
  return *this;
}

SuBilinearForm& SuBilinearForm::operator *=(const complex_t& c)
{
  for (number_t n = 0; n < bfs_.size(); n++)  { bfs_[n].second *= c; }
  return *this;
}

SuBilinearForm& SuBilinearForm::operator /=(const complex_t& c)
{
  if (std::abs(c) < theZeroThreshold) { error("form_divideby0", c, "SuBilinearForm::operator /"); }
  for (number_t n = 0; n < bfs_.size(); n++)  { bfs_[n].second /= c; }
  return *this;
}


//-------------------------------------------------------------------------------
// external functions of SuBilinearForm class
//-------------------------------------------------------------------------------
// general algebraic operations on linear combinations of bilinear forms
SuBilinearForm operator-(const SuBilinearForm& subf)
{
  SuBilinearForm nsubf(subf);
  for (number_t n = 1; n <= nsubf.size(); n++) { nsubf(n).second *= -1; }
  return nsubf;
}

SuBilinearForm operator+(const SuBilinearForm& subf1, const SuBilinearForm& subf2)
{
  subf1.checkConsistancy(subf2); // check compatibility of unknowns
  SuBilinearForm nsubf(subf1); // create a copy of lf1
  for (number_t n = 1; n <= subf2.bfs().size(); n++) // add linear forms from lf2
  { nsubf.bfs().push_back(bfPair(subf2(n).first->clone(), subf2(n).second)); }
  nsubf.setSymType();  //update symmetry property
  return nsubf;
}

SuBilinearForm operator-(const SuBilinearForm& subf1, const SuBilinearForm& subf2)
{
  subf1.checkConsistancy(subf2); // check compatibility of unknowns
  SuBilinearForm nsubf(subf1); // create a copy of lf1
  for (number_t n = 1; n <= subf2.bfs().size(); n++) // add linear forms from lf2
  { nsubf.bfs().push_back(bfPair(subf2(n).first->clone(), -subf2(n).second)); }
  nsubf.setSymType();  //update symmetry property
  return nsubf;
}

SuBilinearForm operator*(const complex_t& c, const SuBilinearForm& subf)
{
  SuBilinearForm nsubf;
  for (number_t n = 1; n <= subf.bfs().size(); n++) // add linear forms from lf2
  { nsubf.bfs().push_back(bfPair(subf(n).first->clone(), c * subf(n).second)); }
  return nsubf;
}

SuBilinearForm operator*(const SuBilinearForm& subf, const complex_t& c)
{
  return c * subf;
}

SuBilinearForm operator/(const SuBilinearForm& subf, const complex_t& c)
{
  if (std::abs(c) < theZeroThreshold) { error("form_divideby0", c, "SuBilinearForm::operator /"); }
  return (1. / c) * subf;
}

// print SuBilinearForm on stream
std::ostream& operator<<(std::ostream& os, const SuBilinearForm& subf)
{
  subf.print(os);
  return os;
}

void SuBilinearForm::print(std::ostream& os) const
{
  if (theVerboseLevel > 0)
  {
    if (bfs_.size() == 0)
    {
      os << message("form_isempty");
      return;
    }
    if (bfs_.size() > 0)
    {
      //os << message("form_blcinfo", words("value", valueType()), up()->parent()->name(), vp()->parent()->name());
      os << message("form_blcinfo", words("value", valueType()), up()->name(), vp()->name());
      os<<", "<<words("symmetry", symType())<< "\n";
    }
  }
  if (theVerboseLevel > 1)
  {
    for (number_t n = 0; n < bfs_.size(); n++)
    {
      os << "   " << bfs_[n].second << " * ";
      bfs_[n].first->print(os);
    }
  }
}

string_t SuBilinearForm::asString() const //! as symbolic string
{
  string_t s="";
  for (number_t n = 0; n < bfs_.size(); n++)
  {
    s+= coefAsString(n==0,bfs_[n].second);
    s+= bfs_[n].first->asString();
  }
  return s;
}


//-------------------------------------------------------------------------------
// member functions of BilinearForm class
//-------------------------------------------------------------------------------
// constructors
SuBilinearForm::SuBilinearForm(const std::vector<bfPair>& bfs)
{
  symmetry =_undefSymmetry;
  for (number_t n = 0; n < bfs.size(); n++)
  { bfs_.push_back(bfPair(bfs[n].first->clone(), bfs[n].second)); }
  setSymType();
}
// from a bilinear combination
BilinearForm::BilinearForm(const SuBilinearForm& subf)
{
  //mlcbf_[uvPair(subf.up()->parent(), subf.vp()->parent())] = subf;
  // note: unknowns indexation uses parent unknowns
  //        in order to component unknown be associated to its parent item
  // change to be consistent with choice of TermVector
  mlcbf_[uvPair(subf.up(), subf.vp())] = subf;
}

// various functionalities
bool BilinearForm::isEmpty() const
{
  return (mlcbf_.size() == 0);
}
bool BilinearForm::singleUnknown() const
{
  return (mlcbf_.size() < 2);
}

// set the unknowns of bilinear forms (only for single unknown bf)
void BilinearForm::setUnknowns(const Unknown& u, const Unknown& v)
{
  if (mlcbf_.size() > 1)
  {
    where("BilinearForm::setUnknowns");
    error("im_not_single");
  }
  it_mublc it = mlcbf_.begin();
  const Unknown* u0=it->first.first, *v0=it->first.second;
  if (u0==&u && v0==&v) return;  //nothing to change
  it->second.setUnknowns(u,v);
  mlcbf_[uvPair(&u,&v)]=it->second;   //add new uvpair to map
  mlcbf_.erase(uvPair(u0,v0));
}

// accessors
// to the linear form associated to an unknown
SuBilinearForm& BilinearForm::operator[](const uvPair& uv)
{
  //uvPair uvpar(uv.first->parent(), uv.second->parent());
  uvPair uvpar(uv);
  if (mlcbf_.find(uvpar) == mlcbf_.end()) { error("form_nolf", "bi", "BilinearForm::operator[]"); }
  return mlcbf_[uvpar];
}
const SuBilinearForm& BilinearForm::operator[](const uvPair& uv) const
{
  //uvPair uvpar(uv.first->parent(), uv.second->parent());
  uvPair uvpar(uv);
  cit_mublc it = mlcbf_.find(uvpar);
  if (it == mlcbf_.end()) { error("form_nolf", "bi", "BilinearForm::operator[]"); }
  return it->second;
}

// to the n-th basic linear form associated to an unknown
BasicBilinearForm& BilinearForm::operator()(const Unknown& u, const Unknown& v, number_t n)
{
  //it_mublc it = mlcbf_.find(uvPair(u.parent(), v.parent()));
  cit_mublc it = mlcbf_.find(uvPair(&u,&v));
  if (it == mlcbf_.end()) { error("form_nolf", "bi", "BilinearForm::operator()"); }
  const SuBilinearForm& subf = it->second;
  if (n < 1 || n > subf.size()) { error("form_outbounds", "bi", n, subf.size(), "BilinearForm::operator()"); }
  return *(subf(n).first);
}
const BasicBilinearForm& BilinearForm::operator()(const Unknown& u, const Unknown& v, number_t n) const
{
  //cit_mublc it = mlcbf_.find(uvPair(u.parent(), v.parent()));
  cit_mublc it = mlcbf_.find(uvPair(&u, &v));
  if (it == mlcbf_.end()) { error("form_nolf", "bi", "BilinearForm::operator()"); }
  const SuBilinearForm& subf = it->second;
  if (n < 1 || n > subf.size()) { error("form_outbounds", "bi", n, subf.size(), "BilinearForm::operator()"); }
  return *(subf(n).first);
}

// return first linear combination
const SuBilinearForm& BilinearForm::first() const
{
  if (isEmpty()) { error("form_nolf", "bi", "BilinearForm::first()"); }
  cit_mublc it = mlcbf_.begin();
  return it->second;
}

SuBilinearForm& BilinearForm::first()
{
  if (isEmpty()) { error("form_nolf", "bi", "BilinearForm::first()"); }
  it_mublc it = mlcbf_.begin();
  return it->second;
}

// access to SuBiLinearForm pointers related to unknowns
const SuBilinearForm* BilinearForm::subLfp(const uvPair& uv) const
{
  cit_mublc it=mlcbf_.find(uv);
  if (it!=mlcbf_.end()) return &(it->second);
  return nullptr;
}

// access to SuBiLinearForm pointers related to unknowns
SuBilinearForm* BilinearForm::subLfp(const uvPair& uv)
{
  it_mublc it=mlcbf_.find(uv);
  if (it!=mlcbf_.end()) return &(it->second);
  return nullptr;
}

// to change the symmetry property (only for single unknown bf)
SymType& BilinearForm::symType()
{
  if (!singleUnknown()) error("bform_not_single_unknown");
  if (isEmpty()) { error("form_nolf", "bi", "BilinearForm::symType()"); }
  return mlcbf_.begin()->second.symType();
}

// return a copy of the (lc)linear form associated to an unknown
// different from operator[]
BilinearForm BilinearForm::operator()(const Unknown& u, const Unknown& v) const
{
  //cit_mublc it = mlcbf_.find(uvPair(u.parent(), v.parent()));
  cit_mublc it = mlcbf_.find(uvPair(&u, &v));
  if (it == mlcbf_.end()) { error("form_nolf", "bi", "BilinearForm::operator()"); }
  return BilinearForm(it->second);
}

// internal algebraic operations
BilinearForm& BilinearForm::operator +=(const BilinearForm& bf)
{
  for (cit_mublc it = bf.mlcbf_.begin(); it != bf.mlcbf_.end(); it++) // loop on linear combinations of bf
  {
    const SuBilinearForm& subf = it->second;
    //uvPair uv(subf.up()->parent(), subf.vp()->parent()); // use parent to associate subf to its parent item
    uvPair uv(subf.up(), subf.vp());
    it_mublc it2 = mlcbf_.find(uv);
    if (it2 == mlcbf_.end()) { mlcbf_[uv] = subf; } // no linear combination with such unknown, create a new entry in map
    else { it2->second += subf; } // add linear combination to item it2
  }
  return *this;
}
BilinearForm& BilinearForm::operator -=(const BilinearForm& bf)
{
  for (cit_mublc it = bf.mlcbf_.begin(); it != bf.mlcbf_.end(); it++) // loop on linear combinations of bf
  {
    const SuBilinearForm& subf = it->second;
    //uvPair uv(subf.up()->parent(), subf.vp()->parent()); // use parent to associate subf to its parent item
    uvPair uv(subf.up(), subf.vp());
    it_mublc it2 = mlcbf_.find(uv);
    if (it2 == mlcbf_.end()) { mlcbf_[uv] = -subf; } // no linear combination with such unknowns, create a new entry in map
    else { it2->second -= subf; } // substract linear combination to item it2
  }
  return *this;
}
BilinearForm& BilinearForm::operator *=(const complex_t& c)
{
  for (it_mublc it = mlcbf_.begin(); it != mlcbf_.end(); it++) { it->second *= c; }
  return *this;
}
BilinearForm& BilinearForm::operator /=(const complex_t& c)
{
  for (it_mublc it = mlcbf_.begin(); it != mlcbf_.end(); it++) { it->second /= c; }
  return *this;
}

// print utility
std::ostream& operator<<(std::ostream& os, const BilinearForm& bf)
{
  bf.print(os);
  return os;
}

void BilinearForm::print(std::ostream& os) const
{
  if (theVerboseLevel < 1) { return; }
  if (isEmpty())
  {
    os << message("form_isempty") << "\n";
    return;
  }
  if (singleUnknown())
  {
    first().print(os);
    return;
  }
  os << message("form_mublinfo") << "\n";
  if (isTestMode)
  {
    std::map<string_t, SuBilinearForm> mlcbf_sorted;
    for (cit_mublc it = mlcbf_.begin(); it != mlcbf_.end(); it++)
    {
      std::stringstream ss;
      ss << "(" << it->second.up()->name() << "," << it->second.vp()->name() << ")";
      mlcbf_sorted[ss.str()]=it->second;
    }
    for (std::map<string_t, SuBilinearForm>::const_iterator it = mlcbf_sorted.begin(); it != mlcbf_sorted.end(); it++)
    {
      os << "   " << words("unknown") << " " << it->first << ": ";
      it->second.print(os);
    }
  }
  else
  {
    for (cit_mublc it = mlcbf_.begin(); it != mlcbf_.end(); it++)
    {
      os << "   " << words("unknown") << " (" << it->second.up()->name() << "," << it->second.vp()->name() << ") : ";
      it->second.print(os);
    }
  }
}

string_t BilinearForm::asString() const
{
  if (isEmpty()) return "";
  cit_mublc it = mlcbf_.begin();
  string_t s=it->second.asString();
  if (singleUnknown()) return s;
  it++;
  for (cit_mublc it = mlcbf_.begin(); it != mlcbf_.end(); it++)
  { s+=" + "+it->second.asString(); }
  return s;
}

//-------------------------------------------------------------------------------
// external functions of BilinearForm class
//-------------------------------------------------------------------------------

//utility to split a collection of bilinearform
std::vector<bfPair> split(std::vector<bfPair>& bfs)
{
  std::vector<bfPair> split_bfs;
  std::vector<bfPair>::iterator itb= bfs.begin(),it;
  for (; itb!=bfs.end(); ++itb)
  {
    BasicBilinearForm* b=itb->first;
    if (b->hasJump() && !b->dom_up().isSidesDomain())  //split jump/mean operator if not on a SidesDomain
    {
      std::vector<bfPair> bs = b->split();
      for (it=bs.begin(); it!=bs.end(); ++it)
        split_bfs.push_back(std::make_pair(it->first,it->second*itb->second));
    }
    else split_bfs.push_back(*itb);
  }
  return split_bfs;
}

// user functions like constructors of IntgBilinearForm
// -----------------------------------------------------------------------------------------

void intgBFBuildParam(const Parameter& p, real_t& bound, bool& isogeom, IntegrationMethod*& meth,
                      IntegrationMethods& meths, QuadRule& qr1, number_t& qo1, QuadRule& qr2, number_t& qo2,
                      SymType& st, const GeomDomain*& extdomu, const GeomDomain*& extdomv)
{
  ParameterKey key=p.key();
  switch (key)
  {
    case _pk_bound:
    {
      switch (p.type())
      {
        case _integer:
        {
          bound=real_t(p.get_n());
          break;
        }
        case _real:
        {
          bound=p.get_r();
          break;
        }
        default:
          error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_isogeo:
    {
      isogeom=true;
      break;
    }
    case _pk_method:
    {
      if (p.type() == _pointerIntegrationMethod) { meth=const_cast<IntegrationMethod*>(reinterpret_cast<const IntegrationMethod*>(p.get_p())); }
      else if (p.type() == _pointerIntegrationMethods) { meths=*reinterpret_cast<const IntegrationMethods*>(p.get_p()); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      break;
    }
    case _pk_order:
    case _pk_order1:
    {
      if (p.type() == _integer) { qo1=p.get_n(); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      break;
    }
    case _pk_order2:
    {
      if (p.type() == _integer) { qo2=p.get_n(); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      break;
    }
    case _pk_quad:
    case _pk_quad1:
    {
      switch (p.type())
      {
        case _integer:
        case _enumQuadRule:
          qr1=QuadRule(p.get_n());
          break;
        default: error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_quad2:
    {
      switch (p.type())
      {
        case _integer:
        case _enumQuadRule:
          qr2=QuadRule(p.get_n());
          break;
        default: error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_symmetry:
    {
      switch (p.type())
      {
        case _integer:
        case _enumSymType:
          st=SymType(p.get_n());
          break;
        default: error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_extension_domain:
    {
      if (p.type() == _pointerGeomDomain) { extdomv=static_cast<const GeomDomain*>(p.get_p()); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      break;
    }
    case _pk_extension_domain_v:
    {
      if (p.type() == _pointerGeomDomain) { extdomv=static_cast<const GeomDomain*>(p.get_p()); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      break;
    }
    default:
      error("bform_unexpected_param_key",words("param key",key));
  }
}

void intgBFParamCompatibility(const ParameterKey& key, std::set<ParameterKey>& usedParams)
{
  // user must use _quad or _quad1, not both
  if (key==_pk_quad && usedParams.find(_pk_quad1) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_quad1)); }
  if (key==_pk_quad1 && usedParams.find(_pk_quad) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_quad)); }
  // user must use _quad or _method, not both
  if (key==_pk_quad && usedParams.find(_pk_method) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_method)); }
  if (key==_pk_method && usedParams.find(_pk_quad) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_quad)); }
  // user must use _quad1 or _method, not both
  if (key==_pk_quad1 && usedParams.find(_pk_method) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_method)); }
  if (key==_pk_method && usedParams.find(_pk_quad1) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_quad1)); }
  // user must use _order or _method, not both
  if (key==_pk_order && usedParams.find(_pk_method) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_method)); }
  if (key==_pk_method && usedParams.find(_pk_order) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_order)); }
  // user must use _order1 or _method, not both
  if (key==_pk_order1 && usedParams.find(_pk_method) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_method)); }
  if (key==_pk_method && usedParams.find(_pk_order1) != usedParams.end())
  { error("param_conflict", words("param key",key), words("param key",_pk_order1)); }
}

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const std::vector<Parameter>& ps)
{
  trace_p->push("intg(Domain, OperatorOnUnknowns, Parameters)");
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_method);
  params.insert(_pk_quad);
  params.insert(_pk_order);
  params.insert(_pk_symmetry);
  params.insert(_pk_isogeo);

  real_t bound=0.;
  bool isogeom=false;
  IntegrationMethod* meth=nullptr;
  IntegrationMethods meths;
  QuadRule qr=_defaultRule;
  number_t qo=0;
  SymType st=_undefSymmetry;
  const GeomDomain* extdomu=nullptr;
  const GeomDomain* extdomv=nullptr;

  for (number_t i=0; i < ps.size(); ++i)
  {
    intgBFBuildParam(ps[i], bound, isogeom, meth, meths, qr, qo, qr, qo, st, extdomu, extdomv);

    ParameterKey key=ps[i].key();
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("bform_unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);

    intgBFParamCompatibility(key, usedParams);
  }

  // (quad, order) has to be set (or method or methods)
  if (params.find(_pk_quad) != params.end() && params.find(_pk_order) == params.end()) { error("param_missing","order"); }
  if (params.find(_pk_order) != params.end() && params.find(_pk_quad) == params.end()) { error("param_missing","quad"); }

  BasicBilinearForm* bbf;
  if (usedParams.find(_pk_method) != usedParams.end())
  {
    // key _method was given
    if (meth!=nullptr) { bbf = static_cast<BasicBilinearForm*>(new IntgBilinearForm(dom, opus, *meth, st)); }
    else { error("param_badtype",words("value",_pointerIntegrationMethods),words("param key",_pk_method)); }
  }
  else
  {
    // key _method was not given, we consider _quad and _order (possibly with their default values)
    bbf = static_cast<BasicBilinearForm*>(new IntgBilinearForm(dom, opus, qr, qo, st));
  }
  bbf->isoGeometric = isogeom;
  if(extdomu!=nullptr)
  {
    if(!dom.isSideDomain() || extdomu->dim() != dom.dim() + 1)
       warning("free_warning","extension_domain key is not used, "+dom.name()+" is not a side domain");
    else
       bbf->extDom_up() = const_cast<const GeomDomain*>(&dom.extendDomain(false,*extdomu));
  }
  if(extdomv!=nullptr && extdomv!=extdomu)
  {
    if(!dom.isSideDomain() || extdomv->dim() != dom.dim() + 1)
       warning("free_warning","extension_domain_v key is not used, "+dom.name()+" is not a side domain");
    else
       bbf->extDom_vp() = const_cast<const GeomDomain*>(&dom.extendDomain(false,*extdomv));
  }
  if(extdomu!=nullptr && extdomu==extdomv) bbf->extDom_vp()=extdomu;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  trace_p->pop();
  return BilinearForm(blc);
}

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus)
{
  std::vector<Parameter> ps(0);
  return intg(dom, opus, ps);
}

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1)
{
  std::vector<Parameter> ps = {p1};
  return intg(dom, opus, ps);
}

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps = {p1, p2};
  return intg(dom, opus, ps);
}

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3)
{
  std::vector<Parameter> ps = {p1, p2, p3};
  return intg(dom, opus, ps);
}

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4};
  return intg(dom, opus, ps);
}

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5};
  return intg(dom, opus, ps);
}

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6};
  return intg(dom, opus, ps);
}

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7};
  return intg(dom, opus, ps);
}

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                  const Parameter& p8)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7, p8};
  return intg(dom, opus, ps);
}

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                  const Parameter& p8, const Parameter& p9)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7, p8, p9};
  return intg(dom, opus, ps);
}

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                  const Parameter& p8, const Parameter& p9, const Parameter& p10)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  return intg(dom, opus, ps);
}

BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++)
  {
    bf += cit->second * intg(dom, *cit->first);
  }
  return bf;
}

BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++)
  {
    bf += cit->second * intg(dom, *cit->first, p1);
  }
  return bf;
}

BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++)
  {
    bf += cit->second * intg(dom, *cit->first, p1, p2);
  }
  return bf;
}

BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++)
  {
    bf += cit->second * intg(dom, *cit->first, p1, p2, p3);
  }
  return bf;
}

BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++)
  {
    bf += cit->second * intg(dom, *cit->first, p1, p2, p3, p4);
  }
  return bf;
}

BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++)
  {
    bf += cit->second * intg(dom, *cit->first, p1, p2, p3, p4, p5);
  }
  return bf;
}

BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++)
  {
    bf += cit->second * intg(dom, *cit->first, p1, p2, p3, p4, p5, p6);
  }
  return bf;
}

BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++)
  {
    bf += cit->second * intg(dom, *cit->first, p1, p2, p3, p4, p5, p6, p7);
  }
  return bf;
}

BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                  const Parameter& p8)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++)
  {
    bf += cit->second * intg(dom, *cit->first, p1, p2, p3, p4, p5, p6, p7, p8);
  }
  return bf;
}

BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                  const Parameter& p8, const Parameter& p9)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++)
  {
    bf += cit->second * intg(dom, *cit->first, p1, p2, p3, p4, p5, p6, p7, p8, p9);
  }
  return bf;
}

BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                  const Parameter& p8, const Parameter& p9, const Parameter& p10)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++)
  {
    bf += cit->second * intg(dom, *cit->first, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
  }
  return bf;
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const std::vector<Parameter>& ps)
{
  trace_p->push("intg(Domain, Domain, KernelOperatorOnUnknowns, Parameters)");
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_method);
  params.insert(_pk_quad);
  params.insert(_pk_quad1);
  params.insert(_pk_quad2);
  params.insert(_pk_order);
  params.insert(_pk_order1);
  params.insert(_pk_order2);
  params.insert(_pk_symmetry);
  params.insert(_pk_isogeo);

  real_t bound=0.;
  bool isogeom=false;
  IntegrationMethod* meth=nullptr;
  IntegrationMethods meths;
  QuadRule qr1=_defaultRule, qr2=_defaultRule;
  number_t qo1=0, qo2=0;
  SymType st=_undefSymmetry;
  const GeomDomain* extdomu=nullptr; // not yet managed
  const GeomDomain* extdomv=nullptr; // not yet managed

  for (number_t i=0; i < ps.size(); ++i)
  {
    intgBFBuildParam(ps[i], bound, isogeom, meth, meths, qr1, qo1, qr2, qo2, st, extdomu, extdomv);

    ParameterKey key=ps[i].key();
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("bform_unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);

    intgBFParamCompatibility(key, usedParams);
  }

  // _quad and _order are to be used together
  if (params.find(_pk_quad) != params.end() && params.find(_pk_order) == params.end()) { error("param_missing","order"); }
  if (params.find(_pk_order) != params.end() && params.find(_pk_quad) == params.end()) { error("param_missing","quad"); }
  // _quad1 and _order1 are to be used together
  if (params.find(_pk_quad1) != params.end() && params.find(_pk_order1) == params.end()) { error("param_missing","order1"); }
  if (params.find(_pk_order1) != params.end() && params.find(_pk_quad1) == params.end()) { error("param_missing","quad1"); }
  // _quad2 and _order2 are to be used together
  if (params.find(_pk_quad2) != params.end() && params.find(_pk_order2) == params.end()) { error("param_missing","order2"); }
  if (params.find(_pk_order2) != params.end() && params.find(_pk_quad2) == params.end()) { error("param_missing","quad2"); }
  // _quad2 cannot be used without _quad1
  if (params.find(_pk_quad2) != params.end() && params.find(_pk_quad1) == params.end()) { error("param_missing","quad1"); }

  BasicBilinearForm* bbf;
  if (usedParams.find(_pk_method) == usedParams.end())
  {
    // key _method was not given, we consider _quad1, _quad2, _order1, _order2 (possibly with their default values)
    BasicBilinearForm* bbf;
    bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, kopus, qr1, qo1, qr2, qo2, st));
    bbf->isoGeometric = isogeom;
    SuBilinearForm blc(bbf->symmetry);
    blc.bfs().push_back(bfPair(bbf, 1.));
    trace_p->pop();
    return BilinearForm(blc);
  }
  else
  {
    // key _method was given
    if (meth!=nullptr)
    {
      // we consider a IntegrationMethod
      bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, kopus, *meth, st));
    }
    else
    {
      // we consider a IntegrationMethods
      bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, kopus, meths, st));
    }
  }
  bbf->isoGeometric = isogeom;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  trace_p->pop();
  return BilinearForm(blc);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus)
{
  std::vector<Parameter> ps(0);
  return intg(domv, domu, KernelOperatorOnUnknowns(opus.opu(), opus.algop(), OperatorOnKernel(), _product, opus.opv()), ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1)
{
  std::vector<Parameter> ps = {p1};
  return intg(domv, domu, KernelOperatorOnUnknowns(opus.opu(), opus.algop(), OperatorOnKernel(), _product, opus.opv()), ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps = {p1, p2};
  return intg(domv, domu, KernelOperatorOnUnknowns(opus.opu(), opus.algop(), OperatorOnKernel(), _product, opus.opv()), ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps = {p1, p2, p3};
  return intg(domv, domu, KernelOperatorOnUnknowns(opus.opu(), opus.algop(), OperatorOnKernel(), _product, opus.opv()), ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4};
  return intg(domv, domu, KernelOperatorOnUnknowns(opus.opu(), opus.algop(), OperatorOnKernel(), _product, opus.opv()), ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5};
  return intg(domv, domu, KernelOperatorOnUnknowns(opus.opu(), opus.algop(), OperatorOnKernel(), _product, opus.opv()), ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6};
  return intg(domv, domu, KernelOperatorOnUnknowns(opus.opu(), opus.algop(), OperatorOnKernel(), _product, opus.opv()), ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7};
  return intg(domv, domu, KernelOperatorOnUnknowns(opus.opu(), opus.algop(), OperatorOnKernel(), _product, opus.opv()), ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7, p8};
  return intg(domv, domu, KernelOperatorOnUnknowns(opus.opu(), opus.algop(), OperatorOnKernel(), _product, opus.opv()), ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7, p8, p9};
  return intg(domv, domu, KernelOperatorOnUnknowns(opus.opu(), opus.algop(), OperatorOnKernel(), _product, opus.opv()), ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  return intg(domv, domu, KernelOperatorOnUnknowns(opus.opu(), opus.algop(), OperatorOnKernel(), _product, opus.opv()), ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus)
{
  std::vector<Parameter> ps(0);
  return intg(domv, domu, kopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1)
{
  std::vector<Parameter> ps = {p1};
  return intg(domv, domu, kopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps = {p1, p2};
  return intg(domv, domu, kopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps = {p1, p2, p3};
  return intg(domv, domu, kopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4};
  return intg(domv, domu, kopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5};
  return intg(domv, domu, kopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6};
  return intg(domv, domu, kopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7};
  return intg(domv, domu, kopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7, p8};
  return intg(domv, domu, kopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7, p8, p9};
  return intg(domv, domu, kopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  return intg(domv, domu, kopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const std::vector<Parameter>& ps)
{
  trace_p->push("intg(Domain, Domain, LcKernelOperatorOnUnknowns, Parameters)");
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_method);
  params.insert(_pk_quad);
  params.insert(_pk_quad1);
  params.insert(_pk_quad2);
  params.insert(_pk_order);
  params.insert(_pk_order1);
  params.insert(_pk_order2);
  params.insert(_pk_symmetry);
  params.insert(_pk_isogeo);

  real_t bound=0.;
  bool isogeom=false;
  IntegrationMethod* meth=nullptr;
  IntegrationMethods meths;
  QuadRule qr1=_defaultRule, qr2=_defaultRule;
  number_t qo1=0, qo2=0;
  SymType st=_undefSymmetry;
  const GeomDomain* extdomu=nullptr;
  const GeomDomain* extdomv=nullptr;

  for (number_t i=0; i < ps.size(); ++i)
  {
    intgBFBuildParam(ps[i], bound, isogeom, meth, meths, qr1, qo1, qr2, qo2, st, extdomu, extdomv);

    ParameterKey key=ps[i].key();
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("bform_unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);

    intgBFParamCompatibility(key, usedParams);
  }

  // _quad and _order are to be used together
  if (params.find(_pk_quad) != params.end() && params.find(_pk_order) == params.end()) { error("param_missing","order"); }
  if (params.find(_pk_order) != params.end() && params.find(_pk_quad) == params.end()) { error("param_missing","quad"); }
  // _quad1 and _order1 are to be used together
  if (params.find(_pk_quad1) != params.end() && params.find(_pk_order1) == params.end()) { error("param_missing","order1"); }
  if (params.find(_pk_order1) != params.end() && params.find(_pk_quad1) == params.end()) { error("param_missing","quad1"); }
  // _quad2 and _order2 are to be used together
  if (params.find(_pk_quad2) != params.end() && params.find(_pk_order2) == params.end()) { error("param_missing","order2"); }
  if (params.find(_pk_order2) != params.end() && params.find(_pk_quad2) == params.end()) { error("param_missing","quad2"); }
  // _quad2 cannot be used without _quad1
  if (params.find(_pk_quad2) != params.end() && params.find(_pk_quad1) == params.end()) { error("param_missing","quad1"); }

  if (usedParams.find(_pk_method) == usedParams.end())
  {
    // key _method was not given, the linear combination is split
    BilinearForm bf;
    LcKernelOperatorOnUnknowns::const_iterator cit;
    for (cit = lckopus.begin(); cit != lckopus.end(); cit++)
    {
      bf += cit->second * intg(domv, domu, *cit->first, ps);
    }
    trace_p->pop();
    return bf;
  }
  else
  {
    // key _method was given
    if (meth!=nullptr)
    {
      if (meth->imType == _CollinoIM)
      {
        // the linear combination on intg is to be analysed as a single element (see CollinoIM)
        BasicBilinearForm* bbf;
        bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, lckopus, *meth, st));
        bbf->isoGeometric = isogeom;
        SuBilinearForm blc(bbf->symmetry);
        blc.bfs().push_back(bfPair(bbf, 1.));
        trace_p->pop();
        return BilinearForm(blc);
      }
      else
      {
        // IntegrationMethod is not CollinoIM, then the linear combination is split
        BilinearForm bf;
        LcKernelOperatorOnUnknowns::const_iterator cit;
        for (cit = lckopus.begin(); cit != lckopus.end(); cit++)
        {
          bf += cit->second * intg(domv, domu, *cit->first, ps);
        }
        trace_p->pop();
        return bf;
      }
    }
    else
    {
      // IntegrationMethods is given, the linear combination is split
      BilinearForm bf;
      LcKernelOperatorOnUnknowns::const_iterator cit;
      for (cit = lckopus.begin(); cit != lckopus.end(); cit++)
      {
        bf += cit->second * intg(domv, domu, *cit->first, ps);
      }
      trace_p->pop();
      return bf;
    }
  }
  trace_p->pop();
  return BilinearForm();
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus)
{
  std::vector<Parameter> ps(0);
  return intg(domv, domu, lckopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1)
{
  std::vector<Parameter> ps = {p1};
  return intg(domv, domu, lckopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps = {p1, p2};
  return intg(domv, domu, lckopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps = {p1, p2, p3};
  return intg(domv, domu, lckopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4};
  return intg(domv, domu, lckopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5};
  return intg(domv, domu, lckopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6};
  return intg(domv, domu, lckopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7};
  return intg(domv, domu, lckopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7, p8};
  return intg(domv, domu, lckopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7, p8, p9};
  return intg(domv, domu, lckopus, ps);
}

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  return intg(domv, domu, lckopus, ps);
}

// downcast of BasicBilinearForm
// -----------------------------
IntgBilinearForm* BasicBilinearForm::asIntgForm()
{
  if (type() != _intg) error("downcast_failure", "IntgBilinearForm");
  return static_cast<IntgBilinearForm*>(this);
}
const IntgBilinearForm* BasicBilinearForm::asIntgForm() const
{
  if (type() != _intg) error("downcast_failure", "IntgBilinearForm");
  return static_cast<const IntgBilinearForm*>(this);
}
DoubleIntgBilinearForm* BasicBilinearForm::asDoubleIntgForm()
{
  if (type() != _doubleIntg) error("downcast_failure", "DoubleIntgBilinearForm");
  return static_cast<DoubleIntgBilinearForm*>(this);
}
const DoubleIntgBilinearForm* BasicBilinearForm::asDoubleIntgForm() const
{
  if (type() != _doubleIntg) error("downcast_failure", "DoubleIntgBilinearForm");
  return static_cast<const DoubleIntgBilinearForm*>(this);
}
UserBilinearForm* BasicBilinearForm::asUserForm()
{
  if (type() != _userLf) error("downcast_failure", "UserBilinearForm");
  return static_cast<UserBilinearForm*>(this);
}
const UserBilinearForm* BasicBilinearForm::asUserForm() const
{
  if (type() != _userLf) error("downcast_failure", "UserBilinearForm");
  return static_cast<const UserBilinearForm*>(this);
}

// general algebraic operations on bilinear forms
// ----------------------------------------------
const BilinearForm& operator+(const BilinearForm& bf)
{
  return bf;
}

BilinearForm operator-(const BilinearForm& bf)
{
  BilinearForm nbf(bf);
  return nbf *= -1;
}

BilinearForm operator+(const BilinearForm& bf1, const BilinearForm& bf2)
{
  BilinearForm nbf(bf1);
  return nbf += bf2;
}

BilinearForm operator-(const BilinearForm& bf1, const BilinearForm& bf2)
{
  BilinearForm nbf(bf1);
  return nbf -= bf2;
}

BilinearForm operator*(const int_t& i, const BilinearForm& bf)
{
  BilinearForm nbf(bf);
  return nbf *= real_t(i);
}

BilinearForm operator*(const int& i, const BilinearForm& bf)
{
  BilinearForm nbf(bf);
  return nbf *= real_t(i);
}

BilinearForm operator*(const number_t& n, const BilinearForm& bf)
{
  BilinearForm nbf(bf);
  return nbf *= real_t(n);
}

BilinearForm operator*(const real_t& r, const BilinearForm& bf)
{
  BilinearForm nbf(bf);
  return nbf *= r;
}

BilinearForm operator*(const complex_t& c, const BilinearForm& bf)
{
  BilinearForm nbf(bf);
  return nbf *= c;
}

BilinearForm operator*(const BilinearForm& bf, const int_t& i)
{
  BilinearForm nbf(bf);
  return nbf *= real_t(i);
}

BilinearForm operator*(const BilinearForm& bf, const int& i)
{
  BilinearForm nbf(bf);
  return nbf *= real_t(i);
}

BilinearForm operator*(const BilinearForm& bf, const number_t& n)
{
  BilinearForm nbf(bf);
  return nbf *= real_t(n);
}

BilinearForm operator*(const BilinearForm& bf, const real_t& r)
{
  BilinearForm nbf(bf);
  return nbf *= r;
}

BilinearForm operator*(const BilinearForm& bf, const complex_t& c)
{
  BilinearForm nbf(bf);
  return nbf *= c;
}

BilinearForm operator/(const BilinearForm& bf, const int_t& i)
{
  if (std::abs(real_t(i)) < theZeroThreshold) { error("form_divideby0", complex_t(real_t(i),0.), "BilinearForm::operator /"); }
  BilinearForm nbf(bf);
  return nbf /= real_t(i);
}

BilinearForm operator/(const BilinearForm& bf, const int& i)
{
  if (std::abs(real_t(i)) < theZeroThreshold) { error("form_divideby0", complex_t(i,0.), "BilinearForm::operator /"); }
  BilinearForm nbf(bf);
  return nbf /= real_t(i);
}

BilinearForm operator/(const BilinearForm& bf, const number_t& n)
{
  if (n==0) { error("form_divideby0", complex_t(real_t(n),0.), "BilinearForm::operator /"); }
  BilinearForm nbf(bf);
  return nbf /= real_t(n);
}

BilinearForm operator/(const BilinearForm& bf, const real_t& r)
{
  if (std::abs(r) < theZeroThreshold) { error("form_divideby0", complex_t(r,0.), "BilinearForm::operator /"); }
  BilinearForm nbf(bf);
  return nbf /= r;
}

BilinearForm operator/(const BilinearForm& bf, const complex_t& c)
{
  if (std::abs(c) < theZeroThreshold) { error("form_divideby0", c, "BilinearForm::operator /"); }
  BilinearForm nbf(bf);
  return nbf /= c;
}

//UserBilinearForm functions
//==========================
UserBilinearForm::UserBilinearForm(const GeomDomain& dom, const Unknown& u, const Unknown& v, BFFunction bffun,
                                   ComputationType ct, SymType st, bool rij, bool rno, const IntegrationMethod& im)
{
  bffun_=bffun;
  u_p=&u; v_p=&v;
  domainu_p=&dom; domainv_p=&dom;
  compuType=ct;  symmetry=st;
  intgMethod_p=im.clone();
  extDomainu_p=nullptr;extDomainv_p=nullptr;
  requireInvJacobian_ = rij;
  requireNormal_ = rno;
}


UserBilinearForm::UserBilinearForm(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Unknown& v,
                                   BFFunction bffun, ComputationType ct,
                                   SymType st, bool rij, bool rno, const IntegrationMethod& im)
{
  bffun_=bffun;
  u_p=&u; v_p=&v;
  domainu_p=&domu; domainv_p=&domv;
  compuType=ct;  symmetry=st;
  intgMethod_p=im.clone();
  extDomainu_p=nullptr;extDomainv_p=nullptr;
  requireInvJacobian_ = rij;
  requireNormal_ = rno;
}

UserBilinearForm::UserBilinearForm(const UserBilinearForm& ubf)
{
  bffun_=ubf.bffun_;
  u_p=ubf.u_p; v_p=ubf.v_p;
  domainu_p=ubf.domainu_p; domainv_p=ubf.domainv_p;
  compuType=ubf.compuType;  symmetry=ubf.symmetry;
  if (ubf.intgMethod_p!=nullptr) intgMethod_p=ubf.intgMethod_p->clone();
  extDomainu_p=nullptr;extDomainv_p=nullptr;
  requireInvJacobian_ = ubf.requireInvJacobian_;
  requireNormal_ = ubf.requireNormal_;
}
UserBilinearForm::~UserBilinearForm()
{
  delete intgMethod_p;
}
ValueType UserBilinearForm::valueType() const
{
   BFComputationData bfd;
   bffun_(bfd);
   return bfd.valueType();
}
BasicBilinearForm* UserBilinearForm::clone() const
{
  return new UserBilinearForm(*this);
}

BilinearForm userBilinearForm(const GeomDomain& dom, const Unknown& u, const Unknown& v, BFFunction bffun, ComputationType ct,
                     SymType st, bool reqIJ, bool reqN, const IntegrationMethod& im)
{
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new UserBilinearForm(dom,u,v,bffun,ct,st,reqIJ,reqN,im));
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}

BilinearForm userBilinearForm(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Unknown& v, BFFunction bffun, ComputationType ct,
                     SymType st, bool reqIJ, bool reqN, const IntegrationMethod& im)
{
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new UserBilinearForm(domu,domv,u,v,bffun,ct,st,reqIJ,reqN,im));
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
//!< print utility

//!< interpret as string for print purpose
string_t UserBilinearForm::asString() const
{
  string_t s="user bf on "+domainu_p->name()+" involving "+u_p->name()+", "+v_p->name();;
  return s;
}
void UserBilinearForm::print(std::ostream& os) const
{
  if (theVerboseLevel== 0) return;
  string_t dv=domainv_p->name();
  if (compuType!=_IEComputation && compuType!=_IESPComputation && compuType!=_IEextComputation && compuType!=_IEHmatrixComputation) dv="";
  os << message("form_userbinfo", words("value", valueType()), domainu_p->name(),dv , u_p->name(), v_p->name());
  os<<", "<<words("computation type",compuType);
  if (requireInvJacobian_) os<<", inverse of jacobian is required";
  if (requireNormal_) os<<", normal vector is required";
  if (theVerboseLevel < 2) return;
  if (intgMethod_p != nullptr) os <<", "<< *intgMethod_p;
}

//----------------------------------------
// DEPRECATED (to be removed)
//----------------------------------------

BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop, const OperatorOnUnknown& opv,
                  const IntegrationMethod& im, bool isogeo, SymType st)
{
  warning("deprecated", "intg(Domain, ..., IntegrationMethod, bool, SymType)", "intg(Domain, ..., _method=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new IntgBilinearForm(dom, opu, aop, opv, im, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop, const OperatorOnUnknown& opv,
                  const IntegrationMethod& im, SymType st)
{
  return intg(dom, opu, aop, opv, im, false, st);
}
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const IntegrationMethod& im, bool isogeo, SymType st)
{
  return intg(dom, opus.opu(), opus.algop(), opus.opv(), im, isogeo, st);
}
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const IntegrationMethod& im,SymType st)
{
  return intg(dom, opus.opu(), opus.algop(), opus.opv(), im, false, st);
}
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const IntegrationMethod& im, bool isogeo, SymType st)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++)
  {
    bf += cit->second * intg(dom, *cit->first, im, isogeo, st);
  }
  return bf;
}
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const IntegrationMethod& im, SymType st)
{
  return intg(dom, lcopus, im, false, st);
}
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop, const OperatorOnUnknown& opv,
                  QuadRule qr, number_t qo, bool isogeo, SymType st)
{
  warning("deprecated", "intg(Domain, ..., QuadRule, Number, bool, SymType)", "intg(Domain, ..., _quad=?, _order=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new IntgBilinearForm(dom, opu, aop, opv, qr, qo, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop, const OperatorOnUnknown& opv,
                  QuadRule qr, number_t qo, SymType st)
{
    return intg(dom, opu, aop, opv, qr, qo, false, st);
}
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, QuadRule qr, number_t qo, bool isogeo, SymType st)
{
  return intg(dom, opus.opu(), opus.algop(), opus.opv(), qr, qo, isogeo, st);
}
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, QuadRule qr, number_t qo,SymType st)
{
  return intg(dom, opus.opu(), opus.algop(), opus.opv(), qr, qo, false, st);
}
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, QuadRule qr, number_t qo, bool isogeo, SymType st)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++ )
  {
    bf += cit->second * intg(dom, *cit->first, qr, qo, isogeo, st);
  }
  return bf;
}
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, QuadRule qr, number_t qo, SymType st)
{
  return intg(dom, lcopus, qr, qo, false, st);
}
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop, const OperatorOnUnknown& opv,
                  bool isogeo, SymType st)
{
  return intg(dom, opu, aop, opv, _defaultRule, 0, isogeo, st);
}
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop, const OperatorOnUnknown& opv,
                  SymType st)
{
  return intg(dom, opu, aop, opv, _defaultRule, 0, false, st);
}
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, bool isogeo, SymType st)
{
  return intg(dom, opus.opu(), opus.algop(), opus.opv(), _defaultRule, 0, isogeo, st);
}
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, SymType st)
{
  return intg(dom, opus.opu(), opus.algop(), opus.opv(), _defaultRule, 0, false, st);
}
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, bool isogeo, SymType st)
{
  BilinearForm bf;
  LcOperatorOnUnknowns::const_iterator cit;
  for (cit = lcopus.begin(); cit != lcopus.end(); cit++ )
  {
    bf += cit->second * intg(dom, *cit->first, isogeo, st);
  }
  return bf;
}
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, SymType st)
{
  return intg(dom, lcopus, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, const IntegrationMethod& im, bool isogeo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., IntegrationMethod, bool, SymType)", "intg(Domain, Domain, ..., _method=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, opu, aop, opv, im, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, const IntegrationMethod& im, SymType st)
{
  return intg(domv, domu, opu, aop, opv, im, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const IntegrationMethod& im, bool isogeo, SymType st)
{
  return intg(domv, domu, opus.opu(), opus.algop(), opus.opv(), im, isogeo, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const IntegrationMethod& im, SymType st)
{
  return intg(domv, domu, opus.opu(), opus.algop(), opus.opv(), im, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethod& im, bool isogeo, SymType st)
{
  OperatorOnKernel opker(ker);
  return intg(domv, domu, opu, aopu, opker, aopv, opv, im, isogeo, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethod& im, SymType st)
{
  OperatorOnKernel opker(ker);
  return intg(domv, domu, opu, aopu, opker, aopv, opv, im, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethod& im, bool isogeo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., IntegrationMethod, bool, SymType)", "intg(Domain, Domain, ..., _method=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, opu, aopu, opker, aopv, opv, im, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethod& im,  SymType st)
{
  return intg(domv, domu, opu, aopu, opker, aopv, opv, im, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const IntegrationMethod& im, bool isogeo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., IntegrationMethod, SymType)", "intg(Domain, Domain, ..., _method=?, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, kopus, im, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const IntegrationMethod& im, SymType st)
{
  return intg(domv, domu, kopus, im, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const IntegrationMethod& im, bool isogeo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., IntegrationMethod, bool, SymType)", "intg(Domain, Domain, ..., _method=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, lckopus, im, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const IntegrationMethod& im, SymType st)
{
   return intg(domv, domu, lckopus, im, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, const IntegrationMethods& ims, bool isogeo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., IntegrationMethods, bool, SymType)", "intg(Domain, Domain, ..., _method=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, opu, aop, opv, ims, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, const IntegrationMethods& ims, SymType st)
{
  return intg(domv, domu, opu, aop, opv, ims, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const IntegrationMethods& ims, bool isogeo, SymType st)
{
  return intg(domv, domu, opus.opu(), opus.algop(), opus.opv(), ims, isogeo, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const IntegrationMethods& ims, SymType st)
{
  return intg(domv, domu, opus.opu(), opus.algop(), opus.opv(), ims, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethods& ims, bool isogeo, SymType st)
{
  OperatorOnKernel opker(ker);
  return intg(domv, domu, opu, aopu, opker, aopv, opv, ims, isogeo, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethods& ims, SymType st)
{
  OperatorOnKernel opker(ker);
  return intg(domv, domu, opu, aopu, opker, aopv, opv, ims, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethods& ims, bool isogeo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., IntegrationMethods, bool, SymType)", "intg(Domain, Domain, ..., _method=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, opu, aopu, opker, aopv, opv, ims, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethods& ims, SymType st)
{
  return intg(domv, domu, opu, aopu, opker, aopv, opv, ims, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const IntegrationMethods& ims, bool isogeo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., IntegrationMethods, bool, SymType)", "intg(Domain, Domain, ..., _method=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, kopus, ims, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const IntegrationMethods& ims, SymType st)
{
  return intg(domv, domu, kopus, ims, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const IntegrationMethods& ims, bool isogeo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., IntegrationMethods, bool, SymType)", "intg(Domain, Domain, ..., _method=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, lckopus, ims, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const IntegrationMethods& ims, SymType st)
{
  return intg(domv, domu, lckopus, ims, false, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, bool isogeo, QuadRule qr, number_t qo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., bool, QuadRule, Number, SymType)", "intg(Domain, Domain, ..., _quad=?, _order=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, opu, aop, opv, qr, qo, qr, qo, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, QuadRule qr, number_t qo, SymType st)
{
  return intg(domv, domu, opu, aop, opv, false, qr, qo, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  bool isogeo, QuadRule qr, number_t qo, SymType st)
{
  return intg(domv, domu, opus.opu(), opus.algop(), opus.opv(), isogeo, qr, qo, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  QuadRule qr, number_t qo, SymType st)
{
  return intg(domv, domu, opus.opu(), opus.algop(), opus.opv(), false, qr, qo, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  bool isogeo, QuadRule qr, number_t qo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., bool, QuadRule, Number, SymType)", "intg(Domain, Domain, ..., _quad=?, _order=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, opu, aopu, opker, aopv, opv, qr, qo, qr, qo, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  QuadRule qr, number_t qo, SymType st)
{
  return intg(domv, domu, opu, aopu, opker, aopv, opv, false, qr, qo, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  bool isogeo, QuadRule qr, number_t qo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., bool, QuadRule, Number, SymType)", "intg(Domain, Domain, ..., _quad=?, _order=?, _isogeo, _symmetry=?)");
  OperatorOnKernel opker(ker);
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, opu, aopu, opker, aopv, opv, qr, qo, qr, qo, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  QuadRule qr, number_t qo, SymType st)
{
  return intg(domv, domu, opu, aopu, ker, aopv, opv, false, qr, qo, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  bool isogeo, QuadRule qr, number_t qo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., bool, QuadRule, Number, SymType)", "intg(Domain, Domain, ..., _quad=?, _order=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, kopus, qr, qo, qr, qo, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  QuadRule qr, number_t qo, SymType st)
{
  return intg(domv, domu, kopus, false, qr, qo, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  bool isogeo, QuadRule qr, number_t qo, SymType st)
{
  warning("deprecated", "intg(Domain, Domain, ..., bool, QuadRule, Number, SymType)", "intg(Domain, Domain, ..., _quad=?, _order=?, _isogeo, _symmetry=?)");
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, lckopus, qr, qo, qr, qo, st));
  bbf->isoGeometric = isogeo;
  SuBilinearForm blc(bbf->symmetry);
  blc.bfs().push_back(bfPair(bbf, 1.));
  return BilinearForm(blc);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  QuadRule qr, number_t qo, SymType st)
{
  return intg(domv, domu, lckopus, false, qr, qo, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, bool isogeo, SymType st)
{
  return intg(domv, domu, opu, aop, opv, isogeo, _defaultRule, 0, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, SymType st)
{
  return intg(domv, domu, opu, aop, opv, false, _defaultRule, 0, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus, bool isogeo, SymType st)
{
  return intg(domv, domu, opus, isogeo, _defaultRule, 0, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus, SymType st)
{
  return intg(domv, domu, opus, false, _defaultRule, 0, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv, bool isogeo, SymType st)
{
  return intg(domv, domu, opu, aopu, opker, aopv, opv, isogeo, _defaultRule, 0, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv, SymType st)
{
  return intg(domv, domu, opu, aopu, opker, aopv, opv, false, _defaultRule, 0, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv, bool isogeo, SymType st)
{
  return intg(domv, domu, opu, aopu, ker, aopv, opv, isogeo, _defaultRule, 0, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv, SymType st)
{
  return intg(domv, domu, opu, aopu, ker, aopv, opv, false, _defaultRule, 0, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus, bool isogeo, SymType st)
{
  return intg(domv, domu, kopus, isogeo, _defaultRule, 0, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus, SymType st)
{
  return intg(domv, domu, kopus, false, _defaultRule, 0, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus, bool isogeo, SymType st)
{
  return intg(domv, domu, lckopus, isogeo, _defaultRule, 0, st);
}
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus, SymType st)
{
  return intg(domv, domu, lckopus, false, _defaultRule, 0, st);
}

} // end of namespace xlifepp
