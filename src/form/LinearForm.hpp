/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LinearForm.hpp
  \author E. Lunéville
  \since 23 mar 2012
  \date  01 apr 2012

  \brief Definition of the xlifepp::LinearForm classes

  This file declares all the classes related to linear form managment.
  The more general linear form under consideration is a list of linear combinations of basic linear forms
  the list is indexed by the unknown of the linear combination
  basic linear forms are, for the moment, integral or double integral type
  The linear form may have multiple unknowns but a linear combination is related to a unique unknown

  xlifepp::LinearForm is the end user class, it deals with a map of <xlifepp::Unknown*,xlifepp::LCLinearForm>
  xlifepp::SuLinearForm deals with linear combination of basic linear forms (single unknown), a vector of pairs of <xlifepp::BasicLinearForm*,xlifepp::complex_t>

  xlifepp::BasicLinearForm is the mother class (abstract) of classes
    - xlifepp::IntgLinearForm for single integral linear form (generally in FEM method)
    - xlifepp::DoubleIngLinearForm for double integral linear form (generally in BEM method)

  As example:
    LinearForm l=intg(Omega,f*u);                   // a single unknown (u) linear form
    LinearForm l=intg(Omega,f*u)+2*intg(Omega,g|v); // a multiple unknowns (u,v) linear form

  all linear algebraic operations are allowed on xlifepp::LinearForm objects
*/


#ifndef LINEAR_FORM_HPP
#define LINEAR_FORM_HPP

#include "config.h"
#include "space.h"
#include "operator.h"
#include "finiteElements.h"

#include <utility>

namespace xlifepp
{
//forward declaration
class IntgLinearForm;
class DoubleIntgLinearForm;
class BilinearFormAsLinearForm;
class EssentialCondition;

/*!
  \class BasicLinearForm
  describes the base class of basic linear forms on a space
  It is an abstract class
*/
class BasicLinearForm
{
  protected:
    const Unknown* u_p = nullptr;                  //!< pointer to unknown
    const GeomDomain* domain_p = nullptr;          //!< pointer to geometric domain of the integral
    mutable const GeomDomain* extDomain_p=nullptr; //!< pointer to extended geometric domain (may be nullptr)
  public:
    ComputationType compuType;      //!< computation type (_FEComputation, _IEComputation, _SPComputation, ...)
    bool isoGeometric;              //!< if true, use isoGeometric computation requiring parametrization (default false)

    BasicLinearForm(const Unknown* up=nullptr, const GeomDomain* dup=nullptr) //! basic and default constructor
     : u_p(up), compuType(_undefComputation), isoGeometric(false) {}
    virtual ~BasicLinearForm() {}      //!< virtual destructor

    //accessor
    const Unknown& up() const          //! return the unknown
     {return *u_p;}
    const GeomDomain* domain() const  //! return the pointer to the domain
     {return domain_p;}
    const GeomDomain& extDom() const   //! return the extended domain u as reference
     {return *extDomain_p;}
    const GeomDomain* extDom_p() const //! to access to extended domain u pointer
     {return extDomain_p;}
    const GeomDomain*& extDom_p()      //! to write extended domain u pointer
     {return extDomain_p;}
    ComputationType computationType() const    //! return the computation type
     {return compuType;}

    //virtual function
    virtual BasicLinearForm* clone() const = 0;   //!< clone of the linear form
    virtual LinearFormType type() const = 0;      //!< return the type of the linear form
    virtual ValueType valueType() const = 0;      //!< return the value type of the linear form
    virtual StrucType strucType() const = 0;      //!< return the struct type of the linear form
    IntgLinearForm* asIntgForm();                 //!< downcast to IntgLinearForm*
    const IntgLinearForm* asIntgForm() const;     //!< downcast to const IntgLinearForm*
    DoubleIntgLinearForm* asDoubleIntgForm();     //!< downcast to DoubleIntgLinearForm*
    const DoubleIntgLinearForm* asDoubleIntgForm() const;           //!< downcast to const DoubleIntgLinearForm*
    BilinearFormAsLinearForm* asBilinearAsLinearForm();             //!< downcast to BilinearFormAsLinearForm*
    const BilinearFormAsLinearForm* asBilinearAsLinearForm() const; //!< downcast to const BilinearFormAsLinearForm*

    virtual string_t asString() const = 0;         //!< interpret as string for print purpose
    virtual void print(std::ostream&) const = 0;   //!< print utility
    virtual void print(PrintStream& os) const =0;
};

/*!
   \class IntgLinearForm
   describes a linear form based on a single integral
*/
class IntgLinearForm : public BasicLinearForm
{
  protected:
    const OperatorOnUnknown* opu_p;                 //!< pointer to the operator on unknown
    const IntegrationMethod* intgMethod_p;          //!< pointer to integration method
  public:
    IntegrationMethods intgMethods;                 //!< list of integration methods

    IntgLinearForm(const GeomDomain& dom, const OperatorOnUnknown& opu,
                   const IntegrationMethods& ims);                 //!< basic constructor from OperatorOnUnknown and intg methods
    IntgLinearForm(const GeomDomain& dom, const OperatorOnUnknown& opu,
                   const IntegrationMethod& im);                  //!< basic constructor from OperatorOnUnknown and an intg method
    IntgLinearForm(const GeomDomain& dom, const OperatorOnUnknown& opu,
                   QuadRule qr=_defaultRule, number_t qo=0);      //!< basic constructor from OperatorOnUnknown and an explicit quadrature (shortcut)

    void setComputationType();                                 //!< set the computation type (_FEComputation, _FEextComputation, _SPComputation, ...)
    void setIntegrationMethods();                              //!< check or define integration methods

    const OperatorOnUnknown* opu() const           //! return the pointer to the operator on unknown
    {return opu_p;}
    const IntegrationMethod* intgMethod() const    //! pointer to integration method object
    {return intgMethod_p;}
    virtual BasicLinearForm* clone() const;        //!< clone of the linear form
    virtual LinearFormType type() const            //! return the type of the linear form
    {return _intg;}
    virtual ValueType valueType() const            //! return the value type of the linear form
    {return opu_p->valueType();}
    virtual StrucType strucType() const            //! return the structure type of the linear form
    {return opu_p->strucType();}
    bool normalRequired() const                    //! true if normal is required
    {return opu_p->normalRequired() || intgMethodsRequireNormal();}
    bool xnormalRequired() const                   //! true if x-normal is required
    {return opu_p->xnormalRequired() || intgMethodsRequireNormal();}
    bool ynormalRequired() const                   //! true if y-normal is required
    {return opu_p->ynormalRequired() || intgMethodsRequireNormal();}
    bool xnormalRequiredByOpKernel() const         //! true if x-normal is required by operator on Kernel
     {return opu_p->xnormalRequiredByOpKernel() || intgMethodsRequireNormal();}
    bool ynormalRequiredByOpKernel() const         //! true if y-normal is required by operator on Kernel
    {return opu_p->ynormalRequiredByOpKernel()|| intgMethodsRequireNormal();}
    bool intgMethodsRequireNormal() const;

    virtual string_t asString() const
    {return "intg_"+domain_p->name()+" "+opu_p->asString();}
    virtual void print(std::ostream& os)const;        //!< print utility
    virtual void print(PrintStream& ps) const {print(ps.currentStream());}
};

/*!
   \class DoubleIntgLinearForm
   describes a linear form based on a double integral
*/
class DoubleIntgLinearForm : public BasicLinearForm
{
  protected:
    const OperatorOnUnknown* opu_p;  //!< pointer to the operator on unknown
    const GeomDomain* domainx_p;     //!< geometric domain of the integral (say x variable)
    const GeomDomain* domainy_p;     //!< geometric domain of the integral (say y variable)
  public:
    DoubleIntgLinearForm(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu,
                         QuadRule qr=_defaultRule, number_t qo=0);            //!< basic construct
    const OperatorOnUnknown* opu() const    //! return the pointer to the operator on unknown
    {return opu_p;}
    const GeomDomain* domainx() const       //! return the pointer to the first domain
    {return domainx_p;}
    const GeomDomain* domainy() const       //! return the pointer to the second domain
    {return domainy_p;}
    virtual BasicLinearForm* clone() const; //!< clone of the linear form
    virtual LinearFormType type() const     //! return the type of the linear form
    {return _doubleIntg;}
    virtual ValueType valueType() const     //! return the value type of the linear form
    {return opu_p->valueType();}
    virtual StrucType strucType() const     //! return the structure type of the linear form
    {return opu_p->strucType();}

    virtual string_t asString() const
      {return "intg_"+domainx_p->name()+"intg_"+domainy_p->name()+" "+opu_p->asString();}
    virtual void print(std::ostream&) const;//!< print utility
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

//@{
//! useful typedefs to SuLinearForm class
typedef std::pair<BasicLinearForm*, complex_t> lfPair;
typedef std::vector<lfPair>::iterator it_vlfp;
typedef std::vector<lfPair>::const_iterator cit_vlfp;
//@}

/*!
   \class SuLinearForm
   describes a linear combination of linear forms
   with the same unknown.
*/
class SuLinearForm
{
  protected:
    std::vector<lfPair> lfs_;                    //!< list of pairs of linear form and coefficient
  public:
    SuLinearForm() {}                            //!< default constructor
    SuLinearForm(const std::vector<lfPair>& lfp); //!< full constructor
    SuLinearForm(const SuLinearForm& sulf);      //!< copy constructor
    ~SuLinearForm();                             //!< destructor
    SuLinearForm& operator=(const SuLinearForm& sulf); //!< assignment operator

    number_t size() const                        //! number of elements of the linear combination
    {return lfs_.size();}
    std::vector<lfPair>& lfs()                   //! access to the list of linear forms
    {return lfs_;}
    const std::vector<lfPair>& lfs() const       //! access to the list of linear forms
    {return lfs_;}
    lfPair& operator()(number_t n)               //! acces to n-th pair
    {return lfs_[n - 1];}
    const lfPair& operator()(number_t n) const   //! acces to n-th pair
    {return lfs_[n - 1];}
    it_vlfp begin()                              //! iterator to the first element of vector of basic linear forms
    {return lfs_.begin();}
    cit_vlfp begin() const                       //! const iterator to the first element of vector of basic linear forms
    {return lfs_.begin();}
    it_vlfp end()                                //! iterator to the last+1 element of vector of basic linear forms
    {return lfs_.end();}
    cit_vlfp end() const                         //! const iterator to the last+1 element of vector of basic linear forms
    {return lfs_.end();}
    const Unknown* unknown() const;    //!< return the unknown of the linear form
    const Space* space() const;        //!< return the space of the linear form
    LinearFormType type() const;       //!< return the type of the linear form (intg,doubleIntg,linearCombination)
    ValueType valueType() const;       //!< return the value type of the linear form
    StrucType strucType() const;       //!< return the structure type of the linear form

    bool checkConsistancy(const SuLinearForm& sulf) const; //!< check consitancy of Unknown
    SuLinearForm& operator +=(const SuLinearForm& sulf);   //!< sum of linear forms
    SuLinearForm& operator -=(const SuLinearForm& sulf);   //!< difference of linear forms
    SuLinearForm& operator *=(const complex_t& c);      //!< multiply by a complex
    SuLinearForm& operator /=(const complex_t& c);      //!< divide by a complex

    bool normalRequired() const;            //!< true if normal is required
    bool xnormalRequired() const;           //!< true if x-normal is required
    bool ynormalRequired() const;           //!< true if y-normal is required
    bool xnormalRequiredByOpKernel() const; //!< true if x-normal is required by operator on Kernel
    bool ynormalRequiredByOpKernel() const; //!< true if y-normal is required by operator on Kernel

    string_t asString() const;              //!< as symbolic string
    void print(std::ostream& os)const;         //!< print utility
    void print(PrintStream& ps) const {print(ps.currentStream());}

}; // end of SuLinearForm class

//-------------------------------------------------------------------------------
//External functions of SuLinearForm class
//-------------------------------------------------------------------------------

//operations on linear form to construct linear combination of forms
SuLinearForm operator-(const SuLinearForm& sulf);                       //!< opposite of linear form
SuLinearForm operator+(const SuLinearForm& sulf1, const SuLinearForm& sulf2);  //!< sum of linear forms
SuLinearForm operator-(const SuLinearForm& sulf1, const SuLinearForm& sulf2);  //!< difference of linear forms
SuLinearForm operator*(const complex_t& c, const SuLinearForm& sulf);     //!< product(left) by a scalar
SuLinearForm operator*(const SuLinearForm& sulf, const complex_t& c);     //!< product(right) by a scalar
SuLinearForm operator/(const SuLinearForm& sulf, const complex_t& c);     //!< division by a scalar
std::ostream& operator<<(std::ostream& os, const SuLinearForm& sulf);      //!< output SuLinearForm

//@{
//! useful typedefs to LinearForm class
typedef std::map<const Unknown*, SuLinearForm>::iterator it_mulc;
typedef std::map<const Unknown*, SuLinearForm>::const_iterator cit_mulc;
//@}

/*!
   \class LinearForm
   describes a general linear form, that is
   a list of linear combinations of basic linear forms.
   It is intend to collect linear forms with different unknowns,
   using a map<const Unknown*,SuLinearForm>
   It is the enduser's class
*/
class LinearForm
{
  protected:
    std::map<const Unknown*, SuLinearForm> mlclf_;        //!< list of linear combinations of basic linear forms
    SuLinearForm& operator[](const Unknown*);             //!< access to the linear form associated to an unknown
    const SuLinearForm& operator[](const Unknown*) const; //!< access to the linear form associated to an unknown

  public:
    LinearForm() {}                                       //!< default constructor
    LinearForm(const SuLinearForm& sulf);                 //!< constructor from a linear combination
    bool isEmpty() const;                                 //!< true if no linear form
    void clear()                                          //! clear the map of linear forms
    {mlclf_.clear();}
    it_mulc begin()                                       //! iterator to the first element of map
    {return mlclf_.begin();}
    cit_mulc begin() const                                //! const iterator to the first element of map
    {return mlclf_.begin();}
    it_mulc end()                                         //! iterator to the last+1 element of map
    {return mlclf_.end();}
    cit_mulc end() const                                  //! const iterator to the last+1 element of map
    {return mlclf_.end();}
    bool singleUnknown() const;                           //!< true if only one unknown involved
    number_t nbOfUnknowns() const;                        //!< number of Unknowns involved
    const Unknown* unknown() const;                       //!< return first unknown involved
    std::set<const Unknown*> unknowns() const;            //!< return set of Unknown's involved
    const SuLinearForm& first() const;                    //!< return first linear combination
    LinearForm operator()(const Unknown&) const;          //!< access to the linear form associated to an unknown
    BasicLinearForm& operator()(const Unknown& u, number_t n); //!< access to the n-th basic linear form associated to an unknown
    const BasicLinearForm& operator()(const Unknown& u, number_t n) const; //!< access to the n-th basic linear form associated to an unknown
    const Unknown& firstUnknown() const                   //! returns first unknown
    {return *mlclf_.begin()->first;}
    ValueType valueType() const;                          //!< return the value type of the linear form
    StrucType strucType() const;                          //!< return the structure type of the linear form
    const SuLinearForm* subLfp(const Unknown* u) const;   //!< access to SuLinearForm pointers related to unknown
    SuLinearForm* subLfp(const Unknown* u);               //!< access to SuLinearForm pointers related to unknown (non const)

    bool normalRequired() const;                          //!< true if normal is required
    bool xnormalRequired() const;                         //!< true if x-normal is required
    bool ynormalRequired() const;                         //!< true if y-normal is required
    bool xnormalRequiredByOpKernel() const;               //!< true if x-normal is required by operator on Kernel
    bool ynormalRequiredByOpKernel() const;               //!< true if y-normal is required by operator on Kernel

    void print(std::ostream& os)const;                       //!< print utility
    void print(PrintStream& ps) const {print(ps.currentStream());}

    LinearForm& operator +=(const LinearForm& lf);           //!< sum of linear forms
    LinearForm& operator -=(const LinearForm& lf);           //!< difference of linear forms
    LinearForm& operator *=(const complex_t& c);            //!< multiply by a complex
    LinearForm& operator /=(const complex_t& c);            //!< divide by a complex

    //Equation constructor (implemented in EssentialCondition.cpp)
    EssentialCondition operator=(const complex_t & c);    //!< construct essential condition lf(u)=c
};

//-------------------------------------------------------------------------------
//External functions of LinearForm class
//-------------------------------------------------------------------------------

//! get values of keys used in intg routines
void intgLfBuildParam(const Parameter& p, real_t& bound, bool& isogeom, IntegrationMethod*& meth,
                      IntegrationMethods& meths, QuadRule& qr, number_t& qo, SymType& st, ComputationType& ct, const GeomDomain*& extdom);
//! check compatibility between keys used in intg routines
void intgLfParamCompatibility(const ParameterKey& key, std::set<ParameterKey>& usedParams);

//! main routine for single integrals
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const std::vector<Parameter>& ps);
//@{
//! Basic single intg routines for users (with up to 10 keys)
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9, const Parameter& p10);
LinearForm intg(const GeomDomain& dom, const Unknown& u);
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1);
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2);
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3);
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4);
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5);
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6);
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7);
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8);
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9);
LinearForm intg(const GeomDomain& dom, const Unknown& u, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9, const Parameter& p10);
//@}
//@{
//! Advanced (linear combinations) single intg routines for users (with up to 10 keys)
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9, const Parameter& p10);
//@}
//@{
//! Basic single intg routines with kernels for users (with up to 10 keys)
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const Parameter& p1, const Parameter& p2,
                const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                const Parameter& p8, const Parameter& p9, const Parameter& p10);
//@}

//! main routine for double integrals
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const std::vector<Parameter>& ps);

//@{
//! Basic double intg routines for users (with up to 10 keys)
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7, const Parameter& p8);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7, const Parameter& p8, const Parameter& p9);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7, const Parameter& p8);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7, const Parameter& p8, const Parameter& p9);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const Parameter& p1,
                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10);
//@}

//@{
/*!
  \deprecated use key-value system for optional arguments (_quad, _order, _method, _symmetry, _isogeo, ...)
*/
//user functions like constructors of single integral involving Unknown: int_dom v
LinearForm intg(const GeomDomain& dom, const Unknown& u, bool isogeo);
LinearForm intg(const GeomDomain& dom, const Unknown& u, const IntegrationMethods& ims, bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const Unknown& u, const IntegrationMethod& im, bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const Unknown& u, QuadRule qr, number_t qo=0, bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const IntegrationMethods& ims,
                bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, const IntegrationMethod& im,
                bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, QuadRule qr, number_t qo=0, bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const IntegrationMethods& ims,
                bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, const IntegrationMethod& im,
                bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, QuadRule qr, number_t qo=0, bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const IntegrationMethods& ims,
                bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, const IntegrationMethod& im,
                bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const KernelOperatorOnUnknowns& kopus, QuadRule qr, number_t qo=0, bool isogeo=false);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, const IntegrationMethod& im,
                bool isogeo=false);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, QuadRule qr, number_t qo=0,
                bool isogeo=false);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, const IntegrationMethod& im,
                bool isogeo=false);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const Unknown& u, QuadRule qr, number_t qo=0,
                bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const Unknown& u, ComputationType ct, const IntegrationMethods& ims,
                bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const Unknown& u, ComputationType ct, const IntegrationMethod& im,
                bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const Unknown& u, ComputationType ct, QuadRule qr=_defaultRule, number_t qo=0,
                bool isogeo=false);
//user functions like constructors of single integral involving OperatorOnUnknown: int_dom op(v)
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, ComputationType ct,
                const IntegrationMethods& ims, bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, ComputationType ct,
                const IntegrationMethod& im, bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, ComputationType ct,
                QuadRule = _defaultRule, number_t =0, bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, ComputationType ct,
                const IntegrationMethods& ims, bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, ComputationType ct,
                const IntegrationMethod& im, bool isogeo=false);
LinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknown& lcopu, ComputationType ct,
                QuadRule = _defaultRule, number_t =0, bool isogeo=false);

//algebraic operators on LinearForm
LinearForm operator-(const LinearForm&);                     //!< opposite of a linear form
LinearForm operator+(const LinearForm&, const LinearForm&);  //!< sum of linear forms
LinearForm operator-(const LinearForm&, const LinearForm&);  //!< difference of linear forms
LinearForm operator*(const int_t&, const LinearForm&);       //!< product(left) by a scalar
LinearForm operator*(const int&, const LinearForm&);         //!< product(left) by a scalar
LinearForm operator*(const number_t&, const LinearForm&);    //!< product(left) by a scalar
LinearForm operator*(const real_t&, const LinearForm&);      //!< product(left) by a scalar
LinearForm operator*(const complex_t&, const LinearForm&);   //!< product(left) by a scalar
LinearForm operator*(const LinearForm&, const int_t&);       //!< product(right) by a scalar
LinearForm operator*(const LinearForm&, const int&);         //!< product(right) by a scalar
LinearForm operator*(const LinearForm&, const number_t&);    //!< product(right) by a scalar
LinearForm operator*(const LinearForm&, const real_t&);      //!< product(right) by a scalar
LinearForm operator*(const LinearForm&, const complex_t&);   //!< product(right) by a scalar
LinearForm operator/(const LinearForm&, const int_t&);       //!< division by a scalar
LinearForm operator/(const LinearForm&, const int&);         //!< division by a scalar
LinearForm operator/(const LinearForm&, const number_t&);    //!< division by a scalar
LinearForm operator/(const LinearForm&, const real_t&);      //!< division by a scalar
LinearForm operator/(const LinearForm&, const complex_t&);   //!< division by a scalar

//print utilities
std::ostream& operator<<(std::ostream&, const LinearForm&);  //!< output LinearForm

} //end of namespace xlifepp

#endif /* LINEAR_FORM_HPP */
