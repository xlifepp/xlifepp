/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file BilinearForm.hpp
  \author E. Lunéville
  \since 23 mar 2012
  \date 9 may 2012

  \brief Definition of the xlifepp::BilinearForm classes

  This file declares all the classes related to bilinear forms management.
  The more general bilinear form under consideration is a list of linear combinations of basic bilinear forms.
  The list is indexed by the unknowns of the linear combination.

  Basic bilinear forms are, for the moment, integral or double integral type.
  The bilinear form may have multiple unknowns but a linear combination is related to a unique unknown

  - BiLinearForm is the end user class, it is a map of <Unknown*,LCBilinearForm>
  - SuBilinearForm deals with linear combination of basic bilinear forms, it is a vector of pairs of <BasicBilinearForm*,complex>
  - BasicBilinearForm is the parent class (abstract) of classes
    - IntgBilinearForm for single integral bilinear forms (generally in FEM method)
    - DoubleIngBilinearForm for double integral bilinear forms (generally in BEM method)
    - MatrixBilinearForm for explicit bilinear forms given by a TermMatrix (NOT AVAILABLE)
    - UserBilinearForm for user bilinear forms handling elementary matrix computation functions

  Child objects are not directly instanced, they are constructed using functions intg(...)

  As example:
  \code
  BilinearForm a = intg(Omega,f*u*v);                               //a single unknown (u) linear form
  BilinearForm a = intg(Omega,f*u*v)+2*intg(Omega,grad(u)|grad(w)); //a multiple unknowns (u,v), (u,w) bilinear form
  BilinearForm a = intg(Gamma,Gamma,u*G*v);  //a IE bilinear form involving domain product Gamma x Gamma (G is a Kernel)
  BilinearForm a = userBilinearForm(Omega,u,v,bf,_FEComputation); // user bilinear form on domain Omega, involving u,v unknowns, of FE computation type
                   with bf a function computing the elementary matrices : void bf(BFComputationData& bfd){...}
                   BFComputationData a class handling some data related to element where the elementary matrix has to be computed
  \endcode

  All linear algebraic operations are allowed on BilinearForm objects

  It is possible to impose the quadrature rule to an integral linear form by specifying a quadrature rule type and its order
  or more general integration methods object
  \code
  BilinearForm l = intg(Omega,f*u*v,_quad=GaussLegendre,_order=5);           //a single unknown (u) linear form
  \endcode
  see Quadrature class for the list of quadrature rules available

  intg(...) or doubleIntg(...) supports the following key options:

    _method             = an IntegrationMethod or an IntegrationsMethods (list of IntegationMethod)
    _quad  (_quad1)     = a QuadRule (defaultQuadrature, GaussLegendre, symmetricalGauss, GaussLobatto, nodalQuadrature, miscQuadrature, GrundmannMoller evenGaussLegendre, evenGaussLobatto)
    _order (_order1)    = a Number being the order of the QuadRule
    _quad2              = a second QuadRule (if required)
    _order2             = order of the second QuadRule (if required)
    _bound              = a Real, representing a relative distance between two elements bound in the context of double integral, used in conjunction with _method or _quad,_order
    _symmetry           = a SymType (noSymmetry, symmetric, skewSymmetric, selfAdjoint, diagonal, undefSymmetry) that enforces symmetry property of bilinear form
    _extension_domain   = a GeomDomain representing the extended domain related to the unknown u when the integration domain is a boundary domain or an interface and computation requires to extend side element to 'volumic' element
    _extension_domain_v = extended domain related to the test function v if required
    _isogeo             : if present, the integral is calculated using an isogeometric approach (experimental, see specific documentation)

    notes: - specify _method or QuadRule data, but not both
           - the symmetry of the integral versus (u,v) is automatically detected, but in some intricate cases, the symmetry detection may fail, so it can be enforced
           - operator with non tangential derivatives or unknown/testfunction related to FE space that have no trace on side domain (e.g Nedelec FE) requires an extension of side domain
             in case of a boundary, extension domain is generally automatically set, but in case of interface it may be required to specify the correct domain extension
*/

#ifndef BILINEAR_FORM_HPP
#define BILINEAR_FORM_HPP

#include "config.h"
#include "space.h"
#include "operator.h"
#include "finiteElements.h"
#include "largeMatrix.h"

#include <utility>

namespace xlifepp
{

class BasicBilinearForm;
class IntgBilinearForm;
class DoubleIntgBilinearForm;
class MatrixBilinearForm;
class UserBilinearForm;

//@{
//! useful typedefs to MatrixBilinearForm class
typedef std::pair<BasicBilinearForm*, complex_t> bfPair;
typedef std::vector<bfPair>::iterator it_vbfp;
typedef std::vector<bfPair>::const_iterator cit_vbfp;
//@}

/*! stuff related to bf user computation, loaded by computation algorithm
    manage information to compute elementary matrix (real or scalar matrices)
    from elements elt_u, elt_v in case of FEM or BEM computation
    and from additional elements elt_u2, elt_v2 in case of DG computation
    produce one matrix in FEM or BEM computation, up to four matrices in DG computation
*/
class BFComputationData
{
  public:
    const Element* elt_u, *elt_v;           // Element pointers (FEM or BEM)
    const Element* elt_u2,*elt_v2;          // additional Element pointers (DG)
    const GeomElement* sidelt;              // side element when DG
    std::vector<Matrix<real_t>> matels;     // real elementary matrices
    std::vector<Matrix<complex_t>> cmatels; // complex elementary matrices
    BFComputationData(const Element* eu=nullptr, const Element* ev=nullptr, const Element* eu2=nullptr, const Element* ev2=nullptr, const GeomElement* se=nullptr)
    :elt_u(eu), elt_v(ev), elt_u2(eu2), elt_v2(ev2), sidelt(se) {}
    ValueType valueType() const
    {
      if(matels.size()>0)  return _real;
      if(cmatels.size()>0) return _complex;
      return _none;
    }
    const Matrix<real_t>& matel() const
    {
      if(matels.size()==0) error("free_error","BFComputationData::matels is empty");
      return matels[0];
    }
    Matrix<real_t>& matel()
    {
      if(matels.size()==0) matels.resize(1);
      return matels[0];
    }
    const Matrix<complex_t>& cmatel() const
    {
      if(cmatels.size()==0) error("free_error","BFComputationData::cmatels is empty");
      return cmatels[0];
    }
    Matrix<complex_t>& cmatel()
    {
      if(cmatels.size()==0) cmatels.resize(1);
      return cmatels[0];
    }
};

typedef void (*BFFunction)(BFComputationData& bfd); //user function to compute elementary bf

//=========================================================================================
/*!
  \class BasicBilinearForm
  describes the base class of bilinear form on a space.
  It is an abstract class
*/
//=========================================================================================
class BasicBilinearForm
{
protected:
  const Unknown* u_p;          //!< pointer to unknown u
  const Unknown* v_p;          //!< pointer to unknown v (test function)
  const GeomDomain* domainu_p; //!< geometric domain of unknown u
  const GeomDomain* domainv_p; //!< geometric domain of unknown v (test function)
  ComputationType compuType;   //!< computation type (_FEComputation, _IEComputation, _SPComputation, ...)
  mutable const GeomDomain* extDomainu_p; //!< extended domain on u (non tangent derivative or interior side domain)
  mutable const GeomDomain* extDomainv_p; //!< extended domain on v (non tangent derivative or interior side domain)

public:
  mutable SymType symmetry; //!< symmetry of the bilinear form ((_noSymmetry , _symmetric, _skewSymmetric, _selfAdjoint, _skewAdjoint)
  bool isoGeometric;        //!< if true, use isoGeometric computation requiring parametrization (default false)

  //constructor/destructor
  BasicBilinearForm(const Unknown* up=nullptr, const GeomDomain* dup=nullptr,
                    const Unknown* vp=nullptr, const GeomDomain* dvp=nullptr) //! basic and default constructor
  : u_p(up), v_p(vp), domainu_p(dup), domainv_p(dvp), compuType(_undefComputation),
    extDomainu_p(nullptr), extDomainv_p(nullptr), symmetry(_undefSymmetry), isoGeometric(false) {}
  virtual ~BasicBilinearForm() {} //!< virtual destructor

  //accessors
  const Unknown& up() const //! return the unknown
  {return *u_p;}
  const Unknown& vp() const //! return the test function
  {return *v_p;}
  const GeomDomain& dom_up() const //! return the unknown domain
  {return *domainu_p;}
  const GeomDomain& dom_vp() const //! return the test function domain
  {return *domainv_p;}
   const GeomDomain& extDom_u() const //! return the extended domain u as reference
  {return *extDomainu_p;}
   const GeomDomain& extDom_v() const //! return the extended domain v as reference
  {return *extDomainv_p;}
  const GeomDomain* extDom_up() const //! to access to extended domain u pointer
  {return extDomainu_p;}
  const GeomDomain* extDom_vp() const //! to access to extended domain v pointer
  {return extDomainv_p;}
  const GeomDomain*& extDom_up() //! to write extended domain u pointer
  {return extDomainu_p;}
  const GeomDomain*& extDom_vp() //! to write extended domain v pointer
  {return extDomainv_p;}
  ComputationType computationType() const //! return the computation type
  {return compuType;}
  ComputationType& computationType() //! return the computation type
  {return compuType;}

  Space* subSpace_up(); //!< return the u subspace of the bilinear form, construct it if does not exist
  Space* subSpace_vp(); //!< return the v subspace of the bilinear form, construct it if does not exist

  void checkUnknowns() const; //!< check the Unknown/TestFunction status of u_p and v_p

  // virtual member functions
  virtual BasicBilinearForm* clone() const = 0;           //!< clone of the bilinear form
  virtual LinearFormType type() const = 0;                //!< return the type of the bilinear form
  virtual ValueType valueType() const = 0;                //!< return the value type of the bilinear form
  IntgBilinearForm* asIntgForm();                         //!< downcast as IntgBilinearForm*
  const IntgBilinearForm* asIntgForm() const;             //!< downcast as const IntgBilinearForm*
  DoubleIntgBilinearForm* asDoubleIntgForm();             //!< downcast as DoubleIntgBilinearForm*
  const DoubleIntgBilinearForm* asDoubleIntgForm() const; //!< downcast as const DoubleIntgBilinearForm*
  UserBilinearForm* asUserForm();                         //!< downcast as const UserBilinearForm*
  const UserBilinearForm* asUserForm() const;             //!< downcast as const UserBilinearForm*
  virtual bool hasJump() const                            //! true if bilinear form has jump, mean operator
    {return false;}
  virtual std::vector<bfPair> split() const //!< split BasicBilinearForm involving particular operator (jump, mean)
    {return std::vector<bfPair>(1,std::make_pair(const_cast<BasicBilinearForm*>(this),complex_t(1.)));} //return self by default
  virtual SymType setSymType() const=0;                   //!< set symmetry property
  SymType symType() const {return symmetry;}              //!< access to symmetry property (const)
  SymType& symType()      {return symmetry;}              //!< access to symmetry property
  virtual void print(std::ostream& os) const = 0;         //!< print utility
  virtual void print(PrintStream& ps) const = 0;          //!< print utility
  virtual string_t asString() const = 0;                  //!< interpret as string for print purpose
  virtual void setUnknowns(const Unknown& u, const Unknown& v) //! set (change) the unknowns
  {u_p=&u; v_p=&v;}
};

/*!
  \class IntgBilinearForm
  describes a bilinear form based on a single integral

       intg_domain opu aopu opv

     with opu, opv: operator on u and v
          aopu, aopv: algebraic operation (*,|,%,^)
*/
class IntgBilinearForm : public BasicBilinearForm
{
protected:
  const OperatorOnUnknowns* opus_p;      //!< pointer to the operator on unknowns
  const IntegrationMethod* intgMethod_p; //!< pointer to integration method

public:
  //! constructor from OperatorOnUnknowns
  IntgBilinearForm(const GeomDomain& dom, const OperatorOnUnknowns& opus, const IntegrationMethod& im, SymType st);
  //! constructor from OperatorOnUnknowns
  IntgBilinearForm(const GeomDomain& dom, const OperatorOnUnknowns& opus, QuadRule qr, number_t qo, SymType st);
  //! constructor from explicit operators
  IntgBilinearForm(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                   const OperatorOnUnknown& opv, const IntegrationMethod& im, SymType st);
  //! constructor from explicit operators
  IntgBilinearForm(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                   const OperatorOnUnknown& opv, QuadRule qr, number_t qo, SymType st);
  IntgBilinearForm(const IntgBilinearForm& ibf);            //!< copy constructor
  ~IntgBilinearForm();                                      //!< destructor
  IntgBilinearForm& operator=(const IntgBilinearForm& ibf); //!< assign operator

  //! set integration method
  void setIntegrationMethod(const GeomDomain& dom, const OperatorOnUnknowns& opus, QuadRule qr, number_t qo);
  void setComputationType(); //!< set computation type (_FEComputation,_SPComputation, _FESPComputation)

  const OperatorOnUnknown& opu() const        //! return the reference to the operator on unknown
  {return opus_p->opu();}
  const OperatorOnUnknown& opv() const        //! return the reference to the operator on unknown
  {return opus_p->opv();}
  const OperatorOnUnknown* opu_p() const      //! return the pointer to the operator on unknown
  {return opus_p->opu_p();}
  const OperatorOnUnknown* opv_p() const      //! return the pointer to the operator on unknown
  {return opus_p->opv_p();}
  AlgebraicOperator algop()const              //! return the algebraic operator
  {return opus_p->algop();}
  const GeomDomain* domain() const            //! return the pointer to the domain
  {return domainu_p;}
  virtual LinearFormType type() const         //! return the type of the linear form
  {return _intg;}
  virtual ValueType valueType() const         //! return the value type of the linear form
  {return opus_p->valueType();}
  const IntegrationMethod* intgMethod() const //! pointer to integration method object
  {return intgMethod_p;}
  SymType setSymType() const;                 //!< set symmetry property
  virtual BasicBilinearForm* clone() const;   //!< clone of the linear form
  virtual void setUnknowns(const Unknown& u, const Unknown& v); //!< set (change) the unknowns
  virtual std::vector<bfPair> split() const; //!< split IntgBilinearForm involving particular operators (jump, mean)
  virtual bool hasJump() const;               //! true if bilinear form has jump, mean operator

  //print stuff
  virtual void print(std::ostream& os) const; //!< print utility
  virtual void print(PrintStream& ps) const { print(ps.currentStream()); }
  virtual string_t asString() const;          //!< interpret as string for print purpose
};

/*!
  \class DoubleIntgBilinearForm
  describes a bilinear form based on a double integral

     intg_domain_v  intg_domain_u  opu(y) aopu opker(x,y) aopv opv(x) dy dx

     with opu, opv: operator on u and v
          aopu, aopv: algebraic operation (*,|,%,^)
          opker: operator on a kernel function

    note: when kernel = 1 the pointer to kernel function in opker is ker_p=0 !
    Be cautious with order of domain, first if for v-domain, second for u-domain

    Actually, two ways to handle some integration methods:
      by pointer to an IntegrationMethod object, so only one integration method (old way, should disappear in futur)
      by an IntegrationMethods object that handles some IntegrationMethod (new way)
*/
class DoubleIntgBilinearForm : public BasicBilinearForm
{
  protected:
    const KernelOperatorOnUnknowns* kopus_p;     //!< pointer to the operator on unknowns with kernel
    const LcKernelOperatorOnUnknowns* lckopus_p; //!< pointer to linear combination of operator on unknowns with kernel (experimental)
    const IntegrationMethod* intgMethod_p;       //!< pointer to an integration method

  public:
    IntegrationMethods intgMethods;              //!< list of integration methods

  public:
    //copy constructor
    DoubleIntgBilinearForm(const DoubleIntgBilinearForm& dibf);            //!< copy constructor
    DoubleIntgBilinearForm& operator=(const DoubleIntgBilinearForm& dibf); //!< assign operator
    ~DoubleIntgBilinearForm();                                             //!< destructor

    //constructors from IntegrationMethod
    //! basic constructor
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const LcKernelOperatorOnUnknowns& lckopus,
                           const IntegrationMethod& im, SymType st);
    //! basic constructor
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const KernelOperatorOnUnknowns& kopus,
                           const IntegrationMethod& im, SymType st);
    //! constructor from unknown operators (no kernel)
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                           AlgebraicOperator aop, const OperatorOnUnknown& opv, const IntegrationMethod& im, SymType st);
    //! constructor from unknown operators and operator on kernel
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                           AlgebraicOperator aopu, const OperatorOnKernel& opker, AlgebraicOperator aopv,
                           const OperatorOnUnknown& opv, const IntegrationMethod& im, SymType st);
    //! constructor from unknown operators and kernel
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                           AlgebraicOperator aopu, const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                           const IntegrationMethod& im, SymType st);

    //constructors from IntegrationMethods
    //! basic constructor
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const LcKernelOperatorOnUnknowns& lckopus,
                           const IntegrationMethods& ims, SymType st);
    //! basic constructor
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const KernelOperatorOnUnknowns& kopus,
                           const IntegrationMethods& ims, SymType st);
    //! constructor from unknown operators (no kernel)
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                           AlgebraicOperator aop, const OperatorOnUnknown& opv, const IntegrationMethods& ims, SymType st);
    //! constructor from unknown operators and operator on kernel
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                           AlgebraicOperator aopu, const OperatorOnKernel& opker, AlgebraicOperator aopv,
                           const OperatorOnUnknown& opv, const IntegrationMethods& ims, SymType st);
    //! constructor from unknown operators and kernel
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu,
                           AlgebraicOperator aopu, const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                           const IntegrationMethods& ims, SymType st);

    //constructors from Quadrule's
    //! basic constructor
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const LcKernelOperatorOnUnknowns& lckopus,
                           QuadRule qr1, number_t qo1, QuadRule qr2, number_t qo2, SymType st);
    //! basic constructor
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const KernelOperatorOnUnknowns& kopus,
                          QuadRule qr1, number_t qo1, QuadRule qr2, number_t qo2, SymType st);
    //! constructor from unknown operators (no kernel)
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                          const OperatorOnUnknown& opv,
                          QuadRule qr1, number_t qo1, QuadRule qr2, number_t qo2, SymType st);
    //! constructor from unknown operators and operator on kernel
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                          const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                          QuadRule qr1, number_t qo1, QuadRule qr2, number_t qo2, SymType st);
    //! constructor from unknown operators and kernel//accessors
    DoubleIntgBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                          const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                          QuadRule qr1, number_t qo1, QuadRule qr2, number_t qo2, SymType st);
    //! set IntegrationMethod for quadrature product rule
    void setIntegrationMethod(const GeomDomain& domu, const GeomDomain& domv, const KernelOperatorOnUnknowns& kopus,
                              QuadRule qr1, number_t qo1, QuadRule qr2, number_t qo2);
    void setComputationType();      //!< set computation type (_IEComputation,_IEExtComputation,IEHMatrixComputation, ..)
    void setIntegrationMethods();   //!< check and update quadrature pointers of IntegrationsMethods
    void setHMIntegrationMethods(); //!< check and update quadrature pointers of IntegrationsMethods when a HMatrixIM
    void checkCollinoIM(const IntegrationMethod*);   //!< check that CollinoIM is available for the double integral

    //accessors
    const KernelOperatorOnUnknowns& kopus() const //! return KernelOperatorOnUnknowns reference (const)
      {return *kopus_p;}
    const KernelOperatorOnUnknowns* kopusp() const //! return KernelOperatorOnUnknowns pointer (const)
      {return kopus_p;}
    const LcKernelOperatorOnUnknowns& lckopus() const //! return KernelOperatorOnUnknowns reference (const)
      {return *lckopus_p;}
    const LcKernelOperatorOnUnknowns* lckopusp() const //! return KernelOperatorOnUnknowns pointer (const)
      {return lckopus_p;}
    const KernelOperatorOnUnknowns& kopuv() const //! return KernelOperatorOnUnknowns reference
      {if(kopus_p!=nullptr) return *kopus_p; return *lckopus_p->begin()->first;}
    const OperatorOnUnknown& opu() const      //! return the pointer to the operator on unknown
      {if(kopus_p!=nullptr) return kopus_p->opu(); return lckopus_p->begin()->first->opu();}
    const OperatorOnUnknown& opv() const      //! return the pointer to the operator on unknown
      {if(kopus_p!=nullptr) return kopus_p->opv(); return lckopus_p->begin()->first->opv();}
    const OperatorOnKernel& opker() const     //! return the pointer to the operator on kernel
      {if(kopus_p!=nullptr) return kopus_p->opker(); return lckopus_p->begin()->first->opker();}
    AlgebraicOperator algopu() const          //! return the algebraic operator on u
      {if(kopus_p!=nullptr) return kopus_p->algopu(); return lckopus_p->begin()->first->algopu();}
    AlgebraicOperator algopv() const          //! return the algebraic operator on v
      {if(kopus_p!=nullptr) return kopus_p->algopu(); return lckopus_p->begin()->first->algopv();}
    const IntegrationMethod* intgMethod() const //! return the pointer to the integration method
      {return intgMethod_p;}
    const GeomDomain* domainx() const         //! return the pointer to the x domain, say v-domain
      {return domainv_p;}
    const GeomDomain* domainy() const         //! return the pointer to the y domain, say u-domain
      {return domainu_p;}
    DomainPair domains() const                //! return the pair of domain pointers
      {return DomainPair(domainu_p,domainv_p);}
    virtual LinearFormType type() const       //! return the type of the linear form
      {return _doubleIntg;}
    virtual ValueType valueType() const;      //! return the value type of the bilinear form
    SymType setSymType() const;               //!< return symmetry property (_noSymmetry, _symmetric, _skewSymmetric, _selfAdjoint, _skewAdjoint)
    virtual BasicBilinearForm* clone() const; //!< clone of the linear form
    virtual void setUnknowns(const Unknown& u, const Unknown& v); //!< set (change) the unknowns

    //print tools
    string_t asString() const;                //!< interpret as string for print purpose
    virtual void print(std::ostream& os) const;  //!< print utility
    virtual void print(PrintStream& ps) const {print(ps.currentStream());}
    friend std::ostream& operator<<(std::ostream& os,const DoubleIntgBilinearForm& dibf)
    {dibf.print(os);return os;} //!< print operator
};

/*!
  \class UserBilinearForm
  describes a bilinear form given by a user function computing elementary matrix
    - bffun_ : user function void uf(BFComputationData&, Parameters&)
*/
class UserBilinearForm : public BasicBilinearForm
{
  public:
    BFFunction bffun_;                     //!< pointer to extern function to compute elementary bf (0 by default)
    const IntegrationMethod* intgMethod_p;  //!< pointer to an integration method
    bool requireInvJacobian_;               //!< flag indicates that computation of inverse of jacobian is required (default=false)
    bool requireNormal_;                    //!< flag indicates that computation of normal vector is required (default=false)

    UserBilinearForm(const GeomDomain& dom, const Unknown& u, const Unknown& v, BFFunction bf, ComputationType ct, SymType st, bool rij, bool rno, const IntegrationMethod& im);
    UserBilinearForm(const GeomDomain& domu, const GeomDomain& domv, const Unknown& u, const Unknown& v, BFFunction bf, ComputationType ct, SymType st, bool rij, bool rno, const IntegrationMethod& im);
    UserBilinearForm(const UserBilinearForm& ubf);
    ~UserBilinearForm();

    virtual LinearFormType type() const         //! return the type of the linear form
    {return _userLf;}
    const IntegrationMethod* intgMethod() const //! pointer to integration method object
    {return intgMethod_p;}
    virtual ValueType valueType() const;        //! return the value type of the linear form
    void requireInvJacobian(bool tf=true)
    {requireInvJacobian_=tf;}
    void requireNormal(bool tf=true)
    {requireNormal_=tf;}
    virtual BasicBilinearForm* clone() const;   //!< clone of the linear form
    virtual SymType setSymType() const          //!< set symmetry property
    { return symmetry;}  //nothing done !
    virtual void print(std::ostream&) const;    //!< print utility
    virtual void print(PrintStream& os) const {print(os.currentStream());}
    virtual string_t asString() const;          //!< interpret as string for print purpose
};

/*!
  \class MatrixBilinearForm
  describes a bilinear form given by an explicit matrix
  explicit matrix is handled by a MatrixEntry object
  MatrixEntry pointer may be assigned from:
    - MatrixEntry pointer or reference: no hard copy to save memory
    - Matrix: matrix is copied as a dense matrix MatrixEntry
    - Vector: vector is copied as a diagonal MatrixEntry
  when hard copy is involved, newMatrixEntry flag is set to true in order to free memory when deleting MatrixBilinearForm
*/
class MatrixBilinearForm : public BasicBilinearForm
{
  protected:
    const MatrixEntry* matrix_p;  //!< pointer to a MatrixEntry
    bool newMatrixEntry_;         //!< useful flag to clean memory

  public:
    //constructors/destructor
    MatrixBilinearForm()                                                           //! default constructor
    : BasicBilinearForm(0,0), matrix_p(nullptr), newMatrixEntry_(false) {}
    MatrixBilinearForm(const Unknown& u, const GeomDomain& du,
                       const Unknown& v, const GeomDomain& dv, const MatrixEntry* mp)  //! basic constructor
    : BasicBilinearForm(&u, &du, &v, &dv), matrix_p(mp), newMatrixEntry_(false) {}
    MatrixBilinearForm(const Unknown& u, const GeomDomain& du,
                       const Unknown& v, const GeomDomain& dv, const MatrixEntry & m)  //! basic constructor
    : BasicBilinearForm(&u, &du, &v, &dv), matrix_p(&m), newMatrixEntry_(false) {}
    template <typename T>
    MatrixBilinearForm(const Unknown&, const GeomDomain&,
                       const Unknown&, const GeomDomain&,
                       const Matrix<T>& , SymType=_noSymmetry);                    //!< constructor from a Matrix<T>
    template <typename T>
    MatrixBilinearForm(const Unknown&, const GeomDomain&,
                       const Matrix<T>& , SymType=_noSymmetry);                    //!< constructor from a Matrix<T>
    template <typename T>
    MatrixBilinearForm(const Unknown&, const GeomDomain&,const std::vector<T>&);   //!< constructor from a diagonal matrix(vector)
    ~MatrixBilinearForm();                                                         //!< destructor

    virtual LinearFormType type() const       //! return the type of the linear form
    {return _explicitLf;}
};

//-------------------------------------------------------------------------------
// External functions of MatrixBiLinearForm class
//-------------------------------------------------------------------------------
//constructor from a Matrix<T>
template <typename T>
MatrixBilinearForm::MatrixBilinearForm(const Unknown& u, const GeomDomain& du, const Matrix<T>& m, SymType sy)
: BasicBilinearForm(&u, &du, &u, &du)
{
  //create dense storage
  MatrixStorage* sto= new RowDenseStorage(m.numberOfRows(), m.numberOfColumns(),"");
  matrix_p= new MatrixEntry(m.valueType(), m.strucType(), sto, m.elementSize(), sy);
  newMatrixEntry_=true;
}

//constructor from a Matrix<T>
template <typename T>
MatrixBilinearForm::MatrixBilinearForm(const Unknown& u, const GeomDomain& du,
                         const Unknown& v, const GeomDomain& dv, const Matrix<T>& m, SymType sy)
: BasicBilinearForm(&u, &du, &v, &dv)
{
  //create dense storage
  MatrixStorage* sto= new RowDenseStorage(m.numberOfRows(), m.numberOfColumns(),"");
  matrix_p= new MatrixEntry(m.valueType(), m.strucType(), sto, m.elementSize(), sy);
  matrix_p->add(m.begin(),m.end()); //fill matrix
  newMatrixEntry_=true;
}

template <typename T>
MatrixBilinearForm::MatrixBilinearForm(const Unknown& u, const GeomDomain& d, const std::vector<T>& v)
{
  //create csr storage
  MatrixStorage* sto= new RowCsStorage(v.size(), v.size(),"");
  matrix_p= new MatrixEntry(v.valueType(), v.strucType(), sto, v.elementSize());
  matrix_p->add(v.begin(),v.end()); //fill matrix
  newMatrixEntry_=true;
}

/*!
  \class SuBilinearForm
  describes a linear combination of bilinear forms
  with the same pair of Unknowns.
*/
class SuBilinearForm
{
  protected:
    std::vector<bfPair> bfs_;                       //!< list of pairs of bilinear form and coefficient
    mutable SymType symmetry;                         //!< symmetry of the Subilinear form (_noSymmetry , _symmetric, _skewSymmetric, _selfAdjoint, _skewAdjoint,_diagonal,_undefSymmetry*)
  public:
    SuBilinearForm(SymType sym =_undefSymmetry) {symmetry =sym;}  //!< default constructor
    SuBilinearForm(const SuBilinearForm& subf);            //!< copy constructor
    SuBilinearForm(const std::vector<bfPair>&);      //!< full constructor from a list of BasicBilinearForms
    ~SuBilinearForm();                                //!< destructor
    SuBilinearForm& operator=(const SuBilinearForm& subf); //!< assignment operator

    number_t size() const                       //! number of elements of the linear combination
      {return bfs_.size();}
    std::vector<bfPair>& bfs()              //! access to the list of linear forms
      {return bfs_;}
    const std::vector<bfPair>& bfs() const  //! access to the list of linear forms
      {return bfs_;}
    bfPair& operator()(number_t n)             //! access to n-th pair
      {return bfs_[n - 1];}
    const bfPair& operator()(number_t n) const //! access to n-th pair
      {return bfs_[n - 1];}
    it_vbfp begin()                          //! iterator to the first element of vector of basic bilinear forms
      {return bfs_.begin();}
    cit_vbfp begin() const                   //! const iterator to the first element of vector of basic bilinear forms
      {return bfs_.begin();}
    it_vbfp end()                            //! iterator to the last+1 element of vector of basic bilinear forms
      {return bfs_.end();}
    cit_vbfp end() const                     //! const iterator to the last+1 element of vector of basic bilinear forms
      {return bfs_.end();}
    const Unknown* up() const;                //!< return the first unknown of the bilinear form
    const Unknown* vp() const;                //!< return the second unknown of the bilinear form
    const GeomDomain* dom_up() const;         //!< return the u-domain of the FIRST basic bilinear form
    const GeomDomain* dom_vp() const;         //!< return the v-domain of the FIRST basic bilinear form
    const Space* uSpace() const;              //!< return the u space of the bilinear form
    const Space* vSpace() const;              //!< return the v space of the bilinear form
    LinearFormType type() const;              //!< return the type of the bilinear form (intg,doubleIntg,linearCombination)
    ValueType valueType() const;              //!< return the value type of the bilinear form
    SymType symType() const{return symmetry;} //!< access to the symmetry property (const)
    SymType& symType(){return symmetry;}      //!< access to the symmetry property
    SymType setSymType() const;               //!< update the symmetry property
    bool hasJump() const;                     //!< true if at least one basic bilinear form has jump like operator
    void setUnknowns(const Unknown&, const Unknown&); //!< set unknowns of SuBilinearForm
    bool isDG()const;                         //!< true if a DG SubilinearForm
    bool hasDGUserBF() const;                //!< true if there is at least a UserbilinearForm of type _DGComputation

    bool checkConsistancy(const SuBilinearForm&) const; //!< check consitancy of Unknown
    SuBilinearForm& operator +=(const SuBilinearForm&); //!< sum of bilinear forms
    SuBilinearForm& operator -=(const SuBilinearForm&); //!< difference of linear forms
    SuBilinearForm& operator *=(const complex_t&);      //!< multiply by a complex
    SuBilinearForm& operator /=(const complex_t&);      //!< divide by a complex

    void print(std::ostream&) const;                    //!< print utility
    string_t asString() const;                          //!< as symbolic string

}; // end of BilinearForm class

//-------------------------------------------------------------------------------
// External functions of SuBilinearForm class
//-------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream&, const SuBilinearForm&); //!< output SuBilinearForm

// operations on bilinear form to construct linear combination of forms
SuBilinearForm operator-(const SuBilinearForm&);                        //!< opposite of bilinear form
SuBilinearForm operator+(const SuBilinearForm&, const SuBilinearForm&); //!< sum of bilinear forms
SuBilinearForm operator-(const SuBilinearForm&, const SuBilinearForm&); //!< difference of bilinear forms
SuBilinearForm operator*(const complex_t&, const SuBilinearForm&);        //!< multiply(left) by a scalar
SuBilinearForm operator*(const SuBilinearForm&, const complex_t&);        //!< multiply(right) by a scalar
SuBilinearForm operator/(const SuBilinearForm&, const complex_t&);        //!< divide by a scalar

//@{
//! useful typedefs to BilinearForm class
typedef std::map<uvPair, SuBilinearForm>::iterator it_mublc;
typedef std::map<uvPair, SuBilinearForm>::const_iterator cit_mublc;
//@}

/*!
  \class BilinearForm
  describes a general bilinear form, that is a list of linear combinations of basic bilinear forms.
  It is intend to collect linear forms with different unknowns, using a map<uvPair,SuBilinearForm>.
  It is the enduser's class
*/
class BilinearForm
{
protected:
  std::map<uvPair, SuBilinearForm> mlcbf_;              //!< list of linear combinations of basic bilinear forms

public:
  SuBilinearForm& operator[](const uvPair&);             //!< access to the bilinear form associated to an unknown
  const SuBilinearForm& operator[](const uvPair&) const; //!< access to the bilinear form associated to a pair of unknowns
  BilinearForm() {}                                                                  //!< default constructor
  BilinearForm(const SuBilinearForm& lc);                                            //!< constructor from a linear combination
  void clear()                                                                       //! clear the map of bilinear form
    {mlcbf_.clear();}
  bool isEmpty() const;                                                              //!< true if no bilinear form
  it_mublc begin()                                                                   //! iterator to the first element of map
    {return mlcbf_.begin();}
  cit_mublc begin() const                                                            //! const iterator to the first element of map
    {return mlcbf_.begin();}
  it_mublc end()                                                                     //! iterator to the last+1 element of map
    {return mlcbf_.end();}
  cit_mublc end() const                                                              //! const iterator to the last+1 element of map
    {return mlcbf_.end();}
  bool singleUnknown() const;                                                        //!< true if only one unknown
  const SuBilinearForm& first() const;                                               //!< return first linear combination
  SuBilinearForm& first();                                                           //!< return first linear combination
  BilinearForm operator()(const Unknown&, const Unknown&) const;                     //!< access to the bilinear form associated to a pair of unknowns
  BasicBilinearForm& operator()(const Unknown&, const Unknown&, number_t);             //!< access to the n-th basic bilinear form associated to a pair of unknowns
  const BasicBilinearForm& operator()(const Unknown&, const Unknown&, number_t) const; //!< access to the n-th basic bilinear form associated to a pair of unknowns
  const SuBilinearForm* subLfp(const uvPair& uv) const;                              //!< access to SuBiLinearForm pointers related to unknown
  SuBilinearForm* subLfp(const uvPair& uv);                                          //!< access to SuBiLinearForm pointers related to unknown (non const)
  void setUnknowns(const Unknown&, const Unknown&);                                  //!< set the unknowns of bilinear forms (only for single unknown bf)
  SymType& symType();                                                                //!< to change the symmetry property (only for single unknown bf)

  void print(std::ostream&) const;                                                   //!< print utility
  void print(PrintStream& os) const {print(os.currentStream());}
  string_t asString() const;

  BilinearForm& operator+=(const BilinearForm&); //!< sum of linear forms
  BilinearForm& operator-=(const BilinearForm&); //!< difference of linear forms
  BilinearForm& operator*=(const complex_t&);      //!< multiply by a complex
  BilinearForm& operator/=(const complex_t&);      //!< divide by a complex
};

//-------------------------------------------------------------------------------
// External functions of BiLinearForm class
//-------------------------------------------------------------------------------
std::vector<bfPair> split(std::vector<bfPair>&);

// Single integral user functions like constructors
// ------------------------------------------------

//! get values of keys used in intg routines
void intgBFBuildParam(const Parameter& p, real_t& bound, bool& isogeom, IntegrationMethod*& meth,
                    IntegrationMethods& meths, QuadRule& qr1, number_t& qo1, QuadRule& qr2, number_t& qo2,
                    SymType& st, const GeomDomain*& extdomu, const GeomDomain*& extdomv);
//! check compatibility between keys used in intg routines
void intgBFParamCompatibility(const ParameterKey& key, std::set<ParameterKey>& usedParams);

//! main routine for single integrals
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const std::vector<Parameter>& ps);
//@{
//! Basic single intg routines for users (with up to 10 keys)
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                  const Parameter& p8);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                  const Parameter& p8, const Parameter& p9);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                  const Parameter& p8, const Parameter& p9, const Parameter& p10);
//@}
//@{
//! Advanced (linear combinations) single intg routines for users (with up to 10 keys)
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                  const Parameter& p8);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                  const Parameter& p8, const Parameter& p9);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const Parameter& p1, const Parameter& p2,
                  const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                  const Parameter& p8, const Parameter& p9, const Parameter& p10);
//@}

//! main routine to double integrals
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const std::vector<Parameter>& ps);
//@{
//! Basic double intg routines without kernels, for users
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10);
//@}
//@{
//! Basic double intg routines with kernels for users
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10);
//@}

//! main routine for advanced (linear combinations) intg routines with kernels
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const std::vector<Parameter>& ps);
//@{
//! Advanced (linear combinations) double intg routines with kernels for users
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
                  const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10);
//@}

//@{
/*!
  \deprecated use key-value system for optional arguments (_quad, _order, _method, _symmetry, _isogeo, ...)
*/
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop, const OperatorOnUnknown& opv,
                  const IntegrationMethod& im, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const IntegrationMethod& im,
                  SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const IntegrationMethod& im,
                  SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop, const OperatorOnUnknown& opv,
                  const IntegrationMethod& im, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, const IntegrationMethod& im, bool isogeo,
                  SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, const IntegrationMethod& im, bool isogeo,
                  SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop, const OperatorOnUnknown& opv,
                  QuadRule qr, number_t qo=0, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, QuadRule qr, number_t qo=0,
                  SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, QuadRule qr, number_t qo=0,
                  SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop, const OperatorOnUnknown& opv,
                  bool isogeo, QuadRule qr, number_t qo=0, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, bool isogeo, QuadRule qr, number_t qo=0,
                  SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, bool isogeo, QuadRule qr, number_t qo=0,
                  SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop, const OperatorOnUnknown& opv,
                  SymType st=_undefSymmetry); //!< construct a simple intg bilinear form
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, SymType st);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, SymType st);
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknown& opu, AlgebraicOperator aop, const OperatorOnUnknown& opv,
                  bool isogeo, SymType st=_undefSymmetry); //!< construct a simple intg bilinear form
BilinearForm intg(const GeomDomain& dom, const OperatorOnUnknowns& opus, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& dom, const LcOperatorOnUnknowns& lcopus, bool isogeo, SymType st=_undefSymmetry);

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, const IntegrationMethod& im, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const IntegrationMethod& im, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethod& im, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethod& im, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const IntegrationMethod& im, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, const IntegrationMethod& im, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const IntegrationMethod& im, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethod& im, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethod& im, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const IntegrationMethod& im, SymType st=_undefSymmetry);

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown&, const IntegrationMethods& ims, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const IntegrationMethods& ims, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethods& ims, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethods& ims, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const IntegrationMethods& ims, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, const IntegrationMethods& ims, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  const IntegrationMethods& ims, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethods& ims, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  const IntegrationMethods& ims, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  const IntegrationMethods& ims, SymType st=_undefSymmetry);

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, bool isogeo, QuadRule qr, number_t qo=0, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  bool isogeo, QuadRule qr, number_t qo=0, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  bool isogeo, QuadRule qr, number_t qo=0, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  bool isogeo, QuadRule qr, number_t qo=0, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  bool isogeo, QuadRule qr, number_t qo=0, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, QuadRule qr, number_t qo=0, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  QuadRule qr, number_t qo=0, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  QuadRule qr, number_t qo=0, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  QuadRule qr, number_t qo=0, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  QuadRule qr, number_t qo=0, SymType st=_undefSymmetry);

BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus,
                  bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const IntegrationMethods& ims, bool isogeo, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aop,
                  const OperatorOnUnknown& opv, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknowns& opus, SymType st);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const Kernel& ker, AlgebraicOperator aopv, const OperatorOnUnknown& opv, SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                  const OperatorOnKernel& opker, AlgebraicOperator aopv, const OperatorOnUnknown& opv,
                  SymType st=_undefSymmetry);
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnUnknowns& kopus,
                  SymType st); //!< construct a double intg bilinear form from kernel operators combination
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  SymType st); //!< construct a double intg bilinear form from kernel operators combination and integration methods
BilinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const LcKernelOperatorOnUnknowns& lckopus,
                  const IntegrationMethods& ims, SymType st=_undefSymmetry);
//@}

// -------------------------------------------------------------
// algebraic operations on BilinearForms
// -------------------------------------------------------------
const BilinearForm& operator+(const BilinearForm&);               //!< same bilinear form
BilinearForm operator-(const BilinearForm&);                      //!< opposite of a bilinear form
BilinearForm operator+(const BilinearForm&, const BilinearForm&); //!< sum of bilinear forms
BilinearForm operator-(const BilinearForm&, const BilinearForm&); //!< difference of bilinear forms
BilinearForm operator*(const int_t&, const BilinearForm&);        //!< product(left) by an integer scalar
BilinearForm operator*(const int&, const BilinearForm&);          //!< product(left) by an integer scalar
BilinearForm operator*(const number_t&, const BilinearForm&);     //!< product(left) by an integer scalar
BilinearForm operator*(const real_t&, const BilinearForm&);       //!< product(left) by a real scalar
BilinearForm operator*(const complex_t&, const BilinearForm&);    //!< product(left) by a complex scalar
BilinearForm operator*(const BilinearForm&, const int_t&);        //!< product(right) by an integer scalar
BilinearForm operator*(const BilinearForm&, const int&);          //!< product(right) by an integer scalar
BilinearForm operator*(const BilinearForm&, const number_t&);     //!< product(right) by an integer scalar
BilinearForm operator*(const BilinearForm&, const real_t&);       //!< product(right) by a real scalar
BilinearForm operator*(const BilinearForm&, const complex_t&);    //!< product(right) by a complex scalar
BilinearForm operator/(const BilinearForm&, const int_t&);        //!< division by an integer scalar
BilinearForm operator/(const BilinearForm&, const int&);          //!< division by an integer scalar
BilinearForm operator/(const BilinearForm&, const number_t&);     //!< division by an integer scalar
BilinearForm operator/(const BilinearForm&, const real_t&);       //!< division by a real scalar
BilinearForm operator/(const BilinearForm&, const complex_t&);    //!< division by a complex scalar

//composition of bilinear forms
BasicBilinearForm& operator*(const BasicBilinearForm&, const BasicBilinearForm&); //!< compose two basic bilinear forms
BilinearForm operator*(const BilinearForm&, const BilinearForm&); //!< compose two bilinear forms, have to be basic linear forms

//userBilinearForm construction
BilinearForm userBilinearForm(const GeomDomain& g, const Unknown& u, const Unknown& v, BFFunction bf, ComputationType ct,
                     SymType=_noSymmetry, bool reqIJ=false, bool reqN=false, const IntegrationMethod& im=QuadratureIM(_GaussLegendreRule,3));
BilinearForm userBilinearForm(const GeomDomain& g, const GeomDomain&, const Unknown& u, const Unknown& v, BFFunction bf, ComputationType ct,
                     SymType=_noSymmetry, bool reqIJ=false, bool reqN=false, const IntegrationMethod& im=QuadratureIM(_GaussLegendreRule,3));

// print utility
std::ostream& operator<<(std::ostream&, const BilinearForm&);     //!< output BilinearForm

} // end of namespace xlifepp

#endif /* BILINEAR_FORM_HPP */
