/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ClusterTree.hpp
  \author E. Lunéville
  \since 14 may 2016
  \date 14 may 2016

  \brief Definition of the xlifepp::ClusterTree and xlifepp::ClusterNode classes that manages a cluster of objects (T)
         being constructed from a characteristic point location of objects. Only requirement on T object are the functions

            dimen_t dim(const T&) const;               // returning the dimension of of the characteristic point of location
            real_t coords(const T&, number_t i) const  // returning the i-th coordinates (i=1,2,..) of the characteristic point of location
            BoundingBox boundingBox(const T &)         // returning the xy(z) bounding box of T object

         They are provided for xlifepp classes: Point, FeDof, Element

         ClusterTree<T> is the front class for ClusterNode<T> class that describes a node of the tree
           - each tree node (ClusterNode<T> with child) represents a box containing at least maxInBox_ points
           - each leaf (ClusterNode<T> with no child) represents a box containing at most maxInBox_ points
           - boxes never overlapped
           - the set of leaf numbers vectors is a partition of 1,2,..., nbpt

                         root node (1,2,3,4,5,6)
                              |
                         child|parent
                              |     next          next
                             node ------>  leaf  ------> node
                              |            (2)            |
                         child|parent                child|parent
                              |                           |     next
                             leaf                        leaf  ------> leaf
                            (1,5)                        (3,4)          (6)

            when using bisection algorithm each node has at most two children

     possible usage: T = Point to deal with a cluster of points
                      T = FeDof to deal with a cluster of finite element dofs
                      T = Element to deal with a cluster of finite elements, clustering being built from element centroids

    ClusterNode<T> manages the following important data
         - boundingBox_: a xyz bounding box of the node set by the clustering algorithm, may be not the smallest
         - realBoundingBox_: the xyz smallest bounding box of the node, computed from object supports
         - numbers_: the indices of objects belonging to the box
         - dofNumbers_: the dof numbers of objects belonging to the box
         - elements_: the element pointers supporting the dof numbers

      Important note: the cluster tree may be constructed in two way
                       - numbers_ are built on each node   (to save time)
                       - numbers_ are only built on leaves (to save memory)
                       dofNumbers_ and elements_ vectors are not set during construction

      For T=Point, T=FeDof, dofNumbers is equivalent to numbers_
      For T=Element elements_ vector is straight related to numbers_ vector

      To deal with ClusterTree, the main operations are
         create a cluster tree:  ClusterTree<Point>   ctp(mesh.nodes, _regularBisection, nbox, maxdepth)
                                   ClusterTree<FeDof>   ctf(space.dofs(), _regularBisection, nbox, maxdepth)
                                   ClusterTree<Element> cte(space.elements(), _regularBisection, nbox, maxdepth)
         update bounding boxes:  cte.updateBoundingBoxes();
                                   cte.updateRealBoundingBoxes();
         update some numberings:  cte.updateDofNumbers();
                                   cte.updateNumbers();      //if they have not been stored on creation
                                   cte.updateElements();
         print or save:  cte.print(thePrintStream);
                                   cte.printLeaves(thePrintStream);
                                   cte.saveToFile("cte.txt",true); //export real bounding boxes if set

*/

#ifndef CLUSTER_TREE_HPP
#define CLUSTER_TREE_HPP

#include "config.h"
#include "utils.h"
#include "geometry.h"
#include "space.h"


namespace xlifepp
{

//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const ClusteringMethod& cm);
enum CloseNodeRule {_fmmCloseNodeRule};

//class Element;    //forward declaration
class FeDof;

//======================================================================================================================
//                                   ClusterNode<T> class definition
//======================================================================================================================
/*!
  \class ClusterNode
  describes a node of a hierarchical cluster of T objects (mean Point for instance)
         it manages
         - a pointer to the cloud object (objects_)
         - a xyz bounding box of the node, may be not the smallest
         - a xyz real bounding box of the node if required
         - some pointers to move in the tree (parent_, child_, next_)
         - the indices of objects belonging to the box (numbers_), may be deallocated for node that are not leaf (see storeNodeData_)
         - an optional list of dof numbers
         - an optional list of element pointers

  \note a node is never void
*/
template <typename T>
class ClusterNode
{
  private:
    ClusterNode(const ClusterNode& cn) {}             //!< copy constructor prohibited
    ClusterNode& operator=(const ClusterNode& cn) {}  //!< assign operator prohibited

  public:
    std::vector<T>* objects_;        //!< pointer to the list of objects
    ClusterNode* parent_;            //!< pointer to its parent, if 0 root node
    ClusterNode* child_;             //!< pointer to its first child, if 0 no child
    ClusterNode* next_;              //!< pointer to its brother, if 0 no brother
    number_t depth_;                 //!< depth of node/leaf in tree, root has 0 depth
    mutable std::vector<number_t> numbers_;  //!< list of object numbers related to objects vector (start at 0)
    BoundingBox boundingBox_;        //!< bounding box from characteristic points of objects
    BoundingBox realBoundingBox_;    //!< bounding box from bounding boxes of objects, differs from boundingBox_ if T is larger than a point

    //additional structures
    mutable std::list<ClusterNode<T>*> closeNodes_;//!< list of close nodes, built on demand
    mutable std::vector<number_t> parentNumbers_;  //!< numbers of dof/objects in parent numbering

    //specific to FE computation
    mutable std::vector<number_t> dofNumbers_;     //!< list of dof numbers related to the node
    mutable std::vector<Element*> elements_;       //!< list of element numbers related to the node

    //constructor, destructor
    ClusterNode(std::vector<T>* objs, ClusterNode* p, number_t d, ClusterNode* c=nullptr, ClusterNode* n=nullptr)
      : objects_(objs), parent_(p), child_(c), next_(n), depth_(d) {}
    ~ClusterNode() {clear();}
    void clear();

    //main tools
    number_t dimSpace() const {return dim(*(objects_->begin()));}
    number_t size() const {if(dofNumbers_.empty()) return numbers_.size(); else return dofNumbers_.size();}
    bool isVoid() const {return numbers_.size()==0;}
    void divideNode(ClusteringMethod, number_t, number_t = 0, bool = true, bool = true);   //!< divide set of points along the clustering method (create children)
    void setBoundingBox();                       //!< set the bounding box from characteristic point of objects
    void setRealBoundingBox();                   //!< set the real bounding box, i.e from bounding box of objects
    void updateBoundingBoxes();                  //!< update recursively bounding boxes
    void updateRealBoundingBoxes();              //!< update recursively real bounding boxes
    void updateCloseNodes(CloseNodeRule, real_t =1.);  //!< update recursively closeNodes vector
    std::vector<ClusterNode*> getLeaves() const;       //!< get leave pointers of the tree
    std::list<number_t> getNumbers(bool store) const;  //!< get numbers as a list (recursive) with a storing option
    void updateNumbers();                        //!< update recursively numbers_ vector
    void updateParentNumbers();                  //!< update parentNumbers_ vector
    void updateAllParentNumbers();               //!< update recursively all parentNumbers_ vector
    const BoundingBox& box() const;              //!< return the real bounding box if allocated else return the bounding box
    real_t boxDiameter() const;                  //!< diameter ot the (real) bounding box
    bool isCloseTo(const ClusterNode<T>&, real_t = 1.) const;    //!< test if a node is close to an other one
    const std::vector<number_t>& getParentNumbers() const;       //!< get parentNumbers_ vector

    //specific to finite element
    const std::vector<number_t>& dofNumbers() const;             //!< direct access to dofNumbers vector (no copy)
    const std::vector<Element*>& elements() const;               //!< direct access to element pointers vector (no copy)
    std::vector<number_t> getDofNumbers(bool store=false) const; //!< list of dof numbers related to a node, can build on fly
    std::vector<Element*> getElements(bool store=false) const;   //!< list of element numbers related to a node, can build on fly
    void updateDofNumbers();                                     //!< update recursively dofNumbers
    void updateElements();                                       //!< update recursively elements
    number_t nbDofs() const {return dofNumbers_.size();}

    //print stuff
    void printNode(std::ostream&, bool shift=true) const; //!< print node
    void printNode(PrintStream& os, bool shift=true) const {printNode(os.currentStream(),shift);}
    void print(std::ostream&) const;                      //!< print tree from current node (recursive)
    void print(PrintStream& os) const {print(os.currentStream());}
    void printLeaves(std::ostream&) const;                //!< print leaves of the tree
    void printLeaves(PrintStream& os) const {printLeaves(os.currentStream());}
    void saveToFile(const string_t&, number_t, bool withRealBox=false) const;    //!< save points and bounding boxes or real bounding boxes on file
};

template <typename T>
std::ostream& operator<<(std::ostream& out, const ClusterNode<T>& cn) {
  cn.print(out);
  return out;
}

//test if two Clusters have common geometrical elements, list of elements has to be up to date
template <typename T>
bool hasCommonElts(const ClusterNode<T>& cn1,const ClusterNode<T>& cn2)
{
  if (cn1.elements_.size() == 0)
  {
    where("bool hasCommonElts(ClusterNode, ClusterNode)");
    error("is_void","cn1");
  }
  if (cn2.elements_.size() == 0)
  {
    where("bool hasCommonElts(ClusterNode, ClusterNode)");
    error("is_void","cn2");
  }
  //geometrical criteria using bounding boxes dist(bn1,bn2) > 2*max(hn1,hn2)
  std::vector<Element*>::iterator ite;
  real_t hn = 0.;
  for(ite=cn1.elements_.begin(); ite!=cn1.elements_.end(); ++ite) hn=std::max(hn,(*ite)->geomElt_p->size());
  for(ite=cn2.elements_.begin(); ite!=cn2.elements_.end(); ++ite) hn=std::max(hn,(*ite)->geomElt_p->size());
  if(dist(cn1.box(),cn2.box()) > 2*hn) return false;
  //compare geomelements
  std::set<GeomElement*> gelts;
  for(ite=cn1.elements_.begin(); ite!=cn1.elements_.end(); ++ite) gelts.insert((*ite)->geomElt_p);
  std::set<GeomElement*>::iterator itse=gelts.end();
  for(ite=cn2.elements_.begin(); ite!=cn2.elements_.end(); ++ite)
    if(gelts.find((*ite)->geomElt_p) != itse) return true;
  return false;
}


//======================================================================================================================
//                                   ClusterTree<T> class definition
//======================================================================================================================
/*!
  \class ClusterTree
  frontal class of a tree representation (xlifepp::ClusterNode class) of cloud of objects
         that have a characteristic Point of location
         Be cautious, it uses a pointer to a vector of T that has to be kept in memory. Full copy of objects should be safer
         but it is memory expansive, see later
*/
template <typename T = FeDof>
class ClusterTree
{
  public:
    std::vector<T>* objects_;        //!< pointer to a list of objects
    ClusteringMethod method_;        //!< clustering method (see enum)
    number_t maxInBox_;              //!< maximum number of points in a leaf
    number_t depth;                  //!< depth of the tree, max depth when given >0 in constructor
    bool storeNodeData_;             //!< store data on each node if true
    bool clearObjects_;              //!< if true deallocate vector points_ when clear
    number_t nbNodes;                //!< number of nodes (info)
    number_t nbLeaves;               //!< number of leaves (info)
    bool withOverlap_;               //!< true if dof numbering of nodes overlap (case of a cluster of elements)
    bool noEmptyBox_;                //!< do not keep empty boxes in tree
    CloseNodeRule closeRule_;        //!< rule to decide if two tree nodes are close
    real_t etaClose_;                //!< scalar parameter for the CloseNodeRule

  private:
    ClusterNode<T>* root_;           //!< root node of cluster

  public:
    ClusterTree(const std::vector<T>&, ClusteringMethod, number_t, number_t=0,
                bool=true, bool = true, CloseNodeRule = _fmmCloseNodeRule, real_t =1.);    //!< constructor from a list of objects
    ~ClusterTree();

    ClusterNode<T>* root() {return root_;}

    void setOverlap();
    void updateBoundingBoxes();                  //!< update recursively bounding boxes
    void updateRealBoundingBoxes();              //!< update recursively real bounding boxes
    void updateNumbers();                        //!< update recursively numbers_ vector
    void updateParentNumbers();                  //!< update recursively all parentNumbers_ vector
    void updateCloseNodes();                     //!< update recursively closeNodes vector
    void updateDofNumbers();                     //!< update recursively dofNumbers_ vector
    void updateElements();                       //!< update recursively elements_ vector
    void updateInfo();                           //!< update tree info (depth, number of nodes, ...)

    number_t nbDofs() const
    {if(root_!=nullptr) return root_->dofNumbers_.size(); else return 0;}

    void print(std::ostream&) const;           //!< print tree
    void print(PrintStream& os) const {print(os.currentStream());}
    void printLeaves(std::ostream&) const;     //!< print only leaves of the tree
    void printLeave(PrintStream& os) const {printLeave(os.currentStream());}
    void saveToFile(const string_t& fn, bool withRealBox= false) const; //!< save points and bounding boxes or real bounding boxes on file (withRealBox=true)
};

template <typename T>
std::ostream& operator<<(std::ostream& out, const ClusterTree<T>& ct)
{ct.print(out); return out;}

typedef ClusterTree<FeDof>& DofCluster; //user shortcut

//===============================================================================
//                         ClusterTree member functions
//===============================================================================
//constructor from a list of objects
//   objs: list of objects
//   meth: clustering method: _regularBisection, _boundingBoxBisection, _cardinalityBisection
//   nmax: the maximal number of points in a leaf
//   depth: the maximal depth of tree, if depth >0 the nmax criteria is not used (default=0)
//   store: a flag (true/false) telling to store all informations on each node, memory expansive but late faster (default=true)
//   noEmpty: a flag (true/false) telling to not keep empty boxes in tree
template <typename T>
ClusterTree<T>::ClusterTree(const std::vector<T>& objs, ClusteringMethod meth, number_t nmax, number_t maxdepth,
                            bool store, bool noEmpty, CloseNodeRule cnr, real_t etacnr)
{

  trace_p->push("ClusterTree<T>::ClusterTree(...)");
  number_t n = objs.size();
  if (objs.size()==0) error("is_void","objs");
  if (objs.size() < nmax) warning("is_lesser",objs.size(), nmax);

  objects_ = const_cast<std::vector<T>*>(&objs);
  method_ = meth;
  maxInBox_= nmax;
  closeRule_=cnr;
  etaClose_=etacnr;
  storeNodeData_ = store;
  noEmptyBox_=noEmpty;
  depth= maxdepth;
  clearObjects_=false;
  setOverlap();

  //initialize root node
  root_= new ClusterNode<T>(objects_,0,0);
  root_->numbers_.resize(n);
  std::vector<number_t>::iterator itn=root_->numbers_.begin();
  for(number_t k=0; k<n; k++, ++itn) *itn=k;
  root_->setBoundingBox();
  //create tree
  root_->divideNode(method_, maxInBox_, maxdepth, storeNodeData_, noEmptyBox_);
  updateInfo();
  if(!storeNodeData_) root_->numbers_.clear();
  trace_p->pop();
}

template <typename T>
ClusterTree<T>::~ClusterTree()
{
  if(root_!=nullptr) delete root_;
  if(clearObjects_) delete objects_;
}

//! update recursively bounding boxes
template <typename T>
void ClusterTree<T>::updateBoundingBoxes()
{
  if (root_ == nullptr)
  {
    where("ClusterTree::updateBoundingBoxes()");
    error("null_pointer", "root_");
  }
  if (!storeNodeData_)
  {
    where("ClusterTree::updateBoundingBoxes()");
    error("is_null", "storeNodeData_");
  }
  root_->updateBoundingBoxes();
}

template <typename T>
void ClusterTree<T>::updateRealBoundingBoxes()
{
  if (root_ == nullptr)
  {
    where("ClusterTree::updateRealBoundingBoxes()");
    error("null_pointer", "root_");
  }
  if (!storeNodeData_)
  {
    where("ClusterTree::updateRealBoundingBoxes()");
    error("is_null", "storeNodeData_");
  }
  root_->updateRealBoundingBoxes();
}

template <typename T>
void ClusterTree<T>::updateNumbers()
{
  if (root_ == nullptr)
  {
    where("ClusterTree::updateNumbers()");
    error("null_pointer", "root_");
  }
  root_->updateNumbers();
}

template <typename T>
void ClusterTree<T>::updateParentNumbers()
{
  if (root_ == nullptr)
  {
    where("ClusterTree::updateParentNumbers()");
    error("null_pointer", "root_");
  }
  root_->updateAllParentNumbers();
}

template <typename T>
void ClusterTree<T>::updateCloseNodes()
{
  if (root_ == nullptr)
  {
    where("ClusterTree::updateCloseNodes()");
    error("null_pointer", "root_");
  }
  root_->closeNodes_.push_back(root_);
  root_->updateCloseNodes(closeRule_, etaClose_);
}

template <typename T>
void ClusterTree<T>::updateDofNumbers()
{
  if (root_ == nullptr)
  {
    where("ClusterTree::updateDofNumbers()");
    error("null_pointer", "root_");
  }
  root_->updateDofNumbers();
}

template <typename T>
void ClusterTree<T>::updateElements()
{
  if (root_ == nullptr)
  {
    where("ClusterTree::updateElements()");
    error("null_pointer", "root_");
  }
  root_->updateElements();
}

template <typename T>
void ClusterTree<T>::setOverlap() {withOverlap_=false;}

// update tree info (depth, number of nodes, ...)
template <typename T>
void ClusterTree<T>::updateInfo()
{
  depth=0;
  nbNodes= 0;
  nbLeaves=0;
  if(root_==nullptr) return;  //void tree
  ClusterNode<T>* hn=root_;
  while(hn!=nullptr)
    {
      nbNodes++;
      depth=std::max(depth,hn->depth_);
      if(hn->child_!=nullptr) hn=hn->child_;
      else //no child move to brother or up
        {
          nbLeaves++;
          if(hn->next_!=nullptr) hn=hn->next_;
          else //move up
            {
              while(hn->parent_!=nullptr && hn->parent_->next_==nullptr) hn = hn->parent_;
              if(hn->parent_==nullptr) hn=nullptr;
              else hn=hn->parent_->next_;
            }
        }
    }
}

//! print tree
template <typename T>
void ClusterTree<T>::print(std::ostream& out) const
{
  if(root_!=nullptr) root_->print(out);
  else out<<"cluster tree is void"<<eol;
}

//! print only leaves of the tree
template <typename T>
void ClusterTree<T>::printLeaves(std::ostream& out) const
{
  if(root_!=nullptr) root_->printLeaves(out);
  else out<<"cluster tree is void"<<eol;
}

//! save points and bounding box on file (for graphic representation)
template <typename T>
void ClusterTree<T>::saveToFile(const string_t& fn, bool withRealBox) const
{
  if (root_==nullptr) {warning("null_pointer","root_"); return;}
  if (withRealBox && root_->realBoundingBox_.dim()==0)
  {
    warning("free_warning","realBoundingBoxes are not available, save BoundingBoxes");
    root_->saveToFile(fn, maxInBox_,false);
  }
  else root_->saveToFile(fn, maxInBox_,withRealBox);
}

//===============================================================================
//                         ClusterNode member functions
//===============================================================================

//--------------------------------------------------------
//generic and specialized functions related to ClusterNode
//--------------------------------------------------------
template <typename T>
dimen_t dim(const T&)
{
  error("not_handled","dim(const T&) (-> ClusterNode)");
  return 0;
}
dimen_t dim(const Point&);
dimen_t dim(const FeDof&);
dimen_t dim(const Element&);
//inline dimen_t dim(const Point*& p)    {return dim(*p);}
//inline dimen_t dim(const FeDof*& fed)  {return dim(*fed);}
//inline dimen_t dim(const Element*& elt) {return dim(*elt);}

template <typename T>
real_t coords(const T& p, dimen_t i)
{
  error("not_handled","coords(const T&, Dimen i) (-> ClusterNode)");
  return 0.;
}
real_t coords(const Point&, dimen_t);
real_t coords(const FeDof&, dimen_t);
real_t coords(const Element&, dimen_t);

template <typename T>
BoundingBox boundingBox(const T &)
{
  BoundingBox bb;
  error("not_handled","boundingBox(const T&) (-> ClusterNode)");
  return bb;
}
BoundingBox boundingBox(const Point&);
BoundingBox boundingBox(const FeDof&);
BoundingBox boundingBox(const Element&);

// return real bounding box if allocated, return the bounding box else
template <typename T>
const BoundingBox& ClusterNode<T>::box() const
{
  if(realBoundingBox_.dim()>0) return realBoundingBox_;
  return boundingBox_;
}

// diameter ot the (real) bounding box
template <typename T>
real_t ClusterNode<T>::boxDiameter() const
{
  if(realBoundingBox_.dim()>0) return realBoundingBox_.diameter2();
  return boundingBox_.diameter2();
}

//! clear cluster node (recursive)
template <typename T>
void ClusterNode<T>::clear()
{
  if(child_!=nullptr)  delete child_;
  if(next_!=nullptr)   delete next_;
  numbers_.clear();
  dofNumbers_.clear();
  elements_.clear();
}

//! set the bounding box of a cluster node
template <typename T>
void ClusterNode<T>::setBoundingBox()
{
  number_t d = dimSpace();
  std::vector<number_t>::iterator itn;
  std::vector<real_t> xmin(d,theRealMax), xmax(d,-theRealMax);
  for(itn=numbers_.begin(); itn!=numbers_.end(); ++itn)
    {
      for(number_t k=0; k<d; ++k)
        {
          real_t x=coords((*objects_)[*itn],k+1);
          if(x<xmin[k]) xmin[k]=x;
          if(x>xmax[k]) xmax[k]=x;
        }
    }
  if(d==1) {boundingBox_=BoundingBox(xmin[0],xmax[0]); return;}
  if(d==2) {boundingBox_=BoundingBox(xmin[0],xmax[0],xmin[1],xmax[1]); return;}
  if(d==3) {boundingBox_=BoundingBox(xmin[0],xmax[0],xmin[1],xmax[1], xmin[2],xmax[2]); return;}
  where("ClusterNode<T>::setBoundingBox()");
  error("dim_not_in_range", 1, 3);
}

//! set the real bounding box of a cluster node from bounding boxes of objects, numbers_ has to be allocated !
template <typename T>
void ClusterNode<T>::setRealBoundingBox()
{
  number_t d = dimSpace();
  if (d==1) realBoundingBox_=BoundingBox(0.,0.);
  if (d==2) realBoundingBox_=BoundingBox(0.,0.,0.,0.);
  if (d==3) realBoundingBox_=BoundingBox(0.,0.,0.,0.,0.,0.);
  if (d > 3)
  {
    where("ClusterNode<T>::setRealBoundingBox()");
    error("dim_not_in_range", 1, 3);
  }
  std::vector<number_t>::iterator itn;
  for(itn=numbers_.begin(); itn!=numbers_.end(); ++itn) realBoundingBox_+=boundingBox((*objects_)[*itn]);
}

//! update recursively the real bounding boxes of a cluster node, numbers_ has to be allocated !
template <typename T>
void ClusterNode<T>::updateBoundingBoxes()
{
  setBoundingBox();
  if(child_!=nullptr) child_->updateBoundingBoxes();
  if(next_!=nullptr) next_->updateBoundingBoxes();
}

//! update recursively the real bounding boxes of a cluster node, numbers_ has to be allocated !
template <typename T>
void ClusterNode<T>::updateRealBoundingBoxes()
{
  setRealBoundingBox();
  if(child_!=nullptr) child_->updateRealBoundingBoxes();
  if(next_!=nullptr) next_->updateRealBoundingBoxes();
}

//! test if a node is close to an other one, for the moment use
//!     dist(b1,b2) <= eta*max(diam(b1), diam(b2))
template <typename T>
bool ClusterNode<T>::isCloseTo(const ClusterNode<T>& cn, real_t eta) const
{
  if(&cn==this) return true;
  return  dist(box(),cn.box())<=eta*std::max(boxDiameter(),cn.boxDiameter())+theEpsilon;
}


//! update recursively closeNodes vector
template <typename T>
void ClusterNode<T>::updateCloseNodes(CloseNodeRule cnr, real_t eta)
{
  switch(cnr)
    {
      case _fmmCloseNodeRule:
        {
          typename std::list<ClusterNode<T>*>::iterator itc=closeNodes_.begin();
          for(; itc!=closeNodes_.end(); ++itc) //loop on nodes close to current
            {
              ClusterNode<T>* child_itc = (*itc)->child_;
              while(child_itc!=nullptr) //loop on child of itc
                {
                  ClusterNode<T>* child= child_;
                  while(child!=nullptr) //loop on child of current node
                    {
                      if(child->isCloseTo(*child_itc, eta)) child_itc->closeNodes_.push_back(child);
                      child=child->next_;
                    }
                  child_itc=child_itc->next_;
                }
            }
          ClusterNode<T>* child= child_;
          while(child!=nullptr) //update close nodes of childs
            {
              child->updateCloseNodes(cnr,eta);
              child=child->next_;
            }
        }
        break;
      default:
      where("ClusterNode::updateCloseNodes(...)");
      error("free_error", "ClusterNode::updateCloseNodes, not available close nodes rule");
    }
}

/*! divide set of points along the clustering method (create children) - recursive
    meth: clustering method:
           _regularBisection: split along the maximal direction the current box in two boxes of same size
           _boundingBoxBisection: same spliting as regular method but set the split boxes to their xyz minimal bounding box
           _cardinalityBisection) : split along the maximal direction in two boxes having the same number of points (to 1 near)
    maxinleaf: the maximum number of points in a leaf
    maxdepth: the maximal depth of the tree, if maxdepth=0, depth criteria is not used
    storeNodeData: if true, data (numbers, bounding boxes, ...) are stored for each node else they are only stored for each leaves
    rmEmptyBox: if true empty box are removed from tree

*/
template <typename T>
void ClusterNode<T>::divideNode(ClusteringMethod meth, number_t maxinleaf, number_t maxdepth, bool storeNodeData, bool rmEmptyBox)
{
  if (maxdepth > 0 && depth_ >= maxdepth) return;   // maximal depth criteria, no division
  if (numbers_.size() <= maxinleaf) return;         // max in leaf criteria, no division
  number_t d=dimSpace();
  number_t n= numbers_.size();
  std::vector<T>& objs=*objects_;

  if (meth <= _cardinalityBisection) //bissection methods
  {
    //find direction of bisection
    number_t j=0;
    RealPair b=boundingBox_.bounds(1);
    real_t lmax= b.second-b.first, l;
    for (number_t k=1; k<d; ++k)
    {
      b=boundingBox_.bounds(k+1);
      l=b.second-b.first;
      if(l>lmax) {j=k; lmax=l;}
    }

    //create two children
    ClusterNode<T>* child_sup = new ClusterNode<T>(objects_, this, depth_+1, 0, 0);
    child_ = new ClusterNode<T>(objects_, this, depth_+1, 0, 0);

    //fill children by sorting point numbers with different methods
    switch (meth)
    {
      case _regularBisection:
      case _boundingBoxBisection:
      {
        b=boundingBox_.bounds(j+1);
        lmax= (b.first+b.second)*0.5;
        child_sup->numbers_.reserve(n);
        child_->numbers_.reserve(n);
        std::vector<number_t>::iterator itn;
        for(itn=numbers_.begin(); itn!=numbers_.end(); ++itn)
        {
          real_t x=coords(objs[*itn],j+1);
          if(x>lmax) child_sup->numbers_.push_back(*itn);
          else child_->numbers_.push_back(*itn);
        }
#if __cplusplus >= 201103L    //c++11 enabled
        child_sup->numbers_.shrink_to_fit();
        child_->numbers_.shrink_to_fit();
#else
        std::vector<number_t>(child_sup->numbers_).swap(child_sup->numbers_);
        std::vector<number_t>(child_->numbers_).swap(child_->numbers_);
#endif
        //do not keep void boxes
        if(child_sup->numbers_.size()==0) {delete child_sup; child_sup=nullptr;}
        else
          {
            if(meth== _regularBisection)
              {
                child_sup->boundingBox_ = boundingBox_;
                RealPair& b = child_sup->boundingBox_.bounds(j+1);
                child_sup->boundingBox_.bounds(j+1)= RealPair(lmax,b.second);
              }
            else child_sup->setBoundingBox();
          }
        if(child_->numbers_.size()==0) {delete child_; child_=child_sup; child_sup=nullptr;}
        else
          {
            child_->next_=child_sup;
            if(meth== _regularBisection)
              {
                child_->boundingBox_=boundingBox_;
                RealPair& b = child_->boundingBox_.bounds(j+1);
                child_->boundingBox_.bounds(j+1)= RealPair(b.first,lmax);
              }
            else child_->setBoundingBox();
          }
      }
      break;
      case _cardinalityBisection:
      {
        std::multimap<real_t,number_t> nummap;
        std::vector<number_t>::iterator itn=numbers_.begin();
        for(; itn!=numbers_.end(); ++itn) nummap.insert(std::make_pair(coords(objs[*itn],j+1),*itn));
        std::multimap<real_t,number_t>::iterator itm=nummap.begin();
        number_t n=nummap.size();
        child_->numbers_.resize(n/2);
        child_sup->numbers_.resize(n-n/2);
        itn=child_->numbers_.begin();
        for (number_t k=0; k<nummap.size()/2; ++k,++itm,++itn) *itn = itm->second;
        itn=child_sup->numbers_.begin();
        for (; itm!=nummap.end(); ++itm,++itn) *itn = itm->second;
        //sort to preserve ascending numbering, set bounding box and do not keep void boxes
        if (child_sup->numbers_.size()>0)
        {
          std::sort(child_sup->numbers_.begin(),child_sup->numbers_.end());
          child_sup->setBoundingBox();
        }
        else { delete child_sup; child_sup=nullptr; }
        if (child_->numbers_.size()>0)
        {
          std::sort(child_->numbers_.begin(),child_->numbers_.end());   //sort to preserve ascending numbering
          child_->setBoundingBox();
          child_->next_=child_sup;
        }
        else { delete child_; child_ = child_sup; child_sup=nullptr; }
      }
      break;
      default:
        where("ClusterNode::divideNode(...)");
        error("clustering_method_not_handled", words("cluster method", meth));
    }

    //next division
    if (child_!=nullptr && child_->numbers_.size()>maxinleaf) child_->divideNode(meth, maxinleaf, maxdepth, storeNodeData, rmEmptyBox);
    if (child_sup!=nullptr && child_sup->numbers_.size()>maxinleaf) child_sup->divideNode(meth, maxinleaf, maxdepth, storeNodeData, rmEmptyBox);

    //if storeNodeData is false, clear node data of child if it is not a leaf
    if (!storeNodeData)
    {
      if (child_!=nullptr && child_->child_!=nullptr) child_->numbers_.clear();
      if (child_sup!=nullptr && child_sup->child_!=nullptr) child_sup->numbers_.clear();
    }
  }
  else if (meth==_uniformKdtree || meth==_nonuniformKdtree)//kdtree, split in sub-boxes in each direction  1<= dim <=3
  {
    number_t nd=2, n1=nd, n2=1, n3=1;  //number of partitions in each direction (default nd=2, may be changed in future)
    real_t l1=0.,l2=0.,l3=0., a1=0., a2=0., a3=0.;
    RealPair& b1 = boundingBox_.bounds(1);
    a1=b1.first;
    l1=(b1.second-a1)/n1;
    if (d>1)
    {
      n2=nd;
      RealPair& b2 = boundingBox_.bounds(2);
      a2=b2.first;
      l2=(b2.second-a2)/n2;
    }
    if (d>2)
    {
      n3=nd;
      RealPair& b3 = boundingBox_.bounds(3);
      a3=b3.first;
      l3=(b3.second-a3)/n3;
    }
    std::vector<ClusterNode<T>*> childs(n1*n2*n3);
    ClusterNode<T>* prevchild=nullptr;
    for (number_t k=0; k<n3; k++)  //create childs
    {
      BoundingBox bb=boundingBox_;  //copy current bounding box
      if (d>2) bb.bounds(3) = RealPair(a3+k*l3,a3+(k+1)*l3);
      for (number_t j=0; j<n2; j++)
      {
        if (d>1) bb.bounds(2) = RealPair(a2+j*l2,a2+(j+1)*l2);
        for (number_t i=0; i<n1; i++) //create children ijk
        {
          ClusterNode<T>* child=new ClusterNode<T>(objects_, this, depth_+1, 0, 0);
          bb.bounds(1) = RealPair(a1+i*l1,a1+(i+1)*l1);
          child->boundingBox_=bb;
          child->numbers_.reserve(n);
          if (prevchild!=nullptr) prevchild->next_=child;  //update next
          prevchild=child;
          childs[i*n2*n3+j*n3+k]=child;
        }
      }
    }
    //update objects in child
    child_=childs[0];
    std::vector<number_t>::iterator itn;
    for (itn=numbers_.begin(); itn!=numbers_.end(); ++itn)
    {
      number_t i=0,j=0,k=0;
      i= std::min(number_t(std::max(int_t(std::floor((coords(objs[*itn],1)-a1)/l1)),int_t(0))),n1-1);
      if(d>1) j= std::min(number_t(std::max(int_t(std::floor((coords(objs[*itn],2)-a2)/l2)),int_t(0))),n2-1);
      if(d>2) k= std::min(number_t(std::max(int_t(std::floor((coords(objs[*itn],3)-a3)/l3)),int_t(0))),n3-1);
      childs[i*n2*n3+j*n3+k]->numbers_.push_back(*itn);
    }
    //resize child->numbers_, update bondingbox if required and divide child
    typename std::vector<ClusterNode<T>*>::iterator itc=childs.begin();
    for (; itc!=childs.end(); ++itc)
    {
#if __cplusplus >= 201103L    //c++11 enabled
      (*itc)->numbers_.shrink_to_fit();
#else
      std::vector<number_t>((*itc)->numbers_).swap((*itc)->numbers_);
#endif
      number_t nbo= (*itc)->numbers_.size();
      if (nbo > 0)
      {
        if (meth==_nonuniformKdtree)  //resize bounding box to the minimal one
        {
          real_t xmin[3]= {theRealMax, theRealMax,theRealMax}, xmax[3]= {-theRealMax,-theRealMax,-theRealMax};
          for (itn=(*itc)->numbers_.begin(); itn!=(*itc)->numbers_.end(); ++itn)
            for (number_t i=0; i<d; ++i)
            {
              real_t xi=coords(objs[*itn],i+1);
              xmin[i]=std::min(xmin[i],xi);
              xmax[i]=std::max(xmax[i],xi);
            }
          if (d==1) (*itc)->boundingBox_= BoundingBox(xmin[0],xmax[0]);
          if (d==2) (*itc)->boundingBox_= BoundingBox(xmin[0],xmax[0],xmin[1],xmax[1]);
          if (d==3) (*itc)->boundingBox_= BoundingBox(xmin[0],xmax[0],xmin[1],xmax[1],xmin[2],xmax[2]);
        }
        //divide if enough objects in
        if (nbo >maxinleaf)(*itc)->divideNode(meth, maxinleaf, maxdepth, storeNodeData, rmEmptyBox);
        if (!storeNodeData && (*itc)->child_!=nullptr)(*itc)->numbers_.clear();
      }
    }
    // remove empty childs (default behavior)
    if (rmEmptyBox)
    {
      itc=childs.begin();
      for (; itc!=childs.end(); ++itc) //resize child->numbers_ and divide child
        if ((*itc)->numbers_.size()==0) {(*itc)->next_=0; delete *itc; *itc=0;}
      itc=childs.begin();
      prevchild=nullptr; child_=nullptr;
      for (; itc!=childs.end(); ++itc) //update next pointers
      {
        if (*itc!=nullptr)
        {
          (*itc)->next_=nullptr;
          if (prevchild==nullptr) {child_=*itc; prevchild=child_;}
          else {prevchild->next_=*itc; prevchild=*itc;}
        }
      }
    }
  }
  else
  {
    where("ClusterNode::divideNode(...)");
    error("clustering_method_not_handled", words("cluster method", meth));
  }
}

//controlled access to dofNumbers
template <typename T>
const std::vector<number_t>& ClusterNode<T>::dofNumbers() const
{
  if(!dofNumbers_.empty()) return dofNumbers_;
  return numbers_;
}

//controlled access to elements
template <typename T>
const std::vector<Element*>& ClusterNode<T>::elements() const
{
  if (elements_.empty())
  {
    where("ClusterNode::elements()");
    error("is_void","elements_");
  }
  return elements_;
}

// get elements, built in if not exist
template <typename T>
std::vector<Element*> ClusterNode<T>::getElements(bool store) const
{
  if (elements_.size()>0) return elements_;
  where("ClusterNode::getElements");
  error("is_void","elements_");
  return std::vector<Element*>();
}
template<> std::vector<Element*> ClusterNode<Element>::getElements(bool store) const;   // list of elements pointers related to a ClusterNode<Element>
template<> std::vector<Element*> ClusterNode<FeDof>::getElements(bool store) const;     // list of elements pointers related to a ClusterNode<FeDof>


template <typename T>
std::vector<number_t> ClusterNode<T>::getDofNumbers(bool store) const
{
  if (dofNumbers_.size()>0) return dofNumbers_;
  where("ClusterNode::getDofNumbers(bool)");
  error("is_void","dofNumbers_");
  return std::vector<number_t>();
}
template<> std::vector<number_t> ClusterNode<Element>::getDofNumbers(bool store) const; // list of dof numbers related to a ClusterNode<Element>
template<> std::vector<number_t> ClusterNode<FeDof>::getDofNumbers(bool store) const;   // list of dof numbers related to a ClusterNode<FeDof>
template<> std::vector<number_t> ClusterNode<Point>::getDofNumbers(bool store) const;   // list of dof numbers related to a ClusterNode<Point>

// update recursively dof numbers related to a node
template<typename T>
void ClusterNode<T>::updateDofNumbers()
{
  if(dofNumbers_.size()>0) return;  //up to date
  getDofNumbers(true);
  if(child_!=nullptr) child_->updateDofNumbers();
  if(next_!=nullptr) next_->updateDofNumbers();
}

// update  recursively elements related to a node
template<typename T> void ClusterNode<T>::updateElements()
{
  if(elements_.size()>0) return;  //up to date
  getElements(true);
  if(child_!=nullptr) child_->updateElements();
  if(next_!=nullptr) next_->updateElements();
}

//list of all leaves of the tree
template <typename T>
std::vector<ClusterNode<T>*> ClusterNode<T>::getLeaves() const
{
  std::vector<ClusterNode<T>*> leaves;
  ClusterNode<T>* cn =const_cast<ClusterNode<T>*>(this);
  while(cn!=nullptr)
    {
      while(cn->child_!=nullptr) cn=cn->child_;
      leaves.push_back(cn);
      while(cn->next_==nullptr && cn->parent_!=nullptr) cn=cn->parent_;
      if(cn->next_!=nullptr) cn=cn->next_;
      if(cn==this) cn=nullptr;
    }
  return leaves;
}

/*! get numbers
      if numbers_ vector already exists return it as a list
      else build the list recursively
      when store is true, the number lists are stored
*/
template <typename T>
std::list<number_t> ClusterNode<T>::getNumbers(bool store) const
{
  std::list<number_t> nums;
  if(numbers_.size()>0)
    {
      nums.assign(numbers_.begin(),numbers_.end());
      return nums;
    }
  //built nums from children
  ClusterNode<T>* cn = child_;
  while(cn!=nullptr)
    {
      if(nums.empty()) nums=cn->getNumbers(store);
      else
        {
          std::list<number_t> numcn=cn->getNumbers(store);
          nums.merge(numcn);
        }
      cn=cn->next_;
    }
  //sort and store result
  if(!nums.empty())
    {
      nums.sort();
      if(store) numbers_.assign(nums.begin(),nums.end());
    }
  return nums;
}

//! update recursively numbers_ vectors if they are are empty
template <typename T>
void ClusterNode<T>::updateNumbers()
{
  if(numbers_.empty()) getNumbers(true);
  return;
}

/*! update parentNumbers_ vector of current node
    parentNumbers[i] gives the rank of the ith object in parent numbering
*/
template <typename T>
void ClusterNode<T>::updateParentNumbers()
{
  if(!parentNumbers_.empty()) return;  //already up to date
  if(parent_==nullptr)  //root
  {
      parentNumbers_=trivialNumbering<number_t>(1,size());
      return;
  }
  //non root, has parent
  std::vector<number_t>* nump=&parent_->dofNumbers_;
  if(nump->empty()) nump=&parent_->numbers_;
  std::map<number_t,number_t> mapNum;
  std::vector<number_t>::iterator it=nump->begin(), itn;
  for(number_t k=1; k<=nump->size(); ++k, ++it) mapNum[*it]=k;  // ! parent index starts from 1
  std::vector<number_t>* num=&dofNumbers_;
  if(num->empty()) num=&numbers_;
  parentNumbers_.resize(num->size());
  itn=parentNumbers_.begin();
  for(it=num->begin(); it!=num->end(); ++itn, ++it) *itn=mapNum[*it];
}

/*! update recursively parentNumbers_ vectors of all nodes
    parentNumbers[i] gives the rank of the ith object in parent numbering
*/
template <typename T>
void ClusterNode<T>::updateAllParentNumbers()
{
  if(!parentNumbers_.empty()) return;  //already up to date
  updateParentNumbers();
  ClusterNode<T>* hn=child_;
  while(hn!=nullptr)
    {
      hn->updateParentNumbers();
      hn=hn->next_;
    }
}

/*! get parentNumbers_ vector
    parentNumbers[i] gives the rank of the ith object in parent numbering
    if does not exist, built on the fly
*/
template <typename T>
const std::vector<number_t>& ClusterNode<T>::getParentNumbers() const
{
  if(parentNumbers_.empty()) const_cast<ClusterNode<T>*>(this)->updateParentNumbers();
  return parentNumbers_;
}

//! print node
template <typename T>
void ClusterNode<T>::printNode(std::ostream& out, bool shift) const
{
  number_t d=dimSpace();
  if(shift && depth_>0) out<<string_t(2*depth_,' ');
  out<<"depth "<<depth_<<" bounding box: ";
  for(number_t k=0; k<d; k++)
    {
      out<<"["<<boundingBox_.bounds(k+1).first<<","<<boundingBox_.bounds(k+1).second<<"]";
      if(k<d-1) out<<" X ";
    }
  if(numbers_.size()<= theVerboseLevel) out<<" numbers = "<<numbers_;
  else
    {
      out<<" numbers = [";
      for(number_t k=0; k<theVerboseLevel; k++) out<<numbers_[k]<<",";
      out<<"...]";
    }
  if(dofNumbers_.size()<= theVerboseLevel) out<<" dof numbers = "<<dofNumbers_;
  else
    {
      out<<" dof numbers = [";
      for(number_t k=0; k<theVerboseLevel; k++) out<<dofNumbers_[k]<<",";
      out<<"...]";
    }
  if(elements_.size()>0)
    {
      out<<" element numbers = ["<<elements_[0]->number();
      for(number_t k=1; k<std::min(theVerboseLevel,elements_.size()); k++) out<<", "<<elements_[k]->number();
      if(elements_.size()>theVerboseLevel) out<<", ...";
      out<<"]";
    }
  if(closeNodes_.size()>0 && theVerboseLevel > 5)
    {
      typename std::list<ClusterNode<T>*>::const_iterator itc = closeNodes_.begin();
      out<<" closeNodes ("<<closeNodes_.size()<<") = ("<<(*itc)->boundingBox_.asString();
      itc++;
      while(itc!=closeNodes_.end())
        {
          out<<", "<<(*itc)->boundingBox_.asString();
          ++itc;
        }
      out<<")";
    }
  out<<eol;
}

// print tree
template <typename T>
void ClusterNode<T>::print(std::ostream& out) const
{
  if(theVerboseLevel==0) return;
  printNode(out,true);
  if(child_!=nullptr) child_->print(out);
  if(next_!=nullptr) next_->print(out);
}

//print all leaves
template <typename T>
void ClusterNode<T>::printLeaves(std::ostream& out) const
{
  std::vector<ClusterNode<T>*> leaves=getLeaves();
  typename std::vector<ClusterNode<T>*>::iterator itl=leaves.begin();
  for(; itl!=leaves.end(); ++itl)(*itl)->printNode(out,false);
}

//save points and bounding box on file (for graphic representation)
template <typename T>
void ClusterNode<T>::saveToFile(const string_t& fn, number_t nmin, bool withRealBox) const
{
  std::ofstream out(fn.c_str());
  number_t d=dimSpace();

  //print points location as x y z
  if(numbers_.size()>0)
    {
      std::vector<number_t>::const_iterator itn=numbers_.begin();
      out<<numbers_.size()<<" "<<d<<" "<<nmin<<eol;
      for(; itn!=numbers_.end(); ++itn)
        {
          for(dimen_t i=1; i<=d; i++) out<<coords((*objects_)[*itn],i)<<" ";
          out<<eol;
        }
    }
  else //numbers_ not allocated, construct it
    {
      std::list<number_t> nums=getNumbers(false);
      std::list<number_t>::iterator itn=nums.begin();
      out<<nums.size()<<" "<<d<<eol;
      for(; itn!=nums.end(); ++itn)
        {
          for(dimen_t i=1; i<=d; i++) out<<coords((*objects_)[*itn],i)<<" ";
          out<<eol;
        }
    }

  //number of nodes
  number_t nbnode=0;
  const ClusterNode* cn=this;
  while(cn!=nullptr)   //travel tree
    {
      nbnode++;
      if(cn->child_!=nullptr) cn=cn->child_;
      else
        {
          if(cn->next_!=nullptr) cn=cn->next_;
          else
            {
              while(cn->parent_!=this && cn->parent_->next_==nullptr) cn=cn->parent_;
              if(cn->parent_==this) cn=nullptr;
              else cn=cn->parent_->next_;
            }
        }
    }
  //print bounding boxes as: depth xmin ymin (zmin) xmax ymax (zmax)
  out<<nbnode<<eol;
  cn=this;
  while(cn!=nullptr)   //travel tree
    {
      if(cn->child_!=nullptr) out<<cn->depth_<<" ";
      else out<<-int_t(cn->depth_)<<" ";
      if(withRealBox)
        {
          for(number_t k=0; k<d; k++) out<<cn->realBoundingBox_.bounds(k+1).first<<" ";
          for(number_t k=0; k<d; k++) out<<cn->realBoundingBox_.bounds(k+1).second<<" ";
        }
      else
        {
          for(number_t k=0; k<d; k++) out<<cn->boundingBox_.bounds(k+1).first<<" ";
          for(number_t k=0; k<d; k++) out<<cn->boundingBox_.bounds(k+1).second<<" ";
        }

      out<<eol;
      if(cn->child_!=nullptr) cn=cn->child_;
      else
        {
          if(cn->next_!=nullptr) cn=cn->next_;
          else
            {
              while(cn->parent_!=this && cn->parent_->next_==nullptr) cn=cn->parent_;
              if(cn->parent_==this) cn=0;
              else cn=cn->parent_->next_;
            }
        }
    }
  out.close();
}

} // end of namespace xlifepp

#endif /* CLUSTER_TREE_HPP */
