/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file HMatrix.hpp
  \author E. Lunéville
  \since 14 may 2016
  \date 14 may 2016

  \brief Definition of the xlifepp::HMatrix and xlifepp::HMatrixNode classes

  xlifepp::HMatrix and HMatrixNode classes manage a tree representation of a matrix
           T its numerical type, I the type of row/col indexation
           a tree node represents a block matrix being
             either a HMatrix (not a leaf, child_!=nullptr)
             or a leaf (child_==nullptr) that can be a LargeMatrix (dense, sparse or skyline) -> mat_!=nullptr
                                           or an ApproximateMatrix  (low rank, fmm, ...) -> appmat_!=nullptr

           The tree matrix representation is based on a row and column numberings given through ClusterNode pointers
           HMatrix is the front end class and HMatrixNode class does the job

           Different methods to construct the cluster matrix are possible:

           the original "HMatrix" method from Hackbush et all, manages dense matrices and low rank matrices,
           the choice being done by a simple criteria based on the row/col bounding boxes embedded by ClusterNode pointers

           Note that this class does not fill the matrix but only create the HMatrix structure
 */

#ifndef H_MATRIX_HPP
#define H_MATRIX_HPP

#include "config.h"
#include "utils.h"
#include "largeMatrix.h"
#include "ClusterTree.hpp"
#include "ApproximateMatrix.hpp"

namespace xlifepp
{

enum HMatrixMethod {_standardHM, _denseHM};
enum HMAdmissibilityRule {_noRule, _boxesRule};

/*!
   \class HMatrixNode
   describes a node of hierarchical representation of matrix
*/
template <typename T, typename I>
class HMatrixNode
{
public:
  HMatrixNode<T, I>* parent_;    //!< pointer to its parent, if 0 root node
  HMatrixNode<T, I>* child_;     //!< pointer to its first child, if 0 no child
  HMatrixNode<T, I>* next_;      //!< pointer to its brother, if 0 no brother
  number_t depth_;               //!< depth of node/leaf in tree, root has 0 depth
  real_t eta_=1.;                //!< ratio in the criteria diam(Br) < 2*eta*dist(Br,Bc)  (default eta = 1)
  ClusterNode<I>* rowNode_;      //!< row cluster node to access to row numbering
  ClusterNode<I>* colNode_;      //!< column cluster node to access to col numbering
  LargeMatrix<T>* mat_;          //!< pointer to a LargeMatrix
  ApproximateMatrix<T>* appmat_; //!< pointer to an ApproximateMatrix
  bool admissible_;              //!< block can be approximated
  number_t rowSub_, colSub_;     //!< sub block numbering (1,1), (1,2), (2,1) or (2,2)
  bool isDiag_;                  //!< boolean flag telling if matrix node is located on the diagonal
  FactorizationType factorization_; //!< one of _noFactorization, _lu, _ldlt, _ldlstar

  static bool counterOn;         //!< flag to activate the counting of some operations (default: false)
  static number_t counter;       //!< to count the number of call of operations
  static void initCounter(number_t n = 0) //! init the counter and enable it
  {
    counter = n;
    counterOn = true;
  }
  static void stopCounter()               //! disable counter
  {
    counterOn = false;
  }

  //constructor, destructor
  HMatrixNode() //!default constructor
    : parent_(nullptr), child_(nullptr), next_(nullptr), depth_(0), mat_(nullptr), appmat_(nullptr) {}
  HMatrixNode(HMatrixNode<T, I>* pa, HMatrixNode<T, I>* ch, HMatrixNode<T, I>* ne, number_t d,
              ClusterNode<I>* rcn, ClusterNode<I>* ccn,  number_t rs, number_t cs,
              LargeMatrix<T>* m, ApproximateMatrix<T>* am, real_t et) //!full constructor
    : parent_(pa), child_(ch), next_(ne), depth_(d), rowNode_(rcn), colNode_(ccn), mat_(m),
      appmat_(am), admissible_(true), rowSub_(rs), colSub_(cs), factorization_(_noFactorization), eta_(et) {}
  HMatrixNode(HMatrixNode<T, I>* pa, number_t d, real_t et)                                 //! node constructor
    : parent_(pa), child_(nullptr), next_(nullptr), depth_(d), rowNode_(nullptr), colNode_(nullptr),
      mat_(nullptr), appmat_(nullptr), admissible_(true), rowSub_(0), colSub_(0), factorization_(_noFactorization), eta_(et){}
  HMatrixNode(LargeMatrix<T>* m, HMatrixNode<T, I>* pa, number_t d, real_t et)              //! LargeMatrix leaf constructor
    : parent_(pa), child_(nullptr), next_(nullptr), depth_(d), rowNode_(nullptr), colNode_(nullptr),
      mat_(m), appmat_(nullptr), admissible_(true), rowSub_(0), colSub_(0), factorization_(_noFactorization), eta_(et){}
  HMatrixNode(ApproximateMatrix<T>* am, HMatrixNode<T, I>* pa, number_t d, real_t et)       //! ApproximateMatrix leaf constructor
    : parent_(pa), child_(nullptr), next_(nullptr), depth_(d), rowNode_(nullptr), colNode_(nullptr),
      mat_(nullptr), appmat_(am), admissible_(true), rowSub_(0), colSub_(0), factorization_(_noFactorization), eta_(et) {}
  HMatrixNode(const HMatrixNode<T, I>&);                  //!< copy constructor (full copy except ClusterNode pointers)
  HMatrixNode<T, I>& operator=(const HMatrixNode<T, I>&); //!< assign operator  (full copy except ClusterNode pointers)
  ~HMatrixNode()
  {
    clear();
  }

  void clear();                               //!< clear all except ClusterNode pointers
  void clearMatrices();                       //!< clear (deallocate) only matrices
  void copy(const HMatrixNode<T, I>&);        //!< copy all (full copy), do not clear

  //access to some properties
  number_t numberOfRows() const               //! return number of rows counted in T
  {
    if(rowNode_ != nullptr) return rowNode_->size();
    else return 0;
  }
  number_t numberOfCols() const               //! return number of columns counted in T
  {
    if(colNode_ != nullptr) return colNode_->size();
    else return 0;
  }
  dimPair dimValues() const                   //! return dimensions of values, (1,1) when scalar
  {
    return dimPair(rowSub_, colSub_);
  }
  number_t numberOfChilds() const;            //!< return the number of childs
  std::vector<HMatrixNode<T, I>* > childsMatrix(number_t&, number_t&) const; //!< all child pointers as a matrix of HMatrixNode pointers, row access storage
  HMatrixNode<T, I>* child(number_t d = 1);   //!< access to child at depth d

  //building stuff
  void divide(number_t rmin, number_t cmin, number_t maxdepth = 0, HMAdmissibilityRule admRule = _boxesRule, bool sym = false); //!< split node according to a splitting rule
  void getLeaves(std::list<HMatrixNode<T, I>* >&, bool = true) const;                 //!< get leaves as a list

  //various stuff
  void setClusterRow(ClusterNode<I>*); //!< update row cluster node pointers (recursive)
  void setClusterCol(ClusterNode<I>*); //!< update col cluster node pointers (recursive)
  number_t nbNonZero() const;          //!< number of T coefficients used to represent the matrix node

  //conversion stuff
  void toHierarchical(number_t depth = 1);                                  //!< move to hierarchical representation  up to the given depth
  void toLargeMatrix(StorageType st = _dense, AccessType at = _row);        //!< move to LargeMatrix using the given storage
  void toApproximateMatrix(MatrixApproximationType = _lowRankApproximation,
                           number_t rank = 0, real_t eps = theTolerance);   //!< move to ApproximateMatrix

  //algebraic stuff
  std::vector<T>& multMatrixVectorNode(const std::vector<T>&, std::vector<T>&, SymType symt = _noSymmetry) const; //!< matrix vector product for a leaf
  std::vector<T>& multMatrixVector(const std::vector<T>&, std::vector<T>&, SymType symt = _noSymmetry) const;    //!< matrix vector product (recursive)
//    std::vector<T>& multMatrixVector2(const std::vector<T>&, std::vector<T>&, SymType symt=_noSymmetry) const;     //!< matrix vector product (non recursive)
  void lu();                           //!< LU factorization
  void multMatrixAdd(HMatrixNode<T, I>&, HMatrixNode<T, I>&, T c = T(1.)); //< performs +=c*A*B
  void lowerSolve(HMatrixNode<T, I>& B) const;   //!< solve L*X=B, L the lower triangular part of current part
  void upperSolve(HMatrixNode<T, I>& B) const;   //!< solve U*X=B, U the upper triangular part of current part
  void lowerLeftSolve(HMatrixNode<T, I>& B) const; //!< solve X*L=B, L the lower triangular part of current part
  void upperLeftSolve(HMatrixNode<T, I>& B) const; //!< solve X*U=B, U the upper triangular part of current part

  //norm stuff
  real_t norm2() const;                //!< Frobenius norm
  real_t norminfty() const;            //!< infinite norm

  //print stuff
  void printNode(std::ostream&, bool = true) const; //!< print current node contents
  void printNode(PrintStream& os, bool b=true) const {print(os.currentStream(), b);}
  void print(std::ostream&) const;                 //!< print tree from current node
  void print(PrintStream& os) const {print(os.currentStream());}
  void printStructure(std::ostream&, number_t, number_t, bool all = false, bool shift = false) const; //!< print matrix node structure
  void printStructure(PrintStream& os, number_t n1, number_t n2, bool all = false, bool shift = false) const
  {printStructure(os.currentStream(), n1, n2, all, shift);}

}; // end of class HMatrixNode definition

template <typename X, typename J>
std::ostream& operator<<(std::ostream& os, const HMatrixNode<X, J>& hm)
{
  hm.print(os);
  return os;
}

//=================================================================================================================
/*!
   \class HMatrix
   describes a matrix represented by a hierarchical structure (tree), each node being a block matrix
          a leaf is either a standard matrix or an approximate matrix
*/
//=================================================================================================================
template <typename T, typename I>
class HMatrix
{
private:
  HMatrixNode<T, I>* root_;     //!< root node of the tree representation
  ClusterTree<I>* rowCT_;       //!< row cluster tree
  ClusterTree<I>* colCT_;       //!< col cluster tree

public:
  string_t name;                //!< optional name, useful for documentation
  ValueType valueType_;         //!< type of values (real, complex)
  StrucType strucType_;         //!< structure of values (scalar, vector, matrix)
  HMatrixMethod method_;        //!< method to define admissible block
  HMAdmissibilityRule admRule_; //!< block admissible rule
  real_t eta_;                  //!< ratio in the criteria diam(Br) < 2*eta*dist(Br,Bc)  (default eta = 1)
  number_t rowmin_, colmin_;    //!< minimum size of block matrix -> not split if nbrow < rowmin and nbcol < colmin
  SymType sym;                  //!< type of symmetry (if has symmetry, only lower part is stored)
  number_t depth;               //!< maximal depth (info)
  number_t nbNodes;             //!< number of nodes (info)
  number_t nbLeaves;            //!< number of leaves (info)
  number_t nbAdmissibles;       //!< number of admissible blocks (info)
  number_t nbAppMatrices;       //!< number of approximate matrices (info)
  FactorizationType factorization_; //!< one of _noFactorization, _lu, _ldlt, _ldlstar; default is _noFactorization


  //constructor/destructor
  HMatrix();
  HMatrix(ClusterTree<I>&, ClusterTree<I>&,  number_t, number_t, number_t = 0,
          const string_t& = "", SymType = _noSymmetry, HMatrixMethod = _standardHM, HMAdmissibilityRule = _boxesRule, real_t = 1); //!< main constructor
  HMatrix(const HMatrix<T, I>&);                 //!< copy constructor
  HMatrix<T, I>& operator=(const HMatrix<T, I>&); //!< assign operator
  ~HMatrix()
  {
    clear();
  }

  //building stuff
  void buildTree();                          //!< build the tree
  void updateInfo();                         //!< update tree info (depth, number of nodes, ...)
  void load(const LargeMatrix<T>&, HMApproximationMethod = _noHMApproximation); //!< load Hmatrix<T> from LargeMatrix<T>
  void copy(const HMatrix<T, I>&);           //!< copy all (full copy), do not clear
  void clear();                              //!< clear all and deallocate matrix pointers
  void clearMatrices()                       //! clear matrices
  {
    if(root_ != nullptr) root_->clearMatrices();
  }

  //access to some properties
  number_t numberOfRows() const;            //!< return number of rows counted in T
  number_t numberOfCols() const;            //!< return number of cols counted in T
  dimPair dimValues() const;                //!< return dimensions of values, (1,1) when scalar
  const ClusterTree<I>* rowTree() const     //! access to row cluster tree pointer
  {
    return rowCT_;
  }
  const ClusterTree<I>* colTree() const     //! access to col cluster tree pointer
  {
    return colCT_;
  }
  std::pair<number_t, number_t> averageSize() const; //!< average size of leaves
  number_t averageRank() const;                      //!< average rank of admissible leaves
  HMatrixNode<T, I>* root() { return root_; }        //!< access to root node
  HMatrixNode<T, I>* child(number_t d = 1)           //! access to child at depth d
  {
    if(root_ == nullptr) return nullptr;
    else return root_->child(d);
  }


  //various stuff
  void setClusterRow(ClusterTree<I>*); //!< change the row ClusterTree pointer
  void setClusterCol(ClusterTree<I>*); //!< change the col ClusterTree pointer
  number_t nbNonZero() const           //! number of T coefficients used to represent the matrix
  {
    if(root_ != nullptr) return root_->nbNonZero();
    else return 0;
  }
  std::list<HMatrixNode<T, I>* > getLeaves(bool = true) const;            //!< get all leaves
  std::list<HMatrixNode<T, I>* > getLeaves(AccessType, number_t,
      number_t&, bool = true) const;  //!< get all leaves ordered by row/col to avoid data races
  void initCounter(number_t n = 0);   //!< initialize internal counter of HMatrixNode<T,I> for counting some operations
  void stopCounter();                 //!< stop internal counter of HMatrixNode<T,I>

  //HMatrix conversion
  LargeMatrix<T> toLargeMatrix(StorageType st = _dense, AccessType at = _row) const; //!< convert HMatrix to LargeMatrix
  void addFELargeMatrix(const LargeMatrix<T>&);                                   //!< add FE LargeMatrix to current HMatrix

  //algebraic stuff
  std::vector<T>& multMatrixVector(const std::vector<T>&, std::vector<T>&) const;        //!< matrix vector product (recursive)
  std::vector<T>& multMatrixVectorOmp(const std::vector<T>&, std::vector<T>&) const;     //!< matrix vector product (non recursive-omp)
  void lu();                                        //!< LU Factorization
  real_t norm2() const;                             //!< Frobenius norm
  real_t norminfty() const;                         //!< infinite norm

  //print and export stuff
  void print(std::ostream&) const;
  void print(PrintStream& os) const {print(os.currentStream());}
  void printSummary(std::ostream&) const;
  void printSummary(PrintStream& os) const {printSummary(os.currentStream());}
  void printStructure(std::ostream&, bool all = false, bool shift = false) const; //!< print matrix structure
  void printStructure(PrintStream& os, bool all = false, bool shift = false) const
   {printStructure(os.currentStream(),all, shift);}
  void saveStructureToFile(const string_t&) const;    //!< save matrix structure to file
}; // end of class HMatrix definition

template <typename X, typename J>
std::ostream& operator<<(std::ostream& os, const HMatrix<X, J>& hm)
{
  hm.print(os);
  return os;
}

template <typename I>
bool blockAdmissible(ClusterNode<I>*, ClusterNode<I>*, HMAdmissibilityRule, real_t eta = 1.); //!< admissibility rule for a cluster node product

//===============================================================================================================
//                                    implementation of HMatrixNode member functions
//===============================================================================================================

// copy constructor (full copy)
template <typename T, typename I>
HMatrixNode<T, I>::HMatrixNode(const HMatrixNode<T, I>& hm)
{
  copy(hm);
}

// assign operator
template <typename T, typename I>
HMatrixNode<T, I>& HMatrixNode<T, I>::operator=(const HMatrixNode<T, I>& hm)
{
  if(&hm == this) return *this;
  clear();
  copy(hm);
}

//clear all except the rowNode_ and colNode_ pointers
template <typename T, typename I>
void HMatrixNode<T, I>::clear()
{
  HMatrixNode<T, I>* hn = child_, *hnp;
  while(hn != nullptr) //delete all childs
    {
      hnp = hn;
      hn = hn->next_;
      delete hnp;
    }
  //update parent childs
  if(parent_ != nullptr)
    {
      hn = parent_->child_;
      while(hn != this) {hnp = hn; hn = hn->next_;}
      if(hn == this) parent_->child_ = next_; //first child
      else hnp->next_ = next_;
    }
  //delete matrices
  if(mat_ != nullptr)    delete mat_;
  if(appmat_ != nullptr) delete appmat_;
  //reset pointers
  child_ = nullptr; next_ = nullptr; mat_ = nullptr; appmat_ = nullptr;
  parent_ = nullptr; depth_ = 0;
  rowNode_ = nullptr; colNode_ = nullptr;
}

// clear (deallocate) only matrices
template <typename T, typename I>
void HMatrixNode<T, I>::clearMatrices()
{
  HMatrixNode<T, I>* hn = child_;
  while(hn != nullptr) //clear matrices of childs
    {
      hn->clearMatrices();
      hn = hn->next_;
    }
  if(mat_ != nullptr)    delete mat_;
  if(appmat_ != nullptr) delete appmat_;
  mat_ = nullptr; appmat_ = nullptr;
}

//copy all (full copy), do not clear
template <typename T, typename I>
void HMatrixNode<T, I>::copy(const HMatrixNode<T, I>& hm)
{
  depth_ = hm.depth_;
  eta_=hm.eta_;
  admissible_ = hm.admissible_;
  rowSub_ = hm.rowSub_;  colSub_ = hm.colSub_;
  isDiag_ = hm.isDiag_;
  rowNode_ = hm.rowNode_;  colNode_ = hm.colNode_;
  factorization_ = hm.factorization_;
  child_=nullptr; next_=nullptr;
  //copy all childs
  HMatrixNode<T, I>* hmc=hm.child_, *hc;
  if(hmc!=nullptr) //create first child
  {
      child_ = new HMatrixNode<T, I>(*hmc);
      child_->parent_ = this;
      hmc = hmc->next_;
      hc = child_;
  }
  while(hmc!=nullptr) //create next childs
  {
      hc->next_= new HMatrixNode<T, I>(*hmc);
      hc->next_->parent_=this;
      hmc=hmc->next_;
      hc=hc->next_;
  }
  if(hm.mat_ != nullptr)    mat_ = new LargeMatrix<T>(*hm.mat_);
  if(hm.appmat_ != nullptr) appmat_ = hm.appmat_->clone();
}

//! return the number of childs
template <typename T, typename I>
number_t HMatrixNode<T, I>::numberOfChilds() const
{
  HMatrixNode<T, I>* hn = child_;
  number_t nc = 0;
  while(hn != nullptr)
    {
      nc++;
      hn = hn->next_;
    }
  return nc;
}

// update row/col cluster node pointers (recursive), tree structures are the same but the pointers are not
template <typename T, typename I>
void HMatrixNode<T, I>::setClusterRow(ClusterNode<I>* cnp)
{
  //travel with synchronization
  HMatrixNode<T, I>* cur_child = child_;
  while(cur_child != nullptr) //update all children
    {
      ClusterNode<I>* cn_child = cur_child->rowNode_;    // rowNode of child matrix
      ClusterNode<I>* rn_child = rowNode_->child_;       // child of current row node
      ClusterNode<I>* cnp_child = cnp->child_;           // child of new row node
      while(rn_child != cn_child)    //try with brothers, should find one brother
        {
          rn_child = rn_child->next_;
          cnp_child = cnp_child->next_;
        }
      cur_child->setClusterRow(cnp_child);
      cur_child = cur_child->next_;
    }
  rowNode_ = cnp;
}

template <typename T, typename I>
void HMatrixNode<T, I>::setClusterCol(ClusterNode<I>* cnp)
{
  //travel with synchronization
  HMatrixNode<T, I>* cur_child = child_;
  while(cur_child != nullptr) //update all children
    {
      ClusterNode<I>* cn_child = cur_child->colNode_;   // colNode of child matrix
      ClusterNode<I>* rn_child = colNode_->child_;       // child of current col node
      ClusterNode<I>* cnp_child = cnp->child_;           // child of new col node
      while(rn_child != cn_child)    //try with brothers, should find one brother
        {
          rn_child = rn_child->next_;
          cnp_child = cnp_child->next_;
        }
      cur_child->setClusterCol(cnp_child);
      cur_child = cur_child->next_;
    }
  colNode_ = cnp;
}

//split a HMatrix node, assuming rowNode_ and colNode_ not null
//   nbAdm: number of admissible blocks, updated by divide
//   rmin, cmin: minimal size of a block
//   maxdepth: maximal depth of tree, if 0 not used else limit the division up to maxdepth
//   sym: if true, build only the lower part
template <typename T, typename I>
void HMatrixNode<T, I>::divide(number_t rmin, number_t cmin, number_t maxdepth, HMAdmissibilityRule admRule, bool sym)
{
  if(maxdepth > 0 && depth_ >= maxdepth) return; //stop division

  //test admissibility
  admissible_ = false;
  if(rowNode_->size() < rmin && colNode_->size() < cmin)  return; //block too small, stop division
  if(blockAdmissible(rowNode_, colNode_, admRule))
    {
      admissible_ = true;  //admissible block, stop division
      return;
    }

  //divide node
  ClusterNode<I>* rowChild = rowNode_->child_, *colChild;
  HMatrixNode<T, I>* hmChild;
  number_t rs = 1, cs = 1;
  bool symdiag = sym && isDiag_;

  while(rowChild != nullptr)
    {
      colChild = colNode_->child_;
      cs = 1;
      if(colChild == nullptr) // no col child, create rowChild x colNode HMatrixNodes
        {
          HMatrixNode<T, I>* newHM = new HMatrixNode<T, I>(this, 0, 0, depth_ + 1, rowChild, colNode_, rs, cs, 0, 0, eta_);
          if(child_ == nullptr)
            {
              child_ = newHM;
              hmChild = child_;
            }
          else
            {
              hmChild->next_ = newHM;
              hmChild = hmChild->next_;
            }
          newHM->isDiag_ = false;
          newHM->divide(rmin, cmin, maxdepth, admRule, sym);
          cs++;
        }
      else
        while(colChild != nullptr) // create  rowChild x colChild HMatrixNodes
          {
            if(!symdiag || (symdiag && rs <= cs)) // split a non diagonal node or a lower block of a diagonal node
              {
                HMatrixNode<T, I>* newHM = new HMatrixNode<T, I>(this, 0, 0, depth_ + 1, rowChild, colChild, rs, cs, 0, 0, eta_);
                if(child_ == nullptr)
                  {
                    child_ = newHM;
                    hmChild = child_;
                  }
                else
                  {
                    hmChild->next_ = newHM;
                    hmChild = newHM;
                  }
                newHM->isDiag_ = (isDiag_ && rs == cs);
                newHM->divide(rmin, cmin, maxdepth, admRule, sym);
              }
            colChild = colChild->next_;  //move to col brother
            cs++;
          }
      rowChild = rowChild->next_;   //move to row brother
      rs++;
    }

  if(rowNode_->child_ == nullptr) // no row child, create rowNode x colChild HMatrixNodes
    {
      colChild = colNode_->child_;
      while(colChild != nullptr) // create  rowNode x colChild HMatrixNode
        {
          HMatrixNode<T, I>* newHM = new HMatrixNode<T, I>(this, 0, 0, depth_ + 1, rowNode_, colChild, rs, cs, 0, 0, eta_);
          if(child_ == nullptr)
            {
              child_ = newHM;
              hmChild = child_;
            }
          else
            {
              hmChild->next_ = newHM;
              hmChild = hmChild->next_;
            }
          newHM->isDiag_ = false;
          newHM->divide(rmin, cmin, maxdepth, admRule, sym);
          colChild = colChild->next_;  //move to col brother
          cs++;
        }
    }
  //Note: if no row child and no col child, return non admissible node
}

//get leaves and add them to leaves list (recursive)
template <typename T, typename I>
void HMatrixNode<T, I>::getLeaves(std::list<HMatrixNode<T, I>* >& leaves, bool removeVoidLeaf) const
{
  if(child_ == nullptr)
    {
      if(!removeVoidLeaf || mat_ != nullptr || appmat_ != nullptr) leaves.push_back(const_cast< HMatrixNode<T, I>*>(this));
    }
  else child_->getLeaves(leaves, removeVoidLeaf);
  if(next_ != nullptr) next_->getLeaves(leaves, removeVoidLeaf);
}

// access to child at depth d
// if d=0 or no child return current, d=1 return child of current if it exists
template <typename T, typename I>
HMatrixNode<T, I>* HMatrixNode<T, I>::child(number_t d)
{
  if (d == 0 || child_ == nullptr) return this;
  if (d == 1) return child_;
  return child_->child(d - 1);
}

//create child pointers as a matrix of node pointers
template <typename T, typename I>
std::vector<HMatrixNode<T, I>* > HMatrixNode<T, I>::childsMatrix(number_t& m, number_t& n) const
{
  std::vector<HMatrixNode<T, I>* > hncs;
  HMatrixNode<T, I>* hn = child_;
  if(hn == nullptr) return hncs; //empty matrix !
  m = 1; n = 1;
  while(hn != nullptr)
    {
      m = std::max(m, hn->rowSub_);
      n = std::max(n, hn->colSub_);
      hn = hn->next_;
    }
  hncs.resize(m * n, 0);
  hn = child_;
  while(hn != nullptr)
    {
      hncs[(hn->rowSub_ - 1)*n + hn->colSub_ - 1] = hn;
      hn = hn->next_;
    }
  return hncs;
}

//number of T coefficients used to represent the matrix node
template <typename T, typename I>
number_t HMatrixNode<T, I>::nbNonZero() const
{
  number_t s = 0;
  HMatrixNode<T, I>* hn = child_;
  while(hn != nullptr) //travel children
    {
      s += hn->nbNonZero();
      hn = hn->next_;
    }
  if(mat_ != nullptr) s += mat_->nbNonZero();
  if(appmat_ != nullptr) s += appmat_->nbNonZero();
  return s;
}

//Frobenius norm
template <typename T, typename I>
real_t HMatrixNode<T, I>::norm2() const
{
  real_t n = 0.;
  if(mat_ != nullptr)     n = mat_->norm2();
  if(appmat_ != nullptr)  n = appmat_->norm2();
  return n;
}

//infinite norm
template <typename T, typename I>
real_t HMatrixNode<T, I>::norminfty() const
{
  real_t n = 0.;
  if(mat_ != nullptr)     n = mat_->norminfty();
  if(appmat_ != nullptr)  n = appmat_->norminfty();
  return n;
}

// -----------------------------------------------------------------------------------------------------------------
//                                          conversion stuff
// -----------------------------------------------------------------------------------------------------------------
/*! move to hierarchical representation  down to the given depth d
      go down to depth d, splitting any HMatrixNode that are either a Largematrix or an ApproximateMatrix
      if current node is a hierarchic matrix nothing is done
      if current node is a LargeMatrix, then split in LargeMatrix
      if current node is an ApproximateMatrix, then split in ApproximateMatrix
*/
template <typename T, typename I>
void HMatrixNode<T, I>::toHierarchical(number_t d)
{
  if(d == 0) return;
  HMatrixNode<T, I>* hn;
  if(mat_ != nullptr || appmat_ != nullptr) //split mat or appmat
    {
      divide(0, 0, depth_ + 1, _noRule, _noSymmetry); //create childs
      hn = child_;
      while(hn != nullptr)
        {
          if(mat_ != nullptr) hn->mat_ = mat_->extract(hn->rowNode_->getParentNumbers(), hn->colNode_->getParentNumbers());
          else
             {
              hn->appmat_ = appmat_->extract(hn->rowNode_->getParentNumbers(), hn->colNode_->getParentNumbers());
              LowRankMatrix<T>* lrmc = static_cast<LowRankMatrix<T>*>(hn->appmat_), *lrm = static_cast<LowRankMatrix<T>*>(appmat_);
              lrmc->eps_=lrm->eps_; lrmc->rank_=lrm->rank_;lrmc->cm_=lrm->cm_;
              lrmc->compress(lrm->cm_, lrm->rank_, lrm->eps_);
             }
          hn = hn->next_;
        }
      if(mat_ != nullptr)
        {
          delete mat_;
          mat_ = nullptr;
        }
      else
        {
          delete appmat_;
          appmat_ = nullptr;
        }
    }
  if(d == 1) return;
  //go down
  hn = child_;
  while(hn != nullptr)
    {
      hn->toHierarchical(d - 1);
      hn = hn->next_;
    }
}

/*! move to LargeMatrix representation */
template <typename T, typename I>
void HMatrixNode<T, I>::toLargeMatrix(StorageType st, AccessType at)
{
  if(mat_ != nullptr) return;
  if(appmat_ != nullptr)
    {
      mat_ = new LargeMatrix<T>(appmat_->toLargeMatrix(st, at));
      delete appmat_;
      appmat_ = nullptr;
      return;
    }

  //hierarchical node
  mat_ = new LargeMatrix<T>(rowNode_->size(), colNode_->size(), st, at);
  HMatrixNode<T, I>* hn = child_, *hnp;
  while(hn != nullptr)  //loop on childs
    {
      hn->toLargeMatrix(st, at);
      mat_->add(*hn->mat_, hn->rowNode_->getParentNumbers(), hn->colNode_->getParentNumbers(), 1.);
      hnp = hn;
      hn = hn->next_;
      delete hnp;    //delete child
    }
  child_ = nullptr;
}

/*! move to ApproximateMatrix */
template <typename T, typename I>
void HMatrixNode<T, I>::toApproximateMatrix(MatrixApproximationType type, number_t ra, real_t eps)
{
  if (appmat_ != nullptr) return;
  if (type != _lowRankApproximation)
  {
    where("HMatrixNode::toApproximateMatrix(...)");
    error("low_rank_only");
  }
  if (mat_ != nullptr)
  {
    LowRankMatrix<T>* lrm = new LowRankMatrix<T>();
    lrm->cm_ = _rsvdCompression;
    lrm->eps_ = eps;
    lrm->rank_ = ra;
    if(ra == 0 && eps == 0.) lrm->cm_ = _svdCompression; //force svd in that case
    lrm->fromLargeMatrix(*mat_);
    appmat_ = lrm;
    delete mat_;
    mat_ = nullptr;
    return;
  }
  //hierarchical node
  number_t mc, nc;
  std::vector<HMatrixNode<T, I>* > childs = childsMatrix(mc, nc);
  typename std::vector<HMatrixNode<T, I>* >::iterator itc = childs.begin();
  number_t r = 0;
  for(; itc != childs.end(); ++itc)  //find total "rank"
    {
      (*itc)->toApproximateMatrix(_lowRankApproximation, ra, eps);
      r += (*itc)->appmat_->rank();
    }
  number_t m = numberOfRows(), n = numberOfCols();
  LowRankMatrix<T>* lrm = new LowRankMatrix<T>(m, n, r);
  lrm->D_ += 1.; //set diagonal to 1
  bool hasdiag = false;
  number_t js = 0;
  typename std::vector<T>::iterator it1, it2;
  for(itc = childs.begin(); itc != childs.end(); ++itc)  //loop on childs
    {
      LowRankMatrix<T>* lrmc = static_cast<LowRankMatrix<T>* >((*itc)->appmat_);
      number_t rc = lrmc->rank();
      std::vector<number_t> index = (*itc)->rowNode_->getParentNumbers();
      std::vector<number_t>::iterator itn = index.begin();
      for(number_t k = 0; k < index.size(); k++, ++itn)
        {
          it1 = lrm->U_.begin() + (*itn - 1) * r + js;
          it2 = lrmc->U_.begin() + k * rc;
          for(number_t j = 0; j < rc; j++, ++it1, ++it2) *it1 = *it2;
        }
      index = (*itc)->colNode_->getParentNumbers();
      itn = index.begin();
      for(number_t k = 0; k < index.size(); k++, ++itn)
        {
          it1 = lrm->V_.begin() + (*itn - 1) * r + js;
          it2 = lrmc->V_.begin() + k * rc;
          for(number_t j = 0; j < rc; j++, ++it1, ++it2) *it1 = *it2;
        }
      if(lrmc->D_.size() == rc)
        {
          hasdiag = true;
          it1 = lrm->D_.begin() + js;
          it2 = lrmc->D_.begin();
          for(number_t j = 0; j < rc; j++, ++it1, ++it2) *it1 = *it2;
        }
      js += rc;
      delete *itc;  //delete child
    }
  if(!hasdiag) lrm->D_.clear();
  if(ra > 0 || eps > 0) lrm->rsvd(ra, eps); //re-compression using rsvd
  appmat_ = lrm;
  child_ = nullptr;
}

// -----------------------------------------------------------------------------------------------------------------
//                                          multMatrixVector
// -----------------------------------------------------------------------------------------------------------------
//multiply HMatrixNode(leaf) by a given vector x, the result is accumulated in hx
//   x: global vector to be multiplied
//   hx: result of the multiplication in global numbering, sized when root node
//   if symt is either _symmetric, _selfAdjoint, _skewSymmetric or _skewAdjoint and not a diagonal node
//     compute +- transpose/adjoint(HMatrixNode) * x in addition to the computation of HMatrixNode * x

template <typename T, typename I>
std::vector<T>& HMatrixNode<T, I>::multMatrixVectorNode(const std::vector<T>& x, std::vector<T>& hx, SymType symt) const
{
  if (mat_ == nullptr && appmat_ == nullptr) return hx; //not of a leaf, nothing done
  if (x.size() == 0)
  {
    where("HMatrixNode::multMatrixVectorNode(...)");
    error("is_void", "x");
  }
  const std::vector<number_t>& rowNum = rowNode_->dofNumbers();
  const std::vector<number_t>& colNum = colNode_->dofNumbers();
  if(parent_ == nullptr) hx.resize(rowNum.size(), 0.*x[0]); // if root node, hx is sized and initialized as a zero vector

  //compute HMatrixNode * x
  if(HMatrixNode<T, I>::counterOn) HMatrixNode<T, I>::counter++;
  std::vector<T> x2(colNum.size()), y(rowNum.size());  //temporary vectors
  std::vector<number_t>::const_iterator itc = colNum.begin();
  typename std::vector<T>::iterator itx = x2.begin();
  for(; itx != x2.end(); ++itx, ++itc) *itx = x[*itc];
  if(mat_ != nullptr) mat_->storagep()->multMatrixVector(mat_->values(), x2, y, mat_->sym);
  if(appmat_ != nullptr) appmat_->multMatrixVector(x2, y);
  std::vector<number_t>::const_iterator itr = rowNum.begin();
  typename std::vector<T>::iterator ity = y.begin();
  for(; ity != y.end(); ++ity, ++itr) hx[*itr] += *ity;
  if(symt == _noSymmetry || isDiag_) return hx; //no symmetry or diagonal node

  //compute  +- transpose/adjoint(HMatrixNode) * x, use multVectorMatrix because  +-adj(A)*x = +-conj(tran(conj(x)*A))
  if(HMatrixNode<T, I>::counterOn) HMatrixNode<T, I>::counter++;
  x2.resize(rowNum.size());
  y.resize(colNum.size());  //temporary vectors
  itc = rowNum.begin();
  itx = x2.begin();
  if(symt == _symmetric || symt == _skewSymmetric)
    for(; itx != x2.end(); ++itx, ++itc) *itx = x[*itc];
  else
    for(; itx != x2.end(); ++itx, ++itc) *itx = conj(x[*itc]);
  if(mat_ != nullptr) mat_->storagep()->multVectorMatrix(mat_->values(), x2, y, mat_->sym);
  if(appmat_ != nullptr) appmat_->multVectorMatrix(x2, y);
  ity = y.begin();
  itr = colNum.begin();
  switch(symt)
    {
    case _selfAdjoint:
      for(; ity != y.end(); ++ity, ++itr) hx[*itr] += conj(*ity);
      break;
    case _skewAdjoint:
      for(; ity != y.end(); ++ity, ++itr) hx[*itr] -= conj(*ity);
      break;
    case _skewSymmetric:
      for(; ity != y.end(); ++ity, ++itr) hx[*itr] -= (*ity);
      break;
    default:
      for(; ity != y.end(); ++ity, ++itr) hx[*itr] += (*ity);
    }
  return hx;
}

//multiply HMatrixNode by a Vector using recursive algorithm, the result is stored in hx
template <typename T, typename I>
std::vector<T>& HMatrixNode<T, I>::multMatrixVector(const std::vector<T>& x, std::vector<T>& hx, SymType symt) const
{
  if(child_ == nullptr) multMatrixVectorNode(x, hx, symt);
  else child_->multMatrixVector(x, hx, symt);
  if(next_ != nullptr) next_->multMatrixVector(x, hx, symt);
  return hx;
}

//multiply HMatrixNode by a Vector using non recursive algorithm, the result is stored in hx
//template <typename T, typename I>
//std::vector<T>& HMatrixNode<T,I>::multMatrixVector2(const std::vector<T>& x, std::vector<T>& hx, SymType symt) const
//{
//  if(x.size()==0) error("free_error","in HMatrixNode * Vector, given vector is void !");
//  std::vector<number_t>& rowNum=rowNode_->numbers_;
//  std::vector<number_t>& colNum=colNode_->numbers_;
//  if(parent_==nullptr) hx.resize(rowNum.size(),0.*x[0]); // if root node, hx is sized and initialized as a zero vector
//  std::vector<HMatrixNode<T,I>*> leaves;   //list of all leaves of the tree
//  //travel tree
//  HMatrixNode<T,I>* hp =this;
//  while(hp!=nullptr)
//    {
//      while(hp->child_!=nullptr) hp=hp->child_;
//      leaves.push_back(hp);
//      while(hp->next_==nullptr && hp->parent_!=nullptr) hp=hp->parent_;
//      if(hp->next_!=nullptr) hp=hp->next_;
//      if(hp==this) hp=nullptr;
//    }
//  //travel leaves
//  typename std::vector<HMatrixNode<T,I>*>::iterator itl=leaves.begin();
//  for(; itl!=leaves.end(); ++itl)(*itl)->multMatrixVectorNode(x,hx);
//  return hx;
//}

// -----------------------------------------------------------------------------------------------------------------
//                                          multMatrixAdd: +=c*A*B
// -----------------------------------------------------------------------------------------------------------------
/*! perform += c*A*B with a complex algorithm regarding the structure of current HMatrixNode and HMatrixNode A, B
    current, A and B nodes must be consistent, not checked here!
    indeed HMatrixNode's may be a hierarchical node, an ApproximateMatrix (LowRankMatrix) or a dense LargeMatrix

    - current node is a hierarchical matrix
        . A or B is not a hierarchical matrix, create hierarchical representation of A or B (A.toHierarchical(...))
        . A,B nodes are hierarchical matrix -> do a block product going down
    - current node is a LargeMatrix (dense)
        . if A and B are not hierarchical do the operation L+=c*LA*LB or L+=c*LA*ApB or L+=c*ApA*LB or L+=c*ApA*ApB
        . if A or B is a hierarchical matrix, move A and B to LargeMatrix and do L+=c*LA*LB
    - current node is an ApproximateMatrix (LowRankMatrix)
        . if A and B are LargeMatrix, move current to LargeMatrix and do operation, then return to ApproximateMatrix
        . if A or B is a ApproximateMatrix, compute A*B as an approximate matrix then combine +=c*Ab using ApproximateMatrix summation
        . if A or B is a Hierarchical matrix, move all matrices to hierarchical representation, then return to ApproximateMatrix
*/
template <typename T, typename I>
void HMatrixNode<T, I>::multMatrixAdd(HMatrixNode<T, I>& A, HMatrixNode<T, I>& B, T c)
{
  HMatrixNode<T, I>* childA = A.child_, *childB = B.child_;
  HMatrixNode<T, I>* Ap = nullptr, *Bp = nullptr;
  bool approximate = appmat_ != nullptr, largematrix = mat_ != nullptr;
  bool allocA, allocB;
  if (largematrix || approximate)   //current node is a LargeMatrix or an ApproximateMatrix
  {
    if (childA == nullptr && childB == nullptr) // A and B are not hierarchical matrix, move to LargeMatrix
    {
      if(mat_ == nullptr) mat_ = new LargeMatrix<T>(appmat_->toLargeMatrix());
      LargeMatrix<T>* Al = A.mat_;
      allocA = (Al == nullptr);
      if(allocA) Al = new  LargeMatrix<T>(A.appmat_->toLargeMatrix());
      LargeMatrix<T>* Bl = B.mat_;
      allocB = (Bl == nullptr);
      if(allocB) Bl = new  LargeMatrix<T>(B.appmat_->toLargeMatrix());
      mat_->multMatrixAdd(*Al, *Bl, c);
      if (allocA) delete Al;
      if (allocB) delete Bl;
      if (appmat_ != nullptr)
      {
        appmat_->fromLargeMatrix(*mat_);
        delete mat_;
        mat_ = nullptr;
      }
      return;
    }
  }
  //move all to Hierarchical and do hierarchical product
  allocA = (childA == nullptr);
  if (allocA)
  {
    Ap = new HMatrixNode<T, I>(A); //to preserve A, copy of A
    Ap->toHierarchical();
  }
  else Ap = &A;

  allocB = (childB == nullptr);
  if (allocB)
  {
    Bp = new HMatrixNode<T, I>(A); //to preserve B, copy of B
    Bp->toHierarchical();
  }
  else Bp = &B;

  number_t m, n;
  std::vector<HMatrixNode<T, I>* > childs = childsMatrix(m, n);
  number_t mA, nA;
  std::vector<HMatrixNode<T, I>* > childsA = Ap->childsMatrix(mA, nA);
  number_t mB, nB;
  std::vector<HMatrixNode<T, I>* > childsB = Bp->childsMatrix(mB, nB);
  if (nA != mB)
  {
    where("HMatrixNode::multMatrixAdd(A,B)");
    error("bad_dim", nA, mB);
  }
  if (mA != m)
  {
    where("HMatrixNode::multMatrixAdd(A,B)");
    error("bad_dim", mA, m);
  }
  if (nB != n)
  {
    where("HMatrixNode::multMatrixAdd(A,B)");
    error("bad_dim", nB, n);
  }
  for (number_t i = 0; i < m; i++)
    for (number_t j = 0; j < n; j++)
      for (number_t k = 0; k < nA; k++)
        childs[i * n + j]->multMatrixAdd(*childsA[i * nA + k], *childsB[k * nB + j], c);

  if (allocA) delete Ap;
  if (allocB) delete Bp;

  //return to the original representation
  if (approximate)
  {
    LowRankMatrix<T>* lrm = static_cast<LowRankMatrix<T>* >(appmat_);   //for the moment, assume appmat_ is a pointer to a LowRankMatrix
    toApproximateMatrix(_lowRankApproximation, lrm->rank_, lrm->eps_);
  }
  if (largematrix) toLargeMatrix();
}

// -----------------------------------------------------------------------------------------------------------------
//                                             LU factorization
// -----------------------------------------------------------------------------------------------------------------
/*! perform LU factorization of the current Hmatrix node as follows
     if the node is a LargeMatrix leaf perform LU standard LU factorization
     if the node is an ApproximateMatrix leaf perform LU factorization on the decompressed matrix, leading to a LU LargeMatrix (should not be handled!)
                                               | A11 A12 ... A1n|
     if the node is the block HMatrix, say  H= | A21 A22 ... A2n|, row access HMatrix
                                               | ... ... ... ...|
                                               | An1 An2 ... Ann|
        perform the block LU factorization
        for k=1,n
          LU factorization of Akk
          for i = k+1,n
              upper solve Lik*Ukk=Aik
          for j = k+1,n
              lower solve  Lkk*Ukj = Akj
          for i = k+1,n
            for j = k+1,n
                Aij-=Lik*Ukj

  when bisection clustering is used, n=2, so the algorithm does
       k=1 : A11=L11*U11,  L21*U11=A21 -> L21, L11*U12=A12 -> U12, A22-=L21*U12
       k=2 : A22 = L22*U22
  \note at the end the node contains the virtual LU factorization
*/
template <typename T, typename I>
void HMatrixNode<T, I>::lu()
{
  factorization_ = _lu;
  if(appmat_ != nullptr)  // should not be an approximate matrix, move to LargeMatrix
    {
      mat_ = new LargeMatrix<T>(appmat_->toLargeMatrix(_dense, _row));
      delete appmat_;
      appmat_ = nullptr;
    }
  if(mat_ != nullptr)
    {
      mat_->luFactorize(false);     //lu factorization with no permutation
      return;
    }

  if (child_ == nullptr)
  {
    where("HMatrixNode::lu()");
    error("null_pointer", "child_");
  }

  number_t m, n, mn;
  std::vector<HMatrixNode<T, I>* > hncs = childsMatrix(m, n);
  mn = std::min(m, n);
  for(number_t k = 0; k < mn; ++k) //main loop of LU factorization
    {
      HMatrixNode<T, I>* Akk = hncs[k * n + k];
      Akk->lu();  //factorize Akk = Lkk*Ukk
      for(number_t i = k + 1; i < m; ++i) Akk->upperLeftSolve(*hncs[i * n + k]);   //solve X*Ukk = Aik -> Lik, i>k
      for(number_t j = k + 1; j < n; ++j) Akk->lowerSolve(*Akk, *hncs[k * n + j]); //solve Lkk*X = Akj -> Ukj, j>k
      for(number_t i = k + 1; i < m; ++i) //update Aij  i>k, j>k
        for(number_t j = k + 1; j < n; ++j) hncs[i * n + j]->multMatrixAdd(*hncs[i * n + k], *hncs[k * n + j], T(-1)); //Aij-=Lik*Ukj
    }
}

/*! solve L*X = B where L is the lower triangular part of the current node, assuming it is factorized as LU
    in  B: right hand side node
    out B: solution node
*/
template <typename T, typename I>
void HMatrixNode<T, I>::lowerSolve(HMatrixNode<T, I>& B) const
{
  if (child_ != nullptr) //current node is a hierarchical matrix
  {
    bool isMat=(B.mat_ != nullptr), isAppMat=(B.appmat_ != nullptr);
    if (isMat || isAppMat) B.toHierarchical(); //B is a LargeMatrix or an ApproximateMatrix, move to hierarchical
    number_t m, n;
    std::vector<HMatrixNode<T, I>* > childs = childsMatrix(m, n);
    if (B.child_ != nullptr) //B is a hierarchical matrix
    {
      number_t mB, nB;
      std::vector<HMatrixNode<T, I>* > childsB = B.childsMatrix(mB, nB);
      if (n != mB)
      {
        where("HMatrixNode::lowerSolve(...)");
        error("bad_dim", n, mB);
      }
      for (number_t i = 0; i < m; i++)
        for (number_t j = 0; j < nB; j++)
        {
          childs[i * n + i]->lowerSolve(*childsB[i * nB + j]);
          for (number_t k = i + 1; k < mB; k++) childsB[k * nB + j]->multMatrixAdd(*childs[k * n + i], *childsB[i * nB + j], T(-1));
        }
    }
    if (isMat) B.toLargeMatrix();
    if (isAppMat) B.toApproximateMatrix(_lowRankApproximation, 0, theTolerance);
    return;
  }
  if (mat_ != nullptr) //current node is a LargeMatrix
  {
    if(B.child_ != nullptr || B.appmat_!=nullptr)  B.toLargeMatrix(); //B is a hierarchical matrix or an ApproximateMatrix, move to LargeMatrix
    mat_->lowerD1Solve(*B.mat_);
    return;
  }
  if (appmat_ != nullptr) //current node is an ApproximateMatrix, it should not
  {
    where("HMatrixNode::lowerSolve(...)");
    error("approx_matrix_not_handled");
  }
  where("HMatrixNode::lowerSolve(...)");
  error("is_void", "HMatrixNode");
}

/*! solve U*X = B where U is the upper triangular part of the current node, assuming it is factorized as LU
    in  B: right hand side node
    out B: solution node
*/
template <typename T, typename I>
void HMatrixNode<T, I>::upperSolve(HMatrixNode<T, I>& B) const
{
  if (child_ != nullptr) //current node is a hierarchical matrix
  {
    bool isMat=(B.mat_ != nullptr), isAppMat=(B.appmat_ != nullptr);
    if (isMat || isAppMat) B.toHierarchical(); //B is a LargeMatrix or an ApproximateMatrix, move to hierarchical
    number_t m, n;
    std::vector<HMatrixNode<T, I>* > childs = childsMatrix(m, n);
    if (B.child_ != nullptr) //B is a hierarchical matrix
    {
      number_t mB, nB;
      std::vector<HMatrixNode<T, I>* > childsB = B.childsMatrix(mB, nB);
      if (n != mB)
      {
        where("HMatrixNode::upperSolve(...)");
        error("bad_dim", n, mB);
      }
      for (int_t i = n-1; i!=-1; i--)
        for (number_t j = 0; j < mB; j++)
        {
          childs[i* n + i]->lowerLeftSolve(*childsB[i * nB + j]);
          for (number_t k =0; k < i; k++) childsB[k * nB + j]->multMatrixAdd(*childs[k * n + i], *childsB[i * nB + j], T(-1));
        }
    }
    if (isMat) B.toLargeMatrix();
    if (isAppMat) B.toApproximateMatrix(_lowRankApproximation, 0, theTolerance);
    return;
  }
  if (mat_ != nullptr) //current node is a LargeMatrix
  {
    if (B.child_ != nullptr || B.appmat_!=nullptr)  B.toLargeMatrix(); //B is a hierarchical matrix or an ApproximateMatrix, move to LargeMatrix
    mat_->upperSolve(*B.mat_);
    return;
  }
  if (appmat_ != nullptr) //current node is an ApproximateMatrix, it should not
  {
    where("HMatrixNode::upperSolve");
    error("approx_matrix_not_handled");
  }
  where("HMatrixNode::upperSolve");
  error("is_void", "HMatrixNode");
}

/*! solve X*L = B where L is the lower triangular part of the current node, assuming it is factorized as LU
    in  B: right hand side node
    out B: solution node
    same as Lt*X = Bt (upper algorithm)
*/
template <typename T, typename I>
void HMatrixNode<T, I>::lowerLeftSolve(HMatrixNode<T, I>& B) const
{
  if (child_ != nullptr) //current node is a hierarchical matrix
  {
    bool isMat=(B.mat_ != nullptr), isAppMat=(B.appmat_ != nullptr);
    if (isMat || isAppMat) B.toHierarchical(); //B is a LargeMatrix or an ApproximateMatrix, move to hierarchical
    number_t m, n;
    std::vector<HMatrixNode<T, I>* > childs = childsMatrix(m, n);
    if (B.child_ != nullptr) //B is a hierarchical matrix
    {
      number_t mB, nB;
      std::vector<HMatrixNode<T, I>* > childsB = B.childsMatrix(mB, nB);
      if (m != nB)
      {
        where("HMatrixNode::lowerLeftSolve(...)");
        error("bad_dim", m, nB);
      }
      for (int_t i = m-1; i!=-1; i--)
        for (number_t j = 0; j < mB; j++)
        {
          childs[i* n + i]->lowerSolve(*childsB[j * mB + i]);
          for (number_t k =0; k < i; k++) childsB[k * nB + j]->multMatrixAdd(*childs[k * n + i], *childsB[j * mB + i], T(-1));
        }
    }
    if (isMat) B.toLargeMatrix();
    if (isAppMat) B.toApproximateMatrix(_lowRankApproximation, 0, theTolerance);
    return;
  }
  if (mat_ != nullptr) //current node is a LargeMatrix
  {
    if (B.child_ != nullptr || B.appmat_!=nullptr)  B.toLargeMatrix(); //B is a hierarchical matrix or an ApproximateMatrix, move to LargeMatrix
    mat_->lowerLeftSolve(*B.mat_);
    return;
  }
  if (appmat_ != nullptr) //current node is an ApproximateMatrix, it should not
  {
    where("HMatrixNode::lowerLeftSolve(...)");
    error("approx_matrix_not_handled");
  }
  where("HMatrixNode::lowerLeftSolve(...)");
  error("is_void", "HMatrixNode");
}

/*! solve X*U = B where U is the upper triangular part of the current node, assuming it factorized as LU
    in  B: right hand side node
    out B: solution node
*/
template <typename T, typename I>
void HMatrixNode<T, I>::upperLeftSolve(HMatrixNode<T, I>& B) const
{
  if (child_ != nullptr) //current node is a hierarchical matrix
  {
    bool isMat=(B.mat_ != nullptr), isAppMat=(B.appmat_ != nullptr);
    if (isMat || isAppMat) B.toHierarchical(); //B is a LargeMatrix or an ApproximateMatrix, move to hierarchical
    number_t m, n;
    std::vector<HMatrixNode<T, I>* > childs = childsMatrix(m, n);
    if (B.child_ != nullptr) //B is a hierarchical matrix
    {
      number_t mB, nB;
      std::vector<HMatrixNode<T, I>* > childsB = B.childsMatrix(mB, nB);
      if (m != nB)
      {
        where("HMatrixNode::upperLeftSolve(...)");
        error("bad_dim", m, nB);
      }
      for (number_t i = 0; i < m; i++)
        for (number_t j = 0; j < nB; j++)
        {
          childs[i * n + i]->upperLeftSolve(*childsB[j * nB + i]);
          for (number_t k = i + 1; k < mB; k++) childsB[j * nB + k]->multMatrixAdd(*childs[i * n + k], *childsB[j * nB + i], T(-1));
        }
    }
    if (isMat) B.toLargeMatrix();
    if (isAppMat) B.toApproximateMatrix(_lowRankApproximation, 0, theTolerance);
    return;
  }
  if (mat_ != nullptr) //current node is a LargeMatrix
  {
    if (B.child_ != nullptr || B.appmat_!=nullptr)  B.toLargeMatrix(); //B is a hierarchical matrix or an ApproximateMatrix, move to LargeMatrix
    mat_->lowerD1Solve(*B.mat_);
    return;
  }
  if (appmat_ != nullptr) //current node is an ApproximateMatrix, it should not
  {
    where("HMatrixNode::upperLeftSolve(...)");
    error("approx_matrix_not_handled");
  }
  where("HMatrixNode::upperLeftSolve(...)");
  error("free_error", "HMatrixNode::upperLeftSolve(B), node is empty");
}

// -----------------------------------------------------------------------------------------------------------------
//                                              print stuff
// -----------------------------------------------------------------------------------------------------------------
template <typename T, typename I>
void HMatrixNode<T, I>::printNode(std::ostream& out, bool shift) const
{
  string_t sh = "";
  number_t sn = 0;
  if(shift) sh = string_t(2 * depth_, ' ');
  eol = "\n" + sh + string_t(sn, ' ');
  out << sh << "depth=" << depth_ << " node " << this;
  if(admissible_) out << " admissible";
  else out << " non admissible";
  out << " block " << rowSub_ << "x" << colSub_ << " size " << numberOfRows() << "x" << numberOfCols()
      << ", row numbers = " << rowNode_->dofNumbers() << " col numbers = " << colNode_->dofNumbers() << eol;
  if(mat_ != nullptr)    mat_->print(out);
  if(appmat_ != nullptr) appmat_->print(out);
  if(child_ != nullptr)
    {
      out << "has " << numberOfChilds() << " childs: ";
      number_t m, n;
      std::vector<HMatrixNode<T, I>* > childs = childsMatrix(m, n);
      for(number_t i = 0; i < m; i++)
        for(number_t j = 0; j < n; j++)
          {
            number_t p = i * n + j;
            HMatrixNode<T, I>* hno = childs[p];
            if(hno != nullptr)
              {
                if(p > 0) out << ", ";
                out << "(" << i + 1 << "," << j + 1 << ") -> ";
                if(hno->appmat_ != nullptr) out << " app. matrix " << hno->appmat_->numberOfRows() << "x" << hno->appmat_->numberOfCols();
                if(hno->mat_ != nullptr) out << " large matrix " << hno->mat_->numberOfRows() << "x" << hno->mat_->numberOfCols();
                if(hno->child_ != nullptr) out << " node " << hno->numberOfRows() << "x" << hno->numberOfCols();
              }
          }
      out << std::endl;
    }
  if(mat_ == nullptr && appmat_ == nullptr && child_ == nullptr) out << "void matrix" << std::endl;
  eol = "\n";
}

template <typename T, typename I>
void HMatrixNode<T, I>::print(std::ostream& out) const
{
  printNode(out);
//  if(child_!=nullptr)  child_->print(out);
//  if(next_!=nullptr)   next_->print(out);
  HMatrixNode<T, I>* hn = child_;
  while(hn != nullptr)
    {
      hn->print(out);
      hn = hn->next_;
    }
}

// print recursively matrix structure as a list of block matrix [I,J] x [K,L]
//     0/1/2   I   J   K   L
// where the first number is the admissibility status of the block: admissible leaf ->1, no admissible leaf -> 2, node -> 0
//   fr and fc are the first rank of row/col numbers
//   if all is false only leaf blocks are printed
//   if shift is true, add 2*depth blanks on each line
template <typename T, typename I>
void HMatrixNode<T, I>::printStructure(std::ostream& out, number_t fr, number_t fc, bool all, bool shift) const
{
  if(child_ == nullptr || all) //leaf block or print all
    {
      if(shift) out << string_t(2 * depth_, ' ');
      if(admissible_) out << "1 ";
      else if(child_ == nullptr) out << "2 ";
      else out << "0 "; // encoding block status: admissible leaf ->1, no admissible leaf -> 2, node -> 0
      out << fr << " " << fr + rowNode_->size() - 1 << " "; // first and last row index
      out << fc << " " << fc + colNode_->size() - 1 << eol; // first and last col index
    }
  // print all children
  HMatrixNode<T, I>* hmn = child_;
  while(hmn != nullptr)
    {
      number_t lr = fr, lc = fc;
      if(hmn->rowSub_ > 1)   //update fr
        {
          ClusterNode<I>* cn = rowNode_->child_;
          for(number_t k = 1; k < hmn->rowSub_ && cn != nullptr; k++, cn = cn->next_)  lr += cn->size();
        }
      if(hmn->colSub_ > 1)   //update fc
        {
          ClusterNode<I>* cn = colNode_->child_;
          for(number_t k = 1; k < hmn->colSub_ && cn != nullptr; k++, cn = cn->next_)  lc += cn->size();
        }
      hmn->printStructure(out, lr, lc, all, shift);
      hmn = hmn->next_;
    }

  return;
}

//===============================================================================================================
//                                           external functions
//===============================================================================================================

//shortcut to member function multMatrixVector
template <typename T, typename I>
Vector<T> multMatrixVector(const HMatrix<T, I>& h, const Vector<T>& x)  //! hmatrix vector product
{
  Vector<T> hx(h.numberOfRows(), T());
  h.multMatrixVectorOmp(x, hx);
  return hx;
}

template <typename T, typename I>
Vector<T> operator*(const HMatrix<T, I>& h, const Vector<T>& x)  //! Hmatrix * Vector
{
  Vector<T> hx(h.numberOfRows(), T());
  h.multMatrixVectorOmp(x, hx);
  return hx;
}

//===============================================================================================================
//                                    implementation of HMatrix member functions
//===============================================================================================================
//constructor
template <typename T, typename I>
HMatrix<T, I>::HMatrix()
  : root_(nullptr), rowCT_(nullptr), colCT_(nullptr), name(""), valueType_(_real), strucType_(_scalar), method_(_denseHM), admRule_(_boxesRule), eta_(1.),
    rowmin_(1), colmin_(1), sym(_noSymmetry), depth(0), nbNodes(0), nbLeaves(0), nbAdmissibles(0), factorization_(_noFactorization) {}

template <typename T, typename I>
HMatrix<T, I>::HMatrix(ClusterTree<I>& rowc, ClusterTree<I>& colc,  number_t rmin, number_t cmin, number_t maxdepth, const string_t& na,
                       SymType sy, HMatrixMethod meth, HMAdmissibilityRule admr, real_t e)
  : root_(nullptr), rowCT_(&rowc), colCT_(&colc), name(na), method_(meth), admRule_(admr), eta_(e), rowmin_(rmin), colmin_(cmin),
    sym(sy), depth(maxdepth), factorization_(_noFactorization)
{
  trace_p->push("HMatrix<T,I>:HMatrix(...)");
  structPair vst = typeOf(T());
  valueType_ = vst.first;
  strucType_ = vst.second;
  nbAdmissibles = 0;
  nbAppMatrices = 0;
  buildTree();
  trace_p->pop();
}

//copy constructor
template <typename T, typename I>
HMatrix<T, I>::HMatrix(const HMatrix<T, I>& hm)
{
  copy(hm);
}

//assign operator
template <typename T, typename I>
HMatrix<T, I>& HMatrix<T, I>::operator=(const HMatrix<T, I>& hm)
{
  clear();
  copy(hm);
  return *this;
}

// copy all, full copy except ClusterTree pointers
template <typename T, typename I>
void HMatrix<T, I>::copy(const HMatrix<T, I>& hm)
{
  name = hm.name + "_copy";
  valueType_ = hm.valueType_;
  strucType_ = hm.strucType_;
  method_ = hm.method_;
  eta_ = hm.eta_;
  rowmin_ = hm.rowmin_;
  colmin_ = hm.colmin_;
  sym = hm.sym;
  depth = hm.depth;
  nbNodes = hm.nbNodes;
  nbLeaves = hm.nbLeaves;
  nbAdmissibles = hm.nbAdmissibles;
  nbAppMatrices = hm.nbAppMatrices;
  rowCT_ = hm.rowCT_;
  colCT_ = hm.colCT_; //pointers copy !
  if(root_ != nullptr) root_ = new HMatrixNode<T, I>(*hm.root_); //full copy of nodes
  factorization_ = hm.factorization_;
}

// clear all and deallocate matrix pointers
template <typename T, typename I>
void HMatrix<T, I>::clear()
{
  if(root_ != nullptr) root_->clear();
  root_ = nullptr;
  rowCT_ = nullptr;
  colCT_ = nullptr; // cluster tree pointers are not deleted !
  depth = 0;
  nbLeaves = 0;
  nbNodes = 0;
  nbAdmissibles = 0;
  nbAppMatrices = 0;
}

//create HMatrix cluster tree from row/col cluster tree with the following criteria:
//       a matrix node made of R and C node is admissible if
//             diam(BoundingBox(R))  < 2*eta*dist(BoundingBox(R),BoundingBox(C))   standard: eta = 1
//       when a matrix node is admissible it becomes a leaf of type approximate matrix
//       when a matrix node is not admissible but its size is lower than minrow x mincol it becomes a leaf of type standard matrix

//recursive method
template <typename T, typename I>
void HMatrix<T, I>::buildTree()
{
  trace_p->push("HMatrix<T,I>::buildTree()");
  if (rowCT_ == nullptr) error("null_pointer", "rowCT_");
  if (colCT_ == nullptr) error("null_pointer", "colCT_");
  //create root node
  ClusterNode<I>* rowC = rowCT_->root(), *colC = colCT_->root();
  root_ = new HMatrixNode<T, I>(0, 0, 0, 0, rowC, colC, 1, 1, 0, 0, eta_);
  root_->isDiag_ = true;
  root_->divide(rowmin_, colmin_, depth, admRule_, sym);
  updateInfo();
  trace_p->pop();
}

// update tree info (depth, number of nodes, ...)
template <typename T, typename I>
void HMatrix<T, I>::updateInfo()
{
  depth = 0;
  nbNodes = 0;
  nbLeaves = 0;
  nbAdmissibles = 0;
  nbAppMatrices = 0;
  if(root_ == nullptr) return; //void tree
  HMatrixNode<T, I>* hn = root_;
  while(hn != nullptr)
    {
      nbNodes++;
      depth = std::max(depth, hn->depth_);
      if(hn->child_ != nullptr) hn = hn->child_;
      else   //no child move to brother or up
        {
          nbLeaves++;
          if(hn->admissible_) nbAdmissibles++;
          if(hn->appmat_ != nullptr)  nbAppMatrices++;
          if(hn->next_ != nullptr) hn = hn->next_;
          else   //move up
            {
              while(hn->parent_ != nullptr && hn->parent_->next_ == nullptr) hn = hn->parent_;
              if(hn->parent_ == nullptr) hn = nullptr;
              else hn = hn->parent_->next_;
            }
        }
    }
}

// change the row/col ClusterTree pointer
// Be care: the new pointer has to describe the same structure as old, involved by copy of ClusterTree outside
template <typename T, typename I>
void HMatrix<T, I>::setClusterRow(ClusterTree<I>* ctp)
{
  rowCT_ = ctp;
  if(root_ != nullptr) root_->setClusterRow(rowCT_->root());
}

template <typename T, typename I>
void HMatrix<T, I>::setClusterCol(ClusterTree<I>* ctp)
{
  colCT_ = ctp;
  if(root_ != nullptr) root_->setClusterCol(colCT_->root());
}

//access to matrix sizes
template <typename T, typename I>
number_t HMatrix<T, I>::numberOfRows() const
{
  if(root_ == nullptr) return 0;
  return root_->rowNode_->dofNumbers().size();
}

template <typename T, typename I>
number_t HMatrix<T, I>::numberOfCols() const
{
  if(root_ == nullptr) return 0;
  return root_->colNode_->dofNumbers().size();
}

template <typename T, typename I>
dimPair HMatrix<T, I>::dimValues() const
{
  if(root_ == nullptr) return dimPair(1, 1);   //scalar coefficients
  return dimPair(root_->rowSub_, root_->colSub_);
}

//get all leaves (recursive)
//when removeVoidLeaf is true, void leafs (mat_==nullptr && appmat_=0) are removed from the list of leaves, default behavior
template <typename T, typename I>
std::list<HMatrixNode<T, I>* > HMatrix<T, I>::getLeaves(bool removeVoidLeaf) const
{
  std::list<HMatrixNode<T, I>* > leaves;
  if(root_ != nullptr) root_->getLeaves(leaves, removeVoidLeaf);
  return leaves;
}

//get leaves in a list ordered such that
//  in each packet of nt nodes, EXCEPT the last one, row/col index sets of every couples of nodes do not intersect
//    unsafe gives the size of the last packet that is not thread safe in a matrix vector product
//    if removeVoidLeaf is true, void leafs (mat_==nullptr && appmat_=0) are removed from the list of leaves, default behavior
template <typename T, typename I>
std::list<HMatrixNode<T, I>* > HMatrix<T, I>::getLeaves(AccessType access, number_t nt, number_t& unsafe, bool removeVoidLeaf) const
{
  std::list<HMatrixNode<T, I>* > leaves;
  if(root_ == nullptr) return leaves;
  root_->getLeaves(leaves);
  if(nt < 2) return leaves; //nothing to do

  Vector<number_t> flags;
  if(access == _col) flags.resize(root_->colNode_->dofNumbers().size(), 0);
  else flags.resize(root_->rowNode_->dofNumbers().size(), 0);
  std::list<HMatrixNode<T, I>* > oleaves;
  typename std::list<HMatrixNode<T, I>* >::iterator itn = leaves.begin();
  number_t k = 1, nbtry;

  while(itn != leaves.end())
    {
      if(leaves.size() == 1) //last packet does not satisfy the independance property !!
        {
          if(!removeVoidLeaf || (*itn)->mat_ != nullptr || (*itn)->appmat_ != nullptr) oleaves.push_back(*itn);
          return oleaves;
        }
      else if(!removeVoidLeaf || (*itn)->mat_ != nullptr || (*itn)->appmat_ != nullptr)
        {
          const std::vector<number_t>* rowcols;
          if(access == _col) rowcols = &(*itn)->colNode_->dofNumbers();
          else rowcols = &(*itn)->rowNode_->dofNumbers();
          std::vector<number_t>::const_iterator itr = rowcols->begin();
          bool reject = false;
          for(; itr != rowcols->end() && !reject; ++itr)
            {
              if(flags[*itr] == 0) flags[*itr] = 1;
              else reject = true; //intersection
            }
          if(reject)   //swap current node and last and try again
            {
              HMatrixNode<T, I>* curNode = *itn;
              leaves.erase(itn);
              leaves.push_back(curNode);
              itn = leaves.begin();
              nbtry++;
              if(nbtry > leaves.size())
                {
                  unsafe = leaves.size() + k - 1;
                  oleaves.insert(oleaves.end(), leaves.begin(), leaves.end());
                  //trace_p->pop();
                  return oleaves;
                }
            }
          else
            {
              oleaves.push_back(*itn);
              leaves.erase(itn);
              itn = leaves.begin();
              if(k == nt)
                {
                  k = 0;
                  flags *= 0;
                }
              k++;
              nbtry = 0;
            }
        }
      else   // removeVoidLeaf && (*itn)->mat_==nullptr && (*itn)->appmat_==nullptr : nothing to compute, remove leaf
        {
          leaves.erase(itn);
          itn = leaves.begin();
        }
    }
  return leaves;
}

// initialize internal counter of HMatrixNode<T,I> for counting some operations
template <typename T, typename I>
void HMatrix<T, I>::initCounter(number_t n)
{
  HMatrixNode<T, I>::initCounter(n);
}

// stop internal counter of HMatrixNode<T,I>
template <typename T, typename I>
void HMatrix<T, I>::stopCounter()
{
  HMatrixNode<T, I>::stopCounter();
}

//average size of leaves
template <typename T, typename I>
std::pair<number_t, number_t> HMatrix<T, I>::averageSize() const
{
  std::list<HMatrixNode<T, I>* > leaves = getLeaves(); //get list of leaves
  typename std::list<HMatrixNode<T, I>* >::iterator itl = leaves.begin();
  number_t nbr = 0, nbc = 0, nbl = leaves.size();
  for(; itl != leaves.end(); ++itl)
    {
      if((*itl)->mat_ != nullptr)
        {
          nbr += (*itl)->mat_->numberOfRows();
          nbc += (*itl)->mat_->numberOfCols();
        }
      else if((*itl)->appmat_ != nullptr)
        {
          nbr += (*itl)->appmat_->numberOfRows();
          nbc += (*itl)->appmat_->numberOfCols();
        }
    }
  return std::pair<real_t, real_t>(nbr / nbl, nbc / nbl);
}

// average rank of admissible leaves
template <typename T, typename I>
number_t HMatrix<T, I>::averageRank() const
{
  std::list<HMatrixNode<T, I>* > leaves = getLeaves(); //get list of leaves
  typename std::list<HMatrixNode<T, I>* >::iterator itl = leaves.begin();
  number_t r = 0, nbl = 0;
  for(; itl != leaves.end(); ++itl)
    {
      if((*itl)->appmat_ != nullptr)
        {
          r += (*itl)->appmat_->rank();
          nbl++;
        }
    }
  if(nbl > 0) return r /= nbl;
  else return 0;
}


template <typename T, typename I>
std::vector<T>&  HMatrix<T, I>::multMatrixVector(const std::vector<T>& x, std::vector<T>& hx) const  //! Hmatrix * Vector
{
  if (root_ == nullptr)
  {
    where("HMatrix::multMatrixVector(...)");
    error("null_pointer", "root_");
  }
  return root_->multMatrixVector(x, hx);
}

// product matrix x vector, omp
template <typename T, typename I>
std::vector<T>&  HMatrix<T, I>::multMatrixVectorOmp(const std::vector<T>& x, std::vector<T>& hx) const
{
  trace_p->push("HMatrix<T,I>::multMatrixVectorOmp(x,hx)");
  if (root_ == nullptr) error("null_pointer", "root_");
  #ifdef XLIFEPP_WITH_OMP
    Environment::parallel(false);  //to disable parallelism in LargeMatrix product
    std::list<HMatrixNode<T, I>* > leaves = getLeaves(); //get list of leaves
    std::vector<HMatrixNode<T, I>* > vleaves(leaves.begin(), leaves.end());
    leaves.clear();
    number_t n = root_->rowNode_->dofNumbers().size();
    number_t nt = numberOfThreads();
    hx.resize(n, T());
    std::vector<std::vector<T> > hxs(nt, std::vector<T>(n, T()));
    #pragma omp parallel
    {
      const int kthread = omp_get_thread_num();
      #pragma omp for schedule(dynamic)
      for(number_t k = 0; k < vleaves.size(); k++)
        {
          vleaves[k]->multMatrixVectorNode(x, hxs[kthread], sym);
        }
      #pragma omp for
      for(number_t i = 0; i < n; i++)
        {
          for(number_t j = 0; j < nt; j++)  hx[i] += hxs[j][i];
        }
    }
    Environment::parallel(true);
    trace_p->pop();
    return hx;
  #else
    std::list<HMatrixNode<T, I>* > leaves = getLeaves(); //get list of leaves
    typename std::list<HMatrixNode<T, I>* >::iterator itl = leaves.begin();
    for(;itl!=leaves.end();++itl) (*itl)->multMatrixVectorNode(x, hx, sym);
    trace_p->pop();
    return hx;
  #endif
}

//LU Factorization
template <typename T, typename I>
void HMatrix<T, I>::lu()
{
  trace_p->push("HMatrix::lu");
  if (root_ == nullptr) error("null_pointer", "root_");
  root_->lu();
  trace_p->pop();
}

//Frobenius norm
template <typename T, typename I>
real_t HMatrix<T, I>::norm2() const
{
  trace_p->push("HMatrix<T,I>::norm2");
  if (root_ == nullptr) error("null_pointer", "root_");
  real_t n = 0.;
  std::list<HMatrixNode<T, I>* > leaves = getLeaves(); //get list of leaves
  typename std::list<HMatrixNode<T, I>* >::iterator itl, itlb = leaves.begin();
  #ifdef XLIFEPP_WITH_OMP
    number_t nbl = leaves.size();
    std::vector<real_t> ns(nbl);
    #pragma omp for private(itl)
    for(number_t k = 0; k < nbl; k++)
      {
        itl = itlb;
        for(number_t i = 0; i < k; i++) ++itl;
        if(sym != _noSymmetry && !(*itl)->isDiag_)
          ns[k] = 2 * (*itl)->norm2();
        else
          ns[k] = (*itl)->norm2();
      }
    std::vector<real_t>::iterator itn = ns.begin();
    for(number_t k = 0; k < nbl; k++, ++itn) n += (*itn) * (*itn);
  #else
    for(itl = itlb; itl != leaves.end(); ++itl)
      {
        real_t nk = (*itl)->norm2();
        n += nk * nk;
        if(sym != _noSymmetry && !(*itl)->isDiag_) n += nk * nk; //add symmetric part
      }
  #endif
  trace_p->pop();
  return std::sqrt(n);
}

//infinite norm
template <typename T, typename I>
real_t HMatrix<T, I>::norminfty() const
{
  trace_p->push("HMatrix<T,I>::norminfty()");
  if (root_ == nullptr) error("null_pointer", "root_");
  std::list<HMatrixNode<T, I>* > leaves = getLeaves(); //get list of leaves
  real_t n = 0.;
  typename std::list<HMatrixNode<T, I>* >::iterator itl, itlb = leaves.begin();
  #ifdef XLIFEPP_WITH_OMP
    number_t nbl = leaves.size();
    std::vector<real_t> ns(nbl);
    #pragma omp for private(itl)
    for(number_t k = 0; k < nbl; k++)
      {
        itl = itlb;
        for(number_t i = 0; i < k; i++) ++itl;
        ns[k] = (*itl)->norminfty();
      }
    std::vector<real_t>::iterator itn = ns.begin();
    for(number_t k = 0; k < nbl; k++, ++itn) n = std::max(n, *itn);
  #else
    for(itl = itlb; itl != leaves.end(); ++itl) n = std::max(n, (*itl)->norminfty());
  #endif
  trace_p->pop();
  return n;
}

/* print recursively matrix structure as a list of block matrix [I,J] x [K,L]
     0/1/2   I   J   K   L
 where the first number is the admissibility status of the block: admissible leaf ->1, no admissible leaf -> 2, node -> 0
   fr and fc are the first rank of row/col numbers
   if all is false only leaf blocks are printed
   if shift is true, add 2*depth blanks on each line

 this function may be useful to plot the matrix structure by printing on a file:
    ofstream out("file.txt");
    hm.printStructure(out);    //print only leaves with no shift
    out.close();
*/
template <typename T, typename I>
void HMatrix<T, I>::printStructure(std::ostream& out, bool all, bool shift) const //! print matrix structure
{
  if(root_ != nullptr) root_->printStructure(out, 1, 1, all, shift);
}

//save Hmatrix structure; same format as printStructure
template <typename T, typename I>
void HMatrix<T, I>::saveStructureToFile(const string_t& fn) const
{
  trace_p->push("HMatrix<T,I>::saveStructureToFile()");
  if (root_ == nullptr) error("null_pointer", "root_");
  std::ofstream out(fn.c_str());
  if (!out.is_open()) error("file_failopen", "HMatrix::saveStructureToFile(String)", fn);
  printStructure(out, false, false);
  out.close();
  trace_p->pop();
}

// print Hmatrix
template <typename T, typename I>
void HMatrix<T, I>::print(std::ostream& out) const
{
  if(theVerboseLevel == 0) return;
  if(root_ == nullptr)
    {
      out << " HMatrix with no structure" << eol;
      return;
    }
  string_t vt = words("value", valueType_) + " " + words("structure", strucType_);
  string_t na = name;
  if(na == "") na = "?";
  out << vt << " HMatrix " << na << " " << root_->rowNode_->size() << " x " << root_->colNode_->size() << " " << words("symmetry", sym);
  out << ", number of non zero: " << nbNonZero();
  out << ", depth: " << depth << ", number of nodes: " << nbNodes << ", number of leaves:" << nbLeaves;
  out << ", number of admissible leaves:" << nbAdmissibles << ", number of approximate matrices:" << nbAppMatrices;
  out << " average size of leaves: " << averageSize() << " average rank of admissible leaves: " << averageRank() << eol;
  if(theVerboseLevel > 2) root_->print(out);
}

// print Hmatrix
template <typename T, typename I>
void HMatrix<T, I>::printSummary(std::ostream& out) const
{
  if(root_ == nullptr)
    {
      out << " void" << eol;
      return;
    }
  out << " (" << nbNonZero() << " coefficients)";
  out << ", depth: " << depth << ", " << nbNodes << " nodes, " << nbLeaves << " leaves, ";
  out << nbAdmissibles << " admissible leaves, " << nbAppMatrices << " approximate matrices, ";
  out << " average size: " << averageSize();
  number_t ar = averageRank();
  if(ar > 0) out << ", average rank of approximate matrices: " << ar;
  out << eol;
}

//admissibility rules of product of ClusterNode
// bounding box is given by the 2 vectors xmin = [x1_min, x2_min, x3_min] and xmax = [x1_max, x2_max, x3_max]
template <typename I>
bool blockAdmissible(ClusterNode<I>* rowNode, ClusterNode<I>* colNode, HMAdmissibilityRule hmaRule, real_t eta)
{
  switch (hmaRule)
  {
    case _noRule:
      return false;
    case _boxesRule: //standard boxes rule diam(rowBox) <= 2*eta*dist(rowBox,colBox)
      return std::max(rowNode->boxDiameter(), colNode->boxDiameter())
             <= 2.*eta * dist(rowNode->box(), colNode->box());

    default:
      where("blockAdmissible(...)");
      error("admissibility_rule_not_handled");
  }
  return false;  //fake return
}

//load Hmatrix<T> from LargeMatrix<T>, HMatrix tree structure has to be up to date
//if tree structure is symmetric only the lower part of LargeMatrix is loaded
template <typename T, typename I>
void HMatrix<T, I>::load(const LargeMatrix<T>& lm, HMApproximationMethod hma)
{
  trace_p->push("HMatrix<T,I>::load(LargeMatrix, ...)");
  if (root_ == nullptr) error("null_pointer", "root_");
  switch(hma)
  {
    case _noHMApproximation:
    {
      //travel tree, extract submatrix and allocate dense matrix on each leaf node of HMatrix
      HMatrixNode<T, I>* hn = root_;
      while (hn != nullptr)
      {
        if (hn->child_ == nullptr) //no child, extract submatrix rowNode->dofNumbers, colNode->dofNumbers
        {
          if (hn->mat_ == nullptr)
          {
            std::vector<number_t> rowNum(hn->rowNode_->dofNumbers());
            std::vector<number_t>::iterator itn = rowNum.begin();
            for (; itn != rowNum.end(); ++itn) *itn += 1;                    //shift from 1 the row numbering
            std::vector<number_t> colNum(hn->colNode_->dofNumbers());
            for (itn = colNum.begin(); itn != colNum.end(); ++itn) *itn += 1; //shift from 1 the col numbering
            std::vector<number_t> pos;
            lm.positions(rowNum, colNum, pos);
            if (*std::max_element(pos.begin(), pos.end()) > 0) //not a zero matrix
              {
                hn->mat_ = new LargeMatrix<T>(rowNum.size(), colNum.size(), _dense, _row);
                typename std::vector<T>::iterator itv = hn->mat_->values().begin();
                typename std::vector<T>::const_iterator itlm = lm.values().begin();
                std::vector<number_t>::iterator itp = pos.begin();
                itv++;
                for (; itp != pos.end(); ++itv, ++itp)
                  if(*itp > 0) *itv = *(itlm + *itp);
              }
          }
          if (hn->next_ != nullptr) hn = hn->next_; //deal with next
          else   //no next go up
          {
            while (hn->parent_ != nullptr && hn->parent_->next_ == nullptr) hn = hn->parent_;
            if (hn->parent_ == nullptr) hn = nullptr;
            else hn = hn->parent_->next_;
          }
        }
        else hn = hn->child_;
      }
    }
    break;
    default:
      error("compression_not_handled", words("HMatrix approximation", hma));
  }
  trace_p->pop();
}

// convert HMatrix to LargeMatrix
template <typename T, typename I>
LargeMatrix<T> HMatrix<T, I>::toLargeMatrix(StorageType st, AccessType at) const
{
  trace_p->push("HMatrix<T,I>::toLargeMatrix(...)");
  number_t nbr = numberOfRows(), nbc = numberOfCols();
  if (nbr == 0) error("is_null", "nbr");
  if (nbc == 0) error("is_null", "nbc");
  if (st != _dense) error("storage_unexpected", words("storage type", _dense), words("storage type", st));
  if (at == _sym) error("access_unexpected", words("access type", _sym), words("access type", at));
  //create dense LargeMatrix
  LargeMatrix<T> lm(nbr, nbc, st, at);
  //fill in LargeMatrix by travelling tree (not recursive)
  HMatrixNode<T, I>* hn = root_;
  while (hn != nullptr)
    {
      if(hn->child_ != nullptr) hn = hn->child_; // move to child
      else   //no child: add matrix and move to brother or up
        {

          //add node matrix
          std::vector<number_t> rowDofs = hn->rowNode_->dofNumbers(), colDofs = hn->colNode_->dofNumbers();
          std::vector<number_t>::iterator itn;
          for(itn = rowDofs.begin(); itn != rowDofs.end(); ++itn) *itn += 1; //shift row dof index
          for(itn = colDofs.begin(); itn != colDofs.end(); ++itn) *itn += 1; //shift col dof index
          LargeMatrix<T> nodelm;
          LargeMatrix<T>* lmnode = hn->mat_;
          if(lmnode == nullptr && hn->appmat_ != nullptr) //convert approximate Matrix to LargeMatrix
            {
              nodelm = hn->appmat_->toLargeMatrix();
              lmnode = &nodelm;
            }
          if(lmnode != nullptr)
            {
              lm.add(*lmnode, rowDofs, colDofs, real_t(1.));  //insert LargeMatrix
              //add "symmetric" node
              if(sym != _noSymmetry && !hn->isDiag_)
                {
                  LargeMatrix<T> lmt = transpose(*lmnode);
                  if(sym == _selfAdjoint || sym == _skewAdjoint) lmt.toConj();
                  real_t a = 1.;
                  if(sym == _skewSymmetric || sym == _skewAdjoint) a = -1.;
                  lm.add(*lmnode, colDofs, rowDofs, a);

                }
            }
          // move to brother or up
          if(hn->next_ != nullptr) hn = hn->next_;
          else
            {
              while(hn->parent_ != nullptr && hn->parent_->next_ == nullptr) hn = hn->parent_;
              if(hn->parent_ == nullptr) hn = nullptr;
              else hn = hn->parent_->next_;
            }
        }
    }
  return lm;
  trace_p->pop();
}

//add FE LargeMatrix to Hmatrix
// it is assumed that FE LargeMatrix is sparse, so only pairs of close node block are taken into consideration
template <typename T, typename I>
void HMatrix<T, I>::addFELargeMatrix(const LargeMatrix<T>& femat)
{
  trace_p->push("HMatrix<T,I>::addFELargeMatrix(...)");

  //add LargeMatrix by travelling Hmatrix leaves (not recursive)
  std::list<HMatrixNode<T, I>* > leaves = getLeaves(); //get list of leaves
  number_t nbl = leaves.size();
  typename std::list<HMatrixNode<T, I>* >::iterator itl, itlb = leaves.begin();
  #ifdef XLIFEPP_WITH_OMP
    Environment::parallel(false);  //to disable internal parallelism
    #pragma omp for private(itl)
  #endif
    for(number_t k = 0; k < nbl; k++)
      {
        itl = itlb;
        for(number_t i = 0; i < k; i++) ++itl;
        HMatrixNode<T, I>& hn = **itl;
        if(!hn.admissible_ && hn.mat_ != nullptr) //deal only with non admissible node
          {
            LargeMatrix<T> &hmat = *hn.mat_;
            ClusterNode<I>* rownode = hn.rowNode_, *colnode = hn.colNode_;
            StorageType stmat = hmat.storageType();
            if(hasCommonElts(*rownode, *colnode))  // has common elements
              {
                std::vector<number_t> rowDofs = rownode->dofNumbers(), colDofs = colnode->dofNumbers();
                std::vector<number_t>::iterator itr, itc;
                std::vector<std::pair<number_t, number_t> >::iterator itp;
                if(stmat == _dense) //hmat is dense
                  {
                    number_t r = 1;
                    for(itr = rowDofs.begin(); itr != rowDofs.end(); ++itr, ++r)
                      {
                        std::vector<std::pair<number_t, number_t> > cols = femat.getRow(*itr + 1);
                        number_t c = 1;
                        itc = colDofs.begin();
                        for(itp = cols.begin(); itp != cols.end(); ++itp) //assuming growing column index
                          {
                            while(itc != colDofs.end() && itp->first != (*itc + 1))
                              {
                                itc++;
                                c++;
                              }
                            if(itc != colDofs.end()) hmat(r, c) += femat(itp->second);
                          }
                      }
                  }
                else
                  error("storage_unexpected", words("storage type", _dense), words("storage type", stmat));
              }
          }
      }
  #ifdef XLIFEPP_WITH_OMP
    Environment::parallel(true);  //to re enable parallelism
  #endif
  trace_p->pop();
}

template<typename T, typename I> number_t HMatrixNode<T, I>::counter = 0;
template<typename T, typename I> bool HMatrixNode<T, I>::counterOn = false;

/*!
  \class HMatrixIM
  integral over a product of geometric domains based on hierarchical matrix representation
  constructor keys  :
     _cluster               : ClusterTree<FeDof> objects (C or {rowC,colC})
     _min_block_size        : minimal row/column size of blocks (Number s ou Numbers {rs,cs})
     _clustering_method     : one of _regularBisection, _boundingBoxBisection, _cardinalityBisection, _uniformKdtree, _nonuniformKdtree
     _approximation_method  : one of  _noHMApproximation, _svdCompression, _rsvdCompression, _r3svdCompression, _acaFull, _acaPartial, _acaPlus
     _max_rank              : maximal rank of approximate matrix (Number)
     _threshold             : criterium in svd compression (keep singular values greater than threshold) (Real)
     _eta                   : ratio in the admissibility criteria diam(Br) < 2*eta*dist(Br,Bc)  (default eta = 1)
     _method                : IntegrationMethod object or IntegrationMethods object

     give mandatory either _clustering_method or_cluster
     give _block_size_min if _clustering_method given
     give mandatory _approximate_method and _integration_methods
     give mandatory either max_rank or eps_rank if _approximate_method != _noHMApproximation
*/
class HMatrixIM : public DoubleIM
{
  public:
    mutable ClusterTree<FeDof>* rowCluster_; //!< pointer to row cluster tree
    mutable ClusterTree<FeDof>* colCluster_; //!< pointer to col cluster tree
    mutable bool deletePointers_;            //!< flag enabling pointers deallocation
    ClusteringMethod clusterMethod;          //!< clustering method
    HMApproximationMethod hmAppMethod;       //!< type of approximation of admissible blocks
    number_t minRowSize, minColSize;         //!< minimum row/col size of leaf matrix
    number_t maxRank;                        //!< maximal rank of approximate matrices
    real_t threshold;                        //!< threshold used in svd compression (keep singular values greater than thr)
    real_t eta;                              //!< ratio in the admissibility criteria diam(Br) < 2*eta*dist(Br,Bc)  (default eta = 1)
    IntegrationMethod* intgMethod;           //!< real integration method on a couple of elements (old fashion)
    IntegrationMethods intgMethods;          //!< list of integration method on a couple of elements

    //constructors with IntegrationMethod
    HMatrixIM(const Parameter& p1)
    {
      std::vector<Parameter> ps={p1};
      build(ps);
    }
    HMatrixIM(const Parameter& p1, const Parameter& p2)
    {
      std::vector<Parameter> ps={p1, p2};
      build(ps);
    }
    HMatrixIM(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    {
      std::vector<Parameter> ps={p1, p2, p3};
      build(ps);
    }
    HMatrixIM(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4};
      build(ps);
    }
    HMatrixIM(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5};
      build(ps);
    }
    HMatrixIM(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
              const Parameter& p6)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
      build(ps);
    }
    HMatrixIM(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
              const Parameter& p6, const Parameter& p7)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
      build(ps);
    }
    HMatrixIM(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
              const Parameter& p6, const Parameter& p7, const Parameter& p8)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
      build(ps);
    }
    HMatrixIM(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
              const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
      build(ps);
    }
    HMatrixIM(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
              const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
      build(ps);
    }
    HMatrixIM(ClusteringMethod clm, number_t minRow, number_t minCol, HMApproximationMethod hmap, number_t maxr, IntegrationMethod& im);
    HMatrixIM(ClusteringMethod clm, number_t minRow, number_t minCol, HMApproximationMethod hmap, real_t epsr, IntegrationMethod& im);
    HMatrixIM(HMApproximationMethod hmap,  number_t maxr, IntegrationMethod& im, ClusterTree<FeDof>& rowC, ClusterTree<FeDof>& colC);
    HMatrixIM(HMApproximationMethod hmap,  int maxr, IntegrationMethod& im, ClusterTree<FeDof>& rowC, ClusterTree<FeDof>& colC);
    HMatrixIM(HMApproximationMethod hmap,  real_t epsr, IntegrationMethod& im, ClusterTree<FeDof>& rowC, ClusterTree<FeDof>& colC);

    //constructors with IntegrationMethods
    HMatrixIM(ClusteringMethod clm, number_t minRow, number_t minCol, HMApproximationMethod hmap, number_t maxr, IntegrationMethods& ims);
    HMatrixIM(ClusteringMethod clm, number_t minRow, number_t minCol, HMApproximationMethod hmap, real_t epsr, IntegrationMethods& ims);
    HMatrixIM(HMApproximationMethod hmap,  number_t maxr, IntegrationMethods& ims, ClusterTree<FeDof>& rowC, ClusterTree<FeDof>& colC);
    HMatrixIM(HMApproximationMethod hmap, int maxr, IntegrationMethods& ims, ClusterTree<FeDof>& rowC, ClusterTree<FeDof>& colC);
    HMatrixIM(HMApproximationMethod hmap,  real_t epsr, IntegrationMethods& ims, ClusterTree<FeDof>& rowC, ClusterTree<FeDof>& colC);

    //destructor
    ~HMatrixIM();
    void clear();

    virtual HMatrixIM* clone() const      //covariant
    { return new HMatrixIM(*this); }

    //access and assign row/col cluster pointers
    ClusterTree<FeDof>& rowCluster() const
    {
      if (rowCluster_==nullptr)
      {
        where("rowCluster()");
        error("null_pointer","rowCluster_");
      }
      return *rowCluster_;
    }

    ClusterTree<FeDof>& colCluster() const
    {
      if (colCluster_==nullptr)
      {
        where("colCluster()");
        error("null_pointer","colCluster_");
      }
      return *colCluster_;
    }
    void setRowCluster(ClusterTree<FeDof>* clp) const
    { rowCluster_ = clp; }
    void setColCluster(ClusterTree<FeDof>* clp) const
    { colCluster_ = clp; }

    //print stuff
    void print(std::ostream& out) const;
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator<<(std::ostream& out, const HMatrixIM& him)
    { him.print(out); return out; }
    
  private:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}
};

/* -----------------------------------------------------------------------------------------------------------------------------
                                    LargeMatrix and ApproximateMatrix product
   -----------------------------------------------------------------------------------------------------------------------------
*/
//! LargeMatrix * ApproximateMatrix -> ApproximateMatrix
template <typename T>
ApproximateMatrix<T>& multMatrix(LargeMatrix<T>& L, ApproximateMatrix<T>& A, ApproximateMatrix<T>& LA)
{
  switch(A.approximationType)
  {
    case _lowRankApproximation:
    {
      if (LA.approximationType != _lowRankApproximation)
      {
        where("multMatrix(LargeMatrix, ApproximateMatrix, ApproximateMatrix)");
        error("low_rank_only");
      }
      return multMatrix(L, static_cast<LowRankMatrix<T>&>(A), static_cast<LowRankMatrix<T>&>(LA));
    }
    break;
    default:
      where("multMatrix(LargeMatrix, ApproximateMatrix, ApproximateMatrix)");
      error("approx_matrix_not_handled");
  }
  return LA;
}

//! ApproximateMatrix * LargeMatrix -> ApproximateMatrix
template <typename T>
ApproximateMatrix<T>& multMatrix(ApproximateMatrix<T>& A, LargeMatrix<T>& L, ApproximateMatrix<T>& AL)
{
  switch(A.approximationType)
  {
    case _lowRankApproximation:
    {
      if (AL.approximationType != _lowRankApproximation)
      {
        where("multMatrix(ApproximateMatrix, LargeMatrix, ApproximateMatrix)");
        error("low_rank_only");
      }
      return multMatrix(static_cast<LowRankMatrix<T>&>(A), L, static_cast<LowRankMatrix<T>&>(AL));
    }
    break;
    default:
      where("multMatrix(ApproximateMatrix, LargeMatrix, ApproximateMatrix)");
      error("approx_matrix_not_handled");
  }
  return AL;
}

//! ApproximateMatrix * ApproximateMatrix  -> ApproximateMatrix
template <typename T>
ApproximateMatrix<T>& multMatrix(ApproximateMatrix<T>& A, ApproximateMatrix<T>& B, ApproximateMatrix<T>& AB)
{
  if (A.approximationType != _lowRankApproximation || B.approximationType != _lowRankApproximation || AB.approximationType != _lowRankApproximation)
  {
    where("multMatrix(ApproximateMatrix, ApproximateMatrix, ApproximateMatrix");
    error("low_rank_only");
  }
  return multMatrix(static_cast<LowRankMatrix<T>&>(A), static_cast<LowRankMatrix<T>&>(B), static_cast<LowRankMatrix<T>&>(AB));
}

} // end of namespace xlifepp

#endif /* H_MATRIX_HPP */
