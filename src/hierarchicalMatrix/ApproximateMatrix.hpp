/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ApproximateMatrix.hpp
  \author E. Lunéville
  \since 22 juin 2016
  \date 22 juin 2016

  \brief Definition of the xlifepp::ApproximateMatrix class

  xlifepp::ApproximateMatrix<T> is the base class of various approximate matrix methods:
             - LowRankMatrix<T> : matrix representation from small set of vectors
             - FMMMatrix<T>     : matrix representation from Fast Multipole Method
 */

#ifndef APPROXIMATE_MATRIX_HPP
#define APPROXIMATE_MATRIX_HPP

#include "config.h"
#include "utils.h"
#include "largeMatrix.h"
#include "mathsResources.h"

namespace xlifepp
{
using std::abs;

enum MatrixApproximationType {_lowRankApproximation = 1,_fmmApproximation};

//==============================================================================================================
/*!
   \class ApproximateMatrix
   base class of all approximate matrix methods
*/
//==============================================================================================================

template <class T>
class ApproximateMatrix
{
public:
  MatrixApproximationType approximationType;     //!< type of approximation
  string_t name;                                 //!< optional name, useful for documentation
  bool isAllocated;                              //!< true if allocated

  virtual ~ApproximateMatrix() {}               //!< virtual destructor
  virtual ApproximateMatrix<T>* clone() const=0; //!< virtual creation of a clone

  virtual number_t numberOfRows() const =0;
  virtual number_t numberOfCols() const =0;
  virtual number_t nbNonZero() const =0;
  virtual number_t rank() const =0;

  virtual ApproximateMatrix<T>* extract(const std::vector<number_t>& rowIndex,
                                        const std::vector<number_t>& colIndex) const=0;  //!< extract part of ApproximateMatrix (virtual)
  virtual void restrict(const std::vector<number_t>& rowIndex,
                        const std::vector<number_t>& colIndex)=0;      //!< restrict ApproximateMatrix to smaller numbering
  virtual void extend(const std::vector<number_t>& rowIndex, const std::vector<number_t>& colIndex,
                      number_t m=0, number_t n=0) =0;                  //!< extend ApproximateMatrix to larger numbering

  virtual std::vector<T>& multMatrixVector(const std::vector<T>&, std::vector<T>&) const = 0; //!< virtual product matrix * vector
  virtual std::vector<T>& multVectorMatrix(const std::vector<T>&, std::vector<T>&) const = 0; //!< virtual product vector * matrix
  virtual void multMatrixRow(T* M, T* R, number_t p) const=0;     //!< right product by a row dense matrix (L*matRow)-> row matrix
  virtual void multLeftMatrixRow(T* M, T* R, number_t p) const=0; //!< left product by a row dense matrix  (matRow*L)-> row matrix
  virtual void multMatrixCol(T* M, T* R, number_t p) const=0;     //!< right product by a col dense matrix (L*matCol)-> col matrix
  virtual void multLeftMatrixCol(T* M, T* R, number_t p) const=0; //!< left product by a col dense matrix  (matCol*L)-> col matrix
  virtual void luFactorize(bool withPermutation=false) = 0;       //!< virtual LU factorization

  virtual real_t norminfty() const=0;              //!< infinite norm
  virtual real_t squaredNorm() const =0;           //!< squared Frobenius norm
  virtual real_t norm2() const                     //! Frobenius norm
  {return std::sqrt(squaredNorm());}

  virtual Matrix<T> toMatrix() const = 0;          //!< convert to Matrix (dense row)
  virtual LargeMatrix<T> toLargeMatrix(StorageType st=_dense, AccessType=_row) const =0; //!< convert to LargeMatrix

  virtual void print(std::ostream&) const =0;      //!< print to stream
  virtual void print(PrintStream& os) const =0;
};

template <typename T>
std::ostream& operator<<(std::ostream& out, const ApproximateMatrix<T>& am)
{
  am.print(out);
  return out;
}

//==============================================================================================================
/*!
   \class LowRankMatrix
   class dealing with matrix represented by a small sets of vectors
                                             A = U D V*
          with U a (m,r) matrix, V a (n,r) matrix and D a (r,r) diagonal matrix stored as a vector
          when D=Id it is not stored
*/
//==============================================================================================================

template <class T>
class LowRankMatrix : public ApproximateMatrix<T>
{
public:
  Matrix<T> U_, V_;
  Vector<T> D_;
  HMApproximationMethod cm_;    //!< compression method used when re-compress
  number_t rank_;               //!< prescribed rank
  real_t eps_;                  //!< prescribed precision
  FactorizationType factorization_; //!< one of _noFactorization, _lu, _ldlt, _ldlstar, _llt, _llstar, _qr, _umfpack

  //constructors
  LowRankMatrix();    //!< default constructor
  LowRankMatrix(const Matrix<T>&, const Matrix<T>&, const Vector<T>&, const string_t& na="");//!< constructor from matrices
  LowRankMatrix(const Matrix<T>&, const Matrix<T>&, const string_t& na="");                  //!< constructor from matrices, no diag
  LowRankMatrix(number_t m, number_t n, number_t r, const string_t& na="");                  //!< constructor from dimensions
  LowRankMatrix(number_t m, number_t n, number_t r, bool noDiag, const string_t& na="");     //!< constructor from dimensions, no diag
  template <typename ITM, typename ITV>
  LowRankMatrix(number_t m, number_t n, number_t r,
                ITM itmu, ITM itmv, ITV itd, const string_t& na="");   //!< constructor from matrice iterator
  template <typename ITM>
  LowRankMatrix(number_t m, number_t n, number_t r,
                ITM itmu, ITM itmv, const string_t& na="");            //!< constructor from matrice iterator, no diag
  LowRankMatrix(const LargeMatrix<T>&, HMApproximationMethod=_r3svdCompression,
                number_t =0, real_t = theTolerance);                   //!< constructor from LargeMatrix to be compressed
  void fromLargeMatrix(const LargeMatrix<T>&);                         //!< build LowRankMatrix from LargeMatrix

  virtual ApproximateMatrix<T>* clone() const;                         //!< create a clone
  virtual LowRankMatrix<T>* extract(const std::vector<number_t>& rowIndex,
                                    const std::vector<number_t>& colIndex) const;   //!< extract part of ApproximateMatrix (virtual)

  virtual void restrict(const std::vector<number_t>& rowIndex,
                        const std::vector<number_t>& colIndex);                 //!< restrict LowRankMatrix to smaller numbering
  virtual void extend(const std::vector<number_t>& rowIndex,const std::vector<number_t>& colIndex,
                      number_t m=0, number_t n=0);                              //!< extend LowRankMatrix to larger numbering

  //properties
  virtual number_t numberOfRows() const {return U_.numberOfRows();}
  virtual number_t numberOfCols() const {return V_.numberOfRows();}
  virtual number_t nbNonZero() const
  {return U_.numberOfRows()*U_.numberOfColumns()+V_.numberOfRows()*V_.numberOfColumns()+D_.size();}
  bool hasDiag() const
  {return D_.size()>0;}
  virtual number_t rank() const {return U_.numberOfColumns();} //!< return the real rank, not the prescribed rank
  std::vector<T> col(number_t) const;            //!< j-th column of UDV* (j>=1)
  std::vector<T> row(number_t) const;            //!< i-th row of UDV* (i>=1)
  T operator()(number_t i, number_t j) const;    //!< coefficient i,j of UDV* (i>=1, j>=1), expansive

  //convert to matrix
  template<typename I>
  void toDenseRow(I itm) const; //!< convert to dense row matrix passing iterator on the first matrix value
  virtual Matrix<T> toMatrix() const;   //!< convert to Matrix (dense row)
  virtual LargeMatrix<T> toLargeMatrix(StorageType st=_dense, AccessType=_row) const; //!< convert to LargeMatrix

  //algebraic operations
  virtual std::vector<T>& multMatrixVector(const std::vector<T>&, std::vector<T>&) const; //!< virtual product matrix * vector
  virtual std::vector<T>& multVectorMatrix(const std::vector<T>&, std::vector<T>&) const; //!< virtual product vector * matrix
  void multMatrixRow(T* M, T* R, number_t p) const;     //!< right product by a row dense matrix (L*matRow)-> row matrix
  void multLeftMatrixRow(T* M, T* R, number_t p) const; //!< left product by a row dense matrix  (matRow*L)-> row matrix
  void multMatrixCol(T* M, T* R, number_t p) const;     //!< right product by a col dense matrix (L*matCol)-> col matrix
  void multLeftMatrixCol(T* M, T* R, number_t p) const; //!< left product by a col dense matrix  (matCol*L)-> col matrix
  virtual void luFactorize(bool withPermutation=false);//!< LU factorization

  LowRankMatrix<T>& operator+=(const LowRankMatrix<T>&);//!< add a LowRank matrix to current one
  LowRankMatrix<T>& operator-=(const LowRankMatrix<T>&);//!< substract a LowRank matrix to current one
  LowRankMatrix<T>& operator*=(const T&);               //!< multiply by a scalar
  LowRankMatrix<T>& operator/=(const T&);               //!< divide by a scalar
  LowRankMatrix<T>& add(const LowRankMatrix<T>&, const T&, real_t =0);   //!< add a scaled LowRank matrix to current one
  LowRankMatrix<T>& toFirsts(number_t r);               //!< restrict U_, V_ to their r first cols leading to a LowRankMatrix of rank r
  LowRankMatrix<T>& compress(HMApproximationMethod cm, number_t r=0, real_t eps=theTolerance); //!< compression of the LowRankMatrix
  LowRankMatrix<T>& svd(number_t r=0, real_t eps=theTolerance);    //!< svd  of a low rank matrice
  LowRankMatrix<T>& rsvd(number_t r=0, real_t eps=theTolerance);   //!< rsvd of a low rank matrice
  LowRankMatrix<T>& r3svd(real_t eps=theTolerance,number_t t = 0,
                          number_t p = 0, number_t q = 0, number_t maxit = 0); //!< r3svd of a low rank matrice

  virtual real_t norminfty() const;            //!< infinite norm
  virtual real_t squaredNorm() const;          //!< squared Frobenius norm

  //print stuff
  virtual void print(std::ostream&) const;     //!< print to stream
  virtual void print(PrintStream& os) const {print(os.currentStream());}
};

//==========================================================================================================
//                             extern declaration of svd, rsvd and r3svd
//==========================================================================================================
template<typename T>
LowRankMatrix<T>& svd(const LargeMatrix<T>& lm, LowRankMatrix<T>& lrm, number_t r=0, real_t eps=theTolerance);  //!< svd of a LargeMatrix
template<typename T>
LowRankMatrix<T>& rsvd(const LargeMatrix<T>& lm, LowRankMatrix<T>& lrm, number_t r=0, real_t eps=theTolerance); //!< rsvd of a LargeMatrix
template<typename T>
LowRankMatrix<T>& r3svd(const LargeMatrix<T>& lm, LowRankMatrix<T>& lrm, real_t eps=theTolerance,
                        number_t t = 0, number_t p = 0, number_t q = 0, number_t maxit = 0);                    //!< r3svd of a LargeMatrix

//==========================================================================================================
//                             LowRankMatrix members implementation
//==========================================================================================================
// default constructor
template<typename T>
LowRankMatrix<T>::LowRankMatrix()
  : cm_(_r3svdCompression),rank_(0),eps_(theTolerance)
{
  ApproximateMatrix<T>::approximationType=_lowRankApproximation;
  ApproximateMatrix<T>::isAllocated=false;
  factorization_=_noFactorization;
  U_.clear(); V_.clear(); D_.clear();
}

//constructor from matrices
template<typename T>
LowRankMatrix<T>::LowRankMatrix(const Matrix<T>& U, const Matrix<T>& V, const Vector<T>& D, const string_t & na)
{
  ApproximateMatrix<T>::approximationType = _lowRankApproximation;
  ApproximateMatrix<T>::name=na;
  number_t nu = U.numberOfColumns(), nv = V.numberOfColumns(), nd = D.size();
  if (nu != nv)
  {
    where("LowRankMatrix(Matrix<T>, Matrix<T>, Vector<T>, String)");
    error("entry_mismatch_dims",nu, nv);
  }
  if (nu != nd)
  {
    where("LowRankMatrix(Matrix<T>, Matrix<T>, Vector<T>, String)");
    error("entry_mismatch_dims",nu, nd);
  }
  U_=U; V_=V; D_=D;
  cm_=_r3svdCompression;
  rank_=0;
  eps_=theTolerance;
  ApproximateMatrix<T>::isAllocated=true;
  factorization_=_noFactorization;
}

template<typename T>
LowRankMatrix<T>::LowRankMatrix(const Matrix<T>& U, const Matrix<T>& V, const string_t & na)
{
  ApproximateMatrix<T>::approximationType = _lowRankApproximation;
  ApproximateMatrix<T>::name=na;
  number_t nu = U.numberOfColumns(), nv = V.numberOfColumns();
  if(nu!=nv)
  {
    where("LowRankMatrix(Matrix<T>, Matrix<T>, String)");
    error("entry_mismatch_dims",nu, nv);
  }
  U_=U; V_=V; D_.clear();
  cm_=_r3svdCompression;
  rank_=0;
  eps_=theTolerance;
  ApproximateMatrix<T>::isAllocated=true;
  factorization_=_noFactorization;
}

//constructor from dimensions
template<typename T>
LowRankMatrix<T>::LowRankMatrix(number_t m, number_t n, number_t r, const string_t & na)
{
  if (m == 0)
  {
    where("LowRankMatrix(Number, Number, Number, String)");
    error("is_null", "m");
  }
  if (n == 0)
  {
    where("LowRankMatrix(Number, Number, Number, String)");
    error("is_null", "n");
  }
  if (r == 0)
  {
    where("LowRankMatrix(Number, Number, Number, String)");
    error("is_null", "r");
  }
  ApproximateMatrix<T>::approximationType = _lowRankApproximation;
  ApproximateMatrix<T>::name=na;
  U_.changesize(m,r,T()); V_.changesize(n,r, T()); D_.resize(r);
  cm_=_r3svdCompression;
  rank_=0;
  eps_=theTolerance;
  ApproximateMatrix<T>::isAllocated=true;
  factorization_=_noFactorization;
}

template<typename T>
LowRankMatrix<T>::LowRankMatrix(number_t m, number_t n, number_t r, bool noDiag, const string_t& na) //! constructor from dimensions, no diag
{
  if (m == 0)
  {
    where("LowRankMatrix(Number, Number, Number, bool, String)");
    error("is_null", "m");
  }
  if (n == 0)
  {
    where("LowRankMatrix(Number, Number, Number, bool, String)");
    error("is_null", "n");
  }
  if (r == 0)
  {
    where("LowRankMatrix(Number, Number, Number, bool, String)");
    error("is_null", "r");
  }
  ApproximateMatrix<T>::approximationType = _lowRankApproximation;
  ApproximateMatrix<T>::name=na;
  U_.changesize(m,r,T()); V_.changesize(n,r, T()); D_.clear();
  cm_=_r3svdCompression;
  rank_=0;
  eps_=theTolerance;
  ApproximateMatrix<T>::isAllocated=true;
  factorization_=_noFactorization;
}

//constructor from iterators or pointers (itmu, itmv travels as a dense row matrix)
template <typename T>
template <typename ITM, typename ITV>
LowRankMatrix<T>::LowRankMatrix(number_t m, number_t n, number_t r,
                                ITM itmu, ITM itmv, ITV itd, const string_t& na)    //! constructor from matrice iterator
{
  if (m == 0)
  {
    where("LowRankMatrix(Number, Number, Number, iteratorM, iteratorM, iteratorV, String)");
    error("is_null", "m");
  }
  if (n == 0)
  {
    where("LowRankMatrix(Number, Number, Number, iteratorM, iteratorM, iteratorV, String)");
    error("is_null", "n");
  }
  if (r == 0)
  {
    where("LowRankMatrix(Number, Number, Number, iteratorM, iteratorM, iteratorV, String)");
    error("is_null", "r");
  }
  ApproximateMatrix<T>::approximationType = _lowRankApproximation;
  ApproximateMatrix<T>::name=na;
  U_.changesize(m,r, T()); V_.changesize(n,r, T()); D_.resize(r);
  typename Matrix<T>::iterator itm=U_.begin();
  for(number_t k=0; k<m*r; ++k, ++itm, ++itmu) *itm = *itmu;
  itm=V_.begin();
  for(number_t k=0; k<n*r; ++k, ++itm, ++itmv) *itm = *itmv;
  typename Vector<T>::iterator itv=D_.begin();
  for(number_t k=0; k<r; ++k, ++itv, ++itd)  *itv = *itd;
  cm_=_r3svdCompression;
  rank_=0;
  eps_=theTolerance;
  ApproximateMatrix<T>::isAllocated=true;
  factorization_=_noFactorization;
}

template <typename T>
template <typename ITM>
LowRankMatrix<T>::LowRankMatrix(number_t m, number_t n, number_t r,
                                ITM itmu, ITM itmv, const string_t& na)    //! constructor from matrice iterator
{
  if (m == 0)
  {
    where("LowRankMatrix(Number, Number, Number, iteratorM, iteratorM, String)");
    error("is_null", "m");
  }
  if (n == 0)
  {
    where("LowRankMatrix(Number, Number, Number, iteratorM, iteratorM, String)");
    error("is_null", "n");
  }
  if (r == 0)
  {
    where("LowRankMatrix(Number, Number, Number, iteratorM, iteratorM, String)");
    error("is_null", "r");
  }
  ApproximateMatrix<T>::approximationType = _lowRankApproximation;
  ApproximateMatrix<T>::name=na;
  U_.changesize(m,r, T()); V_.changesize(n,r, T());
  typename Matrix<T>::iterator itm=U_.begin();
  for(number_t k=0; k<m*r; ++k, ++itm, ++itmu) *itm = *itmu;
  itm=V_.begin();
  for(number_t k=0; k<n*r; ++k, ++itm, ++itmv) *itm = *itmv;
  cm_=_r3svdCompression;
  rank_=0;
  eps_=theTolerance;
  ApproximateMatrix<T>::isAllocated=true;
  factorization_=_noFactorization;
}

//! constructor from LargeMatrix to be compressed
template <typename T>
LowRankMatrix<T>::LowRankMatrix(const LargeMatrix<T>& lm, HMApproximationMethod cm, number_t r, real_t eps)
{
  cm_=cm; rank_=r; eps_=eps;
  fromLargeMatrix(lm);
  ApproximateMatrix<T>::isAllocated=true;
  factorization_=_noFactorization;
}

//! build low rank matrix from LargeMatrix
template <typename T>
void LowRankMatrix<T>::fromLargeMatrix(const LargeMatrix<T>& lm)
{
  switch(cm_)
    {
      case _svdCompression: xlifepp::svd(lm,*this);
        break;
      case _rsvdCompression: xlifepp::rsvd(lm, *this, rank_, eps_);
        break;
      case _r3svdCompression: xlifepp::r3svd(lm, *this, eps_);
        break;
      default:
        where("LowRankMatrix::fromLargeMatrix(LargeMatrix)");
        error("compression_not_handled",words("HMatrix approximation",cm_));
    }
}

//! create a clone
template<typename T>
ApproximateMatrix<T>* LowRankMatrix<T>::clone() const
{
  return new LowRankMatrix<T>(*this);
}

/*======================================================================================================
                                    extraction and conversion
  ======================================================================================================*/

//! restrict LowRankMatrix vector sets to their r first cols leading to a LowRankMatrix of rank r
template<typename T>
LowRankMatrix<T>& LowRankMatrix<T>::toFirsts(number_t r)
{
  number_t ra=U_.numberOfColumns();
  if(r>=ra) return *this; //nothing to do
  if(D_.size()>0) D_.resize(r);
  Matrix<T> cU=U_, cV=V_;
  U_=cU.subMatrix(1,cU.numberOfRows(),1,r);
  V_=cV.subMatrix(1,cV.numberOfRows(),1,r);
  return *this;
}

/*! restrict a LowRankMatrix to row indices I and col indices J
    if low rank matrix has the form
        |UI|     |VJ|'   |UI*D*VJ'  UI*D*VL'|
        |  | |D| |  |  = |                  |
        |UK|     |VL|    |UK*D*VJ'  UK*D*VL'|
    so extract (I, J) part consists in building the new low rank matrix UI*D*VJ'
*/
template<typename T>
void LowRankMatrix<T>::restrict(const std::vector<number_t>& rowIndex, const std::vector<number_t>& colIndex)
{
  LowRankMatrix<T>* am=extract(rowIndex,colIndex);
  U_=am->U_;
  D_=am->D_;
  V_=am->V_;
  delete am;
}

/*! extend LowRankMatrix to row and col parent numberings
    let I, J the row and col numbering of current LowRankMatrix (UI*D*VJ) related to parent numberings
    the new LowRankMatrix is

        |UI|     |VJ|'   |UI*D*VJ'  0 |
        |  | |D| |  |  = |            |
        |0 |     |0 |    |   0      0 |
    so extract (I, J) part consists in extending col of U and V

    rowIndex: indices of rows starting at 1
    colIndex: indices of cols starting at 1
    m: number of rows of extended matrix
    n: number of cols of extended matrix
    if m=0 it is deduced from the rowIndex vector: m = max rowIndex
    if n=0 it is deduced from the colIndex vector: n = max colIndex
*/
template<typename T>
void LowRankMatrix<T>::extend(const std::vector<number_t>& rowIndex, const std::vector<number_t>& colIndex, number_t m, number_t n)
{
  if (rowIndex.empty())
  {
    where("LowRankMatrix::extend(Numbers, Numbers, Number, Number)");
    error("is_void","rowIndex");
  }
  if (colIndex.empty())
  {
    where("LowRankMatrix::extend(Numbers, Numbers, Number, Number)");
    error("is_void","colIndex");
  }
  if (m==0) m = *std::max_element(rowIndex.begin(),rowIndex.end());
  if (n==0) n = *std::max_element(colIndex.begin(),colIndex.end());
  number_t r = U_.numberOfCols();

  if (m == 0)
  {
    where("LowRankMatrix::extend(Numbers, Numbers, Number, Number)");
    error("is_null","m");
  }
  if (n == 0)
  {
    where("LowRankMatrix::extend(Numbers, Numbers, Number, Number)");
    error("is_null","n");
  }
  if (r == 0)
  {
    where("LowRankMatrix::extend(Numbers, Numbers, Number, Number)");
    error("is_null","r");
  }
  Matrix<T> U = U_;
  U_.changesize(m,r,T()); U_*=0;
  std::vector<number_t>::const_iterator itn=rowIndex.begin();
  typename std::vector<T>::iterator itv, itu;
  for(number_t k=0; k<rowIndex.size(); k++, ++itn)
    {
      itv=U_.begin()+ (*itn-1)*r;
      itu=U.begin()+k*r;
      for(number_t j=0; j<r; j++,++itu, ++itv) *itv=*itu;
    }
  U=V_;
  V_.changesize(n,r, T()); V_*=0;
  itn=colIndex.begin();
  for(number_t k=0; k<colIndex.size(); k++, ++itn)
    {
      itv=V_.begin()+ (*itn-1)*r;
      itu=U.begin()+k*r;
      for(number_t j=0; j<r; j++,++itu, ++itv) *itv=*itu;
    }
}

/*! extract part of LowRankMatrix given by row indices I and col indices J
    if low rank matrix has the form
        |UI|     |VJ|'   |UI*D*VJ'  UI*D*VL'|
        |  | |D| |  |  = |                  |
        |UK|     |VL|    |UK*D*VJ'  UK*D*VL'|
    so extract (I, J) part consists in building the new low rank matrix UI*D*VJ'
    in other words, extract rows I of U and rows J of V
*/
template<typename T>
LowRankMatrix<T>* LowRankMatrix<T>::extract(const std::vector<number_t>& rowIndex, const std::vector<number_t>& colIndex) const
{
  number_t nbr=rowIndex.size(), nbc=colIndex.size(), r=U_.numberOfColumns();
  LowRankMatrix<T>* am = new LowRankMatrix(nbr, nbc, r);
  if(D_.size()>0) am->D_=D_;
  std::vector<number_t>::const_iterator cit;
  typename std::vector<T>::const_iterator citv;
  typename std::vector<T>::iterator itv=am->U_.begin();
  for(cit=rowIndex.begin(); cit!=rowIndex.end(); ++cit)
    {
      citv=U_.begin()+ (*cit-1)*r;
      for(number_t j=0; j<r; j++, ++citv, ++itv) *itv=*citv;
    }
  itv=am->V_.begin();
  for(cit=colIndex.begin(); cit!=colIndex.end(); ++cit)
    {
      citv=V_.begin()+ (*cit-1)*r;
      for(number_t j=0; j<r; j++, ++citv, ++itv) *itv=*citv;
    }
  return am;
}

//! convert to LargeMatrix (dense row)
template<typename T>
LargeMatrix<T> LowRankMatrix<T>::toLargeMatrix(StorageType st, AccessType at) const
{

  if(st==_dense && at==_row)
    {
      LargeMatrix<T> lm(numberOfRows(), numberOfCols(), _dense, _row);
      toDenseRow(lm.values().begin()+1);
      return lm;
    }
  //other cases, not optimized
  Matrix<T> mat=toMatrix();
  number_t nbr = numberOfRows(), nbc = numberOfCols();
  LargeMatrix<T> lm(nbr, nbc, st, at);
  lm.add(mat,trivialNumbering<number_t>(1,nbr), trivialNumbering<number_t>(1,nbc));
  return lm;
}

//! convert to Matrix (dense row)
template<typename T>
Matrix<T> LowRankMatrix<T>::toMatrix() const
{
  Matrix<T> mat(numberOfRows(), numberOfCols());
  toDenseRow(mat.begin());
  return mat;
}

//! convert to a dense row matrix passed by iterator itm pointing to the first value of dense row matrix
template<typename T>
template<typename I>
void LowRankMatrix<T>::toDenseRow(I itm) const
{
  number_t nr=numberOfRows(), nc = numberOfCols(),  r = U_.numberOfColumns(), nd =D_.size();
  typename std::vector<T>::const_iterator itu, itv, itd;
  if(nd==0) //no diagonal
    {
      for(number_t i=0; i<nr; i++)
        for(number_t j=0; j<nc; j++, ++itm)
          {
            itu=U_.begin()+i*r;
            itv=V_.begin()+j*r;
            for(number_t l=0; l<r; l++, ++itu, ++itv) *itm += *itu * conj(*itv); // U * conj(V)
          }
    }
  else
    {
      for(number_t i=0; i<nr; i++)
        for(number_t j=0; j<nc; j++, ++itm)
          {
            itu=U_.begin()+i*r;
            itv=V_.begin()+j*r;
            itd=D_.begin();
            for(number_t l=0; l<r; l++, ++itu, ++itv, ++itd) *itm += *itu * *itd * conj(*itv); // U * D * conj(V)
          }
    }
}

// get i-th row, j-th col (i,j >=1)
template<typename T>
std::vector<T> LowRankMatrix<T>::row(number_t i) const
{
  number_t m= numberOfRows(), n=numberOfCols(), r=rank();
  if (i<1 || i>m) error("index_out_of_range","LowRankMatrix::row(Number)", 1, m);
  i-=1;
  std::vector<T> res(n, T());
  typename std::vector<T>::iterator itr=res.begin();
  typename std::vector<T>::const_iterator itu, itv, itd, itui = U_.begin()+i*r;
  if(D_.size()>0)
    {
      for(number_t j=0; j<n; ++j, ++itr)
        {
          itu = itui;
          itv = V_.begin()+j*r;
          itd = D_.begin();
          for(number_t k=0; k<r; ++k, ++itu, ++itv, ++itd) *itr+= *itu * *itd * conj(*itv);
        }
    }
  else  //D_ is Id
    {
      for(number_t j=0; j<n; ++j, ++itr)
        {
          itu = itui;
          itv = V_.begin()+j*r;
          for(number_t k=0; k<r; ++k, ++itu, ++itv) *itr+= *itu * conj(*itv);
        }
    }
  return res;
}

template<typename T>
std::vector<T> LowRankMatrix<T>::col(number_t j) const
{
  number_t m= numberOfRows(), n=numberOfCols(), r=rank();
  if (j<1 || j>n) error("index_out_of_range","LowRankMatrix::col(Number)", 1, n);
  j-=1;
  std::vector<T> res(m, T());
  typename std::vector<T>::iterator itr=res.begin();
  typename std::vector<T>::const_iterator itu, itv, itd, itvj= V_.begin()+j*r;
  if(D_.size()>0)
    {
      for(number_t i=0; i<m; ++i, ++itr)
        {
          itu = U_.begin()+i*r;
          itv = itvj;
          itd = D_.begin();
          for(number_t k=0; k<r; ++k, ++itu, ++itv, ++itd) *itr+= *itu * *itd * conj(*itv);
        }
    }
  else  //D_ is Id
    {
      for(number_t i=0; i<m; ++i, ++itr)
        {
          itu = U_.begin()+i*r;
          itv = itvj;
          for(number_t k=0; k<r; ++k, ++itu, ++itv) *itr+= *itu * conj(*itv);
        }
    }
  return res;
}

// coefficient i,j of UDV* (i>=1, j>=1), expansive
template<typename T>
T LowRankMatrix<T>::operator()(number_t i, number_t j) const
{
  number_t m= numberOfRows(), n=numberOfCols(), r=rank();
  if (i<1 || i>m) error("index_out_of_range","LowRankMatrix::operator()(Number, Number)", 1, m);
  if (j<1 || j>n) error("index_out_of_range","LowRankMatrix::operator()(Number, Number)", 1, n);
  T aij = T();
  typename std::vector<T>::const_iterator itu=U_.begin()+(i-1)*r, itv=V_.begin()+(j-1)*r, itd=D_.begin();
  if(D_.size()>0)
    {
      typename std::vector<T>::const_iterator itd=D_.begin();
      for(number_t k=0; k<r; ++k, ++itu, ++itv, ++itd) aij += *itu * *itd * conj(*itv);
    }
  else
    for(number_t k=0; k<r; ++k, ++itu, ++itv) aij += *itu * conj(*itv);
  return aij;
}

/*======================================================================================================
                                          Algebraic operations
  ======================================================================================================*/
/* add a scaled LowRank matrix to current one: cL += s*L

   L: LowRankMatrix to add
   s: coefficient
   eps: recompression threshold (if eps=0 no recompression)
                                                               | cD  0  |   | cV*|
   cL = cU * cD * cV*  L = U * D * V* ==>  cL + s*L = [cU U] * |        | * |    |
                                                               | 0  s*D |   | V* |
                                                                 | cV*|
   if D and cD do not exist (means D=cD=Id), cL + s*L = [cU U] * |    |
                                                                 | sV*|

   if eps > 0 a svd recompression is done

*/
template<typename T>
LowRankMatrix<T>& LowRankMatrix<T>::add(const LowRankMatrix<T>& L, const T& s, real_t eps)
{
  number_t m=U_.numberOfRows(), n=V_.numberOfRows();
  if(!ApproximateMatrix<T>:: isAllocated) //current matrix is 0 or void
    {
      U_=L.U_;
      V_=L.V_;
      D_=L.D_;
      ApproximateMatrix<T>::isAllocated=true;
      return *this;
    }
  if (L.U_.numberOfRows()!=m || L.V_.numberOfRows()!=n)
  {
    where("LowRankMatrix::add(LowRankMatrix, T, Real");
    error("mat_mismatch_dims", L.U_.numberOfRows(), L.V_.numberOfRows(), m, n);
  }
  //concatenate U
  number_t k = U_.numberOfColumns(), l=L.U_.numberOfColumns(), kpl=k+l;
  Matrix<T> cU(m,kpl);
  typename Matrix<T>::it_vk it = U_.begin(), itC=cU.begin();
  typename Matrix<T>::cit_vk itL =L.U_.begin();
  for(number_t i=0; i<m; ++i)
    {
      for(number_t j=0; j<k; ++j,++it, ++itC) *itC=*it;
      for(number_t j=k; j<kpl; ++j,++itL, ++itC) *itC=*itL;
    }
  U_=cU;
  //concatenate D
  bool hasD = false;
  if(hasDiag())
    {
      D_.resize(kpl);
      typename Vector<T>::it_vk itv=D_.begin()+k;
      if(L.hasDiag())
        {
          typename Vector<T>::cit_vk itw=L.D_.begin();
          for(number_t j=k; j<kpl; ++j,++itv, ++itw) *itv=s* *itw;
        }
      else
        for(number_t j=k; j<kpl; ++j,++itv) *itv = s;
      hasD = true;
    }
  else if(L.hasDiag())
    {
      D_.resize(kpl);
      typename Vector<T>::it_vk itv=D_.begin();
      for(number_t j=0; j<k; ++j,++itv) *itv = 1.;
      typename Vector<T>::cit_vk itw=L.D_.begin();
      for(number_t j=k; j<kpl; ++j,++itv, ++itw) *itv=s* *itw;
      hasD = true;
    }
  //concatenate V
  k = V_.numberOfColumns(); l=L.V_.numberOfColumns(); kpl=k+l;
  Matrix<T> cV(n,kpl);
  it = V_.begin(), itC=cV.begin();
  itL =L.V_.begin();
  for(number_t i=0; i<n; ++i)
    {
      for(number_t j=0; j<k; ++j,++it, ++itC) *itC=*it;
      if(hasD)
        for(number_t j=k; j<kpl; ++j,++itL, ++itC) *itC=*itL;
      else
        for(number_t j=k; j<kpl; ++j,++itL, ++itC) *itC=s* *itL;
    }
  V_=cV;

  if(eps>0) //svd compression if required
    {
      LowRankMatrix<T> lr=svd(eps);
      U_=lr.U_;
      V_=lr.V_;
      D_=lr.D_;
    }
  return *this;
}

// add/substract a LowRank matrix to current one
template<typename T>
LowRankMatrix<T>& LowRankMatrix<T>::operator+=(const LowRankMatrix<T>& L)
{
  return add(L, T(1));
}
template<typename T>
LowRankMatrix<T>& LowRankMatrix<T>::operator-=(const LowRankMatrix<T>& L)
{
  return add(L, -T(1));
}

//multiply/divide current LowRank matrix
template<typename T>
LowRankMatrix<T>& LowRankMatrix<T>::operator*=(const T& s)
{
  if(hasDiag()) D_*=s;
  else V_*=s;
  return *this;

}
template<typename T>
LowRankMatrix<T>& LowRankMatrix<T>::operator/=(const T& s)
{
  if(hasDiag()) D_/=s;
  else V_/=s;
  return *this;
}

//combine two low rank matrices s1*L1+s2*L2
template<typename T>
LowRankMatrix<T> combine(const LowRankMatrix<T>&L1, const T&s1,
                         const LowRankMatrix<T>&L2, const T&s2, real_t eps =0)
{
  LowRankMatrix<T> L=L1;
  if(s1!=T(1)) L1*=s1;
  if(s2!=T(0)) L1.add(L2,s2,eps);
  return L;
}

template<typename T>
LowRankMatrix<T> operator+(const LowRankMatrix<T>&L1, const LowRankMatrix<T>&L2)
{
  LowRankMatrix<T> L=L1;
  return L+=L2;
}

template<typename T>
LowRankMatrix<T> operator-(const LowRankMatrix<T>&L1, const LowRankMatrix<T>&L2)
{
  LowRankMatrix<T> L=L1;
  return L-=L2;
}

template<typename T>
LowRankMatrix<T> operator-(const LowRankMatrix<T>&L1)
{
  LowRankMatrix<T> L=L1;
  return L*=-1.;
}

template<typename T>
LowRankMatrix<T> operator+(const LowRankMatrix<T>&L1)
{
  LowRankMatrix<T> L=L1;
  return L;
}

template<typename T>
LowRankMatrix<T> operator*(const LowRankMatrix<T>&L1, const T& s)
{
  LowRankMatrix<T> L=L1;
  return L*=s;
}

template<typename T>
LowRankMatrix<T> operator/(const LowRankMatrix<T>&L1, const T& s)
{
  LowRankMatrix<T> L=L1;
  return L/=s;
}

template<typename T>
LowRankMatrix<T> operator*(const T& s, const LowRankMatrix<T>&L1)
{
  LowRankMatrix<T> L=L1;
  return L*=s;
}

template<typename T>
std::vector<T> operator*(const LowRankMatrix<T>& lrm, const std::vector<T>& x)
{
  std::vector<T> Ax;
  lrm.multMatrixVector(x, Ax);
  return Ax;
}

template<typename T>
std::vector<T> operator*(const std::vector<T>& x, const LowRankMatrix<T>& lrm)
{
  std::vector<T> xA;
  lrm.multVectorMatrix(x, xA);
  return xA;
}

// squared Frobenius norm
template<typename T>
real_t LowRankMatrix<T>::squaredNorm() const
{
  number_t m=U_.numberOfRows(), n=V_.numberOfRows(), r=U_.numberOfColumns();
  real_t no=0.;
  if(D_.size()>0)
    {
      for(number_t i=1; i<=m; i++)
        for(number_t j=1; j<=n; j++)
          {
            T aij=0.;
            for(number_t k=1; k<=r; k++) aij+=U_(i,k)*D_[k-1]*V_(j,k);
            no+= abs(aij) * abs(aij);
          }
    }
  else
    {
      for(number_t i=1; i<=m; i++)
        for(number_t j=1; j<=n; j++)
          {
            T aij=0.;
            for(number_t k=1; k<=r; k++) aij+=U_(i,k)*V_(j,k);
            no+= abs(aij) * abs(aij);
          }
    }
  return no;
}

//infinite norm
template<typename T>
real_t LowRankMatrix<T>::norminfty() const
{
  number_t m=U_.numberOfRows(), n=V_.numberOfRows(), r=U_.numberOfColumns();
  real_t no=0.;
  if(D_.size()>0)
    {
      for(number_t i=1; i<=m; i++)
        for(number_t j=1; j<=n; j++)
          {
            T aij=0.;
            for(number_t k=1; k<=r; k++) aij+=U_(i,k)*D_[k-1]*V_(j,k);
            no = std::max(no,abs(aij));
          }
    }
  else
    {
      for(number_t i=1; i<=m; i++)
        for(number_t j=1; j<=n; j++)
          {
            T aij=0.;
            for(number_t k=1; k<=r; k++) aij+=U_(i,k)*V_(j,k);
            no = std::max(no,abs(aij));
          }
    }
  return no;
}

/*======================================================================================================
                                          print stuff
  ======================================================================================================*/
//print a low rank matrix
template<typename T>
void LowRankMatrix<T>::print(std::ostream& out) const
{
  out<<"Low rank matrix (UDV*) size "<<numberOfRows()<<" x "<<numberOfCols()
     <<", rank: "<<rank()  <<", number of non zero: "<<nbNonZero()<<eol;
  if(theVerboseLevel<5) return;
  out<<"U = "<<U_<<eol<<"V = "<<V_<<eol;
  if(D_.size()>0) out<<" D = "<<D_<<std::endl;
}

/*======================================================================================================
                                          Matrix/vector products
  ======================================================================================================*/
//product matrix x vector
template<typename T>
std::vector<T>& LowRankMatrix<T>::multMatrixVector(const std::vector<T>& X, std::vector<T>& Y) const
{
  if (X.size()!=numberOfCols())
  {
    where("LowRankMatrix::multMatrixVector(...)");
    error("bad_dim", X.size(), numberOfCols());
  }

  number_t r = V_.numberOfColumns();
  std::vector<T> Z(r);
  T zero = 0*X[0];
  //vecmat(V_.begin(), X.begin(), X.end(), Z.begin(), Z.end());  //Z = X * V_
  typename std::vector<T>::const_iterator itx;
  typename std::vector<T>::iterator itz;
  typename std::vector<T>::const_iterator itm = V_.begin();
  number_t l=0;
  for (itz = Z.begin(); itz != Z.end(); ++itz, ++l)             //Z = X * V*
  {
    T t = zero;
    itm =  V_.begin() + l;
    for(itx = X.begin(); itx != X.end(); ++itx, itm+=r) t += *itx * conj(*itm);
    *itz = t;
  }
  if (D_.size()>0)
  {
    itm=D_.begin();
    itz=Z.begin();
    for(; itm!=D_.end(); ++itm, ++itz) *itz *= *itm;         //Z = D_ * Z
  }
  Y.resize(U_.numberOfRows());
  matvec(U_.begin(), Z.begin(), Z.end(), Y.begin(), Y.end());  //Y = U_ * Z;
  return Y;
}

//product vector x matrix
template<typename T>
std::vector<T>& LowRankMatrix<T>::multVectorMatrix(const std::vector<T>& X, std::vector<T>& Y) const
{
  if (X.size()!=numberOfRows())
  {
    where("LowRankMatrix::multVectorMatrix(...)");
    error("bad_dim", X.size(), numberOfRows());
  }

  std::vector<T> Z(U_.numberOfColumns());
  vecmat(U_.begin(), X.begin(), X.end(), Z.begin(), Z.end());       //Z = X * U_
  if (D_.size()>0)
  {
    typename Vector<T>::const_iterator itd=D_.begin();
    typename std::vector<T>::iterator itz=Z.begin();
    for(; itd!=D_.end(); ++itd, ++itz) *itz *= *itd;             //Z = D_ * Z
  }
  Y.resize(V_.numberOfRows());
  //matvec(V_.begin(), Z.begin(), Z.end(), Y.begin(), Y.end());    //Y = V_* * Z;
  T zero = 0*X[0];
  typename std::vector<T>::iterator itz, ity;
  typename std::vector<T>::const_iterator itm = V_.begin();
  for (ity = Y.begin(); ity != Y.end(); ++ity)
  {
    T t = zero;
    for(itz = Z.begin(); itz != Z.end(); ++itz, ++itm) t +=  conj(*itm) * *itz;
    *ity = t;
  }
  return Y;
}
/*======================================================================================================
                                          Matrix/Matrix products
  ======================================================================================================*/
/*product LowRankMatrix x LowRankMatrix
    let A = UA*DA*VA' with UA a (ma,ra) matrix, DA = (ra,ra) diagonal matrix, may be Id, VA a (na,ra) matrix
        B = UB*DB*VB' with UB a (mb,rb) matrix, DB = (rb,rb) diagonal matrix, may be Id, VB a (nb,rb) matrix
        choose form that requires save memory comparing nb*ra and ma*rb
        if nb*ra < ma*rb: (UA*DA*VA') * (UB*DB*VB') -> UA*DA*(VA'*UB*DB*VB')   => VAB = VB*DB*UB'*VA a (nb,ra) matrix
        if ma*rb <= nb*ra: (UA*DA*VA') * (UB*DB*VB') ->(UA*DA*VA'*UB) *DB*VB' => UAB = UA*DA*VA'*UB a (ma,rb) matrix
    no compression
*/
template<typename T>
LowRankMatrix<T>& multMatrix(LowRankMatrix<T>& A,LowRankMatrix<T>& B, LowRankMatrix<T>& AB)    //! product of two LowRankMatrix's
{
  if (A.numberOfCols()!=B.numberOfRows())
  {
    where("multMatrix(LowRankMatrix,LowRankMatrix)");
    error("bad_dim",A.numberOfCols(),B.numberOfRows());
  }
  number_t ma = A.numberOfRows(), nb=B.numberOfCols(), ra = A.U_.numberOfCols(), rb = B.u_.numberOfCols();
  if (nb*ra < ma*rb)
  {
    AB.U_=A.U_;
    AB.D_=A.D_;
    //do the product VB*DB*UB'*VA
    Matrix<T> M = transpose(B.U_)*A.V_;
    if(B.D_.size()>0) M.diagProduct(B.D_);
    AB.U_= B.V_*M;
  }
  else // ma*rb <= nb*ra
  {
    AB.V_=B.V_;
    AB.D_=B.D_;
    //do the product VB*DB*UB'*VA
    Matrix<T> M = transpose(A.V_)*B.U_;
    if(A.D_.size()>0) M.productDiag(A.D_);
    AB.U_= B.V_*M;
  }
  return AB;
}

/*product LowRankMatrix x LargeMatrix
    let A = UA*DA*VA' with UA a (ma,ra) matrix, DA = (ra,ra) diagonal matrix, may be Id, VA a (na,ra) matrix
    and L a (na, p) LargeMatrix
    A*L = UA*DA*VA'*L = UA*DA*(L'*VA)' with L'*VA a (p,ra) matrix
    no compression
*/
template<typename T>
LowRankMatrix<T>& multMatrix(LowRankMatrix<T>& A,LargeMatrix<T>& L, LowRankMatrix<T>& AL)    //! product of a LowRankMatrix and a LargeMatrix
{
  if (A.numberOfCols()!=L.numberOfRows())
  {
    where("multMatrix(LowRankMatrix,LargeMatrix)");
    error("bad_dim",A.numberOfCols(),L.numberOfRows());
  }
  number_t p=L.numberOfCols(), na=A.numberOfRows(), ra = A.U_.numberOfCols();
  AL.U_=A.U_;
  AL.D_=A.D_;
  AL.V_.changeSize(p,ra,T(0));
  typename std::vector<T> v(na), r(p);
  typename std::vector<T>::iterator itv, ita;
  for (number_t k=0; k<ra; ++ra)
  {
    itv=v.begin();
    ita=A.V_.begin()+k;
    for(number_t i=0; i<na; ++i,++itv,ita+=ra) *itv= *ita;
    multVectorMatrix(L, v, r);
    itv=r.begin();
    ita=AL.V_.begin()+k;
    for(number_t i=0; i<p; ++i,++itv,ita+=ra) *ita= *itv;
  }
  return AL;
}

/*product LowRankMatrix x LargeMatrix
    let A = UA*DA*VA' with UA a (ma,ra) matrix, DA a (ra,ra) diagonal matrix, may be Id, VA a (na,ra) matrix
    and L a (p,ma) LargeMatrix
    L*A = L*UA*DA*VA = (L*UA)*DA*VA' with L*UA a (p,ra) matrix
    no compression
*/
template<typename T>
LowRankMatrix<T>& multMatrix(LargeMatrix<T>& L, LowRankMatrix<T>& A, LowRankMatrix<T>& AL)    //! product of a LargeMatrix and a LowRankMatrix
{
  if (A.numberOfRows()!=L.numberOfCols())
  {
    where("multMatrix(LowRankMatrix,LargeMatrix)");
    error("bad_dim",L.numberOfCols(),A.numberOfRows());
  }
  number_t ma = A.numberOfRows(), p=L.numberOfRows(), na=A.numberOfRows(), ra = A.U_.numberOfCols();
  AL.V_=A.V_;
  AL.D_=A.D_;
  AL.U_.changeSize(p,ra,T(0));
  typename std::vector<T> v(ma), r(p);
  typename std::vector<T>::iterator itv, ita;
  for (number_t k=0; k<ra; ++ra)
  {
    itv=v.begin();
    ita=A.V_.begin()+k;
    for(number_t i=0; i<na; ++i,++itv,ita+=ra) *itv= *ita;
    multMatrixVector(L, v, r);
    itv=r.begin();
    ita=AL.U_.begin()+k;
    for(number_t i=0; i<p; ++i,++itv,ita+=ra) *ita= *itv;
  }
  return AL;
}

/*======================================================================================================
                    special matrix product used by svd compression methods
  ======================================================================================================*/
/*! do the product R = L * M where
       L is the current LowRankMatrix,
         L=U*D*Vt, U (m,r) matrix, V (n,r) matrix stored as dense row, D (r,r) diagonal matrix stored as vector
       M a dense col matrix given by its pointer to first value
       R a dense col matrix given by its pointer to first value
       p the number of cols of M
*/
template<typename T>
void LowRankMatrix<T>::multMatrixCol(T* M, T* R, number_t p) const
{
  number_t m=numberOfRows(), n=numberOfCols(), r= U_.numberOfCols();
  //do VtM = Vt*M, matrix (r,p) stored as dense col
  std::vector<T> VtM(r*p,T());
  typename std::vector<T>::iterator itvtm=VtM.begin();
  typename std::vector<T>::const_iterator itv;
  T* pM=nullptr;
  for(number_t j=0; j<p; ++j)
    {
      for(number_t i=0; i<r; ++i, ++itvtm)
        {
          T t=T();
          pM=M+j*n;
          itv=V_.begin()+i;
          for(number_t k=0; k<n; ++k, itv+=r, ++pM) t+=*itv * *pM;
          *itvtm=t;
        }
    }
  //product by diagonal if L has diagonal
  if(D_.size()>0)
    {
      itvtm=VtM.begin();
      for(number_t j=0; j<p; ++j)
        for(itv=D_.begin(); itv!=D_.end(); ++itv, ++itvtm) *itvtm *= *itv;
    }
  //do R= U*VtM = U*D*Vt*M, matrix (m,p) stored as dense col
  T* pR=R;
  for(number_t j=0; j<p; ++j)
    {
      for(number_t i=0; i<m; ++i, ++pR)
        {
          T t=T();
          itvtm=VtM.begin()+j*r;
          itv=U_.begin()+i*r;
          for(number_t k=0; k<r; ++k, ++itv, ++itvtm) t+=*itv * *itvtm;
          *pR=t;
        }
    }
}

/*! do the product R = M * L where
         L is the current LowRankMatrix,
           L=U*D*Vt, U (m,r) matrix, V (n,r) matrix stored as dense row, D (r,r) diagonal matrix stored as vector
         M a dense col matrix given by its pointer to first value
         R a dense col matrix given by its pointer to first value
         p the number of rows of M
*/
template<typename T>
void LowRankMatrix<T>::multLeftMatrixCol(T* M, T* R, number_t p) const
{
  number_t m=numberOfRows(), n=numberOfCols(), r= U_.numberOfCols();
  //do MU = M*U, matrix (p,r) stored as dense col
  std::vector<T> MU(r*p,T());
  typename std::vector<T>::iterator itmu=MU.begin();
  typename std::vector<T>::const_iterator itv;
  T* pM=nullptr;
  for(number_t j=0; j<r; ++j)
    {
      for(number_t i=0; i<p; ++i, ++itmu)
        {
          T t=T();
          pM=M+i;
          itv=U_.begin()+j;
          for(number_t k=0; k<m; ++k, itv+=r, pM+=p) t+=*itv * *pM;
          *itmu=t;
        }
    }
  //product by diagonal if L has diagonal
  if(D_.size()>0)
    {
      itmu=MU.begin();
      itv=D_.begin();
      for(number_t j=0; j<r; ++j, ++itv)
        for(number_t i=0; i<p; ++i, ++itmu) *itmu *= *itv;
    }
  //do R= MU*Vt = M*U*D*Vt, matrix (p,n) stored as dense col
  T* pR=R;
  for(number_t j=0; j<n; ++j)
    {
      for(number_t i=0; i<p; ++i, ++pR)
        {
          T t=T();
          itmu=MU.begin()+i;
          itv=V_.begin()+j*r;
          for(number_t k=0; k<r; ++k, ++itv, itmu+=p) t+=*itmu * *itv;
          *pR=t;
        }
    }
}

/*! do the product R = L * M where
       L is the current LowRankMatrix,
         L=U*D*Vt, U (m,r) matrix, V (n,r) matrix stored as dense row, D (r,r) diagonal matrix stored as vector
       M a dense row matrix given by its pointer to first value
       R a dense row matrix given by its pointer to first value
       p the number of cols of M
*/
template<typename T>
void LowRankMatrix<T>::multMatrixRow(T* M, T* R, number_t p) const
{
  number_t m=numberOfRows(), n=numberOfCols(), r= U_.numberOfCols();
  //do VtM = Vt*M, matrix (r,p) stored as dense col
  std::vector<T> VtM(r*p,T());
  typename std::vector<T>::iterator itvtm=VtM.begin();
  typename std::vector<T>::const_iterator itv;
  T* pM=nullptr;
  for(number_t j=0; j<p; ++j)
    {
      for(number_t i=0; i<r; ++i, ++itvtm)
        {
          T t=T();
          itv=V_.begin()+i;
          pM=M+j;
          for(number_t k=0; k<n; ++k, itv+=r, pM+=p) t+=*itv * *pM;
          *itvtm=t;
        }
    }
  //product by diagonal if L has diagonal
  if(D_.size()>0)
    {
      itvtm=VtM.begin();
      for(number_t j=0; j<p; ++j)
        for(itv=D_.begin(); itv!=D_.end(); ++itv, ++itvtm) *itvtm *= *itv;
    }
  //do R= U*VtM = U*D*Vt*M, matrix (m,p) stored as dense row
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<p; ++j, ++R)
      {
        T t=T();
        itvtm=VtM.begin()+j*r;
        itv=U_.begin()+i*r;
        for(number_t k=0; k<r; ++k, ++itv, ++itvtm) t+=*itv * *itvtm;
        *R=t;
      }
}

/*! do the product R = M * L where
         L is the current LowRankMatrix,
           L=U*D*Vt, U (m,r) matrix, V (n,r) matrix stored as dense row, D (r,r) diagonal matrix stored as vector
         M a dense col matrix given by its pointer to first value
         R a dense col matrix given by its pointer to first value
         p the number of rows of M
*/
template<typename T>
void LowRankMatrix<T>::multLeftMatrixRow(T* M, T* R, number_t p) const
{
  number_t m=numberOfRows(), n=numberOfCols(), r= U_.numberOfCols();
  //do MU = M*U, matrix (p,r) stored as dense row
  std::vector<T> MU(r*p,T());
  typename std::vector<T>::iterator itmu=MU.begin();
  typename std::vector<T>::const_iterator itv;
  T* pM=nullptr;
  for(number_t i=0; i<p; ++i)
    for(number_t j=0; j<r; ++j, ++itmu)
      {
        T t=T();
        pM=M+i*m;
        itv=U_.begin()+j;
        for(number_t k=0; k<m; ++k, itv+=r, pM++) t+=*itv * *pM;
        *itmu=t;
      }
  //product by diagonal if L has diagonal
  if(D_.size()>0)
    {
      itmu=MU.begin();
      for(number_t i=0; i<p; ++i)
        {
          itv=D_.begin();
          for(number_t j=0; j<r; ++j, ++itmu, ++itv) *itmu *= *itv;
        }
    }
  //do R= MU*Vt = M*U*D*Vt, matrix (p,n) stored as dense row
  for(number_t i=0; i<p; ++i)
    for(number_t j=0; j<n; ++j, ++R)
      {
        T t=T();
        itmu=MU.begin()+i*r;
        itv=V_.begin()+j*r;
        for(number_t k=0; k<r; ++k, ++itv, itmu++) t+=*itmu * *itv;
        *R=t;
      }
}

/*======================================================================================================
                                LU Factorization of LowRankMatrix
  ======================================================================================================*/
/* LU factorize U = LU*UU, LU factorize V = LV*UV
     gives the low rank representation U*D*V= LU*UU*D*UVt*LVt
     so to solve UDV*X=B we do
        LU*X = B   UU*Y = X   D*X  = Y  UVt*Y = X and finally LVt*X = Y
*/
template<typename T>
void LowRankMatrix<T>::luFactorize(bool withPermutation)
{
  if (withPermutation) error("not_handled","LowRankMatrix::luFactorize(bool) bool=1");
  lu(U_); lu(V_);
  factorization_=_lu;
}

/*======================================================================================================
                                compression of LowRankMatrix
  ======================================================================================================*/
/*! compression of LowRankMatrix
    cm: compression method one of _svdCompression, _rsvdCompression, _r3svdCompression, _acaFull, _acaPartial, _acaPlus
    r: prescribed rank
    eps: prescribed precision
    if r>0  use prescribed rank else use prescribed precision
*/
template<typename T>
LowRankMatrix<T>& LowRankMatrix<T>::compress(HMApproximationMethod cm, number_t r, real_t eps)
{
  switch(cm)
    {
      case _svdCompression: svd(r,eps); break;
      case _rsvdCompression: rsvd(r,eps); break;
      case _r3svdCompression: r3svd(eps); break;
      default:
        where("LowRankMatrix::compress(...)");
        error("compression_not_handled",words("HMatrix approximation",cm));
    }
  return *this;
}

/*! specific svd for low rank matrix
    returns the svd  L=USV* as a low rank matrix
    performing a truncation of singular values below eps,
    r: prescribed rank
    eps: prescribed precision
    if r>0  use prescribed rank else use prescribed precision
    ## convert to Matrix and use svd of Matrix class
*/
template <typename T>
LowRankMatrix<T>& LowRankMatrix<T>::svd(number_t r, real_t eps)
{
  Matrix<T> A=toMatrix();
  if(r==0) xlifepp::svd(A,U_,D_,V_,r, eps); //use svd of Matrix class
  else xlifepp::svd(A,U_,D_,V_,r, 0.);
  if(r>0) toFirsts(r);
  return *this;
}

/*! random svd for low rank matrix
    returns the approximate svd  USV* as a low rank matrix
    performing a truncation of singular values at a given rank or below eps
    r: prescribed rank
    eps: prescribed precision
    if r>0  use prescribed rank else use prescribed precision
*/
template <typename T>
LowRankMatrix<T>& LowRankMatrix<T>::rsvd(number_t r, real_t eps)
{
  if(r>0) // prescribed rank
    {
      if(r>rank()) return *this; //current rank is better, nothing to do
      xlifepp::rsvd(*this, r, U_, D_, V_);
    }
  else xlifepp::rsvd(*this, eps, r, U_, D_, V_); // prescibed precision
  return *this;
}

/*! faster random svd for low rank matrix
    returns the approximate svd  USV* as a low rank matrix
    performing a truncation of singular values using an energy criteria
    eps: prescribed precision
    t: number of sampling
    p: number of over sampling
    q: power number (q=0 gives the standard r3svd)
    maxit: maximum of iterations
*/
template <typename T>
LowRankMatrix<T>& LowRankMatrix<T>::r3svd(real_t eps, number_t t, number_t p, number_t q, number_t maxit)
{
  number_t r;
  xlifepp::r3svd(*this, eps, r, U_, D_, V_);
  return *this;
}

/*=====================================================================================================================
   compression methods producing a LowRankMatrix
     svd: full SVD with truncature either to a given rank r or to a given tolerance (lambda_k > tol)
     rsvd: random SVD with truncature either to a given rank r or to a given tolerance (lambda_k > tol)
     r3svd: improved random SVD with truncature to a given tolerance (lambda_k > tol)
  =====================================================================================================================*/
/*! SVD compression method of a LargeMatrix to a LowRankMatrix Lr
    produce the best matrix approximation Lr of rank r (see Eckart-Young-Mirsky theorem)

    T: type of the result
    lm: LargeMatrix to be compressed
    r: prescribed rank of truncature, if 0 not used, the real rank at output
    eps: prescribed precision, if rk > 0 not used
          r=0 and eps=0 gives the full svd
    lrm: LowRankMatrix
*/

template<typename T>
LowRankMatrix<T>& svd(const LargeMatrix<T>& lm, LowRankMatrix<T>& lrm, number_t r, real_t eps)
{
  number_t m=lm.numberOfRows(), n=lm.numberOfCols(), p=std::min(m,n);
  if(lm.storageType()==_dense && lm.accessType()==_row)
    {
      lrm.U_.changesize(m,p,T()); lrm.V_.changesize(n,p,T()); lrm.D_.resize(p);
      r=std::min(r,p);
      svd(const_cast<T*>(&lm.values()[1]), m, n, &lrm.U_[0],&lrm.D_[0], &lrm.V_[0], r, eps);
      if(r<p)  //resize result
        {
          shrink(lrm.U_,m*r); shrink(lrm.V_,n*r); shrink(lrm.D_,r);
          lrm.U_.rows()=m; lrm.V_.rows()=n;
        }
    }
  else
  {
    where("LowRankMatrix svd(LargeMatrix, LowRankMatrix, Number, Real)");
    error("storage_not_handled", words("storage type", lm.storageType()), words("access type", lm.accessType()));
  }
  return lrm;
}

/*! RSVD compression method of a LargeMatrix to a LowRankMatrix Lr
    T: type of the result
    lm: LargeMatrix to be compressed
    r: prescribed rank of truncature, if 0 not used, the real rank at output
    eps: prescribed precision, if rk > 0 not used
          r=0 and eps=0 gives the full svd
    lrm: LowRankMatrix
*/
template<typename T>
LowRankMatrix<T>& rsvd(const LargeMatrix<T>& lm, LowRankMatrix<T>& lrm, number_t r, real_t eps)
{
  if(r>0) rsvd(lm, r, lrm.U_, lrm.D_, lrm.V_);
  else rsvd(lm, eps, r, lrm.U_, lrm.D_, lrm.V_);
  lrm.U_.rows()=lm.numberOfRows();  // update the number of rows of U, V
  lrm.V_.rows()=lm.numberOfCols();  // because rsvd resize only the vector storing the matrix
  return lrm;
}

/*! R3SVD compression method of a LargeMatrix to a LowRankMatrix Lr
    T: type of the result
    lm: LargeMatrix to be compressed
    eps: energy threshold
    t: number of sampling
    p: number of over sampling
    q: power number (q=0 gives the standard r3svd)
    maxit: maximum of iterations
    if t = 0, the parameters t, p, q, maxit are determined by function regarding the matrix dimensions
    lrm: output LowRankMatrix
*/
template<typename T>
LowRankMatrix<T>& r3svd(const LargeMatrix<T>& lm, LowRankMatrix<T>& lrm, real_t eps,
                        number_t t, number_t p, number_t q, number_t maxit)
{
  number_t r;
  r3svd(lm, eps, r, lrm.U_, lrm.D_, lrm.V_, t, p, q, maxit);
  lrm.U_.rows()=lm.numberOfRows();  // update the number of rows of the matrix U, V
  lrm.V_.rows()=lm.numberOfCols();  // because r3svd resize only the vector storing the matrix
  return lrm;
}

/*
   \class SCDMatrix<T>
   class dealing with matrix represented by sinus cardinal decomposition (see ...)
*/
//template <class T>
//class SCDMatrix: public ApproximateMatrix<T>
//{
//  public:
//     SCDMatrixMatrix()
//      : ApproximateMatrix<T>::approximationType(_scdApproximation) {}
//     virtual std::vector<T>& multMatrixVector(const std::vector<T>&, std::vector<T>&) const; //!< virtual product matrix * vector
//};

/*
   \class FMMMatrix<T>
   class dealing with matrix represented by FMM (see ...)
*/
//template <class T>
//class FMMMatrix: public ApproximateMatrix<T>
//{
//  public:
//     FMMMatrixMatrix()
//      : ApproximateMatrix<T>::approximationType(_fmmApproximation) {}
//     virtual std::vector<T>& multMatrixVector(const std::vector<T>&, std::vector<T>&) const; //!< virtual product matrix * vector
//};

} // end of namespace xlifepp

#endif /* APPROXIMATE_MATRIX_HPP */
