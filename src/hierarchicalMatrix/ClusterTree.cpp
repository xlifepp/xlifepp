/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ClusterTree.cpp
  \author E. Lunéville
  \since 17 may 2016
  \date  17 may 2016

  \brief Implementation of xlifepp::ClusterTree and xlifepp::ClusterNode classes and related stuff
*/

#include "ClusterTree.hpp"
#include "space.h"

namespace xlifepp
{

std::ostream& operator<<(std::ostream& out, const ClusteringMethod& cm)
{ out << words("cluster method", cm); return out; }

//special functions required by ClusterTree<Point>
dimen_t dim(const Point& p)
{return p.dim();}

real_t coords(const Point& p, dimen_t i)
{return p(i);}

BoundingBox boundingBox(const Point& p)
{
    if(p.dim()==1) return BoundingBox(p(1),p(1));
    if(p.dim()==2) return BoundingBox(p(1),p(1),p(2),p(2));
    return  BoundingBox(p(1),p(1),p(2),p(2),p(3),p(3));
}

//special functions required by ClusterTree<FeDof>
dimen_t dim(const FeDof& fed)
{return fed.dim();}

real_t coords(const FeDof& fed, dimen_t i)
{return fed.coords(i);}

BoundingBox boundingBox(const FeDof& fed)  // bounding box of a FeDof
{
    if(fed.coords().size()==0) fed.setCoords();
    Point& p=fed.coords();
    dimen_t d=p.size();
    if(d==1) return BoundingBox(p(1),p(1));
    if(d==2) return BoundingBox(p(1),p(1),p(2),p(2));
    return  BoundingBox(p(1),p(1),p(2),p(2),p(3),p(3));
}

//special functions required by ClusterTree<Element>
dimen_t dim(const Element& elt)
{return elt.dim();}

real_t coords(const Element& elt, dimen_t i)
{
    if(elt.geomElt_p->meshElement()==nullptr) elt.geomElt_p->buildSideMeshElement();
    return elt.geomElt_p->meshElement()->centroid(i);
}

BoundingBox boundingBox(const Element& elt)  // bounding box of a Element = bounding box of nodes
{
  if(elt.geomElt_p->meshElement()==nullptr) elt.geomElt_p->buildSideMeshElement();
  MeshElement* melt= elt.geomElt_p->meshElement();
  dimen_t d=melt->spaceDim();
  std::vector<real_t> xmin(d,theRealMax), xmax(d,-(theRealMax/100.));
  std::vector<Point*>::const_iterator itn=melt->nodes.begin();
  for(;itn!=melt->nodes.end();++itn)
  {
      for(dimen_t i=0;i<d;++i)
      {
          real_t x=(**itn)(i+1);
          xmin[i]=std::min(xmin[i],x);
          xmax[i]=std::max(xmax[i],x);
      }
  }
  if(d==1) return BoundingBox(xmin[0],xmax[0]);
  if(d==2) return BoundingBox(xmin[0],xmax[0],xmin[1],xmax[1]);
  return BoundingBox(xmin[0],xmax[0],xmin[1],xmax[1], xmin[2],xmax[2]);
}

template <>
void ClusterTree<Element>::setOverlap()
{
    withOverlap_=true;
}

// list of dof numbers related to a node of type ClusterNode<Point>
template<> std::vector<number_t> ClusterNode<Point>::getDofNumbers(bool store) const
{
   if(numbers_.size()>0) return numbers_;
   std::list<number_t> nums = getNumbers(store);
   if(store) {numbers_.assign(nums.begin(),nums.end()); return numbers_;}
   return std::vector<number_t>(nums.begin(),nums.end());
}

// list of dof numbers related to a node of type ClusterNode<FeDof>
template<> std::vector<number_t> ClusterNode<FeDof>::getDofNumbers(bool store) const
{
   if(numbers_.size()>0) return numbers_;
   std::list<number_t> nums = getNumbers(store);
   if(store) {numbers_.assign(nums.begin(),nums.end()); return numbers_;}
   return std::vector<number_t>(nums.begin(),nums.end());
}

// list of dof numbers related to a node of type ClusterNode<Element>
template<> std::vector<number_t> ClusterNode<Element>::getDofNumbers(bool store) const
{
   if(dofNumbers_.size()>0) return dofNumbers_;
   std::set<number_t> dofNums;
   if(numbers_.size()==0)
   {
       std::list<number_t> nums = getNumbers(store);
       std::list<number_t>::iterator itn = nums.begin();
       for(;itn!=nums.end(); ++itn)
       {
           std::vector<number_t>& dofs = (*objects_)[*itn].dofNumbers;
           dofNums.insert(dofs.begin(), dofs.end());
       }
   }
   else
   {
       std::vector<number_t>::const_iterator itn = numbers_.begin();
       for(;itn!=numbers_.end(); ++itn)
       {
           std::vector<number_t>& dofs = (*objects_)[*itn].dofNumbers;
           dofNums.insert(dofs.begin(), dofs.end());
       }
   }
   if(store) {dofNumbers_.assign(dofNums.begin(), dofNums.end()); return dofNumbers_;}
   return std::vector<number_t>(dofNums.begin(),dofNums.end());
}

// list of element numbers related to a node of type ClusterNode<Element>
template<> std::vector<Element*> ClusterNode<Element>::getElements(bool store) const
{
   if(elements_.size()>0) return elements_;
   std::list<Element*> elements;
   if(numbers_.size()==0)  //update numbers
   {
       std::list<number_t> nums = getNumbers(store);
       std::list<number_t>::iterator itn = nums.begin();
       for(;itn!=nums.end(); ++itn) elements.push_back(&(*objects_)[*itn]);
   }
   else
   {
       std::vector<number_t>::iterator itn = numbers_.begin();
       for(;itn!=numbers_.end(); ++itn) elements.push_back(&(*objects_)[*itn]);
   }
   if(store) {elements_.assign(elements.begin(), elements.end()); return elements_;}
   return std::vector<Element*>(elements.begin(), elements.end());
}

// list of element numbers related to a node of type ClusterNode<FeDof>
template<> std::vector<Element*> ClusterNode<FeDof>::getElements(bool store) const
{
   if(elements_.size()>0) return elements_;
   std::set<Element*> elements;
   if(numbers_.size()==0)
   {
       std::list<number_t> nums = getNumbers(store);
       std::list<number_t>::iterator itn = nums.begin();
       for(;itn!=nums.end(); ++itn)
       {
           const std::vector<std::pair<Element*, number_t> >& elts = (*objects_)[*itn].elements();
           std::vector<std::pair<Element*, number_t> >::const_iterator ite=elts.begin();
           for(;ite!=elts.end(); ++ite) elements.insert(ite->first);
       }
   }
   else
   {
       std::vector<number_t>::const_iterator itn = numbers_.begin();
       for(;itn!=numbers_.end(); ++itn)
       {
           const std::vector<std::pair<Element*, number_t> >& elts = (*objects_)[*itn].elements();
           std::vector<std::pair<Element*, number_t> >::const_iterator ite=elts.begin();
           for(;ite!=elts.end(); ++ite) elements.insert(ite->first);
       }
   }
   if(store) {elements_.assign(elements.begin(), elements.end()); return elements_;}
   return std::vector<Element*>(elements.begin(), elements.end());

}

Parameter& Parameter::operator=(const ClusterTree<FeDof>& ctfd)
{
  if (p_!=nullptr) deletePointer();
  ClusterTree<FeDof>* ctfdb=const_cast<ClusterTree<FeDof>*>(&ctfd);
  p_ = static_cast<void *>(ctfdb);
  type_=_pointerClusterTreeFeDof;
  return *this;
}

void deleteClusterTreeFeDof(void *p)
{
  p=nullptr;
}

void* cloneClusterTreeFeDof(const void* p)
{
  return const_cast<void *>(p);
}

} // end of namespace xlifepp
