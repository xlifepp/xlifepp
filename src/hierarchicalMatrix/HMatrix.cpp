/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file HMatrix.cpp
  \author E. Lunéville
  \since 20 dec 2017
  \date 20 mar 2017

  \brief Implementation of xlifepp::HMatrixIM class members
*/

#include "HMatrix.hpp"

namespace xlifepp
{
//===============================================================================
//member functions of HMatrixIM class
//===============================================================================
void HMatrixIM::build(const std::vector<Parameter>& ps)
{
  trace_p->push("HMatrixIM::build");
  std::set<ParameterKey> params = getParamsKeys(), usedParams;

  this->imType = _HMatrixIM;
  this->deletePointers_ = true;

  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("unexpected_param_key", words("param key", key)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use _cluster or (_clustering_method, _min_block_size), not both
    if (key == _pk_cluster && usedParams.find(_pk_clustering_method) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_clustering_method)); }
    if (key == _pk_clustering_method && usedParams.find(_pk_cluster) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_cluster)); }
    if (key == _pk_cluster && usedParams.find(_pk_min_block_size) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_min_block_size)); }
    if (key == _pk_min_block_size && usedParams.find(_pk_cluster) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_cluster)); }
    // user must use _cluster or (_row_cluster, _col_cluster), not both
    if (key == _pk_cluster && usedParams.find(_pk_row_cluster) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_row_cluster)); }
    if (key == _pk_row_cluster && usedParams.find(_pk_cluster) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_cluster)); }
    if (key == _pk_cluster && usedParams.find(_pk_col_cluster) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_col_cluster)); }
    if (key == _pk_col_cluster && usedParams.find(_pk_cluster) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_cluster)); }
    // user must use (_row_cluster, _col_cluster) or (_clustering_method, _min_block_size), not both
    if (key == _pk_row_cluster && usedParams.find(_pk_clustering_method) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_clustering_method)); }
    if (key == _pk_clustering_method && usedParams.find(_pk_row_cluster) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_row_cluster)); }
    if (key == _pk_row_cluster && usedParams.find(_pk_min_block_size) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_min_block_size)); }
    if (key == _pk_min_block_size && usedParams.find(_pk_row_cluster) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_row_cluster)); }
    if (key == _pk_col_cluster && usedParams.find(_pk_clustering_method) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_clustering_method)); }
    if (key == _pk_clustering_method && usedParams.find(_pk_col_cluster) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_col_cluster)); }
    if (key == _pk_col_cluster && usedParams.find(_pk_min_block_size) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_min_block_size)); }
    if (key == _pk_min_block_size && usedParams.find(_pk_col_cluster) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_col_cluster)); }
    // user must use _threshold or _max_rank, not both
    if (key == _pk_threshold && usedParams.find(_pk_max_rank) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_max_rank)); }
    if (key == _pk_max_rank && usedParams.find(_pk_threshold) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_threshold)); }
  }

  // (_clustering_method, _min_block_size) or _cluster or (_row_cluster, _col_cluster) has to be set
  if (params.find(_pk_clustering_method) != params.end() && params.find(_pk_min_block_size) == params.end())
  { error("param_missing", "min_block_size"); }
  if (params.find(_pk_clustering_method) == params.end() && params.find(_pk_min_block_size) != params.end())
  { error("param_missing", "clustering_method"); }
  if (params.find(_pk_row_cluster) != params.end() && params.find(_pk_col_cluster) == params.end())
  { error("param_missing", "col_cluster"); }
  if (params.find(_pk_row_cluster) == params.end() && params.find(_pk_col_cluster) != params.end())
  { error("param_missing", "row_cluster"); }

  if (params.find(_pk_row_cluster) == params.end()) { params.erase(_pk_cluster); }
  if (params.find(_pk_cluster) == params.end()) { params.erase(_pk_row_cluster); params.erase(_pk_col_cluster); }
  
  // _method has to be set
  if (params.find(_pk_method) != params.end()) { error("param_missing", "method"); }

  // _threshold or _max_rank has to be set
  if (params.find(_pk_threshold) != params.end() && params.find(_pk_max_rank) != params.end()) { error("param_missing", "threshold, max_rank"); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  trace_p->pop();
}

void HMatrixIM::buildParam(const Parameter& p)
{
  trace_p->push("HMatrixIM::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_cluster:
    {
      switch (p.type())
      {
        case _pointerClusterTreeFeDof:
        {
          rowCluster_ = const_cast<ClusterTree<FeDof>*>(static_cast<const ClusterTree<FeDof>*>(p.get_p()));
          colCluster_ = rowCluster_;
          deletePointers_=false;
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_row_cluster:
    {
      switch (p.type())
      {
        case _pointerClusterTreeFeDof:
        {
          rowCluster_ = const_cast<ClusterTree<FeDof>*>(static_cast<const ClusterTree<FeDof>*>(p.get_p()));
          deletePointers_=false;
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_col_cluster:
    {
      switch (p.type())
      {
        case _pointerClusterTreeFeDof:
        {
          colCluster_ = const_cast<ClusterTree<FeDof>*>(static_cast<const ClusterTree<FeDof>*>(p.get_p()));
          deletePointers_=false;
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_min_block_size:
    {
      switch (p.type())
      {
        case _integer:
        {
          minRowSize = p.get_n();
          minColSize = minRowSize;
          break;
        }
        case _integerVector:
        {
          std::vector<number_t> buf = p.get_nv();
          if (buf.size() < 2) { error("bad_size_inf", "_min_block_size", 2, buf.size()); }
          minRowSize=buf[0];
          minColSize=buf[1];
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_clustering_method:
    {
      switch (p.type())
      {
        case _integer:
        case _enumClusteringMethod:
          clusterMethod = ClusteringMethod(p.get_n());
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_approximation_method:
    {
      switch (p.type())
      {
        case _integer:
        case _enumHMApproximationMethod:
          hmAppMethod = HMApproximationMethod(p.get_n());
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_max_rank:
    {
      switch (p.type())
      {
        case _integer:
          maxRank = p.get_n();
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_threshold:
    {
      switch (p.type())
      {
        case _real:
          threshold = p.get_r();
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_eta:
    {
      switch (p.type())
      {
        case _real:
          eta = p.get_r();
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_method:
    {
      if (p.type() == _pointerIntegrationMethod) { intgMethod=const_cast<IntegrationMethod*>(reinterpret_cast<const IntegrationMethod*>(p.get_p())); }
      else if (p.type() == _pointerIntegrationMethods) { intgMethods=*reinterpret_cast<const IntegrationMethods*>(p.get_p()); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      break;
    }
    default:
      break;
  }
  trace_p->pop();
}

void HMatrixIM::buildDefaultParam(ParameterKey key)
{
  trace_p->push("HMatrixIM::buildDefaultParam");
  switch (key)
  {
    case _pk_cluster: rowCluster_=nullptr; colCluster_=nullptr; break;
    case _pk_row_cluster: rowCluster_=nullptr; break;
    case _pk_col_cluster: colCluster_=nullptr; break;
    case _pk_clustering_method:
    {
      if (rowCluster_ != nullptr)
      {
        clusterMethod=rowCluster_->method_;
      }
      else
      {
        clusterMethod=_noClusteringMethod;
      }
      break;
    }
    case _pk_min_block_size:
    {
      if (rowCluster_ != nullptr)
      {
        minRowSize=rowCluster_->maxInBox_;
        minColSize=colCluster_->maxInBox_;
      }
      else
      {
        minRowSize=1;
        minColSize=1;
      }
      break;
    }
    case _pk_approximation_method: hmAppMethod=_noHMApproximation; break;
    case _pk_threshold: threshold=0.; break;
    case _pk_max_rank: maxRank=1; break;
    case _pk_eta: eta=1.; break;
    default: break;
  }
  trace_p->pop();
}

std::set<ParameterKey> HMatrixIM::getParamsKeys()
{
  std::set<ParameterKey> params;
  params.insert(_pk_cluster);
  params.insert(_pk_row_cluster);
  params.insert(_pk_col_cluster);
  params.insert(_pk_cluster);
  params.insert(_pk_min_block_size);
  params.insert(_pk_clustering_method);
  params.insert(_pk_approximation_method);
  params.insert(_pk_max_rank);
  params.insert(_pk_threshold);
  params.insert(_pk_eta);
  params.insert(_pk_method);
  return params;
}

//constructors with IntegrationMethod
HMatrixIM::HMatrixIM(ClusteringMethod clm, number_t minRow, number_t minCol, HMApproximationMethod hmap, number_t maxr, IntegrationMethod& im)
  : rowCluster_(nullptr), colCluster_(nullptr), clusterMethod(clm), hmAppMethod(hmap), minRowSize(minRow), minColSize(minCol), maxRank(maxr),
    threshold(0.), intgMethod(im.clone())
{
  warning("deprecated", "HMatrixIM(ClusteringMethod, Number, Number, HMApproximationMethod, Number, IntegrationMethod)", "HMatrixIM(_clustering_method=xxx, _min_block_size={nr, nc}, _approximation_method=yyy, _max_rank=zzz, _method=im)");
  imType = _HMatrixIM;
  deletePointers_ = true;
}

HMatrixIM::HMatrixIM(ClusteringMethod clm, number_t minRow, number_t minCol, HMApproximationMethod hmap, real_t epsr, IntegrationMethod& im)
  : rowCluster_(nullptr), colCluster_(nullptr), clusterMethod(clm), hmAppMethod(hmap), minRowSize(minRow), minColSize(minCol), maxRank(0),
    threshold(epsr), intgMethod(im.clone())
{
  warning("deprecated", "HMatrixIM(ClusteringMethod, Number, Number, HMApproximationMethod, Real, IntegrationMethod)", "HMatrixIM(_clustering_method=xxx, _min_block_size={nr, nc}, _approximation_method=yyy, _threshold=zzz, _method=im)");
  imType = _HMatrixIM;
  deletePointers_ = true;
}

HMatrixIM::HMatrixIM(HMApproximationMethod hmap,  number_t maxr, IntegrationMethod& im, ClusterTree<FeDof>& rowC, ClusterTree<FeDof>& colC)
  : rowCluster_(&rowC), colCluster_(&colC), hmAppMethod(hmap), maxRank(maxr), threshold(0.), intgMethod(im.clone())
{
  warning("deprecated", "HMatrixIM(HMApproximationMethod, Number, IntegrationMethod, DofCluster, DofCLuster)", "HMatrixIM(_approximation_method=xxx, _max_rank=yyy, _method=im, _row_cluster=rcl, _col_cluster=ccl)");
  imType = _HMatrixIM;
  clusterMethod=rowC.method_;
  minRowSize=rowC.maxInBox_;
  minColSize=colC.maxInBox_;
  deletePointers_ = false;
}

HMatrixIM::HMatrixIM(HMApproximationMethod hmap,  int maxr, IntegrationMethod& im, ClusterTree<FeDof>& rowC, ClusterTree<FeDof>& colC)
  : rowCluster_(&rowC), colCluster_(&colC), hmAppMethod(hmap), maxRank(maxr), threshold(0.), intgMethod(im.clone())
{
  warning("deprecated", "HMatrixIM(HMApproximationMethod, Number, IntegrationMethod, DofCluster, DofCLuster)", "HMatrixIM(_approximation_method=xxx, _max_rank=yyy, _method=im, _row_cluster=rcl, _col_cluster=ccl)");
  imType = _HMatrixIM;
  clusterMethod=rowC.method_;
  minRowSize=rowC.maxInBox_;
  minColSize=colC.maxInBox_;
  deletePointers_ = false;
}

HMatrixIM::HMatrixIM(HMApproximationMethod hmap,  real_t epsr, IntegrationMethod& im, ClusterTree<FeDof>& rowC, ClusterTree<FeDof>& colC)
  : rowCluster_(&rowC), colCluster_(&colC), hmAppMethod(hmap), maxRank(0), threshold(epsr), intgMethod(im.clone())
{
  warning("deprecated", "HMatrixIM(HMApproximationMethod, Real, IntegrationMethod, DofCluster, DofCLuster)", "HMatrixIM(_approximation_method=xxx, _threshold=yyy, _method=im, _row_cluster=rcl, _col_cluster=ccl)");
  imType = _HMatrixIM;
  clusterMethod=rowC.method_;
  minRowSize=rowC.maxInBox_;
  minColSize=colC.maxInBox_;
  deletePointers_ = false;
}

//constructors with IntegrationMethods
HMatrixIM::HMatrixIM(ClusteringMethod clm, number_t minRow, number_t minCol, HMApproximationMethod hmap, number_t maxr, IntegrationMethods& ims)
  : rowCluster_(nullptr), colCluster_(nullptr), clusterMethod(clm), hmAppMethod(hmap), minRowSize(minRow), minColSize(minCol), maxRank(maxr),
    threshold(0.), intgMethod(nullptr), intgMethods(ims)
{
  warning("deprecated", "HMatrixIM(ClusteringMethod, Number, Number, HMApproximationMethod, Number, IntegrationMethods)", "HMatrixIM(_clustering_method=xxx, _min_block_size={nr, nc}, _approximation_method=yyy, _max_rank=zzz, _method=ims)");
  imType = _HMatrixIM;
  deletePointers_ = true;
}

HMatrixIM::HMatrixIM(ClusteringMethod clm, number_t minRow, number_t minCol, HMApproximationMethod hmap, real_t epsr, IntegrationMethods& ims)
  : rowCluster_(nullptr), colCluster_(nullptr), clusterMethod(clm), hmAppMethod(hmap), minRowSize(minRow), minColSize(minCol), maxRank(0),
    threshold(epsr),  intgMethod(nullptr), intgMethods(ims)
{
  warning("deprecated", "HMatrixIM(ClusteringMethod, Number, Number, HMApproximationMethod, Real, IntegrationMethods)", "HMatrixIM(_clustering_method=xxx, _min_block_size={nr, nc}, _approximation_method=yyy, _threshold=zzz, _method=ims)");
  imType = _HMatrixIM;
  deletePointers_ = true;
}

HMatrixIM::HMatrixIM(HMApproximationMethod hmap,  number_t maxr, IntegrationMethods& ims, ClusterTree<FeDof>& rowC, ClusterTree<FeDof>& colC)
  : rowCluster_(&rowC), colCluster_(&colC), hmAppMethod(hmap), maxRank(maxr), threshold(0.),  intgMethod(nullptr), intgMethods(ims)
{
  warning("deprecated", "HMatrixIM(HMApproximationMethod, Number, IntegrationMethods, DofCluster, DofCLuster)", "HMatrixIM(_approximation_method=xxx, _max_rank=yyy, _method=ims, _row_cluster=rcl, _col_cluster=ccl)");
  imType = _HMatrixIM;
  clusterMethod=rowC.method_;
  minRowSize=rowC.maxInBox_;
  minColSize=colC.maxInBox_;
  deletePointers_ = false;
}

HMatrixIM::HMatrixIM(HMApproximationMethod hmap, int maxr, IntegrationMethods& ims, ClusterTree<FeDof>& rowC, ClusterTree<FeDof>& colC)
  : rowCluster_(&rowC), colCluster_(&colC), hmAppMethod(hmap), maxRank(maxr), threshold(0.),  intgMethod(nullptr), intgMethods(ims)
{
  warning("deprecated", "HMatrixIM(HMApproximationMethod, Number, IntegrationMethods, DofCluster, DofCLuster)", "HMatrixIM(_approximation_method=xxx, _max_rank=yyy, _method=ims, _row_cluster=rcl, _col_cluster=ccl)");
  imType = _HMatrixIM;
  clusterMethod=rowC.method_;
  minRowSize=rowC.maxInBox_;
  minColSize=colC.maxInBox_;
  deletePointers_ = false;
}

HMatrixIM::HMatrixIM(HMApproximationMethod hmap,  real_t epsr, IntegrationMethods& ims, ClusterTree<FeDof>& rowC, ClusterTree<FeDof>& colC)
  : rowCluster_(&rowC), colCluster_(&colC), hmAppMethod(hmap), maxRank(0), threshold(epsr), intgMethod(nullptr), intgMethods(ims)
{
  warning("deprecated", "HMatrixIM(HMApproximationMethod, Real, IntegrationMethods, DofCluster, DofCLuster)", "HMatrixIM(_approximation_method=xxx, _threshold=yyy, _method=ims, _row_cluster=rcl, _col_cluster=ccl)");
  imType = _HMatrixIM;
  clusterMethod=rowC.method_;
  minRowSize=rowC.maxInBox_;
  minColSize=colC.maxInBox_;
  deletePointers_ = false;
}

//destructor
HMatrixIM::~HMatrixIM()
{
  if (deletePointers_) clear();
}

void HMatrixIM::clear()
{
  if (rowCluster_ != nullptr) delete rowCluster_;
  if (colCluster_ != rowCluster_ && colCluster_ != nullptr) delete colCluster_;
  rowCluster_ = nullptr;  colCluster_ = nullptr;
  if (intgMethod==nullptr) delete intgMethod;
}

//print stuff
void HMatrixIM::print(std::ostream& out) const
{
  out << "HMatrix method: " << words("cluster method",clusterMethod) << " row leaf size = " << minRowSize << " " << " col leaf size = " << minColSize << " ";
  if (rowCluster_==nullptr && colCluster_==nullptr) out << " cluster not allocated";
  else out << " cluster allocated";
  out << eol << "                " << " compression method: " << words("HMatrix approximation", hmAppMethod) << " threshold=" << threshold << " maxRank=" << maxRank;
  out << eol << "                " << " integration method: ";
  if (intgMethod!=nullptr) out << *intgMethod;
  else out << intgMethods;
  out << eol;
}

}
