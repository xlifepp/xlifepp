/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file HMatrixEntry.cpp
  \author E. Lunéville
  \since 01 nov 2013
  \date 01 nov 2013

  \brief Implementation of xlifepp::HMatrixEntry class member functions and related utilities
*/

#include "HMatrixEntry.hpp"
#include "utils.h"

namespace xlifepp
{

//===============================================================================
//member functions of HMatrixEntry class
//===============================================================================

//! return a reference to HMatrix object, full specialization
template <> template<>
HMatrix<real_t,FeDof>& HMatrixEntry<FeDof>::getHMatrix<real_t>() const
{
  if (rEntries_p!=nullptr) return *rEntries_p;
  error("not_handled","HMatrixEntry::getHMatrix<Real>");
  return *rEntries_p;  //fake return
}

template <> template<>
HMatrix<complex_t,FeDof>& HMatrixEntry<FeDof>::getHMatrix<complex_t>() const
{
  if (cEntries_p!=nullptr) return *cEntries_p;
  error("not_handled","HMatrixEntry::getHMatrix<Complex>");
  return *cEntries_p;  //fake return
}
template <> template<>
HMatrix<Matrix<real_t>,FeDof>& HMatrixEntry<FeDof>::getHMatrix<Matrix<real_t> >() const
{
  if (rmEntries_p!=nullptr) return *rmEntries_p;
  error("not_handled","HMatrixEntry::getHMatrix<Matrix<Real> >");
  return *rmEntries_p;  //fake return
}
template <> template<>
HMatrix<Matrix<complex_t>,FeDof>& HMatrixEntry<FeDof>::getHMatrix<Matrix<complex_t> >() const
{
  if (cmEntries_p!=nullptr) return *cmEntries_p;
  error("not_handled","HMatrixEntry::getHMatrix<Matrix<Complex> >");
  return *cmEntries_p;  //fake return
}

}
