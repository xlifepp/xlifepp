/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file HMatrixEntry.hpp
  \author E. Lunéville
  \since 14 may 2016
  \date 14 may 2016

  \brief Definition of the xlifepp::HMatrixEntry

   xlifepp::HMatrixEntry, I the type of row/col indexation of HMatrix, is a container class to bind the type of matrix coefficient
 */

#ifndef H_MATRIX_ENTRY_HPP
#define H_MATRIX_ENTRY_HPP

#include "config.h"
#include "utils.h"
#include "HMatrix.hpp"


namespace xlifepp
{
/*!
  \class HMatrixEntry
  container class to deal with main type of HMatrix
*/
template <typename I>
class HMatrixEntry
{
public:
    ValueType valueType_;                       //!< value type of the entries (_real,_complex)
    StrucType strucType_;                       //!< structure type of the entries (_scalar,_matrix)
    HMatrix<real_t,I>* rEntries_p;              //!< pointer to real HMatrix
    HMatrix<complex_t,I>* cEntries_p;           //!< pointer to complex HMatrix
    HMatrix<Matrix<real_t>,I>* rmEntries_p;     //!< pointer to HMatrix of real matrices
    HMatrix<Matrix<complex_t>,I>* cmEntries_p;  //!< pointer to HMatrix of complex matrices
    dimPair nbOfComponents;                     //!< number of line and columns of matrix values, (1,1) in scalar case

    //constructor, destructor stuff
    HMatrixEntry(ValueType, StrucType, ClusterTree<I>&, ClusterTree<I>&,
                 number_t, number_t, number_t =1, number_t=1, SymType sy = _noSymmetry);  //!< constructor from cluster trees
    HMatrixEntry(const HMatrixEntry<I>&);              //!< copy constructor
    HMatrixEntry<I>& operator=(const HMatrixEntry<I>&);//!< assign operator
    void copy(const HMatrixEntry<I>&);                //!< full copy of members
    void clear();                                     //!< deallocate memory used by a MatrixEntry
    ~HMatrixEntry(){clear();}                         //!< destructor

    //property modifiers
    void setClusterRow(ClusterTree<I>*);              //!< change ClusterTree row pointer of HMatrix
    void setClusterCol(ClusterTree<I>*);              //!< change ClusterTree col pointer of HMatrix

    //various stuff
    template <typename T>
    HMatrix<T,I>& getHMatrix() const                  //! get HMatrix object
    { error("not_handled","HMatrix::getHMatrix()"); return *new HMatrix<T,I>(); }
    real_t norm2() const;                             //!< Frobenius norm
    real_t norminfty() const;                           //!< infinite norm

    //printing stuff
    void print(std::ostream&) const;                  //!< print HMatrixEntry
    void print(PrintStream& os) const {print(os.currentStream());}
    void printSummary(std::ostream&) const;           //!< print HMatrixEntry in brief
    void printSummary(PrintStream& os) const {printSummary(os.currentStream());}
};

template <typename T>
std::ostream& operator<<(std::ostream& out, const HMatrixEntry<T>& hme)
{
    hme.print(out);
    return out;
}

//! constructor from cluster trees
template<typename I>
HMatrixEntry<I>::HMatrixEntry(ValueType vt, StrucType st, ClusterTree<I>& ctRow, ClusterTree<I>& ctCol,
                           number_t nboxrow, number_t nboxcol, number_t nrow, number_t ncol, SymType sy)
{
  valueType_=vt;
  strucType_=st;
  nbOfComponents = dimPair(nrow,ncol);
  rEntries_p=nullptr; cEntries_p=nullptr; rmEntries_p=nullptr; cmEntries_p=nullptr;
  if(vt==_real)
    {
      if(st==_scalar) rEntries_p=new HMatrix<real_t,I>(ctRow, ctCol, nboxrow, nboxcol, 0, "", sy);
      if(st==_matrix) rmEntries_p=new HMatrix<Matrix<real_t>,I>(ctRow, ctCol, nboxrow, nboxcol, 0, "", sy);
      return;
    }
  //complex
  if(st==_scalar) cEntries_p=new HMatrix<complex_t,I>(ctRow, ctCol, nboxrow, nboxcol, 0, "", sy);
  if(st==_matrix) cmEntries_p=new HMatrix<Matrix<complex_t>,I>(ctRow, ctCol, nboxrow, nboxcol, 0, "", sy);
  return;
}

 // copy constructor(full copy)
template<typename I>
HMatrixEntry<I>::HMatrixEntry(const HMatrixEntry<I>& hm)
{
    rEntries_p=nullptr; cEntries_p=nullptr; rmEntries_p=nullptr; cmEntries_p=nullptr;
    copy(hm);
}

// assign operator (full copy)
template<typename I>
HMatrixEntry<I>& HMatrixEntry<I>::operator=(const HMatrixEntry<I>& hm)
{
    clear();
    copy(hm);
    return *this;
}

//copy
template<typename I>
void HMatrixEntry<I>::copy(const HMatrixEntry<I>& hm)
{
  valueType_=hm.valueType_;
  strucType_=hm.strucType_;
  nbOfComponents=hm.nbOfComponents;
  if(hm.rEntries_p != nullptr)  rEntries_p  = new HMatrix<real_t,I>(*hm.rEntries_p);
  if(hm.cEntries_p != nullptr)  cEntries_p  = new HMatrix<complex_t,I>(*hm.cEntries_p);
  if(hm.rmEntries_p != nullptr) rmEntries_p = new HMatrix<Matrix<real_t>,I>(*hm.rmEntries_p);
  if(hm.cmEntries_p != nullptr) cmEntries_p = new HMatrix<Matrix<complex_t>,I>(*hm.cmEntries_p);
}

//destructor
template<typename I>
void HMatrixEntry<I>::clear()
{
  if(rEntries_p != nullptr) delete rEntries_p;
  if(cEntries_p != nullptr) delete cEntries_p;
  if(rmEntries_p != nullptr) delete rmEntries_p;
  if(cmEntries_p != nullptr) delete cmEntries_p;
  rEntries_p=nullptr; cEntries_p=nullptr; rmEntries_p=nullptr; cmEntries_p=nullptr;
}

//change ClusterTree row/col pointer of HMatrix
template<typename I>
void HMatrixEntry<I>::setClusterRow(ClusterTree<I>* ctp)
{
  if(rEntries_p != nullptr)  rEntries_p->setClusterRow(ctp);
  if(cEntries_p != nullptr)  cEntries_p->setClusterRow(ctp);
  if(rmEntries_p != nullptr) rmEntries_p->setClusterRow(ctp);
  if(cmEntries_p != nullptr) cmEntries_p->setClusterRow(ctp);
}

template<typename I>
void HMatrixEntry<I>::setClusterCol(ClusterTree<I>* ctp)
{
  if(rEntries_p != nullptr)  rEntries_p->setClusterCol(ctp);
  if(cEntries_p != nullptr)  cEntries_p->setClusterCol(ctp);
  if(rmEntries_p != nullptr) rmEntries_p->setClusterCol(ctp);
  if(cmEntries_p != nullptr) cmEntries_p->setClusterCol(ctp);
}

//Frobenius norm
template<typename I>
real_t HMatrixEntry<I>::norm2() const
{
  if(rEntries_p != nullptr)  return rEntries_p->norm2();
  if(cEntries_p != nullptr)  return cEntries_p->norm2();
  if(rmEntries_p != nullptr) return rmEntries_p->norm2();
  if(cmEntries_p != nullptr) return cmEntries_p->norm2();
  return 0.;
}

//infinite norm
template<typename I>
real_t HMatrixEntry<I>::norminfty() const
{
  if(rEntries_p != nullptr)  return rEntries_p->norminfty();
  if(cEntries_p != nullptr)  return cEntries_p->norminfty();
  if(rmEntries_p != nullptr) return rmEntries_p->norminfty();
  if(cmEntries_p != nullptr) return cmEntries_p->norminfty();
  return 0.;
}


// print HMatrixEntry
template<typename I>
void HMatrixEntry<I>:: print(std::ostream& out) const
{
  if(rEntries_p != nullptr) rEntries_p->print(out);
  if(cEntries_p != nullptr) cEntries_p->print(out);
  if(rmEntries_p != nullptr) rmEntries_p->print(out);
  if(cmEntries_p != nullptr) cmEntries_p->print(out);
}

// print HMatrixEntry
template<typename I>
void HMatrixEntry<I>:: printSummary(std::ostream& out) const
{
  if(rEntries_p != nullptr) rEntries_p->printSummary(out);
  if(cEntries_p != nullptr) cEntries_p->printSummary(out);
  if(rmEntries_p != nullptr) rmEntries_p->printSummary(out);
  if(cmEntries_p != nullptr) cmEntries_p->printSummary(out);
}

//specialization of getMatrix
template <> template<> HMatrix<real_t,FeDof>& HMatrixEntry<FeDof>::getHMatrix<real_t>() const;
template <> template<> HMatrix<complex_t,FeDof>& HMatrixEntry<FeDof>::getHMatrix<complex_t>() const;
template <> template<> HMatrix<Matrix<real_t>,FeDof>& HMatrixEntry<FeDof>::getHMatrix<Matrix<real_t> >() const;
template <> template<> HMatrix<Matrix<complex_t>,FeDof>& HMatrixEntry<FeDof>::getHMatrix<Matrix<complex_t> >() const;

} // end of namespace xlifepp

#endif /* H_MATRIX_ENTRY_HPP */
