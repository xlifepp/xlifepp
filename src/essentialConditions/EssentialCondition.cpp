/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file EssentialCondition.cpp
  \author E. Lunéville
  \since 14 jan 2014
  \date  26 nov 2014

  \brief Implementation of xlifepp::EssentialCondition class member functions and related utilities
*/

#include "EssentialCondition.hpp"
#include "utils.h"

namespace xlifepp
{

//===============================================================================
//member functions of EssentialCondition class
//===============================================================================
//constructors/destructor
//constructor lcop = 0 on D (D given by the LcOperatorOnUnknown may be 0)
EssentialCondition::EssentialCondition(const LcOperatorOnUnknown& lc, ECMethod ecm)
{
  lf_p=nullptr;
  ecTerms_=lc;
  fun_p=nullptr;
  ecMethod=ecm;
  redundancyTolerance_=100*theEpsilon;
  setType();
}

//constructor lcop = g on D (D given by the LcOperatorOnUnknown may be 0)
EssentialCondition::EssentialCondition(const LcOperatorOnUnknown& lc, const Function& f, ECMethod ecm)
{
  lf_p=nullptr;
  ecTerms_=lc;
  fun_p=new Function(f);
  ecMethod=ecm;
  redundancyTolerance_=100*theEpsilon;
  setType();
}

//constructor lcop = 0 on D (D given explicitely)
EssentialCondition::EssentialCondition(GeomDomain& dom, const LcOperatorOnUnknown& lc, ECMethod ecm)
{
  lf_p=nullptr;
  ecTerms_=lc;
  ecTerms_.setDomain(dom);
  fun_p=nullptr;
  ecMethod=ecm;
  redundancyTolerance_=100*theEpsilon;
  setType();
}

//constructor lcop = g on D (D given explicitely)
EssentialCondition::EssentialCondition(GeomDomain& dom, const LcOperatorOnUnknown& lc, const Function& f, ECMethod ecm)
{
  lf_p=nullptr;
  ecTerms_=lc;
  ecTerms_.setDomain(dom);
  fun_p=new Function(f);
  ecMethod=ecm;
  redundancyTolerance_=100*theEpsilon;
  setType();
}

/*to avoid cross dependances, the following member functions are implemented in TermVector.cpp
   EssentialCondition::EssentialCondition(const TermVector& tv, const complex_t& c)
   EssentialCondition::EssentialCondition(const LinearForm& lf, const complex_t& c)
   void EssentialCondition::copy(const EssentialCondition& ec)
   void EssentialCondition::clear() // constructor lf(u) = r/c (explicit TermVector or LinearForm)
*/


// copy constructor
EssentialCondition::EssentialCondition(const EssentialCondition& ec)
{
  copy(ec);
}

// assign operator
EssentialCondition& EssentialCondition::operator=(const EssentialCondition& ec)
{
  if(this==&ec) return *this;   //same object
  clear();
  copy(ec);
  return *this;
}

// destructor
EssentialCondition::~EssentialCondition()
{
  clear();
}

//identify the type of essential condition (Dirichlet, Transmission, ...)
EcType EssentialCondition::setType()
{
  type_ = _undefEcType;
  if(isSingleDomain() && isSingleUnknown())  type_=_DirichletEc;
  if(isSingleDomain() && nbOfUnknowns()==2)  type_=_transmissionEc;
  if(nbOfDomains()==2 && isSingleUnknown())  type_=_periodicEc;    //or _crackEc if D1 and D2 are supported by the same geometry
  return type_;
}

/*! analyze consistency of Essential condition except LinearForm ones
    analysis rules:
    each term of ecTerms_ must be attached to a (non void) domain
    no more than two domains be defined
    check structure compatibility
      if stop = true, program stops on error else returns an error message as string_t
      return a void string_t if no error
*/
string_t EssentialCondition::consistency(bool stop) const
{
  if(ecTerms_.size()==0) error("is_void","ecTerms_");

  std::vector<GeomDomain *> doms = ecTerms_.domains();
  std::vector<GeomDomain *>::const_iterator itd=doms.begin();
  cit_opuval it=ecTerms_.begin();
  const GeomDomain* dom1=*itd, *dom2=nullptr;
  StrucType st=it->first->strucType();

  for(; itd!=doms.end(); itd++, it++)
  {
    if(*itd==nullptr)  //check domain existence (1 by term)
    {
      std::stringstream ss;
      ss<<"term "<< *it->first<< " has no domain defined in "<< *this;
      if (stop)
      {
        where("EssentialCondition::consistency()");
        error("free_error",ss.str());
      }
      return "error 1 : "+ss.str();
    }
    else //check number of domains (max 2)
    {
      if(*itd != dom1)
      {
        if(dom2==nullptr) dom2=*itd;
        else if(*itd!=dom2)
        {
          std::stringstream ss;
          ss<<"more than 2 domains in "<< *this;
          if (stop)
          {
            where("EssentialCondition::consistency()");
            error("free_error",ss.str());
          }
          return "error 2 : "+ss.str();
        }
      }
    }
    if(it->first->strucType()!=st) //check structure consistency of terms
    {
      std::stringstream ss;
      ss<<"term "<< *it->first<< "in "<< *this << ", should return a "<< words("structure",st) ;
      if (stop)
      {
        where("EssentialCondition::consistency()");
        error("free_error",ss.str());
      }
      return "error 3 : "+ss.str();
    }
  }
  return "";
}

//access to unknowns
bool EssentialCondition::isSingleUnknown() const
{
  if(lf_p!=nullptr) return isSingleUnknownTV();
  return ecTerms_.isSingleUnknown();
}

dimen_t EssentialCondition::nbOfUnknowns() const
{
  if(lf_p!=nullptr) return nbOfUnknownsTV();
  return ecTerms_.unknowns().size();
}

const Unknown* EssentialCondition::unknown(number_t i) const
{
  if(lf_p!=nullptr) return unknownTV(i);
  return ecTerms_.unknown(i);
}

std::set<const Unknown*> EssentialCondition::unknowns() const
{
  if(lf_p!=nullptr) return unknownsTV();
  return ecTerms_.unknowns();
}

/* to avoid cross dependances, the following member functions are implemented in TermVector.cpp
   bool EssentialCondition::isSingleUnknownTV()
   dimen_t EssentialCondition::nbOfUnknownsTV() const
   const Unknown* EssentialCondition::unknownTV() const
   std::set<const Unknown*> EssentialCondition::unknownsTV() const
   string_t nameTV() const
*/

//utilities
string_t EssentialCondition::name() const
{
  if(lf_p!=nullptr) return nameTV();
  std::stringstream ss;
  ecTerms_.print(ss,true);
  ss<<" = ";
  if(fun_p!=nullptr)
  {
      if(fun_p->name()!="?") ss<<fun_p->name();
      else ss<<"g";
  }
  else  ss<<"0";
  return ss.str();
}

void EssentialCondition::print(std::ostream& os) const
{
  os<<words("essential condition", type_)<<" ";
  if(lf_p!=nullptr)
     {
         printTV(os);
         return;
     }
  bool isd=isSingleDomain();
  ecTerms_.print(os,!isd);
  os<<" = ";
  if(fun_p!=nullptr)
  {
      if(fun_p->name()!="?") os<<fun_p->name();
      else os<<"g";
  }
  else  os<<"0";
  if(isd) os<<" "<<words("on")<<" "<<ecTerms_.domain()->name();
  os<<", redundancy tolerance="<<redundancyTolerance_;
  return;
}

std::ostream& operator<<(std::ostream& os, const EssentialCondition& bc)
{
  bc.print(os);
  return os;
}

//EssentialCondition like constructors
EssentialCondition LcOperatorOnUnknown::operator = (const real_t & a)        // construct  LcOp(u)=a
{
  if(a==0.) return EssentialCondition(*this);
  Parameters* pa=new Parameters(a,"const_value");
  Function f(real_const_fun,tostring(a),*pa);
  return EssentialCondition(*this,f);
}

EssentialCondition LcOperatorOnUnknown::operator = (const complex_t & a)     // construct  LcOp(u)=a
{
  if(a==complex_t(0.)) return EssentialCondition(*this);
  Parameters* pa=new Parameters(a,"const_value");
  Function f(complex_const_fun,tostring(a),*pa);
  return EssentialCondition(*this,f);
}

EssentialCondition LcOperatorOnUnknown::operator = (const Function & f)    // construct  LcOp(u)=f
{
  return EssentialCondition(*this,f);
}

EssentialCondition OperatorOnUnknown::operator = (const real_t & a)            // construct  Op(u)=a
{
  if(a==0.)return EssentialCondition(LcOperatorOnUnknown(*this));
  Parameters* pa=new Parameters(a,"const_value");
  Function f(real_const_fun,tostring(a),*pa);
  return EssentialCondition(LcOperatorOnUnknown(*this),f);
}

EssentialCondition OperatorOnUnknown::operator = (const complex_t & a)            // construct  Op(u)=a
{
  if(a==complex_t(0.)) return EssentialCondition(LcOperatorOnUnknown(*this));
  Parameters* pa=new Parameters(a,"const_value");
  Function f(complex_const_fun,tostring(a),*pa);
  return EssentialCondition(LcOperatorOnUnknown(*this),f);
}

EssentialCondition OperatorOnUnknown::operator = (const Function & f)        // construct  Op(u)=f
{
  return EssentialCondition(LcOperatorOnUnknown(*this), f);
}

EssentialCondition Unknown::operator = (const real_t & a)                     // construct  u = a
{
  if(a==0.)return EssentialCondition(LcOperatorOnUnknown(*this));
  Parameters* pa=new Parameters(a,"const_value");
  Function f(real_const_fun,tostring(a),*pa);
  return EssentialCondition(LcOperatorOnUnknown(*this),f);
}

EssentialCondition Unknown::operator = (const complex_t & a)                   // construct  u = a
{
  if(a==complex_t(0.)) return EssentialCondition(LcOperatorOnUnknown(*this));
  Parameters* pa=new Parameters(a,"const_value");
  Function f(complex_const_fun,tostring(a),*pa);
  return EssentialCondition(LcOperatorOnUnknown(*this),f);
}

EssentialCondition Unknown::operator = (const Function & f)                  // construct  u = f
{
  return EssentialCondition(LcOperatorOnUnknown(*this), f);
}

EssentialCondition on(GeomDomain& dom, const EssentialCondition& bc)
{
  EssentialCondition bcr(bc);
  bcr.setDomain(dom);
  return bcr;
}

//implementation of function related to the small class DomUnkDop
bool operator ==(const DomUnkDop& s1,const DomUnkDop& s2)
{return (s1.dom==s2.dom && s1.unk==s2.unk && s1.dop==s2.dop);}

bool operator !=(const DomUnkDop& s1,const DomUnkDop& s2)
{return !(s1==s2);}

bool operator <(const DomUnkDop& s1,const DomUnkDop& s2)
{
  if(s1.dom<s2.dom ) return true;
  if(s1.dom>s2.dom ) return false;
  if(s1.unk<s2.unk ) return true;
  if(s1.unk>s2.unk ) return false;
  if(s1.dop<s2.dop) return true;
  return false;
}

bool operator > (const DomUnkDop& s1,const DomUnkDop& s2)
{return s2 < s1;}

bool operator <= (const DomUnkDop& s1,const DomUnkDop& s2)
{return !(s1 > s2);}

bool operator >= (const DomUnkDop& s1,const DomUnkDop& s2)
{return !(s1 < s2);}

} // end of namespace xlifepp
