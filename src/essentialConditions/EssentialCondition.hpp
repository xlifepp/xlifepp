/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file EssentialCondition.hpp
  \author E. Lunéville
  \since 14 jan 2014
  \date 14 jan 2014

  \brief Definition of the xlifepp::EssentialCondition class

  xlifepp::EssentialCondition class handles the description of a Essential condition. It is of the form
                 sum a_i op_i(u_i) = 0 or g  on D  (single domain B.C.)
                 sum a_i op_i(u_i) on D1 + sum b_i op_i(v_i) on D2 = 0 or g  (double domain B.C.)
            or   lf(u_i)=s (lf a linear form and s a real or a complex scalar)
  where
        u_i  is an unknown
        op_i is an operator on unknown  (general form: A opa difop(u) opa B)
        a_i  is a real/complex scalar
        D1/D2 are domains() where act the Essential condition

  Single domain B.C are devoted to classic essential condition (Dirichlet) with possibly multiple unknowns
  whereas double domain B.C. are devoted to periodic condition, transmission condition across a crack (single unknown)
  Be care: double domain B.C are not designed to deal with the double condition u=0 on D1 and u=0 on D2

  Example:
      Dirichlet condition: u = 0/g on D, U^n = 0/gG, U.n=0/g on D, ...
      Transmission condition: u - v = 0/g on D, U^n - V^n = 0/G on D, ...
      Periodic condition: u|D1 - u|D2 = 0/g
      Transmission across crack: u|D+ - u|D- = 0/g (required that crack faces D+ and D- are defined)
      Mean condition: int_D u = 0 (deal as a general lf(u)=0)

  \note if it possible to write some general Essential condition, only a few one are taken into account as essential Essential condition !

  xlifepp::EssentialCondition is either
      an expression involving operator on unknowns and a right hand side function  (e.g u|dom=g)
      stored in ecTerms_ (xlifepp::LcOperatorOnUnknown object) and fun_p (pointer to Function object)
  or
      an expression involving a xlifepp::TermVector representing a linear form and a right hand side scalar (e.g lf(u)=s)
      stored in lf_p (pointer to xlifepp::TermVector object) and clf (Complex)

  \note may be in future implement it as two separated child classes
*/


#ifndef ESSENTIAL_CONDITION_HPP
#define ESSENTIAL_CONDITION_HPP

#include "config.h"
#include "operator.h"

namespace xlifepp
{

//forward class declaration
class TermVector;
class LinearForm;

/*!
  \class EssentialCondition
  end user class handling symbolic representation of an essential condition
*/
class EssentialCondition
{
  protected:
    LcOperatorOnUnknown ecTerms_;     //!< linear combination of operator on unknown
    Function *fun_p;                  //!< function defining data of BC (when void, BC is homogeneous ... = 0)
    TermVector* lf_p;                 //!< pointer to TermVector representing linear form essential condition
    EcType type_;                     //!< type of Essential condition
    real_t redundancyTolerance_;      //!< tolerance factor used in reduction process
  public:
    complex_t clf;                    //!< constant scalar in lf(u)=clf
    ECMethod ecMethod;                //!< method used to build constraints (_dofEC, _internalNodeEC, _momentEC)
  public:
    //constructors/destructor and assignment
    EssentialCondition()                                              //! default constructor
        : fun_p(nullptr), lf_p(nullptr), clf(0), ecMethod(_undefECMethod), redundancyTolerance_(100*theEpsilon) {}
    EssentialCondition(const LcOperatorOnUnknown&, ECMethod =_undefECMethod);                               //!< basic constructor lcop =0 on D (D given by the LcOpOnUnknown !)
    EssentialCondition(const LcOperatorOnUnknown&, const Function&, ECMethod =_undefECMethod);              //!< basic constructor lcop =g on D (D given by the LcOpOnUnknown !)
    EssentialCondition(const LcOperatorOnUnknown&, const TermVector&, ECMethod =_undefECMethod);            //!< basic constructor lcop = TV on D (D given by the LcOpOnUnknown !)
    EssentialCondition(GeomDomain&, const LcOperatorOnUnknown&, ECMethod =_undefECMethod);                  //!< basic constructor lcop =0 on D (D given explicitely)
    EssentialCondition(GeomDomain&, const LcOperatorOnUnknown&, const Function&, ECMethod =_undefECMethod); //!< basic constructor lcop =0 on D (D given explicitely)
    EssentialCondition(const EssentialCondition&);                    //!< copy constructor
    ~EssentialCondition();                                            //!< destructor
    EssentialCondition& operator=(const EssentialCondition&);         //!< assignment
    void copy(const EssentialCondition&);                             //!< internal copy tool
    void clear();                                                     //!< clear object

    EcType type() const {return type_;}                               //!< returns type
    const real_t& redundancyTolerance() const{return redundancyTolerance_;}
    real_t& redundancyTolerance(real_t tol) {redundancyTolerance_=tol; return redundancyTolerance_;}

    //member functions related only to symbolic representation
    //--------------------------------------------------------
    const std::vector<OpuValPair>& bcTerms() const   {return ecTerms_;} //!< returns ess. cond. terms
    const LcOperatorOnUnknown& ecTerms() const {return ecTerms_;}       //!< returns ess. cond. terms
    const Function& fun() const   {return *fun_p;}                      //!< returns function (const)
    const Function* funp() const  {return fun_p;}                       //!< returns function as pointer (const)
    Function*& funp()             {return fun_p;}                       //!< returns function as pointer (non const)
    it_opuval begin()        {return ecTerms_.begin();}                 //!< returns iterator on first term (non const)
    cit_opuval begin() const {return ecTerms_.begin();}                 //!< returns const_iterator on first term (const)
    it_opuval end()          {return ecTerms_.end();}                   //!< returns iterator on last term (non const)
    cit_opuval end() const   {return ecTerms_.end();}                   //!< returns const_iterator on last term (const)
    number_t size() const    {return ecTerms_.size();}                  //!< returns number of terms

    bool isSingleUnknown() const ;                        //!< true if all terms involve the same unknown
    dimen_t nbOfUnknowns() const;                         //!< number of unknowns involved in condition
    const Unknown* unknown(number_t i=1) const;           //!< return the ith unknown involved (may be 0 if none)
    std::set<const Unknown*> unknowns() const;            //!< return set of Unknown's involved in condition

    //accessors to symbolic properties
    bool isSingleDomain() const                           //! true for a single domain B.C
        {return ecTerms_.isSingleDomain();}
    GeomDomain* domain(number_t i=1) const                //! return domain if single domain condition (may be 0 if none)
        {return ecTerms_.domain(i);}
    std::set<GeomDomain*> domains() const                 //! return set of Domains's involved in condition
        {return ecTerms_.domainSet();}
    dimen_t nbOfDomains() const                           //! number of domains involved in condition
        {return dimen_t(ecTerms_.domainSet().size());}
    number_t nbTerms() const                              //! return the number of terms
        {return ecTerms_.size();}
    complex_t coefficient() const                         //! return coefficient of first term if a single term condition
        {return ecTerms_.coefficient();}
    std::vector<complex_t> coefficients() const           //! return vector of coefficients involved in condition
        {return ecTerms_.coefficients();}
    std::set<DiffOpType> diffOperators() const            //! return set of differential operator types involved in condition
        {return ecTerms_.diffOperators();}
    number_t nbOfDifOp() const                            //! number of diff operator involved in condition
        {return diffOperators().size();}
    DiffOpType diffOperator() const                       //! return first differential operator type involved in condition
        {return ecTerms_[0].first->difOpType();}
    bool isHomogeneous() const                            //! true if = 0 (no function pointer)
        {return fun_p==nullptr;}

    string_t consistency(bool stop=true) const;           //!< analyze consistency of Essential condition
    EcType setType();                                     //!< set the type of the Essential condition

    //domain affectation
    void setDomain(GeomDomain& dom)                       //! restrict BC (all operators) to domain dom
         {ecTerms_.setDomain(dom);setType();}
    EssentialCondition& operator |(GeomDomain& dom)       //! restrict BC (all operators) to domain dom using syntax |dom
         {ecTerms_.setDomain(dom); setType(); return *this;}

    //member functions related only to TermVector representation
    //----------------------------------------------------------
    EssentialCondition(const TermVector&, const complex_t& = complex_t(0.));    //!< constructor from linear form and scalar
    EssentialCondition(const LinearForm&, const complex_t& = complex_t(0.));    //!< constructor from linear form and scalar
    const TermVector* lfp() const {return lf_p;}          //!< returns assembled linear form
    bool isSingleUnknownTV() const ;                      //!< true if all terms involve the same unknown
    dimen_t nbOfUnknownsTV() const;                       //!< number of unknowns involved in condition
    const Unknown* unknownTV(number_t =1) const;          //!< return ith unknown for TV form(may be 0 if none)
    std::set<const Unknown*> unknownsTV() const;          //!< return set of Unknown's involved in condition
    void printTV(std::ostream&) const;                    //!< specific print for TermVector EC form
    string_t nameTV() const;                              //!< symbolic name ("tv = c")

    // print utilities
    string_t name() const;                                //!< symbolic name (e.g "u|dom=g")
    void print(std::ostream&) const;                      //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator<<(std::ostream&, const EssentialCondition&);
};

std::ostream& operator<<(std::ostream&, const EssentialCondition&); //!< print operator
EssentialCondition on(GeomDomain&, const EssentialCondition&);      //!< set domain to a Essential condition

/*!
   \class DomUnkDop
   small structure to ordered terms of a EssentialCondition
*/
class DomUnkDop
{
  public:
    const GeomDomain* dom; //!< domain
    const Unknown* unk;    //!< unknown
    DiffOpType dop;        //!< differential operator
    DomUnkDop(const GeomDomain* d, const Unknown* u, DiffOpType dif) //! constructor
    : dom(d), unk(u), dop(dif) {}
};

bool operator == (const DomUnkDop&, const DomUnkDop&);  //!< equality
bool operator != (const DomUnkDop&, const DomUnkDop&);  //!< difference
bool operator <  (const DomUnkDop&, const DomUnkDop&);  //!< less than
bool operator >  (const DomUnkDop&, const DomUnkDop&);  //!< greater than
bool operator <= (const DomUnkDop&, const DomUnkDop&);  //!< less than or equal
bool operator >= (const DomUnkDop&, const DomUnkDop&);  //!< greater than or equal

//prototype of rhs functions involving TermVector (implemented in TermVector.cpp)
real_t fun_EC_SR(const Point& P, Parameters& pars);
complex_t fun_EC_SC(const Point& P, Parameters& pars);
Vector<real_t> fun_EC_VR(const Point& P, Parameters& pars);
Vector<complex_t> fun_EC_VC(const Point& P, Parameters& pars);

typedef EssentialCondition BoundaryCondition;
} // end of namespace xlifepp

#endif // ESSENTIAL_CONDITION_HPP
