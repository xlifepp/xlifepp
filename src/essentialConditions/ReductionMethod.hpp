/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ReductionMethod.hpp
  \author E. Lunéville
  \since 15 apr 2016
  \date  15 apr 2016

  \brief Definition of the xlifepp::ReductionMethod class
         it handles informations about the method of reduction of the constraints, mainly manages:
           - method: one of _noReduction, _pseudoReduction, _realReduction, _penalizationReduction, _dualReduction
           - alpha: the diagonal coefficient in case of pseudoReduction or the penalization coefficient in case of penalization
*/


#ifndef REDUCTIONMETHOD_HPP_INCLUDED
#define REDUCTIONMETHOD_HPP_INCLUDED

#include "config.h"
#include "EssentialCondition.hpp"

namespace xlifepp {

//============================================================================================================
/*!
  \class ReductionMethod
   handling constraints reduction method information
   when dealing with essential conditions (constraints), several methods are available
     - pseudo reduction
     - full reduction
     - penalization
     - duality        (not yet available)

   pseudo-reduction is the full reduction method where eliminated row are replaced by the constraints matrix C:
               | beta*I + alpha*C  |   eliminated rows
       Ap=     |                   |
               |    0      Ae      |
   default is alpha=1 and beta=0, good choice when solving a problem
   choose  alpha=0 and beta!=0 when dealing with eigenvalue problem
*/
class ReductionMethod
{
  public:
     ReductionMethodType method;   //!< type of reduction method
     complex_t alpha;              //!< diagonal or penalization coefficient
     complex_t beta;               //!< additional penalization coefficient

    //constructors
    ReductionMethod(ReductionMethodType em = _noReduction, const complex_t& a = complex_t(1.,0.), const complex_t& b = complex_t(0.,0.))
    :method(em), alpha(a), beta(b) {if(em==_realReduction) {alpha=0.;beta=0.;}}
    ValueType coeffType() const{return (alpha.imag()==0)?_real:_complex;}

    //print utilities
    void print(std::ostream& out) const
    {
      out<<words("Reduction method",method);
      if(method==_pseudoReduction) out<<" diagonal";
      if(method==_penalizationReduction) out<<" penalisation";
      out<<" coefficients:  ";
      if(coeffType()==_real) out<<"alpha = "<<alpha.real()<<" beta = "<<beta.real()<<eol;
      else out<<"alpha = "<<alpha<<" beta = "<<beta<<eol;
    }
    friend std::ostream& operator << (std::ostream& out, const ReductionMethod& rm)
    { rm.print(out); return out; }
};

} // end of namespace xlifepp

#endif // REDUCTIONMETHOD_HPP_INCLUDED
