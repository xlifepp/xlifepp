/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file EssentialConditions.hpp
  \author E. Lunéville
  \since 16 jan 2014
  \date  16 jan 2014

  \brief Definition of the xlifepp::EssentialConditions class

  xlifepp::EssentialConditions class handles a set of xlifepp::EssentialCondition's
*/


#ifndef ESSENTIAL_CONDITIONS_HPP
#define ESSENTIAL_CONDITIONS_HPP

#include "config.h"
#include "EssentialCondition.hpp"

namespace xlifepp {

class SetOfConstraints;  //forward declaration

//============================================================================================================
/*!
  \class EssentialConditions
  handling a list of Essential conditions
*/
class EssentialConditions : public std::list<EssentialCondition>
{
  protected:
    real_t redundancyTolerance_;
  public:
    mutable SetOfConstraints* constraints_;  //useful to relate EssentialConditions to SetOfConstraints built later

    //constructors
    EssentialConditions() : constraints_(nullptr), redundancyTolerance_(0.) {}       //!< default constructor
    EssentialConditions(const EssentialCondition&);                            //!< constructor from 1 EssentialCondition
    EssentialConditions(const EssentialCondition&, const EssentialCondition&); //!< constructor from 2 EssentialCondition
    EssentialConditions(const EssentialConditions&);                           //!< copy constructor
    EssentialConditions& operator = (const EssentialConditions&);              //!< assignment operator
    ~EssentialConditions();                                                    //!< destructor

    //various utilities
    const real_t& redundancyTolerance() const{return redundancyTolerance_;}
    real_t& redundancyTolerance(real_t tol);
    bool coupledUnknowns() const;                  //!< true if one essential condition couples at least two unknowns
    std::set<const Unknown*> unknowns() const;     //!< return unknowns involved in constraint
    const Unknown* unknown() const;                //!< return unknown involved in constraint, 0 if more than one unknown

    //print utilities
    void print(std::ostream&) const; //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator << (std::ostream&, const EssentialConditions &);
};

std::ostream& operator << (std::ostream&, const EssentialConditions &); //!< print operator

//pseudo constructor
EssentialConditions operator & (const EssentialCondition&, const EssentialCondition&);   //!< bcs = ec & ec
EssentialConditions operator & (const EssentialCondition&, const EssentialConditions&);  //!< bcs = ec & ecs
EssentialConditions operator & (const EssentialConditions&, const EssentialCondition&);  //!< bcs = ecs & ec
EssentialConditions operator & (const EssentialConditions&, const EssentialConditions&); //!< bcs = ecs & ecs

typedef EssentialConditions BoundaryConditions;

} // end of namespace xlifepp



#endif // Essential_CONDITIONS_HPP
