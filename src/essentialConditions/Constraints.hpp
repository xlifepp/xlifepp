/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Constraints.hpp
  \author E. Lunéville
  \since 26 feb 2014
  \date  26 feb 2014

  \brief Definition of the xlifepp::Constraints class

  xlifepp::Constraints is the main class managing algebraic representation of xlifepp::EssentialConditions
  constraints CU=D has the general following form:

  \verbatim
                        u1          u2         ...       un
                  --------------------------------------------
                  | ... ... ... | ... ... |           |  ... |          | . |
    cond 1   C1 = | ... ... ... | ... ... |    ...    |  ... |      D1= | . |
                  | ... ... ... | ... ... |           |  ... |          | . |
                  --------------------------------------------
                  | ... ... ... | ... ... |           |  ... |          | . |
    cond 2   C2 = | ... ... ... | ... ... |    ...    |  ... |      D2= | . |
                  | ... ... ... | ... ... |           |  ... |          | . |
                  --------------------------------------------
                   ...........................................

    where u1, u2, ..., un are unknowns and the rows are associated to a virtual dual unknowns, say c1, c2, ...
  \endverbatim

  In the simple case of a Dirichlet condition u = g on Sigma, the matrix is the identity matrix (one block)

  \verbatim
			   u|Sigma
		  	-------------
	   		|  1  0 ... |        | g1 |
	  C = |  0  1  0  |    D=  | .  |
			  | ... 0  1  |        | gp |
			  -------------
  \endverbatim

  In the case of a transmission condition u1-u2 = g on Sigma, the matrix is a two blocks matrix

  \verbatim
			    u1|Sigma   u2|Sigma
			  ------------------------
		  	|  1  0 ... | -1  0 .. |         | g1 |
		C = |  0  1  0  |  0 -1  0 |     D=  | .  |
			  | ... 0  1  | ..  0 -1 |         | gp |
			  ------------------------
  \endverbatim

  In the case of a mean constraint intg_Omega u = 0, the matrix should be a one row matrix:

  \verbatim
              u|Omega
			  -----------------
		C = | a1 a2  ... ap |       D =  | 0 |
			  -----------------
  \endverbatim

  The xlifepp::Constraints class handles
    - matrix_p: xlifepp::MatrixEntry pointer representing the constraint matrix
    - rhs_p: xlifepp::VectorEntry pointer representing the constraint right hand side (0 if homogeneous condition)
    - rhs_matrix_p: xlifepp::MatrixEntry pointer handling the rhs correction matrix (may be used in reduction process)
    - cdofsr_: row xlifepp::DofComponent's of matrix_p (constraint dofs, virtual cdofs)
    - cdofsc_: col xlifepp::DofComponent's of matrix_p (unknown cdofs involved in constraints)
    - elcdofs_: map of eliminated cdofs: cdof -> k (rank in cdofsc_)
    - recdofs_: map of reduced cdofs: cdof -> k (rank in cdofsc_)
    - conditions_: list of xlifepp::EssentialCondition's (analytic representation)
    - reduced: true means that the essential conditions have been reduced to a diagonal form
    - local: true if all conditions are local: only interaction with dofs in same element

  NOTE:  even if the unknowns are vector ones, the representation of constraints is always a scalar one

  The general process of dealing with a list of essential conditions is the following (see buildConstraints function)

  1) if one of conditions couples different unknowns, we create a global representation (one Constraints object mixing unknowns)
     if there is no coupling conditions we create one Constraints object for each unknown (see mergeConstraints function)

  2) Using QR algorithm, we reduce each constraints system. QR algorithm is able to detect redundant or conflicting constraints

  At the end of the process, each Constraints proposes finally a useful canonic constraints representation:
                        U_E = B - F*U_R
  where E is the set of eliminated dofs (dim=m), R is the set of reduced dofs (dim=n)
  B a scalar vector of size m (xlifepp::VectorEntry) and F a scalar matrix of size m x n.
  Note that F may be 0 (case of Dirichlet condition for instance)

  For developer: to take into account a new type of essential condition, the only job consists in writing
                  a new createXXX(const EssentialCondition&) that produces a matrix in col cs storage and a right hand side vector,
                  see createDirichlet for instance
*/

#ifndef CONSTRAINTS_HPP_
#define CONSTRAINTS_HPP_

#include "essentialConditions.h"
#include "largeMatrix.h"

namespace xlifepp
{

//============================================================================================================
/*!
   \class Constraints
   class handles the matrix representation of xlifepp::EssentialConditions
*/
//============================================================================================================

class Constraints
{
  protected:
    MatrixEntry* matrix_p;                     //!< matrix of essential condition's and reduced matrix at the end of the process
    VectorEntry* rhs_p;                        //!< right hand side vector of essential condition's

  public:
    std::vector<DofComponent> cdofsr_;         //!< row DofComponent's of matrix_p (constraint dofs, virtual cdofs)
    std::vector<DofComponent> cdofsc_;         //!< col DofComponent's of matrix_p (unknown cdofs involved in constraints)
    std::map<DofComponent, number_t> elcdofs_; //!< map of eliminated cdofs: cdof -> k (rank in cdofsc_)
    std::map<DofComponent, number_t> recdofs_; //!< map of reduced cdofs: cdof -> k (rank in cdofsc_)
    EssentialConditions conditions_;           //!< list of Essential conditions (analytic representation)
    bool reduced;                              //!< true means that the essential conditions have been reduced to a diagonal form
    bool local;                                //!< true if all conditions are local: only interaction with dofs in same element
    bool symmetric;                            //!< true if all constraints keep symmetry
    bool isId;                                 //!< true if matrix is Id matrix
    real_t redundancyTolerance_;               //!< tolerance factor used in reduction process (elimination of redundant constraints)

    //constructors
    Constraints(MatrixEntry* =nullptr, VectorEntry* =nullptr); //!< default constructor
    Constraints(const EssentialCondition&);        //!< construct Constraints system from a EssentialCondition
    Constraints(const Constraints&);               //!< copy constructor (full copy)
    Constraints& operator=(const Constraints&);    //!< assign operator (full copy)
    ~Constraints();                                //!< destructor (delete allocated pointers)
    void copy(const Constraints&);                 //!< copy function (full copy)
    void clear();                                  //!< clear all (reset)

    //accessors
    const MatrixEntry* matrixp() const {return matrix_p;} //!< accessor to matrix
    const VectorEntry* rhsp() const {return rhs_p;}       //!< accessor to rhs
    number_t numberOfConstraints() const {return cdofsr_.size();} //!< number of constraints

    //properties
    bool coupledUnknowns() const              //! true if one essential condition couples at least two unknowns
    {return conditions_.coupledUnknowns();}
    const Unknown* unknown() const            //! return unknown of constraint, 0 if more than one unknown
    {return conditions_.unknown();}
    std::set<const Unknown*> unknowns() const //! return unknowns of constraint
    {return conditions_.unknowns();}
    ValueType valueType() const;              //!< return value type of set of constraints (i.e involved real or complex)

    //building tools
    bool createDirichlet(const EssentialCondition&);        //!< create Dirichlet condition like
//    void createTransmission(const EssentialCondition&);     //!< create transmission condition (deprecated)
//    void createPeriodic(const EssentialCondition&);         //!< create periodic condition (deprecated)
    void createLf(const EssentialCondition&);               //!< create from linear form condition
    void createNodal(const EssentialCondition&);            //!< create general condition (nodal)
    void createArgyris(const EssentialCondition& ec);       //!< create u=0 constraints for Argyris
    MatrixEntry* constraintsMatrix(const OperatorOnUnknown&, const GeomDomain*, Space*, const complex_t&, const std::vector<Point>&,
                                   const Function*, std::vector<DofComponent>&); //!< create submatrix of a constraint
    void concatenateMatrix(MatrixEntry&, std::vector<DofComponent>&, const MatrixEntry&, const std::vector<DofComponent>&); //!< concatenate 2 constraints matrix
    void reduceConstraints(real_t aszero = 10.*theEpsilon); //!< reduce constraints matrix to a set of reduced constraints
    template <typename T>
    void buildRhs(const Function*, const std::vector<Point>&, const std::vector<Vector<real_t> >&,const T&);  //!< tool to build rhs constraint

    //reduction tools
    void pseudoColReduction(MatrixEntry*, std::vector<DofComponent>&,
                              std::vector<DofComponent>&, MatrixEntry*&);   //!< pseudo reduction of columns
    void pseudoRowReduction(MatrixEntry*, std::vector<DofComponent>&,
                              std::vector<DofComponent>&, const complex_t&, const complex_t&, bool);//!< pseudo reduction of rows
    void penalizationReduction(MatrixEntry*, std::vector<DofComponent>&,
                              std::vector<DofComponent>&, const complex_t&);//!< penalization reduction
    void extractCdofs(const std::vector<DofComponent>&, std::map<DofComponent, number_t>&,
                               std::map<DofComponent, number_t>&, bool useDual=false) const; //!< extract eliminated and reduced cdofs from list of cdofs

    void print(std::ostream&) const; //!< print utillity
    void print(PrintStream& os) {print(os.currentStream());}
    friend std::ostream& operator<<(std::ostream&, const Constraints&);
};

//extern functions
std::ostream& operator<<(std::ostream&, const Constraints&); //!< print operator
std::map<const Unknown*, Constraints*> buildConstraints(const EssentialConditions&);  //!< build Constraints from EssentialConditions
std::map<const Unknown*, Constraints*> mergeConstraints(std::vector<Constraints*>&);  //!< merge constraints
void appliedRhsCorrectorTo(VectorEntry*, const std::vector<DofComponent>&,
                           MatrixEntry*, const Constraints*, const Constraints*,
                           const ReductionMethod&);                                   //!< right hand side constraints correction
void applyEssentialConditions(VectorEntry&, const std::vector<DofComponent>&,
                              const Constraints&);                                    //!< apply constraints to a vector
void extendStorage(MatrixEntry*, std::vector<DofComponent>&, std::vector<DofComponent>&,
                   const Constraints*, const Constraints*, bool keepSymmetry=false,
                   bool doRow=true, bool doCol=true, bool doDiag=true);               //!< extend matrix storage when constraints are not local and have column/row combination

void reduceMatrix(MatrixEntry*&, std::vector<DofComponent>&, std::vector<DofComponent>&,
                  std::vector<DofComponent>&, std::vector<DofComponent>&,
                  const Constraints*, const Constraints*);                            //!< full reduction of a pseudo-reduced matrix, assuming scalar matrix entries

string_t unknownEcName(const EssentialCondition&);      //!< to create an unknown name associated to an essential condition


// evaluate right hand side of a constraint at some nodes
// fun: pointer to function
// nodes: list of point where to evaluate the function
// ns: optionnal list of normals (empty if not required)
// valt: fake argument to specify the value type
template <typename T>
void Constraints::buildRhs(const Function *fun, const std::vector<Point>& nodes, const std::vector<Vector<real_t> >& ns, const T& valt)
{
  theThreadData.reset();        //reset threadData of current thread
  dimen_t df=fun->dims().first;
  std::vector<Point>::const_iterator itp;
  number_t i=1;
  bool nor = ns.size()>0;
  if(df==1)  //scalar case
  {
    T v;
    for(itp=nodes.begin(); itp!=nodes.end(); ++itp, ++i)
    {
        if(nor) setNx(const_cast<Vector<real_t>*>(&ns[i-1]));
        rhs_p->setEntry(i,(*fun)(*itp,v));
    }
  }
  else
  {
    std::vector<T> v(df);
    number_t j=1;
    for(itp=nodes.begin(); itp!=nodes.end(); ++itp, ++i)
    {
      if(nor) setNx(const_cast<Vector<real_t>*>(&ns[i-1]));
      (*fun)(*itp,v);
      for(number_t k=0;k<df; ++k, ++j) rhs_p->setEntry(j,v[k]);
    }
  }
}

//============================================================================================================
/*!
    \class SetOfConstraints
    class handles a set of Constraints indexed by Unknown pointer

    a zero Unknown pointer means a global constraints:  coupling unknowns constraints
    in that case, there is only one global constraints system in SetOfConstraints
    if not the case, to each non zero Unknown pointer is associated a constraints system

    Note : SetOfConstraints is constructed from EssentialConditions (also stored)  and it is deep copied to Term
           so it is not shared except the virtual dual space/unknown that are not duplicated
*/
//============================================================================================================
class SetOfConstraints : public std::map<const Unknown*, Constraints*>
{
  public:
    std::set<Unknown*> rowUnknowns;                                      //!< set of row unknowns (ghost)
    //constructors, destructor, assignment, ...
    SetOfConstraints(const Unknown* u, Constraints* c=nullptr)                 //! constructor from a constraints pointer (no full copy of constraints !)
    {insert(std::make_pair(u,c));}
    SetOfConstraints(const std::map<const Unknown*, Constraints*>& mc)   //! constructor from constraints pointers (no full copy of constraints !)
    : std::map<const Unknown*, Constraints*>(mc){setRowUnknowns();}
    SetOfConstraints(const EssentialConditions& ecs)                     //! constructor from EssentialConditions
    : std::map<const Unknown*, Constraints*>(buildConstraints(ecs))
      {setRowUnknowns();}
    void setRowUnknowns();                                               //!< set rowUnknowns list
    SetOfConstraints(const SetOfConstraints&);                           //!< copy constructor with full copy of constraints
    SetOfConstraints& operator = (const SetOfConstraints&);              //!< assign operator with full copy of constraints
    ~SetOfConstraints();                                                 //!< destructor with deallocation of constraints pointers
    void clear();                                                        //!< deallocate constraints pointer and reset the map
    void incEcShare() const;                                             //!< increment row unknowns nbEcShare
    void decEcShare() const;                                             //!< decrement row unknowns nbEcShare

  private:
    void copy(const SetOfConstraints&);                 //!< full copy

  public:
    //accessors and properties
    Constraints* operator()(const Unknown*) const;      //!< direct access to constraints linked to unknown
    bool isGlobal() const;                              //!< return true if SetOfConstraints has a unique global constraints
    bool isReduced() const;                             //!< return true if all constraints in SetOfConstraints have been already reduced
    ValueType valueType() const;                        //!< return value type of set of constraints (i.e involved real or complex)

    void print(std::ostream&) const; //!< print utility
    void print(PrintStream& os) {print(os.currentStream());}
    friend std::ostream& operator<<(std::ostream&, const SetOfConstraints&);
};

std::ostream& operator<<(std::ostream&, const SetOfConstraints&); //!< print operator

} // end of namespace xlifepp

#endif // CONSTRAINTS_HPP
