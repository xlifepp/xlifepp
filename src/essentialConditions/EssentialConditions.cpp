/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file EssentialConditions.cpp
  \author E. Lunéville
  \since 16 jan 2014
  \date  16 jan 2014

  \brief Implementation of xlifepp::EssentialConditions class member functions and related utilities
*/

#include "EssentialConditions.hpp"
#include "Constraints.hpp"
#include "utils.h"

namespace xlifepp
{

// constructors
EssentialConditions::EssentialConditions(const EssentialCondition& ec)
{ constraints_=nullptr; push_back(ec);redundancyTolerance_=ec.redundancyTolerance();}

EssentialConditions::EssentialConditions(const EssentialCondition& ec1, const EssentialCondition& ec2)
{
  push_back(ec1);
  push_back(ec2);
  constraints_=nullptr;
  redundancyTolerance_=std::max(ec1.redundancyTolerance(),ec2.redundancyTolerance());
}

EssentialConditions::EssentialConditions(const EssentialConditions& ecs)
{
  constraints_=nullptr;
  if(ecs.size()==0) return;
  assign(ecs.begin(),ecs.end());
  constraints_= nullptr;
  if(ecs.constraints_!=nullptr) constraints_=new SetOfConstraints(*ecs.constraints_);  //deep copy
  redundancyTolerance_=ecs.redundancyTolerance();
}

EssentialConditions& EssentialConditions::operator = (const EssentialConditions& ecs)
{
  if(&ecs==this) return *this;
  clear();
  if(ecs.size()==0) return *this;
  assign(ecs.begin(),ecs.end());
  if(constraints_!=nullptr) {delete constraints_; constraints_=nullptr;}
  if(ecs.constraints_!=nullptr) constraints_=new SetOfConstraints(*ecs.constraints_);  //deep copy
  redundancyTolerance_=ecs.redundancyTolerance();
  return *this;
}

EssentialConditions::~EssentialConditions()
{
    if(constraints_!=nullptr) delete constraints_;
}

//pseudo constructor
EssentialConditions operator & (const EssentialCondition& ec1, const EssentialCondition& ec2)   // ecs <= ec1 & ec2
{return EssentialConditions(ec1,ec2);}

//insertion
EssentialConditions operator & (const EssentialCondition& ec, const EssentialConditions& ecs)   // ecs <= ec & ecs
{
  EssentialConditions ecr(ecs);
  ecr.push_front(ec);
  ecr.redundancyTolerance(std::max(ecs.redundancyTolerance(),ec.redundancyTolerance()));
  return ecr;
}

EssentialConditions operator & (const EssentialConditions& ecs, const EssentialCondition& ec)   // ecs <= ecs & ec
{
  EssentialConditions ecr(ecs);
  ecr.push_back(ec);
  ecr.redundancyTolerance(std::max(ecs.redundancyTolerance(),ec.redundancyTolerance()));
  return ecr;
}

EssentialConditions operator & (const EssentialConditions& ecs1, const EssentialConditions& ecs2)   // ecs <= ecs & ecs
{
  EssentialConditions ecr(ecs1);
  ecr.insert(ecr.end(),ecs2.begin(),ecs2.end());
  ecr.redundancyTolerance(std::max(ecs1.redundancyTolerance(),ecs2.redundancyTolerance()));
  return ecr;
}

real_t& EssentialConditions::redundancyTolerance(real_t tol)
{
  std::list<EssentialCondition>::iterator it=begin();
  for(; it!=end(); it++)
    it->redundancyTolerance(tol);
  redundancyTolerance_=tol;
  return redundancyTolerance_;
}

//true if one essential condition couples at least two unknowns
bool EssentialConditions::coupledUnknowns() const
{
  std::list<EssentialCondition>::const_iterator it=begin();
  for(; it!=end(); it++)
    if(!it->isSingleUnknown()) return true;
  return false;
}

// return unknowns involved in constraint
std::set<const Unknown*> EssentialConditions::unknowns() const
{
  std::list<EssentialCondition>::const_iterator it=begin();
  std::set<const Unknown*> uss, us;
  for(; it!=end(); it++)
  {
    us=it->unknowns();
    uss.insert(us.begin(),us.end());
  }
  return uss;
}

// return unknown involved in constraint, 0 if more than one
const Unknown* EssentialConditions::unknown() const
{
  std::set<const Unknown*> uss=unknowns();
  if(uss.size()==1) return *uss.begin();
  return nullptr;
}

void EssentialConditions::print(std::ostream& os) const
{
  if(size()==0) return;
  //os<<"list of Essential conditions:"<<eol;
  std::list<EssentialCondition>::const_iterator it=begin();
  for(; it!=end(); it++) os<<"    "<<*it<<eol;
  os<<"    redundancy tolerance = "<<redundancyTolerance_<<eol;
}

std::ostream& operator << (std::ostream& os, const EssentialConditions &bc)
{
  bc.print(os);
  return os;
}

} // end of namespace xlifepp
