/*
XLiFE++ is an extended library of finite elements written in C++
    Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LcKernelOperatorOnUnknowns.hpp
  \author E. Lunéville
  \since 19 jul 2018
  \date  19 jul 2018

  \brief Definition of the xlifepp::LcKernelOperatorOnUnknowns class

  xlifepp::LcKernelOperatorOnUnknowns class handles linear combination of KernelOperatorOnUnknowns

                                    sum ( a_i opk_i(u,k,v) )

  KernelOperatorOnUnknowns must have the same unknown u and the same test function v !!!
  It inherits from std::vector<OpkuvValPair> where OpkuvValPair is an alias of std::pair<xlifepp::KernelOperatorOnUnknowns*, xlifepp::complex_t>
 */

#ifndef LC_KERNEL_OPERATOR_ON_UNKNOWNS_HPP
#define LC_KERNEL_OPERATOR_ON_UNKNOWNS_HPP

#include "config.h"
#include "KernelOperatorOnUnknowns.hpp"

namespace xlifepp
{

// Note: KernelOperatorOnUnknowns are always copy of users KernelOperatorOnUnknowns

//@{
//useful aliases for LcKernelOperatorOnUnknowns class
typedef std::pair<xlifepp::KernelOperatorOnUnknowns*, xlifepp::complex_t> OpkuvValPair;
typedef std::vector<OpkuvValPair>::iterator it_opkuvval;
typedef std::vector<OpkuvValPair>::const_iterator cit_opkuvval;
//@}
/*!
   \class LcKernelOperatorOnUnknowns
   describes the linear combination of KernelOperatorOnUnknowns
*/

class LcKernelOperatorOnUnknowns : public std::vector<OpkuvValPair>
{
public:
    string_t name;                                                                                         //!< optional name
    LcKernelOperatorOnUnknowns() {name="";}                                                                //!< default constructor
    LcKernelOperatorOnUnknowns(const KernelOperatorOnUnknowns&, const real_t& = 1., const string_t& na="");//!< basic constructor
    LcKernelOperatorOnUnknowns(const KernelOperatorOnUnknowns&, const complex_t&, const string_t& na="");  //!< basic constructor
    LcKernelOperatorOnUnknowns(const LcKernelOperatorOnUnknowns&);                  //!< copy constructor
    ~LcKernelOperatorOnUnknowns();                                                  //!< destructor

    LcKernelOperatorOnUnknowns& operator =(const LcKernelOperatorOnUnknowns&);      //!< assignment
    void clear();                                                                   //!< deallocate KernelOperatorOnUnknowns pointers
    void copy(const LcKernelOperatorOnUnknowns& lc);                                //!< full copy of OperatorOnUnknown pointers

    //utilities
    void insert(const KernelOperatorOnUnknowns&);                      //!< insert op(u) on D in list
    void insert(const real_t&, const KernelOperatorOnUnknowns&);       //!< insert a*op(u) on D in list
    void insert(const complex_t&, const KernelOperatorOnUnknowns&);    //!< insert a*op(u) on Din list

    complex_t coefficient() const;                            //!< return coefficient of first term if a single term combination
    std::vector<complex_t> coefficients() const;              //!< return vector of coefficients involved in combination
    uvPair unknowns() const;                                  //!< return unknowns u,v
    const Kernel* kernel() const;                             //!< return kernel of first term if a single term combination, else 0
    std::vector<const Kernel*> kernels() const;               //!< list of kernels involved in linear combination

    //algebraic operations
    LcKernelOperatorOnUnknowns& operator +=(const KernelOperatorOnUnknowns&);   //!< lckop += kop
    LcKernelOperatorOnUnknowns& operator +=(const LcKernelOperatorOnUnknowns&); //!< lckop += lckop
    LcKernelOperatorOnUnknowns& operator -=(const KernelOperatorOnUnknowns&);   //!< lckop -= kop
    LcKernelOperatorOnUnknowns& operator -=(const LcKernelOperatorOnUnknowns&); //!< lckop -= lckop
    LcKernelOperatorOnUnknowns& operator *=(const real_t&);                     //!< lckop *= r
    LcKernelOperatorOnUnknowns& operator *=(const complex_t&);                  //!< lckop *= c
    LcKernelOperatorOnUnknowns& operator /=(const real_t&);                     //!< lckop /= r
    LcKernelOperatorOnUnknowns& operator /=(const complex_t&);                  //!< lckop /= c

    template<typename K>
    void eval(const Point& x, const Point& y, const ShapeValues& Syu, const ShapeValues& Sxv, K alpha,
              const Vector<real_t>* nx, const Vector<real_t>* ny, dimen_t dimf_u, dimen_t dimf_v,
              ExtensionData* extdata, Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_k,
              Matrix<K>& res) const;                //!< evaluate LcKernelOperatorOnUnknowns at (x,y)

    // print utilities
    string_t asString() const;                //!< interpret as string for print purpose
    void print(std::ostream&) const;          //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());} //!< print utility
    friend std::ostream& operator<<(std::ostream&, const LcKernelOperatorOnUnknowns&);
};

std::ostream& operator<<(std::ostream&, const LcKernelOperatorOnUnknowns&); //!< print operator

//@{
//! algebraic operations
LcKernelOperatorOnUnknowns operator +(const LcKernelOperatorOnUnknowns&);
LcKernelOperatorOnUnknowns operator -(const LcKernelOperatorOnUnknowns&);
LcKernelOperatorOnUnknowns operator +(const LcKernelOperatorOnUnknowns&, const KernelOperatorOnUnknowns&);
LcKernelOperatorOnUnknowns operator -(const LcKernelOperatorOnUnknowns&, const KernelOperatorOnUnknowns&);
LcKernelOperatorOnUnknowns operator +(const KernelOperatorOnUnknowns&, const LcKernelOperatorOnUnknowns&);
LcKernelOperatorOnUnknowns operator -(const KernelOperatorOnUnknowns&, const LcKernelOperatorOnUnknowns&);
LcKernelOperatorOnUnknowns operator +(const LcKernelOperatorOnUnknowns&, const LcKernelOperatorOnUnknowns&);
LcKernelOperatorOnUnknowns operator -(const LcKernelOperatorOnUnknowns&, const LcKernelOperatorOnUnknowns&);
LcKernelOperatorOnUnknowns operator *(const LcKernelOperatorOnUnknowns&, const real_t&);
LcKernelOperatorOnUnknowns operator *(const LcKernelOperatorOnUnknowns&, const complex_t&);
LcKernelOperatorOnUnknowns operator *(const real_t&, const LcKernelOperatorOnUnknowns&);
LcKernelOperatorOnUnknowns operator *(const complex_t&, const LcKernelOperatorOnUnknowns&);
LcKernelOperatorOnUnknowns operator /(const LcKernelOperatorOnUnknowns&, const real_t&);
LcKernelOperatorOnUnknowns operator /(const LcKernelOperatorOnUnknowns&, const complex_t&);
LcKernelOperatorOnUnknowns operator +(const KernelOperatorOnUnknowns&, const KernelOperatorOnUnknowns&);
LcKernelOperatorOnUnknowns operator -(const KernelOperatorOnUnknowns&, const KernelOperatorOnUnknowns&);
//@}

/*evaluate LcKernelOperatorOnUnknowns at couple x,y from shapevalues at point x and y
    res(i,j) += alpha * sum_l val_opu_l[j](y) aopu_l K_l(x,y) aopv_l val_opv_l[i](x)  (row unknown v, column unknown u)

    x, y: couple of points where to evaluate the kernel
    Sxu, Sxv: shape values for u/v
    alpha: coefficient involved in
    nx, ny: pointer to normal vectors
    dimf_u, dimf_v: dimension of shape functions (u and v)
    extdata: pointer to extension data (no extension if 0)
    val_opu, val_opv: vector to store temporary value of operator on u/v, has to be sized outside
    val_k: vector to store temporary value of kernel, has to be sized outside
*/

template<typename K>
void LcKernelOperatorOnUnknowns::eval(const Point& x, const Point& y, const ShapeValues& Syu, const ShapeValues& Sxv, K alpha,
                                      const Vector<real_t>* nx, const Vector<real_t>* ny, dimen_t dimf_u, dimen_t dimf_v,
                                      ExtensionData* extdata, Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_k,
                                      Matrix<K>& res) const
{
    cit_opkuvval ito = begin();
    for(;ito!=end();++ito)
    {
        const KernelOperatorOnUnknowns *kopus=ito->first;
        const OperatorOnUnknown& opu=kopus->opu();
        const OperatorOnUnknown& opv=kopus->opv();
        kopus->evalF(x, y, Syu, Sxv, alpha*ito->second, nx, ny, dimf_u, dimf_v,
                     opu.isId(), opv.isId(), opu.hasFun(), opv.hasFun(), kopus->opker().struct_==_scalar,
                     extdata, val_opu, val_opv, val_k, res);
    }

}


} // end of namespace xlifepp

#endif /* LC_OPERATOR_ON_UNKNOWN_HPP */

