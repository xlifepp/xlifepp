/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon;
  Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LcOperatorOnUnknowns.cpp
  \author E. Peillon
  \since 1 jan 2022
  \date  1 jan 2022

  \brief Implementation of xlifepp::LcOperatorOnUnknowns class members and
  related functions
*/

#include "LcOperatorOnUnknowns.hpp"
#include "utils.h"

namespace xlifepp
{
// constructors/destructor
LcOperatorOnUnknowns::LcOperatorOnUnknowns(const OperatorOnUnknowns &opus, const real_t &a)
{
  push_back(OpusValPair(new OperatorOnUnknowns(opus), complex_t(a)));
}

LcOperatorOnUnknowns::LcOperatorOnUnknowns(const OperatorOnUnknowns &opus, const complex_t &a)
{
  push_back(OpusValPair(new OperatorOnUnknowns(opus), a));
}

LcOperatorOnUnknowns::LcOperatorOnUnknowns(const LcOperatorOnUnknowns &lcopus) // copy constructor (full copy)
{
  if (lcopus.size() == 0)
    return;
  copy(lcopus);
}

LcOperatorOnUnknowns::~LcOperatorOnUnknowns()
{
  clear();
}

LcOperatorOnUnknowns &LcOperatorOnUnknowns::operator=(const LcOperatorOnUnknowns &lcopus)
{
  if (&lcopus == this)
    return *this;
  clear();
  copy(lcopus);
  return *this;
}

// deallocate OperatorOnUnknowns pointers
void LcOperatorOnUnknowns::clear()
{
  for (it_opusval it = begin(); it != end(); it++)
  {
    if (it->first != nullptr)
      delete it->first;
  }
  std::vector<OpusValPair>::clear();
}

// full copy of OperatorOnUnknown pointers
void LcOperatorOnUnknowns::copy(const LcOperatorOnUnknowns &lcopus)
{
  resize(lcopus.size());
  cit_opusval itlc = lcopus.begin();
  it_opusval it = begin();
  for (; itlc != lcopus.end(); itlc++, it++)
  {
    OperatorOnUnknowns *op = new OperatorOnUnknowns(*itlc->first); // full copy
    *it = OpusValPair(op, itlc->second);
  }
}

// utilities
void LcOperatorOnUnknowns::insert(const OperatorOnUnknowns &opus)
{
  insert(1., opus);
}

void LcOperatorOnUnknowns::insert(const real_t &a, const OperatorOnUnknowns &opus)
{
  push_back(OpusValPair(new OperatorOnUnknowns(opus), complex_t(a)));
}

void LcOperatorOnUnknowns::insert(const complex_t &a,
                                  const OperatorOnUnknowns &opus)
{
  push_back(OpusValPair(new OperatorOnUnknowns(opus), a));
}

bool LcOperatorOnUnknowns::isSingleUVPair() const
{
  if (size() < 2)
    return true;
  cit_opusval it = begin();
  const Unknown *u = it->first->opu_p()->unknown();
  const Unknown *v = it->first->opv_p()->unknown();
  it++;
  for (; it != end(); it++)
    if (it->first->opu_p()->unknown() != u || it->first->opv_p()->unknown() != v)
      return false;
  return true;
}

const OperatorOnUnknown* LcOperatorOnUnknowns::opu(number_t i) const
{
  if (size() == 0)
    return nullptr;
  if (i > size())
  {
    where("LcOperatorOnUnknowns::opu(Number)");
    error("index_out_of_range", "i", 1, size());
  }
  return (begin() + i - 1)->first->opu_p();
}

const OperatorOnUnknown* LcOperatorOnUnknowns::opv(number_t i) const
{
  if (size() == 0)
    return nullptr;
  if (i > size())
  {
    where("LcOperatorOnUnknowns::opu(Number)");
    error("index_out_of_range", "i", 1, size());
  }
  return (begin() + i - 1)->first->opv_p();
}

const Unknown* LcOperatorOnUnknowns::unknownu(number_t i) const
{
  if (size() == 0)
    return nullptr;
  if (i > size())
  {
    where("LcOperatorOnUnknowns::unknownu(Number)");
    error("index_out_of_range", "i", 1, size());
  }
  return (begin() + i - 1)->first->opu_p()->unknown();
}

const Unknown* LcOperatorOnUnknowns::unknownv(number_t i) const
{
  if (size() == 0)
    return nullptr;
  if (i > size())
  {
    where("LcOperatorOnUnknowns::unknownv(Number)");
    error("index_out_of_range", "i", 1, size());
  }
  return (begin() + i - 1)->first->opv_p()->unknown();
}

complex_t LcOperatorOnUnknowns::coefficient(number_t i) const
{
  if (size() == 0)
    return complex_t(0.);
  if (i > size())
  {
    where("LcOperatorOnUnknowns::coefficient(Number)");
    error("index_out_of_range", "i", 1, size());
  }
  return (begin() + i - 1)->second;
}

std::vector<complex_t> LcOperatorOnUnknowns::coefficients() const
{
  std::vector<complex_t> cs;
  if (size() == 0)
    return cs;
  cs.resize(size());
  cit_opusval it = begin();
  std::vector<complex_t>::iterator itcs = cs.begin();
  for (; it != end(); it++, itcs++)
    *itcs = it->second;
  return cs;
}

// algebraic operations
LcOperatorOnUnknowns &LcOperatorOnUnknowns::operator+=(const OperatorOnUnknowns &opus)
{
  insert(opus);
  return *this;
}

LcOperatorOnUnknowns &LcOperatorOnUnknowns::operator+=(const LcOperatorOnUnknowns &lcopus)
{
  if (this == &lcopus)
  {
    (*this) *= 2;
    return *this;
  }
  cit_opusval itlc = lcopus.begin();
  for (; itlc != lcopus.end(); itlc++)
    insert(itlc->second, *itlc->first);
  return *this;
}

LcOperatorOnUnknowns &LcOperatorOnUnknowns::operator-=(const OperatorOnUnknowns &opus)
{
  insert(-1, opus);
  return *this;
}

LcOperatorOnUnknowns &LcOperatorOnUnknowns::operator-=(const LcOperatorOnUnknowns &lcopus)
{
  if (this == &lcopus)
  {
    (*this) *= 2;
    return *this;
  }
  cit_opusval itlc = lcopus.begin();
  for (; itlc != lcopus.end(); itlc++)
    insert(-itlc->second, *itlc->first);
  return *this;
}

LcOperatorOnUnknowns &LcOperatorOnUnknowns::operator*=(const real_t &a)
{
  for (it_opusval it = begin(); it != end(); it++)
    it->second *= a;
  return *this;
}

LcOperatorOnUnknowns &LcOperatorOnUnknowns::operator*=(const complex_t &a)
{
  for (it_opusval it = begin(); it != end(); it++)
    it->second *= a;
  return *this;
}

LcOperatorOnUnknowns &LcOperatorOnUnknowns::operator/=(const real_t &a)
{
  for (it_opusval it = begin(); it != end(); it++)
    it->second /= a;
  return *this;
}

LcOperatorOnUnknowns &LcOperatorOnUnknowns::operator/=(const complex_t &a)
{
  for (it_opusval it = begin(); it != end(); it++)
    it->second /= a;
  return *this;
}

// print utility
void LcOperatorOnUnknowns::print(std::ostream &os) const
{
  cit_opusval it = begin();
  if (it == end())
  {
    os << "void LcOperatorOnUnknowns";
    return;
  }
  os << "LcOperatorOnUnknowns : \n";
  complex_t a = it->second;
  if (a.imag() != 0)
    os << a << " * ";
  else
  {
    real_t ra = a.real();
    if (std::abs(ra) == 1)
    {
      if (ra == -1)
        os << " - ";
    }
    else
    {
      if (ra > 0)
        os << ra << " * ";
      else
        os << " - " << std::abs(ra) << " * ";
    }
  }
  it->first->print(os);
  it++;
  for (; it != end(); it++)
  {
    complex_t a = it->second;
    if (a.imag() != 0)
      os << " + " << a << " * ";
    else
    {
      real_t ra = a.real();
      if (std::abs(ra) == 1)
      {
        if (ra == 1)
          os << " + ";
        else
          os << " - ";
      }
      else
      {
        if (ra > 0)
          os << " + " << ra << " * ";
        else
          os << " - " << std::abs(ra) << " * ";
      }
    }
    it->first->print(os);
  }
}

std::ostream &operator<<(std::ostream &os, const LcOperatorOnUnknowns &lc)
{
  lc.print(os);
  return os;
}

//extern LcTermFunction functions

LcOperatorOnUnknowns operator+(const LcOperatorOnUnknowns &lc) // unary +
{
  LcOperatorOnUnknowns lcr(lc);
  return lcr;
}

LcOperatorOnUnknowns operator-(const LcOperatorOnUnknowns &lc) // unary -
{
  LcOperatorOnUnknowns lcr(lc);
  return lcr *= -1;
}

LcOperatorOnUnknowns operator+(const LcOperatorOnUnknowns &lc, const OperatorOnUnknowns &opus)
{
  LcOperatorOnUnknowns lcr(lc);
  return lcr += opus;
}

LcOperatorOnUnknowns operator-(const LcOperatorOnUnknowns &lc, const OperatorOnUnknowns &opus)
{
  LcOperatorOnUnknowns lcr(lc);
  return lcr -= opus;
}

LcOperatorOnUnknowns operator+(const OperatorOnUnknowns &opus, const LcOperatorOnUnknowns &lc)
{
  LcOperatorOnUnknowns lcr(lc);
  return lcr += opus;
}

LcOperatorOnUnknowns operator-(const OperatorOnUnknowns &opus, const LcOperatorOnUnknowns &lc)
{
  LcOperatorOnUnknowns lcr(opus);
  return lcr -= lc;
}

LcOperatorOnUnknowns operator+(const LcOperatorOnUnknowns &lc1, const LcOperatorOnUnknowns &lc2)
{
  LcOperatorOnUnknowns lcr(lc1);
  return lcr += lc2;
}

LcOperatorOnUnknowns operator-(const LcOperatorOnUnknowns &lc1, const LcOperatorOnUnknowns &lc2)
{
  LcOperatorOnUnknowns lcr(lc1);
  return lcr -= lc2;
}

LcOperatorOnUnknowns operator*(const LcOperatorOnUnknowns &lc, const real_t &a)
{
  LcOperatorOnUnknowns lcr(lc);
  return lcr *= a;
}

LcOperatorOnUnknowns operator*(const LcOperatorOnUnknowns &lc, const complex_t &a)
{
  LcOperatorOnUnknowns lcr(lc);
  return lcr *= a;
}

LcOperatorOnUnknowns operator*(const real_t &a, const LcOperatorOnUnknowns &lc)
{
  LcOperatorOnUnknowns lcr(lc);
  return lcr *= a;
}

LcOperatorOnUnknowns operator*(const complex_t &a, const LcOperatorOnUnknowns &lc)
{
  LcOperatorOnUnknowns lcr(lc);
  return lcr *= a;
}

LcOperatorOnUnknowns operator/(const LcOperatorOnUnknowns &lc, const real_t &a)
{
  LcOperatorOnUnknowns lcr(lc);
  return lcr /= a;
}

LcOperatorOnUnknowns operator/(const LcOperatorOnUnknowns &lc, const complex_t &a)
{
  LcOperatorOnUnknowns lcr(lc);
  return lcr /= a;
}

LcOperatorOnUnknowns operator+(const OperatorOnUnknowns &opus1, const OperatorOnUnknowns &opus2)
{
  LcOperatorOnUnknowns lcr(opus1);
  return lcr += opus2;
}

LcOperatorOnUnknowns operator-(const OperatorOnUnknowns &opus1, const OperatorOnUnknowns &opus2)
{
  LcOperatorOnUnknowns lcr(opus1);
  return lcr -= opus2;
}

LcOperatorOnUnknowns operator*(const LcOperatorOnUnknown &lcopu, const OperatorOnUnknown &opv)
{
  cit_opuval it = lcopu.begin();
  LcOperatorOnUnknowns lcopus;
  for (; it != lcopu.end(); it++)
  {
    if (!checkConsistancy(*it->first, _product, opv))
    {
      error("opu_badopus", words("algop", _product), "operator *");
    }
    lcopus += it->second * OperatorOnUnknowns(*it->first, opv, _product);
  }
  return lcopus;
}

LcOperatorOnUnknowns operator%(const LcOperatorOnUnknown &lcopu, const OperatorOnUnknown &opv)
{
  cit_opuval it = lcopu.begin();
  LcOperatorOnUnknowns lcopus;
  for (; it != lcopu.end(); it++)
  {
    if (!checkConsistancy(*it->first, _contractedProduct, opv))
    {
      error("opu_badopus", words("algop", _contractedProduct), "operator %");
    }
    lcopus += it->second * OperatorOnUnknowns(*it->first, opv, _contractedProduct);
  }
  return lcopus;
}

LcOperatorOnUnknowns operator|(const LcOperatorOnUnknown &lcopu, const OperatorOnUnknown &opv)
{
  cit_opuval it = lcopu.begin();
  LcOperatorOnUnknowns lcopus;
  for (; it != lcopu.end(); it++)
  {
    if (!checkConsistancy(*it->first, _innerProduct, opv))
    {
      error("opu_badopus", words("algop", _innerProduct), "operator |");
    }
    lcopus += it->second * OperatorOnUnknowns(*it->first, opv, _innerProduct);
  }
  return lcopus;
}

LcOperatorOnUnknowns operator^(const LcOperatorOnUnknown &lcopu, const OperatorOnUnknown &opv)
{
  cit_opuval it = lcopu.begin();
  LcOperatorOnUnknowns lcopus;
  for (; it != lcopu.end(); it++)
  {
    if (!checkConsistancy(*it->first, _crossProduct, opv))
    {
      error("opu_badopus", words("algop", _crossProduct), "operator ^");
    }
    lcopus += it->second * OperatorOnUnknowns(*it->first, opv, _crossProduct);
  }
  return lcopus;
}

LcOperatorOnUnknowns operator*(const OperatorOnUnknown &opu, const LcOperatorOnUnknown &lcopv)
{
  cit_opuval it = lcopv.begin();
  LcOperatorOnUnknowns lcopus;
  for (; it != lcopv.end(); it++)
  {
    if (!checkConsistancy(opu, _product, *it->first))
    {
      error("opu_badopus", words("algop", _product), "operator *");
    }
    lcopus += it->second * OperatorOnUnknowns(opu, *it->first, _product);
  }
  return lcopus;
}

LcOperatorOnUnknowns operator%(const OperatorOnUnknown &opu, const LcOperatorOnUnknown &lcopv)
{
  cit_opuval it = lcopv.begin();
  LcOperatorOnUnknowns lcopus;
  for (; it != lcopv.end(); it++)
  {
    if (!checkConsistancy(opu, _contractedProduct, *it->first))
    {
      error("opu_badopus", words("algop", _contractedProduct), "operator %");
    }
    lcopus += it->second * OperatorOnUnknowns(opu, *it->first, _contractedProduct);
  }
  return lcopus;
}

LcOperatorOnUnknowns operator|(const OperatorOnUnknown &opu, const LcOperatorOnUnknown &lcopv)
{
  cit_opuval it = lcopv.begin();
  LcOperatorOnUnknowns lcopus;
  for (; it != lcopv.end(); it++)
  {
    if (!checkConsistancy(opu, _innerProduct, *it->first))
    {
      error("opu_badopus", words("algop", _innerProduct), "operator |");
    }
    lcopus += it->second * OperatorOnUnknowns(opu, *it->first, _innerProduct);
  }
  return lcopus;
}

LcOperatorOnUnknowns operator^(const OperatorOnUnknown &opu, const LcOperatorOnUnknown &lcopv)
{
  cit_opuval it = lcopv.begin();
  LcOperatorOnUnknowns lcopus;
  for (; it != lcopv.end(); it++)
  {
    if (!checkConsistancy(opu, _crossProduct, *it->first))
    {
      error("opu_badopus", words("algop", _crossProduct), "operator ^");
    }
    lcopus += it->second * OperatorOnUnknowns(opu, *it->first, _crossProduct);
  }
  return lcopus;
}

LcOperatorOnUnknowns operator*(const LcOperatorOnUnknown &lcopu, const Unknown &v)
{
  return lcopu * id(v);
}

LcOperatorOnUnknowns operator%(const LcOperatorOnUnknown &lcopu, const Unknown &v)
{
  return lcopu % id(v);
}

LcOperatorOnUnknowns operator|(const LcOperatorOnUnknown &lcopu, const Unknown &v)
{
  return lcopu | id(v);
}

LcOperatorOnUnknowns operator^(const LcOperatorOnUnknown &lcopu, const Unknown &v)
{
  return lcopu ^ id(v);
}

LcOperatorOnUnknowns operator*(const Unknown &u, const LcOperatorOnUnknown &lcopv)
{
  return id(u) * lcopv;
}
LcOperatorOnUnknowns operator%(const Unknown &u, const LcOperatorOnUnknown &lcopv)
{
  return id(u) % lcopv;
}
LcOperatorOnUnknowns operator|(const Unknown &u, const LcOperatorOnUnknown &lcopv)
{
  return id(u) | lcopv;
}
LcOperatorOnUnknowns operator^(const Unknown &u, const LcOperatorOnUnknown &lcopv)
{
  return id(u) ^ lcopv;
}

LcOperatorOnUnknowns operator*(const LcOperatorOnUnknown &lcopu, const LcOperatorOnUnknown &lcopv)
{
  LcOperatorOnUnknowns lcopus;
  for (cit_opuval itu = lcopu.begin(); itu != lcopu.end(); itu++)
  {
    for (cit_opuval itv = lcopv.begin(); itv != lcopv.end(); itv++)
    {
      if (!checkConsistancy(*itu->first, _product, *itv->first))
      {
        error("opu_badopus", words("algop", _product), "operator *");
      }
      lcopus += itu->second * itv->second * OperatorOnUnknowns(*itu->first, *itv->first, _product);
    }
  }
  return lcopus;
}

LcOperatorOnUnknowns operator%(const LcOperatorOnUnknown &lcopu, const LcOperatorOnUnknown &lcopv)
{
  LcOperatorOnUnknowns lcopus;
  for (cit_opuval itu = lcopu.begin(); itu != lcopu.end(); itu++)
  {
    for (cit_opuval itv = lcopv.begin(); itv != lcopv.end(); itv++)
    {
      if (!checkConsistancy(*itu->first, _contractedProduct, *itv->first))
      {
        error("opu_badopus", words("algop", _contractedProduct), "operator %");
      }
      lcopus += itu->second * itv->second * OperatorOnUnknowns(*itu->first, *itv->first, _contractedProduct);
    }
  }
  return lcopus;
}

LcOperatorOnUnknowns operator|(const LcOperatorOnUnknown &lcopu, const LcOperatorOnUnknown &lcopv)
{
  LcOperatorOnUnknowns lcopus;
  for (cit_opuval itu = lcopu.begin(); itu != lcopu.end(); itu++)
  {
    for (cit_opuval itv = lcopv.begin(); itv != lcopv.end(); itv++)
    {
      if (!checkConsistancy(*itu->first, _innerProduct, *itv->first))
      {
        error("opu_badopus", words("algop", _innerProduct), "operator |");
      }
      lcopus += itu->second * itv->second * OperatorOnUnknowns(*itu->first, *itv->first, _innerProduct);
    }
  }
  return lcopus;
}

LcOperatorOnUnknowns operator^(const LcOperatorOnUnknown &lcopu, const LcOperatorOnUnknown &lcopv)
{
  LcOperatorOnUnknowns lcopus;
  for (cit_opuval itu = lcopu.begin(); itu != lcopu.end(); itu++)
  {
    for (cit_opuval itv = lcopv.begin(); itv != lcopv.end(); itv++)
    {
      if (!checkConsistancy(*itu->first, _crossProduct, *itv->first))
      {
        error("opu_badopus", words("algop", _crossProduct), "operator ^");
      }
      lcopus += itu->second * itv->second * OperatorOnUnknowns(*itu->first, *itv->first, _crossProduct);
    }
  }
  return lcopus;
}

} // namespace xlifepp