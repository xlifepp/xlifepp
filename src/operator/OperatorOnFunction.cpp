/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file OperatorOnFunction.cpp
  \author E. Lunéville
  \since 05 sep 2014
  \date 05 sep 2014

  \brief Implementation of xlifepp::OperatorOnFunction class members and related functions
 */

#include "OperatorOnFunction.hpp"

namespace xlifepp
{
//! constructor from Function
OperatorOnFunction::OperatorOnFunction(const Function& fun, DiffOpType d)
{
  fun_p=new Function(fun, true);   //true means full copy of function parameters
  difOp_p=findDifferentialOperator(d);
  ext_p=nullptr;
  in_ext=true;
  type_=fun.valueType();
  struct_=fun_p->strucType();
  dimsRes_= fun_p->dims();
  transpose_=false;
  conjugate_=false;
  if(d!=_id) initStrucType();
}

OperatorOnFunction::OperatorOnFunction(const Function* fun, DiffOpType d, ValueType vt, StrucType st, dimPair dims)
{
  if(fun!=nullptr) fun_p=new Function(*fun, true); else fun_p=nullptr;  //true means full copy of function parameters
  difOp_p=findDifferentialOperator(d);
  ext_p=nullptr;
  in_ext=true;
  type_=vt;
  struct_=st;
  dimsRes_=dims;
  transpose_=false;
  conjugate_=false;
}
//! full copy constructor
OperatorOnFunction::OperatorOnFunction(const OperatorOnFunction& opf)
{
    copy(opf);
}

//! full copy of members
void OperatorOnFunction::copy(const OperatorOnFunction& opf)
{
    if(opf.fun_p!=nullptr) fun_p=new Function(*opf.fun_p, true);//true means full copy of function parameters
    else fun_p=nullptr;
    difOp_p=opf.difOp_p;
    if(opf.ext_p!=nullptr) ext_p=new Extension(*opf.ext_p);
    else ext_p=nullptr;
    in_ext=opf.in_ext;
    type_=opf.type_;
    struct_=opf.struct_;
    dimsRes_=opf.dimsRes_;
    transpose_=opf.transpose_;
    conjugate_=opf.conjugate_;
}

//! delete members
void OperatorOnFunction::clear()
{
    if(fun_p!=nullptr) delete fun_p;
    fun_p=nullptr; ext_p=nullptr;
    if(ext_p!=nullptr) delete ext_p;
}

//! assign operator (full copy)
OperatorOnFunction& OperatorOnFunction::operator=(const OperatorOnFunction& opf)
{
    if(this==&opf) return *this;
    clear();
    copy(opf);
    return *this;
}

//! set struct_ and dimRes_
void OperatorOnFunction::initStrucType()
{
  struct_ = fun_p->strucType();
  dimsRes_= fun_p->dims();
  //update if diff operator change the structure of function
  //as dimension of point is unknown, space dimension is assumed to be 3
  switch(difOp_p->type())
  {
    case _ndot:
//    case _ncrossndot:
    case _ndot_x:
    case _ndot_y:
    case _nxcrossny_dot:
    case _nycrossnx_dot:
    case _ndotgrad_x:
    case _ndotgrad_y:
    case _ndotgrad:
    case _div_x:
    case _div_y:
    case _divG:
    case _divS:
    case _div:
      if(struct_!=_matrix) {struct_ = _scalar; dimsRes_= std::make_pair(1,1);}
      else {struct_ = _vector; dimsRes_= std::make_pair(dimsRes_.first,1);}
      break;
    case _grad_x:
    case _grad_y:
    case _gradS:
    case _gradG:
    case _grad:
      if(struct_==_scalar) {struct_ = _vector; dimsRes_= std::make_pair(3,1);}
      if(struct_==_vector) {struct_ = _matrix; dimsRes_= std::make_pair(dimsRes_.first,3);}
      if(struct_==_matrix)
      {
        where("OperatorOnFunction::initStrucType()");
        error("operator_not_matrix",words("diffop",difOp_p->type()));
      }
      break;
    case _epsilonR:
    case _epsilonG:
    case _epsilon:
      if(struct_==_vector) {struct_ = _matrix; dimsRes_= std::make_pair(dimsRes_.first,3);}
      else
      {
        where("OperatorOnFunction::initStrucType()");
        error("operator_vector_only",words("diffop", difOp_p->type()));
      }
      break;
    case _timesncrossn:
    case _timesn:
      if(struct_==_scalar) {struct_ = _vector; dimsRes_= std::make_pair(3,1);}
      if(struct_==_matrix) {struct_ = _vector; dimsRes_= std::make_pair(dimsRes_.first,1);}
      if(struct_==_vector)
      {
        where("OperatorOnFunction::initStrucType()");
        error("operator_not_vector",words("diffop",difOp_p->type()));
      }
      break;
     case _ntimes_x:
     case _ntimes_y:
     case _ntimes:
      if(struct_==_scalar) {struct_ = _vector; dimsRes_= std::make_pair(3,1);}
      if(struct_==_matrix) {struct_ = _vector; dimsRes_= std::make_pair(dimsRes_.second,1);}
      if(struct_==_vector)
      {
        where("OperatorOnFunction::initStrucType()");
        error("operator_not_vector",words("diffop", difOp_p->type()));
      }
      break;
    case _ncrossncross_x:
    case _ncrossncross_y:
    case _nxcrossny_cross:
    case _nycrossnx_cross:
    case _ncross_x:
    case _ncross_y:
    case _ncross:
      if(struct_!=_vector)
      {
        where("OperatorOnFunction::initStrucType()");
        error("operator_vector_only",words("diffop",difOp_p->type()));
      }
      break;
    case _ndiv:
    case _ndiv_x:
    case _ndiv_y:
      error("operator_unexpected",words("diffop",difOp_p->type()));
      break;
    case _ncrossgrad:
      if(struct_==_scalar) {struct_ = _vector; dimsRes_= std::make_pair(3,1);}
      else
      {
        where("OperatorOnFunction::initStrucType()");
        error("operator_scalar_only",words("diffop",difOp_p->type()));
      }
      break;
    case _ncrosscurl_x:
    case _ncrosscurl_y:
      if(struct_!=_vector)
      {
        where("OperatorOnFunction::initStrucType()");
        error("operator_vector_only",words("diffop",difOp_p->type()));
      }
      break;
    default: break;
  }
}

//print stuff
void OperatorOnFunction::print(std::ostream& os) const
{
  os<< *fun_p;
  os<<"      x differential operator: "<<* difOp_p;
  if(ext_p!=nullptr)
  {
      os<<"\n      with extension "<< ext_p->name();
      if (in_ext) os<<" : ext(op(f))"; else os<<" : op(ext(f))";
  }
}

string_t OperatorOnFunction::asString() const //! return as symbolic string
{
  string_t s;
  if(ext_p!=nullptr && !in_ext) s+=ext_p->name()+"(";
  if(transpose_)  s+="trans(";
  if(conjugate_)  s+="conj(";
  if(difOp_p->type()!=_id) s+=difOp_p->name()+"(";
  if(ext_p!=nullptr && in_ext) s+=ext_p->name()+"(";
  if(fun_p->transpose_)  s+="trans(";
  if(fun_p->conjugate_)  s+="conj(";
  if(fun_p->name()=="" || fun_p->name()=="?") s+="fun"; else s+=fun_p->name();
  if(fun_p->transpose_ || fun_p->conjugate_)  s+=")";
  if(ext_p!=nullptr && in_ext) s+=")";
  if(difOp_p->type()!=_id) s+=")";
  if(transpose_||conjugate_)  s+=")";
  if(ext_p!=nullptr && !in_ext) s+=")";
  return s;
}

std::ostream& operator<<(std::ostream& os, const OperatorOnFunction& opf)
{
  opf.print(os);
  return os;
}
//-------------------------------------------------------------------------------
// differenial operator on Function
// WARNING: dimsRes_ is only managed for id
//-------------------------------------------------------------------------------
OperatorOnFunction& id(const Function& f)
{return*(new OperatorOnFunction(&f,_id,f.valueType(),f.strucType(),f.dims()));}

OperatorOnFunction& ntimes(const Function& f)  // n*f
{
  if(f.strucType()==_scalar || f.strucType()==_matrix)
     return *(new OperatorOnFunction(&f,_ntimes,f.valueType(),_vector));
  error("operator_unexpected","ntimes(Function)");
  return *(new OperatorOnFunction(&f));
}

OperatorOnFunction& timesn(const Function& f)  // f*n
{
  if(f.strucType()==_scalar || f.strucType()==_matrix)
     return *(new OperatorOnFunction(&f,_timesn,f.valueType(),_vector));
  error("operator_unexpected","timesn(Function)");
  return *(new OperatorOnFunction(&f));
}

OperatorOnFunction& ncrossntimes(const Function& f)  // (n^n)*f
{
  if(f.strucType()==_scalar || f.strucType()==_matrix)
     return *(new OperatorOnFunction(&f,_ncrossntimes,f.valueType(),_vector));
  error("operator_unexpected","ncrossntimes(Function)");
  return *(new OperatorOnFunction(&f));
}

OperatorOnFunction& timesncrossn(const Function& f)  // f*(n^n)
{
  if(f.strucType()==_scalar || f.strucType()==_matrix)
     return *(new OperatorOnFunction(&f,_timesncrossn, f.valueType(),_vector));
  error("operator_unexpected","timesncrossn(Function)");
  return *(new OperatorOnFunction(&f));
}

OperatorOnFunction& ndot(const Function& f)  // n|f
{
  if(f.strucType()==_vector) return *(new OperatorOnFunction(&f,_ndot, f.valueType(),_scalar));
  if(f.strucType()==_matrix) return *(new OperatorOnFunction(&f,_ndot, f.valueType(),_vector));
  error("operator_unexpected","ndot(Function)");
  return * (new OperatorOnFunction(&f));  //fake return
}

OperatorOnFunction& ncross(const Function& f)  // n^f
{
  return *(new OperatorOnFunction(&f,_ncross,f.valueType(),f.strucType()));
}

OperatorOnFunction& ncrossncross(const Function& f)  // (n^n)^f
{
  return *(new OperatorOnFunction(&f,_ncrossncross,f.valueType(),f.strucType()));
}

//OperatorOnFunction& ncrossndot(const Function& f)  // (n^n).f
//{
//  return *(new OperatorOnFunction(&f,_ncrossndot,f.valueType(),f.strucType()));
//}

//-------------------------------------------------------------------------------
// overriding operators on Function and OperatorOnFunction
//-------------------------------------------------------------------------------
OperatorOnFunction& operator*(UnitaryVector v, const Function& f)     // n*f same as ntimes(f) or ncrossntimes(f)
{
  if(v == _n) return ntimes(f);
  if(v == _ncrossn) return ncrossntimes(f);
  error("operator_unexpected","UnitaryVector? * Function");
  return * (new OperatorOnFunction(&f));  //fake return
}

OperatorOnFunction& operator*(const Function& f, UnitaryVector v)     // f*n same as timesn(f) or timesncrossn(f)
{
  if(v == _n) return timesn(f);
  if(v == _ncrossn) return timesncrossn(f);
  error("operator_unexpected","Function * UnitaryVector?");
  return * (new OperatorOnFunction(&f));  //fake return
}

OperatorOnFunction& operator|(UnitaryVector v, const Function& f)     // n|f same as ndot(f)
{
  if(v == _n) return ndot(f);
//  if(v == _ncrossn) return ncrossndot(f);
  error("operator_unexpected"," UnitaryVector? | Function");
  return * (new OperatorOnFunction(&f));  //fake return
}

OperatorOnFunction& operator|(const Function& f,UnitaryVector v)     // f|n same as ndot(f)
{
  return v|f;
}

OperatorOnFunction& operator^(UnitaryVector v, const Function& f)     // n^f same as ncross(f)
{
  if(v == _n) return ncross(f);
  if(v == _ncrossn) return ncrossncross(f);
  error("operator_unexpected"," UnitaryVector? ^ Function");
  return *(new OperatorOnFunction(&f));
}

OperatorOnFunction& id(OperatorOnFunction& opf)               // id(opf)
{
  return opf;
}

OperatorOnFunction& ntimes(OperatorOnFunction& opf) // n*opf
{
  if(opf.difOpType()==_id && (opf.strucType()==_scalar || opf.strucType()==_matrix))
    {
      opf.difOp_()=findDifferentialOperator(_ntimes);
      opf.strucType()=_vector;
      return opf;
    }
  error("operator_unexpected"," n * OperatorOnFunction");
  return opf;
}

OperatorOnFunction& timesn(OperatorOnFunction& opf) // opf*n
{
  if(opf.difOpType()==_id && (opf.strucType()==_scalar || opf.strucType()==_matrix))
    {
      opf.difOp_()=findDifferentialOperator(_timesn);
      opf.strucType()=_vector;
      return opf;
    }
  error("operator_unexpected"," OperatorOnFunction * n");
  return opf;
}

OperatorOnFunction& ndot(OperatorOnFunction& opf)    // n|opf
{
  if(opf.difOpType()==_id && opf.strucType()==_vector)
    {
      opf.difOp_()=findDifferentialOperator(_ndot);
      opf.strucType()=_scalar;
      return opf;
    }
  error("operator_unexpected"," nx | OperatorOnFunction");
  return opf;
}

OperatorOnFunction& ncross(OperatorOnFunction& opf)    // n^opf
{
  if(opf.difOpType()==_id )
    {
      opf.difOp_()=findDifferentialOperator(_ncross);
      return opf;
    }
    if(opf.difOpType()==_ncross )
    {
      opf.difOp_()=findDifferentialOperator(_ncrossncross);
      return opf;
    }
  error("operator_unexpected"," n ^ OperatorOnFunction");
  return opf;
}

OperatorOnFunction& ncrossntimes(OperatorOnFunction& opf)    // (n^n) * opf
{
  if(opf.difOpType()==_id && (opf.strucType()==_scalar || opf.strucType()==_matrix))
    {
      opf.difOp_()=findDifferentialOperator(_ncrossntimes);
      opf.strucType()=_vector;
      return opf;
    }
  error("operator_unexpected"," (n^n)* OperatorOnFunction");
  return opf;
}

OperatorOnFunction& timesncrossn(OperatorOnFunction& opf)    // opf * (n^n)
{
  if(opf.difOpType()==_id && (opf.strucType()==_scalar || opf.strucType()==_matrix))
    {
      opf.difOp_()=findDifferentialOperator(_timesncrossn);
      opf.strucType()=_vector;
      return opf;
    }
  error("operator_unexpected"," OperatorOnFunction * (n^n)");
  return opf;
}

//OperatorOnFunction& ncrossndot(OperatorOnFunction& opf)    // (n^n)|opf
//{
//  if(opf.difOpType()==_id )
//    {
//      opf.difOp_()=findDifferentialOperator(_ncrossndot);
//      opf.strucType()=_scalar;
//      return opf;
//    }
//  error("operator_unexpected"," (n^n) | OperatorOnFunction");
//  return opf;
//}

OperatorOnFunction& ncrossncross(OperatorOnFunction& opf)    // n^n^opf
{
  if(opf.difOpType()==_id )
    {
      opf.difOp_()=findDifferentialOperator(_ncrossncross);
      opf.strucType()=opf.strucType();
      return opf;
    }
  error("operator_unexpected"," (n^n) ^ OperatorOnFunction");
  return opf;
}

OperatorOnFunction& operator*(UnitaryVector v, OperatorOnFunction& opf) // v*opf
{
  if(v == _n) return ntimes(opf);
  if(v == _ncrossn) return ncrossntimes(opf);
  error("operator_unexpected"," UnitaryVector * OperatorOnFunction");
  return opf;
}

OperatorOnFunction& operator*(OperatorOnFunction& opf,UnitaryVector v) // opf*v
{
  if(v == _n) return timesn(opf);
  if(v == _ncrossn) return timesncrossn(opf);
  error("operator_unexpected"," OperatorOnFunction * UnitaryVector *");
  return opf;
}

OperatorOnFunction& operator|(UnitaryVector v, OperatorOnFunction& opf) // v|opf
{
  if(v == _n) return ndot(opf);
//  if(v == _ncrossn) return ncrossndot(opf);
  error("operator_unexpected"," UnitaryVector | OperatorOnFunction");
  return opf;
}

OperatorOnFunction& operator|(OperatorOnFunction& opf, UnitaryVector v) // opf|v
{
  return v|opf;
}

OperatorOnFunction& operator^(UnitaryVector v, OperatorOnFunction& opf) // v^opf
{
  if(v == _n) return ncross(opf);
  if(v == _ncrossn) return ncrossncross(opf);
  error("operator_unexpected"," UnitaryVector | OperatorOnFunction");
  return opf;
}

//comparison
bool operator==(const OperatorOnFunction& opf1, const OperatorOnFunction& opf2)
{
  return (opf1.fun_p==opf2.fun_p && opf1.difOp_p==opf2.difOp_p);
}

bool operator!=(const OperatorOnFunction& opf1, const OperatorOnFunction& opf2)
{
  return !(opf1==opf2);
}

//special case involving constant and unitary vector, create Function on the fly
OperatorOnFunction& operator*(const real_t& v, UnitaryVector n)        //! v*n same as f_v*n
{
  Parameters* pars=new Parameters(v,"const_value");
  Function* f=new Function(real_const_fun, *pars);
  if(n == _n)       return *(new OperatorOnFunction(f,_timesn,_real,_scalar));
  if(v == _ncrossn) return *(new OperatorOnFunction(f,_timesncrossn,_real,_scalar));
  error("operator_unexpected"," real * UnitaryVector?");
  return *(new OperatorOnFunction(f,_timesn,_real,_scalar));  //fake return
}

OperatorOnFunction& operator*(UnitaryVector n, const real_t& v)       //! v*n same as f_v*n
{return v*n;}

OperatorOnFunction& operator*(const complex_t& v, UnitaryVector n)      //! v*n same as f_v*n
{
  Parameters* pars=new Parameters(v,"const_value");
  Function* f=new Function(complex_const_fun, *pars);
  if(n == _n)       return *(new OperatorOnFunction(f,_timesn,_complex,_scalar));
  if(n == _ncrossn) return *(new OperatorOnFunction(f,_timesncrossn,_complex,_scalar));
  error("operator_unexpected"," complex * UnitaryVector?");
  return *(new OperatorOnFunction(f,_timesn,_complex,_scalar));  //fake return
}

OperatorOnFunction& operator*(UnitaryVector n, const complex_t& v)    //! v*n same as f_v*n
{return v*n;}

OperatorOnFunction& operator*(const Matrix<real_t>& v, UnitaryVector n) //! v*n same as f_v*n
{
  Parameters* pars=new Parameters(v,"const_matrix_value");
  Function* f=new Function(real_matrix_const_fun, *pars);
  if(n == _n)       return *(new OperatorOnFunction(f,_timesn,_real,_matrix));
  if(n == _ncrossn) return *(new OperatorOnFunction(f,_timesncrossn,_real,_matrix));
  error("operator_unexpected"," real matrix * UnitaryVector?");
  return *(new OperatorOnFunction(f,_timesn,_real,_matrix));  //fake return
}

OperatorOnFunction& operator*(UnitaryVector n, const Matrix<real_t>& v) //! v*n same as f_v*n
{
  Parameters* pars=new Parameters(v,"const_matrix_value");
  Function* f=new Function(real_matrix_const_fun, *pars);
  if(n == _n)       return *(new OperatorOnFunction(f,_ntimes,_real,_matrix));
  if(n == _ncrossn) return *(new OperatorOnFunction(f,_ncrossntimes,_real,_matrix));
  error("operator_unexpected"," real matrix * UnitaryVector?");
  return *(new OperatorOnFunction(f,_ntimes,_real,_matrix));  //fake return
}

OperatorOnFunction& operator*(const Matrix<complex_t>& v, UnitaryVector n) //! v*n same as f_v*n
{
  Parameters* pars=new Parameters(v,"const_matrix_value");
  Function* f=new Function(complex_matrix_const_fun, *pars);
  if(n == _n)       return *(new OperatorOnFunction(f,_timesn,_complex,_matrix));
  if(n == _ncrossn) return *(new OperatorOnFunction(f,_timesncrossn,_complex,_matrix));
  error("operator_unexpected"," complex matrix * UnitaryVector?");
  return *(new OperatorOnFunction(f,_timesn,_complex,_matrix));  //fake return
}

OperatorOnFunction& operator*(UnitaryVector n, const Matrix<complex_t>& v) //! v*n same as f_v*n
{
  Parameters* pars=new Parameters(v,"const_matrix_value");
  Function* f=new Function(complex_matrix_const_fun, *pars);
  if(n == _n)       return *(new OperatorOnFunction(f,_ntimes,_complex,_matrix));
  if(n == _ncrossn) return *(new OperatorOnFunction(f,_ncrossntimes,_complex,_matrix));
  error("operator_unexpected"," complex matrix * UnitaryVector?");
  return *(new OperatorOnFunction(f,_ntimes,_complex,_matrix));  //fake return
}

OperatorOnFunction& operator|( UnitaryVector n, const Vector<real_t>& v) //! n|v same as n|f_v
{
  Parameters* pars=new Parameters(v,"const_vector_value");
  Function* f=new Function(real_vector_const_fun, *pars);
  if(n == _n) return *(new OperatorOnFunction(f,_ndot,_real,_scalar));
//    if(n == _ncrossn) return *(new OperatorOnFunction(f,_ncrossndot,_real,_scalar));
  error("operator_unexpected"," real vector | UnitaryVector?");
  return *(new OperatorOnFunction(f,_ndot,_real,_scalar)); //fake return
}

OperatorOnFunction& operator|( const Vector<real_t>& v, UnitaryVector n) //! v|n same as f_v|n
{ return n|v;}

OperatorOnFunction& operator|( UnitaryVector n, const Vector<complex_t>& v) //! n|v same as n|f_v
{
  Parameters* pars=new Parameters(v,"const_vector_value");
  Function* f=new Function(complex_vector_const_fun, *pars);
  if(n == _n)       return *(new OperatorOnFunction(f,_ndot,_complex,_scalar));
//    if(n == _ncrossn) return *(new OperatorOnFunction(f,_ncrossndot,_complex,_scalar));
  error("operator_unexpected"," complex vector | UnitaryVector?");
  return *(new OperatorOnFunction(f,_ndot,_complex,_scalar));  //fake return;
}

OperatorOnFunction& operator|( const Vector<complex_t>& v, UnitaryVector n) //! v|n same as f_v|n
{ return n|v;}

//special cases involving Extension
OperatorOnFunction& operator*(const Extension& e, const Function& f)         //! extension of Function
{
  OperatorOnFunction* opf=new OperatorOnFunction(&f,_id,f.valueType(),f.strucType(),f.dims());
  opf->ext_p = new Extension(e);
  opf->in_ext=true;
  return *opf;
}

OperatorOnFunction& operator*(const Extension& e, OperatorOnFunction& opf)   //! extension of OperatorOnFunction
{
  if (opf.ext_p!=nullptr)
  {
    where("Extension * OperatorOnFunction");
    error("extension_already_set");
  }
  opf.ext_p = new Extension(e);
  opf.in_ext=false;
  return opf;
}

//alternate syntax
OperatorOnFunction& Extension::operator()(const Function& f) const           //! apply Extension to Function
  {return (*this)*f;}

OperatorOnFunction& Extension::operator()(OperatorOnFunction& opf) const     //! apply Extension to OperatorOnFunction
  {return (*this)*opf;}


//conjugate or transpose Function
OperatorOnFunction& conj(Function& f)
{
  OperatorOnFunction* opf=new OperatorOnFunction(&f,_id,f.valueType(),f.strucType(),f.dims());
  opf->conjugate_ = true;
  return *opf;
}

OperatorOnFunction& tran(Function& f)
{
  OperatorOnFunction* opf=new OperatorOnFunction(&f,_id,f.valueType(),f.strucType(),f.dims());
  opf->transpose_ = true;
  return *opf;
}

OperatorOnFunction& adj(Function& f)
{
  OperatorOnFunction* opf=new OperatorOnFunction(&f,_id,f.valueType(),f.strucType(),f.dims());
  opf->conjugate_ = true;
  opf->transpose_ = true;
  return *opf;
}


//conjugate or transpose OperatorOnFunction
OperatorOnFunction& conj(OperatorOnFunction& opf)
{
  opf.conjugate_=!opf.conjugate_;
  return opf;
}

OperatorOnFunction& tran(OperatorOnFunction& opf)
{
  opf.transpose_=!opf.transpose_;
  return opf;
}

OperatorOnFunction& adj(OperatorOnFunction& opf)
{
  opf.transpose_=!opf.transpose_;
  opf.conjugate_=!opf.conjugate_;
  return opf;
}


} //end of namespace xlifepp
