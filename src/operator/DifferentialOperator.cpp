/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DifferentialOperator.cpp
  \authors D. Martin, E. Lunéville
  \since 1 mars 2012
  \date 16 jan 2013

  \brief Implementation of xlifepp::DifferentialOperator class member functions and related utilities
*/

#include "DifferentialOperator.hpp"
#include "utils.h"

namespace xlifepp
{

//------------------------------------------------------------------------------------
// DifferentialOperator member functions
//------------------------------------------------------------------------------------
// default constructor

DifferentialOperator::DifferentialOperator()
   : type_(_id), order_(0), requiresExtension_(false), requiresNormal_(false), name_("id") {}

// constructor by type number
DifferentialOperator::DifferentialOperator(DiffOpType ty)
{
   type_ = ty;
   build();
   theDifferentialOperators.push_back(this);
}

// destructor
DifferentialOperator::~DifferentialOperator()
{
   typedef std::vector<DifferentialOperator*>::iterator it_vd;
   // remove pointer of current object from vector of DifferentialOperator*
   it_vd  it = std::find(theDifferentialOperators.begin(), theDifferentialOperators.end(), this);
   if(it != theDifferentialOperators.end()) { theDifferentialOperators.erase(it); }
}

// delete all DifferentialOperator objects
void DifferentialOperator::clearGlobalVector()
{
   while(DifferentialOperator::theDifferentialOperators.size() > 0) { delete DifferentialOperator::theDifferentialOperators[0]; }
}

// define operator properties
void DifferentialOperator::build()
{
   name_ = words("diffop", type_);
   order_ = 0;
   requiresNormal_ = false;
   requiresExtension_ = false;
   switch(type_)
   {
      case _d0:
      case _d1:
      case _d2:
      case _d3:
      case _grad:
      case _div:
      case _curl:
      case _gradG:
      case _divG:
      case _curlG:
      case _grad_x:
      case _grad_y:
      case _grad_xy:
      case _div_x:
      case _div_y:
      case _div_xy:
      case _curl_x:
      case _curl_y:
      case _curl_xy:
      case _trac_x:
      case _trac_y:
      case _trac_xy:
      case _epsilon:
      case _epsilonR:
      case _epsilonG: order_ = 1; requiresExtension_ = true; break;  //_epsilonG may be of order 0 (see diffOrder() function)

      case _gradS:
      case _divS:
      case _scurlS:
      case _curlS: order_ = 1; break;

      case _ntimes:
      case _timesn:
      case _ndot:
      case _ncross:
      case _ncrossncross:
      case _ncrossntimes:
      case _timesncrossn:
//      case _ncrossndot:
      case _ntimesndot:
      case _ntimes_x:
      case _timesn_x:
      case _ndot_x:
      case _ncross_x:
      case _ncrossncross_x:
      case _ncrossntimes_x:
      case _timesncrossn_x:
      case _ntimes_y:
      case _timesn_y:
      case _ndot_y:
      case _ncross_y:
      case _ncrossncross_y:
      case _ncrossntimes_y:
      case _timesncrossn_y:
      case _nxdotny_times:
      case _nxcrossny_dot:
      case _nycrossnx_dot:
      case _nxcrossny_cross:
      case _nycrossnx_cross:
      case _nxcrossny_times:
      case _nycrossnx_times:
      requiresNormal_ = true; break;

      case _ndotgrad:
      case _ndiv:
      case _ncrosscurl:
      case _ncrossgrad:
      case _ndotgrad_x:
      case _ndiv_x:
      case _ncrosscurl_x:
      case _ndotgrad_y:
      case _ndiv_y:
      case _ndotgrad_xy:
      case _ncrosscurl_y:order_ = 1; requiresNormal_ = true; requiresExtension_ = true; break;

      case _d11:
      case _d22:
      case _d33:
      case _d12:
      case _d13:
      case _d23:
      case _lap:
      case _lapG:
      case _d2G: order_ = 2; requiresExtension_ = true; break;

      case _id:
      default: break;
   }
}

/*
--------------------------------------------------------------------------------
 DifferentialOperatorFind: definition of a DifferentialOperator by type number
 create a new one if does not exist
--------------------------------------------------------------------------------
*/
DifferentialOperator* findDifferentialOperator(DiffOpType ty)
{
   typedef std::vector<DifferentialOperator*>::iterator it_vd;
   // find differentialOperator in vector theDifferentialOperators
   for(it_vd it = DifferentialOperator::theDifferentialOperators.begin();
         it != DifferentialOperator::theDifferentialOperators.end(); it++)
   {
      if(ty == (*it)->type()) { return *it; }
   }
   // create new differentialOperator and returns pointer
   return new DifferentialOperator(ty);
}


/*
--------------------------------------------------------------------------------
 Utilities
--------------------------------------------------------------------------------
*/
std::ostream& operator<<(std::ostream& os, const DifferentialOperator& dop)
{
   if(theVerboseLevel > 0)
   { os << words("operator") << " " << dop.name()<<" "; }
   if(theVerboseLevel > 1)
   { os << message("diffop_def", dop.order(), words(dop.normalRequired()), words(dop.extensionRequired())); }
   return os;
}

// print the list of differential operators (check utility)
void printListDiffOp(std::ostream& os)
{
  typedef std::vector<DifferentialOperator*>::iterator it_vd;
  // find differentialOperator in vector theDifferentialOperators
  os << "List of differential operators (created):\n";
  for(it_vd it = DifferentialOperator::theDifferentialOperators.begin();
        it != DifferentialOperator::theDifferentialOperators.end(); it++)
  {os << **it << "\n";}
}

void printListDiffOp(CoutStream& cs)
{
  typedef std::vector<DifferentialOperator*>::iterator it_vd;
  // find differentialOperator in vector theDifferentialOperators
  cs << "List of differential operators (created):\n";
  for(it_vd it = DifferentialOperator::theDifferentialOperators.begin();
        it != DifferentialOperator::theDifferentialOperators.end(); it++)
  {cs << **it << "\n";}
}

} // end of namespace xlifepp

