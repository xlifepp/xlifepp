/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file KernelOperatorOnUnknowns.cpp
  \author E. Lunéville
  \since 4 mar 2012
  \date 9 may 2012

  \brief Implementation of xlifepp::OperatorOnUnknown class members and related functions
*/

//===============================================================================
// library dependencies
#include "KernelOperatorOnUnknowns.hpp"
//===============================================================================

namespace xlifepp
{

KernelOperatorOnUnknowns::KernelOperatorOnUnknowns(const OperatorOnUnknown& opu, AlgebraicOperator aopu,
                                                    const OperatorOnKernel& opker,
                                                    AlgebraicOperator aopv,const OperatorOnUnknown& opv, bool right)
{
  opu_=opu;
  opv_=opv;
  aopu_=aopu; aopv_=aopv;
  opker_=opker;
  rightPriority=right;
}

ValueType KernelOperatorOnUnknowns::valueType() const
{
    if(opu_.valueType()==_complex || opv_.valueType()==_complex || opker_.valueType()==_complex) return _complex;
    return _real;
}

// set (change) the unknowns
void KernelOperatorOnUnknowns::setUnknowns(const Unknown& u, const Unknown& v)
{
    opu_.setUnknown(u);
    opv_.setUnknown(v);
}

void KernelOperatorOnUnknowns::print(std::ostream& os) const
{
  if(theVerboseLevel <= 0) return;
  os << "kernel operator on unknowns:\n ";
  if(opu_.unknown()!=nullptr) os << "  left operator: " << opu_<< "   left operation: " << words("algop", aopu_) << "\n";
  else os << "  no left operator \n";
  os << "   "<<opker_;
  if(opv_.unknown()!=nullptr) os << "\n   right operation: " << words("algop", aopv_) << "\n"<<"   right operator: " << opv_;
  else os << "\n   no right operator \n";
  if(rightPriority) os<<" compute opu "<< words("algop", aopu_)<<" (opker "<< words("algop", aopv_)<<" opv)\n";
  else os<<" compute (opu "<< words("algop", aopu_)<<" opker) "<< words("algop", aopv_)<<" opv\n";
}

std::ostream& operator<<(std::ostream& os, const KernelOperatorOnUnknowns& opku)
{
  opku.print(os);
  return os;
}

string_t KernelOperatorOnUnknowns::asString() const //! return as symbolic string
{
  std::vector<string_t> sop(4); sop[0]=" * ";sop[1]=" | ";sop[2]=" ^ ";sop[3]=" % ";
    if(rightPriority) return opu_.asString() + sop[aopu_] + "("+opker_.asString() + sop[aopv_] + opv_.asString()+")";
    return "("+opu_.asString() + sop[aopu_] +opker_.asString()+")" + sop[aopv_] + opv_.asString();
}

// external functions
// opu op opker
KernelOperatorOnUnknowns operator*(const OperatorOnUnknown& opu, const OperatorOnKernel& opker)
{return KernelOperatorOnUnknowns(opu,_product,opker,_product, OperatorOnUnknown());}

KernelOperatorOnUnknowns operator|(const OperatorOnUnknown& opu, const OperatorOnKernel& opker)
{return KernelOperatorOnUnknowns(opu,_innerProduct,opker,_product, OperatorOnUnknown());}

KernelOperatorOnUnknowns operator^(const OperatorOnUnknown& opu, const OperatorOnKernel& opker)
{return KernelOperatorOnUnknowns(opu,_crossProduct,opker,_product, OperatorOnUnknown());}

KernelOperatorOnUnknowns operator%(const OperatorOnUnknown& opu, const OperatorOnKernel& opker)
{return KernelOperatorOnUnknowns(opu,_contractedProduct,opker,_product, OperatorOnUnknown());}

// opker op opv, set rightPriority to true
KernelOperatorOnUnknowns operator*(const OperatorOnKernel& opker, const OperatorOnUnknown& opv)
{return KernelOperatorOnUnknowns(OperatorOnUnknown(),_product,opker,_product,opv, true);}

KernelOperatorOnUnknowns operator|(const OperatorOnKernel& opker, const OperatorOnUnknown& opv)
{return KernelOperatorOnUnknowns(OperatorOnUnknown(),_product,opker,_innerProduct,opv, true);}

KernelOperatorOnUnknowns operator^(const OperatorOnKernel& opker, const OperatorOnUnknown& opv)
{return KernelOperatorOnUnknowns(OperatorOnUnknown(),_product,opker,_crossProduct,opv, true);}

KernelOperatorOnUnknowns operator%(const OperatorOnKernel& opker, const OperatorOnUnknown& opv)
{return KernelOperatorOnUnknowns(OperatorOnUnknown(),_product,opker,_contractedProduct,opv, true);}

// opu op kernel
KernelOperatorOnUnknowns operator*(const OperatorOnUnknown& opu, const Kernel& ker)
{OperatorOnKernel opk(ker); return (opu * opk);}

KernelOperatorOnUnknowns operator|(const OperatorOnUnknown& opu, const Kernel& ker)
{OperatorOnKernel opk(ker);return opu | opk;}

KernelOperatorOnUnknowns operator^(const OperatorOnUnknown& opu, const Kernel& ker)
{OperatorOnKernel opk(ker);return opu ^ opk;}

KernelOperatorOnUnknowns operator%(const OperatorOnUnknown& opu, const Kernel& ker)
{OperatorOnKernel opk(ker);return opu % opk;}

KernelOperatorOnUnknowns operator*(const Kernel& ker, const OperatorOnUnknown& opv)
{OperatorOnKernel opk(ker);return opk * opv;}

KernelOperatorOnUnknowns operator|(const Kernel& ker, const OperatorOnUnknown& opv)
{OperatorOnKernel opk(ker);return opk| opv;}

KernelOperatorOnUnknowns operator^(const Kernel& ker, const OperatorOnUnknown& opv)
{OperatorOnKernel opk(ker);return opk ^ opv;}

KernelOperatorOnUnknowns operator%(const Kernel& ker, const OperatorOnUnknown& opv)
{OperatorOnKernel opk(ker);return opk % opv;}

KernelOperatorOnUnknowns operator*(const Unknown& u, const Kernel& ker)
{ OperatorOnKernel opk(ker); OperatorOnUnknown opu(u,_id);
 return  opu * opk;}

KernelOperatorOnUnknowns operator|(const Unknown& u, const Kernel& ker)
{ OperatorOnKernel opk(ker); OperatorOnUnknown opu(u,_id);
 return  opu | opk;}

KernelOperatorOnUnknowns operator^(const Unknown& u, const Kernel& ker)
{ OperatorOnKernel opk(ker); OperatorOnUnknown opu(u,_id);
 return  opu ^ opk;}

KernelOperatorOnUnknowns operator%(const Unknown& u, const Kernel& ker)
{ OperatorOnKernel opk(ker); OperatorOnUnknown opu(u,_id);
 return  opu % opk;}

KernelOperatorOnUnknowns operator*(const Kernel& ker , const Unknown& v)
{ OperatorOnKernel opk(ker); OperatorOnUnknown opv(v,_id);
 return  opk * opv;}

KernelOperatorOnUnknowns operator|(const Kernel& ker , const Unknown& v)
{ OperatorOnKernel opk(ker); OperatorOnUnknown opv(v,_id);
 return  opk | opv;}

KernelOperatorOnUnknowns operator^(const Kernel& ker , const Unknown& v)
{ OperatorOnKernel opk(ker); OperatorOnUnknown opv(v,_id);
 return  opk ^ opv;}

KernelOperatorOnUnknowns operator%(const Kernel& ker , const Unknown& v)
{ OperatorOnKernel opk(ker); OperatorOnUnknown opv(v,_id);
 return  opk % opv;}

KernelOperatorOnUnknowns operator*(const OperatorOnUnknown& opu, const KernelOperatorOnUnknowns& kop)
{
    KernelOperatorOnUnknowns rkop(kop);
    rkop.algopu()=_product;
    rkop.opu()=opu;
    return rkop;
}

KernelOperatorOnUnknowns operator|(const OperatorOnUnknown& opu, const KernelOperatorOnUnknowns& kop)
{
    KernelOperatorOnUnknowns rkop(kop);
    rkop.algopu()=_innerProduct;
    rkop.opu()=opu;
    return rkop;
}

KernelOperatorOnUnknowns operator^(const OperatorOnUnknown& opu, const KernelOperatorOnUnknowns& kop)
{
    KernelOperatorOnUnknowns rkop(kop);
    rkop.algopu()=_crossProduct;
    rkop.opu()=opu;
    return rkop;
}

KernelOperatorOnUnknowns operator%(const OperatorOnUnknown& opu, const KernelOperatorOnUnknowns& kop)
{
    KernelOperatorOnUnknowns rkop(kop);
    rkop.algopu()=_contractedProduct;
    rkop.opu()=opu;
    return rkop;
}

KernelOperatorOnUnknowns operator*(const KernelOperatorOnUnknowns& kop, const OperatorOnUnknown& opv)
{
    KernelOperatorOnUnknowns rkop(kop);
    rkop.algopv()=_product;
    rkop.opv()=opv;
    return rkop;
}

KernelOperatorOnUnknowns operator|(const KernelOperatorOnUnknowns& kop, const OperatorOnUnknown& opv)
{
    KernelOperatorOnUnknowns rkop(kop);
    rkop.algopv()=_innerProduct;
    rkop.opv()=opv;
    return rkop;
}

KernelOperatorOnUnknowns operator^(const KernelOperatorOnUnknowns& kop, const OperatorOnUnknown& opv)
{
    KernelOperatorOnUnknowns rkop(kop);
    rkop.algopv()=_crossProduct;
    rkop.opv()=opv;
    return rkop;
}
KernelOperatorOnUnknowns operator%(const KernelOperatorOnUnknowns& kop, const OperatorOnUnknown& opv)
{
    KernelOperatorOnUnknowns rkop(kop);
    rkop.algopv()=_contractedProduct;
    rkop.opv()=opv;
    return rkop;
}

KernelOperatorOnUnknowns operator*(const Unknown& u, const KernelOperatorOnUnknowns& kop)
{OperatorOnUnknown opu(u,_id); return opu * kop;}

KernelOperatorOnUnknowns operator|(const Unknown& u, const KernelOperatorOnUnknowns& kop)
{OperatorOnUnknown opu(u,_id); return opu | kop;}

KernelOperatorOnUnknowns operator^(const Unknown& u, const KernelOperatorOnUnknowns& kop)
{OperatorOnUnknown opu(u,_id); return opu ^ kop;}

KernelOperatorOnUnknowns operator%(const Unknown& u, const KernelOperatorOnUnknowns& kop)
{OperatorOnUnknown opu(u,_id); return opu % kop;}

KernelOperatorOnUnknowns operator*(const KernelOperatorOnUnknowns& kop, const Unknown&v)
{OperatorOnUnknown opv(v,_id); return kop * opv;}

KernelOperatorOnUnknowns operator|(const KernelOperatorOnUnknowns& kop, const Unknown&v)
{OperatorOnUnknown opv(v,_id); return kop | opv;}

KernelOperatorOnUnknowns operator^(const KernelOperatorOnUnknowns& kop, const Unknown&v)
{OperatorOnUnknown opv(v,_id); return kop ^ opv;}

KernelOperatorOnUnknowns operator%(const KernelOperatorOnUnknowns& kop, const Unknown&v)
{OperatorOnUnknown opv(v,_id); return kop % opv;}

//move partial KernelOperatorOnUnknowns to OperatorOnUnknown
OperatorOnUnknown toOperatorOnUnknown(const KernelOperatorOnUnknowns& kopus)
{
   const OperatorOnUnknown& opv=kopus.opv(), opu=kopus.opu();
   const Unknown *v=opv.unknown(), *u=opu.unknown();
   if(u==nullptr && v==nullptr)
   {
     where("toOperatorOnUnknown(const KernelOperatorOnUnknowns");
     error("null_pointer","unknowns");
   }
   const OperatorOnKernel& opk=kopus.opker();
   OperatorOnUnknown nopv;
   AlgebraicOperator aopk;
   if(v!=nullptr)  //take opker aop difop(v) as new OperatorOnUnknown
    {
        if(opv.leftOperand()!=nullptr || opv.rightOperand()!=nullptr)
        {
          where("toOperatorOnUnknown(const KernelOperatorOnUnknowns");
          error("operator_too_complex");
        }
        aopk= kopus.algopv();
        nopv= OperatorOnUnknown(*v,opv.difOpType());
        nopv.leftOperand()=new Operand(opk,aopk);
        nopv.setStructure();
        //nopv.updateReturnedType(aopk, opk.valueType(), opk.strucType(), opk.dims(), true);
    }
   else  //take opu aop opker as syntax
    {
      if(opu.leftOperand()!=nullptr || opu.rightOperand()!=nullptr)
      {
        where("toOperatorOnUnknown(const KernelOperatorOnUnknowns");
        error("operator_too_complex");
      }
       aopk= kopus.algopv();
       nopv= OperatorOnUnknown(*u,opu.difOpType());
       nopv.rightOperand()=new Operand(opk,aopk);
       nopv.setStructure();
       //nopv.updateReturnedType(aopk, opk.valueType(), opk.strucType(), opk.dims(), false);
    }
   return nopv;
}

std::ostream& operator<<(std::ostream&, const KernelOperatorOnUnknowns&);   //!< outputs OperatorOnUnknown attributes

} // end of namespace xlifepp
