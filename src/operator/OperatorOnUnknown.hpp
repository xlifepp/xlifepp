/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file OperatorOnUnknown.hpp
  \author E. Lunéville
  \since 02 mar 2012
  \date 9 may 2012

  \brief Definition of the xlifepp::OperatorOnUnknown class

  class xlifepp::OperatorOnUnknown stores the action of a differential operator on an unknown or test function
  the general following form is accepted:
  \verbatim
  left_operand left_op diff_op(unknown) right_op right_operand
  \endverbatim

  where
    - diff_op is a differential operator of type defined in the DifferentialOperator class
    - left_op and right_op are algebraic operators of type product (*), inner product (|), cross product (^) or contracted product (%)
    - left_operand and right_operand are either Function object or Value object (see related classes)

  All the attributes may be omitted, except unknown!

  For instance, the following syntaxes are available
  \verbatim
  u                       (trivial form)
  grad(u), div(u), ...    (simple form)
  A*grad(u)               (A a constant scalar or matrix  or a function returning a scalar or a Matrix)
  F|grad(u)               (F a constant vector or a function returning a vector)
  (A*grad(u))|F           (a combination of the two previous forms)
  \endverbatim

  The left and right data may be conjugate or transpose:  (tran(A)*grad(u))|conj(F)
  but not the unknown and not the whole expression or a part of it

  It is possible to use predefined vectors such as the normal vector _n
  As these forms are processed using overloaded operators, be cautious with the C++ operator priority.
  In doubt, use paranthesis.

  Note that some compatibility tests are performed but not all possible.
  Generally, structure (scalar, vector or matrix) consistancy is checked but not the dimension consistancy!
  Therefore, it is the responsability of the user to insure the consistancy of his expression

  The class LcOperatorOnUnknown handles linear combination of OperatorOnUnknown's
  and inherits from std::vector<OpuValPair> where OpuValPair is an alias of std::pair<OperatorOnUnknown*, complex_t>
*/

#ifndef OPERATOR_ON_UNKNOWN_HPP
#define OPERATOR_ON_UNKNOWN_HPP

#include "config.h"
#include "DifferentialOperator.hpp"
#include "Operand.hpp"
#include "space.h"

namespace xlifepp
{

//forward declaration
class EssentialCondition;
class TermVector;

/*!
   \class OperatorOnUnknown
   describes the operations to process on unknown.
   Unknown may be scalar or vector
*/
class OperatorOnUnknown
{
  protected:
    const Unknown* u_p;                               //!< unknown involved in operator
    bool conjugateUnknown_;                           //!< true if the unknown has to be conjugated
    DifferentialOperator* difOp_p;                    //!< differential operator involved in operator
    Operand* leftOperand_p;                           //!< object before the diff operator
    Operand* rightOperand_p;                          //!< object after the diff operator
    std::vector<complex_t> coefs_;                    //!< coefficients for generalized operator (gradG, divG, curlG, epsilonG)
    ValueType type_;                                  //!< type of returned value (one among _real, _complex)
    StrucType struct_;                                //!< structure of returned value (one among _scalar, _vector, _matrix)
    bool leftPriority_;                               //!< priority order flag (true if left operand is applied first)
    dimPair dimsRes_;                                 //!< dimension of result, (0,0) if unset
    GeomDomain* domain_p;                             //!< restriction domain pointer (0 by default)
    JumpType jumpType_;                               //!< apply jump or mean to operator on unknown

    void setReturnedType(const Unknown*, DiffOpType);                                //!< set returned type

  public:
    // constructors/destructor
    OperatorOnUnknown(const Unknown* un = nullptr, DiffOpType = _id); //!< default constructor
    OperatorOnUnknown(const Unknown&, DiffOpType = _id);        //!< constructor with differential operator
    OperatorOnUnknown(const Unknown&, DiffOpType, const std::vector<complex_t>&);//!< constructor with generalized differential operator
    OperatorOnUnknown(const Unknown&, const Function&, AlgebraicOperator, bool); //!< useful constructor: (fun op u) or (u op fun)
    OperatorOnUnknown(const Unknown&, const OperatorOnFunction&, AlgebraicOperator, bool); //!< useful constructor: (opfun op u) or (u op opfun)
    OperatorOnUnknown(const Unknown&, const Value&, AlgebraicOperator, bool);    //!< useful constructor: (val op u) or (u op val)
    OperatorOnUnknown(const OperatorOnUnknown&);                                 //!< copy constructor
    ~OperatorOnUnknown();                                                        //!< destructor
    OperatorOnUnknown& operator=(const OperatorOnUnknown&);                      //!< assignment operator

    void updateReturnedType(AlgebraicOperator, ValueType, StrucType, dimPair, bool); //!< update value and struct returned
    void setStructure();                                                             //!< set returned type, structure, dims of returned value

    //accessors
    const Unknown* unknown() const {return u_p;}           //!< return the unknown pointer
    bool conjugateUnknown() const {return conjugateUnknown_;} //!< returns true if the unknown has to be conjugated
    GeomDomain* domain() const {return domain_p;}          //!< return the restriction domain pointer (read)
    GeomDomain*& domain() {return domain_p;}               //!< return the restriction domain pointer (write)
    DiffOpType difOpType() const {return difOp_p->type();} //!< return type of differential operator
    DifferentialOperator*& difOp_() {return difOp_p;}      //!< return pointer to differential operator
    DifferentialOperator& difOp() const {return *difOp_p;} //!< return reference to differential operator
    ValueType& valueType() {return type_;}                 //!< return the operator returned type (non const)
    ValueType valueType()const {return type_;}             //!< return the operator returned type
    StrucType strucType() const {return struct_;}          //!< return the operator returned structure
    dimPair dimsRes() const {return dimsRes_;}             //!< return the dimension of returned values
    Operand*& leftOperand() {return leftOperand_p;}        //!< return pointer to left operand
    Operand*& rightOperand() {return rightOperand_p;}      //!< return pointer to right operand
    const Operand* leftOperand() const                     //! return pointer to left operand
    {return leftOperand_p;}
    const Operand* rightOperand() const                    //! return pointer to left operand
    {return rightOperand_p;}
    std::vector<complex_t>& coefs() {return coefs_;}       //!< return coefs vector (write)
    const std::vector<complex_t>& coefs() const {return coefs_;}  //!< return coefs vector (read)
    bool leftPriority() const {return leftPriority_;}      //!< return the priority flag
    bool& leftPriority() {return leftPriority_;}           //!< return the priority flag
    dimen_t diffOrder() const;                             //!< order of differential operator
    JumpType jumpType() const {return jumpType_;}
    JumpType& jumpType() {return jumpType_;}
    bool normalRequired() const;                           //!< true if normal involved
    bool xnormalRequired() const;                          //!< true if xnormal involved
    bool ynormalRequired() const;                          //!< true if ynormal involved
    bool xnormalRequiredByOpKernel() const;                //!< true if xnormal is required by operator on Kernel
    bool ynormalRequiredByOpKernel() const;                //!< true if xnormal is required by operator on Kernel
    bool tangentRequired() const;                          //!< true if tangent involved
    bool xtangentRequired() const;                         //!< true if xtangent involved
    bool ytangentRequired() const;                         //!< true if ytangent involved
    bool xtangentRequiredByOpKernel() const;               //!< true if xtangent is required by operator on Kernel
    bool ytangentRequiredByOpKernel() const;               //!< true if xtangent is required by operator on Kernel
    bool extensionRequired() const                         //! true if operator involve non-tangential derivatives or space has no FE trace space
    {return difOp_p->extensionRequired() || u_p->extensionRequired();}
    bool elementRequired() const;                          //!< true if normal is required by operator

    // utilities
    number_t unknownDegree() const;                        //!< degree of unknown (basis function)
    number_t degree() const;                               //!< degree of (derivative) unknown (basis function)
    bool hasOperand() const;                               //!< true if operator involves left or right operand
    bool hasFunction() const;                              //!< true if operator involves a user function or kernel
    bool hasKernel() const;                                //!< true if operator involves a user Kernel
    bool hasFun() const;                                   //!< true if operator involves a user Function
    bool isDG() const                                      //!  true if operator involves jump or mean
    {return jumpType_==_jump || jumpType_==_mean;}
    bool isId() const                                      //!  true if id
    {return difOp_p->type()==_id && leftOperand_p == nullptr && rightOperand_p ==nullptr;}
    const Function* functionp() const;                     //!< direct access to Function if unique
    const Kernel* kernelp() const;                         //!< direct access to Kernel
    const OperatorOnKernel* opkernelp() const;             //!< direct access to operator on Kernel
    void changeKernel(Kernel*);                            //!< change Kernel in operator
    void setUnknown(const Unknown& u) {u_p=&u;}            //!< set (change) the unknowns

    bool hasExtension() const                              //! true if handles an extension
    {if((leftOperand_p!=nullptr && leftOperand_p->hasExtension())
     || (rightOperand_p!=nullptr && rightOperand_p->hasExtension())) return true;
     return false;}

   const Extension* extension() const                      //! return extension pointer, 0 if there is no extension
    { const Extension* ext=nullptr;
      if(leftOperand_p!=nullptr) ext=leftOperand_p->extension();
      if(ext==nullptr && rightOperand_p!=nullptr) ext=rightOperand_p->extension();
      return ext;}

    //transmit informations to user function or user kernel
    void setX(const Point& P) const                                         //! set X point if Function involved (usefull for Kernel)
    {if(leftOperand_p!=nullptr) leftOperand_p->setX(P);
     if(rightOperand_p!=nullptr) rightOperand_p->setX(P);}
    void setY(const Point& P) const                                         //! set Y point if Function involved (usefull for Kernel)
    {if(leftOperand_p!=nullptr) leftOperand_p->setY(P);
     if(rightOperand_p!=nullptr) rightOperand_p->setY(P);}

    //Equation constructor (implemented in LcOperatorOnUnknown.cpp)
    EssentialCondition operator = (const real_t &);                         //!< construct equation Op(u)=r
    EssentialCondition operator = (const complex_t &);                      //!< construct equation Op(u)=c
    EssentialCondition operator = (const Function &);                       //!< construct equation Op(u)=f
    EssentialCondition operator = (const TermVector &);                     //!< construct equation Op(u)=TermVector (implemented in TermVector.cpp)

    template <typename T,typename K>
    void eval(const std::vector<K>&,
              const std::vector< std::vector<K> >&,
              const std::vector< std::vector<K> >&,
              dimen_t, Vector<T>&, dimen_t&, dimen_t&,
              const Vector<real_t>* = 0 ) const;                            //!< evaluate operator from shape values (no function to evaluate)

//    template <typename T,typename K>
//    void eval(const Point&, const std::vector<K>&,
//              const std::vector< std::vector<K> >&,
//              dimen_t, Vector<T>&, dimen_t&, dimen_t&,
//              const Vector<real_t>* = 0 ) const;                          //!< evaluate operator from point and shape values (function to evaluate)
    template <typename T,typename K>
    void eval(const Point& p, const std::vector<K>&,
              const std::vector< std::vector<K> >&,
              const std::vector< std::vector<K> >&,
              dimen_t, Vector<T>&, dimen_t&, dimen_t&,
              const Vector<real_t>* =nullptr, const ExtensionData* =nullptr) const;    //!< evaluate operator from point and shape values (function to evaluate)

    template <typename T,typename K>
    void eval(const Point&,const Point&, const std::vector<K>&,
              const std::vector< std::vector<K> >&,
              const std::vector< std::vector<K> >&,
              dimen_t, Vector<T>&, dimen_t&, dimen_t&,
              const Vector<real_t>* = nullptr, const Vector<real_t>* = 0  ) const;  //!< evaluate operator from point and shape values (kernel to evaluate)

    void print(std::ostream&) const;                                          //!< print attributes on stream
    void print(PrintStream& os) const {print(os.currentStream());}
    void printsymbolic(std::ostream &) const;                                 //!< print attributes in symbolic form on stream
    void printsymbolic(PrintStream& os) const {printsymbolic(os.currentStream());}
    string_t asString() const;                                                //!< return as symbolic string
}; // end of class OperatorOnUnknown

// external functions
bool checkConsistancy(const OperatorOnUnknown&, AlgebraicOperator, const OperatorOnUnknown&); //!< check opu aop opv consistancy
bool operator==(const OperatorOnUnknown&, const OperatorOnUnknown&);//!< compare OperatorOnUnknown (same unknowns, same diff operators, same functions ...)
bool operator!=(const OperatorOnUnknown&, const OperatorOnUnknown&);//!< compare OperatorOnUnknown (same unknowns, same diff operators, same functions ...)

//=================================================================================
//@{
//! "differential" operators applied to unknown
OperatorOnUnknown& id(const Unknown&);
OperatorOnUnknown& d0(const Unknown&);
OperatorOnUnknown& dt(const Unknown&);
OperatorOnUnknown& d1(const Unknown&);
OperatorOnUnknown& dx(const Unknown&);
OperatorOnUnknown& d2(const Unknown&);
OperatorOnUnknown& dy(const Unknown&);
OperatorOnUnknown& d3(const Unknown&);
OperatorOnUnknown& dz(const Unknown&);
OperatorOnUnknown& grad(const Unknown&);
OperatorOnUnknown& nabla(const Unknown&);
OperatorOnUnknown& div(const Unknown&);
OperatorOnUnknown& curl(const Unknown&);
OperatorOnUnknown& rot(const Unknown&);
OperatorOnUnknown& gradS(const Unknown&);
OperatorOnUnknown& nablaS(const Unknown&);
OperatorOnUnknown& divS(const Unknown&);
OperatorOnUnknown& curlS(const Unknown&);
OperatorOnUnknown& rotS(const Unknown&);
OperatorOnUnknown& nx(const Unknown&);
OperatorOnUnknown& ndot(const Unknown&);
OperatorOnUnknown& ndotgrad(const Unknown&);
OperatorOnUnknown& ncrossgrad(const Unknown&);
OperatorOnUnknown& ncross(const Unknown&);
OperatorOnUnknown& ncrossncross(const Unknown&);
OperatorOnUnknown& ncrossntimes(const Unknown&);
OperatorOnUnknown& ncrossndot(const Unknown&);
OperatorOnUnknown& ntimesndot(const Unknown&);
OperatorOnUnknown& ndiv(const Unknown&);
OperatorOnUnknown& ncrosscurl(const Unknown&);
OperatorOnUnknown& ncrossrot(const Unknown&);
OperatorOnUnknown& epsilon(const Unknown&);
OperatorOnUnknown& epsilonR(const Unknown&);
OperatorOnUnknown& voigtToM(const Unknown&);   //special operator: convert Voigt vector to tensor matrix
OperatorOnUnknown& gradG(const Unknown& un, const complex_t& ax, const complex_t& ay = complex_t(0., 0.),
                         const complex_t& az = complex_t(0., 0.) , const complex_t& at = complex_t(0., 0.));
OperatorOnUnknown& nablaG(const Unknown& un, const complex_t& ax, const complex_t& ay = complex_t(0., 0.),
                          const complex_t& az = complex_t(0., 0.) , const complex_t& at = complex_t(0., 0.));
OperatorOnUnknown& divG(const Unknown& un, const complex_t& ax, const complex_t& ay = complex_t(0., 0.),
                        const complex_t& az = complex_t(0., 0.) , const complex_t& at = complex_t(0., 0.));
OperatorOnUnknown& curlG(const Unknown& un, const complex_t& ax, const complex_t& ay = complex_t(0., 0.),
                         const complex_t& az = complex_t(0., 0.) , const complex_t& at = complex_t(0., 0.));
OperatorOnUnknown& rotG(const Unknown& un, const complex_t& ax, const complex_t& ay = complex_t(0., 0.),
                        const complex_t& az = complex_t(0., 0.) , const complex_t& at = complex_t(0., 0.));
OperatorOnUnknown& epsilonG(const Unknown& un, const complex_t& id, const complex_t& ax = complex_t(0., 0.),
                            const complex_t& ay = complex_t(0., 0.) , const complex_t& az = complex_t(0., 0.));
OperatorOnUnknown& mean(const Unknown&);
OperatorOnUnknown& jump(const Unknown&);
OperatorOnUnknown& mean(OperatorOnUnknown&);
OperatorOnUnknown& jump(OperatorOnUnknown&);
// second derivative operator
OperatorOnUnknown& dxx(const Unknown&);
OperatorOnUnknown& d11(const Unknown&);
OperatorOnUnknown& dyy(const Unknown&);
OperatorOnUnknown& d22(const Unknown&);
OperatorOnUnknown& dzz(const Unknown&);
OperatorOnUnknown& d33(const Unknown&);
OperatorOnUnknown& dxy(const Unknown&);
OperatorOnUnknown& d12(const Unknown&);
OperatorOnUnknown& dxz(const Unknown&);
OperatorOnUnknown& d13(const Unknown&);
OperatorOnUnknown& dyz(const Unknown&);
OperatorOnUnknown& d23(const Unknown&);
OperatorOnUnknown& lap(const Unknown&);
OperatorOnUnknown& lapG(const Unknown&, const complex_t& axx, const complex_t& ayy, const complex_t& azz=0.); // lapG(u)= axx*dxx+ayy*dyy+azz*dzz
OperatorOnUnknown& d2G (const Unknown&, const Matrix<complex_t>& a);                      // d2G = sum aij*dij, d x d matrix (aij) may be not symmetric !
OperatorOnUnknown& d2G (const Unknown&, const complex_t& axx, const complex_t& axy, const complex_t& ayy,
                       const complex_t& axz=0., const complex_t& ayz=0.,const complex_t& azz=0.); // d2G = sum aij*dij, d x d matrix (aij) symmetric, lower matrix!
//@}

// for syntax difop&Unknown (for instance _grad&u means grad(u))
// OperatorOnUnknown& operator&(DiffOpType,const Unknown &);

//==========================================================================================
// equivalent differential operator with like mathematical forms (UnitaryVector and Unknown)
//==========================================================================================
OperatorOnUnknown& operator*(UnitaryVector, const Unknown&);     //!< n*u same as nx(u)
OperatorOnUnknown& operator|(UnitaryVector, const Unknown&);     //!< n|u same as ndot(u)
OperatorOnUnknown& operator^(UnitaryVector, const Unknown&);     //!< n^u same as ncross(u)
OperatorOnUnknown& operator^(const Unknown&, UnitaryVector);     //!< u^n same as -ncross(u)
OperatorOnUnknown& operator*(const Unknown&, UnitaryVector);     //!< u*n same as nx(u)
OperatorOnUnknown& operator|(const Unknown&, UnitaryVector);     //!< u|n same as ndot(u)
OperatorOnUnknown& operator|(UnitaryVector, OperatorOnUnknown&); //!< n|grad(u) same as ndotgrad(u)
OperatorOnUnknown& operator^(UnitaryVector, OperatorOnUnknown&); //!< n^(n^u) or n^curl(u) same as ncrossncross(u) or ncrosscurl(u)
OperatorOnUnknown& operator^(OperatorOnUnknown&, UnitaryVector); //!< -n^(n^u) or -n^curl(u) same as -ncrossncross(u) or -ncrosscurl(u)
OperatorOnUnknown& operator*(UnitaryVector, OperatorOnUnknown&); //!< n*div same as ndiv(u)
OperatorOnUnknown& operator|(OperatorOnUnknown&, UnitaryVector); //!< grad(u)|n same as ndotgrad(u)
OperatorOnUnknown& operator*(OperatorOnUnknown&, UnitaryVector); //!< div*n same as ndiv(u)

//==========================================================================================
// left and right operations with Function and Unknown
//==========================================================================================
OperatorOnUnknown&  operator*(const Unknown&, const Function&); //!< u*F
OperatorOnUnknown&  operator|(const Unknown&, const Function&); //!< u|F
OperatorOnUnknown&  operator^(const Unknown&, const Function&); //!< u^F
OperatorOnUnknown&  operator%(const Unknown&, const Function&); //!< u%F
OperatorOnUnknown&  operator*(const Function&, const Unknown&); //!< F*u
OperatorOnUnknown&  operator|(const Function&, const Unknown&); //!< F|u
OperatorOnUnknown&  operator^(const Function&, const Unknown&); //!< F^u
OperatorOnUnknown&  operator%(const Function&, const Unknown&); //!< F%u

//==========================================================================================
// left and right operations with OperatorOnFunction and Unknown
//==========================================================================================
OperatorOnUnknown&  operator*(const Unknown&, const OperatorOnFunction&); //!< u*op(F)
OperatorOnUnknown&  operator|(const Unknown&, const OperatorOnFunction&); //!< u|op(F)
OperatorOnUnknown&  operator^(const Unknown&, const OperatorOnFunction&); //!< u^op(F)
OperatorOnUnknown&  operator%(const Unknown&, const OperatorOnFunction&); //!< u%op(F)
OperatorOnUnknown&  operator*(const OperatorOnFunction&, const Unknown&); //!< op(F)*u
OperatorOnUnknown&  operator|(const OperatorOnFunction&, const Unknown&); //!< op(F)|u
OperatorOnUnknown&  operator^(const OperatorOnFunction&, const Unknown&); //!< op(F)^u
OperatorOnUnknown&  operator%(const OperatorOnFunction&, const Unknown&); //!< op(F)%u

//==========================================================================================
// left and right operations with C++ user functions and Unknown
// user shortcuts, avoid using explicit Function encapsulation
// only function, no kernel (see below)
//==========================================================================================
template <typename T>
OperatorOnUnknown& operator*(const Unknown& un, T(fun)(const Point&, Parameters&)) //! u * function(Point,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return un * f;
}
template <typename T>
OperatorOnUnknown& operator*(T(fun)(const Point&, Parameters&), const Unknown& un) //! function(Point,Parameters) * u
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f * un;
}
template <typename T>
OperatorOnUnknown& operator|(const Unknown& un, T(fun)(const Point&, Parameters&)) //! u | function(Point,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return un | f;
}
template <typename T>
OperatorOnUnknown& operator|(T(fun)(const Point&, Parameters&), const Unknown& un) //! function(Point,Parameters) | u
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f | un;
}
template <typename T>
OperatorOnUnknown& operator^(const Unknown& un, T(fun)(const Point&, Parameters&)) //! u ^ function(Point,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return un ^ f;
}
template <typename T>
OperatorOnUnknown& operator^(T(fun)(const Point&, Parameters&), const Unknown& un) //! function(Point,Parameters) ^ u
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f ^ un;
}
template <typename T>
OperatorOnUnknown& operator%(const Unknown& un, T(fun)(const Point&, Parameters&)) //! u % function(Point,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return un % f;
}
template <typename T>
OperatorOnUnknown& operator%(T(fun)(const Point&, Parameters&), const Unknown& un) //! function(Point,Parameters) % u
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f % un;
}

// same for vector function (Vector<Point> as argument)
template <typename T>
OperatorOnUnknown& operator*(const Unknown& un, T(fun)(const Vector<Point>&, Parameters&)) //! u * function(Vector<Point>,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return un * f;
}
template <typename T>
OperatorOnUnknown& operator*(T(fun)(const Vector<Point>&, Parameters&), const Unknown& un) //! function(Vector<Point>,Parameters) * u
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f * un;
}
template <typename T>
OperatorOnUnknown& operator|(const Unknown& un, T(fun)(const Vector<Point>&, Parameters&)) //! u | function(Vector<Point>,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return un | f;
}
template <typename T>
OperatorOnUnknown& operator|(T(fun)(const Vector<Point>&, Parameters&), const Unknown& un) //! function(Vector<Point>,Parameters) | u
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f | un;
}
template <typename T>
OperatorOnUnknown& operator^(const Unknown& un, T(fun)(const Vector<Point>&, Parameters&)) //! u ^ function(Vector<Point>,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return un ^ f;
}
template <typename T>
OperatorOnUnknown& operator^(T(fun)(const Vector<Point>&, Parameters&), const Unknown& un) //! function(Vector<Point>,Parameters) ^ u
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f ^ un;
}
template <typename T>
OperatorOnUnknown& operator%(const Unknown& un, T(fun)(const Vector<Point>&, Parameters&)) //! u % function(Vector<Point>,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return un % f;
}
template <typename T>
OperatorOnUnknown& operator%(T(fun)(const Vector<Point>&, Parameters&), const Unknown& un) //! function(Vector<Point>,Parameters) % u
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f % un;
}

//==========================================================================================
// left and right operations with Value and Unknown
//==========================================================================================
OperatorOnUnknown&  operator*(const Unknown&, const Value&); //!< u*val
OperatorOnUnknown&  operator|(const Unknown&, const Value&); //!< u|val
OperatorOnUnknown&  operator^(const Unknown&, const Value&); //!< u^val
OperatorOnUnknown&  operator%(const Unknown&, const Value&); //!< u%val
OperatorOnUnknown&  operator*(const Value&, const Unknown&); //!< val*u
OperatorOnUnknown&  operator|(const Value&, const Unknown&); //!< val|u
OperatorOnUnknown&  operator^(const Value&, const Unknown&); //!< val^u
OperatorOnUnknown&  operator%(const Value&, const Unknown&); //!< val%u

//==========================================================================================
// left and right operations with any value and Unknown
// to avoid intricated conflict we do not use templated operators
//==========================================================================================
template<typename T>
OperatorOnUnknown& fromUnknownVal(const Unknown& un, const T& val, AlgebraicOperator aop)
{
  OperatorOnUnknown* opu = new OperatorOnUnknown(&un, _id);
  updateRight(*opu, val, aop);
  return *opu;
}

template<typename T>
OperatorOnUnknown& fromValUnknown(const Unknown& un, const T& val, AlgebraicOperator aop)
{
  OperatorOnUnknown* opu = new OperatorOnUnknown(&un, _id);
  updateLeft(*opu, val, aop);
  return *opu;
}

//unknown op r or r op unknown
OperatorOnUnknown& operator*(const Unknown&, const real_t&);  //!< u*r
OperatorOnUnknown& operator|(const Unknown&, const real_t&);  //!< u|r
OperatorOnUnknown& operator^(const Unknown&, const real_t&);  //!< u^r
OperatorOnUnknown& operator%(const Unknown&, const real_t&);  //!< u%r
OperatorOnUnknown& operator*(const real_t&, const Unknown&);  //!< r*u
OperatorOnUnknown& operator|(const real_t&, const Unknown&);  //!< r|u
OperatorOnUnknown& operator^(const real_t&, const Unknown&);  //!< r^u
OperatorOnUnknown& operator%(const real_t&, const Unknown&);  //!< r%u

//unknown op c or c op unknown
OperatorOnUnknown& operator*(const Unknown&, const complex_t&); //!< u*r
OperatorOnUnknown& operator|(const Unknown&, const complex_t&); //!< u|c
OperatorOnUnknown& operator^(const Unknown&, const complex_t&); //!< u^c
OperatorOnUnknown& operator%(const Unknown&, const complex_t&); //!< u%c
OperatorOnUnknown& operator*(const complex_t&, const Unknown&); //!< c*u
OperatorOnUnknown& operator|(const complex_t&, const Unknown&); //!< c|u
OperatorOnUnknown& operator^(const complex_t&, const Unknown&); //!< c^u
OperatorOnUnknown& operator%(const complex_t&, const Unknown&); //!< c%u

//unknown op vector or vector op unknown
template<typename T>
OperatorOnUnknown&  operator*(const Unknown& un, const Vector<T>& val)  //! u*Vector
{return fromUnknownVal(un,val,_product);}
template<typename T>
OperatorOnUnknown&  operator|(const Unknown& un, const Vector<T>& val)  //! u|Vector
{return fromUnknownVal(un,val,_innerProduct);}
template<typename T>
OperatorOnUnknown&  operator^(const Unknown& un, const Vector<T>& val)  //! u^Vector
{return fromUnknownVal(un,val,_crossProduct);}
template<typename T>
OperatorOnUnknown&  operator%(const Unknown& un, const Vector<T>& val)  //! u%Vector
{return fromUnknownVal(un,val,_contractedProduct);}
template<typename T>
OperatorOnUnknown&  operator*(const Vector<T>& val, const Unknown& un)  //! Vector*u
{return fromValUnknown(un,val,_product);}
template<typename T>
OperatorOnUnknown&  operator|(const Vector<T>& val, const Unknown& un)  //! Vector|u
{return fromValUnknown(un,val,_innerProduct);}
template<typename T>
OperatorOnUnknown&  operator^(const Vector<T>& val, const Unknown& un)  //! Vector^u
{return fromValUnknown(un,val,_crossProduct);}
template<typename T>
OperatorOnUnknown&  operator%(const Vector<T>& val, const Unknown& un)  //! Vector%u
{return fromValUnknown(un,val,_contractedProduct);}

//unknown op matrix or matrix op unknown
template<typename T>
OperatorOnUnknown&  operator*(const Unknown& un, const Matrix<T>& val)  //! u*Matrix
{return fromUnknownVal(un,val,_product);}
template<typename T>
OperatorOnUnknown&  operator|(const Unknown& un, const Matrix<T>& val)  //! u|Matrix
{return fromUnknownVal(un,val,_innerProduct);}
template<typename T>
OperatorOnUnknown&  operator^(const Unknown& un, const Matrix<T>& val)  //! u^Matrix
{return fromUnknownVal(un,val,_crossProduct);}
template<typename T>
OperatorOnUnknown&  operator%(const Unknown& un, const Matrix<T>& val)  //! u%Matrix
{return fromUnknownVal(un,val,_contractedProduct);}
template<typename T>
OperatorOnUnknown&  operator*(const Matrix<T>& val, const Unknown& un)  //! Matrix*u
{return fromValUnknown(un,val,_product);}
template<typename T>
OperatorOnUnknown&  operator|(const Matrix<T>& val, const Unknown& un)  //! Matrix|u
{return fromValUnknown(un,val,_innerProduct);}
template<typename T>
OperatorOnUnknown&  operator^(const Matrix<T>& val, const Unknown& un)  //! Matrix^u
{return fromValUnknown(un,val,_crossProduct);}
template<typename T>
OperatorOnUnknown&  operator%(const Matrix<T>& val, const Unknown& un)  //! Matrix%u
{return fromValUnknown(un,val,_contractedProduct);}

//==========================================================================================
// left and right operation with Function:  op(u) aop Function or Function aop op(u)
//==========================================================================================
OperatorOnUnknown& updateRight(OperatorOnUnknown&, const Function&, AlgebraicOperator); //!< update Op(u)*F operation
OperatorOnUnknown& updateLeft(OperatorOnUnknown&, const Function&, AlgebraicOperator);  //!< update F*Op(u) operation
OperatorOnUnknown& operator*(OperatorOnUnknown&, const Function&);                      //!< product syntax Op(u)*F
OperatorOnUnknown& operator|(OperatorOnUnknown&, const Function&);                      //!< innerproduct syntax Op(u)|F
OperatorOnUnknown& operator^(OperatorOnUnknown&, const Function&);                      //!< cross product syntax Op(u)^F
OperatorOnUnknown& operator%(OperatorOnUnknown&, const Function&);                      //!< contracted product syntax Op(u)%F
OperatorOnUnknown& operator*(const Function&, OperatorOnUnknown&);                      //!< product syntax F*Op(u)
OperatorOnUnknown& operator|(const Function&, OperatorOnUnknown&);                      //!< innerproduct syntax F|Op(u)
OperatorOnUnknown& operator^(const Function&, OperatorOnUnknown&);                      //!< cross product syntax F^Op(u)
OperatorOnUnknown& operator%(const Function&, OperatorOnUnknown&);                      //!< contracted product syntax F%Op(u)

//==========================================================================================
// left and right operation with Function:  op(u) aop op(F) or op(F) aop op(u)
//==========================================================================================
OperatorOnUnknown& updateRight(OperatorOnUnknown&, const OperatorOnFunction&, AlgebraicOperator); //!< update Op(u)*op(F) operation
OperatorOnUnknown& updateLeft(OperatorOnUnknown&, const OperatorOnFunction&, AlgebraicOperator);  //!< update op(F)*Op(u) operation
OperatorOnUnknown& operator*(OperatorOnUnknown&, const OperatorOnFunction&);                      //!< product syntax Op(u)*op(F)
OperatorOnUnknown& operator|(OperatorOnUnknown&, const OperatorOnFunction&);                      //!< innerproduct syntax Op(u)|op(F)
OperatorOnUnknown& operator^(OperatorOnUnknown&, const OperatorOnFunction&);                      //!< cross product syntax Op(u)^op(F)
OperatorOnUnknown& operator%(OperatorOnUnknown&, const OperatorOnFunction&);                      //!< contracted product syntax Op(u)%op(F)
OperatorOnUnknown& operator*(const OperatorOnFunction&, OperatorOnUnknown&);                      //!< product syntax op(F)*Op(u)
OperatorOnUnknown& operator|(const OperatorOnFunction&, OperatorOnUnknown&);                      //!< innerproduct syntax op(F)|Op(u)
OperatorOnUnknown& operator^(const OperatorOnFunction&, OperatorOnUnknown&);                      //!< cross product syntax op(F)^Op(u)
OperatorOnUnknown& operator%(const OperatorOnFunction&, OperatorOnUnknown&);                      //!< contracted product syntax op(F)F%Op(u)

//==========================================================================================
// left and right operation with Value:  op(u) aop Value or Value aop op(u)
//==========================================================================================
OperatorOnUnknown& updateRight(OperatorOnUnknown&, const Value&, AlgebraicOperator); //!< update Op(u)*V operation
OperatorOnUnknown& updateLeft(OperatorOnUnknown&, const Value&, AlgebraicOperator);  //!< update V*Op(u) operation
OperatorOnUnknown& operator*(OperatorOnUnknown&, const Value&);                      //!< product syntax Op(u)*V
OperatorOnUnknown& operator|(OperatorOnUnknown&, const Value&);                      //!< innerproduct syntax Op(u)|V
OperatorOnUnknown& operator^(OperatorOnUnknown&, const Value&);                      //!< cross product syntax Op(u)^V
OperatorOnUnknown& operator%(OperatorOnUnknown&, const Value&);                      //!< contracted product syntax Op(u)%V
OperatorOnUnknown& operator*(const Value&, OperatorOnUnknown&);                      //!< product syntax V*Op(u)
OperatorOnUnknown& operator|(const Value&, OperatorOnUnknown&);                      //!< innerproduct syntax V|Op(u)
OperatorOnUnknown& operator^(const Value&, OperatorOnUnknown&);                      //!< cross product syntax V^Op(u)
OperatorOnUnknown& operator%(const Value&, OperatorOnUnknown&);                      //!< contracted product syntax V%Op(u)

//==========================================================================================
// update left and right operation with anything:  op(u) aop anything or anything aop op(u)
//==========================================================================================
template<typename T> OperatorOnUnknown& updateRight(OperatorOnUnknown& opu, const T& val, AlgebraicOperator o)
{
  Value::checkTypeInList(val); // check the type of val
  if(opu.rightOperand() != nullptr) { error("operator_sideisdef", words("right")); }
  opu.rightOperand() = new Operand(val, o);
//  opu.updateReturnedType(o, opu.rightOperand()->value().valueType(),
//                         opu.rightOperand()->value().strucType(), opu.rightOperand()->value().dims(), false);
  if(opu.leftOperand() == nullptr) { opu.leftPriority() = false; }
  opu.setStructure();
  return opu;
}

template<typename T> OperatorOnUnknown& updateLeft(OperatorOnUnknown& opu, const T& val, AlgebraicOperator o)
{
  Value::checkTypeInList(val); // check the type of val
  if(opu.leftOperand() != nullptr) { error("operator_sideisdef", words("left")); }
  opu.leftOperand() = new Operand(val, o);
//  opu.updateReturnedType(o, opu.leftOperand()->value().valueType(),
//                         opu.leftOperand()->value().strucType(), opu.leftOperand()->value().dims(),true);
  if(opu.rightOperand() == nullptr) { opu.leftPriority() = true; }
  opu.setStructure();
  return opu;
}

// left and right operation with real scalar:  op(u) aop r or r aop op(u)
OperatorOnUnknown& operator*(OperatorOnUnknown&, const real_t&);
OperatorOnUnknown& operator|(OperatorOnUnknown&, const real_t&);
OperatorOnUnknown& operator^(OperatorOnUnknown&, const real_t&);
OperatorOnUnknown& operator%(OperatorOnUnknown&, const real_t&);
OperatorOnUnknown& operator*(const real_t&, OperatorOnUnknown&);
OperatorOnUnknown& operator|(const real_t&, OperatorOnUnknown&);
OperatorOnUnknown& operator^(const real_t&, OperatorOnUnknown&);
OperatorOnUnknown& operator%(const real_t&, OperatorOnUnknown&);

// left and right operation with complex scalar:  op(u) aop c or c aop op(u)
OperatorOnUnknown& operator*(OperatorOnUnknown&, const complex_t&);
OperatorOnUnknown& operator|(OperatorOnUnknown&, const complex_t&);
OperatorOnUnknown& operator^(OperatorOnUnknown&, const complex_t&);
OperatorOnUnknown& operator%(OperatorOnUnknown&, const complex_t&);
OperatorOnUnknown& operator*(const complex_t&, OperatorOnUnknown&);
OperatorOnUnknown& operator|(const complex_t&, OperatorOnUnknown&);
OperatorOnUnknown& operator^(const complex_t&, OperatorOnUnknown&);
OperatorOnUnknown& operator%(const complex_t&, OperatorOnUnknown&);

// left and right operation with vector<T> :  op(u) aop vector or vector aop op(u)
template<typename T> OperatorOnUnknown& operator*(OperatorOnUnknown& opu, const Vector<T>& val)
{return updateRight(opu, val, _product);}
template<typename T> OperatorOnUnknown& operator|(OperatorOnUnknown& opu, const Vector<T>& val)
{return updateRight(opu, val, _innerProduct);}
template<typename T> OperatorOnUnknown& operator^(OperatorOnUnknown& opu, const Vector<T>& val)
{return updateRight(opu, val, _crossProduct);}
template<typename T> OperatorOnUnknown& operator%(OperatorOnUnknown& opu, const Vector<T>& val)
{return updateRight(opu, val, _contractedProduct);}
template<typename T> OperatorOnUnknown& operator*(const Vector<T>& val, OperatorOnUnknown& opu)
{return updateLeft(opu, val, _product);}
template<typename T> OperatorOnUnknown& operator|(const Vector<T>& val, OperatorOnUnknown& opu)
{return updateLeft(opu, val, _innerProduct);}
template<typename T> OperatorOnUnknown& operator^(const Vector<T>& val, OperatorOnUnknown& opu)
{return updateLeft(opu, val, _crossProduct);}
template<typename T> OperatorOnUnknown& operator%(const Vector<T>& val, OperatorOnUnknown& opu)
{return updateLeft(opu, val, _contractedProduct);}

// left and right operation with Matrix<T> :  op(u) aop matrix or matrix aop op(u)
template<typename T> OperatorOnUnknown& operator*(OperatorOnUnknown& opu, const Matrix<T>& val)
{return updateRight(opu, val, _product);}
template<typename T> OperatorOnUnknown& operator|(OperatorOnUnknown& opu, const Matrix<T>& val)
{return updateRight(opu, val, _innerProduct);}
template<typename T> OperatorOnUnknown& operator^(OperatorOnUnknown& opu, const Matrix<T>& val)
{return updateRight(opu, val, _crossProduct);}
template<typename T> OperatorOnUnknown& operator%(OperatorOnUnknown& opu, const Matrix<T>& val)
{return updateRight(opu, val, _contractedProduct);}
template<typename T> OperatorOnUnknown& operator*(const Matrix<T>& val, OperatorOnUnknown& opu)
{return updateLeft(opu, val, _product);}
template<typename T> OperatorOnUnknown& operator|(const Matrix<T>& val, OperatorOnUnknown& opu)
{return updateLeft(opu, val, _innerProduct);}
template<typename T> OperatorOnUnknown& operator^(const Matrix<T>& val, OperatorOnUnknown& opu)
{return updateLeft(opu, val, _crossProduct);}
template<typename T> OperatorOnUnknown& operator%(const Matrix<T>& val, OperatorOnUnknown& opu)
{return updateLeft(opu, val, _contractedProduct);}

// left and right operation with anything:  op(u) aop anything or anything aop op(u)
//template<typename T> OperatorOnUnknown& operator*(OperatorOnUnknown& opu, const T& val)
//{return updateRight(opu, val, _product);}
//template<typename T> OperatorOnUnknown& operator|(OperatorOnUnknown& opu, const T& val)
//{return updateRight(opu, val, _innerProduct);}
//template<typename T> OperatorOnUnknown& operator^(OperatorOnUnknown& opu, const T& val)
//{return updateRight(opu, val, _crossProduct);}
//template<typename T> OperatorOnUnknown& operator%(OperatorOnUnknown& opu, const T& val)
//{return updateRight(opu, val, _contractedProduct);}
//template<typename T> OperatorOnUnknown& operator*(const T& val, OperatorOnUnknown& opu)
//{return updateLeft(opu, val, _product);}
//template<typename T> OperatorOnUnknown& operator|(const T& val, OperatorOnUnknown& opu)
//{return updateLeft(opu, val, _innerProduct);}
//template<typename T> OperatorOnUnknown& operator^(const T& val, OperatorOnUnknown& opu)
//{return updateLeft(opu, val, _crossProduct);}
//template<typename T> OperatorOnUnknown& operator%(const T& val, OperatorOnUnknown& opu)
//{return updateLeft(opu, val, _contractedProduct);}

//==========================================================================================
// left and right operations with C++ user functions and OperatorOnUnknown
// user shortcuts, avoid using explicit Function encapsulation
// only function, no kernel (see KernelOperatorOnUnknowns)
//==========================================================================================
template <typename T>
OperatorOnUnknown& operator*(OperatorOnUnknown& opu, T(fun)(const Point&, Parameters&)) //! opu * function(Point,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return opu * f;
}
template <typename T>
OperatorOnUnknown& operator*(T(fun)(const Point&, Parameters&), OperatorOnUnknown& opu) //! function(Point,Parameters) * opu
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f * opu;
}
template <typename T>
OperatorOnUnknown& operator|(OperatorOnUnknown& opu, T(fun)(const Point&, Parameters&)) //! opu | function(Point,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return opu | f;
}
template <typename T>
OperatorOnUnknown& operator|(T(fun)(const Point&, Parameters&), OperatorOnUnknown& opu) //! function(Point,Parameters) | opu
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f | opu;
}
template <typename T>
OperatorOnUnknown& operator^(OperatorOnUnknown& opu, T(fun)(const Point&, Parameters&)) //! opu ^ function(Point,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return opu ^ f;
}
template <typename T>
OperatorOnUnknown& operator^(T(fun)(const Point&, Parameters&), OperatorOnUnknown& opu) //! function(Point,Parameters) ^ opu
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f ^ opu;
}
template <typename T>
OperatorOnUnknown& operator%(OperatorOnUnknown& opu, T(fun)(const Point&, Parameters&)) //! opu % function(Point,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return opu % f;
}
template <typename T>
OperatorOnUnknown& operator%(T(fun)(const Point&, Parameters&), OperatorOnUnknown& opu) //! function(Point,Parameters) % opu
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f % opu;
}
template <typename T>
OperatorOnUnknown& operator*(OperatorOnUnknown& opu, T(fun)(const Vector<Point>&, Parameters&)) //! opu * function(Vector<Point>,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return opu * f;
}
template <typename T>
OperatorOnUnknown& operator*(T(fun)(const Vector<Point>&, Parameters&), OperatorOnUnknown& opu) //! function(Vector<Point>,Parameters) * opu
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f * opu;
}
template <typename T>
OperatorOnUnknown& operator|(OperatorOnUnknown& opu, T(fun)(const Vector<Point>&, Parameters&)) //! opu | function(Vector<Point>,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return opu | f;
}
template <typename T>
OperatorOnUnknown& operator|(T(fun)(const Vector<Point>&, Parameters&), OperatorOnUnknown& opu) //! function(Vector<Point>,Parameters) | opu
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f | opu;
}
template <typename T>
OperatorOnUnknown& operator^(OperatorOnUnknown& opu, T(fun)(const Vector<Point>&, Parameters&)) //! opu ^ function(Vector<Point>,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return opu ^ f;
}
template <typename T>
OperatorOnUnknown& operator^(T(fun)(const Vector<Point>&, Parameters&), OperatorOnUnknown& opu) //! function(Vector<Point>,Parameters) ^ opu
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f ^ opu;
}
template <typename T>
OperatorOnUnknown& operator%(OperatorOnUnknown& opu, T(fun)(const Vector<Point>&, Parameters&)) //! opu % function(Vector<Point>,Parameters)
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return opu % f;
}
template <typename T>
OperatorOnUnknown& operator%(T(fun)(const Vector<Point>&, Parameters&), OperatorOnUnknown& opu) //! function(Vector<Point>,Parameters) % opu
{
  Function& f = *(new Function(*fun)); // create a Function object on heap memory
  return f % opu;
}

//==========================================================================================
// domain restriction operation
//==========================================================================================
OperatorOnUnknown& operator|(OperatorOnUnknown&, const GeomDomain &);   //opu|dom
OperatorOnUnknown& operator|(const Unknown&, const GeomDomain &);        //u|dom

//------------------------------------------------------------------------------------------------------------
/*!
  evaluate operator from shape values and its derivatives
  sv: shape values
  dsv: shape derivatives
  dimFun: dimension of shape function
  val: operator values as a vector stored as following:
        O1 O2 .... Os   s number of shape functions
        Ok may be
         - a scalar
         - a vector O_k=[Ok1, Ok2, ..., Okn]
         - a matrix O_k=[Ok11, Ok12, ..., Ok1n, Ok21, Ok22, ..., Ok2n, ... Okm1, Okm2, ..., Okmn]
   d: block size
   m: number of matrix rows when returning some matrices
   np: pointer to the normal vector, 0 if no available normal vector

    T type of result val has to be consistent (no check)
    K type of shape functions

   There are 3 versions of eval corresponding to the cases:
   - no function to evaluate
   - function to evaluate at a point
   - kernel to evaluate at a pair of points
*/
//------------------------------------------------------------------------------------------------------------
template <typename T,typename K>
void OperatorOnUnknown::eval(const std::vector<K>& sv,
                             const std::vector< std::vector<K> >& dsv,
                             const std::vector< std::vector<K> >& d2sv,
                             dimen_t dimFun, Vector<T>& val, dimen_t& d, dimen_t& m,
                             const Vector<real_t>* np) const
{
  // compute differential operator
  number_t nbw=sv.size()/dimFun;
  d=dimFun;
  Vector<K> difop;
  difOp_p->eval(sv, dsv, d2sv, d, m, difop, np, coefs_);

  if(leftOperand_p == nullptr && rightOperand_p == nullptr) {val=difop; return;}
  if(leftOperand_p != nullptr && rightOperand_p == nullptr) {val=leftOperand_p->leftEval<T>(difop, d, m, nbw);return;}
  if(leftOperand_p == nullptr && rightOperand_p != nullptr) {val=rightOperand_p->rightEval<T>(difop, d, m, nbw);return;}
  val=leftOperand_p->leftEval<T>(rightOperand_p->rightEval<T>(difop, d, m, nbw), d, m, nbw);
}

// // evaluate operator from a point p and shape values sv and its derivatives dsv
// // (dimFun is the dimension of shape functions)
// // template <typename T,typename K>
// void OperatorOnUnknown::eval(const Point& p, const std::vector<K>& sv,
//                             const std::vector< std::vector<K> >& dsv,
//                             dimen_t dimFun, Vector<T>& val, dimen_t& d, dimen_t& m,
//                             const Vector<real_t>* np) const
// {
// // compute differential operator
//  number_t nbw=sv.size()/dimFun;
//  d=dimFun;
//  Vector<K> difop;
//  difOp_p->eval(sv, dsv, d, m, difop, np, coefs_);

//  //compute operator on unknown
//  if(leftOperand_p == nullptr && rightOperand_p == nullptr) {val=difop; return;}
//  if(leftOperand_p != nullptr && rightOperand_p == nullptr) {val=leftOperand_p->leftEval<T>(p, difop, d, m, nbw, np); return;}
//  if(leftOperand_p == nullptr && rightOperand_p != nullptr) {val=rightOperand_p->rightEval<T>(p, difop, d, m, nbw, np); return;}
//  if(leftOperand_p->isFunction()) val=leftOperand_p->leftEval<T>(p, difop, d, m, nbw, np);
//     else val=leftOperand_p->leftEval<T>(difop,d,m,nbw);
//  if(rightOperand_p->isFunction()) val=rightOperand_p->rightEval<T>(p, val, d, m, nbw, np);
//   else val=rightOperand_p->rightEval<T>(val, d, m, nbw);
// }

//evaluate operator from a point p and shape values sv and its derivatives dsv when extension involved
// ExtensionData embends some useful data related to the extension (shapevalues, nodes on current element)
//(dimFun is the dimension of shape functions)
template <typename T,typename K>
void OperatorOnUnknown::eval(const Point& p, const std::vector<K>& sv,
                             const std::vector< std::vector<K> >& dsv,
                             const std::vector< std::vector<K> >& d2sv,
                             dimen_t dimFun, Vector<T>& val, dimen_t& d, dimen_t& m,
                             const Vector<real_t>* np, const ExtensionData* extdata) const
{
// compute differential operator
  number_t nbw=sv.size()/dimFun;
  d=dimFun;
  Vector<K> difop;
  difOp_p->eval(sv, dsv, d2sv, d, m, difop, np, coefs_);

  //compute operator on unknown
  if(leftOperand_p == nullptr)
  {
      if(rightOperand_p == nullptr)  {val=difop; return;}
      val=rightOperand_p->rightEval<T>(p, difop, d, m, nbw, np, extdata);
      return;
  }
  else //leftoperand !=nullptr
  {
      if(leftOperand_p->isFunction()) val=leftOperand_p->leftEval<T>(p, difop, d, m, nbw, np, extdata);
      else val=leftOperand_p->leftEval<T>(difop,d,m,nbw);
      if(rightOperand_p == nullptr)  return;
      if(rightOperand_p->isFunction()) val=rightOperand_p->rightEval<T>(p, val, d, m, nbw, np, extdata);
      else val=rightOperand_p->rightEval<T>(val, d, m, nbw);
  }
}

//evaluate operator from points p,q and shape values sv and its derivatives dsv
//(dimFun is the dimension of shape functions)
//case of a Kernel
template <typename T, typename K>
void OperatorOnUnknown::eval(const Point& p,const Point& q, const std::vector<K>& sv,
                             const std::vector< std::vector<K> >& dsv,
                             const std::vector< std::vector<K> >& d2sv,
                             dimen_t dimFun, Vector<T>& val, dimen_t& d, dimen_t& m,
                             const Vector<real_t>* nx, const Vector<real_t>* ny) const
{
// compute differential operator
  number_t nbw=sv.size()/dimFun;
  d=dimFun;
  Vector<K> difop;
  difOp_p->eval(sv, dsv, d2sv, d, m, difop, ny, coefs_);

  //compute operator on unknown
  if(leftOperand_p == nullptr && rightOperand_p == nullptr) {val=difop; return;}
  if(leftOperand_p != nullptr && rightOperand_p == nullptr) {val=leftOperand_p->leftEval<T>(p,q,difop,d,m,nbw,nx,ny);return;}
  if(leftOperand_p == nullptr && rightOperand_p != nullptr) {val=rightOperand_p->rightEval<T>(p,q,difop,d,m,nbw,nx,ny);return;}
  if(leftOperand_p->isFunction()) val=leftOperand_p->leftEval<T>(p,q,difop,d,m,nbw,nx,ny);
     else val=leftOperand_p->leftEval<T>(difop,d,m,nbw);
  if(rightOperand_p->isFunction()) val=rightOperand_p->rightEval<T>(p,q,val,d,m,nbw,nx,ny);
   else val=rightOperand_p->rightEval<T>(val,d,m,nbw);
}

// print utility
std::ostream& operator<<(std::ostream&, const OperatorOnUnknown&); //!< outputs OperatorOnUnknown attributes

} // end of namespace xlifepp

#endif /* OPERATOR_ON_UNKNOWN_HPP */

