/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DifferentialOperator.hpp
  \authors D. Martin, E. Lunéville
  \since 29 fev 2012
  \date 9 may 2012

  \brief Definition of the xlifepp::DifferentialOperator class

  Class xlifepp::DifferentialOperator describes the differential operators (also order 0)
  which can be applied to an xlifepp::Unknown u(t,x,y,z) (scalar or vector) :

  Standard differential operators (see enum xlifepp::DiffOpType in config.hpp) :

  names          | syntaxes                | operator               | unknown type
  ---------------|-------------------------|------------------------|------------------
  id             | id(u) or u              | identity               | scalar or vector
  ...            | id(u,i) or u~i (or u~x) | u_i i-th component     | vector
  dt or d0       | dt(u)                   | du/dt                  | scalar or vector
  ...            | dt(u,i) or dt(u[i])     | du_i/dt i-th component | vector
  dx or d1       | dx(u)                   | du/dx                  | scalar or vector
  ...            | dx(u,i) or dx(u[i])     | du_i/dx i-th component | vector
  dy or d2       | dy(u)                   | du/dy                  | scalar or vector
  ...            | dy(u,i) or dy(u[i])     | du_i/dy i-th component | vector
  dz or d3       | dz(u)                   | du/dz                  | scalar or vector
  ...            | dz(u,i) or dz(u[i])     | du_i/dz i-th component | vector
  grad or nabla  | grad(u)                 | grad u                 | scalar or vector
  div            | div(u)                  | div u                  | vector
  curl or rot    | curl(u)                 | curl u                 | vector
  gradS          | gradS(u)                | surface gradient       | scalar
  divS           | divS(u)                 | surface divergence     | vector
  curlS or rotS  | curlS(u)                | surface curl           | scalar

  Operators with normal:

  names        | syntaxes                     | operator                                    | unknown type
  -------------|------------------------------|---------------------------------------------|------------------
  ndot         | ndot(u) or u\|n              | inner product of vector u and normal vector | vector
  ncross       | ncross(u) or n^u             | cross product of normal vector and vector u | vector
  ncrossncross | ncross(ncross(u)) or n^(n^u) | double cross product n^(n^u) for vector u   | vector
  ndotgrad     | ngrad(u) or n\|grad(u)       | inner product of normal vector and grad u   | scalar or vector
  ncrossgrad   | ncrossgrad(u) or n^grad(u)   | cross product of normal vector and grad u   | scalar
  ndiv         | ndiv(u) or n*div(u)          | product of div u and normal vector          | vector
  ncrosscurl   | ncrosscurl(u) or n^curl(u)   | cross product of normal vector and curl u   | vector
  ntimes       | ntimes(u) or n*u             | product of scalar u and normal vector       | scalar or matrix
  timesn       | timesn(u) or u*n             | product of scalar u and normal vector       | scalar or matrix
  ncrossntimes | ncrossntimes(u) or (n^n)*u   | double cross product (n^n)*u for vector u   | scalar or matrix
  timesncrossn | timesncrossn(u) or  u*(n^n)  | double cross product u*(n^n) for vector u   | scalar or matrix
  ncrossndot   | ncrossndot(u) or n^(n\|u)    | double cross product (n^n)\|u for vector u  | vector
  ntimesndot   | ntimesndot(u) or n*(n\|u)    | mix normal product n*(n\|u) for vector u    | vector


  Composite operators:

  names           | syntaxes                                              | operator                  | unknown type
  ----------------|-------------------------------------------------------|---------------------------|-------------------
  divG            | divG(u,a,b,c) or a*dx(u~x)+b*dy(u~y)+c*dz(u~z)        | generalized divergence    | vector
  gradG or nablaG | gradG(u,a,b,c) or diffVector(a*dx(u),b*dy(u),c*dz(u)) | generalized gradient      | scalar or vector
  curlG or rotG   | curlG(u,a,b,c) or rotG(u,a,b,c) or diffVector(...)    | generalized curl          | vector
  epsilon         | epsilon(u)                                            | elasticity tensor         | vector
  epsilonG        | epsilonG(u,id,a,b,c) (see documentation)              | general elasticity tensor | vector
  epsilonR        | epsilonR(u) = (e11,e22,e33,e32,e31,e21)               | reduced elasticity tensor | vector
  voigtToM        | voigtToM(e)= [e1 e6 e5; e6 e2 e4; e5 e4 e3]           | Voigt vector to tensor    | vector

  Other operators:

  names        | syntaxes              | operator                     | unknown type
  -------------|-----------------------|------------------------------|-------------------
  jump         | jump(u)               | jump across a boundary       | scalar or vector
  mean         | mean(u)               | mean value across a boundary | scalar or vector

  Operators for kernel k(x,y) or operator on kernel:

  names           | syntaxes                                              | operator                  | kernel type
  ----------------|-------------------------------------------------------|---------------------------|-------------------
  grad_x, nabla_x | grad_x(k)                                             | partial x grad            | scalar or vector
  grad_y, nabla_y | grad_y(k)                                             | partial y grad            | scalar or vector
  div_x           | div_x(k)                                              | partial x div             | vector
  div_y           | div_y(k)                                              | partial y div             | vector
  curl_x, rot_x   | curl_x(k)                                             | partial x curl            | scalar or vector
  curl_y, rot_y   | curl_y(k)                                             | partial y curl            | scalar or vector
  ntimes_x        | ntimes_x(k)                                           | nx*k                      | scalar
  ndot_x          | ndot_x(k)                                             | nx\|k                     | vector
  ncross_x        | ncross_x(k) or nx ^k                                  | nx^k                      | vector
  ncrossncross_x  | ncrossncross_x(k) or (nx^nx)^k                        | nx^nx^k                   | vector
  ndotgrad_x      | ndotgrad_x(k) or nx \| grad_x(k)                      | nx.k                      | vector
  ndiv_x          | ndiv_x(k) or nx * div_x(k)                            | nx*div_x(k)               | vector
  ncrosscurl_x    | ncrosscurl_x(k) or nx ^ curl_x(k)                     | nx^curl_x(k)              | vector
  ntimes_y        | ntimes_y(k)                                           | ny*k                      | scalar
  ndot_y          | ndot_y(k)                                             | ny\|k                     | vector
  ncross_y        | ncross_y(k) or ny ^k                                  | ny^k                      | vector
  ncrossncross_y  | ncrossncross_y(k) or (ny^ny)^k                        | ny^ny^k                   | vector
  ndotgrad_y      | ndotgrad_y(k) or ny \| grad_y(k)                      | ny.k                      | vector
  ndiv_y          | ndiv_y(k) or ny * div_y(k)                            | ny*div_y(k)               | vector
  ncrosscurl_y    | ncrosscurl_y(k) or ny ^ curl_y(k)                     | ny^curl_y(k)              | vector
  nxdotny_times   | nxdotny_times(k) or (nx\|ny)*k or  k*(nx\|ny)         | (nx\|ny)*k                | scalar or vector
  nxcrossny_dot   | nxcrossny_dot(k) or (nx^ny)\|k                        | (nx^ny)\|k                | vector
  nycrossnx_dot   | nycrossnx_dot(k) or (ny^nx)\|k                        | (ny^nx)\|k                | vector
  nxcrossny_cross | nxcrossny_cross(k) or (nx^ny)^k                       | (nx^ny)^k                 | vector
  nycrossnx_cross | nycrossnx_cross(k) or (ny^nx)^k                       | (ny^nx)^k                 | vector

  second order derivative operators:

  names           | syntaxes                                              | operator                  | unknown type
  ----------------|-------------------------------------------------------|---------------------------|-------------------
  dxx,dyy,dxy, ...| dxx(u), dyy(u), dxy(u), dzz(u), dxz(u), dyz(u)        | basic second derivative   | scalar or vector
  d11,d22,d12,... | d11(u), d22(u), d12(u), ... same as dxx,dyy,dxy, ...  | basic second derivative   | scalar or vector
  lap             | lap(u) laplaciaen in dim 1,2,3                        | laplacian                 | scalar or vector
  lapG            | lapG(u,a11,a22,[a33])                                 | generalized laplacian     | scalar or vector
  d2G             | d2G(u,a11,a12,a22,[a13],[a23],[a33]) or d2G(u,Matrix) | sum_ij aij.dij            | scalar or vector

  \note the inner product is the hermitian product when one of arguments is a complex value
*/


#ifndef DIFFERENTIAL_OPERATOR_HPP
#define DIFFERENTIAL_OPERATOR_HPP

#include "config.h"
#include "utils.h"

namespace xlifepp
{

/*!
  \class DifferentialOperator
  description of a differential operator
*/
class DifferentialOperator
{
  private:
    DiffOpType type_=_id;          //!< type of differential operator (see enum DiffOpType)
    number_t order_=0;             //!< derivation order
    bool requiresExtension_=false; //!< does operator involve non-tangential derivatives
    bool requiresNormal_=false;    //!< is normal vector required for this
    bool requiresTangent_=false;   //!< is tangent vector required for this
    string_t name_="id";           //!< operator name

    DifferentialOperator(const DifferentialOperator&);  //!< forbidden copy
    void operator=(const DifferentialOperator&);        //!< forbidden assignment
    void build();                    //!< defines characteristics from type

  public:
    static std::vector<DifferentialOperator*> theDifferentialOperators; //!< unique list of run time diff. operators
    static void clearGlobalVector();                                    //!< delete all DifferentialOperator objects

    // Constructors, Destructor
    // Warning* Constructor should not be invoked directly, use DifferentialOperatorFind
    DifferentialOperator();              //!< default constructor
    DifferentialOperator(DiffOpType ty); //!< constructor with type
    ~DifferentialOperator();             //!< destructor

    // accessors
    string_t name() const {return name_;}      //!< return operator name
    DiffOpType type() const {return type_;}    //!< return operator type
    number_t order() const {return order_;}    //!< return operator order
    bool normalRequired() const {return requiresNormal_;}        //!< returns if operator requires normal vector
    bool tangentRequired() const {return requiresTangent_;}      //!< returns if operator requires tangent vector
    bool extensionRequired() const { return requiresExtension_;} //!< returns if operator involves non-tangential derivatives

    template <typename K>
    void eval(const std::vector<K>&,
              const std::vector< std::vector<K> >&,
              const std::vector< std::vector<K> >&,
              dimen_t&, dimen_t&, Vector<K>&,
              const Vector<real_t>* =nullptr,
              const std::vector<complex_t>& = std::vector<complex_t>()) const;  //!< compute differential operator from shape values
}; // end of class DifferentialOperator

//-------------------------------------------------------------------------------
// External related functions
//-------------------------------------------------------------------------------
DifferentialOperator* findDifferentialOperator(DiffOpType);            //!< contructor-like, returns newly created or existing object
std::ostream& operator<<(std::ostream&, const DifferentialOperator&);  //!< print utility
void printListDiffOp(std::ostream&);                                   //!< print the list of differential operators (check utility)
void printListDiffOp(CoutStream&);                                   //!< print the list of differential operators (check utility)


inline bool operator==(const DifferentialOperator& d1, const DifferentialOperator& d2)
{
   return d1.type()== d2.type();
}

inline bool operator!=(const DifferentialOperator& d1, const DifferentialOperator& d2)
{
   return !(d1==d2);
}

//special operations on unitary vectors, return a symbolic unitary vector
//! inner product of unitary vector (_nx|_ny) -> _nxdotny
inline UnitaryVector operator|(const UnitaryVector& n1,const UnitaryVector& n2)
{
    if((n1==_nx && n2==_ny) || (n1==_ny && n2==_nx) ) return _nxdotny;
    error("not_handled","UnitaryVector|UnitaryVector" );
    return n1;  //fake return
}

//! cross product of unitary vector (_nx^_ny) -> _nxcrossny
inline UnitaryVector operator^(const UnitaryVector& n1,const UnitaryVector& n2)
{
    if(n1==_nx && n2==_ny ) return _nxcrossny;
    if(n1==_ny && n2==_nx ) return _nycrossnx;
    if(n1==_n && n2==_n ) return _ncrossn;
    error("not_handled","UnitaryVector^UnitaryVector" );
    return n1;  //fake return
}

/*!-----------------------------------------------------------------------------------------
 evaluate differential operator from values and derivatives of basis functions
 d: dimension of shape function (d>1 means vector shape functions)
 w: shape functions values ordered as follows (n number of shape functions)
      (w_1)_1,...,(w_1)_d,(w_2)_1,...,(w_2)_d, ..., (w_n)_1,...,(w_n)_d
 dw: shape functions derivatives (1 to m) ordered as follows
      dw[0] = (d1.w_1)_1,...,(d1.w_1)_d,(d1.w_2)_1,...,(d1.w_2)_d, ..., (d1.w_n)_1,...,(d1.w_n)_d
      ...
      dw[m] = (dm.w_1)_1,...,(dm.w_1)_d,(dm.w_2)_1,...,(dm.w_2)_d, ..., (dm.w_n)_1,...,(dm.w_n)_d
 d2w: shape functions second derivatives (1 to m) ordered as follows
      dw[0] = (d11.w_1)_1,...,(d11.w_1)_d,(d11.w_2)_1,...,(d11.w_2)_d, ..., (d11.w_n)_1,...,(d11.w_n)_d
      dw[1] = (d22.w_1)_1,...,(d22.w_1)_d,(d22.w_2)_1,...,(d22.w_2)_d, ..., (d22.w_n)_1,...,(d22.w_n)_d
      dw[2] = (d12.w_1)_1,...,(d12.w_1)_d,(d12.w_2)_1,...,(d12.w_2)_d, ..., (d12.w_n)_1,...,(d12.w_n)_d
      dw[3] = (d33.w_1)_1,...,(d33.w_1)_d,(d33.w_2)_1,...,(d33.w_2)_d, ..., (d33.w_n)_1,...,(d33.w_n)_d  3D only
      dw[4] = (d13.w_1)_1,...,(d13.w_1)_d,(d13.w_2)_1,...,(d13.w_2)_d, ..., (d13.w_n)_1,...,(d13.w_n)_d
      dw[5] = (d23.w_1)_1,...,(d23.w_1)_d,(d23.w_2)_1,...,(d23.w_2)_d, ..., (d23.w_n)_1,...,(d23.w_n)_d

 note that m may be different from d
 np: optionnal pointer to normal vector (default 0 : no normal vector)
 coefs: optional coefficients used in generalized operator (currently works only for real coefficients)

return always a real vector, ordered in the following
  loop on shape functions
     loop on components of shape function (when d >1)
        loop on derivatives of shape function (if required)

output:
  res: r_1,..., r_n where r_k are scalars or vectors or matrices
  d: size of blocks r_k
  m: number of matrix columns when r_k are matrices
-----------------------------------------------------------------------------------------*/
template<typename K>
void DifferentialOperator::eval(const std::vector<K>& w, const std::vector< std::vector<K> >& dw,
                                const std::vector< std::vector<K> >& d2w,
                                dimen_t& d, dimen_t& m, Vector<K>& res,
                                const Vector<real_t>* np, const std::vector<complex_t>& coefs) const
{
  m=1;
  switch(type_)
  {
    case _d0:
    case _id: res=w; return;

    case _d1: res=dw[0]; return;
    case _d2: res=dw[1]; return;
    case _d3: res=dw[2]; return;

    case _grad:  //wi is either a scalar or a vector function
    case _grad_x:
    case _grad_y:
    case _gradS: //on manifold, dimension will be changed when mapping to physical space
    {
      number_t nbw=w.size(), dim=dw.size();
      res.resize(nbw*dim);
      typename std::vector<K>::iterator itr=res.begin();
      for(number_t j=0; j<nbw; j++)
        for(number_t i=0; i<dim; i++,itr++) *itr=dw[i][j];
      m=d; d*=dim;
      return;
    } //end case grad, gradS
    case _div:  // wi has to be vectors of dimension d<=dim
    case _div_x:
    case _div_y:
    case _divS:
    {
      number_t nbw=w.size()/d;
      res.resize(nbw);
      typename std::vector<K>::iterator itr=res.begin();
      for(number_t i=0; i<nbw; i++, itr++)
      {
        *itr=0.;
        for(dimen_t j=0; j<d; j++)  *itr+=dw[j][i*d+j];
      }
      m=d=1;
      return;
    } //end case div
    case _curl: // in 2D return the scalar curl, in 1D return dx.wi
    case _curl_x:
    case _curl_y:
    case _curlS:
    // wi has to be vectors of dimension d<=dim,
    {
      number_t nbw=w.size();
      dimen_t dim=dw.size();
      dimen_t curldim=std::min(dim,d);
      if(curldim==1) //1D curl (=dx)
      {
        if(d==1) { res=dw[0]; return; }
        res.resize(nbw/d);
        typename std::vector<K>::iterator itr=res.begin();
        typename std::vector<K>::const_iterator itdw=dw[0].begin();
        for(number_t i=0; i<nbw; i+=d, itr++, itdw+=d) *itr=*itdw;
        m=d=1;
        return;
      }
      if (curldim==2) //2D curl
      {
        res.resize(nbw/d);
        typename std::vector<K>::iterator itr=res.begin();
        for(number_t i=0; i<nbw; i+=d, itr++) *itr=dw[0][i+1]-dw[1][i];
        m=d=1;
        return;
      }

      res.resize(nbw); //3D curl
      typename std::vector<K>::iterator itr=res.begin();
      for(number_t i=0; i<nbw; i+=d)
      {
        *itr++=dw[1][i+2]-dw[2][i+1];
        *itr++=dw[2][i]-dw[0][i+2];
        *itr++=dw[0][i+1]-dw[1][i];
      }
      return;
    }//end case curl
    case _epsilon:
    {
      // number_t dim=dw.size();
      number_t kd;
      number_t nbw=w.size();
      number_t nbN=nbw/d;         //number of vector shape functions
      res.resize(nbw*d);
      typename std::vector<K>::iterator itr=res.begin();
      for(number_t k=0; k<nbN; k++)
      {
        kd=k*d;
        for(number_t i=0; i<d; i++)
          for(number_t j=0; j<d; j++,itr++)
          {
            *itr = dw[i][kd+j] + dw[j][kd+i];
            *itr *= 0.5;
          }
      }
      m=d; d=m*m;
      return;
    } //end case epsilon
    case _epsilonG:
    // _epsilonG is the generalized epsilon with a1,a2,a3, id=a0 parameters:
    //       | 2d1u1  d1u2  d1u3 |        |   0    d2u1    0  |        |   0    0   d3u1 |
    //  a1/2 |  d1u2    0     0  | + a2/2 | d2u1  2d2u2  d2u3 | + a3/2 |   0    0   d3u2 |
    //       |  d1u3    0     0  |        |   0    d2u3     0 |        | d3u1 d3u2 2d3u3 |
    //  with d1=d2=d3=Id when id=0
    //  in 2d omit third components and block c
    //  note that epsilonG(1,1,1,1) corresponds to the standard epsilon operator in 3D
    {
      number_t dim=dw.size(),kd;  //space dimension (point dimension)
      number_t nbw=w.size();
      number_t nbN=nbw/d;         //number of vector shape functions
      number_t d2=d*d;
      res.resize(nbN*d2);
      res*=0;
      typename std::vector<K>::iterator itr=res.begin();
      number_t id=number_t(coefs[0].real());
      for(number_t k=0; k<nbN; k++, itr+=d2)  //loop on blocks
      {
        kd=k*d;
        for(number_t i=0; i<d; i++)        //loop on a1,a2,a3 blocks
        {
          K ai=complexToT<K>(coefs[i+1]*.5), zero=K(0);
          if(ai!=zero)
          {
            for(number_t j=0; j<d; j++)    //loop on d components
            {
              K e=zero;
              if(id==0) e=ai*w[kd+j];
              else if(i<dim) e=ai*dw[i][kd+j];   //test i<dim to deal with d-vector shape functions and point of dim<d
              if(e!=zero)
              {
                *(itr +(i*d+j)) += e;
                *(itr +(j*d+i)) += e;
              }
            }
          }
        }
      }
      m=d; d=m*m;
      return;
    } //end case epsilonG
    case _epsilonR:
    // _epsilonR is the reduced epsilon, say the vector (e11, e22, e33, e32, e31, e21) (Voigt notation)
    {
      number_t nbw=w.size();
      number_t nbN=nbw/d;         //number of vector shape functions
      number_t p=(d*(d+1))/2;
      res.resize(nbN*p);
      typename std::vector<K>::iterator itr=res.begin();
      for(number_t k=0; k<nbN; k++)
      {
        number_t kd=k*d;
        for(number_t i=0; i<d; i++, itr++) *itr = dw[i][kd+i];
        for(number_t i=d-1; i>0; i--)
          for(number_t j=i-1; j!=0; j--, itr++) *itr = 0.5*(dw[i][kd+j] + dw[j][kd+i]);
      }
      m=1; d=p;
      return;
    }//end case epsilonR
    case _voigtToM:
    // convert Voigt vector representation (e1,e2,...e6) to matrix representation [e1 e6 e5; e6 e2 e4; e5 e4 e3]  (3D)
    // convert Voigt vector representation (e1,e2,e3) to matrix representation [e1 e3; e3 e2] (2D)
    {
      number_t nbw=w.size();
      number_t nbN=nbw/d;         //number of vector shape functions
      if(d==1) {res=w; return;}
      if(d==3) //2d case
      {
        res.resize(nbN*4);
        typename std::vector<K>::iterator itr=res.begin();
        for(number_t k=0; k<nbN; k++, itr+=4)
        {
          number_t kd=k*d;
          *itr = w[kd]; *(itr+1)= w[kd+2]; *(itr+2)= w[kd+2]; *(itr+3)= w[kd+1];
        }
        m=2; d=4;
        return;
      }
      if (d==6) //3d case
      {
        res.resize(nbN*9);
        typename std::vector<K>::iterator itr=res.begin();
        for(number_t k=0; k<nbN; k++, itr+=9)
        {
          number_t kd=k*d;
          *itr = w[kd]; *(itr+1)= w[kd+5]; *(itr+2)= w[kd+4];
          *(itr+3)= w[kd+5]; *(itr+4)= w[kd+1]; *(itr+5)= w[kd+3];
          *(itr+6)= w[kd+4]; *(itr+7)= w[kd+3]; *(itr+8)= w[kd+2];
        }
        m=3; d=9;
        return;
      }
      where("DifferentialOperator::eval(...)");
      error("operator_bad_size",d, words("diffop",_voigtToM));
    }//end case voigtToM
    case _ntimes:  //only for scalar shape functions NOT CHECKED
    case _ntimes_x:
    case _ntimes_y:
    case _timesn:
    case _timesn_x:
    case _timesn_y:
    {
      number_t nbw=w.size();
      if(d>1)
      {
        where("DifferentialOperator::eval(...)");
        error("scalar_only");
      }
      if(np==nullptr)
      {
        where("DifferentialOperator::eval(...)");
        error("null_pointer","normal in timesn");
      }
      d=dimen_t(np->size());
      res.resize(nbw*d);
      typename std::vector<K>::iterator itr=res.begin();
      typename std::vector<K>::const_iterator itw=w.begin();
      std::vector<real_t>::const_iterator itn;
      for(; itw!=w.end(); itw++)
      {
         itn=np->begin();
         for(dimen_t i=0; i<d; i++, itr++, itn++) *itr= *itn **itw;
      }
      return;
    }//end case ntimes
    case _ndot: //only for vector shape functions NOT CHECKED
    case _ndot_x:
    case _ndot_y:
    {
      if(np==nullptr)
      {
        where("DifferentialOperator::eval(...)");
        error("null_pointer","normal in ndot");
      }
      number_t nbw=w.size(), nbN=nbw/d;         //number of vector shape functions
      res.resize(nbN);
      typename std::vector<K>::iterator itr=res.begin();
      typename std::vector<K>::const_iterator itw=w.begin();
      std::vector<real_t>::const_iterator itn;
      for(; itr!=res.end(); itr++)
      {
        *itr=0.;
        itn=np->begin();
        for(dimen_t i=0; i<d; i++, itw++, itn++) *itr+= *itn **itw;
      }
      m=1; d=1;
      return;
    }//end case ndot
    case _ncross: //only for vector shape functions NOT CHECKED
    case _ncross_x:
    case _ncross_y:
    {
      if(np==nullptr)
      {
        where("DifferentialOperator::eval(...)");
        error("null_pointer","normal in ncross");
      }
      number_t nbw=w.size(), nbN=nbw/d;         //number of vector shape functions
      if(d==1)
      {
        res= (*np->begin()) * w;
        return;
      }
      if(d==2) //scalar result
      {
        res.resize(nbN);
        typename std::vector<K>::iterator itr=res.begin();
        typename std::vector<K>::const_iterator itw=w.begin();
        std::vector<real_t>::const_iterator itn=np->begin();
        real_t nx=*itn++, ny=*itn;
        for(; itr!=res.end(); itr++, itw+=2) *itr=  nx **(itw+1)-ny **itw;
        m=1; d=1;
        return;
      }
      //3D case
      res.resize(nbN*3);
      typename std::vector<K>::iterator itr=res.begin();
      typename std::vector<K>::const_iterator itw=w.begin();
      std::vector<real_t>::const_iterator itn=np->begin();
      real_t nx=*itn++, ny=*itn++, nz=*itn;
      for(; itr!=res.end(); itr+=3, itw+=3)
      {
        *itr    =  ny **(itw+2)-nz **(itw+1);
        *(itr+1)=  nz **itw -nx **(itw+2);
        *(itr+2)=  nx **(itw+1) - ny**itw;
      }
      m=1; d=3;
      return;
    }//end case ncross
    case _ncrossncross: // n x (n x u), only in vector case NOT CHECKED
    case _ncrossncross_x:
    case _ncrossncross_y:
    {
      if(np==nullptr)
      {
        where("DifferentialOperator::eval(...)");
        error("null_pointer","normal in ncrossncross");
      }
      number_t nbw=w.size(), nbN=nbw/d;         //number of vector shape functions
      if(d==1)  //same as n * n * u
      {
        real_t nx=*np->begin();
        res= nx * nx * w;
        return;
      }
      if(d==2) //vector result
      {
        res.resize(nbN*2);
        typename std::vector<K>::iterator itr=res.begin();
        typename std::vector<K>::const_iterator itw=w.begin();
        std::vector<real_t>::const_iterator itn=np->begin();
        real_t nx=*itn++, ny=*itn;
        for(; itr!=res.end(); itr+=2, itw+=2)
        {
          K t = nx * *(itw+1) - ny * *itw;
          *itr=  ny * t;
          *(itr+1)=  -nx * t;
        }
        m=1; d=2;
        return;
      }
      //3D case
      res.resize(nbN*3);
      typename std::vector<K>::iterator itr=res.begin();
      typename std::vector<K>::const_iterator itw=w.begin();
      std::vector<real_t>::const_iterator itn=np->begin();
      real_t nx=*itn++, ny=*itn++, nz=*itn;
      K t1, t2, t3;
      for(; itr!=res.end(); itr+=3, itw+=3)
      {
        t1 =  ny * *(itw+2)-nz **(itw+1);
        t2 =  nz * *itw -nx **(itw+2);
        t3 =  nx * *(itw+1) - ny**itw;
        *itr    =  ny*t3 - nz*t2;
        *(itr+1)=  nz*t1 - nx*t3;
        *(itr+2)=  nx*t2 - ny*t1;
      }
      m=1; d=3;
      return;
    }//end case ncrossncross
    case _ntimesndot: // n * (n | w), only in vector case NOT CHECKED
    {
      if(np==nullptr)
      {
        where("DifferentialOperator::eval(...)");
        error("null_pointer","normal in ntimesndot");
      }
      number_t nbw=w.size(), nbN=nbw/d;         //number of vector shape functions
      if(d==1)  //same as n * n * w
      {
        real_t nx=*np->begin();
        res= nx * nx * w;
        return;
      }

      if(d==2) //vector result
      {
        res.resize(nbN*2);
        typename std::vector<K>::iterator itr=res.begin();
        typename std::vector<K>::const_iterator itw=w.begin();
        std::vector<real_t>::const_iterator itn=np->begin();
        real_t nx=*itn++, ny=*itn;
        for(; itr!=res.end(); itr+=2, itw+=2)
        {
          K t = nx * *itw + ny * *(itw+1);  //n|w
          *itr=  nx * t;
          *(itr+1)  =  ny * t;
        }
        m=1; d=2;
        return;
      }
      //3D case
      res.resize(nbN*3);
      typename std::vector<K>::iterator itr=res.begin();
      typename std::vector<K>::const_iterator itw=w.begin();
      std::vector<real_t>::const_iterator itn=np->begin();
      real_t nx=*itn++, ny=*itn++, nz=*itn;
      for(; itr!=res.end(); itr+=3, itw+=3)
      {
        K t =  nx * *itw + ny * *(itw+1) + nz * *(itw+1);
        *itr    =  nx * t;
        *(itr+1)=  ny * t;
        *(itr+2)=  nz * t;
      }
      m=1; d=3;
      return;
    }//end case ntimesndot
    case _ndotgrad: // n.grad(u) (scalar for vector shape functions) NOT CHECKED
    case _ndotgrad_x:
    case _ndotgrad_y:
    {
      if(np==nullptr)
      {
        where("DifferentialOperator::eval(...)");
        error("null_pointer","normal in ndotgrad");
      }
      if(m>1)
      {
        where("DifferentialOperator::eval(...)");
        error("operator_vector_only",words("diffop",_ndotgrad));
      }
      number_t nbw=w.size(), dim=dw.size();
      res.resize(nbw);
      typename std::vector<K>::iterator itr=res.begin();
      std::vector<real_t>::const_iterator itn;
      for(number_t j=0; j<nbw; j++, itr++)
      {
        *itr=0.;
        itn=np->begin();
        for(number_t i=0; i<dim; i++, itn++) *itr += *itn * dw[i][j];
      }
      d=1;
      return;
    }//end case ndotgrad
    case _ndiv: // n* div(u), only for vector shape functions, currently no matrix NOT CHECKED
    case _ndiv_x:
    case _ndiv_y:
    {
      if(np==nullptr)
      {
        where("DifferentialOperator::eval(...)");
        error("null_pointer","normal in ndiv");
      }
      number_t nbw=w.size(), nbN=nbw/d;         //number of vector shape functions
      res.resize(nbw);
      typename std::vector<K>::iterator itr=res.begin();
      std::vector<real_t>::const_iterator itn;
      for(number_t i=0; i<nbN; i++)
      {
        K t=0.;
        for(dimen_t j=0; j<d; j++)  t+=dw[j][i*d+j];  //div
        itn=np->begin();
        for(dimen_t j=0; j<d; j++, itr++)  *itr = *itn * t;  //n * div
      }
      return;
    }//end case ndiv
    case _ncrosscurl: //n x curl (u), wi has to be vectors of dimension d<=dim, NOT CHECKED
    case _ncrosscurl_x:
    case _ncrosscurl_y:
    {
      if(np==nullptr)
      {
        where("DifferentialOperator::eval(...)");
        error("null_pointer","normal in ncrosscurl");
      }
      if(m>1)
      {
        where("DifferentialOperator::eval(...)");
        error("operator_vector_only",words("diffop",_ncrosscurl));
      }
      number_t nbw=w.size(), dim=dw.size();  //assuming d=dim
      std::vector<real_t>::const_iterator itn=np->begin();
      if(dim==1) //1D curl (=dx)
      {
        res= *itn * dw[0];
        d=1;
        return;
      }
      res.resize(nbw);
      typename std::vector<K>::iterator itr=res.begin();
      if (dim==2) //2D curl as a scalar, result is a 2D vector [ny*curl(u), -nx*curl(u)]
      {
        real_t nx = *itn, ny = * ++itn;
        for(number_t i=0; i<nbw; i+=d)
        {
          K curl=dw[0][i*d+1]-dw[1][i*d];
          * itr =  ny*curl; itr++;
          * itr = -nx*curl; itr++;
        }
        d=2;
        return;
      }
      //3D curl
      K cx, cy, cz;
      real_t nx = *itn, ny = * ++itn, nz = * ++itn;
      for(number_t i=0; i<nbw; i+=d)
      {
        number_t id=i*d;
        cx=dw[1][id+2]-dw[2][id+1];
        cy=dw[2][id]-dw[0][id+2];
        cz=dw[0][id+1]-dw[1][id];
        * itr = ny*cz - nz*cy; itr++;
        * itr = nz*cx - nx*cz; itr++;
        * itr = nx*cy - ny*cx; itr++;
      }
      d=3;
      return;
    } //end case ncrosscurl
    case _ncrossgrad: //n x grad (u), wi has to be scalar, result is a vector in 3D and a scalar else - NOT CHECKED
    {
      if(np==nullptr)
      {
        where("DifferentialOperator::eval(...)");
        error("null_pointer","normal in ncrossgrad");
      }
      if(d>1)
      {
        where("DifferentialOperator::eval(...)");
        error("operator_scalar_only",words("diffop", _ncrossgrad));
      }
      number_t nbw=w.size(), dim=dw.size();
      std::vector<real_t>::const_iterator itn=np->begin();
      if(dim==1) //1D grad return nx*dx
      {
        res= *itn * dw[0];
        d=1;
        return;
      }

      if (dim==2) //2D n x grad, return the scalar nx*dy-ny*dx
      {
        res.resize(nbw);
        typename std::vector<K>::iterator itr=res.begin();
        real_t nx = *itn, ny = * ++itn;
        for(number_t i=0; i<nbw; i++, itr++) * itr = nx*dw[1][i]- ny*dw[0][i];
        d=1;
        return;
      }
      //3D n x grad, return the vector [ny*dz-nz*dy, nz*dx-nx*dz, nx*dy-ny*dx]
      K dx, dy, dz;
      real_t nx = *itn, ny = * ++itn, nz = * ++itn;
      res.resize(nbw*dim);
      typename std::vector<K>::iterator itr=res.begin();
      for(number_t i=0; i<nbw; i++)
      {
        dx=dw[0][i]; dy=dw[1][i]; dz=dw[2][i];
        * itr = ny*dz - nz*dy; itr++;
        * itr = nz*dx - nx*dz; itr++;
        * itr = nx*dy - ny*dx; itr++;
      }
      d=3;
      return;
    } //end case ncrossgrad
    case _dxx:  res=d2w[0]; return;
    case _dyy:  res=d2w[1]; return;
    case _dxy:  res=d2w[2]; return;
    case _dzz:  res=d2w[3]; return;
    case _dxz:  res=d2w[4]; return;
    case _dyz:  res=d2w[5]; return;
    case _lap:  //laplacian
    {
      number_t dim=dw.size();
      res.resize(w.size());
      typename std::vector<K>::iterator itr=res.begin();
      typename std::vector<K>::const_iterator itd=d2w[0].begin();
      for(;itr!=res.end(); ++itr, ++itd) *itr = *itd;
      if(dim>1)
      {
        itr=res.begin(); itd=d2w[1].begin();
        for(;itr!=res.end(); ++itr, ++itd) *itr += *itd;
      }
      if(dim>2)
      {
        itr=res.begin(); itd=d2w[3].begin();
        for(;itr!=res.end(); ++itr, ++itd) *itr += *itd;
      }
      return;
    }
    case _lapG:  // axx*dxx+ayy*dyy+axy*dxy
    {
      number_t dim=dw.size();
      K a=complexToT<K>(coefs[0]);
      res.resize(w.size());
      typename std::vector<K>::iterator itr=res.begin();
      typename std::vector<K>::const_iterator itd=d2w[0].begin();
      for(;itr!=res.end(); ++itr, ++itd) *itr=a* *itd;
      itr=res.begin(); itd=d2w[1].begin(); a=complexToT<K>(coefs[1]);
      if(dim>1) { for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd; }
      itr=res.begin(); itd=d2w[3].begin(); a=complexToT<K>(coefs[2]);
      if(dim>2) { for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd; }
      return;
    }
    case _d2G:  // sum aij dij
    {
     K a=complexToT<K>(coefs[0]);
     res.resize(w.size());
     typename std::vector<K>::iterator itr=res.begin();
     typename std::vector<K>::const_iterator itd=d2w[0].begin();
     for(;itr!=res.end(); ++itr, ++itd) *itr=a* *itd;   //a11*d11
     if(dw.size()==1) return;  //1D case
     if(coefs.size()==3) // symetric 2x2 matrix: a11 a12 a22
     {
       itr=res.begin(); itd=d2w[2].begin(); a=complexToT<K>(2*coefs[1]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // +a12*d12
       itr=res.begin(); itd=d2w[1].begin(); a=complexToT<K>(coefs[2]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // +a22*d22
       return;
     }
     if(coefs.size()==4) // 2x2 row matrix: a11 a12 a21 a22
     {
       itr=res.begin(); itd=d2w[2].begin(); a=complexToT<K>(coefs[1]+coefs[2]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // +(a12+a21)*d12
       itr=res.begin(); itd=d2w[1].begin(); a=complexToT<K>(coefs[3]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // +a22*d22
       return;
     }
     if(coefs.size()==6) // symetric 3x3 matrix: a11 a12 a22 a13 a23 a33
     {
       itr=res.begin(); itd=d2w[2].begin(); a=complexToT<K>(2*coefs[1]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // +a12*d12
       itr=res.begin(); itd=d2w[1].begin(); a=complexToT<K>(coefs[2]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // +a22*d22
       itr=res.begin(); itd=d2w[3].begin(); a=complexToT<K>(coefs[5]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // +a33*d33
       itr=res.begin(); itd=d2w[4].begin(); a=complexToT<K>(2*coefs[3]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // +a13*d13
       itr=res.begin(); itd=d2w[5].begin(); a=complexToT<K>(2*coefs[4]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // +a23*d23
       return;
     }
     if(coefs.size()==9) // symetric 3x3 matrix: a11 a12 a13 a21 a22 a23 a31 a32 a33
     {
       itr=res.begin(); itd=d2w[2].begin(); a=complexToT<K>(coefs[1]+coefs[3]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // (a12+a21)*d12
       itr=res.begin(); itd=d2w[1].begin(); a=complexToT<K>(coefs[4]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // aa22*d22
       itr=res.begin(); itd=d2w[3].begin(); a=complexToT<K>(coefs[8]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // aa33*d33
       itr=res.begin(); itd=d2w[4].begin(); a=complexToT<K>(coefs[2]+coefs[6]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // (a13+a31)*d13
       itr=res.begin(); itd=d2w[5].begin(); a=complexToT<K>(coefs[5]+coefs[7]);
       for(;itr!=res.end(); ++itr, ++itd) *itr+=a* *itd;   // (a23+a32)*d23
       return;
     }
     where("DifferentialOperator::eval(...)");
     error("free_error","in d2G operator, inconsistant coefficients size");
    }
//    case _divS:
//    case _curlS:
    case _gradG:
    {
      number_t nbw=w.size(), dim=dw.size();
      res.resize(nbw*dim);
      typename std::vector<K>::iterator itr=res.begin();
      for(number_t j=0; j<nbw; j++)
      {
        for(number_t i=0; i<dim; i++,itr++)
        {
          K ai=complexToT<K>(coefs[i]);
          *itr=ai*dw[i][j];
        }
      }
      m=d; d*=dim;
      return;
    } //end case grad, gradS
    case _divG:
    {
      number_t nbw=w.size()/d;
      res.resize(nbw);
      typename std::vector<K>::iterator itr=res.begin();
      for(number_t i=0; i<nbw; i++, itr++)
      {
        *itr=0.;
        for (dimen_t j=0; j<d; j++)
        {
          K aj=complexToT<K>(coefs[j]);
          *itr+=aj*dw[j][i*d+j];
        }
      }
      m=d=1;
      return;
    } //end case div
    case _curlG:
    // wi has to be vectors of dimension d<=dim,
    {
      number_t nbw=w.size();
      dimen_t dim=dw.size();
      dimen_t curldim=std::min(dim,d);
      K a0=complexToT<K>(coefs[0]),
        a1=complexToT<K>(coefs[1]),
        a2=complexToT<K>(coefs[2]);
      if(curldim==1) //1D curl (=dx)
      {
        if(d==1) { res=dw[0]; return; }
        res.resize(nbw/d);
        typename std::vector<K>::iterator itr=res.begin();
        typename std::vector<K>::const_iterator itdw=dw[0].begin();
        for(number_t i=0; i<nbw; i+=d, itr++, itdw+=d) *itr=a0**itdw;
        m=d=1;
        return;
      }
      if (curldim==2) //2D curl
      {
        res.resize(nbw/d);
        typename std::vector<K>::iterator itr=res.begin();
        for(number_t i=0; i<nbw; i+=d, itr++) *itr=a0*dw[0][i+1]-a1*dw[1][i];
        m=d=1;
        return;
      }

      res.resize(nbw); //3D curl
      typename std::vector<K>::iterator itr=res.begin();
      for(number_t i=0; i<nbw; i+=d)
      {
        *itr++=a1*dw[1][i+2]-a2*dw[2][i+1];
        *itr++=a2*dw[2][i]-a0*dw[0][i+2];
        *itr++=a0*dw[0][i+1]-a1*dw[1][i];
      }
      return;
    }//end case curl

    default:
      where("DifferentialOperator::eval(...)");
      error("operator_not_yet_handled",words("diffop",int(type_)));
  }
}

} // end of namespace xlifepp

#endif /* DIFFERENTIAL_OPERATOR_HPP */
