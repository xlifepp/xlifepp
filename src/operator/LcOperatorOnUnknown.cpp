/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LcOperatorOnUnknown.cpp
  \author E. Lunéville
  \since 14 jan 2014
  \date  16 jan 2014

  \brief Implementation of xlifepp::LcOperatorOnUnknown class members and related functions
*/

#include "LcOperatorOnUnknown.hpp"
#include "utils.h"

namespace xlifepp
{

//constructors/destructor
LcOperatorOnUnknown::LcOperatorOnUnknown(const OperatorOnUnknown& opu, const real_t& a)
{
  push_back(OpuValPair(new OperatorOnUnknown(opu),complex_t(a)));
}

LcOperatorOnUnknown::LcOperatorOnUnknown(const OperatorOnUnknown& opu, const complex_t& a)
{
  push_back(OpuValPair(new OperatorOnUnknown(opu),a));
}

LcOperatorOnUnknown::LcOperatorOnUnknown(const Unknown& u, const real_t& a)
{
  push_back(OpuValPair(new OperatorOnUnknown(u),complex_t(a)));
}

LcOperatorOnUnknown::LcOperatorOnUnknown(const Unknown& u, const complex_t& a)
{
  push_back(OpuValPair(new OperatorOnUnknown(u),a));
}


LcOperatorOnUnknown::LcOperatorOnUnknown(const LcOperatorOnUnknown& lc)  // copy constructor (full copy)
{
  if(lc.size()==0) return;
  copy(lc);
}

LcOperatorOnUnknown& LcOperatorOnUnknown::operator=(const LcOperatorOnUnknown& lc)         //! assignment
{
  if(&lc == this) return *this;
  clear();
  copy(lc);
  return *this;
}

LcOperatorOnUnknown::~LcOperatorOnUnknown()
{
  clear();
}

//deallocate OperatorOnUnknown pointers
void LcOperatorOnUnknown::clear()
{
  for(it_opuval it=begin(); it!=end(); it++)
  {
    if (it->first != nullptr) delete it->first;
  }
  std::vector<OpuValPair>::clear();
}

//full copy of OperatorOnUnknown pointers
void LcOperatorOnUnknown::copy(const LcOperatorOnUnknown& lc)
{
  //already cleared !!!
  resize(lc.size());
  cit_opuval itlc=lc.begin();
  it_opuval it= begin();
  for(; itlc!=lc.end(); itlc++, it++)
  {
    OperatorOnUnknown* op=new OperatorOnUnknown(*itlc->first);  //full copy
    *it=OpuValPair(op,itlc->second);
  }
}

//utilities
bool LcOperatorOnUnknown::isSingleUnknown() const
{
  if(size()<2) return true;
  cit_opuval it=begin();
  const Unknown* u=it->first->unknown();
  it++;
  for(; it!=end(); it++)
    if(it->first->unknown()!=u) return false;
  return true;
}
// return ith Unknown involved in LcOperator (i>=1)
const Unknown* LcOperatorOnUnknown::unknown(number_t i) const
{
  if(size()==0) return nullptr;
  if(i>size())
  {
    where("LcOperatorOnUnknown::unknown(Number)");
    error("index_out_of_range","i", 1, size());
  }
  return (begin()+i-1)->first->unknown();
}

//return list of Unknowns involved in LcOperator
std::set<const Unknown*> LcOperatorOnUnknown::unknowns() const
{
  std::set<const Unknown*> us;
  if(size()==0) return us;
  cit_opuval it=begin();
  for(; it!=end(); it++) us.insert(it->first->unknown());
  return us;
}

// return true if all operator are associated to a domain (non null pointer)
bool LcOperatorOnUnknown::withDomains() const
{
  cit_opuval it=begin();
  for(; it!=end(); it++)
    if(it->first->domain()==nullptr) return false;
  return true;
}

// return true if all terms involve the same domain and it is a non null pointer
bool LcOperatorOnUnknown::isSingleDomain() const
{
  GeomDomain* domp = nullptr;
  cit_opuval it=begin();
  for(; it!=end(); it++)
    {
        GeomDomain* dom = it->first->domain();
        if(domp==nullptr) domp = dom;
        else if(dom!=nullptr && dom!=domp) return false;
    }
  if(domp==nullptr) return false;
  return true;
}

// return ith domain (may be 0 if none)
GeomDomain* LcOperatorOnUnknown::domain(number_t i) const
{
  if(size()==0) return nullptr;
  if(i>size())
  {
    where("LcOperatorOnUnknown::domain(Number)");
    error("index_out_of_range","i", 1, size());
  }
  return (begin()+i-1)->first->domain();
}

// return set of GeomDomains's involved in condition (may be void)
std::set<GeomDomain*> LcOperatorOnUnknown::domainSet() const
{
  std::set<GeomDomain*> ds;
  cit_opuval it=begin();
  for(; it!=end(); it++)
    {
        GeomDomain* dom = it->first->domain();
        if(dom!=nullptr) ds.insert(dom);
    }
  return ds;
}

// return list of GeomDomains's attached to OperatorOnUnknown
std::vector<GeomDomain*> LcOperatorOnUnknown::domains() const
{
  std::vector<GeomDomain*> ds(size());
  cit_opuval it=begin();
  std::vector<GeomDomain*>::iterator itd=ds.begin();
  for(; it!=end(); ++it, ++itd)
    {
        *itd  = it->first->domain();
    }
  return ds;
}

//return coefficient of first term if a single term combination
complex_t LcOperatorOnUnknown::coefficient() const
{
  complex_t c=0.;
  if(size()==1) c=begin()->second;
  return c;
}

//return vector of coefficients involved in combination
std::vector<complex_t> LcOperatorOnUnknown::coefficients() const
{
  std::vector<complex_t> cs;
  if(size()==0) return cs;
  cs.resize(size());
  cit_opuval it=begin();
  std::vector<complex_t>::iterator itcs=cs.begin();
  for(; it!=end(); it++,itcs++) *itcs = it->second;
  return cs;
}

// return set of differential operator types involved in condition
std::set<DiffOpType> LcOperatorOnUnknown::diffOperators() const
{
  std::set<DiffOpType> dts;
  if(size()==0) return dts;
  cit_opuval it=begin();
  for(; it!=end(); it++) dts.insert(it->first->difOpType());
  return dts;
}

// insert terms in list
void LcOperatorOnUnknown::insert(const OperatorOnUnknown& opu)
{
  insert(1.,opu);
}

void LcOperatorOnUnknown::insert(const real_t& a, const OperatorOnUnknown& opu)
{
  push_back(OpuValPair(new OperatorOnUnknown(opu),complex_t(a)));
}

void LcOperatorOnUnknown::insert(const complex_t& a, const OperatorOnUnknown& opu)
{
  push_back(OpuValPair(new OperatorOnUnknown(opu),a));
}

void LcOperatorOnUnknown::insert(const Unknown& u)
{
  insert(OperatorOnUnknown(u,_id));
}

void LcOperatorOnUnknown::insert(const real_t& a, const Unknown& u)
{
  insert(a, OperatorOnUnknown(u,_id));
}

void LcOperatorOnUnknown::insert(const complex_t& a, const Unknown& u)
{
  insert(a, OperatorOnUnknown(u,_id));
}

LcOperatorOnUnknown& LcOperatorOnUnknown::operator +=(const OperatorOnUnknown& opu)
{
  insert(opu);
  return *this;
}

LcOperatorOnUnknown& LcOperatorOnUnknown::operator +=(const Unknown& u)
{
  insert(u);
  return *this;
}

LcOperatorOnUnknown& LcOperatorOnUnknown::operator +=(const LcOperatorOnUnknown& lc)
{
  if(this==&lc)
  {
    (*this)*=2;
    return *this;
  }
  cit_opuval itlc=lc.begin();
  for(; itlc!=lc.end(); itlc++) insert(itlc->second, *itlc->first);
  return *this;
}

LcOperatorOnUnknown& LcOperatorOnUnknown::operator -=(const OperatorOnUnknown& opu)
{
  insert(-1.,opu);
  return *this;
}

LcOperatorOnUnknown& LcOperatorOnUnknown::operator -=(const Unknown& u)
{
  insert(-1.,u);
  return *this;
}

LcOperatorOnUnknown& LcOperatorOnUnknown::operator -=(const LcOperatorOnUnknown& lc)
{
  if(this==&lc)
  {
    clear();
    return *this;
  }
  cit_opuval itlc=lc.begin();
  for(; itlc!=lc.end(); itlc++) insert(-itlc->second,*itlc->first);
  return *this;
}

LcOperatorOnUnknown& LcOperatorOnUnknown::operator *=(const real_t& a)
{
  it_opuval it=begin();
  for(; it!=end(); it++) it->second*=a;
  return *this;
}

LcOperatorOnUnknown& LcOperatorOnUnknown::operator *=(const complex_t& a)
{
  it_opuval it=begin();
  for(; it!=end(); it++) it->second*=a;
  return *this;
}

LcOperatorOnUnknown& LcOperatorOnUnknown::operator /=(const real_t& a)
{
   (*this) *= (1./a);
   return *this;
}

LcOperatorOnUnknown& LcOperatorOnUnknown::operator /=(const complex_t& a)
{
  (*this) *= (1./a);
   return *this;
}

// restrict LcOperatorOnUnknown (all operators) to domain dom
void LcOperatorOnUnknown::setDomain(GeomDomain& dom)
{
  it_opuval it=begin();
  for(; it!=end(); it++) it->first->domain() = &dom;
}

LcOperatorOnUnknown& LcOperatorOnUnknown::operator |(GeomDomain& dom)
{
  setDomain(dom);
  return *this;
}

//print utility
void LcOperatorOnUnknown::print(std::ostream& os, bool withdomain) const
{
  cit_opuval it=begin();
  if(it==end())
  {
    os<<" void LcOperatorOnUnknown";
    return;
  }
  complex_t a=it->second;
  if(a.imag()!=0) os<<a<<" * ";
  else
  {
    real_t ra=a.real();
    if(std::abs(ra)== 1)
    {
      if(ra==-1) os<<" - ";
    }
    else
    {
      if(ra>0) os<<ra<<" * ";
      else os<<" - "<<std::abs(ra)<<" * ";
    }
  }
  it->first->printsymbolic(os);
  it++;
  for(; it!=end(); it++)
  {
    complex_t a=it->second;
    if(a.imag()!=0) os<<" + "<<a<<" * ";
    else
    {
      real_t ra=a.real();
      if(std::abs(ra)== 1)
      {
        if(ra==1) os<<" + ";
        else os<<" - ";
      }
      else
      {
        if(ra>0) os<<" + "<<ra<<" * ";
        else os<<" - "<<std::abs(ra)<<" * ";
      }
    }
    it->first->printsymbolic(os);
  }
}

std::ostream& operator<<(std::ostream& os, const LcOperatorOnUnknown& lc)
{
  lc.print(os);
  return os;
}

//extern LcTermFunction functions

LcOperatorOnUnknown operator +(const LcOperatorOnUnknown& lc)  //unary +
{
  LcOperatorOnUnknown lcr(lc);
  return lcr;
}

LcOperatorOnUnknown operator -(const LcOperatorOnUnknown& lc)  //unary -
{
  LcOperatorOnUnknown lcr(lc);
  return lcr*=-1;
}

LcOperatorOnUnknown operator +(const LcOperatorOnUnknown& lc, const OperatorOnUnknown& opu)
{
  LcOperatorOnUnknown lcr(lc);
  return lcr+=opu;
}

LcOperatorOnUnknown operator -(const LcOperatorOnUnknown& lc, const OperatorOnUnknown& opu)
{
  LcOperatorOnUnknown lcr(lc);
  return lcr-=opu;
}
LcOperatorOnUnknown operator +(const OperatorOnUnknown& opu, const LcOperatorOnUnknown& lc)
{
  LcOperatorOnUnknown lcr(lc);
  return lcr+=opu;
}

LcOperatorOnUnknown operator -(const OperatorOnUnknown&opu , const LcOperatorOnUnknown& lc)
{
  LcOperatorOnUnknown lcr(-lc);
  return lcr+=opu;
}

LcOperatorOnUnknown operator +(const LcOperatorOnUnknown& lc, const Unknown& u)
{
  LcOperatorOnUnknown lcr(lc);
  return lcr+=u;
}

LcOperatorOnUnknown operator -(const LcOperatorOnUnknown& lc, const Unknown& u)
{
  LcOperatorOnUnknown lcr(lc);
  return lcr-=u;
}

LcOperatorOnUnknown operator +(const Unknown& u, const LcOperatorOnUnknown& lc)
{
  LcOperatorOnUnknown lcr(lc);
  return lcr+=u;
}

LcOperatorOnUnknown operator -(const Unknown& u, const LcOperatorOnUnknown& lc)
{
  LcOperatorOnUnknown lcr(-lc);
  return lcr+=u;
}
LcOperatorOnUnknown operator +(const LcOperatorOnUnknown& lc1, const LcOperatorOnUnknown& lc2)
{
  LcOperatorOnUnknown lcr(lc1);
  return lcr+=lc2;
}

LcOperatorOnUnknown operator -(const LcOperatorOnUnknown& lc1, const LcOperatorOnUnknown& lc2)
{
  LcOperatorOnUnknown lcr(lc1);
  return lcr-=lc2;
}

LcOperatorOnUnknown operator *(const LcOperatorOnUnknown& lc, const real_t& a)
{
  LcOperatorOnUnknown lcr(lc);
  return lcr*=a;
}

LcOperatorOnUnknown operator *(const LcOperatorOnUnknown& lc, const complex_t& a)
{
  LcOperatorOnUnknown lcr(lc);
  return lcr*=a;
}

LcOperatorOnUnknown operator *(const real_t& a, const LcOperatorOnUnknown& lc)
{
  LcOperatorOnUnknown lcr(lc);
  return lcr*=a;
}
LcOperatorOnUnknown operator *(const complex_t& a, const LcOperatorOnUnknown& lc)
{
  LcOperatorOnUnknown lcr(lc);
  return lcr*=a;
}

LcOperatorOnUnknown operator /(const LcOperatorOnUnknown& lc, const real_t& a)
{
  LcOperatorOnUnknown lcr(lc);
  return lcr/=a;
}
LcOperatorOnUnknown operator /(const LcOperatorOnUnknown& lc, const complex_t& a)
{
  LcOperatorOnUnknown lcr(lc);
  return lcr/=a;
}

LcOperatorOnUnknown operator +(const OperatorOnUnknown& opu, const OperatorOnUnknown& opv)
{
  LcOperatorOnUnknown lcr(opu);
  return lcr+=opv;
}

LcOperatorOnUnknown operator -(const OperatorOnUnknown& opu, const OperatorOnUnknown& opv)
{
  LcOperatorOnUnknown lcr(opu);
  return lcr-=opv;
}

LcOperatorOnUnknown operator +(const OperatorOnUnknown& opu, const Unknown& v)
{
  LcOperatorOnUnknown lcr(opu);
  return lcr+=v;
}

LcOperatorOnUnknown operator -(const OperatorOnUnknown& opu, const Unknown& v)
{
  LcOperatorOnUnknown lcr(opu);
  return lcr-=v;
}

LcOperatorOnUnknown operator +(const Unknown& u, const OperatorOnUnknown& opv)
{
  LcOperatorOnUnknown lcr(u);
  return lcr+=opv;
}
LcOperatorOnUnknown operator -(const Unknown& u, const OperatorOnUnknown& opv)
{
  LcOperatorOnUnknown lcr(u);
  return lcr-=opv;
}

LcOperatorOnUnknown operator +(const Unknown& u, const Unknown& v)
{
  LcOperatorOnUnknown lcr(u);
  return lcr+=v;
}
LcOperatorOnUnknown operator -(const Unknown& u, const Unknown& v)
{
  LcOperatorOnUnknown lcr(u);
  return lcr-=v;
}

} // end of namespace xlifepp

