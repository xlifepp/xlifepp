/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file OperatorOnKernel.cpp
  \author E. Lunéville
  \since 21 sep 2013
  \date 21 sep 2013

  \brief Implementation of xlifepp::OperatorOnKernel class members and related functions
 */

#include "OperatorOnKernel.hpp"

namespace xlifepp
{
//constructor
OperatorOnKernel::OperatorOnKernel(const Kernel& ker, DiffOpType dx, DiffOpType dy)
{
  ker_p=ker.clone();
  xdifOp_p=findDifferentialOperator(dx);
  ydifOp_p=findDifferentialOperator(dy);
  xydifOp_p=findDifferentialOperator(_id);
  ext_p=nullptr;
  in_ext=true;
  setDims();
  conjugate_=false;
  transpose_=false;
  noUpdatedNormal=false;
}

OperatorOnKernel::OperatorOnKernel(const Kernel* ker, DiffOpType dx, DiffOpType dy,
                                   ValueType vt, StrucType st, dimPair dims)
{
  ker_p=nullptr;
  if(ker!=nullptr) ker_p=ker->clone();
  xdifOp_p=findDifferentialOperator(dx);
  ydifOp_p=findDifferentialOperator(dy);
  xydifOp_p=findDifferentialOperator(_id);
  ext_p=nullptr;
  in_ext=true;
  type_=vt;
  struct_=st;
  dimsRes_=dims;
  conjugate_=false;
  transpose_=false;
  noUpdatedNormal=false;
}

OperatorOnKernel::OperatorOnKernel(const Kernel* ker, DiffOpType dx, DiffOpType dy, DiffOpType dxy,
                                   ValueType vt, StrucType st, dimPair dims)
{
  ker_p=ker->clone();
  xdifOp_p=findDifferentialOperator(dx);
  ydifOp_p=findDifferentialOperator(dy);
  xydifOp_p=findDifferentialOperator(dxy);
  ext_p=nullptr;
  in_ext=true;
  type_=vt;
  struct_=st;
  dimsRes_=dims;
  conjugate_=false;
  transpose_=false;
  noUpdatedNormal=false;
}

OperatorOnKernel::OperatorOnKernel(const OperatorOnKernel& opk)
{
  //ker_p= opk.ker_p;      //copy pointer!
  if(opk.ker_p!=nullptr) ker_p=opk.ker_p->clone(); //full copy to be thread safe
  else ker_p=nullptr;
  xdifOp_p=opk.xdifOp_p;
  ydifOp_p=opk.ydifOp_p;
  xydifOp_p=opk.xydifOp_p;
  ext_p=nullptr;
  if(opk.ext_p!=nullptr) ext_p=new Extension(*opk.ext_p);
  in_ext=opk.in_ext;
  type_=opk.type_;
  struct_=opk.struct_;
  dimsRes_=opk.dimsRes_;
  conjugate_=opk.conjugate_;
  transpose_=opk.transpose_;
  noUpdatedNormal=opk.noUpdatedNormal;
}

OperatorOnKernel& OperatorOnKernel::operator=(const OperatorOnKernel& opk)
{
  if(&opk==this) return *this;
  //ker_p= opk.ker_p;           //copy pointer!
  if(opk.ker_p!=nullptr) ker_p=opk.ker_p->clone(); //full copy to be thread safe
  else ker_p=nullptr;
  xdifOp_p=opk.xdifOp_p;
  ydifOp_p=opk.ydifOp_p;
  xydifOp_p=opk.xydifOp_p;
  if(ext_p!=nullptr) delete ext_p;
  ext_p=nullptr;
  if(opk.ext_p!=nullptr) ext_p=new Extension(*opk.ext_p);
  in_ext =opk.in_ext;
  type_=opk.type_;
  struct_=opk.struct_;
  dimsRes_=opk.dimsRes_;
  conjugate_=opk.conjugate_;
  transpose_=opk.transpose_;
  noUpdatedNormal=opk.noUpdatedNormal;
  return *this;
}

OperatorOnKernel::~OperatorOnKernel()
{
  if(ker_p!=nullptr) delete ker_p;
  if(ext_p!=nullptr) delete ext_p;
}

// set dims of operator on kernel, should not be called in FE computation
void OperatorOnKernel::setDims()
{
  dimsRes_=dimPair(1,1);
  struct_=_scalar;
  type_=ker_p->valueType();
  if(ker_p->type()==_tensorKernel) //dimension not set for tensorKernel, to do in future
    {
      struct_= ker_p->strucType();
      dimsRes_= ker_p->dims();   // no operation taken into account !!! To do in future
      return;
    }
  //evaluate operator on kernel to get the dimension of result
  Vector<real_t> nor(ker_p->dimPoint,0.);
  setNx(nor); setNy(nor);
  Point x(nor), y=x; y[0]=1.;
  if(type_==_real)
    {
      Vector<real_t> opk;
      eval(x,y,opk,&nor,&nor, &dimsRes_);
    }
  else
    {
      Vector<complex_t> opk;
      eval(x,y,opk,&nor,&nor,&dimsRes_);
    }
  //set struct_
  if(ker_p->strucType()==_matrix && dimsRes_==ker_p->dims())  //case of a Kernel that returns a matrix n x 1 (not robust!)
  {
     struct_=_matrix;
     return;
  }
  if(dimsRes_.first>1) struct_=_vector;
  if(dimsRes_.second>1) struct_=_matrix;
}

//return operator on kernel as an operator on function with x as parameter
const OperatorOnFunction& OperatorOnKernel::operator()(const Point& x, VariableName vy) const
{
  switch(ydifOpType())
    {
      case _id:         ker_p->setX(x); return id(ker_p->kernel);
      case _ntimes_y:   ker_p->setX(x); return ntimes(ker_p->kernel);
      case _ndot_y:     ker_p->setX(x); return ndot(ker_p->kernel);
      case _ncross_y:   ker_p->setX(x); return ncross(ker_p->kernel);
      case _ndotgrad_y: ker_p->grady.setX(x); return ndot(ker_p->grady);
      default:
        where("OperatorOnKernel::operator()");
        error("operator_unexpected",words("diffop",ydifOpType()));
    }
  return id(ker_p->kernel);
}

//return operator on kernel as an operator on x function with y as parameter
const OperatorOnFunction& OperatorOnKernel::operator()(VariableName vx, const Point& y) const
{

  switch(xdifOpType())
    {
      case _id:         ker_p->setY(y); return id(ker_p->kernel);
      case _ntimes_x:   ker_p->setY(y); return ntimes(ker_p->kernel);
      case _ndot_x:     ker_p->setY(y); return ndot(ker_p->kernel);
      case _ncross_x:   ker_p->setY(y); return ncross(ker_p->kernel);
      case _ndotgrad_x: ker_p->gradx.setY(y); return ndot(ker_p->gradx);
      default:
        where("OperatorOnKernel::operator()");
        error("operator_unexpected",words("diffop",xdifOpType()));
    }
  return id(ker_p->kernel);
}

//return operator on kernel as an operator on x/y function with y/x as parameter
const OperatorOnFunction& OperatorOnKernel::operator()(VariableName xory) const
{
  Point p(0.);
  if(xory==_x)  return (*this)(_x,p);
  if(xory==_y)  return (*this)(p,_y);
  where("OperatorOnKernel::operator()");
  error("opk_varname_not_handled");
  return id(ker_p->kernel);
}


void OperatorOnKernel::print(std::ostream& os) const
{
  if(theVerboseLevel==0) return;
  os<< "OperatorOnKernel "<<asString()<<" returning a " << words("value",type_)<<" "<<words("structure",struct_);
  if(struct_==_vector) os<<" ("<<dimsRes_.first<<")";
  if(struct_==_matrix) os<<" ("<<dimsRes_.first<<" x "<<dimsRes_.second<<")";
  if(theVerboseLevel>1)
    {
      os<<"\n       x differential operator: "<<* xdifOp_p;
      os<<"\n       y differential operator: "<<* ydifOp_p;
      os<<"\n      xy differential operator: "<<* xydifOp_p;
    }
  if(ext_p!=nullptr)
    {
      os<<"\n      with extension "<< ext_p->name()<<" on ";
      if(ext_p->var_==_x) os<<"x";
      else os<<"y";
      if(in_ext) os<<": ext(op(f))";
      else os<<": op(ext(f))";
    }
  os<<eol;
}

std::ostream& operator<<(std::ostream& os, const OperatorOnKernel& opk)
{
  opk.print(os);
  return os;
}

string_t OperatorOnKernel::asString() const //! return as symbolic string
{
  string_t s;
  string_t v="_x";
  if(ext_p!=nullptr && ext_p->var_==_y) v="_y";
  if(ext_p!=nullptr && !in_ext) s+=ext_p->name()+v+"(";
  if(transpose_)  s+="trans(";
  if(conjugate_)  s+="conj(";
  if(xdifOp_p!=nullptr && xdifOp_p->type()!=_id) s+=xdifOp_p->name()+"(";
  if(ydifOp_p!=nullptr && ydifOp_p->type()!=_id) s+=ydifOp_p->name()+"(";
  if(xydifOp_p!=nullptr && xydifOp_p->type()!=_id) s+=xydifOp_p->name()+"(";
  if(ext_p!=nullptr && in_ext) s+=ext_p->name()+v+"(";
  if(ker_p!=nullptr)
    {
      if(ker_p->transpose_)  s+="trans(";
      if(ker_p->conjugate_)  s+="conj(";
      if(ker_p->shortname=="" || ker_p->shortname=="?")
        {
          if(ker_p->type()==_tensorKernel) s+="TK";
          else s+="K";
        }
      else s+=ker_p->shortname;
      if(ker_p->transpose_||ker_p->conjugate_)  s+=")";
    }
  else s+="I";
  if(ext_p!=nullptr && in_ext) s+=")";
  if(xydifOp_p!=nullptr && xydifOp_p->type()!=_id) s+=")";
  if(ydifOp_p!=nullptr && ydifOp_p->type()!=_id) s+=")";
  if(xdifOp_p!=nullptr && xdifOp_p->type()!=_id) s+=")";
  if(transpose_||conjugate_)  s+=")";
  if(ext_p!=nullptr && !in_ext) s+=")";
  //s+="(x,y)";
  return s;
}

//-------------------------------------------------------------------------------
// differenial operator on Kernel
// WARNING: dimsRes_ is only managed for id
//-------------------------------------------------------------------------------
OperatorOnKernel& id(const Kernel& k)
{return*(new OperatorOnKernel(&k,_id,_id,k.valueType(),k.strucType(),k.dims()));}

OperatorOnKernel& grad_x(const Kernel& k)
{
  if (k.gradx.isVoidFunction())
  {
    where("grad_x(Kernel)");
    error("kernel_op_not_handled", k.shortname, "gradx");
  }
  if (k.strucType()==_scalar)
  {
    if (k.gradx.strucType()!=_vector)
    {
      where("grad_x(Kernel)");
      error("vector_only");
    }
    return *(new OperatorOnKernel(&k,_grad_x,_id,k.valueType(),_vector, k.gradx.dims()));
  }
  if (k.strucType()==_vector)
  {
    if (k.gradx.strucType()!=_matrix)
    {
      where("grad_x(Kernel)");
      error("matrix_only");
    }
    return*(new OperatorOnKernel(&k,_grad_x,_id,k.valueType(),_matrix, k.gradx.dims()));
  }
  error("operator_unexpected","grad_x(kernel)");
  return * (new OperatorOnKernel());  //fake return
}

OperatorOnKernel& grad_y(const Kernel& k)
{
  if (k.grady.isVoidFunction())
  {
    where("grad_y(Kernel)");
    error("kernel_op_not_handled", k.shortname, "grady");
  }
  if (k.strucType()==_scalar)
  {
    if (k.gradx.strucType()!=_vector)
    {
      where("grad_y(Kernel)");
      error("vector_only");
    }
    return *(new OperatorOnKernel(&k,_id,_grad_y,k.valueType(),_vector, k.grady.dims()));
  }
  if (k.strucType()==_vector)
  {
    if (k.grady.strucType()!=_matrix)
    {
      where("grad_y(Kernel)");
      error("matrix_only");
    }
    return *(new OperatorOnKernel(&k,_id,_grad_y,k.valueType(),_matrix, k.grady.dims()));
  }
  error("operator_unexpected","grad_y(matrix kernel)");
  return * (new OperatorOnKernel(&k));  //fake return
}

OperatorOnKernel& nabla_x(const Kernel& k)
{return grad_x(k);}

OperatorOnKernel& nabla_y(const Kernel& k)
{return grad_y(k);}

OperatorOnKernel& div_x(const Kernel& k)
{
  if (k.divx.isVoidFunction())
  {
    where("div_x(Kernel)");
    error("kernel_op_not_handled", k.shortname, "divx");
  }
  if (k.strucType()==_vector)
  {
    if (k.divx.strucType()!=_scalar)
    {
      where("div_x(Kernel)");
      error("scalar_only");
    }
    return *(new OperatorOnKernel(&k,_div_x,_id,k.valueType(),_scalar));
  }
  if (k.strucType()==_matrix)
  {
    if (k.divx.strucType()!=_vector)
    {
      where("div_x(Kernel)");
      error("vector_only");
    }
    return *(new OperatorOnKernel(&k,_div_x,_id,k.valueType(),_vector, k.divx.dims()));
  }
  error("operator_unexpected","div_x(kernel)");
  return * (new OperatorOnKernel(&k));  //fake return
}

OperatorOnKernel& div_y(const Kernel& k)
{
  if( k.divy.isVoidFunction())
  {
    where("div_y(Kernel)");
    error("kernel_op_not_handled", k.shortname, "divy");
  }
  if (k.strucType()==_vector)
  {
    if (k.divy.strucType()!=_scalar)
    {
      where("div_y(Kernel)");
      error("scalar_only");
    }
    return *(new OperatorOnKernel(&k,_id,_div_y,k.valueType(),_scalar));
  }
  if (k.strucType()==_matrix)
  {
    if (k.divy.strucType()!=_vector)
    {
      where("div_y(Kernel)");
      error("vector_only");
    }
    return*(new OperatorOnKernel(&k,_id,_div_y,k.valueType(),_vector, k.divy.dims()));
  }
  error("operator_unexpected","div_y(kernel)");
  return * (new OperatorOnKernel(&k));  //fake return
}

OperatorOnKernel& curl_x(const Kernel& k)
{
  if (k.curlx.isVoidFunction())
  {
    where("curl_x(Kernel)");
    error("kernel_op_not_handled", k.shortname, "curlx");
  }
  if (k.curlx.strucType()!=k.strucType())
  {
    where("curl_x(Kernel)");
    error("structure_only", words("structure",k.strucType()));
  }
  return *(new OperatorOnKernel(&k,_curl_x,_id,k.valueType(),k.strucType(),k.curlx.dims()));
}

OperatorOnKernel& curl_y(const Kernel& k)
{
  if (k.curly.isVoidFunction())
  {
    where("curl_y(Kernel)");
    error("kernel_op_not_handled", k.shortname, "curly");
  }
  if (k.curly.strucType()!=k.strucType())
  {
    where("curl_y(Kernel)");
    error("structure_only", words("structure",k.strucType()));
  }
  return *(new OperatorOnKernel(&k,_id,_curl_y, k.valueType(),k.strucType(),k.curly.dims()));
}

OperatorOnKernel& rot_x(const Kernel& k)
{return curl_x(k);}

OperatorOnKernel& rot_y(const Kernel& k)
{return curl_y(k);}

OperatorOnKernel& trac_x(const Kernel& k)
{
    if(k.tracx.isVoidFunction())
        error("free_error", " in trac_x operator, kernel "+k.shortname+" does not handle tracx function");
    if(k.tracx.strucType()!=k.strucType())
        error("free_error", " in trac_x operator, the involved function "+k.tracx.name()+" should return a "+words("structure",k.strucType()));
    return*(new OperatorOnKernel(&k,_trac_x,_id,k.valueType(),k.strucType(),k.tracx.dims()));
}

OperatorOnKernel& trac_y(const Kernel& k)
{
     if(k.tracy.isVoidFunction())
        error("free_error", " in trac_y operator, kernel "+k.shortname+" does not handle tracy function");
    if(k.tracy.strucType()!=k.strucType())
        error("free_error", " in trac_y operator, the involved function "+k.tracy.name()+" should return a "+words("structure",k.strucType()));
    return*(new OperatorOnKernel(&k,_id,_trac_y, k.valueType(),k.strucType(),k.tracy.dims()));
}

OperatorOnKernel& nxtensornxtimesNGr(const Kernel& k)
{
    if(k.nxtensornxtimesNGr.isVoidFunction())
        error("free_error", " in nxtensornxtimesNGr operator, kernel "+k.shortname+" does not handle nxtensornxtimesNGr function");
    if(k.nxtensornxtimesNGr.strucType()!=k.strucType())
        error("free_error", " in nxtensornxtimesNGr operator, the involved function "+k.nxtensornxtimesNGr.name()+" should return a "+words("structure",k.strucType()));
    return*(new OperatorOnKernel(&k,_id,_nxtensornxtimesNGr,k.valueType(),k.strucType(),k.nxtensornxtimesNGr.dims()));
}

OperatorOnKernel& nxtensornxtimestrac_y(const Kernel& k)
{
    if(k.nxtensornxtimestracy.isVoidFunction())
        error("free_error", " in nxtensornxtimestrac_y operator, kernel "+k.shortname+" does not handle nxtensornxtimestrac_y function");
    if(k.nxtensornxtimestracy.strucType()!=k.strucType())
        error("free_error", " in nxtensornxtimestrac_y operator, the involved function "+k.nxtensornxtimestracy.name()+" should return a "+words("structure",k.strucType()));
    return*(new OperatorOnKernel(&k,_id,_nxtensornxtimestrac_y,k.valueType(),k.strucType(),k.nxtensornxtimestracy.dims()));
}

OperatorOnKernel& ntimes_x(const Kernel& k)  // nx*k
{
  if (k.strucType()==_scalar)
  {
    return *(new OperatorOnKernel(&k,_ntimes_x,_id,k.valueType(),_vector));
  }
  error("operator_unexpected","ntimes_x(kernel)");
  return *(new OperatorOnKernel(&k));
}

OperatorOnKernel& ntimes_y(const Kernel& k) // ny*k
{
  if (k.strucType()==_scalar) return *(new OperatorOnKernel(&k,_id,_ntimes_y,k.valueType(),_vector));
  error("operator_unexpected","ntimes_y(kernel)");
  return * (new OperatorOnKernel(&k));  //fake return
}

OperatorOnKernel& ndot_x(const Kernel& k)  // nx|k
{
  if (k.strucType()==_vector) return *(new OperatorOnKernel(&k,_ndot_x,_id,k.valueType(),_scalar));
  if (k.strucType()==_matrix) return *(new OperatorOnKernel(&k,_ndot_x,_id,k.valueType(),_vector, dimPair(k.dims().first,1)));
  error("operator_unexpected","ndot_x(kernel)");
  return * (new OperatorOnKernel(&k));  //fake return
}

OperatorOnKernel& ndot_y(const Kernel& k)  // ny|k
{
  if (k.strucType()==_vector) return *(new OperatorOnKernel(&k,_id,_ndot_y,k.valueType(),_scalar));
  if (k.strucType()==_matrix) return *(new OperatorOnKernel(&k,_id,_ndot_y,k.valueType(),_vector, dimPair(k.dims().first,1)));
  error("operator_unexpected","ndot_y(kernel)");
  return * (new OperatorOnKernel(&k));  //fake return
}

OperatorOnKernel& ncross_x(const Kernel& k)  // nx^k
{
  return *(new OperatorOnKernel(&k,_ncross_x,_id,k.valueType(),k.strucType(), k.dims()));
}

OperatorOnKernel& ncross_y(const Kernel& k)  // ny^k
{
  return *(new OperatorOnKernel(&k,_id,_ncross_y,k.valueType(),k.strucType(), k.dims()));
}

OperatorOnKernel& ncrossncross_x(const Kernel& k)  // nx^nx^k
{
  return *(new OperatorOnKernel(&k,_ncrossncross_x,_id,k.valueType(),k.strucType(), k.dims()));
}

OperatorOnKernel& ncrossncross_y(const Kernel& k)  // ny^ny^k
{
  return *(new OperatorOnKernel(&k,_id,_ncrossncross_y,k.valueType(),k.strucType(), k.dims()));
}

OperatorOnKernel& ndotgrad_x(const Kernel& k)  // nx|grad_x(k)
{
  if (k.gradx.isVoidFunction() && k.ndotgradx.isVoidFunction())
  {
    where("ndotgrad_x(Kernel)");
    error("kernel_op_not_handled", k.shortname, "gradx");
  }
  if (k.strucType()==_scalar)
    return *(new OperatorOnKernel(&k,_ndotgrad_x,_id,k.valueType(),_scalar));
  where("ndotgrad_x(Kernel)");
  error("operator_unexpected","ndotgrad_x(kernel)");
  return * (new OperatorOnKernel(&k));  //fake return
}

OperatorOnKernel& ndotgrad_y(const Kernel& k)  // ny|grad_y(k)
{
  if (k.grady.isVoidFunction() && k.ndotgrady.isVoidFunction())
  {
    where("ndotgrad_y(Kernel)");
    error("kernel_op_not_handled", k.shortname, "grady");
  }
  if (k.strucType()==_scalar)
    return *(new OperatorOnKernel(&k,_id,_ndotgrad_y,k.valueType(),_scalar));
  error("operator_unexpected","ndotgrad_y(kernel)");
  return * (new OperatorOnKernel(&k));  //fake return
}

OperatorOnKernel& ndiv_x(const Kernel& k)  // nx*div_x(k)
{
  if (k.divx.isVoidFunction())
  {
    where("ndiv_x(Kernel)");
    error("kernel_op_not_handled", k.shortname, "divx");
  }
  if (k.strucType()==_vector) return *(new OperatorOnKernel(&k,_ndiv_x,_id,k.valueType(),_vector, k.dims()));
  error("operator_unexpected","ndiv_x(kernel");
  return * (new OperatorOnKernel(&k));  //fake return
}

OperatorOnKernel& ndiv_y(const Kernel& k)  // ny*div_y(k)
{
  if (k.divy.isVoidFunction())
  {
    where("ndiv_y(Kernel)");
    error("kernel_op_not_handled", k.shortname, "divy");
  }
  if (k.strucType()==_vector) return *(new OperatorOnKernel(&k,_id,_ndiv_y,k.valueType(),_vector, k.dims()));
  error("operator_unexpected","ndiv_y(kernel");
  return * (new OperatorOnKernel(&k));  //fake return
}

OperatorOnKernel& ncrosscurl_x(const Kernel& k)  // nx^curl_x(k)
{
  if (k.curlx.isVoidFunction())
  {
    where("ncrosscurl_x(Kernel)");
    error("kernel_op_not_handled", k.shortname, "curlx");
  }
  return *(new OperatorOnKernel(&k,_ncrosscurl_x,_id,k.valueType(),k.strucType(), k.dims()));
}

OperatorOnKernel& ncrosscurl_y(const Kernel& k)  // ny^curl_y(k)
{
  if (k.curly.isVoidFunction())
  {
    where("ncrosscurl_y(Kernel)");
    error("kernel_op_not_handled", k.shortname, "curly");
  }
  return *(new OperatorOnKernel(&k,_id,_ncrosscurl_y,k.valueType(),k.strucType(), k.dims()));
}

OperatorOnKernel& ncrossrot_x(const Kernel& k)  // nx^curl_x(k)
{
  return ncrosscurl_x(k);
}

OperatorOnKernel& ncrossrot_y(const Kernel& k)  // ny^curl_y(k)
{
  return ncrosscurl_y(k);
}

OperatorOnKernel& nxdotny_times(const Kernel& k) // (nx.ny)*k
{
  return *(new OperatorOnKernel(&k,_id,_id,_nxdotny_times,k.valueType(),k.strucType(), k.dims()));
}

OperatorOnKernel& nxcrossny_dot(const Kernel& k) // (nx.ny).k
{
  return *(new OperatorOnKernel(&k,_id,_id,_nxcrossny_dot,k.valueType(),k.strucType(), k.dims()));
}

OperatorOnKernel& nycrossnx_dot(const Kernel& k) // (ny^nx).k
{
  return *(new OperatorOnKernel(&k,_id,_id,_nycrossnx_dot,k.valueType(),k.strucType(), k.dims()));
}

OperatorOnKernel& nxcrossny_cross(const Kernel& k) // (nx^ny)^k
{
  return *(new OperatorOnKernel(&k,_id,_id,_nxcrossny_cross,k.valueType(),k.strucType(), k.dims()));
}

OperatorOnKernel& nycrossnx_cross(const Kernel& k) // (ny^ny)^k
{
  return *(new OperatorOnKernel(&k,_id,_id,_nycrossnx_cross,k.valueType(),k.strucType(), k.dims()));
}

//-------------------------------------------------------------------------------
// overriding operators on Kernel and OperatorOnKernel
//-------------------------------------------------------------------------------
OperatorOnKernel& operator*(UnitaryVector v, const Kernel& k)     // n*ker same as ntimes(ker)
{
  if(v == _nx) return ntimes_x(k);
  if(v == _ny) return ntimes_y(k);
  if(v == _nxdotny) return nxdotny_times(k);
  error("operator_unexpected","UnitaryVector * kernel");
  return * (new OperatorOnKernel(&k));  //fake return
}
OperatorOnKernel& operator*(const Kernel& k,UnitaryVector v)     // n*ker same as ntimes(ker)
{
  if(v == _nx) return ntimes_x(k);
  if(v == _ny) return ntimes_y(k);
  if(v == _nxdotny) return nxdotny_times(k);
  error("operator_unexpected","kernel * UnitaryVector");
  return * (new OperatorOnKernel(&k));  //fake return
}

OperatorOnKernel& operator|(UnitaryVector v, const Kernel& k)     // n*ker same as ntimes(ker)
{
  if(v == _nx) return ndot_x(k);
  if(v == _ny) return ndot_y(k);
  if(v == _nxcrossny) return nxcrossny_dot(k);
  if(v == _nycrossnx) return nycrossnx_dot(k);
  error("operator_unexpected"," UnitaryVector | kernel");
  return * (new OperatorOnKernel(&k));  //fake return
}
OperatorOnKernel& operator|(const Kernel& k,UnitaryVector v)     // n*ker same as ntimes(ker)
{
  if(v == _nx) return ndot_x(k);
  if(v == _ny) return ndot_y(k);
  if(v == _nxcrossny) return nxcrossny_dot(k);
  if(v == _nycrossnx) return nycrossnx_dot(k);
  error("operator_unexpected","kernel | UnitaryVector");
  return * (new OperatorOnKernel(&k));  //fake return
}

OperatorOnKernel& operator^(UnitaryVector v, const Kernel& k)     // n*ker same as ntimes(ker)
{
  if(v == _nx) return ncross_x(k);
  if(v == _ny) return ncross_y(k);
  if(v == _nxcrossny) return nxcrossny_cross(k);
  if(v == _nycrossnx) return nycrossnx_cross(k);
  error("operator_unexpected"," UnitaryVector ^ kernel");
  return *(new OperatorOnKernel(&k));
}


OperatorOnKernel& id(OperatorOnKernel& opk)               // id(opk)
{
  return opk;
}

OperatorOnKernel& grad_x(OperatorOnKernel& opk)           // grad_x(opk)
{
  StrucType st=opk.struct_;  //current struct
  const Kernel* k=opk.kernelp();
  if (st ==_scalar)
  {
    if (k->gradx.isVoidFunction())
    {
      where("grad_x(OperatorOnKernel)");
      error("kernel_op_not_handled", k->shortname, "gradx");
    }
    opk.xdifOp_() = findDifferentialOperator(_grad_x);
    opk.strucType()=_vector;
    opk.dimsRes_=k->gradx.dims();
  }
  if (st ==_vector)
  {
    if (k->gradxy.isVoidFunction())
    {
      where("grad_x(OperatorOnKernel)");
      error("kernel_op_not_handled", k->shortname, "gradxy");
    }
    opk.xdifOp_() = findDifferentialOperator(_grad_x);
    opk.strucType()=_matrix;
    if (opk.ydifOpType()==_grad_y) opk.dimsRes_=k->gradxy.dims();
    else opk.dimsRes_.second = opk.dimsRes_.first; //same dimension
  }
  if (st==_matrix) error("operator_unexpected"," grad_x(OperatorOnKernel)");
  if (opk.ext_p!=nullptr) opk.in_ext=false;
  return opk;
}

OperatorOnKernel& grad_y(OperatorOnKernel& opk)           // grad_y(opk)
{
  StrucType st=opk.struct_;  //current struct
  const Kernel* k=opk.kernelp();
  if (st ==_scalar)
  {
    opk.strucType()=_vector;
    opk.dimsRes_=opk.kernelp()->grady.dims();
    if (k->grady.isVoidFunction())
    {
      where("grad_y(OperatorOnKernel)");
      error("kernel_op_not_handled", k->shortname, "grady");
    }
    opk.ydifOp_() = findDifferentialOperator(_grad_y);
  }
  if (st ==_vector)
  {
    opk.strucType()=_matrix;
    if (opk.xdifOpType()==_grad_x) opk.dimsRes_=opk.kernelp()->gradxy.dims();
    else opk.dimsRes_.second = opk.dimsRes_.first; //same dimension
    if (k->gradxy.isVoidFunction())
    {
      where("grad_y(OperatorOnKernel)");
      error("kernel_op_not_handled", k->shortname, "gradxy");
    }
    opk.ydifOp_() = findDifferentialOperator(_grad_y);
  }
  if (st==_matrix) error("operator_unexpected"," grad_y(OperatorOnKernel)");
  if (opk.ext_p!=nullptr) opk.in_ext=false;
  return opk;
}

OperatorOnKernel& nabla_x(OperatorOnKernel& opk) // grad_x(opk)
{
  return grad_x(opk);
}

OperatorOnKernel& nabla_y(OperatorOnKernel& opk) // grad_y(opk)
{
  return grad_y(opk);
}

OperatorOnKernel& div_x(OperatorOnKernel& opk)   // div_x(opk)
{
  StrucType st=opk.strucType();
  const Kernel* k=opk.kernelp();
  if (opk.xdifOpType()!=_id || st ==_scalar) error("operator_unexpected"," div_x(OperatorOnKernel)");
  opk.xdifOp_()=findDifferentialOperator(_div_x);
  if (st ==_vector)
  {
    opk.strucType()=_scalar;
    opk.dimsRes_=dimPair(1,1);
    switch (opk.ydifOpType())
    {
      case _id:
      {
        if (k->divx.isVoidFunction())
        {
          where("div_x(OperatorOnKernel)");
          error("kernel_op_not_handled", k->shortname, "divx");
        }
        break;
      }
      case _div_y:
      {
        if (k->divxy.isVoidFunction())
        {
          where("div_x(OperatorOnKernel)");
          error("kernel_op_not_handled", k->shortname, "divxy");
        }
        break;
      }
      default: error("operator_unexpected"," div_x(OperatorOnKernel)");
    }
    if (opk.ext_p!=nullptr) opk.in_ext=false;
    return opk;
  }

  if (st ==_matrix)
  {
    opk.strucType()=_vector;
    if (opk.ydifOpType()!=_id) error("operator_unexpected"," div_x(OperatorOnKernel)");
    if (k->divx.isVoidFunction())
    {
      where("div_x(OperatorOnKernel)");
      error("kernel_op_not_handled", k->shortname, "divx");
    }
    opk.dimsRes_= k->divx.dims();
  }
  if(opk.ext_p!=nullptr) opk.in_ext=false;
  return opk;
}

OperatorOnKernel& div_y(OperatorOnKernel& opk)   // div_y(opk)
{
  StrucType st=opk.strucType();
  const Kernel* k=opk.kernelp();
  if (opk.ydifOpType()!=_id || st ==_scalar) error("operator_unexpected"," div_x(OperatorOnKernel)");
  opk.ydifOp_()=findDifferentialOperator(_div_y);
  if (st ==_vector)
  {
    opk.strucType()=_scalar;
    opk.dimsRes_=dimPair(1,1);
    switch (opk.xdifOpType())
    {
      case _id:
      {
        if (k->divy.isVoidFunction())
        {
          where("div_y(OperatorOnKernel)");
          error("kernel_op_not_handled", k->shortname, "divy");
        }
        break;
      }
      case _div_x:
      {
        if (k->divxy.isVoidFunction())
        {
          where("div_y(OperatorOnKernel)");
          error("kernel_op_not_handled", k->shortname, "divxy");
        }
        break;
      }
      default: error("operator_unexpected"," div_y(OperatorOnKernel)");
    }
    if (opk.ext_p!=nullptr) opk.in_ext=false;
    return opk;
  }
  if (st ==_matrix)
  {
    opk.strucType()=_vector;
    if (opk.xdifOpType()!=_id) error("operator_unexpected"," div_y(OperatorOnKernel)");
    if (k->divy.isVoidFunction())
    {
      where("div_y(OperatorOnKernel)");
      error("kernel_op_not_handled", k->shortname, "divy");
    }
    opk.dimsRes_= k->divxy.dims();
  }
  if (opk.ext_p!=nullptr) opk.in_ext=false;
  return opk;
}

OperatorOnKernel& curl_x(OperatorOnKernel& opk)   // curl_x(opk)
{
  const Kernel* k=opk.kernelp();
  opk.xdifOp_()=findDifferentialOperator(_curl_x);
  opk.struct_=opk.kernelp()->strucType();
  opk.dimsRes_=opk.kernelp()->dims();
  if (opk.ydifOpType()==_id)
  {
    if (k->curlx.isVoidFunction())
    {
      where("curl_x(OperatorOnKernel)");
      error("kernel_op_not_handled", k->shortname, "curlx");
    }
  }
  else if(opk.ydifOpType()==_curl_y)
  {
    if (k->curlxy.isVoidFunction())
    {
      where("curl_x(OperatorOnKernel)");
      error("kernel_op_not_handled", k->shortname, "curlxy");
    }
  }
  else error("operator_unexpected"," curl_x(OperatorOnKernel)");
  if(opk.ext_p!=nullptr) opk.in_ext=false;
  return opk;
}

OperatorOnKernel& curl_y(OperatorOnKernel& opk)   // curl_y(opk)
{
  const Kernel* k=opk.kernelp();
  opk.ydifOp_()=findDifferentialOperator(_curl_y);
  opk.struct_=opk.kernelp()->strucType();
  opk.dimsRes_=opk.kernelp()->dims();
  if (opk.xdifOpType()==_id)
  {
    if (k->curly.isVoidFunction())
    {
      where("curl_y(OperatorOnKernel)");
      error("kernel_op_not_handled", k->shortname, "curly");
    }
  }
  else if(opk.xdifOpType()==_curl_x)
  {
    if (k->curlxy.isVoidFunction())
    {
      where("curl_y(OperatorOnKernel)");
      error("kernel_op_not_handled", k->shortname, "curlxy");
    }
  }
  else error("operator_unexpected"," curl_y(OperatorOnKernel)");
  if (opk.ext_p!=nullptr) opk.in_ext=false;
  return opk;
}

OperatorOnKernel& rot_x(OperatorOnKernel& opk)   // curl_x(opk)
{
  return curl_x(opk);
}

OperatorOnKernel& rot_y(OperatorOnKernel& opk)   // curl_y(opk)
{
  return curl_y(opk);
}

OperatorOnKernel& trac_x(OperatorOnKernel& opk)   // trac_x(opk)
{
    const Kernel* k=opk.kernelp();
    opk.xdifOp_()=findDifferentialOperator(_trac_x);
    opk.struct_=opk.kernelp()->strucType();
    opk.dimsRes_=opk.kernelp()->dims();
    if(opk.ydifOpType()==_id)
    {
        if(k->tracx.isVoidFunction())
            error("free_error", " in trac_x operator, kernel "+k->shortname+" does not handle tracx function");
    }
    else if(opk.ydifOpType()==_trac_y)
    {
        if(k->tracxy.isVoidFunction())
            error("free_error", " in trac_y(trac_x) operator, kernel "+k->shortname+" does not handle tracxy function");
    }
    else  error("operator_unexpected"," trac_x(OperatorOnKernel)");
    if(opk.ext_p!=nullptr) opk.in_ext=false;
    return opk;
}

OperatorOnKernel& trac_y(OperatorOnKernel& opk)   // trac_y(opk)
{
     const Kernel* k=opk.kernelp();
    opk.ydifOp_()=findDifferentialOperator(_trac_y);
    opk.struct_=opk.kernelp()->strucType();
    opk.dimsRes_=opk.kernelp()->dims();
    if(opk.xdifOpType()==_id)
    {
        if(k->tracy.isVoidFunction())
            error("free_error", " in trac_y operator, kernel "+k->shortname+" does not handle tracy function");
     }
    else if(opk.xdifOpType()==_trac_x)
    {
        if(k->tracxy.isVoidFunction())
            error("free_error", " in trac_x(trac_y) operator, kernel "+k->shortname+" does not handle tracxy function");
    }
    else  error("operator_unexpected"," trac_y(OperatorOnKernel)");
    if(opk.ext_p!=nullptr) opk.in_ext=false;
    return opk;
}

OperatorOnKernel& ntimes_x(OperatorOnKernel& opk) //! nx*opk
{
  if (opk.xdifOpType()==_id && opk.strucType()==_scalar)
  {
    opk.xdifOp_()=findDifferentialOperator(_ntimes_x);
    opk.strucType()=_vector;
    return opk;
  }
  if (opk.xdifOpType()==_div_x && opk.strucType()==_vector)
  {
    opk.xdifOp_()=findDifferentialOperator(_ndiv_x);
    opk.strucType()=_vector;
    return opk;
  }
  error("operator_unexpected"," nx * OperatorOnKernel");
  return opk;
}

OperatorOnKernel& ntimes_y(OperatorOnKernel& opk) //! ny*opk
{
  if (opk.ydifOpType()==_id && opk.strucType()==_scalar)
  {
    opk.ydifOp_()=findDifferentialOperator(_ntimes_y);
    opk.strucType()=_vector;
    return opk;
  }
  if (opk.ydifOpType()==_div_y && opk.strucType()==_vector)
  {
    opk.ydifOp_()=findDifferentialOperator(_ndiv_y);
    opk.strucType()=_vector;
    return opk;
  }
  error("operator_unexpected"," nx * OperatorOnKernel");
  return opk;
}

OperatorOnKernel& nxdotny_times(OperatorOnKernel& opk) //!< (nx.ny)*opk
{
  opk.xydifOp_() = findDifferentialOperator(_nxdotny_times);
  return opk;
}

OperatorOnKernel& nxcrossny_dot(OperatorOnKernel& opk) //!< (nx.ny).opk
{
  if (opk.strucType()==_vector)
  {
    opk.xydifOp_() = findDifferentialOperator(_nxcrossny_dot);
    opk.strucType()=_scalar;
    return opk;
  }
  error("operator_unexpected"," (nx^ny) . OperatorOnKernel");
  return opk;
}

OperatorOnKernel& nycrossnx_dot(OperatorOnKernel& opk) //!< (nx.ny).opk
{
  if (opk.strucType()==_vector)
  {
    opk.xydifOp_() = findDifferentialOperator(_nycrossnx_dot);
    opk.strucType()=_scalar;
    return opk;
  }
  error("operator_unexpected"," (ny^nx) . OperatorOnKernel");
  return opk;
}

OperatorOnKernel& nxcrossny_cross(OperatorOnKernel& opk) //!< (nx.ny).opk
{
  if (opk.strucType()==_vector)
  {
    opk.xydifOp_() = findDifferentialOperator(_nxcrossny_cross);
    return opk;
  }
  error("operator_unexpected"," (nx^ny) ^ OperatorOnKernel");
  return opk;
}

OperatorOnKernel& nycrossnx_cross(OperatorOnKernel& opk) //!< (nx.ny).opk
{
  if (opk.strucType()==_vector)
  {
    opk.xydifOp_() = findDifferentialOperator(_nycrossnx_cross);
    return opk;
  }
  error("operator_unexpected"," (ny^nx) ^ OperatorOnKernel");
  return opk;
}

OperatorOnKernel& ndot_x(OperatorOnKernel& opk)    // nx|opk
{
  if (opk.xdifOpType()==_id && opk.strucType()==_vector)
  {
    opk.xdifOp_()=findDifferentialOperator(_ndot_x);
    opk.strucType()=_scalar;
    opk.dimsRes_.first=1;
    return opk;
  }
  if (opk.xdifOpType()==_grad_x)
  {
    opk.xdifOp_()=findDifferentialOperator(_ndotgrad_x);
    opk.strucType()=_scalar;
    opk.dimsRes_.first=1;
    return opk;
  }
  error("operator_unexpected"," nx | OperatorOnKernel");
  return opk;
}

OperatorOnKernel& ndot_y(OperatorOnKernel& opk)    // ny|opk
{
  if (opk.ydifOpType()==_id && opk.strucType()==_vector)
  {
    opk.ydifOp_()=findDifferentialOperator(_ndot_y);
    opk.strucType()=_scalar;
    opk.dimsRes_.first=1;
    return opk;
  }
  if (opk.ydifOpType()==_grad_y)
  {
    opk.ydifOp_()=findDifferentialOperator(_ndotgrad_y);
    opk.strucType()=_scalar;
    opk.dimsRes_.first=1;
    return opk;
  }
  error("operator_unexpected"," ny | OperatorOnKernel");
  return opk;
}

OperatorOnKernel& ncross_x(OperatorOnKernel& opk)    // nx^opk
{
  if (opk.xdifOpType()==_id)
  {
    opk.xdifOp_()=findDifferentialOperator(_ncross_x);
    return opk;
  }
  if (opk.xdifOpType()==_ncross_x)
  {
    opk.xdifOp_()=findDifferentialOperator(_ncrossncross_x);
    return opk;
  }
  if (opk.xdifOpType()==_curl_x)
  {
    opk.xdifOp_()=findDifferentialOperator(_ncrosscurl_x);
    return opk;
  }
  error("operator_unexpected"," nx ^ OperatorOnKernel");
  return opk;
}

OperatorOnKernel& ncross_y(OperatorOnKernel& opk)    // ny^opk
{
  if (opk.ydifOpType()==_id)
  {
    opk.ydifOp_()=findDifferentialOperator(_ncross_y);
    return opk;
  }
  if (opk.ydifOpType()==_ncross_y)
  {
    opk.ydifOp_()=findDifferentialOperator(_ncrossncross_y);
    return opk;
  }
  if (opk.ydifOpType()==_curl_y)
  {
    opk.ydifOp_()=findDifferentialOperator(_ncrosscurl_y);
    return opk;
  }
  error("operator_unexpected"," ny ^ OperatorOnKernel");
  return opk;
}

OperatorOnKernel& ncrossncross_x(OperatorOnKernel& opk)    // nx^nx^opk
{
  if (opk.xdifOpType()==_id)
  {
    opk.xdifOp_()=findDifferentialOperator(_ncrossncross_x);
    return opk;
  }
  error("operator_unexpected"," nx ^ OperatorOnKernel");
  return opk;
}

OperatorOnKernel& ncrossncross_y(OperatorOnKernel& opk)    // ny^ny^opk
{
  if (opk.ydifOpType()==_id)
  {
    opk.ydifOp_()=findDifferentialOperator(_ncrossncross_y);
    return opk;
  }
  error("operator_unexpected"," ny ^ OperatorOnKernel");
  return opk;
}

OperatorOnKernel& ndotgrad_x(OperatorOnKernel& opk)    // nx.grad_x opk
{
  if (opk.xdifOpType()==_id)
  {
    opk.xdifOp_()=findDifferentialOperator(_ndotgrad_x);
    const Kernel& k=*opk.kernelp();
    if (k.gradx.isVoidFunction() && k.ndotgradx.isVoidFunction())
    {
      where("ndotgrad_x(OperatorOnKernel)");
      error("kernel_op_not_handled", k.shortname, "(ndot)gradx");
    }
    return opk;
  }
  error("operator_unexpected"," nx.grad OperatorOnKernel");
  if (opk.ext_p!=nullptr) opk.in_ext=false;
  return opk;
}

OperatorOnKernel& ndotgrad_y(OperatorOnKernel& opk)    // ny.grad_y opk
{
  if (opk.ydifOpType()==_id)
  {
    opk.ydifOp_()=findDifferentialOperator(_ndotgrad_y);
    const Kernel& k=*opk.kernelp();
    if (k.grady.isVoidFunction() && k.ndotgrady.isVoidFunction())
    {
      where("ndotgrad_y(OperatorOnKernel)");
      error("kernel_op_not_handled", k.shortname, "(ndot)grady");
    }
    return opk;
  }
  error("operator_unexpected"," ny.grad OperatorOnKernel");
  if (opk.ext_p!=nullptr) opk.in_ext=false;
  return opk;
}

OperatorOnKernel& ndiv_x(OperatorOnKernel& opk)    // nx*div_x opk
{
  if (opk.xdifOpType()==_id)
  {
    opk.xdifOp_()=findDifferentialOperator(_ndiv_x);
    const Kernel& k=*opk.kernelp();
    if (k.divx.isVoidFunction())
    {
      where("ndiv_x(OperatorOnKernel)");
      error("kernel_op_not_handled", k.shortname, "divx");
    }
    return opk;
  }
  error("operator_unexpected"," nx*div_x OperatorOnKernel");
  if (opk.ext_p!=nullptr) opk.in_ext=false;
  return opk;
}

OperatorOnKernel& ndiv_y(OperatorOnKernel& opk)    //ny*div_y opk
{
  if (opk.ydifOpType()==_id)
  {
    opk.ydifOp_()=findDifferentialOperator(_ndiv_y);
    const Kernel& k=*opk.kernelp();
    if (k.divy.isVoidFunction())
    {
      where("ndiv_y(OperatorOnKernel)");
      error("kernel_op_not_handled", k.shortname, "divy");
    }
    return opk;
  }
  error("operator_unexpected"," ny*div_y OperatorOnKernel");
  if (opk.ext_p!=nullptr) opk.in_ext=false;
  return opk;
}

OperatorOnKernel& ncrosscurl_x(OperatorOnKernel& opk)    // nx^curl_x opk
{
  if (opk.xdifOpType()==_id)
  {
    opk.xdifOp_()=findDifferentialOperator(_ncrosscurl_x);
    const Kernel& k=*opk.kernelp();
    if (k.curlx.isVoidFunction())
    {
      where("ncrosscurl_x(OperatorOnKernel)");
      error("kernel_op_not_handled", k.shortname, "curlx");
    }
    return opk;
  }
  error("operator_unexpected"," ncrosscurl_x OperatorOnKernel");
  if (opk.ext_p!=nullptr) opk.in_ext=false;
  return opk;
}

OperatorOnKernel& ncrosscurl_y(OperatorOnKernel& opk)    // ny^curl_y opk
{
  if (opk.xdifOpType()==_id)
  {
    opk.ydifOp_()=findDifferentialOperator(_ncrosscurl_y);
    const Kernel& k=*opk.kernelp();
    if (k.curly.isVoidFunction())
    {
      where("ncrosscurl_y(OperatorOnKernel)");
      error("kernel_op_not_handled", k.shortname, "curly");
    }
    return opk;
  }
  error("operator_unexpected"," ncrosscurl_y OperatorOnKernel");
  if (opk.ext_p!=nullptr) opk.in_ext=false;
  return opk;
}

OperatorOnKernel& ncrossrot_x(OperatorOnKernel& opk)    // nx^curl_x opk
{
  return ncrosscurl_x(opk);
}

OperatorOnKernel& ncrossrot_y(OperatorOnKernel& opk)    // ny^curl_y opk
{
  return ncrosscurl_y(opk);
}

OperatorOnKernel& operator*(UnitaryVector v, OperatorOnKernel& opk) // v*opker
{
  if (v == _nx) return ntimes_x(opk);
  if (v == _ny) return ntimes_y(opk);
  if (v == _nxdotny) return nxdotny_times(opk);
  error("operator_unexpected"," UnitaryVector * OperatorOnKernel");
  return opk;
}

OperatorOnKernel& operator*(OperatorOnKernel& opk,UnitaryVector v) // opker*v
{
  return v*opk;
}

OperatorOnKernel& operator|(UnitaryVector v, OperatorOnKernel& opk) // v|opker
{
  if (v == _nx) return ndot_x(opk);
  if (v == _ny) return ndot_y(opk);
  if (v == _nxcrossny) return nxcrossny_dot(opk);
  if (v == _nycrossnx) return nycrossnx_dot(opk);
  error("operator_unexpected"," UnitaryVector | OperatorOnKernel");
  return opk;
}

OperatorOnKernel& operator|(OperatorOnKernel& opk, UnitaryVector v) // opker|v
{
  return v|opk;
}

OperatorOnKernel& operator^(UnitaryVector v, OperatorOnKernel& opk) // v^opker
{
  if (v == _nx) return ncross_x(opk);
  if (v == _ny) return ncross_y(opk);
  if (v == _nxcrossny) return nxcrossny_cross(opk);
  if (v == _nycrossnx) return nycrossnx_cross(opk);
  error("operator_unexpected"," UnitaryVector | OperatorOnKernel");
  return opk;
}

//conjugate or transpose
OperatorOnKernel& conj(OperatorOnKernel& opk)
{
  opk.conjugate_=!opk.conjugate_;
  return opk;
}

OperatorOnKernel& tran(OperatorOnKernel& opk)
{
  opk.transpose_=!opk.transpose_;
  return opk;
}

OperatorOnKernel& adj(OperatorOnKernel& opk)
{
  opk.transpose_=!opk.transpose_;
  opk.conjugate_=!opk.conjugate_;
  return opk;
}

// set to true or false the temporary conjugate flag
OperatorOnKernel& conj(Kernel& k)
{
  OperatorOnKernel* opk=new OperatorOnKernel(&k,_id,_id,k.valueType(),k.strucType(),k.dims());
  opk->conjugate_=true;
  return *opk;
}

// set to true or false the temporary transpose flag
OperatorOnKernel& tran(Kernel& k)
{
  OperatorOnKernel* opk=new OperatorOnKernel(&k,_id,_id,k.valueType(),k.strucType(),k.dims());
  opk->transpose_=true;
  return *opk;
}

// set to true or false the temporary conjugate/transpose flag
OperatorOnKernel& adj(Kernel& k)
{
  OperatorOnKernel* opk=new OperatorOnKernel(&k,_id,_id,k.valueType(),k.strucType(),k.dims());
  opk->conjugate_=true;
  opk->transpose_=true;
  return *opk;
}

//special cases involving Extension
OperatorOnKernel& operator*(const Extension& e, const Kernel& k)         //! extension of Kernel
{
  OperatorOnKernel* opk=new OperatorOnKernel(&k,_id,_id,k.valueType(),k.strucType(),k.dims());
  opk->ext_p = new Extension(e);
  opk->in_ext=true;
  return *opk;
}

OperatorOnKernel& operator*(const Extension& e, OperatorOnKernel& opk)   //! extension of OperatorOnKernel
{
  if (opk.ext_p!=nullptr)
  {
    where("Extension * OperatorOnKernel");
    error("extension_already_set");
  }
  opk.ext_p = new Extension(e);
  opk.in_ext=true;
  return opk;
}

//alternate syntax
OperatorOnKernel& Extension::operator()(const Kernel& k) const           //! apply Extension to Kernel
{return (*this)*k;}

OperatorOnKernel& Extension::operator()(OperatorOnKernel& opk) const     //! apply Extension to OperatorOnKernel
{return (*this)*opk;}

//comparison
bool operator==(const OperatorOnKernel& opf1, const OperatorOnKernel& opf2)
{
  return (opf1.ker_p==opf2.ker_p && opf1.xdifOp_p==opf2.xdifOp_p &&
          opf1.ydifOp_p==opf2.ydifOp_p && opf1.xydifOp_p==opf2.xydifOp_p);
}

bool operator!=(const OperatorOnKernel& opf1, const OperatorOnKernel& opf2)
{
  return !(opf1==opf2);
}

} //end of namespace xlifepp
