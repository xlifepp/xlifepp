/*
XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LcKernelOperatorOnUnknowns.cpp
  \author E. Lunéville
  \since 19 jul 2018
  \date  19 jul 2018

  \brief Implementation of xlifepp::LcKernelOperatorOnUnknownsclass members and related functions
*/

#include "LcKernelOperatorOnUnknowns.hpp"
#include "utils.h"

namespace xlifepp
{

//constructors/destructor
LcKernelOperatorOnUnknowns::LcKernelOperatorOnUnknowns(const KernelOperatorOnUnknowns& opkuv, const real_t& a, const string_t& na)
{
  push_back(OpkuvValPair(new KernelOperatorOnUnknowns(opkuv),complex_t(a)));
  name=na;
}

LcKernelOperatorOnUnknowns::LcKernelOperatorOnUnknowns(const KernelOperatorOnUnknowns& opkuv, const complex_t& a, const string_t& na)
{
  name=na;
  push_back(OpkuvValPair(new KernelOperatorOnUnknowns(opkuv),a));
}


LcKernelOperatorOnUnknowns::LcKernelOperatorOnUnknowns(const LcKernelOperatorOnUnknowns& lc)  // copy constructor (full copy)
{
  if(lc.size()==0) return;
  copy(lc);
}

LcKernelOperatorOnUnknowns& LcKernelOperatorOnUnknowns::operator=(const LcKernelOperatorOnUnknowns& lc)         //! assignment
{
  if(&lc == this) return *this;
  clear();
  copy(lc);
  return *this;
}

LcKernelOperatorOnUnknowns::~LcKernelOperatorOnUnknowns()
{
  clear();
}

//deallocate KernelOperatorOnUnknowns pointers
void LcKernelOperatorOnUnknowns::clear()
{
  for(it_opkuvval it=begin(); it!=end(); it++)
  {
    if (it->first != nullptr) delete it->first;
  }
  std::vector<OpkuvValPair>::clear();
}

//full copy of KernelOperatorOnUnknowns pointers
void LcKernelOperatorOnUnknowns::copy(const LcKernelOperatorOnUnknowns& lc)
{
  //already cleared !!!
  resize(lc.size());
  cit_opkuvval itlc=lc.begin();
  it_opkuvval it= begin();
  for(; itlc!=lc.end(); itlc++, it++)
  {
    KernelOperatorOnUnknowns* opkuv=new KernelOperatorOnUnknowns(*itlc->first);  //full copy
    *it=OpkuvValPair(opkuv,itlc->second);
  }
  name=lc.name;
}

//utilities

// return ith Unknown/TestFunction involved in LcKernelOperatorOnUnknowns
uvPair LcKernelOperatorOnUnknowns::unknowns() const
{
  if(size()==0) return uvPair(0,0);
  return begin()->first->unknowns();
}

//return coefficient of first term if a single term combination
complex_t LcKernelOperatorOnUnknowns::coefficient() const
{
  complex_t c=0.;
  if(size()==1) c=begin()->second;
  return c;
}

//return vector of coefficients involved in combination
std::vector<complex_t> LcKernelOperatorOnUnknowns::coefficients() const
{
  std::vector<complex_t> cs;
  if(size()==0) return cs;
  cs.resize(size());
  cit_opkuvval it=begin();
  std::vector<complex_t>::iterator itcs=cs.begin();
  for(; it!=end(); it++,itcs++) *itcs = it->second;
  return cs;
}

//return kernel of first term if a single term combination, else 0
const Kernel* LcKernelOperatorOnUnknowns::kernel() const
{
    if(size()==1) return begin()->first->kernel();
    return nullptr;
}

std::vector<const Kernel*> LcKernelOperatorOnUnknowns::kernels() const
{
  std::vector<const Kernel*> ks;
  if(size()==0) return ks;
  ks.resize(size());
  cit_opkuvval it=begin();
  std::vector<const Kernel*>::iterator itks=ks.begin();
  for(; it!=end(); it++,itks++) *itks = it->first->kernel();
  return ks;
}

// insert terms in list
void LcKernelOperatorOnUnknowns::insert(const KernelOperatorOnUnknowns& opkuv)
{
  insert(1.,opkuv);
}

void LcKernelOperatorOnUnknowns::insert(const real_t& a, const KernelOperatorOnUnknowns& opkuv)
{
  if(unknowns()==opkuv.unknowns()) push_back(OpkuvValPair(new KernelOperatorOnUnknowns(opkuv),complex_t(a)));
  else error("not_same_unknowns_in_lc");
}

void LcKernelOperatorOnUnknowns::insert(const complex_t& a, const KernelOperatorOnUnknowns& opkuv)
{
  if(unknowns()==opkuv.unknowns()) push_back(OpkuvValPair(new KernelOperatorOnUnknowns(opkuv),a));
  else error("not_same_unknowns_in_lc");
}

LcKernelOperatorOnUnknowns& LcKernelOperatorOnUnknowns::operator +=(const KernelOperatorOnUnknowns& opkuv)
{
  insert(1.,opkuv);
  return *this;
}

LcKernelOperatorOnUnknowns& LcKernelOperatorOnUnknowns::operator +=(const LcKernelOperatorOnUnknowns& lc)
{
  if(this==&lc)
  {
    (*this)*=2;
    return *this;
  }
  cit_opkuvval itlc=lc.begin();
  for(; itlc!=lc.end(); itlc++) insert(itlc->second, *itlc->first);
  return *this;
}

LcKernelOperatorOnUnknowns& LcKernelOperatorOnUnknowns::operator -=(const KernelOperatorOnUnknowns& opkuv)
{
  insert(-1.,opkuv);
  return *this;
}


LcKernelOperatorOnUnknowns& LcKernelOperatorOnUnknowns::operator -=(const LcKernelOperatorOnUnknowns& lc)
{
  if(this==&lc)
  {
    clear();
    return *this;
  }
  cit_opkuvval itlc=lc.begin();
  for(; itlc!=lc.end(); itlc++) insert(-itlc->second,*itlc->first);
  return *this;
}

LcKernelOperatorOnUnknowns& LcKernelOperatorOnUnknowns::operator *=(const real_t& a)
{
  it_opkuvval it=begin();
  for(; it!=end(); it++) it->second*=a;
  return *this;
}

LcKernelOperatorOnUnknowns& LcKernelOperatorOnUnknowns::operator *=(const complex_t& a)
{
  it_opkuvval it=begin();
  for(; it!=end(); it++) it->second*=a;
  return *this;
}

LcKernelOperatorOnUnknowns& LcKernelOperatorOnUnknowns::operator /=(const real_t& a)
{
   (*this) *= (1./a);
   return *this;
}

LcKernelOperatorOnUnknowns& LcKernelOperatorOnUnknowns::operator /=(const complex_t& a)
{
  (*this) *= (1./a);
   return *this;
}


//print utility

string_t LcKernelOperatorOnUnknowns::asString() const
{
  std::stringstream os;
  cit_opkuvval it=begin();
  if(it==end())
  {
    os<<" void LcKernelOperatorOnUnknowns";
    return os.str();
  }
  complex_t a=it->second;
  if(a.imag()!=0) os<<a<<" * ";
  else
  {
    real_t ra=a.real();
    if(std::abs(ra)== 1)
    {
      if(ra==-1) os<<" - ";
    }
    else
    {
      if(ra>0) os<<ra<<" * ";
      else os<<" - "<<std::abs(ra)<<" * ";
    }
  }
  os<<it->first->asString();
  it++;
  for(; it!=end(); it++)
  {
    complex_t a=it->second;
    if(a.imag()!=0) os<<" + "<<a<<" * ";
    else
    {
      real_t ra=a.real();
      if(std::abs(ra)== 1)
      {
        if(ra==1) os<<" + ";
        else os<<" - ";
      }
      else
      {
        if(ra>0) os<<" + "<<ra<<" * ";
        else os<<" - "<<std::abs(ra)<<" * ";
      }
    }
    os<<it->first->asString();
  }
  return os.str();
}

void LcKernelOperatorOnUnknowns::print(std::ostream& os) const
{
    if(name!="") os<<name<<": ";
    os<<asString();
}

std::ostream& operator<<(std::ostream& os, const LcKernelOperatorOnUnknowns& lc)
{
  lc.print(os);
  return os;
}

//extern LcTermFunction functions

LcKernelOperatorOnUnknowns operator +(const LcKernelOperatorOnUnknowns& lc)  //unary +
{
  return LcKernelOperatorOnUnknowns(lc);
}

LcKernelOperatorOnUnknowns operator -(const LcKernelOperatorOnUnknowns& lc)  //unary -
{
  LcKernelOperatorOnUnknowns lcr(lc);
  return lcr*=-1;
}

LcKernelOperatorOnUnknowns operator +(const LcKernelOperatorOnUnknowns& lc, const KernelOperatorOnUnknowns& opkuv)
{
  LcKernelOperatorOnUnknowns lcr(lc);
  return lcr+=opkuv;
}

LcKernelOperatorOnUnknowns operator -(const LcKernelOperatorOnUnknowns& lc, const KernelOperatorOnUnknowns& opkuv)
{
  LcKernelOperatorOnUnknowns lcr(lc);
  return lcr-=opkuv;
}
LcKernelOperatorOnUnknowns operator +(const KernelOperatorOnUnknowns& opkuv, const LcKernelOperatorOnUnknowns& lc)
{
  LcKernelOperatorOnUnknowns lcr(lc);
  return lcr+=opkuv;
}

LcKernelOperatorOnUnknowns operator -(const KernelOperatorOnUnknowns&opkuv , const LcKernelOperatorOnUnknowns& lc)
{
  LcKernelOperatorOnUnknowns lcr(-lc);
  return lcr+=opkuv;
}

LcKernelOperatorOnUnknowns operator +(const LcKernelOperatorOnUnknowns& lc1, const LcKernelOperatorOnUnknowns& lc2)
{
  LcKernelOperatorOnUnknowns lcr(lc1);
  return lcr+=lc2;
}

LcKernelOperatorOnUnknowns operator -(const LcKernelOperatorOnUnknowns& lc1, const LcKernelOperatorOnUnknowns& lc2)
{
  LcKernelOperatorOnUnknowns lcr(lc1);
  return lcr-=lc2;
}

LcKernelOperatorOnUnknowns operator *(const LcKernelOperatorOnUnknowns& lc, const real_t& a)
{
  LcKernelOperatorOnUnknowns lcr(lc);
  return lcr*=a;
}

LcKernelOperatorOnUnknowns operator *(const LcKernelOperatorOnUnknowns& lc, const complex_t& a)
{
  LcKernelOperatorOnUnknowns lcr(lc);
  return lcr*=a;
}

LcKernelOperatorOnUnknowns operator *(const real_t& a, const LcKernelOperatorOnUnknowns& lc)
{
  LcKernelOperatorOnUnknowns lcr(lc);
  return lcr*=a;
}
LcKernelOperatorOnUnknowns operator *(const complex_t& a, const LcKernelOperatorOnUnknowns& lc)
{
  LcKernelOperatorOnUnknowns lcr(lc);
  return lcr*=a;
}

LcKernelOperatorOnUnknowns operator /(const LcKernelOperatorOnUnknowns& lc, const real_t& a)
{
  LcKernelOperatorOnUnknowns lcr(lc);
  return lcr/=a;
}
LcKernelOperatorOnUnknowns operator /(const LcKernelOperatorOnUnknowns& lc, const complex_t& a)
{
  LcKernelOperatorOnUnknowns lcr(lc);
  return lcr/=a;
}

LcKernelOperatorOnUnknowns operator +(const KernelOperatorOnUnknowns& opkuv1, const KernelOperatorOnUnknowns& opkuv2)
{
  LcKernelOperatorOnUnknowns lcr(opkuv1);
  return lcr+=opkuv2;
}

LcKernelOperatorOnUnknowns operator -(const KernelOperatorOnUnknowns& opkuv1, const KernelOperatorOnUnknowns& opkuv2)
{
  LcKernelOperatorOnUnknowns lcr(opkuv1);
  return lcr-=opkuv2;
}

} // end of namespace xlifepp

