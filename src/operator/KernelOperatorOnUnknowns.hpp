/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file KernelOperatorOnUnknowns.hpp
  \author E. Lunéville
  \since 02 mar 2012
  \date  10 nov 2015

  \brief Definition of the xlifepp::KernelOperatorOnUnknowns class

  Class xlifepp::KernelOperatorOnUnknowns deals with a pair of xlifepp::OperatorOnUnknown related by an xlifepp::AlgebraicOperator
  It is useful to store syntax  opu(y) aop opker(x,y) aop opv(x)  occuring in bilinear forms
 */

#ifndef KERNEL_OPERATOR_ON_UNKNOWNS_HPP
#define KERNEL_OPERATOR_ON_UNKNOWNS_HPP

#include "config.h"
#include "OperatorOnUnknown.hpp"
#include "OperatorOnKernel.hpp"

namespace xlifepp
{
  typedef std::pair<const Unknown*, const Unknown*> uvPair;

/*!
  \class KernelOperatorOnUnknowns
  generalization to kernel form
  describes a pair of OperatorOnUnknows linked by a Kernel operation:
                 (op_u  aop_u op_ker) aop_v op_v
  respect the order of operators, operator u at left and operator v at right
*/
class KernelOperatorOnUnknowns
{
  protected:
    OperatorOnUnknown opu_;  //!< left operator on unknown
    OperatorOnUnknown opv_;  //!< right operator on unknown (test function)
    AlgebraicOperator aopu_; //!< algebraic operation (*,|,%,^) between u and kernel
    AlgebraicOperator aopv_; //!< algebraic operation (*,|,%,^) between kernel and v
    OperatorOnKernel opker_; //!< Kernel operator
    bool rightPriority;      //!< change the default priority to: op_u aop_u (op_ker aop_v op_v)

  public:
    KernelOperatorOnUnknowns(){} //!< default constructor
    KernelOperatorOnUnknowns(const OperatorOnUnknown&, AlgebraicOperator, const OperatorOnKernel&,
                             AlgebraicOperator, const OperatorOnUnknown&, bool right=false); //!< basic constructor

    const OperatorOnUnknown& opu() const {return opu_;} //!< returns left operator on unknown (const)
    const OperatorOnUnknown& opv() const {return opv_;} //!< reutrns right operator on unknown (const)
    OperatorOnUnknown& opu() {return opu_;}             //!< returns left operator on unknown (non const)
    OperatorOnUnknown& opv() {return opv_;}             //!< returns right operator on unknown (non const)
    AlgebraicOperator algopu() const {return aopu_;}    //!< returns algebraic operation between u and kernel (const)
    AlgebraicOperator algopv() const {return aopv_;}    //!< returns algebraic operation between kernel and v (const)
    AlgebraicOperator& algopu() {return aopu_;}         //!< returns algebraic operation between u and kernel (non const)
    AlgebraicOperator& algopv() {return aopv_;}         //!< returns algebraic operation between kernel and v (non const)
    bool priorityIsRight() const {return rightPriority;}//!< returns true if right priority in operation: opu aop (opker aop opv)
    const Kernel* kernel() const {return opker_.kernelp();}
    const OperatorOnKernel& opker() const {return opker_;} //!< returns kernel operator
    bool isKernelType() const {return true;}               //!< returns true if there is a kernel
    ValueType valueType() const;                           //!< return value type (_real or _complex)
    SingularityType singularType() const                    //! returns type of singularity (_notsingular, _r, _logr,_loglogr)
    {return opker_.singularType();}
    real_t singularOrder() const                           //! returns singularity order (0 if not singular)
    {return opker_.singularOrder();}
    const string_t& kernelName() const                     //! returns kernel shortname
    {return opker_.kernelName();}
    bool xnormalRequired() const                           //! returns true if normal required on u
    {return opker_.xnormalRequired() || opv_.normalRequired();}
    bool ynormalRequired() const                           //! returns true if normal required on v
    {return opker_.ynormalRequired() || opu_.normalRequired();}
    dimen_t diffOrder_u() const                            //! returns order of differential operator on u
    {return opu_.diffOrder();}
    dimen_t diffOrder_v() const                            //! returns order of differential operator on v
    {return opv_.diffOrder();}
    dimen_t diffOrder() const                              //! returns global order of differential operator
    {return std::max(opu_.diffOrder(), opv_.diffOrder());}
    uvPair unknowns() const                                //! returns unknown pointers u,v
    {return std::make_pair(opu_.unknown(),opv_.unknown());}
    void setUnknowns(const Unknown&, const Unknown&);      //!< set (change) the unknowns

    //evaluation functions
    template<typename K>
    void eval(const Point&, const Point&, const ShapeValues&, const ShapeValues&, K,
              Matrix<K>&, const Vector<real_t>* nx=nullptr, const Vector<real_t>* ny=nullptr) const; //!< evaluate at a couple X,Y from shapevalues
    template<typename K>
    void evalF(const Point&, const Point&, const ShapeValues&, const ShapeValues&, K,
               const Vector<real_t>*, const Vector<real_t>*,
               dimen_t, dimen_t, bool, bool, bool, bool, bool, ExtensionData* extdata,
               Vector<K>&, Vector<K>&, Vector<K>&,
               Matrix<K>& res) const;                            //!< evaluate at a couple X,Y from shapevalues (faster)

    //in/out functions
    void print(std::ostream&) const;               //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());}
    string_t asString() const;                     //!< return as symbolic string
};

std::ostream& operator<<(std::ostream&, const KernelOperatorOnUnknowns&);   //!< outputs OperatorOnUnknown attributes

// external functions

//kernel and unknown
KernelOperatorOnUnknowns operator*(const OperatorOnUnknown&, const OperatorOnKernel&); //!< opu * opker
KernelOperatorOnUnknowns operator|(const OperatorOnUnknown&, const OperatorOnKernel&); //!< opu | opker
KernelOperatorOnUnknowns operator^(const OperatorOnUnknown&, const OperatorOnKernel&); //!< opu ^ opker
KernelOperatorOnUnknowns operator%(const OperatorOnUnknown&, const OperatorOnKernel&); //!< opu % opker

KernelOperatorOnUnknowns operator*(const OperatorOnKernel&, const OperatorOnUnknown&); //!< opker * opv
KernelOperatorOnUnknowns operator|(const OperatorOnKernel&, const OperatorOnUnknown&); //!< opker | opv
KernelOperatorOnUnknowns operator^(const OperatorOnKernel&, const OperatorOnUnknown&); //!< opker ^ opv
KernelOperatorOnUnknowns operator%(const OperatorOnKernel&, const OperatorOnUnknown&); //!< opker % opv

KernelOperatorOnUnknowns operator*(const OperatorOnUnknown&, const Kernel&);           //!< opu * ker
KernelOperatorOnUnknowns operator|(const OperatorOnUnknown&, const Kernel&);           //!< opu | ker
KernelOperatorOnUnknowns operator^(const OperatorOnUnknown&, const Kernel&);           //!< opu ^ ker
KernelOperatorOnUnknowns operator%(const OperatorOnUnknown&, const Kernel&);           //!< opu % ker

KernelOperatorOnUnknowns operator*(const Kernel&, const OperatorOnUnknown&);           //!< ker * opv
KernelOperatorOnUnknowns operator|(const Kernel&, const OperatorOnUnknown&);           //!< ker | opv
KernelOperatorOnUnknowns operator^(const Kernel&, const OperatorOnUnknown&);           //!< ker ^ opv
KernelOperatorOnUnknowns operator%(const Kernel&, const OperatorOnUnknown&);           //!< ker % opv

KernelOperatorOnUnknowns operator*(const Unknown&, const Kernel&);         //!< u * ker
KernelOperatorOnUnknowns operator|(const Unknown&, const Kernel&);         //!< u | ker
KernelOperatorOnUnknowns operator^(const Unknown&, const Kernel&);         //!< u ^ ker
KernelOperatorOnUnknowns operator%(const Unknown&, const Kernel&);         //!< u % ker

KernelOperatorOnUnknowns operator*(const Kernel&, const Unknown&);         //!< ker * v
KernelOperatorOnUnknowns operator|(const Kernel&, const Unknown&);         //!< ker | v
KernelOperatorOnUnknowns operator^(const Kernel&, const Unknown&);         //!< ker ^ v
KernelOperatorOnUnknowns operator%(const Kernel&, const Unknown&);         //!< ker % v

KernelOperatorOnUnknowns operator*(const OperatorOnUnknown&, const KernelOperatorOnUnknowns&); //!< opu * opker
KernelOperatorOnUnknowns operator|(const OperatorOnUnknown&, const KernelOperatorOnUnknowns&); //!< opu | opker
KernelOperatorOnUnknowns operator^(const OperatorOnUnknown&, const KernelOperatorOnUnknowns&); //!< opu ^ opker
KernelOperatorOnUnknowns operator%(const OperatorOnUnknown&, const KernelOperatorOnUnknowns&); //!< opu % opker

KernelOperatorOnUnknowns operator*(const KernelOperatorOnUnknowns&, const OperatorOnUnknown&); //!< opker * opv
KernelOperatorOnUnknowns operator|(const KernelOperatorOnUnknowns&, const OperatorOnUnknown&); //!< opker | opv
KernelOperatorOnUnknowns operator^(const KernelOperatorOnUnknowns&, const OperatorOnUnknown&); //!< opker ^ opv
KernelOperatorOnUnknowns operator%(const KernelOperatorOnUnknowns&, const OperatorOnUnknown&); //!< opker % opv

KernelOperatorOnUnknowns operator*(const Unknown&, const KernelOperatorOnUnknowns&); //!< u * opker
KernelOperatorOnUnknowns operator|(const Unknown&, const KernelOperatorOnUnknowns&); //!< u | opker
KernelOperatorOnUnknowns operator^(const Unknown&, const KernelOperatorOnUnknowns&); //!< u ^ opker
KernelOperatorOnUnknowns operator%(const Unknown&, const KernelOperatorOnUnknowns&); //!< u % opker

KernelOperatorOnUnknowns operator*(const KernelOperatorOnUnknowns&, const Unknown&); //!< opker * v
KernelOperatorOnUnknowns operator|(const KernelOperatorOnUnknowns&, const Unknown&); //!< opker | v
KernelOperatorOnUnknowns operator^(const KernelOperatorOnUnknowns&, const Unknown&); //!< opker ^ v
KernelOperatorOnUnknowns operator%(const KernelOperatorOnUnknowns&, const Unknown&); //!< opker % v

//explicit kernel and unknown
template <typename T>
KernelOperatorOnUnknowns operator*(const Unknown& un, T(fun)(const Point&, const Point&, Parameters&)) //! u * function(Point,Parameters)
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return un * f;
}
template <typename T>
KernelOperatorOnUnknowns operator*(T(fun)(const Point&, const Point&, Parameters&), const Unknown& un) //! function(Point,Parameters) * u
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return f * un;
}
template <typename T>
KernelOperatorOnUnknowns operator|(const Unknown& un, T(fun)(const Point&, const Point&, Parameters&)) //! u | function(Point,Parameters)
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return un | f;
}
template <typename T>
KernelOperatorOnUnknowns operator|(T(fun)(const Point&, const Point&, Parameters&), const Unknown& un) //! function(Point,Parameters) | u
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return f | un;
}
template <typename T>
KernelOperatorOnUnknowns operator^(const Unknown& un, T(fun)(const Point&, const Point&, Parameters&)) //! u ^ function(Point,Parameters)
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return un ^ f;
}
template <typename T>
KernelOperatorOnUnknowns operator^(T(fun)(const Point&, const Point&, Parameters&), const Unknown& un) //! function(Point,Parameters) ^ u
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return f ^ un;
}
template <typename T>
KernelOperatorOnUnknowns operator%(const Unknown& un, T(fun)(const Point&, const Point&, Parameters&)) //! u % function(Point,Parameters)
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return un % f;
}
template <typename T>
KernelOperatorOnUnknowns operator%(T(fun)(const Point&, const Point&, Parameters&), const Unknown& un) //! function(Point,Parameters) % u
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return f % un;
}

// same for vector function (Vector<Point> as argument)
template <typename T>
KernelOperatorOnUnknowns operator*(const Unknown& un, T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&)) //! u * function(Vector<Point>,Parameters)
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return un * f;
}
template <typename T>
KernelOperatorOnUnknowns operator*(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), const Unknown& un) //! function(Vector<Point>,Parameters) * u
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return f * un;
}
template <typename T>
KernelOperatorOnUnknowns operator|(const Unknown& un, T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&)) //! u | function(Vector<Point>,Parameters)
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return un | f;
}
template <typename T>
KernelOperatorOnUnknowns operator|(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), const Unknown& un) //! function(Vector<Point>,Parameters) | u
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return f | un;
}
template <typename T>
KernelOperatorOnUnknowns operator^(const Unknown& un, T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&)) //! u ^ function(Vector<Point>,Parameters)
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return un ^ f;
}
template <typename T>
KernelOperatorOnUnknowns operator^(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), const Unknown& un) //! function(Vector<Point>,Parameters) ^ u
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return f ^ un;
}
template <typename T>
KernelOperatorOnUnknowns operator%(const Unknown& un, T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&)) //! u % function(Vector<Point>,Parameters)
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return un % f;
}
template <typename T>
KernelOperatorOnUnknowns operator%(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), const Unknown& un) //! function(Vector<Point>,Parameters) % u
{
  Kernel& f = *(new Kernel(*fun)); // create a Kernel object on heap memory
  return f % un;
}


OperatorOnUnknown toOperatorOnUnknown(const KernelOperatorOnUnknowns&); //!< move partial KernelOperatorOnUnknowns to OperatorOnUnknown


/* evaluate KernelOperatorOnUnknowns at couple X,Y from shapevalues at point X and Y
        res(i,j) += val_opu[j](Y) aopu K(X,Y) aopv val_opv[i](X) * alpha
          (be cautious: row unknown v, column unknown: u)

   x,y: point where to evaluate
*/
template<typename K>
void KernelOperatorOnUnknowns::eval(const Point& x, const Point& y, const ShapeValues& Syu, const ShapeValues& Sxv, K alpha,
                                    Matrix<K>& res, const Vector<real_t>* nx, const Vector<real_t>* ny) const
{
  //evaluate operator on unknowns
  dimen_t dimfu = opu_.unknown()->space()->dimFun(), dimfv = opv_.unknown()->space()->dimFun() ;   //dimension of u-v shape functions
  dimen_t du, mu, dv, mv; //block size of val_opu et val_opv and kernel
  Vector<K> val_opu, val_opv;
  if(opu_.hasFunction()) opu_.eval(y, Syu.w, Syu.dw, Syu.d2w, dimfu, val_opu, du, mu, ny);
  else                   opu_.eval(Syu.w, Syu.dw, Syu.d2w, dimfu, val_opu, du, mu, ny);
  if(opv_.hasFunction()) opv_.eval(x, Sxv.w, Sxv.dw, Sxv.d2w, dimfv, val_opv, dv, mv, nx);
  else                   opv_.eval(Sxv.w, Sxv.dw, Sxv.d2w, dimfv, val_opv, dv, mv, nx);

  //evaluate kernel part
  Vector<K> val_k(1,K(0));
  if(opker_.strucType()==_scalar)  opker_.eval(x,y,val_k[0],nx,ny);
  else opker_.eval(x,y,val_k,nx,ny);

  //tensor operation: res(i1,i2)=val_opu[i2] aopu valk aopv val_opv[i1]
  // only scalar kernel for the moment
  if(val_k.size()==1)  //scalar Kernel
    {
      if(du!=dv)
        {
          where("KernelOperatorOnUnknowns::eval(...)");
          error("bad_size", dv, du);
        }
      val_opu *= val_k[0];
      typename Matrix<K>::iterator itr= res.begin();
      number_t nbr=res.numberOfRows(), nbc=res.numberOfColumns();
      if(du==1)
        {
          for(number_t i1=0; i1<nbr; i1++)
            for(number_t i2=0; i2<nbc; i2++, itr++)
              *itr += val_opu[i2] * val_opv[i1] * alpha;
          return;
        }
      if(aopv_==_innerProduct) //inner product in any case
        {
          for(number_t i1=0; i1<nbr; i1++)
            {
              number_t id1=i1*du;
              for(number_t i2=0; i2<nbc; i2++, itr++)
                {
                  number_t id2=i2*du;
                  for(number_t j=0; j<du; j++)
                    *itr += val_opu[id2+j] * val_opv[id1+j] * alpha;
                }
            }
          return;
        }

      where("KernelOperatorOnUnknowns::eval(...)");
      error("not_handled","KernelOnOperators::eval(...) scalar");
    }
  else
    {
      where("KernelOperatorOnUnknowns::eval(...)");
      error("scalar_only");
    }

}
//===================================  fast version =======================================================
/*evaluate KernelOperatorOnUnknowns at couple X,Y from shapevalues at point X and Y
  res(i,j) += val_opu[j](y) aopu K(x,y) aopv val_opv[i](x) * alpha (be cautious: row unknown v, column unknown: u)
    x, y: couple of points where to evaluate the kernel
    Syu, Sxv: shape values for u/v
    alpha: coefficient involved in
    nx, ny: pointer to normal vectors
    dimf_u, dimf_v: dimension of shape functions (u and v)
    opisid_u, opisid_v: true if operator on u/v is identity
    hasf_u, hasf_v: true if operator on u/v requires to evaluate a function
    scalar_k: true if scalar kernel
    extdata: pointer to extension data (no extension if 0)
    val_opu, val_opv: vector to store temporary value of operator on u/v, has to be sized outside
    val_k: vector to store temporary value of kernel, has to be sized outside
*/

template<typename K>
void KernelOperatorOnUnknowns::evalF(const Point& x, const Point& y, const ShapeValues& Syu, const ShapeValues& Sxv, K alpha,
                                     const Vector<real_t>* nx, const Vector<real_t>* ny,
                                     dimen_t dimf_u, dimen_t dimf_v, bool opisid_u, bool opisid_v, bool hasf_u, bool hasf_v, bool scalar_k,
                                     ExtensionData* extdata, Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_k,
                                     Matrix<K>& res) const
{
  number_t nbr=res.numberOfRows(), nbc=res.numberOfColumns();
  bool scalar_uv = (dimf_u==1 && dimf_v==1);

  if (opisid_u && !hasf_u && opisid_v && !hasf_v && scalar_k && scalar_uv) //shortcut u*opker*v (scalar and no function)
  {
    opker_.eval(x, y, val_k, nx, ny, 0, extdata);
    K& vk = val_k[0];
    vk*=alpha;
    typename Matrix<K>::iterator itr= res.begin();
    typename std::vector<real_t>::const_iterator itu, itv=Sxv.w.begin();
    for (number_t i1=0; i1<nbr; i1++, ++itv)
    {
      itu=Syu.w.begin();
      for (number_t i2=0; i2<nbc; i2++, ++itu, ++itr)
        *itr += *itv * *itu * vk;
    }
    return;
  }

  //evaluate operator on unknowns
  dimen_t du=dimf_u, mu, dv=dimf_v, mv; //block size of val_opu et val_opv and kernel
  if (hasf_u) opu_.eval(y, Syu.w, Syu.dw, Syu.d2w, dimf_u, val_opu, du, mu, ny);
  else        opu_.eval(Syu.w, Syu.dw, Syu.d2w, dimf_u, val_opu, du, mu, ny);
  if (hasf_v) opv_.eval(x, Sxv.w, Sxv.dw, Sxv.d2w, dimf_v, val_opv, dv, mv, nx);
  else        opv_.eval(Sxv.w, Sxv.dw, Sxv.d2w, dimf_v, val_opv, dv, mv, nx);

  //evaluate kernel part
  dimen_t m=1,n=1;
  const Kernel* kerp=opker_.kernelp();
  if (kerp!=nullptr) opker_.eval(x, y, val_k, nx, ny, 0, extdata);   //note: dims not used in the following

//  thePrintStream<<"    x="<<x<<" y="<<y<<" val_opk="<<val_k<<eol<<std::flush;
//  thePrintStream<<"    Syu.w="<<Syu.w<<eol<<"    Sxv.w="<<Syu.w<<eol<<std::flush;
//  thePrintStream<<"    Syu.dw="<<Syu.dw<<eol<<"    Sxv.dw="<<Sxv.dw<<eol<<std::flush;
//  thePrintStream<<"    val_opu="<<val_opu<<eol<<"    val_opv="<<val_opv<<eol<<std::flush;

  /*tensor operation: res(i1,i2)=val_opu[i2] aopu valk aopv val_opv[i1]
       opu = U1_11,  U1_12, ...,  U1_1nu, ...  U1_mu1,  U1_mu2, ...,  U1_munu     shape function 1
             U2_11,  U2_12, ...,  U2_1nu, ...  U2_mu1,  U2_mu2, ...,  U2_munu     shape function 2
             ...
             Usu_11, Usu_12, ..., Usu_1nu, ... Usu_mu1, Usu_mu2, ..., Usu_munu    shape function su
       opk   K11 K12 ...K1n,... Km1, Km2, ...Kmn
       opv = V1_11,  V1_12, ...,  V1_1nv, ...  V1_mv1,  V1_mv2, ...,  V2_mvnv     shape fvnction 1
             V2_11,  V2_12, ...,  V21_nv, ...  V2_mv1,  V2_mv2, ...,  V2_mvnv     shape fvnction 2
             ...
             Vsv_11, Vsv_12, ..., Vsv_1nv, ... Vsv_mv1, Vsv_mv2, ..., Vsv_mvnv    shape fvnction sv

      tensor operation opu aop opk aop opv should be a sv*su scalar matrix

  */
  if (opker_.strucType()==_scalar)  //operator on kernel is scalar
  {
    if (du!=dv)
    {
      where("KernelOperatorOnUnknowns::evalF(...)");
      error("bad_size",dv,du);
    }
    if (kerp!=nullptr) val_opu *= val_k[0];
    typename Vector<K>::iterator itub=val_opu.begin(), itue=val_opu.end(), itu;
    typename Vector<K>::iterator itvb=val_opv.begin(), itve=val_opv.end(), itv;
    typename Matrix<K>::iterator itr= res.begin();
    if (du==1)
    {
      for (itv=itvb; itv!=itve; ++itv)
      {
        K t = *itv * alpha;
        for(itu=itub; itu!=itue; ++itu, ++itr) *itr += *itu * t;
      }
      return;
    }
    if ((aopu_==_product && aopv_==_innerProduct) || (aopu_==_innerProduct && aopv_==_product))   //inner product
    {
      for (number_t i1=0; i1<nbr; i1++)
      {
        number_t id1=i1*du;
        for (number_t i2=0; i2<nbc; i2++, itr++)
        {
          number_t id2=i2*du;
          for (number_t j=0; j<du; j++)
            *itr += val_opu[id2+j] * val_opv[id1+j] * alpha;
        }
      }
      return;
    }
    where("KernelOperatorOnUnknowns::evalF(...)");
    error("not_handled","KernelOnOperators::evalF(...) scalar");
  }
  else //non scalar kernel
  {
    m=opker_.dimsRes_.first; n=opker_.dimsRes_.second;
    dimen_t nu=du/mu, nbsu=val_opu.size()/du; //number of u-shape values
    dimen_t nv=dv/mv, nbsv=val_opv.size()/dv; //number of v-shape values
    if (mu>1 || mv>1)
    {
      where("KernelOperatorOnUnknowns::evalF(...)");
      error("vector_only");
    }

    //if(aopu_!=_innerProduct || opker_.strucType()== _vector) // do opu_ (nu x mu) aop_u opker_ (m x n), exclude cases: aopu | matrix or aopu | scalar
    if (!rightPriority) // do opu_ (nu x mu) aop_u opker_ (m x n), exclude cases: aopu | matrix or aopu | scalar
    {
      Vector<K> opuopk;
      dimen_t muk,nuk;
      switch (aopu_)
      {
        case _product:
        {
          if (du==1) // mu=nu=1, scalar opu
          {
            opuopk.resize(nbsu*m*n,K(0));
            muk=m; nuk=n;
            typename Vector<K>::iterator itkb=val_k.begin(),itke=val_k.end(), itk;
            typename Vector<K>::iterator ituk=opuopk.begin();
            typename Vector<K>::iterator itub=val_opu.begin(), itue = val_opu.end(), itu;
            for (itu=itub; itu!=itue; ++itu)
            {
              for (itk=itkb; itk!=itke; ++itk, ++ituk) *ituk = *itu * *itk;
            }
          }
          else if (nu==m) //matrix product (1 x nu) * (m x n)
          {
            muk=n; nuk=mu;
            opuopk.resize(nbsu*muk*nuk,K(0));
            typename Vector<K>::iterator itkb=val_k.begin(), itk;
            typename Vector<K>::iterator itub=val_opu.begin(), itu;
            typename Vector<K>::iterator ituk=opuopk.begin();
            for (number_t s=0; s<nbsu; ++s, itub+=du)
            {
              for (number_t i=0; i<mu; ++i)
                for (number_t j=0; j<n; ++j, ++ituk)
                {
                  itu = itub+i*nu;
                  itk = itkb+j;
                  for(number_t k=0; k<nu; ++k, ++itu, itk+=n) *ituk+=*itu* *itk;
                }
            }
          }
          else
          {
            where("KernelOperatorOnUnknowns::evalF(...)");
            error("aop_inconsistent_operators","*");
          }
          break;
        }
        case _innerProduct:
        {
          if (nu!=m || mu!=1 || n!=1)
          {
            where("KernelOperatorOnUnknowns::evalF(...)");
            error("aop_inconsistent_operators","|");
          }
          opuopk.resize(nbsu, K(0));
          muk=1; nuk=1;
          typename Vector<K>::iterator itkb=val_k.begin(), itk;
          typename Vector<K>::iterator itu=val_opu.begin();
          typename Vector<K>::iterator ituk=opuopk.begin();
          for (number_t s=0; s<nbsu; ++s, ++ituk)
          {
            itk=itkb;
            for (number_t i=0; i<nu; ++i, ++itu,++itk) *ituk+=*itu * *itk;
          }
          break;
        }
        case _crossProduct:
        {
          if (nu!=m || mu!=1 || n!=1)
          {
            where("KernelOperatorOnUnknowns::evalF(...)");
            error("aop_inconsistent_operators","^");
          }
          if (m==3)   //3d cross product
          {
            opuopk.resize(3*nbsu, K(0));
            muk=3; nuk=1;
            typename Vector<K>::iterator itu=val_opu.begin();
            typename Vector<K>::iterator ituk=opuopk.begin();
            K v1=val_k[0], v2=val_k[1],v3=val_k[2];
            for (number_t s=0; s<nbsu; ++s, itu+=3, ituk+=3)
            {
               K u1=*itu,  u2=*(itu+1), u3=*(itu+2);
               * ituk    = u2*v3 - u3*v3;
               *(ituk+1) = u3*v1 - u1*v3;
               *(ituk+2) = u1*v2 - u2*v1;
            }
          }
          else
          {
            where("KernelOperatorOnUnknowns::evalF(...)");
            error("3D_only","opu ^ opk");
          }
          break;
        }
        default:
        {
          where("KernelOperatorOnUnknowns::evalF(...)");
          error("aop_unexpected", words("algop",aopu_));
        }
      }
      //thePrintStream<<"opuopk="<<opuopk<<eol<<std::flush;
      //opuopk (muk x nuk) aop_v opv_ (nv x mv)
      switch (aopv_) // as the final result is scalar, only product and inner product are handled
      {
        case _product: // only scalar * scalar
        {
          if (dv==1 && mu*nuk==1)  // scalar opv, scalar opuopk
          {
            typename Vector<K>::iterator itukb=opuopk.begin(), ituke=opuopk.end(), ituk;
            typename Vector<K>::iterator itvb=val_opv.begin(), itve=val_opv.end(), itv;
            typename Vector<K>::iterator itr=res.begin();
            for (itv = itvb; itv != itve; ++itv)
            {
              K t = *itv *alpha;
              for (ituk = itukb; ituk != ituke; ++ituk, ++itr)  *itr += *ituk * t;
            }
          }
          else
          {
            where("KernelOperatorOnUnknowns::evalF(...)");
            error("aop_inconsistent_operators","*");
          }
          break;
        }
        case _innerProduct: // (muk x 1) | (nv x 1)
        {
          if (muk!=nv)
          {
            where("KernelOperatorOnUnknowns::evalF(...)");
            error("aop_inconsistent_operators", "|");
          }
          typename Vector<K>::iterator itkb=opuopk.begin(), itk;
          typename Vector<K>::iterator itvb=val_opv.begin(), itv;
          typename Vector<K>::iterator itr=res.begin();
          for (number_t sv=0; sv<nbsv; ++sv, itvb+=nv)
          {
            itk = itkb;
            for (number_t su=0; su<nbsu; ++su, ++itr)
            {
              itv = itvb;
              for (number_t i=0; i<nv; ++i, ++itk, ++itv) *itr+= *itk * *itv *alpha;
            }
          }
          break;
        }
        default:
        {
          where("KernelOperatorOnUnknowns::evalF(...)");
          error("aop_unexpected", words("algop",aopv_));
        }
      }
    }
    else // do first opker_ (m x n) aopv opv_ (nv x mv)
    {
      Vector<K> opkopv;
      dimen_t mkv,nkv;
      switch (aopv_)
      {
        case _product:
        {
          if (dv==1) // mv=nv=1, scalar opv
          {
            opkopv.resize(nbsv*m*n,K(0));
            mkv=m; nkv=n;
            typename Vector<K>::iterator itkb=val_k.begin(),itk;
            typename Vector<K>::iterator itvk=opkopv.begin();
            typename Vector<K>::iterator itv=val_opv.begin();
            for (number_t s=0; s<nbsv; ++s, ++itv)
            {
              itk=itkb;
              for (number_t k=0; k<m*n; ++k, ++itk,++itvk) *itvk= *itk * *itv;
            }
          }
          else if (n==nv) //matrix product opk(m x n) * opv(nv x mv)
          {
            opkopv.resize(nbsv*m*mv,K(0));
            mkv=m; nkv=mv;
            typename Vector<K>::iterator itkb=val_k.begin(), itk;
            typename Vector<K>::iterator itvb=val_opv.begin(), itv;
            typename Vector<K>::iterator itkv=opkopv.begin();
            for (number_t s=0; s<nbsv; ++s, itvb+=dv)
            {
              for (number_t i=0; i<m; ++i)
                for (number_t j=0; j<mv; ++j, ++itkv)
                {
                  itk = itkb+i*n;
                  itv = itvb+j;
                  for (number_t k=0; k<n; ++k, ++itv, ++itk) *itkv+=*itk* *itv;
                }
            }
          }
          else
          {
            where("KernelOperatorOnUnknowns::evalF(...)");
            error("aop_inconsistent_operators", "*");
          }
          break;
        }
        case _innerProduct: //inner product opk(m x 1) | opv(nv x 1)
        {
          if (nv!=m || mv!=1 || n!=1)
          {
            where("KernelOperatorOnUnknowns::evalF(...)");
            error("aop_inconsistent_operators", "|");
          }
          opkopv.resize(nbsv, K(0));
          mkv=1; nkv=1;
          typename Vector<K>::iterator itkb=val_k.begin(), itk;
          typename Vector<K>::iterator itv=val_opv.begin();
          typename Vector<K>::iterator itkv=opkopv.begin();
          for (number_t s=0; s<nbsv; ++s, ++itkv)
          {
            itk=itkb;
            for (number_t i=0; i<nv; ++i, ++itv, ++itk) *itkv+=*itv * *itk;
          }
          break;
        }
        case _crossProduct: //inner product opk(m x 1) ^ opv(nv x 1)
        {
          if (nv!=m || mv!=1 || n!=1)
          {
            where("KernelOperatorOnUnknowns::evalF(...)");
            error("aop_inconsistent_operators", "^");
          }
          if (m==3)   //3d cross product
          {
            opkopv.resize(3*nbsv, K(0));
            mkv=3; nkv=1;
            typename Vector<K>::iterator itv=val_opv.begin();
            typename Vector<K>::iterator itkv=opkopv.begin();
            K u1=val_k[0], u2=val_k[1],u3=val_k[2];
            for (number_t s=0; s<nbsv; ++s, itv+=3, itkv+=3)
            {
              K v1=*itv,  v2=*(itv+1), v3=*(itv+2);
              * itkv    = u2*v3 - u3*v2;
              *(itkv+1) = u3*v1 - u1*v3;
              *(itkv+2) = u1*v2 - u2*v1;
            }
          }
          else
          {
            where("KernelOperatorOnUnknowns::evalF(...)");
            error("3D_only", "opk ^ opv");
          }
          break;
        }
        default:
        {
          where("KernelOperatorOnUnknowns::evalF(...)");
          error("aop_unexpected", words("algop",aopu_));
        }
      }
      //thePrintStream<<"opkopv="<<opkopv<<eol<<std::flush;

      //opu(mu x nu) aopu opkopv(mkv x nkv)
      switch (aopu_) // as the final result is scalar, only product and inner product are handled
      {
        case _product: // only scalar * scalar
        {
          if (du==1 && mv*nkv==1)  // scalar opu, scalar opkopv
          {
            typename Vector<K>::iterator itkv=opkopv.begin();
            typename Vector<K>::iterator itub=val_opu.begin(), itu;
            typename Vector<K>::iterator itr=res.begin();
            for (number_t sv=0; sv<nbsv; ++sv, ++itkv)
            {
              itu = itub;
              K t = *itkv *alpha;
              for(number_t su=0; su<nbsu; ++su, ++itu, ++itr)  *itr+= *itu * t;
            }
          }
          else
          {
            where("KernelOperatorOnUnknowns::evalF(...)");
            error("aop_inconsistent_operators", "*");
          }
          break;
        }
        case _innerProduct: // vector opu(nu x 1), vector opkopv (mkv,1)
        {
          if (nu!=mkv)
          {
            where("KernelOperatorOnUnknowns::evalF(...)");
            error("aop_inconsistent_operators", "|");
          }
          typename Vector<K>::iterator itkvb=opkopv.begin(), itkv;
          typename Vector<K>::iterator itub=val_opu.begin(), itu;
          typename Vector<K>::iterator itr=res.begin();
          for (number_t sv=0; sv<nbsv; ++sv, itkvb+=mkv)
          {
            itu=itub;
            for (number_t su=0; su<nbsu; ++su, ++itr)
            {
              itkv = itkvb;
              for (number_t i=0; i<nu; ++i, ++itkv, ++itu) *itr+= *itkv * *itu *alpha;
            }
          }
          break;
        }
        default:
        {
          where("KernelOperatorOnUnknowns::evalF(...)");
          error("aop_unexpected", words("algop",aopu_));
        }
      }
    }
  }
}

} // end of namespace xlifepp

#endif /* KERNEL_OPERATOR_ON_UNKNOWNS_HPP */
