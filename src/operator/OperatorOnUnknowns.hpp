/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file OperatorOnUnknowns.hpp
  \author E. Lunéville
  \since 02 mar 2012
  \date 9 may 2012

  \brief Definition of the xlifepp::OperatorOnUnknowns class

  class xlifepp::OperatorOnUnknowns deals with a pair of xlifepp::OperatorOnUnknow related by an xlifepp::AlgebraicOperator
  It is useful to store syntax like opu aop opv occuring in bilinear forms
 */

#ifndef OPERATOR_ON_UNKNOWNS_HPP
#define OPERATOR_ON_UNKNOWNS_HPP

#include "config.h"
#include "OperatorOnUnknown.hpp"

namespace xlifepp
{

/*!
  \class OperatorOnUnknowns
  describes a pair of xlifepp::OperatorOnUnknown linked by an algebraic operation.
  it is a part of a bilinear form (see BilinearForm class)
*/
class OperatorOnUnknowns
{
  protected:
    OperatorOnUnknown opu_;  //!< left operator on unknown
    OperatorOnUnknown opv_;  //!< right operator on unknown (test function)
    AlgebraicOperator aop_;  //!< algebraic operation (*,|,%,^)
  public:
    OperatorOnUnknowns() {}    //!< default constructor
    OperatorOnUnknowns(const OperatorOnUnknown& opu, const OperatorOnUnknown& opv, AlgebraicOperator aop) //! basic constructor
      : opu_(opu), opv_(opv), aop_(aop) {}
    virtual ~OperatorOnUnknowns() {}
    const OperatorOnUnknown& opu() const {return opu_;}    //!< returns left operator un unknown (const)
    const OperatorOnUnknown& opv() const {return opv_;}    //!< returns right operator on unknown (const)
    OperatorOnUnknown& opu() {return opu_;}                //!< returns left operator on unknown (non const)
    OperatorOnUnknown& opv() {return opv_;}                //!< returns right operator on unknown (non const)
    const OperatorOnUnknown* opu_p() const {return &opu_;} //!< returns pointer to left operator un unknown (const)
    const OperatorOnUnknown* opv_p() const {return &opv_;} //!< returns pointer to right operator on unknown (const)
    AlgebraicOperator algop() const {return aop_;} //!< returns algebraic operator
    ValueType valueType() const; //!< return value type (real or complex)
    bool checkConsistancy();     //!< check the structure consitancy of operators and operation
    bool extensionRequired() const    //! true if operator involve non-tangential derivatives
      {return opu_.extensionRequired() || opv_.extensionRequired();}
    void setUnknowns(const Unknown&, const Unknown&); //!< set (change) the unknowns
    virtual void print(std::ostream&) const; //!< print utility
    virtual void print(PrintStream& os) const {print(os.currentStream());}
    string_t asString() const;               //!< for print purpose
};

// external functions
// function like constructor
OperatorOnUnknowns operator*(OperatorOnUnknown&, OperatorOnUnknown&); //!< opu * opv
OperatorOnUnknowns operator|(OperatorOnUnknown&, OperatorOnUnknown&); //!< opu | opv
OperatorOnUnknowns operator^(OperatorOnUnknown&, OperatorOnUnknown&); //!< opu ^ opv
OperatorOnUnknowns operator%(OperatorOnUnknown&, OperatorOnUnknown&); //!< opu % opv
OperatorOnUnknowns operator*(OperatorOnUnknown&, Unknown&);           //!< opu * v
OperatorOnUnknowns operator|(OperatorOnUnknown&, Unknown&);           //!< opu | v
OperatorOnUnknowns operator^(OperatorOnUnknown&, Unknown&);           //!< opu ^ v
OperatorOnUnknowns operator%(OperatorOnUnknown&, Unknown&);           //!< opu % v
OperatorOnUnknowns operator*(Unknown&, OperatorOnUnknown&);           //!< u * opv
OperatorOnUnknowns operator|(Unknown&, OperatorOnUnknown&);           //!< u | opv
OperatorOnUnknowns operator^(Unknown&, OperatorOnUnknown&);           //!< u ^ opv
OperatorOnUnknowns operator%(Unknown&, OperatorOnUnknown&);           //!< u % opv
OperatorOnUnknowns operator*(Unknown&, Unknown&);                     //!< u * v
OperatorOnUnknowns operator|(Unknown&, Unknown&);                     //!< u | v
OperatorOnUnknowns operator^(Unknown&, Unknown&);                     //!< u ^ v
OperatorOnUnknowns operator%(Unknown&, Unknown&);                     //!< u % v

std::ostream& operator<<(std::ostream&, const OperatorOnUnknowns&);   //!< outputs OperatorOnUnknown attributes

} // end of namespace xlifepp

#endif /* OPERATOR_ON_UNKNOWNS_HPP */
