/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file OperatorOnUnknowns.cpp
  \author E. Lunéville
  \since 4 mar 2012
  \date 9 may 2012

  \brief Implementation of xlifepp::OperatorOnUnknowns class members and related functions
*/


#include "OperatorOnUnknowns.hpp"

namespace xlifepp
{
// return value type (real or complex)
ValueType OperatorOnUnknowns::valueType() const
{
    if(opu_.valueType()==_complex || opv_.valueType()==_complex) return _complex;
    return _real;
}

// set (change) the unknowns
void OperatorOnUnknowns::setUnknowns(const Unknown& u, const Unknown& v)
{
    opu_.setUnknown(u);
    opv_.setUnknown(v);
}

void OperatorOnUnknowns::print(std::ostream& os) const
{
  if(theVerboseLevel > 0)
    {
      os << "operator on unknowns:\n ";
      os << "  left operator: " << opu_;
      os << "\n  right operator: " << opv_;
      os << "\n  operation: " << words("algop", aop_) << "\n";
    }
}

string_t OperatorOnUnknowns::asString() const
{
  std::vector<string_t> sop(4); sop[0]=" * ";sop[1]=" | ";sop[2]=" ^ ";sop[3]=" % ";
 return opu_.asString()+sop[aop_]+ opv_.asString();
}

std::ostream& operator<<(std::ostream& os, const OperatorOnUnknowns& opus)
{
  opus.print(os);
  return os;
}

// external functions
// overloaded algebraic operator between two operatoronunknowns
// note: argument are not const to avoid conflicts with templated forms (see above)
OperatorOnUnknowns operator*(OperatorOnUnknown& opu, OperatorOnUnknown& opv)
{
  if(!checkConsistancy(opu, _product, opv)) { error("opu_badopus", words("algop", _product), "operator *"); }
  return OperatorOnUnknowns(opu, opv, _product);
}

OperatorOnUnknowns operator|(OperatorOnUnknown& opu, OperatorOnUnknown& opv)
{
  if(!checkConsistancy(opu, _innerProduct, opv)) { error("opu_badopus", words("algop", _innerProduct), "operator |"); }
  return OperatorOnUnknowns(opu, opv, _innerProduct);
}
OperatorOnUnknowns operator^(OperatorOnUnknown& opu, OperatorOnUnknown& opv)
{
  if(!checkConsistancy(opu, _crossProduct, opv)) { error("opu_badopus", words("algop", _crossProduct), "operator ^"); }
  return OperatorOnUnknowns(opu, opv, _crossProduct);
}
OperatorOnUnknowns operator%(OperatorOnUnknown& opu, OperatorOnUnknown& opv)
{
  if(!checkConsistancy(opu, _contractedProduct, opv)) { error("opu_badopus", words("algop", _contractedProduct), "operator %"); }
  return OperatorOnUnknowns(opu, opv, _contractedProduct);
}

// overloaded algebraic operator between two operatoronunknown and unknown
// note: argument are not const to avoid conflicts with templated forms (see above)
OperatorOnUnknowns operator*(OperatorOnUnknown& opu, Unknown& v)
{ return opu * id(v);}

OperatorOnUnknowns operator|(OperatorOnUnknown& opu, Unknown& v)
{ return opu | id(v);}

OperatorOnUnknowns operator^(OperatorOnUnknown& opu, Unknown& v)
{ return opu ^ id(v);}

OperatorOnUnknowns operator%(OperatorOnUnknown& opu, Unknown& v)
{ return opu % id(v);}

OperatorOnUnknowns operator*(Unknown& u, OperatorOnUnknown& opv)
{ return id(u) * opv;}

OperatorOnUnknowns operator|(Unknown& u, OperatorOnUnknown& opv)
{ return id(u) | opv;}

OperatorOnUnknowns operator^(Unknown& u, OperatorOnUnknown& opv)
{ return id(u) ^ opv;}

OperatorOnUnknowns operator%(Unknown& u, OperatorOnUnknown& opv)
{ return id(u) % opv;}

// overloaded algebraic operator between two unknowns
// note: argument are not const to avoid conflicts with templated forms (see above)
OperatorOnUnknowns operator*(Unknown& u, Unknown& v)
{   return id(u) * id(v);}

OperatorOnUnknowns operator|(Unknown& u, Unknown& v)
{   return id(u) | id(v);}

OperatorOnUnknowns operator^(Unknown& u, Unknown& v)
{   return id(u) ^ id(v);}

OperatorOnUnknowns operator%(Unknown& u, Unknown& v)
{   return id(u) % id(v);}


} // end of namespace xlifepp

