/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file OperatorOnFunction.hpp
  \author E. Lunéville
  \since 05 sep 2014
  \date 05 sep 2014

  \brief Definition of the xlifepp::OperatorOnFunction class

  Class xlifepp::OperatorOnFunction stores the action of a differential operator on a xlifepp::Function
  the general following forms are accepted:
  \verbatim
  diff_op(Function)
  \endverbatim

  where diff_op is a 'differential' operator of type defined in the xlifepp::DifferentialOperator class
  only order 0 operators are managed, no derivative !
 */

#ifndef OPERATOR_ON_FUNCTION_HPP
#define OPERATOR_ON_FUNCTION_HPP

#include "config.h"
#include "DifferentialOperator.hpp"
#include "utils.h"
#include "space.h"

namespace xlifepp
{

/*!
   \class OperatorOnFunction
   describes the operations to process on Function.
*/
class OperatorOnFunction
{
  protected:
    const Function* fun_p;              //!< Function involved in operator
    DifferentialOperator* difOp_p;      //!< differential operator involved in operator
    ValueType type_;                    //!< type of returned value (one among _real, _complex)
    StrucType struct_;                  //!< structure of returned value (one among _scalar, _vector, _matrix)
    dimPair dimsRes_;                   //!< dimension of result, (0,0) if unset (NOT FULL MANAGED)

  public:
    Extension* ext_p;                   //!< extension operator to apply to
    bool in_ext;                        //!< if true interpreted as ext(difop(f)) else interpreted as difop(ext(f)); default is true
    mutable bool conjugate_;            //!< true if it has to be conjugated
    mutable bool transpose_;            //!< true if it has to be transposed

    // constructors/destructor
    OperatorOnFunction()                                                         //! default constructor
      :fun_p(nullptr), difOp_p(nullptr), type_(_real), struct_(_scalar), dimsRes_(dimPair(0,0)),
       ext_p(nullptr), in_ext(true), conjugate_(false), transpose_(false) {}
    OperatorOnFunction(const Function*, DiffOpType = _id, ValueType vt=_real, StrucType st=_scalar,
                       dimPair= dimPair(1,1));                                   //!< basic constructor
    OperatorOnFunction(const Function&, DiffOpType = _id);                       //!< basic constructor
    OperatorOnFunction(const OperatorOnFunction&);                               //!< full copy constructor
    void initStrucType();                                                        //!< build struct_ and dimsRes_
    void copy(const OperatorOnFunction&); ;                                      //!< full copy of members
    OperatorOnFunction& operator=(const OperatorOnFunction&);                    //!< assign operator (full copy)
    void clear();                                                                //!< delete all members
    ~OperatorOnFunction() {clear();}                                             //!< destructor

    //accessors
    const Function* funp() const {return fun_p;}               //!< return the Kernel pointer
    DiffOpType difOpType() const {return difOp_p->type();}     //!< return type of differential operator
    DifferentialOperator*& difOp_() {return difOp_p;}          //!< return pointer to differential operator
    DifferentialOperator&  difOp() const {return *difOp_p;}    //!< return reference to differential operator
    ValueType& valueType() {return type_;}                     //!< return the operator returned type
    ValueType valueType()const {return type_;}                 //!< return the operator returned type
    StrucType& strucType() {return struct_;}                   //!< return the operator returned structure
    StrucType strucType() const {return struct_;}              //!< return the operator returned structure
    dimPair dims() const {return dimsRes_;}                    //!< return the dimension of returned values
    dimen_t diffOrder() const                                  //!  order of differential operator
    {return difOp_p->order();}
    bool voidFunction() const                                  //!  true if null function
    {return fun_p==nullptr;}
    bool elementRequired() const                               //!  true if geom element involved
    {return fun_p!=nullptr && fun_p->requireElt;}
    bool normalRequired() const                                //!  true if normal involved
    {return difOp_p->normalRequired() || (fun_p!=nullptr && fun_p->requireNx);}
    bool tangentRequired() const                                //!  true if normal involved
    {return difOp_p->tangentRequired() ||(fun_p!=nullptr && fun_p->requireTx);}
    void setX(const Point& P) const                            //!  set X point (usefull for Kernel)
    {if(fun_p!=nullptr) fun_p->setX(P);}
    void setY(const Point& P) const                            //!  set Y point (usefull for Kernel)
    {if(fun_p!=nullptr) fun_p->setY(P);}

    //evaluation at points of operator on kernel
    template <typename T>
    T& eval(const Point&, T&,const Vector<real_t>* np=nullptr, const ExtensionData* =nullptr) const;  //!< evaluate function operator at x
    template <typename T>
    std::vector<T>& eval(const std::vector<Point>&, std::vector<T>&, const Vector<real_t>* np=nullptr, const ExtensionData* =nullptr) const;  //!< evaluate function operator at (xi)

    void print(std::ostream&) const;                          //!< print on stream attributes
    void print(PrintStream& os) const {print(os.currentStream());}
    string_t asString() const;                                //!< return as symbolic string

    friend bool operator==(const OperatorOnFunction&,const OperatorOnFunction&);
    friend bool operator!=(const OperatorOnFunction&,const OperatorOnFunction&);

}; // end of class OperatorOnFunction

bool operator==(const OperatorOnFunction&,const OperatorOnFunction&);  //!< same operator on function
bool operator!=(const OperatorOnFunction&,const OperatorOnFunction&);  //!< different operator on function

std::ostream& operator<<(std::ostream& os, const OperatorOnFunction& opf);

//---------------------------------------------------------------------------------------
//  external functions
//---------------------------------------------------------------------------------------
OperatorOnFunction& id(const Function&);                               //!< id(k)
OperatorOnFunction& ntimes(const Function&);                           //!< n*f
OperatorOnFunction& timesn(const Function&);                           //!< f*n
OperatorOnFunction& ndot(const Function&);                             //!< n|f
OperatorOnFunction& ncross(const Function&);                           //!< n^f
OperatorOnFunction& ncrossncross(const Function&);                     //!< n^(n^f)
OperatorOnFunction& ncrossntimes(Function&);                           //!< (n^n)*f
OperatorOnFunction& timesncrossn(const Function&);                     //!< f*(n^n)
OperatorOnFunction& ncrossndot(Function&);                             //!< (n^n)|f

OperatorOnFunction& operator*(UnitaryVector, const Function&);         //!< n*f same as ntimes(f)/ncrossntimes(f)
OperatorOnFunction& operator*(const Function&, UnitaryVector);         //!< f*n same as timesn(f)/timesncrossn(f)
OperatorOnFunction& operator|(UnitaryVector, const Function&);         //!< n|f same as ndot(f)
OperatorOnFunction& operator|(const Function&, UnitaryVector);         //!< f|n same as ndot(f)
OperatorOnFunction& operator^(UnitaryVector, const Function&);         //!< n^f same as ncross(f)

OperatorOnFunction& id(OperatorOnFunction&);                           //!< id(opf)
OperatorOnFunction& ntimes(OperatorOnFunction&);                       //!< n*opf
OperatorOnFunction& timesn(OperatorOnFunction&);                       //!< opf*n
OperatorOnFunction& ndot(OperatorOnFunction&);                         //!< n|opf
OperatorOnFunction& ncross(OperatorOnFunction&);                       //!< n^opf
OperatorOnFunction& ncrossntimes(OperatorOnFunction&);                 //!< (n^n)*opf
OperatorOnFunction& timesncrossn(OperatorOnFunction&);                 //!< opf*(n^n)
OperatorOnFunction& ncrossndot(OperatorOnFunction&);                   //!< (n^n)|opf
OperatorOnFunction& ncrossncross(OperatorOnFunction&);                 //!< n^(n^opf)

OperatorOnFunction& operator*(UnitaryVector, OperatorOnFunction&);     //!< n*opf
OperatorOnFunction& operator*(OperatorOnFunction&, UnitaryVector);     //!< opf*n
OperatorOnFunction& operator|(UnitaryVector, OperatorOnFunction&);     //!< n|opf
OperatorOnFunction& operator|(OperatorOnFunction&, UnitaryVector);     //!< opf|n
OperatorOnFunction& operator^(UnitaryVector, OperatorOnFunction&);     //!< n^opf

//special cases involving constant and unitary vector, create some Function on the fly
OperatorOnFunction& operator*(const real_t& v, UnitaryVector n);             //!< v*n same as f_v*n
OperatorOnFunction& operator*(UnitaryVector n, const real_t& v);             //!< n*v same as n*f_v
OperatorOnFunction& operator*(const complex_t& v, UnitaryVector n);          //!< v*n same as f_v*n
OperatorOnFunction& operator*(UnitaryVector n, const complex_t& v);          //!< n*v same as n*f_v
OperatorOnFunction& operator*(const Matrix<real_t>& v, UnitaryVector n);     //!< v*n same as f_v*n
OperatorOnFunction& operator*(UnitaryVector n, const Matrix<real_t>& v);     //!< n*v same as n*f_v
OperatorOnFunction& operator*(const Matrix<complex_t>& v, UnitaryVector n);  //!< v*n same as f_v*n
OperatorOnFunction& operator*(UnitaryVector n, const Matrix<complex_t>& v);  //!< n*v same as n*f_v
OperatorOnFunction& operator|(UnitaryVector n, const Vector<real_t>& v);     //!< n|v same as n|f_v
OperatorOnFunction& operator|(const Vector<real_t>& v, UnitaryVector n);     //!< v|n same as f_v|n
OperatorOnFunction& operator|(UnitaryVector n, const Vector<complex_t>& v);  //!< n|v same as n|f_v
OperatorOnFunction& operator|(const Vector<complex_t>& v, UnitaryVector n);  //!< v|n same as f_v|n

OperatorOnFunction& conj(OperatorOnFunction&);    //!< conjugate opf
OperatorOnFunction& tran(OperatorOnFunction&);    //!< transpose opf
OperatorOnFunction& adj(OperatorOnFunction&);     //!< conjugate and transpose opf
OperatorOnFunction& conj(Function&);              //!< conjugate f
OperatorOnFunction& tran(Function&);              //!< transpose f
OperatorOnFunction& adj(Function&);               //!< conjugate and transpose f

//special cases involving Extension
OperatorOnFunction& operator*(const Extension&, const Function&);            //!< extension of Function
OperatorOnFunction& operator*(const Extension&, OperatorOnFunction&);        //!< extension of OperatorOnFunction

// ------------------------------------------------------------------------------------------------------------------------------------
// evaluate operator on scalar functions
// ------------------------------------------------------------------------------------------------------------------------------------

//! some fake functions to override template compilation problem of OperatorOnFunction::eval
template <typename T, typename K>
inline void assignVectorTo(T& t, const K& v) { error("not_handled", "assignVectorTo<T,K>(T,K)"); }
template <> inline void assignVectorTo(real_t& t, const real_t& v) {t=v;};
template <> inline void assignVectorTo(complex_t& t, const complex_t& v) {t=v;};
template <> inline void assignVectorTo(complex_t& t, const real_t& v) {t=v;};
template <> inline void assignVectorTo(Vector<real_t>& t, const Vector<real_t>& v) {t=v;};
template <> inline void assignVectorTo(Vector<complex_t>& t, const Vector<complex_t>& v) {t=v;};
template <> inline void assignVectorTo(Vector<complex_t>& t, const Vector<real_t>& v) {t=v;};

template <typename T, typename K>
T crossProduct(const T& v, const K& s)
{
  error("not_handled", "crossProduct<T,K>(T,K)");
  return v; // dummy return
}
template <typename T, typename K>
T crossProduct2D(const T& v, const K& s)
{
  error("not_handled", "crossProduct2D<T,K>(T,K)");
  return v; // dummy return
}


//! evaluate operator on function operator at x, f function value, n pointer to normal vector (null if not available)
// T is the type of the result, be cautious there is no compatibility test
template <typename T>
T& OperatorOnFunction::eval(const Point& x, T& valf, const Vector<real_t>* np, const ExtensionData* extdata) const
{
  if(ext_p==nullptr || extdata==nullptr)
    {
      switch(difOpType())
        {
          case _id: (*fun_p)(x, valf); break;

          case _ntimes:
            {
              if(np==nullptr)
                {
                  where("OperatorOnFunction::eval(...)");
                  error("null_pointer","normal");
                }
              if(np->size()==0)
                {
                  where("OperatorOnFunction::eval(...)");
                  error("is_void","normal");
                }
              if(fun_p->strucType()==_scalar)  //scalar function
                {
                  assignVectorTo<T>(valf,*np);
                  if(fun_p->valueType()==_real) //real function
                    {
                      real_t f;
                      (*fun_p)(x, f);
                      valf*=f;
                    }
                  else                           //complex function
                    {
                      complex_t f;
                      (*fun_p)(x, f);
                      valf*=complexToRorC(f,valf);
                    }
                }
              else if(fun_p->strucType()==_matrix) //matrix function
                {
                  if(fun_p->valueType()==_real) //real function
                    {
                      Matrix<real_t> mf;
                      (*fun_p)(x, mf);
                      assignVectorTo(valf, transpose(mf) * *np);
                    }
                  else
                    {
                      Matrix<complex_t> mf;
                      (*fun_p)(x, mf);
                      assignVectorTo(valf, transpose(mf) * *np);
                    }
                }
              else
                {

                  where("OperatorOnFunction::eval(...)");
                  error("operator_not_vector",words("diffop", _ntimes));
                }
              break;
            }

          case _timesn:
            {
              if(np==nullptr  || np->size()==0)
                {
                  where("OperatorOnFunction::eval(...)");
                  error("null_pointer","normal");
                }
              if(fun_p->strucType()==_scalar)  //scalar function
                {
                  assignVectorTo<T>(valf,*np);
                  if(fun_p->valueType()==_real) //real function
                    {
                      real_t f;
                      (*fun_p)(x, f);
                      valf*=f;
                    }
                  else                           //complex function
                    {
                      complex_t f;
                      (*fun_p)(x, f);
                      valf*=complexToRorC(f,valf);
                    }
                }
              else if(fun_p->strucType()==_matrix) //matrix function
                {
                  if(fun_p->valueType()==_real) //real function
                    {
                      Matrix<real_t> mf;
                      (*fun_p)(x, mf);
                      assignVectorTo(valf, mf * *np);

                    }
                  else
                    {
                      Matrix<complex_t> mf;
                      (*fun_p)(x, mf);
                      assignVectorTo(valf, mf * *np);
                    }
                }
              else
                {
                  where("OperatorOnFunction::eval(...)");
                  error("operator_not_vector",words("diffop",_timesn));
                }
              break;
            }

          case _ndot: //assume T is a scalar type and fun a vector function
            {
              if(np==nullptr  || np->size()==0)
                {
                  where("OperatorOnFunction::eval(...)");
                  error("null_pointer","normal");
                }
              valf*=0;
              Vector<T> f; (*fun_p)(x, f);
              Vector<real_t>::const_iterator itn=np->begin();
              typename Vector<T>::iterator itf=f.begin();
              for(; itn!=np->end() && itf!=f.end(); itn++, itf++) valf+= *itn** itf;
              break;
            }

          case _ncross: //assume T is a vector type and fun a vector function
            {
              if(np==nullptr)
                {
                  where("OperatorOnFunction::eval(...)");
                  error("null_pointer","normal");
                }
              number_t d=np->size();
              if(d<2)
                {
                  where("OperatorOnFunction::eval(...)");
                  error("bad_size","normal", ">=2", d);
                }
              valf*=0;
              if(d==3)
                {
                  T f; (*fun_p)(x, f);
                  assignVectorTo<T>(valf,crossProduct(*np,f));
                }
              else
                {
                  Vector<T> f; (*fun_p)(x, f);
                  assignVectorTo<T>(valf,crossProduct2D(*np,f));
                }
              break;
            }

          case _ncrossntimes:  //(n^n)*F
            {
              if(np==nullptr)
                {
                  where("OperatorOnFunction::eval(...)");
                  error("null_pointer","normal");
                }
              number_t d=np->size();
              if(d<2)
                {
                  where("OperatorOnFunction::eval(...)");
                  error("bad_size","normal", ">=2",d);
                }
              valf*=0;
              if(fun_p->strucType()==_vector)
                {
                  where("OperatorOnFunction::eval(...)");
                  error("operator_fun_not_vector",words("diffop",_ncrossntimes));
                }
              if(fun_p->valueType()==_real)
                {
                  real_t f; (*fun_p)(x, f);
                  if(d==3) assignVectorTo<T>(valf,crossProduct(*np,*np)*f);    //return a vector
                  else     assignVectorTo<T>(valf,crossProduct2D(*np,*np)*f);  //return a scalar
                }
              else
                {
                  complex_t f; (*fun_p)(x, f);
                  if(d==3) assignVectorTo<T>(valf,crossProduct(*np,*np)*f);    //return a vector
                  else     assignVectorTo<T>(valf,crossProduct2D(*np,*np)*f);  //return a scalar
                }
              break;
            }

//          case _ncrossndot:  //(n^n)|F   NO MEANING n^n=0 !
//            {
//              if(np==nullptr)
//                {
//                  where("OperatorOnFunction::eval(...)");
//                  error("null_pointer","normal");
//                }
//              number_t d=np->size();
//              if(d<2)
//                {
//                  where("OperatorOnFunction::eval(...)");
//                  error("bad_size","normal", ">=2", d);
//                }
//              valf*=0;
//              Vector<T> f; (*fun_p)(x, f);
//              Vector<real_t> ncn;
//              if(d==3) ncn=crossProduct(*np,*np);
//              else     ncn=crossProduct2D(*np,*np);
//              Vector<real_t>::iterator itn=ncn.begin();
//              typename Vector<T>::iterator itf=f.begin();
//              for(; itn!=np->end() && itf!=f.end(); itn++, itf++) valf+= *itn** itf;
//              break;
//            }

          case _ncrossncross:  //n^(n^F)
            {
              if(np==nullptr)
                {
                  where("OperatorOnFunction::eval(...)");
                  error("null_pointer","normal");
                }
              number_t d=np->size();
              if(d<2)
                {
                  where("OperatorOnFunction::eval(...)");
                  error("bad_size","normal", ">=2",d);
                }
              valf*=0;
              T f; (*fun_p)(x, f);
              if(d==3)
                {
                  assignVectorTo<T>(valf,crossProduct(*np,f));
                  assignVectorTo<T>(valf,crossProduct(*np,valf));
                }
              else valf=crossProduct2D(*np,*np)*f;                         //return a scalar
              break;
            }
          default:
            where("OperatorOnFunction::eval(...)");
            error("operator_unexpected",words("diffop",difOpType()));
        }
      if(xlifepp::strucType(valf)==_matrix  && transpose_) valf=tran(valf);
      if(xlifepp::valueType(valf)==_complex && conjugate_) valf=conj(valf);
      return valf;
    }

  // extension from side to domain
  std::vector<Point>::const_iterator itp= extdata->nodes.begin();  //nodes on side domain
  std::vector<real_t>::const_iterator itw=extdata->w.begin();      //shapevalues related to nodes at x
  T vali;  valf*=0;
  if (in_ext || difOp_p->order()==0) //ext(op(f))= sum_{i} op(f(xi))*wi(x)
    {
      for(; itp!=extdata->nodes.end(); ++itp, ++itw)
        {
          this->eval(*itp, vali, np, 0);
          valf+=*itw*vali;
        }
    }
  else
  {
    where("OperatorOnFunction::eval(...)");
    error("derivative_op_no_extension");
  }
  return valf; // dummy return
}

/*! evaluate function operator at (xi):  op [f(x1), f(x2), ..., f(xm)]
    n pointer to normal vector n (null if not available) */
template <typename T>
std::vector<T>& OperatorOnFunction::eval(const std::vector<Point>& xs, std::vector<T>& fs,
    const Vector<real_t>* np, const ExtensionData* extdata) const
{
  fs.resize(xs.size(),T(0));
  std::vector<Point>::const_iterator itx=xs.begin();
  typename std::vector<T>::iterator itf=fs.begin();
  for(; itx!=xs.end(); itx++)  eval(*itx, * itf, np, extdata);
  return fs;
}

} // end of namespace xlifepp

#endif /* OPERATOR_ON_FUNCTION_HPP */
