/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LcOperatorOnUnknown.hpp
  \author E. Lunéville
  \since 14 jan 2014
  \date  29 jun 2018

  \brief Definition of the xlifepp::LcOperatorOnUnknown class

  xlifepp::LcOperatorOnUnknown class handles linear combination of OperatorOnUnknown's

                                    sum ( a_i op_i(u_i) on dom_i )

  It inherits from std::vector<OpuValPair> where OpuValPair is an alias of std::pair<xlifepp::OperatorOnUnknown*, xlifepp::complex_t>
  and domains are stored as pointer in OperatorOnUnknown
  some domain pointers may be 0, it means that the corresponding operators have no domain restriction
 */

#ifndef LC_OPERATOR_ON_UNKNOWN_HPP
#define LC_OPERATOR_ON_UNKNOWN_HPP

#include "config.h"
#include "OperatorOnUnknown.hpp"

namespace xlifepp
{
    //forward declaration
    class EssentialCondition;
    class TermVector;

// Note: OperatorOnUnknown are always copy of users OperatorOnUnknown

//@{
//i useful aliases for LcOperatorOnUnknown class
typedef std::pair<OperatorOnUnknown*, complex_t> OpuValPair;
typedef std::vector<OpuValPair>::iterator it_opuval;
typedef std::vector<OpuValPair>::const_iterator cit_opuval;
//@}
/*!
   \class LcOperatorOnUnknown
   describes the linear combination of OperatorOnUnknown
*/

class LcOperatorOnUnknown : public std::vector<OpuValPair>
{
public:
    LcOperatorOnUnknown() {}                                            //!< default constructor
    LcOperatorOnUnknown(const OperatorOnUnknown&, const real_t& = 1.);  //!< basic constructor
    LcOperatorOnUnknown(const OperatorOnUnknown&, const complex_t&);    //!< basic constructor
    LcOperatorOnUnknown(const Unknown&, const real_t& = 1.);            //!< basic constructor
    LcOperatorOnUnknown(const Unknown&, const complex_t&);              //!< basic constructor
    LcOperatorOnUnknown(const LcOperatorOnUnknown&);                    //!< copy constructor
    ~LcOperatorOnUnknown();                                             //!< destructor

    LcOperatorOnUnknown& operator =(const LcOperatorOnUnknown&);        //!< assignment
    void clear();                                                       //!< deallocate OperatorOnUnknown pointers
    void copy(const LcOperatorOnUnknown& lc);                           //!< full copy of OperatorOnUnknown pointers

    //utilities
    void insert(const OperatorOnUnknown&);                      //!< insert op(u) on D in list
    void insert(const real_t&, const OperatorOnUnknown&);       //!< insert a*op(u) on D in list
    void insert(const complex_t&, const OperatorOnUnknown&);    //!< insert a*op(u) on Din list
    void insert(const Unknown&);                                //!< insert u on D in list
    void insert(const real_t&, const Unknown&);                 //!< insert a*u on D in list
    void insert(const complex_t&, const Unknown&);              //!< insert a*u on D in list

    bool isSingleUnknown() const;                             //!< true if all terms involve the same unknown
    const Unknown* unknown(number_t i=1) const;               //!< return ith Unknown involved in LcOperator
    std::set<const Unknown*> unknowns() const;                //!< return set of Unknown's involved in LcOperator
    bool withDomains() const;                                 //!< true if all terms have a non null domain pointer
    bool isSingleDomain() const;                              //!< true if all terms involve the same domain
    GeomDomain* domain(number_t i=1) const;                   //!< return ith domain (may be 0 if none)
    std::set<GeomDomain*> domainSet() const;                  //!< return set of GeomDomains's involved in condition (may be void)
    std::vector<GeomDomain*> domains() const;                 //!< return list of GeomDomains's attached to
    complex_t coefficient() const;                            //!< return coefficient of first term if a single term combination
    std::vector<complex_t> coefficients() const;              //!< return vector of coefficients involved in combination
    std::set<DiffOpType> diffOperators() const;               //!< return set of differential operator types involved in condition

    //algebraic operations
    LcOperatorOnUnknown& operator +=(const OperatorOnUnknown&);     //!< lcop += opu
    LcOperatorOnUnknown& operator +=(const Unknown&);               //!< lcop += u
    LcOperatorOnUnknown& operator +=(const LcOperatorOnUnknown&);   //!< lcop += lcop
    LcOperatorOnUnknown& operator -=(const OperatorOnUnknown&);     //!< lcop -= opu
    LcOperatorOnUnknown& operator -=(const Unknown&);               //!< lcop -= u
    LcOperatorOnUnknown& operator -=(const LcOperatorOnUnknown&);   //!< lcop -= lcop
    LcOperatorOnUnknown& operator *=(const real_t&);                  //!< lcop *= r
    LcOperatorOnUnknown& operator *=(const complex_t&);               //!< lcop *= c
    LcOperatorOnUnknown& operator /=(const real_t&);                  //!< lcop /= r
    LcOperatorOnUnknown& operator /=(const complex_t&);               //!< lcop /= c

    //domain affectation
    void setDomain(GeomDomain&);                              //!< restrict LcOperatorOnUnknown (all operators) to domain dom
    LcOperatorOnUnknown& operator |(GeomDomain&);             //!< restrict LcOperatorOnUnknown (all operators) to domain dom using syntax |dom

    //Equation constructor
    EssentialCondition operator = (const real_t &);            //!< construct equation lcop = r
    EssentialCondition operator = (const complex_t &);         //!< construct equation lcop = c
    EssentialCondition operator = (const Function &);          //!< construct equation lcop = f
    EssentialCondition operator = (const TermVector &);        //!< construct equation lcop = TermVector

    // print utilities
    void print(std::ostream&, bool withdomain=true) const; //!< print utility
    void print(PrintStream& os, bool withdomain=true) const {print(os.currentStream(), withdomain);} //!< print utility
    friend std::ostream& operator<<(std::ostream&, const LcOperatorOnUnknown&);
};

std::ostream& operator<<(std::ostream&, const LcOperatorOnUnknown&); //!< print operator

//@{
//! algebraic operations
LcOperatorOnUnknown operator +(const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator -(const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator +(const LcOperatorOnUnknown&, const OperatorOnUnknown&);
LcOperatorOnUnknown operator -(const LcOperatorOnUnknown&, const OperatorOnUnknown&);
LcOperatorOnUnknown operator +(const OperatorOnUnknown&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator -(const OperatorOnUnknown&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator +(const LcOperatorOnUnknown&, const Unknown&);
LcOperatorOnUnknown operator -(const LcOperatorOnUnknown&, const Unknown&);
LcOperatorOnUnknown operator +(const Unknown&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator -(const Unknown&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator +(const LcOperatorOnUnknown&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator -(const LcOperatorOnUnknown&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator *(const LcOperatorOnUnknown&, const real_t&);
LcOperatorOnUnknown operator *(const LcOperatorOnUnknown&, const complex_t&);
LcOperatorOnUnknown operator *(const real_t&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator *(const complex_t&, const LcOperatorOnUnknown&);
LcOperatorOnUnknown operator /(const LcOperatorOnUnknown&, const real_t&);
LcOperatorOnUnknown operator /(const LcOperatorOnUnknown&, const complex_t&);

LcOperatorOnUnknown operator +(const OperatorOnUnknown&, const OperatorOnUnknown&);
LcOperatorOnUnknown operator -(const OperatorOnUnknown&, const OperatorOnUnknown&);
LcOperatorOnUnknown operator +(const OperatorOnUnknown&, const Unknown&);
LcOperatorOnUnknown operator -(const OperatorOnUnknown&, const Unknown&);
LcOperatorOnUnknown operator +(const Unknown&, const OperatorOnUnknown&);
LcOperatorOnUnknown operator -(const Unknown&, const OperatorOnUnknown&);
LcOperatorOnUnknown operator +(const Unknown&, const Unknown&);
LcOperatorOnUnknown operator -(const Unknown&, const Unknown&);

//@}

} // end of namespace xlifepp

#endif /* LC_OPERATOR_ON_UNKNOWN_HPP */

