/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LcOperatorOnUnknown.hpp
  \author E. PEILLON
  \since 1 jan 2022
  \date  1 jan 2022

  \brief Definition of the xlifepp::LcOperatorOnUnknowns class

  xlifepp::LcOperatorOnUnknowns class handles linear combination of
  OperatorOnUnknowns's (note the double 's')

                                    sum ( a * Op(u,v) )

  It inherits from std::vector<Opuv> where Opuv is an alias of xlifepp::OperatorOnUnknowns*
 */

#ifndef LC_OPERATOR_ON_UNKNOWNS_HPP
#define LC_OPERATOR_ON_UNKNOWNS_HPP

#include "LcOperatorOnUnknown.hpp"
#include "OperatorOnUnknowns.hpp"
#include "config.h"

namespace xlifepp
{

//@{
//i useful aliases for LcOperatorOnUnknowns class
typedef std::pair<OperatorOnUnknowns *, complex_t> OpusValPair;
typedef std::vector<OpusValPair>::iterator it_opusval;
typedef std::vector<OpusValPair>::const_iterator cit_opusval;
//@}
/*!
   \class LcOperatorOnUnknowns
   describes the linear combination of OperatorOnUnknowns
*/

class LcOperatorOnUnknowns : public std::vector<OpusValPair>
{
public:
  LcOperatorOnUnknowns() {}                                              //!< default constructor
  LcOperatorOnUnknowns(const OperatorOnUnknowns &, const real_t & = 1.); //!< basic constructor
  LcOperatorOnUnknowns(const OperatorOnUnknowns &, const complex_t &);   //!< basic constructor
  LcOperatorOnUnknowns(const LcOperatorOnUnknowns &);                    //!< basic constructor
  ~LcOperatorOnUnknowns();                                               //!< destructor

  LcOperatorOnUnknowns &operator=(const LcOperatorOnUnknowns &); //!< assignment
  void clear();                                                  //!< deallocate OperatorOnUnknown pointers
  void copy(const LcOperatorOnUnknowns &lc);                     //!< full copy of OperatorOnUnknown pointers

  //utilities
  void insert(const OperatorOnUnknowns &);                    //!< insert opuv in the list
  void insert(const real_t &, const OperatorOnUnknowns &);    //!< insert a*opuv in the list
  void insert(const complex_t &, const OperatorOnUnknowns &); //!< insert a*opuv in the list

  bool isSingleUVPair() const;                        //!< true if all terms involve the same unknown
  const OperatorOnUnknown *opu(number_t i = 1) const; //!< return ith left OperatorOnUnknown involved in LcOperator's
  const OperatorOnUnknown *opv(number_t i = 1) const; //!< return ith right OperatorOnUnknown involved in LcOperator's
  const Unknown *unknownu(number_t i = 1) const;      //!< return ith left Unknown involved in LcOperator
  const Unknown *unknownv(number_t i = 1) const;      //!< return ith right Unknown involved in LcOperator
  // bool withDomains() const;                                 //!< true if all terms have a non null domain pointer
  // bool isSingleDomain() const;                              //!< true if all terms involve the same domain
  // GeomDomain* domain(number_t i=1) const;                   //!< return ith domain (may be 0 if none)
  // std::set<GeomDomain*> domainSet() const;                  //!< return set of GeomDomains's involved in condition (may be void)
  // std::vector<GeomDomain*> domains() const;                 //!< return list of GeomDomains's attached to
  complex_t coefficient(number_t i = 1) const; //!< return ith coefficient
  std::vector<complex_t> coefficients() const; //!< return vector of coefficients involved in combination

  //algebraic operations
  LcOperatorOnUnknowns &operator+=(const OperatorOnUnknowns &);   //!< lcopuv += opuv
  LcOperatorOnUnknowns &operator+=(const LcOperatorOnUnknowns &); //!< lcopuv += lcopuv
  LcOperatorOnUnknowns &operator-=(const OperatorOnUnknowns &);   //!< lcopuv -= opuv
  LcOperatorOnUnknowns &operator-=(const LcOperatorOnUnknowns &); //!< lcopuv -= lcopuv
  LcOperatorOnUnknowns &operator*=(const real_t &);               //!< lcopuv *= r
  LcOperatorOnUnknowns &operator*=(const complex_t &);            //!< lcopuv *= c
  LcOperatorOnUnknowns &operator/=(const real_t &);               //!< lcopuv /= r
  LcOperatorOnUnknowns &operator/=(const complex_t &);            //!< lcopuv /= c

  // //domain affectation
  // void setDomain(GeomDomain&);                              //!< restrict LcOperatorOnUnknown (all operators) to domain dom
  // LcOperatorOnUnknown& operator |(GeomDomain&);             //!< restrict LcOperatorOnUnknown (all operators) to domain dom using syntax |dom

  // print utilities
  void print(std::ostream &) const;                                //!< print utility
  void print(PrintStream &os) const { print(os.currentStream()); } //!< print utility
  friend std::ostream &operator<<(std::ostream &, const LcOperatorOnUnknowns &);
};

std::ostream &operator<<(std::ostream &, const LcOperatorOnUnknowns &); //!< print operator

//@{
//! algebraic operations
// OperatoronUnknowns and OperatorOnUnknowns
LcOperatorOnUnknowns operator+(const OperatorOnUnknowns &, const OperatorOnUnknowns &);
LcOperatorOnUnknowns operator-(const OperatorOnUnknowns &, const OperatorOnUnknowns &);

// LcOperatorOnUnknowns and LcOperatorOnUnknowns
LcOperatorOnUnknowns operator+(const LcOperatorOnUnknowns &);
LcOperatorOnUnknowns operator-(const LcOperatorOnUnknowns &);
LcOperatorOnUnknowns operator+(const LcOperatorOnUnknowns &, const LcOperatorOnUnknowns &);
LcOperatorOnUnknowns operator-(const LcOperatorOnUnknowns &, const LcOperatorOnUnknowns &);

// LcOperatorOnUnknowns and OperatorOnUnknowns
LcOperatorOnUnknowns operator+(const LcOperatorOnUnknowns &, const OperatorOnUnknowns &);
LcOperatorOnUnknowns operator-(const LcOperatorOnUnknowns &, const OperatorOnUnknowns &);
LcOperatorOnUnknowns operator+(const OperatorOnUnknowns &, const LcOperatorOnUnknowns &);
LcOperatorOnUnknowns operator-(const OperatorOnUnknowns &, const LcOperatorOnUnknowns &);

// LcOperatorOnUnknowns and scalar
LcOperatorOnUnknowns operator*(const LcOperatorOnUnknowns &, const real_t &);
LcOperatorOnUnknowns operator*(const LcOperatorOnUnknowns &, const complex_t &);
LcOperatorOnUnknowns operator*(const real_t &, const LcOperatorOnUnknowns &);
LcOperatorOnUnknowns operator*(const complex_t &, const LcOperatorOnUnknowns &);
LcOperatorOnUnknowns operator/(const LcOperatorOnUnknowns &, const real_t &);
LcOperatorOnUnknowns operator/(const LcOperatorOnUnknowns &, const complex_t &);

// LcOperatorOnUnknown and OperatorOnUnkown
LcOperatorOnUnknowns operator*(const LcOperatorOnUnknown &, const OperatorOnUnknown &);
LcOperatorOnUnknowns operator*(const OperatorOnUnknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator%(const LcOperatorOnUnknown &, const OperatorOnUnknown &);
LcOperatorOnUnknowns operator%(const OperatorOnUnknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator|(const LcOperatorOnUnknown &, const OperatorOnUnknown &);
LcOperatorOnUnknowns operator|(const OperatorOnUnknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator^(const LcOperatorOnUnknown &, const OperatorOnUnknown &); // not implemented
LcOperatorOnUnknowns operator^(const OperatorOnUnknown &, const LcOperatorOnUnknown &); // not implemented

// LcOperatorOnUnknown and Unkown
LcOperatorOnUnknowns operator*(const LcOperatorOnUnknown &, const Unknown &);
LcOperatorOnUnknowns operator*(const Unknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator%(const LcOperatorOnUnknown &, const Unknown &);
LcOperatorOnUnknowns operator%(const Unknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator|(const LcOperatorOnUnknown &, const Unknown &);
LcOperatorOnUnknowns operator|(const Unknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator^(const LcOperatorOnUnknown &, const Unknown &); // not implemented
LcOperatorOnUnknowns operator^(const Unknown &, const LcOperatorOnUnknown &); // not implemented

// LcOperatorOnUnknown and LcOperatorOnUnkown
LcOperatorOnUnknowns operator*(const LcOperatorOnUnknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator%(const LcOperatorOnUnknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator|(const LcOperatorOnUnknown &, const LcOperatorOnUnknown &);
LcOperatorOnUnknowns operator^(const LcOperatorOnUnknown &, const LcOperatorOnUnknown &); // not implemented

//@}

} // end of namespace xlifepp

#endif /* LC_OPERATOR_ON_UNKNOWNS_HPP */