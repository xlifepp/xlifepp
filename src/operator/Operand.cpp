/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Operand.cpp
  \author E. Lunéville
  \since 04 mar 2012
  \date 09 may 2012

  \brief Implementation of xlifepp::Operand class members and related functions
*/

#include "Operand.hpp"

namespace xlifepp
{

std::ostream& operator<<(std::ostream& out, const AlgebraicOperator& ao)
{ out << words("algop", ao); return out; }

//------------------------------------------------------------------------------------
// Operand member functions
//------------------------------------------------------------------------------------
// construct Operand from Function (not copied)
Operand::Operand(const OperatorOnFunction& opfun, AlgebraicOperator aop)
{
  opfun_p = new OperatorOnFunction(opfun);
  opker_p = nullptr;
  val_p = nullptr;
  operation_ = aop;
  conjugate_ = false;
  transpose_ = false;
  dims_=opfun.dims();
  if(opfun_p->funp()->conjugate())
    {
      conjugate_ = true;
      opfun_p->funp()->conjugate(false); // reset temporary conjugate state flag
    }
  if(opfun_p->funp()->transpose())
    {
      transpose_ = true;
      opfun_p->funp()->transpose(false); // reset temporary conjugate state flag
    }
}

// construct Operand from Function (not copied)
Operand::Operand(const Function& fun, AlgebraicOperator aop)
{
  //opfun_p = new OperatorOnFunction(&fun);
  opfun_p = &id(fun);
  opker_p = nullptr;
  val_p = nullptr;
  operation_ = aop;
  conjugate_ = false;
  transpose_ = false;
  dims_=fun.dims();
  if(fun.conjugate())
    {
      conjugate_ = true;
      fun.conjugate(false); // reset temporary conjugate state flag
    }
  if(fun.transpose())
    {
      transpose_ = true;
      fun.transpose(false); // reset temporary conjugate state flag
    }
}

// construct Operand from Function (not copied)
Operand::Operand(OperatorOnKernel& opker, AlgebraicOperator aop)
{
  opfun_p = nullptr;
  opker_p = &opker;
  val_p = nullptr;
  operation_ = aop;
  conjugate_ = false;
  transpose_ = false;
  dims_=opker.dims();
  if(opker_p->kernelp()->conjugate_)
    {
      conjugate_ = true;
      opker_p->kernelp()->conjugate_=false; // reset temporary conjugate state flag
    }
  if(opker_p->kernelp()->transpose_)
    {
      transpose_ = true;
      opker_p->kernelp()->transpose_=false; // reset temporary conjugate state flag
    }
}

Operand::Operand(const OperatorOnKernel& opker, AlgebraicOperator aop)
{
  {
  opfun_p = nullptr;
  opker_p = new OperatorOnKernel(opker);
  val_p = nullptr;
  operation_ = aop;
  conjugate_ = false;
  transpose_ = false;
  dims_=opker.dims();
  if(opker_p->kernelp()->conjugate_)
    {
      conjugate_ = true;
      opker_p->kernelp()->conjugate_=false; // reset temporary conjugate state flag
    }
  if(opker_p->kernelp()->transpose_)
    {
      transpose_ = true;
      opker_p->kernelp()->transpose_=false; // reset temporary conjugate state flag
    }
  }
}

// construct Operand from Kernel (full copy)
Operand::Operand(const Kernel& ker, AlgebraicOperator aop)
{
  opfun_p = nullptr;
  opker_p = new OperatorOnKernel(&ker);
  val_p = nullptr;
  operation_ = aop;
  conjugate_ = false;
  transpose_ = false;
  dims_=ker.dims();
  if(ker.conjugate_)
    {
      conjugate_ = true;
      ker.conjugate_=false; // reset temporary conjugate state flag
    }
  if(ker.transpose_)
    {
      transpose_ = true;
      ker.transpose_=false; // reset temporary conjugate state flag
    }
}

// construct Operand from Value (copied)
Operand::Operand(const Value& v, AlgebraicOperator aop)
{
  opfun_p = nullptr;
  opker_p = nullptr;
  val_p = new Value(v); // copy of Value
  operation_ = aop;
  conjugate_ = false;
  transpose_ = false;
  dims_=v.dims();
  if(v.conjugate_)
    {
      conjugate_ = true;
      v.conjugate_= false; // reset temporary conjugate state flag
    }
  if(v.transpose_)
    {
      transpose_ = true;
      v.transpose_= false; // reset temporary transpose state flag
    }
}

// construct Operand from Value (value not copied)
Operand::Operand(const Value* v, AlgebraicOperator aop)
{
  opfun_p = nullptr;
  opker_p = nullptr;
  val_p = v;
  operation_ = aop;
  conjugate_ = false;
  transpose_ = false;
  dims_=v->dims();
  if(v->conjugate_)
    {
      conjugate_ = true;
      v->conjugate_=false; // reset temporary conjugate state flag
    }
  if(v->transpose_)
    {
      transpose_ = true;
      v->transpose_=false; // reset temporary transpose state flag
    }
}

// copy construct  and assign operator (full copy)
Operand::Operand(const Operand& op)
{
  val_p=nullptr;
  opfun_p=nullptr;
  opker_p=nullptr;
  copy(op);
}

Operand& Operand::operator = (const Operand& op)
{
    if(this != &op) {
      clear();
      copy(op);
      }
    return *this;
}

// copy tool, pointers have to be reset to 0
void Operand::copy(const Operand& op)
{
  if(op.val_p!=nullptr) val_p= new Value(*op.val_p);
  if(op.opfun_p!=nullptr) opfun_p= new OperatorOnFunction(*op.opfun_p);
  if(op.opker_p!=nullptr) opker_p= new OperatorOnKernel(*op.opker_p);
  conjugate_=op.conjugate_;
  transpose_=op.transpose_;
  operation_=op.operation_;
  dims_=op.dims_;
}

// clear all pointer and reset to 0
void Operand::clear()
{
  if(val_p!=nullptr) delete val_p;
  if(opfun_p!=nullptr) delete opfun_p;
  if(opker_p!=nullptr) delete opker_p;
  val_p=nullptr;
  opfun_p=nullptr;
  opker_p=nullptr;
}



// access to operand attributes (Function or Value)
const OperatorOnFunction& Operand::opfunction() const
{
  if(opfun_p == nullptr)
    { error("operand_notafun", "in operand, try to access to function and it is not "); }
  return *opfun_p;
}

const OperatorOnKernel& Operand::opkernel() const
{
  if(opker_p == nullptr)
    { error("operand_notafun", "in operand, try to access to kernel and it is not "); }
  return *opker_p;
}

const Function& Operand::function() const
{
  if(opfun_p == nullptr)
    { error("operand_notafun", "in operand, try to access to function and it is not "); }
  return *opfun_p->funp();
}

const Kernel& Operand::kernel() const
{
  if(opker_p == nullptr)
    { error("operand_notafun", "in operand, try to access to kernel and it is not "); }
  return *opker_p->kernelp();
}

// return the operand value as object
const Value& Operand::value() const
{
  if(val_p == nullptr)
    { error("operand_notavalue", "in operand, try to access to a value and it is not "); }
  return *val_p;
}

//const Value& Operand::value()
Value& Operand::value()
{
  if(val_p == nullptr)
    { error("operand_notavalue", "in operand, try to access to a value and it is not "); }
  return const_cast<Value&>(*val_p);
}

//return value and structure type of value or function
ValueType Operand::valueType() const
{
  if(opfun_p != nullptr) return opfun_p->valueType();
  if(opker_p != nullptr) return opker_p->valueType();
  return val_p->valueType();
}
StrucType Operand::strucType() const
{
  if(opfun_p != nullptr) return opfun_p->strucType();
  if(opker_p != nullptr) return opker_p->strucType();
  return val_p->strucType();
}

// true if normal involved
bool Operand::normalRequired() const
{
    if(opfun_p!=nullptr && opfun_p->normalRequired()) return true;
    if(opker_p!=nullptr && opker_p->normalRequired()) return true;
    return false;
}

// true if x-normal involved
bool Operand::xnormalRequired() const
{
    if(opfun_p!=nullptr && opfun_p->normalRequired()) return true;
    if(opker_p!=nullptr && opker_p->xnormalRequired()) return true;
    return false;
}

// true if y-normal involved
bool Operand::ynormalRequired() const
{
    if(opfun_p!=nullptr && opfun_p->normalRequired()) return true;
    if(opker_p!=nullptr && opker_p->ynormalRequired()) return true;
    return false;
}

// true if tangent involved
bool Operand::tangentRequired() const
{
    if(opfun_p!=nullptr && opfun_p->tangentRequired()) return true;
    if(opker_p!=nullptr && opker_p->tangentRequired()) return true;
    return false;
}

// true if x-tangent involved
bool Operand::xtangentRequired() const
{
    if(opfun_p!=nullptr && opfun_p->tangentRequired()) return true;
    if(opker_p!=nullptr && opker_p->xtangentRequired()) return true;
    return false;
}

// true if y-tangent involved
bool Operand::ytangentRequired() const
{
    if(opfun_p!=nullptr && opfun_p->tangentRequired()) return true;
    if(opker_p!=nullptr && opker_p->ytangentRequired()) return true;
    return false;
}

// true if element is required by any function in
bool Operand::elementRequired() const
{
    if(opfun_p!=nullptr && opfun_p->elementRequired()) return true;
    if(opker_p!=nullptr && opker_p->elementRequired()) return true;
    return false;

}


// compare Operands
bool operator==(const Operand& op1, const Operand& op2)
{
  if(op1.operation() != op2.operation()) return false;
  if(op1.conjugate_ != op2.conjugate_) return false;
  if(op1.transpose_ != op2.transpose_) return false;
  if(op1.isFunction() && op2.isFunction()) return op1.opfunction()==op2.opfunction();
  if(op1.isKernel() && op2.isKernel()) return op1.opkernel()==op2.opkernel();
  if(op1.isValue() && op2.isValue()) return op1.value()==op2.value();
  return false;
}

bool operator!=(const Operand& op1, const Operand& op2)
{
  return !(op1 == op2);
}

// print utility
void Operand::print(std::ostream& os) const
{
  string_t st;
  if(isFunction())
    {
      st=words("function", opfun_p->funp()->typeFunction()) + " '" + opfun_p->funp()->name() + "'";
      if(opfun_p->difOpType()!=_id) st+= "operator "+words("diffop",opfun_p->difOpType());
    }
  else if(isKernel())
    {
      st=words("function", _kernel) + " '" + opker_p->kernelp()->name + "'";
      if(opker_p->xdifOpType()!=_id) st+= " operator "+words("diffop",opker_p->xdifOpType());
      if(opker_p->ydifOpType()!=_id) st+= " operator "+words("diffop",opker_p->ydifOpType());
      if(opker_p->xydifOpType()!=_id) st+= " operator "+words("diffop",opker_p->xydifOpType());
    }
  else if(val_p != nullptr) { st = "(" + words("value", val_p->valueType()) + "," + words("structure", val_p->strucType()) + ")"; }
  os << " " << st;
  if(conjugate_) { os << " (" << words("conjugate") << ")"; }
  if(transpose_) { os << " (" << words("transpose") << ")"; }
  os << " " << words("algop", operation_);
}

void Operand::printsymbolic(std::ostream& os) const
{
  if(isFunction())
  {
      if(opfun_p->funp()->name()!="") os<<opfun_p->funp()->name();
      else os<<"fun";
      os<<"() "<< words("algop", operation_)<<" ";
  }
  else if(val_p != nullptr)
    os<< words("value", val_p->valueType())<< " " <<words("structure", val_p->strucType())<<" " << words("algop", operation_)<<" ";
}

 string_t Operand::asString() const
 {
   std::vector<string_t> sop(4); sop[0]=" * ";sop[1]=" | ";sop[2]=" ^ ";sop[3]=" % ";
     string_t s="";
     if(isFunction())
     {
      s+=opfun_p->asString();
      s+=sop[operation_]+" ";
     }
     if(isKernel())
     {
       s+=opker_p->asString();
       s+=sop[operation_]+" ";
     }
    if(isValue())
    {
      s+= words("value", val_p->valueType())+ " " +words("structure", val_p->strucType());
      s+=sop[operation_]+" ";
    }
    return s;
 }

std::ostream& operator<<(std::ostream& os, const Operand& op)
{
  op.print(os);
  return os;
}

//for template compilation reasons, fake definition of dot and crossproduct
real_t dot(const real_t& x, const real_t& y) {return x * y;}
complex_t dot(const complex_t& x, const complex_t& y) {return x * y;}
complex_t dot(const complex_t& x, const real_t& y) {return x * y;}
complex_t dot(const real_t& x, const complex_t& y) {return x * y;}
real_t crossProduct(const real_t& x, const real_t& y) {return 0.;}
complex_t crossProduct(const complex_t& x, const complex_t& y) {return 0.;}
complex_t crossProduct(const complex_t& x, const real_t& y) {return 0.;}
complex_t crossProduct(const real_t& x, const complex_t& y) {return 0.;}

} // end of namespace xlifepp

