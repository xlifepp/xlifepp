/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file OperatorOnKernel.hpp
  \author E. Lunéville
  \since 21 sep 2013
  \date 21 sep 2013

  \brief Definition of the xlifepp::OperatorOnKernel class

  Class xlifepp::OperatorOnKernel stores the action of a differential operator on a xlifepp::Kernel function
  the general following forms are accepted:
  \verbatim
  diff_op_y diff_op_y (Kernel)
  \endverbatim

  where
    - diff_op_y, dif_op_x are differential operator of type defined in the xlifepp::DifferentialOperator class

  All the attributes may be omitted, except Kernel!

  For instance, the following syntaxes are available
  \verbatim
  K                       (trivial form)
  grad_x(K), grad_y(K), grad_x(grad_y(K))
  dn_x(K), dn_y(K)
  dn_x(dn_y(K))
  ndot_x(K) or K|nx
  ncross_x(K) or K^nx
  ncross_x(ncross_x(K)) or nx^(nx^K)
  \endverbatim

  It is possible to use predefined vectors such as the normal vector _nx, _ny
  As these forms are processed using overloaded operators, be cautious with the C++ operator priority.
  In doubt, use paranthesis.

  Note that some compatibility tests are performed but not all possible.
  Generally, structure (scalar, vector or matrix) consistancy is checked but not the dimension consistancy!
  Therefore, it is the responsability of the user to insure the consistancy of his expression
 */

#ifndef OPERATOR_ON_KERNEL_HPP
#define OPERATOR_ON_KERNEL_HPP

#include "config.h"
#include "DifferentialOperator.hpp"
#include "OperatorOnFunction.hpp"
#include "utils.h"
#include "space.h"

namespace xlifepp
{

/*!
   \class OperatorOnKernel
   describes the operations to process on kernel.
   ker_p=nullptr means K(x,y)=1 !
*/
class OperatorOnKernel
{
  protected:
    const Kernel* ker_p;               //!< Kernel involved in operator
    DifferentialOperator* xdifOp_p;    //!< x differential operator involved in operator
    DifferentialOperator* ydifOp_p;    //!< y differential operator involved in operator
    DifferentialOperator* xydifOp_p;   //!< x,y differential operator involved in operator (e.g _nxdotny_times)

  public:
    ValueType type_;                    //!< type of returned value (one among _real, _complex)
    StrucType struct_;                  //!< structure of returned value (one among _scalar, _vector, _matrix)
    dimPair dimsRes_;                   //!< dimension of result, (0,0) if unset (NOT FULL MANAGED)
    Extension* ext_p;                   //!< extension operator to apply to
    bool in_ext;                        //!< if true interpreted as ext(difop(f)) else interpreted as difop(ext(f)); default is true

    mutable bool conjugate_;            //!< temporary flag for conjugate operation construction
    mutable bool transpose_;            //!< temporary flag for transpose operation construction
    mutable bool noUpdatedNormal;       //!< true means that the normal is not available in Kernel parameters
    // as a consequence, ndotgradx, ndotgrady are not used in evaluation, address mainly parallel computation

    // constructors/destructor
    OperatorOnKernel()                                                         //! default constructor
      : ker_p(nullptr), xdifOp_p(nullptr),ydifOp_p(nullptr), xydifOp_p(nullptr), type_(_real), struct_(_scalar), dimsRes_(dimPair(0,0)),
        ext_p(nullptr), in_ext(true), conjugate_(false), transpose_(false), noUpdatedNormal(false)
    {DifferentialOperator* d=findDifferentialOperator(_id); xdifOp_p=d; ydifOp_p=d; xydifOp_p=d;}
    OperatorOnKernel(const Kernel*, DiffOpType = _id, DiffOpType = _id,
                     ValueType vt=_real, StrucType st=_scalar,
                     dimPair= dimPair(1,1));                                   //!< basic constructor
    OperatorOnKernel(const Kernel*, DiffOpType, DiffOpType, DiffOpType,
                     ValueType vt=_real, StrucType st=_scalar,
                     dimPair= dimPair(1,1));                                   //!< basic constructor
    OperatorOnKernel(const Kernel&, DiffOpType = _id, DiffOpType = _id);       //!< basic constructor
    OperatorOnKernel(const OperatorOnKernel&);                                 //!< copy constructor
    OperatorOnKernel& operator=(const OperatorOnKernel&);                      //!< assignment operator
    void setDims();
    ~OperatorOnKernel();                                                       //!< destructor

    //accessors
    const Kernel* kernelp() const {return ker_p;}              //!< return the Kernel pointer
    SingularityType singularType() const                       //! returns type of singularity (_notsingular, _r, _logr,_loglogr)
    {if(ker_p!=nullptr) return ker_p->singularType; return _notsingular;}
    real_t singularOrder() const                               //! returns singularity order (0 if not singular)
    {if(ker_p!=nullptr) return ker_p->singularOrder; return 0.;}
    const string_t& kernelName() const                        //! returns kernel shortname
    {if(ker_p!=nullptr) return ker_p->shortname; return *new string_t("");}
    DiffOpType xdifOpType() const {return xdifOp_p->type();}   //!< return type of x differential operator
    DiffOpType ydifOpType() const {return ydifOp_p->type();}   //!< return type of y differential operator
    DiffOpType xydifOpType() const {return xydifOp_p->type();} //!< return type of y differential operator
    DifferentialOperator*& xdifOp_() {return xdifOp_p;}        //!< return pointer to differential operator
    DifferentialOperator& xdifOp() const {return *xdifOp_p;}   //!< return reference to differential operator
    DifferentialOperator*& ydifOp_() {return ydifOp_p;}        //!< return pointer to differential operator
    DifferentialOperator& ydifOp() const {return *ydifOp_p;}   //!< return reference to differential operator
    DifferentialOperator*& xydifOp_() {return xydifOp_p;}      //!< return pointer to differential operator
    DifferentialOperator& xydifOp() const {return *xydifOp_p;} //!< return reference to differential operator
    bool isId() const
    {return xdifOp_p->type()==_id && ydifOp_p->type()==_id && xydifOp_p->type()==_id;}
    ValueType& valueType() {return type_;}                     //!< return the operator returned type
    ValueType valueType()const {return type_;}                 //!< return the operator returned type
    StrucType& strucType() {return struct_;}                   //!< return the operator returned structure
    StrucType strucType() const {return struct_;}              //!< return the operator returned structure
    dimPair dims() const {return dimsRes_;}                    //!< return the dimension of returned values
    number_t xdiffOrder() const                                //! order of differential operator
    {return xdifOp_p->order();}
    number_t ydiffOrder() const                                //! order of differential operator
    {return ydifOp_p->order();}
    bool voidKernel() const                                    //! returns true if kernel pointer is null
    {return ker_p==nullptr;}

    bool xnormalRequired() const                               //! true if x-normal involved
    {return xdifOp_p->normalRequired() || xydifOp_p->normalRequired() || (ker_p!=nullptr && ker_p->requireNx);}
    bool ynormalRequired() const                               //! true if y-normal involved
    {return ydifOp_p->normalRequired() || xydifOp_p->normalRequired() || (ker_p!=nullptr && ker_p->requireNy) ;}
    bool normalRequired() const                                //! true if x or y normals involved
    {return xdifOp_p->normalRequired() || ydifOp_p->normalRequired() || xydifOp_p->normalRequired()
        || (ker_p!=nullptr && (ker_p->requireNx ||  ker_p->requireNy));}
    bool xtangentRequired() const                               //! true if x-tangent involved
    {return xdifOp_p->tangentRequired() || xydifOp_p->tangentRequired() || (ker_p!=nullptr && ker_p->requireTx);}
    bool ytangentRequired() const                               //! true if y-tangent involved
    {return ydifOp_p->tangentRequired() || xydifOp_p->tangentRequired() || (ker_p!=nullptr && ker_p->requireTy) ;}
    bool tangentRequired() const                                //! true if x or y tangents involved
    {return xdifOp_p->tangentRequired() || ydifOp_p->tangentRequired() || xydifOp_p->tangentRequired()
        || (ker_p!=nullptr && (ker_p->requireTx ||  ker_p->requireTy));}
    bool elementRequired() const                                //! true if geom element is required by kernel
    {return ker_p!=nullptr && ker_p->requireElt;}

    void setX(const Point& P) const                            //! set X point (usefull for Kernel)
    {if(ker_p!=nullptr) ker_p->setX(P);}
    void setY(const Point& P) const                            //! set Y point (usefull for Kernel)
    {if(ker_p!=nullptr) ker_p->setY(P);}
    void changeKernel(Kernel* ker)                             //! change Kernel in operator
    {ker_p=ker;}
    number_t extensionOrder() const                            //! derivative order in extension
    {
      number_t ordx = xdifOp_p->order(), ordy = ydifOp_p->order(), ordxy = xydifOp_p->order();
      if(ext_p==nullptr || in_ext || (ordx==0 && ordy==0 && ordxy==0)
          || (ordx>0 && ext_p->var_==_y) || (ordy>0 && ext_p->var_==_x)) return 0;
      else return 1;
    }

    // general evaluation of operator on kernel (scalar, vector and matrix) return always a vector
    template <typename T>
    Vector<T>& eval(const Point&, const Point&, Vector<T>&,
                    const Vector<real_t>* = 0, const Vector<real_t>* = 0,
                    dimPair* =nullptr, const ExtensionData* extdata =nullptr) const;      //!< evaluate vector kernel operator at (x,y)

    // particular cases for scalar kernel
    template <typename T>
    T& eval(const Point&,const Point&, T&,
            const Vector<real_t>* = 0, const Vector<real_t>* = 0) const;      //!< evaluate scalar kernel operator at (x,y)
    template <typename T>
    T& eval(const Point&, T&,
            const Vector<real_t>* = 0, const Vector<real_t>* = 0) const;       //!< evaluate kernel operator at (x,P) or (P,x)
    template <typename T>
    std::vector<T>& eval(const std::vector<Point>&, const std::vector<Point>&, std::vector<T>&, number_t =0,
                         const std::vector<Vector<real_t> >* =nullptr, const std::vector<Vector<real_t> >* =nullptr) const; //!< evaluate kernel operator at (xi,yj)
    template <typename T>
    std::vector<Vector<T> >& eval(const std::vector<Point>&, const std::vector<Point>&, std::vector<Vector<T> >&, number_t =0,
                                  const std::vector<Vector<real_t> >* =nullptr, const std::vector<Vector<real_t> >* =nullptr) const; //!< evaluate vector kernel operator at (xi,yj)
    template <typename T>
    Matrix<T>& eval(const Point&, const Point&, Matrix<T>&,
                    const Vector<real_t>* = 0, const Vector<real_t>* = 0) const;      //!< evaluate matrix kernel operator at (x,y)
    template <typename T>
    std::vector<Matrix<T> >& eval(const std::vector<Point>&, const std::vector<Point>&, std::vector<Matrix<T> >&, number_t =0,
                                  const std::vector<Vector<real_t> >* =nullptr, const std::vector<Vector<real_t> >* =nullptr) const; //!< evaluate vector kernel operator at (xi,yj)

    const OperatorOnFunction& operator()(const Point&, VariableName) const; //!<interpret operator on kernel as operator on y function
    const OperatorOnFunction& operator()(VariableName, const Point&) const; //!<interpret operator on kernel as operator on x function
    const OperatorOnFunction& operator()(VariableName) const;               //!<interpret operator on kernel as operator on variableName function

    void print(std::ostream&) const;     //!< print on stream attributes
    void print(PrintStream& os) const {print(os.currentStream());}
    string_t asString() const;           //!< return as symbolic string

    friend bool operator==(const OperatorOnKernel&, const OperatorOnKernel&);
    friend bool operator!=(const OperatorOnKernel&, const OperatorOnKernel&);
}; // end of class OperatorOnKernel

std::ostream& operator<<(std::ostream& os, const OperatorOnKernel& opk);
bool operator==(const OperatorOnKernel&, const OperatorOnKernel&);  //!< same operator on kernel
bool operator!=(const OperatorOnKernel&, const OperatorOnKernel&);  //!< different operator on kernel

//---------------------------------------------------------------------------------------
//  external functions
//---------------------------------------------------------------------------------------
OperatorOnKernel& id(const Kernel&);                           //!< id(k)
OperatorOnKernel& grad_x(const Kernel&);                       //!< grad_x(k)
OperatorOnKernel& grad_y(const Kernel&);                       //!< grad_y(k)
OperatorOnKernel& nabla_x(const Kernel&);                      //!< grad_x(k)
OperatorOnKernel& nabla_y(const Kernel&);                      //!< grad_y(k)
OperatorOnKernel& div_x(const Kernel&);                        //!< grad_x(k)
OperatorOnKernel& div_y(const Kernel&);                        //!< grad_y(k)
OperatorOnKernel& curl_x(const Kernel&);                       //!< curl_x(k)
OperatorOnKernel& curl_y(const Kernel&);                       //!< curl_y(k)
OperatorOnKernel& rot_x(const Kernel&);                        //!< curl_x(k)
OperatorOnKernel& rot_y(const Kernel&);                        //!< curl_y(k)
OperatorOnKernel& trac_x(const Kernel&);                       //!< trac_x(k)
OperatorOnKernel& trac_y(const Kernel&);                       //!< trac_y(k)
OperatorOnKernel& ntimes_x(const Kernel&);                     //!< nx*k
OperatorOnKernel& ntimes_y(const Kernel&);                     //!< ny*k
OperatorOnKernel& ndot_x(const Kernel&);                       //!< nx|k
OperatorOnKernel& ndot_y(const Kernel&);                       //!< ny|k
OperatorOnKernel& ncross_x(const Kernel&);                     //!< nx^k
OperatorOnKernel& ncross_y(const Kernel&);                     //!< ny^k
OperatorOnKernel& ncrossncross_x(const Kernel&);               //!< nx^nx^k
OperatorOnKernel& ncrossncross_y(const Kernel&);               //!< ny^ny^k
OperatorOnKernel& ndotgrad_x(const Kernel&);                   //!< nx|grad_x(k)
OperatorOnKernel& ndotgrad_y(const Kernel&);                   //!< ny|grad_y(k)
OperatorOnKernel& ndiv_x(const Kernel&);                       //!< nx*div_x(k)
OperatorOnKernel& ndiv_y(const Kernel&);                       //!< ny*div_y(k)
OperatorOnKernel& ncrosscurl_x(const Kernel&);                 //!< nx^curl_x(k)
OperatorOnKernel& ncrosscurl_y(const Kernel&);                 //!< ny^curl_y(k)
OperatorOnKernel& ncrossrot_x(const Kernel&);                  //!< nx^curl_x(k)
OperatorOnKernel& ncrossrot_y(const Kernel&);                  //!< ny^curl_y(k)
OperatorOnKernel& nxdotny_times(const Kernel&);                //!< (nx.ny)*k
OperatorOnKernel& nxcrossny_dot(const Kernel&);                //!< (nx^ny).k
OperatorOnKernel& nycrossnx_dot(const Kernel&);                //!< (ny^nx).k
OperatorOnKernel& nxcrossny_cross(const Kernel&);              //!< (nx^ny)^k
OperatorOnKernel& nycrossnx_cross(const Kernel&);              //!< (ny^ny)^k
OperatorOnKernel& nxtensornxtimesNGr(const Kernel&);        //!< (nxtensornx)*Id(k)
OperatorOnKernel& nxtensornxtimestrac_y(const Kernel&);        //!< (nxtensornx)*trac_y(k)

OperatorOnKernel& operator*(UnitaryVector, const Kernel&);     //!< n*ker same as ntimes(ker)
OperatorOnKernel& operator*(const Kernel&, UnitaryVector);     //!< ker*n same as ntimes(ker)
OperatorOnKernel& operator|(UnitaryVector, const Kernel&);     //!< n|ker same as ndot(ker)
OperatorOnKernel& operator|(const Kernel&, UnitaryVector);     //!< ker|n same as ndot(ker)
OperatorOnKernel& operator^(UnitaryVector, const Kernel&);     //!< n^ker same as ncross(ker)

OperatorOnKernel& id(OperatorOnKernel&);                           //!< id(opk)
OperatorOnKernel& grad_x(OperatorOnKernel&);                       //!< grad_x(opk)
OperatorOnKernel& grad_y(OperatorOnKernel&);                       //!< grad_y(opk)
OperatorOnKernel& nabla_x(OperatorOnKernel&);                      //!< grad_x(opk)
OperatorOnKernel& nabla_y(OperatorOnKernel&);                      //!< grad_y(opk)
OperatorOnKernel& div_x(OperatorOnKernel&);                        //!< grad_x(opk)
OperatorOnKernel& div_y(OperatorOnKernel&);                        //!< grad_y(opk)
OperatorOnKernel& curl_x(OperatorOnKernel&);                       //!< curl_x(opk)
OperatorOnKernel& curl_y(OperatorOnKernel&);                       //!< curl_y(opk)
OperatorOnKernel& rot_x(OperatorOnKernel&);                        //!< curl_x(opk)
OperatorOnKernel& rot_y(OperatorOnKernel&);                        //!< curl_y(opk)
OperatorOnKernel& trac_x(OperatorOnKernel&);                       //!< trac_x(opk)
OperatorOnKernel& trac_y(OperatorOnKernel&);                       //!< trac_y(opk)
OperatorOnKernel& ntimes_x(OperatorOnKernel&);                     //!< nx*opk
OperatorOnKernel& ntimes_y(OperatorOnKernel&);                     //!< ny*opk
OperatorOnKernel& ndot_x(OperatorOnKernel&);                       //!< nx|opk
OperatorOnKernel& ndot_y(OperatorOnKernel&);                       //!< ny|opk
OperatorOnKernel& ncross_x(OperatorOnKernel&);                     //!< nx^opk
OperatorOnKernel& ncross_y(OperatorOnKernel&);                     //!< ny^opk
OperatorOnKernel& ncrossncross_x(OperatorOnKernel&);               //!< nx^nx^opk
OperatorOnKernel& ncrossncross_y(OperatorOnKernel&);               //!< ny^ny^opk
OperatorOnKernel& ndotgrad_x(OperatorOnKernel&);                   //!< nx|grad_x(opk)
OperatorOnKernel& ndotgrad_y(OperatorOnKernel&);                   //!< ny|grad_y(opk)
OperatorOnKernel& ndiv_x(OperatorOnKernel&);                       //!< nx*div_x(opk)
OperatorOnKernel& ndiv_y(OperatorOnKernel&);                       //!< ny*div_y(opk)
OperatorOnKernel& ncrosscurl_x(OperatorOnKernel&);                 //!< nx^curl_x(opk)
OperatorOnKernel& ncrosscurl_y(OperatorOnKernel&);                 //!< ny^curl_y(opk)
OperatorOnKernel& ncrossrot_x(OperatorOnKernel&);                  //!< nx^curl_x(opk)
OperatorOnKernel& ncrossrot_y(OperatorOnKernel&);                  //!< ny^curl_y(opk)

OperatorOnKernel& operator*(UnitaryVector, OperatorOnKernel&);     //!< n*opker
OperatorOnKernel& operator*(OperatorOnKernel&, UnitaryVector);     //!< opker*n
OperatorOnKernel& operator|(UnitaryVector, OperatorOnKernel&);     //!< n|opker
OperatorOnKernel& operator|(OperatorOnKernel&, UnitaryVector);     //!< opker|n
OperatorOnKernel& operator^(UnitaryVector, OperatorOnKernel&);     //!< n^opker

OperatorOnKernel& nxdotny_times(OperatorOnKernel&);                //!< (nx.ny)*opk
OperatorOnKernel& nxcrossny_dot(OperatorOnKernel&);                //!< (nx^ny).opk
OperatorOnKernel& nycrossnx_dot(OperatorOnKernel&);                //!< (ny^nx).opk
OperatorOnKernel& nxcrossny_cross(OperatorOnKernel&);              //!< (nx^ny)^opk
OperatorOnKernel& nycrossnx_cross(OperatorOnKernel&);              //!< (ny^nx)^opk

// set to true or false the temporary conjugate flag
OperatorOnKernel& conj(Kernel&);              //!< conjugate k
OperatorOnKernel& tran(Kernel&);              //!< transpose k
OperatorOnKernel&  adj(Kernel&);              //!< conjugate and transpose k
OperatorOnKernel& conj(OperatorOnKernel&);    //!< conjugate opk
OperatorOnKernel& tran(OperatorOnKernel&);    //!< transpose opk
OperatorOnKernel& adj(OperatorOnKernel&);     //!< conjugate and transpose opk

//special cases involving Extension
OperatorOnKernel& operator*(const Extension&, const Kernel&);      //!< extension of Kernel
OperatorOnKernel& operator*(const Extension&, OperatorOnKernel&);  //!< extension of OperatorOnKernel



// ------------------------------------------------------------------------------------------------------------------------------------
/*! general function evaluating operator on kernel,
    output may be either a scalar, a vector or a matrix but it is always returned as a vector
      scalar output: set opk[0] = out
      vector output: set opk[i] = out[i] 0<i<n
      matrix output: set opk[i*n+j]=out[i,j], 0<i<m; 0<j<n

      x, y: points where current operator on kernel is evaluated
      opk: return value of opk as a vector (may be resized here)
      dims: dimension of the output  (m=n=1 if scalar, n=1 if a vector), computed if pointer is not 0
      nxp, nyp: pointers to normal vectors (0 if not allocated)
      extdata: data related to extension if required, used if not 0
*/
// ------------------------------------------------------------------------------------------------------------------------------------
template <typename T>
Vector<T>& OperatorOnKernel::eval(const Point& x,const Point& y, Vector<T>& opk,
                                  const Vector<real_t>* nxp, const Vector<real_t>* nyp,
                                  dimPair* dims, const ExtensionData* extdata) const
{
  if(ker_p==nullptr)   //no kernel pointer, ker=1
    {
      opk.resize(1);
      opk[0]=T(1);
      return opk;
    }

  opk.resize(dimsRes_.first*dimsRes_.second);
  opk*=0;
  ValueType vto = xlifepp::valueType(opk);
  if(ext_p==nullptr || extdata==nullptr)   //no extension
    {
      const Function& ker = ker_p->kernel;
      switch(ker.returnedStruct_)
        {
          case _scalar: //call scalar kernel case
            {
              switch(xdifOpType())
                {
                  case _id:
                    {
                      switch(ydifOpType())
                        {
                          case _id:
                            if(ker_p->valueType()!=vto && vto==_complex) //force calling ker in real
                              {
                                real_t res;
                                ker_p->kernel(x,y,res);
                                opk[0]=res;
                              }
                            else ker_p->kernel(x,y,opk[0]);
                            break;
                          case _grad_y:
                            if(ker_p->grady.valueType()!=vto && vto==_complex) //force calling ker in real
                              {
                                Vector<real_t> res(opk.size());
                                ker_p->grady(x,y,res);
                                opk=res;
                              }
                            else  ker_p->grady(x,y,opk);
                            break;
                          case _ndotgrad_y:
                            {
                              if(noUpdatedNormal || ker_p->no_ndotgrady())
                                {
                                  if(nyp==nullptr)
                                    {
                                      where("OperatorOnKernel::eval(...)");
                                      error("null_pointer","normal");
                                    }
                                  if(ker_p->grady.valueType()!=vto && vto==_complex) //force calling ker in real
                                    {
                                      Vector<real_t> gky;
                                      ker_p->grady(x,y,gky);
                                      opk[0]=T(0);
                                      Vector<real_t>::const_iterator itn=nyp->begin();
                                      typename Vector<real_t>::iterator itg=gky.begin();
                                      for(; itg!=gky.end(); itg++, itn++) opk[0] += *itg * T(*itn);
                                    }
                                  else
                                    {
                                      Vector<T> gky;
                                      ker_p->grady(x,y,gky);
                                      opk[0]=T(0);
                                      Vector<real_t>::const_iterator itn=nyp->begin();
                                      typename Vector<T>::iterator itg=gky.begin();
                                      for(; itg!=gky.end(); itg++, itn++) opk[0] += *itg * T(*itn);
                                    }
                                }
                              else
                                {
                                  if(ker_p->grady.valueType()!=vto && vto==_complex) //force calling ker in real
                                    {
                                      real_t res;
                                      ker_p->ndotgrady(x,y,res);
                                      opk[0]=res;
                                    }
                                  else ker_p->ndotgrady(x,y,opk[0]);
                                }
                            }
                            break;
                          default:
                            where("OperatorOnKernel::eval(...)");
                            error("operator_unexpected",words("diffop",ydifOpType()));
                        }
                    }
                    break;

                  case _grad_x:
                    switch(ydifOpType())
                      {
                        case _id:
                          if(ker_p->gradx.valueType()!=vto && vto==_complex) //force calling ker in real
                            {
                              Vector<real_t> res(opk.size());
                              ker_p->gradx(x,y,res);
                              opk=res;
                            }
                          else ker_p->gradx(x,y,opk);
                          break;
                        case _grad_y:
                          if(ker_p->gradx.valueType()!=vto && vto==_complex) //force calling ker in real
                            {
                              Matrix<real_t> mat;
                              ker_p->gradxy(x,y,mat);
                              typename Matrix<real_t>::iterator itm=mat.begin();
                              typename Vector<T>::iterator itv=opk.begin();
                              for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                            }
                          else
                            {
                              Matrix<T> mat;
                              ker_p->gradxy(x,y,mat);
                              typename Matrix<T>::iterator itm=mat.begin();
                              typename Vector<T>::iterator itv=opk.begin();
                              for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                            }
                          break;

                        case _ndotgrad_y: // grad_x(grad_y|ny)b -> vector
                          {
                            if(nyp==nullptr)
                              {
                                where("OperatorOnKernel::eval(...)");
                                error("null_pointer","x-normal");
                              }
                            if(ker_p->gradxy.valueType()!=vto && vto==_complex) //force calling ker in real
                              {
                                Matrix<real_t> mkxy;
                                opk = * nyp * ker_p->gradxy(x,y,mkxy);
                              }
                            else
                              {
                                Matrix<T> mkxy;
                                opk = * nyp * ker_p->gradxy(x,y,mkxy);
                              }
                            break;
                          }
                        default:
                          where("OperatorOnKernel::eval(...)");
                          error("operator_unexpected",words("diffop",ydifOpType()));
                      }
                    break;

                  case _ndotgrad_x:
                    switch(ydifOpType())
                      {
                        case _id:
                          if(noUpdatedNormal || ker_p->no_ndotgradx())
                            {
                              if(nxp==nullptr)
                                {
                                  where("OperatorOnKernel::eval(...)");
                                  error("null_pointer","x-normal");
                                }
                              opk[0]=T(0);
                              if(ker_p->gradx.valueType()!=vto && vto==_complex) //force calling ker in real
                                {
                                  Vector<real_t> gkx;
                                  ker_p->gradx(x,y,gkx);
                                  Vector<real_t>::const_iterator itn=nxp->begin();
                                  typename Vector<real_t>::iterator itg=gkx.begin();
                                  for(; itg!=gkx.end(); itg++, itn++) opk[0] += *itg * T(*itn);

                                }
                              else
                                {
                                  Vector<T> gkx;
                                  ker_p->gradx(x,y,gkx);
                                  Vector<real_t>::const_iterator itn=nxp->begin();
                                  typename Vector<T>::iterator itg=gkx.begin();
                                  for(; itg!=gkx.end(); itg++, itn++) opk[0] += *itg * T(*itn);
                                }
                            }
                          else
                            {
                              if(ker_p->ndotgradx.valueType()!=vto && vto==_complex) //force calling ker in real
                                {
                                  real_t res;
                                  ker_p->ndotgradx(x,y,res);
                                  opk[0]=res;
                                }
                              else ker_p->ndotgradx(x,y,opk[0]);
                            }
                          break;
                        case _grad_y: // grad_y(grad_x|nx)b -> vector
                          {
                            if(nxp==nullptr)
                              {
                                where("OperatorOnKernel::eval(...)");
                                error("null_pointer","x-normal");
                              }
                            if(ker_p->gradxy.valueType()!=vto && vto==_complex) //force calling ker in real
                              {
                                Matrix<real_t> mkxy;
                                opk = ker_p->gradxy(x,y,mkxy)** nxp;
                              }
                            else
                              {
                                Matrix<T> mkxy;
                                opk = ker_p->gradxy(x,y,mkxy)** nxp;
                              }
                          }
                          break;
                        case _ndotgrad_y:
                          {
                            if(nxp==nullptr || nyp==nullptr)
                              {
                                where("OperatorOnKernel::eval(...)");
                                error("null_pointer","normal");
                              }
                            if(ker_p->gradxy.valueType()!=vto && vto==_complex) //force calling ker in real
                              {
                                Matrix<real_t> mkxy;
                                Vector<real_t> dx=ker_p->gradxy(x,y,mkxy)** nxp;
                                opk[0]=complexToT<T>(dotRC(dx, *nyp));
                              }
                            else
                              {
                                Matrix<T> mkxy;
                                Vector<T> dx=ker_p->gradxy(x,y,mkxy)** nxp;
                                opk[0]=complexToT<T>(dotRC(dx, *nyp));
                              }
                          }
                          break;
                        default:
                          where("OperatorOnKernel::eval(...)");
                          error("operator_unexpected",words("diffop",ydifOpType()));
                      }
                    break;

                  default:
                    where("OperatorOnKernel::eval(...)");
                    error("operator_unexpected",words("diffop",xdifOpType()));
                }

              if(xydifOpType()==_id) return opk;

              //xyDifop to apply
              switch(xydifOpType())
                {
                  case _nxdotny_times:
                    {
                      if(nxp==nullptr)
                        {
                          where("OperatorOnKernel::eval(...)");
                          error("null_pointer","x-normal");
                        }
                      if(nyp==nullptr)
                        {
                          where("OperatorOnKernel::eval(...)");
                          error("null_pointer","y-normal");
                        }
                      opk[0]*=dot(*nxp,*nyp);
                      return opk;
                    }
                    break;
                  default:
                    where("OperatorOnKernel::eval(...)");
                    error("operator_unexpected",words("diffop",xydifOpType()));
                }
            }
          case _matrix:
            {
              switch(xdifOpType())
                {
                  case _id:
                    switch(ydifOpType())
                      {
                        case _id:
                          {
                            if(ker.valueType()!=vto && vto==_complex) //force calling ker in real
                              {
                                Matrix<real_t> mat;
                                ker(x,y,mat);
                                if(transpose_) mat.transpose();
                                opk.resize(mat.size());
                                typename Matrix<real_t>::iterator itm=mat.begin();
                                typename Vector<T>::iterator itv=opk.begin();
                                for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                if(dims!=nullptr) *dims=mat.dims();
                              }
                            else
                              {
                                Matrix<T> mat;
                                ker(x,y,mat);
                                if(transpose_) mat.transpose();
                                opk.resize(mat.size());
                                typename Matrix<T>::iterator itm=mat.begin();
                                typename Vector<T>::iterator itv=opk.begin();
                                for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                if(dims!=nullptr) *dims=mat.dims();
                              }
                            break;
                          }
                          case _nxtensornxtimesNGr:
                             {
                              if(ker_p->nxtensornxtimesNGr.valueType()!=vto && vto==_complex) //force calling ker->tracy in real
                              {
                                Matrix<real_t> mat;
                                ker_p->nxtensornxtimesNGr(x,y,mat);
                                if(transpose_) mat.transpose();
                                opk.resize(mat.size());
                                typename Matrix<real_t>::iterator itm=mat.begin();
                                typename Vector<T>::iterator itv=opk.begin();
                                for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                if(dims!=nullptr) *dims=mat.dims();
                              }
                             else
                              {
                                Matrix<T> mat;
                                ker_p->nxtensornxtimesNGr(x,y,mat);
                                if(transpose_) mat.transpose();
                                opk.resize(mat.size());
                                typename Matrix<T>::iterator itm=mat.begin();
                                typename Vector<T>::iterator itv=opk.begin();
                                for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                if(dims!=nullptr) *dims=mat.dims();
                              }
                            break;
                            }
                         case _div_y:  //vector of size n: [div_y K.1, div_y K.2, ..., div_y K.n]
                          {
                            if(ker_p->divy.valueType()!=vto && vto==_complex) //force calling ker->divy in real
                              {
                                Vector<real_t> ropk;
                                ker_p->divy(x,y,ropk);
                                opk=ropk;
                              }
                            else ker_p->divy(x,y,opk);
                            if(dims!=nullptr) *dims=dimPair(opk.size(),1);
                            break;
                          }
                        case _curl_y:  //matrix of size m x n: [curl_y K.1, curl_y K.2, ..., curl_y K.n]
                          {
                            if(ker_p->curly.valueType()!=vto && vto==_complex) //force calling ker->curly in real
                              {
                                Matrix<real_t> mat;
                                ker_p->curly(x,y,mat);
                                if(transpose_) mat.transpose();
                                opk.resize(mat.size());
                                typename Matrix<real_t>::iterator itm=mat.begin();
                                typename Vector<T>::iterator itv=opk.begin();
                                for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                if(dims!=nullptr) *dims=mat.dims();

                              }
                            else
                              {
                                Matrix<T> mat;
                                ker_p->curly(x,y,mat);
                                if(transpose_) mat.transpose();
                                opk.resize(mat.size());
                                typename Matrix<T>::iterator itm=mat.begin();
                                typename Vector<T>::iterator itv=opk.begin();
                                for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                if(dims!=nullptr) *dims=mat.dims();
                              }
                            break;
                          }
                        case _trac_y:  //matrix of size m x n: [trac_y K.1, trac_y K.2, ..., trac_y K.n]
                         {
                             if(ker_p->tracy.valueType()!=vto && vto==_complex) //force calling ker->tracy in real
                                {
                                  Matrix<real_t> mat;
                                  ker_p->tracy(x,y,mat);
                                  if(transpose_) mat.transpose();
                                  opk.resize(mat.size());
                                  typename Matrix<real_t>::iterator itm=mat.begin();
                                  typename Vector<T>::iterator itv=opk.begin();
                                  for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                  if(dims!=nullptr) *dims=mat.dims();

                                }
                              else
                                {
                                  Matrix<T> mat;
                                  ker_p->tracy(x,y,mat);
                                  if(transpose_) mat.transpose();
                                  opk.resize(mat.size());
                                  typename Matrix<T>::iterator itm=mat.begin();
                                  typename Vector<T>::iterator itv=opk.begin();
                                  for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                  if(dims!=nullptr) *dims=mat.dims();
                                }
                             break;
                           }
                           case _nxtensornxtimestrac_y:
                          {
                              if(ker_p->nxtensornxtimestracy.valueType()!=vto && vto==_complex) //force calling ker->tracy in real
                              {
                                Matrix<real_t> mat;
                                ker_p->nxtensornxtimestracy(x,y,mat);
                                if(transpose_) mat.transpose();
                                opk.resize(mat.size());
                                typename Matrix<real_t>::iterator itm=mat.begin();
                                typename Vector<T>::iterator itv=opk.begin();
                                for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                if(dims!=nullptr) *dims=mat.dims();
                              }
                             else
                              {
                                Matrix<T> mat;
                                ker_p->nxtensornxtimestracy(x,y,mat);
                                if(transpose_) mat.transpose();
                                opk.resize(mat.size());
                                typename Matrix<T>::iterator itm=mat.begin();
                                typename Vector<T>::iterator itv=opk.begin();
                                for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                if(dims!=nullptr) *dims=mat.dims();
                              }
                            break;
                            }
                        default:
                          where("OperatorOnKernel::eval(...)");
                          error("operator_unexpected",words("diffop",ydifOpType()));
                      } //end of switch (ydifOpType()) of case id_x
                    break;

                  case _div_x:
                    switch(ydifOpType())
                      {
                        case _id: //vector of size n: [div_x K.1, div_x K.2, ..., div_x K.n]
                          {
                            if(ker_p->divx.valueType()!=vto && vto==_complex) //force calling ker->divx in real
                              {
                                Vector<real_t> ropk;
                                ker_p->divx(x,y,ropk);
                                opk=ropk;
                              }
                            else ker_p->divx(x,y,opk);
                            if(dims!=nullptr) *dims=dimPair(opk.size(),1);
                            break;
                          }
                        case _div_y:  //scalar: div_x [div_y K.1, div_y K.2, ..., div_y K.n]
                          {
                            opk.resize(1);
                            if(ker_p->divxy.valueType()!=vto && vto==_complex) //force calling ker->divxy in real
                              {
                                real_t ropk;
                                ker_p->divxy(x,y,ropk);
                                opk[0]=ropk;
                              }
                            else ker_p->divxy(x,y,opk[0]);
                            if(dims!=nullptr) *dims=dimPair(1,1);
                            break;
                          }
                        default:
                          where("OperatorOnKernel::eval(...)");
                          error("operator_unexpected",words("diffop",ydifOpType()));
                      } //end of switch (ydifOpType()) of case div_x
                    break;

                  case _curl_x:
                    switch(ydifOpType())
                      {
                        case _id: //matrix of size m x n: [curl_x K.1, curl_x K.2, ..., curl_x K.n]
                          {
                            if(ker_p->curlx.valueType()!=vto && vto==_complex) //force calling ker->curlx in real
                              {
                                Matrix<real_t> mat;
                                ker_p->curlx(x,y,mat);
                                if(transpose_) mat.transpose();
                                opk.resize(mat.size());
                                typename Matrix<real_t>::iterator itm=mat.begin();
                                typename Vector<T>::iterator itv=opk.begin();
                                for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                if(dims!=nullptr) *dims=mat.dims();
                              }
                            else
                              {
                                Matrix<T> mat;
                                ker_p->curlx(x,y,mat);
                                if(transpose_) mat.transpose();
                                opk.resize(mat.size());
                                typename Matrix<T>::iterator itm=mat.begin();
                                typename Vector<T>::iterator itv=opk.begin();
                                for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                if(dims!=nullptr) *dims=mat.dims();
                              }
                            break;
                          }
                        case _curl_y:  //scalar: curl_x [curl_y K.1, div_y K.2, ..., div_y K.n]
                          {
                            if(ker_p->curlxy.valueType()!=vto && vto==_complex) //force calling ker->curlxy in real
                              {
                                Matrix<real_t> mat;
                                ker_p->curlxy(x,y,mat);
                                if(transpose_) mat.transpose();
                                opk.resize(mat.size());
                                typename Matrix<real_t>::iterator itm=mat.begin();
                                typename Vector<T>::iterator itv=opk.begin();
                                for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                if(dims!=nullptr) *dims=mat.dims();

                              }
                            else
                              {
                                Matrix<T> mat;
                                ker_p->curlxy(x,y,mat);
                                if(transpose_) mat.transpose();
                                opk.resize(mat.size());
                                typename Matrix<T>::iterator itm=mat.begin();
                                typename Vector<T>::iterator itv=opk.begin();
                                for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                if(dims!=nullptr) *dims=mat.dims();
                              }
                            break;
                          }
                        default:
                          where("OperatorOnKernel::eval(...)");
                          error("operator_unexpected",words("diffop",ydifOpType()));
                      } //end of switch (ydifOpType()) of case curl_x
                    break;
                   case _trac_x:
                          switch(ydifOpType())
                      {
                          case _id: //matrix of size m x n: [curl_x K.1, curl_x K.2, ..., curl_x K.n]
                          {
                              if(ker_p->tracx.valueType()!=vto && vto==_complex) //force calling ker->tracx in real
                              {
                                  Matrix<real_t> mat;
                                  ker_p->tracx(x,y,mat);
                                  if(transpose_) mat.transpose();
                                  opk.resize(mat.size());
                                  typename Matrix<real_t>::iterator itm=mat.begin();
                                  typename Vector<T>::iterator itv=opk.begin();
                                  for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                  if(dims!=nullptr) *dims=mat.dims();
                              }
                              else
                              {
                                  Matrix<T> mat;
                                  ker_p->tracx(x,y,mat);
                                  if(transpose_) mat.transpose();
                                  opk.resize(mat.size());
                                  typename Matrix<T>::iterator itm=mat.begin();
                                  typename Vector<T>::iterator itv=opk.begin();
                                  for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                  if(dims!=nullptr) *dims=mat.dims();
                              }
                              break;
                          }
                          case _trac_y:  //scalar: trac_x [trac_y K.1, trac_y K.2, ..., trac_y K.n]
                          {
                               if(ker_p->tracxy.valueType()!=vto && vto==_complex) //force calling ker->curlxy in real
                              {
                                  Matrix<real_t> mat;
                                  ker_p->tracxy(x,y,mat);
                                  if(transpose_) mat.transpose();
                                  opk.resize(mat.size());
                                  typename Matrix<real_t>::iterator itm=mat.begin();
                                  typename Vector<T>::iterator itv=opk.begin();
                                  for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                  if(dims!=nullptr) *dims=mat.dims();
                              }
                              else
                              {
                                  Matrix<T> mat;
                                  ker_p->tracxy(x,y,mat);
                                  if(transpose_) mat.transpose();
                                  opk.resize(mat.size());
                                  typename Matrix<T>::iterator itm=mat.begin();
                                  typename Vector<T>::iterator itv=opk.begin();
                                  for(; itv!=opk.end(); ++itv, ++itm) *itv=*itm;
                                  if(dims!=nullptr) *dims=mat.dims();
                              }
                              break;
                          }
                          default:
                              where("OperatorOnKernel::eval(...)");
                              error("operator_unexpected",words("diffop",ydifOpType()));
                      } //end of switch (ydifOpType()) of case trac_x
                         break;

                  default:
                    where("OperatorOnKernel::eval(...)");
                    error("operator_unexpected",words("diffop",xdifOpType()));

                }  //end of switch (xdifOpType())

              if(xydifOpType()==_id) return opk;  //no additional xy differential operator

              //xyDifop to apply
              switch(xydifOpType())
                {
                  case _nxdotny_times:  // do nx.ny * opk
                    {
                      if(nxp==nullptr || nyp==nullptr)
                        {
                          where("OperatorOnKernel::eval(...)");
                          error("null_pointer","x-normal or y-normal");
                        }
                      return opk*=dot(*nxp,*nyp);
                    }
                    break;
                  default:
                    where("OperatorOnKernel::eval(...)");
                    error("operator_unexpected",words("diffop",xydifOpType()));
                } //end of switch (xydifOpType())
              break;

            } //end case _matrix

          case _vector: //ONLY ID operator ix now available
            if(xdifOpType()==_id && ydifOpType()==_id)
              {
                ker(x,y,opk);
                if(dims!=nullptr) *dims=ker.dims();
              }
            else
              {
                where("OperatorOnKernel::eval(...)");
                error("free_error","only ID operator are supported on vector Kernel");
              }
            break;
          default:
            where("OperatorOnKernel::eval(...)");
            error("bad_struct_type",words("structure",struct_));
        }
      if(vto==_complex && conjugate_) opk=conj(opk);
      return opk;
    }

  //==============================================================================================================
  // extension from side to domain: pi node of the side domain, wi Lagrange shape function related to pi
  //==============================================================================================================
  //      ext_x(op(k)) = sum_{i} op(k)(pi,y)*wi(x),  ext_y(op(k)) = sum_{i} op(k)(x,pi)*wi(y)
  //      op_x(ext_x(k)) = sum_{i} k(xi,y)*op_x(wi)(x), op_y(ext_x(k)) = sum_{i} op_y(k)(pi,x)*wi(y)
  //      op_x.op_y(ext_x(k)) = sum_{i} op_y(k)(xi,y)*op_x(wi)(x)
  //  if the operator is inside the extension operator or no derivative or derivative variable different from the extension variable then
  //  only the simple forms  sum_{i} op(k)(pi,y)*wi(x)   sum_{i} op(k)(x,pi)*wi(y)
  //  note wi(x) should be scalar !

  std::vector<Point>::const_iterator itp= extdata->nodes.begin();  //nodes on side domain
  std::vector<real_t>::const_iterator itw=extdata->w.begin();      //shapevalues related to nodes at x
  Vector<T> opi;
  number_t ordx = xdifOp_p->order(), ordy = ydifOp_p->order(), ordxy = xydifOp_p->order();
  VariableName extvar = ext_p->var_;
  if(in_ext || (ordx==0 && ordy==0 && ordxy==0) || (ordx>0 && extvar==_y) || (ordy>0 && extvar==_x))
    {
      if(extvar==_x) // sum_{i} op(k)(pi,y)*wi(x)
        {
          for(; itp!=extdata->nodes.end(); ++itp, ++itw)
            {
              this->eval(*itp, y, opi, nxp, nyp, dims, 0);  //op(k)(pi,y)
              opk+=*itw*opi;
            }
          return opk;
        }
      else // sum_{i} op(k)(x,pi)*wi(y)
        {
          for(; itp!=extdata->nodes.end(); ++itp, ++itw)
            {
              this->eval(x,*itp, opi, nxp, nyp, dims, 0);  //op(k)(x,pi)
              opk+=*itw*opi;
            }
          return opk;
        }
    }
  /*other cases with derivative operators: difop_x(ext_x(k)) difop_y(ext_y(k)) difop_xy(ext_x(kr)) difop_xy(ext_y(k))
    more intricate cases because the dif operator has to be applied to scalar or vector or matrix shape functions
    regarding the dimensions of the kernel, for instance
      scalar kernel:
         difop_x(ext_x(k(x,y)))= sum_{i} k(pi,y)*difop_x(wi(x))
         difop_y(ext_y(k(x,y)))= sum_{i} k(x,pi)*difop_y(wi(y))
      vector kernel k=(k1,k2,...), ej canonical vector
         difop_x(ext_x(k(x,y)))= sum_{i} sum{j} kj(pi,y)*difop_x(wi(x)ej)
         difop_y(ext_y(k(x,y)))= sum_{i} sum{j} kj(x,pi)*difop_y(wi(y)ej)
      matrix kernel k=(k11,k12,..., k21, k22,...), Eij canonical matrix
         difop_x(ext_x(k(x,y)))= sum_{i} sum{m,n} kmn(pi,y)*difop_x(wi(x)Eij)
         difop_y(ext_y(k(x,y)))= sum_{i} sum{m,n} kmn(x,pi)*difop_y(wi(y)Eij)
  */
  StrucType stk= ker_p->strucType();
  Vector<real_t> opw;
  dimen_t nd=extdata->dw.size();
  if((ordx>0 && extvar==_x)|| (ordy>0 && extvar==_y))
    {
      dimen_t d=1, nbc=1;
      if(stk==_scalar)
        {
          if(extvar==_x) xdifOp_p->eval(extdata->w, extdata->dw, extdata->d2w, d, nbc, opw, nxp);  // opw = difop_x(wi(x))
          else ydifOp_p->eval(extdata->w, extdata->dw, extdata->d2w, d, nbc, opw, nyp);            // opw = difop_y(wi(y))
          auto ito=opw.begin();
          for(; itp!=extdata->nodes.end(); ++itp)
            {
              if(extvar==_x) ker_p->kernel(*itp, y, opi[0]); // k(pi,y)
              else ker_p->kernel(x, *itp, opi[0]);           // k(x,pi)
              for(auto j=0;j<d;j++, ++ito)
                opk[j] += opi[0] * *ito;  //scalar opi, vector opw (d dim of opk(wi(x)))
            }
          return opk;
        }
      //non scalar
      number_t M=ker_p->dims().first, N=ker_p->dims().second;
      number_t i=0;
      std::vector<real_t> w;
      std::vector< std::vector<real_t> > dw;
      std::vector< std::vector<real_t> > d2w;
      number_t nd2=1;
      if(nd>1) nd2=3*(nd-1);
      for(; itp!=extdata->nodes.end(); ++itp, ++i)
        {
          if(extvar==_x) ker_p->kernel(*itp, y, opi); // k(pi,y)
          else ker_p->kernel(x, *itp, opi);           // k(x,pi)
          if(stk==_vector)
            {
              d=M;
              w.resize(M,0.); dw.resize(M,std::vector<real_t>(1,0.));
              for(number_t  m=0; m<M; m++)
                {
                  w.assign(M,0.); w[m]=extdata->w[i];
                  dw.assign(nd,std::vector<real_t>(M,0.)); dw[m]=extdata->dw[i];
                  //in futur, add second derivative
                  if(extvar==_x) xdifOp_p->eval(w, dw, d2w, d, nbc, opw, nxp);
                  else ydifOp_p->eval(w, dw, d2w, d, nbc, opw, nyp);
                  opk+=opi[m] * opw;
                }
            }
          else // _matrix:
            {
              number_t MN=M*N;
              d=MN; nbc=M;
              w.resize(MN,0.); dw.resize(M,std::vector<real_t>(MN,0.));
              for(number_t  m=0; m<M; m++)
                for(number_t  n=0; n<N; n++)
                  {
                    w.assign(MN,0.); w[m*N+n]=extdata->w[i];
                    dw.assign(nd,std::vector<real_t>(MN,0.)); dw[m*N+n]=extdata->dw[i];
                    //in futur, add second derivative
                    if(extvar==_x) xdifOp_p->eval(w, dw, d2w, d, nbc, opw, nyp);
                    else ydifOp_p->eval(w, dw, d2w, d, nbc, opw, nyp);
                    opk+=opi[m*N+n] * opw;
                  }
            }
        }
      return opk;
    }
  if(ordxy>0)
    {
      //to be done
    }
  error("free_error"," in OperatorOnKernel::eval with extension, difop_xy(ext(ker)) is not yet handled");
  return opk; // dummy return
}
// ------------------------------------------------------------------------------------------------------------------------------------
// for SCALAR kernel
// ------------------------------------------------------------------------------------------------------------------------------------
//! evaluate operator on kernel operator at (x,y), k kernel value, nxp, nyp pointer to normal vectors nx and ny (null if not available)
template <typename T>
T& OperatorOnKernel::eval(const Point& x,const Point& y, T& k, const Vector<real_t>* nx, const Vector<real_t>* ny) const
{
  if(ker_p==nullptr) return k=T(1);  //no kernel pointer, ker=1

  switch(xdifOpType())
    {
      case _id:
        switch(ydifOpType())
          {
            case _id: ker_p->kernel(x,y,k); break;
            case _grad_y: ker_p->grady(x,y,k); break;
            case _ndotgrad_y:
              if(noUpdatedNormal || ker_p->no_ndotgrady())
                {
                  if(ny==nullptr)
                    {
                      where("OperatorOnKernel::eval(...)");
                      error("null_pointer","normal");
                    }
                  Vector<T> gky;
                  ker_p->grady(x,y,gky);
                  k=T(0);
                  Vector<real_t>::const_iterator itn=ny->begin();
                  typename Vector<T>::iterator itg=gky.begin();
                  for(; itg!=gky.end(); itg++, itn++) k += *itg * T(*itn);
                }
              else ker_p->ndotgrady(x,y,k);
              break;
            default:
              where("OperatorOnKernel::eval(...)");
              error("operator_unexpected",words("diffop",ydifOpType()));
          }
        break;

      case _grad_x:
        switch(ydifOpType())
          {
            case _id: ker_p->gradx(x,y,k); break;
            case _grad_y: ker_p->gradxy(x,y,k); break;

            default:
              where("OperatorOnKernel::eval(...)");
              error("operator_unexpected",words("diffop",ydifOpType()));
          }
        break;

      case _ndotgrad_x:
        switch(ydifOpType())
          {
            case _id:
              if(noUpdatedNormal || ker_p->no_ndotgradx())
                {
                  if(nx==nullptr)
                    {
                      where("OperatorOnKernel::eval(...)");
                      error("null_pointer","normal");
                    }
                  Vector<T> gkx;
                  ker_p->gradx(x,y,gkx);
                  k=T(0);
                  Vector<real_t>::const_iterator itn=nx->begin();
                  typename Vector<T>::iterator itg=gkx.begin();
                  for(; itg!=gkx.end(); itg++, itn++) k += *itg * T(*itn);
                }
              else ker_p->ndotgradx(x,y,k);
              break;
            case _ndotgrad_y:
              {
                if(nx==nullptr || ny==nullptr)
                  {
                    where("OperatorOnKernel::eval(...)");
                    error("null_pointer","normal");
                  }
                Matrix<T> mkxy;
                Vector<T> dx=ker_p->gradxy(x,y,mkxy)** nx;
                k=complexToT<T>(dotRC(dx, *ny));
                break;
              }
            default:
              where("OperatorOnKernel::eval(...)");
              error("operator_unexpected",words("diffop",ydifOpType()));
          }
        break;

      default:
        where("OperatorOnKernel::eval(...)");
        error("operator_unexpected",words("diffop",xdifOpType()));
    }

  if(xydifOpType()==_id) return k;

  //xyDifop to apply
  switch(xydifOpType())
    {
      case _nxdotny_times:
        {
          if(nx==nullptr)
            {
              where("OperatorOnKernel::eval(...)");
              error("null_pointer","x-normal");
            }
          if(ny==nullptr)
            {
              where("OperatorOnKernel::eval(...)");
              error("null_pointer","y-normal");
            }
          return k*=dot(*nx,*ny);
        }
        break;
      default:
        where("OperatorOnKernel::eval(...)");
        error("operator_unexpected",words("diffop",xydifOpType()));
    }
  return k; // dummy return
}

/*! evaluate operator on kernel operator at (x,P) or (P,x) regarding xpar, xory=P
    k kernel value, nxp, nyp pointer to normal vectors nx and ny (null if not available)*/
template <typename T>
T& OperatorOnKernel::eval(const Point& x, T& k, const Vector<real_t>* nxp, const Vector<real_t>* nyp) const
{
  if(ker_p->xpar) return eval(ker_p->xory, x, k, nxp, nyp);
  else return eval(x, ker_p->xory, k, nxp, nyp);
}

// ------------------------------------------------------------------------------------------------------------------------------------
//   scalar kernel operator at some points (xi,yj)
// ------------------------------------------------------------------------------------------------------------------------------------
/*! evaluate kernel operator at (xi,yj):
    op [k(x1,y1), k(x1,y2),..., k(x1,yn), k(x2,y1), k(x2,y2), ..., k(xm,yn)]
    xs, ys  : points collection
    nxs, nys: pointer to normal vectors nx and ny (null if not available)
    shift   : shift index of result (by default 0)

    ##### NOTE: be careful, address of ks begins at 1 and not at 0 !!!!
*/
template <typename T>
std::vector<T>& OperatorOnKernel::eval(const std::vector<Point>& xs,const std::vector<Point>& ys, std::vector<T>& ks, number_t shift,
                                       const std::vector<Vector<real_t> >* nxs, const std::vector<Vector<real_t> >* nys) const
{
  ks.resize(xs.size()*ys.size()+1,T(0));
  std::vector<Point>::const_iterator itx=xs.begin(),ity;
  typename std::vector<T>::iterator itk=ks.begin()+shift;       //iterator shift
  switch(xdifOpType())
    {
      case _id:
        switch(ydifOpType())
          {
            case _id:
              {
                for(; itx!=xs.end(); itx++)
                  for(ity=ys.begin(); ity!=ys.end(); ity++, itk++) ker_p->kernel(*itx,*ity,*itk);
                break;
              }
            case _grad_y:
              {
                for(; itx!=xs.end(); itx++)
                  for(ity=ys.begin(); ity!=ys.end(); ity++, itk++) ker_p->grady(*itx,*ity,*itk);
                break;
              }
            case _ndotgrad_y:
              {
                if(nys==nullptr)
                  {
                    where("OperatorOnKernel::eval(...)");
                    error("null_pointer","normal");
                  }
                for(; itx!=xs.end(); itx++)
                  {
                    std::vector<Vector<real_t> >::const_iterator itn=nys->begin();
                    for(ity=ys.begin(); ity!=ys.end(); ity++, itn++, itk++)
                      {
                        Vector<T> gky;
                        ker_p->grady(*itx,*ity, gky);
                        *itk=T(0);
                        Vector<real_t>::const_iterator itnd=itn->begin();
                        typename Vector<T>::iterator itg=gky.begin();
                        for(; itg!=gky.end(); itg++, itnd++) *itk += *itg * T(*itnd);
                      }
                  }
                break;
              }
            default:
              where("OperatorOnKernel::eval(...)");
              error("operator_unexpected",words("diffop",ydifOpType()));
          }
        break;

      case _grad_x:
        switch(xdifOpType())
          {
            case _id:
              {
                for(; itx!=xs.end(); itx++)
                  for(ity=ys.begin(); ity!=ys.end(); ity++, itk++) ker_p->gradx(*itx,*ity,*itk);
                break;
              }
            case _grad_y:
              {
                for(; itx!=xs.end(); itx++)
                  for(ity=ys.begin(); ity!=ys.end(); ity++, itk++) ker_p->gradxy(*itx,*ity,*itk);
                break;
              }
            default:
              where("OperatorOnKernel::eval(...)");
              error("operator_unexpected",words("diffop",ydifOpType()));
          }
        break;

      case _ndotgrad_x:
        switch(ydifOpType())
          {
            case _id:
              {
                if(nxs==nullptr)
                  {
                    where("OperatorOnKernel::eval(...)");
                    error("null_pointer","normal");
                  }
                for(; itx!=xs.end(); itx++)
                  {
                    std::vector<Vector<real_t> >::const_iterator itn=nxs->begin();
                    for(ity=ys.begin(); ity!=ys.end(); ity++, itn++, itk++)
                      {
                        Vector<T> gkx;
                        ker_p->gradx(*itx,*ity, gkx);
                        *itk=T(0);
                        Vector<real_t>::const_iterator itnd=itn->begin();
                        typename Vector<T>::iterator itg=gkx.begin();
                        for(; itg!=gkx.end(); itg++, itnd++) *itk += *itg * T(*itnd);
                      }
                  }
                break;
              }
            default:
              where("OperatorOnKernel::eval(...)");
              error("operator_unexpected",words("diffop",ydifOpType()));
          }
        break;

      default:
        where("OperatorOnKernel::eval(...)");
        error("operator_unexpected",words("diffop",xdifOpType()));
    }
  return ks;
}

template <typename T>
std::vector<Vector<T> >& OperatorOnKernel::eval(const std::vector<Point>& xs,const std::vector<Point>& ys,std::vector<Vector<T> >& ks, number_t shift,
    const std::vector<Vector<real_t> >* nxs, const std::vector<Vector<real_t> >* nys) const
{
  error("not_yet_implemented","OperatorOnKernel::eval<T>(Vector<Point>, Vector<Point>, Vector<Vector<T> >, Number, Vector<Vector<Real> >, Vector<Vector<Real> >)");
}

// ------------------------------------------------------------------------------------------------------------------------------------
// for MATRIX kernel
// ------------------------------------------------------------------------------------------------------------------------------------
template <typename T>
Matrix<T>& OperatorOnKernel::eval(const Point& x,const Point& y, Matrix<T>& k, const Vector<real_t>* nxp, const Vector<real_t>* nyp) const
{
  error("not_yet_implemented","OperatorOnKernel::eval<T>(Point x, Point y, Matrix<T>& k, Vector<real_t>* nxp, Vector<real_t>* nyp)");
  return k;
}

template <typename T>
std::vector<Matrix<T> >& OperatorOnKernel::eval(const std::vector<Point>& xs,const std::vector<Point>& ys,std::vector<Matrix<T> >& ks, number_t shift,
    const std::vector<Vector<real_t> >* nxs, const std::vector<Vector<real_t> >* nys) const
{
  error("not_yet_implemented","OperatorOnKernel::eval<T>(Vector<Point>, Vector<Point>, Vector<Matrix<T> >, Number, Vector<Vector<Real> >, Vector<Vector<Real> >)");
  return ks; // dummy return
}

} // end of namespace xlifepp

#endif /* OPERATOR_ON_KERNEL_HPP */
