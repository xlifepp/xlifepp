/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TermMatrix.hpp
  \author E. Lunéville
  \since 23 mar 2012
  \date 5 oct 2012

  \brief Definition of the xlifepp::TermMatrix class

  xlifepp::TermMatrix class handles the numerical representation of general bilinear forms (xlifepp::BilinearForm)
  that is the matrix a(vi,wj), i=1,...m; j=1,..,n with (vi)and (wj) the basis functions.
  It inherits from xlifepp::Term class and it is an end user class.

  A xlifepp::BilinearForm object (see BilinearForm.hpp) is a multiple unknowns/spaces bilinear form
  that is a list of bilinear forms acting on a couple of (sub)spaces (xlifepp::SuBilinearForm object)
  indexed by unknown/testfunction . As a consquence, a xlifepp::TermMatrix object is a list of xlifepp::SuTermMatrix objects
  that are related to xlifepp::SuBilinearForm objects

  \verbatim
          child
  Term  --------> TermMatrix
                    | BilinearForm
                    | MatrixEntry*
                    | map<uvPair,SuTermMatrix>  (uvPair is a pair of unknown, testfunction)
                    | ...
  \endverbatim

  End user has to instanciate only xlifepp::TermMatrix object, xlifepp::SuTermMatrix is an internal object never managed by end user,
  even if its bilinear form is really a single unknown/testfunction bilinear form.

  xlifepp::MatrixEntry pointer of xlifepp::TermMatrix is in general null, may be not in case of multiple uvPair bilinear form.
  xlifepp::TermMatrix being seen as a block matrix, its matrix values are given by xlifepp::MatrixEntry of each xlifepp::SuTermMatrix.
  In some circonstances (direct solvers), xlifepp::MatrixEntry pointer of xlifepp::TermMatrix may be allocated to store xlifepp::TermMatrix
  as a contiguous matrix without block structure.
  Be care, both representations may exist at the same time but with two times memory consuming !!!

  Note: if required it is possible to go to scalar representation of matrix by using scalar_entries_p
         and renumbering vectors cdofs_u (column) and cdofs_v (row).
         This representation has no interest if all the unknowns involved in xlifepp::TemMatrix are scalar !
*/


#ifndef TERM_MATRIX_HPP
#define TERM_MATRIX_HPP

#include "config.h"
#include "Term.hpp"
#include "TermVector.hpp"
#include "SuTermMatrix.hpp"
#include "form.h"
#include "largeMatrix.h"
#include "solvers.h"
#include "computation/termUtils.hpp"
#include "EigenTerms.hpp"

namespace xlifepp
{

//@{
//! useful aliases for TermMatrix class
typedef std::map<uvPair, SuTermMatrix*>::iterator it_mustm;
typedef std::map<uvPair, SuTermMatrix*>::const_iterator cit_mustm;
//@}

extern TermMatrix theDefaultTermMatrix;
extern const TermVector theDefaultTermVector;
extern Preconditioner theDefaultPreconditioner;

/*!
   \class TermMatrix
   end user class handling numerical representation of any
   bilinear form (BilinearForm). May be multiple unknowns
*/
class TermMatrix : public Term
{
  protected:
    BilinearForm bilinForm_;                    //!< bilinear form
    std::map<uvPair, SuTermMatrix*> suTerms_;   //!< list of single unknowns term matrices
    MatrixEntry* entries_p;                     //!< pointer to values of TermMatrix (see explanations)

    //to deal with constraints
    SetOfConstraints* constraints_u_p;         //!< constraints data related to conditions to apply to unknowns  (col)
    SetOfConstraints* constraints_v_p;         //!< constraints data related to conditions to apply to test functions (row), in general equal to constraints_u_p
    MatrixEntry* rhs_matrix_p;                 //!< pointer to a correction matrix when constraints applied

    // for scalar representation
    MatrixEntry* scalar_entries_p;             //!< values of TermMatrix in scalar representation, same as entries_p if scalar unknowns
    std::vector<DofComponent> cdofs_c;         //!< col scalar numbering when scalar_entries_p is allocated
    std::vector<DofComponent> cdofs_r;         //!< row scalar numbering when scalar_entries_p is allocated
    std::vector<DofComponent> orgcdofs_c;      //!< original col scalar numbering when scalar_entries_p is allocated and reduced matrix
    std::vector<DofComponent> orgcdofs_r;      //!< original row scalar numbering when scalar_entries_p is allocated and reduced matrix

  public:
    TermMatrix(const string_t & na="?"); //!< default constructor
    std::set<ParameterKey> getParamsKeys(); //! returns the list of authorized keys
    void buildParam(const Parameter& p, bool& toCompute, bool& toAssemble, SymType& sym);
    //! effective constructor from bilinear form, essential conditions  and options
    void build(const BilinearForm& bf, const EssentialConditions* ecsu, const EssentialConditions* ecsv,
    const std::vector<Parameter>& ps);

    // constructors with options and no essential boundary conditions
    TermMatrix(const BilinearForm& bf, const string_t& na) : TermMatrix(bf, _name=na)
    {
      warning("deprecated", "TermMatrix(BilinearForm, String)", "TermMatrix(BilinearForm, _name=?)");
    }
    TermMatrix(const BilinearForm& bf, const char* na) : TermMatrix(bf, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(BilinearForm, String)", "TermMatrix(BilinearForm, _name=?)");
    }
    TermMatrix(const BilinearForm& bf)
    {
      std::vector<Parameter> ps(0);
      build(bf, 0, 0, ps);
    }
    TermMatrix(const BilinearForm& bf, const Parameter& p1)
    {
      std::vector<Parameter> ps = {p1};
      build(bf, 0, 0, ps);
    }
    TermMatrix(const BilinearForm& bf, const Parameter& p1, const Parameter& p2)
    {
      std::vector<Parameter> ps = {p1, p2};
      build(bf, 0, 0, ps);
    }
    TermMatrix(const BilinearForm& bf, const Parameter& p1, const Parameter& p2, const Parameter& p3)
    {
      std::vector<Parameter> ps = {p1, p2, p3};
      build(bf, 0, 0, ps);
    }
    TermMatrix(const BilinearForm& bf, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
    {
      std::vector<Parameter> ps = {p1, p2, p3, p4};
      build(bf, 0, 0, ps);
    }
    TermMatrix(const BilinearForm& bf, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4,
               const Parameter& p5)
    {
      std::vector<Parameter> ps = {p1, p2, p3, p4, p5};
      build(bf, 0, 0, ps);
    }
    TermMatrix(const BilinearForm& bf, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4,
               const Parameter& p5, const Parameter& p6)
    {
      std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6};
      build(bf, 0, 0, ps);
    }

    //constructor with options and one essential boundary condition (same on u and v)
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const string_t& na) : TermMatrix(bf, ecsu, _name=na)
    {
      warning("deprecated", "TermMatrix(BilinearForm, EssentialConditions, String)", "TermMatrix(BilinearForm, EssentialConditions, _name=?)");
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const char* na) : TermMatrix(bf, ecsu, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(BilinearForm, EssentialConditions, String)", "TermMatrix(BilinearForm, EssentialConditions, _name=?)");
    }
    //constructor with one essential boundary condition (same on u and v) and explicit reduction method
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const ReductionMethod& rm, const string_t& na="") : TermMatrix(bf, ecsu, _reduction=rm, _name=na)
    {
      warning("deprecated", "TermMatrix(BilinearForm, EssentialConditions, ReductionMethod, String)", "TermMatrix(BilinearForm, EssentialConditions, _reduction=?, _name=?)");
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const ReductionMethod& rm, const char* na) : TermMatrix(bf, ecsu, _reduction=rm, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(BilinearForm, EssentialConditions, ReductionMethod, String)", "TermMatrix(BilinearForm, EssentialConditions, _reduction=?, _name=?)");
    }

    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu)
    {
      std::vector<Parameter> ps(0);
      build(bf, &ecsu, 0, ps);
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const Parameter& p1)
    {
      std::vector<Parameter> ps = {p1};
      build(bf, &ecsu, 0, ps);
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const Parameter& p1, const Parameter& p2)
    {
      std::vector<Parameter> ps = {p1, p2};
      build(bf, &ecsu, 0, ps);
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const Parameter& p1, const Parameter& p2, const Parameter& p3)
    {
      std::vector<Parameter> ps = {p1, p2, p3};
      build(bf, &ecsu, 0, ps);
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const Parameter& p1, const Parameter& p2,
               const Parameter& p3, const Parameter& p4)
    {
      std::vector<Parameter> ps = {p1, p2, p3, p4};
      build(bf, &ecsu, 0, ps);
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const Parameter& p1, const Parameter& p2,
               const Parameter& p3, const Parameter& p4, const Parameter& p5)
    {
      std::vector<Parameter> ps = {p1, p2, p3, p4, p5};
      build(bf, &ecsu, 0, ps);
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const Parameter& p1, const Parameter& p2,
               const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
    {
      std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6};
      build(bf, &ecsu, 0, ps);
    }

    //constructor with options and two essential boundary conditions (resp. on u and v)
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const string_t& na) : TermMatrix(bf, ecsu, ecsv, _name=na)
    {
      warning("deprecated", "TermMatrix(BilinearForm, EssentialConditions, EssentialConditions, String)", "TermMatrix(BilinearForm, EssentialConditions, EssentialConditions, _name=?)");
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const char* na) : TermMatrix(bf, ecsu, ecsv, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(BilinearForm, EssentialConditions, EssentialConditions, String)", "TermMatrix(BilinearForm, EssentialConditions, EssentialConditions, _name=?)");
    }
    //constructor with options and two essential boundary conditions (resp. on u and v) and explicit reduction method
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const ReductionMethod& rm, const string_t& na="") : TermMatrix(bf, ecsu, ecsv, _reduction=rm, _name=na)
    {
      warning("deprecated", "TermMatrix(BilinearForm, EssentialConditions, EssentialConditions, ReductionMethod, String)", "TermMatrix(BilinearForm, EssentialConditions, EssentialConditions, _reduction=rm, _name=?)");
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const ReductionMethod& rm, const char* na) : TermMatrix(bf, ecsu, ecsv, _reduction=rm, _name=na)
    {
      warning("deprecated", "TermMatrix(BilinearForm, EssentialConditions, EssentialConditions, ReductionMethod, String)", "TermMatrix(BilinearForm, EssentialConditions, EssentialConditions, _reduction=rm, _name=?)");
    }
    
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const EssentialConditions& ecsv)
    {
      std::vector<Parameter> ps(0);
      build(bf, &ecsu, &ecsv, ps);
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const Parameter& p1)
    {
      std::vector<Parameter> ps = {p1};
      build(bf, &ecsu, &ecsv, ps);
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const Parameter& p1,
               const Parameter& p2)
    {
      std::vector<Parameter> ps = {p1, p2};
      build(bf, &ecsu, &ecsv, ps);
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const Parameter& p1, 
               const Parameter& p2, const Parameter& p3)
    {
      std::vector<Parameter> ps = {p1, p2, p3};
      build(bf, &ecsu, &ecsv, ps);
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const Parameter& p1, 
               const Parameter& p2, const Parameter& p3, const Parameter& p4)
    {
      std::vector<Parameter> ps = {p1, p2, p3, p4};
      build(bf, &ecsu, &ecsv, ps);
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const Parameter& p1, 
               const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
    {
      std::vector<Parameter> ps = {p1, p2, p3, p4, p5};
      build(bf, &ecsu, &ecsv, ps);
    }
    TermMatrix(const BilinearForm& bf, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const Parameter& p1, 
               const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
    {
      std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6};
      build(bf, &ecsu, &ecsv, ps);
    }

    //! effective constructor from TermMatrix, essential conditions  and options
    void build(const TermMatrix& tm, const EssentialConditions* ecsu, const EssentialConditions* ecsv,
               const std::vector<Parameter>& ps);

    // constructor from TermMatrix and EssentialConditions (same on u and v) and explicit reduction method
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const string_t& na) : TermMatrix(tm, ecsu, _name=na)
    {
      warning("deprecated", "TermMatrix(TermMatrix, EssentialConditions, String)", "TermMatrix(TermMatrix, EssentialConditions, _name=?)");
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const char* na) : TermMatrix(tm, ecsu, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(TermMatrix, EssentialConditions, String)", "TermMatrix(TermMatrix, EssentialConditions, _name=?)");
    }
    // constructor with one essential boundary condition (same on u and v) and explicit reduction method
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const ReductionMethod& rm, const string_t& na="") : TermMatrix(tm, ecsu, _reduction=rm, _name=na)
    {
      warning("deprecated", "TermMatrix(TermMatrix, EssentialConditions, ReductionMethod, String)", "TermMatrix(TermMatrix, EssentialConditions, _reduction=?, _name=?)");
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const ReductionMethod& rm, const char* na) : TermMatrix(tm, ecsu, _reduction=rm, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(TermMatrix, EssentialConditions, ReductionMethod, String)", "TermMatrix(TermMatrix, EssentialConditions, _reduction=?, _name=?)");
    }

    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu)
    {
      std::vector<Parameter> ps(0);
      build(tm, &ecsu, 0, ps);
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const Parameter& p1)
    {
      std::vector<Parameter> ps={p1};
      build(tm, &ecsu, 0, ps);
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const Parameter& p1, const Parameter& p2)
    {
      std::vector<Parameter> ps={p1, p2};
      build(tm, &ecsu, 0, ps);
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const Parameter& p1, const Parameter& p2,
               const Parameter& p3)
    {
      std::vector<Parameter> ps={p1, p2, p3};
      build(tm, &ecsu, 0, ps);
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const Parameter& p1, const Parameter& p2,
               const Parameter& p3, const Parameter& p4)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4};
      build(tm, &ecsu, 0, ps);
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const Parameter& p1, const Parameter& p2,
               const Parameter& p3, const Parameter& p4, const Parameter& p5)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5};
      build(tm, &ecsu, 0, ps);
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const Parameter& p1, const Parameter& p2,
               const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
      build(tm, &ecsu, 0, ps);
    }

    // constructor with options and two essential boundary conditions (resp. on u and v)
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const string_t& na) : TermMatrix(tm, ecsu, ecsv, _name=na)
    {
      warning("deprecated", "TermMatrix(TermMatrix, EssentialConditions, EssentialConditions, String)", "TermMatrix(TermMatrix, EssentialConditions, EssentialConditions, _name=?)");
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const char* na) : TermMatrix(tm, ecsu, ecsv, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(TermMatrix, EssentialConditions, EssentialConditions, String)", "TermMatrix(TermMatrix, EssentialConditions, EssentialConditions, _name=?)");
    }
    // constructor with options and two essential boundary conditions (resp. on u and v) and explicit reduction method
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const ReductionMethod& rm, const string_t& na="") : TermMatrix(tm, ecsu, ecsv, _reduction=rm, _name=na)
    {
      warning("deprecated", "TermMatrix(TermMatrix, EssentialConditions, EssentialConditions, ReductionMethod, String)", "TermMatrix(TermMatrix, EssentialConditions, EssentialConditions, _reduction=?, _name=?)");
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const ReductionMethod& rm, const char* na) : TermMatrix(tm, ecsu, ecsv, _reduction=rm, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(TermMatrix, EssentialConditions, EssentialConditions, ReductionMethod, String)", "TermMatrix(TermMatrix, EssentialConditions, EssentialConditions, _reduction=?, _name=?)");
    }

    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const EssentialConditions& ecsv)
    {
      std::vector<Parameter> ps(0);
      build(tm, &ecsu, &ecsv, ps);
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const Parameter& p1)
    {
      std::vector<Parameter> ps={p1};
      build(tm, &ecsu, &ecsv, ps);
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const Parameter& p1, const Parameter& p2)
    {
      std::vector<Parameter> ps={p1, p2};
      build(tm, &ecsu, &ecsv, ps);
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const Parameter& p1, const Parameter& p2, const Parameter& p3)
    {
      std::vector<Parameter> ps={p1, p2, p3};
      build(tm, &ecsu, &ecsv, ps);
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4};
      build(tm, &ecsu, &ecsv, ps);
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5};
      build(tm, &ecsu, &ecsv, ps);
    }
    TermMatrix(const TermMatrix& tm, const EssentialConditions& ecsu, const EssentialConditions& ecsv, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
    {
      std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
      build(tm, &ecsu, &ecsv, ps);
    }

    //special constructors
    TermMatrix(const TermMatrix& tm, const string_t& na="");                  //!< copy constructor (full copy)
    explicit TermMatrix(const TermVector& tv, const string_t& na="");         //!< constructor of a diagonal TermMatrix from a TermVector

    //! sf(tv1, tv2)
    TermMatrix(const TermVector& tv1, const TermVector& tv2, const SymbolicFunction& sf, const Parameter& p1);
    //! sf(tv1, tv2)
    TermMatrix(const TermVector& tv1, const TermVector& tv2, const SymbolicFunction& sf) : TermMatrix(tv1, tv2, sf, _name="") {}
    //! sf(tv1, tv2)
    TermMatrix(const TermVector& tv1, const TermVector& tv2, const SymbolicFunction& sf, const string_t& na) : TermMatrix(tv1, tv2, sf, _name=na)
    {
      warning("deprecated", "TermMatrix(TermVector, TermVector, SymbolicFunction, String)", "TermMatrix(TermVector, TermVector, SymbolicFunction, _name=?)");
    }
    //! sf(tv1, tv2)
    TermMatrix(const TermVector& tv1, const TermVector& tv2, const SymbolicFunction& sf, const char* na) : TermMatrix(tv1, tv2, sf, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(TermVector, TermVector, SymbolicFunction, String)", "TermMatrix(TermVector, TermVector, SymbolicFunction, _name=?)");
    }

    //! effective constructor of special matrix from a TermMatrix (e.g _IdMatrix)
    void buildSpecialMatrix(const TermMatrix& A, SpecialMatrix sm, const complex_t& a, const string_t& na);
    //! construct special matrix from a TermMatrix (e.g _IdMatrix)
    TermMatrix(const TermMatrix& tm, SpecialMatrix sm, const complex_t& a)
    { buildSpecialMatrix(tm, sm, a, ""); }
    //! construct special matrix from a TermMatrix (e.g _IdMatrix)
    TermMatrix(const TermMatrix& tm, SpecialMatrix sm, const real_t& a)
    { buildSpecialMatrix(tm, sm, complex_t(a), ""); }
    //! construct special matrix from a TermMatrix (e.g _IdMatrix)
    TermMatrix(const TermMatrix& tm, SpecialMatrix sm, const complex_t& a, const Parameter& p1);
    //! construct special matrix from a TermMatrix (e.g _IdMatrix)
    TermMatrix(const TermMatrix& tm, SpecialMatrix sm, const complex_t& a, const string_t& na) : TermMatrix(tm, sm, a, _name=na)
    {
      warning("deprecated", "TermMatrix(TermMatrix, SpecialMatrix, Complex, String)", "TermMatrix(TermMatrix, SpecialMatrix, Complex, _name=?)");
    }
    //! construct special matrix from a TermMatrix (e.g _IdMatrix)
    TermMatrix(const TermMatrix& tm, SpecialMatrix sm, const complex_t& a, const char* na) : TermMatrix(tm, sm, a, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(TermMatrix, SpecialMatrix, Complex, String)", "TermMatrix(TermMatrix, SpecialMatrix, Complex, _name=?)");
    }
    //! construct special matrix from a TermMatrix (e.g _IdMatrix)
    TermMatrix(const TermMatrix& tm, SpecialMatrix sm, const real_t& a, const Parameter& p1) : TermMatrix(tm, sm, complex_t(a), p1) {}
    //! construct special matrix from a TermMatrix (e.g _IdMatrix)
    TermMatrix(const TermMatrix& tm, SpecialMatrix sm, const real_t& a, const string_t& na) : TermMatrix(tm, sm, a, _name=na)
    {
      warning("deprecated", "TermMatrix(TermMatrix, SpecialMatrix, Real, String)", "TermMatrix(TermMatrix, SpecialMatrix, Real, _name=?)");
    }
    //! construct special matrix from a TermMatrix (e.g _IdMatrix)
    TermMatrix(const TermMatrix& tm, SpecialMatrix sm, const real_t& a, const char* na) : TermMatrix(tm, sm, a, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(TermMatrix, SpecialMatrix, Real, String)", "TermMatrix(TermMatrix, SpecialMatrix, Real, _name=?)");
    }
    //! construct special matrix from a TermMatrix (e.g _IdMatrix)
    TermMatrix(const TermMatrix& tm, SpecialMatrix sm) : TermMatrix(tm, sm, complex_t(1.)) {}
    //! construct special matrix from a TermMatrix (e.g _IdMatrix)
    TermMatrix(const TermMatrix& tm, SpecialMatrix sm, const Parameter& p1) : TermMatrix(tm, sm, complex_t(1.), p1) {}
    //! construct special matrix from a TermMatrix (e.g _IdMatrix)
    TermMatrix(const TermMatrix& tm, SpecialMatrix sm, const string_t& na) : TermMatrix(tm, sm, complex_t(1.0), _name=na)
    {
      warning("deprecated", "TermMatrix(TermMatrix, SpecialMatrix, String)", "TermMatrix(TermMatrix, SpecialMatrix, _name=?)");
    }
    //! construct special matrix from a TermMatrix (e.g _IdMatrix)
    TermMatrix(const TermMatrix& tm, SpecialMatrix sm, const char* na) : TermMatrix(tm, sm, complex_t(1.0), _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(TermMatrix, SpecialMatrix, String)", "TermMatrix(TermMatrix, SpecialMatrix, _name=?)");
    }

    //! constructor of a TermMatrix from a SuTermMatrix (full copy)
    TermMatrix(const SuTermMatrix&, const string_t& na="");
    //! constructor of a TermMatrix from a SuTermMatrix * (pointer copy)
    TermMatrix(SuTermMatrix*, const string_t& na="");
    //! special constructor of a diagonal TermMatrix from a vector
    template<typename T>
    TermMatrix(const Unknown& u, const GeomDomain& dom, const Vector<T>& vt, const Parameter& p1);
    //! special constructor of a diagonal TermMatrix from a vector
    template<typename T>
    TermMatrix(const Unknown& u, const GeomDomain& dom, const Vector<T>& vt) : TermMatrix(u, dom, vt, _name="") {}
    //! special constructor of a diagonal TermMatrix from a vector
    template<typename T>
    TermMatrix(const Unknown& u, const GeomDomain& dom, const Vector<T>& vt, const string_t& na) : TermMatrix(u, dom, vt, _name=na)
    {
      warning("deprecated", "TermMatrix(Unknown, Domain, Vector<T>, String)", "TermMatrix(Unknown, Domain, Vector<T>, _name=?)");
    }
    //! special constructor of a diagonal TermMatrix from a vector
    template<typename T>
    TermMatrix(const Unknown& u, const GeomDomain& dom, const Vector<T>& vt, const char* na) : TermMatrix(u, dom, vt, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(Unknown, Domain, Vector<T>, String)", "TermMatrix(Unknown, Domain, Vector<T>, _name=?)");
    }

    //! constructor of matrix opker(xi,yj) (Lagrange interpolation)
    TermMatrix(const Unknown& v, const GeomDomain& domv, const Unknown& u, const GeomDomain& domu, const OperatorOnKernel& opker, const Parameter& p1);
    //! constructor of matrix opker(xi,yj) (Lagrange interpolation)
    TermMatrix(const Unknown& v, const GeomDomain& domv, const Unknown& u, const GeomDomain& domu, const OperatorOnKernel& opker) : TermMatrix(v, domv, u, domu, opker, _name="") {}
    //! constructor of matrix opker(xi,yj) (Lagrange interpolation)
    TermMatrix(const Unknown& v, const GeomDomain& domv, const Unknown& u, const GeomDomain& domu, const OperatorOnKernel& opker, const string_t& na) : TermMatrix(v, domv, u, domu, opker, _name=na)
    {
      warning("deprecated", "TermMatrix(Unknown, Domain, Unknown, Domain, OperatorOnKernel, String)", "TermMatrix(Unknown, Domain, Unknown, Domain, OperatorOnKernel, _name=?)");
    }
    //! constructor of matrix opker(xi,yj) (Lagrange interpolation)
    TermMatrix(const Unknown& v, const GeomDomain& domv, const Unknown& u, const GeomDomain& domu, const OperatorOnKernel& opker, const char* na) : TermMatrix(v, domv, u, domu, opker, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(Unknown, Domain, Unknown, Domain, OperatorOnKernel, String)", "TermMatrix(Unknown, Domain, Unknown, Domain, OperatorOnKernel, _name=?)");
    }

    //! constructor from a linear combination of TermMatrix's
    TermMatrix(const LcTerm<TermMatrix>& lctm, const Parameter& p1);
    //! constructor from a linear combination of TermMatrix's
    TermMatrix(const LcTerm<TermMatrix>& lctm) : TermMatrix(lctm, _name="") {}
    //! constructor from a linear combination of TermMatrix's
    TermMatrix(const LcTerm<TermMatrix>& lctm, const string_t& na) : TermMatrix(lctm, _name=na)
    {
      warning("deprecated", "TermMatrix(LcTerm, String)", "TermMatrix(LcTerm, _name=?)");
    }
    //! constructor from a linear combination of TermMatrix's
    TermMatrix(const LcTerm<TermMatrix>& lctm, const char* na) : TermMatrix(lctm, _name=string_t(na))
    {
      warning("deprecated", "TermMatrix(LcTerm, String)", "TermMatrix(LcTerm, _name=?)");
    }
    
    //! construct TermMatrix from a standard Matrix (virtual unknown)
    template<typename T>
    TermMatrix(const Matrix<T>& mat, const Parameter& p1);
    //! construct TermMatrix from a standard Matrix (virtual unknown)
    template<typename T>
    explicit TermMatrix(const Matrix<T>& mat) : TermMatrix(mat, _name="") {}
    //! construct TermMatrix from a standard Matrix (virtual unknown)
    template<typename T>
    TermMatrix(const Matrix<T>& mat, const string_t& na) : TermMatrix(mat, _name=na)
    {
      warning("deprecated", "TermMatrix(Matrix<T>, String)", "TermMatrix(Matrix<T>, _name=?)");
    }
    //! construct TermMatrix from a standard Matrix (virtual unknown)
    template<typename T>
    TermMatrix(const Matrix<T>& mat, const char* na) : TermMatrix(mat, _name=na)
    {
      warning("deprecated", "TermMatrix(Matrix<T>, String)", "TermMatrix(Matrix<T>, _name=?)");
    }

    //! construct TermMatrix from a row dense matrix of size m x n (virtual unknown)
    template<typename T>
    TermMatrix(const std::vector<T>& mat, number_t m, number_t n=0, const string_t& na="");
    template <typename T>
    TermMatrix(const Unknown& u, const TestFunction& v, const std::vector<T>& mat, number_t m, number_t n, const string_t& na="");
    template <typename T>
    TermMatrix(const Unknown& u, const TestFunction& v, const Matrix<T>& mat, const string_t& na="");
    //! real constructor from matrix
    template<typename T>
    void buildFromRowDenseMatrix(const std::vector<T>& mat, number_t m, number_t n, const string_t& na);
    template<typename T>
    void buildFromRowDenseMatrix(const Unknown& u, const Space& spu, const Unknown& v, const Space& spv, const std::vector<T>& mat, number_t m, number_t n, const string_t& na="");

    //destructor, assign and construction tools
    virtual ~TermMatrix(); //!< destructor
    TermMatrix& operator=(const LcTerm<TermMatrix>& lctm); //!< assign operator from a linear combination of TermMatrix's
    TermMatrix& operator=(const TermMatrix& tm);           //!< assign operator (full copy)
    void initFromBF(const BilinearForm& bf, const string_t& na = "", bool noass = false); //!< initializer with bf
    void copy(const TermMatrix& tm);                                  //!< copy a TermMatrix in current TermMatrix
    void clear();                                                     //!< deallocate memory used by a TermMatrix
    void insert(const SuTermMatrix& sutm);                            //!< insert a SuTermMatrix  with full copy
    void insert(SuTermMatrix* sutm);                                  //!< insert a SuTermMatrix with pointer copy
    //! initialize a TermVector consistent with matrix rows or columns
    void initTermVector(TermVector& tv, ValueType vt, bool col=true) const;
    //! initialize a TermVector from value T, consistent with matrix rows or columns
    template<typename T>
    void initTermVectorT(TermVector& tv, const T& t, bool col=true) const;

    //accessors and properties
    string_t name() const { return Term::name(); }
    void name(const string_t& nam);                        //!< update the name of a TermMatrix and its SuTermMatrixs
    void setStorage(StorageType st, AccessType at);        //!< specify the storage of SuTermMatrix's
    StorageType storageType()                              //! returns storage type (cs, skyline, dense)
    { return computingInfo_.storageType; }
    AccessType storageAccess()                             //! returns storage access (row, col, sym, dual)
    { return computingInfo_.storageAccess; }
    bool isSingleUnknown() const                           //!  true if a single unknown matrix
    { return suTerms_.size()==1; }
    std::set<const Unknown*> rowUnknowns() const;          //!< return set of row Unknowns (say v unknowns)
    std::set<const Unknown*> colUnknowns() const;          //!< return set of col Unknowns (say u unknowns)
    std::set<const Space*> unknownSpaces() const;          //!< list of involved unknown spaces
    ValueType valueType() const;                           //!< return value type (_real,_complex)
    bool isScalar() const;                                 //!< true if all SuTermMatrix's are scalar
    bool hasConstraints() const                            //! true if one constraints pointer is not null
    { return (constraints_u_p!=nullptr || constraints_v_p!=nullptr); }
    const SetOfConstraints* constraints_u() const
    { return constraints_u_p; }
    const SetOfConstraints* constraints_v() const
    { return constraints_v_p; }
    ValueType constraintsValueType() const;      //!< value type of constraints (if no constraint return _real)
    number_t numberOfRows() const;               //!< number of rows (counted in scalar dof)
    number_t numberOfCols() const;               //!< number of columns (counted in scalar dof)
    number_t nnz() const;                        //!< number of non zero coefficients (counted in scalar)
    void markAsComputed(bool isComputed = true); //!< TermMatrix and SutermMatrix's computed state = true or false

    TermMatrix operator()(const Unknown& u, const Unknown& v) const; //!< access to single unknowns block matrix as TermMatrix
    bool hasBlock(const Unknown& u, const Unknown& v) const;         //!< true if TermMatrix has a block (u,v)
    SuTermMatrix& subMatrix(const Unknown* up, const Unknown* vp);               //!< access to SuTermMatrix
    const SuTermMatrix& subMatrix(const Unknown* up, const Unknown* vp) const;   //!< access to SuTermMatrix (const)
    SuTermMatrix* subMatrix_p(const Unknown* up, const Unknown* vp);             //!< access to SuTermMatrix pointer
    const SuTermMatrix* subMatrix_p(const Unknown* up, const Unknown* vp) const; //!< access to SuTermMatrix pointer (const)
    //! return the i-th row or col as a TermVector (i=1,...)
    TermVector& getRowCol(number_t i, AccessType at, TermVector& tv) const;
    TermVector row(number_t r) const;    //!< return the i-th row as a TermVector (i=1,...)
    TermVector column(number_t c) const; //!< return the j-th col as a TermVector (j=1,...)
    template <typename T>
    LargeMatrix<T>& getLargeMatrix(StrucType st=_undefStrucType) const; //!< get internal matrix (restrictive), advanced usage
    template <typename T>
    HMatrix<T,FeDof>& getHMatrix(StrucType st=_undefStrucType) const;   //!< get internal HMatrix if exists, advanced usage

    cit_mustm begin() const        //! iterator to the first element of the SuTermMatrix map (const)
    { return suTerms_.begin(); }
    it_mustm begin()               //! iterator to the first element of the SuTermMatrix map
    { return suTerms_.begin(); }
    cit_mustm end() const          //! iterator to the last element of the SuTermMatrix map (const)
    { return suTerms_.end(); }
    it_mustm end()                 //! iterator to the last element of the SuTermMatrix map
    { return suTerms_.end(); }
    SuTermMatrix* firstSut() const //! first SuTermMatrix as pointer, 0 if void TermMatrix (const)
    {
      if (suTerms_.size()>0) return suTerms_.begin()->second;
      else return nullptr;
    }
    SuTermMatrix* firstSut()       //! first SuTermMatrix as pointer, 0 if void TermMatrix
    {
      if (suTerms_.size()>0) return suTerms_.begin()->second;
      else return nullptr;
    }
    std::map<uvPair, SuTermMatrix*>& suTerms() //! access to suterms
    { return suTerms_; }
    number_t nbTerms() const                   //! number of suTerms
    { return suTerms_.size(); }
    MatrixEntry*& entries()                    //! access to entries pointer (r/w)
    { return entries_p; }
    const MatrixEntry* entries() const         //! access to entries pointer (r)
    { return entries_p; }
    MatrixEntry*& scalar_entries()             //! access to scalar entries pointer (r/w)
    { return scalar_entries_p; }
    const MatrixEntry* scalar_entries() const  //! access to scalar entries pointer (r)
    { return scalar_entries_p; }
    MatrixEntry* actual_entries() const;       //!< return the actual pointer to entries (priority to scalar entry)
    MatrixEntry*& rhs_matrix()                 //! access to rhs matrix pointer (r/w)
    { return rhs_matrix_p; }
    const MatrixEntry* rhs_matrix() const      //! access to rhs matrix pointer (r)
    { return rhs_matrix_p; }
    const std::vector<DofComponent>& cdofsr() const //! access to cdofs_r vector (r)
    { return cdofs_r; }
    const std::vector<DofComponent>& cdofsc() const //! access to cdofs_c vector (r)
    { return cdofs_c; }
    std::vector<DofComponent>& cdofsr()             //! access to cdofs_r vector (r/w)
    { return cdofs_r; }
    std::vector<DofComponent>& cdofsc()             //! access to cdofs_c vector (r/w)
    { return cdofs_c; }

    SymType symmetry() const;                       //!< return global symmetry property
    FactorizationType factorization() const;        //!< return factorization type if factorized

    //computation functions and related tools
    void compute(); //!< compute TermMatrix from bilinear form
    //! compute TermMatrix from a linear combination of TermMatrix's
    void compute(const LcTerm<TermMatrix>& lctm,const string_t& na="");
    //! apply essential conditions to a computed TermMatrix (internal tool)
    void applyEssentialConditions();
    //! apply essential conditions to a computed TermMatrix (user tool)
    void applyEssentialConditions(const EssentialConditions& ecs, const ReductionMethod& rm=ReductionMethod(_pseudoReduction));
    //! apply essential conditions to a computed TermMatrix (user tool)
    void applyEssentialConditions(const EssentialConditions& ecsu, const EssentialConditions& ecsv, const ReductionMethod& rm=ReductionMethod(_pseudoReduction));

    //storage management tools
    std::pair<StorageType,AccessType> findGlobalStorageType() const;      //!< suggest storage type for global representation
    void toScalar(bool keepEntries=false);                                //!< create scalar representation of TermMatrix
    //! create global scalar representation of TermMatrix
    void toGlobal(StorageType st, AccessType at, SymType symt=_noSymmetry, bool keepSuTerms=false);
    void toSkyline(); //!< convert to skyline storage (works only for cs storage single unknown matrix)
    //! merging tool used by toGlobal()
    void mergeNumbering(std::map<const Unknown*, std::list<SuTermMatrix* > >& rcsut, std::map<SuTermMatrix*, std::vector<int_t> >& rcnum, std::vector<DofComponent>& rcdof, AccessType rc);
    //! merging tool used by toGlobal(), fast version
    void mergeNumberingFast(std::map<const Unknown*, std::list<SuTermMatrix*> >& rcsut, std::map<SuTermMatrix*, std::vector<int_t> >& rcnum, std::vector<DofComponent>& rcdof, AccessType rc);

    void addIndices(std::vector<std::set<number_t> >& indices, MatrixStorage* msu, const std::vector<int_t>& rowmap, const std::vector<int_t>& colmap); //!< merging tool used by toGlobal()
    void mergeBlocks();                                                   //!< merge blocks referring to same vector unknowns
    void insertDiagonalBlocks();                                          //!< insert diagonal blocks if missing (multiple unknowns)
    MatrixEntry* matrixData();                                            //!< turns to scalar representation and returns pointer to data

    //essential conditions tools
    void pseudoReduction();                           //!< reduction of essential condition using pseudo reduction method
    void penalizationReduction();                     //!< reduction of essential condition using penalization method
    void realReduction();                             //!< reduction of essential condition using real reduction

    //scalar type moving
    TermMatrix& toReal();                             //!< go to real representation getting the real part when complex
    TermMatrix& toComplex();                          //!< go to complex representation
    TermMatrix& toImag();                             //!< go to real representation getting the imag part when complex
    TermMatrix& toConj();                             //!< change to conj(U)
    TermMatrix& roundToZero(real_t aszero=10*theEpsilon);  //!< round to zero all coefficients close to 0 (|.| < aszero)
    friend TermMatrix real(const TermMatrix& tm);
    friend TermMatrix imag(const TermMatrix& tm);
    friend TermMatrix toComplex(const TermMatrix& tm);
    friend TermMatrix roundToZero(const TermMatrix& tm, real_t aszero);

    // manage unknowns, testfunctions
    void changeUnknown(const Unknown& newu);      //!< change first unknown (col) to an other (be cautious)
    void changeTestFunction(const Unknown& newv); //!< change first testFuncion (row) to an other (be cautious)
    void changeUnknowns(const Unknown& newu, const Unknown& newv); //!< change both first unknown and TestFuncion to others (be cautious)
    void changeUnknown(const Unknown& oldu, const Unknown& newu); //!< change oldu unknown (col) to an other (be cautious)
    void changeTestFunction(const Unknown& oldv, const Unknown& newv); //!< change oldv testfunction (row) to an other (be cautious)
    void changeUnknowns(const Unknowns& oldus, const Unknowns& newvs); //!< change set of current unknowns to others (be cautious)
    void changeTestFunctions(const Unknowns& oldvs, const Unknowns& newvs); //!< change set of current test functions to others (be cautious)
    void changeUnknowns(const Unknowns& oldus, const Unknowns& oldvs, const Unknowns& newus, const Unknowns& newvs); //!< change set of current unknowns and test functions to others (be cautious)

    //value managment
    number_t rowRank(const Dof& dof) const; //!< rank (>=1) in first submatrix row numbering of a Dof
    number_t colRank(const Dof& dof) const; //!< rank (>=1) in first submatrix column numbering of a Dof
    //! rank (>=1) in (u,v) submatrix row numbering of a Dof
    number_t rowRank(const Unknown& u, const Unknown& v, const Dof& dof) const;
    //! rank (>=1) in (u,v) submatrix column numbering of a Dof
    number_t colRank(const Unknown& u, const Unknown& v, const Dof& dof) const;

    Value getValue(number_t, number_t) const;                                       //!< access to first sub-matrix coefficient (i,j >= 1)
    Value getValue(const Unknown&,const Unknown&,number_t, number_t) const;         //!< access to (u,v) sub-matrix coefficient (i,j >= 1)
    void setValue(number_t, number_t, const Value&);                               //!< set value of first sub-matrix coefficient (i,j >= 1) in storage
    void setValue(const Unknown&,const Unknown&,number_t, number_t, const Value&); //!< set value of (u,v) ub-matrix coefficient (i,j >= 1) in storage

    Value getValue(const Dof& du, const Dof& dv) const                              //! access to first sub-matrix coefficient related to dofs (dv,du)
          {return getValue(rowRank(dv),colRank(du));}
    void setValue(const Dof& du, const Dof& dv, const Value& val)                  //! set value of first sub-matrix coefficient related to dofs (dv,du)
          {setValue(rowRank(dv),colRank(du),val);}
    Value getValue(const Unknown& u,const Unknown& v, const Dof& du, const Dof& dv) const             //! access to (u,v) sub-matrix coefficient related to dofs (dv,du)
          {return getValue(u,v,rowRank(u,v,dv),colRank(u,v,du));}
    void setValue(const Unknown& u,const Unknown& v, const Dof& du, const Dof& dv, const Value& val) //! set value of (u,v) sub-matrix coefficient related to dofs (dv,du)
          {setValue(u,v,rowRank(u,v,dv),colRank(u,v,du),val);}

    Value getScalarValue(const Unknown&,const Unknown&, number_t, number_t, dimen_t=0, dimen_t=0) const;        //!< access to scalar (u,v) sub-matrix coefficient (i,j >= 1)
    void setScalarValue(const Unknown&,const Unknown&, number_t, number_t, const Value&, dimen_t=0, dimen_t=0);//!< set value of scalar (u,v) sub_matrix coefficient (i,j >= 1) in storage
    Value getScalarValue(number_t, number_t, dimen_t=0, dimen_t=0) const;           //!< access to scalar first sub-matrix coefficient (i,j >= 1)
    void setScalarValue(number_t, number_t, const Value&, dimen_t=0, dimen_t=0);   //!< set value of scalar first sub_matrix coefficient (i,j >= 1) in storage

    void setRow(const Value&, number_t r1, number_t r2);      //!< set values of first sub-matrix rows r1->r2 (>= 1), no storage change
    void setCol(const Value&, number_t c1, number_t c2);     //!< set values of first sub-matrix cols c1->c2 (>= 1), no storage change
    void setRow(const Unknown&, const Unknown&, const Value&, number_t r1, number_t r2); //!< set values of sub_matrix rows r1->r2 (>= 1), no storage change
    void setCol(const Unknown&, const Unknown&, const Value&, number_t c1, number_t c2); //!< set values of sub_matrix cols c1->c2 (>= 1), no storage change
    void setRow(const Value& val, const Dof& d) //! set values of first sub-matrix row related to dof
    { number_t r=rowRank(d); setRow(val,r,r); }
    void setCol(const Value& val, const Dof& d) //! set values of first sub-matrix col related to dof
    { number_t c=colRank(d); setCol(val,c,c); }
    //! set values of (u,v) sub-matrix row related to dof
    void setRow(const Unknown& u, const Unknown& v, const Value& val, const Dof& d)
    { number_t r=rowRank(u,v,d); setRow(val,r,r); }
    //! set values of (u,v) sub-matrix col related to dof
    void setCol(const Unknown& u, const Unknown& v, const Value& val, const Dof& d)
    { number_t c=colRank(u,v,d); setCol(val,c,c); }

    //norms
    real_t norm2() const;                                                 //!< Frobenius norm: sqrt(sum|a_ij|^2)
    real_t norminfty() const;                                             //!< infinite norm: sup|a_ij|

    //in/out utilities
    void print(std::ostream& out) const;                                      //!< print TermMatrix
    void print(PrintStream& os) const {print(os.currentStream());}
    void printSummary(std::ostream& out) const;                               //!< print summary TermMatrix
    void printSummary(PrintStream& os) const {printSummary(os.currentStream());}
    void viewStorage(std::ostream& out) const;                                //!< print storage on stream
    void viewStorage(PrintStream& os) const {viewStorage(os.currentStream());}

    void saveToFile(const string_t& filename, const std::vector<Parameter>& ps) const;
    void saveToFile(const string_t& filename, StorageType st, number_t prec=fullPrec, bool encode=false, real_t tol=theTolerance) const; //!< save TermMatrix to file (dense or Matlab format)
    void saveToFile(const string_t& filename, StorageType st, bool encode, real_t tol=theTolerance) const //! save TermMatrix to file (dense or Matlab format)
    { saveToFile(filename, st, fullPrec, encode, tol); }

    //various algebraic operation
    template<typename T> TermMatrix& operator*=(const T& t);                //!< TermMatrix *= t
    template<typename T> TermMatrix& operator/=(const T& t);                //!< TermMatrix /= t
    TermMatrix& operator+=(const TermMatrix& tM);                            //!< TermMatrix += TermMatrix
    TermMatrix& operator-=(const TermMatrix& tM);                            //!< TermMatrix -= TermMatrix
    friend TermVector& multMatrixVector(const TermMatrix&,const TermVector&, TermVector&);
    friend TermVector operator*(const TermMatrix&,const TermVector&);
    friend TermVector& multVectorMatrix(const TermVector&, const TermMatrix&, TermVector&);
    friend TermVector& multVectorMatrix(const TermMatrix&, const TermVector&, TermVector&);
    friend TermVector operator*(const TermVector&, const TermMatrix&);
    friend TermMatrix operator*(const TermMatrix&, const TermMatrix&);

    //solvers
    friend TermVector prepareLinearSystem(TermMatrix&, TermVector&, MatrixEntry*&, VectorEntry*&,
                                          StorageType, AccessType, bool);
    friend void updateRhs(TermMatrix&, TermVector&);
    void ldltSolve(const TermVector&, TermVector&);   //!< solve linear system using LDLt factorization
    void ldlstarSolve(const TermVector&,TermVector&); //!< solve linear system using LDL* factorization
    void luSolve(const TermVector&,TermVector&);      //!< solve linear system using LU factorization
    void umfpackSolve(const TermVector&,TermVector&); //!< solve linear system using umfpack
    void sorSolve(const TermVector& V, TermVector& R, const real_t w, SorSolverType sType ); //!< solve linear system using SOR
    void sorUpperSolve(const TermVector& V, TermVector& R, const real_t w );           //!< solve linear system using SOR for upper triangular matrices
    void sorDiagonalMatrixVector(const TermVector& V, TermVector& R, const real_t w ); //!< solve linear system using SOR for block diagonal matrices
    void sorLowerSolve(const TermVector& V, TermVector& R, const real_t w );           //!< solve linear system using SOR for lower triangular matrices
    void sorDiagonalSolve(const TermVector& V, TermVector& R, const real_t w );        //!< solve linear system using SOR for diagonal matrices
};

TermMatrix real(const TermMatrix& tm);        //!< return real part as a real TermMatrix
TermMatrix imag(const TermMatrix& tm);        //!< return imag part as a real TermMatrix
TermMatrix toComplex(const TermMatrix& tm);   //!< return the complex representation of the given TermMatrix
TermMatrix roundToZero(const TermMatrix& tm, real_t aszero);   //!< return the 0 rounded TermMatrix

TermVector& multMatrixVector(const TermMatrix&,const TermVector&, TermVector&);   //!< product TermMatrix * TermVector
TermVector operator*(const TermMatrix&,const TermVector&);     //!< product TermMatrix * TermVector
TermVector& multVectorMatrix(const TermVector&, const TermMatrix&, TermVector&);  //!< product TermVector * TermMatrix
TermVector& multVectorMatrix(const TermMatrix&, const TermVector&, TermVector&);  //!< product TermVector * TermMatrix
TermVector operator*(const TermVector&, const TermMatrix&);    //!< product TermVector * TermMatrix
TermMatrix operator*(const TermMatrix&, const TermMatrix&);    //!< product of TermMatrix

TermVector prepareLinearSystem(TermMatrix&, TermVector&, MatrixEntry*&, VectorEntry*&, StorageType, AccessType, bool); //!< prepare linear system AX=B (internal tool)
void updateRhs(TermMatrix&, TermVector&); //!< prepare rhs B of linear system AX=B (internal tool)
void alignTermMatrix(TermMatrix*&, TermMatrix*&, bool keepMatrix=true); //!< check if same dofs and align Termatrix's if not the same dofs (internal tool)

// ------------------------------------------------------------------------------------------------------------------------
// External functions declaration
// ------------------------------------------------------------------------------------------------------------------------
#include "decLinSys.hpp"
#include "decEigen.hpp"

inline real_t norm2(const TermMatrix& A)
{return A.norm2();}

inline real_t norminfty(const TermMatrix& A)
{return A.norminfty();}

//in/out utils
void buildParamSaveToFile(const Parameter& p, StorageType& st, bool& encodingFileName);
inline void saveToFile(const string_t& filename, const TermMatrix& A)     //! save TermMatrix to file (dense or Matlab format)
{
  std::vector<Parameter> ps(0);
  A.saveToFile(filename, ps);
}
inline void saveToFile(const string_t& filename, const TermMatrix& A, const Parameter& p1)     //! save TermMatrix to file (dense or Matlab format)
{
  std::vector<Parameter> ps={p1};
  A.saveToFile(filename, ps);
}
inline void saveToFile(const string_t& filename, const TermMatrix& A, const Parameter& p1, const Parameter& p2)     //! save TermMatrix to file (dense or Matlab format)
{
  std::vector<Parameter> ps={p1, p2};
  A.saveToFile(filename, ps);
}

inline void saveToFile(const string_t& filename, const TermMatrix& A, StorageType st, bool enc = false)     //! save TermMatrix to file (dense or Matlab format)
{
  warning("deprecated", "saveToFile(String, TermMatrix, StorageType[, bool])", "saveToFile(String, TermMatrix, _storage=?, _encodingFileName/_noEncodingFileName)");
  A.saveToFile(filename, st, enc);
}

//integral representation
TermMatrix integralRepresentation(const Unknown&, const GeomDomain&, const LinearForm&, string_t nam="IR"); //!< integral representation
TermMatrix integralRepresentation(const GeomDomain&, const LinearForm&, string_t nam="IR");         //!< integral representation, no unknown
TermMatrix integralRepresentation(const std::vector<Point>&, const LinearForm&, string_t nam="IR"); //!< integral representation, no unknown, no domain

//----------------------------------------------------------------------------------------------------------
// template functions
//----------------------------------------------------------------------------------------------------------
//! special constructor of a diagonal TermMatrix from a vector
template<typename T>
TermMatrix::TermMatrix(const Unknown& u, const GeomDomain& dom, const Vector<T> & vt, const Parameter& p1)
{
  trace_p->push("TermMatrix::TermMatrix(Vector<T>)");
  if (p1.key() != _pk_name) { error("unexpected_parameter", words("param key", p1.key())); }
  string_t na =p1.get_s();
  entries_p = nullptr;
  scalar_entries_p=nullptr;
  rhs_matrix_p=nullptr;
  constraints_u_p = nullptr;
  constraints_v_p = nullptr;
  termType_ = _termMatrix;
  computingInfo_.noAssembly = false;
  name_ = na;
  TermVector tv(u,dom,vt);
  for (it_mustv it = tv.begin(); it != tv.end(); it++)
    suTerms_[uvPair(&u,&u)]=new SuTermMatrix(*it->second, na+"_"+u.name());
  computingInfo_.isComputed = true;
  trace_p->pop();
}

//! construct TermMatrix from a standard Matrix (virtual unknown)
template<typename T>
TermMatrix::TermMatrix(const Matrix<T>& mat, const Parameter& p1)
{
  if (p1.key() != _pk_name) { error("unexpected_parameter", words("param key", p1.key())); }
  string_t na =p1.get_s();
  buildFromRowDenseMatrix(mat,mat.numberOfRows(),mat.numberOfCols(),na);
}

//! construct TermMatrix from a row dense matrix of size m x n (virtual unknown)
template<typename T>
TermMatrix::TermMatrix(const std::vector<T>& mat, number_t m, number_t n, const string_t& na)
{
  buildFromRowDenseMatrix(mat,m,n,na);
}
//! construct TermMatrix from a row dense matrix of size m x n (virtual unknown)
template <typename T>
TermMatrix::TermMatrix(const Unknown& u, const TestFunction& v, const std::vector<T>& mat, number_t m, number_t n, const string_t& na)
{
  buildFromRowDenseMatrix(u,*u.space(), v, *v.space(), mat, m, n, na);
}

template <typename T>
TermMatrix::TermMatrix(const Unknown& u, const TestFunction& v, const Matrix<T>& mat, const string_t& na)
{
  buildFromRowDenseMatrix(u,*u.space(), v, *v.space(), mat, mat.numberOfRows(), mat.numberOfCols(), na);
}

template<typename T>
void TermMatrix::buildFromRowDenseMatrix(const std::vector<T>& mat, number_t m, number_t n, const string_t& na)
{
  trace_p->push("TermMatrix::TermMatrix(Matrix, Number, Number, String)");

  // find or create spaces
  string_t nam = "C_"+tostring(m);
  Space* spm = Space::findSpace(nam);
  if (spm==nullptr) spm = new Space(_dim=m, _name=nam);
  Space* spn = spm;
  if (n!=m)
  {
    nam = "C_"+tostring(n);
    spn = Space::findSpace(nam);
    if (spn==nullptr) spn=new Space(_dim=n, _name=nam);
  }
  //find or create artificial unknowns related to spaces spm, spn
  nam="#u_"+tostring(m);
  Unknown* up=findUnknown(nam);
  if (up==nullptr) up=new Unknown(*spm, _name=nam);
  Unknown* vp=up;
  if (n!=m)
  {
    nam="#u_"+tostring(n);
    vp=findUnknown(nam);
    if (vp==nullptr) vp=new Unknown(*spn, _name=nam);
  }
  // create SutermMatrix
  buildFromRowDenseMatrix(*up, *spn, *vp, *spn, mat, m, n, nam);
  trace_p->pop();
}

template<typename T>
void TermMatrix::buildFromRowDenseMatrix(const Unknown& u, const Space& spu, const Unknown& v, const Space& spv, const std::vector<T>& mat, number_t m, number_t n, const string_t& na)
{
  trace_p->push("TermMatrix::TermMatrix(u,v,matrix)");
  // create SutermMatrix
  entries_p = nullptr;  scalar_entries_p=nullptr; rhs_matrix_p=nullptr;
  constraints_u_p = nullptr;  constraints_v_p = nullptr;
  termType_ = _termMatrix;
  computingInfo_.noAssembly = false;
  name_ = na;
  suTerms_[uvPair(&u,&v)]=new SuTermMatrix(&u, &spu, &v, &spv, mat, m, n, na+"_"+u.name()+v.name());
  computingInfo_.isComputed = true;
  trace_p->pop();
}

/*
  multiple algebraic operations on TermMatrix
  produce a LcTerm object (linear combination of terms)
  the computation is done by constructor from LcTerm or assign operation of a LcTerm
*/
const TermMatrix& operator+(const TermMatrix&);         //!< unary operator+
LcTerm<TermMatrix> operator-(const TermMatrix&);                                //!< unary operator- (returns a LcTerm)
LcTerm<TermMatrix> operator+(const TermMatrix&, const TermMatrix&);             //!< addition of TermMatrix
LcTerm<TermMatrix> operator-(const TermMatrix&, const TermMatrix&);             //!< substraction of TermMatrix of a TermMatrix
LcTerm<TermMatrix> operator+(const LcTerm<TermMatrix>&, const TermMatrix&);     //!< addition of a TermMatrix to a LcTerm
LcTerm<TermMatrix> operator-(const LcTerm<TermMatrix>&, const TermMatrix&);     //!< substraction of a TermMatrix of a LcTerm
LcTerm<TermMatrix> operator+(const TermMatrix&, const LcTerm<TermMatrix>&);     //!< addition of a TermMatrix to a LcTerm
LcTerm<TermMatrix> operator-(const TermMatrix&, const LcTerm<TermMatrix>&);     //!< substraction of a LcTerm from a TermMatrix
TermVector operator*(const LcTerm<TermMatrix>&, const TermVector&);             //!< LcTerm * TermVector

//! product of TermMatrix by a real or a complex (template T)
template<typename T>
LcTerm<TermMatrix> operator*(const TermMatrix& tv, const T& t)
{
  return LcTerm<TermMatrix>(&tv,t);
}
//! product of TermMatrix by a real or a complex (template T)
template<typename T>
LcTerm<TermMatrix> operator*(const T& t, const TermMatrix& tv)
{
  return LcTerm<TermMatrix>(&tv,t);
}
//! division of TermMatrix by a real or a complex (template T)
template<typename T>
LcTerm<TermMatrix> operator/(const TermMatrix& tv, const T& t)
{
  return LcTerm<TermMatrix>(&tv,1./t);
}

//! product assignment by a real or a complex (template T)
template<typename T>
TermMatrix& TermMatrix::operator*=(const T& t)
{
  for(it_mustm it = suTerms_.begin(); it != suTerms_.end(); it++) *it->second *= t;
  bilinForm_*=t;
  return *this;
}

//! division assignment by a real or a complex (template T)
template<typename T>
TermMatrix& TermMatrix::operator/=(const T& t)
{
  if(t == 0) error("divBy0");
  for(it_mustm it = suTerms_.begin(); it != suTerms_.end(); it++) *it->second /= t;
  bilinForm_/=t;
  return *this;
}

//! product of TermMatrix by a real or a complex (template T)
template<typename T>
TermMatrix operator*(const TermMatrix & tM, const T& t)
{
  TermMatrix R(tM);
  return R*=t;
}

//! initialize a TermVector consistent with matrix column unknown (col=true), with matrix row unknown (col=false)
template<typename T>
void TermMatrix::initTermVectorT(TermVector& tv, const T& v, bool col) const
{
  string_t na = tv.name(); //preserving only name
  tv.clear();
  tv.name() = na;
  //make list of col unknowns
  std::map<const Unknown*, SuTermMatrix*> terms;
  for(cit_mustm itA = begin(); itA != end(); itA++)
  {
    const Unknown* u;
    if(col) u = itA->first.first;
    else    u = itA->first.second;
    SuTermMatrix* Auv = itA->second;
    if(terms.find(u) == terms.end()) terms[u] = Auv;
  }
  std::map<const Unknown*, SuTermMatrix*>::iterator it;
  for(it = terms.begin(); it != terms.end(); it++)
  {
    const Unknown* u = it->first;
    SuTermMatrix* sut = it->second;
    Space* sp = sut->space_up();
    if(!col) sp = sut->space_vp();
    SuTermVector* sutX = new SuTermVector("", u, sp, v);
    tv.insert(u, sutX);
  }
}

/*! get internal matrix (restrictive), i.e LargeMatrix object if available
    available only for single unknowns TermMatrix and multiple unknowns TermMatrix having a global representation
    if scalar and vector representations both exist, the scalar representation is prior except if StrucType argument is specified
    user has to specify the LargeMatrix type:
        TermMatrix M(intg(omega,u*v));
        LargeMatrix& Alm=M.getMatrix<Real>();
    It returns a reference to the LargeMatrix
    Note: designed for advanced usage
*/
template <typename T>
LargeMatrix<T>& TermMatrix::getLargeMatrix(StrucType str) const
{
  number_t nbsut=suTerms_.size();
  if (nbsut==1) return suTerms_.begin()->second->getLargeMatrix<T>(str);
  if (scalar_entries_p!=nullptr && str!=_matrix) return scalar_entries_p->getLargeMatrix<T>();
  if (entries_p!=nullptr) return entries_p->getLargeMatrix<T>();
  where("TermMatrix::getLargeMatrix(StrucType)");
  error("term_no_entries");
  return *(new LargeMatrix<T>());  //fake return
}

/*! get internal H-matrix (restrictive), i.e HMatrix object if available
    available only for single unknown TermMatrix
    if scalar and vector representations both exist, the scalar representation is prior except if StrucType argument is specified
    It returns a reference to a HMatrix
    Note: designed for advanced usage
*/
template <typename T>
HMatrix<T,FeDof>& TermMatrix::getHMatrix(StrucType str) const
{
  number_t nbsut=suTerms_.size();
  if (nbsut==1) return suTerms_.begin()->second->getHMatrix<T>(str);
  where("TermMatrix::getHMatrix(StrucType)");
  error("term_no_entries");
  return *(new HMatrix<T,FeDof>());  //fake return
}

/* ================================================================================================
                       computation algorithm for bilinear form as linear form
                        (placed here because it uses TermMatrix's machinery)
   ================================================================================================ */
/*!
  Computation of a linear form defined from a bilinear form and a TermVector to apply to
                   l(v) = a(U,v) or l(u)=a(u,V)   where U,V are some term vectors
  \param lf: a pair of BasicLinearForm and coefficient
  \param val: vector of values to fill in
  \param vt: type of value type

  example:  intg_gamma grad(U)|grad(v) dy  U a termvector
             int_sigma intg_gamma (U*G)*v   U a termvector

  The algorithm is the most lazy one:
      - go to the TermMatrix machinery to compute matrix A from a(u,v)
      - do the product A*U or V*A
      - add the product result to vector val
*/

template<typename T, typename K>
void computeBilAsLin(const std::pair<BasicLinearForm*, complex_t>& lf, Vector<T>& val, K& vt)
{
  if(lf.first->type()!=_bilinearAsLinear)
  {
    where("SuTermVector::computeBilAsLin(...)");
    error("lform_not_handled", words("form type", lf.first->type()));
  }
  BilinearFormAsLinearForm* bfaslf = lf.first->asBilinearAsLinearForm();
  K coef = complexToT<K>(lf.second);
  //create TermMatrix
  SuBilinearForm subf(std::vector<bfPair>(1, bfPair(bfaslf->bbf(), lf.second)));
  BilinearForm bf(subf);
  TermMatrix S(bf);
  //do product
  TermVector R;
  if(bfaslf->asUnknown_)  R=S* *bfaslf->termVector();
  else                     R=*bfaslf->termVector()*S;
  //add R to vector val
  Vector<T> r;
  R.asVector(r);
  typename  Vector<T>::iterator itv=val.begin(), itr=r.begin();
  for(;itr!=r.end(); ++itr, ++itv) *itv+=*itr;
}

} //end of namespace xlifepp

#endif /* TERM_MATRIX_HPP */
