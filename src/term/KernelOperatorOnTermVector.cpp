/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file KernelOperatorOnTermVector.cpp
  \authors E. Lunéville
  \since 13 dec 2015
  \date 13 dec 2015

  \brief Implementation of xlifepp::KernelOperatorOnTermVector class members and related utilities
 */

#include "KernelOperatorOnTermVector.hpp"

namespace xlifepp
{

//=================================================================================
// operation on TermVector and (OperatorOn)Kernel
//=================================================================================
ValueType KernelOperatorOnTermVector::valueType() const
{
  if(tv_->valueType()==_complex || opker_.valueType()==_complex) return _complex;
  return _real;
}

KernelOperatorOnTermVector operator*(const TermVector& tv, const Kernel& ker)  // tv * ker
{
  OperatorOnKernel opk(ker);
  return KernelOperatorOnTermVector(opk, _product, tv, false);
}
KernelOperatorOnTermVector operator|(const TermVector& tv, const Kernel& ker)  // tv | ker
{
  OperatorOnKernel opk(ker);
  return KernelOperatorOnTermVector(opk, _innerProduct, tv, false);
}
KernelOperatorOnTermVector operator^(const TermVector& tv, const Kernel& ker)  // tv ^ ker
{
  OperatorOnKernel opk(ker);
  return KernelOperatorOnTermVector(opk, _crossProduct, tv, false);
}
KernelOperatorOnTermVector operator%(const TermVector& tv, const Kernel& ker)  // tv % ker
{
  OperatorOnKernel opk(ker);
  return KernelOperatorOnTermVector(opk, _contractedProduct, tv, false);
}
KernelOperatorOnTermVector operator*(const Kernel& ker, const TermVector& tv)  // ker * tv
{
  OperatorOnKernel opk(ker);
  return KernelOperatorOnTermVector(opk, _product, tv, true);
}
KernelOperatorOnTermVector operator|(const Kernel& ker, const TermVector& tv)  // ker | tv
{
  OperatorOnKernel opk(ker);
  return KernelOperatorOnTermVector(opk, _innerProduct, tv, true);
}
KernelOperatorOnTermVector operator^(const Kernel& ker, const TermVector& tv)  // ker ^ tv
{
  OperatorOnKernel opk(ker);
  return KernelOperatorOnTermVector(opk, _crossProduct, tv, true);
}
KernelOperatorOnTermVector operator%(const Kernel& ker, const TermVector& tv)  // ker * tv
{
  OperatorOnKernel opk(ker);
  return KernelOperatorOnTermVector(opk, _contractedProduct, tv, true);
}
KernelOperatorOnTermVector operator*(const TermVector& tv, const OperatorOnKernel& opk)  // tv * opker
{
  return KernelOperatorOnTermVector(opk, _product, tv, false);
}
KernelOperatorOnTermVector operator|(const TermVector& tv, const OperatorOnKernel& opk)  // tv | opker
{
  return KernelOperatorOnTermVector(opk, _innerProduct, tv, false);
}
KernelOperatorOnTermVector operator^(const TermVector& tv, const OperatorOnKernel& opk)  // tv ^ opker
{
  return KernelOperatorOnTermVector(opk, _crossProduct, tv, false);
}
KernelOperatorOnTermVector operator%(const TermVector& tv, const OperatorOnKernel& opk)  // tv % opker
{
  return KernelOperatorOnTermVector(opk, _contractedProduct, tv, false);
}
KernelOperatorOnTermVector operator*(const OperatorOnKernel& opk, const TermVector& tv) // opker * tv
{
  return KernelOperatorOnTermVector(opk, _product, tv, true);
}
KernelOperatorOnTermVector operator|(const OperatorOnKernel& opk, const TermVector& tv) // opker | tv
{
  return KernelOperatorOnTermVector(opk, _innerProduct, tv, true);
}
KernelOperatorOnTermVector operator^(const OperatorOnKernel& opk, const TermVector& tv) // opker ^ tv
{
  return KernelOperatorOnTermVector(opk, _crossProduct, tv, true);
}
KernelOperatorOnTermVector operator%(const OperatorOnKernel& opk, const TermVector& tv) // opker % tv
{
  return KernelOperatorOnTermVector(opk, _contractedProduct, tv, true);
}

//move KernelOperatorTermVector to OperatorOnUnknown
OperatorOnUnknown toOperatorOnUnknown(const KernelOperatorOnTermVector& koptv)
{
  const TermVector* tv=koptv.termVector();
  if(tv==nullptr)
  {
    where("toOperatorOnUnknown(KernelOperatorOnTermVector)");
    error("null_pointer","koptv");
  }
  if(!tv->isSingleUnknown())
  {
    where("toOperatorOnUnknown(KernelOperatorOnTermVector)");
    error("ir_su_tv_only");
  }
  const Unknown* up=tv->unknown();
  const OperatorOnKernel& opk=koptv.opker();
  OperatorOnUnknown opv(*up,_id);
  AlgebraicOperator aopk= koptv.algop();
  if(koptv.termAtLeft)  //take opker aop id(v) as new OperatorOnUnknown
  {

    opv.leftOperand()=new Operand(opk,aopk);
    opv.updateReturnedType(aopk, opk.valueType(), opk.strucType(), opk.dims(), true);
  }
  else  //take id(v) aop opker as syntax
  {
    opv.rightOperand()=new Operand(opk,aopk);
    opv.updateReturnedType(aopk, opk.valueType(), opk.strucType(), opk.dims(), false);
  }
  return opv;
}

//=================================================================================
// intg involving KernelOperatorOnTermVector
//=================================================================================

std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv, const std::vector<Parameter>& ps)
{
  trace_p->push("Domain, KernelOperatorOnTermVector, Parameters");
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_method);
  params.insert(_pk_quad);
  params.insert(_pk_order);
  params.insert(_pk_symmetry);
  params.insert(_pk_isogeo);

  real_t bound=0.;
  bool isogeom=false;
  IntegrationMethod* meth=nullptr;
  IntegrationMethods meths;
  QuadRule qr=_defaultRule;
  number_t qo=0;
  SymType st=_undefSymmetry;
  ComputationType ct=_undefComputation;
  const GeomDomain* extdom = nullptr;

  for (number_t i=0; i < ps.size(); ++i)
  {
    intgLfBuildParam(ps[i], bound, isogeom, meth, meths, qr, qo, st, ct, extdom);

    ParameterKey key=ps[i].key();
    if (params.find(key) != params.end()) { params.erase(key); }
    else {
      if (usedParams.find(key) == usedParams.end())
      { error("lform_unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);

    intgLfParamCompatibility(key, usedParams);
  }

  // (quad, order) has to be set (or method)
  if (params.find(_pk_quad) != params.end() && params.find(_pk_order) == params.end()) { error("param_missing","order"); }
  if (params.find(_pk_order) != params.end() && params.find(_pk_quad) == params.end()) { error("param_missing","quad"); }

  OperatorOnUnknown* opv = new OperatorOnUnknown(toOperatorOnUnknown(koptv));
  BasicLinearForm* bf;
  if (usedParams.find(_pk_method) != usedParams.end())
  {
    // key _method was given
    if (meth == nullptr)
    {
      // IntegrationMethods is given
      bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, *opv, meths));
    }
    else
    {
      // IntegrationMethod is given
      bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, *opv, *meth));
    }
  }
  else
  {
    // key _method was not given, we consider _quad and _order (possibly with their default values)
    bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, *opv, qr, qo));
  }
  bf->isoGeometric = isogeom;
  if(extdom!=nullptr)
    warning("free_warning","extension_domain key not yet handled in intg involving KernelOperatorOnTermVector");
  SuLinearForm sulf;
  sulf.lfs().push_back(lfPair(bf, 1.));
  trace_p->pop();
  return std::pair<LinearForm,const TermVector*>(sulf,koptv.termVector());
}

std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv)
{
  std::vector<Parameter> ps(0);
  return intg(dom, koptv, ps);
}

std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
                                             const Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  return intg(dom, koptv, ps);
}

std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
                                             const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps={p1, p2};
  return intg(dom, koptv, ps);
}

std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
                                             const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps={p1, p2, p3};
  return intg(dom, koptv, ps);
}

std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
                                             const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                             const Parameter& p4)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  return intg(dom, koptv, ps);
}

std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
                                             const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                             const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  return intg(dom, koptv, ps);
}

//=================================================================================
// operation on OperatorOnUnknown and  KernelOperatorOnTermVector
//=================================================================================
ValueType KernelOperatorOnTermVectorAndUnknown::valueType() const
{
  if(koptv_.valueType()==_complex || opv_.valueType()==_complex) return _complex;
  return _real;
}

KernelOperatorOnTermVectorAndUnknown operator*(const OperatorOnUnknown& opv, const KernelOperatorOnTermVector&  koptv)  // opv * koptv
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _product, opv, false);
}
KernelOperatorOnTermVectorAndUnknown operator|(const OperatorOnUnknown& opv, const KernelOperatorOnTermVector&  koptv)  // opv | koptv
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _innerProduct, opv, false);
}
KernelOperatorOnTermVectorAndUnknown operator^(const OperatorOnUnknown& opv, const KernelOperatorOnTermVector&  koptv)  // opv ^ koptv
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _crossProduct, opv, false);
}
KernelOperatorOnTermVectorAndUnknown operator%(const OperatorOnUnknown& opv, const KernelOperatorOnTermVector&  koptv)  // opv % koptv
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _contractedProduct, opv, false);
}
KernelOperatorOnTermVectorAndUnknown operator*(const KernelOperatorOnTermVector&  koptv, const OperatorOnUnknown& opv)  // kopv * opv
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _product, opv, true);
}
KernelOperatorOnTermVectorAndUnknown operator|(const KernelOperatorOnTermVector&  koptv, const OperatorOnUnknown& opv)  // kopv | opv
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _innerProduct, opv, true);
}
KernelOperatorOnTermVectorAndUnknown operator^(const KernelOperatorOnTermVector&  koptv, const OperatorOnUnknown& opv)  // kopv ^ opv
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _crossProduct, opv, true);
}
KernelOperatorOnTermVectorAndUnknown operator%(const KernelOperatorOnTermVector&  koptv, const OperatorOnUnknown& opv)  // kopv * �pv
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _contractedProduct, opv, true);
}
KernelOperatorOnTermVectorAndUnknown operator*(const Unknown& v, const KernelOperatorOnTermVector& koptv)  // v * koptv
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _product, OperatorOnUnknown(v,_id), false);
}
KernelOperatorOnTermVectorAndUnknown operator|(const Unknown& v, const KernelOperatorOnTermVector& koptv)  // v | koptv
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _innerProduct, OperatorOnUnknown(v,_id), false);
}
KernelOperatorOnTermVectorAndUnknown operator^(const Unknown& v, const KernelOperatorOnTermVector& koptv)  // v ^ koptv
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _crossProduct, OperatorOnUnknown(v,_id), false);
}
KernelOperatorOnTermVectorAndUnknown operator%(const Unknown& v, const KernelOperatorOnTermVector& koptv)  // v % koptv
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _contractedProduct, OperatorOnUnknown(v,_id), false);
}
KernelOperatorOnTermVectorAndUnknown operator*(const KernelOperatorOnTermVector& koptv, const Unknown& v) // koptv * v
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _product, OperatorOnUnknown(v,_id), true);
}
KernelOperatorOnTermVectorAndUnknown operator|(const KernelOperatorOnTermVector& koptv, const Unknown& v) // koptv | v
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _innerProduct, OperatorOnUnknown(v,_id), true);
}
KernelOperatorOnTermVectorAndUnknown operator^(const KernelOperatorOnTermVector& koptv, const Unknown& v) // koptv ^ v
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _crossProduct, OperatorOnUnknown(v,_id), true);
}
KernelOperatorOnTermVectorAndUnknown operator%(const KernelOperatorOnTermVector& koptv, const Unknown& v) // koptv % v
{
  return KernelOperatorOnTermVectorAndUnknown(koptv, _contractedProduct, OperatorOnUnknown(v,_id), true);
}

//----------------------------------------
// DEPRECATED (to be removed)
//----------------------------------------

//user functions like constructors of single integral involving KernelOperatorOnTermVector: int_dom koptv
std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv, const IntegrationMethod& im)  // construct a simple intg linear form from KernelOperatorOnTermVector
{
  warning("deprecated", "intg(Domain, ..., IntegrationMethod)", "intg(Domain, ..., _method=?)");
  OperatorOnUnknown* opv = new OperatorOnUnknown(toOperatorOnUnknown(koptv));
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, *opv, IntegrationMethods(im)));
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return std::pair<LinearForm,const TermVector*>(lc,koptv.termVector());
}

std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv, const IntegrationMethods& ims)  // construct a simple intg linear form from KernelOperatorOnTermVector
{
  warning("deprecated", "intg(Domain, ..., IntegrationMethods)", "intg(Domain, ..., _method=?)");
  OperatorOnUnknown* opv = new OperatorOnUnknown(toOperatorOnUnknown(koptv));
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, *opv, ims));
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return std::pair<LinearForm,const TermVector*>(lc,koptv.termVector());
}

std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
    QuadRule qr, number_t qro) // construct a simple intg linear form from KernelOperatorTermVector (shortcut)
{
  warning("deprecated", "intg(Domain, ..., QuadRule, Number)", "intg(Domain, ..., _quad=?, _order=?)");
  OperatorOnUnknown* opv = new OperatorOnUnknown(toOperatorOnUnknown(koptv));
  BasicLinearForm* bf = static_cast<BasicLinearForm*>(new IntgLinearForm(dom, *opv, IntegrationMethods(QuadratureIM(qr,qro))));
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bf, 1.));
  return std::pair<LinearForm,const TermVector*>(lc,koptv.termVector());
}

} //end of namespace xlifepp
