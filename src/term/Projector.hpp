/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Projector.hpp
  \authors E. Lunéville
  \since 18 nov 2016
  \date 18 nov 20163

  \brief Definition of the xlifepp::Projector class

  xlifepp::Projector class handles projection from a space to an other one
                                                                                       2
               P: Vh --> Wh       where ph in Wh is the solution of   Min   ||ph -vh||
                   vh --> ph=P.vh                                    wh in Wh

            where ||.|| is a norm on space Wh defined from any definite positive symetric bilinear form a(.,.)
            So ph is the unique solution of  a(ph,wh)=a(vh,wh) for any wh in Wh
            and the vector P representing ph the projection of vh represented by the vector V
            is the unique solution of the linear system AP=BV where  Aij=a(wi,wj) and Bij=a(wi,vj)
            (wi) basis of W and (vi) basis of V

            Standard norms are pre-defined (Omega: domain of the norm):
               L2   : a(u,v)=intg_Omega u.v
               H1   : a(u,v)=intg_Omega u.v + intg_Omega grad(u)|grad(v)
               H10  : a(u,v)=intg_Omega grad(u)|grad(v)

            that are related to the enumeration:
            ProjectorType {_noProjectorType=0, _L2Projector,_H1Projector, _H10Projector}

            So this class mainly manages the data:
               ProjectorType projType_: type of the projection
               TermMatrix * A_, B_: projection matrices
               BilinearForm* a_: bilinear form used for projection

            and the operators:
               TermVector operator()(TermVector V);  returning the projection of V
               TermVector operator*(TermVector V);   returning the projection of V
               TermMatrix operator*(TermMatrix M):   returning P*M as a TermMatrix

            Exemple of usage:
              Space V(...), Space W(...);// definition of space
              Projector PVW(V, W,_L2);   //create the L2 projector from V to W
              TermVector U(...);         //create a TermVector related to V space
              TermVector Up = PVW(U);    //create the projection of U
              TermVector Uq = PVW*U;     //create the projection of U, alternate syntax
              TermMatrix M(...);         //create a TermMatrix on V space
              TermMatrix PM=PVW*M;       //create the TermMatrix P*M acting from V to W

            Rmk: as these class will allocate internal unknowns that are not related to user unknowns
                 the product of projector by a TermVector or a TermMatrix do not check unknown compatibility

*/

#ifndef PROJECTOR_HPP
#define PROJECTOR_HPP

#include "config.h"
#include "form.h"
#include "TermMatrix.hpp"

namespace xlifepp
{

//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const ProjectorType& pt);

//=================================================================================
/*!
   \class Projector
   end user class handling  projection from a space V to a space W
*/
//=================================================================================
class Projector
{
public:
    ProjectorType projectorType;  //!< type of projection (see enum ProjectionType)
    string_t name;                //!< name of projection

protected:
    Space* V_=nullptr, *W_=nullptr;               //!< spaces involved
    Unknown* u_V=nullptr, *u_W=nullptr;           //!< internal unknowns
    TestFunction * v_W=nullptr;                   //!< internal testfunction
    const GeomDomain* domain_=nullptr;            //!< domain  involved in norm
    BilinearForm* a_V=nullptr, *a_W=nullptr;      //!< bilinear forms used by projector
    mutable TermMatrix* A_=nullptr, *B_=nullptr;  //!< matrix a(W,W) and a(W,V)
    mutable TermMatrix* invA_B=nullptr;           //!< matrix inv(A)*B if allocated
    bool deluV=false, deluW=false, delvW=false;   //!< flag to delete u_V, u_W, v_W

private:
    Projector(const Projector& P)  //!< forbidden copy constructor
    {error("forbidden","Projector::Projector(const Projector& )");}
    Projector& operator=(const Projector& P)  //!< forbidden assign constructor
    {error("forbidden","Projector::operator=(const Projector& )"); return *this;}

public:
    Projector(Space& V, Space& W, ProjectorType pt=_L2Projector,
              const string_t& na="");                                      //!< constructor with predefined projection
    Projector(Space& V, Space& W, const GeomDomain&,
              ProjectorType pt=_L2Projector, const string_t& na="");       //!< constructor with predefined projection with domain
    Projector(Space& V, Space& W, BilinearForm& a, const string_t& na=""); //!< constructor with user bilinear form
    //constructor with nbc additionnal arguments
    Projector(Space& V, dimen_t nbcV, Space& W, dimen_t nbcW,
              ProjectorType pt=_L2Projector, const string_t& na="");       //!< constructor with predefined projection
    Projector(Space& V, dimen_t nbcV, Space& W, dimen_t nbcW, const GeomDomain&,
              ProjectorType pt=_L2Projector, const string_t& na="");       //!< constructor with predefined projection with domain
    Projector(Space& V, dimen_t nbcV, Space& W, dimen_t nbcW, BilinearForm& a,
              const string_t& na="");                                      //!< constructor with user bilinear form
    ~Projector();

   //accessors
    Unknown* uV() { return u_V;}
    Unknown* uW()  { return u_W;}
    Space* V() const { return V_;}
    Space* W() const { return W_;}
    const GeomDomain* domain() { return domain_;}
    BilinearForm* aV() { return a_V;}
    BilinearForm* aW() { return a_W;}
    TermMatrix* A() { return A_; }
    TermMatrix* B() { return B_; }
    TermMatrix* invAB() { return invA_B; }

    void init(dimen_t nbc_V, dimen_t nbc_W);                 //!< initialize projector
    TermVector operator()(const TermVector&) const;          //!< projection of a TermVector
    TermVector operator()(TermVector&,const Unknown&) const; //!< projection of a TermVector, specifying result unknown
    TermVector& operator()(TermVector&, TermVector&) const;  //!< projection of a TermVector, specifying TermVector result
    TermMatrix& asTermMatrix(const Unknown&,const Unknown&,
                             const string_t& = "Projector::invA_B");//!< return projector as TermMatrix (build inv(A)*B)
    void print(std::ostream&) const;                        //!< print info on stream
    void print(PrintStream& os) const {print(os.currentStream());}

    friend TermVector operator*(const Projector&, const TermVector&);
    friend std::ostream& operator<<(std::ostream&, const Projector&);

    static std::vector<Projector *> theProjectors; //!< list of existing Projector objects
    static void printAllProjectors(std::ostream&);  //!< print the list of Projector objects in memory
    static void printAllProjectors(PrintStream& os) {printAllProjectors(os.currentStream());}

    static void clearGlobalVector();               //!< delete all Projector objects
    friend Projector& findProjector(Space& V, dimen_t nbcV, Space& W, dimen_t nbcW, ProjectorType pt); //!< create if not exist
};

TermVector operator*(const Projector&, const TermVector&);  //!< projection of a TermVector using *
std::ostream& operator<<(std::ostream&, const Projector&);  //!< output on stream

TermVector projection(const TermVector&, Space& W, ProjectorType pt=_L2Projector, KeepStatus =_keep);
TermVector projection(const TermVector&, Space& W, const Unknown& u, ProjectorType pt=_L2Projector, KeepStatus =_keep);
TermVector projection(const TermVector&, Space& W, dimen_t nbcW, ProjectorType pt=_L2Projector, KeepStatus =_keep);
TermVector projection(const TermVector&, Space& W, dimen_t nbcW, const Unknown& u, ProjectorType pt=_L2Projector, KeepStatus =_keep);

} //end of namespace xlifepp

#endif /* PROJECTOR_HPP */
