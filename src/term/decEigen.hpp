/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file decEigen.hpp
  \author Y. Lafranche
  \since 25 jul 2016
  \date 25 jul 2016

  \brief Declaration of functions related to the computation of eigen elements

  This file contains declarations and implementation intentionaly gathered into this place.
  It is included in TermMatrix.hpp only and should not been used elsewhere.
*/

//! Utility function that creates a vector from a list of parameters of variable length
// To be replaced by an initializer list when C++11 will be enabled.
std::vector<Parameter> makeParList(int nbpar, ...);

//! Main entry point for non specific eigenvalue solver
EigenElements eigenSolve(TermMatrix* A, TermMatrix* B, std::vector<Parameter> ps);
//@{
//! user front-end for any eigen solvers
inline EigenElements eigenSolve(TermMatrix& A, TermMatrix& B)
      { return eigenSolve(&A, &B, std::vector<Parameter>()); }
inline EigenElements eigenSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1)
      { return eigenSolve(&A, &B, std::vector<Parameter>(1,p1)); }
inline EigenElements eigenSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2)
      { return eigenSolve(&A, &B, makeParList(2, &p1, &p2)); }
inline EigenElements eigenSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3)
      { return eigenSolve(&A, &B, makeParList(3, &p1, &p2, &p3)); }
inline EigenElements eigenSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4)
      { return eigenSolve(&A, &B, makeParList(4, &p1, &p2, &p3, &p4)); }
inline EigenElements eigenSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5)
      { return eigenSolve(&A, &B, makeParList(5, &p1, &p2, &p3, &p4, &p5)); }
inline EigenElements eigenSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5, const Parameter& p6)
      { return eigenSolve(&A, &B, makeParList(6, &p1, &p2, &p3, &p4, &p5, &p6)); }
inline EigenElements eigenSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
      { return eigenSolve(&A, &B, makeParList(7, &p1, &p2, &p3, &p4, &p5, &p6, &p7)); }
inline EigenElements eigenSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                const Parameter& p8)
      { return eigenSolve(&A, &B, makeParList(8, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8)); }
inline EigenElements eigenSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                const Parameter& p8, const Parameter& p9)
      { return eigenSolve(&A, &B, makeParList(9, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9)); }
inline EigenElements eigenSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                const Parameter& p8, const Parameter& p9, const Parameter& p10)
      { return eigenSolve(&A, &B, makeParList(10, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9, &p10)); }
inline EigenElements eigenSolve(TermMatrix& A)
      { return eigenSolve(&A, 0, std::vector<Parameter>()); }
inline EigenElements eigenSolve(TermMatrix& A, const Parameter& p1)
      { return eigenSolve(&A, 0, std::vector<Parameter>(1,p1)); }
inline EigenElements eigenSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2)
      { return eigenSolve(&A, 0, makeParList(2, &p1, &p2)); }
inline EigenElements eigenSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3)
      { return eigenSolve(&A, 0, makeParList(3, &p1, &p2, &p3)); }
inline EigenElements eigenSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4)
      { return eigenSolve(&A, 0, makeParList(4, &p1, &p2, &p3, &p4)); }
inline EigenElements eigenSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5)
      { return eigenSolve(&A, 0, makeParList(5, &p1, &p2, &p3, &p4, &p5)); }
inline EigenElements eigenSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5, const Parameter& p6)
      { return eigenSolve(&A, 0, makeParList(6, &p1, &p2, &p3, &p4, &p5, &p6)); }
inline EigenElements eigenSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
      { return eigenSolve(&A, 0, makeParList(7, &p1, &p2, &p3, &p4, &p5, &p6, &p7)); }
inline EigenElements eigenSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                const Parameter& p8)
      { return eigenSolve(&A, 0, makeParList(8, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8)); }
inline EigenElements eigenSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                const Parameter& p8, const Parameter& p9)
      { return eigenSolve(&A, 0, makeParList(9, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9)); }
inline EigenElements eigenSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                const Parameter& p8, const Parameter& p9, const Parameter& p10)
      { return eigenSolve(&A, 0, makeParList(10, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9, &p10)); }
//@}

//! Main entry point for intern eigenvalue solver
EigenElements eigenInternSolve(TermMatrix* A, TermMatrix* B, const std::vector<Parameter>& ps);
//@{
//! user front-end for internal eigen solver
inline EigenElements eigenInternSolve(TermMatrix& A, TermMatrix& B)
      { return eigenInternSolve(&A, &B, std::vector<Parameter>()); }
inline EigenElements eigenInternSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1)
      { return eigenInternSolve(&A, &B, std::vector<Parameter>(1,p1)); }
inline EigenElements eigenInternSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2)
      { return eigenInternSolve(&A, &B, makeParList(2, &p1, &p2)); }
inline EigenElements eigenInternSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3)
      { return eigenInternSolve(&A, &B, makeParList(3, &p1, &p2, &p3)); }
inline EigenElements eigenInternSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4)
      { return eigenInternSolve(&A, &B, makeParList(4, &p1, &p2, &p3, &p4)); }
inline EigenElements eigenInternSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4, const Parameter& p5)
      { return eigenInternSolve(&A, &B, makeParList(5, &p1, &p2, &p3, &p4, &p5)); }
inline EigenElements eigenInternSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4, const Parameter& p5, const Parameter& p6)
      { return eigenInternSolve(&A, &B, makeParList(6, &p1, &p2, &p3, &p4, &p5, &p6)); }
inline EigenElements eigenInternSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
      { return eigenInternSolve(&A, &B, makeParList(7, &p1, &p2, &p3, &p4, &p5, &p6, &p7)); }
inline EigenElements eigenInternSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                      const Parameter& p8)
      { return eigenInternSolve(&A, &B, makeParList(8, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8)); }
inline EigenElements eigenInternSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                      const Parameter& p8, const Parameter& p9)
      { return eigenInternSolve(&A, &B, makeParList(9, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9)); }
inline EigenElements eigenInternSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                      const Parameter& p8, const Parameter& p9, const Parameter& p10)
      { return eigenInternSolve(&A, &B, makeParList(10, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9, &p10)); }
inline EigenElements eigenInternSolve(TermMatrix& A)
      { return eigenInternSolve(&A, 0, std::vector<Parameter>()); }
inline EigenElements eigenInternSolve(TermMatrix& A, const Parameter& p1)
      { return eigenInternSolve(&A, 0, std::vector<Parameter>(1,p1)); }
inline EigenElements eigenInternSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2)
      { return eigenInternSolve(&A, 0, makeParList(2, &p1, &p2)); }
inline EigenElements eigenInternSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3)
      { return eigenInternSolve(&A, 0, makeParList(3, &p1, &p2, &p3)); }
inline EigenElements eigenInternSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4)
      { return eigenInternSolve(&A, 0, makeParList(4, &p1, &p2, &p3, &p4)); }
inline EigenElements eigenInternSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4, const Parameter& p5)
      { return eigenInternSolve(&A, 0, makeParList(5, &p1, &p2, &p3, &p4, &p5)); }
inline EigenElements eigenInternSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4, const Parameter& p5, const Parameter& p6)
      { return eigenInternSolve(&A, 0, makeParList(6, &p1, &p2, &p3, &p4, &p5, &p6)); }
inline EigenElements eigenInternSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
      { return eigenInternSolve(&A, 0, makeParList(7, &p1, &p2, &p3, &p4, &p5, &p6, &p7)); }
inline EigenElements eigenInternSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                      const Parameter& p8)
      { return eigenInternSolve(&A, 0, makeParList(8, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8)); }
inline EigenElements eigenInternSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                      const Parameter& p8, const Parameter& p9)
      { return eigenInternSolve(&A, 0, makeParList(9, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9)); }
inline EigenElements eigenInternSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                      const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                      const Parameter& p8, const Parameter& p9, const Parameter& p10)
      { return eigenInternSolve(&A, 0, makeParList(10, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9, &p10)); }
//@}

//! Main entry point for Arpack eigenvalue solver
EigenElements arpackSolve(TermMatrix* A, TermMatrix* B, const std::vector<Parameter>& ps);
//@{
//! user front-end for external Arpack eigen solver
inline EigenElements arpackSolve(TermMatrix& A, TermMatrix& B)
      { return arpackSolve(&A, &B, std::vector<Parameter>()); }
inline EigenElements arpackSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1)
      { return arpackSolve(&A, &B, std::vector<Parameter>(1,p1)); }
inline EigenElements arpackSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2)
      { return arpackSolve(&A, &B, makeParList(2, &p1, &p2)); }
inline EigenElements arpackSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3)
      { return arpackSolve(&A, &B, makeParList(3, &p1, &p2, &p3)); }
inline EigenElements arpackSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                 const Parameter& p4)
      { return arpackSolve(&A, &B, makeParList(4, &p1, &p2, &p3, &p4)); }
inline EigenElements arpackSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                 const Parameter& p4, const Parameter& p5)
      { return arpackSolve(&A, &B, makeParList(5, &p1, &p2, &p3, &p4, &p5)); }
inline EigenElements arpackSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                 const Parameter& p4, const Parameter& p5, const Parameter& p6)
      { return arpackSolve(&A, &B, makeParList(6, &p1, &p2, &p3, &p4, &p5, &p6)); }
inline EigenElements arpackSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                 const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
      { return arpackSolve(&A, &B, makeParList(7, &p1, &p2, &p3, &p4, &p5, &p6, &p7)); }
inline EigenElements arpackSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                 const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                 const Parameter& p8)
      { return arpackSolve(&A, &B, makeParList(8, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8)); }
inline EigenElements arpackSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                 const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                 const Parameter& p8, const Parameter& p9)
      { return arpackSolve(&A, &B, makeParList(9, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9)); }
inline EigenElements arpackSolve(TermMatrix& A, TermMatrix& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                 const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7,
                                 const Parameter& p8, const Parameter& p9, const Parameter& p10)
      { return arpackSolve(&A, &B, makeParList(10, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9, &p10)); }
inline EigenElements arpackSolve(TermMatrix& A)
      { return arpackSolve(&A, 0, std::vector<Parameter>()); }
inline EigenElements arpackSolve(TermMatrix& A, const Parameter& p1)
      { return arpackSolve(&A, 0, std::vector<Parameter>(1,p1)); }
inline EigenElements arpackSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2)
      { return arpackSolve(&A, 0, makeParList(2, &p1, &p2)); }
inline EigenElements arpackSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3)
      { return arpackSolve(&A, 0, makeParList(3, &p1, &p2, &p3)); }
inline EigenElements arpackSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
      { return arpackSolve(&A, 0, makeParList(4, &p1, &p2, &p3, &p4)); }
inline EigenElements arpackSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4,
                                 const Parameter& p5)
      { return arpackSolve(&A, 0, makeParList(5, &p1, &p2, &p3, &p4, &p5)); }
inline EigenElements arpackSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4,
                                 const Parameter& p5, const Parameter& p6)
      { return arpackSolve(&A, 0, makeParList(6, &p1, &p2, &p3, &p4, &p5, &p6)); }
inline EigenElements arpackSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4,
                                 const Parameter& p5, const Parameter& p6, const Parameter& p7)
      { return arpackSolve(&A, 0, makeParList(7, &p1, &p2, &p3, &p4, &p5, &p6, &p7)); }
inline EigenElements arpackSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4,
                                 const Parameter& p5, const Parameter& p6, const Parameter& p7, const Parameter& p8)
      { return arpackSolve(&A, 0, makeParList(8, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8)); }
inline EigenElements arpackSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4,
                                 const Parameter& p5, const Parameter& p6, const Parameter& p7, const Parameter& p8,
                                 const Parameter& p9)
      { return arpackSolve(&A, 0, makeParList(9, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9)); }
inline EigenElements arpackSolve(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4,
                                 const Parameter& p5, const Parameter& p6, const Parameter& p7, const Parameter& p8,
                                 const Parameter& p9, const Parameter& p10)
      { return arpackSolve(&A, 0, makeParList(10, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9, &p10)); }
//@}

//! Main entry point for SVD decomposition using Arpack
SvdElements svd(TermMatrix* A, const std::vector<Parameter>& ps);
//@{
//! user front-end for external Arpack SVD decomposition
inline SvdElements svd(TermMatrix& A)
      { return svd(&A, std::vector<Parameter>()); }
inline SvdElements svd(TermMatrix& A, const Parameter& p1)
      { return svd(&A, std::vector<Parameter>(1,p1)); }
inline SvdElements svd(TermMatrix& A, const Parameter& p1, const Parameter& p2)
      { return svd(&A, makeParList(2, &p1, &p2)); }
inline SvdElements svd(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3)
      { return svd(&A, makeParList(3, &p1, &p2, &p3)); }
inline SvdElements svd(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
      { return svd(&A, makeParList(4, &p1, &p2, &p3, &p4)); }
inline SvdElements svd(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4,
                       const Parameter& p5)
      { return svd(&A, makeParList(5, &p1, &p2, &p3, &p4, &p5)); }
inline SvdElements svd(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4,
                       const Parameter& p5, const Parameter& p6)
      { return svd(&A, makeParList(6, &p1, &p2, &p3, &p4, &p5, &p6)); }
inline SvdElements svd(TermMatrix& A, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4,
                       const Parameter& p5, const Parameter& p6, const Parameter& p7)
      { return svd(&A, makeParList(7, &p1, &p2, &p3, &p4, &p5, &p6, &p7)); }
//@}

