/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file decLinSys.hpp
  \author Y. Lafranche
  \since 25 jul 2016
  \date 25 jul 2016

  \brief Declaration of functions related to the computation of linear systems
         Implementations are in TermMatrixExt.cpp

  This file contains declarations and implementation intentionaly gathered into this place.
  It is included in TermMatrix.hpp only and should not been used elsewhere.
*/

//! prepare linear system (internal tool)
TermVector prepareLinearSystem(TermMatrix&, TermVector&, MatrixEntry*&, VectorEntry*&, StorageType st=_noStorage, AccessType at=_noAccess, bool toScal=false);

// factorization tools
void ldltFactorize(TermMatrix&, TermMatrix&);                          //!< factorize symmetric matrix as LDLt
void ldlstarFactorize(TermMatrix&, TermMatrix&);                       //!< factorize hermitian matrix as LDL*
void luFactorize(TermMatrix&, TermMatrix&, bool withPermutation=true); //!< factorize matrix as LU
void iluFactorize(TermMatrix&, TermMatrix&);                           //!< incomplete factorize matrix as iLU
void ildltFactorize(TermMatrix&, TermMatrix&);                         //!< incomplete factorize matrix as iLDLt
void illtFactorize(TermMatrix&, TermMatrix&);                          //!< incomplete factorize matrix as iLLt
void ildlstarFactorize(TermMatrix&, TermMatrix&);                      //!< incomplete factorize matrix as iLDL*
void illstarFactorize(TermMatrix&, TermMatrix&);                       //!< incomplete factorize matrix as iLL*
void umfpackFactorize(TermMatrix&, TermMatrix&);                       //!< factorize using umfpack
void factorize(TermMatrix&, TermMatrix&, FactorizationType ft=_noFactorization, bool withPermutation=true); //!< factorize matrix as LU or LDLt or LDL*
void factorize(TermMatrix&, FactorizationType ft=_noFactorization, bool withPermutation=true);              //!< factorize matrix as LU or LDLt or LDL*
void iFactorize(TermMatrix&, TermMatrix&, FactorizationType ft=_noFactorization);  //!< factorize matrix as iLU or iLDLt or iLDL*
void iFactorize(TermMatrix&, FactorizationType ft=_noFactorization);              //!< factorize matrix as iLU or iLDLt or iLDL*

// direct solver (one right hand side)
TermVector factSolve(TermMatrix&, const TermVector&);                       //!< solve linear system after factorization method
TermVector ldltSolve(TermMatrix&, const TermVector&, TermMatrix&);          //!< solve linear system using LDLt factorization
TermVector ldlstarSolve(TermMatrix&, const TermVector&, TermMatrix&);       //!< solve linear system using LDL* factorization
TermVector luSolve(TermMatrix&, const TermVector&, TermMatrix&);            //!< solve linear system using LU factorization
TermVector umfpackSolve(TermMatrix&, const TermVector&, bool keepA = false);//!< solve linear system using umfpack
TermVector gaussSolve(TermMatrix&, const TermVector&, bool keepA = false);  //!< solve linear system using Gauss elimination (dense system)
TermVector schurSolve(TermMatrix&, const TermVector&, const Unknown&, const Unknown&, bool keepA = false); //!< solve 2x2 block linear system using Schur decomposition
TermVector directSolve(TermMatrix&, const TermVector&, bool keepA = false); //!< solve linear system using direct method
// direct solver (few right hand sides)
TermVectors factSolve(TermMatrix&, const std::vector<TermVector>&);                       //!< solve linear system after factorization method, multiple rhs
TermVectors ldltSolve(TermMatrix&, const std::vector<TermVector>&, TermMatrix&);          //!< solve linear system using LDLt factorization, multiple rhs
TermVectors ldlstarSolve(TermMatrix&, const std::vector<TermVector>&, TermMatrix&);       //!< solve linear system using LDL* factorization, multiple rhs
TermVectors luSolve(TermMatrix&, const std::vector<TermVector>&, TermMatrix&);            //!< solve linear system using LU factorization, multiple rhs
TermVectors umfpackSolve(TermMatrix&, const std::vector<TermVector>&,bool keepA = false); //!< solve linear system using umfpack, multiple rhs
TermVectors gaussSolve(TermMatrix&, const std::vector<TermVector>&,bool keepA = false);   //!< solve linear system using Gauss elimination, multiple rhs
TermVectors directSolve(TermMatrix&, const std::vector<TermVector>&,bool keepA = false);  //!< solve linear system using direct method, multiple rhs
// direct solver (TermMatrix right hand side)
TermMatrix factSolve(TermMatrix&, TermMatrix&);                     //!< solve linear system after factorization method, TermMatrix rhs, i.e inv(A)*B
TermMatrix directSolve(TermMatrix&, TermMatrix&, KeepStatus=_keep); //!< solve linear system using direct method, TermMatrix rhs, i.e inv(A)*B
TermMatrix inverse(TermMatrix&);                                    //!< compute the inverse of TermMatrix, inverse is stored in dense column

#ifdef XLIFEPP_WITH_MAGMA
  TermVector magmaSolve(TermMatrix& A, const TermVector& B, bool useGPU=false, bool keepA=false);
#endif //XLIFEPP_WITH_MAGMA

#ifdef XLIFEPP_WITH_ARPACK
  TermVector lapackSolve(TermMatrix& A, const TermVector& B, bool keepA=false);
#endif //XLIFEPP_WITH_ARPACK

// transposed direct solver
TermVector factLeftSolve(TermMatrix&, const TermVector&);                       //!< solve transposed linear system At*X=B when matrix is already factorized
TermVector ldltLeftSolve(TermMatrix&, const TermVector&, TermMatrix&);          //!< solve transposed linear system using LDLt factorization
TermVector ldlstarLeftSolve(TermMatrix&, const TermVector&, TermMatrix&);       //!< solve transposed linear system using LDL* factorization
TermVector luSolveLeft(TermMatrix&, const TermVector&, TermMatrix&);            //!< solve transposed linear system using LU factorization
TermVectors factLeftSolve(TermMatrix&, const std::vector<TermVector>&);                       //!< solve transposed linear system after factorization method, multiple rhs
TermVectors ldltLeftSolve(TermMatrix&, const std::vector<TermVector>&, TermMatrix&);          //!< solve transposed linear system using LDLt factorization, multiple rhs
TermVectors ldlstarLeftSolve(TermMatrix&, const std::vector<TermVector>&, TermMatrix&);       //!< solve transposed linear system using LDL* factorization, multiple rhs
TermVectors luLeftSolve(TermMatrix&, const std::vector<TermVector>&, TermMatrix&);            //!< solve transposed linear system using LU factorization, multiple rhs
//TermVector umfpackLeftSolve(TermMatrix&, const TermVector&, bool keepA = false);//!< solve transposed linear system using umfpack
//TermVector gaussLeftSolve(TermMatrix&, const TermVector&, bool keepA = false);  //!< solve transposed linear system using Gauss elimination (dense system)
//TermVector schurLeftSolve(TermMatrix&, const TermVector&, const Unknown&, const Unknown&, bool keepA = false); //!< solve transposed  2x2 linear system using Schur decomposition
//TermVector directLeftSolve(TermMatrix&, const TermVector&, bool keepA = false); //!< solve transposed linear system using direct method

// iterative solvers (only one right hand side)
//! general iterative solver
TermVector iterativeSolveGen(IterativeSolverType isType, TermMatrix& A,  TermVector& B, const TermVector& X0,
                             Preconditioner& P, real_t tol, number_t iterMax, const real_t omega,
                             const number_t krylovDim, const number_t verboseLevel, const string_t& nam);
//! general front end for iterativeSolve
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P,
                          const std::vector<Parameter>& ps);

//@{
//! user front-end for iterative solver
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3, const Parameter& p4);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                          const Parameter& p7);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                          const Parameter& p7);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                          const Parameter& p7);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                          const Parameter& p4);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                          const Parameter& p4, const Parameter& p5);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                          const Parameter& p4, const Parameter& p5, const Parameter& p6);
TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                          const Parameter& p4, const Parameter& p5, const Parameter& p6,
                          const Parameter& p7);
//@}

//@{
//! user front-end for BiCG solver
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P)
{ return iterativeSolve(A, B, X0, P, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1)
{ return iterativeSolve(A, B, X0, P, p1, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                            const Parameter& p2)
{ return iterativeSolve(A, B, X0, P, p1, p2, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                            const Parameter& p2, const Parameter& p3)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                            const Parameter& p2, const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, p4, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                            const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, p4, p5, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, Preconditioner& P)
{ return iterativeSolve(A, B, P, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1)
{ return iterativeSolve(A, B, P, p1, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, P, p1, p2, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                            const Parameter& p3)
{ return iterativeSolve(A, B, P, p1, p2, p3, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                            const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, P, p1, p2, p3, p4, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                            const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, P, p1, p2, p3, p4, p5, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0)
{ return iterativeSolve(A, B, X0, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1)
{ return iterativeSolve(A, B, X0, p1, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, X0, p1, p2, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                            const Parameter& p3)
{ return iterativeSolve(A, B, X0, p1, p2, p3, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                            const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, X0, p1, p2, p3, p4, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                            const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, X0, p1, p2, p3, p4, p5, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B)
{ return iterativeSolve(A, B, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const Parameter& p1)
{ return iterativeSolve(A, B, p1,_solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, p1, p2, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{ return iterativeSolve(A, B, p1, p2, p3, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                            const Parameter& p4)
{ return iterativeSolve(A, B, p1, p2, p3, p4, _solver=_bicg); }
inline TermVector bicgSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                            const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, p1, p2, p3, p4, p5, _solver=_bicg); }
//@}

//@{
//! user front-end for BiCGStab solver
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P)
{ return iterativeSolve(A, B, X0, P, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1)
{ return iterativeSolve(A, B, X0, P, p1, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                                const Parameter& p2)
{ return iterativeSolve(A, B, X0, P, p1, p2, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                                const Parameter& p2, const Parameter& p3)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                                const Parameter& p2, const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, p4, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                                const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, p4, p5, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, Preconditioner& P)
{ return iterativeSolve(A, B, P, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1)
{ return iterativeSolve(A, B, P, p1, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, P, p1, p2, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                                const Parameter& p3)
{ return iterativeSolve(A, B, P, p1, p2, p3, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                                const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, P, p1, p2, p3, p4, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                                const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, P, p1, p2, p3, p4, p5, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0)
{ return iterativeSolve(A, B, X0, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1)
{ return iterativeSolve(A, B, X0, p1, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, X0, p1, p2, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                                const Parameter& p3)
{ return iterativeSolve(A, B, X0, p1, p2, p3, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                                const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, X0, p1, p2, p3, p4, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                                const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, X0, p1, p2, p3, p4, p5, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B)
{ return iterativeSolve(A, B, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const Parameter& p1)
{ return iterativeSolve(A, B, p1, _solver=_bicg); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, p1, p2, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{ return iterativeSolve(A, B, p1, p2, p3, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4)
{ return iterativeSolve(A, B, p1, p2, p3, p4, _solver=_bicgstab); }
inline TermVector bicgStabSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, p1, p2, p3, p4, p5, _solver=_bicgstab); }
//@}

//@{
//! user front-end for CG solver
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P)
{ return iterativeSolve(A, B, X0, P, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1)
{ return iterativeSolve(A, B, X0, P, p1, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2)
{ return iterativeSolve(A, B, X0, P, p1, p2, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, p4, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, p4, p5, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, Preconditioner& P)
{ return iterativeSolve(A, B, P, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1)
{ return iterativeSolve(A, B, P, p1, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, P, p1, p2, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3)
{ return iterativeSolve(A, B, P, p1, p2, p3, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, P, p1, p2, p3, p4, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, P, p1, p2, p3, p4, p5, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0)
{ return iterativeSolve(A, B, X0, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1)
{ return iterativeSolve(A, B, X0, p1, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, X0, p1, p2, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3)
{ return iterativeSolve(A, B, X0, p1, p2, p3, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, X0, p1, p2, p3, p4, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, X0, p1, p2, p3, p4, p5, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B)
{ return iterativeSolve(A, B, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const Parameter& p1)
{ return iterativeSolve(A, B, p1, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, p1, p2, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{ return iterativeSolve(A, B, p1, p2, p3, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                          const Parameter& p4)
{ return iterativeSolve(A, B, p1, p2, p3, p4, _solver=_cg); }
inline TermVector cgSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                          const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, p1, p2, p3, p4, p5, _solver=_cg); }
//@}

//@{
//! user front-end for CGS solver
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P)
{ return iterativeSolve(A, B, X0, P, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1)
{ return iterativeSolve(A, B, X0, P, p1, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                           const Parameter& p2)
{ return iterativeSolve(A, B, X0, P, p1, p2, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                           const Parameter& p2, const Parameter& p3)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                           const Parameter& p2, const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, p4, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                           const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, p4, p5, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, Preconditioner& P)
{ return iterativeSolve(A, B, P, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1)
{ return iterativeSolve(A, B, P, p1, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, P, p1, p2, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                           const Parameter& p3)
{ return iterativeSolve(A, B, P, p1, p2, p3, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                           const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, P, p1, p2, p3, p4, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                           const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, P, p1, p2, p3, p4, p5, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0)
{ return iterativeSolve(A, B, X0, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1)
{ return iterativeSolve(A, B, X0, p1, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, X0, p1, p2, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                           const Parameter& p3)
{ return iterativeSolve(A, B, X0, p1, p2, p3, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                           const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, X0, p1, p2, p3, p4, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                           const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, X0, p1, p2, p3, p4, p5, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B)
{ return iterativeSolve(A, B, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const Parameter& p1)
{ return iterativeSolve(A, B, p1, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, p1, p2, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{ return iterativeSolve(A, B, p1, p2, p3, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                           const Parameter& p4)
{ return iterativeSolve(A, B, p1, p2, p3, p4, _solver=_cgs); }
inline TermVector cgsSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                           const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, p1, p2, p3, p4, p5, _solver=_cgs); }
//@}

//@{
//! user front-end for GMRES solver
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P)
{ return iterativeSolve(A, B, X0, P, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1)
{ return iterativeSolve(A, B, X0, P, p1, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                             const Parameter& p2)
{ return iterativeSolve(A, B, X0, P, p1, p2, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                             const Parameter& p2, const Parameter& p3)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                             const Parameter& p2, const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, p4, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                             const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, p4, p5, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, Preconditioner& P)
{ return iterativeSolve(A, B, P, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1)
{ return iterativeSolve(A, B, P, p1, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, P, p1, p2, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                             const Parameter& p3)
{ return iterativeSolve(A, B, P, p1, p2, p3, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                             const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, P, p1, p2, p3, p4, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                             const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, P, p1, p2, p3, p4, p5, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0)
{ return iterativeSolve(A, B, X0, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1)
{ return iterativeSolve(A, B, X0, p1, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, X0, p1, p2, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                             const Parameter& p3)
{ return iterativeSolve(A, B, X0, p1, p2, p3, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                             const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, X0, p1, p2, p3, p4, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                             const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, X0, p1, p2, p3, p4, p5, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B)
{ return iterativeSolve(A, B, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const Parameter& p1)
{ return iterativeSolve(A, B, p1, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, p1, p2, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{ return iterativeSolve(A, B, p1, p2, p3, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                             const Parameter& p4)
{ return iterativeSolve(A, B, p1, p2, p3, p4, _solver=_gmres); }
inline TermVector gmresSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                             const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, p1, p2, p3, p4, p5, _solver=_gmres); }
//@}

//@{
//! user front-end for QMR solver
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P)
{ return iterativeSolve(A, B, X0, P, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1)
{ return iterativeSolve(A, B, X0, P, p1, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                           const Parameter& p2)
{ return iterativeSolve(A, B, X0, P, p1, p2, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                           const Parameter& p2, const Parameter& p3)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                           const Parameter& p2, const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, p4, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                           const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, X0, P, p1, p2, p3, p4, p5, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, Preconditioner& P)
{ return iterativeSolve(A, B, P, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1)
{ return iterativeSolve(A, B, P, p1, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, P, p1, p2, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                           const Parameter& p3)
{ return iterativeSolve(A, B, P, p1, p2, p3, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                           const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, P, p1, p2, p3, p4, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                           const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, P, p1, p2, p3, p4, p5, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0)
{ return iterativeSolve(A, B, X0, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1)
{ return iterativeSolve(A, B, X0, p1, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, X0, p1, p2, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                           const Parameter& p3)
{ return iterativeSolve(A, B, X0, p1, p2, p3, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                           const Parameter& p3, const Parameter& p4)
{ return iterativeSolve(A, B, X0, p1, p2, p3, p4, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                           const Parameter& p3, const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, X0, p1, p2, p3, p4, p5, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B)
{ return iterativeSolve(A, B, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const Parameter& p1)
{ return iterativeSolve(A, B, p1, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2)
{ return iterativeSolve(A, B, p1, p2, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{ return iterativeSolve(A, B, p1, p2, p3, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                           const Parameter& p4)
{ return iterativeSolve(A, B, p1, p2, p3, p4, _solver=_qmr); }
inline TermVector qmrSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                           const Parameter& p4, const Parameter& p5)
{ return iterativeSolve(A, B, p1, p2, p3, p4, p5, _solver=_qmr); }
//@}
