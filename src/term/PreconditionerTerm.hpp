/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file PreconditionerTerm.hpp
  \author C. Chambeyron, D. Martin, Ha NGUYEN, N. Kielbasiewicz
  \since 6 Sept 2005
  \date  17 Nov 2016

  \brief Definition of the xlifepp::PreconditionerTerm class

  members:
  --------
    - precondMatrix_p    [ pointer to TermMatrix ]       pointer to precondition matrix
    - type_              [ PreconditionerType, private ] (see enum in init/config.hpp)
    - omega_             [ Real, private ]               relaxation parameter for SSOR preconditioners
*/

#ifndef PRECONDITIONER_TERM_HPP
#define PRECONDITIONER_TERM_HPP

#include "config.h"
#include "utils.h"
#include "solvers.h"
#include <iostream>

namespace xlifepp
{

class TermMatrix;
class MatrixEntry;
class TermVector;
class VectorEntry;
/*!
  \class PreconditionerTerm
  Define the preconditioner for linear systems

  This is a main class that users can derive to deal with
  preconditioning by operators. In most of the cases, users will use
  xlifepp::PreconditionerTerm when it is a preconditioning with TermMatrix/TermVector
  level (or MatrixEntry/VectorEntry)
*/

class PreconditionerTerm : public Preconditioner
{
  public:
    //! precondition matrix
    TermMatrix* precondMatrix_p;
  private:
    //! Relaxation parameter for SSOR preconditioners
    real_t omega_;
    //! true if allocated
    bool isAllocated;

  public:
    //! default constructor
    PreconditionerTerm()
      : Preconditioner(), precondMatrix_p(nullptr), omega_(0.), isAllocated(false) {}
    //! constructor from matrix
    PreconditionerTerm(TermMatrix& A, PreconditionerType pt = _productPrec, const real_t w = 1.);
    //! copy constructor
    PreconditionerTerm(const PreconditionerTerm& pt) : Preconditioner(pt)
    {
      precondMatrix_p = pt.precondMatrix_p;
      omega_ = pt.omega_;
      isAllocated = pt.isAllocated;
    }
    //! destructor
    ~PreconditionerTerm();

    // bool isEmpty() const;
    //! returns ValueType of preconditionner
    ValueType valueType() const;
    MatrixEntry* getPrecondEntries() const;

    void print(std::ostream& os) const //!< print utility
    { os << std::endl << " with " << name() << " preconditioner " << std::endl; }
    void print(PrintStream& os) const {print(os.currentStream());}
    void print() const { print(thePrintStream); } //!< print utility (const)

    //! Solver with B and X
    void solve(TermVector& B, TermVector& X) const;
    void solve(VectorEntry& B, VectorEntry& X) const;
    //! Solver with B
    TermVector solve(TermVector& B) const;
    VectorEntry solve(VectorEntry& B) const;
    //! Transpose Solver with B and X
    void leftSolve(TermVector& B, TermVector& X) const;
    void leftSolve(VectorEntry& B, VectorEntry& X) const;
    //! Transpose Solver with B
    TermVector leftSolve(TermVector& B) const;
    VectorEntry leftSolve(VectorEntry& B) const;
}; // end of PreconditionerTerm class

} // namespace xlifepp

#endif  /* PRECONDITIONER_TERM_HPP */
