/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ioTermVector.cpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 16 apr 2013
  \date 5 jul 2013

  \brief Main functions related to import terms from file or to export them to file
*/

#include "../Term.hpp"
#include "../TermVector.hpp"
#include "../TermMatrix.hpp"

namespace xlifepp
{
  using std::endl;
  using std::list;
  using std::map;
  using std::ostream;
  using std::pair;
  using std::vector;

  //----------------------------------------------------------------------------------
  // input/output functions
  //----------------------------------------------------------------------------------

  //! save a list of SuTermVectors on a same Space to files
  void saveToFile(const string_t &filename, const Space *sp, const list<SuTermVector *> &sutvs, IOFormat iof, string_t dataName, bool aFilePerDomain)
  {
    trace_p->push("saveToFile(String, Space*, list<SuTermVector*>, IOFormat, String, bool)");
    // SpaceType spt = sp->typeOfSpace();
    const GeomDomain *dom = sp->domain();

    const Mesh *meshp = dom->mesh();
    if (meshp == nullptr)
    {
      error("domain_notmesh", dom->name(), words("domain type", dom->domType()));
      trace_p->pop();
      return;
    }

    // open file
    string_t fn = trim(filename);
    std::pair<string_t, string_t> rootext = fileRootExtension(fn, Environment::authorizedSaveToFileExtensions());
    fn = rootext.first;

    if (aFilePerDomain)
    {
      fn += "_" + dom->nameWithoutSharp();
    }

    switch (iof)
    {
    case _matlab:
      fn += ".m";
      break;
    case _msh:
      fn += ".msh";
      break;
    case _vtk:
      fn += ".vtk";
      break;
    case _vtu:
      fn += ".vtu";
      break;
    case _vizir4:
      fn += ".sol";
      break;
    case _xyzv:
      fn += ".xyzv";
      break;
    default:
      error("bad_format", words("ioformat", iof));
    }

    std::ofstream fout(fn.c_str());
    if (!fout.is_open())
    {
      error("bad_file", fn);
    }
    if (isTestMode)
    {
      fout.precision(testPrec);
    }
    else
    {
      fout.precision(fullPrec);
    }

    pair<vector<Point>, map<number_t, number_t>> coordsInfo = ioPoints(sp);
    vector<pair<ShapeType, vector<number_t>>> elementsInfo = ioElementsBySplitting(sp, coordsInfo.second);

    list<SuTermVector *>::const_iterator itv;
    if (isTestMode)
    {
      for (itv = sutvs.begin(); itv != sutvs.end(); itv++)
      {
        (*itv)->round(1e-6);
      }
    }
    switch (iof)
    {
    case _matlab:
      saveToMtlb(fout, sp, sutvs, coordsInfo.first, elementsInfo, dom, dataName);
      break;
    case _msh:
      saveToMsh(fout, sp, sutvs, coordsInfo.first, elementsInfo, dom, dataName);
      break;
    case _vtk:
      saveToVtk(fout, sp, sutvs, coordsInfo.first, elementsInfo, dom, dataName);
      break;
    case _vizir4:
      saveToVizir4(fout, sp, sutvs, coordsInfo.first, elementsInfo, dom, dataName);
      break;
    case _vtu:
      saveToVtkVtu(fout, sp, sutvs, coordsInfo.first, elementsInfo, dom, dataName);
      break;
    case _xyzv:
      saveToXyzVs(fout, sp, sutvs, coordsInfo.first, dataName, false); // todo: deal with the writeHeader option in calling functions
      break;
    default:
      error("bad_format", words("ioformat", iof));
      break;
    }
    fout.close();
    trace_p->pop();
  }

  void buildParamSaveToFile(const Parameter &p, IOFormat &iof, string_t &dataName, bool &aFilePerDomain)
  {
    ParameterKey key = p.key();
    switch (key)
    {
    case _pk_format:
    {
      switch (p.type())
      {
      case _integer:
      case _enumIOFormat:
        iof = IOFormat(p.get_n());
        break;
      default:
        error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_data_name:
    {
      switch (p.type())
      {
      case _string:
        dataName = p.get_s();
        break;
      default:
        error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_aFilePerDomain:
    {
      aFilePerDomain = true;
      break;
    }
    case _pk_aUniqueFile:
    {
      aFilePerDomain = false;
      break;
    }
    default:
    {
      where("saveToFile(...)");
      error("unexpected_param_key", words("param key", p.key()));
    }
    }
  }

  void saveToFile(const string_t &filename, const std::list<const TermVector *> &tvs, const std::vector<Parameter> &ps)
  {
    trace_p->push("saveToFile(list<TermVector*>)");
    IOFormat iof = _undefFormat;
    bool aFilePerDomain = true;
    string_t dataName = "";
    std::set<ParameterKey> params, usedParams;
    params.insert(_pk_format);
    params.insert(_pk_data_name);
    params.insert(_pk_aFilePerDomain);
    params.insert(_pk_aUniqueFile);
    for (number_t i = 0; i < ps.size(); ++i)
    {
      buildParamSaveToFile(ps[i], iof, dataName, aFilePerDomain);
      ParameterKey key = ps[i].key();
      if (params.find(key) != params.end())
      {
        params.erase(key);
      }
      else
      {
        if (usedParams.find(key) == usedParams.end())
          error("unexpected_parameter", words("param key", key));
        else
          error("param_already_used", words("param key", key));
      }
      usedParams.insert(key);
      // user must use aFilePerDomain or aUniqueFile, not both
      if (key == _pk_aFilePerDomain && usedParams.find(_pk_aUniqueFile) != usedParams.end())
      {
        error("param_conflict", words("param key", key), words("param key", _pk_aUniqueFile));
      }
      if (key == _pk_aUniqueFile && usedParams.find(_pk_aFilePerDomain) != usedParams.end())
      {
        error("param_conflict", words("param key", key), words("param key", _pk_aFilePerDomain));
      }
    }

    std::pair<string_t, string_t> rootext = fileRootExtension(trim(filename), Environment::authorizedSaveToFileExtensions());

    // file extension has priority to key _format
    if (rootext.second == "m")
    {
      if (iof != _undefFormat && iof != _matlab)
      {
        warning("file_extension_priority", "m", words("ioformat", iof), words("ioformat", _matlab));
      }
      iof = _matlab;
    }
    else if (rootext.second == "vtk")
    {
      if (iof != _undefFormat && iof != _vtk)
      {
        warning("file_extension_priority", "vtk", words("ioformat", iof), words("ioformat", _vtk));
      }
      iof = _vtk;
    }
    else if (rootext.second == "vtu")
    {
      if (iof != _undefFormat && iof != _vtu)
      {
        warning("file_extension_priority", "vtu", words("ioformat", iof), words("ioformat", _vtu));
      }
      iof = _vtu;
    }
    else if (rootext.second == "sol")
    {
      if (iof != _undefFormat && iof != _vizir4)
      {
        warning("file_extension_priority", "vizir4", words("ioformat", iof), words("ioformat", _vizir4));
      }
      iof = _vizir4;
    }
    else if (rootext.second == "msh")
    {
      if (iof != _undefFormat && iof != _msh)
      {
        warning("file_extension_priority", "msh", words("ioformat", iof), words("ioformat", _msh));
      }
      iof = _msh;
    }
    else if (rootext.second == "xyvz")
    {
      if (iof != _undefFormat && iof != _xyzv)
      {
        warning("file_extension_priority", "xyzv", words("ioformat", iof), words("ioformat", _xyzv));
      }
      iof = _xyzv;
    }
    else if (rootext.second == "txt")
    {
      if (iof != _undefFormat && iof != _raw)
      {
        warning("file_extension_priority", "txt", words("ioformat", iof), words("ioformat", _raw));
      }
      iof = _raw;
    }
    else
    {
      if (rootext.second != "")
      {
        warning("file_extension_ignored", rootext.second);
        if (iof == _undefFormat)
        {
          warning("file_default_extension", words("ioformat", _vtu));
          iof = _vtu;
          rootext.second = "vtu";
        }
      }
      else
      {
        if (iof == _undefFormat)
        {
          warning("file_default_extension", words("ioformat", _vtu));
          iof = _vtu;
          rootext.second = "vtu";
        }
        else
        {
          switch (iof)
          {
          case _matlab:
            rootext.second = "m";

          case _vtk:
            rootext.second = "vtk";
            break;
          case _vtu:
            rootext.second = "vtu";
            break;
          case _vizir4:
            rootext.second = "sol";
            break;
          case _xyzv:
            rootext.second = "xyzv";
            break;
          case _msh:
            rootext.second = "msh";
            break;
          case _raw:
            rootext.second = "txt";
            break;
          default:
            break;
          }
        }
      }
    }
    string_t filename2 = fileNameFromComponents(rootext.first, rootext.second);

    // output to multiple files in raw format
    if (iof == _raw)
    {
      list<const TermVector *>::const_iterator itvl;
      for (itvl = tvs.begin(); itvl != tvs.end(); itvl++)
      {
        (*itvl)->saveToFile(filename2);
      }
      trace_p->pop();
      return;
    }

    // collect SuTermVector's stored by space
    list<const TermVector *>::const_iterator itvl;
    map<const Space *, list<SuTermVector *>> slist;
    map<const Space *, list<SuTermVector *>>::iterator its;
    for (itvl = tvs.begin(); itvl != tvs.end(); itvl++)
    {
      const TermVector *tv = *itvl;
      for (cit_mustv it = tv->begin(); it != tv->end(); it++) // travel SuTermVector's
      {
        const Space *sp = it->second->spacep();
        its = slist.find(sp);
        if (its != slist.end())
        {
          its->second.push_back(it->second);
        }
        else
        {
          slist.insert(pair<const Space *, list<SuTermVector *>>(it->second->spacep(), list<SuTermVector *>(1, it->second)));
        }
      }
    }

    // for each space save to file a SuTermVector's list
    for (its = slist.begin(); its != slist.end(); its++)
    {
      if (slist.size() == 1)
        saveToFile(filename2, its->first, its->second, iof, dataName, aFilePerDomain);
      else // case of multiple spaces, we add the space name in filename
      {
        filename2 = fileNameFromComponents(rootext.first, its->first->name(), rootext.second);
        saveToFile(filename2, its->first, its->second, iof, dataName, aFilePerDomain);
      }
    }

    trace_p->pop();
  }

  void saveToFile(const string_t &filename, const TermVector &tv)
  {
    list<const TermVector *> tvs = {&tv};
    saveToFile(filename, tvs);
  }

  void saveToFile(const string_t &filename, const TermVector &tv, const Parameter &p1)
  {
    list<const TermVector *> tvs = {&tv};
    saveToFile(filename, tvs, p1);
  }
  void saveToFile(const string_t &filename, const TermVector &tv, const Parameter &p1, const Parameter &p2)
  {
    list<const TermVector *> tvs = {&tv};
    saveToFile(filename, tvs, p1, p2);
  }
  void saveToFile(const string_t &filename, const TermVector &tv, const Parameter &p1, const Parameter &p2, const Parameter &p3)
  {
    list<const TermVector *> tvs = {&tv};
    saveToFile(filename, tvs, p1, p2, p3);
  }

  void saveToFile(const string_t &filename, const TermVector &tv1, const TermVector &tv2)
  {
    list<const TermVector *> tvs = {&tv1, &tv2};
    saveToFile(filename, tvs);
  }
  void saveToFile(const string_t &filename, const TermVector &tv1, const TermVector &tv2, const Parameter &p1)
  {
    list<const TermVector *> tvs = {&tv1, &tv2};
    saveToFile(filename, tvs, p1);
  }
  void saveToFile(const string_t &filename, const TermVector &tv1, const TermVector &tv2, const Parameter &p1, const Parameter &p2)
  {
    list<const TermVector *> tvs = {&tv1, &tv2};
    saveToFile(filename, tvs, p1, p2);
  }
  void saveToFile(const string_t &filename, const TermVector &tv1, const TermVector &tv2, const Parameter &p1, const Parameter &p2, const Parameter &p3)
  {
    list<const TermVector *> tvs = {&tv1, &tv2};
    saveToFile(filename, tvs, p1, p2, p3);
  }

  void saveToFile(const string_t &filename, const TermVector &tv1, const TermVector &tv2, const TermVector &tv3)
  {
    list<const TermVector *> tvs = {&tv1, &tv2, &tv3};
    saveToFile(filename, tvs);
  }
  void saveToFile(const string_t &filename, const TermVector &tv1, const TermVector &tv2, const TermVector &tv3, const Parameter &p1)
  {
    list<const TermVector *> tvs = {&tv1, &tv2, &tv3};
    saveToFile(filename, tvs, p1);
  }
  void saveToFile(const string_t &filename, const TermVector &tv1, const TermVector &tv2, const TermVector &tv3, const Parameter &p1, const Parameter &p2)
  {
    list<const TermVector *> tvs = {&tv1, &tv2, &tv3};
    saveToFile(filename, tvs, p1, p2);
  }
  void saveToFile(const string_t &filename, const TermVector &tv1, const TermVector &tv2, const TermVector &tv3, const Parameter &p1, const Parameter &p2, const Parameter &p3)
  {
    list<const TermVector *> tvs = {&tv1, &tv2, &tv3};
    saveToFile(filename, tvs, p1, p2, p3);
  }

  void saveToFile(const string_t &filename, const TermVector &tv1, const TermVector &tv2, const TermVector &tv3, const TermVector &tv4)
  {
    list<const TermVector *> tvs = {&tv1, &tv2, &tv3, &tv4};
    saveToFile(filename, tvs);
  }
  void saveToFile(const string_t &filename, const TermVector &tv1, const TermVector &tv2, const TermVector &tv3, const TermVector &tv4, const Parameter &p1)
  {
    list<const TermVector *> tvs = {&tv1, &tv2, &tv3, &tv4};
    saveToFile(filename, tvs, p1);
  }
  void saveToFile(const string_t &filename, const TermVector &tv1, const TermVector &tv2, const TermVector &tv3, const TermVector &tv4, const Parameter &p1, const Parameter &p2)
  {
    list<const TermVector *> tvs = {&tv1, &tv2, &tv3, &tv4};
    saveToFile(filename, tvs, p1, p2);
  }
  void saveToFile(const string_t &filename, const TermVector &tv1, const TermVector &tv2, const TermVector &tv3, const TermVector &tv4, const Parameter &p1, const Parameter &p2, const Parameter &p3)
  {
    list<const TermVector *> tvs = {&tv1, &tv2, &tv3, &tv4};
    saveToFile(filename, tvs, p1, p2, p3);
  }
  //! save 1 TermVector to files with a specific format (alias of the version with a list of TermVectors)
  void saveToFile(const string_t &fn, const TermVector &tv, const string_t &dataName, IOFormat format, bool aFilePerDomain)
  {
    warning("deprecated", "saveToFile(String, TermVector, String, IOFormat, bool)", "saveToFile(String, TermVector, _data_name=?, _format=?, _aFilePerDomain/_aUniqueFile)");
    list<const TermVector *> tvs = {&tv};
    if (aFilePerDomain)
      saveToFile(fn, tvs, _format = format, _data_name = dataName, _aFilePerDomain);
    else
      saveToFile(fn, tvs, _format = format, _data_name = dataName, _aUniqueFile);
  }

  //! save 2 TermVector to files with a specific format (alias of the version with a list of TermVectors)
  void saveToFile(const string_t &fn, const TermVector &tv1, const TermVector &tv2, IOFormat format, bool aFilePerDomain)
  {
    warning("deprecated", "saveToFile(String, TermVector, TermVector, IOFormat, bool)", "saveToFile(String, TermVector, TermVector, _format=?, _aFilePerDomain/_aUniqueFile)");
    list<const TermVector *> tvs = {&tv1, &tv2};
    if (aFilePerDomain)
      saveToFile(fn, tvs, _format = format, _aFilePerDomain);
    else
      saveToFile(fn, tvs, _format = format, _aUniqueFile);
  }
  //! save 2 TermVectors to files with a specific data name (alias of the version with a list of TermVectors)
  void saveToFile(const string_t &fn, const TermVector &tv1, const TermVector &tv2, string_t dataName, bool aFilePerDomain)
  {
    warning("deprecated", "saveToFile(String, TermVector, TermVector, String, bool)", "saveToFile(String, TermVector, TermVector, _data_name=?, _aFilePerDomain/_aUniqueFile)");
    list<const TermVector *> tvs = {&tv1, &tv2};
    if (aFilePerDomain)
      saveToFile(fn, tvs, _data_name = dataName, _aFilePerDomain);
    else
      saveToFile(fn, tvs, _data_name = dataName, _aUniqueFile);
  }

  //! save 3 TermVector to files with a specific format (alias of the version with a list of TermVectors)
  void saveToFile(const string_t &fn, const TermVector &tv1, const TermVector &tv2, const TermVector &tv3, IOFormat format, bool aFilePerDomain)
  {
    warning("deprecated", "saveToFile(String, TermVector, TermVector, TermVector, IOFormat, bool)", "saveToFile(String, TermVector, TermVector, TermVector, _format=?, _aFilePerDomain/_aUniqueFile)");
    list<const TermVector *> tvs = {&tv1, &tv2, &tv3};
    if (aFilePerDomain)
      saveToFile(fn, tvs, _format = format, _aFilePerDomain);
    else
      saveToFile(fn, tvs, _format = format, _aUniqueFile);
  }
  //! save 3 TermVectors to files with a specific data name (alias of the version with a list of TermVectors)
  void saveToFile(const string_t &fn, const TermVector &tv1, const TermVector &tv2, const TermVector &tv3, string_t dataName, bool aFilePerDomain)
  {
    warning("deprecated", "saveToFile(String, TermVector, TermVector, TermVector, String, bool)", "saveToFile(String, TermVector, TermVector, TermVector, _data_name=?, _aFilePerDomain/_aUniqueFile)");
    list<const TermVector *> tvs = {&tv1, &tv2, &tv3};
    if (aFilePerDomain)
      saveToFile(fn, tvs, _data_name = dataName, _aFilePerDomain);
    else
      saveToFile(fn, tvs, _data_name = dataName, _aUniqueFile);
  }

  //! save 4 TermVector to filesc01;nC110404 with a specific format (alias of the version with a list of TermVectors)
  void saveToFile(const string_t &fn, const TermVector &tv1, const TermVector &tv2, const TermVector &tv3, const TermVector &tv4, IOFormat format, bool aFilePerDomain)
  {
    warning("deprecated", "saveToFile(String, TermVector, TermVector, TermVector, TermVector, IOFormat, bool)", "saveToFile(String, TermVector, TermVector, TermVector, TermVector, _format=?, _aFilePerDomain/_aUniqueFile)");
    list<const TermVector *> tvs = {&tv1, &tv2, &tv3, &tv4};
    if (aFilePerDomain)
      saveToFile(fn, tvs, _format = format, _aFilePerDomain);
    else
      saveToFile(fn, tvs, _format = format, _aUniqueFile);
  }
  //! save 4 TermVectors to files with a specific data name (alias of the version with a list of TermVectors)
  void saveToFile(const string_t &fn, const TermVector &tv1, const TermVector &tv2, const TermVector &tv3, const TermVector &tv4, string_t dataName, bool aFilePerDomain)
  {
    warning("deprecated", "saveToFile(String, TermVector, TermVector, TermVector, TermVector, String, bool)", "saveToFile(String, TermVector, TermVector, TermVector, TermVector, _data_name=?, _aFilePerDomain/_aUniqueFile)");
    list<const TermVector *> tvs = {&tv1, &tv2, &tv3, &tv4};
    if (aFilePerDomain)
      saveToFile(fn, tvs, _data_name = dataName, _aFilePerDomain);
    else
      saveToFile(fn, tvs, _data_name = dataName, _aUniqueFile);
  }

  //! plot a TermVector using external call of appropriate viewer
  void plot(const TermVector &tv, IOFormat iof)
  {
    trace_p->push("plot(TermVector,IOFormat");

    string_t cmd;
    string_t rootname = "xlifepp_plot";

    switch (iof)
    {
    case _msh:
    {
#ifndef XLIFEPP_WITH_GMSH
      error("xlifepp_without_gmsh");
#endif
#ifdef OS_IS_WIN
      cmd = "\"" + theEnvironment_p->gmshExe() + "\" " + rootname + ".msh";
#elif defined(OS_IS_UNIX)
      cmd = theEnvironment_p->gmshExe() + " " + rootname + ".msh";
#endif
      break;
    }
    case _vtk:
    {
#ifndef XLIFEPP_WITH_PARAVIEW
      error("xlifepp_without_paraview");
#endif
#ifdef OS_IS_WIN
      cmd = "\"" + theEnvironment_p->paraviewExe() + "\" " + rootname + ".vtk";
#elif defined(OS_IS_UNIX)
      cmd = theEnvironment_p->paraviewExe() + " " + rootname + ".vtk";
#endif
      break;
    }
    case _vtu:
    {
#ifndef XLIFEPP_WITH_PARAVIEW
      error("xlifepp_without_paraview");
#endif
#ifdef OS_IS_WIN
      cmd = "\"" + theEnvironment_p->paraviewExe() + "\" " + rootname + ".vtu";
#elif defined(OS_IS_UNIX)
      cmd = theEnvironment_p->paraviewExe() + " " + rootname + ".vtu";
#endif
      break;
    }
    case _vizir4:
    {
#ifndef XLIFEPP_WITH_VIZIR4
      error("xlifepp_without_vizir4");
#endif
#ifdef OS_IS_WIN
      cmd = "\"" + theEnvironment_p->paraviewExe() + "\" " + rootname + ".sol";
      // cmd = "\""+theEnvironment_p->vizir4Exe() +"\" "+rootname+".sol";
#elif defined(OS_IS_UNIX)
      cmd = theEnvironment_p->paraviewExe() + " " + rootname + ".sol";
      // cmd=theEnvironment_p->vizir4Exe() +" "+rootname+".sol";
#endif
      break;
    }
    default:
      error("bad_format", words("ioformat", iof));
    }

    saveToFile(rootname, tv, _format = iof, _aUniqueFile);

    int ierr = system(cmd.c_str());
    if (theVerboseLevel > 1)
    {
      info("cmd_exec", cmd, ierr);
    }
    trace_p->pop();
  }

  /*! extract coordinates of output order 1 split mesh from dofs
      return as a pair, the list of O1 nodes and the renumbering map dofid -> i (rank of O1 node)
  */
  pair<vector<Point>, map<number_t, number_t>> ioPoints(const Space *sp)
  {
    trace_p->push("ioPoints(Space*)");
    vector<Point> coords;
    map<number_t, number_t> renumbering;
    map<number_t, number_t>::iterator itm;

    const Interpolation *spaceInterp = sp->rootSpace()->feSpace()->interpolation();
    if (spaceInterp->type == _Lagrange)
    {
      if (spaceInterp->subtype != _standard && spaceInterp->subtype != _GaussLobattoPoints)
      {
        error("export_not_yet_implemented", words("non standard or GaussLobatto Lagrange"), "");
      }

      if (spaceInterp->numtype == 0) // PO space, coords = domain coordinates, renumbering mesh vertex numbering to domain vertex numbering
      {
        const vector<GeomElement *> &gelts = sp->domain()->meshDomain()->geomElements;
        vector<GeomElement *>::const_iterator itg = gelts.begin();
        number_t k = 0;
        for (; itg != gelts.end(); itg++)
        {
          vector<number_t> vnum = (*itg)->vertexNumbers();
          vector<number_t>::iterator itv = vnum.begin();
          vector<Point> no = sp->domain()->mesh()->nodes;
          for (; itv != vnum.end(); itv++)
          {
            if (renumbering.find(*itv) == renumbering.end())
            {
              renumbering[*itv] = ++k;
              coords.push_back(no[*itv - 1]);
            }
          }
        }
        trace_p->pop();
        return std::make_pair(coords, renumbering);
      }

      if (sp->typeOfSpace() == _feSpace)
      {
        vector<FeDof>::const_iterator it_fedofs;
        number_t i = 1;
        for (it_fedofs = sp->feSpace()->dofs.begin(); it_fedofs != sp->feSpace()->dofs.end(); it_fedofs++, i++)
        {
          renumbering[it_fedofs->id()] = i;
          coords.push_back(it_fedofs->coords());
        }
      }
      else if (sp->typeOfSpace() == _subSpace && sp->subSpace()->isFeSubspace())
      {
        for (number_t i = 1; i <= sp->subSpace()->feSubSpace()->nbDofs(); i++)
        {
          renumbering[sp->subSpace()->feSubSpace()->dofId(i)] = i;
          const FeDof &dof = static_cast<const FeDof &>(sp->subSpace()->feSubSpace()->dof(i));
          coords.push_back(dof.coords());
        }
      }
      else
      {
        error("not_fe_space_type", sp->name());
      }
    }
    else // not Lagrange interpolation
    {
      error("export_not_yet_implemented", words("non Lagrange"), "");
    }

    trace_p->pop();
    return std::make_pair(coords, renumbering);
  }

  /*! construct list of node numbers for each element after splitting space elements
      return the list of pair of shape O1 element and node number
  */
  vector<pair<ShapeType, vector<number_t>>> ioElementsBySplitting(const Space *sp, map<number_t, number_t> renumbering)
  {
    trace_p->push("ioElementsBySplitting(Space*,...)");
    vector<pair<ShapeType, vector<number_t>>> splitElts;

    const Interpolation *spaceInterp = sp->rootSpace()->feSpace()->interpolation();
    if (spaceInterp->type == _Lagrange)
    {
      if (spaceInterp->subtype != _standard && spaceInterp->subtype != _GaussLobattoPoints)
      {
        error("export_not_yet_implemented", words("non standard or GaussLobatto Lagrange"), "");
      }
      if (sp->typeOfSpace() == _feSpace)
      {
        vector<Element>::const_iterator it_elts;
        for (it_elts = sp->feSpace()->elements.begin(); it_elts != sp->feSpace()->elements.end(); it_elts++)
        {
          vector<pair<ShapeType, vector<number_t>>> buf = it_elts->splitO1(&renumbering);
          for (number_t i = 0; i < buf.size(); i++)
          {
            splitElts.push_back(buf[i]);
          }
        }
      }
      else if (sp->typeOfSpace() == _subSpace && sp->subSpace()->isFeSubspace())
      {
        vector<Element *>::const_iterator it_elts;
        for (it_elts = sp->subSpace()->feSubSpace()->elements.begin(); it_elts != sp->subSpace()->feSubSpace()->elements.end(); it_elts++)
        {
          vector<pair<ShapeType, vector<number_t>>> buf = (*it_elts)->splitO1(&renumbering);
          for (number_t i = 0; i < buf.size(); i++)
          {
            splitElts.push_back(buf[i]);
          }
        }
      }
      else
      {
        where("in ioElementsBySplitting(const Space *, map<number_t,number_t>)");
        error("not_fe_space_type", sp->name());
      }
    }
    else // not Lagrange interpolation
    {
      where("ioElementsBySplitting(const Space *, map<number_t,number_t>)");
      error("export_not_yet_implemented", words("non Lagrange"), "");
    }
    trace_p->pop();
    return splitElts;
  }

  //! save a list of SuTermVectors on the same Space to vtk files
  void saveToVtk(ostream &fout, const Space *sp, const list<SuTermVector *> &sutvs, const vector<Point> &coords,
                 const vector<pair<ShapeType, vector<number_t>>> &elementsInfo, const GeomDomain *dom, string_t dataName)
  {
    trace_p->push("saveToVtk(...)");
    vtkExport(*dom, coords, elementsInfo, fout);

    // export values
    list<SuTermVector *>::const_iterator itv;
    const Interpolation *spaceInterp = sp->rootSpace()->feSpace()->interpolation();
    if (spaceInterp->type == _Lagrange)
    {
      if (spaceInterp->subtype != _standard && spaceInterp->subtype != _GaussLobattoPoints)
      {
        error("export_not_yet_implemented", words("non standard or GaussLobatto Lagrange"), "");
      }
      switch (spaceInterp->numtype)
      {
      case 0: // save value by cell
      {
        for (itv = sutvs.begin(); itv != sutvs.end(); itv++)
        {
          SuTermVector &stv = **itv;
          string_t na = dataName;
          if (na == "")
          {
            na = stv.name();
            if (na == "")
            {
              na = "term_" + stv.up()->name();
            }
          }
          replaceChar(na, ' ', '_');
          dimen_t nbc = stv.up()->nbOfComponents();
          if (nbc == 1)
          {
            switch (stv.valueType())
            {
            case _real:
            {
              fout << "CELL_DATA " << stv.entries()->rEntries_p->size() << endl;
              fout << "SCALARS " << na << " float 1" << endl;
              fout << "LOOKUP_TABLE default" << endl;
              vector<real_t>::const_iterator iv;
              for (iv = stv.entries()->rEntries_p->begin(); iv != stv.entries()->rEntries_p->end(); iv++)
              {
                fout << *iv << endl;
              }
              break;
            }
            case _complex:
            {
              fout << "CELL_DATA " << stv.entries()->cEntries_p->size() << endl;
              fout << "SCALARS real_part_" << na << " float 1" << endl;
              fout << "LOOKUP_TABLE default" << endl;
              vector<complex_t>::const_iterator iv;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); iv++)
              {
                fout << (*iv).real() << endl;
              }
              fout << "CELLS_DATA " << stv.entries()->cEntries_p->size() << endl;
              fout << "SCALARS imag_part_" << na << " float 1" << endl;
              fout << "LOOKUP_TABLE default" << endl;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); iv++)
              {
                fout << (*iv).imag() << endl;
              }
              fout << "CELLS_DATA " << stv.entries()->cEntries_p->size() << endl;
              fout << "SCALARS abs_part_" << na << " float 1" << endl;
              fout << "LOOKUP_TABLE default" << endl;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); iv++)
              {
                fout << std::abs(*iv) << endl;
              }
              break;
            }
            default:
              break;
            }
          }
          else // vector case
          {
            switch (stv.valueType())
            {
            case _real:
            {
              fout << "CELL_DATA " << stv.entries()->rvEntries_p->size() << endl;
              fout << "VECTORS " << na << " float" << endl;
              // fout << "LOOKUP_TABLE default" << endl;
              vector<Vector<real_t>>::const_iterator iv;
              for (iv = stv.entries()->rvEntries_p->begin(); iv != stv.entries()->rvEntries_p->end(); iv++)
              {
                for (number_t i = 0; i < nbc; i++)
                {
                  fout << (*iv)[i] << " ";
                }
                for (number_t i = nbc; i < 3; i++)
                {
                  fout << "0 ";
                } // always 3D vector in vtk !
                fout << endl;
              }
              break;
            }
            case _complex:
            {
              fout << "CELL_DATA " << stv.entries()->cvEntries_p->size() << endl;
              fout << "VECTORS real_part_" << na << " float" << endl;
              // fout << "LOOKUP_TABLE default" << endl;
              vector<Vector<complex_t>>::const_iterator iv;
              for (iv = stv.entries()->cvEntries_p->begin(); iv != stv.entries()->cvEntries_p->end(); iv++)
              {
                for (number_t i = 0; i < nbc; i++)
                {
                  fout << (*iv)[i].real() << " ";
                }
                for (number_t i = nbc; i < 3; i++)
                {
                  fout << "0 ";
                } // always 3D vector in vtk !
                fout << endl;
              }
              fout << "CELL_DATA " << stv.size() << endl;
              fout << "VECTORS imag_part_" << na << " float" << endl;
              // fout << "LOOKUP_TABLE default" << endl;
              for (iv = stv.entries()->cvEntries_p->begin(); iv != stv.entries()->cvEntries_p->end(); iv++)
              {
                for (number_t i = 0; i < nbc; i++)
                {
                  fout << (*iv)[i].imag() << " ";
                }
                for (number_t i = nbc; i < 3; i++)
                {
                  fout << "0 ";
                } // always 3D vector in vtk !
                fout << endl;
              }
              break;
            }
            default:
              break;
            } // end switch real
          } // end vector case
        } // end for
        break;
      } // end case 0
      default: // save values on outMesh=meshp
      {
        for (itv = sutvs.begin(); itv != sutvs.end(); itv++) // loop on SuTermVectors
        {
          SuTermVector &stv = **itv;
          string_t na = dataName;
          if (na == "")
          {
            na = stv.name();
            if (na == "")
            {
              na = "term_" + stv.up()->name();
            }
          }
          replaceChar(na, ' ', '_');
          dimen_t nbc = stv.up()->nbOfComponents();
          if (nbc == 1)
          {
            switch (stv.valueType())
            {
            case _real:
            {
              fout << "POINT_DATA " << stv.nbDofs() << endl;
              fout << "SCALARS " << na << " float 1" << endl;
              fout << "LOOKUP_TABLE default" << endl;
              vector<real_t>::const_iterator iv;
              for (iv = stv.entries()->rEntries_p->begin(); iv != stv.entries()->rEntries_p->end(); iv++)
              {
                fout << *iv << endl;
              }
              break;
            }
            case _complex: // saved real part, imaginary part and modulus
            {
              fout << "POINT_DATA " << stv.nbDofs() << endl;
              fout << "SCALARS " << na << "_(r,i,a) float 3" << endl;
              fout << "LOOKUP_TABLE default" << endl;
              vector<complex_t>::const_iterator iv;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); iv++)
              {
                fout << (*iv).real() << " " << (*iv).imag() << " " << std::abs(*iv) << endl;
              }
              break;
            }
            default:
              break;
            }
          }
          else // vector Unknown
          {
            switch (stv.valueType())
            {
            case _real:
            {
              fout << "POINT_DATA " << stv.nbDofs() << endl;
              fout << "VECTORS " << na << " float" << endl;
              // fout << "LOOKUP_TABLE default" << endl;
              vector<Vector<real_t>>::const_iterator iv;
              for (iv = stv.entries()->rvEntries_p->begin(); iv != stv.entries()->rvEntries_p->end(); iv++)
              {
                for (number_t i = 0; i < nbc; i++)
                {
                  fout << (*iv)[i] << " ";
                }
                for (number_t i = nbc; i < 3; i++)
                {
                  fout << "0 ";
                } // always 3D vector in vtk !
                fout << endl;
              }
              break;
            }
            case _complex: // saved real part, imaginary part
            {
              fout << "POINT_DATA " << stv.nbDofs() << endl;
              fout << "VECTORS real_part_" << na << " float" << endl;
              // fout << "LOOKUP_TABLE default" << endl;
              vector<Vector<complex_t>>::const_iterator iv;
              for (iv = stv.entries()->cvEntries_p->begin(); iv != stv.entries()->cvEntries_p->end(); iv++)
              {
                for (number_t i = 0; i < nbc; i++)
                {
                  fout << (*iv)[i].real() << " ";
                }
                for (number_t i = nbc; i < 3; i++)
                {
                  fout << "0 ";
                } // always 3D vector in vtk !
                fout << endl;
              }
              fout << "POINT_DATA " << stv.nbDofs() << endl;
              fout << "VECTORS imag_part_" << na << " float" << endl;
              // fout << "LOOKUP_TABLE default" << endl;
              for (iv = stv.entries()->cvEntries_p->begin(); iv != stv.entries()->cvEntries_p->end(); iv++)
              {
                for (number_t i = 0; i < nbc; i++)
                {
                  fout << (*iv)[i].imag() << " ";
                }
                for (number_t i = nbc; i < 3; i++)
                {
                  fout << "0 ";
                } // always 3D vector in vtk !
                fout << endl;
              }
              break;
            }
            default:
              break;
            } // end switch real
          } // end vector case
        } // end for
        break;
      } // end case default
      } // end switch interp
    }
    else // not Lagrange interpolation
    {
      error("export_not_yet_implemented", words("non Lagrange"), "");
    }
    trace_p->pop();
  }

  //! save a list of SuTermVectors on the same Space to vtk xml files for unstructured grid type meshes
  void saveToVtkVtu(ostream &fout, const Space *sp, const list<SuTermVector *> &sutvs, const vector<Point> &coords,
                    const vector<pair<ShapeType, vector<number_t>>> &elementsInfo, const GeomDomain *dom, string_t dataName)
  {
    trace_p->push("saveToVtkVtu(...)");
    fout << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">" << endl;
    fout << "<UnstructuredGrid>" << endl;
    fout << "<Piece NumberOfPoints=\"" << coords.size() << "\" NumberOfCells=\"" << elementsInfo.size() << "\">" << endl;
    std::stringstream pdout, cdout;
    string_t pdScalarDefault = "", pdVectorDefault = "", cdScalarDefault = "", cdVectorDefault = "";

    // export values
    list<SuTermVector *>::const_iterator itv;
    const Interpolation *spaceInterp = sp->rootSpace()->feSpace()->interpolation();
    if (spaceInterp->type == _Lagrange)
    {
      if (spaceInterp->subtype != _standard && spaceInterp->subtype != _GaussLobattoPoints)
      {
        error("export_not_yet_implemented", words("non standard or GaussLobatto Lagrange"), "");
      }
      switch (spaceInterp->numtype)
      {
      case 0: // save value by cell
      {
        for (itv = sutvs.begin(); itv != sutvs.end(); itv++)
        {
          SuTermVector &stv = **itv;
          string_t na = dataName;
          if (na == "")
          {
            na = stv.name();
            if (na == "")
            {
              na = "term_" + stv.up()->name();
            }
          }
          replaceChar(na, ' ', '_');
          dimen_t nbc = stv.up()->nbOfComponents();
          if (nbc == 1)
          {
            switch (stv.valueType())
            {
            case _real:
            {
              if (cdScalarDefault == "")
              {
                cdScalarDefault = na;
              }
              vector<real_t>::const_iterator iv;
              cdout << "<DataArray type=\"Float64\" Name=\"" << na << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->rEntries_p->begin(); iv != stv.entries()->rEntries_p->end(); iv++)
              {
                cdout << *iv << endl;
              }
              cdout << "</DataArray>" << endl;
              break;
            }
            case _complex:
            {
              if (cdScalarDefault == "")
              {
                cdScalarDefault = "abs_" + na;
              }
              vector<complex_t>::const_iterator iv;
              cdout << "<DataArray type=\"Float64\" Name=\"real_part_" << na << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); iv++)
              {
                cdout << (*iv).real() << endl;
              }
              cdout << "</DataArray>" << endl;

              cdout << "<DataArray type=\"Float64\" Name=\"imag_part_" << na << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); iv++)
              {
                cdout << (*iv).imag() << endl;
              }
              cdout << "</DataArray>" << endl;

              cdout << "<DataArray type=\"Float64\" Name=\"abs_" << na << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); iv++)
              {
                cdout << std::abs(*iv) << endl;
              }
              cdout << "</DataArray>" << endl;
              break;
            }
            default:
              break;
            }
          }
          else // vector case
          {
            switch (stv.valueType())
            {
            case _real:
            {
              if (cdVectorDefault == "")
              {
                cdVectorDefault = na;
              }
              vector<Vector<real_t>>::const_iterator iv;
              // if not a 3D vector, Paraview launches a warning
              cdout << "<DataArray type=\"Float64\" Name=\"" << na << "\" NumberOfComponents=\"" << std::max(nbc, dimen_t(3)) << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->rvEntries_p->begin(); iv != stv.entries()->rvEntries_p->end(); iv++)
              {
                cdout << (*iv)[0];
                for (number_t i = 1; i < nbc; i++)
                {
                  cdout << " " << (*iv)[i];
                }
                for (number_t i = nbc; i < 3; i++)
                {
                  cdout << " 0";
                }
                cdout << endl;
              }
              cdout << "</DataArray>" << endl;
              break;
            }
            case _complex:
            {
              if (cdVectorDefault == "")
              {
                cdVectorDefault = "real_part_" + na;
              }
              vector<Vector<complex_t>>::const_iterator iv;
              // if not a 3D vector, Paraview launches a warning
              cdout << "<DataArray type=\"Float64\" Name=\"real_part_" << na << "\" NumberOfComponents=\"" << std::max(nbc, dimen_t(3)) << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->cvEntries_p->begin(); iv != stv.entries()->cvEntries_p->end(); iv++)
              {
                cdout << (*iv)[0].real();
                for (number_t i = 1; i < nbc; i++)
                {
                  cdout << " " << (*iv)[i].real();
                }
                for (number_t i = nbc; i < 3; i++)
                {
                  cdout << " 0";
                }
                cdout << endl;
              }
              cdout << "</DataArray>" << endl;

              // if not a 3D vector, Paraview launches a warning
              cdout << "<DataArray type=\"Float64\" Name=\"imag_part_" << na << "\" NumberOfComponents=\"" << std::max(nbc, dimen_t(3)) << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->cvEntries_p->begin(); iv != stv.entries()->cvEntries_p->end(); iv++)
              {
                cdout << (*iv)[0].imag();
                for (number_t i = 1; i < nbc; i++)
                {
                  cdout << " " << (*iv)[i].imag();
                }
                for (number_t i = nbc; i < 3; i++)
                {
                  cdout << " 0";
                }
                cdout << endl;
              }
              cdout << "</DataArray>" << endl;
              break;
            }
            default:
              break;
            } // end switch real
          } // end vector case
        } // end for
        break;
      } // end case 0
      default: // save values on outMesh=meshp
      {
        for (itv = sutvs.begin(); itv != sutvs.end(); itv++) // loop on SuTermVectors
        {
          SuTermVector &stv = **itv;
          string_t na = dataName;
          if (na == "")
          {
            na = stv.name();
            if (na == "")
            {
              na = "term_" + stv.up()->name();
            }
          }
          replaceChar(na, ' ', '_');
          dimen_t nbc = stv.up()->nbOfComponents();
          if (nbc == 1)
          {
            switch (stv.valueType())
            {
            case _real:
            {
              if (pdScalarDefault == "")
              {
                pdScalarDefault = na;
              }
              vector<real_t>::const_iterator iv;
              pdout << "<DataArray type=\"Float64\" Name=\"" << na << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->rEntries_p->begin(); iv != stv.entries()->rEntries_p->end(); iv++)
              {
                pdout << *iv << endl;
              }
              pdout << "</DataArray>" << endl;
              break;
            }
            case _complex: // saved real part, imaginary part and modulus
            {
              if (pdScalarDefault == "")
              {
                pdScalarDefault = "abs_" + na;
              }
              vector<complex_t>::const_iterator iv;
              pdout << "<DataArray type=\"Float64\" Name=\"real_part_" << na << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); iv++)
              {
                pdout << (*iv).real() << endl;
              }
              pdout << "</DataArray>" << endl;

              pdout << "<DataArray type=\"Float64\" Name=\"imag_part_" << na << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); iv++)
              {
                pdout << (*iv).imag() << endl;
              }
              pdout << "</DataArray>" << endl;

              pdout << "<DataArray type=\"Float64\" Name=\"abs_" << na << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); iv++)
              {
                pdout << std::abs(*iv) << endl;
              }
              pdout << "</DataArray>" << endl;
              break;
            }
            default:
              break;
            }
          }
          else // vector Unknown
          {
            switch (stv.valueType())
            {
            case _real:
            {
              if (pdVectorDefault == "")
              {
                pdVectorDefault = na;
              }
              vector<Vector<real_t>>::const_iterator iv;
              // if not a 3D vector, Paraview launches a warning
              pdout << "<DataArray type=\"Float64\" Name=\"" << na << "\" NumberOfComponents=\"" << std::max(nbc, dimen_t(3)) << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->rvEntries_p->begin(); iv != stv.entries()->rvEntries_p->end(); iv++)
              {
                pdout << (*iv)[0];
                for (number_t i = 1; i < nbc; i++)
                {
                  pdout << " " << (*iv)[i];
                }
                for (number_t i = nbc; i < 3; i++)
                {
                  pdout << " 0";
                }
                pdout << endl;
              }
              pdout << "</DataArray>" << endl;
              break;
            }
            case _complex: // saved real part, imaginary part
            {
              if (pdVectorDefault == "")
              {
                pdVectorDefault = "real_part_" + na;
              }
              vector<Vector<complex_t>>::const_iterator iv;
              // if not a 3D vector, Paraview launches a warning
              pdout << "<DataArray type=\"Float64\" Name=\"real_part_" << na << "\" NumberOfComponents=\"" << std::max(nbc, dimen_t(3)) << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->cvEntries_p->begin(); iv != stv.entries()->cvEntries_p->end(); iv++)
              {
                pdout << (*iv)[0].real();
                for (number_t i = 1; i < nbc; i++)
                {
                  pdout << " " << (*iv)[i].real();
                }
                for (number_t i = nbc; i < 3; i++)
                {
                  pdout << " 0";
                }
                pdout << endl;
              }
              pdout << "</DataArray>" << endl;

              // if not a 3D vector, Paraview launches a warning
              pdout << "<DataArray type=\"Float64\" Name=\"imag_part_" << na << "\" NumberOfComponents=\"" << std::max(nbc, dimen_t(3)) << "\" format=\"ascii\">" << endl;
              for (iv = stv.entries()->cvEntries_p->begin(); iv != stv.entries()->cvEntries_p->end(); iv++)
              {
                pdout << (*iv)[0].imag();
                for (number_t i = 1; i < nbc; i++)
                {
                  pdout << " " << (*iv)[i].imag();
                }
                for (number_t i = nbc; i < 3; i++)
                {
                  pdout << " 0";
                }
                pdout << endl;
              }
              pdout << "</DataArray>" << endl;
              break;
            }
            default:
              break;
            } // end switch real
          } // end vector case
        } // end for
        break;
      } // end case default
      } // end switch interp
    }
    else // not Lagrange interpolation
    {
      error("export_not_yet_implemented", words("non Lagrange"), "");
    }

    if (pdScalarDefault != "" || pdVectorDefault != "")
    {
      fout << "<PointData";
      if (pdScalarDefault != "")
      {
        fout << " Scalars=\"" << pdScalarDefault << "\"";
      }
      if (pdVectorDefault != "")
      {
        fout << " Vectors=\"" << pdVectorDefault << "\"";
      }
      fout << ">" << endl;
      fout << pdout.str();
      fout << "</PointData>" << endl;
    }

    if (cdScalarDefault != "" || cdVectorDefault != "")
    {
      fout << "<CellData";
      if (cdScalarDefault != "")
      {
        fout << " Scalars=\"" << cdScalarDefault << "\"";
      }
      if (cdVectorDefault != "")
      {
        fout << " Vectors=\"" << cdVectorDefault << "\"";
      }
      fout << ">" << endl;
      fout << cdout.str();
      fout << "</CellData>" << endl;
    }

    fout << "<Points>" << endl;
    fout << "<DataArray type=\"Float64\" Name=\"Nodes\" NumberOfComponents=\"3\" format=\"ascii\">" << endl;
    // list of coordinates of each vertex
    vector<Point>::const_iterator itp;
    for (itp = coords.begin(); itp != coords.end(); itp++)
    {
      for (number_t i = 0; i < dom->mesh()->spaceDim(); ++i)
      {
        fout << (*itp)[i] << " ";
      }
      for (number_t i = dom->mesh()->spaceDim(); i < 3; ++i)
      {
        fout << "0 ";
      } // 3D points in vtu format !
      fout << endl;
    }
    fout << "</DataArray>" << endl;
    fout << "</Points>" << endl;
    fout << "<Cells>" << endl;
    fout << "<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">" << endl;
    // list of vertices of each cell
    vector<number_t> offsets(elementsInfo.size());
    vector<pair<ShapeType, vector<number_t>>>::const_iterator ite;
    number_t o = 0;
    for (ite = elementsInfo.begin(); ite != elementsInfo.end(); ++ite, ++o)
    {
      // defining offsets for next DataArray tag
      if (o == 0)
      {
        offsets[o] = ite->second.size();
      }
      else
      {
        offsets[o] = ite->second.size() + offsets[o - 1];
      }
      vector<number_t>::const_iterator itn;
      for (itn = (ite->second).begin(); itn != (ite->second).end(); itn++)
      {
        fout << " " << *itn - 1;
      } // vtk indexes node from 0
      fout << endl;
    }
    fout << "</DataArray>" << endl;
    fout << "<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">" << endl;
    // indice du dernier sommet de chaque cellule
    for (number_t i = 0; i < offsets.size(); ++i)
    {
      fout << offsets[i] << " ";
    }
    fout << endl;
    fout << "</DataArray>" << endl;
    fout << "<DataArray type=\"Int8\" Name=\"types\" format=\"ascii\">" << endl;
    // cell types
    vector<dimen_t> vtkShape(10, 0);
    vtkShape[_point] = 1;
    vtkShape[_segment] = 3;
    vtkShape[_triangle] = 5;
    vtkShape[_quadrangle] = 9;
    vtkShape[_tetrahedron] = 10;
    vtkShape[_hexahedron] = 12;
    vtkShape[_prism] = 13;
    vtkShape[_pyramid] = 14;
    for (ite = elementsInfo.begin(); ite != elementsInfo.end(); ++ite)
    {
      fout << vtkShape[ite->first] << " ";
    }
    fout << endl;
    fout << "</DataArray>" << endl;
    fout << "</Cells>" << endl;
    fout << "</Piece>" << endl;
    fout << "</UnstructuredGrid>" << endl;
    fout << "</VTKFile>" << endl;

    trace_p->pop();
  }
  //

  //! save a list of SuTermVectors on the same Space to vtk xml files for unstructured grid type meshes
  void saveToVizir4(ostream &fout, const Space *sp, const list<SuTermVector *> &sutvs, const vector<Point> &coords,
                    const vector<pair<ShapeType, vector<number_t>>> &elementsInfo, const GeomDomain *dom, string_t dataName)
  {
    trace_p->push("saveToVizir4(...)");
    fout << "MeshVersionFormatted 3" << eol << eol;
    fout << "Dimension" << eol;
    std::map<string_t, number_t> sideIndex;
    fout << dom->mesh()->spaceDim() << eol << eol;
    std::stringstream pdout, cdout, qdout, mdout, fdout;
    std::vector<string_t> VVV;
    number_t ncoef, nterm, isvect=1;
    string_t dataInfo, RefStrings;
    string_t cvalR, cvalI;
    real_t valM;
    string_t pdScalarDefault = "", pdVectorDefault = "", cdScalarDefault = "", cdVectorDefault = "";

    string_t SideElemNature = "";
    // export values
    list<SuTermVector *>::const_iterator itv;
    const Interpolation *spaceInterp = sp->rootSpace()->feSpace()->interpolation();
    nterm = sutvs.size();
    number_t nut=1;
    if (spaceInterp->type == _Lagrange)
    {
      if (spaceInterp->subtype != _standard && spaceInterp->subtype != _GaussLobattoPoints)
      {
        error("export_not_yet_implemented", words("non standard or GaussLobatto Lagrange"), "");
      }
        RefStrings = "";
        for (itv = sutvs.begin(); itv != sutvs.end(); itv++, nut++) // loop on SuTermVectors
        {
          SuTermVector &stv = **itv;
          string_t na = dataName;
          if (na == "")
          {
            na = stv.name();
            if (na == "")
            {
              na = "term_" + stv.up()->name();
            }
          }
          replaceChar(na, ' ', '_');
          dimen_t nbc = stv.up()->nbOfComponents();
          if (nbc > 1) {isvect=2;}
          if (stv.valueType() == _real)
          {
            dataInfo += tostring(isvect)+ " ";
          }
          else // complex case
          {
            nterm++;
            dataInfo += tostring(isvect)+ " " + tostring(isvect)+ " ";
          }
          number_t ind = 0;
          if (nbc == 1)
          {
            if (itv == sutvs.begin())
            {
              if (stv.valueType() == _real)
              {
                ncoef = stv.entries()->rEntries_p->size();
              }
              else // complex case
              {
                ncoef = stv.entries()->cEntries_p->size();
              }
              VVV.resize(ncoef);
              pdout << ncoef << eol;
            }
            switch (stv.valueType())
            {
            case _real:
            {
              if (pdScalarDefault == "")
              {
                pdScalarDefault = na;
              }
              vector<real_t>::const_iterator iv;
              for (iv = stv.entries()->rEntries_p->begin(); iv != stv.entries()->rEntries_p->end(); iv++, ind++)
              {
                VVV[ind] += tostring(*iv) + " ";
              }
              RefStrings+="62 "+tostring(nut)+" "+stv.up()->name()+"\n";
              valM=(stv.maxValAbs()).real();
              break;
            }
            case _complex: // saved real part, imaginary part
            {
              vector<complex_t>::const_iterator iv;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); iv++, ind++)
              {
                VVV[ind] += tostring((*iv).real()) + " " + tostring((*iv).imag()) + " ";
              }
              RefStrings+="62 "+tostring(nut)+" RealPart "+stv.up()->name()+"\n";
              nut++;
              RefStrings+="62 "+tostring(nut)+" ImagPart "+stv.up()->name()+"\n";
              nut++;
              valM=(stv.maxValAbs()).real();
              break;
            }
            default:
              break;
            }
          }
          else // vector Unknown
          {
            if (itv == sutvs.begin())
            {
              if (stv.valueType() == _real)
              {
                ncoef = stv.entries()->rvEntries_p->size();
              }
              else // complex case
              {
                ncoef = stv.entries()->cvEntries_p->size();
              }
              pdout << ncoef << eol;
              VVV.resize(nbc*ncoef);
            }
            switch (stv.valueType())
            {
            case _real:
            {
              if (pdVectorDefault == "")
              {
                pdVectorDefault = na;
              }
              vector<Vector<real_t>>::const_iterator iv;
              for (iv = stv.entries()->rvEntries_p->begin(); iv != stv.entries()->rvEntries_p->end(); iv++, ind++)
              {
                for (number_t i = 0; i < nbc; i++)
                {
                  VVV[ind] += tostring((*iv)[i]) + " ";
                }
              }
              RefStrings+="62 "+tostring(nut)+" "+stv.up()->name()+"\n";
              nut++;
              // RefStrings+="62 "+tostring(nut)+" X compo. "+stv.up()->name()+"\n";
              // nut++;
              // RefStrings+="62 "+tostring(nut)+" Y compo. "+stv.up()->name()+"\n";
              valM=(stv.maxValAbs()).real();
              break;
            }
            case _complex: // saved real part, imaginary part
            {
              if (pdVectorDefault == "")
              {
                pdVectorDefault = "real_part_" + na;
              }
              vector<Vector<complex_t>>::const_iterator iv;
              for (iv = stv.entries()->cvEntries_p->begin(); iv != stv.entries()->cvEntries_p->end(); iv++, ind++)
              {
                cvalR="";
                cvalI="";
                for (number_t i = 0; i < nbc; i++)
                {
                  cvalR += tostring(((*iv)[i]).real()) + " ";
                  cvalI += tostring(((*iv)[i]).imag()) + " ";
                  // VVV[ind] += tostring(((*iv)[i]).real()) + " " + tostring(((*iv)[i]).imag()) + " ";
                }
                VVV[ind] = cvalR + cvalI;
              }
              RefStrings+="62 "+tostring(nut)+" Real part of "+stv.up()->name()+"\n";
              nut++;
              RefStrings+="62 "+tostring(nut)+" Imag part of "+stv.up()->name()+"\n";
              nut++;
              valM=(stv.maxValAbs()).real();
              break;
            }
            default:
              break;
            } // end switch real
          } // end vector case
        } // end for
        pdout << tostring(nterm)<<" " << dataInfo ;
    }
    else // not Lagrange interpolation
    {
      error("export_not_yet_implemented", words("non Lagrange"), "");
    }
    fout<<"ReferenceStrings"<<eol;
    fout << tostring(nterm)<<eol;
    fout << RefStrings << eol;
    fout << "Palette"<<eol;
    fout << tostring(-valM)<< " " <<  tostring(-valM/2.)<<" "<< 0 << " "<< tostring(valM/2.)<< " " <<tostring(valM)<<eol<<eol;

    switch (spaceInterp->numtype)
    {
      case 0: // save value by cell
      {
        fdout << "SolAt"<<vizir4Nature(vizir4Type(elementsInfo[1].first, dom->order())) <<eol;
        break;
      }
      default:
      {
        fdout << "SolAtVertices" << eol;
        break;
      }
    }

    if (pdScalarDefault != "" || pdVectorDefault != "")
    {
      pdout << eol;
      fdout << pdout.str();
      for (number_t ind = 0; ind < ncoef; ind++)
      {
        fdout << tostring(VVV[ind]) << eol;
      }
    }

    // Maillages
    string_t MeshElemNature;
    number_t nbside = 0;
    number_t nbFaces;
    mdout << eol << "Vertices" << eol;
    mdout << tostring(coords.size()) << eol;
    vector<Point>::const_iterator itp;
    for (itp = coords.begin(); itp != coords.end(); itp++)
    {
      for (number_t i = 0; i < dom->mesh()->spaceDim(); ++i)
      {
        mdout << tostring((*itp)[i] ) << " ";
      }
      mdout << tostring(1) << endl;
    }
    mdout << " " << eol;
    std::map<string_t, number_t>::iterator itsn;
    // list of vertices of each cell
    vector<number_t> offsets(elementsInfo.size());
    vector<pair<ShapeType, vector<number_t>>>::const_iterator ite;
    vector<number_t>::const_iterator itn;
    MeshElemNature = vizir4Nature(vizir4Type(elementsInfo[1].first, dom->order()));
    string_t typeEltSurf; // in 3D case
    number_t nbElt = elementsInfo.size();
    number_t nbSides = 0;
    number_t o = 1;
    MeshElement *melt;
    std::stringstream ss(std::stringstream::out);
    std::stringstream sb(std::stringstream::out);
    if (sp->dimDomain() > 1)
//    if (sp->dimDomain() ==3)
    {
      if (MeshElemNature == "Tetrahedra")
      {
        nbFaces = 4;
        SideElemNature = "Triangles";
      }
      else if (MeshElemNature == "Hexahedra")
      {
        nbFaces = 6;
        SideElemNature = "Quadrilaterals";
      }
      else if (MeshElemNature == "Triangles")
      {
        nbFaces = 3;
        SideElemNature = "Edges";
      }
      else if (MeshElemNature == "Quadrilaterals")
      {
        nbFaces = 4;
        SideElemNature = "Edges";
      }
      number_t NbTrgInterface=0;
      // loop on elem
      for (ite = elementsInfo.begin(); ite != elementsInfo.end(); ++ite, ++o)
      {
        std::vector<string_t> keys;
        std::vector<number_t> keystmp;
        if (ite->first == 6) //(Tetrahedron)
        {
          // number_t nbFaces = 4;
          keys.resize(nbFaces);
          keystmp.resize(3);
          keystmp[0] = ite->second[0];
          keystmp[1] = ite->second[1];
          keystmp[2] = ite->second[2];
          std::sort(keystmp.begin(), keystmp.end());
          keys[0] = tostring(keystmp[0]) + " " + tostring(keystmp[1]) + " " + tostring(keystmp[2]) + " ";
          keystmp[0] = ite->second[0];
          keystmp[1] = ite->second[1];
          keystmp[2] = ite->second[3];
          std::sort(keystmp.begin(), keystmp.end());
          keys[1] = tostring(keystmp[0]) + " " + tostring(keystmp[1]) + " " + tostring(keystmp[2]) + " ";
          keystmp[0] = ite->second[1];
          keystmp[1] = ite->second[2];
          keystmp[2] = ite->second[3];
          std::sort(keystmp.begin(), keystmp.end());
          keys[2] = tostring(keystmp[0]) + " " + tostring(keystmp[1]) + " " + tostring(keystmp[2]) + " ";
          keystmp[0] = ite->second[2];
          keystmp[1] = ite->second[0];
          keystmp[2] = ite->second[3];
          std::sort(keystmp.begin(), keystmp.end());
          keys[3] = tostring(keystmp[0]) + " " + tostring(keystmp[1]) + " " + tostring(keystmp[2]) + " ";
        } // Tetrahedron
        else if (ite->first == 7) //(Hexahedron)
        {
          // number_t nbFaces = 6;
          keys.resize(nbFaces);
          keystmp.resize(4);
          keystmp[0] = ite->second[0];
          keystmp[1] = ite->second[1];
          keystmp[2] = ite->second[2];
          keystmp[3] = ite->second[3];
          std::sort(keystmp.begin(), keystmp.end());
          keys[0] = tostring(keystmp[0]) + " " + tostring(keystmp[1]) + " " + tostring(keystmp[2]) + " " + tostring(keystmp[3]) + " ";
          keystmp[0] = ite->second[4];
          keystmp[1] = ite->second[5];
          keystmp[2] = ite->second[6];
          keystmp[3] = ite->second[7];
          std::sort(keystmp.begin(), keystmp.end());
          keys[1] = tostring(keystmp[0]) + " " + tostring(keystmp[1]) + " " + tostring(keystmp[2]) + " " + tostring(keystmp[3]) + " ";
          keystmp[0] = ite->second[0];
          keystmp[1] = ite->second[1];
          keystmp[2] = ite->second[5];
          keystmp[3] = ite->second[4];
          std::sort(keystmp.begin(), keystmp.end());
          keys[2] = tostring(keystmp[0]) + " " + tostring(keystmp[1]) + " " + tostring(keystmp[2]) + " " + tostring(keystmp[3]) + " ";
          keystmp[0] = ite->second[3];
          keystmp[1] = ite->second[2];
          keystmp[2] = ite->second[6];
          keystmp[3] = ite->second[7];
          std::sort(keystmp.begin(), keystmp.end());
          keys[3] = tostring(keystmp[0]) + " " + tostring(keystmp[1]) + " " + tostring(keystmp[2]) + " " + tostring(keystmp[3]) + " ";
          keystmp[0] = ite->second[0];
          keystmp[1] = ite->second[3];
          keystmp[2] = ite->second[7];
          keystmp[3] = ite->second[4];
          std::sort(keystmp.begin(), keystmp.end());
          keys[3] = tostring(keystmp[0]) + " " + tostring(keystmp[1]) + " " + tostring(keystmp[2]) + " " + tostring(keystmp[3]) + " ";
          keystmp[0] = ite->second[1];
          keystmp[1] = ite->second[2];
          keystmp[2] = ite->second[6];
          keystmp[3] = ite->second[5];
          std::sort(keystmp.begin(), keystmp.end());
          keys[3] = tostring(keystmp[0]) + " " + tostring(keystmp[1]) + " " + tostring(keystmp[2]) + " " + tostring(keystmp[3]) + " ";
        } // Hexahedron
        else if (ite->first == 4) //(Triangles)
        {
          // number_t nbFaces = 3;
          keys.resize(nbFaces);
          keystmp.resize(2);
          keystmp[0] = ite->second[0];
          keystmp[1] = ite->second[1];
          std::sort(keystmp.begin(), keystmp.end());
          keys[0] = tostring(keystmp[0]) + " " + tostring(keystmp[1])  + " ";

          keystmp[0] = ite->second[1];
          keystmp[1] = ite->second[2];
          std::sort(keystmp.begin(), keystmp.end());
          keys[1] = tostring(keystmp[0]) + " " + tostring(keystmp[1])  + " ";

          keystmp[0] = ite->second[2];
          keystmp[1] = ite->second[0];
          std::sort(keystmp.begin(), keystmp.end());
          keys[2] = tostring(keystmp[0]) + " " + tostring(keystmp[1])  + " ";
        }
        else if (ite->first == 5) //(Quadrilaterals)
        {
          // number_t nbFaces = 4;
          keys.resize(nbFaces);
          keystmp.resize(2);
          keystmp[0] = ite->second[0];
          keystmp[1] = ite->second[1];
          std::sort(keystmp.begin(), keystmp.end());
          keys[0] = tostring(keystmp[0]) + " " + tostring(keystmp[1])  + " ";

          keystmp[0] = ite->second[1];
          keystmp[1] = ite->second[2];
          std::sort(keystmp.begin(), keystmp.end());
          keys[1] = tostring(keystmp[0]) + " " + tostring(keystmp[1])  + " ";

          keystmp[0] = ite->second[2];
          keystmp[1] = ite->second[3];
          std::sort(keystmp.begin(), keystmp.end());
          keys[2] = tostring(keystmp[0]) + " " + tostring(keystmp[1])  + " ";

          keystmp[0] = ite->second[3];
          keystmp[1] = ite->second[0];
          std::sort(keystmp.begin(), keystmp.end());
          keys[3] = tostring(keystmp[0]) + " " + tostring(keystmp[1])  + " ";
        }

        for (number_t s = 0; s < nbFaces; s++) // loop on element sides
        {
          string_t key = keys[s];
          NbTrgInterface++;
          itsn = sideIndex.find(key);  // search side in sideIndex map
          if (itsn == sideIndex.end()) // create a new side element
          {
            nbside++;
            sideIndex[key] = o; // update sideIndex
          }
          else // here, side already exists so it is not on the boundary side so it is erased.
          {
            nbside--;
            sideIndex.erase(key); // erasing by key
          } // end of else
        } // end for (number_t s
        for (itn = (ite->second).begin(); itn != (ite->second).end(); itn++)
        {
          ss << *itn << " ";
        }
        ss << 1 << eol;
      }
      mdout << " " << eol << SideElemNature << eol << tostring(nbside) << eol;
      if (spaceInterp->numtype ==0)
      {
        cdout << " " << eol << "SolAt"<<SideElemNature << eol << tostring(nbside) << eol;
        cdout << tostring(nterm)<<" " << dataInfo<<eol ;
        for (std::map<string_t, number_t>::const_iterator it = sideIndex.begin(); it != sideIndex.end(); ++it)
        {
           mdout << tostring(it->first) << " " << tostring(1) << eol;
           cdout<<  tostring(VVV[it->second-1])<<eol;
        }
      }
      else
      {
        for (std::map<string_t, number_t>::const_iterator it = sideIndex.begin(); it != sideIndex.end(); ++it)
           mdout << tostring(it->first) << " " << tostring(1) << eol;
      }
      cdout<< eol;
    } // end 3D
    else
    {
      // loop on elem
      for (ite = elementsInfo.begin(); ite != elementsInfo.end(); ++ite, ++o)
      {
        for (itn = (ite->second).begin(); itn != (ite->second).end(); itn++)
        {
          ss << *itn << " ";
        }
        ss << ite->first<< eol;
      }
    }
    mdout << " " << eol;
    mdout << MeshElemNature << eol << tostring(nbElt) << eol;
    mdout << ss.str();
    mdout << " " << eol;
    mdout << "End" << eol;

    fout << cdout.str();
    fout << fdout.str();
    fout << mdout.str();

    trace_p->pop();
  }

  //
  //! save a list of SuTermVectors on the same Space to msh files
  void saveToMsh(ostream &fout, const Space *sp, const list<SuTermVector *> &sutvs, const vector<Point> &coords,
                 const vector<pair<ShapeType, vector<number_t>>> &elementsInfo, const GeomDomain *dom, string_t dataName)
  {
    trace_p->push("saveToMsh(...)");
    mshExport(*dom, coords, elementsInfo, fout);

    // export values
    list<SuTermVector *>::const_iterator itv;
    const Interpolation *spaceInterp = sp->rootSpace()->feSpace()->interpolation();
    if (spaceInterp->type == _Lagrange)
    {
      if (spaceInterp->subtype != _standard && spaceInterp->subtype != _GaussLobattoPoints)
      {
        error("export_not_yet_implemented", words("non standard or GaussLobatto Lagrange"), "");
      }
      switch (spaceInterp->numtype)
      {
      case 0: // save values by cell
      {
        for (itv = sutvs.begin(); itv != sutvs.end(); itv++) // loop on SuTermVectors
        {
          SuTermVector &stv = **itv;
          string_t na = dataName;
          if (na == "")
          {
            na = stv.name();
            if (na == "")
            {
              na = "term_" + stv.up()->name();
            }
          }
          replaceChar(na, ' ', '_');
          dimen_t nbc = stv.up()->nbOfComponents();
          if (nbc == 1)
          {
            switch (stv.valueType())
            {
            case _real:
            {
              fout << "$ElementData" << endl;
              fout << "1" << endl; // number of string tags
              fout << "\"" << na << "\"" << endl;
              fout << "1" << endl;          // number of real tags
              fout << "0.0" << endl;        // the time value associated with the dataset
              fout << "3" << endl;          // number of integer tags
              fout << "0" << endl;          // the time step
              fout << "1" << endl;          // 1 for scalar field
              fout << stv.nbDofs() << endl; // the number of values

              vector<real_t>::const_iterator iv;
              number_t i = 1;
              for (iv = stv.entries()->rEntries_p->begin(); iv != stv.entries()->rEntries_p->end(); ++iv, ++i)
              {
                fout << i << " " << *iv << endl;
              }
              fout << "$EndElementData" << endl;
              break;
            }
            case _complex: // saved real part, imaginary part and modulus
            {
              fout << "$ElementData" << endl;
              fout << "1" << endl; // number of string tags
              fout << "\"" << na << "\"" << endl;
              fout << "1" << endl;          // number of real tags
              fout << "0.0" << endl;        // the time value associated with the dataset
              fout << "3" << endl;          // number of integer tags
              fout << "0" << endl;          // the time step
              fout << "3" << endl;          // 3 for vector field (real part, imaginary part and modulus here)
              fout << stv.nbDofs() << endl; // the number of values

              vector<complex_t>::const_iterator iv;
              number_t i = 1;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); ++iv, ++i)
              {
                fout << i << " " << (*iv).real() << " " << (*iv).imag() << " " << std::abs(*iv) << endl;
              }
              fout << "$EndElementData" << endl;
              break;
            }
            default:
              break;
            }
          }
          else // vector Unknown
          {
            switch (stv.valueType())
            {
            case _real:
            {
              fout << "$ElementData" << endl;
              fout << "1" << endl; // number of string tags
              fout << "\"" << na << "\"" << endl;
              fout << "1" << endl;          // number of real tags
              fout << "0.0" << endl;        // the time value associated with the dataset
              fout << "3" << endl;          // number of integer tags
              fout << "0" << endl;          // the time step
              fout << "3" << endl;          // 3 for vector field
              fout << stv.nbDofs() << endl; // the number of values

              vector<Vector<real_t>>::const_iterator iv;
              number_t i = 1;
              for (iv = stv.entries()->rvEntries_p->begin(); iv != stv.entries()->rvEntries_p->end(); ++iv, ++i)
              {
                fout << i << " ";
                for (number_t j = 0; j < nbc; ++j)
                {
                  fout << (*iv)[j] << " ";
                }
                for (number_t j = nbc; j < 3; ++j)
                {
                  fout << "0. ";
                } // always 3D vector in msh !
                fout << endl;
              }
              fout << "$EndelementData" << endl;
              break;
            }
            case _complex: // saved real part, imaginary part
            {
              fout << "$ElementData" << endl;
              fout << "1" << endl; // number of string tags
              fout << "\"" << na << "\"" << endl;
              fout << "1" << endl;          // number of real tags
              fout << "0.0" << endl;        // the time value associated with the dataset
              fout << "3" << endl;          // number of integer tags
              fout << "0" << endl;          // the time step
              fout << "9" << endl;          // 9 for matrix field (real part, imaginary part and modulus here of each component)
              fout << stv.nbDofs() << endl; // the number of values

              vector<Vector<complex_t>>::const_iterator iv;
              number_t i = 1;
              for (iv = stv.entries()->cvEntries_p->begin(); iv != stv.entries()->cvEntries_p->end(); ++iv, ++i)
              {
                for (number_t j = 0; j < nbc; ++j)
                {
                  fout << (*iv)[j].real() << " " << (*iv)[j].imag() << " " << std::abs((*iv)[j]) << " ";
                }
                for (number_t j = nbc; j < 3; ++j)
                {
                  fout << "0.0 0.0 0.0 ";
                } // always 3D vector in msh !
                fout << endl;
              }
              fout << "$EndElementData" << endl;
              break;
            }
            default:
              break;
            } // end switch real
          } // end vector case
        } // end for
        break;
      } // end case 0
      default: // save values on outMesh=meshp
      {
        for (itv = sutvs.begin(); itv != sutvs.end(); itv++) // loop on SuTermVectors
        {
          SuTermVector &stv = **itv;
          string_t na = dataName;
          if (na == "")
          {
            na = stv.name();
            if (na == "")
            {
              na = "term_" + stv.up()->name();
            }
          }
          replaceChar(na, ' ', '_');
          dimen_t nbc = stv.up()->nbOfComponents();
          if (nbc == 1)
          {
            switch (stv.valueType())
            {
            case _real:
            {
              fout << "$NodeData" << endl;
              fout << "1" << endl; // number of string tags
              fout << "\"" << na << "\"" << endl;
              fout << "1" << endl;          // number of real tags
              fout << "0.0" << endl;        // the time value associated with the dataset
              fout << "3" << endl;          // number of integer tags
              fout << "0" << endl;          // the time step
              fout << "1" << endl;          // 1 for scalar field
              fout << stv.nbDofs() << endl; // the number of values

              vector<real_t>::const_iterator iv;
              number_t i = 1;
              for (iv = stv.entries()->rEntries_p->begin(); iv != stv.entries()->rEntries_p->end(); ++iv, ++i)
              {
                fout << i << " " << *iv << endl;
              }
              fout << "$EndNodeData" << endl;
              break;
            }
            case _complex: // saved real part, imaginary part and modulus
            {
              fout << "$NodeData" << endl;
              fout << "1" << endl; // number of string tags
              fout << "\"" << na << "\"" << endl;
              fout << "1" << endl;          // number of real tags
              fout << "0.0" << endl;        // the time value associated with the dataset
              fout << "3" << endl;          // number of integer tags
              fout << "0" << endl;          // the time step
              fout << "3" << endl;          // 3 for vector field (real part, imaginary part and modulus here)
              fout << stv.nbDofs() << endl; // the number of values

              vector<complex_t>::const_iterator iv;
              number_t i = 1;
              for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); ++iv, ++i)
              {
                fout << i << " " << (*iv).real() << " " << (*iv).imag() << " " << std::abs(*iv) << endl;
              }
              fout << "$EndNodeData" << endl;
              break;
            }
            default:
              break;
            }
          }
          else // vector Unknown
          {
            switch (stv.valueType())
            {
            case _real:
            {
              fout << "$NodeData" << endl;
              fout << "1" << endl; // number of string tags
              fout << "\"" << na << "\"" << endl;
              fout << "1" << endl;          // number of real tags
              fout << "0.0" << endl;        // the time value associated with the dataset
              fout << "3" << endl;          // number of integer tags
              fout << "0" << endl;          // the time step
              fout << "3" << endl;          // 3 for vector field
              fout << stv.nbDofs() << endl; // the number of values

              vector<Vector<real_t>>::const_iterator iv;
              number_t i = 1;
              for (iv = stv.entries()->rvEntries_p->begin(); iv != stv.entries()->rvEntries_p->end(); ++iv, ++i)
              {
                fout << i << " ";
                for (number_t j = 0; j < nbc; ++j)
                {
                  fout << (*iv)[j] << " ";
                }
                for (number_t j = nbc; j < 3; ++j)
                {
                  fout << "0. ";
                } // always 3D vector in msh !
                fout << endl;
              }
              fout << "$EndNodeData" << endl;
              break;
            }
            case _complex: // saved real part, imaginary part
            {
              fout << "$NodeData" << endl;
              fout << "1" << endl; // number of string tags
              fout << "\"" << na << "\"" << endl;
              fout << "1" << endl;          // number of real tags
              fout << "0.0" << endl;        // the time value associated with the dataset
              fout << "3" << endl;          // number of integer tags
              fout << "0" << endl;          // the time step
              fout << "9" << endl;          // 9 for matrix field (real part, imaginary part and modulus here of each component)
              fout << stv.nbDofs() << endl; // the number of values

              vector<Vector<complex_t>>::const_iterator iv;
              number_t i = 1;
              for (iv = stv.entries()->cvEntries_p->begin(); iv != stv.entries()->cvEntries_p->end(); ++iv, ++i)
              {
                for (number_t j = 0; j < nbc; ++j)
                {
                  fout << (*iv)[j].real() << " " << (*iv)[j].imag() << " " << std::abs((*iv)[j]) << " ";
                }
                for (number_t j = nbc; j < 3; ++j)
                {
                  fout << "0.0 0.0 0.0 ";
                } // always 3D vector in msh !
                fout << endl;
              }
              fout << "$EndNodeData" << endl;
              break;
            }
            default:
              break;
            } // end switch real
          } // end vector case
        } // end for
        break;
      } // end case default
      } // end switch interp
    }
    else // not Lagrange interpolation
    {
      error("export_not_yet_implemented", words("non Lagrange"), "");
    }

    trace_p->pop();
  }

  //! save a list of SuTermVectors on the same Space to Matlab - Octave files
  void saveToMtlb(ostream &fout, const Space *sp, const list<SuTermVector *> &sutvs, const vector<Point> &coords,
                  const vector<pair<ShapeType, vector<number_t>>> &elementsInfo, const GeomDomain *dom, string_t dataName)
  {
    trace_p->push("saveToMtlb(...)");
    mtlbExport(*dom, coords, elementsInfo, fout);

    // export values
    list<SuTermVector *>::const_iterator itv;
    const Interpolation *spaceInterp = sp->rootSpace()->feSpace()->interpolation();
    if (spaceInterp->type == _Lagrange)
    {
      if (spaceInterp->subtype != _standard && spaceInterp->subtype != _GaussLobattoPoints)
      {
        error("export_not_yet_implemented", words("non standard or GaussLobatto Lagrange"), "");
      }
      fout << "% Interpolation degree" << endl;
      fout << "interpDeg = " << spaceInterp->numtype << ";" << endl;
      if (spaceInterp->numtype == 0)
      {
        fout << "% Data values in each element, given in the same order as the elements" << endl;
      }
      else
      {
        fout << "% Data values at each interpolation node, given in the same order as their coordinates" << endl;
      }
      fout << "unknown = []; % name of unknowns" << endl;
      fout << "val     = [];   % values for each unknown stored column-wise" << endl;
      // Nota: same treatment in both cases, value at node or value at element (degree 0)
      for (itv = sutvs.begin(); itv != sutvs.end(); itv++) // loop on SuTermVectors
      {
        SuTermVector &stv = **itv;
        string_t na = dataName;
        if (na == "")
        {
          na = stv.name();
          if (na == "")
          {
            na = "term_" + stv.up()->name();
          }
        }
        replaceChar(na, ' ', '_');
        dimen_t nbc = stv.up()->nbOfComponents();
        fout << "%  Add values for unknown " << na << " ; size = [" << stv.nbDofs() << ", " << nbc << "]" << endl;
        fout << "unknown = [unknown; \"" << na;
        if (nbc > 1)
        {
          for (int ii = 1; ii < nbc; ii++)
          {
            fout << ii << "\"; \"" << na;
          }
          fout << nbc;
        }
        fout << "\"];" << endl;
        fout << "val = [val [" << endl;
        if (nbc == 1)
        {
          switch (stv.valueType())
          {
          case _real:
          {
            vector<real_t>::const_iterator iv;
            for (iv = stv.entries()->rEntries_p->begin(); iv != stv.entries()->rEntries_p->end(); ++iv)
            {
              fout << *iv << endl;
            }
            break;
          }
          case _complex:
          {
            vector<complex_t>::const_iterator iv;
            for (iv = stv.entries()->cEntries_p->begin(); iv != stv.entries()->cEntries_p->end(); ++iv)
            {
              fout << iv->real() << std::showpos << iv->imag() << "i" << endl;
            }
            break;
          }
          default:
            break;
          } // end switch
        }
        else // vector Unknown
        {
          switch (stv.valueType())
          {
          case _real:
          {
            vector<Vector<real_t>>::const_iterator iv;
            for (iv = stv.entries()->rvEntries_p->begin(); iv != stv.entries()->rvEntries_p->end(); ++iv)
            {
              for (number_t j = 0; j < nbc; ++j)
              {
                fout << " " << (*iv)[j];
              }
              fout << endl;
            }
            break;
          }
          case _complex:
          {
            vector<Vector<complex_t>>::const_iterator iv;
            for (iv = stv.entries()->cvEntries_p->begin(); iv != stv.entries()->cvEntries_p->end(); ++iv)
            {
              for (number_t j = 0; j < nbc; ++j)
              {
                fout << " " << (*iv)[j].real() << std::showpos << (*iv)[j].imag() << "i";
              }
              fout << endl;
            }
            break;
          }
          default:
            break;
          } // end switch
        } // end else (vector case)
        fout << "]];" << endl;
      } // end for loop on SuTermVectors

      fout << string_t("addpath('" + theEnvironment_p->visuTermVecMPath() + "')") << endl;
      fout << string_t("if exist('opts', 'var') == nullptr, opts = ['']; end") << endl;
      fout << string_t("visuTermVec(coord, elem, elemtype, unknown, val, mfilename, opts)") << endl;
    }
    else // not Lagrange interpolation
    {
      error("export_not_yet_implemented", words("non Lagrange"), "");
    }

    trace_p->pop();
  }

  /*! save a list of SuTermVectors v's on the same Space as raw file x [y [z]] v1 [v2 ...]
      the ouput of coordinates depends of the dimension of mesh nodes
      when vi is a complex output vi.real vi.imag
      when vi is a real vector output vi_x vi_y [vi_z]
      when vi is a complex vector output vi_x.real vi_x.imag vi_y.real vi_y.imag [vi_z.real vi_z.imag]
  */
  void saveToXyzVs(ostream &fout, const Space *sp, const list<SuTermVector *> &sutvs, const vector<Point> &coords, string_t dataName, bool writeHeader)
  {
    trace_p->push("saveToXyzVs(...)");
    list<SuTermVector *>::const_iterator itv;
    const Interpolation *spaceInterp = sp->rootSpace()->feSpace()->interpolation();
    if (spaceInterp->type != _Lagrange || spaceInterp->subtype != _standard)
      error("export_not_yet_implemented", words("non standard Lagrange"), "");

    if (writeHeader)
    {
      dimen_t d = coords[0].size();
      if (d >= 1)
        fout << "x ";
      if (d >= 2)
        fout << "y ";
      if (d >= 3)
        fout << "z ";
      for (itv = sutvs.begin(); itv != sutvs.end(); itv++) // loop on SuTermVectors
      {
        SuTermVector &stv = **itv;
        dimen_t nbc = stv.up()->nbOfComponents();
        ValueType vt = stv.valueType();
        string_t na = dataName;
        if (na == "")
        {
          na = stv.name();
          if (na == "")
          {
            na = "term_" + stv.up()->name();
          }
        }
        if (nbc == 1)
        {
          if (vt == _real)
            fout << na << " ";
          else
            fout << "real(" << na << ") imag(" << na << ") ";
        }
        else
        {
          number_t s = stv.entries()->nbOfComponents;
          if (vt == _real)
            for (number_t i = 1; i <= s; i++)
              fout << na << "_" << i << " ";
          else
            for (number_t i = 1; i <= s; i++)
              fout << "real(" << na << "_" << i << ") imag(" << na << "_" << i << ") ";
        }
      }
      fout << std::endl;
    }

    vector<Point>::const_iterator itp = coords.begin();
    for (number_t i = 0; i < coords.size(); i++, ++itp)
    {
      itp->printRaw(fout);
      for (itv = sutvs.begin(); itv != sutvs.end(); itv++) // loop on SuTermVectors
      {
        SuTermVector &stv = **itv;
        dimen_t nbc = stv.up()->nbOfComponents();
        ValueType vt = stv.valueType();
        if (nbc == 1) // scalar values
        {
          if (vt == _real)
            fout << *(stv.entries()->rEntries_p->begin() + i) << " ";
          else
          {
            complex_t v = *(stv.entries()->cEntries_p->begin() + i);
            fout << v.real() << " " << v.imag() << " ";
          }
        }
        else // vector values
        {
          if (vt == _real)
          {
            Vector<real_t> &v = *(stv.entries()->rvEntries_p->begin() + i);
            for (Vector<real_t>::iterator itv = v.begin(); itv != v.end(); ++itv)
              fout << *itv << " ";
          }
          else
          {
            Vector<complex_t> &v = *(stv.entries()->cvEntries_p->begin() + i);
            for (Vector<complex_t>::iterator itv = v.begin(); itv != v.end(); ++itv)
              fout << itv->real() << " " << itv->imag() << " ";
          }
        }
      }
      fout << std::endl;
    }
    trace_p->pop();
  }

} // end of namespace xlifepp
