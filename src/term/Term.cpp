/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Term.cpp
  \authors D. Martin, E. Lunéville
  \since 03 apr 2012
  \date 03 apr 2012

  \brief Implementation of xlifepp::Term class members and related utilities
*/

#include "Term.hpp"
#include "utils.h"

namespace xlifepp
{

std::ostream& operator<<(std::ostream& out, const TermType& tt)
{ out << words("term type", tt); return out; }

//constructor/destructor

Term::~Term()
{
  std::vector<Term*>::iterator it=find(theTerms.begin(), theTerms.end(),this);
  if(it!=theTerms.end()) theTerms.erase(it);
}

// delete all term objects
void Term::clearGlobalVector()
{
  //delete first TermMatrix and TermVector because they delete their SuTermMatrix's and SuTermVector's
  std::list<Term*> lt;
  std::vector<Term*>::iterator it=theTerms.begin()+2;  //start from 2 to keep theDefaultTermVector and theDefaultTermMatrix
  for(; it!=theTerms.end(); ++it)
        if((*it)->termType_==_termMatrix || (*it)->termType_==_termVector) lt.push_back(*it);
  std::list<Term*>::iterator itl=lt.begin();
  for(; itl!=lt.end(); ++itl) delete *itl;
  //then delete the remaining SuTermVectors and SuTerMatrix's
  number_t s = theTerms.size();
  while(s > 2)
    {
      delete Term::theTerms[s-1];
      s = theTerms.size();
    }
}

std::set<ParameterKey> Term::getParamsKeys()
{
  std::set<ParameterKey> params;
  params.insert(_pk_compute);
  params.insert(_pk_notCompute);
  params.insert(_pk_assembled);
  params.insert(_pk_unassembled);
  params.insert(_pk_name);
  return params;
}

void Term::buildParam(const Parameter& p, bool& toCompute, bool& toAssemble)
{
  switch (p.key())
  {
    case _pk_compute:
    {
      toCompute=true;
      break;
    }
    case _pk_notCompute:
    {
      toCompute=false;
      break;
    }
    case _pk_assembled:
    {
      toAssemble=true;
      break;
    }
    case _pk_unassembled:
    {
      toAssemble=false;
      toCompute=false;
      break;
    }
    case _pk_name:
    {
      switch (p.type())
      {
        case _string:
          this->name_ = p.get_s();
          break;
        default:
          error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    default:
    {
      where("Term::buildParam(...)");
      error("unexpected_param_key", words("param key", p.key()));
    }
  }
}

// print the list of terms in memory
void Term::printAllTerms(std::ostream& out)
{
    number_t vb=theVerboseLevel;
    verboseLevel(1);
    out<<"Terms in memory: "<<eol;
    std::vector<Term*>::iterator it=theTerms.begin();
    for(; it!=theTerms.end(); ++it) out<<"  "<<(*it)<<": "<<(**it)<<eol;
    verboseLevel(vb);
}

//! name without trailing @
string_t Term::primaryName() const
{return trimTrailing(name(),"@");}

// compute terms
void compute(Term& t1)
{
  if(!t1.computed()) t1.compute();
}
void compute(Term& t1, Term& t2)
{
  if(!t1.computed()) t1.compute();
  if(!t2.computed()) t2.compute();
}
void compute(Term& t1, Term& t2, Term& t3)
{
  if(!t1.computed()) t1.compute();
  if(!t2.computed()) t2.compute();
  if(!t3.computed()) t3.compute();
}
void compute(Term& t1, Term& t2, Term& t3, Term& t4)
{
  if(!t1.computed()) t1.compute();
  if(!t2.computed()) t2.compute();
  if(!t3.computed()) t3.compute();
  if(!t4.computed()) t4.compute();
}
void compute(Term& t1, Term& t2, Term& t3, Term& t4, Term& t5)
{
  if(!t1.computed()) t1.compute();
  if(!t2.computed()) t2.compute();
  if(!t3.computed()) t3.compute();
  if(!t4.computed()) t4.compute();
  if(!t5.computed()) t5.compute();
}

//deallocate terms
void clear(Term& t1)
{
  t1.clear();
}
void clear(Term& t1, Term& t2)
{
  t1.clear();
  t2.clear();
}
void clear(Term& t1, Term& t2, Term& t3)
{
  t1.clear();
  t2.clear();
  t3.clear();
}
void clear(Term& t1, Term& t2, Term& t3, Term& t4)
{
  t1.clear();
  t2.clear();
  t3.clear();
  t4.clear();
}
void clear(Term& t1, Term& t2, Term& t3, Term& t4, Term& t5)
{
  t1.clear();
  t2.clear();
  t3.clear();
  t4.clear();
  t5.clear();
}

//print term on output stream
std::ostream& operator<<(std::ostream& out,const Term& t)
{
  t.print(out);
  return out;
}

//delete all terms depending on space sp
void clearTerms(const Space& sp)
{
  number_t s = Term::theTerms.size();
  //first delete TermMatrix and TermVector
  while(s > 2)  // theDefaultTermVector and theDefaultTermMatrix are the two first ones, omit them
    {
      Term* t = Term::theTerms[s-1];
      TermType tt = t->termType();
      bool del=false;
      if(tt==_termMatrix || tt==_termVector)
        {
          std::set<const Space*> sps = t->unknownSpaces();
          std::set<const Space*>::iterator its = sps.begin();
          for(; its!=sps.end() && !del; ++its)
            if(*its == &sp) {delete t; del=true;}
        }
      if(del) s=Term::theTerms.size();
      else s--;
    }
  //delete SuTermMatrix and SuTermVector if they have not been deleted in previous step
  s=Term::theTerms.size();
  while(s > 2)  // theDefaultTermVector and theDefaultTermMatrix are the two first ones, omit them
    {
      Term* t = Term::theTerms[s-1];
      TermType tt = t->termType();
      bool del=false;
      if(tt==_sutermMatrix || tt==_sutermVector)
        {
          std::set<const Space*> sps = t->unknownSpaces();
          std::set<const Space*>::iterator its = sps.begin();
          for(; its!=sps.end() && !del; ++its)
            if(*its == &sp) {delete t; del=true;}
        }
      if(del) s=Term::theTerms.size();
      else s--;
    }
}

// clear all storages depending on a space (implemented in Term.cpp)
void clearStorages(const Space& sp)
{
  number_t n = MatrixStorage::theMatrixStorages.size(), p, q;
  string_t s,s1,s2;
  while(n > 0)
    {
      MatrixStorage* ms = MatrixStorage::theMatrixStorages[n-1];
      s = ms->stringId;
      p = s.find('-');
      s1 = s.substr(0,p);
      q = s.find('-',p+1);
      if(q==string_t::npos) s2 = s.substr(p+1);
      else                  s2 = s.substr(p+1,q-p-1);
      std::stringstream ss;
      ss << &sp;
      string_t sr=ss.str();
      //thePrintStream<<" try to clear "<<s<<" s1="<<s1<<" s2="<<s2<<" sr="<<sr<<eol;
      if(s1==sr || s2==sr)  delete MatrixStorage::theMatrixStorages[0];
      n--;
    }
}

} //end of namespace xlifepp
