/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TensorKernel.hpp
  \author E. Lunéville
  \since 14 nov 2013
  \date  22 oct 2014

  \brief Definition of the xlifepp::TensorKernel class

  xlifepp::TensorKernel class, inherited from xlifepp::Kernel, manages kernels of the following form
                  K(x,y) = sum_mn psi_n(x) Amn phi_m(y)
                  where (psi_n)and (phi_m) are spectral basis

  Such kernel allows to deal with DirichletToNeuman operator. Indeed, the usual DtN term in variational form is
            sum_mn int_Sigma v(x)psi_m(x) Amn int_Gamma u(y)phi_n(y)
  rewritten
            intg_Gamma intg_Sigma  u(y) [sum_mn phi_n(y) Amn psi_m(x)] v(x)

  It manages mainly
    - two xlifepp::SpectralBasis pointers (for phi and psi)
    - a 'matrix' describing Amn as a scalar vector
    - two xlifepp::Function's  xmap, ymap to map spectral basis defined on a domain onto an other domain
    - isDiag: a boolean flag to indicate that the matrix Amn is a diagonal matrix
    - isConjugate: a boolean flag to tell the code to conjugate phi_n in computation of integral ( !!! DEFAULT IS FALSE)
                         intg_Gamma intg_Sigma sum_mn  u(y) * conj(phi_n(y)) * Amn *psi_m(x)* v(x)
                     (see IESPMatrixComputation)
*/


#ifndef TENSOR_KERNEL_HPP
#define TENSOR_KERNEL_HPP


#include "utils.h"
#include "space.h"
#include "TermVector.hpp"
#include "SpectralBasisInt.hpp"

namespace xlifepp
{

/* ---------------------------------------------------------------------------------------
                    TensorKernel class K(x,y)= sum_mn psi_m(x) Amn phi_n(y)
   ---------------------------------------------------------------------------------------*/
class TensorKernel : public Kernel
{
  protected:
    const SpectralBasis* phi_p; //!< pointer to SpectralBasis object
    const SpectralBasis* psi_p; //!< pointer to SpectralBasis object
    VectorEntry* matrix_p;      //!< matrix stored as a vector (pointer)

  public:
    bool isDiag;                //!< true if matrix is a diagonal matrix (assumed M=N)
    Function xmap;              //!< geometric transformation applied to point x
    Function ymap;              //!< geometric transformation applied to point y
    bool isConjugate;           //!< conjugate phi_p in computation (default=false)
    bool freeBasis;             //!< if true, delete SpectralBasis object

  public:
    //constructors
    TensorKernel()             //! default constructor
      : Kernel() {phi_p=nullptr;psi_p=nullptr;matrix_p=nullptr;isDiag=true; isConjugate=false;freeBasis=false;}

    template<typename T>
    TensorKernel(const SpectralBasis&, const std::vector<T>&, bool =false);     //!< constructor from spectral basis with one family and a vector (matrix)
    template<typename T>
    TensorKernel(const SpectralBasis&, const Matrix<T>&, bool =false);          //!< constructor from spectral basis with one family and a matrix
    template<typename T>
    TensorKernel(const SpectralBasis&, const std::vector<T>&, const SpectralBasis&); //!< constructor from spectral basis with two families and a vector (matrix)
    template<typename T>
    TensorKernel(const SpectralBasis&, const Matrix<T>&, const SpectralBasis&); //!< constructor from spectral basis with two families and a matrix
    template<typename T>
    TensorKernel(Function, const std::vector<T>&, bool =false);                 //!< constructor with one family of functions and a vector (matrix)
    template<typename T>
    TensorKernel(Function, const Matrix<T>&, bool =false);                      //!< constructor with one family of functions and a matrix
    template<typename T>
    TensorKernel(Function, const std::vector<T>&, Function);                    //!< constructor with two families of functions and a vector (matrix)
    template<typename T>
    TensorKernel(Function, const Matrix<T>&, Function);                         //!< constructor with two families of functions and a matrix
    template<typename T>
    TensorKernel(const Unknown&, const std::vector<T>&, bool =false);           //!< constructor from unknown with one family and a vector (matrix)
    template<typename T>
    TensorKernel(const Unknown&, const Matrix<T>&, bool =false);                //!< constructor from unknown with one family and a matrix
    template<typename T>
    TensorKernel(const Unknown&, const std::vector<T>&, const Unknown&); //!< constructor from unknown with two families and a vector (matrix)
    template<typename T>
    TensorKernel(const Unknown&, const Matrix<T>&, const Unknown&);      //!< constructor from unknown with two families and a matrix
    template<typename T>
    TensorKernel(const std::vector<TermVector>&, const std::vector<T>&, bool =false); //!< constructor with one family of TermVectors and a vector (matrix)
    template<typename T>
    TensorKernel(const std::vector<TermVector>&, const Matrix<T>&, bool =false);      //!< constructor with one family of TermVectors and a matrix
    template<typename T>
    TensorKernel(const std::vector<TermVector>&, const std::vector<T>&, const std::vector<TermVector>&); //!< constructor with two families of TermVectors and a vector (matrix)
    template<typename T>
    TensorKernel(const std::vector<TermVector>&, const Matrix<T>&, const std::vector<TermVector>&);      //!< constructor with two families of TermVectors and a matrix

    TensorKernel(const TensorKernel&); //!< copy constructor with full copy of SpectralBasis and matrix
    ~TensorKernel();                   //!< destructor
    TensorKernel* clone() const;       //!< clone current tensor kernel

    //@{
    //!accessors
    const VectorEntry& vectorEntry() const {return *matrix_p;}
    const SpectralBasis* phip() const {return phi_p;}
    const SpectralBasis* psip() const {return psi_p;}
    const SpectralBasis& phi()  const {return *phi_p;}
    const SpectralBasis& psi()  const {return *psi_p;}
    //@}

    //properties
    dimen_t dimOfPhi() const {return phi_p->dimFun();}                      //!< returns dimension of phi_p
    dimen_t dimOfPsi() const {return psi_p->dimFun();}                      //!< returns dimension of psi_p
    dimPair dims ()const {return dimPair(phi_p->dimFun(),psi_p->dimFun());} //!< returns (phi_p dim , psi_p dim)
    number_t nbOfPhi() const {return phi_p->numberOfFun();}                 //!< returns number of phi_p
    number_t nbOfPsi() const {return psi_p->numberOfFun();}                 //!< returns number of psi_p
    bool phiIsAnalytic() const {return phi_p->funcFormType()==_analytical;} //!< returns true if phi_p is analytic
    bool psiIsAnalytic() const {return psi_p->funcFormType()==_analytical;} //!< returns true if psi_p is analytic
    virtual KernelType type() const {return _tensorKernel;}                 //!< returns kernel type
    virtual bool isSymbolic() const {return false;}                         //!< returns true is kernel is symbolic
    virtual ValueType valueType() const;            //!< return kernel value type (_real or _complex)
    virtual StrucType strucType() const;            //!< return kernel structure type (_scalar, _vector or _matrix)
    bool sameBasis() const {return phi_p==psi_p;}   //!< returns true if phi_p==psi_p

    //@{
    //! downcast methods
    virtual const TensorKernel* tensorKernel() const {return this;}
    virtual TensorKernel* tensorKernel() {return this;}
    //@}

    //compute facilities
    template<typename T>
    T kernelProduct(number_t, const T&) const;           //!< return A_m*v (n=0,...)
    template<typename T>
    T kernelProduct(number_t, number_t, const T&) const; //!<return A_mn*v (n=0,...,m=0,...)

    //! print utilities
    virtual void print(std::ostream&) const;
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};


/* ---------------------------------------------------------------------------------------
                                       template functions
   ---------------------------------------------------------------------------------------*/
//constructors
template<typename T>
TensorKernel::TensorKernel(const SpectralBasis& phi, const std::vector<T>& mat, bool conj)
{
  phi_p=&phi;
  psi_p=phi_p;
  if (mat.size()<phi_p->numberOfFun())
  {
    where("TensorKernel::TensorKernel(SpectralBasis, Vector, bool)");
    error("is_lesser",mat.size(), phi_p->numberOfFun());
  }
  matrix_p=new VectorEntry(mat);
  isDiag= (mat.size()==phi_p->numberOfFun());
  if (!isDiag)
  {
    if (mat.size() != (phi_p->numberOfFun() * psi_p->numberOfFun()))
    {
      where("TensorKernel::TensorKernel(SpectralBasis, Vector, bool)");
      error("bad_dim",mat.size(), phi_p->numberOfFun()*psi_p->numberOfFun());
    }
  }
  isConjugate = conj;
  freeBasis=false;
}

template<typename T>
TensorKernel::TensorKernel(const SpectralBasis& phi, const Matrix<T>& mat, bool conj)
{
  phi_p=&phi;
  psi_p=phi_p;
  if (mat.size()<phi_p->numberOfFun())
  {
    where("TensorKernel::TensorKernel(SpectralBasis, Matrix, bool)");
    error("is_lesser",mat.size(), phi_p->numberOfFun());
  }
  matrix_p=new VectorEntry(mat);
  isDiag=false;
  if ((mat.numberOfRows() != phi_p->numberOfFun()) || (mat.numberOfCols() != psi_p->numberOfFun()))
  {
    where("TensorKernel::TensorKernel(SpectralBasis, Matrix, bool)");
    error("bad_dims",mat.numberOfRows(), mat.numberOfCols(), phi_p->numberOfFun(), psi_p->numberOfFun());
  }
  isConjugate = conj;
  freeBasis=false;
}

template<typename T>
TensorKernel::TensorKernel(const SpectralBasis& phi, const std::vector<T>& mat, const SpectralBasis& psi)
{
  phi_p=&phi;
  psi_p=&psi;
  if (mat.size()<phi_p->numberOfFun())
  {
    where("TensorKernel::TensorKernel(SpectralBasis, Vector, SpectralBasis)");
    error("is_lesser",mat.size(), phi_p->numberOfFun());
  }
  matrix_p=new VectorEntry(mat);
  isDiag= (mat.size()==phi_p->numberOfFun() && mat.size()==psi_p->numberOfFun());
  if (!isDiag)
  {
    if (mat.size() != (phi_p->numberOfFun() * psi_p->numberOfFun()))
    {
      where("TensorKernel::TensorKernel(SpectralBasis, Vector, SpectralBasis)");
      error("bad_dim",mat.size(), phi_p->numberOfFun()*psi_p->numberOfFun());
    }
  }
  isConjugate = false;
  freeBasis=false;
}

template<typename T>
TensorKernel::TensorKernel(const SpectralBasis& phi, const Matrix<T>& mat, const SpectralBasis& psi)
{
  phi_p=&phi;
  psi_p=&psi;
  if (mat.size()<phi_p->numberOfFun())
  {
    where("TensorKernel::TensorKernel(SpectralBasis, Matrix, SpectralBasis)");
    error("is_lesser",mat.size(), phi_p->numberOfFun());
  }
  matrix_p=new VectorEntry(mat);
  isDiag=false;
  if ((mat.numberOfRows() != phi_p->numberOfFun()) || (mat.numberOfCols() != psi_p->numberOfFun()))
  {
    where("TensorKernel::TensorKernel(SpectralBasis, Matrix, SpectralBasis)");
    error("bad_dims",mat.numberOfRows(), mat.numberOfCols(), phi_p->numberOfFun(), psi_p->numberOfFun());
  }
  isConjugate = false;
  freeBasis=false;
}

template<typename T>
TensorKernel::TensorKernel(const Unknown& phi, const std::vector<T>& mat, bool conj)
{
  if (phi.space()->typeOfSpace()!=_spSpace)
  {
    where("TensorKernel::TensorKernel(Unknown, Vector, bool)");
    error("not_sp_space_type",phi.space()->name());
  }
  phi_p=phi.space()->spSpace()->spectralBasis();
  psi_p=phi_p;
  if (mat.size()<phi_p->numberOfFun())
  {
    where("TensorKernel::TensorKernel(Unknown, Vector, bool)");
    error("is_lesser",mat.size(), phi_p->numberOfFun());
  }
  matrix_p=new VectorEntry(mat);
  isDiag= (mat.size()==phi_p->numberOfFun());
  if (!isDiag)
  {
    if (mat.size() != (phi_p->numberOfFun() * psi_p->numberOfFun()))
    {
      where("TensorKernel::TensorKernel(Unknown, Vector, bool)");
      error("bad_dim",mat.size(), phi_p->numberOfFun()*psi_p->numberOfFun());
    }
  }
  isConjugate = conj;
  name = "tensor kernel based on "+phi.name();
  symmetry = _noSymmetry;
  if(!conj) symmetry = _symmetric;
  freeBasis=false;
}

template<typename T>
TensorKernel::TensorKernel(const Unknown& phi, const Matrix<T>& mat, bool conj)
{
  if (phi.space()->typeOfSpace()!=_spSpace)
  {
    where("TensorKernel::TensorKernel(Unknown, Matrix, bool)");
    error("not_sp_space_type",phi.space()->name());
  }
  phi_p=phi.space()->spSpace()->spectralBasis();
  psi_p=phi_p;
  if (mat.size()<phi_p->numberOfFun())
  {
    where("TensorKernel::TensorKernel(Unknown, Matrix, bool)");
    error("is_lesser",mat.size(), phi_p->numberOfFun());
  }
  matrix_p=new VectorEntry(mat);
  isDiag=false;
  if ((mat.numberOfRows() != phi_p->numberOfFun()) || (mat.numberOfCols() != psi_p->numberOfFun()))
  {
    where("TensorKernel::TensorKernel(Unknown, Vector, bool)");
    error("bad_dims",mat.numberOfRows(), mat.numberOfCols(), phi_p->numberOfFun(), psi_p->numberOfFun());
  }
  isConjugate = conj;
  name = "tensor kernel based on "+phi.name();
  symmetry = _noSymmetry; // may be symmetric if mat is symmetric
  freeBasis=false;
}

template<typename T>
TensorKernel::TensorKernel(const Unknown& phi, const std::vector<T>& mat, const Unknown& psi)
{
  if (phi.space()->typeOfSpace()!=_spSpace)
  {
    where("TensorKernel::TensorKernel(Unknown, Vector, Unknown)");
    error("not_sp_space_type",phi.space()->name());
  }
  if (psi.space()->typeOfSpace()!=_spSpace)
  {
    where("TensorKernel::TensorKernel(Unknown, Vector, Unknown)");
    error("not_sp_space_type",psi.space()->name());
  }
  phi_p=phi.space()->spSpace()->spectralBasis();
  psi_p=psi.space()->spSpace()->spectralBasis();
  if (mat.size()<phi_p->numberOfFun())
  {
    where("TensorKernel::TensorKernel(Unknown, Vector, Unknown)");
    error("is_lesser",mat.size(), phi_p->numberOfFun());
  }
  matrix_p=new VectorEntry(mat);
  isDiag= (mat.size()==phi_p->numberOfFun() && mat.size()==psi_p->numberOfFun());
  if (!isDiag)
  {
    if (mat.size() != (phi_p->numberOfFun() * psi_p->numberOfFun()))
    {
      where("TensorKernel::TensorKernel(Unknown, Vector, Unknown)");
      error("bad_dim",mat.size(), phi_p->numberOfFun()*psi_p->numberOfFun());
    }
  }
  isConjugate = false;
  name = "tensor kernel based on "+phi.name()+" and "+psi.name();
  symmetry = _noSymmetry; // may be symmetric if mat is symmetric
  freeBasis=false;
}

template<typename T>
TensorKernel::TensorKernel(const Unknown& phi, const Matrix<T>& mat, const Unknown& psi)
{
  if (phi.space()->typeOfSpace()!=_spSpace)
  {
    where("TensorKernel::TensorKernel(Unknown, Matrix, Unknown)");
    error("not_sp_space_type",phi.space()->name());
  }
  if (psi.space()->typeOfSpace()!=_spSpace)
  {
    where("TensorKernel::TensorKernel(Unknown, Matrix, Unknown)");
    error("not_sp_space_type",psi.space()->name());
  }
  phi_p=phi.space()->spSpace()->spectralBasis();
  psi_p=psi.space()->spSpace()->spectralBasis();
  if (mat.size()<phi_p->numberOfFun())
  {
    where("TensorKernel::TensorKernel(Unknown, Matrix, Unknown)");
    error("is_lesser",mat.size(), phi_p->numberOfFun());
  }
  matrix_p=new VectorEntry(mat);
  isDiag=false;
  if ((mat.numberOfRows() != phi_p->numberOfFun()) || (mat.numberOfCols() != psi_p->numberOfFun()))
  {
    where("TensorKernel::TensorKernel(Unknown, Matrix, Unknown)");
    error("bad_dims",mat.numberOfRows(), mat.numberOfCols(), phi_p->numberOfFun(), psi_p->numberOfFun());
  }
  isConjugate = false;
  name = "tensor kernel based on "+phi.name()+" and "+psi.name();
  symmetry = _noSymmetry; // may be symmetric if mat is symmetric
  freeBasis=false;
}

// constructor with families of function and a vector(matrix)
// Function object has to embed the number of function in parameters with "basis size" parameter

//! constructor with one family of functions and a vector(matrix)
template<typename T>
TensorKernel::TensorKernel(Function fs, const std::vector<T>& mat, bool conj)
{
  //create spectral space on fly, no GeomDomain !
  Parameters* parfs=fs.params_p();
  number_t n=0;
  if (!parfs->contains("basis size"))
  {
      n=mat.size();  //use mat.size to get the number of functions
//    where("TensorKernel::TensorKernel(Function, Vector, bool)");
//    error("param_missing","basis size");

  }
  else n=parfs->get("basis size",n);
  if (n==0)
  {
    where("TensorKernel::TensorKernel(Function, Vector, bool)");
    error("is_null","basis size");
  }
  phi_p = new SpectralBasisFun(fs,n);
  psi_p = phi_p;
  if (mat.size()<n)
  {
    where("TensorKernel::TensorKernel(Function, Vector, bool)");
    error("is_lesser",mat.size(), n);
  }
  matrix_p=new VectorEntry(mat);
  isDiag= (mat.size()==phi_p->numberOfFun());
  if (!isDiag)
  {
    if (mat.size() != (phi_p->numberOfFun() * psi_p->numberOfFun()))
    {
      where("TensorKernel::TensorKernel(Function, Vector, bool)");
      error("bad_dim",mat.size(), phi_p->numberOfFun()*psi_p->numberOfFun());
    }
  }
  isConjugate = conj;
  name = "tensor kernel based on functions "+fs.name();
  symmetry = _noSymmetry;
  if(!conj) symmetry = _symmetric;
  freeBasis=true;
}

//! constructor with one family of functions and a matrix
template<typename T>
TensorKernel::TensorKernel(Function fs, const Matrix<T>& mat, bool conj)
{
  //create spectral space on fly, no GeomDomain !
  Parameters* parfs=fs.params_p();
  number_t n=0;
  if (!parfs->contains("basis size"))
  {
     n=mat.numberOfRows();  //use mat.numberOfRows to get the number of functions
//     where("TensorKernel::TensorKernel(Function, Matrix, bool)");
//     error("param_missing","basis size");
  }
  else n=parfs->get("basis size",n);
  if (n==0)
  {
    where("TensorKernel::TensorKernel(Function, Matrix, bool)");
    error("is_null","basis size");
  }
  phi_p = new SpectralBasisFun(fs,n);
  psi_p = phi_p;
  if (mat.size()<n)
  {
    where("TensorKernel::TensorKernel(Function, Matrix, bool)");
    error("is_lesser",mat.size(), n);
  }
  matrix_p=new VectorEntry(mat);
  isDiag=false;
  if ((mat.numberOfRows() != phi_p->numberOfFun()) || (mat.numberOfCols() != psi_p->numberOfFun()))
  {
    where("TensorKernel::TensorKernel(Function, Matrix, bool)");
    error("bad_dims",mat.numberOfRows(), mat.numberOfCols(), phi_p->numberOfFun(), psi_p->numberOfFun());
  }
  isConjugate = conj;
  name = "tensor kernel based on "+fs.name();
  symmetry = _noSymmetry;
  freeBasis=true;
}

//! constructor with two families of functions and a vector(matrix)
template<typename T>
TensorKernel::TensorKernel(Function fs, const std::vector<T>& mat, Function gs)
{
  //create spectral spaces on fly, no GeomDomain !
  Parameters* pars=fs.params_p();
  number_t n=0;
  if (!pars->contains("basis size"))
  {
    n=mat.size();  //use mat.size to get the number of functions
//    where("TensorKernel::TensorKernel(Function, Vector, Function)");
//    error("param_missing","basis size");
  }
  else n=pars->get("basis size",n);
  if (n==0)
  {
    where("TensorKernel::TensorKernel(Function, Vector, Function)");
    error("is_null","basis size");
  }
  phi_p = new SpectralBasisFun(fs,n);
  if (&fs == &gs) psi_p = phi_p;
  else
  {
    pars=gs.params_p();
    n=0;
    if (!pars->contains("basis size"))
    {
      n=mat.size();  //use mat.size to get the number of functions
//      where("TensorKernel::TensorKernel(Function, Vector, Function)");
//      error("param_missing","basis size");
    }
    else n=pars->get("basis size",n);
    if (n==0)
    {
      where("TensorKernel::TensorKernel(Function, Vector, Function)");
      error("is_null","basis size");
    }
    psi_p = new SpectralBasisFun(gs,n);
  }

  if (mat.size()<n)
  {
    where("TensorKernel::TensorKernel(Function, Vector, Function)");
    error("is_lesser",mat.size(), n);
  }
  matrix_p=new VectorEntry(mat);
  isDiag= (mat.size()==phi_p->numberOfFun());
  if (!isDiag)
  {
    if (mat.size() != (phi_p->numberOfFun() * psi_p->numberOfFun()))
    {
      where("TensorKernel::TensorKernel(Function, Vector, Function)");
      error("bad_dim",mat.size(), phi_p->numberOfFun()*psi_p->numberOfFun());
    }
  }
  isConjugate = false;
  name = "tensor kernel based on functions "+fs.name()+" and "+gs.name();
  symmetry = _noSymmetry;
  freeBasis=true;
}

//! constructor with two families of functions and a matrix
template<typename T>
TensorKernel::TensorKernel(Function fs, const Matrix<T>& mat, Function gs)
{
  //create spectral spaces on fly, no GeomDomain !
  Parameters* pars=fs.params_p();
  number_t n=0;
  if (!pars->contains("basis size"))
  {
    n=mat.numberOfRows();  //use mat.numberOfRows to get the number of functions
//    where("TensorKernel::TensorKernel(Function, Matrix, Function)");
//    error("param_missing","basis size");
  }
  else n=pars->get("basis size",n);
  if(n==0)
  {
    where("TensorKernel::TensorKernel(Function, Matrix, Function)");
    error("is_null","basis size");
  }
  phi_p = new SpectralBasisFun(fs,n);
  if(&fs == &gs) psi_p = phi_p;
  else
  {
    pars=gs.params_p();
    n=0;
    if(!pars->contains("basis size"))
    {
      n=mat.numberOfCols();  //use mat.numberOfCols to get the number of functions
//      where("TensorKernel::TensorKernel(Function, Matrix, Function)");
//      error("param_missing","basis size");
    }
    else n=pars->get("basis size",n);
    if(n==0)
    {
      where("TensorKernel::TensorKernel(Function, Matrix, Function)");
      error("is_null","basis size");
    }
    psi_p = new SpectralBasisFun(gs,n);
  }

  if(mat.size()<n)
  {
    where("TensorKernel::TensorKernel(Function, Matrix, Function)");
    error("is_lesser",mat.size(), n);
  }
  matrix_p=new VectorEntry(mat);
  isDiag=false;
  if ((mat.numberOfRows() != phi_p->numberOfFun()) || (mat.numberOfCols() != psi_p->numberOfFun()))
  {
    where("TensorKernel::TensorKernel(SpectralBasis, Matrix, bool)");
    error("bad_dims",mat.numberOfRows(), mat.numberOfCols(), phi_p->numberOfFun(), psi_p->numberOfFun());
  }
  isConjugate = false;
  name = "tensor kernel based on functions "+fs.name()+" and "+gs.name();
  symmetry = _noSymmetry;
  freeBasis=true;
}

// constructor with families of TermVectors and a vector(matrix)
template<typename T>
TensorKernel::TensorKernel(const std::vector<TermVector>& phis, const std::vector<T>& mat, bool conj)
{
  phi_p = new SpectralBasisInt(phis);
  psi_p =phi_p;
  if (mat.size()<phi_p->numberOfFun())
  {
    where("TensorKernel::TensorKernel(TermVectors, Vector, bool)");
    error("is_lesser",mat.size(), phi_p->numberOfFun());
  }
  matrix_p=new VectorEntry(mat);
  isDiag= (mat.size()==phi_p->numberOfFun());
  if (!isDiag)
  {
    if (mat.size() != (phi_p->numberOfFun() * psi_p->numberOfFun()))
    {
      where("TensorKernel::TensorKernel(TermVectors, Vector, bool)");
      error("bad_dim",mat.size(), phi_p->numberOfFun()*psi_p->numberOfFun());
    }
  }
  isConjugate = conj;
  name = "tensor kernel based on TermVectors ";
  symmetry = _noSymmetry;
  if(!conj) symmetry = _symmetric;
  freeBasis=true;
}

// constructor with families of TermVectors and a matrix
template<typename T>
TensorKernel::TensorKernel(const std::vector<TermVector>& phis, const Matrix<T>& mat, bool conj)
{
  phi_p = new SpectralBasisInt(phis);
  psi_p =phi_p;
  if (mat.size()<phi_p->numberOfFun())
  {
    where("TensorKernel::TensorKernel(TermVectors, Matrix, bool)");
    error("is_lesser",mat.size(), phi_p->numberOfFun());
  }
  matrix_p=new VectorEntry(mat);
  isDiag=false;
  if ((mat.numberOfRows() != phi_p->numberOfFun()) || (mat.numberOfCols() != psi_p->numberOfFun()))
  {
    where("TensorKernel::TensorKernel(SpectralBasis, Matrix, bool)");
    error("bad_dims",mat.numberOfRows(), mat.numberOfCols(), phi_p->numberOfFun(), psi_p->numberOfFun());
  }
  isConjugate = conj;
  name = "tensor kernel based on TermVectors ";
  symmetry = _noSymmetry;
  freeBasis=true;
}

template<typename T>
TensorKernel::TensorKernel(const std::vector<TermVector>& phis, const std::vector<T>& mat, const std::vector<TermVector>& psis)
{
  phi_p= new SpectralBasisInt(phis);
  psi_p= new SpectralBasisInt(psis);
  if (mat.size()<phi_p->numberOfFun())
  {
    where("TensorKernel::TensorKernel(TermVectors, Vector, TermVectors)");
    error("is_lesser",mat.size(), phi_p->numberOfFun());
  }
  matrix_p=new VectorEntry(mat);
  isDiag= (mat.size()==phi_p->numberOfFun());
  if (!isDiag)
  {
    if (mat.size() != (phi_p->numberOfFun() * psi_p->numberOfFun()))
    {
      where("TensorKernel::TensorKernel(TermVectors, Vector, TermVectors)");
      error("bad_dim",mat.size(), phi_p->numberOfFun()*psi_p->numberOfFun());
    }
  }
  isConjugate = false;
  name = "tensor kernel based on TermVectors ";
  symmetry = _noSymmetry;
  freeBasis=true;
}

template<typename T>
TensorKernel::TensorKernel(const std::vector<TermVector>& phis, const Matrix<T>& mat, const std::vector<TermVector>& psis)
{
  phi_p= new SpectralBasisInt(phis);
  psi_p= new SpectralBasisInt(psis);
  if (mat.size()<phi_p->numberOfFun())
  {
    where("TensorKernel::TensorKernel(TermVectors, Matrix, TermVectors)");
    error("is_lesser",mat.size(), phi_p->numberOfFun());
  }
  matrix_p=new VectorEntry(mat);
  isDiag=false;
  if ((mat.numberOfRows() != phi_p->numberOfFun()) || (mat.numberOfCols() != psi_p->numberOfFun()))
  {
    where("TensorKernel::TensorKernel(SpectralBasis, Matrix, bool)");
    error("bad_dims",mat.numberOfRows(), mat.numberOfCols(), phi_p->numberOfFun(), psi_p->numberOfFun());
  }
  isConjugate = false;
  name = "tensor kernel based on TermVectors ";
  symmetry = _noSymmetry;
  freeBasis=true;
}

//utilities for computation of tensorkernel  A_mm*v if diagonal matrix else A_mn*v
template<typename T>
T TensorKernel::kernelProduct(number_t m, const T& v) const  //return A_m*v (n=0,...)
{
  return matrix_p->product(m,v);
}

template<typename T>
T TensorKernel::kernelProduct(number_t m, number_t n, const T& v) const  //return A_mn*v (n=0,...,m=0,...)
{
  return matrix_p->product(m*nbOfPhi()+n,v);
}

// template<typename itphi, typename itpsi, typename itmat, typename T>
// T& kernelProduct(itphi ith, itphi ithe, itmat itm, itpsi its, T& res)
// {
//  for(; ith!=ithe; ith++, its++, itm++) res+=*its * *itm * *ith;
//  return res;
// }

// template<typename itphi, typename itpsi, typename itmat, typename T>
// T& kernelProduct(itphi ithb, itphi ithe, itmat itm, itpsi itsb, itpsi itse, T& res)
// {
//  itpsi its=itsb;
//  for(; its!=itse; its++)
//    {
//      itphi ith=ithb;
//      T v(0);
//      for(; ith!=ithb; ith++, itm++) v+=*itm * *ith;
//      res+=*its * v;
//    }
//  return res;
// }
// //compute Kernel values
// template<typename T>
// T& TensorKernel::compute(const Point& x, const Point& y, T& res) const  //! compute kernel value at (x,y)
// {
//  res=T(0);

//  //compute values  phi_n(x) and psi_m(y)
//  std::vector<T> phi(dimOfPhi_), psi(dimOfPsi_);
//  typename std::vector<T>::iterator ith=phi.begin(), its=psi.begin();
//  for(number_t n = 0; n < dimOfPhi_; n++, ith++)
//    {
//      phi_.params()("basis index") = n;
//      phi_(y, *ith);
//    }
//  for(number_t n = 0; n < dimOfPsi_; n++,its++)
//    {
//      psi_.params()("basis index") = n;
//      psi_(x, *its);
//    }

//  switch(matrix_p->valueType)
//    {
//      case _real:
//        switch(matrix_p->strucType)
//          {
//            case _scalar:
//              {
//                Vector<real_t>::const_iterator itm=matrix_p->beginr();
//                if(isDiag_) return kernelProduct(phi.begin(), phi.end(), itm, psi.begin(), res);
//                else    return kernelProduct(phi.begin(),phi.end(), itm, psi.begin(), psi.end(), res);
//              }

//            case _vector:
//              {
//                Vector<Vector<real_t> >::const_iterator itm=matrix_p->beginrv();
//                if(isDiag_) return kernelProduct(phi.begin(),phi.end(), itm, psi.begin(), res);
//                else    return kernelProduct(phi.begin(),phi.end(), itm, psi.begin(), psi.end(), res);
//              }
//          }
//        break;
//      case _complex: switch(matrix_p->strucType)
//          {
//            case _scalar:
//              {
//                Vector<complex_t>::const_iterator itm=matrix_p->beginc();
//                if(isDiag_) return kernelProduct(phi.begin(),phi.end(), itm, psi.begin(), res);
//                else    return kernelProduct(phi.begin(),phi.end(), itm, psi.begin(), psi.end(), res);
//              }
//            case _vector:
//              {
//                Vector<Vector<complex_t> >::const_iterator itm=matrix_p->begincv();
//                if(isDiag_) return kernelProduct(phi.begin(),phi.end(), itm, psi.begin(), res);
//                else    return kernelProduct(phi.begin(),phi.end(), itm, psi.begin(), psi.end(), res);
//              }
//          }
//    }
// }

// template<typename T>
// Vector<T>& TensorKernel::compute(const Vector<Point>& x, const Vector<Point>& y, Vector<T>& res) const //! compute kernel value at a list of points
// {
//  res.resize(x.size());
//  Vector<Point>::const_iterator itx=x.begin(), ity=y.begin();
//  typename Vector<T>::iterator itr=res.begin();
//  for(; itx!=x.end(); itx++, ity++, itr++) compute(*itx,*ity,*itr);
//  return res;
// }

// //trick to solve the template virtualization paradigm
// template<typename T>
// T& compute(const TensorKernel& tk, const Point& x, const Point& y, T& res)  //! compute kernel value at (x,y)
// {return tk.compute(x,y,res);}

// template<typename T>
// Vector<T>& compute(const TensorKernel& tk, const Point& x, const Point& y, Vector<T>& res)  //! compute kernel value at (x,y)
// {return tk.compute(x,y,res);}

} // end of namespace xlifepp

#endif // TENSOR_KERNEL_HPP
