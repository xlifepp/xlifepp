/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ExplicitIntgRep.hpp
  \authors N.Salles
  \since 25 March  2016
  \date 25 March 216

  \brief header file for explicit formulas to compute integrals arising in integral representation.
 */

#include "config.h"
#include "utils.h"
#include "finiteElements.h"
#include "geometry.h"
#include "space.h"

namespace xlifepp {
/*!-------------------------------------------------------------------------------
 Computation functions
-------------------------------------------------------------------------------*/
  void LaplaceSLP1(const Element* elt, const Point &pos, Vector<real_t> & res);
  void LaplaceDLP1(const Element* elt, const Point &pos, Vector<real_t> & res);
  real_t LaplaceSLP0(const Element* elt, const Point &pos);
  real_t LaplaceDLP0(const Element* elt, const Point &pos);

/*!-------------------------------------------------------------------------------
 Explicit primitives
-------------------------------------------------------------------------------*/
  real_t integrandLapSLP0(const Point& Sm, const Point& Sp, real_t h,real_t d, const Point& Ip, real_t alpha);
  real_t integrandLapDLP0(const Point& Sm, const Point& Sp, real_t h,real_t d, const Point& Ip);
  real_t integrandLapSLP1const(const Point& Sm, const Point& Sp, real_t h,real_t d, const Point& Ip, real_t alpha);
  void integrandLapSLP1lin(const Point & Sm, const Point & Sp, real_t h, real_t d, const Point& Ip, Vector<real_t> & res, real_t alpha);
  real_t integrandLapDLP1const(const Point& Sm, const Point& Sp, real_t h,real_t d, const Point& Ip);
  void integrandLapDLP1lin(const Point & Sm, const Point & Sp, real_t h, real_t d, const Point& Ip, Vector<real_t> & res);


/*!-------------------------------------------------------------------------------
 Internal functions
-------------------------------------------------------------------------------*/
   void computeGeometricalQuantities(const Point & S1, const Point & S2, const Point & S3, const Point & normalT,const Point &X,Vector<Point> & I, real_t & h, bool I3=false);

  real_t signe(real_t x);

/*================================
 HELMHOLTZ
 ===============================*/
template<class T>
void HelmholtzSingDLP1(T k, const Element* elt, const Point &pos, Vector<T> & res)
{
  real_t h=0.;
  const MeshElement* melt=elt->geomElt_p->meshElement();
  const Point & S1=melt->node(1);
  const Point & S2=melt->node(2);
  const Point & S3=melt->node(3);
  std::vector<real_t> & normalT=melt->geomMapData_p->normalVector;
  Point normalTp(normalT);
  Vector<Point> I(4);
  computeGeometricalQuantities(S1,S2,S3, normalTp,pos,I,h,true);
  Point ptmp=pos-I[3];
  real_t coef=(ptmp[0]*normalT[0]+ptmp[1]*normalT[1]+ptmp[2]*normalT[2])/norm2(ptmp); // We normalize to include h in the integrand
  ShapeValues shv=elt->computeShapeValues(I[3], false,false,false);
  Vector<real_t> signedD(3,0.);

  std::vector< Vector<real_t> > & nu=melt->geomMapData_p->sideNV();

  // Numerotation sideNV
  signedD[0]=-((pos[0]-I[0][0])*(nu[1][0])+(pos[1]-I[0][1])*(nu[1][1])+(pos[2]-I[0][2])*(nu[1][2]));
  signedD[1]=-((pos[0]-I[1][0])*(nu[2][0])+(pos[1]-I[1][1])*(nu[2][1])+(pos[2]-I[1][2])*(nu[2][2]));
  signedD[2]=-((pos[0]-I[2][0])*(nu[0][0])+(pos[1]-I[2][1])*(nu[0][1])+(pos[2]-I[2][2])*(nu[0][2]));

  real_t common=0., commonr=0.;
  // Compute common term
  real_t d=std::abs(signedD[0]);
  if(d> theEpsilon)
    {
      common+=signe(signedD[0])*integrandLapDLP1const(S2,S3,h,d, I[0]);
      commonr+=signe(signedD[0])*integrandLapSLP1const(S2,S3,h,d, I[0],h);
    }

  d=std::abs(signedD[1]);
  if(d > theEpsilon)
    {
      common+=signe(signedD[1])*integrandLapDLP1const(S3,S1,h,d, I[1]);
      commonr+=signe(signedD[1])*integrandLapSLP1const(S3,S1,h,d, I[1],h);
    }

  d=std::abs(signedD[2]);
  if(d > theEpsilon)
    {
      common+=signe(signedD[2])*integrandLapDLP1const(S1,S2,h,d, I[2]);
      commonr+=signe(signedD[2])*integrandLapSLP1const(S1,S2,h,d, I[2],h);
    }


  // Now compute the linear part
  Vector<real_t> tmp(2,0.); // Use to compute some temporary integrals
  Vector<real_t> tmpr(2,0.); // Use to compute some temporary integrals

  Vector<Point> V(3);
  V[0]=S3-S2;
  V[1]=S1-S3;
  V[2]=S2-S1;
  Vector<real_t> normA(3);
  normA[0]=norm2(V[0]);
  normA[1]=norm2(V[1]);
  normA[2]=norm2(V[2]);

  // res = vector with values corresponding to phi0, phi1, phi2.

  // Computation with basis function that does not vanish on [S2,S3]
  d=std::abs(signedD[0]);
  integrandLapDLP1lin(S2,S3,h,d,I[0],tmp);
  integrandLapSLP1lin(S2,S3,h,d,I[0],tmpr,h); // final argument to correct from 1/r to x-y/r

  T coefr=0.5*k*k;

  real_t sm=dot(S2-I[0],V[0])/(normA[0]); // abscissa of S-
  real_t sp=dot(S3-I[0],V[0])/(normA[0]); // abscissa of S-
  real_t eta=signe(sp-sm);
  res[1]+=signe(signedD[0])*((1.+eta*sm/normA[0])*tmp[0] - eta/normA[0]*tmp[1]);
  res[2]+=signe(signedD[0])*eta/normA[0]*(tmp[1] -sm*tmp[0]);
  // Part for 1/r
  res[1]+=coefr*signe(signedD[0])*((1.+eta*sm/normA[0])*tmpr[0] - eta/normA[0]*tmpr[1]); // part 1/r
  res[2]+=coefr*signe(signedD[0])*eta/normA[0]*(tmpr[1] -sm*tmpr[0]);

   // Computation with basis function that does not vanish on [S3,S1]
  tmp=0.;
  tmpr=0.;

  d=std::abs(signedD[1]);
  integrandLapDLP1lin(S3,S1,h,d,I[1],tmp);
  integrandLapSLP1lin(S3,S1,h,d,I[1],tmpr,h);
  sm=dot(S3-I[1],V[1])/(normA[1]); // abscissa of S-
  sp=dot(S1-I[1],V[1])/(normA[1]); // abscissa of S+
  eta=signe(sp-sm);
  //sm=dot(S3-I[1],V[1])/(normA[1]*normA[1]); // abscissa of S-
  res[2]+=signe(signedD[1])*((1.+eta*sm/normA[1])*tmp[0] - eta/normA[1]*tmp[1]);
  res[0]+=signe(signedD[1])*eta/normA[1]*(tmp[1] -eta*sm*tmp[0]);
  res[2]+=coefr*signe(signedD[1])*((1.+eta*sm/normA[1])*tmpr[0] - eta/normA[1]*tmpr[1]);
  res[0]+=coefr*signe(signedD[1])*eta/normA[1]*(tmpr[1] -eta*sm*tmpr[0]);

   // Computation with basis function that does not vanish on [S1,S2]
  tmp=0.;
  tmpr=0.;
  d=std::abs(signedD[2]);
  integrandLapDLP1lin(S1,S2,h,d,I[2],tmp);
  integrandLapSLP1lin(S1,S2,h,d,I[2],tmpr,h);
  sm=dot(S1-I[2],V[2])/(normA[2]); // abscissa of S-
  sp=dot(S2-I[2],V[2])/(normA[2]); // abscissa of S-
  eta=signe(sp-sm);
  res[0]+=signe(signedD[2])*((1.+eta*sm/normA[2])*tmp[0] - eta/normA[2]*tmp[1]);
  res[1]+=signe(signedD[2])*eta/normA[2]*(tmp[1] -sm*tmp[0]);
  res[0]+=coefr*signe(signedD[2])*((1.+eta*sm/normA[2])*tmpr[0] - eta/normA[2]*tmpr[1]);
  res[1]+=coefr*signe(signedD[2])*eta/normA[2]*(tmpr[1] -sm*tmpr[0]);


  for(size_t j=0;j<3;j++)
    {
      res[j]+=shv.w[j]*(common+coefr*commonr);
      res[j]*=coef;
    }
}

template<class T>
T HelmholtzSingDLP0(T k, const Element* elt, const Point &pos)
{
  real_t h=0.;
  const MeshElement* melt=elt->geomElt_p->meshElement();
  const Point & S1=melt->node(1);
  const Point & S2=melt->node(2);
  const Point & S3=melt->node(3);
  std::vector<real_t> & normalT=melt->geomMapData_p->normalVector;
  Point normalTp(normalT);
  Vector<Point> I(4);
  computeGeometricalQuantities(S1,S2,S3, normalTp,pos,I,h,true);
  Point ptmp=pos-I[3];
  real_t coef=dot(ptmp,normalTp)/norm2(ptmp);
  //real_t coef=(ptmp[0]*normalT[0]+ptmp[1]*normalT[1]+ptmp[2]*normalT[2])/norm2(ptmp); // We normalize to include h in the integrand
  Vector<real_t> signedD(3,0.);

  std::vector< Vector<real_t> > & nu=melt->geomMapData_p->sideNV();

  // Numerotation sideNV
  signedD[0]=-((pos[0]-I[0][0])*(nu[1][0])+(pos[1]-I[0][1])*(nu[1][1])+(pos[2]-I[0][2])*(nu[1][2]));
  signedD[1]=-((pos[0]-I[1][0])*(nu[2][0])+(pos[1]-I[1][1])*(nu[2][1])+(pos[2]-I[1][2])*(nu[2][2]));
  signedD[2]=-((pos[0]-I[2][0])*(nu[0][0])+(pos[1]-I[2][1])*(nu[0][1])+(pos[2]-I[2][2])*(nu[0][2]));

  real_t common=0., commonr=0.;
  // Compute common term
  real_t d=std::abs(signedD[0]);
  if(d> theEpsilon)
    {
      common+=signe(signedD[0])*integrandLapDLP0(S2,S3,h,d, I[0]);
      commonr+=signe(signedD[0])*integrandLapSLP0(S2,S3,h,d, I[0],h);
    }

  //  Ip=projectionOnStraightLine(Xp,S3,S1,d);
  d=std::abs(signedD[1]);
  if(d > theEpsilon)
    {
      common+=signe(signedD[1])*integrandLapDLP0(S3,S1,h,d, I[1]);
      commonr+=signe(signedD[1])*integrandLapSLP0(S3,S1,h,d, I[1],h);
    }

  d=std::abs(signedD[2]);
  if(d > theEpsilon)
    {
      common+=signe(signedD[2])*integrandLapDLP0(S1,S2,h,d, I[2]);
      commonr+=signe(signedD[2])*integrandLapSLP0(S1,S2,h,d, I[2],h);
    }

  T coefr=0.5*k*k;
  T res=common+coefr*commonr;

  return res*coef;
}

} // end of namespace xlifepp
