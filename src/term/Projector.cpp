/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
  \file Projector.cpp
  \authors E. Lunéville
  \since 18 nov 2016
  \date  18 nov 2016


  \brief Implementation of functions related to Projector class
  */

#include "Projector.hpp"

namespace xlifepp
{

std::ostream& operator<<(std::ostream& out, const ProjectorType& pt)
{ out << words("Projector", pt); return out; }

//===============================================================================
//member functions of Projector class
//===============================================================================
//constructors, destructor, assignment
Projector::Projector(Space& V, Space& W, ProjectorType pt, const string_t& na)
{
  projectorType=pt;
  name=na;
  V_=&V; W_=&W;
  domain_=W.domain();
  init(1,1);
}

Projector::Projector(Space& V, dimen_t nbcV, Space& W, dimen_t nbcW, ProjectorType pt, const string_t& na)
{
  projectorType=pt;
  name=na;
  V_=&V; W_=&W;
  domain_=W.domain();
  init(nbcV,nbcW);
}

Projector::Projector(Space& V, Space& W, const GeomDomain& d, ProjectorType pt, const string_t& na)
{
  projectorType=pt;
  name=na;
  V_=&V;  W_=&W;
  domain_=&d;
  init(1,1);
}

Projector::Projector(Space& V, dimen_t nbcV, Space& W, dimen_t nbcW, const GeomDomain& d, ProjectorType pt, const string_t& na)
{
  projectorType=pt;
  name=na;
  V_=&V; W_=&W;
  domain_=&d;
  init(nbcV,nbcW);
}

Projector::Projector(Space& V, Space& W, BilinearForm& a, const string_t& na)
{
  projectorType=_userProjector;
  name=na;
  V_=&V;  W_=&W;
  a_W=&a;
  init(1,1);
}

Projector::Projector(Space& V, dimen_t nbcV, Space& W, dimen_t nbcW, BilinearForm& a, const string_t& na)
{
  projectorType=_userProjector;
  name=na;
  V_=&V;  W_=&W;
  a_W=&a;
  init(nbcV,nbcW);
}

Projector::~Projector()
{
  //delete related objects
  if (A_!=nullptr) delete A_;
  if (B_!=nullptr) delete B_;
  if (invA_B!=nullptr) delete invA_B;
  if (a_W!=nullptr) delete a_W;
  if (a_V!=nullptr) delete a_V;
  if (u_W!=nullptr && deluW) delete u_W;
  if (u_V!=nullptr && deluV) delete u_V;
  if (v_W!=nullptr && delvW) delete v_W;
  // remove from list of Projectors
  std::vector<Projector*>::iterator it = find(theProjectors.begin(), theProjectors.end(), this);
  if(it != theProjectors.end()) { theProjectors.erase(it); }
}

//initialize projector, check and compute projection matrix
// nbcV/nbcW are the number of the components of unknown
void Projector::init(dimen_t nbc_V, dimen_t nbc_W)
{
  trace_p->push("Projector::init");
  if (V_==W_) error("equal_spaces");
  if (projectorType==_noProjectorType && a_W==nullptr) projectorType=_L2Projector;
  name=words("Projector", projectorType)+" "+V_->name()+" --> "+W_->name();
  //create internal V and W unknown
  //check dimension, to be improved by taking dimFun in physical space (after mapping)
  dimen_t dimf_V = V_->dimFun(), dimf_W = W_->dimFun(), dimpt = V_->dimPoint();
  if (dimf_V>1 && dimf_V < dimpt) dimf_V = dimpt;     // shapevalues are mapped from dimf_V to V_dimPoint
  if (dimf_W>1 && dimf_W < dimpt) dimf_W = dimpt;     // shapevalues are mapped from dimf_W to V_dimPoint
  if (dimf_V*nbc_V != dimf_W*nbc_W)  error("space_mismatch_dims");
  bool scalar = dimf_V*nbc_V==1;
  string_t na = "u_proj_"+tostring(V_); //index unknown name with space address
  if (nbc_V>1) na+="_"+tostring(nbc_V);
  u_V = findUnknown(na);
  if (u_V==nullptr) {u_V = new Unknown(*V_, _name=na, _dim=nbc_V); deluV=true;}
  na = "u_proj_"+tostring(W_);
  if (nbc_W>1) na+="_"+tostring(nbc_W);
  u_W = findUnknown(na);
  if (u_W==nullptr) {u_W = new Unknown(*W_, _name=na, _dim=nbc_W); deluW=true;}
  na = "v_proj_"+tostring(W_);
  if (nbc_W>1) na+="_"+tostring(nbc_W);
  v_W = static_cast<TestFunction*>(findUnknown(na));
  if (v_W==nullptr) {v_W = new TestFunction(*u_W, _name=na); delvW=true;}

  //create or copy bilinear forms
  if (a_W!=nullptr)
  {
    projectorType=_userProjector;
    if (!a_W->singleUnknown()) error("bform_not_single_unknown");
    if (a_W->begin()->second.type()==_doubleIntg) error("bform_not_single_integral");
    domain_=a_W->begin()->second.dom_up();
    //find largest domain
    if (a_W->begin()->second.type()==_linearCombination)
    {
      SuBilinearForm& subf = a_W->begin()->second;
      std::set<const GeomDomain*> doms;
      for (it_vbfp it=subf.begin(); it!=subf.end(); ++it)
      {
        BasicBilinearForm* bbf=it->first;
        if (bbf->type()!=_intg) error("bform_not_CL_single_integrals");
        doms.insert(&bbf->dom_up());
      }
      domain_ = W_->domain()->largestDomain(std::vector<const GeomDomain*>(doms.begin(),doms.end()));
    }
    //copy bilinear form and change unknown
    a_V = new BilinearForm(*a_W); a_V->setUnknowns(*u_V, *v_W);
    a_W = new BilinearForm(*a_W); a_W->setUnknowns(*u_W, *v_W);
  }
  else // a_W==nullptr, use predefined projector
  {
    if (scalar) //scalar spaces
    {
      switch (projectorType)
      {
        case _L2Projector: a_W = new BilinearForm(intg(*domain_, *u_W * *v_W));
                           a_V = new BilinearForm(intg(*domain_, *u_V * *v_W));
                           break;
        case _H1Projector: a_W = new BilinearForm(intg(*domain_, *u_W * *v_W) + intg(*domain_, grad(*u_W) | grad(*v_W)));
                           a_V = new BilinearForm(intg(*domain_, *u_V * *v_W) + intg(*domain_, grad(*u_V) | grad(*v_W)));
                           break;
        case _H10Projector: a_W = new BilinearForm(intg(*domain_, grad(*u_W) | grad(*v_W)));
                            a_V = new BilinearForm(intg(*domain_, grad(*u_V) | grad(*v_W)));
                            break;
        default: error("projector_not_handled",words("projector",projectorType));
      }
    }
    else //vector spaces
    {
      switch(projectorType)
      {
        case _L2Projector: a_W = new BilinearForm(intg(*domain_, *u_W | *v_W));
                           a_V = new BilinearForm(intg(*domain_, *u_V | *v_W));
                           break;
        case _H1Projector: a_W = new BilinearForm(intg(*domain_, *u_W | *v_W) + intg(*domain_, grad(*u_W) % grad(*v_W)));
                           a_V = new BilinearForm(intg(*domain_, *u_V | *v_W) + intg(*domain_, grad(*u_V) % grad(*v_W)));
                           break;
        case _H10Projector: a_W = new BilinearForm(intg(*domain_, grad(*u_W) % grad(*v_W)));
                            a_V = new BilinearForm(intg(*domain_, grad(*u_V) % grad(*v_W)));
                            break;
        default: error("projector_not_handled",words("projector",projectorType));
      }
    }
  }
  //create TermMatrix's
  number_t vb=theVerboseLevel;
  verboseLevel(0);
  A_ = new TermMatrix(*a_W, _name="A_W");
  B_ = new TermMatrix(*a_V, _name="A_V");
  factorize(*A_);
  theProjectors.push_back(this);
  verboseLevel(vb);
  trace_p->pop();
}

//! projection of a TermVector, () operator
//  X: TermVector
//  u: unknown of result
TermVector Projector::operator()(TermVector& X, const Unknown& u) const
{
  TermVector PX;
  (*this)(X,PX);
  PX.setUnknown(u);
  return PX;
}

TermVector Projector::operator()(const TermVector& X) const
{
  const Unknown* v=u_W;
  const Space* spv=v->space();
  //find the result unknown
  std::vector<Unknown*>::const_iterator itu=Unknown::theUnknowns.begin();
  bool cont=true;
  for(; itu!=Unknown::theUnknowns.end() && cont; ++itu)
    {
      if((*itu)->space()==spv)
        {
          v=*itu;
          cont=false;
        }
    }
  //do projection
  return (*this)(const_cast<TermVector&>(X),*v);
}

TermVector& Projector::operator()(TermVector& X, TermVector& PX) const
{
  //check unknown consitancy and change temporary the col unknown of B
  if (!X.isSingleUnknown())
  {
    where("Projector::operator()(TermVector, TermVector)");
    error("term_not_suterm",X.name());
  }
  const Unknown* u_X = X.unknown();
  if (u_V->space()!=u_X->space() && u_V->space()!=X.subVector().spacep())
  {
    where("Projector::operator()(TermVector, TermVector)");
    error("term_mismatch_spaces", u_V->space()->name(), u_X->space()->name());
  }

  const Unknown* u_PX = PX.unknown();

  if(invA_B!=nullptr) //use direct product
    {
      X.setUnknown(*invA_B->suTerms().begin()->second->up());
      PX = *invA_B*X;
    }
  else
    {
      X.setUnknown(*u_V);
      if (A_!=nullptr && B_!=nullptr) PX=factSolve(*A_,*B_*X);
      else
      {
        where("Projector::operator()(TermVector, TermVector)");
        error("null_pointer", "A_ || B_");
      }
    }
  X.setUnknown(*u_X);
  if(u_PX!=nullptr) PX.setUnknown(*u_PX);
  if(X.name()!="") PX.name(name+" * "+X.name());
  else PX.name(name+" * ?");
  return PX;
}

//! projection of a TermVector using *
TermVector operator*(const Projector& P, const TermVector& X)
{
  return P(X);
}

/*! return projector as TermMatrix: inv(A)*B
    update TermMatrix member invA_B and delete A_ and B_
*/
TermMatrix& Projector::asTermMatrix(const Unknown& u, const Unknown& v, const string_t& name)
{
  if (u.space()!=u_V->space())
  {
    where("Projector::asTermMatrix(Unknown, Unknown, String)");
    error("unknowns_mismatch_spaces", u.space()->name(), u_V->space()->name());
  }
  if (v.space()!=u_W->space())
  {
    where("Projector::asTermMatrix(Unknown, Unknown, String)");
    error("unknowns_mismatch_spaces", v.space()->name(), u_W->space()->name());
  }
  trace_p->push("Projector::asTermMatrix");
  if (invA_B==nullptr) //compute inverse in col dense storage
  {

    MatrixEntry* matA, * matB;
    number_t nbr=u_W->nbOfComponents(), nbc=u_V->nbOfComponents();
    if (nbr>1 || nbc>1) //go to scalar representation
    {
      A_->toScalar(false);
      B_->toScalar(false);
      matA=A_->begin()->second->scalar_entries();
      matB=B_->begin()->second->scalar_entries();
    }
    else
    {
      matA=A_->begin()->second->entries();
      matB=B_->begin()->second->entries();
    }
    if (matA==nullptr || matB==nullptr)
    {
      where("Projector::asTermMatrix(Unknown, Unknown, String)");
      error("term_no_entries");
    }

    number_t m=matA->nbOfCols(), n=matB->nbOfCols();
    ValueType vt=matA->valueType();
    if (vt==_real) vt=matB->valueType();

    MatrixStorage* sto= new ColDenseStorage(m, n);
    MatrixEntry* mat=new MatrixEntry(vt,_scalar, sto);
    VectorEntry rhs=VectorEntry(_real,_scalar, n);
    if (vt==_real)
    {
      std::vector<real_t>::iterator itm = mat->rEntries_p->values().begin()+1, itx;
      for (number_t k=1; k<=n; k++)
      {
        rhs.setEntry(k,1.);
        VectorEntry X=factSolve(*matA,*matB*rhs);
        itx = X.rEntries_p->begin();
        for(; itx!=X.rEntries_p->end(); ++itx, ++itm) *itm = *itx;
        rhs.setEntry(k,0.);
      }
    }
    else
    {
      std::vector<complex_t>::iterator itm = mat->cEntries_p->values().begin()+1, itx;
      for (number_t k=1; k<=n; k++)
      {
        rhs.setEntry(k,1.);
        VectorEntry X=factSolve(*matA,*matB*rhs);
        itx = X.cEntries_p->begin();
        for(; itx!=X.cEntries_p->end(); ++itx, ++itm) *itm = *itx;
        rhs.setEntry(k,0.);
      }
    }
    if (nbr>1 || nbc>1) //go back to vector representation
    {
      number_t mm=m/nbr, nn=n/nbc;
      MatrixStorage* sto_m= new ColDenseStorage(mm, nn);
      MatrixEntry* mat_m;
      if (vt==_real) //real matrix
      {
        mat_m = new MatrixEntry(_real,_matrix, sto_m, dimPair(nbr,nbc));
        Matrix<real_t> smat(nbr,nbc);
        typedef std::vector<real_t>::iterator itvr;
        std::vector<itvr> itmat(nbc);
        std::vector<Matrix<real_t> >::iterator itm =mat_m->rmEntries_p->values().begin()+1;
        for (number_t c=0; c<nn; c++)
        {
          for (number_t cc=0; cc<nbc; cc++) itmat[cc] = mat->rEntries_p->values().begin()+(1+(c*nbc+cc)*m);
          for (number_t r=0; r<mm; r++, itm++)
          {
            for (number_t rr=1; rr<=nbr; rr++)
              for (number_t cc=1; cc<=nbc; cc++) smat(rr,cc) = *itmat[cc-1]++;
            *itm=smat;
          }
        }
      }
      else //complex matrix
      {
        mat_m=new MatrixEntry(_complex,_matrix, sto_m, dimPair(nbr,nbc));
        Matrix<complex_t> smat(nbr,nbc);
        typedef std::vector<complex_t>::iterator itvr;
        std::vector<itvr> itmat(nbc);
        std::vector<Matrix<complex_t> >::iterator itm =mat_m->cmEntries_p->values().begin()+1;
        for (number_t c=0; c<nn; c++)
        {
          for (number_t cc=0; cc<nbc; cc++) itmat[cc] = mat->cEntries_p->values().begin()+(1+(c*nbc+cc)*m);
          for (number_t r=0; r<mm; r++, itm++)
          {
            for(number_t rr=1; rr<=nbr; rr++)
              for(number_t cc=1; cc<=nbc; cc++) smat(rr,cc) = *itmat[cc-1]++;
            *itm=smat;
          }
        }
      }
      delete mat;
      mat=mat_m;
    }
    //create TermMatrix result
    SuTermMatrix* sut=new SuTermMatrix(&u,B_->begin()->second->space_up(), &v, A_->begin()->second->space_up(), mat, name);
    //if(u.nbOfComponents()>1 || v.nbOfComponents()) sut->entries()=nullptr;
    invA_B=new TermMatrix(sut,name);
  }
  delete A_; A_=nullptr;
  delete B_; B_=nullptr;
  trace_p->pop();
  return *invA_B;
}

// print info on stream
void Projector::print(std::ostream& out) const
{
  if(theVerboseLevel==0) return;
  out<<"Projector "<<name;
  if(find (Space::theSpaces.begin(), Space::theSpaces.end(), V_)==Space::theSpaces.end()) return;  // space probably deallocated
  out<<": "<< V_->name();
  if(V_->domain()!=domain_) out<<"|"<<domain_->name();
  out<<" --> "<<W_->name();
  if(W_->domain()!=domain_) out<<"|"<<domain_->name();
  string_t sta=a_W->first().asString();
  replaceString(sta, a_W->first().up()->name(),"u");
  replaceString(sta, a_W->first().vp()->name(),"v");
  out<<" "<<words("Projector", projectorType)<<", bilinear form: "<<sta<<eol;
  if(theVerboseLevel<2) return;
  if(A_!=nullptr) out<<*A_;
  if(B_!=nullptr) out<<*B_;
  if(invA_B!=nullptr) out<<*invA_B;
}

// output on stream
std::ostream& operator<<(std::ostream& out, const Projector& P)
{
  P.print(out);
  return out;
}

//find a projector
Projector& findProjector(Space& V, dimen_t nbcV, Space& W, dimen_t nbcW, ProjectorType pt=_L2Projector)
{
   trace_p->push("findProjector(...)");
  //check number of components
  dimen_t nV=nbcV*V.dimFun(), nW=nbcW*W.dimFun();
  if(nV!=nW)
  {
    warning("free_warning"," may be inconsistent vector dimensions,  nV="+tostring(nV)+", nW="+tostring(nW));
    // nV may be different from nW but correct when local transformation change the dimension of shape functions
    // for instance dimFun_phys =dimFun+1 when dealing with 2D Nedelec in a plane of R3
    // to improve in future by computing dimFun_phys
  }
  //search for existing projector, if not found, create a new one
  std::vector<Projector*>::const_iterator itp=Projector::theProjectors.begin();
  Projector * p=nullptr;
  for(; itp!=Projector::theProjectors.end() && p==nullptr; ++itp)
    {
      Projector *pi=*itp;
      if(pi->V_==&V && pi->W_==&W && pi->projectorType==pt &&
          pi->u_V->nbOfComponents()==nbcV && pi->u_W->nbOfComponents()==nbcW) p=pi;
    }
  if(p==nullptr) p=new Projector(V,nbcV,W,nbcW,pt);
  trace_p->pop();
  return *p;
}

// delete all Projector objects
void Projector::clearGlobalVector()
{
  while(Projector::theProjectors.size() > 0)
    { delete Projector::theProjectors[0];}
}

// clear all projectors depending on a space
void clearProjectors(const Space& sp)
{
  if(Projector::theProjectors.empty()) return;
  auto itp=Projector::theProjectors.begin();
  while(itp!=Projector::theProjectors.end())
  {
    Projector *pi=*itp;
    if(pi->V()==&sp || pi->W()==&sp)
    {
       delete pi;    // remove also from theProjectors
       itp=Projector::theProjectors.begin();
    }
    else itp++;
  }
}

// print the list of projectors in memory
void Projector::printAllProjectors(std::ostream& out)
{
    number_t vb=theVerboseLevel;
    verboseLevel(1);
    out<<"Projectors in memory: "<<eol;
    std::vector<Projector*>::iterator it=theProjectors.begin();
    for(; it!=theProjectors.end(); ++it) out<<"  "<<(*it)<<": "<<(**it)<<eol;
    verboseLevel(vb);
}

//compute projection of a TermVector X on space W, creating projector if it does not exist
TermVector projection(const TermVector& X, Space& W, ProjectorType pt, KeepStatus keep)
{
  if (!X.isSingleUnknown())
  {
    where("projection(TermVector, Space, ProjectorType, KeepStatus)");
    error("term_not_suterm", X.name());
  }
  Space* spX = const_cast<Space*>(X.subVector().spacep());
  Projector& pr=findProjector(*spX, X.unknown()->nbOfComponents(),W,1,pt);
  if (keep==_keep) return pr*X;
  TermVector PX=pr*X;
  Projector::theProjectors.pop_back(); delete &pr;
  return PX;
}

//compute projection of a TermVector X on space W, creating projector if it does not exist and renaming TermVector unknown
TermVector projection(const TermVector& X, Space& W, const Unknown& u, ProjectorType pt, KeepStatus keep)
{
    TermVector proj = projection(X, W, pt, keep);
    proj.changeUnknown(u);
    return proj;
}

//compute projection of a TermVector X on space W^n, creating projector if it does not exist
TermVector projection(const TermVector& X, Space& W, dimen_t nbcW, ProjectorType pt, KeepStatus keep)
{
  if (!X.isSingleUnknown())
  {
    where("projection(TermVector, Space, Dimen, ProjectorType, KeepStatus)");
    error("term_not_suterm", X.name());
  }
  Space* spX = const_cast<Space*>(X.subVector().spacep());
  dimen_t nbcX = X.subVector().up()->nbOfComponents();
  Projector& pr=findProjector(*spX,nbcX,W,nbcW,pt);
  if (keep==_keep) return pr*X;
  TermVector PX=pr*X;
  Projector::theProjectors.pop_back(); delete &pr;
  return PX;
}

//compute projection of a TermVector X on space W^n, creating projector if it does not exist and renaming TermVector unknown
TermVector projection(const TermVector& X, Space& W, dimen_t nbcW, const Unknown& u, ProjectorType pt, KeepStatus keep)
{
    TermVector proj = projection(X, W, nbcW, pt, keep);
    proj.changeUnknown(u);
    return proj;
}

} //end of namespace xlifepp
