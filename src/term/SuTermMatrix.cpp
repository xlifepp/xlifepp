/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SuTermMatrix.cpp
  \author E. Lunéville
  \since 03 apr 2012
  \date 13 jan 2014

  \brief Implementation of xlifepp::SuTermMatrix class members and related utilities
*/

#include "SuTermMatrix.hpp"
#include "utils.h"
#include "computation/termUtils.hpp"


namespace xlifepp
{

//===============================================================================
//member functions of SuTermMatrix class
//===============================================================================

void SuTermMatrix::initPointers()
{
  entries_p=nullptr;
  scalar_entries_p = nullptr;
  rhs_matrix_p = nullptr;
  hm_entries_p=nullptr;
  hm_scalar_entries_p=nullptr;
  cluster_u =nullptr;
  cluster_v=nullptr;
}

// basic constructor from MatrixEntry
SuTermMatrix::SuTermMatrix(const Unknown* u, Space* spu, const Unknown* v, Space* spv, MatrixEntry* me, const string_t& na)
  :subf_p(nullptr), u_p(u), v_p(v), space_u_p(spu), space_v_p(spv)
{
  termType_ = _sutermMatrix;
  name_=na;
  initPointers();
  entries_p =me;
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
}

// basic constructor from a bilinear form
SuTermMatrix::SuTermMatrix(SuBilinearForm* subf, const string_t& na, bool noass)
{
  termType_ = _sutermMatrix;
  subf_p = subf;
  u_p=nullptr;
  v_p=nullptr;
  if(subf_p!=nullptr)
    {
      u_p = subf_p->up();
      v_p = subf_p->vp();
    }
  name_ = na;
  computingInfo_.noAssembly = noass;
  initPointers();
  if(subf_p != nullptr)
    {
      if(subf_p->hasJump()) subf_p = new SuBilinearForm(xlifepp::split(subf_p->bfs()));  //split bilinear form with jump or mean (should be removed ???)
      buildSubspaces();   //construct the required subspaces
    }
  //WARNING: at this step, entries_p is not allocated !
}

SuTermMatrix::SuTermMatrix(SuBilinearForm* subf, const string_t& na, ComputingInfo cp)
{
  termType_ = _sutermMatrix;
  subf_p = subf;
  u_p=nullptr;
  v_p=nullptr;
  if(subf_p!=nullptr)
    {
      u_p = subf_p->up();
      v_p = subf_p->vp();
    }
  name_ = na;
  computingInfo_ = cp;
  initPointers();
  if(subf_p != nullptr)
    {
      if(subf_p->hasJump()) subf_p = new SuBilinearForm(xlifepp::split(subf_p->bfs()));  //split bilinear form with jump or mean (should be removed ???)
      buildSubspaces();   //construct the required subspaces
    }
  //WARNING: at this step, entries_p is not allocated !
}

// full constructor
SuTermMatrix::SuTermMatrix(SuBilinearForm* subf, const Unknown* u, const Unknown* v, Space* spu, Space* spv,
                           const std::vector<Space*>& sspu, const std::vector<Space*>& sspv,
                           const string_t& na, MatrixEntry* me)
{
  termType_ = _sutermMatrix;
  name_ = na;
  subf_p = subf;
  u_p = u;
  v_p = v;
  space_u_p=spu;
  space_v_p=spv;
  subspaces_u=sspu;
  subspaces_v=sspv;
  initPointers();
  entries_p = me;  //shared pointer !!!
}

/*! constructor of matrix opker(xi,yj) - Lagrange interpolation -
    opker is evaluated on dof coordinates linked to Unknown and GeomDomains
     ux, domx is associated to matrix rows and  uy, domy to matrix columns
     NOTE: do not use this constructor if kernel is singular on some couple of points
*/
SuTermMatrix::SuTermMatrix(const Unknown& ux, const GeomDomain& domx,
                           const Unknown& uy, const GeomDomain& domy, const OperatorOnKernel& opk, const string_t& na)
{
  trace_p->push("SuTermMatrix::SuTermMatrix(Unknown, Domain, Unknown, Domain, OperatorOnKernel, ...)");
  if(theVerboseLevel>0) std::cout<<"computing SuTermMatrix "<< opk.asString()<<"("<<domx.name()<<", "<<domy.name()
                                   <<"), using "<<numberOfThreads()<<" threads: "<<std::flush;
  //initialization
  termType_ = _sutermMatrix;
  name_ = na;
  subf_p = nullptr;
  u_p = &uy;
  v_p = &ux;
  initPointers();

  //retry subspaces
  space_v_p=Space::findSubSpace(&domx,ux.space());
  if(space_v_p==nullptr) space_v_p= new Space(domx,*ux.space());
  space_u_p=Space::findSubSpace(&domy,uy.space());
  if(space_u_p==nullptr) space_u_p= new Space(domy,*uy.space());
  if(!space_u_p->isFE())
    {
      where("SuTermMatrix::SuTermMatrix(Unknown&,GeomDomain&,Unknown&,GeomDomain&,OperatorOnKernel&,String&)");
      error("not_fe_space_type",space_u_p->name());
    }
  if(!space_v_p->isFE())
    {
      where("SuTermMatrix::SuTermMatrix(Unknown&,GeomDomain&,Unknown&,GeomDomain&,OperatorOnKernel&,String&)");
      error("not_fe_space_type",space_v_p->name());
    }

  //allocate matrix entries (row dense storage)
  std::stringstream ss;
  ss << space_u_p << "-" << space_v_p;
  MatrixStorage* sto = createMatrixStorage(_dense, _row, _feBuild, space_v_p->dimSpace(), space_u_p->dimSpace(),
                       std::vector<std::vector<number_t> >(), ss.str());
  ValueType vt=opk.valueType();
  StrucType st=opk.strucType();
  entries_p= new MatrixEntry(vt, st, sto, opk.dims(), _noSymmetry);

  //load points xs and normal nxs if required
  std::vector<Point> xs(space_v_p->nbDofs());
  std::vector<Point>::iterator itp=xs.begin();
  for(number_t i=1; i<=space_v_p->nbDofs(); i++, itp++) *itp=space_v_p->dof(i).coords();
  std::vector<Vector<real_t> >* nxs=nullptr;
  number_t vb=theVerboseLevel;
  if(opk.xnormalRequired())
    {
      verboseLevel(0); //silent mode
      nxs = new std::vector<Vector<real_t> >();
      interpolatedNormals(*space_v_p,*nxs);
    }

  //load points ys and normal nys if required
  std::vector<Point> ys(space_u_p->nbDofs());
  itp=ys.begin();
  for(number_t i=1; i<=space_u_p->nbDofs(); i++, itp++) *itp=space_u_p->dof(i).coords();
  std::vector<Vector<real_t> >* nys=nullptr;
  if(opk.ynormalRequired())
    {
      verboseLevel(0); //silent mode
      nys = new std::vector<Vector<real_t> >();
      interpolatedNormals(*space_u_p,*nys);
    }
  verboseLevel(vb); //restaure verbose level

  //print status
  number_t nbx=xs.size(), nxdiv10 = nbx/10, nby=ys.size();
  bool show_status = (theVerboseLevel > 0 && nbx > 100 &&  nxdiv10 > 1);

  //evaluate matrix opk(xi,yj)
  dimPair dims;
  std::vector<Point>::const_iterator ity;
  Vector<real_t>* nx=nullptr, *ny=nullptr;
  std::vector<Vector<real_t> >::iterator itny;
  switch(st)
    {
      case _scalar:
        if(vt==_real)  // real scalar
          {
            std::vector<real_t>::iterator itkb=entries_p->rEntries_p->values().begin()+1, itk;
            Vector<real_t> vopk;
            #ifdef XLIFEPP_WITH_OMP
            #pragma omp parallel for firstprivate(vopk, itk, ity, itny, ny, nx)
            #endif // XLIFEPP_WITH_OMP
            for(number_t i=0; i<nbx; i++)
              {
                if(nxs!=nullptr) {nx=&(*nxs)[i];setNx(nx);}
                if(nys!=nullptr) itny=nys->begin();
                itk = itkb + i*nby;
                for(ity=ys.begin(); ity!=ys.end(); ++ity, ++itk)
                  {
                    if(nys!=nullptr) {ny=&*itny; ++itny;setNy(ny);}
                    opk.eval(xs[i],*ity,vopk,nx,ny);
                    *itk=vopk[0];
                  }
                if(show_status && currentThread()==0 && i!=0 && i % nxdiv10 == 0) { std::cout<< i/nxdiv10 <<"0% "<<std::flush; }
              }
          }
        else //complex scalar
          {
            std::vector<complex_t>::iterator itkb=entries_p->cEntries_p->values().begin()+1, itk;
            Vector<complex_t> vopk;
            #ifdef XLIFEPP_WITH_OMP
            #pragma omp parallel for firstprivate(vopk, itk, ity, itny, ny, nx)
            #endif // XLIFEPP_WITH_OMP
            for(number_t i=0; i<nbx; i++)
              {
                if(nxs!=nullptr) {nx=&(*nxs)[i];setNx(nx);}
                if(nys!=nullptr) itny=nys->begin();
                itk = itkb + i*nby;
                for(ity=ys.begin(); ity!=ys.end(); ++ity, ++itk)
                  {
                    if(nys!=nullptr) {ny=&*itny; ++itny;setNy(ny);}
                    opk.eval(xs[i],*ity,vopk,nx,ny);
                    *itk=vopk[0];
                  }
                if(show_status && currentThread()==0 && i!=0 && i % nxdiv10 == 0) { std::cout<< i/nxdiv10 <<"0% "<<std::flush; }
              }
          }
        break;
      case _matrix:
        if(vt==_real)  // real matrix
          {
            std::vector<Matrix<real_t> >::iterator itkb=entries_p->rmEntries_p->values().begin()+1, itk;
            Vector<real_t> vopk;
            #ifdef XLIFEPP_WITH_OMP
            #pragma omp parallel for firstprivate(vopk, itk, ity, itny, ny, nx)
            #endif // XLIFEPP_WITH_OMP
            for(number_t i=0; i<nbx; i++)
              {
                if(nxs!=nullptr) {nx=&(*nxs)[i];setNx(nx);}
                if(nys!=nullptr) itny=nys->begin();
                itk = itkb + i*nby;
                for(ity=ys.begin(); ity!=ys.end(); ++ity, ++itk)
                  {
                    if(nys!=nullptr) {ny=&*itny; ++itny;setNy(ny);}
                    opk.eval(xs[i],*ity,vopk,nx,ny,&dims);
                    *itk = Matrix<real_t>(dims.first, dims.second, vopk.begin(), vopk.end()); //! move vector to matrix;
                  }
                if(show_status && currentThread()==0 && i!=0 && i % nxdiv10 == 0) { std::cout<< i/nxdiv10 <<"0% "<<std::flush; }
              }
          }
        else //complex scalar
          {
            std::vector<Matrix<complex_t> >::iterator itkb=entries_p->cmEntries_p->values().begin()+1, itk;
            Vector<complex_t> vopk;
            #ifdef XLIFEPP_WITH_OMP
            #pragma omp parallel for firstprivate(vopk, itk, ity, itny, ny, nx)
            #endif // XLIFEPP_WITH_OMP
            for(number_t i=0; i<nbx; i++)
              {
                if(nxs!=nullptr) {nx=&(*nxs)[i];setNx(nx);}
                if(nys!=nullptr) itny=nys->begin();
                itk = itkb + i*nby;
                for(ity=ys.begin(); ity!=ys.end(); ++ity, ++itk)
                  {
                    if(nys!=nullptr) {ny=&*itny; ++itny;setNy(ny);}
                    opk.eval(xs[i],*ity,vopk,nx,ny,&dims);
                    *itk = Matrix<complex_t>(dims.first, dims.second, vopk.begin(), vopk.end()); //! move vector to matrix;
                  }
                if(show_status && currentThread()==0 && i!=0 && i % nxdiv10 == 0) { std::cout<< i/nxdiv10 <<"0% "<<std::flush; }
              }
          }
        break;
      default:
        where("SuTermMatrix::SuTermMatrix(Unknown&,GeomDomain&,Unknown&,GeomDomain&,OperatorOnKernel&,String&)");
        error("structure_not_handled", words("structure",st));
    }
  computed()=true;
  if(theVerboseLevel > 0) std::cout<<" done"<<eol<<std::flush;
  if(nxs!=nullptr) delete nxs;
  if(nys!=nullptr) delete nys;
  trace_p->pop();
}


// copy constructor
SuTermMatrix::SuTermMatrix(const SuTermMatrix& mat, const string_t& na)
{
  copy(mat);
  if(na != "") name_ = na;
}

/*! constructor of a diagonal SuTermMatrix from a SuTermVector
    the diagonal Matrix is stored using SymCsStorage
    Note: SuTermVector cannot be passed as const because of a cast of the space pointer
*/

SuTermMatrix::SuTermMatrix(SuTermVector& sut, const string_t& na)
{
  const Unknown* u=sut.up();
  Space* spu=sut.spacep();
  diagFromSuTermVector(u, spu, u, spu, sut,_cs, _sym, na);
}

SuTermMatrix::SuTermMatrix(SuTermVector& sut1, SuTermVector& sut2, const SymbolicFunction& sf, const string_t& na)
{
  const Unknown* u=sut1.up();
  Space* spu=sut1.spacep();

  buildFromSuTermVectorPair(u, spu, u, spu, sut1, sut2, sf, _cs, _dual, na);
}

SuTermMatrix::SuTermMatrix(const Unknown* u, Space* spu, const Unknown* v, Space* spv, SuTermVector& sut,
                           StorageType st, AccessType at,const string_t& na)
{
  diagFromSuTermVector(u, spu, v, spv, sut, st, at, na);
}

void SuTermMatrix::diagFromSuTermVector(const Unknown* u, Space* spu, const Unknown* v, Space* spv, SuTermVector& sut,
                                        StorageType st, AccessType at, const string_t& na)
{
  trace_p->push("SuTermMatrix::diagFromSuTermVector");
  computingInfo_.noAssembly = false;
  termType_ = _sutermMatrix;
  name_ = na;
  subf_p=nullptr;
  initPointers();
  u_p = u;
  v_p = v;
  space_u_p=spu;
  space_v_p=spv;
  std::stringstream ss;
  ss << space_u_p << "-" << space_v_p;
  number_t dimu=space_u_p->dimSpace(), dimv=space_v_p->dimSpace();
  number_t dim=std::min(dimu,dimv);

  //create storage
  std::vector<std::vector<number_t> > indices(dim);
  std::vector<std::vector<number_t> >::iterator it=indices.begin();
  for(number_t i=0; i<dim; it++, i++) *it=std::vector<number_t>(1,i+1);
  MatrixStorage* mstorage = nullptr;
  StorageType sto=st;
  AccessType aty=at;
  if (sto==_noStorage)
  {
    sto=_cs;
    if(spu==spv) aty=_sym;
  }
  if (aty==_noAccess)
  {
    aty=_row;
    if(spu==spv) aty=_sym;
  }
  mstorage = createMatrixStorage(sto, aty, _diagBuild, dimv, dimu, indices, ss.str());

  //create entry
  StrucType str = sut.strucType();
  number_t nbc = sut.nbOfComponents();
  ValueType vt = sut.valueType();
  dimPair duv = dimPair(1,1);
  if (nbc > 1)   //move to matrix structure
  {
    str=_matrix;
    duv=dimPair(nbc,nbc);
  }
  entries_p = new MatrixEntry(vt, str, mstorage, duv, _symmetric);

  std::vector<number_t> diagadrs(dim);
  std::vector<number_t>::iterator itd=diagadrs.begin();
  for(number_t k=1; k<=dim; k++, itd++) *itd=mstorage->pos(k,k);
  itd=diagadrs.begin();
  //copy of TermVector
  switch (str)
  {
    case _scalar:
      switch(vt)
      {
        case _real:
        {
          Vector<real_t>& vec = *sut.entries()->rEntries_p;
          std::vector<real_t>& mat = entries_p->rEntries_p->values();
          Vector<real_t>::iterator itv=vec.begin();
          std::vector<real_t>::iterator itm=mat.begin();
          for(number_t k=1; k<=dim; k++, itv++, itd++) *(itm + *itd) = *itv;
          break;
        }
        default:
        {
          Vector<complex_t>& vec = *sut.entries()->cEntries_p;
          std::vector<complex_t>& mat = entries_p->cEntries_p->values();
          Vector<complex_t>::iterator itv=vec.begin();
          std::vector<complex_t>::iterator itm=mat.begin();
          for(number_t k=1; k<=dim; k++, itv++, itd++) *(itm + *itd) = *itv;
        }
      }
      break;
    case _matrix:
      switch(vt)
      {
        case _real:
        {
          Vector<Vector<real_t> >& vec = *sut.entries()->rvEntries_p;
          std::vector<Matrix<real_t> >& mat = entries_p->rmEntries_p->values();
          Vector<Vector<real_t> >::iterator itv=vec.begin();
          std::vector<Matrix<real_t> >::iterator itm=mat.begin();
          for(number_t k=1; k<=dim; k++, itv++, itd++) *(itm + *itd) = Matrix<real_t>(*itv);
          break;
        }
        default:
        {
          Vector<Vector<complex_t> >& vec = *sut.entries()->cvEntries_p;
          std::vector<Matrix<complex_t> >& mat = entries_p->cmEntries_p->values();
          Vector<Vector<complex_t> >::iterator itv=vec.begin();
          std::vector<Matrix<complex_t> >::iterator itm=mat.begin();
          for(number_t k=1; k<=dim; k++, itv++, itd++) *(itm + *itd) = Matrix<complex_t>(*itv);
        }
      }
      break;
    default:
      error("structure_not_handled",words("structure",str));
  }
  computingInfo_.isComputed=true;
  trace_p->pop();
}

void SuTermMatrix::buildFromSuTermVectorPair(const Unknown* u, Space* spu, const Unknown* v, Space* spv,
                                             SuTermVector& sut1, SuTermVector& sut2, const SymbolicFunction& sf,
                                             StorageType st, AccessType at, const string_t& na)
{
  trace_p->push("SuTermMatrix::buildFromSuTermVectorPair");
  computingInfo_.noAssembly = false;
  termType_ = _sutermMatrix;
  name_ = na;
  subf_p=nullptr;
  initPointers();
  u_p = u;
  v_p = v;
  space_u_p=spu;
  space_v_p=spv;
  std::stringstream ss;
  ss << space_u_p << "-" << space_v_p;
  number_t dimu=space_u_p->dimSpace(), dimv=space_v_p->dimSpace();
  // number_t dim=std::min(dimu,dimv);

  //create storage
  MatrixStorage* mstorage = new DualDenseStorage(dimv, dimu, ss.str());

  //create entry
  StrucType str1 = sut1.strucType(), str2=sut2.strucType(), str=_scalar;
  number_t nbc1 = sut1.nbOfComponents(), nbc2 = sut2.nbOfComponents();
  ValueType vt1 = sut1.valueType(), vt2 = sut2.valueType(), vt=_real;
  dimPair duv = dimPair(nbc1,nbc2);
  SymType symtype=_noSymmetry;

  if (nbc1 > 1 || nbc2 > 1) { str=_matrix; } //move to matrix structure

  if (vt1 == _complex || vt2 == _complex) { vt=_complex; }

  if (u==v) { symtype=_symmetric; }

  entries_p = new MatrixEntry(vt, str, mstorage, duv, symtype);

  //copy of TermVector
  switch(str)
  {
    case _scalar: // both SuTermVectors are scalar
    {
      switch(vt)
      {
        case _real: // both SuTermVectors are real_
        {
          Vector<real_t>& vec1 = *sut1.entries()->rEntries_p;
          Vector<real_t>& vec2 = *sut2.entries()->rEntries_p;
          std::vector<real_t>& mat = entries_p->rEntries_p->values();
          Vector<real_t>::iterator itv1=vec1.begin();
          Vector<real_t>::iterator itv2=vec2.begin();
          std::vector<real_t>::iterator itm=mat.begin();
          for (Vector<real_t>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
          {
            for (Vector<real_t>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
            {
              *itm = sf(*itv1, *itv2);
            }
          }
          break;
        }
        default:
        {
          if (vt1 ==_real) // vt2 is necessarily _complex
          {
            Vector<real_t>& vec1 = *sut1.entries()->rEntries_p;
            Vector<complex_t>& vec2 = *sut2.entries()->cEntries_p;
            std::vector<complex_t>& mat = entries_p->cEntries_p->values();
            Vector<real_t>::iterator itv1=vec1.begin();
            Vector<complex_t>::iterator itv2=vec2.begin();
            std::vector<complex_t>::iterator itm=mat.begin();
            for (Vector<real_t>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
            {
              for (Vector<complex_t>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
              {
                *itm = sf(*itv1, *itv2);
              }
            }
          }
          else if (vt2==_real) // vt1 is _complex
          {
            Vector<complex_t>& vec1 = *sut1.entries()->cEntries_p;
            Vector<real_t>& vec2 = *sut2.entries()->rEntries_p;
            std::vector<complex_t>& mat = entries_p->cEntries_p->values();
            Vector<complex_t>::iterator itv1=vec1.begin();
            Vector<real_t>::iterator itv2=vec2.begin();
            std::vector<complex_t>::iterator itm=mat.begin();
            for (Vector<complex_t>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
            {
              for (Vector<real_t>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
              {
                *itm = sf(*itv1, *itv2);
              }
            }
          }
          else // both vt1 and vt2 are _complex
          {
            Vector<complex_t>& vec1 = *sut1.entries()->cEntries_p;
            Vector<complex_t>& vec2 = *sut2.entries()->cEntries_p;
            std::vector<complex_t>& mat = entries_p->cEntries_p->values();
            Vector<complex_t>::iterator itv1=vec1.begin();
            Vector<complex_t>::iterator itv2=vec2.begin();
            std::vector<complex_t>::iterator itm=mat.begin();
            for (Vector<complex_t>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
            {
              for (Vector<complex_t>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
              {
                *itm = sf(*itv1, *itv2);
              }
            }
          }
        } // end of case complex
      } // end of switch vt
      break;
    } // end of case scalar
    default: // str1 or str2 can still be _scalar
    {
      if (str1 ==_scalar) // str2 is necessarily _vector
      {
        switch (vt)
        {
          case _real: // both vt1 and vt2 are _real
          {
            Vector<real_t>& vec1 = *sut1.entries()->rEntries_p;
            Vector<Vector<real_t>>& vec2 = *sut2.entries()->rvEntries_p;
            std::vector<Matrix<real_t>>& mat = entries_p->rmEntries_p->values();
            Vector<real_t>::iterator itv1=vec1.begin();
            Vector<Vector<real_t>>::iterator itv2=vec2.begin();
            std::vector<Matrix<real_t>>::iterator itm=mat.begin();
            for (Vector<real_t>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
            {
              for (Vector<Vector<real_t>>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
              {
                Matrix<real_t>::iterator itmv=(*itm).begin();
                for (Vector<real_t>::iterator itv2v=(*itv2).begin(); itv2v!=(*itv2).end(); ++itv2v, ++itmv)
                {
                  *itmv = sf(*itv1, *itv2v);
                }
              }
            }
            break;
          }
          default:
          {
            if (vt1 ==_real) // vt2 is necessarily _complex
            {
              Vector<real_t>& vec1 = *sut1.entries()->rEntries_p;
              Vector<Vector<complex_t>>& vec2 = *sut2.entries()->cvEntries_p;
              std::vector<Matrix<complex_t>>& mat = entries_p->cmEntries_p->values();
              Vector<real_t>::iterator itv1=vec1.begin();
              Vector<Vector<complex_t>>::iterator itv2=vec2.begin();
              std::vector<Matrix<complex_t>>::iterator itm=mat.begin();
              for (Vector<real_t>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
              {
                for (Vector<Vector<complex_t>>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
                {
                  Matrix<complex_t>::iterator itmv=(*itm).begin();
                  for (Vector<complex_t>::iterator itv2v=(*itv2).begin(); itv2v!=(*itv2).end(); ++itv2v, ++itmv)
                  {
                    *itmv = sf(*itv1, *itv2v);
                  }
                }
              }
              break;
            }
            else if (vt2 ==_real) // vt1 is _complex
            {
              Vector<complex_t>& vec1 = *sut1.entries()->cEntries_p;
              Vector<Vector<real_t>>& vec2 = *sut2.entries()->rvEntries_p;
              std::vector<Matrix<complex_t>>& mat = entries_p->cmEntries_p->values();
              Vector<complex_t>::iterator itv1=vec1.begin();
              Vector<Vector<real_t>>::iterator itv2=vec2.begin();
              std::vector<Matrix<complex_t>>::iterator itm=mat.begin();
              for (Vector<complex_t>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
              {
                for (Vector<Vector<real_t>>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
                {
                  Matrix<complex_t>::iterator itmv=(*itm).begin();
                  for (Vector<real_t>::iterator itv2v=(*itv2).begin(); itv2v!=(*itv2).end(); ++itv2v, ++itmv)
                  {
                    *itmv = sf(*itv1, *itv2v);
                  }
                }
              }
              break;
            }
            else // both vt1 and vt2 are complex
            {
              Vector<complex_t>& vec1 = *sut1.entries()->cEntries_p;
              Vector<Vector<complex_t>>& vec2 = *sut2.entries()->cvEntries_p;
              std::vector<Matrix<complex_t>>& mat = entries_p->cmEntries_p->values();
              Vector<complex_t>::iterator itv1=vec1.begin();
              Vector<Vector<complex_t>>::iterator itv2=vec2.begin();
              std::vector<Matrix<complex_t>>::iterator itm=mat.begin();
              for (Vector<complex_t>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
              {
                for (Vector<Vector<complex_t>>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
                {
                  Matrix<complex_t>::iterator itmv=(*itm).begin();
                  for (Vector<complex_t>::iterator itv2v=(*itv2).begin(); itv2v!=(*itv2).end(); ++itv2v, ++itmv)
                  {
                    *itmv = sf(*itv1, *itv2v);
                  }
                }
              }
              break;
            }
          }
        }
      }
      else if (str2 ==_scalar) // str1 is _vector
      {
        switch (vt)
        {
          case _real: // both vt1 and vt2 are _real
          {
            Vector<Vector<real_t>>& vec1 = *sut1.entries()->rvEntries_p;
            Vector<real_t>& vec2 = *sut2.entries()->rEntries_p;
            std::vector<Matrix<real_t>>& mat = entries_p->rmEntries_p->values();
            Vector<Vector<real_t>>::iterator itv1=vec1.begin();
            Vector<real_t>::iterator itv2=vec2.begin();
            std::vector<Matrix<real_t>>::iterator itm=mat.begin();
            for (Vector<Vector<real_t>>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
            {
              for (Vector<real_t>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
              {
                Matrix<real_t>::iterator itmv=(*itm).begin();
                for (Vector<real_t>::iterator itv1v=(*itv1).begin(); itv1v!=(*itv1).end(); ++itv1v, ++itmv)
                {
                  *itmv = sf(*itv1v, *itv2);
                }
              }
            }
            break;
          }
          default:
          {
            if (vt1 ==_real) // vt2 is necessarily _complex
            {
              Vector<Vector<real_t>>& vec1 = *sut1.entries()->rvEntries_p;
              Vector<complex_t>& vec2 = *sut2.entries()->cEntries_p;
              std::vector<Matrix<complex_t>>& mat = entries_p->cmEntries_p->values();
              Vector<Vector<real_t>>::iterator itv1=vec1.begin();
              Vector<complex_t>::iterator itv2=vec2.begin();
              std::vector<Matrix<complex_t>>::iterator itm=mat.begin();
              for (Vector<Vector<real_t>>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
              {
                for (Vector<complex_t>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
                {
                  Matrix<complex_t>::iterator itmv=(*itm).begin();
                  for (Vector<real_t>::iterator itv1v=(*itv1).begin(); itv1v!=(*itv1).end(); ++itv1v, ++itmv)
                  {
                    *itmv = sf(*itv1v, *itv2);
                  }
                }
              }
              break;
            }
            else if (vt2 ==_real) // vt1 is _complex
            {
              Vector<Vector<complex_t>>& vec1 = *sut1.entries()->cvEntries_p;
              Vector<real_t>& vec2 = *sut2.entries()->rEntries_p;
              std::vector<Matrix<complex_t>>& mat = entries_p->cmEntries_p->values();
              Vector<Vector<complex_t>>::iterator itv1=vec1.begin();
              Vector<real_t>::iterator itv2=vec2.begin();
              std::vector<Matrix<complex_t>>::iterator itm=mat.begin();
              for (Vector<Vector<complex_t>>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
              {
                for (Vector<real_t>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
                {
                  Matrix<complex_t>::iterator itmv=(*itm).begin();
                  for (Vector<complex_t>::iterator itv1v=(*itv1).begin(); itv1v!=(*itv1).end(); ++itv1v, ++itmv)
                  {
                    *itmv = sf(*itv1v, *itv2);
                  }
                }
              }
              break;
            }
            else // both vt1 and vt2 are complex
            {
              Vector<Vector<complex_t>>& vec1 = *sut1.entries()->cvEntries_p;
              Vector<complex_t>& vec2 = *sut2.entries()->cEntries_p;
              std::vector<Matrix<complex_t>>& mat = entries_p->cmEntries_p->values();
              Vector<Vector<complex_t>>::iterator itv1=vec1.begin();
              Vector<complex_t>::iterator itv2=vec2.begin();
              std::vector<Matrix<complex_t>>::iterator itm=mat.begin();
              for (Vector<Vector<complex_t>>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
              {
                for (Vector<complex_t>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
                {
                  Matrix<complex_t>::iterator itmv=(*itm).begin();
                  for (Vector<complex_t>::iterator itv1v=(*itv1).begin(); itv1v!=(*itv1).end(); ++itv1v, ++itmv)
                  {
                    *itmv = sf(*itv1v, *itv2);
                  }
                }
              }
              break;
            }
          }
        }
      }
      else // both str1 and str2 are _vector
      {
        switch (vt)
        {
          case _real: // both vt1 and vt2 are _real
          {
            Vector<Vector<real_t>>& vec1 = *sut1.entries()->rvEntries_p;
            Vector<Vector<real_t>>& vec2 = *sut2.entries()->rvEntries_p;
            std::vector<Matrix<real_t>>& mat = entries_p->rmEntries_p->values();
            Vector<Vector<real_t>>::iterator itv1=vec1.begin();
            Vector<Vector<real_t>>::iterator itv2=vec2.begin();
            std::vector<Matrix<real_t>>::iterator itm=mat.begin();
            for (Vector<Vector<real_t>>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
            {
              for (Vector<Vector<real_t>>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
              {
                Matrix<real_t>::iterator itmv=(*itm).begin();
                for (Vector<real_t>::iterator itv1v=(*itv1).begin(); itv1v!=(*itv1).end(); ++itv1v, ++itmv)
                {
                  for (Vector<real_t>::iterator itv2v=(*itv2).begin(); itv2v!=(*itv2).end(); ++itv2v, ++itmv)
                  {
                    *itmv = sf(*itv1v, *itv2v);
                  }
                }
              }
            }
            break;
          }
          default:
          {
            if (vt1 ==_real) // vt2 is necessarily _complex
            {
              Vector<Vector<real_t>>& vec1 = *sut1.entries()->rvEntries_p;
              Vector<Vector<complex_t>>& vec2 = *sut2.entries()->cvEntries_p;
              std::vector<Matrix<complex_t>>& mat = entries_p->cmEntries_p->values();
              Vector<Vector<real_t>>::iterator itv1=vec1.begin();
              Vector<Vector<complex_t>>::iterator itv2=vec2.begin();
              std::vector<Matrix<complex_t>>::iterator itm=mat.begin();
              for (Vector<Vector<real_t>>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
              {
                for (Vector<Vector<complex_t>>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
                {
                  Matrix<complex_t>::iterator itmv=(*itm).begin();
                  for (Vector<real_t>::iterator itv1v=(*itv1).begin(); itv1v!=(*itv1).end(); ++itv1v, ++itmv)
                  {
                    for (Vector<complex_t>::iterator itv2v=(*itv2).begin(); itv2v!=(*itv2).end(); ++itv2v, ++itmv)
                    {
                      *itmv = sf(*itv1v, *itv2v);
                    }
                  }
                }
              }
              break;
            }
            else if (vt2 ==_real) // vt1 is _complex
            {
              Vector<Vector<complex_t>>& vec1 = *sut1.entries()->cvEntries_p;
              Vector<Vector<real_t>>& vec2 = *sut2.entries()->rvEntries_p;
              std::vector<Matrix<complex_t>>& mat = entries_p->cmEntries_p->values();
              Vector<Vector<complex_t>>::iterator itv1=vec1.begin();
              Vector<Vector<real_t>>::iterator itv2=vec2.begin();
              std::vector<Matrix<complex_t>>::iterator itm=mat.begin();
              for (Vector<Vector<complex_t>>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
              {
                for (Vector<Vector<real_t>>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
                {
                  Matrix<complex_t>::iterator itmv=(*itm).begin();
                  for (Vector<complex_t>::iterator itv1v=(*itv1).begin(); itv1v!=(*itv1).end(); ++itv1v, ++itmv)
                  {
                    for (Vector<real_t>::iterator itv2v=(*itv2).begin(); itv2v!=(*itv2).end(); ++itv2v, ++itmv)
                    {
                      *itmv = sf(*itv1v, *itv2v);
                    }
                  }
                }
              }
              break;
            }
            else // both vt1 and vt2 are complex
            {
              Vector<Vector<complex_t>>& vec1 = *sut1.entries()->cvEntries_p;
              Vector<Vector<complex_t>>& vec2 = *sut2.entries()->cvEntries_p;
              std::vector<Matrix<complex_t>>& mat = entries_p->cmEntries_p->values();
              Vector<Vector<complex_t>>::iterator itv1=vec1.begin();
              Vector<Vector<complex_t>>::iterator itv2=vec2.begin();
              std::vector<Matrix<complex_t>>::iterator itm=mat.begin();
              for (Vector<Vector<complex_t>>::iterator itv1=vec1.begin(); itv1!=vec1.end(); ++itv1, ++itm)
              {
                for (Vector<Vector<complex_t>>::iterator itv2=vec2.begin(); itv2!=vec2.end(); ++itv2, ++itm)
                {
                  Matrix<complex_t>::iterator itmv=(*itm).begin();
                  for (Vector<complex_t>::iterator itv1v=(*itv1).begin(); itv1v!=(*itv1).end(); ++itv1v, ++itmv)
                  {
                    for (Vector<complex_t>::iterator itv2v=(*itv2).begin(); itv2v!=(*itv2).end(); ++itv2v, ++itmv)
                    {
                      *itmv = sf(*itv1v, *itv2v);
                    }
                  }
                }
              }
              break;
            }
          }
        }
      }
    } // end of case _vector
  }
  computingInfo_.isComputed=true;
  trace_p->pop();
}

/*! construct special matrix from a structure given by a TermMatrix
  A: the SuTermMatrix model
  sm: special matrix type (up to now, only _idMatrix is available)
  a: coefficient to apply
  na: option name
*/
SuTermMatrix::SuTermMatrix(const SuTermMatrix& A, SpecialMatrix sm, const complex_t& a, const string_t& na)
{
  if(na=="") name_="Id";
  else name_ = na;
  switch(sm)
    {
      case _idMatrix:
        {
          if(std::imag(a)==0.)
          {
              SuTermVector v("cte_vector "+tostring(a),A.u_p, A.space_u_p,std::real(a));
              diagFromSuTermVector(A.u_p, A.space_u_p, A.v_p, A.space_v_p, v,_cs, _sym, na);
          }
          else
          {
              SuTermVector v("cte_vector "+tostring(a),A.u_p, A.space_u_p,a);
              diagFromSuTermVector(A.u_p, A.space_u_p, A.v_p, A.space_v_p, v,_cs, _sym, na);
          }
          break;
        }
      default:
        where("SuTermMatrix::SuTermMatrix(SuTermMatrix, SpecialMatrix, String)");
        error("matrix_type_not_handled", words("matrix", sm));
    }
}

//constructor from linear combination of SuTermMatrix's
SuTermMatrix::SuTermMatrix(const LcTerm<SuTermMatrix>& lc, const string_t& na)
{
  termType_ = _sutermMatrix;
  subf_p = nullptr;
  u_p = nullptr;
  v_p = nullptr;
  space_u_p = nullptr;
  space_v_p = nullptr;
  initPointers();
  compute(lc, na);
}

SuTermMatrix::~SuTermMatrix()
{
  if(entries_p != nullptr) delete entries_p;
  if(scalar_entries_p != entries_p && scalar_entries_p != nullptr) delete scalar_entries_p;
  if(rhs_matrix_p != nullptr) delete rhs_matrix_p;
  if(hm_entries_p != nullptr) delete hm_entries_p;
  if(hm_scalar_entries_p != nullptr && hm_scalar_entries_p != hm_entries_p) delete hm_scalar_entries_p;
  if(cluster_u != nullptr) delete cluster_u;
  if(cluster_v != nullptr && cluster_v != cluster_u) delete cluster_u;

}

void SuTermMatrix::copy(const SuTermMatrix& mat)
{
  termType_ = _sutermMatrix;
  subf_p = mat.subf_p;             // no copy, shared pointer !
  u_p = mat.u_p;
  v_p = mat.v_p;
  space_u_p = mat.space_u_p;
  space_v_p = mat.space_v_p;
  subspaces_u = mat.subspaces_u;
  subspaces_v = mat.subspaces_v;
  initPointers();
  computingInfo_.noAssembly = mat.computingInfo_.noAssembly;
  computingInfo_.isComputed = mat.computingInfo_.isComputed;
  if(mat.entries_p != nullptr) entries_p = new MatrixEntry(*mat.entries_p);
  if(mat.scalar_entries_p != nullptr)              //copy scalar representation
    {
      if(mat.scalar_entries_p != mat.entries_p) scalar_entries_p = new MatrixEntry(*mat.scalar_entries_p);
      else scalar_entries_p = entries_p;
      cdofs_u = mat.cdofs_u;
      cdofs_v = mat.cdofs_v;
      orgcdofs_u = mat.orgcdofs_u;
      orgcdofs_v = mat.orgcdofs_v;
    }
  if(mat.rhs_matrix_p != nullptr) rhs_matrix_p = new MatrixEntry(*mat.rhs_matrix_p);    //copy rhs matrix

  //copy hierarchical representation
  if(mat.hm_entries_p != nullptr) hm_entries_p = new HMatrixEntry<FeDof>(*mat.hm_entries_p);
  if(mat.hm_scalar_entries_p != nullptr)
    {
      if(mat.hm_scalar_entries_p == mat.hm_entries_p) hm_scalar_entries_p=hm_entries_p;
      else  hm_scalar_entries_p = new HMatrixEntry<FeDof>(*mat.hm_scalar_entries_p);
    }
  //cluster trees are not copied in HMatrix (shared pointers), ClusterTree copy is managed here
  if(mat.cluster_u != nullptr)
    {
      cluster_u = new ClusterTree<FeDof>(*mat.cluster_u);
      if(hm_entries_p != nullptr) hm_entries_p->setClusterCol(cluster_u);
      if(hm_scalar_entries_p != nullptr) hm_scalar_entries_p->setClusterCol(cluster_u);
    }
  if(mat.cluster_v != nullptr)
    {
      if(mat.cluster_v == mat.cluster_u) cluster_v = cluster_u;
      else cluster_v = new ClusterTree<FeDof>(*cluster_v);
      if(hm_entries_p != nullptr) hm_entries_p->setClusterRow(cluster_v);
      if(hm_scalar_entries_p != nullptr) hm_scalar_entries_p->setClusterRow(cluster_v);
    }
}

// assign
SuTermMatrix& SuTermMatrix::operator=(const SuTermMatrix& mat)
{
  clear();
  copy(mat);
  return *this;
}

//deallocate memory used by a SuTermMatrix
void SuTermMatrix::clear()
{
  if(entries_p != nullptr) delete entries_p;
  if(scalar_entries_p != entries_p && scalar_entries_p != nullptr) delete scalar_entries_p;
  if(rhs_matrix_p != nullptr) delete rhs_matrix_p;
  cdofs_u.clear(); cdofs_v.clear();
  orgcdofs_u.clear(); orgcdofs_v.clear();
  if(hm_entries_p != nullptr) delete hm_entries_p;
  if(hm_scalar_entries_p != hm_entries_p && hm_scalar_entries_p != nullptr) delete hm_scalar_entries_p;
  if(cluster_u != nullptr) delete cluster_u;
  if(cluster_v != nullptr && cluster_v != cluster_u) delete cluster_v;
  initPointers();
  computed() = false;
}

//deallocate memory used by scalar part of SuTermMatrix
void SuTermMatrix::clearScalar()
{
  if(scalar_entries_p != entries_p && scalar_entries_p != nullptr) delete scalar_entries_p;
  scalar_entries_p = nullptr;
  cdofs_u.clear(); cdofs_v.clear();
  orgcdofs_u.clear(); orgcdofs_v.clear();
  if(hm_scalar_entries_p != nullptr) delete hm_scalar_entries_p;
  hm_scalar_entries_p = nullptr;
}

// specify the storage of SuTermMatrix BEFORE COMPUTATION
void SuTermMatrix::setStorage(StorageType st, AccessType at)
{
  //change matrix storage if matrix is computed else change only storage attributes
  if(computed() && (computingInfo_.storageType != st || computingInfo_.storageAccess != at)) toStorage(st, at);
  computingInfo_.storageType = st;
  computingInfo_.storageAccess = at;
}

// change storage of matrix (entries), heavy computation
void SuTermMatrix::toStorage(StorageType st, AccessType at)
{
  //change matrix storage if matrix is computed else change only storage attributes
  MatrixStorage* sto=nullptr, *msto=nullptr;
  if(entries_p != nullptr && (entries_p->storageType() != st || entries_p->accessType() != at))
  {
    sto = storagep();
    MatrixStorage* msto = findMatrixStorage(sto->stringId, st, at, sto->buildType(), numberOfRows(), numberOfCols());
    if(msto==nullptr) //expansive method, to improve in future
    {
      std::vector<std::vector<number_t> > colIndices = sto->scalarColIndices();  //retry column indices
      msto = buildStorage(st, at, sto->buildType(), numberOfRows(), numberOfCols(), colIndices, storagep()->stringId);
      entries_p->toStorage(msto);
    }
  }
  if(scalar_entries_p != nullptr && scalar_entries_p != entries_p && (scalar_entries_p->storageType() != st || scalar_entries_p->accessType() != at))
  {
    sto = scalarStoragep();
    MatrixStorage* msto = findMatrixStorage(sto->stringId, st, at, sto->buildType(), numberOfRows(), numberOfCols());
    if(msto==nullptr) //expansive method, to improve in future
    {
      std::vector<std::vector<number_t> > colIndices = sto->scalarColIndices();  //retry column indices
      msto = buildStorage(st, at, sto->buildType(), numberOfRows(), numberOfCols(), colIndices, sto->stringId);
      scalar_entries_p->toStorage(msto);
    }
  }
  //if not computed, change only the storage attributes
  computingInfo_.storageType = st;
  computingInfo_.storageAccess = at;
}

// return value type (_real,_complex)
ValueType SuTermMatrix::valueType() const
{
  if(entries_p != nullptr) return entries_p->valueType_;
  if(scalar_entries_p != nullptr) return scalar_entries_p->valueType_;
  return _real;
}

// return structure type (_scalar,_matrix)
StrucType SuTermMatrix::strucType() const
{
  if(entries_p != nullptr) return entries_p->strucType_;
  if(scalar_entries_p != nullptr) return scalar_entries_p->strucType_;
  return _scalar;
}

// return symetry of submatrix
SymType SuTermMatrix::symmetry() const
{
  if(entries_p != nullptr) return entries_p->symmetry();
  if(scalar_entries_p != nullptr) return scalar_entries_p->symmetry();
  return _noSymmetry;
}

// return storage type of submatrix
StorageType SuTermMatrix::storageType() const
{
  if(entries_p != nullptr) return entries_p->storageType();
  if(scalar_entries_p != nullptr) return scalar_entries_p->storageType();
  return _noStorage;
}

// return access type of submatrix
AccessType SuTermMatrix::accessType() const
{
  if(entries_p != nullptr) return entries_p->accessType();
  if(scalar_entries_p != nullptr) return scalar_entries_p->accessType();
  return _noAccess;
}

// return submatrix storage if exists
MatrixStorage* SuTermMatrix::storagep() const
{
  if(entries_p != nullptr) return entries_p->storagep();
  return nullptr;
}

// return submatrix scalar storage pointer (may be 0)
MatrixStorage* SuTermMatrix::scalarStoragep() const
{
  if(scalar_entries_p != nullptr) return scalar_entries_p->storagep();
  return nullptr;
}

// return factorization type if factorized else return _noFactorization
FactorizationType SuTermMatrix::factorization() const
{
  if(scalar_entries_p != nullptr) return scalar_entries_p->factorization();
  if(entries_p != nullptr) return entries_p->factorization();
  return _noFactorization;
}

// number of rows (counted in scalar dof)
number_t SuTermMatrix::numberOfRows() const
{
  if(scalar_entries_p != nullptr) return cdofs_v.size();
  return space_v_p->nbDofs() * v_p->nbOfComponents();
}

// number of columns (counted in scalar dof)
number_t SuTermMatrix::numberOfCols() const
{
  if(scalar_entries_p != nullptr) return cdofs_u.size();
  return space_u_p->nbDofs() * u_p->nbOfComponents();
}

// list of involved unknown spaces
std::set<const Space*> SuTermMatrix::unknownSpaces() const
{
  std::set<const Space*>sps;
  if(u_p!=nullptr) sps.insert(u_p->space());
  if(v_p!=nullptr) sps.insert(v_p->space());
  return sps;
}

// return the actual pointer to entries (priority to scalar entry), return 0 is no allocated pointer
MatrixEntry* SuTermMatrix::actual_entries() const
{
  MatrixEntry* me = scalar_entries_p;
  if(me != nullptr) return me;
  return entries_p;
}

// return the number of non zero (scalar non zero)
number_t SuTermMatrix::nnz() const
{
  if(scalar_entries_p != nullptr) return scalar_entries_p->size();
  if(entries_p != nullptr) return entries_p->scalarSize();
  return 0;
}

/*build the required subspaces and the largest subspace (possibly the whole space)
  this function:
   - travel along basic bilinear forms of the linear combination
   - identify the largest geometric domain
   - build the largest subspaces (unknown and testfunction)
   - create subspace related to each basic linear form if it does not exist
   - create dofs numbering between domain subspaces ands largest subspaces

 dofs numbering:
    whole space     largest subspace    domain subspace
            1               5                2
            2               3                1
            3               1
            4
            5
*/
void SuTermMatrix::buildSubspaces()
{
  //for safety
  if(subf_p == nullptr) return ;                //no linear form, nothing to do
  trace_p->push("SuTermMatrix::buildSubspaces");

  //create the list of domains
  number_t nbbf = subf_p->size();                 //number of basic bilinear forms in bilinear form
  std::vector<const GeomDomain* > doms_u, doms_v;   //null list of domains
  doms_u.reserve(nbbf);
  doms_v.reserve(nbbf);
  bool hasDoubleIntg = false;
  bool hasDifExt= false;
  bool hasRestriction= false;
  Space* spu = subf_p->up()->space();  //space of the unknown
  Space* spv = subf_p->vp()->space();  //space of the testfunction

  for(it_vbfp it = subf_p->begin(); it != subf_p->end(); it++)   //travel the linear combination of forms
    {
      BasicBilinearForm* blc = it->first;
      switch(blc->type())
        {
          case _intg:
            {
              const GeomDomain *dom = blc->asIntgForm()->domain(); //get the domain of bilinear form
              const MeshDomain *mdom = dom->meshDomain();
              const GeomDomain *sdomu = spu->domain(), *sdomv = spv->domain();
              const MeshDomain *smdomu = sdomu->meshDomain(), *smdomv = sdomv->meshDomain();
              const OperatorOnUnknown& opu=blc->asIntgForm()->opu(), &opv=blc->asIntgForm()->opv();
              if(blc->asIntgForm()->computationType()==_FEextComputation)
              {
                 // if u extension domain not given, find or build it
                 auto& extdomu = blc->extDom_up();
                 if(extdomu==nullptr)
                 {
                   if(mdom->isSideOf(*smdomu)) //extended domain from boundary
                   {
                      if(opu.extensionRequired() && spu->dimDomain()> dom->dim())
                         extdomu = &mdom->extendDomain(false,*sdomu);
                   }
                   else  //fictitious domain
                   {
                     if(spu->dimDomain() > dom->dim() || spu->domain()->mesh()!= dom->mesh())
                        extdomu = &mdom->ficticiousDomain(*sdomu);
                   }
                 }
                 if(extdomu==nullptr) doms_u.push_back(dom); else doms_u.push_back(extdomu);
                 // if v extension domain not given, find or build it
                 auto& extdomv = blc->extDom_vp();
                 if(extdomv==nullptr)
                 {
                   if(mdom->isSideOf(*smdomv)) //extended domain from boundary
                   {
                      if(opv.extensionRequired() && spv->dimDomain()> dom->dim())
                      {
                        if(extdomu!=nullptr) extdomv = extdomu;  //assume same extended domain
                        else                 extdomv = &mdom->extendDomain(false,*sdomv);
                      }
                   }
                   else  //fictitious domain
                   {
                      if(spv->dimDomain() > dom->dim() || spv->domain()->mesh()!= dom->mesh())
                      {
                         if(extdomu!=nullptr) extdomv = extdomu;  //assume same extended domain
                         else                 extdomv = &mdom->ficticiousDomain(*sdomv);
                      }
                   }
                 }
                 if(extdomv==nullptr) doms_v.push_back(dom); else doms_v.push_back(extdomv);
                 if(!hasDifExt && extdomu!=extdomv) hasDifExt=true;
              }
              else //standard case
                {
                  if(opu.domain()!=nullptr) {doms_u.push_back(opu.domain()); hasRestriction=true;}  // domain restriction operator on opu
                  else doms_u.push_back(dom);
                  if(opv.domain()!=nullptr) {doms_v.push_back(opv.domain()); hasRestriction=true;}   // domain restriction operator on opv
                  else doms_v.push_back(dom);
                }
              break;
            }
          case _doubleIntg:
            {
              // extend subspace domain when a sidedomain and extension required only if the unknown space dim is greater than side domain dim
              // thus the dofs will be the extend domain dofs
              DoubleIntgBilinearForm *dbif = blc->asDoubleIntgForm();
              DomainPair domp = dbif->domains();
              if(domp.first->meshDomain()->isSideDomain() && spu->dimDomain()>domp.first->dim()
                  &&  (dbif->computationType() == _IEextComputation || dbif->computationType() == _FEextComputation || dbif->opu().extensionRequired()))
                doms_u.push_back(&domp.first->meshDomain()->extendDomain(false,*(spu->domain())));  //extend towards spu->domain
              else doms_u.push_back(domp.first);
              if(domp.second->meshDomain()->isSideDomain() && spv->dimDomain()>domp.second->dim()
                  && (dbif->computationType() == _IEextComputation || dbif->computationType() == _FEextComputation ||dbif->opv().extensionRequired()))
                doms_v.push_back(&domp.second->meshDomain()->extendDomain(false,*(spv->domain()))); //extend towards spv->domain
              else doms_v.push_back(domp.second);
              hasDoubleIntg = true;
              break;
            }
          case _userLf:  //extension and restriction not yet handled
            {
              doms_u.push_back(&blc->dom_up());
              doms_v.push_back(&blc->dom_vp());
              break;
            }
          default:
            error("bform_not_handled", words("form type", blc->type())); // basic bilinear form not handled
        }
    }

  //for unknown u
  space_u_p = spu->buildSubspaces(doms_u, subspaces_u); //build subspaces
  //for test function v
  if(spv == spu && !hasDoubleIntg && !hasDifExt && !hasRestriction)  //same spaces, no double integral, no differential extension, no restriction
    {
      space_v_p = space_u_p;
      subspaces_v = subspaces_u;
    }
  else
    {
      space_v_p = spv->buildSubspaces(doms_v, subspaces_v); //build subspaces
    }
  trace_p->pop();
}

/*! Algebraic operation += and -=
    the result of these operations is a SuTermMatrix with extended dof numbering, may require a new storage !!!
    works with entries prior to scalar entries
*/
SuTermMatrix& SuTermMatrix::operator+=(const SuTermMatrix& stM)
{
  trace_p->push("SuTermMatrix::operator+=(SuTermMatrix)");
  if(u_p != stM.u_p && v_p != stM.v_p) return *this;   //different row or col unknowns, nothing is done

  if(subf_p != nullptr)
    {
      if(stM.subf_p != nullptr) *subf_p += *stM.subf_p;    //add bilinear form
    }
  else subf_p = stM.subf_p;
  if(!computed() && !stM.computed()) return *this;    //no hard sum, compute is delayed
  if(!computed() && stM.computed()) error("not_computed_term", name());
  if(computed() && !stM.computed()) error("not_computed_term", stM.name());

  //hard sum
  if(space_u_p == stM.space_u_p && space_v_p == stM.space_v_p)
    {
      MatrixEntry* mat =scalar_entries_p, *matM =stM.scalar_entries_p;
      if(mat==nullptr || matM==nullptr) { mat=entries_p; matM=stM.entries_p;}  //try with scalar entries
      if(mat==nullptr) error("null_pointer", "mat");
      if(matM==nullptr) error("null_pointer", "matM");
      if(mat->storagep()==matM->storagep())
        {
          *mat += *matM;
          trace_p->pop();
          return *this;
        }
    }

  //not the same storage, use general linear combination algorithm
  LcTerm<SuTermMatrix> lc(*this, 1., stM, 1.);
  SuTermMatrix* nsut=new SuTermMatrix(0,"",false);
  nsut->compute(lc);
  clear();
  copy(*nsut);
  delete nsut;
  trace_p->pop();
  return *this;
}

SuTermMatrix& SuTermMatrix::operator-=(const SuTermMatrix& stM)
{
  trace_p->push("SuTermMatrix::operator-=(SuTermMatrix)");
  if(u_p != stM.u_p || v_p != stM.v_p)  //different row or col unknowns, nothing is done
    {
      warning("free_warning"," in SuTermMatrix-=SuTermMatrix, not the same row or col unknown, nothing has been done");
      trace_p->pop();
      return *this;
    }

  if(subf_p != nullptr)
    {
      if(stM.subf_p != nullptr) *subf_p -= *stM.subf_p;    //add bilinear form
    }
  else subf_p = stM.subf_p;

  if(!computed() && !stM.computed()) return *this;    //no hard sum, compute is delayed
  if(!computed() && stM.computed()) error("not_computed_term", name());
  if(computed() && !stM.computed()) error("not_computed_term", stM.name());

  //hard sum
  if(space_u_p == stM.space_u_p && space_v_p == stM.space_v_p)
    {
      MatrixEntry* mat =scalar_entries_p, *matM =stM.scalar_entries_p;
      if(mat==nullptr || matM==nullptr) {mat=entries_p; matM=stM.entries_p;}  //try with scalar entries
      if(mat==nullptr) error("null_pointer", "mat");
      if(matM==nullptr) error("null_pointer", "matM");
      if(mat->storagep()==matM->storagep())
        {
          *mat -= *matM;
          trace_p->pop();
          return *this;
        }
    }

  //not the same storage, use general linear combination algorithm
  LcTerm<SuTermMatrix> lc(*this, 1., stM, -1.);
  SuTermMatrix* nsut=new SuTermMatrix(0,"",false);
  nsut->compute(lc);
  clear();
  copy(*nsut);
  delete nsut;
  trace_p->pop();
  return *this;
}

//utility for SuTermMatrix::compute, collect subilinearform by type of computation and domain
std::vector<SuBilinearForm> SuTermMatrix::getSubfs(const SuBilinearForm& subf, ComputationType ct, ValueType& vt) const
{
  bool oneDomain = !(ct==_IEComputation || ct==_IEextComputation || ct==_IESPComputation || ct==_IEHmatrixComputation);
  std::vector<SuBilinearForm> subfs;
  if(oneDomain)
  {
    std::map<std::pair<const GeomDomain*,const GeomDomain*>, std::vector<bfPair> > bfByDomain;       //list of ct bilinear forms by dom_u, dom_v
    std::map<std::pair<const GeomDomain*,const GeomDomain*>, std::vector<bfPair> >::iterator itm;
    for(cit_vbfp it = subf.begin(); it != subf.end(); it++)
    {
      BasicBilinearForm* bbfp = it->first;
      if(bbfp->computationType()==ct)
      {
        const GeomDomain* gpu = &bbfp->dom_up(), *gpv=gpu;
        if(bbfp->type()==_intg)
        {
          const IntgBilinearForm* ibl = bbfp->asIntgForm();
          if(ibl->opu().domain()!=nullptr) gpu=ibl->opu().domain();  // case of restriction of opu to a domain
          if(ibl->opv().domain()!=nullptr) gpv=ibl->opv().domain();  // case of restriction of opv to a domain
        }
        std::pair<const GeomDomain*,const GeomDomain*> gps(gpu,gpv);
        itm = bfByDomain.find(gps);
        if(itm == bfByDomain.end()) bfByDomain[gps] = std::vector<bfPair>(1, bfPair(*it));
        else                         itm->second.push_back(bfPair(*it));
        if(bbfp->valueType() == _complex) vt = _complex;
      }
    }
    for(itm = bfByDomain.begin(); itm != bfByDomain.end(); itm++)
        subfs.push_back(SuBilinearForm(itm->second));
  }
  else //two domains
  {
    std::map<DomainPair, std::vector<bfPair> > bfByDomain;       //list of ct bilinear forms by domain
    std::map<DomainPair, std::vector<bfPair> >::iterator itm;
    for(cit_vbfp it = subf.begin(); it != subf.end(); it++)
    {
      BasicBilinearForm* bbfp = it->first;
      if(bbfp->computationType()==ct && bbfp->type()==_doubleIntg)
      {
        DomainPair dom = bbfp->asDoubleIntgForm()->domains();
        itm = bfByDomain.find(dom);
        if(itm == bfByDomain.end()) bfByDomain[dom] = std::vector<bfPair>(1, bfPair(*it));
        else                         itm->second.push_back(bfPair(*it));
        if(bbfp->valueType() == _complex) vt = _complex;
      }
    }
    for(itm = bfByDomain.begin(); itm != bfByDomain.end(); itm++)
        subfs.push_back(SuBilinearForm(itm->second));
  }
  return subfs;
}

//utility for SuTermMatrix::compute(), update to dense storage type regarding dense part of storage
//(all types of bilinear form except FE)
void SuTermMatrix::updateStorageType(const std::vector<SuBilinearForm>& subfs, std::set<number_t>& dofu,
                                     std::set<number_t>& dofv, StorageType& st) const
{
  if(st==_dense) return;    //nothing to do, already dense
  number_t fullsize = space_u_p->nbDofs() * space_v_p->nbDofs();
  std::vector<SuBilinearForm>::const_iterator itsubf=subfs.begin();
  for(; itsubf != subfs.end() && st != _dense; itsubf++)   //travel double intg IE forms
    {
      ComputationType ct=itsubf->begin()->first->computationType();
      const Space* spu=nullptr;
      if(ct==_FEextComputation || ct==_IEextComputation)
        spu = Space::findSubSpace(itsubf->dom_up()->extendedDomain(false,*space_u_p->domain()),space_u_p);
      else spu = Space::findSubSpace(itsubf->dom_up(),space_u_p);
      const Space* spv=nullptr;
      if(ct==_FEextComputation || ct==_IEextComputation)
        spv = Space::findSubSpace(itsubf->dom_vp()->extendedDomain(false,*space_v_p->domain()),space_v_p);
      else spv = Space::findSubSpace(itsubf->dom_vp(),space_v_p);
      if(spu==nullptr || spv==nullptr) return;   //abnormal, abort and keep current storage
      if(spu == space_u_p || spv == space_v_p) st = _dense;
      else
        {
          std::vector<number_t> dofs = spu->dofIds();
          dofu.insert(dofs.begin(), dofs.end());
          if(spu != spv) dofs = spv->dofIds();
          dofv.insert(dofs.begin(), dofs.end());
          if(2 * dofu.size()*dofv.size() > fullsize) st = _dense;   //dense part >50%
        }
    }
}

//utility for SuTermMatrix::compute(), add dense part to non dense storage
void SuTermMatrix::addDenseToStorage(const std::vector<SuBilinearForm>& subfs, MatrixStorage* mstorage) const
{
  trace_p->push("SuTermMatrix::addDenseToStorage");
  std::vector<SuBilinearForm>::const_iterator itsubf=subfs.begin();
  for(; itsubf != subfs.end(); itsubf++)    //travel bilinear forms
  {
    ComputationType ct=itsubf->begin()->first->computationType();
    const Space* subspu=nullptr;
    if(ct==_FEextComputation || ct==_IEextComputation)
      subspu = Space::findSubSpace(itsubf->dom_up()->extendedDomain(false,*space_u_p->domain()),space_u_p);
    if(subspu==nullptr) subspu = Space::findSubSpace(itsubf->dom_up(),space_u_p);   //ok in case of double integral not requiring extension domain in u
    if(subspu==nullptr)
    {
      where("SuTermMatrix::addDenseToStorage(...)");
      error("subspace_not_found", space_u_p->name(), itsubf->dom_up()->name());
    }
    const Space* subspv=nullptr;
    if(ct==_FEextComputation || ct==_IEextComputation)
      subspv = Space::findSubSpace(itsubf->dom_vp()->extendedDomain(false,*space_v_p->domain()),space_v_p);
    if(subspv==nullptr) subspv = Space::findSubSpace(itsubf->dom_vp(),space_v_p); //ok in case of double integral not requiring extension domain in v
    if(subspv==nullptr)
    {
      where("SuTermMatrix::addDenseToStorage(...)");
      error("subspace_not_found", space_v_p->name(), itsubf->dom_vp()->name());
    }
    std::vector<number_t> dofu, dofv;
    std::vector<number_t>::iterator it;
    if(subspu->typeOfSpace() == _subSpace) dofu = subspu->subSpace()->dofNumbers();
    else
    {
      dofu.resize(subspu->nbDofs());
      number_t k = 1;
      for(it = dofu.begin(); it != dofu.end(); it++, k++) *it = k;
    }
    std::stringstream ss;
    if(subspu == subspv) { mstorage->addSubMatrixIndices(dofu, dofu); ss << subspu << "-" << subspu;}
    else
    {
      if(subspv->typeOfSpace() == _subSpace) dofv = subspv->subSpace()->dofNumbers();
      else
      {
        dofv.resize(subspv->nbDofs());
        number_t k = 1;
        for(it = dofv.begin(); it != dofv.end(); it++, k++) *it = k;
      }
      mstorage->addSubMatrixIndices(dofv, dofu);
      ss << subspv << "-" << subspu;
    }
    mstorage->stringId+=" +dense_part "+ss.str(); //update storage string id
  }
  trace_p->pop();
}

//utility for SuTermMatrix::compute(), check if there is FE subf with a domain restriction
// see SuTermMatrix::addDenseToStorageFE
bool SuTermMatrix::hasDomainRestriction(const std::vector<SuBilinearForm>& subfs) const
{
  trace_p->push("SuTermMatrix::hasDomainRestriction");
  std::vector<SuBilinearForm>::const_iterator itsubf=subfs.begin();
  for(; itsubf != subfs.end(); itsubf++)    //travel bilinear forms
  {
    const Space* subspu=nullptr, *subspv=nullptr;
    for(cit_vbfp itbf=itsubf->begin(); itbf!=itsubf->end(); ++itbf)
    {
      const GeomDomain* domu=nullptr, *domv=nullptr;
      if(itbf->first->type()!=_userLf)
      {
        const IntgBilinearForm* ibf = itbf->first->asIntgForm();
        domu = ibf->opu().domain();
        if(domu!=nullptr) subspu = Space::findSubSpace(domu,space_u_p); //restriction on u
        else subspu = Space::findSubSpace(itsubf->dom_up(),space_u_p);
        if(subspu==nullptr)
        {
         where("SuTermMatrix::hasDomainRestriction(...)");
         error("subspace_not_found", space_u_p->name(), itsubf->dom_up()->name());
        }
        domv = ibf->opv().domain();
        if(domv!=nullptr) subspv = Space::findSubSpace(domv,space_v_p); //restriction on u
        else subspv = Space::findSubSpace(itsubf->dom_vp(),space_v_p);
        if(subspv==nullptr)
        {
         where("SuTermMatrix::hasDomainRestriction(...)");
         error("subspace_not_found", space_v_p->name(), itsubf->dom_vp()->name());
        }
        if(domu!=nullptr || domv!=nullptr)  {trace_p->pop(); return true;}
      }
    }
  }
  trace_p->pop(); return false;
}

//utility for SuTermMatrix::compute(), add dense part to non dense storage if there exists a domain restriction in SuBilinear forms
// only for FE computation !
void SuTermMatrix::addDenseToStorageFE(const std::vector<SuBilinearForm>& subfs, MatrixStorage* mstorage) const
{
  trace_p->push("SuTermMatrix::addDenseToStorageFE");
  std::vector<SuBilinearForm>::const_iterator itsubf=subfs.begin();
  for(; itsubf != subfs.end(); itsubf++)    //travel bilinear forms
    {
      const Space* subspu=nullptr, *subspv=nullptr;
      for(cit_vbfp itbf=itsubf->begin(); itbf!=itsubf->end(); ++itbf)
      {
        const GeomDomain* domu=nullptr, *domv=nullptr;
        if(itbf->first->type()!=_userLf)
        {
          const IntgBilinearForm* ibf = itbf->first->asIntgForm();
          domu = ibf->opu().domain();
          if(domu!=nullptr) subspu = Space::findSubSpace(domu,space_u_p); //restriction on u
          else subspu = Space::findSubSpace(itsubf->dom_up(),space_u_p);
          if(subspu==nullptr)
          {
            where("SuTermMatrix::addDenseToStorageFE(...)");
            error("subspace_not_found", space_u_p->name(), itsubf->dom_up()->name());
          }
          domv = ibf->opv().domain();
          if(domv!=nullptr) subspv = Space::findSubSpace(domv,space_v_p); //restriction on u
          else subspv = Space::findSubSpace(itsubf->dom_vp(),space_v_p);
          if(subspv==nullptr)
          {
            where("SuTermMatrix::addDenseToStorageFE(...)");
            error("subspace_not_found", space_v_p->name(), itsubf->dom_vp()->name());
          }
          if(domu!=nullptr || domv!=nullptr)  //add a dense part
          {
            std::stringstream ss;
            std::vector<number_t> dofu, dofv;
            std::vector<number_t>::iterator it;
            if(subspu->typeOfSpace() == _subSpace) dofu = subspu->subSpace()->dofNumbers();
            else
            {
              dofu.resize(subspu->nbDofs());
              number_t k = 1;
              for(it = dofu.begin(); it != dofu.end(); it++, k++) *it = k;
            }
            if(subspu == subspv) {mstorage->addSubMatrixIndices(dofu, dofu); ss << subspu << "-" << subspu;}
            else
            {
              if(subspv->typeOfSpace() == _subSpace) dofv = subspv->subSpace()->dofNumbers();
              else
              {
                dofv.resize(subspv->nbDofs());
                number_t k = 1;
                for(it = dofv.begin(); it != dofv.end(); it++, k++) *it = k;
              }
              mstorage->addSubMatrixIndices(dofv, dofu);
              ss << subspv << "-" << subspu;
            }
            mstorage->stringId+="-dense_part"+ss.str(); //update storage string id
          }
        }
      }
    }
    trace_p->pop();
}

//utility for SuTermMatrix::compute(), add DG part to non dense storage
// complex business (time expansive):
//   - create the set of indices (i,j) of current storage
//   - add to this set the indices (i,j) of DG part
//   - if no new indices, return without changing storage
//   - if new indices : recreate a new storage from set of indices (the original one is not deleted!)
void SuTermMatrix::addDGToStorage(const std::vector<SuBilinearForm>& subfs, MatrixStorage*& mstorage) const
{
  trace_p->push("SuTermMatrix::addDGStorage");
  std::map<number_t,std::set<number_t>> indices;  // map : row -> list of col indices
  //add indices from current storage
  std::set<number_t> cols;
  for(number_t r=1;r<=mstorage->nbOfRows();r++)
  {
     cols= mstorage->getCols(r);
     for(auto its=cols.begin();its!=cols.end();++its)
        indices[r].insert(*its);
  }
  number_t s0=0;
  for(auto itm=indices.begin();itm!=indices.end();++itm) s0+=itm->second.size();
  number_t s=s0;

  // add DG
  std::vector<number_t> dofNum_u1, dofNum_v1, dofNum_u2, dofNum_v2;
  std::vector<number_t>* dnv1p, *dnv2p;
  bool samespace = space_v_p==space_u_p;
  std::stringstream ss;
  std::vector<SuBilinearForm>::const_iterator itsubf=subfs.begin();
  for(; itsubf != subfs.end(); itsubf++)    //travel bilinear forms
  {
    if(itsubf->isDG())  //sides domain
    {
      const GeomDomain* sdom=itsubf->dom_up();
      const MeshDomain* mdom=sdom->meshDomain();
      const std::vector<GeomElement*>& gelts=mdom->geomElements;
      for(number_t k = 0; k < gelts.size(); k++)
      {
        GeomElement* gelt = gelts[k];                         //geometric side element
        std::vector<GeoNumPair>& parelts=gelt->parentSides(); // parent elements of side element
        GeomElement* gelt1 = parelts[0].first, *gelt2=nullptr;
        const Element* elt1 = space_u_p->element_p(gelt1);
        const Element* elt2 = nullptr;
        ranks(space_u_p->dofid2rank(), elt1->dofNumbers, dofNum_u1);
        dnv1p=&dofNum_u1;
        if(!samespace)
        {
          elt1 = space_v_p->element_p(gelt1);
          ranks(space_v_p->dofid2rank(), elt1->dofNumbers, dofNum_v1);
          dnv1p=&dofNum_v1;
        }
        for(auto itr=dnv1p->begin();itr!=dnv1p->end();++itr)         // v1 x u1 block
          for(auto itc=dofNum_u1.begin();itc!=dofNum_u1.end();++itc)
            indices[*itr].insert(*itc);
        if(parelts.size()>1) gelt2=parelts[1].first;
        if(gelt2!=nullptr)
        {
          elt2 = space_u_p->element_p(gelt2);
          ranks(space_u_p->dofid2rank(), elt2->dofNumbers, dofNum_u2);
          dnv2p=&dofNum_u2;
          if(!samespace)
          {
            elt2 = space_v_p->element_p(gelt2);
            ranks(space_v_p->dofid2rank(), elt2->dofNumbers, dofNum_v2);
            dnv2p=&dofNum_v2;
          }
          for(auto itr=dnv1p->begin();itr!=dnv1p->end();++itr)
            for(auto itc=dofNum_u2.begin();itc!=dofNum_u2.end();++itc)  // v1 x u2 block
               indices[*itr].insert(*itc);
          for(auto itr=dnv2p->begin();itr!=dnv2p->end();++itr)
          {
            for(auto itc=dofNum_u1.begin();itc!=dofNum_u1.end();++itc)  // v2 x u1 block
               indices[*itr].insert(*itc);
            for(auto itc=dofNum_u2.begin();itc!=dofNum_u2.end();++itc)  // v2 x u2 block
               indices[*itr].insert(*itc);
          }
        }
      }
      number_t ns=0;
      for(auto itm=indices.begin();itm!=indices.end();++itm) ns+=itm->second.size();
      if(ns>s)  //new indices added
      {
        ss <<"-"<<sdom;
        s=ns;
      }
    }
  }
  if(s==s0) {trace_p->pop(); return;} //no new indices added

  // recreate a new storage from col indices
  StorageType st=mstorage->storageType();
  AccessType at=mstorage->accessType();
  number_t dimr=mstorage->nbOfRows(), dimc=mstorage->nbOfColumns();
  string_t na=mstorage->stringId;
  std::vector<std::vector<number_t>> vindices(dimr);
  for(auto its=indices.begin();its!=indices.end(); ++its)
     vindices[its->first-1]=std::vector<number_t>(its->second.begin(), its->second.end());
  delete mstorage; // because it is a clone  (see SuTermMatrix::compute)
  mstorage = buildStorage(st,at,_dgBuild, dimr,dimc,vindices, na+"-DG_part"+ss.str()); //recreate a new storage
  trace_p->pop();
}

/*! create scalar representation scalar_entries_p and cdofs_u, cdofs_v numbering vectors
    - if one of unknowns is a vector unknown a new MatrixEntry is created using  MatrixEntry::toScalar() function
      and linked to the  MatrixEntry pointer scalar_entries_p. In that case, if keepVector is false the vector representation is destroyed
    - if both unknowns are scalar unknown, no new MatrixEntry: scalar_entries_p = entries_p
    This function builds also, in any case, the cdofs_u, cdofs_v numbering vectors (vector of ComponentDof's)

*/
void SuTermMatrix::toScalar(bool keepEntries)
{
  trace_p->push("SuTermMatrix::toScalar");
  if(scalar_entries_p != nullptr) {trace_p->pop(); return;}    //already done, delete it outside if you want to rebuild

  //create or update scalar_entries_p
  dimen_t nbc_u=u_p->nbOfComponents(), nbc_v=v_p->nbOfComponents();
  if(nbc_u== 1 && nbc_v==1)  scalar_entries_p = entries_p;           //no conversion, pointer copy
  else if(entries_p != nullptr) scalar_entries_p= entries_p->toScalar();     //create new scalar entries from matrix entries

  //create cdofs_u
  std::vector<number_t> dofs= space_u_p->dofIds();
  cdofs_u.resize(dofs.size()*nbc_u);
  std::vector<number_t>::iterator itd=dofs.begin();
  std::vector<DofComponent>::iterator itdc=cdofs_u.begin();
  for(; itd!=dofs.end(); itd++)
    for(dimen_t d = 0; d < nbc_u; d++, itdc++) *itdc= DofComponent(u_p, *itd, d+1);

  //create cdofs_v
  dofs = space_v_p->dofIds();
  cdofs_v.resize(dofs.size()*nbc_v);
  itdc = cdofs_v.begin();
  itd = dofs.begin();
  for(; itd!=dofs.end(); itd++)
    for(dimen_t d = 0; d < nbc_v; d++, itdc++) *itdc= DofComponent(v_p, *itd, d+1);

  //delete entries_p
  if(!keepEntries && entries_p!=scalar_entries_p)
    {
      delete entries_p;
      entries_p=nullptr;
    }
  trace_p->pop();
}

/*! compute the term matrix from bilinear form and constraints data (possibly none)
    the process works as follows:
    - collect bilinear forms by type (FE, FE requiring domain extension, SP, mixed FE-SP, IE, mixed IE-SP)
    - if not imposed, find a suited storage with the following rule: compressed sparse for FE, else dense
    - construct the storage
    - create matrix entry with the right type
    - call computation routines for each collection of bilinear forms
*/
void SuTermMatrix::compute()
{
  trace_p->push("SuTermMatrix::compute");
  if(Trace::traceMemory) thePrintStream<<"compute SuTermMatrix "<<name() << " :   block ("<<v_p->name()<<", "<<u_p->name()<<")"<<eol;

  //get bilinear forms by types
  ValueType vt=_real;
  std::vector<SuBilinearForm> FEsubfs     = getSubfs(*subf_p,_FEComputation,vt);        //FE bilinear forms
  std::vector<SuBilinearForm> FEextsubfs  = getSubfs(*subf_p,_FEextComputation,vt);     //FE bilinear forms requiring domain extension
  std::vector<SuBilinearForm> SPsubfs     = getSubfs(*subf_p,_SPComputation,vt);        //SP bilinear forms
  std::vector<SuBilinearForm> FESPsubfs   = getSubfs(*subf_p,_FESPComputation,vt);      //FE-SP bilinear forms
  std::vector<SuBilinearForm> IEsubfs     = getSubfs(*subf_p,_IEComputation,vt);        //IE bilinear forms
  std::vector<SuBilinearForm> IEextsubfs  = getSubfs(*subf_p,_IEextComputation,vt);     //IE bilinear forms requiring domain extension
  std::vector<SuBilinearForm> IESPsubfs   = getSubfs(*subf_p,_IESPComputation,vt);      //IE-SP bilinear forms
  std::vector<SuBilinearForm> IEHMsubfs   = getSubfs(*subf_p,_IEHmatrixComputation,vt); //IE bilinear forms, HMatrix
  std::vector<SuBilinearForm> DGsubfs     = getSubfs(*subf_p,_DGComputation,vt);        //DG bilinear forms (sidesDomain)

  std::vector<SuBilinearForm>::iterator itsubf;

  number_t nbnoHM = FEsubfs.size() + FEextsubfs.size() + SPsubfs.size() + FESPsubfs.size()+
                    IEsubfs.size() + IEextsubfs.size() + IESPsubfs.size() + DGsubfs.size();
  number_t nbHM = IEHMsubfs.size();
  StrucType str = _scalar;

  if(theVerboseLevel>0) std::cout<<"compute SuTermMatrix "<<u_p->name()<<" "<<v_p->name()<<",  subilinear form: "<<subf_p->asString();

  if(nbnoHM>0)   //LargeMatrix structure
    {
      /* create storage if not exists, the process is the following:
      if storage is imposed (computingInfo_.storageType!=_noStorage) we use it
      if there is no double intg form we use the dual or sym compressed sparse storage (sym when the bilinear form has symmetry)
      if there is no intg form we use the row or sym dense storage (sym when the bilinear form has symmetry)
      if there is a mixture of intg and double intg
          if the double intg term almost fill the matrix we use row or sym dense storage
          if not we use dual or sym compressed sparse storage
      */
      StorageType st = _noStorage;
      AccessType at = _noAccess;
      if(st == _noStorage && FEsubfs.size() == 0 && FEextsubfs.size() == 0 && DGsubfs.size()==0) st = _dense;     //no FE forms, dense storage
      if(st == _noStorage)
        {
          st = _cs; // has FE forms
          std::set<number_t> dofu, dofv;
          if(IEsubfs.size()>0) updateStorageType(IEsubfs, dofu, dofv, st);
          if(st!=_dense && IEextsubfs.size()>0) updateStorageType(IEextsubfs, dofu, dofv, st);
          if(st!=_dense && IESPsubfs.size()>0)  updateStorageType(IESPsubfs, dofu, dofv, st);
          if(st!=_dense && space_u_p!=space_v_p && u_p->space()->dimDomain()!=v_p->space()->dimDomain()) st=_dense;
            //force dense storage because geomElement are not necessary ordered in a same way
            //in future developp a new buildStorage function that build a safe compressed storage
        }
      if(at == _noAccess)
        {
          at = _dual;
          if(space_u_p != space_v_p || (u_p!=v_p && u_p!=v_p->dual_p())) at = _row;
          else if((u_p==v_p || u_p==v_p->dual_p()) && subf_p->symType() != _noSymmetry) at = _sym;
        }
      //modify storage type if required, never override dense storage
      if(st!=_dense && computingInfo_.storageType!=_noStorage) st=computingInfo_.storageType;
      if(computingInfo_.storageAccess!=_noAccess) at=computingInfo_.storageAccess;

      //now construct storage or find an existing one
      MatrixStorage* mstorage = buildStorage(*space_v_p, *space_u_p, st, at, _feBuild);
      //mstorage->visual(thePrintStream); thePrintStream.flush();

      bool addStorage = IEsubfs.size()>0 || IESPsubfs.size()>0 || IEextsubfs.size()>0 || DGsubfs.size()>0 || hasDomainRestriction(FEsubfs);
      if(addStorage && st != _dense)   //add dense or DG part to current non dense storage
        {
          if(mstorage->numberOfObjects()>0) mstorage = mstorage->clone(); //existing storage, do a copy to be safe
          if(IEsubfs.size()>0)    addDenseToStorage(IEsubfs, mstorage);
          if(IESPsubfs.size()>0)  addDenseToStorage(IESPsubfs, mstorage);
          if(IEextsubfs.size()>0) addDenseToStorage(IEextsubfs, mstorage);
          if(FEsubfs.size()>0)    addDenseToStorageFE(FEsubfs, mstorage);
          if(DGsubfs.size()>0)    addDGToStorage(DGsubfs, mstorage);
        }
      //mstorage->visual(thePrintStream); thePrintStream.flush();

      //create matrix entry
      SymType symat = subf_p->symType();
      if(at != _sym) symat = _noSymmetry;   //non symmetric storage induces non symmetric matrix
      dimPair duv = dimPair(subf_p->vp()->nbOfComponents(), subf_p->up()->nbOfComponents());
      if(duv != dimPair(1, 1)) str = _matrix;
      vt=subf_p->valueType();
      entries_p = new MatrixEntry(vt, str, mstorage, duv, symat); //allocate entries
      if(theVerboseLevel>0) {
        std::cout << ", ";
        entries_p->printHeader(std::cout);
      }
      //elapsedTime("build storage");
      if(FEsubfs.size()>0)    compute<_FEComputation>(FEsubfs, vt, str);
      if(FEextsubfs.size()>0) compute<_FEextComputation>(FEextsubfs, vt, str);
      if(SPsubfs.size()>0)    compute<_SPComputation>(SPsubfs, vt, str);
      if(FESPsubfs.size()>0)  compute<_FESPComputation>(FESPsubfs, vt, str);
      if(IEsubfs.size()>0)    compute<_IEComputation>(IEsubfs, vt, str);
      if(IEextsubfs.size()>0) compute<_IEComputation>(IEextsubfs, vt, str);
      if(IESPsubfs.size()>0)  compute<_IESPComputation>(IESPsubfs, vt, str);
      if(DGsubfs.size()>0)    compute<_DGComputation>(DGsubfs, vt, str);
    }

  if(nbHM>0)  // HMatrix structure ONLY SCALAR
    {
      if(nbHM>1) error("term_not_suterm", name());
      itsubf=IEHMsubfs.begin();
      dimPair duv = dimPair(itsubf->vp()->nbOfComponents(), itsubf->up()->nbOfComponents());
      if(duv != dimPair(1, 1)) str = _matrix;
      Space* spu=const_cast<Space*>(itsubf->uSpace()), *spv=const_cast<Space*>(itsubf->vSpace());
      //create ClusterTree of domain_u, use FeDOf of space_u
      const DoubleIntgBilinearForm* ibf = itsubf->begin()->first->asDoubleIntgForm(); //basic bilinear form as double integral
      if(ibf->intgMethod()->imType !=_HMatrixIM) error("im_not_handled", words("imtype", ibf->intgMethod()->imType));
      const HMatrixIM* him= static_cast<const HMatrixIM*>(ibf->intgMethod());
      ClusterTree<FeDof>* ctu = him->colCluster_;
      if(ctu==nullptr) ctu=new ClusterTree<FeDof>(spu->feDofs(),him->clusterMethod, him->minColSize);
      ctu->updateElements();
      ClusterTree<FeDof>* ctv = ctu;
      if(spu!=spv)
        {
          ctv=him->rowCluster_;
          ctv = new ClusterTree<FeDof>(spv->feDofs(),him->clusterMethod,him->minRowSize);
          ctv->updateElements();
        }
      him->setRowCluster(ctv);
      him->setColCluster(ctu);
      hm_entries_p = new HMatrixEntry<FeDof>(vt, str, *ctv, *ctu, him->minRowSize, him->minColSize, 1, 1, subf_p->symType());
      real_t c=1.; complex_t zc=1.;
      if(vt == _real)
        {
          computeHMatrix(*itsubf, *hm_entries_p->rEntries_p, c, spu, spv, itsubf->up(), itsubf->vp());
          if(nbnoHM>0) hm_entries_p->rEntries_p->addFELargeMatrix(*entries_p->rEntries_p);
          delete entries_p;
          entries_p=nullptr;
        }
      else
        {
          computeHMatrix(*itsubf, *hm_entries_p->cEntries_p, zc, spu, spv, itsubf->up(), itsubf->vp());
          if(nbnoHM>0) hm_entries_p->cEntries_p->addFELargeMatrix(*entries_p->cEntries_p);
          delete entries_p;
          entries_p=nullptr;
        }
    }
  this->computed() = true;
  //elapsedTime("SutermMatrix computation");
  trace_p->pop();
}

/*! compute SuTermMatrix from a linear combination of SuTermMatrix's (same unknowns)
 the current SuTermMatrix is assumed to be void, this function fills every things
 if it is not the case, every thing is clear
   step 1 :
     find structure and value type of the linear combination
     move SuTermMatrix's entries to entries_p if not allocated (means move to vector representation)
     find largest spaces
     choose storage type (type, access, symmetry)
   step 2 :
     if dense storage create a new one from largest spaces
     if not, merge SutermMatrix storage building the colIndices (to be improved)
             and then create a new storage
   step 3 : do the linear combination
   step 4 : update info and go back to original representation of SuTermMatrix's

*/
void SuTermMatrix::compute(const LcTerm<SuTermMatrix>& lc, const string_t& na)
{
  trace_p->push("SuTermMatrix::compute(LcTerm)");
  clear();
  if(lc.size() == 0) error("is_void", "LcTerm");
  if(lc.size() == 1)   //copy and multiplication
    {
      const SuTermMatrix* sut = dynamic_cast<const SuTermMatrix*>(lc.begin()->first);
      if(sut == nullptr) error("null_pointer", "SuTermMatrix");
      if(!sut->computed()) error("not_computed_term", sut->name());
      copy(*sut);
      name_ = na;
      complex_t a = lc.begin()->second;
      if(name_ == "") name_ = tostring(a) + " * ( " + sut->name() + " )" ;   //set default name
      if(a != complex_t(1., 0.)) *this *= a;
      computed() = true;
      trace_p->pop();
      return;
    }

  // Analyze SuTermMatrix to add,
  // if some are related to vector unknown but in scalar representation, move them back to vector representation
  // and move the result to scalar representation
  std::vector<Space*> spaces_u(lc.size(), 0);
  std::vector<Space*> spaces_v(lc.size(), 0);
  std::vector<Space*>::iterator its_u = spaces_u.begin();
  std::vector<Space*>::iterator its_v = spaces_v.begin();
  SuTermMatrix* stM = const_cast<SuTermMatrix*>(lc.begin()->first);
  if(stM == nullptr) error("null_pointer", "SuTermMatrix");
  MatrixEntry* mat=stM->entries_p;
  if(mat == nullptr)  //move to vector unknown representation
    {
      mat = stM->scalar_entries_p;
      if(mat==nullptr) error("not_computed_term", stM->name());
      stM->entries_p = new MatrixEntry(*mat); //create entries_p
      stM->toVectorUnknown();
      mat = stM->entries_p;
    }
  ValueType vt = stM->valueType();
  StrucType st = stM->strucType();
  u_p = stM->u_p;
  v_p = stM->v_p;
  SymType sym = stM->symmetry();
  StorageType sto = stM->storageType();
  dimPair valdim = mat->nbOfComponents;
  complex_t a = lc.begin()->second;
  name_ = na;
  if(na == "")
    {
      if(a!=complex_t(1.,0.))
        {
          if(a.imag() == 0) name_ = tostring(a.real());
          else name_ = tostring(a);
          name_ += " * ( " + stM->name() + " )" ;
        }
      else name_ += " " + stM->name();
    }
  for(LcTerm<SuTermMatrix>::const_iterator itt = lc.begin(); itt != lc.end(); itt++, its_u++, its_v++)
    {
      stM = const_cast<SuTermMatrix*>(itt->first);
      if(stM == nullptr) error("null_pointer", "SuTermMatrix");
      *its_u = stM->space_u_p; // list of (sub)spaces
      *its_v = stM->space_v_p;
      if(stM->valueType() == _complex) vt = _complex;
      if(stM->strucType() != st) error("term_mismatch_structures", words("structure",stM->strucType()),words("structure",st));
      mat=stM->entries_p;
      if(mat == nullptr)  //move to vector unknown representation
        {
          mat = stM->scalar_entries_p;
          if(mat==nullptr) error("not_computed_term", stM->name());
          stM->entries_p = new MatrixEntry(*mat); //create entries_p
          stM->toVectorUnknown();
          mat = stM->entries_p;
        }
      if(stM->entries_p->nbOfComponents.first != valdim.first)
        error("entry_mismatch_dims", stM->entries_p->nbOfComponents.first, valdim.first);
      if(stM->entries_p->nbOfComponents.second != valdim.second)
        error("entry_mismatch_dims", stM->entries_p->nbOfComponents.second, valdim.second);
      if(sym != _noSymmetry && sym != stM->symmetry()) sym = _noSymmetry;
      if(sto != stM->storageType()) sto = _cs;   //if storage types are different we choose compressed sparse as storage
      if(na == "")
        {
          if(a!=complex_t(1.,0.))
            {
              if(a.imag() == 0) name_ = tostring(a.real());
              else name_ = tostring(a);
              name_ += " * ( " + stM->name() + " )" ;
            }
          else name_ += " " + stM->name();
        }
    }
  if(lc.coefType() == _complex) vt = _complex;
  // create largest space and subspace numberings related to this largest space
  space_u_p = mergeSubspaces(spaces_u, true);
  space_v_p = mergeSubspaces(spaces_v, true);

  //find storage or create a new one
  number_t denseSize = (space_u_p->dimSpace())*(space_v_p->dimSpace());
  for(LcTerm<SuTermMatrix>::const_iterator itt = lc.begin(); itt != lc.end() && sto!=_dense; itt++)
    {
      stM = const_cast<SuTermMatrix*>(itt->first);
      if(stM->storagep()->size() > 0.25 * denseSize) sto=_dense;
    }
  AccessType at = _dual;
  if(sym != _noSymmetry) at = _sym;
  if(space_u_p != space_v_p) at = _row;

  MatrixStorage* sto_p = nullptr;
  if(sto == _dense) sto_p = buildStorage(*space_v_p, *space_u_p, sto, at,_feBuild);  //build or retry a storage
  else  // merge storage if required
  {
    number_t dimu=space_u_p->nbDofs(), dimv=space_v_p->nbDofs();
    its_u = spaces_u.begin(); its_v = spaces_v.begin();
    std::vector<std::set<number_t> > colIndices(space_v_p->nbDofs(),std::set<number_t>());
    for(LcTerm<SuTermMatrix>::const_iterator itt = lc.begin(); itt != lc.end(); itt++, its_u++, its_v++)
    {
      stM = const_cast<SuTermMatrix*>(itt->first);
      MatrixStorage* stoM=stM->storagep();
      Space* sp_u = *its_u;
      Space* sp_v = *its_v;
      std::vector<number_t> num_u, num_v;
      number_t nv=dimv;
      if(sp_u != space_u_p && sp_u->typeOfSpace() == _subSpace)  num_u = sp_u->subSpace()->dofNumbers();
      if(sp_v != space_v_p && sp_v->typeOfSpace() == _subSpace) {num_v = sp_v->subSpace()->dofNumbers(); nv=num_v.size();}
      for(number_t r=0;r<nv; r++)
      {
        std::set<number_t> colr=stoM->getCols(r+1);
        for(std::set<number_t>::iterator its=colr.begin();its!=colr.end();++its)
         {
            number_t rr=r, cc=*its;
            if(num_v.size()!=0) rr=num_v[r]-1;
            if(num_u.size()!=0) cc=num_u[*its-1];
            colIndices[rr].insert(cc);
         }
      }
    }
    std::stringstream ss; ss<<space_v_p<<"-"<<space_u_p;
    sto_p = buildStorage(sto, at, _feBuild, dimv, dimu, colIndices,ss.str());
  }

  //allocate entries of current SuTermMatrix
  entries_p = new MatrixEntry(vt, st, sto_p, valdim, sym);

  //do linear combination
  its_u = spaces_u.begin();
  its_v = spaces_v.begin();
  for(LcTerm<SuTermMatrix>::const_iterator itt = lc.begin(); itt != lc.end(); itt++, its_u++, its_v++)
    {
      const SuTermMatrix* stM = dynamic_cast<const SuTermMatrix*>(itt->first);
      Space* sp_u = *its_u;
      std::vector<number_t> num_u, num_v;
      if(sp_u != space_u_p && sp_u->typeOfSpace() == _subSpace) num_u = sp_u->subSpace()->dofNumbers();
      Space* sp_v = *its_v;
      if(sp_v != space_v_p && sp_v->typeOfSpace() == _subSpace) num_v = sp_v->subSpace()->dofNumbers();
      if(vt == _real) entries_p->add(*stM->entries_p, num_v, num_u, itt->second.real());    //add entries to current entries
      else entries_p->add(*stM->entries_p, num_v, num_u, itt->second);
    }

  //may be it is best to delete new subspaces created here (to be done later)
  computed() = true;
  termType_ = _sutermMatrix;

  //SuBilinearForm informations are no more managed, clear them. May be later it should be necessary to deal with
  if(subf_p != nullptr) delete subf_p;
  subf_p = nullptr;
  subspaces_u.clear();
  subspaces_v.clear();
  bool hasScalar=false;
  for(LcTerm<SuTermMatrix>::const_iterator itt = lc.begin(); itt != lc.end(); itt++)
    {
      stM = const_cast<SuTermMatrix*>(itt->first);
      if(stM->scalar_entries_p!=nullptr && stM->scalar_entries_p!=stM->entries_p) // remove entries_p that has been allocated
        {
          delete stM->entries_p;
          stM->entries_p=nullptr;
          hasScalar=true;
        }
    }
  if(hasScalar) toScalar(); //move result to scalar representation (perhaps useless)

  trace_p->pop();
}
//------------------------------------------------------------------------------
//                           OLD VERSION
//------------------------------------------------------------------------------
//void SuTermMatrix::compute(const LcTerm<SuTermMatrix>& lc, const string_t& na)
//{
//  trace_p->push("SuTermMatrix::compute(LcTerm)");
//  clear();
//  if(lc.size() == 0) error("is_void", "LcTerm");
//  if(lc.size() == 1)   //copy and multiplication
//    {
//      const SuTermMatrix* sut = dynamic_cast<const SuTermMatrix*>(lc.begin()->first);
//      if(sut == nullptr) error("null_pointer", "SuTermMatrix");
//      if(!sut->computed()) error("not_computed_term", sut->name());
//      copy(*sut);
//      name_ = na;
//      complex_t a = lc.begin()->second;
//      if(name_ == "") name_ = tostring(a) + " * ( " + sut->name() + " )" ;   //set default name
//      if(a != complex_t(1., 0.)) *this *= a;
//      computed() = true;
//      trace_p->pop();
//      return;
//    }
//
//  // Analyze SuTermMatrix to add,
//  // if some are related to vector unknown but in scalar representation, move them back to vector representation
//  // and move the result to scalar representation
//  std::vector<Space*> spaces_u(lc.size(), 0);
//  std::vector<Space*> spaces_v(lc.size(), 0);
//  std::vector<Space*>::iterator its_u = spaces_u.begin();
//  std::vector<Space*>::iterator its_v = spaces_v.begin();
//  SuTermMatrix* stM = const_cast<SuTermMatrix*>(lc.begin()->first);
//  if(stM == nullptr) error("null_pointer", "SuTermMatrix");
//  MatrixEntry* mat=stM->entries_p;
//  if(mat == nullptr)  //move to vector unknown representation
//    {
//      mat = stM->scalar_entries_p;
//      if(mat==nullptr) error("not_computed_term", stM->name());
//      stM->entries_p = new MatrixEntry(*mat); //create entries_p
//      stM->toVectorUnknown();
//      mat = stM->entries_p;
//    }
//  ValueType vt = stM->valueType();
//  StrucType st = stM->strucType();
//  u_p = stM->u_p;
//  v_p = stM->v_p;
//  SymType sym = stM->symmetry();
//  StorageType sto = stM->storageType();
//  dimPair valdim = mat->nbOfComponents;
//  complex_t a = lc.begin()->second;
//  name_ = na;
//  if(na == "")
//    {
//      if(a!=complex_t(1.,0.))
//        {
//          if(a.imag() == 0) name_ = tostring(a.real());
//          else name_ = tostring(a);
//          name_ += " * ( " + stM->name() + " )" ;
//        }
//      else name_ += " " + stM->name();
//    }
//  for(LcTerm<SuTermMatrix>::const_iterator itt = lc.begin(); itt != lc.end(); itt++, its_u++, its_v++)
//    {
//      stM = const_cast<SuTermMatrix*>(itt->first);
//      if(stM == nullptr) error("null_pointer", "SuTermMatrix");
//      *its_u = stM->space_u_p; // list of (sub)spaces
//      *its_v = stM->space_v_p;
//      if(stM->valueType() == _complex) vt = _complex;
//      if(stM->strucType() != st) error("term_mismatch_structures", words("structure",stM->strucType()),words("structure",st));
//      mat=stM->entries_p;
//      if(mat == nullptr)  //move to vector unknown representation
//        {
//          mat = stM->scalar_entries_p;
//          if(mat==nullptr) error("not_computed_term", stM->name());
//          stM->entries_p = new MatrixEntry(*mat); //create entries_p
//          stM->toVectorUnknown();
//          mat = stM->entries_p;
//        }
//      if(stM->entries_p->nbOfComponents.first != valdim.first)
//        error("entry_mismatch_dims", stM->entries_p->nbOfComponents.first, valdim.first);
//      if(stM->entries_p->nbOfComponents.second != valdim.second)
//        error("entry_mismatch_dims", stM->entries_p->nbOfComponents.second, valdim.second);
//      if(sym != _noSymmetry && sym != stM->symmetry()) sym = _noSymmetry;
//      if(sto != stM->storageType()) sto = _cs;   //if storage types are different we choose compressed sparse as storage
//      if(na == "")
//        {
//          if(a!=complex_t(1.,0.))
//            {
//              if(a.imag() == 0) name_ = tostring(a.real());
//              else name_ = tostring(a);
//              name_ += " * ( " + stM->name() + " )" ;
//            }
//          else name_ += " " + stM->name();
//        }
//    }
//
//  if(lc.coefType() == _complex) vt = _complex;
//  // create largest space and subspace numberings related to this largest space
//  space_u_p = mergeSubspaces(spaces_u, true);
//  space_v_p = mergeSubspaces(spaces_v, true);
//
//  //find storage or create a new one
//  number_t denseSize = (space_u_p->dimSpace())*(space_v_p->dimSpace());
//  for(LcTerm<SuTermMatrix>::const_iterator itt = lc.begin(); itt != lc.end() && sto!=_dense; itt++)
//    {
//      stM = const_cast<SuTermMatrix*>(itt->first);
//      if(stM->storagep()->size() > 0.25 * denseSize) sto=_dense;
//    }
//  AccessType at = _dual;
//  if(sym != _noSymmetry) at = _sym;
//  if(space_u_p != space_v_p) at = _row;
//
//  MatrixStorage* sto_p = buildStorage(*space_v_p, *space_u_p, sto, at);  //build or retry a storage
//
//  if(sto != _dense)   //modify storage if required (IE SuTermMatrix for instance)
//    {
//      bool modifiedStorage = false;
//      for(LcTerm<SuTermMatrix>::const_iterator itt = lc.begin(); itt != lc.end(); itt++)
//        {
//          stM = const_cast<SuTermMatrix*>(itt->first);
//          if(stM->storageType()==_dense)
//            {
//              if(!modifiedStorage)    //copy storage sto_p in a new one to prevent overriding storage
//                {
//                  sto_p=sto_p->clone();
//                  modifiedStorage=true;
//                }
//
//              std::vector<number_t> dofu, dofv;
//              Space* spu=stM->space_up(), *spv=stM->space_vp();
//              if(spu->typeOfSpace() == _subSpace && spu!=space_u_p) dofu = spu->subSpace()->dofNumbers();
//              else
//                {
//                  dofu.resize(spu->nbDofs());
//                  number_t k = 1;
//                  std::vector<number_t>::iterator it;
//                  for(it = dofu.begin(); it != dofu.end(); it++, k++) *it = k;
//                }
//              if(spu==spv) sto_p->addSubMatrixIndices(dofu, dofu);
//              else
//                {
//                  if(spv->typeOfSpace() == _subSpace && spv!=space_v_p) dofv = spv->subSpace()->dofNumbers();
//                  else
//                    {
//                      dofv.resize(spv->nbDofs());
//                      number_t k = 1;
//                      std::vector<number_t>::iterator it;
//                      for(it = dofv.begin(); it != dofv.end(); it++, k++) *it = k;
//                    }
//                  sto_p->addSubMatrixIndices(dofv, dofu);
//                }
//            }
//        }
//    }
//
//  //allocate entries of current SuTermMatrix
//  entries_p = new MatrixEntry(vt, st, sto_p, valdim, sym);
//
//  //do linear combination
//  its_u = spaces_u.begin();
//  its_v = spaces_v.begin();
//  for(LcTerm<SuTermMatrix>::const_iterator itt = lc.begin(); itt != lc.end(); itt++, its_u++, its_v++)
//    {
//      const SuTermMatrix* stM = dynamic_cast<const SuTermMatrix*>(itt->first);
//      Space* sp_u = *its_u;
//      std::vector<number_t> num_u, num_v;
//      if(sp_u != space_u_p && sp_u->typeOfSpace() == _subSpace) num_u = sp_u->subSpace()->dofNumbers();
//      Space* sp_v = *its_v;
//      if(sp_v != space_v_p && sp_v->typeOfSpace() == _subSpace) num_v = sp_v->subSpace()->dofNumbers();
//      if(vt == _real) entries_p->add(*stM->entries_p, num_v, num_u, itt->second.real());    //add entries to current entries
//      else entries_p->add(*stM->entries_p, num_v, num_u, itt->second);
//    }
//
//  //may be it is best to delete new subspaces created here (to be done later)
//  computed() = true;
//  termType_ = _sutermMatrix;
//
//  //SuBilinearForm informations are no more managed, clear them. May be later it should be necessary to deal with
//  if(subf_p != nullptr) delete subf_p;
//  subf_p = nullptr;
//  subspaces_u.clear();
//  subspaces_v.clear();
//  bool hasScalar=false;
//  for(LcTerm<SuTermMatrix>::const_iterator itt = lc.begin(); itt != lc.end(); itt++)
//    {
//      stM = const_cast<SuTermMatrix*>(itt->first);
//      if(stM->scalar_entries_p!=nullptr && stM->scalar_entries_p!=stM->entries_p) // remove entries_p that has been allocated
//        {
//          delete stM->entries_p;
//          stM->entries_p=nullptr;
//          hasScalar=true;
//        }
//    }
//  if(hasScalar) toScalar(); //move result to scalar representation (perhaps useless)
//
//  trace_p->pop();
//}


/*! move to vector unknown representation if SuTermMatrix has component unknown
    Note: the SutermMatrix is modified its unknowns too
*/
void SuTermMatrix::toVectorUnknown()
{
  if(u_p==nullptr || v_p==nullptr) return;
  if(!u_p->isComponent() && !v_p->isComponent()) return;
  trace_p->push("SuTermMatrix::toVectorUnknown()");

  const Unknown* u=u_p;
  if(u_p->isComponent()) u=u_p->parent();
  const Unknown* v=v_p;
  if(v_p->isComponent()) v=v_p->parent();
  dimen_t nbcv=v->nbOfComponents(), nbcu=u->nbOfComponents();
  std::vector<dimen_t> rowPos(nbcv,0), colPos(nbcu,0);
  if(u_p->isComponent()) colPos[u_p->componentIndex()-1]=u_p->componentIndex();
  else for(dimen_t i=0; i<colPos.size(); i++) colPos[i]=i+1;
  if(v_p->isComponent()) rowPos[v_p->componentIndex()-1]=v_p->componentIndex();
  else for(dimen_t i=0; i<rowPos.size(); i++) rowPos[i]=i+1;
  entries_p->toMatrix(rowPos,colPos);
  u_p=u; v_p=v;
  trace_p->pop();
}

// manage unknowns, testfunctions
/*! change unknown (col unknown) to an other
    be cautious: nothing is recomputed, bilinear form is not updated, numbering remains the same. Be sure what you are doing !!!
    emit warnings if something seems to be hazardous
*/
void SuTermMatrix::changeUnknown(const Unknown& newu)
{
    if(u_p==&newu) return;
    if(u_p->space()!=newu.space()) warning("free_warning","space of new Unknown is different from space of current Unknown");
    u_p=&newu;
    std::vector<DofComponent>::iterator it=cdofs_u.begin();
    for(;it!=cdofs_u.end();++it) it->u_p=&newu;
    for(it=orgcdofs_u.begin();it!=orgcdofs_u.end();++it) it->u_p=&newu;
}

/*! change test function (csay row unknown) to an other
    be cautious: nothing is recomputed, bilinear form is not updated, numbering remains the same. Be sure what you are doing !!!
    emit warnings if something seems to be hazardous
*/
void SuTermMatrix::changeTestFunction(const Unknown& newv)
{
    if(v_p==&newv) return;
    if(v_p->space()!=newv.space()) warning("free_warning","space of new Unknown is different from space of current Unknown");
    v_p=&newv;
    std::vector<DofComponent>::iterator it=cdofs_v.begin();
    for(;it!=cdofs_v.end();++it) it->u_p=&newv;
    for(it=orgcdofs_v.begin();it!=orgcdofs_v.end();++it) it->u_p=&newv;
}

/*! change unknown and test function to others
    be cautious: nothing is recomputed, bilinear form is not updated, numbering remains the same. Be sure what you are doing !!!
    emit warnings if something seems to be hazardous
*/
void SuTermMatrix::changeUnknowns(const Unknown& newu, const Unknown& newv)
{
    changeUnknown(newu);
    changeTestFunction(newv);
}

//initialize a SuTermVector consistent with matrix column unknown (col=true), with matrix row unknown (col=false)
void SuTermMatrix::initSuTermVector(SuTermVector& tv, ValueType vt, bool col) const
{
  string_t na = tv.name(); //preserving only name
  tv.clear();
  const Unknown* u = u_p;
  if(!col) u=v_p;
  Space* sp = space_up();
  if(!col) sp = space_vp();
  dimen_t nv = u->nbOfComponents();
  number_t n = sp->dimSpace();
  tv= SuTermVector(na, u, sp, vt, n, nv);
}

/* -----------------------------------------------------------------------------------------------------------
   merge a list of SuTermMatrix's referring to same vector unknown(s)
   The SuTermMatrix's in list MUST have the same vector unknown in row/col
   It produces a new SuTermMatrix if at least one SuTermMatrix has a component unknown as unknown
   and if not the case, nothing is done and it returns a null pointer
   NOTE: to merge SuTermMatrix's with different numbering use other functions
 -----------------------------------------------------------------------------------------------------------*/
SuTermMatrix* mergeSuTermMatrix(const std::list<SuTermMatrix*>& suts)
{
  if(suts.size()==0) return nullptr;
  trace_p->push("mergeSuTermMatrix(list<SuTermMatrix*>");

  bool toMerge=false;
  SuTermMatrix* msut=nullptr;
  std::list<SuTermMatrix*>::const_iterator itl=suts.begin();
  SuTermMatrix* sut=*itl;
  const Unknown* u=sut->u_p, *v=sut->v_p;
  if(u->isComponent()) {u=u->parent(); toMerge=true;}
  if(v->isComponent()) {v=v->parent(); toMerge=true;}
  if(toMerge) sut->toVectorUnknown();     //go to vector unknown representation
  itl++;
  for(; itl!=suts.end(); itl++)
    {
      sut=*itl;
      const Unknown* up=sut->u_p, *vp=sut->v_p;
      const Unknown* upar=up, *vpar=vp;
      if(up->isComponent()) upar= up->parent();
      if(vp->isComponent()) vpar= vp->parent();
      if(upar!=u) error("term_mismatch_unknowns", upar->name(), u->name());
      if(vpar!=v) error("term_mismatch_unknowns", vpar->name(), v->name());
      if(up->isComponent() || vp->isComponent())
        {
          toMerge=true;
          sut->toVectorUnknown();   //go to vector unknown representation
        }
    }
  if(toMerge)
    {
      LcTerm<SuTermMatrix> lct("SuTermMatrix");
      for(itl=suts.begin(); itl!=suts.end(); itl++) lct.push_back(**itl,1.);
      msut=new SuTermMatrix(u,0,v,0,0);
      msut->compute(lct);
    }

  trace_p->pop();
  return msut;
}

// -----------------------------------------------------------------------------------------------------------
// get real/imaginary part or move from real/complex to complex/real representation
// -----------------------------------------------------------------------------------------------------------
SuTermMatrix& SuTermMatrix::toReal()
{
  if(!computed())
    {
      warning("free_warning","SuTermMatrix::toReal does nothing, because it is not computed");
      return *this;
    }
  if(valueType()==_real) return *this;   //nothing to do
  if(entries_p!=nullptr) entries_p->toReal();
  if(scalar_entries_p!=nullptr) scalar_entries_p->toReal();
  return *this;
}

SuTermMatrix& SuTermMatrix::toImag()
{
  if(!computed())
    {
      warning("free_warning","SuTermMatrix::toImag does nothing, because it is not computed");
      return *this;
    }
  if(valueType()==_real)   //reset to 0 !!!
    {
      warning("free_warning","SuTermMatrix::toImag on a real SuTermMatrix gives a 0 SuTermMatrix !");
      if(entries_p!=nullptr) *entries_p *= 0.;
      if(scalar_entries_p!=nullptr) *scalar_entries_p *= 0.;
      return *this;
    }
  if(entries_p!=nullptr) entries_p->toReal(false);
  if(scalar_entries_p!=nullptr) scalar_entries_p->toReal(false);
  return *this;
}

SuTermMatrix& SuTermMatrix::toComplex()
{
  if(!computed()) warning("free_warning","SuTermMatrix::toComplex does nothing, because it is not computed");
  if(valueType()==_complex) return *this;   //nothing to do
  if(entries_p!=nullptr) entries_p->toComplex();
  if(scalar_entries_p!=nullptr) scalar_entries_p->toComplex();
  return *this;
}

SuTermMatrix& SuTermMatrix::toConj()
{
  if(!computed()) warning("free_warning","SuTermMatrix::toConj does nothing, because it is not computed");
  if(valueType()==_real) return *this;   //nothing to do
  if(entries_p!=nullptr) entries_p->toConj();
  if(scalar_entries_p!=nullptr) scalar_entries_p->toConj();
  return *this;
}

// round to zero all coefficients close to 0 (|.| < aszero), storage is not changed
SuTermMatrix& SuTermMatrix::roundToZero(real_t aszero)
{
  if(!computed()) warning("free_warning","SuTermMatrix::roundToZero does nothing, because it is not computed");
  if(entries_p!=nullptr) entries_p->roundToZero(aszero);
  if(scalar_entries_p!=nullptr) scalar_entries_p->roundToZero(aszero);
  return *this;
}

// -----------------------------------------------------------------------------------------------------------
// value managment
// -----------------------------------------------------------------------------------------------------------
/*! access to the matrix coefficient (i,j) >= 1 in the scalar/vector row-col numbering
    return a scalar value if a scalar matrix, a matrix value if a matrix of matrices
    see getScalarValue to get scalar coefficient
    if (i,j) is out of storage return 0
*/
Value SuTermMatrix::getValue(number_t i, number_t j) const
{
  number_t nbr=0, nbc=0;
  if(entries_p!=nullptr)
    {
      nbr=entries_p->nbOfRows(); nbc=entries_p->nbOfCols();
      if(i==0 || j==0 || i>nbr || j>nbc) error("index_out_of_range",i,j);
      return entries_p->getEntry(i,j);
    }
  if(scalar_entries_p!=nullptr)
    {
      nbr=scalar_entries_p->nbOfRows(); nbc=scalar_entries_p->nbOfCols();
      if(i==0 || j==0 || i>nbr || j>nbc) error("index_out_of_range",i,j);
      return scalar_entries_p->getEntry(i,j);
    }
  if(hm_entries_p!=nullptr)
    error("free_error","direct access to coefficients is not yet available for HMatrix");
  return Value(0.);
}

/*! access to the matrix coefficient (i,j) >= 1
    when a matrix of of m x n matrices
       if(k!=0) returns A(i,j)(k,l) so i,j are indexes in the vector numberings !
       else i,j are indexes in the scalar numberings and function returns A(I,J)(k,l)
            with I=(i-1)/m + 1, k=(i-1)%m +1 , J=(j-1)/n + 1, l=(j-1)%n +1
*/
Value SuTermMatrix::getScalarValue(number_t i, number_t j, dimen_t k, dimen_t l) const
{
  number_t nbr=0, nbc=0;
  if(scalar_entries_p!=nullptr)
    {
      nbr=scalar_entries_p->nbOfRows(); nbc=scalar_entries_p->nbOfCols();
      if(i==0 || j==0 || i>nbr || j>nbc) error("index_out_of_range",i,j);
      return scalar_entries_p->getEntry(i,j);
    }
  if(entries_p!=nullptr)
    {
      nbr=entries_p->nbOfRows(); nbc=entries_p->nbOfCols();
      if(entries_p->strucType()==_scalar)
        {
          if(i==0 || j==0 || i>nbr || j>nbc) error("index_out_of_range",i,j);
          return entries_p->getEntry(i,j);
        }
      dimen_t m=entries_p->nbOfComponents.first,  n=entries_p->nbOfComponents.second;
      if(k!=0)
        {
          if(i==0 || j==0 || i>nbr || j>nbc) error("index_out_of_range",k,l);
          if(l==0 || k>m || l>n) error("index_out_of_range",i,j);
          return entries_p->getEntry(i,j,k,l);
        }
      number_t i1=i-1, j1=j-1, I=(i1/m)+1, J=(j1/n)+1;
      k=(i1%m)+1; l=(j1%n)+1;
      if(i==0 || j==0 || I>nbr || J>nbc || k>m || l>n) error("index_out_of_range",i,j);
      return entries_p->getEntry(I,J,k,l);
    }
  if(hm_entries_p!=nullptr)
    error("free_error","direct access to coefficients is not yet available for HMatrix");
  return Value(0.);
}

/*! set value to matrix coefficient (i,j >= 1) in storage,
    Value must be of the same type of the matrix coefficient
    (i,j) must be in the matrix storage else an error occurs
    see getScalarValue to set a scalar coefficient of a matrix of matrices
*/
void SuTermMatrix::setValue(number_t i, number_t j, const Value& v)
{
  number_t nbr=0, nbc=0;
  if(entries_p!=nullptr)
    {
      nbr=entries_p->nbOfRows(); nbc=entries_p->nbOfCols();
      if(i==0 || j==0 || i>nbr || j>nbc) error("index_out_of_range",i,j);
      entries_p->setEntry(i,j,v,true);
      return;
    }
  if(scalar_entries_p!=nullptr)
    {
      nbr=scalar_entries_p->nbOfRows(); nbc=scalar_entries_p->nbOfCols();
      if(i==0 || j==0 || i>nbr || j>nbc) error("index_out_of_range",i,j);
      scalar_entries_p->setEntry(i,j,v,true);
      return;
    }
  if(hm_entries_p!=nullptr)
    error("free_error","direct access to coefficients is not yet available for HMatrix");
  error("free_error","SuTermMatrix::setValue, no available matrix entries");
}

/*! set matrix coefficient (i,j) >= 1 from a value
    when a matrix of of m x n matrices
       if(k!=0) set A(i,j)(k,l) so i,j are indexes in the vector numberings !
       else i,j are indexes in the scalar numberings and function sets A(I,J)(k,l)
            with I=(i-1)/m + 1, k=(i-1)%m +1 , J=(j-1)/n + 1, l=(j-1)%n +1
*/
void  SuTermMatrix::setScalarValue(number_t i, number_t j, const Value& v, dimen_t k, dimen_t l)
{
  number_t nbr=0, nbc=0;
  if(scalar_entries_p!=nullptr)
    {
      nbr=scalar_entries_p->nbOfRows(); nbc=scalar_entries_p->nbOfCols();
      if(i==0 || j==0 || i>nbr || j>nbc) error("index_out_of_range",i,j);
      scalar_entries_p->setEntry(i,j,v);
      return;
    }
  if(entries_p!=nullptr)
    {
      nbr=entries_p->nbOfRows(); nbc=entries_p->nbOfCols();
      if(entries_p->strucType()==_scalar) // scalar !
        {
          if(i==0 || j==0 || i>nbr || j>nbc) error("index_out_of_range",i,j);
          entries_p->setEntry(i,j,v);
          return;
        }
      dimen_t m=entries_p->nbOfComponents.first,  n=entries_p->nbOfComponents.second;
      if(k!=0)
        {
          if(i==0 || j==0 || i>nbr || j>nbc) error("index_out_of_range",k,l);
          if(l==0 || k>m || l>n) error("index_out_of_range",i,j);
          entries_p->setEntry(i,j,v,k,l);
        }
      number_t i1=i-1, j1=j-1, I=(i1/m)+1, J=(j1/n)+1;
      k=(i1%m)+1; l=(j1%n)+1;
      if(i==0 || j==0 || I>nbr || J>nbc || k>m || l>n) error("index_out_of_range",i,j);
      entries_p->setEntry(I,J,v,k,l);
    }
  if(hm_entries_p!=nullptr)
    error("free_error","direct access to coefficients is not yet available for HMatrix");
  error("free_error","SuTermMatrix::setValue, no available matrix entries");
}

/*! set values of a matrix rows r1->r2 (>= 1), c2=0 means nbrows,
    value has to be consistant with matrix structure
    no storage change
*/
void SuTermMatrix::setRow(const Value& val, number_t r1, number_t r2)
{
  if(val.strucType()==_scalar)
    {
      if(scalar_entries_p!=nullptr) scalar_entries_p->setRow(val,r1,r2);
      else if(entries_p!=nullptr && entries_p->strucType()==_scalar) entries_p->setRow(val,r1,r2);
      else error("free_error"," in SuTermMatrix::setRow, cannot set scalar value in a matrix of matrices");
      return;
    }
  if(entries_p!=nullptr && val.dims()==entries_p->nbOfComponents)
    {
      entries_p->setRow(val,r1,r2);
      return;
    }
  error("free_error"," in SuTermMatrix::setRow, given matrix value is not consistent with matrix entries");
}

/*! set values of a matrix cols c1->c2 (>= 1), c2=0 means nbrows, no storage change!*/
void  SuTermMatrix::setCol(const Value& val, number_t c1, number_t c2)
{
  if(val.strucType()==_scalar)
    {
      if(scalar_entries_p!=nullptr) scalar_entries_p->setCol(val,c1,c2);
      else if(entries_p!=nullptr && entries_p->strucType()==_scalar) entries_p->setCol(val,c1,c2);
      else error("free_error"," in SuTermMatrix::setCol, cannot set scalar value in a matrix of matrices");
      return;
    }
  if(entries_p!=nullptr && val.dims()==entries_p->nbOfComponents)
    {
      entries_p->setCol(val,c1,c2);
      return;
    }
  error("free_error"," in SuTermMatrix::setCol, given matrix value is not consistent with matrix entries");
}

//rank (>=1) of dof
number_t SuTermMatrix::rowRank(const Dof& dof) const
{
  if(v_p->space()== space_v_p) return dof.id();
  space_v_p->builddofid2rank();
  std::map<number_t, number_t>::const_iterator itm=space_v_p->dofid2rank().find(dof.id());
  if(itm==space_v_p->dofid2rank().end()) error("free_error"," dof not found in SuTermMatrix::rowRank(Dof)");
  return itm->second;
}

number_t SuTermMatrix::rowRank(const DofComponent& cdof) const
{
  if(v_p->space()== space_v_p) return cdof.dofnum*v_p->nbOfComponents()+cdof.numc-1;
  space_v_p->builddofid2rank();
  std::map<number_t, number_t>::const_iterator itm=space_v_p->dofid2rank().find(cdof.dofnum);
  if(itm==space_v_p->dofid2rank().end()) error("free_error"," dof not found in SuTermMatrix::rowRank(DofComponent)");
  return itm->second*v_p->nbOfComponents()+cdof.numc-1;
}

number_t SuTermMatrix::colRank(const Dof& dof) const
{
  if(u_p->space()== space_u_p) return dof.id();
  space_u_p->builddofid2rank();
  std::map<number_t, number_t>::const_iterator itm=space_u_p->dofid2rank().find(dof.id());
  if(itm==space_u_p->dofid2rank().end()) error("free_error"," dof not found in SuTermMatrix::colRank(Dof)");
  return itm->second;
}

number_t SuTermMatrix::colRank(const DofComponent& cdof) const
{
  if(u_p->space()== space_u_p) return cdof.dofnum*u_p->nbOfComponents()+cdof.numc-1;
  space_u_p->builddofid2rank();
  std::map<number_t, number_t>::const_iterator itm=space_u_p->dofid2rank().find(cdof.dofnum);
  if(itm==space_u_p->dofid2rank().end()) error("free_error"," dof not found in SuTermMatrix::colRank(DofComponent)");
  return itm->second*u_p->nbOfComponents()+cdof.numc-1;
}

// -----------------------------------------------------------------------------------------------------------
// norm of SuTermMatrix
// -----------------------------------------------------------------------------------------------------------
//Frobenius norm of a SuTermMatrix
real_t SuTermMatrix::norm2() const
{
  if(!computed())
    {
      warning("free_warning","SuTermMatrix::norm2 is not computed, return 0!");
      return 0.;
    }
  if(scalar_entries_p!=nullptr) return scalar_entries_p->norm2();
  if(entries_p!=nullptr) return entries_p->norm2();
  if(hm_scalar_entries_p!=nullptr) return hm_scalar_entries_p->norm2();
  if(hm_entries_p!=nullptr) return hm_entries_p->norm2();
  return 0.;
}

//infinite norm of a SuTermMatrix
real_t SuTermMatrix::norminfty() const
{
  if(!computed())
    {
      warning("free_warning","SuTermMatrix::norm2 is not computed, return 0!");
      return 0.;
    }
  if(scalar_entries_p!=nullptr) return scalar_entries_p->norminfty();
  if(entries_p!=nullptr) return entries_p->norminfty();
  if(hm_scalar_entries_p!=nullptr) return hm_scalar_entries_p->norminfty();
  if(hm_entries_p!=nullptr) return hm_entries_p->norminfty();
  return 0.;

}

// -----------------------------------------------------------------------------------------------------------
// print and save stuff
// -----------------------------------------------------------------------------------------------------------
//print storage on stream
void SuTermMatrix::viewStorage(std::ostream& out) const
{
  if(entries_p == nullptr && scalar_entries_p == nullptr) message("matrix_nostorage");
  else entries_p->viewStorage(out);
}

//print SuTermMatrix
void SuTermMatrix::print(std::ostream& out) const
{
  print(out, true);
}

void SuTermMatrix::print(std::ostream& out, bool header) const
{
  if(theVerboseLevel == 0) return;
  if(header) out << "SuTermMatrix " << name() << ", row unknown: "<<v_p->name()<<", col unknown: "<<u_p->name()<<eol;
  if(subf_p != nullptr) out << (*subf_p) << "\n";
  if(!computed())
    {
      out << words("not computed") << "\n";
      return;
    }
  if(theVerboseLevel < 2) return;
  if(entries_p == nullptr && scalar_entries_p==nullptr)
    {
      out << words("no entries") << "\n";
      return;
    }
  if(entries_p!=nullptr)
    {
      FactorizationType fact=factorization();
      if(fact!=_noFactorization) out <<words("factorized")<<" "<<words("factorization type", fact)<<"\n";
      out << words("firste") << " " << words("row") << " ids: ";
      number_t nbd=space_v_p->nbDofs();
      number_t m = std::min(number_t(theVerboseLevel),nbd);
      for(number_t i = 1; i <= m; i++)  out << i << "->" << space_v_p->dofId(i) << "  ";
      if(m < nbd) out<<"...";
      out <<eol<< words("firste") << " " << words("column") << " ids: ";
      nbd=space_u_p->nbDofs();
      m = std::min(number_t(theVerboseLevel), nbd);
      for(number_t i = 1; i <= m; i++) out << i << "->" << space_u_p->dofId(i) << ", ";
      if(m < nbd) out<<"...";
      out<<eol;
      out << (*entries_p)<<eol;
    }

  if(scalar_entries_p!=nullptr)
    {
      FactorizationType fact=factorization();
      if(fact!=_noFactorization) out <<words("factorized")<<" "<<words("factorization type", fact)<<"\n";
      if(orgcdofs_u.size()>0 || orgcdofs_v.size()>0) out<<"reduced matrix ";
      out << words("firste") << " " << words("row") << " cdofs: ";
      number_t nbd=cdofs_v.size();
      number_t m = std::min(number_t(theVerboseLevel),nbd);
      std::vector<DofComponent>::const_iterator itd=cdofs_v.begin();
      for(number_t i = 0; i < m; i++, itd++)  out  << *itd << "  ";
      if(m < nbd) out<<"...";
      out<<eol;
      out << words("firste") << " " << words("column") << " cdofs: ";
      nbd=cdofs_u.size();
      m = std::min(number_t(theVerboseLevel),nbd);
      itd=cdofs_u.begin();
      for(number_t i = 0; i < m; i++, itd++)  out  << *itd << "  ";
      if(m < nbd) out<<"...";
      out<<eol;
      if(scalar_entries_p!=entries_p) out << (*scalar_entries_p)<<eol;
    }

  if(rhs_matrix_p!=nullptr)
    {
      out<<"essential condition correction matrix: "<<*rhs_matrix_p<<eol;
    }
  out<<eol;
}

//print brief computation report
void SuTermMatrix::printSummary(std::ostream& out) const
{
  out << "  SuTermMatrix " << name() << " :   block ("<<v_p->name()<<", "<<u_p->name()<<") -> ";
  string_t typem="matrix ";
  if(hm_entries_p!=nullptr || hm_scalar_entries_p!=nullptr) typem="Hmatrix ";
  out << typem << numberOfRows() << " X " << numberOfCols() << " "
      << words("of") << " " << words("value",valueType()) << " " << words("structure",strucType());
  if(entries_p != nullptr) out << " " << words("in") << " " << storagep()->name()
                           << " (" << storagep()->size() << " " << words("coefficients") << ")" << eol;
  else if(scalar_entries_p != nullptr) out << " in scalar " << scalarStoragep()->name()
                                       << " (" << scalarStoragep()->size() << " " << words("coefficients") << ")" << eol;
  if(hm_entries_p!=nullptr) hm_entries_p->printSummary(out);
  else if(hm_scalar_entries_p!=nullptr) hm_scalar_entries_p->printSummary(out);
  if(orgcdofs_u.size()>0 || orgcdofs_v.size()>0) out<<" reduced matrix";
  if(rhs_matrix_p!=nullptr) out<<", has an essential condition correction matrix "<< rhs_matrix_p->nbOfRows()<<" X "<<rhs_matrix_p->nbOfCols()<< eol;
}

// save SuTermMatrix to file (dense or Matlab format)
void SuTermMatrix::saveToFile(const string_t& fn, StorageType st, number_t prec, bool encode, real_t tol) const
{
  if(entries_p != nullptr) entries_p->saveToFile(fn, st, prec, encode, tol);
  if(scalar_entries_p != nullptr) scalar_entries_p->saveToFile(fn, st, prec, encode, tol);
}

/*!
  Product of a SuTermMatrix M and a SuTermVector V when both have scalar_entries, the result is a SuTermVector R
  the columns dof indices of M, say (j1,j2, ...jn), may be different from the dof indices of V
  say (k1,k2, ... kp). The dof indices of result vector will be always the lines dof indices
  of the matrix, say (i1,i2, ...im).
  It means that the product M_i,j * V_j is performed only for j=k.
  - when {k1,k2, ... kp}={j1,j2, ...jn} the product is performed using largematrix product
  - when {k1,k2, ... kp} differs from {j1,j2, ...jn}, the vector V is extended and reduced to {j1,j2, ...jn}
    and the product is performed using largematrix product
*/
SuTermVector SuTermMatrix::multMVScalarEntries(const SuTermVector& sutV, bool toVector) const
{
  SuTermVector sutR("", this->vp(), this->space_vp()); // entries_p is not allocated !
  // number_t m=this->space_up()->dimSpace();
  number_t n=this->space_vp()->dimSpace();
  // number_t nu=this->up()->nbOfComponents();
  number_t nv=this->vp()->nbOfComponents();

  // renumbering
  std::vector<number_t> dofrenumber;
  if(sutV.up()== this->up()) dofrenumber = renumber(this->cdofsu(), sutV.cdofs());
  else                        dofrenumber = renumber(this->cdofsu(), dualDofComponents(sutV.cdofs()));
  bool extension = dofrenumber.size() != 0;

  if(sutV.valueType() == _real)   //V is real
    {
      Vector<real_t>* entriesV = sutV.scalar_entries()->rEntries_p;
      if(extension)
        {
          entriesV = new Vector<real_t>(this->cdofsv().size(), 0.);
          extendVector(dofrenumber, entriesV->begin(), sutV.scalar_entries()->rEntries_p->begin());
        }
      if(this->valueType() == _real)    //M is real
        {
          sutR.scalar_entries() = new VectorEntry(_real, 1, n*nv);
          *(sutR.scalar_entries()->rEntries_p) = *(this->scalar_entries()->rEntries_p) * *entriesV;
        }
      else // M is complex
        {
          sutR.scalar_entries() = new VectorEntry(_complex, 1, n*nv);
          *(sutR.scalar_entries()->cEntries_p) = *(this->scalar_entries()->cEntries_p) * *entriesV;
        }
      if(extension) delete entriesV;
    }
  else // V is complex
    {
      Vector<complex_t>* entriesV = sutV.scalar_entries()->cEntries_p;
      if(extension)
        {
          entriesV = new Vector<complex_t>(this->cdofsv().size(), complex_t(0.,0.));
          extendVector(dofrenumber, entriesV->begin(), sutV.scalar_entries()->cEntries_p->begin());
        }
      if(this->valueType() == _real)   //M is real, scalar
        {
          sutR.scalar_entries() = new VectorEntry(_complex, 1, n*nv);
          *(sutR.scalar_entries()->cEntries_p) = *(this->scalar_entries()->rEntries_p) * *entriesV;
        }
      else // M is complex, scalar
        {
          sutR.scalar_entries() = new VectorEntry(_complex, 1, n*nv);
          *(sutR.scalar_entries()->cEntries_p) = *(this->scalar_entries()->cEntries_p) * *entriesV;
        }
      if(extension) delete entriesV;
    }
  sutR.cdofs()=this->cdofsv();
  if(toVector)
    {
      sutR.toVector();
    }
  sutR.computed()=true;
  trace_p->pop();
  return sutR;
}

/*!
  Product of a SuTermMatrix M and a SuTermVector V when both have entries, the result is a SuTermVector R
  the columns dof indices of M, say (j1,j2, ...jn), may be different from the dof indices of V
  say (k1,k2, ... kp). The dof indices of result vector will be always the lines dof indices
  of the matrix, say (i1,i2, ...im).
  It means that the product M_i,j * V_j is performed only for j=k.
  - when {k1,k2, ... kp}={j1,j2, ...jn} the product is performed using largematrix product
  - when {k1,k2, ... kp} differs from {j1,j2, ...jn}, the vector V is extended and reduced to {j1,j2, ...jn}
    and the product is performed using largematrix product
*/
SuTermVector SuTermMatrix::multMVVectorEntries(const SuTermVector& sutV, bool toScalar) const
{
  SuTermVector sutR("", this->vp(), this->space_vp()); // entries_p is not allocated !
  number_t m=this->space_up()->dimSpace();
  number_t n=this->space_vp()->dimSpace();
  number_t nu=this->up()->nbOfComponents();
  number_t nv=this->vp()->nbOfComponents();

  //renumbering
  std::vector<number_t> dofrenumber = renumber(this->space_up(), sutV.spacep());
  bool extension = dofrenumber.size() != 0;

  if(nu == 1)  // V is scalar
    {
      if(sutV.valueType() == _real)  // V is real
        {
          Vector<real_t>* entriesV = sutV.entries()->rEntries_p;
          if(extension)
            {
              entriesV = new Vector<real_t>(m, 0.);
              extendVector(dofrenumber, entriesV->begin(), sutV.entries()->rEntries_p->begin());
            }
          if(nv == 1)  // M is scalar
            {
              if(this->valueType() == _real)  // M is real
                {
                  sutR.entries() = new VectorEntry(_real, nv, n);
                  *(sutR.entries()->rEntries_p) = *(this->entries()->rEntries_p) * *entriesV;
                }
              else // M is complex
                {
                  sutR.entries() = new VectorEntry(_complex, nv, n);
                  *(sutR.entries()->cEntries_p) = *(this->entries()->cEntries_p) * *entriesV;
                }
            }
          else // M is vector: nv x 1  *  1
            {
              // move entriesV to vector
              Vector<Vector<real_t> >* entriesVV = new Vector<Vector<real_t> >(m,Vector<real_t>(1,0.));
              Vector<real_t>::iterator itv=entriesV->begin();
              Vector<Vector<real_t> >::iterator itvv=entriesVV->begin();
              for(; itv!=entriesV->end(); ++itv, ++itvv) *itvv = Vector<real_t>(1,*itv);

              if(this->valueType() == _real)    //M is real
                {
                  sutR.entries() = new VectorEntry(_real, nv, n);
                  *(sutR.entries()->rvEntries_p) = *(this->entries()->rmEntries_p) * *entriesVV;
                }
              else  //M is complex
                {
                  sutR.entries() = new VectorEntry(_complex, nv, n);
                  *(sutR.entries()->cvEntries_p) = *(this->entries()->cmEntries_p) * *entriesVV;
                }
              delete entriesVV;
            }
          if(extension) delete entriesV;
        }
      else //V is complex
        {
          Vector<complex_t>* entriesV = sutV.entries()->cEntries_p;
          if(extension)
            {
              entriesV = new Vector<complex_t>(m, complex_t(0.,0.));
              extendVector(dofrenumber, entriesV->begin(), sutV.entries()->cEntries_p->begin());
            }
          if(nv == 1)  // M is scalar
            {
              if(this->valueType() == _real)   //M is real, scalar
                {
                  sutR.entries() = new VectorEntry(_complex, nv, n);
                  *(sutR.entries()->cEntries_p) = *(this->entries()->rEntries_p) * *entriesV;
                }
              else // M is complex, scalar
                {
                  sutR.entries() = new VectorEntry(_complex, nv, n);
                  *(sutR.entries()->cEntries_p) = *(this->entries()->cEntries_p) * *entriesV;
                }
            }
          else // M is vector
            {
              // move entriesV to vector
              Vector<Vector<complex_t> >* entriesVV = new Vector<Vector<complex_t> >(m, Vector<complex_t>(1,complex_t(0.)));
              Vector<complex_t>::iterator itv=entriesV->begin();
              Vector<Vector<complex_t> >::iterator itvv=entriesVV->begin();
              for(; itv!=entriesV->end(); itv++, itvv++) *itvv = Vector<complex_t>(1,*itv);

              if(this->valueType() == _real)   //M is real, vector
                {
                  sutR.entries() = new VectorEntry(_complex, nv, n);
                  *(sutR.entries()->cvEntries_p) = *(this->entries()->rmEntries_p) * *entriesVV;
                }
              else //M is complex, vector
                {
                  sutR.entries() = new VectorEntry(_complex, nv, n);
                  *(sutR.entries()->cvEntries_p) = *(this->entries()->cmEntries_p) * *entriesVV;
                }
              delete entriesVV;
            }
          if(extension) delete entriesV;
        }
    }
  else //V is vector and  M is vector
    {
      if(sutV.valueType() == _real)    //V is real
        {
          Vector<Vector<real_t> >* entriesV = sutV.entries()->rvEntries_p;
          if(extension)
            {
              entriesV = new Vector<Vector<real_t> >(m, Vector<real_t>(nu, 0.));
              extendVector(dofrenumber, entriesV->begin(), sutV.entries()->rvEntries_p->begin());
            }
          if(this->valueType() == _real)   //M is real
            {
              sutR.entries() = new VectorEntry(Vector<real_t>(nv, 0.), n);
              *sutR.entries()->rvEntries_p = *(this->entries()->rmEntries_p) * *entriesV;
            }
          else //M is complex
            {
              sutR.entries() = new VectorEntry(Vector<complex_t>(nv, complex_t(0., 0.)), n);
              *sutR.entries()->cvEntries_p = *(this->entries()->cmEntries_p) * *entriesV;
            }
          if(extension) delete entriesV;
        }
      else //V is complex
        {
          Vector<Vector<complex_t> >* entriesV = sutV.entries()->cvEntries_p;
          if(extension)
            {
              entriesV = new Vector<Vector<complex_t> >(m, Vector<complex_t>(nv, complex_t(0., 0.)));
              extendVector(dofrenumber, entriesV->begin(), sutV.entries()->cvEntries_p->begin());
            }
          if(this->valueType() == _real)    //M is real
            {
              sutR.entries() = new VectorEntry(Vector<complex_t>(nv, complex_t(0., 0.)), n);
              *sutR.entries()->cvEntries_p = *(this->entries()->rmEntries_p) * *entriesV;
            }
          else // M is complex
            {
              sutR.entries() = new VectorEntry(Vector<complex_t>(nv, complex_t(0., 0.)), n);
              *sutR.entries()->cvEntries_p = *(this->entries()->cmEntries_p) * *entriesV;
            }
          if(extension) delete entriesV;
        }

      if(nv == 1)  // goto scalar entries
        {
          if(sutR.valueType() == _real)
            {
              Vector<real_t>* entriesR = new Vector<real_t>(n, 0.);
              Vector<real_t>::iterator itr=entriesR->begin();
              Vector<Vector<real_t> >::iterator itvr=sutR.entries()->rvEntries_p->begin();
              for(; itr!=entriesR->end(); itr++, itvr++) *itr = (*itvr)(1);
              delete sutR.entries()->rvEntries_p;
              sutR.entries()->rvEntries_p=nullptr;
              sutR.entries()->rEntries_p=entriesR;
              sutR.entries()->strucType_=_scalar;
            }
          else
            {
              Vector<complex_t>* entriesR = new Vector<complex_t>(n, complex_t(0., 0.));
              Vector<complex_t>::iterator itr=entriesR->begin();
              Vector<Vector<complex_t> >::iterator itvr=sutR.entries()->cvEntries_p->begin();
              for(; itr!=entriesR->end(); itr++, itvr++) *itr = (*itvr)(1);
              delete sutR.entries()->cvEntries_p;
              sutR.entries()->cvEntries_p=nullptr;
              sutR.entries()->cEntries_p=entriesR;
              sutR.entries()->strucType_=_scalar;
            }
        }
    }
  if(toScalar)
    {
      sutR.toScalar();
    }
  sutR.computed()=true;
  trace_p->pop();
  return sutR;
}

/*!
  Product of a SuTermMatrix M and a SuTermVector V, the result is a SuTermVector R
  the columns dof indices of M, say (j1,j2, ...jn), may be different from the dof indices of V
  say (k1,k2, ... kp). The dof indices of result vector will be always the lines dof indices
  of the matrix, say (i1,i2, ...im).
  It means that the product M_i,j * V_j is performed only for j=k.
  - when {k1,k2, ... kp}={j1,j2, ...jn} the product is performed using largematrix product
  - when {k1,k2, ... kp} differs from {j1,j2, ...jn}, the vector V is extended and reduced to {j1,j2, ...jn}
    and the product is performed using largematrix product
*/
SuTermVector operator*(const SuTermMatrix& sutM, const SuTermVector& sutV)
{
  trace_p->push("SuTermMatrix * SuTermVector");
  if(!sutM.computed()) error("not_computed_term", sutM.name());
  const Unknown* uM=sutM.up(), *uV=sutV.up();
  if(uM != uV && uM != uV->dual_p())   //authorise with dual unknown - permissive
    error("term_inconsistent_unknowns");
  if(uV->nbOfComponents()!=uM->nbOfComponents())
    error("term_mismatch_nb_components", uV->nbOfComponents(),uM->nbOfComponents());
  if(sutM.entries() == nullptr && sutM.scalar_entries() && sutM.hm_entries() == nullptr && sutM.hm_scalar_entries() == nullptr)
    error("null_pointer", "sutM.xxEntries_p");
  if(sutV.entries() == nullptr && sutV.scalar_entries() == nullptr)
    error("null_pointer", "sutV.xxEntries_p");

  if(sutM.entries() != nullptr || sutM.scalar_entries()!=nullptr)  // standard matrix (not a HMatrix)
  {
    // first, we have to check if both vectors have the same entries structure
    // if no, we have to set v to the same structure, through a copy
    bool copyNeeded=false;
    bool vToScalar=false;
    bool vToVector=false;
    if(sutM.scalar_entries() != nullptr && sutM.scalar_entries() != sutM.entries() && sutV.scalar_entries() == nullptr) { copyNeeded=vToScalar=true; }
    else if(sutM.scalar_entries() == nullptr && sutV.scalar_entries() != nullptr && sutV.scalar_entries() != sutV.entries()) { copyNeeded=vToVector=true; }
    if(copyNeeded)
    {
      SuTermVector sutVcopy(sutV);
      if(vToScalar)
        {
          sutVcopy.toScalar();
          return sutM.multMVScalarEntries(sutVcopy, true);
        }
      if(vToVector)
        {
          sutVcopy.toVector();
          return sutM.multMVVectorEntries(sutVcopy, true);
        }
      }
    // first, we test if we have to work with scalar_entries
    if(sutM.scalar_entries() != nullptr && sutM.scalar_entries() != sutM.entries()) { return sutM.multMVScalarEntries(sutV); }
    return sutM.multMVVectorEntries(sutV);
  }

  // case of HMatrix only scalar case
  const HMatrixEntry<FeDof>* hme=sutM.hm_scalar_entries();
  if(hme==nullptr) hme=sutM.hm_entries();
  const VectorEntry* ve=sutV.scalar_entries();
  if(ve==nullptr) ve=sutV.entries();
  if(hme==nullptr || ve==nullptr)
     error("null_pointer", "HMatrix case : only scalar data");
  ValueType vtm=hme->valueType_;
  ValueType vtv=sutV.valueType();
  SuTermVector sutR("", sutM.vp(), sutM.space_vp()); // entries_p is not allocated !
  if(vtm==_real)
  {
     HMatrix<real_t,FeDof>& Hm=hme->getHMatrix<real_t>();
     if(vtv==_real)
     {
        Vector<real_t> mv=Hm*(*ve->rEntries_p);
        sutR.entries()  = new VectorEntry(mv);  //hard copy
        trace_p->pop();
        return sutR;
     }
     else // vtv complex
     {
        Vector<complex_t> mv=Hm*real(*ve->cEntries_p)+i_*(Hm*imag(*ve->cEntries_p));
        sutR.entries()  = new VectorEntry(mv);  //hard copy
        trace_p->pop();
        return sutR;
     }
  }
  else // vtm complex
  {
     HMatrix<complex_t,FeDof>& Hm=hme->getHMatrix<complex_t>();
     if(vtv==_complex)
     {
        Vector<complex_t> mv=Hm*(*ve->cEntries_p);
        sutR.entries()  = new VectorEntry(mv);  //hard copy
        trace_p->pop();
        return sutR;
     }
     else // vtv real
     {
        Vector<complex_t> mv=Hm*cmplx(*ve->rEntries_p);
        sutR.entries()  = new VectorEntry(mv);  //hard copy
        trace_p->pop();
        return sutR;
     }
  }
  trace_p->pop();
  return sutR;
}

/*!
  Product of a SuTermVector V and a SuTermMatrix M when both have scalar_entries, the result is a SuTermVector R
  the lines dof indices of M, say (i1,i2, ...in), may be different from the dof indices of V
  say (k1,k2, ... kp). The dof indices of result vector will be always the columns dof indices
  of the matrix, say (j1,j2, ...jm).
  It means that the product V_i * M_i,j is performed only for i=k.
  - when {k1,k2, ... kp}={i1,i2, ...in} the product is performed using largematrix product
  - when {k1,k2, ... kp} differs from {i1,i2, ...in}, the vector V is extended and reduced to {i1,i2, ...in}
    and the product is performed using largematrix product
*/
SuTermVector SuTermMatrix::multVMScalarEntries(const SuTermVector& sutV, bool toVector) const
{
  SuTermVector sutR("", this->up(), this->space_up()); // entries_p is not allocated !
  number_t m=this->space_up()->dimSpace();
  // number_t n=this->space_vp()->dimSpace();
  number_t nu=this->up()->nbOfComponents();
  // number_t nv=this->vp()->nbOfComponents();

  // renumbering
  std::vector<number_t> dofrenumber;
  if(sutV.up()== this->vp()) dofrenumber = renumber(this->cdofsv(), sutV.cdofs());
  else                       dofrenumber = renumber(this->cdofsv(), dualDofComponents(sutV.cdofs()));
  bool extension = dofrenumber.size() != 0;

  if(sutV.valueType() == _real)   //V is real
    {
      Vector<real_t>* entriesV = sutV.scalar_entries()->rEntries_p;
      if(extension)
        {
          entriesV = new Vector<real_t>(this->cdofsu().size(), 0.);
          extendVector(dofrenumber, entriesV->begin(), sutV.scalar_entries()->rEntries_p->begin());
        }
      if(this->valueType() == _real)    //M is real
        {
          sutR.scalar_entries() = new VectorEntry(_real, 1, m*nu);
          *(sutR.scalar_entries()->rEntries_p) = *entriesV * *(this->scalar_entries()->rEntries_p);
        }
      else // M is complex
        {
          sutR.scalar_entries() = new VectorEntry(_complex, 1, m*nu);
          *(sutR.scalar_entries()->cEntries_p) = *entriesV * *(this->scalar_entries()->cEntries_p);
        }
      if(extension) delete entriesV;
    }
  else // V is complex
    {
      Vector<complex_t>* entriesV = sutV.scalar_entries()->cEntries_p;
      if(extension)
        {
          entriesV = new Vector<complex_t>(this->cdofsu().size(), complex_t(0.,0.));
          extendVector(dofrenumber, entriesV->begin(), sutV.scalar_entries()->cEntries_p->begin());
        }
      if(this->valueType() == _real)   //M is real, scalar
        {
          sutR.scalar_entries() = new VectorEntry(_complex, 1, m*nu);
          *(sutR.scalar_entries()->cEntries_p) = *entriesV * *(this->scalar_entries()->rEntries_p);
        }
      else // M is complex, scalar
        {
          sutR.scalar_entries() = new VectorEntry(_complex, 1, m*nu);
          *(sutR.scalar_entries()->cEntries_p) = *entriesV * *(this->scalar_entries()->cEntries_p);
        }
      if(extension) delete entriesV;
    }
  sutR.cdofs()=this->cdofsu();
  if(toVector)
    {
      sutR.toVector();
    }
  sutR.computed()=true;
  trace_p->pop();
  return sutR;
}

/*!
  Product of a SuTermVector V and a SuTermMatrix M when both have entries, the result is a SuTermVector R
  the lines dof indices of M, say (i1,i2, ...in), may be different from the dof indices of V
  say (k1,k2, ... kp). The dof indices of result vector will be always the columns dof indices
  of the matrix, say (j1,j2, ...jm).
  It means that the product V_i * M_i,j is performed only for i=k.
  - when {k1,k2, ... kp}={i1,i2, ...in} the product is performed using largematrix product
  - when {k1,k2, ... kp} differs from {i1,i2, ...in}, the vector V is extended and reduced to {i1,i2, ...in}
    and the product is performed using largematrix product
*/
SuTermVector SuTermMatrix::multVMVectorEntries(const SuTermVector& sutV, bool toScalar) const
{
  SuTermVector sutR("", this->up(), this->space_up()); // entries_p is not allocated !
  number_t m=this->space_up()->dimSpace();
  number_t n=this->space_vp()->dimSpace();
  number_t nu=this->up()->nbOfComponents();
  number_t nv=this->vp()->nbOfComponents();

  //renumbering
  std::vector<number_t> dofrenumber = renumber(this->space_vp(), sutV.spacep());
  bool extension = dofrenumber.size() != 0;

  if(nv == 1)  // V is scalar
    {
      if(sutV.valueType() == _real)  // V is real
        {
          Vector<real_t>* entriesV = sutV.entries()->rEntries_p;
          if(extension)
            {
              entriesV = new Vector<real_t>(n, 0.);
              extendVector(dofrenumber, entriesV->begin(), sutV.entries()->rEntries_p->begin());
            }
          if(nu == 1)  // M is scalar
            {
              if(this->valueType() == _real)  // M is real
                {
                  sutR.entries() = new VectorEntry(_real, nu, m);
                  *(sutR.entries()->rEntries_p) = *entriesV * *(this->entries()->rEntries_p);
                }
              else // M is complex
                {
                  sutR.entries() = new VectorEntry(_complex, nu, m);
                  *(sutR.entries()->cEntries_p) = *entriesV * *(this->entries()->cEntries_p);
                }
            }
          else // M is vector: 1 * 1 x nu
            {
              // move entriesV to vector
              Vector<Vector<real_t> >* entriesVV = new Vector<Vector<real_t> >(n, Vector<real_t>(1,0.));
              Vector<real_t>::iterator itv=entriesV->begin();
              Vector<Vector<real_t> >::iterator itvv=entriesVV->begin();
              for(; itv!=entriesV->end(); ++itv, ++itvv) *itvv = Vector<real_t>(1,*itv);

              if(this->valueType() == _real)  // M is real
                {
                  sutR.entries() = new VectorEntry(_real, nu, m);
                  *(sutR.entries()->rvEntries_p) = *entriesVV * *(this->entries()->rmEntries_p);
                }
              else  //M is complex
                {
                  sutR.entries() = new VectorEntry(_complex, nu, m);
                  *(sutR.entries()->cvEntries_p) = *entriesVV * *(this->entries()->cmEntries_p);
                }
              delete entriesVV;
            }
          if(extension) delete entriesV;
        }
      else // V is complex
        {
          Vector<complex_t>* entriesV = sutV.entries()->cEntries_p;
          if(extension)
            {
              entriesV = new Vector<complex_t>(n, complex_t(0.,0.));
              extendVector(dofrenumber, entriesV->begin(), sutV.entries()->cEntries_p->begin());
            }
          if(nu == 1)  // M is scalar
            {
              if(this->valueType() == _real)  // M is real, scalar
                {
                  sutR.entries() = new VectorEntry(_complex, nu, m);
                  *(sutR.entries()->cEntries_p) = *entriesV * *(this->entries()->rEntries_p);
                }
              else // M is complex, scalar
                {
                  sutR.entries() = new VectorEntry(_complex, nu, m);
                  *(sutR.entries()->cEntries_p) = *entriesV * *(this->entries()->cEntries_p);
                }
            }
          else // M is vector
            {
              // move entriesV to vector
              Vector<Vector<complex_t> >* entriesVV = new Vector<Vector<complex_t> >(n, Vector<complex_t>(1,complex_t(0.)));
              Vector<complex_t>::iterator itv=entriesV->begin();
              Vector<Vector<complex_t> >::iterator itvv=entriesVV->begin();
              for(; itv!=entriesV->end(); itv++, itvv++) *itvv = Vector<complex_t>(1,*itv);

              if(this->valueType() == _real)   //M is real, vector
                {
                  sutR.entries() = new VectorEntry(_complex, nu, m);
                  *(sutR.entries()->cvEntries_p) = *entriesVV * *(this->entries()->rmEntries_p);
                }
              else //M is complex, vector
                {
                  sutR.entries() = new VectorEntry(_complex, nu, m);
                  *(sutR.entries()->cvEntries_p) = *entriesVV * *(this->entries()->cmEntries_p);
                }
              delete entriesVV;
            }
          if(extension) delete entriesV;
        }
    }
  else // V is vector and  M is vector
    {
      if(sutV.valueType() == _real)  // V is real
        {
          Vector<Vector<real_t> >* entriesV = sutV.entries()->rvEntries_p;
          if(extension)
            {
              entriesV = new Vector<Vector<real_t> >(n, Vector<real_t>(nv, 0.));
              extendVector(dofrenumber, entriesV->begin(), sutV.entries()->rvEntries_p->begin());
            }
          if(this->valueType() == _real)  // M is real
            {
              sutR.entries() = new VectorEntry(Vector<real_t>(nu, 0.), m);
              *sutR.entries()->rvEntries_p = *entriesV * *(this->entries()->rmEntries_p);
            }
          else // M is complex
            {
              sutR.entries() = new VectorEntry(Vector<complex_t>(nu, complex_t(0., 0.)), m);
              *sutR.entries()->cvEntries_p = *entriesV * *(this->entries()->cmEntries_p);
            }
          if(extension) delete entriesV;
        }
      else // V is complex
        {
          Vector<Vector<complex_t> >* entriesV = sutV.entries()->cvEntries_p;
          if(extension)
            {
              entriesV = new Vector<Vector<complex_t> >(n, Vector<complex_t>(nv, complex_t(0., 0.)));
              extendVector(dofrenumber, entriesV->begin(), sutV.entries()->cvEntries_p->begin());
            }
          if(this->valueType() == _real)  // M is real
            {
              sutR.entries() = new VectorEntry(Vector<complex_t>(nu, complex_t(0., 0.)), m);
              *sutR.entries()->cvEntries_p = *entriesV * *(this->entries()->rmEntries_p);
            }
          else // M is complex
            {
              sutR.entries() = new VectorEntry(Vector<complex_t>(nu, complex_t(0., 0.)), m);
              *sutR.entries()->cvEntries_p = *entriesV * *(this->entries()->cmEntries_p);
            }
          if(extension) delete entriesV;
        }

      if(nu == 1)  // goto scalar entries
        {
          if(sutR.valueType() == _real)
            {
              Vector<real_t>* entriesR = new Vector<real_t>(m, 0.);
              Vector<real_t>::iterator itr=entriesR->begin();
              Vector<Vector<real_t> >::iterator itvr=sutR.entries()->rvEntries_p->begin();
              for(; itr!=entriesR->end(); itr++, itvr++) *itr = (*itvr)(1);
              delete sutR.entries()->rvEntries_p;
              sutR.entries()->rvEntries_p=nullptr;
              sutR.entries()->rEntries_p=entriesR;
              sutR.entries()->strucType_=_scalar;
            }
          else
            {
              Vector<complex_t>* entriesR = new Vector<complex_t>(m, complex_t(0., 0.));
              Vector<complex_t>::iterator itr=entriesR->begin();
              Vector<Vector<complex_t> >::iterator itvr=sutR.entries()->cvEntries_p->begin();
              for(; itr!=entriesR->end(); itr++, itvr++) *itr = (*itvr)(1);
              delete sutR.entries()->cvEntries_p;
              sutR.entries()->cvEntries_p=nullptr;
              sutR.entries()->cEntries_p=entriesR;
              sutR.entries()->strucType_=_scalar;
            }
        }
    }
  if(toScalar)
    {
      sutR.toScalar();
    }

  sutR.computed()=true;
  trace_p->pop();
  return sutR;
}

/*!
  Product of a SuTermVector V and a SuTermMatrix M, the result is a SuTermVector R
  the lines dof indices of M, say (i1,i2, ...in), may be different from the dof indices of V
  say (k1,k2, ... kp). The dof indices of result vector will be always the columns dof indices
  of the matrix, say (j1,j2, ...jm).
  It means that the product V_i * M_i,j is performed only for i=k.
  - when {k1,k2, ... kp}={i1,i2, ...in} the product is performed using largematrix product
  - when {k1,k2, ... kp} differs from {i1,i2, ...in}, the vector V is extended and reduced to {i1,i2, ...in}
    and the product is performed using largematrix product

  exists only for particular storage type
*/
SuTermVector operator*(const SuTermVector& sutV, const SuTermMatrix& sutM)
{
  trace_p->push("SuTermVector * SuTermMatrix");
  if(!sutM.computed()) error("not_computed_term", sutM.name());
  const Unknown* vM=sutM.vp(), *uV=sutV.up();
  if(vM != uV && vM != uV->dual_p())   //authorise with dual unknown - permissive
    error("term_inconsistent_unknowns");
  if(uV->nbOfComponents() != vM->nbOfComponents())
    error("term_mismatch_nb_components", uV->nbOfComponents(),vM->nbOfComponents());
  if(sutM.entries() == nullptr && sutM.scalar_entries() == nullptr)
    error("null_pointer", "sutM.xxEntries_p");
  if(sutV.entries() == nullptr && sutV.scalar_entries() == nullptr)
    error("null_pointer", "sutV.xxEntries_p");

  // first, we have to check if both vectors have the same entries structure
  // if no, we have to set v to the same structure, through a copy
  bool copyNeeded=false;
  bool vToScalar=false;
  bool vToVector=false;
  if(sutM.scalar_entries() != nullptr && sutM.scalar_entries() != sutM.entries() && sutV.scalar_entries() == nullptr) { copyNeeded=vToScalar=true; }
  else if(sutM.scalar_entries() == nullptr && sutV.scalar_entries() != nullptr && sutV.scalar_entries() != sutV.entries()) { copyNeeded=vToVector=true; }

  if(copyNeeded)
    {
      SuTermVector sutVcopy(sutV);
      if(vToScalar)
        {
          sutVcopy.toScalar();
          return sutM.multVMScalarEntries(sutVcopy, true);
        }
      if(vToVector)
        {
          sutVcopy.toVector();
          return sutM.multVMVectorEntries(sutVcopy, true);
        }
    }

  // first, we test if we have to work with scalar_entries
  if(sutM.scalar_entries() != nullptr && sutM.scalar_entries() != sutM.entries()) { return sutM.multVMScalarEntries(sutV); }
  return sutM.multVMVectorEntries(sutV);
}

// product SuTermMatrix * SuTermMatrix
//   exist only for particular storage type (dense for instance)
//   product is done using scalar entries when standard entries are null
SuTermMatrix operator*(const SuTermMatrix& tA, const SuTermMatrix& tB)
{
  if(tA.up()!=tB.vp() && tA.up()!=tB.vp()->dual_p())
    {
      where("SuTermMatrix * SuTermMatrix");
      error("term_mismatch_unknowns",tA.up()->name(), tB.vp()->name());
    }
  string_t na=tA.name()+" x "+tB.name();
  SuTermMatrix tR(0,tB.u_p,tA.v_p,tB.space_u_p,tA.space_v_p,tB.subspaces_u,tA.subspaces_v,na,0);
  bool inScalar=false;
  const MatrixEntry* matA=tA.entries(), *matB=tB.entries();
  if(matA==nullptr || matB==nullptr) {matA=tA.scalar_entries(); matB=tB.scalar_entries(); inScalar=true;}
  if(matA==nullptr)
    {
      where("SuTermMatrix * SuTermMatrix");
      error("null_pointer","matA");
      return tR;    //null matrix
    }
  if(matB==nullptr)
    {
      where("SuTermMatrix * SuTermMatrix");
      error("null_pointer","matB");
      return tR;    //null matrix
    }
  tR.entries_p= new MatrixEntry(*matA * *matB);
  if(inScalar) {tR.scalar_entries_p = tR.entries_p; tR.entries_p=nullptr;}
  tR.computed()=true;
  return tR;
}

// factorize matrix as LU or LDLt or LDL*, preserving A
void factorize(SuTermMatrix& A, SuTermMatrix& Af, FactorizationType ft, bool withPermutation)
{
  trace_p->push("factorize(SuTermMatrix, SuTermMatrix, ...");
  if(!A.computed()) error("not_computed_term", A.name());
  if(&Af != &A)
    {
      Af.clear();    //copy A to Af
      Af.copy(A);
    }
  if(Af.space_up() != Af.space_vp()) error("term_incompatible_spaces");
  if(A.strucType()==_matrix) A.toScalar();
  MatrixEntry* mat = A.scalar_entries();    //if scalar entries pointer is allocated exists, use it !
  if(mat==nullptr)   mat=Af.entries();
  factorize(*mat,ft,withPermutation);
  trace_p->pop();
}

// factorize sumatrix as LU or LDLt or LDL*, not preserving A
void factorize(SuTermMatrix& A, FactorizationType ft, bool withPermutation) { factorize(A,A,ft,withPermutation); }

//solve linear system when matrix is already factorized
//Note: right hand side B is not updated to take into account essential condition, do it before
SuTermVector factSolve(SuTermMatrix& A, const SuTermVector& B)
{
  trace_p->push("factSolve(SuTermMatrix, SuTermVector)");
  if(A.factorization() == _noFactorization) error("term_not_factorized", A.name());


  MatrixEntry* mat=A.scalar_entries();
  SuTermVector cB(B);    //copy B entries to preserve it
  cB.extendScalarTo(A.cdofsu(),true);
  VectorEntry* b=cB.scalar_entries();

  ValueType vtX = _real;
  if(A.valueType() == _complex || B.valueType() == _complex) vtX = _complex;
  SuTermVector X;
  A.initSuTermVector(X,vtX,true);
  X.toScalar(true);
  VectorEntry* x=X.scalar_entries();

  switch(A.factorization())
    {
      case _ldlt:
        mat->ldltSolve(*b, *x);
        break;
      case _ldlstar:
        mat->ldlstarSolve(*b, *x);
        break;
      case _lu:
        mat->luSolve(*b, *x);
        break;
      case _umfpack:
        mat->umfluSolve(*b,*x);
        break;
      default:
        error("wrong_factorization_type", words("factorization type",A.factorization()));
    }

  //finalization
  X.toVector(true);  //return to vector representation
  X.computed() = true;
  trace_p->pop();
  return X;
}

/*! solve linear system with multiple right hand sides when matrix is already factorized
   Bs may be a SuTermVectors
*/
SuTermVectors factSolve(SuTermMatrix& A, const std::vector<SuTermVector>& Bs)
{
  trace_p->push("factSolve(SuTermMatrix, vector<SuTermVector>");
  if(A.factorization() == _noFactorization) error("term_not_factorized", A.name());
  number_t n=Bs.size();
  SuTermVectors Xs(n);
  for(number_t k=0; k<n; k++) Xs[k]=factSolve(A,SuTermVector(Bs[k]));
  trace_p->pop();
  return Xs;
}

/*! solve linear system with right hand side matrix when matrix is already factorized
    in other words create the matrix C = inv(A) * B, C is stored in a dense format
    Works on scalar representation !
*/
SuTermMatrix factSolve(SuTermMatrix& A, SuTermMatrix& B)
{
  trace_p->push("factSolve(SuTermMatrix, SuTermMatrix)");
  if(A.factorization() == _noFactorization) error("term_not_factorized", A.name());
  A.toScalar(); B.toScalar();  //force scalar representation
  const MatrixEntry* matA = A.scalar_entries(), *matB = B.scalar_entries();
  ValueType vtC = _real, vtB = B.valueType(), vtA=A.valueType();
  if(vtA == _complex || vtB == _complex) vtC = _complex;
  if(matA==nullptr || matB==nullptr) error("scalar_only");

  //create matrix result
  string_t name="inv("+A.name()+")*"+B.name();
  SuTermMatrix C(B.up(), B.space_up(), A.vp(), A.space_vp(), 0, name);
  std::stringstream ss;
  ss << A.space_vp() << "-" << B.space_up();
  MatrixStorage* ms = buildStorage(_dense, _col, A.storagep()->buildType(), A.numberOfRows(), B.numberOfCols(),ss.str());
  C.scalar_entries() =  new MatrixEntry(vtC,_scalar, ms);
  number_t m=A.numberOfCols(), n=B.numberOfCols();

  SuTermVector Bk(B.name()+"_col_k", A.up(), B.space_up(), vtB, 0, 1);
  Bk.scalar_entries() = new VectorEntry(vtB,_scalar, m);
  Bk.cdofs()=B.cdofsv();
  Bk.extendScalarTo(A.cdofsu(),true);
  Bk.computed() = true;
  std::vector<std::pair<number_t, number_t> > rowIndex;
  std::vector<std::pair<number_t, number_t> >::iterator itr;
  for(number_t k=1; k<=n; k++)   //solve system for each column of B
    {
      (*Bk.scalar_entries())*=0.;   //reset to 0
      if(vtB==_real)
        {
          Vector<real_t>& bk = *Bk.scalar_entries()->rEntries_p;
          LargeMatrix<real_t>& mat = *B.scalar_entries()->rEntries_p;
          rowIndex=mat.getCol(k);
          for(itr=rowIndex.begin(); itr!=rowIndex.end(); ++itr)  //write col k of B to Bk entries
            *(bk.begin()+(itr->first-1))=mat(itr->second);
        }
      else
        {
          Vector<complex_t>& bk = *Bk.scalar_entries()->cEntries_p;
          LargeMatrix<complex_t>& mat = *B.scalar_entries()->cEntries_p;
          rowIndex=mat.getCol(k);
          for(itr=rowIndex.begin(); itr!=rowIndex.end(); ++itr) //write col k of B to Bk entries
            *(bk.begin()+(itr->first-1))=mat(itr->second);
        }
      VectorEntry Xk = factSolve(*A.scalar_entries(), *Bk.scalar_entries());
      if(vtC==_real)
        std::copy(Xk.rEntries_p->begin(), Xk.rEntries_p->end(), C.scalar_entries()->rEntries_p->at(1,k));  //copy Xk to C column k (real)
      else
        std::copy(Xk.cEntries_p->begin(), Xk.cEntries_p->end(), C.scalar_entries()->cEntries_p->at(1,k));  //copy Xk to C column k
    }
  C.computed() = true;
  trace_p->pop();
  return C;
}

// solve linear system by Gauss elimination
// if keepA is true, the original matrix is preserved
SuTermVector gaussSolve(SuTermMatrix& A, const SuTermVector& B, bool keepA)
{
  trace_p->push("gaussSolve(SuTermMatrix&, SuTermVector&)");
  SuTermVector cB(B);    //copy B entries
  cB.extendScalarTo(A.cdofsu(),true);
  VectorEntry* b=cB.scalar_entries();

  SuTermMatrix* cA = &A;
  if(keepA) cA = new SuTermMatrix(A);  //preserved A
  MatrixEntry* mat = cA->scalar_entries();

  ValueType vtX = _real;
  if(A.valueType() == _complex || B.valueType() == _complex) vtX = _complex;
  SuTermVector X;
  A.initSuTermVector(X,vtX,true);
  X.toScalar(true);
  VectorEntry* x=X.scalar_entries();

  gaussSolve(*mat,*b,*x);

  X.toVector(true);
  X.computed() = true;
  if(keepA) delete cA;
  trace_p->pop();
  return X;
}

// solve linear system using umfpack (if installed)
//   update the reciprocal condition number rcond = 1/||A||*||inv(A)|| in norm 1
//   if keepA is true, the original matrix is preserved
SuTermVector umfpackSolve(SuTermMatrix& A, const SuTermVector& B, real_t& rcond, bool keepA)
{
  #ifdef XLIFEPP_WITH_UMFPACK
    trace_p->push("umfpackSolve(SuTermMatrix, SuTermVector)");
    SuTermVector cB(B);    //copy B entries
    cB.extendScalarTo(A.cdofsu(),true);
    VectorEntry* b=cB.scalar_entries();

    SuTermMatrix* cA = &A;
    if(keepA) cA = new SuTermMatrix(A);  //preserved A
    MatrixEntry* mat = cA->scalar_entries();

    ValueType vtX = _real;
    if(A.valueType() == _complex || B.valueType() == _complex) vtX = _complex;
    SuTermVector X;
    A.initSuTermVector(X,vtX,true);
    X.toScalar(true);
    VectorEntry* x=X.scalar_entries();

    umfpackSolve(*mat,*b,*x, rcond);

    X.computed() = true;
    if(keepA) delete cA;    //delete the copy
    trace_p->pop();
    return X;
  #else
    error("xlifepp_without_umfpack");
    return B;
  #endif
}

SuTermVector umfpackSolve(SuTermMatrix& A, const SuTermVector& B, bool keepA)
{
  real_t rcond;
  return umfpackSolve(A,B,rcond,keepA);
}

} //end of namespace xlifepp
