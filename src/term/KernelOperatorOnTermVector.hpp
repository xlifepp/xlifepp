/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file KernelOperatorOnTermVector.hpp
  \authors E. Lunéville
  \since 13 dec 2015
  \date 13 dec 2015

  \brief Definition of the xlifepp::KernelOperatorOnTermVector class

  xlifepp::KernelOperatorOnTermVector class handles kernel operators applied to TermVector, say
              opker aop tv   or   tv aop opker
           where
              - opker is an operator acting on a kernel (OpertorOnKernel class)
              - aop is an algebraic operation, one of _product, _innerProduct, _crossProduct, _contractedProduct
              - tv  is a TermVector

           such a KernelOperatorOnTermVector object is involved in integral representation , for instance: intg(sigma, G*U)
*/


#ifndef KERNEL_OPERATOR_ON_TERMVECTOR_HPP
#define KERNEL_OPERATOR_ON_TERMVECTOR_HPP

#include "TermVector.hpp"
#include "config.h"

namespace xlifepp
{
//=================================================================================
/*!
   \class KernelOperatorOnTermVector
   useful class to deal with integral representation
         u(x) = int_dom opker(x,y) * tv(y)  or  u(x) = int_dom  tv(y) * opker(x,y)
    or with linear form defined from a bilinear form applied to a TermVector
         int_domx int_domy tv(y) * opker(x,y) * v(x)  or   int_domx int_domy  v(x) * opker(x,y) * tv(y)
*/
//=================================================================================
class KernelOperatorOnTermVector
{
protected:

   OperatorOnKernel opker_; //!< Kernel operator
   AlgebraicOperator aop_;  //!< algebraic operation (*,|,%,^) between kernel and TermVector
   const TermVector*  tv_;  //!< TermVector involved

public:
   bool termAtLeft;         //!< true if form is: opker aop tv false if form is:  tv aop opker

   //! basic constructor
   KernelOperatorOnTermVector(const OperatorOnKernel& opk, AlgebraicOperator aop, const TermVector& tv, bool atleft) 
   : opker_(opk), aop_(aop), tv_(&tv), termAtLeft(atleft) {}

   AlgebraicOperator algop() const {return aop_;}   //!< returns algebraic operation between TermVector and kernel (const)
   AlgebraicOperator& algop() {return aop_;}        //!< returns algebraic operation between TermVector and kernel (non const)
   const OperatorOnKernel& opker() const {return opker_;} //!< returns kernel operator
   const TermVector* termVector() const{return tv_;}     //!< returns TermVector pointer
   ValueType valueType() const;                     //!< return value type (_real or _complex)
   //! returns true if normal required on u
   bool xnormalRequired() const                     
     {return opker_.xnormalRequired();}
   //! returns true if normal required on v  
   bool ynormalRequired() const                     
     {return opker_.ynormalRequired();}
};

KernelOperatorOnTermVector operator*(const TermVector& tv, const Kernel& ker);             //!< tv * ker
KernelOperatorOnTermVector operator|(const TermVector& tv, const Kernel& ker);             //!< tv | ker
KernelOperatorOnTermVector operator^(const TermVector& tv, const Kernel& ker);             //!< tv ^ ker
KernelOperatorOnTermVector operator%(const TermVector& tv, const Kernel& ker);             //!< tv % ker
KernelOperatorOnTermVector operator*(const Kernel& ker, const TermVector& tv);             //!< ker * tv
KernelOperatorOnTermVector operator|(const Kernel& ker, const TermVector& tv);             //!< ker | tv
KernelOperatorOnTermVector operator^(const Kernel& ker, const TermVector& tv);             //!< ker ^ tv
KernelOperatorOnTermVector operator%(const Kernel& ker, const TermVector& tv);             //!< ker % tv
KernelOperatorOnTermVector operator*(const TermVector& tv, const OperatorOnKernel& opker); //!< tv * opker
KernelOperatorOnTermVector operator|(const TermVector& tv, const OperatorOnKernel& opker); //!< tv | opker
KernelOperatorOnTermVector operator^(const TermVector& tv, const OperatorOnKernel& opker); //!< tv ^ opker
KernelOperatorOnTermVector operator%(const TermVector& tv, const OperatorOnKernel& opker); //!< tv % opker
KernelOperatorOnTermVector operator*(const OperatorOnKernel& opk, const TermVector& tv);   //!< opker * tv
KernelOperatorOnTermVector operator|(const OperatorOnKernel& opk, const TermVector& tv);   //!< opker | tv
KernelOperatorOnTermVector operator^(const OperatorOnKernel& opk, const TermVector& tv);   //!< opker ^ tv
KernelOperatorOnTermVector operator%(const OperatorOnKernel& opk, const TermVector& tv);   //!< opker % tv

OperatorOnUnknown toOperatorOnUnknown(const KernelOperatorOnTermVector& koptv);

//! main routine for single integrals involving Kernel and TermVector
std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
                                             const std::vector<Parameter>& ps);

//@{
//! User single intg routines involving Kernel and TermVector (with up to 5 keys)
std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv);
std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
                                             const Parameter& p1);
std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
                                             const Parameter& p1, const Parameter& p2);
std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
                                             const Parameter& p1, const Parameter& p2, const Parameter& p3);
std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
                                             const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                             const Parameter& p4);
std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
                                             const Parameter& p1, const Parameter& p2, const Parameter& p3,
                                             const Parameter& p4, const Parameter& p5);
//@}
//@{
/*!
  \deprecated use key-value system for optional arguments (_quad, _order, _method, ...)
*/
std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
     const IntegrationMethod& im);
std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
     const IntegrationMethods& ims);
std::pair<LinearForm,const TermVector*> intg(const GeomDomain& dom, const KernelOperatorOnTermVector& koptv,
     QuadRule qr, number_t qo=0);
//@}

//=================================================================================
/*!
   \class KernelOperatorOnTermVectorAndUnknown
   useful class to deal with
          linear form defined from a bilinear form applied to a TermVector
          int_domx int_domy tv(y) * opker(x,y) * v(x)  or   int_domx int_domy  v(x) * opker(x,y) * tv(y)
          tv(y) * opker(x,y) or  opker(x,y) * tv(y) are read as KernelOperatorOnTermVector

          NOTE: the operator on unknown is always considered as a function depending on x
*/
//=================================================================================
class KernelOperatorOnTermVectorAndUnknown
{
protected:

   KernelOperatorOnTermVector koptv_; //!< kernel operator on TermVector involved
   AlgebraicOperator aop_;            //!< algebraic operation (*,|,%,^) between koptv and operator on unknown
   OperatorOnUnknown opv_;            //!< operator on unknown involved

public:
   bool opvAtLeft;         //!< true if form is: koptv aop opv,  false if form is:  opv aop koptv

   KernelOperatorOnTermVectorAndUnknown(const KernelOperatorOnTermVector& koptv, AlgebraicOperator aop,
                                        const OperatorOnUnknown& opv, bool atleft) //! basic constructor
   : koptv_(koptv), aop_(aop), opv_(opv), opvAtLeft(atleft) {}

   AlgebraicOperator algop() const {return aop_;}   //!< returns algebraic operation between TermVector and kernel (const)
   AlgebraicOperator& algop() {return aop_;}        //!< returns algebraic operation between TermVector and kernel (non const)
   const KernelOperatorOnTermVector& koptv() const {return koptv_;}    //!< returns KernelOperatorOnTermVector
   const OperatorOnKernel& opker() const {return koptv_.opker();}      //!< returns kernel operator
   const OperatorOnUnknown& opv() const{return opv_;}                  //!< return operator on v
   const TermVector*  termVector() const{return koptv_.termVector();}  //!< returns TermVector pointer
   ValueType valueType() const;                     //!< return value type (_real or _complex)
   bool xnormalRequired() const                     //! returns true if normal required on u
     {return opker().xnormalRequired() || opv_.normalRequired();}
      //! returns true if normal required on v
   bool ynormalRequired() const                    
     {return opker().ynormalRequired();}
};

KernelOperatorOnTermVectorAndUnknown operator*(const OperatorOnUnknown&, const KernelOperatorOnTermVectorAndUnknown&);  // opv * koptv
KernelOperatorOnTermVectorAndUnknown operator|(const OperatorOnUnknown&, const KernelOperatorOnTermVectorAndUnknown&);  // opv | koptv
KernelOperatorOnTermVectorAndUnknown operator^(const OperatorOnUnknown&, const KernelOperatorOnTermVectorAndUnknown&);  // opv ^ koptv
KernelOperatorOnTermVectorAndUnknown operator%(const OperatorOnUnknown&, const KernelOperatorOnTermVectorAndUnknown&);  // opv % koptv
KernelOperatorOnTermVectorAndUnknown operator*(const KernelOperatorOnTermVectorAndUnknown&, const OperatorOnUnknown&);  // kopv * opv
KernelOperatorOnTermVectorAndUnknown operator|(const KernelOperatorOnTermVectorAndUnknown&, const OperatorOnUnknown&);  // kopv | opv
KernelOperatorOnTermVectorAndUnknown operator^(const KernelOperatorOnTermVectorAndUnknown&, const OperatorOnUnknown&);  // kopv ^ opv
KernelOperatorOnTermVectorAndUnknown operator%(const KernelOperatorOnTermVectorAndUnknown&, const OperatorOnUnknown&);  // kopv * �pv
KernelOperatorOnTermVectorAndUnknown operator*(const Unknown&, const KernelOperatorOnTermVector&);  // v * koptv
KernelOperatorOnTermVectorAndUnknown operator|(const Unknown&, const KernelOperatorOnTermVector&);  // v | koptv
KernelOperatorOnTermVectorAndUnknown operator^(const Unknown&, const KernelOperatorOnTermVector&);  // v ^ koptv
KernelOperatorOnTermVectorAndUnknown operator%(const Unknown&, const KernelOperatorOnTermVector&);  // v % koptv
KernelOperatorOnTermVectorAndUnknown operator*(const KernelOperatorOnTermVector&, const Unknown&);  // koptv * v
KernelOperatorOnTermVectorAndUnknown operator|(const KernelOperatorOnTermVector&, const Unknown&);  // koptv | v
KernelOperatorOnTermVectorAndUnknown operator^(const KernelOperatorOnTermVector&, const Unknown&);  // koptv ^ v
KernelOperatorOnTermVectorAndUnknown operator%(const KernelOperatorOnTermVector&, const Unknown&);  // koptv % v

} //end of namespace xlifepp

#endif /* KERNEL_OPERATOR_ON_TERMVECTOR_HPP */
