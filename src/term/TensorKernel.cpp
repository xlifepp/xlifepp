/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TensorKernel.cpp
  \author E. Lunéville
  \since 20 sep 2013
  \date  06 sep 2016

  \brief Implementation of xlifepp::TensorKernel class member functions
*/

#include "TensorKernel.hpp"


namespace xlifepp
{

// copy constructor with full copy of SpectralBasis and matrix
TensorKernel::TensorKernel(const TensorKernel& tk)
    :  Kernel(tk)
{
    phi_p=tk.phi_p;
    psi_p=tk.psi_p;
    matrix_p=nullptr;
    if(phi_p!=nullptr) phi_p=tk.phi_p->clone();
    if(psi_p!=nullptr)
      {
        if(tk.psi_p!=tk.phi_p) psi_p=tk.psi_p->clone();
        else psi_p=phi_p;
      }
    if(tk.matrix_p!=nullptr)  matrix_p=new VectorEntry(*tk.matrix_p);
    freeBasis=true;
    isDiag=tk.isDiag;
    xmap=tk.xmap;
    ymap=tk.ymap;
    isConjugate=tk.isConjugate;
}

//!< clone current kernel (virtual)
TensorKernel* TensorKernel::clone() const
{
    return new TensorKernel(*this);
}

TensorKernel::~TensorKernel()
{
   if(matrix_p!=nullptr) delete matrix_p;
   if(freeBasis)
   {
       if(phi_p!=nullptr) delete phi_p;
       if(psi_p!=nullptr  && psi_p!=phi_p) delete psi_p;
   }
}

// return kernel value and structure type
ValueType TensorKernel::valueType() const
{
    if(phi_p->valueType()==_complex) return _complex;
    if(psi_p->valueType()==_complex) return _complex;
    if(matrix_p!=nullptr && matrix_p->valueType_==_complex) return _complex;
    return _real;
}
StrucType TensorKernel::strucType() const
{
    if(phi_p->strucType()==_vector || psi_p->strucType()==_vector) return _matrix;
    return _scalar;
}

//print utilities
void TensorKernel::print(std::ostream& os) const
{
    os<<"TensorKernel object: "<< name<<", kernel functions: "<<eol;
    os<<"       phi(y) : "<< (*phi_p)<<eol;
    os<<"       psi(x) : "<< (*psi_p)<<eol;
    if(theVerboseLevel<2) return;
    os<<"       ";
    if(isDiag) os<<"diagonal ";
    os<<"kernel matrix of size "<<tostring(nbOfPsi())<<" x "<<tostring(nbOfPhi())<<eol<<"     ";
    if(theVerboseLevel>5) os<<(*matrix_p);
    if(userData.size()>0) os<<userData;
    os<<eol;
}

} // end of namespace xlifepp

