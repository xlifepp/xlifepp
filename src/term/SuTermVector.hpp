/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SuTermVector.hpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 3 apr 2012
  \date 13 jn 2014

  \brief Definition of the xlifepp::SuTermVector class

  xlifepp::SuTermVector class handles numerical representation of a single unknown linear form
  and has the following items:
    - the largest subspace of unknown space involved in linear form, possibly the whole space
    - the list of subspaces involved by each basic linear form
    - a list of single unknown essential boundary conditions
  xlifepp::SuTermVector inherits from xlifepp::Term !

  \verbatim
          child
  Term  --------> SuTermVector
                    | SuLinearForm
                    | VectorEntry *
                    | ...
  \endverbatim

  xlifepp::SuTermVector is an internal object never managed by end user !

  Note: if the unknown is not scalar and if it is required by special operation (solver),
         it is possible to go to a scalar representation by using scalar_entries_p and renumbering vector cdofs.
         Be cautious, this scalar representation is not updated by most operations
*/


#ifndef SU_TERM_VECTOR_HPP
#define SU_TERM_VECTOR_HPP

#include "Term.hpp"
#include "LcTerm.hpp"
#include "config.h"
#include "form.h"
#include "computation/termUtils.hpp"
#include <list>

namespace xlifepp
{

//forward class declaration
class TermVector;

typedef real_t(funSR1_t)(const real_t&);
typedef real_t(funSR2_t)(const real_t&, const real_t&);
typedef complex_t(funSC1_t)(const complex_t&);
typedef complex_t(funSC2_t)(const complex_t&, const complex_t&);

//=================================================================================
/*!
   \class SuTermVector
   handles numerical representation
          of single unknown linear forms (SuLinearForm). Internal class
*/
//=================================================================================
class SuTermVector : public Term
{
  protected:
    SuLinearForm* sulf_p;             //!< single unknown linear form (pointer)
    mutable Space* space_p;           //!< largest subspace involved in linear form, may be the whole space
    std::vector<Space*> subspaces;    //!< subspaces related to each basic linear forms GeomDomain
    VectorEntry* entries_p;           //!< values of SuTermVector
    const Unknown* u_p;               //!< pointer to unknown

    // for scalar representation
    VectorEntry* scalar_entries_p;    //!< values of SuTermVector in scalar representation, same as entries_p if scalar unknown
    std::vector<DofComponent> cdofs_; //!< scalar numbering when scalar_entries_p is allocated

  public:
    SuTermVector(SuLinearForm* sulf = nullptr, const string_t& na = "", bool noass = false); //!< basic constructor from SuLinearForm
    SuTermVector(const string_t&, const Unknown*, Space*, ValueType vt = _real, number_t n = 0, dimen_t nv = 0, bool noass = false); //!< basic constructor from explicit data type
    template<typename T>
    SuTermVector(const string_t&, const Unknown*, Space*, const T&); //!< basic constructor from a constant value

    template <typename T>
    SuTermVector(const Unknown& u, const GeomDomain& dom, const Vector<T>& v, const string_t& na, bool nodalFct, bool noass); //!< constructor from constant vector or a vector
    template <typename T>
    SuTermVector(const Unknown& u, const GeomDomain& dom, const T& v, const string_t& na, bool nodalFct, bool noass); //!< constructor from constant scalar value
    //! SuTermVector from explicit real scalar function (interpolation on Lagrange Dof's)
    SuTermVector(const Unknown& u, const GeomDomain& dom, funSR_t& f, const string_t& na, bool nodalFct, bool noass);
    //! SuTermVector from explicit complex scalar function (interpolation on Lagrange Dof's)
    SuTermVector(const Unknown& u, const GeomDomain& dom, funSC_t& f, const string_t& na, bool nodalFct, bool noass);
    //! SuTermVector from explicit real vector function (interpolation on Lagrange Dof's)
    SuTermVector(const Unknown& u, const GeomDomain& dom, funVR_t& f, const string_t& na, bool nodalFct, bool noass);
    //! SuTermVector from explicit complex vector function (interpolation on Lagrange Dof's)
    SuTermVector(const Unknown& u, const GeomDomain& dom, funVC_t& f, const string_t& na, bool nodalFct, bool noass);
    //! SuTermVector from a SuTermVector with geometrical mapping
    // SuTermVector(const Unknown& u, const GeomDomain& dom, const SuTermVector& sutv, const Function* f, bool , const string_t& na = "");
    //! SuTermVector from the restriction of a SuTermVector to a given domain
    SuTermVector(const SuTermVector& sutv, const GeomDomain& dom, const string_t& na = "");
    //! interpolation constructor for non Lagrange interpolation involving first derivative dofs
    SuTermVector(const Unknown& u, const GeomDomain& dom, const Function& f, const Function& gf, const string_t& na);
    //! interpolation value constructor for non Lagrange interpolation involving second derivative dofs
    SuTermVector(const Unknown& u, const GeomDomain& dom, const Function& f, const Function& gf, const Function g2f, const string_t& na);
    //constructor from SuTermVector involving mesh interpolation
    SuTermVector(const Unknown& u, const GeomDomain& dom, const SuTermVector& sut, const Function* fmap, bool useNearest, bool errorOnOutDom, real_t tol, const string_t& na);

    // constructor from explicit function of SuTermVector's
    //! real_fun(SuTermVector)
    SuTermVector(const SuTermVector&, funSR1_t& f, const string_t& na="");
    //! real_fun(SuTermVector, SuTermVector)
    SuTermVector(const SuTermVector&, const SuTermVector&, funSR2_t& f, const string_t& na="");
    //! complex_fun(SuTermVector)
    SuTermVector(const SuTermVector&, funSC1_t& f, const string_t& na="");
    //! complex_fun(SuTermVector, SuTermVector)
    SuTermVector(const SuTermVector&, const SuTermVector&, funSC2_t& f, const string_t& na="");

    // constructor from symbolic function of SuTermVector's
    //! symb_fun(SuTermVector)
    SuTermVector(const SuTermVector&, const SymbolicFunction& , const string_t& na="");
    //! symb_fun(SuTermVector, SuTermVector)
    SuTermVector(const SuTermVector&, const SuTermVector&, const SymbolicFunction& fs, const string_t& na="");
    //! symb_fun(SuTermVector, SuTermVector, SuTermVector)
    SuTermVector(const SuTermVector&, const SuTermVector&, const SuTermVector&, const SymbolicFunction& fs, const string_t& na="");

    // constructor of a vector SuTermVector from scalar SutermVector's
    //! concatenate 2 scalar SuTermVector's in a vector SutermVector
    SuTermVector(const Unknown&, const SuTermVector&, const SuTermVector&, const string_t& ="");
    //! concatenate n scalar SuTermVector's in a vector SutermVector
    SuTermVector(const Unknown&, const std::list<const SuTermVector*>&, const string_t& ="");

    //tools to build SuTermVector (interpolent) from function
    //! initialize SuTermVector from function (interpolation on Lagrange Dof's)
    void initFromFunction(const Unknown& u, const GeomDomain& dom, const OperatorOnFunction& opf, const string_t& na, bool noass);
    //! initialize SuTermVector from functions (interpolation on non Lagrange Dof's)
    void initFromFunction(const Unknown& u, const GeomDomain& dom, const Function& f, const Function& gf, const Function& g2f, const string_t& na);

    void copy(const SuTermVector&);               //!< full copy of SuTermVector
    SuTermVector(const LcTerm<SuTermVector>&);    //!< constructor from a linear combination of SuTermVector's
    SuTermVector(const SuTermVector&);            //!< copy constructor (full copy)
    template<typename T>
    SuTermVector(const SuTermVector&, const T&);  //!< copy constructor and assign a constant value
    SuTermVector& operator=(const SuTermVector&); //!< assign operator
    virtual ~SuTermVector();                      //!< destructor
    void clear();                                 //!< deallocate memory used by a SuTermVector

    //accessors and various utilities
    string_t name() const { return Term::name(); }
    void name(const string_t& nam)      //! update the name of a SuTermVector
    { name_=nam; }
    SuLinearForm* sulfp() const         //! access to single unknown linear form (pointer)
    { return sulf_p; }
    SuLinearForm*& sulfp()              //! access to single unknown linear form (pointer) (writable)
    { return sulf_p; }
    VectorEntry*& entries()             //! access to entries pointer (r/w)
    { return entries_p; }
    const VectorEntry* entries() const  //! access to entries pointer (r)
    { return entries_p; }
    TermType termType() const           //! return _termVector
    { return _termVector; }
    ValueType valueType() const;        //!< return value type (_real,_complex)
    StrucType strucType() const;        //!< return structure type (_scalar,_vector)
    const Unknown* up() const           //! return pointer to unknown (column)
    { return u_p; }
    const Unknown*& up()                //! return pointer to unknown (column), write
    { return u_p; }
    number_t nbOfComponents() const     //! number of components when each element is a vector (1 if scalar element)
    { return u_p->nbOfComponents(); }
    const Space* spacep() const         //! return the pointer to the largest space in u involved in
    { return space_p; }
    Space*& spacep()                    //! return the pointer to the largest space in u involved in
    { return space_p; }
    std::set<const Space*> unknownSpaces() const;   //!< set of unknown spaces involved
    number_t nbDofs() const;                        //!< size of SuTermvector counted in dofs
    number_t size() const;                          //!< size of SuTermvector counted in scalar
    VectorEntry*& scalar_entries()                  //! access to entries pointer (r/w)
    { return scalar_entries_p; }
    const VectorEntry* scalar_entries() const       //! access to entries pointer (r)
    { return scalar_entries_p; }
    VectorEntry* actual_entries() const;            //!< return the actual pointer to entries (priority to scalar entry)
    const std::vector<DofComponent>& cdofs() const  //! access to cdofs_r vector (r)
    { return cdofs_; }
    std::vector<DofComponent>& cdofs()              //! access to cdofs_r vector (r/w)
    { return cdofs_; }
    const Dof& dof(number_t n) const;               //!< return n-th dof of unknown (n>=1)
    const GeomDomain* domain() const;               //!< return geometric domain pointer attached to SuTermVector (may be 0)

    //direct value managment
    number_t dofRank(const Dof& dof) const;         //!< return rank in vector of dof
    Value getValue(number_t) const;                 //!< access to vector component (n>=1)
    void setValue(number_t, const Value&);          //!< set value to vector component (n>=1)
    void setValue(const Value&, const GeomDomain&);        //!< set values of SuTermVector from Value, restricted to a domain: sut|dom=v
    void setValue(const Function&, const GeomDomain&);     //!< set values of SuTermVector from Function, restricted to a domain: sut|dom=f
    void setValue(const SuTermVector&, const GeomDomain&); //!< set values of SuTermVector from SuTermVector, restricted to a domain: sut|dom= sutv|dom
    void setValue(const SuTermVector&);                    //!< set values of SuTermVector from SuTermVector
    Value evaluate(const Point&, const Element*, bool useNearest=false, bool errorOnOutDom=true) const;    //!< evaluate interpolated value at a point, may be knowing Element (general function)
    Value evaluate(const Point& p, bool useNearest=false, bool errorOnOutDom=true) const                   //! evaluate interpolated value at a point
    { return evaluate(p,0, useNearest, errorOnOutDom); }
    Value evaluate(const Point& p, const Element& elt) const //!< evaluate interpolated value at a point, knowing Element
    { return evaluate(p,&elt); }
    template<typename T>
    Vector<T>& asVector(Vector<T>&) const;          //!< reinterpret SuTermVector as a raw Vector<T>

    //compute and complex calculation
    void buildSubspaces();                 //!< build the required subspaces and the largest subspace
    void compute();                        //!< compute SuTermVector
    void compute(const LcTerm<SuTermVector>&); //!< compute SuTermVector from a linear combination of SuTermVector's
    void toScalar(bool keepVector=false);  //!< go to scalar representation
    void toVector(bool keepEntries=false); //!< return to vector representation from scalar representation
    void extendTo(const SuTermVector&);    //!< extend current vector according to numbering of an other vector
    void extendTo(const Space&);           //!< extend current vector according to numbering of a space
    void extendScalarTo(const std::vector<DofComponent>&, bool useDual = false); //!< extend current scalar entries according to a DofComponent numbering vector
    //! produce the mapped SuTermVector from current GeomDomains to a GeomDomain
    SuTermVector* mapTo(const GeomDomain&, const Unknown&, bool useNearest= false, bool erDom=true, Function* fmap= nullptr, real_t tol=theLocateToleranceFactor) const;
    void changeUnknown(const Unknown&, const Vector<number_t>&); //!< move SuTermVector unknown to an other
    void adjustScalarEntries(const std::vector<DofComponent>&);  //!< adjust scalar entries to cdofs numbering given

    //computing algorithms
    template<typename T, typename K>
    void computeFE(const SuLinearForm&, Vector<T>&, K&); //!< compute SuTermVector (FE and FEext computation)
    template<typename T, typename K>
    void computeSP(const SuLinearForm&, Vector<T>&, K&); //!< compute SuTermVector (SP computation)
    //! compute integral representation using integration methods
    template<typename T, typename K>
    void computeIR(const SuLinearForm&, Vector<T>& res, K& vt, const std::vector<Point>& xs, const Vector<T>&, const std::vector<Vector<real_t> >* nxs=nullptr);
    //! compute scalar SuTermVector (IR computation)
    template<typename T, typename K>
    void computeIROld(const SuLinearForm&, Vector<T>& res, K& vt, const std::vector<Point>& xs, const Vector<T>&, const std::vector<Vector<real_t> >* nxs=nullptr);
    //algebraic functions
    SuTermVector& operator+=(const SuTermVector&);            //!< operation U+=V
    SuTermVector& operator-=(const SuTermVector&);            //!< operation U+=V
    SuTermVector& merge(const SuTermVector&);                 //!< merge a SuTermVector with preserving the current value when common dofs
    template<typename T>
    SuTermVector& operator*=(const T&);                       //!< operation U*=real
    template<typename T>
    SuTermVector& operator/=(const T&);                       //!< operation U/=real
    SuTermVector operator()(const ComponentOfUnknown&) const; //!< extract unknown component term vector as a SuTermVector
    SuTermVector toVectorUnknown() const;                     //!< return vector unknown representation if component unknown SutermVector
    SuTermVector& toAbs();                                    //!< change to abs(U)
    SuTermVector& toReal();                                   //!< change to real part of U
    SuTermVector& toImag();                                   //!< change to imaginary part of U
    SuTermVector& toComplex();                                //!< change to complex representation
    SuTermVector& toConj();                                   //!< change to conj(U)
    SuTermVector& roundToZero(real_t aszero=std::sqrt(theEpsilon)); //!< round to zero all coefficients close to 0 (|.| < aszero)
    SuTermVector& round(real_t prec=0.); //!< round with a given precision (0 no round)
    complex_t maxValAbs() const; //!< return the value (in complex) of component being the largest one in absolute value
    SuTermVector* onDomain(const GeomDomain& dom) const; //!< restriction of SuTermVector to a domain, return 0 pointer if void
    SuTermVector& applyEssentialConditions(const Constraints&); //!< apply constraints (essential conditions) to current SuTermVector

    //interpolation functions
    template<typename T>
    T& operator()(const std::vector<real_t>&, T&) const;          //!< compute interpolated value of unknown at point
    SuTermVector* interpolate(const Unknown&, const GeomDomain&); //!< interpolate on u space and geomdomain
    //! locate element of the SuTermVector FESpace containing P
    Element& locateElement (const Point& p,bool useNearest=false, bool errorOnOutDom=true) const;
    //! locate element (pointer) of the SuTermVector FESpace containing P
    Element* locateElementP(const Point& p,bool useNearest=false, bool errorOnOutDom=true) const;

    void print(std::ostream&) const;                           //!< print SuTermVector
    void print(PrintStream& os) const {print(os.currentStream());}
    void print(std::ostream&, bool r, bool h) const;           //!< print SuTermVector with raw and header option
    void print(PrintStream& os, bool r, bool h) const {print(os.currentStream(), r ,h);}
    void saveToFile(const string_t& fname, number_t prec=fullPrec, bool encode=false) const; //!< save SuTermVector to file
    void saveToFile(const string_t& fname, bool encode) const //! save SuTermVector to file
    { saveToFile(fname, fullPrec, encode); }

    friend class TermVector;
};

// product and norms of vector
complex_t innerProduct(const SuTermVector&, const SuTermVector&);     //!< inner product
complex_t hermitianProduct(const SuTermVector&, const SuTermVector&); //!< hermitian product
real_t norm1(const SuTermVector&);       //!< l1 vector SuTermVector norm
real_t norm2(const SuTermVector&);       //!< l2 vector SuTermVector norm (quadratic norm)
real_t norminfty(const SuTermVector&);   //!< l_infinite SuTermVector norm (sup norm)
SuTermVector tensorInnerProduct(const SuTermVector&, const SuTermVector&);     //! tensor inner product => scalar SuTermVector (not conjugate)
SuTermVector tensorHermitianProduct(const SuTermVector&, const SuTermVector&); //! tensor hermitian product => scalar SuTermVector
SuTermVector tensorCrossProduct(const SuTermVector&, const SuTermVector&);     //! tensor cross product => vector SuTermVector in 3D, scalar SuTermVector in 2D

//general tools
bool sameDofs(const SuTermVector&, const SuTermVector&);          //!< check if 2 SuTermVector have same dofs
SuTermVector* mergeSuTermVector(const std::list<SuTermVector*>&); //!< merge blocks with components of the same unknown
void adjustScalarEntriesG(VectorEntry*&, std::vector<DofComponent>&, const std::vector<DofComponent>&); //!< tool to adjust VectorEntry

//@{
//! input/output function
void saveToFile(const string_t& filename, const Space* sp, const std::list<SuTermVector*>& sutvs, IOFormat iof, string_t dataName="", bool aFilePerDomain=true);
void saveToMsh(std::ostream& fout, const Space*, const std::list<SuTermVector*>&, const std::vector<Point>& coords,
               const splitvec_t& elementsInfo, const GeomDomain* dom, string_t dataName="");
void saveToMtlb(std::ostream& fout, const Space*, const std::list<SuTermVector*>&, const std::vector<Point>& coords,
               const splitvec_t& elementsInfo, const GeomDomain* dom, string_t dataName="");
void saveToVtk(std::ostream& fout, const Space*, const std::list<SuTermVector*>&, const std::vector<Point>& coords,
               const splitvec_t& elementsInfo, const GeomDomain* dom, string_t dataName="");
void saveToVizir4(std::ostream& fout, const Space*, const std::list<SuTermVector*>&, const std::vector<Point>& coords,
               const splitvec_t& elementsInfo, const GeomDomain* dom, string_t dataName="SolAtVertices");
void saveToVtkVtu(std::ostream& fout, const Space*, const std::list<SuTermVector*>&, const std::vector<Point>& coords,
                  const splitvec_t& elementsInfo, const GeomDomain* dom, string_t dataName="");
void saveToXyzVs(std::ostream& fout, const Space* sp, const std::list<SuTermVector*>& stvlist,
                 const std::vector<Point>& coords, string_t dataName="", bool writeHeader=true);
std::pair<std::vector<Point>, std::map<number_t, number_t> > ioPoints(const Space* sp);
splitvec_t ioElementsBySplitting(const Space* sp, std::map<number_t, number_t> renumbering);
//@}

//------------------------------------------------------------------------------------------
// template functions
//------------------------------------------------------------------------------------------

/*! SuTermVector constructor from Unknown and real value, explicit vector construction from a constant value (no linear form)
    T may be a real/complex scalar or a real/complex vector (only in the case of a vector unknown)
*/
template <typename T>
SuTermVector::SuTermVector(const Unknown& u, const GeomDomain& dom, const T& v, const string_t& na, bool nodalFct, bool noass)
{
  termType_ = _sutermVector;
  computingInfo_.noAssembly = noass;
  computingInfo_.isComputed = true;
  name_ = na;
  sulf_p = nullptr;
  u_p = &u;
  // find subspace or create it
  Space* spu = u.space();
  space_p = spu->findSubSpace(&dom, spu);
  if (space_p == nullptr) space_p = new Space(dom, *spu, spu->name() + "_" + dom.name());
  entries_p = nullptr;
  scalar_entries_p=nullptr;
  number_t n = space_p->nbDofs();
  if (n == 0) return;  //void vector
  // allocate entries and fill in with constant value
  dimen_t nbc= u.nbOfComponents();  //dim of dofs
  if (nbc==1) entries_p = new VectorEntry(v, n);
  else entries_p = new VectorEntry(Vector<T>(nbc,v),n);
}

/*! SuTermVector constructor from Unknown and real value, explicit vector construction from a given vector (no linear form)
    T may be a real/complex scalar or a real/complex vector (only in the case of a vector unknown)
    to raise the ambiguity between u(i)=v i=1,n and u = v we test the number of components (nbc) of the unknown and the size (s) of v
    if nbc=s we apply u(i)=v i=1,n else we apply u = v
    Note: if v is too small compared to number of dofs, the omitted values are replaced by 0
*/
template <typename T>
SuTermVector::SuTermVector(const Unknown& u, const GeomDomain& dom, const Vector<T>& v, const string_t& na, bool nodalFct, bool noass)
{
  termType_ = _sutermVector;
  computingInfo_.noAssembly = noass;
  computingInfo_.isComputed = true;
  name_ = na;
  sulf_p = nullptr;
  u_p = &u;
  // find subspace or create it
  Space* spu = u.space();
  space_p = spu->findSubSpace(&dom, spu);
  if (space_p == nullptr) space_p = new Space(dom, *spu, spu->name() + "_" + dom.name());
  entries_p = nullptr;
  scalar_entries_p=nullptr;
  number_t n = space_p->dimSpace();
  if (n == 0) return;  //void vector

  //create entries
  dimen_t nbc= u.nbOfComponents(); // dim of dofs
  if (nbc==v.size()) // case u(i)=v i=1,n
  {
    Vector<Vector<T> > w(n,v);
    entries_p = new VectorEntry(w);
  }
  else // case u=v
    entries_p = new VectorEntry(v);
}

//! basic constructor from a constant value
template<typename T>
SuTermVector::SuTermVector(const string_t& na, const Unknown* u , Space* sp, const T& v)
{
  termType_ = _sutermVector;
  name_ = na;
  sulf_p = nullptr;
  u_p = u;
  space_p = sp;
  computingInfo_.noAssembly = false;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  dimen_t nv=u->nbOfComponents();
  number_t n=sp->nbDofs();
  if (n == 0) return; // no allocation !
  if (nv >1 && dimsOf(v).first == 1) // goto vector from scalar
    entries_p = new VectorEntry(Vector<T>(nv,v), n);
  else entries_p = new VectorEntry(v, n);
}


// specialization of SuTermVector constructor from Unknown, GeomDomain and Function
template <>
SuTermVector::SuTermVector(const Unknown& u, const GeomDomain& dom, const Function& f, const string_t& na, bool nodalFct, bool noass);

// specialization of SuTermVector constructor from Unknown, GeomDomain,and operator on function
template <>
SuTermVector::SuTermVector(const Unknown& u, const GeomDomain& dom, const OperatorOnFunction& opf, const string_t& na, bool nodalFct, bool noass);

// specialization of SuTermVector constructor from Unknown, GeomDomain and symbolic function
template <>
SuTermVector::SuTermVector(const Unknown& u, const GeomDomain& dom, const SymbolicFunction& sf, const string_t& na, bool nodalFct, bool noass);

// specialization of SuTermVector constructor from Unknown, GeomDomain and VariableName
template <>
SuTermVector::SuTermVector(const Unknown& u, const GeomDomain& dom, const VariableName& vn, const string_t& na, bool nodalFct, bool noass);

//! copy constructor from SuTermVector with values set to a constant value
template<typename T>
SuTermVector::SuTermVector(const SuTermVector& suV, const T& v)
{
  sulf_p = suV.sulf_p;
  name_ = suV.name_;
  u_p = suV.u_p;
  // space_p = nullptr;
  space_p = const_cast<Space*>(suV.spacep());
  termType_ = _sutermVector;
  computingInfo_ = suV.computingInfo_;
  entries_p = nullptr;
  scalar_entries_p=nullptr;
  number_t nbc = suV.nbOfComponents();
  if (suV.entries_p != nullptr)
  {
    if (nbc ==1 ) entries_p = new VectorEntry(v, suV.entries_p->size());
    else entries_p = new VectorEntry(Vector<T>(nbc,v), suV.entries_p->size());
  }
  if (suV.scalar_entries_p != nullptr)
  {
    if (suV.scalar_entries_p!=suV.entries_p) scalar_entries_p  = new VectorEntry(v, suV.scalar_entries_p ->size());
    else scalar_entries_p=entries_p;
    cdofs_=suV.cdofs_;
  }
}

template<typename T>
SuTermVector& SuTermVector::operator*=(const T& t) //! operation U*=t
{
  if (entries_p!=nullptr) *entries_p *= t; // v has entries
  if (scalar_entries_p!=nullptr && scalar_entries_p!=entries_p) *scalar_entries_p *= t; // v has scalar entries
  return *this;
}

template<typename T>
SuTermVector& SuTermVector::operator/=(const T& t) // operation U/=t
{
  if (entries_p!=nullptr) *entries_p /= t; // v has entries
  if (scalar_entries_p!=nullptr && scalar_entries_p!=entries_p) *scalar_entries_p /= t; // v has scalar entries
  return *this;
}

/*! interpolation functions
  compute interpolated value of unknown at point p, T has to be compliant with SuTermVector entries */
template<typename T>
T& SuTermVector::operator()(const std::vector<real_t>& p, T& v) const
{
  v=T();
  if (space_p->isSpectral())
  {
    space_p->spSpace()->spectralBasis()->evaluate(p, *entries_p->entriesp<T>(), v);
    return v;
  }
  if (space_p->isFE())
  {
    const MeshDomain* mdom=space_p->domain()->meshDomain();
    if (mdom==nullptr)
    {
      where("SuTermVector::operator()(const Point&, T&)");
      error("domain_notmesh",space_p->domain()->name(),words("domain type",space_p->domain()->domType()));
    }
    GeomElement* gelt = mdom->locate(p);
    if (gelt==nullptr)
    {
      where("SuTermVector::operator()(const Point&, T&)");
      error("geoelt_not_found");
    }
    number_t k=space_p->numElement(gelt);
    const Element* elt=space_p->element_p(k);
    elt->interpolate(*entries_p, p, space_p->elementDofs(k),v);
    return v;
  }

  where("SuTermVector::operator()(const Point&, T&)");
  error("spacetype_not_handled",space_p->name());
  return v;
}

//! reinterpret SuTermVector as a raw Vector<T>
template<typename T>
Vector<T>& SuTermVector::asVector(Vector<T>& vec) const
{
  if (scalar_entries_p==nullptr && entries_p==nullptr)
  {
    where("SuTermvector::asVector(Vector<T>)");
    error("term_no_entries");
  }
  if (entries_p!=nullptr) entries_p->asVector(vec);
  else scalar_entries_p->asVector(vec);
  return vec;
}

//! special operation to accumulate t*v in x, assumed consistent unknown and same size
template<typename T>
void addScaledVector(SuTermVector& x, SuTermVector& v, const T& t)    //!  x+=v*t
{
  if (v.entries()==nullptr) return;  //nothing to add
  if (x.up() != v.up() && x.up() != v.up()->dual_p())
  {
    where("addScaledVector<T>(SuTermvector, SuTermvector, T)");
    error("term_inconsistent_unknowns");
  }
  if (x.entries()==nullptr)
  {
     x.entries()=new VectorEntry(*v.entries());
     *x.entries() *=t;
     return;
  }
  if (x.spacep()->dimSpace()==v.spacep()->dimSpace()) addScaledVector(*x.entries(),*v.entries(),t);
  else
  {
    where("addScaledVector<T>(SuTermvector, SuTermvector, T)");
    error("term_incompatible_spaces");
  }
}

//!binary and unary operators applied to SuTermVector's, assuming they have the same size
inline SuTermVector operator +(const SuTermVector& s1, const SuTermVector& s2)
{return SuTermVector(s1,s2,x_1+x_2);}

inline SuTermVector operator -(const SuTermVector& s1, const SuTermVector& s2)
{return SuTermVector(s1,s2,x_1-x_2);}

inline SuTermVector operator *(const SuTermVector& s1, const SuTermVector& s2)
{return SuTermVector(s1,s2,x_1*x_2);}

inline SuTermVector operator /(const SuTermVector& s1, const SuTermVector& s2)
{return SuTermVector(s1,s2,x_1/x_2);}

inline SuTermVector operator ^(const SuTermVector& s, const real_t& p)
{return SuTermVector(s,x_1^p);}

inline SuTermVector operator +(const SuTermVector& s)
{return s;}

inline SuTermVector operator -(const SuTermVector& s)
{
  SuTermVector r(s);
  return r*=-1;
}

//! mathematical function applied to SuTermVector
inline SuTermVector realPart(const SuTermVector& s)
{
  if (s.valueType()==_real) return s;
  SuTermVector r(s);
  r.toReal();
  return r;
}

inline SuTermVector imagPart(const SuTermVector& s)
{
  SuTermVector r(s);
  r.toImag();
  return r;
}

inline SuTermVector toComplex(const SuTermVector& s)
{
  if (s.valueType()==_complex) return s;
  SuTermVector r(s);
  r.toComplex();
  return r;
}

inline SuTermVector abs(const SuTermVector& s)
{ return SuTermVector(s,abs(x_1)); }

inline SuTermVector sqrt(const SuTermVector& s)
{ return SuTermVector(s,sqrt(x_1)); }

inline SuTermVector squared(const SuTermVector& s)
{ return SuTermVector(s,squared(x_1)); }

inline SuTermVector sin(const SuTermVector& s)
{ return SuTermVector(s,sin(x_1)); }

inline SuTermVector cos(const SuTermVector& s)
{ return SuTermVector(s,cos(x_1)); }

inline SuTermVector tan(const SuTermVector& s)
{ return SuTermVector(s,tan(x_1)); }

inline SuTermVector asin(const SuTermVector& s)
{ return SuTermVector(s,asin(x_1)); }

inline SuTermVector acos(const SuTermVector& s)
{ return SuTermVector(s,acos(x_1)); }

inline SuTermVector atan(const SuTermVector& s)
{ return SuTermVector(s,atan(x_1)); }

inline SuTermVector sinh(const SuTermVector& s)
{ return SuTermVector(s,sinh(x_1)); }

inline SuTermVector cosh(const SuTermVector& s)
{ return SuTermVector(s,cosh(x_1)); }

inline SuTermVector tanh(const SuTermVector& s)
{ return SuTermVector(s,tanh(x_1)); }

inline SuTermVector asinh(const SuTermVector& s)
{ return SuTermVector(s,asinh(x_1)); }

inline SuTermVector acosh(const SuTermVector& s)
{ return SuTermVector(s,acosh(x_1)); }

inline SuTermVector atanh(const SuTermVector& s)
{ return SuTermVector(s,atanh(x_1)); }

inline SuTermVector exp(const SuTermVector& s)
{ return SuTermVector(s,exp(x_1)); }

inline SuTermVector log(const SuTermVector& s)
{ return SuTermVector(s,log(x_1)); }

inline SuTermVector log10(const SuTermVector& s)
{ return SuTermVector(s,log10(x_1)); }

inline SuTermVector pow(const SuTermVector& s, const real_t &p)
{ return SuTermVector(s,x_1^p); }

//=================================================================================
/*!
   \class SuTermVectors
   end user class handling vector of SuTermVector
   it inherits from std::vector
*/
//=================================================================================
class SuTermVectors : public std::vector<SuTermVector>
{
  public:
    SuTermVectors(number_t n=0)   //! basic constructor
      {if (n>0) resize(n);}
    SuTermVectors(const std::vector<SuTermVector>& vs) //! constructor from vector<TermVector>
     : std::vector<SuTermVector>(vs) {}
    const SuTermVector& operator()(number_t n) const   //! acces to n-th TermVector (const)
       {if (n==0) error("bad_index_in_access_op");
        return at(n-1);}
    SuTermVector& operator()(number_t n)               //! acces to n-th TermVector
    {
      if (n==0) error("bad_index_in_access_op");
      return at(n-1);
    }
    void print(std::ostream&) const;     //!< output on stream
    SuTermVectors& toReal();             //!< move all TermVector's to real
    SuTermVectors& toImag();             //!< move all TermVector's to real
};
std::ostream& operator<<(std::ostream&, const SuTermVectors&);

//=================================================================================
//   predefined function used by function constructors
//=================================================================================
inline real_t fun_productR(const real_t& x, const real_t& y) {return x*y;}
inline complex_t fun_productC(const complex_t& x, const complex_t& y) {return x*y;}

//templated computation functions
#include "computation/FEVectorComputation.hpp"

} //end of namespace xlifepp

#endif /* SU_TERM_VECTOR_HPP */
