/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file BilinearFormAsLinearForm.hpp
  \author E. Lunéville
  \since 23 mar 2012
  \date  01 apr 2012

  \brief Definition of the xlifepp::LinearForm classes
*/
#ifndef BILINEARFORM_AS_LINEARFORM_HPP
#define BILINEARFORM_AS_LINEARFORM_HPP

#include "config.h"
#include "KernelOperatorOnTermVector.hpp"

namespace xlifepp
{
/*!
   \class BilinearFormAsLinearForm
   describes a linear form based on a Bilinearform a(u,v) where u or v are specified as a TermVector
*/
class BilinearFormAsLinearForm : public BasicLinearForm
{
  protected:
      BasicBilinearForm* bbf_;     //!< Basic bilinear form a(u,v)
      const TermVector* tv_;       //!< TermVector
  public:
      bool asUnknown_;        //!< true if tv_ is related to unknown u, false if tv_ is related to test function v

  //! Constructor with a reference of BasicLinearForm
  BilinearFormAsLinearForm(const BasicBilinearForm& bbf, const TermVector& tv, bool asUnknown)
     : bbf_(bbf.clone()), tv_(&tv), asUnknown_(asUnknown) {}
  //! Constructor with a pointer of BasicBilinearForm   
  BilinearFormAsLinearForm(BasicBilinearForm* bbf, const TermVector& tv, bool asUnknown)
     :  bbf_(bbf), tv_(&tv), asUnknown_(asUnknown) {if(asUnknown) u_p=&bbf->vp(); else u_p=&bbf->up();}
  
  //! copy-constructor
  BilinearFormAsLinearForm(const BilinearFormAsLinearForm& bfaslf)
     {
         u_p = bfaslf.u_p;
         bbf_= bfaslf.bbf_->clone();
         tv_= bfaslf.tv_;
         asUnknown_=bfaslf.asUnknown_;
     }
  //! operator = (copy-constructor)
  BilinearFormAsLinearForm& operator = (const BilinearFormAsLinearForm& bfaslf)
    {
         if(this == &bfaslf) return *this;
         if(bbf_!=nullptr) delete bbf_;
         u_p = bfaslf.u_p;
         bbf_= bfaslf.bbf_->clone();
         tv_= bfaslf.tv_;
         asUnknown_=bfaslf.asUnknown_;
         return *this;
    }
    //! destructor
  ~BilinearFormAsLinearForm()
    {if(bbf_!=nullptr) delete bbf_;}  
//! return the attribute basicLinearForm_
  BasicBilinearForm* bbf() const {return bbf_;}
//! return the attribute TermVector tv_
  const TermVector* termVector() const {return tv_;}
  virtual BasicLinearForm* clone() const        //! clone of the linear form
     {return new BilinearFormAsLinearForm(*this);}
  virtual LinearFormType type() const            //! return the type of the linear form
    {return _bilinearAsLinear;}
  virtual ValueType valueType() const            //! return the value type of the linear form
    {return bbf_->valueType();}
  virtual StrucType strucType() const            //! return the structure type of the linear form
    {if(asUnknown_) return bbf_->vp().strucType(); else return bbf_->up().strucType();}
  const GeomDomain* domain() const               //! return the pointer to the domain
    {if(asUnknown_) return &bbf_->dom_vp(); else return &bbf_->dom_up();}

  virtual string_t asString() const
    {return "";}
  virtual void print(std::ostream&) const;       //!< print utility
  virtual void print(PrintStream& os) const {print(os.currentStream());}  //!< print to current stream
};

//! main routine for double integrals involving Kernel and TermVector
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const std::vector<Parameter>& ps);

//@{
//! User single intg routines involving Kernel and TermVector (with up to 5 keys)
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const Parameter& p1);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const Parameter& p1, const Parameter& p2);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const Parameter& p1, const Parameter& p2, const Parameter& p3);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5);
//@}

//@{
/*!
  \deprecated use key-value system for optional arguments (_quad, _order, _method, ...)
*/
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu,
                const KernelOperatorOnTermVectorAndUnknown& koptvv, const IntegrationMethod& im);
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu,
                const KernelOperatorOnTermVectorAndUnknown& koptvv, QuadRule qr, number_t qo);
//@}

} //end of namespace xlifepp

#endif // BILINEARFORM_AS_LINEARFORM_HPP
