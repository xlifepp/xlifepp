/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file EigenTerms.cpp
  \author Y. Lafranche
  \since 25 jul 2016
  \date 25 jul 2016

  \brief Implementation of functions related to the computation of eigen elements
*/

#include "config.h"
#include "TermMatrix.hpp"
#include "EigenTerms.hpp"
#include <cstdarg>

namespace xlifepp {
using std::vector;
using std::pair;

//------------------------------------------------------------------------------
// EigenElements and SvdElements member functions
//------------------------------------------------------------------------------

// External utilitary structure used only in the following constructor. To be put inside it
// when C++11 compiler will be enabled.
  struct CompEvCl {
    const std::vector<pair<complex_t,VectorEntry*> >& val;
    CompEvCl(const std::vector<pair<complex_t,VectorEntry*> >& valp, EigenSortKind esortk):val(valp){
      switch (esortk) {
        default:
        case   _decr_module: cmp_p = &CompEvCl::cmp_decrM; break;
        case _decr_realpart: cmp_p = &CompEvCl::cmp_decrR; break;
        case _decr_imagpart: cmp_p = &CompEvCl::cmp_decrI; break;
        case   _incr_module: cmp_p = &CompEvCl::cmp_incrM; break;
        case _incr_realpart: cmp_p = &CompEvCl::cmp_incrR; break;
        case _incr_imagpart: cmp_p = &CompEvCl::cmp_incrI; break;
      }
    }
    bool cmp_decrM (int i, int j) const { return (std::abs(val[i].first) > std::abs(val[j].first)); }
    bool cmp_decrR (int i, int j) const { return (   val[i].first.real() > val[j].first.real()   ); }
    bool cmp_decrI (int i, int j) const { return (   val[i].first.imag() > val[j].first.imag()   ); }
    bool cmp_incrM (int i, int j) const { return (std::abs(val[i].first) < std::abs(val[j].first)); }
    bool cmp_incrR (int i, int j) const { return (   val[i].first.real() < val[j].first.real()   ); }
    bool cmp_incrI (int i, int j) const { return (   val[i].first.imag() < val[j].first.imag()   ); }
    bool (CompEvCl::*cmp_p) (int i, int j) const;
    bool operator() (int i, int j) { return (this->*cmp_p)(i,j); }
  };

//! main constructor
EigenElements::EigenElements(const TermMatrix* A_p, bool singleUnknown, bool isr,
                             const std::vector<std::pair<complex_t,VectorEntry*> >& vvp,
                             EigenSortKind esortk, string_t resname)
: realEigV_(isr), values(vvp.size()), vectors(vvp.size()) {
  void storeEigenVector(const TermMatrix* A_p, ValueType vt, bool singleUnknown, VectorEntry* eigvec, TermVector& resVec);
// struct CompEvCl {...} to be moved here
// Sort eigenvalues according to input specification
  std::vector<int> ind(vvp.size());
  for (int i=0; i<ind.size(); i++) { ind[i] = i; }
  sort (ind.begin(), ind.end(), CompEvCl(vvp, esortk));

  ValueType type = (realEigV_) ? _real: _complex;
  for (size_t i = 0; i < vvp.size(); i++) {
    values[i] = vvp[ind[i]].first;
    storeEigenVector(A_p, type, singleUnknown, vvp[ind[i]].second, vectors[i]);
  }
  vectors.name(resname);  // modify name of TermVectors and corresponding SuTermVectors only once they are created
}

//! Output on stream
void EigenElements::print(std::ostream& os) const {
  os<<"number of eigen elements: "<<values.size()<<eol;
  std::vector<complex_t>::const_iterator itval=values.begin();
  std::vector<TermVector>::const_iterator itvec=vectors.begin();
  number_t i=1;
  if (theVerboseLevel<=1) return;
  for (; itval!=values.end(); itval++, itvec++, i++) {
    os<<"  lambda_"<<i<<" = "<<*itval<<eol;
    if (theVerboseLevel>2) os<<"  eigen vector: "<<*itvec<<eol;
  }
}

//! main constructor
SvdElements::SvdElements(const TermMatrix& A, const EigenElements& ee)
: S_(ee.numberOfEigenValues()), TermU_(S_.size()), TermV_(S_.size()) {
  for (size_t i=0; i<ee.values.size(); i++) {
    // Although the eigenvalues are all >= 0, abs is taken below because, when the matrix
    // is singular, the smallest eigenvalue (in theory 0) may be numerically slightly < 0.
    S_[i] = std::sqrt(std::abs(ee.values[i].real()));
  }
  if (A.numberOfRows() >= A.numberOfCols()) {
    TermV_ = ee.vectors;
    // Compute left singular vectors: U = A * V * S^-1
    for (size_t i=0; i<S_.size(); i++) {
      TermU_[i] = A * TermV_[i];
      if (S_[i] > theTolerance) { TermU_[i] /= S_[i]; }
    }
  }
  else {
    TermU_ = ee.vectors;
    bool isR = TermU_[0].valueType() == _real;
    // Compute right singular vectors V = A' * U * S^-1
    for (size_t i=0; i<S_.size(); i++) {
      if (isR) { TermV_[i] = TermU_[i] * A; }
      else {
        Vector<complex_t>& vecU = *(TermU_[i].actual_entries()->entriesp<complex_t>());
        for (Vector<complex_t>::iterator it=vecU.begin(); it!=vecU.end(); it++) { *it = std::conj(*it); }
        TermV_[i] = TermU_[i] * A;
        for (Vector<complex_t>::iterator it=vecU.begin(); it!=vecU.end(); it++) { *it = std::conj(*it); }
        Vector<complex_t>& vecV = *(TermV_[i].actual_entries()->entriesp<complex_t>());
        for (Vector<complex_t>::iterator it=vecV.begin(); it!=vecV.end(); it++) { *it = std::conj(*it); }
      }
      if (S_[i] > theTolerance) { TermV_[i] /= S_[i]; }
    }
  }
}

template<class K_>
void SvdElements::buildSvdElements(const LargeMatrix<K_>* mat_p,
                         const std::vector<pair<complex_t, VectorEntry*> >& vvp, EigenSortKind esortk) {
// Sort eigenvalues according to input specification
  std::vector<int> ind(vvp.size());
  for (int i=0; i<ind.size(); i++) { ind[i] = i; }
  sort (ind.begin(), ind.end(), CompEvCl(vvp, esortk));

  for (size_t i=0; i<S_.size(); i++) {
    // Although the eigenvalues are all >= 0, abs is taken below because, when the matrix
    // is singular, the smallest eigenvalue (in theory 0) may be numerically slightly < 0.
    S_[i] = std::sqrt(std::abs(vvp[ind[i]].first.real()));
    U_[i] = V_[i] = vvp[ind[i]].second;
  }
  if (mat_p->numberOfRows() >= mat_p->numberOfCols()) {
    // Compute left singular vectors
    for (size_t i=0; i<S_.size(); i++) {
      U_[i] = new VectorEntry(*U_[i]);
      multMatrixVector(*mat_p, *V_[i]->entriesp<K_>(), *U_[i]->entriesp<K_>());
      *U_[i] /= S_[i];
    }
  }
  else {
    // Compute right singular vectors
    for (size_t i=0; i<S_.size(); i++) {
      V_[i] = new VectorEntry(*V_[i]);
      // conjuguer UU
      multVectorMatrix(*mat_p, *U_[i]->entriesp<K_>(), *V_[i]->entriesp<K_>());
      // conjuguer UU et VV
      *V_[i] /= S_[i];
    }
  }
}

SvdElements::SvdElements(const LargeMatrix<real_t>* mat_p,
                         const std::vector<std::pair<complex_t, VectorEntry*> >& vvp, EigenSortKind esortk)
: S_(vvp.size()), U_(S_.size()), V_(S_.size()) { buildSvdElements(mat_p, vvp, esortk); }

//------------------------------------------------------------------------------
//  External functions related to EigenElements and SvdElements classes
//------------------------------------------------------------------------------

std::ostream& operator<<(std::ostream& os, const EigenElements& evs) {
  evs.print(os);
  return os;
}

//! save eigenvalues into one file and eigenvectors into separate files
void saveToFile(const string_t& filename, const EigenElements& evs, const std::vector<Parameter>& ps)
{
  std::pair<string_t, string_t> rootext=fileRootExtension(trim(filename), Environment::authorizedSaveToFileExtensions());
  if (rootext.second == "") { rootext.second="txt"; }
  string_t filename2=fileNameFromComponents(rootext.first, "eigenvalues", "txt");
  std::ofstream fout(filename2.c_str());
  fout.precision(fullPrec);
  for (vector<complex_t>::const_iterator itval=evs.values.begin(); itval!=evs.values.end(); itval++)
  {
    if (evs.isReal()) { fout << itval->real(); }
    else              { fout << itval->real() << " " << itval->imag(); }
    fout << std::endl;
  }
  fout.close();
  saveToFile(filename, evs.vectors, ps);
}

//! save singular values into one file and singular vectors into separate files
void saveToFile(const string_t& filename, const SvdElements& evs, const std::vector<Parameter>& ps)
{
  std::pair<string_t, string_t> rootext=fileRootExtension(trim(filename), Environment::authorizedSaveToFileExtensions());
  if (rootext.second == "") { rootext.second="txt"; }
  std::string filename2=fileNameFromComponents(rootext.first, "singularvalues", "txt");
  std::ofstream fout(filename2.c_str());
  fout.precision(fullPrec);
  for (vector<real_t>::const_iterator itval=evs.svalues().begin(); itval != evs.svalues().end(); itval++)
  { fout << *itval << std::endl; }
  fout.close();
  if (evs.leftSTermVectors().size() > 0)
  {
    filename2=fileNameFromComponents(rootext.first, "U", rootext.second);
    saveToFile(filename2, evs.leftSTermVectors(), ps);
    filename2=fileNameFromComponents(rootext.first, "V", rootext.second);
    saveToFile(filename2, evs.rightSTermVectors(), ps);
  }
  else
  {
    for (int i=1; i<=evs.leftSVectors().size(); i++)
    {
      filename2=fileNameFromComponents(rootext.first, "U_"+tostring(i), "txt");
      evs.leftSVector(i)->saveToFile(filename2);
      filename2=fileNameFromComponents(rootext.first, "V_"+tostring(i), "txt");
      evs.rightSVector(i)->saveToFile(filename2);
    }
  }
}

//------------------------------------------------------------------------------
//  Utility functions
//------------------------------------------------------------------------------
//! Utility function to store data of an eigenvector into a TermVector
//  (internal tool used in EigenElements constructor only)
void storeEigenVector(const TermMatrix* A_p, ValueType vt, bool singleUnknown, VectorEntry* eigvec, TermVector& resVec) {
  A_p->initTermVector(resVec, vt, true);
  if (singleUnknown) {
    const Unknown* u = A_p->begin()->first.first;
    SuTermVector* vX = resVec.subVector_p(u);
    if (vX == nullptr) vX = resVec.subVector_p(u->dual_p());
    //vX->entries() = eigvec;
    if(vX->nbOfComponents()>1)  //move to vector representation
    {
       vX->scalar_entries() = eigvec;
       delete vX->entries();
       vX->entries()=nullptr;
       vX->cdofs() = A_p->begin()->second->cdofsu();
       vX->toVector(false);
    }
    else vX->entries() = eigvec;
  }
  else {
    resVec.cdofs() = A_p->cdofsc();
    resVec.scalar_entries() = eigvec;
    resVec.toLocal(false);
    //resVec.toVector(false);
  }
  resVec.markAsComputed(true);
}

//! Utility structure to store parameters for eigen solvers
struct EigPars {
  number_t maxIt;
  EigenComputationalMode mode;
  number_t ncv, nev;
  complex_t sigma;
  EigenSortKind sortKind;
  real_t tolerance;
  number_t verboseLevel;
  string_t which;
  string_t name;
};

/*! Parser of user parameters for eigen solvers (internal tool)
    Returns the list of parameter keys specified by the user (usedParams)
    and the parameter values to be used (ep).
    Nota: the solver parameter is skipped.
*/
void parseEigenPars(const std::vector<Parameter> ps, const number_t nevmax,
                    std::set<ParameterKey>& usedParams, EigPars& ep) {
  trace_p->push("parseEigenPars(inputps,  params, usedParams)");
  std::set<ParameterKey> params;
  // Authorized keys and default values of parameters for the internal eigensolver:
  params.insert(_pk_convToStd);
  params.insert(_pk_forceNonSym);
  params.insert(_pk_maxIt);           ep.maxIt = defaultMaxIterations;
  params.insert(_pk_mode);            ep.mode = _krylovSchur; // default value for internal solver, no default value for Arpack
  params.insert(_pk_ncv);             ep.ncv=0;
  params.insert(_pk_nev);             ep.nev = std::min(number_t(10), nevmax-1); // for all solvers
  params.insert(_pk_sigma);           ep.sigma = complex_t(0.,0.);
  params.insert(_pk_solver); // skipped, but should be present here
  params.insert(_pk_sort);            ep.sortKind = _incr_module;
  params.insert(_pk_tolerance);       ep.tolerance = 1.e-6;
  params.insert(_pk_verbose);         ep.verboseLevel = 0;
  params.insert(_pk_which);           ep.which = string_t("LM");
  // The last one is used to transmit information to the final result object:
  params.insert(_pk_name);            ep.name = string_t("eigvec");

  int algospec=0;
  // Check each given key and retrieve associated value:
  for(number_t i=0; i < ps.size(); ++i) {
    ParameterKey key=ps[i].key();
    switch(key) {
      case _pk_convToStd:
      case _pk_forceNonSym:
        break;
      case _pk_maxIt:
        switch(ps[i].type())
        {
          case _integer: ep.maxIt = ps[i].get_n(); break;
          default: error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      case _pk_mode:
        switch(ps[i].type())
        {
          case _integer:
          case _enumEigenComputationalMode:
            ep.mode = EigenComputationalMode(ps[i].get_n());
            break;
          default: error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      case _pk_ncv:
        switch(ps[i].type())
        {
          case _integer: ep.ncv = ps[i].get_n(); break;
          default: error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      case _pk_nev:
        switch(ps[i].type())
        {
          case _integer: ep.nev = ps[i].get_n(); break;
          default: error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      case _pk_sigma:
        switch(ps[i].type())
        {
          case _integer: ep.sigma = complex_t(real_t(ps[i].get_i()),0.); break;
          case _real:    ep.sigma = complex_t(ps[i].get_r(),0.);         break;
          case _complex: ep.sigma = ps[i].get_c();                       break;
          default: error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        algospec++;
        break;
      case _pk_solver:
        break;
      case _pk_sort:
        switch(ps[i].type())
        {
          case _integer:
          case _enumEigenSortKind:
            ep.sortKind = EigenSortKind(ps[i].get_n());
            break;
          default: error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      case _pk_tolerance:
        switch(ps[i].type())
        {
          case _real: ep.tolerance = ps[i].get_r(); break;
          default: error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      case _pk_verbose:
        switch(ps[i].type())
        {
          case _integer: ep.verboseLevel = ps[i].get_n(); break;
          default: error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      case _pk_which:
        switch(ps[i].type())
        {
          case _string: ep.which = ps[i].get_s(); break;
          default: error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        algospec++;
        break;
      case _pk_name:
        switch(ps[i].type())
        {
          case _string: ep.name = ps[i].get_s(); break;
          default: error("param_badtype",words("value",ps[i].type()),words("param key",key));
        }
        break;
      default: error("unexpected_param_key",words("param key",key));
    } // end switch(key)
    if (params.find(key) != params.end()) { params.erase(key); }
    else {
      if(usedParams.find(key) == usedParams.end())
           { error("unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    if (key != _pk_solver) { usedParams.insert(key); }
  } // end for
  if (algospec == 2) {
    // Both sigma and which have been specified. These parameters are incompatible:
    // warn the user and keep only sigma.
    usedParams.erase(_pk_which);
    info("incompatible_params");
  }
  trace_p->pop();
}

// Utility function that creates a vector containing the nbpar Parameter objects
// whose addresses are passed as arguments (after nbpar).
// To be replaced by an initializer list when C++11 will be enabled.
std::vector<Parameter> makeParList(int nbpar, ...) {
  va_list vl;
  va_start(vl,nbpar);
  std::vector<Parameter> parList;
  for (int i=0; i<nbpar ; i++) {
    const Parameter* par_p = va_arg(vl, const Parameter*);
    parList.push_back(*par_p);
  }
  va_end(vl);
  return parList;
}

//------------------------------------------------------------------------------
//  Solvers for eigenvalue problems and SVD decomposition
//------------------------------------------------------------------------------

//! Internal eigen solver
EigenElements eigenInternGen(TermMatrix* pA, TermMatrix* ptB, number_t nev, string_t which, real_t tol,
                             EigenComputationalMode eigCompMode, complex_t sigma, bool isShift, string_t nam,
                             EigenSortKind esortk = _incr_module) {
  bool isRealShift(sigma.imag() == 0.);
  TermVector X("X solves A*X=lambda*X, A=" + pA->name());
  TermMatrix* pB = ptB;
  if(nullptr == pB) pB = pA;
  MatrixEntry* matA=nullptr, *matB = nullptr;
  std::vector<std::pair<complex_t,VectorEntry* > > eigenRes;
  bool isSingleUnknown = (pA->isSingleUnknown() && pB->isSingleUnknown());

  // Compute eigenvalues and eigenvectors
  if(isSingleUnknown) {
    SuTermMatrix* sutmA = pA->begin()->second;
    SuTermMatrix* sutmB = pB->begin()->second;
    if(sutmA->space_up() != sutmA->space_vp()) { error("term_incompatible_spaces"); }
    if(sutmB->space_up() != sutmB->space_vp()) { error("term_incompatible_spaces"); }
    matA= sutmA->entries();
    matB= sutmB->entries();
    if(sutmA->strucType()==_matrix && sutmA->scalar_entries()==nullptr) sutmA->toScalar(false);  //force moving to scalar representation
    if(sutmB->strucType()==_matrix && sutmB->scalar_entries()==nullptr) sutmB->toScalar(false);
    if(sutmA->scalar_entries()!=nullptr) matA= sutmA->scalar_entries();
    if(sutmB->scalar_entries()!=nullptr) matB= sutmB->scalar_entries();
    if(matA->valueType_!=matB->valueType_) { //align ValueType, one is complex and the other is real
      if(matA->valueType_==_real) matA->toComplex();
      if(matB->valueType_==_real) matB->toComplex();
    }

    if(nullptr != ptB) eigenRes = matA->eigenSolve(matB, eigCompMode, nev, tol, which, sigma, isRealShift, isShift);
    else eigenRes = matA->eigenSolve(eigCompMode, nev, tol, which, sigma, isRealShift, isShift);
  }
  else {  //multiple unknowns (goto scalar global representation)
    //prevent misfit unknowns
    if(pA->rowUnknowns()!=pB->rowUnknowns() || pA->colUnknowns()!=pB->colUnknowns())
      { error("term_inconsistent_unknowns"); }
    SymType syA=pA->symmetry();  //detect global symmetry of TermMatrix
    if(pA->entries()==nullptr && pA->scalar_entries()==nullptr) pA->toGlobal(pA->storageType(), pA->storageAccess(), syA, true);
    else if(pA->scalar_entries()==nullptr) pA->toScalar(false);
    matA = pA->scalar_entries();
    SymType syB=pB->symmetry();  //detect global symmetry of TermMatrix
    if(pB->entries()==nullptr && pB->scalar_entries()==nullptr) pB->toGlobal(pB->storageType(), pB->storageAccess(), syB, true);
    else if(pB->scalar_entries()==nullptr) pB->toScalar(false);
    matB =pB->scalar_entries();

    if(matA->valueType_!=matB->valueType_) { //align ValueType, one is complex and the other is real
      if(matA->valueType_==_real) matA->toComplex();
      if(matB->valueType_==_real) matB->toComplex();
    }
    //matA->saveToFile("Ag.dat",_coo,true); matB->saveToFile("Bg.dat",_coo,true);
    if(nullptr != ptB) eigenRes = matA->eigenSolve(matB, eigCompMode, nev, tol, which, sigma, isRealShift, isShift);
    else eigenRes = matA->eigenSolve(eigCompMode, nev, tol, which, sigma, isRealShift, isShift);
  }

  if ( ! eigenRes.empty() ) { // return results
    bool isr = pA->valueType() == _real && pA->symmetry() == _symmetric;
    EigenElements eigElts(pA, isSingleUnknown, isr, eigenRes, esortk, nam);
    if(pA->constraints_u()!=nullptr) eigElts.applyEssentialConditions(*pA->constraints_u());
    return eigElts;
  }
  else {
    warning("eigenvalues_not_found");
    return EigenElements();
  }
}

//! Main entry point for intern eigenvalue solver
EigenElements eigenInternSolve(TermMatrix* A, TermMatrix* B, const std::vector<Parameter>& ps) {
  std::set<ParameterKey> usedParams;
  EigPars ep;
  parseEigenPars(ps, A->numberOfCols(), usedParams, ep);

  // Test to know if the user has set sigma or not
  bool isShift = usedParams.find(_pk_sigma) != usedParams.end();
  return eigenInternGen(A, B, ep.nev, ep.which, ep.tolerance, ep.mode, ep.sigma, isShift, ep.name, ep.sortKind);
}

#ifdef XLIFEPP_WITH_ARPACK
  // Declaration of function called only in arpackSolve or eigenSolve
  EigenElements arpCompute(TermMatrix* A, TermMatrix* B, int nev, EigenComputationalMode mode,
                          const std::vector<Parameter>& pars, const std::set<ParameterKey>& upars);
  EigenElements arpCheckNCompute(TermMatrix* A, TermMatrix* B, int nev, EigenComputationalMode mode,
                                const std::vector<Parameter>& pars, const std::set<ParameterKey>& upars);
#endif


//! Main entry point for Arpack eigenvalue solver
EigenElements arpackSolve(TermMatrix* A, TermMatrix* B, const std::vector<Parameter>& ps) {
  #ifdef XLIFEPP_WITH_ARPACK
    TermMatrix* At=A, *Bt=B;
    EigenElements res;
    // as arpack uses largeMatrix's in that context, A and B must have the same size; align them if not the same size
    At->matrixData();           // move to global or scalar
    if(Bt!=nullptr) Bt->matrixData(); // move to global or scalar
    if(Bt!=nullptr) alignTermMatrix(At,Bt);
    std::set<ParameterKey> usedParams;
    EigPars ep;
    parseEigenPars(ps, At->numberOfCols(), usedParams, ep);
    bool convToStd = usedParams.find(_pk_convToStd) != usedParams.end();
    if (convToStd) { res = arpCheckNCompute(At, Bt, ep.nev, ep.mode, ps, usedParams); }
    else           { res = arpCompute(At, Bt, ep.nev, ep.mode, ps, usedParams); }
    if(At!=A) delete At;
    if(Bt!=B) delete Bt;
    return res;
  #else
    error("xlifepp_without_arpack");
    return EigenElements(); // dummy return
  #endif
}

//! Main entry point for non specific eigenvalue solver
EigenElements eigenSolve(TermMatrix* A, TermMatrix* B, std::vector<Parameter> ps) {
  // Set default value of solver
  EigenSolverType solverType = _intern;
  #ifdef XLIFEPP_WITH_ARPACK
    solverType = _arpack;
  #endif
  // Retrieve selected eigen solver if any:
  for (std::vector<Parameter>::const_iterator itp=ps.begin() ; itp != ps.end(); itp++) {
    if (itp->key() == _pk_solver) { solverType = EigenSolverType(itp->get_n()); break; }
  }

  if (solverType == _intern) { return eigenInternSolve(A, B, ps); }
  // else solverType ==_arpack
  #ifdef XLIFEPP_WITH_ARPACK
    // If  _which = "SM"  is requested, replace it by  _sigma = 0.  statistically more powerful.
    // -> this mode can however be used by directly calling arpackSolve instead of eigenSolve
    for (std::vector<Parameter>::iterator itp=ps.begin() ; itp != ps.end(); itp++) {
      if (itp->key() == _pk_which && itp->get_s() == string_t("SM")) {
        *itp = _sigma = 0.;
        info("change_mode");
        break;
      }
    }
    std::set<ParameterKey> usedParams;
    EigPars ep;
    parseEigenPars(ps, A->numberOfCols(), usedParams, ep);
    number_t defaultNcv = std::min(std::max(2*ep.nev+1,number_t(20)),A->numberOfCols());
    if (ep.ncv == 0)
    {
      ep.ncv=defaultNcv;
      info("ncv_info", ep.ncv);
    }
    else if (ep.ncv < defaultNcv)
    {
      warning("ncv_too_low", ep.ncv, defaultNcv);
    }
    return arpCheckNCompute(A, B, ep.nev, ep.mode, ps, usedParams);
  #else
    error("xlifepp_without_arpack");
    return EigenElements(); // dummy return
  #endif
}

//! Main entry point for SVD decomposition using Arpack
SvdElements svd(TermMatrix* A, const std::vector<Parameter>& ps) {
  #ifdef XLIFEPP_WITH_ARPACK
    std::set<ParameterKey> usedParams;
    EigPars ep;
    parseEigenPars(ps, A->numberOfCols(), usedParams, ep);

    // The following function is to be called only here:
    SvdElements arpSVDCompute(TermMatrix* A, int nev, const std::vector<Parameter>& pars,
                              const std::set<ParameterKey>& upars);
    return arpSVDCompute(A, ep.nev, ps, usedParams);
  #else
    error("xlifepp_without_arpack");
    return SvdElements(); // dummy return
  #endif
}

} // end of namespace xlifepp
