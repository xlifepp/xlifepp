/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file PreconditionerTerm.cpp
  \author C. Chambeyron, D. Martin, Ha NGUYEN, N. Kielbasiewicz
  \since 6 Sept 2005
  \date  17 Nov 2016

  \brief Definition of the xlifepp::PreconditionerTerm class

  members:
  --------
    - precondMatrix_p    [ pointer to TermMatrix ]       pointer to precondition matrix
    - type_              [ PreconditionerType, private ] (see enum in init/config.hpp)
    - omega_             [ Real, private ]               relaxation parameter for SSOR preconditioners
*/

#include "PreconditionerTerm.hpp"
#include "TermMatrix.hpp"
#include "TermVector.hpp"

namespace xlifepp
{

PreconditionerTerm::PreconditionerTerm(TermMatrix& A, PreconditionerType pt, const real_t w)
: Preconditioner(pt), precondMatrix_p(nullptr), omega_(w)
{
  //elapsedTime();
  if ( (_noFactorization == A.factorization()) && (_ssorPrec != type_) && (_diagPrec != type_) && (_productPrec != type_))
  {
   precondMatrix_p = new TermMatrix();
   isAllocated = true;
   switch (type_)
    {
      case _luPrec:       factorize(A, *precondMatrix_p, _lu);   break;
      case _ldltPrec:     factorize(A, *precondMatrix_p, _ldlt);   break;
      case _ldlstarPrec:  factorize(A, *precondMatrix_p, _ldlstar);   break;
      case _iluPrec:     iFactorize(A, *precondMatrix_p, _ilu);   break;
      case _illtPrec:    iFactorize(A, *precondMatrix_p, _illt);   break;
      case _ildltPrec:   iFactorize(A, *precondMatrix_p, _ildlt);   break;
      case _ildlstarPrec:iFactorize(A, *precondMatrix_p, _ildlstar);   break;
      default:
        std::cout<<"unknown preconditioner"<<std::endl;
       break;
    }
  }
  else
  {
   precondMatrix_p = &A;
   isAllocated = false;
 }
 //elapsedTime("factorisation time: ",theCout);
}

//! destructor
PreconditionerTerm::~PreconditionerTerm() { if (isAllocated) { delete precondMatrix_p; } }

//! test if Preconditioner is empty or not
ValueType PreconditionerTerm::valueType() const
{
  if (precondMatrix_p != nullptr) { return precondMatrix_p->valueType(); }
  error("null_pointer", "precondMatrix_p");
  return _none;
}

MatrixEntry* PreconditionerTerm::getPrecondEntries() const
{
  if (termVector_p->scalar_entries() != nullptr)
  {
    precondMatrix_p->toGlobal(precondMatrix_p->storageType(), precondMatrix_p->storageAccess(), precondMatrix_p->symmetry(), true);
    return precondMatrix_p->scalar_entries();
  }
  if (termVector_p->isSingleUnknown())
  {
    if (termVector_p->firstSut()->scalar_entries() != nullptr)
    {
      precondMatrix_p->toScalar();
      return precondMatrix_p->firstSut()->scalar_entries();
    }
    return precondMatrix_p->firstSut()->entries();
  }
  return nullptr;
}

//! Solver with B and X
void PreconditionerTerm::solve(TermVector& B, TermVector& X) const
{
  switch (type_)
  {
    case _ldltPrec:     // Preconditionner is a L.D.Lt factorized matrix
    case _ldlstarPrec:  // Preconditionner is a L.D.L* factorized matrix
    case _luPrec:       // Preconditionner is a L.U factorized matrix
    case _iluPrec:      // Preconditionner is a iL.U factorized matrix
    case _illtPrec:     // Preconditionner is a iL.Lt factorized matrix
    case _ildltPrec:    // Preconditionner is a iL.D.Lt factorized matrix
    case _ildlstarPrec: // Preconditionner is a iL.D.L* factorized matrix
    {
      if (_noFactorization != precondMatrix_p->factorization() ) X=factSolve(*precondMatrix_p, B);
      else
      {
        where("PreconditionerTerm::solve(TermVector,TermVector)");
        error("term_not_factorized", name());
      }
      break;
    }
    case _diagPrec:
      precondMatrix_p->sorDiagonalSolve(B, X, 1.);
      break;
    case _ssorPrec:     // Preconditionner is an iterative SSOR solver
      precondMatrix_p->sorLowerSolve(B, X, omega_);
      precondMatrix_p->sorDiagonalMatrixVector(X, X, omega_);
      precondMatrix_p->sorUpperSolve(X, X, omega_);
      break;
    case _productPrec: // when precondMatrix_p is already an inverse
      multMatrixVector(*precondMatrix_p,B,X);
      break;
    default:
      break;
  }
}

void PreconditionerTerm::solve(VectorEntry& B, VectorEntry& X) const
{
  MatrixEntry* precondMatrixEntry_p=this->getPrecondEntries();
  switch (type_)
  {
    case _ldltPrec:     // Preconditionner is a L.D.Lt factorized matrix
    case _ldlstarPrec:  // Preconditionner is a L.D.L* factorized matrix
    case _luPrec:       // Preconditionner is a L.U factorized matrix
    case _iluPrec:      // Preconditionner is a iL.U factorized matrix
    case _illtPrec:     // Preconditionner is a iL.Lt factorized matrix
    case _ildltPrec:    // Preconditionner is a iL.D.Lt factorized matrix
    case _ildlstarPrec: // Preconditionner is a iL.D.L* factorized matrix
    {
      if (_noFactorization != precondMatrixEntry_p->factorization() ) X=factSolve(*precondMatrixEntry_p, B);
      else
      {
        where("PreconditionerTerm::solve(TermVector,TermVector)");
        error("term_not_factorized", name());
      }
      break;
    }
    case _diagPrec:
      precondMatrixEntry_p->sorDiagonalSolve(B, X, 1.);
      break;
    case _ssorPrec:     // Preconditionner is an iterative SSOR solver
      precondMatrixEntry_p->sorLowerSolve(B, X, omega_);
      precondMatrixEntry_p->sorDiagonalMatrixVector(X, X, omega_);
      precondMatrixEntry_p->sorUpperSolve(X, X, omega_);
      break;
    case _productPrec:  // when precondMatrixEntry_p is already an inverse
      multMatrixVector(*precondMatrixEntry_p,B,X);
      break;
    default:
      break;
  }
}

//! Solver with B
TermVector PreconditionerTerm::solve(TermVector& B) const
{
  TermVector X=B;
  solve(B, X);
  return X;
}
VectorEntry PreconditionerTerm::solve(VectorEntry& B) const
{
  VectorEntry X=B;
  solve(B, X);
  return X;
}

//! Transpose Solver with B and X
void PreconditionerTerm::leftSolve(TermVector& B, TermVector& X) const
{
  if (precondMatrix_p->symmetry() == _symmetric) { this->solve(B, X); return;}
  switch (type_)
  {
    case _ldltPrec:     // Preconditionner is a L.D.Lt factorized matrix
    case _ldlstarPrec:  // Preconditionner is a L.D.L* factorized matrix
    case _luPrec:       // Preconditionner is a L.U factorized matrix
    case _iluPrec:      // Preconditionner is a iL.U factorized matrix
    case _illtPrec:     // Preconditionner is a iL.Lt factorized matrix
    case _ildltPrec:    // Preconditionner is a iL.D.Lt factorized matrix
    case _ildlstarPrec: // Preconditionner is a iL.D.L* factorized matrix
    {
      if (_noFactorization != precondMatrix_p->factorization() ) X=factLeftSolve(*precondMatrix_p, B);
      else
      {
        where("PreconditionerTerm::solve(TermVector,TermVector)");
        error("term_not_factorized", name());
      }
      break;
    }
    case _diagPrec:
      precondMatrix_p->sorDiagonalSolve(B, X, 1.);
      break;
    case _ssorPrec:     // Preconditionner is an iterative SSOR solver
      precondMatrix_p->sorUpperSolve(B, X, omega_);
      precondMatrix_p->sorDiagonalMatrixVector(X, X, omega_);
      precondMatrix_p->sorLowerSolve(X, X, omega_);
      break;
    case _productPrec: // when precondMatrix_p is already an inverse
      multVectorMatrix(B,*precondMatrix_p,X);
      break;
    default:
      break;
  }
}

void PreconditionerTerm::leftSolve(VectorEntry& B, VectorEntry& X) const
{
  MatrixEntry* precondMatrixEntry_p=this->getPrecondEntries();
  switch (type_)
  {
    case _ldltPrec:     // Preconditionner is a L.D.Lt factorized matrix
    case _ldlstarPrec:  // Preconditionner is a L.D.L* factorized matrix
    case _luPrec:       // Preconditionner is a L.U factorized matrix
    case _iluPrec:      // Preconditionner is a iL.U factorized matrix
    case _illtPrec:     // Preconditionner is a iL.Lt factorized matrix
    case _ildltPrec:    // Preconditionner is a iL.D.Lt factorized matrix
    case _ildlstarPrec: // Preconditionner is a iL.D.L* factorized matrix
    {
      if (_noFactorization != precondMatrixEntry_p->factorization() ) X=factLeftSolve(*precondMatrixEntry_p, B);
      else
      {
        where("PreconditionerTerm::leftSolve(VectorEntry, VectorEntry)");
        error("term_not_factorized", name());
      }
      break;
    }
    case _diagPrec:
      precondMatrixEntry_p->sorDiagonalSolve(B, X, 1.);
      break;
    case _ssorPrec:     // Preconditionner is an iterative SSOR solver
      precondMatrixEntry_p->sorLowerSolve(B, X, omega_);
      precondMatrixEntry_p->sorDiagonalMatrixVector(X, X, omega_);
      precondMatrixEntry_p->sorUpperSolve(X, X, omega_);
      break;
    case _productPrec:  // when precondMatrixEntry_p is already an inverse
      multMatrixVector(*precondMatrixEntry_p,B,X);
      break;
    default:
      break;
  }
}

//! LeftSolver with B
TermVector PreconditionerTerm::leftSolve(TermVector& B) const
{
  TermVector X=B;
  leftSolve(B, X);
  return X;
}
VectorEntry PreconditionerTerm::leftSolve(VectorEntry& B) const
{
  VectorEntry X=B;
  leftSolve(B, X);
  return X;
}

} // namespace xlifepp
