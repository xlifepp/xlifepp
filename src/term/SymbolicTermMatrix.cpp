/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SymbolicTermMatrix.cpp
  \author E. Lunéville
  \since 21 dec 2017
  \date 21 dec 2017

  \brief implementation of SymbolicTermMatrix related functions

*/

#include "SymbolicTermMatrix.hpp"

namespace xlifepp
{
SymbolicTermMatrix::SymbolicTermMatrix(LcTerm<TermMatrix>& LC)
{
  if (LC.size()==0)
  {
    where("SymbolicTermMatrix::SymbolicTermMatrix(LcTerm)");
    error("is_void","LcTerm");
  }
  add(LC,LC.begin());
}

void SymbolicTermMatrix::add(LcTerm<TermMatrix>& LC, std::vector<std::pair<const TermMatrix*, complex_t> >::iterator itlc)
{
  if(itlc==LC.end()) return;
  std::vector<std::pair<const TermMatrix*, complex_t> >::iterator it=itlc+1;
  if(it==LC.end())
    {
      op_=_idop;
      tm_=(*itlc).first;
      coef_=(*itlc).second;
      st1_=nullptr; st2_=nullptr;
      delMat_=false;
      return;
    }
  op_=_plus; tm_=nullptr;
  st1_=new SymbolicTermMatrix(*((*itlc).first),(*itlc).second);
  st2_=new SymbolicTermMatrix();
  st2_->add(LC,it);
}

SymbolicTermMatrix::SymbolicTermMatrix(const SymbolicTermMatrix& S)
{
  op_=S.op_;
  tm_=S.tm_;
  coef_=S.coef_;
  if(S.st1_!=nullptr) st1_=new SymbolicTermMatrix(*S.st1_);
  else st1_=nullptr;
  if(S.st2_!=nullptr) st2_=new SymbolicTermMatrix(*S.st2_);
  else st2_=nullptr;
  delMat_=false;
  if(S.delMat_ && S.tm_!=nullptr) {tm_=new TermMatrix(*S.tm_); delMat_=true;} //FULL COPY of TermMatrix to be safe
}

SymbolicTermMatrix::~SymbolicTermMatrix()
{
  if(st1_!=nullptr) delete st1_;
  if(st2_!=nullptr) delete st2_;
  if(delMat_ && tm_!=nullptr) delete tm_;
}

SymbolicTermMatrix& SymbolicTermMatrix::operator=(const SymbolicTermMatrix& S)
{
    if(st1_!=nullptr) delete st1_;
    if(st2_!=nullptr) delete st2_;
    if(delMat_ && tm_!=nullptr) delete tm_;
    st1_=nullptr;st2_=nullptr;
    op_=S.op_;
    tm_=S.tm_;
    coef_=S.coef_;
    if(S.st1_!=nullptr) st1_=new SymbolicTermMatrix(*S.st1_);
    if(S.st2_!=nullptr) st2_=new SymbolicTermMatrix(*S.st2_);
    delMat_=false;
    if(S.delMat_ && S.tm_!=nullptr) {tm_=new TermMatrix(*S.tm_); delMat_=true;} //FULL COPY of TermMatrix to be safe
    return *this;
}

SymbolicTermMatrix& operator *(const TermMatrix& M, SymbolicTermMatrix& S)
{return *new SymbolicTermMatrix(_multiply,new SymbolicTermMatrix(M),&S);}

SymbolicTermMatrix& operator *(SymbolicTermMatrix& S, const TermMatrix& M)
{return *new SymbolicTermMatrix(_multiply,&S,new SymbolicTermMatrix(M));}

SymbolicTermMatrix& operator +(const TermMatrix& M, SymbolicTermMatrix& S)
{return *new SymbolicTermMatrix(_plus,new SymbolicTermMatrix(M),&S);}

SymbolicTermMatrix& operator +(SymbolicTermMatrix& S, const TermMatrix& M)
{return *new SymbolicTermMatrix(_plus,&S,new SymbolicTermMatrix(M));}

SymbolicTermMatrix& operator -(const TermMatrix& M, SymbolicTermMatrix& S)
{return *new SymbolicTermMatrix(_minus,new SymbolicTermMatrix(M),&S);}

SymbolicTermMatrix& operator -(SymbolicTermMatrix& S, const TermMatrix& M)
{return *new SymbolicTermMatrix(_minus,&S,new SymbolicTermMatrix(M));}

SymbolicTermMatrix& operator +(LcTerm<TermMatrix>& LC,SymbolicTermMatrix& S)
{return *new SymbolicTermMatrix(_plus,new SymbolicTermMatrix(LC),&S);}

SymbolicTermMatrix& operator +(SymbolicTermMatrix& S, LcTerm<TermMatrix>& LC)
{return *new SymbolicTermMatrix(_plus,&S,new SymbolicTermMatrix(LC));}

SymbolicTermMatrix& operator -(LcTerm<TermMatrix>& LC,SymbolicTermMatrix& S)
{return *new SymbolicTermMatrix(_minus,new SymbolicTermMatrix(LC),&S);}

SymbolicTermMatrix& operator -(SymbolicTermMatrix& S, LcTerm<TermMatrix>& LC)
{return *new SymbolicTermMatrix(_minus,&S,new SymbolicTermMatrix(LC));}

SymbolicTermMatrix& operator *(LcTerm<TermMatrix>& LC,SymbolicTermMatrix& S)
{return *new SymbolicTermMatrix(_multiply,new SymbolicTermMatrix(LC),&S);}

SymbolicTermMatrix& operator *(SymbolicTermMatrix& S, LcTerm<TermMatrix>& LC)
{return *new SymbolicTermMatrix(_multiply,&S,new SymbolicTermMatrix(LC));}

SymbolicTermMatrix& operator *(SymbolicTermMatrix& S1, SymbolicTermMatrix& S2)
{return *new SymbolicTermMatrix(_multiply,&S1,&S2);}

SymbolicTermMatrix& operator +(SymbolicTermMatrix& S1, SymbolicTermMatrix& S2)
{return *new SymbolicTermMatrix(_plus,&S1,&S2);}

SymbolicTermMatrix& operator -(SymbolicTermMatrix& S1, SymbolicTermMatrix& S2)
{return *new SymbolicTermMatrix(_minus,&S1,&S2);}


SymbolicTermMatrix& conj(SymbolicTermMatrix& S)
{return *new SymbolicTermMatrix(_conj,&S);}

SymbolicTermMatrix& adj(SymbolicTermMatrix& S)
{return *new SymbolicTermMatrix(_adj,&S);}

SymbolicTermMatrix& tran(SymbolicTermMatrix& S)
{return *new SymbolicTermMatrix(_tran,&S);}

SymbolicTermMatrix& inv(SymbolicTermMatrix& S)
{
  if (S.op_!=_idop || S.tm_==nullptr)
  {
    where("inv(SymbolicTermMatrix)");
    error("symbolic_tm_not_handled");
  }
  if (S.tm_->factorization()==_noFactorization)
  {
    TermMatrix* MF=new TermMatrix();
    xlifepp::factorize(const_cast<TermMatrix&>(*S.tm_),*MF,_lu); //force LU because UMFPACK data are not copied
    S.tm_=const_cast<const TermMatrix *>(MF);
    S.delMat_=true;
  }
  S.op_=_inv;
  return S;
}

SymbolicTermMatrix& inv(const TermMatrix& M)
{
  SymbolicTermMatrix* S=nullptr;
  //if M is not factorized, create factorization
  if(M.factorization()==_noFactorization)
    {
      TermMatrix* MF=new TermMatrix();
      xlifepp::factorize(const_cast<TermMatrix&>(M),*MF,_lu); //force LU because UMFPACK data are not copied
      S = new SymbolicTermMatrix(*MF);
      S->delMat_=true;
    }
  else S =new SymbolicTermMatrix(M);
  S->op_=_inv;
  return *S;
}

SymbolicTermMatrix& operator *(SymbolicTermMatrix& S, const complex_t& c)
{S.coef_*=c; return S;}

SymbolicTermMatrix& operator *(const complex_t& c, SymbolicTermMatrix& S)
{S.coef_*=c; return S;}

SymbolicTermMatrix& operator /(SymbolicTermMatrix& S, const complex_t& c)
{S.coef_/=c; return S;}

void SymbolicTermMatrix::print(std::ostream& out) const
{
    out<<asString();
}

string_t SymbolicTermMatrix::asString() const
{
  std::stringstream out;
  if (coef_!=complex_t(1.,0.))
  {
    if (coef_.imag()==0) out<< coef_.real();
    else out<<coef_;
    out<<"*";
  }
  switch (op_)
  {
    case _plus:    out<<"("<<*st1_<<" + "<<*st2_<<")"; break;
    case _minus:   out<<"("<<*st1_<<" - "<<*st2_<<")"; break;
    case _multiply:out<<"("<<*st1_<<" x "<<*st2_<<")"; break;
    case _idop:
      if(tm_!=nullptr) out<<tm_->primaryName();
      else out<<*st1_;
      break;
    case _tran:
      out<<"tran(";
      if (tm_!=nullptr) out<<tm_->primaryName();
      else out<<*st1_;
      out<<")";
      break;
    case _adj:
      out<<"adj(";
      if (tm_!=nullptr) out<<tm_->primaryName();
      else out<<*st1_;
      out<<")";
      break;
    case _conj:
      out<<"conj(";
      if (tm_!=nullptr) out<<tm_->primaryName();
      else out<<*st1_;
      out<<")";
      break;
    case _inv:
      out<<"inv(";
      if (tm_!=nullptr) out<<tm_->primaryName();
      else out<<*st1_;
      out<<")";
      break;
    default:
      break;
  }
  return out.str();
}


TermVector operator*(const SymbolicTermMatrix& S, const TermVector& X)
{return multMatrixVector(S,X);}
TermVector operator*( const TermVector& X,const SymbolicTermMatrix& S)
{return multVectorMatrix(X,S);}

TermVector multMatrixVector(const SymbolicTermMatrix& S, const TermVector& X)
{
  TermVector R;
  switch (S.op_)
  {
  case _multiply: R = multMatrixVector(*S.st1_, multMatrixVector(*S.st2_,X)); break;
  case _idop: R = *S.tm_*X; break;
  case _plus: R = multMatrixVector(*S.st1_,X)+multMatrixVector(*S.st2_,X); break;
  case _minus: R = multMatrixVector(*S.st1_,X)-multMatrixVector(*S.st2_,X); break;
  case _tran: if(S.tm_!=nullptr) R = X**S.tm_; else R = multVectorMatrix(X,*S.st1_);break;
  case _conj: if(S.tm_!=nullptr) R = conj(*S.tm_*conj(X)); else R =  conj(multMatrixVector(*S.st1_,conj(X))); break;
  case _adj: if(S.tm_!=nullptr) R = conj(conj(X)**S.tm_); else R = conj(multVectorMatrix(conj(X),*S.st1_));break;
  case _inv: R = factSolve(*const_cast<TermMatrix*>(S.tm_),X); break;
  default:
    where("multMatrixVector(SymbolicTermMatrix, TermVector)");
    error("symbolic_operation_not_handled");
  }
  if (S.coef_!=complex_t(1,0))
  {
    if (S.coef_.imag()==0) R*=S.coef_.real();
    else R*=S.coef_;
  }
  return R;
}
TermVector multVectorMatrix(const TermVector& X,const SymbolicTermMatrix& S)
{
  TermVector R;
  switch (S.op_)
  {
  case _multiply: R = multVectorMatrix(multVectorMatrix(X,*S.st1_),*S.st2_); break;
  case _idop: R = X**S.tm_; break;
  case _plus: R = multVectorMatrix(X,*S.st1_)+multVectorMatrix(X,*S.st2_); break;
  case _minus: R = multVectorMatrix(X,*S.st1_)-multVectorMatrix(X,*S.st2_); break;
  case _tran: if(S.tm_!=nullptr) R= *S.tm_*X; else R=multMatrixVector(*S.st1_,X);break;
  case _conj: if(S.tm_!=nullptr) R = conj(conj(X)**S.tm_); else R = conj(multVectorMatrix(conj(X),*S.st1_)); break;
  case _adj: if(S.tm_!=nullptr) R = conj(*S.tm_*conj(X)); else R = conj(multMatrixVector(*S.st1_,conj(X)));break;
  case _inv:
  default:
    where("multVectorMatrix(TermVector, SymbolicTermMatrix)");
    error("symbolic_operation_not_handled");
  }
  if (S.coef_!=complex_t(1,0))
  {
    if(S.coef_.imag()==0) R*=S.coef_.real();
    else R*=S.coef_;
  }
  return R;
}

}
