/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SymbolicTermMatrix.hpp
  \author E. Lunéville
  \since 21 dec 2017
  \date 21 dec 2017

  \brief Definition of the xlifepp::SymbolicTermMatrix class
*/

#ifndef SYMBOLIC_TERM_MATRIX_HPP
#define SYMBOLIC_TERM_MATRIX_HPP

#include "config.h"
#include "TermMatrix.hpp"

namespace xlifepp
{

/*!
  \class SymbolicTermMatrix
  handles expressions involving xlifepp::TermMatrix's

  xlifepp::SymbolicTermMatrix class is designed to handle any algebraic expression involving some xlifepp::TermMatrix
  The following symbolic operations are available  + - * *scalar /scalar inv conj adj trans Id
  No real operations on xlifepp::TermMatrix are performed only the product by a xlifepp::TermVector may be performed
  example:  A + trans(K)*inv(M)*K   inv(A+B) is not allowed!
  We use a binary tree to represent such symbolic operation, a node representing an operation on one or two xlifepp::SymbolicTermMatrix
  The previous example has the following tree representation

                                       (+)
                               |--------|--------|
                            (Id)A               (*)
                                        |--------|-------|
                                     (trans)K           (*)
                                                |--------|-------|
                                               (inv)M          (Id)K

  The product by a xlifepp::TermVector will produce the xlifepp::TermVector A*X + trans(K)*inv(M)*K*X
  For instance A*B is not recognized as symbolic because the product of two xlifepp::TermMatrix exists
  so writing ~A*B or A*~B or ~A*~B will enforce a symbolic representation!
*/
class SymbolicTermMatrix
{
  public:
    SymbolicTermMatrix* st1_, *st2_; //!< may be one or two SymbolicTermMatrix (pointers)
    const TermMatrix* tm_;           //!< may be one TermMatrix (pointers)
    complex_t coef_;                 //!< coefficient product
    SymbolicOperation op_;           //!< algebraic operator related to the current node
    bool delMat_;                    //!< true if TermMatrix has to be deleted

    //@{
    //! constructor
    SymbolicTermMatrix()
      : st1_(nullptr), st2_(nullptr), tm_(nullptr), coef_(0), op_(_idop), delMat_(false) {}
    SymbolicTermMatrix(const TermMatrix& A, const complex_t& c=complex_t(1.,0.))
      : st1_(nullptr), st2_(nullptr), tm_(&A), coef_(c), op_(_idop), delMat_(false) {}
    SymbolicTermMatrix(SymbolicOperation o, SymbolicTermMatrix* s1, SymbolicTermMatrix* s2=nullptr)
      : st1_(s1), st2_(s2), tm_(nullptr), coef_(complex_t(1.,0.)), op_(o), delMat_(false) {}
    SymbolicTermMatrix(LcTerm<TermMatrix>& lc);
    //@}
    //!Destructor
    ~SymbolicTermMatrix();
    void add(LcTerm<TermMatrix>& LC, std::vector<std::pair<const TermMatrix*, complex_t> >::iterator);
    SymbolicTermMatrix(const SymbolicTermMatrix&);
    SymbolicTermMatrix& operator=(const SymbolicTermMatrix&);

    string_t asString() const;
    //@{
    //! print tools
    void print(std::ostream&) const;
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator<<(std::ostream& out,const SymbolicTermMatrix& S)
    {S.print(out); return out;}
    //@}
};

inline SymbolicTermMatrix& symb(const TermMatrix& M)
{return *new SymbolicTermMatrix(M);}
inline SymbolicTermMatrix& operator~(const TermMatrix& M)
{return *new SymbolicTermMatrix(M);}

SymbolicTermMatrix& operator *(const TermMatrix&,SymbolicTermMatrix&);
SymbolicTermMatrix& operator *(SymbolicTermMatrix&, const TermMatrix&);
SymbolicTermMatrix& operator +(const TermMatrix&,SymbolicTermMatrix&);
SymbolicTermMatrix& operator +(SymbolicTermMatrix&, const TermMatrix&);
SymbolicTermMatrix& operator -(const TermMatrix&,SymbolicTermMatrix&);
SymbolicTermMatrix& operator -(SymbolicTermMatrix&, const TermMatrix&);
SymbolicTermMatrix& operator +(LcTerm<TermMatrix>&,SymbolicTermMatrix&);
SymbolicTermMatrix& operator +(SymbolicTermMatrix&, LcTerm<TermMatrix>&);
SymbolicTermMatrix& operator -(LcTerm<TermMatrix>&,SymbolicTermMatrix&);
SymbolicTermMatrix& operator -(SymbolicTermMatrix&, LcTerm<TermMatrix>&);
SymbolicTermMatrix& operator *(LcTerm<TermMatrix>&,SymbolicTermMatrix&);
SymbolicTermMatrix& operator *(SymbolicTermMatrix&, LcTerm<TermMatrix>&);
SymbolicTermMatrix& operator *(SymbolicTermMatrix&, SymbolicTermMatrix&);
SymbolicTermMatrix& operator +(SymbolicTermMatrix&, SymbolicTermMatrix&);
SymbolicTermMatrix& operator -(SymbolicTermMatrix&, SymbolicTermMatrix&);

SymbolicTermMatrix& conj(SymbolicTermMatrix&);
SymbolicTermMatrix& adj(SymbolicTermMatrix&);
SymbolicTermMatrix& tran(SymbolicTermMatrix&);
SymbolicTermMatrix& operator *(SymbolicTermMatrix&, const complex_t&);
SymbolicTermMatrix& operator *(const complex_t&, SymbolicTermMatrix&);
SymbolicTermMatrix& operator /(SymbolicTermMatrix&, const complex_t&);
SymbolicTermMatrix& inv(const TermMatrix&);
SymbolicTermMatrix& inv(SymbolicTermMatrix&);


TermVector operator*(const SymbolicTermMatrix&, const TermVector&);
TermVector multMatrixVector(const SymbolicTermMatrix&, const TermVector&);
TermVector operator*(const TermVector&, const SymbolicTermMatrix&);
TermVector multVectorMatrix(const TermVector&, const SymbolicTermMatrix&);

}
#endif // SYMBOLIC_TERM_MATRIX_HPP
