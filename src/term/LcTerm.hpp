/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LcTerm.hpp
  \authors E. Lunéville
  \since 03 apr 2012
  \date 13 jan 2014

  \brief Definition of the xlifepp::LcTerm class

  useful class to compute linear combination of terms (xlifepp::TermVector, xlifepp::SuTermVector, xlifepp::TermMatrix, xlifepp::SuTermMatrix)
  can not mix different types !
  inherits from std::vector<std::pair<const TT *, complex_t> >
  xlifepp::Term pointers in list are not deallocated when linear combination is destroyed
  xlifepp::LcTerm is a temporary class used in linear combination of TermVector(s), SuTermVector(s), ...
*/


#ifndef LC_TERM_HPP
#define LC_TERM_HPP

#include "config.h"
#include "utils.h"
#include "Term.hpp"

namespace xlifepp
{

//termtype TT= TermVector, SuTermVector, TermMatrix, SuTermMatrix, ...
template <typename TT>
class LcTerm : public std::vector<std::pair<const TT *, complex_t> >
{
public:
    string_t nametype;              //!< type name of linear combination
    typedef typename std::vector<std::pair<const TT *, complex_t> >::iterator iterator;
    typedef typename std::vector<std::pair<const TT *, complex_t> >::const_iterator const_iterator;

    //! default constructor
    LcTerm(const string_t& ty="undefined type") : nametype(ty) {}
    //operator TT() {return TT(*this);}

    //@{
    //! template constructor
    template<typename T>
    LcTerm(const TT*,const T&);
    template<typename T>
    LcTerm(const TT*,const T&, const TT*, const T&);
    template<typename T>
    LcTerm(const TT&,const T&);
    template<typename T>
    LcTerm(const TT&,const T&, const TT&, const T&);
    template<typename T>
    LcTerm(const std::vector<const TT *>&, const std::vector<T>&);
    //@}

    number_t size() const {return std::vector<std::pair<const TT *, complex_t> >::size();} 
    const_iterator begin() const {return std::vector<std::pair<const TT *, complex_t> >::begin();}
    const_iterator end() const {return std::vector<std::pair<const TT *, complex_t> >::end();}
    iterator begin(){return std::vector<std::pair<const TT *, complex_t> >::begin();}
    iterator end() {return std::vector<std::pair<const TT *, complex_t> >::end();}
    void reserve(number_t n){std::vector<std::pair<const TT *, complex_t> >::reserve(n);} 
    //!< Requests that the vector capacity be at least enough to contain n elements

    //@{
    //! insertion of Term in linear combination
    template<typename T>
    void push_back(const TT*,const T&);
    template<typename T>
    void push_back(const TT&,const T&);
    //@}

    // algebraic operation
    template<typename T>
    LcTerm<TT>& operator*=(const T&);       //!< LcTerm * v
    template<typename T>
    LcTerm<TT>& operator/=(const T&);       //!< LcTerm / v
    LcTerm<TT>& operator+=(const LcTerm<TT>&);  //!< LcTerm += LcTerm
    LcTerm<TT>& operator-=(const LcTerm<TT>&);  //!< LcTerm -= LcTerm

    // utils
    ValueType coefType() const;         //!< value type of the linear combination coefficients
    void print(std::ostream &) const;   //!< print LcTerm on ostream
    void print(PrintStream& os) const {print(os.currentStream());}
    template <typename TX>
    friend std::ostream& operator<<(std::ostream &,const LcTerm<TX>&);
};

// return valuetype of coefficients: real if all are real else complex
template<typename TT>
ValueType LcTerm<TT>::coefType() const
{
  ValueType vt =_real;
  if(size()==0) return vt;
  const_iterator it=begin();
  while(it!=end() && vt==_real)
    {
      if(it->second.imag()!=0) vt=_complex;
      it++;
    }
  return vt;
}

template<typename TT>
template<typename T>
LcTerm<TT>::LcTerm(const TT* t,const T& v)
{
   reserve(1);
   push_back(t,complex_t(v));
   nametype =words("term type",t->termType());
}

template<typename TT>
template<typename T>
LcTerm<TT>::LcTerm(const TT* t1,const T& v1, const TT* t2, const T& v2)
{
 reserve(2);
 push_back(t1,complex_t(v1));
 push_back(t2,complex_t(v2));
 nametype =words("term type",t1->termType());
 }

template<typename TT>
template<typename T>
LcTerm<TT>::LcTerm(const TT& t,const T& v)
{
   reserve(1);
   push_back(&t,complex_t(v));
   nametype =words("term type",t.termType());
}

template<typename TT>
template<typename T>
LcTerm<TT>::LcTerm(const TT& t1,const T& v1, const TT& t2, const T& v2)
{
 reserve(2);
 push_back(&t1,complex_t(v1));
 push_back(&t2,complex_t(v2));
 nametype =words("term type",t1.termType());
 }

template<typename TT>
template<typename T>
LcTerm<TT>::LcTerm(const std::vector<const TT *>& vt, const std::vector<T>& vv)
{
    number_t n=vt.size();
    reserve(n);
    if(vv.size()<n) error("not_enough_values", vv.size(),n);
    typename std::vector<const TT *>::const_iterator itt=vt.begin();
    typename std::vector<T>::const_iterator itv=vv.begin();
    for(;itt!=vt.end(); itt++, itv++)
        push_back(*itt,complex_t(*itv));
    nametype = "undefined type";
    if(n>0) nametype =words("term type",(*vt.begin())->termType());
}

template<typename TT>
template<typename T>
void LcTerm<TT>::push_back(const TT* t,const T& v)
{
    std::vector<std::pair<const TT *, complex_t> >::push_back(std::pair<const TT *, complex_t>(t,complex_t(v)));
}

template<typename TT>
template<typename T>
void LcTerm<TT>::push_back(const TT& t,const T& v)
{
    std::vector<std::pair<const TT *, complex_t> >::push_back(std::pair<const TT *, complex_t>(&t,complex_t(v)));
}

template<typename TT>
LcTerm<TT>& LcTerm<TT>::operator+=(const LcTerm<TT>& cl)  //LcTerm += LcTerm
{
    for(const_iterator it=cl.begin();it!=cl.end();it++)
        push_back(it->first,complex_t(it->second));
    return *this;
}

template<typename TT>
LcTerm<TT>& LcTerm<TT>::operator-=(const LcTerm<TT>& cl)  //LcTerm -= LcTerm
{
    for(const_iterator it=cl.begin();it!=cl.end();it++)
        push_back(it->first,complex_t(-(it->second)));
    return *this;
}

template<typename TT>
template<typename T>
LcTerm<TT>& LcTerm<TT>::operator*=(const T& v)       //LcTerm * v
{
  for(iterator it=begin(); it!=end(); it++) it->second *=v;
  return *this;
}

template<typename TT>
template<typename T>
LcTerm<TT>& LcTerm<TT>::operator/=(const T& v)       //LcTerm / v
{
  for(iterator it=begin(); it!=end(); it++) it->second /=v;
  return *this;
}

template<typename TT>
LcTerm<TT> operator+(const LcTerm<TT>& cl)
{
     return cl;
}

template<typename TT>
LcTerm<TT> operator-(const LcTerm<TT>& cl)
{
    LcTerm<TT> rcl(cl);
    rcl*=-1.;
    return rcl;
}

template<typename TT>
LcTerm<TT> operator+(const LcTerm<TT>& cl1, const LcTerm<TT>& cl2)
{
    LcTerm<TT> rcl(cl1);
    rcl+=cl2;
    return rcl;
}

template<typename TT>
LcTerm<TT> operator-(const LcTerm<TT>& cl1, const LcTerm<TT>& cl2)
{
    LcTerm<TT> rcl(cl1);
    rcl-=cl2;
    return rcl;
}

template<typename TT, typename T>
LcTerm<TT> operator*(const LcTerm<TT>& lc, const T& v)       // LcTerm * v
{
    LcTerm<TT> r(lc);
    return r*=v;
}

template<typename TT, typename T>
LcTerm<TT> operator*(const T& v, const LcTerm<TT>& lc)       // v * LcTerm
{
    LcTerm<TT> r(lc);
    return r*=v;
}

template<typename TT, typename T>
LcTerm<TT> operator/(const LcTerm<TT>& lc, const T& v)       // LcTerm / v
{
    LcTerm<TT> r(lc);
    return r/=v;
}

//! print LcTerm on ostream
template<typename TT>
void LcTerm<TT>::print(std::ostream& out) const
{
    out<<"linear combination of "<<size()<<" "<<nametype+"'s: \n";
    for(const_iterator it=begin();it!=end();it++)
    {
        const TT * t=it->first;
        complex_t c=it->second;
        out<<"  "<<words("term type",t->termType())<<" "<<t->name()<<" coef = ";
        if (c.imag()==0) out<<c.real()<<eol;
        else out<<c<<eol;
        if(theVerboseLevel > 30) out<<*t<<eol;
    }
}

//! print operator
template<typename TT>
std::ostream& operator<<(std::ostream & out,const LcTerm<TT>& lc)
{
    TT term;
    term.compute(lc);
    term.print(out);
    return out;
}

} //end of namespace xlifepp

#endif /* LC_TERM_HPP */

