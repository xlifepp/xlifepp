/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TermVector.cpp
  \authors D. Martin, E. Lunéville
  \since 03 apr 2012
  \date  26 jul 2012

  \brief Implementation of xlifepp::TermVector class members and related utilities
 */

#include "TermVector.hpp"
#include "geometry.h"
#include "utils.h"
#include "SpectralBasisInt.hpp"
#include <numeric>


namespace xlifepp
{
using std::vector;
using std::pair;

//===============================================================================
//member functions of TermVector class
//===============================================================================

// default constructor
TermVector::TermVector(const string_t& na)
{
  this->computingInfo_.noAssembly = false;
  this->termType_ = _termVector;
  this->name_ = na;
  this->entries_p = nullptr;
  this->scalar_entries_p = nullptr;
}

//! constructor with no essential boundary conditions
TermVector::TermVector(const LinearForm& lf, const string_t& na)
{
  warning("deprecated", "TermVector(LinearForm, String)", "TermVector(LinearForm, _name=?)");
  std::vector<Parameter> ps(1,_name=na);
  build(lf, 0, ps);
}

//! constructor with one essential boundary conditions
TermVector::TermVector(const LinearForm& lf, const EssentialConditions& ecs, const string_t& na)
{
  warning("deprecated", "TermVector(LinearForm, EssentialConditions, String)", "TermVector(LinearForm, EssentialConditions, _name=?)");
  std::vector<Parameter> ps(1,_name=na);
  build(lf, &ecs, ps);
}

void TermVector::build(const LinearForm& lf, const EssentialConditions* ecs, const std::vector<Parameter>& ps)
{
  if (ecs!=nullptr) error("not_yet_implemented", "TermVector::TermVector with boundary conditions");
  bool toCompute=true, toAssemble=true;
  std::set<ParameterKey> params=this->getParamsKeys(), usedParams;

  for (number_t i=0; i< ps.size(); ++i)
  {
    ParameterKey key=ps[i].key();
    buildParam(ps[i], toCompute, toAssemble);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);
  }

  this->linForm_ = lf; //copy the linearform

  this->termType_ = _termVector;
  this->entries_p = nullptr;
  this->scalar_entries_p = nullptr;
  for (it_mulc it = linForm_.begin(); it != linForm_.end(); it++)  //travel the map of single unknown linear forms
  {
    string_t nam = this->name_ + "_" + it->second.unknown()->name();
    this->suTerms_[it->first] = new SuTermVector(&(it->second), nam); //construct SuTermVector objects
  }
  if (toCompute) this->compute();
}

void TermVector::buildParamFromFct(const Parameter& p, bool& nodalFct)
{
  switch (p.key())
  {
    case _pk_name:
    {
      switch (p.type())
      {
        case _string:
          this->name_ = p.get_s();
          break;
        default:
          error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_nodal:
    {
      switch (p.type())
      {
        case _bool:
          nodalFct = p.get_b();
          break;
        default:
          error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    default:
    {
      where("Term::FromFct(...)");
      error("unexpected_param_key", words("param key", p.key()));
    }
  }
}

//copy constructor (full copy of entries)
TermVector::TermVector(const TermVector& tv, const string_t& na)
{
  this->entries_p = nullptr;
  this->scalar_entries_p = nullptr;
  this->termType_ = _termVector;
  this->copy(tv, na);
}

//copy function, current has to be cleaned before !!!
void TermVector::copy(const TermVector& tv, const string_t & na)
{
  termType_=_termVector;
  linForm_ = tv.linForm_;   //note: full copy of map<const Unknown*, SuLinearForm>
  name_ = na;
  //if (na == "") name_ = tv.name() + "-copy";
  suTerms_.clear();
  for (cit_mustv it = tv.suTerms_.begin(); it != tv.suTerms_.end(); it++)  //copy SuTermVector's
  {
    SuTermVector* sut = new SuTermVector(*it->second);
    sut->name() = name_ + "_" + it->first->name();
    suTerms_[it->first] = sut;
    sut->sulfp() = linForm_.subLfp(it->first);   //maintain links between linearform and sulinearforms
  }
  //update SuLinearForm pointers of suTerms_ to be consistent with linForm_
  for (it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)
  {
    it->second->sulfp()= linForm_.subLfp(it->first);
  }

  if (tv.entries_p != nullptr) entries_p = new VectorEntry(*tv.entries_p);
  if (tv.scalar_entries_p != nullptr)
  {
    scalar_entries_p = new VectorEntry(*tv.scalar_entries_p);
    cdofs_= tv.cdofs_;
  }
  computingInfo_=tv.computingInfo_;
}

// assign operator (full copy of entries)
TermVector& TermVector::operator=(const TermVector& tv)
{
  if (&tv == this) return *this;  //same object
  clear();
  //copy(tv, tv.name() + "-copy");
  copy(tv, tv.name());
  return *this;
}

// constructor of a TermVector from a SuTermVector   (full copy)
TermVector::TermVector(const SuTermVector& sutv, const string_t& na)
{
  termType_ = _termVector;
  name_ = na;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  suTerms_[sutv.up()] = new SuTermVector(sutv);          //full copy
  if(sutv.sulf_p!=nullptr) linForm_=LinearForm(*sutv.sulf_p);   //update linear form if possible
  computingInfo_=sutv.computingInfo_;
}

// constructor of a TermVector from a SuTermVector * (pointer copy)
TermVector::TermVector(SuTermVector* sutv, const string_t& na)
{
  termType_ = _termVector;
  name_ = na;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  suTerms_[sutv->up()] = sutv;          //pointer copy
  computingInfo_=sutv->computingInfo_;
}

// insert a SuTermVector  with full copy
void TermVector::insert(const SuTermVector& sutv)
{
  trace_p->push("TermVector::insert(SuTermVector)");
  suTerms_[sutv.up()]=new SuTermVector(sutv);  //full copy
  if(!sutv.computed()) computingInfo_.isComputed= false;
  trace_p->pop();
}

// insert a SuTermVector with pointer copy
void TermVector::insert(SuTermVector* sutv)
{
  trace_p->push("TermVector::insert(SuTermVector*)");
  suTerms_[sutv->up()]=sutv;                //pointer copy
  if(!sutv->computed()) computingInfo_.isComputed= false;
  trace_p->pop();
}

//constructor from a linear combination of TermVector's
TermVector::TermVector(const LcTerm<TermVector>& lctv)
{
  trace_p->push("TermVector::TermVector(LcTerm)");
  termType_ = _termVector;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  compute(lctv);
  trace_p->pop();
}

/*! create a single unknown TermVector from a SuTermVector :
      tv(u)[i] = sut(Mi) with Mi nodes of u-interpolation
    designed to work with different interpolation or mesh (use interpolate function of tv)
    if Mi does not belong to the domain of tv, the projection Pi on the sut-domain is computed
    and used tv(u)[i]=sut(Pi) if d(Mi,Pi)<tol*elt_size
    # do not use if sut is defined on u-interpolation, use the 'copy' constructor TermVector(SuTermVector)
       u : single unknown of the resulting TermVector
       d : domain where evaluate
       sut : SuTermVector source
       na : name of the TermVector (default="")
       fmap : optional map function (default=0)
       errorOnOutDomain : if true stop construction if a node is not located else emit a warning (default=false)
       tol : tolerance factor controlling error/warning (default=theLocateToleranceFactor)
*/
TermVector::TermVector(const Unknown& u, const GeomDomain& dom, const SuTermVector& sutv, const string_t& na,
                       bool useNearest, const Function* fmap, bool errorOnOutDom, real_t tol)
{
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
  termType_ = _termVector;
  name_ = na;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  string_t nam = name_ + "_" + u.name();
  suTerms_[&u] = new SuTermVector(u, dom, sutv, fmap, useNearest, errorOnOutDom, tol, na); //construct SuTermVector object
}

//constructor from explicit function of TermVector's: f(TermVector) or f(TermVector, TermVector)
//restricted to single unknown TermVector
TermVector::TermVector(const TermVector& tv, funSR1_t& f, const Parameter& p1)
{
  if (p1.key() != _pk_name) { error("unexpected_parameter", words("param key", p1.key())); }
  string_t na =p1.get_s();
  if (!tv.isSingleUnknown())
  {
    where("TermVector::TermVector(TermVector, funSR1_t, String)");
    error("term_not_suterm", tv.name());
  }
  suTerms_[tv.unknown()]=new SuTermVector(tv.subVector(), f, na+"_"+tv.unknown()->name());
  termType_ = _termVector;
  name_ = na;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
}

TermVector::TermVector(const TermVector& tv1, TermVector& tv2, funSR2_t& f, const Parameter& p1)
{
  if (p1.key() != _pk_name) { error("unexpected_parameter", words("param key", p1.key())); }
  string_t na =p1.get_s();
  if (!tv1.isSingleUnknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, funSR2_t, String)");
    error("term_not_suterm", tv1.name());
  }
  if (!tv2.isSingleUnknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, funSR2_t, String)");
    error("term_not_suterm", tv2.name());
  }
  if (tv1.unknown()!=tv2.unknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, funSR2_t, String)");
    error("term_mismatch_unknowns", tv1.unknown()->name(), tv2.unknown()->name());
  }
  suTerms_[tv1.unknown()]=new SuTermVector(tv1.subVector(), tv2.subVector(), f, na+"_"+tv1.unknown()->name());
  termType_ = _termVector;
  name_ = na;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
}

TermVector::TermVector(const TermVector& tv, funSC1_t& f, const Parameter& p1)
{
  if (p1.key() != _pk_name) { error("unexpected_parameter", words("param key", p1.key())); }
  string_t na =p1.get_s();
  if (!tv.isSingleUnknown())
  {
    where("TermVector::TermVector(TermVector, funSC1_t, String)");
    error("term_not_suterm", tv.name());
  }
  suTerms_[tv.unknown()]=new SuTermVector(tv.subVector(), f, na+"_"+tv.unknown()->name());
  termType_ = _termVector;
  name_ = na;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
}

TermVector::TermVector(const TermVector& tv1, TermVector& tv2, funSC2_t& f, const Parameter& p1)
{
  if (p1.key() != _pk_name) { error("unexpected_parameter", words("param key", p1.key())); }
  string_t na =p1.get_s();
  if (!tv1.isSingleUnknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, funSC2_t, String)");
    error("term_not_suterm", tv1.name());
  }
  if (!tv2.isSingleUnknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, funSC2_t, String)");
    error("term_not_suterm", tv2.name());
  }
  if (tv1.unknown()!=tv2.unknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, funSC2_t, String)");
    error("term_mismatch_unknowns", tv1.unknown()->name(), tv2.unknown()->name());
  }
  suTerms_[tv1.unknown()]=new SuTermVector(tv1.subVector(), tv2.subVector(), f, na+"_"+tv1.unknown()->name());
  termType_ = _termVector;
  name_ = na;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
}

//constructor from symbolic function of TermVector's
TermVector::TermVector(const TermVector& tv, const SymbolicFunction& sf, const Parameter& p1)
{
  if (p1.key() != _pk_name) { error("unexpected_parameter", words("param key", p1.key())); }
  string_t na =p1.get_s();
  if (!tv.isSingleUnknown())
  {
    where("TermVector::TermVector(TermVector, SymbolicFunction, String)");
    error("term_not_suterm", tv.name());
  }
  suTerms_[tv.unknown()]=new SuTermVector(tv.subVector(), sf, na+"_"+tv.unknown()->name());
  termType_ = _termVector;
  name_ = na;
  if (name_=="") name_=sf.asString(0,false,tv.name());
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
}

TermVector::TermVector(const TermVector& tv1, const TermVector& tv2, const SymbolicFunction& sf, const Parameter& p1)
{
  if (p1.key() != _pk_name) { error("unexpected_parameter", words("param key", p1.key())); }
  string_t na =p1.get_s();
  if (!tv1.isSingleUnknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, SymbolicFunction, String)");
    error("term_not_suterm", tv1.name());
  }
  if (!tv2.isSingleUnknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, SymbolicFunction, String)");
    error("term_not_suterm", tv2.name());
  }
  if (tv1.subVector().spacep()!=tv2.subVector().spacep()) //allow different unknowns on same space (permissive)
  {
    where("TermVector::TermVector(TermVector, TermVector, SymbolicFunction, String)");
    error("term_mismatch_unknowns", tv1.unknown()->name(), tv2.unknown()->name());
  }
  suTerms_[tv1.unknown()]=new SuTermVector(tv1.subVector(), tv2.subVector(), sf, na+"_"+tv1.unknown()->name());
  termType_ = _termVector;
  name_ = na;
  if (name_=="") name_=sf.asString(0,false,tv1.name(),tv2.name());
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
}

TermVector::TermVector(const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const SymbolicFunction& sf, const Parameter& p1)
{
  if (p1.key() != _pk_name) { error("unexpected_parameter", words("param key", p1.key())); }
  string_t na =p1.get_s();
  if (!tv1.isSingleUnknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, TermVector, SymbolicFunction, String)");
    error("term_not_suterm", tv1.name());
  }
  if (!tv2.isSingleUnknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, TermVector, SymbolicFunction, String)");
    error("term_not_suterm", tv2.name());
  }
  if (!tv3.isSingleUnknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, TermVector, SymbolicFunction, String)");
    error("term_not_suterm", tv3.name());
  }
  if (tv1.unknown()!=tv2.unknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, TermVector, SymbolicFunction, String)");
    error("term_mismatch_unknowns", tv1.unknown()->name(), tv2.unknown()->name());
  }
  if (tv1.unknown()!=tv3.unknown())
  {
    where("TermVector::TermVector(TermVector, TermVector, TermVector, SymbolicFunction, String)");
    error("term_mismatch_unknowns", tv1.unknown()->name(), tv3.unknown()->name());
  }
  suTerms_[tv1.unknown()]=new SuTermVector(tv1.subVector(), tv2.subVector(), tv3.subVector(), sf, na+"_"+tv1.unknown()->name());
  termType_ = _termVector;
  name_ = na;
  if (name_=="") name_=sf.asString(0, false, tv1.name(), tv2.name(), tv3.name());
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
}

/*! construct vector TermVector from 2 scalar single unknown TermVector's
    scalar TermVector's have to be defined on the same space
    vu has to be a vector unknown at least d=2 */
TermVector::TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const Parameter& p1)
{
  trace_p->push("TermVector::TermVector(Unknown,TermVector,TermVector)");
  if (p1.key() != _pk_name) { error("unexpected_parameter", words("param key", p1.key())); }
  string_t na =p1.get_s();
  if (!tv1.isSingleUnknown()) error("term_not_suterm", tv1.name());
  if (!tv2.isSingleUnknown()) error("term_not_suterm", tv2.name());
  if (u.nbOfComponents()<2) error("unknown_vector",2);
  suTerms_[&u]=new SuTermVector(u, tv1.subVector(), tv2.subVector(),na);
  termType_ = _termVector;
  name_ = na;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
  trace_p->pop();
}

/*! construct vector TermVector from 3 scalar single unknown TermVector's
    scalar TermVector's have to be defined on the same space
    vu has to be a vector unknown at least d=3 */
TermVector::TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const Parameter& p1)
{
  trace_p->push("TermVector::TermVector(Unknown,TermVector,TermVector,TermVector)");
  if (p1.key() != _pk_name) { error("unexpected_parameter", words("param key", p1.key())); }
  string_t na =p1.get_s();
  if (!tv1.isSingleUnknown()) error("term_not_suterm", tv1.name());
  if (!tv2.isSingleUnknown()) error("term_not_suterm", tv2.name());
  if (!tv3.isSingleUnknown()) error("term_not_suterm", tv3.name());
  if (u.nbOfComponents()<3) error("unknown_vector",3);
  std::list<const SuTermVector*> sutvs;
  sutvs.push_back(&tv1.subVector()); sutvs.push_back(&tv2.subVector()); sutvs.push_back(&tv3.subVector());
  suTerms_[&u]=new SuTermVector(u,sutvs,na);
  termType_ = _termVector;
  name_ = na;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
  trace_p->pop();
}

/*! construct vector TermVector from 4 scalar single unknown TermVector's
    scalar TermVector's have to be defined on the same space
    vu has to be a vector unknown at least d=4 */
TermVector::TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const TermVector& tv4, const Parameter& p1)
{
  trace_p->push("TermVector::TermVector(Unknown,TermVector,TermVector,TermVector)");
  if (p1.key() != _pk_name) { error("unexpected_parameter", words("param key", p1.key())); }
  string_t na =p1.get_s();
  if (!tv1.isSingleUnknown()) error("term_not_suterm", tv1.name());
  if (!tv2.isSingleUnknown()) error("term_not_suterm", tv2.name());
  if (!tv3.isSingleUnknown()) error("term_not_suterm", tv3.name());
  if (!tv4.isSingleUnknown()) error("term_not_suterm", tv4.name());
  if (u.nbOfComponents()<4) error("unknown_vector",4);
  std::list<const SuTermVector*> sutvs;
  sutvs.push_back(&tv1.subVector()); sutvs.push_back(&tv2.subVector());
  sutvs.push_back(&tv3.subVector()); sutvs.push_back(&tv4.subVector());
  suTerms_[&u]=new SuTermVector(u,sutvs,na);
  termType_ = _termVector;
  name_ = na;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
  trace_p->pop();
}

// assign operator from a linear combinations of TermVector's
TermVector& TermVector::operator=(const LcTerm<TermVector>& lctv)
{
  trace_p->push("TermVector::operator=(LcTerm)");
  //prevent self adding case (for instance Tv=Tv+Tv2+...), Tv has to be copied before cleared !!!
  LcTerm<TermVector> lctv2 = lctv;
  TermVector* clone = nullptr;
  for(LcTerm<TermVector>::iterator it = lctv2.begin(); it != lctv2.end(); it++)
    {
      if (it->first == const_cast<const TermVector*>(this))    //self adding
      {
        if (clone == nullptr) clone = new TermVector(*this);  //full copy !!!
        complex_t a = it->second;
        *it = std::pair<TermVector*,complex_t>(clone, a); //modify linear combination by replacing current TermMatrix by its copy
      }
    }
  //clear current TermVector and compute linear combination
  clear();
  compute(lctv2);

  //finalization
  if (clone != nullptr) delete clone;
  trace_p->pop();
  return *this;
}

//destructor
TermVector::~TermVector()
{
  VectorEntry* ov=nullptr, *ovs=nullptr;
  if (suTerms_.size()==1)  //to prevent double erasing
  {
    ov = suTerms_.begin()->second->entries_p;
    ovs= suTerms_.begin()->second->scalar_entries_p;
  }
  for (it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++) delete it->second;  //delete SuTermVector
  if (entries_p != nullptr &&  entries_p != ov) delete entries_p;
  if (scalar_entries_p != nullptr && scalar_entries_p != entries_p
      &&  scalar_entries_p != ovs && scalar_entries_p != ov)  delete scalar_entries_p;
}

// return value type (_real,_complex)
ValueType TermVector::valueType() const
{
  for(cit_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)
    if(it->second->valueType() == _complex) return _complex;
  return _real;
}

// size of Termvector counted in scalar
number_t TermVector::size() const
{
  number_t s = 0;
  for(cit_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)
    s += it->second->size();
  return s;
}

// size of Termvector counted in dofs
number_t TermVector::nbDofs() const
{
  number_t s = 0;
  for(cit_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)
    s += it->second->nbDofs();
  return s;
}

// return number of u-dofs
number_t TermVector::nbDofs(const Unknown& u) const
{
  const SuTermVector* sut=subVector_p(&u);
  if(sut==nullptr)
    {
      where("TermVector::nbDofs(Unknown)");
      error("termvector_subvector_not_found", "no unknown "+u.name());
    }
  return sut->nbDofs();
}

// return n-th dof of unknown (n>=1)
const Dof& TermVector::dof(const Unknown& u, number_t n) const
{
  const SuTermVector* sut=subVector_p(&u);
  if(sut==nullptr)
    {
      where("TermVector::dof(Unknown, Number)");
      error("termvector_subvector_not_found", "no unknown "+u.name());
    }
  return sut->dof(n);
}

bool TermVector::hasBlock(const Unknown& u) const
{
  return suTerms_.find(&u)!= suTerms_.end();
}

// ith unknown (i>=1), 0 if no SuTermVector
const Unknown* TermVector::unknown(number_t i) const
{
  if(suTerms_.size()==0) return nullptr;
  if(i>suTerms_.size()) error("index_out_of_range","i", 1, suTerms_.size());
  cit_mustv it = suTerms_.begin();
  for(number_t k=1; k<i; k++) ++it;
  return it->first;
}

// set of Unknowns involved
std::set<const Unknown*> TermVector::unknowns() const
{
  std::set<const Unknown*> sunk;
  for(cit_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)
    sunk.insert(it->first);
  return sunk;
}

// set of unknown spaces involved
std::set<const Space*> TermVector::unknownSpaces() const
{
  std::set<const Space*> sps;
  for(cit_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)
    sps.insert(it->first->space());
  return sps;
}

// set the unknown of single unknown TermVector
void TermVector::setUnknown(const Unknown& u)
{
  if(suTerms_.size()!=1)
    {
      where("TermVector::setUnknown(Unknown)");
      error("term_not_suterm", name());
    }
  SuTermVector* sut=suTerms_.begin()->second;
  sut->up()=&u;
  suTerms_.clear();
  suTerms_[&u]=sut;
}

/*!
  update the name of a TermVector and its SuTermVectors
  SuTermVector name is defined as follows: name(TermVector)_name(Unknown)
 */
void TermVector::name(const string_t& nam)
{
  name_=nam;
  for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)  //travel the map of single unknown linear forms
    {
      it->second->name(nam + "_" + it->first->name());
    }
}

// return the actual pointer to entries (priority to scalar entry), return 0 is no allocated pointer
VectorEntry* TermVector::actual_entries() const
{
  VectorEntry* ve=scalar_entries_p;
  if(ve!=nullptr) return ve;
  if(suTerms_.size()==1)
    {
      ve= firstSut()->scalar_entries();
      if(ve==nullptr) ve=firstSut()->entries();
    }
  return ve;
}

// TermVector and SutermVector's computed state = c (true or false)
void TermVector::markAsComputed(bool c)
{
  computingInfo_.isComputed=c;
  for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)  //travel the map of single unknown vectors
    {
      it->second->computed()=c;
    }
}

// TermVector, call the computation of all SuTermVector's
void TermVector::compute()
{
  if(computed()) return; //already computed
  trace_p->push("TermVector::compute");
  bool hasComponent = false;
  for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)  //travel the map of single unknown linear forms
    {
      if(!it->second->computed()) it->second->compute();
      if(!hasComponent && it->second->up()->isComponent()) hasComponent=true;
    }
  if(suTerms_.size()>1 && hasComponent) mergeBlocks(); // possibly some components has to be combined
  computed() = true;
  trace_p->pop();
}

// ----------------------------------------------------------------------------------------------------------------
/*! merge blocks referring to same vector unknown
   - when a block has a component of unknown, it is convert to its vector representation
   - when some blocks have different components of same unknown the blocks are really merged into a block with vector unknowns
*/
// ----------------------------------------------------------------------------------------------------------------
void TermVector::mergeBlocks()
{
  trace_p->push("TermVector::mergeBlocks()");
  std::map<const Unknown*, std::list<SuTermVector*> > suts; //to collect block by vector unknowns
  std::map<const Unknown*, std::list<SuTermVector*> >::iterator itm;
  bool mergeFlag=false;
  for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)  //travel the map of single unknown SuTermVector
    {
      SuTermVector* sut=it->second;
      const Unknown* up=sut->up();
      if(up->isComponent()) {up=up->parent(); mergeFlag=true;}
      itm=suts.find(up);
      if(itm!=suts.end()) itm->second.push_back(sut);
      else suts[up]=std::list<SuTermVector*>(1,sut);
    }
  if(!mergeFlag) {trace_p->pop(); return;}   //nothing to merge

  //now merge each collected block
  suTerms_.clear();
  for(itm=suts.begin(); itm!=suts.end(); itm++)
    {
      SuTermVector* sut = mergeSuTermVector(itm->second);   //merge blocks, NO DELETION
      if(sut!=nullptr)  //real merging
        {
          std::list<SuTermVector*>::iterator itl;
          for(itl=itm->second.begin(); itl!=itm->second.end(); itl++)  //delete old SuTermMatrix's
            {
              if(*itl!=sut) delete *itl;
            }
          suTerms_[sut->up()]=sut;  //assign the merged SuTermMatrix
        }
    }
  trace_p->pop();
}

/*! create vector representation of each SuTermVectors (if vector uknown) from their scalar representation (have to exist)
    if SuTermVector has a scalar unknown, entries_p = scalar_entries_p
    keepEntries: if false (default value), SutermVector scalar entries are deleted
*/
void TermVector::toVector(bool keepEntries)
{
  trace_p->push("TermVector::toVector()");
  for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++) it->second->toVector(keepEntries);
  trace_p->pop();
}

/*! create scalar representation of TermVector
    for each SuTermVector (if computed, entries_!=nullptr), if not exists create its scalar representation (set scalar_entries_p and cdofs vectors)
      if SuTermVector is already scalar, scalar_entries_p = entries_p
    NOTE: scalar_entries_p of TermVector is not update by this function (see toGlobal function)
    keepEntries: if false (default value), SutermVector non scalar entries are deleted
*/
void TermVector::toScalar(bool keepEntries)
{
  trace_p->push("TermVector::toScalar()");
  for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++) it->second->toScalar(keepEntries);
  trace_p->pop();
}


/*! create scalar global representation of TermVector
    make scalar representations of SuTermVectors's if not exist, and merge it in a scalar global representation by allocating
    scalar_entries_p and setting the cdofs vectors
    keepSuterms: if false (default value), SutermVector entries are deleted
*/
void TermVector::toGlobal(bool keepSuTerms)
{
  if(scalar_entries_p!=nullptr) return;  //already done, delete it outside if you want to rebuild
  toScalar();                      //nothing is done if SuTermMatrix's are already scalar !

  if(suTerms_.size()==1)       //nothing to merge, only a copy of scalar entries of SutermMatrix
    {
      SuTermVector* suv=suTerms_.begin()->second;
      cdofs_=suv->cdofs();
      if(keepSuTerms)   //copy entries
        {
          scalar_entries_p = new VectorEntry(*suv->scalar_entries_p);
        }
      else    //copy entries pointer and clear SuTermvector
        {
          scalar_entries_p = suv->scalar_entries_p;
          suv->scalar_entries_p=nullptr;
          if(suv->entries_p != scalar_entries_p && suv->entries_p!=nullptr) delete suv->entries_p;
          suv->entries_p=nullptr;
          suv->cdofs().clear();
        }
      return;
    }

  trace_p->push("TermVector::toGlobal()");

  //merge all SutermVector's
  ValueType vt=_real;
  number_t n=0;
  std::map<number_t,const Unknown*> order;  //to order unknowns according to their rank
  std::map<number_t,const Unknown*>::iterator ito;
  for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)
    {
      VectorEntry* ve = it->second->scalar_entries_p;
      if(vt==_real) vt = ve->valueType_;
      n+=ve->size();
      const Unknown* u=it->first;
      number_t r=u->rank();
      if(order.find(r)==order.end()) order.insert(std::make_pair(r,u));
    }
  scalar_entries_p= new VectorEntry(vt, _scalar, n);
  cdofs_.resize(n);
  vector<DofComponent>::iterator itd, itdn = cdofs_.begin();
  if(vt==_real)   //full real computation
    {
      Vector<real_t>::iterator itn=scalar_entries_p->beginr();
      for(ito = order.begin(); ito != order.end(); ito++)
        {
          VectorEntry* ve = suTerms_[ito->second]->scalar_entries_p;
          itd=suTerms_[ito->second]->cdofs_.begin();
          Vector<real_t>::iterator itv=ve->beginr();
          for(number_t n=0; n < ve->size(); n++, itv++, itn++, itd++, itdn++)
            {
              *itn = *itv;
              *itdn = *itd;
            }
        }
    }
  else
    {
      Vector<complex_t>::iterator itn=scalar_entries_p->beginc();
      for(ito = order.begin(); ito != order.end(); ito++)
        {
          VectorEntry* ve = suTerms_[ito->second]->scalar_entries_p;
          itd=suTerms_[ito->second]->cdofs_.begin();
          if(ve->valueType_==_real)
            {
              Vector<real_t>::iterator itv = ve->beginr();
              for(number_t n=0; n<ve->size(); n++, itv++, itn++, itd++, itdn++)
                {
                  *itn = complex_t(*itv);
                  *itdn = *itd;
                }
            }
          else
            {
              Vector<complex_t>::iterator itv = ve->beginc();
              for(number_t n=0; n<ve->size(); n++, itv++, itn++, itd++, itdn++)
                {
                  *itn = *itv;
                  *itdn = *itd;
                }
            }
        }
    }
  //Clear suTermVectors
  if(!keepSuTerms)
    for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++) it->second->clear();

  trace_p->pop();
}

/*! create local representation of TermVector from global scalar representation
    from global scalar representation reallocate the SuTermVector's in their standard representation (vector if it is)
    keepGlobal: if false (default value), scalar_entries is deallocated
*/
void TermVector::toLocal(bool keepGlobal)
{
  if(scalar_entries_p==nullptr) return;  //nothing to do
  trace_p->push("TermVector::toLocal()");

  //dofcompnents from cdofs_, collected by unknown
  std::map<const Unknown*,std::map<DofComponent, number_t> > mudn;
  vector<DofComponent>::iterator itd=cdofs_.begin();
  std::map<const Unknown*,std::map<DofComponent, number_t> >::iterator itm;
  number_t k=1;
  for(; itd!=cdofs_.end(); itd++, k++)
    {
      const Unknown* u = itd->u_p;
      itm = mudn.find(u);
      if(itm==mudn.end())
        {
          std::map<DofComponent, number_t> mdn;
          mdn[*itd]=k;
          mudn[u]=mdn;
        }
      else itm->second.insert(std::make_pair(*itd,k));
    }
  //re allocate SuTermVector entries
  ValueType vt=scalar_entries_p->valueType_;
  for(itm=mudn.begin(); itm!=mudn.end(); itm++)
    {
      const Unknown* u=itm->first;
      dimen_t nbc=u->nbOfComponents();
      std::map<DofComponent, number_t>& mdn = mudn[u];
      SuTermVector* sut = subVector_p(u);
      // SuTermVector result
      if(sut==nullptr)  //SuTermVector does not exist, create a new one
        {
          //NOTE: real space is hard to reconstruct, we take the whole space linked to unknown u
          //       it may occurs some trouble
          sut= new SuTermVector(name_+"_"+u->name(),u, u->space(), vt);
          suTerms_[u]=sut;
        }
      if(sut->entries()!=nullptr) delete sut->entries();
      if(sut->scalar_entries()!=nullptr && sut->scalar_entries()!=sut->entries()) delete sut->scalar_entries();
      sut->scalar_entries()=nullptr;
      sut->cdofs().clear();
      // buid numdofs map: global dof numbering -> rank in result sub vector
      std::map<number_t,number_t> numdofs;
      std::map<DofComponent, number_t>::iterator itdn;
      std::map<number_t,number_t>::iterator itn;
      if(sut->spacep()==u->space())
        {
          number_t k=1;
          for(itdn=mdn.begin(); itdn!=mdn.end(); itdn++)
            {
              itn=numdofs.find(itdn->first.dofnum);
              if(itn==numdofs.end())
                {
                  numdofs[itdn->first.dofnum]=k;
                  k++;
                }
            }
        }
      else //case of subspace
        {
          sut->spacep()->builddofid2rank();
          const std::map<number_t, number_t>& dofid2rank = sut->spacep()->dofid2rank();
          for(itdn=mdn.begin(); itdn!=mdn.end(); itdn++)
            numdofs[itdn->first.dofnum]=dofid2rank.at(itdn->first.dofnum);
        }
      sut->entries() = new VectorEntry(vt, nbc, numdofs.size());
      if(nbc==1)  //scalar case
        {
          for(itdn=mdn.begin(); itdn!=mdn.end(); itdn++)
            {
              if(vt==_real)  //real value
                {
                  real_t r;
                  scalar_entries_p->getEntry(itdn->second,r);
                  sut->entries()->setEntry(numdofs[itdn->first.dofnum],r);
                }
              else //complex value
                {
                  complex_t c;
                  scalar_entries_p->getEntry(itdn->second,c);
                  sut->entries()->setEntry(numdofs[itdn->first.dofnum],c);
                }
            }
        }
      else //vector case
        {
          for(itdn=mdn.begin(); itdn!=mdn.end(); itdn++)
            {
              if(vt==_real)   //real value
                {
                  Vector<real_t> rv;
                  real_t r;
                  scalar_entries_p->getEntry(itdn->second,r);
                  number_t n=numdofs[itdn->first.dofnum];
                  sut->entries()->getEntry(n,rv);
                  rv(itdn->first.numc)=r;
                  sut->entries()->setEntry(numdofs[itdn->first.dofnum],rv);
                }
              else //complex value
                {
                  Vector<complex_t> cv;
                  complex_t c;
                  scalar_entries_p->getEntry(itdn->second,c);
                  number_t n=numdofs[itdn->first.dofnum];
                  sut->entries()->getEntry(n,cv);
                  cv(itdn->first.numc)=c;
                  sut->entries()->setEntry(numdofs[itdn->first.dofnum],cv);
                }
            }
        }
      sut->computed()=true;
    }
  if(!keepGlobal)
    {
      delete scalar_entries_p;
      scalar_entries_p=nullptr;
      cdofs_.clear();
    }

  trace_p->pop();
}

// reinterpret TermVector as a real Vector
Vector<real_t> TermVector::asRealVector() const
{
  Vector<real_t> v;
  asVector(v);
  return v;
}

// reinterpret TermVector as a complex Vector
Vector<complex_t> TermVector::asComplexVector() const
{
  Vector<complex_t> v;
  asVector(v);
  return v;
}

real_t TermVector::norm2() const
{
  return std::sqrt(real(hermitianProduct(*this, *this)));
}


//deallocate memory used by a TermVector
void TermVector::clear()
{
  trace_p->push("TermVector::clear");
  for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++) it->second->clear();  //travel the map of single unknown linear forms
  linForm_.clear();
  suTerms_.clear();
  if(entries_p != nullptr) delete entries_p;
  if(scalar_entries_p != entries_p && scalar_entries_p!=nullptr)
    {
      delete scalar_entries_p;
      cdofs_.clear();
    }
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  computed() = false;
  trace_p->pop();
}

/*! compute a LcTerm (linear combination of TermVectors) to a TermVector
 all the termvector's have to be computed before
 and the current termvector is 'void'
 Note: when combination involved terms with component unknowns, it they are few to refer the same vector unknown
        they will be merged in the same term with vector unknown:  suT_u1 + suT_u3 -> suT_u   with [suT]_2=0
        if only one component unknwow in term, it stay the same, say suT_2 -> suT_2
*/
void TermVector::compute(const LcTerm<TermVector>& lctv)
{
  trace_p->push("TermVector::compute(LcTerm)");
  if (lctv.size() == 0)  error("is_void", "LcTerm");        //abnormal !!!

  if (lctv.size() == 1)
  {
    const TermVector* tv = dynamic_cast<const TermVector*>(lctv.begin()->first);
    if (tv == nullptr) error("null_pointer", "TermVector");
    complex_t a = lctv.begin()->second;
    for (cit_mustv it = tv->suTerms_.begin(); it != tv->suTerms_.end(); it++)
    {
      SuTermVector* sut = new SuTermVector(*it->second);   //copy SuTermVector
      sut->name() = tostring(a) + " * " + it->second->name();
      if (a != complex_t(1., 0))   //product if a!=1
      {
        if (a.imag() == 0) *sut *= a.real();
        else *sut *= a;
      }
      suTerms_[it->first] = sut;
    }
    linForm_ = tv->linearForm();
    termType_ = tv->termType_;
    computingInfo_ = tv->computingInfo_;
    computed() = true;
    trace_p->pop();
    return;
  }

  //case of a linear combination
  //create linear combinations of SuTermVector collected by unknown
  std::map<const Unknown*, LcTerm<SuTermVector> > lcsutv;
  std::map<const Unknown*, LcTerm<SuTermVector> >::iterator itm;
  for (LcTerm<TermVector>::const_iterator itt = lctv.begin(); itt != lctv.end(); itt++)
  {
    const TermVector* tv = dynamic_cast<const TermVector*>(itt->first);
    if (tv == nullptr) error("null_pointer", "TermVector");
    complex_t a = itt->second;
    for (cit_mustv it = tv->suTerms_.begin(); it != tv->suTerms_.end(); it++)
    {
      const Unknown* up = it->first;
      if (up->isComponent()) up=up->parent();    //component case
      SuTermVector* sut = it->second;
      itm = lcsutv.find(up);
      if (itm == lcsutv.end()) itm = lcsutv.find(up->dual_p());
      if (itm == lcsutv.end()) lcsutv.insert(std::pair<const Unknown*, LcTerm<SuTermVector> >(up, LcTerm<SuTermVector>(sut, a)));
      else  itm->second.push_back(sut, a);
    }
  }

  //extend component unknown term to vector unknown term if it has more than one
  for (itm = lcsutv.begin(); itm != lcsutv.end(); itm++)
  {
    std::map<number_t, const SuTermVector*> newsuts;  //the key number refers to LcTerm rank
    std::map<number_t, const SuTermVector*>::iterator its;
    if (itm->first->nbOfComponents()>1)    //may be there are component unknown terms
    {
      LcTerm<SuTermVector>& lct=itm->second;
      number_t k=0;
      for (LcTerm<SuTermVector>::iterator itlc=lct.begin(); itlc!=lct.end(); itlc++, k++)   //locate component unknown terms
      {
        const SuTermVector* sut = static_cast<const SuTermVector*>(itlc->first);
        if (sut->up()->isComponent()) newsuts[k]=sut;
      }
      if (newsuts.size()>0 && lct.size()>1)   //extend to vector suterm in newsuts
      {
        its=newsuts.begin();
        for (; its!=newsuts.end(); its++)
        {
          SuTermVector* nsut= new SuTermVector(its->second->toVectorUnknown());  //copy component unknown term
          complex_t a=lct[its->first].second;
          lct[its->first]=std::pair<SuTermVector*,complex_t>(nsut,a);     //modify current Lcterm
          its->second=nsut;
        }
      }
      else newsuts.clear();   //erase newsuts (only one term!)
    } //end of component unknown term analysis

    SuTermVector* sut = new SuTermVector(itm->second);   //create SuTermVector
    suTerms_[itm->first] = sut;                          //compute linear combination of SuTermVector's
    for(its=newsuts.begin(); its!=newsuts.end(); its++) delete its->second;  //clean newsuts
  }

  //update TermVector info
  linForm_.clear();  //after a Term combination, linear form is no longer managed
  computed() = true;
  trace_p->pop();
  return;
}

/*! adjust the global scalar entries to the cdofs numbering newcdofs
    when TermVector cdofs  is included in newcdofs, new zeros are introduced (extension)
    when newcdofs is included in cdofs Termvector, coefficients are removed (restriction)
    else it is in a same time a restriction (removed cdofs)  and an extension (omitted cdof)
    in any case, the coefficient may be permuted and cdofs TermVector = newcdofs at the end
    Note: unknowns and SuTermVectors are NOT updated
*/
void TermVector::adjustScalarEntries(const vector<DofComponent>& newcdofs)
{
  trace_p->push("TermVector::adjustScalarEntries()");
  std::map<DofComponent, number_t> mcdofs;
  vector<DofComponent>::iterator itd=cdofs_.begin();
  vector<DofComponent>::const_iterator itn=newcdofs.begin();
  number_t k=0;
  bool sameNum = true;
  for(; itd!=cdofs_.end(); itd++, k++)
    {
      mcdofs[*itd]=k;
      if(sameNum && itn!=newcdofs.end())
        {
          if(*itd != *itn  && *itd!=itn->dual()) sameNum=false;   //allows dual unknown (permissive)
          itn++;
        }
    }
  if(sameNum && cdofs_.size()==newcdofs.size())
    {
      trace_p->pop();
      return;
    }

  //create new vector
  ValueType vt=_real;
  if(scalar_entries_p!=nullptr) vt=scalar_entries_p->valueType_;
  VectorEntry* ve= new VectorEntry(vt,_scalar, newcdofs.size());
  std::map<DofComponent, number_t>::iterator itmd;
  if(vt==_real)  //real case
    {
      Vector<real_t>::iterator itve=ve->beginr(), itv=scalar_entries_p->beginr();
      for(itn=newcdofs.begin(); itn!=newcdofs.end(); itn++, itve++)
        {
          itmd=mcdofs.find(*itn);
          if(itmd==mcdofs.end()) itmd=mcdofs.find(itn->dual());
          if(itmd!=mcdofs.end()) *itve = *(itv + itmd->second);
        }
    }
  else  //complex case
    {
      Vector<complex_t>::iterator itve=ve->beginc(), itv=scalar_entries_p->beginc();
      for(itn=newcdofs.begin(); itn!=newcdofs.end(); itn++, itve++)
        {
          itmd=mcdofs.find(*itn);
          if(itmd==mcdofs.end()) itmd=mcdofs.find(itn->dual());   //find with dual (permissive)
          if(itmd!=mcdofs.end()) *itve = *(itv + itmd->second);
        }
    }

  //update current vector
  if(scalar_entries_p!=nullptr) delete scalar_entries_p;
  scalar_entries_p = ve;
  cdofs_=newcdofs;
  trace_p->pop();
}

TermVector add(const TermVector& tv1, const TermVector& tv2)
{
  TermVector tU = tv1;
  tU += tv2;
  return tU;
}
/*!algebraic operations on TermVector
   for += and -= operations work as follows
     assumed that u=[t_u1 t_u2]  v=[s_u1 s_u3] where u_i is the unknown related to a block of vector
     then  u+=v => u=[(t_u1+s_u1) t_u2 s_u3]
  it means that += add blocks having the same unknown but also performs an union of blocks
  Note: these operations does not update scalar representation !!!
*/
TermVector& TermVector::operator+=(const TermVector& v)  // operation U+=V
{
  trace_p->push("TermVector::operator+=");
  for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)  //travel the map of single unknown vectors
    {
      const Unknown* u = it->first;
      SuTermVector* su = it->second;
      const SuTermVector* sv = v.subVector_p(u);
      if(sv == nullptr && u->dual_p() != nullptr)  sv = v.subVector_p(u->dual_p());  //
      if(sv != nullptr) *su += *sv;
    }
  // travel the suTerms of v to add to current TermVector a copy of those that does not exist in current term
  for(cit_mustv it = v.begin(); it != v.end(); it++)
    {
      const Unknown* u = it->first;
      SuTermVector* su = it->second;
      const SuTermVector* sv = subVector_p(u);
      // if (sv == nullptr) sv = subVector_p(u->dual_p());
      if(sv == nullptr && u->dual_p() != nullptr) sv = subVector_p(u->dual_p());
      if(sv == nullptr) insert(u, new SuTermVector(*su));  //insert a copy of su in current term
    }
  trace_p->pop();
  return *this;
}

TermVector& TermVector::operator-=(const TermVector& v)  // operation U+=V
{
  trace_p->push("TermVector::operator-=");
  for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)   //travel the map of single unknown vectors
    {
      const Unknown* u = it->first;
      SuTermVector* su = it->second;
      const SuTermVector* sv = v.subVector_p(u);
      if(sv == nullptr) sv = v.subVector_p(u->dual_p());
      if(sv != nullptr) *su -= *sv;
    }
  // travel the suTerms of v to add to current TermVector a copy of those that does not exist in current term
  for(cit_mustv it = v.begin(); it != v.end(); it++)
    {
      const Unknown* u = it->first;
      SuTermVector* su = it->second;
      const SuTermVector* sv = subVector_p(u);
      if(sv == nullptr) sv = subVector_p(u->dual_p());
      if(sv == nullptr)
        {
          SuTermVector* su_copy = new SuTermVector(*su);
          *su_copy *= -1.;
          insert(u, su_copy); //insert a copy of su in current term
        }
    }
  trace_p->pop();
  return *this;
}

/*! merge a TermVector with preserving the current value when common dofs
    merge only SuTermVectors not global representation !!!
*/
TermVector& TermVector::merge(const TermVector& v)
{
  trace_p->push("TermVector::merge(const TermVector)");
  // travel the suTerms of v to add to current TermVector a copy of those that does not exist in current term
  for(cit_mustv it = v.begin(); it != v.end(); it++)
    {
      const Unknown* u = it->first;
      SuTermVector* su = it->second;
      SuTermVector* sv = subVector_p(u);
      if(sv == nullptr) sv = subVector_p(u->dual_p());
      if(sv == nullptr)
        {
          SuTermVector* su_copy = new SuTermVector(*su);
          insert(u, su_copy); //insert a copy of su in current term
        }
      else  sv->merge(*su);
    }
  trace_p->pop();
  return *this;
}

/*! produce the mapped TermVector from current GeomDomains to other GeomDomain
    the maps from dom to domains of current TermVector must be already defined, if not id is assumed

       dom  : arriving domain
       u    : arriving unknown
       errorOnOutDom : if true the program stop on locating error, else the interpolated value is set to 0
       fmap : optional mapping function from starting domain to arriving domain
       tol  : tolerance factor used by locating error : d(P,dom) > tol * elt.size

    it produces a new TermVector, act on SuTermVector's, do not work for global representation !
*/
TermVector TermVector::mapTo(const GeomDomain& dom, const Unknown& u, bool useNearest,
                             bool errorOnOutDom, Function* fmap, real_t tol) const
{
  trace_p->push("TermVector::mapTo");
  if(suTerms_.size() == 0)
    error("is_void","TermVector");

  TermVector tv(name_+"->"+dom.name());
  for(cit_mustv it = begin(); it != end(); it++)
    {
      SuTermVector* sut=it->second->mapTo(dom,u,useNearest,errorOnOutDom,fmap,tol);
      tv.insert(&u,sut);
    }
  tv.computed()=true;
  trace_p->pop();
  return tv;
}

// same but extern
TermVector mapTo(const TermVector& v, const GeomDomain& dom, const Unknown& u, bool useNearest,
                 bool errorOnOutDom, Function* fmap, real_t tol)
{
  trace_p->push("mapTo");
  if(v.isVoid())
    error("is_void","TermVector");

  TermVector tv(v.name()+"->"+dom.name());
  for(cit_mustv it = v.begin(); it != v.end(); it++)
    {
      SuTermVector* sut=it->second->mapTo(dom,u,useNearest,errorOnOutDom,fmap,tol);
      tv.insert(&u,sut);
    }
  tv.computed()=true;
  trace_p->pop();
  return tv;
}


//! locate element of the single unknown TermVector Space containing P
Element& TermVector::locateElement(const Point& P, bool useNearest, bool errorOnOutDom) const
{
  if(!isSingleUnknown()) error("term_not_suterm");
  const SuTermVector* sut=suTerms_.begin()->second;
  if(sut==nullptr)  error("termvector_subvector_not_found", "first one");
  return sut->locateElement(P, useNearest, errorOnOutDom);
}

//! locate element of the unknown SuTermVector Space containing P
Element& TermVector::locateElement(const Unknown& u, const Point& P, bool useNearest, bool errorOnOutDom) const
{
  const SuTermVector* sut=subVector_p(&u);
  if(sut==nullptr)  error("termvector_subvector_not_found", u.name());
  return sut->locateElement(P, useNearest, errorOnOutDom);
}

//! locate element of the single unknown TermVector Space containing P
Element* TermVector::locateElementP(const Point& P, bool useNearest, bool errorOnOutDom) const
{
  if(!isSingleUnknown()) error("term_not_suterm");
  const SuTermVector* sut=suTerms_.begin()->second;
  if(sut==nullptr)  error("termvector_subvector_not_found", "first one");
  return sut->locateElementP(P, useNearest, errorOnOutDom);
}

//! locate element of the unknown SuTermVector Space containing P
Element* TermVector::locateElementP(const Unknown& u, const Point& P, bool useNearest, bool errorOnOutDom) const
{
  const SuTermVector* sut=subVector_p(&u);
  if(sut==nullptr)  error("termvector_subvector_not_found", u.name());
  return sut->locateElementP(P, useNearest, errorOnOutDom);
}

//! evaluate unknown at a point (use interpolation method) and return a value that may be a real/complex scalar or a real/complex vector
Value TermVector::evaluate(const Unknown& u, const Point& P, bool useNearest, bool errorOnOutDom) const
{
  const SuTermVector* sut=subVector_p(&u);
  if(sut==nullptr)  error("termvector_subvector_not_found", u.name());
  return sut->evaluate(P);
}

//! evaluate unknown at a point (use interpolation method) and return a value that may be a real/complex scalar or a real/complex vector
Value TermVector::evaluate(const Point& P, bool useNearest, bool errorOnOutDom) const
{
  if(!isSingleUnknown()) error("term_not_suterm");
  const SuTermVector* sut=suTerms_.begin()->second;
  if(sut==nullptr)  error("termvector_subvector_not_found", "first one");
  return sut->evaluate(P, useNearest,errorOnOutDom);
}

//! evaluate unknown at a point (use interpolation method) and return a value that may be a real/complex scalar or a real/complex vector
Value TermVector::evaluate(const Unknown& u, const Point& P, const Element& elt) const
{
  const SuTermVector* sut=subVector_p(&u);
  if(sut==nullptr)  error("termvector_subvector_not_found", u.name());
  return sut->evaluate(P,elt);
}

//! evaluate unknown at a point (use interpolation method) and return a value that may be a real/complex scalar or a real/complex vector
Value TermVector::evaluate(const Point& P,const Element& elt) const
{
  if(!isSingleUnknown()) error("term_not_suterm");
  const SuTermVector* sut=suTerms_.begin()->second;
  if(sut==nullptr)  error("termvector_subvector_not_found", "first one");
  return sut->evaluate(P,elt);
}


//print TermVector
void TermVector::print(std::ostream& out) const
{
  if(theVerboseLevel == 0) return;  //no print
  out << "TermVector " << name() << ": ";
  if(suTerms_.size() == 0)
    {
      out << " void ";
      return;
    }
  cit_mustv it = suTerms_.begin();
  if(suTerms_.size() == 1)
    {
      out << words("unknown") << " \"" << it->first->name()<<"\" ("
          << words("value", it->second->valueType()) << " " << words("structure", it->second->strucType())<<")\n";
    }
  else
    {
      out << words("unknowns") << ": ";
      for(it = suTerms_.begin(); it != suTerms_.end(); it++) out << "\"" << it->first->name() << "\" ";
      out << eol;
    }
  if(theVerboseLevel < 2) return;

  //print SuTermVectors
  number_t vb = theVerboseLevel;
  if(theVerboseLevel < 5) verboseLevel(2);
  bool header = (suTerms_.size() > 1);
  for(it = suTerms_.begin(); it != suTerms_.end(); it++)  it->second->print(out,false, header);

  //print global entries
  if(entries_p != nullptr)  out << " global entries: "<<*entries_p<<eol;
  if(scalar_entries_p != nullptr)
    {
      out << " global scalar entries: "<<tostring(scalar_entries_p->size())<<" values"<<eol;
      out << words("firste") << " " << words("row") << " cdofs: ";
      number_t nbd=cdofs_.size();
      number_t m = std::min(number_t(theVerboseLevel),nbd);
      vector<DofComponent>::const_iterator itd=cdofs_.begin();
      for(number_t i = 0; i < m; i++, itd++)  out  << *itd << "  ";
      if(m < nbd) out<<"...";
      out<<eol;
      if(scalar_entries_p==entries_p) out<<" same values as global entries"<<eol;
      else  out << (*scalar_entries_p)<<eol;
    }

  verboseLevel(vb);
}

// access to first unknown term vector (const), useful when single unknown term
const SuTermVector& TermVector::subVector() const
{
  if(suTerms_.size()==0) error("is_void", "TermVector");
  return *(suTerms_.begin()->second);
}

SuTermVector& TermVector::subVector()
{
  if(suTerms_.size()==0) error("is_void", "TermVector");
  return *(suTerms_.begin()->second);
}

//access to single unknown term vector (internal use)
SuTermVector& TermVector::subVector(const Unknown* u)
{
  if (u == nullptr) error("null_pointer", "u");
  it_mustv it = suTerms_.find(u);
  if (it == suTerms_.end()) error("termvector_subvector_not_found", u->name());
  return *(it->second);
}

SuTermVector& TermVector::subVector(const Unknown& u)
{
  return subVector(&u);
}

const SuTermVector& TermVector::subVector(const Unknown* u) const
{
  if (u == nullptr) error("null_pointer", "u");
  cit_mustv it = suTerms_.find(u);
  if (it == suTerms_.end()) error("termvector_subvector_not_found", u->name());
  return *(it->second);
}

const SuTermVector& TermVector::subVector(const Unknown& u) const
{
  return subVector(&u);
}

//access to single unknown term vector pointer (internal use)
SuTermVector* TermVector::subVector_p(const Unknown* u)
{
  if (u == nullptr) error("null_pointer", "u");
  it_mustv it = suTerms_.find(u);
  if (it == suTerms_.end()) return nullptr;
  else return (it->second);
}

const SuTermVector* TermVector::subVector_p(const Unknown* u) const
{
  if (u == nullptr) error("null_pointer", "u");
  cit_mustv it = suTerms_.find(u);
  if (it == suTerms_.end()) return nullptr;
  else return (it->second);
}

/*! restrict TermVector to a GeomDomain

*/
TermVector TermVector::onDomain(const GeomDomain& dom) const
{
  trace_p->push("TermVector::onDomain");
  if(suTerms_.size() == 0)
    error("is_void","TermVector");

  TermVector tv(name_+"|"+dom.name());
  for(cit_mustv it = begin(); it != end(); it++)
    {
      SuTermVector* sut=it->second->onDomain(dom);
      if(sut!=nullptr) tv.insert(it->first,sut);
    }
  tv.computed()=true;
  trace_p->pop();
  return tv;
}

//symbolic syntax  T|dom
TermVector operator|(const TermVector& tv, const GeomDomain& dom)
{
  return tv.onDomain(dom);
}

/*!return single unknown term vector as TermVector (copy constructor like),
   useful to extract single unknown term from multiple unknowns term
   induce full copy
*/
TermVector TermVector::operator()(const Unknown& u) const
{
  string_t na = name_ + "_" + u.name();
  if(u.isComponent()) return TermVector(subVector(u.parent())(*u.asComponent()),na);
  return TermVector(this->subVector(&u),na);
}

//!return value ot the n-th component of u-vector
Value TermVector::getValue(const Unknown& u, number_t n) const
{
  const SuTermVector* sut=subVector_p(&u);
  if(sut==nullptr)  error("null_pointer", u.name());
  return sut->getValue(n);
}

// set value to vector component of unknown term (n>=1)
void TermVector::setValue(const Unknown& u, number_t n, const Value& val)
{
  SuTermVector* sut=subVector_p(&u);
  if(sut==nullptr)  error("null_pointer", u.name());
  sut->setValue(n,val);
}

void TermVector::setValue(const Unknown& u, number_t n, const real_t& r)
{
  setValue(u,n,Value(r));
}
void TermVector::setValue(const Unknown& u, number_t n, const complex_t& c)
{
  setValue(u,n,Value(c));
}
void TermVector::setValue(const Unknown& u, number_t n, const vector<real_t>& vr)
{
  setValue(u,n,Value(Vector<real_t>(vr)));
}
void TermVector::setValue(const Unknown& u, number_t n, const vector<complex_t>& vc)
{
  setValue(u,n,Value(Vector<complex_t>(vc)));
}

number_t TermVector::dofRank(const Unknown& u, const Dof& dof) const
{
  return subVector(u).dofRank(dof);
}

number_t TermVector::dofRank(const Dof& dof) const
{
  return subVector().dofRank(dof);
}

/*! move SuTermVector unknown to an other
    consist in changing the unknown u of SutermVector by the unknown v (u and v must be defined on the same Space!)
    with an optional management of components of unknown, examples
      u a scalar unknown and v a scalar unknown:  SuTermVector if not modified
      u a scalar unknown and v a 3 vector unknown, ci ={2} the SutermVector is changed to a 3 column vector:
             new sutv=[ [0 ...0] [sutv] [0...0] ]
      u a 3-vector unknown and v a 3-vector unknown, ci ={2 3 1} the SutermVector is changed columns are permuted:
             new sutv=[ [sutv2] [sutv3] [sutv1] ]   v may be equal to u in that case
      u a 4-vector unknown and v a 2-vector unknown, ci ={2 4} the SutermVector is changed columns are permuted:
             new sutv=[ [sutv2] [sutv4] ]
    u is replaced by v,
    v may be equal to u but be cautious if the result is not of the dimension of v because many operations are not supported
    in doubt, create a new unknown of the consistent dimension

*/
void TermVector::changeUnknown(const Unknown& u, const Unknown& v, const Vector<number_t>& ci)
{
  it_mustv itu = suTerms_.find(&u);
  if (itu == suTerms_.end())
  {
    where("TermVector::changeUnknown(const Unknown&, const Unknown&, const Numbers&)");
    error("unknown_not_found",u.name());
  }
  it_mustv itv = suTerms_.find(&v);
  if (itv != suTerms_.end() && &u != &v)
  {
    where("TermVector::changeUnknown(const Unknown&, const Unknown&, const Numbers&)");
    error("term_mismatch_unknowns",u.name(),v.name());
  }
  SuTermVector* sut=itu->second;
  sut->changeUnknown(v,ci);
  if (&u != &v)
  {
    suTerms_.erase(itu);
    suTerms_[&v]=sut;
  }
}

void TermVector::changeUnknown(const Unknown& v, const Vector<number_t>& ci)
{
  if(!isSingleUnknown())
    {
      where("TermVector::changeUnknown(const Unknown&, const Numbers&)");
      error("term_not_suterm");
    }
  changeUnknown(*unknown(),v,ci);
}


// insert in SuTerms map a SuTermVector
void TermVector::insert(const Unknown* up, SuTermVector* sutvp)
{
  it_mustv it = suTerms_.find(up);
  if(it != suTerms_.end())   //a SuTermMatrix already exists, delete it
    {
      delete it->second;
      suTerms_.erase(it);
    }
  suTerms_[up] = sutvp;  //insert new term
}

/*! save TermVector to file in a raw format (values as columns )
    if encodeName is true, the name of file includes some additionnal matrix informations
        newname = name(m_ValueType_StructType_p).ext
    m size of vector, p size of subvectors if StrucType=_vector
*/

void TermVector::saveToFile(const string_t& fn, number_t prec, bool encode) const
{
  //if (!computed() || suTerms_.size() == nullptr)
  if(suTerms_.size() == 0)
    {
      warning("free_warning", "vector is void, no saving");
      return;
    }
  cit_mustv it = suTerms_.begin();
  if(suTerms_.size() == 1)
    {
      it->second->saveToFile(fn, prec, encode);
      return;
    }

  pair<string_t, string_t> rootext = fileRootExtension(fn, Environment::authorizedSaveToFileExtensions());
  for(cit_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)
    {
      string_t fnb = rootext.first += "_" + it->first->name() + "." + rootext.second;
      it->second->saveToFile(fnb, prec, encode);
    }
}

//algebraic operation on TermVector
const TermVector& operator+(const TermVector& tv)
{
  return tv;
}
LcTerm<TermVector> operator-(const TermVector& tv)
{
  return LcTerm<TermVector>(&tv, -1.);
}
LcTerm<TermVector> operator+(const TermVector& tv1, const TermVector& tv2)
{
  return LcTerm<TermVector>(&tv1, 1., &tv2, 1.);
}
LcTerm<TermVector> operator-(const TermVector& tv1, const TermVector& tv2)
{
  return LcTerm<TermVector>(&tv1, 1., &tv2, -1.);
}
LcTerm<TermVector> operator+(const LcTerm<TermVector>& lctv, const TermVector& tv)
{
  LcTerm<TermVector> rlctv(lctv);
  rlctv.push_back(&tv, 1.);
  return rlctv;
}
LcTerm<TermVector> operator-(const LcTerm<TermVector>& lctv, const TermVector& tv)
{
  LcTerm<TermVector> rlctv(lctv);
  rlctv.push_back(&tv, -1.);
  return rlctv;
}
LcTerm<TermVector> operator+(const TermVector& tv, const LcTerm<TermVector>& lctv)
{
  LcTerm<TermVector> rlctv(lctv);
  rlctv.push_back(&tv, 1.);
  return rlctv;
}
LcTerm<TermVector> operator-(const TermVector& tv, const LcTerm<TermVector>& lctv)
{
  LcTerm<TermVector> rlctv(lctv);
  rlctv *= -1.;
  rlctv.push_back(&tv, 1.);
  return rlctv;
}

void addVectorThenAssign(TermVector& tv1, const TermVector& tv2)
{
  tv1 += tv2;
}

//inner and hermitian product of (Su)TermVectors
//return ALWAYS a complex
complex_t dotRC(const TermVector& tv1, const TermVector& tv2)
{
  return innerProduct(tv1, tv2);
}

complex_t innerProduct(const TermVector& tv1, const TermVector& tv2)
{
  trace_p->push("innerProduct(TermVector,TermVector)");
  complex_t res = 0;
  for(cit_mustv it = tv1.begin(); it != tv1.end(); it++)
    {
      const Unknown* u = it->first;
      const SuTermVector* sutv1 = it->second;
      const SuTermVector* sutv2 = tv2.subVector_p(u);
      if(sutv2 != nullptr)
        {
          if(sutv2->entries() == nullptr && sutv2->scalar_entries() == nullptr && u->dual_p() != nullptr)  sutv2 = tv2.subVector_p(u->dual_p());   //
        }
      else if(u->dual_p() != nullptr)  sutv2 = tv2.subVector_p(u->dual_p());     //
      if(sutv1 != nullptr)
        {
          if(sutv1->entries() == nullptr && sutv1->scalar_entries() == nullptr && u->dual_p() != nullptr)  sutv1 = tv1.subVector_p(u->dual_p());   //
        }
      else if(u->dual_p() != nullptr)  sutv1 = tv1.subVector_p(u->dual_p());     //

      if(sutv2 != nullptr && sutv1 != nullptr) res += innerProduct(*sutv1, *sutv2);
    }
  trace_p->pop();
  return res;
}

complex_t hermitianProduct(const TermVector& tv1, const TermVector& tv2)
{
  trace_p->push("hermitianProduct(TermVector,TermVector)");
  complex_t res = 0;
  for(cit_mustv it = tv1.begin(); it != tv1.end(); it++)
    {
      const Unknown* u = it->first;
      const SuTermVector* sutv1 = it->second;
      const SuTermVector* sutv2 = tv2.subVector_p(u);
      if(sutv2 != nullptr)
        {
          if(sutv2->entries() == nullptr && sutv2->scalar_entries() == nullptr && u->dual_p() != nullptr) sutv2 = tv2.subVector_p(u->dual_p());  //
        }
      else if(u->dual_p() != nullptr) sutv2 = tv2.subVector_p(u->dual_p());     //
      if(sutv1 != nullptr)
        {
          if(sutv1->entries() == nullptr && sutv1->scalar_entries() == nullptr && u->dual_p() != nullptr)  sutv1 = tv1.subVector_p(u->dual_p());  //
        }
      else if(u->dual_p() != nullptr)  sutv1 = tv1.subVector_p(u->dual_p());     //
      if(sutv2 != nullptr && sutv1 != nullptr) res += hermitianProduct(*sutv1, *sutv2);
    }
  trace_p->pop();
  return res;
}

complex_t operator|(const TermVector& tv1, const TermVector& tv2)
{
  if(tv1.valueType() == _real && tv2.valueType() == _real)
    return innerProduct(tv1, tv2);
  return hermitianProduct(tv1, tv2);
}

// vector norms
real_t norm(const TermVector& tv, number_t l)
{
  switch(l)
    {
    case 0:
      return norminfty(tv);
    case 1:
      return norm1(tv);
    default:
      return norm2(tv);
    }
}

real_t norm2(const TermVector& tv)
{
  return std::sqrt(real(hermitianProduct(tv, tv)));
}

real_t norm1(const TermVector& tv)
{
  real_t r = 0.;
  trace_p->push("norm1(SuTermVector)");
  for(cit_mustv it = tv.begin(); it != tv.end(); it++) r += norm1(*it->second);
  trace_p->pop();
  return r;
}

real_t norminfty(const TermVector& tv)
{
  real_t r = 0.;
  trace_p->push("norm1(SuTermVector)");
  for(cit_mustv it = tv.begin(); it != tv.end(); it++)
    {
      r = std::max(r, norminfty(*it->second));
    }
  trace_p->pop();
  return r;
}

// return the value (in complex) of component being the largest one in absolute value
// travel all SCALAR components of all SuTermVector's to find the largest
complex_t TermVector::maxValAbs() const
{
  complex_t vmax=0., v;
  for(cit_mustv it = suTerms_.begin(); it != suTerms_.end(); it++)
    {
      v=it->second->maxValAbs();
      if(std::abs(v)> std::abs(vmax)) vmax=v;
    }
  return vmax;
}

//operation by component (abs, real and imag part)
TermVector& TermVector::toAbs()
{
  for(it_mustv it = begin(); it != end(); it++) it->second->toAbs();
  return *this;
}

TermVector abs(const TermVector& tv)
{
  TermVector atv=tv;
  atv.name()="abs("+tv.name()+")";
  atv.toAbs();
  return atv;
}

TermVector& TermVector::toReal()
{
  for(it_mustv it = begin(); it != end(); it++) it->second->toReal();
  return *this;
}

TermVector real(const TermVector& tv)
{
  TermVector atv=tv;
  atv.name()="real("+tv.name()+")";
  atv.toReal();
  return atv;
}

TermVector& TermVector::toImag()
{
  for(it_mustv it = begin(); it != end(); it++) it->second->toImag();
  return *this;
}

TermVector imag(const TermVector& tv)
{
  TermVector atv=tv;
  atv.name()="imag("+tv.name()+")";
  atv.toImag();
  return atv;
}

TermVector& TermVector::toComplex()
{
  for(it_mustv it = begin(); it != end(); it++) it->second->toComplex();
  return *this;
}

TermVector complex(const TermVector& tv)
{
  TermVector atv=tv;
  atv.name()="complex("+tv.name()+")";
  atv.toComplex();
  return atv;
}

TermVector& TermVector::toConj()
{
  for(it_mustv it = begin(); it != end(); it++) it->second->toConj();
  return *this;
}

TermVector conj(const TermVector& tv)
{
  TermVector atv=tv;
  if(tv.valueType()==_real) warning("free_warning"," taking the conjugate of the REAL TermVector "+tv.name()+" is useless");
  else
    {
      atv.name()="complex("+tv.name()+")";
      atv.toConj();
    }
  return atv;
}

TermVector& TermVector::roundToZero(real_t aszero)
{
  for(it_mustv it = begin(); it != end(); it++) it->second->roundToZero(aszero);
  return *this;
}


// create Function handling TermVector (restrict to single unknown scalar/vector TermVector)
// may be useful to pass TermVector to OperatorOnUnknown
const Function& TermVector::toFunction() const
{
  if(!isSingleUnknown())
    {
      where("TermVector::toFunction()");
      error("term_not_suterm", name());
    }
  const SuTermVector& suTV=subVector();
  ValueType vt=suTV.valueType();
  StrucType st=suTV.strucType();
  dimen_t dimf=suTV.up()->dimFun(), nbc = suTV.up()->nbOfComponents();
  if(dimf > 1) st=_vector;
  if(nbc>1 && dimf>1)  error("fun_bad_dim",1,dimf);
  if(st!=_scalar && st!=_vector)
    {
      where("TermVector::toFunction()");
      error("scalar_or_vector");
    }
  Parameters* pars=new Parameters(this,"_TermVector");   //note: TermVector is not copied, may be unsafe
  pars->freeParams_=true;
  Function* fun_p=nullptr;
  if(vt==_real)
    {
      if(st==_scalar) fun_p=new Function(fun_EC_SR, *pars);
      else fun_p=new Function(fun_EC_VR, *pars);
    }
  else //complex values
    {
      if(st==_scalar) fun_p=new Function(fun_EC_SC, *pars);
      else fun_p=new Function(fun_EC_VC, *pars);
    }
  if(fun_p==nullptr)
    {
      where("TermVector::toFunction()");
      error("null_pointer","fun_p");
    }
  fun_p->name_= "interpolation from TermVector "+name_;
  fun_p->requireElt=true;
  //update dimension
  fun_p->dims_= dimPair(std::max(dimf,nbc),1);
  return *fun_p;
}

// ================================================================================================
//!< apply essential conditions to a TermVector (user tool); ReductionMethod not yet managed
//================================================================================================
TermVector& TermVector::applyEssentialConditions(const SetOfConstraints& soc, const ReductionMethod& rm)
{
  trace_p->push("TermVector::applyEssentialCondition(SetOfConstraints)");
  if (!soc.isGlobal())  //non global constraints, process on each SuTermVector
  {
    for (it_mustv it = this->begin(); it != this->end(); it++)
    {
      Constraints* ci = soc(it->first);
      if (ci!=nullptr) it->second->applyEssentialConditions(*ci);  //do the job on the current SuTermVector
    }
  }
  else  //global constraints, process global representation of TermVector
  {
    toGlobal();
    xlifepp::applyEssentialConditions(*scalar_entries_p, cdofs_, *soc.begin()->second);
    toLocal(true);  //update local representation
  }
  trace_p->pop();
  return *this;
}

TermVector& TermVector::applyEssentialConditions(const EssentialConditions& ecs, const ReductionMethod& rm)
{
  trace_p->push("TermVector::applyEssentialCondition(EssentialConditions)");
  if (ecs.constraints_==nullptr) ecs.constraints_ = new SetOfConstraints(buildConstraints(ecs));
  applyEssentialConditions(*ecs.constraints_,rm);
  trace_p->pop();
  return *this;
}

// ================================================================================================
//! evaluate dofs on a function: dof_i(f) for any dofs related to unknown u and domain
// return a single unknown TermVector
// when involving a Lagrange space, it produces the same TermVector as the constructor TermVector(u,dom,F)
// for Hermite like intepolation, grad(f) and may be grad2(f) must be provided
// up to now: ONLY FOR SCALAR Dofs
// ================================================================================================
TermVector interpolent(const Unknown& u, const GeomDomain& dom, const Function& f,const Function& gradf, const Function& grad2f)
{
 if(f.strucType()!=_scalar)
    error("free_error","interpolent is only designed for scalar dofs");
 if(f.valueType()==_real)
 {
   TermVector r(u,dom,0.);
   SuTermVector sut = r.subVector();
   for(int i=1;i<=sut.nbDofs();i++)
     r.setValue(i,sut.dof(i)(f,gradf,grad2f));
   return r;
 }
 TermVector r(u,dom,complex_t(0.));
 SuTermVector sut = r.subVector();
 for(int i=1;i<=sut.nbDofs();i++)
   r.setValue(i,sut.dof(i)(f,gradf,grad2f));
 return r;
}


/* ================================================================================================
   merge two TermVectors, preserving values of first one when common dofs
   do not cofuse with + operator where values on common dofs are added
   merging acts only on SuTermVectors of TermVectors, it does not merged global representation
   ================================================================================================ */
TermVector merge(const TermVector& tu, const TermVector& tv)
{
  TermVector tm(tu);
  return tm.merge(tv);
}

/* ================================================================================================
   interpolation of a TermVector on a Lagrange space
       create a new TermVector (tr) from a given one (tv) using nodes linked to unknown (u) located in domain (dom)
       for each sutermvector
           sutr(i) = sum_j sutv_j w_j(Mi)  Mi nodes of Lagrange u-space, Mi in dom
                                           wj shape functions related to sutv

       if the sutermvector sutv is linked to u space only restriction to domain dom is processed

       Note: for L2 space projection see project function
   ================================================================================================ */
TermVector interpolate(const Unknown& u, const GeomDomain& dom,
                       const TermVector& tv, const string_t& na)
{
  trace_p->push("interpolate(Unknown,GeomDomain, ...)");
  string_t nam=na;
  if(nam=="") nam=tv.name()+"_interpolated_"+u.space()->name()+"_"+dom.name();
  TermVector tr;
  for(cit_mustv it = tv.begin(); it != tv.end(); it++)
    {
      const Unknown* ui = it->first;   //unknown of suTermVector
      SuTermVector* sut = it->second;  //suTermVector itself
      if(ui->space()==u.space())       //same space, take the restriction to domain dom
        {
          SuTermVector* nsut=sut->onDomain(dom);
          if(nsut==nullptr) warning("term_wrong_domain",dom.name());
          if(nsut!=nullptr) tr.insert(&u,nsut);
        }
      else   //interpolate
        {
          SuTermVector* nsut=sut->interpolate(u,dom);
          if(nsut==nullptr) warning("term_wrong_space",u.space()->name());
          if(nsut!=nullptr) tr.insert(&u,nsut);
        }
    }
  tr.computed()=true;
  trace_p->pop();
  tr.name(nam);
  return tr;
}

//=================================================================================
// TermVectors member functions
//=================================================================================

// move all TermVector's to real
TermVectors& TermVectors::toReal()
{
  vector<TermVector>::iterator itv=begin();
  for(; itv!=end(); itv++) itv->toReal();
  return *this;
}

TermVectors& TermVectors::toImag()
{
  vector<TermVector>::iterator itv=begin();
  for(; itv!=end(); itv++) itv->toImag();
  return *this;
}

TermVectors& TermVectors::toLocal(bool keepGlobal)
{
  vector<TermVector>::iterator itv=begin();
  for(; itv!=end(); itv++) itv->toLocal(keepGlobal);
  return *this;
}

TermVectors& TermVectors::toVector(bool keepScalar)
{
  vector<TermVector>::iterator itv=begin();
  for(; itv!=end(); itv++) itv->toVector(keepScalar);
  return *this;
}

//! apply essential conditions to a TermVectors
TermVectors& TermVectors::applyEssentialConditions(const EssentialConditions& ecs, const ReductionMethod& rm)
{
  vector<TermVector>::iterator itv=begin();
  for(; itv!=end(); itv++) itv->applyEssentialConditions(ecs,rm);
  return *this;

}

//! apply constraints to a TermVectors
TermVectors& TermVectors::applyEssentialConditions(const SetOfConstraints& sec, const ReductionMethod& rm)
{
  vector<TermVector>::iterator itv=begin();
  for(; itv!=end(); itv++) itv->applyEssentialConditions(sec,rm);
  return *this;

}

void TermVectors::print(std::ostream& os) const
{
  os<<"list of TermVector's ("<<size()<<")"<<eol;
  if(theVerboseLevel<2) return;
  vector<TermVector>::const_iterator itv=begin();
  for(; itv!=end(); itv++) itv->print(os);
}

/*!
  update the name of each TermVector and their SuTermVectors
  SuTermVector name is defined as follows: name(TermVector)_name(Unknown)
 */
void TermVectors::name(const string_t& nam)
{
  vector<TermVector>::iterator itv=begin();
  number_t i=1;
  for(; itv!=end(); itv++, i++) itv->name(nam);
}

std::ostream& operator<<(std::ostream& os, const TermVectors& tvs)
{
  tvs.print(os);
  return os;
}

//print term on output stream
std::ostream& operator<<(std::ostream& out,const TermVector& t)
{
  t.print(out);
  return out;
}

// save TermVectors to files
void saveToFile(const string_t& filename, const TermVectors& tvs, const std::vector<Parameter>& ps)
{
  if (tvs.size()==0)
  {
    warning("is_void","tvs");
    return;
  }
  if (tvs.size()==1)
  {
    std::list<const TermVector*> tvlist={&(tvs[0])};
    saveToFile(filename, tvlist, ps);
  }
  else
  {
    std::pair<string_t, string_t> rootext=fileRootExtension(trim(filename), Environment::authorizedSaveToFileExtensions());
    for(number_t n=0; n<tvs.size(); n++)
    {
      string_t filename2=fileNameFromComponents(rootext.first, tostring(n+1), rootext.second);
      std::list<const TermVector*> tvlist={&(tvs[n])};
      saveToFile(filename2, tvlist, ps);
    }
  }
}

//====================================================================================================
// stuff to rebuild domains
//====================================================================================================
/*!
    main routine for levelset method

    \param tv TermVector containing the criteria
    \param mainDomNum domain number of the main domain (the domain number is the index in Mesh::domains_)
    \param insideDomNums domain numbers of the obstacles to find by the levelset method (one or more)
    \param outside if true, elements of interface between main domain and inner domains (obstacles)
           are considered in the inner domain, in the main domain otherwise.
 */
std::vector<number_t> domainsFromLevelSet(TermVector& tv, number_t mainDomNum, std::vector<number_t> insideDomNums, bool outside)
{
  trace_p->push("domainsFromLevelSet(...)");
  // test if tv is scalar and real
  if(!tv.isSingleUnknown()) error("term_not_suterm", tv.name());
  SuTermVector* sutv=tv.subVector_p(tv.unknown());
  if(sutv->valueType() != _real || sutv->strucType() != _scalar)
    error("value_wrongtype", words("structure", sutv->strucType()), words("value", sutv->valueType()),
          words("structure", _scalar), words("value", _real));
  Vector<real_t> values=tv.asRealVector();

  Space* sp=sutv->spacep();
  if(!sp->feSpace()) error("not_fe_space_type", sp->name());

  const Mesh* m=sp->domain()->mesh();
  number_t nbMeshElements=m->nbOfElements();
  std::vector<number_t> eltCol(nbMeshElements,mainDomNum);

  // we need to build the list of geom element numbers for each plain domain (main and insides)
  std::map<number_t, std::set<number_t> > eltNumsPerPlainDomains;

  for(number_t d=0; d < insideDomNums.size(); ++d)
    {
      for(number_t e=0; e < m->domain(insideDomNums[d]).meshDomain()->numberOfElements(); ++e)
        {
          eltNumsPerPlainDomains[insideDomNums[d]].insert(m->domain(insideDomNums[d]).meshDomain()->element(e+1)->number());
        }
    }
  for(number_t e=0; e < m->domain(mainDomNum).meshDomain()->numberOfElements(); ++e)
    {
      eltNumsPerPlainDomains[mainDomNum].insert(m->domain(mainDomNum).meshDomain()->element(e+1)->number());
    }

  for(number_t i=0; i < sp->feSpace()->dofs.size(); ++i)
    {
      FeDof& fed=sp->feSpace()->dofs[i];
      std::vector<std::pair<Element*, number_t> > parentElts = fed.elements();
      std::vector<std::pair<Element*, number_t> >::const_iterator it_pe;
      for(it_pe=parentElts.begin(); it_pe != parentElts.end(); ++it_pe)
        {
          number_t geltNum=it_pe->first->geomElt_p->number()-1;
          if(eltCol[geltNum] == mainDomNum)
            {
              if(values[i] < 0. && outside)
                {
                  for(number_t d=0; d < insideDomNums.size(); ++d)
                    {
                      if(eltNumsPerPlainDomains[insideDomNums[d]].count(geltNum+1))
                        {
                          eltCol[geltNum] = insideDomNums[d];
                        }
                    }
                }
            }
          else
            {
              if(values[i] > 0. && !outside)
                {
                  eltCol[geltNum] = mainDomNum;
                }
            }
        }
    }

  trace_p->pop();
  return eltCol;
}

/*! set color of geom elements of domain according to vertex values and a coloring rule
    dom: the domain to colorize
    val: a single unknown TermVector containing the values used to colorize
    cr: the ColoringRule, i.e. a function of the form: real_t cr(const Geomelement& gelt, const std::vector<real_t>& v)
          returning the color of the geometric element (see defaultColoringRule function for instance)
*/
void setColor(const GeomDomain& dom, const TermVector& tv, ColoringRule cr)
{
  trace_p->push("setColor(Domain, TermVector, ColoringRule)");
  if (!tv.isSingleUnknown()) error("term_not_suterm", tv.name());
  const SuTermVector* sutv=&tv.subVector();
  if (tv.valueType()!=_real || sutv->strucType()!=_scalar)
    error("value_wrongtype", words("structure", sutv->strucType()), words("value", tv.valueType()),
          words("structure", _scalar), words("value", _real));
  TermVector* tvdom=nullptr;
  if (sutv->domain()!=&dom)  //try with restriction of val to dom
  {
    tvdom = new TermVector(tv.onDomain(dom));
    sutv=&tvdom->subVector();
  }
  const MeshDomain* mdom=dom.meshDomain();
  if (mdom==nullptr) error("domain_notmesh", dom.name());
  std::set<number_t> vs = mdom->vertexNumbers();  //ascending list of vertex numbers of domain
  std::map<number_t,number_t> vertexIndex;        //map: mesh vertex number -> domain vertex number
  std::set<number_t>::iterator its=vs.begin();
  number_t k=1;
  for (; its!=vs.end(); ++its, k++) vertexIndex[*its]=k;
  const std::vector<Point>& meshnodes=dom.mesh()->nodes;
  std::vector<real_t> v(vs.size());
  std::vector<real_t>::iterator itv=v.begin();
  for (its=vs.begin(); its!=vs.end(); ++its, ++itv)
    *itv = sutv->evaluate(meshnodes[*its -1]).asReal();
  mdom->setColor(v, vertexIndex, cr);
  if (tvdom!=nullptr) delete tvdom;
  trace_p->pop();
}

/*! set color of geom elements of domain according to vertex vector values and a vector coloring rule
    dom: the domain to colorize
    val: a single unknown TermVector containing the vector values used to colorize
    vcr: the vector ColoringRule, i.e. a function of the form: real_t vcr(const Geomelement& gelt, const std::vector<Vector<real_t> >& v)
          returning the color of the geometric element (see defaultColoringRule function for instance)
*/
void setColor(const GeomDomain& dom, const TermVector& tv, VectorColoringRule vcr)
{
  trace_p->push("setColor(Domain, TermVector, ColoringRule)");
  if (!tv.isSingleUnknown()) error("term_not_suterm", tv.name());
  const SuTermVector* sutv=&tv.subVector();
  if (tv.valueType()!=_real || sutv->strucType()!=_vector)
    error("value_wrongtype", words("structure", sutv->strucType()), words("value", tv.valueType()),
          words("structure", _vector), words("value", _real));
  TermVector* tvdom=nullptr;
  if (sutv->domain()!=&dom)  //try with restriction of val to dom
  {
    tvdom = new TermVector(tv.onDomain(dom));
    sutv=&tvdom->subVector();
  }
  const MeshDomain* mdom=dom.meshDomain();
  if (mdom==nullptr) error("domain_notmesh", dom.name());
  std::set<number_t> vs = mdom->vertexNumbers();  //ascending list of vertex numbers of domain
  std::map<number_t,number_t> vertexIndex;        //map: mesh vertex number -> domain vertex number
  std::set<number_t>::iterator its=vs.begin();
  number_t k=1;
  for (; its!=vs.end(); ++its, k++) vertexIndex[*its]=k;
  const std::vector<Point>& meshnodes=dom.mesh()->nodes;
  std::vector<Vector<real_t> > v(vs.size());
  std::vector<Vector<real_t> >::iterator itv=v.begin();
  for (its=vs.begin(); its!=vs.end(); ++its, ++itv)
  {
    *itv = sutv->evaluate(meshnodes[*its -1]).asRealVector();
  }
  mdom->setColor(v, vertexIndex, vcr);
  if (tvdom!=nullptr) delete tvdom;
  trace_p->pop();
}

/*! set color of geom elements
    dom: the domain to colorize
    col: color value
*/
void setColor(const GeomDomain& dom, real_t r)
{
  trace_p->push("setColor(Domain, Real)");
  const MeshDomain* mdom=dom.meshDomain();
  if (mdom==nullptr) error("domain_notmesh",dom.name());
  std::vector<GeomElement*>::const_iterator itg=mdom->geomElements.begin();
  for (; itg!=mdom->geomElements.end(); ++itg)(*itg)->color = r;
  trace_p->pop();
}


/*! rebuild some plain domains from some values given by a single unknown TermVector and criteria
    the rebuilding works as follow:
    - rebuild each of given domains by using ComparisonFunction and element color
    - update side domains related to given domains (boundary or interface)

    ComparaisonFunction's are objects that handles simple boolean expressions involving boolean operator (e.g. _color == 0)
    For instance to update two domains dom1, dom2  distinguished by values 0 and 1
        rebuild(tv, dom1, _color==0, dom2, _color==1);

    doms: list of domain to be rebuilt
    crs: list of comparison criteria
    sidedoms: set of side domains to be rebuilt, if empty rebuild all side domains found
*/

//! rebuild some plain domains
void rebuild(std::vector<GeomDomain*>& doms, const std::vector<ComparisonFunction<> >& crs, const std::set<GeomDomain*>& sidedoms)
{
  trace_p->push("rebuild");

  // some checks
  if(doms.size()==0)
    error("is_void", "doms");
  if(doms.size()!=crs.size()) error("bad_size", "crs", doms.size(), crs.size());

  //find involved side domains
  std::map<GeomElement*, std::set<GeomDomain*> > domsIndex;  //index of all elements of doms
  std::map<GeomElement*, std::set<GeomDomain*> >::iterator itm;
  std::vector<GeomDomain*>::iterator itd=doms.begin();
  for(; itd!=doms.end(); ++itd)
    {
      std::vector<GeomElement*>& elts = (*itd)->meshDomain()->geomElements;
      std::vector<GeomElement*>::iterator ite=elts.begin();
      for(; ite!=elts.end(); ++ite)
        {
          itm=domsIndex.find(*ite);
          if(itm==domsIndex.end()) domsIndex[*ite]=std::set<GeomDomain*>();
          domsIndex[*ite].insert(*itd);
        }
    }
  std::map<GeomDomain*, std::list<GeomDomain*> > sideDomains;  //side domain -> list of domains of doms
  const Mesh* m =(*doms.begin())->mesh();
  const std::vector<GeomDomain*>& mdoms = m->domains();        //all domains of mesh
  std::vector<GeomDomain*>::const_iterator itmd=mdoms.begin();
  for(; itmd!=mdoms.end(); ++itmd)
    {
      MeshDomain* mdom = (*itmd)->meshDomain();
      if(mdom->isSideDomain())  // side domain
        {
          std::vector<GeoNumPair>& pars = (*mdom->geomElements.begin())->parentSides();
          std::vector<GeoNumPair>::iterator itp=pars.begin();
          for(; itp!=pars.end(); ++itp)  //loop on (parent elt, side number)
            {
              itm=domsIndex.find(itp->first);
              if(itm!=domsIndex.end())   //find an element
                {
                  for(itd=doms.begin(); itd!=doms.end(); ++itd)
                    if(itm->second.find(*itd)!=itm->second.end())  //domains found
                      {
                        if(sideDomains.find(*itmd)==sideDomains.end()) sideDomains[*itmd]=std::list<GeomDomain*>(1,*itd);
                        else sideDomains[*itmd].push_back(*itd);
                      }
                }
            }
        }
    }
  domsIndex.clear();

  // rebuild plain domains
  std::vector<ComparisonFunction<> >::const_iterator itc=crs.begin();
  for(itd=doms.begin(); itd!=doms.end(); ++itd,++itc)   //may be parallelized
    {
      (*itd)->meshDomain()->rebuild(*itc);
    }

  //rebuild side domains
  bool nosidedoms = sidedoms.size()==0;
  if(sideDomains.size()>0)
    {
      std::map<string_t,std::vector<GeoNumPair> > sideIndex;  //indexed list of all side
      std::map<string_t, GeomElement*> sideltIndex;  //indexed list of all side elt
      m->createSideIndex(sideIndex);
      m->createSideEltIndex(sideltIndex);
      std::map<GeomDomain*,std::set<GeomElement*> > parelts;   //set of elements of parents of side domain
      for(itd=doms.begin(); itd!=doms.end(); ++itd)  //loop on domains
        {
          std::vector<GeomElement*>& elts=(*itd)->meshDomain()->geomElements;
          parelts[*itd]=std::set<GeomElement*> (elts.begin(),elts.end());  //insert the list of parent elements
        }
      std::map<GeomDomain*, std::list<GeomDomain*> >::iterator its=sideDomains.begin();
      for(; its!=sideDomains.end(); ++its)  //may be parallelized
        {
          if(nosidedoms || sidedoms.find(its->first)!=sidedoms.end())
            {
              std::map<GeomDomain*,std::set<GeomElement*>* > parsidelts;   //set of elements of parents of side domain
              std::list<GeomDomain*>::iterator itl;
              for(itl=its->second.begin(); itl!=its->second.end(); ++itl)  parsidelts[*itl]=&parelts[*itl];
              its->first->meshDomain()->rebuild(sideIndex, sideltIndex, parsidelts); //refactor side domain
            }
        }
    }
  trace_p->pop();
}

//! rebuild one plain domain
void rebuild(GeomDomain& dom, const ComparisonFunction<>& cr)
{
  std::vector<GeomDomain*> doms(1,&dom);
  std::vector<ComparisonFunction<> > crs(1, cr);
  rebuild(doms, crs);
}

//! rebuild 2 plain domains
void rebuild(GeomDomain& dom1, const ComparisonFunction<>& cr1, GeomDomain& dom2, const ComparisonFunction<>& cr2)
{
  std::vector<GeomDomain*> doms(2,&dom1);
  doms[1]=&dom2;
  std::vector<ComparisonFunction<> > crs(2);
  crs[0]=cr1;
  crs[1]=cr2;
  rebuild(doms, crs);
}

//! rebuild one plain domain and one side domain
void rebuild(GeomDomain& dom, const ComparisonFunction<>& cr, GeomDomain& sdom)
{
  std::vector<GeomDomain*> doms(1,&dom);
  std::vector<ComparisonFunction<> > crs(1, cr);
  std::set<GeomDomain*> sdoms;
  sdoms.insert(&sdom);
  rebuild(doms, crs, sdoms);
}

//! rebuild 2 plain domains and one side domain
void rebuild(GeomDomain& dom1, const ComparisonFunction<>& cr1, GeomDomain& dom2, const ComparisonFunction<>& cr2, GeomDomain& sdom)
{
  std::vector<GeomDomain*> doms(2,&dom1);
  doms[1]=&dom2;
  std::vector<ComparisonFunction<> > crs(2);
  crs[0]=cr1;
  crs[1]=cr2;
  std::set<GeomDomain*> sdoms;
  sdoms.insert(&sdom);
  rebuild(doms, crs, sdoms);
}

//! rebuild 2 plain domains and two side domains
void rebuild(GeomDomain& dom1, const ComparisonFunction<>& cr1, GeomDomain& dom2, const ComparisonFunction<>& cr2,
             GeomDomain& sdom1, GeomDomain& sdom2)
{
  std::vector<GeomDomain*> doms(2,&dom1);
  doms[1]=&dom2;
  std::vector<ComparisonFunction<> > crs(2);
  crs[0]=cr1;
  crs[1]=cr2;
  std::set<GeomDomain*> sdoms;
  sdoms.insert(&sdom1);
  sdoms.insert(&sdom2);
  rebuild(doms, crs, sdoms);
}

/*======================================================================================================
  implementation of extension functions involving TermVector to avoid cross dependences!
  ======================================================================================================
  Perform extension of a function f from a domain Ds to an other domain De, as follows
      f_ext(M) = f(g(M)) o h(M)
  where g : Rn->Rn a map linking De to Ds
        h : any Function (nullptr means h=1)
        o : consistent operation between f and h, generally *
  =======================================================================================*/
//evaluate at a given point P
Value extension(const Function& f, const Function& g, const Point& P)
{
    Vector<real_t> Q;
    Value val(f(Point(g(P,Q))));
    return val;
}
Value extension(const Function& f, const Function& g, const Point& P, const Function& h)
{
    Vector<real_t> Q;
    Value val(f(Point(g(P,Q))));
    return val*=h(P);
}
Value extension(const TermVector& f, const Function& g, const Point& P)
{
    Vector<real_t> Q;
    Value val=f.evaluate(Point(g(P,Q)),true);
    return val;
}
Value extension(const TermVector& f, const Function& g, const Point& P, const Function& h)
{
    Vector<real_t> Q;
    Value val=f.evaluate(Point(g(P,Q)),true);
    return val*=h(P);
}

//evaluate at 'node' of domain dom related to Lagrange interpolation supported by u
TermVector extension(const Function& f, const Function& g, const GeomDomain& dom, const Unknown& u)
{
    if(!u.isLagrange()) error("free_error","extension(...) is restricted to Lagrange interpolation");
    TermVector tv;
    Value v=extension(f,g,Point(0,0,0));
    switch(v.strucType())
    {
      case _scalar :
        {
            if(v.valueType()==_real)  tv=TermVector(u,dom,0.);
            else  tv=TermVector(u,dom,complex_t(0.));
        } break;
      case _vector :
        {
            int n=v.dims().first;
            if(v.valueType()==_real)  tv=TermVector(u,dom,Vector<real_t>(n,0.));
            else  tv=TermVector(u,dom,Vector<complex_t>(n,complex_t(0.)));
        } break;
       default : error("free_error","extension restricted to scalar or vector");
    }
    VectorEntry* ent=tv.subVector().entries();
    int n=tv.subVector().nbDofs();
    for(int i=1;i<=n;++i)
      ent->setValue(i, extension(f,g,tv.subVector().dof(i).coords()));
    return tv;
}
TermVector extension(const Function& f, const Function& g, const GeomDomain& dom, const Unknown& u, const Function& h)
{
    if(!u.isLagrange()) error("free_error","extension(...) is restricted to Lagrange interpolation");
    TermVector tv;
    Value v=extension(f,g,Point(0,0,0),h);
    switch(v.strucType())
    {
      case _scalar :
        {
            if(v.valueType()==_real)  tv=TermVector(u,dom,0.);
            else  tv=TermVector(u,dom,complex_t(0.));
        } break;
      case _vector :
        {
            int n=v.dims().first;
            if(v.valueType()==_real)  tv=TermVector(u,dom,Vector<real_t>(n,0.));
            else  tv=TermVector(u,dom,Vector<complex_t>(n,complex_t(0.)));
        } break;
       default : error("free_error","extension restricted to scalar or vector");
    }
    VectorEntry* ent=tv.subVector().entries();
    int n=tv.subVector().nbDofs();
    for(int i=1;i<=n;++i)
      ent->setValue(i, extension(f,g,tv.subVector().dof(i).coords(),h));
    return tv;
}
TermVector extension(const TermVector& f, const Function& g, const GeomDomain& dom, const Unknown& u)
{
    if(!u.isLagrange()) error("free_error","extension(...) is restricted to Lagrange interpolation");
    TermVector tv;
    Value v=extension(f,g,Point(0,0,0));
    switch(v.strucType())
    {
      case _scalar :
        {
            if(v.valueType()==_real)  tv=TermVector(u,dom,0.);
            else  tv=TermVector(u,dom,complex_t(0.));
        } break;
      case _vector :
        {
            int n=v.dims().first;
            if(v.valueType()==_real)  tv=TermVector(u,dom,Vector<real_t>(n,0.));
            else  tv=TermVector(u,dom,Vector<complex_t>(n,complex_t(0.)));
        } break;
       default : error("free_error","extension restricted to scalar or vector");
    }
    VectorEntry* ent=tv.subVector().entries();
    int n=tv.subVector().nbDofs();
    for(int i=1;i<=n;++i)
      ent->setValue(i, extension(f,g,tv.subVector().dof(i).coords()));
    return tv;
}
TermVector extension(const TermVector& f, const Function& g, const GeomDomain& dom, const Unknown& u, const Function& h)
{
    if(!u.isLagrange()) error("free_error","extension(...) is restricted to Lagrange interpolation");
    TermVector tv; ;
    Value v=extension(f,g,Point(0,0,0),h);
    switch(v.strucType())
    {
      case _scalar :
        {
            if(v.valueType()==_real)  tv=TermVector(u,dom,0.);
            else  tv=TermVector(u,dom,complex_t(0.));
        } break;
      case _vector :
        {
            int n=v.dims().first;
            if(v.valueType()==_real)  tv=TermVector(u,dom,Vector<real_t>(n,0.));
            else  tv=TermVector(u,dom,Vector<complex_t>(n,complex_t(0.)));
        } break;
       default : error("free_error","extension restricted to scalar or vector");
    }
    VectorEntry* ent=tv.subVector().entries();
    int n=tv.subVector().nbDofs();
    for(int i=1;i<=n;++i)
      ent->setValue(i, extension(f,g,tv.subVector().dof(i).coords(),h));
    return tv;
}

//======================================================================================================
//implementation of EssentialCondition member functions involving TermVector to avoid cross dependences!
//======================================================================================================

EssentialCondition::EssentialCondition(const TermVector& tv, const complex_t& c)
{
  fun_p=nullptr;
  lf_p = new TermVector(tv); //full copy
  clf=c;
  type_=_lfEc;
}

//constructor lf(u) = r/c
EssentialCondition::EssentialCondition(const LinearForm& lf, const complex_t& c)
{
  fun_p=nullptr;
  lf_p = new TermVector(lf, _name="ec_from_lf");
  lf_p->compute();
  clf=c;
  type_=_lfEc;
}

//pointer's full copy assuming current EC object is clean
void EssentialCondition::copy(const EssentialCondition& ec)
{
  fun_p=nullptr;
  lf_p=nullptr;
  if(ec.fun_p!=nullptr) fun_p=new Function(*ec.fun_p);   //full copy
  if(ec.lf_p!=nullptr) lf_p=new TermVector(*ec.lf_p);    //full copy
  ecTerms_=ec.ecTerms_;
  clf=ec.clf;
  type_=ec.type_;
  ecMethod=ec.ecMethod;
  redundancyTolerance_=ec.redundancyTolerance_;
}

//clear essential condition
void EssentialCondition::clear()
{
  if(fun_p!=nullptr) delete fun_p;
  if(lf_p!=nullptr) delete lf_p;
  fun_p=nullptr;
  lf_p=nullptr;
  ecTerms_.clear();
  clf=0.;
  type_=_undefEcType;
}

bool EssentialCondition::isSingleUnknownTV() const
{
  return lf_p->isSingleUnknown();
}

dimen_t EssentialCondition::nbOfUnknownsTV() const
{
  return lf_p->nbOfUnknowns();
}

const Unknown* EssentialCondition::unknownTV(number_t i) const
{
  return lf_p->unknown(i);
}

std::set<const Unknown*> EssentialCondition::unknownsTV() const
{
  return lf_p->unknowns();
}

void EssentialCondition::printTV(std::ostream& os) const
{
  os<<"TermVector: "<<lf_p->name()<<" rhs = "<<clf<<eol;
  if(theVerboseLevel>10) os<<*lf_p<<eol;
}

string_t EssentialCondition::nameTV() const
{
  std::stringstream ss;
  ss<<lf_p->name()<<" = ";
  if(std::abs(clf.imag())==0.) ss<<clf.real();
  else ss<<clf;
  return ss.str();
}

// construct essential condition of the form lf(u)=r/c
EssentialCondition LinearForm::operator=(const complex_t& c)
{
  return EssentialCondition(*this,c);
}

// construct essential condition of the form T=r/c
EssentialCondition TermVector::operator=(const complex_t& c)
{
  return EssentialCondition(*this,c);
}


// EssentialCondition constructor LcOperatorOnUnknown = TermVector on D (D given by the LcOperatorOnUnknown !)
EssentialCondition::EssentialCondition(const LcOperatorOnUnknown& lc, const TermVector& TV, ECMethod ecm)
{
  lf_p=nullptr;
  ecTerms_=lc;
  fun_p = const_cast<Function*>(&TV.toFunction());
  ecMethod=ecm;
  setType();
}

EssentialCondition LcOperatorOnUnknown::operator = (const TermVector & TV)    // construct  LcOp(u)=TV
{
  return EssentialCondition(*this,TV);
}

EssentialCondition OperatorOnUnknown::operator = (const TermVector & TV)      // construct  Op(u)=TV
{
  return EssentialCondition(LcOperatorOnUnknown(*this),TV);
}

EssentialCondition Unknown::operator = (const TermVector & TV)                // construct  u = TV
{
  return EssentialCondition(LcOperatorOnUnknown(*this),TV);
}

/*
  implemented here to avoid conflict between libs
*/
const TermVector* Function::termVector() const    //! return TermVector pointer when interpolated function (0 if not)
{
  if(fun_==nullptr ) return nullptr;
  if(params_!=nullptr && params_->contains("_TermVector"))
  {
      const void* p=nullptr;
      return static_cast<const TermVector*>(params_->get("_TermVector",p));
  }
  return nullptr;
}

//! return a dof coordinate from TermVector if interpolated function
Point Function::termVectorPoint() const
{
    const TermVector* TV=termVector();
    if(TV!=nullptr) return TV->dof(*TV->unknown(),1).coords();  // return first dof coords as P
    return Point(0.);
}

//! tensor inner product of single unknown vector TermVector => single unknown scalar TermVector (not conjugate)
TermVector tensorInnerProduct(const TermVector& s1, const TermVector& s2)
{
  if(!s1.isSingleUnknown() || !s2.isSingleUnknown())
    error("free_error","tensorInnerProduct(TermVector,TermVector) handles only single unknown TermVector");
  return TermVector(tensorInnerProduct(s1.subVector(),s2.subVector()));
}

//! tensor hermitian product of single unknown vector TermVector => single unknown scalar TermVector
TermVector tensorHermitianProduct(const TermVector& s1, const TermVector& s2)
{
  if(!s1.isSingleUnknown() || !s2.isSingleUnknown())
    error("free_error","tensorHermitianProduct(TermVector,TermVector) handles only single unknown TermVector");
  return TermVector(tensorHermitianProduct(s1.subVector(),s2.subVector()));
}

//! tensor cross product of single unknown vector TermVector => single unknown vector/scalar TermVector (in 3D/2D)
TermVector tensorCrossProduct(const TermVector& s1, const TermVector& s2)
{
  if(!s1.isSingleUnknown() || !s2.isSingleUnknown())
    error("free_error","tensorCrossProduct(TermVector,TermVector) handles only single unknown TermVector");
  return TermVector(tensorCrossProduct(s1.subVector(),s2.subVector()));
}

/* functions evaluating a single unknown TermVector passed in Parameters as void * and named _TermVector
   TermVector type has to be consistent with the return value
   no check here !!
*/
real_t fun_EC_SR(const Point& P, Parameters& pars)
{
  const void* p=nullptr;
  const TermVector* TV=static_cast<const TermVector*>(pars.get("_TermVector",p));
  if(TV==nullptr)
    {
      where("fun_EC_SR(Point, Parameters)");
      error("null_pointer","TV");
    }
  //GeomElement* gelt=const_cast<GeomElement*>(geomElementFromParameters(pars));  old style
  GeomElement* gelt=getElementP();
  if(gelt==nullptr || (gelt!=nullptr && gelt->elementDim()!=TV->subVector().domain()->dim())) //check that TV supports gelt using dimension (to be improved)
    return TV->evaluate(P).value<real_t>();  //locate element and interpolate (slow)
  const SuTermVector& suTV = TV->subVector();
  const Space* sp = suTV.spacep();
  sp->buildgelt2elt(); // build gelt to elt map, nothing done if it exists
  number_t numElt=sp->numElement(gelt);
  if(numElt==sp->nbOfElements())   // because numElement() return element.size() if not found
  {
      where("fun_EC_SR(Point, Parameters)");
      error("geoelt_not_found");
  }
  real_t res;
  return sp->element(numElt).interpolate(*TV->subVector().entries()->rEntries_p, P, sp->elementDofs(numElt), res,_id);
}

complex_t fun_EC_SC(const Point& P, Parameters& pars)
{
  const void* p=nullptr;
  const TermVector* TV=static_cast<const TermVector*>(pars.get("_TermVector",p));
  if(TV==nullptr)
    {
      where("fun_EC_SC(Point, Parameters)");
      error("null_pointer","TV");
    }
  //GeomElement* gelt=const_cast<GeomElement*>(geomElementFromParameters(pars));
  GeomElement* gelt=getElementP();
  if(gelt==nullptr || gelt->elementDim()!=TV->subVector().domain()->dim()) return TV->evaluate(P).value<complex_t>();  //locate element and interpolate (slow)
  const SuTermVector& suTV = TV->subVector();
  const Space* sp = suTV.spacep();
  sp->buildgelt2elt(); // build gelt to elt map, nothing done if it exists
  number_t numElt=sp->numElement(gelt);
  if(numElt==sp->nbOfElements())   // because numElement() return element.size() if not found
  {
      where("fun_EC_SC(Point, Parameters)");
      error("geoelt_not_found");
  }
  complex_t res;
  return sp->element(numElt).interpolate(*TV->subVector().entries()->cEntries_p, P, sp->elementDofs(numElt), res,_id);
}

Vector<real_t> fun_EC_VR(const Point& P, Parameters& pars)
{
  const void* p=nullptr;
  const TermVector* TV=static_cast<const TermVector*>(pars.get("_TermVector",p));
  if(TV==nullptr)
    {
      where("fun_EC_VR(Point, Parameters)");
      error("null_pointer", "TV");
    }
//GeomElement* gelt=const_cast<GeomElement*>(geomElementFromParameters(pars));
  GeomElement* gelt=getElementP();
  if(gelt==nullptr || gelt->elementDim()!=TV->subVector().domain()->dim()) return TV->evaluate(P).value<Vector<real_t> >();  //locate element and interpolate (slow)
  const SuTermVector& suTV = TV->subVector();
  const Space* sp = suTV.spacep();
  sp->buildgelt2elt(); // build gelt to elt map, nothing done if it exists
  number_t numElt=sp->numElement(gelt);
  if(numElt==sp->nbOfElements())   // because numElement() return element.size() if not found
  {
      where("fun_EC_VR(Point, Parameters)");
      error("geoelt_not_found");
  }
  Vector<real_t> res;
  return sp->element(numElt).interpolate(*TV->subVector().entries()->rvEntries_p, P, sp->elementDofs(numElt), res,_id);
}

Vector<complex_t> fun_EC_VC(const Point& P, Parameters& pars)
{
  const void* p=nullptr;
  const TermVector* TV=static_cast<const TermVector*>(pars.get("_TermVector",p));
  if(TV==nullptr)
    {
      where("fun_EC_VC(Point, Parameters)");
      error("null_pointer", "TV");
    }
  //GeomElement* gelt=const_cast<GeomElement*>(geomElementFromParameters(pars));
  GeomElement* gelt=getElementP();
  if(gelt==nullptr || gelt->elementDim()!=TV->subVector().domain()->dim()) return TV->evaluate(P).value<Vector<complex_t> >();  //locate element and interpolate (slow)
  const SuTermVector& suTV = TV->subVector();
  const Space* sp = suTV.spacep();
  sp->buildgelt2elt(); // build gelt to elt map, nothing done if it exists
  number_t numElt=sp->numElement(gelt);
  if(numElt==sp->nbOfElements())   // because numElement() return element.size() if not found
  {
      where("fun_EC_VC(Point, Parameters)");
      error("geoelt_not_found");
  }
  Vector<complex_t> res;
  return sp->element(numElt).interpolate(*TV->subVector().entries()->cvEntries_p, P, sp->elementDofs(numElt), res,_id);
}

//==========================================================================================
// left and right operations with (single unknown) TermVector and Unknown
//==========================================================================================
void checkTermVectorInOperator(const TermVector& tv, const string_t& op)
{
  if(!tv.isSingleUnknown())
    {
      where("checkTermVectorInOperator(TermVector, String)");
      error("term_not_suterm", tv.name());
    }
  const Space* sp = tv.subVector().spacep();
  if(sp==nullptr) error("null_pointer","sp");
  if(!sp->isFE()) error("not_fe_space_type", sp->name());
  if(!tv.subVector().computed()) error("not_computed_term", tv.name());
  if(tv.subVector().entries()->isEmpty()) error("term_no_entries");
  sp->buildgelt2elt();  //to be thread safe in future computation
}

OperatorOnUnknown&  operator*(const Unknown& un, const TermVector& tv)  //! u*tv
{
  checkTermVectorInOperator(tv,"u*tv");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _product, false));
}

OperatorOnUnknown&  operator|(const Unknown& un, const TermVector& tv) //! u|tv
{
  checkTermVectorInOperator(tv,"u|tv");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _innerProduct, false));
}

OperatorOnUnknown&  operator^(const Unknown& un, const TermVector& tv) //! u^tv
{
  checkTermVectorInOperator(tv,"u^tv");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _crossProduct, false));
}

OperatorOnUnknown&  operator%(const Unknown& un, const TermVector& tv) //! u%tv
{
  checkTermVectorInOperator(tv,"u%tv");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _contractedProduct, false));
}

OperatorOnUnknown&  operator*(const TermVector& tv, const Unknown& un) //! tv*u
{
  checkTermVectorInOperator(tv,"tv*u");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _product, true));
}

OperatorOnUnknown&  operator|(const TermVector& tv, const Unknown& un) //! tv|u
{
  checkTermVectorInOperator(tv,"tv|u");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _innerProduct, true));
}

OperatorOnUnknown&  operator^(const TermVector& tv, const Unknown& un) //! tv^u
{
  checkTermVectorInOperator(tv,"tv^u");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _crossProduct, true));
}

OperatorOnUnknown& operator%(const TermVector& tv, const Unknown& un) //! tv%u
{
  checkTermVectorInOperator(tv,"tv%u");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _contractedProduct, true));
}

OperatorOnUnknown& operator*(const TestFunction& un, const TermVector& tv)  //! u*tv
{
  checkTermVectorInOperator(tv,"u*tv");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _product, false));
}

OperatorOnUnknown& operator|(const TestFunction& un, const TermVector& tv) //! u|tv
{
  checkTermVectorInOperator(tv,"u|tv");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _innerProduct, false));
}

OperatorOnUnknown& operator^(const TestFunction& un, const TermVector& tv) //! u^tv
{
  checkTermVectorInOperator(tv,"u^tv");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _crossProduct, false));
}

OperatorOnUnknown& operator%(const TestFunction& un, const TermVector& tv) //! u%tv
{
  checkTermVectorInOperator(tv,"u%tv");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _contractedProduct, false));
}

OperatorOnUnknown& operator*(const TermVector& tv, const TestFunction& un) //! tv*u
{
  checkTermVectorInOperator(tv,"tv*u");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _product, true));
}

OperatorOnUnknown& operator|(const TermVector& tv, const TestFunction& un) //! tv|u
{
  checkTermVectorInOperator(tv,"tv|u");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _innerProduct, true));
}

OperatorOnUnknown& operator^(const TermVector& tv, const TestFunction& un) //! tv^u
{
  checkTermVectorInOperator(tv,"tv^u");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _crossProduct, true));
}

OperatorOnUnknown& operator%(const TermVector& tv, const TestFunction& un) //! tv%u
{
  checkTermVectorInOperator(tv,"tv%u");
  return *(new OperatorOnUnknown(un, tv.toFunction(), _contractedProduct, true));
}

//==========================================================================================
// left and right operations with (single unknown) TermVector and OperatorOnUnknown
//==========================================================================================
OperatorOnUnknown& operator*(OperatorOnUnknown& opu, const TermVector& tv) //! opu*tv
{
  checkTermVectorInOperator(tv,"opu*tv");
  return updateRight(opu, tv.toFunction(), _product);
}

OperatorOnUnknown& operator|(OperatorOnUnknown& opu, const TermVector& tv) //! opu|tv
{
  checkTermVectorInOperator(tv,"opu|tv");
  return updateRight(opu, tv.toFunction(), _innerProduct);
}

OperatorOnUnknown& operator^(OperatorOnUnknown& opu, const TermVector& tv) //! opu^v
{
  checkTermVectorInOperator(tv,"opu^tv");
  return updateRight(opu, tv.toFunction(), _crossProduct);
}

OperatorOnUnknown& operator%(OperatorOnUnknown& opu, const TermVector& tv) //! opu%tv
{
  checkTermVectorInOperator(tv,"opu%tv");
  return updateRight(opu, tv.toFunction(), _contractedProduct);
}

OperatorOnUnknown& operator*(const TermVector& tv, OperatorOnUnknown& opu) //! tv*opu
{
  checkTermVectorInOperator(tv,"tv*opu");
  return updateLeft(opu, tv.toFunction(), _product);
}

OperatorOnUnknown& operator|(const TermVector& tv, OperatorOnUnknown& opu) //! tv|opu
{
  checkTermVectorInOperator(tv,"tv|opu");
  return updateLeft(opu, tv.toFunction(), _innerProduct);
}

OperatorOnUnknown& operator^(const TermVector& tv, OperatorOnUnknown& opu) //! tv^opu
{
  checkTermVectorInOperator(tv,"tv^opu");
  return updateLeft(opu, tv.toFunction(), _crossProduct);
}

OperatorOnUnknown& operator%(const TermVector& tv, OperatorOnUnknown& opu) //! tv%opu
{
  checkTermVectorInOperator(tv,"tv%opu");
  return updateLeft(opu, tv.toFunction(), _contractedProduct);
}

Parameter::Parameter(const TermVectors& tvs, const string_t& nm, const string_t& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pointerTermVectors)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  TermVectors* tvsb=const_cast<TermVectors*>(&tvs);
  p_ = static_cast<void *>(tvsb);
}

Parameter::Parameter(const TermVectors& tvs, const string_t& nm, const Strings& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pointerTermVectors)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  TermVectors* tvsb=const_cast<TermVectors*>(&tvs);
  p_ = static_cast<void *>(tvsb);
}

Parameter& Parameter::operator=(const TermVectors& tvs)
{
  if (p_!=nullptr) deletePointer();
  TermVectors* tvsb=const_cast<TermVectors*>(&tvs);
  p_ = static_cast<void *>(tvsb);
  type_=_pointerTermVectors;
  return *this;
}

void deleteTermVectors(void *p)
{
  p=nullptr;
  // if (p!=nullptr) delete reinterpret_cast<TermVectors*>(p);
}

void* cloneTermVectors(const void* p)
{
  return const_cast<void *>(p);
  // return reinterpret_cast<void*>(new TermVectors(*reinterpret_cast<const TermVectors*>(p)));
}

} //end of namespace xlifepp
