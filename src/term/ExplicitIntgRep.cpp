/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ExplicitIntgRep.cpp
  \authors N.Salles
  \since 25 March 2016
  \date 6 April 2016

  \brief code file for explicit formulas to compute integrals arising in integral representation.
 */

#include "ExplicitIntgRep.hpp"

namespace xlifepp {

// Formulas are available in ref?

//  integrand(Sm,Sp,h,d,Ip) is (( the true integrand) x d) for SL potentials
//  integrand(Sm,Sp,h,d,Ip) is ( (the true integrand) x d x h ) for SL potentials
//  Then instead of multiplying be the signed distances we just adapt the sign
//  after calling the integrand.

//-------------------------------------------------------------------------------
// Explicit primitives
//-------------------------------------------------------------------------------

// P0 case
//--------
  real_t integrandLapSLP0(const Point& Sm, const Point& Sp, real_t h,real_t d, const Point& Ip, real_t alpha=1.)
    {
      real_t tmp=0.;
      Point V=Sp-Sm;
      real_t nV=norm2(V);
      real_t sm=dot(Sm-Ip,V)/nV;
      real_t sp=dot(Sp-Ip,V)/nV;
      real_t h2=h*h;
      real_t d2=d*d;
      real_t sp2=sp*sp;
      real_t sm2=sm*sm;
      real_t R=std::sqrt(h2+d2+sp2);
      real_t Rm=std::sqrt(h2+d2);
      real_t mR=std::sqrt(h2+d2+sm2);

      if(d>theEpsilon) // otherwise, return 0
	{
	  tmp+=h*(std::atan(h*sp/(d*R))-std::atan(sp/d))+d*asinh(sp/Rm);
	  tmp-=h*(std::atan(h*sm/(d*mR))-std::atan(sm/d))+d*asinh(sm/Rm);
	}
      // no treatment needed when h=0 or s=0.

      return tmp*alpha;
    }


real_t integrandLapDLP0(const Point& Sm, const Point& Sp, real_t h,real_t d, const Point& Ip)
    {

      real_t tmp=0.;
      Point V=Sp-Sm;
      real_t nV=norm2(V);
      real_t sm=dot(Sm-Ip,V)/nV;
      real_t sp=dot(Sp-Ip,V)/nV;
      real_t h2=h*h;
      real_t d2=d*d;
      real_t sp2=sp*sp;
      real_t sm2=sm*sm;
      real_t R=std::sqrt(h2+d2+sp2);
      real_t mR=std::sqrt(h2+d2+sm2);

      // We multiply by h coming from <(x-xhat)| ny>. We normalize x-xhat and multiply here by h.
      // It simplifies the case h ~ 0
      // Same idea, we multiply by d and the normalize the signed distance.
      if(d>theEpsilon)
	{
	  tmp+=(std::atan(sp/d)-std::atan(h*sp/(d*R)));
	  tmp-=(std::atan(sm/d)-std::atan(h*sm/(d*mR)));
	}

      // for d~0, res = 0.

      return tmp;
    }

// P1 case
//--------
  real_t integrandLapSLP1const(const Point& Sm, const Point& Sp, real_t h,real_t d, const Point& Ip, real_t alpha=1.)
  {
    real_t tmp=0.;
    Point V=Sp-Sm;
    real_t nV=norm2(V);
    real_t sm=dot(Sm-Ip,V)/nV;
    real_t sp=dot(Sp-Ip,V)/nV;
    real_t h2=h*h;
    real_t d2=d*d;
    real_t sp2=sp*sp;
    real_t R=std::sqrt(h2+d2+sp2);
    real_t Rm=std::sqrt(h2+d2);

    if(d>theEpsilon) // when d~0, res = 0.
      {
	if(h>theEpsilon)
	  {
	    real_t ds=std::sqrt(d2+sp2);
	    tmp+=0.5/d*((d2-h2)*asinh(sp/Rm)+sp*h2/ds*asinh(ds/h));
	    tmp+=h*(std::atan(h*sp/(d*R)) - std::atan(sp/d));

	    real_t sm2=sm*sm;
	    ds=std::sqrt(d2+sm2);
	    R=std::sqrt(h2+d2+sm2);
	    tmp-=0.5/d*((d2-h2)*asinh(sm/Rm)+sm*h2/ds*asinh(ds/h));
	    tmp-=h*(std::atan(h*sm/(d*R)) - std::atan(sm/d));
	  }
	else // case h~0
	  {
	    tmp+=0.5*d*(asinh(sp/d)-asinh(sm/d));
	  }
      }
    return alpha*tmp;
  }

  void integrandLapSLP1lin(const Point & Sm, const Point & Sp, real_t h, real_t d, const Point& Ip, Vector<real_t> & res, real_t alpha=1.)
    {
      Point V=Sp-Sm;
      real_t nV=norm2(V);
      real_t sm=dot(Sm-Ip,V)/nV;
      real_t sp=dot(Sp-Ip,V)/nV;
      real_t h2=h*h;
      real_t d2=d*d;
      real_t sp2=sp*sp;
      real_t R=std::sqrt(h2+d2+sp2);
      real_t Rm=std::sqrt(h2+d2);
      res[0]=0.; res[1]=0.;

      if(d>theEpsilon) // d ~ 0 -> res = 0.
	{
	  if(h>theEpsilon)
	    {
	      real_t ds=std::sqrt(d2+sp2);
	      real_t tt=h2/ds*asinh(ds/h);
	      res[0]+=0.5/d*((d2+h2)*asinh(sp/Rm) - sp*tt);
	      res[1]+=0.5*d*(R+tt);

	      real_t sm2=sm*sm;
	      ds=std::sqrt(d2+sm2);
	      tt=h2/ds*asinh(ds/h);
	      R=std::sqrt(h2+d2+sm2);

	      res[0]-=0.5/d*((d2+h2)*asinh(sm/Rm) - sm*tt);
	      res[1]-=0.5*d*(R+tt);
	    }
	  else
	    {
	      res[0]+=0.5*d*asinh(sp/d);
	      res[1]+=0.5*d*R;
	      real_t sm2=sm*sm;
	      R=std::sqrt(h2+d2+sm2);
	      res[0]-=0.5*d*asinh(sm/d);
	      res[1]-=0.5*d*R;
	    }
	}
      res*=alpha;
    }

  real_t integrandLapDLP1const(const Point& Sm, const Point& Sp, real_t h,real_t d, const Point& Ip)
  {
    real_t tmp=0.;
    Point V=Sp-Sm;
    real_t nV=norm2(V);
    real_t sm=dot(Sm-Ip,V)/nV;
    real_t sp=dot(Sp-Ip,V)/nV;
    real_t h2=h*h;
    real_t d2=d*d;
    real_t sp2=sp*sp;
    real_t R=std::sqrt(h2+d2+sp2);
    real_t Rm=std::sqrt(h2+d2);

    // We multiply by h coming from <(x-xhat)| ny>. We normalize x-xhat and multiply here by h.
    if(d>theEpsilon)
      {
	if(h>theEpsilon)
	  {
	    real_t ds=std::sqrt(d2+sp2);
	    tmp+=(std::atan(sp/d)-std::atan(sp*h/(d*R)));
	    tmp+=h/d*(asinh(sp/Rm)-sp/ds*asinh(ds/h));
	    real_t sm2=sm*sm;
	    ds=std::sqrt(d2+sm2);
	    R=std::sqrt(h2+d2+sm2);
	    tmp-=(std::atan(sm/d)-std::atan(sm*h/(d*R)));
	    tmp-=h/d*(asinh(sm/Rm)-sm/ds*asinh(ds/h));
	  }
	else // case h ~ 0
	  {
	    tmp+=std::atan(sp/d)-std::atan(sm/d);
	  }
      }
    return tmp;
  }

  void integrandLapDLP1lin(const Point & Sm, const Point & Sp, real_t h, real_t d, const Point& Ip, Vector<real_t> & res)
  {
    Point V=Sp-Sm;
    real_t nV=norm2(V);
    real_t sm=dot(Sm-Ip,V)/nV;
    real_t sp=dot(Sp-Ip,V)/nV;
    real_t h2=h*h;
    real_t d2=d*d;
    real_t sp2=sp*sp;
    real_t Rm=std::sqrt(h2+d2);
    res[0]=0.; res[1]=0.;


    // We multiply by h coming from <(x-xhat)| ny>. We normalize x-xhat and multiply here by h.
    // Same thing with d coming from the signed distance.
    if(d>theEpsilon)
      {
	if(h>theEpsilon)
	  {
	    real_t hd=h*d;
	    real_t ds=std::sqrt(d2+sp2);
	    res[0]+=h/d*(-asinh(sp/Rm) + sp/ds*asinh(ds/h));
	    res[1]+=-hd/ds*asinh(ds/h);
	    real_t sm2=sm*sm;
	    ds=std::sqrt(d2+sm2);
	    res[0]-=h/d*(-asinh(sm/Rm) + sm/ds*asinh(ds/h));
	    res[1]-=-hd/ds*asinh(ds/h);
	  }
      } // When d ~ 0 or h ~ 0, res[0,1] = 0.
  }

//-------------------------------------------------------------------------------
// Computation functions
//-------------------------------------------------------------------------------
void LaplaceSLP1(const Element* elt, const Point &pos, Vector<real_t> & res)
{
  //res.resize(3);
  real_t h=0.;
  //std::cout << " starting computeLaplaceIntegralP1" << eol;
  const MeshElement* melt=elt->geomElt_p->meshElement();
  const Point & S1=melt->node(1);
  const Point & S2=melt->node(2);
  const Point & S3=melt->node(3);
  Vector<real_t> & normalT=melt->geomMapData_p->normalVector;
  Point normalTp(normalT);
  Vector<Point> I(4);
  computeGeometricalQuantities(S1,S2,S3, normalTp,pos,I,h,true);
  ShapeValues shv=elt->computeShapeValues(I[3],false,false,false);
  Vector<real_t> signedD(3,0.);

  std::vector< Vector<real_t> > & nu=melt->geomMapData_p->sideNV();

  // Numerotation sideNV
  signedD[0]=-((pos[0]-I[0][0])*(nu[1][0])+(pos[1]-I[0][1])*(nu[1][1])+(pos[2]-I[0][2])*(nu[1][2]));
  signedD[1]=-((pos[0]-I[1][0])*(nu[2][0])+(pos[1]-I[1][1])*(nu[2][1])+(pos[2]-I[1][2])*(nu[2][2]));
  signedD[2]=-((pos[0]-I[2][0])*(nu[0][0])+(pos[1]-I[2][1])*(nu[0][1])+(pos[2]-I[2][2])*(nu[0][2]));

  real_t common=0.;
  // Compute common term
  real_t d=std::abs(signedD[0]);
  if(d > theEpsilon)
    {
      common+=signe(signedD[0])*integrandLapSLP1const(S2,S3,h,d, I[0]);
    }

  d=std::abs(signedD[1]);
  if(d > theEpsilon)
    {
      common+=signe(signedD[1])*integrandLapSLP1const(S3,S1,h,d, I[1]);
    }

  d=std::abs(signedD[2]);

  if(d > theEpsilon)
    {
      common+=signe(signedD[2])*integrandLapSLP1const(S1,S2,h,d, I[2]);
    }

  // Now compute the linear part
  Vector<real_t> tmp(2,0.); // Use to compute some temporary integrals

  Vector<Point> V(3);
  V[0]=S3-S2;
  V[1]=S1-S3;
  V[2]=S2-S1;
  Vector<real_t> normA(3);
  normA[0]=norm2(V[0]);
  normA[1]=norm2(V[1]);
  normA[2]=norm2(V[2]);

  // res = vector with values corresponding to phi0, phi1, phi2.

  // Computation with basis function that does not vanish on [S2,S3]
  d=std::abs(signedD[0]);
  integrandLapSLP1lin(S2,S3,h,d,I[0],tmp);
  real_t sm=dot(S2-I[0],V[0])/(normA[0]); // abscissa of S-
  real_t sp=dot(S3-I[0],V[0])/(normA[0]); // abscissa of S-
  real_t eta=signe(sp-sm);
  res[1]+=signe(signedD[0])*((1.+eta*sm/normA[0])*tmp[0] - eta/normA[0]*tmp[1]);
  res[2]+=signe(signedD[0])*eta/normA[0]*(tmp[1] -sm*tmp[0]);

   // Computation with basis function that does not vanish on [S3,S1]
  tmp=0.;
  d=std::abs(signedD[1]);
  integrandLapSLP1lin(S3,S1,h,d,I[1],tmp);
  sm=dot(S3-I[1],V[1])/(normA[1]); // abscissa of S-
  sp=dot(S1-I[1],V[1])/(normA[1]); // abscissa of S+
  eta=signe(sp-sm);
  res[2]+=signe(signedD[1])*((1.+eta*sm/normA[1])*tmp[0] - eta/normA[1]*tmp[1]);
  res[0]+=signe(signedD[1])*eta/normA[1]*(tmp[1] -eta*sm*tmp[0]);

   // Computation with basis function that does not vanish on [S1,S2]
  tmp=0.;
  d=std::abs(signedD[2]);
  integrandLapSLP1lin(S1,S2,h,d,I[2],tmp);
  sm=dot(S1-I[2],V[2])/(normA[2]); // abscissa of S-
  sp=dot(S2-I[2],V[2])/(normA[2]); // abscissa of S-
  eta=signe(sp-sm);
  res[0]+=signe(signedD[2])*((1.+eta*sm/normA[2])*tmp[0] - eta/normA[2]*tmp[1]);
  res[1]+=signe(signedD[2])*eta/normA[2]*(tmp[1] -sm*tmp[0]);

  for(size_t j=0;j<3;j++)
    res[j]+=shv.w[j]*common;
}


void LaplaceDLP1(const Element* elt, const Point &pos, Vector<real_t> & res)
{
  real_t h=0.;
  const MeshElement* melt=elt->geomElt_p->meshElement();
  const Point & S1=melt->node(1);
  const Point & S2=melt->node(2);
  const Point & S3=melt->node(3);
  Vector<real_t> & normalT=melt->geomMapData_p->normalVector;
  Point normalTp(normalT);
  Vector<Point> I(4);
  computeGeometricalQuantities(S1,S2,S3, normalTp,pos,I,h,true);
  Point ptmp=pos-I[3];
  real_t coef=dot(ptmp,normalTp)/norm2(ptmp);
  //real_t coef=(ptmp[0]*normalT[0]+ptmp[1]*normalT[1]+ptmp[2]*normalT[2])/norm2(ptmp);
  ShapeValues shv=elt->computeShapeValues(I[3],false,false,false);
  Vector<real_t> signedD(3,0.);

  std::vector< Vector<real_t> > & nu=melt->geomMapData_p->sideNV();

  // Numerotation sideNV
  signedD[0]=-((pos[0]-I[0][0])*(nu[1][0])+(pos[1]-I[0][1])*(nu[1][1])+(pos[2]-I[0][2])*(nu[1][2]));
  signedD[1]=-((pos[0]-I[1][0])*(nu[2][0])+(pos[1]-I[1][1])*(nu[2][1])+(pos[2]-I[1][2])*(nu[2][2]));
  signedD[2]=-((pos[0]-I[2][0])*(nu[0][0])+(pos[1]-I[2][1])*(nu[0][1])+(pos[2]-I[2][2])*(nu[0][2]));

  real_t common=0.;
  // Compute common term
  real_t d=std::abs(signedD[0]);

  if(d > theEpsilon)
    {
      common+=signe(signedD[0])*integrandLapDLP1const(S2,S3,h,d, I[0]);
    }

  d=std::abs(signedD[1]);
  if(d > theEpsilon)
    {
      common+=signe(signedD[1])*integrandLapDLP1const(S3,S1,h,d, I[1]);
    }

  d=std::abs(signedD[2]);
  if(d > theEpsilon)
    {
      common+=signe(signedD[2])*integrandLapDLP1const(S1,S2,h,d, I[2]);
    }

  // Now compute the linear part
  Vector<real_t> tmp(2,0.); // Use to compute some temporary integrals

  Vector<Point> V(3);
  V[0]=S3-S2;
  V[1]=S1-S3;
  V[2]=S2-S1;
  Vector<real_t> normA(3);
  normA[0]=norm2(V[0]);
  normA[1]=norm2(V[1]);
  normA[2]=norm2(V[2]);

  // res = vector with values corresponding to phi0, phi1, phi2.

  // Computation with basis function that does not vanish on [S2,S3]
  d=std::abs(signedD[0]);
  integrandLapDLP1lin(S2,S3,h,d,I[0],tmp);
  real_t sm=dot(S2-I[0],V[0])/(normA[0]); // abscissa of S-
  real_t sp=dot(S3-I[0],V[0])/(normA[0]); // abscissa of S-
  real_t eta=signe(sp-sm);
  res[1]+=signe(signedD[0])*((1.+eta*sm/normA[0])*tmp[0] - eta/normA[0]*tmp[1]);
  res[2]+=signe(signedD[0])*eta/normA[0]*(tmp[1] -sm*tmp[0]);

   // Computation with basis function that does not vanish on [S3,S1]
  tmp*=0.;
  d=std::abs(signedD[1]);
  integrandLapDLP1lin(S3,S1,h,d,I[1],tmp);
  sm=dot(S3-I[1],V[1])/(normA[1]); // abscissa of S-
  sp=dot(S1-I[1],V[1])/(normA[1]); // abscissa of S+
  eta=signe(sp-sm);
  res[2]+=signe(signedD[1])*((1.+eta*sm/normA[1])*tmp[0] - eta/normA[1]*tmp[1]);
  res[0]+=signe(signedD[1])*eta/normA[1]*(tmp[1] -eta*sm*tmp[0]);

   // Computation with basis function that does not vanish on [S1,S2]
  tmp=0.;
  d=std::abs(signedD[2]);
  integrandLapDLP1lin(S1,S2,h,d,I[2],tmp);
  sm=dot(S1-I[2],V[2])/(normA[2]); // abscissa of S-
  sp=dot(S2-I[2],V[2])/(normA[2]); // abscissa of S-
  eta=signe(sp-sm);
  res[0]+=signe(signedD[2])*((1.+eta*sm/normA[2])*tmp[0] - eta/normA[2]*tmp[1]);
  res[1]+=signe(signedD[2])*eta/normA[2]*(tmp[1] -sm*tmp[0]);


  for(size_t j=0;j<3;j++)
    {
      res[j]+=shv.w[j]*common;
      res[j]*=coef;
    }
}


 real_t LaplaceSLP0(const Element* elt, const Point & pos)
{
  real_t res=0.;
  real_t h=0.,d=0.;
  const MeshElement* melt=elt->geomElt_p->meshElement();
  const Point S1=melt->vertex(1);
  const Point S2=melt->vertex(2);
  const Point S3=melt->vertex(3);
  Vector<real_t> & normalT=melt->geomMapData_p->normalVector;
  Vector<Point> I(3);
  computeGeometricalQuantities(S1,S2,S3, normalT,pos,I,h,false);

  Vector<real_t> signedD(3,0.);
  std::vector<Vector<real_t> > & nu=melt->geomMapData_p->sideNV();
  signedD[0]=-((pos[0]-I[0][0])*(nu[1][0])+(pos[1]-I[0][1])*(nu[1][1])+(pos[2]-I[0][2])*(nu[1][2]));
  signedD[1]=-((pos[0]-I[1][0])*(nu[2][0])+(pos[1]-I[1][1])*(nu[2][1])+(pos[2]-I[1][2])*(nu[2][2]));
  signedD[2]=-((pos[0]-I[2][0])*(nu[0][0])+(pos[1]-I[2][1])*(nu[0][1])+(pos[2]-I[2][2])*(nu[0][2]));

  d=std::abs(signedD[0]);

  if(d > theEpsilon)
    {
      res+=signe(signedD[0])*integrandLapSLP0(S2,S3,h,d, I[0]);
    }

  d=std::abs(signedD[1]);
  if(d > theEpsilon)
    {
      res+=signe(signedD[1])*integrandLapSLP0(S3,S1,h,d, I[1]);
    }

  d=std::abs(signedD[2]);
  if(d > theEpsilon)
    {
      res+=signe(signedD[2])*integrandLapSLP0(S1,S2,h,d, I[2]);
    }
  return res;
}


  real_t LaplaceDLP0(const Element* elt, const Point &pos)
{
  real_t res=0.;
  real_t h=0.,d=0.;
  const MeshElement* melt=elt->geomElt_p->meshElement();
  const Point S1=melt->vertex(1);
  const Point S2=melt->vertex(2);
  const Point S3=melt->vertex(3);
  Vector<real_t> & normalT=melt->geomMapData_p->normalVector;
  Point verif(normalT);
  Vector<Point> I(4);
  computeGeometricalQuantities(S1,S2,S3, normalT,pos,I,h,true);
  Point tmp=pos-I[3];
  real_t coef=(tmp[0]*normalT[0]+tmp[1]*normalT[1]+tmp[2]*normalT[2])/norm2(tmp);

  Vector<real_t> signedD(3,0.);
  std::vector<Vector<real_t> > & nu=melt->geomMapData_p->sideNV();
  signedD[0]=-((pos[0]-I[0][0])*(nu[1][0])+(pos[1]-I[0][1])*(nu[1][1])+(pos[2]-I[0][2])*(nu[1][2]));
  signedD[1]=-((pos[0]-I[1][0])*(nu[2][0])+(pos[1]-I[1][1])*(nu[2][1])+(pos[2]-I[1][2])*(nu[2][2]));
  signedD[2]=-((pos[0]-I[2][0])*(nu[0][0])+(pos[1]-I[2][1])*(nu[0][1])+(pos[2]-I[2][2])*(nu[0][2]));

  d=std::abs(signedD[0]);
    if(d > theEpsilon)
    {
      res+=signe(signedD[0])*integrandLapDLP0(S2,S3,h,d, I[0]);
    }

  d=std::abs(signedD[1]);
  if(d > theEpsilon)
    {
      res+=signe(signedD[1])*integrandLapDLP0(S3,S1,h,d, I[1]);
    }

  d=std::abs(signedD[2]);
  if(d > theEpsilon)
    {
      res+=signe(signedD[2])*integrandLapDLP0(S1,S2,h,d, I[2]);
    }
  res*=coef;
  return res;
}

//===============================================
// HELMHOLTZ INTEGRALS



//-------------------------------------------------------------------------------
// Other functions
//-------------------------------------------------------------------------------

  // Input:
  // - (S1,S2,S3): the vertices of a triangle
  // - normaT: the normal of the plane of the triangle
  // - X: the observation point
  // - P1: boolean to compute or not I[3]
  // Output:
  // - I: Vector of points that are the projection of X onto the support of the segments of the triangle and I[3]=projection of X onto the plane of the triangle
  // - h: distance from point X to the plane of the triangle
  void computeGeometricalQuantities(const Point & S1, const Point & S2, const Point & S3, const Point & normalT,const Point &X,Vector<Point> & I, real_t & h, bool I3)
  {
    Vector<real_t> normS(3,0.);
    Vector<Point> vecS(3);
    vecS[0]=S3-S2;
    vecS[1]=S1-S3;
    vecS[2]=S2-S1;
    normS[0]=norm2(vecS[0]);
    vecS[0]/=normS[0];
    normS[1]=norm2(vecS[1]);
    vecS[1]/=normS[1];
    normS[2]=norm2(vecS[2]);
    vecS[2]/=normS[2];
    //I.resize(4); done before calling the function
    I[0]=S2+dot(X-S2,vecS[0])*vecS[0];
    I[1]=S3+dot(X-S3,vecS[1])*vecS[1];
    I[2]=S1+dot(X-S1,vecS[2])*vecS[2];
    Point tmp=S1-X;
    if(I3==true)
      {
	h=dot(tmp,normalT);
	//h=(tmp[0]*normalT[0]+tmp[1]*normalT[1]+tmp[2]*normalT[2]);
	I[3]=X+h*normalT;
	h=std::abs(h);
      }
    else
      h=std::abs(tmp[0]*normalT[0]+tmp[1]*normalT[1]+tmp[2]*normalT[2]);
  }

  real_t signe(real_t x)
  {
    real_t sign=0.;
    if(x>0)sign=1;
    if(x<0)sign=-1;

    return sign;
  }

} // end of namespace xlifepp
