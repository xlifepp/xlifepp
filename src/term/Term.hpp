/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
   \file Term.hpp
   \authors D. Martin, E. Lunéville
   \since 03 apr 2012
   \date 13 jan 2014

  \brief Definition of the xlifepp::Term class

  Class xlifepp::Term is the base class for inherited classes xlifepp::TermVector and xlifepp::TermMatrix
  that handle numerical representations of linear and bilinear forms
*/


#ifndef TERM_HPP
#define TERM_HPP

#include "config.h"
#include "utils.h"
#include "essentialConditions.h"

namespace xlifepp
{

//various declarations
enum TermType {_termUndef,_termVector,_termMatrix,_sutermVector,_sutermMatrix};   //!< types of term
//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const TermType& tt);

//=================================================================================
/*!
   \class ComputingInfo
   class collecting all information related to state of
   computation of term
*/
//=================================================================================
class ComputingInfo
{
  public:
    bool isComputed;        //!< true if term is computed (assemblied)
    bool noAssembly;        //!< true if the term is never assembly (isComputed stay false)
    bool multithreading;    //!< true if multithreading has to be used
    bool useGpu;            //!< true if gpu capabilities is activated
    StorageType storageType;           //!< to specify the storage type (_cs,_skyline,_dense)
    AccessType storageAccess;          //!< to specify the storage access (_row,_col,_dual,_sym)
    ReductionMethod reductionMethod;   //!< reduction  method to deal with essential condition

    //! constructor
    ComputingInfo()
      : isComputed(false), noAssembly(false), multithreading(false), useGpu(false), storageType(_noStorage), storageAccess(_noAccess),
      reductionMethod(ReductionMethod(_noReduction)) {}
    ComputingInfo(const ComputingInfo& ci)
      : isComputed(ci.isComputed), noAssembly(ci.noAssembly), multithreading(ci.multithreading), useGpu(ci.useGpu),
      storageType(ci.storageType), storageAccess(ci.storageAccess), reductionMethod(ci.reductionMethod) {}
};

//=================================================================================
/*!
   \class Term
   abstract base class of TermVector and TermMatrix,
   that are the numerical representations of linear and bilinear forms on spaces.
*/
//=================================================================================
class Term
{
  protected:
    string_t name_;                      //!< term name
    ComputingInfo computingInfo_;        //!< computating information
    TermType termType_;                  //!< type of term

  protected:
    Term(const string_t& na="", ComputingInfo ci=ComputingInfo())  //! basic constructor (protected!)
      : name_(na), computingInfo_(ci) { if(trackingObjects) theTerms.push_back(this); }

    std::set<ParameterKey> getParamsKeys(); //! returns the list of authorized keys
    //! common buildParam routine to manage _compute, _name and _assemble keys
    void buildParam(const Parameter& p, bool& toCompute, bool& toAssemble);

  public:
    Parameters params;                   //!< a free zone to store additional data (magic zone)

    virtual ~Term();                     //!< virtual destructor (do nothing)
    const string_t& name() const         //! return the Term name (const)
    {return name_;}
    virtual void name(const string_t& nam) =0;
    string_t primaryName() const;        //!< name without trailing @
    TermType termType() const            //! child term type
    {return termType_;}
    TermType& termType()                 //! child term type
    {return termType_;}
    ComputingInfo computingInfo() const  //! returns computingInfo (const)
    {return computingInfo_;}
    ComputingInfo& computingInfo()       //! returns computingInfo (const)
    {return computingInfo_;}
    bool& computed() {return computingInfo_.isComputed;} //!< returns computation state (non const)
    bool computed() const {return computingInfo_.isComputed;} //!< returns computation state (const)
    virtual void compute()=0;            //!< compute the Term
    virtual void clear()=0;              //!< deallocate memory used by a Term
    virtual std::set<const Space*> unknownSpaces() const =0;    //!< list of involved unknown spaces
    virtual void print(std::ostream&) const =0;                 //!< print the Term
    virtual void print(PrintStream& os) const =0;
    friend std::ostream& operator <<(std::ostream&,const Term&);

    static std::vector<Term *> theTerms; //!< list of existing Term objects
    static void clearGlobalVector();     //!< delete all term objects
    static void printAllTerms(std::ostream&);  //!< print the list of terms in memory
    static void printAllTerms(PrintStream& os) {printAllTerms(os.currentStream());}
};

std::ostream& operator <<(std::ostream&,const Term&); //!< print operator

//to compute many terms at same time
//@{
//! user aliases to term(s) computation
void compute(Term& t);
void compute(Term& t1, Term& t2);
void compute(Term& t1, Term& t2, Term& t3);
void compute(Term& t1, Term& t2, Term& t3, Term& t4);
void compute(Term& t1, Term& t2, Term& t3, Term& t4, Term& t5);
//@}
//@{
//! user aliases to term(s) cleaning
void clear(Term& t1);
void clear(Term& t1, Term& t2);
void clear(Term& t1, Term& t2, Term& t3);
void clear(Term& t1, Term& t2, Term& t3, Term& t4);
void clear(Term& t1, Term& t2, Term& t3, Term& t4, Term& t5);
//@}

} //end of namespace xlifepp

#endif /* TERM_HPP */
