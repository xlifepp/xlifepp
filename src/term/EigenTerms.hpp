/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file EigenTerms.hpp
  \author Y. Lafranche
  \since 25 jul 2016
  \date 25 jul 2016

  \brief Definition of classes related to the computation of eigen elements

  Definition of the xlifepp::EigenElements and xlifepp::SvdElements classes
*/

#ifndef EIGEN_TERMS_HPP
#define EIGEN_TERMS_HPP

#include "TermVector.hpp"

namespace xlifepp {
class TermMatrix;

/*!
   \class EigenElements
   end user class containing eigenValues and eigenVectors computed by an eigen solver.
*/
class EigenElements {
    bool realEigV_;                //!< true if the eigenvalues and eigenvectors are real (real symmetric case)
public:
    std::vector<complex_t> values; //!< list of eigen values
    TermVectors vectors;           //!< list of eigen vectors, same size as values

    //! main constructor
    EigenElements(const TermMatrix* A_p, bool singleUnknown, bool isr,
                  const std::vector<std::pair<complex_t,VectorEntry*> >& vvp,
                  EigenSortKind esortk = _incr_module, string_t resname="");
    //! constructor from size
    EigenElements(number_t n = 0, string_t resname="")
    : realEigV_(false), values(n), vectors(n, resname) {}

    void resize(number_t n)           //!< resizes values and vectors
    { values.resize(n); vectors.resize(n); }

    number_t numberOfEigenValues() const //!< access to number of eigen values
    { return values.size(); }

    //! access to n-th eigen vector (n>=1)
    const TermVector& vector(number_t n) const {
      if (n<1 || n>values.size()) error("index_out_of_range", "n", 1,values.size());
      return vectors[n-1];
    }

     //! access to n-th eigen vector (n>=1)
    TermVector& vector(number_t n) {
      if (n<1 || n>values.size()) error("index_out_of_range", "n", 1,values.size());
      return vectors[n-1];
    }

     //! access to n-th eigen value (n>=1)
    complex_t value(number_t n) const {
      if (n<1 || n>values.size()) error("index_out_of_range", "n", 1,values.size());
      return values[n-1];
    }

    //! access to nth eigen element (n>=1)
    std::pair<complex_t, TermVector> element(number_t n) const {
      if (n<1 || n>values.size()) error("index_out_of_range", "n", 1,values.size());
      return std::make_pair(values[n-1], vectors[n-1]);
    }

    bool isReal() const { return realEigV_; }

     //!< move eigenvectors to real
    EigenElements& toReal()
      {vectors.toReal();return *this;}
    EigenElements& applyEssentialConditions(const EssentialConditions& ecs,
                                            const ReductionMethod& rm=ReductionMethod()) //!< apply essential conditions to eigen vectors
                 {vectors.applyEssentialConditions(ecs,rm); return *this;}
    EigenElements& applyBoundaryConditions(const EssentialConditions& ecs,
                                         const ReductionMethod& rm=ReductionMethod())//!< apply essential conditions to eigen vectors
                 {return applyEssentialConditions(ecs,rm);}
    EigenElements& applyEssentialConditions(const SetOfConstraints& sec,
                                            const ReductionMethod& rm=ReductionMethod()) //!< apply constraints to eigen vectors
                 {vectors.applyEssentialConditions(sec,rm); return *this;}


    void print(std::ostream&) const;   //!< output on stream
    void print(PrintStream& os) const {print(os.currentStream());}
};

/*
   External output functions
*/
//! print operator
std::ostream& operator<<(std::ostream&, const EigenElements&);
//! save eigenvalues into one file and eigenvectors into separate files
void saveToFile(const string_t& filename, const EigenElements& evs, const std::vector<Parameter>& ps);
inline void saveToFile(const string_t& filename, const EigenElements& evs)
{
  std::vector<Parameter> ps(0);
  saveToFile(filename, evs, ps);
}
inline void saveToFile(const string_t& filename, const EigenElements& evs, const Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  saveToFile(filename, evs, ps);
}
inline void saveToFile(const string_t& filename, const EigenElements& evs, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps={p1, p2};
  saveToFile(filename, evs, ps);
}
inline void saveToFile(const string_t& filename, const EigenElements& evs, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps={p1, p2, p3};
  saveToFile(filename, evs, ps);
}
inline void saveToFile(const string_t& filename, const EigenElements& evs, IOFormat iof)
{
  warning("deprecated", "saveToFile(String, EigenElements, IOFormat)", "saveToFile(String, EigenElements, _format=?)");
  saveToFile(filename, evs, _format=iof);
}

typedef std::vector<std::pair<complex_t, VectorEntry*> > VpacVEp;
class SvdElements {
  public:
    //! constructors
    SvdElements() {}
    SvdElements(const TermMatrix& A, const EigenElements& ee);
    SvdElements(const LargeMatrix<real_t>* mat_p, const std::vector<std::pair<complex_t, VectorEntry*> >& vvp, EigenSortKind esortk = _decr_realpart);
    template<class K_>
    void buildSvdElements(const LargeMatrix<K_>* mat_p, const std::vector<std::pair<complex_t, VectorEntry*> >& vvp, EigenSortKind esortk);

    // Access functions ------------------------------------------------

    //! returns number of singular values
    number_t numberOfSingularValues() const { return S_.size(); }

    //! returns singular values
    const std::vector<real_t>& svalues() const { return S_; }
    //! returns n-th singular value (n>=1)
    real_t svalue(number_t n) const {
      if (n<1 || n>S_.size()) error("index_out_of_range", "n", 1,S_.size());
      return S_[n-1];
    }

    //! returns left singular vectors
   const TermVectors& leftSTermVectors() const { return TermU_; }
   const std::vector<VectorEntry*>& leftSVectors() const { return U_; }

   //! returns n-th left singular vector (n>=1)
   TermVector leftSTermVector(number_t n) const {
     if (n<1 || n>TermU_.size()) error("index_out_of_range", "n", 1,TermU_.size());
     return TermU_[n-1];
   }
   const VectorEntry* leftSVector(number_t n) const {
     if (n<1 || n>U_.size()) error("index_out_of_range", "n", 1,U_.size());
     return U_[n-1];
   }

    //! returns right singular vectors
   const TermVectors& rightSTermVectors() const {return TermV_; }
   const std::vector<VectorEntry*>& rightSVectors() const { return V_; }

   //! returns n-th right singular vector (n>=1)
   TermVector rightSTermVector(number_t n) const {
     if (n<1 || n>TermV_.size()) error("index_out_of_range", "n", 1,TermV_.size());
     return TermV_[n-1];
   }
   const VectorEntry* rightSVector(number_t n) const {
     if (n<1 || n>V_.size()) error("index_out_of_range", "n", 1,V_.size());
     return V_[n-1];
   }

  private:
    std::vector<real_t> S_;           //!< list of singular values
    TermVectors TermU_, TermV_;       //!< list of left and right singular vectors
    std::vector<VectorEntry*> U_, V_; //!< list of left and right singular vectors
};

//! save singular values into one file and singular vectors into separate files
void saveToFile(const string_t& fname, const SvdElements& evs, const std::vector<Parameter>& ps);

inline void saveToFile(const string_t& filename, const SvdElements& evs)
{
  std::vector<Parameter> ps(0);
  saveToFile(filename, evs, ps);
}
inline void saveToFile(const string_t& filename, const SvdElements& evs, const Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  saveToFile(filename, evs, ps);
}
inline void saveToFile(const string_t& filename, const SvdElements& evs, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps={p1, p2};
  saveToFile(filename, evs, ps);
}
inline void saveToFile(const string_t& filename, const SvdElements& evs, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps={p1, p2, p3};
  saveToFile(filename, evs, ps);
}
inline void saveToFile(const string_t& filename, const SvdElements& evs, IOFormat iof)
{
  warning("deprecated", "saveToFile(String, SvdElements, IOFormat)", "saveToFile(String, SvdElements, _format=?)");
  saveToFile(filename, evs, _format=iof);
}

} //end of namespace xlifepp

#endif /* EIGEN_TERMS_HPP */
