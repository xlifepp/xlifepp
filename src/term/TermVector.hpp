/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TermVector.hpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 3 apr 2012
  \date 22 aug 2013

  \brief Definition of the xlifepp::TermVector class

  xlifepp::TermVector class handles the numerical representation of general linear forms (xlifepp::LinearForm)
  that is the vector l(wi), i=1,...n; with (wi) the basis functions.
  It inherits from xlifepp::Term class and it is an end user class.

  A xlifepp::LinearForm object (see LinearForm.hpp) is a multiple unknowns/spaces linear form
  that is a list of linear forms acting on a single (sub)space (xlifepp::SuLinearForm object)
  indexed by unknown . As a consquence, a TermVector object is a list of xlifepp::SuTermVector objects
  that are related to xlifepp::SuLinearForm objects

  \verbatim
          child
  Term  --------> TermVector
                    | LinearForm
                    | VectorEntry*
                    | map<Unknown *,SuTermVector>
                    | ...
  \endverbatim

  End user has to instanciate only xlifepp::TermVector object, xlifepp::SuTermVector is an internal object never managed by end user,
  even if its linear form is really a single unknown linear form.

  xlifepp::VectorEntry pointer of xlifepp::TermVector is in general null, may be not in case of multiple unknowns linear form.
  xlifepp::TermVector being seen as a block vector, its vector values are given by xlifepp::VectorEntry of each xlifepp::SuTermVector.
  But in some circonstances (direct solvers), VectorEntry pointer of TermVector may be allocated to store xlifepp::TermVector
  as a contiguous vector without block structure.
  Be care, both representations may exist at the same time but with two times memory consuming !!!

  Now scalar_entries_p plays this role and entries_p has been kept for compatibility (should be not used)

  Note: if all the unknowns involved in xlifepp::TermVector are not scalar and if it is required by special operation (solver),
         it is possible to go to a scalar representation by using scalar_entries_p and renumbering vector cdofs.
         Be cautious, this scalar representation is not updated by most operations
*/

#ifndef TERM_VECTOR_HPP
#define TERM_VECTOR_HPP

#include "Term.hpp"
#include "SuTermVector.hpp"
#include "config.h"
#include "form.h"
#include "computation/termUtils.hpp"
#include "ExplicitIntgRep.hpp"
#include <list>

namespace xlifepp {

//=================================================================================
/*!
   \class TermVector
   end user class handling numerical representation of any linear form (LinearForm).
*/
//=================================================================================

//useful aliases
typedef std::map<const Unknown*, SuTermVector*>::iterator it_mustv;
typedef std::map<const Unknown*, SuTermVector*>::const_iterator cit_mustv;

class TermVector : public Term {
  protected:
    LinearForm linForm_;                                 //!< linear form (may be not exist)
    std::map<const Unknown*, SuTermVector*> suTerms_;    //!< list of single unknown term vectors
    VectorEntry* entries_p;                              //!< pointer to values of TermVector (see explanations)
    // for scalar representation
    VectorEntry* scalar_entries_p;                       //!< values of TermVector in scalar representation, same as entries_p if scalar unknowns
    std::vector<DofComponent> cdofs_;                    //!< scalar numbering when scalar_entries_p is allocated

  public:
    //constructors/destructor
    TermVector(const string_t& na = ""); //!< default constructor

    //! effective constructor from bilinear form, essential conditions  and options
    void build(const LinearForm& bf, const EssentialConditions* ecs, const std::vector<Parameter>& ps);

    //TermVector(const LinearForm&, const string_t& na = "", bool noass = false);  //!< constructor from linear form
    //TermVector(const LinearForm&, const EssentialConditions&, const string_t& na = "", bool noass = false); //!< constructor from linear form and essential condition

    //constructors from linear form with options with no essential condition
    TermVector(const LinearForm& lf, const string_t& na);
    TermVector(const LinearForm& lf, const char* na) : TermVector(lf, string_t(na)) {}
    TermVector(const LinearForm& lf)
    {
      std::vector<Parameter> ps(0);
      build(lf, 0, ps);
    }
    TermVector(const LinearForm& lf, const Parameter& p1)
    {
      std::vector<Parameter> ps={p1};
      build(lf, 0, ps);
    }
    TermVector(const LinearForm& lf, const Parameter& p1, const Parameter& p2)
    {
      std::vector<Parameter> ps={p1, p2};
      build(lf, 0, ps);
    }
    TermVector(const LinearForm& lf, const Parameter& p1, const Parameter& p2, const Parameter& p3)
    {
      std::vector<Parameter> ps={p1, p2, p3};
      build(lf, 0, ps);
    }

    //constructors with options and one essential boundary condition (for future usage)
    TermVector(const LinearForm& lf, const EssentialConditions& ecs, const string_t& na);
    TermVector(const LinearForm& lf, const EssentialConditions& ecs, const char* na) : TermVector(lf, ecs, string_t(na)) {}
    TermVector(const LinearForm& lf, const EssentialConditions& ecs)
    {
      std::vector<Parameter> ps(0);
      build(lf, &ecs, ps);
    }
    TermVector(const LinearForm& lf, const EssentialConditions& ecs, const Parameter& p1)
    {
      std::vector<Parameter> ps={p1};
      build(lf, &ecs, ps);
    }
    TermVector(const LinearForm& lf, const EssentialConditions& ecs, const Parameter& p1, const Parameter& p2)
    {
      std::vector<Parameter> ps={p1, p2};
      build(lf, &ecs, ps);
    }
    TermVector(const LinearForm& lf, const EssentialConditions& ecs, const Parameter& p1, const Parameter& p2, const Parameter& p3)
    {
      std::vector<Parameter> ps={p1, p2, p3};
      build(lf, &ecs, ps);
    }

    // constructors of interpolated TermVector
    void buildParamFromFct(const Parameter& p, bool& nodalFct);
    template <typename T>
    void buildTFromFct(const Unknown& u, const GeomDomain & dom, const T& v, const std::vector<Parameter> & ps);
    //! interpolation value constructor (anything), see SutermVector specialisation
    template <typename T>
    TermVector(const Unknown& u, const GeomDomain& dom, const T& v)
    {
      std::vector<Parameter> ps(0);
      buildTFromFct(u, dom, v, ps);
    }
    template <typename T>
    TermVector(const Unknown& u, const GeomDomain& dom, const T& v, const Parameter& p1)
    {
      std::vector<Parameter> ps={p1};
      buildTFromFct(u, dom, v, ps);
    }
    template <typename T>
    TermVector(const Unknown& u, const GeomDomain& dom, const T& v, const Parameter& p1, const Parameter& p2)
    {
      std::vector<Parameter> ps={p1, p2};
      buildTFromFct(u, dom, v, ps);
    }
    template <typename T>
    TermVector(const Unknown& u, const GeomDomain& dom, const T& v, const string_t& na) : TermVector(u, dom, v, _name=na)
    {
      warning("deprecated", "TermVector(Unknown, Domain, T, String)", "TermVector(Unknown, Domain, T, _name=?)");
    }
    //! interpolation value constructor (anything), see SutermVector specialisation
    template <typename T>
    TermVector(const Unknown& u, const GeomDomain& dom, const T& v, const char* na) : TermVector(u, dom, v, string_t(na)) {}
    //! interpolation value constructor for interpolation involving first derivative dofs
    TermVector(const Unknown& u, const GeomDomain& dom, const Function& f, const Function& gf, const string_t& na="");
    //! interpolation value constructor for interpolation involving first derivative dofs
    TermVector(const Unknown& u, const GeomDomain& dom, const Function& f, const Function& gf, const char* na) : TermVector(u, dom, f, gf, string_t(na)) {}
    //! interpolation value constructor for interpolation involving second derivative dofs
    TermVector(const Unknown& u, const GeomDomain& dom, const Function& f, const Function& gf, const Function& g2f, const string_t& na="");
    //! interpolation value constructor for interpolation involving second derivative dofs
    TermVector(const Unknown& u, const GeomDomain& dom, const Function& f, const Function& gf, const Function& g2f, const char* na) : TermVector(u, dom, f, gf, g2f, string_t(na)) {}

    //copy like constructors
    TermVector(const TermVector& tv, const string_t& na = "");      //!< copy constructor
    template <typename T>
    TermVector(const TermVector& tv, const T&, const string_t& na); //!< copy constructor with constant value
    TermVector(const SuTermVector& sutv, const string_t& na="");    //!< constructor of a TermVector from a SuTermVector   (full copy)
    TermVector(SuTermVector* sutv, const string_t& na="");          //!< constructor of a TermVector from a SuTermVector * (pointer copy)
    TermVector& operator=(const TermVector& tv);                    //!< assign operator

    //Constructors from linear combination
    TermVector(const LcTerm<TermVector>& lctv);                     //!< constructor from a linear combination of TermVector's
    TermVector& operator=(const LcTerm<TermVector>& lctv);          //!< assign operator from a linear combination of TermVector's

    //constructor from explicit function of TermVector's: Ri=f(Ui,Vi) (component by component)
    //! f(TermVector), real, single unknown
    TermVector(const TermVector& tv, funSR1_t& f, const Parameter& p1);
    //! f(TermVector), real, single unknown
    TermVector(const TermVector& tv, funSR1_t& f) :TermVector(tv, f, _name="") {}
    //! f(TermVector), real, single unknown
    TermVector(const TermVector& tv, funSR1_t& f, const string_t& na) : TermVector(tv, f, _name=na)
    {
      warning("deprecated", "TermVector(TermVector, funSR1, String)", "TermVector(TermVector, funSR1, _name=?)");
    }
    //! f(TermVector), real, single unknown
    TermVector(const TermVector& tv, funSR1_t& f, const char* na) : TermVector(tv, f, _name=string_t(na))
    {
      warning("deprecated", "TermVector(TermVector, funSR1, String)", "TermVector(TermVector, funSR1, _name=?)");
    }

    //! f(TermVector, TermVector), real, single unknown
    TermVector(const TermVector& tv1, TermVector& tv2, funSR2_t& f, const Parameter& p1);
    //! f(TermVector, TermVector), real, single unknown
    TermVector(const TermVector& tv1, TermVector& tv2, funSR2_t& f) : TermVector(tv1, tv2, f, _name="") {}
    //! f(TermVector, TermVector), real, single unknown
    TermVector(const TermVector& tv1, TermVector& tv2, funSR2_t& f, const string_t& na) : TermVector(tv1, tv2, f, _name=na)
    {
      warning("deprecated", "TermVector(TermVector, TermVector, funSR2, String)", "TermVector(TermVector, TermVector, funSR2, _name=?)");
    }
    //! f(TermVector, TermVector), real, single unknown
    TermVector(const TermVector& tv1, TermVector& tv2, funSR2_t& f, const char* na) : TermVector(tv1, tv2, f, _name=string_t(na))
    {
      warning("deprecated", "TermVector(TermVector, TermVector, funSR2, String)", "TermVector(TermVector, TermVector, funSR2, _name=?)");
    }

    //! f(TermVector), complex, single unknown
    TermVector(const TermVector& tv, funSC1_t& f, const Parameter& p1);
    //! f(TermVector), complex, single unknown
    TermVector(const TermVector& tv, funSC1_t& f) : TermVector(tv, f, _name="") {}
    //! f(TermVector), complex, single unknown
    TermVector(const TermVector& tv, funSC1_t& f, const string_t& na) : TermVector(tv, f, _name=na)
    {
      warning("deprecated", "TermVector(TermVector, funSC1, String)", "TermVector(TermVector, funSC1, _name=?)");
    }
    //! f(TermVector), complex, single unknown
    TermVector(const TermVector& tv, funSC1_t& f, const char* na) : TermVector(tv, f, _name=string_t(na))
    {
      warning("deprecated", "TermVector(TermVector, funSC1, String)", "TermVector(TermVector, funSC1, _name=?)");
    }

    //! f(TermVector, TermVector), complex, single unknown
    TermVector(const TermVector& tv1, TermVector& tv2, funSC2_t& f, const Parameter& p1);
    //! f(TermVector, TermVector), complex, single unknown
    TermVector(const TermVector& tv1, TermVector& tv2, funSC2_t& f) : TermVector(tv1, tv2, f, _name="") {}
    //! f(TermVector, TermVector), complex, single unknown
    TermVector(const TermVector& tv1, TermVector& tv2, funSC2_t& f, const string_t& na) : TermVector(tv1, tv2, f, _name=na)
    {
      warning("deprecated", "TermVector(TermVector, TermVector, funSC2, String)", "TermVector(TermVector, TermVector, funSC2, _name=?)");
    }
    //! f(TermVector, TermVector), complex, single unknown
    TermVector(const TermVector& tv1, TermVector& tv2, funSC2_t& f, const char* na) : TermVector(tv1, tv2, f, _name=string_t(na))
    {
      warning("deprecated", "TermVector(TermVector, TermVector, funSC2, String)", "TermVector(TermVector, TermVector, funSC2, _name=?)");
    }

    //constructor from symbolic function of TermVector's
    //! sf(TermVector)
    TermVector(const TermVector& tv, const SymbolicFunction& sf, const Parameter& p1);
    //! sf(TermVector)
    TermVector(const TermVector& tv, const SymbolicFunction& sf) : TermVector(tv, sf, _name="") {}
    //! sf(TermVector)
    TermVector(const TermVector& tv, const SymbolicFunction& sf, const string_t& na) : TermVector(tv, sf, _name=na)
    {
      warning("deprecated", "TermVector(TermVector, SymbolicFunction, String)", "TermVector(TermVector, SymbolicFunction, _name=?)");
    }
    //! sf(TermVector)
    TermVector(const TermVector& tv, const SymbolicFunction& sf, const char* na) : TermVector(tv, sf, _name=string_t(na))
    {
      warning("deprecated", "TermVector(TermVector, SymbolicFunction, String)", "TermVector(TermVector, SymbolicFunction, _name=?)");
    }

    //! sf(TermVector, TermVector)
    TermVector(const TermVector& tv1, const TermVector& tv2, const SymbolicFunction& sf, const Parameter& p1);
    //! sf(TermVector, TermVector)
    TermVector(const TermVector& tv1, const TermVector& tv2, const SymbolicFunction& sf) : TermVector(tv1, tv2, sf, _name="") {}
    //! sf(TermVector, TermVector)
    TermVector(const TermVector& tv1, const TermVector& tv2, const SymbolicFunction& sf, const string_t& na) : TermVector(tv1, tv2, sf, _name=na)
    {
      warning("deprecated", "TermVector(TermVector, TermVector, SymbolicFunction, String)", "TermVector(TermVector, TermVector, SymbolicFunction, _name=?)");
    }
    //! sf(TermVector, TermVector)
    TermVector(const TermVector& tv1, const TermVector& tv2, const SymbolicFunction& sf, const char* na) : TermVector(tv1, tv2, sf, _name=string_t(na))
    {
      warning("deprecated", "TermVector(TermVector, TermVector, SymbolicFunction, String)", "TermVector(TermVector, TermVector, SymbolicFunction, _name=?)");
    }

    //! sf(TermVector, TermVector, TermVector)
    TermVector(const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const SymbolicFunction& sf, const Parameter& p1);
    //! sf(TermVector, TermVector, TermVector)
    TermVector(const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const SymbolicFunction& sf) : TermVector(tv1, tv2, tv3, sf, _name="") {}
    //! sf(TermVector, TermVector, TermVector)
    TermVector(const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const SymbolicFunction& sf, const string_t& na="") : TermVector(tv1, tv2, tv3, sf, _name=na)
    {
      warning("deprecated", "TermVector(TermVector, TermVector, TermVector, SymbolicFunction, String)", "TermVector(TermVector, TermVector, TermVector, SymbolicFunction, _name=?)");
    }
    //! sf(TermVector, TermVector, TermVector)
    TermVector(const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const SymbolicFunction& sf, const char* na) : TermVector(tv1, tv2, tv3, sf, _name=string_t(na))
    {
      warning("deprecated", "TermVector(TermVector, TermVector, TermVector, SymbolicFunction, String)", "TermVector(TermVector, TermVector, TermVector, SymbolicFunction, _name=?)");
    }

    //constructor of a vector TermVector from scalar TermVector's (single unknown, same space)
    //! concatenate 2 scalar single unknown TermVector's in a vector TermVector
    TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const Parameter& p1);
    //! concatenate 2 scalar single unknown TermVector's in a vector TermVector
    TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2) : TermVector(u, tv1, tv2, _name="") {}
    //! concatenate 2 scalar single unknown TermVector's in a vector TermVector
    TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const string_t& na) : TermVector(u, tv1, tv2, _name=na)
    {
      warning("deprecated", "TermVector(Unknown, TermVector, TermVector, String)", "TermVector(Unknown, TermVector, TermVector, _name=?)");
    }
    //! concatenate 2 scalar single unknown TermVector's in a vector TermVector
    TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const char* na="") : TermVector(u, tv1, tv2, _name=string_t(na))
    {
      warning("deprecated", "TermVector(Unknown, TermVector, TermVector, String)", "TermVector(Unknown, TermVector, TermVector, _name=?)");
    }

    //! concatenate 3 scalar single unknown TermVector's in a vector TermVector
    TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const Parameter& p1);
    //! concatenate 3 scalar single unknown TermVector's in a vector TermVector
    TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3) : TermVector(u, tv1, tv2, tv3, _name="") {}
    //! concatenate 3 scalar single unknown TermVector's in a vector TermVector
    TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const string_t& na) : TermVector(u, tv1, tv2, tv3, _name=na)
    {
      warning("deprecated", "TermVector(Unknown, TermVector, TermVector, TermVector, String)", "TermVector(Unknown, TermVector, TermVector, TermVector, _name=?)");
    }
    //! concatenate 3 scalar single unknown TermVector's in a vector TermVector
    TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const char* na) : TermVector(u, tv1, tv2, tv3, _name=string_t(na))
    {
      warning("deprecated", "TermVector(Unknown, TermVector, TermVector, TermVector, String)", "TermVector(Unknown, TermVector, TermVector, TermVector, _name=?)");
    }

    //! concatenate 4 scalar single unknown TermVector's in a vector TermVector
    TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const TermVector& tv4, const Parameter& p1);
    //! concatenate 4 scalar single unknown TermVector's in a vector TermVector
    TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const TermVector& tv4) : TermVector(u, tv1, tv2, tv3, tv4, _name="") {}
    //! concatenate 4 scalar single unknown TermVector's in a vector TermVector
    TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const TermVector& tv4, const string_t& na) : TermVector(u, tv1, tv2, tv3, tv4, _name=na)
    {
      warning("deprecated", "TermVector(Unknown, TermVector, TermVector, TermVector, TermVector, String)", "TermVector(Unknown, TermVector, TermVector, TermVector, TermVector, _name=?)");
    }
    //! concatenate 4 scalar single unknown TermVector's in a vector TermVector
    TermVector(const Unknown& u, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const TermVector& tv4, const char* na) : TermVector(u, tv1, tv2, tv3, tv4, _name=string_t(na))
    {
      warning("deprecated", "TermVector(Unknown, TermVector, TermVector, TermVector, TermVector, String)", "TermVector(Unknown, TermVector, TermVector, TermVector, TermVector, _name=?)");
    }

    //constructor of a single unknown TermVector from a SuTermVector/TermVector with different interpolation/mesh
    TermVector(const Unknown& u, const GeomDomain& dom, const SuTermVector& sut, const string_t& na="", bool useNearest=false,
               const Function* fmap=nullptr, bool errorOnOutDom =false, real_t tol = theLocateToleranceFactor);
    TermVector(const Unknown& u, const GeomDomain& dom, const TermVector& tv, const string_t& na, bool useNearest=false,
               const Function* fmap=nullptr, bool errorOnOutDom =false, real_t tol = theLocateToleranceFactor)
               : TermVector(u, dom, tv.subVector(), na, useNearest, fmap, errorOnOutDom, tol) {}

    virtual ~TermVector();                                      //!< destructor

    //construction tools
    void insert(const SuTermVector& sutv);                      //!< insert a SuTermVector with full copy
    void insert(SuTermVector* sutv);                            //!< insert a SuTermVector with pointer copy
    void insert(const Unknown* u, SuTermVector* sutv);          //!< insert a SuTermVector in SuTerms map
    void copy(const TermVector& tv, const string_t& na);        //!< copy a TermVector with renaming
    void clear();                                               //!< deallocate memory used by a TermVector

    //basic accessors
    string_t name() const { return Term::name(); }
    void name(const string_t& nam);                        //!< update the name of a TermVector and its SuTermVectors
    cit_mustv begin() const                                //! iterator to the first element of the SuTermVector map (const)
      {return suTerms_.begin();}
    it_mustv begin()                                       //! iterator to the first element of the SuTermVector map
      {return suTerms_.begin();}
    cit_mustv end() const                                  //! iterator to the first element of the SuTermVector map (const)
      {return suTerms_.end();}
    it_mustv end()                                         //! iterator to the first element of the SuTermVector map
      {return suTerms_.end();}
    SuTermVector* firstSut() const                         //! first SuTermVector as pointer, 0 if void TermVector (const)
      {if(suTerms_.size()>0) return suTerms_.begin()->second;
       else return nullptr;}
    SuTermVector* firstSut()                               //! first SuTermVector as pointer, 0 if void TermVector
      {if(suTerms_.size()>0) return suTerms_.begin()->second;
       else return nullptr;}
    VectorEntry*& entries()                                //! access to entries pointer (r/w)
      {return entries_p;}
    const VectorEntry* entries() const                     //! access to entries pointer (r)
      {return entries_p;}
    VectorEntry*& scalar_entries()                         //! access to entries pointer (r/w)
      {return scalar_entries_p;}
    const VectorEntry* scalar_entries() const              //! access to entries pointer (r)
      {return scalar_entries_p;}
    VectorEntry* actual_entries() const;                   //!< return the actual pointer to entries (priority to scalar entry)

    //user accessors and properties
    const LinearForm& linearForm() const                   //! read access to linear form
      {return linForm_;}
    number_t nbOfUnknowns() const                          //! number of unknowns
      {return suTerms_.size();}
    bool isVoid() const                                    //! true if no SuTermVector
      {return suTerms_.size()==0;}
    bool isSingleUnknown() const                           //! number of unknowns
      {return suTerms_.size()==1;}
    bool hasBlock(const Unknown& u) const;                 //!< true if TermVector has a block (u)
    const Unknown* unknown(number_t i=1) const;            //!< return ith unknown as pointer (i>=1)
    std::set<const Unknown*> unknowns() const;             //!< return set of Unknowns
    std::set<const Space*> unknownSpaces() const;          //!< list of involved unknown spaces
    ValueType valueType() const;                           //!< return value type (_real if all SuTermVector's are real else _complex)
    number_t size() const;                                 //!< size of Termvector counted in scalar
    number_t nbDofs() const;                               //!< size of Termvector counted in dofs
    number_t nbDofs(const Unknown& u) const;               //!< return number of u-dofs
    const std::vector<DofComponent>& cdofs() const         //! access to cdofs_r vector (r)
      {return cdofs_;}
    std::vector<DofComponent>& cdofs()                     //! access to cdofs_r vector (r/w)
      {return cdofs_;}
    const Dof& dof(const Unknown& u, number_t n) const;    //!< return n-th dof of unknown
    const Dof& dof(number_t n) const                       //!< return n-th dof of first unknown
      {return dof(*unknown(),n);}
    const Point& coords(const Unknown& u, number_t n) const//!< return n-th dof coords of unknown
    {return dof(u,n).coords();}
    const Point& coords(number_t n) const                  //!< return n-th dof coords of first unknown
    {return dof(n).coords();}
    void setUnknown(const Unknown& u);                     //!< set the unknown of single unknown TermVector

    //access to subvector by unknown index
    SuTermVector& subVector();                               //!< access to first unknown term vector
    const SuTermVector& subVector() const;                   //!< access to first unknown term vector (const)
    SuTermVector& subVector(const Unknown* u);               //!< access to single unknown term vector
    const SuTermVector& subVector(const Unknown* u) const;   //!< access to single unknown term vector (const)
    SuTermVector& subVector(const Unknown& u);               //!< access to single unknown term vector
    const SuTermVector& subVector(const Unknown& u) const;   //!< access to single unknown term vector (const)
    SuTermVector* subVector_p(const Unknown* u);             //!< access to single unknown term vector pointer
    const SuTermVector* subVector_p(const Unknown* u) const; //!< access to single unknown term vector pointer (const)
    TermVector operator()(const Unknown& u) const;           //!< access to single unknown term vector as a TermVector
    template<typename T>
    Vector<T>& asVector(Vector<T>& v) const;                 //!< reinterpret TermVector as a raw Vector
    Vector<real_t> asRealVector() const;                     //!< reinterpret TermVector as a real Vector
    Vector<complex_t> asComplexVector() const;               //!< reinterpret TermVector as a complex Vector

    //compute
    void compute();                                        //!< compute TermVector
    void compute(const LcTerm<TermVector>& lctv);          //!< compute TermVector from a linear combination
    void markAsComputed(bool =true);                       //!< TermVector and SutermVector's computed state = true or false
    void mergeBlocks();                                    //!< tool to merge blocks related to the components of a same unknown

    //algebraic functions
    TermVector& operator+=(const TermVector& tv);             //!< operation U+=V
    TermVector& operator-=(const TermVector& tv);             //!< operation U+=V
    TermVector& merge(const TermVector& tv);                  //!< merge a TermVector with preserving the current value when common dofs
    template<typename T>
     TermVector& operator*=(const T& t);                     //!< operation U*=t
    template<typename T>
     TermVector& operator/=(const T& t);                     //!< operation U*=t

    //modify representation
    void toScalar(bool keepEntries=false);                 //!< go to scalar representation of TermVector (done on SuTermVectors)
    void toVector(bool keepEntries=false);                 //!< return to vector representations from scalar representations (done on SuTermVectors)
    void toGlobal(bool keepSuTerms=false);                 //!< go to global scalar representation of TermVector
    void toLocal(bool keepGlobal=false);                   //!< go to local representation of TermVector
    void adjustScalarEntries(const std::vector<DofComponent>& newcdofs); //!< adjust global scalar entries to cdofs numbering given

    //modify unknown definition
    //! move SuTermVector unknown to an other
    void changeUnknown(const Unknown& u, const Unknown& v, const Vector<number_t>& ci = Vector<number_t>(0));
    //! move SuTermVector unknown to an other (single unknown term)
    void changeUnknown(const Unknown& v, const Vector<number_t>& ci = Vector<number_t>(0));
    void changeUnknown(const Unknown& u, number_t i) //! move SuTermVector unknown to an other (single unknown term)
    { changeUnknown(u,Vector<number_t>(1,i)); }
    void changeUnknown(const Unknown& u, number_t i, number_t j) //! move SuTermVector unknown to an other (single unknown term)
    { Vector<number_t> c(2,i); c(2)=j; changeUnknown(u,c); }
    void changeUnknown(const Unknown& u, number_t i, number_t j, number_t k) //! move SuTermVector unknown to an other (single unknown term)
    { Vector<number_t> c(3,i); c(2)=j; c(3)=k; changeUnknown(u,c); }

    //value management
    number_t dofRank(const Unknown& u, const Dof& dof) const;        //!< rank in u-SuTermVector of a given dof
    number_t dofRank(const Dof& dof) const;                          //!< rank in first SuTermVector of a given dof
    Value getValue(const Unknown& u, number_t n) const;              //!< access to vector component of unknown term (n>=1)
    Value getValue(const Unknown& u, const Dof& dof) const           //! access to vector component of unknown term using dof
    {return getValue(u, dofRank(u,dof));}
    void setValue(const Unknown& u, number_t n, const Value& val);   //!< set value to vector component of unknown term (n>=1)
    void setValue(const Unknown& u, const Dof& dof, const Value& v)  //! set value to vector component of unknown term using dof
    { setValue(u, dofRank(u,dof),v); }
    //! set real value to vector component of unknown term (n>=1)
    void setValue(const Unknown& u, number_t n, const real_t& r);
    //! set complex value to vector component of unknown term (n>=1)
    void setValue(const Unknown& u, number_t n, const complex_t& c);
    //! set real vector value to vector component of unknown term (n>=1)
    void setValue(const Unknown& u, number_t n, const std::vector<real_t>& vr);
    //! set complex vector value to vector component of unknown term (n>=1)
    void setValue(const Unknown& u, number_t n, const std::vector<complex_t>& vc);
    //! set values of TermVector from T object, restricted to an unknown and a domain
    template <typename T>
    void setValue(const Unknown& u, const GeomDomain& dom, const T& val);
    //! set values of TermVector from TermVector, restricted to an unknown and a domain
    void setValue(const Unknown& u, const GeomDomain& dom, const TermVector& tv);
    //! set values of TermVector from TermVector, restricted to an unknown
    void setValue(const Unknown& u, const TermVector& tv);

    // shortcut, take first unknown
    Value getValue(number_t n) const                            //! access to vector component of first unknown term (n>=1)
    { return getValue(*unknown(), n); }
    Value getValue(const Dof& dof) const                        //! access to vector component of first unknown term using dof
    { return getValue(dofRank(dof)); }
    void setValue(number_t n, const Value& v)                   //! set value to vector component of first unknown term (n>=1)
    { setValue(*unknown(), n, v); }
    void setValue(const Dof& dof, const Value& v)               //! set value to vector component of first unknown term using dof
    { setValue(dofRank(dof), v); }
    void setValue(number_t n, const real_t& r)                  //! set real value to vector component of first unknown term (n>=1)
    { setValue(*unknown(), n, r); }
    void setValue(number_t n, const complex_t& c)               //! set complex value to vector component of first unknown term (n>=1)
    { setValue(*unknown(), n, c); }
    void setValue(number_t n, const std::vector<real_t>& rv)    //! set real vector value to vector component of first unknown term (n>=1)
    {setValue(*unknown(), n, rv); }
    void setValue(number_t n, const std::vector<complex_t>& cv) //! set complex vector value to vector component of first unknown term (n>=1)
    {setValue(*unknown(), n, cv); }
    template <typename T>
    void setValue(const GeomDomain& d, const T& t)              //! set values of TermVector from T object, restricted to first unknown and a domain
    { setValue(*unknown(), d, t); }
    void setValue(const GeomDomain& d, const TermVector& t)     //! set values of TermVector from TermVector, restricted to first unknown and a domain
    { setValue(*unknown(), d, t); }
    void setValue(const TermVector& t)                          //! set values of TermVector from TermVector, restricted to first unknown
    { setValue(*unknown(), t); }

    //transformation
    TermVector& toAbs();     //!< change to abs(U)
    TermVector& toReal();    //!< change to real part of U
    TermVector& toImag();    //!< change to imaginary part of U
    TermVector& toComplex(); //!< change representation to complex
    TermVector& toConj();    //!< change to conj(U)
    TermVector& roundToZero(real_t aszero=std::sqrt(theEpsilon)); //!< round to zero all coefficients close to 0 (|.| < aszero)
    //! apply constraints to a TermVector (not end user)
    TermVector& applyEssentialConditions(const SetOfConstraints& soc, const ReductionMethod& rm=ReductionMethod());
    //! apply essential conditions to a TermVector
    TermVector& applyEssentialConditions(const EssentialConditions& ecs, const ReductionMethod& rm=ReductionMethod());
    //! apply essential conditions to a TermVector
    TermVector& applyBoundaryConditions(const EssentialConditions& ecs, const ReductionMethod& rm=ReductionMethod())
    { return applyEssentialConditions(ecs, rm); }

    const Function& toFunction() const; //!< return a function handling the TermVector (restrict to single unknown TermVector)
    TermVector mapTo(const GeomDomain& dom, const Unknown& u, bool useNearest=false, bool errOutDom=true, Function* fmap=nullptr, real_t tol=theLocateToleranceFactor) const;  //!< produce the mapped TermVector from current GeomDomains to a GeomDomain
    TermVector onDomain(const GeomDomain& dom) const;         //!< restrict TermVector to a GeomDomain
    EssentialCondition operator=(const complex_t& c);         //!< construct essential condition  tv=c

    //norms
    real_t norm2() const;                                     //!< quadratic norm
    complex_t maxValAbs() const; //!< return the value (in complex) of component being the largest one in absolute value

    //interpolation functions
    Element& locateElement(const Point& P, bool useNearest=false, bool errorOnOutDom=true) const;                  //!< locate element of the single unknown TermVector Space containing P
    Element& locateElement(const Unknown& u, const Point& P, bool useNearest=false, bool errorOnOutDom=true) const;  //!< locate element of the unknown SuTermVector Space containing P
    Element* locateElementP(const Point& P, bool useNearest=false, bool errorOnOutDom=false) const;                //!< locate element of the single unknown TermVector Space containing P (pointer)
    Element* locateElementP(const Unknown& u, const Point& P, bool useNearest=false, bool errorOnOutDom=false) const;//!< locate element of the unknown SuTermVector Space containing P (pointer)
    Value evaluate(const Unknown& u, const Point& P, bool useNearest=false, bool errorOnOutDom=true) const;  //!< evaluate unknown at a point (use interpolation method)
    Value evaluate(const Point& P, bool useNearest=false, bool errorOnOutDom=true) const;                  //!< evaluate single unknown at a point (use interpolation method)
    Value evaluate(const Unknown& u, const Point& P, const Element& elt) const;  //!< evaluate unknown at a point, element known (use interpolation method), faster
    Value evaluate(const Point& P, const Element& elt) const;                //!< evaluate single unknown at a point, element known (use interpolation method), faster
    template<typename T>
    T& operator()(const std::vector<real_t>& p, T& v) const;       //!< compute interpolated value of unknown at point (single unknown term)
    template<typename T>
    T& operator()(const Unknown& u, const std::vector<real_t>& p, T& v) const;  //!< compute interpolated value of unknown at point

    //io functions
    void print(std::ostream& out) const;                           //!< print TermVector
    void print(PrintStream& os) const {print(os.currentStream());}
    void saveToFile(const string_t& filename, number_t prec=fullPrec, bool encode=false) const; //!< save TermVector to file
    void saveToFile(const string_t& filename, bool encode) const //! save TermVector to file
    { saveToFile(filename, fullPrec, encode); }
    friend std::ostream& operator<<(std::ostream& out,const TermVector&);
};

/*!multiple algebraic operations on TermVector
 produce a LcTerm object (linear combination of terms)
 the computation is done by constructor from LcTerm or assign operation of a LcTerm
*/
//@{
//! addition of TermVector
const TermVector& operator+(const TermVector& tv);
LcTerm<TermVector> operator+(const TermVector& tv1, const TermVector& tv2);
LcTerm<TermVector> operator+(const LcTerm<TermVector>& lctv, const TermVector& tv);
LcTerm<TermVector> operator+(const TermVector& tv, const LcTerm<TermVector>& lctv);
//@}

//@{
//! substraction of TermVector
LcTerm<TermVector> operator-(const TermVector& tv);
LcTerm<TermVector> operator-(const TermVector& tv1, const TermVector& tv2);
LcTerm<TermVector> operator-(const LcTerm<TermVector>& lctv, const TermVector& tv);
LcTerm<TermVector> operator-(const TermVector& tv, const LcTerm<TermVector>& lctv);
//@}

//@{
//! product and division by a real or a complex (template T)
template<typename T>
LcTerm<TermVector> operator*(const TermVector& tv, const T& t) { return LcTerm<TermVector>(&tv,t); }
template<typename T>
LcTerm<TermVector> operator*(const T& t, const TermVector& tv) { return LcTerm<TermVector>(&tv,t); }
template<typename T>
LcTerm<TermVector> operator/(const TermVector& tv, const T& t) { return LcTerm<TermVector>(&tv,1./t); }
//@}

TermVector merge(const TermVector&, const TermVector&); //!< merge two termvectors, preserving values of first one when common dofs
TermVector add(const TermVector&, const TermVector&);  //!< create TermVector U+V
//! evaluate dofs on a function: dof_i(f) for any scalar dofs related to scalar unknown u and domain
TermVector interpolent(const Unknown&, const GeomDomain&, const Function& f,const Function& gradf=Function(), const Function& grad2f=Function());
TermVector mapTo(const TermVector& v, const GeomDomain& dom, const Unknown& u, bool useNearest=false,
                 bool errorOnOutDom=true, Function* fmap=nullptr, real_t tol=theLocateToleranceFactor);  // map a TermVector onto other (domain/unknown)

TermVector abs(const TermVector& tv);  //!< extracts modulus
TermVector complex(const TermVector& tv); //!< converts a real TermVector into a complex one
TermVector real(const TermVector& tv); //!< extracts real part
TermVector imag(const TermVector& tv); //!< extracts imag part
TermVector conj(const TermVector& tv); //!< conjugate TermVector

complex_t innerProduct(const TermVector& tv1, const TermVector& tv2);     //!< inner product
complex_t dotRC(const TermVector& tv1, const TermVector& tv2);            //!< inner product when one TermVector is complex
complex_t hermitianProduct(const TermVector& tv1, const TermVector& tv2); //!< hermitian product
complex_t operator|(const TermVector& tv1, const TermVector& tv2);        //!< main operator for inner or hermitian products

// norms of vector
real_t norm(const TermVector& tv, number_t l=2); //!< vector norm
real_t norm1(const TermVector& vt);              //!< l1 TermVector norm
real_t norm2(const TermVector& vt);              //!< l2 TermVector norm (quadratic norm)
real_t norminfty(const TermVector& vt);          //!< l_infinite TermVector norm (sup norm)

//! function used in solver
void addVectorThenAssign(TermVector& tv1, const TermVector& tv2);  //!< operation U+=t
template<typename T>
void multScalarThenAssign(TermVector& tv, const T& t); //!< operation U*=t

//@{
//! input/output function
void buildParamSaveToFile(const Parameter& p, IOFormat& iof, string_t& dataName, bool& aFilePerDomain);
void saveToFile(const string_t& filename, const std::list<const TermVector*>& tvs, const std::vector<Parameter>& ps);
inline void saveToFile(const string_t& filename, const std::list<const TermVector*>& tvs)
{
  std::vector<Parameter> ps(0);
  saveToFile(filename, tvs, ps);
}
inline void saveToFile(const string_t& filename, const std::list<const TermVector*>& tvs, const Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  saveToFile(filename, tvs, ps);
}
inline void saveToFile(const string_t& filename, const std::list<const TermVector*>& tvs, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps={p1, p2};
  saveToFile(filename, tvs, ps);
}
inline void saveToFile(const string_t& filename, const std::list<const TermVector*>& tvs, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps={p1, p2, p3};
  saveToFile(filename, tvs, ps);
}
void saveToFile(const string_t& filename, const TermVector& tv);
void saveToFile(const string_t& filename, const TermVector& tv, const Parameter& p1);
void saveToFile(const string_t& filename, const TermVector& tv, const Parameter& p1, const Parameter& p2);
void saveToFile(const string_t& filename, const TermVector& tv, const Parameter& p1, const Parameter& p2, const Parameter& p3);

void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2);
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const Parameter& p1);
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const Parameter& p1, const Parameter& p2);
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const Parameter& p1, const Parameter& p2, const Parameter& p3);

void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3);
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const Parameter& p1);
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const Parameter& p1, const Parameter& p2);
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const Parameter& p1, const Parameter& p2, const Parameter& p3);

void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const TermVector& tv4);
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const TermVector& tv4, const Parameter& p1);
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const TermVector& tv4, const Parameter& p1, const Parameter& p2);
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const TermVector& tv4, const Parameter& p1, const Parameter& p2, const Parameter& p3);

void saveToFile(const string_t& filename, const TermVector& tv, const string_t& dataName, IOFormat iof, bool aFilePerDomain=true);
inline void saveToFile(const string_t& filename, const TermVector& tv, IOFormat iof, bool aFilePerDomain=true)
{ saveToFile(filename, tv, string_t(), iof, aFilePerDomain);}
inline void saveToFile(const string_t& filename, const TermVector& tv, string_t dataName, bool aFilePerDomain=true)
{ saveToFile(filename, tv, dataName, _undefFormat, aFilePerDomain);}
inline void saveToFile(const string_t& filename, const TermVector& tv, bool aFilePerDomain)
{ saveToFile(filename, tv, string_t(), _undefFormat, aFilePerDomain);}

void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, IOFormat iof, bool aFilePerDomain=true);
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, string_t dataName, bool aFilePerDomain=true);
inline void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, bool aFilePerDomain)
{ saveToFile(filename, tv1, tv2, string_t(), aFilePerDomain); }
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, IOFormat iof, bool aFilePerDomain=true);
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, string_t dataName, bool aFilePerDomain=true);
inline void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, bool aFilePerDomain)
{ saveToFile(filename , tv1, tv2, tv3, string_t(), aFilePerDomain); }
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const TermVector& tv4, IOFormat iof, bool aFilePerDomain=true);
void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const TermVector& tv4, string_t dataName, bool aFilePerDomain=true);
inline void saveToFile(const string_t& filename, const TermVector& tv1, const TermVector& tv2, const TermVector& tv3, const TermVector& tv4, bool aFilePerDomain)
{ saveToFile(filename, tv1, tv2, tv3, tv4, string_t(), aFilePerDomain); }
//@}

//! plotting a TermVector
void plot(const TermVector& tv, IOFormat iof=_msh);

// domain rebuilding tools
std::vector<number_t> domainsFromLevelSet(TermVector& tv, number_t mainDomNum, std::vector<number_t> insideDomNums, bool outside=true);
inline std::vector<number_t> domainsFromLevelSet(TermVector& tv, number_t mainDomNum, number_t insideDomNum, bool outside=true)
{ return domainsFromLevelSet(tv, mainDomNum, std::vector<number_t>(1, insideDomNum), outside); }

void setColor(const GeomDomain& dom, const TermVector& tv, ColoringRule cr=defaultColoringRule); //!< set color of geom elements of domain according to vertex values and a coloring rule
void setColor(const GeomDomain& dom, const TermVector& tv, VectorColoringRule vcr=defaultVectorColoringRule); //!< set color of geom elements of domain according to vertex vector values and a vector coloring rule
void setColor(const GeomDomain& dom, real_t r); //!< set the color of geom elements
void rebuild(std::vector<GeomDomain*>& doms, const std::vector<ComparisonFunction<> >& crs, const std::set<GeomDomain*>& sidedoms=std::set<GeomDomain*>()); //!< rebuild some plain domains
void rebuild(GeomDomain& dom, const ComparisonFunction<>& cr);                   //!< rebuild 1 plain domain
void rebuild(GeomDomain& dom, const ComparisonFunction<>& cr, GeomDomain& sdom); //!< rebuild 1 plain domain and one side domain
//! rebuild 2 plain domains
void rebuild(GeomDomain& dom1, const ComparisonFunction<>& cr1, GeomDomain& dom2, const ComparisonFunction<>& cr2);
void rebuild(GeomDomain& dom1, const ComparisonFunction<>& cr1, GeomDomain& dom2, const ComparisonFunction<>& cr2, GeomDomain& sdom); //!< rebuild 2 plain domains and one side domain
void rebuild(GeomDomain& dom1, const ComparisonFunction<>& cr1, GeomDomain& dom2, const ComparisonFunction<>& cr2, GeomDomain& sdom1, GeomDomain& sdom2); //!< rebuild 2 plain domains and two side domains

//------------------------------------------------------------------------------------------
// template functions
//------------------------------------------------------------------------------------------

//TermVector constructor from Unknown and anything (no linear form), see SuTermVector specializations
//! create a single unknown termvector
// template<typename T>
// TermVector::TermVector(const Unknown& u, const GeomDomain& dom, const T& v, const string_t& na)
// {
//   computingInfo_.noAssembly = false;
//   computingInfo_.isComputed = true;
//   termType_ = _termVector;
//   name_ = na;
//   entries_p = nullptr;
//   scalar_entries_p = nullptr;
//   string_t nam = name_ + "_" + u.name();
//   suTerms_[&u] = new SuTermVector(u, dom, v, nam, false, false); //construct SuTermVector objects
// }
template<typename T>
void TermVector::buildTFromFct(const Unknown& u, const GeomDomain& dom, const T& v, const std::vector<Parameter>& ps)
{
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_name);
  params.insert(_pk_nodal);
  bool nodalFct=false;

  if (u.isLagrange()) { nodalFct=true; }

  for (number_t i=0; i< ps.size(); ++i)
  {
    ParameterKey key=ps[i].key();
    buildParamFromFct(ps[i], nodalFct);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);
  }

  termType_ = _termVector;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  string_t nam = name_ + "_" + u.name();
  suTerms_[&u] = new SuTermVector(u, dom, v, nam, nodalFct, false); //construct SuTermVector objects
}

//TermVector constructor from Unknown involving first derivative dof, see SuTermVector specializations
//  when scalar unknown, f returns a scalar, gf the vector ( dxf, [dyf, [dzf]] )
//  when vector unknown, f returns a vector, gf the vector ( dxf1, dxf2, dxf3, [dyf1, dyf2, dyf3, [dzf1,, dzf2, dzf3]] )
inline TermVector::TermVector(const Unknown& u, const GeomDomain& dom, const Function& f, const Function& gf, const string_t& na)
{
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
  termType_ = _termVector;
  name_ = na;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  string_t nam = name_ + "_" + u.name();
  suTerms_[&u] = new SuTermVector(u, dom, f, gf, nam); //construct SuTermVector objects
}

//TermVector constructor from Unknown involving first and second derivative, see SuTermVector specializations
//   when scalar unknown, f returns a scalar, gf the vector ( dxf, [dyf, [dzf]] ), g2f the vector ( dxxf, [dyyf, dxyf, [dzzf, dxz2f, dyzf]] )
//   when vector unknown, f returns a vector, gf the vector ( dxf1, dxf2, dxf3, [dyf1, dyf2, dyf3, [dzf1,, dzf2, dzf3]] ),
//                        g2f the vector ( dxxf1, [dyyf1, dxyf1, [dzzf1, dxz2f1, dyzf1]], dxxf2, [dyyf2, dxyf2, [dzzf2, dxz2f2, dyzf2]], ...)
inline TermVector::TermVector(const Unknown& u, const GeomDomain& dom, const Function& f, const Function& gf, const Function& g2f, const string_t& na)
{
  computingInfo_.noAssembly = false;
  computingInfo_.isComputed = true;
  termType_ = _termVector;
  name_ = na;
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  string_t nam = name_ + "_" + u.name();
  suTerms_[&u] = new SuTermVector(u, dom, f, gf, g2f, nam); //construct SuTermVector objects
}

//! copy constructor with values set to a constant value (full copy of entries if not null)
template<typename T>
TermVector::TermVector(const TermVector& tv, const T& v, const string_t& na)
{
  entries_p = nullptr;
  scalar_entries_p = nullptr;
  name_ = na;
  if(na == "") name_ = tv.name() + "-copy";
  if(tv.entries_p != nullptr) entries_p = new VectorEntry(tv.entries_p->size(), v);
  for(cit_mustv it = tv.suTerms_.begin(); it != tv.suTerms_.end(); it++) //copy SuTermVector's
  {
    SuTermVector* sut = new SuTermVector(*it->second, v);
    sut->name() = name_ + "_" + it->first->name();
    suTerms_[it->first] = sut;
  }
}

//! template algebraic operation on TermVectors
template<typename T>
TermVector& TermVector::operator*=(const T& t)
{
	multScalarThenAssign((*this),t);
  return *this;
}

template<typename T>
TermVector& TermVector::operator/=(const T& t)
{
  if(t == T(0)) error("divBy0");
  for(it_mustv it = suTerms_.begin(); it != suTerms_.end(); it++) *it->second /= t;
  return *this;
}

template<typename T>
void multScalarThenAssign(TermVector& p, const T& t)  //! p*=t
{
	 for(it_mustv it = p.begin(); it != p.end(); it++) *it->second *= t;
}

template<typename T>
void addScaledVector(TermVector& x, TermVector& v, const T& t) //! x+=t*v
{
  for(it_mustv it = x.begin(); it != x.end(); it++)
  {
    SuTermVector* sutv=v.subVector_p(it->first);
    if(sutv==nullptr) sutv=v.subVector_p(it->first->dual_p());
    if(sutv!=nullptr)  addScaledVector(*it->second, *sutv ,t);
  }
  for(it_mustv it = v.begin(); it != v.end(); it++)
  {
    SuTermVector* sutx=x.subVector_p(it->first);
    if(sutx==nullptr) sutx=x.subVector_p(it->first->dual_p());
    if(sutx==nullptr) //a block in v is not present in x, create a new SuTermVector in x
    {
      sutx=new SuTermVector(*it->second);
      *sutx*=t;
      x.insert(it->first,sutx);
    }
  }
}

// reinterpret TermVector as a raw Vector
// if multiple unknowns, SuTermVectors are concatenated
template<typename T>
Vector<T>& TermVector::asVector(Vector<T>& v) const
{
  v.clear();
  for(cit_mustv it = begin(); it != end(); it++)
  {
    Vector<T> w;
    it->second->asVector(w);
    v.insert(v.end(),w.begin(),w.end());
  }
  return v;
}

//interpolation functions
//! compute interpolated value of unknown at point (single unknown term)
template<typename T>
T& TermVector::operator()(const std::vector<real_t>& p, T& v) const
{
  v=T();
  if(!isSingleUnknown())
  {
    where("TermVector::operator()(const Point&, T&)");
    error("term_not_suterm");
  }
  if(size()==0)
  {
    where("TermVector::operator()(const Point&, T&)");
    error("is_void","TermVector");
  }
  const SuTermVector& sut=*begin()->second;
  return sut(p,v);
}

//compute interpolated value of unknown at point
template<typename T>
T& TermVector::operator()(const Unknown& u, const std::vector<real_t>& p, T& v) const
{
  return subVector(u)(p,v);
}

// set values of TermVector from T val (constant value, function, termvector), restricted to an unknown  u and a domain dom, say t(u)|dom = val|dom
template <typename T>
void TermVector::setValue(const Unknown& u, const GeomDomain& dom, const T& val)
{
    SuTermVector* sut=subVector_p(&u);
    if(sut==nullptr)  error("null_pointer", u.name());
    sut->setValue(val,dom);
}

// set values of TermVector from TermVector, restricted to an unknown and a domain, say t(u)|dom = tv(u)|dom
inline void TermVector::setValue(const Unknown& u, const GeomDomain& dom, const TermVector& tv)
{
    SuTermVector* sut=subVector_p(&u);
    if(sut==nullptr)  error("null_pointer", u.name());
    const SuTermVector* sutv=tv.subVector_p(&u);
    if(sutv==nullptr)  error("null_pointer", u.name());
    sut->setValue(*sutv,dom);
}

// set values of TermVector from TermVector, restricted to an unknown say t(u) = tv(u) only for dofs of tv
inline void TermVector::setValue(const Unknown& u, const TermVector& tv)
{
    SuTermVector* sut=subVector_p(&u);
    if(sut==nullptr)  error("null_pointer", u.name());
    const SuTermVector* sutv=tv.subVector_p(&u);
    if(sutv==nullptr)  error("null_pointer", u.name());
    sut->setValue(*sutv);
}

TermVector operator|(const TermVector&, const GeomDomain&); //!< restrict TermVector to a GeomDomain
TermVector interpolate(const Unknown&, const GeomDomain&,
                       const TermVector&, const string_t& na = "");     //!< interpolation of a TermVector on a domain, specifying unknown

//binary and unary operators applied to TermVector's, assuming they are single unknown and have the same size
inline TermVector operator*(const TermVector& s1, const TermVector& s2)
{return TermVector(s1,s2,x_1*x_2);}

inline TermVector operator/(const TermVector& s1, const TermVector& s2)
{return TermVector(s1,s2,x_1/x_2);}

inline TermVector operator^(const TermVector& s, const real_t& p)
{return TermVector(s,x_1^p);}

TermVector tensorInnerProduct(const TermVector& s1, const TermVector& s2);     //!< tensor inner product of single unknown vector TermVector => single unknown scalar TermVector (not conjugate)
TermVector tensorHermitianProduct(const TermVector& s1, const TermVector& s2); //!< tensor hermitian product of single unknown vector TermVector => single unknown scalar TermVector
TermVector tensorCrossProduct(const TermVector& s1, const TermVector& s2);     //!< tensor cross product of single unknown vector TermVector => single unknown vector/scalar TermVector (in 3D/2D)

//mathematical function applied to TermVector
inline TermVector sqrt(const TermVector& s)
{return TermVector(s,sqrt(x_1));}

inline TermVector squared(const TermVector& s)
{return TermVector(s,squared(x_1));}

inline TermVector sin(const TermVector& s)
{return TermVector(s,sin(x_1));}

inline TermVector cos(const TermVector& s)
{return TermVector(s,cos(x_1));}

inline TermVector tan(const TermVector& s)
{return TermVector(s,tan(x_1));}

inline TermVector asin(const TermVector& s)
{return TermVector(s,asin(x_1));}

inline TermVector acos(const TermVector& s)
{return TermVector(s,acos(x_1));}

inline TermVector atan(const TermVector& s)
{return TermVector(s,atan(x_1));}

inline TermVector sinh(const TermVector& s)
{return TermVector(s,sinh(x_1));}

inline TermVector cosh(const TermVector& s)
{return TermVector(s,cosh(x_1));}

inline TermVector tanh(const TermVector& s)
{return TermVector(s,tanh(x_1));}

inline TermVector asinh(const TermVector& s)
{return TermVector(s,asinh(x_1));}

inline TermVector acosh(const TermVector& s)
{return TermVector(s,acosh(x_1));}

inline TermVector atanh(const TermVector& s)
{return TermVector(s,atanh(x_1));}

inline TermVector exp(const TermVector& s)
{return TermVector(s,exp(x_1));}

inline TermVector log(const TermVector& s)
{return TermVector(s,log(x_1));}

inline TermVector log10(const TermVector& s)
{return TermVector(s,log10(x_1));}

inline TermVector pow(const TermVector& s, const real_t &p)
{return TermVector(s,x_1^p);}

void checkTermVectorInOperator(const TermVector& tv, const string_t& op="?");

//================================================================================
// left and right operations with (single unknown) TermVector and Unknown
//================================================================================
OperatorOnUnknown& operator*(const Unknown&, const TermVector&); //!< u*tv
OperatorOnUnknown& operator|(const Unknown&, const TermVector&); //!< u|tv
OperatorOnUnknown& operator^(const Unknown&, const TermVector&); //!< u^tv
OperatorOnUnknown& operator%(const Unknown&, const TermVector&); //!< u%tv
OperatorOnUnknown& operator*(const TermVector&, const Unknown&); //!< tv*u
OperatorOnUnknown& operator|(const TermVector&, const Unknown&); //!< tv|u
OperatorOnUnknown& operator^(const TermVector&, const Unknown&); //!< tv^u
OperatorOnUnknown& operator%(const TermVector&, const Unknown&); //!< tv%u
OperatorOnUnknown& operator*(const TestFunction&, const TermVector&); //!< u*tv
OperatorOnUnknown& operator|(const TestFunction&, const TermVector&); //!< u|tv
OperatorOnUnknown& operator^(const TestFunction&, const TermVector&); //!< u^tv
OperatorOnUnknown& operator%(const TestFunction&, const TermVector&); //!< u%tv
OperatorOnUnknown& operator*(const TermVector&, const TestFunction&); //!< tv*u
OperatorOnUnknown& operator|(const TermVector&, const TestFunction&); //!< tv|u
OperatorOnUnknown& operator^(const TermVector&, const TestFunction&); //!< tv^u
OperatorOnUnknown& operator%(const TermVector&, const TestFunction&); //!< tv%u


//=================================================================================
// left and right operations with (single unknown) TermVector and OperatorOnUnknown
//=================================================================================
OperatorOnUnknown& operator*(OperatorOnUnknown&, const TermVector&); //!< opu*tv
OperatorOnUnknown& operator|(OperatorOnUnknown&, const TermVector&); //!< opu|tv
OperatorOnUnknown& operator^(OperatorOnUnknown&, const TermVector&); //!< opu^v
OperatorOnUnknown& operator%(OperatorOnUnknown&, const TermVector&); //!< opu%tv
OperatorOnUnknown& operator*(const TermVector&, OperatorOnUnknown&); //!< tv*opu
OperatorOnUnknown& operator|(const TermVector&, OperatorOnUnknown&); //!< tv|opu
OperatorOnUnknown& operator^(const TermVector&, OperatorOnUnknown&); //!< tv^opu
OperatorOnUnknown& operator%(const TermVector&, OperatorOnUnknown&); //!< tv%opu

//=================================================================================
/*!
   \class TermVectors
   end user class handling vector of TermVector
   it inherits from std::vector
*/
//=================================================================================
class TermVectors : public std::vector<TermVector> {
  public:
    TermVectors(number_t n=0, string_t nam=string_t() )   //! basic constructor
    {
      if (n>0) resize(n);
      if (nam != string_t()) name(nam);
    }
    TermVectors(const std::vector<TermVector>& vs) //! constructor from vector<TermVector>
     : std::vector<TermVector>(vs) {}
    const TermVector& operator()(number_t n) const   //! acces to n-th TermVector (const)
    {
      if (n==0) error("bad_index_in_access_op");
      return at(n-1);
    }
    TermVector& operator()(number_t n)               //! acces to n-th TermVector
    {
      if (n==0) error("bad_index_in_access_op");
      return at(n-1);
    }
    void print(std::ostream&) const;   //!< output on stream
    void print(PrintStream& os) const {print(os.currentStream());}
    TermVectors& toReal();             //!< move all TermVector's to real
    TermVectors& toImag();             //!< move all TermVector's to real
    TermVectors& toVector(bool keepScalar=false);    //!< move all TermVector's to vector representation
    TermVectors& toLocal (bool keepGlobal=false);    //!< move all TermVector's to local representation
    TermVectors& applyEssentialConditions(const EssentialConditions&,
                                         const ReductionMethod& =ReductionMethod()); //!< apply essential conditions to a TermVectors
    TermVectors& applyBoundaryConditions(const EssentialConditions& ecs,
                                         const ReductionMethod& rm=ReductionMethod())//!< apply essential conditions to a TermVectors
                 {return applyEssentialConditions(ecs,rm);}
    TermVectors& applyEssentialConditions(const SetOfConstraints&,
                                          const ReductionMethod& =ReductionMethod()); //!< apply constraints to a TermVectors
    void name(const string_t& nam);    //!< update the name of each TermVector and theirs SuTermVectors
};
std::ostream& operator<<(std::ostream&, const TermVectors&);
//@{
//! save TermVectors to files
void saveToFile(const string_t& filename, const TermVectors& tvs, const std::vector<Parameter>& ps);
inline void saveToFile(const string_t& filename, const TermVectors& tvs)
{
  std::vector<Parameter> ps(0);
  saveToFile(filename, tvs, ps);
}
inline void saveToFile(const string_t& filename, const TermVectors& tvs, const Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  saveToFile(filename, tvs, ps);
}
inline void saveToFile(const string_t& filename, const TermVectors& tvs, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps={p1, p2};
  saveToFile(filename, tvs, ps);
}
inline void saveToFile(const string_t& filename, const TermVectors& tvs, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps={p1, p2, p3};
  saveToFile(filename, tvs, ps);
}
//@}
inline void saveToFile(const string_t& filename, const TermVectors& tvs, IOFormat iof, bool aFilePerDomain=false)
{
  warning("deprecated", "saveToFile(String, TermVectors, IOFormat, bool)", "saveToFile(String, TermVectors, _format=?, _aFilePerDomain/_aUniqueFile)");
  if (aFilePerDomain) saveToFile(filename, tvs, _format=iof, _aFilePerDomain);
  else saveToFile(filename, tvs, _format=iof, _aUniqueFile);
}
inline void saveToFile(const string_t& filename, const TermVectors& tvs, string_t dataName="", bool aFilePerDomain=false)
{
  warning("deprecated", "saveToFile(String, TermVectors, IOFormat, bool)", "saveToFile(String, TermVectors, _data_name=?, _aFilePerDomain/_aUniqueFile)");
  if (aFilePerDomain) saveToFile(filename, tvs, _data_name=dataName, _aFilePerDomain);
  else saveToFile(filename, tvs, _data_name=dataName, _aUniqueFile); 
}
inline void saveToFile(const string_t& filename, const TermVectors& tvs, bool aFilePerDomain)
{
  warning("deprecated", "saveToFile(String, TermVectors, bool)", "saveToFile(String, TermVectors, _aFilePerDomain/_aUniqueFile)");
  if (aFilePerDomain) saveToFile(filename, tvs, _aFilePerDomain);
  else saveToFile(filename, tvs, _aUniqueFile); 
}

} //end of namespace xlifepp

#endif /* TERM_VECTOR_HPP */
