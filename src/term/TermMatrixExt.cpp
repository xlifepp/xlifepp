/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TermMatrixExt.cpp
  \author E. Lunéville
  \since 03 apr 2012
  \date 20 mar 2013

  \brief Implementation of external functions related to xlifepp::TermMatrix class
*/

#include "TermMatrix.hpp"
#include "computation/otherComputation.hpp"
#include "solvers.h"
#include <cstdarg>

#ifdef XLIFEPP_WITH_MAGMA
  #include "magma.h"
#endif //XLIFEPP_WITH_MAGMA

namespace xlifepp
{

#ifdef XLIFEPP_WITH_ARPACK
  extern "C" {
    void dgesv_(int_t* n, int_t* nrhs,  real_t* a,  int_t*  lda,
                int_t* ipivot, real_t* b, int_t* ldb, int_t* info) ;
  }

  extern "C" {
    void zgesv_(int_t* n, int_t* nrhs,  complex_t* a,  int_t*  lda,
                int_t* ipivot, complex_t* b, int_t* ldb, int_t* info) ;
  }
#endif //XLIFEPP_WITH_ARPACK

// -----------------------------------------------------------------------------------------------------------
// linear combination of TermMatrix's and algebraic operations
// -----------------------------------------------------------------------------------------------------------
const TermMatrix& operator+(const TermMatrix& tm) { return tm; }

//LcTerm operator-(const TermMatrix& tm) { return LcTerm(tm, -1.); }
//LcTerm operator+(const TermMatrix& tm1, const TermMatrix& tm2) { return LcTerm(tm1, 1., tm2, 1.); }
//LcTerm operator-(const TermMatrix& tm1, const TermMatrix& tm2) { return LcTerm(tm1, 1., tm2, -1.); }
//LcTerm operator+(const LcTerm& lc, const TermMatrix& tm)
//{
//  LcTerm rlc(lc);
//  rlc.push_back(tm, 1.);
//  return rlc;
//}
//LcTerm operator-(const LcTerm& lc, const TermMatrix& tm)
//{
//  LcTerm rlc(lc);
//  rlc.push_back(tm, -1.);
//  return rlc;
//}
//LcTerm operator+(const TermMatrix& tm, const LcTerm<TermMatrix>& lc)
//{
//  LcTerm<TermMatrix> rlc(lc);
//  rlc.push_back(tm, 1.);
//  return rlc;
//}
//LcTerm<TermMatrix> operator-(const TermMatrix& tm, const LcTerm<TermMatrix>& lc)
//{
//  LcTerm<TermMatrix> rlc(lc);
//  rlc *= -1.;
//  rlc.push_back(tm, 1.);
//  return rlc;
//}
// product of LcTerm by a TermVector only if LcTerm is a matrix linear combination
//TermVector operator*(const LcTerm& lc, const TermVector& tv)
//{
//  trace_p->push("LcTerm * TermVector");
//  if(lc.clType!=_termMatrix) error("lcterm_term_only","TermMatrix");
//  TermMatrix tm(lc);
//  trace_p->pop();
//  return tm*tv;
//}

LcTerm<TermMatrix> operator-(const TermMatrix& tm) { return LcTerm<TermMatrix>(tm, -1.); }
LcTerm<TermMatrix> operator+(const TermMatrix& tm1, const TermMatrix& tm2) { return LcTerm<TermMatrix>(tm1, 1., tm2, 1.); }
LcTerm<TermMatrix> operator-(const TermMatrix& tm1, const TermMatrix& tm2) { return LcTerm<TermMatrix>(tm1, 1., tm2, -1.); }
LcTerm<TermMatrix> operator+(const LcTerm<TermMatrix>& lc, const TermMatrix& tm)
{
  LcTerm<TermMatrix> rlc(lc);
  rlc.push_back(tm, 1.);
  return rlc;
}
LcTerm<TermMatrix> operator-(const LcTerm<TermMatrix>& lc, const TermMatrix& tm)
{
  LcTerm<TermMatrix> rlc(lc);
  rlc.push_back(tm, -1.);
  return rlc;
}
LcTerm<TermMatrix> operator+(const TermMatrix& tm, const LcTerm<TermMatrix>& lc)
{
  LcTerm<TermMatrix> rlc(lc);
  rlc.push_back(tm, 1.);
  return rlc;
}
LcTerm<TermMatrix> operator-(const TermMatrix& tm, const LcTerm<TermMatrix>& lc)
{
  LcTerm<TermMatrix> rlc(lc);
  rlc *= -1.;
  rlc.push_back(tm, 1.);
  return rlc;
}
// product of LcTerm by a TermVector only if LcTerm is a matrix linear combination
TermVector operator*(const LcTerm<TermMatrix>& lc, const TermVector& tv)
{
  trace_p->push("LcTermMatrix * TermVector");
  //if(lc.clType!=_termMatrix) error("lcterm_term_only","TermMatrix");
  TermMatrix tm(lc);
  trace_p->pop();
  return tm*tv;
}

/*!
  product of a TermMatrix A and a TermVector X
  There are 2 cases:

  - TermMatrix has a global representation, say scalar_entries_p !=nullptr or entries_p!=nullptr
  in that case the product is realized as a standard matrix/vector product
  Note that TermVector X has to have a global representation consistent with column numbering of TermMatrix
      if it not the case, global representation of X is computed

  - TermMatrix has a local representation, say scalar_entries_p =0 and entries_p=0
  Assume that A has (v1,v2, ..., vm) has row unknowns and (u1,u2, ..., un) has column unknowns
  and X has (p1,p2, ..., pq) unknowns where pi may belongs to {u1,u2, ..., un} but it is not mandatory
  the result will be always a (v1,v2, ..., vm)-TermVector.
  Only product of matrix block (vi,uj) with vector block pk=uj will be performed
  For instance:
   the product of a (u,v)-Termatrix M with a p-TermVector X is a zero v-TermVector:
              |Mvu 0vp| * [0u Xp]t = [0v]
   the product of a [(u,p),(v,q)]-Termatrix M with a p-TermVector X is a (v,q)-TermVector:
              |Mvu Mvp| |0u|   |Mvp*Xp|
              |       | |  | = |      |
              |Mqu Mqp| |Xp|   |Mqp*Xp|
   Note that the zero blocks are never created; they are not indexed in the map TermVector::suTerms_ !
   The real products are made with SuTermMatrix and SuTermVector
*/

TermVector operator*(const TermMatrix& tM, const TermVector& tV)
{
  TermVector tR(tM.name() + "*" + tV.name());
  multMatrixVector(tM,tV,tR);
  return tR;
}

TermVector& multMatrixVector(const TermMatrix& tM, const TermVector& tV, TermVector& tR)
{
  trace_p->push("TermMatrix * TermVector");

  //reset TermVector tR
  if (&tV != &tR) tR.clear();
  tR.name()=tM.name() + "*" + tV.name();

  //compute in case of global representation
  if (tM.scalar_entries()!=nullptr)  //global scalar representation
    {
      ValueType vtM=tM.valueType(), vtV=tV.valueType(), vtR=vtM;
      if (vtR==_real && vtV==_complex) vtR=_complex;
      if (tR.scalar_entries()==nullptr)
        {
          tM.initTermVector(tR, vtR, false);
          tR.toGlobal(true);
        }
      const VectorEntry* vp= tV.scalar_entries();
      if (vp==nullptr || (vp!=nullptr && vp->size()!=tM.scalar_entries()->nbOfCols()))   //no scalar entries or wrong size
        {
          TermVector tVc(tV);  //copy of tV
          tVc.toGlobal(true);
          tVc.adjustScalarEntries(tM.cdofsc());
          multMatrixVector(*tM.scalar_entries(),*tVc.scalar_entries(),*tR.scalar_entries());
        }
      else multMatrixVector(*tM.scalar_entries(),*tV.scalar_entries(),*tR.scalar_entries());

      tR.toLocal(true);   //return to block representation
      tR.computed()=true;
      trace_p->pop();
      return tR;
    }

  if (tM.entries()!=nullptr)  //global matrix representation
    {
      error("structure_not_handled");
      tR.computed()=true;
      trace_p->pop();
      return tR;
    }

  //compute in case of block representation
  std::map<const Unknown*, std::list<SuTermVector*> > suterms;
  std::map<const Unknown*, std::list<SuTermVector*> >::iterator its;
  for (cit_mustm itM = tM.begin(); itM != tM.end(); itM++)
    {
      SuTermVector tVcopy;
      const Unknown* ui = itM->first.first, *vj = itM->first.second;
      const SuTermVector* sutV = tV.subVector_p(ui);
      if (sutV == nullptr && ui->dual_p()!=nullptr) sutV =  tV.subVector_p(ui->dual_p());
      if (sutV != nullptr)  //block found, compute the product and store the result in a LcTerm map
        {
          const SuTermMatrix* sutM = itM->second;
          SuTermVector* sutR = new SuTermVector(*sutM** sutV);
          sutR->name() = tR.name() + "_" + vj->name();
          its=suterms.find(vj);
          if (its==suterms.end()) suterms[vj]=std::list<SuTermVector*>(1,sutR);
          else its->second.push_back(sutR);
        }
      else if(ui->nbOfComponents()>1)  //try to deal with vector over component of unknown
        {
          for(dimen_t i=1; i<=ui->nbOfComponents(); i++)
            {
              const Unknown* uici=findUnknown(ui->name()+"_"+tostring(i));
              if (uici!=nullptr)
                {
                  sutV = tV.subVector_p(uici);
                  if (sutV ==nullptr) sutV = tV.subVector_p(uici->dual_p());           //try with dual (permissive)
                  if (sutV!=nullptr && sutV->nbOfComponents()<ui->nbOfComponents())
                    {
                      tVcopy=sutV->toVectorUnknown();  //go to block vector representation
                      sutV=&tVcopy;
                      const SuTermMatrix* sutM = itM->second;
                      SuTermVector* sutR = new SuTermVector(*sutM** sutV);
                      sutR->name() = tR.name() + "_" + vj->name();
                      its=suterms.find(vj);
                      if (its==suterms.end()) suterms[vj]=std::list<SuTermVector*>(1,sutR);
                      else its->second.push_back(sutR);
                    }
                }
            }
        }
    }

  //merge SuTermVectors
  for(its=suterms.begin(); its!=suterms.end(); its++)
    {
      const Unknown* vj=its->first;
      if (its->second.size()==1)
        tR.insert(vj, *its->second.begin());
      else //real merging
        {
          //LcTerm lc(_sutermVector);
          LcTerm<SuTermVector> lc("SuTermVector");
          std::list<SuTermVector*>::iterator itl=its->second.begin();
          for (; itl!=its->second.end(); itl++) lc.push_back(*itl,1.);
          SuTermVector* sutR = new SuTermVector(lc);
          sutR->name() = tR.name() + "_" + vj->name();
          tR.insert(vj, sutR);
          for (itl=its->second.begin(); itl!=its->second.end(); itl++) delete *itl;
        }
    }
  tR.computed()=true;
  trace_p->pop();

  return tR;
}

// Product TermVector * TermMatrix:

TermVector operator*(const TermVector& tV, const TermMatrix& tM)
{
  TermVector tR(tV.name() + "*" + tM.name());
  //multMatrixVector(tM,tV,tR);
  multVectorMatrix(tM,tV,tR);
  return tR;
}

TermVector& multVectorMatrix(const TermMatrix& tM, const TermVector& tV, TermVector& tR)
{
  return multVectorMatrix(tV, tM, tR);
}

TermVector& multVectorMatrix(const TermVector& tV, const TermMatrix& tM, TermVector& tR)
{
  trace_p->push("TermVector * TermMatrix");

  //reset TermVector tR
  if (&tV != &tR) tR.clear();
  tR.name()=tM.name() + "*" + tV.name();

  //compute in case of global representation
  if (tM.scalar_entries()!=nullptr)  //global scalar representation
    {
      ValueType vtM=tM.valueType(), vtV=tV.valueType(), vtR=vtM;
      if (vtR==_real && vtV==_complex) vtR=_complex;
      if (tR.scalar_entries()==nullptr)
        {
          tM.initTermVector(tR, vtR, false);
          tR.toGlobal(true);
        }
      const VectorEntry* vp= tV.scalar_entries();
      if (vp==nullptr || (vp!=nullptr && vp->size()!=tM.scalar_entries()->nbOfRows()))   //no scalar entries or wrong size
        {
          TermVector tVc(tV);  //copy of tV
          tVc.toGlobal(true);
          tVc.adjustScalarEntries(tM.cdofsr());
          multVectorMatrix(*tVc.scalar_entries(),*tM.scalar_entries(),*tR.scalar_entries());
        }
      else multVectorMatrix(*tV.scalar_entries(),*tM.scalar_entries(),*tR.scalar_entries());

      tR.toLocal(true);   //return to block representation
      tR.computed()=true;
      trace_p->pop();
      return tR;
    }

  if (tM.entries()!=nullptr)  //global matrix representation
    {
      error("structure_not_handled");
      tR.computed()=true;
      trace_p->pop();
      return tR;
    }

  //compute in case of block representation
  std::map<const Unknown*, std::list<SuTermVector*> > suterms;
  std::map<const Unknown*, std::list<SuTermVector*> >::iterator its;
  for (cit_mustm itM = tM.begin(); itM != tM.end(); itM++)
    {
      SuTermVector tVcopy;
      const Unknown* ui = itM->first.first, *vj = itM->first.second;
      const SuTermVector* sutV = tV.subVector_p(vj);
      if (sutV == nullptr && vj->dual_p()!=nullptr) sutV =  tV.subVector_p(vj->dual_p());
      if (sutV != nullptr)  //block found, compute the product and store the result in a LcTerm map
        {
          const SuTermMatrix* sutM = itM->second;
          SuTermVector* sutR = new SuTermVector(*sutV * *sutM);
          sutR->name() = ui->name() + "_" + tR.name();
          its=suterms.find(ui);
          if (its==suterms.end()) suterms[ui]=std::list<SuTermVector*>(1,sutR);
          else its->second.push_back(sutR);
        }
      else if (vj->nbOfComponents()>1)  //try to deal with vector over component of unknown
        {
          for (dimen_t i=1; i<=vj->nbOfComponents(); i++)
            {
              const Unknown* vjci=findUnknown(vj->name()+"_"+tostring(i));
              if (vjci!=nullptr)
                {
                  sutV = tV.subVector_p(vjci);
                  if (sutV ==nullptr) sutV = tV.subVector_p(vjci->dual_p());           //try with dual (permissive)
                  if (sutV!=nullptr && sutV->nbOfComponents()<vj->nbOfComponents())
                    {
                      tVcopy=sutV->toVectorUnknown();  //go to block vector representation
                      sutV=&tVcopy;
                      const SuTermMatrix* sutM = itM->second;
                      SuTermVector* sutR = new SuTermVector(*sutV * *sutM);
                      sutR->name() = ui->name() + "_" + tR.name();
                      its=suterms.find(ui);
                      if (its==suterms.end()) suterms[ui]=std::list<SuTermVector*>(1,sutR);
                      else its->second.push_back(sutR);
                    }
                }
            }
        }
    }

  //merge SuTermVectors
  for (its=suterms.begin(); its!=suterms.end(); its++)
    {
      const Unknown* ui=its->first;
      if (its->second.size()==1)
        tR.insert(ui, *its->second.begin());
      else //real merging
        {
          //LcTerm lc(_sutermVector);
          LcTerm<SuTermVector> lc("SuTermVector");
          std::list<SuTermVector*>::iterator itl=its->second.begin();
          for(; itl!=its->second.end(); itl++) lc.push_back(*itl,1.);
          SuTermVector* sutR = new SuTermVector(lc);
          sutR->name() = ui->name() + "_" + tR.name();
          tR.insert(ui, sutR);
          for(itl=its->second.begin(); itl!=its->second.end(); itl++) delete *itl;
        }
    }
  tR.computed()=true;
  trace_p->pop();

  return tR;


  // trace_p->push("TermMatrix * TermVector");

  // //reset TermVector tR
  // if(&tV != &tR) tR.clear();
  // tR.name()=tV.name() + "*" + tM.name();

  // //compute
  // for(cit_mustm itM = tM.begin(); itM != tM.end(); itM++)
  //   {
  //     SuTermVector tVcopy;
  //     const Unknown* ui = itM->first.first, *vj = itM->first.second;
  //     const SuTermVector* sutV = tV.subVector_p(vj);
  //     if(sutV == nullptr) sutV =  tV.subVector_p(vj->dual_p());
  //     //to deal with vector over component of unknown
  //     if(sutV == nullptr && vj->nbOfComponents()>1)
  //       {
  //         for(dimen_t i=1; i<=vj->nbOfComponents() && sutV==nullptr; i++)
  //           {
  //             const Unknown* vjci=findUnknown(vj->name()+"_"+tostring(i));
  //             if(vjci!=nullptr)
  //               {
  //                 sutV = tV.subVector_p(vjci);
  //                 if(sutV ==nullptr) sutV = tV.subVector_p(vjci->dual_p());
  //               }
  //           }
  //         if(sutV!=nullptr && sutV->nbOfComponents()<vj->nbOfComponents())
  //           {
  //             tVcopy=sutV->toVectorUnknown();  //go to block vector representation
  //             sutV=&tVcopy;
  //           }
  //       }
  //     if(sutV != nullptr)  //block found, compute the product
  //       {
  //         if(sutV->entries() ==nullptr) { sutV = tV.subVector_p(vj->dual_p());  }
  //         const SuTermMatrix* sutM = itM->second;
  //         SuTermVector* sutR = new SuTermVector(*sutV * *sutM);
  //         sutR->name() = tR.name() + "_" + ui->name();
  //         tR.insert(ui, sutR);
  //       }
  //   }
  // trace_p->pop();
  // return tR;
}

//product of TermMatrix
TermMatrix operator*(const TermMatrix& tA, const TermMatrix& tB)
{
  //identify SuTermMatrix blocks in A and B
  std::map<const Unknown*, std::vector<SuTermMatrix*> > colA, rowB;
  std::map<const Unknown*, std::vector<SuTermMatrix*> >::iterator itm, itmB;
  for(cit_mustm itA = tA.begin(); itA != tA.end(); itA++)  //travel the blocks of A
    {
      const Unknown* vp = itA->first.first; //unknowns of current block of A
      itm=colA.find(vp);
      if(itm==colA.end()) colA[vp]=std::vector<SuTermMatrix*>(1,itA->second);
      else itm->second.push_back(itA->second);
    }
  for(cit_mustm itB = tB.begin(); itB != tB.end(); itB++)  //travel the blocks oB
    {
      const Unknown* up = itB->first.second; //unknowns of current block of B
      itm=rowB.find(up);
      if(itm==rowB.end()) rowB[up]=std::vector<SuTermMatrix*>(1,itB->second);
      else itm->second.push_back(itB->second);
    }
  //product
  TermMatrix R(tA.name()+" x "+tB.name());
  std::map<uvPair, SuTermMatrix*>& suts = R.suTerms();
  for(itm=colA.begin(); itm!=colA.end(); itm++)  //travel the blocks of A
    {
      itmB=rowB.find(itm->first);
      if(itmB==rowB.end()) itmB=rowB.find(itm->first->dual_p());  //try with dual unknown (permissive)
      if(itmB!=rowB.end())   //block found
        {
          std::vector<SuTermMatrix*>::iterator ita,itb;
          for(ita=itm->second.begin(); ita!=itm->second.end(); ita++)
            {
              for(itb=itmB->second.begin(); itb!=itmB->second.end(); itb++)
                {
                  SuTermMatrix* sut=new SuTermMatrix(**ita** *itb); //product of submatrix
                  uvPair uv(sut->up(),sut->vp());
                  if(suts.find(uv)!= suts.end()) *suts[uv] += *sut;     //add to existing block
                  suts[uv]=sut;                                        //assign to termMatrix
                }
            }
        }
    }
  R.computed()=true;
  return R;
}

/*! check if A and B are defined on the same dofs,
    if not, align matrix to the largest set of dofs (union of A dofs and B dofs)
    use the sums A+0*diag(B) and B+0*diag(A) to produced matrices with same dofs
    if keepMatrix = true, create copy of A, B matrices on the memory stack
    if keepMatrix = false,  A, B matrices are modified if not the same dofs
    NOTE: adding diagonal 0 induces storage change and may increase significatively the storage size !
*/
void alignTermMatrix(TermMatrix*& A, TermMatrix*& B, bool keepMatrix)
{
    if(A==nullptr || B==nullptr) return;   //nothing to do
    trace_p->push("alignTermMatrix(A,B)");
    if(A->numberOfRows()==B->numberOfRows() && A->numberOfCols()==B->numberOfCols())
    {
         if(A->scalar_entries()!=nullptr) //check cdofs
         {
            std::vector<DofComponent>::iterator itA=A->cdofsr().begin(), itB=B->cdofsr().begin();
            while(itA!=A->cdofsr().end() && itB!=B->cdofsr().end() && itA==itB) {++itA;++itB;}
            bool sameRow= (itA==A->cdofsr().end() && itB==B->cdofsr().end());
            itA=A->cdofsc().begin(), itB=B->cdofsc().begin();
            while(itA!=A->cdofsc().end() && itB!=B->cdofsc().end() && itA==itB) {++itA;++itB;}
            if(sameRow && itA==A->cdofsc().end() && itB==B->cdofsc().end()) {trace_p->pop(); return;} //same row and col
         }
         else    //check  blocks
        {
            if(A->isSingleUnknown() && B->isSingleUnknown())
            {
                SuTermMatrix* sA=A->firstSut(), *sB=B->firstSut();
                if(sA->space_up()==sB->space_up() && sA->space_vp()==sB->space_vp()) {trace_p->pop();return;} //same row and col
            }
            else // multiple unknowns
            {
                // too complex, to do later or enforce the global representation because similar job done in toGlobal
                // summation process will do the job
            }
        }
    }
    // align A and B using sum of TermMatrix
    TermMatrix* At=A, *Bt=B;
    if(keepMatrix) {At=new TermMatrix(*A); Bt=new TermMatrix(*B);} // copy matrix to preserve original matrix
    *At+=TermMatrix(*B,_idMatrix,0.);
    *Bt+=TermMatrix(*A,_idMatrix,0.);
    A=At;B=Bt;
    trace_p->pop();
}


// -----------------------------------------------------------------------------------------------------------
// factorization tools
// -----------------------------------------------------------------------------------------------------------
/*! factorization of a TermMatrix A in Af
   when the TermMatrix is stored as a compressed sparse matrix, it is restored as a skyline matrix
   when the TermMatrix is a multiple unknown matrix, the matrix is rewritten in a "single" unknown matrix
   stored as a skyline matrix or a dense matrix if all blocks are dense
*/
// factorize symmetric matrix as LDLt
void ldltFactorize(TermMatrix& A, TermMatrix& Af) { factorize(A, Af, _ldlt); }

// factorize hermitian matrix as LDL*
void ldlstarFactorize(TermMatrix& A, TermMatrix& Af) { factorize(A, Af, _ldlstar); }

// factorize matrix as LU
void luFactorize(TermMatrix& A, TermMatrix& Af, bool withPermutation) { factorize(A, Af,_lu, withPermutation); }

// incomplete  factorize matrix as iLU
void iluFactorize(TermMatrix& A, TermMatrix& Af) { iFactorize(A, Af, _ilu); }
// incomplete  factorize matrix as iLDLt
void ildltFactorize(TermMatrix& A, TermMatrix& Af) { iFactorize(A, Af, _ildlt); }
// incomplete  factorize matrix as iLLt
void illtluFactorize(TermMatrix& A, TermMatrix& Af) { iFactorize(A, Af, _illt); }


// factorize matrix as LU
void umfpackFactorize(TermMatrix& A, TermMatrix& Af) { factorize(A, Af, _umfpack); }

/*! factorize matrix as LU or LDLt or LDL* along matrix property
    move to global scalar representation in any case

   if ft=_noFactorisation, type of factorization is searched
   if matrix is symmetric ldlt factorisation is used
   if matrix is self-adjoint ldlstar factorisation is used
   note that factorisation may be failed because there is no pivoting strategy (except for definite symmetric matrix!)
   if matrix is skew-adjoint ldlstar factorisation may be worked if the diagonal is non zero
   if matrix is skew-symmetric (diagonal is zero!) ldlt failed in any case !
   For the moment LU factorisation is used in case of a skew-adjoint or a skew-symmetric matrix
   in future, for specific algorithm for skew symmetric or skew adjoint matrices see following papers:
    Bunch J.R. Stable Algorithms for Solving Symmetric and Skew-Symmetric Systems, Bull. Austral. Math. Soc., vol 26, 107-119, 1982
    Lau T., Numerical Solution of Skew-Symmetric Linear Systems, Master Thesis, University of British Columbia, 2007

   If umfPack is available, it is used when ft is not specified
   if withPermution = true, LU with row permutation will be used to deal with dense matrices
*/
void factorize(TermMatrix& A, TermMatrix& Af, FactorizationType ft, bool withPermutation)
{
   if(!A.computed())
   {
     where("factorize(TermMatrix,TermMatrix,FactorizationType)");
     error("not_computed_term", A.name());
   }
   trace_p->push("factorize(TermMatrix, TermMatrix, ...");
   if(&Af != &A)
   {
     Af.clear();    //copy A to Af
     Af.copy(A);
   }
   if(A.factorization()!=_noFactorization)   // already factorized
   {
     trace_p->pop();
     return;
   }
  //go to scalar global representation in any case and get scalar matrix entry
  MatrixEntry* mat=nullptr;
  if(Af.isSingleUnknown())
    {
      SuTermMatrix* sutm = Af.begin()->second;
      if(sutm->space_up() != sutm->space_vp())
        { error("term_mismatch_spaces", sutm->space_up()->name(), sutm->space_vp()->name()); }
      if(sutm->strucType()==_matrix) sutm->toScalar();               //move to scalar representation
      if(sutm->scalar_entries()!=nullptr) mat = sutm->scalar_entries();    //if scalar entries pointer is allocated exists, use it !
      else mat=sutm->entries();
    }
  else  //multiple unknowns (goto scalar global representation)
    {
      SymType sy=Af.symmetry();  //detect global symmetry of TermMatrix
      if(Af.entries()==nullptr && Af.scalar_entries()==nullptr)  //move to global scalar representation
        {
          switch(ft)
            {
            case _lu:
              Af.toGlobal(_skyline, _dual, sy, true);
              break;
            case _llt:
            case _llstar:
              Af.toGlobal(_skyline, _sym, sy, true);
              break;
            default:
              Af.toGlobal(_noStorage, _noAccess, sy, true);
            }
        }
      else if(Af.scalar_entries()==nullptr) Af.toScalar(false);
      mat= Af.scalar_entries();
    }

  //do factorization on MatrixEntry
  factorize(*mat, ft, withPermutation);

  trace_p->pop();
}

//! factorize matrix as LU or LDLt or LDL*, not preserving A
void factorize(TermMatrix& A, FactorizationType ft, bool withPermutation) { factorize(A,A,ft,withPermutation); }

void iFactorize(TermMatrix& A, TermMatrix& Af, FactorizationType ft)
{
  if(!A.computed())
    {
      where("iFactorize(TermMatrix,TermMatrix,FactorizationType)");
      error("not_computed_term", A.name());
    }
  trace_p->push("iFactorize(TermMatrix, TermMatrix, ...");
  if(&Af != &A)
    {
      Af.clear();    //copy A to Af
      Af.copy(A);
    }
  //go to scalar global representation in any case and get scalar matrix entry
  MatrixEntry* mat=nullptr;
  if(Af.isSingleUnknown())
    {
      SuTermMatrix* sutm = Af.begin()->second;
      if(sutm->space_up() != sutm->space_vp())
        { error("term_mismatch_spaces", sutm->space_up()->name(), sutm->space_vp()->name()); }
      if(sutm->strucType()==_matrix) sutm->toScalar();               //move to scalar representation
      if(sutm->scalar_entries()!=nullptr) mat = sutm->scalar_entries();    //if scalar entries pointer is allocated exists, use it !
      else mat=sutm->entries();
    }
  else  //multiple unknowns (goto scalar global representation)
    {
      SymType sy=Af.symmetry();  //detect global symmetry of TermMatrix
      if(Af.entries()==nullptr && Af.scalar_entries()==nullptr)  //move to global scalar representation
        {
          switch(ft)
            {
            case _ilu:
              Af.toGlobal(_cs, _dual, sy, true);
              break;
            case _llt:
            case _llstar:
              Af.toGlobal(_skyline, _sym, sy, true);
              break;
            default:
              Af.toGlobal(_noStorage, _noAccess, sy, true);
            }
        }
      else if(Af.scalar_entries()==nullptr) Af.toScalar(false);
      mat= Af.scalar_entries();
    }
  //do factorization
  iFactorize(*mat,ft);

  trace_p->pop();
}

void iFactorize(TermMatrix& A, FactorizationType ft) { iFactorize(A,A,ft); }
/* -----------------------------------------------------------------------------------------------------------
  Prepare linear system:
   from TermMatrix A and right hand side B, construct entries (mat, b, x) to send to solvers
    - move to global scalar representation
    - apply to rhs the corrections induced by essential conditions
    - storage for global representation may be imposed by giving st, at arguments
    - if toScal is true, force scalar representation (default=false)

  note: to get the correct pointer to entries of X, use x=X.actualEntries() or
      x=X.scalar_entries();
      if (X.isSingleUnknown())
      {
       x= X.firstSut()->scalar_entries();
       if (x==nullptr) x=X.firstSut()->entries();
      }
 -----------------------------------------------------------------------------------------------------------*/

TermVector prepareLinearSystem(TermMatrix& A, TermVector& B, MatrixEntry*& mat, VectorEntry*& b, StorageType st,
                               AccessType at, bool toScal)
{

  trace_p->push("prepareLinearSystem(TermMatrix, TermVector, ...)");
  mat=nullptr;
  b=nullptr;

  //create TermVector X
  ValueType vtX = _real;
  if(A.valueType() == _complex || B.valueType() == _complex) vtX = _complex;
  if(vtX==_real && A.constraintsValueType()==_complex) vtX = _complex;
  TermVector X("X solves AX=B, A=" + A.name() + ", B=" + B.name());
  A.initTermVector(X, vtX, true);

  if(A.isSingleUnknown())   //SINGLE UNKNOWN
    {
      if(toScal)   //force to scalar representation
        {
          A.toScalar(true);
          B.toScalar(true);
        }
      if(st!=_noStorage && at!=_noAccess) A.setStorage(st,at);   //change matrix storage if required
      const Unknown* v = A.begin()->first.second, *u=A.begin()->first.first;
      SuTermMatrix* sutm = A.begin()->second;
      //check A, B unknown consistancy
      SuTermVector* vB = B.subVector_p(u);
      if(vB == nullptr)
        {
          vB=B.subVector_p(u->dual_p());
          if(vB==nullptr)
            {
              where("prepareLinearSystem(TermMatrix,TermVector,MatrixEntry*&,VectorEntry*&,bool)");
              error("term_inconsistent_unknowns");
            }
        }
      //prepare right hand side and solution
      SuTermVector* vX = X.subVector_p(u);
      if(vX == nullptr) vX = X.subVector_p(u->dual_p());
      if(sutm->scalar_entries()==nullptr)   //NON SCALAR SYSTEM
        {
          mat=sutm->entries();
          b=vB->entries();
          std::vector<number_t> renum = renumber(sutm->space_up(), vB->spacep());
          if(renum.size() != 0)
            {
              b->extendEntries(renum,sutm->space_up()->dimSpace());   //extend vector to matrix column numbering
              vB->spacep()=sutm->space_up(); //change space support
            }
        }
      else //ALLOCATED SCALAR ENTRIES
        {
          mat=sutm->scalar_entries();
          b=vB->scalar_entries();
          if(b==nullptr) {vB->toScalar(); b=vB->scalar_entries();}
          std::vector<number_t> renum;
          if(vB->up()== u)  renum = renumber(sutm->cdofsu(), vB->cdofs());
          else               renum = renumber(sutm->cdofsu(), dualDofComponents(vB->cdofs()));
          if(renum.size() != 0)
            {
              b->extendEntries(renum,sutm->cdofsu().size());       //extend vector to matrix column numbering
              if(vB->up()== u)  vB->cdofs()=sutm->cdofsu();         //update cdofs
              else vB->cdofs()=dualDofComponents(sutm->cdofsu());
            }
          vX->toScalar();
        }

      if(A.hasConstraints())   //deal with CONSTRAINTS if there are, force scalar representation
        {
          mat=sutm->scalar_entries();
          if(vX->scalar_entries()==nullptr) vX->toScalar();          //go to scalar representation
          if(vB->scalar_entries()==nullptr) vB->toScalar();          //go to scalar representation
          b=vB->scalar_entries();
          Constraints* cons_u = nullptr, *cons_v = nullptr;
          if(A.constraints_u_p !=nullptr) cons_u = (*A.constraints_u_p)(u);
          if(A.constraints_v_p !=nullptr)
            {
              cons_v = (*A.constraints_v_p)(v);
              if(cons_v == nullptr) cons_v = (*A.constraints_v_p)(v->dual_p());          //try with dual unknown
            }
          // correct rhs to take into account constraints
          appliedRhsCorrectorTo(b, vB->cdofs(), sutm->rhs_matrix(), cons_u, cons_v, A.computingInfo_.reductionMethod);
        }
    }
  else  //MULTIPLE UNKNOWNS
    {
      if(A.hasConstraints())  // case of constraints on A, applied rhs correction
        {
          Constraints* cons_u = nullptr, *cons_v = nullptr;
          if(A.constraints_u_p !=nullptr) cons_u = (*A.constraints_u_p)(0);
          if(A.constraints_v_p !=nullptr) cons_v = (*A.constraints_v_p)(0);
          if(cons_u!=nullptr || cons_v!=nullptr)   //global constraint
            {
              A.toGlobal(st, at, _noSymmetry, false);
              B.toGlobal(false);                            //go to scalar global representation
              B.adjustScalarEntries(A.cdofsc());            //adjust vector to matrix column scalar numbering
              appliedRhsCorrectorTo(B.scalar_entries(), B.cdofs(), A.rhs_matrix(),
                                    cons_u, cons_v, A.computingInfo_.reductionMethod);
            }
          else //local constraints, correction of subsystems
            {
              B.toScalar();
              for(it_mustm it = A.begin(); it != A.end(); it++)
                {

                  const Unknown* u=it->first.first, *v=it->first.second;
                  SuTermMatrix* sutm= it->second;
                  bool isdual=false;
                  SuTermVector* sutv= B.subVector_p(v);
                  if(sutv==nullptr) {sutv= B.subVector_p(v->dual_p()); isdual=true;}  //try with dual
                  if(A.constraints_u_p !=nullptr) cons_u = (*A.constraints_u_p)(u);
                  if(A.constraints_v_p !=nullptr)
                    {
                      cons_v = (*A.constraints_v_p)(v);
                      if(cons_v == nullptr) cons_v = (*A.constraints_v_p)(v->dual_p());   //try with dual unknown
                    }
                  if(cons_u!=nullptr && cons_v!=nullptr)
                    {
                      if(sutv==nullptr)  //no right hand side related to unknown v whereas constraints involves v, create a 0 SuTermVector
                        {
                          sutv = new SuTermVector("_ec_ext",v,it->second->space_vp(), 0.);
                          sutv->computed()=true;
                          sutv->toScalar();
                          B.insert(sutv);
                        }
                      std::vector<number_t> renum;
                      // if(!isdual) renum = renumber(sutm->cdofsu(), sutv->cdofs());
                      // else renum = renumber(sutm->cdofsu(), dualDofComponents(sutv->cdofs()));
                      if(isdual) renum = renumber(sutm->cdofsu(), sutv->cdofs());
                      else renum = renumber(sutm->cdofsu(), dualDofComponents(sutv->cdofs()));
                      if(renum.size() != 0)
                        {
                          sutv->scalar_entries()->extendEntries(renum,sutm->cdofsv().size()); //extend vector to matrix column numbering
                          sutv->cdofs()=sutm->cdofsv();                                       //update cdofs
                        }
                      if(!isdual) appliedRhsCorrectorTo(sutv->scalar_entries(), sutv->cdofs(), sutm->rhs_matrix(),
                                                          cons_u, cons_v, A.computingInfo_.reductionMethod);
                      else        appliedRhsCorrectorTo(sutv->scalar_entries(), dualDofComponents(sutv->cdofs()), sutm->rhs_matrix(),
                                                          cons_u, cons_v, A.computingInfo_.reductionMethod);
                    }
                }
              B.toGlobal();
              A.toGlobal(st, at, _noSymmetry, false);
              B.adjustScalarEntries(A.cdofsc());             //adjust vector to matrix column scalar numbering
            }
        }
      else //no constraints to apply
        {
          A.toGlobal(st, at, _noSymmetry, false);
          B.toGlobal();
          B.adjustScalarEntries(A.cdofsc());             //adjust vector to matrix column scalar numbering
        }

      X.toGlobal(false);
      X.adjustScalarEntries(A.cdofsr());
      b=B.scalar_entries();
      mat=A.scalar_entries();
    }
  trace_p->pop();
  return X;
}

//update rhs term vector by taking into account constraints
void updateRhs(TermMatrix& A, TermVector& B)
{

  if(!A.hasConstraints()) return;  // no constraints
  trace_p->push("updateRhs(TermMatrix, TermVector, ...)");

  if(A.isSingleUnknown())   //SINGLE UNKNOWN, work on SuTermMatrix
    {
      const Unknown* v = A.begin()->first.second, *u=A.begin()->first.first;
      SuTermMatrix* sutm = A.begin()->second;
      //check A, B unknown consistancy
      SuTermVector* vB = B.subVector_p(u);
      if(vB == nullptr) vB=B.subVector_p(u->dual_p());
      if(vB==nullptr) {where("updateRhs(TermMatrix,TermVector,...)"); error("term_inconsistent_unknowns");}
      //adjust right hand side
      if(sutm->scalar_entries()==nullptr)   vB->extendTo(*sutm->space_up());   //NON SCALAR SYSTEM
      else               vB->extendScalarTo(sutm->cdofsu(),vB->up()!= u);//allocated SCALAR SYSTEM

      //deal with CONSTRAINTS if there are, force scalar representation
      if(A.hasConstraints())
        {
          if(vB->scalar_entries()==nullptr) vB->toScalar();          //go to scalar representation
          VectorEntry* b=vB->scalar_entries();
          Constraints* cons_u = nullptr, *cons_v = nullptr;
          if(A.constraints_u_p !=nullptr) cons_u = (*A.constraints_u_p)(u);
          if(A.constraints_v_p !=nullptr)
            {
              cons_v = (*A.constraints_v_p)(v);
              if(cons_v == nullptr) cons_v = (*A.constraints_v_p)(v->dual_p());          //try with dual unknown
            }
          // correct rhs to take into account constraints
          appliedRhsCorrectorTo(b, vB->cdofs(), sutm->rhs_matrix(), cons_u, cons_v, A.computingInfo_.reductionMethod);
        }
    }

  else  //MULTIPLE UNKNOWNS
    {
      if(A.hasConstraints())  // case of constraints on A, applied rhs correction
        {
          Constraints* cons_u = nullptr, *cons_v = nullptr;
          if(A.constraints_u_p !=nullptr) cons_u = (*A.constraints_u_p)(0);
          if(A.constraints_v_p !=nullptr) cons_v = (*A.constraints_v_p)(0);
          if(cons_u!=nullptr || cons_v!=nullptr)   //global constraint
            {
              B.toGlobal(false);                            //go to scalar global representation
              B.adjustScalarEntries(A.cdofsc());            //adjust vector to matrix column scalar numbering
              appliedRhsCorrectorTo(B.scalar_entries(), B.cdofs(), A.rhs_matrix(),
                                    cons_u, cons_v, A.computingInfo_.reductionMethod);
            }
          else //local constraints, correction of subsystems
            {
              B.toScalar();
              for(it_mustm it = A.begin(); it != A.end(); it++)
                {

                  const Unknown* u=it->first.first, *v=it->first.second;
                  SuTermMatrix* sutm= it->second;
                  bool isdual=false;
                  SuTermVector* sutv= B.subVector_p(u);
                  if(sutv==nullptr) {sutv= B.subVector_p(u->dual_p()); isdual=true;}  //try with dual
                  if(sutv==nullptr) {where("updateRhs(TermMatrix,TermVector,...)"); error("term_inconsistent_unknowns");}
                  if(A.constraints_u_p !=nullptr) cons_u = (*A.constraints_u_p)(u);
                  if(A.constraints_v_p !=nullptr)
                    {
                      cons_v = (*A.constraints_v_p)(v);
                      if(cons_v == nullptr) cons_v = (*A.constraints_v_p)(v->dual_p());   //try with dual unknown
                    }
                  if(cons_u!=nullptr && cons_v!=nullptr && sutv!=nullptr)  sutv->extendScalarTo(sutm->cdofsu(),isdual);
                  if(!isdual) appliedRhsCorrectorTo(sutv->scalar_entries(), sutv->cdofs(), sutm->rhs_matrix(),
                                                      cons_u, cons_v, A.computingInfo_.reductionMethod);
                  else        appliedRhsCorrectorTo(sutv->scalar_entries(), dualDofComponents(sutv->cdofs()), sutm->rhs_matrix(),
                                                      cons_u, cons_v, A.computingInfo_.reductionMethod);
                }
            }
          B.toVector(true);  //update vector representation
        }
      else //no constraints to apply, align B to A columns
        {
          for(it_mustm it = A.begin(); it != A.end(); it++)
            {
              const Unknown* u=it->first.first;
              SuTermMatrix* sutm= it->second;
              bool isdual=false;
              SuTermVector* sutv= B.subVector_p(u);
              if(sutv==nullptr) {sutv= B.subVector_p(u->dual_p()); isdual=true;}  //try with dual
              if(sutv==nullptr) {where("updateRhs(TermMatrix,TermVector,...)"); error("term_inconsistent_unknowns");}
              sutv->extendTo(*sutm->space_up());
            }
        }
    }

  trace_p->pop();
}

//------------------------------------------------------------------------------------------------------------
//  Direct Solvers
//------------------------------------------------------------------------------------------------------------
//solve linear system when matrix is already factorized
TermVector factSolve(TermMatrix& A, const TermVector& B)
{
  trace_p->push("factSolve(TermMatrix, TermVector)");
  if(A.factorization() == _noFactorization) error("term_not_factorized", A.name());

  MatrixEntry* mat=nullptr;
  VectorEntry* b=nullptr;
  TermVector cB(B);    //copy B entries
  TermVector X=prepareLinearSystem(A,cB,mat,b);
  VectorEntry* x=X.actual_entries();

  switch(A.factorization())
    {
    case _ildlt:
    case _ldlt:
      mat->ldltSolve(*b, *x);
      break;
    case _ldlstar:
      mat->ldlstarSolve(*b, *x);
      break;
    case _ilu:
    case _lu:
      mat->luSolve(*b, *x);
      break;
    case _umfpack:
      mat->umfluSolve(*b,*x);
      break;
    default:
      error("wrong_factorization_type", words("factorization type",A.factorization()));
    }

  //finalization
  if(A.isSingleUnknown())
    {
      SuTermVector* vX = X.begin()->second;
      if(vX->up()->nbOfComponents()> 1)  vX->toVector();  //return to vector representation
      vX->computed() = true;
    }
  else  X.toLocal(false);

  X.computed() = true;
  trace_p->pop();
  return X;
}

/*! solve linear system with multiple right hand sides when matrix is already factorized
   B may be a TermVectors
*/
TermVectors factSolve(TermMatrix& A, const std::vector<TermVector>& Bs)
{
  trace_p->push("factSolve(TermMatrix, TermVectors)");
  if(A.factorization() == _noFactorization) error("term_not_factorized", A.name());

  number_t n=Bs.size();
  TermVectors Xs(n);
  for(number_t k=0; k<n; k++)
    {
      MatrixEntry* mat=nullptr;
      VectorEntry* b=nullptr;
      TermVector cB=Bs[k];      //copy rhs entries
      Xs[k]=prepareLinearSystem(A,cB,mat,b);
      VectorEntry* x=Xs[k].actual_entries();

      switch(A.factorization())  //solve system AXs[k]=Bs[k] <=> Ax=b
        {
        case _ldlt:
          mat->ldltSolve(*b, *x);
          break;
        case _ldlstar:
          mat->ldlstarSolve(*b, *x);
          break;
        case _lu:
          mat->luSolve(*b, *x);
          break;
        case _umfpack:
          mat->umfluSolve(*b, *x);
          break;
        default:
          error("wrong_factorization_type", words("factorization type",A.factorization()));
        }
      //finalization
      if(A.isSingleUnknown())
        {
          SuTermVector* vX = Xs[k].begin()->second;
          if(vX->up()->nbOfComponents()> 1)  vX->toVector();  //return to vector representation
          vX->computed() = true;
        }
      else  Xs[k].toLocal(false);
      Xs[k].computed() = true;
    }
  trace_p->pop();
  return Xs;
}

/*! solve linear system with TermMatrix as right hand side when matrix is already factorized
    return a TermMatrix in column dense storage
*/
TermMatrix factSolve(TermMatrix& A, TermMatrix& B)
{
  trace_p->push("factSolve(TermMatrix, TermMatrix)");
  if (A.factorization() == _noFactorization) error("term_not_factorized", A.name());

  //create TermMatrix result R
  /*   if A is a single unknown u x u TermMatrix and B is single unknown u x v TermMatrix,
          R=inv(A)B will be a single unknown u x v TermMatrix stored in its first SuTermMatrix
       if A is a multiple unknown (u1, u2,...) x (u1, u2,...) TermMatrix and B is a multiple unknown (u1, u2,...) x (v1, v2,...)
          R=inv(A)B will be a multiple unknown (u1, u2,...) x (v1, v2,...) TermMatrix stored global representation
       For the moment, the row unknows of B must be exactly the column unknowns of A

       as A has been factorized, it has a scalar representation, so B must have also a scalar representation
       if it is not the case it is enforced
  */
  //check the unknowns
  std::set<const Unknown*> un_A=A.colUnknowns(), un_B=B.rowUnknowns();
  std::set<const Unknown*>::iterator itu,itv;
  for (itu=un_A.begin(); itu!=un_A.end(); ++itu)
  {
    if (un_B.find(*itu)==un_B.end() && un_B.find((*itu)->dual_p())==un_B.end())
      error("term_inconsistent_unknowns");
  }
  for (itu=un_B.begin(); itu!=un_B.end(); ++itu)
  {
    if (un_A.find(*itu)==un_A.end() && un_A.find((*itu)->dual_p())==un_A.end())
      error("term_inconsistent_unknowns");
  }

  //enforced global scalar representation of B if multiple unknown or if single unknown vector unknown

  if (!B.isSingleUnknown()) B.toGlobal(_noStorage,_noAccess,_noSymmetry,true);
  else if (!B.isScalar()) B.toScalar();

  //create TermMatrix result (only structure)
  string_t name="inv("+A.name()+")*"+B.name();
  TermMatrix R(name);
  un_A=A.rowUnknowns(); un_B=B.colUnknowns();
  std::map<const Unknown*, Space*> spu, spv;
  for (cit_mustm it = A.suTerms().begin(); it != A.suTerms().end(); it++)  spv[it->first.second]=it->second->space_vp();
  for (cit_mustm it = B.suTerms().begin(); it != B.suTerms().end(); it++)  spu[it->first.first]=it->second->space_up();
  for (itv=un_A.begin(); itv!=un_A.end(); ++itv) //create void SuTermMatrix
    for (itu=un_B.begin(); itu!=un_B.end(); ++itu)
    {
      R.insert(new SuTermMatrix(*itu, spu[*itu], *itv, spv[*itv], 0, name+"_"+(*itu)->name()+"_"+(*itv)->name()));
    }

  //set value type
  ValueType vt=A.valueType();
  if (vt==_real) vt=B.valueType();

  if (A.isSingleUnknown() && B.isSingleUnknown())
  {
    //allocate SuTermMatrix Matrixentry
    number_t nbr=A.numberOfCols(), nbc=B.numberOfCols();
    SuTermMatrix* sutR=R.suTerms().begin()->second;
    MatrixEntry* matR = new MatrixEntry(vt,_scalar, new ColDenseStorage(nbr,nbc));
    SuTermMatrix* sutA = A.suTerms().begin()->second;
    if (sutA->scalar_entries()!=nullptr) sutR->scalar_entries() = matR;
    else sutR->entries() = matR;
    if(sutA->scalar_entries()==sutA->entries()) sutR->entries() = matR; //to enforce both scalar and non scalar representation

    //compute inverse
    for (number_t k=1; k<=nbc; k++)
    {
      MatrixEntry* mat=nullptr;
      VectorEntry* b=nullptr;
      TermVector Bk=B.column(k);
      TermVector Xk=prepareLinearSystem(A,Bk,mat,b);
      VectorEntry* x=Xk.actual_entries();
      switch (A.factorization())  //solve system A*Xk=Bk
      {
        case _ldlt:
          mat->ldltSolve(*b, *x);
          break;
        case _ldlstar:
          mat->ldlstarSolve(*b, *x);
          break;
        case _lu:
          mat->luSolve(*b, *x);
          break;
        case _umfpack:
          mat->umfluSolve(*b, *x);
          break;
        default:
          error("wrong_factorization_type", words("factorization type",A.factorization()));
      }
      // write in SuTermMatrix
      if (vt==_real)
      {
        std::vector<real_t>::iterator itm=matR->rEntries_p->values().begin()+(1+(k-1)*nbr),
                                      itv=x->rEntries_p->begin();
        for (; itv!=x->rEntries_p->end(); ++itv, ++itm) *itm=*itv;
      }
      else
      {
        std::vector<complex_t>::iterator itm=matR->cEntries_p->values().begin()+(1+(k-1)*nbr),
                                         itv=x->cEntries_p->begin();
        for (; itv!=x->cEntries_p->end(); ++itv, ++itm) *itm=*itv;
      }
    }
  }
  else
    error("term_not_suterm", A.name() + " || " + B.name());

  trace_p->pop();
  return R;
}

// solve linear system using direct method, TermMatrix rhs, i.e inv(A)*B
// if k=_keep, A is preserved
TermMatrix directSolve(TermMatrix& A, TermMatrix& B, KeepStatus k)
{
  if(A.factorization()==_noFactorization)
    {
      if(k==_keep)
        {
          TermMatrix Af;
          factorize(A,Af);
          return factSolve(Af,B);
        }
      factorize(A);
    }
  return factSolve(A,B);
}

// compute the inverse of A using factSolve(A,Id)
TermMatrix inverse(TermMatrix& A)
{
  TermMatrix Id = TermMatrix(A,_idMatrix);
  if(A.factorization()==_noFactorization)
    {
      TermMatrix Af;
      factorize(A,Af);
      return factSolve(Af,Id);
    }
  return factSolve(A,Id);
}

/*! solve left linear system X*A=b (~ At*X=B) when matrix is already factorized */
//  never solve the adjoint system, see factAdjointSolve function
//  when factorization is a symmetric one (ldlt, ildlt, ldlstar), not transposed solvers are used!
TermVector factLeftSolve(TermMatrix& A, const TermVector& B)
{
  trace_p->push("factLeftSolve(TermMatrix, TermVector)");
  if(A.factorization() == _noFactorization) error("term_not_factorized", A.name());
  MatrixEntry* mat=nullptr;
  VectorEntry* b=nullptr;
  TermVector cB(B);    //copy B entries
  TermVector X=prepareLinearSystem(A,cB,mat,b);
  VectorEntry* x=X.actual_entries();
  switch(A.factorization())
    {
    case _ildlt:
    case _ldlt:
      mat->ldltSolve(*b, *x);
      break;
    case _ldlstar:
      mat->ldlstarSolve(*b, *x);
      break;
    case _ilu:
    case _lu:
      mat->luLeftSolve(*b, *x);
      break;
    case _umfpack:
      mat->umfluLeftSolve(*b,*x);
      break;
    default:
      error("wrong_factorization_type", words("factorization type",A.factorization()));
    }
  //finalization
  if(A.isSingleUnknown())
    {
      SuTermVector* vX = X.begin()->second;
      if(vX->up()->nbOfComponents()> 1)  vX->toVector();  //return to vector representation
      vX->computed() = true;
    }
  else  X.toLocal(false);
  X.computed() = true;
  trace_p->pop();
  return X;
}
/*! solve left linear system Xs*A=b (~ At*Xs=B) with multiple right hand sides when matrix is already factorized
   B may be a TermVectors
*/
TermVectors factLeftSolve(TermMatrix& A, const std::vector<TermVector>& Bs)
{
  trace_p->push("factLeftSolve(TermMatrix, TermVectors)");
  if(A.factorization() == _noFactorization) error("term_not_factorized", A.name());

  number_t n=Bs.size();
  TermVectors Xs(n);
  for(number_t k=0; k<n; k++)
    {
      MatrixEntry* mat=nullptr;
      VectorEntry* b=nullptr;
      TermVector cB=Bs[k];      //copy rhs entries
      Xs[k]=prepareLinearSystem(A,cB,mat,b);
      VectorEntry* x=Xs[k].actual_entries();

      switch(A.factorization())  //solve system AXs[k]=Bs[k] <=> Ax=b
        {
        case _ldlt:
          mat->ldltSolve(*b, *x);
          break;
        case _ldlstar:
          mat->ldlstarSolve(*b, *x);
          break;
        case _lu:
          mat->luLeftSolve(*b, *x);
          break;
        case _umfpack:
          mat->umfluLeftSolve(*b, *x);
          break;
        default:
          error("wrong_factorization_type", words("factorization type",A.factorization()));
        }
      //finalization
      if(A.isSingleUnknown())
        {
          SuTermVector* vX = Xs[k].begin()->second;
          if(vX->up()->nbOfComponents()> 1)  vX->toVector();  //return to vector representation
          vX->computed() = true;
        }
      else  Xs[k].toLocal(false);
      Xs[k].computed() = true;
    }
  trace_p->pop();
  return Xs;
}

/*! solve left linear system with TermMatrix as right hand side when matrix is already factorized
    return a TermMatrix in column dense storage
*/
TermMatrix factLeftSolve(TermMatrix& A, TermMatrix& B)
{
  trace_p->push("factLeftSolve(TermMatrix, TermMatrix)");
  if (A.factorization() == _noFactorization) error("term_not_factorized", A.name());

  //create TermMatrix result R
  /*   if A is a single unknown u x u TermMatrix and B is single unknown u x v TermMatrix,
          R=inv(At)B will be a single unknown u x v TermMatrix stored in its first SuTermMatrix
       if A is a multiple unknown (u1, u2,...) x (u1, u2,...) TermMatrix and B is a multiple unknown (u1, u2,...) x (v1, v2,...)
          R=inv(At)B will be a multiple unknown (u1, u2,...) x (v1, v2,...) TermMatrix stored global representation
       For the moment, the row unknows of B must be exactly the column unknowns of At

       as A has been factorized, it has a scalar representation, so B must have also a scalar representation
       if it is not the case it is enforced
  */
  //check the unknowns
  std::set<const Unknown*> un_A=A.rowUnknowns(), un_B=B.rowUnknowns();
  std::set<const Unknown*>::iterator itu,itv;
  for (itu=un_A.begin(); itu!=un_A.end(); ++itu)
  {
    if (un_B.find(*itu)==un_B.end() && un_B.find((*itu)->dual_p())==un_B.end())
      error("term_inconsistent_unknowns");
  }
  for (itu=un_B.begin(); itu!=un_B.end(); ++itu)
  {
    if (un_A.find(*itu)==un_A.end() && un_A.find((*itu)->dual_p())==un_A.end())
      error("term_inconsistent_unknowns");
  }
  //enforced global scalar representation of B if multiple unknown or if single unknown vector unknown
  if (!B.isSingleUnknown()) B.toGlobal(_noStorage,_noAccess,_noSymmetry,true);
  else if (!B.isScalar()) B.toScalar();
  //create TermMatrix result (only structure)
  string_t name="inv("+A.name()+")t*"+B.name();
  TermMatrix R(name);
  un_A=A.colUnknowns(); un_B=B.colUnknowns();
  std::map<const Unknown*, Space*> spu, spv;
  for (cit_mustm it = A.suTerms().begin(); it != A.suTerms().end(); it++)  spv[it->first.second]=it->second->space_vp();
  for (cit_mustm it = B.suTerms().begin(); it != B.suTerms().end(); it++)  spu[it->first.first]=it->second->space_up();
  for (itv=un_A.begin(); itv!=un_A.end(); ++itv) //create void SuTermMatrix
    for (itu=un_B.begin(); itu!=un_B.end(); ++itu)
    {
      R.insert(new SuTermMatrix(*itu, spu[*itu], *itv, spv[*itv], 0, name+"_"+(*itu)->name()+"_"+(*itv)->name()));
    }
  //set value type
  ValueType vt=A.valueType();
  if (vt==_real) vt=B.valueType();
  if (A.isSingleUnknown() && B.isSingleUnknown())
  {
    //allocate SuTermMatrix Matrixentry
    number_t nbr=A.numberOfRows(), nbc=B.numberOfCols();
    SuTermMatrix* sutR=R.suTerms().begin()->second;
    MatrixEntry* matR = new MatrixEntry(vt,_scalar, new ColDenseStorage(nbr,nbc));
    SuTermMatrix* sutA = A.suTerms().begin()->second;
    if (sutA->scalar_entries()!=nullptr) sutR->scalar_entries() = matR;
    else sutR->entries() = matR;
    if(sutA->scalar_entries()==sutA->entries()) sutR->entries() = matR; //to enforce both scalar and non scalar representation
    //compute inverse
    for (number_t k=1; k<=nbc; k++)
    {
      MatrixEntry* mat=nullptr;
      VectorEntry* b=nullptr;
      TermVector Bk=B.column(k);
      TermVector Xk=prepareLinearSystem(A,Bk,mat,b);
      VectorEntry* x=Xk.actual_entries();
      switch (A.factorization())  //solve system At*Xk=Bk
      {
        case _ldlt:
          mat->ldltSolve(*b, *x);
          break;
        case _ldlstar:
          mat->ldlstarSolve(*b, *x);
          break;
        case _lu:
          mat->luLeftSolve(*b, *x);
          break;
        case _umfpack:
          mat->umfluLeftSolve(*b, *x);
          break;
        default:
          error("wrong_factorization_type", words("factorization type",A.factorization()));
      }
      // write in SuTermMatrix
      if (vt==_real)
      {
        std::vector<real_t>::iterator itm=matR->rEntries_p->values().begin()+(1+(k-1)*nbr),
                                      itv=x->rEntries_p->begin();
        for (; itv!=x->rEntries_p->end(); ++itv, ++itm) *itm=*itv;
      }
      else
      {
        std::vector<complex_t>::iterator itm=matR->cEntries_p->values().begin()+(1+(k-1)*nbr),
                                         itv=x->cEntries_p->begin();
        for (; itv!=x->cEntries_p->end(); ++itv, ++itm) *itm=*itv;
      }
    }
  }
  else
    error("term_not_suterm", A.name() + " || " + B.name());

  trace_p->pop();
  return R;
}

// solve linear system using LDLt factorization, return factorized matrix
TermVector ldltSolve(TermMatrix& A, const TermVector& B, TermMatrix& Af)
{
  factorize(A, Af, _ldlt);
  return factSolve(Af, B);
}

// solve linear system using LDLt factorization with multiple right hand sides, return factorized matrix
TermVectors ldltSolve(TermMatrix& A, const std::vector<TermVector>& Bs, TermMatrix& Af)
{
  factorize(A, Af, _ldlt);
  return factSolve(Af, Bs);
}

// solve transposed linear system using LDLt factorization, return factorized matrix
TermVector ldltLeftSolve(TermMatrix& A, const TermVector& B, TermMatrix& Af)
{
  factorize(A, Af, _ldlt);
  return factSolve(Af, B);  // At=A
}

// solve transpose linear system using LDLt factorization with multiple right hand sides, return factorized matrix
TermVectors ldltLeftSolve(TermMatrix& A, const std::vector<TermVector>& Bs, TermMatrix& Af)
{
  factorize(A, Af, _ldlt);
  return factSolve(Af, Bs); // At=A
}

// solve linear system using LDL* factorization, return factorized matrix
TermVector ldlstarSolve(TermMatrix& A, const TermVector& B, TermMatrix& Af)
{
  factorize(A, Af, _ldlstar);
  return factSolve(Af, B);
}

// solve linear system using LDL* factorization, with multiple right hand sides return factorized matrix
TermVectors ldlstarSolve(TermMatrix& A, const std::vector<TermVector>& Bs, TermMatrix& Af)
{
  factorize(A, Af, _ldlstar);
  return factSolve(Af, Bs);
}

// solve transposed linear system using LDL* factorization, return factorized matrix
TermVector ldlstarLeftSolve(TermMatrix& A, const TermVector& B, TermMatrix& Af)
{
  factorize(A, Af, _ldlstar);
  return factSolve(Af, B);
}

// solve transposed linear system using LDL* factorization, with multiple right hand sides return factorized matrix
TermVectors ldlstarLeftSolve(TermMatrix& A, const std::vector<TermVector>& Bs, TermMatrix& Af)
{
  factorize(A, Af, _ldlstar);
  return factSolve(Af, Bs);
}

// solve linear system using LU factorization, return factorized matrix
TermVector luSolve(TermMatrix& A, const TermVector& B, TermMatrix& Af)
{
  factorize(A, Af, _lu);
  return factSolve(Af, B);
}

// solve linear system using LU factorization, with multiple right hand side, return factorized matrix
TermVectors luSolve(TermMatrix& A, const std::vector<TermVector>& Bs, TermMatrix& Af)
{
  factorize(A, Af, _lu);
  return factSolve(Af, Bs);
}

// solve linear system using LU factorization, return factorized matrix
TermVector luLeftSolve(TermMatrix& A, const TermVector& B, TermMatrix& Af)
{
  factorize(A, Af, _lu);
  return factLeftSolve(Af, B);
}

// solve linear system using LU factorization, with multiple right hand side, return factorized matrix
TermVectors luLeftSolve(TermMatrix& A, const std::vector<TermVector>& Bs, TermMatrix& Af)
{
  factorize(A, Af, _lu);
  return factLeftSolve(Af, Bs);
}

// solve linear system by Gauss elimination (moving to row dense storage)
// if keepA is true, the original matrix is preserved
TermVector gaussSolve(TermMatrix& A, const TermVector& B, bool keepA)
{
  trace_p->push("gaussSolve(TermMatrix&, TermVector&)");
  MatrixEntry* mat=nullptr;
  VectorEntry* b=nullptr;
  TermVector cB(B);    //copy B entries
  TermMatrix* cA = &A;
  if(keepA) cA = new TermMatrix(A); //preserved A
  TermVector X=prepareLinearSystem(*cA, cB, mat, b, _dense, _row, true);
  VectorEntry* x=X.actual_entries();

  gaussSolve(*mat,*b,*x);

  //finalization
  if(A.isSingleUnknown())
    {
      SuTermVector* vX = X.begin()->second;
      if(vX->up()->nbOfComponents()> 1)  vX->toVector();  //return to vector representation
      vX->computed() = true;
    }
  else  X.toLocal(false);

  X.computed() = true;
  if(keepA) delete cA;   //delete the copy
  trace_p->pop();
  return X;
}

// solve linear system by Gauss elimination (moving to row dense storage)
// if keepA is true, matrix A is preserved
// Does not exist with multiple right hand side !!!
TermVectors gaussSolve(TermMatrix& A, const std::vector<TermVector>& Bs, bool keepA)
{
  error("not_yet_implemented","gaussSolve(TermMatrix&, const TermVectors&, bool)");
  return Bs;
}

// solve linear system using umfpack (if installed)
//   update the reciprocal condition number rcond = 1/||A||*||inv(A)|| in norm 1
//   if keepA is true, the original matrix is preserved
TermVector umfpackSolve(TermMatrix& A, const TermVector& B, real_t& rcond, bool keepA)
{
  #ifdef XLIFEPP_WITH_UMFPACK
    trace_p->push("umfpackSolve(TermMatrix, TermVector)");
    MatrixEntry* mat=nullptr;
    VectorEntry* b=nullptr;
    TermVector cB(B);    //copy B entries
    TermMatrix* cA = &A;
    if(keepA) cA = new TermMatrix(A); //preserved A
    TermVector X=prepareLinearSystem(*cA, cB, mat, b, _cs, _col, true);  //force matrix to cs_col storage well adapted to umfpack
    VectorEntry* x=X.actual_entries();
    umfpackSolve(*mat,*b,*x, rcond);

    //finalization
    if(A.isSingleUnknown())
      {
        SuTermVector* vX = X.begin()->second;
        if(vX->up()->nbOfComponents()> 1)  vX->toVector();  //return to vector representation
        vX->computed() = true;
      }
    else  X.toLocal(false);

    X.computed() = true;
    if(keepA) delete cA;   //delete the copy
    trace_p->pop();
    return X;
  #else
    error("xlifepp_without_umfpack");
    return B;
  #endif
}

// solve linear system using umfpack (if installed) with multiple right hand sides
//    update the reciprocal condition number rcond = 1/||A||*||inv(A)|| in norm 1
//    if keepA is true, the original matrix is preserved (default= false)
TermVectors umfpackSolve(TermMatrix& A, const std::vector<TermVector>& Bs, real_t& rcond, bool keepA)
{
  #ifdef XLIFEPP_WITH_UMFPACK
    trace_p->push("umfpackSolve(TermMatrix, TermVectors)");
    TermVectors cBs=Bs;    //preserve right hand sides
    number_t n=cBs.size();
    TermVectors Xs(n);
    std::vector<VectorEntry*> bs(n,0);
    std::vector<VectorEntry*> xs(n,0);
    TermMatrix* cA = &A;
    if(keepA) cA = new TermMatrix(A); //preserved A
    MatrixEntry* mat=nullptr;
    for(number_t k=0; k<n; k++)
      {
        Xs[k]=prepareLinearSystem(*cA,cBs[k],mat,bs[k],_cs, _col, true);  //force matrix to cs col storage well adapted to Umfpack
        xs[k]=Xs[k].actual_entries();
      }
    umfpackSolve(*mat,bs,xs,rcond);
    if(theVerboseLevel>0) thePrintStream<<"UmfPack reciprocal number = "<<tostring(rcond)<<eol;

    //finalization
    for(number_t k=0; k<n; k++)
      {
        if(A.isSingleUnknown())
          {
            SuTermVector* vX = Xs[k].begin()->second;
            if(vX->up()->nbOfComponents()> 1)  vX->toVector();  //return to vector representation
            vX->computed() = true;
          }
        else  Xs[k].toLocal(false);

        Xs[k].computed() = true;
      }
    if(keepA) delete cA;   //delete the copy
    trace_p->pop();
    return Xs;
  #else
    error("xlifepp_without_umfpack");
    return Bs;
  #endif
}

//shorcut without rcond
TermVector umfpackSolve(TermMatrix& A, const TermVector& B, bool keepA)
{
  real_t rcond;
  return umfpackSolve(A,B,rcond, keepA);
}

TermVectors umfpackSolve(TermMatrix& A, const std::vector<TermVector>& Bs, bool keepA)
{
  real_t rcond;
  return umfpackSolve(A,Bs,rcond, keepA);
}

#ifdef XLIFEPP_WITH_MAGMA
  /*! solve linear system using magma solver (after moving to row dense storage)
      if useGPU is true, use specific magma gpu version: fails if the matrix is too large (larger than gpu memory)
      if keepA is true, the original matrix is preserved
  */
  TermVector magmaSolve(TermMatrix& A, const TermVector& B, bool useGPU, bool keepA)
  {
    magma_init();
    trace_p->push("magmaSolve(TermMatrix, TermVector)");
    //prepare linear system
    MatrixEntry* mat=nullptr;
    VectorEntry* b=nullptr;
    TermVector cB(B);                 //copy B to preserve right hand side
    TermMatrix* cA = &A;
    if(keepA) cA = new TermMatrix(A); //preserved A if demanded
    TermVector X = prepareLinearSystem(*cA, cB, mat, b, _dense, _col, true);
    VectorEntry* x=X.actual_entries();
    //call magma function
    *x=*b;      //copy b to x

    magma_int_t n=mat->nbOfRows();

    std::vector<magma_int_t> ipiv(n);  //allocate magma pivoting vector
    //for(number_t i=1;i<=n;i++) ipiv[i-1]=i;
    magma_int_t info = 0;
    ValueType vta = mat->valueType();
    if(vta==_real)  //real scalar type
      {
        real_t* mp = &(mat->rEntries_p->values()[1]);
        real_t* xp = &(*x->rEntries_p)[0];
        if(!useGPU)
          {
            std::cout << "using magma in complex (zgesv)" << eol;
            magma_dgesv(n, 1,mp, n, &ipiv[0], xp, n, &info);
          }
        else
          {
            std::cout << "using gpu magma in complex (zgesv_gpu)" << eol;
            magmaDouble_ptr dA,dB;
            magma_malloc((void**) &dA, sizeof(real_t)*((n*n)+10));
            magma_malloc((void**) &dB, sizeof(real_t)*(n));
            magma_dsetmatrix(n,n,mp,n,dA,n);
            magma_int_t dx=1;
            magma_dsetvector(n,xp, dx, dB, dx);
            //magma_zgesv_nopiv_gpu( n, 1, dA,n,dB,n,&info);
            magma_dgesv_gpu(n, 1, dA,n, &ipiv[0],dB,n,&info);
            magma_dgetvector(n, dB, 1, xp,1);
          }
        if(info!=0) magma_xerbla("dgesv",info);
      }
    else  //complex scalar type
      {
        complex_t* mp = &(mat->cEntries_p->values()[1]);
        complex_t* xp = &(*x->cEntries_p)[0];
        if(!useGPU)
          {
            std::cout << "using magma in complex (zgesv)" << eol;
            magma_zgesv(n, 1, (magmaDoubleComplex*) mp, n, &ipiv[0], (magmaDoubleComplex*) xp, n, &info);
          }
        else //use specific magma gpu
          {
            std::cout << "using gpu magma in complex (zgesv_gpu)" << eol;
            magmaDoubleComplex* dA,*dB;
            magma_malloc((void**) &dA, sizeof(magmaDoubleComplex)*((n*n)+10));
            magma_malloc((void**) &dB, sizeof(magmaDoubleComplex)*(n));
            magma_zsetmatrix(n,n,(magmaDoubleComplex*) mp,n,(magmaDoubleComplex*)dA,n);
            magma_int_t dx=1;
            magma_zsetvector(n, (magmaDoubleComplex*)xp, dx, (magmaDoubleComplex*) dB, dx);
            //magma_zgesv_nopiv_gpu( n, 1, dA,n,dB,n,&info);
            magma_zgesv_gpu(n, 1, dA,n, &ipiv[0],dB,n,&info);
            magma_zgetvector(n, dB, 1, (magmaDoubleComplex*)xp,1);
          }
        if(info!=0) magma_xerbla("zdgesv",info);
      }
    //finalization
    magma_finalize();
    if(A.isSingleUnknown())
      {
        SuTermVector* vX = X.begin()->second;
        if(vX->up()->nbOfComponents()> 1) vX->toVector();  //return to vector representation
        vX->computed() = true;
      }
    else  X.toLocal(false);

    X.computed() = true;
    if(keepA) delete cA;   //delete the copy
    trace_p->pop();
    return X;
  }
#endif // XLIFEPP_WITH_MAGMA

#ifdef XLIFEPP_WITH_ARPACK
  /*! solve linear system using lapack solvers dgesv and zgesv
      if optimized blas and lapack lib are used, really faster than xlifepp gauss solver
      if not optimized blas and lapack lib are used, may be slower than xlifepp gauss solver
  */
  TermVector lapackSolve(TermMatrix& A, const TermVector& B, bool keepA)
  {
    trace_p->push("lapackSolve(TermMatrix, TermVector)");
    //prepare linear system
    MatrixEntry* mat=nullptr;
    VectorEntry* b=nullptr;
    TermVector cB(B);                 //copy B to preserve right hand side
    TermMatrix* cA = &A;
    if (keepA) cA = new TermMatrix(A); //preserved A if demanded
    TermVector X = prepareLinearSystem(*cA, cB, mat, b, _dense, _col, true);
    VectorEntry* x=X.actual_entries();
    *x=*b;                            //copy b to x
    int_t n=mat->nbOfRows();
    int_t nrhs=1;
    std::vector<int_t> ipiv(mat->nbOfRows());  //allocate lapack pivoting vector
    int_t info=0;

    ValueType vta = mat->valueType(), vtx=x->valueType();
    if (vta==_real)  //real matrix
    {
      if (vtx==_real) //real rhs
      {
        real_t* mp = &(mat->rEntries_p->values()[1]);
        real_t* xp = &(*x->rEntries_p)[0];
        dgesv_(&n, &nrhs, mp, &n, &ipiv[0], xp, &n, &info);
      }
      else //complex rhs
      {
        VectorEntry xrs(_real,_scalar,2*n);
        Vector<complex_t>::iterator itcb=x->cEntries_p->begin(), itce=x->cEntries_p->end(), itc;
        Vector<real_t>::iterator itr=xrs.rEntries_p->begin(), itrc=itr+n;
        for (itc=itcb; itc!=itce; ++itc,++itr,++itrc) {*itr=itc->real(); *itrc=itc->imag();}
        nrhs=2;
        real_t* mp = &(mat->rEntries_p->values()[1]);
        real_t* xp = &(*xrs.rEntries_p)[0];
        dgesv_(&n, &nrhs, mp, &n, &ipiv[0], xp, &n, &info);
        itr=xrs.rEntries_p->begin(), itrc=itr+n;
        for(itc=itcb; itc!=itce; ++itc,++itr,++itrc) *itc=complex_t(*itr,*itrc);
      }
    }
    else  //complex matrix
    {
      if (vtx==_real) x->toComplex();  //real rhs, move to complex
      complex_t* mp = &(mat->cEntries_p->values()[1]);
      complex_t* xp = &(*x->cEntries_p)[0];
      zgesv_(&n, &nrhs, mp, &n, &ipiv[0], xp, &n, &info);
    }

    if (info<0) error("invalid_option");
    else if (info>0) error("small_pivot");

    if (A.isSingleUnknown())
    {
      SuTermVector* vX = X.begin()->second;
      if(vX->up()->nbOfComponents()> 1) vX->toVector();  //return to vector representation
      vX->computed() = true;
    }
    else X.toLocal(false);

    X.computed() = true;
    if (keepA) delete cA;   //delete the copy
    trace_p->pop();
    return X;
  }
#endif // XLIFEPP_WITH_ARPACK

//------------------------------------------------------------------------------------------------------------
/*! solve linear system by direct method, trying to find the well adapted direct method
     if A is already factorized goto upper-lower solver (factSolve)
     if umfpack is available, it is used (umfpackSolve)
     if A is single unknown dense matrix use pivoting gauss elimination process (gaussSolve)
     else factorize LU, LDLt or LDL* and  goto upper-lower solver (factSolve)

      if keepA is false (default) the matrix A is modified else it is not modified
*/
//------------------------------------------------------------------------------------------------------------
TermVector directSolve(TermMatrix& A, const TermVector& B, bool keepA)
{

  if(theVerboseLevel>0) std::cout<<"solving linear system "<<A.name()<< " * X = " <<B.name()<<" (size "<<A.numberOfRows()<<") ";
  if(A.factorization()!=_noFactorization) return factSolve(A,B);   //already factorized
  trace_p->push("directSolve(TermMatrix, TermVector)");

  std::pair<StorageType,AccessType> stat(_noStorage,_noAccess);
  if(A.isSingleUnknown()) //get current storage of single unknown matrix
    {
      stat.first  = A.begin()->second->storageType();
      stat.second = A.begin()->second->accessType();
    }
  else //find target storage of multiple unknowns matrix
    {
      const MatrixEntry* mat=A.scalar_entries();
      if(mat!=nullptr) stat = std::make_pair(mat->storageType(), mat->accessType());
      else stat = A.findGlobalStorageType();
    }

  if(stat.first==_dense) //use Gauss reduction or Lapack solver if available
    {
      TermVector X;
      #ifdef XLIFEPP_WITH_ARPACK
        if(stat.second==_col)
        {
          if(theVerboseLevel>0) std::cout<<"using Lapack solver in col dense storage"<<eol;
          X = lapackSolve(A, B, keepA);
        }
        else
        {
          if(theVerboseLevel>0) std::cout<<"using Gauss elimination with row pivoting in row dense storage"<<eol;
          X = gaussSolve(A, B, keepA);
        }
      #else
        if(theVerboseLevel>0) std::cout<<"using Gauss elimination with row pivoting in row dense storage"<<eol;
        X = gaussSolve(A, B, keepA);
      #endif // XLIFEPP_WITH_ARPACK
      trace_p->pop();
      return X;
    }

  //use umfpack if installed else use LU type factorization
  #ifdef XLIFEPP_WITH_UMFPACK
    if(theVerboseLevel>0) std::cout<<"using umfpack"<<eol;
    TermVector X = umfpackSolve(A, B, keepA);
  #else
    if(theVerboseLevel>0) std::cout<<"using ";
    TermMatrix* Af = &A;
    if(keepA)  Af = new TermMatrix(A);   //copy of A if keepA
    factorize(A, *Af);                   //factorize in Af
    TermVector X =factSolve(*Af, B);
    if(keepA) delete Af;
  #endif // XLIFEPP_WITH_UMFPACK

  trace_p->pop();
  return X;
}

TermVectors directSolve(TermMatrix& A, const std::vector<TermVector>& Bs, bool keepA)
{
  if(theVerboseLevel>0)
    {
      std::cout<<"solving linear system "<<A.name()<< " * X = Bs (size "<<A.numberOfRows()<<") ";
      std::cout<<" factorization = "<<words("factorization type",A.factorization())<<eol<<std::flush;
    }
  if(A.factorization()!=_noFactorization) return factSolve(A,Bs);   //already factorized
  trace_p->push("directSolve(...)");

  std::pair<StorageType,AccessType> stat(_noStorage,_noAccess);
  if(A.isSingleUnknown()) //get current storage of single unknown matrix
    {
      stat.first  = A.begin()->second->storageType();
      stat.second = A.begin()->second->accessType();
    }
  else //find target storage of multiple unknowns matrix
    {
      const MatrixEntry* mat=A.scalar_entries();
      if(mat!=nullptr) stat = std::make_pair(mat->storageType(), mat->accessType());
      else stat = A.findGlobalStorageType();
    }

  if(stat.first==_dense) //use Gauss reduction
    {
      if(theVerboseLevel>0) std::cout<<"using Gauss elimination with row pivoting in row dense storage"<<eol;
      TermVectors Xs =  gaussSolve(A, Bs, keepA);
      trace_p->pop();
      return Xs;
    }

  //use umfpack if installed else use LU type factorization
#ifdef XLIFEPP_WITH_UMFPACK
  if(theVerboseLevel>0) std::cout<<"using umfpack"<<eol;
  TermVectors Xs =  umfpackSolve(A, Bs, keepA);
#else
  if(theVerboseLevel>0) std::cout<<"using ";
  TermMatrix* Af = &A;
  if(keepA)  Af = new TermMatrix(A);   //copy of A
  factorize(A, *Af);                  //factorize in Af
  TermVectors Xs =factSolve(*Af, Bs);
  if(keepA) delete Af;
#endif // XLIFEPP_WITH_UMFPACK

  trace_p->pop();
  return Xs;
}

//------------------------------------------------------------------------------------------------------------
/*!  Schur Solver, only for 2x2 unknowns TermMatrix:

         |A11  A12 ||X1|   |B1|
         |         ||  | = |  |
         |A21  A22 ||X2|   |B1|

    if A11 is invertible:
        C21 = A21 * inv(A11) * A12
        C22 = A22 - C21
        D2  = B2 - A21 * inv(A11) * B1
    if C22 invertible
        X2 = inv(C22) * D2
        X1 = inv(A11)(B1 - A12 * X2)

    A: multiple unknowns TermMatrix
    B: multiple unknowns TermVector
    row_v, col_u: unknowns pair defining the diagonal block used as pivot (say A11)
    keepA: true if TermMatrix has to be preserved (TermVector B is always preserved)
*/
//------------------------------------------------------------------------------------------------------------

TermVector schurSolve(TermMatrix& A, const TermVector& B, const Unknown& row_v, const Unknown& col_u, bool keepA)
{
  trace_p->push("schurSolve(...)");

  //check validity: 2x2 TermMatrix with same unknowns, v dual of u and belongs to list of TermMatrix unknowns
  std::set<const Unknown*> row_vs = A.rowUnknowns();      // set of row Unknowns (say v unknowns)
  std::set<const Unknown*> col_us = A.colUnknowns();      // set of col Unknowns (say u unknowns)
  if (row_vs.size()!=2 || col_us.size()!=2) error("block_term_bad_size",A.name(), row_vs.size(), col_us.size());
  if (&row_v!=&col_u && &row_v!=col_u.dual_p()) error("term_inconsistent_unknowns");
  std::set<const Unknown*>::iterator itv=row_vs.begin(), itu;
  for (; itv!=row_vs.end(); ++itv)
  {
    itu=col_us.find(*itv);
    if (itu==col_us.end()) itu=col_us.find((*itv)->dual_p());
    if (itu==col_us.end()) error("term_inconsistent_unknowns");
  }

  //extract matrix blocks
  TermMatrix* pA=&A;
  if (keepA) pA=new TermMatrix(A); //copy A
  std::vector<const Unknown*> rvs(2), cus(2);
  itu=col_us.begin();
  if (*itu==&col_u || *itu==col_u.dual_p()) {cus[0]=*itu; cus[1]=*(++itu);}
  else {cus[1]=*itu; cus[0]=*(++itu);}
  itv=row_vs.begin();
  if (*itv==&row_v || *itv==row_v.dual_p()) {rvs[0]=*itv; rvs[1]=*(++itv);}
  else {rvs[1]=*itv; rvs[0]=*(++itv);}
  SuTermMatrix* A11=pA->subMatrix_p(cus[0],rvs[0]), *A12=pA->subMatrix_p(cus[1],rvs[0]),
                *A21=pA->subMatrix_p(cus[0],rvs[1]), *A22=pA->subMatrix_p(cus[1],rvs[1]);
  if (A11==nullptr) error("is_null","A11");

  //prepare sub matrices: align numbering using += algorithm that manages extension and move to scalar representation
  std::vector<Space*> spaces_u1, spaces_v1, spaces_u2, spaces_v2;
  spaces_u1.push_back(A11->space_up());
  spaces_v1.push_back(A11->space_vp());
  if (A12!=nullptr) {spaces_u2.push_back(A12->space_up()); spaces_v1.push_back(A12->space_vp());}
  if (A21!=nullptr) {spaces_u1.push_back(A21->space_up()); spaces_v2.push_back(A21->space_vp());}
  if (A22!=nullptr) {spaces_u2.push_back(A22->space_up()); spaces_v2.push_back(A22->space_vp());}
  Space* space_u1 = mergeSubspaces(spaces_u1, true);
  Space* space_v1 = mergeSubspaces(spaces_v1, true);
  Space* space_u2 = mergeSubspaces(spaces_u2, true);
  Space* space_v2 = mergeSubspaces(spaces_v2, true);
  if (A11->space_up()!=space_u1 || A11->space_vp()!=space_v1)  //extend matrix
  {
    SuTermVector zero("zero11", A11->up(), space_u1, 0.);
    SuTermMatrix diag(A11->up(), space_u1, A11->vp(),space_v1, zero);
    *A11+=diag;
  }
  if (A12!=nullptr && (A12->space_up()!=space_u2 || A12->space_vp()!=space_v1))  //extend matrix
  {
    SuTermVector zero("zero12", A12->up(), space_u2, 0.);
    SuTermMatrix diag(A12->up(), space_u2, A12->vp(),space_v1, zero);
    *A12+=diag;
  }
  if (A21!=nullptr && (A21->space_up()!=space_u1 || A21->space_vp()!=space_v2))  //extend matrix
  {
    SuTermVector zero("zero21", A21->up(), space_u1, 0.);
    SuTermMatrix diag(A21->up(), space_u1, A21->vp(),space_v2, zero);
    *A21+=diag;
  }
  if (A22!=nullptr && (A22->space_up()!=space_u2 || A22->space_vp()!=space_v2))  //extend matrix
  {
    SuTermVector zero("zero22", A22->up(), space_u2, 0.);
    SuTermMatrix diag(A22->up(), space_u2, A22->vp(),space_v2, zero);
    *A22+=diag;
  }
  A11->toScalar();
  if (A12!=nullptr) A12->toScalar();
  if (A21!=nullptr) A21->toScalar();
  if (A22!=nullptr) A22->toScalar();

  //prepare rhs sub vectors (move to scalar representation and adjust numbering)
  TermVector cB(B);    //copy B entries
  updateRhs(A,cB);     //take into account essential conditions
  SuTermVector* B1=cB.subVector_p(rvs[0]);
  if (B1==nullptr) B1=cB.subVector_p(cus[0]);   //try with dual
  if (B1!=nullptr && B1->spacep()->dimSpace()!=space_v1->dimSpace())
  {
    SuTermVector zero("zero", rvs[0], space_v1, 0.);
    *B1+=zero;
  }
  if (B1!=nullptr) B1->toScalar();
  SuTermVector* B2=cB.subVector_p(rvs[1]);
  if (B2==nullptr) B2=cB.subVector_p(cus[1]);   //try with dual
  if (B2!=nullptr &&  B2->spacep()->dimSpace()!=space_v2->dimSpace())
  {
    SuTermVector zero("zero", rvs[1], space_v2, 0.);
    *B2+=zero;
  }
  if (B2!=nullptr) B2->toScalar();

  //Schur method
  //------------
  TermVector X("X solves AX=B, A=" + A.name() + ", B=" + B.name());
  SuTermVector* D1=nullptr, *D2=nullptr;
  //factorize A11
  factorize(*A11,_noFactorization);
  //create matrix C21 = A21 * inv(A11) * A12
  SuTermMatrix C21;
  if (A12!=nullptr && A21!=nullptr)
  {
    C21=(*A21)*factSolve(*A11,*A12);
    C21.setStorage(_dense,_row);
  }
  //create matrix C22 = A22 - C21 * A12, override A22
  if (A22!=nullptr)
  {
    if (A12!=nullptr && A21!=nullptr)
    {
      A22->scalar_entries()->toStorage(C21.scalar_entries()->storagep());
      *A22->scalar_entries()-= *C21.scalar_entries(); //same storage do straightforward
    }
  }
  else
  {
    if (A12!=nullptr && A21!=nullptr) {A22=&C21; *A22 *=-1;}
    else error("mat_noinvert");
  }
  //create vector D2 = B2 - A21 * inv(A11) * B1
  if (B2!=nullptr)
  {
    D2 = new SuTermVector(*B2);
    if (A21!=nullptr && B1!=nullptr) *D2-=((*A21)*factSolve(*A11,*B1));
  }
  else if (A21!=nullptr && B1!=nullptr)
    D2=new SuTermVector((*A21)*factSolve(*A11,*B1)); *D2*=-1.;
  //create X1, X2
  if (D2!=nullptr)
  {
    //create vector X2 = inv(A22) * D2
    SuTermVector X2;
    if (A22->storageType()==_dense)
    {
      if (A22->accessType()!=_row) A22->setStorage(_dense,_row);
      X2 = gaussSolve(*A22,*B2);
    }
    else
    {
      #ifdef XLIFEPP_WITH_UMFPACK
        X2= umfpackSolve(*A22,*D2);
      #else
        factorize(*A22);
        X2 =factSolve(*A22, *D2);
      #endif
    }
    //create vector D1 = B1 - A12 * X2
    if (B1!=nullptr)
      { D1=new SuTermVector(*B1); if(A12!=nullptr) *D1-=(*A12)*X2; }
    else if (A12!=nullptr)
      { D1=new SuTermVector((*A12)*X2); *D1*=-1.;}

    //create vector X1 = inv(A11) * D1
    SuTermVector X1;
    if (D1!=nullptr) X1=factSolve(*A11,*D1);
    X.insert(X1);
    X.insert(X2);
  }
  else  //D2=0 -> X2=0
  {
    ValueType vtX=_real;
    if (A.valueType()==_complex || B.valueType()==_complex) vtX=_complex;
    if (B1!=nullptr)
    {
      SuTermVector X1=factSolve(*A11,*B1);
      SuTermVector X2;
      A22->initSuTermVector(X2,vtX,true);
      X.insert(X1);
      X.insert(X2);
    }
    else //X1=X2=0
    {
      SuTermVector X1;
      A11->initSuTermVector(X1,vtX,true);
      SuTermVector X2;
      A22->initSuTermVector(X2,vtX,true);
      X.insert(X1);
      X.insert(X2);
    }
  }

  //clean or delete temporary structures
  if (D2!=nullptr) delete D2;
  if (D1!=nullptr) delete D1;
  if (keepA) delete pA;
  else //clean scalar parts
  {
    if (A11!=nullptr) A11->clearScalar();
    if (A21!=nullptr) A21->clearScalar();
    if (A12!=nullptr) A12->clearScalar();
    if (A22!=nullptr) A22->clearScalar();
  }

  trace_p->pop();
  return X;
}

//------------------------------------------------------------------------------------------------------------
//  Iterative Solvers
//------------------------------------------------------------------------------------------------------------
TermVector iterativeSolveGen(IterativeSolverType isType, TermMatrix& A,  TermVector& B, const TermVector& X0,
                             Preconditioner& P, real_t tol, number_t iterMax, const real_t omega,
                             const number_t krylovDim, const number_t verboseLevel, const string_t& nam)
{
  trace_p->push("iterativeSolveGen(...)");
  PreconditionerType pType = P.kind();
  MatrixEntry* mat=nullptr;
  VectorEntry* b=nullptr;
  TermVector cB(B);    //copy B entries
  bool toScalar=false;
  if (P.kind() != _noPrec) toScalar=true;
  TermVector X=prepareLinearSystem(A,cB,mat,b,_noStorage, _noAccess, toScalar);
  bool useScalarGlobal = A.scalar_entries() != nullptr;
  bool useScalarLocal  = A.isSingleUnknown() && A.begin()->second->scalar_entries() != nullptr;

  if (useScalarGlobal || useScalarLocal)  //A has a scalar representation, use scalar MatrixEntry and scalar VectorEntry
    {
      VectorEntry* x=X.actual_entries();
      VectorEntry* px0 =0 ;
      TermVector pX0(X0);
      if (pX0.size()==0) A.initTermVector(pX0, A.valueType());
      if(X.valueType()==_complex && pX0.valueType()==_real) pX0.toComplex();  //move initial guess to complex
      if (useScalarGlobal)
        {
          pX0.toGlobal(true);
          pX0.adjustScalarEntries(A.cdofsc());
          px0 = pX0.scalar_entries();
        }
      else //use scalar local (single unknown), work with first SuTermMatrix
        {
          SuTermVector* sP0 = pX0.begin()->second;
          sP0->toScalar(true);
          sP0->adjustScalarEntries(A.begin()->second->cdofsu());
          px0 = sP0->scalar_entries();
        }
      P.termVector(pX0);
      switch (isType)
        {
        case _cg:
        {
          CgSolver cg(tol, iterMax, verboseLevel);
          if (_noPrec == pType) *x = cg(*mat, *b, *px0);
          else                  *x = cg(*mat, *b, *px0, P);
          break;
        }
        case _cgs:
        {
          CgsSolver cgs(tol, iterMax, verboseLevel);
          if (_noPrec == pType) *x = cgs(*mat, *b, *px0);
          else                  *x = cgs(*mat, *b, *px0, P);
          break;
        }
        case _qmr:
        {
          QmrSolver qmr(tol, iterMax, verboseLevel);
          if (_noPrec == pType) *x = qmr(*mat, *b, *px0);
          else                  *x = qmr(*mat, *b, *px0, P);
          break;
        }
        case _bicg:
        {
          BicgSolver bicg(tol, iterMax, verboseLevel);
          if (_noPrec == pType) *x = bicg(*mat, *b, *px0);
          else                  *x = bicg(*mat, *b, *px0, P);
          break;
        }
        case _bicgstab:
        {
          BicgStabSolver bicgStab(tol, iterMax, verboseLevel);
          if (_noPrec == pType) *x = bicgStab(*mat, *b, *px0);
          else                  *x = bicgStab(*mat, *b, *px0, P);
          break;
        }
        case _gmres:
        {
          GmresSolver gmres(krylovDim, tol, iterMax, verboseLevel);
          if (_noPrec == pType) *x = gmres(*mat, *b, *px0);
          else                  *x = gmres(*mat, *b, *px0, P);
          break;
        }
        /*case _sor:
        {
          SorSolver sor(omega, tol, iterMax, verboseLevel);
          *x = sor(*mat, *b, *px0);
          break;
        }
        case _ssor:
        {
          SsorSolver ssor(omega, tol, iterMax, verboseLevel);
          *x = ssor(*mat, *b, *px0);
          break;
        }*/
        default:
          error("solver_NotHandled", words("iterative solver",isType));
          break;
        }
      if (useScalarGlobal) X.toLocal();
      if (useScalarLocal)  X.toVector();
      X.name(nam);
      trace_p->pop();
      return X;
    }

  //Block matrix structure, use TermMatrix and TermVector
  TermVector pX0(X0);
  if (pX0.size()==0) A.initTermVector(pX0, A.valueType());
  if(X.valueType()==_complex && pX0.valueType()==_real) pX0.toComplex();  //move initial guess to complex

  // TermVector X(X0);
  // if (0 == X0.size())
  // {
  //   TermVector temp;
  //   TermMatrix* pA = const_cast<TermMatrix*>(&A);
  //   ValueType vType = A.valueType();
  //   pA->initTermVector(temp, vType);
  //   pX0 = temp;
  // }

  if (A.colUnknowns().find(pX0.unknown()) == A.colUnknowns().end()) error("term_inconsistent_unknowns");
  if (A.rowUnknowns().find(B.unknown()) == A.rowUnknowns().end()) error("term_inconsistent_unknowns");

  switch(isType)
    {
    case _cg:
    {
      CgSolver cg(tol, iterMax, verboseLevel);
      if (_noPrec == pType) X = cg(A, cB, pX0);
      else                  X = cg(A, cB, pX0, P);
      break;
    }
    case _cgs:
    {
      CgsSolver cgs(tol, iterMax, verboseLevel);
      if (_noPrec == pType) X = cgs(A, cB, pX0);
      else                  X = cgs(A, cB, pX0, P);
      break;
    }
    case _qmr:
    {
      QmrSolver qmr(tol, iterMax, verboseLevel);
      if (_noPrec == pType) X = qmr(A, cB, pX0);
      else                  X = qmr(A, cB, pX0, P);
      break;
    }
    case _bicg:
    {
      BicgSolver bicg(tol, iterMax, verboseLevel);
      if (_noPrec == pType) X = bicg(A, cB, pX0);
      else                  X = bicg(A, cB, pX0, P);
      break;
    }
    case _bicgstab:
    {
      BicgStabSolver bicgStab(tol, iterMax, verboseLevel);
      if(_noPrec == pType) X = bicgStab(A, cB, pX0);
      else                 X = bicgStab(A, cB, pX0, P);
      break;
    }
    case _gmres:
    {
      GmresSolver gmres(krylovDim, tol, iterMax, verboseLevel);
      if (_noPrec == pType) X = gmres(A, cB, pX0);
      else                  X = gmres(A, cB, pX0, P);
      break;
    }
    // case _sor:
    // {
    //   SorSolver sor(omega, tol, iterMax, verboseLevel);
    //   X = sor(A, B, pX0);
    //   break;
    // }
    // case _ssor:
    // {
    //   SsorSolver ssor(omega, tol, iterMax, verboseLevel);
    //   X = ssor(A, B, pX0);
    //   break;
    // }
    default:
      error("solver_NotHandled", words("iterative solver",isType));
      break;
    }
  X.markAsComputed(true);
  X.name(nam);
  trace_p->pop();
  return X;
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P,
                          const std::vector<Parameter>& ps)
{
  trace_p->push("iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const std::vector<Parameter>& ps)");
  number_t maxIt, krylovDim, verboseLevel;
  real_t tol, omega;
  IterativeSolverType solverType;
  string_t name;

  buildSolverParams(ps, tol, maxIt, omega, krylovDim, verboseLevel, name, solverType);
  trace_p->pop();
  return iterativeSolveGen(solverType, A, B, X0, P, tol, maxIt, omega, krylovDim, verboseLevel, name);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1)
{
  std::vector<Parameter> ps(1,p1);
  return iterativeSolve(A, B, X0, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2)
{
  std::vector<Parameter> ps(2);
  ps[0]=p1;
  ps[1]=p2;
  return iterativeSolve(A, B, X0, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps(3);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  return iterativeSolve(A, B, X0, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps(4);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  return iterativeSolve(A, B, X0, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps(5);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  ps[4]=p5;
  return iterativeSolve(A, B, X0, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  std::vector<Parameter> ps(6);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  ps[4]=p5;
  ps[5]=p6;
  return iterativeSolve(A, B, X0, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, Preconditioner& P, const Parameter& p1,
                          const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6,
                          const Parameter& p7)
{
  std::vector<Parameter> ps(7);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  ps[4]=p5;
  ps[5]=p6;
  ps[6]=p7;
  return iterativeSolve(A, B, X0, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1)
{
  std::vector<Parameter> ps(1,p1);
  return iterativeSolve(A, B, theDefaultTermVector, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps(2);
  ps[0]=p1;
  ps[1]=p2;
  return iterativeSolve(A, B, theDefaultTermVector, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3)
{
  std::vector<Parameter> ps(3);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  return iterativeSolve(A, B, theDefaultTermVector, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps(4);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  return iterativeSolve(A, B, theDefaultTermVector, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps(5);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  ps[4]=p5;
  return iterativeSolve(A, B, theDefaultTermVector, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  std::vector<Parameter> ps(6);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  ps[4]=p5;
  ps[5]=p6;
  return iterativeSolve(A, B, theDefaultTermVector, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, Preconditioner& P, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
{
  std::vector<Parameter> ps(7);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  ps[4]=p5;
  ps[5]=p6;
  ps[6]=p7;
  return iterativeSolve(A, B, theDefaultTermVector, P, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1)
{
  std::vector<Parameter> ps(1,p1);
  return iterativeSolve(A, B, X0, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps(2);
  ps[0]=p1;
  ps[1]=p2;
  return iterativeSolve(A, B, X0, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3)
{
  std::vector<Parameter> ps(3);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  return iterativeSolve(A, B, X0, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps(4);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  return iterativeSolve(A, B, X0, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps(5);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  ps[4]=p5;
  return iterativeSolve(A, B, X0, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  std::vector<Parameter> ps(6);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  ps[4]=p5;
  ps[5]=p6;
  return iterativeSolve(A, B, X0, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const TermVector& X0, const Parameter& p1, const Parameter& p2,
                          const Parameter& p3, const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
{
  std::vector<Parameter> ps(7);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  ps[4]=p5;
  ps[5]=p6;
  ps[6]=p7;
  return iterativeSolve(A, B, X0, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1)
{
  std::vector<Parameter> ps(1,p1);
  return iterativeSolve(A, B, theDefaultTermVector, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps(2);
  ps[0]=p1;
  ps[1]=p2;
  return iterativeSolve(A, B, theDefaultTermVector, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps(3);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  return iterativeSolve(A, B, theDefaultTermVector, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                          const Parameter& p4)
{
  std::vector<Parameter> ps(4);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  return iterativeSolve(A, B, theDefaultTermVector, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                          const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps(5);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  ps[4]=p5;
  return iterativeSolve(A, B, theDefaultTermVector, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                          const Parameter& p4, const Parameter& p5, const Parameter& p6)
{
  std::vector<Parameter> ps(6);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  ps[4]=p5;
  ps[5]=p6;
  return iterativeSolve(A, B, theDefaultTermVector, theDefaultPreconditioner, ps);
}

TermVector iterativeSolve(TermMatrix& A, TermVector& B, const Parameter& p1, const Parameter& p2, const Parameter& p3,
                          const Parameter& p4, const Parameter& p5, const Parameter& p6, const Parameter& p7)
{
  std::vector<Parameter> ps(7);
  ps[0]=p1;
  ps[1]=p2;
  ps[2]=p3;
  ps[3]=p4;
  ps[4]=p5;
  ps[5]=p6;
  ps[6]=p7;
  return iterativeSolve(A, B, theDefaultTermVector, theDefaultPreconditioner, ps);
}

/* ================================================================================================
   compute integral representation on dof of Lagrange interpolation on a FE domain:
        lf(u) = intg_gamma op(K)(xi,y) aop op(u) dy
          op(K) : operator on a Kernel K
          op(u) : operator on a Unknown (defined on gamma)
          aop: algebraic operator
   produce a TermMatrix   Rij=intg_gamma op(K)(xi,y) aop op(wj) dy    (wj basis of unknown space)

   syntax example:
        TermMatrix R=integralRepresentation(v,sigma,intg(gamma,G(_y) * u));
   ================================================================================================ */

//loop on points inside FE loop, use SutermVector::computeIR
TermMatrix integralRepresentation(const Unknown& v, const GeomDomain& dom, const LinearForm& lf, string_t nam)
{
  trace_p->push("integralRepresentation(Unknown, GeomDomain, LinearForm)");
  if (lf.isEmpty()) error("is_void","LinearForm");

  //create subspace if necessary
  Space* spacev = v.space();
  if (spacev->domain()!=&dom)
  {
    spacev = Space::findSubSpace(&dom,v.space());
    if(spacev == nullptr) spacev = new Space(dom,*v.space());
  }
  number_t nv=spacev->dimSpace();

  //load points
  std::vector<Point> xs;
  if (spacev->isFE()) xs=dofCoords(*spacev,dom);
  else
  {
    switch(dom.domType())
    {
      case _meshDomain:
        xs = dom.meshDomain()->nodes();
        break;
      case _pointsDomain:
        xs = (static_cast<const PointsDomain*>(&dom))->points;
        break;
      default:
        error("domain_not_handled", dom.domTypeName());
    }
  }

  //get normals at pts if required
  std::vector<Vector<real_t> > ns;
  if (lf.xnormalRequiredByOpKernel()&& (dom.isSideDomain() || dom.dim()!=dom.spaceDim())) ns = computeNormalsAt(dom,xs);

  //create TermMatrix
  TermMatrix R;
  real_t r;
  complex_t c;
  Vector<real_t> rs;
  Vector<complex_t> cs;

  //compute integral representations
  for (cit_mulc it = lf.begin(); it != lf.end(); it++) //loop on linear combinations of lf
  {
    const SuLinearForm& sulf = it->second;
    const Unknown* u=sulf.unknown();
    number_t nu=sulf.space()->dimSpace();
    ValueType vt=sulf.valueType();
    StrucType st=sulf.strucType();
    SuTermMatrix* sut=nullptr;
    MatrixStorage* sto = new RowDenseStorage(nv, nu);
    MatrixEntry* entry = new MatrixEntry(vt, st, sto);
    sut= new SuTermMatrix(u, const_cast<Space*>(sulf.space()), &v, spacev, entry);
    if (st==_scalar)
    {
      if(vt==_real) sut->computeIR(sulf, &entry->rEntries_p->values()[1], r, xs, &ns);
      else sut->computeIR(sulf, &entry->cEntries_p->values()[1], c, xs, &ns);
    }
    else
    {
      error("structure_not_handled", words("structure", st));
      // if(vt==_real)  sut->computeIR(sulf, *entry->rmEntries_p, rs, xs, &ns);
      // else sut->computeIR(sulf, *entry->cmEntries_p, cs, xs, &ns);
    }
    R.insert(sut);
  }
  R.markAsComputed(true);
  R.name(nam);
  trace_p->pop();
  return R;
}

TermMatrix integralRepresentation(const GeomDomain& dom, const LinearForm& lf, string_t nam)
{
  if(dom.domType()!=_meshDomain) error("domain_notmesh", dom.name(), words("domain type", dom.domType()));
  // create a fake space, unknown because required by TermMatrix
  Space* sp=new Space(_dim=dom.meshDomain()->nodes().size(), _domain=dom, _name="ir_space_"+dom.name());
  Unknown* v= new Unknown(*sp, _name="ir_v_"+dom.name()+"_"+tostring(sp));  //name with space adress to have a unique unknown name
  return integralRepresentation(*v, dom, lf, nam);
}

TermMatrix integralRepresentation(const std::vector<Point>& xs, const LinearForm& lf, string_t nam)
{
  // create a fake domain points
  GeomDomain* dom= new PointsDomain(xs,"ir_pointsdomain");
  // create a fake space, unknown because required by TermMatrix
  Space* sp=new Space(_dim=xs.size(), _domain=*dom, _name="ir_space_"+dom->name());
  Unknown* v= new Unknown(*sp, _name="ir_v_"+dom->name()+"_"+tostring(sp));  //name with space adress to have a unique unknown name
  return integralRepresentation(*v, *dom, lf, nam);
}

} // end of namespace xlifepp
