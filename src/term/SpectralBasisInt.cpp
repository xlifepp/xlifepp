/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SpectralBasisInt.cpp
  \author E. Lunéville
  \since 27 mar 2014
  \date 27 mar 2014

  \brief Implementation of xlifepp::SpectralBasisInt class members and related functionnalities
*/

#include "SpectralBasisInt.hpp"

namespace xlifepp
{

SpectralBasisInt::SpectralBasisInt(const std::vector<TermVector>& tvs)
{
  funcFormType_ = _interpolated;

  if(tvs.size()==0)
  {
    where("SpectralBasisInt::SpectralBasisInt");
    error("is_void","std::vector<Termvector>");
  }
  numberOfFun_ = tvs.size();
  functions_.resize(numberOfFun_);
  std::vector<TermVector>::iterator itf=functions_.begin();
  std::vector<TermVector>::const_iterator itv=tvs.begin();
  if(itv->nbOfUnknowns()==0)
  {
    where("SpectralBasisInt::SpectralBasisInt");
    error("is_void","TermVector");
  }
  if(itv->nbOfUnknowns() >1)
  {
    where("SpectralBasisInt::SpectralBasisInt");
    error("term_not_suterm",itv->name());
  }
  SuTermVector* sut=itv->begin()->second;
  const Unknown* u=sut->up();
  const Space* sp=sut->spacep();
  dimFun_ = sut->entries()->nbOfComponents;
  returnedType_ = sut->valueType();
  returnedStruct_= sut->strucType();
  domain_=sut->spacep()->domain();
  *itf = *itv;
  //check consistancy and assign functions
  itv++; itf++;
  for(;itv!=tvs.end();itv++, itf++)
  {
    if(itv->nbOfUnknowns()>1)
    {
      where("SpectralBasisInt::SpectralBasisInt");
      error("term_not_suterm",itv->name());
    }
    sut=itv->begin()->second;
    if(u!=sut->up())
    {
      where("SpectralBasisInt::SpectralBasisInt");
      error("term_mismatch_unknowns",u->name(), sut->up()->name());
    }
    if(sp!=sut->spacep())
    {
      where("SpectralBasisInt::SpectralBasisInt");
      error("term_mismatch_spaces",sp->name(),sut->spacep()->name());
    }
    if(sut->entries()->nbOfComponents != dimFun_)
    {
      where("SpectralBasisInt::SpectralBasisInt");
      error("term_mismatch_nb_components",sut->entries()->nbOfComponents, dimFun_);
    }
    *itf = *itv;  //copy TermVector
  }
}

void SpectralBasisInt::print(std::ostream& os) const
{
    os << message("spectral_int_def",functions_.size());
    if(dimFun_>1) os<<" (vector basis dim = "<<dimFun_<<")";
    if(theVerboseLevel<2) return;
//    std::vector<TermVector>::const_iterator itv=functions_.begin();
//    for(;itv!=functions_.end();itv++) itv->print(os);
}

} // end of namespace xlifepp
