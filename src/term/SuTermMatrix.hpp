/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SuTermMatrix.hpp
  \author E. Lunéville
  \since 23 mar 2012
  \date 13 jan 2014

  \brief Definition of the xlifepp::SuTermMatrix class

  xlifepp::SuTermMatrix class handles numerical representation of a single unknown (couple) bilinear form
  and has the following items:
  - the largest subspace of unknown space involved in bilinear form, possibly the whole space
  - the largest subspace of testfunction space involved in bilinear form, possibly the whole space
  - the list of subspaces of unknown/testfunction spaces involved by each basic bilinear form
  - a list of single unknown/testfunction essential boundary conditions

  xlifepp::SuTermMatrix inherits from xlifepp::Term and it is a piece of xlifepp::TermMatrix

  Global organization looks like:

  \verbatim
      child
  Term  --------> SuTermMatrix
          | SuBilinearForm
          | MatrixEntry *
          | ...
  \endverbatim

  xlifepp::SuTermMatrix is an internal object never managed by end user !

  Note: if required it is possible to go to scalar representation of matrix by using scalar_entries_p
     and renumbering vectors cdofs_u (column) and cdofs_v (row).
     This representation has no interest if all the unknowns involved in xlifepp::SuTermMatrix are scalar !
*/


#ifndef SU_TERM_MATRIX_HPP
#define SU_TERM_MATRIX_HPP

#include "config.h"
#include "Term.hpp"
#include "TermVector.hpp"
#include "SpectralBasisInt.hpp"
#include "form.h"
#include "largeMatrix.h"
#include "computation/termUtils.hpp"
#include "TensorKernel.hpp"
#include "BilinearFormAsLinearForm.hpp"
#include "hierarchicalMatrix.h"

#include "computation/SauterSchwabIM.hpp"
#include "computation/SauterSchwabSymIM.hpp"
#include "computation/DuffyIM.hpp"
#include "computation/LenoirSallesIM.hpp"
#include "computation/CollinoIM.hpp"

namespace xlifepp {

//forward class declaration
class TermMatrix;

/*!
   \class SuTermMatrix
   handles numerical representation
      of single unknowns bilinear forms (SuBilinearForm). Internal class
*/
class SuTermMatrix : public Term
{
  protected:
    SuBilinearForm* subf_p;           //!< single unknown bilinear form (pointer)
    const Unknown* u_p;               //!< pointer to unknown (column)
    const Unknown* v_p;               //!< pointer to test function (line)
    mutable Space* space_u_p;         //!< largest subspace involved in bilinear form for unknown u, may be the whole space
    mutable Space* space_v_p;         //!< largest subspace involved in bilinear form for testfunction v, may be the whole space
    std::vector<Space *> subspaces_u; //!< list of subspaces related to each basic bilinear form's GeomDomain
    std::vector<Space *> subspaces_v; //!< list of subspaces related to each basic bilinear form's GeomDomain
    MatrixEntry* entries_p;           //!< values of SuTermMatrix

    // for scalar representation if required
    MatrixEntry* scalar_entries_p; //!< values of SuTermMatrix in scalar representation, same as entries_p if scalar unknowns
    std::vector<DofComponent> cdofs_u;    //!< scalar numbering of u (in component dofs)
    std::vector<DofComponent> cdofs_v;    //!< scalar numbering of v (in component dofs)
    std::vector<DofComponent> orgcdofs_u; //!< original col scalar numbering when reduced matrix
    std::vector<DofComponent> orgcdofs_v; //!< original row scalar numbering when reduced matrix

    //to deal with constraints
    MatrixEntry* rhs_matrix_p;         //!< pointer to a correction matrix when constraints applied

    //for hierarchical representation
    HMatrixEntry<FeDof>* hm_entries_p; //!< values of SuTermMatrix in a hierarchical matrix representation
    ClusterTree<FeDof>*  cluster_u;    //!< column cluster tree
    ClusterTree<FeDof>*  cluster_v;    //!< row cluster tree
    //! values of scalar form of SuTermMatrix in a hierarchical representation (=hm_entries_p if scalar SuTermMatrix)
    HMatrixEntry<FeDof>* hm_scalar_entries_p;

  public:
    SuTermMatrix(const Unknown*, Space*, const Unknown*, Space*, MatrixEntry*, const string_t& na=""); //!< basic constructor
    SuTermMatrix(SuBilinearForm* subf = nullptr, const string_t& na="", bool noass=false);                   //!< basic constructor
    SuTermMatrix(SuBilinearForm* subf, const string_t& na="", ComputingInfo cp= ComputingInfo());      //!< basic constructor
    SuTermMatrix(SuBilinearForm*, const Unknown*, const Unknown*, Space*, Space*,
          const std::vector<Space *>&, const std::vector<Space *>&, const string_t&, MatrixEntry* =nullptr); //!< full constructor

    SuTermMatrix(const Unknown&, const GeomDomain&, const Unknown&, const GeomDomain&,
          const OperatorOnKernel&, const string_t& na=""); //!< constructor of matrix opker(xi,yj) (Lagrange interpolation)
    SuTermMatrix(SuTermVector &, const string_t& na=""); //!< constructor of a diagonal SuTermMatrix from a SuTermVector
    SuTermMatrix(SuTermVector &, SuTermVector &, const SymbolicFunction& sf, const string_t& na=""); //!< constructor of a diagonal SuTermMatrix from a SuTermVector
    SuTermMatrix(const Unknown*, Space*, const Unknown*, Space*, SuTermVector&, StorageType=_noStorage, AccessType=_noAccess,
          const string_t& na=""); //!< constructor of a diagonal SuTermMatrix from a SuTermVector
    void diagFromSuTermVector(const Unknown*, Space*, const Unknown*,Space*, SuTermVector&, StorageType=_noStorage, AccessType=_noAccess, const string_t& na=""); //!< actual constructor of diagonal SuTermMatrix from a SuTermVector
    void buildFromSuTermVectorPair(const Unknown*, Space*, const Unknown*, Space*, SuTermVector&, SuTermVector&, const SymbolicFunction&, StorageType=_noStorage, AccessType=_noAccess, const string_t& na=""); //!< actual constructor of diagonal SuTermMatrix from a SuTermVector
    SuTermMatrix(const SuTermMatrix&,const string_t& na=""); //!< copy constructor
    SuTermMatrix(const SuTermMatrix&, SpecialMatrix, const complex_t& =1., const string_t& na=""); //!< construct a special matrix from a SuTermMatrix (e.g _IdMatrix)
    template <typename T>
    SuTermMatrix(const Unknown*, const Space*, const Unknown*, const Space*, const std::vector<T>&, number_t, number_t, const string_t& =""); //!< constructor from a row dense matrix

    virtual ~SuTermMatrix();                      //!< destructor
    SuTermMatrix& operator=(const SuTermMatrix&); //!< assign operator
    //! constructor from a linear combination of SuTermMatrix's
    SuTermMatrix(const LcTerm<SuTermMatrix>&, const string_t& na="");
    void copy(const SuTermMatrix&); //!< copy SuTermMatrix in current SuTermMatrix
    void initPointers();            //!< set all pointers to 0
    //! initialize a SuTermVector consistent with matrix rows or columns
    void initSuTermVector(SuTermVector&, ValueType, bool col=true) const;

    //accessors and properties
    string_t name() const { return Term::name(); }
    //! update the name of a SuTermMatrix
    void name(const string_t& nam) { name_=nam; }
    //! return pointer to bilinear form
    SuBilinearForm*& subfp() { return subf_p; }
    //! return _sutermMatrix
    TermType termType() const { return _sutermMatrix; }
    ValueType valueType() const; //!< return value type (_real, _complex)
    StrucType strucType() const; //!< return structure type (_scalar, _matrix)
    //! return the largest space in u involved in SuBilinearForm
    Space& space_u() const { return *space_u_p; }
    //! return the largest space in v involved in SuBilinearForm
    Space& space_v() const { return *space_v_p; }
    //! return the pointer to the largest space in u involved in SuBilinearForm
    Space* space_up() const { return space_u_p; }
    //! return the pointer to the largest space in v involved in SuBilinearForm
    Space* space_vp() const { return space_v_p; }
    //! return the pointer to the largest space in u involved in SuBilinearForm (write)
    Space*& space_up() { return space_u_p; }
    //! return the pointer to the largest space in v involved in SuBilinearForm (write)
    Space*& space_vp() { return space_v_p; }
    //! return pointer to unknown (column)
    const Unknown* up() const { return u_p; }
    //! return pointer to testfunction (line)
    const Unknown* vp() const { return v_p; }
    //! return pointer to unknown (column) (write)
    const Unknown*& up() { return u_p; }
    //! return pointer to testfunction (line) (write)
    const Unknown*& vp() { return v_p; }
    number_t numberOfRows() const;                //!< number of rows (counted in scalar dof)
    number_t numberOfCols() const;                //!< number of columns (counted in scalar dof)
    std::set<const Space*> unknownSpaces() const; //!< list of involved unknown spaces
    //! access to entries pointer (r/w)
    MatrixEntry*& entries() { return entries_p; }
    //! access to entries pointer (r)
    const MatrixEntry* entries() const { return entries_p; }
    //! access to entries pointer (r/w)
    MatrixEntry*& scalar_entries() { return scalar_entries_p; }
    //! access to entries pointer (r)
    const MatrixEntry* scalar_entries() const { return scalar_entries_p; }
    //! access to hmatrix entries pointer (r/w)
    HMatrixEntry<FeDof>*& hm_entries() { return hm_entries_p; }
    //! access to hmatrix entries pointer (r)
    HMatrixEntry<FeDof>* hm_entries() const { return hm_entries_p;}
    //! access to hmatrix entries pointer (r/w)
    HMatrixEntry<FeDof>*& hm_scalar_entries() { return hm_scalar_entries_p; }
    //! access to hmatrix entries pointer (r)
    HMatrixEntry<FeDof>* hm_scalar_entries() const { return hm_scalar_entries_p;}
    //! return the actual pointer to entries (priority to scalar entry)
    MatrixEntry* actual_entries() const;
    //! access to rhs matrix pointer (r/w)
    MatrixEntry*& rhs_matrix() { return rhs_matrix_p; }
    //! access to rhs matrix pointer (r)
    const MatrixEntry* rhs_matrix() const { return rhs_matrix_p; }
    template <typename T>
    LargeMatrix<T>& getLargeMatrix(StrucType str=_undefStrucType) const; //!< get LargeMatrix representation
    template<typename T>
    HMatrix<T,FeDof>& getHMatrix(StrucType str=_undefStrucType) const;   //!< get HMatrix representation if it exists
    //! access to cdofs_u vector (r)
    const std::vector<DofComponent>& cdofsu() const { return cdofs_u; }
    //! access to cdofs_v vector (r)
    const std::vector<DofComponent>& cdofsv() const { return cdofs_v; }
    //! access to cdofs_u vector (r/w)
    std::vector<DofComponent>& cdofsu() { return cdofs_u; }
    //! access to cdofs_v vector (r/w)
    std::vector<DofComponent>& cdofsv() { return cdofs_v; }

    bool hasHierarchicalMatrix() const
    {return (hm_entries_p!=nullptr || hm_scalar_entries_p!=nullptr);}

    number_t nnz() const;                     //!< return the number of non zero (scalar non zero)
    SymType symmetry() const;                 //!< return symmetry of submatrix
    FactorizationType factorization() const;  //!< return factorization type if factorized
    StorageType storageType() const;          //!< return storage type of submatrix (_cs,_skyline,_dense)
    AccessType accessType() const;            //!< return access type of submatrix (_row, _col, _sym, _dual)
    MatrixStorage* storagep() const;          //!< return storage of submatrix (0 if no entries or storage)
    MatrixStorage* scalarStoragep() const;    //!< return scalar storage of submatrix (0 if no scalar entries or storage)
    void setStorage(StorageType, AccessType); //!< set the storage of SuTermMatrix
    void toStorage(StorageType, AccessType);  //!< change the storage of SuTermMatrix (real algorithm)

    //utilities
    void buildSubspaces();                    //!< build the required subspaces and largest subspaces
    void clear();                             //!< deallocate memory used by a SuTermMatrix
    void clearScalar();                       //!< deallocate memory used by scalar part of SuTermMatrix

    //compute facilities
    void compute(); //!< compute SuTermMatrix from bilinear form
    //! compute SuTermMatrix from a linear combination of SuTermMatrix's
    void compute(const LcTerm<SuTermMatrix>&,const string_t& na="");
    template<number_t CM>
    void compute(const std::vector<SuBilinearForm>&, ValueType, StrucType); //!< utility for computing SuTermMatrix
    template<typename T, typename K>
    void computeIR(const SuLinearForm& sulf, T* mat, K& vt, const std::vector<Point>& xs,
            const std::vector<Vector<real_t> >* ns=nullptr); //!< compute integral representation
    //! returns single unknown bilinear forms
    std::vector<SuBilinearForm> getSubfs(const SuBilinearForm&,ComputationType, ValueType&) const;
    //! check if there are some FE subfs with domain restriction
    bool hasDomainRestriction(const std::vector<SuBilinearForm>& subfs) const;
    //! update storage type
    void updateStorageType(const std::vector<SuBilinearForm>&,std::set<number_t>&,std::set<number_t>&,StorageType&) const;
    //! add dense matrix indexes to storage (extension, double intg)
    void addDenseToStorage(const std::vector<SuBilinearForm>&,MatrixStorage*) const;
    //! add dense matrix indexes to storage (FE with domain restriction)
    void addDenseToStorageFE(const std::vector<SuBilinearForm>&, MatrixStorage*) const;
    void addDGToStorage(const std::vector<SuBilinearForm>&, MatrixStorage*&) const; //!< add DG matrix indexes to storage
    void toScalar(bool keepEntries=false); //!< go to scalar representation
    void toVectorUnknown();                //!< go to vector unknown representation if it has component unknown

    // manage unknowns, test functions
    void changeUnknown(const Unknown& newu);      //!< change unknown (col unknown) to an other (be cautious)
    void changeTestFunction(const Unknown& newv); //!< change TestFuncion (row unknown) to an other (be cautious)
    void changeUnknowns(const Unknown& newu, const Unknown& newv); //!< change both first Unknown and TestFuncion to others (be cautious)

    //scalar type moving
    SuTermMatrix& toReal();    //!< go to real representation getting the real part when complex
    SuTermMatrix& toComplex(); //!< go to complex representation
    SuTermMatrix& toImag();    //!< go to real representation getting the imag part when complex
    SuTermMatrix& toConj();    //!< change to conj(U)
    SuTermMatrix& roundToZero(real_t aszero=10*theEpsilon); //!< round to zero all coefficients close to 0 (|.| < aszero)

    //value management
    Value getValue(number_t, number_t) const;           //!< access to matrix coefficient (i,j >= 1)
    Value getScalarValue(number_t, number_t, dimen_t=0, dimen_t=0) const; //!< access to scalar matrix coefficient (i,j >= 1)
    void  setValue(number_t, number_t, const Value&); //!< set value to matrix coefficient (i,j >= 1) in storage
    //! set value to matrix coefficient (i,j >= 1) in storage
    void  setScalarValue(number_t, number_t, const Value&, dimen_t=0, dimen_t=0);
    void  setRow(const Value&, number_t r1, number_t r2); //!< set values of matrix rows r1->r2 (>= 1)
    void  setCol(const Value&, number_t r1, number_t r2); //!< set values of matrix colss c1->c2 (>= 1)
    number_t rowRank(const Dof&) const;                   //!< rank (>=1) in row of a Dof
    number_t rowRank(const DofComponent&) const;          //!< rank (>=1) in row of a DofComponent
    number_t colRank(const Dof&) const;                   //!< rank (>=1) in column of a Dof
    number_t colRank(const DofComponent&) const;          //!< rank (>=1) in column of a DofComponent

    //in/out tools
    void print(std::ostream&) const; //!< print SuTermMatrix
    void print(PrintStream& os) const { print(os.currentStream()); }
    void print(std::ostream&, bool) const; //!< print SuTermMatrix with header option
    void print(PrintStream& os, bool h) const { print(os.currentStream(),h); }
    void printSummary(std::ostream&) const; //!< print SuTermMatrix in brief
    void printSummary(PrintStream& os) const { printSummary(os.currentStream()); }
    void viewStorage(std::ostream&) const; //!< print storage on stream
    void viewStorage(PrintStream& os) const { viewStorage(os.currentStream()); }
    void saveToFile(const string_t& fn, StorageType st, number_t prec=fullPrec, bool encode=false, real_t tol=theTolerance) const; //!< save TermMatrix to file (dense or Matlab format)
    //! save TermMatrix to file (dense or Matlab format)
    void saveToFile(const string_t& fn, StorageType st, bool encode, real_t tol=theTolerance) const
    { saveToFile(fn, st, fullPrec, encode, tol); }

    //various algebraic operations
    template<typename T>
    SuTermMatrix& operator*=(const T&);            //!< SutermMatrix *= t
    template<typename T>
    SuTermMatrix& operator/=(const T&);            //!< SutermMatrix /= t
    SuTermMatrix& operator+=(const SuTermMatrix&); //!< SutermMatrix += SutermMatrix
    SuTermMatrix& operator-=(const SuTermMatrix&); //!< SutermMatrix -= SutermMatrix
    friend SuTermVector operator*(const SuTermMatrix&, const SuTermVector&);
    friend SuTermVector operator*(const SuTermVector&, const SuTermMatrix&);
    friend SuTermMatrix operator*(const SuTermMatrix&, const SuTermMatrix&);
    friend SuTermMatrix* mergeSuTermMatrix(const std::list<SuTermMatrix*>&);

    real_t norm2() const;     //!< Frobenius norm of a SuTermMatrix
    real_t norminfty() const; //!< infinite norm of a SuTermMatrix

    friend class TermMatrix;

  private:
    //! product SuTermMatrix * SuTermVector when scalar_entries
    SuTermVector multMVScalarEntries(const SuTermVector& sutV, bool toVector=false) const;
    //! product SuTermMatrix * SuTermVector when entries
    SuTermVector multMVVectorEntries(const SuTermVector& sutV, bool toScalar=false) const;
    //! product SuTermVector * SuTermMatrix when scalar_entries
    SuTermVector multVMScalarEntries(const SuTermVector& sutV, bool toVector=false) const;
    //! product SuTermVector * SuTermMatrix when entries
    SuTermVector multVMVectorEntries(const SuTermVector& sutV, bool toScalar=false) const;
};

SuTermVector operator*(const SuTermMatrix&, const SuTermVector&); //!< product SuTermMatrix * SuTermVector
SuTermVector operator*(const SuTermVector&, const SuTermMatrix&); //!< product SuTermVector * SuTermMatrix
SuTermMatrix operator*(const SuTermMatrix&, const SuTermMatrix&); //!< product SuTermMatrix * SuTermMatrix
SuTermMatrix* mergeSuTermMatrix(const std::list<SuTermMatrix*>&); //!< merge SuTerMatrix's referring to the same vector unknown

// ------------------------------------------------------------------------------------------------------------------------
// External functions  declaration
// ------------------------------------------------------------------------------------------------------------------------
//! factorize matrix as LU or LDLt or LDL*
void factorize(SuTermMatrix&, SuTermMatrix&, FactorizationType ft=_noFactorization,bool withPermutation=true);
//! factorize matrix as LU or LDLt or LDL*
void factorize(SuTermMatrix&, FactorizationType ft=_noFactorization,bool withPermutation=true);
SuTermVector  factSolve(SuTermMatrix&, const SuTermVector&);                   //!< solve AX=B  when A is factorized
SuTermVectors factSolve(SuTermMatrix&, const std::vector<SuTermVector>&);      //!< solve AXs=Bs  when A is factorized
SuTermMatrix  factSolve(SuTermMatrix&, SuTermMatrix&);                         //!< create inv(A)*B when A is factorized
SuTermVector gaussSolve(SuTermMatrix&, const SuTermVector&, bool keepA=false); //!< solve AX=B using Gauss reduction
//! solve AX=B using umfpack if available
SuTermVector umfpackSolve(SuTermMatrix&, const SuTermVector&, real_t&, bool keepA =false);
SuTermVector umfpackSolve(SuTermMatrix&, const SuTermVector&, bool keepA =false); //!< solve AX=B using umfpack if available

// ------------------------------------------------------------------------------------------------------------------------
// template algebraic operation on SuTermMatrix
// ------------------------------------------------------------------------------------------------------------------------
template<typename T>
SuTermMatrix& SuTermMatrix::operator*=(const T& t)
{
  if (subf_p!=nullptr) *subf_p*=t;
  if (computed()) *entries_p*=t;
  return *this;
}

template<typename T>
SuTermMatrix& SuTermMatrix::operator/=(const T& t)
{
  if (subf_p!=nullptr) *subf_p/=t;
  if (computed()) *entries_p/=t;
  return *this;
}

//! constructor from a row dense matrix (only scalar entry)
template <typename T>
SuTermMatrix::SuTermMatrix(const Unknown* up, const Space* spu, const Unknown* vp, const Space* spv, const std::vector<T>& mat, number_t m, number_t n, const string_t& na)
{
  trace_p->push("SuTermMatrix::SuTermMatrix(matrix)");
  computingInfo_.noAssembly = false;
  termType_ = _sutermMatrix;
  name_ = na;
  subf_p=nullptr;
  initPointers();
  u_p = up; v_p = vp;
  space_u_p=const_cast<Space*>(spu); space_v_p=const_cast<Space*>(spv);
  number_t nbc=space_u_p->dimSpace(), nbr=space_v_p->dimSpace();
  if (nbr!=m || nbc!=n)
    error("free_error","SuTermMatrix from Matrix : inconsistent sizes");
  //find or create row dense storage
  string_t sna="_dense_row_"+tostring(nbr)+"x"+tostring(nbc);
  MatrixStorage* mstorage= findMatrixStorage(sna,_dense,_row);
  if (mstorage==nullptr) mstorage = new RowDenseStorage(nbr, nbc, sna);
  // create matrix entry
  entries_p = new MatrixEntry(xlifepp::valueType(mat[0]), _scalar, mstorage);
  typename std::vector<T>::const_iterator itm=mat.begin();
  for (number_t i=1;i<=nbr;i++)
    for (number_t j=1;j<=nbc;j++, ++itm)
      entries_p->setEntry(i,j,*itm);
  // end of construction
  computingInfo_.isComputed=true;
  trace_p->pop();
}


/*!
  templated computation functions
  ComputationAlgorithm class is a trick to template FE/IE computation algorithm
*/
template <number_t CM>
class ComputationAlgorithm
{
  public:
    //! computation algorithm
    template <typename T, typename K>
    static void compute(const SuBilinearForm& sub, LargeMatrix<T>& mat, K& vt, Space * space_u_p, Space * space_v_p, const Unknown* u_p, const Unknown* v_p)
    { error("not_handled", "compute(const SuBilinearForm&, LargeMatrix<T>&, K&, ...)"); }
};

template<number_t CM>
void SuTermMatrix::compute(const std::vector<SuBilinearForm>& subfs, ValueType vt, StrucType str)
{
  real_t r = 0;
  complex_t c = 0.;
  std::vector<SuBilinearForm>::const_iterator itsubf;
  for (itsubf = subfs.begin(); itsubf != subfs.end(); itsubf++)
  {
    switch (str)
    {
      case _scalar:
        if (vt == _real) ComputationAlgorithm<CM>::compute(*itsubf, *(entries_p->rEntries_p), r, space_u_p, space_v_p, u_p, v_p);
        else ComputationAlgorithm<CM>::compute(*itsubf, *(entries_p->cEntries_p), c, space_u_p, space_v_p, u_p, v_p);
        break;
      case _matrix:
        if (vt == _real) ComputationAlgorithm<CM>::compute(*itsubf, *(entries_p->rmEntries_p), r, space_u_p, space_v_p, u_p, v_p);
        else ComputationAlgorithm<CM>::compute(*itsubf, *(entries_p->cmEntries_p), c, space_u_p, space_v_p, u_p, v_p);
        break;
      default:
        where("SuTerMatrix::compute(Vector<SuBilinearForm, ValueType, StrucType)");
        error("scalar_or_matrix");
    }
  }
}

//!compare SuTermMatrix regarding their numbers of non zero
inline bool compNnzSutermMatrix (SuTermMatrix* sut1, SuTermMatrix* sut2)
{return (sut1->nnz() > sut2->nnz());}

//!compare SuTermMatrix regarding row sizes
inline bool compRowSize (SuTermMatrix* sut1, SuTermMatrix* sut2)
{return (sut1->cdofsv().size() > sut2->cdofsv().size());}

//! compare SuTermMatrix regarding col sizes
inline bool compColSize (SuTermMatrix* sut1, SuTermMatrix* sut2)
{return (sut1->cdofsu().size() > sut2->cdofsu().size());}

/*! get internal matrix (restrictive), i.e LargeMatrix object if available
  if scalar and vector representations both exist, the scalar representation is prior except if StrucType argument is specified
  It returns a reference to the LargeMatrix
*/
template <typename T>
LargeMatrix<T>& SuTermMatrix::getLargeMatrix(StrucType str) const
{
  if (scalar_entries_p!=nullptr && str!=_matrix) return scalar_entries_p->getLargeMatrix<T>();
  if (entries_p!=nullptr) return entries_p->getLargeMatrix<T>();
  where("SuTermMatrix::getLargeMatrix(StrucType)");
  error("term_no_entries");
  return *(new LargeMatrix<T>());  //fake return
}

/*! get HMatrix representation if it exists, it returns a reference to the HMatrix
  if scalar and vector representations both exist, the scalar representation is prior except if StrucType argument is specified
*/
template<typename T>
HMatrix<T,FeDof>& SuTermMatrix::getHMatrix(StrucType str) const
{
  if (hm_scalar_entries_p!=nullptr && str!=_matrix) return hm_scalar_entries_p->getHMatrix<T>();
  if (hm_entries_p!=nullptr) return hm_entries_p->getHMatrix<T>();
  where("SuTermMatrix::getHMatrix(StrucType)");
  error("term_no_entries");
  return *(new HMatrix<T,FeDof>());  //fake return
}

} //end of namespace xlifepp

//include computation algorithms
#include "computation/FEMatrixComputation.hpp"
#include "computation/FEextMatrixComputation.hpp"
#include "computation/SPMatrixComputation.hpp"
#include "computation/FESPMatrixComputation.hpp"
#include "computation/IESPMatrixComputation.hpp"
#include "computation/IEMatrixComputation.hpp"
#include "computation/IEHMatrixComputation.hpp"
#include "computation/DGMatrixComputation.hpp"

#endif /* SU_TERM_MATRIX_HPP */
