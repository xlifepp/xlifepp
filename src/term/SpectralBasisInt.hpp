/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SpectralBasisInt.hpp
  \author E. Lunéville
  \since 27 mars 2014
  \date 27 mars 2014

  \brief Definition of the xlifepp::SpectralBasisInt class

  xlifepp::SpectralBasisInt class deals with basis functions defined as interpolated functions on approximation space
  : say a collection of xlifepp::TermVector's.
  xlifepp::TermVector's have to be single unknown xlifepp::TermVector with same unknown and must have same space support
  It is check at construction

  xlifepp::SpectralBasisInt class inherits from xlifepp::SpectralBasis
  and it is located in term library in order to avoid reverse library dependence
*/

#ifndef SPECTRAL_BASIS_INT_HPP
#define SPECTRAL_BASIS_INT_HPP

#include "config.h"
#include "utils.h"
#include "space.h"
#include "TermVector.hpp"

namespace xlifepp
{

/*!
  \class SpectralBasisInt
  defines global basis functions given by interpolated functions, say TermVectors
 */
class SpectralBasisInt : public SpectralBasis
{
  protected:
    std::vector<TermVector> functions_; //!< list of interpolated basis functions
  public:
    //constructor and related stuff
    SpectralBasisInt(const std::vector<TermVector>&);      //!< constructor from consistent TermVectors (same (sub)space, same unknown)
    virtual SpectralBasisInt* clone() const                //! clone of current returned as pointer (covariant)
    {return new SpectralBasisInt(*this);}

    //!accessors and shortcuts to properties
    //! access to basis
    std::vector<TermVector>& basis()                       
    {return functions_;}
    //! access to basis
    virtual const std::vector<TermVector>& basis() const   
    {return functions_;}
    //! access to common unknown of TermVectors
    const Unknown* unknown() const                         
    {return *(functions_[0].unknowns().begin());}
    template<typename T>
    T& function(number_t, const std::vector<real_t>&, T&, const Element* elt=nullptr) const;        //!< compute n-th function
    template<typename T>
    Vector<T>& functions(const std::vector<real_t>&, Vector<T>&, const Element* elt=nullptr) const;  //!< compute all basis functions
    template<typename T>
    T& evaluateT(const std::vector<real_t>&, const Vector<T>&, T&) const; //!< evaluate linear combination of basis functions
    virtual real_t& evaluate(const std::vector<real_t>& p, const Vector<real_t>& a, real_t& v) const
    {return evaluateT(p,a,v);} //!< evaluate linear combination of real basis functions
    virtual complex_t& evaluate(const std::vector<real_t>& p, const Vector<complex_t>& a, complex_t& v) const
    {return evaluateT(p,a,v);} //!< evaluate linear combination of complex basis functions
    //@{
    //! printing utility
    virtual void print(std::ostream&) const;               
    virtual void print(PrintStream& os) const {print(os.currentStream());}
    //@}
};

/*! evaluation of basis functions using interpolation
general version locating element belonging point (quite expansive) */
template<typename T>
T& SpectralBasisInt::function(number_t n, const std::vector<real_t>& P, T& res, const Element* elt) const // compute the n-th function (n>=1)
{
  SuTermVector* stv=functions_[n-1].begin()->second;
  Space* sp=stv->spacep();
  number_t k=0;
  if(elt==nullptr) //locate element
    {
      const MeshDomain* mdom=sp->domain()->meshDomain();
      if(mdom==nullptr)
        {
          where("SpectralBasisInt::function<T>(Number, const Reals&, T&)");
          error("domain_notmesh",sp->domain()->name(), words("domain type",sp->domain()->domType()));
        }

      GeomElement* gelt = mdom->locate(P);
      if(gelt==nullptr)
        {
          where("SpectralBasisInt::function(Number, const Reals&, T&)");
          error("geoelt_not_found");
        }
      k=sp->numElement(gelt);
      elt=sp->element_p(k);
    }
    else //retry dof numbering of elt
    {
        k = elt->number();
        if(elt!=sp->element_p(k)) k=sp->numElement(elt->geomElt_p); //non trivial elt number -> space elements rank
    }                                                               //be care in multithread, call buildgelt2elt
  return elt->interpolate(*stv->entries(),P,sp->elementDofs(k),res);
}

/* compute at a point the values of all spectral functions given by interpolation (say TermVector's)
  P: point where to evaluate
  res: vector of phi_n(P)
  elt: pointer to the element belonging P, if null elt will be found by this function using time expansive locate process */
template<typename T>
Vector<T>& SpectralBasisInt::functions(const std::vector<real_t>& P, Vector<T>& res, const Element* elt) const // compute all basis functions
{
  std::vector<TermVector>::const_iterator itv=functions_.begin();
  SuTermVector* stv=itv->begin()->second;
  Space* sp=stv->spacep();
  number_t k=0;
  if(elt==nullptr) //locate element
    {
      const MeshDomain* mdom=sp->domain()->meshDomain();
      if(mdom==nullptr)
        {
          where("SpectralBasisInt::functions<T>(const Reals&, Vector<T>&)");
          error("domain_notmesh",sp->domain()->name(), words("domain type",sp->domain()->domType()));
        }
      GeomElement* gelt = mdom->locate(P);
      if(gelt==nullptr)
        {
          where("SpectralBasisInt::functions<T>(const Reals&, Vector<T>&)");
          error("geoelt_not_found");
        }
      k=sp->numElement(gelt); //be care in multithread, call buildgelt2elt
      elt=sp->element_p(k);
    }
    else //retry dof numbering of elt
    {
        k = elt->number();
        if(elt!=sp->element_p(k)) k=sp->numElement(elt->geomElt_p); //non trivial elt number -> space elements rank
    }                                                               //be care in multithread, call buildgelt2elt
  //evaluate interpolated functions
  res.resize(functions_.size());
  typename Vector<T>::iterator itr=res.begin();
  for(; itv!=functions_.end(); itv++,itr++)
    {
      stv=itv->begin()->second;
      elt->interpolate(*stv->entries(),P, sp->elementDofs(k),*itr);
    }
  return res;
}

/* evaluate linear combination of spectral functions, say v = sum_k a_k f_k(P)
currently works only for scalars of same value type  */
template<typename T>
T& SpectralBasisInt::evaluateT(const std::vector<real_t>& P, const Vector<T>& a, T& v) const
{
  Vector<T> fs;
  functions(P,fs);
  typename Vector<T>::const_iterator ita=a.begin();
  typename Vector<T>::iterator itf=fs.begin();
  for(; ita!=a.end() && itf!=fs.end(); ita++, itf++) v+= *ita * *itf;
  return v;
}

} // end of namespace xlifepp

#endif // SPECTRAL_BASIS_HPP
