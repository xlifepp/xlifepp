/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Collino.hpp
  \authors F. Collino, E. Lunéville
  \since 6 sep 2018
  \date  6 sep 2018

  \brief header file of Collino's stuff
 */

#ifndef COLLINO_HPP
#define COLLINO_HPP

#include "utils.h"

#ifndef DOXYGEN_SHOULD_SKIP_THIS

namespace xlifepp
{
//imported Collino stuff
#define ERR -1
#define  mmin(a,b)  ((a) < (b) ? (a) : (b))
#define  mmax(a,b)  ((a) > (b) ? (a) : (b))
#define  mymalloc(ptr, nr, type) \
           if (!(ptr = (type*)malloc((mmax(nr,1)) * sizeof(type)))) \
            { printf("malloc failed on line %d of file %s (nr=%lu)\n", \
                     __LINE__, __FILE__, nr); \
              exit(ERR); \
            }

#define myrealloc(ptr, nr, type) \
           if (!(ptr = (type*)realloc(ptr, (nr) * sizeof(type)))) \
            { printf("realloc failed on line %d of file %s (nr=%lu)\n", \
                     __LINE__, __FILE__, nr); \
              exit(ERR); \
            }


struct Myquad_t
{
  number_t ng        ;/* nombre de points de Gauss sur triangle (regulier)     */
  real_t*  w         ;/* poids[]                                  */
  real_t*  a         ;/* x[i] = a[i]*S_1 +  b[i]*S_2 + c[i}*S_3   */
  real_t*  b         ;/* c[i] = 1- a[i] - b[i]                    */
  real_t  eta        ;/* permet de discriminer les singuliers des r�guliers */
  number_t ngs       ;/* nombre de points de Gauss sur triangle (singulier)     */
  real_t*  ws        ;/* poids[]                                  */
  real_t*  as        ;/* x[i] = as[i]*S_1 +  bs[i]*S_2 + cs[i}*S_3*/
  real_t*  bs        ;/* c[i] = 1- a[i] - b[i]                    */
  number_t ngo       ;/*  nombre de points de Gauss sur segment (singulier)     */
  real_t*  wo        ;/* poids[]                                 */
  real_t*  ao        ;/* x[i] = ao[i]*S_1 +  (1-ao[i])*S_2        */
  bool     use_demcem ;
  number_t np_demcem_v;/* nb de pts pour les intégrales 1d de demcem (vertex) */
  number_t np_demcem_e;/* nb de pts pour les intégrales 1d de demcem (edge)   */
  number_t np_demcem_t;/* nb de pts pour les intégrales 1d de demcem (triangle)*/
};

struct Mypermut_t
{
  number_t sk[3] ;
  number_t sl[3] ;
};

struct Mytriangle_t
{
  real_t vertex[3][3];
  real_t    tau[3][3];
  real_t     nu[3][3];
  real_t normal[3];
  real_t  ledge[3];
  real_t  hedge[3];
  real_t   area;
  real_t   diam;
};

struct Myct_t
{
  real_t  h   ;  /* coordonnee selon la normale au triangle     */
  real_t xt[3];  /* le vecteur x - sa composante normale     */
  real_t  x[3];  /* le vecteur x                             */
  real_t  d[3];  /* coordonnee selon les normales aux aretes */
  real_t t1[3];  /* coordonnee du sommet 1, 2 en choisissant */
  real_t t2[3];  /* la coordonnee selon l'arete comme origine*/
};

void free_quad(Myquad_t* quad);
void ElemTools_weakstrong_c(real_t ck[3][3], real_t cl[3][3],real_t ka[2], Myquad_t* quad, complex_t zt[3][3], complex_t zk[3][3], number_t cf=1);
void ElemTools_singular(real_t ck[3][3],real_t cl[3][3], real_t ka[2], Myquad_t* quad, complex_t zt[3][3], complex_t zk[3][3], number_t cf=1);
Myquad_t* elem_quad(number_t ngtr, number_t ngts, number_t ngso, real_t eta,
                    number_t demcem_nt, number_t demcem_ne, number_t demcem_nv, bool use_demcem);
void ElemTools_stroud_c(number_t n, real_t* Qa, real_t* Qb, real_t* Qw);
void ElemTools_legset(number_t  norder, real_t* xtab,  real_t* weight);
void E_lenoir_triangle(real_t ck[3][3], real_t ka[2], Myquad_t* quad, complex_t zts[3][3], complex_t zks[3][3], number_t cf=1);
void E_lenoir_edge(real_t ck[3][3],real_t cl[3][3],real_t ka[2],Myquad_t* quad, complex_t zts[3][3],complex_t zks[3][3], number_t cf=1);
void E_lenoir_vertex(real_t ck[3][3],real_t cl[3][3],real_t ka[2], Myquad_t* quad, complex_t zts[3][3], complex_t zks[3][3], number_t cf=1);
Myct_t* ElemTools_triangle_x(real_t x[3], Mytriangle_t* triangle);
real_t solid_angle(real_t xl[3], real_t ck[3][3]);
Mytriangle_t* ElemTools_triangle_c(real_t vertex[3][3]);
void ElemTools_hanninen_step23(real_t ka[2], real_t ck[3][3], real_t cl[3][3], real_t  s[3], real_t si[7], real_t sj[8][3],
                               complex_t zts[3][3], complex_t zks[3][3], number_t cf=1);
void ElemTools_hanninen_t(Mytriangle_t* triangle, real_t x[3], real_t integrale[12]);
void ElemTools_strong_sinGauss_kareal(real_t ck[3][3], real_t cl[3][3], real_t ka, Myquad_t* quad, real_t zksin[3][3]);
void ElemTools_strong_Gauss_kareal(real_t ck[3][3], real_t cl[3][3], real_t ka, Myquad_t* quad, complex_t zt[3][3],complex_t zk[3][3], number_t cf=1);
void ElemTools_strong_Gauss_kacmplx(real_t ck[3][3], real_t cl[3][3], real_t ka[2], Myquad_t* quad, complex_t zt[3][3],complex_t zk[3][3], number_t cf=1);
void ElemTools_elem_rc(real_t ck[3][3],   real_t cl[3][3], real_t kat[2],    Myquad_t* quad, complex_t zt[3][3],complex_t zk[3][3], number_t cf=1);
void ElemTools_elem_r(real_t ck[3][3],real_t cl[3][3], real_t ka, Myquad_t* quad, complex_t zt[3][3],complex_t zk[3][3], number_t cf=1);

}

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

#endif // COLLINO_HPP
