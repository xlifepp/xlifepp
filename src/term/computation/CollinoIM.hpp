/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file CollinoIM.hpp
  \authors S.Roman and N.Salles, E. Lunéville
  \since 6 sep 2018
  \date  6 sep 2018

  \brief Implementation of the Collino method to compute integrals involved in Maxwell BEM
 */


#ifndef COLLINO_IM_HPP
#define COLLINO_IM_HPP

#include "finiteElements.h"
#include "utils.h"
#include "config.h"
#include "termUtils.hpp"
#include "Collino.hpp"

namespace xlifepp
{

/*!
  \class CollinoIM
  integral over a product of triangles for Maxwell IE using a method developped by F. Collino
  compute for Raviart-Thomas basis of order 1 (RT0!) the following integrals
            I1 = intg_SxT k*G(x,y)[ wi(x).wj(y)-1/k2 div(wi(x)div(wj(y) ]
            I2 = intg_SxT (grad_y G(x,y) x wj(y)).wi(x)
*/

enum ComputeIntgFlag {_computeI1=1, _computeI2,_computeBoth};

class CollinoIM : public DoubleIM
{
private:
  Myquad_t* quads_;            //!< quadratures structure
public:
  number_t ordSNear;           //!< order of quadrature for a segment  (near case, default 12)
  number_t ordTNear;           //!< order of quadrature for a triangle (near case, default 64)
  number_t ordTFar;            //!< order of quadrature for far triangles (default 3)
  real_t eta;                  //!< parameter in element distance criteria
  ComputeIntgFlag computeFlag; //!< tells what integrals have to be computed (default compute I1)

  CollinoIM() //! default constructor
  {
    ordTFar=3; ordSNear=12; ordTNear=64; eta=3;
    quads_= elem_quad(ordTFar, ordTNear, ordSNear, eta, 0,0,0,0);
    name="Collino_"+tostring(ordTFar)+"_"+tostring(ordSNear)+"_"+tostring(ordTNear);
    imType=_CollinoIM; singularType=_r_; singularOrder=-1;
    computeFlag=_computeI1;
  }

  CollinoIM(ComputeIntgFlag cf, number_t otf, number_t otn, number_t osn, real_t e) //! full constructor
  {
    ordTFar=otf; ordSNear=osn; ordTNear=otn; eta=e;
    quads_ = elem_quad(ordTFar, ordTNear, ordSNear, eta, 0,0,0,0);
    name = "Collino_"+tostring(ordTFar)+"_"+tostring(ordSNear)+"_"+tostring(ordTNear);
    imType = _CollinoIM; singularType=_r_; singularOrder=-1;
    computeFlag=cf;
  }

  CollinoIM(const CollinoIM& cim) //! copy constructor
  {
    ordTFar=cim.ordTFar; ordSNear=cim.ordSNear; ordTNear=cim.ordTNear; eta=cim.eta;
    quads_ = elem_quad(ordTFar, ordTNear, ordSNear, eta, 0,0,0,0);
    name=cim.name;
    imType=_CollinoIM; singularType=_r_; singularOrder=-1;
    computeFlag=cim.computeFlag;
  }

  ~CollinoIM() //! destructor
  {
    if(quads_!=nullptr)
      {
        free_quad(quads_);
        delete quads_;
      }
  }

  virtual CollinoIM* clone() const
  {return new CollinoIM(*this);}

  CollinoIM& operator=(const CollinoIM& cim) //! assign operator
  {
    if(this==&cim) return *this;
    ordTFar=cim.ordTFar; ordSNear=cim.ordSNear; ordTNear=cim.ordTNear; eta=cim.eta;
    if(quads_!=nullptr) delete quads_;
    quads_=elem_quad(ordTFar, ordTNear, ordSNear, eta, 0,0,0,0);
    computeFlag=cim.computeFlag;
    return *this;
  }

  virtual std::list<Quadrature*> quadratures() const  //! return the list of (single) quadratures in a list
  {
    std::list<Quadrature*> lsq;
    return lsq;
  }

  virtual void print(std::ostream& os) const          //! print IntegrationMethod on stream
  {
    os<<"Collino integration method with ordTFar="<<ordTFar <<" ordTNear="<<ordTNear<<" ordSNear="<<ordSNear<<" eta="<<eta<<eol;
  }
  virtual void print(PrintStream& os) const {print(os.currentStream());}

  //template computeIE function, explicit instantiation because template functions are not inherited
  template<typename K>
  void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                 const KernelOperatorOnUnknowns& kuv, Matrix<K>& res,IEcomputationParameters& ieparams,
                 Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk) const;
  void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                 const KernelOperatorOnUnknowns& kuv, Matrix<real_t>& res,IEcomputationParameters& ieparams,
                 Vector<real_t>& val_opu, Vector<real_t>& val_opv, Vector<real_t>& val_opk) const
  {computeIE<>(elt_S, elt_T,adj, kuv, res,ieparams, val_opu, val_opv, val_opk);}
  void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                 const KernelOperatorOnUnknowns& kuv, Matrix<complex_t>& res,IEcomputationParameters& ieparams,
                 Vector<complex_t>& val_opu, Vector<complex_t>& val_opv, Vector<complex_t>& val_opk) const
  {computeIE<>(elt_S, elt_T,adj, kuv, res,ieparams, val_opu, val_opv, val_opk);}
};

// implementation of template member functions
template<typename K>
void CollinoIM::computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                          const KernelOperatorOnUnknowns& kuv, Matrix<K>& res, IEcomputationParameters& ieparams,
                          Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk) const
{
  if(ieparams.sh_u!=_triangle)
    error("shape_not_handled", words("shape", ieparams.sh_u));
  if(ieparams.sh_v!=_triangle)
    error("shape_not_handled", words("shape", ieparams.sh_v));

  //load normals
  Vector<real_t>* nx=nullptr, *ny=nullptr;
  MeshElement* melt_S=ieparams.melt_u, *melt_T=ieparams.melt_v;
  if(kuv.xnormalRequired()) nx = &melt_T->geomMapData_p->normalVector;
  if(kuv.ynormalRequired()) ny = &melt_S->geomMapData_p->normalVector;

  //wrap to Collino's entry function
  complex_t zt[3][3], zk[3][3];
  real_t ck[3][3], cl[3][3];
  for(number_t k=0; k<3; k++)
    for(number_t i=0; i<3; i++)
      {
        ck[k][i]= (*melt_T->nodes[k])[i];
        cl[k][i]= (*melt_S->nodes[k])[i];
      }
  const Kernel* ker=kuv.kernel();
  complex_t kc= ker->userData("k");
  real_t *ka = reinterpret_cast<real_t*>(&kc);
  number_t cf = number_t(computeFlag);
  ElemTools_weakstrong_c(ck, cl, ka, quads_, zt, zk, cf);

  //change signs
  bool chs_u= ieparams.changeSign_u, chs_v= ieparams.changeSign_v, chs=chs_u || chs_v;
  Vector<real_t>& sign_u=*ieparams.sign_u;
  Vector<real_t>& sign_v=*ieparams.sign_v;
  real_t sv=1, su=1;

  //fill res
  typename Matrix<K>::iterator itr=res.begin();
  if(computeFlag ==_computeI1)
    {
      for(number_t i=0; i<3; i++)
        {
          if(chs_v) sv=sign_v[i];
          for(number_t j=0; j<3; j++, ++itr)
            {
              if(chs_u) su=sign_u[j];
              if(chs && su*sv==-1.) *itr-= complexToT<K>(zt[i][j]);
              else *itr+= complexToT<K>(zt[i][j]);
            }
        }
    }
  if(computeFlag ==_computeI2)
    {
      for(number_t i=0; i<3; i++)
        {
          if(chs_v) sv=sign_v[i];
          for(number_t j=0; j<3; j++, ++itr)
            {
              if(chs_u) su=sign_u[i];
              if(chs && su*sv==-1.) *itr-= complexToT<K>(zk[i][j]);
              else *itr+= complexToT<K>(zk[i][j]);
            }
        }
    }
}

}
#endif // COLLINO_IM_HPP
