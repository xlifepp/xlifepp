/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file FEextMatrixComputation.hpp
  \author E. Lunéville
  \since 6 jan 2015
  \date 6 jan 2015

  \brief Implementation of template FE TermMatrix computation functions when domain extension is required

  this file is included by TermMatrix.hpp
  do not include it elsewhere
*/

/* ================================================================================================
                            FE computation algorithm with domain extension
   ================================================================================================
   address boundary terms involving non tangential derivatives, for instance   intg_gamma dx(u)*v
   in that case as dofs not located on boundary may have some effects,
   all dofs located in extension of side domain (say elements having an edge on side domain) have to be taken into account

   u space and v space have to be FE spaces
   subf: a single unknown bilinear form defined on a unique side domain (assumed)
   mat: reference to matrix entries of type T
   vt: to pass as template the scalar type (real or complex)
   space_u_p: largest unknown space in computation
   space_v_p: largest test space in computation
   u_p: unknown (col)
   v_p: test function (row)

current version is NOT optimized (recomputation of shape values)
*/

namespace xlifepp
{

template <>
template <typename T, typename K>
void ComputationAlgorithm<_FEextComputation>::compute(const SuBilinearForm& subf, LargeMatrix<T>& mat, K& vt,
    Space* space_u_p, Space* space_v_p, const Unknown* u_p, const TestFct* v_p)
{
  if(subf.size() == 0) return; // void bilinear form, nothing to compute !!!
  trace_p->push("SuTermMatrix::computeFEext");
  if(theVerboseLevel>0) std::cout<<"computing FE extended term "<< subf.asString()<<", using "<<numberOfThreads()<<" threads: "<<std::flush;

  cit_vbfp itf = subf.begin();
  const GeomDomain* dom = itf->first->asIntgForm()->domain(); //common domain of integrals
  const MeshDomain* mdom = dom->meshDomain();

  //required extension and max derivative orders
  dimen_t ordu = 0, ordv = 0;
  const GeomDomain* extend_u=nullptr, *extend_v=nullptr;
  bool normalRequired=false;
  for(; itf != subf.end(); ++itf)
    {
      const IntgBilinearForm* ibf = itf->first->asIntgForm(); //basic bilinear form as simple integral
      const OperatorOnUnknown& op_u = ibf->opu();             //operator on unknown
      const OperatorOnUnknown& op_v = ibf->opv();             //operator on test function
      ordu = std::max(ordu, dimen_t(op_u.diffOrder()));       //order of u differential involved in operators
      ordv = std::max(ordv, dimen_t(op_v.diffOrder()));       //order of v differential involved in operators
      setDomain(const_cast<GeomDomain*>(dom));                //transmit domain pointer to thread data
      if(op_u.normalRequired() || op_v.normalRequired())  normalRequired=true;
      if(extend_u==nullptr) extend_u = ibf->extDom_up();
      if(extend_v==nullptr) extend_v = ibf->extDom_vp();
    }
  //update normal
  if(normalRequired && !dom->meshDomain()->orientationComputed) dom->meshDomain()->setNormalOrientation();

  //retry boundary spaces
  const Space* sp_u = subf.up()->space();  //space of unknown
  const Space* sp_v = subf.vp()->space();  //space of test function
  if(sp_u->typeOfSpace() != _feSpace) error("not_fe_space_type", sp_u->name(), sp_u->typeOfSpace());
  if(sp_v->typeOfSpace() != _feSpace) error("not_fe_space_type", sp_v->name(), sp_v->typeOfSpace());
  const Space* subsp_u=nullptr;
  if(extend_u!=nullptr) subsp_u = Space::findSubSpace(extend_u, space_u_p); //find subspace (linked to domain dom_ext) of space_u_p
  else subsp_u=Space::findSubSpace(dom, space_u_p);  //find subspace (linked to domain dom) of space_u_p
  if(subsp_u == nullptr) subsp_u = space_u_p;         //is a FESpace, not a FESubspace
  const Space* subsp_v=nullptr;
  if(extend_v!=nullptr) subsp_v = Space::findSubSpace(extend_v, space_v_p); //find subspace (linked to domain dom_ext) of space_v_p
  else subsp_v=Space::findSubSpace(dom, space_v_p);  //find subspace (linked to domain dom) of space_b_p
  if(subsp_v == nullptr) subsp_v = space_v_p;         //is a FESpace, not a FESubspace
  bool same_interpolation = (subsp_u == subsp_v);

  //retry dof information
  bool doflag_u = (subsp_u == space_u_p), doflag_v = (subsp_v == space_v_p);
  if(!doflag_u) space_u_p->builddofid2rank();              //build map dofid_u->rank
  if(!doflag_v) space_v_p->builddofid2rank();              //build map dofid_v->rank
  dimen_t dimfun_u = sp_u->dimFun(), dimfun_v = sp_v->dimFun();
  dimen_t nbc_u = u_p->nbOfComponents(), nbc_v = v_p->nbOfComponents(); //number of components of unknown
  if(nbc_u > 1) dimfun_u = nbc_u;                                       //case of vector dofs (scalar shape functions will be extended to vector shape functions)
  if(nbc_v > 1) dimfun_v = nbc_v;                                       //case of vector dofs (scalar shape functions will be extended to vector shape functions)

  //retry FE information
  FEMapType femt_u=subsp_u->element_p(number_t(0))->refElt_p->mapType,
            femt_v=subsp_v->element_p(number_t(0))->refElt_p->mapType;
  bool invertJacobian =(femt_u==_covariantPiolaMap || femt_v==_covariantPiolaMap);
  bool same_shv = (same_interpolation && nbc_v==nbc_u);
  bool sym = same_shv && !(mat.sym == _noSymmetry);
  number_t nbelt = dom->numberOfElements();

  //extension type
  DomainExtensionType extype_u=_noExtension, extype_v=_noExtension;
  if(extend_u!=nullptr) extype_u=extend_u->meshDomain()->extensionType;
  if(extend_v!=nullptr) extype_v=extend_v->meshDomain()->extensionType;

  // load map dom->extended domain (if exists)
  const Function* map_u=nullptr, *map_v=nullptr;
  if(extype_u == _ficticiousExtension) map_u=findMap(*dom,*extend_u->meshDomain()->extensionin_p);
  if(extype_v == _ficticiousExtension) map_v=findMap(*dom,*extend_v->meshDomain()->extensionin_p);

  //print status
  number_t nbeltdiv10 = nbelt/10;
  bool show_status = (theVerboseLevel > 0 && mat.nbRows > 1000 &&  nbeltdiv10 > 1);

  //temporary vector
  std::vector<number_t> dofNum_u, dofNum_v;
  Vector<real_t>* sign_u=nullptr, *sign_v=nullptr;
  std::vector<ShapeValues> shv_us, shv_vs;

//  #ifdef XLIFEPP_WITH_OMP
//  #pragma omp parallel for firstprivate(shv_us, shv_vs, sign_u, sign_v, dofNum_u, dofNum_v)
//  #endif // XLIFEPP_WITH_OMP   NOT THREAD SAFE
  //main loop on elements of side domain
  //====================================
  for(number_t k = 0; k < nbelt; k++)
    {
      GeomElement* gelt = mdom->geomElements[k];        //geometric side element
//      thePrintStream<<"compute on side element: "<<*gelt<<eol<<std::flush;
      ShapeType sh = gelt->shapeType();                 //shape type of side element
      MeshElement* melt = gelt->meshElement();
      if(melt == nullptr) melt = gelt->buildSideMeshElement(); //it is a side element with no meshElement structure, build one
      GeomMapData mapdata(melt);    //structure to store jacobian

      std::list<GeoNumPair> *sideToExt_u, *sideToExt_v;
      std::list<GeoNumPair>::iterator itlu, itlv;
      if(extend_u==nullptr) sideToExt_u = new std::list<GeoNumPair>(1,GeoNumPair(gelt,0)); // no extension for u, use side element
      else sideToExt_u = &extend_u->meshDomain()->sideToExt[gelt];
      if(extend_v==nullptr) sideToExt_v = new std::list<GeoNumPair>(1,GeoNumPair(gelt,0)); // no extension for v, use side element
      else sideToExt_v = &extend_v->meshDomain()->sideToExt[gelt];

      for(itlu=sideToExt_u->begin(); itlu!=sideToExt_u->end(); ++itlu)
        {
          GeomElement* gelt_u = itlu->first;
          const Element* elt_u = subsp_u->element_p(gelt_u);
          RefElement* relt_u = elt_u->refElt_p;
          if(doflag_u) dofNum_u = subsp_u->elementDofs(subsp_u->numElement(gelt_u)); //dof numbers (local numbering related to subsp_u=space_u_p)
          else ranks(space_u_p->dofid2rank(), elt_u->dofNumbers, dofNum_u);        //use ranks function with map dofid2rank (faster)
          number_t nb_u = dofNum_u.size();
          number_t side_u = itlu->second;

          for(itlv=sideToExt_v->begin(); itlv!=sideToExt_v->end(); ++itlv)
            {
              GeomElement* gelt_v = itlv->first;
              const Element* elt_v = subsp_v->element_p(gelt_v);
              RefElement* relt_v = elt_v->refElt_p;
              if(doflag_v) dofNum_v = subsp_v->elementDofs(subsp_v->numElement(gelt_v)); //dof numbers (local numbering related to subsp_u=space_u_p)
              else ranks(space_v_p->dofid2rank(), elt_v->dofNumbers, dofNum_v);        //use ranks function with map dofid2rank (faster)
              number_t nb_v = dofNum_v.size();
              number_t side_v = itlv->second;

              // local to global numbering matrix
              std::vector<number_t> adrs(nb_v * nb_u, 0);
              mat.positions(dofNum_v, dofNum_u, adrs, true); //adresses in mat of elementary matrix stored as a row vector
              std::vector<number_t>::iterator itadrs = adrs.begin(), itk;
              number_t nb_ut = nb_u * nbc_u, nb_vt = nb_v* nbc_v;

              //sign correction for some element (div or rot element)
              bool changeSign_u=false, changeSign_v=false;
              if(relt_u->dofCompatibility == _signDofCompatibility)
                {
                  sign_u=&elt_u->getDofSigns();
                  changeSign_u = sign_u->size()!=0;
                }
              if(same_shv)
                {
                  changeSign_v=changeSign_u;
                  sign_v=sign_u;
                }
              else if(relt_v->dofCompatibility == _signDofCompatibility)
                {
                  sign_v=&elt_v->getDofSigns();
                  changeSign_v = sign_v->size()!=0;
                }

              // dof rotation flag
              bool rotsh_u=elt_u->refElt_p->rotateDof;
              bool rotsh_v=elt_v->refElt_p->rotateDof;

              //loop on basic bilinear forms
              for(cit_vbfp itf = subf.begin(); itf != subf.end(); itf++)
                {
                  const IntgBilinearForm* ibf = itf->first->asIntgForm(); //basic bilinear form as simple integral
                  const OperatorOnUnknown& op_u = ibf->opu();             //operator on unknown
                  const OperatorOnUnknown& op_v = ibf->opv();             //operator on test function
                  if(op_u.elementRequired() || op_v.elementRequired()) setElement(gelt);  // transmit gelt to thread data
                  number_t ord_opu = op_u.diffOrder();                    //derivative order of op_u
                  number_t ord_opv = op_v.diffOrder();                    //derivative order of op_v
                  bool mapsh_u = (femt_u!=_standardMap || ord_opu>0);
                  bool mapsh_v = (femt_v!=_standardMap || ord_opv>0);
                  AlgebraicOperator aop = ibf->algop();                   //algebraic operator between differential operator
                  K coef = complexToT<K>(itf->second);                    //coefficient of linear form
                  const QuadratureIM* qim = dynamic_cast<const QuadratureIM*>(ibf->intgMethod());
                  const Quadrature* quad = qim->getQuadrature(sh);        //quadrature rule pointer attached to integral
                  Vector<real_t>* np=nullptr;                                   //pointer to normal

                  //geometric stuff
                  bool invJacobian = invertJacobian || op_u.diffOrder() > 0  || op_v.diffOrder() > 0;
                  bool upmapdata = true;
                  if(melt->linearMap) //compute geometric data only once if geom elt map is linear
                    {
                      mapdata.computeJacobianMatrix(quad->point(0));       //compute jacobian
                      mapdata.computeJacobianDeterminant();                //compute differential element and normal if element of dim = spacedim-1
                      if(invJacobian) mapdata.invertJacobianMatrix();      //compute inverse of jacobian
                      if(normalRequired)
                        {
                          mapdata.computeOrientedNormal();                 //compute oriented normal if required
                          np=&mapdata.normalVector;
                        }
                      upmapdata = false;
                    }

                  number_t nbquad = quad->numberOfPoints();
                  shv_us.clear();shv_vs.clear();
                  shv_us.resize(nbquad,ShapeValues(*relt_u,ord_opu>0,ord_opu>1));
                  shv_vs.resize(nbquad,ShapeValues(*relt_v,ord_opv>0,ord_opv>1));

                  MeshElement* melt_u = melt, *melt_v = melt;
                  GeomMapData* mapdata_u = &mapdata, * mapdata_v = &mapdata;
                  bool upmapdata_u = true, upmapdata_v = true;
                  if(extend_u!=nullptr) {
                      melt_u = gelt_u->meshElement();
                      if(melt_u == nullptr) melt_u = gelt_u->buildSideMeshElement(); //no meshElement structure, build one
                      mapdata_u=new GeomMapData(melt_u);                       //structure to store jacobian
                      if(melt_u->linearMap)   //compute geometric data only once if geom elt map is linear
                        {
                          mapdata_u->computeJacobianMatrix(gelt_u->geomRefElement()->centroid());  //compute jacobian
                          mapdata_u->computeJacobianDeterminant();          //compute differential element and normal if element of dim = spacedim-1
                          mapdata_u->invertJacobianMatrix();                //compute inverse of jacobian
                          upmapdata_u = false;
                        }
                  }
                  else {
                      upmapdata_u = false;
                      // compute shape values on quadrature points
                      for(number_t q = 0; q < nbquad; q++) {
                          std::vector<real_t>::const_iterator p = quad->point(q);
                          relt_u->computeShapeValues(p, shv_us[q], ord_opu > 0);
                          if(nbc_u > 1) shv_us[q].extendToVector(nbc_u);  //extend scalar shape functions to nbc_u vector shape functions
                      }
                  }

                  if(extend_v!=nullptr) {
                      melt_v = gelt_v->meshElement();
                      if(melt_v == nullptr) melt_v = gelt_v->buildSideMeshElement(); //no meshElement structure, build one
                      mapdata_v= new GeomMapData(melt_v);                      //structure to store jacobian
                      if(melt_v->linearMap)   //compute geometric data only once if geom elt map is linear
                        {
                          mapdata_v->computeJacobianMatrix(gelt_v->geomRefElement()->centroid());  //compute jacobian
                          mapdata_v->computeJacobianDeterminant();          //compute differential element and normal if element of dim = spacedim-1
                          mapdata_v->invertJacobianMatrix();                //compute inverse of jacobian
                          upmapdata_v = false;
                        }
                  }
                  else {
                      upmapdata_v = false;
                      // compute shape values on quadrature points
                      for(number_t q = 0; q < nbquad; q++) {
                          std::vector<real_t>::const_iterator p = quad->point(q);
                          relt_v->computeShapeValues(p, shv_vs[q], ord_opv > 0);
                          if(nbc_v > 1) shv_vs[q].extendToVector(nbc_v);  //extend scalar shape functions to nbc_v vector shape functions
                      }
                  }
//                  thePrintStream<<"shv_u:"<<shv_us[0]<<eol<<"shv_v:"<<shv_vs[0]<<eol<<std::flush;

                  //compute integrand on quadrature points
                  Matrix<K> matel(nb_vt, nb_ut, K(0));
                  dimen_t d=0;
                  for(number_t q = 0; q < nbquad; q++)   //loop on quadrature points
                    {
                      if(upmapdata)  //update side element mapdata
                        {
                          mapdata.computeJacobianMatrix(quad->point(q));          //compute jacobian at q th quadrature point
                          mapdata.computeJacobianDeterminant();                   //compute differential element and normal if element of dim = spacedim-1
                          if(invJacobian) mapdata.invertJacobianMatrix();         //compute inverse of jacobian
                          if(normalRequired)
                            {
                              mapdata.computeOrientedNormal();                     //compute oriented normal if required
                              np=&mapdata.normalVector;
                            }
                        }
                      Point xq(quad->point(q), quad->dim()), x = mapdata.geomMap(xq);
                      if(upmapdata_u) //update plain element mapdata
                        {
                          std::vector<real_t> p;
                          if(extype_u == _ficticiousExtension) p = mapdata_u->geomMapInverse(x);
                          else p= gelt_u->geomRefElement()->sideToElt(side_u,quad->point(q));
                          mapdata_u->computeJacobianMatrix(p.begin());          //compute jacobian at q th quadrature point
                          mapdata_u->computeJacobianDeterminant();              //compute differential element and normal if element of dim = spacedim-1
                          if(invJacobian) mapdata_u->invertJacobianMatrix();    //compute inverse of jacobian
                        }
                      if(upmapdata_v) //update plain element mapdata
                        {
                          std::vector<real_t> p;
                          if(extype_v == _ficticiousExtension) p = mapdata_v->geomMapInverse(x);
                          else p= gelt_v->geomRefElement()->sideToElt(side_v,quad->point(q));
                          mapdata_v->computeJacobianMatrix(p.begin());          //compute jacobian at q th quadrature point
                          mapdata_v->computeJacobianDeterminant();              //compute differential element and normal if element of dim = spacedim-1
                          if(invJacobian) mapdata_v->invertJacobianMatrix();    //compute inverse of jacobian
                        }
                      bool compute = true;
                      Point yu(x); real_t td;
                      if(extype_u==_ficticiousExtension)
                      {
                          if(map_u!=nullptr)
                          {
                              (*map_u)(x,yu);
                              GeomElement* gu=extend_u->meshDomain()->extensionin_p->nearest(yu,td);
                              compute = (gu==gelt_u);
                          }
                          else compute = pointInElement(yu,*gelt_u);
                      }
                      Point yv(x);
                      if(compute && extype_v==_ficticiousExtension )
                        {
                          if(map_v!=nullptr)
                          {
                              (*map_v)(x,yv);
                              GeomElement* gv=extend_v->meshDomain()->extensionin_p->nearest(yv,td);
                              compute = (gv=gelt_v);
                          }
                          else compute = pointInElement(x,*gelt_u);
                      }
                      if(compute)
                        {
                          if(extend_u!=nullptr) //update u-shapevalues
                            {
                              std::vector<real_t> xm;
                              if(extype_u == _ficticiousExtension) xm = mapdata_u->geomMapInverse(yu);
                              else xm= gelt_u->geomRefElement()->sideToElt(side_u, xq.begin());
                              relt_u->computeShapeValues(xm.begin(), shv_us[q], ord_opu > 0);
                              if(nbc_u > 1) shv_us[q].extendToVector(nbc_u);  //extend scalar shape functions to nbc_u vector shape functions
                            }
                          if(extend_v!=nullptr) //update v-shapevalues
                            {
                              std::vector<real_t> xm;
                              if(extype_v == _ficticiousExtension) xm = mapdata_v->geomMapInverse(yv);
                              else xm= gelt_v->geomRefElement()->sideToElt(side_v, xq.begin());
                              relt_v->computeShapeValues(xm.begin(), shv_vs[q], ord_opv > 0);
                              if(nbc_v > 1) shv_vs[q].extendToVector(nbc_v);  //extend scalar shape functions to nbc_u vector shape functions
                            }

                          //map shapevalues
                          ShapeValues* sv_u = &shv_us[q];           //original u shape values
                          ShapeValues* sv_v = &shv_vs[q];           //original v shape values
                          ShapeValues svt_u, svt_v;
                          if(mapsh_u || changeSign_u || rotsh_u)    //map u shape values
                            {
                              mapShapeValues(*relt_u, *melt_u, *mapdata_u, mapsh_u, femt_u, rotsh_u, ord_opu, changeSign_u, sign_u, dimfun_u, d, shv_us[q], svt_u);
                              sv_u = &svt_u;
                            }
                          if(mapsh_v || changeSign_v || rotsh_v)    //map v shape values
                            {
                              mapShapeValues(*relt_v, *melt_v, *mapdata_v, mapsh_v, femt_v, rotsh_v, ord_opv, changeSign_v, sign_v, dimfun_v, d, shv_vs[q], svt_v);
                              sv_v = &svt_v;
                            }
                          K alpha = coef * mapdata.differentialElement** (quad->weight(q));
//                          thePrintStream<<"map shv_u:"<<*sv_u<<eol<<"map shv_v:"<<*sv_v<<eol<<std::flush;

                          //compute integrand (OperatorOnUnknown)
                          dimen_t du,mu,dv,mv; //blocks size
                          Vector<K> val_u, val_v;
                          if(op_u.hasFunction()) op_u.eval(yu, sv_u->w, sv_u->dw, sv_u->d2w, dimfun_u, val_u, du, mu, np);  //evaluate differential operator with function
                          else                   op_u.eval(sv_u->w, sv_u->dw, sv_u->d2w, dimfun_u, val_u, du, mu, np);     //evaluate differential operator
                          if(op_v.hasFunction()) op_v.eval(yv, sv_v->w, sv_v->dw, sv_v->d2w, dimfun_v, val_v, dv, mv, np);  //evaluate differential operator with function
                          else                   op_v.eval(sv_v->w, sv_v->dw, sv_v->d2w, dimfun_v, val_v, dv, mv, np);     //evaluate differential operator
                          tensorOpAdd(aop, val_v, nb_vt, val_u, nb_ut, matel, alpha);                           //elementary matrix, note the transposition of u and v
//                          thePrintStream<<"   xq="<<xq<<" x="<<x<<eol<<"  val_u="<<val_u<<eol<<"  val_v="<<val_v<<eol<<std::flush;
                        }//end compute
                    }//end of quadrature points loop

//                  thePrintStream<<"dofNum_u="<<dofNum_u<<" dofNum_v="<<dofNum_v<<eol<<std::flush;
//                  thePrintStream<<"adress = "<<adrs<<eol<<std::flush;
//                  thePrintStream<<"matel="<<matel<<eol<<std::flush;

                  //assembling matel in global matrix, matel is never a block matrix as mat is a block matrix in case of vector unknowns
                  std::vector<number_t>::iterator itd_u, itd_v, it_ue;   //dof numbering iterators
                  typename Matrix<K>::iterator itm = matel.begin(), itm_u;
                  number_t incr = nbc_u * nbc_v * nb_u; //block row increment in matel
                  number_t i = 0;
                  //loop on (vector) dofs in u and v
                  for(itd_v = dofNum_v.begin(); itd_v != dofNum_v.end() ; itd_v++, itm += incr, i++)
                    {
                      itm_u = itm;
                      itk = itadrs + i * nb_u;
                      for(itd_u = dofNum_u.begin(); itd_u != dofNum_u.end() ; itd_u++, itm_u += nbc_u, itk++)
                        {
                          if(!sym || *itd_v >= *itd_u)
                            {
                              assemblyMat(mat(*itk), itm_u, nb_ut);
                            }
                        }
                    } //end of dofNum loops
                  if(extend_u!=nullptr) delete mapdata_u;
                  if(extend_v!=nullptr) delete mapdata_v;
                } //end of bilinear forms loop
            } //end v loop
        } //end u loop
      if(extend_u==nullptr) delete sideToExt_u;
      if(extend_v==nullptr) delete sideToExt_v;
      if(show_status && currentThread()==0&& k!=0 && k % nbeltdiv10 == 0)  //progress status
        { std::cout<< k/nbeltdiv10 <<"0% "<<std::flush; }
    }//end of elements loop

  if(theVerboseLevel > 0) std::cout<<" done"<<eol<<std::flush;
  trace_p->pop();
}

// #########################################################################################################
//   OLD VERSION
// #########################################################################################################
//template <>
//template <typename T, typename K>
//void ComputationAlgorithm<_FEextComputation>::compute(const SuBilinearForm& subf, LargeMatrix<T>& mat, K& vt,
//    Space* space_u_p, Space* space_v_p, const Unknown* u_p, const TestFct* v_p)
//{
//  if(subf.size() == 0) return; // void bilinear form, nothing to compute !!!
//  trace_p->push("SuTermMatrix::computeFE");
//  if(theVerboseLevel>0) std::cout<<"computing FE extended term "<< subf.asString()<<", using "<<numberOfThreads()<<" threads: "<<std::flush;
//
//  cit_vbfp itf = subf.begin();
//  const GeomDomain* dom = itf->first->asIntgForm()->domain(); //common domain of integrals
//
//  //required extension and max derivative orders
//  dimen_t ordu = 0, ordv = 0;
//  bool extend_u=false, extend_v=false, normalRequired=false;
//  for(; itf != subf.end(); ++itf)
//    {
//      const IntgBilinearForm* ibf = itf->first->asIntgForm(); //basic bilinear form as simple integral
//      const OperatorOnUnknown& op_u = ibf->opu();             //operator on unknown
//      const OperatorOnUnknown& op_v = ibf->opv();             //operator on test function
//      if(op_u.extensionRequired()) extend_u=true;
//      if(op_v.extensionRequired()) extend_v=true;
//      ordu = std::max(ordu, dimen_t(op_u.diffOrder()));       //order of u differential involved in operators
//      ordv = std::max(ordv, dimen_t(op_v.diffOrder()));       //order of v differential involved in operators
//      setDomain(const_cast<GeomDomain*>(dom));                //transmit domain pointer to thread data
//      if(op_u.normalRequired() || op_v.normalRequired())  normalRequired=true;
//    }
//
//  if(normalRequired) dom->setNormalOrientation();
//
//  //create side to ext map
//  if(!extend_u && !extend_v) error("no_extension_required");
//  const std::map<GeomElement*, std::list<GeoNumPair> >& sideToExt = dom->meshDomain()->buildSideToExt();  //map side element -> list of extension elements
//  GeomDomain& dom_ext = dom->meshDomain()->extendDomain();
//  std::map<GeomElement*, std::list<GeoNumPair> >::const_iterator itse=sideToExt.begin();
//  std::list<GeoNumPair>::const_iterator itsel;
//
//  //update normal
//  if(normalRequired && !dom->meshDomain()->orientationComputed) dom->meshDomain()->setNormalOrientation();
//
//  //retry boundary spaces
//  const Space* sp_u = subf.up()->space();  //space of unknown
//  const Space* sp_v = subf.vp()->space();  //space of test function
//  if(sp_u->typeOfSpace() != _feSpace) error("not_fe_space_type", sp_u->name(), sp_u->typeOfSpace());
//  if(sp_v->typeOfSpace() != _feSpace) error("not_fe_space_type", sp_v->name(), sp_v->typeOfSpace());
//  const Space* subsp_u=nullptr;
//  if(extend_u) subsp_u = Space::findSubSpace(&dom_ext, space_u_p); //find subspace (linked to domain dom_ext) of space_u_p
//  else subsp_u=Space::findSubSpace(dom, space_u_p);  //find subspace (linked to domain dom) of space_u_p
//  if(subsp_u == nullptr) subsp_u = space_u_p;         //is a FESpace, not a FESubspace
//  const Space* subsp_v=nullptr;
//  if(extend_v) subsp_v = Space::findSubSpace(&dom_ext, space_v_p); //find subspace (linked to domain dom_ext) of space_v_p
//  else subsp_v=Space::findSubSpace(dom, space_v_p);  //find subspace (linked to domain dom) of space_b_p
//  if(subsp_v == nullptr) subsp_v = space_v_p;         //is a FESpace, not a FESubspace
//  bool same_interpolation = (subsp_u == subsp_v);
//
//  //retry dof information
//  bool doflag_u = (subsp_u == space_u_p), doflag_v = (subsp_v == space_v_p);
//  if(!doflag_u) space_u_p->builddofid2rank();              //build map dofid_u->rank
//  if(!doflag_v) space_v_p->builddofid2rank();              //build map dofid_v->rank
//  dimen_t dimfun_u = sp_u->dimFun(), dimfun_v = sp_v->dimFun();
//  dimen_t nbc_u = u_p->nbOfComponents(), nbc_v = v_p->nbOfComponents(); //number of components of unknown
//  if(nbc_u > 1) dimfun_u = nbc_u;                                       //case of vector dofs (scalar shape functions will be extended to vector shape functions)
//  if(nbc_v > 1) dimfun_v = nbc_v;                                       //case of vector dofs (scalar shape functions will be extended to vector shape functions)
//
//  //retry FE information
//  FEMapType femt_u=subsp_u->element_p(number_t(0))->refElt_p->mapType,
//            femt_v=subsp_v->element_p(number_t(0))->refElt_p->mapType;
//  bool invertJacobian =(femt_u==_covariantPiolaMap || femt_v==_covariantPiolaMap);
//  bool same_shv = (same_interpolation && nbc_v==nbc_u);
//  bool sym = same_shv && !(mat.sym == _noSymmetry);
//  number_t nbelt = dom->numberOfElements();
//
//  //print status
//  number_t nbeltdiv10 = nbelt/10;
//  bool show_status = (theVerboseLevel > 0 && mat.nbRows > 1000 &&  nbeltdiv10 > 1);
//
//  //temporary vector
//  std::vector<number_t> dofNum_u, dofNum_v;
//  Vector<real_t>* sign_u=nullptr, *sign_v=nullptr;
//  std::vector<ShapeValues> shv_us, shv_vs;
//
//  //main loop on elements of side domain
//  //====================================
//  for(number_t k = 0; k < nbelt; k++)
//    {
//      GeomElement* gelt = dom->meshDomain()->geomElements[k];   //geometric side element
//      ShapeType sh = gelt->shapeType();                         //shape type of side element
//
//      //thePrintStream<<"compute on side element: "<<*gelt<<eol;
//
//      // data related to u
//      const Element* elt_u = 0;
//      RefElement* relt_u =nullptr;
//      number_t nb_u = 0;
//      if(!extend_u) // no extension for u, use u side element
//        {
//          elt_u = subsp_u->element_p(gelt);  //u side FE
//          relt_u = elt_u->refElt_p;          //v reference side element;
//          if(doflag_u) dofNum_u = subsp_u->elementDofs(subsp_u->numElement(gelt)); //dof numbers (local numbering related to subsp_u=space_u_p)
//          else ranks(space_u_p->dofid2rank(), elt_u->dofNumbers, dofNum_u);        //use ranks function with map dofid2rank (faster)
//          nb_u = dofNum_u.size();
//        }
//
//      // data related to v
//      const Element* elt_v = 0;
//      RefElement* relt_v =nullptr;
//      number_t nb_v = 0;
//      if(!extend_v) // no extension for v, use v side element
//        {
//          elt_v = subsp_v->element_p(gelt);  //v side FE
//          relt_v = elt_v->refElt_p;          //v reference side element
//          if(doflag_v) dofNum_v = subsp_v->elementDofs(subsp_v->numElement(gelt)); //dof numbers (local numbering related to subsp_v=space_v_p)
//          else ranks(space_v_p->dofid2rank(), elt_v->dofNumbers, dofNum_v);        //use ranks function with map dofid2rank (faster)
//          nb_v = dofNum_v.size();
//        }
//
//      itse=sideToExt.find(gelt);
//      if(itse==sideToExt.end()) error("geoelt_parent_not_found", dom->name());
//      GeomElement* gelt_ext = gelt;
//
//      //loop on parents of side element, in general only one parent
//      //### DOES NOT WORK FOR AN INTERFACE AND TWO SPACES INVOLVED ON BOTH SIDES, FIX IT
//      for(itsel=itse->second.begin(); itsel!=itse->second.end(); ++itsel)
//        {
//          gelt_ext = itsel->first;              //geometric plain element (parent)
//          number_t side = itsel->second;
//          if(extend_u) //u extension
//            {
//              elt_u = subsp_u->element_p(gelt_ext);//u plain FE
//              relt_u = elt_u->refElt_p;            //reference element
//              nb_u =elt_u->dofNumbers.size();
//              dofNum_u = std::vector<number_t>(nb_u);
//              if(doflag_u) dofNum_u = subsp_u->elementDofs(subsp_u->numElement(gelt_ext)); //dof numbers (local numbering related to subsp_u=space_u_p)
//              else ranks(space_u_p->dofid2rank(), elt_u->dofNumbers, dofNum_u);            //use ranks function with map dofid2rank (faster)
//            }
//          if(extend_v) //v extension
//            {
//              elt_v = subsp_v->element_p(gelt_ext);//v plain FE
//              relt_v = elt_v->refElt_p;            //reference element
//              nb_v =elt_v->dofNumbers.size();
//              dofNum_v = std::vector<number_t>(nb_v);
//              if(doflag_v) dofNum_v = subsp_v->elementDofs(subsp_v->numElement(gelt_ext)); //dof numbers (local numbering related to subsp_v=space_v_p)
//              else ranks(space_v_p->dofid2rank(), elt_v->dofNumbers, dofNum_v);            //use ranks function with map dofid2rank (faster)
//            }
//
//          // local to global numbering matrix
//          std::vector<number_t> adrs(nb_v * nb_u, 0);
//          mat.positions(dofNum_v, dofNum_u, adrs, true); //adresses in mat of elementary matrix stored as a row vector
//          std::vector<number_t>::iterator itadrs = adrs.begin(), itk;
//          number_t nb_ut = nb_u * nbc_u, nb_vt = nb_v* nbc_v;
//
//          //sign correction for some element (div or rot element)
//          bool changeSign_u=false, changeSign_v=false;
//          if(relt_u->dofCompatibility == _signDofCompatibility)
//            {
//              sign_u=&elt_u->getDofSigns();
//              changeSign_u = sign_u->size()!=0;
//            }
//          if(same_shv)
//            {
//              changeSign_v=changeSign_u;
//              sign_v=sign_u;
//            }
//          else if(relt_v->dofCompatibility == _signDofCompatibility)
//            {
//              sign_v=&elt_v->getDofSigns();
//              changeSign_v = sign_v->size()!=0;
//            }
//
//          // dof rotation flag
//          bool rotsh_u=elt_u->refElt_p->rotateDof;
//          bool rotsh_v=elt_v->refElt_p->rotateDof;
//
//          //loop on basic bilinear forms
//          for(cit_vbfp itf = subf.begin(); itf != subf.end(); itf++)
//            {
//              const IntgBilinearForm* ibf = itf->first->asIntgForm(); //basic bilinear form as simple integral
//              const OperatorOnUnknown& op_u = ibf->opu();             //operator on unknown
//              const OperatorOnUnknown& op_v = ibf->opv();             //operator on test function
//              if(op_u.elementRequired() || op_v.elementRequired()) setElement(gelt);  // transmit gelt to thread data
//              number_t ord_opu = op_u.diffOrder();                    //derivative order of op_u
//              number_t ord_opv = op_v.diffOrder();                    //derivative order of op_v
//              bool mapsh_u = (femt_u!=_standardMap || ord_opu>0);
//              bool mapsh_v = (femt_v!=_standardMap || ord_opv>0);
//              bool hasf = op_u.hasFunction() || op_v.hasFunction();   //true if data function
//              AlgebraicOperator aop = ibf->algop();                   //algebraic operator between differential operator
//              K coef = complexToT<K>(itf->second);                    //coefficient of linear form
//              const QuadratureIM* qim = dynamic_cast<const QuadratureIM*>(ibf->intgMethod());
//              const Quadrature* quad = qim->getQuadrature(sh);        //quadrature rule pointer attached to integral
//
//              //compute shape values on quadrature points
//              number_t nbquad = quad->numberOfPoints();
//              shv_us.resize(nbquad), shv_vs.resize(nbquad);
//              Vector<real_t>* np=nullptr;                  //pointer to normal
//              for(number_t q = 0; q < nbquad; q++)
//                {
//                  std::vector<real_t>::const_iterator p = quad->point(q);
//                  std::vector<real_t> p_ext= gelt_ext->geomRefElement()->sideToElt(side,p);
//                  if(!extend_u) relt_u->computeShapeValues(p, ord_opu > 0);
//                  else relt_u->computeShapeValues(p_ext.begin(), ord_opu > 0);
//                  shv_us[q] = relt_u->shapeValues;
//                  if(nbc_u > 1) shv_us[q].extendToVector(nbc_u);  //extend scalar shape functions to nbc_u vector shape functions
//                  if(!extend_v) relt_v->computeShapeValues(p, ord_opv > 0);
//                  else relt_v->computeShapeValues(p_ext.begin(), ord_opv > 0);
//                  shv_vs[q] = relt_v->shapeValues;
//                  if(nbc_v > 1) shv_vs[q].extendToVector(nbc_v);  //extend scalar shape functions to nbc_v vector shape functions
//                }
//              //thePrintStream<<"shv_u:"<<shv_us[0]<<eol<<"shv_v:"<<shv_vs[0]<<eol;
//
//              //geometric stuff (side element)
//              MeshElement* melt = gelt->meshElement();
//              if(melt == nullptr) melt = gelt->buildSideMeshElement(); //it is a side element with no meshElement structure, build one
//              GeomMapData mapdata(melt);                         //structure to store jacobian
//              bool invJacobian = invertJacobian || op_u.diffOrder() > 0  || op_v.diffOrder() > 0;
//              bool upmapdata = true;
//              if(melt->linearMap) //compute geometric data only once if geom elt map is linear
//                {
//                  mapdata.computeJacobianMatrix(quad->point(0));       //compute jacobian
//                  mapdata.computeJacobianDeterminant();                //compute differential element and normal if element of dim = spacedim-1
//                  if(invJacobian) mapdata.invertJacobianMatrix();      //compute inverse of jacobian
//                  if(normalRequired)
//                    {
//                      mapdata.computeOrientedNormal();                  //compute outward normal if required
//                      np=&mapdata.normalVector;
//                    }
//                  upmapdata = false;
//                }
//              //geometric stuff (extended element)
//              MeshElement* melt_ext = gelt_ext->meshElement();
//              if(melt_ext == nullptr) melt = gelt_ext->buildSideMeshElement(); //no meshElement structure, build one
//              bool upmapdata_ext = true;
//              GeomMapData mapdata_ext(melt_ext);                         //structure to store jacobian
//              if(melt_ext->linearMap)   //compute geometric data only once if geom elt map is linear
//                {
//                  std::vector<real_t> p_ext= gelt_ext->geomRefElement()->sideToElt(side,quad->point(0));
//                  mapdata_ext.computeJacobianMatrix(p_ext.begin());        //compute jacobian
//                  mapdata_ext.computeJacobianDeterminant();                //compute differential element and normal if element of dim = spacedim-1
//                  if(invJacobian) mapdata_ext.invertJacobianMatrix();      //compute inverse of jacobian
//                  upmapdata_ext = false;
//                }
//
//              //compute integrand on quadrature points
//              Matrix<K> matel(nb_vt, nb_ut, K(0));
//              dimen_t d=0;
//              for(number_t q = 0; q < nbquad; q++)   //loop on quadrature points
//                {
//                  if(upmapdata)  //update side element mapdata
//                    {
//                      mapdata.computeJacobianMatrix(quad->point(q));          //compute jacobian at q th quadrature point
//                      mapdata.computeJacobianDeterminant();                   //compute differential element and normal if element of dim = spacedim-1
//                      if(invJacobian) mapdata.invertJacobianMatrix();         //compute inverse of jacobian
//                      if(normalRequired)
//                        {
//                          mapdata.computeOrientedNormal();                     //compute outward normal if required
//                          np=&mapdata.normalVector;
//                        }
//                    }
//                  if(upmapdata_ext) //update plain element mapdata
//                    {
//                      std::vector<real_t> p= gelt_ext->geomRefElement()->sideToElt(side,quad->point(q));
//                      mapdata_ext.computeJacobianMatrix(p.begin());          //compute jacobian at q th quadrature point
//                      mapdata_ext.computeJacobianDeterminant();              //compute differential element and normal if element of dim = spacedim-1
//                      if(invJacobian) mapdata_ext.invertJacobianMatrix();    //compute inverse of jacobian
//                    }
//
//                  //map shapevalues
//                  ShapeValues* sv_u = &shv_us[q];           //original u shape values
//                  ShapeValues* sv_v = &shv_vs[q];           //original v shape values
//                  ShapeValues svt_u, svt_v;
//                  if(mapsh_u || changeSign_u || rotsh_u)    //map u shape values
//                    {
//                      if(!extend_u) mapShapeValues(*relt_u, *melt, mapdata, mapsh_u, femt_u, rotsh_u, ord_opu, changeSign_u, sign_u, dimfun_u, d, shv_us[q], svt_u);
//                      else  mapShapeValues(*relt_u, *melt_ext, mapdata_ext, mapsh_u, femt_u, rotsh_u, ord_opu, changeSign_u, sign_u, dimfun_u, d, shv_us[q], svt_u);
//                      sv_u = &svt_u;
//                    }
//                  if(mapsh_v || changeSign_v || rotsh_v)    //map v shape values
//                    {
//                      if(!extend_v) mapShapeValues(*relt_v, *melt, mapdata, mapsh_v, femt_v, rotsh_v, ord_opv, changeSign_v, sign_v, dimfun_v, d, shv_vs[q], svt_v);
//                      else  mapShapeValues(*relt_v, *melt_ext, mapdata_ext, mapsh_v, femt_v, rotsh_v, ord_opv, changeSign_v, sign_v, dimfun_v, d, shv_vs[q], svt_v);
//                      sv_v = &svt_v;
//                    }
//                  K alpha = coef * mapdata.differentialElement** (quad->weight(q));
//                  //thePrintStream<<"map shv_u:"<<*sv_u<<eol<<"map shv_v:"<<*sv_v<<eol;
//
//                  //compute integrand (OperatorOnUnknown)
//                  dimen_t du,mu,dv,mv; //blocks size
//                  Vector<K> val_u, val_v;
//                  Point xq(quad->point(q), quad->dim()), x;
//                  if(hasf) x = mapdata.geomMap(xq);
//                  if(op_u.hasFunction()) op_u.eval(x, sv_u->w, sv_u->dw, dimfun_u, val_u, du, mu, np);  //evaluate differential operator with function
//                  else                   op_u.eval(sv_u->w, sv_u->dw, dimfun_u, val_u, du, mu, np);     //evaluate differential operator
//                  if(op_v.hasFunction()) op_v.eval(x, sv_v->w, sv_v->dw, dimfun_v, val_v, dv, mv, np);  //evaluate differential operator with function
//                  else                   op_v.eval(sv_v->w, sv_v->dw, dimfun_v, val_v, dv, mv, np);     //evaluate differential operator
//                  tensorOpAdd(aop, val_v, nb_vt, val_u, nb_ut, matel, alpha);                           //elementary matrix, note the transposition of u and v
//                  //thePrintStream<<"   xq="<<xq<<" x="<<x<<eol<<"  val_u="<<val_u<<eol<<"  val_v="<<val_v<<eol;
//                }//end of quadrature points loop
//
////              thePrintStream<<"dofNum_u="<<dofNum_u<<" dofNum_v="<<dofNum_v<<eol;
////              thePrintStream<<"matel="<<matel<<eol;
//
//              //assembling matel in global matrix, matel is never a block matrix as mat is a block matrix in case of vector unknowns
//              std::vector<number_t>::iterator itd_u, itd_v, it_ue;   //dof numbering iterators
//              typename Matrix<K>::iterator itm = matel.begin(), itm_u;
//              number_t incr = nbc_u * nbc_v * nb_u; //block row increment in matel
//              number_t i = 0;
//              //loop on (vector) dofs in u and v
//              for(itd_v = dofNum_v.begin(); itd_v != dofNum_v.end() ; itd_v++, itm += incr, i++)
//                {
//                  itm_u = itm;
//                  itk = itadrs + i * nb_u;
//                  for(itd_u = dofNum_u.begin(); itd_u != dofNum_u.end() ; itd_u++, itm_u += nbc_u, itk++)
//                    {
//                      if(!sym || *itd_v >= *itd_u)
//                        {
//                          assemblyMat(mat(*itk), itm_u, nb_ut);
//                        }
//                    }
//                } //end of dofNum loops
//            } //end of bilinear forms loop
//        } //end extension elements loop
//      if(show_status)   //progress status
//        {
//          number_t numthread=0;
//#ifdef XLIFEPP_WITH_OMP
//          numthread = omp_get_thread_num();
//#endif // XLIFEPP_WITH_OMP
//          if(numthread==0 && k!=0 && k % nbeltdiv10 == 0) { std::cout<< k/nbeltdiv10 <<"0% "<<std::flush; }
//        }
//    } //end of elements loop
//  if(theVerboseLevel > 0) std::cout<<" done"<<eol<<std::flush;
//  trace_p->pop();
//}

} // end of namespace xlifepp
