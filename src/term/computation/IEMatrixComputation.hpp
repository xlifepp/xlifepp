/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file IEMatrixComputation.hpp
  \author E. Lunéville
  \since 22 jan 2017
  \date 22 jan 2017

  \brief Implementation of template IE TermMatrix computation functions,
         compute by different methods (tensor quadrature, Sauter-schwab formulae, Lenoir-Salles methods)

         intg_domx intg_domy op(u)(y) aop op(k)(x,y) aop op(v)(x)
    or
         intg_domx intg_domy op(v)(x) aop op(k)(x,y) aop op(u)(y)

  where u denotes the unknown and v the test function

  this file is included by SuTermMatrix.hpp
  do not include it elsewhere

    NEW VERSION OF IEMatrixComputation
       dealing with IntegrationMethods object of bilinear form instead of IntegrationMethod pointer
       dealing with extension of side domain
       taking into account vector problems
*/

namespace xlifepp
{

/*! computation of elementary matrix of IE term using double quadrature (quadx, quady)
      intg elt_S intg elt_V opu(y) opk(x,y) opv(x) dy dx
   assuming  precomputation of geometrical stuff (jacobian,diffelt, normals, ...), shape values and mapping of quadrature points onto physical space

   the template parameter K is always a scalar type (real_t or complex_t)

     elt_S: element supporting u(y)
     elt_T: element supporting v(x)
     kuv: operator on kernel and unknowns  opu(y) opk(x,y) opv(x)
     quadx: quadrature in x variable (i.e on elt_T)
     quady: quadrature in y variable (i.e on elt_S)
     ieparams: computation parameters
     val_opu, val_opv, val_opk: working vectors to store value of opu, opv and opk
     res: matrix result in SCALAR representation even the problem is a vector problem !
           the calling function has to do the conversion from SCALAR MATRIX to MATRIX of MATRIX in case of vector problem

    Assume here that either Kernel is Matrix or ShapeFunctions are vectors
*/
template<typename K>
void computeQuadratureIE(const Element* elt_S, const Element* elt_T, const KernelOperatorOnUnknowns& kuv,
                         Quadrature* quadx, Quadrature* quady, Matrix<K>& res, IEcomputationParameters& ieparams,
                         Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk)
{
  //load geometric info
  MeshElement* melt_S=ieparams.melt_u, *melt_T=ieparams.melt_v;  //side elements
  bool firstOrderElt_u = melt_S->linearMap, firstOrderElt_v = melt_T->linearMap;
  if(ieparams.isoGeo) firstOrderElt_u=firstOrderElt_v=false;  //deactivate linear shortcut
  GeomMapData* gmap_S, *gmap_T;
  GeomMapData gmapS, gmapT;
  if(firstOrderElt_u) gmap_S=melt_S->geomMapData_p;
  else
  {
     gmapS=GeomMapData(melt_S,true,true,kuv.ynormalRequired()); // copy of geom data to be thread safe
     gmap_S=&gmapS;
  }
  if(firstOrderElt_v) gmap_T=melt_T->geomMapData_p;
  else
  {
     gmapT=GeomMapData(melt_T,true,true,kuv.xnormalRequired()); // copy of geom data to be thread safe
     gmap_T=&gmapT;
  }
  real_t difelt_S = gmap_S->differentialElement, difelt_T=  gmap_T->differentialElement, difelt=difelt_S*difelt_T;
  Vector<real_t>* nx=nullptr, *ny=nullptr;
  if(kuv.xnormalRequired()) nx = &gmap_T->normalVector;
  if(kuv.ynormalRequired()) ny = &gmap_S->normalVector;
  //thePrintStream<<"difelt="<<difelt;

  //load quadrature stuff
  dimen_t dqx = quadx->dim(), dqy = quady->dim();
  std::vector< real_t >::const_iterator itwxb = quadx->weights().begin(), itwxe = quadx->weights().end(), itwx = itwxb,
                                        itwyb = quady->weights().begin(), itwye = quady->weights().end(), itwy = itwyb,
                                        itqxb = quadx->point(), itqyb = quady->point(), itqx, itqy;        //points of quadratures for x/y
  std::vector<Point>::iterator itpxb = melt_T->geomMapData_p->phyPoints[quadx].begin(), itpx = itpxb,
                               itpyb = melt_S->geomMapData_p->phyPoints[quady].begin(), itpy = itpyb;   //get quadrature points in physical space
  K alpha;
  const OperatorOnKernel& opker=kuv.opker();
  const Kernel* ker = opker.kernelp();
  StrucType stk = _scalar, stopk=opker.strucType();
  if(ker!=nullptr) stk = ker->strucType();
  bool opkIsId = opker.isId();
  Extension* ext = opker.ext_p;
  number_t nbcu=ieparams.dimf_u, nbcv = ieparams.dimf_v;
  bool realker = false;
  if(ker!=nullptr) realker=(ker->valueType()==_real && xlifepp::valueType(val_opk)==_complex);

  // shortcut P0 : u and v interpolation are P0, id operator on u and v, first order geometric elements, no extension
  // ----------------------------------------------------------------------------------------------------------------
  if(ext==nullptr && ieparams.isP0 && ieparams.isId && firstOrderElt_u && firstOrderElt_v & !ieparams.hasf_u && !ieparams.hasf_v)
    {
      if(opkIsId)  //u*K*v
        {
          if(stk==_scalar)  //u*K*v
            {
              K vk;
              if(nbcu==1 && nbcv==1)   //scalar shape function
                {
                  for(itwx =itwxb; itwx!=itwxe; ++itwx, ++itpx)
                    {
                      alpha = *itwx * difelt;
                      itpy  = itpyb;
                      for(itwy =itwyb; itwy!=itwye; ++itwy, ++itpy)
                        {
                          if(ker!=nullptr)
                            {
                              if(realker)
                                {
                                  real_t vr;
                                  (*ker)(*itpx, *itpy, vr);
                                  res[0]+=alpha* * itwy * vr;
                                }
                              else
                                {
                                  (*ker)(*itpx, *itpy, vk);
                                  res[0]+=alpha* * itwy * vk;
                                }
                            }
                          else res[0]+=alpha* * itwy;
                        }
                    }
                  return;
                }
              else if(nbcu==nbcv)  //vector P0 shape function and scalar Kernel: K*(u|v) -> K*Id
                {
                  for(itwx =itwxb; itwx!=itwxe; ++itwx, ++itpx)
                    {
                      alpha = *itwx * difelt;
                      itpy  = itpyb;
                      for(itwy =itwyb; itwy!=itwye; ++itwy, ++itpy)
                        {
                          if(ker!=nullptr)
                            {
                              if(realker)
                                {
                                  real_t vr;
                                  (*ker)(*itpx, *itpy, vr);
                                  for(number_t i=0; i<nbcu; ++i) res[i*nbcv+i]+=alpha* * itwy * vr;
                                }
                              else
                                {
                                  (*ker)(*itpx, *itpy, vk);
                                  for(number_t i=0; i<nbcu; ++i) res[i*nbcv+i]+=alpha* * itwy * vk;

                                }
                            }
                          else
                            for(number_t i=0; i<nbcu; ++i) res[i*nbcv+i]+=alpha* * itwy;
                        }
                    }
                  return;
                }
              else
                {
                  where("computeQuadratureIE(...)");
                  error("operator_mismatch_size", nbcu, nbcv);
                }
            }
          else if(stk==_matrix)   //matrix kernel, assume ui, vj be the vectors (1,0,..), (0,1,...), ... consitent with the matrix kernel
            //  -> uj*K*vi = Kij
            {
              if(realker)
                {
                  Matrix<real_t> vk(res.numberOfRows(),res.numberOfCols(),0.);
                  for(itwx =itwxb; itwx!=itwxe; ++itwx, ++itpx)
                    {
                      alpha = *itwx * difelt;
                      itpy  = itpyb;
                      for(itwy =itwyb; itwy!=itwye; ++itwy, ++itpy)
                        {
                          if(ker!=nullptr)
                            {
                              (*ker)(*itpx, *itpy, vk);
                              res +=alpha* * itwy * vk;
                            }
                          else res +=alpha* * itwy;
                        }
                    }
                }
              else
                {
                  Matrix<K> vk = res*K(0);
                  for(itwx =itwxb; itwx!=itwxe; ++itwx, ++itpx)
                    {
                      alpha = *itwx * difelt;
                      itpy  = itpyb;
                      for(itwy =itwyb; itwy!=itwye; ++itwy, ++itpy)
                        {
                          if(ker!=nullptr)
                            {
                              (*ker)(*itpx, *itpy, vk);
                              res +=alpha* * itwy * vk;
                            }
                          else res +=alpha* * itwy;
                        }
                    }
                }
              return;
            }
          else
            {
              where("computeQuadratureIE(...)");
              error("scalar_or_matrix");
            }
        }
      else //opk is not id
        {
          if(stopk==_scalar)   //scalar opk
            {
              K vk;
              if(nbcu==1 && nbcv==1)   //scalar shape function
                {
                  for(itwx =itwxb; itwx!=itwxe; ++itwx, ++itpx)
                    {
                      alpha = *itwx * difelt;
                      itpy  = itpyb;
                      for(itwy =itwyb; itwy!=itwye; ++itwy, ++itpy)
                        {
                          opker.eval(*itpx, *itpy, vk, nx, ny);
                          res[0]+=alpha* * itwy * vk;
                        }
                    }
                  return;
                }
              else if(nbcu==nbcv)  //vector P0 shape function and scalar Kernel: opK*(u|v) -> opK*Id
                {
                  for(itwx =itwxb; itwx!=itwxe; ++itwx, ++itpx)
                    {
                      alpha = *itwx * difelt;
                      itpy  = itpyb;
                      for(itwy =itwyb; itwy!=itwye; ++itwy, ++itpy)
                        {
                          opker.eval(*itpx, *itpy, vk, nx, ny);
                          for(number_t i=0; i<nbcu; ++i) res[i*nbcv+i]+=alpha* * itwy * vk;
                        }
                    }
                  return;
                }
              else
                {
                  where("computeQuadratureIE(...)");
                  error("operator_mismatch_size", nbcu, nbcv);
                }
            }
          else if(stopk==_matrix)   //matrix operator kernel, assume ui, vj be the vectors (1,0,..), (0,1,...), ... consitent with
            //  -> uj*opK*vi = opK_ij
            {
              Matrix<K> vk = res*K(0);
              for(itwx =itwxb; itwx!=itwxe; ++itwx, ++itpx)
                {
                  alpha = *itwx * difelt;
                  itpy  = itpyb;
                  for(itwy =itwyb; itwy!=itwye; ++itwy, ++itpy)
                    {
                      opker.eval(*itpx, *itpy, vk, nx, ny);  //only for scalar kernel
                      res+=alpha* * itwy * vk;
                    }
                }
              return;
            }
          else
            {
              where("computeQuadratureIE(...)");
              error("scalar_or_matrix");
            }
        }
    }

  // general case
  // ------------
  //extension stuff
  ExtensionData* extdata=nullptr;
  bool kill_extdata = false;
  number_t extOrder=0;
  bool ext_x=false, ext_y=false;
  if(ext!=nullptr)  // opker has an extension
    {
      if(ext->var_==_x) ext_x=true;
      if(ext->var_==_y) ext_y=true;
      std::map<GeomElement*,std::set<number_t> >::const_iterator itme=ext->domToSide.end();
      if(ext_x) if(ext->domToSide.find(elt_T->geomElt_p)==itme) return;   //outside x_extension
      if(ext_y) if(ext->domToSide.find(elt_S->geomElt_p)==itme) return;   //outside y_extension
      extdata=new ExtensionData();
      kill_extdata = true;
      extOrder=opker.extensionOrder();
    }

  // get shapevalues and map them if required
  std::vector<ShapeValues> *shv_Sy = &elt_S->refElt_p->qshvs_[quady], *shv_Tx ;
  if(ieparams.useAux) shv_Tx = &elt_T->refElt_p->qshvs_aux[quadx]; else shv_Tx = &elt_T->refElt_p->qshvs_[quadx];
  bool mapsh_u = (ieparams.femt_u!=_standardMap || ieparams.difforder_u>0);
  bool mapsh_v = (ieparams.femt_v!=_standardMap || ieparams.difforder_v>0);
  std::vector<ShapeValues>::iterator itvxb=shv_Tx->begin(), itvxe= shv_Tx->end(), itvx;
  std::vector<ShapeValues>::iterator ituyb=shv_Sy->begin(), ituye= shv_Sy->end(), ituy;

  // if extension, restrict shape values to shape values related to side (because computed on all sides, see preComputationIE)
  if(ieparams.extend_u)
    {
      number_t nbqy = quady->numberOfPoints();
      ituyb=shv_Sy->begin()+(ieparams.side_u-1)*nbqy;
      ituye= ituyb + nbqy;
    }
  if(ieparams.extend_v)
    {
      number_t nbqx = quadx->numberOfPoints();
      itvxb=shv_Tx->begin()+(ieparams.side_v-1)*nbqx;
      itvxe= itvxb + nbqx;
    }

  bool kill_shv_Sy=false, kill_shv_Tx=false;
  if(mapsh_u || ieparams.rotsh_u || ieparams.changeSign_u)
    {
      shv_Sy = new std::vector<ShapeValues>(ituyb, ituye);
      bool rotsh = ieparams.rotsh_u, changeSign=ieparams.changeSign_u;
      FEMapType femt = ieparams.femt_u;
      dimen_t dimfun = ieparams.dimf_u;
      RefElement* relt = elt_S->refElt_p;
      number_t ord=ieparams.difforder_u;
      Vector<real_t>* sign=ieparams.sign_u;
      MeshElement* melt=elt_S->geomElt_p->meshElement();
      GeomMapData* gmap = melt->geomMapData_p;     // to be done : in case of curved elt, check if it is correct
      if(gmap->inverseJacobianMatrix.size()==0 && (ord>0 || femt==_covariantPiolaMap)) gmap->invertJacobianMatrix();
      dimen_t d=dimfun;
      for(ituy = shv_Sy->begin(); ituy != shv_Sy->end(); ++ituy)
        mapShapeValues(*relt, *melt, *gmap, mapsh_u, femt, rotsh, ord, changeSign, sign, dimfun, d, *ituy, *ituy);
      ituyb=shv_Sy->begin(); ituye= shv_Sy->end();
      kill_shv_Sy=true;
    }
  if(mapsh_v || ieparams.rotsh_v || ieparams.changeSign_v)
    {
      shv_Tx = new std::vector<ShapeValues>(itvxb, itvxe);
      bool rotsh = ieparams.rotsh_v, changeSign=ieparams.changeSign_v;
      FEMapType femt = ieparams.femt_v;
      dimen_t dimfun = ieparams.dimf_v;
      RefElement* relt = elt_T->refElt_p;
      number_t ord=ieparams.difforder_v;
      Vector<real_t>* sign=ieparams.sign_v;
      MeshElement* melt=elt_T->geomElt_p->meshElement();
      GeomMapData* gmap =melt->geomMapData_p;
      if(gmap->inverseJacobianMatrix.size()==0 && (ord>0 || femt==_covariantPiolaMap)) gmap->invertJacobianMatrix();
      dimen_t d=dimfun;
      for(itvx = shv_Tx->begin(); itvx != shv_Tx->end(); ++itvx)
        mapShapeValues(*relt, *melt, *gmap, mapsh_v, femt, rotsh, ord, changeSign, sign, dimfun, d, *itvx, *itvx);
      itvxb=shv_Tx->begin();
      itvxe= shv_Tx->end();
      kill_shv_Tx=true;
    }

//quadrature loops
  itpy = itpyb;
  itqy = itqyb;
  for(ituy = ituyb; ituy != ituye; ++ituy, ++itwy, ++itpy, itqy+=dqy)     //loop on y quadrature points (addressing u)
    {
      if(!firstOrderElt_u)  //curved element, update geometrical data
        {
          gmap_S->computeJacobianMatrix(itqy);    //compute jacobian at y
          gmap_S->computeDifferentialElement();   //update dif element
          difelt_S = gmap_S->differentialElement;
          if(ny!=nullptr)
            {
              gmap_S->computeOrientedNormal();     //compute oriented y-normal
              ny=&gmap_S->normalVector;           //update normal vector
            }
        }
      if(ext_y) extdata->compute(opker.ext_p, elt_S->geomElt_p, itqy, extOrder>0);
      itwx  = itwxb;
      itpx  = itpxb;
      itqx  = itqxb;
      alpha = *itwy * difelt_S;
      for(itvx = itvxb; itvx != itvxe; ++itvx, ++itwx, ++itpx, itqx+=dqx)  //loop on x quadrature points (addressing v)
        {
          if(!firstOrderElt_v)    //curved element, update geometrical data
            {
              gmap_T->computeJacobianMatrix(itqx);     //compute jacobian at x
              gmap_T->computeDifferentialElement();    //update dif element
              difelt_T = gmap_T->differentialElement;
              if(nx!=nullptr)
                {
                  gmap_T->computeOrientedNormal();     //compute oriented x-normal
                  nx=&gmap_T->normalVector;           //update normal vector
                }
            }
          if(ext_x) extdata->compute(opker.ext_p, elt_T->geomElt_p, itqx, extOrder>0);
          kuv.evalF(*itpx, *itpy, *ituy, *itvx, alpha* * itwx * difelt_T, nx, ny,
                    ieparams.dimfp_u, ieparams.dimfp_v, ieparams.opisid_u, ieparams.opisid_v,
                    ieparams.hasf_u, ieparams.hasf_v, ieparams.scalar_k, extdata,
                    val_opu, val_opv, val_opk, res);           //evaluate opkuv at Xp and Yp for all shape functions
          //thePrintStream<<"y= "<<(*itpy)<<" x="<<(*itpx)<<" val_opu="<<val_opu<<eol<<"  val_opv="<<val_opv<<eol<<"  val_opk="<<val_opk<<eol<<"  res="<<res<<eol;
        } //end loop on itx
    } //end loop on ity

  if(kill_shv_Sy)  delete shv_Sy;
  if(kill_shv_Tx)  delete shv_Tx;
  if(kill_extdata) delete extdata;

} //end of double quadrature computation


/* ================================================================================================
IE computation algorithm
================================================================================================ */
/*!IE computation of a scalar SuBiinearForm on a == unique domain ==
   subf: a single unknown bilinear form defined on a unique domains pair (assumed)
   mat: T LargeMatrix to be filled in
   space_u_p: space related to col index of LargeMatrix
   space_v_p: space related to row index of LargeMatrix
   u_p, v_p: involved unknown and test function
   vt: to pass as template the scalar type (real or complex)

   deals with scalar (T = K = real or complex)  or vector problem (T = Matrix<K> with K= real or complex)
   deals with standard IE or extended IE (requiring to extend computation to volume dofs)
   deals with a list of integration methods for regular or singular part, for close to far elements interaction
*/
template <>
template <typename T, typename K>
void ComputationAlgorithm<_IEComputation>::compute(const SuBilinearForm& subf0, LargeMatrix<T>& mat, K& vt,
    Space* space_u_p, Space* space_v_p, const Unknown* u_p, const TestFct* v_p)
{
  SuBilinearForm subf=subf0;

  if(subf.size() == 0) return;  // void bilinear form, nothing to compute !!!
  trace_p->push("SuTermMatrix::compute");

//  //manage  normal update
//  bool noUpdatedNormal=false;//enable automatic update of normals in parameters of kernel
//  #ifdef XLIFEPP_WITH_OMP
//  noUpdatedNormal=true;      //disable automatic update of normals in parameters of kernel (not thread safe)
//  #endif // XLIFEPP_WITH_OMP

  //retry spaces
  cit_vbfp it = subf.begin();
  const Space* sp_u = subf.up()->space();  //space of unknown
  const Space* sp_v = subf.vp()->space();  //space of test function
  const GeomDomain* domx = it->first->asDoubleIntgForm()->domainx(); //common domain x of v-domain integrals
  const GeomDomain* domy = it->first->asDoubleIntgForm()->domainy(); //common domain y of u-domain integrals
  const MeshDomain* mdomx = domx->meshDomain();
  const MeshDomain* mdomy = domy->meshDomain();
  bool same_interpolation = (sp_u == sp_v && domx == domy);

  //retry extension information
  bool ext_u=false, ext_v=false;
  bool sidex= mdomx->isSideDomain() && sp_v->domain()->dim() > mdomx->dim(); //true if a side x-domain and v-space is supported by a domain of larger dimension
  bool sidey= mdomy->isSideDomain() && sp_u->domain()->dim() > mdomy->dim(); //true if a side y-domain and u-space is supported by a domain of larger dimension
  for(cit_vbfp itf = subf.begin(); itf != subf.end() && !ext_u && !ext_v; ++itf)
    {
      const DoubleIntgBilinearForm* ibf=itf->first->asDoubleIntgForm();
      if(!ext_u && sidey && (ibf->opu().extensionRequired() || ibf->computationType()==_IEextComputation || ibf->computationType()==_FEextComputation)) ext_u=true;
      if(!ext_v && sidex && (ibf->opv().extensionRequired() || ibf->computationType()==_IEextComputation || ibf->computationType()==_FEextComputation)) ext_v=true;
      Extension* ext_opk = ibf->opker().ext_p;
      if(ext_opk!=nullptr) //when extension of operator on kernel,to avoid a possible data race, preload Lagrange finite element used by the extension
      {
        if(ext_opk->var_==_x)
        {
          std::set<ShapeType>::const_iterator its= mdomx->shapeTypes.begin();
          for(; its!=mdomx->shapeTypes.end(); ++its)
            findRefElement(*its, findInterpolation(_Lagrange, _standard, ext_opk->order_,_H1));
        }
        if(ext_opk->var_==_y)
        {
          std::set<ShapeType>::const_iterator its= mdomy->shapeTypes.begin();
          for(; its!=mdomy->shapeTypes.end(); ++its)
            findRefElement(*its, findInterpolation(_Lagrange, _standard, ext_opk->order_,_H1));
        }
      }
    }

  number_t nbthread=numberOfThreads();
  theThreadData.resize(nbthread);
  string_t ext="";
  if(ext_u || ext_v) ext="extended ";
  if(theVerboseLevel > 0) std::cout<<"computing IE "<<ext<<"term "<< subf.asString()<<", using "<<nbthread<<" threads: "<<std::flush;

  //compute orientation if it is not computed (outward normal if a boundary of a domain, towards infinite if an interface or a manifold)
  if(mdomx->isSideDomain() && !mdomx->orientationComputed) mdomx->setNormalOrientation();
  if(domy!=domx && mdomy->isSideDomain() && !mdomy->orientationComputed) mdomy->setNormalOrientation();
  //theCout<<"domx "<<domx->name()<<" domy "<<domy->name()<<eol<<std::flush;

  //space_u_p and space_v_p are the largest subspaces involved in the computation, may be different from the whole spaces
  Space* subsp_u = nullptr;
  if(!ext_u) subsp_u = sp_u->findSubSpace(domy, space_u_p);                 //find subspace (linked to domain domy) of space_u_p
  else subsp_u = sp_u->findSubSpace(domy->extendedDomain(false,*space_u_p->domain()), space_u_p);    //find subspace (linked to extended domain domy) of space_u_p
  if(subsp_u == nullptr) subsp_u = space_u_p;                                     //is a FESpace, not a FESubspace
  Space* subsp_v = subsp_u;
  if(!same_interpolation || ext_u!=ext_v)
    {
      if(!ext_v) subsp_v = sp_v->findSubSpace(domx, space_v_p);             //find subspace (linked to domain domx) of space_v_p
      else subsp_v = sp_v->findSubSpace(domx->extendedDomain(false,*space_v_p->domain()), space_v_p);//find subspace (linked to extended domain domx) of space_v_p
      if(subsp_v == nullptr) subsp_v = space_v_p;                                 //is a FESpace, not a FESubspace
    }
  bool doflag_u = (subsp_u == sp_u), doflag_v = (subsp_v == sp_v);
  if(!doflag_u) space_u_p->builddofid2rank();                            //build the map dofid -> rank for space_u_p
  if(!doflag_v && space_v_p!=space_u_p) space_v_p->builddofid2rank();    //build the map dofid -> rank for space_v_p
  dimen_t nbc_u = u_p->nbOfComponents(), nbc_v = v_p->nbOfComponents(); //number of components of unknown and test function
  same_interpolation = same_interpolation  && subsp_u==subsp_v;

  //scalar or vector problem
  // dimfun_u, dimfun_v are the dimensions of shape functions in the reference element
  // if the ref element is mapped to a physical element of greater dimension, dimension of shape functions has to be increased
  // occurs for element with mapType _contravariantPiolaMap or _covariantPiolaMap and  dim() > dimElt()
  dimen_t dimfun_u = sp_u->dimFun();   //dimension of u-shape functions of ref element
  dimen_t dimfunp_u = dimfun_u;         //dimension of u-shape functions of phy element
  const Element* elt = subsp_u->element_p(number_t(0));
  FEMapType femt = elt->refElt_p->mapType;
  if((femt==_contravariantPiolaMap || femt==_covariantPiolaMap) && elt->dim() > elt->dimElt()) dimfunp_u = elt->dim();
  if(nbc_u > 1)    //case of vector dofs (scalar shape functions will be extended to vector shape functions)
    {
      if(dimfun_u==1) {dimfun_u = nbc_u; dimfunp_u = nbc_u;}
      else error("fun_bad_dim", 1, dimfun_u);
    }
  dimen_t dimfun_v = sp_v->dimFun();   //dimension of v-shape functions of ref element
  dimen_t dimfunp_v = dimfun_v;         //dimension of v-shape functions of phy element
  elt = subsp_v->element_p(number_t(0));
  femt = elt->refElt_p->mapType;
  if((femt==_contravariantPiolaMap || femt==_covariantPiolaMap) && elt->dim() > elt->dimElt()) dimfunp_v = elt->dim();
  if(nbc_v > 1)
    {
      if(dimfun_v==1) {dimfun_v = nbc_v; dimfunp_v = nbc_v;}
      else error("fun_bad_dim", 1, dimfun_v);
    }

  bool sym = same_interpolation && !(mat.sym == _noSymmetry);         //symmetry flag

  //set IE computation parameters
  IEcomputationParameters ieparams;
  ieparams.dim_sp = domx->spaceDim();
  ieparams.dimf_u = dimfun_u;
  ieparams.dimf_v = dimfun_v;
  ieparams.dimfp_u = dimfunp_u;
  ieparams.dimfp_v = dimfunp_v;
  ieparams.ord_u  = sp_u->interpolation()->numtype;
  ieparams.ord_v  = sp_v->interpolation()->numtype;
  ieparams.isP0   = (sp_u->interpolation()->numtype==0) && (sp_v->interpolation()->numtype==0);
  ieparams.femt_u = subsp_u->element_p(number_t(0))->refElt_p->mapType;
  ieparams.femt_v = subsp_v->element_p(number_t(0))->refElt_p->mapType;
  ieparams.rotsh_u= subsp_u->element_p(number_t(0))->refElt_p->rotateDof;
  ieparams.rotsh_v= subsp_v->element_p(number_t(0))->refElt_p->rotateDof;
  ieparams.nb_bf = subf.size();
  //extension flags, if true means that u/v lives on a extended domain
  ieparams.extend_u = ext_u;
  ieparams.extend_v = ext_v;

  //define side numbers of extended domain elements (if required)
  std::vector<number_t> sides_u, sides_v;  //empty
  std::vector<GeomElement*> sidelts_u, sidelts_v;  //empty
  if(ieparams.extend_u)
    {
      std::map<GeomElement*, number_t> eltext;
      number_t n=subsp_u->nbOfElements();
      for(number_t k = 0; k < subsp_u->nbOfElements(); k++)
        eltext[subsp_u->element_p(k)->geomElt_p]=k;
      sides_u.resize(n);
      sidelts_u.resize(n);
      std::vector<GeomElement*>::const_iterator ite=domy->meshDomain()->geomElements.begin();
      for(; ite!=domy->meshDomain()->geomElements.end(); ++ite)
        {
          std::vector<GeoNumPair>& parents=(*ite)->parentSides(); //parent of side element
          std::vector<GeoNumPair>::iterator itgn=parents.begin();
          for(; itgn!=parents.end(); ++itgn)
            if(eltext.find(itgn->first)!=eltext.end())
              {
                number_t k=eltext[itgn->first];
                sides_u[k]=itgn->second;
                sidelts_u[k]=*ite;
              }
        }
    }
  if(ieparams.extend_v)
    {
      std::map<GeomElement*, number_t> eltext;
      number_t n=subsp_v->nbOfElements();
      for(number_t k = 0; k < subsp_v->nbOfElements(); k++)
        eltext[subsp_v->element_p(k)->geomElt_p]=k;
      sides_v.resize(n);
      sidelts_v.resize(n);
      std::vector<GeomElement*>::const_iterator ite=domx->meshDomain()->geomElements.begin();
      for(; ite!=domx->meshDomain()->geomElements.end(); ++ite)
        {
          std::vector<GeoNumPair>& parents=(*ite)->parentSides(); //parent of side element
          std::vector<GeoNumPair>::iterator itgn=parents.begin();
          for(; itgn!=parents.end(); ++itgn)
            if(eltext.find(itgn->first)!=eltext.end())
              {
                number_t k=eltext[itgn->first];
                sides_v[k]=itgn->second;
                sidelts_v[k]=*ite;
              }
        }
    }

  //collect information from bilinear form to prepare computation
  std::set<Quadrature*> quad_x, quad_y;   //to store single quadrature pointers
  bool der_x=false, der_y=false, nor_x=false, nor_y=false, der_xx=false, der_yy=false, isoGeometric=false;;
  std::list<std::multimap<real_t, IntgMeth> > intgMaps;  //list of IntegrationMethod indexed by distance for each bilinear form
  std::list<std::multimap<real_t, IntgMeth> >::iterator itiml;
  std::multimap<real_t, IntgMeth>::iterator itim, itime, itimf;
  number_t nbf = subf.size();
  std::vector<KernelOperatorOnUnknowns*> kopregs(nbf,0), kopsings(nbf,0);   //regular and singular operators for each DoubleIntgBilinearForm
  std::vector<KernelOperatorOnUnknowns*>::iterator itsin=kopsings.begin(), itreg=kopregs.begin();
  for(cit_vbfp itf = subf.begin(); itf != subf.end(); ++itf, ++itreg, ++itsin)
    {
      const DoubleIntgBilinearForm* ibf = itf->first->asDoubleIntgForm(); //basic bilinear form as double integral
      // collect info from integration methods
      intgMaps.push_back(std::multimap<real_t, IntgMeth>());
      IntegrationMethods::const_iterator itims;
      for(itims=ibf->intgMethods.begin(); itims!=ibf->intgMethods.end(); ++itims)
        {
          intgMaps.back().insert(std::pair<real_t, IntgMeth>(itims->bound,*itims)); //insert intg method in map
          // create singular and regular KernelOperatorOnUnknowns if required by integration method
          if(itims->functionPart==_singularPart)
            {
              Kernel* ker = ibf->opker().kernelp()->singPart;
              if(ker == nullptr) error("null_pointer","ker->singPart");
              OperatorOnKernel opker(ibf->opker());
              opker.changeKernel(ker);
              *itsin = new KernelOperatorOnUnknowns(ibf->opu(), ibf->algopu(), opker, ibf->algopv(), ibf->opv(), ibf->kopuv().priorityIsRight());
            }
          if(itims->functionPart==_regularPart)
            {
              Kernel* ker = ibf->opker().kernelp()->regPart;
              if(ker == nullptr) error("null_pointer","ker->regPart");
              OperatorOnKernel opker(ibf->opker());
              opker.changeKernel(ker);
              *itreg = new KernelOperatorOnUnknowns(ibf->opu(), ibf->algopu(), opker, ibf->algopv(), ibf->opv(), ibf->kopuv().priorityIsRight());
            }

          //collect quadratureIM's from double quadrature IM
          const IntegrationMethod* im = itims->intgMeth;              //integration method
          if(im->useQuadraturePoints() && im->type() == _productIM)    //double quadrature, collect quadratureIM's
            {
              const ProductIM* pim = static_cast<const ProductIM*>(im);
              QuadratureIM* qimx = static_cast<QuadratureIM*>(pim->getxIM());
              QuadratureIM* qimy = static_cast<QuadratureIM*>(pim->getyIM());
              std::list<Quadrature*> quads=qimx->quadratures();
              quad_x.insert(quads.begin(),quads.end());
              quads=qimy->quadratures();
              quad_y.insert(quads.begin(),quads.end());
            }
          else
            {
              std::list<Quadrature*> quads=im->quadratures();
              quad_x.insert(quads.begin(),quads.end());
              quad_y.insert(quads.begin(),quads.end());
            }
        }

      // collect derivative and normal requirements
      const KernelOperatorOnUnknowns& kopuv=ibf->kopuv();
      ieparams.scalar_k = true;
      if(kopuv.opker().kernelp()!=nullptr) ieparams.scalar_k = (kopuv.opker().kernelp()->strucType()==_scalar);
      if(!der_x && kopuv.diffOrder_v() > 0) der_x=true;
      if(!der_y && kopuv.diffOrder_u() > 0) der_y=true;
      if(!der_xx && kopuv.diffOrder_v() > 1) der_xx=true;
      if(!der_yy && kopuv.diffOrder_u() > 1) der_yy=true;
      if(!nor_x && (kopuv.opker().xnormalRequired() || kopuv.opv().normalRequired())) nor_x=true;
      if(!nor_y && (kopuv.opker().ynormalRequired() || kopuv.opu().normalRequired())) nor_y=true;
      //collect others
      if(ibf->isoGeometric) isoGeometric = true;
    }

  // to prevent error
  if((der_y && ieparams.ord_u==0) || (der_x && ieparams.ord_v==0))
      error("free_error",subf0.asString()+" involves derivatives with O order finite element !");
  // build isoNodes if isogeometric (in future, separate isoGeometric computations regarding domains)
  if(isoGeometric)
  {
      domy->meshDomain()->buildIsoNodes();
      if(domx!=domy) domx->meshDomain()->buildIsoNodes();
      ieparams.isoPar_y = &domy->parametrization();
      ieparams.isoPar_x = &domx->parametrization();
      ieparams.isoGeo = true;
  }
  // precompute shapevalues, geometric data, ... for each element
  preComputationIE(*domy, subsp_u, quad_y, nbc_u, false, der_y, der_yy, nor_y, ext_u, isoGeometric);
  if(!same_interpolation || domx!=domy || quad_x!=quad_y || der_x!=der_y || der_xx!=der_yy || nor_x!=nor_y || nbc_u!=nbc_v)
  {
    ieparams.useAux = subsp_u->element_p(number_t(0))->refElt_p == subsp_v->element_p(number_t(0))->refElt_p;
    preComputationIE(*domx, subsp_v,quad_x, nbc_v, ieparams.useAux, der_x, der_xx, nor_x, ext_v, isoGeometric);
  }

  //allocate temporary vector
  Vector<K> val_opu, val_opv, val_opk;
  number_t nbelt_u= subsp_u->nbOfElements();
  AdjacenceInfo adjInfo;
  std::vector<number_t> adrs, adrst;
  std::vector<number_t> dof_u, dof_v;
  Matrix<K> matel;                    //define elementary matrix structure

  //print status
  number_t nbelt_udiv10 = nbelt_u/10;
  bool show_status = (theVerboseLevel > 0 && mat.nbRows > 100 && nbelt_udiv10 > 1);
  //thePrintStream<<"\n element loop nb_elt_u = "<<nbelt_u<<eol<<std::flush;
  //--------------------------------------------------
  //main loop on finite elements of fesubspace subsp_u
  //--------------------------------------------------
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for \
  firstprivate(ieparams, dof_u, dof_v, val_opu, val_opv, val_opk, matel, adrs, adrst, itim, itime, itimf, itiml, itsin, itreg, intgMaps, adjInfo, subf) \
  shared(mat) schedule(dynamic,5)
  #endif // XLIFEPP_WITH_OMP
  for(number_t ku = 0; ku < nbelt_u; ku++)
    {
      // data related to u
      //thePrintStream<<"elt "<<ku<<std::flush;
      const Element* elt_u = subsp_u->element_p(ku);
      if(elt_u->geomElt_p->meshElement()==nullptr) elt_u->geomElt_p->buildSideMeshElement();
      const std::vector<number_t>* dofNum_u = nullptr;
      if(doflag_u) dofNum_u = &subsp_u->elementDofs(ku);  //dof numbers (local numbering) of element for u
      else
        {
          ranks(space_u_p->dofid2rank(), elt_u->dofNumbers, dof_u); //numbering related to parent, use ranks function with map dofid2rank
          dofNum_u = &dof_u;
        }
      number_t nb_u = dofNum_u->size();
      //thePrintStream<<" nb_u "<<nb_u<<eol<<std::flush;
      GeomElement* sidelt_u = elt_u->geomElt_p;
      if(ieparams.extend_u)
        {
          sidelt_u = sidelts_u[ku];
          ieparams.side_u=sides_u[ku];
        }
      ieparams.melt_u = sidelt_u->meshElement();
      ieparams.sh_u = sidelt_u->shapeType();
      ieparams.changeSign_u=false;
      if(elt_u->refElt_p->dofCompatibility == _signDofCompatibility)
        {
          ieparams.sign_u=&elt_u->getDofSigns();
          ieparams.changeSign_u=ieparams.sign_u->size()!=0;
        }

      //transmit y-normal to kernels if required
      // ### Note: normal is not yet transmitted to regular and singular kernel part: to do in future
      bool sny=false;
      for(cit_vbfp itf = subf.begin(); itf != subf.end() && !sny; itf++)
        {
//          const KernelOperatorOnUnknowns& kopuv= itf->first->asDoubleIntgForm()->kopuv();
          if(itf->first->asDoubleIntgForm()->opker().ynormalRequired())
            {
              setNy(&ieparams.melt_u->geomMapData_p->normalVector);   //update y normal in thread data
              sny=true;
            }
        }
      //main loop on finite elements of fesubspace subsp_v
      //--------------------------------------------------
      for(number_t kv = 0; kv < subsp_v->nbOfElements(); kv++)
        {
          //thePrintStream<<"     elt_v "<<kv<<eol<<std::flush;
          // data related to v
          const Element* elt_v = subsp_v->element_p(kv);
          const std::vector<number_t>* dofNum_v = nullptr;
          if(doflag_v) dofNum_v = &subsp_v->elementDofs(kv);  //dof numbers (local numbering) of element for u
          else
            {
              ranks(space_v_p->dofid2rank(), elt_v->dofNumbers, dof_v);//numbering related to parent, use ranks function with map dofid2rank
              dofNum_v = &dof_v;
            }
          number_t nb_v = dofNum_v->size();
          GeomElement* sidelt_v = elt_v->geomElt_p;
          if(ieparams.extend_v)
            {
              sidelt_v = sidelts_v[kv];
              ieparams.side_v=sides_v[kv];
            }
          ieparams.melt_v = sidelt_v->meshElement();
          ieparams.sh_v = sidelt_v->shapeType();
          ieparams.changeSign_v=false;
          if(elt_v->refElt_p->dofCompatibility == _signDofCompatibility)
            {
              ieparams.sign_v=&elt_v->getDofSigns();
              ieparams.changeSign_v=ieparams.sign_v->size()!=0;
            }
          // local to global numbering
          mat.positions(*dofNum_v, *dofNum_u, adrs, true); //adresses in mat of elementary matrix stored as a row vector
          std::vector<number_t>::iterator itadrs = adrs.begin(), itadrst, itk;
          if(same_interpolation && ku!=kv)  //used for computation with symmetry
            {
              mat.positions(*dofNum_u, *dofNum_v, adrst, true); //adresses in mat of elementary matrix stored as a row vector (transposed)
              itadrst=adrst.begin();
            }
          number_t nb_ut = nb_u * nbc_u, nb_vt = nb_v * nbc_v;

          adjInfo.init(*sidelt_u, *sidelt_v, true, false);  //status of elements pair (distance, adjacent by edge, by face, ...)
          itiml=intgMaps.begin();
          itsin=kopsings.begin();
          itreg=kopregs.begin();

          //loop on basic bilinear forms, compute elementary matrix on elt_v x elt_u
          //-------------------------------------------------------------------------
          for(cit_vbfp itf = subf.begin(); itf != subf.end(); ++itf, ++itiml, ++itreg, ++itsin)
            {
              const DoubleIntgBilinearForm* ibf = itf->first->asDoubleIntgForm(); //basic bilinear form as double integral
              const OperatorOnKernel& opk = ibf->opker();       //operator on kernel
              bool inext=true;
              Extension* ext=opk.ext_p;
              if(ext!=nullptr)  // opker has an extension, restrict to elements in domain extension
                {
                  std::map<GeomElement*,std::set<number_t> >::const_iterator itme=ext->domToSide.end();
                  if(ext->var_==_x) if(ext->domToSide.find(sidelt_v)==itme) inext=false;
                  if(ext->var_==_y) if(ext->domToSide.find(sidelt_u)==itme) inext=false;
                }
              SymType symform= ibf->symmetry;

              if(inext && (!same_interpolation || symform == _noSymmetry || ku <= kv))  // computation ku x kv
                {
                  const OperatorOnUnknown& op_u = ibf->opu();       //operator on unknown
                  const OperatorOnUnknown& op_v = ibf->opv();       //operator on test function
                  if(!ieparams.isUptodate || ieparams.nb_bf > 1)    //update computation parameters
                    {
                      ieparams.opisid_u = (op_u.difOpType()==_id);
                      ieparams.opisid_v = (op_v.difOpType()==_id);
                      ieparams.hasf_u = op_u.hasOperand();
                      ieparams.hasf_v = op_v.hasOperand();
                      ieparams.difforder_u = op_u.diffOrder();
                      ieparams.difforder_v = op_v.diffOrder();
                      ieparams.isId = (ieparams.opisid_u && ieparams.opisid_v && !ieparams.hasf_u && !ieparams.hasf_v);
                      if(opk.kernelp()!=nullptr) ieparams.scalar_k = (opk.kernelp()->strucType()==_scalar);
                      ieparams.knor_x=opk.xnormalRequired();
                      ieparams.knor_y=opk.ynormalRequired();
                      ieparams.isUptodate = true;
                    }

                  //transmit x/y-normal to kernels if required (block it if opk.noUpdatedNormal=true;)
                  if(ieparams.knor_x)
                    setNx(&ieparams.melt_v->geomMapData_p->normalVector);  //update x normal in kernel

                  // compute integral
                  K coef = complexToT<K>(itf->second);                        //coefficient of linear form
                  const KernelOperatorOnUnknowns* kopus = &ibf->kopuv();
                  matel.changesize(dimen_t(nb_vt), dimen_t(nb_ut), K(0));     //reset elementary matrix to 0
                  bool first =true;
                  std::multimap<real_t, IntgMeth>& intgMap=*itiml;            //integration methods indexed by bound
                  itim = intgMap.lower_bound(adjInfo.dist);
                  itime= intgMap.upper_bound(adjInfo.dist);
                  if(itim==itime) itime = intgMap.upper_bound(itim->first);
                  for(; itim!=itime; ++itim)  // loop on integration methods, 2 if compute singular and regular part, 1 else
                    {
                      const IntegrationMethod* im = itim->second.intgMeth;
                      switch(itim->second.functionPart)
                        {
                          case _regularPart: kopus = *itreg; break;
                          case _singularPart: kopus = *itsin; break;
                          default: break;
                        }
                      if(!first) matel*=K(0);
                      first = false;
                      if(im->type()==_productIM)   //generic quadrature method
                        {
                          if(!im->useQuadraturePoints()) error("im_not_product");
                          Quadrature* qimx= static_cast<QuadratureIM*>(static_cast<const ProductIM*>(im)->getxIM())->getQuadrature(sidelt_v->shapeType()),
                                    * qimy= static_cast<QuadratureIM*>(static_cast<const ProductIM*>(im)->getyIM())->getQuadrature(sidelt_u->shapeType());
                          computeQuadratureIE(elt_u, elt_v, *kopus, qimx, qimy, matel, ieparams, val_opu, val_opv, val_opk);
                        }
                      else //singular or special integration method
                        {
                          reinterpret_cast<const DoubleIM*>(im)->computeIE(elt_u, elt_v, adjInfo, *kopus, matel, ieparams, val_opu, val_opv, val_opk);
                        }
                      matel*=coef;
//                      thePrintStream<<" ku="<<ku<<" kv="<<kv<<"   dofNum_u="<<*dofNum_u<<"  dofNum_v="<<*dofNum_v<<"intg meth="<<im->name;
//                      thePrintStream<<"\n   adrs="<<adrs<<eol<<"  matel="<<eol<<matel<<eol<<std::flush;

                      //assembling matel in global matrix
                      std::vector<number_t>::const_iterator itd_u, itd_v;   //dof numbering iterators
                      typename Matrix<K>::iterator itmatel = matel.begin(), itm_u;
                      number_t incr = nbc_u * nbc_v * nb_u, i=0; //block row increment in matel
                      //loop on (vector) dofs in u and v
                      for(itd_v = dofNum_v->begin(); itd_v != dofNum_v->end() ; itd_v++, itmatel += incr, i++)
                        {
                          itm_u = itmatel;
                          itk = itadrs + i * nb_u;
                          for(itd_u = dofNum_u->begin(); itd_u != dofNum_u->end() ; itd_u++, itm_u += nbc_u, itk++)
                            {
                              if(!sym || *itd_v >= *itd_u) assemblyMat(mat(*itk), itm_u, nb_ut);
                            }
                        }

                      //assembling transpose of matel in global matrix if form has symmetry (kv x ku) - save time computation -
                      if(same_interpolation && symform != _noSymmetry && ku!=kv)
                        {
                          Matrix<K> matelt;
                          switch(symform)
                            {
                              case _symmetric:    matelt =  transpose(matel); break;
                              case _selfAdjoint:    matelt =  adjoint(matel);   break;
                              case _skewSymmetric: matelt = -transpose(matel); break;
                              case _skewAdjoint:   matelt = -adjoint(matel);   break;
                              default: break;
                            }
//                        thePrintStream<<"  adrst = "<<adrst<<eol<<"  matelt = "<<eol<<matelt<<eol;
                          typename Matrix<K>::iterator itm_v;
                          itmatel = matelt.begin();
                          number_t incr = nbc_v * nbc_u * nb_v, i=0; //block row increment in matel
                          //loop on (vector) dofs in u and v
                          for(itd_u = dofNum_u->begin(); itd_u != dofNum_u->end() ; itd_u++, itmatel += incr, i++)
                            {
                              itm_v = itmatel;
                              itk = itadrst + i * nb_v;
                              for(itd_v = dofNum_v->begin(); itd_v != dofNum_v->end() ; itd_v++, itm_v += nbc_v, itk++)
                                {
                                  if(!sym || *itd_u >= *itd_v) assemblyMat(mat(*itk), itm_v, nb_vt);
                                }
                            }
                        }
                    }
                } //end if(inext && (!same_interpolation || symform == _noSymmetry || ku <= kv))
            } //end loop bilinear form
        }//end loop elt_v
      if(show_status && ku%nbelt_udiv10==0 && ku!=0)     //progress status
        {
          std::cout<< ku/nbelt_udiv10 <<"0% "<<std::flush;
        }
    }//end loop elt_u

  //delete opregs and opsins
  itsin=kopsings.begin();
  itreg=kopregs.begin();
  for(; itsin != kopsings.end(); ++itreg, ++itsin)
    {
      if(*itsin!=nullptr) delete *itsin;
      if(*itreg!=nullptr) delete *itreg;
    }

  //cpuTime("IE computation loop");
  if(theVerboseLevel > 0) std::cout<<" done"<<eol<<std::flush;
  trace_p->pop();
}

/*!
  FE computation of a scalar SuLinearForm with kernels on a == unique domain ==
              Rij= intg_gamma opk(xi,y)*op(wj)(y) dy

  \param sulf: a single unknown linear form defined on a unique domain (assumed)
  \param vt: to pass as template the scalar type (real or complex)
  \param xs: points where to evaluate IR
  \param nxs: pointers to normal where to evaluate IR
  \param mat: pointer to a matrix stored as row dense, storing the result Rij

  example:  intg_gamma G(x,y)*U(y) dy

  Note: U comes from a SuTermVector having same unknown as SuLinearForm
*/

template<typename T, typename K>
void SuTermMatrix::computeIR(const SuLinearForm& sulf, T* mat, K& vt, const std::vector<Point>& xs, const std::vector<Vector<real_t> >* nxs)
{
  if(sulf.size() == 0) return;  // void linear form, nothing to compute !!!
  trace_p->push("SuTermMatrix::computeIR");

  if(theVerboseLevel > 0) std::cout<<"computing IR matrix "<< sulf.asString()<<" on "<<tostring(xs.size())
                                   <<" points, using "<<numberOfThreads()<<" threads: "<<std::flush;

  const Space* sp = sulf.space();                                      //space of unknown
  const GeomDomain* dom = sulf.begin()->first->asIntgForm()->domain(); //common domain of integrals
  const Space* subsp = sp->findSubSpace(dom, sp);                      //find subspace (linked to domain dom) of sp
  if(subsp == nullptr) subsp = new Space(*dom, const_cast<Space&>(*sp));      //subspace not constructed, construct it
  bool doflag = (subsp == sp);
  const Unknown* u = sulf.unknown();                                   //unknown of linear form
  RefElement* relt = subsp->element_p(number_t(0))->refElt_p;          //first ref element
  bool useColor=dom->meshDomain()->useColor;
  if(!doflag) sp->builddofid2rank();

  //global parameters
  number_t nbcol = subsp->dimSpace();                                //number of columns of matrix mat
  dimen_t nbc = u->nbOfComponents();                                 //number of components of unknown
  dimen_t dimf  = sp->dimFun();
  if(nbc > 1) dimf=nbc;                  //case of vector dofs (scalar shape functions will be extended to vector shape functions)
  number_t ord   = sp->interpolation()->numtype;
  bool isP0      = (sp->interpolation()->numtype==0);
  number_t nb_lf = sulf.size();
  const IntgLinearForm* ilf = nullptr;
  const OperatorOnUnknown* op=nullptr;
  const IntegrationMethod* im = nullptr;
  IntegrationMethodType imtype = _quadratureIM;
  bool firstOrderElt = dom->order() == 1;
  bool hasKer = true, hasFun = false;
  bool isId = false;
  bool opkerisId = true;
  FEMapType mapType = relt->mapType;
  bool mapsh = false;
  bool changeSignTest = (relt->dofCompatibility == _signDofCompatibility);   //dof sign change for particular element (Nedelec, Raviart, ....)
  number_t der_ord = 0;
  K coef ;
  bool changeSign = false;
  Vector<real_t>* sign=nullptr;
  Vector<real_t>* ny = nullptr;

  //collect information from linear form to prepare computation
  std::set<Quadrature*> quad;   //to store single quadrature pointers
  const Kernel* kerp=nullptr;
  std::list<std::multimap<real_t, IntgMeth> > intgMaps;  //list of IntegrationMethod indexed by distance for each bilinear form
  std::list<std::multimap<real_t, IntgMeth> >::iterator itiml;
  std::multimap<real_t, IntgMeth>::iterator itim, itime, itimf;
  number_t nlf = sulf.size();
  std::vector<OperatorOnUnknown*> opregs(nlf,0), opsings(nlf,0);   //regular and singular operators for each DoubleIntgBilinearForm
  std::vector<OperatorOnUnknown*>::iterator itsin=opsings.begin(), itreg=opregs.begin();
  bool nor=false, norx=false;
  for(cit_vlfp itf = sulf.begin(); itf != sulf.end(); ++itf, ++itreg, ++itsin)
    {
      ilf = itf->first->asIntgForm();                  //basic linear form as single integral
      op = ilf->opu();                                 //operator on unknown
      isId = op->difOp().type()==_id;                  //operator on unknown is Id: (opker)*v
      opkerisId = op->opkernelp()->isId();             //operator on kernel is Id:  ker*v
      coef = complexToT<K>(itf->second);               //coefficient of linear form
      // collect derivative and normal requirements
      der_ord = std::max(der_ord,number_t(op->diffOrder()));
      if(op->normalRequired()) nor=true;
      if(op->xnormalRequired()) norx=true;
      mapsh = (mapType !=_standardMap || der_ord>0);
      kerp=op->kernelp();                       //id if null, no singular part
      op->opkernelp()->noUpdatedNormal=true;    //say the normal in kernel parameter is not to be updated
      // collect info from integration methods
      intgMaps.push_back(std::multimap<real_t, IntgMeth>());
      IntegrationMethods::const_iterator itims;
      for(itims=ilf->intgMethods.begin(); itims!=ilf->intgMethods.end(); ++itims)
        {
          intgMaps.back().insert(std::pair<real_t, IntgMeth>(itims->bound,*itims)); //insert intg method in map
          // create singular and regular KernelOperatorOnUnknowns if required by integration method
          if(kerp!=nullptr && itims->functionPart==_singularPart)
            {
              Kernel* ker = kerp->singPart;
              if(ker == nullptr) error("null_pointer", "ker->singPart");
              OperatorOnUnknown* opsing = new OperatorOnUnknown(*op);    //copy of op
              opsing->changeKernel(ker);
              opsing->opkernelp()->noUpdatedNormal=true;
              *itsin =opsing;
            }
          if(kerp!=nullptr && itims->functionPart==_regularPart)
            {
              Kernel* ker = kerp->regPart;
              if(ker == nullptr) error("null_pointer", "ker->regPart");
              OperatorOnUnknown* opreg = new OperatorOnUnknown(*op);    //copy of op
              opreg->changeKernel(ker);
              opreg->opkernelp()->noUpdatedNormal=true;
              *itreg =opreg;
            }
          //collect quadratureIM's from intgMeth
          const IntegrationMethod* im = itims->intgMeth;
          if(!nor && im->requireNormal) nor =true;  //special case of ing method requiring normal (e.g LenoirSallesIR)
          std::list<Quadrature*> quads=im->quadratures();
          if(quads.size()>0) quad.insert(quads.begin(),quads.end());
        }
    }

  //precomputation of shapevalues, physical points
  preComputationIE(*dom, subsp, quad, nbc, false, der_ord > 0, der_ord > 0, nor);
  number_t nbxs=xs.size();

  //print status
  number_t nbelt = subsp->nbOfElements(), nbelt_div10 = nbelt/10;
  bool show_status = (theVerboseLevel > 0 && xs.size() > 100 && nbelt_div10 > 1);

  //main loop on finite elements of fesubspace
  for(number_t k = 0; k < subsp->nbOfElements(); k++)
    {
      const Element* elt = subsp->element_p(k);
      GeomElement* gelt = elt->geomElt_p;          //geometric element
      if(!useColor || gelt->color == 0)   // do not take into account element with non null color if useColor flag is on
        {
          RefElement*  relt = elt->refElt_p;           //ref element
          MeshElement* melt = gelt->meshElement();     //mesh element related to
          GeomMapData* mapdata = melt->geomMapData_p;  //geometrical data
          real_t difelt = mapdata->differentialElement;//dif element
          ShapeType sh = gelt->shapeType();            //shape type of element
          Point& center = melt->centroid;
          real_t diam = melt->size;
          if(changeSignTest)
            {
              sign=&elt->getDofSigns();
              changeSign = sign->size()!=0;
            }
          if(nor) ny = &melt->geomMapData_p->normalVector;     //update normal if required
          std::vector<number_t> dofNum;
          if(doflag) dofNum = subsp->elementDofs(k);    //dof numbers (local numbering) of element
          else ranks(sp->dofid2rank(), elt->dofNumbers, dofNum);//numbering related to parent, use ranks function
          //else dofNum = subsp->elementParentDofs(k);   //dof numbers (parent numbering) of element
          number_t nbu=dofNum.size();
          std::vector<Vector<K> > val;
          bool norop=nor;
          bool isSimplex= relt->isSimplex();
          itiml=intgMaps.begin();
          itsin=opsings.begin();
          itreg=opregs.begin();

          //loop on basic linear forms
          for(cit_vlfp itf = sulf.begin(); itf != sulf.end(); ++itf, ++itiml, ++itreg, ++itsin)
            {
              if(nb_lf > 1)  //update linear form parameters
                {
                  ilf  = itf->first->asIntgForm();       //basic linear form as single integral
                  op   = ilf->opu();                     //operator on unknown
                  im   = ilf->intgMethod();              //integration method
                  imtype = im->type();                   //integration method type
                  coef = complexToT<K>(itf->second);     //coefficient of linear form
                  isId = op->difOp().type()==_id;
                  opkerisId = op->opkernelp()->isId();   //operator on kernel is Id:  ker*v
                  der_ord = op->diffOrder();
                  norop = op->normalRequired();
                  norx  = op->xnormalRequired();
                  mapsh = (relt->mapType !=_standardMap || der_ord>0);
                  kerp=op->kernelp();
                }
              std::multimap<real_t, IntgMeth>& intgMap=*itiml;            //integration methods indexed by bound
              number_t nbi=intgMap.size();
              #ifdef XLIFEPP_WITH_OMP
              #pragma omp parallel for firstprivate(val, itim, itime) schedule(dynamic)
              #endif // XLIFEPP_WITH_OMP
              for(int_t i=0; i<nbxs; i++)  //loop on x point
                {
                  const OperatorOnUnknown* opu = op;
                  real_t dist = xs[i].distance(center)/diam; //relative distance
                  if(nbi>1)
                    {
                      itim = intgMap.lower_bound(dist);
                      itime= intgMap.upper_bound(dist);
                      if(itim==itime) itime= intgMap.upper_bound(itim->first);
                    }
                  else
                    {
                      itim = intgMap.begin();
                      itime= intgMap.end();
                    }
                  for(; itim!=itime; ++itim)  // loop on integration methods, 2 if compute singular and regular part, 1 else
                    {
                      switch(itim->second.functionPart)
                        {
                          case _regularPart:
                            opu = *itreg;
                            break;
                          case _singularPart:
                            opu = *itsin;
                            break;
                          default:
                            break;
                        }
                      const IntegrationMethod* im = itim->second.intgMeth;
                      dimen_t d,m;  // block size of val (d size of block, m number of vectors in block -> d/m size of each vector)
                      Vector<real_t>* nx = nullptr;
                      if(norx && nxs!=nullptr) nx = const_cast<Vector<real_t>*>(&((*nxs)[i]));    //update nx pointer, not transmitted to kernels (not thread safe)
                      if(im->imType==_quadratureIM)
                        {
                          Quadrature* quad = static_cast<const QuadratureIM*>(im)->quadratures_[sh];
                          std::vector<ShapeValues>* shv = &relt->qshvs_[quad]; //retry shape values at quadrature points
                          std::vector<Point>& phypts = mapdata->phyPoints[quad];//quadrature points in physical space
                          number_t nbq=quad->numberOfPoints();
                          val.resize(nbq);
                          computeOperatorByQuadrature(*elt, isP0, *opu, isId, opkerisId, quad, nbq, quad->dim(), shv, phypts,
                                                      ord, dimf, nbc, firstOrderElt && isSimplex, der_ord, norop, hasFun, hasKer, mapsh, mapType,
                                                      changeSign, sign, difelt, mapdata, xs[i], nx, ny, val, d, m);
                          // assembly
                          std::vector<number_t>::iterator itn = dofNum.begin();
                          for(number_t j=0; j<nbu; ++j, ++itn)
                            {
                              T t=T(0);
                              for(number_t q = 0; q < nbq; q++)  t +=  val[q][j];
                              t*=coef;
                              number_t ad = i * nbcol + *itn -1;
                              *(mat + ad)+=t;
                            }
                        }
                      else //works only in scalar
                        {
                          if(val.size()==0) val.resize(1);
                          val[0].resize(nbc*nbu);
                          reinterpret_cast<const SingleIM*>(im)->computeIR(elt, xs[i], *opu, ord, nx, ny, val[0],d);
                          std::vector<number_t>::iterator itn = dofNum.begin();
                          for(number_t j=0; j<nbu; ++j, ++itn)
                            {
                              number_t ad = i * nbcol + *itn -1;
                              *(mat + ad)+= val[0][j]*coef;
                            }
                        }
                    }
                }
            }//end of SuLinearForm  loop
          if(show_status && k%nbelt_div10==0 && k!=0)     //progress status
            {
              std::cout<< k/nbelt_div10 <<"0% "<<std::flush;
            }
        }
    } //end of element loop
  if(theVerboseLevel > 0) std::cout<<" done"<<eol<<std::flush;
  trace_p->pop();
}

} // end of namespace xlifepp
