/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file FEVectorComputation.hpp
  \author E. Lunéville
  \since 9 jan 2013
  \date 21 juin 2013

  \brief Implementation of templated FE TermVector computation functions

  this file is included by SuTermVector.hpp
  do not include it elsewhere
*/

#include "FEOperatorComputation.hpp"

/* ================================================================================================
                                      tools
   ================================================================================================ */

//vector and scalar assembly functions
template <typename K, typename IteratorW >
inline void assembly(Vector<K>& v, IteratorW itW, dimen_t nbc, const K& coeff)
{
  typename Vector<K>::iterator itV=v.begin();
  for(dimen_t k=0; k<nbc; k++,itV++,itW++) *itV+=coeff** itW;
}

template <typename K, typename IteratorW >
inline void assembly(K& v, IteratorW itW, dimen_t nbc, const K& coeff)
{
  v+=coeff** itW;
}

// OMP assembly
template <typename IteratorW >
inline void assemblyOmp(real_t& v, IteratorW itW, dimen_t nbc, const real_t& coeff)
{
  #ifdef XLIFEPP_WITH_OMP
    #pragma omp atomic
  #endif // XLIFEPP_WITH_OMP
  v+= coeff * *itW;
}

template <typename IteratorW >
inline void assemblyOmp(complex_t& v, IteratorW itW, dimen_t nbc, const complex_t& coeff)
{
  #ifdef XLIFEPP_WITH_OMP
    complex_t r = coeff * *itW;
    real_t* vr = reinterpret_cast<real_t*>(&v), *vc = vr+1;  //trick to move to real in order atomic works
    #pragma omp atomic
    *vr += r.real();
    #pragma omp atomic
    *vc += r.imag();
  #else
    v+=coeff.real()* *itW;
  #endif // XLIFEPP_WITH_OMP
}

//vector and scalar assembly functions
template <typename IteratorW >
inline void assemblyOmp(Vector<real_t>& v, IteratorW itW, dimen_t nbc, const real_t& coeff)
{
  typename Vector<real_t>::iterator itV=v.begin();
  for(dimen_t k=0; k<nbc; k++,itV++,itW++)
  {
    #ifdef XLIFEPP_WITH_OMP
      #pragma omp atomic
    #endif // XLIFEPP_WITH_OMP
    *itV+=coeff** itW;
  }
}

template <typename IteratorW >
inline void assemblyOmp(Vector<complex_t>& v, IteratorW itW, dimen_t nbc, const complex_t& coeff)
{
  typename Vector<complex_t>::iterator itV=v.begin();
  for(dimen_t k=0; k<nbc; k++,itV++,itW++)
  {
    #ifdef XLIFEPP_WITH_OMP
      complex_t r = coeff * *itW;
      real_t* vr = reinterpret_cast<real_t*>(&*itV), *vc = vr+1;  //trick to move to real in order atomic works
      #pragma omp atomic
      *vr += r.real();
      #pragma omp atomic
      *vc += r.imag();
    #else
      *itV+=coeff** itW;
    #endif
  }
}


// assembly function used by computeIR
template <typename T, typename K>
inline void assembly(const std::vector<Vector<K> >& val, const Vector<T>& vu, T* resi, number_t nbdof, dimen_t nbc, dimen_t d, dimen_t m, const K& coef)
{
  number_t nbq = val.size();
  if(nbc==1)  //scalar unknown
    {
      if(d==1) //res is a vector of scalars
        {
          T t=T(0);
          for(number_t q = 0; q < nbq; q++)   t += coef * complexToT<T>(dotRC(val[q], vu));
          *resi+=t;
        }
      else  //res is a vector of vectors of size d>1, has to be equal to s
        {

          for(number_t j=0; j<d; j++)
            {
              T t=T(0);
              for(number_t q = 0; q < nbq; q++)
                {
                  const Vector<K>& vq=val[q];
                  for(number_t k = 0; k < nbdof; k++)  t += coef * vq[k*d+j]*vu[k];
                }
              *(resi+j)+=t;

            }
        }
    }
  else // vector unknown
    {
      if(d==1) //res is a vector of vectors of size nbc>1, has to be equal to s
        {
          for(number_t j=0; j<d; j++)
            {
              T t=T(0);
              for(number_t q = 0; q < nbq; q++)
                {
                  const Vector<K>& vq=val[q];
                  for(number_t k = 0; k < nbdof; k++) t += coef * val[q][k]*vu[k*nbc+j];
                }
              *(resi+j)+=t;
            }
        }
      else //res is a vector of vectors of size nbc>1, has to be equal to s
        error("free_error","SuTermVector::computeIR does not handle yet operator on vector unknown returning a vector or a matrix");
    }
}

/* ================================================================================================
                                    FE computation algorithm
   ================================================================================================ */

/*!
  FE computation of a scalar SuLinearForm on a == unique domain ==
  \param sulf: a single unknown linear form defined on a unique domain (assumed)
  \param v: reference to vector entries of type T
  \param vt: to pass as template the scalar type (real or complex)
*/
template<typename T, typename K>
void SuTermVector::computeFE(const SuLinearForm& sulf, Vector<T>& v, K& vt)
{
  if(sulf.size() == 0) return; // void linear form, nothing to compute !!!
  trace_p->push("SuTermVector::computeFE");

  cit_vlfp it = sulf.begin();
  const Space* sp = sulf.space();  //space of unknown
  const GeomDomain* dom = it->first->asIntgForm()->domain(); //common domain of integrals
  const MeshDomain* mdom = dom->meshDomain();

  bool ext=false;
  const GeomDomain* extdom=nullptr;
  if(mdom->isSideDomain() && dom!=sp->domain())  //detect if extension exists or if it is required when computing on a side domain
    {
      for(cit_vlfp itf = sulf.begin(); itf != sulf.end() && !ext; ++itf)
      {
        const IntgLinearForm* ilf = itf->first->asIntgForm();
        if(ilf->extDom_p()!=nullptr || ilf->compuType==_FEextComputation || ilf->opu()->extensionRequired()) ext=true;
        if(ilf->extDom_p()!=nullptr)
        {
            if(extdom==nullptr) extdom=ilf->extDom_p();
            else if(extdom!=ilf->extDom_p())
              error("free_error"," two different extended domains required in computation; not yet handle");
        }
      }
    }

  //space_p is the largest subspace involved in the computation, may be different from the whole space sp
  const Space* subsp = nullptr;
  if(!ext) subsp = sp->findSubSpace(dom, space_p);              //find subspace (linked to domain dom) of space_p
  else
  {
    if(extdom==nullptr) extdom = dom->extendedDomain(false,*sp->domain()); // find or create extended domain
    subsp = sp->findSubSpace(extdom, space_p);          //find subspace (linked to extended domain dom) of sp
  }
  if(subsp == nullptr) subsp = space_p;                               //is a FESpace, not a FESubspace
  bool doflag=(subsp == space_p);
  if(!doflag) space_p->builddofid2rank();                       //build map dofid->rank

  //shape functions dimension
  dimen_t nbc = u_p->nbOfComponents();                     //number of components of unknown
  dimen_t dimfun = u_p->dimFun();                          //dimension of shape functions
  if(dimfun >1) dimfun += u_p->space()->dimPoint()-u_p->space()->dimDomain();  //modify dimension because of mapping mD->nD
  if(nbc > 1)   //case of vector dofs (scalar shape functions will be extended to vector shape functions)
    {
      if(dimfun==1) dimfun = nbc;                        //case of vector dofs (scalar shape functions will be extended to vector shape functions)
      else error("free_error"," SuTermVector::computeFE does not support vector unknown with vector shape functions");
    }

  //collect information from linear form to prepare computation
  bool der=false, nor=false, isoGeometric = false;
  dimen_t dimres = 0;
  for(cit_vlfp itf = sulf.begin(); itf != sulf.end(); ++itf)
    {
      const OperatorOnUnknown* opu = itf->first->asIntgForm()->opu();
      if(!der && opu->diffOrder()>0) der=true;
      if(!nor && opu->normalRequired()) nor=true;
      dimres = opu->dimsRes().first;
      if(itf->first->asIntgForm()->isoGeometric) isoGeometric = true;
    }
  if(nor && mdom->isSideDomain() && !mdom->orientationComputed) mdom->setNormalOrientation();
  nbc=std::max(dimres,nbc);  // NOTE: dimres and nbc should not be greater than 1 at the same time

  const std::map<GeomElement*, std::list<GeoNumPair> > sideToExtF;  //fake map
  const std::map<GeomElement*, std::list<GeoNumPair> >* sideToExt = &sideToExtF;
//  if(ext) sideToExt = &mdom->buildSideToExt();  //map side element -> list of extension elements
  if(ext) sideToExt = &extdom->meshDomain()->sideToExt;  //map side element -> list of extension elements
  std::map<GeomElement*, std::list<GeoNumPair> >::const_iterator itse=sideToExt->begin();
  std::list<GeoNumPair>::const_iterator itsel;

  //retry FE information
  number_t nbelt = dom->numberOfElements();

  //to prevent data race
  space_p->buildgelt2elt();
  if(isoGeometric) mdom->buildIsoNodes();

  //temporary data structure to store shapevalues
  std::vector<Point> phyPoint;
  std::vector<Vector<K> > val;

  //main loop on domain elements
  //----------------------------
//  #ifdef XLIFEPP_WITH_OMP
//  #pragma omp parallel for firstprivate(phyPoint,val,itse,itsel) shared(v) schedule(dynamic)
//  #endif // XLIFEPP_WITH_OMP  //data race due to shape values computation,to be done later using the precomputeFE machinery
  for(number_t k = 0; k < nbelt; k++)
    {
      GeomElement* gelt = mdom->geomElements[k];    //geometric element
      const Element* elt = nullptr;
      RefElement* relt =nullptr;
      std::vector<number_t> dofNum;
      number_t nbdof=0, nbse=1;
      if(ext)
        {
          itse = sideToExt->find(gelt);       // list of extended elements connected to side element
          itsel= itse->second.begin();
          nbse = itse->second.size();
        }
      // loop on extended element if extension else deal with current elt (nbse=1)
      for(number_t s=0; s<nbse; s++)
        {
          number_t sidenum = 0;
          if(!ext) //no extension
            {
              number_t numelt = subsp->numElement(gelt);
              elt = subsp->element_p(numelt);                   //finite element
              relt = elt->refElt_p;                            //reference finite element
              if(doflag) dofNum = subsp->elementDofs(numelt);  //dof numbers (local numbering) of element
              else ranks(space_p->dofid2rank(), elt->dofNumbers, dofNum); //use ranks function with map dofid2rank (faster)
              nbdof = dofNum.size();
            }
          else //extension
            {
              GeomElement* gelt_ext = itsel->first;//geometric plain element (parent)
              sidenum = itsel->second;             //side number of the current side in extended element
              elt = subsp->element_p(gelt_ext);    //extended element
              relt = elt->refElt_p;                //reference element
              ranks(space_p->dofid2rank(), elt->dofNumbers, dofNum); //use ranks function with map dofid2rank (faster)
              nbdof =elt->dofNumbers.size();
              itsel++;
            }
//          thePrintStream<<"elt k="<<k<<" dofNum="<<dofNum<<eol;
          //loop on basic linear forms
          for(cit_vlfp itf = sulf.begin(); itf != sulf.end(); itf++)
            {
              const IntgLinearForm* ilf = itf->first->asIntgForm();   //basic linear form as single integral
              const OperatorOnUnknown* op = ilf->opu();               //operator on unknown
              K coef = complexToT<K>(itf->second);                    //coefficient of linear form
              if(ilf->intgMethod()->type()==_quadratureIM)
                {
                  const QuadratureIM* qim=static_cast<const QuadratureIM*>(ilf->intgMethod());
                  computeOperatorByQuadrature(gelt, *elt, *op, *qim, ilf->isoGeometric, mdom, val, phyPoint, sidenum);
                }
              else
                {
                  error("im_not_handled",words("integration", ilf->intgMethod()->type()));
                }
              //thePrintStream<<"dofnum="<<dofNum<<" val="<<val<<eol;
              //assembly in vector entry
              std::vector<number_t>::iterator itdn;
              typename std::vector<Vector<K> >::iterator itvq=val.begin();
              for(; itvq!=val.end(); itvq++)
                {
                  typename Vector<K>::iterator itv = itvq->begin();
                  for(itdn = dofNum.begin(); itdn != dofNum.end() ; itdn++, itv+=nbc)
                    assembly(*(v.begin()+(*itdn)-1),itv,nbc,coef);
                }
            }
        } //end of extended element loop
    } //end of element loop

  trace_p->pop();
}

/*!
  SP computation of a scalar SuLinearForm on a == unique domain ==
  \param sulf: a single unknown linear form defined on a unique domain (assumed) and involving a spectral unknown
  \param v: reference to vector entries of type T
  \param vt: to pass as template the scalar type (real or complex)
*/
template<typename T, typename K>
void SuTermVector::computeSP(const SuLinearForm& sulf, Vector<T>& v, K& vt)
{
  if(sulf.size() == 0) return; // void linear form, nothing to compute !!!
  trace_p->push("SuTermVector::computeSP");
  //space info
  const Space* sp = sulf.space();    // space of unknown
  const Unknown* u = sulf.unknown(); // spectral unknown
  if (sp->typeOfSpace() != _spSpace) error("not_sp_space_type", sp->name());
  const SpectralBasis* basis = sp->spSpace()->spectralBasis(); //spectral basis
  number_t nb = sp->dimSpace(); //space dimension
  dimen_t nbc = u->nbOfComponents();
  dimen_t dimfun = sp->dimFun();
  if (nbc > 1)
  {
    if (dimfun==1) dimfun = nbc;//case of vector dofs (scalar shape functions will be extended to vector shape functions)
    else error("fun_bad_dim", 1, dimfun);
  }
  //spectral numbering
  std::vector<number_t>* dofNum = new std::vector<number_t>(nb);
  std::vector<number_t>::iterator itd = dofNum->begin();
  for (number_t k = 1; k <= nb; k++, itd++) *itd = k;
  //domain info
  const GeomDomain* dom = sulf.begin()->first->asIntgForm()->domain(); //common domain of integrals
  const Function* mapTo=findMap(*dom,*sp->domain());  //map function from domain to spectral basis domain
  const MeshDomain* mdom = dom->meshDomain();
  if (mdom == nullptr)  error("domain_notmesh", dom->name(), words("domain type",dom->domType()));
  //collect information from linear form to prepare computation
  bool der=false, nor=false, isoGeometric = false;
  dimen_t dimres = 0;
  for(cit_vlfp itf = sulf.begin(); itf != sulf.end(); ++itf)
    {
      const OperatorOnUnknown* op = itf->first->asIntgForm()->opu();
      if(!der && op->diffOrder()>0) der=true;
      if(!nor && op->normalRequired()) nor=true;
      dimres = op->dimsRes().first;
    }
  if(nor && mdom->isSideDomain() && !mdom->orientationComputed) mdom->setNormalOrientation();

  //retry FE information
  number_t nbelt = dom->numberOfElements();
  Vector<K> val;

  //main loop on domain elements
  //----------------------------
//  #ifdef XLIFEPP_WITH_OMP
//  #pragma omp parallel for firstprivate(phyPoint,val,itse,itsel) shared(v) schedule(dynamic)
//  #endif // XLIFEPP_WITH_OMP  //data race due to shape values computation,to be done later using the precomputeFE machinery
  for (number_t k=0; k<mdom->geomElements.size(); ++k) //main loop on elements of domain
  {
    GeomElement* gelt = mdom->geomElements[k];  //geometric element
    ShapeType sh = gelt->shapeType();          //shape type of element
    MeshElement* melt = gelt->meshElement();
    if (melt == nullptr) melt = gelt->buildSideMeshElement(); //it is a side element with no meshElement structure, build one
    GeomMapData mapdata(melt);                         //structure to store jacobian
    number_t nbt = nb * nbc;
    if (basis->funcFormType()==_analytical) basis->setGeomElementPointer(gelt);
    for(cit_vlfp itf = sulf.begin(); itf != sulf.end(); itf++)
    {
      const IntgLinearForm* ilf = itf->first->asIntgForm();   //basic linear form as single integral
      const OperatorOnUnknown* op = ilf->opu();               //operator on unknown
      K coef = complexToT<K>(itf->second);                    //coefficient of linear form
      if(ilf->intgMethod()->type()!=_quadratureIM)
        error("im_not_handled",words("integration", ilf->intgMethod()->type()));
      const QuadratureIM* qim=static_cast<const QuadratureIM*>(ilf->intgMethod());
      const Quadrature* quad = qim->getQuadrature(sh);        //quadrature rule pointer attached to integral
      number_t nbquad = quad->numberOfPoints();
      //loop on quadrature points
      for (number_t q = 0; q < nbquad; q++)
      {
        mapdata.computeJacobianMatrix(quad->point(q));          //compute jacobian at q th quadrature point
        mapdata.computeDifferentialElement();                   //compute differential element
        K alpha =  coef*mapdata.differentialElement** (quad->weight(q));
        Point xq(quad->point(q), quad->dim());
        Point x = mapdata.geomMap(xq);                          //point in physical space
        Point xm=x;
        if (mapTo!=nullptr)(*mapTo)(x,xm);
        computeSPOperator(*op, basis, xm, sp->valueType(), nb, dimfun, val); //op(phi_i)(xm) i=1,nb
        //assembly in vector entry v
        typename Vector<K>::iterator itv=val.begin();
        for(auto itdn = dofNum->begin(); itdn != dofNum->end() ; ++itdn, itv+=nbc)
          assembly(*(v.begin()+(*itdn)-1),itv,nbc,alpha);
      }
    }
  } //end of element loop
  trace_p->pop();
}

/* ================================================================================================
                      FE computation algorithm for Integral Representation
   ================================================================================================ */
/*!
  FE computation of a scalar SuLinearForm with kernels on a == unique domain ==
  \param sulf: a single unknown linear form defined on a unique domain (assumed)
  \param vt: to pass as template the scalar type (real or complex)
  \param xs: points where to evaluate IR
  \param nxs: pointer to vector of normals at points where to evaluate IR (null if not available)
  \param U: vector to apply to IR; U has always a scalar representation even if it is related to a vector unknown
  \param res: reference to vector entries of type T (result)

  examples:  v(x)=intg_gamma G(x,y)*U(y) dy   v(x)=intg_gamma n_y.grad_y(G(x,y))*U(y) dy

  U comes from a SuTermVector having same unknown as SuLinearForm !

  Algorithm:
  * various initialisation
  * pre-computation of shape values on ref element, geometric data and quadrature points on each element
  * loop on finite elements related to the integration domain
       | initialisation related to the element (geometric element, dof numbering, n_y)
       | loop on linear forms
            | if more than one lf, load some information related to linear form
            | if ny required, transmit n_y to kernel
            | loop on xi points (parallelized)
                 | compute relative distance(xi,e)/diameter(e)
                 | loop on integration methods related to the distance
                     | compute intg_E opk(xi,y)*opu(y)dy by quadrature or specific method

  Result of integral representation may be a scalar, a vector or a matrix, always stored as a vector of scalars!

  Integration methods are described by the IntegrationMethods object handled by each linear form. Such object manages
  a list of (IntegrationMethod, FunctionPart, bound) that allows to deal with complex choices of integration methods.
  For instance,
      for point x on gamma (bound=0)              : specific method for the singular part,
                                                    standard quadrature method (e.g GaussLegendreRule, 5) for the regular part
      for point x close to gamma (bound=1)        : high quadrature method (e.g GaussLegendreRule, 20) for all part of the function
      for point x not so far from gamma (bound=3) : quadrature method (e.g Gasus_Legendre, 10) for all part of the function
      for point x not far from gamma (bound=inf)  : low quadrature method (e.g Gasus_Legendre, 3) for all part of the function

  This function supports scalar and vector unknown, scalar and vector shape functions,
  and domain extension if required (non tangential operators or volumic FE with no FE trace)
*/

template<typename T, typename K>
void SuTermVector::computeIR(const SuLinearForm& sulf, Vector<T>& res, K& vt,
                             const std::vector<Point>& xs, const Vector<T>& U,
                             const std::vector<Vector<real_t> >* nxs)
{
  if (sulf.size() == 0) return; // void linear form, nothing to compute !!!
  trace_p->push("SuTermVector::computeIR");

  const Space* sp = sulf.space();                                      //space of unknown
  const Unknown* u = sulf.unknown();                                   //unknown of linear form
  const GeomDomain* dom = sulf.begin()->first->asIntgForm()->domain(); //common domain of integrals
  const MeshDomain* mdom = dom->meshDomain();
  bool useColor = mdom->useColor;

  //get extension information
  bool ext=false;
  if (mdom->isSideDomain() && sp->domain()->dim() > mdom->dim())// side domain and space supported by a domain of larger dimension
    {
      for (cit_vlfp itf = sulf.begin(); itf != sulf.end() && !ext; ++itf)
        {
          const OperatorOnUnknown* op = itf->first->asIntgForm()->opu();           //operator on unknown
          if (!ext && mdom->isSideDomain() && op->extensionRequired()) ext=true;
        }
    }

  number_t nbthread=numberOfThreads();
  theThreadData.resize(nbthread);
  string_t sext="";
  if (ext) sext="extended ";
  if (theVerboseLevel > 0) std::cout<<"computing IR "<<sext<<"term "<< sulf.asString()<<" on "<<tostring(xs.size())
                                      <<" points, using "<<nbthread<<" threads: "<<std::flush;

  //get subspace
  const Space* subsp = nullptr;
  const GeomDomain* spdom = dom;
  if (ext) spdom = &dom->extendDomain(false,*sp->domain());           //extended domain required
  subsp = sp->findSubSpace(spdom, sp);                               //find subspace (linked to domain dom) of unknown space
  if (subsp == nullptr) subsp = new Space(*spdom, const_cast<Space&>(*sp)); //subspace not constructed, construct it
  bool doflag = (subsp == sp);                                       //flag telling that there is no dof renumbering

  //get global info
  RefElement* relt = subsp->element_p(number_t(0))->refElt_p;   //first ref element
  std::set<ShapeType> shapes = dom->meshDomain()->shapeTypes;
  std::set<ShapeType>::iterator its;
  dimen_t nbc = u->nbOfComponents();                            //number of components of unknown
  dimen_t dimf  = sp->dimFun();
  if (nbc > 1) dimf=nbc;                 //case of vector dofs (scalar shape functions will be extended to vector shape functions)
  number_t ord   = sp->interpolation()->numtype;
  bool isP0      = (sp->interpolation()->numtype==0);
  number_t nb_lf = sulf.size();
  const IntgLinearForm* ilf = nullptr;
  const OperatorOnUnknown* op=nullptr;
  const OperatorOnKernel* opker=nullptr;
  const Kernel* ker=nullptr;
  const IntegrationMethod* im = nullptr;
  IntegrationMethodType imtype = _quadratureIM;
  bool firstOrderElt = dom->order() == 1;
  bool hasKer = true, hasFun = false;
  bool opisId = false;
  bool opkerisId = true;
  FEMapType mapType = relt->mapType;
  bool mapsh = false;
  bool changeSignTest = (relt->dofCompatibility == _signDofCompatibility);   //dof sign change for particular element (Nedelec, Raviart, ....)
  number_t der_ord = 0;
  K coef ;
  bool changeSign = false;
  Vector<real_t>* sign=nullptr;
  Vector<real_t>* ny = nullptr;
  bool der=false, nor=false, norx=false;
  dimen_t dimres=0;
  bool isoGeometric = false;

  //collect information from linear form to prepare computation
  std::list<std::multimap<real_t, IntgMeth> > intgMaps;               //list of IntegrationMethod indexed by distance for each linear form
  std::list<std::multimap<real_t, IntgMeth> >::iterator itiml;
  std::multimap<real_t, IntgMeth>::iterator itim, itime, itimf;
  std::set<Quadrature*> quad;   //to store single quadrature pointers
  std::vector<OperatorOnUnknown*> opregs(nb_lf,0), opsings(nb_lf,0);   //regular and singular operators for each IntglinearForm
  std::vector<OperatorOnUnknown*>::iterator itsin=opsings.begin(), itreg=opregs.begin();
  for (cit_vlfp itf = sulf.begin(); itf != sulf.end(); ++itf, ++itreg, ++itsin)   //loop on linear form
    {
      ilf = itf->first->asIntgForm();                  //basic linear form as single integral
      op = ilf->opu();                                 //operator on unknown
      if (dimres==0) dimres=op->dimsRes().first;
      else if (dimres!=op->dimsRes().first) error("free_error","in SuTermVector::computeIR, linear forms return inconsistent results");
      opker =op->opkernelp();                          //operator on kernel
      ker=opker->kernelp();                            //kernel
      if (ker==nullptr) error("null_pointer","Kernel");
      opisId = op->difOp().type()==_id;                //operator on unknown is Id: (opker)*v
      opkerisId = opker->isId();                       //operator on kernel is Id:  ker*op(v)
      coef = complexToT<K>(itf->second);               //coefficient of linear form
      if(ilf->isoGeometric) isoGeometric=true;
      //get integration methods
      intgMaps.push_back(std::multimap<real_t, IntgMeth>());
      IntegrationMethods::const_iterator itims;
      for (itims=ilf->intgMethods.begin(); itims!=ilf->intgMethods.end(); ++itims)
        {
          intgMaps.back().insert(std::pair<real_t, IntgMeth>(itims->bound,*itims)); //insert intg method in map
          if (itims->functionPart==_singularPart)
            {
              if (ker->singPart == nullptr) error("free_error","in ComputeIR, singular part of kernel "+ker->shortname+" required but not defined");
              *itsin = new OperatorOnUnknown(*op);     //copy of op
              (*itsin)->changeKernel(ker->singPart);
            }
          else if (itims->functionPart==_regularPart)
            {
              if (ker->regPart == nullptr) error("free_error","in ComputeIR, regular part of kernel "+ker->shortname+" required but not defined");
              *itreg = new OperatorOnUnknown(*op);     //copy of op
              (*itreg)->changeKernel(ker->regPart);
            }
          const IntegrationMethod* im = itims->intgMeth;
          if (im!=nullptr)
            {
              IntegrationMethodType imt=im->imType;
              if (imt ==_quadratureIM)   //single quadrature
                {
                  for (its=shapes.begin(); its!=shapes.end(); ++its)
                    quad.insert(static_cast<const QuadratureIM*>(im)->quadratures_[*its]);
                }
              else if (imt==_LenoirSalles2dIR || imt==_LenoirSalles3dIR)  nor=true;  //analytic quadrature

            }
        }
      // collect derivative and normal requirements
      der_ord = op->diffOrder();
      if (der_ord > 0) der=true;
      if (op->normalRequired()) nor=true;
      if (op->xnormalRequired()) norx=true;
      mapsh = (mapType !=_standardMap || der_ord>0);
      ker=op->kernelp();
      op->opkernelp()->noUpdatedNormal=true;    //say the normal in kernel parameter is not up to date, may be more unstable but a little faster
    }

  //define side numbers of extended domain elements (if required)
  std::vector<number_t> sides;  //empty
  std::vector<GeomElement*> sidelts;  //empty
  if (ext)
    {
      std::map<GeomElement*, number_t> eltext;
      number_t n=subsp->nbOfElements();
      for (number_t k = 0; k < subsp->nbOfElements(); k++)
        { eltext[subsp->element_p(k)->geomElt_p]=k; }
      sides.resize(n); sidelts.resize(n);
      std::vector<GeomElement*>::const_iterator ite=dom->meshDomain()->geomElements.begin();
      for (; ite!=dom->meshDomain()->geomElements.end(); ++ite)
        {
          std::vector<GeoNumPair>& parents=(*ite)->parentSides(); //parent of side element
          std::vector<GeoNumPair>::iterator itgn=parents.begin();
          for (; itgn!=parents.end(); ++itgn)
            {
              if (eltext.find(itgn->first)!=eltext.end())
                {
                  number_t k=eltext[itgn->first];
                  sides[k]=itgn->second;
                  sidelts[k]=*ite;
                }
            }
        }
    }
  // build isonodes if required
  if(isoGeometric)  dom->meshDomain()->buildIsoNodes();

  //precomputation of shapevalues, physical points
  preComputationIE(*dom, subsp, quad, nbc, false, der, nor, sides.size()>0,isoGeometric);

  //allocate res
  std::vector<number_t> dofs;
  number_t nbxs=xs.size();
  number_t nbelt=subsp->nbOfElements();
  res.resize(nbxs*dimres);
  res*=0.;

  //print status
  number_t nbeltdiv10 = nbelt/10;
  bool show_status = (theVerboseLevel > 0 && xs.size() > 100 && U.size()>100 && nbeltdiv10 > 1);

  //main loop on finite elements of fesubspace
  for (number_t k = 0; k < nbelt; k++)
    {
      const Element* elt = subsp->element_p(k);    //finite element
      GeomElement* sidelt = elt->geomElt_p;        //side element
      number_t sidenum = 0;
      if (ext) {sidelt = sidelts[k]; sidenum=sides[k];}  //side element when extension
      if (!useColor || sidelt->color == 0)  // do not take into account element with non null color if useColor flag is on
        {
          MeshElement* melt = sidelt->meshElement();   //mesh element related to sidelt
          GeomMapData* mapdata = melt->geomMapData_p;  //geometrical data
          firstOrderElt = melt->isSimplex() && (melt->order()==1) && !isoGeometric;
          real_t difelt = mapdata->differentialElement;//dif element
          if(isoGeometric)
          {
              mapdata->isoPar_p = &dom->meshDomain()->parametrization();
              mapdata->useParametrization = true;
              mapdata->useIsoNodes = true;
              setElement(sidelt);
          }
          if (nor) ny = &mapdata->normalVector;        //update y-normal if required
          ShapeType sh = sidelt->shapeType();          //shape type of side element
          RefElement* relt = elt->refElt_p;            //ref element
          if (changeSignTest) //update sign if required
            {
              sign=&elt->getDofSigns();
              changeSign = sign->size()!=0;
            }
          mapdata->sideNV();                      //compute normal to sides of element (required by some integration methods)

          const std::vector<number_t>* dofNum = &dofs;
          if (doflag) dofNum = &subsp->elementDofs(k);  //dof numbers (local numbering) of element for u
          else ranks(sp->dofid2rank(), elt->dofNumbers, dofs); //numbering related to parent, use ranks function with map dofid2rank
          number_t nbdof = dofNum->size();
          number_t nbcdof = nbdof*nbc;
          Vector<T> vu(nbcdof);            //U in local numbering, scalar representation
          std::vector<number_t>::const_iterator itdn;
          typename Vector<T>::iterator itvu=vu.begin();
          for (itdn = dofNum->begin(); itdn != dofNum->end() ; itdn++)
            for (number_t i=0; i<nbc; ++i, ++itvu) *itvu = U[(*itdn -1)*nbc+i];

          std::vector<Vector<K> > val(1);
          bool norop=nor;
          bool isSimplex = (sh==_segment || sh==_triangle || sh==_tetrahedron);
          itiml=intgMaps.begin();
          Point& center = melt->centroid;
          real_t diam = melt->size;
          itsin=opsings.begin();
          itreg=opregs.begin();

          //loop on basic linear forms
          for (cit_vlfp itf = sulf.begin(); itf != sulf.end(); ++itf, ++itreg, ++itsin, ++itiml)
            {
              if (nb_lf > 1) //update linear form parameters
                {
                  ilf  = itf->first->asIntgForm();       //basic linear form as single integral
                  op   = ilf->opu();                     //operator on unknown
                  im   = ilf->intgMethod();              //integration method
                  imtype = im->type();                   //integration method type
                  coef = complexToT<K>(itf->second);     //coefficient of linear form
                  opisId = op->difOp().type()==_id;
                  opkerisId = op->opkernelp()->isId();   //operator on kernel is Id:  ker*v
                  der_ord = op->diffOrder();
                  norop  = op->normalRequired();
                  norx   = op->xnormalRequired();
                  mapsh = (relt->mapType !=_standardMap || der_ord>0);
                  ker = op->kernelp();
                }
              if (nor) setNy(ny,0);   //transmit normal pointers ny to kernels using threadData_0

              std::multimap<real_t, IntgMeth>& intgMap=*itiml;            //integration methods indexed by bound
              number_t nbi=intgMap.size();

              #ifdef XLIFEPP_WITH_OMP
              #pragma omp parallel for firstprivate(val, itim, itime) schedule(dynamic)
              #endif // XLIFEPP_WITH_OMP
              for (number_t i=0; i<nbxs; i++) //loop on x point
                {
                  const OperatorOnUnknown* opu = op;
                  real_t dist = xs[i].distance(center)/diam; //relative distance
                  if (nbi>1)
                    {
                      itim = intgMap.lower_bound(dist);
                      itime= intgMap.upper_bound(dist);
                      if(itim==itime) itime= intgMap.upper_bound(itim->first);
                    }
                  else
                    {
                      itim = intgMap.begin();
                      itime= intgMap.end();
                    }
                  for(; itim!=itime; ++itim) // loop on integration methods, 2 if compute singular and regular part, 1 else
                    {
                      switch(itim->second.functionPart)
                        {
                        case _regularPart: opu = *itreg; break;
                        case _singularPart: opu = *itsin; break;
                        default: break;
                        }
                      const IntegrationMethod* im = itim->second.intgMeth;
                      dimen_t d,m;  // block size of val (d size of block, m number of vectors in block -> d/m size of each vector)
                      Vector<real_t>* nx = nullptr;
                      if (norx && nxs!=nullptr) nx = const_cast<Vector<real_t>*>(&((*nxs)[i]));   //update nx pointer, not transmitted to kernels (not thread safe)
                      if (im->imType==_quadratureIM)
                        {
                          Quadrature* quad = static_cast<const QuadratureIM*>(im)->quadratures_[sh];
                          std::vector<ShapeValues>* shv = &relt->qshvs_[quad]; //retry shape values at quadrature points
                          std::vector<Point>& phypts = mapdata->phyPoints[quad];//quadrature points in physical space
                          number_t nbq=quad->numberOfPoints();
                          val.resize(nbq);
                          computeOperatorByQuadrature(*elt, isP0, *opu, opisId, opkerisId, quad, nbq, quad->dim(), shv, phypts,
                                                      ord, dimf, nbc, firstOrderElt && isSimplex, der_ord, norop, hasFun, hasKer, mapsh, mapType,
                                                      changeSign, sign, difelt, mapdata, xs[i], nx, ny, val, d, m, sidenum);
                          // assembly
                          if (d==1) //scalar val
                            {
                              if (nbc==1) //scalar unknown, return scalar: res[i] += sum_q sum_j val[q][j] * vu[j]
                                {
                                  T t=T(0);
                                  for(number_t q = 0; q < nbq; q++)  t += coef * complexToT<T>(dotRC(val[q], vu));
                                  res[i]+=t;
                                }
                              else //scalar val and vector unknown, return vector: res[i+k] += sum_q sum_j val[q][j] * vu[j*nbc+k]   k=1,nbc
                                {
                                  for (number_t l = 0; l < nbc; l++)
                                    {
                                      T t=T(0);
                                      for (number_t q = 0; q < nbq; q++)
                                        for (number_t j = 0; j < nbdof; j++)
                                          t += coef * val[q][j]* vu[j*nbc+l];
                                      res[i]+=t;
                                    }
                                }
                            }
                          else  //vector or matrix val, res[i*d+l] += sum_q sum_j val[q][j*d+l] * vu[j]   l=1,d
                            {
                              for (number_t l = 0; l < d; l++)
                                {
                                  T t=T(0);
                                  for (number_t q = 0; q < nbq; q++)
                                    for (number_t j = 0; j < nbcdof; j++)
                                      t += coef * val[q][j*d+l]* vu[j];
                                  res[i*d+l]+=t;
                                }
                            }
                        }
                      else //singular computation
                        {
                          Vector<K> val(nbcdof);
                          reinterpret_cast<const SingleIM*>(im)->computeIR(elt, xs[i], *opu, ord, nx, ny, val,d);
                          if(d==1) res[i]+= coef * complexToT<T>(dotRC(val, vu));
                          else //return a vector
                            {
                              for (number_t l = 0; l < d; l++)
                                {
                                  T t=T(0);
                                  for (number_t j = 0; j < nbcdof; j++)
                                    t += coef * val[j*d+l]* vu[j];
                                  res[i*d+l]+=t;
                                }

                            }
                        }
                    }
                } // end of Point loop
            }//end of SuLinearForm loop
        } // end of color == 0
      if (show_status)   //progress status
        {
          if (k % nbeltdiv10 == 0 && k!=0) { std::cout<< k/nbeltdiv10 <<"0% "<<std::flush; }
        }
    } //end of element loop
  if (theVerboseLevel > 0) std::cout<<" done"<<eol<<std::flush;
  trace_p->pop();
}
