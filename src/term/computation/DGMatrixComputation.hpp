/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DGMatrixComputation.hpp
  \author E. Lunéville
  \since 17 jan 2022
  \date 17 jan 2022

  \brief Implementation of template DG TermMatrix computation functions

  this file is included by SuTermMatrix.hpp
  do not include it elsewhere
*/

/* ================================================================================================
                                 DG matrix computation algorithm
   ================================================================================================
   address boundary terms involving integral over a sides domain involving jump and mean values

   Let S a common side to E1 and E2 element : [opu]|S = opu1|S-opu2|S  and {opu}|S = (opu1|S+opu2|S)/2

   consider intg_S [opu]|S o {opv}|S = (intg_S opu1|S o opv1|S + intg_S opu1|S o opv2|S
                                      - intg_S opu2|S o opv1|S - intg_S opu2|S o opv2|S)/2
   Let the  basis functions related to u-space and v-space (may be the same)
       on E1 : wi1, i=1,m1   tk1, k=1,n1   opu1=sum_i ui1.wi1    v=tk1
       on E2 : wj2, j=1,m2   tl2, l=1,n2   opu2=sum_j uj1.wj2    v=tl2

   intg_S [opu]|S o {optk1}|S = 0.5 sum_i ui1 intg_S opwi1|S o optk1|S - 0.5 sum_j uj2 intg_S opwj2|S o optk1|S = 0.5(M11_ik - M12_jk)
   intg_S [opu]|S o {optl2}|S = 0.5 sum_i ui1 intg_S opwi1|S o optl2|S - 0.5 sum_j uj2 intg_S opwj2|S o optl2|S = 0.5(M21_il - M22_jl)
                                                                   u1    u2
      - for intg_S [opu]|S o {opv}|S, we get the matrix MS =  0.5| M11  -M12| v1      u1 and u2 means u-dofs on E1 and E2
                                                                 | M21  -M22| v2      v1 and v2 means v-dofs on E1 and E2
      - for intg_S [opu]|S o [opv]|S, we get the matrix MS =     | M11  -M12|
                                                                 |-M21   M22|
      - for intg_S {opu}|S o {opv}|S, we get the matrix MS = 0.25| M11   M12|
                                                                 | M11   M12|
      - for intg_S {opu}|S o [opv]|S, we get the matrix MS =  0.5| M11   M12|
                                                                 |-M21  -M22|
   When S has only one parent, say E1, only M11 is required to the exact sign!

   The normal is always the outward normal to elements and will be chosen for BOTH as n=n1=-n2
       [u.n_] will be interpreted as [u1.n-u2.n]  and [p*n] as [p1*n-p2*n]

   When u or v does not involve jump or mean, only element E1 is considered
       [opu] o opv => |M11 -M12|       {opu} o opv  => 0.5|M11 M12|
       opu o [opv] => | M11|            opu o {opv} => 0.5|M11|
                      |-M21|                              |M21|
        opu o opv =>   M11
   results should be correct if u-space or/and v-space are continuous space

   As a consequence, the computation principle is the following
    1) build for each bilinear form intg_S opu o opv, the vector (a11, a12, a21, a22)
       allowing to build the matrix [a11*M11 a12*M12 ; a21*M21 a22*M22]
    2) do a loop on element S of sides domain
         - get E1 u-element and its dof numbering
         - get E1 v-element and its dof numbering (may be the same)
         - loop on bilinear forms
            - compute M11
            - if no E2 or no jump/mean, assembly a11*M11
              else regarding bilinear form, compute M12, M21, M22 and assembly [a11*M11 a12*M12 ; a21*M21 a22*M22]

   Note : trace on S of all "volumic" dofs of E1/E2 are taken into account even if their traces on S is 0. This could be optimized in the future

   in arguments :
     subf     : a single unknown bilinear form defined on a unique sides domain (assumed)
     mat       : reference to matrix entries of type T
     vt        : to pass as template the scalar type (real or complex)
     space_u_p : largest unknown space in computation
     space_v_p : largest test space in computation
     u_p       : unknown (col)
     v_p       : test function (row)

   the subspace (sp_u=sp_v) related to bilinear form is the space of the parent domain of the sides domain

*/

namespace xlifepp
{

template <>
template <typename T, typename K>
void ComputationAlgorithm<_DGComputation>::compute(const SuBilinearForm& subf, LargeMatrix<T>& mat, K& vt,
    Space* space_u_p, Space* space_v_p, const Unknown* u_p, const TestFct* v_p)
{
  if(subf.size() == 0) return; // void bilinear form, nothing to compute !!!
  trace_p->push("SuTermMatrix::computeDG");
  if(theVerboseLevel>0) std::cout<<"computing DG term "<< subf.asString()<<", using "<<numberOfThreads()<<" threads : "<<std::flush;
  std::cout<<"\n --- Discontinuous Galerkin computation is a new XLiFE++ feature, it has been checked only for 2D scalar cases ---"<<eol;

  number_t nbthread=numberOfThreads();
  theThreadData.resize(nbthread);

  cit_vbfp itf = subf.begin();
  //const GeomDomain* dom = itf->first->asIntgForm()->domain(); //common domain of integrals
  const GeomDomain* dom = &itf->first->dom_up();    //common domain of integrals
  const MeshDomain* mdom = dom->meshDomain();

  //required extension and max derivative orders
  std::list<std::vector<complex_t> > coefs(subf.size(),std::vector<complex_t>());
  std::list<std::vector<complex_t> >::iterator itc=coefs.begin();
  number_t nbmat=0;
  dimen_t ordu = 0, ordv = 0;
  bool invertJacobian = false;
  bool nor = false;             // normal computation flag
  bool hasUF=false;             // has user bf flag
  const GeomDomain* extdom = mdom->isSidesOf_p;
  bool normalRequired=false;
  for(; itf != subf.end(); ++itf, ++itc)
  {
    LinearFormType lft=itf->first->type();
    if(lft==_intg)
    {
      const IntgBilinearForm* ibf = itf->first->asIntgForm(); //basic bilinear form as simple integral
      const OperatorOnUnknown& op_u = ibf->opu();             //operator on unknown
      const OperatorOnUnknown& op_v = ibf->opv();             //operator on test function
      ordu = std::max(ordu, dimen_t(op_u.diffOrder()));       //order of u differential involved in operators
      ordv = std::max(ordv, dimen_t(op_v.diffOrder()));       //order of v differential involved in operators
      setDomain(const_cast<GeomDomain*>(dom));                //transmit domain pointer to thread data
      if(op_u.normalRequired() || op_v.normalRequired())  normalRequired=true;
      complex_t c=itf->second;  //first coef indicates if there is a 2-contribution
      switch (op_u.jumpType())
      {
       case _jump:
       {
        switch(op_v.jumpType())
        {
          case _jump :         *itc = {c,-c,-c, c}; nbmat=std::max(nbmat,number_t(4));break;   //    [ M11 -M12;-M21  M22]
          case _mean : c*=0.5; *itc = {c,-c, c,-c}; nbmat=std::max(nbmat,number_t(4));break;   // 0.5[ M11 -M12; M21 -M22]
          default    :         *itc = {c,-c, 0, 0}; nbmat=std::max(nbmat,number_t(2));break;           //    [ M11 -M12]
        }
        break;
       }
       case _mean:
       {
        c*=0.5;
        switch(op_v.jumpType())
        {
          case _jump :         *itc = {c, c,-c,-c}; nbmat=std::max(nbmat,number_t(4)); break;   // 0.5 [ M11 M12; -M21 -M22]
          case _mean : c*=0.5; *itc = {c, c, c, c}; nbmat=std::max(nbmat,number_t(4)); break;   // 0.25[ M11 M12;  M21  M22]
          default    :         *itc = {c, c, 0, 0}; nbmat=std::max(nbmat,number_t(2)); break;   // 0.5 [ M11 M12]
        }
        break;
       }
       default :
       {
        switch(op_v.jumpType())
        {
          case _jump :         *itc = {c, 0,-c, 0}; nbmat=std::max(nbmat,number_t(2)); break;   //    [ M11 ;-M21 ]
          case _mean : c*=0.5; *itc = {c, 0, c, 0}; nbmat=std::max(nbmat,number_t(2)); break;   // 0.5[ M11 ; M21 ]
          default    :         *itc = {c, 0, 0, 0}; nbmat=std::max(nbmat,number_t(1)); break;   //    [ M11 ]
        }
        break;
       }
      }
    }
    else if (lft==_userLf)
    {
      if(!nor) nor = itf->first->asUserForm()->requireNormal_;
      if(!invertJacobian) invertJacobian = itf->first->asUserForm()->requireInvJacobian_;
      hasUF=true;
    }
  }

  //retry boundary spaces
  const Space* sp_u = subf.up()->space();  //space of unknown
  const Space* sp_v = subf.vp()->space();  //space of test function
  if(sp_u->typeOfSpace() != _feSpace) error("not_fe_space_type", sp_u->name(), sp_u->typeOfSpace());
  if(sp_v->typeOfSpace() != _feSpace) error("not_fe_space_type", sp_v->name(), sp_v->typeOfSpace());
  const Space* subsp_u=nullptr;
  subsp_u = Space::findSubSpace(extdom, space_u_p); //find subspace of space_u_p
  if(subsp_u == nullptr) subsp_u = space_u_p;             //is a FESpace, not a FESubspace
  const Space* subsp_v=nullptr;
  subsp_v = Space::findSubSpace(extdom, space_v_p); //find subspace of space_v_p
  if(subsp_v == nullptr) subsp_v = space_v_p;             //is a FESpace, not a FESubspace
  bool same_interpolation = (subsp_u == subsp_v);

  //retry dof information
  bool doflag_u = (subsp_u == space_u_p), doflag_v = (subsp_v == space_v_p);
  if(!doflag_u) space_u_p->builddofid2rank();              //build map dofid_u->rank
  if(!doflag_v) space_v_p->builddofid2rank();              //build map dofid_v->rank
  dimen_t dimfun_u = sp_u->dimFun(), dimfun_v = sp_v->dimFun();
  dimen_t nbc_u = u_p->nbOfComponents(), nbc_v = v_p->nbOfComponents(); //number of components of unknown
  if(nbc_u > 1) dimfun_u = nbc_u;                                       //case of vector dofs (scalar shape functions will be extended to vector shape functions)
  if(nbc_v > 1) dimfun_v = nbc_v;                                       //case of vector dofs (scalar shape functions will be extended to vector shape functions)

  //retry FE information
  FEMapType femt_u=subsp_u->element_p(number_t(0))->refElt_p->mapType,
            femt_v=subsp_v->element_p(number_t(0))->refElt_p->mapType;
  if(!invertJacobian) invertJacobian =(femt_u==_covariantPiolaMap || femt_v==_covariantPiolaMap);
  bool same_shv = (same_interpolation && nbc_v==nbc_u);
  bool sym = same_shv && !(mat.sym == _noSymmetry);
  number_t nbelt = dom->numberOfElements();

  //print status
  number_t nbeltdiv10 = nbelt/10;
  bool show_status = (theVerboseLevel > 0 && mat.nbRows > 1000 &&  nbeltdiv10 > 1);

  //temporary vector
  std::vector<number_t> dofNum_u1, dofNum_v1, dofNum_u2, dofNum_v2, perm;
  Vector<real_t> *sign_u1=nullptr, *sign_v1=nullptr, *sign_u2=nullptr, *sign_v2=nullptr;;
  std::vector<ShapeValues> shv_us, shv_vs, shv_us2,shv_vs2;
  Vector<real_t> nv;
  BFComputationData bfData;

//  #ifdef XLIFEPP_WITH_OMP
//  #pragma omp parallel for firstprivate(shv_us, shv_vs, sign_u, sign_v, dofNum_u, dofNum_v, perm, bfdata)
//  #endif // XLIFEPP_WITH_OMP   NOT THREAD SAFE

  //main loop on elements of sides domain
  //=====================================
  for(number_t k = 0; k < nbelt; k++)
  {
    GeomElement* gelt = mdom->geomElements[k];            //geometric side element
    //thePrintStream<<eol<<"=== compute on side element : "<<*gelt<<eol;
    ShapeType sh = gelt->shapeType();                     //shape type of side element
    MeshElement* melt = gelt->meshElement();
    if(melt == nullptr) melt = gelt->buildSideMeshElement();    //it is a side element with no meshElement structure, build one
    GeomMapData mapdata(melt);                            //structure to store jacobian of side element
    std::vector<GeoNumPair>& parelts=gelt->parentSides(); // parent elements of side element

    // data related to first parent element E1
    GeomElement* gelt1 = parelts[0].first;
    number_t side1 =  parelts[0].second;
    const Element* elt_u1 = subsp_u->element_p(gelt1);
    RefElement* relt_u1 = elt_u1->refElt_p;
    if(doflag_u) dofNum_u1 = subsp_u->elementDofs(subsp_u->numElement(gelt1)); //dof numbers (local numbering related to subsp_u=space_u_p)
    else ranks(space_u_p->dofid2rank(), elt_u1->dofNumbers, dofNum_u1);          //use ranks function with map dofid2rank (faster)
    number_t nb_u1 = dofNum_u1.size();
    const Element* elt_v1 = subsp_v->element_p(gelt1);
    RefElement* relt_v1 = elt_v1->refElt_p;
    if(doflag_v) dofNum_v1 = subsp_v->elementDofs(subsp_v->numElement(gelt1)); //dof numbers (local numbering related to subsp_u=space_u_p)
    else ranks(space_v_p->dofid2rank(), elt_v1->dofNumbers, dofNum_v1);           //use ranks function with map dofid2rank (faster)
    number_t nb_v1 = dofNum_v1.size();

    // local to global numbering matrix
    std::vector<number_t> adrs11(nb_v1 * nb_u1, 0);
    mat.positions(dofNum_v1, dofNum_u1, adrs11, true); //adresses in mat of elementary matrix stored as a row vector
    number_t nb_ut1 = nb_u1 * nbc_u, nb_vt1 = nb_v1* nbc_v;

    if(hasUF)
    {
        bfData.sidelt=gelt;
        bfData.elt_u=elt_u1; bfData.elt_v=elt_v1;
        bfData.elt_u2=nullptr; bfData.elt_v2=nullptr;
    }

    //sign correction for some element (div or rot element)
    bool changeSign_u1=false, changeSign_v1=false;
    if(relt_u1->dofCompatibility == _signDofCompatibility)
    {sign_u1=&elt_u1->getDofSigns(); changeSign_u1 = sign_u1->size()!=0;}
    if(same_shv){changeSign_v1=changeSign_u1; sign_v1=sign_u1;}
    else if(relt_v1->dofCompatibility == _signDofCompatibility)
    {sign_v1=&elt_v1->getDofSigns();changeSign_v1 = sign_v1->size()!=0; }
    // dof rotation flag
    bool rotsh_u1=elt_u1->refElt_p->rotateDof;
    bool rotsh_v1=elt_v1->refElt_p->rotateDof;

    GeomElement* gelt2;
    const Element* elt_u2, *elt_v2;
    RefElement* relt_u2, *relt_v2;
    number_t side2, nb_u2, nb_v2, nb_ut2, nb_vt2;
    std::vector<number_t> adrs12, adrs21, adrs22;
    bool changeSign_u2=false, changeSign_v2=false, rotsh_u2, rotsh_v2;
    bool compute2 = parelts.size()>1 && nbmat>1;

    if(parelts.size()>1) // data related to second parent element E2
    {
      gelt2 = parelts[1].first;
      side2 =  parelts[1].second;
      elt_u2 = subsp_u->element_p(gelt2); relt_u2 = elt_u2->refElt_p;
      if(doflag_u) dofNum_u2 = subsp_u->elementDofs(subsp_u->numElement(gelt2)); //dof numbers (local numbering related to subsp_u=space_u_p)
      else ranks(space_u_p->dofid2rank(), elt_u2->dofNumbers, dofNum_u2);          //use ranks function with map dofid2rank (faster)
      nb_u2 = dofNum_u2.size();
      elt_v2 = subsp_v->element_p(gelt2); relt_v2 = elt_v2->refElt_p;
      if(doflag_v) dofNum_v2 = subsp_v->elementDofs(subsp_v->numElement(gelt2)); //dof numbers (local numbering related to subsp_u=space_u_p)
      else ranks(space_v_p->dofid2rank(), elt_v2->dofNumbers, dofNum_v2);           //use ranks function with map dofid2rank (faster)
      nb_v2 = dofNum_v2.size();

      if(hasUF)  // specific to user bf
      {
        bfData.elt_u2=elt_u2; bfData.elt_v2=elt_v2;
      }

      // local to global numbering matrices
      adrs12.resize(nb_v1 * nb_u2, 0);
      mat.positions(dofNum_v1, dofNum_u2, adrs12, true); //adresses in mat of elementary matrix stored as a row vector
      adrs21.resize(nb_v2 * nb_u1, 0);
      mat.positions(dofNum_v2, dofNum_u1, adrs21, true); //adresses in mat of elementary matrix stored as a row vector
      adrs22.resize(nb_v2 * nb_u2, 0);
      mat.positions(dofNum_v2, dofNum_u2, adrs22, true); //adresses in mat of elementary matrix stored as a row vector
      nb_ut2 = nb_u2 * nbc_u; nb_vt2 = nb_v2* nbc_v;

      //sign correction for some element (div or rot element)
      if(relt_u2->dofCompatibility == _signDofCompatibility)
      {sign_u2=&elt_u2->getDofSigns(); changeSign_u2 = sign_u2->size()!=0;}
      if(same_shv) {changeSign_v2=changeSign_u2; sign_v2=sign_u2;}
      else if(relt_v2->dofCompatibility == _signDofCompatibility)
      {sign_v2=&elt_v2->getDofSigns(); changeSign_v2 = sign_v2->size()!=0; }
      // dof rotation flag
      rotsh_u2=elt_u2->refElt_p->rotateDof;
      rotsh_v2=elt_v2->refElt_p->rotateDof;
      // side dof permutation, to match images of side quadrature point on elements
      switch(gelt->shapeType())
      {
         case _segment :
         { perm.resize(2); perm[0]=0; perm[1]=1;
           if(gelt2->vertex(1,side2)!=gelt1->vertex(1,side1)) {perm[0]=1;perm[1]=0;}
           break;
         }
         case _triangle :
         { perm.resize(3); perm[0]=0; perm[1]=1; perm[2]=2;
           if(gelt2->vertex(1,side2)==gelt1->vertex(1,side1))
           {
              if(gelt2->vertex(2,side2)!=gelt1->vertex(2,side1)) {perm[1]=2;perm[2]=1;}
           }
           else if(gelt2->vertex(1,side2)==gelt1->vertex(2,side1))
           {
              perm[0]=1;
              if(gelt2->vertex(2,side2)==gelt1->vertex(1,side1)) perm[1]=0;
              else {perm[1]=2;perm[2]=0;}
           }
           else
           {
              perm[0]=2;
              if(gelt2->vertex(2,side2)==gelt1->vertex(1,side1)) {perm[1]=0;perm[2]=1;}
              else {perm[1]=1;perm[2]=0;}
           }
           break;
         }
         default: error("not_yet_handled"," in DG computation, shapetype "+words("shape",gelt->shapeType()));
      }
    }
    //thePrintStream<<"  nbmat="<<nbmat<<" nbpar="<<parelts.size()<<" dofNum_u1="<<dofNum_u1<<" dofNum_v1="<<dofNum_v1<<" dofNum_u2="<<dofNum_u2<<" dofNum_v2="<<dofNum_v2<<" perm="<<perm<<eol;

    //loop on basic bilinear forms
    itc=coefs.begin();
    for(cit_vbfp itf = subf.begin(); itf != subf.end(); itf++, ++itc)
    {
      if(itf->first->type()!=_userLf)  // not a user bf
      {
        const IntgBilinearForm* ibf = itf->first->asIntgForm(); //basic bilinear form as simple integral
        const OperatorOnUnknown& op_u = ibf->opu();             //operator on unknown
        const OperatorOnUnknown& op_v = ibf->opv();             //operator on test function
        if(op_u.elementRequired() || op_v.elementRequired()) setElement(gelt);  // transmit gelt to thread data
        number_t ord_opu = op_u.diffOrder();                    //derivative order of op_u
        number_t ord_opv = op_v.diffOrder();                    //derivative order of op_v
        bool mapsh_u = (femt_u!=_standardMap || ord_opu>0);
        bool mapsh_v = (femt_v!=_standardMap || ord_opv>0);
        AlgebraicOperator aop = ibf->algop();                   //algebraic operator between differential operator
        K coef = complexToT<K>(itc->at(0));                     //coefficient of linear form
        if(parelts.size()==1)                                   //when not shared, cancel the half in mean operators
        {
          if(op_u.jumpType()==_mean) coef*=2;
          if(op_v.jumpType()==_mean) coef*=2;
        }
        const QuadratureIM* qim = dynamic_cast<const QuadratureIM*>(ibf->intgMethod());
        const Quadrature* quad = qim->getQuadrature(sh);        //quadrature rule pointer attached to integral
        Vector<real_t>* np=nullptr;                                   //pointer to normal
        //geometric stuff
        bool invJacobian = invertJacobian || op_u.diffOrder() > 0  || op_v.diffOrder() > 0;
        bool upmapdata = true;
        if(melt->linearMap) //compute geometric data only once if geom elt map is linear
        {
          mapdata.computeJacobianMatrix(quad->point(0));       //compute jacobian
          mapdata.computeJacobianDeterminant();                //compute differential element and normal if element of dim = spacedim-1
          if(invJacobian) mapdata.invertJacobianMatrix();      //compute inverse of jacobian
          upmapdata = false;
        }
        //prepare quadrature
        number_t nbquad = quad->numberOfPoints();
        shv_us.assign(nbquad,ShapeValues(*relt_u1,ord_opu>0,ord_opu>1));
        shv_vs.assign(nbquad,ShapeValues(*relt_v1,ord_opv>0,ord_opv>1));
        bool upmapdata1 = true;
        MeshElement* melt1 = gelt1->meshElement();
        if(melt1 == nullptr) melt1 = gelt1->buildSideMeshElement(); //no meshElement structure, build one
        GeomMapData* mapdata1=new GeomMapData(melt1);         //structure to store jacobian related to E1
        if(melt1->linearMap)   //compute geometric data only once if geom elt map is linear
        {
          mapdata1->computeJacobianMatrix(gelt1->geomRefElement()->centroid());  //compute jacobian
          mapdata1->computeJacobianDeterminant();          //compute differential element and normal if element of dim = spacedim-1
          mapdata1->invertJacobianMatrix();                //compute inverse of jacobian
          if(normalRequired)
          {
            nv=gelt1->normalVector(side1);    //compute outward  normal to gelt1 on side1
            nv/=norm(nv);
          }
          upmapdata1 = false;
        }
        //compute matrix M11 and add to mat
        //thePrintStream<<"  ------- compute M11 --------"<<eol<<std::flush;
        computeMatel(mat, adrs11, sym, quad, upmapdata, &mapdata, nv, coef, invJacobian, normalRequired, aop, true, true,
                     relt_u1, gelt1, side1, melt1, upmapdata1, mapdata1, nbc_u, nb_ut1, op_u, ord_opu, mapsh_u, femt_u, rotsh_u1, changeSign_u1, sign_u1, dimfun_u, shv_us, dofNum_u1,
                     relt_v1, gelt1, side1, melt1, upmapdata1, mapdata1, nbc_v, nb_vt1, op_v, ord_opv, mapsh_v, femt_v, rotsh_v1, changeSign_v1, sign_v1, dimfun_v, shv_vs, dofNum_v1);
        if(compute2) //other matrices
        {
          shv_us2.assign(nbquad,ShapeValues(*relt_u2,ord_opu>0,ord_opu>1));
          shv_vs2.assign(nbquad,ShapeValues(*relt_v2,ord_opv>0,ord_opv>1));
          bool upmapdata2 = true;
          MeshElement* melt2 = gelt2->meshElement();
          if(melt2 == nullptr) melt2 = gelt2->buildSideMeshElement(); //no meshElement structure, build one
          GeomMapData* mapdata2=new GeomMapData(melt2);         //structure to store jacobian related to E1
          if(melt2->linearMap)   //compute geometric data only once if geom elt map is linear
          {
            mapdata2->computeJacobianMatrix(gelt2->geomRefElement()->centroid());  //compute jacobian
            mapdata2->computeJacobianDeterminant();          //compute differential element and normal if element of dim = spacedim-1
            mapdata2->invertJacobianMatrix();                //compute inverse of jacobian
            upmapdata2 = false;
          }
          if (itc->at(1) != 0.) //compute matrix M12 and add to mat
          {//thePrintStream<<"  ------- compute M12 --------"<<eol<<std::flush;
            computeMatel(mat, adrs12, sym, quad, upmapdata, &mapdata, nv, complexToT<K>(itc->at(1)), invJacobian, normalRequired, aop, false, true,
                         relt_u2, gelt2, side2, melt2, upmapdata2, mapdata2, nbc_u, nb_ut2, op_u, ord_opu, mapsh_u, femt_u, rotsh_u2, changeSign_u2, sign_u2, dimfun_u, shv_us2, dofNum_u2,
                         relt_v1, gelt1, side1, melt1, upmapdata1, mapdata1, nbc_v, nb_vt1, op_v, ord_opv, mapsh_v, femt_v, rotsh_v1, changeSign_v1, sign_v1, dimfun_v, shv_vs, dofNum_v1, perm);}
          if (itc->at(2) != 0.) //compute matrix M21 and add to mat
          {//thePrintStream<<"  ------- compute M21 --------"<<eol<<std::flush;
            computeMatel(mat, adrs21, sym, quad, upmapdata, &mapdata, nv, complexToT<K>(itc->at(2)), invJacobian, normalRequired, aop, true, false,
                         relt_u1, gelt1, side1, melt1, upmapdata1, mapdata1, nbc_u, nb_ut1, op_u, ord_opu, mapsh_u, femt_u, rotsh_u1, changeSign_u1, sign_u1, dimfun_u, shv_us, dofNum_u1,
                         relt_v2, gelt2, side2, melt2, upmapdata2, mapdata2, nbc_v, nb_vt2, op_v, ord_opv, mapsh_v, femt_v, rotsh_v2, changeSign_v2, sign_v2, dimfun_v, shv_vs2, dofNum_v2, perm);}
          if (itc->at(3) != 0.) //compute matrix M22 and add to mat
          {//thePrintStream<<"  ------- compute M22 --------"<<eol<<std::flush;
            computeMatel(mat, adrs22, sym, quad, upmapdata, &mapdata, nv, complexToT<K>(itc->at(3)), invJacobian, normalRequired, aop, false, false,
                         relt_u2, gelt2, side2, melt2, upmapdata2, mapdata2, nbc_u, nb_ut2, op_u, ord_opu, mapsh_u, femt_u, rotsh_u2, changeSign_u2, sign_u2, dimfun_u, shv_us2, dofNum_u2,
                         relt_v2, gelt2, side2, melt2, upmapdata2, mapdata2, nbc_v, nb_vt2, op_v, ord_opv, mapsh_v, femt_v, rotsh_v2, changeSign_v2, sign_v2, dimfun_v, shv_vs2, dofNum_v2);}
        }
      }
      else // is a user bf, call user function
      {
        itf->first->asUserForm()->bffun_(bfData);
        //assembly to mat, matel is a scalar matrix stored as [Mv1u1 Mv1u2; Mv2u2 Mv2u2]
        if(bfData.valueType()==_real)
            assemblyDG(mat,bfData.matels, complexToT<real_t>(itf->second), bfData.elt_u2==nullptr, adrs11, adrs12, adrs21,adrs22, sym,
                       dofNum_u1,dofNum_v1, nbc_u, nbc_v, dofNum_u2, dofNum_v2, nbc_u, nbc_v);
        else
            assemblyDG(mat,bfData.cmatels, complexToT<complex_t>(itf->second), bfData.elt_u2==nullptr, adrs11, adrs12, adrs21,adrs22, sym,
                       dofNum_u1,dofNum_v1, nbc_u, nbc_v, dofNum_u2, dofNum_v2, nbc_u, nbc_v);
      }
    } //end of bilinear forms loop

    if(show_status && currentThread()==0&& k!=0 && k % nbeltdiv10 == 0)  //progress status
    { std::cout<< k/nbeltdiv10 <<"0% "<<std::flush; }
  } //end element loop

  if(theVerboseLevel > 0) std::cout<<" done"<<eol<<std::flush;
  trace_p->pop();
}

template <typename T, typename K>
void computeMatel(LargeMatrix<T>& mat, std::vector<number_t>& adrs, bool sym, const Quadrature* quad, bool upmapdata, GeomMapData* mapdata,
                  Vector<real_t>& nv, K coef, bool invJacobian, bool normalRequired, AlgebraicOperator aop, bool uonE1, bool vonE1,
                  RefElement* relt_u, GeomElement* gelt_u, number_t side_u, MeshElement* melt_u, bool upmapdata_u, GeomMapData* mapdata_u,
                  number_t nbc_u, number_t nbt_u, const OperatorOnUnknown& op_u, number_t ord_opu, bool mapsh_u, FEMapType femt_u, bool rotsh_u,
                  bool changeSign_u, Vector<real_t>* sign_u, dimen_t dimfun_u, std::vector<ShapeValues>& shv_us, std::vector<number_t>& dofNum_u,
                  RefElement* relt_v, GeomElement* gelt_v, number_t side_v, MeshElement* melt_v, bool upmapdata_v, GeomMapData* mapdata_v,
                  number_t nbc_v, number_t nbt_v, const OperatorOnUnknown& op_v, number_t ord_opv, bool mapsh_v, FEMapType femt_v, bool rotsh_v,
                  bool changeSign_v, Vector<real_t>* sign_v, dimen_t dimfun_v, std::vector<ShapeValues>& shv_vs, std::vector<number_t>& dofNum_v,
                  const std::vector<number_t>& perm = std::vector<number_t>())
{
  dimen_t dimfunp=0;
  number_t nb_u=dofNum_u.size(), nb_v = dofNum_v.size();
  Matrix<K> matel(nb_v, nb_u, K());
  std::vector<number_t>::iterator itadrs = adrs.begin(), itk;
  std::vector<real_t> yq_u , yq_v;
  for(number_t q = 0; q < quad->numberOfPoints(); q++)   //loop on quadrature points on side
  {
    if(upmapdata)  //update side mapdata
    {
       mapdata->computeJacobianMatrix(quad->point(q));        //compute jacobian at q th quadrature point
       mapdata->computeJacobianDeterminant();                 //compute differential element and normal if element of dim = spacedim-1
       if(invJacobian) mapdata->invertJacobianMatrix();       //compute inverse of jacobian
    }
    Point xq(quad->point(q), quad->dim()), x = mapdata->geomMap(xq);
    if(uonE1) yq_u = gelt_u->geomRefElement()->sideToElt(side_u, xq.begin());
    else      yq_u = gelt_u->geomRefElement()->sideToElt(side_u, xq.begin(),perm);  // u on E2
    //yq_u=mapdata_u->geomMapInverse(x);
    if(gelt_u==gelt_v) yq_v = yq_u;
    else
    {
      if(vonE1) yq_v = gelt_v->geomRefElement()->sideToElt(side_v, xq.begin());
      else yq_v = gelt_v->geomRefElement()->sideToElt(side_v, xq.begin(),perm);
//      yq_v=mapdata_v->geomMapInverse(x);
    }
    //thePrintStream<<"q="<<q<<" gelt_u="<<gelt_u<<" gelt_v="<<gelt_v<<" xq="<<xq<<" x="<<x<<" yq_u="<<yq_u<<" yq_v="<<yq_v<<eol<<std::flush;
    if(upmapdata_u) //update E_u mapdata
    {
       mapdata_u->computeJacobianMatrix(yq_u.begin());              //compute jacobian at q th quadrature point
       mapdata_u->computeJacobianDeterminant();                     //compute differential element
       if(invJacobian) mapdata_u->invertJacobianMatrix();           //compute inverse of jacobian
       if(normalRequired)
       {
         nv=gelt_u->normalVector(side_u); //update outward  normal to gelt_u on side_u
         nv/=norm(nv);
         if(!uonE1) nv*=-1;   //reverse normal sign because on E2
       }
    }
    if(upmapdata_v && mapdata_v!=mapdata_u) //update E_v mapdata
    {
       mapdata_v->computeJacobianMatrix(yq_v.begin());               //compute jacobian at q th quadrature point
       mapdata_v->computeJacobianDeterminant();                      //compute differential element
       if(invJacobian) mapdata_v->invertJacobianMatrix();            //compute inverse of jacobian
    }

    //thePrintStream<<"mapdata->differentialElement="<<mapdata->differentialElement<<" nv="<<nv<<eol<<std::flush;
    relt_u->computeShapeValues(yq_u.begin(), shv_us[q], ord_opu > 0);
    if(nbc_u > 1) shv_us[q].extendToVector(nbc_u);  //extend scalar shape functions to nbc_u vector shape functions
    relt_v->computeShapeValues(yq_v.begin(), shv_vs[q], ord_opv > 0);
    if(nbc_v > 1) shv_vs[q].extendToVector(nbc_v);  //extend scalar shape functions to nbc_v vector shape functions
    //map shapevalues
    ShapeValues* sv_u = &shv_us[q];           //original u shape values
    ShapeValues* sv_v = &shv_vs[q];           //original v shape values
    ShapeValues svt_u, svt_v;
    if(mapsh_u || changeSign_u || rotsh_u)    //map u shape values
    {
       mapShapeValues(*relt_u, *melt_u, *mapdata_u, mapsh_u, femt_u, rotsh_u, ord_opu, changeSign_u, sign_u, dimfun_u, dimfunp, shv_us[q], svt_u);
       sv_u = &svt_u;
    }
    if(mapsh_v || changeSign_v || rotsh_v)    //map v shape values
    {
       mapShapeValues(*relt_v, *melt_v, *mapdata_v, mapsh_v, femt_v, rotsh_v, ord_opv, changeSign_v, sign_v, dimfun_v, dimfunp, shv_vs[q], svt_v);
       sv_v = &svt_v;
    }
    K alpha = coef * mapdata->differentialElement** (quad->weight(q));
    //thePrintStream<<"map shv_u:"<<*sv_u<<eol<<"map shv_v:"<<*sv_v<<eol<<std::flush;
    //compute integrand (OperatorOnUnknown)
    dimen_t du,mu,dv,mv; //blocks size
    Vector<K> val_u, val_v;
    if(op_u.hasFunction()) op_u.eval(x, sv_u->w, sv_u->dw, sv_u->d2w, dimfun_u, val_u, du, mu, &nv); //evaluate differential operator with function
    else                   op_u.eval(sv_u->w, sv_u->dw, sv_u->d2w, dimfun_u, val_u, du, mu, &nv);    //evaluate differential operator
    if(op_v.hasFunction()) op_v.eval(x, sv_v->w, sv_v->dw, sv_v->d2w, dimfun_v, val_v, dv, mv, &nv); //evaluate differential operator with function
    else                   op_v.eval(sv_v->w, sv_v->dw, sv_v->d2w, dimfun_v, val_v, dv, mv, &nv);    //evaluate differential operator
    tensorOpAdd(aop, val_v, nbt_v, val_u, nbt_u, matel, alpha);                              //elementary matrix, note the transposition of u and v
//    thePrintStream<<"   xq="<<xq<<" x="<<x<<" alpha="<<alpha<<eol<<
//                    " sv_u="<<(*sv_u)<<" sv_v="<<(*sv_v)<<"val_u="<<val_u<<eol<<"  val_v="<<val_v<<eol;
  }

  //assembling matel in global matrix, matel is never a block matrix as mat is a block matrix in case of vector unknowns
  std::vector<number_t>::iterator itd_u, itd_v, it_ue;   //dof numbering iterators
  typename Matrix<K>::iterator itm = matel.begin(), itm_u;
  number_t incr = nbc_u * nbc_v * nb_u; //block row increment in matel
  number_t i = 0;
  for(itd_v = dofNum_v.begin(); itd_v != dofNum_v.end() ; itd_v++, itm += incr, i++) //loop on (vector) dofs in u and v
  {
     itm_u = itm;
     itk = itadrs + i * nb_u;
     for(itd_u = dofNum_u.begin(); itd_u != dofNum_u.end() ; itd_u++, itm_u += nbc_u, itk++)
     {
        if(!sym || *itd_v >= *itd_u)
        {
           assemblyMat(mat(*itk), itm_u, nbt_u);
        }
     }
   } //end of dofNum loops
}

/* assembly scalar matrix matels m =[m11 m12; m21 m22] of size ntvi x ntuj to mat M, mij may be a block matrix with block of size ncvi x ncuj
   with nc.. : number of components, nd.. : number of dofs (vector) and ntui = ndui x  ncui and ntvj = ndvj x  ncvj

           ntu1       ntv1
      |-------------------------|
      | xx  xx  xx | xx  xx  xx |                         u1          u2
      | xx  xx  xx | xx  xx  xx |                  |------------------------|
      |            |            |                  |                        |
      | xx  xx  xx | xx  xx  xx |                  |                        |
 ntv1 | xx  xx  xx | xx  xx  xx |                  |     xxx         xxx    |
      |            |            |               v1 |     xxx         xxx    |
      | xx  xx  xx | xx  xx  xx |                  |     xxx         xxx    |
      | xx  xx  xx | xx  xx  xx |                  |                        |
 m :  |-------------------------|  ====>    M :    |                        |
      | xx  xx  xx | xx  xx  xx |                  |                        |
      | xx  xx  xx | xx  xx  xx |                  |     xxx         xxx    |
      |            |            |               v2 |     xxx         xxx    |
      | xx  xx  xx | xx  xx  xx |                  |     xxx         xxx    |
      | xx  xx  xx | xx  xx  xx |                  |                    |   |
      |            |            |                  |                        |
      | xx  xx  xx | xx  xx  xx |                  |------------------------|
      | xx  xx  xx | xx  xx  xx |                    x : matrix ncvi x ncuj
      |-------------------------|
            x : scalar

      mat        : global matrix
      matels     : vector of scalar elementary matrices matel_ij
      adrssij    : adress in global mat of matelij
      sym        : true if symmetric assembly (no upper part)
      nc..       : number of components
      dofnum..   : dof numberings
*/
template <typename T, typename K>
void assemblyDG(LargeMatrix<T>& mat, std::vector<Matrix<K>>& matels, const K& coef, bool onlyM11, const std::vector<number_t>& adrs11, const std::vector<number_t>& adrs12,
                const std::vector<number_t>& adrs21, const std::vector<number_t>& adrs22, bool sym,
                const std::vector<number_t>& dofNum_u1, const std::vector<number_t>& dofNum_v1, number_t ncu1, number_t ncv1,
                const std::vector<number_t>& dofNum_u2, const std::vector<number_t>& dofNum_v2, number_t ncu2, number_t ncv2)
{
  std::vector<number_t>::const_iterator itd_u, itd_v;   //dof numbering iterators
  const std::vector<number_t> *dofNum_u, *dofNum_v;
  number_t nbc_u, nbc_v, nb_u, nbt_u;
  typename Matrix<K>::iterator itm, itm_u;
  std::vector<number_t>::const_iterator itadrs, itk;
  number_t incr;   //block row increment in matel
  number_t rmax=2, cmax=2;
  if (onlyM11) {rmax=1; cmax=1;}

  for(number_t r=0; r<rmax; r++)   // row block r
  {
    if(r==0) {dofNum_v = &dofNum_v1; nbc_v = ncv1;}
    else     {dofNum_v = &dofNum_v2; nbc_v = ncv2;}
    for(number_t c=0; c<cmax; c++) // col block r, deal with block vr, uc
    {

     if(!sym || r>=c)  //if symmetric assembly do not assembly upper block matel_21
     {
      if(c==0)
      {
        dofNum_u= &dofNum_u1;
        nbc_u = ncu1;
        if(r==0) itadrs=adrs11.begin(); else itadrs=adrs21.begin();
      }
      else
      {
        dofNum_u= &dofNum_u2;
        nbc_u = ncu2;
        if(r==0) itadrs=adrs12.begin(); else itadrs=adrs22.begin();
      }
      //thePrintStream<<"  matel("<<r<<","<<c<<") = "<<eol<<matels[2*r+c]<<eol;
      itm = matels[2*r+c].begin();
      nb_u = dofNum_u->size();
      nbt_u= nb_u*nbc_u;
      incr = nbt_u * nbc_v;
      number_t i = 0;
      for(itd_v = dofNum_v->begin(); itd_v != dofNum_v->end() ; itd_v++, itm += incr, i++) //loop on (vector) dofs in u and v
      {
        itm_u = itm;
        itk = itadrs + i * nb_u;
        for(itd_u = dofNum_u->begin(); itd_u != dofNum_u->end() ; itd_u++, itm_u += nbc_u, itk++)
        {
          if(!sym || *itd_v >= *itd_u) //if symmetric assembly do not assembly upper block (m_ij) j>i
          {
          assemblyMat(mat(*itk), itm_u, nbt_u);
          }
        }
      }//end of dofNum loops
    }
   }
  }
}

} // end of namespace xlifepp
