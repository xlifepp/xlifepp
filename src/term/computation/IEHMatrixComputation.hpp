/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file IEHMatrixComputation.hpp
  \author E. Lunéville
  \since 01 jul 2016
  \date 01 jul 2016

  \brief Implementation of template IE HMatrix computation functions,
         compute by different methods (tensor quadrature, Sauter-schwab formulae, Lenoir-Salles methods)

         intg_domx intg_domy op(u)(y) aop op(k)(x,y) aop op(v)(x)
    or
         intg_domx intg_domy op(v)(x) aop op(k)(x,y) aop op(u)(y)

  where u denotes the unknown and v the test function

  this file is included by SuTermMatrix.hpp
  do not include it elsewhere
*/

using std::abs;

/* ================================================================================================
                                      IE partial computation algorithm
   ================================================================================================ */

namespace xlifepp
{

/*! partial IE computation of a scalar SuBilinearForm on a list of elements
   subf: a single unknown bilinear form defined on a unique domains pair (assumed)
   mat: matrix to build, with size rowDofs.size x colDofs.size
   rowDofs, colDofs: vector of row/col dofs involved
   rowElts, colElts: vector of row/col elements involved
   vt: to pass as template the scalar type (real or complex)
   ieparams: some precomputed IE information
   intgMaps: list of integration methods to be used (built outside)
   kopregs , kopsings: list of regular and singular kernels when required

   NOTE: precomputation of normals, quadrature points, shape values, ... has to be done before
*/
template <typename T, typename K>
void computePartialIE(const SuBilinearForm& subf, LargeMatrix<T>& mat, K& vt,
                      const std::vector<number_t>& rowDofs, const std::vector<number_t>& colDofs,
                      const std::vector<Element*>& rowElts, const std::vector<Element*>& colElts,
                      IEcomputationParameters& ieparams, const std::list<std::multimap<real_t, IntgMeth> >& intgMaps,
                      const std::vector<KernelOperatorOnUnknowns*>& kopregs, const std::vector<KernelOperatorOnUnknowns*>& kopsings,
                      const std::map<Element*,GeoNumPair>& sidelts_u, const std::map<Element*,GeoNumPair>& sidelts_v,
                      bool noUpdatedNormal, bool same_interpolation, bool sym)
{
  if(subf.size() == 0 || rowElts.size()==0 || colElts.size()==0 || rowDofs.size()==0 || rowDofs.size()==0)
    {
      string_t stv="", virg=" ", are="";
      if(rowElts.size()==0) { stv+="rowElts "; virg=", "; are=" is "; }
      if(colElts.size()==0) { stv+=virg+"colElts"; virg=", "; if(are=="") are=" is "; else are=" are "; }
      if(rowDofs.size()==0) { stv+=virg+"rowDofs"; virg=", "; if(are=="") are=" is "; else are=" are "; }
      if(colDofs.size()==0) { stv+=virg+"colDofs"; virg=", "; if(are=="") are=" is "; else are=" are "; }
      where("computePartialIE(...)");
      error("is_void", stv+are+"void");
    }

  //retry some global informations
  const Unknown* u_p=subf.up(), *v_p=subf.vp();
  dimen_t nbc_u = u_p->nbOfComponents(), nbc_v = v_p->nbOfComponents(); //number of components of unknown and test function
  number_t nbdofs_u=colDofs.size(), nbdofs_v=rowDofs.size();

  std::vector<number_t>::const_iterator itd;

  //create reverse maps row/col dof numbers -> rank in row/colDofs vector (start from 1)
  // in future: precompute these maps because they are shared
  std::map<number_t,number_t> revRowDofs, revColDofs;
  itd=colDofs.begin();
  for(number_t k=1; k<=nbdofs_u; ++itd, ++k) revColDofs[*itd+1]=k;
  itd=rowDofs.begin();
  for(number_t k=1; k<=nbdofs_v; ++itd, ++k) revRowDofs[*itd+1]=k;

  //allocate temporary vectors and declare main iterators
  Vector<K> val_opu, val_opv, val_opk;
  AdjacenceInfo adjInfo;
  Matrix<K> matel;
  std::map<number_t,number_t>::const_iterator itmd;
  std::list<std::multimap<real_t, IntgMeth> >::const_iterator itiml;
  std::multimap<real_t, IntgMeth>::const_iterator itim, itime, itimf;
  std::vector<KernelOperatorOnUnknowns*>::const_iterator itsin=kopsings.begin(), itreg=kopregs.begin();

  //--------------------------------------------------
  //main loop on u finite elements -> col
  //--------------------------------------------------
  for(number_t ku = 0; ku < colElts.size(); ku++)
    {
      // data related to u
      Element* elt_u = colElts[ku];
      if(elt_u->geomElt_p->meshElement()==nullptr) elt_u->geomElt_p->buildSideMeshElement();
      GeomElement* sidelt_u = elt_u->geomElt_p;
      if(ieparams.extend_u)
        {
          std::map<Element*,GeoNumPair>::const_iterator itm=sidelts_u.find(elt_u);
          if(itm == sidelts_u.end())
            {
              where("computePartialIE(...)");
              error("elt_not_found");
            }
          sidelt_u = itm->second.first;
          ieparams.side_u = itm->second.second;
        }
      ieparams.melt_u = sidelt_u->meshElement();
      ieparams.sh_u = sidelt_u->shapeType();
      ieparams.changeSign_u=false;
      if(elt_u->refElt_p->dofCompatibility == _signDofCompatibility)
        {
          ieparams.sign_u=&elt_u->getDofSigns();
          ieparams.changeSign_u=ieparams.sign_u->size()!=0;
        }
      //local numbering of u element dofs relative to mat col numbering, if not in colDofs specify 0
      const std::vector<number_t>& eltuDofNum = elt_u->dofNumbers;
      number_t nb_u = eltuDofNum.size();
      std::vector<number_t> dofnum_u(nb_u,0);
      std::vector<number_t>::iterator itdu = dofnum_u.begin();
      for(itd=eltuDofNum.begin(); itd!=eltuDofNum.end(); ++itd, ++itdu)
        {
          itmd=revColDofs.find(*itd);
          if(itmd!=revColDofs.end()) *itdu=itmd->second;
        }

      //transmit y-normal to kernels if required and noUpdatedNormal is false
      for(cit_vbfp itf = subf.begin(); itf != subf.end(); itf++)
        {
          const OperatorOnKernel& opk = itf->first->asDoubleIntgForm()->opker();
          if(opk.ynormalRequired()&& !opk.noUpdatedNormal)  setNy(&ieparams.melt_u->geomMapData_p->normalVector);   //update y normal in thread data
        }

      std::vector<number_t> adrs, adrst;
      Matrix<K> matel;                    //define elementary matrix structure

      //main loop on v finite elements -> row
      //--------------------------------------------------
      for(number_t kv = 0; kv < rowElts.size(); kv++)
        {
          // data related to v
          Element* elt_v = rowElts[kv];
          GeomElement* sidelt_v = elt_v->geomElt_p;
          if(ieparams.extend_v)
            {
              std::map<Element*,GeoNumPair>::const_iterator itm=sidelts_v.find(elt_v);
              if(itm == sidelts_v.end())
                {
                  where("computePartialIE(...)");
                  error("elt_not_found");
                }
              sidelt_v = itm->second.first;
              ieparams.side_v = itm->second.second;
            }
          ieparams.melt_v = sidelt_v->meshElement();
          ieparams.sh_v = sidelt_v->shapeType();
          ieparams.changeSign_v=false;
          if(elt_v->refElt_p->dofCompatibility == _signDofCompatibility)
            {
              ieparams.sign_v=&elt_v->getDofSigns();
              ieparams.changeSign_v=ieparams.sign_v->size()!=0;
            }
          //local numbering of v element dofs relative to mat row numbering, if not in rowDofs specify 0
          const std::vector<number_t>& eltvDofNum = elt_v->dofNumbers;
          number_t nb_v = eltvDofNum.size();
          std::vector<number_t> dofnum_v(nb_v,0);
          std::vector<number_t>::iterator itdv = dofnum_v.begin();
          for(itd=eltvDofNum.begin(); itd!=eltvDofNum.end(); ++itd, ++itdv)
            {
              itmd=revRowDofs.find(*itd);
              if(itmd!=revRowDofs.end()) *itdv=itmd->second;
            }
          // local to global numbering
          mat.positions(dofnum_v, dofnum_u, adrs, true); //adresses in mat of elementary matrix stored as a row vector
          std::vector<number_t>::iterator itadrs = adrs.begin(), itk;
          number_t nb_ut = nb_u * nbc_u, nb_vt = nb_v * nbc_v;
          adjInfo.init(*sidelt_u, *sidelt_v, true, false);  //status of elements pair (distance, adjacent by edge, by face, ...)
          itiml=intgMaps.begin();
          itsin=kopsings.begin();
          itreg=kopregs.begin();

          // thePrintStream<<" ====================================================================="<<eol;
          // thePrintStream<<"                  compute IE matel on Elt_"<<elt_u->number()<<" x Elt_"<<elt_v->number()<<eol;
          // thePrintStream<<" ====================================================================="<<eol;
          // thePrintStream<<"  eltvDofNum = "<<eltvDofNum<<"  eltuDofNum = "<<eltuDofNum<<eol;
          // thePrintStream<<"  dofnum_v = "<<dofnum_v<<"  dofnum_u = "<<dofnum_u<<eol<<std::flush;

          //loop on basic bilinear forms, compute elementary matrix on elt_v x elt_u
          //-------------------------------------------------------------------------
          for(cit_vbfp itf = subf.begin(); itf != subf.end(); ++itf, ++itiml, ++itreg, ++itsin)
            {
              const DoubleIntgBilinearForm* ibf = itf->first->asDoubleIntgForm(); //basic bilinear form as double integral
              const OperatorOnKernel& opk = ibf->opker();       //operator on kernel
              bool inext=true;
              Extension* ext=opk.ext_p;
              if(ext!=nullptr)  // opker has an extension, restrict to elements in domain extension
                {
                  std::map<GeomElement*,std::set<number_t> >::const_iterator itme=ext->domToSide.end();
                  if(ext->var_==_x) if(ext->domToSide.find(sidelt_v)==itme) inext=false;
                  if(ext->var_==_y) if(ext->domToSide.find(sidelt_u)==itme) inext=false;
                }
              if(inext)  // computation ku x kv
                {
                  const OperatorOnUnknown& op_u = ibf->opu();       //operator on unknown
                  const OperatorOnUnknown& op_v = ibf->opv();       //operator on test function
                  if(!ieparams.isUptodate || ieparams.nb_bf > 1)    //update computation parameters
                    {
                      ieparams.opisid_u = (op_u.difOpType()==_id);
                      ieparams.opisid_v = (op_v.difOpType()==_id);
                      ieparams.hasf_u = op_u.hasOperand();
                      ieparams.hasf_v = op_v.hasOperand();
                      ieparams.difforder_u = op_u.diffOrder();
                      ieparams.difforder_v = op_v.diffOrder();
                      ieparams.isId = (ieparams.opisid_u && ieparams.opisid_v && !ieparams.hasf_u && !ieparams.hasf_v);
                      if(opk.kernelp()!=nullptr) ieparams.scalar_k = (opk.kernelp()->strucType()==_scalar);
                      ieparams.knor_x=opk.xnormalRequired();
                      ieparams.knor_y=opk.ynormalRequired();
                      ieparams.isUptodate = true;
                    }

                  //transmit x/y-normal to kernels if required (block it if opk.noUpdatedNormal=true;)
                  if(ieparams.knor_x && !noUpdatedNormal && !opk.noUpdatedNormal)
                     setNx(&ieparams.melt_v->geomMapData_p->normalVector);  //update x normal in thread data

                  // compute integral
                  K coef = complexToT<K>(itf->second);                        //coefficient of linear form
                  const KernelOperatorOnUnknowns* kopus = &ibf->kopus();
                  matel.changesize(dimen_t(nb_vt), dimen_t(nb_ut), K(0));     //reset elementary matrix to 0
                  const std::multimap<real_t, IntgMeth>& intgMap=*itiml;      //integration methods indexed by bound
                  itim = intgMap.lower_bound(adjInfo.dist);
                  itime= intgMap.upper_bound(adjInfo.dist);
                  if(itim==itime) itime= intgMap.upper_bound(itim->first);
                  bool first = true;
                  for(; itim!=itime; ++itim)  // loop on integration methods, 2 if compute singular and regular part, 1 else
                    {
                      const IntegrationMethod* im = itim->second.intgMeth;
                      switch(itim->second.functionPart)
                        {
                          case _regularPart: kopus = *itreg; break;
                          case _singularPart: kopus = *itsin; break;
                          default: break;
                        }
                      if(!first) matel*=K(0);
                      first=false;
                      if(im->type()==_productIM)   //generic quatrature method
                        {
                          if(!im->useQuadraturePoints()) error("im_not_product");
                          Quadrature* qimx= static_cast<QuadratureIM*>(static_cast<const ProductIM*>(im)->getxIM())->getQuadrature(sidelt_v->shapeType()),
                                      *qimy= static_cast<QuadratureIM*>(static_cast<const ProductIM*>(im)->getyIM())->getQuadrature(sidelt_u->shapeType());
                          computeQuadratureIE(elt_u, elt_v, *kopus, qimx, qimy, matel, ieparams, val_opu, val_opv, val_opk);
                        }
                      else //singular or special integration method
                        {
                          reinterpret_cast<const DoubleIM*>(im)->computeIE(elt_u, elt_v, adjInfo, *kopus, matel, ieparams, val_opu, val_opv, val_opk);
                        }
                      matel*=coef;
                      // assembling matel in global matrix
                      std::vector<number_t>::iterator itd_ub = dofnum_u.begin(), itd_ue = dofnum_u.end(), itd_u, itd_v;   //dof numbering iterators
                      typename Matrix<K>::iterator itmatel = matel.begin(), itm_u;
                      number_t incr = nbc_u * nbc_v * nb_u; //block row increment in matel
                      number_t i = 0;
                      //loop on (vector) dofs in u and v
                      for(itd_v = dofnum_v.begin(); itd_v != dofnum_v.end() ; itd_v++, itmatel += incr, i++)
                        {
                          itm_u = itmatel;
                          itk = itadrs + i * nb_u;
                          for(itd_u = itd_ub; itd_u != itd_ue ; itd_u++, itm_u += nbc_u, itk++)
                            {
                              if(*itd_u>0) assemblyMatNoCritical(mat(*itk), itm_u, nb_ut);
                            }
                        }
                    }
                } //end if(inext)
            } //end loop bilinear form
        }//end loop elt_v
    }//end loop elt_u
}

/* ================================================================================================
                                IE partial row/col computation
   ================================================================================================ */

/*! compute a row or a col of integral equation
  subf: bilinear form defining ie to compute
  row: true if a row computation, false if a col computation
  n: length of row/col
  rowcol: pointer to the first element of row or col, has to be correctly sized before
  rc: row/col index (>=1)
  crDofs: col/ row dof indices
  crElts: list of elements supporting col/row dofs
  rcSpace: space of row/col dof
  IEcomputationParameters: computation ie parameters
  intgMaps: list of integration methods to be used (built outside)
  kopregs , kopsings: list of regular and singular kernels when required

  NOTE:for the moment this routine uses the general computePartialIE that involves LargeMatrix
*/
template <typename T, typename K>
void computeRowColIE(const SuBilinearForm& subf, bool row, number_t rc, T* rowcol, number_t n, const std::vector<number_t>& crDofs,
                     const std::vector<Element*>& crElts, const Space* rcSpace, IEcomputationParameters& ieparams, K& vt,
                     const std::list<std::multimap<real_t, IntgMeth> >& intgMaps,
                     const std::vector<KernelOperatorOnUnknowns*>& kopregs, const std::vector<KernelOperatorOnUnknowns*>& kopsings,
                     const std::map<Element*,GeoNumPair>& sidelts_u, const std::map<Element*,GeoNumPair>& sidelts_v,
                     bool noUpdatedNormal, bool same_interpolation, bool sym)
{
  std::vector<number_t> rcdof(1,0);                      //to store temporary row/col dof index
  std::vector<Element*> rcelts;                          //to store temporary element pointers related to row/col dof
  rcdof[0]=rc;                                           //numbering shifting has to be done before !
  const std::vector<std::pair<Element*, number_t> >& elts=rcSpace->feDofs()[rc].elements();
  rcelts.resize(elts.size());
  std::vector<std::pair<Element*, number_t> >::const_iterator itme = elts.begin();
  std::vector<Element*>::iterator ite = rcelts.begin();
  for(; ite!=rcelts.end(); ++ite,++itme) *ite=itme->first;
  LargeMatrix<T>* lm;
  if(row)
    {
      lm = new LargeMatrix<T>(1, n, _dense, _row);
      computePartialIE(subf, *lm, vt, rcdof, crDofs, rcelts, crElts, ieparams, intgMaps, kopregs, kopsings,
                       sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
    }
  else
    {
      lm = new LargeMatrix<T>(n, 1, _dense, _col);
      computePartialIE(subf, *lm, vt, crDofs, rcdof, crElts, rcelts,  ieparams, intgMaps, kopregs, kopsings,
                       sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
    }

  typename std::vector<T>::iterator itlm=lm->values().begin()+1;
  for(number_t i=0; i<n; ++i, ++rowcol, ++itlm) *rowcol = *itlm;

  delete lm;
}

/* ================================================================================================
                Adaptative Cross Approximation with Full pivoting (ACA full)
   ================================================================================================ */
/*!compute Low Rank Matrix  using ACA full pivoting method
algorithm from the phd thesis of Benoît Lizé

T: type of the matrix coefficient    K: type of computation (real or complex)

subf: a single unknown bilinear form defined on a unique domains pair (assumed)
vt: to pass as template the scalar type (real or complex)
lrm: the LowRankMatrix to build
rmax: maximum rank of low rank matrix (if 0, not used)
eps: threshold of low rank matrix: |A-Ar|< eps*|A|
rowDofs, colDofs: row and col dofs involved
rowElts, colElts: row/col elements supporting row/col dofs
ieparams: useful informations on ie computation
intgMaps: list of integration methods to be used (built outside)
kopregs , kopsings: list of regular and singular kernels when required

Produce a low rank matrix of the form A*B' with A of size m x k and B of size n x k, k the rank
*/
template <typename T, typename K>
void acaFullMethod(const SuBilinearForm& subf, LowRankMatrix<T>& lrm, number_t rmax, real_t eps,
             const std::vector<number_t>& rowDofs, const std::vector<number_t>& colDofs,
             const std::vector<Element*>& rowElts, const std::vector<Element*>& colElts,
             IEcomputationParameters& ieparams, K& vt,
             const std::list<std::multimap<real_t, IntgMeth> >& intgMaps,
             const std::vector<KernelOperatorOnUnknowns*>& kopregs, const std::vector<KernelOperatorOnUnknowns*>& kopsings,
             const std::map<Element*,GeoNumPair>& sidelts_u, const std::map<Element*,GeoNumPair>& sidelts_v,
             bool noUpdatedNormal, bool same_interpolation, bool sym)
{
  number_t k=1, m=rowDofs.size(), n=colDofs.size();
  LargeMatrix<T>* lm = new LargeMatrix<T>(m, n,_dense, _row);
  computePartialIE(subf, *lm, vt, rowDofs, colDofs, rowElts, colElts, ieparams, intgMaps, kopregs, kopsings,
                   sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
  //thePrintStream<<"Large matrix "<<*lm<<eol;
  std::vector<T>& mat=lm->values();
  real_t r0=lm->norm2(), rk;
  typename std::vector<T>::iterator itm;
  std::list<std::vector<T> > A, B ; //list of A,B rows
  typename std::list<std::vector<T> >::iterator itA, itB;
  typename std::vector<T>::iterator itv, ita;
  number_t kmax=std::min(m,n);

  bool cont =true;
  while(cont && k<=kmax)
    {
      itm=mat.begin()+1;
      number_t imax = 0, jmax=0;
      T amax=0.;
      for(number_t i=0; i<m; ++i)
        for(number_t j=0; j<n; ++j, ++itm)
          if(abs(*itm)>abs(amax))
            {
              amax=*itm;
              imax=i;
              jmax=j;
            }
      if(abs(amax) < theTolerance) cont = false;
      else
        {
          //update low rank approximation
          std::vector<T> a(m,T());
          itv=a.begin();
          itm=mat.begin()+(1+jmax);
          for(number_t i=0; i<m; ++i, itm+=n, ++itv) *itv = *itm;
          A.push_back(a);
          std::vector<T> b(n,T());
          itv = b.begin();
          itm=mat.begin()+(1+imax*n);
          for(number_t j=0; j<n; ++j, ++itm, ++itv) *itv = *itm/amax;
          B.push_back(b);
          // update mat = mat - A*B'
          itm=mat.begin()+1;
          for(number_t i=0; i<m; ++i)
            {
              T ai = *(a.begin()+i);
              for(number_t j=0; j<n; ++j, ++itm)
                *itm -=  ai * conj(*(b.begin()+j));
            }
          // compute new residue
          rk=lm->norm2();
          // update convergence test
          k++;
          if(rmax==0) cont = rk > eps * r0;
          else cont = k < rmax;
        }
    }

  //fill LowRankMatrix
  number_t r=A.size();
  lrm.D_.clear();
  lrm.U_.changesize(m,r,T());
  lrm.V_.changesize(n,r,T());
  itA=A.begin();
  itB=B.begin();
  for(number_t l=0; l<r; ++l,++itA, ++itB)
    {
      itv = lrm.U_.begin()+l;
      for(ita = itA->begin(); ita != itA->end(); ++ita, itv+=r) *itv=*ita;

      itv = lrm.V_.begin()+l;
      for(ita = itB->begin(); ita != itB->end(); ++ita, itv+=r) *itv=*ita;
    }
//  thePrintStream<<"Low rank approximation using ACA full  of a "<<m<<" x "<<n<<" matrice, rank "<<r<<eol;
//  thePrintStream<<"U matrix "<<lrm.U_<<eol;
//  thePrintStream<<"V matrix "<<lrm.V_<<eol;
//  thePrintStream<<"U*V matrix "<<lrm.toLargeMatrix()<<eol;
  delete lm;
  return;
}

/* ================================================================================================
                Adaptative Cross Approximation with Partial pivoting (ACA partial)
   ================================================================================================ */
/*!compute Low Rank Matrix  using ACA partial pivoting method
   algorithm from the phd thesis of Benoît Lizé

   T: type of the matrix coefficient    K: type of computation (real or complex)

   subf: a single unknown bilinear form defined on a unique domains pair (assumed)
   vt: to pass as template the scalar type (real or complex)
   mat: the LowRankMatrix to build
   rowDofs, colDofs: row and col dofs involved (start at 0)
   rowElts, colElts: pointers to row/col element supporting row/col dofs
   rowSpace, colSpace: row/col spaces (required to get FeDof)
   ieparams: useful informations on ie computation
   intgMaps: list of integration methods to be used (built outside)
   kopregs , kopsings: list of regular and singular kernels when required

   Produce a low rank matrix of the form A*I*B
*/
template <typename T, typename K>
void acaPartialMethod(const SuBilinearForm& subf, LowRankMatrix<T>& lrm, number_t rmax, real_t eps,
                const std::vector<number_t>& rowDofs, const std::vector<number_t>& colDofs,
                const std::vector<Element*>& rowElts, const std::vector<Element*>& colElts,
                const Space* rowSpace, const Space* colSpace,
                IEcomputationParameters& ieparams, K& vt,
                const std::list<std::multimap<real_t, IntgMeth> >& intgMaps,
                const std::vector<KernelOperatorOnUnknowns*>& kopregs, const std::vector<KernelOperatorOnUnknowns*>& kopsings,
                const std::map<Element*,GeoNumPair>& sidelts_u, const std::map<Element*,GeoNumPair>& sidelts_v,
                bool noUpdatedNormal, bool same_interpolation, bool sym)
{
  number_t m=rowDofs.size(), n=colDofs.size();
  number_t k=1, kmax=std::min(m,n), ip = 0, jp=0;
  const std::vector<FeDof> &rowFeDofs=rowSpace->feDofs(), &colFeDofs=colSpace->feDofs(); //list of all FeDofs
  std::vector<number_t> rowmat(m,0), colmat(n,0);        //row/col matrix computed indices
  std::list<Vector<T> > A, B ;                           //list of A,B rows
  std::vector<number_t> rcdof(1,0);                      //to store temporary row/col dof index
  std::vector<Element*> rcelts;                          //to store temporary element pointers related to row/col dof
  typename std::list<Vector<T> >::iterator itA, itB;
  typename Vector<T>::iterator itv, ita;
  std::vector<std::pair<Element*, number_t> >::const_iterator itme;
  std::vector<Element*>::iterator ite;
  real_t rk=0.;             //estimated norm of low rank approximation
  T piv;
  bool cont =true;

  while(cont && k<=kmax)
    {
      //compute row ip of matrix
      rcdof[0]=rowDofs[ip];
      const std::vector<std::pair<Element*, number_t> >& elts=rowFeDofs[rcdof[0]].elements();
      rcelts.resize(elts.size());
      itme=elts.begin();
      ite=rcelts.begin();
      for(; ite!=rcelts.end(); ++ite,++itme) *ite=itme->first;
      LargeMatrix<T>* lm = new LargeMatrix<T>(1, n, _dense, _row);
      computePartialIE(subf, *lm, vt, rcdof, colDofs, rcelts, colElts, ieparams, intgMaps, kopregs, kopsings,
                       sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
      B.push_back(std::vector<T>(lm->values().begin()+1,lm->values().end()));
      delete lm;
      rowmat[ip]=1;
      //update B
      if(k>1)
        {
          itv=B.back().begin();
          for(number_t j=0; j<n; ++j, ++itv)
            {
              T t=T();
              itA = A.begin();
              itB=B.begin();
              for(number_t l=1; l<k; ++l, ++itA, ++itB) t+=(*(itA->begin()+ip) * conj(*(itB->begin()+j)));
              *itv-=t;
            }
        }
      //new column pivot
      itv=B.back().begin();
      jp=0;
      piv=0.;
      for(number_t j=0; j<n; ++j, ++itv)
        if(colmat[j]==0 && abs(*itv)>abs(piv))
          {
            piv=*itv;
            jp=j;
          }
      if(abs(piv)<theZeroThreshold)
        {
          ip=0;
          while(rowmat[ip]!=0  && ip<m) ip++;
          if(ip==m)
            {
              thePrintStream<<"in acaPartial, iteration "<<k<<" pivot breakdown, ip="<<ip<<eol;
              thePrintStream<<"rcdofs="<<rcdof[0]<<" rcelts="<<*rcelts[0]<<" colDofs="<<colDofs<<" colElts="<<colElts<<eol<<"B = "<<B<<eol;
              where("acaPartialMethod(...)");
              error("pivot_not_available");
            }
        }
      else
        {
          itv=B.back().begin();
          for(; itv!=B.back().end(); ++itv) *itv/=piv;
          //compute col jp of matrix
          rcdof[0]=colDofs[jp];
          const std::vector<std::pair<Element*, number_t> >& elts=colFeDofs[rcdof[0]].elements();
          rcelts.resize(elts.size());
          itme=elts.begin();
          ite=rcelts.begin();
          for(; ite!=rcelts.end(); ++ite,++itme) *ite=itme->first;
          LargeMatrix<T>* lm = new LargeMatrix<T>(m, 1, _dense, _col);
          computePartialIE(subf, *lm, vt, rowDofs, rcdof, rowElts, rcelts, ieparams, intgMaps, kopregs, kopsings,
                           sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
          A.push_back(std::vector<T>(lm->values().begin()+1,lm->values().end()));
          colmat[jp]=1;
          delete lm;
          //update A
          itv=A.back().begin();
          if(k>1)
            {
              for(number_t i=0; i<m; ++i, ++itv)
                {
                  T t=T();
                  itA = A.begin();
                  itB=B.begin();
                  for(number_t l=1; l<k; ++l, ++itA, ++itB) t+=(*(itA->begin()+i) * conj(*(itB->begin()+jp)));
                  *itv-=t;
                }
            }
          //update row pivot
          itv=A.back().begin();
          ip=0;
          piv=0.;
          for(number_t i=0; i<m; ++i, ++itv)
            if(rowmat[i]==0 && abs(*itv)>abs(piv))
              {
                piv=*itv;
                ip=i;
              }

          //convergence criteria
          if(rmax!=0) cont = k < rmax;
          else  //estimate residue
            {
              if(k>1)
                {
                  itA = A.begin();
                  itB=B.begin();
                  for(number_t l=1; l<k; ++l, ++itA, ++itB)
                    {
                      rk+=2*(dotC(A.back(),*itA)*dotC(B.back(), *itB)).real();
                    }
                }
              real_t ak=A.back().norm2(), bk=B.back().norm2();
              rk+= ak*ak*bk*bk;
              //thePrintStream<<"ak*bk="<<ak*bk<<" rk="<<std::sqrt(rk);
              cont = ak*bk > eps * std::sqrt(rk);
            }
          k++;
        }
    }

  //fill LowRankMatrix
  number_t r=A.size();
  lrm.D_.clear();
  lrm.U_.changesize(m,r,T());
  lrm.V_.changesize(n,r,T());
  itA=A.begin();
  itB=B.begin();
  for(number_t l=0; l<r; ++l,++itA, ++itB)
    {
      itv = lrm.U_.begin()+l;
      for(ita = itA->begin(); ita != itA->end(); ++ita, itv+=r) *itv=*ita;

      itv = lrm.V_.begin()+l;
      for(ita = itB->begin(); ita != itB->end(); ++ita, itv+=r) *itv=*ita;
    }
}

// update tool for ACA+
template <typename ITV, typename ITAB, typename T>
void updateAcaPlus(ITV itv, ITAB itAb, ITAB itBb, number_t mn, number_t lmax, number_t ijp, bool row, T& piv, number_t& jip,
                   bool updatePiv, std::vector<number_t>* rowcol = nullptr)
{
  ITAB itA, itB;
  if(row)
    {
      if(updatePiv) piv=0.*T();
      for(number_t ij=0; ij<mn; ++ij, ++itv)
        {
          itA = itAb;
          itB=itBb;
          //for(number_t l=1; l<lmax; ++l, ++itA, ++itB) *itv-=(*(itA->begin()+ij) * conj(*(itB->begin()+ijp)));
          for(number_t l=1; l<lmax; ++l, ++itA, ++itB) *itv-=(*(itA->begin()+ij) * *(itB->begin()+ijp));
          if(updatePiv)
            {
              if((rowcol==nullptr || (rowcol!=nullptr && (*rowcol)[ij]==0)) && abs(*itv)>abs(piv))
                {
                  piv=*itv;
                  jip=ij;
                }
            }
          else  *itv/=piv;
        }
    }
  else
    {
      if(updatePiv) piv=0.*T();
      for(number_t ij=0; ij<mn; ++ij, ++itv)
        {
          itA = itAb;
          itB=itBb;
          //for(number_t l=1; l<lmax; ++l, ++itA, ++itB) *itv-=(*(itA->begin()+ijp) * conj(*(itB->begin()+ij)));
          for(number_t l=1; l<lmax; ++l, ++itA, ++itB) *itv-=(*(itA->begin()+ijp) * *(itB->begin()+ij));
          if(updatePiv)
            {
              if((rowcol==nullptr || (rowcol!=nullptr && (*rowcol)[ij]==0)) && abs(*itv)>abs(piv))
                {
                  piv=*itv;
                  jip=ij;
                }
            }
          else  *itv/=piv;
        }
    }
}


/* ================================================================================================
            Improved Adaptative Cross Approximation with Partial pivoting (ACA plus)
   ================================================================================================ */
/*!compute Low Rank Matrix  using ACA plus partial pivoting method
   algorithm from the phd thesis of Benoît Lizé

   T: type of the matrix coefficient    K: type of computation (real or complex)

   subf: a single unknown bilinear form defined on a unique domains pair (assumed)
   vt: to pass as template the scalar type (real or complex)
   mat: the LowRankMatrix to build
   rowDofs, colDofs: row and col dofs involved (start at 0)
   rowElts, colElts: pointers to row/col element supporting row/col dofs
   rowSpace, colSpace: row/col spaces (required to get FeDof)
   ieparams: useful informations on ie computation
   intgMaps: list of integration methods to be used (built outside)
   kopregs , kopsings: list of regular and singular kernels when required

   Produce a low rank matrix of the form A*I*B
*/
template <typename T, typename K>
void acaPlusMethod(const SuBilinearForm& subf, LowRankMatrix<T>& lrm, number_t rmax, real_t eps,
             const std::vector<number_t>& rowDofs, const std::vector<number_t>& colDofs,
             const std::vector<Element*>& rowElts, const std::vector<Element*>& colElts,
             const Space* rowSpace, const Space* colSpace, IEcomputationParameters& ieparams, K& vt,
             const std::list<std::multimap<real_t, IntgMeth> >& intgMaps,
             const std::vector<KernelOperatorOnUnknowns*>& kopregs, const std::vector<KernelOperatorOnUnknowns*>& kopsings,
             const std::map<Element*,GeoNumPair>& sidelts_u, const std::map<Element*,GeoNumPair>& sidelts_v,
             bool noUpdatedNormal, bool same_interpolation, bool sym)
{
  number_t m=rowDofs.size(), n=colDofs.size();
  number_t k=1, kmax=std::min(m,n), ip = 0, jp=0;
  std::vector<number_t> rowmat(m,0), colmat(n,0);        //row/col matrix computed indices
  std::list<Vector<T> > A, B ;                           //list of A,B rows
  typename std::list<Vector<T> >::iterator itA, itB;
  typename Vector<T>::iterator itv, ita;
  real_t rk=0.;             //estimated norm of low rank approximation
  T piv=0.;
  bool cont =true;

  //initialization
  Vector<T> cpr(m), lpr(n);
  number_t jrp = 0;                     // choose randomly later
  computeRowColIE(subf,false,colDofs[jrp],&cpr[0],m,rowDofs,rowElts,rowSpace,ieparams, vt, intgMaps, kopregs, kopsings,
                  sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
  number_t irp = 0;
  itv=cpr.begin();
  piv=*itv;
  itv++;
  for(number_t i=1; i<m; ++i, ++itv)    //find row ip related to the max of cp
    if(abs(*itv)<abs(piv))
      {
        piv=*itv;
        irp=i;
      }
  computeRowColIE(subf,true,rowDofs[irp],&lpr[0],n,colDofs,colElts,colSpace,ieparams, vt, intgMaps, kopregs, kopsings,
                  sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
  Vector<T> cp(m), lp(n);

  while(cont && k<=kmax)
    {
      // update cp, lp
      cp=cpr;
      lp=lpr;
      updateAcaPlus(cp.begin(), A.begin(), B.begin(), m, k, jrp, true, piv, ip, true);
      updateAcaPlus(lp.begin(), A.begin(), B.begin(), n, k, irp, false, piv, jp, true);

      if(abs(cp[ip]) > abs(lp[jp]))  // ip is fixed, find jp
        {
          B.push_back(std::vector<T>(n));
          computeRowColIE(subf,true,rowDofs[ip],&B.back()[0],n,colDofs,colElts,colSpace,ieparams, vt, intgMaps, kopregs, kopsings,
                          sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
          updateAcaPlus(B.back().begin(), A.begin(), B.begin(), n, k, ip, false, piv, jp, true);
          if(abs(piv)< theZeroThreshold)
            {
              thePrintStream<<"ACA+ compression stops on a small pivot at iteration "<<k<<" pivot="<<piv<<eol;
              thePrintStream<<" row "<<ip<<" B= "<<B.back()<<eol;
              warning("free_warning","ACA+ compression stops on a small pivot");
              B.pop_back();   //remove last vector B and stop the process
              cont = false;
            }
          else
            {
              A.push_back(std::vector<T>(m));
              computeRowColIE(subf,false,colDofs[jp],&A.back()[0],m,rowDofs,rowElts,rowSpace,ieparams, vt, intgMaps, kopregs, kopsings,
                              sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
              updateAcaPlus(A.back().begin(), A.begin(), B.begin(), m, k, jp, true, piv, ip, false);
            }
        }
      else // jp is fixed, find ip
        {
          A.push_back(std::vector<T>(m));
          computeRowColIE(subf,false,colDofs[jp],&A.back()[0],m,rowDofs,rowElts,rowSpace,ieparams, vt, intgMaps, kopregs,  kopsings,
                          sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
          updateAcaPlus(A.back().begin(), A.begin(), B.begin(), m, k, jp, true, piv, ip, true);
          if(abs(piv)< theZeroThreshold)
            {
              thePrintStream<<"ACA+ compression stops on a small pivot at iteration  "<<k<<" pivot="<<piv<<eol;
              thePrintStream<<" col "<<jp<<" B= "<<A.back()<<eol;
              warning("free_warning","ACA+ compression stops on a small pivot");
              A.pop_back();   //remove last vector A and stop the process
              cont = false;
            }
          else
            {
              B.push_back(std::vector<T>(n));
              computeRowColIE(subf,true,rowDofs[ip],&B.back()[0],n,colDofs,colElts,colSpace,ieparams, vt, intgMaps, kopregs,  kopsings,
                              sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
              updateAcaPlus(B.back().begin(), A.begin(), B.begin(), n, k, ip, false, piv, jp, false);
            }
        }

      if(cont)
        {
          rowmat[ip]=1;
          colmat[jp]=1;
          if(ip==irp && jp==jrp)
            {
              jrp=0;
              while(colmat[jrp]!=0 && jrp<n) jrp++;   //choose randomly later
              if(jrp<n)
                {
                  computeRowColIE(subf,false,colDofs[jrp],&cpr[0],m,rowDofs,rowElts,rowSpace,ieparams, vt, intgMaps, kopregs,  kopsings,
                                  sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
                  irp = 0;
                  itv=cpr.begin();
                  piv=*itv;
                  ++itv;
                  for(number_t i=1; i<m; ++i, ++itv)    //find row ip related to the max of cp
                    if(abs(*itv)<abs(piv))
                      {
                        piv=*itv;
                        irp=i;
                      }
                  computeRowColIE(subf,true,rowDofs[irp],&lpr[0],n,colDofs,colElts,colSpace,ieparams, vt, intgMaps, kopregs,  kopsings,
                                  sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
                }
              else
                {
                  thePrintStream<<"ACA+ compression stops because no more columns are available"<<eol;
                  warning("free_warning","ACA+ compression stops because no more columns are available");
                  cont=false;
                }
            }
          else
            {
              if(ip==irp)
                {
                  itv=B.back().begin();
                  jrp=0;
                  bool minset=false;
                  for(number_t j=0; j<n; ++j, ++itv)
                    {
                      if(colmat[j]==0)
                        {
                          if(!minset)
                            {
                              piv=*itv;
                              jrp=j;
                              minset=true;
                            }
                          else if(abs(*itv)<abs(piv))
                            {
                              piv=*itv;
                              jrp=j;
                            }
                        }
                    }
                  computeRowColIE(subf,false,colDofs[jrp],&cpr[0],m,rowDofs,rowElts,rowSpace,ieparams, vt, intgMaps, kopregs,  kopsings,
                                  sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
                }
              if(jp==jrp)
                {
                  itv=A.back().begin();
                  irp=0;
                  bool minset=false;
                  for(number_t i=0; i<m; ++i, ++itv)
                    {
                      if(rowmat[i]==0)
                        {
                          if(!minset)
                            {
                              piv=*itv;
                              irp=i;
                              minset=true;
                            }
                          else if(abs(*itv)<abs(piv))
                            {
                              piv=*itv;
                              irp=i;
                            }
                        }
                    }
                  computeRowColIE(subf,true,rowDofs[irp],&lpr[0],n,colDofs,colElts,colSpace,ieparams, vt, intgMaps, kopregs,  kopsings,
                                  sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
                }
            }

          //convergence criteria
          if(rmax!=0) cont = k < rmax;
          else  //estimate residue
            {
              if(k>1)
                {
                  itA = A.begin();
                  itB=B.begin();
                  for(number_t l=1; l<k; ++l, ++itA, ++itB)
                    {
                      rk+=2*(dotC(A.back(),*itA)*dotC(B.back(), *itB)).real();
                    }
                }
              real_t ak=A.back().norm2(), bk=B.back().norm2();
              rk+= ak*ak*bk*bk;
              cont = ak*bk > eps * std::sqrt(rk);
            }
          k++;
        }
    }

//fill LowRankMatrix
  number_t r=A.size();
  lrm.D_.clear();
  lrm.U_.changesize(m,r,T());
  lrm.V_.changesize(n,r,T());
  itA=A.begin();
  itB=B.begin();
  for(number_t l=0; l<r; ++l,++itA, ++itB)
    {
      itv = lrm.U_.begin()+l;
      for(ita = itA->begin(); ita != itA->end(); ++ita, itv+=r) *itv=*ita;

      itv = lrm.V_.begin()+l;
      for(ita = itB->begin(); ita != itB->end(); ++ita, itv+=r) *itv=conj(*ita);
    }
}

/* ================================================================================================
                                IE computation HMatrix algorithm
   ================================================================================================ */
/*!IE computation of a scalar SuBiinearForm on a == unique domain == using HMatrix method type
   T: type of the matrix coefficient
   K: type of computation (real or complex)
   I: type of the tree node index of the HMatrix

   subf: a single unknown bilinear form defined on a unique domains pair (assumed)
   mat: the HMatrix, its structure is assumed to be up to date
   space_u_p, space_v_p: pointers to real u-space and real v-space
   vt: to pass as template the scalar type (real or complex)
*/

template <typename T, typename K, typename I>
void computeHMatrix(const SuBilinearForm& subf, HMatrix<T,I>& mat, K& vt,
                    Space* space_u_p, Space* space_v_p, const Unknown* u_p, const TestFct* v_p)
{
  trace_p->push("SuTermMatrix::computeIE_HMatrix");

  //general information
  //manage  normal update
  bool noUpdatedNormal=false;//enable automatic update of normals in parameters of kernel
  #ifdef XLIFEPP_WITH_OMP
    noUpdatedNormal=true;      //disable automatic update of normals in parameters of kernel (not thread safe)
  #endif // XLIFEPP_WITH_OMP

  //retry spaces
  cit_vbfp it = subf.begin();
  const Space* sp_u = subf.up()->space();  //space of unknown
  const Space* sp_v = subf.vp()->space();  //space of test function
  const GeomDomain* domx = it->first->asDoubleIntgForm()->domainx(); //common domain x of v-domain integrals
  const GeomDomain* domy = it->first->asDoubleIntgForm()->domainy(); //common domain y of u-domain integrals
  const MeshDomain* mdomx = domx->meshDomain();
  const MeshDomain* mdomy = domy->meshDomain();
  bool same_interpolation = (sp_u == sp_v && domx == domy);

  //retry extension information
  bool ext_u=false, ext_v=false;
  Extension* ext_opk = nullptr;
  bool sidex= mdomx->isSideDomain() && sp_v->domain()->dim() > mdomx->dim(); //true if a side x-domain and v-space is supported by a domain of larger dimension
  bool sidey= mdomy->isSideDomain() && sp_u->domain()->dim() > mdomy->dim(); //true if a side y-domain and u-space is supported by a domain of larger dimension
  for (cit_vbfp itf = subf.begin(); itf != subf.end() && !ext_u && !ext_v; ++itf)
  {
    const KernelOperatorOnUnknowns& kopuv=itf->first->asDoubleIntgForm()->kopus();
    if (!ext_u && sidey && kopuv.opu().extensionRequired()) ext_u=true;
    if (!ext_v && sidex && kopuv.opv().extensionRequired()) ext_v=true;
    if (kopuv.opker().ext_p!=nullptr)
    {
      if(ext_opk!=nullptr) error("extension_already_set");
      else ext_opk = kopuv.opker().ext_p;
    }
  }

  number_t nbthread=numberOfThreads();
  theThreadData.resize(nbthread);
  string_t ext="";
  if (ext_u || ext_v) ext="extended ";
  if (theVerboseLevel > 0) std::cout<<"computing IE "<<ext<<"term "<< subf.asString()<<", using "<<nbthread<<" threads: "<<std::flush;

  //compute orientation if it is not computed (outward normal if a boundary of a domain, towards infinite if an interface or a manifold)
  if (mdomx->isSideDomain() && !mdomx->orientationComputed) mdomx->setNormalOrientation();
  if (domy!=domx && mdomy->isSideDomain() && !mdomy->orientationComputed) mdomy->setNormalOrientation();

  //space_u_p and space_v_p are the largest subspaces involved in the computation, may be different from the whole spaces
  Space* subsp_u = nullptr;
  if (!ext_u) subsp_u = sp_u->findSubSpace(domy, space_u_p);                 //find subspace (linked to domain domy) of space_u_p
  else subsp_u = sp_u->findSubSpace(domy->extendedDomain(), space_u_p);    //find subspace (linked to extended domain domy) of space_u_p
  if(subsp_u == nullptr) subsp_u = space_u_p;                                     //is a FESpace, not a FESubspace
  Space* subsp_v = subsp_u;
  if (!same_interpolation || ext_u!=ext_v)
  {
    if (!ext_v) subsp_v = sp_v->findSubSpace(domx, space_v_p);             //find subspace (linked to domain domx) of space_v_p
    else subsp_v = sp_v->findSubSpace(domx->extendedDomain(), space_v_p);//find subspace (linked to extended domain domx) of space_v_p
    if (subsp_v == nullptr) subsp_v = space_v_p;                                 //is a FESpace, not a FESubspace
  }
  bool doflag_u = (subsp_u == sp_u), doflag_v = (subsp_v == sp_v);
  if (!doflag_u) space_u_p->builddofid2rank();                            //build the map dofid -> rank for space_u_p
  if (!doflag_v && space_v_p!=space_u_p) space_v_p->builddofid2rank();    //build the map dofid -> rank for space_v_p
  dimen_t nbc_u = u_p->nbOfComponents(), nbc_v = v_p->nbOfComponents(); //number of components of unknown and test function

  same_interpolation = same_interpolation  && subsp_u==subsp_v;

  //scalar or vector problem
  // dimfun_u, dimfun_v are the dimensions of shape functions in the reference element
  // if the ref element is mapped to a physical element of greater dimension, dimension of shape functions has to be increased
  // occurs for element with mapType _contravariantPiolaMap or _covariantPiolaMap and  dim() > dimElt()
  dimen_t dimfun_u = sp_u->dimFun();    //dimension of u-shape functions of ref element
  dimen_t dimfunp_u = dimfun_u;         //dimension of u-shape functions of phy element
  const Element* elt = subsp_u->element_p(number_t(0));
  FEMapType femt = elt->refElt_p->mapType;
  if ((femt==_contravariantPiolaMap || femt==_covariantPiolaMap) && elt->dim() > elt->dimElt()) dimfunp_u = elt->dim();
  if (nbc_u > 1)    //case of vector dofs (scalar shape functions will be extended to vector shape functions)
  {
    if(dimfun_u==1) {dimfun_u = nbc_u; dimfunp_u = nbc_u;}
    else error("fun_bad_dim", 1, dimfun_u);
  }
  dimen_t dimfun_v = sp_v->dimFun();   //dimension of v-shape functions of ref element
  dimen_t dimfunp_v = dimfun_v;         //dimension of v-shape functions of phy element
  elt = subsp_v->element_p(number_t(0));
  femt = elt->refElt_p->mapType;
  if ((femt==_contravariantPiolaMap || femt==_covariantPiolaMap) && elt->dim() > elt->dimElt()) dimfunp_v = elt->dim();
  if (nbc_v > 1)
  {
    if(dimfun_v==1) {dimfun_v = nbc_v; dimfunp_v = nbc_v;}
    else error("fun_bad_dim", 1, dimfun_v);
  }
  bool sym = same_interpolation && !(mat.sym == _noSymmetry);         //symmetry flag

  //set IE computation parameters
  IEcomputationParameters ieparams;
  ieparams.dim_sp = domx->spaceDim();
  ieparams.dimf_u = dimfun_u;
  ieparams.dimf_v = dimfun_v;
  ieparams.dimfp_u = dimfunp_u;
  ieparams.dimfp_v = dimfunp_v;
  ieparams.ord_u  = sp_u->interpolation()->numtype;
  ieparams.ord_v  = sp_v->interpolation()->numtype;
  ieparams.isP0   = (sp_u->interpolation()->numtype==0) && (sp_v->interpolation()->numtype==0);
  ieparams.femt_u = subsp_u->element_p(number_t(0))->refElt_p->mapType;
  ieparams.femt_v = subsp_v->element_p(number_t(0))->refElt_p->mapType;
  ieparams.rotsh_u= subsp_u->element_p(number_t(0))->refElt_p->rotateDof;
  ieparams.rotsh_v= subsp_v->element_p(number_t(0))->refElt_p->rotateDof;
  ieparams.nb_bf = subf.size();
  //extension flags, if true means that u/v lives on a extended domain
  ieparams.extend_u = ext_u;
  ieparams.extend_v = ext_v;

  //define side numbers of extended domain elements (if required)
  std::map<Element*,GeoNumPair> sidelts_u, sidelts_v;  //empty
  if (ext_u)
  {
    std::map<GeomElement*, Element*> eltext;
    for (number_t k = 0; k < subsp_u->nbOfElements(); k++)
    {
      Element* elt=const_cast<Element*>(subsp_u->element_p(k));
      eltext[elt->geomElt_p]=elt;
    }
    std::map<GeomElement*, Element*>::iterator itl;
    std::vector<GeomElement*>::const_iterator ite=domy->meshDomain()->geomElements.begin();
    for (; ite!=domy->meshDomain()->geomElements.end(); ++ite)
    {
      std::vector<GeoNumPair>& parents=(*ite)->parentSides(); //parent of side element
      std::vector<GeoNumPair>::iterator itgn=parents.begin();
      for (; itgn!=parents.end(); ++itgn)
      {
        itl=eltext.find(itgn->first);
        if (eltext.find(itgn->first)!=eltext.end()) sidelts_u[itl->second]=*itgn;
      }
    }
  }
  if (ext_v)
  {
    std::map<GeomElement*, Element*> eltext;
    for (number_t k = 0; k < subsp_v->nbOfElements(); k++)
    {
      Element* elt=const_cast<Element*>(subsp_v->element_p(k));
      eltext[elt->geomElt_p]=elt;
    }
    std::map<GeomElement*, Element*>::iterator itl;
    std::vector<GeomElement*>::const_iterator ite=domx->meshDomain()->geomElements.begin();
    for (; ite!=domx->meshDomain()->geomElements.end(); ++ite)
    {
      std::vector<GeoNumPair>& parents=(*ite)->parentSides(); //parent of side element
      std::vector<GeoNumPair>::iterator itgn=parents.begin();
      for (; itgn!=parents.end(); ++itgn)
      {
        itl=eltext.find(itgn->first);
        if(eltext.find(itgn->first)!=eltext.end()) sidelts_v[itl->second]=*itgn;
      }
    }
  }

  //collect information from bilinear form to prepare computation
  std::set<Quadrature*> quad_x, quad_y;   //to store single quadrature pointers
  bool der_x=false, der_y=false, nor_x=false, nor_y=false;
  std::list<std::multimap<real_t, IntgMeth> > intgMaps;  //list of IntegrationMethod indexed by distance for each bilinear form
  std::list<std::multimap<real_t, IntgMeth> >::iterator itiml;
  std::multimap<real_t, IntgMeth>::iterator itim, itime, itimf;
  number_t nbf = subf.size();
  std::vector<KernelOperatorOnUnknowns*> kopregs(nbf,0), kopsings(nbf,0);   //regular and singular operators for each DoubleIntgBilinearForm
  std::vector<KernelOperatorOnUnknowns*>::iterator itsin=kopsings.begin(), itreg=kopregs.begin();

  for (cit_vbfp itf = subf.begin(); itf != subf.end(); ++itf, ++itreg, ++itsin)
  {
    const DoubleIntgBilinearForm* ibf = itf->first->asDoubleIntgForm(); //basic bilinear form as double integral

    // collect info from integration methods
    intgMaps.push_back(std::multimap<real_t, IntgMeth>());
    IntegrationMethods::const_iterator itims;
    for (itims=ibf->intgMethods.begin(); itims!=ibf->intgMethods.end(); ++itims)
    {
      intgMaps.back().insert(std::pair<real_t, IntgMeth>(itims->bound,*itims)); //insert intg method in map
      // create singular and regular KernelOperatorOnUnknowns if required by integration method
      const KernelOperatorOnUnknowns& kopus= ibf->kopus();
      if (itims->functionPart==_singularPart)
      {
        Kernel* ker = kopus.opker().kernelp()->singPart;
        if (ker == nullptr) error("null_pointer","ker->singPart");
        OperatorOnKernel opker(kopus.opker());
        opker.changeKernel(ker);
        *itsin = new KernelOperatorOnUnknowns(kopus.opu(), kopus.algopu(), opker, kopus.algopv(), kopus.opv(), kopus.priorityIsRight());
      }
      if (itims->functionPart==_regularPart)
      {
        Kernel* ker = kopus.opker().kernelp()->regPart;
        if (ker == nullptr) error("null_pointer", "ker->regPart");
        OperatorOnKernel opker(kopus.opker());
        opker.changeKernel(ker);
        *itreg = new KernelOperatorOnUnknowns(kopus.opu(), kopus.algopu(), opker, kopus.algopv(), kopus.opv(), kopus.priorityIsRight());
      }

      //collect quadratureIM's from double quadrature IM
      const IntegrationMethod* im = itims->intgMeth;              //integration method
      if (im->useQuadraturePoints() && im->type() == _productIM)    //double quadrature, collect quadratureIM's
      {
        const ProductIM* pim = static_cast<const ProductIM*>(im);
        QuadratureIM* qimx = static_cast<QuadratureIM*>(pim->getxIM());
        QuadratureIM* qimy = static_cast<QuadratureIM*>(pim->getyIM());
        std::list<Quadrature*> quads=qimx->quadratures();
        quad_x.insert(quads.begin(),quads.end());
        quads=qimy->quadratures();
        quad_y.insert(quads.begin(),quads.end());
      }
      else
      {
        std::list<Quadrature*> quads=im->quadratures();
        quad_x.insert(quads.begin(),quads.end());
        quad_y.insert(quads.begin(),quads.end());
      }
    }
    // collect derivative and normal requirements
    const KernelOperatorOnUnknowns& kopuv=ibf->kopus();
    ieparams.scalar_k = true;
    if (kopuv.opker().kernelp()!=nullptr) ieparams.scalar_k = (kopuv.opker().kernelp()->strucType()==_scalar);
    if (!der_x && kopuv.diffOrder_v() > 0) der_x=true;
    if (!der_y && kopuv.diffOrder_u() > 0) der_y=true;
    if (!nor_x && (kopuv.opker().xnormalRequired() || kopuv.opv().normalRequired())) nor_x=true;
    if (!nor_y && (kopuv.opker().ynormalRequired() || kopuv.opu().normalRequired())) nor_y=true;

    #ifdef XLIFEPP_WITH_OMP
      kopuv.opker().noUpdatedNormal=true;
    #endif // XLIFEPP_WITH_OMP
  }

  // precompute shapevalues, geometric data, ... for each element
  preComputationIE(*domy, subsp_u, quad_y, nbc_u, false, der_y, nor_y, ext_u);
  if (!same_interpolation || domx!=domy || quad_x!=quad_y || der_x!=der_y || nor_x!=nor_y || nbc_u!=nbc_v)
  {
    ieparams.useAux = subsp_u->element_p(number_t(0))->refElt_p == subsp_v->element_p(number_t(0))->refElt_p;
    preComputationIE(*domx, subsp_v,quad_x, nbc_v, ieparams.useAux, der_x, nor_x, ext_v);
  }

  //when extension of operator on kernel,to avoid a possible data race, preload Lagrange finite element used by the extension
  if (ext_opk!=nullptr)
  {
    if (ext_opk->var_==_x)
    {
      std::set<ShapeType>::const_iterator its= mdomx->shapeTypes.begin();
      for (; its!=mdomx->shapeTypes.end(); ++its)
        findRefElement(*its, findInterpolation(_Lagrange, _standard, ext_opk->order_,_H1));
    }
    if (ext_opk->var_==_y)
    {
      std::set<ShapeType>::const_iterator its= mdomy->shapeTypes.begin();
      for (; its!=mdomy->shapeTypes.end(); ++its)
        findRefElement(*its, findInterpolation(_Lagrange, _standard, ext_opk->order_,_H1));
    }
  }

  const IntegrationMethod* im= subf.begin()->first->asDoubleIntgForm()->intgMethod();  //use intgMethod of the first bilinear form
  const HMatrixIM* hmim = static_cast<const HMatrixIM*>(im);
  HMApproximationMethod hmap = hmim->hmAppMethod;
  number_t r = hmim->maxRank;
  real_t thr = hmim->threshold;
  //bool useThreshold = r==0;

  //retry list of HMatrixNodes that have to be computed
  std::list<HMatrixNode<T,I>*> matnodes=mat.getLeaves(false);

  //main loop on matrix nodes
  trackingObjects = false;  //deactivate storage of objects
  number_t nbnodes = matnodes.size();

  //print status
  number_t nbnodesdiv10 = nbnodes/10;
  bool show_status = (theVerboseLevel > 0 && mat.numberOfRows() > 100 && nbnodesdiv10 > 1);

  #ifdef XLIFEPP_WITH_OMP
    #ifdef XLIFEPP_WITH_EIGEN
      Eigen::setNbThreads(1);      //disable omp multithreading  in Eigen
    #endif // XLIFEPP_WITH_EIGEN
    #pragma omp parallel for firstprivate(ieparams) schedule(dynamic)
  #endif // XLIFEPP_WITH_OMP
  for (number_t k=0; k<nbnodes; k++)
  {
    typename std::list<HMatrixNode<T,I>*>::iterator itno=matnodes.begin();
    for (number_t j=0; j<k; j++) itno++;
    ClusterNode<I>* rowNode=(*itno)->rowNode_;      //row cluster node
    ClusterNode<I>* colNode=(*itno)->colNode_;      //col cluster node
    const std::vector<number_t>& rowDofs=rowNode->dofNumbers();
    const std::vector<number_t>& colDofs=colNode->dofNumbers();
    const std::vector<Element*>& rowElts=rowNode->elements();
    const std::vector<Element*>& colElts=colNode->elements();
    number_t m = rowDofs.size(), n = colDofs.size();

    if (!(*itno)->admissible_ || hmap==_noHMApproximation)  //exact computation of matrix node
    {
      (*itno)->mat_ = new LargeMatrix<T>(m, n, _dense, _row);
      computePartialIE(subf, *(*itno)->mat_, vt, rowDofs, colDofs, rowElts, colElts, ieparams, intgMaps,
                        kopregs, kopsings, sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
    }
    else //admissible node, approximate it
    {
      switch (hmap)
      {
        case _svdCompression:  // svd compression
        {
          LargeMatrix<T>* lm = new LargeMatrix<T>(m, n,_dense, _row);
          computePartialIE(subf, *lm, vt, rowDofs, colDofs, rowElts, colElts, ieparams, intgMaps,
                            kopregs, kopsings, sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
          LowRankMatrix<T>* lrm = new LowRankMatrix<T>();
          svd(*lm,*lrm, r, thr);
          (*itno)->appmat_=lrm;
          delete lm;
          break;
        }
        case _rsvdCompression: //Randomized Singular Value Decomposition
        {
          LargeMatrix<T>* lm = new LargeMatrix<T>(m, n,_dense, _row);
          computePartialIE(subf, *lm, vt, rowDofs, colDofs, rowElts, colElts, ieparams, intgMaps,
                            kopregs, kopsings, sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
          LowRankMatrix<T>* lrm = new LowRankMatrix<T>();
          rsvd(*lm,*lrm, r, thr);
          (*itno)->appmat_=lrm;
          delete lm;
          break;
        }
        case _r3svdCompression: //Rank Revealing Randomized Singular Value Decomposition
        {
          LargeMatrix<T>* lm = new LargeMatrix<T>(m, n,_dense, _row);
          computePartialIE(subf, *lm, vt, rowDofs, colDofs, rowElts, colElts, ieparams, intgMaps,
                            kopregs, kopsings, sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
          LowRankMatrix<T>* lrm = new LowRankMatrix<T>();
          r3svd(*lm,*lrm, thr);
          (*itno)->appmat_=lrm;
          delete lm;
          break;
        }
        case _acaFull: //ACA with full pivoting
        {
          LowRankMatrix<T>* lrm =  new LowRankMatrix<T>();
          acaFullMethod(subf, *lrm, r, thr, rowDofs, colDofs, rowElts, colElts, ieparams, vt, intgMaps, kopregs, kopsings,
                  sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
          (*itno)->appmat_ = lrm;
          break;
        }
        case _acaPartial: // ACA with partial pivoting
        {
          LowRankMatrix<T>* lrm =  new LowRankMatrix<T>();
          acaPartialMethod(subf, *lrm, r, thr, rowDofs, colDofs, rowElts, colElts, subsp_v, subsp_u, ieparams, vt,
                     intgMaps, kopregs, kopsings, sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
          (*itno)->appmat_ = lrm;
          break;
        }
        case _acaPlus: //ACA+
        {
          LowRankMatrix<T>* lrm =  new LowRankMatrix<T>();
          acaPlusMethod(subf, *lrm, r, thr, rowDofs, colDofs, rowElts, colElts, subsp_v, subsp_u, ieparams, vt, intgMaps, kopregs, kopsings,
                  sidelts_u, sidelts_v, noUpdatedNormal, same_interpolation, sym);
          (*itno)->appmat_ = lrm;
          break;
        }
        default:
          error("compression_not_handled", words("HMatrix approximation", hmap));
      }
      // LowRankMatrix<T>& lrm = reinterpret_cast<LowRankMatrix<T>&>(*(*itno)->appmat_);
      // thePrintStream<<lrm.D_<<eol;
      // std::vector<T> x((*itno)->appmat_->numberOfCols(),T(1)), y((*itno)->appmat_->numberOfRows(),T(0));
      // lrm.multMatrixVector(x,y);
      // thePrintStream<<"Ax="<<y<<eol;
    }
    if (show_status)  //progress status
    {
      if (k % nbnodesdiv10 == 0 && k!=0) { std::cout<< k/nbnodesdiv10 <<"0% "<<std::flush; }
    }

  }
  if (hmap!=_noHMApproximation) mat.nbAppMatrices = mat.nbAdmissibles;
  trackingObjects = true;
  if (theVerboseLevel > 0) std::cout<<" done"<<eol<<std::flush;
#if defined(XLIFEPP_WITH_OMP) && defined( XLIFEPP_WITH_EIGEN)
  Eigen::setNbThreads(numberOfThreads(-1));   //restore number of threads used by Eigen
#endif // XLIFEPP_WITH_EIGEN
  trace_p->pop();
}

} // end of namespace xlifepp
