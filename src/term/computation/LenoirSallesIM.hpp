/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LenoirSallesIM.hpp
  \authors S. Roman, N.Salles, E. Lunéville
  \since 06 feb 2017
  \date 06 feb 2017

  \brief Definition of the Lenoir and Salles' method to compute the BEM TermMatrix.
*/

#ifndef LENOIR_SALLES_IM_NEW_HPP_INCLUDED
#define LENOIR_SALLES_IM_NEW_HPP_INCLUDED

#include "finiteElements.h"
#include "utils.h"
#include "config.h"
#include "termUtils.hpp"
#include "form.h"

namespace xlifepp
{

//========================================================================================
/*!
  \class LenoirSalles2dIM
  integral over a product of geometric elements with singularity using Lenoir-Salles analytic technique
  restricted for the moment to P0 triangle single layer and double layer and P1 single layer
  Marc Lenoir, Nicolas Salles, Evaluation of 3-D Singular and Nearly Singular Integrals
  in Galerkin BEM for Thin Layers, SIAM Journal on Scientific Computing, vol. 36, pp. 3057-3078, 2012
*/
//========================================================================================
class LenoirSalles2dIM: public DoubleIM
{
public:
  LenoirSalles2dIM(IntegrationMethodType imt=_LenoirSalles2dIM)  //! basic constructor
    :DoubleIM(imt) {name="Lenoir-Salles_2d"; singularType=_logr; singularOrder=1;}

  virtual LenoirSalles2dIM* clone() const               //covariant
  {return new LenoirSalles2dIM(*this);}

  virtual std::list<Quadrature*> quadratures() const    //! return the list of (single) quadratures in a list
  {return std::list<Quadrature*>();}

  template<typename K>
  void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                 const KernelOperatorOnUnknowns& kuv, Matrix<K>& res,IEcomputationParameters& ieparams,
                 Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk) const;
  void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                 const KernelOperatorOnUnknowns& kuv, Matrix<real_t>& res,IEcomputationParameters& ieparams,
                 Vector<real_t>& val_opu, Vector<real_t>& val_opv, Vector<real_t>& val_opk) const
  {computeIE<>(elt_S, elt_T,adj, kuv, res,ieparams, val_opu, val_opv, val_opk);}
  void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                 const KernelOperatorOnUnknowns& kuv, Matrix<complex_t>& res,IEcomputationParameters& ieparams,
                 Vector<complex_t>& val_opu, Vector<complex_t>& val_opv, Vector<complex_t>& val_opk) const
  {computeIE<>(elt_S, elt_T,adj, kuv, res,ieparams, val_opu, val_opv, val_opk);}

  // computation stuff
  void computeLaplace2dSLP0(const Element* elt_S, const Element* elt_T, real_t& res) const;
  void computeLaplace2dDLP0(const Element* elt_S, const Element* elt_T, real_t& res) const;
  void computeLaplace2dSLP1(const Element* elt_S, const Element* elt_T, Matrix<real_t> & res) const;
  real_t selfInfluenceLaplace2dSLP0(const Point& A, const Point& B) const;
  void   selfInfluenceLaplace2dSLP1(const Point& A, const Point& B, Matrix<real_t>& res) const;
  real_t vertexLaplace2dSLP0(const Point& A, const Point& B, const Point& C) const;
  real_t vertexLaplace2dDLP0(const Point& A, const Point& B, const Point& C) const;
  real_t vertexLaplace2dDLP0b(const Point& A, const Point& B, const Point& C, Point& ny) const;
  void   vertexLaplace2dSLP1(const Point& A, const Point& B, const Point& C, const Vector<number_t>& indi,
                             const Vector<number_t>& indj, Matrix<real_t>& res) const;
  real_t vertexLaplace2dSLP1b(const Point& A, const Point& B, const Point& C, int i, int j) const;
  real_t R1(real_t y, real_t a, real_t teta) const;
  real_t R2(real_t y, real_t a, real_t teta) const;
  real_t fun1P0(const Point& A, const Point& B, const Point& M, real_t d, const Point& alpha, real_t nV) const;
  real_t fun2P0(const Point& A, const Point &B, const Point &M, real_t d, const Point& alpha, real_t nV) const;

  // print stuff
  virtual void print(std::ostream& os) const    //! print IntegrationMethod on stream
  {os<<"Lenoir-Salles 2D Integration Method ";}
  virtual void print(PrintStream& os) const {print(os.currentStream());}
};

// front-end computation function (virtual function of DoubleIM class)
// -------------------------------------------------------------------
template <typename K>
void LenoirSalles2dIM::computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                                 const KernelOperatorOnUnknowns& kuv, Matrix<K>& res, IEcomputationParameters& ieparams,
                                 Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk) const
{
  // Note: check on kernel type (Lap 2D) and shape type (segment) has to be done before
  if (ieparams.isP0) //order 0
    {
      real_t tmp = 0.0;
      if (kuv.opker().isId()) computeLaplace2dSLP0(elt_S,elt_T,tmp);
      else if (kuv.opker().ydifOpType()==_ndotgrad_y) computeLaplace2dDLP0(elt_S,elt_T,tmp);
      else error("single_and_double_layer_only");
      res[0]=K(tmp);
      return;
    }
  //order 1
  if (ieparams.ord_u==1 && ieparams.ord_v==1)
    {
      Matrix<real_t> tmp(dimen_t(2),dimen_t(2), 0.);
      if (kuv.opker().isId()) computeLaplace2dSLP1(elt_S,elt_T,tmp);
      //else if(kuv.opker().ydifOpType()==_ndotgrad_y) computeLaplace2dDLP1(elt_S,elt_T,tmp);
      else error("single_layer_only");
      res=tmp;
      return;
    }
  error("interp_order_out_of_range", 0, 1);
}

//========================================================================================
/*!
  \class LenoirSalles3dIM
  integral over a product of geometric elements with singularity using Lenoir-Salles analytic technique
  restricted for the moment to P0 triangle and single layer
  Marc Lenoir, Nicolas Salles, Evaluation of 3-D Singular and Nearly Singular Integrals
  in Galerkin BEM for Thin Layers, SIAM Journal on Scientific Computing, vol. 36, pp. 3057-3078, 2012
*/
//========================================================================================
class LenoirSalles3dIM: public DoubleIM
{
public:
  LenoirSalles3dIM(IntegrationMethodType imt=_LenoirSalles3dIM)  //! basic constructor
    :DoubleIM(imt) {name="Lenoir-Salles_3d"; singularType=_r_; singularOrder=-1;}

  virtual LenoirSalles3dIM* clone() const               //covariant
  {return new LenoirSalles3dIM(*this);}

  virtual std::list<Quadrature*> quadratures() const    //! return the list of (single) quadratures in a list
  {return std::list<Quadrature*>();}

  template<typename K>
  void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                 const KernelOperatorOnUnknowns& kuv, Matrix<K>& res,IEcomputationParameters& ieparams,
                 Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk) const;
  void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                 const KernelOperatorOnUnknowns& kuv, Matrix<real_t>& res,IEcomputationParameters& ieparams,
                 Vector<real_t>& val_opu, Vector<real_t>& val_opv, Vector<real_t>& val_opk) const
  {computeIE<>(elt_S, elt_T,adj, kuv, res,ieparams, val_opu, val_opv, val_opk);}
  void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                 const KernelOperatorOnUnknowns& kuv, Matrix<complex_t>& res,IEcomputationParameters& ieparams,
                 Vector<complex_t>& val_opu, Vector<complex_t>& val_opv, Vector<complex_t>& val_opk) const
  {computeIE<>(elt_S, elt_T,adj, kuv, res,ieparams, val_opu, val_opv, val_opk);}

  // computation stuff
  void computeLaplace3dSLP0(const Element* elt_S, const Element* elt_T, real_t& res) const;
  real_t selfInfluenceLaplace3dSLP0(const Point & S1, const Point & S2, const Point & S3) const;
  real_t adjacentTrianglesByEdgeLaplace3dSLP0(const Point & S3, const Point & S1, const Point & S2, const Point& T3) const;
  real_t adjacentTrianglesByEdgeCoplanarLaplace3dSLP0(const Point & S3,const Point & S1,const Point & S2,const Point & T3) const;
  real_t adjacentTrianglesByVertexCoplanarLaplace3dSLP0(const Point & S1,const Point & S2,const Point & S3, const Point & T2, const Point & T3) const;
  real_t nonAdjacentTrianglesCoplanarLaplace3dSLP0(const Point & S1,const Point & S2,const Point & S3,const Point & T1, const Point & T2, const Point & T3) const;
  real_t adjacentTrianglesByEdgeSecantPlanesLaplace3dSLP0(const Point & S3,const Point & S1,const Point & S2,const Point & T3) const;
  real_t adjacentTrianglesByVertexSecantPlanesLaplace3dSLP0(const Point & S1,const Point & S2,const Point & S3, const Point & T2, const Point & T3) const;
  real_t nonAdjacentTrianglesSecantPlanesLaplace3dSLP0(const Point & S1,const Point & S2,const Point & S3,const Point & T1, const Point & T2, const Point & T3) const;
  real_t nonAdjacentTrianglesParallelPlanesLaplace3dSLP0(const Point & S1,const Point & S2,const Point & S3,const Point & T1, const Point & T2, const Point & T3) const;
  real_t I0_Coplanar_R(const  Point& I, const  Point& A, const  Point& B, real_t h) const;
  real_t I0_Coplanar_Rp(const Point& I, const Point& Am, const Point& Ap, real_t h) const;
  real_t I0_Coplanar_P(const Point& I, const Point& A, const Point& B, const Point& C) const;
  real_t I0_Coplanar_Q(const Point & Am, const Point & Ap, const Point & Bm, const Point & Bp) const;
  real_t I0_SecantPlanes_R(const Point& B, const Point& Am, const Point& Ap, const real_t h) const;
  real_t I0_SecantPlanes_Rp(const Point& B, const Point& Am, const Point& Ap, const real_t h) const;
  real_t I0_SecantPlanes_Pp(const Point& A, const Point& S1, const Point& S2, const Point& S3, const real_t h) const;
  real_t I0_SecantPlanes_Rpp(const Point& B, const Point& Am, const Point& Ap, const real_t h, const real_t d) const;
  real_t I0_SecantPlanes_P(const Point& B, const Point& S1, const Point& S2, const Point& S3) const;
  real_t I0_SecantPlanes_Q(const Point& Am, const Point& Ap, const Point& Bm, const Point& Bp) const;
  real_t I0_SecantPlanes_Qp(const Point& Am, const Point& Ap, const Point& Bm, const Point& Bp, const real_t h) const;
  real_t I0_SecantPlanes_U(const Point& S1, const Point& S2, const Point& S3,const Point & T1, const Point & T2, const Point & T3,const Point& I1,const Point& I2) const;
  real_t I0_ParallelPlanes_R(const Point & M, const Point & Tm, const Point & Tp, real_t h) const;
  real_t I0_ParallelPlanes_P(const Point & M, const Point & T1, const Point & T2, const Point & T3, real_t h) const;
  real_t I0_ParallelPlanes_Rp(const Point & M, const Point & Tm, const Point & Tp, real_t h, real_t D) const;
  real_t I0_ParallelPlanes_Q(const Point & Am, const Point & Ap, const Point & Bm, const Point & Bp, real_t h) const;
  real_t I0_ParallelPlanes_U(const Point & Am, const Point & Ap, const Point & T1, const Point & T2, const Point & T3, real_t h) const;
  real_t I0_ParallelPlanes_0S(const Point & S1, const Point & S2, const Point & S3, const Point & T1,const Point & T2,const Point & T3, real_t h) const;
  real_t I0_ParallelPlanes_1S(const Point & S1, const Point & S2, const Point & S3, const Point & T2,const Point & T3, real_t h) const;
  real_t I0_ParallelPlanes_2S(const Point & S1, const Point & S2, const Point & S3, const Point & T3, real_t h) const;
  real_t I0_ParallelPlanes_3S(const Point & S1, const Point & S2, const Point & S3, real_t h) const;
  int testDistanceBetweenTriangles(const Point & S1,const Point & S2, const Point & S3,const Point & T1,const Point & T2,const Point & T3) const;
  void loadElement(const Element* elt_S, Point& S1, Point& S2, Point& S3, Vector<number_t>& indi) const;

  // print stuff
  virtual void print(std::ostream& os) const    //! print IntegrationMethod on stream
  {os<<"Lenoir-Salles 3d Integration Method ";}
  virtual void print(PrintStream& os) const {print(os.currentStream());}
};

// front-end computation function (virtual function of DoubleIM class)
// -------------------------------------------------------------------
template <typename K>
void LenoirSalles3dIM::computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                                 const KernelOperatorOnUnknowns& kuv, Matrix<K>& res, IEcomputationParameters& ieparams,
                                 Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk) const
{
  // Note: check on kernel type (Lap3D) and shape type (triangle) has to be done before
  if (ieparams.isP0) //order 0
    {
      real_t tmp = 0.0;
      if(kuv.opker().isId()) computeLaplace3dSLP0(elt_S, elt_T, tmp);
      //else if(kuv.opker().ydifOpType()==_ndotgrad_y) computeLaplace3dDLP0(elt_S,elt_T,tmp);
      else error("single_layer_only");
      res[0]=K(tmp);
      return;
    }
  //order 1
  // if (ieparams.ord_u==1 && ieparams.ord_v==1)
  // {
  //   Matrix<real_t> tmp(dimen_t(3),dimen_t(3), 0.);
  //   if (kuv.opker().isId()) computeLaplace3dSLP1(elt_S, elt_T, tmp);
  //   else if(kuv.opker().ydifOpType()==_ndotgrad_y) computeLaplace3dDLP1(elt_S,elt_T,tmp);
  //   else error("free_error","Lenoir-Salles not yet designed for P1 approximation");
  //   res=tmp;
  // }
  error("interp_order_unexpected", 0, std::max(ieparams.ord_u,ieparams.ord_v));
}


//========================================================================================
/*!
  \class LenoirSalles2dIR
  integral over a geometric element with singularity using Lenoir-Salles analytic technique
  restricted for the moment to P0/P1 segment and single/double layer potential
*/
//========================================================================================
class LenoirSalles2dIR: public SingleIM
{
public:
  LenoirSalles2dIR(IntegrationMethodType imt=_LenoirSalles2dIR)  //! basic constructor
    :SingleIM(imt) {name="Lenoir-Salles_2d_IR"; singularType=_logr; singularOrder=1; requireNormal=true;}

  virtual LenoirSalles2dIR* clone() const               //covariant
  {return new LenoirSalles2dIR(*this);}

  template<typename K>
  void computeIR(const Element* elt, const Point& p, const OperatorOnUnknown& op, number_t ord,
                 const Vector<real_t>* nx, const Vector<real_t>* ny, Vector<K>& res, dimen_t& d) const;
  void computeIR(const Element* elt, const Point& p, const OperatorOnUnknown& op, number_t ord,
                 const Vector<real_t>* nx, const Vector<real_t>* ny, Vector<real_t>& res, dimen_t& d) const
  {computeIR<>(elt, p, op, ord, nx, ny, res, d);}
  void computeIR(const Element* elt, const Point& p, const OperatorOnUnknown& op, number_t ord,
                 const Vector<real_t>* nx, const Vector<real_t>* ny, Vector<complex_t>& res, dimen_t& d) const
  {computeIR<>(elt, p, op, ord, nx, ny, res, d);}

  //compute stuff
  void geometricalStuff(const Element*, const Point&, const Vector<real_t>*, real_t&, real_t&, real_t&, real_t&) const;
  void computeLaplace2dSLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res) const;
  void computeLaplace2dSLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const;
  void computeLaplace2dDLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res)const;
  void computeLaplace2dDLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const;

  void computeLaplace2dDTSLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res) const;
  void computeLaplace2dDTSLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const;
  void computeLaplace2dDTDLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res)const;
  void computeLaplace2dDTDLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const;

  void computeLaplace2dDNSLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res) const;
  void computeLaplace2dDNSLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const;
  void computeLaplace2dDNDLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res)const;
  void computeLaplace2dDNDLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const;

  void computeLaplace2dGradSLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const;
  void computeLaplace2dGradSLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const;
  void computeLaplace2dGradDLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const;
  void computeLaplace2dGradDLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const;
};

/*! main computation function: intg_elt op(p,y,wi(y))dy for i=1,nbdofs
    ord: order of the interpolation (shortcut P0)
    nx, ny: normal vector pointers (given non zero if required by operator)
    d: dimension of the result (d=1 for scalar computation), updated here
*/
template<typename K>
void LenoirSalles2dIR::computeIR(const Element* elt, const Point& p, const OperatorOnUnknown& op, number_t ord,
                                 const Vector<real_t>* nx, const Vector<real_t>* ny, Vector<K>& res, dimen_t& d) const
{
  // Note: check on kernel type (Lap 2D) and shape type (segment) has to be done before
  d=1; //scalar result
  if (ord==0) //order 0
    {
      real_t tmp=0.;
      Vector<real_t> vtmp;
      switch (op.opkernelp()->ydifOpType())
        {
        case _id:
          switch (op.opkernelp()->xdifOpType())
            {
            case _id: computeLaplace2dSLP0(elt, p, ny, tmp); res[0]=tmp; return;
            case _grad:
            case _grad_x: computeLaplace2dGradSLP0(elt, p, ny, vtmp); res=-vtmp; d=2; return;
            default:  break;
            }
          break;
        case _ndotgrad_y:
          switch (op.opkernelp()->xdifOpType())
            {
            case _id: computeLaplace2dDLP0(elt, p, ny, tmp); res[0]=tmp; return;
            case _grad:
            case _grad_x: computeLaplace2dGradDLP0(elt, p, ny, vtmp); res=-vtmp; d=2; return;
            default:  break;
            }
          break;
        case _grad_y:
          switch (op.opkernelp()->xdifOpType())
            {
            case _id: computeLaplace2dGradSLP0(elt, p, nx, vtmp); res=vtmp; d=2; return;
            default:  break;
            }
          break;
        default:  break;
        }
      error("free_error","in LenoirSalles2dIR::computeIR, operator "+op.asString()+" not supported");
    }

  //order 1
  if (ord==1)
    {
      Vector<real_t> vtmp;
      switch (op.opkernelp()->ydifOpType())
        {
        case _id:
          switch (op.opkernelp()->xdifOpType())
            {
            case _id: computeLaplace2dSLP1(elt, p, ny, vtmp); res=vtmp; return;
            case _grad:
            case _grad_x: computeLaplace2dGradSLP1(elt, p, ny, vtmp); res=-vtmp; d=2; return;
            default: break;
            }
          break;
        case _ndotgrad_y:
          switch (op.opkernelp()->xdifOpType())
            {
            case _id: computeLaplace2dDLP1(elt, p, ny, vtmp); res=vtmp; return;
            case _grad:
            case _grad_x: computeLaplace2dGradDLP1(elt, p, ny, vtmp); res=-vtmp; d=2; return;
            default: break;
            }
          break;
        case _grad_y:
          switch (op.opkernelp()->xdifOpType())
            {
            case _id: computeLaplace2dGradSLP1(elt, p, ny, vtmp); res=vtmp; d=2; return;
            default: break;
            }
          break;
        default: break;
        }
      error("free_error","in LenoirSalles2dIR::computeIR, operator "+op.asString()+" not supported");
    }
  error("interp_order_out_of_range", 0, 1);
}

//========================================================================================
/*!
  \class LenoirSalles3dIR
  integral over a geometric element with singularity using Lenoir-Salles analytic technique
  restricted for the moment to P0/P1 triangle and single/double layer potential
*/
//========================================================================================

class LenoirSalles3dIR: public SingleIM
{
public:
  LenoirSalles3dIR(IntegrationMethodType imt=_LenoirSalles3dIR)  //! basic constructor
    :SingleIM(imt) {name="Lenoir-Salles_3d_IR"; singularType=_r_; singularOrder=-1;}

  virtual LenoirSalles3dIR* clone() const               //covariant
  {return new LenoirSalles3dIR(*this);}

  template<typename K>
  void computeIR(const Element* elt, const Point& p, const OperatorOnUnknown& op, number_t ord,
                 const Vector<real_t>* nx, const Vector<real_t>* ny, Vector<K>& res, dimen_t& d) const;
  void computeIR(const Element* elt, const Point& p, const OperatorOnUnknown& op, number_t ord,
                 const Vector<real_t>* nx, const Vector<real_t>* ny, Vector<real_t>& res, dimen_t& d) const
  {computeIR<>(elt, p, op, ord, nx, ny, res, d);}
  void computeIR(const Element* elt, const Point& p, const OperatorOnUnknown& op, number_t ord,
                 const Vector<real_t>* nx, const Vector<real_t>* ny, Vector<complex_t>& res, dimen_t& d) const
  {computeIR<>(elt, p, op, ord, nx, ny, res, d);}

  //computing stuff
  void computeLaplace3dSLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res) const;
  void computeLaplace3dDLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res)const;
  void computeLaplace3dSLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const;
  void computeLaplace3dDLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const;
  real_t integrandLapSLP0(const Point& Sm, const Point& Sp, real_t h, real_t d, const Point& Ip, real_t alpha=1.) const;
  real_t integrandLapDLP0(const Point& Sm, const Point& Sp, real_t h, real_t d, const Point& Ip) const;
  real_t integrandLapSLP1const(const Point& Sm, const Point& Sp, real_t h,real_t d, const Point& Ip, real_t alpha=1.) const;
  real_t integrandLapDLP1const(const Point& Sm, const Point& Sp, real_t h,real_t d, const Point& Ip) const;
  void integrandLapSLP1lin(const Point & Sm, const Point & Sp, real_t h, real_t d, const Point& Ip, Vector<real_t> & res,
                           real_t alpha=1.) const;
  void integrandLapDLP1lin(const Point & Sm, const Point & Sp, real_t h, real_t d, const Point& Ip, Vector<real_t> & res) const;
  void geometricalStuff(const Point& S1, const Point& S2, const Point& S3, const Vector<real_t>& normalT, const Point& X,
                        std::vector<Point>& I, real_t& h, bool I3) const;
};

/*! main computation function: intg_elt op(p,y,wi(y))dy for i=1,nbdofs
    ord: order of the interpolation (shortcut P0)
    nx, ny: normal vector pointers (given non zero if required by operator)
    d: dimension of the result (d=1 for scalar computation), updated here
*/
template<typename K>
void LenoirSalles3dIR::computeIR(const Element* elt, const Point& p, const OperatorOnUnknown& op, number_t ord,
                                 const Vector<real_t>* nx, const Vector<real_t>* ny, Vector<K>& res, dimen_t& d) const
{
  // Note: kernel type (Lap 3D) and shape type (triangle) has to be checked before calling computation
  d=1; //scalar result
  if (ord==0) //order 0
    {
      real_t tmp=0.;
      if (op.opkernelp()->isId()) computeLaplace3dSLP0(elt, p, ny, tmp);
      else if (op.opkernelp()->ydifOpType()==_ndotgrad_y) computeLaplace3dDLP0(elt, p, ny, tmp);
      else error("single_and_double_layer_only");
      res[0]=tmp;
      return;
    }
  //order 1
  if (ord==1)
    {
      Vector<real_t> tmp(3, 0.);
      if (op.opkernelp()->isId()) computeLaplace3dSLP1(elt, p, ny, tmp);
      else if (op.opkernelp()->ydifOpType()==_ndotgrad_y) computeLaplace3dDLP1(elt, p, ny, tmp);
      else error("single_and_double_layer_only");
      res=tmp;
      return;
    }
  error("interp_order_out_of_range", 0, 1);
}

//special sign function
inline real_t signe(real_t x)
{
  if(x>0) return  1.;
  if(x<0) return -1.;
  return 0.;
}

}
#endif // LENOIR_SALLES_IM_NEW_HPP_INCLUDED
