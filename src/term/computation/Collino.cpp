/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Collino.cpp
  \authors F. Collino, E. Lunéville
  \since 7 sep 2018
  \date  7 sep 2018

  \brief Collino's stuff adapted to XLiFE++
*/

#include "Collino.hpp"
#include "utils.h"
#include <string.h>

#ifndef DOXYGEN_SHOULD_SKIP_THIS

namespace xlifepp
{
void free_quad(Myquad_t* Q)
{
  free(Q->w);
  free(Q->a);
  free(Q->b);
  free(Q->ws);
  free(Q->as);
  free(Q->bs);
  free(Q->wo);
  free(Q->ao);
  //free(Q);
  return;
}

Myquad_t* elem_quad(number_t ngtr, number_t ngts, number_t ngso, real_t eta, number_t demcem_nt, number_t demcem_ne, number_t demcem_nv, bool use_demcem)
{
  Myquad_t* Q;
  mymalloc(Q, number_t(1), Myquad_t);

  /* integration on a triangle (far interact=> regular))... */

  Q->ng = ngtr;
  mymalloc(Q->w, Q->ng, real_t);
  mymalloc(Q->a, Q->ng, real_t);
  mymalloc(Q->b, Q->ng, real_t);
  ElemTools_stroud_c(Q->ng, Q->a, Q->b, Q->w);


  /*  integration on a triangle  (close interactions)... */
  Q->ngs = ngts;
  if( Q->ngs <= 0) {Q->ngs=1;}
  mymalloc(Q->ws, Q->ngs, real_t);
  mymalloc(Q->as, Q->ngs, real_t);
  mymalloc(Q->bs, Q->ngs, real_t);
  ElemTools_stroud_c(Q->ngs, Q->as, Q->bs, Q->ws);

  /* integration on a segment... */
  Q->ngo = ngso;
  if( Q->ngo <= 0) {Q->ngo=1;}
  mymalloc(Q->wo, Q->ngo, real_t);
  mymalloc(Q->ao, Q->ngo, real_t);
  ElemTools_legset(Q->ngo, Q->ao, Q->wo);

  Q->use_demcem = use_demcem;
  if( Q->use_demcem ==1 )
    {
      /* choice demcem for touching elements... */
      Q->np_demcem_e = demcem_ne ;
      Q->np_demcem_v = demcem_nv ;
      Q->np_demcem_t = demcem_nt ;
    }
  else
    {
      /* non used data...*/
      Q->np_demcem_e = 0 ;
      Q->np_demcem_v = 0 ;
      Q->np_demcem_t = 0 ;
    }
  /* const that allows the separation regular singular...*/
  Q->eta = eta;
  return(Q) ;
}

void ElemTools_stroud_c(number_t n, real_t* Qa, real_t* Qb, real_t* Qw)
{
  real_t a, b, c, w;
  real_t weight1[8]; real_t weight2[8];
  real_t xtab1[8];   real_t xtab2[8];
  number_t k = 0;

  switch (n)
    {
    case 1:
      Qw[0]=1.0   ; Qa[0]=1.0/3 ; Qb[0]=1.0/3 ;
      break;
    case 3:
      a = 2.0/3; b = 1.0/6; w = 1.0/3;
      Qa[0] = a; Qb[0] = b; Qw[0] = w;
      Qa[1] = b; Qb[1] = a; Qw[1] = w;
      Qa[2] = b; Qb[2] = b; Qw[2] = w;
      break;
    case 7:
      a = 1.0/3.0 ; b = 1.0/3.0; c = 1.0/3.0; w = 0.225;
      Qa[0] = a; Qb[0] = b; Qw[0] = w;
      real_t phi, sigma ;
      phi = std::sqrt ( 15. );  sigma = std::sqrt (7.);
      a = ( 9.0 + 2.0 * phi )/21.0;
      b = ( 6.0 -       phi )/21.0;
      c = ( 6.0 -       phi )/21.0;
      w = ( 155.0 - std::sqrt ( 15.0 ) )/1200.0;
      Qa[1] = a; Qb[1] = b; Qw[1] = w;
      Qa[2] = b; Qb[2] = a; Qw[2] = w;
      Qa[3] = b; Qb[3] = b; Qw[3] = w;
      a = ( 9.0 - 2.0 * phi )/21.0;
      b = ( 6.0 +       phi )/21.0;
      c = ( 6.0 +       phi )/21.0;
      w = ( 155.0 + std::sqrt ( 15.0 ) )/1200.0;
      Qa[4] = a; Qb[4] = b; Qw[4] = w;
      Qa[5] = b; Qb[5] = a; Qw[5] = w;
      Qa[6] = b; Qb[6] = b; Qw[6] = w;
      break;
    case 12:
      a = 0.873821971016996;
      b = 0.063089014491502;
      c = 0.063089014491502;
      w = 0.050844906370207;
      Qa[0] = a; Qb[0] = b; Qw[0] =  w;
      Qa[1] = b; Qb[1] = a; Qw[1] =  w;
      Qa[2] = b; Qb[2] = b; Qw[2] =  w;
      a = 0.501426509658179;
      b = 0.249286745170910;
      c = 0.249286745170910;
      w = 0.116786275726379;
      Qa[3] = b; Qb[3] = a; Qw[3] = w;
      Qa[4] = a; Qb[4] = b; Qw[4] = w;
      Qa[5] = b; Qb[5] = b; Qw[5] = w;
      a = 0.636502499121399;
      b = 0.310352451033785;
      c = 0.053145049844816;
      w = 0.082851075618374;
      Qa[6]  = a; Qb[6] = b; Qw[6]  = w;
      Qa[7]  = a; Qb[7] = c; Qw[7]  = w;
      Qa[8]  = b; Qb[8] = a; Qw[8]  = w;
      Qa[9]  = b; Qb[9] = c; Qw[9]  = w;
      Qa[10] = c; Qb[10] = a; Qw[10] = w;
      Qa[11] = c; Qb[11] = b; Qw[11] = w;
      break;
    case(37):
      a = 0.333333333333333333333333333333;
      b = 0.333333333333333333333333333333;
      c = 1.0 - a - b;
      w = 0.051739766065744133555179145422;
      Qa[0] = a; Qb[0] = b; Qw[0] = w;
      a = 0.950275662924105565450352089520;
      b = 0.024862168537947217274823955239;
      c = 1.0 - a - b;
      w = 0.008007799555564801597804123460;
      Qa[1] = a; Qb[1] = b; Qw[1] = w;
      Qa[2] = b; Qb[2] = a; Qw[2] = w;
      Qa[3] = b; Qb[3] = b; Qw[3] = w;
      a = 0.171614914923835347556304795551;
      b = 0.414192542538082326221847602214;
      c = 1.0 - a - b;
      w = 0.046868898981821644823226732071;
      Qa[4] = a; Qb[4] = b; Qw[4] = w;
      Qa[5] = b; Qb[5] = a; Qw[5] = w;
      Qa[6] = b; Qb[6] = b; Qw[6] = w;
      a = 0.539412243677190440263092985511;
      b = 0.230293878161404779868453507244;
      c = 1.0 - a - b;
      w = 0.046590940183976487960361770070;
      Qa[7]  = a;  Qb[7] =  b; Qw[7] = w;
      Qa[8]  = b;  Qb[8] =  a; Qw[8] = w;
      Qa[9] =  b;  Qb[9] =  b; Qw[9] = w;
      a = 0.772160036676532561750285570113;
      b = 0.113919981661733719124857214943;
      c = 1.0 - a - b;
      w = 0.031016943313796381407646220131;
      Qa[10] = a; Qb[10] = b; Qw[10] = w;
      Qa[11] = b; Qb[11] = a; Qw[11] = w;
      Qa[12] = b; Qb[12] = b; Qw[12] = w;
      a = 0.009085399949835353883572964740;
      b = 0.495457300025082323058213517632;
      c = 1.0 - a - b;
      w = 0.010791612736631273623178240136;
      Qa[13] = a; Qb[13] = b; Qw[13] = w;
      Qa[14] = b; Qb[14] = a; Qw[14] = w;
      Qa[15] = b; Qb[15] = b; Qw[15] = w;
      a = 0.062277290305886993497083640527;
      b = 0.468861354847056503251458179727;
      c = 1.0 - a - b;
      w = 0.032195534242431618819414482205;
      Qa[16] = a; Qb[16] = b; Qw[16] = w;
      Qa[17] = b; Qb[17] = a; Qw[17] = w;
      Qa[18] = b; Qb[18] = b; Qw[18] = w;
      a = 0.022076289653624405142446876931;
      b = 0.851306504174348550389457672223;
      c = 1.0 - a - b;
      w = 0.015445834210701583817692900053;
      Qa[19] = a; Qb[19] = b; Qw[19] = w;
      Qa[20] = a; Qb[20] = c; Qw[20] = w;
      Qa[21] = b; Qb[21] = a; Qw[21] = w;
      Qa[22] = b; Qb[22] = c; Qw[22] = w;
      Qa[23] = c; Qb[23] = a; Qw[23] = w;
      Qa[24] = c; Qb[24] = b; Qw[24] = w;
      a = 0.018620522802520968955913511549;
      b = 0.689441970728591295496647976487;
      c = 1.0 - a - b;
      w = 0.017822989923178661888748319485;
      Qa[25] = a; Qb[25] = b; Qw[25] = w;
      Qa[26] = a; Qb[26] = c; Qw[26] = w;
      Qa[27] = b; Qb[27] = a; Qw[27] = w;
      Qa[28] = b; Qb[28] = c; Qw[28] = w;
      Qa[29] = c; Qb[29] = a; Qw[29] = w;
      Qa[30] = c; Qb[30] = b; Qw[30] = w;
      a = 0.096506481292159228736516560903;
      b = 0.635867859433872768286976979827;
      c = 1.0 - a - b;
      w = 0.037038683681384627918546472190;
      Qa[31] = a; Qb[31] = b; Qw[31] = w;
      Qa[32] = a; Qb[32] = c; Qw[32] = w;
      Qa[33] = b; Qb[33] = a; Qw[33] = w;
      Qa[34] = b; Qb[34] = c; Qw[34] = w;
      Qa[35] = c; Qb[35] = a; Qw[35] = w;
      Qa[36] = c; Qb[36] = b; Qw[36] = w;
      break;
    case 64:
      weight2[0] = 0.00329519144;
      weight2[1] = 0.01784290266;
      weight2[2] = 0.04543931950;
      weight2[3] = 0.07919959949;
      weight2[4] = 0.10604735944;
      weight2[5] = 0.11250579947;
      weight2[6] = 0.09111902364;
      weight2[7] = 0.04455080436;
      xtab2[0] = 0.04463395529;
      xtab2[1] = 0.14436625704;
      xtab2[2] = 0.28682475714;
      xtab2[3] = 0.45481331520;
      xtab2[4] = 0.62806783542;
      xtab2[5] = 0.78569152060;
      xtab2[6] = 0.90867639210;
      xtab2[7] = 0.98222008485;
      xtab1[0] = -0.960289856497536231683560868569;
      xtab1[1] = -0.796666477413626739591553936476;
      xtab1[2] = -0.525532409916328985817739049189;
      xtab1[3] = -0.183434642495649804939476142360;
      xtab1[4] =  0.183434642495649804939476142360;
      xtab1[5] =  0.525532409916328985817739049189;
      xtab1[6] =  0.796666477413626739591553936476;
      xtab1[7] =  0.960289856497536231683560868569;
      weight1[0] = 0.101228536290376259152531354310;
      weight1[1] = 0.222381034453374470544355994426;
      weight1[2] = 0.313706645877887287337962201987;
      weight1[3] = 0.362683783378361982965150449277;
      weight1[4] = 0.362683783378361982965150449277;
      weight1[5] = 0.313706645877887287337962201987;
      weight1[6] = 0.222381034453374470544355994426;
      weight1[7] = 0.101228536290376259152531354310;
      for(number_t j=0; j < 8; j++)
        {
          for(number_t i=0; i< 8; i++, k++)
            {
              Qa[k] = 1.0 - xtab2[j];
              Qb[k] = 0.5 * ( 1.0 + xtab1[i] ) * xtab2[j];
              Qw[k] = weight1[i] * weight2[j];
            };
        };
      break;
    default:
      printf(" number of quadrature points required %lu not allowed \n",n);
      printf(" 1, 3, 7, 12, 37, 64 are possible \n");
      printf(" Emergency exit \n");
      exit(ERR); break;
    }
}

void ElemTools_legset (number_t norder, real_t *xtab, real_t *weight )
/*
    LEGSET sets abscissas and weights for Gauss-Legendre quadrature.
    Integration interval: [ -1, 1 ]
    Weight function: 1
    Integral to approximate: INTEGRAL ( -1 <=  X <= 1 ) F(X) dX.
    Approximate integral: SUM ( I = 1 to NORDER ) WEIGHT(I) * F ( XTAB(I) ).
    Precision: The quadrature rule will integrate exactly all polynomials up to X**(2*NORDER-1).
    The abscissas of the rule are the zeroes of the Legendre polynomial P(NORDER)(X).
        The integral produced by a Gauss-Legendre rule is equal to the
        integral of the unique polynomial of degree NORDER-1 which
        agrees with the function at the NORDER abscissas of the rule.
    !
    !    Input, integer norder, the order of the rule.
    !    norder must be between 1 and 16, 20, 32 or 64.
    !    Output, real_t XTAB(NORDER), the abscissas of the rule.
    !
    !    Output, real_t WEIGHT(NORDER), the weights of the rule.
*/
{
  switch (norder)
    {
    case 1:
      xtab[0] = 0.0e0;
      weight[0] = 2.0e0;
      break;
    case 2:
      xtab[0] = -0.577350269189625e0;
      xtab[1] =  0.577350269189625e0;
      weight[0] = 1.0e0;
      weight[1] = 1.0e0;
      break;
    case 3:
      xtab[0] = -0.774596669241483e0;
      xtab[1] =  0.0e0;
      xtab[2] =  0.7745966692414833e0;
      weight[0] = 5.0e0/ 9.0e0;
      weight[1] = 8.0e0/ 9.0e0;
      weight[2] = 5.0e0/ 9.0e0;
      break;
    case 4:
      xtab[0] = -0.86113631159405257e0;
      xtab[1] = -0.33998104358485626e0;
      xtab[2] =  0.33998104358485626e0;
      xtab[3] =  0.86113631159405257e0;
      weight[0] = 0.347854845137453e0;
      weight[1] = 0.652145154862546e0;
      weight[2] = 0.652145154862546e0;
      weight[3] = 0.347854845137453e0;
      break;
    case 5:
      xtab[0] = -0.906179845938663992e0;
      xtab[1] = -0.538469310105683091e0;
      xtab[2] =  0.0e0;
      xtab[3] =  0.538469310105683091e0;
      xtab[4] =  0.906179845938663992e0;
      weight[0] = 0.2369268850561890e0;
      weight[1] = 0.4786286704993664e0;
      weight[2] = 0.568888888888888888e0;
      weight[3] = 0.478628670499366468e0;
      weight[4] = 0.23692688505618908e0;
      break;
    case 6:
      xtab[0] = -0.932469514203152027e0;
      xtab[1] = -0.66120938646626451e0;
      xtab[2] = -0.23861918608319690e0;
      xtab[3] =  0.23861918608319690e0;
      xtab[4] =  0.66120938646626451e0;
      xtab[5] =  0.93246951420315202e0;
      weight[0] = 0.1713244923791703e0;
      weight[1] = 0.3607615730481386e0;
      weight[2] = 0.46791393457269104e0;
      weight[3] = 0.46791393457269104e0;
      weight[4] = 0.36076157304813860e0;
      weight[5] = 0.1713244923791703e0;
      break;
    case 7:
      xtab[0] = -0.94910791234275852e0;
      xtab[1] = -0.74153118559939443e0;
      xtab[2] = -0.40584515137739716e0;
      xtab[3] =  0.0e0;
      xtab[4] =  0.40584515137739716e0;
      xtab[5] =  0.74153118559939443e0;
      xtab[6] =  0.94910791234275852e0;
      weight[0] = 0.1294849661688696e0;
      weight[1] = 0.2797053914892766e0;
      weight[2] = 0.3818300505051189e0;
      weight[3] = 0.4179591836734693e0;
      weight[4] = 0.3818300505051189e0;
      weight[5] = 0.2797053914892766e0;
      weight[6] = 0.1294849661688696e0;
      break;
    case 8:
      xtab[0] = -0.960289856497536231e0;
      xtab[1] = -0.796666477413626739e0;
      xtab[2] = -0.525532409916328985e0;
      xtab[3] = -0.183434642495649804e0;
      xtab[4] =  0.183434642495649804e0;
      xtab[5] =  0.525532409916328985e0;
      xtab[6] =  0.796666477413626739e0;
      xtab[7] =  0.960289856497536231e0;
      weight[0] = 0.10122853629037625e0;
      weight[1] = 0.22238103445337447e0;
      weight[2] = 0.31370664587788728e0;
      weight[3] = 0.36268378337836198e0;
      weight[4] = 0.36268378337836198e0;
      weight[5] = 0.31370664587788728e0;
      weight[6] = 0.22238103445337447e0;
      weight[7] = 0.10122853629037625e0;
      break;
    case 9:
      xtab[1-1] = -0.968160239507626089e0;
      xtab[2-1] = -0.836031107326635794e0;
      xtab[3-1] = -0.613371432700590397e0;
      xtab[4-1] = -0.324253423403808929e0;
      xtab[5-1] =  0.0e0;
      xtab[6-1] =  0.324253423403808929e0;
      xtab[7-1] =  0.613371432700590397e0;
      xtab[8-1] =  0.836031107326635794e0;
      xtab[9-1] =  0.968160239507626089e0;
      weight[1-1] = 0.08127438836157441e0;
      weight[2-1] = 0.18064816069485740e0;
      weight[3-1] = 0.26061069640293546e0;
      weight[4-1] = 0.31234707704000284e0;
      weight[5-1] = 0.33023935500125976e0;
      weight[6-1] = 0.31234707704000284e0;
      weight[7-1] = 0.26061069640293546e0;
      weight[8-1] = 0.18064816069485740e0;
      weight[9-1] = 0.08127438836157441e0;
      break;
    case 10:
      xtab[1-1] = -0.973906528517171720e0;
      xtab[2-1] = -0.865063366688984510e0;
      xtab[3-1] = -0.679409568299024406e0;
      xtab[4-1] = -0.433395394129247290e0;
      xtab[5-1] = -0.148874338981631210e0;
      xtab[6-1] =  0.148874338981631210e0;
      xtab[7-1] =  0.433395394129247290e0;
      xtab[8-1] =  0.679409568299024406e0;
      xtab[9-1] =  0.865063366688984510e0;
      xtab[10-1] = 0.973906528517171720e0;
      weight[1-1] =  0.06667134430868813e0;
      weight[2-1] =  0.14945134915058059e0;
      weight[3-1] =  0.21908636251598204e0;
      weight[4-1] =  0.26926671930999635e0;
      weight[5-1] =  0.29552422471475287e0;
      weight[6-1] =  0.29552422471475287e0;
      weight[7-1] =  0.26926671930999635e0;
      weight[8-1] =  0.21908636251598204e0;
      weight[9-1] =  0.14945134915058059e0;
      weight[10-1] = 0.06667134430868813e0;
      break;
    case 11:
      xtab[1-1] = -0.97822865814605699e0;
      xtab[2-1] = -0.88706259976809529e0;
      xtab[3-1] = -0.73015200557404932e0;
      xtab[4-1] = -0.51909612920681181e0;
      xtab[5-1] = -0.26954315595234497e0;
      xtab[6-1] =  0.0e0;
      xtab[7-1] =  0.2695431559523449e0;
      xtab[8-1] =  0.5190961292068118e0;
      xtab[9-1] =  0.7301520055740493e0;
      xtab[10-1] = 0.8870625997680952e0;
      xtab[11-1] = 0.9782286581460569e0;
      weight[1-1] =  0.0556685671161736e0;
      weight[2-1] =  0.1255803694649046e0;
      weight[3-1] =  0.1862902109277342e0;
      weight[4-1] =  0.2331937645919904e0;
      weight[5-1] =  0.2628045445102466e0;
      weight[6-1] =  0.2729250867779006e0;
      weight[7-1] =  0.2628045445102466e0;
      weight[8-1] =  0.2331937645919904e0;
      weight[9-1] =  0.18629021092773425e0;
      weight[10-1] = 0.1255803694649046e0;
      weight[11-1] = 0.0556685671161736e0;
      break;
    case 12:
      xtab[1-1] = -0.9815606342467192e0;
      xtab[2-1] = -0.9041172563704748e0;
      xtab[3-1] = -0.7699026741943046e0;
      xtab[4-1] = -0.5873179542866174e0;
      xtab[5-1] = -0.3678314989981801e0;
      xtab[6-1] = -0.1252334085114689e0;
      xtab[7-1] =  0.1252334085114689e0;
      xtab[8-1] =  0.3678314989981801e0;
      xtab[9-1] =  0.5873179542866174e0;
      xtab[10-1] = 0.7699026741943046e0;
      xtab[11-1] = 0.9041172563704748e0;
      xtab[12-1] = 0.9815606342467192e0;
      weight[1-1] =  0.04717533638651182e0;
      weight[2-1] =  0.10693932599531843e0;
      weight[3-1] =  0.16007832854334622e0;
      weight[4-1] =  0.20316742672306592e0;
      weight[5-1] =  0.23349253653835480e0;
      weight[6-1] =  0.24914704581340278e0;
      weight[7-1] =  0.24914704581340278e0;
      weight[8-1] =  0.23349253653835480e0;
      weight[9-1] =  0.20316742672306592e0;
      weight[10-1] = 0.16007832854334622e0;
      weight[11-1] = 0.10693932599531843e0;
      weight[12-1] = 0.04717533638651182e0;
      break;
    case 13:
      xtab[1-1] =  -0.9841830547185881e0;
      xtab[2-1] =  -0.9175983992229779e0;
      xtab[3-1] =  -0.8015780907333099e0;
      xtab[4-1] =  -0.6423493394403402e0;
      xtab[5-1] =  -0.4484927510364468e0;
      xtab[6-1] =  -0.2304583159551347e0;
      xtab[7-1] =   0.0e0;
      xtab[8-1] =   0.2304583159551347e0;
      xtab[9-1] =   0.4484927510364468e0;
      xtab[10-1] =  0.6423493394403402e0;
      xtab[11-1] =  0.8015780907333099e0;
      xtab[12-1] =  0.9175983992229779e0;
      xtab[13-1] =  0.9841830547185881e0;
      weight[1-1] =  0.040484004765315e0;
      weight[2-1] =  0.092121499837728e0;
      weight[3-1] =  0.138873510219787e0;
      weight[4-1] =  0.178145980761945e0;
      weight[5-1] =  0.207816047536888e0;
      weight[6-1] =  0.226283180262897e0;
      weight[7-1] =  0.232551553230873e0;
      weight[8-1] =  0.226283180262897e0;
      weight[9-1] =  0.207816047536888e0;
      weight[10-1] = 0.178145980761945e0;
      weight[11-1] = 0.138873510219787e0;
      weight[12-1] = 0.092121499837728e0;
      weight[13-1] = 0.040484004765315e0;
      break;
    case 14:
      xtab[1-1] =  -0.9862838086968123e0;
      xtab[2-1] =  -0.9284348836635735e0;
      xtab[3-1] =  -0.8272013150697649e0;
      xtab[4-1] =  -0.6872929048116854e0;
      xtab[5-1] =  -0.5152486363581540e0;
      xtab[6-1] =  -0.3191123689278897e0;
      xtab[7-1] =  -0.1080549487073436e0;
      xtab[8-1] =   0.1080549487073436e0;
      xtab[9-1] =   0.3191123689278897e0;
      xtab[10-1] =  0.5152486363581540e0;
      xtab[11-1] =  0.6872929048116854e0;
      xtab[12-1] =  0.8272013150697649e0;
      xtab[13-1] =  0.9284348836635735e0;
      xtab[14-1] =  0.9862838086968123e0;
      weight[1-1] =  0.03511946033175e0;
      weight[2-1] =  0.08015808715976e0;
      weight[3-1] =  0.12151857068790e0;
      weight[4-1] =  0.15720316715819e0;
      weight[5-1] =  0.18553839747793e0;
      weight[6-1] =  0.20519846372129e0;
      weight[7-1] =  0.21526385346315e0;
      weight[8-1] =  0.21526385346315e0;
      weight[9-1] =  0.20519846372129e0;
      weight[10-1] = 0.18553839747793e0;
      weight[11-1] = 0.15720316715819e0;
      weight[12-1] = 0.12151857068790e0;
      weight[13-1] = 0.08015808715976e0;
      weight[14-1] = 0.03511946033175e0;
      break;
    case 15:
      xtab[1-1] =  -0.9879925180204854e0;
      xtab[2-1] =  -0.9372733924007059e0;
      xtab[3-1] =  -0.8482065834104272e0;
      xtab[4-1] =  -0.7244177313601700e0;
      xtab[5-1] =  -0.5709721726085388e0;
      xtab[6-1] =  -0.3941513470775634e0;
      xtab[7-1] =  -0.2011940939974345e0;
      xtab[8-1] =   0.0e0;
      xtab[9-1] =   0.2011940939974345e0;
      xtab[10-1] =  0.3941513470775634e0;
      xtab[11-1] =  0.5709721726085388e0;
      xtab[12-1] =  0.7244177313601700e0;
      xtab[13-1] =  0.8482065834104272e0;
      xtab[14-1] =  0.9372733924007059e0;
      xtab[15-1] =  0.9879925180204854e0;
      weight[1-1] =  0.03075324199611727e0;
      weight[2-1] =  0.07036604748810812e0;
      weight[3-1] =  0.1071592204671719e0;
      weight[4-1] =  0.1395706779261543e0;
      weight[5-1] =  0.1662692058169939e0;
      weight[6-1] =  0.1861610000155622e0;
      weight[7-1] =  0.1984314853271116e0;
      weight[8-1] =  0.2025782419255613e0;
      weight[9-1] =  0.1984314853271116e0;
      weight[10-1] = 0.1861610000155622e0;
      weight[11-1] = 0.1662692058169939e0;
      weight[12-1] = 0.1395706779261543e0;
      weight[13-1] = 0.1071592204671719e0;
      weight[14-1] = 0.0703660474881081e0;
      weight[15-1] = 0.0307532419961172e0;
      break;
    case 16:
      xtab[1-1] = -0.98940093499164993e0;
      xtab[2-1] = -0.94457502307323257e0;
      xtab[3-1] = -0.86563120238783174e0;
      xtab[4-1] = -0.75540440835500303e0;
      xtab[5-1] = -0.61787624440264374e0;
      xtab[6-1] = -0.45801677765722738e0;
      xtab[7-1] = -0.28160355077925891e0;
      xtab[8-1] = -0.09501250983763744e0;
      xtab[9-1] =  0.095012509837637440e0;
      xtab[10-1] = 0.281603550779258913e0;
      xtab[11-1] = 0.458016777657227386e0;
      xtab[12-1] = 0.617876244402643748e0;
      xtab[13-1] = 0.755404408355003033e0;
      xtab[14-1] = 0.865631202387831743e0;
      xtab[15-1] = 0.944575023073232576e0;
      xtab[16-1] = 0.989400934991649932e0;
      weight[1-1] =  0.02715245941175409e0;
      weight[2-1] =  0.06225352393864789e0;
      weight[3-1] =  0.09515851168249278e0;
      weight[4-1] =  0.12462897125553387e0;
      weight[5-1] =  0.14959598881657673e0;
      weight[6-1] =  0.16915651939500253e0;
      weight[7-1] =  0.18260341504492358e0;
      weight[8-1] =  0.18945061045506849e0;
      weight[9-1] =  0.18945061045506849e0;
      weight[10-1] = 0.18260341504492358e0;
      weight[11-1] = 0.16915651939500253e0;
      weight[12-1] = 0.14959598881657673e0;
      weight[13-1] = 0.12462897125553387e0;
      weight[14-1] = 0.09515851168249278e0;
      weight[15-1] = 0.06225352393864789e0;
      weight[16-1] = 0.02715245941175409e0;
      break;
    case 20:
      xtab[1-1] =  -0.9931285991850949e0;
      xtab[2-1] =  -0.9639719272779138e0;
      xtab[3-1] =  -0.9122344282513259e0;
      xtab[4-1] =  -0.8391169718222188e0;
      xtab[5-1] =  -0.7463319064601508e0;
      xtab[6-1] =  -0.6360536807265150e0;
      xtab[7-1] =  -0.5108670019508271e0;
      xtab[8-1] =  -0.3737060887154196e0;
      xtab[9-1] =  -0.2277858511416451e0;
      xtab[10-1] = -0.0765265211334973e0;
      xtab[11-1] =  0.0765265211334973e0;
      xtab[12-1] =  0.2277858511416451e0;
      xtab[13-1] =  0.3737060887154196e0;
      xtab[14-1] =  0.5108670019508271e0;
      xtab[15-1] =  0.6360536807265150e0;
      xtab[16-1] =  0.7463319064601508e0;
      xtab[17-1] =  0.8391169718222188e0;
      xtab[18-1] =  0.9122344282513259e0;
      xtab[19-1] =  0.9639719272779138e0;
      xtab[20-1] =  0.9931285991850949e0;
      weight[1-1] =   0.01761400713915212e0;
      weight[2-1] =   0.04060142980038694e0;
      weight[3-1] =   0.06267204833410906e0;
      weight[4-1] =   0.08327674157670475e0;
      weight[5-1] =   0.1019301198172404e0;
      weight[6-1] =   0.1181945319615184e0;
      weight[7-1] =   0.1316886384491766e0;
      weight[8-1] =   0.1420961093183821e0;
      weight[9-1] =   0.1491729864726037e0;
      weight[10-1] =  0.1527533871307259e0;
      weight[11-1] =  0.1527533871307259e0;
      weight[12-1] =  0.1491729864726037e0;
      weight[13-1] =  0.1420961093183821e0;
      weight[14-1] =  0.1316886384491766e0;
      weight[15-1] =  0.1181945319615184e0;
      weight[16-1] =  0.1019301198172404e0;
      weight[17-1] =  0.08327674157670475e0;
      weight[18-1] =  0.0626720483341090e0;
      weight[19-1] =  0.0406014298003869e0;
      weight[20-1] =  0.01761400713915212e0;

      break;
    case 32:
      xtab[1-1] =  -0.997263861849482e0;
      xtab[2-1] =  -0.985611511545268e0;
      xtab[3-1] =  -0.964762255587506e0;
      xtab[4-1] =  -0.934906075937740e0;
      xtab[5-1] =  -0.896321155766052e0;
      xtab[6-1] =  -0.849367613732570e0;
      xtab[7-1] =  -0.794483795967942e0;
      xtab[8-1] =  -0.732182118740290e0;
      xtab[9-1] =  -0.663044266930215e0;
      xtab[10-1] = -0.587715757240762e0;
      xtab[11-1] = -0.506899908932229e0;
      xtab[12-1] = -0.421351276130635e0;
      xtab[13-1] = -0.331868602282128e0;
      xtab[14-1] = -0.239287362252137e0;
      xtab[15-1] = -0.144471961582796e0;
      xtab[16-1] = -0.0483076656877383e0;
      xtab[17-1] =  0.0483076656877383e0;
      xtab[18-1] =  0.144471961582796e0;
      xtab[19-1] =  0.239287362252137e0;
      xtab[20-1] =  0.331868602282128e0;
      xtab[21-1] =  0.421351276130635e0;
      xtab[22-1] =  0.506899908932229e0;
      xtab[23-1] =  0.587715757240762e0;
      xtab[24-1] =  0.663044266930215e0;
      xtab[25-1] =  0.732182118740290e0;
      xtab[26-1] =  0.794483795967942e0;
      xtab[27-1] =  0.849367613732570e0;
      xtab[28-1] =  0.896321155766052e0;
      xtab[29-1] =  0.934906075937740e0;
      xtab[30-1] =  0.964762255587506e0;
      xtab[31-1] =  0.985611511545268e0;
      xtab[32-1] =  0.997263861849482e0;
      weight[1-1] =  0.00701861000947010e0;
      weight[2-1] =  0.0162743947309057e0;
      weight[3-1] =  0.0253920653092621e0;
      weight[4-1] =  0.0342738629130214e0;
      weight[5-1] =  0.0428358980222267e0;
      weight[6-1] =  0.0509980592623762e0;
      weight[7-1] =  0.0586840934785355e0;
      weight[8-1] =  0.0658222227763618e0;
      weight[9-1] =  0.0723457941088485e0;
      weight[10-1] = 0.0781938957870703e0;
      weight[11-1] = 0.0833119242269468e0;
      weight[12-1] = 0.0876520930044038e0;
      weight[13-1] = 0.0911738786957639e0;
      weight[14-1] = 0.0938443990808046e0;
      weight[15-1] = 0.0956387200792749e0;
      weight[16-1] = 0.0965400885147278e0;
      weight[17-1] = 0.0965400885147278e0;
      weight[18-1] = 0.0956387200792749e0;
      weight[19-1] = 0.0938443990808046e0;
      weight[20-1] = 0.0911738786957639e0;
      weight[21-1] = 0.0876520930044038e0;
      weight[22-1] = 0.0833119242269468e0;
      weight[23-1] = 0.0781938957870703e0;
      weight[24-1] = 0.0723457941088485e0;
      weight[25-1] = 0.0658222227763618e0;
      weight[26-1] = 0.0586840934785355e0;
      weight[27-1] = 0.0509980592623762e0;
      weight[28-1] = 0.0428358980222267e0;
      weight[29-1] = 0.0342738629130214e0;
      weight[30-1] = 0.0253920653092621e0;
      weight[31-1] = 0.0162743947309057e0;
      weight[32-1] = 0.00701861000947010e0;
      break;
    case  64:
      xtab[1-1] =  -0.999305041735772e0;
      xtab[2-1] =  -0.996340116771955e0;
      xtab[3-1] =  -0.991013371476744e0;
      xtab[4-1] =  -0.983336253884626e0;
      xtab[5-1] =  -0.973326827789911e0;
      xtab[6-1] =  -0.961008799652054e0;
      xtab[7-1] =  -0.946411374858403e0;
      xtab[8-1] =  -0.929569172131940e0;
      xtab[9-1] =  -0.910522137078503e0;
      xtab[10-1] = -0.889315445995114e0;
      xtab[11-1] = -0.865999398154093e0;
      xtab[12-1] = -0.840629296252580e0;
      xtab[13-1] = -0.813265315122798e0;
      xtab[14-1] = -0.783972358943341e0;
      xtab[15-1] = -0.752819907260532e0;
      xtab[16-1] = -0.719881850171611e0;
      xtab[17-1] = -0.685236313054233e0;
      xtab[18-1] = -0.648965471254657e0;
      xtab[19-1] = -0.611155355172393e0;
      xtab[20-1] = -0.571895646202634e0;
      xtab[21-1] = -0.531279464019895e0;
      xtab[22-1] = -0.489403145707053e0;
      xtab[23-1] = -0.446366017253464e0;
      xtab[24-1] = -0.402270157963992e0;
      xtab[25-1] = -0.357220158337668e0;
      xtab[26-1] = -0.311322871990211e0;
      xtab[27-1] = -0.264687162208767e0;
      xtab[28-1] = -0.217423643740007e0;
      xtab[29-1] = -0.169644420423993e0;
      xtab[30-1] = -0.121462819296121e0;
      xtab[31-1] = -0.0729931217877990e0;
      xtab[32-1] = -0.0243502926634244e0;
      xtab[33-1] =  0.0243502926634244e0;
      xtab[34-1] =  0.0729931217877990e0;
      xtab[35-1] =  0.121462819296121e0;
      xtab[36-1] =  0.169644420423993e0;
      xtab[37-1] =  0.217423643740007e0;
      xtab[38-1] =  0.264687162208767e0;
      xtab[39-1] =  0.311322871990211e0;
      xtab[40-1] =  0.357220158337668e0;
      xtab[41-1] =  0.402270157963992e0;
      xtab[42-1] =  0.446366017253464e0;
      xtab[43-1] =  0.489403145707053e0;
      xtab[44-1] =  0.531279464019895e0;
      xtab[45-1] =  0.571895646202634e0;
      xtab[46-1] =  0.611155355172393e0;
      xtab[47-1] =  0.648965471254657e0;
      xtab[48-1] =  0.685236313054233e0;
      xtab[49-1] =  0.719881850171611e0;
      xtab[50-1] =  0.752819907260532e0;
      xtab[51-1] =  0.783972358943341e0;
      xtab[52-1] =  0.813265315122798e0;
      xtab[53-1] =  0.840629296252580e0;
      xtab[54-1] =  0.865999398154093e0;
      xtab[55-1] =  0.889315445995114e0;
      xtab[56-1] =  0.910522137078503e0;
      xtab[57-1] =  0.929569172131940e0;
      xtab[58-1] =  0.946411374858403e0;
      xtab[59-1] =  0.961008799652054e0;
      xtab[60-1] =  0.973326827789911e0;
      xtab[61-1] =  0.983336253884626e0;
      xtab[62-1] =  0.991013371476744e0;
      xtab[63-1] =  0.996340116771955e0;
      xtab[64-1] =  0.999305041735772e0;
      weight[1-1] =  0.00178328072169643e0;
      weight[2-1] =  0.00414703326056247e0;
      weight[3-1] =  0.00650445796897836e0;
      weight[4-1] =  0.00884675982636395e0;
      weight[5-1] =  0.0111681394601311e0;
      weight[6-1] =  0.0134630478967186e0;
      weight[7-1] =  0.0157260304760247e0;
      weight[8-1] =  0.0179517157756973e0;
      weight[9-1] =  0.0201348231535302e0;
      weight[10-1] = 0.0222701738083833e0;
      weight[11-1] = 0.0243527025687109e0;
      weight[12-1] = 0.0263774697150547e0;
      weight[13-1] = 0.0283396726142595e0;
      weight[14-1] = 0.0302346570724025e0;
      weight[15-1] = 0.0320579283548516e0;
      weight[16-1] = 0.0338051618371416e0;
      weight[17-1] = 0.0354722132568824e0;
      weight[18-1] = 0.0370551285402400e0;
      weight[19-1] = 0.0385501531786156e0;
      weight[20-1] = 0.0399537411327203e0;
      weight[21-1] = 0.0412625632426235e0;
      weight[22-1] = 0.0424735151236536e0;
      weight[23-1] = 0.0435837245293235e0;
      weight[24-1] = 0.0445905581637566e0;
      weight[25-1] = 0.0454916279274181e0;
      weight[26-1] = 0.0462847965813144e0;
      weight[27-1] = 0.0469681828162100e0;
      weight[28-1] = 0.0475401657148303e0;
      weight[29-1] = 0.0479993885964583e0;
      weight[30-1] = 0.0483447622348030e0;
      weight[31-1] = 0.0485754674415034e0;
      weight[32-1] = 0.0486909570091397e0;
      weight[33-1] = 0.0486909570091397e0;
      weight[34-1] = 0.0485754674415034e0;
      weight[35-1] = 0.0483447622348030e0;
      weight[36-1] = 0.0479993885964583e0;
      weight[37-1] = 0.0475401657148303e0;
      weight[38-1] = 0.0469681828162100e0;
      weight[39-1] = 0.0462847965813144e0;
      weight[40-1] = 0.0454916279274181e0;
      weight[41-1] = 0.0445905581637566e0;
      weight[42-1] = 0.0435837245293235e0;
      weight[43-1] = 0.0424735151236536e0;
      weight[44-1] = 0.0412625632426235e0;
      weight[45-1] = 0.0399537411327203e0;
      weight[46-1] = 0.0385501531786156e0;
      weight[47-1] = 0.0370551285402400e0;
      weight[48-1] = 0.0354722132568824e0;
      weight[49-1] = 0.0338051618371416e0;
      weight[50-1] = 0.0320579283548516e0;
      weight[51-1] = 0.0302346570724025e0;
      weight[52-1] = 0.0283396726142595e0;
      weight[53-1] = 0.0263774697150547e0;
      weight[54-1] = 0.0243527025687109e0;
      weight[55-1] = 0.0222701738083833e0;
      weight[56-1] = 0.0201348231535302e0;
      weight[57-1] = 0.0179517157756973e0;
      weight[58-1] = 0.0157260304760247e0;
      weight[59-1] = 0.0134630478967186e0;
      weight[60-1] = 0.0111681394601311e0;
      weight[61-1] = 0.00884675982636395e0;
      weight[62-1] = 0.00650445796897836e0;
      weight[63-1] = 0.00414703326056247e0;
      weight[64-1] = 0.00178328072169643e0;
      break;
    default:
      printf(" 'LEGSET - Fatal error! \n");
      printf(" Illegal value of NORDER = %lu \n", norder);
      printf("  Legal values are 1 to 16, 20, 32 or 64 \n ");
      exit(ERR);
    };
  for(number_t i=0; i<norder; i++)
    {
      xtab[i]   = (1- xtab[i])/2;
      weight[i] =  weight[i]/2;
    };
}

void ElemTools_strong_Gauss_kareal(real_t ck[3][3], real_t cl[3][3], real_t ka, Myquad_t* quad, complex_t zt[3][3],complex_t zk[3][3], number_t cf)
{
  complex_t gkl, g0kl, c;
  real_t u[3],v[3],uv[3],t[3], x[3],y[3] ;
  real_t r, r2, kar, cosinus, sinus, cst;
  number_t num[3];
  number_t gk, gl, nk, nl, dk, dl;
  /*---------------------------------------*/
  /* num[dof] = opposite vertex number of the edge associated to the dof*/
  num[0] = 2;
  num[1] = 0;
  num[2] = 1;
  /* cst = 1/(16pi)=  1/4pi x 1/2 x 1/2  */
  cst = 0.0198943678864869169711104704215642961;
  cst = 1/(16*std::acos(-1.));
  c   =-4/(ka*ka);
  number_t i1, j1;
  for( i1=0; i1 < 3; i1++)
    {
      for( j1=0; j1 < 3; j1++)
        {
          zk[i1][j1]=0;
          zt[i1][j1]=0;
        }
    }
  for( gk=0; gk < quad->ng; gk++)
    {
      x[0]=     ck[0][0]                   +
                quad->a[gk]*( ck[1][0]-  ck[0][0]) +
                quad->b[gk]*( ck[2][0]-  ck[0][0]);
      x[1]=     ck[0][1]                   +
                quad->a[gk]*( ck[1][1]-  ck[0][1]) +
                quad->b[gk]*( ck[2][1]-  ck[0][1]);
      x[2]=     ck[0][2]                   +
                quad->a[gk]*( ck[1][2]-  ck[0][2]) +
                quad->b[gk]*( ck[2][2]-  ck[0][2]);

      for( gl=0; gl < quad->ng; gl++)
        {
          y[0]=     cl[0][0]                   +
                    quad->a[gl]*( cl[1][0]-  cl[0][0]) +
                    quad->b[gl]*( cl[2][0]-  cl[0][0]);
          y[1]=     cl[0][1]                   +
                    quad->a[gl]*( cl[1][1]-  cl[0][1]) +
                    quad->b[gl]*( cl[2][1]-  cl[0][1]);
          y[2]=     cl[0][2]                   +
                    quad->a[gl]*( cl[1][2]-  cl[0][2]) +
                    quad->b[gl]*( cl[2][2]-  cl[0][2]);
          t[0]    = y[0]-x[0];
          t[1]    = y[1]-x[1];
          t[2]    = y[2]-x[2];
          r2      = t[0]*t[0]+ t[1]*t[1]+ t[2]*t[2];
          r       = std::sqrt(r2);
          kar     = ka*r;
          cosinus =   std::cos(kar);
          sinus   =   std::sin(kar);
          cosinus =   cosinus*(quad->w[gk]*quad->w[gl]*cst);
          sinus   =     sinus*(quad->w[gk]*quad->w[gl]*cst);
          g0kl    =   (ka/r)*cosinus+i_*(ka/r)*sinus;
          cosinus =   cosinus/r2;
          sinus   =     sinus/r2;
          gkl     =   cosinus+i_*sinus;
          gkl     =   gkl*(-1/r + i_*ka);
          {
            for( dk=0; dk < 3; dk++)
              {
                nk= num[dk];
                u[0] = x[0]-ck[nk][0];
                u[1] = x[1]-ck[nk][1];
                u[2] = x[2]-ck[nk][2];
                for( dl=0; dl < 3; dl++)
                  {
                    nl= num[dl];
                    v[0] = y[0]-cl[nl][0];
                    v[1] = y[1]-cl[nl][1];
                    v[2] = y[2]-cl[nl][2];
                    uv[0]= v[1]*u[2]-v[2]*u[1];
                    uv[1]= v[2]*u[0]-v[0]*u[2];
                    uv[2]= v[0]*u[1]-v[1]*u[0];
                    if(cf>=2) zk[dk][dl] += (t[0]*uv[0]+t[1]*uv[1]+t[2]*uv[2])*gkl;
                    if(cf==1 || cf==3) zt[dk][dl] += ((u[0]*v[0]+u[1]*v[1]+u[2]*v[2])+c)*g0kl;
                  }
              }
          }
        }
    }
}

void ElemTools_strong_Gauss_kacmplx(real_t ck[3][3],   real_t cl[3][3], real_t ka[2], Myquad_t* quad,
                                    complex_t zt[3][3],complex_t zk[3][3], number_t cf)
{
  complex_t gkl, g0kl, c;
  real_t u[3],v[3],uv[3],t[3], x[3],y[3] ;
  real_t r, r2, cst;
  complex_t zka, zkar, zexpo;
  number_t   num[3];
  number_t  gk, gl,nk, nl, dk, dl;

  /*---------------------------------------*/
  /* num[dof] =   opposite vertex number of the edge associated to the dof*/
  num[0] = 2;
  num[1] = 0;
  num[2] = 1;
  // cst = 1/(16pi)=  1/4pi x 1/2 x 1/2
//  cst = 0.0198943678864869169711104704215642961;
//  cst = 1/(16*std::acos(-1.));
  cst =over4pi_/4;
  zka = ka[0] + i_*ka[1];
  c   =-4./(zka*zka);
  number_t i1, j1;
  for( i1=0; i1 < 3; i1++)
    {
      for( j1=0; j1 < 3; j1++)
        {
          zk[i1][j1]=0;
          zt[i1][j1]=0;
        }
    }
  for( gk=0; gk < quad->ng; gk++)
    {
      x[0]=     ck[0][0]                   +
                quad->a[gk]*( ck[1][0]-  ck[0][0]) +
                quad->b[gk]*( ck[2][0]-  ck[0][0]);
      x[1]=     ck[0][1]                   +
                quad->a[gk]*( ck[1][1]-  ck[0][1]) +
                quad->b[gk]*( ck[2][1]-  ck[0][1]);
      x[2]=     ck[0][2]                   +
                quad->a[gk]*( ck[1][2]-  ck[0][2]) +
                quad->b[gk]*( ck[2][2]-  ck[0][2]);

      for( gl=0; gl < quad->ng; gl++)
        {
          y[0]=     cl[0][0]                   +
                    quad->a[gl]*( cl[1][0]-  cl[0][0]) +
                    quad->b[gl]*( cl[2][0]-  cl[0][0]);
          y[1]=     cl[0][1]                   +
                    quad->a[gl]*( cl[1][1]-  cl[0][1]) +
                    quad->b[gl]*( cl[2][1]-  cl[0][1]);
          y[2]=     cl[0][2]                   +
                    quad->a[gl]*( cl[1][2]-  cl[0][2]) +
                    quad->b[gl]*( cl[2][2]-  cl[0][2]);
          t[0]    = y[0]-x[0];
          t[1]    = y[1]-x[1];
          t[2]    = y[2]-x[2];
          r2      = t[0]*t[0]+ t[1]*t[1]+ t[2]*t[2];
          r       = std::sqrt(r2);
          zkar    = zka*r;
          /*printf(" kar %e \n",kar);*/
          zexpo   =   std::exp(i_*zkar)*(quad->w[gk]*quad->w[gl]*cst);
          g0kl    =   (zka/r)*zexpo;
          gkl     =   zexpo/r2 ;
          gkl     =   gkl*(-1/r + i_*zka);
          /*printf(" zkar %e %e \n",zkar);
            printf(" expo %e %e \n",creal(zexpo),cimag(zexpo));
            printf(" gkl %e %e \n",creal(gkl),cimag(gkl));
            printf(" g0kl %e %e \n",creal(g0kl),cimag(g0kl));
            exit(ERR);*/
          for( dk=0; dk < 3; dk++)
            {
              nk= num[dk];
              u[0] = x[0]-ck[nk][0];
              u[1] = x[1]-ck[nk][1];
              u[2] = x[2]-ck[nk][2];
              for( dl=0; dl < 3; dl++)
                {
                  nl= num[dl];
                  v[0] = y[0]-cl[nl][0];
                  v[1] = y[1]-cl[nl][1];
                  v[2] = y[2]-cl[nl][2];
                  uv[0]= v[1]*u[2]-v[2]*u[1];
                  uv[1]= v[2]*u[0]-v[0]*u[2];
                  uv[2]= v[0]*u[1]-v[1]*u[0];
                  if(cf>=2) zk[dk][dl] += (t[0]*uv[0]+t[1]*uv[1]+t[2]*uv[2])*gkl;
                  if(cf==1 || cf==3) zt[dk][dl] += ((u[0]*v[0]+u[1]*v[1]+u[2]*v[2])+c)*g0kl;
                }
            }
        }
    }
}

void ElemTools_strong_sinGauss_kareal(real_t ck[3][3], real_t cl[3][3], real_t ka, Myquad_t* quad, real_t zksin[3][3])
{
  real_t gkl, g0kl;
  real_t   u[3],v[3],uv[3],t[3], x[3],y[3] ;
  real_t r, r2, kar, cosinus, sinus, cst;
  number_t   num[3];
  number_t  gk, gl,nk, nl, dk, dl;
  /*---------------------------------------*/
  /* num[dof] =  opposite vertex number of the edge associated to the dof*/
  num[0] = 2;
  num[1] = 0;
  num[2] = 1;
  /* cst = k^3/(16pi)=  1/4pi x 1/2 x 1/2  */
  /*cst = 0.0198943678864869169711104704215642961;*/
  cst = ka*ka*ka/(16*std::acos(-1.));
  number_t i1, j1;
  for( i1=0; i1 < 3; i1++)
    {
      for( j1=0; j1 < 3; j1++)
        {
          zksin[i1][j1]=0;
        }
    }
  for( gk=0; gk < quad->ng; gk++)
    {
      x[0]=     ck[0][0]                   +
                quad->a[gk]*( ck[1][0]-  ck[0][0]) +
                quad->b[gk]*( ck[2][0]-  ck[0][0]);
      x[1]=     ck[0][1]                   +
                quad->a[gk]*( ck[1][1]-  ck[0][1]) +
                quad->b[gk]*( ck[2][1]-  ck[0][1]);
      x[2]=     ck[0][2]                   +
                quad->a[gk]*( ck[1][2]-  ck[0][2]) +
                quad->b[gk]*( ck[2][2]-  ck[0][2]);

      for( gl=0; gl < quad->ng; gl++)
        {
          y[0]=     cl[0][0]                   +
                    quad->a[gl]*( cl[1][0]-  cl[0][0]) +
                    quad->b[gl]*( cl[2][0]-  cl[0][0]);
          y[1]=     cl[0][1]                   +
                    quad->a[gl]*( cl[1][1]-  cl[0][1]) +
                    quad->b[gl]*( cl[2][1]-  cl[0][1]);
          y[2]=     cl[0][2]                   +
                    quad->a[gl]*( cl[1][2]-  cl[0][2]) +
                    quad->b[gl]*( cl[2][2]-  cl[0][2]);

          t[0]    = y[0]-x[0];
          t[1]    = y[1]-x[1];
          t[2]    = y[2]-x[2];
          r2      = t[0]*t[0]+ t[1]*t[1]+ t[2]*t[2];
          r       = std::sqrt(r2);
          kar     = ka*r;
          /*printf(" kar %e \n",kar);*/

          cosinus =   std::cos(kar);
          sinus   =   std::sin(kar);
          /*g0kl    =   (ka/r)*cosinus+I*(ka/r)*sinus;*/
          /*  Je ne calcule que la partie régulière  */
          if (  kar <=  0.0055 )
            {   gkl = -(1- 0.1e0*kar*kar )/3;}/* precision 3. 10^{-12} */
          else
            {   gkl =  (-sinus/kar + cosinus)/(kar*kar) ;}
          gkl *= (quad->w[gk]*quad->w[gl]*cst);
          /*printf(" kar %e gkl %e \n",kar, gkl);*/
          for( dk=0; dk < 3; dk++)
            {
              nk= num[dk];
              u[0] = x[0]-ck[nk][0];
              u[1] = x[1]-ck[nk][1];
              u[2] = x[2]-ck[nk][2];
              for( dl=0; dl < 3; dl++)
                {
                  nl= num[dl];
                  v[0] = y[0]-cl[nl][0];
                  v[1] = y[1]-cl[nl][1];
                  v[2] = y[2]-cl[nl][2];
                  uv[0]= v[1]*u[2]-v[2]*u[1];
                  uv[1]= v[2]*u[0]-v[0]*u[2];
                  uv[2]= v[0]*u[1]-v[1]*u[0];
                  zksin[dk][dl] += (t[0]*uv[0]+t[1]*uv[1]+t[2]*uv[2])*gkl;
                  /*printf(" zkin %e \n",zksin[dk][dl]);*/
                  /*zt[dk][dl]+=((u[0]*v[0]+u[1]*v[1]+u[2]*v[2])+c)*g0kl;*/
                }
            }
        }
    }
}

void Print_matelemo(real_t zi[3][3], complex_t zt[3][3], complex_t z[3][3])
{
  for( number_t i=0; i<3; i++)
    {
      for(number_t j=0; j<3; j++)
        {
          printf("   %3lu %3lu               %15.9e %15.9e  \n", i,j, real(z[i][j]), imag(z[i][j]) );
          printf("   %3lu %3lu               %15.9e %15.9e  \n", i,j, real(zt[j][i]), imag(zt[j][i]) );
          printf("   %3lu %3lu               %15.9e %15.9e  \n", i,j, 0.,   zi[i][j]  );
          printf(" \n");
        }
    }
}


void ElemTools_strong_getgoodresult( real_t zksin[3][3], complex_t zkt[3][3], complex_t zk[3][3])
{
  real_t e, et, s;
  number_t i,j;
  // Print_matelemo(zksin, zkt, zk); }

  s=0; et=0; e=0;
  for( i=0; i<3; i++)
    {
      for( j=0; j<3; j++)
        {
          s  += std::abs( zksin[i][j] );
          e  += std::abs( imag( zk[i][j]) - zksin[i][j] );
          et += std::abs( imag(zkt[j][i]) - zksin[i][j] );
        }
    }
  if( s!=0 )
    {
      e =  e/s;
      et= et/s;
      //printf("  s %3e e %3e  et %3e  e-et %e     \n",s,e,et,e-et);
      if( et < e)
        {
          for( i=0; i<3; i++)
            {
              for( j=0; j<3; j++)
                {
                  zk[i][j]= zkt[j][i];
                }
            }
        }
    }

  /* exit(0);*/
}

void print_tri(real_t ck[3][3], real_t cl[3][3])
{
  printf("triangle k sommet 0: %e %e %e \n", ck[0][0], ck[0][1], ck[0][2]);
  printf("triangle k sommet 1: %e %e %e \n", ck[1][0], ck[1][1], ck[1][2]);
  printf("triangle k sommet 2: %e %e %e \n", ck[2][0], ck[2][1], ck[2][2]);
  printf("triangle l sommet 0: %e %e %e \n", cl[0][0], cl[0][1], cl[0][2]);
  printf("triangle l sommet 1: %e %e %e \n", cl[1][0], cl[1][1], cl[1][2]);
  printf("triangle l sommet 2: %e %e %e \n", cl[2][0], cl[2][1], cl[2][2]);
}


int_t ElemTools_strong_ncv(real_t ck[3][3], real_t cl[3][3], number_t Numero[3], real_t eta )
{
  /* si   Numero(i)=0 => aucun des sommets de L n'est le ième sommet de K
     si   Numero(i)=j => Le j-ième sommet  de L  est  le ième sommet de K
     la routine sort le nombre de sommets communs 0,1,2,3
     + il sort -1 si les deux triangles sont considérés comme proche */
  real_t r2, crit;
  int_t n;
  /* On calcule le carre de la longueur de la plus petite des aretes */
  r2 = 10.E15;
  r2 =    fmin(r2,
               (ck[0][0]-ck[1][0])*(ck[0][0]-ck[1][0])+
               (ck[0][1]-ck[1][1])*(ck[0][1]-ck[1][1])+
               (ck[0][2]-ck[1][2])*(ck[0][2]-ck[1][2]));
  r2 =    fmin(r2,
               (ck[0][0]-ck[2][0])*(ck[0][0]-ck[2][0])+
               (ck[0][1]-ck[2][1])*(ck[0][1]-ck[2][1])+
               (ck[0][2]-ck[2][2])*(ck[0][2]-ck[2][2]));
  r2 =    fmin(r2,
               (ck[1][0]-ck[2][0])*(ck[1][0]-ck[2][0])+
               (ck[1][1]-ck[2][1])*(ck[1][1]-ck[2][1])+
               (ck[1][2]-ck[2][2])*(ck[1][2]-ck[2][2]));

  r2 =    fmin(r2,
               (cl[0][0]-cl[1][0])*(cl[0][0]-cl[1][0])+
               (cl[0][1]-cl[1][1])*(cl[0][1]-cl[1][1])+
               (cl[0][2]-cl[1][2])*(cl[0][2]-cl[1][2]));
  r2 =    fmin(r2,
               (cl[0][0]-cl[2][0])*(cl[0][0]-cl[2][0])+
               (cl[0][1]-cl[2][1])*(cl[0][1]-cl[2][1])+
               (cl[0][2]-cl[2][2])*(cl[0][2]-cl[2][2]));
  r2 =    fmin(r2,
               (cl[1][0]-cl[2][0])*(cl[1][0]-cl[2][0])+
               (cl[1][1]-cl[2][1])*(cl[1][1]-cl[2][1])+
               (cl[1][2]-cl[2][2])*(cl[1][2]-cl[2][2]));
  /* mon critere de points confondus est (attention fragile)*/
  crit = r2*1.e-12 ;
  n         = 0;
  Numero[0] = 0;
  if(    (ck[0][0]-cl[0][0])*(ck[0][0]-cl[0][0])+
         (ck[0][1]-cl[0][1])*(ck[0][1]-cl[0][1])+
         (ck[0][2]-cl[0][2])*(ck[0][2]-cl[0][2])  < crit) {Numero[0] = 1 ; n++;};
  if(    (ck[0][0]-cl[1][0])*(ck[0][0]-cl[1][0])+
         (ck[0][1]-cl[1][1])*(ck[0][1]-cl[1][1])+
         (ck[0][2]-cl[1][2])*(ck[0][2]-cl[1][2])  < crit) {Numero[0] = 2 ; n++;};
  if(    (ck[0][0]-cl[2][0])*(ck[0][0]-cl[2][0])+
         (ck[0][1]-cl[2][1])*(ck[0][1]-cl[2][1])+
         (ck[0][2]-cl[2][2])*(ck[0][2]-cl[2][2])  < crit) {Numero[0] = 3 ; n++;};
  Numero[1] = 0;
  if(    (ck[1][0]-cl[0][0])*(ck[1][0]-cl[0][0])+
         (ck[1][1]-cl[0][1])*(ck[1][1]-cl[0][1])+
         (ck[1][2]-cl[0][2])*(ck[1][2]-cl[0][2])  < crit) {Numero[1] = 1 ; n++;};
  if(    (ck[1][0]-cl[1][0])*(ck[1][0]-cl[1][0])+
         (ck[1][1]-cl[1][1])*(ck[1][1]-cl[1][1])+
         (ck[1][2]-cl[1][2])*(ck[1][2]-cl[1][2])  < crit) {Numero[1] = 2 ; n++;};
  if(    (ck[1][0]-cl[2][0])*(ck[1][0]-cl[2][0])+
         (ck[1][1]-cl[2][1])*(ck[1][1]-cl[2][1])+
         (ck[1][2]-cl[2][2])*(ck[1][2]-cl[2][2])  < crit) {Numero[1] = 3 ; n++;};
  Numero[2] = 0;
  if(    (ck[2][0]-cl[0][0])*(ck[2][0]-cl[0][0])+
         (ck[2][1]-cl[0][1])*(ck[2][1]-cl[0][1])+
         (ck[2][2]-cl[0][2])*(ck[2][2]-cl[0][2])  < crit) {Numero[2] = 1 ; n++;};
  if(    (ck[2][0]-cl[1][0])*(ck[2][0]-cl[1][0])+
         (ck[2][1]-cl[1][1])*(ck[2][1]-cl[1][1])+
         (ck[2][2]-cl[1][2])*(ck[2][2]-cl[1][2])  < crit) {Numero[2] = 2 ; n++;};
  if(    (ck[2][0]-cl[2][0])*(ck[2][0]-cl[2][0])+
         (ck[2][1]-cl[2][1])*(ck[2][1]-cl[2][1])+
         (ck[2][2]-cl[2][2])*(ck[2][2]-cl[2][2])  < crit) {Numero[2] = 3 ; n++;};

  //printf(" nb_cv    %3d...   \n", n);
  if( n!=0)   return(n);
  if( eta==0.e0) return(n);
  /* Les triangles ne se touchent pas...
     on renvoie -1 si le critere de proximité est satisfait  */
  /* on calcule le max des carrés des longueurs des arêtes de K et de L... */
  real_t r2k;
  r2k  =
    (ck[1][0]-ck[0][0])*(ck[1][0]-ck[0][0]) +
    (ck[1][1]-ck[0][1])*(ck[1][1]-ck[0][1]) +
    (ck[1][2]-ck[0][2])*(ck[1][2]-ck[0][2]) ;
  r2k = fmax(r2k,
             (ck[2][0]-ck[1][0])*(ck[2][0]-ck[1][0]) +
             (ck[2][1]-ck[1][1])*(ck[2][1]-ck[1][1]) +
             (ck[2][2]-ck[1][2])*(ck[2][2]-ck[1][2]) );
  r2k = fmax(r2k,
             (ck[0][0]-ck[2][0])*(ck[0][0]-ck[2][0]) +
             (ck[0][1]-ck[2][1])*(ck[0][1]-ck[2][1]) +
             (ck[0][2]-ck[2][2])*(ck[0][2]-ck[2][2]) );
  real_t r2l;
  r2l =
    (cl[1][0]-cl[0][0])*(cl[1][0]-cl[0][0]) +
    (cl[1][1]-cl[0][1])*(cl[1][1]-cl[0][1]) +
    (cl[1][2]-cl[0][2])*(cl[1][2]-cl[0][2]) ;
  r2l = fmax(r2l,
             (cl[2][0]-cl[1][0])*(cl[2][0]-cl[1][0]) +
             (cl[2][1]-cl[1][1])*(cl[2][1]-cl[1][1]) +
             (cl[2][2]-cl[1][2])*(cl[2][2]-cl[1][2]) );
  r2l = fmax(r2l,
             (cl[0][0]-cl[2][0])*(cl[0][0]-cl[2][0]) +
             (cl[0][1]-cl[2][1])*(cl[0][1]-cl[2][1]) +
             (cl[0][2]-cl[2][2])*(cl[0][2]-cl[2][2]) );
  /* les triangles sont a priori distincts
     on dira que deux triangles sont proches
     si la distance entre leur centre de gravite
     est plus petite que eta fois la somme des
     diametres des triangles */

  /* d2 = carre de 3 fois la distance entre centre de gravite */
  real_t d;
  d         =      ck[0][0]+ ck[1][0]+ ck[2][0] ;
  d         =  d-( cl[0][0]+ cl[1][0]+ cl[2][0]);
  real_t d2;
  d2        =  d*d;
  d         =      ck[0][1]+ ck[1][1]+ ck[2][1] ;
  d         =  d-( cl[0][1]+ cl[1][1]+ cl[2][1]);
  d2        =  d2+ d*d;
  d         =      ck[0][2]+ ck[1][2]+ ck[2][2] ;
  d         =  d-( cl[0][2]+ cl[1][2]+ cl[2][2]);
  d2        =  d2+ d*d;
  if(std::sqrt(d2) < 3*eta*(std::sqrt(r2k)+std::sqrt(r2l))) n  =-1 ;
  return(n);
}

Mypermut_t*  ElemTools_strong_getpermut(number_t numero[3])
{
  Mypermut_t* p;
  mymalloc(p, number_t(1), Mypermut_t);
  number_t  e[3]= {0,1,2} ;   number_t pg[3]= {1,2,0} ; number_t pd[3]= {2,0,1} ;
  number_t s1[3]= {0,2,1} ;   number_t s2[3]= {2,1,0} ; number_t s3[3]= {1,0,2} ;
  number_t flag =  numero[0] + 4*numero[1] + 16*numero[2]  ;
  //printf(" flag %4d numerop %5d %5d %5d \n",flag, numero[0],numero[1],numero[2] );
  number_t sz3= 3*sizeof(number_t);
  switch (flag)
    {
    case  0:  memcpy(p->sk,e,sz3); memcpy(p->sl,e,sz3); break;
    case  1:  memcpy(p->sk,e,sz3); memcpy(p->sl,e,sz3); break;
    case  2:  memcpy(p->sk,e,sz3); memcpy(p->sl,pg,sz3); break;
    case  3:  memcpy(p->sk,e,sz3); memcpy(p->sl,pd,sz3); break;
    case  4:  memcpy(p->sk,pg,sz3); memcpy(p->sl,e,sz3); break;
    case  6:  memcpy(p->sk,e,sz3); memcpy(p->sl,e,sz3); break;
    case  7:  memcpy(p->sk,e,sz3); memcpy(p->sl,s1,sz3); break;
    case  8:  memcpy(p->sk,pg,sz3); memcpy(p->sl,pg,sz3); break;
    case  9:  memcpy(p->sk,e,sz3); memcpy(p->sl,s3,sz3); break;
    case 11:  memcpy(p->sk,e,sz3); memcpy(p->sl,pg,sz3); break;
    case 12:  memcpy(p->sk,pg,sz3); memcpy(p->sl,pd,sz3); break;
    case 13:  memcpy(p->sk,e,sz3); memcpy(p->sl,pd,sz3); break;
    case 14:  memcpy(p->sk,e,sz3); memcpy(p->sl,s2,sz3); break;
    case 16:  memcpy(p->sk,pd,sz3); memcpy(p->sl,e,sz3); break;
    case 18:  memcpy(p->sk,pd,sz3); memcpy(p->sl,s3,sz3); break;
    case 19:  memcpy(p->sk,pd,sz3); memcpy(p->sl,pd,sz3); break;
    case 24:  memcpy(p->sk,pg,sz3); memcpy(p->sl,e,sz3); break;
    case 27:  memcpy(p->sk,e,sz3); memcpy(p->sl,s2,sz3); break;
    case 28:  memcpy(p->sk,pg,sz3); memcpy(p->sl,s1,sz3); break;
    case 30:  memcpy(p->sk,e,sz3); memcpy(p->sl,pg,sz3); break;
    case 32:  memcpy(p->sk,pd,sz3); memcpy(p->sl,pg,sz3); break;
    case 33:  memcpy(p->sk,pd,sz3); memcpy(p->sl,e,sz3); break;
    case 35:  memcpy(p->sk,pd,sz3); memcpy(p->sl,s2,sz3); break;
    case 36:  memcpy(p->sk,pg,sz3); memcpy(p->sl,s3,sz3); break;
    case 39:  memcpy(p->sk,e,sz3); memcpy(p->sl,pd,sz3); break;
    case 44:  memcpy(p->sk,pg,sz3); memcpy(p->sl,pg,sz3); break;
    case 45:  memcpy(p->sk,e,sz3); memcpy(p->sl,s1,sz3); break;
    case 48:  memcpy(p->sk,pd,sz3); memcpy(p->sl,pd,sz3); break;
    case 49:  memcpy(p->sk,pd,sz3); memcpy(p->sl,s1,sz3); break;
    case 50:  memcpy(p->sk,pd,sz3); memcpy(p->sl,pg,sz3); break;
    case 52:  memcpy(p->sk,pg,sz3); memcpy(p->sl,pd,sz3); break;
    case 54:  memcpy(p->sk,e,sz3); memcpy(p->sl,s3,sz3); break;
    case 56:  memcpy(p->sk,pg,sz3); memcpy(p->sl,s2,sz3); break;
    case 57:  memcpy(p->sk,e,sz3); memcpy(p->sl,e,sz3); break;
    default:  printf("bug \n"); break;
    };
  return(p);
}

void cal_invlength( real_t ck[3][3],  number_t sig[3], real_t lenght[3])
{
  number_t n1, n2;
  n1 = sig[1]; n2=sig[2];
  lenght[0]=  1/std::sqrt(
                (ck[n1][0]-ck[n2][0])*(ck[n1][0]-ck[n2][0])+
                (ck[n1][1]-ck[n2][1])*(ck[n1][1]-ck[n2][1])+
                (ck[n1][2]-ck[n2][2])*(ck[n1][2]-ck[n2][2])
              );
  n1 = sig[0]; n2=sig[2];
  lenght[1]=  1/std::sqrt(
                (ck[n1][0]-ck[n2][0])*(ck[n1][0]-ck[n2][0])+
                (ck[n1][1]-ck[n2][1])*(ck[n1][1]-ck[n2][1])+
                (ck[n1][2]-ck[n2][2])*(ck[n1][2]-ck[n2][2])
              );
  n1 = sig[0]; n2=sig[1];
  lenght[2]=  1/std::sqrt(
                (ck[n1][0]-ck[n2][0])*(ck[n1][0]-ck[n2][0])+
                (ck[n1][1]-ck[n2][1])*(ck[n1][1]-ck[n2][1])+
                (ck[n1][2]-ck[n2][2])*(ck[n1][2]-ck[n2][2])
              );
}

void ElemTools_permtri( real_t ck[3][3],  real_t cl[3][3], Mypermut_t *p, real_t ckp[3][3],  real_t clp[3][3])
{
  number_t ip, jp, i, j;
  for(ip=0; ip<3; ip++)
    {
      i = p->sk[ip];
      ckp[ip][0] = ck[i][0];
      ckp[ip][1] = ck[i][1];
      ckp[ip][2] = ck[i][2];
    }
  for(ip=0; ip<3; ip++)
    {
      i = p->sl[ip];
      clp[ip][0] = cl[i][0];
      clp[ip][1] = cl[i][1];
      clp[ip][2] = cl[i][2];
    }
}

void ElemTools_permutmat(Mypermut_t * p, complex_t ztr[3][3], complex_t zkr[3][3], complex_t zts[3][3],
                         complex_t zks[3][3], complex_t zt[3][3],  complex_t zk[3][3], number_t cf)
{
  number_t i, j, dl, dk ;
  complex_t z[3][3];
  number_t num[3]= {1,2,0};

  if(cf==1 || cf==3)
    {
      /* bêtement, j'ai fait le calcul des transposées, mais bon...*/
      for(i=0; i<3; i++) { for(j=0; j<3; j++) { z[i][j]=  zts[j][i]+ztr[j][i];};};
      /* prise en compte du changement de numérotation des arêtes:
           convention prise pour le calcul: arête a --> opposée au vertex a
           convention du code: arête a --> opposée au vertex a+2 modulo 3
      */
      /* prise en compte des deux permutations + changement de convention...*/
      for(i=0; i<3; i++)
        {
          dk = num[p->sk[i]];
          for(j=0; j<3; j++)
            {
              dl =  num[p->sl[j]]  ;
              zt[dk][dl] = z[i][j] ;
            }
        }
    }
  if(cf>=2)
    {
      for(i=0; i<3; i++) { for(j=0; j<3; j++) { z[i][j]=  zks[j][i]+zkr[j][i];};};
      for(i=0; i<3; i++)
        {
          dk = num[p->sk[i]];
          for(j=0; j<3; j++)
            {
              dl =  num[p->sl[j]];
              zk[dk][dl] = z[i][j] ;
            }
        }
    }
}


void oh(complex_t zts[3][3], complex_t zks[3][3], complex_t ztr[3][3], complex_t zkr[3][3]  )
{
  printf("apres Lenoir zts 0: %e %e %e  \n", real(zts[0][0]),
         real(zts[0][1]),  real(zts[0][2]) );
  printf("apres Lenoir zts 1: %e %e %e  \n", real(zts[1][0]),
         real(zts[1][1]),  real(zts[1][2]) );
  printf("apres Lenoir zts 2: %e %e %e  \n", real(zts[0][0]),
         real(zts[2][1]),  real(zts[2][2]) );
  printf("apres Lenoir zks 0: %e %e %e  \n", real(zks[0][0]),
         real(zks[0][1]),  real(zks[0][2]) );
  printf("apres Lenoir zks 1: %e %e %e  \n", real(zks[1][0]),
         real(zks[1][1]),  real(zks[1][2]) );
  printf("apres Lenoir zks 2: %e %e %e  \n", real(zks[2][0]),
         real(zks[2][1]),  real(zks[2][2]) );
  printf("apres Lenoir ztr 0: %e %e %e  \n", real(ztr[0][0]),
         real(ztr[0][1]),  real(ztr[0][2]) );
  printf("apres Lenoir ztr 1: %e %e %e  \n", real(ztr[1][0]),
         real(ztr[1][1]),  real(ztr[1][2]) );
  printf("apres Lenoir ztr 2: %e %e %e  \n", real(ztr[0][0]),
         real(ztr[2][1]),  real(ztr[2][2]) );
  printf("apres Lenoir zkr 0: %e %e %e  \n", real(zkr[0][0]),
         real(zkr[0][1]),  real(zkr[0][2]) );
  printf("apres Lenoir zkr 1: %e %e %e  \n", real(zkr[1][0]),
         real(zkr[1][1]),  real(zkr[1][2]) );
  printf("apres Lenoir zkr 2: %e %e %e  \n", real(zkr[2][0]),
         real(zkr[2][1]),  real(zkr[2][2]) );
}

void ElemTools_weakstrong_c(real_t ck[3][3],real_t cl[3][3], real_t ka[2], Myquad_t* quad, complex_t zt[3][3], complex_t zk[3][3], number_t cf)
{
  number_t Numero[3] ;
  number_t Numerot[3];
  int_t nb_cv, nb_cvt;
  Mypermut_t* p;
  Mypermut_t* pt;
  complex_t zkt[3][3];
  real_t ckp[3][3];
  real_t clp[3][3];
  complex_t zts[3][3];
  complex_t zks[3][3];
  complex_t ztr[3][3];
  complex_t zkr[3][3];
  real_t zksin[3][3];

  /******* Détermination  du cas à traiter .... out=> Numero[0:2] (quad->eta sert à discriminer proche ou lointain) */
  nb_cv =  ElemTools_strong_ncv( ck, cl, Numero, quad->eta );
  /*************************************************/
  switch (nb_cv)
    {
    case -1://************* PAIRE DE TRIANGLES DISJOINTS MAIS PROCHES *****
      ElemTools_singular(ck, cl, ka, quad, zt, zk, cf);
      break;

    case 0: //************* PAIRE DE TRIANGLES ELOIGNES *******************
      if (  ka[1] ==  0. )
        {ElemTools_strong_Gauss_kareal(ck,cl, ka[0], quad, zt, zk, cf);}
      else
        {ElemTools_strong_Gauss_kacmplx(ck,cl, ka, quad, zt, zk, cf);}
      break;

    case 1: //************* PAIRE DE TRIANGLES SOMMET COMMUN  *************
      p=ElemTools_strong_getpermut( Numero );
      ElemTools_permtri(ck, cl, p,ckp, clp);
      if( ka[1]==0.e0) {ElemTools_elem_r (ckp,clp,ka[0],quad,ztr,zkr, cf);}
      else            {ElemTools_elem_rc(ckp,clp,ka,quad,ztr,zkr, cf);}
      E_lenoir_vertex(ckp,clp,ka, quad, zts, zks, cf);
      ElemTools_permutmat( p,ztr, zkr, zts, zks, zt, zk, cf);
      free(p);
      break;

    case 2: //************* PAIRE DE TRIANGLES ARETE COMMUNE  *************
      p=ElemTools_strong_getpermut( Numero );
      int tmp; tmp = p->sl[0];  p->sl[0] = p->sl[1] ;  p->sl[1] = tmp;

      ElemTools_permtri(ck, cl, p,ckp, clp);
      if( ka[1]==0.e0) {ElemTools_elem_r (ckp, clp, ka[0], quad, ztr, zkr, cf);}
      else             {ElemTools_elem_rc(ckp, clp, ka, quad, ztr, zkr, cf);}
      E_lenoir_edge(ckp,clp,ka, quad, zts, zks, cf);
      ElemTools_permutmat( p,ztr, zkr, zts, zks, zt, zk, cf);
      free(p);
      break;

    case 3: //************* PAIRE DE TRIANGLES CONFONDUS    ***************
      p=ElemTools_strong_getpermut( Numero );
      ElemTools_permtri(ck, cl, p,ckp, clp);
      if( ka[1]==0.e0) {ElemTools_elem_r (ckp, clp, ka[0], quad, ztr, zkr, cf);}
      else             {ElemTools_elem_rc(ckp, clp, ka, quad, ztr, zkr, cf);}
      E_lenoir_triangle(ckp,ka, quad, zts, zks, cf );
      ElemTools_permutmat( p,ztr, zkr, zts, zks, zt, zk, cf);
      free(p);
      for(number_t i1=0; i1 < 3; i1++)
        for(number_t j1=0; j1 < 3; j1++) zk[i1][j1]=0;;
      break;
    }
}

void ElemTools_elem_rc( real_t ck[3][3],   real_t cl[3][3], real_t kat[2], Myquad_t* quad, complex_t zt[3][3],complex_t zk[3][3], number_t cf)
{
  real_t u[3]; real_t v[3]; real_t uv[3];
  real_t t[3]; real_t x[3]; real_t  y[3];
  real_t cst, r, r2, p;
  complex_t kar, c, cexpo, devh_tay, devg_tay, devd_tay, devh_reg, devg_reg, devd_reg, gkl, hkl, ka, fkar, fkai, ka3, ka2;
  complex_t karp[6];
  number_t gk, gl, dk, dl;
  number_t num[3];
  number_t i1, j1;
  for( i1=0; i1 < 3; i1++)
    {
      for( j1=0; j1 < 3; j1++)
        {
          zk[i1][j1]=0;
          zt[i1][j1]=0;
        }
    }
  ka   = kat[0] +  i_*kat[1];
  c    =-4./(ka*ka);
  cst  = 1./(16*std::acos(-1.e0));
  for(  gl=0; gl< quad->ng; gl++)
    {
      x[0]=     cl[0][0]                   +
                quad->a[gl]*( cl[1][0]-  cl[0][0]) +
                quad->b[gl]*( cl[2][0]-  cl[0][0]);
      x[1]=     cl[0][1]                   +
                quad->a[gl]*( cl[1][1]-  cl[0][1]) +
                quad->b[gl]*( cl[2][1]-  cl[0][1]);
      x[2]=     cl[0][2]                   +
                quad->a[gl]*( cl[1][2]-  cl[0][2]) +
                quad->b[gl]*( cl[2][2]-  cl[0][2]);
      for(  gk=0; gk< quad->ng ; gk++)
        {
          y[0]=     ck[0][0]                   +
                    quad->a[gk]*( ck[1][0]-  ck[0][0]) +
                    quad->b[gk]*( ck[2][0]-  ck[0][0]);
          y[1]=     ck[0][1]                   +
                    quad->a[gk]*( ck[1][1]-  ck[0][1]) +
                    quad->b[gk]*( ck[2][1]-  ck[0][1]);
          y[2]=     ck[0][2]                   +
                    quad->a[gk]*( ck[1][2]-  ck[0][2]) +
                    quad->b[gk]*( ck[2][2]-  ck[0][2]);

          t[0]    = y[0]-x[0];
          t[1]    = y[1]-x[1];
          t[2]    = y[2]-x[2];
          r2      = t[0]*t[0]+ t[1]*t[1]+ t[2]*t[2];
          r2      = std::max(r2,1.e-40);
          r       = std::sqrt(r2);
          kar     = ka*r ;
          p       = quad->w[gk]*quad->w[gl]*cst;
          if( std::abs(kar) > 0.4e0)
            {
              /* cosinus =   cos(kar);
              sinus   =   sin(kar);
              cosinus =   cosinus*p ;
              sinus   =     sinus*p ;
              gkl     =   (ka/r)*(cosinus+I*sinus);*/
              cexpo   =   std::exp(i_*kar)*p;
              gkl     =   (ka/r)*cexpo;
              devg_tay=   (ka/r)*(1 -0.5e0*kar*kar ) ;
              devg_tay=   devg_tay*p ;
              devg_reg=   gkl - devg_tay ;
              devd_tay=   (ka/r)*(1 -0.5e0*kar*kar*(1-kar*kar/12)) ;
              devd_tay=   devd_tay*p ;
              devd_reg=   gkl - devd_tay ;
              /* cosinus =    cosinus/r2 ;
              sinus   =      sinus/r2 ;
              hkl     =   cosinus+I*sinus ;*/
              hkl     =   cexpo/r2;
              hkl     =   hkl*(-1.e0/r+i_*ka) ;
              devh_tay=  -(1+ 0.5e0*kar*kar*(1-kar*kar/4 ))/(r2*r);
              devh_tay =  devh_tay*p ;
              devh_reg =  hkl - devh_tay;
            }
          else
            {
              karp[1] = kar*kar;
              karp[2] = karp[1]*karp[1];
              karp[3] = karp[1]*karp[2];
              karp[4] = karp[1]*karp[3];
              karp[5] = karp[1]*karp[4];
              karp[0] = kar*karp[1];
              ka2     = ka*ka ;
              ka3     = ka*ka2;
              fkai = (-ka3/3)*( 1- karp[1]/10.e0 +karp[2]/280.e0 -karp[3]/15120.e0 + karp[4]/1330860.e0 - karp[5]/172972800.e0  );
              fkar =  -ka3*karp[0]*( 1.e0/144.e0 - karp[1]/5760.e0 +karp[2]/403200-karp[3]/43545600.e0 + karp[4]/6706022400.e0 - karp[5]/1394852659200.e0);
              devh_reg =   fkar+i_*fkai ;
              fkai = ka2*(1 - karp[1]/6 +karp[2]/120 -karp[3]/5040.e0 + karp[4]/362880.e0 - karp[5]/399168000.e0);
              fkar = ka2*karp[0]*(1.e0/24 -karp[1]/720.e0   + karp[2]/40320.e0 -karp[3]/3628800.e0+ karp[4]/479001600.e0 );
              devg_reg =   fkar+i_*fkai ;
              fkar = ka2*karp[0]*( - karp[1]/720.e0     +karp[2]/40320.e0 -karp[3]/3628800.e0 + karp[4]/479001600.e0 );
              devd_reg =  fkar+i_*fkai ;
              devd_reg =  p*devd_reg;
              devg_reg =  p*devg_reg;
              devh_reg =  p*devh_reg;
            }

          {
            for( dl=0; dl < 3; dl++)
              {
                u[0] = x[0]-cl[dl][0];
                u[1] = x[1]-cl[dl][1];
                u[2] = x[2]-cl[dl][2];
                for( dk=0; dk < 3; dk++)
                  {
                    v[0] = y[0]-ck[dk][0];
                    v[1] = y[1]-ck[dk][1];
                    v[2] = y[2]-ck[dk][2];
                    uv[0]= v[1]*u[2]-v[2]*u[1];
                    uv[1]= v[2]*u[0]-v[0]*u[2];
                    uv[2]= v[0]*u[1]-v[1]*u[0];
                    if(cf>=2) zk[dl][dk] += (t[0]*uv[0]+t[1]*uv[1]+t[2]*uv[2])*devh_reg;
                    if(cf==1 || cf==3) zt[dl][dk] += (u[0]*v[0]+u[1]*v[1]+u[2]*v[2])*devg_reg+c* devd_reg;
                  }
              }
          }
        }
    }
}

void ElemTools_elem_r(real_t ck[3][3],real_t cl[3][3], real_t ka, Myquad_t* quad, complex_t zt[3][3],complex_t zk[3][3], number_t cf)
{
  real_t u[3]; real_t v[3]; real_t uv[3];
  real_t t[3]; real_t x[3]; real_t y[3];
  real_t r, r2, kar, c, p, cosinus, devh_tay, devg_tay, devd_tay;
  complex_t devh_reg, devg_reg, devd_reg;
  real_t   sinus; real_t cst;
  number_t  gk, gl, dk, dl;
  complex_t gkl, hkl, hklreg;
  real_t fkar, fkai, ka3, ka2;
  real_t  karp[6];
  number_t num[3];
  number_t i1, j1;
  for( i1=0; i1 < 3; i1++)
    {
      for( j1=0; j1 < 3; j1++)
        {
          zk[i1][j1]=0;
          zt[i1][j1]=0;
        }
    }
  c  =-4./(ka*ka);
  cst = 1./(16*std::acos(-1.e0));
  for(  gl=0; gl< quad->ng; gl++)
    {
      x[0]=     cl[0][0]                   +
                quad->a[gl]*( cl[1][0]-  cl[0][0]) +
                quad->b[gl]*( cl[2][0]-  cl[0][0]);
      x[1]=     cl[0][1]                   +
                quad->a[gl]*( cl[1][1]-  cl[0][1]) +
                quad->b[gl]*( cl[2][1]-  cl[0][1]);
      x[2]=     cl[0][2]                   +
                quad->a[gl]*( cl[1][2]-  cl[0][2]) +
                quad->b[gl]*( cl[2][2]-  cl[0][2]);
      for(  gk=0; gk< quad->ng ; gk++)
        {
          y[0]=     ck[0][0]                   +
                    quad->a[gk]*( ck[1][0]-  ck[0][0]) +
                    quad->b[gk]*( ck[2][0]-  ck[0][0]);
          y[1]=     ck[0][1]                   +
                    quad->a[gk]*( ck[1][1]-  ck[0][1]) +
                    quad->b[gk]*( ck[2][1]-  ck[0][1]);
          y[2]=     ck[0][2]                   +
                    quad->a[gk]*( ck[1][2]-  ck[0][2]) +
                    quad->b[gk]*( ck[2][2]-  ck[0][2]);
          t[0]    = y[0]-x[0];
          t[1]    = y[1]-x[1];
          t[2]    = y[2]-x[2];
          r2      = t[0]*t[0]+ t[1]*t[1]+ t[2]*t[2];
          r2      = std::max(r2,1.e-40);
          r       = std::sqrt(r2);
          kar     = ka*r ;
          p       = quad->w[gk]*quad->w[gl]*cst;
          if( kar > 0.4e0)
            {
              cosinus =   std::cos(kar);
              sinus   =   std::sin(kar);
              cosinus =   cosinus*p ;
              sinus   =     sinus*p ;
              gkl     =   (ka/r)*(cosinus+i_*sinus);
              devg_tay=   (ka/r)*(1 -0.5e0*kar*kar ) ;
              devg_tay =  devg_tay*p ;
              devg_reg =  gkl - devg_tay ;
              devd_tay=   (ka/r)*(1 -0.5e0*kar*kar*(1-kar*kar/12)) ;
              devd_tay =  devd_tay*p ;
              devd_reg =  gkl - devd_tay ;
              cosinus =    cosinus/r2 ;
              sinus   =      sinus/r2 ;
              hkl     =   cosinus+i_*sinus ;
              hkl     =   hkl*(-1.e0/r+i_*ka) ;
              devh_tay=  -(1+ 0.5e0*kar*kar*(1-kar*kar/4 ))/(r2*r);
              devh_tay =  devh_tay*p ;
              devh_reg =  hkl - devh_tay;
            }
          else
            {
              karp[1] = kar*kar;
              karp[2] = karp[1]*karp[1];
              karp[3] = karp[1]*karp[2];
              karp[4] = karp[1]*karp[3];
              karp[5] = karp[1]*karp[4];
              karp[0] = kar*karp[1];
              ka2     = ka*ka ;
              ka3     = ka*ka2;
              fkai = (-ka3/3)*( 1- karp[1]/10.e0 +karp[2]/280.e0 -karp[3]/15120.e0 + karp[4]/1330860.e0 - karp[5]/172972800.e0  );
              fkar =  -ka3*karp[0]*( 1.e0/144.e0 - karp[1]/5760.e0 +karp[2]/403200-karp[3]/43545600.e0 + karp[4]/6706022400.e0 - karp[5]/1394852659200.e0);
              devh_reg =   fkar+i_*fkai ;
              fkai = ka2*(1 - karp[1]/6 +karp[2]/120 -karp[3]/5040.e0 + karp[4]/362880.e0 - karp[5]/399168000.e0);
              fkar = ka2*karp[0]*(1.e0/24 -karp[1]/720.e0   + karp[2]/40320.e0 -karp[3]/3628800.e0+ karp[4]/479001600.e0 );
              devg_reg =   fkar+i_*fkai ;
              fkar = ka2*karp[0]*( - karp[1]/720.e0     +karp[2]/40320.e0 -karp[3]/3628800.e0 + karp[4]/479001600.e0 );
              devd_reg =  fkar+i_*fkai ;
              devd_reg =  p*devd_reg;
              devg_reg =  p*devg_reg;
              devh_reg =  p*devh_reg;
            }
          for( dl=0; dl < 3; dl++)
            {
              u[0] = x[0]-cl[dl][0];
              u[1] = x[1]-cl[dl][1];
              u[2] = x[2]-cl[dl][2];
              for( dk=0; dk < 3; dk++)
                {
                  v[0] = y[0]-ck[dk][0];
                  v[1] = y[1]-ck[dk][1];
                  v[2] = y[2]-ck[dk][2];
                  uv[0]= v[1]*u[2]-v[2]*u[1];
                  uv[1]= v[2]*u[0]-v[0]*u[2];
                  uv[2]= v[0]*u[1]-v[1]*u[0];
                  if(cf>=2) zk[dl][dk] += (t[0]*uv[0]+t[1]*uv[1]+t[2]*uv[2])*devh_reg;
                  if(cf==1 || cf==3) zt[dl][dk] += (u[0]*v[0]+u[1]*v[1]+u[2]*v[2])*devg_reg+c* devd_reg;
                }
            }
        }
    }
}

Mytriangle_t* ElemTools_triangle_c(real_t vertex[3][3])
{
  Mytriangle_t* triangle;
  real_t u0, u1, u2, v0, v1, v2, r;
  real_t y[3];
  mymalloc(triangle, number_t(1), Mytriangle_t);

  triangle->vertex[0][0] = vertex[0][0];
  triangle->vertex[0][1] = vertex[0][1];
  triangle->vertex[0][2] = vertex[0][2];
  triangle->vertex[1][0] = vertex[1][0];
  triangle->vertex[1][1] = vertex[1][1];
  triangle->vertex[1][2] = vertex[1][2];
  triangle->vertex[2][0] = vertex[2][0];
  triangle->vertex[2][1] = vertex[2][1];
  triangle->vertex[2][2] = vertex[2][2];

  u0=vertex[1][0]-vertex[0][0];
  u1=vertex[1][1]-vertex[0][1];
  u2=vertex[1][2]-vertex[0][2];
  v0=vertex[2][0]-vertex[0][0];
  v1=vertex[2][1]-vertex[0][1];
  v2=vertex[2][2]-vertex[0][2];

  r=(u1*v2-v1*u2)*(u1*v2-v1*u2)+(u0*v2-v0*u2)*(u0*v2-v0*u2)+(u0*v1-v0*u1)*+(u0*v1-v0*u1);
  triangle->normal[0] = (u1*v2-v1*u2)/std::sqrt(r);
  triangle->normal[1] = (u2*v0-v2*u0)/std::sqrt(r);
  triangle->normal[2] = (u0*v1-v0*u1)/std::sqrt(r);
  triangle->area      = std::sqrt(r)*0.5e0;

  /* arete 0 oppos\'ee au sommet 0 :*/
  y[0]               = vertex[2][0] - vertex[1][0];
  y[1]               = vertex[2][1] - vertex[1][1];
  y[2]               = vertex[2][2] - vertex[1][2];
  u0                 = std::sqrt(  y[0]*y[0] + y[1]*y[1] + y[2]*y[2]);
  triangle->ledge[0]  = u0;
  triangle->tau[0][0] = y[0]/u0;
  triangle->tau[0][1] = y[1]/u0;
  triangle->tau[0][2] = y[2]/u0;

  /* arete 1 oppos\'ee au sommet 1 :*/
  y[0]                = vertex[0][0] - vertex[2][0];
  y[1]                = vertex[0][1] - vertex[2][1];
  y[2]                = vertex[0][2] - vertex[2][2];
  u1                  = std::sqrt( y[0]*y[0] + y[1]*y[1] + y[2]*y[2]);
  triangle->ledge[1]   = u1;
  triangle->tau[1][0]  = y[0]/u1;
  triangle->tau[1][1]  = y[1]/u1;
  triangle->tau[1][2]  = y[2]/u1;

  /* arete 2 oppos\'ee au sommet 2 :*/
  y[0]               = vertex[1][0] - vertex[0][0];
  y[1]               = vertex[1][1] - vertex[0][1];
  y[2]               = vertex[1][2] - vertex[0][2];
  u2                 = std::sqrt(  y[0]*y[0] + y[1]*y[1] + y[2]*y[2]);
  triangle->ledge[2]  = u2;
  triangle->tau[2][0]  = y[0]/u2;
  triangle->tau[2][1]  = y[1]/u2;
  triangle->tau[2][2]  = y[2]/u2;

  /* normales sortantes aux aretes dans le plan du triangle */
  triangle->nu[0][0] = triangle->normal[2]*triangle->tau[0][1]- triangle->normal[1]*triangle->tau[0][2];
  triangle->nu[1][0] = triangle->normal[2]*triangle->tau[1][1]- triangle->normal[1]*triangle->tau[1][2];
  triangle->nu[2][0] = triangle->normal[2]*triangle->tau[2][1]- triangle->normal[1]*triangle->tau[2][2];
  triangle->nu[0][1] = triangle->normal[0]*triangle->tau[0][2]- triangle->normal[2]*triangle->tau[0][0];
  triangle->nu[1][1] = triangle->normal[0]*triangle->tau[1][2]- triangle->normal[2]*triangle->tau[1][0];
  triangle->nu[2][1] = triangle->normal[0]*triangle->tau[2][2]- triangle->normal[2]*triangle->tau[2][0];
  triangle->nu[0][2] = triangle->normal[1]*triangle->tau[0][0]- triangle->normal[0]*triangle->tau[0][1];
  triangle->nu[1][2] = triangle->normal[1]*triangle->tau[1][0]- triangle->normal[0]*triangle->tau[1][1];
  triangle->nu[2][2] = triangle->normal[1]*triangle->tau[2][0]- triangle->normal[0]*triangle->tau[2][1];

  /* + grande longueur d'arête du triangle */
  real_t lp;
  lp = triangle->ledge[0] ;
  if( lp <  triangle->ledge[1]) { lp = triangle->ledge[1]; };
  if( lp <  triangle->ledge[2]) { lp = triangle->ledge[2];};
  triangle->diam = lp ;
  return(triangle);
}

Myct_t* ElemTools_triangle_x(  real_t x[3], Mytriangle_t *triangle)
{
  real_t v[3];
  real_t y[3];
  real_t z[3];
  real_t d, t1, t2;
  number_t a, s1, s2;
  Myct_t*   ct;
  mymalloc(ct, number_t(1), Myct_t);

  ct->x[0]    = x[0];
  ct->x[1]    = x[1];
  ct->x[2]    = x[2];
  v[0]        = x[0]- triangle->vertex[0][0];
  v[1]        = x[1]- triangle->vertex[0][1];
  v[2]        = x[2]- triangle->vertex[0][2];
  ct->h  =             triangle->normal[0]*v[0];
  ct->h  =   ct->h  +  triangle->normal[1]*v[1];
  ct->h  =   ct->h  +  triangle->normal[2]*v[2];
  /*  y = projection de x sur le triangle...*/
  ct->xt[0] = x[0] -  ct->h*triangle->normal[0];
  ct->xt[1] = x[1] -  ct->h*triangle->normal[1];
  ct->xt[1] = x[2] -  ct->h*triangle->normal[2];

  for ( a=0; a <3; a++)
    {
      s1 = (a+1) % 3 ;
      s2 = (a+2) % 3 ;
      // je calcule les coordonnees de v selon la normale a l'arete
      d  =    (x[0]-triangle->vertex[s1][0])*triangle->nu[a][0];
      d  = d +(x[1]-triangle->vertex[s1][1])*triangle->nu[a][1];
      d  = d +(x[2]-triangle->vertex[s1][2])*triangle->nu[a][2];
      // coordonnees des sommets 1][ 2 en choisissant la coordonnee z(:) comme origine...
      t1 =       (x[0]-triangle->vertex[s1][0])*triangle->tau[a][0];
      t1 = t1 +  (x[1]-triangle->vertex[s1][1])*triangle->tau[a][1];
      t1 = t1 +  (x[2]-triangle->vertex[s1][2])*triangle->tau[a][2];
      t2 =       (x[0]-triangle->vertex[s2][0])*triangle->tau[a][0];
      t2 = t2 +  (x[1]-triangle->vertex[s2][1])*triangle->tau[a][1];
      t2 = t2 +  (x[2]-triangle->vertex[s2][2])*triangle->tau[a][2];
      ct->d[a]  = d;
      ct->t1[a] = t1;
      ct->t2[a] = t2;
    }
  return(ct);
}

real_t solid_angle(real_t xl[3], real_t ck[3][3] )
{
  real_t r[3][3];
  real_t  vmx[3];
  real_t  uxv[3];
  real_t    u[3];
  real_t    v[3];
  real_t sa, x, y, usn, s12, s01, s02;
  number_t k;
  // Formule (50)-(51)-(52) du papier Hanninen et al. qui donne la valeur absolue de l'angle solide
  for( k=0; k < 3; k++)
    {
      vmx[0] = ck[k][0]-xl[0];
      vmx[1] = ck[k][1]-xl[1];
      vmx[2] = ck[k][2]-xl[2];
      usn    = 1/std::sqrt(  vmx[0]* vmx[0] +vmx[1]*vmx[1] + vmx[2]*vmx[2] );
      r[k][0] = vmx[0]*usn;
      r[k][1] = vmx[1]*usn;
      r[k][2] = vmx[2]*usn;
    }
  s12 = r[1][0]*r[2][0] +r[1][1]*r[2][1] + r[1][2]*r[2][2];
  s02 = r[0][0]*r[2][0] +r[0][1]*r[2][1] + r[0][2]*r[2][2];
  s01 = r[0][0]*r[1][0] +r[0][1]*r[1][1] + r[0][2]*r[1][2];
  x   = 1 + s01 + s02 + s12;
  u[0] = r[1][0]- r[0][0];
  u[1] = r[1][1]- r[0][1];
  u[2] = r[1][2]- r[0][2];
  v[0] = r[2][0]- r[0][0];
  v[1] = r[2][1]- r[0][1];
  v[2] = r[2][2]- r[0][2];
  uxv[0] = u[1]*v[2]-u[2]*v[1];
  uxv[1] = u[2]*v[0]-u[0]*v[2];
  uxv[2] = u[0]*v[1]-u[1]*v[0];
  y  = r[0][0]* uxv[0]+r[0][1]* uxv[1]+r[0][2]* uxv[2];
  if( y<0) { y=-y;}
  sa = 2*std::atan2(y,x);
  return sa ;
}

void ElemTools_hanninen_t(Mytriangle_t *triangle, real_t x[3], real_t integrale[12])
{
  /*
    calcule a l'aide des formules données par Hanninen et al. de
    integrale(0) = \frac{1}{|t|}\int_t  |x-y|^{-1} dy
    integrale(4) = \frac{1}{|t|}\int_t  |x-y|      dy
    integrale(8) = \frac{1}{|t|}\int_t  |x-y|^3    dy
    puis de leurs gradients par rapport a x:
    integrale(1:3)  =-\frac{1}{|t|}\int_t  (x-y)/|x-y|^3 dy
    integrale(5:7)  = \frac{1}{|t|}\int_t  (x-y)/|x-y|   dy
    integrale(8:11) = \frac{1}{|t|}\int_t  (x-y) |x-y|   dy */
  real_t  int_edg[3][3];
  number_t a, v, vm, vp;
  real_t R[3]; real_t xmv[3];
  real_t Rm, Rp, R02, h2, unsuraire;
  real_t ints_tri[4];
  real_t intv_tri[3][3];
  real_t tiers =1.e0/3.e0, cinquieme =1.e0/5.e0;
  Myct_t* ct;

  ct = ElemTools_triangle_x(x, triangle);
  for( v=0; v<3; v++)
    {
      xmv[0] = x[0]-triangle->vertex[v][0];
      xmv[1] = x[1]-triangle->vertex[v][1];
      xmv[2] = x[2]-triangle->vertex[v][2];
      R[v]   = std::sqrt( xmv[0]*xmv[0]+ xmv[1]*xmv[1]+xmv[2]*xmv[2]);
    }
  // Je commence par calculer mes int�grales d'ar�tes
  for( a=0; a<3; a++)
    {
      Rm = R[ ((a+1) % 3) ];
      Rp = R[ ((a+2) % 3) ];

      R02 =  ct->h*ct->h+  ct->d[a]*ct->d[a];

      if( std::abs( Rm+ct->t1[a]) >  std::abs( Rp-ct->t2[a]))
        {int_edg[a][0] = std::log( std::abs( (Rp+ct->t2[a])/(Rm+ct->t1[a]) ));}
      else
        {int_edg[a][0] = std::log( std::abs( (Rm-ct->t1[a] )/( Rp-ct->t2[a]) ));}
      // formule (58) du papier avec p=1
      int_edg[a][1] =  0.5e0*(R02* int_edg[a][0]+(ct->t2[a]*Rp-ct->t1[a]*Rm));
      // et p=3
      int_edg[a][2] = 0.25e0*(3*R02*int_edg[a][1]+(ct->t2[a]*Rp*Rp*Rp-ct->t1[a]*Rm*Rm*Rm));
    }
  /* puis mes intégrales de surface scalaires int_T |x-y|^q dT(y)
     q= -3... */
  if( std::abs(ct->h)  ==0 ) {  ints_tri[0] =  0;}
  else                       {  ints_tri[0] =  solid_angle(x,triangle->vertex)/std::abs(ct->h); }
  /* je prends en compte formule (63)
     q= -1...*/
  h2 = ct->h*ct->h ;
  ints_tri[1] =           (    -h2*ints_tri[0] + ct->d[0]*int_edg[0][0] + ct->d[1]*int_edg[1][0]+ ct->d[2]*int_edg[2][0]);
  /* q=  1...*/
  ints_tri[2] =     tiers*(     h2*ints_tri[1] + ct->d[0]*int_edg[0][1] + ct->d[1]*int_edg[1][1]+ ct->d[2]*int_edg[2][1]);
  /*   ! q=  3...*/
  ints_tri[3] = cinquieme*(3.e0*h2*ints_tri[2] + ct->d[0]*int_edg[0][2] + ct->d[1]*int_edg[1][2]+ ct->d[2]*int_edg[2][2]);
  /* je remplis mes quantités scalaires...*/
  unsuraire    = 1.e0/triangle->area;
  integrale[0] = unsuraire*ints_tri[1] ;
  integrale[4] = unsuraire*ints_tri[2] ;
  integrale[8] = unsuraire*ints_tri[3];
  /* je passe au vectoriel...
     j'utilise la formule (74)...
     q=-1... */
  intv_tri[0][0] =  -ct->h*triangle->normal[0]*ints_tri[0]  ;
  intv_tri[0][0] +=      triangle->nu[0][0]*int_edg[0][0]    ;
  intv_tri[0][0] +=      triangle->nu[1][0]*int_edg[1][0];
  intv_tri[0][0] +=      triangle->nu[2][0]*int_edg[2][0];
  intv_tri[0][1] = -ct->h*triangle->normal[1]*ints_tri[0]  ;
  intv_tri[0][1] +=      triangle->nu[0][1]*int_edg[0][0]   ;
  intv_tri[0][1] +=      triangle->nu[1][1]*int_edg[1][0]   ;
  intv_tri[0][1] +=      triangle->nu[2][1]*int_edg[2][0]   ;
  intv_tri[0][2] = -ct->h*triangle->normal[2]*ints_tri[0]  ;
  intv_tri[0][2] +=      triangle->nu[0][2]*int_edg[0][0]   ;
  intv_tri[0][2] +=      triangle->nu[1][2]*int_edg[1][0]   ;
  intv_tri[0][2] +=      triangle->nu[2][2]*int_edg[2][0]   ;
  /* q= 1...*/
  intv_tri[1][0]  = -ct->h*triangle->normal[0]*ints_tri[1]  ;
  intv_tri[1][0] -=      triangle->nu[0][0]*int_edg[0][1]   ;
  intv_tri[1][0] -=      triangle->nu[1][0]*int_edg[1][1]   ;
  intv_tri[1][0] -=      triangle->nu[2][0]*int_edg[2][1]   ;
  intv_tri[1][1]  = -ct->h*triangle->normal[1]*ints_tri[1]  ;
  intv_tri[1][1] -=      triangle->nu[0][1]*int_edg[0][1]   ;
  intv_tri[1][1] -=      triangle->nu[1][1]*int_edg[1][1]   ;
  intv_tri[1][1] -=      triangle->nu[2][1]*int_edg[2][1];
  intv_tri[1][2]  = -ct->h*triangle->normal[2]*ints_tri[1] ;
  intv_tri[1][2] -=      triangle->nu[0][2]*int_edg[0][1];
  intv_tri[1][2] -=      triangle->nu[1][2]*int_edg[1][1]  ;
  intv_tri[1][2] -=     triangle->nu[2][2]*int_edg[2][1];

  /*  q= 3....*/
  intv_tri[2][0]  = -3*ct->h*triangle->normal[0]*ints_tri[2];
  intv_tri[2][0] -=         triangle->nu[0][0]*int_edg[0][2];
  intv_tri[2][0] -=         triangle->nu[1][0]*int_edg[1][2];
  intv_tri[2][0] -=         triangle->nu[2][0]*int_edg[2][2];
  intv_tri[2][1]  = -3*ct->h*triangle->normal[1]*ints_tri[2] ;
  intv_tri[2][1] -=         triangle->nu[0][1]*int_edg[0][2];
  intv_tri[2][1] -=         triangle->nu[1][1]*int_edg[1][2];
  intv_tri[2][1] -=         triangle->nu[2][1]*int_edg[2][2];
  intv_tri[2][2]  = -3*ct->h*triangle->normal[2]*ints_tri[2] ;
  intv_tri[2][2] -=         triangle->nu[0][2]*int_edg[0][2];          ;
  intv_tri[2][2] -=         triangle->nu[1][2]*int_edg[1][2];         ;
  intv_tri[2][2] -=        triangle->nu[2][2]*int_edg[2][2];

  /* et je remplis mes composantes vectorielles*/
  integrale[1] = unsuraire*intv_tri[0][0];
  integrale[2] = unsuraire*intv_tri[0][1];
  integrale[3] = unsuraire*intv_tri[0][2];
  integrale[5] = -unsuraire*intv_tri[1][0];
  integrale[6] = -unsuraire*intv_tri[1][1];
  integrale[7] = -unsuraire*intv_tri[1][2];
  integrale[ 9] = -(unsuraire/3)*intv_tri[2][0];
  integrale[10] = -(unsuraire/3)*intv_tri[2][1];
  integrale[11] = -(unsuraire/3)*intv_tri[2][2];
  free(ct);
}

void ElemTools_hanninen_step3(real_t ka, real_t ck[3][3], real_t cl[3][3], real_t int_l[10], real_t int_k[6], real_t s[3], complex_t zts[3][3], complex_t zks[3][3], number_t cf)
{
  real_t zta[3][3];
  real_t zka[3][3];
  real_t vg, int_t189;
  real_t              wg[3];
  real_t             smv[3];
  int               indx[3];
  number_t i, j, iv, jv;

  if(cf==1 || cf==3)
    {
      int_t189   = int_l[1] + int_l[8] + int_l[9] ;
      for(i=0; i<3; i++)
        {
          vg =  int_l[5]*(s[0]-cl[i][0]) +  int_l[6]*(s[1]-cl[i][1]) + int_l[7]*(s[2]-cl[i][2]) + int_t189 ;
          for(j=0; j<3; j++)
            {
              zta[i][j]=((s[0]-cl[i][0])* (s[0]-ck[j][0]) +
                         (s[1]-cl[i][1])* (s[1]-ck[j][1]) +
                         (s[2]-cl[i][2])* (s[2]-ck[j][2]) )*int_l[0]+
                        int_l[2]* (2*s[0]-cl[i][0]-ck[j][0]) +
                        int_l[3]* (2*s[1]-cl[i][1]-ck[j][1]) +
                        int_l[4]* (2*s[2]-cl[i][2]-ck[j][2]) +
                        vg;

            }
        }
    }

  if(cf>=2)
    {
      for(i=0; i<3; i++)
        {
          smv[0] = s[0]-cl[i][0];
          smv[1] = s[1]-cl[i][1];
          smv[2] = s[2]-cl[i][2];

          wg[0] =  (smv[1]*int_k[2]-smv[2]*int_k[1] ) + int_k[3] ;
          wg[1] =  (smv[2]*int_k[0]-smv[0]*int_k[2] ) + int_k[4] ;
          wg[2] =  (smv[0]*int_k[1]-smv[1]*int_k[0] ) + int_k[5] ;
          for(j=0; j<3; j++)
            {
              zka[i][j]= wg[0]*(cl[i][0]-ck[j][0]) + wg[1]*(cl[i][1]-ck[j][1]) + wg[2]*(cl[i][2]-ck[j][2]) ;
            }
        }
    }

  indx[0] = 0; indx[1] = 1; indx[2] = 2;
  for(i=0; i<3; i++)
    {
      iv= indx[i];
      for(j=0; j<3; j++)
        {
          jv = indx[j];
          if(cf==1 || cf==3) zts[i][j] = ka*zta[iv][jv]/(16*acos(-1.e0));
          if(cf>=2) zks[i][j] =    zka[iv][jv]/(16*acos(-1.e0));
        }
    }
}

void ElemTools_hanninen_step3c(real_t kat[2], real_t ck[3][3], real_t cl[3][3], complex_t int_l[10], complex_t int_k[6], real_t  s[3], complex_t zts[3][3], complex_t zks[3][3], number_t cf)
{
  complex_t zta[3][3];
  complex_t zka[3][3];
  complex_t vg, int_t189;
  complex_t wg[3];
  complex_t ka;
  real_t  smv[3];
  number_t indx[3];
  number_t i, j, iv, jv;

  ka = kat[0]+ i_* kat[1];
  if(cf==1 || cf==3)
    {
      int_t189   = int_l[1] + int_l[8] + int_l[9] ;
      for(i=0; i<3; i++)
        {
          vg =  int_l[5]*(s[0]-cl[i][0]) +  int_l[6]*(s[1]-cl[i][1]) + int_l[7]*(s[2]-cl[i][2]) + int_t189 ;
          for(j=0; j<3; j++)
            {
              zta[i][j]=((s[0]-cl[i][0])* (s[0]-ck[j][0]) +
                         (s[1]-cl[i][1])* (s[1]-ck[j][1]) +
                         (s[2]-cl[i][2])* (s[2]-ck[j][2]) )*int_l[0]+
                        int_l[2]* (2*s[0]-cl[i][0]-ck[j][0]) +
                        int_l[3]* (2*s[1]-cl[i][1]-ck[j][1]) +
                        int_l[4]* (2*s[2]-cl[i][2]-ck[j][2]) +
                        vg;

            }
        }
    }

  if(cf>=2)
    {
      for(i=0; i<3; i++)
        {
          smv[0] = s[0]-cl[i][0];
          smv[1] = s[1]-cl[i][1];
          smv[2] = s[2]-cl[i][2];

          wg[0] =  (smv[1]*int_k[2]-smv[2]*int_k[1] ) + int_k[3] ;
          wg[1] =  (smv[2]*int_k[0]-smv[0]*int_k[2] ) + int_k[4] ;
          wg[2] =  (smv[0]*int_k[1]-smv[1]*int_k[0] ) + int_k[5] ;
          for(j=0; j<3; j++)
            {
              zka[i][j]= wg[0]*(cl[i][0]-ck[j][0]) + wg[1]*(cl[i][1]-ck[j][1]) + wg[2]*(cl[i][2]-ck[j][2]) ;
            }
        }
    }

  indx[0] = 0; indx[1] = 1; indx[2] = 2;
  for(i=0; i<3; i++)
    {
      iv= indx[i];
      for(j=0; j<3; j++)
        {
          jv = indx[j];
          if(cf==1 || cf==3) zts[i][j] = ka*zta[iv][jv]/(16*acos(-1.e0));
          if(cf>=2)          zks[i][j] = zka[iv][jv]/(16*acos(-1.e0));
        }
    }
}

void ElemTools_hanninen_step2(real_t si[7], real_t sj[8][3], real_t ka, real_t int_l[10], real_t int_k[6])
{
  real_t  ck = ka*ka/2;
  int_l[0] = si[0]   - ck* si[1] ;
  int_l[1] = si[3]   - ck* si[4];
  int_l[2] = sj[3][0] - ck* sj[4][0];
  int_l[3] = sj[3][1] - ck* sj[4][1];
  int_l[4] = sj[3][2] - ck* sj[4][2];
  int_l[5] = sj[1][0] - ck* sj[2][0];
  int_l[6] = sj[1][1] - ck* sj[2][1];
  int_l[7] = sj[1][2] - ck* sj[2][2];
  int_l[8] = si[5]   - ck* si[6];
  int_l[9] = -2*(si[0]/ck - si[1] + si[2]*ck/6);
  int_k[0] = -sj[0][0] - ck* (sj[1][0] - ck*sj[2][0]/2);
  int_k[1] = -sj[0][1] - ck* (sj[1][1] - ck*sj[2][1]/2);
  int_k[2] = -sj[0][2] - ck* (sj[1][2] - ck*sj[2][2]/2);
  int_k[3] = sj[5][0]  - ck* (sj[6][0] - ck*sj[7][0]/2);
  int_k[4] = sj[5][1]  - ck* (sj[6][1] - ck*sj[7][1]/2);
  int_k[5] = sj[5][2]  - ck* (sj[6][2] - ck*sj[7][2]/2);
}

void ElemTools_hanninen_step2c(real_t si[7], real_t sj[8][3], real_t kat[2], complex_t int_l[10], complex_t int_k[6])
{
  complex_t ka = kat[0]+i_* kat[1], ck = ka*ka/2;
  int_l[0] = si[0]   - ck* si[1] ;
  int_l[1] = si[3]   - ck* si[4];
  int_l[2] = sj[3][0] - ck* sj[4][0];
  int_l[3] = sj[3][1] - ck* sj[4][1];
  int_l[4] = sj[3][2] - ck* sj[4][2];
  int_l[5] = sj[1][0] - ck* sj[2][0];
  int_l[6] = sj[1][1] - ck* sj[2][1];
  int_l[7] = sj[1][2] - ck* sj[2][2];
  int_l[8] = si[5]   - ck* si[6];
  int_l[9] = -2*(si[0]/ck - si[1] + si[2]*ck/6);
  int_k[0] = -sj[0][0] - ck* (sj[1][0] - ck*sj[2][0]/2);
  int_k[1] = -sj[0][1] - ck* (sj[1][1] - ck*sj[2][1]/2);
  int_k[2] = -sj[0][2] - ck* (sj[1][2] - ck*sj[2][2]/2);
  int_k[3] = sj[5][0]  - ck* (sj[6][0] - ck*sj[7][0]/2);
  int_k[4] = sj[5][1]  - ck* (sj[6][1] - ck*sj[7][1]/2);
  int_k[5] = sj[5][2]  - ck* (sj[6][2] - ck*sj[7][2]/2);
}

void ElemTools_hanninen_step1(real_t ck[3][3],real_t  cl[3][3], real_t s[3], Myquad_t *quad, real_t si[7], real_t sj[8][3])
{
  /*real_t:: ck(0:2,0:2)
    real_t:: cl(0:2,0:2)
    number_t:: npg, npgs
    real_t::  gauss(0:npg-1,0:3)
    real_t:: sgauss(0:npgs-1,0:2)
    real_t:: s(0:2)
    real_t:: si(0:6), sj(0:7, 0:2)     */

  Mytriangle_t*  trik;
  real_t integrale[12];
  real_t integrale_t[12];
  real_t x[3];
  real_t xms[3];
  real_t  sc;
  real_t  wk[3];
  bool test = false;
  number_t gl, i, j, n;

  trik = ElemTools_triangle_c(ck);
  for(i=0; i<7; i++) {si[i] = 0;};
  for(i=0; i<8; i++) {sj[i][0] = 0; sj[i][1] = 0; sj[i][2] = 0;  };
  for(gl=0; gl< quad->ngs; gl++)
    {
      if( test) {;}
      x[0]=     cl[0][0]                   +
                quad->as[gl]*( cl[1][0]-  cl[0][0])+
                quad->bs[gl]*( cl[2][0]-  cl[0][0]);
      x[1]=     cl[0][1]                   +
                quad->as[gl]*( cl[1][1]-  cl[0][1])+
                quad->bs[gl]*( cl[2][1]-  cl[0][1]);
      x[2]=     cl[0][2]                   +
                quad->as[gl]*( cl[1][2]-  cl[0][2])+
                quad->bs[gl]*( cl[2][2]-  cl[0][2]);

      ElemTools_hanninen_t(trik, x, integrale);
      /*printf("x012 %e %e %e \n", integrale[0] , integrale[1], integrale[2] );
      printf("x345 %e %e %e \n", integrale[3] , integrale[4], integrale[5] );
      printf("x678 %e %e %e \n", integrale[6] , integrale[7], integrale[8] );
      printf("x901 %e %e %e \n", integrale[9] , integrale[10], integrale[11] );
      */

      /*if( test )
      {
          c_hanninen_b(integrale_t,trik, x,gauss, npg)
            for(i=0,i<12;i++)
        { write(6,*) i,integrale(i)
      write(6,*) i,integrale_t(i)}
        }*/

      for(i=0; i<12; i++) integrale[i] = quad->ws[gl]*integrale[i];
      xms[0]       =   x[0]-s[0] ;
      xms[1]       =   x[1]-s[1] ;
      xms[2]       =   x[2]-s[2] ;
      si[0]     =   si[0] + integrale[0];
      si[1]     =   si[1] + integrale[4];
      si[2]     =   si[2] + integrale[8];
      sc        = (xms[0]*xms[0] +xms[1]*xms[1]+xms[2]*xms[2]);
      si[3]     =   si[3] + sc*integrale[0];
      si[4]     =   si[4] + sc*integrale[4];
      sc        = integrale[5]*xms[0]+integrale[6]*xms[1]+integrale[7]*xms[2];
      si[5]     =   si[5] - sc;
      sc        = integrale[9]*xms[0]+integrale[10]*xms[1]+integrale[11]*xms[2];
      si[6]     =   si[6] - sc ;
      sj[0][0]   =  sj[0][0] + integrale[1];
      sj[0][1]   =  sj[0][1] + integrale[2];
      sj[0][2]   =  sj[0][2] + integrale[3];
      sj[1][0]   =  sj[1][0] - integrale[5];
      sj[1][1]   =  sj[1][1] - integrale[6];
      sj[1][2]   =  sj[1][2] - integrale[7];
      sj[2][0]   =  sj[2][0] - integrale[9];
      sj[2][1]   =  sj[2][1] - integrale[10];
      sj[2][2]   =  sj[2][2] - integrale[11];
      sj[3][0]   =  sj[3][0] + xms[0]*integrale[0];
      sj[3][1]   =  sj[3][1] + xms[1]*integrale[0];
      sj[3][2]   =  sj[3][2] + xms[2]*integrale[0];
      sj[4][0]   =  sj[4][0] + xms[0]*integrale[4];
      sj[4][1]   =  sj[4][1] + xms[1]*integrale[4];
      sj[4][2]   =  sj[4][2] + xms[2]*integrale[4];
      wk[0] =  integrale[3]*xms[1] -  integrale[2]*xms[2];
      wk[1] =  integrale[1]*xms[2] -  integrale[3]*xms[0];
      wk[2] =  integrale[2]*xms[0] -  integrale[1]*xms[1];
      sj[5][0]   =  sj[5][0] -  wk[0];
      sj[5][1]   =  sj[5][1] -  wk[1];
      sj[5][2]   =  sj[5][2] -  wk[2];
      wk[0] =  integrale[7]*xms[1] -  integrale[6]*xms[2];
      wk[1] =  integrale[5]*xms[2] -  integrale[7]*xms[0];
      wk[2] =  integrale[6]*xms[0] -  integrale[5]*xms[1];
      sj[6][0]   =  sj[6][0] -  wk[0];
      sj[6][1]   =  sj[6][1] -  wk[1];
      sj[6][2]   =  sj[6][2] -  wk[2];
      wk[0] =  integrale[11]*xms[1] -  integrale[10]*xms[2];
      wk[1] =  integrale[ 9]*xms[2] -  integrale[11]*xms[0];
      wk[2] =  integrale[10]*xms[0] -  integrale[ 9]*xms[1];
      sj[7][0]   =  sj[7][0] -  wk[0];
      sj[7][1]   =  sj[7][1] -  wk[1];
      sj[7][2]   =  sj[7][2] -  wk[2];
    }
}

void ElemTools_hanninen_step23(real_t ka[2], real_t ck[3][3], real_t cl[3][3], real_t s[3], real_t si[7], real_t sj[8][3], complex_t zts[3][3], complex_t zks[3][3], number_t cf)
{
  real_t int_l[10]; real_t int_k[6];
  complex_t zint_t[10]; complex_t zint_k[6];
  real_t kareal;
  if( ka[1]==0.e0)
    {
      kareal=  ka[0];
      ElemTools_hanninen_step2(si,sj,kareal,int_l,int_k);
      ElemTools_hanninen_step3(kareal,ck,cl,int_l,int_k,s,zts,zks,cf);
    }
  else
    {
      ElemTools_hanninen_step2c(si,sj,ka,zint_t,zint_k);
      ElemTools_hanninen_step3c(ka,ck,cl,zint_t,zint_k,s,zts,zks,cf);
    }
}

void ElemTools_singular(real_t ck[3][3],real_t cl[3][3], real_t ka[2], Myquad_t *quad, complex_t zt[3][3],complex_t zk[3][3], number_t cf)
{
  real_t        si[7];
  real_t     sj[8][3];
  real_t         s[3];
  complex_t zts[3][3];
  complex_t zks[3][3];
  complex_t ztr[3][3];
  complex_t zkr[3][3];
  real_t       kareal;
  number_t i, j, iv, jv;

  if(  ka[1]==0.e0 )
    {
      kareal = ka[0];
      ElemTools_elem_r(ck,cl,kareal, quad, ztr, zkr, cf);
    }
  else
    {
      ElemTools_elem_rc(ck,cl,ka, quad, ztr, zkr, cf);
    }
  /*printf(" npgf %d  %e \n", quad->ng, kareal );*/
  /*for(i=0;i<3;i++){ for(j=0;j<3;j++)
       { printf("i j %d %d \n", i,j );
         printf("matrice T  i j %e %e \n", creal( ztr[i][j]) ,cimag( ztr[i][j]) );
         printf("matrice K  i j %e %e \n", creal( zkr[i][j]) ,cimag( zkr[i][j]) );}}   exit(-1);*/

  s[0] = cl[0][0];
  s[1] = cl[0][1];
  s[2] = cl[0][2];

  /* Calcul de si et sj...*/
  ElemTools_hanninen_step1(ck, cl, s, quad, si, sj);

  /*for( i=0; i < 7; i++){ printf("si  %e  \n", si[i] );}
  for( i=0; i < 8; i++){ printf("sj %d %e %e %e \n", i, sj[i][0],sj[i][1], sj[i][2] );}
  exit(-1);*/

  // Calcul de int_t et int_k...,   En fait ai calculé la transposée; c'est pas grave ...
  ElemTools_hanninen_step23(ka, ck, cl,s, si, sj, zts, zks, cf);

  // num[dof] =  opposite vertex number of the edge associated to the dof
  number_t num[3] ;
  num[0] = 2; num[1] = 0; num[2] = 1;
  for( iv=0; iv < 3; iv++)
    {
      for( jv=0; jv < 3; jv++)
        {
          i = num[iv]; j = num[jv];
          if(cf>=2) zk[iv][jv]=zkr[j][i]+zks[j][i];
          if(cf==1 || cf==3) zt[iv][jv]=ztr[j][i]+zts[j][i];
        }
    }
}

void ds(real_t u[7], real_t s[8][3],real_t v[7], real_t t[8][3])
{
  number_t i; real_t d; real_t x[3];
  for(i=0; i<7; i++)
    {
      d = std::abs((u[i]-v[i])/u[i]);
      printf( "erreur si  %lu %e \n", i, d);
    }
  for(i=0; i<8; i++)
    {
      x[0] = s[i][0]-t[i][0];
      x[1] = s[i][1]-t[i][1];
      x[2] = s[i][2]-t[i][2];
      d = std::sqrt( x[0]*x[0]+  x[1]*x[1]+  x[2]*x[2] );
      d = d/ std::sqrt( s[i][0]*s[i][0]+  s[i][1]* s[i][1]+  s[i][2]*s[i][2]);
      printf("erreur sj %lu %e \n", i, d);
    };
}

void E_lenoir_printv(real_t s[3])
{
  printf(" ck %e %e %e  \n",s[0], s[1], s[2]);
}
void E_lenoir_printt(real_t ck[3][3], real_t cl[3][3])
{
  printf(" ck %e %e %e  \n",ck[0][0], ck[0][1], ck[0][2]);
  printf(" ck %e %e %e  \n",ck[1][0], ck[1][1], ck[1][2]);
  printf(" ck %e %e %e  \n",ck[2][0], ck[2][1], ck[2][2]);
  printf(" cl %e %e %e  \n",cl[0][0], cl[0][1], cl[0][2]);
  printf(" cl %e %e %e  \n",cl[1][0], cl[1][1], cl[1][2]);
  printf(" cl %e %e %e  \n",cl[2][0], cl[2][1], cl[2][2]);
}

void E_lenoir_prints(real_t si[7], real_t sj[8][3])
{
  for(number_t i=0; i<7; i++)
    {
      printf("i, si[i] %lu %e  \n ", i, si[i]);
    }
  for(number_t i=0; i<8; i++)
    {
      printf("i, sj[i] %lu %e %e %e \n ", i, sj[i][0], sj[i][1], sj[i][2] );
    }
}
void E_lenoir_printi(real_t v[12])
{
  for(number_t i=0; i<12; i++)
    {
      printf("i, integrale[i] %lu %e  \n ", i, v[i]);
    }
}

void E_lenoir_vstep1(real_t sik[7], real_t sjk[8][3], real_t sil[7], real_t sjl[8][3], real_t si[7], real_t sj[8][3])
{
  si[0]  =  sik[0]+ sil[0];
  si[1]  =  sik[1]+ sil[1];
  si[2]  =  sik[2]+ sil[2];
  si[3]  =  sik[3]+ sil[3]+2*sil[5]+sil[1];
  si[4]  =  sik[4]+ sil[4]+2*sil[6]+sil[2];
  si[5]  =  sik[5]- sil[5]-sil[1];
  si[6]  =  sik[6]- sil[6]-sil[2];
  sj[0][0] = sjk[0][0]- sjl[0][0];
  sj[0][1] = sjk[0][1]- sjl[0][1];
  sj[0][2] = sjk[0][2]- sjl[0][2];
  sj[1][0] = sjk[1][0]- sjl[1][0];
  sj[1][1] = sjk[1][1]- sjl[1][1];
  sj[1][2] = sjk[1][2]- sjl[1][2];
  sj[2][0] = sjk[2][0]- sjl[2][0];
  sj[2][1] = sjk[2][1]- sjl[2][1];
  sj[2][2] = sjk[2][2]- sjl[2][2];
  sj[3][0] = sjk[3][0]+ sjl[3][0] +  sjl[1][0];
  sj[3][1] = sjk[3][1]+ sjl[3][1] +  sjl[1][1];
  sj[3][2] = sjk[3][2]+ sjl[3][2] +  sjl[1][2];
  sj[4][0] = sjk[4][0]+ sjl[4][0] +  sjl[2][0];
  sj[4][1] = sjk[4][1]+ sjl[4][1] +  sjl[2][1];
  sj[4][2] = sjk[4][2]+ sjl[4][2] +  sjl[2][2];
  sj[5][0] = sjk[5][0]- sjl[5][0];
  sj[5][1] = sjk[5][1]- sjl[5][1];
  sj[5][2] = sjk[5][2]- sjl[5][2];
  sj[6][0] = sjk[6][0]- sjl[6][0];
  sj[6][1] = sjk[6][1]- sjl[6][1];
  sj[6][2] = sjk[6][2]- sjl[6][2];
  sj[7][0] = sjk[7][0]- sjl[7][0];
  sj[7][1] = sjk[7][1]- sjl[7][1];
  sj[7][2] = sjk[7][2]- sjl[7][2];
}


void E_lenoir_vstep0(Mytriangle_t *trik, Mytriangle_t *tril, real_t s[3], Myquad_t *quad, real_t si[7], real_t sj[8][3])
{
  real_t   integrale[12]; real_t   integrale_n[12]; real_t   wk[3];
  real_t            x[3]; real_t             ab[3]; real_t  xms[3];
  real_t sc;
  number_t gl, i;
  for(i=0; i<7; i++) {si[i] = 0;};
  for(i=0; i<8; i++) {sj[i][0] = 0; sj[i][1] = 0; sj[i][2] = 0;};
  ab[0] = tril->vertex[2][0]-tril->vertex[1][0];
  ab[1] = tril->vertex[2][1]-tril->vertex[1][1];
  ab[2] = tril->vertex[2][2]-tril->vertex[1][2];
  for( gl=0; gl< quad->ngo; gl++)
    {
      x[0] =  tril->vertex[1][0]+ quad->ao[gl]*ab[0];
      x[1] =  tril->vertex[1][1]+ quad->ao[gl]*ab[1];
      x[2] =  tril->vertex[1][2]+ quad->ao[gl]*ab[2];

      ElemTools_hanninen_t(trik,x,integrale);
      /*E_lenoir_printi(integrale);*/
      for(i=0; i<12; i++) {integrale[i] = 2*quad->wo[gl]*integrale[i];};
      si[0]     =   si[0] + integrale[0]/3;
      xms[0]    =   x[0]-s[0];
      xms[1]    =   x[1]-s[1];
      xms[2]    =   x[2]-s[2];
      si[1]     =   si[1] + integrale[4]/5;
      si[2]     =   si[2] + integrale[8]/7;
      sc        = xms[0]* xms[0] +xms[1]*xms[1]+xms[2]*xms[2];
      si[3]     =   si[3] + sc*integrale[0]/5;
      si[4]     =   si[4] + sc*integrale[4]/7;
      sc        = integrale[5]*xms[0]+integrale[6]*xms[1]+integrale[7]*xms[2];
      si[5]     =   si[5] - sc/5;
      sc        = integrale[9]*xms[0]+integrale[10]*xms[1]+integrale[11]*xms[2];
      si[6]     =   si[6] - sc/7;
      sj[0][0]   =  sj[0][0] + integrale[1]/2;
      sj[0][1]   =  sj[0][1] + integrale[2]/2;
      sj[0][2]   =  sj[0][2] + integrale[3]/2;
      sj[1][0]   =  sj[1][0] - integrale[5]/4;
      sj[1][1]   =  sj[1][1] - integrale[6]/4;
      sj[1][2]   =  sj[1][2] - integrale[7]/4;
      sj[2][0]   =  sj[2][0] - integrale[ 9]/6;
      sj[2][1]   =  sj[2][1] - integrale[10]/6;
      sj[2][2]   =  sj[2][2] - integrale[11]/6;
      sj[3][0]   =  sj[3][0] + xms[0]*integrale[0]/4;
      sj[3][1]   =  sj[3][1] + xms[1]*integrale[0]/4;
      sj[3][2]   =  sj[3][2] + xms[2]*integrale[0]/4;
      sj[4][0]   =  sj[4][0] + xms[0]*integrale[4]/6;
      sj[4][1]   =  sj[4][1] + xms[1]*integrale[4]/6;
      sj[4][2]   =  sj[4][2] + xms[2]*integrale[4]/6;
      wk[0] =  integrale[3]*xms[1] -  integrale[2]*xms[2];
      wk[1] =  integrale[1]*xms[2] -  integrale[3]*xms[0];
      wk[2] =  integrale[2]*xms[0] -  integrale[1]*xms[1];
      sj[5][0]   =  sj[5][0] -  wk[0]/3;
      sj[5][1]   =  sj[5][1] -  wk[1]/3;
      sj[5][2]   =  sj[5][2] -  wk[2]/3;
      wk[0] =  integrale[7]*xms[1] -  integrale[6]*xms[2];
      wk[1] =  integrale[5]*xms[2] -  integrale[7]*xms[0];
      wk[2] =  integrale[6]*xms[0] -  integrale[5]*xms[1];
      sj[6][0]   =  sj[6][0] -  wk[0]/5;
      sj[6][1]   =  sj[6][1] -  wk[1]/5;
      sj[6][2]   =  sj[6][2] -  wk[2]/5;
      wk[0] =  integrale[11]*xms[1] -  integrale[10]*xms[2];
      wk[1] =  integrale[ 9]*xms[2] -  integrale[11]*xms[0];
      wk[2] =  integrale[10]*xms[0] -  integrale[ 9]*xms[1];
      sj[7][0]   =  sj[7][0] -  wk[0]/7;
      sj[7][1]   =  sj[7][1] -  wk[1]/7;
      sj[7][2]   =  sj[7][2] -  wk[2]/7;
    }
}

void  E_lenoir_integr_edge(Mytriangle_t *tri, real_t int_edge[3][12] )
{
  number_t a, am, ap, i;
  real_t        w[3];
  real_t        z[3];
  real_t sc, rm3, rp3, sm, sp, r02, rm, rp, unsurlong;
  real_t Ik[12];
  for( a=0; a<3; a++)
    {
      am = ((a+1) % 3);
      ap = ((a+2) % 3);
      w[0] =  tri->vertex[a][0]-tri->vertex[am][0];
      w[1] =  tri->vertex[a][1]-tri->vertex[am][1];
      w[2] =  tri->vertex[a][2]-tri->vertex[am][2];
      sc = w[0]*tri->tau[a][0]+ w[1]*tri->tau[a][1]+ w[2]*tri->tau[a][2];
      z[0] = w[0] - sc*tri->tau[a][0];
      z[1] = w[1] - sc*tri->tau[a][1];
      z[2] = w[2] - sc*tri->tau[a][2];
      r02 = z[0]*z[0]+ z[1]*z[1]+ z[2]* z[2];
      rm = tri->ledge[ap] ; rm3= rm*rm*rm;
      rp = tri->ledge[am] ; rp3= rp*rp*rp;
      sm = -sc;
      sp = tri->ledge[a]-sc;
      if( std::abs( rm+sm) >  std::abs( rp-sp))
        { Ik[0]= std::log( std::abs( (rp+sp )/( rm+sm) )) ;}
      else
        { Ik[0]= std::log( std::abs( (rm-sm)/( rp-sp) ));} ;
      Ik[1]   =-tri->tau[a][0]*(1/rp-1/rm)  - z[0]*(sp/rp-sm/rm)/r02;
      Ik[2]   =-tri->tau[a][1]*(1/rp-1/rm)  - z[1]*(sp/rp-sm/rm)/r02;
      Ik[3]   =-tri->tau[a][2]*(1/rp-1/rm)  - z[2]*(sp/rp-sm/rm)/r02;
      Ik[4]   = 0.5e0*(r02* Ik[0]          + (sp*rp-sm*rm));
      Ik[5]   =-tri->tau[a][0]*(rp-rm)      + z[0]*Ik[0]   ;
      Ik[6]   =-tri->tau[a][1]*(rp-rm)      + z[1]*Ik[0]  ;
      Ik[7]   =-tri->tau[a][2]*(rp-rm)      + z[2]*Ik[0]  ;
      Ik[8]   = 0.25e0*(3*r02*Ik[4]        + (sp*rp3-sm*rm3));
      Ik[9]   =-tri->tau[a][0]*(rp3-rm3)/3  + z[0]*Ik[4]  ;
      Ik[10]  =-tri->tau[a][1]*(rp3-rm3)/3  + z[1]*Ik[4]  ;
      Ik[11]  =-tri->tau[a][2]*(rp3-rm3)/3  + z[2]*Ik[4]  ;
      unsurlong  = 1/tri->ledge[a];
      for( i=0; i<12; i++) {int_edge[a][i]=unsurlong*Ik[i];};
    }
}

void E_lenoir_integseg_gauss(real_t integrale[12],  Mytriangle_t *triangle, real_t x[3], Myquad_t *quad )
{
  number_t g, i;
  real_t ymx[3];
  real_t ab[3];
  real_t r, r3;
  ab[0] = triangle->vertex[2][0]-triangle->vertex[0][0];
  ab[1] = triangle->vertex[2][1]-triangle->vertex[0][1];
  ab[2] = triangle->vertex[2][2]-triangle->vertex[0][2];
  for( i=0; i<12; i++) {integrale[i] = 0;};
  for( g=0; g< quad->ngo; g++)
    {
      ymx[0] =  triangle->vertex[0][0]+ quad->ao[g]*ab[0] -x[0];
      ymx[1] =  triangle->vertex[0][1]+ quad->ao[g]*ab[1] -x[1] ;
      ymx[2] =  triangle->vertex[0][2]+ quad->ao[g]*ab[2] -x[2] ;
      r      = std::sqrt( ymx[0]*ymx[0] +ymx[1]*ymx[1]+ ymx[2]*ymx[2]);
      integrale[0]  =  integrale[0]  +  quad->wo[g]/r;
      r3 = r*r*r;
      integrale[1]  =  integrale[1]  + quad->wo[g]*ymx[0]/r3;
      integrale[2]  =  integrale[2]  + quad->wo[g]*ymx[1]/r3;
      integrale[3]  =  integrale[3]  + quad->wo[g]*ymx[2]/r3;
      integrale[4]  =  integrale[4]  + quad->wo[g]*r   ;
      integrale[5]  =  integrale[5]  - quad->wo[g]*ymx[0]/r;
      integrale[6]  =  integrale[6]  - quad->wo[g]*ymx[1]/r;
      integrale[7]  =  integrale[7]  - quad->wo[g]*ymx[2]/r;
      integrale[8]  =  integrale[8]  + quad->wo[g]*r3;
      integrale[9]  =  integrale[9]  - quad->wo[g]*ymx[0]*r;
      integrale[10] =  integrale[10] - quad->wo[g]*ymx[1]*r;
      integrale[11] =  integrale[11] - quad->wo[g]*ymx[2]*r;
    }
}

void E_lenoir_integseg(real_t integrale[12], Mytriangle_t *triangle, real_t x[3])
{
  number_t a, am, ap, v, vm, vp, i;
  real_t r[3]; real_t xmv[3];
  real_t rm, rp, r02,  sc, rp3, rm3;
  Myct_t* ct ;
  for( i=0; i<12; i++) {integrale[i] = 0;};
  ct        = ElemTools_triangle_x(x, triangle);
  for( v=0; v<3; v++)
    {
      xmv[0] = x[0]-triangle->vertex[v][0];
      xmv[1] = x[1]-triangle->vertex[v][1];
      xmv[2] = x[2]-triangle->vertex[v][2];
      r[v]   = std::sqrt( xmv[0]*xmv[0]+ xmv[1]*xmv[1]+xmv[2]*xmv[2]);
    } ;
  a = 1;
  am = ((a+1) % 3);
  rm = r[am];
  ap = ((a+2) % 3);
  rp = r[ap];
  r02 =  ct->h*ct->h +  ct->d[a]*ct->d[a];
  if( std::abs( rm+ct->t1[a]) >  std::abs( rp-ct->t2[a]))
    {integrale[0] = std::log( std::abs( (rp+ct->t2[a] )/( rm+ct->t1[a]) ));}
  else
    {integrale[0] = std::log( std::abs( (rm-ct->t1[a] )/( rp-ct->t2[a]) ));}
  integrale[4]= 0.5e0*(   r02*integrale[0]+ (ct->t2[a]*rp   -ct->t1[a]*rm));
  rp3 = rp*rp*rp;
  rm3 = rm*rm*rm;
  integrale[8]= 0.25e0*(3*r02*integrale[4]+ (ct->t2[a]*rp3-ct->t1[a]*rm3));
  sc =       (x[0]-triangle->vertex[am][0])*triangle->tau[a][0];
  sc =  sc + (x[1]-triangle->vertex[am][1])*triangle->tau[a][1];
  sc =  sc + (x[2]-triangle->vertex[am][2])*triangle->tau[a][2];
  r[0] = triangle->vertex[am][0] + sc*triangle->tau[a][0];
  r[1] = triangle->vertex[am][1] + sc*triangle->tau[a][1];
  r[2] = triangle->vertex[am][2] + sc*triangle->tau[a][2];
  xmv[0] = x[0]-r[0];
  xmv[1] = x[1]-r[1];
  xmv[2] = x[2]-r[2];
  /*    !write(6,*) 'r02', r02
  !write(6,*) 'r02', xmv(0)**2 +  xmv(1)**2 +  xmv(2)**2
  !stop 'oui?'*/
  integrale[1] = triangle->tau[a][0]*(1/rp-1/rm) - xmv[0]*(ct->t2[a]/rp-ct->t1[a]/rm)/r02;
  integrale[2] = triangle->tau[a][1]*(1/rp-1/rm) - xmv[1]*(ct->t2[a]/rp-ct->t1[a]/rm)/r02;
  integrale[3] = triangle->tau[a][2]*(1/rp-1/rm) - xmv[2]*(ct->t2[a]/rp-ct->t1[a]/rm)/r02;
  integrale[5] = triangle->tau[a][0]*(rp-rm)   +  xmv[0]*integrale[0];
  integrale[6] = triangle->tau[a][1]*(rp-rm)   +  xmv[1]*integrale[0];
  integrale[7] = triangle->tau[a][2]*(rp-rm)   +  xmv[2]*integrale[0];
  integrale[9 ] = triangle->tau[a][0]*(rp3-rm3)/3  +  xmv[0]*integrale[4];
  integrale[10] = triangle->tau[a][1]*(rp3-rm3)/3  +  xmv[1]*integrale[4];
  integrale[11] = triangle->tau[a][2]*(rp3-rm3)/3  +  xmv[2]*integrale[4];
  for( i=0; i<12; i++) {integrale[i]= -integrale[i]/triangle->ledge[a];};
  free(ct);
}

void E_lenoir_integr_edge_gauss(Mytriangle_t *triangle, Myquad_t *quad,real_t int_edge[3][12] )
{
  real_t integrale[12];
  real_t ymx[3];
  real_t  ab[3];
  real_t r, r3;
  number_t a, am, ap, i, g;
  for( a=0; a<3; a++)
    {
      am = ((a+1) % 3);
      ap = ((a+2) % 3);
      ab[0] = triangle->vertex[ap][0]-triangle->vertex[am][0];
      ab[1] = triangle->vertex[ap][1]-triangle->vertex[am][1];
      ab[2] = triangle->vertex[ap][2]-triangle->vertex[am][2];
      for( i=0; i<12; i++) {integrale[i] = 0;};
      for( g=0; g< quad->ngo; g++)
        {
          ymx[0] =  triangle->vertex[am][0]+ quad->ao[g]*ab[0] -triangle->vertex[a][0];
          ymx[1] =  triangle->vertex[am][1]+ quad->ao[g]*ab[1] -triangle->vertex[a][1] ;
          ymx[2] =  triangle->vertex[am][2]+ quad->ao[g]*ab[2] -triangle->vertex[a][2] ;
          r      = std::sqrt( ymx[0]*ymx[0] +ymx[1]*ymx[1]+ ymx[2]*ymx[2]);
          integrale[0]  =  integrale[0]  +  quad->wo[g]/r;
          r3 = r*r*r;
          integrale[1]  =  integrale[1]  + quad->wo[g]*ymx[0]/r3;
          integrale[2]  =  integrale[2]  + quad->wo[g]*ymx[1]/r3;
          integrale[3]  =  integrale[3]  + quad->wo[g]*ymx[2]/r3;
          integrale[4]  =  integrale[4]  + quad->wo[g]*r   ;
          integrale[5]  =  integrale[5]  - quad->wo[g]*ymx[0]/r;
          integrale[6]  =  integrale[6]  - quad->wo[g]*ymx[1]/r;
          integrale[7]  =  integrale[7]  - quad->wo[g]*ymx[2]/r;
          integrale[8]  =  integrale[8]  + quad->wo[g]*r3;
          integrale[9]  =  integrale[9]  - quad->wo[g]*ymx[0]*r;
          integrale[10] =  integrale[10] - quad->wo[g]*ymx[1]*r;
          integrale[11] =  integrale[11] - quad->wo[g]*ymx[2]*r;
        }
      for( i=0; i<12; i++) {int_edge[a][i]=integrale[i];};
    }
}

void E_lenoir_estep0dd(Mytriangle_t *trik, Mytriangle_t *tril, real_t s[3],
                       Myquad_t *quad, real_t si[7], real_t sj[8][3])
{
  real_t   integrale[12]; real_t   integrale_n[12]; real_t   wk[3];
  real_t            x[3]; real_t             ab[3]; real_t  xms[3];
  real_t sc;
  int gl, i;
  for(i=0; i<7; i++) {si[i] = 0;};
  for(i=0; i<8; i++) {sj[i][0] = 0; sj[i][1] = 0; sj[i][2] = 0;};
  ab[0] = tril->vertex[2][0]-tril->vertex[1][0];
  ab[1] = tril->vertex[2][1]-tril->vertex[1][1];
  ab[2] = tril->vertex[2][2]-tril->vertex[1][2];
  for( gl=0; gl< quad->ngo; gl++)
    {
      x[0] =  tril->vertex[1][0]+ quad->ao[gl]*ab[0];
      x[1] =  tril->vertex[1][1]+ quad->ao[gl]*ab[1];
      x[2] =  tril->vertex[1][2]+ quad->ao[gl]*ab[2];
      if (1+1==2) { E_lenoir_integseg(integrale,trik,x);}
      else { E_lenoir_integseg_gauss(integrale,trik,x,quad); }
      /*
      goto 1000
      call E_lenoir_integseg_gauss(integrale_n,trik,x,gauss, npg )
      write(6,*) integrale[0],'(0 =)'
      write(6,*) integrale_n[0]
      write(6,*) integrale[4],'(4 =)'
      write(6,*) integrale_n[4]
      write(6,*) integrale[8],'(8 =)'
      write(6,*) integrale_n[8]
      write(6,*)
      write(6,*) integrale[1]*trik->tau[1][0] + &
      &     integrale[2]*trik->tau[1][1] + &
      &     integrale[3]*trik->tau[1][2], 'voilu'
      write(6,*) integrale[1:3], '(123 =)'
      write(6,*) integrale_n[1:3]

      write(6,*) integrale[5]*trik->tau[1][0] + &
      &     integrale[6]*trik->tau[1][1] + &
      &     integrale[7]*trik->tau[1][2], 'voilus'
      write(6,*) integrale[5:7], '(567 =)'
      write(6,*) integrale_n[5:7]
      write(6,*) integrale[9] *trik->tau[1][0] + &
      &     integrale[10]*trik->tau[1][1] + &
      &     integrale[11]*trik->tau[1][2], 'voilos'
      write(6,*) integrale[9:11], '(901 =)'
      write(6,*) integrale_n[9:11]
      !stop ' integration segment'
      1000   continue */
      for(i=0; i<12; i++) {integrale[i] = 4*quad->wo[gl]*integrale[i];};
      si[0]     =   si[0] + integrale[0]/(3*2);
      xms[0]    =   x[0]-s[0];
      xms[1]    =   x[1]-s[1];
      xms[2]    =   x[2]-s[2];
      si[1]     =   si[1] + integrale[4]/(5*4);
      si[2]     =   si[2] + integrale[8]/(7*6);
      sc        =   xms[0]*xms[0] +xms[1]*xms[1]+ xms[2]*xms[2];
      si[3]     =   si[3] + sc*integrale[0]/(5*4);
      si[4]     =   si[4] + sc*integrale[4]/(7*6);
      sc        = integrale[5]*xms[0]+integrale[6]*xms[1]+integrale[7]*xms[2];
      si[5]     =   si[5] - sc/(5*4);
      sc        = integrale[9]*xms[0]+integrale[10]*xms[1]+integrale[11]*xms[2];
      si[6]     =   si[6] - sc/(7*6);
      sj[0][0]   =  sj[0][0] + integrale[1]/2;
      sj[0][1]   =  sj[0][1] + integrale[2]/2;
      sj[0][2]   =  sj[0][2] + integrale[3]/2;
      sj[1][0]   =  sj[1][0] - integrale[5]/(4*3);
      sj[1][1]   =  sj[1][1] - integrale[6]/(4*3);
      sj[1][2]   =  sj[1][2] - integrale[7]/(4*3);
      sj[2][0]   =  sj[2][0] - integrale[ 9]/(6*5);
      sj[2][1]   =  sj[2][1] - integrale[10]/(6*5);
      sj[2][2]   =  sj[2][2] - integrale[11]/(6*5);
      sj[3][0]   =  sj[3][0] + xms[0]*integrale[0]/(4*3);
      sj[3][1]   =  sj[3][1] + xms[1]*integrale[0]/(4*3);
      sj[3][2]   =  sj[3][2] + xms[2]*integrale[0]/(4*3);
      sj[4][0]   =  sj[4][0] + xms[0]*integrale[4]/(6*5);
      sj[4][1]   =  sj[4][1] + xms[1]*integrale[4]/(6*5);
      sj[4][2]   =  sj[4][2] + xms[2]*integrale[4]/(6*5);
      wk[0] =  integrale[3]*xms[1] -  integrale[2]*xms[2];
      wk[1] =  integrale[1]*xms[2] -  integrale[3]*xms[0];
      wk[2] =  integrale[2]*xms[0] -  integrale[1]*xms[1];
      sj[5][0]   =  sj[5][0] -  wk[0]/(3*2);
      sj[5][1]   =  sj[5][1] -  wk[1]/(3*2);
      sj[5][2]   =  sj[5][2] -  wk[2]/(3*2);
      wk[0] =  integrale[7]*xms[1] -  integrale[6]*xms[2];
      wk[1] =  integrale[5]*xms[2] -  integrale[7]*xms[0];
      wk[2] =  integrale[6]*xms[0] -  integrale[5]*xms[1];
      sj[6][0]   =  sj[6][0] -  wk[0]/(5*4);
      sj[6][1]   =  sj[6][1] -  wk[1]/(5*4);
      sj[6][2]   =  sj[6][2] -  wk[2]/(5*4);
      wk[0] =  integrale[11]*xms[1] -  integrale[10]*xms[2];
      wk[1] =  integrale[ 9]*xms[2] -  integrale[11]*xms[0];
      wk[2] =  integrale[10]*xms[0] -  integrale[ 9]*xms[1];
      sj[7][0]   =  sj[7][0] -  wk[0]/(7*6);
      sj[7][1]   =  sj[7][1] -  wk[1]/(7*6);
      sj[7][2]   =  sj[7][2] -  wk[2]/(7*6);
    }
}

void E_lenoir_tstep0dd(Mytriangle_t *tri, real_t intedge[3][12],real_t si[7], real_t sj[8][3])
{
  real_t v[3]; real_t w[3];
  real_t sb2, sa1, sa2, sb1, lb2, la2;
  number_t a=0, b=1, c=2;
  v[0]  = tri->vertex[a][0] - tri->vertex[c][0];
  v[1]  = tri->vertex[a][1] - tri->vertex[c][1];
  v[2]  = tri->vertex[a][2] - tri->vertex[c][2];
  w[0]  = tri->vertex[b][0] - tri->vertex[c][0];
  w[1]  = tri->vertex[b][1] - tri->vertex[c][1];
  w[2]  = tri->vertex[b][2] - tri->vertex[c][2] ;
  lb2   = tri->ledge[b]*tri->ledge[b];
  la2   = tri->ledge[a]*tri->ledge[a];
  sa1   = v[0]*intedge[a][5] +  v[1]*intedge[a][6] + v[2]*intedge[a][7];
  sa2   = v[0]*intedge[a][9] +  v[1]*intedge[a][10]+ v[2]*intedge[a][11];
  sb1   = w[0]*intedge[b][5] +  w[1]*intedge[b][6] + w[2]*intedge[b][7];
  sb2   = w[0]*intedge[b][9] +  w[1]*intedge[b][10]+ w[2]*intedge[b][11];
  si[0] = 4*( intedge[a][0] +  intedge[b][0]  )/(3*2*1);
  si[1] = 4*( intedge[a][4] +  intedge[b][4]  )/(5*4*3);
  si[2] = 4*( intedge[a][8] +  intedge[b][8]  )/(7*6*5);
  si[3] = 4*(intedge[a][4]-2*sa1+ lb2*intedge[a][0]+la2 *intedge[b][0])/(5*4*3);
  si[4] = 4*(intedge[a][8]-2*sa2+ lb2*intedge[a][4]+la2 *intedge[b][4])/(7*6*5);
  si[5] = 4*(-intedge[a][4]- sa1 + sb1)/(5*4*3);
  si[6] = 4*(-intedge[a][8]- sa2 + sb2)/(7*6*5);
  sj[1][0] = 4*( intedge[a][5] - intedge[b][5] )/(4*3*2);
  sj[1][1] = 4*( intedge[a][6] - intedge[b][6] )/(4*3*2);
  sj[1][2] = 4*( intedge[a][7] - intedge[b][7] )/(4*3*2);
  sj[2][0] = 4*( intedge[a][9 ]- intedge[b][9 ]  )/(6*5*4);
  sj[2][1] = 4*( intedge[a][10]- intedge[b][10]  )/(6*5*4);
  sj[2][2] = 4*( intedge[a][11]- intedge[b][11]  )/(6*5*4);
  sj[3][0] = 4*(-intedge[a][5] + intedge[a][0]*v[0]+intedge[a][0]*w[0] )/(4*3*2);
  sj[3][1] = 4*(-intedge[a][6] +intedge[a][0]*v[1]+intedge[a][0]*w[1] )/(4*3*2);
  sj[3][2] = 4*(-intedge[a][7] +intedge[a][0]*v[2]+intedge[a][0]*w[2] )/(4*3*2);
  sj[4][0] = 4*(-intedge[a][9 ]+intedge[a][4]*v[0]+intedge[a][4]*w[0] )/(6*5*4);
  sj[4][1] = 4*(-intedge[a][10]+intedge[a][4]*v[1]+intedge[a][4]*w[1] )/(6*5*4);
  sj[4][2] = 4*(-intedge[a][11]+intedge[a][4]*v[2]+intedge[a][4]*w[2] )/(6*5*4);
  sj[0][0] = 0 ; sj[0][1] = 0 ; sj[0][2] = 0;
  sj[5][0]=0; sj[5][1]=0;  sj[5][2]=0 ;
  sj[6][0]=0; sj[6][1]=0;  sj[6][2]=0 ;
  sj[7][0]=0; sj[7][1]=0;  sj[7][2]=0 ;
}



void E_lenoir_estep0tp(Mytriangle_t *tri, real_t x[3], real_t s[3], real_t si[7], real_t sj[8][3])
{
  real_t integrale[12];
  real_t xms[3];
  real_t  wk[3];
  real_t  sc;
  number_t i;
  ElemTools_hanninen_t(tri, x, integrale);
  for(i=0; i<12; i++) {integrale[i] =  2*integrale[i];};
  si[0]     =   si[0] + integrale[0]/(3*2);
  xms[0]    =   x[0]-s[0];
  xms[1]    =   x[1]-s[1];
  xms[2]    =   x[2]-s[2];
  si[1]     =   si[1] + integrale[4]/(5*4);
  si[2]     =   si[2] + integrale[8]/(7*6);
  sc        = xms[0]*xms[0] +xms[1]*xms[1]+xms[2]*xms[2];
  si[3]     =   si[3] + sc*integrale[0]/(5*4);
  si[4]     =   si[4] + sc*integrale[4]/(7*6);
  sc        = integrale[5]*xms[0]+integrale[6]*xms[1]+integrale[7]*xms[2];
  si[5]     =   si[5] - sc/(5*4);
  sc        = integrale[9]*xms[0]+integrale[10]*xms[1]+integrale[11]*xms[2];
  si[6]     =   si[6] - sc/(7*6);
  sj[0][0]   =  sj[0][0] + integrale[1]/2;
  sj[0][1]   =  sj[0][1] + integrale[2]/2;
  sj[0][2]   =  sj[0][2] + integrale[3]/2;
  sj[1][0]   =  sj[1][0] - integrale[5]/(4*3);
  sj[1][1]   =  sj[1][1] - integrale[6]/(4*3);
  sj[1][2]   =  sj[1][2] - integrale[7]/(4*3);
  sj[2][0]   =  sj[2][0] - integrale[ 9]/(6*5);
  sj[2][1]   =  sj[2][1] - integrale[10]/(6*5);
  sj[2][2]   =  sj[2][2] - integrale[11]/(6*5);
  sj[3][0]   =  sj[3][0] + xms[0]*integrale[0]/(4*3);
  sj[3][1]   =  sj[3][1] + xms[1]*integrale[0]/(4*3);
  sj[3][2]   =  sj[3][2] + xms[2]*integrale[0]/(4*3);
  sj[4][0]   =  sj[4][0] + xms[0]*integrale[4]/(6*5);
  sj[4][1]   =  sj[4][1] + xms[1]*integrale[4]/(6*5);
  sj[4][2]   =  sj[4][2] + xms[2]*integrale[4]/(6*5);
  wk[0] =  integrale[3]*xms[1] -  integrale[2]*xms[2];
  wk[1] =  integrale[1]*xms[2] -  integrale[3]*xms[0];
  wk[2] =  integrale[2]*xms[0] -  integrale[1]*xms[1];
  sj[5][0]   =  sj[5][0] -  wk[0]/(3*2);
  sj[5][1]   =  sj[5][1] -  wk[1]/(3*2);
  sj[5][2]   =  sj[5][2] -  wk[2]/(3*2);
  wk[0] =  integrale[7]*xms[1] -  integrale[6]*xms[2];
  wk[1] =  integrale[5]*xms[2] -  integrale[7]*xms[0];
  wk[2] =  integrale[6]*xms[0] -  integrale[5]*xms[1];
  sj[6][0]   =  sj[6][0] -  wk[0]/(5*4);
  sj[6][1]   =  sj[6][1] -  wk[1]/(5*4);
  sj[6][2]   =  sj[6][2] -  wk[2]/(5*4);
  wk[0] =  integrale[11]*xms[1] -  integrale[10]*xms[2];
  wk[1] =  integrale[ 9]*xms[2] -  integrale[11]*xms[0];
  wk[2] =  integrale[10]*xms[0] -  integrale[ 9]*xms[1];
  sj[7][0]   =  sj[7][0] -  wk[0]/(7*6);
  sj[7][1]   =  sj[7][1] -  wk[1]/(7*6);
  sj[7][2]   =  sj[7][2] -  wk[2]/(7*6);
}

void E_lenoir_tstep0tp(Mytriangle_t *tri, real_t int_edge[3][12], real_t si[7], real_t sj[8][3])
{
  real_t   integrale[12];
  real_t   integrale_n[12];
  real_t   xms[3]; real_t sc;
  real_t   x[3]; real_t wk[3];
  real_t   s[3];
  x[0] = tri->vertex[2][0];
  x[1] = tri->vertex[2][1];
  x[2] = tri->vertex[2][2];
  s[0] = tri->vertex[1][0];
  s[1] = tri->vertex[1][1];
  s[2] = tri->vertex[1][2];
  xms[0]    =   x[0]-s[0];
  xms[1]    =   x[1]-s[1];
  xms[2]    =   x[2]-s[2];
  integrale[0] = 4*int_edge[2][0];
  integrale[1] = 0;
  integrale[2] = 0;
  integrale[3] = 0;
  integrale[4] = 4*int_edge[2][4]/3;
  integrale[5] = 4*(int_edge[2][5]/2 );
  integrale[6] = 4*(int_edge[2][6]/2 );
  integrale[7] = 4*(int_edge[2][7]/2 );
  integrale[8] = 4*int_edge[2][8]/5;
  integrale[9] = 4*(int_edge[2][9]/4  );
  integrale[10]= 4*(int_edge[2][10]/4 );
  integrale[11]= 4*(int_edge[2][11]/4 );
  si[0]     =   si[0] + integrale[0]/(3*2);
  si[1]     =   si[1] + integrale[4]/(5*4);
  si[2]     =   si[2] + integrale[8]/(7*6);
  sc        = xms[0]*xms[0] +xms[1]*xms[1]+xms[2]*xms[2];
  si[3]     =   si[3] + sc*integrale[0]/(5*4);
  si[4]     =   si[4] + sc*integrale[4]/(7*6);
  sc        = integrale[5]*xms[0]+integrale[6]*xms[1]+integrale[7]*xms[2];
  si[5]     =   si[5] - sc/(5*4);
  sc        = integrale[9]*xms[0]+integrale[10]*xms[1]+integrale[11]*xms[2];
  si[6]     =   si[6] - sc/(7*6);
  sj[0][0]   =  sj[0][0] + integrale[1]/2;
  sj[0][1]   =  sj[0][1] + integrale[2]/2;
  sj[0][2]   =  sj[0][2] + integrale[3]/2;
  sj[1][0]   =  sj[1][0] - integrale[5]/(4*3);
  sj[1][1]   =  sj[1][1] - integrale[6]/(4*3);
  sj[1][2]   =  sj[1][2] - integrale[7]/(4*3);
  sj[2][0]   =  sj[2][0] - integrale[ 9]/(6*5);
  sj[2][1]   =  sj[2][1] - integrale[10]/(6*5);
  sj[2][2]   =  sj[2][2] - integrale[11]/(6*5);
  sj[3][0]   =  sj[3][0] + xms[0]*integrale[0]/(4*3);
  sj[3][1]   =  sj[3][1] + xms[1]*integrale[0]/(4*3);
  sj[3][2]   =  sj[3][2] + xms[2]*integrale[0]/(4*3);
  sj[4][0]   =  sj[4][0] + xms[0]*integrale[4]/(6*5);
  sj[4][1]   =  sj[4][1] + xms[1]*integrale[4]/(6*5);
  sj[4][2]   =  sj[4][2] + xms[2]*integrale[4]/(6*5);
  wk[0] =  integrale[3]*xms[1] -  integrale[2]*xms[2];
  wk[1] =  integrale[1]*xms[2] -  integrale[3]*xms[0];
  wk[2] =  integrale[2]*xms[0] -  integrale[1]*xms[1];
  sj[5][0]   =  sj[5][0] -  wk[0]/(3*2);
  sj[5][1]   =  sj[5][1] -  wk[1]/(3*2);
  sj[5][2]   =  sj[5][2] -  wk[2]/(3*2);
  wk[0] =  integrale[7]*xms[1] -  integrale[6]*xms[2];
  wk[1] =  integrale[5]*xms[2] -  integrale[7]*xms[0];
  wk[2] =  integrale[6]*xms[0] -  integrale[5]*xms[1];
  sj[6][0]   =  sj[6][0] -  wk[0]/(5*4);
  sj[6][1]   =  sj[6][1] -  wk[1]/(5*4);
  sj[6][2]   =  sj[6][2] -  wk[2]/(5*4);
  wk[0] =  integrale[11]*xms[1] -  integrale[10]*xms[2];
  wk[1] =  integrale[ 9]*xms[2] -  integrale[11]*xms[0];
  wk[2] =  integrale[10]*xms[0] -  integrale[ 9]*xms[1];
  sj[7][0]   =  sj[7][0] -  wk[0]/(7*6);
  sj[7][1]   =  sj[7][1] -  wk[1]/(7*6);
  sj[7][2]   =  sj[7][2] -  wk[2]/(7*6);
}


void E_lenoir_chorig_edge(real_t s[3], real_t sn[3], real_t si[7], real_t sj[8][3])
{
  real_t v[3]; real_t vxw[3]; real_t w[3];
  real_t d2, sc;
  v[0]  = s[0]-sn[0];
  v[1]  = s[1]-sn[1];
  v[2]  = s[2]-sn[2];
  d2      = v[0]*v[0]    +  v[1]*v[1]   +  v[2]*v[2]  ;
  sc      = sj[3][0]*v[0] + sj[3][1]*v[1] +  sj[3][2]*v[2];
  si[3]   = si[3] + (3*d2*si[0] - 8*sc)/5 ;
  sc      = sj[4][0]*v[0] + sj[4][1]*v[1] +  sj[4][2]*v[2];
  si[4]   = si[4] + (5*d2*si[1] - 12*sc)/7 ;
  sc      = sj[1][0]*v[0] + sj[1][1]*v[1] +  sj[1][2]*v[2];
  si[5]   = si[5] - 4*sc/5;
  sc      = sj[2][0]*v[0] + sj[2][1]*v[1] +  sj[2][2]*v[2];
  si[6]   = si[6] - 6*sc/7;
  /* je passe aux sj  indice 0,1,2 => rien à faire*/
  sj[3][0] =  sj[3][0] - 3*v[0]*si[0]/4;
  sj[3][1] =  sj[3][1] - 3*v[1]*si[0]/4;
  sj[3][2] =  sj[3][2] - 3*v[2]*si[0]/4;
  sj[4][0] =  sj[4][0] - 5*v[0]*si[1]/6;
  sj[4][1] =  sj[4][1] - 5*v[1]*si[1]/6;
  sj[4][2] =  sj[4][2] - 5*v[2]*si[1]/6;
  w[0]    =  sj[0][0];
  w[1]    =  sj[0][1];
  w[2]    =  sj[0][2];
  vxw[0]  =  v[1]*w[2]-v[2]*w[1];
  vxw[1]  =  v[2]*w[0]-v[0]*w[2];
  vxw[2]  =  v[0]*w[1]-v[1]*w[0];
  sj[5][0] =  sj[5][0] + 2*vxw[0]/3;
  sj[5][1] =  sj[5][1] + 2*vxw[1]/3;
  sj[5][2] =  sj[5][2] + 2*vxw[2]/3;
  w[0]    =  sj[1][0];
  w[1]    =  sj[1][1];
  w[2]    =  sj[1][2];
  vxw[0]  =  v[1]*w[2]-v[2]*w[1];
  vxw[1]  =  v[2]*w[0]-v[0]*w[2];
  vxw[2]  =  v[0]*w[1]-v[1]*w[0];
  sj[6][0] =  sj[6][0] - 4*vxw[0]/5;
  sj[6][1] =  sj[6][1] - 4*vxw[1]/5;
  sj[6][2] =  sj[6][2] - 4*vxw[2]/5;
  w[0]    =  sj[2][0];
  w[1]    =  sj[2][1];
  w[2]    =  sj[2][2];
  vxw[0]  =  v[1]*w[2]-v[2]*w[1];
  vxw[1]  =  v[2]*w[0]-v[0]*w[2];
  vxw[2]  =  v[0]*w[1]-v[1]*w[0];
  sj[7][0] =  sj[7][0] - 6*vxw[0]/7;
  sj[7][1] =  sj[7][1] - 6*vxw[1]/7;
  sj[7][2] =  sj[7][2] - 6*vxw[2]/7;

}
void E_lenoir_chorig_tri(real_t s[3], real_t sn[3], real_t si[7], real_t sj[8][3])
{
  real_t sc, d2; real_t v[3];
  /* input  calcul au point sn, output calcul au point s*/
  /* pour si  indices 0,1,2 => S n'apparait pas rien à faire*/
  v[0]  = s[0]-sn[0];
  v[1]  = s[1]-sn[1];
  v[2]  = s[2]-sn[2];
  d2      = v[0]*v[0]     +  v[1]*v[1]    +  v[2]*v[2]    ;
  sc      = sj[3][0]*v[0] + sj[3][1]*v[1] +  sj[3][2]*v[2];
  si[3]   = si[3] + ((3*2)*d2*si[0] - 2*(4*3)*sc)/(5*4)   ;
  sc      = sj[4][0]*v[0] + sj[4][1]*v[1] +  sj[4][2]*v[2];
  si[4]   = si[4] + ((5*4)*d2*si[1] - 2*(6*5)*sc)/(7*6)   ;
  sc      = sj[1][0]*v[0] + sj[1][1]*v[1] +  sj[1][2]*v[2];
  si[5]   = si[5] - (4*3)*sc/(5*4)                        ;
  sc      = sj[2][0]*v[0] + sj[2][1]*v[1] +  sj[2][2]*v[2];
  si[6]   = si[6] - (6*5)*sc/(7*6);                        ;
  /* pour sj indice 0][1,2 => rien à faire */
  sj[3][0] =  sj[3][0] - (3*2)*v[0]*si[0]/(4*3);
  sj[3][1] =  sj[3][1] - (3*2)*v[1]*si[0]/(4*3);
  sj[3][2] =  sj[3][2] - (3*2)*v[2]*si[0]/(4*3);
  sj[4][0] =  sj[4][0] - (5*4)*v[0]*si[1]/(6*5);
  sj[4][1] =  sj[4][1] - (5*4)*v[1]*si[1]/(6*5);
  sj[4][2] =  sj[4][2] - (5*4)*v[2]*si[1]/(6*5);
}

void E_lenoir_vertex(real_t ck[3][3],real_t cl[3][3],real_t ka[2], Myquad_t *quad, complex_t zts[3][3], complex_t zks[3][3], number_t cf)
{
  real_t sil[7]; real_t sjl[8][3];
  real_t sik[7]; real_t sjk[8][3];
  real_t si [7]; real_t sj [8][3];
  real_t s[3];  real_t s2[3]  ;
  real_t sib[7]; real_t sjb[8][3];
  Mytriangle_t *trik;
  Mytriangle_t *tril;
  /*E_lenoir_printt( ck, cl);*/
  s[0] = ck[0][0];
  s[1] = ck[0][1];
  s[2] = ck[0][2];
  trik = ElemTools_triangle_c(ck);
  tril = ElemTools_triangle_c(cl);
  E_lenoir_vstep0(trik, tril, s, quad, sik, sjk);
  /*E_lenoir_prints( sik, sjk);*/
  E_lenoir_vstep0(tril, trik, s, quad, sil, sjl);
  /*E_lenoir_prints( sil, sjl);*/
  E_lenoir_vstep1(sik,  sjk, sil, sjl, si, sj);
//  if( DEBUGV ==1)
//    {
//      ElemTools_hanninen_step1(ck, cl, s, quad,sib, sjb);
//      ds(sib, sjb, si, sj);
//    };
  ElemTools_hanninen_step23(ka, ck, cl,s, si, sj, zts, zks, cf);
}

void E_lenoir_edge(real_t ck[3][3],real_t cl[3][3],real_t ka[2],
                   Myquad_t *quad, complex_t zts[3][3],
                   complex_t zks[3][3], number_t cf)
{
  real_t sil[7]; real_t sjl[8][3];
  real_t sik[7]; real_t sjk[8][3];
  real_t si [7]; real_t sj [8][3];
  real_t s0[3]; real_t s1[3] ; real_t s2[3] ;
  real_t sib [7]; real_t sjb [8][3];
  real_t sibb[7]; real_t sjbb[8][3];
  real_t silb[7]; real_t sjlb[8][3];
  real_t sikb[7]; real_t sjkb[8][3];
  number_t i;

  Mytriangle_t *trik;
  Mytriangle_t *tril;
  s0[0] = ck[0][0]; s0[1] = ck[0][1]; s0[2] = ck[0][2];
  s1[0] = ck[1][0]; s1[1] = ck[1][1]; s1[2] = ck[1][2];
  s2[0] = ck[2][0]; s2[1] = ck[2][1]; s2[2] = ck[2][2];

  trik = ElemTools_triangle_c(ck);
  tril = ElemTools_triangle_c(cl);
//  if( DEBUGA ==1)
//    {
//      E_lenoir_vstep0(trik, tril, s0, quad, sikb, sjkb);
//      E_lenoir_vstep0(tril, trik, s0, quad, silb, sjlb);
//      E_lenoir_vstep1(sikb, sjkb, silb, sjlb, sib, sjb);
//      if( DEBUGV==1)
//        {
//          printf("comparaison entre tri-gauss  et tri-analytique ...\n");
//          Elemtools_hanninen_step1(ck, cl,s0, quad,sibb, sjbb);
//          ds(sibb, sjbb, sib, sjb);
//        };
//    };
  E_lenoir_estep0dd(trik, tril, s1, quad, sik, sjk);
  /* ajout contribution triangle point S2...*/
  s2[0] = cl[2][0]; s2[1] = cl[2][1]; s2[2] = cl[2][2];
  E_lenoir_estep0tp(trik, s2, s1, sik, sjk);
  E_lenoir_chorig_edge(s0, s1, sik, sjk);
//  if( DEBUGA==1 )
//    {
//      printf("comparaison entre tri-arete et tri-point + edge-edge pour ck... \n");
//      ds(sikb, sjkb, sik, sjk);
//    };
  /* contribution arete arete opposée à S1...*/
  E_lenoir_estep0dd(tril, trik, s1, quad, sil, sjl);
  /* ajout contribution triangle point...*/
  s2[0] = ck[2][0]; s2[1] = ck[2][1]; s2[2] = ck[2][2];
  E_lenoir_estep0tp(tril, s2, s1, sil, sjl);
  E_lenoir_chorig_edge(s0, s1, sil, sjl);
//  if( DEBUGA==1 )
//    {
//      printf("comparaison entre tri-arete et tri-point + edge-edge pour cl.../n");
//      ds(silb, sjlb, sil, sjl);
//    }
  E_lenoir_vstep1(sik, sjk, sil, sjl, si, sj);
//  if( DEBUGA==1 )
//    {
//      printf("comparaison entre hanninen et arete commune /n");
//    };
  ElemTools_hanninen_step23(ka, ck, cl,s0, si, sj, zts, zks, cf);
//  if( DEBUGA==1 )
//    {
//      ds(sibb, sjbb, si, sj);
//    };
}

void E_lenoir_triangle(real_t ck[3][3], real_t ka[2], Myquad_t *quad, complex_t zts[3][3], complex_t zks[3][3], number_t cf)
{
  real_t sil[7]; real_t sjl[8][3];
  real_t sik[7]; real_t sjk[8][3];
  real_t si [7]; real_t sj [8][3];
  real_t s0[3]; real_t s1[3] ; real_t s2[3] ;

  real_t sib [7]; real_t sjb [8][3];
  real_t sibb[7]; real_t sjbb[8][3];
  real_t silb[7]; real_t sjlb[8][3];
  real_t sikb[7]; real_t sjkb[8][3];
  real_t intedge[3][12]; real_t iK[3][12];
  number_t i;
  Mytriangle_t *trik;
  s0[0] = ck[0][0]; s0[1] = ck[0][1]; s0[2] = ck[0][2];
  s1[0] = ck[1][0]; s1[1] = ck[1][1]; s1[2] = ck[1][2];
  s2[0] = ck[2][0]; s2[1] = ck[2][1]; s2[2] = ck[2][2];
  trik = ElemTools_triangle_c(ck);
  E_lenoir_integr_edge(trik, intedge);

//  if( DEBUGT==1 )
//    {
//      E_lenoir_integr_edge_gauss(trik, quad, iK );
//      for(i=0; i<12; i++)
//        {
//          printf("i, intedge(0,i) %d %e %e %e \n", i, intedge[i][0],intedge[i][1],intedge[i][2]);
//          printf("i, intedge(0,i) %d %e %e %e \n", i, iK[i][0],iK[i][1],iK[i][2]);
//        };
//    };
  /* calcul de l'integrale sur la paire de segments edge_0 x edge_1*/
  E_lenoir_tstep0dd(trik,  intedge,sib, sjb);
  /* l'origine est S2=> je passe à S1*/
  E_lenoir_chorig_tri(s1, s2, sib, sjb);

//  if( DEBUGT==1 )
//    {
//      /* contribution arete arete opposée à S1...*/
//      E_lenoir_estep0dd(trik, trik, s1, quad, sik, sjk);
//      ds(sik,sjk,sib,sjb);
//    };
  /* ajout de  l'intégrale triangle point S2 ...*/
  E_lenoir_tstep0tp(trik, intedge, sib, sjb);
//  if( DEBUGT==1 )
//    {
//      if( 1+1==2)
//        {
//          printf("attention debug ...\n");
//          s2[0] = s2[0] + trik->ledge[2]*0.00001e0;
//          s2[1] = s2[1] + trik->ledge[2]*0.00001e0;
//          s2[2] = s2[2] + trik->ledge[2]*0.00001e0;
//          E_lenoir_estep0tp(trik, s2, s1, sik, sjk);
//          ds(sik,sjk,sib,sjb);
//        }
//      else
//        {
//          E_lenoir_estep0tp_debug(trik, s2, s1, quad, sik, sjk);
//        };
//      E_lenoir_chorig_edge(s0, s1, sik, sjk);
//    };
  /* puis changement d'origine de S1 vers S0*/
  E_lenoir_chorig_edge(s0, s1, sib, sjb);

//  if( DEBUGT==1)
//    {
//      E_lenoir_estep0dd(trik, trik, s1, quad, sil, sjl);
//      printf("attention debug ...\n");
//      s2[0] = s2[0] + trik->ledge[2]*0.00001e0;
//      s2[1] = s2[1] + trik->ledge[2]*0.00001e0;
//      s2[2] = s2[2] + trik->ledge[2]*0.00001e0;
//      E_lenoir_estep0tp(trik, s2, s1, sil, sjl);
//      E_lenoir_chorig_edge(s0, s1, sil, sjl);
//      ds(sik,sjk,sib,sjb);
//      ds(sik,sjk,sil,sjl);
//      E_lenoir_vstep1(sib, sjb, sib, sjb, si, sj);
//    }
//  else
//    {
  E_lenoir_vstep1(sib, sjb, sib, sjb, si, sj);

//    }

  ElemTools_hanninen_step23(ka, ck, ck,s0, si, sj, zts, zks, cf);
}

} //end of namespace

#endif /* DOXYGEN_SHOULD_SKIP_THIS */
