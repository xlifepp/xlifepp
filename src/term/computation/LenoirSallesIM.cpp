/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file LenoirSallesIM.cpp
  \authors S. Roman, N.Salles, E. Lunéville
  \since 06 feb 2017
  \date 06 feb 2017

  \brief Implementation of the Lenoir and Salles' method to compute the BEM TermMatrix.
*/

#include "LenoirSallesIM.hpp"

namespace xlifepp
{
// ===========================================================================================
//  LENOIR-SALLES method for Laplace operator in 2D:  single layer P0 and P1, double layer P0
// ===========================================================================================
void LenoirSalles2dIM::computeLaplace2dSLP0(const Element* elt_S, const Element* elt_T, real_t& res) const
{
  MeshElement* melt_S=elt_S->geomElt_p->meshElement(), *melt_T=elt_S->geomElt_p->meshElement(); //self influence
  if(melt_S==melt_T) res=selfInfluenceLaplace2dSLP0(melt_S->vertex(1),melt_S->vertex(2));
  else
    {
      number_t i1=0, j1=0;
      Vector<number_t> indi(2,0), indj(2,0);
      number_t SC=0;
      for(number_t i=1; i<3; i++)
        for(number_t j=1; j<3; j++)
          if((elt_S->geomElt_p->vertexNumber(i))==(elt_T->geomElt_p->vertexNumber(j)))
            {
              SC++;
              indi[i1]=i;
              i1++;
              indj[j1]=j;
              j1++;
            }
      if(SC==1)
        {
          indi[1]=(indi[0]%2)+1;
          indj[1]=(indj[0]%2)+1;
          res=vertexLaplace2dSLP0(melt_S->vertex(indi[0]),melt_S->vertex(indi[1]),melt_T->vertex(indj[1]));
        }
      else
        {
          where("LenoirSalles2dIM::computeLaplace2dSLP0(...)");
          error("elt_adjacent_only");
        }
    }
}

void LenoirSalles2dIM::computeLaplace2dDLP0(const Element* elt_S, const Element* elt_T, real_t& res) const
{
  MeshElement* melt_S=elt_S->geomElt_p->meshElement(), *melt_T=elt_T->geomElt_p->meshElement();
  if(melt_S==melt_T)  res=0.;  // self_influence
  else
    {
      number_t i1=0, j1=0, SC=0;
      Vector<number_t> indi(2,0), indj(2,0);
      for(number_t i=1; i<3; i++)
        for(number_t j=1; j<3; j++)
          if(elt_S->geomElt_p->vertexNumber(i)==elt_T->geomElt_p->vertexNumber(j))
            {
              SC++;
              indi[i1]=i;
              i1++;
              indj[j1]=j;
              j1++;
            }
      if(SC==1)
        {
          indi[1]=(indi[0]%2)+1;
          indj[1]=(indj[0]%2)+1;
          Vector<real_t>& ny=melt_S->geomMapData_p->normalVector;
          Point nyb=Point(ny);
          res=vertexLaplace2dDLP0(melt_S->vertex(indi[0]),melt_S->vertex(indi[1]),melt_T->vertex(indj[1])); //,nyb);
        }
      else
        {
          where("LenoirSalles2dIM::computeLaplace2dDLP0(...)");
          error("elt_adjacent_only");
        }
    }
}

void LenoirSalles2dIM::computeLaplace2dSLP1(const Element* elt_S, const Element* elt_T, Matrix<real_t> & res) const
{
  MeshElement* melt_S=elt_S->geomElt_p->meshElement(), *melt_T=elt_T->geomElt_p->meshElement();
  if(melt_S==melt_T)  selfInfluenceLaplace2dSLP1(melt_S->vertex(1), melt_S->vertex(2), res);   // self_influence
  else
    {
      number_t i1=0, j1=0, SC=0;
      Vector<number_t> indi(2,0), indj(2,0);
      for(number_t i=1; i<3; i++)
        for(number_t j=1; j<3; j++)
          if(elt_S->geomElt_p->vertexNumber(i)==elt_T->geomElt_p->vertexNumber(j))
            {
              SC++;
              indi[i1]=i;
              i1++;
              indj[j1]=j;
              j1++;
            }
      if(SC==1)
        {
          indi[1]=(indi[0]%2)+1;
          indj[1]=(indj[0]%2)+1;
          vertexLaplace2dSLP1(melt_S->vertex(indi[0]), melt_S->vertex(indi[1]), melt_T->vertex(indj[1]),indi,indj,res);
        }
      else
        {
          where("LenoirSalles2dIM::computeLaplace2dSLP1(...)");
          error("elt_adjacent_only");
        }
    }
}


//various stuff
//-------------
real_t crossProduct2D(const Point& A, const Point& B)
{
  return A[0]*B[1]-A[1]*B[0];
}

real_t LenoirSalles2dIM::selfInfluenceLaplace2dSLP0(const Point& A, const Point& B) const
{
  real_t norm_alpha=A.distance(B);
  real_t na2=norm_alpha*norm_alpha;
  return  -over2pi_*(na2*std::log(norm_alpha)-3./2.*na2);
}

void LenoirSalles2dIM::selfInfluenceLaplace2dSLP1(const Point& A, const Point& B, Matrix<real_t>& res) const
{
  real_t nA=norm2(A-B);
  real_t nA2=nA*nA;
  real_t t1=0.25*std::log(nA);
  res(1,1)+=-over2pi_*nA2*(t1-7./16.);
  res(1,2)+=-over2pi_*nA2*(t1-5./16.);
  res(2,1)+=-over2pi_*nA2*(t1-5./16.);
  res(2,2)+=-over2pi_*nA2*(t1-7./16.);
}

real_t LenoirSalles2dIM::vertexLaplace2dSLP0(const Point& A, const Point& B, const Point& C) const
{
  real_t nA=A.distance(B), nB=A.distance(C);
  real_t nAB=nA*nB; //norm_alpha*norm_beta;
  Point alpha=B-A, beta=C-A;
  real_t dAB=dot(alpha,beta);
  real_t taup=dAB/nB, sigmap=dAB/nA;
  real_t cAB=std::abs(crossProduct2D(alpha,beta));
  real_t dbetap=cAB/nB, dalphap=cAB/nA;
  real_t dBC=B.distance(C);
  real_t tmp=-dAB/cAB;
  real_t t1=nA*((nB-taup)*std::log(dBC)+taup*std::log(nA)+dbetap*(std::atan((nB*nB-dAB)/cAB)-std::atan(tmp)));
  real_t t2=nB*((nA-sigmap)*std::log(dBC) +sigmap*std::log(nB) + dalphap*(std::atan((nA*nA-dAB)/cAB)-std::atan(tmp)));
  return -over4pi_*(t1+t2-3.*nAB);
}

real_t LenoirSalles2dIM::vertexLaplace2dSLP1b(const Point& A, const Point& B, const Point& C, int i, int j) const
{
  Vector<real_t> alpha=toVector(B,A), beta=toVector(C,A);
  real_t norm_alpha,res=0,norm_beta,tp,sp,tm,sm,taup,sigmap,dbetap,dalphap;
  int indi, indj;
  Vector <Point> a(2), b(2);
  Vector <real_t> s(2), t(2);
  real_t Ilog, Iarc, Irat, t1, t2, t3, t4, t5, t6, t7, t8;
  indi=2.-(i+1)/2.;
  indj=2.-(j+1)/2.;
  norm_alpha=alpha.norm2();
  norm_beta=beta.norm2();
  tp=norm_beta;
  sp=norm_alpha;
  tm=0.;
  sm=0.;
  taup=dot(alpha,beta)/norm_beta;
  sigmap=dot(alpha,beta)/norm_alpha;
  dbetap=sp*std::abs(crossProduct2D(alpha,beta))/norm_beta/norm_alpha;
  dalphap=tp*std::abs(crossProduct2D(alpha,beta))/norm_beta/norm_alpha;
  a(1)=A;
  a(2)=B;
  b(1)=A;
  b(2)=C;
  s(1)=sm;
  s(2)=sp;
  t(1)=tm;
  t(2)=tp;
  Ilog=0;
  for(int v=1; v<3; v++)
    {
      t1=i*j/norm_alpha*(2.*v-3.)*std::log(norm(a(v)-C));
      t2=s(indi)*(s(v)-sigmap)*(t(indj)/2.-norm_beta/3.);
      t3=((sigmap-0.)*(s(v)-sigmap)+norm(a(v)-C)*norm(a(v)-C)/2.);
      t4=(t(indj)/3.-norm_beta/4.);
      t5=i*j/norm_beta*(2.*v-3.)*std::log(norm(b(v)-B));
      t6=t(indj)*(t(v)-taup)*(s(indi)/2.-norm_alpha/3.);
      t7=((taup-0.)*(t(v)-taup)+norm(b(v)-B)*norm(b(v)-B)/2.);
      t8=(s(indi)/3.-norm_alpha/4.);
      Ilog=Ilog+t1*(t2-t3*t4)+t5*(t6-t7*t8);
    }
  Iarc=i*j/norm_alpha*(s(indi)*(t(indj)/2.-norm_beta/3.)-(sigmap-sm)*(t(indj)/3.-
                       norm_beta/4.))*dalphap*(std::atan((sp-sigmap)/dalphap)-std::atan((sm-sigmap)/dalphap))
       +i*j/norm_beta*(t(indj)*(s(indi)/2.-norm_alpha/3.)-(taup-tm)*(s(indi)/3.-
                       norm_alpha/4.))*dbetap*(std::atan((tp-taup)/dbetap)-std::atan((tm-taup)/dbetap));
  t1=-i*j*(s(indi)*(t(indj)/2.-norm_beta/3.)-(sigmap-sm)*(t(indj)/3.-norm_beta/4.));
  t2=-i*j*(t(indj)*(s(indi)/2.-norm_alpha/3.)-(taup-tm)*(s(indi)/3.-norm_alpha/4.));
  t3=i*j/4./norm_alpha*(t(indj)/3.-norm_beta/4.)*(norm(B-C)*norm(B-C)-norm(C-A)*norm(C-A));
  t4=i*j/4./norm_beta*(s(indi)/3.-norm_alpha/4.)*(norm(B-C)*norm(B-C)-norm(B-A)*norm(B-A));
  t5=-i*j/2.*s(indi)*t(indj)+i*j/6.*t(indj)*norm_alpha+i*j/6.*s(indi)*norm_beta
     -i*j/16.*norm_alpha*norm_beta;
  Irat=t1+t2+t3+t4+t5;
  res=Ilog+Iarc+Irat;
  return res;
}

void LenoirSalles2dIM::vertexLaplace2dSLP1(const Point& A, const Point& B, const Point& C,
    const Vector<number_t>& indi, const Vector<number_t>& indj,
    Matrix<real_t>& res) const
{
  int sindi0=1, sindj0=1, sindi1=1, sindj1=1;
  if(indi[0]==1) { sindi0=-1; }
  if(indi[1]==1) { sindi1=-1; }
  if(indj[0]==1) { sindj0=-1; }
  if(indj[1]==1) { sindj1=-1; }
  res(1,1)=-over2pi_*vertexLaplace2dSLP1b(A,B,C,sindi0,sindj0);
  res(1,2)=-over2pi_*vertexLaplace2dSLP1b(A,B,C,sindi0,sindj1);
  res(2,1)=-over2pi_*vertexLaplace2dSLP1b(A,B,C,sindi1,sindj0);
  res(2,2)=-over2pi_*vertexLaplace2dSLP1b(A,B,C,sindi1,sindj1);
}

real_t LenoirSalles2dIM::vertexLaplace2dDLP0b(const Point& A, const Point& B, const Point& C, Point& ny) const //, Parameters & pars)
{
  real_t res=0.;
  Point alpha=B-A;
  Point beta=C-A;
  real_t na=norm2(alpha);
  real_t nb=norm2(beta);
  real_t hb=0., hc=0.;
  Point Bp=projectionOnSegment(B,A,C,hb);
  Point Cp=projectionOnSegment(C,A,B,hc);
  res+=na*dot(B-Bp,ny)*fun1P0(A,C,Bp,hb,beta,nb)-na*dot(beta,ny)/nb*fun2P0(A,C,Bp,hb,beta,nb);
  res+=nb*dot(Cp-C,ny)*fun1P0(A,B,Cp,hc,alpha,na);
  return over2pi_*res;
}

real_t LenoirSalles2dIM::vertexLaplace2dDLP0(const Point& A, const Point& B, const Point& C) const //, Parameters & pars)
{
  Point alpha=B-A;
  Point beta=C-A;
  real_t na=norm2(alpha);
  real_t nb=norm2(beta);
  real_t res;
  real_t t=std::acos(dot(alpha,beta)/(na*nb));
  res=na*na*std::sin(t)*R1(na,nb,t)+nb*std::sin(t)*R2(nb,na,t);
  return res;
}

//R1: int sur alpha de 1/norm(x-y)^2
real_t LenoirSalles2dIM::R1(real_t y, real_t a, real_t teta) const
{
  real_t res =1./y/std::sin(teta)*std::atan(a*std::sin(teta)/(y-a*std::cos(teta)));
  return over2pi_*res;
}

//R2: int sur alpha de x/norm(x-y)^2
real_t LenoirSalles2dIM::R2(real_t y, real_t a, real_t teta) const
{
  real_t res = 0.5*std::log((a/y-std::cos(teta))*(a/y-std::cos(teta))+std::sin(teta)*std::sin(teta))
               + y*std::cos(teta)*R1(y,a,teta);
  return over2pi_*res;
}

real_t LenoirSalles2dIM::fun1P0(const Point& A, const Point& B, const Point& M, real_t d, const Point& alpha, real_t nV) const
{
  real_t sm=dot(A-M,alpha)/nV;
  real_t sp=dot(B-M,alpha)/nV;
  return 1./d*(std::atan(sp/d)-std::atan(sm/d));
}

real_t LenoirSalles2dIM::fun2P0(const Point& A, const Point &B, const Point &M, real_t d, const Point& alpha, real_t nV) const
{
  real_t sm=dot(A-M,alpha)/nV;
  real_t sp=dot(B-M,alpha)/nV;
  real_t d2=d*d;
  return 0.5*(std::log(d2+sp*sp)-std::log(d2+sm*sm));
}

// ===================================================================
//  LENOIR-SALLES method for Laplace operator in 3D:  single layer P0
// ===================================================================
void LenoirSalles3dIM::computeLaplace3dSLP0(const Element* elt_S, const Element* elt_T, real_t& res) const
{
  Vector<number_t> indi(3,0), indj(3,0);
  Point S1, S2, S3;
  MeshElement* melt_S=elt_S->geomElt_p->meshElement(), *melt_T=elt_T->geomElt_p->meshElement();
  if(melt_S==melt_T)
    {
      indi[0]=1;
      indi[1]=2;
      indi[2]=3;
      loadElement(elt_S, S1, S2, S3, indi);
      res = selfInfluenceLaplace3dSLP0(S1,S2,S3);
    }
  else
    {
      Point T1, T2, T3;
      number_t i1=0,j1=0,SC=0;
      for(number_t i=1; i<=3; i++)
        for(number_t j=1; j<=3; j++)
          if((elt_S->geomElt_p->vertexNumber(i))==(elt_T->geomElt_p->vertexNumber(j)))
            {
              SC++;
              indi[i1]=i;
              i1++;
              indj[j1]=j;
              j1++;
            }
      if(SC==2)
        {
          indi[2]=6-indi[0]-indi[1];
          indj[2]=6-indj[0]-indj[1];
          loadElement(elt_S,S1,S2,S3,indi);
          loadElement(elt_T,T1,T2,T3,indj);
          res= adjacentTrianglesByEdgeLaplace3dSLP0(S3, S1, S2, T3);
        }
      if(SC==1)
        {
          indi[1] = int((-3./2)*indi[0]*indi[0]+(5.5)*(indi[0])-2);
          indi[2] = int((3. / 2)*indi[0] * indi[0] - (6.5)*indi[0] + 8);
          indj[1] = int((-3. / 2)*indj[0] * indj[0] + (5.5)*indj[0] - 2);
          indj[2] = int((3. / 2)*indj[0] * indj[0] - (6.5)*indj[0] + 8);
          loadElement(elt_S,S1,S2,S3,indi);
          loadElement(elt_T,T1,T2,T3,indj);
          Point M=crossProduct(S3-S1,S2-S1);
          real_t t=dot((0.5*(S2+S3) - 0.5*(T2+T3)),M);
          if(std::abs(t) < theEpsilon) { res= adjacentTrianglesByVertexCoplanarLaplace3dSLP0(S1, S2, S3, T2, T3); }
          else { res= adjacentTrianglesByVertexSecantPlanesLaplace3dSLP0(S1, S2, S3, T2, T3); }
        }
      if(SC==0)  //may be close triangles
        {
          indi[0]=1;
          indi[1]=2;
          indi[2]=3;
          indj[0]=1;
          indj[1]=2;
          indj[2]=3;
          loadElement(elt_S,S1,S2,S3,indi);
          loadElement(elt_T,T1,T2,T3,indj);

          Point Test(0., 0., 0.), O(0., 0., 0.);
          std::vector<real_t> M1=eqtOfPlane(S1,S2,S3);
          O[0]=M1[0]*T1[0]+M1[1]*T1[1]+M1[2]*T1[2]+M1[3];
          O[1]=M1[0]*T2[0]+M1[1]*T2[1]+M1[2]*T2[2]+M1[3];
          O[2]=M1[0]*T3[0]+M1[1]*T3[1]+M1[2]*T3[2]+M1[3];
          if(norm2(O) < theEpsilon) { res=nonAdjacentTrianglesCoplanarLaplace3dSLP0(S1, S2, S3, T1, T2, T3); }
          else
            {
              Point v1=crossProduct(S3-S1,S2-S1);
              Point v2=crossProduct(T3-T1,T2-T1);
              Point v3=crossProduct(v1,v2);
              if(norm2(v3) < theEpsilon) { res=nonAdjacentTrianglesParallelPlanesLaplace3dSLP0(S1, S2, S3, T1, T2, T3); }
              else { res=nonAdjacentTrianglesSecantPlanesLaplace3dSLP0(S1, S2, S3, T1, T2, T3); }
            } // end else norm2(O)
        } // end SC=0
    }
  res*=over4pi_;
}

real_t LenoirSalles3dIM::adjacentTrianglesByEdgeCoplanarLaplace3dSLP0(const Point & S3,const Point & S1,const Point & S2,const Point & T3) const
{
  real_t g1, g2,d1, d2;
  real_t res=0.;
  Point I1=projectionOnStraightLine(S1,S2,S3,g1);
  Point I2=projectionOnStraightLine(S2,S1,S3,g2);
  Point J1=projectionOnStraightLine(S1,S2,T3,d1);
  Point J2=projectionOnStraightLine(S2,T3,S1,d2);
  real_t aireS=0.5*g2*norm2(S1-S3);
  real_t aireT=0.5*d2*norm2(S1-T3);
  res+=(aireS/3.)*I0_Coplanar_P(S3,S1,S2,T3)+(aireT/3.)*I0_Coplanar_P(T3,S1,S2,S3);
  res+=(g1*d2/6.)*I0_Coplanar_Q(S2,S3,T3,S1)+ (g2*d1/6.)*I0_Coplanar_Q(S3,S1,S2,T3);
  return res;
}

real_t LenoirSalles3dIM::selfInfluenceLaplace3dSLP0(const Point& S1, const Point& S2, const Point& S3) const
{
  real_t res=0.;
  std::vector<real_t> D=triangleHeightsLengths(S1,S2,S3);
  real_t AireS=D[0]*norm2(S2-S3)*0.5;
  res=(2./3)*AireS*(D[0]*I0_Coplanar_R(S1, S2, S3, D[0])+D[1]*I0_Coplanar_R(S2, S3, S1, D[1])+D[2]*I0_Coplanar_R(S3, S1, S2, D[2]));
  return res;
}

real_t LenoirSalles3dIM::adjacentTrianglesByVertexCoplanarLaplace3dSLP0(const Point& S1,const Point& S2, const Point& S3,
    const Point& T2, const Point& T3) const
{
  real_t g1, g2, g3, d1, d2, d3;
  real_t res=0.;
  std::vector<real_t> D=signedDistancesToTriangleEdges(S2,S1,T2,T3);
  std::vector<real_t> G=signedDistancesToTriangleEdges(T2,S1,S2,S3);
  Point I1=projectionOnStraightLine(S1,S2,S3,g1);
  Point I2=projectionOnStraightLine(S2,S1,S3,g2);
  Point I3=projectionOnStraightLine(S3,S2,S1,g3);
  Point J1=projectionOnStraightLine(S1,T2,T3,d1);
  Point J2=projectionOnStraightLine(T2,S1,T3,d2);
  Point J3=projectionOnStraightLine(T3,T2,S1,d3);
  real_t aireS=0.5*g2*norm2(S1-S3);
  real_t aireT=0.5*d2*norm2(S1-T3);
  res=(1./3.)*(aireS*I0_Coplanar_P(S3,S1,T2,T3)+aireT*I0_Coplanar_P(T3,S1,S2,S3))
      +(g1/6.)*(D[1]*I0_Coplanar_Q(S2,S3,T3,S1)+D[2]*I0_Coplanar_Q(S2,S3,S1,T2))
      +(d1/6.)*(G[1]*I0_Coplanar_Q(S3,S1,T2,T3)+G[2]*I0_Coplanar_Q(S1,S2,T2,T3))
      +(1./6.)*(g1*D[0]+d1*G[0])*I0_Coplanar_Q(S2,S3,T2,T3);
  return res;
}

real_t LenoirSalles3dIM::adjacentTrianglesByVertexSecantPlanesLaplace3dSLP0(const Point& S1,const Point& S2,const Point& S3,
    const Point& T2, const Point& T3) const
{
  real_t res=0.;
  Point T1=S1;
  std::vector<real_t> dS=triangleHeightsLengths(S1,S2,S3);
  std::vector<real_t> dT=triangleHeightsLengths(S1,T2,T3);
  Point I2=intersectionOfPlanesWithOneSharedPoint(S1,S2,S3,T2,T3);
  if(std::abs(dS[0])>2*theEpsilon) { res+=dS[0]*I0_SecantPlanes_U(S1,S2,S3,T1,T2,T3,S1,I2); }
  if(std::abs(dT[0])>2*theEpsilon) { res+=dT[0]*I0_SecantPlanes_U(S1,T2,T3,S1,S2,S3,S1,I2); }
  return res/3.;
}

real_t LenoirSalles3dIM::nonAdjacentTrianglesCoplanarLaplace3dSLP0(const Point & S1,const Point & S2,const Point & S3,
    const Point & T1, const Point & T2, const Point & T3) const
{
  real_t g1=0.;
  real_t res=0.;
  Point I1=projectionOnStraightLine(S1,S2,S3,g1);
  std::vector<real_t> D1=signedDistancesToTriangleEdges(S1,T1,T2,T3);
  std::vector<real_t> D2=signedDistancesToTriangleEdges(S2,T1,T2,T3);
  std::vector<real_t> G1=signedDistancesToTriangleEdges(T1,S1,S2,S3);
  std::vector<real_t> G2=signedDistancesToTriangleEdges(T2,S1,S2,S3);
  std::vector<real_t> G3=signedDistancesToTriangleEdges(T3,S1,S2,S3);
  res=(g1/3.)*(0.5*norm2(S3-S2)*I0_Coplanar_P(S3,T1,T2,T3) + 0.5*(D2[0]*I0_Coplanar_Q(S2,S3,T2,T3) + D2[1]*I0_Coplanar_Q(S2,S3,T3,T1) + D2[2]*I0_Coplanar_Q(S2,S3,T1,T2)));
  real_t Ub1 = 0.5*norm2(T3-T2)*I0_Coplanar_P(T2,S1,S2,S3) + 0.5*(G3[0]*I0_Coplanar_Q(T2,T3,S2,S3) + G3[1]*I0_Coplanar_Q(T2,T3,S3,S1) + G3[2]*I0_Coplanar_Q(T2,T3,S1,S2));
  real_t Ub2 = 0.5*norm2(T1-T3)*I0_Coplanar_P(T3,S1,S2,S3) + 0.5*(G1[0]*I0_Coplanar_Q(T3,T1,S2,S3) + G1[1]*I0_Coplanar_Q(T3,T1,S3,S1) + G1[2]*I0_Coplanar_Q(T3,T1,S1,S2));
  real_t Ub3 = 0.5*norm2(T2-T1)*I0_Coplanar_P(T1,S1,S2,S3) + 0.5*(G2[0]*I0_Coplanar_Q(T1,T2,S2,S3) + G2[1]*I0_Coplanar_Q(T1,T2,S3,S1) + G2[2]*I0_Coplanar_Q(T1,T2,S1,S2));
  res+=(1./3.)*(D1[0]*Ub1+D1[1]*Ub2+D1[2]*Ub3);
  return res;
}

real_t LenoirSalles3dIM::adjacentTrianglesByEdgeLaplace3dSLP0(const Point & S3, const Point & S1, const Point & S2, const Point& T3) const
{
  real_t res1=0.;
  const real_t theEpsilon_ = DBL_EPSILON;
  Point C=crossProduct((S3-S2), (S3-S1));
  C*=(1./norm2(C));
  Point Cb=(S3-T3)*(1./norm2((S3-T3)));
  real_t res=dot(Cb,C);
  if(std::abs(res)<100.*theEpsilon_) { res1=adjacentTrianglesByEdgeCoplanarLaplace3dSLP0(S3,S1,S2,T3); }
  else { res1=adjacentTrianglesByEdgeSecantPlanesLaplace3dSLP0(S3,S1,S2,T3); }
  return res1;
}

real_t LenoirSalles3dIM::adjacentTrianglesByEdgeSecantPlanesLaplace3dSLP0(const Point& S3,const Point& S1,const Point& S2,const Point& T3) const
{
  real_t g1,g2,d1,d2;
  real_t res=0.;
  Point I1=projectionOnStraightLine(S1,S2,S3,g1);
  Point I2=projectionOnStraightLine(S2,S3,S1,g2);
  Point J1=projectionOnStraightLine(S1,S2,T3,d1);
  Point J2=projectionOnStraightLine(S2,T3,S1,d2);
  real_t aireS=0.5*g2*norm2(S1-S3);
  real_t aireT=0.5*d2*norm2(T3-S1);
  res=aireS/(3.)*I0_SecantPlanes_P(S3,S1,S2,T3)+aireT/(3.)*I0_SecantPlanes_P(T3,S1,S2,S3);
  res+=g1*d2/(6.)*I0_SecantPlanes_Q(S2,S3,T3,S1)+ g2*d1/(6.)*I0_SecantPlanes_Q(S3,S1,S2,T3);
  return res;
}

real_t LenoirSalles3dIM::nonAdjacentTrianglesSecantPlanesLaplace3dSLP0(const Point& S1,const Point& S2,const Point& S3,
    const Point& T1, const Point& T2, const Point& T3) const
{
  real_t res=0.;
  std::pair<Point,Point> intersect=intersectionOfPlanes(S1,S2,S3,T1,T2,T3);
  Point& I1=intersect.first;
  Point& I2=intersect.second;
  std::vector<real_t> G=signedDistancesToTriangleEdges(I1,S1,S2,S3);
  std::vector<real_t> D=signedDistancesToTriangleEdges(I1,T1,T2,T3);
  if(std::abs(G[0])>10.*theEpsilon) { res+=G[0]*I0_SecantPlanes_U(S1,S2,S3,T1,T2,T3,I1,I2); }
  if(std::abs(G[1])>10.*theEpsilon) { res+=G[1]*I0_SecantPlanes_U(S2,S3,S1,T1,T2,T3,I1,I2); }
  if(std::abs(G[2])>10.*theEpsilon) { res+=G[2]*I0_SecantPlanes_U(S3,S1,S2,T1,T2,T3,I1,I2); }
  if(std::abs(D[0])>10.*theEpsilon) { res+=D[0]*I0_SecantPlanes_U(T1,T2,T3,S1,S2,S3,I1,I2); }
  if(std::abs(D[1])>10.*theEpsilon) { res+=D[1]*I0_SecantPlanes_U(T2,T3,T1,S1,S2,S3,I1,I2); }
  if(std::abs(D[2])>10.*theEpsilon) { res+=D[2]*I0_SecantPlanes_U(T3,T1,T2,S1,S2,S3,I1,I2); }
  res/=3.;
  return res;
}

//-------------------------------------------------------------------------------
// Coplanar triangles
//-------------------------------------------------------------------------------
real_t LenoirSalles3dIM::I0_Coplanar_R(const  Point& I, const  Point& A, const  Point& B, real_t h) const
{
  real_t r=0.;
  Point V=B-A;
  real_t seglength=norm2(V);
  Point Im=A-I;
  Point Ip=B-I;
  real_t sm=dot(Im,V)/seglength;
  real_t sp=dot(Ip,V)/seglength;
  if(h>2.*theEpsilon) { r=xlifepp::asinh(sp/h) - xlifepp::asinh(sm/h);}
  return r;
}

real_t LenoirSalles3dIM::I0_Coplanar_Rp(const Point& I, const Point& Am, const Point& Ap, real_t h) const
{
  real_t r=0.;
  Point V=Ap-Am;
  real_t seglength=norm2(V);
  Point Im=Am-I;
  Point Ip=Ap-I;
  real_t sm=dot(Im,V)/seglength;
  real_t sp=dot(Ip,V)/seglength;
  real_t h2=h*h;
  if(h>2*theEpsilon)
    {
      if(std::abs(sp)>2*theEpsilon) { r+=(-(std::sqrt(h2+sp*sp)-h)/sp + xlifepp::asinh(sp/h)); }
      if(std::abs(sm)>2*theEpsilon) { r-=(-(std::sqrt(h2+sm*sm)-h)/sm + xlifepp::asinh(sm/h)); }
    }
  return r;
}

real_t LenoirSalles3dIM::I0_Coplanar_P(const Point& I, const Point& A, const Point& B, const Point& C) const
{
  real_t r=0.;
  std::vector<real_t> gamma=signedDistancesToTriangleEdges(I,A,B,C);
  Point Itmp(0., 0., 0.);
  real_t h=0.;
  if(std::abs(gamma[0])>5.*theEpsilon)
    {
      Itmp=projectionOnStraightLine(I,B,C,h);
      r+=gamma[0]*I0_Coplanar_R(Itmp,B,C,h);
    }
  if(std::abs(gamma[1])>5.*theEpsilon)
    {
      Itmp=projectionOnStraightLine(I,C,A,h);
      r+=gamma[1]*I0_Coplanar_R(Itmp,C,A,h);
    }
  if(std::abs(gamma[2])>5.*theEpsilon)
    {
      Itmp=projectionOnStraightLine(I,A,B,h);
      r+=gamma[2]*I0_Coplanar_R(Itmp,A,B,h);
    }
  return r;
}

real_t LenoirSalles3dIM::I0_Coplanar_Q(const Point& Am, const Point& Ap, const Point& Bm, const Point& Bp) const
{
  real_t r=0.;
  bool hasIntersection=0;
  Point I=intersectionOfStraightLines(Am,Ap,Bm,Bp,hasIntersection);
  if(hasIntersection)  // segments are not parallel
    {
      real_t hm,hp,lm,lp;
      Point Im=projectionOnStraightLine(Am,Bm,Bp,hm);
      Point Ip=projectionOnStraightLine(Ap,Bm,Bp,hp);
      Point Jm=projectionOnStraightLine(Bm,Am,Ap,lm);
      Point Jp=projectionOnStraightLine(Bp,Am,Ap,lp);
      real_t nA=norm2(Ap-Am);
      Point A=Ap-Am;
      real_t nB=norm2(Bp-Bm);
      Point B=Bp-Bm;
      real_t sm=dot(Am-I,A)/nA;
      real_t sp=dot(Ap-I,A)/nA;
      real_t tm=dot(Bm-I,B)/nB;
      real_t tp=dot(Bp-I,B)/nB;
      if(std::abs(hp)>2*theEpsilon) { r+=sp*I0_Coplanar_R(Ap,Bm,Bp,hp); }
      if(std::abs(hm)>2*theEpsilon) { r-=sm*I0_Coplanar_R(Am,Bm,Bp,hm); }
      if(std::abs(lp)>2*theEpsilon) { r+=tp*I0_Coplanar_R(Bp,Am,Ap,lp); }
      if(std::abs(lm)>2*theEpsilon) { r-=tm*I0_Coplanar_R(Bm,Am,Ap,lm); }
    }
  else // segments are parallel
    {
      real_t hm, hp, h;
      Point Bmp=projectionOnStraightLine(Bm,Am,Ap,hm);
      Point Bpp=projectionOnStraightLine(Bp,Am,Ap,hp);
      h=0.5*(hm+hp);
      real_t nA=norm2(Ap-Am);
      real_t nB=norm2(Bpp-Bmp);
      real_t sp=nA;
      real_t tm=dot(Bmp-Am,Bpp-Bmp)/nB;
      real_t tp=dot(Bpp-Am,Bpp-Bmp)/nB;
      r=sp*I0_Coplanar_Rp(Ap,Bmp,Bpp,h);
      if(std::abs(tp)>2*theEpsilon) { r+=tp*I0_Coplanar_Rp(Bpp,Am,Ap,h); }
      if(std::abs(tm)>2*theEpsilon) { r-=tm*I0_Coplanar_Rp(Bmp,Am,Ap,h); }
    }
  return r;
}

real_t LenoirSalles3dIM::nonAdjacentTrianglesParallelPlanesLaplace3dSLP0(const Point& S1,const Point& S2,const Point& S3,
    const Point& T1,const Point& T2,const Point& T3) const
{
  real_t h=0.;
  std::vector<Point>Tp=projectionOfTriangleOnPlane(T1,T2,T3,S1,S2,S3,h);
  Vector< Point > Sp(3);
  Sp[0]=S1;
  Sp[1]=S2;
  Sp[2]=S3;
  Vector< number_t > indi(3,0), indj(3,0);
  real_t res=0.;
  number_t i1=0, j1=0, SC=0;
  for(number_t i=0; i<3; i++)
    for(number_t j=0; j<3; j++)
      if(Sp[i]==Tp[j])
        {
          SC ++;
          indi[i1]=i;
          i1++;
          indj[j1]=j;
          j1++;
        }
  if(SC==3) { res=I0_ParallelPlanes_3S(S1,S2,S3,h); }
  if(SC==2)
    {
      indi[2]=3-indi[0]-indi[1];
      indj[2]=3-indj[0]-indj[1];
      res = I0_ParallelPlanes_2S(Sp[indi[0]],Sp[indi[1]],Sp[indi[2]],Tp[indj[2]],h);
    }
  if(SC==1)
    {
      indi[1]=int((-1.5)*indi[0]*indi[0]+(2.5)*(indi[0])+1);
      indi[2] = int((1.5)*indi[0] * indi[0] - (3.5)*indi[0] + 2);
      indj[1] = int((-1.5)*indj[0] * indj[0] + (2.5)*indj[0] + 1);
      indj[2] = int((1.5)*indj[0] * indj[0] - (3.5)*indj[0] + 2);
      res = I0_ParallelPlanes_1S(Sp[indi[0]],Sp[indi[1]],Sp[indi[2]],Tp[indj[1]],Tp[indj[2]],h);
    }
  if(SC==0) { res=I0_ParallelPlanes_0S(S1,S2,S3,Tp[0],Tp[1],Tp[2],h); }
  return res;
}

//-------------------------------------------------------------------------------
// triangles in secant planes
//-------------------------------------------------------------------------------
real_t LenoirSalles3dIM::I0_SecantPlanes_R(const Point& B, const Point& Am, const Point& Ap, const real_t h) const
{
  real_t res=0.;
  real_t d=0.;
  Point I=projectionOnStraightLine(B,Am,Ap,d);
  Point beta=Ap-Am;
  real_t nB=norm2(beta);
  real_t sm=dot(Am-I,beta)/nB;
  real_t sp=dot(Ap-I,beta)/nB;
  real_t d2=d*d;
  real_t h2=h*h;
  real_t R2=d2+h2;
  if(d>xlifepp::theEpsilon)
    {
      real_t hd=h/d;
      real_t sp2=sp*sp;
      real_t sm2=sm*sm;
      res+=xlifepp::asinh(sp/std::sqrt(R2))- hd*(std::atan(sp/d) - std::atan(sp*h/(d*std::sqrt(R2+sp2))));
      res-=xlifepp::asinh(sm/std::sqrt(R2))- hd*(std::atan(sm/d) - std::atan(sm*h/(d*std::sqrt(R2+sm2))));
    }
  else
    {
      if(std::abs(sp)>xlifepp::theEpsilon) { res+=(h-std::sqrt(h2+sp*sp))/sp + xlifepp::asinh(sp/h); }
      if(std::abs(sm)>xlifepp::theEpsilon) { res-=(h-std::sqrt(h2+sm*sm))/sm + xlifepp::asinh(sm/h); }
    }
  return res;
}

real_t LenoirSalles3dIM::I0_SecantPlanes_Rp(const Point& B, const Point& Am, const Point& Ap, const real_t h) const
{
  real_t res=0.;
  real_t d=0.;
  Point I=projectionOnStraightLine(B,Am,Ap,d);
  Point beta= Ap-Am;
  real_t sm=dot(Am-I,beta)*(1./norm2(beta));
  real_t sp=dot(Ap-I,beta)*(1./norm2(beta));
  real_t h2=h*h;
  real_t d2=d*d;
  real_t sp2=sp*sp;
  real_t sm2=sm*sm;
  if(d>2.*theEpsilon && h>2.*theEpsilon)
    {
      real_t hd=h/d;
      real_t R=std::sqrt(d2+h2);
      real_t R2=d2+h2;

      if(std::abs(sp)>2.*theEpsilon)  // f(0,h,d) = 0.
        {
          res+=(h2*sp)/(2.*d2*std::sqrt(d2+sp2))*xlifepp::asinh(std::sqrt(d2+sp2)/h) - hd*(std::atan(sp/d) - std::atan(hd*sp/std::sqrt(R2+sp2))) + 0.5*(1.-h2/d2)*xlifepp::asinh(sp/R);
        }
      if(std::abs(sm)>2.*theEpsilon)
        {
          res-=(h2*sm)/(2.*d2*std::sqrt(d2+sm2))*xlifepp::asinh(std::sqrt(d2+sm2)/h) - hd*(std::atan(sm/d) - std::atan(h*sm/(d*std::sqrt(R2+sm2)))) + 0.5*(1.-h2/d2)*xlifepp::asinh(sm/R);
        }
    }
  else
    {
      std::cout << "Rp, h or d = 0!!" << eol;
      if(h<2.*theEpsilon) { std::cout << "I0_SecantPlanes_Rp:: h=0" << eol; }
      if(d<2.*theEpsilon)
        {
          std::cout << "Rp, d=0!!" << eol;
          if(std::abs(sp)>2.*theEpsilon)
            {
              real_t hs=h/sp;
              res+=hs-0.75*std::sqrt(h2+sp2)/sp + (0.5-0.25*hs*hs)*xlifepp::asinh(sp/h);
            }
          if(std::abs(sm)>2.*theEpsilon)
            {
              real_t hs=h/sm;
              res-=hs-0.75*std::sqrt(h2+sm2)/sm + (0.5-0.25*hs*hs)*xlifepp::asinh(sm/h);
            }
        }
    }
  return res;
}

real_t LenoirSalles3dIM::I0_SecantPlanes_Pp(const Point& A, const Point& S1, const Point& S2, const Point& S3, const real_t h) const
{
  // A is in the plane of (S1,S2,S3)
  std::vector<real_t> D=signedDistancesToTriangleEdges(A, S1, S2, S3);
  real_t res=0.;
  if(std::abs(D[0]) >theEpsilon) { res += D[0]*I0_SecantPlanes_Rp(A,S2,S3,h); }
  if(std::abs(D[1]) >theEpsilon) { res += D[1]*I0_SecantPlanes_Rp(A,S3,S1,h); }
  if(std::abs(D[2]) >theEpsilon) { res += D[2]*I0_SecantPlanes_Rp(A,S1,S2,h); }
  return res;
}

real_t LenoirSalles3dIM::I0_SecantPlanes_Rpp(const Point& B, const Point& Am, const Point& Ap, const real_t h, const real_t d) const
{
  real_t res=0.;
  if(h<2.*theEpsilon) { std::cout<<"Rpp, h=0" <<std::endl; }
  if(d<2.*theEpsilon) { std::cout<<"Rpp, d=0" <<std::endl; }
  if(d>=2.*theEpsilon)  //d~=0
    {
      Point beta=Ap-Am;
      real_t nB=norm2(beta);
      real_t sm=dot(Am-B,beta)/nB;
      real_t sp=dot(Ap-B,beta)/nB;
      real_t d2=d*d;
      real_t h2=h*h;
      real_t R=std::sqrt(d*d+h*h);
      real_t Rb=d2+h2;

      // if < 40000, problem when |s| << 1 (f(s)->0 but in c++ f(s) there is an instability for small s
      if(std::abs(sp)>40000.*theEpsilon)
        {
          real_t sp2=sp*sp;
          res+=((R-std::sqrt(Rb+sp2))/(2.*sp) + Rb/(2.*d2)*xlifepp::asinh(sp/R) - (h2*std::sqrt(sp2+d2)/(2.*sp*d2))*xlifepp::asinh(std::sqrt(sp2+d2)/h) + (h2/(2.*d*sp))*xlifepp::asinh(d/h));
        }
      if(std::abs(sm)>40000.*theEpsilon)
        {
          real_t sm2=sm*sm;
          res-= ((R-std::sqrt(Rb+sm2))/(2.*sm)+ Rb/(2.*d2)*xlifepp::asinh(sm/R)- (h2*std::sqrt(sm2+d2)/(2.*sm*d2))*xlifepp::asinh(std::sqrt(sm2+d2)/h) + (h2/(2.*d*sm))*xlifepp::asinh(d/h));
        }
    }
  else // STILL TO BE VALIDATED !!!
    {
      std::cout<<"Rpp d=0!!" <<std::endl;
      Point beta= Ap-Am;
      real_t sm=dot(Am-B,beta)/norm2(beta);
      real_t sp=dot(Ap-B,beta)/norm2(beta);
      real_t h2=h*h;

      if((std::abs(sp)>2*theEpsilon) && (std::abs(sm)>2*theEpsilon))
        {
          res= (2.*h/sp - 3.*std::sqrt(h2+sp*sp)/(2.*sp) + xlifepp::asinh(sp/h) - (xlifepp::atanh(std::abs(sp)/std::sqrt(sp*sp+h2))*h2)/(2.*sp*std::abs(sp))) \
               - (2.*h/sm - 3.*std::sqrt(h2+sm*sm)/(2.*sm) + xlifepp::asinh(sm/h) - (xlifepp::atanh(std::abs(sm)/std::sqrt(sm*sm+h2))*h2)/(2.*sm*std::abs(sm)));
        }
      else
        {
          if(std::abs(sp)<= 2.*theEpsilon)
            {
              if(std::abs(sm)>2.*theEpsilon)
                {
                  res= -(2.*h/sm - 3.*std::sqrt(h2+sm*sm)/(2.*sm) + xlifepp::asinh(sm/h) - (xlifepp::atanh(std::abs(sm)/std::sqrt(sm*sm+h2))*h2)/(2.*sm*std::abs(sm)));
                }
              else { res=0.; }
            }
          else
            {
              res=(2.*h/sp - 3.*std::sqrt(h2+sp*sp)/(2*sp) + xlifepp::asinh(sp/h) - (xlifepp::atanh(std::abs(sp)/std::sqrt(sp*sp+h2))*h2)/(2.*sp*std::abs(sp)));
            }
        }
    }
  return res;
}

real_t LenoirSalles3dIM::I0_SecantPlanes_P(const Point& B, const Point& S1, const Point& S2, const Point& S3) const
{
  real_t res=0.;
  real_t h=0.;
  Point Bp=projectionOfPointOnPlane(B,S1,S2,S3,h);
  std::vector<real_t> gamma=signedDistancesToTriangleEdges(Bp,S1,S2,S3);
  if(std::abs(gamma[0])>2.*theEpsilon) { res+=gamma[0]*I0_SecantPlanes_R(Bp,S2,S3,h); }
  if(std::abs(gamma[1])>2.*theEpsilon) { res+=gamma[1]*I0_SecantPlanes_R(Bp,S3,S1,h); }
  if(std::abs(gamma[2])>2.*theEpsilon) { res+=gamma[2]*I0_SecantPlanes_R(Bp,S1,S2,h); }
  return res;
}

real_t LenoirSalles3dIM::I0_SecantPlanes_Q(const Point& Am, const Point& Ap, const Point& Bm, const Point& Bp) const
{
  real_t res=0.;
  std::pair<Point,Point> perp=commonPerpendicularOfStraightLines(Am,Ap,Bm,Bp);
  Point& I1=perp.first; // Intersection Perp Comm et [Am,Ap]
  Point& I2=perp.second; // Intersection Perp Comm et [Bm,Bp]
  Point V=I2-I1;
  real_t h=norm2(V); // distance between the two planes
  Point Amp=Am+V;
  Point App=Ap+V;
  bool hasIntersection;
  Point I=intersectionOfStraightLines(Amp,App,Bm,Bp,hasIntersection);
  // The intersection exists since (Am,Ap) is not parallel to (Bm,Bp)
  Point alpha=Ap-Am;
  Point beta=Bp-Bm;
  real_t nA=norm2(alpha);
  real_t nB=norm2(beta);
  real_t nAi=1./nA;
  real_t nBi=1./nB;
  real_t sm=dot(Amp-I,alpha)*nAi;
  real_t sp=dot(App-I,alpha)*nAi;
  real_t tm=dot(Bm-I,beta)*nBi;
  real_t tp=dot(Bp-I,beta)*nBi;
  if(std::abs(sp)>2*theEpsilon) { res+=sp*I0_SecantPlanes_R(App,Bm,Bp,h); }
  if(std::abs(sm)>2*theEpsilon) { res-=sm*I0_SecantPlanes_R(Amp,Bm,Bp,h); }
  if(std::abs(tp)>2*theEpsilon) { res+=tp*I0_SecantPlanes_R(Bp,Amp,App,h); }
  if(std::abs(tm)>2*theEpsilon) { res-=tm*I0_SecantPlanes_R(Bm,Amp,App,h); }
  return res;
}

real_t LenoirSalles3dIM::I0_SecantPlanes_Qp(const Point& Am, const Point& Ap, const Point& Bm, const Point& Bp, const real_t h) const
{
  // (Am,Ap) is parallel to omega (intersection of the planes)
  real_t res=0.;
  bool hasIntersection;
  Point I=intersectionOfStraightLines(Am, Ap, Bm, Bp, hasIntersection);
  if(hasIntersection)  // segments are not parallel
    {
      Point alpha=Ap-Am;
      Point beta=Bp-Bm;
      real_t nA=norm2(Ap-Am);
      real_t nB=norm2(Bp-Bm);
      real_t sm=dot(Am-I,alpha)/nA;
      real_t sp=dot(Ap-I,alpha)/nA;
      real_t tm=dot(Bm-I,beta)/nB;
      real_t tp=dot(Bp-I,beta)/nB;
      if(std::abs(sp)>40.*theEpsilon) { res+=sp*I0_SecantPlanes_Rp(Ap,Bm,Bp,h); }
      if(std::abs(sm)>40.*theEpsilon) { res-=sm*I0_SecantPlanes_Rp(Am,Bm,Bp,h); }
      if(std::abs(tp)>40.*theEpsilon) { res+=tp*I0_SecantPlanes_Rp(Bp,Am,Ap,h); }
      if(std::abs(tm)>40.*theEpsilon) { res-=tm*I0_SecantPlanes_Rp(Bm,Am,Ap,h); }
    }
  else // segments are parallel
    {
      real_t h1=0.;
      real_t h2=0.;
      Point Amp=projectionOnStraightLine(Am, Bm, Bp, h1);
      Point App=projectionOnStraightLine(Ap, Bm, Bp, h2);
      real_t d=0.5*(h1+h2);
      Point alpha=App-Amp;
      Point beta=Bp-Bm;
      real_t nA=norm2(alpha);
      real_t nB=norm2(beta);
      real_t sp=nA;
      real_t tm=dot(Bm-Amp,beta)/nB;
      real_t tp=dot(Bp-Amp,beta)/nB;
      if(std::abs(sp)>2.*theEpsilon) { res+=sp*I0_SecantPlanes_Rpp(App,Bm,Bp,h,d); }
      if(std::abs(tp)>40.*theEpsilon) { res+=tp*I0_SecantPlanes_Rpp(Bp,Amp,App,h,d); }
      if(std::abs(tm)>40.*theEpsilon) { res-=tm*I0_SecantPlanes_Rpp(Bm,Amp,App,h,d); }
    }
  return res;
}

real_t LenoirSalles3dIM::I0_SecantPlanes_U(const Point& S1, const Point& S2, const Point& S3,
    const Point& T1, const Point& T2, const Point& T3,const Point& I1,const Point& I2) const
{
  // Compute U of (S2,S3) times Triangle T
  // Intersection of the 2 planes defined by (I1,I2)

  // We compute the intersection between
  // alpha and (w) defined by I1, I2
  Point Am=S2;
  Point Ap=S3;
  Point alpha=Ap-Am;
  real_t nam=1./norm2(alpha);
  bool hasIntersection;
  Point O=intersectionOfStraightLines(Am,Ap,I1,I2,hasIntersection);
  real_t res=0.;
  if(hasIntersection)  // segments are not parallel
    {
      real_t sm=dot(Am-O,alpha)*nam;
      real_t sp=dot(Ap-O,alpha)*nam;
      // computation of dj
      std::vector<real_t> d=signedDistancesToTriangleEdges(O,T1,T2,T3);
      if(std::abs(sp)>10.*theEpsilon) { res+=sp*I0_SecantPlanes_P(Ap,T1,T2,T3); }
      if(std::abs(sm)>10.*theEpsilon) { res-=sm*I0_SecantPlanes_P(Am,T1,T2,T3); }
      if(std::abs(d[0])>10.*theEpsilon) { res+=d[0]*I0_SecantPlanes_Q(S2,S3,T2,T3); }
      if(std::abs(d[1])>10.*theEpsilon) { res+=d[1]*I0_SecantPlanes_Q(S2,S3,T3,T1); }
      if(std::abs(d[2])>10.*theEpsilon) { res+=d[2]*I0_SecantPlanes_Q(S2,S3,T1,T2); }
      return 0.5*res;
    }
  else // segments are parallel
    {
      real_t h=0.;
      std::pair<Point,Point> proj=projectionOfSegmentOnPlane(Am,Ap,T1,T2,T3,h);
      Point& Amp=proj.first;
      Point& App=proj.second;
      real_t r=I0_SecantPlanes_Pp(App,T1,T2,T3,h);
      res=norm2(Ap-Am)*r;
      std::vector<real_t> d=signedDistancesToTriangleEdges(Amp,T1,T2,T3);
      if(std::abs(d[0])>10.*theEpsilon) { res+=d[0]*I0_SecantPlanes_Qp(Amp,App,T2,T3,h); }
      if(std::abs(d[1])>10.*theEpsilon) { res+=d[1]*I0_SecantPlanes_Qp(Amp,App,T3,T1,h); }
      if(std::abs(d[2])>10.*theEpsilon) { res+=d[2]*I0_SecantPlanes_Qp(Amp,App,T1,T2,h); }
    }
  return res;
}

//-------------------------------------------------------------------------------
// triangles in parallel planes
//-------------------------------------------------------------------------------
real_t LenoirSalles3dIM::I0_ParallelPlanes_R(const Point& M, const Point& Tm, const Point& Tp, real_t h) const
{
  real_t res=0.;
  real_t d=0.;
  Point I=projectionOnStraightLine(M,Tm,Tp,d);
  Point beta=Tp-Tm;
  real_t nb=norm2(beta);
  Point Im=Tm-I;
  Point Ip=Tp-I;
  real_t tm=dot(Im,beta)/nb;
  real_t tp=dot(Ip,beta)/nb;
  if(d>5.*theEpsilon)
    {
      real_t h2=h*h;
      real_t d2=d*d;
      real_t R=std::sqrt(d2+h2);
      real_t R2=d2+h2;
      real_t hd=h/d;
      real_t hd2=hd*hd;
      real_t tp2=tp*tp;
      real_t tm2=tm*tm;
      real_t Rp=std::sqrt(d2+tp2);
      real_t Rm=std::sqrt(d2+tm2);
      real_t C1=1.-3.*(hd2);
      real_t C2=hd*(hd2-3.);
      real_t f1=xlifepp::asinh(tp/R)-xlifepp::asinh(tm/R);
      real_t f2=std::atan(tp/d)-std::atan(tm/d)-(std::atan(h*tp/(d*std::sqrt(R2+tp2))) - std::atan(h*tm/(d*std::sqrt(R2+tm2))));
      real_t f4=tp/Rp*xlifepp::asinh(Rp/h)-tm/Rm*xlifepp::asinh(Rm/h);
      real_t f5=tp*(std::sqrt(R2+tp2)-h)/(Rp*Rp)-tm*(std::sqrt(R2+tm2)-h)/(Rm*Rm);
      res=C1*f1+C2*f2+hd2*(3.*f4-f5); //formule 73 du poly
    }
  else { std::cout << "I0_ParallelPlanes_R: d==0" << eol; }
  return res/6.;
}

real_t LenoirSalles3dIM::I0_ParallelPlanes_P(const Point& M, const Point& T1, const Point& T2, const Point& T3, real_t h) const
{
  /*
   (T1,T2,T3) defines a triangle
   h is the distance between the two planes
   M is the point where we evaluate the "potential"
   */
  real_t res=0.;
  std::vector<real_t> d=signedDistancesToTriangleEdges(M, T1, T2, T3);
  if(std::abs(d[0])>2*theEpsilon) { res+=d[0]*I0_ParallelPlanes_R(M, T2, T3, h); }
  if(std::abs(d[1])>2*theEpsilon) { res+=d[1]*I0_ParallelPlanes_R(M, T3, T1, h); }
  if(std::abs(d[2])>2*theEpsilon) { res+=d[2]*I0_ParallelPlanes_R(M, T1, T2, h); }
  return res;
}

real_t LenoirSalles3dIM::I0_ParallelPlanes_Rp(const Point& M, const Point& Tm, const Point& Tp, real_t h, real_t D) const
{
  real_t r=0.;
  Point V=Tp-Tm;
  real_t nB=norm2(V);
  Point Im=Tm-M;
  Point Ip=Tp-M;
  real_t sm=dot(Im,V)/nB;
  real_t sp=dot(Ip,V)/nB;
  real_t D2=D*D;
  real_t h2=h*h;
  real_t Dh2=D2/h2;
  real_t R=std::sqrt(D2+h2);
  real_t R2=D2+h2;
  if(h>5.0*theEpsilon)
    {
      if(std::abs(sp)>5.*theEpsilon)
        {
          real_t sp2=sp*sp;

          r= r + ((1.-(2.)*Dh2)*(R-std::sqrt(sp2+R2))/sp)
             + (1.0+(3.0)*Dh2)*xlifepp::asinh(sp/R)
             - ((2.0)*(Dh2)*(D/h))*(-1.*(std::atan(D*sp/(h*std::sqrt(R2+sp2)))) + std::atan(sp/h))
             - ((3.0)*(Dh2))*((std::sqrt(sp2+h2)/sp)*xlifepp::asinh(std::sqrt(sp2+h2)/D)-(h/sp)*xlifepp::asinh(h/D));
        }
      if(std::abs(sm)>5.*theEpsilon)
        {
          real_t sm2=sm*sm;
          r = r - ((1.-(2.)*(Dh2))*(R-std::sqrt(sm2+R2))/sm)
              - (1.+(3.)*(Dh2))*xlifepp::asinh(sm/std::sqrt(D2+h2))
              + ((2.)*(Dh2)*(D/h))*(-1.*(std::atan(D*sm/(h*std::sqrt(R2+sm2))))+std::atan(sm/h))
              + ((3.)*(Dh2))*((std::sqrt(sm2+h2)/sm)*xlifepp::asinh(std::sqrt(sm2+h2)/D)-(h/sm)*xlifepp::asinh(h/D));
        }
    }
  else
    {
      if(std::abs(sp)>5.*theEpsilon)
        {
          r= r + std::sqrt(sp*sp+D2)/(6.*sp*sp*sp)*(4.*D2-11.*sp*sp) - (D/(3.*sp*sp*sp))*(2.*D2-9.*sp*sp)
             +(1.-3.*D2/(2.*sp*sp))*xlifepp::asinh(sp/D);
        }
      if(std::abs(sm)>5.*theEpsilon)
        {
          real_t sm2=sm*sm;
          real_t sm3=sm2*sm;
          r=r- (std::sqrt(sm2+D2)/(6.*sm3)*(4.*D2-11.*sm2) - (D/(3.*sm3))*(2.*D2-9.*sm2)
                +(1.-3.*D2/(2.*sm2))*xlifepp::asinh(sm/D));
        }
    }
  return r/6.0;
}

real_t LenoirSalles3dIM::I0_ParallelPlanes_Q(const Point& Am, const Point& Ap, const Point& Bm, const Point& Bp, real_t h) const
{
  real_t r=0.;
  bool hasIntersection;
  Point I=intersectionOfStraightLines(Am,Ap,Bm,Bp,hasIntersection);
  if(hasIntersection)  // segments are not parallel
    {
      Point alpha=Ap-Am;
      Point beta=Bp-Bm;
      real_t nA=norm2(alpha);
      real_t nB=norm2(beta);
      real_t sm=dot(Am-I,alpha)/nA;
      real_t sp=dot(Ap-I,alpha)/nA;
      real_t tm=dot(Bm-I,beta)/nB;
      real_t tp=dot(Bp-I,beta)/nB;
      if(std::abs(sp)>2.*theEpsilon) { r+=sp*I0_ParallelPlanes_R(Ap,Bm, Bp,h); }
      if(std::abs(sm)>2.*theEpsilon) { r-=sm*I0_ParallelPlanes_R(Am,Bm,Bp,h); }
      if(std::abs(tp)>2.*theEpsilon) { r+=tp*I0_ParallelPlanes_R(Bp,Am,Ap,h); }
      if(std::abs(tm)>2.*theEpsilon) { r-=tm*I0_ParallelPlanes_R(Bm,Am,Ap,h); }
    }
  else // segments are parallel
    {
      real_t hm, hp;
      Point Bmp=projectionOnStraightLine(Bm,Am,Ap,hm);
      Point Bpp=projectionOnStraightLine(Bp,Am,Ap,hp);
      real_t d=0.5*(hm+hp);
      Point beta=Bpp-Bmp;
      real_t nA=norm2(Ap-Am);
      real_t nB=norm2(Bpp-Bmp);
      real_t sp=nA;
      real_t tm=dot(Bmp-Am,beta)/nB;
      real_t tp=dot(Bpp-Am,beta)/nB;
      r=sp*I0_ParallelPlanes_Rp(Ap,Bmp,Bpp,d,h);
      if(std::abs(tp)>2.*theEpsilon) { r+=tp*I0_ParallelPlanes_Rp(Bpp,Am,Ap,d,h); }
      if(std::abs(tm)>2.*theEpsilon) { r-=tm*I0_ParallelPlanes_Rp(Bmp,Am,Ap,d,h); }
    }
  return r;
}

real_t LenoirSalles3dIM::I0_ParallelPlanes_U(const Point& Am, const Point& Ap, const Point& T1, const Point& T2, const Point& T3, real_t h) const
{
  real_t nA=norm2(Ap-Am);
  std::vector<real_t> d=signedDistancesToTriangleEdges(Am, T1, T2, T3);
  real_t r=nA*I0_ParallelPlanes_P(Ap,T1, T2, T3,h);
  if(std::abs(d[0])>theEpsilon) { r+=d[0]*I0_ParallelPlanes_Q(Am,Ap,T2,T3,h); }
  if(std::abs(d[1])>theEpsilon) { r+=d[1]*I0_ParallelPlanes_Q(Am,Ap,T3,T1,h); }
  if(std::abs(d[2])>theEpsilon) { r+=d[2]*I0_ParallelPlanes_Q(Am,Ap,T1,T2,h); }
  return r;
}

real_t LenoirSalles3dIM::I0_ParallelPlanes_0S(const Point& S1, const Point& S2, const Point& S3,
    const Point& T1,const Point& T2,const Point& T3, real_t h) const
{
  std::vector<real_t> d=signedDistancesToTriangleEdges(S1,T1,T2,T3);
  std::vector<real_t> dS=triangleHeightsLengths(S1, S2, S3);
  real_t r=dS[0]*I0_ParallelPlanes_U(S2,S3,T1,T2,T3,h);
  if(std::abs(d[0])>theEpsilon) { r+=d[0]*I0_ParallelPlanes_U(T2,T3,S1,S2,S3,h); }
  if(std::abs(d[1])>theEpsilon) { r+=d[1]*I0_ParallelPlanes_U(T3,T1,S1,S2,S3,h); }
  if(std::abs(d[2])>theEpsilon) { r+=d[2]*I0_ParallelPlanes_U(T1,T2,S1,S2,S3,h); }
  return r;
}

real_t LenoirSalles3dIM::I0_ParallelPlanes_1S(const Point& S1, const Point& S2, const Point& S3, const Point& T2, const Point & T3, real_t h) const
{
  /*
    triangles (S1,S2,S3) and (S1,T2,T3) in the same plane
    h: distance between the two planes   */

  std::vector<real_t> dS=triangleHeightsLengths(S1, S2, S3);
  std::vector<real_t> dT=triangleHeightsLengths(S1, T2, T3);
  real_t r=dS[0]*I0_ParallelPlanes_U(S2,S3,S1,T2,T3,h)+dT[0]*I0_ParallelPlanes_U(T2,T3,S1,S2,S3,h);
  return r;
}

real_t LenoirSalles3dIM::I0_ParallelPlanes_2S(const Point& S1, const Point& S2, const Point& S3, const Point& T3,real_t h) const
{
  /*
   triangles (S1,S2,S3) and (S1,S2,T3) in the same plane
   h: distance between the two planes
   */
  std::vector<real_t> dS=triangleHeightsLengths(S1, S2, S3);
  std::vector<real_t> dT=triangleHeightsLengths(S1, S2, T3);
  real_t aireS=0.5*norm2(S3-S2)*dS[0];
  real_t aireT=0.5*norm2(S1-S2)*dT[2];

  real_t r=2.*(aireS*I0_ParallelPlanes_P(S3,S1,S2,T3,h)+aireT*I0_ParallelPlanes_P(T3,S1,S2,S3,h));
  r+=(dS[1]*dT[0]*I0_ParallelPlanes_Q(S3,S1,S2,T3,h) + dT[1]*dS[0]*I0_ParallelPlanes_Q(S2,S3,T3,S1,h));
  return r;
}

real_t LenoirSalles3dIM::I0_ParallelPlanes_3S(const Point& S1, const Point& S2, const Point& S3, real_t h) const
{
  std::vector<real_t> dS=triangleHeightsLengths(S1, S2, S3);
  real_t aireS=0.5*norm2(S3-S1)*dS[0];
  real_t r= 4.*aireS*(dS[0]*I0_ParallelPlanes_R(S1,S2,S3,h)+dS[1]*I0_ParallelPlanes_R(S2,S3,S1,h)+dS[2]*I0_ParallelPlanes_R(S3,S1,S2,h));
  return r;
}

//-------------------------------------------------------------------------------
// 3d tools
//-------------------------------------------------------------------------------
int LenoirSalles3dIM::testDistanceBetweenTriangles(const Point& S1,const Point& S2,const Point& S3,
    const Point& T1,const Point& T2,const Point& T3) const
{
  int R=0;
  // Centers of Gravity
  Point GS=(1./3.)*(S1+S2+S3);
  Point GT=(1./3.)*(T1+T2+T3);
  real_t lS=std::min({norm2(S1-S2), norm2(S1-S3), norm2(S2-S3)});
  real_t lT=std::min({norm2(T1-T2), norm2(T1-T3), norm2(T2-T3)});
  real_t h=3.*norm2(GS-GT);
  if((h<lS)&&(h<lT)) { R=1; }
  return R;
}

//template<typename K>
void LenoirSalles3dIM::loadElement(const Element* elt_S, Point& S1, Point& S2, Point& S3, Vector<number_t> & indi) const
{
  if(elt_S->geomElt_p->meshP()->spaceDim()==2)
    {
      S1=Point(elt_S->geomElt_p->meshElement()->vertex(indi[0])[0], elt_S->geomElt_p->meshElement()->vertex(indi[0])[1], 0.0);
      S2=Point(elt_S->geomElt_p->meshElement()->vertex(indi[1])[0], elt_S->geomElt_p->meshElement()->vertex(indi[1])[1], 0.0);
      S3=Point(elt_S->geomElt_p->meshElement()->vertex(indi[2])[0], elt_S->geomElt_p->meshElement()->vertex(indi[2])[1], 0.0);
    }
  else
    {
      S1=elt_S->geomElt_p->meshElement()->vertex(indi[0]);
      S2=elt_S->geomElt_p->meshElement()->vertex(indi[1]);
      S3=elt_S->geomElt_p->meshElement()->vertex(indi[2]);
    }

  for(int l=0; l<3; l++)
    {
      if(std::abs(S1[l])< theEpsilon) { S1[l]=0.; }
      if(std::abs(S2[l])< theEpsilon) { S2[l]=0.; }
      if(std::abs(S3[l])< theEpsilon) { S3[l]=0.; }
    }
}

// =============================================================================
//  LENOIR-SALLES method for integral representation with Laplace operator in 2D
//  restricted to single and double layer and P0 or P1
// =============================================================================
//common stuff
void LenoirSalles2dIR::geometricalStuff(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& a1, real_t& a2, real_t& d, real_t& nt) const
{
  const MeshElement* melt=elt->geomElt_p->meshElement();
  const Point& S1=melt->vertex(1);
  const Point& S2=melt->vertex(2);
  Point t=S2-S1;
  nt=norm2(t);
  t/=nt;
  Point X1=S1-p, X2=S2-p;
  a1 =dot(X1,t); a2 = dot(X2,t);  // signed distance to endpoints
  d = dot(X1,Point(*ny));         // distance of P to the segment
  //thePrintStream<<"p="<<p<<" elt "<<elt->number()<<" S1="<<S1<<" S2="<<S2<<" ny="<<*ny<<" t="<<t<<" a1="<<a1<<" a2="<<a2<<" d="<<d;
}

// =============================================================================
//                                      P0 case
// =============================================================================
// compute intg_elt G(x,y).wi(y).dy  in P0 : return a scalar
void LenoirSalles2dIR::computeLaplace2dSLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res) const
{
  res=0.;
  real_t a1,a2,d, nt;
  geometricalStuff(elt,p,ny,a1,a2,d,nt);
  real_t d2=d*d, a1d=a1*a1+d2, a2d=a2*a2+d2;
  res=a1-a2;
  if(a1d>theEpsilon) res -= 0.5*a1*std::log(a1d);
  if(a2d>theEpsilon) res += 0.5*a2*std::log(a2d);
  if(std::abs(d)>=theEpsilon)  res += d*(std::atan(a2/d)-std::atan(a1/d));
  res/=(-2*pi_);
  //thePrintStream<<" SLP0="<<res<<eol;
}

// compute intg_elt dn_y(G(x,y)).wi(y).dy  in P0 : return a scalar
void LenoirSalles2dIR::computeLaplace2dDLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res) const
{
  res=0.;
  real_t a1,a2,d, nt;
  geometricalStuff(elt,p,ny,a1,a2,d,nt);
  if(std::abs(d)<theEpsilon)
    {
      if(std::abs(a1)>theEpsilon)
        {if(a1>=0) res -= 0.25; else res += 0.25;}
      if(std::abs(a2)>theEpsilon)
        {if(a2>=0) res += 0.25; else res -= 0.25;}
    }
  else
    {
      res = std::atan(a2/d)-std::atan(a1/d);
      res*=-over2pi_;
    }
    //thePrintStream<<" DLP0="<<res<<eol;
}

// compute intg_elt dt_y(G(x,y)).wi(y).dy  in P0 : return a scalar
void LenoirSalles2dIR::computeLaplace2dDTSLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res) const
{
  res=0.;
  real_t a1,a2,d,nt;
  geometricalStuff(elt,p,ny,a1,a2,d,nt);
  real_t d2=d*d, a1d=a1*a1+d2, a2d=a2*a2+d2;
  if(a1d <theEpsilon || a2d<theEpsilon) return;  //singular return 0 !!!
  res = -over4pi_*std::log(a2d/a1d);
  //thePrintStream<<" DTSLP0="<<res<<eol;
}

// compute intg_elt dt_y(dn_y G(x,y)).wi(y).dy  in P0 : return a scalar
void LenoirSalles2dIR::computeLaplace2dDTDLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res)const
{
  res=0.;
  real_t a1,a2,d,nt;
  geometricalStuff(elt,p,ny,a1,a2,d,nt);
  real_t a12=a1*a1, a22=a2*a2, d2=d*d, a1d=a12+d2, a2d=a22+d2;
  if(a1d<theEpsilon || a2d<theEpsilon) return;  //singular return 0 !!!
  res = over2pi_*d*(a22-a12)/(a1d*a2d);
  //thePrintStream<<" DTDLP0="<<res<<eol;
}

// compute intg_elt (dn_y G(x,y)).wi(y).dy  in P0 : return a scalar, idem computeLaplace2dDLP0
void LenoirSalles2dIR::computeLaplace2dDNSLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res) const
{
  computeLaplace2dDLP0(elt,p,ny,res);
}

// compute intg_elt dn_y(dn_y G(x,y)).wi(y).dy  in P0 : return a scalar
void LenoirSalles2dIR::computeLaplace2dDNDLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res)const
{
  res=0.;
  real_t a1,a2,d, nt;
  geometricalStuff(elt,p,ny,a1,a2,d,nt);
  real_t a1d=a1*a1+d*d, a2d=a2*a2+d*d;
  if(a1d < theEpsilon) res = over2pi_/a2;         // lim a1->0 then d->0
  else
  {
      if(a2d < theEpsilon) res = -over2pi_/a1;   // lim a2->0 then d->0
      else res=over2pi_*(a2-a1)*(d*d-a1*a2)/(a1d*a2d);
  }
  //thePrintStream<<" DNDLP0="<<res<<eol;
}

// compute intg_elt grad_y(G(x,y)).wi(y).dy  in P0 : return a 2-vector
void LenoirSalles2dIR::computeLaplace2dGradSLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res) const
{
  res.resize(2);
  real_t rn=0., rt=0.;
  computeLaplace2dDNSLP0(elt,p,ny,rn);
  computeLaplace2dDTSLP0(elt,p,ny,rt);
  const MeshElement* melt=elt->geomElt_p->meshElement();
  Point t=melt->vertex(2)-melt->vertex(1);
  t/=norm2(t);
  res[0]=rn*(*ny)[0]+rt*t[0];
  res[1]=rn*(*ny)[1]+rt*t[1];
  //thePrintStream<<" GradSLP0="<<res<<eol;
}

// compute intg_elt grad_y(dn_y G(x,y)).wi(y).dy  in P0 : return a 2-vector
void LenoirSalles2dIR::computeLaplace2dGradDLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const
{
  res.resize(2);
  real_t rn=0., rt=0.;
  computeLaplace2dDNDLP0(elt,p,ny,rn);
  computeLaplace2dDTDLP0(elt,p,ny,rt);
  const MeshElement* melt=elt->geomElt_p->meshElement();
  Point t=melt->vertex(2)-melt->vertex(1);
  t/=norm2(t);
  res[0]=rn*(*ny)[0]+rt*t[0];
  res[1]=rn*(*ny)[1]+rt*t[1];
  //thePrintStream<<" GradDLP0="<<res<<eol;
}

// =============================================================================
//                                      P1 case
// =============================================================================
// compute intg_elt G(x,y).wi(y).dy  in P1 : return a 2-vector
void LenoirSalles2dIR::computeLaplace2dSLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res) const
{
  res.resize(2);
  real_t a1,a2,d, nt;
  geometricalStuff(elt,p,ny,a1,a2,d,nt);
  real_t da = std::abs(d);
  real_t I=0., J=0.;
  real_t a1d=a1*a1+d*d, a2d=a2*a2+d*d;
  real_t eps2=theEpsilon*theEpsilon;
  if(a1d>eps2)
    {
      real_t l=std::log(a1d);
      I-=a1*(0.5*l-1);
      if(da>theEpsilon) I-=d*std::atan(a1/d);
      J-=0.25*(a1d*l-a1*a1);
    }
  if(a2d>eps2)
    {
      real_t l=std::log(a2d);
      I+=a2*(0.5*l-1);
      if(da>theEpsilon) I+=d*std::atan(a2/d);
      J+=0.25*(a2d*l-a2*a2);
    }
  real_t c =-over2pi_/nt;
  res[0] = c*(a2*I-J);
  res[1] = c*(J-a1*I);
  //thePrintStream<<" SLP1="<<res<<eol;
}

// compute intg_elt dn_y(G(x,y)).wi(y).dy  in P1 : return a 2-vector
void LenoirSalles2dIR::computeLaplace2dDLP1(const Element* elt, const Point& p, const Vector<real_t>* ny,Vector<real_t>& res) const
{
  res.resize(2);
  real_t a1,a2,d,nt;
  geometricalStuff(elt,p,ny,a1,a2,d,nt);
  real_t da = std::abs(d);
  real_t I=0., J=0.;
  real_t a1d=a1*a1+d*d, a2d=a2*a2+d*d;;
  real_t eps2=theEpsilon*theEpsilon;
  if(a1d>eps2)
    {
      if(da>theEpsilon) I-=std::atan(a1/d);
      else {if(a1>=0) I+=pi_*0.5; else I-=pi_*0.5;}  //force limit d=0 from normal
      J-=std::log(a1d);
    }

  if(a2d>eps2)
    {
      if(da>theEpsilon) I+=std::atan(a2/d);
      else {if(a2>=0) I-=pi_*0.5; else I+=pi_*0.5;} //force limit d=0 from normal
      J+=std::log(a2d);
    }
  J*=0.5*d;
  real_t c = -over2pi_/nt;
  res[0] = c*(a2*I-J);
  res[1] = c*(J-a1*I);
  //thePrintStream<<" DLP1="<<res<<eol;
}

// compute intg_elt dt_y(G(x,y)).wi(y).dy  in P1 : return a 2-vector
void LenoirSalles2dIR::computeLaplace2dDTSLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const
{
  res.resize(2,0.);
  real_t a1,a2,d,nt;
  geometricalStuff(elt,p,ny,a1,a2,d,nt);
  real_t d2=d*d;
  real_t a1d=a1*a1+d2, a2d=a2*a2+d2;
  real_t eps2=theEpsilon*theEpsilon;
  if(a1d <eps2 || a2d<eps2) return;   //singular return 0

  real_t J=std::log(a2d/a1d);
  real_t c=over4pi_/(a2-a1);
  real_t I=0;
  if(std::abs(d)>theEpsilon) I=2*d*(std::atan(a2/d)-std::atan(a1/d));
  res[0]= -c*(I+a2*J)+over2pi_;
  res[1]=  c*(I+a1*J)-over2pi_;
  //thePrintStream<<" DTSLP1="<<res<<eol;
}

// compute intg_elt dt_y(dn_y G(x,y)).wi(y).dy  in P1 : return a 2-vector
void LenoirSalles2dIR::computeLaplace2dDTDLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const
{
  res.resize(2,0.);
  real_t a1,a2,d,nt;
  geometricalStuff(elt,p,ny,a1,a2,d,nt);
  real_t a1d=a1*a1+d*d, a2d=a2*a2+d*d;
  real_t eps2=theEpsilon*theEpsilon;
  if(a1d <eps2)
    {
      real_t I=std::atan(a2/d)/(a2-a1);
      res[0]=-over2pi_*I;
      res[1]=over2pi_*(I-d/a2d);
      return;
    }
  if(a2d <eps2)
    {
      real_t I=-std::atan(a1/d)/(a2-a1);
      res[0]=over2pi_*(d/a1d-I);
      res[1]=over2pi_*I;
      return;
    }
  real_t I=0.;
  if(std::abs(d)>theEpsilon) I=(std::atan(a2/d)-std::atan(a1/d))/(a2-a1);
  else //force limit d=0 from normal
  {
      if(a1>=0) I+=pi_*0.5; else I-=pi_*0.5;
      if(a2>=0) I-=pi_*0.5; else I+=pi_*0.5;
      I/=(a2-a1);
  }
  res[0]=over2pi_*(d/a1d-I);
  res[1]=over2pi_*(I-d/a2d);
  //thePrintStream<<" DTDLP1="<<res<<eol;
}

// compute intg_elt (dn_y G(x,y)).wi(y).dy  in P1 : return a 2-vector, idem computeLaplace2dDLP1
void LenoirSalles2dIR::computeLaplace2dDNSLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const
{
  computeLaplace2dDLP1(elt,p,ny,res);
}

// compute intg_elt dn_y(dn_y G(x,y)).wi(y).dy  in P1 : return a 2-vector
void LenoirSalles2dIR::computeLaplace2dDNDLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const
{
  res.resize(2,0.);
  real_t a1,a2,d,nt;
  geometricalStuff(elt,p,ny,a1,a2,d,nt);
  real_t a1d=a1*a1+d*d, a2d=a2*a2+d*d;
  real_t eps2=theEpsilon*theEpsilon;
  if(a1d <eps2)
    {
      real_t I=std::log(a2d)/(a2-a1);
      res[0]=over4pi_*I;
      res[1]=over4pi_*(2*a2/a2d-I);
      return;
    }
  if(a2d<eps2)
    {
      real_t I=-std::log(a1d)/(a2-a1);
      res[0]=over4pi_*(I-2*a1/a1d);
      res[1]=-over4pi_*I;
      return;
    }
  real_t I=std::log(a2d/a1d)/(a2-a1);
  res[0]=over4pi_*(I-2.*a1/a1d);
  res[1]=over4pi_*(2.*a2/a2d-I);
  //thePrintStream<<" DNDLP1="<<res<<eol;
}

// compute intg_elt grad_y(G(x,y)).wi(y).dy  in P1 : return a 4-vector (2x2-vector)
void LenoirSalles2dIR::computeLaplace2dGradSLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const
{
  res.resize(4);
  Vector<real_t> rn(2,0.), rt(2,0.);
  computeLaplace2dDLP1(elt,p,ny,rn);
  computeLaplace2dDTSLP1(elt,p,ny,rt);
  const MeshElement* melt=elt->geomElt_p->meshElement();
  Point t=melt->vertex(2)-melt->vertex(1);
  t/=norm2(t);
  real_t n1=(*ny)[0], n2=(*ny)[1], t1=t[0], t2=t[1];
  res[0]=rn[0]*n1+rt[0]*t1; //first shape function
  res[1]=rn[0]*n2+rt[0]*t2;
  res[2]=rn[1]*n1+rt[1]*t1; //second shape function
  res[3]=rn[1]*n2+rt[1]*t2;
}

// compute intg_elt grad_y(dn_y G(x,y)).wi(y).dy  in P1 : return a 4-vector (2x2-vector)
void LenoirSalles2dIR::computeLaplace2dGradDLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res)const
{
  res.resize(4);
  Vector<real_t> rn(2,0.), rt(2,0.);
  computeLaplace2dDNDLP1(elt,p,ny,rn);
  computeLaplace2dDTDLP1(elt,p,ny,rt);
  const MeshElement* melt=elt->geomElt_p->meshElement();
  Vector<real_t> t=melt->vertex(2)-melt->vertex(1);
  t/=norm2(t);
  real_t n1=(*ny)[0], n2=(*ny)[1], t1=t[0], t2=t[1];
  res[0]=rn[0]*n1+rt[0]*t1; //first shape function
  res[1]=rn[0]*n2+rt[0]*t2;
  res[2]=rn[1]*n1+rt[1]*t1; //second shape function
  res[3]=rn[1]*n2+rt[1]*t2;
}

// =============================================================================
//  LENOIR-SALLES method for integral representation with Laplace operator in 3D
//  restricted to single and double layer and P0 or P1
// =============================================================================

// =============================================================================
//                                      P0 case
// =============================================================================
void LenoirSalles3dIR::computeLaplace3dSLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res) const
{
  res=0.;
  real_t h=0., s=0., d=0.;
  const MeshElement* melt=elt->geomElt_p->meshElement();
  const Point& S1=melt->vertex(1);
  const Point& S2=melt->vertex(2);
  const Point& S3=melt->vertex(3);
  std::vector<Point> I(3);
  geometricalStuff(S1,S2,S3,*ny,p,I,h,false);
  std::vector< Vector<real_t> >& nu=melt->geomMapData_p->sideNV();
  real_t p0=p[0], p1=p[1], p2=p[2];
  Point& i0=I[0], &i1=I[1], &i2=I[2];
  Vector<real_t>& nu0=nu[0], &nu1=nu[1], &nu2=nu[2];
  s = -((p0-i0[0])*nu1[0]+(p1-i0[1])*nu1[1]+(p2-i0[2])*nu1[2]);
  d=std::abs(s);
  if(d > theEpsilon) res+=signe(s)*integrandLapSLP0(S2,S3,h,d,i0);
  s = -((p0-i1[0])*nu2[0]+(p1-i1[1])*nu2[1]+(p2-i1[2])*nu2[2]);
  d=std::abs(s);
  if(d > theEpsilon) res+=signe(s)*integrandLapSLP0(S3,S1,h,d,i1);
  s = -((p0-i2[0])*nu0[0]+(p1-i2[1])*nu0[1]+(p2-i2[2])*nu0[2]);
  d=std::abs(s);
  if(d > theEpsilon) res+=signe(s)*integrandLapSLP0(S1,S2,h,d,i2);
  res*=over4pi_;
}

void LenoirSalles3dIR::computeLaplace3dDLP0(const Element* elt, const Point& p, const Vector<real_t>* ny, real_t& res) const
{
  res=0.;
  real_t h=0., s=0., d=0.;
  const MeshElement* melt=elt->geomElt_p->meshElement();
  const Point S1=melt->vertex(1);
  const Point S2=melt->vertex(2);
  const Point S3=melt->vertex(3);
  std::vector<Point> I(4);
  const Vector<real_t>& nt=*ny;
  geometricalStuff(S1,S2,S3,*ny,p,I,h,true);
  Point tmp=p-I[3];
  real_t nn=norm2(tmp);
  if(nn<theEpsilon) return;
  real_t coef=(tmp[0]*nt[0]+tmp[1]*nt[1]+tmp[2]*nt[2])/nn;
  std::vector< Vector<real_t> > & nu=melt->geomMapData_p->sideNV();
  real_t p0=p[0], p1=p[1], p2=p[2];
  Point& i0=I[0], &i1=I[1], &i2=I[2];
  Vector<real_t>& nu0=nu[0], &nu1=nu[1], &nu2=nu[2];
  s = -((p0-i0[0])*nu1[0]+(p1-i0[1])*nu1[1]+(p2-i0[2])*nu1[2]);
  d=std::abs(s);
  if(d > theEpsilon) res+=signe(s)*integrandLapDLP0(S2,S3,h,d,i0);
  s = -((p0-i1[0])*nu2[0]+(p1-i1[1])*nu2[1]+(p2-i1[2])*nu2[2]);
  d=std::abs(s);
  if(d > theEpsilon) res+=signe(s)*integrandLapDLP0(S3,S1,h,d,i1);
  s = -((p0-i2[0])*nu0[0]+(p1-i2[1])*nu0[1]+(p2-i2[2])*nu0[2]);
  d=std::abs(s);
  if(d > theEpsilon) res+=signe(s)*integrandLapDLP0(S1,S2,h,d,i2);
  res*=coef*over4pi_;
}

real_t LenoirSalles3dIR::integrandLapSLP0(const Point& Sm, const Point& Sp, real_t h, real_t d, const Point& Ip, real_t alpha) const
{
  real_t tmp=0.;
  Point V=Sp-Sm;
  real_t nV=norm2(V);
  real_t sm=dot(Sm-Ip,V)/nV, sp=dot(Sp-Ip,V)/nV;
  real_t h2=h*h, d2=d*d;
  real_t sp2=sp*sp, sm2=sm*sm;
  real_t R=std::sqrt(h2+d2+sp2), Rm=std::sqrt(h2+d2), mR=std::sqrt(h2+d2+sm2);
  if(d>theEpsilon) // otherwise, return 0
    {
      tmp+=h*(std::atan(h*sp/(d*R))-std::atan(sp/d))+d*asinh(sp/Rm);
      tmp-=h*(std::atan(h*sm/(d*mR))-std::atan(sm/d))+d*asinh(sm/Rm);
    }
  // no treatment needed when h=0 or s=0.
  return tmp*alpha;
}

real_t LenoirSalles3dIR::integrandLapDLP0(const Point& Sm, const Point& Sp, real_t h, real_t d, const Point& Ip) const
{
  real_t tmp=0.;
  Point V=Sp-Sm;
  real_t nV=norm2(V);
  real_t sm=dot(Sm-Ip,V)/nV, sp=dot(Sp-Ip,V)/nV;
  real_t h2=h*h, d2=d*d;
  real_t sp2=sp*sp, sm2=sm*sm;
  real_t R=std::sqrt(h2+d2+sp2), mR=std::sqrt(h2+d2+sm2);
  // We multiply by h coming from <(x-xhat)| ny>. We normalize x-xhat and multiply here by h.
  // It simplifies the case h ~ 0
  if(d>theEpsilon)  // for d~0, res = 0.
    {
      tmp+=(std::atan(sp/d)-std::atan(h*sp/(d*R)));
      tmp-=(std::atan(sm/d)-std::atan(h*sm/(d*mR)));
    }
  return tmp;
}

// =============================================================================
//                                    P1 case
// =============================================================================
void LenoirSalles3dIR::computeLaplace3dSLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res) const
{
  real_t h=0.;
  const MeshElement* melt=elt->geomElt_p->meshElement();
  const Point & S1=melt->node(1);
  const Point & S2=melt->node(2);
  const Point & S3=melt->node(3);
  std::vector<Point> I(4);
  geometricalStuff(S1,S2,S3,*ny,p,I,h,true);
  ShapeValues shv=elt->computeShapeValues(I[3],false,false,false);
  Vector<real_t> signedD(3,0.);
  std::vector<Vector<real_t> > & nu=melt->geomMapData_p->sideNV();
  signedD[0]=-((p[0]-I[0][0])*(nu[1][0])+(p[1]-I[0][1])*(nu[1][1])+(p[2]-I[0][2])*(nu[1][2]));
  signedD[1]=-((p[0]-I[1][0])*(nu[2][0])+(p[1]-I[1][1])*(nu[2][1])+(p[2]-I[1][2])*(nu[2][2]));
  signedD[2]=-((p[0]-I[2][0])*(nu[0][0])+(p[1]-I[2][1])*(nu[0][1])+(p[2]-I[2][2])*(nu[0][2]));

  real_t common=0.;
  real_t d=std::abs(signedD[0]);
  if(d > theEpsilon) common+=signe(signedD[0])*integrandLapSLP1const(S2,S3,h,d,I[0]);
  d=std::abs(signedD[1]);
  if(d > theEpsilon) common+=signe(signedD[1])*integrandLapSLP1const(S3,S1,h,d,I[1]);
  d=std::abs(signedD[2]);
  if(d > theEpsilon) common+=signe(signedD[2])*integrandLapSLP1const(S1,S2,h,d,I[2]);

  // Compute the linear part
  Vector<real_t> tmp(2,0.); // Use to compute some temporary integrals
  std::vector<Point> V(3);
  V[0]=S3-S2;
  V[1]=S1-S3;
  V[2]=S2-S1;
  Vector<real_t> normA(3);
  normA[0]=norm2(V[0]);
  normA[1]=norm2(V[1]);
  normA[2]=norm2(V[2]);
  // Computation with basis function that does not vanish on [S2,S3]
  d=std::abs(signedD[0]);
  integrandLapSLP1lin(S2,S3,h,d,I[0],tmp);
  real_t sm=dot(S2-I[0],V[0])/(normA[0]); // abscissa of S-
  real_t sp=dot(S3-I[0],V[0])/(normA[0]); // abscissa of S-
  real_t eta=signe(sp-sm);
  res[1]+=signe(signedD[0])*((1.+eta*sm/normA[0])*tmp[0] - eta/normA[0]*tmp[1]);
  res[2]+=signe(signedD[0])*eta/normA[0]*(tmp[1] -sm*tmp[0]);
  // Computation with basis function that does not vanish on [S3,S1]
  tmp=0.;
  d=std::abs(signedD[1]);
  integrandLapSLP1lin(S3,S1,h,d,I[1],tmp);
  sm=dot(S3-I[1],V[1])/(normA[1]); // abscissa of S-
  sp=dot(S1-I[1],V[1])/(normA[1]); // abscissa of S+
  eta=signe(sp-sm);
  res[2]+=signe(signedD[1])*((1.+eta*sm/normA[1])*tmp[0] - eta/normA[1]*tmp[1]);
  res[0]+=signe(signedD[1])*eta/normA[1]*(tmp[1] -eta*sm*tmp[0]);
  // Computation with basis function that does not vanish on [S1,S2]
  tmp=0.;
  d=std::abs(signedD[2]);
  integrandLapSLP1lin(S1,S2,h,d,I[2],tmp);
  sm=dot(S1-I[2],V[2])/(normA[2]); // abscissa of S-
  sp=dot(S2-I[2],V[2])/(normA[2]); // abscissa of S-
  eta=signe(sp-sm);
  res[0]+=signe(signedD[2])*((1.+eta*sm/normA[2])*tmp[0] - eta/normA[2]*tmp[1]);
  res[1]+=signe(signedD[2])*eta/normA[2]*(tmp[1] -sm*tmp[0]);

  for(size_t j=0; j<3; j++)
    {
      res[j]+=shv.w[j]*common;
      res[j]*=over4pi_;
    }
}

void LenoirSalles3dIR::computeLaplace3dDLP1(const Element* elt, const Point& p, const Vector<real_t>* ny, Vector<real_t>& res) const
{
  real_t h=0.;
  const MeshElement* melt=elt->geomElt_p->meshElement();
  const Point & S1=melt->node(1);
  const Point & S2=melt->node(2);
  const Point & S3=melt->node(3);
  std::vector<Point> I(4);
  geometricalStuff(S1,S2,S3,*ny,p,I,h,true);
  Point ptmp=p-I[3];
  real_t nn=norm2(ptmp);
  if(nn<theEpsilon) return;
  real_t coef=(ptmp[0]*(*ny)[0]+ptmp[1]*(*ny)[1]+ptmp[2]*(*ny)[2])/nn;
  ShapeValues shv=elt->computeShapeValues(I[3],false,false,false);
  Vector<real_t> signedD(3,0.);
  std::vector<Vector<real_t> > & nu=melt->geomMapData_p->sideNV();
  signedD[0]=-((p[0]-I[0][0])*(nu[1][0])+(p[1]-I[0][1])*(nu[1][1])+(p[2]-I[0][2])*(nu[1][2]));
  signedD[1]=-((p[0]-I[1][0])*(nu[2][0])+(p[1]-I[1][1])*(nu[2][1])+(p[2]-I[1][2])*(nu[2][2]));
  signedD[2]=-((p[0]-I[2][0])*(nu[0][0])+(p[1]-I[2][1])*(nu[0][1])+(p[2]-I[2][2])*(nu[0][2]));

  real_t common=0.;
  real_t d=std::abs(signedD[0]);
  if(d > theEpsilon) common+=signe(signedD[0])*integrandLapDLP1const(S2,S3,h,d,I[0]);
  d=std::abs(signedD[1]);
  if(d > theEpsilon) common+=signe(signedD[1])*integrandLapDLP1const(S3,S1,h,d,I[1]);
  d=std::abs(signedD[2]);
  if(d > theEpsilon) common+=signe(signedD[2])*integrandLapDLP1const(S1,S2,h,d,I[2]);

  // Compute the linear part
  Vector<real_t> tmp(2,0.); // Use to compute some temporary integrals
  std::vector<Point> V(3);
  V[0]=S3-S2;
  V[1]=S1-S3;
  V[2]=S2-S1;
  Vector<real_t> normA(3);
  normA[0]=norm2(V[0]);
  normA[1]=norm2(V[1]);
  normA[2]=norm2(V[2]);
  // Computation with basis function that does not vanish on [S2,S3]
  d=std::abs(signedD[0]);
  integrandLapDLP1lin(S2,S3,h,d,I[0],tmp);
  real_t sm=dot(S2-I[0],V[0])/(normA[0]); // abscissa of S-
  real_t sp=dot(S3-I[0],V[0])/(normA[0]); // abscissa of S-
  real_t eta=signe(sp-sm);
  res[1]+=signe(signedD[0])*((1.+eta*sm/normA[0])*tmp[0] - eta/normA[0]*tmp[1]);
  res[2]+=signe(signedD[0])*eta/normA[0]*(tmp[1] -sm*tmp[0]);
  // Computation with basis function that does not vanish on [S3,S1]
  tmp*=0.;
  d=std::abs(signedD[1]);
  integrandLapDLP1lin(S3,S1,h,d,I[1],tmp);
  sm=dot(S3-I[1],V[1])/(normA[1]); // abscissa of S-
  sp=dot(S1-I[1],V[1])/(normA[1]); // abscissa of S+
  eta=signe(sp-sm);
  res[2]+=signe(signedD[1])*((1.+eta*sm/normA[1])*tmp[0] - eta/normA[1]*tmp[1]);
  res[0]+=signe(signedD[1])*eta/normA[1]*(tmp[1] -eta*sm*tmp[0]);
  // Computation with basis function that does not vanish on [S1,S2]
  tmp=0.;
  d=std::abs(signedD[2]);
  integrandLapDLP1lin(S1,S2,h,d,I[2],tmp);
  sm=dot(S1-I[2],V[2])/(normA[2]); // abscissa of S-
  sp=dot(S2-I[2],V[2])/(normA[2]); // abscissa of S-
  eta=signe(sp-sm);
  res[0]+=signe(signedD[2])*((1.+eta*sm/normA[2])*tmp[0] - eta/normA[2]*tmp[1]);
  res[1]+=signe(signedD[2])*eta/normA[2]*(tmp[1] -sm*tmp[0]);
  for(size_t j=0; j<3; j++)
    {
      res[j]+=shv.w[j]*common;
      res[j]*=(coef*over4pi_);
    }
}

real_t LenoirSalles3dIR::integrandLapSLP1const(const Point& Sm, const Point& Sp, real_t h,real_t d,
    const Point& Ip, real_t alpha) const
{
  real_t tmp=0.;
  Point V=Sp-Sm;
  real_t nV=norm2(V);
  real_t sm=dot(Sm-Ip,V)/nV, sp=dot(Sp-Ip,V)/nV;
  real_t h2=h*h, d2=d*d;
  real_t sp2=sp*sp;
  real_t R=std::sqrt(h2+d2+sp2), Rm=std::sqrt(h2+d2);
  if(d>theEpsilon) // when d~0, res = 0.
    {
      if(h>theEpsilon)
        {
          real_t ds=std::sqrt(d2+sp2);
          tmp+=0.5/d*((d2-h2)*asinh(sp/Rm)+sp*h2/ds*asinh(ds/h));
          tmp+=h*(std::atan(h*sp/(d*R)) - std::atan(sp/d));
          real_t sm2=sm*sm;
          ds=std::sqrt(d2+sm2);
          R=std::sqrt(h2+d2+sm2);
          tmp-=0.5/d*((d2-h2)*asinh(sm/Rm)+sm*h2/ds*asinh(ds/h));
          tmp-=h*(std::atan(h*sm/(d*R)) - std::atan(sm/d));
        }
      else tmp+=0.5*d*(asinh(sp/d)-asinh(sm/d)); // case h~0
    }
  return alpha*tmp;
}

void LenoirSalles3dIR::integrandLapSLP1lin(const Point & Sm, const Point & Sp, real_t h, real_t d, const Point& Ip,
    Vector<real_t> & res, real_t alpha) const
{
  Point V=Sp-Sm;
  real_t nV=norm2(V);
  real_t sm=dot(Sm-Ip,V)/nV, sp=dot(Sp-Ip,V)/nV;
  real_t h2=h*h, d2=d*d;
  real_t sp2=sp*sp;
  real_t R=std::sqrt(h2+d2+sp2), Rm=std::sqrt(h2+d2);
  res[0]=0.;
  res[1]=0.;
  if(d>theEpsilon) // d ~ 0 -> res = 0.
    {
      if(h>theEpsilon)
        {
          real_t ds=std::sqrt(d2+sp2);
          real_t tt=h2/ds*asinh(ds/h);
          res[0]+=0.5/d*((d2+h2)*asinh(sp/Rm) - sp*tt);
          res[1]+=0.5*d*(R+tt);
          real_t sm2=sm*sm;
          ds=std::sqrt(d2+sm2);
          tt=h2/ds*asinh(ds/h);
          R=std::sqrt(h2+d2+sm2);
          res[0]-=0.5/d*((d2+h2)*asinh(sm/Rm) - sm*tt);
          res[1]-=0.5*d*(R+tt);
        }
      else
        {
          res[0]+=0.5*d*asinh(sp/d);
          res[1]+=0.5*d*R;
          real_t sm2=sm*sm;
          R=std::sqrt(h2+d2+sm2);
          res[0]-=0.5*d*asinh(sm/d);
          res[1]-=0.5*d*R;
        }
    }
  res*=alpha;
}

real_t LenoirSalles3dIR::integrandLapDLP1const(const Point& Sm, const Point& Sp, real_t h,real_t d, const Point& Ip) const
{
  real_t tmp=0.;
  Point V=Sp-Sm;
  real_t nV=norm2(V);
  real_t sm=dot(Sm-Ip,V)/nV, sp=dot(Sp-Ip,V)/nV;
  real_t h2=h*h, d2=d*d;
  real_t sp2=sp*sp;
  real_t R=std::sqrt(h2+d2+sp2), Rm=std::sqrt(h2+d2);
  // We multiply by h coming from <(x-xhat)| ny>. We normalize x-xhat and multiply here by h.
  if(d>theEpsilon)
    {
      if(h>theEpsilon)
        {
          real_t ds=std::sqrt(d2+sp2);
          tmp+=(std::atan(sp/d)-std::atan(sp*h/(d*R)));
          tmp+=h/d*(asinh(sp/Rm)-sp/ds*asinh(ds/h));
          real_t sm2=sm*sm;
          ds=std::sqrt(d2+sm2);
          R=std::sqrt(h2+d2+sm2);
          tmp-=(std::atan(sm/d)-std::atan(sm*h/(d*R)));
          tmp-=h/d*(asinh(sm/Rm)-sm/ds*asinh(ds/h));
        }
      else tmp+=std::atan(sp/d)-std::atan(sm/d); // case h ~ 0
    }
  return tmp;
}

void LenoirSalles3dIR::integrandLapDLP1lin(const Point & Sm, const Point & Sp, real_t h, real_t d,
    const Point& Ip, Vector<real_t> & res) const
{
  Point V=Sp-Sm;
  real_t nV=norm2(V);
  real_t sm=dot(Sm-Ip,V)/nV, sp=dot(Sp-Ip,V)/nV;
  real_t h2=h*h, d2=d*d;
  real_t sp2=sp*sp;
  real_t Rm=std::sqrt(h2+d2);
  res[0]=0.;
  res[1]=0.;
  // We multiply by h coming from <(x-xhat)| ny>. We normalize x-xhat and multiply here by h.
  // Same thing with d coming from the signed distance.
  if(d>theEpsilon)
    {
      if(h>theEpsilon)
        {
          real_t hd=h*d;
          real_t ds=std::sqrt(d2+sp2);
          res[0]+=h/d*(-asinh(sp/Rm) + sp/ds*asinh(ds/h));
          res[1]+=-hd/ds*asinh(ds/h);
          real_t sm2=sm*sm;
          ds=std::sqrt(d2+sm2);
          res[0]-=h/d*(-asinh(sm/Rm) + sm/ds*asinh(ds/h));
          res[1]-=-hd/ds*asinh(ds/h);
        }
    } // When d ~ 0 or h ~ 0, res[0,1] = 0.
}


// =============================================================================
//                                additionnal stuff
// =============================================================================
// Input:
// - (S1,S2,S3): the vertices of a triangle
// - normaT: the normal of the plane of the triangle
// - X: the observation point
// - P1: boolean to compute or not I[3]
// Output:
// - I: Vector of points that are the projection of X onto the support of the segments of the triangle and I[3]=projection of X onto the plane of the triangle
// - h: distance from point X to the plane of the triangle
void LenoirSalles3dIR::geometricalStuff(const Point& S1, const Point& S2, const Point& S3, const Vector<real_t>& N,const Point& X,
                                        std::vector<Point>& I, real_t & h, bool I3) const
{
  Point Sij=S3-S2;
  Sij/=norm2(Sij);
  I[0]=S2+dot(X-S2,Sij)*Sij;
  Sij=S1-S3;
  Sij/=norm2(Sij);
  I[1]=S3+dot(X-S3,Sij)*Sij;
  Sij=S2-S1;
  Sij/=norm2(Sij);
  I[2]=S1+dot(X-S1,Sij)*Sij;
  Point tmp=S1-X;
  h=tmp[0]*N[0]+tmp[1]*N[1]+tmp[2]*N[2];
  if(I3)
    {
      I[3].resize(3);
      std::vector<real_t>::iterator itI=I[3].begin();
      std::vector<real_t>::const_iterator itX=X.begin(), itN=N.begin();
      *itI++=*itX++ +h**itN++;
      *itI++=*itX++ +h**itN++;
      *itI=*itX+h**itN;
    }
  h=std::abs(h);
}

} // end of namespace xlifepp

