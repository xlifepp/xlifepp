/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file termUtils.cpp
  \author N. Kielbasiewicz
  \since 27 feb 2013
  \date 1 mar 2013

  \brief Implementation of term utility functions
*/

#include "termUtils.hpp"
#include "../TermMatrix.hpp"
#include "../KernelOperatorOnTermVector.hpp"
#include "../../form/BilinearForm.hpp"
#include "SauterSchwabIM.hpp"
#include "SauterSchwabSymIM.hpp"
#include "DuffyIM.hpp"
#include "CollinoIM.hpp"
#include <vector>
#include <sstream>

namespace xlifepp
{

//! special cast function used by FEComputation's
real_t toRealComplex(const Vector<real_t>& v,const complex_t& c) {return c.real();}
complex_t toRealComplex(const Vector<complex_t>&,const complex_t& c) {return c;}
real_t toRealComplex(const Vector<Vector<real_t> >& v,const complex_t& c) {return c.real();}
complex_t toRealComplex(const Vector<Vector<complex_t> >& v,const complex_t& c) {return c;}

// build a storage from a pair of Space
// for FE space it is assumed that only connected dofs in contiguous elements give non zero values
// FE spaces must live on same domain
MatrixStorage* buildStorage(const Space& rs, const Space& cs, StorageType st, AccessType at, StorageBuildType bt)
{
  trace_p->push("buildStorage");

  // the storage id is built from the adresses of Space objects
  std::stringstream ss;
  ss << &rs << "-" << &cs;
  MatrixStorage* res = findMatrixStorage(ss.str(), st, at, bt);

  if (res != nullptr)
  {
    // the storage already exists, then we return a pointer to
    trace_p->pop();
    return res;
  }

  // detect special case: extended domain
  const MeshDomain* cdom = cs.domain()->meshDomain(), *rdom = rs.domain()->meshDomain();
  const GeomDomain* gamma_u = cdom->extensionof_p, *gamma_v = rdom->extensionof_p;
  std::vector<number_t>::const_iterator cit_r, cit_c;
  bool extension = (gamma_u!=nullptr || gamma_v!=nullptr);
  MatrixPart mp=_lower;                         // used only for symmetric matrix

  //build col index
  number_t dimr=rs.nbDofs(), dimc=cs.nbDofs();
  std::vector<std::set<number_t> > colset;      // temporary vector of col index
  if(st!=_dense)
    {
      colset.resize(dimr);
      if(rs.typeOfSubSpace() == _spSpace || cs.typeOfSubSpace() == _spSpace || (cs.domain()!=rs.domain() && !extension))
        {
          std::set<number_t> colInd;
          std::set<number_t>::iterator jt=colInd.begin();
          for(number_t j = 0; j < dimc; j++) jt=colInd.insert(jt, j + 1);
          std::vector<std::set<number_t> >::iterator it=colset.begin();
          for(number_t i = 0; i < dimr; i++, it++) { *it = colInd; }
        }
      else if(extension) // case of extension of row domain
        {
          mp=_all;
          const MeshDomain* mgamma=nullptr;
          if(gamma_u!=nullptr) mgamma = gamma_u->meshDomain();
          else mgamma=gamma_v->meshDomain();

          std::vector<GeomElement*>::const_iterator itg=mgamma->geomElements.begin(), itge=mgamma->geomElements.end();
          std::list<GeomElement*>::iterator itlu, itlv;
          for(; itg!=itge; ++itg)
            {
              std::list<GeomElement*> gelts_u, gelts_v;
              if(gamma_u==nullptr) gelts_u.push_back(*itg); // no extension for u, use side element
              else
                {
                  std::list<GeoNumPair>& sideToExt = cdom->sideToExt[*itg];
                  std::list<GeoNumPair>::iterator itl=sideToExt.begin();
                  for(; itl!=sideToExt.end(); ++itl) gelts_u.push_back(itl->first);
                }
              if(gamma_v==nullptr) gelts_v.push_back(*itg); // no extension for u, use side element
              else
                {
                  std::list<GeoNumPair>& sideToExt = rdom->sideToExt[*itg];
                  std::list<GeoNumPair>::iterator itl=sideToExt.begin();
                  for(; itl!=sideToExt.end(); ++itl) gelts_v.push_back(itl->first);
                }
              for(itlu=gelts_u.begin(); itlu!=gelts_u.end(); ++itlu)
                {
                  std::vector<number_t> ced = cs.elementDofs(cs.numElement(*itlu));
                  for(itlv=gelts_v.begin(); itlv!=gelts_v.end(); ++itlv)
                    {
                      std::vector<number_t> red = rs.elementDofs(rs.numElement(*itlv));
                      for(cit_r = red.begin(); cit_r != red.end(); cit_r++)
                        for(cit_c = ced.begin(); cit_c != ced.end(); cit_c++)
                          colset[*cit_r - 1].insert(*cit_c);
                    }
                }
            }
        }
      else     // both spaces are standard finite element spaces
        {
          number_t n = rs.nbOfElements();
          // build list of col index for each rows
          if(at!=_sym)
            {
              for(number_t k = 0; k < n; k++)
                {
                  // dof ranks for element k
                  std::vector<number_t> red = rs.elementDofs(k);               // row dof ranks for element k
                  GeomElement* gelt = rs.element_p(k)->geomElt_p;              // geometric element supporting element
                  number_t kc=k;
                  if(cs.element_p(k)->geomElt_p!=gelt) kc=cs.numElement(gelt); //not the same support, find the right one
                  std::vector<number_t> ced = cs.elementDofs(kc);              // col dof ranks for element k
                  for(cit_r = red.begin(); cit_r != red.end(); cit_r++)
                    for(cit_c = ced.begin(); cit_c != ced.end(); cit_c++)
                      colset[*cit_r - 1].insert(*cit_c);
                }
            }
          else //symmetric storage: keep only  strict lower part indices
            {
              for(number_t k = 0; k < n; k++)
                {
                  std::vector<number_t> red = rs.elementDofs(k);               // row dof ranks for element k
                  GeomElement* gelt = rs.element_p(k)->geomElt_p;              // geometric element supporting element
                  number_t kc=k;
                  if(cs.element_p(k)->geomElt_p!=gelt) kc=cs.numElement(gelt); //not the same support, find the right one
                  std::vector<number_t> ced = cs.elementDofs(kc);              // col dof ranks for element k
                  for(cit_r = red.begin(); cit_r != red.end(); cit_r++)
                    for(cit_c = ced.begin(); cit_c != ced.end(); cit_c++)
                      if(*cit_r > *cit_c) colset[*cit_r - 1].insert(*cit_c);
                }
            }
        }
    }
  // construct the storage with the column indices by rows
  switch(st)
    {
      case _cs:
        switch(at)
          {
            case _sym: res = new SymCsStorage(dimr, colset, mp, ss.str()); break;
            case _row: res = new RowCsStorage(dimr, dimc, colset, ss.str()); break;
            case _col: res = new ColCsStorage(dimr, dimc, colset, ss.str()); break;
            case _dual: res = new DualCsStorage(dimr, dimc, colset, ss.str()); break;
            default: error("storage_bad_access", words("access type",at), words("storage type",_cs));
          }
        break;
      case _skyline:
        switch(at)
          {
            case _sym:  res = new SymSkylineStorage(dimr, colset, ss.str()); break;
            case _dual: res = new DualSkylineStorage(dimr, dimc, colset, ss.str()); break;
            default: error("storage_bad_access", words("access type",at), words("storage type",_skyline));
          }
        break;
      case _dense:
        switch(at)
          {
            case _sym: res = new SymDenseStorage(dimr, ss.str()); break;
            case _row: res = new RowDenseStorage(dimr, dimc, ss.str()); break;
            case _col: res = new ColDenseStorage(dimr, dimc, ss.str()); break;
            case _dual: res = new DualDenseStorage(dimr, dimc, ss.str()); break;
            default: error("storage_bad_access", words("access type",at), words("storage type",_dense));
          }
        break;
      default: error("storage_not_implemented","buildStorage",words("storage type",st));
    }
  res->buildType()=bt;
  trace_p->pop();
  return res;
}

/*! modify MatrixStorage stm to include dense submatrix given by its row and column indices (rows and cols)
    the new storage is of type (st,at)
    rows (resp. cols) has to be a subset of row indices (resp. column indices) of the stm storage
    if overwrite=true the current storage is overwritten and returned
    else a new MatrixStorage is returned, the stm storage being not cleared!
*/

MatrixStorage* updateStorage(MatrixStorage& stm, const std::vector<number_t> rows, const std::vector<number_t> cols,
                             StorageType st, AccessType at, bool overwrite)
{
  trace_p->push("updateStorage(MatrixStorage, rows, cols,...)");
  MatrixStorage* newstm=&stm;
  if(!overwrite) newstm= stm.clone();
  if(stm.storageType()==st && stm.accessType()==at)  //target storage is same as stm storage
    {
      switch(st)
        {
          case _cs:
          case _skyline: newstm->addSubMatrixIndices(rows,cols);
          default:  //dense in dense, nothing to do
            trace_p->pop();
            return newstm;
        }
    }

  error("storage_not_handled", words("storage type",stm.storageType()),words("access type",stm.accessType()));
  trace_p->pop();
  return newstm;
}

// build a storage from  type and dimension, no allocation of pointers (void matrix)
MatrixStorage* buildStorage(StorageType st, AccessType at, StorageBuildType bt, number_t dimr, number_t dimc, const string_t& id)
{
  MatrixStorage* ms=nullptr;
  trace_p->push("buildStorage");
  switch(st)
    {
      case _cs:
        switch(at)
          {
            case _sym: ms = new SymCsStorage(dimr, id); break;
            case _row: ms = new RowCsStorage(dimr, dimc, id); break;
            case _col: ms = new ColCsStorage(dimr, dimc, id); break;
            case _dual: ms = new DualCsStorage(dimr, dimc, id); break;
            default: error("storage_bad_access", words("access type",at), words("storage type",_cs));
          }
        break;
      case _skyline:
        switch(at)
          {
            case _sym: ms = new SymSkylineStorage(dimr, id); break;
            case _dual: ms = new DualSkylineStorage(dimr, dimc, id);break;
            default: error("storage_bad_access", words("access type",at), words("storage type",_skyline));
          }
        break;
      case _dense:
        switch(at)
          {
            case _sym: ms = new SymDenseStorage(dimr,id); break;
            case _row: ms = new RowDenseStorage(dimr, dimc, id);break;
            case _col: ms = new ColDenseStorage(dimr, dimc, id);break;
            case _dual: ms = new DualDenseStorage(dimr, dimc, id);break;
            default : error("storage_bad_access", words("access type",at), words("storage type",_dense));
          }
        break;
      default: error("storage_not_implemented","buildStorage",words("storage type",st));
    }
  ms->buildType() = bt;
  trace_p->pop();
  return ms;
}

// build a storage from  type, dimension and column indices (vector of vectors)
MatrixStorage* buildStorage(StorageType st, AccessType at, StorageBuildType bt, number_t dimr, number_t dimc,
                            const std::vector<std::vector<number_t> >& indices, const string_t& id)
{
  trace_p->push("buildStorage");
  MatrixStorage* ms=nullptr;
  switch(st)
    {
      case _cs:
        switch(at)
          {
            case _sym: ms = new SymCsStorage(dimr, indices, _all, id); break;
            case _row: ms = new RowCsStorage(dimr, dimc, indices, id); break;
            case _col:
            {
              std::vector<std::vector<number_t> > rowindices(dimc);
              std::vector<std::vector<number_t> >::const_iterator itr=indices.begin();
              std::vector<number_t>::const_iterator itc;
              number_t r=1;
              for(; itr!=indices.end(); itr++, r++)
                  for(itc=itr->begin(); itc!= itr->end(); itc++) rowindices[*itc - 1].push_back(r);
              ms = new ColCsStorage(dimr, dimc, rowindices, id); break;
            }
            case _dual: ms = new DualCsStorage(dimr, dimc, indices, id); break;
            default: error("storage_bad_access", words("access type",at), words("storage type",_cs));
          }
        break;
      case _skyline:
        switch(at)
          {
            case _sym: ms = new SymSkylineStorage(dimr, indices, id); break;
            case _dual: ms = new DualSkylineStorage(dimr, dimc, indices, id); break;
            default: error("storage_bad_access", words("access type",at), words("storage type",_skyline));
          }
        break;
      case _dense:
        switch(at)
          {
            case _sym: ms = new SymDenseStorage(dimr, id); break;
            case _row: ms = new RowDenseStorage(dimr, dimc, id); break;
            case _col: ms = new ColDenseStorage(dimr, dimc, id); break;
            case _dual: ms = new DualDenseStorage(dimr, dimc, id); break;
            default : error("storage_bad_access", words("access type",at), words("storage type",_dense));
          }
        break;
      default: error("storage_not_implemented","buildStorage",words("storage type",st));
    }
  ms->buildType()=bt;
  trace_p->pop();
  return ms;
}

// build a storage from  type, dimension and column indices (vector of sets)
MatrixStorage* buildStorage(StorageType st, AccessType at, StorageBuildType bt, number_t dimr, number_t dimc,
                            const std::vector<std::set<number_t> >& colIndices, const string_t& id)
{
  // convert set to vector
  std::vector<std::vector<number_t> > indices(colIndices.size());
  std::vector<std::set<number_t> >::const_iterator itc;
  std::vector<std::vector<number_t> >::iterator iti = indices.begin();
  for(itc = colIndices.begin(); itc != colIndices.end(); ++itc, ++iti)
    *iti = std::vector<number_t>(itc->begin(), itc->end());
  return buildStorage(st,at,bt,dimr,dimc,indices,id);
}

/*! compute interpolated normals on Lagrange Dofs of space sp
    solve the projection problem Mk*nk = M0*n0 where
       Mk is the mass matrix associated to sp interpolation
       M0 is the "hybrid" mass matrix between sp interpolation and P0 interpolation
    the normals are returned as a vector of Vector

    NOTE: this method is well adapted for planar element (same normal every where in element)
           in case of curved geometric element, the method may be improved by using Gauss Lobatto interpolation of higher degree
*/
void interpolatedNormals(Space& sp, std::vector<Vector<real_t> >& ns)
{
  if (!sp.isFE())
  {
    where("interpolatedNormals(Space, vector<Vector<Real> >");
    error("not_fe_space_type", sp.name());
  }
  const Interpolation* intp=sp.rootSpace()->interpolation();
  if (intp->type!=_Lagrange) error("lagrange_fe_space_only",sp.name());

  trace_p->push("interpolatedNormals");

  if (intp->numtype > 0)  //non P0 case
  {
    const GeomDomain& dom=*sp.domain();
    dimen_t d=dom.mesh()->spaceDim();
    Unknown n(*sp.rootSpace(), _name="n", _dim=d);
    TestFunction t(n, _name="t");

    //define matrix
    TermMatrix Mk(intg(dom,n|t), _name="normalPkM");
    Space V0(_domain=dom, _interpolation=P0, _name="V0", _notOptimizeNumbering);
    Unknown n0(V0, _name="n0", _dim=d);
    TermMatrix M0(intg(dom,n0|t), _name="normalPkP0M");

    //fill normal P0 in a TermVector
    TermVector N0(n0, dom, Vector<real_t>(d,0.), _name="normalsP0");
    Vector<Vector<real_t> >::iterator it=N0.begin()->second->entries()->beginrv();
    for (number_t k=0; k<sp.nbOfElements(); k++, it++)
    {
      const Element* elt=sp.element_p(k);
      MeshElement* melt = elt->geomElt_p->meshElement();
      if (melt==nullptr) melt=elt->geomElt_p->buildSideMeshElement();
      GeomMapData gmap(melt);
      gmap.computeJacobianMatrix(elt->refElt_p->geomRefElem_p->centroid());
      gmap.computeOrientedNormal();
      *it = gmap.normalVector;
    }

    //solve projection problem
    TermVector Nk=directSolve(Mk,M0*N0);

    //return normals
    ns= *Nk.begin()->second->entries()->rvEntries_p;
    std::vector<Vector<real_t> >::iterator itn=ns.begin();
    for(; itn!=ns.end(); itn++)  *itn/=itn->norm2();
  }

  else //P0 interpolation
  {
    verboseLevel(10);
    ns.resize(sp.nbOfElements());
    std::vector<Vector<real_t> >::iterator it=ns.begin();
    for (number_t k=0; k<sp.nbOfElements(); k++, it++)
    {
      const Element* elt=sp.element_p(k);
      MeshElement* melt = elt->geomElt_p->meshElement();
      if(melt==nullptr) melt=elt->geomElt_p->buildSideMeshElement();
      GeomMapData gmap(melt);
      gmap.computeJacobianMatrix(elt->refElt_p->geomRefElem_p->centroid());
      gmap.computeOrientedNormal();
      *it = gmap.normalVector;
    }
  }
  trace_p->pop();
  return;
}

//compute normals of a side domain using interpolation given by an unknown
TermVector normalsOn(GeomDomain& dom, const Unknown& u)
{
  if(!dom.isSideDomain() && ((dom.dim()+1)!=dom.spaceDim()))
    {
      where("normalsOn(Domain, Unknown)");
      error("domain_not_side", dom.name());
    }
  const Space* spu = u.space();
  const Interpolation* itp0 = findInterpolation(Lagrange, _standard, 0, L2);
  const Space* sp0=nullptr; bool sub = false;
  if(spu->interpolation()==itp0)
    {
      if(spu->domain() == &dom) sp0 = spu;   // u space is a P0 space
      else if(spu->domain()->include(dom)) {sp0 = spu; sub=true;}
    }
  if(sp0==nullptr)  // find or create space P0
    {
      std::vector<Space*>::iterator it;
      for(it = Space::theSpaces.begin(); it != Space::theSpaces.end() && (sp0==nullptr || sub==false); ++it)
        {
          const Space* spit = (*it)->space();
          if(spit != nullptr)
            {
              const Interpolation* itp = spit->interpolation();
              if(itp == itp0)
                {
                  if(spit->domain()==&dom) sp0=spit;
                  else if(spit->domain()->include(dom)) {sp0=spit; sub=true;}
                }
            }
        }
    }
  if(sp0==nullptr) sp0 = new Space(_domain=dom, _interpolation=_P0, _name="P0");            // no P0 space found
  else if(sub) sp0 = Space::findSubSpace(&dom, sp0);     // P0 parent space found
  dimen_t d = dom.spaceDim();
  Unknown n0(*const_cast<Space*>(sp0), "n_"+dom.name(),d);

  // fill normal P0 in a TermVector
  TermVector N0(n0, dom, Vector<real_t>(d,0.),"normalsP0");
  Vector<Vector<real_t> >::iterator it=N0.begin()->second->entries()->beginrv();
  for(number_t k=0; k<sp0->nbOfElements(); k++, it++)
    {
      if(!dom.meshDomain()->normalComputed)
        {
          const Element* elt=sp0->element_p(k);
          MeshElement* melt = elt->geomElt_p->meshElement();
          if(melt==nullptr) melt=elt->geomElt_p->buildSideMeshElement();
          GeomMapData gmap(melt);
          gmap.computeJacobianMatrix(elt->refElt_p->geomRefElem_p->centroid());
          gmap.computeOrientedNormal();
          *it = gmap.normalVector;
        }
      else *it = sp0->element_p(k)->geomElt_p->meshElement()->geomMapData_p->normalVector;

    }

  if(spu->interpolation()==itp0) return N0;

  //project normal on space spu
  Unknown* uc = const_cast<Unknown*>(&u);
  //if(spu->dimFun()==1 && u.nbOfComponents()==1)  uc=new Unknown(*const_cast<Space*>(spu), _name="uc_"+tostring(spu),d); // permissive, user pass a scalar unknown
  if(uc->nbOfComponents()!=d && spu->dimFun()!=d) error("unknown_inconsistent_size", uc->name(), d, uc->nbOfComponents());
  TestFunction t(*uc, _name="t_u");
  TermMatrix Mk(intg(dom,(*uc)|t), _name="u_t_mass");
  TermMatrix M0(intg(dom,n0|t), _name="n0_t_mass");
  TermVector Nk=directSolve(Mk,M0*N0);
  return Nk;
}

/*! precompute geometry data and shape values on any element for any quadrature involved
    restricted to one order geometric element, take large place but computation is then faster in case of IE!!!

    dom: integration domain
    subsp: subspace providing the list of elements
    quads: list of quadratures
    nbc: number of components of the unknown
    useAux: use relt->qshvs_aux to store shapevalues (occurs when relt->qshvs_ is already used)
    der1: compute shape first derivatives if true
    der2: compute shape second derivatives if true
    nor: compute normal if true
    extendedDomain: true when dealing with an extended domain
    isogeo : if true, use domain parametrization in geometric computation
*/
void preComputationIE(const GeomDomain& dom, const Space* subsp, std::set<Quadrature*>& quads, dimen_t nbc, bool useAux, bool der1, bool der2, bool nor,
                      bool extendedDomain, bool isogeo)
{
  der1=true;  //force computation of derivatives to avoid misfit when recall
  bool twinAnalyze=!extendedDomain && dom.isInterface();
  std::map<Quadrature*,std::vector<ShapeValues> >* qshvs_p=nullptr;     //pointer to shapevalues map
  std::map<Quadrature*,std::vector<ShapeValues> >::iterator itmq;       //iterator to shapevalues map item

  bool buildP1=false;  //P1 ref element building flag

  std::set<GeomElement*> gelts;  //to store geomelement pointers related to elements, if required
  //shapevalues for all single quadrature (non parallel)
  for(number_t k = 0; k < subsp->nbOfElements(); k++) // data related to element
    {
      const Element* elt = subsp->element_p(k);
      RefElement* relt = elt->refElt_p;                      //reference element related to element
      GeomElement* gelt = elt->geomElt_p;
      MeshElement* melt = gelt->meshElement();
      if(!buildP1 && (!melt->linearMap || isogeo))           //build once P1 ref element if required
      {
         findRefElement(melt->shapeType(),findInterpolation(_Lagrange, _standard, 1,_H1));
         buildP1=true;
      }
      if(twinAnalyze) gelts.insert(gelt);                    //useful only for an interface and not an extended domain
      const GeomRefElement* grelt = gelt->geomRefElement();
      number_t sw = relt->nbDofs()*nbc*nbc;
      if(extendedDomain) //build geometric data for extended element
        {
          if(gelt->meshElement()==nullptr) gelt->buildSideMeshElement();
          MeshElement* melt = gelt->meshElement();
          GeomMapData* mapdata = melt->geomMapData_p;
          if(mapdata==nullptr) mapdata=new GeomMapData(melt);
          mapdata->computeJacobianMatrix(std::vector<real_t>(gelt->elementDim(),0.));
          mapdata->invertJacobianMatrix();
          melt->geomMapData_p = mapdata;
        }
      //precompute reference shape values
      if(useAux) qshvs_p = &relt->qshvs_aux; else qshvs_p = &relt->qshvs_;
      std::set<Quadrature*>::iterator itq=quads.begin();
      for(; itq!=quads.end(); ++itq)
        {
          itmq=qshvs_p->find(*itq);
          if(itmq==qshvs_p->end() || itmq->second.begin()->w.size()!=sw) //shapevalues re-computed
            {
              dimen_t dq = (*itq)->dim();                               //dimension of quadrature points
              number_t nq = (*itq)->numberOfPoints();                   //number of quadrature points
              if(!extendedDomain)  //no extension
                {
                  std::vector<real_t>::const_iterator itp = (*itq)->point();
                  (*qshvs_p)[*itq]=std::vector<ShapeValues>(nq,ShapeValues(*relt,der1,der2)); //to store all shapevalues at quadrature points
                  std::vector<ShapeValues>::iterator itshv = (*qshvs_p)[*itq].begin();
                  for(number_t i = 0; i < nq; i++, itp += dq, itshv++)
                    {
                      relt->computeShapeValues(itp, *itshv, der1,der2);
                      if(nbc>1) itshv->extendToVector(nbc);    //extend scalar shape functions to nbc vector shape functions
                    }
                }
              else // in case of extension build shapevalues for each side of ref element stored sequentially
                {
                  number_t nbs = relt->geomRefElem_p->nbSides();
                  (*qshvs_p)[*itq]=std::vector<ShapeValues>(nbs*nq,ShapeValues(*relt,der1,der2)); //to store all shapevalues at quadrature points
                  std::vector<ShapeValues>::iterator itshv = (*qshvs_p)[*itq].begin();
                  for(number_t s=1; s<=nbs; ++s) //loop on side
                    {
                      std::vector<real_t>::const_iterator itp = (*itq)->point();
                      for(number_t i = 0; i < nq; i++, itp += dq, itshv++)
                        {
                          relt->computeShapeValues(grelt->sideToElt(s,itp).begin(), *itshv, der1,der2);
                          if(nbc>1) itshv->extendToVector(nbc);    //extend scalar shape functions to nbc vector shape functions
                        }
                    }
                }
            }
        }
    }

  //precompute signDofs of element if required
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t k = 0; k < subsp->nbOfElements(); k++) // data related to element
    {
      const Element* elt = subsp->element_p(k);
      if(elt->refElt_p->dofCompatibility == _signDofCompatibility)  elt->setDofSigns();
    }

  MeshDomain* mdom=const_cast<MeshDomain*>(dom.meshDomain());
  //compute map of quadrature points (parallel)
  #ifdef XLIFEPP_WITH_OMP
  #pragma omp parallel for
  #endif // XLIFEPP_WITH_OMP
  for(number_t k = 1; k <= mdom->numberOfElements(); k++)
    {
      GeomElement* gelt =mdom->element(k);
      if(twinAnalyze && gelts.find(gelt)==gelts.end())  //in case of an interface, may be some element refers to the twin of geomelement
        {
          gelt=gelt->twin_p;
          if(gelt==nullptr || (gelt!=nullptr && gelts.find(gelt)==gelts.end()))
            error("free_error","internal error in preComputationIE, geomElement not found");
        }
      //geometry information
      if(gelt->meshElement()==nullptr) gelt->buildSideMeshElement();
      MeshElement* melt = gelt->meshElement();
      GeomMapData* mapdata=melt->geomMapData_p;
      if(mapdata==nullptr)  // new melt->geomMapData_p
      {
        mapdata=new GeomMapData(melt);
        melt->geomMapData_p = mapdata;
        if(isogeo)
        {
          mapdata->isoPar_p = &mdom->parametrization();
          mapdata->useParametrization = true;
          mapdata->useIsoNodes = true;
        }
        mapdata->computeJacobianMatrix(std::vector<real_t>(gelt->elementDim(),0.));
        mapdata->computeJacobianDeterminant();// induced  mapdata->computeDifferentialElement();
        mapdata->invertJacobianMatrix();
        if(nor) mapdata->computeOrientedNormal();
      }
      else
      {
        if((mapdata->useParametrization && !isogeo) || (!mapdata->useParametrization && isogeo)) // change melt->geomMapData_p
        {
          if(isogeo)
          {
            mapdata->isoPar_p = &mdom->parametrization();
            mapdata->useParametrization = true;
            mapdata->useIsoNodes = true;
            setElement(gelt);
          }
          else {mapdata->useParametrization = false; mapdata->useIsoNodes = false;}
          mapdata->computeJacobianMatrix(std::vector<real_t>(gelt->elementDim(),0.));
          mapdata->computeJacobianDeterminant();// induced  mapdata->computeDifferentialElement();
          mapdata->invertJacobianMatrix();
          if(nor) mapdata->computeOrientedNormal();
        }
        else // update melt->geomMapData_p
        {
          if(mapdata->jacobianMatrix.size()==0) mapdata->computeJacobianMatrix(std::vector<real_t>(gelt->elementDim(),0.));
          if(mapdata->jacobianDeterminant==0) mapdata->computeJacobianDeterminant();
          mapdata->invertJacobianMatrix();
          if(nor && mapdata->normalVector.size()==0) mapdata->computeOrientedNormal();
        }
      }
      melt->geomMapData_p = mapdata;
      // construct P1 meshElement if isogeo or not linear element
      if(!melt->linearMap || isogeo)  // P1 element related to curved element
      {
        if(isogeo && melt->meltP1==melt) melt->meltP1=nullptr; //force construction of meltP1 (case isogeo and P1 mesh element)
        melt->meltP1 = melt->toP1();                           // create related P1 MeshElement
        if(melt->meltP1->geomMapData_p==nullptr)               // geometric data related to P1 element with jacobian and inverse jacobian
           melt->meltP1->geomMapData_p = new GeomMapData(melt->meltP1,true,true,false);
      }
      //thePrintStream<<(*melt)<<eol;
      //physical points
      std::set<Quadrature*>::iterator itq=quads.begin();
      for(; itq!=quads.end(); ++itq)
        {
          dimen_t dq = (*itq)->dim();                                 //dimension of quadrature points
          number_t nq = (*itq)->numberOfPoints();                     //number of quadrature points
          std::map<Quadrature*,std::vector<Point> >::iterator itmqphy= mapdata->phyPoints.find(*itq);
          if(itmqphy==mapdata->phyPoints.end())  //create physical points
            {
              mapdata->phyPoints[*itq] = std::vector<Point>(nq);
              std::vector<Point>::iterator itphy=mapdata->phyPoints[*itq].begin();
              std::vector<real_t>::const_iterator itp = (*itq)->point();
              for(number_t i = 0; i < nq; i++, itp += dq, ++itphy) *itphy = mapdata->geomMap(itp);
            }
         //thePrintStream<<" quad points : "<<mapdata->phyPoints[*itq]<<eol;
        }
    }
}

/*! precompute geometry data and shape values on any element for any quadrature involved
    restricted to one order geometric element, take large place but computation is then faster in case of IE!!!

    subsp: subspace providing the list of elements
    quads: list of quadratures
    nbc: number of components of the unknown
    der1: compute shape first derivatives if true
    der2: compute shape second derivatives if true
    nor: compute normal if true
*/
void preComputationIE_old(const Space* subsp, std::set<Quadrature*>& quads, dimen_t nbc, bool der1, bool der2, bool nor)
{
  der1=true;  //force computation of derivatives to avoid misfit when recall

  for(number_t k = 0; k < subsp->nbOfElements(); k++) // data related to u
    {
      const Element* elt = subsp->element_p(k);
      GeomElement* gelt = elt->geomElt_p;

      //geometry information
      if(gelt->meshElement()==nullptr) gelt->buildSideMeshElement();
      MeshElement* melt = gelt->meshElement();
      GeomMapData* mapdata=melt->geomMapData_p;
      if(mapdata==nullptr) mapdata=new GeomMapData(melt);
      mapdata->computeJacobianMatrix(std::vector<real_t>(elt->dim(),0.));
      mapdata->invertJacobianMatrix();
      mapdata->computeDifferentialElement();
      melt->geomMapData_p = mapdata;
      if(nor) mapdata->computeOrientedNormal();

      //shapevalues and physical points for all single quadrature
      RefElement* relt = elt->refElt_p;                              //reference element related to element
      std::set<Quadrature*>::iterator itq=quads.begin();
      for(; itq!=quads.end(); ++itq)
        {
          std::map<Quadrature*,std::vector<ShapeValues> >::iterator itmq=relt->qshvs_.find(*itq);
          dimen_t dq = (*itq)->dim();                                 //dimension of quadrature points
          number_t nq = (*itq)->numberOfPoints();                     //number of quadrature points
          if(itmq==relt->qshvs_.end()) //shapevalues not computed
            {
              relt->qshvs_[*itq]=std::vector<ShapeValues>(nq,ShapeValues(*relt,der1,der2)); //to store all shapevalues at quadrature points
              std::vector<real_t>::const_iterator itp = (*itq)->point();
              std::vector<ShapeValues>::iterator itshv = relt->qshvs_[*itq].begin();
              for(number_t i = 0; i < nq; i++, itp += dq, itshv++)
                {
                  relt->computeShapeValues(itp, *itshv, der1,der2);
                  if(nbc>1) itshv->extendToVector(nbc);    //extend scalar shape functions to nbc vector shape functions
                }
            }
          //compute map of quadrature points
          std::map<Quadrature*,std::vector<Point> >::iterator itmqphy= mapdata->phyPoints.find(*itq);
          if(itmqphy==mapdata->phyPoints.end())  //create physical points
            {
              mapdata->phyPoints[*itq] = std::vector<Point>(nq);
              std::vector<Point>::iterator itphy=mapdata->phyPoints[*itq].begin();
              std::vector<real_t>::const_iterator itp = (*itq)->point();
              for(number_t i = 0; i < nq; i++, itp += dq, ++itphy)
                *itphy = mapdata->geomMap(itp);                      //quadrature point in physical space
            }
        }
    }
}

//===================================================================================================================================
/*! check and set quadrature pointers of IntegrationsMethods if they are not set

  if intgMethods is empty
      if kernel is singular (Kernel::singularType!=_notSingular) add if possible
          - a well suited singular quadrature method applied to adjacent elements (dist=0)
          - some regular quadrature products applied to non adjacent elements (one for 0<dist<=1, one for 1<dist<=2 and one for dist>2)
      if kernel is regular (Kernel::singularType==_notSingular) add
          - a regular quadrature products
  if intgMethods contains only a singular quadrature
      if kernel is regular stop on error
      if kernel is singular, check if singular quadrature is consistent
         if not stop on error
         else add some regular quadrature products applied to non adjacent elements (one for 0<dist<=1, one for 1<dist<=2 and one for dist>2)
  if intgMethods contains only a regular quadrature
      if kernel is regular check if it is consistent
      if kernel is singular add a well suited singular quadrature method applied to adjacent elements (dist=0)
  if intgMethods are more than one integration method
     if kernel is singular,
        check if there is only one consistent singular quadrature for dist=0, if not stop on error
        if there is no regular quadrature, add some
     if kernel is regular, check that all are regular quadratures

 At the end of the process, produce an intgMethods vector consistent with the Kernel
 In a second step, update pointers of quadrature if required
*/
// Implemented here due to a cyclic dependance library
//===================================================================================================================================
void DoubleIntgBilinearForm::setIntegrationMethods()
{
  const Kernel* kerp =nullptr;
  if(kopus_p!=nullptr) kerp= kopus_p->kernel();
  else if(lckopus_p!=nullptr)   //case of a linear combination of KernelOperatorOnUnknowns
    {
      real_t sor=0;
      std::vector<const Kernel*> ks=lckopus_p->kernels();
      std::vector<const Kernel*>::iterator itks=ks.begin();
      for(; itks!=ks.end(); ++itks)
        if(*itks!=nullptr && (*itks)->singularOrder<sor) {kerp=*itks; sor=kerp->singularOrder;}
    }
  string_t mes="in DoubleIntgBilinearForm::setIntegrationMethods(), ";
  const std::set<ShapeType>& shapes_u = domainu_p->meshDomain()->shapeTypes;
  const std::set<ShapeType>& shapes_v = domainv_p->meshDomain()->shapeTypes;
  if(shapes_u.size()!=1 || shapes_v.size()!=1)
    {
      where("DoubleIntgBilinearForm::setIntegrationMethods()");
      error("domain_multiple_shapes");
    }
  ShapeType shu = *shapes_u.begin(), shv = *shapes_v.begin();

  //analyze given integration methods
  SingularityType st=_notsingular;
  real_t so = 0;
  bool hasSingPart = false, hasRegPart = false;
  string_t sn ="Id", ssn="Id";
  number_t nbsq=0, nbrq=0, i=0;
  bool waitRegularPart = false;
  if(kerp!=nullptr)  //if kerp not allocated, assume ker=Id
    {
      st=kerp->singularType;
      so=kerp->singularOrder;
      hasSingPart = kerp->singPart!=nullptr;
      hasRegPart  = kerp->regPart!=nullptr;
      sn=kerp->shortname;
      if(hasSingPart) ssn = kerp->singPart->shortname;
    }
  std::multimap<real_t, int_t> ims;  //intg methods number indexed by distance (negative number for singular quadrature method)
  std::multimap<real_t, int_t>::iterator itm, itme;
  std::vector<IntgMeth>::iterator iti=intgMethods.begin();
  for(; iti!=intgMethods.end(); ++iti, ++i)   //loop on integration methods
    {
      const IntegrationMethod* im =iti->intgMeth;
      if(im->imType==_quadratureIM)  //single quadrature, build the product of quadrature
        {
          const QuadratureIM* qim = static_cast<const QuadratureIM*>(im);
          iti->intgMeth = new ProductIM(qim->quadRule,qim->degree);
          im = iti->intgMeth;
        }
      if(!im->isDoubleIM())
        {
          where("DoubleIntgBilinearForm::setIntegrationMethods()");
          error("im_not_single");
        }
      if(im->imType==_CollinoIM)  //special case of Collino IM, check validity
        {
          checkCollinoIM(im);
        }
      FunctionPart pai=iti->functionPart;
      if(pai==_regularPart && !hasRegPart)
        {
          where("DoubleIntgBilinearForm::setIntegrationMethods()");
          error("null_pointer","kerp->regPart");
        }
      if(pai==_singularPart && !hasSingPart)
        {
          where("DoubleIntgBilinearForm::setIntegrationMethods()");
          error("null_pointer","kerp->singPart");
        }
      SingularityType sti=im->singularType;
      real_t soi=im->singularOrder;
      real_t di = iti->bound;
      if(sti ==_notsingular)    //regular quadrature
        {
          if(st!=_notsingular && pai!=_regularPart && di==0)
            {
              where("DoubleIntgBilinearForm::setIntegrationMethods()");
              warning("im_regular_for_singular",im->name);
            }
          ims.insert(std::pair<real_t,number_t>(di,i+1));
          nbrq++;  //regular quadrature is valid
        }
      else //singular quadrature
        {
          if(sti!=st || soi!=so)   //check singularity consitency
            {
              where("DoubleIntgBilinearForm::setIntegrationMethods()");
              error("im_not_handled",im->name);
            }
          string_t nai=im->kerName;
          string_t na= sn;
          if(pai==_singularPart) {na=ssn; waitRegularPart = true;}
          if(nai!="" && nai!=na)   // check if intg method is dedicated to particular kernel
            {
              where("DoubleIntgBilinearForm::setIntegrationMethods()");
              error("im_not_handled",im->name);
            }
//      if (di>0)
//      {
//        where("DoubleIntgBilinearForm::setIntegrationMethods()");
//        error("im_not_handled",im->name);
//      }
          IntegrationMethodType imt=im->type();
          string_t mes2="";
          if((imt==_SauterSchwabIM  || imt==_SauterSchwabSymIM || imt==_LenoirSalles3dIM || imt==_CollinoIM) && (shu!=_triangle || shv!=_triangle))
            {
              where("DoubleIntgBilinearForm::setIntegrationMethods()");
              error("im_shape_only", im->name, words("shape", _triangle), words("shape",shu), words("shape",shv));
            }
          if((imt==_DuffyIM || imt==_LenoirSalles2dIM) && (shu!=_segment || shv!=_segment))
            {
              where("DoubleIntgBilinearForm::setIntegrationMethods()");
              error("im_shape_only", im->name, words("shape", _segment), words("shape",shu), words("shape",shv));
            }
          if(imt==_SauterSchwabSymIM && kerp!=nullptr && kerp->symmetry!=_symmetric)
            warning("free_warning", "SauterSchwabSymIM is designed for symmetric kernel, "+kerp->shortname+" seems to be not!");
          ims.insert(std::pair<real_t,number_t>(di,-(i+1)));
          nbsq++;  //singular quadrature is valid
        }
    }

  // finalize analyze and add omitted integration methods by using default ones
  if(st!=_notsingular && so<0 && domainu_p==domainv_p)   //kernel is singular, nbsq should be 1
    {
      if(nbsq>1)  //too much singular quadrature
        {
          where("DoubleIntgBilinearForm::setIntegrationMethods()");
          string_t mes="";
          itm=ims.lower_bound(0.); itme = ims.upper_bound(0.);
          for(; itm!=itme; ++itm)
            if(itme->second<0) mes+=" "+intgMethods[std::abs(itme->second)-1].intgMeth->name;
          error("multiple_im",mes);
        }
      if(nbsq==0)  //add default singular method  (NOT WORK FOR MOMENT BECAUSE DuffyIMNew and SauterSchwabNew are not included, lib dependance !)
        {
          if(shu==_segment &&  shv==_segment)   //use Duffy order 6
            intgMethods.push_back(IntgMeth(DuffyIM(),_allFunction, 0.));
          else
            {
              if(shu==_triangle && shv==_triangle)   //use Sauter-Schwab order 3
                intgMethods.push_back(IntgMeth(SauterSchwabIM(),_allFunction, 0.));
              else
                {
                  where("DoubleIntgBilinearForm::setIntegrationMethods()");
                  error("im_not_found");
                }
            }
          nbsq=1;
        }
    }

  //deal with regular quadratures
  if(st!=_notsingular && so<0)   //kernel is singular
    {
      if(waitRegularPart)   //find if there is a regular quadrature for regularPart and dist=0
        {
          itm=ims.lower_bound(0.); itme = ims.upper_bound(0.);
          for(; itm!=itme && waitRegularPart ; ++itm)
            if(itme->second>0 && intgMethods[itme->second-1].functionPart==_regularPart)  waitRegularPart=false;
        }
      if(waitRegularPart)  // create a quadrature for regular part
        intgMethods.push_back(IntgMeth(ProductIM(symmetricalGauss,3),_regularPart, 0.));

      if(nbrq==0 && intgMethods.begin()->intgMeth->imType!=_CollinoIM)  //add additional regular quadrature (except for Collino method)
        {
          intgMethods.push_back(IntgMeth(ProductIM(symmetricalGauss,5),_allFunction, 1.));
          intgMethods.push_back(IntgMeth(ProductIM(symmetricalGauss,4),_allFunction, 2.));
          intgMethods.push_back(IntgMeth(ProductIM(symmetricalGauss,3),_allFunction, theRealMax));
        }
    }
  else  //kernel is regular
    {
      if(nbrq==0) intgMethods.push_back(IntgMeth(ProductIM(symmetricalGauss,3),_allFunction, theRealMax));
    }

  // update QuadratureIM pointers of regular quadratures
  for(iti=intgMethods.begin(); iti!=intgMethods.end(); ++iti)
    {
      const IntegrationMethod* im =iti->intgMeth;
      if(im->type()==_productIM)  //update single quadrature pointers
        {
          const ProductIM* pim=static_cast<const ProductIM*>(im);
          if(pim->getxIM()->type()==_quadratureIM) static_cast<QuadratureIM*>(pim->getxIM())->setQuadratures(shapes_v);
          if(pim->getyIM()->type()==_quadratureIM) static_cast<QuadratureIM*>(pim->getyIM())->setQuadratures(shapes_u);
          pim->name = pim->getxIM()->name+" x "+pim->getyIM()->name;
        }
    }
}

void DoubleIntgBilinearForm::setHMIntegrationMethods()
{
  if(intgMethod_p==nullptr) return;   //no integration method
  if(intgMethod_p->type()!=_HMatrixIM) return;   //not a HMatrixIM method
  const HMatrixIM* him= static_cast<const HMatrixIM*>(intgMethod_p);
  intgMethods=him->intgMethods;
  setIntegrationMethods();
}
// check that CollinoIM is available for the integral
void DoubleIntgBilinearForm::checkCollinoIM(const IntegrationMethod* im)
{
  CollinoIM* cim = reinterpret_cast<CollinoIM*>(const_cast<IntegrationMethod*>(im));
  // check bilinear form
  bool ok = false;
  if(kopus_p!=nullptr)
    {
      const Interpolation* iu = kopus_p->opu().unknown()->space()->interpolation(),
                           *iv = kopus_p->opv().unknown()->space()->interpolation();
      ok = iu->type==_RaviartThomas && iu->numtype==1 && iv->type==_RaviartThomas && iv->numtype==1;  //spaces ok
      //check form (grad_y G(x,y) x wj(y)).wi(x) or (grad_x G(x,y) x wj(y)).wi(x)
      if(ok) ok = kopus_p->kernel()->shortname == "Helmz3D";
      if(ok) ok = kopus_p->opker().ydifOpType()==_grad_y || kopus_p->opker().xdifOpType()==_grad_x;
      if(ok) ok = kopus_p->opu().difOpType()==_id && kopus_p->opv().difOpType()==_id;
      if(ok) cim->computeFlag=_computeI2;
    }
  else if(lckopus_p!=nullptr)
    {
      uvPair us=lckopus_p->unknowns();
      const Interpolation* iu = us.first->space()->interpolation(), *iv=us.second->space()->interpolation();
      ok = iu->type==_RaviartThomas && iu->numtype==1 && iv->type==_RaviartThomas && iv->numtype==1;  //spaces ok
      //check form k*G(x,y)*wi(x).wj(y)-1/k G*div(wi(x)*div(wj(y) ]
      if(ok && lckopus_p->size()==2)  //check form (grad_y G(x,y) x wj(y)).wi(x)
        {
          cit_opkuvval it = lckopus_p->begin();
          const OperatorOnKernel&  opk=it->first->opker();
          ok = opk.kernelp()->shortname == "Helmz3D" && opk.xdifOpType()==_id && opk.ydifOpType()==_id;
          const OperatorOnUnknown& opu=it->first->opu();
          const OperatorOnUnknown& opv=it->first->opv();
          if(ok)
            {
              bool wait_div = opu.difOpType()==_id && opv.difOpType()==_id;
              bool wait_id  = opu.difOpType()==_div && opv.difOpType()==_div;
              ok = wait_id || wait_div;
              if(ok)
                {
                  it++;
                  const OperatorOnKernel& opk=it->first->opker();
                  ok = opk.kernelp()->shortname == "Helmz3D" && opk.xdifOpType()==_id && opk.ydifOpType()==_id;
                  const OperatorOnUnknown& opu=it->first->opu();
                  const OperatorOnUnknown& opv=it->first->opv();
                  if(ok && wait_div) ok = opu.difOpType()==_div && opv.difOpType()==_div;
                  if(ok && wait_id)  ok = opu.difOpType()==_id && opv.difOpType()==_id;
                  if(ok) cim->computeFlag=_computeI1;
                }
            }
        }
    }
//  if(!ok)
//    {
//      where("DoubleIntgBilinearForm::setIntegrationMethods()");
//      error("free_error","Collino intg method is restricted to particular integrals with first order Raviart-Thomas elements, see doc");
//    }
}

void IntegrationMethods::buildParam(const Parameter& p, IntegrationMethodType& imt, IntegrationMethod*& meth, FunctionPart& fp, QuadRule& qr, number_t& qo, real_t& bound)
{
  ParameterKey key=p.key();
  switch (key)
  {
    case _pk_bound:
    {
      switch (p.type())
      {
        case _integer:
        {
          bound=real_t(p.get_n());
          break;
        }
        case _real:
        {
          bound=p.get_r();
          break;
        }
        default:
          error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_function_part:
    {
      switch(p.type())
      {
        case _integer:
        case _enumFunctionPart:
          fp=FunctionPart(p.get_n());
          break;
        default:
          error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_method:
    {
      switch (p.type())
      {
        case _pointerIntegrationMethod:
          meth=const_cast<IntegrationMethod*>(reinterpret_cast<const IntegrationMethod*>(p.get_p()));
          break;
        case _integer:
        case _enumIntegrationMethodType:
          imt=IntegrationMethodType(p.get_n());
          break;
        case _enumQuadRule:
          qr=QuadRule(p.get_n());
          break;
        default: error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_order:
    {
      if (p.type() == _integer) { qo=p.get_n(); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      break;
    }
    case _pk_quad:
    {
      switch (p.type())
      {
        case _pointerIntegrationMethod:
          meth=const_cast<IntegrationMethod*>(reinterpret_cast<const IntegrationMethod*>(p.get_p()));
          break;
        case _integer:
        case _enumQuadRule:
          qr=QuadRule(p.get_n());
          break;
        default: error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    default:
      error("intgmeths_unexpected_param_key",words("param key",key));
  }
}

void IntegrationMethods::build(const std::vector<Parameter>& ps)
{
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_method);
  params.insert(_pk_function_part);
  params.insert(_pk_quad);
  params.insert(_pk_order);
  params.insert(_pk_bound);
  std::set<ParameterKey> firstKeys={_pk_method, _pk_quad};

  std::map<std::pair<real_t, FunctionPart>, IntgMeth> methods;
  std::map<std::pair<real_t, FunctionPart>, IntgMeth>::const_iterator cit_m;
  IntegrationMethodType imt=_undefIM;
  IntegrationMethod* im=nullptr;
  FunctionPart fp=_allFunction;
  QuadRule qr=_defaultRule;
  number_t qo=0;
  real_t bound=0.;
  bool methodAdded=false;

  for (number_t i=0; i < ps.size(); ++i)
  {
    const Parameter& p=ps[i];
    ParameterKey key=p.key();
    ValueType type=p.type();

    if (i==0) // First, we have to store the first key: _quad or _method
    {
      if (firstKeys.find(key) == firstKeys.end()) { error("wrong_first_key", words("param key", key), words("param key", _pk_quad)+" "+words("or")+" "+words("param_key", _pk_method)); }
    }
    else if (firstKeys.find(key) != firstKeys.end()) // If one of first keys is repeated, then we have a new integration method to deal with before reading the repeated key
    {
      if (key == _pk_method && type == _enumIntegrationMethodType)
      {
        if (usedParams.find(_pk_bound) == usedParams.end()) {
          bound=0.;
        }
      }
      else
      {
        if (usedParams.find(_pk_bound) == usedParams.end()) {
          bound=theRealMax;
        }
      }
      // if the first key is repeated, we have to add the fully defined Quadrature formulae
      if (imt != _undefIM)
      {
        if (!methodAdded)
        {
          switch (imt)
          {
            case _SauterSchwabIM:
            {
              if (qo < 3) { qo = 3; }
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(SauterSchwabIM(qo), fp, bound)));
              break;
            }
            case _SauterSchwabSymIM:
            {
              if (qo < 3) { qo = 3; }
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(SauterSchwabSymIM(qo), fp, bound)));
              break;
            }
            case _DuffyIM:
            {
              if (qo < 6) { qo = 6; }
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(DuffyIM(qo), fp, bound)));
              break;
            }
            case _LenoirSalles2dIM:
            {
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(LenoirSalles2dIM(), fp, bound)));
              break;
            }
            case _LenoirSalles3dIM:
            {
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(LenoirSalles3dIM(), fp, bound)));
              break;
            }
            case _LenoirSalles2dIR:
            {
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(LenoirSalles2dIR(), fp, bound)));
              break;
            }
            case _LenoirSalles3dIR:
            {
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(LenoirSalles3dIR(), fp, bound)));
              break;
            }
            default:
              where("IntegrationMethods::build(...)");
              error("im_not_handled", words("imtype", imt));
          }
          methodAdded=true;
        }
        else { warning("param_already_used" "method"); }
      }
      else if (im != nullptr)
      {
        methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(*im, fp, bound)));
      }
      else { methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(QuadratureIM(qr, qo), fp, bound))); }
      params.insert(_pk_method);
      params.insert(_pk_function_part);
      params.insert(_pk_quad);
      params.insert(_pk_order);
      params.insert(_pk_bound);
      usedParams.clear();
      imt=_undefIM;
      qr=_defaultRule;
      qo=0;
      bound=theRealMax;
    }
    buildParam(p, imt, im, fp, qr, qo, bound);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) { error("intgmeths_unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);
    // when last key is analyzed, we have to add the last quadrature formulae
    if (i == ps.size()-1)
    {
      if (key == _pk_method && type ==_enumIntegrationMethodType)
      {
        if (usedParams.find(_pk_bound) == usedParams.end()) {
          bound=0.;
        }
      }
      else
      {
        if (usedParams.find(_pk_bound) == usedParams.end()) {
          bound=theRealMax;
        }
      }
      if (imt != _undefIM)
      {
        if (!methodAdded)
        {
          switch (imt)
          {
            case _SauterSchwabIM:
            {
              if (qo < 3) { qo = 3; }
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(SauterSchwabIM(qo), fp, bound)));
              break;
            }
            case _SauterSchwabSymIM:
            {
              if (qo < 3) { qo = 3; }
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(SauterSchwabSymIM(qo), fp, bound)));
              break;
            }
            case _DuffyIM:
            {
              if (qo < 6) { qo = 6; }
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(DuffyIM(qo), fp, bound)));
              break;
            }
            case _LenoirSalles2dIM:
            {
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(LenoirSalles2dIM(), fp, bound)));
              break;
            }
            case _LenoirSalles3dIM:
            {
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(LenoirSalles3dIM(), fp, bound)));
              break;
            }
            case _LenoirSalles2dIR:
            {
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(LenoirSalles2dIR(), fp, bound)));
              break;
            }
            case _LenoirSalles3dIR:
            {
              methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(LenoirSalles3dIR(), fp, bound)));
              break;
            }
            default:
              where("IntegrationMethods::build(...)");
              error("im_not_handled", words("imtype", imt));
          }
        }
        else { warning("param_already_used" "method"); }
      }
      else if (im != nullptr)
      {
        methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(*im, fp, bound)));
      }
      else { methods.insert(std::make_pair(std::make_pair(bound,fp), IntgMeth(QuadratureIM(qr, qo), fp, bound))); }
    }
  }

  intgMethods.reserve(methods.size());
  for (cit_m=methods.begin(); cit_m != methods.end(); ++cit_m) { intgMethods.push_back(cit_m->second); }
}

// IntegrationMethods constructors
// implemented here due to cyclic dependance library
IntegrationMethods::IntegrationMethods(IntegrationMethodType imt)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethodType)", "IntegrationMethods(_method=?)");
  switch (imt)
  {
    case _SauterSchwabIM: add(SauterSchwabIM(), _allFunction, theRealMax); break;
    case _SauterSchwabSymIM: add(SauterSchwabSymIM(), _allFunction, theRealMax); break;
    case _DuffyIM: add(DuffyIM(), _allFunction, theRealMax); break;
    case _LenoirSalles2dIM: add(LenoirSalles2dIM(), _allFunction, theRealMax); break;
    case _LenoirSalles3dIM: add(LenoirSalles3dIM(), _allFunction, theRealMax); break;
    case _LenoirSalles2dIR: add(LenoirSalles2dIR(), _allFunction, theRealMax); break;
    case _LenoirSalles3dIR: add(LenoirSalles3dIR(), _allFunction, theRealMax); break;
    case _CollinoIM: add(CollinoIM(), _allFunction, theRealMax); break;
    default:
      where("IntegrationMethods::IntegrationMethods(IntegrationMethodType)");
      error("im_not_handled", words("imtype", imt));
  }
}

IntegrationMethods::IntegrationMethods(QuadRule qr, number_t ord)
{
  warning("deprecated", "IntegrationMethods(QuadRule, Number)", "IntegrationMethods(_quad=?, _order=)");
  add(QuadratureIM(qr,ord), _allFunction, theRealMax);
}

IntegrationMethods::IntegrationMethods(IntegrationMethodType imt, QuadRule qr2, number_t ord2)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethodType, QuadRule, Number)", "IntegrationMethods(_method=?, _quad=?, _order=?)");
  switch (imt)
  {
    case _SauterSchwabIM: add(SauterSchwabIM(), _allFunction, 0.); break;
    case _SauterSchwabSymIM: add(SauterSchwabSymIM(), _allFunction, 0.); break;
    case _DuffyIM: add(DuffyIM(), _allFunction, 0.); break;
    case _LenoirSalles2dIM: add(LenoirSalles2dIM(), _allFunction, 0.); break;
    case _LenoirSalles3dIM: add(LenoirSalles3dIM(), _allFunction, 0.); break;
    case _LenoirSalles2dIR: add(LenoirSalles2dIR(), _allFunction, 0.); break;
    case _LenoirSalles3dIR: add(LenoirSalles3dIR(), _allFunction, 0.); break;
    default:
      where("IntegrationMethods::IntegrationMethods(IntegrationMethodType, QuadRule, Number)");
      error("im_not_handled", words("imtype", imt));
  }
  add(QuadratureIM(qr2,ord2), _allFunction, theRealMax);
}

IntegrationMethods::IntegrationMethods(IntegrationMethodType imt, number_t ord1, real_t b1, QuadRule qr2, number_t ord2)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethodType, Number, Real, QuadRule, Number)", "IntegrationMethods(_method=?, _order=?, _bound=?, _quad=?, _order=?)");
  switch (imt)
  {
    case _SauterSchwabIM: add(SauterSchwabIM(ord1), _allFunction, b1); break;
    case _SauterSchwabSymIM: add(SauterSchwabSymIM(ord1), _allFunction, b1); break;
    case _DuffyIM: add(DuffyIM(ord1), _allFunction, b1); break;
    case _LenoirSalles2dIM: add(LenoirSalles2dIM(), _allFunction, b1); break;
    case _LenoirSalles3dIM: add(LenoirSalles2dIM(), _allFunction, b1); break;
    case _LenoirSalles2dIR: add(LenoirSalles2dIR(), _allFunction, b1); break;
    case _LenoirSalles3dIR: add(LenoirSalles3dIR(), _allFunction, b1); break;
    default:
      where("IntegrationMethods::IntegrationMethods(IntegrationMethodType, Number, Real, QuadRule, Number)");
      error("im_not_handled", words("imtype", imt));
  }
  add(QuadratureIM(qr2,ord2), _allFunction, theRealMax);
}

IntegrationMethods::IntegrationMethods(IntegrationMethodType imt, number_t ord1, real_t b1,
                                       QuadRule qr2, number_t ord2, real_t b2, QuadRule qr3, number_t ord3)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethodType, Number, Real, QuadRule, Number, Real, QuadRule, Number)", "IntegrationMethods(_method=?, _order=?, _bound=?, _quad=?, _order=?, _bound=?, _quad=?, _order=?)");
  switch (imt)
  {
    case _SauterSchwabIM: add(SauterSchwabIM(ord1), _allFunction, b1); break;
    case _SauterSchwabSymIM: add(SauterSchwabSymIM(ord1), _allFunction, b1); break;
    case _DuffyIM: add(DuffyIM(ord1), _allFunction, b1); break;
    case _LenoirSalles2dIM: add(LenoirSalles2dIM(), _allFunction, b1); break;
    case _LenoirSalles3dIM: add(LenoirSalles2dIM(), _allFunction, b1); break;
    case _LenoirSalles2dIR: add(LenoirSalles2dIR(), _allFunction, b1); break;
    case _LenoirSalles3dIR: add(LenoirSalles3dIR(), _allFunction, b1); break;
    default:
      where("IntegrationMethods::IntegrationMethods(IntegrationMethodType, Number, Real, QuadRule, Number, Real, QuadRule, Number)");
      error("im_not_handled", words("imtype", imt));
  }
  add(QuadratureIM(qr2,ord2), _allFunction, b2);
  add(QuadratureIM(qr3,ord3), _allFunction, theRealMax);
}

IntegrationMethods::IntegrationMethods(IntegrationMethodType imt, number_t ord1, real_t b1,
                                       QuadRule qr2, number_t ord2, real_t b2, QuadRule qr3, number_t ord3, real_t b3, QuadRule qr4, number_t ord4)
{
  warning("deprecated", "IntegrationMethods(IntegrationMethodType, Number, Real, QuadRule, Number, Real, QuadRule, Number, Real, QuadRule, Number)", "IntegrationMethods(_method=?, _order=?, _bound=?, _quad=?, _order=?, _bound=?, _quad=?, _order=?, _bound=?, _quad=?, _order=?)");
  switch (imt)
  {
    case _SauterSchwabIM: add(SauterSchwabIM(ord1), _allFunction, b1); break;
    case _SauterSchwabSymIM: add(SauterSchwabSymIM(ord1), _allFunction, b1); break;
    case _DuffyIM: add(DuffyIM(ord1), _allFunction, b1); break;
    case _LenoirSalles2dIM: add(LenoirSalles2dIM(), _allFunction, b1); break;
    case _LenoirSalles3dIM: add(LenoirSalles2dIM(), _allFunction, b1); break;
    case _LenoirSalles2dIR: add(LenoirSalles2dIR(), _allFunction, b1); break;
    case _LenoirSalles3dIR: add(LenoirSalles3dIR(), _allFunction, b1); break;
    default:
      where("IntegrationMethods::IntegrationMethods(IntegrationMethodType, Number, Real, QuadRule, Number, Real, QuadRule, Number, Real, QuadRule, Number)");
      error("im_not_handled", words("imtype", imt));
  }
  add(QuadratureIM(qr2,ord2), _allFunction, b2);
  add(QuadratureIM(qr3,ord3), _allFunction, b3);
  add(QuadratureIM(qr4,ord4), _allFunction, theRealMax);
}

IntegrationMethods::IntegrationMethods(QuadRule qr1, number_t ord1, real_t b1, QuadRule qr2, number_t ord2)
{
  warning("deprecated", "IntegrationMethods(QuadRule, Number, Real, QuadRule, Number)", "IntegrationMethods(_quad=?, _order=?, _bound=?, _quad=?, _order=?)");
  add(QuadratureIM(qr1,ord1), _allFunction, b1);
  add(QuadratureIM(qr2,ord2), _allFunction, theRealMax);
}

IntegrationMethods::IntegrationMethods(QuadRule qr1, number_t ord1, real_t b1, QuadRule qr2, number_t ord2, real_t b2, QuadRule qr3, number_t ord3)
{
  warning("deprecated", "IntegrationMethods(QuadRule, Number, Real, QuadRule, Number, Real, QuadRule, Number)", "IntegrationMethods(_quad=?, _order=?, _bound=?, _quad=?, _order=?, _bound=?, _quad=?, _order=?)");
  add(QuadratureIM(qr1,ord1), _allFunction, b1);
  add(QuadratureIM(qr2,ord2), _allFunction, b2);
  add(QuadratureIM(qr3,ord3), _allFunction, theRealMax);
}

std::ostream& operator<<(std::ostream& out, const IEcomputationParameters & iep)
{
  out << "IEcomputationParameters:" << eol;
  out << "  - space dimension = " << iep.dim_sp;
  out << "  - number of bilinear form = " << iep.nb_bf << eol;
  out << "  - pointer to mesh_element u = " << iep.melt_u << ", pointer to mesh_element v = " <<  iep.melt_v << eol;
  out << "  - extend u = " << (iep.extend_u?string_t("true"):string_t("false")) << ", extend v = " << (iep.extend_v?string_t("true"):string_t("false")) << eol;
  out << "  - side_u = " << iep.side_u << ", side_v = " <<  iep.side_v << eol;
  out << "  - fe map u = " <<  words("FE map type",iep.femt_u) << ", fe map v = " <<  words("FE map type",iep.femt_v) << eol;
  out << "  - change sign of u = " << (iep.changeSign_u?string_t("true"):string_t("false")) << ", change sign of u = " << (iep.changeSign_v?string_t("true"):string_t("false")) << eol;
  out << "  - pointer to sign of u = " << iep.sign_u << ", pointer to sign of v = " <<  iep.sign_v << eol;
  out << "  - shape of u = " << words("shape",iep.sh_u) << " shape of v = " <<  words("shape",iep.sh_v) << eol;
  out << "  - degree u = " << iep.ord_u << ", degree v = " << iep.ord_v << eol;
  out << "  - dim fun u = " << iep.dimf_u << ", dim fun v = " << iep.dimf_v << eol;
  out << "  - dim fun phy u = " << iep.dimfp_u << ", dim fun phy v = " << iep.dimfp_v << eol;
  out << "  - opu is id = " << (iep.opisid_u?string_t("true"):string_t("false")) << ", opv is id = " << (iep.opisid_v?string_t("true"):string_t("false")) << eol;
  out << "  - opu has fun = " << (iep.hasf_u?string_t("true"):string_t("false")) << ", opv has fun = " << (iep.hasf_v?string_t("true"):string_t("false")) << eol;
  out << "  - diff order of opu = " << iep.difforder_u << ", diff order of opv =" <<  iep.difforder_v << eol;
  out << "  - kernel is scalar = " << (iep.scalar_k?string_t("true"):string_t("false")) << eol;
  out << "  - FE is P0 = " << (iep.isP0?string_t("true"):string_t("false")) << eol;
  out << "  - operator is Id for u and v = " << (iep.isId?string_t("true"):string_t("false")) << eol;
  out << "  - x-normal resuired by kernel = " << (iep.knor_x?string_t("true"):string_t("false")) << ", y-normal resuired by kernel = " << (iep.knor_y?string_t("true"):string_t("false")) << eol;
  out << "  is uptodate = " << (iep.isUptodate?string_t("true"):string_t("false")) << eol;
  return out;
}

} // end of namespace xlifepp
