/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file IESPMatrixComputation.hpp
  \author E. Lunéville
  \since 9 jan 2013
  \date 25 juin 2013

  \brief Implementation of template FE TermMatrix computation functions

  this file is included by TermMatrix.hpp
  do not include it elsewhere
*/


/* ================================================================================================
                                     tools
   ================================================================================================ */

namespace xlifepp
{

/*! utility for computeSP:  compute sum_q[pq*phi_m(Xq)*wj(xq)] from wj(xq)  GENERAL CASE
     (xq, Xq) : quadrature points in reference/physical space
     (phi_m)  : basis functions from spectral basis
     (pq)     : quadrature weights
     (wj(xq)) : FE shape functions at quadrature points (already computed)

      val: wj(xq)) may be d-vector shape functions: val(q) = w11(xq),w12(xq), ..., w1d(xq), w21(xq), ...
      spbasis: spectral basis (function)
      phyPts: physical points
      quad: quadrature object
      mapto: if non zero, pointer to the map: domain -> reference domain of spectral basis

      phi_w: computed values  sum_q[pq*phi_m(Xq)*wj(xq)]        when isconj=false
                              sum_q[pq*conj(phi_m(Xq))*wj(xq)]  when isconj=true
*/
template<typename K>
void computeSPfunByQuadrature(const std::vector<Vector<K> >& val, const SpectralBasis& spbasis,
                              const std::vector<Point>& phyPts, const Quadrature& quad,
                              std::vector<Vector<K> >& phi_w, bool isconj, const Function* mapto = nullptr)
{
  number_t n = spbasis.numberOfFun();
  ValueType vtb = spbasis.valueType();
  dimen_t d=spbasis.dimFun();  //must be equal to shape function dimension
  const SpectralBasisFun& spbfun=reinterpret_cast<const SpectralBasisFun&>(spbasis);

  std::vector<Point>::const_iterator itpt = phyPts.begin();           //quadrature point in physical space (first point)
  typename std::vector<Vector<K> >::const_iterator itv = val.begin();
  typename Vector<K>::const_iterator itvq;
  typename std::vector<Vector<K> >::iterator itphw = phi_w.begin();
  typename Vector<K>::iterator itvf;
  typename Vector<Vector<K> >::iterator itph;

  if(d==1) //scalar case
    {
      Vector<K> phin(n, K(0));
      for(; itphw != phi_w.end(); itphw++) *itphw = phin; //reset phi_w
      for(; itpt != phyPts.end(); itpt++, itv++)          //loop on quadrature points
        {
          Point P(*itpt);
          if(mapto != nullptr) //map physical point to point in the reference space of basisfunction
            {
              std::vector<real_t> Q;
              P = Point((*mapto)(P, Q));
            }
          if(vtb == _real)         //test value type of basis functions
            {
              Vector<real_t> phinr(n, 0.);
              phin = spbfun.functions(P, phinr);   // compute all basis functions at quadrature point (real)
            }
          else spbfun.functions(P, phin);          // compute all basis functions at quadrature point (complex)
          if(isconj && vtb==_complex) phin.toConj();

          itvq = itv->begin();
          itphw = phi_w.begin();
          for(; itphw != phi_w.end(); itphw++, itvq++) *itphw += phin** itvq; //loop on FE shape functions
        }
      return;
    }

  //vector case
  Vector<K> zero(n, K(0));
  for(; itphw != phi_w.end(); itphw++) *itphw = zero;  //reset phi_w
  Vector<Vector<K> > phin(n);
  for(; itpt != phyPts.end(); itpt++, itv++)          //loop on quadrature points in physical space
    {
      Point P(*itpt);
      if(mapto != nullptr) //map physical point to point in the reference space of basis function
        {
          std::vector<real_t> Q;
          P = Point((*mapto)(P, Q));
        }
      if(vtb == _real)         //test value type of basis functions
        {
          Vector<Vector<real_t> > phinr(n);      //phin_r(j)=(fj1,...,fjd)
          phin = spbfun.functions(P, phinr);   // compute all basis functions at quadrature point (real)
        }
      else spbfun.functions(P, phin);          // compute all basis functions at quadrature point (complex)
      if(isconj && vtb==_complex) phin.toConj();

      itvq = itv->begin();
      itphw = phi_w.begin();
      for(; itphw != phi_w.end(); itphw++)    //loop on FE shape functions wj
        {
          for(dimen_t k=0; k<d; k++, itvq++)            //loop on dimension d
            {
              itph=phin.begin();
              for(itvf=itphw->begin(); itvf!=itphw->end(); itvf++, itph++) //loop on spectral basis function
                *itvf += (*itph)[k]** itvq;
            }
        }
    }
}

/*! utility for computeSP, compute  sum_q[pq*phi_m(Xq) op wj(xq)] from wj(xq) (INTERPOLATED CASE, interpolated spectral basis function)
     (xq, Xq) : quadrature points in reference/physical space
     (phi_m)  : basis functions from spectral basis
     (pq)     : quadrature weights
     (wj(xq)) : FE shape functions at quadrature points (already computed)
      op:  * or |, automatically detected regarding structure of SpectralBasis

    phi_m(Xq) is computed by interpolation:
             sum_s phi_ms tau_s(xq) where phi_ms are components of phi_m in current element
    two cases occur:
       - same interpolation (common case) -> tau_s = wj
       - different interpolation -> computation of tau_s

      val: wj(xq)) may be d-vector shape functions: val(q) = w11(xq),w12(xq), ..., w1d(xq), w21(xq), ...
      spbasis: spectral basis (vector)
      phyPts: physical points
      quad: quadrature object
      mapto: if non zero, pointer to the map: domain -> reference domain of spectral basis
      elt: current element
      space_u: space
      dom: current domain
      shv: shapevalues

      phi_w: computed values  Vjm=sum_q[pq*phi_m(Xq) op wj(xq)] ( say phi_w(j)=[Vj1,Vj2, ..., Vjn])
*/
template<typename K>
void computeSPintByQuadrature(const std::vector<Vector<K> >& val, const SpectralBasis& spbasis,
                              const std::vector<Point>& phyPts, const Quadrature& quad,
                              const Element& elt, const Space* space_u, const GeomDomain* dom,
                              const std::vector<ShapeValues>& shv,
                              std::vector<Vector<K> >& phi_w, bool isconj, const Function* mapto = nullptr)
{

  number_t n = spbasis.numberOfFun();
  if(n== 0)
    {
      where("computeSPintByQuadrature(...)");
      error("is_void", "spbasis");
    }
  const SpectralBasisInt& spbint=reinterpret_cast<const SpectralBasisInt&>(spbasis);
  ValueType vtb = spbasis.valueType();
  dimen_t d=spbasis.dimFun();  //must be equal to shape function dimension
  std::vector<Point>::const_iterator itpt = phyPts.begin();        //quadrature point in physical space (first point)
  typename std::vector<Vector<K> >::const_iterator itv = val.begin();
  typename Vector<K>::const_iterator itvq;
  typename std::vector<Vector<K> >::iterator itphw = phi_w.begin();
  typename Vector<K>::iterator itvf;
  typename Vector<Vector<K> >::iterator itph;

  if(d==1) //scalar case
    {
      Vector<K> phin(n, K(0));
      for(; itphw != phi_w.end(); itphw++) *itphw = phin;              //reset phi_w
      for(; itpt != phyPts.end(); itpt++, itv++)          //loop on quadrature points in physical space
        {
          Point P(*itpt);
          if(mapto != nullptr) //map physical point to point in the reference space of basis function
            {
              std::vector<real_t> Q;
              P = Point((*mapto)(P, Q));
            }
          if(vtb == _real)         //test value type of basis functions
            {
              Vector<real_t> phinr(n, 0.);
              phin = spbint.functions(P, phinr);   // compute all basis functions at quadrature point (real)
            }
          else spbint.functions(P, phin);          // compute all basis functions at quadrature point (complex)
          if(isconj && vtb==_complex) phin.toConj();

          itvq = itv->begin();
          itphw = phi_w.begin();
          for(; itphw != phi_w.end(); itphw++, itvq++) *itphw += phin** itvq; //loop on FE shape functions
        }
      return;
    }

  //vector case
  Vector<K> zero(n, K(0));
  for(; itphw != phi_w.end(); itphw++) *itphw = zero;  //reset phi_w
  Vector<Vector<K> > phin(n);
  for(; itpt != phyPts.end(); itpt++, itv++)          //loop on quadrature points in physical space
    {
      Point P(*itpt);
      if(mapto != nullptr) //map physical point to point in the reference space of basis function
        {
          std::vector<real_t> Q;
          P = Point((*mapto)(P, Q));
        }
      if(vtb == _real)         //test value type of basis functions
        {
          Vector<Vector<real_t> > phinr(n);      //phin_r(j)=(fj1,...,fjd)
          phin = spbint.functions(P, phinr);   // compute all basis functions at quadrature point (real)
        }
      else spbint.functions(P, phin);          // compute all basis functions at quadrature point (complex)
      if(isconj && vtb==_complex) phin.toConj();

      itvq = itv->begin();
      itphw = phi_w.begin();
      for(; itphw != phi_w.end(); itphw++)    //loop on FE shape functions wj
        {
          for(dimen_t k=0; k<d; k++, itvq++)            //loop on dimension d
            {
              itph=phin.begin();
              for(itvf=itphw->begin(); itvf!=itphw->end(); itvf++, itph++) //loop on spectral basis function
                *itvf += (*itph)[k]** itvq;
            }
        }
    }
}

/* ================================================================================================
                                      IE SP computation algoritm
   ================================================================================================ */
/*!IE SP computation of a scalar SuBilinearForm on a == unique domain ==
   subf: a single unknown bilinear form defined on a unique domains pair (assumed)
   of IE form with a TensorKernel (see TensorKernel.hpp)  :

               /     /
               |     |   v(x)|[sum_mn psi_m(x)*Amn*phi_n(y)]|u(y) dy dx
               /domx /domy

    or when TensorKernel.isConjugate is true:
               /     /
               |     |   v(x)|[sum_mn psi_m(x)*Amn*conj(phi_n(y))]|u(y) dy dx
               /domx /domy

    Note that the conjugate is applied to phi_n basis related to unknown

   v: reference to matrix entries of type T
   vt: to pass as template the scalar type (real or complex)

   algorithm:
     loop on bilinear form
        loop on element on uy-domain
           compute op_u(wj)
           compute sum_k[pk*phi_m(yk)*wj(yk)]
           loop on element on vx-domain
              compute op_v(ti)
              compute sum_k[qk*psi_n(xk)*ti(xk)]  (psi may be conjugated)
              compute elem matrix by tensorial product

   Note: this algorithm is general and not optimal when psi=phi, (wj)=(ti),  op_u=op_v, quad_x=quad_y
          because some quantities are computed many times, nevertheless it is not memory expansive
          to be improved in future

*/
template <>
template <typename T, typename K>
void ComputationAlgorithm<_IESPComputation>::compute(const SuBilinearForm& subf, LargeMatrix<T>& mat, K& vt,
    Space* space_u_p, Space* space_v_p,  const Unknown* u_p, const TestFct* v_p)
{
  if(subf.size() == 0) return; // void bilinear form, nothing to compute !!!
  trace_p->push("SuTermMatrix::computeSP");

  if (theVerboseLevel>0) theCout << "computing FE term " << subf.asString() << ", using " << numberOfThreads() << " threads : " << std::flush;

  //retry spaces
  cit_vbfp it = subf.begin();
  const Space* sp_u = subf.up()->space();  //space of unknown
  const Space* sp_v = subf.vp()->space();  //space of test function
  const GeomDomain* domx = it->first->asDoubleIntgForm()->domainx(); //common domain x of integrals
  const GeomDomain* domy = it->first->asDoubleIntgForm()->domainy(); //common domain y of integrals
  bool same_interpolation = (sp_u == sp_v && domx == domy);

  //space_u_p and space_v_p are the largest subspaces involved in the computation, may be different from the whole spaces
  Space* subsp_u = sp_u->findSubSpace(domy, space_u_p);    //find subspace (linked to domain dom) of space_u_p
  if(subsp_u == nullptr) subsp_u = space_u_p;                    //is a FESpace, not a FESubspace
  Space* subsp_v = subsp_u;
  if(!same_interpolation)
    {
      subsp_v = sp_v->findSubSpace(domx, space_v_p);       //find subspace (linked to domain dom) of space_v_p
      if(subsp_v == nullptr) subsp_v = space_v_p;                //is a FESpace, not a FESubspace
    }

  //space_u properties
  number_t ordelt_u   = sp_u->interpolation()->numtype;
  dimen_t nbc_u = u_p->nbOfComponents();                          //number of components of unknown
  dimen_t dimf_u =sp_u->dimFun();
  number_t nbelt_u = subsp_u->nbOfElements();
  bool doflag_u = (subsp_u == space_u_p);
  RefElement*  relt_u =subsp_u->element_p(number_t(0))->refElt_p;  //assuming same elements
  FEMapType femt_u=relt_u->mapType;
  bool firstOrder_u  = (domy->order() == 1);
  dimen_t der_u = 0;
  bool mapsh_u = (femt_u!=_standardMap || der_u>0);
  bool changeSignTest_u = (relt_u->dofCompatibility == _signDofCompatibility);   //dof sign change for particular element (Nedelec, Raviart, ....)
  bool changeSign_u = false;
  Vector<real_t>* sign_u=nullptr;
  bool nor_u=false;
  Vector<real_t>* ny= nullptr;
  std::set<Quadrature*>  quad_u;      //to store single quadrature pointers
  std::vector<Vector<K> > val_u;      //to store values of opu on quadrature point
  std::vector<number_t> dofNum_u;

  //space_v properties
  number_t ordelt_v   = sp_v->interpolation()->numtype;
  dimen_t nbc_v = v_p->nbOfComponents();                        //number of components of test function
  dimen_t dimf_v =sp_v->dimFun();
  number_t nbelt_v = subsp_v->nbOfElements();
  bool doflag_v = (subsp_v == space_v_p);
  RefElement*  relt_v =subsp_v->element_p(number_t(0))->refElt_p;  //assuming same elements
  FEMapType femt_v=relt_v->mapType;
  bool firstOrder_v  = (domx->order() == 1);
  dimen_t der_v = 0;
  bool mapsh_v = (femt_v!=_standardMap || der_v>0);
  bool changeSignTest_v = (relt_v->dofCompatibility == _signDofCompatibility);   //dof sign change for particular element (Nedelec, Raviart, ....)
  bool changeSign_v= false;
  Vector<real_t>* sign_v=nullptr;
  bool nor_v=false;
  Vector<real_t>* nx= nullptr;
  std::set<Quadrature*>  quad_v;      //to store single quadrature pointers
  std::vector<Vector<K> > val_v;      //to store values of opvu on quadrature point
  std::vector<number_t> dofNum_v;

  //some global properties
  bool same_shv = (same_interpolation && nbc_v==nbc_u);
  bool sym = same_shv  &&  !(mat.sym == _noSymmetry);

  //get info for precomputation
  std::set<Quadrature*> quads;  //list of all quadratures required
  bool intphi=false, intpsi=false;
  for(cit_vbfp itf = subf.begin(); itf != subf.end(); itf++)
    {
      const DoubleIntgBilinearForm* ibf = itf->first->asDoubleIntgForm(); //basic bilinear form as double integral
      const TensorKernel* tk = ibf->opker().kernelp()->tensorKernel();    //tensor kernel
      const OperatorOnUnknown& op_u = ibf->opu();                         //operator on unknown
      const OperatorOnUnknown& op_v = ibf->opv();                         //operator on test function
      der_u = std::max(der_u, dimen_t(op_u.diffOrder()));                 //order of u differential involved in operators
      der_v = std::max(der_v, dimen_t(op_v.diffOrder()));                 //order of v differential involved in operators
      if(op_u.normalRequired()) nor_u=true;
      if(op_v.normalRequired())  nor_v=true;                              // normal vector required
      setDomainy(const_cast<GeomDomain*>(domy));                           //transmit domainy pointer to thread data
      setDomainx(const_cast<GeomDomain*>(domx));                           //transmit domainx pointer to thread data
      const IntegrationMethod* im = ibf->intgMethod();                    // get integration methods
      if(im->type() != _productIM) error("im_not_product");
      const ProductIM* pim = static_cast<const ProductIM*>(im);
      std::list<Quadrature*> quadl=pim->getxIM()->quadratures();
      quad_v.insert(quadl.begin(),quadl.end());
      quadl=pim->getyIM()->quadratures();
      quad_u.insert(quadl.begin(),quadl.end());
      if(!intphi && !tk->phiIsAnalytic())  //interpolated spectral basis phi, build gelt2elt map and kdtree if not built
      {
          intphi=true;
          const Space* spint = reinterpret_cast<const SpectralBasisInt*>(tk->phip())->basis().begin()->begin()->second->spacep();
          spint->buildgelt2elt();
          spint->domain()->meshDomain()->buildKdTree();
      }
      if(!intpsi && !tk->psiIsAnalytic())  //interpolated spectral basis psi, build gelt2elt map and kdtree if not built
      {
          intpsi=true;
          const Space* spint = reinterpret_cast<const SpectralBasisInt*>(tk->psip())->basis().begin()->begin()->second->spacep();
          spint->buildgelt2elt();
          spint->domain()->meshDomain()->buildKdTree();
      }
    }

  //precompute , geometry stuff on each element, shapevalues on ref element, no mapping of quadrature points
  bool useAux = false;
  preComputationIE(*domy, subsp_u, quad_u, nbc_u, false, der_u>0, der_u>1, nor_u);
  if(!same_shv || domx!=domy || quad_u!=quad_v || der_u!=der_v || nor_u!=nor_v || nbc_u!=nbc_v)
  {
     useAux = subsp_u->element_p(number_t(0))->refElt_p == subsp_v->element_p(number_t(0))->refElt_p;
     preComputationIE(*domx, subsp_v, quad_v, nbc_v, useAux, der_v>0, der_v>1, nor_v);
  }

  std::vector<Vector<K> > phi_w;   //to store sum_k[pk*phi_m(yk)*wj(yk)]
  std::vector<Vector<K> > psi_w;   //to store sum_k[pk*phi_m(yk)*wj(yk)]

  number_t nbelt_udiv10 = nbelt_u/10;
  bool show_status = (theVerboseLevel > 0 && mat.nbCols > 100 && nbelt_udiv10 > 1);

  number_t nbf=subf.size(), nf=0;
  Point x0;

  // main loop on basic bilinear forms
  //==================================
  for(cit_vbfp itf = subf.begin(); itf != subf.end(); itf++, nf++)
    {
      const DoubleIntgBilinearForm* ibf = itf->first->asDoubleIntgForm(); //basic bilinear form as double integral
      const OperatorOnUnknown& op_u = ibf->opu();                         //operator on unknown
      const OperatorOnUnknown& op_v = ibf->opv();                         //operator on test function
      bool isid_u=op_u.isId(), isid_v=op_v.isId();
      bool hasFun_u = op_u.hasFunction(), hasFun_v = op_v.hasFunction();
      const TensorKernel* tk = ibf->opker().kernelp()->tensorKernel();    //tensor kernel
      K coef = complexToT<K>(itf->second);                                //coefficient of linear form
      const IntegrationMethod* im = ibf->intgMethod();                    //integration method
      if(im->type() != _productIM) error("im_not_product");
      const ProductIM* pim = static_cast<const ProductIM*>(im);
      QuadratureIM* qimx = static_cast<QuadratureIM*>(pim->getxIM());
      QuadratureIM* qimy = static_cast<QuadratureIM*>(pim->getyIM());
      const Function* xmap = nullptr, *ymap = nullptr;
      if(!tk->xmap.isVoidFunction()) xmap = &(tk->xmap);                 //geometric map for x
      if(!tk->ymap.isVoidFunction()) ymap = &(tk->ymap);                 //geometric map for y
      bool analyticPhi = tk->phiIsAnalytic(), analyticPsi=tk->psiIsAnalytic();

      #ifdef XLIFEPP_WITH_OMP
        SpectralBasisFun sbf(Function(),0);
        if(analyticPsi) sbf= SpectralBasisFun(reinterpret_cast<const SpectralBasisFun&>(*tk->psip())) ;    //to be thread safe, make a copy of spectralBasis tk->psip()
      #endif

      // loop on finite elements of fesubspace subsp_u
      //------------------------------------------------------------------
      for(number_t ku = 0; ku <nbelt_u; ku++)
        {
          // data related to u
          const Element* elt_u = subsp_u->element_p(ku);
          RefElement*  relt_u = elt_u->refElt_p;                        //ref element
          GeomElement* gelt_u = elt_u->geomElt_p;                       //geom element
          MeshElement* melt_u = gelt_u->meshElement();                  //mesh element related to
          GeomMapData* mapdata_u = melt_u->geomMapData_p;               //geometrical data
          real_t difelt_u = mapdata_u->differentialElement;             //dif element
          if(changeSignTest_u) //update sign if required
          {
              sign_u=&elt_u->getDofSigns();
              changeSign_u = sign_u->size()!=0;
          }
          if(nor_u) ny = &melt_u->geomMapData_p->normalVector;          //update normal if required
          bool isSimplex_u = relt_u->isSimplex();                       //elt_u is a simplex

          //local to global numbering
          if(doflag_u) dofNum_u =subsp_u->elementDofs(ku); //dof numbers (local numbering) of element for u
          else dofNum_u = subsp_u->elementParentDofs(ku);  //dof numbers (parent numbering) of element for u
          number_t nb_u = dofNum_u.size();
          number_t nb_ut = nb_u * nbc_u;

          //compute opu  at quadrature points
          ShapeType sh_u =  gelt_u->shapeType();                         //shape type of element
          Quadrature* quady = qimy->getQuadrature(sh_u);
          number_t nbqy = quady->numberOfPoints();
          dimen_t dqy = quady->dim();
          std::vector<ShapeValues>* shv_u = &relt_u->qshvs_[quady];   //retry shape values at quadrature points
          std::vector<Point>& phypts_u = mapdata_u->phyPoints[quady]; //quadrature points in physical space
          val_u.resize(nbqy);
          dimen_t d,m;  // block size of val (d size of block, m number of vectors in block -> d/m size of each vector)
          computeOperatorByQuadrature(*elt_u, ordelt_u==0, op_u, isid_u, true, quady,  nbqy, dqy, shv_u, phypts_u,
                                      ordelt_u, dimf_u, nbc_u, firstOrder_u && isSimplex_u, der_u, nor_u, hasFun_u,
                                      false, mapsh_u, femt_u, changeSign_u, sign_u, difelt_u, mapdata_u, x0, ny, ny, val_u, d, m);      //x0 is not used !
          //compute spectral functions  at quadrature points
          phi_w.resize(nb_ut);
          if(analyticPhi)
            computeSPfunByQuadrature(val_u, *tk->phip(), phypts_u, *quady, phi_w, tk->isConjugate, ymap);                           //compute sum_k[pk*phi_m(yk)*wj(yk)] - analytic -
          else
            computeSPintByQuadrature(val_u, *tk->phip(), phypts_u, *quady, *elt_u, sp_u, domy, *shv_u, phi_w, tk->isConjugate, ymap);//compute sum_k[pk*phi_m(yk)*wj(yk)] - interpolation -

          //thePrintStream<<std::setw(8) << std::setprecision(6)<<"val_u="<<val_u<<eol<<"phi_w="<<phi_w<<eol;

          //loop on finite elements of fesubspace subsp_v
          //-----------------------------------------------------------------
          #ifdef XLIFEPP_WITH_OMP
          #pragma omp parallel firstprivate(sbf, dofNum_v,  val_v, psi_w, nx, changeSign_v, sign_v, x0) shared(mat)
          #pragma omp for schedule(dynamic) nowait
          #endif
          for(number_t kv = 0; kv < nbelt_v; kv++)
            {
              // data related to u
              const Element* elt_v = subsp_v->element_p(kv);
              RefElement*  relt_v = elt_v->refElt_p;                      //ref element
              GeomElement* gelt_v = elt_v->geomElt_p;                     //geom element
              MeshElement* melt_v = gelt_v->meshElement();                //mesh element related to
              GeomMapData* mapdata_v = melt_v->geomMapData_p;             //geometrical data
              real_t difelt_v = mapdata_v->differentialElement;           //dif element
              if(changeSignTest_v)                                        //update sign if required
              {
                  sign_v=&elt_v->getDofSigns();
                  changeSign_v = sign_v->size()!=0;

              }
              if(nor_v) nx = &melt_v->geomMapData_p->normalVector;        //update normal if required
              bool isSimplex_v = relt_v->isSimplex();                     //elt_v is a simplex

              //local to global numbering
              if(doflag_v) dofNum_v = subsp_v->elementDofs(kv); //dof numbers (local numbering) of element for v
              else dofNum_v = subsp_v->elementParentDofs(kv);   //dof numbers (parent numbering) of element for v
              number_t nb_v = dofNum_v.size();
              number_t nb_vt = nb_v * nbc_v;

              //compute opu  at quadrature points
              ShapeType sh_v =  gelt_v->shapeType();                       //shape type of element
              Quadrature* quadx = qimx->getQuadrature(sh_v);
              number_t nbqx = quadx->numberOfPoints();
              dimen_t dqx = quadx->dim();
              std::vector<ShapeValues>* shv_v;
              if(useAux) shv_v= &relt_v->qshvs_aux[quadx];  //retry auxiliary shape values at quadrature points
              else  shv_v = &relt_v->qshvs_[quadx];         //retry shape values at quadrature points
              std::vector<Point>& phypts_v = mapdata_v->phyPoints[quadx];  //quadrature points in physical space
              val_v.resize(nbqx);
              dimen_t d,m;  // block size of val (d size of block, m number of vectors in block -> d/m size of each vector)
              computeOperatorByQuadrature(*elt_v, ordelt_v==0, op_v, isid_v, true, quadx,  nbqx, dqx,  shv_v, phypts_v,
                                          ordelt_v, dimf_v, nbc_v, firstOrder_v && isSimplex_v, der_v, nor_v,  hasFun_v,
                                          false, mapsh_v, femt_v, changeSign_v, sign_v, difelt_v, mapdata_v, x0, nx, nx, val_v, d, m);  //x0 is not used !

              //compute spectral functions  at quadrature points
              psi_w.resize(nb_vt);      //to store sum_k[pk*psi_n(xk)*wj(xk)]
              if(analyticPsi)           //compute sum_k[pk*psi_n(xk)*wj(xk)] - analytic -
                #ifdef XLIFEPP_WITH_OMP
                  computeSPfunByQuadrature(val_v, sbf, phypts_v, *quadx, psi_w, false, xmap);
                #else
                  computeSPfunByQuadrature(val_v, *tk->psip(), phypts_v, *quadx, psi_w, false, xmap);
                #endif
              else                      //compute sum_k[pk*psi_n(xk)*wj(xk)] - interpolation -
                computeSPintByQuadrature(val_v, *tk->psip(), phypts_v, *quadx, *elt_v, sp_v, domx, *shv_v, psi_w, false, xmap);
              //thePrintStream<<"val_v="<<val_v<<eol<<"psi_w="<<psi_w<<eol;

              //compute matel
              Matrix<K> matel(dimen_t(nb_vt), dimen_t(nb_ut), K(0));                     //define elementary matrix structure
              typename Vector<K>::iterator itm = matel.begin();
              typename std::vector<Vector<K> >::iterator itu, itv;
              typename Vector<K>::iterator itun, itvm;
              number_t nbphi=tk->nbOfPhi(), nbpsi = tk->nbOfPsi();
              if(tk->isDiag)  //diagonal matrix in TensorKernel
                {
                  for(itv = psi_w.begin(); itv != psi_w.end(); itv++)
                    for(itu = phi_w.begin(); itu != phi_w.end(); itu++, itm++)
                      {
                        itun = itu->begin();   itvm = itv->begin();
                        for(number_t m = 0; m < nbpsi; m++, itun++, itvm++)
                          {
                            K t =  *itun** itvm;
                            if(t != K(0)) *itm += coef * tk->kernelProduct(m,t);
                          }
                      }
                }
              else  //non diagonal matrix in TensorKernel
                {
                  for(itv = psi_w.begin(); itv != psi_w.end(); itv++)
                    for(itu = phi_w.begin(); itu != phi_w.end(); itu++, itm++)
                      {
                        itvm = itv->begin();
                        for(number_t m = 0; m < nbpsi; m++, itvm++)
                          if(*itvm!=K(0))
                            {
                              itun = itu->begin();
                              for(number_t n = 0; n < nbphi; n++, itun++)
                                {
                                  K t=  *itun** itvm;
                                  if(t!=K(0)) *itm += coef * tk->kernelProduct(m, n, t);
                                }
                            }
                      }
                }

//         thePrintStream<<"dofnum V="<<dofNum_v<<"dofnum U="<<dofNum_u<<" nb_u="<<nb_u<<" nb_v="<<nb_v<<" nbc_u="<<nbc_u<<" nbc_v="<<nbc_v<<eol;
//         thePrintStream<<"matel="<<matel<<eol;

              //assembling matel in global matrix
              std::vector<number_t> adrs(nb_u * nb_v, 0);
              mat.positions(dofNum_v, dofNum_u, adrs, true); //adresses in mat of elementary matrix stored as a row vector
              std::vector<number_t>::iterator itadrs = adrs.begin(), itk;
              std::vector<number_t>::iterator itd_u, itd_v, it_ue;   //dof numbering iterators
              typename Matrix<K>::iterator itmatel = matel.begin(), itm_u;
              number_t incr = nbc_u * nbc_v * nb_u; //block row increment in matel
              number_t i = 0;
              //loop on (vector) dofs in u and v
              for(itd_v = dofNum_v.begin(); itd_v != dofNum_v.end() ; itd_v++, itmatel += incr, i++)
                {
                  itm_u = itmatel;
                  itk = itadrs + i * nb_u;
                  for(itd_u = dofNum_u.begin(); itd_u != dofNum_u.end() ; itd_u++, itm_u += nbc_u, itk++)
                    {
                      if(!sym || *itd_v >= *itd_u) assemblyMat(mat(*itk), itm_u, nb_ut);
                    }
                }
              //delete dofNum_v;
            }//end loop elt_v

          if(show_status)  //progress status
            if(ku!=0 && ku % nbelt_udiv10 == 0) { std::cout<< number_t((100.*nf)/nbf+(10.*ku)/(nbelt_u*nbf)) <<"0% "<<std::flush; }

        }//end loop elt_u
    } //end loop bilinear form

  if(show_status) std::cout<<" done"<<eol<<std::flush;
  trace_p->pop();
}

} // end of namespace xlifepp
