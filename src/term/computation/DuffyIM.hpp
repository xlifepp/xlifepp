/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DuffyIM.hpp
  \authors T.Khaled and N.Salles, E. Lunéville.
  \since 25 aug 2016
  \date 25 aug 2016

  \brief Implementation of a method based on the Duffy transform to compute the FE TermMatrix
             intg_Eu intg Ev opu(y) opk(x,y) opv(x) dy dx
   with Eu and Ev 2 segments.
 */

#ifndef DUFFY_IM_HPP
#define DUFFY_IM_HPP

#include "finiteElements.h"
#include "utils.h"
#include "config.h"
#include "termUtils.hpp"

#include <stack>

namespace xlifepp{


/*!
 \class DuffyIM
 integral over a product of 2D geometric elements with singularity using
  - for adjacent elements : a method based on Duffy transform
  - for self influence : an hybrid method based on t^p transform
 may manage different quadrature rules
 Elements must be segment!
 Note : does not address the case of separate elements
*/
class DuffyIM : public DoubleIM
{
  public:
    Quadrature* quadSelfX, *quadSelfY;  //!< quadratures on segment [0,1] for self influence
    Quadrature* quadAdjtX, *quadAdjtY;  //!< quadratures on segment [0,1] for adjacent elements
    number_t ordSelfX, ordSelfY;        //!< order of quadratures on the segment for self influence
    number_t ordAdjtX, ordAdjtY;        //!< order of quadratures on the segment for adjacent elements
    number_t selfOrd;                   //!< order of t^p transform or number of diminishing layers (def=5)
    bool useSelfTrans;                  //!< use improved method for self influence (def=true)

    DuffyIM(IntegrationMethodType imt)  //! basic constructor
      : DoubleIM(imt), quadSelfX(nullptr), quadAdjtX(nullptr), quadSelfY(nullptr), quadAdjtY(nullptr), ordSelfX(0), ordAdjtX(0), ordSelfY(0), ordAdjtY(0), selfOrd(5), useSelfTrans(true) { name="Duffy_?"; }

    DuffyIM(number_t oX=6, number_t oY=6)              //! full constructor from quadrature order
    {
      ordSelfX=ordSelfY=oX;
      ordAdjtX=ordAdjtY=oY;
      quadSelfX=findQuadrature(_segment,_GaussLegendreRule,ordSelfX);
      quadAdjtX= quadSelfX;
      if (oY!=oX) quadSelfY=findQuadrature(_segment,_GaussLegendreRule,ordSelfY);
      else quadSelfY=quadSelfX;
      quadAdjtY= quadSelfY;
      name="Duffy_LegendreRule "+tostring(ordSelfX);
      if (oY!=oX) name+="x"+tostring(ordSelfY);
      imType=_DuffyIM; singularType=_logr; singularOrder=1;
      selfOrd=5;
      useSelfTrans=true;
    }

    DuffyIM(QuadRule qsX, number_t osX, QuadRule qsY, number_t osY,
            QuadRule qaX, number_t oaX, QuadRule qaY, number_t oaY,
            number_t so=5, bool us=true)                                //! full constructor from quadrules
    {
      quadSelfX=findQuadrature(_segment,qsX,osX);
      if (qsY==qsX && osX==osY) quadSelfY=quadSelfX;
      else quadSelfY=findQuadrature(_segment,qsY,osY);
      if (qaX==qsX && oaX==osX) quadAdjtX=quadSelfX;
      else
      {
        if (qaX==qsY && oaX==osY) quadAdjtX=quadSelfY;
        else quadAdjtX=findQuadrature(_segment,qaX,oaX);
      }
      if (qaY==qsX && oaY==osX) quadAdjtY=quadSelfX;
      else
      {
        if (qaY==qsY && oaY==osY) quadAdjtY=quadSelfY;
        else
        {
          if (qaY==qaX && oaY==oaX) quadAdjtY=quadAdjtX;
          else quadAdjtY=findQuadrature(_segment,qaY,oaY);
        }
      }
      ordSelfX=osX;ordAdjtX=oaX;
      ordSelfY=osY;ordAdjtY=oaY;
      name="Duffy_" + words("quadrule",qsX) + tostring(osX) + "_"
                    + words("quadrule",qsY) + tostring(osY) + "_"
                    + words("quadrule",qaX) + tostring(oaX) + "_"
                    + words("quadrule",qaY) + tostring(oaY);
      imType=_DuffyIM; singularType=_logr; singularOrder=1;
      useSelfTrans=true;
      selfOrd=so;
      if (useSelfTrans && selfOrd==0) selfOrd=1; // p>0 in t^p transform
    }

    DuffyIM(const Quadrature& q)       //! full constructor from a quadrature object
    {
      if (q.shapeType()!=_segment)
      {
        where("DuffyIM::DuffyIM(Quadrature)");
        error("shape_not_handled", words("shape", q.shapeType()));
      }
      quadSelfX = const_cast<Quadrature*>(&q); quadAdjtX = quadSelfX;
      quadSelfY = quadSelfX; quadAdjtY = quadSelfY;
      ordSelfX = q.degree; ordAdjtX = ordSelfX;
      ordSelfY = ordSelfX; ordAdjtX = ordAdjtY;
      name="Duffy_"+q.name+tostring(ordSelfX);
      imType=_DuffyIM; singularType=_logr; singularOrder=1;
      selfOrd=0;
      useSelfTrans=true;
    }

    virtual DuffyIM* clone() const // covariant
    {return new DuffyIM(*this);}

    virtual std::list<Quadrature*> quadratures() const            //! return the list of (single) quadratures in a list
    {
      std::list<Quadrature *> lsq;
      if (quadSelfX!=nullptr) lsq.push_back(quadSelfX);
      if (quadAdjtX!=nullptr && quadAdjtX!=quadSelfX)  lsq.push_back(quadAdjtX);
      return lsq;
    }

    virtual void print(std::ostream& os) const    //! print IntegrationMethod on stream
    {
      os << "Duffy integration method " << name;
      if (useSelfTrans) { os << " using t^p transformation with p=" << selfOrd; }
      else { os << " using " << selfOrd << " progressive layers"; }
      if (quadSelfX != nullptr || quadSelfY != nullptr || quadAdjtX != nullptr || quadAdjtY != nullptr) { os<< " and quadratures:"<<eol; }
      if (quadSelfX != nullptr)
      {
        os << "   quad self X:" << quadSelfX->name << " degree "
           << quadSelfX->degree << " ( " << quadSelfX->quadratureRule.size() << " points )" << eol;
      }
      if (quadSelfY != nullptr)
      {
        os << ", quad self Y:" << quadSelfY->name << " degree "
           << quadSelfY->degree << " ( " << quadSelfY->quadratureRule.size() << " points )" << eol;
      }
      if (quadAdjtX != nullptr)
      {
        os << "   quad Adjt X:" << quadAdjtX->name << " degree "
           << quadAdjtX->degree << " ( " << quadAdjtX->quadratureRule.size() << " points )" << eol;
      }
      if (quadAdjtY != nullptr)
      {
        os << ", quad Adjt Y:" << quadAdjtY->name << " degree "
           << quadAdjtY->degree << " ( " << quadAdjtY->quadratureRule.size() << " points )" << eol;
      }
    }
    virtual void print(PrintStream& os) const {print(os.currentStream());}

    //template computeIE function, explicit instanciation because template functions are not inherited
    template<typename K>
    void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                   const KernelOperatorOnUnknowns& kuv, Matrix<K>& res,IEcomputationParameters& ieparams,
                   Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk) const;
    void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                   const KernelOperatorOnUnknowns& kuv, Matrix<real_t>& res,IEcomputationParameters& ieparams,
                   Vector<real_t>& val_opu, Vector<real_t>& val_opv, Vector<real_t>& val_opk) const
    {computeIE<>(elt_S, elt_T,adj, kuv, res,ieparams, val_opu, val_opv, val_opk);}
    void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                   const KernelOperatorOnUnknowns& kuv, Matrix<complex_t>& res,IEcomputationParameters& ieparams,
                   Vector<complex_t>& val_opu, Vector<complex_t>& val_opv, Vector<complex_t>& val_opk) const
    {computeIE<>(elt_S, elt_T,adj, kuv, res,ieparams, val_opu, val_opv, val_opk);}

    template<typename K>
    void SelfInfluences(const Element* elt_S, const Element* elt_T, const KernelOperatorOnUnknowns& kuv,
                        const Vector<real_t>* nx, const Vector<real_t>* ny, Matrix<K>& res,
                        IEcomputationParameters& ieparams) const;
    template<typename K>
    void SelfInfluencesAdaptive(const Element* elt_S, const Element* elt_T, const KernelOperatorOnUnknowns& kuv,
                        const Vector<real_t>* nx, const Vector<real_t>* ny,
                        Matrix<K>& res, IEcomputationParameters& ieparams) const;
    template<typename K>
    void SelfInfluencesTP(const Element* elt_S, const Element* elt_T, const KernelOperatorOnUnknowns& kuv,
                        const Vector<real_t>* nx, const Vector<real_t>* ny,
                        Matrix<K>& res, IEcomputationParameters& ieparams) const;
    template<typename K>
    void AdjacentSegments(const Element* elt_S, const Element* elt_T, const KernelOperatorOnUnknowns& kuv,
                          const Vector<real_t>* nx, const Vector<real_t>* ny, const Vector<number_t>& indi,
                          const Vector<number_t>& indj, Matrix<K>& res, IEcomputationParameters& ieparams) const;
    template<typename K>
    void k4(real_t z1,real_t z2, const KernelOperatorOnUnknowns& kuv, const Point& S1, const Point & M,
            const Element* elt_S,const Element* elt_T, Matrix<K>& res, const Vector<real_t>* nx, const Vector<real_t>* ny,
            IEcomputationParameters& ieparams) const;
    template<typename K>
    void k4TP(real_t z1, real_t z2, const KernelOperatorOnUnknowns& kuv, const Point& S1, const Point & M,
                 const Element* elt_S, const Element* elt_T, Matrix<K>& res, const Vector<real_t>* nx, const Vector<real_t>* ny,
                 IEcomputationParameters& ieparams) const;
    template<typename K>
    void k5(real_t z1,real_t z2, const KernelOperatorOnUnknowns& kuv, const Point& S1, const Point & MS,
            const Point & T1, const Point & MT, const Element* elt_S,const Element* elt_T, Matrix<K>& res,
            const Vector<real_t>* nx, const Vector<real_t>* ny, IEcomputationParameters& ieparams) const;

};

template<typename K>
void DuffyIM::computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                        const KernelOperatorOnUnknowns& kuv, Matrix<K>& res,IEcomputationParameters& ieparams,
                        Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk) const
{
  //load normals
  Vector<real_t>* nx=nullptr, *ny=nullptr;
  MeshElement* melt_S=ieparams.melt_u, *melt_T=ieparams.melt_v;
//  thePrintStream<<"compute on melt_S : "<<melt_S->vertex(1)<<" "<<melt_S->vertex(2)
//                          <<" melt_T : "<<melt_T->vertex(1)<<" "<<melt_T->vertex(2)<<eol<<std::flush;
  if (kuv.xnormalRequired()) nx = &melt_T->geomMapData_p->normalVector;
  if (kuv.ynormalRequired()) ny = &melt_S->geomMapData_p->normalVector;

  if (adj.status==_adjacentByElement)  //same geometrical triangle
  {
    if (useSelfTrans) SelfInfluencesTP(elt_S, elt_T, kuv, nx, ny, res, ieparams);
    else              SelfInfluences(elt_S, elt_T, kuv, nx, ny, res, ieparams);
    //thePrintStream<<"self influence : res = "<<eol<<res<<eol<<std::flush;
    return;
}
  //ajacent by vertex
  std::vector<number_t>& indi=adj.sharedVertex1, &indj=adj.sharedVertex1;
  if (indi[0]==1) indi[1]=2; else indi[1]=1;
  if (indj[0]==1) indj[1]=2; else indj[1]=1;
  AdjacentSegments(elt_S, elt_T, kuv, nx, ny, indi, indj, res, ieparams);
//  thePrintStream<<"adjacent : res = "<<eol<<res<<eol<<std::flush;
}

// because integral is quasi singular on y=0,
// compute self influences using few layers along y [0,(1/2)^n] [(1/2)^n, (1/2)^n-1], ...[1/2,1]
// on the smaller layer [0,(1/2)^n] use quadSelfY, on the other layers use quadSelfX
// typically parameters : selfOrd=5 layers, quadSelfX of order 30, quadSelfY of order 120
template<typename K>
void DuffyIM::SelfInfluences(const Element* elt_S, const Element* elt_T, const KernelOperatorOnUnknowns& kuv,
                             const Vector<real_t>* nx, const Vector<real_t>* ny,
                             Matrix<K>& res, IEcomputationParameters& ieparams) const
{
  MeshElement *meltu=ieparams.melt_u, *meltv=ieparams.melt_v;
  GeomMapData *gmapu=meltu->geomMapData_p, *gmapv=meltv->geomMapData_p;
  bool firstOrderEltu = meltu->order()==1, firstOrderEltv = meltv->order()==1;
  Point& S1=*meltu->nodes[0], & S2= *meltu->nodes[1];
  //definition of matrix MS
  Point  M=S2-S1;
  real_t G1=gmapu->differentialElement, G2=gmapv->differentialElement;
  //quadrature
  number_t Nx=quadSelfX->numberOfPoints(), Ny=quadSelfY->numberOfPoints();
  real_t a, b;
  const std::vector< real_t >& coordsX=quadSelfX->coords();
  const std::vector< real_t >& weightsX=quadSelfX->weights();
  const std::vector< real_t >& coordsY=quadSelfY->coords();
  const std::vector< real_t >& weightsY=quadSelfY->weights();
  const std::vector< real_t >* cYp=& coordsY, *wYp=& weightsY;
  Matrix<K> matel=0.*res;
  real_t y1=0, y2=std::pow(0.5,selfOrd), ly;
  for (number_t d=0; d<=selfOrd; d++)
  {
    ly=y2-y1;
    if (d>0) {cYp=&coordsX; wYp=&weightsX;Ny=quadSelfX->numberOfPoints();}
    for (number_t i1=0; i1<Nx; i1++)
    {
      a=coordsX[i1];
      real_t w1=weightsX[i1];
      for (number_t i2=0; i2<Ny; i2++)
      {
        b=y1+ly*(*cYp)[i2];
        matel*=0.;
        k4(a,b,kuv,S1,M,elt_S,elt_T,matel,nx,ny,ieparams);
        res+=(ly*w1*(*wYp)[i2])*matel;
      }
    }
    y1=y2;y2*=2;
  }
  if (firstOrderEltu && !ieparams.isoGeo) res*=G1;
  if (firstOrderEltv && !ieparams.isoGeo) res*=G2;
}

class Sq
{
  public:
    real_t a, b, c, d;  //rectangle [a,b]x[c,d]
    number_t selfOrd;
    Sq(real_t ai=0, real_t bi=1, real_t ci=0, real_t di=1, number_t dep=0) : a(ai), b(bi), c(ci), d(di), selfOrd(dep) {}
};

// compute self influences using adaptive quadrature (experimental)
template<typename K>
void DuffyIM::SelfInfluencesAdaptive(const Element* elt_S, const Element* elt_T, const KernelOperatorOnUnknowns& kuv,
                                const Vector<real_t>* nx, const Vector<real_t>* ny,
                                Matrix<K>& res, IEcomputationParameters& ieparams) const
{
  MeshElement *meltu=ieparams.melt_u, *meltv=ieparams.melt_v;
  GeomMapData *gmapu=meltu->geomMapData_p, *gmapv=meltv->geomMapData_p;
  bool firstOrderEltu = meltu->order()==1, firstOrderEltv = meltv->order()==1;
  Point& S1=*meltu->nodes[0], & S2= *meltu->nodes[1];
  //definition of matrix MS
  Point  M=S2-S1;
  real_t G1=gmapu->differentialElement, G2=gmapv->differentialElement;
  // thePrintStream<<" G1="<<G1<<" G2="<<G2<<" eltdif="<<G1*G2<<eol;
  //quadrature
  number_t Nx=quadSelfX->numberOfPoints(), Ny=quadSelfY->numberOfPoints();
  real_t a, b, l1, l2, m1, m2, s1, s2, s3, s4;
  real_t tol=1.E-6;
  const std::vector< real_t >& coordsX=quadSelfX->coords();
  const std::vector< real_t >& weightsX=quadSelfX->weights();
  const std::vector< real_t >& coordsY=quadSelfY->coords();
  const std::vector< real_t >& weightsY=quadSelfY->weights();
  Matrix<K> matel=0.*res;
  Matrix<K> res1=0.*res, res2=0.*res;
  std::stack<Sq> sqs;
  sqs.push(Sq(0,1,0,1,0));
  Sq sq;

  while (!sqs.empty())
  {
    sq=sqs.top(); sqs.pop();
    res1*=0;
    l1=(sq.b-sq.a); l2=(sq.d-sq.c);
    for (number_t i1=0; i1<Nx; i1++)
    {
      a=sq.a+l1*coordsX[i1];
      real_t w1=l1*weightsX[i1];
      for (number_t i2=0; i2<Ny; i2++)
      {
        b=sq.c+l2*coordsY[i2];
        matel*=0.;
        k4(a,b,kuv,S1,M,elt_S,elt_T,matel,nx,ny,ieparams);
        res1+=(l2*w1*weightsY[i2])*matel;
      }
    }
    //compute split square
    res2*=0;
    for (number_t i=0;i<4;i++)
    {
      switch (i)
      {
        case 0: s1=sq.a;s2=s1+l1*0.5; s3=sq.c; s4=s3+l2*0.5; break;
        case 1: s1=sq.a+l1*0.5;s2=sq.b; s3=sq.c; s4=s3+l2*0.5; break;
        case 2: s1=sq.a;s2=s1+l1*0.5; s3=sq.c+0.5*l2; s4=sq.d; break;
        case 3: s1=sq.a+l1*0.5;s2=sq.b; s3=sq.c+0.5*l2; s4=sq.d; break;
      }
      m1=(s2-s1); m2=(s4-s3);
      for (number_t i1=0; i1<Nx; i1++)
      {
        a=s1+m1*coordsX[i1];
        real_t w1=m1*weightsX[i1];
        for (number_t i2=0; i2<Ny; i2++)
        {
          b=s3+m2*coordsY[i2];
          matel*=0.;
          k4(a,b,kuv,S1,M,elt_S,elt_T,matel,nx,ny,ieparams);
          res2+=(m2*w1*weightsY[i2])*matel;
        }
      }
    }
    //compare res1 and res2
    real_t err=norm(res1-res2)/norm(res2);
    if ( err < tol || sq.selfOrd>selfOrd) res+=res2;
    else
    {
      number_t dep=sq.selfOrd+1;
      sqs.push(Sq(sq.a,sq.a+l1*0.5,sq.c,sq.c+l2*0.5,dep));
      sqs.push(Sq(sq.a+l1*0.5,sq.b,sq.c,sq.c+l2*0.5,dep));
      sqs.push(Sq(sq.a,sq.a+l1*0.5,sq.c+0.5*l2,sq.d,dep));
      sqs.push(Sq(sq.a+l1*0.5,sq.b,sq.c+0.5*l2,sq.d,dep));
    }
  }
  if (firstOrderEltu && !ieparams.isoGeo) res*=G1;
  if (firstOrderEltv && !ieparams.isoGeo) res*=G2;
}

// Self influence computation using t^p transformations in diagonal directions
template<typename K>
void DuffyIM::SelfInfluencesTP(const Element* elt_S, const Element* elt_T, const KernelOperatorOnUnknowns& kuv,
                               const Vector<real_t>* nx, const Vector<real_t>* ny,
                               Matrix<K>& res, IEcomputationParameters& ieparams) const
{
  MeshElement *meltu=ieparams.melt_u, *meltv=ieparams.melt_v;
  GeomMapData *gmapu=meltu->geomMapData_p, *gmapv=meltv->geomMapData_p;
  bool firstOrderEltu = meltu->order()==1, firstOrderEltv = meltv->order()==1;
  Point& S1=*meltu->nodes[0], & S2= *meltu->nodes[1];
  //definition of matrix MS
  Point  M=S2-S1;
  real_t G1=gmapu->differentialElement, G2=gmapv->differentialElement;
  //quadrature
  number_t N1=quadSelfX->numberOfPoints(), N2=quadSelfY->numberOfPoints();
  real_t a, b, tp, ap, bp, s, w, w2;
  number_t p = selfOrd;
  const std::vector< real_t >& coords1=quadSelfX->coords();
  const std::vector< real_t >& weights1=quadSelfX->weights();
  const std::vector< real_t >& coords2=quadSelfY->coords();
  const std::vector< real_t >& weights2=quadSelfY->weights();
  Matrix<K> matel=0.*res;

  for (number_t i2=0; i2<N2; i2++)
  {
    b=2*coords2[i2]-1;  // move quad point to [-1,1]
    tp=1;
    for (int_t k=0;k<p-1;k++) tp*=b;
    s=tp*b;
    w2=weights2[i2]*p*tp;
    for (number_t i1=0; i1<N1; i1++)
    {
      a=coords1[i1];
      w=w2*weights1[i1];
      ap=0.5*(1-s)*a;
      bp=0.5*(1+s)*a;
      matel*=0.;
      k4TP(ap,bp,kuv,S1,M,elt_S,elt_T,matel,nx,ny,ieparams);
//        thePrintStream<<"i1="<<i1<<" i2="<<i2<<" a="<<a<<" b="<<b<<" ap="<<ap<<" bp="<<bp<<" matel="<<eol<<matel<<eol;;
      res+=(a*w)*matel;
      matel*=0.;
      ap=0.5*(1+a+(1-a)*s);
      bp=0.5*(1+a-(1-a)*s);
      k4TP(ap,bp,kuv,S1,M,elt_S,elt_T,matel,nx,ny,ieparams);
//        thePrintStream<<" ap2="<<ap<<" bp2="<<bp<<" matel2="<<eol<<matel<<eol;
      res+=((1-a)*w)*matel;
    }
  }

  if (firstOrderEltu && !ieparams.isoGeo) res*=G1;
  if (firstOrderEltv && !ieparams.isoGeo) res*=G2;
}

// Ajacent influence computation using Duffy transformations
template<typename K>
void DuffyIM::AdjacentSegments(const Element* elt_S, const Element* elt_T, const KernelOperatorOnUnknowns& kuv,
                               const Vector<real_t>* nx, const Vector<real_t>* ny, const Vector< number_t >& indi, const Vector< number_t >& indj,
                               Matrix<K>& res, IEcomputationParameters& ieparams) const
{
  MeshElement *meltu=ieparams.melt_u, *meltv=ieparams.melt_v;
  GeomMapData* gmapu=meltu->geomMapData_p, *gmapv=meltv->geomMapData_p;
  bool firstOrderEltu = meltu->order()==1, firstOrderEltv = meltv->order()==1;
  Point& S1=*meltu->nodes[indi[0]-1], & S2= *meltu->nodes[indi[1]-1];
  Point& T1=*meltv->nodes[indj[0]-1], & T2= *meltv->nodes[indj[1]-1];

  //definition of matrices MS, MT
  Point MS=S2-S1;
  Point MT=T2-T1;
  real_t G1=gmapu->differentialElement, G2=gmapv->differentialElement;
  //quadrature
  number_t Nx=quadAdjtX->numberOfPoints(), Ny=quadAdjtY->numberOfPoints();
  real_t a,b;
  const std::vector< real_t >& coordsX=quadAdjtX->coords();
  const std::vector< real_t >& weightsX=quadAdjtX->weights();
  const std::vector< real_t >& coordsY=quadAdjtY->coords();
  const std::vector< real_t >& weightsY=quadAdjtY->weights();
  Matrix<K> matel(res.numberOfRows(),res.numberOfColumns(),K(0));
  for (number_t i1=0; i1<Nx; i1++)
  {
    real_t w1=weightsX[i1];
    a=coordsX[i1];
    for (number_t i2=0; i2<Ny; i2++)
    {
      b=coordsY[i2];
      matel*=0.;
      k5(a,b,kuv,S1,MS,T1,MT,elt_S,elt_T,matel,nx,ny,ieparams);
      res+=(w1*weightsY[i2])*matel;
    } // end i2
  } // end i1
  if (firstOrderEltu && !ieparams.isoGeo) res*=G1;
  if (firstOrderEltv && !ieparams.isoGeo) res*=G2;
  return;
}

template<typename K>
void DuffyIM::k4(real_t z1, real_t z2, const KernelOperatorOnUnknowns& kuv, const Point& S1, const Point & M,
                 const Element* elt_S, const Element* elt_T, Matrix<K>& res, const Vector<real_t>* nx, const Vector<real_t>* ny,
                 IEcomputationParameters& ieparams) const
{
  MeshElement *melt=ieparams.melt_u;
  bool iso=ieparams.isoGeo;
  bool firstOrderElt = melt->order()==1, linearMap = firstOrderElt && !iso ;
  GeomMapData gmap;  //used when curved element
  Vector<real_t> nxt, nyt;
  const Vector<real_t>* ny2=ny;
  Point Xp=S1+z1*M; //Ca(X);
  Point Y1=S1+(z1-z1*z2)*M; // Ca(a(1-b))
  Point Y2=S1+(z1+(1.-z1)*z2)*M; // Ca(a+(a-1)b)
  Point x, y1, y2;

  // thePrintStream<<" S1="<<S1<<" M="<<M<<" z1="<<z1<<" z2="<<z2<<" Xp="<<Xp<<" Y1="<<Y1<<" Y2="<<Y2<<eol<<std::flush;

  real_t eltdif1 = 1., eltdif2=1.;
  if (!linearMap)  //update difelt and normal (done outside if linear map)
  {
    GeomMapData *mapP1=melt->meltP1->geomMapData_p;
    x=mapP1->geomMapInverse(Xp);
    gmap=GeomMapData(melt,x,true,true,false); //hard copy to avoid data race
    if(iso)
    {
       gmap.isoPar_p = ieparams.isoPar_x;
       gmap.useParametrization = true;
       gmap.useIsoNodes = true;
    }
    Xp=gmap.geomMap(x);
    gmap.computeDifferentialElement();   //update dif element
    eltdif1 *= gmap.differentialElement;
    eltdif2 = eltdif1;
    if (nx!=nullptr)
    {
      gmap.computeOrientedNormal();     //compute oriented y-normal
      nxt=gmap.normalVector;            //update normal vector
      nx=&nxt;
    }
    y1=mapP1->geomMapInverse(Y1);
    Y1=gmap.geomMap(y1);
    gmap.computeJacobianMatrix(y1);       //compute jacobian at Y1
    gmap.computeDifferentialElement();   //update dif element
    eltdif1 *= gmap.differentialElement;
    if (ny!=nullptr)
    {
      gmap.computeOrientedNormal();     //compute oriented y-normal
      nyt=gmap.normalVector;            //update normal vector
      ny=&nyt;
    }
    y2=mapP1->geomMapInverse(Y2);
    Y2=gmap.geomMap(y2);
    gmap.computeJacobianMatrix(y2);       //compute jacobian at Y2
    gmap.computeDifferentialElement();   //update dif element
    eltdif2 *= gmap.differentialElement;
    if (ny!=nullptr)
    {
      gmap.computeOrientedNormal();     //compute oriented y-normal
      ny2=&gmap.normalVector;            //update normal vector
    }
  }

  if (ieparams.isP0 && ieparams.dimf_u==1 &&  ieparams.dimf_v==1)    //P0 shortcut
  {
    if (ieparams.isId)
    {
      K tmp;
      kuv.opker().eval(Xp, Y1, tmp, nx, ny);
      res[0]+=eltdif1*z1*tmp;
      tmp*=0.;
      kuv.opker().eval(Xp, Y2, tmp, nx, ny2);
      res[0]+=eltdif2*(1-z1)*tmp;
    }
    else
    {
      ShapeValues shv;
      shv.w=std::vector<real_t>(1,1.);
      Matrix<K> tmp=0.*res;
      kuv.eval(Xp, Y1, shv, shv, K(1.), tmp, nx, ny);
      res+=(eltdif1*z1)*tmp;
      tmp*=0.;
      kuv.eval(Xp, Y2, shv, shv, K(1.), tmp, nx, ny2);
      res+=(eltdif2*(1.-z1))*tmp;
    }
    return;
  }

  // update reference points if linear map
  if (linearMap)
  {
    x=melt->geomMapData_p->geomMapInverse(Xp);  // point X in reference element
    y1=melt->geomMapData_p->geomMapInverse(Y1); // point Y1 in reference element
    y2=melt->geomMapData_p->geomMapInverse(Y2); // point Y2 in reference element
  }

  //thePrintStream<<" xp="<<x<<" y1="<<y1<<" y2="<<y2<<" Xp="<<Xp<<" Y1="<<Y1<<" Y2="<<Y2<<" eltdif1="<<eltdif1<<" eltdif2="<<eltdif2<<eol<<std::flush;

  //evaluate shape values
  bool refPoint = true;
  bool deru=ieparams.difforder_u>0, derv=ieparams.difforder_v>0;   //order of operator on unknowns >0
  ShapeValues shv_Tvx =elt_T->computeShapeValues(x,refPoint,derv,false);
  ShapeValues shv_Suy1=elt_S->computeShapeValues(y1,refPoint,deru,false);
  ShapeValues shv_Suy2=elt_S->computeShapeValues(y2,refPoint,deru,false);
  //evaluate matrix
  Vector<K> val_opu, val_opv, val_opk;
  kuv.evalF(Xp, Y1, shv_Suy1, shv_Tvx, K(eltdif1*z1), nx, ny,
            ieparams.dimfp_u, ieparams.dimfp_v, ieparams.opisid_u, ieparams.opisid_v,
            ieparams.hasf_u, ieparams.hasf_v, ieparams.scalar_k, 0, val_opu, val_opv, val_opk, res);
  kuv.evalF(Xp, Y2, shv_Suy2, shv_Tvx, K(eltdif2*(1.-z1)), nx, ny2,
            ieparams.dimfp_u, ieparams.dimfp_v, ieparams.opisid_u, ieparams.opisid_v,
            ieparams.hasf_u, ieparams.hasf_v, ieparams.scalar_k, 0, val_opu, val_opv, val_opk, res);
  return;
}

template<typename K>
void DuffyIM::k4TP(real_t z1, real_t z2, const KernelOperatorOnUnknowns& kuv, const Point& S1, const Point & M,
                 const Element* elt_S, const Element* elt_T, Matrix<K>& res, const Vector<real_t>* nx, const Vector<real_t>* ny,
                 IEcomputationParameters& ieparams) const
{
  MeshElement *melt=ieparams.melt_u;
  bool iso=ieparams.isoGeo;
  bool firstOrderElt = melt->order()==1, linearMap = firstOrderElt && !iso ;
  GeomMapData gmap;  //used when curved element
  Vector<real_t> nxt, nyt;
  Point Xp=S1+z1*M;
  Point Yp=S1+z2*M;
  Point xp, yp;

  //thePrintStream<<"k4TP :  S1="<<S1<<" M="<<M<<" z1="<<z1<<" z2="<<z2<<" Xp="<<Xp<<" Yp="<<Yp<<eol<<std::flush;

  real_t eltdif = 1.;
  if (!linearMap)  //update difelt and normal (done outside if linear map)
  {
    GeomMapData *mapP1=melt->meltP1->geomMapData_p;
    xp=mapP1->geomMapInverse(Xp);
    gmap=GeomMapData(melt,xp,true,true,false); //hard copy to avoid data race
    if(iso)
    {
       gmap.isoPar_p = ieparams.isoPar_x;
       gmap.useParametrization = true;
       gmap.useIsoNodes = true;
    }
    Xp=gmap.geomMap(xp);
    gmap.computeDifferentialElement();   //update dif element
    eltdif *= gmap.differentialElement;
    if (nx!=nullptr)
    {
      gmap.computeOrientedNormal();     //compute oriented y-normal
      nxt=gmap.normalVector;            //update normal vector
      nx=&nxt;
    }
    yp=mapP1->geomMapInverse(Yp);
    Yp=gmap.geomMap(yp);
    gmap.computeJacobianMatrix(yp);       //compute jacobian at Yp
    gmap.computeDifferentialElement();   //update dif element
    eltdif *= gmap.differentialElement;
    if (ny!=nullptr)
    {
      gmap.computeOrientedNormal();     //compute oriented y-normal
      nyt=gmap.normalVector;            //update normal vector
      ny=&nyt;
    }
  }

  if (ieparams.isP0 && ieparams.dimf_u==1 &&  ieparams.dimf_v==1)    //P0 shortcut
  {
    if (ieparams.isId)
    {
      K tmp;
      kuv.opker().eval(Xp, Yp, tmp, nx, ny);
      res[0]+=eltdif*tmp;
    }
    else
    {
      ShapeValues shv;
      shv.w=std::vector<real_t>(1,1.);
      Matrix<K> tmp=0.*res;
      kuv.eval(Xp, Yp, shv, shv, K(1.), tmp, nx, ny);
      res+=eltdif*tmp;
    }
    return;
  }

  // update reference points if linear map
  if (linearMap)
  {
    xp=melt->geomMapData_p->geomMapInverse(Xp);  // point Xp in reference element
    yp=melt->geomMapData_p->geomMapInverse(Yp);  // point Yp in reference element
  }

  //evaluate shape values
  bool refPoint = true;
  bool deru=ieparams.difforder_u>0, derv=ieparams.difforder_v>0;   //order of operator on unknowns >0
  ShapeValues shv_Tvx = elt_T->computeShapeValues(xp,refPoint,derv,false);
  ShapeValues shv_Suy = elt_S->computeShapeValues(yp,refPoint,deru,false);
  //evaluate matrix
  Vector<K> val_opu, val_opv, val_opk;
  kuv.evalF(Xp, Yp, shv_Suy, shv_Tvx, K(eltdif), nx, ny,
            ieparams.dimfp_u, ieparams.dimfp_v, ieparams.opisid_u, ieparams.opisid_v,
            ieparams.hasf_u, ieparams.hasf_v, ieparams.scalar_k, 0, val_opu, val_opv, val_opk, res);
  //thePrintStream<<std::scientific<<"    : xp="<<xp<<" yp="<<yp<<" Xp="<<Xp<<" Yp="<<Yp<<" eltdif="<<eltdif<<eol<<"res="<<res/eltdif<<eol;
  return;
}



template<typename K>
void DuffyIM::k5(real_t z1, real_t z2, const KernelOperatorOnUnknowns& kuv, const Point& S1, const Point & MS,
                 const Point & T1, const Point & MT, const Element* elt_S, const Element* elt_T, Matrix<K>& res,
                 const Vector<real_t>* nx, const Vector<real_t>* ny, IEcomputationParameters& ieparams) const
{
  MeshElement *meltu=ieparams.melt_u, *meltv=ieparams.melt_v;
  bool iso=ieparams.isoGeo;
  GeomMapData gmapu, gmapv;
  bool firstOrderEltu = meltu->order()==1, firstOrderEltv = meltv->order()==1;

  Point Y1=S1+z1*MS; //Ca(X);
  Point X1=T1+(z1*z2)*MT; // Ca(a(1-b))
  Point Y2=S1+(z1*z2)*MS;
  Point X2=T1+z2*MT;

  real_t eltdif1 = 1., eltdif2=1.;
  Vector<real_t> nxt, nyt;
  const Vector<real_t>* nx1=nx, *ny1=ny, *nx2=nx, *ny2=ny;  // linear geom element
  Point x1,x2,y1,y2;

  if (!firstOrderEltu || iso)  //update difelt and normal (done outside if linear map)
  {
    GeomMapData *mapP1=meltu->meltP1->geomMapData_p;
    y1=mapP1->geomMapInverse(Y1);
    gmapu=GeomMapData(meltu,y1,true,true,false);
    if(iso)
    {
       gmapu.isoPar_p = ieparams.isoPar_y;
       gmapu.useParametrization = true;
       gmapu.useIsoNodes = true;
    }
    Y1=gmapu.geomMap(y1);
    gmapu.computeDifferentialElement();   //update dif element
    eltdif1 *= gmapu.differentialElement;
    if (ny!=nullptr)
    {
      gmapu.computeOrientedNormal();     //compute oriented y-normal
      nyt=gmapu.normalVector;            //update normal vector
      ny1=&nyt;
    }
    y2=mapP1->geomMapInverse(Y2);
    Y2=gmapu.geomMap(y2);
    gmapu.computeJacobianMatrix(y2);      //compute jacobian at Y2
    gmapu.computeDifferentialElement();   //update dif element
    eltdif2 *= gmapu.differentialElement;
    if (ny!=nullptr)
    {
      gmapu.computeOrientedNormal();     //compute oriented y-normal
      ny2=&gmapu.normalVector;           //update normal vector
    }
  }
  if (!firstOrderEltv || iso)  //update difelt and normal (done outside if linear map)
  {
    GeomMapData *mapP1=meltv->meltP1->geomMapData_p;
    x1=mapP1->geomMapInverse(X1);
    gmapv=GeomMapData(meltv,x1,true,true,false);
    if(iso)
    {
       gmapv.isoPar_p = ieparams.isoPar_x;
       gmapv.useParametrization = true;
       gmapv.useIsoNodes = true;
    }
    X1=gmapv.geomMap(x1);
    gmapv.computeDifferentialElement();   //update dif element
    eltdif1 *= gmapv.differentialElement;
    if (nx!=nullptr)
    {
      gmapv.computeOrientedNormal();     //compute oriented x-normal
      nxt=gmapv.normalVector;            //update normal vector
      nx1=&nxt;
    }
    x2=mapP1->geomMapInverse(X2);
    X2=gmapv.geomMap(x2);
    gmapv.computeJacobianMatrix(x2);      //compute jacobian at X2
    gmapv.computeDifferentialElement();   //update dif element
    eltdif2 *= gmapv.differentialElement;
    if (nx!=nullptr)
    {
      gmapv.computeOrientedNormal();     //compute oriented x-normal
      nx2=&gmapv.normalVector;           //update normal vector
    }
  }

  if (ieparams.isP0 && ieparams.dimf_u==1 &&  ieparams.dimf_v==1)    //P0 shortcut
  {
    if (ieparams.isId)
    {
      K tmp;
      kuv.opker().eval(X1, Y1, tmp, nx1, ny1);
      res[0]+=eltdif1*z1*tmp;
      tmp*=0.;
      kuv.opker().eval(X2, Y2, tmp, nx2, ny2);
      res[0]+=eltdif2*z2*tmp;
    }
    else
    {
      ShapeValues shv;
      shv.w=std::vector<real_t>(1,1.);
      Matrix<K> tmp=0.*res;
      kuv.eval(X1, Y1, shv, shv, K(1.), tmp, nx1, ny1);
      res+=(eltdif1*z1)*tmp;
      tmp=0.*res;
      kuv.eval(X2, Y2, shv, shv, K(1.), tmp, nx2, ny2);
      res+=(eltdif2*z2)*tmp;
    }
    return;
  } // end if isP0

  // update reference points if linear map
  if (firstOrderEltu && !ieparams.isoGeo)
  {
    y1=meltu->geomMapData_p->geomMapInverse(Y1); // point Y1 in reference element
    y2=meltu->geomMapData_p->geomMapInverse(Y2); // point Y2 in reference element
  }
  if (firstOrderEltv && !ieparams.isoGeo)
  {
    x1=meltv->geomMapData_p->geomMapInverse(X1); // point X1 in reference element
    x2=meltv->geomMapData_p->geomMapInverse(X2); // point X2 in reference element
  }

  //evaluate shape values
  bool refPoint=true;
  bool deru=ieparams.difforder_u>0, derv=ieparams.difforder_v>0;   //order of operator on unknowns >0
  ShapeValues shv_Tvx1=elt_T->computeShapeValues(x1,refPoint,derv,false);
  ShapeValues shv_Tvx2=elt_T->computeShapeValues(x2,refPoint,derv,false);
  ShapeValues shv_Suy1=elt_S->computeShapeValues(y1,refPoint,deru,false);
  ShapeValues shv_Suy2=elt_S->computeShapeValues(y2,refPoint,deru,false);

  //evaluate matrix
  Vector<K> val_opu, val_opv, val_opk;
  kuv.evalF(X1, Y1,shv_Suy1,shv_Tvx1, K(eltdif1*z1), nx1, ny1,
            ieparams.dimfp_u, ieparams.dimfp_v, ieparams.opisid_u, ieparams.opisid_v,
            ieparams.hasf_u, ieparams.hasf_v, ieparams.scalar_k, 0, val_opu, val_opv, val_opk, res);
  kuv.evalF(X2, Y2, shv_Suy2, shv_Tvx2, K(eltdif2*z2), nx2, ny2,
            ieparams.dimfp_u, ieparams.dimfp_v, ieparams.opisid_u, ieparams.opisid_v,
            ieparams.hasf_u, ieparams.hasf_v, ieparams.scalar_k, 0, val_opu, val_opv, val_opk, res);
  return;
}

}
#endif // DUFFY_IM_HPP
