/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SauterSchwabIM.hpp
  \authors S.Roman and N.Salles, E. Lunéville
  \since 16 jun 2014
  \date 16 jun 2017

  \brief Implementation of the Sauter and Schwab's method to compute the FE TermMatrix
             intg_Eu intg Ev opu(y) opk(x,y) opv(x) dy dx
 */


#ifndef SAUTER_SCHWAB_IM_HPP
#define SAUTER_SCHWAB_IM_HPP

#include "finiteElements.h"
#include "utils.h"
#include "config.h"
#include "termUtils.hpp"

namespace xlifepp
{

/*!
  \class SauterSchwabIM
  integral over a product of geometric elements with singularity using Sauter-Schwab technique (for elements that shared vertices)
  Stefan A. Sauter, Christoph Schwab, "Boundary Element Methods", Springer, 2010
*/
class SauterSchwabIM : public DoubleIM
{
  public:
    Quadrature* quadSelf;    //!< quadrature on segment [0,1] for self influence (same element)
    Quadrature* quadEdge;    //!< quadrature on segment [0,1] for elements adjacent only by an edge
    Quadrature* quadVertex;  //!< quadrature on segment [0,1] for elements adjacent only by a vertex
    number_t ordSelf;        //!< order of quadrature for self influence (same element)
    number_t ordEdge;        //!< order of quadrature for for elements adjacent only by an edge
    number_t ordVertex;      //!< order of quadrature for for elements adjacent only by a vertex

    SauterSchwabIM(number_t ord =3) //! basic constructor (order 3 on segment)
    {
      ordSelf=ord; ordEdge=ordSelf; ordVertex=ordSelf;
      quadSelf=findQuadrature(_segment,_GaussLegendreRule,ordSelf); quadEdge=quadSelf; quadVertex=quadSelf;
      name="Sauter-Schwab_"+tostring(ordSelf);
      imType=_SauterSchwabIM; singularType=_r_; singularOrder=-1;
    }

    SauterSchwabIM(Quadrature& q) //! full constructor from quadrature object
    {
      ordSelf=q.degree; ordEdge=ordSelf; ordVertex=ordSelf;
      quadSelf=&q; quadEdge=quadSelf; quadVertex=quadSelf;
      imType=_SauterSchwabIM; singularType=_r_; singularOrder=-1;
      name="Sauter-Schwab_"+tostring(ordSelf);
    }

    virtual SauterSchwabIM* clone() const
    {return new SauterSchwabIM(*this);}

    virtual std::list<Quadrature*> quadratures() const  //! return the list of (single) quadratures in a list
    {
      std::list<Quadrature*> lsq;
      if(quadSelf!=nullptr) lsq.push_back(quadSelf);
      if(quadEdge!=nullptr && quadEdge!=quadSelf)  lsq.push_back(quadEdge);
      if(quadVertex!=nullptr && quadVertex!=quadSelf && quadVertex!=quadEdge) lsq.push_back(quadVertex);
      return lsq;
    }

    virtual void print(std::ostream& os) const          //! print IntegrationMethod on stream
    {
      os<<"Sauter-Schwab integration method with "<< quadSelf->name<<" degree " << quadSelf->degree
        << " ( " << quadSelf->quadratureRule.size() << " points ) " <<eol;
    }
    virtual void print(PrintStream& os) const {print(os.currentStream());}

    //template computeIE function, explicit instanciation because template functions are not inherited
    template<typename K>
    void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                   const KernelOperatorOnUnknowns& kuv, Matrix<K>& res,IEcomputationParameters& ieparams,
                   Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk) const;
    void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                   const KernelOperatorOnUnknowns& kuv, Matrix<real_t>& res,IEcomputationParameters& ieparams,
                   Vector<real_t>& val_opu, Vector<real_t>& val_opv, Vector<real_t>& val_opk) const
    {computeIE<>(elt_S, elt_T,adj, kuv, res,ieparams, val_opu, val_opv, val_opk);}
    void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                   const KernelOperatorOnUnknowns& kuv, Matrix<complex_t>& res,IEcomputationParameters& ieparams,
                   Vector<complex_t>& val_opu, Vector<complex_t>& val_opv, Vector<complex_t>& val_opk) const
    {computeIE<>(elt_S, elt_T,adj, kuv, res,ieparams, val_opu, val_opv, val_opk);}

    //quadrature tools
    template<typename K>
    void selfInfluences(const Element* elt_S, const KernelOperatorOnUnknowns& kuv,
                        const Vector<real_t>* nx, const Vector<real_t>* ny,
                        Matrix<K>& res, IEcomputationParameters& ieparams) const;
    template<typename K>
    void adjacentTrianglesbyEdge(const Element* elt_S, const Element* elt_T, const KernelOperatorOnUnknowns& kuv,
                                 const Vector<real_t>* nx, const Vector<real_t>* ny, const Vector< number_t >& indi, const Vector< number_t >& indj,
                                 Matrix<K>& res, IEcomputationParameters& ieparams) const;
    template<typename K>
    void adjacentTrianglesbyVertex(const Element* elt_S, const Element* elt_T, const KernelOperatorOnUnknowns& kuv, const Vector<real_t>* nx,
                                   const Vector<real_t>* ny, const Vector< number_t >& indi, const Vector< number_t >& indj,
                                   Matrix<K>& res, IEcomputationParameters& ieparams) const;

    template<typename K>
    void k2(const Point & Xp, const Point & Zp,const KernelOperatorOnUnknowns& kuv, const Point& S1, const Vector< Point >& M,
            const Element* elt_S,const Element* elt_T, Matrix<K>& res, const Vector<real_t>* nx, const Vector<real_t>* ny,
            IEcomputationParameters& ieparams) const;
    template<typename K>
    void k3(const Point & Ca, const Point & Cb, const KernelOperatorOnUnknowns& kuv, const Point& S1, const Point& T1,
            const Vector< Point >& MS, const Vector< Point >& MT, const Element* elt_S, const Element* elt_T, Matrix<K>& res,
            const Vector<real_t>* nx, const Vector<real_t>* ny, IEcomputationParameters& ieparams) const;
};

// implementation of template member functions
// main function that computes, using Sauter-Schwab method, the matrix
//    Aij = int_Tx int_Sy opu(wj)(y) op K(x,y) op opv(ti)(x) for (i,j)
// (wj) basis attached to u, (ti) basis attached to v
//  regards 3 cases : S=T (self influence), S adjacent to T by edge and S adjacent to T by vertex
//  Do not deal with disjoint triangles
template<typename K>
void SauterSchwabIM::computeIE(const Element* elt_Su, const Element* elt_Tv, AdjacenceInfo& adj,
                               const KernelOperatorOnUnknowns& kuv, Matrix<K>& res, IEcomputationParameters& ieparams,
                               Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk) const
{
  if (ieparams.sh_u!=_triangle)
    error("shape_not_handled", words("shape", ieparams.sh_u));
  if (ieparams.sh_v!=_triangle)
    error("shape_not_handled", words("shape", ieparams.sh_v));

  //load normals
  Vector<real_t>* nx=nullptr, *ny=nullptr;
  MeshElement* melt_S=ieparams.melt_u, *melt_T=ieparams.melt_v;
  if (kuv.xnormalRequired()) nx = &melt_T->geomMapData_p->normalVector;
  if (kuv.ynormalRequired()) ny = &melt_S->geomMapData_p->normalVector;

//  thePrintStream<<"compute on melt_S : "<<melt_S->vertex(1)<<" "<<melt_S->vertex(2)<<" "<<melt_S->vertex(3)
//                          <<" melt_T : "<<melt_T->vertex(1)<<" "<<melt_T->vertex(2)<<" "<<melt_T->vertex(3)<<eol;

  //  compute quadrature regarding triangle position
  std::vector<number_t>& indi=adj.sharedVertex1, &indj=adj.sharedVertex2;
  switch (adj.status)
  {

    case _adjacentByElement:
    {
        selfInfluences(elt_Su, kuv, nx, ny, res, ieparams);
        //thePrintStream<<"self influence : res = "<<eol<<res<<eol;
        return;
    }
    case _adjacentBySide:
    {
      indi[2]=6-indi[0]-indi[1]; indj[2]=6-indj[0]-indj[1];
      adjacentTrianglesbyEdge(elt_Su, elt_Tv, kuv, nx, ny, indi, indj, res, ieparams);
      //thePrintStream<<"adjacent by side: res = "<<eol<<res<<eol;
      return;
    }
    case _adjacentByVertex:
    {
      switch (indi[0])
      {
        case 1: indi[1]=2; indi[2]=3; break;
        case 2: indi[1]=3; indi[2]=1; break;
        default: indi[1]=1; indi[2]=2;
      }
      switch (indj[0])
      {
        case 1: indj[1]=2; indj[2]=3; break;
        case 2: indj[1]=3; indj[2]=1; break;
        default: indj[1]=1; indj[2]=2;
      }
      adjacentTrianglesbyVertex(elt_Su, elt_Tv, kuv, nx, ny, indi, indj, res, ieparams);
      //thePrintStream<<"adjacent by vertex : res = "<<eol<<res<<eol;
      return;
    }
    default:
      error("adjacence_status_not_handled");
  }
}

/*================================================================================================================
                          Same triangles, singular integral -> use Sauter-Schwab formulae

  up to now, it works only for scalar cases

  arguments:
     elt_S, elt_T: elements involved in integral
     kuv: integrand op(u)*op(ker)*op(v)
     nx,ny: pointers to normal of elt_T and elt_S (0 if not used)
     ieparams: some parameters of computation
     indi, indj: vertex numbering
     quad:  1d quadrature method used inSauter-Schwabb method
     res: matrix int_elt_S x elt_T op(wj)*op(ker)*op(wi)

     see Sauter-Schwab (5.34)
  ================================================================================================================*/

template<typename K>
void SauterSchwabIM::selfInfluences(const Element* elt_S, const KernelOperatorOnUnknowns& kuv,
                                       const Vector<real_t>* nx, const Vector<real_t>* ny,
                                       Matrix<K>& res, IEcomputationParameters& ieparams) const
{
  MeshElement* meltu=ieparams.melt_u;
  Point& S1=*meltu->nodes[0], & S2= *meltu->nodes[1],  & S3= *meltu->nodes[2];
  bool iso=ieparams.isoGeo;

  //definition of matrix MS
  Vector< Point > M(2);
  M[0]=S2-S1; M[1]=S3-S2;
  real_t G12=meltu->geomMapData_p->differentialElement; G12*=G12;
//  thePrintStream<<"self_edge G1G2="<<G12<<" |S1S2xS2S3|^2="<<norm(crossProduct(M[0],M[1]))*norm(crossProduct(M[0],M[1]))<<eol<<std::flush;
//  thePrintStream<<"    S1="<<S1<<" S2="<<S2<<" S3="<<S3<<" linear="<<(meltu->linearMap && !iso?"true":"false")<<eol<<std::flush;
  number_t N=quadSelf->numberOfPoints();
  real_t a, b, c, d;
  const std::vector< real_t >& coords=quadSelf->coords();
  const std::vector< real_t >& weights=quadSelf->weights();
  Matrix<K> matel(res.numberOfRows(),res.numberOfColumns(),K(0));
  Point& U=M[0];
  Point& V=M[1];
  Point X0,Z0,abV,Z10,X30,abcV,Z3,X1,Z2,tU,tV,Z1,X2,X3;
  for(number_t i1=0; i1<N; i1++)
  {
    a=coords[i1];
    X0=S1+a*U; //M[0];
    Z0=X0+a*V;
    for(number_t i2=0; i2<N; i2++)
    {
      real_t w2=weights[i1]*weights[i2];
      b=coords[i2];
      real_t ab=a*b;
      abV=ab*V;
      Z10=Z0-abV;
      X30=X0+abV; //M[1];
      for(number_t i3=0; i3<N; i3++)
      {
        c=coords[i3];
        real_t w3=w2*weights[i3];
        //real_t a3b2c=a*a*a*b*b*c*G12, abc=ab*c;  move product by diff. element after loop if linear element
        real_t a3b2c=a*a*a*b*b*c, abc=ab*c;
        abcV=abc*V;
        Z3=X30-abcV;
        X1=Z10+abcV;
        Z2=X30-abcV-abc*U;
        for(number_t i4=0; i4<N; i4++)
        {
          real_t w4=w3*weights[i4];
          d=coords[i4];
          real_t abcd=abc*d;
          matel*=0.;
          tU=abcd*U;
          tV=abcd*V;
          Z1=Z10-tU;
          X2=Z3+tV;
          X3=X30-tU-tV;
          //thePrintStream<<"is="<<i1<<" "<<i2<<" "<<i3<<" "<<i4<<eol;
          k2(X1,Z1, kuv, S1, M, elt_S, elt_S, matel, nx, ny, ieparams);
          k2(X2,Z2, kuv, S1, M, elt_S, elt_S, matel, nx, ny, ieparams);
          k2(X3,Z3, kuv, S1, M, elt_S, elt_S, matel, nx, ny, ieparams);
          res+=w4*a3b2c*matel;
        }
      }
    }
  }
  if(meltu->linearMap && !iso) res*=G12; // product by diff. element here if linear map
  //thePrintStream<<"self "<<elt_S->number()<<" res="<<res<<eol<<std::flush;
}

/*================================================================================================================
                          Adjacent triangles by edge, singular integral -> use Sauter-Schwab formulae
  up to now, it works only for scalar cases

  arguments:
     elt_Su, elt_Tv: elements involved in integral
     kuv: integrand op(u)*op(ker)*op(v)
     nx,ny: pointers to normal of elt_Tv and elt_Su (0 if not used)
     ieparams: some parameters of computation
     indi, indj: vertex numbering
     quad:  1d quadrature method used in Sauter-Schwab method
     res: matrix int_elt_Sy x elt_Tx op(wj)(y)*op(ker)(x,y)*op(wi)(x)

     see Sauter-Schwab (5.38)
  ================================================================================================================*/
template<typename K>
void SauterSchwabIM::adjacentTrianglesbyEdge(const Element* elt_Su, const Element* elt_Tv, const KernelOperatorOnUnknowns& kuv,
    const Vector<real_t>* nx, const Vector<real_t>* ny, const Vector< number_t >& indi, const Vector< number_t >& indj,
    Matrix<K>& res, IEcomputationParameters& ieparams) const
{
  MeshElement* meltu=ieparams.melt_u;
  Point& S1=*meltu->nodes[indi[0]-1], & S2= *meltu->nodes[indi[1]-1],  & S3= *meltu->nodes[indi[2]-1];
  MeshElement* meltv=ieparams.melt_v;
  Point& T1=*meltv->nodes[indj[0]-1], & T2= *meltv->nodes[indj[1]-1],  & T3= *meltv->nodes[indj[2]-1];
  //thePrintStream<<"adjacent edge indi="<<indi<<" indj="<<indj<<eol;
  bool iso=ieparams.isoGeo;

  //definition of matrices MS and MT
  Vector< Point > MS(2), MT(2);
  MS[0]=S2-S1;   MS[1]=S3-S2;
  MT[0]=T2-T1;   MT[1]=T3-T2;
  Point& S12=MS[0], &S23=MS[1];
  Point& T12=MT[0], &T23=MT[1];

  real_t G1=meltu->geomMapData_p->differentialElement, G2= meltv->geomMapData_p->differentialElement;
  //thePrintStream<<"adj_edge G1="<<G1<<" G2="<<G2<<" G1G2="<<G1*G2<<eol<<std::flush;

  // Definition des Points et Poids de quadrature
  number_t N=quadEdge->numberOfPoints();
  real_t a,b,c,d;
  const std::vector< real_t >& coords=quadEdge->coords();
  const std::vector< real_t >& weights=quadEdge->weights();
  Matrix<K> matel1(res.numberOfRows(),res.numberOfColumns(),K(0)), matel(res.numberOfRows(),res.numberOfColumns(),K(0));

  Point S13=S3-S1;   //S3-S1
  Point T13=T3-T1;   //T3-T1
  Point X0, X1, X2, X3, X4, X5, Y0, Y1, Y2, Y3, Y4, Y5, Y4t, Yt, tmpY;
  for(number_t i1=0; i1<N; i1++)
  {
    a=coords[i1];
    Y0=S1+a*S12;  //Y0=S12;Y0*=a;Y0+=S1;
    X0=T1+a*T12;  //X0=T12;X0*=a;X0+=T1;
    for(number_t i2=0; i2<N; i2++)
    {
      real_t w2=weights[i1]*weights[i2];
      b=coords[i2];
      real_t ab=a*b, a3b2=a*a*ab*b;
      Y2=Y0+ab*S23; //Y2=S23; Y2*=ab; Y2+=Y0;
      X4=X0+ab*T23; //X4=T32; X4*=ab; X4+=X0;
      for(number_t i3=0; i3<N; i3++)
      {
        real_t w3=w2*weights[i3];
        c=coords[i3];
        real_t abc=ab*c;
        Y3=Y2-abc*S13;  // Y3=S13; Y3*=(-abc); Y3+=Y2;
        Y4t=Y0+abc*S23; // Y4t=S23; Y4t*=abc; Y4t+=Y0;
        X5=X0+abc*T23;  // X5=T23; X5*=abc; X5+=X0
        X1=X4-abc*T13;  // X1=T13; Y1*=(-abc); X1+=X4;
        for(number_t i4=0; i4<N; i4++)
        {
          real_t w4=w3*weights[i4];
          d=coords[i4];
          real_t abcd=abc*d;
          real_t abd=ab*d;
          Yt=abcd*S13;    // Yt=S13; Yt*=abcd;
          Y4=Y4t-Yt;      // Y4=Y4t; Y4-=Yt;
          Y1=Y0+abd*S23;  // Y1=S23; Y1*=abd; Y1+=Y0;
          Y5=Y2-Yt;       // Y5=Y2; Y5-=Yt;
          X2=X5-abcd*T13; // X2=T13; X2*=(-abcd); X2+=X5;
          X3=X0+abcd*T23; // X3=T23; X3*=abcd; X3+=X0;
//          thePrintStream<<"is="<<i1<<" "<<i2<<" "<<i3<<" "<<i4<<" X1="<<X1<<" Y1="<<Y1
//           <<" X2="<<X2<<" Y2="<<Y2<<" X3="<<X3<<" Y3="<<Y3<<" X4="<<X4<<" Y4="<<Y4<<" X5="<<X5<<" Y5="<<Y5<<eol<<std::flush;
          matel*=0.; matel1*=0.;
          k3(X1, Y1, kuv, S1, T1, MS, MT, elt_Su, elt_Tv, matel1, nx, ny, ieparams);
          k3(X2, Y2, kuv, S1, T1, MS, MT, elt_Su, elt_Tv, matel, nx, ny, ieparams);
          k3(X3, Y3, kuv, S1, T1, MS, MT, elt_Su, elt_Tv, matel, nx, ny, ieparams);
          k3(X4, Y4, kuv, S1, T1, MS, MT, elt_Su, elt_Tv, matel, nx, ny, ieparams);
          k3(X5, Y5, kuv, S1, T1, MS, MT, elt_Su, elt_Tv, matel, nx, ny, ieparams);
          res+=a3b2*w4*(c*matel+matel1);
        }
      }
    }
  }
  if(meltu->linearMap && !iso) res*=G1;
  if(meltv->linearMap && !iso) res*=G2;
  //thePrintStream<<"adj_edge "<<elt_Tv->number()<<" "<<elt_Su->number()<<" res="<<res<<eol<<std::flush;
}

/*================================================================================================================
                          Adjacent triangles by vertex, singular integral -> use Sauter-Schwab formulae

  up to now, it works only for scalar cases

  arguments:
     elt_Su, elt_Tv: elements involved in integral
     kuv: integrand op(u)*op(ker)*op(v)
     nx,ny: pointers to normal of elt_Tv and elt_Su (0 if not used)
     ieparams: some parameters of computation
     indi, indj: vertex numbering
     quad:  1d quadrature method used inSauter-Schwabb method
     res: matrix int_elt_Sy x elt_Tx op(wj)(y)*op(ker)(x,y)*op(wi)(x)

     see Sauter-Schwab (5.39)
  ================================================================================================================*/
template<typename K>
void SauterSchwabIM::adjacentTrianglesbyVertex(const Element* elt_Su, const Element* elt_Tv, const KernelOperatorOnUnknowns& kuv,
    const Vector<real_t>* nx, const Vector<real_t>* ny, const Vector< number_t >& indi, const Vector< number_t >& indj,
    Matrix<K>& res, IEcomputationParameters& ieparams) const
{
  MeshElement* meltu=ieparams.melt_u;
  Point& S1=*meltu->nodes[indi[0]-1], & S2= *meltu->nodes[indi[1]-1],  & S3= *meltu->nodes[indi[2]-1];
  MeshElement* meltv=ieparams.melt_v;
  Point& T1=*meltv->nodes[indj[0]-1], & T2= *meltv->nodes[indj[1]-1],  & T3= *meltv->nodes[indj[2]-1];
//  thePrintStream<<"adjacent vertex indi="<<indi<<" indj="<<indj<<eol;
  bool iso=ieparams.isoGeo;

  //definition of matrices MS, MT
  Vector< Point > MS(2), MT(2);
  MS[0]=S2-S1;  MS[1]=S3-S2;
  MT[0]=T2-T1;  MT[1]=T3-T2;
  real_t G1=meltu->geomMapData_p->differentialElement, G2= meltv->geomMapData_p->differentialElement;
//  thePrintStream<<"adj_vertex G1="<<G1<<" G2="<<G2<<" G1G2="<<G1*G2<<"|S1S2xS2S3|="<<norm(crossProduct(MS[0],MS[1]))<<eol<<std::flush;

  number_t N=quadVertex->numberOfPoints();
  real_t a,b,c,d;
  const std::vector< real_t >& coords=quadVertex->coords();
  const std::vector< real_t >& weights=quadVertex->weights();
  Matrix<K> matel(res.numberOfRows(),res.numberOfColumns(),K(0));

  Point X0,Y0,X1,Y1,X2,Y2,Xt,Yt;

  for(number_t i1=0; i1<N; i1++)
  {
    a=coords[i1];
    Y0=S1+a*MS[0];
    X0=T1+a*MT[0];
    for(number_t i2=0; i2<N; i2++)
    {
      real_t w2=weights[i2]*weights[i1];
      real_t ab=a*coords[i2];
      Y1=Y0+ab*MS[1];
      X2=X0+ab*MT[1];
      for(number_t i3=0; i3<N; i3++)
      {
        real_t w3=w2*weights[i3];
        //real_t a3c=a*a*a*c*G1G2, ac=a*c;
        real_t ac=a*coords[i3], a3c=a*a*ac;
        Yt=S1+ac*MS[0];
        Xt=T1+ac*MT[0];
        for(number_t i4=0; i4<N; i4++)
        {
          real_t w4=w3*weights[i4];
          real_t acd=ac*coords[i4];
          Y2=Yt+acd*MS[1];
          X1=Xt+acd*MT[1];
          matel*=0.;
          k3(X1,Y1, kuv, S1, T1, MS, MT, elt_Su, elt_Tv, matel, nx, ny, ieparams);
          k3(X2,Y2, kuv, S1, T1, MS, MT, elt_Su, elt_Tv, matel, nx, ny, ieparams);
//          k3(Y1,X1, kuv, S1, T1, MS, MT, elt_Su, elt_Tv, matel, nx, ny, ieparams);
//          k3(Y2,X2, kuv, S1, T1, MS, MT, elt_Su, elt_Tv, matel, nx, ny, ieparams);
          res+=a3c*w4*matel;
        } // end i4
      } // end i3
    } // end i2
  } // end i1
  if(meltu->linearMap && !iso) res*=G1;
  if(meltv->linearMap && !iso) res*=G2;
  //thePrintStream<<"adj_vertex "<<elt_Tv->number()<<" "<<elt_Su->number()<<" res="<<res<<eol<<std::flush;
}

/*================================================================================================================
                                       member functions k2 ans k3
  ================================================================================================================*/
template<typename K>
void SauterSchwabIM::k2(const Point & X, const Point & Y,const KernelOperatorOnUnknowns& kuv, const Point& S1, const Vector< Point >& M,
                        const Element* elt_Su,const Element* elt_Tv, Matrix<K>& res, const Vector<real_t>* nx, const Vector<real_t>* ny,
                        IEcomputationParameters& ieparams) const
{
  MeshElement* meltu=ieparams.melt_u;
  bool iso=ieparams.isoGeo;
  real_t eltdif=1.;
  const Vector<real_t> *nx2=nx, *ny2=ny;
  Vector<real_t> nu, nv;
  Point Xc, Yc, x, y;
  const Point *Xp=&X, *Yp=&Y;
  if(!meltu->linearMap || iso)  //curved element (diff. elements product, done outside if linear)
  {
    /* X and Y are points in the linear triangle (S1,S2,S3). They must be moved on the curved element
       using the P1 related mesh element meltP1 to get points x and y in the reference triangle
       and then mapped to points Xc and Yc in the curved element */
    y = meltu->meltP1->geomMapData_p->geomMapInverse(Y);           // point Y in reference element
    //GeomMapData geomap(meltu,y,true,true,ny!=nullptr);
    GeomMapData geomap(meltu,y);
    if(iso)
    {
       geomap.isoPar_p = ieparams.isoPar_x;
       geomap.useParametrization = true;
       geomap.useIsoNodes = true;
    }
    geomap.computeJacobianMatrix(y);
    geomap.computeDifferentialElement();
    eltdif *= geomap.differentialElement;   // y differential element
    Yc = geomap.geomMap(y); Yp=&Yc;         // Yc on curved element
    //thePrintStream<<" Y="<<Y<<" y="<<y<<" Yc="<<Yc<<" dy="<<geomap.differentialElement<<eol;
    if(ny!=nullptr)
    {
       geomap.computeOrientedNormal();
       nu=geomap.normalVector;              // update normal vector
       ny=&nu;
       if(nx!=nullptr) nx2=ny;
    }
    x = meltu->meltP1->geomMapData_p->geomMapInverse(X);   // point X in reference element
    geomap.computeJacobianMatrix(x);
    geomap.computeDifferentialElement();
    eltdif*= geomap.differentialElement;    // x differential element
    Xc = geomap.geomMap(x); Xp=&Xc;         // Xc on curved element
    //thePrintStream<<" X="<<X<<" x="<<x<<" Xc="<<Xc<<" dx="<<geomap.differentialElement<<" dxdy="<<eltdif<<eol;
    if(nx!=nullptr)
    {
       geomap.computeOrientedNormal();
       nv=geomap.normalVector;             //update normal vector
       nx=&nv;
       if(ny!=nullptr) ny2=nx;
    }
  }

  //scalar P0 shortcut
  if(ieparams.isP0 &&  ieparams.dimf_u==1 &&  ieparams.dimf_v==1)
  {
    if(ieparams.isId)
    {
      K tmp;
      kuv.opker().eval(*Xp, *Yp, tmp, nx, ny);
      res[0]+=tmp*eltdif;
      kuv.opker().eval(*Yp, *Xp, tmp, nx2, ny2);
      res[0]+=tmp*eltdif;
    }
    else
    {
      ShapeValues shv;
      shv.w=std::vector<real_t>(1,1.);
      kuv.eval(*Xp, *Yp, shv, shv, K(eltdif), res, nx, ny);
      kuv.eval(*Yp, *Xp, shv, shv, K(eltdif), res, nx2, ny2);
    }
    return;
  }

  // update reference points if linear map
  if(meltu->linearMap && !iso)
  {
     //thePrintStream<<" Y="<<Y<<" X="<<X;
     x=meltu->geomMapData_p->geomMapInverse(X); // point X in reference element
     y=meltu->geomMapData_p->geomMapInverse(Y); // point Y in reference element
     //thePrintStream<<" y="<<y<<" x="<<x<<eol;
  }

  // general case (when linear element diff. elements product is done outside)
  K alpha = eltdif;
  if(elt_Su==elt_Tv) //same elements means same interpolation and same shapevalues
  { //evaluate shape values
    bool wd  = (ieparams.difforder_u>0 || ieparams.difforder_v>0),     //order of operator on unknowns >0
         wd2 = (ieparams.difforder_u>1 || ieparams.difforder_v>1);     //order of operator on unknowns >1
    ShapeValues shv_Svx=elt_Su->computeShapeValues(x,true,wd,wd2,ieparams.sign_u); // =shv_Tx
    ShapeValues shv_Suy=elt_Su->computeShapeValues(y,true,wd,wd2,ieparams.sign_u); // =shv_Ty
    //thePrintStream<<"  shv_Svx.w="<<shv_Svx.w<<"  shv_Suy.w="<<shv_Suy.w<<eol;
    //evaluate matrix
    Vector<K> val_opu, val_opv, val_opk;
    kuv.evalF(*Xp, *Yp, shv_Suy, shv_Svx, alpha, nx, ny,
              ieparams.dimfp_u, ieparams.dimfp_v, ieparams.opisid_u, ieparams.opisid_v,
              ieparams.hasf_u, ieparams.hasf_v, ieparams.scalar_k, 0, val_opu, val_opv, val_opk, res);
              //thePrintStream<<"   val_opk(Xp,Yp)="<<val_opk<<" res[0]="<<res[0]<<eol;
    kuv.evalF(*Yp, *Xp, shv_Svx, shv_Suy, alpha, nx2, ny2,
              ieparams.dimfp_u, ieparams.dimfp_v, ieparams.opisid_u, ieparams.opisid_v,
              ieparams.hasf_u, ieparams.hasf_v, ieparams.scalar_k, 0, val_opu, val_opv, val_opk, res);
              //thePrintStream<<"   val_opk(Yp,Xp)="<<val_opk<<" res[0]="<<res[0]<<eol;
    return;
  }
  //different element, means not same interpolation if geometric elements are the same
  //evaluate shape values
  bool wdu=ieparams.difforder_u>0, wdv=ieparams.difforder_v>0;   //order of operator on unknowns >0
  bool wd2u=ieparams.difforder_u>1, wd2v=ieparams.difforder_v>1; //order of operator on unknowns >1
  ShapeValues shv_Sux=elt_Su->computeShapeValues(x,true,wdu,wd2u,ieparams.sign_u);
  ShapeValues shv_Suy=elt_Su->computeShapeValues(y,true,wdu,wd2u,ieparams.sign_u);
  ShapeValues shv_Tvx=elt_Tv->computeShapeValues(x,true,wdv,wd2v,ieparams.sign_v);
  ShapeValues shv_Tvy=elt_Tv->computeShapeValues(y,true,wdv,wd2v,ieparams.sign_v);
  //evaluate matrix
  Vector<K> val_opu, val_opv, val_opk;
  kuv.evalF(*Xp, *Yp, shv_Suy, shv_Tvx, alpha, nx, ny,
            ieparams.dimfp_u, ieparams.dimfp_v, ieparams.opisid_u, ieparams.opisid_v,
            ieparams.hasf_u, ieparams.hasf_v, ieparams.scalar_k, 0, val_opu, val_opv, val_opk, res);
  kuv.evalF(*Yp, *Xp, shv_Sux, shv_Tvy, alpha, nx2, ny2,
            ieparams.dimfp_u, ieparams.dimfp_v, ieparams.opisid_u, ieparams.opisid_v,
            ieparams.hasf_u, ieparams.hasf_v, ieparams.scalar_k, 0, val_opu, val_opv, val_opk, res);
  return;
}

template<typename K>
void SauterSchwabIM::k3(const Point & X, const Point & Y, const KernelOperatorOnUnknowns& kuv, const Point& S1, const Point& T1,
                        const Vector<Point>& MS, const Vector<Point>& MT, const Element* elt_S, const Element* elt_T, Matrix<K>& res,
                        const Vector<real_t>* nx, const Vector<real_t>* ny, IEcomputationParameters& ieparams) const
{

  /* X and Y are points in the linear triangle (S1,S2,S3). They must be moved on the curved element
     using the P1 related mesh element meltP1 to get points (x,y) in the reference triangle
     and then mapped to points Xc and Yc in the curved element */
  MeshElement* meltu=ieparams.melt_u, *meltv=ieparams.melt_v;
  bool iso=ieparams.isoGeo;
  const Vector<real_t> *nx2=nx, *ny2=ny;
  Vector<real_t> nu, nv;
  Point Xc, Yc, x, y;
  const Point *Xp=&X, *Yp=&Y;
  real_t eltdif=1.;
  //thePrintStream<<" in k3 : ";
  if(!meltu->linearMap || iso)  //curved element (diff. elements product, done outside if linear)
  {
    /* X and Y are points in the linear triangle (S1,S2,S3). They must be moved on the curved element
       using the P1 related mesh element meltP1 to get points x and y in the reference triangle
       and then mapped to points Xc and Yc in the curved element */
    y = meltu->meltP1->geomMapData_p->geomMapInverse(Y);           // point Y in reference element
    //GeomMapData geomapu(meltu,y,true,true,ny!=nullptr);
    GeomMapData geomapu(meltu,y);
    if(iso)
    {
       geomapu.isoPar_p = ieparams.isoPar_y;
       geomapu.useParametrization = true;
       geomapu.useIsoNodes = true;
    }
    geomapu.computeJacobianMatrix(y);
    geomapu.computeDifferentialElement();
    eltdif *= geomapu.differentialElement;   // y differential element
    Yc = geomapu.geomMap(y); Yp=&Yc;         // Yc on curved element
    //thePrintStream<<" Y="<<Y<<" y="<<y<<" Yc="<<Yc<<" dy="<<geomap->differentialElement<<eol;
    if(ny!=nullptr)
    {
       geomapu.computeOrientedNormal();
       nu=geomapu.normalVector;              // update normal vector
       ny=&nu;
       if(nx!=nullptr) nx2=ny;
    }
  }
  if(!meltv->linearMap || iso)  //curved element (diff. elements product, done outside if linear)
  {
    x = meltv->meltP1->geomMapData_p->geomMapInverse(X);   // point X in reference element
    //GeomMapData geomapv(meltv,x,true,true,nx!=nullptr);
    GeomMapData geomapv(meltv,y);
    if(iso)
    {
       geomapv.isoPar_p = ieparams.isoPar_x;
       geomapv.useParametrization = true;
       geomapv.useIsoNodes = true;
    }
    geomapv.computeJacobianMatrix(x);
    geomapv.computeDifferentialElement();
    eltdif*= geomapv.differentialElement;    // x differential element
    Xc = geomapv.geomMap(x); Xp=&Xc;         // Xc on curved element
    //thePrintStream<<" X="<<X<<" x="<<x<<" Xc="<<Xc<<" dx="<<geomap->differentialElement<<" dxdy="<<eltdif<<eol;
    if(nx!=nullptr)
    {
       geomapv.computeOrientedNormal();
       nv=geomapv.normalVector;             //update normal vector
       nx=&nv;
       if(ny!=nullptr) ny2=nx;
    }
  }

  K alpha = eltdif; //eltdif=1 for linear element, diff. elements product done outside

  if(ieparams.isP0 &&  ieparams.dimf_u==1 &&  ieparams.dimf_v==1)
  {
    if(ieparams.isId)
    {
      K tmp=K(0.);
      kuv.opker().eval(*Xp, *Yp, tmp, nx, ny);
      res[0]+=tmp*alpha;
    }
    else
    {
      ShapeValues shv;
      shv.w=std::vector<real_t>(1,1.);
      kuv.eval(*Xp, *Yp , shv, shv, alpha, res, nx, ny);
    }
    return;
  }

  if(y.size()==0) y=meltu->geomMapData_p->geomMapInverse(Y); //linear element
  if(x.size()==0) x=meltv->geomMapData_p->geomMapInverse(X); //linear element

  //thePrintStream<<" eltdif="<<eltdif<<" x="<<x<<" y="<<y<<" Xp="<<*Xp<<"Yp="<<*Yp<<eol;

  //general case
  ShapeValues shv_Suy=elt_S->computeShapeValues(y,true,ieparams.difforder_u>0,ieparams.difforder_u>1,ieparams.sign_u);
  ShapeValues shv_Tvx=elt_T->computeShapeValues(x,true,ieparams.difforder_v>0,ieparams.difforder_v>1,ieparams.sign_v);

  Vector<K> val_opu, val_opv, val_opk;
  kuv.evalF(*Xp, *Yp, shv_Suy, shv_Tvx, alpha, nx, ny,
            ieparams.dimfp_u, ieparams.dimfp_v, ieparams.opisid_u, ieparams.opisid_v,
            ieparams.hasf_u, ieparams.hasf_v, ieparams.scalar_k, 0, val_opu, val_opv, val_opk, res);
}

}
#endif // SAUTER_SCHWAB_IM_HPP
