/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SPMatrixComputation.hpp
  \author E. Lunéville
  \since 9 jan 2013
  \date 25 juin 2013

  \brief Implementation of template SP TermMatrix computation functions

  this file is included by TermMatrix.hpp
  do not include it elsewhere
*/

/* ================================================================================================
                                      tools
   ================================================================================================ */

namespace xlifepp
{

/*!compute values of operator on spectral unknown at quadrature point, say
    op(phi_n)(x)   with (phi) i=1,n the spectral functions and x a quadrature point in physical space

 op: operator on unknown
 basis: spectral basis associated to unknown
 x: point where operator is evaluated (in physical space of spectral functions)
 vtphi: value type of basis function
 nbfun: number of function
 dimfun: dimension of function
 val: computed values as a vector of vectors: val = (op(phi_n)(x)) i=1,n
 phyPoints: quadrature points in physical space

 NOTE: operator with derivatives are only supported with ANALYTICAL spectral functions
        in Function defining them, user has to provide derivatives using the parameter "derivative"
        and its value Number(_dx), Number(_dxx), ....

  ### operator with derivatives are not yet handled for VECTOR spectral functions ###
*/
template <typename K>
void computeSPOperator(const OperatorOnUnknown& op, const SpectralBasis* spbasis, const Point& x,
                       ValueType vtphi, number_t nbfun, dimen_t dimfun, Vector<K>& val, const Vector<real_t>* np =nullptr)
{
  number_t diford=op.diffOrder();
  if(diford>0 && spbasis->funcFormType()==_interpolated)
  {
    where("computeSPOperator(...)"); error("non_derivative_op_only");
  }
  number_t nbv=nbfun*dimfun, dim=x.size();
  bool shortcut = !op.hasFunction() && op.difOpType()==_id;

  if(dimfun==1) //scalar case
  {
    val.resize(nbv);
    if(shortcut)  // id and no function
    {
      if(spbasis->funcFormType()==_analytical) static_cast<const SpectralBasisFun*>(spbasis)->functions(x,val);
      else static_cast<const SpectralBasisInt*>(spbasis)->functions(x,val);
      if(op.conjugateUnknown()) val.toConj();
      return;
    }
    else
    {
      Vector<K> v(nbfun, K(0.));
      Vector<std::vector<K> > der(dim);
      Vector<std::vector<K> > der2;
      Vector<K> dv;
      dimen_t d,m; //block size
      if(op.difOpType()==_id)
      {
        if(spbasis->funcFormType()==_analytical) static_cast<const SpectralBasisFun*>(spbasis)->functions(x,v);
        else static_cast<const SpectralBasisInt*>(spbasis)->functions(x,v);
      }
      else //compute derivatives of spectral functions
      {
        if(spbasis->funcFormType()==_analytical)
        {
          if(diford==0) static_cast<const SpectralBasisFun*>(spbasis)->functions(x,v);
          else if(diford==1)
          {
            static_cast<const SpectralBasisFun*>(spbasis)->functions(x,dv,_dx);
            der[0]=dv;
            if(dim>1)
            {
              static_cast<const SpectralBasisFun*>(spbasis)->functions(x,dv,_dy);
              der[1]=dv;
            }
            if(dim>2)
            {
              static_cast<const SpectralBasisFun*>(spbasis)->functions(x,dv,_dz);
              der[2]=dv;
            }
          }
          else if(diford==2)  // second derivatives stored as: dxx  [dyy dxy] [dzz dxz dyz]
          {
            number_t d2=(dim*(dim+1))/2;
            der2.resize(d2);
            static_cast<const SpectralBasisFun*>(spbasis)->functions(x,dv,_dxx);
            der2[0]=dv;
            if(dim>1)
            {
              static_cast<const SpectralBasisFun*>(spbasis)->functions(x,dv,_dyy);
              der2[1]=dv;
              static_cast<const SpectralBasisFun*>(spbasis)->functions(x,dv,_dxy);
              der2[2]=dv;
            }
            else if (dim>2) // dim=3
            {
              static_cast<const SpectralBasisFun*>(spbasis)->functions(x,dv,_dzz);
              der2[3]=dv;
              static_cast<const SpectralBasisFun*>(spbasis)->functions(x,dv,_dxz);
              der2[4]=dv;
              static_cast<const SpectralBasisFun*>(spbasis)->functions(x,dv,_dyz);
              der2[5]=dv;
            }
          }
        }
        else static_cast<const SpectralBasisInt*>(spbasis)->functions(x,v);
      }
      if(op.conjugateUnknown())
      {
          if(diford==0) v.toConj();
          else if (diford==1) der.toConj();
          else if (diford==2) der2.toConj();
      }
      if(op.hasFunction()) op.eval(x, v, der, der2, dimfun, val, d,m, np);     //evaluate differential operator with function
      else                 op.eval(v, der, der2, dimfun, val, d, m, np);       //evaluate differential operator
      return;
    }
  }
  if(diford>0)  //in vector case, op with derivative not yet implemented
    {
      where("computeSPOperator(...)");
      error("non_derivative_op_only");
    }
  number_t nbc=op.unknown()->nbOfComponents();
  if(nbc>1) //scalar extended case
    {
      Vector<K> v(nbfun, K(0.));
      dimen_t d,m; //block size
      if(spbasis->funcFormType()==_analytical) static_cast<const SpectralBasisFun*>(spbasis)->functions(x,v);
      else static_cast<const SpectralBasisInt*>(spbasis)->functions(x,v);
      if(op.conjugateUnknown()) v.toConj();
      Vector<K>* vp=nullptr;
      Vector<K> vv;
      if(shortcut) { val.resize(nbfun*nbc*nbc);  val*=0.; vp=&val;}
      else {vv.resize(nbfun*nbc*nbc); vp = &vv;}
      //extend v to vector
      typename Vector<K>::iterator itv=v.begin(), itval=vp->begin();
      for(; itv!=v.end(); ++itv)
      {
           *itval++=*itv;
           for(dimen_t i=1; i<nbc; i++) {itval+=nbc; *itval++=*itv;}
      }
      if(shortcut) return;
      //apply operator with vv, derivatives not used
      number_t dimop=op.dimsRes().first*op.dimsRes().second;
      val.resize(nbfun*nbc*nbc*dimop); val*=0.;
      std::vector<std::vector<K> > der(dim);
      std::vector<std::vector<K> > der2;    // NOT MANAGED
      if(op.hasFunction()) op.eval(x, vv, der, der2, dimfun, val, d,m, np);     //evaluate differential operator with function
      else                 op.eval(vv, der, der2, dimfun, val, d, m, np);       //evaluate differential operator
      return;
    }

  //vector case
  Vector<K> zero(dimfun, K(0));
  Vector<Vector<K> > v(nbfun,zero);
  if(spbasis->funcFormType()==_analytical) static_cast<const SpectralBasisFun*>(spbasis)->functions(x,v);
  else static_cast<const SpectralBasisInt*>(spbasis)->functions(x,v);
  if(op.conjugateUnknown()) v.toConj();

  if(shortcut)  // id and no function
    {
      val.resize(nbv);
      typename Vector<Vector<K> >::iterator itv=v.begin();
      typename Vector<K>::iterator itval=val.begin(), it;
      for(; itv!=v.end(); itv++)
        for(it=itv->begin(); it!=itv->end(); ++it, ++itval) *itval = *it;
      return;
    }

  Vector<K> vv(nbv);
  typename Vector<Vector<K> >::iterator itv=v.begin();
  typename Vector<K>::iterator itvv=vv.begin(), it;
  for(; itv!=v.end(); itv++)
    for(it=itv->begin(); it!=itv->end(); it++, itvv++) *itvv = *it;
  dimen_t d,m; //block size
  std::vector<std::vector<K> > der(dim);
  std::vector<std::vector<K> > der2;    //NOT MANAGED
  if(op.hasFunction()) op.eval(x, vv, der, der2, dimfun, val, d,m, np);     //evaluate differential operator with function
  else                 op.eval(vv, der, der2, dimfun, val, d, m, np);       //evaluate differential operator
}

/* ================================================================================================
                                      SP computation algorithm
   ================================================================================================ */
//SP computation of a scalar SuBilinearForm on a == unique domain ==
// ---- u space and v space have to be SP spaces --------

/*! compute SuTermMatrix for spectral unknowns u, v
    subf: a single unknown bilinear form defined on a unique domain (assumed), single integral
    v: reference to matrix entries of type T
    vt: to pass as template the scalar type (real or complex)
*/
template <>
template <typename T, typename K>
void ComputationAlgorithm<_SPComputation>::compute(const SuBilinearForm& subf, LargeMatrix<T>& mat, K& vt,
    Space* space_u_p, Space* space_v_p,
    const Unknown* u_p, const TestFct* v_p)
{
  if (subf.size() == 0) return; // void bilinear form, nothing to compute !!!
  trace_p->push("SuTermMatrix::computeSP");
  if(theVerboseLevel>0) std::cout<<"computing SP-SP term "<< subf.asString()<<", using "<< numberOfThreads()<<" threads: "<<std::flush;

  //retry space informations
  const Space* sp_u = subf.up()->space();  //space of unknown
  const Space* sp_v = subf.vp()->space();  //space of test function
  if (sp_u->typeOfSpace() != _spSpace) error("not_sp_space_type", sp_u->name());
  const SpectralBasis* basis_u = sp_u->spSpace()->spectralBasis(); //spectral basis
  const SpectralBasis* basis_v = basis_u;
  number_t nb_u = sp_u->dimSpace(); //space dimension
  number_t nb_v = nb_u;
  dimen_t nbc_u = u_p->nbOfComponents();
  dimen_t nbc_v = nbc_u;
  dimen_t dimfun_u = sp_u->dimFun();
  if (nbc_u > 1)
  {
    if (dimfun_u==1) dimfun_u = nbc_u;//case of vector dofs (scalar shape functions will be extended to vector shape functions)
    else error("fun_bad_dim", 1, dimfun_u);
  }
  dimen_t dimfun_v = dimfun_u;
  std::vector<number_t>* dofNum_u = new std::vector<number_t>(nb_u);
  std::vector<number_t>::iterator itd = dofNum_u->begin();
  for (number_t k = 1; k <= nb_u; k++, itd++) *itd = k;
  std::vector<number_t>* dofNum_v = dofNum_u;

  const GeomDomain* gdom = subf.begin()->first->asIntgForm()->domain(); //common domain of integrals
  const Function* mapTo_u=findMap(*gdom,*sp_u->domain());  //map function from domain to spectral basis domain
  const Function* mapTo_v=mapTo_u;

  bool same_interpolation = (sp_u == sp_v);
  if (!same_interpolation)
  {
    if (sp_v->typeOfSpace() != _spSpace) error("not_sp_space_type", sp_v->name());
    basis_v = sp_v->spSpace()->spectralBasis(); //spectral basis
    nb_v = sp_v->dimSpace(); //space dimension
    nbc_v = v_p->nbOfComponents();
    dimfun_v = sp_v->dimFun();
    if (nbc_v > 1)
    {
      if (dimfun_v==1) dimfun_v = nbc_v;//case of vector dofs (scalar shape functions will be extended to vector shape functions)
      else error("fun_bad_dim", 1, dimfun_v);
    }
    dofNum_v = new std::vector<number_t>(nb_v);
    for (number_t k = 1; k <= nb_v; k++, itd++) *itd = k;
    mapTo_v=findMap(*gdom,*sp_v->domain());  //map function from domain to spectral basis domain
  }

  std::vector<number_t> adrs(nb_u * nb_v, 0);
  mat.positions(*dofNum_v, *dofNum_u, adrs, true); //adresses in mat of elementary matrix stored as a row vector
  std::vector<number_t>::iterator itadrs = adrs.begin(), itk;

  const MeshDomain* dom = gdom->meshDomain(); //common domain of integrals
  if (dom == nullptr)  error("domain_notmesh", gdom->name(), words("domain type",gdom->domType()));

  //max derivative orders
  dimen_t ordu = 0, ordv = 0;
  for (cit_vbfp itf = subf.begin(); itf != subf.end(); itf++)
  {
    const IntgBilinearForm* ibf = itf->first->asIntgForm(); //basic bilinear form as simple integral
    ordu = std::max(ordu, dimen_t(ibf->opu().diffOrder()));   //order of u differential involved in operators
    ordv = std::max(ordv, dimen_t(ibf->opv().diffOrder()));   //order of v differential involved in operators
  }

  bool sym = same_interpolation && !(mat.sym == _noSymmetry); //symmetry flag

  //  //------------------------------------------
  //  //main loop on geomelements of domain
  //  //------------------------------------------
  //  #ifdef XLIFEPP_WITH_OMP
  //  #pragma omp parallel for
  //  #endif // XLIFEPP_WITH_OMP   to be thread safe,  duplicate mat and do reduction
  for (number_t k=0; k<dom->geomElements.size(); ++k) //main loop on elements of domain
  {
    GeomElement* gelt = dom->geomElements[k];  //geometric element
    ShapeType sh = gelt->shapeType();          //shape type of element
    MeshElement* melt = gelt->meshElement();
    if (melt == nullptr) melt = gelt->buildSideMeshElement(); //it is a side element with no meshElement structure, build one
    GeomMapData mapdata(melt);                         //structure to store jacobian
    number_t nb_ut = nb_u * nbc_u, nb_vt = nb_v * nbc_v;
    if (basis_u->funcFormType()==_analytical) basis_u->setGeomElementPointer(gelt);
    if (basis_v!=basis_u && basis_v->funcFormType()==_analytical) basis_v->setGeomElementPointer(gelt);

    //loop on basic bilinear forms
    for (cit_vbfp itf = subf.begin(); itf != subf.end(); itf++)
    {
      const IntgBilinearForm* ibf = itf->first->asIntgForm(); //basic bilinear form as simple integral
      const OperatorOnUnknown& op_u = ibf->opu();             //operator on unknown
      const OperatorOnUnknown& op_v = ibf->opv();             //operator on test function
      if(op_u.elementRequired() || op_v.elementRequired()) setElement(gelt);  // transmit gelt to thread data
      AlgebraicOperator aop = ibf->algop();                   //algebraic operator between differential operator
      K coef = complexToT<K>(itf->second);                    //coefficient of linear form
      bool same_operator = (op_u == op_v);
      const QuadratureIM* qim = dynamic_cast<const QuadratureIM*>(ibf->intgMethod());
      const Quadrature* quad = qim->getQuadrature(sh);        //quadrature rule pointer attached to integral
      number_t nbquad = quad->numberOfPoints();
      // bool invertJacobian = (op_u.diffOrder() > 0 || op_v.diffOrder() > 0);
      Matrix<K> matel(nb_vt, nb_ut, K(0));

      //loop on quadrature points
      for (number_t q = 0; q < nbquad; q++)
      {
        mapdata.computeJacobianMatrix(quad->point(q));          //compute jacobian at q th quadrature point
        mapdata.computeDifferentialElement();                   //compute differential element
        K alpha = coef * mapdata.differentialElement** (quad->weight(q));
        Point xq(quad->point(q), quad->dim());
        Point x = mapdata.geomMap(xq);                          //point in physical space
        Vector<K> val_u, val_v;
        Point xm=x;
        if (mapTo_u!=nullptr)(*mapTo_u)(x,xm);
        computeSPOperator(op_u, basis_u, xm, sp_u->valueType(), nb_u, dimfun_u, val_u);
        if (same_operator)
          tensorOpAdd(aop, val_u, nb_ut, val_u, nb_ut, matel, alpha);
        else
        {
          if (mapTo_v!=nullptr && mapTo_v!=mapTo_u)(*mapTo_v)(x,xm);
          computeSPOperator(op_v, basis_v, xm, sp_v->valueType(), nb_v, dimfun_v, val_v);
          tensorOpAdd(aop, val_v, nb_vt, val_u, nb_ut, matel, alpha);
        }
      }  //end quadrature loop

      //assembling matel in global matrix, matel is never a block matrix as mat is a block matrix in case of vector unknowns
      std::vector<number_t>::iterator itd_u, itd_v, it_ue;   //dof numbering iterators
      typename Matrix<K>::iterator itm = matel.begin(), itm_u;
      number_t incr = nbc_u * nbc_v * nb_u; //block row increment in matel
      number_t i = 0;
      //loop on (vector) dofs in u and v
      for (itd_v = dofNum_v->begin(); itd_v != dofNum_v->end() ; itd_v++, itm += incr, i++)
      {
        itm_u = itm;
        itk = itadrs + i * nb_u;
        for (itd_u = dofNum_u->begin(); itd_u != dofNum_u->end() ; itd_u++, itm_u += nbc_u, itk++)
        {
          if(!sym || *itd_v >= *itd_u) assemblyMat(mat(*itk), itm_u, nb_ut);
        }
      }
    } //end of bilinear forms loop
  } //end of elements loop

  delete dofNum_u;//clean temporary dofNum structure
  if (!same_interpolation) delete dofNum_v;
  if(theVerboseLevel>0) std::cout<<" done"<<eol<<std::flush;
  trace_p->pop();
}

} // end of namespace xlifepp
