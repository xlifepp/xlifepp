/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file otherComputation.hpp
  \author E. Lunéville
  \since 29 nov 2016
  \date 29 nov 2016

  \brief some computation tools using all the TERM stuff
*/

#ifndef OTHER_COMPUTATION_HPP
#define OTHER_COMPUTATION_HPP

#include "config.h"
#include "term.h"

namespace xlifepp
{

//! compute normals at some points of a GeomDomain
std::vector<Vector<real_t> > computeNormalsAt(const GeomDomain& dom, const std::vector<Point>& xs, ProjectorType pt = _noProjectorType,
    OrientationType orient=_undefOrientationType, const GeomDomain* gp=nullptr);

// ================================================================================================
/*!   compute integral representation on a set of points:
      intg_gamma op(K)(xi,y) aop op(u) dy
        op(K) : operator on a Kernel K
        op(u) : operator on a TermVector u (defined on gamma)
        aop: algebraic operator

   syntax examples:
        Vector<real_t> val;
        integralRepresentation(Points, intg(gamma,G * u), U, val);  //U values of unknown
        integralRepresentation(Points, intg(gamma,(grad_y(G)|_ny) * u, U, GaussLegendre,3), val);
        integralRepresentation(Points, intg(gamma,(grad_x(G)|_nx) * u, U, GaussLegendre,3), val);

        be cautious, returned values may be of type Vector<real_t>, Vector<complex_t>  and has to be consistent with computation */

//  ================================================================================================
//loop on points inside FE computation loop, see SuTermVector::computeIR
// xs: points where to evaluate IR
// lf: linear form describing the IR
// U: TermVector representing the "solution" where to apply the linear form
// val: the values of IR as a [v1(x1), v2(x1), ...,vn(x1), ... v1(xp), v2(xp), ...,vn(xp)]
// ns: vector of normals at point pts (empty if not required)

template<typename T>
Vector<T>& integralRepresentation(const std::vector<Point>& xs, const LinearForm& lf, const TermVector& U, Vector<T>& val,
                                                    const std::vector<Vector<real_t> >& ns = std::vector<Vector<real_t> >())
{
  trace_p->push("integralRepresentation(Vector<Point>,LinearForm, ...)");
  if(lf.isEmpty()) error("is_void","LinearForm");
  number_t nx=xs.size();
  if(nx==0) error("is_void","Vector<Point>");

  // get result structure
  ValueType vt=_real;
  StrucType st=_scalar;
  dimPair dims(0,0);
  for(cit_mulc it = lf.begin(); it != lf.end(); it++)  //loop on linear combinations of lf
    {
      const SuLinearForm& sulf = it->second;
      const Unknown* u=sulf.unknown();
      const SuTermVector* pU=U.subVector_p(u);
      if(pU==nullptr) pU=U.subVector_p(u->dual_p());      //try with dual (permissive)
      if(pU==nullptr) error("term_inconsistent_unknowns");
      if(vt==_real) vt = pU->valueType();
      for(cit_vlfp its=sulf.begin(); its!=sulf.end(); ++its)
        {
          IntgLinearForm* ilf=its->first->asIntgForm();
          dimPair dimopu=ilf->opu()->dimsRes();
          if(dims==dimPair(0,0)) dims=dimopu;
          else if(dimopu!=dims) error("operator_mismatch_dims", dims.first, dims.second, dimopu.first, dimopu.second);
          if(vt==_real) vt=ilf->valueType();
        }
    }
  if(dims.first > 1) st=_vector;
  if(dims.second > 1) st=_matrix;
  if(st==_matrix) error("scalar_or_vector");
  if(vt==_complex && valueType(val)==_real) error("c_to_r");
  val.resize(xs.size()*dims.first*dims.second);
  val*=0.;

  //compute integral representations
  for(cit_mulc it = lf.begin(); it != lf.end(); it++) //loop on linear combinations of lf
    {
      const SuLinearForm& sulf = it->second;
      const Unknown* u=sulf.unknown();
      TermVector cU(U);      // copy U, to preserve it in case of extension
      SuTermVector* pU=cU.subVector_p(u);
      if(pU==nullptr) pU=cU.subVector_p(u->dual_p());   //try with dual (permissive)
      //to prevent misalignment of U with sulf.space, extend U to sulf.space
      const Space* sp = sulf.space();
      if(sp!=pU->spacep())
        {
          std::vector<number_t> dofrenumber = renumber(sp, pU->spacep());
          if(dofrenumber.size() > 0) pU->entries()->extendEntries(dofrenumber, sp->dimSpace());
        }
      //compute integral representation for current SuLinearForm
      ValueType vtu=pU->valueType();
      StrucType stu=pU->strucType();
      VectorEntry* valU = pU->entries();
      bool killValU = false;
      if(stu!=_scalar) //move U to scalar
        {
          valU = pU->scalar_entries();
          if(valU==nullptr)
            {
              valU= new VectorEntry(*pU->entries());
              valU->toScalar();
              killValU = true;
            }
        }
      real_t r;
      complex_t c;
      SuTermVector* sut = new SuTermVector();
      if(vt==_real && vtu==_real)
        {
          Vector<real_t> vr;
          sut->computeIR(sulf, vr, r, xs, *valU->rEntries_p, &ns); //real result
          val+=vr;
        }
      else
      {
          Vector<complex_t> vc;
          if(vtu==_real) sut->computeIR(sulf, vc, c, xs, cmplx(*valU->rEntries_p),&ns);    //complex result
          else sut->computeIR(sulf, vc, c, xs, *valU->cEntries_p, &ns);
          val+=vc;
      }
      if(killValU) delete valU;
    }
  trace_p->pop();
  return val;
}

//! compute integral representation on a set of points returning a vector of vector
template<typename T>
Vector<Vector<T> >& integralRepresentation(const std::vector<Point>& xs, const LinearForm& lf, const TermVector& U, Vector<Vector<T> >& val,
                                           const std::vector<Vector<real_t> >& ns = std::vector<Vector<real_t> >())
{
  if(lf.isEmpty()) error("is_void","LinearForm");
  trace_p->push("integralRepresentation(Vector<Point>,LinearForm, ...)");
  Vector<T> scal;
  integralRepresentation(xs, lf, U, scal, ns);
  //move to vector representation
  number_t nx=xs.size();
  dimPair dims= lf.begin()->second.begin()->first->asIntgForm()->opu()->dimsRes();
  number_t d=dims.first*dims.second;
  typename Vector<T>::iterator its=scal.begin();
  val.resize(nx);
  typename Vector<Vector<T> >::iterator itv=val.begin();
  for(number_t k=0; k<nx; k++, ++itv, its+=d) *itv=Vector<T>(its,its+d);
  trace_p->pop();
  return val;
}


//! points are given from a GeomDomain, return values and points in input arguments list (pts)
template<typename T>
Vector<T>& integralRepresentation(const GeomDomain& dom, const LinearForm& lf, const TermVector& U, Vector<T>& val,std::vector<Point>& xs)
{
  trace_p->push("integralRepresentation(GeomDomain,LinearForm, ...)");
  if(dom.domType()==_meshDomain) xs=dom.meshDomain()->nodes();
  else error("domain_notmesh", dom.name(), words("domain type", dom.domType()));
  std::vector<Vector<real_t> > ns;
  if(lf.xnormalRequiredByOpKernel()) ns = computeNormalsAt(dom,xs);
  integralRepresentation(xs, lf, U, val, ns);
  trace_p->pop();
  return val;
}

// compute int_dom opker(x,y) * tv(y) on a domain, return values and points in input arguments list (pts)
template<typename T>
Vector<T>& integralRepresentation(const GeomDomain& dom, const std::pair<LinearForm,const TermVector*> lftv,
                                  Vector<T>& val, std::vector<Point>& pts)
{return integralRepresentation(dom, lftv.first,*lftv.second, val, pts);}



// compute int_dom opker(x,y) * tv(y) on points, return values and points in input arguments list (pts)
template<typename T>
Vector<T>& integralRepresentation(const std::vector<Point>& xs, const std::pair<LinearForm,const TermVector*> lftv, Vector<T>& val,
                                  const std::vector<Vector<real_t> >& ns = std::vector<Vector<real_t> >())
{return integralRepresentation(xs, lftv.first, *lftv.second, val, ns);}

//! integralRepresentation, points are given from a GeomDomain, return a TermVector related to unknown u
TermVector integralRepresentation(const Unknown&, const GeomDomain&, const LinearForm&, const TermVector&, const string_t& nam="IR");

// compute int_dom opker(x,y) * tv(y) on a domain, return a TermVector related to unknown u
inline TermVector integralRepresentation(const Unknown& u, const GeomDomain& dom, std::pair<LinearForm, const TermVector*> lftv, const string_t& nam="IR")
{return integralRepresentation(u, dom, lftv.first, *lftv.second, nam);}

// //=======================================================================================================
// // IR alternate algorithm: loop on points outside FE computation
// // use standard FE machinery; kernel G(x,y) is seen as the function Gx(y)
// // !!! works only for quadrature rules and does not take into account singularity !!!
// //=======================================================================================================

// // pts: points where to evaluate IR
// // lf: linear form describing the IR
// // U: TermVector representing the "solution" where to apply the linear form
// // val: the values of IR
// // ns: vector of normals at point pts (empty if not required)
// //   return a well formatted TermVector containing the IR
// template<typename T>
// Vector<T>& integralRepresentationF(const std::vector<Point>& pts, const LinearForm& lf, const TermVector& U,
//                                   Vector<T>& val, const std::vector<Vector<real_t> >& ns = std::vector<Vector<real_t> >())
// {
//  trace_p->push("integralRepresentationF(Vector<Point>,LinearForm, ...)");
//  //check consistancy
//  if(lf.isEmpty()) error("is_void","LinearForm");
//  if(!lf.singleUnknown()) error("term_not_suterm");
//  const Unknown* u=&lf.firstUnknown();
//  const SuTermVector* pU=U.subVector_p(u);
//  if(pU==nullptr) pU=U.subVector_p(u->dual_p());  //try with dual (permissive)
//  if(pU==nullptr) error("term_inconsistent_unknowns");

//  const SuLinearForm& sulf=lf.first();
//  std::vector<Point>::const_iterator itp = pts.begin();
//  val.resize(pts.size());
//  typename Vector<T>::iterator itv=val.begin();
//  for(;itp!=pts.end(); itp++, itv++)
//  {
//    for(cit_vlfp itf = sulf.begin(); itf != sulf.end(); itf++)  //set X points
//    {
//      const OperatorOnUnknown* opu=itf->first->asIntgForm()->opu();
//      if(opu->leftOperand()!=nullptr) opu->leftOperand()->setX(*itp);
//      if(opu->rightOperand()!=nullptr) opu->rightOperand()->setX(*itp);
//    }
//    TermVector Vi(lf);
//    compute(Vi);
//    *itv=complexToT<T>(dotRC(Vi,U));
//  }
//  trace_p->pop();
//  return val;
// }

// TermVector integralRepresentationF(const Unknown&, const GeomDomain&, const LinearForm&, const TermVector&); //!< evaluate integral representation on a domain

// //points are given from a GeomDomain, return values and points in input arguments list (pts)
// template<typename T>
// Vector<T>& integralRepresentationF(const GeomDomain& dom, const LinearForm& lf, const TermVector& U,
//                                   Vector<T>& val, std::vector<Point>& pts)
// {
//  trace_p->push("integralRepresentationF(GeomDomain,LinearForm, ...)");
//  if(dom.domType()==_meshDomain) pts=dom.meshDomain()->nodes();
//  else error("domain_notmesh", dom.name(), words("domain type",dom.domType()));
//  integralRepresentationF(pts, lf, U, val);
//  trace_p->pop();
//  return val;
// }
}

#endif // OTHER_COMPUTATION_HPP
