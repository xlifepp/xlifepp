/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file otherComputation.cpp
  \author E. Lunéville
  \since 29 nov 2016
  \date 29 nov 2016

  \brief some computation tools using all the TERM stuff
*/

#include "otherComputation.hpp"

namespace xlifepp
{
/* ================================================================================================
 compute the normals at points of a GeomDomain
 xs: points where to compute normals
 dom: pointer to the GeomDomain (has to be a MeshDomain)
 pt: type of projection used

 for future usage
 orient: orientation of the normal (_undefOrientationType, _towardsInfinite, outwardsInfinite, _towardsDomain,_outwardsDomain)*
 gp: pointer to the domain when _towardsDomain or _outwardsDomain have been chosen

 if element normals have not yet been computed on GeomDomain, compute them using orientation passed

 if pt = _noProjectorType (well suited for non smooth geometry)
    for each point x of xs
      locate the element of GeomDomain that contains x with a tolerance
      take as normal at this point the normal given by the element normal

 if pt = _L2Projector (well suited for smooth geometry)
    Perform the L2 projection from P0 to P1 of the element normals (P0)
    compute normals at xs by interpolation of P1 normals
================================================================================================*/
std::vector<Vector<real_t> > computeNormalsAt(const GeomDomain& dom, const std::vector<Point>& xs, ProjectorType pt,
    OrientationType orient, const GeomDomain* gp)
{
  trace_p->push("computeNormalsAt(vector<Point>, GeomDomain)");
  if (dom.meshDomain()==nullptr) error("domain_notmesh",dom.name());
  number_t np=xs.size();
  if (np==0) error("is_void","xs");

  const MeshDomain& mdom = *dom.meshDomain();
  if (!mdom.normalComputed) mdom.setNormalOrientation(orient,gp);   //set orientation of normal on elements
  std::vector<Vector<real_t> > ns(np);

  //case of L2 projection
  if (pt==_L2Projector)
  {
    dimen_t d=dom.spaceDim();
    //define P1 and P0 space
    Space sP1(_domain=dom, _interpolation=_P1, _name="P1_"+dom.name(), _optimizeNumbering);
    Unknown n1(sP1,"n1",d); TestFunction tnk(n1,"tn1",d);
    Space sP0(_domain=dom, _interpolation=_P0, _name="P0_"+dom.name(), _notOptimizeNumbering);
    Unknown n0(sP0,"n0",d);
    //fill normal P0 in a TermVector
    TermVector N0(n0, dom, Vector<real_t>(d,0.),"normalsP0");
    Vector<Vector<real_t> >::iterator itn0=N0.begin()->second->entries()->beginrv();
    for (number_t k=0; k<sP0.nbOfElements(); k++, ++itn0)
    {
      *itn0 = sP0.element_p(k)->geomElt_p->meshElement()->geomMapData_p->normalVector;
    }
    //compute projection
    TermVector N1=projection(N0,sP1,d,_L2Projector, _nokeep);
    //fill in vector of normals at xs
    #pragma omp for
    for (number_t k=0; k<np; ++k)
    {
      ns[k] = N1.evaluate(xs[k]).value<Vector<real_t> >();
    }
    trace_p->pop();
    return ns;
  }

  // default: direct interpolation
  bool firstorder = dom.mesh()->order()==1;
  if (!firstorder) error("free_warning", "computeNormalsAt uses first order approximate of a domain which is of order "+tostring(dom.mesh()->order()));
  if (mdom.kdtree.isVoid()) mdom.buildKdTree();       //build kdtree on fly
  #pragma omp for
  for (number_t k=0; k<np; ++k)
  {
    GeomElement* gelt=mdom.locate(xs[k]);
    if (gelt==nullptr) error("domain_point_not_located", tostring(pt), dom.name());
    MeshElement* melt=gelt->meshElement();
    if (melt==nullptr) error("geoelt_not_meshelement");
    ns[k] = melt->geomMapData_p->normalVector;
  }
  trace_p->pop();
  return ns;
}

/* =======================================================================================================
   compute integral representation on dof of Lagrange interpolation on a FE domain:
      intg_gamma op(K)(xi,y) aop op(U) dy
        op(G) : operator on a Kernel K
        op(U) : operator on a TermVector U (defined on gamma)
        aop: algebraic operator

   syntax example:
        TermVector R=integralRepresentation(v,sigma,intg(gamma,G(_y) * u), U);  //U values of unknown
=======================================================================================================*/
TermVector integralRepresentation(const Unknown& v, const GeomDomain& dom, const LinearForm& lf, const TermVector& U, const string_t& nam)
{
  trace_p->push("integralRepresentation(Unknown, GeomDomain, ...)");
  //check consitancy
  bool visLagrange = (v.space()->typeOfSpace() == _feSpace && v.space()->feSpace()->interpolation()->type==_Lagrange);
  if (!visLagrange) error("lagrange_fe_space_only",v.space()->name());
  if (lf.isEmpty()) error("is_void","LinearForm");

  Space* spacev = v.space();
  if (spacev->domain()!=&dom)
  {
    spacev = Space::findSubSpace(&dom,v.space());
    if (spacev == nullptr) spacev = new Space(dom,*v.space());
  }
  //get dof coordinates
  std::vector<Point> xs;
  if (spacev->isFE()) xs=dofCoords(*spacev,dom);
  else
  {
    switch (dom.domType())
    {
      case _meshDomain:  xs = dom.meshDomain()->nodes(); break;
      case _pointsDomain:  xs = (static_cast<const PointsDomain*>(&dom))->points; break;
      default: error("domain_type_not_handled", dom.domTypeName());
    }
  }

  //get normals at pts if required
  std::vector<Vector<real_t> > ns;
  if (lf.xnormalRequiredByOpKernel()) ns = computeNormalsAt(dom,xs);

  // get result structure
  ValueType vt=_real;
  StrucType st=_scalar;
  dimPair dims(0,0);
  for (cit_mulc it = lf.begin(); it != lf.end(); it++)  //loop on linear combinations of lf
  {
    const SuLinearForm& sulf = it->second;
    const Unknown* u=sulf.unknown();
    const SuTermVector* pU=U.subVector_p(u);
    if (pU==nullptr) pU=U.subVector_p(u->dual_p());      //try with dual (permissive)
    if (pU==nullptr) error("term_inconsistent_unknowns");
    if (vt==_real) vt = pU->valueType();
    for (cit_vlfp its=sulf.begin(); its!=sulf.end(); ++its)
    {
      IntgLinearForm* ilf=its->first->asIntgForm();
      dimPair dimopu=ilf->opu()->dimsRes();
      if (dims==dimPair(0,0)) dims=dimopu;
      else if (dimopu!=dims) error("operator_mismatch_dims", dims.first, dims.second, dimopu.first, dimopu.second);
      if (vt==_real) vt=ilf->valueType();
    }
  }
  if (dims.first > 1) st=_vector;
  if (dims.second > 1) st=_matrix;
  if (st==_matrix) error("scalar_or_vector");
  if (dims.first!=v.nbOfComponents()) error("bad_size", "v", v.nbOfComponents(), dims.first);

  //create SuTermVector result and TermVector result
  SuTermVector* sut = nullptr;
  string_t nam_sut = nam+"_"+v.name();
  VectorEntry* vals = nullptr;
  if (st==_scalar)
  {
    if(vt==_real) sut = new SuTermVector(v, dom, 0., nam_sut, false, false);
    else sut = new SuTermVector(v, dom, complex_t(0.), nam_sut, false, false);
    vals = sut->entries();
  }
  else
  {
    if(vt==_real) sut = new SuTermVector(v, dom, Vector<real_t>(dims.first,0.), nam_sut, false, false);
    else sut = new SuTermVector(v, dom, Vector<complex_t>(dims.first,complex_t(0.)), nam_sut, false, false);
    sut->toScalar();  //move to scalar
    vals = sut->scalar_entries();
  }
  TermVector res(sut,nam);

  //compute integral representations
  for (cit_mulc it = lf.begin(); it != lf.end(); it++)  //loop on linear combinations of lf
  {
    const SuLinearForm& sulf = it->second;
    const Unknown* u=sulf.unknown();
    TermVector cU(U);      // copy U, to preserve it in case of extension
    SuTermVector* pU=cU.subVector_p(u);
    if (pU==nullptr) pU=cU.subVector_p(u->dual_p());   //try with dual (permissive)
    //to prevent misalignment of U with sulf.space, extend U to sulf.space
    const Space* sp = sulf.space();
    if (sp!=pU->spacep())
    {
      std::vector<number_t> dofrenumber = renumber(sp, pU->spacep());
      if(dofrenumber.size() > 0) pU->entries()->extendEntries(dofrenumber, sp->dimSpace());
    }
    //compute integral representation for current SuLinearForm
    ValueType vtu=pU->valueType();
    StrucType stu=pU->strucType();
    VectorEntry* valU = pU->entries();
    bool killValU = false;
    if (stu!=_scalar) //move U to scalar
    {
      valU = pU->scalar_entries();
      if (valU==nullptr)
      {
        valU = pU->entries()->toScalar();
        killValU = true;
      }
    }
    real_t r;
    complex_t c;
    if (vt==_real && vtu==_real)
    {
      Vector<real_t> vr;
      sut->computeIR(sulf, vr, r, xs, *valU->rEntries_p, &ns); //real result
      *vals->rEntries_p+=vr;
    }
    else
    {
      Vector<complex_t> vc;
      if (vtu==_real) sut->computeIR(sulf, vc, c, xs, cmplx(*valU->rEntries_p),&ns);    //complex result
      else sut->computeIR(sulf, vc, c, xs, *valU->cEntries_p,&ns);
      *vals->cEntries_p+=vc;
    }
    if(killValU) delete valU;
  }

  //return to vector representation
  if (st==_vector) sut->toVector();
  trace_p->pop();
  return res;
}

}
