/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file termUtils.hpp
  \author N. Kielbasiewicz
  \since 27 feb 2013
  \date 1 mar 2013

  \brief Definition of term utility functions
*/

#ifndef TERM_UTILS_HPP
#define TERM_UTILS_HPP

#include "config.h"
#include "largeMatrix.h"
#include "space.h"

namespace xlifepp
{

//data structure to store IE computation parameters
class IEcomputationParameters
{
  public:
    dimen_t dim_sp=2;                       //!< space dimension (elt->geomElt_p->mesh()->spaceDim())
    bool extend_u=false, extend_v=false;    //!< true if u/v lives on a extended domain
    number_t side_u=0, side_v=0;            //!< side numbers when a extended domain (0 when not used)
    FEMapType femt_u=_standardMap,
              femt_v=_standardMap;          //!< type of map type (_standardMap,_contravariantPiolaMap,_covariantPiolaMap)
    ShapeType sh_u=_noShape, sh_v=_noShape; //!< shape of elements
    number_t ord_u=0, ord_v=0;              //!< degree of u/v interpolation
    dimen_t dimf_u=1, dimf_v=1;             //!< dimension of u/v shape functions of reference element
    dimen_t dimfp_u=1, dimfp_v=1;           //!< dimension of u/v shape functions of physical element
    bool opisid_u=true, opisid_v=true;      //!< true if operator on u/v is identity
    bool hasf_u=false, hasf_v=false;        //!< true if operator on u/v requires to evaluate a function
    dimen_t difforder_u=0, difforder_v=0;   //!< order of differential operator
    bool scalar_k=true;                     //!< true if scalar kernel
    bool isP0=true;                         //!< true if u and v interpolations are P0
    bool isId=true;                         //!< true if operator on u and v are identity
    number_t nb_bf=1;                       //!< number of bilinear forms
    MeshElement *melt_u=nullptr,
                *melt_v=nullptr;            //!< pointer to mesh elements
    bool rotsh_u=false, rotsh_v=false;      //!< true if dof rotation has to be done
    bool changeSign_u=false,
         changeSign_v=false;                //!< if true change signs of u/v dofs according to sign_u/sign_v
    Vector<real_t> *sign_u=nullptr,
                   *sign_v=nullptr;         //!< pointer to dof sign vectors
    bool knor_x=false, knor_y=false;        //!< true if x-normal/y-normal is required by kernel;
    bool useAux=false;                      //!< true if v-shapevalues must be stored in relt_v->qshvs_aux
    bool isoGeo=false;                      //!< true if computation uses iso-geometric way
    const Parametrization *isoPar_x=nullptr,
                          *isoPar_y=nullptr;//!< pointers to x or y-parametrization if defined
    bool isUptodate=false;                  //!< true if up to date

    IEcomputationParameters() {}
    friend std::ostream& operator<<(std::ostream& out, const IEcomputationParameters & iep);
};

//storage stuff
MatrixStorage* buildStorage(const Space &rs, const Space &cs, StorageType st, AccessType at, StorageBuildType bt);   //!< build a storage from a pair of Space
MatrixStorage* buildStorage(StorageType st, AccessType at, StorageBuildType bt, number_t dimr, number_t dimc, const string_t& id="");     //!< build a storage from  type and dimension, no allocation of pointers (void matrix)
MatrixStorage* buildStorage(StorageType st, AccessType at, StorageBuildType bt, number_t dimr, number_t dimc, const std::vector<std::vector<number_t> >& indices, const string_t& id=""); //!< build a storage from  type, dimension and column indices (vector of vectors)
MatrixStorage* buildStorage(StorageType st, AccessType at, StorageBuildType bt, number_t dimr, number_t dimc, const std::vector<std::set<number_t> >& indices, const string_t& id="");    //!< build a storage from  type, dimension and column indices (vector of sets)
MatrixStorage* updateStorage(MatrixStorage& stm, const std::vector<number_t> rows, const std::vector<number_t> cols,
                             StorageType st, AccessType at, bool overwrite=false);  //!< update storage for adding dense submatrix given by its row and column indices

//interpolation stuff
void interpolatedNormals(Space& sp, std::vector<Vector<real_t> >& ns); //!< compute interpolated normals on Lagrange Dofs of space sp
TermVector normalsOn(GeomDomain& dom, const Unknown& u); //!< compute normals of a side domain using interpolation given by an unknown

// special cast function used by FEComputation's
real_t toRealComplex(const Vector<real_t>&,const complex_t&);
complex_t toRealComplex(const Vector<complex_t>&,const complex_t&);
real_t toRealComplex(const Vector<Vector<real_t> >&,const complex_t&);
complex_t toRealComplex(const Vector<Vector<complex_t> >&,const complex_t&);

//precomputation
void preComputationIE(const GeomDomain& dom, const Space* subsp, std::set<Quadrature*>& quads,
                      dimen_t nbc, bool useAux, bool der1, bool der2, bool nor,
                      bool extendedDomain=false, bool isogeo=false); //!< precompute geometry data and shape values on any element for any quadrature involved

} //end of namespace xlifepp

#endif // TERM_UTILS_HPP

