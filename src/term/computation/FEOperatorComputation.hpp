/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file FEOperatorComputation.hpp
  \author E. Lunéville
  \since 8 sep 2014
  \date 8 sep 2014

  \brief Implementation of template FE operator computation functions

  this file is included by SuTermMatrix.hpp and SuTermVector.hpp
  do not include it elsewhere
*/

/* ================================================================================================
                                      tools
   ================================================================================================ */
/*!compute values of operator on unknown at quadrature points, say
    op(wi)(xq)   with (wi) i=1,n the shape functions and (xq) q=1,p the quadrature points

 not otpimized, see below for optimized version

 gelt: geometrical element in physical space (support of the integral)
 elt: finite element in physical space
 op: operator on unknown
 isoGeometric : if true use isoGeometric approximation
 dom : related mesh domain pointer
 qim: quadrature handling quadrature points in reference space
 val: computed values as a vector of vectors: val(q) = (op(wi)(xq)) i=1,n
 phyPoints: quadrature points in physical space
 sidenum: when extended computation, the side number of gelt in elt; when 0 not used

 NOTES: In case of extension of FE, gelt is not the geometric support of elt !!!
         the result is multiplied by quadrature weights and differential element !!!
*/
template <typename K>
void computeOperatorByQuadrature(GeomElement* gelt, const Element& elt, const OperatorOnUnknown& op, const QuadratureIM& qim,
                                 bool isoGeometric, const MeshDomain* dom, std::vector<Vector<K> >& val, std::vector<Point >& phyPoint,
                                 number_t sidenum = 0)
{
  //thePrintStream<<(*gelt)<<eol<<elt<<eol<<"qim="<<qim<<eol<<"domain="<<dom->name()<<" isogeo="<<(isoGeometric?"true":"false")<<"sidenum="<<sidenum<<eol<<std::flush;
  bool ext = sidenum>0;                                //extension flag
  dimen_t ord = op.diffOrder();                        //differential operator order
  dimen_t dimf = op.unknown()->space()->dimFun();      //dimension of shape functions
  dimen_t nbc=op.unknown()->nbOfComponents();          //number of components of unknown
  dimen_t dim=gelt->spaceDim();                        //physical dimension
  if(nbc>1) dimf=nbc;                                  //case of vector dofs (scalar shape functions will be extended to vector shape functions)

  RefElement* relt = elt.refElt_p;                     //reference element related to element
  MeshElement* melt = gelt->meshElement();             //mesh element related to
  if(melt == nullptr) melt = gelt->buildSideMeshElement();   //it is a side element with no meshelement structure, build one
  bool linearMap = melt->linearMap;

  MeshElement* melt_elt = melt;  //no extension
  if(ext) melt_elt = elt.geomElt_p->meshElement();        // extension: melt_elt different from melt

  ShapeType sh = gelt->shapeType();                       //shape type of geometric element
  Quadrature* quad = qim.getQuadrature(sh);               //retry quadrature object
  dimen_t dq = quad->dim();                               //dimension of quadrature points
  number_t nq = quad->numberOfPoints();                   //number of quadrature points
  std::vector<ShapeValues>* shv = qim.getShapeValues(sh); //retry shape values at quadrature points

  bool normalRequired = op.normalRequired();
  bool tangentRequired= op.tangentRequired();
  bool hasKernel = op.hasKernel();
  bool hasFun = op.hasFun();

  // Note: when extension, the quadrature points on side ref element (dim d) has to be mapped to the extended ref element (dim d+1)
  //        now, there is no structure to store all the shape values on all sides of the extended ref element, so they are recomputed
  //        in future, find a way to store it in order to avoid re-computation
  if(shv == nullptr || ext) //shape values on ref element are not computed or have to be recomputed
    {
      shv = new std::vector<ShapeValues>(nq,ShapeValues(*relt, ord>0, ord>1));
      std::vector<real_t>::const_iterator itp = quad->point();
      std::vector<ShapeValues>::iterator itshv = shv->begin();
      GeomElement* gelt_ext = nullptr;
      if(ext) gelt_ext = elt.geomElt_p;
      for(number_t i = 0; i < nq; i++, itp += dq, itshv++)
        {
          if(ext) {
              std::vector<real_t> p_ext = gelt_ext->geomRefElement()->sideToElt(sidenum,itp);
              //thePrintStream<<"p_ext="<<p_ext<<eol;
              relt->computeShapeValues(p_ext.begin(), *itshv, ord > 0, ord>1);  // extension
          }
          else { relt->computeShapeValues(itp, *itshv, ord > 0, ord>1); } //no extension
          if(nbc>1) itshv->extendToVector(nbc);    //extend scalar shape functions to nbc vector shape functions
        }
      if(!ext) qim.setShapeValues(sh, shv); //if no extension, save shapevalues to qim
    }
  //thePrintStream<<"shv="<<(*shv)<<eol;
  //compute vector of dof sign for particular element (Nedelec, Raviart, ....)
  number_t nbfun = relt->nbDofs()*nbc;// number of shape functions
  bool changeSign=false;
  Vector<real_t>* sign=nullptr;
  if(relt->dofCompatibility == _signDofCompatibility)
    {
        sign=&elt.getDofSigns();
        changeSign = sign->size()!=0;
    }

  //rotate shape values in case of dofs on face with tangent vectors basis (case of Nedelec Edge FE in 3d of order >1)
  if(relt->rotateDof)
  {
      std::vector<ShapeValues>* rotshv = new std::vector<ShapeValues>(*shv);  //copy original shape values
      std::vector<ShapeValues>::iterator itshv = rotshv->begin();
      std::vector<number_t> ns = melt_elt->verticesNumbers();
      for(;itshv!=rotshv->end();++itshv) relt->rotateDofs(ns, *itshv, ord > 0, ord>1);
      shv = rotshv;
      //thePrintStream<<"rotated shv : "<<(*shv)<<eol<<std::flush;
  }

  //compute values of operator for each shape function at quadrature points
  bool diffComputation = true;
  if(melt->geomMapData_p==nullptr) melt->geomMapData_p=new GeomMapData(melt); //data structure for geometric transform
  GeomMapData* mapdata = melt->geomMapData_p;
  GeomMapData* mapdata_elt = mapdata;
  if(isoGeometric)
  {
     mapdata->isoPar_p = &dom->parametrization();
     mapdata->useParametrization = true;
     mapdata->useIsoNodes = true;
     setElement(gelt);
     linearMap=false;
  }
  if(ext)  //allocate a new mapadata related to melt_elt, does not handle isoGeometric approximation
  {
      if(melt_elt->geomMapData_p==nullptr) melt_elt->geomMapData_p=new GeomMapData(melt_elt); //data structure for geometric transform
      mapdata_elt = melt_elt->geomMapData_p;
  }
  val.resize(nq), phyPoint.resize(nq);                                  //resize output vectors
  std::vector<ShapeValues>::iterator itsh = shv->begin();               //shapevalues at first quadrature point
  std::vector<real_t>::const_iterator itp = quad->point();              //first point of quadrature for v
  std::vector<real_t>::const_iterator itw = quad->weights().begin();    //first weight of quadrature for v
  typename std::vector<Vector<K> >::iterator itv = val.begin();         //iterator to val
  std::vector<Point >::iterator itphy = phyPoint.begin();               //iterator to phyPoint
  dimen_t d,m;  //block size
  bool evalfun= op.hasFunction();
  number_t dimpf = dimf;                                                //dimension of shape functions of physical element
  for(; itphy != phyPoint.end(); itphy++, itp += dq, itsh++, itv++, itw++)     //loop on quadrature points for v
    {
      // compute shape values in physical space
      ShapeValues* sv = new ShapeValues(*itsh);                       //copy to preserve reference shape values
      if(diffComputation || !linearMap)                               //differential elements are computed once if linear element
        {
          mapdata->computeJacobianMatrix(itp);                        //compute jacobian at quadrature point
          mapdata->computeDifferentialElement();                      //compute differential element at quadrature point
          if(normalRequired)
          {
            mapdata->computeOrientedNormal();                         //compute oriented normal
            if(hasFun) setNx(&mapdata->normalVector);                 //update normal n in thread data
            if(hasKernel)                                             //update normal nx, ny
            {
               setNx(&mapdata->normalVector);                         //update x-normal in thread data
               setNy(&mapdata->normalVector);                         //update y-normal in thread data
            }
          }
          if(tangentRequired)
          {
            mapdata->computeTangentVector();                          //compute tangent vector(s)
            if(hasFun)
            {
               setTx(&mapdata->tangentVector);                        //update tangent t in thread data
               if(dim==3) setBx(&mapdata->bitangentVector);           //update bi tangent t in thread data
            }
            if(hasKernel)                                             //update x/y-tangents and x/y-bitangent
            {
               setTx(&mapdata->tangentVector);                        //update x-tangent in thread data
               setTy(&mapdata->tangentVector);                        //update y-tangent in thread data
               if(dim==3)
               {
                  setBx(&mapdata->bitangentVector);                   //update x-bitangent in thread data
                  setBy(&mapdata->bitangentVector);                   //update y-bitangent in thread data
               }
            }
          }
          if(ord > 0 || relt->mapType==_covariantPiolaMap)  mapdata_elt->invertJacobianMatrix();
          diffComputation=!linearMap;
        }
      switch(relt->mapType)                                           //map reference shapevalues to physical space
        {
          case _contravariantPiolaMap: sv->contravariantPiolaMap(*itsh, *mapdata_elt, ord>0, ord>1); break;
          case _covariantPiolaMap: sv->covariantPiolaMap(*itsh, *mapdata_elt, ord>0, ord>1); break;
          case _MorleyMap: sv->Morley2dMap (*itsh, *mapdata_elt, ord>0, ord>1); break;
          case _ArgyrisMap:sv->Argyris2dMap(*itsh, *mapdata_elt, ord>0, ord>1); break;
          default: sv->map(*itsh, *mapdata_elt, ord>0, ord>1);
        }
      dimpf = sv->w.size()/nbfun;                                     //dimension of physical shape functions, may be differ from dimf!
      if(changeSign) sv->changeSign(*sign, dimpf, ord>0, ord>1);      //change sign of shape functions according to sign vector
      //thePrintStream<<"sign="<<(*sign)<<eol;

      //evaluate operator
      *itphy = mapdata->geomMap(itp);                                 //quadrature point in physical space
      if(evalfun) op.eval(*itphy, sv->w, sv->dw, sv->d2w, dimpf, *itv, d, m,
                          &mapdata->normalVector);                    //evaluate differential operator with function
      else  op.eval(sv->w, sv->dw, sv->d2w, dimpf, *itv, d, m,
                    &mapdata->normalVector);                          //evaluate differential operator
      *itv *= mapdata->differentialElement** itw;                     //multiply by differential element * quadrature weight

//      thePrintStream<<"   x="<<*itphy<<eol<<"   n="<<mapdata->normalVector<<eol
//                    <<"   difelt="<<mapdata->differentialElement<<eol
//                    <<"   ref shape values="<<itsh->w<<eol
//                    <<"        shapevalues="<<sv->w<<eol
//                    <<"                val="<<*itv<<eol;
       delete sv;
    }
    if(relt->rotateDof) delete shv;
}

// ----------------------------------------------------------------------------------------------------------------
/*!compute values of operator on unknown at quadrature points, say
    op(wi)(xq)   with (wi) i=1,n the shape functions and (xq) q=1,p the quadrature points

 Optimized version, assuming precomputation of shapevalues,  quadrature points in physical space, ... see precomputeIE in TermUtils

 elt: finite element in physical space
 isP0   : true if P0 approximation on first order simplex geometric element
 op: operator on unknown
 isId: true if op is Id
 quad: number of quadrature points
 dq: dimension of quadrature points
 shv: pointer to shapevalues in reference space
 phyPoints: quadrature points in physical space
 ord: element order
 dimf: dimension of shape functions of ref element
 firstOrderElt: true if first order simplex geometric element (no update of jacobian)
 nbc: number of components of unknown
 der_ord: differential operator order
 nor: true if normal is required
 hasFun: true if operator involves function
 hasKer: true if operator involves kernel
 mapsh: true if the mapping of shapevalues has to be done
 mapType: type of shapevalues mapping (standard, contravariant Piola, covariant Piola)
 changeSign: true if signe change according to sign vector, has to be done
 sign: sign vector used by edge elements
 difelt: differential element
 gmap: pointer to the geometrical structure
 x: point used in operator involving a kernel K(x,y)
 ny: y-normal vector involved in operator (may be null)
 nx: x-normal vector involved in operator (may be null)
 sidenum: when extended computation, the side number of gelt in elt; when 0 not used

 Note: normal vectors required by Kernel has to be transmitted to Kernel object before

 val: computed values as a vector of vectors: val(q) = (op(wi)(xq)) i=1,n
 d,m: block size in val op(wi)(xq) = [op(wi)(xq)_1 op(wi)(xq)_2, ... op(wi)(xq)_d]       vector case m=1
                          op(wi)(xq) = [op(wi)(xq)_11 op(wi)(xq)_12, ... op(wi)(xq)_1n
                                        op(wi)(xq)_21 op(wi)(xq)_22, ... op(wi)(xq)_2n
                                        ...                                                matrix case m>1, n=d/m
                                        op(wi)(xq)_m1 op(wi)(xq)_m2, ... op(wi)(xq)_mn ]

 NOTE: the result is multiplied by quadrature weights and differential element !!!
*/
// ----------------------------------------------------------------------------------------------------------------
template <typename K>
void computeOperatorByQuadrature(const Element& elt, bool isP0, const OperatorOnUnknown& op, bool isId, bool opkerisId,
                                 Quadrature* quad, number_t nbq, dimen_t dq, std::vector<ShapeValues>* shv, const std::vector<Point>& phyPoints,
                                 dimen_t ord , dimen_t dimf, dimen_t nbc, bool firstOrderElt, number_t der_ord, bool nor, bool hasFun, bool hasKer,
                                 bool mapsh, FEMapType mapType, bool changeSign, Vector<real_t>* sign, real_t difElt, GeomMapData* gmapdata,
                                 const Point& x, Vector<real_t>* nx, Vector<real_t>* ny, std::vector<Vector<K> >& val, dimen_t& d, dimen_t& m, number_t sidenum=0)
{
  bool evalfun = hasFun || hasKer;
  //get references on quad data and physical points
  std::vector< real_t >::const_iterator itw = quad->weights().begin(), itwe = quad->weights().end(); // iterator on quadrature weights
  std::vector< real_t >::const_iterator itq = quad->point();                                         // iterator on quadrature points
  std::vector<Point>::const_iterator itp = phyPoints.begin();            //quadrature points in physical space
  typename std::vector<Vector<K> >::iterator itv = val.begin();          //iterator to val

  // --------- scalar PO shortcut for Kernel operator, evaluate only operator on kernel or kernel ------
  if(isP0 && dimf==1 && firstOrderElt && hasKer && op.opkernelp()->dimsRes_.first==1)
    {
      if(opkerisId) //evaluate Kernel
        {
          const Kernel* ker = op.kernelp();
          for(; itw != itwe; ++itw, ++itp, ++itv)            //loop on quadrature points
            {
              K& t = (*itv)[0];
              (*ker)(x,*itp, t);
              t *= difElt * *itw;
            }
            d=1; m=1;
          return;
        }
      //applied operator on Kernel
      const OperatorOnKernel* opk = op.opkernelp();
      for(; itw != itwe; ++itw, ++itp, ++itv)            //loop on quadrature points
        {
          K& t = (*itv)[0];
          opk->eval(x, *itp, t, nx, ny);
          t *= difElt * *itw;
        }
      d=1; m=1;
      return;
    }

  //----------------  General case --------------------
  //compute values of operator for each shape function at quadrature points
  std::vector<ShapeValues>::iterator itsh = shv->begin();               //shapevalues at first quadrature point
  if(sidenum>0) itsh = shv->begin()+(sidenum-1)*nbq;                    //if extension, take shapevalues on side
  ShapeValues svt;
  GeomMapData* gmap = gmapdata;
  if(!firstOrderElt) gmap = new GeomMapData(*gmapdata);                 //copy of gmapdata to avoid data races in case of multithreads
  number_t nbfun = elt.refElt_p->nbDofs()*nbc;                          //number of shape functions of ref element
  number_t dimpf = dimf;                                                //dimension of shape functions of ref element
  bool rotsh = elt.refElt_p->rotateDof;                                 //true if dof has to be rotated;
  std::vector<number_t> vertnums;                                       //to store vertex numbers if required
  if(rotsh) vertnums = elt.geomElt_p->meshElement()->verticesNumbers();
  for(; itw != itwe; ++itw, ++itp, itq += dq, ++itsh, ++itv)            //loop on quadrature points
    {
      if(!firstOrderElt)  //differential elements are to be updated
      {
         gmap->computeJacobianMatrix(itq);     //compute jacobian at quadrature point
         gmap->computeJacobianDeterminant();   //compute differential element at quadrature point
         difElt = gmap->differentialElement;
         if(nor)
         {
           gmap->computeOrientedNormal();       //compute outwards normal
           ny = &gmap->normalVector;
         }
         if(der_ord > 0 || mapType==_covariantPiolaMap) gmap->invertJacobianMatrix(); //compute inverse jacobian matrix if required
      }
      ShapeValues* sv = &(*itsh);
      if(mapsh || changeSign) //map shape values
        {
          svt = ShapeValues(*itsh);                    //copy to preserve reference shape values
          sv = &svt;
          if(mapsh)
            {
              if(rotsh) elt.refElt_p->rotateDofs(vertnums, svt, der_ord>0); //rotate shape values
              switch(mapType)                         //map reference shapevalues to physical space
                {
                  case _contravariantPiolaMap: svt.contravariantPiolaMap(svt, *gmap, der_ord>0, der_ord>1); break;
                  case _covariantPiolaMap: svt.covariantPiolaMap(svt, *gmap, der_ord>0, der_ord>1); break;
                  case _MorleyMap: svt.Morley2dMap (svt, *gmap, der_ord>0, der_ord>1); break;
                  case _ArgyrisMap:svt.Argyris2dMap(svt, *gmap, der_ord>0, der_ord>1); break;
                  default: svt.map(svt, *gmap, der_ord>0, der_ord>1);
                }
            }
          dimpf = sv->w.size()/nbfun;                         //dimension of physical shape functions, may be differ from dimf!
          if(changeSign) sv->changeSign(*sign, dimpf, der_ord>0, der_ord>1);//change sign of shape functions according to sign vector
        }

      //evaluate operator
      if(evalfun) op.eval(x,*itp, sv->w, sv->dw, sv->d2w, dimpf, *itv, d, m, nx, ny); //evaluate differential operator with function
      else  op.eval(sv->w, sv->dw, sv->d2w, dimpf, *itv, d, m, ny);                   //evaluate differential operator
      *itv *= difElt* * itw;                                                //multiply by differential element * quadrature weight
    }
    if(!firstOrderElt) delete gmap;
}

