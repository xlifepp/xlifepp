/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

/*!
  \file SauterSchwabSymIM.hpp
  \authors J. Grimaldi Calderon and E. Lunéville.
  \since 2 jul 2018
  \date 21 aug 2018

  \brief Implementation of the Sauter and Schwab's method to compute the FE TermMatrix
             intg_Eu intg Ev opu(y) opk(x,y) opv(x) dy dx
         for symetric kernels (only depending on x-y )
 */

#ifndef SAUTER_SCHWAB_SYM_IM_HPP
#define SAUTER_SCHWAB_SYM_IM_HPP

#include "finiteElements.h"
#include "utils.h"
#include "config.h"
#include "operator.h"
#include "termUtils.hpp"

namespace xlifepp
{

/*!
  \class SauterSchwabSymIM
  integral over a product of geometric elements with singularity using Sauter-Schwab technique (for elements that shared vertices)
  Stefan A. Sauter, Christoph Schwab, "Boundary Element Methods", Springer, 2010
*/
class SauterSchwabSymIM: public DoubleIM
{
public:
  Quadrature* quadSelf;    //!< quadrature on segment [0,1] for self influence (same element)
  Quadrature* quadSelfTri; //!< quadrature on reference triangle for self influence (same element)
  Quadrature* quadEdge;    //!< quadrature on segment [0,1] for elements adjacent only by an edge
  Quadrature* quadVertex;  //!< quadrature on segment [0,1] for elements adjacent only by a vertex
  number_t ordSelf;        //!< order of quadrature for self influence (same element)
  number_t ordSelfTri;     //!< order of triangle quadrature for self influence (same element)
  number_t ordEdge;        //!< order of quadrature for for elements adjacent only by an edge
  number_t ordVertex;      //!< order of quadrature for for elements adjacent only by a vertex

private:
  mutable std::vector<Matrix<real_t> > mlambdasSelfReal_; //!< pre-computed shape value matrices for self influence
  mutable std::vector<Matrix<real_t> > mlambdasEdgeReal_; //!< pre-computed shape value matrices for elements adjacent only by an edge
  mutable std::vector<Matrix<real_t> > mlambdasVertexReal_; //!< pre-computed shape value matrices for elements only by a vertex
  mutable std::vector<Matrix<complex_t> > mlambdasSelfComplex_; //!< pre-computed shape value matrices for self influence
  mutable std::vector<Matrix<complex_t> > mlambdasEdgeComplex_; //!< pre-computed shape value matrices for elements adjacent only by an edge
  mutable std::vector<Matrix<complex_t> > mlambdasVertexComplex_; //!< pre-computed shape value matrices for elements only by a vertex

public:
  SauterSchwabSymIM(number_t ord =3) //! basic constructor (order 3 on segment and triangle)
  {
    ordSelf=ord; ordEdge=ordSelf; ordVertex=ordSelf;
    ordSelfTri=2*ordSelf;
    quadSelf=findQuadrature(_segment,_GaussLegendreRule, ordSelf); quadEdge=quadSelf; quadVertex=quadSelf;
    quadSelfTri=findBestQuadrature(_triangle, ordSelfTri);
    name="Sauter-Schwab_sym_"+tostring(ordSelf);
    imType=_SauterSchwabSymIM; singularType=_r_; singularOrder=-1;
  }

  SauterSchwabSymIM(Quadrature& q, Quadrature& qTri) //! full constructor from quadrature objects
  {
    ordSelf=q.degree; ordEdge=ordSelf; ordVertex=ordSelf;
    ordSelfTri=qTri.degree;
    quadSelf=&q; quadEdge=quadSelf; quadVertex=quadSelf;
    quadSelfTri=&qTri;
    imType=_SauterSchwabSymIM; singularType=_r_; singularOrder=-1;
    name="Sauter-Schwab_sym_"+tostring(ordSelf);
  }

  virtual SauterSchwabSymIM* clone() const
  {return new SauterSchwabSymIM(*this);}

  virtual std::list<Quadrature*> quadratures() const  //! return the list of (single) quadratures in a list
  {
    std::list<Quadrature*> lsq;
    if(quadSelf!=nullptr) lsq.push_back(quadSelf);
    if(quadSelfTri!=nullptr) lsq.push_back(quadSelfTri);
    if(quadEdge!=nullptr && quadEdge!=quadSelf)  lsq.push_back(quadEdge);
    if(quadVertex!=nullptr && quadVertex!=quadSelf && quadVertex!=quadEdge) lsq.push_back(quadVertex);
    return lsq;
  }

  virtual void print(std::ostream& os) const          //! print IntegrationMethod on stream
  {
    os<<"Sauter-Schwab integration method with "<< quadSelf->name<<" degree " << quadSelf->degree
      << " ( " << quadSelf->quadratureRule.size() << " points ) " <<eol;
  }
  virtual void print(PrintStream& os) const {print(os.currentStream());}

  //template computeIE function, explicit instanciation because template functions are not inherited
  template <typename K>
  std::vector<Matrix<K> >& mlambdasSelf(K k) const;
  template <typename K>
  std::vector<Matrix<K> >& mlambdasEdge(K k) const;
  template <typename K>
  std::vector<Matrix<K> >& mlambdasVertex(K k) const;

  template<typename K>
  void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                 const KernelOperatorOnUnknowns& kuv, Matrix<K>& res,IEcomputationParameters& ieparams,
                 Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk) const;
  void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                 const KernelOperatorOnUnknowns& kuv, Matrix<real_t>& res,IEcomputationParameters& ieparams,
                 Vector<real_t>& val_opu, Vector<real_t>& val_opv, Vector<real_t>& val_opk) const
  {computeIE<>(elt_S, elt_T,adj, kuv, res,ieparams, val_opu, val_opv, val_opk);}
  void computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                 const KernelOperatorOnUnknowns& kuv, Matrix<complex_t>& res,IEcomputationParameters& ieparams,
                 Vector<complex_t>& val_opu, Vector<complex_t>& val_opv, Vector<complex_t>& val_opk) const
  {computeIE<>(elt_S, elt_T,adj, kuv, res,ieparams, val_opu, val_opv, val_opk);}

  //quadrature tools
  template<typename K>
  void selfInfluences(const RefElement* relt_S, const RefElement* relt_T, const KernelOperatorOnUnknowns& kuv,
                      const Vector<real_t>* nx, const Vector<real_t>* ny,
                      Matrix<K>& res, IEcomputationParameters& ieparams) const;
  template<typename K>
  void adjacentTrianglesbyEdge(const RefElement* relt_S, const RefElement* relt_T, const KernelOperatorOnUnknowns& kuv,
                               const Vector<real_t>* nx, const Vector<real_t>* ny,
                               const Vector< number_t >& indi, const Vector< number_t >& indj,
                               Matrix<K>& res, IEcomputationParameters& ieparams) const;
  template<typename K>
  void adjacentTrianglesbyVertex(const RefElement* relt_S, const RefElement* relt_T, const KernelOperatorOnUnknowns& kuv,
                                 const Vector<real_t>* nx, const Vector<real_t>* ny,
                                 const Vector< number_t >& indi, const Vector< number_t >& indj,
                                 Matrix<K>& res, IEcomputationParameters& ieparams) const;
  template<typename K>
  void lambdaSelf(const number_t i, const real_t e1, const real_t e2,
                  const RefElement* relt_S, const RefElement* relt_T, const KernelOperatorOnUnknowns& kuv,
                  Matrix<K>& res,
                  const Vector<real_t>* nx, const Vector<real_t>* ny,
                  IEcomputationParameters& ieparams) const;
  template<typename K>
  void lambdaEdge(const number_t l, const number_t k, const real_t e1, const real_t e2, const real_t e3,
                  const RefElement* relt_S, const RefElement* relt_T, const KernelOperatorOnUnknowns& kuv,
                  Matrix<K>& res,
                  const Vector<real_t>* nx, const Vector<real_t>* ny,
                  const Vector< number_t >& indi, const Vector< number_t >& indj,
                  IEcomputationParameters& ieparams) const;
  template<typename K>
  void lambdaVertex(const Point& p1, const Point& p2,
                    const RefElement* relt_S, const RefElement* relt_T, const KernelOperatorOnUnknowns& kuv,
                    Matrix<K>& res,
                    const Vector<real_t>* nx, const Vector<real_t>* ny,
                    const Vector< number_t >& indi, const Vector< number_t >& indj,
                    IEcomputationParameters& ieparams) const;
};

// implementation of template member functions
template<typename K>
void SauterSchwabSymIM::computeIE(const Element* elt_S, const Element* elt_T, AdjacenceInfo& adj,
                                  const KernelOperatorOnUnknowns& kuv, Matrix<K>& res, IEcomputationParameters& ieparams,
                                  Vector<K>& val_opu, Vector<K>& val_opv, Vector<K>& val_opk) const
{
  if (ieparams.sh_u!=_triangle)
    error("shape_not_handled", words("shape", ieparams.sh_u));
  if (ieparams.sh_v!=_triangle)
    error("shape_not_handled", words("shape", ieparams.sh_v));

  //load normals
  Vector<real_t>* nx=nullptr, *ny=nullptr;
  MeshElement* melt_S=ieparams.melt_u, *melt_T=ieparams.melt_v;
  if (kuv.xnormalRequired()) nx = &melt_T->geomMapData_p->normalVector;
  if (kuv.ynormalRequired()) ny = &melt_S->geomMapData_p->normalVector;
  //Reference elements
  RefElement* relt_S = elt_S->refElt_p;
  RefElement* relt_T = elt_T->refElt_p;

  if(relt_S->order()>1 || relt_T->order()>1)
    error("interp_order_too_large", 1, std::max(relt_S->order(),relt_T->order()));

  //  compute quadrature regarding triangle position
  std::vector<number_t>& indi=adj.sharedVertex1, &indj=adj.sharedVertex2;
  switch (adj.status)
    {

    case _adjacentByElement: selfInfluences(relt_S, relt_T, kuv, nx, ny, res, ieparams); return;
    case _adjacentBySide:
      {
        indi[2]=6-indi[0]-indi[1]; indj[2]=6-indj[0]-indj[1];
        adjacentTrianglesbyEdge(relt_S, relt_T, kuv, nx, ny, indi, indj, res, ieparams);
        return;
      }
    case _adjacentByVertex:
      {
        switch (indi[0])
          {
          case 1: indi[1]=2; indi[2]=3; break;
          case 2: indi[1]=3; indi[2]=1; break;
          default: indi[1]=1; indi[2]=2;
          }
        switch (indj[0])
          {
          case 1: indj[1]=2; indj[2]=3; break;
          case 2: indj[1]=3; indj[2]=1; break;
          default: indj[1]=1; indj[2]=2;
          }
        adjacentTrianglesbyVertex(relt_S, relt_T, kuv, nx, ny, indi, indj, res, ieparams);
        return;
      }
    default:
      error("adjacence_status_not_handled");
    }
}

template <>
std::vector<Matrix<real_t> >& SauterSchwabSymIM::mlambdasSelf(real_t r) const;
template <>
std::vector<Matrix<complex_t> >& SauterSchwabSymIM::mlambdasSelf(complex_t c) const;
template <>
std::vector<Matrix<real_t> >& SauterSchwabSymIM::mlambdasEdge(real_t r) const;
template <>
std::vector<Matrix<complex_t> >& SauterSchwabSymIM::mlambdasEdge(complex_t c) const;
template <>
std::vector<Matrix<real_t> >& SauterSchwabSymIM::mlambdasVertex(real_t r) const;
template <>
std::vector<Matrix<complex_t> >& SauterSchwabSymIM::mlambdasVertex(complex_t c) const;
/*================================================================================================================
                          Same triangles, singular integral -> use Sauter-Schwab formulae

  up to now, it works only for scalar cases

  arguments:
     elt_S, elt_T: elements involved in integral
     kuv: integrand op(u)*op(ker)*op(v)
     nx,ny: pointers to normal of elt_T and elt_S (0 if not used)
     ieparams: some parameters of computation
     indi, indj: vertex numbering
     quad:  1d quadrature method used inSauter-Schwabb method
     res: matrix int_elt_S x elt_T op(wj)*op(ker)*op(wi)

  ================================================================================================================*/

template<typename K>
void SauterSchwabSymIM::selfInfluences(const RefElement* relt_S, const RefElement* relt_T, const KernelOperatorOnUnknowns& kuv,
                                       const Vector<real_t>* nx, const Vector<real_t>* ny,
                                       Matrix<K>& res, IEcomputationParameters& ieparams) const
{
  //theCout << "Self influences" << eol;
  MeshElement* meltu = ieparams.melt_u;
  Point& S1 = *meltu->nodes[0], & S2 = *meltu->nodes[1],  & S3 = *meltu->nodes[2];

  //Definition of matrix Dphi
  Point S13 = S1-S3, S23 = S2-S3;
  //Jacobien phi
  real_t G12 = meltu->geomMapData_p->differentialElement; G12*=G12;

  //Quadrature points
  number_t N=quadSelf->numberOfPoints();
  const std::vector< real_t >& coords = quadSelf->coords();
  const std::vector< real_t >& weights = quadSelf->weights();

  real_t z0,z1;
  number_t m = res.numberOfRows(), n = res.numberOfColumns();

  bool buildLambdas = (mlambdasSelf(K(0.)).size() == 0);
  if (buildLambdas) { mlambdasSelf(K(0.)).resize(6*N*N, Matrix<K>(m,n,K(0.))); }
  K tmp;
  Point zero(0.,0.,0.);
  number_t index=0;
  Matrix<K> mlambda(m,n,K(0.));

  for(number_t i1 = 1; i1<7; i1++) // six integrals to be computed
    {
      for(number_t i2 = 0; i2<N; i2++)
        {
          real_t x1 = coords[i2];
          for(number_t i3 = 0; i3<N; i3++, index++)
            {
              real_t y1 = coords[i3];
              real_t w = weights[i2]*weights[i3];
              switch (i1)
                {
                //we compute psi(k) o delta(e)
                case 1: z0 = x1*(1.-y1); z1 = x1*y1; break;
                case 2: z0 = -x1*y1; z1 = x1; break;
                case 3: z0 = -x1; z1 = x1*(1.-y1); break;
                case 4: z0 = x1*(y1-1.); z1 = -x1*y1; break;
                case 5: z0 = x1*y1; z1 = -x1; break;
                case 6: z0 = x1; z1 = x1*(y1-1.); break;
                default: break;
                }
              //we compute k(Dphi * (psi(k) o delta (e))
              Point dz = z0*S13+z1*S23;
              kuv.opker().eval(dz, zero, tmp, nx, ny);

              //we compute the matrix lambda(i) considering all possible shape values
              // mlambda*=0.;
              // lambdaSelf(i1, x1, y1, elt_S, kuv, mlambda, nx, ny, ieparams);
              std::vector<Matrix<K> >& mlambdas=mlambdasSelf(K(0.));
              if (buildLambdas) { lambdaSelf(i1, x1, y1, relt_S, relt_T, kuv, mlambdas[index], nx, ny, ieparams); }

              //we compute f(e1,e2)
              res+=(tmp*x1*w*G12)*mlambdas[index];
              // res+=(tmp*x1*w*G12)*mlambda;
            }
        }
    }
}

/*================================================================================================================
                          Adjacent triangles by edge, singular integral -> use Sauter-Schwab formulae

  up to now, it works only for scalar cases

  arguments:
     elt_S, elt_T: elements involved in integral
     kuv: integrand op(u)*op(ker)*op(v)
     nx,ny: pointers to normal of elt_T and elt_S (0 if not used)
     ieparams: some parameters of computation
     indi, indj: vertex numbering
     quad:  1d quadrature method used inSauter-Schwabb method
     res: matrix int_elt_Sy x elt_Tx op(wj)(y)*op(ker)(x,y)*op(wi)(x)

  ================================================================================================================*/

template<typename K>
void SauterSchwabSymIM::adjacentTrianglesbyEdge(const RefElement* relt_S, const RefElement* relt_T, const KernelOperatorOnUnknowns& kuv,
    const Vector<real_t>* nx, const Vector<real_t>* ny,
    const Vector< number_t >& indi, const Vector< number_t >& indj,
    Matrix<K>& res, IEcomputationParameters& ieparams) const
{
  //theCout << "Adjacent by Edge" << eol;
  //nodes of the triangles
  MeshElement* meltu=ieparams.melt_u;
  Point& S1=*meltu->nodes[indi[0]-1], & S2= *meltu->nodes[indi[1]-1],  & S3= *meltu->nodes[indi[2]-1];
  MeshElement* meltv=ieparams.melt_v;
  Point& T1=*meltv->nodes[indj[0]-1], & T2= *meltv->nodes[indj[1]-1],  & T3= *meltv->nodes[indj[2]-1];

  //definition of Dphi1 Dphi2
  Point S21 = S2-S1, S32 = S3-S2, S31 = S3-S1;
  Point T21 = T2-T1, T32 = T3-T2, T31 = T3-T1;

  //Quadrature points and weights
  number_t N=quadEdge->numberOfPoints();
  const std::vector< real_t >& coords=quadEdge->coords();
  const std::vector< real_t >& weights=quadEdge->weights();

  //jacobian
  real_t G1G2=meltu->geomMapData_p->differentialElement * meltv->geomMapData_p->differentialElement;

  number_t m = res.numberOfRows(), n = res.numberOfColumns();
  // Matrix<K> mlambda(m, n, K(0));

  bool buildLambdas = (mlambdasEdge(K(0.)).size() == 0);
  if (buildLambdas) { mlambdasEdge(K(0.)).resize(6*N*N*N, Matrix<K>(m,n,K(0.))); }
  real_t z0, z1, z2;
  K tmp=0.;
  Point zero(0.,0.,0.);
  number_t index=0;
  // Matrix<K> mlambda(m,n,K(0.));
  for(number_t k = 1; k<3; k++)
    {
      for(number_t l = 1; l<4; l++)
        {
          for(number_t i1 = 0; i1<N; i1++)
            {
              real_t x = coords[i1];
              for(number_t i2 = 0; i2<N; i2++)
                {
                  real_t y = coords[i2];
                  real_t w2 = weights[i1]*weights[i2];
                  for(number_t i3 = 0; i3<N; i3++, index++)
                    {
                      real_t z = coords[i3];
                      real_t w3 = w2*weights[i3];

                      //we compute psi(k) o delta (e) (in the variables z0, z1, z2)
                      if (l==1 && k==1) {z0 = x*(1.-y+y*z); z1 = x*y*(1.-z); z2 = x*y*z;}
                      if (l==1 && k==2) {z0 = -x*y*z; z1 = x; z2 = x*y*(1.-z);}
                      if (l==2 && k==1) {z0 = x*(1.-y); z1 = x*y; z2 = x*(1.-y+y*z);}
                      if (l==2 && k==2) {z0 = x*(y-1.); z1 = x*(1.-y*z); z2 = x*y;}
                      if (l==3 && k==1) {z0 = x*(1.-y); z1 = x*(y-y*z); z2 =  x;}
                      if (l==3 && k==2) {z0 = x*(y*z-1.); z1 = x*(1.-y); z2 = x*y*z;}

                      //we compute the matrix lambda(i) considering all possible shape values
                      // mlambda*=0.;
                      // lambdaEdge(l, k, x, y, z, elt_S, elt_T, kuv, mlambda, nx, ny, indi, indj, ieparams);
                      std::vector<Matrix<K> >& mlambdas=mlambdasEdge(K(0.));
                      if (buildLambdas) { lambdaEdge(l, k, x, y, z, relt_S, relt_T, kuv, mlambdas[index], nx, ny, indi, indj, ieparams); }

                      //we compute Dphi o psi(k) o delta (e)
                      Point dz = -z1*T31-z0*T21+z2*S31;

                      //we compute k^(psi(k) o delta (e))
                      kuv.opker().eval(dz, zero, tmp, nx, ny);

                      //we compute f(e1,e2)
                      res+=(tmp*x*x*y*w3*G1G2)*mlambdas[index];
                      // res+=(tmp*x*x*y*w3*G1G2)*mlambda;
                    }
                }
            }
        }
    }

  // Additional permutation to do for P1 case, to improve in future
  number_t oS = relt_S->order(), oT=relt_T->order();
  if(oS==0 && oT==0) return;  //no permutation

  Matrix<K> res2(m,n);
  std::vector<number_t> indi2(3,0), indj2(3,0);
  if(oS==1)
    {
      indi2[1]=indi[2];
      indi2[2]=indi[0];
      indi2[0]=indi[1];
    }
  if(oT==1)
    {
      indj2[1]=indj[2];
      indj2[2]=indj[0];
      indj2[0]=indj[1];
    }
  for (number_t i=0; i < m; ++i)
    for (number_t j=0; j < n; ++j)
      res2(indj2[j],indi2[i])=res(j+1,i+1);
  res=res2;
}

/*================================================================================================================
                          Adjacent triangles by vertex, singular integral -> use Sauter-Schwab formulae

  up to now, it works only for scalar cases

  arguments:
     elt_S, elt_T: elements involved in integral
     kuv: integrand op(u)*op(ker)*op(v)
     nx,ny: pointers to normal of elt_T and elt_S (0 if not used)
     ieparams: some parameters of computation
     indi, indj: vertex numbering
     quad:  1d quadrature method used inSauter-Schwabb method
     res: matrix int_elt_Sy x elt_Tx op(wj)(y)*op(ker)(x,y)*op(wi)(x)

  ================================================================================================================*/
template<typename K>
void SauterSchwabSymIM::adjacentTrianglesbyVertex(const RefElement* relt_S, const RefElement* relt_T, const KernelOperatorOnUnknowns& kuv,
    const Vector<real_t>* nx, const Vector<real_t>* ny,
    const Vector< number_t >& indi, const Vector< number_t >& indj,
    Matrix<K>& res, IEcomputationParameters& ieparams) const
{
  //theCout << "Adjacent by Vertex" << eol;
  MeshElement* meltu=ieparams.melt_u;
  Point& S1=*meltu->nodes[indi[0]-1], & S2= *meltu->nodes[indi[1]-1],  & S3= *meltu->nodes[indi[2]-1];
  MeshElement* meltv=ieparams.melt_v;
  Point& T1=*meltv->nodes[indj[0]-1], & T2= *meltv->nodes[indj[1]-1],  & T3= *meltv->nodes[indj[2]-1];

  //Jacobien
  real_t G1G2=meltu->geomMapData_p->differentialElement * meltv->geomMapData_p->differentialElement;

  //Quadrature points
  number_t N=quadVertex->numberOfPoints();
  const std::vector< real_t >& coords=quadVertex->coords();
  const std::vector< real_t >& weights=quadVertex->weights();

  //definition of Dphi1 Dphi2
  Point S21 = S2-S1, S32 = S3-S2, S31 = S3-S1;
  Point T21 = T2-T1, T32 = T3-T2, T31 = T3-T1;

  number_t m = res.numberOfRows(), n = res.numberOfColumns();
  Matrix<K> matel(m, n, K(0));
  bool buildLambdas = (mlambdasVertex(K(0.)).size() == 0);
  if (buildLambdas) { mlambdasVertex(K(0.)).resize(6*N*N*N*N, Matrix<K>(m,n,K(0.))); }
  real_t e1, e2, e3, e4;
  K tmp;

  Point p1(0.,0.), p2(0.,0.);
  Point zero(0.,0.,0.);
  number_t index=0;

  for(number_t k = 1; k<7; k++)
    {
      for(number_t i1 = 0; i1<N; i1++)
        {
          e1 = coords[i1];
          for(number_t i2 = 0; i2<N; i2++)
            {
              e2 = coords[i2];
              for(number_t i3 = 0; i3<N; i3++)
                {
                  e3 = coords[i3];
                  for(number_t i4 = 0; i4<N; i4++, index++)
                    {
                      e4 = coords[i4];
                      real_t w = weights[i1]*weights[i2]*weights[i3]*weights[i4];

                      switch(k)
                        {
                        case 1:
                          {
                            p1[0] = e1*(1.-e2);
                            p1[1] = e1*(e2-e3*e2);
                            p2[0] = e1*(1.-e2*e3*e4);
                            p2[1] = e1*e2*e3*e4;
                            break;
                          }
                        case 2:
                          {
                            p1[0] = e1*(1.-e2);
                            p1[1] = e1*(e2-e2*e3*e4);
                            p2[0] = e1*(1.-e2*e3);
                            p2[1] = e1*e2*e3;
                            break;
                          }
                        case 3:
                          {
                            p1[0] = e1*(1.-e2*e3);
                            p1[1] = e1*(e2*e3-e2*e3*e4);
                            p2[0] = e1*(1.-e2);
                            p2[1] = e1*e2;
                            break;
                          }
                        case 4:
                          {
                            p1[0] = e1*(1.-e2);
                            p1[1] = e1*e2;
                            p2[0] = e1*(1.-e2*e3);
                            p2[1] = e1*(e2*e3-e2*e3*e4);
                            break;
                          }
                        case 5:
                          {
                            p1[0] = e1*(1.-e2*e3);
                            p1[1] = e1*e2*e3;
                            p2[0] = e1*(1.-e2);
                            p2[1] = e1*(e2-e2*e3*e4);
                            break;
                          }
                        case 6:
                          {
                            p1[0] = e1*(1.-e2*e3*e4);
                            p1[1] = e1*e2*e3*e4;
                            p2[0] = e1*(1.-e2);
                            p2[1] = e1*(e2-e2*e3);
                            break;
                          }
                        default:
                          break;
                        }

                      //we compute k(Dphi * (psi(k) o delta (e))
                      Point dz = p2[0]*T21+p2[1]*T31-p1[0]*S21-p1[1]*S31;
                      kuv.opker().eval(dz, zero, tmp, nx, ny);
                      // matel*=0.;
                      // lambdaVertex(p1, p2, elt_S, elt_T, kuv, matel, nx, ny, indi, indj, ieparams);
                      std::vector<Matrix<K> >& mlambdas=mlambdasVertex(K(0.));
                      if (buildLambdas) { lambdaVertex(p1, p2, relt_S, relt_T, kuv, mlambdas[index], nx, ny, indi, indj, ieparams); }

                      res+=e1*e1*e1*e2*e2*e3*tmp*w*G1G2*mlambdas[index];
                      // res+=e1*e1*e1*e2*e2*e3*tmp*w*G1G2*matel;
                    }
                }
            }
        }
    }

  // Additional permutation to do for P1 case, to improve in future
  number_t oS = relt_S->order(), oT=relt_T->order();
  if(oS==0 && oT==0) return;  //no permutation

  Matrix<K> res2(m,n);
  std::vector<number_t> indi2(3,0), indj2(3,0);
  if(oS==1)
    {
      indi2[1]=indi[2];
      indi2[2]=indi[0];
      indi2[0]=indi[1];
    }
  if(oT==1)
    {
      indj2[1]=indj[2];
      indj2[2]=indj[0];
      indj2[0]=indj[1];
    }
  for (number_t i=0; i < m; ++i)
    for (number_t j=0; j < n; ++j)
      res2(indj2[j],indi2[i])=res(j+1,i+1);
  res=res2;
}

/*================================================================================================================
                                       member functions lambdaself and lambdaedge
  ================================================================================================================*/

template<typename K>
void SauterSchwabSymIM::lambdaSelf(const number_t i, const real_t e1, const real_t e2,
                                   const RefElement* relt_S, const RefElement* relt_T, const KernelOperatorOnUnknowns& kuv, Matrix<K>& res,
                                   const Vector<real_t>* nx, const Vector<real_t>* ny,
                                   IEcomputationParameters& ieparams) const
{
  number_t N=quadSelfTri->numberOfPoints();
  const std::vector< real_t >& coords=quadSelfTri->coords();
  const std::vector< real_t >& weights=quadSelfTri->weights();
  real_t me1=1.-e1, me2=1.-e2;

  //P0 shortcut
  if (ieparams.isP0)
    {
      res[0]=me1*me1/2.;
      return;
    }

  //Iterator
  std::vector< real_t >::const_iterator itc=coords.begin();
  Point p1(0.,0.), p2(0.,0.);
  bool deru1=ieparams.difforder_u>0, deru2=ieparams.difforder_u>1;
  bool derv1=ieparams.difforder_v>0, derv2=ieparams.difforder_v>1;
  ShapeValues shv_phi(*relt_S,deru1,deru2), shv_psi(*relt_T,derv1,derv2);
  Vector<K> val_opu, val_opv;
  dimen_t du,mu,dv,mv;
  number_t nb_ut = relt_S->nbDofs(), nb_vt = nb_ut;
  Matrix<K> tmp1(res.numberOfRows(), res.numberOfColumns(), K(0.));

  //Interpolation for Pn; n>0
  for (number_t i1 = 0; i1<N; ++i1, itc+=2)
    {
      real_t x=*itc, y=*(itc+1);
      real_t w = weights[i1];
      switch (i)
        {
        case 1:
          {
            p1[0] = me1*x;
            p1[1] = me1*y;
            p2[0] = e1*me2+me1*x;
            p2[1] = e1*e2+me1*y;
            break;
          }
        case 2:
          {
            p1[0] = me1*x+e1*e2;
            p1[1] = me1*y;
            p2[0] = me1*x;
            p2[1] = e1+me1*y;
            break;
          }
        case 3:
          {
            p1[0] = e1+me1*x;
            p1[1] = me1*y;
            p2[0] = me1*x;
            p2[1] = e1*me2+me1*y;
            break;
          }
        case 4:
          {
            p1[0] = e1*me2+me1*x;
            p1[1] = e1*e2+me1*y;
            p2[0] = me1*x;
            p2[1] = me1*y;
            break;
          }
        case 5:
          {
            p1[0] = me1*x;
            p1[1] = e1+me1*y;
            p2[0] = e1*e2+me1*x;
            p2[1] = me1*y;
            break;
          }
        case 6:
          {
            p1[0] = me1*x;
            p1[1] = e1*me2+me1*y;
            p2[0] = e1+me1*x;
            p2[1] = me1*y;
            break;
          }
        default:
          break;
        }

      relt_S->computeShapeValues(p1.begin(), shv_phi, deru1, deru2);
      relt_T->computeShapeValues(p2.begin(), shv_psi, derv1, derv2);

      kuv.opu().eval(shv_phi.w, shv_phi.dw, shv_phi.d2w, ieparams.dimf_u, val_opu, du, mu, ny);
      kuv.opv().eval(shv_psi.w, shv_psi.dw, shv_psi.d2w, ieparams.dimf_v, val_opv, dv, mv, nx);
      tmp1*=K(0.);
      tensorOpAdd(_product, val_opv, nb_vt, val_opu, nb_ut, tmp1, K(1.));
      res+=w*me1*me1*tmp1;
    }
}

template<typename K>
void SauterSchwabSymIM::lambdaEdge(const number_t l, const number_t k, const real_t e1, const real_t e2, const real_t e3,
                                   const RefElement* relt_S, const RefElement* relt_T, const KernelOperatorOnUnknowns& kuv,
                                   Matrix<K>& res,
                                   const Vector<real_t>* nx, const Vector<real_t>* ny,
                                   const Vector< number_t >& indi, const Vector< number_t >& indj,
                                   IEcomputationParameters& ieparams) const
{
  real_t me1=1.-e1, me2=1.-e2, me3=1.-e3, e1e2=e1*e2, e1e2e3=e1e2*e3, e2e3=e2*e3;
  //P0 shortcut
  if (ieparams.isP0)
    {
      res[0]=me1;
      return;
    }

  //Quadrature points over a segment
  number_t N=quadEdge->numberOfPoints();
  const std::vector< real_t >& coords=quadEdge->coords();
  const std::vector< real_t >& weights=quadEdge->weights();

  //Iterator
  std::vector< real_t >::const_iterator itc=coords.begin();
  Point p1(0.,0.), p2(0.,0.);

  bool deru1=ieparams.difforder_u>0, deru2=ieparams.difforder_u>1;
  bool derv1=ieparams.difforder_v>0, derv2=ieparams.difforder_v>1;
  ShapeValues shv_phi(*relt_S,deru1,deru2), shv_psi(*relt_T,derv1,derv2);
  Vector<K> val_opu, val_opv;
  dimen_t du,mu,dv,mv;
  number_t nb_ut = relt_S->nbDofs(), nb_vt = nb_ut;
  Matrix<K> tmp1(res.numberOfRows(), res.numberOfColumns(),K(0.));

  for (number_t i1 = 0; i1<N; ++i1, ++itc)
    {
      real_t x =*itc;
      real_t w = weights[i1];
      switch (l)
        {
        //we compute mlambda in function of the values of the indexes k and l
        case 1:
          {
            if (k==1)
              {
                p1[0] = me1*x;
                p1[1] = e1e2e3;
                p2[0] = (1.-e2*me3)*e1+me1*x;
                p2[1] = me3*e1e2;
              }
            if (k==2)
              {
                p1[0] = me1*x+e1e2e3;
                p1[1] = me3*e1e2;
                p2[0] = me1*x;
                p2[1] = e1;
              }
            break;
          }
        case 2:
          {
            if (k==1)
              {
                p1[0] = me1*x;
                p1[1] = (1.-e2*me3)*e1;
                p2[0] = me2*e1+me1*x;
                p2[1] = e1e2;
              }
            if (k==2)
              {
                p1[0] = me1*x+e1*me2;
                p1[1] = e1e2;
                p2[0] = me1*x;
                p2[1] = (1.-e2e3)*e1;
              }
            break;
          }
        case 3:
          {
            if (k==1)
              {
                p1[0] = me1*x;
                p1[1] = e1;
                p2[0] = me2*e1+me1*x;
                p2[1] = me3*e1e2;
              }
            if (k==2)
              {
                p1[0] = me1*x+e1*(1.-e2e3);
                p1[1] = e1e2e3;
                p2[0] = me1*x;
                p2[1] = me2*e1;
              }
            break;
          }
        default:
          break;
        }

      relt_S->computeShapeValues(p1.begin(), shv_phi, deru1,deru2);
      relt_T->computeShapeValues(p2.begin(), shv_psi, derv1,derv2);

      kuv.opu().eval(shv_phi.w, shv_phi.dw, shv_phi.d2w, ieparams.dimf_u, val_opu, du, mu, nx);
      kuv.opv().eval(shv_psi.w, shv_psi.dw, shv_psi.d2w, ieparams.dimf_v, val_opv, dv, mv, nx);

      tmp1*=0.;
      tensorOpAdd(_product, val_opv, nb_vt, val_opu, nb_ut, tmp1, K(1.));

      res+=me1*w*tmp1;
    }
  return;
}

template<typename K>
void SauterSchwabSymIM::lambdaVertex(const Point& p1, const Point& p2,
                                     const RefElement* relt_S, const RefElement* relt_T, const KernelOperatorOnUnknowns& kuv,
                                     Matrix<K>& res,
                                     const Vector<real_t>* nx, const Vector<real_t>* ny,
                                     const Vector< number_t >& indi, const Vector< number_t >& indj,
                                     IEcomputationParameters& ieparams) const
{
  //P0 shortcut
  if (ieparams.isP0)
    {
      res[0]=1.;
      return;
    }

  bool deru1=ieparams.difforder_u>0, deru2=ieparams.difforder_u>1;
  bool derv1=ieparams.difforder_v>0, derv2=ieparams.difforder_v>1;
  ShapeValues shv_phi(*relt_S,deru1,deru2), shv_psi(*relt_T,derv1,derv2);
  Vector<K> val_opu, val_opv, val_opk;
  dimen_t du,mu,dv,mv;
  number_t nb_ut = relt_S->nbDofs(), nb_vt = nb_ut;

  relt_S->computeShapeValues(p1.begin(), shv_phi, deru1, deru2);
  relt_T->computeShapeValues(p2.begin(), shv_psi, derv1, derv2);

  kuv.opu().eval(shv_phi.w, shv_phi.dw, shv_phi.d2w, ieparams.dimf_u, val_opu, du, mu, nx);
  kuv.opv().eval(shv_psi.w, shv_psi.dw, shv_psi.d2w, ieparams.dimf_v, val_opv, dv, mv, nx);

  tensorOpAdd(_product, val_opv, nb_vt, val_opu, nb_ut, res, K(1.));
  return;
}

}
#endif // SAUTER_SCHWAB_SYM_IM_HPP
