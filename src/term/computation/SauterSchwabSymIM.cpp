/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */
 
 /*!
   \file SauterSchwabSymIM.cpp
   \authors J. Grimaldi Calderon and E. Lunéville.
   \since 2 jul 2018
   \date 21 aug 2018
 
   \brief Implementation of the Sauter and Schwab's method to compute the FE TermMatrix
              intg_Eu intg Ev opu(y) opk(x,y) opv(x) dy dx
          for symetric kernels (only depending on x-y )
  */

#include "SauterSchwabSymIM.hpp"

namespace xlifepp
{

template <>
std::vector<Matrix<real_t> >& SauterSchwabSymIM::mlambdasSelf(real_t r) const { return mlambdasSelfReal_; }
template <>
std::vector<Matrix<complex_t> >& SauterSchwabSymIM::mlambdasSelf(complex_t c) const { return mlambdasSelfComplex_; }
template <>
std::vector<Matrix<real_t> >& SauterSchwabSymIM::mlambdasEdge(real_t r) const { return mlambdasEdgeReal_; }
template <>
std::vector<Matrix<complex_t> >& SauterSchwabSymIM::mlambdasEdge(complex_t c) const { return mlambdasEdgeComplex_; }
template <>
std::vector<Matrix<real_t> >& SauterSchwabSymIM::mlambdasVertex(real_t r) const { return mlambdasVertexReal_; }
template <>
std::vector<Matrix<complex_t> >& SauterSchwabSymIM::mlambdasVertex(complex_t c) const { return mlambdasVertexComplex_; }


}
