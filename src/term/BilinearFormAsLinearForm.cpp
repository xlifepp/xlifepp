/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file BilinearFormAsLinearForm.cpp
  \author E. Lunéville
  \since 23 mar 2012
  \date  02 apr 2012

  \brief Implementation of xlifepp::BilinearFormAsLinearForm classes member functions and related utilities
*/

#include "BilinearFormAsLinearForm.hpp"
#include "utils.h"


namespace xlifepp
{

//user functions like constructors of double integral involving Termvector: int_domx int_dom_y KernelOperatorOnTermVectorAndUnknown

LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const std::vector<Parameter>& ps)
{
  trace_p->push("intg(Domain, Domain, KernelOperatorOnTermVectorAndUnknown, Parameters)");
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_method);
  params.insert(_pk_quad);
  params.insert(_pk_order);
  params.insert(_pk_symmetry);
  params.insert(_pk_isogeo);

  real_t bound=0.;
  bool isogeom=false;
  IntegrationMethod* meth=nullptr;
  IntegrationMethods meths;
  QuadRule qr=_defaultRule;
  number_t qo=0;
  SymType st=_undefSymmetry;
  ComputationType ct=_undefComputation;
  const GeomDomain* extdom=nullptr;

  for (number_t i=0; i < ps.size(); ++i)
  {
    intgLfBuildParam(ps[i], bound, isogeom, meth, meths, qr, qo, st, ct, extdom);

    ParameterKey key=ps[i].key();
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("lform_unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);

    intgLfParamCompatibility(key, usedParams);
  }

  // (quad, order) has to be set (or method)
  if (params.find(_pk_quad) != params.end() && params.find(_pk_order) == params.end()) { error("param_missing","order"); }
  if (params.find(_pk_order) != params.end() && params.find(_pk_quad) == params.end()) { error("param_missing","quad"); }

  const TermVector& tvu = *koptvv.termVector(); //extract unknown from termvector
  if (!tvu.isSingleUnknown())
  {
    where("intg(Domain,Domain,KernelOperatorOnTermVectorAndUnknown,...)");
    error("intgintg_su_tv_only");
  }
  const Unknown& u = *tvu.unknown();
  BasicBilinearForm* bbf;
  if (usedParams.find(_pk_method) != usedParams.end())
  {
    // key _method was given
    if (meth != nullptr)
    {
      // IntegrationMethod is given
      bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, OperatorOnUnknown(u,_id), koptvv.algop(),
                                                                       koptvv.opker(), koptvv.koptv().algop(), koptvv.opv(),
                                                                       qr, qo, qr, qo, st));
    }
    else
    {
      // IntegrationMethods is given
      bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, OperatorOnUnknown(u,_id), koptvv.algop(),
                                                                       koptvv.opker(), koptvv.koptv().algop(), koptvv.opv(),
                                                                       qr, qo, qr, qo, st));
    }
  }
  else
  {
    // key _method was not given, we consider _quad and _order (possibly with their default values)
    bbf = static_cast<BasicBilinearForm*>(new DoubleIntgBilinearForm(domu, domv, OperatorOnUnknown(u,_id), koptvv.algop(),
                                                                     koptvv.opker(), koptvv.koptv().algop(), koptvv.opv(),
                                                                     qr, qo, qr, qo, st));
  }
  bbf->isoGeometric = isogeom;
  if(extdom!=nullptr)
    warning("free_warning","extension_domain key not yet handled in double intg involving KernelOperatorOnTermVectorAndUnknown");
  BilinearFormAsLinearForm* bfalf = new BilinearFormAsLinearForm(bbf, tvu, true);
  SuLinearForm sulf;
  sulf.lfs().push_back(lfPair(bfalf, 1.));
  trace_p->pop();
  return LinearForm(sulf);
}

LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv)
{
  std::vector<Parameter> ps(0);
  return intg(domv, domu, koptvv, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  return intg(domv, domu, koptvv, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps={p1, p2};
  return intg(domv, domu, koptvv, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps={p1, p2, p3};
  return intg(domv, domu, koptvv, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  return intg(domv, domu, koptvv, ps);
}
LinearForm intg(const GeomDomain& domv, const GeomDomain& domu, const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  return intg(domv, domu, koptvv, ps);
}

void BilinearFormAsLinearForm::print(std::ostream& os) const
{
  if(theVerboseLevel > 0)
    { os << " bilinear form as linear form: "<<eol;
      bbf_->print(os);
      os<<eol<<*tv_;
  }
}

//----------------------------------------
// DEPRECATED (to be removed)
//----------------------------------------

LinearForm intg(const GeomDomain& domv, const GeomDomain& domu,
                const KernelOperatorOnTermVectorAndUnknown& koptvv,
                const IntegrationMethod& im)  //! construct a simple intg linear form from KernelOperatorOnTermVector
{
  warning("deprecated", "intg(Domain, Domain, ..., IntegrationMethod)", "intg(Domain, Domain, ..., _method=?)");
  const TermVector& tvu = *koptvv.termVector(); //extract unknown from termvector
  if(!tvu.isSingleUnknown())
  {
    where("intg(Domain,Domain,KernelOperatorOnTermVectorAndUnknown,IntegrationMethod)");
    error("intgintg_su_tv_only");
  }
  const Unknown& u = *tvu.unknown();
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>
                          (new DoubleIntgBilinearForm(domu, domv, OperatorOnUnknown(u,_id), koptvv.algop(),
                           koptvv.opker(), koptvv.koptv().algop(), koptvv.opv(), im, _noSymmetry));
  BilinearFormAsLinearForm* bfalf = new BilinearFormAsLinearForm(bbf, tvu, true);
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bfalf, 1.));
  return LinearForm(lc);
}

LinearForm intg(const GeomDomain& domv, const GeomDomain& domu,
                const KernelOperatorOnTermVectorAndUnknown& koptvv,
                QuadRule qr, number_t qo)
{
  warning("deprecated", "intg(Domain, Domain, ..., QuadRule, Number)", "intg(Domain, Domain, ..., _quad=?, _order=?)");
  const TermVector& tvu = *koptvv.termVector(); //extract unknown from termvector
  if(!tvu.isSingleUnknown())
  {
    where("intg(Domain,Domain,KernelOperatorOnTermVectorAndUnknown,QuadRule,Number)");
    error("intgintg_su_tv_only");
  }
  const Unknown& u = *tvu.unknown();
  BasicBilinearForm* bbf = static_cast<BasicBilinearForm*>
                          (new DoubleIntgBilinearForm(domu, domv, OperatorOnUnknown(u,_id), koptvv.algop(),
                           koptvv.opker(), koptvv.koptv().algop(), koptvv.opv(), qr, qo, qr, qo, _noSymmetry));
  BilinearFormAsLinearForm* bfalf = new BilinearFormAsLinearForm(bbf, tvu, true);
  SuLinearForm lc;
  lc.lfs().push_back(lfPair(bfalf, 1.));
  return LinearForm(lc);
}

} //end of namespace xlifepp
