/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file PrintStream.hpp
  \authors E. Lunéville
  \since 27 jan 2017
  \date 27 jan 2017

  \brief Definition of the xlifepp::PrintStream class
 */

#ifndef PRINTSTREAM_HPP
#define PRINTSTREAM_HPP

#include "config.h"
#include "String.hpp"
#include "Environment.hpp"
#include "printUtils.hpp"

#include <fstream>
#include <ios>
#include <iostream>

namespace xlifepp
{
//=========================================================================================================================
/*!
   \class PrintStream
   PrintSream class encapsulates multiple std::ofstream objects allowing to print result of threads on different ofstream
   it there is only one thread available, print as usual in a single file

   Construct such object from one name file, e.g print.txt
   then the following file will be created
      print.txt for thread 0 or no multithreading
      print1.txt for thread 1
      print2.txt for thread 2
      ...

    PrintSream inherits from std::ostream and can be used as ordinary stream, for instance
         PrintStream thePrintStream("print.txt");
         thePrintStream << "toto "<<i<<...

 */
class PrintStream
{
public:
  std::string name;                       //!< root name of files
  std::vector<std::ofstream*> ofstreams;  //!< file stream pointers
  std::vector<number_t> tps;              //!< initial lengths of files
  bool notOpen;                           //!< open status

  void open(const std::string& na);
  void close();

  PrintStream(const std::string& na="") : name(na), notOpen(true) {}
  ~PrintStream() {close();}

  std::ofstream& operator[](number_t i) {return *ofstreams[i];}
  std::ofstream& currentStream()
  {
    number_t n=currentThread();
    if(n<ofstreams.size()) return *ofstreams[n];
    return *ofstreams[0];
  }

  //to print anything
  template <typename T>
  PrintStream& operator<<(const T & t)
  {
    currentStream() << t;
    return *this;
  }

  //to deal with manipulator
  PrintStream& operator<<(std::ostream& (*manip)(std::ostream&))
  {
    (*manip)(currentStream());
    return *this;
  }

  std::ostream& flush()
  {return currentStream().flush();}

  //overloaded function on ostream
  std::ios_base::fmtflags setf(std::ios_base::fmtflags fmtfl)
  {return currentStream().setf(fmtfl);}

  std::ios_base::fmtflags setf(std::ios_base::fmtflags fmtfl, std::ios_base::fmtflags mask)
  {return currentStream().setf(fmtfl,mask);}

  void unsetf(std::ios_base::fmtflags mask)
  {currentStream().unsetf(mask);}

};

//=========================================================================================================================
/*!
   \class CoutStream
   manages output on console that can be also output on PrintStream files
*/
class CoutStream
{
public:
  PrintStream * printStream;    //!< print files
  std::stringstream * stringStream;
  bool traceOnFile;             //!< to trace cout on files (default behavior)
  CoutStream() : printStream(nullptr), stringStream(nullptr), traceOnFile(false) {}
  void open(PrintStream & ps, std::stringstream& ss, bool t=true) { printStream=&ps; stringStream = &ss; traceOnFile=t;}

  //to output anything else
  template <typename T>
  CoutStream& operator<<(const T & t)
  {
    if (isTestMode)
    {
      if (currentThread()==0) { *stringStream << t; }
    }
    else { std::cout << t; }
    if (!traceOnFile) return *this;
    printStream->currentStream() << t;
    return *this;
  }

  //to deal with manipulator
  CoutStream& operator<<(std::ostream& (*manip)(std::ostream&))
  {
    if (isTestMode)
    {
      if (currentThread()==0) { (*manip)(*stringStream); }
    }
    else (*manip)(std::cout);
    if (!traceOnFile) return *this;
    (*manip)(printStream->currentStream());
    return *this;
  }

  CoutStream& flush()
  {
    if (traceOnFile) printStream->currentStream().flush();
    if (isTestMode) stringStream->flush();
    else std::cout.flush();
    return *this;
  }

  //overloaded function on ostream
  std::ios_base::fmtflags setf(std::ios_base::fmtflags fmtfl)
  {
    if (traceOnFile) printStream->currentStream().setf(fmtfl);
    if (isTestMode)
    {
      if (currentThread()==0) return stringStream->setf(fmtfl);
      else { return fmtfl; }
    }
    else return std::cout.setf(fmtfl);
  }

  std::ios_base::fmtflags setf(std::ios_base::fmtflags fmtfl, std::ios_base::fmtflags mask)
  {
    if (traceOnFile) printStream->currentStream().setf(fmtfl,mask);
    if (isTestMode)
    {
      if (currentThread()==0) return stringStream->setf(fmtfl,mask);
      else { return fmtfl; }
    }
    else return std::cout.setf(fmtfl,mask);
  }

  void unsetf(std::ios_base::fmtflags mask)
  {
    if (traceOnFile) printStream->currentStream().unsetf(mask);
    if (isTestMode)
    {
      if (currentThread()) { stringStream->unsetf(mask); }
    }
    else std::cout.unsetf(mask);
  }
};

} // end of namespace xlifepp

#endif /* PRINTSTREAM_HPP */
