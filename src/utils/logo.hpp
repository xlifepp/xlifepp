/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file logo.hpp
  \author N. Kielbasiewicz
  \since 15 dec 2002
  \date 16 sept 2024

  \brief Definition of the function related to ASCII logos
 */

#ifndef LOGO_HPP
#define LOGO_HPP

#include "config.h"

namespace xlifepp
{

//! logo returns 'random' XLiFE++ logo (among 11!) as a multi-line string
string_t logo(const dimen_t);

} // end of namespace xlifepp

#endif /* LOGO_HPP */
