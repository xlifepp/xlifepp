/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Collection.hpp
  \authors E. Lunéville
  \since 11 dec 2017
  \date 11 dec 2017

  \brief Definition of xlifepp::Collection and xlifepp::PCollection classes

  xlifepp::Collection class inheriting from std::vector<T> is a simple interface to a vector of T objects
  xlifepp::PCollection class inheriting from std::vector<T*> is a simple interface to a vector of T* objects
 */

#ifndef COLLECTION_HPP_INCLUDED
#define COLLECTION_HPP_INCLUDED

#include "config.h"
#include <sstream>
#include <vector>
#include "String.hpp"
#include "Point.hpp"
#include "PrintStream.hpp"

namespace xlifepp
{

//===================================================================================
/*!
  \class Collection
  a simple interface to std::vector<T>
  add the insertion operator <<, the constructors from enumerating items
  and access to items using operator ()
*/
//===================================================================================
template <typename T>
class Collection : public std::vector<T>
{
  public:
    //! access to n-th number (const)
    const T& operator()(number_t n) const
    {
      if (n < 1 || n > std::vector<T>::size())
      {
        error("index_out_of_range", n, "Collection<T>",std::vector<T>::size());
      }
      return std::vector<T>::at(n-1);
    }
    //! access to n-th number (non const)
    T& operator()(number_t n)
    {
      if (n < 1 || n > std::vector<T>::size())
      {
        error("index_out_of_range", n, "Collection<T>",std::vector<T>::size());
      }
      return std::vector<T>::at(n-1);
    }

    //! default constructor
    Collection() : std::vector<T>() {}
    //! constructor from std::vector<T>
    Collection(const std::vector<T>& ts) : std::vector<T>(ts) {}
    //! constructor from std::initializer_list<T>
    Collection(const std::initializer_list<T>& ts) : std::vector<T>(ts) {}
    //! constructor from size
    explicit Collection(int n)
    {
      std::vector<T>::resize(n,T());
    }
    //! constructor from size and value
    explicit Collection(int n, const T& s)
    {
      std::vector<T>::resize(n,s);
    }
    //! constructor from 1 T objects
    Collection(const T& s1)
      : std::vector<T>(1)
    {
      std::vector<T>::at(0)=s1;
    }
    //! constructor from 2 T objects
    Collection(const T& s1, const T& s2)
      : std::vector<T>(2)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
    }
    //! constructor from 3 T objects
    Collection(const T& s1, const T& s2, const T& s3)
      : std::vector<T>(3)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
    }
    //! constructor from 4 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4)
      : std::vector<T>(4)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
    }
    //! constructor from 5 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5)
      : std::vector<T>(5)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
    }
    //! constructor from 6 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6)
      : std::vector<T>(6)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
    }
    //! constructor from 7 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7)
      : std::vector<T>(7)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
    }
    //! constructor from 8 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8)
      : std::vector<T>(8)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
    }
    //! constructor from 9 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9)
      : std::vector<T>(9)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
    }
    //! constructor from 10 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10)
      : std::vector<T>(10)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
    }
    //! constructor from 11 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11)
      : std::vector<T>(11)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
    }
    //! constructor from 12 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12)
      : std::vector<T>(12)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
    }
    //! constructor from 13 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13
              )
      : std::vector<T>(13)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
    }
    //! constructor from 14 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14
              )
      : std::vector<T>(14)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
    }
    //! constructor from 15 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15
              )
      : std::vector<T>(15)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
    }
    //! constructor from 16 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16
              )
      : std::vector<T>(16)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
    }
    //! constructor from 17 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17
              )
      : std::vector<T>(17)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
    }
    //! constructor from 18 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18
              )
      : std::vector<T>(18)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
    }
    //! constructor from 19 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19
              )
      : std::vector<T>(19)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
    }
    //! constructor from 20 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20
              )
      : std::vector<T>(20)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
    }
    //! constructor from 21 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21
              )
      : std::vector<T>(21)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
    }
    //! constructor from 22 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22
              )
      : std::vector<T>(22)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
    }
    //! constructor from 23 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23
              )
      : std::vector<T>(23)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
    }
    //! constructor from 24 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24
              )
      : std::vector<T>(24)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
    }
    //! constructor from 25 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25
              )
      : std::vector<T>(25)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
    }
    //! constructor from 26 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26
              )
      : std::vector<T>(26)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
    }
    //! constructor from 27 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27
              )
      : std::vector<T>(27)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
    }
    //! constructor from 28 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28
              )
      : std::vector<T>(28)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
    }
    //! constructor from 29 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29
              )
      : std::vector<T>(29)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
    }
    //! constructor from 30 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30
              )
      : std::vector<T>(30)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
    }
    //! constructor from 31 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31
              )
      : std::vector<T>(31)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
    }
    //! constructor from 32 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32
              )
      : std::vector<T>(32)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
    }
    //! constructor from 33 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33
              )
      : std::vector<T>(33)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
    }
    //! constructor from 34 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34
              )
      : std::vector<T>(34)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
    }
    //! constructor from 35 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35
              )
      : std::vector<T>(35)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
    }
    //! constructor from 36 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36
              )
      : std::vector<T>(36)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
    }
    //! constructor from 37 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36,
               const T& s37
              )
      : std::vector<T>(37)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
      std::vector<T>::at(36)=s37;
    }
    //! constructor from 38 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36,
               const T& s37, const T& s38
              )
      : std::vector<T>(38)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
      std::vector<T>::at(36)=s37;
      std::vector<T>::at(37)=s38;
    }
    //! constructor from 39 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36,
               const T& s37, const T& s38, const T& s39
              )
      : std::vector<T>(39)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
      std::vector<T>::at(36)=s37;
      std::vector<T>::at(37)=s38;
      std::vector<T>::at(38)=s39;
    }
    //! constructor from 40 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36,
               const T& s37, const T& s38, const T& s39, const T& s40
              )
      : std::vector<T>(40)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
      std::vector<T>::at(36)=s37;
      std::vector<T>::at(37)=s38;
      std::vector<T>::at(38)=s39;
      std::vector<T>::at(39)=s40;
    }
    //! constructor from 41 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36,
               const T& s37, const T& s38, const T& s39, const T& s40, const T& s41
              )
      : std::vector<T>(41)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
      std::vector<T>::at(36)=s37;
      std::vector<T>::at(37)=s38;
      std::vector<T>::at(38)=s39;
      std::vector<T>::at(39)=s40;
      std::vector<T>::at(40)=s41;
    }
    //! constructor from 42 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36,
               const T& s37, const T& s38, const T& s39, const T& s40, const T& s41, const T& s42
              )
      : std::vector<T>(42)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
      std::vector<T>::at(36)=s37;
      std::vector<T>::at(37)=s38;
      std::vector<T>::at(38)=s39;
      std::vector<T>::at(39)=s40;
      std::vector<T>::at(40)=s41;
      std::vector<T>::at(41)=s42;
    }
    //! constructor from 43 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36,
               const T& s37, const T& s38, const T& s39, const T& s40, const T& s41, const T& s42,
               const T& s43
              )
      : std::vector<T>(43)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
      std::vector<T>::at(36)=s37;
      std::vector<T>::at(37)=s38;
      std::vector<T>::at(38)=s39;
      std::vector<T>::at(39)=s40;
      std::vector<T>::at(40)=s41;
      std::vector<T>::at(41)=s42;
      std::vector<T>::at(42)=s43;
    }
    //! constructor from 44 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36,
               const T& s37, const T& s38, const T& s39, const T& s40, const T& s41, const T& s42,
               const T& s43, const T& s44
              )
      : std::vector<T>(44)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
      std::vector<T>::at(36)=s37;
      std::vector<T>::at(37)=s38;
      std::vector<T>::at(38)=s39;
      std::vector<T>::at(39)=s40;
      std::vector<T>::at(40)=s41;
      std::vector<T>::at(41)=s42;
      std::vector<T>::at(42)=s43;
      std::vector<T>::at(43)=s44;
    }
    //! constructor from 45 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36,
               const T& s37, const T& s38, const T& s39, const T& s40, const T& s41, const T& s42,
               const T& s43, const T& s44, const T& s45
              )
      : std::vector<T>(45)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
      std::vector<T>::at(36)=s37;
      std::vector<T>::at(37)=s38;
      std::vector<T>::at(38)=s39;
      std::vector<T>::at(39)=s40;
      std::vector<T>::at(40)=s41;
      std::vector<T>::at(41)=s42;
      std::vector<T>::at(42)=s43;
      std::vector<T>::at(43)=s44;
      std::vector<T>::at(44)=s45;
    }
    //! constructor from 46 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36,
               const T& s37, const T& s38, const T& s39, const T& s40, const T& s41, const T& s42,
               const T& s43, const T& s44, const T& s45, const T& s46
              )
      : std::vector<T>(46)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
      std::vector<T>::at(36)=s37;
      std::vector<T>::at(37)=s38;
      std::vector<T>::at(38)=s39;
      std::vector<T>::at(39)=s40;
      std::vector<T>::at(40)=s41;
      std::vector<T>::at(41)=s42;
      std::vector<T>::at(42)=s43;
      std::vector<T>::at(43)=s44;
      std::vector<T>::at(44)=s45;
      std::vector<T>::at(45)=s46;
    }
    //! constructor from 47 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36,
               const T& s37, const T& s38, const T& s39, const T& s40, const T& s41, const T& s42,
               const T& s43, const T& s44, const T& s45, const T& s46, const T& s47
              )
      : std::vector<T>(47)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
      std::vector<T>::at(36)=s37;
      std::vector<T>::at(37)=s38;
      std::vector<T>::at(38)=s39;
      std::vector<T>::at(39)=s40;
      std::vector<T>::at(40)=s41;
      std::vector<T>::at(41)=s42;
      std::vector<T>::at(42)=s43;
      std::vector<T>::at(43)=s44;
      std::vector<T>::at(44)=s45;
      std::vector<T>::at(45)=s46;
      std::vector<T>::at(46)=s47;
    }
    //! constructor from 48 T objects
    Collection(const T& s1, const T& s2, const T& s3, const T& s4, const T& s5, const T& s6,
               const T& s7, const T& s8, const T& s9, const T& s10, const T& s11, const T& s12,
               const T& s13, const T& s14, const T& s15, const T& s16, const T& s17, const T& s18,
               const T& s19, const T& s20, const T& s21, const T& s22, const T& s23, const T& s24,
               const T& s25, const T& s26, const T& s27, const T& s28, const T& s29, const T& s30,
               const T& s31, const T& s32, const T& s33, const T& s34, const T& s35, const T& s36,
               const T& s37, const T& s38, const T& s39, const T& s40, const T& s41, const T& s42,
               const T& s43, const T& s44, const T& s45, const T& s46, const T& s47, const T& s48
              )
      : std::vector<T>(48)
    {
      std::vector<T>::at(0)=s1;
      std::vector<T>::at(1)=s2;
      std::vector<T>::at(2)=s3;
      std::vector<T>::at(3)=s4;
      std::vector<T>::at(4)=s5;
      std::vector<T>::at(5)=s6;
      std::vector<T>::at(6)=s7;
      std::vector<T>::at(7)=s8;
      std::vector<T>::at(8)=s9;
      std::vector<T>::at(9)=s10;
      std::vector<T>::at(10)=s11;
      std::vector<T>::at(11)=s12;
      std::vector<T>::at(12)=s13;
      std::vector<T>::at(13)=s14;
      std::vector<T>::at(14)=s15;
      std::vector<T>::at(15)=s16;
      std::vector<T>::at(16)=s17;
      std::vector<T>::at(17)=s18;
      std::vector<T>::at(18)=s19;
      std::vector<T>::at(19)=s20;
      std::vector<T>::at(20)=s21;
      std::vector<T>::at(21)=s22;
      std::vector<T>::at(22)=s23;
      std::vector<T>::at(23)=s24;
      std::vector<T>::at(24)=s25;
      std::vector<T>::at(25)=s26;
      std::vector<T>::at(26)=s27;
      std::vector<T>::at(27)=s28;
      std::vector<T>::at(28)=s29;
      std::vector<T>::at(29)=s30;
      std::vector<T>::at(30)=s31;
      std::vector<T>::at(31)=s32;
      std::vector<T>::at(32)=s33;
      std::vector<T>::at(33)=s34;
      std::vector<T>::at(34)=s35;
      std::vector<T>::at(35)=s36;
      std::vector<T>::at(36)=s37;
      std::vector<T>::at(37)=s38;
      std::vector<T>::at(38)=s39;
      std::vector<T>::at(39)=s40;
      std::vector<T>::at(40)=s41;
      std::vector<T>::at(41)=s42;
      std::vector<T>::at(42)=s43;
      std::vector<T>::at(43)=s44;
      std::vector<T>::at(44)=s45;
      std::vector<T>::at(45)=s46;
      std::vector<T>::at(46)=s47;
      std::vector<T>::at(47)=s48;
    }

    void print(std::ostream& out) const //! print utility
    {
      typename std::vector<T>::const_iterator its=std::vector<T>::begin();
      number_t i=1;
      for(; its!=std::vector<T>::end(); ++its, ++i)
      {
        out << " item " << i << " : " << (*its);
        if (i < std::vector<T>::size()) { out << eol; }
      }
    }

    void print(PrintStream& os) const { print(os.currentStream()); } //!< print utility
};

template<>
inline Collection<number_t>::Collection(int n, const number_t& s)
: std::vector<number_t>(2)
{
  std::vector<number_t>::at(0)=n;
  std::vector<number_t>::at(1)=s;
}

template<typename T>
std::ostream& operator<<(std::ostream& out, const Collection<T>& ns) //! print utility
{ns.print(out); return out;}

template<typename T>
Collection<T>& operator<<(Collection<T>& ns, const T& n) //! insertion utility
{
  ns.push_back(n);
  return ns;
}

//===============================================================================
/*!
  \class PCollectionItem
  utility class for xlifepp::PCollection class to access to an item of a collection of pointers
*/
//===============================================================================
template<typename T> class PCollectionItem
{
  public:
    typename std::vector<T*>::iterator itp;
    PCollectionItem(typename std::vector<T*>::iterator it) : itp(it){} //!< constructor
    T& operator = (const T& t){*itp=const_cast<T*>(&t); return **itp;} //!< assignment operator
    operator T&(){return **itp;}  //! autocast PCollectionItem->T&
    void print(std::ostream& out) const //! print utility
    { out << **itp << eol; }
    void print(PrintStream& os) const //! print utility
    {print(os.currentStream());}
};

template <typename T>
std::ostream& operator<<(std::ostream& out,const PCollectionItem<T>& item) //! output PCollectionItem on stream
{ item.print(out); return out; }

/*! \class PCollection
    a simple interface to std::vector<T*>
    hide pointers, add the insertion operator <<, the constructors from enumerating items
    and access to items using operator ()

    Be cautious when using it because pointers are shared (no copy).
    For instance, if you plan to create a list of T with a loop, do the following

    PCollection<T> ts(10);
    for(int i=0;i<10;++i)
      ts[i] = new T(...);
    and clear the T pointers using:  ts.clearPointers();

    to access to item as const  reference you can use either ts(i+1) (same as *ts[i])


*/
template <typename T>
class PCollection : public std::vector<T*>
{
  public:
    //! access to n-th number (const)
    const T & operator()(number_t n) const
    {
      if(n<1 || n>std::vector<T*>::size()) {theCout<<"item "<<n<<" : out of bounds"; exit(-1);}
      return *std::vector<T*>::at(n-1);
    }
    //! access to n-th number (non const)
    PCollectionItem<T> operator()(number_t n)
    {
      if(n<1 || n> std::vector<T*>::size()) {theCout<<"item "<<n<<" : out of bounds"; exit(-1);}
      return PCollectionItem<T>(std::vector<T*>::begin()+(n-1));
    }

    //@{
    //! constructor from size
    explicit PCollection(number_t n=0) : std::vector<T*>(n,(T*)(0)) {}
    explicit PCollection(int n) : std::vector<T*>(n,(T*)(0)) {}
    //@}

    //! constructor from std::vector<T*>
    PCollection(const std::vector<T*>& ts) : std::vector<T*>(ts) {}
    //! constructor from std::initializer_list<T*>
    PCollection(const std::initializer_list<T*>& ts) : std::vector<T*>(ts) {}
    //! constructor from 1 T objects
    PCollection(const T& t1)
      : std::vector<T*>(1)
    {
      std::vector<T*>::at(0)=const_cast<T*>(&t1);
    }
    //! constructor from 2 T objects
    PCollection(const T& t1, const T& t2)
      : std::vector<T*>(2)
    {
      std::vector<T*>::at(0)=const_cast<T*>(&t1);
      std::vector<T*>::at(1)=const_cast<T*>(&t2);
    }
    //! constructor from 3 T objects
    PCollection(const T& t1, const T& t2, const T& t3)
      : std::vector<T*>(3)
    {
      std::vector<T*>::at(0)=const_cast<T*>(&t1);
      std::vector<T*>::at(1)=const_cast<T*>(&t2);
      std::vector<T*>::at(2)=const_cast<T*>(&t3);
    }
    //! constructor from 4 T objects
    PCollection(const T& t1, const T& t2, const T& t3, const T& t4)
      : std::vector<T*>(4)
    {
      std::vector<T*>::at(0)=const_cast<T*>(&t1);
      std::vector<T*>::at(1)=const_cast<T*>(&t2);
      std::vector<T*>::at(2)=const_cast<T*>(&t3);
      std::vector<T*>::at(3)=const_cast<T*>(&t4);
    }
    //! constructor from 5 T objects
    PCollection(const T& t1, const T& t2, const T& t3, const T& t4, const T& t5)
      : std::vector<T*>(5)
    {
      std::vector<T*>::at(0)=const_cast<T*>(&t1);
      std::vector<T*>::at(1)=const_cast<T*>(&t2);
      std::vector<T*>::at(2)=const_cast<T*>(&t3);
      std::vector<T*>::at(3)=const_cast<T*>(&t4);
      std::vector<T*>::at(4)=const_cast<T*>(&t5);
    }
    //! constructor from 6 T objects
    PCollection(const T& t1, const T& t2, const T& t3, const T& t4, const T& t5, const T& t6)
      : std::vector<T*>(6)
    {
      std::vector<T*>::at(0)=const_cast<T*>(&t1);
      std::vector<T*>::at(1)=const_cast<T*>(&t2);
      std::vector<T*>::at(2)=const_cast<T*>(&t3);
      std::vector<T*>::at(3)=const_cast<T*>(&t4);
      std::vector<T*>::at(4)=const_cast<T*>(&t5);
      std::vector<T*>::at(5)=const_cast<T*>(&t6);
    }
    //! constructor from 7 T objects
    PCollection(const T& t1, const T& t2, const T& t3, const T& t4, const T& t5, const T& t6,
                const T& t7)
      : std::vector<T*>(7)
    {
      std::vector<T*>::at(0)=const_cast<T*>(&t1);
      std::vector<T*>::at(1)=const_cast<T*>(&t2);
      std::vector<T*>::at(2)=const_cast<T*>(&t3);
      std::vector<T*>::at(3)=const_cast<T*>(&t4);
      std::vector<T*>::at(4)=const_cast<T*>(&t5);
      std::vector<T*>::at(5)=const_cast<T*>(&t6);
      std::vector<T*>::at(6)=const_cast<T*>(&t7);
    }
    //! constructor from 8 T objects
    PCollection(const T& t1, const T& t2, const T& t3, const T& t4, const T& t5, const T& t6,
                const T& t7, const T& t8)
      : std::vector<T*>(8)
    {
      std::vector<T*>::at(0)=const_cast<T*>(&t1);
      std::vector<T*>::at(1)=const_cast<T*>(&t2);
      std::vector<T*>::at(2)=const_cast<T*>(&t3);
      std::vector<T*>::at(3)=const_cast<T*>(&t4);
      std::vector<T*>::at(4)=const_cast<T*>(&t5);
      std::vector<T*>::at(5)=const_cast<T*>(&t6);
      std::vector<T*>::at(6)=const_cast<T*>(&t7);
      std::vector<T*>::at(7)=const_cast<T*>(&t8);
    }
    //! constructor from 9 T objects
    PCollection(const T& t1, const T& t2, const T& t3, const T& t4, const T& t5, const T& t6,
                const T& t7, const T& t8, const T& t9)
      : std::vector<T*>(9)
    {
      std::vector<T*>::at(0)=const_cast<T*>(&t1);
      std::vector<T*>::at(1)=const_cast<T*>(&t2);
      std::vector<T*>::at(2)=const_cast<T*>(&t3);
      std::vector<T*>::at(3)=const_cast<T*>(&t4);
      std::vector<T*>::at(4)=const_cast<T*>(&t5);
      std::vector<T*>::at(5)=const_cast<T*>(&t6);
      std::vector<T*>::at(6)=const_cast<T*>(&t7);
      std::vector<T*>::at(7)=const_cast<T*>(&t8);
      std::vector<T*>::at(8)=const_cast<T*>(&t9);
    }
    //! constructor from 10 T objects
    PCollection(const T& t1, const T& t2, const T& t3, const T& t4, const T& t5, const T& t6,
                const T& t7, const T& t8, const T& t9, const T& t10)
      : std::vector<T*>(10)
    {
      std::vector<T*>::at(0)=const_cast<T*>(&t1);
      std::vector<T*>::at(1)=const_cast<T*>(&t2);
      std::vector<T*>::at(2)=const_cast<T*>(&t3);
      std::vector<T*>::at(3)=const_cast<T*>(&t4);
      std::vector<T*>::at(4)=const_cast<T*>(&t5);
      std::vector<T*>::at(5)=const_cast<T*>(&t6);
      std::vector<T*>::at(6)=const_cast<T*>(&t7);
      std::vector<T*>::at(7)=const_cast<T*>(&t8);
      std::vector<T*>::at(8)=const_cast<T*>(&t9);
      std::vector<T*>::at(9)=const_cast<T*>(&t10);
    }

    void clearPointers()
    {
      typename std::vector<T*>::iterator it=std::vector<T*>::begin();
      for(; it!=std::vector<T*>::end(); ++it) delete *it;
      std::vector<T*>::clear();
    }

    void print(std::ostream& out) const //! print utility
    {
      typename std::vector<T*>::const_iterator it=std::vector<T*>::begin();
      number_t i=1;
      for(; it!=std::vector<T*>::end(); ++it, ++i)
      {
        out<<" T_"<<i<<" : ";
        if (*it!=nullptr)
        {
          out << *(*it);
          if (i < std::vector<T*>::size()) { out << eol; }
        }
        else
        {
          out << "no domain";
        }
      }
    }

    void print(PrintStream& os) const { print(os.currentStream()); } //!< print utility
};

template <typename T>
std::ostream& operator<<(std::ostream& out, const PCollection<T>& ts) //! print utility
{ts.print(out); return out;}

template <typename T>
PCollection<T>& operator<<(PCollection<T>& ts, const T& t) //! insertion operator
{
  ts.push_back(const_cast<T*>(&t));
  return ts;
}

//some useful aliases
typedef Collection<string_t> Strings;   //!< collection of String
typedef Collection<number_t> Numbers;   //!< collection of Number
typedef Collection<int_t> Ints;         //!< collection of Int
typedef Collection<Point> Points;       //!< collection of Point
typedef Collection<Points> PointMatrix; //!< collection of Points

//! join utility
inline string_t join(const Strings& ss, const string_t& delim)
{
  if(ss.size()==0) return "";
  string_t s=ss[0];
  for (number_t i=1; i < ss.size(); ++i) s+=delim+ss[i];
  return s;
}

//! split a string into vector of strings using blank space as default delimiter
inline Strings split(const string_t& s, char delim = ' ')
{
  Strings words;
  int j=0;
  for (int i=0; i<s.length(); ++i)
  {
    if (s[i] == delim)
    {
      words.push_back(s.substr(j,i-j));
      j=i+1;
    }
  }
  words.push_back(s.substr(j));
  return words;
}

} // end of namespace xlifepp;

#endif // COLLECTION_HPP_INCLUDED
