/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Messages.hpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 25 jun 2003
  \date 3 may 2012

  \brief Definition of the xlifepp::Messages class

  Classes to handle XLiFE++ messages and error messages according to language
    - xlifepp::MsgData   defines a structure which contains temporary message data
    - xlifepp::MsgFormat stores a message format
    - xlifepp::Messages  stores the list of message formats as a std::map

  to add a new message class:
    - make a new message file (one for each language) : theMessageToUser_.txt
    - create a new Messages class: myMessages = new Messages(msgFile,outStream,msgType)

  to output a message:
      - msg(myMessages, msgId, msgData)

  main usage is to create that list by loading a file of message formats.

  such file has the following format records:
          @ string_id [ERROR/WARNING/INFO] [STOP/CONT] [STDOUT/NONE] string %x string %x ... \n
  where %x data are %i (integer), %r (real), %c (complex), %s (string), %b (bool)

  a comment begins with '@/' and ends at the next '@' or eof

  Example of records [en]
          \verbatim
          @ file_not_found  ERROR STOP STDOUT %s: File not found
          @ fe_constr       INFO  CONT STDOUT Finite Element space "%s" on domain "%s" of mesh "%s".
          \endverbatim
 */

#ifndef MESSAGES_HPP
#define MESSAGES_HPP

#include <map>
#include <fstream>

#include "config.h"
#include "String.hpp"
#ifdef XLIFEPP_WITH_OMP
  #include <omp.h>
#endif // XLIFEPP_WITH_OMP

namespace xlifepp
{

/*!
  \class MsgData

  \brief data to pass to message handling engine

  define a structure which contains temporary data to pass to message handling engine \n
  data structure allows any of the following types in any order:
          string, real, complex, int or boolean \n
  such data is unlimited in number as far as memory allows it
 */
class MsgData
{
  private:
    std::vector<int_t>  i_;    //!< storage for signed int type data
    std::vector<real_t> r_;    //!< storage for real type data
    std::vector<complex_t> c_; //!< storage for complex type data
    std::vector<string_t> s_;  //!< storage for string type data
    std::vector<bool> b_;      //!< storage for boolean type data
    bool read_;                //!< flag to specify that the structure has been read

  public:
    //! default constructor
    MsgData()
      : i_(0), r_(0), c_(0), s_(0), b_(0), read_(true) {}

    //------------------------------------------------------------------------------
    // Public inline functions
    //------------------------------------------------------------------------------

  public:
    //! access to int_t data
    int_t intParameter(const number_t n) const
    {
      if (n < i_.size()) { return i_[n]; }
      else { return 0; }
    }

    //! access to real
    real_t realParameter(const number_t n) const
    {
      if (n < r_.size()) { return r_[n]; }
      else { return 0.; }
    }

    //! access to complex
    complex_t complexParameter(const number_t n) const
    {
      if (n < c_.size()) { return c_[n]; }
      else { return complex_t(0., 0.); }
    }

    //! access to string
    string_t stringParameter(const number_t n) const
    {
      if (n < s_.size()) { return s_[n]; }
      else { return ""; }
    }

    //! access to boolean
    bool booleanParameter(const number_t n) const
    {
      if (n < b_.size()) { return b_[n]; }
      else { return false; }
    }

    //! read status
    bool read() const { return read_; }

    //! declare data read operator
    void readData() { read_ = true; }

    //! appends an int
    void push(const int i)
    {
      reset_();
      i_.push_back(static_cast<int_t>(i));
      read_ = false;
    }

    //! appends a long int
    void push(const long int i)
    {
      reset_();
      i_.push_back(static_cast<int_t>(i));
      read_ = false;
    }

    //! appends a long longint
    void push(const long long int i)
    {
      reset_();
      i_.push_back(static_cast<int_t>(i));
      read_ = false;
    }
    //! appends an unsigned int
    void push(const unsigned int i)
    {
      reset_();
      i_.push_back(static_cast<number_t>(i));
      read_ = false;
    }

    //! appends an unsigned long int
    void push(const unsigned long int i)
    {
      reset_();
      i_.push_back(static_cast<number_t>(i));
      read_ = false;
    }
    //! appends an unsigned long long int
    void push(const unsigned long long int i)
    {
      reset_();
      i_.push_back(static_cast<number_t>(i));
      read_ = false;
    }
    //! appends an short unsigned int
    void push(const short unsigned int i)
    {
      reset_();
      i_.push_back(static_cast<number_t>(i));
      read_ = false;
    }

    //! appends a real_t
    void push(const real_t r)
    {
      reset_();
      r_.push_back(r);
      read_ = false;
    }

    //! appends a complex_t
    void push(const complex_t& c)
    {
      reset_();
      c_.push_back(c);
      read_ = false;
    }

    //! appends a string_t
    void push(const string_t& s)
    {
      reset_();
      s_.push_back(s);
      read_ = false;
    }

    //! appends a boolean
    void push(const bool b)
    {
      reset_();
      b_.push_back(b);
      read_ = false;
    }

    //! appends a string
    void push(const char* s)
    {
      reset_();
      s_.push_back(string_t(s));
      read_ = false;
    }

    //! appends an int via streaming
    MsgData& operator<<(const int i)
    {
      push(i);
      return *this;
    }

    //! appends a long int via streaming
    MsgData& operator<<(const long int i)
    {
      push(i);
      return *this;
    }

    //! appends a long long int via streaming
    MsgData& operator<<(const long long int i)
    {
      push(i);
      return *this;
    }

    //! appends an unsigned int via streaming
    MsgData& operator<<(const unsigned int i)
    {
      push(i);
      return *this;
    }

    //! appends an unsigned long int via streaming
    MsgData& operator<<(const unsigned long int i)
    {
      push(i);
      return *this;
    }

    //! appends an unsigned long long int via streaming
    MsgData& operator<<(const unsigned long long int i)
    {
      push(i);
      return *this;
    }

    //! appends an short unsigned int via streaming
    MsgData& operator<<(const short unsigned int i)
    {
      push(i);
      return *this;
    }

    //! appends a real_t via streaming
    MsgData& operator<<(const real_t r)
    {
      push(r);
      return *this;
    }

    //! appends a complex via streaming
    MsgData& operator<<(const complex_t& c)
    {
      push(c);
      return *this;
    }

    //! appends a string via streaming
    MsgData& operator<<(const string_t& s)
    {
      push(s);
      return *this;
    }

    //! appends a boolean via streaming
    MsgData& operator<<(const bool b)
    {
      push(b);
      return *this;
    }

    //! appends a string via streaming
    MsgData& operator<<(const char* s)
    {
      push(s);
      return *this;
    }

  private:
    //! resets MsgData structure
    void reset_()
    {
      if ( !read_ ) { return; }

      i_.clear();
      r_.clear();
      c_.clear();
      s_.clear();
      b_.clear();
    }

}; // end of class MsgData

//! Message types enumeration
enum MsgType {_error = 0, _warning, _info};
//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const MsgType& mt);

/*!
   \class MsgFormat
   store a message format for output
 */
class MsgFormat
{
  private:
    string_t format_;   //!< format of message
    MsgType type_;    //!< flag for warning/error
    bool stop_;       //!< flag for stop/continue
    bool consoleOut_; //!< flag for output to console
    string_t ids_;      //!< string id of format (a mnemotechnic word)

  public:
    //------------------------------------------------------------------------------
    // Constructors, Destructor
    //------------------------------------------------------------------------------

    MsgFormat() //! default constructor
      : format_(""), type_(_info), stop_(false), consoleOut_(false), ids_("") { }

    MsgFormat(const string_t& ms, const string_t& ids, MsgType t, bool s, bool c) //! full constructor for messages
      : format_(ms), type_(t), stop_(s), consoleOut_(c), ids_(ids) { }

    //------------------------------------------------------------------------------
    // Public Inline Access functions
    //------------------------------------------------------------------------------

    //! format_ accessor
    string_t format() const { return format_; }
    //! warning_ accessor
    MsgType type() const { return type_; }
    //! stop_ accessor
    bool stop() const { return stop_; }
    //! consoleOut_ accessor
    bool console() const { return consoleOut_; }
    //! ids_ accessor
    string_t stringId() const { return ids_; }

}; // end of MsgFormat class

/*!
  \class Messages
  store the list of message formats by type as a std::map is indexed by the message format
 */
class Messages
{
  private:
    string_t msgType_;                           //!< message type
    std::map<string_t, MsgFormat*> stringIndex_; //!< index of messages by string id
    std::ofstream* msgStream_p;                  //!< file stream where are sent messages
    string_t msgFile_;                           //!< file where are sent messages
  public:
    bool traceWhere;                             //!< internal flag for printing trace

    //--------------------------------------------------------------------------------
    // Constructors, Destructor
    //--------------------------------------------------------------------------------
    Messages(); //!< Default constructor
    Messages(const string_t& file, std::ofstream& out, const string_t& msgFile, const string_t& msgType = string_t("XLiFE++")); //!< Constructor from file
    ~Messages(); //!< Destructor

    //--------------------------------------------------------------------------------
    // Public inline access functions
    //--------------------------------------------------------------------------------
    number_t numberOfMessages() const //! returns number of messages
    { return stringIndex_.size(); }
    string_t msgType() const          //! returns type of message
    { return msgType_; }
    string_t msgFile() const          //! returns message file
    { return msgFile_; }
    std::ofstream* msgStream() const  //! returns message stream
    { return msgStream_p; }

    //--------------------------------------------------------------------------------
    // Public member function declarations
    //--------------------------------------------------------------------------------
    void loadFormat(const string_t&);     //!< load format from a file
    void printList(std::ofstream&);       //!< print the list of all messages
    MsgFormat* find(const string_t&);     //!< find an error format by its stringId (log complexity)
    void append(MsgFormat&);              //!< append a new message format in list
    void appendFromFile(const string_t&); //!< append a new message format in list

}; // end of Messages class

//--------------------------------------------------------------------------------
// External functions
//--------------------------------------------------------------------------------
void msg(const string_t& msgIds, MsgData& msgData, Messages* msgSrc, MsgType& msgType);                      //!< throw messages
string_t message(const string_t& msgIds, MsgData& msgData = theMessageData, Messages* msgSrc = theMessages_p); //!< build formated message
void error(const string_t& msgIds, MsgData& msgData = theMessageData, Messages* msgSrc = theMessages_p);     //!< throw error messages
void info(const string_t& msgIds, MsgData& msgData = theMessageData, Messages* msgSrc = theMessages_p);      //!< throw info messages
void warning(const string_t& msgIds, MsgData& msgData = theMessageData, Messages* msgSrc = theMessages_p);   //!< throw warning messages

//same as currentThread defined in Environment, define also here to avoid a cyclic dependance
inline number_t curThread() //! if omp is available, return the current thread number else return 0
{
  #ifdef XLIFEPP_WITH_OMP
    return omp_get_thread_num();
  #endif // XLIFEPP_WITH_OMP
  return 0;
}

//@{
//! templated message functions (shortcuts avoiding the use of MsgData object)
template <typename T>
string_t message(const string_t& msgIds, const T& v, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return "";
  theMessageData << v;
  return message(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2>
string_t message(const string_t& msgIds, const T1& v1, const T2& v2, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return "";
  theMessageData << v1 << v2;
  return message(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3>
string_t message(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return "";
  theMessageData << v1 << v2 << v3;
  return message(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4>
string_t message(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return "";
  theMessageData << v1 << v2 << v3 << v4;
  return message(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5>
string_t message(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return "";
  theMessageData << v1 << v2 << v3 << v4 << v5;
  return message(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
string_t message(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, const T6& v6, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return "";
  theMessageData << v1 << v2 << v3 << v4 << v5 << v6;
  return message(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
string_t message(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, const T6& v6, const T7& v7, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return "";
  theMessageData << v1 << v2 << v3 << v4 << v5 << v6 << v7;
  return message(msgIds, theMessageData, msgSrc);
}
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
string_t message(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, const T6& v6, const T7& v7, const T8& v8, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return "";
  theMessageData << v1 << v2 << v3 << v4 << v5 << v6 << v7 << v8;
  return message(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
string_t message(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, const T6& v6, const T7& v7, const T8& v8, const T9& v9, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return "";
  theMessageData << v1 << v2 << v3 << v4 << v5 << v6 << v7 << v8 << v9;
  return message(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
string_t message(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, const T6& v6, const T7& v7, const T8& v8, const T9& v9, const T10& v10, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return "";
  theMessageData << v1 << v2 << v3 << v4 << v5 << v6 << v7 << v8 << v9 << v10;
  return message(msgIds, theMessageData, msgSrc);
}

template <typename T>
void error(const string_t& msgIds, const T& v, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v;
  error(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2>
void error(const string_t& msgIds, const T1& v1, const T2& v2, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return ;
  theMessageData << v1 << v2;
  error(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3>
void error(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3;
  error(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4>
void error(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3 << v4;
  error(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5>
void error(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3 << v4 << v5;
  error(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
void error(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, const T6& v6, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3 << v4 << v5 << v6;
  error(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
void error(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, const T6& v6, const T7& v7, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3 << v4 << v5 << v6 << v7;
  error(msgIds, theMessageData, msgSrc);
}

template <typename T>
void warning(const string_t& msgIds, const T& v, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v;
  warning(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2>
void warning(const string_t& msgIds, const T1& v1, const T2& v2, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2;
  warning(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3>
void warning(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3;
  warning(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4>
void warning(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3 << v4;
  warning(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5>
void warning(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3 << v4 << v5;
  warning(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
void warning(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, const T6& v6, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3 << v4 << v5 << v6;
  warning(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
void warning(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, const T6& v6, const T7& v7, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3 << v4 << v5 << v6 << v7;
  warning(msgIds, theMessageData, msgSrc);
}

template <typename T>
void info(const string_t& msgIds, const T& v, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v;
  info(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2>
void info(const string_t& msgIds, const T1& v1, const T2& v2, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2;
  info(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3>
void info(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3;
  info(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4>
void info(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3 << v4;
  info(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5>
void info(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3 << v4 << v5;
  info(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
void info(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, const T6& v6, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3 << v4 << v5 << v6;
  info(msgIds, theMessageData, msgSrc);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
void info(const string_t& msgIds, const T1& v1, const T2& v2, const T3& v3, const T4& v4, const T5& v5, const T6& v6, const T7& v7, Messages* msgSrc = theMessages_p)
{
  if(curThread()>0) return;
  theMessageData << v1 << v2 << v3 << v4 << v5 << v6 << v7;
  info(msgIds, theMessageData, msgSrc);
}
//@}

//--------------------------------------------------------------------------------
// Global Scope Message objects
//--------------------------------------------------------------------------------
extern Messages* theMessages;  //!< all messages
extern MsgData theMessageData; //!< global data message object

} // end of namespace xlifepp

#endif /* MESSAGES_HPP */
