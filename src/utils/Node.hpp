/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
   \file Node.hpp
   \authors E. Lunéville
   \since 9 jul 2014
   \date 9 jul 2014

  \brief Definition of the xlifepp::Node class

  A minimal node template class for tree management
*/
#ifndef NODE_HPP
#define NODE_HPP

#include "config.h"

namespace xlifepp
{

//================================================================================
/*!
  \class Node
  node of a tree
*/
//================================================================================
template <class T>
class Node
{
public:
  number_t level_;    //!< level in a tree
  Node * parent_;     //!< pointer to parent node (0 if root node)
  Node * child_;      //!< pointer to first child node
  Node * right_;      //!< pointer to right node
  T * obj_;           //!< object pointer linked to node

public:
  //constructors, destructor
  //! default constructor
  Node() : level_(0), parent_(nullptr),child_(nullptr),right_(nullptr),obj_(nullptr) {}
  //! basic constructor
  Node(T *O, Node * p, number_t l)
   : level_(l), parent_(p), child_(nullptr),right_(nullptr), obj_(O) {}
  ~Node(); //!< destructor

   //accessors
   T* objectp() const   //! return object pointer linked to node
   {return obj_;}
   bool endNode() const //! true if a end node (no child)
   {return child_==nullptr;}


   //output utilities
   void print(std::ostream&, const string_t & = "") const; //!< print utility
   void print(PrintStream& os, const string_t &s = "") const {print(os.currentStream(), s);}
   template <class S>
   friend std::ostream& operator<<(std::ostream& os, const Node<S> & node);
};

template <class T>
Node<T>::~Node()
{
  // recursive method, limited by stack
  //  if (child_!=nullptr) delete child_;
  //  if (right_!=nullptr) delete right_;
  // non recursive method
  Node *n=child_, *np, *nq;
  if(n==nullptr) return;
  while (n!=this)
    {
     if(n->child_ !=nullptr) {n=n->child_;}
     else
     {
         if(n->right_ !=nullptr) {n=n->right_;}
         else
         {
            n=n->parent_; //kill all children
            np = n->child_;
            while(np!=nullptr)
            {
                nq=np->right_;
                delete np;
                np=nq;
            }
            n->child_=nullptr;
         }
     }
    }
}

template <class T>
void Node<T>::print(std::ostream& os, const string_t& s) const
{
  os << s << "lev " << tostring(level_) << " : " << *obj_ << eol;
  string_t s2=s;
  if (child_!=nullptr) {child_->print(os, s2+="    ");}
  if (right_!=nullptr) {right_->print(os, s);}
}

//! print operator
template <class S>
std::ostream& operator<<(std::ostream& os, const Node<S> & node)
{
  string_t s="";
  node.print(os,s);
  return os;
}

} // end of namespace xlifepp

#endif /* NODE_HPP */
