/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file KdTree.hpp
  \authors E. Lunéville
  \since 15 mar 2014
  \date 15 mar 2014

  \brief Definition of the xlifepp::KdTree class

  A xlifepp::KdTree is a binary tree partitioning space in boxes such that any box contains only one object of a given set of objects
  The class KdTree is templated by the type of objects (T)
  and uses the template xlifepp::KdNode (defined here)

  It requires that objects have separated components and an ordering relation by component. The T class has to provide
    - a separator value type: T::SepValueType
    - the access T::operator()(int) to the c component of object (c>=1)
    - the dimension of object, T::dim()
    - the component comparison functions:
        int compare(const Object& O, int c, const T::SepValueType& s)
           return  -1 (O_c<s), 1 (O_c>s) or 0 (O_c=s)
        int maxSeparator(const Object& O1, const Object& O2, int &c, T::SepValueType& s)  c=1,2,..
           find the component index c where separation is maximal, update s as the middle value
           and return the comparison of O1 and O2 (1,-1,0)

  NOTE: For the moment, the kdtree construction is based on a point by point insertion process, leading to possibly unbalanced tree
        TO BE IMPROVED LATER
*/

#ifndef KD_TREE_HPP
#define KD_TREE_HPP

#include "config.h"

namespace xlifepp
{
/*!
  \class Box
  utility class for printing KdTree
*/
template <typename T>
class Box
{
  public:
    std::vector<T> vmin; //!< first vertex (minimum values of coordinates)
    std::vector<T> vmax; //!< second vertex (mazximum values of coordinates)
    //! default constructor
    Box(number_t dim=1) {vmin.resize(dim,T(0)); vmax.resize(dim,T(0));}
    //! constructor from 1D coordinates
    Box(const T& x1, const T& x2) {vmin.resize(1,x1); vmax.resize(1,x2);}
    //! constructor from 2D coordinates
    Box(const T& x1, const T& x2, const T& y1, const T& y2)
      { vmin.resize(2); vmax.resize(2); vmin[0]=x1; vmin[1]=y1; vmax[0]=x2; vmax[1]=y2; }
    //! constructor from 3D coordinates
    Box(const T& x1, const T& x2, const T& y1, const T& y2, const T& z1, const T& z2)
      { vmin.resize(3); vmax.resize(3); vmin[0]=x1; vmin[1]=y1; vmin[2]=z1; vmax[0]=x2; vmax[1]=y2; vmax[2]=z2; }
    //! print utility
    void print(std::ostream& os) const
    {
      number_t d=vmin.size();
      for(number_t i=0; i<d; i++)  os<<vmin[i]<<" ";
      for(number_t i=0; i<d; i++)  os<<vmax[i]<<" ";
      os<<"\n";
    }
    void print(PrintStream& os) const {print(os.currentStream());}
};

/*!
  \class KdNode
  node of a KdTree
*/
template <class T>
class KdNode
{
  private:
   typedef typename T::SepValueType SvType;   //!< separator value type
   KdNode * parent_;                          //!< pointer to parent node
   KdNode * left_;                            //!< pointer to left node
   KdNode * right_;                           //!< pointer to right node
   const T * obj_;                            //!< object pointer linked to node
   int separator_;                            //!< separation component
   SvType sv_;                                //!< separation value

  public:
   //constructors, destructor
   KdNode() : parent_(nullptr),left_(nullptr),right_(nullptr),obj_(nullptr),separator_(1),sv_(0) {}                             //!< default constructor
   KdNode(const T *O,int s, KdNode * p) : parent_(p),left_(nullptr),right_(nullptr),obj_(O),separator_(s),sv_(0) {} //!< basic constructor
   ~KdNode(); //!< destructor

   const T* objectp() const                   //! return object pointer linked to node
   {return obj_;}
   bool isVoid() const                        //! true if a void node
   {return obj_==nullptr && left_==nullptr && right_==nullptr;}

   void insert(const T *);           //!< node insertion
   number_t& depth(number_t &) const;    //!< return node depth
   void searchNearest(const T *, const T *&, SvType &);  //!< search in tree from current node

   //output utilities
   void print(std::ostream&, std::string &) const; //!< print utility
   void print(PrintStream& os, std::string& s) const {print(os.currentStream(),s);}
   void printBoxes(std::ostream &os, Box<SvType> b) const; //!< print utility
   void printBoxes(PrintStream& os, Box<SvType> b) const {printBoxes(os.currentStream(), b);}
   template <class S>
   friend std::ostream& operator<<(std::ostream& os,const KdNode<S> & node);

   //static number_t countSearch; //!< global counter
};

/*!
  \class KdTree
  definition of a Kd-tree
*/
template <class T>
class KdTree
{
  private:
    typedef typename T::SepValueType SvType;   //!< separator value type
    KdNode<T>* root;                           //!< rootnode of the tree
    dimen_t dimT;                                //!< object dimension, set during insertion of first object

  public:
    //constructor, destructor
    KdTree() {root=new KdNode<T>();dimT=0;} //!< default constructor
    KdTree(const std::vector<T>&);          //!< basic constructor
    ~KdTree() {if(root!=nullptr) delete root;}    //!< destructor

    bool isVoid() const                     //! true if a void tree
      { return root->isVoid(); }
    dimen_t dim() const {return dimT;}      //!< returns dimension

    void clear();                           //!< clear KdTree
    void insert(const T&);                  //!< insert object
    void insert(const std::vector<T>&);     //!< insert objects set given by vector
    template <typename iterator>
    void insert(iterator, iterator);        //!< insert objects set given by an iterator range
    number_t depth() const;                 //!< return the depth of the tree
    const T* searchNearest(const T &) const;//!< search the nearest Object in the KdTree of a given Object

    // print utilities
    void print(std::ostream& out) const; //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());}
    template <typename R>
    void printBoxes(std::ostream &os, const Box<R>& rb); //!< print utility
    template <typename R>
    void printBoxes(PrintStream &os, const Box<R>& rb) {printBoxes(os.currentStream(), rb);}
    template <typename R>
    void printBoxes(const std::string& file, const Box<R>& rb); //!< print utility

    template <class S>
    friend std::ostream& operator<<(std::ostream& os,const KdTree<S>& node);
};

//================================================================================
// member functions of KdTree
//================================================================================

//template <class T> number_t KdNode<T>::countSearch;

template <class T>
void KdTree<T>::clear()
{
    if(root!=nullptr) delete root;
    root=new KdNode<T>();  //set first Kdnode
}

//construct kdtree from vector<T>
template <class T>
KdTree<T>::KdTree(const std::vector<T>& V)
{
   root=new KdNode<T>();
   insert(V);
}

//insert an object in tree
template <class T>
void KdTree<T>::insert(const T & V)
{
   if(isVoid()) dimT=V.dim();  //first insertion, set dim
   root->insert(&V);
}

//insert a set of objects in tree given by iterator
template <class T>
template <typename iterator>
void KdTree<T>::insert(iterator itb, iterator ite)
{
   if(isVoid()) dimT=(*itb).dim();  //first insertion, set dim
   iterator it=itb;
   for(; it!=ite; it++) root->insert(&(*it));
}

//insert a set of objects in tree
template <class T>
void KdTree<T>::insert(const std::vector<T>& V)
{
   if(V.size()==0) return;
   if(isVoid()) dimT=V[0].dim();  //first insertion, set dim
   typename std::vector<T>::const_iterator it;
   for(it=V.begin(); it!=V.end(); it++)
   {
      root->insert(&(*it));
   }
}

//search the nearest object in KdTree
template <class T>
const T* KdTree<T>::searchNearest(const T & O) const
{
   //KdNode<T>::countSearch = 0;
   typename T::SepValueType r;
   r=std::numeric_limits<typename T::SepValueType>::max()/2;
   const T* best=nullptr;
   root->searchNearest(&O, best, r);
   return best;
}

template <class T>
number_t KdTree<T>::depth() const
{
   number_t d=0;
   return root->depth(d);
}

//print utilities
template <class T>
void KdTree<T>::print(std::ostream& os) const
{
   if(root==nullptr)             //no tree
   {
      os<<"empty tree";
      return;
   }
   std::string level="";
   root->print(os,level);  //call KdNode::print
}

//! print operator
template <class S>
std::ostream& operator<<(std::ostream& os,const KdTree<S>& kd)
{
   kd.print(os);
   return os;
}

//================================================================================
// member functions of KdNode
//================================================================================

template <class T>
KdNode<T>::~KdNode()
{
   if(left_!=nullptr) delete left_;
   if(right_!=nullptr) delete right_;
}

template <class T >
void KdNode<T>::insert(const T * O)
{
   if(obj_!=nullptr)   //separate obj_ and O
   {
      int r=maxSeparator(*obj_, *O, separator_, sv_);   // choose the direction (separator_) which maximizes the separation
      if(r==0) return;                          //non separable objects, not inserted
      if(r==1)                                  //obj>O, create child nodes
      {
         left_=new KdNode(O,0,this);
         right_=new KdNode(obj_,0,this);
      }
      else //obj<O, create child nodes
      {
         left_=new KdNode(obj_,0,this);
         right_=new KdNode(O,0,this);
      }
      obj_=nullptr;
      return;
   }
   else       // insert down
   {
      if(left_==nullptr) {obj_=O; return;}
      if(compare(*O, sv_, separator_)<=0) left_->insert(O);
      else right_->insert(O);
   }
}

//search the nearest object in a KdTree (recursive)
template <class T>
void KdNode<T>::searchNearest(const T* O, const T*& best, SvType & r)
{
   //countSearch++;
   if(obj_!=nullptr)
   {
      SvType d1=dist(*O,*obj_);
      if(best==nullptr || d1<r)
      {
         best=obj_;
         r=d1;
      }
   }
   else
   {
      if((*O)(separator_)<=sv_)      //point at left
      {
         left_->searchNearest(O, best, r);
         if((*O)(separator_)+r >= sv_) right_->searchNearest(O, best, r);
      }
      else                          //point at right (strict)
      {
         right_->searchNearest(O, best, r);
         if((*O)(separator_)-r <= sv_) left_->searchNearest(O, best, r);
      }
   }
}

template <class T>
number_t& KdNode<T>::depth(number_t & d) const
{
   if(obj_!=nullptr) return d;
   d++;
   number_t dl=0, dr=0;
   if(left_!= nullptr) left_->depth(dl);
   if(right_!= nullptr) right_->depth(dr);
   d+=std::max(dl,dr);
   return d;
}

//print utilities
template <class T>
void KdNode<T>::print(std::ostream& os, std::string & level) const
{
   if(obj_!=nullptr)
   {
      obj_->print(os);
      os<<"\n";
      return;
   }
   number_t d=level.size()/2;
   os<<"depth = "<<d<<" sep="<<separator_<<" sv="<<sv_<<"\n";
   string_t le1=level+"  ";
   if(left_!=nullptr) {os<<le1<<"left "; left_->print(os,le1);}
   if(right_!=nullptr) {os<<le1<<"right "; right_->print(os,le1);}

}

//! print operator
template <class S>
std::ostream& operator<<(std::ostream& os, const KdNode<S> & node)
{
   os<<"node="<<&node<<" sep="<<node.separator_<<" sv_="<<node.sv_<<" obj=";
   if(node.obj_!=nullptr) node.obj_->print();
   os<<" left="<<node.left_<<" right="<<node.right_;
   return os;
}


//================================================================================
// functions to print boxes of kdtree
//================================================================================

template <class T>
void KdNode<T>::printBoxes(std::ostream &os, Box<SvType> b) const
{
   if(obj_==nullptr)
   {
      Box<SvType> bl(b), br(b);
      number_t j=separator_-1;
      bl.vmax[j]=sv_; br.vmin[j]=sv_;
      bl.print(os);
      br.print(os);
      left_->printBoxes(os,bl);
      right_->printBoxes(os,br);
   }
}

template <class T>
template <typename R>
void KdTree<T>::printBoxes(std::ostream &os, const Box<R>& rb)
{
   rb.print(os);
   root->printBoxes(os,rb);
}

template <class T>
template <typename R>
void KdTree<T>::printBoxes(const std::string& file, const Box<R>& rb)
{
   std:: ofstream ofs(file.c_str());
   printBoxes(ofs,rb);
   ofs.close();
}

} // end of namespace xlifepp
#endif

