/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Trace.cpp
  \authors D. Martin, N. Kielbasiewicz
  \since 10 dec 2002
  \date 3 may 2012

  \brief Implementation of xlifepp::Trace class members and related functions
 */

#include "Environment.hpp"
#include "Trace.hpp"
#include "Messages.hpp"

#include <fstream>
#include <iostream>
#include <algorithm>

namespace xlifepp
{

//! push adds name into Trace function list member
void Trace::push(const string_t& s)
{
  if (currentThread() > 0) return;
  if (disablePushPop) return;
  if ( pos_ < maxPos_ - 1 )
  {
    fList[pos_++] = s;
    if (trackingMode && isLogged_) theLogStream << string_t(pos_,' ') << "enter " << s << eol << std::flush;
  }
  else
  {
    error("maxpos", maxPos_);
  }
}

//! pop "pops" current input out of a Trace function list member
void Trace::pop()
{
  if (currentThread() > 0) return;
  if (disablePushPop) return;
  if ( pos_ > 0 )
  {
    if (trackingMode && isLogged_) theLogStream << string_t(pos_,' ') << "exit  " << fList[pos_-1] << eol << std::flush;
    pos_--;
  }
  else
  {
    error("poszero", pos_);
  }
}

//! current returns string of last l to current inputs of a Trace object list
string_t Trace::current(const number_t l)
{
  if ( pos_ > l )
  {
    return fList[pos_ - l - 1];
  }
  else
  {
    return "_main_";
  }
}

string_t Trace::current()
{
  return current(0);
}

//! returns function list of a Trace object list
string_t Trace::list()
{
  string_t out = "";

  if ( pos_ > 0 )
  {
    for ( number_t i = 0; i < pos_ - 1; i++ )
    {
      out += (fList[i] + ">");
    }

    out += fList[pos_ - 1];
  }

  return out;
}

number_t Trace::length() const //! function length returns the number of items of f_list
{
  if ( pos_ > 0 )
  {
    return pos_ - 1;
  }
  else
  {
    return 0;
  }
}

//! function indent prints context-depending indentation to log file
void Trace::indent()
{
  if ( isLogged_ )
  {
    // digits
    for ( number_t ip = 0; ip < number_t(std::min(int(pos_), 10 )); ip++ )
    {
      theLogStream << ip << "-";
    }

    // capital letters A ... Z
    for ( number_t ip = 10; ip < number_t(std::min(int(pos_), 36)); ip++ )
    {
      theLogStream << char(ip + 55) << "-";
    }

    // lowercase letters a ... z
    // for ( number_t ip = 36; ip < tplMin(pos_, 62); ip++ ) theLogStream << char(ip+61) <<"-";
    theLogStream << std::endl;
  }
}

//! function not yet implemented
void Trace::noSuchFunction(const string_t& s)
{
  error("nofunc", current(), s);
}


//! print prints function list to opened ofstream
void Trace::print(std::ofstream& os)
{
  for ( number_t i = 0; i < pos_ - 1; i++ )
  {
    os << fList[i] << "->";
  }

  os << fList[pos_ - 1] << std::endl;
}

//! print prints function list to default print file
void Trace::print()
{
  for ( number_t i = 0; i < pos_ - 1; i++ )
  {
    std::cout << fList[i] << "->";
  }

  if ( pos_ > 0 )
  {
    std::cout << fList[pos_ - 1] << std::endl;
  }
}

//-------------------------------------------------------------------------------
// manage verbose level
//-------------------------------------------------------------------------------

void setGlobalVerboseLevel(const number_t v)
{
  if (currentThread() > 0) return;
  theGlobalVerboseLevel = v;
}

number_t verboseLevel(const number_t v)
{
  #pragma omp single
  theVerboseLevel = std::min(v, theGlobalVerboseLevel);
  
  return theVerboseLevel;
}

Trace& operator<<(Trace& met, const string_t& s)
{
  if ( met.pos_ < met.maxPos_ - 1 )
  {
    met.fList[met.pos_++] = s;
  }
  else
  {
    error("maxpos", met.maxPos_);
  }

  return met;
}

//-------------------------------------------------------------------------------
// display the Trace->current
//-------------------------------------------------------------------------------
string_t& where(const string_t& s)
{
  if (currentThread() == 0)
  {
    theWhereData = trace_p->list() + " ..., ";

    if (s != "")
    {
      theWhereData += words("in") + " " + s + "\n";
    }
  }
  return theWhereData;
}

//-------------------------------------------------------------------------------
// Trouble in some unforeseen situations
//-------------------------------------------------------------------------------
void incompleteFunction(const string_t& s)
{
  if ( s.empty() )
  {
    error("incomplete", trace_p->current());
  }
  else
  {
    error("incomplete", s);
  }
}

void invalidFunction(const string_t& s)
{
  if ( s.empty() )
  {
    error("invalid", trace_p->current());
  }
  else
  {
    error("invalid", s);
  }
}

//-------------------------------------------------------------------------------
// Trouble in object constructors
//-------------------------------------------------------------------------------

void constructorError()
{
  error("constructor", trace_p->current());
}

void setNewHandler()
{
  constructorError();
}

} // end of namespace xlifepp
