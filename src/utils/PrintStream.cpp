/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file PrintStream.cpp
  \authors E. Lunéville
  \since 8 feb 2017
  \date 8 feb 2017

  \brief Implementation of xlifepp::PrintStream class members and related functions
 */

#include "PrintStream.hpp"
#include <stdio.h>

namespace xlifepp
{

void PrintStream::open(const std::string& na)
{
  name=na;
  number_t nbt=numberOfThreads();
  ofstreams.resize(nbt);
  tps.resize(nbt);
  ofstreams[0]=new std::ofstream(name.c_str());
  tps[0]=0;
  std::pair<string_t, string_t> namext = fileRootExtension(name);
  for(number_t n=1; n<nbt; n++)
    {
      string_t name_n = namext.first+tostring(n);
      if(namext.second!="") name_n += "."+namext.second;
      ofstreams[n]=new std::ofstream(name_n.c_str());
      (*ofstreams[n])<<"==============================================================================================="<<eol
                     <<"                                    ouputs of thread "<<n<<eol
                     <<"==============================================================================================="<<eol;
      ofstreams[n]->flush();
      tps[n]=ofstreams[n]->tellp();
    }
  notOpen=false;
}

void PrintStream::close()
{
  std::pair<string_t, string_t> namext = fileRootExtension(name);
  for(number_t n=0; n<ofstreams.size(); n++)
    {
      ofstreams[n]->flush();
      number_t tp=ofstreams[n]->tellp();
      ofstreams[n]->close();
      string_t name_n = namext.first+tostring(n);
      if(namext.second!="") name_n += "."+namext.second;
      if(tp==tps[n]) remove(name_n.c_str());  //nothing written
      delete ofstreams[n];
    }
  ofstreams.clear();
  notOpen=true;
}

}
