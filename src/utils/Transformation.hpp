/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Transformation.hpp
  \author N. Kielbasiewicz
  \since 8 oct 2014
  \date 08 feb 2016

  \brief Definition of the xlifepp::Transformation class

  xlifepp::Transformation class represents geometric transformations applied to Mesh or Point objects
*/

#ifndef TRANSFORMATIONS_HPP
#define TRANSFORMATIONS_HPP

#include "config.h"
#include "Point.hpp"
#include "Messages.hpp"
#include "Environment.hpp"
#include "Matrix.hpp"
#include "Parameters.hpp"

namespace xlifepp
{

// forward declarations
class Translation;
class Rotation2d;
class Rotation3d;
class Homothety;
class PointReflection;
class Reflection2d;
class Reflection3d;

/*!
   \class Transformation
    handles mesh transformations
 */
class Transformation
{
  protected:
    string_t name_;               //!< name of the transformation
    TransformType transformType_; //!< geometrical transformation
    Matrix<real_t> mat_;          //!< matrix of the transformation (when linear), default Id in R3
    Vector<real_t> vec_;          //!< vector of the transformation (when linear), default (0,0,0)
    bool is3D_;                   //!< true if a 3D transformation (set by buildMat)

 private:
    std::vector<const Transformation*> components_;   //!< chain rule Transformation if not empty

  public:
    Transformation(const string_t& nam = "", TransformType trt=_noTransform); //!< basic constructor
    Transformation(real_t a11,real_t a12, real_t a13, real_t a21,real_t a22, real_t a23,real_t a31,real_t a32, real_t a33,
                   real_t b1=0, real_t b2=0, real_t b3=0);                    //!< explicit constructor from matrix coefficients
    Transformation(const Matrix<real_t>& M, const Vector<real_t>& b=Vector<real_t>(3,0.)); //!< explicit constructor from matrix
    Transformation(const Transformation& t);                                  //!< copy constructor

    virtual Transformation* clone() const { return new Transformation(*this); } //!< virtual copy constructor

    virtual ~Transformation(); //!< destructor

    void buildParam(const Parameter& p, Point& center, real_t& angle, Reals& scale, Reals& dir, Reals& axis, Reals& normal);

    Transformation & operator=(const Transformation& t); //!< assign operator
    TransformType transformType() const      //! return the geometrical transformation
      { return transformType_; }
    void transformType(TransformType tr) //! sets the geometrical shape
      { transformType_=tr; }

    const Matrix<real_t>& mat()const {return mat_;}
    Matrix<real_t>& mat(){return mat_;}

    const Vector<real_t>& vec()const {return vec_;}
    Vector<real_t>& vec(){return vec_;}

    string_t name() const { return name_; }       //!< get the Transformation name
    void name(const string_t& nm) { name_ = nm; } //!< set the Transformation name
    bool is3D() const {return is3D_;}
    real_t scaleFactor() const;                   //!< return the scale factor (=1. if no homotethy in)

    const std::vector<const Transformation*> components() const { return components_;} //!< accessor to components
    const Transformation* component(number_t i) const { return components_[i];} //!< accessor to i-th component

    void buildMat();             //! build mat_ and vec_ related to the transformation (general function)

    virtual void print(std::ostream&) const;           //!< print Transformation
    virtual void print(PrintStream& os) const { print(os.currentStream()); }

    virtual const Translation* translation() const //! access to child Translation object (const)
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _translation)); return nullptr; }
    virtual Translation* translation() //! access to child Translation object
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _translation)); return nullptr; }

    virtual const Rotation2d* rotation2d() const //! access to child Rotation2d object (const)
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _rotation2d)); return nullptr; }
    virtual Rotation2d* rotation2d() //! access to child Rotation2d object
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _rotation2d)); return nullptr; }

    virtual const Rotation3d* rotation3d() const //! access to child Rotation3d object (const)
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _rotation3d)); return nullptr; }
    virtual Rotation3d* rotation3d() //! access to child Rotation3d object
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _rotation3d)); return nullptr; }

    virtual const Homothety* homothety() const //! access to child Homothety object (const)
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _homothety)); return nullptr; }
    virtual Homothety* homothety() //! access to child Homothety object
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _homothety)); return nullptr; }

    virtual const PointReflection* pointReflection() const //! access to child PointReflection object (const)
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _ptReflection)); return nullptr; }
    virtual PointReflection* pointReflection() //! access to child PointReflection object
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _ptReflection)); return nullptr; }

    virtual const Reflection2d* reflection2d() const //! access to child Reflection object (const)
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _reflection2d)); return nullptr; }
    virtual Reflection2d* reflection2d() //! access to child Reflection object
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _reflection2d)); return nullptr; }

    virtual const Reflection3d* reflection3d() const //! access to child Reflection3d object (const)
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _reflection3d)); return nullptr; }
    virtual Reflection3d* reflection3d() //! access to child Reflection3d object
    { error("bad_transform", name_, words("transform", transformType_), words("transform", _reflection3d)); return nullptr; }

    //! computes the image of a point by the transformation
    virtual Point apply(const Point& p) const;
    //! computes the image of a point by the inverse transformation (use matrix representation)
    virtual Point inverse(const Point& p) const;

    //----------------------------------
    // composition of 2 transformations
    //----------------------------------
    //! composition of transformations (general case)
    Transformation& operator*=(const Transformation& t);
    friend Transformation composeCompositeAndComposite(const Transformation& t1, const Transformation& t2);
    friend Transformation composeCompositeAndCanonical(const Transformation& t1, const Transformation& t2);
    friend Transformation composeCanonicalAndComposite(const Transformation& t1, const Transformation& t2);
    friend Transformation composeCanonicalAndCanonical(const Transformation& t1, const Transformation& t2);
    friend Transformation operator*(const Transformation& t1, const Transformation& t2);

    friend Transformation toComposite(const Transformation& t);
    friend std::ostream& operator<<(std::ostream&, const Transformation&);
};

//----------------------------------
// composition of 2 transformations
//----------------------------------
//! composition of composite transformations
Transformation composeCompositeAndComposite(const Transformation& t1, const Transformation& t2);
//! composition of a composite transformation and a canonical transformation
Transformation composeCompositeAndCanonical(const Transformation& t1, const Transformation& t2);
//! composition of a canonical transformation and a composite transformation
Transformation composeCanonicalAndComposite(const Transformation& t1, const Transformation& t2);
//! composition of canonical transformations
Transformation composeCanonicalAndCanonical(const Transformation& t1, const Transformation& t2);
Transformation operator*(const Transformation& t1, const Transformation& t2); //!< composition of transformations (general case)
Transformation toComposite(const Transformation& t); //!< return a composite Transformation with only one component: t
std::ostream& operator<<(std::ostream&, const Transformation&); //!< output Transformation

/*!
  \class Translation
  representation of a translation of vector u
*/
class Translation : public Transformation
{
  protected:
    std::vector<real_t> u_; //!< vector of the translation

  public:
    //! default constructor
    Translation();
    void build(const std::vector<Parameter>& ps);
    //! main constructor from one key
    Translation(const Parameter& p) : Transformation("Translation", _translation)
    { build({p}); }
    //! main constructor from a vector of keys
    Translation(const std::vector<Parameter>& ps) : Transformation("Translation", _translation)
    { build(ps); }
    //! constructor with a vector of real numbers
    Translation(std::vector<real_t> u) : Translation(_direction=u)
    { warning("deprecated", "Translation(Reals)", "Translation(_direction=xxx)"); }
    //! constructor from 3 values
    Translation(real_t ux, real_t uy=0., real_t uz=0.) : Translation(_direction={ux,uy,uz})
    { warning("deprecated", "Translation(Real, Real, Real)", "Translation(_direction={ux,uy,uz})"); }
    virtual ~Translation() {}

    const std::vector<real_t>& u() const { return u_; } //!< accessor to vector u

    virtual Transformation* clone() const { return new Translation(*this); } //!< virtual copy constructor

    //! access to child Translation object (const)
    virtual const Translation* translation() const {return this;}
    //! access to child Translation object
    virtual Translation* translation() {return this;}

    virtual Point apply(const Point& p) const;

    virtual void print(std::ostream&) const;           //!< print Transformation
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

/*!
  \class Rotation2d
  representation of a rotation from a point and an angle
*/
class Rotation2d: public Transformation
{
  protected:
    Point c_;    //!< center of the rotation
    real_t angle_; //!< angle of the rotation (in radians)

  public:
    //! default constructor
    Rotation2d();
    void build(const std::vector<Parameter>& ps);
    //! main constructor from one key
    Rotation2d(const Parameter& p1) : Transformation("Rotation 2d", _rotation2d)
    { build({p1}); }
    //! main constructor from 2 keys
    Rotation2d(const Parameter& p1, const Parameter& p2) : Transformation("Rotation 2d", _rotation2d)
    { build({p1, p2}); }
    //! main constructor from a vector of keys
    Rotation2d(const std::vector<Parameter>& ps) : Transformation("Rotation 2d", _rotation2d)
    { build(ps); }
    //! constructor with a vector of real numbers
    Rotation2d(const Point& c, real_t angle) : Rotation2d(_center=c, _angle=angle)
    { warning("deprecated", "Rotation2d(Point, Real)", "Rotation2d(_center=xxx, _angle=yyy)"); }
    //! destructor
    virtual ~Rotation2d() {}

    const Point& center() const { return c_; } //!< accessor to rotation center
    real_t angle() const { return angle_; } //!< accessor to rotation angle

    virtual Transformation* clone() const { return new Rotation2d(*this); } //!< virtual copy constructor

    //! access to child Rotation2d object (const)
    virtual const Rotation2d* rotation2d() const {return this;}
    //! access to child Rotation2d object
    virtual Rotation2d* rotation2d() {return this;}

    virtual Point apply(const Point& p) const;

    virtual void print(std::ostream&) const;           //!< print Transformation
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

/*!
  \class Rotation3d
  representation of a rotation from an axis ( a point and a vector ) and an angle
*/
class Rotation3d: public Transformation
{
  protected:
    Point c_;             //!< one point of the rotation axis
    std::vector<real_t> d_; //!< direction vector of the rotation axis
    real_t angle_;          //!< angle of the rotation (in radians)

  public:
    //! default constructor
    Rotation3d();
    void build(const std::vector<Parameter>& ps);
    //! main constructor from one key
    Rotation3d(const Parameter& p1) : Transformation("Rotation 3d", _rotation3d)
    { build({p1}); }
    //! main constructor from 2 keys
    Rotation3d(const Parameter& p1, const Parameter& p2) : Transformation("Rotation 3d", _rotation3d)
    { build({p1, p2}); }
    //! main constructor from 3 keys
    Rotation3d(const Parameter& p1, const Parameter& p2, const Parameter& p3) : Transformation("Rotation 3d", _rotation3d)
    { build({p1, p2, p3}); }
    //! main constructor from a vector of keys
    Rotation3d(const std::vector<Parameter>& ps) : Transformation("Rotation 3d", _rotation3d)
    { build(ps); }
    //! constructor with a vector of real numbers
    Rotation3d(const Point& c, std::vector<real_t> d, real_t angle) : Rotation3d(_center=c, _axis=d, _angle=angle)
    { warning("deprecated", "Rotation3d(Point, Reals, Real)", "Rotation3d(_center=xxx, _axis=yyy, _angle=zzz)"); }
    //! constructor with direction given with 2 reals
    Rotation3d(const Point& c, real_t dx, real_t dy, real_t angle) : Rotation3d(_center=c, _axis={dx, dy}, _angle=angle)
    { warning("deprecated", "Rotation3d(Point, Real, Real, Real)", "Rotation3d(_center=xxx, _axis={dx, dy}, _angle=zzz)"); }
    //! constructor with direction given with 3 reals
    Rotation3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle) : Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle)
    { warning("deprecated", "Rotation3d(Point, Real, Real, Real, Real)", "Rotation3d(_center=xxx, _axis={dx, dy, dz}, _angle=zzz)"); }
    //! destructor
    virtual ~Rotation3d() {}

    const Point& axisPoint() const { return c_; } //!< accessor to rotation axis point
    const std::vector<real_t>& axisDirection() const { return d_; } //!< accessor to rotation axis direction
    real_t angle() const { return angle_; } //!< accessor to rotation angle

    virtual Transformation* clone() const { return new Rotation3d(*this); } //!< virtual copy constructor

    //! access to child Rotation3d object (const)
    virtual const Rotation3d* rotation3d() const {return this;}
    //! access to child Rotation3d object
    virtual Rotation3d* rotation3d() {return this;}

    virtual Point apply(const Point& p) const;

    virtual void print(std::ostream&) const;           //!< print Transformation
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

/*!
  \class Scaling
  representation of a scaling from a center and a scaling factor vector
*/
class Scaling : public Transformation
{
  protected:
    Point c_;                    //!< center of the scaling
    std::vector<real_t> factor_; //!< scale factors in each direction
    bool isIsotropic_;

    //! hidden constructor from name and type (for inheritance purpose)
    Scaling(const string_t& name, TransformType tt) : Transformation(name, tt), c_(0.,0.,0.), factor_(1,1.), isIsotropic_(false)
    { buildMat(); }

  public:
    //! default constructor
    Scaling() : Transformation("Scaling", _scaling), c_(0.,0.,0.), factor_(1,1.), isIsotropic_(false)
    { buildMat(); }
    void build(const std::vector<Parameter>& ps);
    //! main constructor from one key
    Scaling(const Parameter& p1) : Transformation("Scaling", _scaling)
    { build({p1}); }
    //! main constructor from 2 keys
    Scaling(const Parameter& p1, const Parameter& p2) : Transformation("Scaling", _scaling)
    { build({p1, p2}); }
    //! main constructor from a vector of keys
    Scaling(const std::vector<Parameter>& ps) : Transformation("Scaling", _scaling)
    { build(ps); }
    //! default constructor from a center and a factor
    Scaling(const Point& c, const std::vector<real_t>& factor = std::vector<real_t>(1,1.)) : Scaling(_center=c, _scale=factor)
    { warning("deprecated", "Scaling(Point, Reals)", "Scaling(_center=xxx, _scale=yyy)"); }
    //! destructor
    virtual ~Scaling() {}

    const Point& center() const { return c_; } //!< returns the center of the homothety
    const std::vector<real_t>& factors() const { return factor_; } //!< returns the scale factors of the scaling

    virtual Transformation* clone() const { return new Scaling(*this); } //!< virtual copy constructor

    //! access to child Scaling object (const)
    virtual const Scaling* scaling() const {return this;}
    //! access to child Scaling object
    virtual Scaling* scaling() {return this;}

    virtual Point apply(const Point& p) const;

    virtual void print(std::ostream&) const;           //!< print Transformation
    virtual void print(PrintStream& os) const { print(os.currentStream()); }
};

/*!
  \class Homothety
  representation of an homothety from a center and a factor
*/
class Homothety : public Scaling
{
  protected:
    //! hidden constructor from name and type (for inheritance purpose)
    Homothety(const string_t& name, TransformType tt) : Scaling(name, tt) {}

  public:
    //! default constructor
    Homothety() : Scaling("Homothety", _homothety) {}
    //! main constructor from one key
    Homothety(const Parameter& p1) : Scaling("Homothety", _homothety)
    { build({p1}); }
    //! main constructor from 2 keys
    Homothety(const Parameter& p1, const Parameter& p2) : Scaling("Homothety", _homothety)
    { build({p1, p2}); }
    //! main constructor from a vector of keys
    Homothety(const std::vector<Parameter>& ps) : Scaling("Homothety", _homothety)
    { build(ps); }
    //! constructor from a center and a factor
    Homothety(const Point& c, real_t factor = 1.) : Homothety(_center=c, _scale=factor)
    { warning("deprecated", "Homothety(Point, Real)", "Homothety(_center=xxx, _scale=yyy)"); }
    //! destructor
    virtual ~Homothety() {}

    real_t factor() const { return factor_[0]; } //!< returns the scale factor of the homothety

    virtual Transformation* clone() const { return new Homothety(*this); } //!< virtual copy constructor

    //! access to child Homothety object (const)
    virtual const Homothety* homothety() const {return this;}
    //! access to child Homothety object
    virtual Homothety* homothety() {return this;}

    virtual void print(std::ostream&) const;           //!< print Transformation
};

/*!
  \class PointReflection
  representation of a point symmetry
  it is an homothety of factor -1.
*/
class PointReflection : public Homothety
{
  public:
    //! default constructor
    PointReflection() : Homothety("Point Reflection", _ptReflection)
    { factor_=std::vector<real_t>(1,-1.); }
    //! main constructor from one key
    PointReflection(const Parameter& p) : Homothety("Point Reflection", _ptReflection)
    { build({p}); }
    //! main constructor from a vector of keys
    PointReflection(const std::vector<Parameter>& ps) : Homothety("Point Reflection", _ptReflection)
    { build(ps); }
    //! constructor from a center
    PointReflection(const Point& c) : PointReflection(_center=c)
    { warning("deprecated", "PointReflection(Point)", "PointReflection(_center=xxx)"); }
    //! destructor
    virtual ~PointReflection() {}

    virtual Transformation* clone() const { return new PointReflection(*this); } //!< virtual copy constructor

    //! access to child PointReflection object (const)
    virtual const PointReflection* pointReflection() const {return this;}
    //! access to child PointReflection object
    virtual PointReflection* pointReflection() {return this;}

    virtual void print(std::ostream&) const;           //!< print Transformation
};

/*!
  \class Reflection2d
  representation of a reflection
*/
class Reflection2d: public Transformation
{
  protected:
    Point c_;             //!< one point of the reflection axis
    std::vector<real_t> d_; //!< direction vector of the reflection axis

  public:
    //! default constructor
    Reflection2d();
    void build(const std::vector<Parameter>& ps);
    //! main constructor from one key
    Reflection2d(const Parameter& p1) : Transformation("Reflection 2D", _reflection2d)
    { build({p1}); }
    //! main constructor from 2 keys
    Reflection2d(const Parameter& p1, const Parameter& p2) : Transformation("Reflection 2D", _reflection2d)
    { build({p1, p2}); }
    //! main constructor from a vector of keys
    Reflection2d(const std::vector<Parameter>& ps) : Transformation("Reflection 2D", _reflection2d)
    { build(ps); }
    //! constructor from a center and a direction vector
    Reflection2d(const Point& c, std::vector<real_t> d) : Reflection2d(_center=c, _direction=d)
    { warning("deprecated", "Reflection2d(Point, Reals)", "Reflection2d(_center=xxx, _direction=yyy)"); }
    //! constructor from a center and coordinates of the direction vector
    Reflection2d(const Point& c, real_t dx, real_t dy): Reflection2d(_center=c, _direction={dx,dy})
    { warning("deprecated", "Reflection2d(Point, Real, Real)", "Reflection2d(_center=xxx, _direction={yyy, zzz})"); }
    //! destructor
    virtual ~Reflection2d() {}

    const Point& axisPoint() const { return c_; } //!< returns the point on the reflection axis
    const std::vector<real_t>& axisDirection() const { return d_; } //!< returns the direction vector of the reflection axis


    virtual Transformation* clone() const { return new Reflection2d(*this); } //!< virtual copy constructor

    //! access to child Reflection2d object (const)
    virtual const Reflection2d* reflection2d() const {return this;}
    //! access to child Reflection2d object
    virtual Reflection2d* reflection2d() {return this;}

    virtual Point apply(const Point& p) const;

    virtual void print(std::ostream&) const;           //!< print Transformation
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

/*!
  \class Reflection3d
  representation of a reflection on a plane
*/
class Reflection3d: public Transformation
{
  protected:
    Point c_;             //!< one point od the reflection plane
    std::vector<real_t> n_; //!< normal vector of the reflection plane
  
  public:
    //! default constructor
    Reflection3d();
    void build(const std::vector<Parameter>& ps);
    //! main constructor from one key
    Reflection3d(const Parameter& p1) : Transformation("Reflection 3D", _reflection3d)
    { build({p1}); }
    //! main constructor from 2 keys
    Reflection3d(const Parameter& p1, const Parameter& p2) : Transformation("Reflection 3D", _reflection3d)
    { build({p1, p2}); }
    //! main constructor from a vector of keys
    Reflection3d(const std::vector<Parameter>& ps) : Transformation("Reflection 3D", _reflection3d)
    { build(ps); }
    //! constructor from a center and a normal vector
    Reflection3d(const Point& c, std::vector<real_t> n) : Reflection3d(_center=c, _normal=n)
    { warning("deprecated", "Reflection3d(Point, Reals)", "Reflection2d(_center=xxx, _normal=yyy)"); }
    //! constructor from a center and coordinates of the direction vector
    Reflection3d(const Point& c, real_t nx, real_t ny, real_t nz): Reflection3d(_center=c, _normal={nx,ny, nz})
    { warning("deprecated", "Reflection3d(Point, Real, Real, Real)", "Reflection2d(_center=xxx, _normal={nx, ny, nz})"); }
    //! constructor from a center and coordinates of the normal vector of the plane
    virtual ~Reflection3d() {}

    const Point& planePoint() const { return c_; } //!< returns the point on the reflection plane
    const std::vector<real_t>& planeNormal() const { return n_; } //!< returns the normal vector of the reflection plane

    std::vector<real_t> u() const; //!< returns a vector "in" the plane
    std::vector<real_t> v() const; //!< returns another vector "in" the plane
    virtual Transformation* clone() const { return new Reflection3d(*this); } //!< virtual copy constructor

    //! access to child Reflection3d object (const)
    virtual const Reflection3d* reflection3d() const {return this;}
    //! access to child Reflection3d object
    virtual Reflection3d* reflection3d() {return this;}

    virtual Point apply(const Point& p) const;

    virtual void print(std::ostream&) const;           //!< print Transformation
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

//=============================================================
//  external functions to compute transformations on a Point
//=============================================================

//! apply a geometrical transformation on a Point
inline Point transform(const Point& p, const Transformation& t) { return t.apply(p); }
//! apply a translation on a Point (1 key) (template external)
inline Point translate(const Point& g, const Parameter& p1)
{ return transform(g, Translation(p1)); }
//! apply a translation on a Point (vector version) (template external)
inline Point translate(const Point& g, std::vector<real_t> u = std::vector<real_t>(3,0.))
{
  warning("deprecated", "translate(Point, Reals)", "translate(Point, _direction=xxx)");
  return transform(g, Translation(_direction=u));
}
//! apply a translation on a Point (3 reals version) (template external)
inline Point translate(const Point& g, real_t ux, real_t uy, real_t uz = 0.)
{
  warning("deprecated", "translate(Point, Real, Real, Real)", "translate(Point, _direction={ux, uy, uz})");
  return transform(g, Translation(_direction={ux, uy, uz}));
}
//! apply a rotation 2d on a Point (1 key) (template external)
inline Point rotate2d(const Point& g, const Parameter& p1)
{ return transform(g, Rotation2d(p1)); }
//! apply a rotation 2d on a Point (2 keys) (template external)
inline Point rotate2d(const Point& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Rotation2d(p1, p2)); }
//! apply a rotation 2d on a Point (template external)
inline Point rotate2d(const Point& g, const Point& c = Point(0.,0.), real_t angle = 0.)
{
  warning("deprecated", "rotate2d(Point, Point, Real)", "rotate2d(Point, _center=xxx, _angle=yyy)");
  return transform(g, Rotation2d(_center=c, _angle=angle));
}
//! apply a rotation 3d on a Point (1 key) (template external)
inline Point rotate3d(const Point& g, const Parameter& p1)
{ return transform(g, Rotation3d(p1)); }
//! apply a rotation 3d on a Point (2 keys) (template external)
inline Point rotate3d(const Point& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Rotation3d(p1, p2)); }
//! apply a rotation 3d on a Point (3 keys) (template external)
inline Point rotate3d(const Point& g, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{ return transform(g, Rotation3d(p1, p2, p3)); }
//! apply a rotation 3d on a Point (template external)
inline Point rotate3d(const Point& g, const Point& c = Point(0.,0.,0.),
                                    std::vector<real_t> d = std::vector<real_t>(3,0.), real_t angle = 0.)
{
  warning("deprecated", "rotate3d(Point, Point, Reals, Real)", "rotate3d(Point, _center=xxx, _axis=yyy, _angle=zzz)");
  return transform(g, Rotation3d(_center=c, _axis=d, _angle=angle));
}
//! apply a rotation 3d on a Point (template external)
inline Point rotate3d(const Point& g, real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "rotate3d(Point, Real, Real, Real)", "rotate3d(Point, _axis={dx, dy}, _angle=zzz)");
  return transform(g, Rotation3d(_axis={dx, dy}, _angle=angle));
}
//! apply a rotation 3d on a Point (template external)
inline Point rotate3d(const Point& g, real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "rotate3d(Point, Real, Real, Real, Real)", "rotate3d(Point, _axis={dx, dy, dz}, _angle=zzz)");
  return transform(g, Rotation3d(_axis={dx, dy, dz}, _angle=angle));
}
//! apply a rotation 3d on a Point (template external)
inline Point rotate3d(const Point& g, const Point& c, real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "rotate3d(Point, Point, Real, Real, Real)", "rotate3d(Point, _center=xxx, _axis={dx, dy}, _angle=zzz)");
  return transform(g, Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
}
//! apply a rotation 3d on a Point (template external)
inline Point rotate3d(const Point& g, const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "rotate3d(Point, Point, Real, Real, Real, Real)", "rotate3d(Point, _center=xxx, _axis={dx, dy, dz}, _angle=zzz)");
  return transform(g, Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
}
//! apply a homothety on a Point (1 key) (template external)
inline Point homothetize(const Point& g, const Parameter& p1)
{ return transform(g, Homothety(p1)); }
//! apply a homothety on a Point (2 keys) (template external)
inline Point homothetize(const Point& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Homothety(p1, p2)); }
//! apply a homothety on a Point (template external)
inline Point homothetize(const Point& g, const Point& c = Point(0.,0.,0.), real_t factor = 0.)
{
  warning("deprecated", "homothetize(Point, Point, Real)", "homothetize(Point, _center=xxx, _scale=yyy)");
  return transform(g, Homothety(_center=c, _scale=factor));
}
//! apply a homothety on a Point (template external)
inline Point homothetize(const Point& g, real_t factor)
{
  warning("deprecated", "homothetize(Point, Real)", "homothetize(Point, _scale=xxx)");
  return transform(g, Homothety(_scale=factor));
}
//! apply a point reflection on a Point (1 key)  (template external)
inline Point pointReflect(const Point& g, const Parameter& p1)
{ return transform(g, PointReflection(p1)); }
//! apply a point reflection on a Point (template external)
inline Point pointReflect(const Point& g, const Point& c = Point(0.,0.,0.))
{
  warning("deprecated", "pointReflect(Point, Real)", "pointReflect(Point, _center=xxx)");
  return transform(g, PointReflection(_center=c));
}
//! apply a reflection 2d on a Point (1 key) (template external)
inline Point reflect2d(const Point& g, const Parameter& p1)
{ return transform(g, Reflection2d(p1)); }
//! apply a reflection 2d on a Point (2 keys) (template external)
inline Point reflect2d(const Point& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Reflection2d(p1, p2)); }
//! apply a reflection 2d on a Point (template external)
inline Point reflect2d(const Point& g, const Point& c = Point(0.,0.),
                                     std::vector<real_t> d = std::vector<real_t>(2,0.))
{
  warning("deprecated", "reflect2d(Point, Point, Reals)", "reflect2d(Point, _center=xxx, _direction=yyy)");
  return transform(g, Reflection2d(_center=c, _direction=d));
}
//! apply a reflection 2d on a Point (template external)
inline Point reflect2d(const Point& g, const Point& c, real_t dx, real_t dy = 0.)
{
  warning("deprecated", "reflect2d(Point, Point, Real, Real)", "reflect2d(Point, _center=xxx, _direction={dx, dy})");
  return transform(g, Reflection2d(_center=c, _direction={dx, dy}));
}
//! apply a reflection 3d on a Point (1 key) (template external)
inline Point reflect3d(const Point& g, const Parameter& p1)
{ return transform(g, Reflection3d(p1)); }
//! apply a reflection 3d on a Point (2 keys) (template external)
inline Point reflect3d(const Point& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Reflection3d(p1, p2)); }
//! apply a reflection 3d on a Point (template external)
inline Point reflect3d(const Point& g, const Point& c = Point(0.,0.,0.),
                                     std::vector<real_t> n = std::vector<real_t>(3,0.))
{
  warning("deprecated", "reflect3d(Point, Point, Reals)", "reflect3d(Point, _center=xxx, _normal=yyy)");
  return transform(g, Reflection3d(_center=c, _normal=n));
}
//! apply a reflection 3d on a Point (template external)
inline Point reflect3d(const Point& g, const Point& c, real_t nx, real_t ny, real_t nz = 0.)
{
  warning("deprecated", "reflect3d(Point, Point, Real, Real, Real)", "reflect2d(Point, _center=xxx, _normal={nx, ny, nz})");
  return transform(g, Reflection3d(_center=c, _normal={nx, ny, nz}));
}

} // end of namespace xlifepp

#endif // TRANSFORMATIONS_HPP

