/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file VectorEntry.cpp
  \author E. Lunéville
  \since 01 nov 2013
  \date 01 nov 2013

  \brief Implementation of xlifepp::VectorEntry class member functions and related utilities
*/

#include "VectorEntry.hpp"
#include "utils.h"

namespace xlifepp
{

//===============================================================================
//member functions of VectorEntry class
//===============================================================================

VectorEntry::VectorEntry() //! default constructor: real vector of size 0
{
  valueType_ = _real;
  strucType_ = _scalar;
  nbOfComponents = 1;
  rEntries_p = new Vector<real_t>();
  cEntries_p = nullptr;
  rvEntries_p = nullptr;
  cvEntries_p = nullptr;
}

//constructor from explicit scalar type and vector size n
VectorEntry::VectorEntry(ValueType vt, StrucType st, number_t n, number_t nv)
{
  valueType_ = vt;
  strucType_ = st;
  nbOfComponents = nv;
  rEntries_p = nullptr;
  cEntries_p = nullptr;
  rvEntries_p = nullptr;
  cvEntries_p = nullptr;
  if(vt == _real && st == _scalar)
    {
      rEntries_p = new Vector<real_t>(n, 0.);
      return;
    }
  if(vt == _complex && st == _scalar)
    {
      cEntries_p = new Vector<complex_t>(n, complex_t(0.));
      return;
    }
  if(vt == _real && st == _vector)
    {
      rvEntries_p = new Vector<Vector<real_t> >(n, Vector<real_t>(nv, 0.));
      return;
    }
  if(vt == _complex && st == _vector)
    {
      cvEntries_p = new Vector<Vector<complex_t> >(n, Vector<complex_t>(nv, complex_t(0.)));
      return;
    }
  where("VectorEntry::VectorEntry(ValueType,StrucType,Number,Number)");
  error("vectorentry_unknowntype",words("value",vt),words("structure",st));
}

VectorEntry::VectorEntry(ValueType vt, number_t nv, number_t n) //! constructor from explicit vector type and vector size n
{
  valueType_ = vt;
  StrucType st = _scalar;
  if(nv > 1) st = _vector;
  strucType_ = st;
  nbOfComponents = nv;
  rEntries_p = nullptr;
  cEntries_p = nullptr;
  rvEntries_p = nullptr;
  cvEntries_p = nullptr;
  if(vt == _real && st == _scalar)
    {
      rEntries_p = new Vector<real_t>(n, 0.);
      return;
    }
  if(vt == _complex && st == _scalar)
    {
      cEntries_p = new Vector<complex_t>(n, complex_t(0.));
      return;
    }
  if(vt == _real && st == _vector)
    {
      rvEntries_p = new Vector<Vector<real_t> >(n, Vector<real_t>(nv, 0));
      return;
    }
  if(vt == _complex && st == _vector)
    {
      cvEntries_p = new Vector<Vector<complex_t> >(n, Vector<complex_t>(nv, complex_t(0.)));
      return;
    }
  where("VectorEntry::VectorEntry(ValueType,Number,Number)");
  error("vectorentry_unknowntype",words("value",vt),words("structure",st));
}

VectorEntry::VectorEntry(real_t v, number_t n)                //! constructor from constant real_t value and vector size
{
  valueType_ = _real;
  strucType_ = _scalar;
  nbOfComponents = 1;
  rEntries_p = new Vector<real_t>(n, v);
  cEntries_p = nullptr;
  rvEntries_p = nullptr;
  cvEntries_p = nullptr;
}

VectorEntry::VectorEntry(const complex_t& v, number_t n)      //! constructor from constant complex_t value and vector size
{
  valueType_ = _complex;
  strucType_ = _scalar;
  nbOfComponents = 1;
  cEntries_p = new Vector<complex_t>(n, v);
  rEntries_p = nullptr;
  rvEntries_p = nullptr;
  cvEntries_p = nullptr;
}

VectorEntry::VectorEntry(const Vector<real_t>& v, number_t n) //! constructor from constant Vector<real_t> value and vector size
{
  valueType_ = _real;
  strucType_ = _vector;
  nbOfComponents = v.size();
  rvEntries_p = new Vector<Vector<real_t> >(n, v);
  rEntries_p = nullptr;
  cEntries_p = nullptr;
  cvEntries_p = nullptr;
}

VectorEntry::VectorEntry(const Vector<complex_t>& v, number_t n)  //! constructor from constant Vector<complex_t> value and vector size
{
  valueType_ = _complex;
  strucType_ = _vector;
  nbOfComponents = v.size();
  cvEntries_p = new Vector<Vector<complex_t> >(n, v);
  rEntries_p = nullptr;
  cEntries_p = nullptr;
  rvEntries_p = nullptr;
}

VectorEntry::VectorEntry(const Vector<real_t>& v)         //! constructor from a vector of reals
{
  valueType_ = _real;
  strucType_ = _scalar;
  nbOfComponents =1;
  rEntries_p  = new Vector<real_t>(v);  //hard copy
  cEntries_p = nullptr;
  rvEntries_p  = nullptr;
  cvEntries_p = nullptr;
}

VectorEntry::VectorEntry(const Vector<complex_t>& v)         //! constructor from a vector of complexes
{
  valueType_ = _complex;
  strucType_ = _scalar;
  nbOfComponents =1;
  rEntries_p = nullptr;
  cEntries_p  = new Vector<complex_t>(v); //hard copy
  rvEntries_p = nullptr;
  cvEntries_p  = nullptr;
}

VectorEntry::VectorEntry(const Vector<Vector<real_t> >& v)       //! constructor from a vector of real vectors
{
  valueType_ = _real;
  strucType_ = _vector;
  if(v.size()!=0) nbOfComponents =v[0].size();
  else nbOfComponents=1;
  rEntries_p  = nullptr;
  cEntries_p = nullptr;
  rvEntries_p  = new Vector<Vector<real_t> >(v);  //hard copy
  cvEntries_p = nullptr;
}

VectorEntry::VectorEntry(const Vector<Vector<complex_t> >& v)       //! constructor from a vector of complex vectors
{
  valueType_ = _complex;
  strucType_ = _vector;
  if(v.size()!=0) nbOfComponents =v[0].size();
  else nbOfComponents=1;
  rEntries_p  = nullptr;
  cEntries_p = nullptr;
  rvEntries_p = nullptr;
  cvEntries_p  = new Vector<Vector<complex_t> >(v); //hard copy
}

//full copy of entries; normally only one kind of entries vector should be copied
VectorEntry::VectorEntry(const VectorEntry& vec) //! copy constructor
{
  valueType_ = vec.valueType_;
  strucType_ = vec.strucType_;
  nbOfComponents = vec.nbOfComponents;
  rEntries_p = nullptr;
  cEntries_p = nullptr;
  rvEntries_p = nullptr;
  cvEntries_p = nullptr;
  if(vec.rEntries_p != nullptr) rEntries_p = new Vector<real_t>(*vec.rEntries_p);
  if(vec.cEntries_p != nullptr) cEntries_p = new Vector<complex_t>(*vec.cEntries_p);
  if(vec.rvEntries_p != nullptr) rvEntries_p = new Vector<Vector<real_t> >(*vec.rvEntries_p);
  if(vec.cvEntries_p != nullptr) cvEntries_p = new Vector<Vector<complex_t> >(*vec.cvEntries_p);
}

VectorEntry::~VectorEntry()  //! destructor
{
  clear();
}

//assign operator: full copy of entries; normally only one kind of entries vector should be copied
VectorEntry& VectorEntry::operator=(const VectorEntry& vec)
{
  valueType_ = vec.valueType_;
  strucType_ = vec.strucType_;
  nbOfComponents = vec.nbOfComponents;
  //delete all entries
  clear();
  //copy entries
  if(vec.rEntries_p != nullptr)  rEntries_p = new Vector<real_t>(*vec.rEntries_p);
  if(vec.cEntries_p != nullptr)  cEntries_p = new Vector<complex_t>(*vec.cEntries_p);
  if(vec.rvEntries_p != nullptr) rvEntries_p = new Vector<Vector<real_t> >(*vec.rvEntries_p);
  if(vec.cvEntries_p != nullptr) cvEntries_p = new Vector<Vector<complex_t> >(*vec.cvEntries_p);
  return *this;
}

// deallocate memory used by a VectorEntry
void VectorEntry::clear()
{
  if(rEntries_p != nullptr)  delete rEntries_p;
  if(cEntries_p != nullptr)  delete cEntries_p;
  if(rvEntries_p != nullptr) delete rvEntries_p;
  if(cvEntries_p != nullptr) delete cvEntries_p;
  rEntries_p = nullptr;
  cEntries_p = nullptr;
  rvEntries_p = nullptr;
  cvEntries_p = nullptr;
}

//return the number of values counted in value type; normally one and only one vector entries is assigned
number_t VectorEntry::size() const
{
  if(rEntries_p != nullptr)  return rEntries_p->size();
  if(cEntries_p != nullptr)  return cEntries_p->size();
  if(rvEntries_p != nullptr) return rvEntries_p->size();
  if(cvEntries_p != nullptr) return cvEntries_p->size();
  return 0;
}

//return the number of values counted in value type; normally one and only one vector entries is assigned
void VectorEntry::resize(number_t k)
{
  if(rEntries_p != nullptr)  {rEntries_p->resize(k); return;}
  if(cEntries_p != nullptr)  {cEntries_p->resize(k); return;}
  if(rvEntries_p != nullptr) {rvEntries_p->resize(k); return;}
  if(cvEntries_p != nullptr) {cvEntries_p->resize(k); return;}
}

//! true if no one of pointers allocated
bool VectorEntry::isEmpty() const
{return (rEntries_p==nullptr && cEntries_p==nullptr && rvEntries_p==nullptr && cvEntries_p==nullptr);}

// set/get entry i
void VectorEntry::setEntry(number_t i, const real_t& r)
{if(rEntries_p != nullptr)(*rEntries_p)[i-1]=r;}

void VectorEntry::setEntry(number_t i, const complex_t& c)
{if(cEntries_p != nullptr)(*cEntries_p)[i-1]=c;}

void VectorEntry::setEntry(number_t i, const Vector<real_t>& rv)
{if(rvEntries_p != nullptr)(*rvEntries_p)[i-1]=rv;}

void VectorEntry::setEntry(number_t i, const Vector<complex_t>& cv)
{if(cvEntries_p != nullptr)(*cvEntries_p)[i-1]=cv;}

void VectorEntry::getEntry(number_t i, real_t& r) const
{r=0.;  if(rEntries_p != nullptr)  r=(*rEntries_p)[i-1];}

void VectorEntry::getEntry(number_t i, complex_t& c) const
{c=complex_t(0.);  if(cEntries_p != nullptr)  c=(*cEntries_p)[i-1];}

void VectorEntry::getEntry(number_t i, Vector<real_t>& rv)  const
{rv=Vector<real_t>();  if(rvEntries_p != nullptr) rv=(*rvEntries_p)[i-1];}

void VectorEntry::getEntry(number_t i, Vector<complex_t>& cv) const
{cv=Vector<complex_t>();  if(cvEntries_p != nullptr)  cv=(*cvEntries_p)[i-1];}

//! get (i) and set (i) as value (indices start from 1)
Value VectorEntry::getValue(number_t i) const
{
  if(rEntries_p != nullptr)  {return Value((*rEntries_p)[i-1]);}
  if(cEntries_p != nullptr)  {return Value((*cEntries_p)[i-1]);}
  if(rvEntries_p != nullptr) {return Value((*rvEntries_p)[i-1]);}
  if(cvEntries_p != nullptr) {return Value((*cvEntries_p)[i-1]);}
  return Value(0.);
}

void VectorEntry::setValue(number_t i, const Value& val)
{
  switch(val.valueType())
    {
      case _real:
        switch(val.strucType())
          {
            case _scalar:
              {
                real_t r=val.value<real_t>();
                if(rEntries_p != nullptr) {(*rEntries_p)[i-1]=r; return;}
                if(cEntries_p != nullptr) {(*cEntries_p)[i-1]=r; return;}
                if(rvEntries_p != nullptr)
                  {
                    number_t n=(*rvEntries_p)[i-1].size();
                    (*rvEntries_p)[i-1]=Vector<real_t>(n,r);
                    return;
                  }
                if(cvEntries_p != nullptr)
                  {
                    number_t n=(*cvEntries_p)[i-1].size();
                    (*cvEntries_p)[i-1]=Vector<complex_t>(n,complex_t(r));
                    return;
                  }
              }
            case _vector:
              {
                Vector<real_t> vr=val.value<Vector<real_t> >();
                if(rvEntries_p != nullptr) {(*rvEntries_p)[i-1]=vr; return;}
                if(cvEntries_p != nullptr) {(*cvEntries_p)[i-1]=vr; return;}
                where("VectorEntry::setValue");
                error("entry_inconsistent_structures");
              }
            default:
              break;
          }
      case _complex:
        switch(val.strucType())
          {
            case _scalar:
              {
                complex_t c=val.value<complex_t>();
                if(cEntries_p != nullptr) {(*cEntries_p)[i-1]=c; return;}
                if(cvEntries_p != nullptr)
                  {
                    number_t n=(*cvEntries_p)[i-1].size();
                    (*cvEntries_p)[i-1]=Vector<complex_t>(n,c);
                    return;
                  }
                where("VectorEntry::setValue");
                error("entry_inconsistent_structures");
              }
            case _vector:
              {
                Vector<complex_t> vc=val.value<Vector<complex_t> >();
                if(cvEntries_p != nullptr) {(*cvEntries_p)[i-1]=vc; return;}
                where("VectorEntry::setValue");
                error("entry_inconsistent_structures");
              }
            default:
              break;
          }
      default:
        where("VectorEntry::setValue");
        error("value_badtype",words("value",val.strucType()));
    }
}

// return as ...
Vector<real_t>& VectorEntry::asVector(Vector<real_t>& v) const
{
  if(rEntries_p!=nullptr) return v=*rEntries_p;
  if(rvEntries_p!=nullptr)
    {
      v.resize(rvEntries_p->begin()->size()*rvEntries_p->size());
      Vector<Vector<real_t> >::iterator ite;
      Vector<real_t>::iterator it, itv=v.begin();
      for(ite=rvEntries_p->begin(); ite!=rvEntries_p->end(); ++ite)
        for(it=ite->begin(); it!=ite->end() && itv < v.end(); ++it, ++itv) *itv=*it;
      if(ite!=rvEntries_p->end())
        {
          where("VectorEntry::asVector(Vector<Real>");
          error("entry_subvector_size_not_constant");
        }
      return v;
    }
  where("VectorEntry::asVector(Vector<Real>");
  error("entry_inconsistent_structure");
  return v;
}

Vector<complex_t>& VectorEntry::asVector(Vector<complex_t>& v) const
{
  if(cEntries_p!=nullptr) return v=*cEntries_p;
  if(cvEntries_p!=nullptr)
    {
      v.resize(cvEntries_p->begin()->size()*cvEntries_p->size());
      Vector<Vector<complex_t> >::iterator ite;
      Vector<complex_t>::iterator it, itv=v.begin();
      for(ite=cvEntries_p->begin(); ite!=cvEntries_p->end(); ++ite)
        for(it=ite->begin(); it!=ite->end() && itv<v.end(); ++it, ++itv) *itv=*it;
      if(ite!=cvEntries_p->end())
        {
          where("VectorEntry::asVector(Vector<Complex>)");
          error("entry_subvector_size_not_constant");
        }
      return v;
    }
  if(rEntries_p!=nullptr) return v=cmplx(*rEntries_p);
  if(rvEntries_p!=nullptr)
    {
      v.resize(rvEntries_p->begin()->size()*rvEntries_p->size());
      Vector<Vector<real_t> >::iterator ite;
      Vector<real_t>::iterator it;
      Vector<complex_t>::iterator itv=v.begin();
      for(ite=rvEntries_p->begin(); ite!=rvEntries_p->end(); ++ite)
        for(it=ite->begin(); it!=ite->end() && itv<v.end(); ++it, ++itv) *itv=*it;
      if(ite!=rvEntries_p->end())
        {
          where("VectorEntry::asVector(Vector<Complex>)");
          error("entry_subvector_size_not_constant");
        }
      return v;
    }
  where("VectorEntry::asVector(Vector<Complex>)");
  error("null_pointer","xxEntries_p");
  return v;
}

Vector<Vector<real_t> >& VectorEntry::asVector(Vector<Vector<real_t> >& v) const
{
  if(rvEntries_p!=nullptr) return v=*rvEntries_p;
  if(rEntries_p !=nullptr)
    {
      v.resize(rEntries_p->size());
      Vector<real_t>::iterator ite;
      Vector<Vector<real_t> >::iterator itv=v.begin();
      for(ite=rEntries_p->begin(); ite!=rEntries_p->end(); ++ite, ++itv)
        *itv = Vector<real_t>(1,*ite);
    }
  where("VectorEntry::Vector<Vector<Real>>");
  error("entry_inconsistent_structure");
  return v;
}

Vector<Vector<complex_t> >& VectorEntry::asVector(Vector<Vector<complex_t> >& v) const
{
  if(cvEntries_p!=nullptr) return v=*cvEntries_p;
  if(cEntries_p !=nullptr)
    {
      v.resize(cEntries_p->size());
      Vector<complex_t>::iterator ite;
      Vector<Vector<complex_t> >::iterator itv=v.begin();
      for(ite=cEntries_p->begin(); ite!=cEntries_p->end(); ++ite, ++itv)
        *itv = Vector<complex_t>(1,*ite);
    }
  if(rvEntries_p!=nullptr) return v=cmplx(*rvEntries_p);
  if(rEntries_p!=nullptr)
    {
      v.resize(rEntries_p->size());
      Vector<real_t>::iterator ite;
      Vector<Vector<complex_t> >::iterator itv=v.begin();
      for(ite=rEntries_p->begin(); ite!=rEntries_p->end(); ++ite, ++itv)
        *itv = Vector<complex_t>(1,complex_t(*ite));
    }
  where("VectorEntry::asVector(Vector<Vector<Complex>>)");
  error("null_pointer","xxEntries_p");
  return v;
}

//extend current entries along vector of size n, along new numbering (see renumber function in space.cpp)
void VectorEntry::extendEntries(std::vector<number_t> renumber, number_t n)
{
  if(rEntries_p != nullptr)
    {
      Vector<real_t>* newEntries_p = new Vector<real_t>(n, 0.); //reallocate new entries
      extendVector(renumber, newEntries_p->begin(), rEntries_p->begin());
      delete rEntries_p;
      rEntries_p = newEntries_p;
      return;
    }
  if(cEntries_p != nullptr)
    {
      Vector<complex_t>* newEntries_p = new Vector<complex_t>(n, complex_t(0.)); //reallocate new entries
      extendVector(renumber, newEntries_p->begin(), cEntries_p->begin());
      delete cEntries_p;
      cEntries_p = newEntries_p;
      return;
    }

  if(rvEntries_p != nullptr)
    {
      number_t nbc = rvEntries_p->begin()->size();
      Vector<Vector<real_t> >* newEntries_p = new Vector<Vector<real_t> >(n, Vector<real_t>(nbc, 0.)); //reallocate new entries
      extendVector(renumber, newEntries_p->begin(), rvEntries_p->begin());
      delete rvEntries_p;
      rvEntries_p = newEntries_p;
      return;
    }

  if(cvEntries_p != nullptr)
    {
      number_t nbc = cvEntries_p->begin()->size();
      Vector<Vector<complex_t> >* newEntries_p = new Vector<Vector<complex_t> >(n, Vector<complex_t>(nbc, complex_t(0.))); //reallocate new entries
      extendVector(renumber, newEntries_p->begin(), cvEntries_p->begin());
      delete cvEntries_p;
      cvEntries_p = newEntries_p;
      return;
    }
}

// extract component i>0 from vector entry of type vector<vector<K> >
// and store extracted values in current entry of type Vector<K>
void VectorEntry::extractComponent(const VectorEntry& v, dimen_t i)
{
  if(v.rvEntries_p != nullptr)  extractComponents(*v.rvEntries_p, *rEntries_p, number_t(i));
  if(v.cvEntries_p != nullptr)  extractComponents(*v.cvEntries_p, *cEntries_p, number_t(i));
  return;
}


// algebraic operations
VectorEntry& VectorEntry::operator+=(const VectorEntry& v)    // operation U+=V (same size, but value types may be different)
{
  // scalar case
  if(rEntries_p != nullptr && v.rEntries_p != nullptr)
    {
      *rEntries_p += * v.rEntries_p;
      return *this;
    }

  if(cEntries_p != nullptr && v.cEntries_p != nullptr)
    {
      *cEntries_p += * v.cEntries_p;
      return *this;
    }

  if(cEntries_p != nullptr && v.rEntries_p != nullptr)
    {
      *cEntries_p += * v.rEntries_p;
      return *this;
    }

  if(rEntries_p != nullptr && v.cEntries_p != nullptr)
    {
      cEntries_p = new Vector<complex_t>(*rEntries_p); //copy real vector in complex vector
      delete rEntries_p;
      rEntries_p=nullptr;
      valueType_ = _complex;
      *cEntries_p += * v.cEntries_p;
      return *this;
    }

  // vector case
  if(rvEntries_p != nullptr && v.rvEntries_p != nullptr)
    {
      *rvEntries_p += * v.rvEntries_p;
      return *this;
    }

  if(cvEntries_p != nullptr && v.cvEntries_p != nullptr)
    {
      *cvEntries_p += * v.cvEntries_p;
      return *this;
    }

  if(cvEntries_p != nullptr && v.rvEntries_p != nullptr)
    {
      *cvEntries_p += * v.rvEntries_p;
      return *this;
    }

  if(rvEntries_p != nullptr && v.cvEntries_p != nullptr)
    {
      cvEntries_p = new Vector<Vector<complex_t> >(*rvEntries_p); //copy real vector in complex vector
      delete rvEntries_p;
      rvEntries_p=nullptr;
      valueType_ = _complex;
      *cvEntries_p += * v.cvEntries_p;
      return *this;
    }

  return *this;
}

VectorEntry& VectorEntry::operator-=(const VectorEntry& v)    // operation U-=V (same size, but value types may be different)
{
  // scalar case
  if(rEntries_p != nullptr && v.rEntries_p != nullptr)
    {
      *rEntries_p -= * v.rEntries_p;
      return *this;
    }

  if(cEntries_p != nullptr && v.cEntries_p != nullptr)
    {
      *cEntries_p -= * v.cEntries_p;
      return *this;
    }

  if(cEntries_p != nullptr && v.rEntries_p != nullptr)
    {
      *cEntries_p -= * v.rEntries_p;
      return *this;
    }

  if(rEntries_p != nullptr && v.cEntries_p != nullptr)
    {
      cEntries_p = new Vector<complex_t>(*rEntries_p); //copy real vector in complex vector
      delete rEntries_p;
      rEntries_p=nullptr;
      valueType_ = _complex;
      *cEntries_p -= * v.cEntries_p;
      return *this;
    }

  // vector case
  if(rvEntries_p != nullptr && v.rvEntries_p != nullptr)
    {
      *rvEntries_p -= * v.rvEntries_p;
      return *this;
    }

  if(cvEntries_p != nullptr && v.cvEntries_p != nullptr)
    {
      *cvEntries_p -= * v.cvEntries_p;
      return *this;
    }

  if(cvEntries_p != nullptr && v.rvEntries_p != nullptr)
    {
      *cvEntries_p -= * v.rvEntries_p;
      return *this;
    }

  if(rvEntries_p != nullptr && v.cvEntries_p != nullptr)
    {
      cvEntries_p = new Vector<Vector<complex_t> >(*rvEntries_p); //copy real vector in complex vector
      delete rvEntries_p;
      rvEntries_p=nullptr;
      valueType_ = _complex;
      *cvEntries_p -= * v.cvEntries_p;
      return *this;
    }
  return *this;
}

VectorEntry& VectorEntry::operator*=(const real_t& r)    // operation U*=r
{
  if(rEntries_p != nullptr)
    {
      *rEntries_p *= r;
      return *this;
    }

  if(rvEntries_p != nullptr)
    {
      std::vector<Vector<real_t> >::iterator it;
      for(it = rvEntries_p->begin(); it != rvEntries_p->end(); it++)  *it *= r;
      return *this;
    }

  if(cEntries_p != nullptr)
    {
      *cEntries_p *= r;
      return *this;
    }

  if(cvEntries_p != nullptr)
    {
      std::vector<Vector<complex_t> >::iterator it;
      for(it = cvEntries_p->begin(); it != cvEntries_p->end(); it++)  *it *= r;
      return *this;
    }

  warning("free_warning", "no allocated values in VectorEntry::operator*=, nothing done");
  return *this;
}

VectorEntry& VectorEntry::operator/=(const real_t& r)    // operation U*=r
{
  if(r == 0)
    {
      where("VectorEntry::operator/=(Real)");
      error("divBy0");
    }
  *this *= (1. / r);
  return *this;
}

VectorEntry& VectorEntry::operator*=(const complex_t& c)    // operation U*=r
{
  if(rEntries_p != nullptr)
    {
      cEntries_p = new Vector<complex_t>(*rEntries_p);   //copy real vector in complex vector
      delete rEntries_p;
      rEntries_p=nullptr;
      valueType_ = _complex;
      *cEntries_p *= c;
      return *this;
    }

  if(rvEntries_p != nullptr)
    {
      cvEntries_p = new Vector<Vector<complex_t> >(*rvEntries_p);   //copy real vector in complex vector
      delete rvEntries_p;
      rvEntries_p=nullptr;
      valueType_ = _complex;
      std::vector<Vector<complex_t> >::iterator it;
      for(it = cvEntries_p->begin(); it != cvEntries_p->end(); it++)  *it *= c;
      return *this;
    }

  if(cEntries_p != nullptr)
    {
      *cEntries_p *= c;
      return *this;
    }

  if(cvEntries_p != nullptr)
    {
      std::vector<Vector<complex_t> >::iterator it;
      for(it = cvEntries_p->begin(); it != cvEntries_p->end(); it++)  *it *= c;
      return *this;
    }

  warning("free_warning", "no allocated values in VectorEntry::operator*=, nothing done");
  return *this;
}

VectorEntry& VectorEntry::operator/=(const complex_t& c)    // operation U*=r
{
  if(std::abs(c) == 0)
    {
      where("VectorEntry::operator/=(Complex)");
      error("divBy0");
    }
  *this *= (1. / c);
  return *this;
}

/*! add to current entry the entry v multiplied by the constant a
  it is assumed that structure types and value types are consistent
*/

void VectorEntry::add(const VectorEntry& v, real_t a)
{
  if(v.strucType_ != strucType_)
    {
      where("VectorEntry::add(VectorEntry,Real)");
      error("entry_mismatch_structures", words("structure",strucType_), words("structure", v.strucType_));
    }

  if(v.size() != size())
    {
      where("VectorEntry::add(VectorEntry,Real)");
      error("entry_mismatch_dims", size(), v.size());
    }

  if(rEntries_p != nullptr && v.rEntries_p != nullptr)
    {
      std::vector<real_t>::iterator it = rEntries_p->begin();
      std::vector<real_t>::const_iterator itv = v.rEntries_p->begin();
      for(; it != rEntries_p->end(); it++, itv++)  *it += a** itv;
      return;
    }

  if(cEntries_p != nullptr && v.rEntries_p != nullptr)
    {
      std::vector<complex_t>::iterator it = cEntries_p->begin();
      std::vector<real_t>::const_iterator itv = v.rEntries_p->begin();
      for(; it != cEntries_p->end(); it++, itv++)  *it += a** itv;
      return;
    }

  if(cEntries_p != nullptr && v.cEntries_p != nullptr)
    {
      std::vector<complex_t>::iterator it = cEntries_p->begin();
      std::vector<complex_t>::const_iterator itv = v.cEntries_p->begin();
      for(; it != cEntries_p->end(); it++, itv++)  *it += a** itv;
      return;
    }

  if(rvEntries_p != nullptr && v.rvEntries_p != nullptr)
    {
      std::vector<Vector<real_t> >::iterator it = rvEntries_p->begin();
      std::vector<Vector<real_t> >::const_iterator itv = v.rvEntries_p->begin();
      for(; it != rvEntries_p->end(); it++, itv++)  *it += a** itv;
      return;
    }

  if(cvEntries_p != nullptr && v.rvEntries_p != nullptr)
    {
      std::vector<Vector<complex_t> >::iterator it = cvEntries_p->begin();
      std::vector<Vector<real_t> >::const_iterator itv = v.rvEntries_p->begin();
      for(; it != cvEntries_p->end(); it++, itv++)  *it += a** itv;
      return;
    }

  if(cvEntries_p != nullptr && v.cvEntries_p != nullptr)
    {
      std::vector<Vector<complex_t> >::iterator it = cvEntries_p->begin();
      std::vector<Vector<complex_t> >::const_iterator itv = v.cvEntries_p->begin();
      for(; it != cvEntries_p->end(); it++, itv++)  *it += a** itv;
      return;
    }

  where("VectorEntry::add(VectorEntry,Real)");
  error("entry_inconsistent_structures") ;
  return;
}

void VectorEntry::add(const VectorEntry& v, complex_t a)
{
  if(v.strucType_ != strucType_)
    {
      where("VectorEntry::add(VectorEntry,Complex)");
      error("entry_mismatch_structures", words("structure",strucType_), words("structure", v.strucType_));
    }

  if(v.size() != size())
    {
      where("VectorEntry::add(VectorEntry,Complex)");
      error("entry_mismatch_dims", size(), v.size());
    }

  if(cEntries_p != nullptr && v.rEntries_p != nullptr)
    {
      std::vector<complex_t>::iterator it = cEntries_p->begin();
      std::vector<real_t>::const_iterator itv = v.rEntries_p->begin();
      for(; it != cEntries_p->end(); it++, itv++)  *it += a** itv;
      return;
    }

  if(cEntries_p != nullptr && v.cEntries_p != nullptr)
    {
      std::vector<complex_t>::iterator it = cEntries_p->begin();
      std::vector<complex_t>::const_iterator itv = v.cEntries_p->begin();
      for(; it != cEntries_p->end(); it++, itv++)  *it += a** itv;
      return;
    }

  if(cvEntries_p != nullptr && v.rvEntries_p != nullptr)
    {
      std::vector<Vector<complex_t> >::iterator it = cvEntries_p->begin();
      std::vector<Vector<real_t> >::const_iterator itv = v.rvEntries_p->begin();
      for(; it != cvEntries_p->end(); it++, itv++)  *it += a** itv;
      return;
    }

  if(cvEntries_p != nullptr && v.cvEntries_p != nullptr)
    {
      std::vector<Vector<complex_t> >::iterator it = cvEntries_p->begin();
      std::vector<Vector<complex_t> >::const_iterator itv = v.cvEntries_p->begin();
      for(; it != cvEntries_p->end(); it++, itv++)  *it += a** itv;
      return;
    }

  where("VectorEntry::add(VectorEntry,Complex)");
  error("entry_inconsistent_structures") ;
  return;

}

/*! add to current entry, at the positions given by pos, the entry ve multiplied by the constant a
    pos is the vector giving the position of the n-th (>=1) coefficients of ve in the current entry
 */
void VectorEntry::add(const VectorEntry& v, const std::vector<number_t>& pos, real_t a)
{
  if(v.strucType_ != strucType_)
    {
      where("VectorEntry::add(VectorEntry,vector<Number>,Real)");
      error("entry_mismatch_structures", words("structure",strucType_), words("structure", v.strucType_));
    }

  if(v.size() > size())
    {
      where("VectorEntry::add(VectorEntry,vector<Number>,Real)");
      error("entry_oversize", size(), v.size());
    }

  if(rEntries_p != nullptr && v.rEntries_p != nullptr)
    {
      std::vector<real_t>::iterator itb = rEntries_p->begin();
      std::vector<real_t>::const_iterator itv = v.rEntries_p->begin();
      std::vector<number_t>::const_iterator itp = pos.begin();
      for(; itp != pos.end(); itv++, itp++)  *(itb + *itp - 1)  += a** itv;
      return;
    }

  if(cEntries_p != nullptr && v.rEntries_p != nullptr)
    {
      std::vector<complex_t>::iterator itb = cEntries_p->begin();
      std::vector<real_t>::const_iterator itv = v.rEntries_p->begin();
      std::vector<number_t>::const_iterator itp = pos.begin();
      for(; itp != pos.end(); itv++, itp++)  *(itb + *itp - 1)  += a** itv;
      return;
    }

  if(cEntries_p != nullptr && v.cEntries_p != nullptr)
    {
      std::vector<complex_t>::iterator itb = cEntries_p->begin();
      std::vector<complex_t>::const_iterator itv = v.cEntries_p->begin();
      std::vector<number_t>::const_iterator itp = pos.begin();
      for(; itp != pos.end(); itv++, itp++)  *(itb + *itp - 1)  += a** itv;
      return;
    }

  if(rvEntries_p != nullptr && v.rvEntries_p != nullptr)
    {
      std::vector<Vector<real_t> >::iterator itb = rvEntries_p->begin();
      std::vector<Vector<real_t> >::const_iterator itv = v.rvEntries_p->begin();
      std::vector<number_t>::const_iterator itp = pos.begin();
      for(; itp != pos.end(); itv++, itp++)  *(itb + *itp - 1)  += a** itv;
      return;
    }

  if(cvEntries_p != nullptr && v.rvEntries_p != nullptr)
    {
      std::vector<Vector<complex_t> >::iterator itb = cvEntries_p->begin();
      std::vector<Vector<real_t> >::const_iterator itv = v.rvEntries_p->begin();
      std::vector<number_t>::const_iterator itp = pos.begin();
      for(; itp != pos.end(); itv++, itp++)  *(itb + *itp - 1)  += a** itv;
      return;
    }

  if(cvEntries_p != nullptr && v.cvEntries_p != nullptr)
    {
      std::vector<Vector<complex_t> >::iterator itb = cvEntries_p->begin();
      std::vector<Vector<complex_t> >::const_iterator itv = v.cvEntries_p->begin();
      std::vector<number_t>::const_iterator itp = pos.begin();
      for(; itp != pos.end(); itv++, itp++)  *(itb + *itp - 1)  += a** itv;
      return;
    }

  where("VectorEntry::add(VectorEntry,vector<Number,Real)");
  error("entry_inconsistent_structures");
  return;
}

void VectorEntry::add(const VectorEntry& v, const std::vector<number_t>& pos, complex_t a)
{
  if(v.strucType_ != strucType_)
    {
      where("VectorEntry::add(VectorEntry,vector<Number>,Complex)");
      error("entry_mismatch_structures", words("structure",strucType_), words("structure", v.strucType_));
    }

  if(v.size() > size())
    {
      where("VectorEntry::add(VectorEntry,vector<Number>,Complex)");
      error("entry_oversize", size(), v.size());
    }

  if(cEntries_p != nullptr && v.rEntries_p != nullptr)
    {
      std::vector<complex_t>::iterator itb = cEntries_p->begin();
      std::vector<real_t>::const_iterator itv = v.rEntries_p->begin();
      std::vector<number_t>::const_iterator itp = pos.begin();
      for(; itp != pos.end(); itv++, itp++)  *(itb + *itp - 1)  += a** itv;
      return;
    }

  if(cEntries_p != nullptr && v.cEntries_p != nullptr)
    {
      std::vector<complex_t>::iterator itb = cEntries_p->begin();
      std::vector<complex_t>::const_iterator itv = v.cEntries_p->begin();
      std::vector<number_t>::const_iterator itp = pos.begin();
      for(; itp != pos.end(); itv++, itp++)  *(itb + *itp - 1)  += a** itv;
      return;
    }

  if(cvEntries_p != nullptr && v.rvEntries_p != nullptr)
    {
      std::vector<Vector<complex_t> >::iterator itb = cvEntries_p->begin();
      std::vector<Vector<real_t> >::const_iterator itv = v.rvEntries_p->begin();
      std::vector<number_t>::const_iterator itp = pos.begin();
      for(; itp != pos.end(); itv++, itp++)  *(itb + *itp - 1)  += a** itv;
      return;
    }

  if(cvEntries_p != nullptr && v.cvEntries_p != nullptr)
    {
      std::vector<Vector<complex_t> >::iterator itb = cvEntries_p->begin();
      std::vector<Vector<complex_t> >::const_iterator itv = v.cvEntries_p->begin();
      std::vector<number_t>::const_iterator itp = pos.begin();
      for(; itp != pos.end(); itv++, itp++)  *(itb + *itp - 1)  += a** itv;
      return;
    }

  where("VectorEntry::add(VectorEntry,vector<Number,Complex)");
  error("entry_inconsistent_structures");
  return;
}

//! delete rows r1,..., r2
void VectorEntry::deleteRows(number_t r1, number_t r2)
{
  if(rEntries_p != nullptr)  rEntries_p->deleteRows(r1,r2);
  if(cEntries_p != nullptr)  cEntries_p->deleteRows(r1,r2);
  if(rvEntries_p != nullptr) rvEntries_p->deleteRows(r1,r2);
  if(cvEntries_p != nullptr) cvEntries_p->deleteRows(r1,r2);
}

//! return number of 0 components (|xi|<=tol)
number_t VectorEntry::nbzero(real_t tol) const
{
  if(rEntries_p != nullptr)  return rEntries_p->nbzero(tol);
  if(cEntries_p != nullptr)  return cEntries_p->nbzero(tol);
  if(rvEntries_p != nullptr) return rvEntries_p->nbzero(tol);
  if(cvEntries_p != nullptr) return cvEntries_p->nbzero(tol);
  return 0;
}

// create new scalar vector entry from non scalar vector entry
VectorEntry* VectorEntry::toScalar()
{
  dimen_t nbc=nbOfComponents;
  if(nbc == 1) return this;   //already scalar, nothing done
  //create new scalar entries
  VectorEntry* vec = new VectorEntry(valueType_, _scalar, nbc*size());
  if(rvEntries_p != nullptr)
    {
      Vector<real_t>::iterator its=vec->rEntries_p->begin();
      Vector<Vector<real_t> >::iterator itv=rvEntries_p->begin();
      for(; itv!=rvEntries_p->end(); itv++)
        for(dimen_t d=0; d<nbc; d++, its++) *its = (*itv)[d];
    }

  if(cvEntries_p != nullptr)
    {
      Vector<complex_t>::iterator its=vec->cEntries_p->begin();
      Vector<Vector<complex_t> >::iterator itv=cvEntries_p->begin();
      for(; itv!=cvEntries_p->end(); itv++)
        for(dimen_t d=0; d<nbc; d++, its++) *its = (*itv)[d];
    }

  nbOfComponents=1;
  return vec;
}

/*! transform current data to abs, real and imaginary part
    in any case, these functions transform entries v to real entries
*/
VectorEntry& VectorEntry::toAbs()
{
  if(rEntries_p != nullptr)
    {
      Vector<real_t>* newEntries=new Vector<real_t>(abs(*rEntries_p));
      delete rEntries_p;
      rEntries_p=newEntries;
      return *this;
    }

  if(cEntries_p != nullptr)
    {
      Vector<real_t>* newEntries=new Vector<real_t>(abs(*cEntries_p));
      delete cEntries_p;
      cEntries_p=nullptr;
      rEntries_p=newEntries;
      valueType_=_real;
      return *this;
    }

  if(rvEntries_p != nullptr)
    {
      Vector<Vector<real_t> >* newEntries=new Vector<Vector<real_t> >(abs(*rvEntries_p));
      delete rvEntries_p;
      rvEntries_p=newEntries;
      return *this;
    }

  if(cvEntries_p != nullptr)
    {
      Vector<Vector<real_t> >* newEntries=new Vector<Vector<real_t> >(abs(*cvEntries_p));
      delete cvEntries_p;
      cvEntries_p=nullptr;
      rvEntries_p=newEntries;
      valueType_=_real;
      return *this;
    }

  return *this;
}

VectorEntry& VectorEntry::toReal()
{
  if(rEntries_p != nullptr)
    {
      return *this;
    }

  if(cEntries_p != nullptr)
    {
      Vector<real_t>* newEntries=new Vector<real_t>(real(*cEntries_p));
      delete cEntries_p;
      rEntries_p=newEntries;
      valueType_=_real;
      cEntries_p=nullptr;
      return *this;
    }

  if(rvEntries_p != nullptr)
    {
      return *this;
    }

  if(cvEntries_p != nullptr)
    {
      Vector<Vector<real_t> >* newEntries=new Vector<Vector<real_t> >(real(*cvEntries_p));
      delete cvEntries_p;
      rvEntries_p=newEntries;
      valueType_=_real;
      cvEntries_p=nullptr;
      return *this;
    }

  return *this;
}

VectorEntry& VectorEntry::toImag()
{
  if(rEntries_p != nullptr)
    {
      Vector<real_t>* newEntries=new Vector<real_t>(imag(*rEntries_p));
      delete rEntries_p;
      rEntries_p=newEntries;
      return *this;
    }

  if(cEntries_p != nullptr)
    {
      Vector<real_t>* newEntries=new Vector<real_t>(imag(*cEntries_p));
      delete cEntries_p;
      cEntries_p=nullptr;
      rEntries_p=newEntries;
      valueType_=_real;
      return *this;
    }

  if(rvEntries_p != nullptr)
    {
      Vector<Vector<real_t> >* newEntries=new Vector<Vector<real_t> >(imag(*rvEntries_p));
      delete rvEntries_p;
      rvEntries_p=newEntries;
      return *this;
    }

  if(cvEntries_p != nullptr)
    {
      Vector<Vector<real_t> >* newEntries=new Vector<Vector<real_t> >(imag(*cvEntries_p));
      delete cvEntries_p;
      cvEntries_p=nullptr;
      rvEntries_p=newEntries;
      valueType_=_real;
      return *this;
    }

  return *this;
}

//v -> conjugate of v (modify current entries), if real nothing is done
VectorEntry& VectorEntry::toConj()
{
    if(rEntries_p != nullptr || rvEntries_p != nullptr) return *this;   // real entries, nothing to do
    if(cEntries_p != nullptr)  cEntries_p->toConj();
    if(cvEntries_p != nullptr) cvEntries_p->toConj();
    return *this;
}

// round to zero all coefficients close to 0 (|.| < aszero)
VectorEntry& VectorEntry::roundToZero(real_t aszero)
{
    if(rEntries_p != nullptr) rEntries_p->roundToZero(aszero);
    if(rvEntries_p != nullptr) rvEntries_p->roundToZero(aszero);
    if(cEntries_p != nullptr)  cEntries_p->roundToZero(aszero);
    if(cvEntries_p != nullptr) cvEntries_p->roundToZero(aszero);
    return *this;
}

// round with a given prec
VectorEntry& VectorEntry::round(real_t prec)
{
    if(prec<=0) return *this;
    if(rEntries_p != nullptr) rEntries_p->round(prec);
    if(rvEntries_p != nullptr) rvEntries_p->round(prec);
    if(cEntries_p != nullptr)  cEntries_p->round(prec);
    if(cvEntries_p != nullptr) cvEntries_p->round(prec);
    return *this;
}

// modify current entries to complex entries if they are not
VectorEntry& VectorEntry::toComplex()
{
  if(cEntries_p != nullptr || cvEntries_p != nullptr) return *this;   //already complex
  if(rEntries_p != nullptr)
    {
      Vector<complex_t>* newEntries=new Vector<complex_t>(cmplx(*rEntries_p));
      delete rEntries_p;
      rEntries_p=nullptr;
      cEntries_p=newEntries;
      valueType_=_complex;
      return *this;
    }

  if(rvEntries_p != nullptr)
    {
      Vector<Vector<complex_t> >* newEntries=new Vector<Vector<complex_t> >(cmplx(*rvEntries_p));
      delete rvEntries_p;
      rvEntries_p=nullptr;
      cvEntries_p=newEntries;
      valueType_=_complex;
    }

  return *this;
}

// if scalar entries, modify current scalar entries to vector entries (1<=i<=n)
VectorEntry& VectorEntry::toVector(dimen_t n, dimen_t i)
{
  if(rEntries_p == nullptr && cEntries_p == nullptr) return *this;   //not scalar, nothing done
  if(rEntries_p != nullptr)
    {
      if(rvEntries_p!=nullptr) delete rvEntries_p;   //should be not in that case
      rvEntries_p=new Vector<Vector<real_t> >(rEntries_p->size());
      Vector<real_t>::iterator itv=rEntries_p->begin();
      Vector<Vector<real_t> >::iterator itvv=rvEntries_p->begin();
      Vector<real_t> v(n,0.);
      for(; itv!=rEntries_p->end(); itv++, itvv++)
        {
          v(i)=*itv; *itvv=v;
        }
      delete rEntries_p;
      rEntries_p=nullptr;
      nbOfComponents=n;
      strucType_=_vector;
      return *this;
    }

  if(cEntries_p != nullptr)
    {
      if(cvEntries_p!=nullptr) delete cvEntries_p;   //should be not in that case
      cvEntries_p=new Vector<Vector<complex_t> >(cEntries_p->size());
      Vector<complex_t>::iterator itv=cEntries_p->begin();
      Vector<Vector<complex_t> >::iterator itvv=cvEntries_p->begin();
      Vector<complex_t> v(n,complex_t(0.));
      for(; itv!=cEntries_p->end(); itv++, itvv++)
        {
          v(i)=*itv; *itvv=v;
        }
      delete cEntries_p;
      cEntries_p=nullptr;
      nbOfComponents=n;
      strucType_=_vector;
      return *this;
    }
  return *this; // dummy return
}

// return the value (in complex) of component being the largest one in absolute value
complex_t VectorEntry::maxValAbs() const
{
  if(rEntries_p != nullptr)  return maxAbsVal(*rEntries_p);
  if(cEntries_p != nullptr)  return maxAbsVal(*cEntries_p);
  if(rvEntries_p != nullptr) return maxAbsVal(*rvEntries_p);
  if(cvEntries_p != nullptr) return maxAbsVal(*cvEntries_p);
  return complex_t(0);
}

//in/out VectorEntry
void VectorEntry::saveToFile(const string_t& fn, number_t prec, bool encode) const
{
  string_t fen = fn;
  if(encode) fen = encodeFileName(fn);
  std::ofstream of(fen.c_str());
  if(of.fail()) error("file_failopen", "VectorEntry::saveToFile", fen);
  of.precision(prec);
  if(rEntries_p != nullptr) rEntries_p->printRaw(of);
  if(cEntries_p != nullptr) cEntries_p->printRaw(of);
  if(rvEntries_p != nullptr) rvEntries_p->printRaw(of);
  if(cvEntries_p != nullptr) cvEntries_p->printRaw(of);
  of.close();
}

string_t  VectorEntry::encodeFileName(const string_t& fn) const
{
  std::pair<string_t, string_t> fext = fileRootExtension(fn, Environment::authorizedSaveToFileExtensions());
  string_t fname = fext.first;
  string_t vt = "real";
  if(valueType_ == _complex) vt = "complex";
  fname += "(" + tostring(size()) +  "_" + vt;
  if(strucType_ == _scalar) fname += "_scalar)." + fext.second;
  else fname += "_vector_" + tostring(nbOfComponents) + ")." + fext.second;
  return fname;
}

//print on stream first n values with comment for each line
void VectorEntry::printFirst(std::ostream& out, number_t n, std::vector<string_t> comment) const
{
  if(rEntries_p != nullptr)
    {
      number_t m = std::min(n, number_t(rEntries_p->size()));
      std::vector<real_t>::const_iterator it = rEntries_p->begin();
      if(comment.size() == 0) comment.assign(m, "");
      std::vector<string_t>::iterator itc = comment.begin();
      for(number_t j = 0; j < m; it++, itc++, j++)  out << *itc << *it << eol;
      return;
    }
  if(cEntries_p != nullptr)
    {
      number_t m = std::min(n, number_t(cEntries_p->size()));
      std::vector<complex_t>::const_iterator it = cEntries_p->begin();
      if(comment.size() == 0) comment.assign(m, "");
      std::vector<string_t>::iterator itc = comment.begin();
      for(number_t j = 0; j < m; it++, itc++, j++)  out << *itc << *it << eol;
      return;
    }
  if(rvEntries_p != nullptr)
    {
      number_t m = std::min(n, number_t(rvEntries_p->size()));
      std::vector<Vector<real_t> >::const_iterator it = rvEntries_p->begin();
      if(comment.size() == 0) comment.assign(m, "");
      std::vector<string_t>::iterator itc = comment.begin();
      for(number_t j = 0; j < m; it++, itc++, j++)  out << *itc << *it << eol;
      return;
    }
  if(cvEntries_p != nullptr)
    {
      number_t m = std::min(n, number_t(cvEntries_p->size()));
      std::vector<Vector<complex_t> >::const_iterator it = cvEntries_p->begin();
      if(comment.size() == 0) comment.assign(m, "");
      std::vector<string_t>::iterator itc = comment.begin();
      for(number_t j = 0; j < m; it++, itc++, j++)  out << *itc << *it << eol;
      return;
    }
}

//print on stream last n values
void VectorEntry::printLast(std::ostream& out, number_t n, std::vector<string_t> comment) const
{
  if(rEntries_p != nullptr)
    {
      number_t s = std::max(number_t(0), rEntries_p->size() - n);
      number_t m = std::min(n, number_t(rEntries_p->size()));
      std::vector<real_t>::const_iterator it = rEntries_p->begin() + s;
      if(comment.size() == 0) comment.assign(m, "");
      std::vector<string_t>::iterator itc = comment.begin();
      for(number_t j = 0; j < m; it++, itc++, j++)   out << *itc << *it << eol;
      return;
    }
  if(cEntries_p != nullptr)
    {
      number_t s = std::max(number_t(0), cEntries_p->size() - n);
      number_t m = std::min(n, number_t(cEntries_p->size()));
      std::vector<complex_t>::const_iterator it = cEntries_p->begin() + s;
      if(comment.size() == 0) comment.assign(m, "");
      std::vector<string_t>::iterator itc = comment.begin();
      for(number_t j = 0; j < m; it++, itc++, j++)   out << *itc << *it << eol;
      return;
    }
  if(rvEntries_p != nullptr)
    {
      number_t s = std::max(number_t(0), rvEntries_p->size() - n);
      number_t m = std::min(n, number_t(rvEntries_p->size()));
      std::vector<Vector<real_t> >::const_iterator it = rvEntries_p->begin() + s;
      if(comment.size() == 0) comment.assign(m, "");
      std::vector<string_t>::iterator itc = comment.begin();
      for(number_t j = 0; j < m; it++, itc++, j++)   out << *itc << *it << eol;
      return;
    }
  if(cvEntries_p != nullptr)
    {
      number_t s = std::max(number_t(0), cvEntries_p->size() - n);
      number_t m = std::min(n, number_t(cvEntries_p->size()));
      std::vector<Vector<complex_t> >::const_iterator it = cvEntries_p->begin() + s;
      if(comment.size() == 0) comment.assign(m, "");
      std::vector<string_t>::iterator itc = comment.begin();
      for(number_t j = 0; j < m; it++, itc++, j++)   out << *itc << *it << eol;
      return;
    }
}

// return v(m)*a
real_t VectorEntry::product(number_t m, const real_t& a) const
{
  if(rEntries_p != nullptr)  return (*rEntries_p)[m] * a;
  where("VectorEntry::product(Number,Real)");
  error("entry_inconsistent_structure");
  return 0.;
}

complex_t VectorEntry::product(number_t m, const complex_t& a) const
{
  if(rEntries_p != nullptr)  return (*rEntries_p)[m] * a;
  if(cEntries_p != nullptr)  return (*cEntries_p)[m] * a;
  where("VectorEntry::product(Number,complex)");
  error("entry_inconsistent_structure");
  return complex_t(0);
}
/*! move columns according to a map m between current entries and a vector entries of dim n=size(m)
    the map m= (m1, ..., mk, ...) gives the displacement of columns in set {1,2,...n} :
       the column mk moves at position k
       mk=0 means that the column k is 0
    example:   m=[0 2 1]  :  [col1, col2, ...] --> [0 col2 col1]
    Numbering starts at 1
*/
void VectorEntry::moveColumns(const std::vector<number_t>& m)
{
  number_t n=m.size(), s=nbOfComponents;
  if(n==0)
    {
      where("VectorEntry::moveColumns");
      error("is_void", "mapping");
    }
  number_t mx=0;
  std::vector<number_t>::const_iterator itm=m.begin();
  for(; itm!=m.end(); itm++)
    if(*itm > mx) mx=*itm;
  if(mx>nbOfComponents) error("entry_incorrect_mapping",mx,nbOfComponents);

  nbOfComponents=n;

  if(s==1 && n==1)   //scalar to scalar
    {
      if(m[0]!=0) return ; //nothing to do
      if(rEntries_p!=nullptr) *rEntries_p *=0;
      else if(cEntries_p!=nullptr) *cEntries_p *=0;
      return;
    }

  if(s==1 && n>1)   //scalar to vector
    {
      if(rEntries_p!=nullptr)   //real case
        {
          rvEntries_p= new Vector<Vector<real_t> >(size(),Vector<real_t>(n,0.));
          Vector<real_t>::iterator itr=rEntries_p->begin();
          Vector<Vector<real_t> >::iterator itrv=rvEntries_p->begin();
          Vector<real_t> v(n,0.);
          for(; itrv!=rvEntries_p->end(); itrv++)
            {
              number_t i=0;
              for(itm=m.begin(); itm!=m.end(); itm++,i++)
                {
                  number_t k=*itm;
                  if(k!=0) v[i]=*itr;
                }
              *itrv=v;
            }
          delete rEntries_p;
          rEntries_p=nullptr;
          return;
        }
      if(cEntries_p!=nullptr)   //complex case
        {
          cvEntries_p= new Vector<Vector<complex_t> >(size(),Vector<complex_t>(n,complex_t(0.)));
          Vector<complex_t>::iterator itr=cEntries_p->begin();
          Vector<Vector<complex_t> >::iterator itrv=cvEntries_p->begin();
          Vector<complex_t> v(n,complex_t(0.));
          for(; itrv!=cvEntries_p->end(); itrv++)
            {
              number_t i=0;
              for(itm=m.begin(); itm!=m.end(); itm++,i++)
                {
                  number_t k=*itm;
                  if(k!=0) v[i]=*itr;
                }
              *itrv=v;
            }
          delete cEntries_p;
          cEntries_p=nullptr;
          return;
        }
    }

  if(s>1 && n==1)   //vector to scalar
    {
      number_t k=m[0];
      if(rvEntries_p!=nullptr)   //real case
        {
          rEntries_p= new Vector<real_t>(size(), 0.);
          if(k!=0)
            {
              k--;
              Vector<real_t>::iterator itr=rEntries_p->begin();
              Vector<Vector<real_t> >::iterator itrv=rvEntries_p->begin();
              for(; itrv!=rvEntries_p->end(); itrv++, itr++) *itr = (*itrv)[k];
            }
          delete rvEntries_p;
          rvEntries_p=nullptr;
          return;
        }
      if(cvEntries_p!=nullptr)   //complex case
        {
          cEntries_p= new Vector<complex_t>(size(), complex_t(0.));
          if(k!=0)
            {
              k--;
              Vector<complex_t>::iterator itr=cEntries_p->begin();
              Vector<Vector<complex_t> >::iterator itrv=cvEntries_p->begin();
              for(; itrv!=cvEntries_p->end(); itrv++, itr++) *itr = (*itrv)[k];
            }
          delete cvEntries_p;
          cvEntries_p=nullptr;
          return;
        }
    }

  //vector to vector
  if(rvEntries_p!=nullptr)   //real case
    {
      Vector<Vector<real_t> >::iterator itrv=rvEntries_p->begin();
      Vector<real_t> v(n,0.);
      for(; itrv!=rvEntries_p->end(); itrv++)
        {
          number_t i=0;
          for(itm=m.begin(); itm!=m.end(); itm++,i++)
            {
              number_t k=*itm;
              if(k!=0) v[i]=(*itrv)[k-1];
            }
          *itrv=v;
        }
      return;
    }
  if(cvEntries_p!=nullptr)   //real case
    {
      Vector<Vector<complex_t> >::iterator itrv=cvEntries_p->begin();
      Vector<complex_t> v(n,complex_t(0.));
      for(; itrv!=cvEntries_p->end(); itrv++)
        {
          number_t i=0;
          for(itm=m.begin(); itm!=m.end(); itm++,i++)
            {
              number_t k=*itm;
              if(k!=0) v[i]=(*itrv)[k-1];
            }
          *itrv=v;
        }
    }
}

// output VectorEntry on stream
void VectorEntry::print(std::ostream& out) const
{
  if(rEntries_p != nullptr)  out << *rEntries_p;
  if(cEntries_p != nullptr)  out << *cEntries_p;
  if(rvEntries_p != nullptr) out << *rvEntries_p;
  if(cvEntries_p != nullptr) out << *cvEntries_p;
}

std::ostream& operator<<(std::ostream& out, const VectorEntry& ve)
{
  ve.print(out);
  return out;
}

//inner and hermitian products, it is assumed that vectors have the same size
complex_t innerProduct(const VectorEntry& v1, const VectorEntry& v2)
{
  complex_t res = 0.;
  if(v1.rEntries_p != nullptr)
    {
      if(v2.rEntries_p != nullptr)  return std::inner_product(v1.rEntries_p->begin(), v1.rEntries_p->end(), v2.rEntries_p->begin(), res);
      if(v2.cEntries_p != nullptr)  return std::inner_product(v1.rEntries_p->begin(), v1.rEntries_p->end(), v2.cEntries_p->begin(), res);
      where("innerProduct(VectorEntry,VectorEntry)");
      error("entry_inconsistent_structures");
    }
  if(v1.cEntries_p != nullptr)
    {
      if(v2.rEntries_p != nullptr)  return std::inner_product(v1.cEntries_p->begin(), v1.cEntries_p->end(), v2.rEntries_p->begin(), res);
      if(v2.cEntries_p != nullptr)  return std::inner_product(v1.cEntries_p->begin(), v1.cEntries_p->end(), v2.cEntries_p->begin(), res);
      where("innerProduct(VectorEntry,VectorEntry)");
      error("entry_inconsistent_structures");
    }

  if(v1.rvEntries_p != nullptr)
    {
      if(v2.rvEntries_p != nullptr)
        {
          Vector<Vector<real_t> >::const_iterator itv1, itv2 = v2.rvEntries_p->begin();
          for(itv1 = v1.rvEntries_p->begin(); itv1 != v1.rvEntries_p->end(); itv1++, itv2++)
            res += dotRC(*itv1, *itv2);
          return res;
        }
      if(v2.cvEntries_p != nullptr)
        {
          Vector<Vector<real_t> >::const_iterator itv1;
          Vector<Vector<complex_t> >::const_iterator itv2 = v2.cvEntries_p->begin();
          for(itv1 = v1.rvEntries_p->begin(); itv1 != v1.rvEntries_p->end(); itv1++, itv2++)
            res += dotRC(*itv1, *itv2);
          return res;
        }
      where("innerProduct(VectorEntry,VectorEntry)");
      error("entry_inconsistent_structures");
    }
  if(v1.cvEntries_p != nullptr)
    {
      if(v2.rvEntries_p != nullptr)
        {
          Vector<Vector<complex_t> >::const_iterator itv1;
          Vector<Vector<real_t> >::const_iterator itv2 = v2.rvEntries_p->begin();
          for(itv1 = v1.cvEntries_p->begin(); itv1 != v1.cvEntries_p->end(); itv1++, itv2++)
            res += dotRC(*itv1, *itv2);
          return res;
        }
      if(v2.cvEntries_p != nullptr)
        {
          Vector<Vector<complex_t> >::const_iterator itv1, itv2 = v2.cvEntries_p->begin();
          for(itv1 = v1.cvEntries_p->begin(); itv1 != v1.cvEntries_p->end(); itv1++, itv2++)
            res += dotRC(*itv1, *itv2);
          return res;
        }
      where("innerProduct(VectorEntry,VectorEntry)");
      error("entry_inconsistent_structures");
    }
  where("innerProduct(VectorEntry,VectorEntry)");
  error("null_pointer","xxEntries_p");
  return res;
}

// same as inner product of two vectorentry's
complex_t dotRC(const VectorEntry& v1, const VectorEntry& v2)
{
  return innerProduct(v1, v2);
}

// hermitian product of two vectorentry's
complex_t hermitianProduct(const VectorEntry& v1, const VectorEntry& v2)
{
  complex_t res = 0.;
  if(v1.rEntries_p != nullptr)
    {
      if(v2.rEntries_p != nullptr)  return dotC(*v1.rEntries_p, *v2.rEntries_p);
      if(v2.cEntries_p != nullptr)  return dotC(*v1.rEntries_p, *v2.cEntries_p);
      where("hermitianProduct(VectorEntry,VectorEntry)");
      error("entry_inconsistent_structures");
    }
  if(v1.cEntries_p != nullptr)
    {
      if(v2.rEntries_p != nullptr)  return dotC(*v1.cEntries_p, *v2.rEntries_p);
      if(v2.cEntries_p != nullptr)  return dotC(*v1.cEntries_p, *v2.cEntries_p);
      where("hermitianProduct(VectorEntry,VectorEntry)");
      error("entry_inconsistent_structures");
    }
  if(v1.rvEntries_p != nullptr)
    {
      if(v2.rvEntries_p != nullptr)
        {
          Vector<Vector<real_t> >::const_iterator itv1, itv2 = v2.rvEntries_p->begin();
          for(itv1 = v1.rvEntries_p->begin(); itv1 != v1.rvEntries_p->end(); itv1++, itv2++) res += dotC(*itv1, *itv2);
          return res;
        }
      if(v2.cvEntries_p != nullptr)
        {
          Vector<Vector<real_t> >::const_iterator itv1;
          Vector<Vector<complex_t> >::const_iterator itv2 = v2.cvEntries_p->begin();
          for(itv1 = v1.rvEntries_p->begin(); itv1 != v1.rvEntries_p->end(); itv1++, itv2++) res += dotC(*itv1, *itv2);
          return res;
        }
      where("hermitianProduct(VectorEntry,VectorEntry)");
      error("entry_inconsistent_structures");
    }
  if(v1.cvEntries_p != nullptr)
    {
      if(v2.rvEntries_p != nullptr)
        {
          Vector<Vector<complex_t> >::const_iterator itv1;
          Vector<Vector<real_t> >::const_iterator itv2 = v2.rvEntries_p->begin();
          for(itv1 = v1.cvEntries_p->begin(); itv1 != v1.cvEntries_p->end(); itv1++, itv2++) res += dotC(*itv1, *itv2);
          return res;
        }
      if(v2.cvEntries_p != nullptr)
        {
          Vector<Vector<complex_t> >::const_iterator itv1, itv2 = v2.cvEntries_p->begin();
          for(itv1 = v1.cvEntries_p->begin(); itv1 != v1.cvEntries_p->end(); itv1++, itv2++) res += dotC(*itv1, *itv2);
          return res;
        }
      where("hermitianProduct(VectorEntry,VectorEntry)");
      error("entry_inconsistent_structures");
    }
  where("hermitianProduct(VectorEntry,VectorEntry)");
  error("null_pointer","xxEntries_p");
  return res;
}

//norm of vectorentry
real_t norm(const VectorEntry& v, number_t l)
{
  switch(l)
    {
      case 0:
        return norminfty(v);
      case 1:
        return norm1(v);
      default:
        return norm2(v);
    }
}

real_t norm2(const VectorEntry& v)
{
  if(v.rEntries_p != nullptr) return  v.rEntries_p->norm2();
  if(v.cEntries_p != nullptr) return  v.cEntries_p->norm2();
  if(v.rvEntries_p != nullptr)
    {
      real_t r = 0.;
      Vector<Vector<real_t> >::const_iterator itv;
      for(itv = v.rvEntries_p->begin(); itv != v.rvEntries_p->end(); itv++)
        {
          real_t t=itv->norm2();
          r+=t*t;
        }
      return std::sqrt(r);
    }
  if(v.cvEntries_p != nullptr)
    {
      real_t r = 0.;
      Vector<Vector<complex_t> >::const_iterator itv;
      for(itv = v.cvEntries_p->begin(); itv != v.cvEntries_p->end(); itv++)
        {
          real_t t=itv->norm2();
          r+=t*t;
        }
      return std::sqrt(r);
    }
  return 0.;
}

real_t norm1(const VectorEntry& v)
{
  if(v.rEntries_p != nullptr) return  v.rEntries_p->norm1();
  if(v.cEntries_p != nullptr) return  v.cEntries_p->norm1();
  if(v.rvEntries_p != nullptr)
    {
      real_t r = 0.;
      Vector<Vector<real_t> >::const_iterator itv;
      for(itv = v.rvEntries_p->begin(); itv != v.rvEntries_p->end(); itv++) r += itv->norm1();
      return r;
    }
  if(v.cvEntries_p != nullptr)
    {
      real_t r = 0.;
      Vector<Vector<complex_t> >::const_iterator itv;
      for(itv = v.cvEntries_p->begin(); itv != v.cvEntries_p->end(); itv++) r += itv->norm1();
      return r;
    }
  return 0.;
}

real_t norminfty(const VectorEntry& v)
{
  if(v.rEntries_p != nullptr) return  v.rEntries_p->norminfty();
  if(v.cEntries_p != nullptr) return  v.cEntries_p->norminfty();
  if(v.rvEntries_p != nullptr)
    {
      real_t r = 0.;
      Vector<Vector<real_t> >::const_iterator itv;
      for(itv = v.rvEntries_p->begin(); itv != v.rvEntries_p->end(); itv++) r = std::max(r, itv->norminfty());
      return r;
    }
  if(v.cvEntries_p != nullptr)
    {
      real_t r = 0.;
      Vector<Vector<complex_t> >::const_iterator itv;
      for(itv = v.cvEntries_p->begin(); itv != v.cvEntries_p->end(); itv++)  r = std::max(r, itv->norminfty());
      return r;
    }
  return 0.;
}

//special accumulation x+=t*v (t real)
void addScaledVector(VectorEntry& x, VectorEntry& v, const real_t& t)
{
  ValueType vtx=x.valueType_, vtv=v.valueType_;
  StrucType stx=x.strucType_, stv=v.strucType_;
  if(stx==_scalar && stv==_scalar)
    {
      if(vtv==_real)
        {
          if(vtx==_real)
            {
              Vector<real_t>::iterator itx, itv=v.rEntries_p->begin();
              for(itx = x.rEntries_p->begin(); itx != x.rEntries_p->end() && itv != v.rEntries_p->end(); ++itx, ++itv) *itx += t** itv;
            }
          else //vtx = _complex
            {
              Vector<complex_t>::iterator itx;
              Vector<real_t>::iterator itv=v.rEntries_p->begin();
              for(itx = x.cEntries_p->begin(); itx != x.cEntries_p->end() && itv != v.rEntries_p->end(); ++itx, ++itv) *itx += t** itv;
            }
        }
      else //vtv = _complex
        {
          if(vtx==_real) x.toComplex();
          Vector<complex_t>::iterator itx, itv=v.cEntries_p->begin();
          for(itx = x.cEntries_p->begin(); itx != x.cEntries_p->end() && itv != v.cEntries_p->end(); ++itx, ++itv) *itx += t** itv;
        }
      return;
    }
   if(stx==_vector && stv==_vector && x.nbOfComponents==v.nbOfComponents)
    {
      if(vtv==_real)
        {
          if(vtx==_real)
            {
              Vector<Vector<real_t> >::iterator itx, itv=v.rvEntries_p->begin();
              for(itx = x.rvEntries_p->begin(); itx != x.rvEntries_p->end() && itv != v.rvEntries_p->end(); ++itx, ++itv) *itx += t** itv;
            }
          else //vtx = _complex
            {
              Vector<Vector<complex_t> >::iterator itx;
              Vector<Vector<real_t> >::iterator itv=v.rvEntries_p->begin();
              for(itx = x.cvEntries_p->begin(); itx != x.cvEntries_p->end() && itv != v.rvEntries_p->end(); ++itx, ++itv) *itx += t** itv;
            }
        }
      else //vtv = _complex
        {
          if(vtx==_real) x.toComplex();
          Vector<Vector<complex_t> >::iterator itx, itv=v.cvEntries_p->begin();
          for(itx = x.cvEntries_p->begin(); itx != x.cvEntries_p->end() && itv != v.cvEntries_p->end(); ++itx, ++itv) *itx += t** itv;
        }
      return;
    }
    where("addScaledVector(VectorEntry, VectorEntry, real)");
    error("entry_inconsistent_structures");
}

//special accumulation x+=t*v (t complex)
void addScaledVector(VectorEntry& x, VectorEntry& v, const complex_t& t)
{
  if(x.valueType_==_real) x.toComplex();
  ValueType vtv=v.valueType_;
  StrucType stx=x.strucType_, stv=v.strucType_;
  if(stx==_scalar && stv==_scalar)
    {
      if(vtv==_real)
        {
          Vector<complex_t>::iterator itx;
          Vector<real_t>::iterator itv=v.rEntries_p->begin();
           for(itx = x.cEntries_p->begin(); itx != x.cEntries_p->end() && itv != v.rEntries_p->end(); ++itx, ++itv) *itx += t** itv;
         }
      else //vtv = _complex
        {
          Vector<complex_t>::iterator itx, itv=v.cEntries_p->begin();
          for(itx = x.cEntries_p->begin(); itx != x.cEntries_p->end() && itv != v.cEntries_p->end(); ++itx, ++itv) *itx += t** itv;
        }
      return;
    }
   if(stx==_vector && stv==_vector && x.nbOfComponents==v.nbOfComponents)
    {
      if(vtv==_real)
        {
              Vector<Vector<complex_t> >::iterator itx;
              Vector<Vector<real_t> >::iterator itv=v.rvEntries_p->begin();
              for(itx = x.cvEntries_p->begin(); itx != x.cvEntries_p->end() && itv != v.rvEntries_p->end(); ++itx, ++itv) *itx += t** itv;
            }
      else //vtv = _complex
        {
          Vector<Vector<complex_t> >::iterator itx, itv=v.cvEntries_p->begin();
          for(itx = x.cvEntries_p->begin(); itx != x.cvEntries_p->end() && itv != v.cvEntries_p->end(); ++itx, ++itv) *itx += t** itv;
        }
      return;
    }
    where("addScaledVector(VectorEntry, VectorEntry, complex)");
    error("entry_inconsistent_structures");
}


real_t VectorEntry::norm2() const           //! intern norm2
{return std::sqrt(real(hermitianProduct(*this, *this)));}

//@{
// specialization (no check of null pointer, do it before!)
template<>
Vector<real_t>* VectorEntry::entriesp<real_t>() const  {return rEntries_p;}
template<>
Vector<complex_t>* VectorEntry::entriesp<complex_t>() const  {return cEntries_p;}
template<>
Vector<Vector<real_t> >* VectorEntry::entriesp<Vector<real_t> >() const  {return rvEntries_p;}
template<>
Vector<Vector<complex_t> >* VectorEntry::entriesp<Vector<complex_t> >() const  {return cvEntries_p;}
//@}

} //end of namespace xlifepp
