/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Traits.hpp
  \author Manh Ha NGUYEN
  \since 22 Jan 2013
  \date  7 Mars 2013

  \brief Trait of different types.
*/

#ifndef EIGEN_TRAITS_HPP
#define EIGEN_TRAITS_HPP

#include "config.h"
#include "utils.h"

namespace xlifepp
{

typedef std::vector<number_t> Indexing;
typedef int_t Index;

struct TrueType {  enum { value = 1 }; };
struct FalseType { enum { value = 0 }; };

template<bool Condition, typename Then, typename Else>
struct Conditional { typedef Then type; };

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<typename Then, typename Else>
struct Conditional<false, Then, Else> { typedef Else type; };

#endif

template<typename T> struct RemoveReference { typedef T type; };
#ifndef DOXYGEN_SHOULD_SKIP_THIS
template<typename T> struct RemoveReference<T&> { typedef T type; };
#endif
template<typename T> struct RemovePointer { typedef T type; };
#ifndef DOXYGEN_SHOULD_SKIP_THIS
template<typename T> struct RemovePointer<T*> { typedef T type; };
template<typename T> struct RemovePointer<T*const> { typedef T type; };
#endif

template <class T> struct RemoveConst { typedef T type; };
#ifndef DOXYGEN_SHOULD_SKIP_THIS
template <class T> struct RemoveConst<const T> { typedef T type; };
template <class T> struct RemoveConst<const T[]> { typedef T type[]; };
template <class T, unsigned int Size> struct RemoveConst<const T[Size]> { typedef T type[Size]; };
#endif

template<typename T> struct RemoveAll { typedef T type; };
#ifndef DOXYGEN_SHOULD_SKIP_THIS
template<typename T> struct RemoveAll<const T>   { typedef typename RemoveAll<T>::type type; };
template<typename T> struct RemoveAll<T const&>  { typedef typename RemoveAll<T>::type type; };
template<typename T> struct RemoveAll<T&>        { typedef typename RemoveAll<T>::type type; };
template<typename T> struct RemoveAll<T const*>  { typedef typename RemoveAll<T>::type type; };
template<typename T> struct RemoveAll<T*>        { typedef typename RemoveAll<T>::type type; };
#endif

/*!
  \struct NumTraits
  Trait struct to determine the value type
*/
template<typename K>
struct NumTraits {
    typedef K Type;
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<>
struct NumTraits<real_t>
{
  enum
  {
    IsComplex = 0
  };
  typedef real_t RealScalar;
  typedef complex_t ComplexScalar;
  typedef real_t Scalar;
  typedef real_t magnitudeType;

  static inline bool isReal() { return true;}
  static inline bool isComplex() { return false;}
  static inline real_t zero() { return real_t(0.0);}
  static inline real_t one() { return real_t(1.0);}
  static inline real_t imag(const real_t& v) { return v;}
  static inline real_t real(const real_t& v) { return v;}
  static inline real_t realPart(const real_t& v) { return v;}
  static inline real_t imagPart(const real_t& v) { return real_t(0.0);}
  static inline real_t value(const real_t& v) { return v;}
  static inline real_t abs2(const real_t& v) { return v * v;}
  static inline real_t lapy2(const real_t& x, const real_t& y) { return std::sqrt(x*x + y*y);}
  static inline real_t epsilon() { return std::numeric_limits<real_t>::epsilon(); }
  static inline real_t random() {real_t rnd = (real_t) std::rand() / RAND_MAX; return (real_t)(-1.0 + 2.0 * rnd);}
  static inline real_t base() { return std::numeric_limits<real_t>::radix; }
  static inline real_t prec() { return epsilon()*base(); }
  static inline real_t magnitude(real_t a) { return std::fabs(a); }
  static inline real_t pow(real_t a, real_t b) { return std::pow(a,b); }
  static inline real_t squareroot(real_t a) { return std::sqrt(a); }
  static inline real_t sfmin() {
    return std::numeric_limits<real_t>::min();
  }
  static inline real_t nan() { return +(real_t)(0.0)/theZeroThreshold; }
  static inline real_t conjugate(real_t a) { return a;}
  static inline real_t compare2epsilon(real_t a)
  {
      real_t tmp = magnitude(a) > NumTraits<real_t>::epsilon() ? a: NumTraits<real_t>::zero();
      return tmp;
  }
  static inline real_t norm1(real_t a) { return std::fabs(a);}
};

template<>
struct NumTraits<complex_t>
{
  enum
  {
    IsComplex = 1
  };
  typedef real_t RealScalar;
  typedef complex_t ComplexScalar;
  typedef complex_t Scalar;
  typedef real_t magnitudeType;

  static inline bool isReal() {return false;}
  static inline bool isComplex() { return true;}
  static inline complex_t zero() { return complex_t(0.0, 0.0);}
  static inline complex_t one() { return complex_t(1.0, 0.0);}
  static inline real_t real(const complex_t& v) { return v.real();}
  static inline real_t imag(const complex_t& v) { return v.imag();}
  static inline real_t realPart(const complex_t& v) { return v.real();}
  static inline real_t imagPart(const complex_t& v) { return v.imag();}
  static inline complex_t value(const complex_t& v) { return v;}
  static inline real_t abs2(const complex_t& v) { return v.real() * v.real() + v.imag() * v.imag();}
  static inline real_t epsilon() { return std::numeric_limits<real_t>::epsilon(); }
  static inline complex_t random() {
      const real_t rnd1 = NumTraits<real_t>::random();
      const real_t rnd2 = NumTraits<real_t>::random();
      return complex_t(rnd1,rnd2);
  }
  static inline real_t magnitude(complex_t a) { return std::abs(a); }
  static inline complex_t squareroot(complex_t x)
  {
      typedef NumTraits<real_t>  STMT;
      const real_t r  = x.real(), i = x.imag(), zero = STMT::zero(), two = 2.0;
      const real_t a  = STMT::squareroot((r*r)+(i*i));
      const real_t nr = STMT::squareroot((a+r)/two);
      const real_t ni = ( i == zero ? zero: STMT::squareroot((a-r)/two) );
      return complex_t(nr,ni);
  }
  static inline real_t sfmin() {
    return std::numeric_limits<real_t>::min();
  }
  static inline complex_t conjugate(complex_t a) { return complex_t(a.real(), -a.imag()); }
  static inline complex_t compare2epsilon(complex_t a)
  {
      real_t realPart = NumTraits<real_t>::magnitude(a.real()) > NumTraits<real_t>::epsilon() ? a.real() : NumTraits<real_t>::zero();
      real_t imagPart = NumTraits<real_t>::magnitude(a.imag()) > NumTraits<real_t>::epsilon() ? a.imag() : NumTraits<real_t>::zero();
      return complex_t(realPart, imagPart);
  }
  static inline real_t norm1(complex_t a) { return (std::abs(a.real())+std::abs(a.imag()));}
};

#endif

/*!
  \struct VectorTrait
  Trait struct to determine the value type for vector f values
*/
template<typename Vec>
struct VectorTrait {
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<>
struct VectorTrait<std::vector<real_t> > {
    typedef real_t Type;

};

template<>
struct VectorTrait<std::vector<complex_t> > {
    typedef complex_t Type;
};

template<>
struct VectorTrait<Vector<real_t> > {
    typedef real_t Type;

};

template<>
struct VectorTrait<Vector<complex_t> > {
    typedef complex_t Type;
};

#endif

/*!
  \struct IterationVectorTrait
  Trait struct to determine (scalar) type of Iterator!
*/
template<typename K>
struct IterationVectorTrait {
    typedef K ScalarType;
    typedef K Type;
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

//scalar iterator specialization
template<>
struct IterationVectorTrait<std::vector<real_t>::iterator> {
    typedef real_t ScalarType;
    typedef real_t Type;
};

template<>
struct IterationVectorTrait<std::vector<complex_t>::iterator> {
    typedef complex_t ScalarType;
    typedef complex_t Type;
};

template<>
struct IterationVectorTrait<std::vector<real_t>::const_iterator> {
    typedef real_t ScalarType;
    typedef real_t Type;
};

template<>
struct IterationVectorTrait<std::vector<complex_t>::const_iterator> {
    typedef complex_t ScalarType;
    typedef complex_t Type;
};

// scalar pointer specialization
template<>
struct IterationVectorTrait<real_t*> {
    typedef real_t ScalarType;
    typedef real_t Type;
};

template<>
struct IterationVectorTrait<complex_t*> {
    typedef complex_t ScalarType;
    typedef complex_t Type;
};

template<>
struct IterationVectorTrait<const real_t*> {
    typedef real_t ScalarType;
    typedef real_t Type;
};

template<>
struct IterationVectorTrait<const complex_t*> {
    typedef complex_t ScalarType;
    typedef complex_t Type;
};

//vector iterator specialization
template<>
struct IterationVectorTrait<std::vector<Vector<real_t> >::iterator> {
    typedef real_t ScalarType;
    typedef Vector<real_t> Type;
};

template<>
struct IterationVectorTrait<std::vector<Vector<complex_t> >::iterator> {
    typedef complex_t ScalarType;
    typedef Vector<complex_t> Type;
};

template<>
struct IterationVectorTrait<std::vector<Vector<real_t> >::const_iterator> {
    typedef real_t ScalarType;
    typedef Vector<real_t> Type;
};

template<>
struct IterationVectorTrait<std::vector<Vector<complex_t> >::const_iterator> {
    typedef complex_t ScalarType;
    typedef Vector<complex_t> Type;
};

#endif

/*!
  \struct Int2Type
  A trick to overcome problem of static dispatch
*/
template <int v>
struct Int2Type
{
  enum { value = v };
};

/*!
  \struct SolverChoice
  Helper struct to make choice
*/
template<bool, bool>
struct SolverChoice
{
  enum { choice = 0};
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<>
struct SolverChoice<false, true>
{
    enum { choice = 1 };
};

template<>
struct SolverChoice<true, false>
{
    enum { choice = 2 };
};

template<>
struct SolverChoice<true, true>
{
    enum { choice = 3 };
};

#endif

/*!
  \struct NormImplement
  Trait struct to determine value of a norm
*/
template<typename K>
struct NormImplement {};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<>
struct NormImplement<real_t>
{
  static inline real_t run(real_t x) { return std::abs(x); }
};

template<>
struct NormImplement<complex_t>
{
  static inline real_t run(complex_t x) { return (std::abs(x.real()) + std::abs(x.imag())); }
};

template<typename Scalar>
typename NumTraits<Scalar>::RealScalar norm1(const Scalar x)
{
  return NormImplement<Scalar>::run(x);
}

#endif

/*!
  \class NonCopyable
  A base class to disable default copy ctor and copy assignement operator.
*/
class NonCopyable
{
  NonCopyable(const NonCopyable&); //!< copy constructor forbidden
  const NonCopyable& operator=(const NonCopyable&); //!< assignment operator forbidden
protected:
  NonCopyable() {} //!< constructor
  ~NonCopyable() {} //!< destructor
};

}

#endif //TRAITS_HPP
