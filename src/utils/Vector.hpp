/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Vector.hpp
  \author D. Martin, E. Lunéville
  \since 07 jul 2007
  \date 08 feb 2016

  \brief Definition of the xlifepp::Vector class

  general template class for (small) real or complex vectors
  Standard operations are inherited for stl class vector or implemented
  with autocast real to complex (never complex to real)
  may be used for vector of vectors and so on, but restricted using
 */

#ifndef VECTOR_HPP
#define VECTOR_HPP

#include "config.h"
#include "Environment.hpp"
#include "String.hpp"
#include "Algorithms.hpp"
#include "Messages.hpp"
#include "Point.hpp"
#include "printUtils.hpp"

#include <iostream>
#include <type_traits>

namespace xlifepp
{
 template<typename T>
 inline const std::vector<T>& tran(const std::vector<T>& v)
 {return v;}

 template<typename T>
 inline std::vector<T> conj(const std::vector<T>& v)
 {
    //return conj(Vector<T>(v));
    std::vector<T> cv(v);
    typename std::vector<T>::iterator it=cv.begin();
    for(; it != cv.end(); it++) *it = conj(*it);
    return cv;
 }

/*!
  \class Vector
  to deal with numeric vector (say real or complex vector).
  may be used for vector of vectors but restricted usage
 */
template<typename K>
class Vector : public std::vector<K>
{
  public:
    //@{
    //! useful typedefs for Vector class
    typedef K type_t;
    typedef typename std::vector<K>::iterator it_vk;
    typedef typename std::vector<K>::const_iterator cit_vk;
    //@}

    // constructors
    Vector()                                //! default constructor
      { std::vector<K>::resize(1); }

    explicit Vector(number_t l)             //! constructor by length
      { std::vector<K>::resize(l);}

    explicit Vector(int l)                  //! constructor by length
      { std::vector<K>::resize(l);}

    explicit Vector(number_t l, const K& v) //! constructor by length for constant vector
      { std::vector<K>::resize(l, v);}

    explicit Vector(int l, const K& v)      //! constructor by length for constant vector
      { std::vector<K>::resize(l, v);}

    Vector(cit_vk b, cit_vk e)              //! constructor by iterator
      { std::vector<K>::assign(b, e);}

    Vector(const std::vector<K>& v)         //! constructor by a std::vector
      : std::vector<K>(v) { }

    template<typename KK>
    explicit Vector(const std::vector<KK>& v) //!<  vector<K>(vector<KK>)
    {
      //forbiden assignment except specialization (see Vector.cpp)
      error("forbidden", words("copy constructor"));
    }

    template<typename KK>
    explicit Vector(const std::vector<Vector<KK> >& v) //!<  vector<K>(std::vector<vector<KK>>)
    {
      //forbiden assignment except specialization (see Vector.cpp)
      error("forbidden", words("copy constructor"));
    }

    Vector(const std::initializer_list<K>& v) //! constructor by an initializer list
      : std::vector<K>(v) { }

    //@{
    //! specific constructor from n items
    Vector(const K& r1)
      : Vector<K>{r1} {}
    Vector(const K& r1, const K& r2)
      : Vector<K>{r1,r2} {}
    Vector(const K& r1, const K& r2, const K& r3)
      : Vector<K>{r1,r2,r3} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4)
      : Vector<K>{r1,r2,r3,r4} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5)
      : Vector<K>{r1,r2,r3,r4,r5} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6)
      : Vector<K>{r1,r2,r3,r4,r5,r6} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9, const K& r10)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9, const K& r10, const K& r11)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9, const K& r10, const K& r11, const K& r12)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9, const K& r10, const K& r11, const K& r12, const K& r13)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9, const K& r10, const K& r11, const K& r12, const K& r13, const K& r14)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9, const K& r10, const K& r11, const K& r12, const K& r13, const K& r14,
           const K& r15)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9, const K& r10, const K& r11, const K& r12, const K& r13, const K& r14,
           const K& r15, const K& r16)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9, const K& r10, const K& r11, const K& r12, const K& r13, const K& r14,
           const K& r15, const K& r16, const K& r17)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9, const K& r10, const K& r11, const K& r12, const K& r13, const K& r14,
           const K& r15, const K& r16, const K& r17, const K& r18)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9, const K& r10, const K& r11, const K& r12, const K& r13, const K& r14,
           const K& r15, const K& r16, const K& r17, const K& r18, const K& r19)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18,r19} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9, const K& r10, const K& r11, const K& r12, const K& r13, const K& r14,
           const K& r15, const K& r16, const K& r17, const K& r18, const K& r19, const K& r20)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18,r19,r20} {}
    Vector(const K& r1, const K& r2, const K& r3, const K& r4, const K& r5, const K& r6, const K& r7,
           const K& r8, const K& r9, const K& r10, const K& r11, const K& r12, const K& r13, const K& r14,
           const K& r15, const K& r16, const K& r17, const K& r18, const K& r19, const K& r20,const K& r21)
      : Vector<K>{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18,r19,r20,r21} {}
    //@}

  private:
    void assign(const K& v) //! assign const scalar value to all vector entries
      { for(it_vk it = this->begin(); it < this->end(); it++) *it = v; }

  public:
    //-------------------------------------------------------------
    // accessors
    //-------------------------------------------------------------
    size_t size() const //! overloaded size
      { return std::vector<K>::size();}

    std::pair<number_t, number_t> dims() const //! dimension pair
      { return std::pair<number_t, number_t>(std::vector<K>::size(),1);}

    it_vk begin() //! overloaded iterator begin()
      { return std::vector<K>::begin();}

    cit_vk begin() const //! overloaded const iterator begin()
      { return std::vector<K>::begin(); }

    it_vk end() //! overloaded iterator end()
      { return std::vector<K>::end();    }

    cit_vk end() const //! overloaded const iterator end()
      { return std::vector<K>::end();}

    K operator()(number_t i) const //! i-th component (i=1..n) (r)
      { return (*this)[i - 1];}

    K& operator()(number_t i) //! i-th component (i=1..n) (r/w)
      { return (*this)[i - 1];}

    //--------------------------------------------------------------------------------
    // generalized access (indices start from 1)
    //--------------------------------------------------------------------------------
    //! return i1->i2 components as vector
    Vector<K> get(number_t i1, number_t i2) const
    {
      if (i1==0 || i1>size())
      {
        where("Vector<K>::get(Number,Number)");
        error("index_out_of_range", "Vector<K>", 1, size());
      }
      if (i2==0 || i2>size())
      {
        where("Vector<K>::get(Number,Number)");
        error("index_out_of_range", "Vector<K>", 1, size());
      }
      if (i2<i1)
      {
        where("Vector<K>::get(Number,Number)");
        error("is_greater", i1, i2);
      }
      Vector<K> vec(i2-i1+1);
      typename Vector<K>::iterator itv=vec.begin();
      typename Vector<K>::const_iterator itcv=begin()+i1-1;
      for(number_t i=i1; i<=i2; ++i, ++itv, ++itcv) *itv = *itcv;
      return vec;
    }

    //! return 'is' components as vector
    Vector<K> get(const std::vector<number_t>& is) const
    {
      if (is.size()==0)
      {
        where("Vector<K>::get(Numbers)");
        error("is_void","is");
      }
      number_t mx = *max_element(is.begin(), is.end());
      if (mx > size())
      {
        where("Vector<K>::get(Numbers)");
        error("index_out_of_range","is", 1, size());
      }
      mx = *min_element(is.begin(), is.end());
      if (mx == 0)
      {
        where("Vector<K>::get(Numbers)");
        error("index_out_of_range","is", 1, size());
      }
      Vector<K> vec(is.size());
      std::vector<number_t>::const_iterator it=is.begin();
      typename Vector<K>::iterator itv=vec.begin();
      for (;it!=is.end(); ++it, ++itv) *itv= *(begin() + *it - 1);
      return vec;
    }

    //! set i1->i2 components of current vector
    void set(number_t i1, number_t i2, const std::vector<K>& vec)
    {
      if (i1==0 || i1>size())
      {
        where("Vector<K>::set(Number,Number,Vector<K>");
        error("index_out_of_range", "Vector<K>", 1, size());
      }
      if (i2==0 || i2>size())
      {
        where("Vector<K>::set(Number,Number,Vector<K>");
        error("index_out_of_range", "Vector<K>", 1, size());
      }
      if (i2<i1)
      {
        where("Vector<K>::set(Number,Number,Vector<K>");
        error("is_greater", i1, i2);
      }
      typename Vector<K>::const_iterator itv=vec.begin();
      typename Vector<K>::iterator itcv=begin()+i1-1;
      for (number_t i=i1; i<=i2 && itv!=vec.end(); ++i, ++itv, ++itcv) *itcv = *itv;
    }

    //! set 'is' components of current vector
    void set(const std::vector<number_t>& is, const std::vector<K>& vec)
    {
      if (is.size()==0)
        {
          where("Vector<K>::set(Numbers, Vector<K>)");
          error("is_void","is");
        }
      number_t mx = *max_element(is.begin(), is.end());
      if (mx > size())
      {
        where("Vector<K>::set(Numbers, Vector<K>)");
        error("index_out_of_range","is", 1, size());
      }
      mx = *min_element(is.begin(), is.end());
      if (mx ==0 )
      {
        where("Vector<K>::set(Numbers, Vector<K>)");
        error("index_out_of_range","is", 1, size());
      }
      std::vector<number_t>::const_iterator it=is.begin();
      typename Vector<K>::iterator itv=vec.begin();
      for (;it!=is.end()&& itv!=vec.end(); ++it, ++itv) *(begin() + *it - 1) = *itv ;
    }

    //! return 'is' components as vector
    Vector<K> operator()(const std::vector<number_t>& is) const
    {
      return get(is);
    }
    //! return i1->i2 components as vector
    Vector<K> operator()(number_t i1, number_t i2) const
    {
      return get(i1,i2);
    }

    //--------------------------------------------------------------------------------
    // member operators
    //--------------------------------------------------------------------------------
    // assignment operator =
    Vector<K>& operator=(const K& x) //!  assign const scalar value to all vector entries
    {
      assign(x);
      return *this;
    }

    Vector<K>& operator=(const std::vector<K>& x) //!  assign std::vector<K>
    {
      this->resize(x.size());
      typename std::vector<K>::const_iterator itx = x.begin();
      for(it_vk it = this->begin(); it != this->end(); it++, itx++) *it = *itx;
      return *this;
    }

    Vector<K>& operator=(const std::initializer_list<K>& x) //!  assign brace enclosed list
    {
      this->resize(x.size());
      auto itx = x.begin();
      for(auto it = this->begin(); it != this->end(); it++, itx++) *it = *itx;
      return *this;
    }

    template<typename KK>
    Vector<K>& operator=(const Vector<KK>& x)  //!  vector<K>=vector<KK>
    {
      //forbiden assignment except specialization (see below)
      error("forbidden", words("assignment operator"));
      return *this;
    }

    // algebraic operations
    Vector<K>& operator+=(const K& x) //! add a scalar to current vector
    {
      for(it_vk it = begin(); it != end(); it++) *it += x;
      return *this;
    }

    Vector<K>& operator-=(const K& x) //! substract a scalar to current vector
    {
      for(it_vk it = begin(); it != end(); it++) *it -= x;
      return *this;
    }

    template <typename KK> // specialization Complex to Vector<Real> is forbidden (see specialization)
    Vector<K>& operator*=(const KK& x) //! multiply current vector by a scalar
    {
      for(it_vk it = begin(); it != end(); it++) *it *= x;
      return *this;
    }

    template <typename KK> // specialization Complex to Vector<Real> is forbidden (see specialization)
    Vector<K>& operator/=(const KK& x) //! divide current vector by a scalar
    {
      if(std::abs(x) < theEpsilon) {divideByZero("A/=x");}
      for(it_vk it = begin(); it != end(); it++) *it /= x;
      return *this;
    }

    Vector<K>& operator+=(const Vector<K>& b) //! add a vector to current vector
    {
      if(size() != b.size()) mismatchSize("+=", b.size());
      cit_vk it_b = b.begin();
      for(it_vk it = begin(); it != end(); it++, it_b++) *it += *it_b;
      return *this;
    }

    template<typename KK> //forbiden assignment except specialization (see below)
    Vector<K>& operator+=(const Vector<KK>& x)  //!  vector<K>=vector<KK>
    {
      error("forbidden", words("assignment operator"));
      return *this;
    }


    Vector<K>& operator-=(const Vector<K>& b) //! substract a vector to current vector
    {
      if(size() != b.size()) mismatchSize("-=", b.size());
      cit_vk it_b = b.begin();
      for(it_vk it = begin(); it != end(); it++, it_b++) *it -= *it_b;
      return *this;
    }

    template<typename KK> //forbiden assignment except specialization (see below)
    Vector<K>& operator-=(const Vector<KK>& x)  //!  vector<K>-=vector<KK>
    {
      error("forbidden", words("assignment operator"));
      return *this;
    }

    template<typename KK> //forbiden operation (declare here to override some template compilation problems)
    Vector<K>& operator*=(const Vector<KK>& x)  //!  vector<K>*=vector<KK>
    {
      error("forbidden", "vector*=vector");
      return *this;
    }

    template<typename KK> //forbiden operation (declare here to override some template compilation problems)
    Vector<K>& operator/=(const Vector<KK>& x)  //!  vector<K>*=vector<KK>
    {
      error("forbidden", "vector/=vector");
      return *this;
    }

    void deleteRows(number_t r1i, number_t r2i)  //! delete rows r1,..., r2
    {
      number_t nbr=size();
      number_t r1=std::min(std::max(r1i,number_t(1)),nbr);  //to prevent oversized indices
      number_t r2=std::min(std::max(r2i,number_t(1)),nbr);
      if(r2 < r1) return;
      it_vk it1=std::vector<K>::begin()+r1-1, it2=std::vector<K>::begin()+r2;
      std::vector<K>::erase(it1,it2);
    }

    number_t nbzero(real_t tol = 0) const  //! return number of 0 components (|xi|<=tol)
    {
      number_t n=0;
      for(cit_vk it = begin(); it != end(); it++)
        if(norm(*it) < tol) n++;
      return n;
    }

    //------------------------------------------------------------------------------
    // get value type and structure type of matrix using template specialization
    // only main types are handled real/complex value and scalar/matrix/vector structure
    //------------------------------------------------------------------------------

    ValueType valueType() const //! get value type of matrix using template specialization
      { return _real; }

    StrucType strucType() const //! get structure type of matrix using template specialization
      { return _scalar; }

    //! return size of element of a matrix of matrices or a matrix of vectors (size of first block!)
    number_t elementSize() const
    {
      return 1;  //default is for scalar vector
    }

    //--------------------------------------------------------------------------------
    //norm of vector
    //--------------------------------------------------------------------------------------------
    real_t norminfty() const //! sup norm
    {
      return std::abs(*maxElementTpl(begin(), end()));
    }

    real_t norm2squared() const //! squared quadratic norm
    {
      return hermitianInnerProductTpl(begin(), end(), begin()).real();
    }

    real_t norm2() const //! quadratic norm
    {
      return std::sqrt(norm2squared());
    }

    real_t norm1() const //! l1 norm
    {
      real_t r = 0.;
      for(cit_vk it = begin(); it != end(); it++) { r += std::abs(*it); }
      return r;
    }

    Vector<K>& toConj()  //! to its conjugate (assuming conj(real)->real)
    {
      for(it_vk it = begin(); it != end(); it++) *it = conj(*it);
      return *this;
    }

    Vector<K>& roundToZero(real_t aszero=std::sqrt(theEpsilon)) //! round to 0 all coefficients smaller (in norm) than a aszero, storage is not modified
    {
      (*this)=xlifepp::roundToZero(*this, aszero);
      return (*this);
    }

    Vector<K>& round(real_t prec=0.) //! round with given precision
    {
      if(prec>0) (*this)=xlifepp::round(*this, prec);
      return (*this);
    }

    Vector<K>& normalize(number_t no=2) //! normalize current vector in l1=1, l2=2 or linf=0
    {
       real_t n;
       switch(no)
       {
           case 0: n=norminfty(); break;
           case 1: n=norm1(); break;
           default: n=norm2();
       }
       for(it_vk it = begin(); it != end(); it++)  *it/=n;
       return *this;
    }

    //--------------------------------------------------------------------------------
    // warning and error handlers
    //--------------------------------------------------------------------------------
    void mismatchSize(const string_t& s, const size_t sz) const //! error: in vector operation s dimensions disagree dim(op1)= ... dim(op2)=...
    {
      where(s);
      error("vec_mismatch_dims", size(), sz);
    }

    void divideByZero(const string_t& s) const //! error: divide by 0 in vector operation s vector dimension dim(op1)
    {
      where(s);
      error("vec/0", size());
    }

    void complexCastError(const string_t& s, const number_t sz) const //! error: try to cast a complex to a real
    {
      where(s);
      error("vec_cast", size(), sz);
    }

    //--------------------------------------------------------------------------------
    // I/O utilities
    //--------------------------------------------------------------------------------
    void print(std::ostream& os) const //! write to a output stream
    {
      os << *this;
    }

    void print(PrintStream& os) const {print(os.currentStream());}


    void print() const //! write to default file stream
    {
      thePrintStream << *this;
    }

    void printRaw(std::ostream& os) const //! print in a raw format to stream
    {
        for(cit_vk it = begin(); it != end(); it++) { it->printRaw(os); }
    }
    void printRaw(PrintStream& os) const {printRaw(os.currentStream());}

}; // end of class Vector


//--------------------------------------------------------------------------------
// member function specializations
//--------------------------------------------------------------------------------
template<> template<>
Vector<complex_t>::Vector(const std::vector<real_t>&);  //!< specialization vector<complex>(vector<real>)

template<> template<>
Vector<real_t>::Vector(const std::vector<complex_t>&);  //!< specialization vector<real>(vector<complex>)

template<> template<>
Vector<Vector<complex_t> >::Vector(const std::vector<Vector<real_t> >&); //!< specialization vector<vector<complex>>=vector<vector<real>>

template<> template<>
Vector<Vector<real_t> >::Vector(const std::vector<Vector<complex_t> >&); //!< specialization vector<vector<real>>=vector<vector<complex>>

template<> template<>
Vector<complex_t>& Vector<complex_t>::operator=(const Vector<real_t>&);  //!< specialization vector<complex>=vector<real>

template<> template<>
Vector<real_t>& Vector<real_t>::operator=(const Vector<complex_t>&);  //!< specialization vector<real>=vector<complex>

template<> template<>
Vector<Vector<complex_t> >& Vector<Vector<complex_t> >::operator=(const Vector<Vector<real_t> >&); //!< specialization vector<vector<complex>>=vector<vector<real>>

template<> template<>
Vector<Vector<real_t> >& Vector<Vector<real_t> >::operator=(const Vector<Vector<complex_t> >&); //!< specialization vector<vector<real>>=vector<vector<complex>>

template<> inline
void Vector<real_t>::printRaw(std::ostream& os) const //! print in a raw format (1 column)
{
	Vector<real_t>::const_iterator it;
  for (it = begin(); it != end(); it++) { os << *it  << "\n"; }
}
template<> inline
void Vector<complex_t>::printRaw(std::ostream& os) const //! print in a raw format (2 columns)
{
	Vector<complex_t>::const_iterator it;
  for (it = begin(); it != end(); it++) { os << it->real() << " " << it->imag() << "\n"; }
}

template<> inline
void Vector<Vector<real_t> >::printRaw(std::ostream& os) const //! print in a raw format (N columns)
{
	Vector<Vector<real_t> >::const_iterator itv;
  Vector<real_t>::const_iterator it;
	for (itv = begin(); itv != end(); ++itv)
  {
     bool addSpace=false;
     for (it=itv->begin(); it != itv->end(); ++it)
     {
       if (addSpace) { os << " "; }
       else { addSpace=true; }
       os << *it;
     }
    os << "\n";
  }
}
template<> inline
void Vector<Vector<complex_t> >::printRaw(std::ostream& os) const //! print in a raw format (2N columns)
{
	Vector<Vector<complex_t> >::const_iterator itv;
  Vector<complex_t>::const_iterator it;
   for (itv = begin(); itv != end(); ++itv)
   {
     bool addSpace=false;
     for (it=itv->begin(); it != itv->end(); ++it)
     {
      if (addSpace) { os << " "; }
      else { addSpace=true; }
      os << it->real() << " " << it->imag();
     }
     os << "\n";
   }
}

template<> template<>
Vector<complex_t>& Vector<complex_t>::operator=(const Vector<real_t>&);  //!< specialization vector<complex>=vector<real>
template<> template<>
Vector<complex_t>& Vector<complex_t>::operator+=(const Vector<real_t>&); //!< specialization vector<complex>+=vector<real>
template<> template<>
Vector<complex_t>& Vector<complex_t>::operator-=(const Vector<real_t>&); //!< specialization vector<complex>-=vector<real>
template<> template<>
Vector<Vector<complex_t> >& Vector<Vector<complex_t> >::operator+=(const Vector<Vector<real_t> >&); //!< specialization vector<vector<complex>>+=vector<vector<real>>
template<> template<>
Vector<Vector<complex_t> >& Vector<Vector<complex_t> >::operator-=(const Vector<Vector<real_t> >&);  //!< specialization vector<vector<complex>>-=vector<vector<real>>

template<> template<>
Vector<real_t>& Vector<real_t>::operator*=(const complex_t& vc); //!< specialization vector<real>*=complex
template<> template<>
Vector<real_t>& Vector<real_t>::operator/=(const complex_t& vc); //!< specialization vector<real>/=complex
//--------------------------------------------------------------------------------
// I/O utilities
//--------------------------------------------------------------------------------
template<typename K>
std::ostream& operator<<(std::ostream& os, const Vector<K>& v) //! general vector flux insertion (write)
{
  number_t n=v.size();
  if(n==0) {os<<"[]"; return os;}
  typename Vector<K>::cit_vk it=v.begin();
  os << "[";
  for(number_t i=0;i<n-1; i++, it++)
    { os << *it << " ";}
  os << *it;
  os << "]";
  return os;
}

//-------------------------------------------------------------------------------
// unary operators: + (neutral) and - (opposite vector)
//-------------------------------------------------------------------------------
template<typename K>
Vector<K> operator+(const Vector<K>& a) //! unary + (return same vector)
{
  return Vector<K>(a);
}

template<typename K>
Vector<K> operator-(const Vector<K>& a) //! unary - (return opposite vector)
{
  Vector<K> r(a);
  typename Vector<K>::it_vk it;

  for(it = r.begin(); it != r.end(); it++)
    {
      *it = - *it;
    }

  return r;
}

//-------------------------------------------------------------------------------
// vector addition
//-------------------------------------------------------------------------------
template<typename K>
Vector<K> operator+(const Vector<K>& a, const Vector<K>& b) //! add two vectors
{
  Vector<K> r(a);

  if(a.size() != b.size())
    {
      a.mismatchSize("Vector<K>+Vector<K>", b.size());
    }

  typename Vector<K>::cit_vk it_b = b.begin();
  typename Vector<K>::it_vk it;

  for(it = r.begin(); it != r.end(); it++, it_b++)
    {
      *it += *it_b;
    }

  return r;
}
//specialization
Vector<complex_t> operator+(const Vector<real_t>&, const Vector<complex_t>&); //!< vector addition " real A + complex B"
Vector<complex_t> operator+(const Vector<complex_t>&, const Vector<real_t>&); //!< vector addition " complex A + real B"

//-------------------------------------------------------------------------------
// scalar addition to vector
//-------------------------------------------------------------------------------
template<typename K>
Vector<K> operator+(const K& x, const Vector<K>& a) //! add a scalar to a vect or "x+A"
{
  Vector<K> r(a);
  typename Vector<K>::it_vk it;

  for(it = r.begin(); it != r.end(); it++)
    {
      *it += x;
    }

  return r;
}
template<typename K>
Vector<K> operator+(const Vector<K>& a, const K& x) //! add a scalar to a vector "A+x"
{
  return x + a;
}
//specialization real->complex
Vector<complex_t> operator+(const Vector<real_t>&, const complex_t&); //!< add complex scalar to real vector
Vector<complex_t> operator+(const complex_t&, const Vector<real_t>&); //!< add complex scalar to real vector
Vector<complex_t> operator+(const Vector<complex_t>&, const real_t&); //!< add real scalar to complex vector
Vector<complex_t> operator+(const real_t&, const Vector<complex_t>&); //!< add real scalar to complex vector

//-------------------------------------------------------------------------------
// vector subtraction
//-------------------------------------------------------------------------------
template<typename K>
Vector<K> operator-(const Vector<K>& a, const Vector<K>& b) //! vector subtraction "A-B"
{
  Vector<K> r(a);

  if(a.size() != b.size())
    {
      a.mismatchSize("Vector<K>-Vector<K>", b.size());
    }

  typename Vector<K>::cit_vk it_b = b.begin();
  typename Vector<K>::it_vk it;

  for(it = r.begin(); it != r.end(); it++, it_b++)
    {
      *it -= *it_b;
    }

  return r;
}
//specialization real->complex
Vector<complex_t> operator-(const Vector<real_t>&, const Vector<complex_t>&); //!< vector subtraction "real A - complex B"
Vector<complex_t> operator-(const Vector<complex_t>&, const Vector<real_t>&); //!< vector subtraction "complex A - real B"

//-------------------------------------------------------------------------------
// subtract scalar from vector
//-------------------------------------------------------------------------------
template<typename K>
Vector<K> operator-(const Vector<K>& a, const K& x) //! subtract vector from scalar "A-x"
{
  Vector<K> r(a);
  typename Vector<K>::it_vk it;

  for(it = r.begin(); it != r.end(); it++)
    {
      *it -= x;
    }

  return r;
}
template<typename K>
Vector<K> operator-(const K& x, const Vector<K>& a) //! subtract vector from scalar "x-A"
{
  Vector<K> r(a);
  typename Vector<K>::it_vk it;

  for(it = r.begin(); it != r.end(); it++)
    {
      *it = x - (*it);
    }

  return r;

}
//specialization real->complex
Vector<complex_t> operator-(const Vector<real_t>&, const complex_t&); //!< subtract scalar to vector " A - complex x"
Vector<complex_t> operator-(const complex_t&, const Vector<real_t>&); //!< subtract "complex x - A"
Vector<complex_t> operator-(const Vector<complex_t>&, const real_t&); //!< subtract real scalar to complex vector
Vector<complex_t> operator-(const real_t&, const Vector<complex_t>&); //!< subtract real scalar to complex vector

//-------------------------------------------------------------------------------
// multiply or divide vector by scalar
//-------------------------------------------------------------------------------
template<typename K>
Vector<K> operator*(const K& x, const Vector<K>& a) //! multiply vector by a scalar "x * A"
{
  Vector<K> r(a);
  typename Vector<K>::it_vk it;

  for(it = r.begin(); it != r.end(); it++)
    {
      *it *= x;
    }

  return r;
}

template<typename K>
Vector<K> operator*(const Vector<K>& a, const K& x) //! multiply vector by a scalar "A * x"
{
  return x * a;
}


template<typename K>
Vector<K> operator/(const Vector<K>& a, const K& x) //! divide vector by a scalar " A / x"
{
  if(std::abs(x) < theEpsilon) { a.divideByZero("A/=x"); }
  return (1. / x) * a;
}

//specialization real->complex
Vector<complex_t> operator*(const complex_t&, const Vector<real_t>&); //!< multiply real vector by complex scalar "x * A"
Vector<complex_t> operator*(const Vector<real_t>&, const complex_t&); //!< multiply real vector by complex scalar "A * x"
Vector<complex_t> operator*(const Vector<complex_t>&, const real_t&); //!< multiply complex vector by real scalar "A * x"
Vector<complex_t> operator/(const Vector<real_t>&, const complex_t&); //!< divide real vector by complex scalar "A / x"
Vector<complex_t> operator/(const Vector<complex_t>&, const real_t&); //!< divide complex vector by real scalar "A / x"

template<typename T>
inline
Vector<Vector<T> > operator*(const real_t& x, const Vector<Vector<T> >& A)          //! multiply vector of vector<T> by a real "x * A"
{
  Vector<Vector<T> > r(A);
  typename Vector<Vector<T> >::iterator it = r.begin();
  for(; it < r.end(); it++) { *it *= x; }
  return r;
}

template<typename T>
Vector<Vector<T> > operator*(const Vector<Vector<T> >& A, const real_t& x)          //! multiply vector of vector<T> by a real "A * x"
{
  Vector<Vector<T> > r(A);
  typename Vector<Vector<T> >::iterator it = r.begin();
  for(; it < r.end(); it++) { *it *= x; }
  return r;
}

template<typename T>
Vector<Vector<T> > operator/(const Vector<Vector<T> >& A, const real_t& x)          //! multiply vector of vector<T> by a real "A * x"
{
  Vector<Vector<T> > r(A);
  if(std::abs(x) < theEpsilon) { A.divideByZero("A/=x"); }
  typename Vector<Vector<T> >::iterator it = r.begin();
  for(; it < r.end(); it++) { *it /= x;}
  return r;
}

Vector<Vector<complex_t> > operator*(const Vector<Vector<complex_t> >&, const complex_t&); //!< multiply complex vector by complex scalar "A * x"
Vector<Vector<complex_t> > operator/(const Vector<Vector<complex_t> >&, const complex_t&); //!< divide complex vector by complex scalar "A / x"
Vector<Vector<complex_t> > operator*(const complex_t&, const Vector<Vector<complex_t> >&); //!< multiply complex scalar  by complex vector "x * A"

Vector<complex_t> operator+(const Vector<real_t>& rA, const Vector<complex_t>& cB);
Vector<complex_t> operator+(const Vector<complex_t>& cA, const Vector<real_t>& rB);
Vector<complex_t> operator+(const Vector<real_t>& rA, const complex_t& x);
Vector<complex_t> operator+(const complex_t& x, const Vector<real_t>& rA);
Vector<complex_t> operator+(const Vector<complex_t>& cA, const real_t& x);
Vector<complex_t> operator+(const real_t& x, const Vector<complex_t>& cA);

Vector<complex_t> operator-(const Vector<real_t>& rA, const Vector<complex_t>& cB);
Vector<complex_t> operator-(const Vector<complex_t>& cA, const Vector<real_t>& rB);
Vector<complex_t> operator-(const Vector<real_t>& rA, const complex_t& x);
Vector<complex_t> operator-(const complex_t& x, const Vector<real_t>& rA);
Vector<complex_t> operator-(const Vector<complex_t>& cA, const real_t& x);
Vector<complex_t> operator-(const real_t& x, const Vector<complex_t>& cA);

Vector<complex_t> operator*(const Vector<real_t>& rA, const complex_t& x);
Vector<complex_t> operator*(const complex_t& x, const Vector<real_t>& rA);
Vector<complex_t> operator*(const Vector<complex_t>& cA, const real_t& x);
Vector<complex_t> operator*(const real_t& x, const Vector<complex_t>& cA);

Vector<complex_t> operator/(const Vector<real_t>& rA, const complex_t& x);
Vector<complex_t> operator/(const Vector<complex_t>& cA, const real_t& x);

//--------------------------------------------------------------------------------
// forbidden operations (may be involved during compilation of template
//--------------------------------------------------------------------------------
template<typename K>
Vector<K> operator*(const Vector<K>& a, const Vector<K>& b) //! forbidden product of vector
{
  error("forbidden", "operator*(Vector<K>&,Vector<K>&)");
  return a;  //fake return
}

//--------------------------------------------------------------------------------
// template conjugaison, assuming conj(real)-> real
//--------------------------------------------------------------------------------
template <typename K>
Vector<K> conj(const Vector<K>& a)  //! conjugate a Vector<K>
{
  Vector<K> r(a.size());
  typename Vector<K>::const_iterator ita = a.begin();
  typename Vector<K>::iterator itr;

  for(itr = r.begin(); itr != r.end(); itr++, ita++)
    {
      *itr = conj(*ita);
    }

  return r;
}

//--------------------------------------------------------------------------------
// extern specialized functions involved Vector<complex_t> and Vector<real_t>
//--------------------------------------------------------------------------------
Vector<complex_t> cmplx(const Vector<real_t>&);     //!< cast a real vector to a complex vector
Vector<complex_t> cmplx(const Vector<complex_t>&);  //!< cast a complex vector to a complex vector
Vector<real_t> real(const Vector<real_t>&);         //!< real part of a real vector
Vector<real_t> real(const Vector<complex_t>&);      //!< real part of a complex vector
Vector<real_t> imag(const Vector<real_t>&);         //!< imaginary part of a real vector
Vector<real_t> imag(const Vector<complex_t>&);      //!< imaginary part of a complex vector
Vector<real_t> abs(const Vector<real_t>&);          //!< abs of a real vector
Vector<real_t> abs(const Vector<complex_t>&);       //!< abs of a complex vector

//--------------------------------------------------------------------------------
// extern specialized functions involving Vector<Vector<complex_t>> and Vector<Vector<real_t>>
//--------------------------------------------------------------------------------
Vector<Vector<complex_t> > cmplx(const Vector<Vector<real_t> >&);   //!< cast a real vector vector to a complex vector vector
Vector<Vector<real_t> > real(const Vector<Vector<real_t> >&);       //!< real part of a vector of real vectors
Vector<Vector<real_t> > real(const Vector<Vector<complex_t> >&);    //!< real part of a vector of complex vectors
Vector<Vector<real_t> > imag(const Vector<Vector<real_t> >&);       //!< imaginary part of a vector of real vectors
Vector<Vector<real_t> > imag(const Vector<Vector<complex_t> >&);    //!< imaginary part of a vector of complex vectors
Vector<Vector<real_t> > abs(const Vector<Vector<real_t> >&);        //!< abs of a vector of real vectors
Vector<Vector<real_t> > abs(const Vector<Vector<complex_t> >&);     //!< abs of a vector of complex vectors

//--------------------------------------------------------------------------------
// special functions on vector (tensor computation, i.e components by components)
//--------------------------------------------------------------------------------
Vector<real_t> toVector(const Point &p);                   //!< return  as Vector
Vector<real_t> toVector(const Point &p, const Point& q);   //!< return q-p as Vector

template <typename K>
Vector<K> sqrt(const Vector<K>& v)  //!sqrt(v)
{
  Vector<K> r(v);
  typename std::vector<K>::const_iterator it_v=v.begin();
  typename std::vector<K>::iterator it_r=r.begin();
  for(; it_v!=v.end(); it_v++,it_r++) *it_r = sqrt(*it_v);
  return r;
}

template <typename K>
Vector<K> log(const Vector<K>& v)  //!log(v)
{
  Vector<K> r(v);
  typename std::vector<K>::const_iterator it_v=v.begin();
  typename std::vector<K>::iterator it_r=r.begin();
  for(; it_v!=v.end(); it_v++,it_r++) *it_r = log(*it_v);
  return r;
}

template <typename K>
Vector<K> power(const Vector<K>& v, real_t n)  //!log(v)
{
  Vector<K> r(v);
  typename std::vector<K>::const_iterator it_v=v.begin();
  typename std::vector<K>::iterator it_r=r.begin();
  for(; it_v!=v.end(); it_v++,it_r++) *it_r = power(*it_v,n);
  return r;
}

//--------------------------------------------------------------------------------
//
// Some wrappers for "strange" functions taken from Melina++ to overcome
// the compilation issue of Solver (Instantiation with real += complex)
//
//--------------------------------------------------------------------------------
template<typename K, typename KK>
void addVectorThenAssign(Vector<K>& v, const Vector<KK>& b)
{
  if(v.size() != b.size())
    {
      v.mismatchSize("-=", b.size());
    }

  typedef typename std::vector<K>::iterator it_vk;
  typedef typename std::vector<KK>::const_iterator cit_vk;

  cit_vk it_b = b.begin();

  for(it_vk it = v.begin(); it != v.end(); it++, it_b++)
    {
      *it += *it_b;
    }
}
void addVectorThenAssign(Vector<real_t>&, const Vector<complex_t>&);

template<typename K, typename KK>
void multScalarThenAssign(Vector<K>& v, const KK& x)
{
  typedef typename std::vector<K>::iterator it_vk;

  for(it_vk it = v.begin(); it != v.end(); it++)
    {
      *it *= x;
    }
}
void multScalarThenAssign(Vector<real_t>&, const complex_t&);

template<typename K, typename K1, typename K2>
void scaleVector(const K& k, const Vector<K1>& v, Vector<K2>& r)
{
  if(v.size() != r.size())
    {
      v.mismatchSize("-=", r.size());
    }

  typedef typename std::vector<K2>::iterator it_vk;
  typedef typename std::vector<K1>::const_iterator cit_vk;

  cit_vk it_v = v.begin();

  for(it_vk it = r.begin(); it != r.end(); it++, it_v++)
    {
      *it = *it_v * k;
    }
}

//!extract i-th components (i>0) of a Vector<Vector<K> > and store extract values in Vector<K> (index i>0 is not checked!)
template<typename K>
void extractComponents(const Vector<Vector<K> >& vov, Vector<K>& v, number_t i)
{
  number_t n=vov.size();
  v.resize(n);
  typename Vector<K>::iterator itv;
  typename Vector<Vector<K> >::const_iterator itvov=vov.begin();
  for(itv=v.begin(); itv!=v.end(); itv++,itvov++) *itv=(*itvov)[i-1];
}

template<typename K>
Vector<K> extractComponents(const Vector<Vector<K> >& vov, number_t i)
{
  Vector<K> v;
  extractComponents(vov,v,i);
  return v;
}

void scaleVector(const complex_t&, const Vector<real_t>&, Vector<real_t>&);
void scaleVector(const complex_t&, const Vector<complex_t>&, Vector<real_t>&);
void scaleVector(const real_t&, const Vector<complex_t>&, Vector<real_t>&);

//! useful dimension functions
template<typename T>
std::pair<dimen_t, dimen_t> dimsOf(const Vector<T>& v)
{return std::pair<dimen_t, dimen_t>(v.size(),1);}
std::pair<dimen_t, dimen_t> dimsOf(const real_t&);
std::pair<dimen_t, dimen_t> dimsOf(const complex_t&);

//! Inner product with COMPLEX result (not hermitian product)
template<typename K1, typename K2>
complex_t dotRC(const Vector<K1>& vecFirst, const Vector<K2>& vecSecond)
{
  complex_t z(0.), zero(0.);
  z = std::inner_product((vecFirst).begin(), (vecFirst).end(), (vecSecond).begin(), zero);
  return z;
}

complex_t dotRC(const Vector<real_t>& vecFirst, const Vector<real_t>& vecSecond);

//! Hermitian Inner product
template<typename K1, typename K2>
complex_t dotC(const Vector<K1>& vecFirst, const Vector<K2>& vecSecond)
{
  complex_t z(0.);
  z = hermitianInnerProductTpl((vecFirst).begin(), (vecFirst).end(), (vecSecond).begin());
  return z;
}

complex_t dotC(const Vector<real_t>& vecFirst, const Vector<real_t>& vecSecond);
complex_t dotC(const Vector<complex_t>& vecFirst, const Vector<real_t>& vecSecond);

template<typename K1, typename K2>
complex_t hermitianProduct(const Vector<K1>& vecFirst, const Vector<K2>& vecSecond)
{
    return dotC(vecFirst,vecSecond);
}

//! inner product for real and complex
template<typename K>
K dot(const Vector<K>& u, const Vector<K>& v)
{
  K z(0.), zero(0.);
  z = std::inner_product(u.begin(), u.end(), v.begin(), zero);
  return z;
}
complex_t dot(const Vector<complex_t>& u, const Vector<real_t>& v);
complex_t dot(const Vector<real_t>& u, const Vector<complex_t>& v);


//! cross product in 3D
template<typename K>
Vector<K> crossProduct(const Vector<K>& u, const Vector<K>& v)
{
  if(u.size() != 3 || v.size() != 3) error("3D_only", "crossProduct");
  Vector<K> w(3);
  w[0] = u[1] * v[2] - u[2] * v[1];
  w[1] = u[2] * v[0] - u[0] * v[2];
  w[2] = u[0] * v[1] - u[1] * v[0];
  return w;
}

//! cross product in 2D, return a scalar
template<typename K>
K crossProduct2D(const Vector<K>& u, const Vector<K>& v)
{
  if(u.size() != 2 || v.size() != 2) error("2D_only", "crossProduct2D");
  return u[0] * v[1] - u[1] * v[0];
}

Vector<complex_t> crossProduct(const Vector<complex_t>& , const Vector<real_t>&);
Vector<complex_t> crossProduct(const Vector<real_t>&, const Vector<complex_t>&);
complex_t crossProduct2D(const Vector<complex_t>&, const Vector<real_t>&);
complex_t crossProduct2D(const Vector<real_t>&, const Vector<complex_t>&);


//! cross product with vector and iterator in 3D and 2D (return a scalar)
template<typename K, typename IteratorV, typename IteratorR>
void crossProduct(const Vector<K>& u, const IteratorV& itv, IteratorR& itr)
{
  if(u.size() == 3)
    {
      *itr++ = u[1]**(itv + 2) - u[2]**(itv + 1);
      *itr++ = u[2]**itv - u[0]**(itv + 2);
    }
  *itr = u[0]**(itv + 1) - u[1]**itv;
}

//! norm of Vector, extern forms
template<typename T> real_t norm2(const Vector<T>& v)
{return v.norm2();}

template<typename T> real_t norm1(const Vector<T>& v)
{return v.norm1();}

template<typename T> real_t norminfty(const Vector<T>& v)
{return v.norminfty();}

//! create trivial numbering vector n1, n1+1, ...,n2
template<typename T>
Vector<T> trivialNumbering(const T& n1, const T& n2)
{
  Vector<T> tn;
  if(n2<n1) return tn;   //void numbering !
  tn.resize(n2-n1+1);
  typename Vector<T>::iterator it=tn.begin();
  for(T n=n1; n<=n2; n++, it++) *it=n;
  return tn;
}

//! generates n points xi = a+i*dx with x = (b-a)/(n-1).
template<typename T>
Vector<T> lineSpace(const T& a, const T& b, number_t n)
{
  Vector<T> tn;
  if(n==0) return tn;
  tn.resize(n);
  if(n==1) {tn[0]=a; return tn;}
  T x=a, dx=(b-a)/(n-1);
  auto it=tn.begin();
  for(number_t i=0;i<n-1;i++,++it,x+=dx) *it=x;
  *it=b;   // to avoid rounding effect
  return tn;
}

//! return the value (in complex) of component being the largest one in absolute value
template<typename K>
complex_t maxAbsVal(const Vector<K>& u)
{
  typedef typename std::vector<K>::const_iterator cit_vk;
  complex_t vmax=0., v;
  for(cit_vk it = u.begin(); it != u.end(); it++) {
    v=maxAbsVal(*it);
    if(std::abs(v) > std::abs(vmax)) { vmax = v; }
  }
  return vmax;
}

//! forced cast complex to any
template<class T>
inline T complexToT(const Vector<complex_t>& c){return c;}
template<>
inline Vector<real_t> complexToT(const Vector<complex_t>& c){ return real(c);}
inline real_t complexToRorC(const complex_t& c, const Vector<real_t>& vr) {return c.real();}
inline complex_t complexRorC(const complex_t& c, const Vector<complex_t>& vc) {return c;}

//for consistent conversion
template<typename T>
inline const Vector<T>& tran(const Vector<T>& v)
{
    return v;
}

//=========================================================================================
typedef Vector<real_t> Reals;
typedef Vector<complex_t> Complexes;

} // end of namespace xlifepp

#endif /* VECTOR_HPP */

