/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Environment.cpp
  \authors D. Martin, E. Lunéville
  \since 14 dec 2002
  \date 3 may 2012

  \brief Implementation of xlifepp::Environment class members and related functions
 */

#include "Environment.hpp"
#include "Point.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "Parameters.hpp"
#include "Timer.hpp"
#include "String.hpp"
#include "ThreadData.hpp"

#include <fstream>
#include <iostream>
#include <sstream>

// specific headers for windows os
#if defined(OS_IS_WIN)
#include "winUtils.hpp"
#endif

namespace xlifepp
{

//! constructor
Environment::Environment(Language lang)
  : theLanguage_(lang), theOS_("unknown"), theProcessor_("undef"), theMachineName_("(none)"), thePathToMessageFiles_("_path_to_msg_files_")
{
  running_ = true;
  parallelOn_= true;  //default behaviour: parallel is active if it is available
  names();
  processor();
  setGeoMacroFilePath();
  version();
  setMsgFilePath();
  localizedStrings();
}

//! true constructor
void Environment::rebuild()
{
  setMsgFilePath();
  localizedStrings();
}

/*!
  find OS and machine names
 */
void Environment::names()
{
#if defined(OS_IS_UNIX)
  const string_t tmp("__xlife++_env__");
  system("uname > __xlife++_env__ ; uname -n | cut -f1 -d. >> __xlife++_env__ ");

  // open file xlife++_env and read to theOS_ & theMachineName_
  std::fstream fs;
  fs.open(tmp.c_str(), std::ios::in);
  fs >> theOS_ >> theMachineName_;
  fs.close();
  remove(tmp.c_str());

#elif defined(OS_IS_WIN)
  StrUname uts = winUname();
  theOS_ = uts.osName;
  theMachineName_ = uts.machineName;
  theProcessor_ = uts.processor;
#endif
}

//! Search for processor or machine type name
void Environment::processor()
{
  #ifdef OS_IS_UNIX
    const string_t tmp("__xlife++_env__");

    // write processor name to file __xlife++_env__
    if (theOS_ == "Linux") /* `uname -p` yields "unknown" in most cases */
    {
      system("uname -m > __xlife++_env__");
    }
    else
    {
      system("uname -p > __xlife++_env__");
    }

    // open file __xlife++_env__ and read to theProcessor_
    std::fstream fs;
    fs.open(tmp.c_str(), std::ios::in);
    fs >> theProcessor_;
    fs.close();
    remove(tmp.c_str());
  #endif
  // for Windows, processor is set up in names()
}

//! Default path to message and error files
void Environment::setMsgFilePath()
{
  // Environment is pre-initialized with english language
  string_t lang="en";
  if (theLanguage_ != 0) lang=languageString();
  #ifdef OS_IS_UNIX
    thePathToMessageFiles_ = installPath() + "/etc/messages/" + lang + "/";
  #elif defined(OS_IS_WIN)
    thePathToMessageFiles_ = installPath() + "\\etc\\messages\\" + lang + "\\";
  #endif
}

//! Default path to message and error files
void Environment::setGeoMacroFilePath()
{
  #ifdef OS_IS_UNIX
    thePathToGeoMacroFile_ = installPath() + "/etc/gmsh/";
  #elif defined(OS_IS_WIN)
    thePathToGeoMacroFile_ = installPath() + "\\etc\\gmsh\\";
  #endif
}

/*!
  reads dictionnary words from file
      format:  # string_family_id
                  string_id word
                  ...
  create an MsgFormat object for each item read
 */
void Environment::localizedStrings()
{
  // open the dictionnary file and connect to an ifstream
  string_t file = thePathToMessageFiles_ + "dictionary.txt";
  std::ifstream ifs(file.c_str());

  // error: unable to open file for input
  if (!ifs)
  {
    // neither Message nor Dictionnary have been set up here so output an explicit error message here
    string_t stars(76, '-');

    if ( this->language() == _fr )
    {
      std::cout << ";-( " << stars << "\n"
                << ";-( " << trace_p->current() << " Erreur: Impossible d'ouvrir le fichier " << file << "\n"
                << ";-( " << stars << "\n" << "\n";
    }
    else
    {
      std::cout << ";-( " << stars << "\n"
                << ";-( " << trace_p->current() << " Error: Unable to open input file " << file << "\n"
                << ";-( " << stars << "\n" << "\n";
    }

    abort();
  }

  // read file
  string_t buffer(""), family(""), word(""), ids("");
  // main loop on the stream
  string_t line("");
  getline(ifs, line , '@');

  // if dictionary maps are non empty, we clear them
  if (enumWords_.size() > 0) { enumWords_.clear(); }
  if (words_.size() > 0) { words_.clear(); }
  
  while (!ifs.eof())
  {
    getline(ifs, ids, '\n');

    // a comment, jump over
    if (ids[0] == '/')
    {
      getline(ifs, buffer, '@');
    }
    else
    {
      family = trim(ids);

      if (family == "dictionary")
      {
        getline(ifs, buffer, '@');

        if (buffer[0] == '\n')
        {
          buffer.erase(0, 1);  // erase first eol
        }

        buffer.erase(buffer.length() - 1, 1);    // erase last eol

        std::istringstream iss(buffer);

        while (!iss.eof())
        {
          getline(iss, ids, '=');
          getline(iss, word, '\n');
          words_[trim(ids)] = trim(word);
        }
      }
      else
      {
        getline(ifs, buffer, '@');

        if (buffer[0] == '\n')
        {
          buffer.erase(0, 1);  // erase first eol
        }

        buffer.erase(buffer.length() - 1, 1);    // erase last eol

        std::istringstream iss(buffer);

        while (!iss.eof())
        {
          getline(iss, ids, '=');
          getline(iss, word, '\n');
          enumWords_[family].push_back(trim(word));
        }
      }
    }
  }

  ifs.close();
}

//! defines version number and date of XLiFE++ from the VERSION.txt file
void Environment::version() {
  #ifdef OS_IS_UNIX
    string_t file = installPath() + "/VERSION.txt";
  #elif defined(OS_IS_WIN)
    string_t file = installPath() + "\\VERSION.txt";
  #endif
  std::ifstream ifs(file.c_str());

  // error: unable to open file for input
  if (!ifs)
  {
    // install path has not been set up here so output an explicit error message here
    string_t stars(76, '-');

    if ( this->language() == _fr )
    {
      std::cout << ";-( " << stars << "\n"
                << ";-( " << trace_p->current() << " Erreur: Impossible d'ouvrir le fichier " << file << "\n"
                << ";-( " << stars << "\n" << "\n";
    }
    else
    {
      std::cout << ";-( " << stars << "\n"
                << ";-( " << trace_p->current() << " Error: Unable to open input file " << file << "\n"
                << ";-( " << stars << "\n" << "\n";
    }

    abort();
  }

  // read file
  string_t buffer(""), family(""), word(""), ids("");
  // main loop on the stream
  string_t line("");

  getline(ifs, buffer, '\n'); // This line begins with "OVERVIEW: " It is ignored
  getline(ifs, buffer, '\n'); // This line begins with "VERSION: "
  theXlifeppVersion_ = buffer.substr(9);
  getline(ifs, buffer, '\n'); // This line begins with "DATE: "
  theXlifeppDate_ = buffer.substr(6);
  ifs.close();

}



//! print dictionary
void Environment::printDictionary(std::ostream& out)
{
  std::map<string_t, std::vector<string_t> >::iterator it;
  out << "============== Dictionary, enumeration words ===================" << std::endl;

  for (it = enumWords_.begin(); it != enumWords_.end(); it++)
  {
    out << it->first << ": " << std::endl;
    std::vector<string_t>& enit = it->second;

    for (number_t n = 0; n < enit.size(); n++)
    {
      out << "   " << n << " -> " << enit[n] << std::endl;
    }
  }

  out << "=================== Dictionary, words ==========================" << std::endl;
  out << "number of words = " << words_.size();
  std::map<string_t, string_t >::iterator jt;

  for (jt = words_.begin(); jt != words_.end(); jt++)
  {
    out << jt->first << " = " << jt->second << std::endl;
  }
}

//! returns language as a string
string_t Environment::languageString() const
{
  return words("language", theLanguage_);
}

//! display logo and header information on console
void Environment::printHeader(const Timer& tm)
{
  if (theVerboseLevel>0)
  {
    std::cout << logo(tm.seconds()) << std::endl;
    std::cout << " XLiFE++ " << theXlifeppVersion_ << " (" << theXlifeppDate_ << ")" << std::endl
              << " " << words_["running on"] << " " << theLongDate() << " " << words_["at"] << " " << theTime() << " "
              << words_["on"] << " " << osName() << "-" << processorName() << " (" << machineName() << "), ";
    number_t nbt = numberOfThreads();
    std::cout << nbt << " " << words("available thread");
    if (nbt > 1) std::cout << "s";
    std::cout << std::endl << std::endl;
  }
}

//! display logo and header information on aux files
void Environment::printHeader(std::ofstream& ofs)
{
  if (theVerboseLevel>0)
  {
    ofs << " XLiFE++ " << theXlifeppVersion_ << " (" << theXlifeppDate_ << ")" << std::endl
        << " " << words_["running on"] << " " << theDate() << " " << words_["at"] << " " << theTime() << " "
        << words_["on"] << " " << osName() << "-" << processorName() << " (" << machineName() << "), ";
    number_t nbt = numberOfThreads();
    ofs << nbt << " " << words("available thread");
    if (nbt > 1) ofs << "s";
    ofs << std::endl << std::endl;
  }
  ofs << "Machine epsilon                     theEpsilon_= " << theEpsilon << std::endl
      << "Greatest real number (real_t)       theRealMax_= " << theRealMax << std::endl
      << "Divide by zero threshold      theZeroThreshold_= " << theZeroThreshold << std::endl
      << "Default convergence threshold     theTolerance_= " << theTolerance << std::endl
      << "Greatest integer (int)                 INT_MAX = " << INT_MAX << std::endl
      << "Greatest unsigned (number_t)      theNumberMax_= " << theNumberMax << std::endl
      << "Greatest address (size_t)           theSizeMax_= " << theSizeMax << std::endl
      << "Greatest dimension (dimen_t)         theDimMax_= " << theDimMax << std::endl
      << "-------------------------------------------------------------" << std::endl;
}

//! accessor to words with a string_t
string_t words(const string_t& key)
{
  if (Environment::words_.find(key) != Environment::words_.end() )
  {
    return Environment::words_[key];
  }
  else
  {
    warning("undef_key_word", key);
    string_t s = key;
    return s;
  }
}

//! accessor to words with a boolean
string_t words(const bool b)
{
  if(b) { return words("yes"); }
  return words("no");
}

//! accessor to words with a const char* (needed to avoid automatic cast to bool type)
string_t words(const char* key)
{
  return words(string_t(key));
}

//! accessor to enumWords
string_t words(const string_t& key, const int id)
{
  if (Environment::enumWords_.find(key) != Environment::enumWords_.end() )
  {
    if (id >= 0 && id < (int)Environment::enumWords_[key].size())
    {
      return Environment::enumWords_[key][id];
    }
    else
    {
      warning("undef_id_enumWords", key, id);
      std::stringstream oss;
      oss << key << "[" << id << "]";
      string_t s = oss.str();
      return s;
    }
  }
  else
  {
    warning("undef_key_enumWords", key);
    std::stringstream oss;
    oss << key << "[" << id << "]";
    string_t s = oss.str();
    return s;
  }
}

/*! manage the number of threads in OpenMP
    if n==0  set to the maximum of number of threads
    if n >0  set the number of threads to n
    if n==-1 return the number of threads (default)
    if omp is not available, always return 1
*/
number_t numberOfThreads(int n)
{
  int nt=1;
  #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel
    #pragma omp master
      { nt=omp_get_num_threads();}   //current number of threads
    if (n==-1 || nt==n) return nt;    //nothing to change
    int nmax = omp_get_num_procs();
    if (n==0) nt=nmax; else nt=n;
    if (nt>nmax) warning("omp_too_much_threads",nt,nmax);
    omp_set_num_threads(nt);       //change the number of threads
    theThreadData.resize(nt);      //resize global thread current vectors
  #endif // XLIFEPP_WITH_OMP
  return nt;  // 1 if no OMP
}

} // end of namespace xlifepp
