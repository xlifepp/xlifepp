/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Environment.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 14 dec 2002
  \date 3 may 2012

  \brief Definition of the xlifepp::Environment class
 */

#ifndef ENVIRONMENT_HPP
#define ENVIRONMENT_HPP

#include "config.h"
#include "Timer.hpp"
#include "Messages.hpp"
#include "String.hpp"
#include "logo.hpp"

#ifdef XLIFEPP_WITH_OMP
  #include <omp.h>
#endif // XLIFEPP_WITH_OMP

#include <fstream>
#include <map>
#include <vector>

namespace xlifepp
{

/*!
  \class Environment
  Class Environment stores environment (!) variables and methods to build them
*/
class Environment
{
  private:
    Language theLanguage_;           //!< language as an int
    string_t theXlifeppVersion_;     //!< version number of XLiFE++
    string_t theXlifeppDate_;        //!< version date of XLiFE++
    string_t theOS_;                 //!< OS name ( = `uname` )
    string_t theProcessor_;          //!< processor or machine type ( = `uname -p` under most OSes, `uname -m` under Linux )
    string_t theMachineName_;        //!< machine name ( = `uname -n` )
    string_t thePathToMessageFiles_; //!< path to error files
    string_t thePathToGeoMacroFile_; //!< path to geo macro file (for runtime mesh generation with gmsh)

    static const string_t theInstallPath_;                        //!< carries xlife++ installation path !
    static const string_t theVisuTermVecMPath_;                   //!< carries path to visuTermVec.m !
    static const string_t theGmshExe_;                            //!< carries gmsh binary !
    static const string_t theParaviewExe_;                        //!< carries paraview binary !
    static bool running_;                                         //!< true when running !
    static bool parallelOn_;                                      //!< true if parallel is active (default on) !
    static std::map<string_t, std::vector<string_t> > enumWords_; //!< localized strings for global enum lists
    static std::map<string_t, string_t> words_;                   //!< localized strings for global words or group of words
    static std::vector<string_t> systemKeys_;                     //!< keys that are used for system command line options
    static std::vector<string_t> authorizedSaveToFileExtensions_; //!< extensions that are authorized for every saveToFile routine

  public:
    //------------------------------------------------------------------------------
    // Constructors, Destructor
    //------------------------------------------------------------------------------
    Environment(Language lang = _en ); //!< constructor
    ~Environment() {} //!< destructor

    void rebuild(); //!< true constructor

    //------------------------------------------------------------------------------
    // Inline Access functions
    //------------------------------------------------------------------------------
    //! returns running state
    static bool running()
    { return running_; }
    static bool parallelOn()
    {return parallelOn_;}
    //! return installation path
    static string_t installPath()
    { return theInstallPath_; }
    //! return path to visuTermVec.m
    static string_t visuTermVecMPath()
    { return theVisuTermVecMPath_; }
    //! return gmsh binary
    static string_t gmshExe()
    { return theGmshExe_; }
    //! return paraview binary
    static string_t paraviewExe()
    { return theParaviewExe_; }
    //! return number of recognized languages
    int numberOfLanguages()
    { return _nbLanguages; }
    //! return language as an int
    int language() const
    { return theLanguage_; }
    void language(Language l)
    { theLanguage_=l; }
    //! returns OS name
    string_t osName() const
    { return theOS_; }
    //! returns processor or machine type name
    string_t processorName() const
    { return theProcessor_; }
    //! returns machine name
    string_t machineName() const
    { return theMachineName_; }
    //! returns path to error files
    string_t msgFilePath() const
    { return thePathToMessageFiles_; }
    //! returns path to geo macro file (for runtime mesh generation with gmsh)
    string_t geoMacroFilePath() const
    { return thePathToGeoMacroFile_; }
    //! returns true if the machine name is not empty
    bool known() const
    { return (theMachineName_ != ""); }
    static void parallel(bool on)
    { parallelOn_=on; }
    //! return the list of system keys
    static std::vector<string_t> systemKeys()
    { return systemKeys_; }
    //! return the list of system keys
    static void systemKeys(const std::vector<string_t>& keys)
    { systemKeys_=keys; }
    //! return the list of authorized file extensions in saveToFile routines
    static std::vector<string_t> authorizedSaveToFileExtensions()
    { return authorizedSaveToFileExtensions_; }
    //! sets the list of authorized file extensions in saveToFile routines
    static void authorizedSaveToFileExtensions(const std::vector<string_t>& extensions)
    { authorizedSaveToFileExtensions_=extensions; }

    //------------------------------------------------------------------------------
    // Public Utility functions
    //------------------------------------------------------------------------------

    //! defines OS and machine names
    void names();
    //! defines processor or machine type name
    void processor();
    //! defines path to xlife++ error files
    void setMsgFilePath();
    //! defines path to xlife++ geo macro file (for runtime mesh generation with gmsh)
    void setGeoMacroFilePath();
    //! builds localized strings
    void localizedStrings();
    //! defines version number and date of XLiFE++ from the VERSION.txt file
    void version();
    //! returns language as a string
    string_t languageString() const;

    //! return type of returned function value
    int returnedFunctionType(const string_t& s);
    //! return type of returned kernel function value
    int returnedKernelType(const string_t& s);

    //! display logo and header information on console
    void printHeader(const Timer&);
    //! display logo and header information on aux files
    void printHeader(std::ofstream&);

    void printDictionary(std::ostream&); //!< print dictionary

    // external function to manage localized strings
    friend string_t words(const string_t&);
    friend string_t words(const bool);
    friend string_t words(const char*);
    friend string_t words(const string_t&, int);

    friend bool parallelOn() { return Environment::parallelOn_; }
    friend void parallel(bool on) { Environment::parallelOn_=on; }

    //------------------------------------------------------------------------------
    // private
    //------------------------------------------------------------------------------
  private:
    //! copy constructor (needed by compiler because of theLanguage_ "constness")
    Environment(const Environment& other)
      : theLanguage_(other.theLanguage_) {}

    //! no assignment operator=
    void operator=(const Environment&) {}

}; // end of class Environment

//! Global Scope Environment object
extern Environment* theEnvironment_p;

//@{
//! external function to manage localized strings
string_t words(const string_t&);
string_t words(const bool b);
string_t words(const char*);
string_t words(const string_t&, const int);
//@}

inline bool is32bits(){return 8*sizeof(void*) == 32;}
inline bool is64bits(){return 8*sizeof(void*) == 64;}

//! manage eol variable by adding n spaces after carriage return
inline void seteol(number_t n=0)
{
  if (n>0) eol="\n"+string_t(n,' ');
  else eol="\n";
}

// multi-threading tools
number_t numberOfThreads(int n=-1);  //!< if omp is available, set or get the number of threads (default)
inline number_t currentThread()      //! if omp is available, return the current thread number else return 0
{
  #ifdef XLIFEPP_WITH_OMP
    return omp_get_thread_num();
  #else
    return 0;
  #endif // XLIFEPP_WITH_OMP
}
void resizeThreadData(number_t); //!< resize global vectors currentNxs, currentNys, ...

//c++11 check
inline bool cpp11()
{
#if __cplusplus >= 201103L    // C++2011 available
  return true;
#else
  return false;
#endif // __cplusplus
}

// sets the tolerance factor when point is located outside each element s of a mesh but near enought to be projected
inline void locateToleranceFactor(real_t f)
{
  theLocateToleranceFactor=f;
}

} // end of namespace xlifepp

#endif /* ENVIRONMENT_HPP */
