/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file winUtils.cpp
  \author E. Lunéville
  \since 27 dec 2011
  \date 3 may 2012

  \brief Provides specific windows utilities
  uname()
 */

#include <iostream>
#include "String.hpp"

#ifdef OS_IS_WIN

  #include "winUtils.hpp"
  //#include <windef.h>
  //#include <stdio.h>
  //#include <cstdarg>
  //#include <winbase.h>
  #include <windows.h>

  /* windows OS major and minor version
  Windows 7                  6.1
  Windows Server 2008 R2     6.1
  Windows Server 2008        6.0
  Windows Vista              6.0
  Windows Server 2003 R2     5.2
  Windows Server 2003        5.2
  Windows XP 64-Bit Edition  5.2
  Windows XP                 5.1
  Windows 2000               5.0


  struct OSVERSIONINFO {
    DWORD dwOSVersionInfoSize;    The size of this data structure, in bytes
    DWORD dwMajorVersion;         The major version number of the operating system.
    DWORD dwMinorVersion;         The minor version number of the operating system.
    DWORD dwBuildNumber;          The build number of the operating system.
    DWORD dwPlatformId;           The operating system platform.
    TCHAR szCSDVersion[128];      The latest Service Pack installed on the system
  }

  typedef struct _SYSTEM_INFO {
    union {
      DWORD  dwOemId;                       An obsolete member that is retained for compatibility
      struct {
        WORD wProcessorArchitecture;        The processor architecture of the installed operating system
                                              = PROCESSOR_ARCHITECTURE_AMD64 (9) : x64 (AMD or Intel)
                                              = PROCESSOR_ARCHITECTURE_IA64 (6) : Intel Itanium-based
                                              = PROCESSOR_ARCHITECTURE_INTEL (6) : x86
                                              = PROCESSOR_ARCHITECTURE_PPC
                                              = PROCESSOR_ARCHITECTURE_ALPHA
                                              = PROCESSOR_ARCHITECTURE_MIPS
                                              = PROCESSOR_ARCHITECTURE_UNKNOWN (0xffff)
        WORD wReserved;                     This member is reserved for future use.
        };
    };
    DWORD     dwPageSize;                   The page size and the granularity of page protection and commitment
    LPVOID    lpMinimumApplicationAddress;  A pointer to the lowest memory address accessible to applications and dynamic-link libraries (DLLs).
    LPVOID    lpMaximumApplicationAddress;  A pointer to the highest memory address accessible to applications and DLLs.
    DWORD_PTR dwActiveProcessorMask;        A mask representing the set of processors configured into the system. Bit 0 is processor 0; bit 31 is processor 31.
    DWORD     dwNumberOfProcessors;         The number of logical processors in the current group.
    DWORD     dwProcessorType;              An obsolete member that is retained for compatibility  (win3, win95, win98)
                                              PROCESSOR_INTEL_386 (386)
                                              PROCESSOR_INTEL_486 (486)
                                              PROCESSOR_INTEL_PENTIUM (586)
                                              PROCESSOR_INTEL_IA64 (2200)
                                              PROCESSOR_AMD_X8664 (8664)
    DWORD     dwAllocationGranularity;      The granularity for the starting address at which virtual memory can be allocated.
    WORD      wProcessorLevel;              The architecture-dependent processor level.
    WORD      wProcessorRevision;           The architecture-dependent processor revision.
  }

  */

  namespace xlifepp
  {

  StrUname winUname() //! return os, machine and processor names
  {
    StrUname uts;
    OSVERSIONINFO osver;
    SYSTEM_INFO sysinfo;
    osver.dwOSVersionInfoSize = sizeof (osver);
    GetVersionEx (&osver);

    switch (osver.dwPlatformId)
    {
      case VER_PLATFORM_WIN32_NT: /* NT, Windows 2000 or Windows XP */

        if (osver.dwMajorVersion <= 3)
        {
          uts.osName = "WIN_NT-3x";
        }
        else if (osver.dwMajorVersion == 4)
        {
          uts.osName = "WIN_NT-4x";
        }
        else if (osver.dwMajorVersion == 5)
        {
          if (osver.dwMinorVersion == 0)
          {
            uts.osName = "WIN_NT-5.0";
          }
          else if (osver.dwMinorVersion == 1)
          {
            uts.osName = "WIN_NT-5.1";
          }
          else if (osver.dwMinorVersion == 2)
          {
            uts.osName = "WIN_NT-5.2";
          }
        }

        else if (osver.dwMajorVersion == 6)
        {
          if (osver.dwMinorVersion < 1)
          {
            uts.osName = "WIN_NT-6.0";
          }
          else
          {
            uts.osName = "WIN_NT-6.1";
          }
        }

        break;

      case VER_PLATFORM_WIN32_WINDOWS: /* Win95, Win98 or WinME */

        if ((osver.dwMajorVersion > 4) || ((osver.dwMajorVersion == 4) && (osver.dwMinorVersion > 0)))
        {
          if (osver.dwMinorVersion >= 90)
          {
            uts.osName = "WIN-ME";
          }
          else
          {
            uts.osName = "WIN-98";
          }
        }
        else
        {
          uts.osName = "WIN-95";
        }

        break;

      case VER_PLATFORM_WIN32s: /* Windows 3.x */
        uts.osName = "WIN-3.x";
        break;
    }

    GetSystemInfo (&sysinfo);

    switch (sysinfo.wProcessorArchitecture)
    {
      case PROCESSOR_ARCHITECTURE_PPC:
        uts.processor = "ppc";
        break;

      case PROCESSOR_ARCHITECTURE_ALPHA:
        uts.processor = "alpha";
        break;

      case PROCESSOR_ARCHITECTURE_MIPS:
        uts.processor = "mips";
        break;

      case PROCESSOR_ARCHITECTURE_INTEL:

        switch (osver.dwPlatformId)
        {
          case VER_PLATFORM_WIN32_WINDOWS: /* Win95, Win98 or WinME */

            switch (sysinfo.dwProcessorType)
            {
              case PROCESSOR_INTEL_386:
              case PROCESSOR_INTEL_486:
              case PROCESSOR_INTEL_PENTIUM:
                uts.processor = tostring(sysinfo.dwProcessorType) + "ld";
                break;

              default:
                uts.processor = "i386";
                break;
            }

            break;

          case VER_PLATFORM_WIN32_NT:
            uts.processor = "i" + tostring(sysinfo.wProcessorLevel) + "86";
            break;

          default:
            uts.processor = "unknown";
            break;
        }

        break;

      default:
        uts.processor = "unknown";
        break;
    }

    const DWORD INFO_BUFFER_SIZE = 32767;
    TCHAR  infoBuf[INFO_BUFFER_SIZE];
    DWORD  bufCharCount = INFO_BUFFER_SIZE;

    if (GetComputerName( infoBuf, &bufCharCount ) != 0)
    {
      uts.machineName = string_t((char*)infoBuf);
    }

    return uts;
  }

  } // end of namespace xlifepp

#endif
