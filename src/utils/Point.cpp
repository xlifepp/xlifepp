/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Point.cpp
  \authors D. Martin, E. Lunéville
  \since 29 jan 2005
  \date 14 dec 2011

  \brief Implementation of xlifepp::Point class members and related functions
 */

#include "Point.hpp"
#include "../mathsResources/earcut.hpp"
#include "Environment.hpp"
#include "Messages.hpp"
#include "Trace.hpp"
#include "config.h"
#include "Algorithms.hpp"

#include <iostream>
#include <numeric>

namespace xlifepp
{

//------------------------------------------------------------------------------
// constructors (use inherited std::vector standard constructors and destructor)
//------------------------------------------------------------------------------
//constructor by dimension
// Point::Point(const dimen_t d, const real_t v)
// {
//   resize(d, v);
// }
// Point::Point(const int d, const real_t v)
// {
//   resize(d, v);
// }

//1D constructor by coordinate
Point::Point(const real_t x1)
{ this->reserve(1); this->push_back(x1); }

//2D constructor by coordinates
Point::Point(const real_t x1, const real_t x2)
{ this->reserve(2); this->push_back(x1); this->push_back(x2);}

//3D constructor by coordinates
Point::Point(const real_t x1, const real_t x2, const real_t x3)
{ this->reserve(3); this->push_back(x1); this->push_back(x2); this->push_back(x3); }

//constructor by dimension and real_t array
Point::Point(const dimen_t d, real_t* pt)
{ this->resize(d); std::vector<real_t>::assign(pt, pt + d); }

// constructor by stl vector<real_t>
Point::Point(const Point& pt)
{ this->resize(pt.size()); std::vector<real_t>::assign(pt.begin(), pt.end()); }

// constructor by stl vector<real_t>
Point::Point(const std::vector<real_t>& pt)
{ this->resize(pt.size()); std::vector<real_t>::assign(pt.begin(), pt.end()); }

//constructor by stl vector with dimension management
Point::Point(const std::vector<real_t>& pt, dimen_t dim)
{ this->resize(dim);
  number_t n=std::min(number_t(dim),pt.size());
  std::vector<real_t>::iterator itp=begin(), itpe = itp+n;
  std::vector<real_t>::const_iterator itpt = pt.begin();
  for (;itp!=itpe; ++itp, ++itpt) *itp=*itpt;
}

// constructor by stl vector iterator
Point::Point(const std::vector<real_t>::const_iterator itp, dimen_t d)
{
  this->resize(d);
  std::vector<real_t>::assign(itp,itp+d);
}

//------------------------------------------------------------------------------
// accessors (to point coordinates)
//------------------------------------------------------------------------------
// return math style d-th coordinates ( 1 <= d <= dim )
// to get C-style d-th coordinates (0 <= d < dim ) use inherited stl vector::operator[]
real_t Point::operator()(const dimen_t d) const
{
  if (d < 1 || d > size()) { dimError("Point::operator() const", d); }
  return *(begin() + d - 1);
}
real_t& Point::operator()(const dimen_t d)
{
  if (d < 1 || d > size()) { dimError("Point::operator()", d); }
  return *(begin() + d - 1);
}

//------------------------------------------------------------------------------
// I/O utilities and error handlers
//-----------------------------------------------------------------------------
void Point::dimError(const string_t& s, const dimen_t d) const
{
  where(s);
  error("point_baddim", d, size());
}

void Point::print(std::ostream& os) const
{
  if (size()==0) {os<<"()";return;}
  os << "(";
  for (cit_vr it = begin(); it != end() - 1 ; it++) { os << *it << ", "; }
  os << *(end() - 1) << ")";
}

void Point::printRaw(std::ostream& os) const
{
  for (cit_vr it = begin(); it != end() ; it++) { os << *it << " "; }
}

string_t Point::toString() const
{
  if (size()==0) {return "()";}
  string_t sp="(";
  for (cit_vr it = begin(); it != end() - 1 ; it++) sp+= tostring(*it) + ", ";
  sp+= tostring(*(end() - 1));
  sp+= ")";
  return sp;
}

/*!
  Prints, on stream os, the coordinates of the point in fixed format, separated
  by commas, for TeX exploitation.
 */
void Point::printTeX(std::ostream &os) const
{
  os << "(";
  os.setf(std::ios::fixed,std::ios::floatfield);
  cit_vr it = begin();
  os << *it;
  for (it++; it < end() ; it++) { os << "," << *it; }
  os << ")";
  os.unsetf(std::ios::fixed);
}

std::ostream& operator<<(std::ostream& os, const Point& p)
{
  p.print(os);
  return os;
}

//------------------------------------------------------------------------------
// algebraic operations members
//------------------------------------------------------------------------------
Point& Point::operator +=(const Point& p)
{
  if (p.size() < size()) { dimError("Point::operator+=", p.size()); }
  if (p.size() > size()) { p.dimError("Point::operator+=", size()); }
  cit_vr it_p(p.begin());
  for (it_vr it_this = this->begin(); it_this != this->end(); it_this++, it_p++)
    { (*it_this) += (*it_p); }
  return *this;
}

Point& Point::operator -=(const Point& p)
{
  if (p.size() < size()) { dimError("Point::operator-=", p.size()); }
  if (p.size() > size()) { p.dimError("Point::operator-=", size()); }
  cit_vr it_p(p.begin());
  for (it_vr it_this = this->begin(); it_this != this->end(); it_this++, it_p++)
    { (*it_this) -= *(it_p); }
  return *this;
}

Point& Point::operator *=(const real_t s)
{
  for (it_vr it_this = this->begin(); it_this != this->end(); it_this++)
    { (*it_this) *= s; }
  return *this;
}

Point& Point::operator /=(const real_t s)
{
  if (std::abs(s) < Point::tolerance) {error("divBy0");}
  for (it_vr it_this = this->begin(); it_this != this->end(); it_this++)
    { (*it_this) /= s; }
  return *this;
}

Point& Point::operator +=(const real_t s)
{
  for (it_vr it_this = this->begin(); it_this != this->end(); it_this++)
    { (*it_this) += s; }
  return *this;
}

Point& Point::operator -=(const real_t s)
{
  for (it_vr it_this = this->begin(); it_this != this->end(); it_this++)
    { (*it_this) -= s; }
  return *this;
}

//square distance and distance to an other point
real_t Point::squareDistance(const Point& p) const
{
  if (p.size() < size()) { dimError("Point::squareDistance", p.size()); }
  if (p.size() > size()) { p.dimError("Point::squareDistance", size()); }
  real_t dist(0.);
  cit_vr itp(p.begin());
  for (cit_vr it_this = this->begin(); it_this != this->end(); it_this++, itp++)
    { dist += (*itp - *it_this) * (*itp - *it_this); }
  return dist;
}

real_t Point::distance(const Point& p) const { return sqrt(this->squareDistance(p));}

Point Point::roundToZero(real_t aszero) const
{
  Point P=*this;
  for (number_t i=0; i < P.size(); ++i)
  {
    if (std::abs(P[i]) < aszero) { P[i]=0.; }
  }
  return P;
}

//! extends a copy of a point to a 3D point
Point force3D(const Point& p)
{
  Point p3d=p;
  for (number_t i = p.size(); i < 3; ++i) { p3d.push_back(0.); }
  return p3d;
}

//------------------------------------------------------------------------------
//Extern utilities: algebric operations
//------------------------------------------------------------------------------
Point operator+(const Point& p)
{ return p; }

Point operator-(const Point& p)
{ return -1 * p; }

Point operator+(const Point& p, const Point& q)
{
  Point r(p);
  return r += q;
}

Point operator-(const Point& p, const Point& q)
{
  Point r(p);
  return r -= q;
}

Point operator+(const Point& p, const real_t s)
{
  Point r(p);
  return r += Point(std::vector<real_t>(p.size(), s));
}

Point operator-(const Point& p, const real_t s)
{
  Point r(p);
  return r -= Point(std::vector<real_t>(p.size(), s));
}

Point operator+(const real_t s, const Point& p)
{ return p + s; }

Point operator-(const real_t s, const Point& p)
{ return -(p - s); }

Point operator*(const real_t s, const Point& p)
{
  Point r(p);
  return r *= s;
}

Point operator*(const Point& p, const real_t s)
{
  Point r(p);
  return r *= s;
}

Point operator/(const Point& p, const real_t s)
{
  Point r(p);
  return r /= s;
}

real_t pointDistance(const Point& p, const Point& q)
{ return p.distance(q); }

real_t squareDistance(const Point& p, const Point& q)
{ return p.squareDistance(q); }

//Extern utilities: comparison operations
bool operator==(const Point& p, const Point& q)
{
  if (p.size() != q.size()) { q.dimError("Point::operator==", p.size()); }
  cit_vr itp = p.begin(), itq = q.begin();
  if(Point::tolerance==0.) // tolerance
  {
    while (itp != p.end())
    {
      if (*itp != *itq) return false;
      itp++; itq++;
    }
    return true;
  }
  while (itp != p.end())
  {
   if (*itp < *itq - Point::tolerance) return false;
   if (*itp > *itq + Point::tolerance) return false;
   itp++; itq++;
  }
  return true;
}

bool operator!=(const Point& p, const Point& q)
{ return !(p == q); }

bool operator< (const Point& p, const Point& q)
{
  if (p.size() != q.size()) { q.dimError("Point::operator<", p.size()); }
  cit_vr itp = p.begin(), itq = q.begin();
  if(Point::tolerance==0.) // no tolerance
  {
    while (itp != p.end())
    {
      if (*itp < *itq) return true;
      if (*itp > *itq) return false;
      itp++; itq++;
    }
    return false;
  }
  while (itp != p.end())
  {
   if (*itp < *itq - Point::tolerance) return true;
   if (*itp > *itq + Point::tolerance) return false;
   itp++; itq++;
  }
  return false;
}

bool operator<=(const Point& p, const Point& q)
{ return (p < q) || (p == q); }

bool operator> (const Point& p, const Point& q)
{ return !(p <= q); }

bool operator>=(const Point& p, const Point& q)
{ return !(p < q); }

//------------------------------------------------------------------------------
// functions used by KdTree<Point>
//------------------------------------------------------------------------------
int_t compare(const Point& P, const real_t& s, int c)
{
  real_t v=P[c-1];
  if (v>s) return 1;
  if (v<s) return -1;
  return 0;
}

int_t maxSeparator(const Point& p, const Point& q, int& c, real_t& s)
{
  real_t l=0.;
  number_t j=0;
  for (number_t i=0; i<p.size(); i++)
    {
      real_t a=std::abs(p[i]-q[i]);
      if (a>l) {j=i; l=a;}
    }
  c=j+1;
  s=(p[j]+q[j])/2;
  if (p[j]>q[j]) return 1;
  else return -1;
}

real_t dot(const Point& p, const Point& q)
{
  real_t res=0.;
  dimen_t d=std::min(p.dim(),q.dim());
  for (dimen_t i=0; i<d; ++i) { res+=p[i]*q[i]; }
  return res;
}

real_t norm2(const Point& p)
{
  return std::sqrt(dot(p,p));
}

//cross product in 3D and 2D (return a 3D point)
Point crossProduct(const Point& p, const Point& q)
{
  dimen_t s=p.dim();
  if (s != q.dim()) error("diff_pts_size","crossProduct",s,q.dim());
  if (s<=1 || s>3) error("diff_pts_size","crossProduct",s,q.dim());
  Point m(0., 0., 0.);
  m[2]=p[0]*q[1]-p[1]*q[0];
  if (p.size() == 3)
    {
      m[0]=p[1]*q[2]-p[2]*q[1];
      m[1]=p[2]*q[0]-p[0]*q[2];
    }
  return m;
}

// special 2D cross product OA x OB
real_t crossProduct2D(const Point &O, const Point &A, const Point &B)
{
	return (A[0] - O[0]) * (B[1] - O[1]) - (A[1] - O[1]) * (B[0] - O[0]);
}


//mixed product (p x q).r in 3D and 2D (return a real)
real_t mixedProduct(const Point& p, const Point& q, const Point& r)
{
  dimen_t s=p.dim();
  if (s != q.dim()) error("diff_pts_size","mixedProduct (p x q).r",s,q.dim());
  if (s<=1 || s>3) error("diff_pts_size","mixedProduct (p x q).r",s,q.dim());
  return dot(crossProduct(p,q),r);
}

real_t dist(const Point& p, const Point& q)
{ return p.distance(q); }

/*!< returns Point as Point in polar coordinates/cylindrical (r,theta,z)
     r=sqrt(x*x+y*y)  theta = atan2(y,x) in ]-pi,pi], z unchanged
     reverse map: x= r cos(theta)  y = r sin(theta)  z= z
*/
Point toPolar(const Point& P)
{
    Point Q=P;  //copy P to keep original point dimension
    Q(1)=norm2(P);
    Q(2)=std::atan2(P(2),P(1));
    return Q;
}

/*!< returns Point as Point in spherical coordinates (r,theta,phi)
   r=sqrt(x*x+y*y+z*z)  azymuth theta = atan2(y,x) in ]-pi,pi],  elevation phi = asin(z/r) in [-pi/2,pi/2]
   reverse map: x= r cos(theta) cos(phi), y = r sin(theta) cos(phi), z = r sin(phi)
*/
Point toSpherical(const Point& P)
{
    Point Q=P;  //copy P to keep original point dimension
    Q(1)=norm2(P);
    Q(2)=std::atan2(P(2),P(1));
    Q(3)=0;
    if (std::abs(Q(1))>theEpsilon) Q(3)=std::asin(P(3)/Q(1));
    return Q;
}
/*!< returns (r,theta) coordinates of a point related to an ellipse (C center, A1,A2, the two apogees)
       P-C = r(A1-C)) cos(theta) + r(A2-C) sin(theta)
       r >=0 , azymuth theta in ]-pi,pi]
   NB: if C=(0,0), A1=(1,0), A2=(0,1) then it gives the usual polar coordinates
        r=1 on the boundary of the ellipse
*/
Point toEllipticCoordinates(const Point& P, const Point& C, const Point& A1, const Point& A2)
{
  Point O=force3D(C);
  Point P1=force3D(A1)-O, P2=force3D(A2)-O, Q=force3D(P)-O;
  Point u1 =orthogonalPoint3D(P2), u2=orthogonalPoint3D(P1);
  real_t d1=dot(P1,u1), d2=dot(P2,u2);  // dot(Pi,ui)=0 => degenerated ellipsoid
  if (sqrt(d1*d1+d2*d2)<theTolerance)  error("free_error","degenarated ellipsoid in toEllipticCoordinates");
  real_t sp1=dot(Q,u1)/dot(P1,u1), sp2=dot(Q,u2)/dot(P2,u2);
  real_t r=sqrt(sp1*sp1+sp2*sp2);
  if (std::abs(r)<theTolerance) return Point(0.,0.);
  real_t theta=std::atan2(sp2/r,sp1/r);
  if (std::abs(theta+pi_)<theTolerance) theta=pi_;         // force -pi+-tol to pi
  if (std::abs(theta)<theTolerance) theta=std::abs(theta); // force +-tol to 0
  return Point(r,theta);
}

/*!< returns (r,theta,phi) coordinates of a point related to an ellipsoid (C center, A1,A2,A3 the three apogees)
       P-C = r(A1-C)) cos(theta) cos(phi) + r(A2-C) sin(theta) cos(phi) + r(A3-C) sin(phi)
   r >=0 , azymuth theta in ]-pi,pi],  elevation phi in [-pi/2,pi/2]
   NB: if C=(0,0,0), A1=(1,0,0), A2=(0,1,0), A3=(0,0,1) then it gives the usual sperical coordinates
        r=1 on the boundary of the ellisoid
*/
Point toEllipticCoordinates(const Point& P, const Point& C, const Point& A1, const Point& A2, const Point& A3)
{
  Point P1=A1-C, P2=A2-C, P3=A3-C, Q=P-C;
  Point u1 = crossProduct(P2,P3), u2=crossProduct(P1,P3), u3=crossProduct(P1,P2);
  real_t d1=dot(P1,u1), d2=dot(P2,u2), d3=dot(P3,u3);  // dot(Pi,ui)=0 => degenerated ellipsoid
  if (sqrt(d1*d1+d2*d2+d3*d3)<theTolerance)  error("free_error","degenarated ellipsoid in toEllipticCoordinates");
  real_t sp1=dot(Q,u1)/dot(P1,u1), sp2=dot(Q,u2)/dot(P2,u2), sp3=dot(Q,u3)/dot(P3,u3);
  real_t r=sqrt(sp1*sp1+sp2*sp2+sp3*sp3);
  if (std::abs(r)<theTolerance) return Point(0.,0.,0.);
  real_t phi= std::asin(sp3/r);
  if (std::abs(phi)<theTolerance) phi=std::abs(phi); // force +-tol to 0
  real_t cp=std::cos(phi);
  if (std::abs(cp)<theTolerance) return Point(r,0.,phi);  // third apogee (any theta is available)
  real_t ct=sp1/(cp*r), st=sp2/(cp*r);
  real_t theta=std::atan2(st,ct);
  if (std::abs(theta+pi_)<theTolerance) theta=pi_;         // force -pi+-tol to pi
  if (std::abs(theta)<theTolerance) theta=std::abs(theta); // force +-tol to 0
  return Point(r,theta,phi);
}

/*! returns (r,theta) coordinates of a point related to an a centered orthogonal ellipse with radius r1,r2
    calls general case (not the fastest method)
*/
Point toEllipticCoordinates(const Point& P, real_t r1, real_t r2)
{
    return toEllipticCoordinates(P,Point(0.,0.), Point(r1,0.),Point(0.,r2));
}

/*! returns (r,theta,phi) coordinates of a point related to an a centered orthogonal ellipsoid with radius r1,r2,r3
    calls general case (not the fastest method)
*/
Point toEllipticCoordinates(const Point& P, real_t r1, real_t r2, real_t r3)
{
  return toEllipticCoordinates(P,Point(0.,0.,0.), Point(r1,0.,0.),Point(0.,r2,0.),Point(0.,0.,r3));
}

/*! return barycentric coordinates of a point M related to segment (S1,S2), to triangle (S1,S3,S3), to tetrahedron (S1,S2,S3,S4)
    works in 2D, 3D,
    does not require that M belongs to line (S1,S2) or plane (S1,S2,S3)
    in that case, it gives the barycentric coordinates of the orthogonal projection of M on line or plane
    if fails (degenerated simplex), return a void Point
*/
Point toBarycentricNocheck(const Point& M, const Point& S1, const Point& S2, const Point& S3, const Point& S4)
{
  if (S3.size()==0) // segment
  {
     Point S1S2=S2-S1;
     real_t n=dot(S1S2,S1S2);
     if (n<theTolerance) return Point();
     real_t l=dot(M-S1,S1S2)/n;
     return Point(l,1-l);
  }
  if (S4.size()==0) //triangle
  {
     Point S12=S2-S1, S13=S3-S1, S1M=M-S1;
     real_t a=dot(S12,S12), b=dot(S13,S13), c=dot(S12,S13), f=dot(S12,S1M), g=dot(S13,S1M);
     real_t s=a*b-c*c;
     if (std::abs(s)<theEpsilon) return Point();
     real_t l2=(b*f-c*g)/s, l3=(a*g-f*c)/s;
     return Point(1-l2-l3, l2, l3);
  }
  //tetahedron
  Point S12=S2-S1, S13=S3-S1, S14=S4-S3, S1M=M-S1;
  Point n = cross3D(S13,S14);
  real_t d = dot(n,S12);
  if (std::abs(d)<theTolerance) return Point();
  Point B; B.resize(4);
  B[0]=1;
  B[1]=dot(S1M,n)/d; B[0]-=B[1];
  n = cross3D(S12,S14); B[2]=dot(S1M,n)/dot(S13,n); B[0]-=B[2];
  n = cross3D(S12,S13); B[3]=dot(S1M,n)/dot(S14,n); B[0]-=B[3];
  return B;
}

/*!return barycentric coordinates of a point M related to segment (S1,S2), to triangle (S1,S3,S3), to tetrahedron (S1,S2,S3,S4)
  checking degenerancy of simplex
*/
Point toBarycentric(const Point& M, const Point& S1, const Point& S2, const Point& S3, const Point& S4)
{
  Point B=toBarycentricNocheck(M,S1,S2,S3,S4);
  if (B.size()==0)
  {
    if (S3.size()==0) error("degenerated_elt",words("shape",_segment));
    if (S4.size()==0) error("degenerated_elt",words("shape",_triangle));
    error("degenerated_elt",words("shape",_tetrahedron));
  }
  return B;
}

//! test if 4 points are coplanar
bool arePointsCoplanar(const Point& p1, const Point& p2, const Point& p3, const Point& p4, real_t tol)
{
  if (p1.distance(p2) < tol || p1.distance(p3) < tol || p1.distance(p4) < tol ) { return true; }
  if (p2.distance(p3) < tol || p2.distance(p4) < tol || p3.distance(p4) < tol ) { return true; }
  real_t mi=dot(crossProduct(p2-p1,p3-p1),force3D(p4-p1));
  if (std::abs(mi) > tol) { return false; }
  return true;
}

//! test if a set of points is coplanar
bool arePointsCoplanar(const std::vector<Point>& p, real_t tol)
{
  if (p.size() < 4) return true;
  for (number_t i=3; i < p.size(); ++i)
    {
      if (!arePointsCoplanar(p[0], p[1], p[2], p[i], tol)) return false;
    }
  return true;
}

//! test if P belongs to [A,B], works in 2D-3D, assuming same point dimensions
bool isPointInSegment(const Point& P, const Point& A, const Point& B, real_t tol)
{
  Point AB=B-A, AP=P-A;
  if (norm2(crossProduct(AB,AP))>tol) return false;  //not aligned
  real_t a=dot(AP,AB)/dot(AB,AB);
  return (a>=-tol && a<=1+tol);
}

bool isPointInTriangle(const Point& P, const Point& A, const Point& B, const Point& C, real_t tol)
{
  if (P.size()==2) //2D case
  {
    real_t x=P[0], y=P[1], xA=A[0], yA=A[1], xB=B[0], yB=B[1], xC=C[0], yC=C[1];
    bool bAB = (x-xB)*(yA-yB)-(y-yB)*(xA-xB) <= tol;
    bool bBC = (x-xC)*(yB-yC)-(y-yC)*(xB-xC) <= tol;
    bool bCA = (x-xA)*(yC-yA)-(y-yA)*(xC-xA) <= tol;
    return bAB==bBC && bAB==bCA;
  }
  //3D case
   if (!arePointsCoplanar(P,A,B,C,tol)) return false;
   Point AB=B-A, AC=C-A, AP=P-A, n=cross3D(AB,AC);
   if (norm(n)<tol) error("free_error","degenerated triangle in isPointInTriangle");
   Point w=cross3D(AB,n);
   real_t a=dot(AP,w)/dot(AC,w);
   if (a<-tol || a>1+tol) return false;
   w=cross3D(AC,n);
   a=dot(AP,w)/dot(AB,w);
   if (a<-tol || a>1+tol) return false;
   return true;
}

//----------- projections ------------//

/*!
  orthogonal projection of a point M on a straight line defined by 2 points (A and B)
  returns the projected point P and the distance h=MP
 */
Point projectionOnStraightLine(const Point& M, const Point& A, const Point& B, real_t& h, bool only3D)
{
  if (A.size() != B.size()) { error("diff_pts_size","projectionOnStraightLine",A.size(),B.size()); }
  if (A.size() != M.size()) { error("diff_pts_size","projectionOnStraightLine",A.size(),M.size()); }
  if (A.size() != 3 && only3D) { error("3D_only","projectionOnStraightLine"); }
  Point V=B-A;
  real_t nV=norm2(V);
  nV=1./(nV*nV);
  Point Tmp=M-A;
  real_t j=dot(V,Tmp)*nV;
  Point P=j*V+A;
  for (dimen_t l=0; l<A.size(); l++) { if (std::fabs(P[l])<theEpsilon) { P[l]=0.; } }
  h=norm2(M-P);
  return P;
}

/*!
  orthogonal projection of point M on plane defined by 3 non aligned given points (P1, P2, and P3)
  returns the projected point P and the distance h between M and the plane
 */
Point projectionOfPointOnPlane(const Point& M, const Point& P1, const Point& P2, const Point& P3, real_t& h, bool only3D)
{
  if (M.size() != P1.size()) { error("diff_pts_size","projectionOfPointOnPlane", M.size(), P1.size()); }
  if (M.size() != P2.size()) { error("diff_pts_size","projectionOfPointOnPlane", M.size(), P2.size()); }
  if (M.size() != P3.size()) { error("diff_pts_size","projectionOfPointOnPlane", M.size(), P3.size()); }
  if (M.size() != 3 && only3D) { error("3D_only","projectionOfPointOnPlane"); }
  Point N=crossProduct(P1-P2,P1-P3);
  N*=(1./norm2(N));
  real_t k=dot(P1-M,N);
  h=std::abs(k);
  return M+k*N;
}

/*!
  orthogonal projection of segment defined by the points T1 and T2 on the parallel plane
  defined by the 3 non aligned given points S1, S2, and S3
  returns the projected points and the distance h between the segment and the plane
*/
std::pair<Point,Point> projectionOfSegmentOnPlane(const Point& S1, const Point& S2, const Point& P1, const Point& P2, const Point& P3, real_t& h)
{
  if (S1.size() != S2.size()) { error("diff_pts_size","projectionOfSegmentOnPlane", S1.size(), S2.size()); }
  if (S1.size() != P1.size()) { error("diff_pts_size","projectionOfSegmentOnPlane", S1.size(), P1.size()); }
  if (S1.size() != P2.size()) { error("diff_pts_size","projectionOfSegmentOnPlane", S1.size(), P2.size()); }
  if (S1.size() != P3.size()) { error("diff_pts_size","projectionOfSegmentOnPlane", S1.size(), P3.size()); }
  if (S1.size() != 3) { error("3D_only","projectionOfSegmentOnPlane"); }
  Point N=crossProduct(P1-P2,P1-P3);
  N=(1./norm2(N))*N;
  real_t k=dot(P1-S1,N);
  h=std::abs(k);
  return std::make_pair(S1+k*N,S2+k*N);
}

/*!
  orthogonal projection of triangle defined by the points T1, T2 and T3 on the parallel plane
  defined by the 3 non aligned given points P1, P2, and P3
  returns the projected points and the distance h between the triangle and the plane
*/
std::vector<Point> projectionOfTriangleOnPlane(const Point& T1, const Point& T2, const Point& T3, const Point& P1, const Point& P2, const Point& P3, real_t& h)
{
  if (T1.size() != T2.size()) { error("diff_pts_size","projectionOfTriangleOnPlane", T1.size(), T2.size()); }
  if (T1.size() != T3.size()) { error("diff_pts_size","projectionOfTriangleOnPlane", T1.size(), T3.size()); }
  if (T1.size() != P1.size()) { error("diff_pts_size","projectionOfTriangleOnPlane", T1.size(), P1.size()); }
  if (T1.size() != P2.size()) { error("diff_pts_size","projectionOfTriangleOnPlane", T1.size(), P2.size()); }
  if (T1.size() != P3.size()) { error("diff_pts_size","projectionOfTriangleOnPlane", T1.size(), P3.size()); }
  if (T1.size() != 3) { error("3D_only","projectionOfTriangleOnPlane"); }
  Point N=crossProduct(P1-P2,P1-P3);
  N=(1./norm2(N))*N;
  real_t k=dot(P1-T1,N);
  h=std::abs(k);
  std::vector<Point> res;
  res.push_back(T1+k*N);
  res.push_back(T2+k*N);
  res.push_back(T3+k*N);
  return res;
}

/*!
  projection (minimal distance) of a point M on segment [AB]
  returns the projected point P and the distance h=MP
 */
Point projectionOnSegment(const Point& M, const Point& A, const Point& B, real_t & h)
{
  if (A.size() != M.size()) { error("diff_pts_size","projectionOnSegment",A.size(),M.size()); }
  Point AB=B-A, AM=M-A;
  real_t lambda=dot(AB,AM)/dot(AB,AB);
  if (lambda < theEpsilon) lambda=0.;  //project on segment
  if (lambda> 1-theEpsilon) lambda=1.;
  Point P=A+lambda*AB;
  h=norm2(M-P);
  return P;
}

/*!
  projection (minimal distance) of point M on triangle given vertices T1, T2, and T3
  returns the projected point P and the distance h=MP (2D-3D)
 */
Point projectionOnTriangle(const Point& M, const Point& T1, const Point& T2, const Point& T3, real_t& h)
{
  if (M.size() != T1.size()) { error("diff_pts_size","projectionOfPointOnTriangle", M.size(), T1.size()); }
  h=0.;
  //compute barycentric coordinates of M
  Point T12=T2-T1, T13=T3-T1, T1M=M-T1;
  real_t a=dot(T12,T12), b=dot(T13,T13), c=dot(T12,T13), f=dot(T12,T1M), g=dot(T13,T1M);
  real_t s=a*b-c*c;
  if (std::abs(s)<theEpsilon) error("degenerated_elt",words("shape",_triangle));
  real_t lambda2=(b*f-c*g)/s, lambda3=(a*g-f*c)/s, lambda1=1.-lambda2-lambda3;
  //compute projection
  if (lambda1 < 0.)
  {
    if (lambda2 <0.) {h=norm2(M-T3); return T3;}
    if (lambda3 <0.) {h=norm2(M-T2); return T2;}
    return projectionOnSegment(M, T2, T3, h);
  }
  if (lambda2 < 0.)
  {
    if (lambda3 <0.) {h=norm2(M-T1); return T1;}
    return projectionOnSegment(M, T1, T3, h);
  }
  if (lambda3 < 0.) return projectionOnSegment(M, T1, T2, h);
  Point P=lambda1*T1+lambda2*T2+lambda3*T3;  //when M is outside the triangle plane
  h=norm2(M-P);
  return P;
}

/*!
  projection (minimal distance) of point M on quadrangle given by vertices Q1, Q2, Q3, Q4 (assuming Q1, Q2, Q3, Q4 are coplanar)
  returns the projected point P and the distance h=MP (2D-3D)
 */
Point projectionOnQuadrangle(const Point& M, const Point& Q1, const Point& Q2, const Point& Q3, const Point& Q4, real_t& h)
{
    Point P = projectionOnTriangle(M, Q1, Q2, Q3, h);
    if (h==0.) return P;
    real_t h2=0.;
    Point Q = projectionOnTriangle(M, Q1, Q3, Q4, h2);
    if (h2>h) return P;
    h=h2;
    return Q;
}

/*!
  projection (minimal distance) of point M on tetrahedron given vertices T1, T2, T3 and T4
  returns the projected point P and the distance h=MP (3D)
*/
Point projectionOnTetrahedron(const Point& M, const Point& T1, const Point& T2, const Point& T3, const Point& T4, real_t& h)
{
  if (M.size() != T1.size()) { error("diff_pts_size","projectionOfPointOnTriangle", M.size(), T1.size()); }
  h=0.;
  // compute barycentric coordinates
  Point S12=T2-T1, S13=T3-T1, S14=T4-T1, S1M=M-T1;
  Point c=cross3D(S13,S14);
  real_t l2=dot(S1M,c)/dot(S12,c);
  c = cross3D(S12,S14);
  real_t l3=dot(S1M,c)/dot(S13,c);
  c = cross3D(S12,S13);
  real_t l4=dot(S1M,c)/dot(S14,c);
  real_t l1=1-l2-l3-l4;
  // compute projection
  if (l1<0.)
  {
    if (l2<0.)
    {
      if (l3<0.) {h=norm2(M-T4); return T4;}
      if (l4<0.) {h=norm2(M-T3); return T3;}
      return projectionOnSegment(M, T3, T4, h);
    }
    // l1<0 l2>0
    if (l3<0.)
    {
      if (l4<0.) {h=norm2(M-T2); return T2;}
      return projectionOnSegment(M, T2, T4, h);
    }
    // l1<0 l2>0 l3>0
    if (l4<0.) return projectionOnSegment(M, T2, T3, h);
    return projectionOnTriangle(M, T2, T3, T4, h);
  }
  // l1>0
  if (l2<0.)
  {
    if (l3<0.)
    {
      if (l4<0.) {h=norm2(M-T1); return T1;}
      return projectionOnSegment(M, T1, T4, h);
    }
    // l1>0 l2<0 l3>0
    if (l4<0.) return projectionOnSegment(M, T1, T3, h);
    return projectionOnTriangle(M, T1, T3, T4, h);
  }
  // l1 >0 l2>0
  if (l3<0.)
  {
    if (l4<0.) return projectionOnSegment(M, T1, T2, h);
    return projectionOnTriangle(M, T1, T2, T4, h);
  }
  // l1 >0 l2>0 l3 >0
  if (l4<0.) return projectionOnTriangle(M, T1, T2, T3, h);
  return M;
}

//----------- normals ------------//

/*!
  computation of outward normals to the triangle whose vertices are T1, T2 and T3
  normals[0] : outward normal of [T2T3]
  normals[1] : outward normal of [T1T3]
  normals[2] : outward normal of [T1T2]
*/
std::vector< std::vector<real_t> > outwardNormalsOfTriangle(const Point& T1, const Point& T2, const Point& T3)
{
  if (T1.size() != T2.size()) { error("diff_pts_size","outwardNormalsOfTriangle", T1.size(), T2.size()); }
  if (T1.size() != T3.size()) { error("diff_pts_size","outwardNormalsOfTriangle", T1.size(), T3.size()); }
  if (T1.size() != 3) { error("3D_only","outwardNormalsOfTriangle"); }
  real_t h=0.;
  Point I1=projectionOnStraightLine(T1, T2, T3, h);
  Point I2=projectionOnStraightLine(T2, T3, T1, h);
  Point I3=projectionOnStraightLine(T3, T1, T2, h);
  std::vector< std::vector<real_t> > normals;
  real_t n=(1./norm2(I1-T1));
  normals.push_back(n*(I1-T1));
  n=(1./norm2(I2-T1));
  normals.push_back(n*(I2-T2));
  n=(1./norm2(I3-T3));
  normals.push_back(n*(I3-T3));
  return normals;
}

//----------- distances ------------//

//! signed distances of a point M to the three edges of the triangle whose vertices are T1, T2 and T3
std::vector<real_t> signedDistancesToTriangleEdges(const Point& M, const Point& T1, const Point& T2, const Point& T3)
{
  if (M.size() != T1.size()) { error("diff_pts_size","signedDistancesToTriangleEdges", M.size(), T1.size()); }
  if (M.size() != T2.size()) { error("diff_pts_size","signedDistancesToTriangleEdges", M.size(), T2.size()); }
  if (M.size() != T3.size()) { error("diff_pts_size","signedDistancesToTriangleEdges", M.size(), T3.size()); }
  if (M.size() != 3) { error("3D_only","signedDistancesToTriangleEdges"); }
  std::vector<real_t> D(3,0.);
  // Calculation of the outgoint normal at each edge
  std::vector< std::vector<real_t> > res=outwardNormalsOfTriangle(T1,T2,T3);
  // calculation of the distances
  Point Tmp=T2-M;
  D[0]=std::inner_product(Tmp.begin(), Tmp.end(),res[0].begin(),0.);
  Tmp=T3-M;
  D[1]=std::inner_product(Tmp.begin(), Tmp.end(),res[1].begin(),0.);
  Tmp=T1-M;
  D[2]=std::inner_product(Tmp.begin(), Tmp.end(),res[2].begin(),0.);
  return D;
}

/*!
  lengths of edges of the triangle whose vertices are T1, T2 and T3
  first edge is T2T3, second height T1T3 and last height T1T2
*/
std::vector<real_t> triangleEdgesLengths(const Point& T1, const Point& T2, const Point& T3)
{
  std::vector<real_t> L(3,0.);
  L[0]=norm2(T3-T2); L[1]=norm2(T1-T3); L[2]=norm2(T2-T1);
  return L;
}

/*!
  lengths of heights of the triangle whose vertices are T1, T2 and T3
  first height is from T1, second height from T2 and last height from T3
*/
std::vector<real_t> triangleHeightsLengths(const Point& T1, const Point& T2, const Point& T3)
{
  Point Itmp(0., 0., 0.);
  std::vector<real_t> D(3,0.);
  Itmp=projectionOnStraightLine(T1, T2, T3, D[0]);
  Itmp=projectionOnStraightLine(T2, T3, T1, D[1]);
  Itmp=projectionOnStraightLine(T3, T1, T2, D[2]);
  return D;
}

//----------- intersections ------------//

//! determines if 2 2D segments [PQ] and [AB] have a unique intersection or not
bool doesSegmentCrossesSegment2D(const Point& P, const Point& Q, const Point& A, const Point& B, real_t tol)
{
  int o1 = pointOrientation2D(P,Q,A,tol), o2 = pointOrientation2D(P,Q,B,tol);
  int o3 = pointOrientation2D(A,B,P,tol), o4 = pointOrientation2D(A,B,Q,tol);
  if (o1 != o2 && o3 != o4)  return true;                     //general case
  if (o1 == 0 && isPointInSegment(A,P,Q,tol))  return true;   // P,Q and B are colinear and A lies on segment PQ
  if (o2 == 0 && isPointInSegment(B,P,Q,tol))  return true;   // P,Q and B are colinear and B lies on segment PQ
  if (o3 == 0 && isPointInSegment(P,A,B,tol))  return true;   // A,B and P are colinear and P lies on segment AB
  if (o4 == 0 && isPointInSegment(Q,A,B,tol))  return true;   // A,B and Q are colinear and Q lies on segment AB
  return false;
}

//! determines if 2 segments [PQ] and [AB] have a unique intersection or not (2D or 3D)
bool doesSegmentCrossesSegment(const Point& P, const Point& Q, const Point& A, const Point& B, real_t tol)
{
  if (P.size()==2) return doesSegmentCrossesSegment2D(P,Q,A,B,tol);
  Point I; return intersectionOfSegments(P,Q,A,B,I,tol);
}

//! determines if a segment [PQ] and a triangle ABC have a unique intersection or not
bool doesSegmentCrossesTriangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, real_t tol)
{
  if (doesSegmentCrossesSegment(P,Q,A,B,tol)) return true;
  if (doesSegmentCrossesSegment(P,Q,B,C,tol)) return true;
  return doesSegmentCrossesSegment(P,Q,C,A,tol);
}

//! determines if a segment [PQ] and a quadrangle ABCD have a unique intersection or not
bool doesSegmentCrossesQuadrangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, const Point& D, real_t tol)
{
  if (!arePointsCoplanar(A,B,C,D,tol)) return false;
  if (doesSegmentCrossesTriangle(P,Q,A,B,C,tol)) return true;
  if (doesSegmentCrossesTriangle(P,Q,A,C,D,tol)) return true;
  return false;
}

//! determines if segments [PQ] and [AB] have a intersection or not
bool doesSegmentIntersectsTriangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, real_t tol)
{
  if (isSegmentInTriangle(P,Q,A,B,C,tol)) return true;
  return doesSegmentCrossesTriangle(P,Q,A,B,C,tol);
}

//! determines if a segment [PQ] and a triangle ABC have a intersection or not
bool doesSegmentIntersectsQuadrangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, const Point& D, real_t tol)
{
  if (doesSegmentIntersectsTriangle(P,Q,A,B,C,tol)) return true;
  return doesSegmentIntersectsTriangle(P,Q,A,C,D,tol);
}

//! determines if triangles PQR and ABC have a intersection or not
bool doesTriangleIntersectsTriangle(const Point& P, const Point& Q, const Point& R, const Point& A, const Point& B, const Point& C, real_t tol)
{
  if (isTriangleInTriangle(P,Q,R,A,B,C,tol) || isTriangleInTriangle(A,B,C,P,Q,R,tol)) return true;
  if (doesSegmentCrossesTriangle(P,Q,A,B,C,tol) || doesSegmentCrossesTriangle(Q,R,A,B,C,tol)) return true;
  return doesSegmentCrossesTriangle(P,R,A,B,C,tol);
}

//! determines if a triangle PQR and a quadrangle ABCD have a intersection or not
bool doesTriangleIntersectsQuadrangle(const Point& P, const Point& Q, const Point& R, const Point& A, const Point& B, const Point& C, const Point& D, real_t tol)
{
  if (doesTriangleIntersectsTriangle(P,Q,R,A,B,C,tol)) return true;
  return doesTriangleIntersectsTriangle(P,Q,R,A,C,D,tol);
}

//! determines if quadrangles PQRS and ABCD have a intersection or not
bool doesQuadrangleIntersectsQuadrangle(const Point& P, const Point& Q, const Point& R, const Point& S, const Point& A, const Point& B, const Point& C, const Point& D, real_t tol)
{
  if (!arePointsCoplanar(P,Q,R,S,tol) || !arePointsCoplanar(A,B,C,D,tol)) return false;
  if (doesTriangleIntersectsQuadrangle(P,Q,R,A,B,C,D,tol))  return true;
  return doesTriangleIntersectsQuadrangle(P,R,S,A,B,C,D,tol);
}

/*!
  intersection point I of straight lines (S1S2) and (T1T2)
  if (S1S2) // (T1T2), I is of size 0 and hasIntersect is set to false
  else hasIntersect is set to true
*/
Point intersectionOfStraightLines(const Point& S1, const Point& S2, const Point& T1, const Point& T2, bool& hasIntersect)
{
  hasIntersect=true;
  if (S1.size() != S2.size()) { error("diff_pts_size","intersectionOfStraightLines",S1.size(),S2.size()); }
  if (S1.size() != T1.size()) { error("diff_pts_size","intersectionOfStraightLines",S1.size(),T1.size()); }
  if (S1.size() != T2.size()) { error("diff_pts_size","intersectionOfStraightLines",S1.size(),T2.size()); }
  // if (S1.size() != 3) { error("3D_only","intersectionOfStraightLines"); }
  if (norm2(S1-T1) < 2.*theEpsilon || norm2(S1-T2) < 2.*theEpsilon ) return S1;
  else if (norm2(S2-T1) < 2.*theEpsilon || norm2(S2-T2) < 2.*theEpsilon) return S2;
  else
  {
    Point alpha=S2-S1, beta=T2-T1;
    real_t n1=norm2(alpha), n2=norm2(beta), j=dot(alpha,beta);
    if ((1.0-std::fabs(j/(n1*n2))) < 2.*theEpsilon)
    {
      hasIntersect=false;
      return Point(std::vector<real_t>(S1.size()));
    }
    else
    {
      Point Tau=S1-T1;
      real_t p=(n1*n1)*(n2*n2)-j*j,
             s=(j*dot(Tau,beta) - dot(Tau,alpha)*(n2*n2))/p,
             t=(n1*n1*dot(Tau,beta) - j*dot(Tau,alpha))/p;
      Point I=0.5*(S1+s*alpha+T1+t*beta);
      return I;
    }
  }
}

/*!
  straight line intersection I of 2 planes (S1,S2,S3) and (T1,T2,T3)
  returns a pair of points defining the intersection
*/
std::pair<Point,Point> intersectionOfPlanes(const Point& S1, const Point& S2, const Point& S3,
    const Point& T1, const Point& T2, const Point& T3)
{
  if (S1.size() != S2.size()) { error("diff_pts_size","intersectionOfPlanes",S1.size(),S2.size()); }
  if (S1.size() != S3.size()) { error("diff_pts_size","intersectionOfPlanes",S1.size(),S3.size()); }
  if (S1.size() != T1.size()) { error("diff_pts_size","intersectionOfPlanes",S1.size(),T1.size()); }
  if (S1.size() != T2.size()) { error("diff_pts_size","intersectionOfPlanes",S1.size(),T2.size()); }
  if (S1.size() != T3.size()) { error("diff_pts_size","intersectionOfPlanes",S1.size(),T3.size()); }
  if (S1.size() != 3) { error("3D_only","intersectionOfPlanes"); }
  std::vector<real_t> pT=eqtOfPlane(T1, T2, T3);
  std::vector<real_t> pS=eqtOfPlane(S1, S2, S3);
  Point pt(0., 0., 0.);
  pt[0]=pT[0]; pt[1]=pT[1]; pt[2]=pT[2];
  Point ps(0., 0., 0.);
  ps[0]=pS[0]; ps[1]=pS[1]; ps[2]=pS[2];
  Point U=crossProduct(ps,pt);
  real_t nu=norm2(U);
  U*=1./(nu*nu);
  Point v(nu*U);
  Point Tmp(pT[3]*ps - pS[3]*pt);
  Point I=crossProduct(Tmp,U);
  real_t h=0.;
  Point I1=projectionOnStraightLine(S1,I,I+v,h);
  Point I2=I1+v;
  for (int l=0; l<3; l++)
    {
      if (std::abs(I1[l])<theEpsilon) { I1[l]=0.; }
      if (std::abs(I2[l])<theEpsilon) { I2[l]=0.; }
    }
  return std::make_pair(I1,I2);
}

/*!
  straight line intersection of 2 planes (S1,S2,S3) and (S1,T2,T3)
  S1 is on the intersection, so the remaining calculations are shorter
  returns a point defining the intersection with S1
*/
Point intersectionOfPlanesWithOneSharedPoint(const Point& S1, const Point& S2, const Point& S3, const Point& T2, const Point& T3)
{
  if (S1.size() != S2.size()) { error("diff_pts_size","intersectionOfPlanesWithOneSharedPoint",S1.size(),S2.size()); }
  if (S1.size() != S3.size()) { error("diff_pts_size","intersectionOfPlanesWithOneSharedPoint",S1.size(),S3.size()); }
  if (S1.size() != T2.size()) { error("diff_pts_size","intersectionOfPlanesWithOneSharedPoint",S1.size(),T2.size()); }
  if (S1.size() != T3.size()) { error("diff_pts_size","intersectionOfPlanesWithOneSharedPoint",S1.size(),T3.size()); }
  if (S1.size() != 3) { error("3D_only","intersectionOfPlanesWithOneSharedPoint"); }
  std::vector<real_t> pT=eqtOfPlane(S1, T2, T3);
  std::vector<real_t> pS=eqtOfPlane(S1, S2, S3);
  Point pt(0., 0., 0.);
  pt[0]=pT[0]; pt[1]=pT[1]; pt[2]=pT[2];
  Point ps(0., 0., 0.);
  ps[0]=pS[0]; ps[1]=pS[1]; ps[2]=pS[2];
  Point U=crossProduct(ps,pt);
  real_t nu=norm2(U);
  U*=1./(nu);
  return S1+U;
}

/*!
  intersection of half line [M,d) with segment [AB]
  return void Point if no intersection or non unique point
  assume 3D points, A!=B, d!=0
*/
Point intersectionHalfLineSegment(const Point& M, const std::vector<real_t>& d, const Point& A, const Point& B, bool& hasUniqueIntersection, real_t tol)
{
  real_t b;
  Point AB=B-A, AM=M-A;
  Point n=cross3D(d,AB);
  hasUniqueIntersection=true;
  if (norm2(n)<tol) //AB parallel to D
  {
    if (norm2(cross3D(AB,AM))<=tol) // A,B,M aligned
    {
      b=dot(AB,AM)/dot(AB,AB);
      if (b>=0 && b<=1) { return M; } // M in segment [A,B]
      real_t s=dot(AB,d);
      if (b<-tol)
      {
        if (s>=-tol) { return A; }
        else
        {
          hasUniqueIntersection=false;
          return Point();
        }
      }
      if (b>1+tol)
      {
        if (s<=tol) { return B; }
        else
        {
          hasUniqueIntersection=false;
          return Point();
        }
      }
    }
    hasUniqueIntersection=false;
    return Point(); // A,B,M not aligned and D parallel to AB => no intersection
  }

  // aD=bAB-AM  n=ABxd w1=nxd => b=AM.w1/AB.w1; w2=nxAB => a=-d.w2/AM.w2
  Point w=cross3D(n,d);
  b=dot(AM,w)/dot(AB,w);
  if (b<-tol)   return Point();  //intersection not in segment
  if (b<=tol)   return A;        //b=0
  if (b>1+tol)  return Point();  //intersection not in segment
  if (b>=1-tol) return B;        //b=1
  w=cross3D(n,AB);
  real_t a=-dot(AM,w)/dot(d,w);
  if (a<-tol) return Point();    //intersection not in the right direction
  if (a<=tol) return M;          // a=0
  return M+a*d;
}

/*! intersection half line (M,D) with polygon given by its vertices */
Point intersectionHalfLinePolygon(const Point& M, const std::vector<real_t>& d, const std::vector<Point>& vs, bool& hasUniqueIntersection, real_t tol)
{
  real_t a=0, an;
  number_t n=vs.size(),ip;
  Point Q;
  for (number_t i=0;i<n;i++)
  {
    ip=(i<n-1?i+1:0);
    Q=intersectionHalfLineSegment(M, d, vs[i], vs[ip], hasUniqueIntersection, tol);
    if (hasUniqueIntersection)
    {
      an=dot(Q-M,d)/dot(d,d);
      if (an>0) { if (a>0) a=std::min(an,a); else a=an; }
    }
  }
  if (a>0)
  {
    hasUniqueIntersection=true;
    return M+a*d;
  }
  hasUniqueIntersection=false;
  return Point();
}

/*! intersection half line (M,D) with ellipse (C,A,B, tmin, tmax)
    C center, A first apogee, B second apogee,
    sector [tmin,tmax], tmin,tmax in [0,2pi]
      P(t)= C + CA*cos(t) + CB*sin(t)  t in (tmin,tmax)
    P1=P(tmin), P2=P(tmax)
    return void Point if no intersection or on boundary
    assume 3D points, A!=B, D!=0 and DxAB!=0 (non paralell)
*/
Point intersectionHalfLineEllipse(const Point& M, const std::vector<real_t>& d, const Point& C,
                                  const Point& A, const Point& B, bool& hasUniqueIntersection, real_t tmin, real_t tmax, real_t tol)
{
  real_t a=0, an;
  // solve M+aD=P(t) <=> aD=CA*cos(t)+CB*sin(t)-CM; n=CAxCB, w=nxD  => CA.w cos(t)+ CB.w sin(t) = CM.w
  //   m^2=(CA.w)^2+(CB.w)^2 and s=atan2(CA.w,CB.w) sin(s)*cos(t)+cos(s)*sin(t) = CM.w/m => sin(t+s)=CM.w/m
  //   => t = asin(CM/w)+kpi-s and a=(CA*cos(t)+CB*sin(t)-CM).d/d.d
  Point CA=A-C, CB=B-C, CM=M-C;
  Point n=cross3D(CA,CB), w=cross3D(n,d);
  real_t caw=dot(CA,w), cbw=dot(CB,w), cmw=dot(CM,w), m=std::sqrt(caw*caw+cbw*cbw), s=std::atan2(caw,cbw);
  real_t g=cmw/m;
  if (std::abs(g)>=0 && std::abs(g)<=1) // intersection with ellipse exist
  {
    real_t t=std::asin(cmw/m)-s; //in [-3pi/2,3pi/2]
    //find first t >=tmin
    for (number_t k=0;k<2;k++)
    {
      if (k==1) t=-t-2*s-pi_*sign(caw);
      bool inc=false;
      while (t<tmin) {t+=pi_;inc=true;}
      if (!inc)
      {
        while (t>tmin) t-=pi_;
        t+=pi_;
      }
      while (t<=tmax+tol)
      {
        an=dot(CA*std::cos(t)+CB*std::sin(t)-CM,d)/dot(d,d);
        if (std::abs(an)>tol)
        {
          if (a>0) a=std::min(an,a); else a=an;
        }
        t+=pi_;
      }
      if (a>0) return M+a*d;
    }
    if (tmax-tmin==2*pi_) return Point(); // not a sector
  }
  // solve M+ad=C+bP1C and M+ad=C+bP2C
  Point P1=C+CA*cos(tmin)+CB*sin(tmin), P2=C+CA*cos(tmax)+CB*sin(tmax);
  Point Q=intersectionHalfLineSegment(M, d, C, P1, hasUniqueIntersection, tol);
  if (hasUniqueIntersection)
  {
    an=dot(Q-M,d)/dot(d,d);
    if (std::abs(an)>tol)
    { if (a>0) a=std::min(an,a); else a=an; }
  }
  Q=intersectionHalfLineSegment(M, d, C, P2, hasUniqueIntersection, tol);
  if (hasUniqueIntersection)
  {
    an=dot(Q-M,d)/dot(d,d);
    if (std::abs(an)>tol)
    { if (a>0) a=std::min(an,a); else a=an; }
  }
  if (a>tol)
  {
    hasUniqueIntersection=true;
    return M+a*d;
  }
  hasUniqueIntersection=false;
  return Point();
}

/*!
  unique intersection of segments [AB] and [CD], in 2D-3D
  all points have the same dimension,
  return true if intersection exists and is unique, else false (CDAB non coplanar or CD//AB)
    I= (1-alpha)*C + alpha*D = (1-gamma)*A + gamma*B   and  nCD: normal to CD in CDAB plane
    gamma = AC.nCD / PQ.nCD    alpha = (AP+gamma*AB.CD)/CD.CD
    alpha and gamma must belong to [0,1] with a tolerance t, say [-t, 1+t]
*/
bool intersectionOfSegments(const Point& A, const Point& B, const Point& C, const Point& D, Point& I, real_t tol)
{
  number_t d=A.size();
  if (d<2) error("2D_or_3D_only","intersectionOfSegments(..)");
  I.clear();
  if (d==3 && !arePointsCoplanar(A,B,C,D,tol)) return false;
  Point AB=B-A, CD=D-C, AC=C-A,
        nCD(A); //normal to CD in plane CD,AB
  if (d==2) { nCD[0]=D[1]-C[1]; nCD[1]=C[0]-D[0]; }
  else
  {
    Point n=cross3D(AC, CD);
    real_t no=norm(n);
    if (no>theTolerance) nCD=cross3D(CD/norm(CD),n/no);
    else // PA quasi parallel to AB
    {
      n=cross3D(C-B, CD);
      no=norm(n);
      if (no>theTolerance) nCD=cross3D(CD/norm(CD),n/no);
      else return false;// QA quasi parallel to CD
    }
  }
  real_t PQn=dot(AB, nCD);
  // theCout << "\n  intersectionOfSegments: nCD=" << nCD << " PQ|nAN=" << PQn << "\n" << std::flush;
  if (std::abs(PQn)< tol)  return false;       // PQ parallel to CD
  real_t gamma=dot(AC, nCD)/PQn;
  // theCout << "                         : gamma=" << gamma << "\n" << std::flush;
  if (gamma < -tol || gamma > 1+tol) return false; // out of [AB]
  real_t alpha=dot((gamma*AB-AC), CD)/dot(CD,CD);
  // theCout << "                         : alpha=" << alpha << "\n" << std::flush;
  if (alpha < -tol || alpha > 1+tol) return false; // out of [CD]
  I=A+gamma*AB;
  return true;
}

/*!
  strict intersection of [P,Q] and triangle [A,B,C]
  all points must have the same dimension (not checked)
  return true if some intersection points exist else false
  in 3D non coplanar case: return one or no point
  in 3D coplanar or 2D cases: return one or two or no point
  in other words, if intersection is a segment [I,J] (I!=J) it returns false ans I,J are not updated
*/
bool intersectionSegmentTriangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, Point& I, Point& J, real_t tol)
{
  I.clear(); J.clear();
  if (P.size()==3)
  {
    Point n=crossProduct(B-A,C-A); //compute a normal to [A,B,C]
    Point PQ=Q-P;
    real_t PQn=dot(PQ,n);
    if (std::abs(PQn) > tol) // PQ is not parallel to ABC
    {
      real_t gamma=dot(C-P,n)/PQn;
      if (gamma<-tol || gamma>1+tol) return false;
      Point S=P+gamma*PQ-C, CB=B-C, CA=A-C;
      Point nca=cross3D(CA,n), ncb=cross3D(CB,n);
      real_t a=dot(S,ncb)/dot(CA,ncb), b=dot(S,nca)/dot(CB,nca), c=1-a-b;
      if (a<-tol || a>1+tol || b<-tol || b>1+tol|| c<-tol || c>1+tol) return false;
      I=P+gamma*PQ;
      return true;
    }
     if (!arePointsCoplanar(P,A,B,C,tol)) return false;  // parallel but not coplanar: no intersection
  }
  // 2D case or 3D coplanar case, check intersection with the triangle edges
  bool hasI=false, hasJ=false; Point K;
  hasI=intersectionOfSegments(P,Q,A,B,I,tol);
  if (intersectionOfSegments(P,Q,B,C,K,tol))
  {
    if (!hasI) {I=K;hasI=true;}
    else {J=K;hasJ=true;}
  }
  if (!hasJ && intersectionOfSegments(P,Q,A,C,K,tol))
  {
    if (!hasI) {I=K;hasI=true;}
    else {J=K;hasJ=true;}
  }
  if (hasJ && norm(I-J)<tol) J.clear(); // I=J
  return hasI;
}

bool intersectionSegmentQuadrangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, const Point& D, Point& I, Point&J, real_t tol)
{
  if (!arePointsCoplanar(A,B,C,D,tol)) return false;
  bool hasI=false, hasJ=false; Point K;
  hasI=intersectionOfSegments(P,Q,A,B,I,tol);
  if (intersectionOfSegments(P,Q,B,C,K,tol))
  {
    if (!hasI) {I=K;hasI=true;}
    else {J=K;hasJ=true;}
  }
  if (!hasJ && intersectionOfSegments(P,Q,C,D,K,tol))
  {
    if (!hasI) {I=K;hasI=true;}
    else {J=K;hasJ=true;}
  }
  if (!hasJ && intersectionOfSegments(P,Q,A,D,K,tol))
  {
    if (!hasI) {I=K;hasI=true;}
    else {J=K;hasJ=true;}
  }
  if (hasJ && norm(I-J)<tol) J.clear(); // I=J
  return hasI;
}

//! determines one edge of the polygon defined by its list of vertices that do not separate the polygon
std::pair<number_t, number_t> nonSeparatingEdge(const std::vector<Point>& p)
{
  std::pair<number_t, number_t> edge;
  Point center=p[0];
  for (number_t i=1; i < p.size(); ++i) { center += p[i]; }
  center/=p.size();
  for (number_t i=0; i < p.size(); ++i)
    {
      number_t i1, i2;
      if (i==p.size()-1) { i1=p.size()-1; i2=0; }
      else { i1=i; i2=i+1; }

      Point p21=p[i2]-p[i1];
      const Point& p0=p[i1];
      Point normal=crossProduct(p21, center-p0);
      bool edgeFound=false;
      for (number_t j=i2+1; j < p.size() && !edgeFound; ++j) { if (dot(normal,crossProduct(p21, p[j]-p0)) < 0) { edgeFound = true; } }
      if (!edgeFound) { return std::make_pair(i1, i2); }
    }
  return edge; // dummy return
}

/*!
  common perpendicular of the straight lines defined respectively by the pair of points Am, Ap and Bm, Bp
  returns a pair of points defining the perpendicular
  the first point is on (AmAp), the second on (BmBp)
*/
std::pair<Point,Point> commonPerpendicularOfStraightLines(const Point& Am, const Point& Ap, const Point& Bm, const Point& Bp)
{
  if (Am.size() != Ap.size()) { error("diff_pts_size","commonPerpendicularOfStraightLines",Am.size(),Ap.size()); }
  if (Am.size() != Bm.size()) { error("diff_pts_size","commonPerpendicularOfStraightLines",Am.size(),Bm.size()); }
  if (Am.size() != Bp.size()) { error("diff_pts_size","commonPerpendicularOfStraightLines",Am.size(),Bp.size()); }
  if (Am.size() != 3) { error("3D_only","commonPerpendicularOfStraightLines"); }
  Point alpha=Ap-Am;
  alpha=(1./norm2(alpha))*alpha;
  Point beta=Bp-Bm;
  beta=(1./norm2(beta))*beta;
  Point tau=Bm-Am;
  real_t p=dot(alpha, beta);
  real_t l=dot(tau, alpha-p*beta)/(1.-p*p);
  real_t m=l*p-dot(tau,beta);
  return std::make_pair(Am+l*alpha,Bm+m*beta);
}

//! returns coefficients (a,b,c,d) of the equation of the plane (ax+by+cz+d=0) defined by the 3 non aligned given points S1, S2, and S3
std::vector<real_t> eqtOfPlane(const Point& S1, const Point& S2, const Point& S3)
{
  if (S1.size() != S2.size()) { error("diff_pts_size","eqtOfPlane",S1.size(),S2.size()); }
  if (S1.size() != S3.size()) { error("diff_pts_size","eqtOfPlane",S1.size(),S3.size()); }
  if (S1.size() != 3) { error("3D_only","eqtOfPlane"); }
  std::vector<real_t> T(4,0.);
  Point M=crossProduct(S3-S2,S1-S2);
  T[0]=M[0]; T[1]=M[1]; T[2]=M[2];
  T[3]=-dot(S1,M); //-S1[0]*T[0]-S1[1]*T[1]-S1[2]*T[2];
  return T;
}

/*!
  computes orientation of a trihedral, namely rotation angles around each axis to come from the standard trihedral to the given one
  basic idea:
  - first we determine the rotation Rz around z axis so that image (o, px1) of (o, px) is in the plane xOz with positive x coordinate
  - secondly we determine the rotation Ry around y axis so that image (o, px2) of (o, px1) is along x axis with positive coordinate
  - at last we determine the rotation Rx around x axis so that image (o, py1) of (o, py) is along y axis with positive coordinate
  - we store opposite values of found rotation angles when not null in opposite order (rotations does not commute)

  angles are in degree and between 0. and 360.
 */
std::vector<std::pair<real_t, dimen_t> > trihedralOrientation(const Point& o, const Point& px, const Point& py, const Point& pz)
{
  real_t theta_x=0., theta_y=0., theta_z=0., t=0.;
  Point ox = px - o, oy = py - o, oz = pz - o;

  // determination of Rz so that Rz(ox).ey = 0 and Rz(ox).ex > 0
  //  <=> sin(theta_z) ox[0] + cos(theta_z) ox[1]=0 and cos(theta_z) ox[0] - sin(theta_z) ox[1] > 0.
  if (ox[1] == 0.) { if (ox[0] < 0.) { theta_z=-180.; } }
  else
    {
      if (ox[0] == 0.)
        {
          if (ox[1] > 0.) { theta_z=-90.; }
          else { theta_z=-270.; }
        }
      else
        {
          // first constraint is tan(theta_z) = -ox[1]/ox[0] that gives 2 possible values
          // second constraint is sin(theta_z) ox[1] < cos(theta_z) ox[0] <=> -cos(theta_z) ox[0] < cos(theta_z) ox[0]
          // => ox[0] cos(theta_z) > 0
          t=-std::atan(ox[1]/ox[0]);
          if (ox[0] > 0.)
            {
              if (std::cos(t) > 0.) { theta_z=t*180./pi_; }
              else { theta_z = t*180./pi_+180; }
            }
          else
            {
              if (std::cos(t) > 0.) { theta_z=t*180./pi_+180.; }
              else { theta_z = t*180./pi_; }
            }
          while (theta_z <= -360.) { theta_z += 360.; }
          while (theta_z > 0.) { theta_z -= 360.; }
        }
    }

  // oxz=Rz(ox)
  t=theta_z*pi_/180.;
  Point oxz(std::cos(t) * ox[0] - std::sin(t) * ox[1], 0., ox[2]);

  // determination of Ry so that Ry(oxz).ez = 0 and Ry(oxz).ex > 0
  //  <=> -sin(theta_y) oxz[0] +cos(theta_y) oxz[2] = 0 and cos(theta_y) oxz[0] + sin(theta_y) oxz[2] > 0.
  if (oxz[2] != 0.)
    {
      // first constraint is tan(theta_y)=oxz[2]/oxz[0]
      // second constraint is cos(theta_y) oxz[0] > 0 <=> cos(theta_y) > 0 as oxz[0] is positive by definition
      t=std::atan(oxz[2]/oxz[0]);
      if (std::cos(t) > 0.) { theta_y = t*180./pi_; }
      else { theta_y = t*180./pi_ + 180.; }
      while (theta_y <= -360.) { theta_y += 360.; }
      while (theta_y > 0.) { theta_y -= 360.; }
    }

  // oyz=Rz(oy)
  t=theta_z*pi_/180.;
  Point oyz(std::cos(t) * oy[0] - std::sin(t) * oy[1], std::sin(t) * oy[0] + std::cos(t) * oy[1], oy[2]);
  // oyzy=Ry(oyz)
  t=theta_y*pi_/180.;
  Point oyzy(std::cos(t) * oyz[0] + std::sin(t) *oyz[2], oyz[1], -std::sin(t) * oyz[0] + std::cos(t) * oyz[2]);

  // determination of Rx xo that Rx(oyzy).ez=0 and Rx(oyzy).ey > 0
  //  <=> sin(theta_x) oyzy[1] + cos(theta_x) oyzy[2] = 0 and cos(theta_x) oyzy[1] - sin(theta_x) oyzy[2] > 0
  if (oyzy[2] == 0.) { if (oyzy[1] < 0.) { theta_x = -180.; } }
  else
    {
      if (oyzy[1] == 0)
        {
          if (oyzy[2] > 0) { theta_x=-90.; }
          else { theta_x=-270.; }
        }
      else
        {
          // first constraint is tan(theta_x)=-oyzy[2]/oyzy[1]
          // second constraint is -cos(theta_x) oyzy[1] < cos(theta_x) oyzy[1] <=> cos(theta_x) oyzy[1] > 0
          t=-std::atan(oyzy[2]/oyzy[1]);
          if (oyzy[1] > 0.)
            {
              if (std::cos(t) > 0.) { theta_x=t*180./pi_; }
              else { theta_x = t*180./pi_+180; }
            }
          else
            {
              if (std::cos(t) > 0.) { theta_x=t*180./pi_+180.; }
              else { theta_x = t*180./pi_; }
            }
          while (theta_x <= -360.) { theta_x += 360.; }
          while (theta_x > 0.) { theta_x -= 360.; }
        }
    }

  // assembling result
  std::vector<std::pair<real_t, dimen_t> > rots;
  if (theta_x != 0.) { rots.push_back(std::make_pair(-theta_x, 1)); }
  if (theta_y != 0.) { rots.push_back(std::make_pair(-theta_y, 2)); }
  if (theta_z != 0.) { rots.push_back(std::make_pair(-theta_z, 3)); }

  return rots;
}

//for a set of coplanar points, return their 2D coordinates in plane
//assuming p[0], p[1], p[2] defining a non degenerated triangle
//    origin is the first point p[0]
//    first basis vector is  u = (p[1]-p[0])/norm(p[1]-p[0])
//    second basis vector is v = u x w with w = u x u2 and u2=(p[2]-p[0])/norm(p[2]-p[0])
// return the 2D coordinates of points and the 3D orthogonal basis used (p[0],u,v,w), points in (u,v) plane
// error if not enough points (at least 3), if not 3D points, if first 3 points not defined a non degegenarated triangle
std::vector<Point> to2D(const std::vector<Point>& p, Point& u, Point& v, Point& w)
{
    if (p.size()<3) error("free_error"," give at least 3 points in to2D function");
    if (p[0].size()!=3) error("free_error"," to2D function is designed for 3D points");
    u=p[1]-p[0]; real_t nu=norm2(u);
    if (nu<theTolerance) error("free_error"," first and second point are too close in to2D function");
    u/=nu;
    Point u2=p[2]-p[0]; real_t nu2=norm2(u2);
    if (nu2<theTolerance) error("free_error"," first and third point are too close in to2D function");
    u2/=nu2;
    w=crossProduct(u,u2); real_t nw=norm2(w);
    if (nw<theTolerance) error("free_error"," the first 3 points must defined a non degenarated triangle in to2D function");
    w/=nw;
    v=crossProduct(u,w);
    std::vector<Point> p2d(p.size());
    std::vector<Point>::const_iterator itp=p.begin();
    std::vector<Point>::iterator itp2=p2d.begin();
    for (;itp!=p.end();++itp, ++itp2)
    {
        Point q=*itp-p[0];
        if (dot(q,w)>theTolerance) error("free_error"," non coplanar point in to2D function");
        *itp2 = Point(dot(q,u),dot(q,v));
    }
    return p2d;
}

// return the 2D coordinates of 3D coplanar points (3D orthogonal basis not returned)
std::vector<Point> to2D(const std::vector<Point>& p)
{
    Point u,v,w;
    return to2D(p,u,v,w);
}

//==================================================================================================
//     specific 2D geometric tools
//==================================================================================================

// split 2D/3D polygon (with no holes) in triangles (not a mesh), use earcut algorithm (see mathresources)
std::vector<std::vector<number_t> > splitInTriangles(const std::vector<Point>& pts)
{
 if (pts.size()<3) error("free_error"," give at least 3 points in splitInTriangles function");
 if (pts.size()==3)
 {
     std::vector<number_t> tri(3,0);tri[1]=1;tri[2]=2;
     return std::vector<std::vector<number_t> >(1,tri);
 }
 number_t dim=pts[0].size();
 if (dim<2 || dim>3) error("free_error"," give 2D or 3D points in splitInTriangles function");
 if (dim==2) return earcutTriangulation(pts);
 //3D points, move to 2D coordinates (pts must be in a same plane, checked in to2D function)
 Point u,v,w;
 return earcutTriangulation(to2D(pts,u,v,w));
}

//! determines if points A, B and C are colinear (returns 0) or a clockwise triangle (returns 1) or a counter-clockwise triangle (returns 2). A, B and C are supposed to be 2D points (not checked)
int pointOrientation2D(const Point& A, const Point& B, const Point& C, real_t tol)
{
  real_t v = (B[1]-A[1])*(C[0]-B[0])-(B[0]-A[0])*(C[1]-B[1]);
  if (std::abs(v) <= tol) return 0;  // colinear
  return (v > tol)? 1: 2;            // clock or counterclock wise
}

//! convex hull of 2D points
std::vector<Point> convexHull2D(std::vector<Point> P)
{
	number_t n = P.size(), k = 0;
	if (n <= 3) return P;
	std::vector<Point> H(2*n);

	// Sort points lexicographically
	std::sort(P.begin(), P.end());

	// Build lower hull
	for (size_t i = 0; i < n; ++i) {
		while (k >= 2 && crossProduct2D(H[k-2], H[k-1], P[i]) <= 0) k--;
		H[k++] = P[i];
	}

	// Build upper hull
	for (size_t i = n-1, t = k+1; i > 0; --i) {
		while (k >= t && crossProduct2D(H[k-2], H[k-1], P[i-1]) <= 0) k--;
		H[k++] = P[i-1];
	}

	H.resize(k-1);
	return H;
}

} // end of namespace xlifepp
