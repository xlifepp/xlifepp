/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Graph.cpp
  \authors D. Martin, E. Lunéville
  \since 7 may 2005
  \date 3 may 2012

  \brief Implementation of xlifepp::Graph class members and related functions
*/

// system header files
#include <algorithm>
#include <functional>

#include "Graph.hpp"
#include "Messages.hpp"

namespace xlifepp
{

//--------------------------------------------------------------------------------
// sort graph nodes by ascending degree
//--------------------------------------------------------------------------------

void Graph::sortByAscendingDegree()
{
  for ( Graph::iterator cn_i = begin(); cn_i != end(); cn_i++)
  {
    // sort node i connectivity set (*cn_i) and remove duplicates
    sort((*cn_i).begin(), (*cn_i).end());
    std::vector<number_t>::iterator nw_end = unique((*cn_i).begin(), (*cn_i).end());
    // erase duplicates from node set
    (*cn_i).erase(nw_end, (*cn_i).end());

    // sort nodes of set (*cn_i) by ascending degree
    for ( std::vector<number_t>::iterator i = (*cn_i).begin(); i != (*cn_i).end() - 1; i++ )
    {
      // find smallest degree node among current (i) node neigbors
      number_t deg = nodeDegree(*i), j_deg;
      std::vector<number_t>::iterator j, k(i);

      for ( j = i + 1; j != (*cn_i).end(); j++ )
        if ( ( j_deg = (*(begin() + *j - 1)).size() ) < deg )
        {
          k = j;
          deg = j_deg;
        }

      // degree of k is less then degree of i: swap i and k
      if ( k != i )
      {
        number_t aux = *i;
        *i = *k;
        *k = aux;
      }
    }
  }
}

//--------------------------------------------------------------------------------
// compute maximum degree (maximum number of neighbors taken over all nodes)
//--------------------------------------------------------------------------------

number_t Graph::maximumDegree() const
{
  number_t s = 0;

  for ( Graph::const_iterator cn_i = begin(); cn_i != end(); cn_i++)
  {
    s = std::max(s, number_t(cn_i->size()));
  }

  return s;
}

//-------------------------------------------------------------------------------
// compute band width (maximum distance between two connected nodes)
// and skyline storage size (accumulated distance between nodes)
//-------------------------------------------------------------------------------

std::pair<number_t, number_t> Graph::bandWitdhAndSkyline() const
{
  number_t b = 0;
  number_t nod_current = 1;
  number_t s = 0;

  for ( Graph::const_iterator cn_i = begin(); cn_i != end(); cn_i++, nod_current++ )
  {
    number_t degree = 0;

    for ( std::vector<number_t>::const_iterator nod_i = (*cn_i).begin(); nod_i != (*cn_i).end(); nod_i++)
    {

      number_t d = number_t(std::abs(int(nod_current) - int(*nod_i)));
      b = std::max( b, d);
      degree = std::max( degree, d);

      //number_t nodi = *nod_i;
      //b = std::max( b, number_t(std::abs(nod_current - nodi)));
      //degree = std::max( degree, number_t(std::abs(nod_current - nodi)));
    }

    s += degree;
  }

  return std::pair<number_t, number_t>(b, s);
}

number_t Graph::bandWidth() const
{
  number_t b = 0;
  number_t nod_current = 1;

  for ( Graph::const_iterator cn_i = begin(); cn_i != end(); cn_i++, nod_current++ )
    for ( std::vector<number_t>::const_iterator nod_i = (*cn_i).begin(); nod_i != (*cn_i).end(); nod_i++)
    {
      number_t nodi = *nod_i;
      b = std::max( b, number_t(std::abs(int(nod_current) - int(nodi))) );
    }

  return b;
}

number_t Graph::skylineSize() const
{
  number_t nod_current = 1;
  number_t s = 0;

  for ( Graph::const_iterator cn_i = begin(); cn_i != end(); cn_i++, nod_current++ )
  {
    number_t degree = 0;

    for ( std::vector<number_t>::const_iterator nod_i = (*cn_i).begin(); nod_i != (*cn_i).end(); nod_i++)
    {
      number_t nodi = *nod_i;
      degree = std::max( degree, number_t(std::abs(int(nod_current) - int(nodi))) );
    }

    s += degree;
  }

  return s;
}

//--------------------------------------------------------------------------------
// renumbering algorithm
//--------------------------------------------------------------------------------

std::vector<number_t> Graph::renumber(number_t nbNodes)
{
  // vector to store new node numbers
  std::vector<number_t> new_Nodes(nbNodes, 0);
  number_t max_degree = maximumDegree();

  std::vector<bool> markers(nbNodes, false);
  number_t last_new_Node = 1, i_level = 1, itree = 0;

  number_t connected_sets = 1;
  bool node_left(true);

  while ( node_left )
  {
    if ( connected_sets++ > 100)
    {
      error("graph_overflow", 100);
    }

    //cout << "****BUG *** Connected components > 100";
    number_t n_i = 1;

    for ( std::vector<bool>::iterator mark_i = markers.begin(); mark_i != markers.end(); mark_i++, n_i++ )
    {
      if ( ! *mark_i )
      {
        std::vector<number_t> n_tree(size(), 0);
        number_t i_begin = n_i;
        number_t start_level = 1;

        number_t re_start = renumEngine(markers, n_tree, itree, max_degree, i_begin, i_level);

        if ( i_level > start_level )
        {
          i_begin = re_start;
        }

        renumEngine(markers, n_tree, max_degree, itree, re_start, i_level);

        for ( std::vector<number_t>::reverse_iterator it = n_tree.rend() - itree;
              it != n_tree.rend(); it++ )
        {
          new_Nodes[*it - 1] = last_new_Node++;
        }
      }
    }

    // are all nodes marked ?
    std::vector<bool>::iterator left_node = find_if(markers.begin(), markers.end(),
                                            std::bind(std::equal_to<bool>(), std::placeholders::_1, false));
    node_left = left_node != markers.end();
  }

  return new_Nodes;
}

number_t Graph::renumEngine(std::vector<bool>& markers, std::vector<number_t>& n_tree,
                          number_t& itree, number_t max_degree, number_t& i_begin, number_t& i_level)
{
  std::vector<number_t> n_level(size(), 0);
  n_level[i_begin - 1] = 1;
  n_tree[0] = i_begin;
  itree = 1;
  number_t jtree = 0;

  while ( jtree < itree )
  {
    number_t n_i = n_tree[jtree++];
    Graph::iterator cn_i = begin() + n_i - 1;

    for (std::vector<number_t>::iterator nod_j = (*cn_i).begin(); nod_j != (*cn_i).end(); nod_j++)
    {
      if ( n_level[*nod_j - 1] == 0 )
      {
        n_tree[itree++] = *nod_j;
        i_level = n_level[*nod_j - 1] = n_level[n_i - 1] + 1;
      }
    }
  }

  // for all node of maximum depth, find one with smallest degree
  number_t n_i = 1, i_start = i_begin, m_deg = max_degree, i_deg;
  std::vector<number_t>::iterator n_level_i = n_level.begin();
  std::vector<bool>::iterator mark_i = markers.begin();

  for ( Graph::iterator cn_i = begin(); cn_i != end(); cn_i++, n_i++, mark_i++, n_level_i++)
  {
    // mark all D.o.F of current connected component
    if ( *n_level_i > 0 )
    {
      *mark_i = true;

      // and find the first one with smallest degree
      if ( *n_level_i == i_level )
        if ( ( i_deg = (*cn_i).size() ) < m_deg )
        {
          m_deg = i_deg;
          i_start = n_i;
        }
    }
  }

  return i_start;
}

/*
--------------------------------------------------------------------------------
 I/O Utilities
--------------------------------------------------------------------------------
*/
void Graph::print(std::ostream& os, const string_t& title) const
{
  os << std::endl << " " << title << " Number of nodes " << size();
  os << std::endl << " Node # \\ Connected nodes";
  number_t ns = 1;

  for (Graph::const_iterator cn_i = begin(); cn_i != end(); cn_i++)
    //printRowWise(thePrintStream, " "+tostring(ns++)+": ",20,5, (*cn_i).begin(), (*cn_i).end());
  {
    os << " " << ns++ << ": ";

    for (std::vector<number_t>::const_iterator it = cn_i->begin(); it != cn_i->end(); it++)
    {
      os << *it << " ";
    }

    os << "\n";
  }
}

void Graph::print(const string_t& title) const
{
  //print(thePrintStream, title);
}

void Graph::printNodes(std::ostream& os, const std::vector<number_t>& new_Nodes)
{
  os << std::endl << " New Node numbers";
  number_t nod = 1;

  for (std::vector<number_t>::const_iterator n_i = new_Nodes.begin(); n_i != new_Nodes.end(); n_i++)
  {
    if ( nod % 10 == 1 )
    {
      os << " " << std::endl;
    }

    os << " | " << std::setw(5) << nod++ << " -> " << std::setw(5) << *n_i;
  }
}

void Graph::printNodes(const std::vector<number_t>& new_Nodes)
{
  //printNodes(thePrintStream, new_Nodes);
}

} // end of namespace xlifepp

