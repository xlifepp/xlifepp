/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file VectorEntry.hpp
  \author E. Lunéville
  \since 01 nov 2013
  \date 12 jun 2014

  \brief Definition of the xlifepp::VectorEntry class
*/

#ifndef VECTOR_ENTRY_HPP
#define VECTOR_ENTRY_HPP

#include "Vector.hpp"
#include "Function.hpp"
#include "Value.hpp"
#include "config.h"

namespace xlifepp{

/*!
   \class VectorEntry
   small class handling current types of Vector
   shadowing template parameter of Vector
 */
class VectorEntry
{
  public:
    ValueType valueType_;                             //!< value type of the entries (_real,_complex)
    StrucType strucType_;                             //!< structure type of the entries (_scalar,_vector)
    Vector<real_t>* rEntries_p;                       //!< pointer to real vector
    Vector<complex_t>* cEntries_p;                    //!< pointer to complex vector
    Vector<Vector<real_t> >* rvEntries_p;             //!< pointer to vector of real vectors
    Vector<Vector<complex_t> >* cvEntries_p;          //!< pointer to vector of complex vectors
    dimen_t nbOfComponents;                           //!< number of components of value (1 in scalar case, 2,3,.. in vector case)

    //constructors/destructor
    VectorEntry();                                    //!< default constructor
    VectorEntry(ValueType, StrucType, number_t n = 0, number_t nv = 1); //!< constructor from explicit vector type and vector size
    VectorEntry(ValueType, number_t nv, number_t n = 0); //!< constructor from explicit vector type and vector size
    VectorEntry(real_t, number_t n);                   //!< constructor from constant real_t value and vector size
    VectorEntry(const complex_t&, number_t n);         //!< constructor from constant complex_t value and vector size
    VectorEntry(const Vector<real_t>&, number_t n);    //!< constructor from constant Vector<real_t> value and vector size
    VectorEntry(const Vector<complex_t>&, number_t n); //!< constructor from constant Vector<complex_t> value and vector size
    VectorEntry(const Function&, number_t n);          //!< constructor from a function
    VectorEntry(const Vector<real_t>&);                //!< constructor from a real vector
    VectorEntry(const Vector<complex_t>&);             //!< constructor from a complex  vector
    VectorEntry(const Vector<Vector<real_t> >&);       //!< constructor from a vector of real vectors
    VectorEntry(const Vector<Vector<complex_t> >&);    //!< constructor from a vector of complex vectors
    VectorEntry(const VectorEntry&);                   //!< copy constructor
    ~VectorEntry();                                    //!< destructor
    VectorEntry& operator=(const VectorEntry&);        //!< assign operator (full copy)
    void clear();                                      //!< deallocate memory used by a VectorEntry
    void resize(number_t);                             //!< resize entry

    void extendEntries(std::vector<number_t>, number_t); //!< extend current entries along a new numbering
    void deleteRows(number_t, number_t);                 //!< delete rows r1,..., r2
    VectorEntry* toScalar();                             //!< create new scalar vector entry from non scalar vector entry

    bool isEmpty() const;                                //!< true if no one of pointers allocated
    void setEntry(number_t, const real_t &);             //!< set (i) real value (indices start from 1)
    void setEntry(number_t, const complex_t &);          //!< set (i) complex value (indices start from 1)
    void setEntry(number_t, const Vector<real_t> &);     //!< set (i) real matrix value (indices start from 1)
    void setEntry(number_t, const Vector<complex_t> &);  //!< set (i) complex matrix value (indices start from 1)
    void getEntry(number_t, real_t &) const;             //!< get (i) real value (indices start from 1)
    void getEntry(number_t, complex_t &) const;          //!< get (i) complex value (indices start from 1)
    void getEntry(number_t, Vector<real_t> &) const;     //!< get (i) real matrix value (indices start from 1)
    void getEntry(number_t, Vector<complex_t> &) const;  //!< get (i) complex matrix value (indices start from 1)
    Value getValue(number_t) const;                      //!< get (i) as value (indices start from 1)
    void setValue(number_t, const Value&);               //!< set (i) value (indices start from 1)
    Vector<real_t>& asVector(Vector<real_t>&) const;                        //!< return as Vector<real_t>
    Vector<complex_t>& asVector(Vector<complex_t>&)const;                   //!< return as Vector<complex_t>
    Vector<Vector<real_t> >& asVector(Vector<Vector<real_t> >&)const;       //!< return as Vector<Vector<real_t> >
    Vector<Vector<complex_t> >& asVector(Vector<Vector<complex_t> >&)const; //!< return as Vector<Vector<complex_t> >

    number_t nbzero(real_t tol = 0) const;             //!< return number of 0 components (|xi|<=tol)
    VectorEntry& operator+=(const VectorEntry&);       //!< operation U+=V (same size, but value types may be different)
    VectorEntry& operator-=(const VectorEntry&);       //!< operation U+=V (same size, but value types may be different)
    VectorEntry& operator*=(const real_t &);           //!< operation U*=r
    VectorEntry& operator/=(const real_t &);           //!< operation U/=r
    VectorEntry& operator*=(const complex_t &);        //!< operation U*=c
    VectorEntry& operator/=(const complex_t &);        //!< operation U/=c
    void extractComponent(const VectorEntry&,dimen_t); //!< extract component i from vector entry of type vector<vector<K> >
    void add(const VectorEntry&, real_t);                                 //!<add to current entry an entry multiplied by a real scalar
    void add(const VectorEntry&, complex_t);                              //!<add to current entry an entry multiplied by a complex scalar
    void add(const VectorEntry&,const std::vector<number_t>&, real_t);    //!<add to current entry at given positions an entry multiplied by a real scalar
    void add(const VectorEntry&,const std::vector<number_t>&, complex_t); //!<add to current entry at given positions an entry multiplied by a complex scalar
    VectorEntry& toAbs();                            //!< v -> abs(v)   (modify current entries to real entries)
    VectorEntry& toReal();                           //!< v -> real part of v (modify current entries to real entries)
    VectorEntry& toImag();                           //!< v -> imaginary part of v (modify current entries to real entries)
    VectorEntry& toComplex();                        //!< modify current entries to complex entries if they are not
    VectorEntry& toVector(dimen_t n, dimen_t i);     //!< if scalar entries, modify current scalar entries to vector entries (1<=i<=n)
    VectorEntry& toConj();                           //!< v -> conjugate of v (modify current entries), if real nothing is done
    VectorEntry& roundToZero(real_t aszero=std::sqrt(theEpsilon));   //!< round to zero all coefficients close to 0 (|.| < aszero)
    VectorEntry& round(real_t prec=0.);                             //!< round with a given precision (0 no round)

    //miscellaneous operations
    real_t product(number_t, const real_t&) const;       //!< return v(n)*a (real)
    complex_t product(number_t, const complex_t&) const; //!< return v(n)*a (complex)
    complex_t maxValAbs() const;                         //!< return the value (in complex) of component being the largest one in absolute value
    void moveColumns(const std::vector<number_t>&);      //!< move columns according to a map

    //iterator
    Vector<real_t>::const_iterator beginr() const              //! const iterator to the first real value
    {return rEntries_p->begin();}
    Vector<complex_t>::const_iterator beginc() const           //! const iterator to the first complex value
    {return cEntries_p->begin();}
    Vector<Vector<real_t> >::const_iterator beginrv() const    //! const iterator to the first real vector value
    {return rvEntries_p->begin();}
    Vector<Vector<complex_t> >::const_iterator begincv() const //! const iterator to the first complex vector value
    {return cvEntries_p->begin();}
    Vector<real_t>::iterator beginr()                          //! iterator to the first real value
    {return rEntries_p->begin();}
    Vector<complex_t>::iterator beginc()                       //! iterator to the first complex value
    {return cEntries_p->begin();}
    Vector<Vector<real_t> >::iterator beginrv()                //! iterator to the first real vector value
    {return rvEntries_p->begin();}
    Vector<Vector<complex_t> >::iterator begincv()             //! iterator to the first complex vector value
    {return cvEntries_p->begin();}
    template<typename T>
    Vector<T>* entriesp() const                                //! returns entries of type T
    {
      where("VectorEntry::entriesp<T>");
      error("no_specialization");
      return new Vector<T>();  //fake return
    }
    //utilities
    number_t size() const;                             //!< return number of values counted in value type
    ValueType valueType() const                        //! return valueType (const)
      {return valueType_;}
    ValueType& valueType()                             //! return valueType (non const)
      {return valueType_;}
    StrucType strucType() const                        //! return strucType (const)
      {return strucType_;}
    StrucType& strucType()                             //! return strucType (non const)
      {return strucType_;}
    real_t norm2() const;                              //!< intern norm2

    void print(std::ostream&) const;                   //!< print entries on ostream
    void print(PrintStream& os) const {print(os.currentStream());}
    void printFirst(std::ostream&, number_t, std::vector<string_t> comment=std::vector<string_t>()) const;   //!< print first n entries
    void printFirst(PrintStream& os, number_t n, std::vector<string_t> comment=std::vector<string_t>()) const
     {printFirst(os.currentStream(), n, comment);}
    void printLast(std::ostream&, number_t,std::vector<string_t> comment=std::vector<string_t>()) const;    //!< print last n entries
    void printLast(PrintStream& os, number_t n, std::vector<string_t> comment=std::vector<string_t>()) const
     {printLast(os.currentStream(), n, comment);}
    void saveToFile(const string_t& fn, number_t prec=fullPrec, bool encode=false) const;        //!< save VectorEntry to file
    void saveToFile(const string_t& fn, bool encode) const //! save VectorEntry to file
    { saveToFile(fn, fullPrec, encode); }
    string_t encodeFileName(const string_t& fn) const;                //!< encode file name with some additional informations
    friend std::ostream& operator<<(std::ostream&,const VectorEntry&);
};

//external functions related to VectorEntry
std::ostream& operator<<(std::ostream&,const VectorEntry&);//!< output VectorEntry on stream
real_t norm(const VectorEntry&, number_t l=2);                     //!< norm of Vector entry (default l2)
real_t norm1(const VectorEntry&);                                  //!< l1 vector VectorEntry norm
real_t norm2(const VectorEntry&);                                  //!< l2 vector VectorEntry  (quadratic norm)
real_t norminfty(const VectorEntry&);                              //!< l_infinite VectorEntry  (sup norm)
complex_t innerProduct(const VectorEntry&, const VectorEntry&);    //!< inner product of two vectorentry's
complex_t hermitianProduct(const VectorEntry&, const VectorEntry&);//!< hermitian product of two vectorentry's
complex_t dotRC(const VectorEntry&, const VectorEntry&);           //!< same as inner product of two vectorentry's

void addScaledVector(VectorEntry&, VectorEntry&, const real_t&);   //!< x+=t*v (t real)
void addScaledVector(VectorEntry&, VectorEntry&, const complex_t&);//!< x+=t*v (t complex)

template<typename T>
inline VectorEntry operator/(const VectorEntry& v, const T& a)     //! operation V/=a
{
    VectorEntry r=v;
    return r/=a;
}

//utility for extend vector from one numbering to an other numbering
template <typename I1, typename I2>
void extendVector(const std::vector<number_t>& renum, I1 it1, I2 it2)    //! template extension method
{
  std::vector<number_t>::const_iterator it;
  for(it = renum.begin(); it != renum.end(); it++, it2++)
    if(*it != 0) *(it1 + *it - 1) = *it2;
}

//@{
//! specialization (no check of null pointer, do it before!)
template<>
Vector<real_t>* VectorEntry::entriesp<real_t>() const;
template<>
Vector<complex_t>* VectorEntry::entriesp<complex_t>() const;
template<>
Vector<Vector<real_t> >* VectorEntry::entriesp<Vector<real_t> >() const;
template<>
Vector<Vector<complex_t> >* VectorEntry::entriesp<Vector<complex_t> >() const;
//@}

} //end of namespace xlifepp

#endif // VECTOR_ENTRY_HPP
