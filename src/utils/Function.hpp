/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Function.hpp
  \author E. Lunéville
  \since 1 nov 2011
  \date 3 may 2012

  \brief Definition of the xlifepp::Function class

  Class to handle function to encapsulate user's functions
  with one of the following prototypes:
      \code
      r function(const Point&, Parameters&)
      r function(const Point&, const Point&, Parameters&)
      Vector<r> function(const Vector<Point>&, Parameters&)
      Vector<r> function(const Vector<Point>&, const Vector<Point>&, Parameters&)
      \endcode

  the function may return a:
      \code
      r=real_t
      r=complex_t
      r=Vector<real_t>
      r=Vector<complex_t>
      r=Matrix<real_t>
      r=Matrix<complex_t>
      \endcode

  This is a non templated class. This choice avoids complexity for user
  and allows to pass such object functions to general computation functions

  How it works:
      - User function definitions (standard c++):
          - a scalar one:
              \code
              real_t sinf(const Point& P, Parameters& pa = default_parameters)
              {return sin(pi*P[0]);}
              \endcode
          - a vector one:
              \code
              Vector<complex_t> electricField(const Point& P, Parameters& pa = default_parameters)
              {
                Vector<complex_t> V(3);
                real_t omega=real(pa(1));
                for (int k = 0 ; k < P.size() ; k++)
                {
                  V[k] = exp(complex_(0.,omega)*P[k]);
                }
                return V;
              }
              \endcode
      - Function object encapsulation:
          \code
          Function sin_f(sinf);                   // define object Function with no parameter
          Parameter omega=3.1415926;              // define the parameter omega
          Parameters param(); param<<omega;       // define a parameters list containing omega
          Function E_field(electricField, param); // define object Function and associate param
          \endcode
      - calling the object Function to compute value of the user function (two ways) :
          \code
          Point P;...
          Vector<Point> VP; ...
          Vector<real_t) Vr;
          real_t r(0.);
          r=sinf(P,r);                         // compute sinf(P) and return a reference
          r=sinf.funSR(P);                     // same result with no check, return an object
          sinf(VP,Vr);                         // compute sinf for a vector of points and return a vector of values
                                               // funSR means direct access to a scalar real function (take care!)
          Vector<complex_t> V;                 // create vector to store result
          E_field(P,V);                        // compute vector and return a reference (automatic resizment)
          Vector<complex_t> W=E_field.funVC(P);// same result with no check, return an object
          \endcode

  The xlifepp::Function class uses xlifepp::Point, xlifepp::Vector and xlifepp::Matrix classes. It is assumed that these classes provide
  default constructor and copy constructor and the xlifepp::Vector class provides (as std::vector)
      - constructor with dim and default value: Vector<T>(number_t dim,const T& v)
      - size() and resize(int n) functions
      - usual access operator [int i] (i=0,...)
 */

#ifndef FUNCTION_HPP
#define FUNCTION_HPP

#include <map>
#include <vector>
#include <complex>
#include <utility>
#include <iostream>
#include <cstdlib>
#include <typeinfo> // we use RTTI typeid to retrieve the returned value type

#include "Point.hpp" // Point class definition
#include "Parameters.hpp" // Parameters class definition
#include "Vector.hpp"  // Vector class
#include "Matrix.hpp"  // Matrix class
#include "Tabular.hpp" // Point class definition
#include "config.h" // typedef, enum, ... definitions

namespace xlifepp
{
//forward class declaration
class OperatorOn;  //experimental
class TermVector;

enum ArgType {_pointArg, _vectorOfPointArg}; //!< enum of types of function arguments

/*!
  \class PointerF
  Fake templated class to select right pointer function along point or vector<point> input argument
*/
template <typename T> class PointerF
{
  public:
    typedef T(fun_t)(const Point&, Parameters&);                                         //!< alias of scalar function
    typedef Vector<T> (vfun_t)(const Vector<Point>&, Parameters&);                       //!< alias of vector function
    typedef T(ker_t)(const Point&, const Point&, Parameters&);                           //!< alias of scalar kernel
    typedef Vector<T> (vker_t)(const Vector<Point>&, const Vector<Point>&, Parameters&); //!< alias of vector kernel
};

// typedef of all authorized function types
typedef real_t(funSR_t)(const Point&, Parameters&);
typedef complex_t(funSC_t)(const Point&, Parameters&);
typedef Vector<real_t>(funVR_t)(const Point&, Parameters&);
typedef Vector<complex_t>(funVC_t)(const Point&, Parameters&);
typedef Matrix<real_t>(funMR_t)(const Point&, Parameters&);
typedef Matrix<complex_t>(funMC_t)(const Point&, Parameters&);
typedef real_t(kerSR_t)(const Point&, const Point&, Parameters&);
typedef complex_t(kerSC_t)(const Point&, const Point&, Parameters&);
typedef Vector<real_t>(kerVR_t)(const Point&, const Point&, Parameters&);
typedef Vector<complex_t>(kerVC_t)(const Point&, const Point&, Parameters&);
typedef Matrix<real_t>(kerMR_t)(const Point&, const Point&, Parameters&);
typedef Matrix<complex_t>(kerMC_t)(const Point&, const Point&, Parameters&);
typedef Vector<real_t>(vfunSR_t)(const Vector<Point>&, Parameters&);
typedef Vector<complex_t>(vfunSC_t)(const Vector<Point>&, Parameters&);
typedef Vector<Vector<real_t> >(vfunVR_t)(const Vector<Point>&, Parameters&);
typedef Vector<Vector<complex_t> >(vfunVC_t)(const Vector<Point>&, Parameters&);
typedef Vector<Matrix<real_t> >(vfunMR_t)(const Vector<Point>&, Parameters&);
typedef Vector<Matrix<complex_t> >(vfunMC_t)(const Vector<Point>&, Parameters&);
typedef Vector<real_t>(vkerSR_t)(const Vector<Point>&, const Vector<Point>&, Parameters&);
typedef Vector<complex_t>(vkerSC_t)(const Vector<Point>&, const Vector<Point>&, Parameters&);
typedef Vector<Vector<real_t> >(vkerVR_t)(const Vector<Point>&, const Vector<Point>&, Parameters&);
typedef Vector<Vector<complex_t> >(vkerVC_t)(const Vector<Point>&, const Vector<Point>&, Parameters&);
typedef Vector<Matrix<real_t> >(vkerMR_t)(const Vector<Point>&, const Vector<Point>&, Parameters&);
typedef Vector<Matrix<complex_t> >(vkerMC_t)(const Vector<Point>&, const Vector<Point>&, Parameters&);

/*!
  \class Function
  The class Function encapsulates in object function a user's functions. It is used by internal computing functions

  this class is able to deal with any user function of type
      \code
      r function(const Point&, Parameters&) (function tpe)
      r function(const Point&, const Point&, Parameters&) (kernel type)
      \endcode
  and their vector forms
      \code
      Vector<r> function(const Vector<Point>&, Parameters&)
      Vector<r> function(const Vector<Point>&, const Vector<Point>&, Parameters&)
      \endcode
  with r of type: real_t, complex_t, Vector<real_t>, Vector<complex_t>, Matrix<real_t> or Matrix<complex_t>

  This class mainly proposes:
      - two constructors (unamed and named)
      - a fast call to compute function values when function is well identified (see member functions fun_xxx)
      - a general safe call to compute function values (see operator ()); require only output argument compatibility
        if output argument is not compatible, an error occurs with a message
      - some utilities to return type of function and type of arguments (use rtti functions)

  Note: A Parameters is attached to a Function using a pointer. This Parameters is passed to the function by reference
  therefore, it may be shared by few functions and when it is modified it is for any function sharing it.
  In a way, this parameters is global
 */
class Function
{
  protected:
    mutable Parameters* params_=nullptr; //!< pointer to parameter list
    bool freeParams_=false;          //!< flag to delete parameters pointer
    void* fun_=nullptr;              //!< store pointer to function in a void *
    Function* grad_=nullptr;         //!< possible pointer to grad function (default 0)

    void* table_=nullptr;            //!< Tabular pointer as void* for dealing with tabulated values (default 0)
    Function* pointToTabx_=nullptr;  //!< Function to associate Point or Point pair to variables of Tabular (default 0)

  public:
    ValueType returnedType_;         //!< type of returned value (one among _real, _complex)
    StrucType returnedStruct_;       //!< structure of returned value (one among _scalar, _vector, _matrix)
    FunctType functionType_;         //!< type of the function (_function or _kernel}
    ArgType argType_;                //!< type of the function argument (_pointArg or _vectorOfPointArg)
    dimen_t dimPoint_;               //!< dimension of point (default=3)
    dimPair dims_;                   //!< dimension of returned values (does not take into account vectorOfPointArg)
    string_t name_;                  //!< name of function and comments
    mutable bool checkType_=false;   //!< to activate the checking of arguments (see operator ())
    mutable bool conjugate_=false;   //!< temporary flag for conjugate operation in OperatorOnUnknown construction (see operator lib)
    mutable bool transpose_=false;   //!< temporary flag for transpose operation in OperatorOnUnknown construction (see operator lib)

  public:
    bool requireNx=false;            //!< true if user declares that its function requires normal or x-normal vector
    bool requireNy=false;            //!< true if user declares that its function (kernel) requires y-normal vector
    bool requireTx=false;            //!< true if user declares that its function requires normal or x-tangent vector
    bool requireTy=false;            //!< true if user declares that its function (kernel) requires y-tangent vector
    bool requireElt=false;           //!< true if user declares that its function requires element
    bool requireDom=false;           //!< true if user declares that its function requires domain
    bool requireDof=false;           //!< true if user declares that its function requires dof

  public:
    //special attributes for kernel,  evaluate as one argument function x with y passed as parameter or the contrary
    mutable bool xpar=false;         //!< true means that point x is consider as a parameter (unused for ordinary function)
    mutable Point xory;              //!< point x or y as parameter of the kernel (unused for ordinary function)

  public:
    Function(const Function &, bool fullcopy=false);  //!< copy constructor with full copy option
    void fullCopy(const Function &) ;                 //!< full copy of  a Function

    // utilities
    ValueType typeReturned() const     //! return the type of return argument (real_t or complex_t)
    {return returnedType_;}
    StrucType structReturned() const   //! return the structure of return argument (saclar_t, vector_t or matrix_t)
    {return returnedStruct_;}
    ValueType valueType() const        //! return the type of return argument (real_t or complex_t)
    {return returnedType_;}
    StrucType strucType() const        //! return the structure of return argument (saclar_t, vector_t or matrix_t)
    {return returnedStruct_;}
    FunctType typeFunction() const     //! return the function type (function or kernel)
    {return functionType_;}
    ArgType typeArg() const            //! return the input argument type (_pointArg or vectorOfPointArg)
    {return argType_;}
    bool conjugate() const             //! return the conjugate state (true/false) flag
    {return conjugate_;}
    bool& conjugate()                  //! <return the conjugate state (true/false) flag
    {return conjugate_;}
    void conjugate(bool v) const       //! set the conjugate stateflag
    {conjugate_ = v;}
    bool transpose() const             //! return the transpose state (true/false) flag
    {return transpose_;}
    bool& transpose()                  //! return the transpose state (true/false) flag
    {return transpose_;}
    void transpose(bool v) const       //! set the transpose stateflag
    {transpose_ = v;}
    bool isVoidFunction() const        //! true if fun pointer is 0
    {return fun_==nullptr;}
    const void* fun_p() const          //! return fun void pointer
    {return fun_;}
    Parameters& params()               //! return the parameters list
    {return *params_;}
    Parameters& params() const         //! return the parameters list
    {return *params_;}
    Parameters*& params_p()            //! return the parameters list pointer
    {return params_;}
    const Parameters* params_p() const //! return the parameters list pointer
    {return params_;}
    dimPair dims() const               //! return the dimensions of object returned by function
    {return dims_;}
    string_t name() const              //! return the function name
    {return name_;}
    const Function* grad_p() const     //! return grad Function pointer
    {return grad_;}

    //manage parameters
    void addParam(Parameter& pa)     //! add a parameter to the current list of parameters
    {if(params_ == nullptr) params_ = new Parameters(pa);  else(*params_) << pa;}
    template<typename K>
    void setParam(const K& v, const string_t& name) const;  //!< set parameter name to v
    Parameter& parameter(const string_t&) const;      //!< access parameter by name (string)
    Parameter& parameter(const char* s) const;        //!< access parameter by name (char *)
    Parameter& parameter(const size_t) const;         //!< accses parameter by rank >=1

    //normal requirements
    bool normalRequired() const                       //! true if normal required
    {return requireNx;}
    bool tangentRequired() const                      //! true if tangent required
    {return requireTx;}
    void require(UnitaryVector uv, bool tf=true)
    {
      switch(uv)
        {
          case _n:
          case _nx: requireNx=tf; return;
          case _ny: requireNy=tf; return; //should not be used for a function of one variable
          case _tau:
          case _taux: requireTx=tf; return;
          case _tauy: requireTy=tf; return; //should not be used for a function of one variable
          default:
            where("Function::require(UnitaryVector)");
            error("unitaryvector_not_handled",words("normal",uv));
        }
    }

    //all requirements
    void require(const string_t& s, bool tf=true)
    {
      if(s=="_n" || s=="_nx") {requireNx=tf; return;}
      if(s=="_ny") {requireNy=tf; return;}
      if(s=="_tau" || s=="_taux") {requireTx=tf; return;}
      if(s=="_tauy") {requireTy=tf; return;}
      if(s=="element") {requireElt=tf; return;}
      if(s=="dof") {requireDof=tf; return;}
      if(s=="domain") {requireDom=tf; return;}
      where("Function::require(String)"); error("data_not_handled",s);
    }

    //DEPRECATED normal transmission
    void set_np(const Vector<real_t>* np=nullptr) const               //! set pointer to normal in theThreadData
    {warning("deprecated","Parameters::set_np","extern setNx"); setNx(const_cast<Vector<real_t>*>(np));}
    const Vector<real_t>* get_np() const                        //! get pointer to normal in theThreadData
    {warning("deprecated","Parameters::get_np","extern getN or getNx"); return &getNx();}
    void set_n(const Vector<real_t>& n) const                   //! set pointer to normal in theThreadData
    {warning("deprecated","Parameters::set_n","extern setNx"); setNx(const_cast<Vector<real_t>*>(&n));}
    const Vector<real_t>& get_n() const                         //! get pointer to normal in theThreadData
    {warning("deprecated","Parameters::get_n","extern getN or getNx"); return getNx();}
    void associateVector(UnitaryVector uv, number_t d=3);       //!< associate to the function a unitary vector

    //@{
    //! management of kernel argument
    void setX(const Point& P) const
    {xpar=true; xory=P;}
    void setY(const Point& P) const
    {xpar=false; xory=P;}
    const Function& operator()(const Point& P, VariableName vn) const
    { setX(P); return *this;}
    const Function& operator()(VariableName vn, const Point& P) const
    { setY(P); return *this;}
    OperatorOn& operator()(VariableName) const;   //experimental
    //@}

    void printInfo(std::ostream& out) const; //!< print utility
    friend std::ostream& operator<<(std::ostream&, const Function &);

    // error management
    void isNotAFunction() const;  //!< true if not a function
    void isNotAKernel() const;    //!< true if not a kernel
    void checkTypeOn() const      //! activate the checking of integrity
    {checkType_ = true;}
    void checkTypeOff() const     //! deactivate the checking of integrity
    {checkType_ = false;}
    template<class T> void checkFunctionType(T&, FunctType) const; //!< to check the type of output and input arguments

    // static data and members
    static std::map<string_t, std::pair<ValueType, StrucType> > returnTypes; //!< rtti names of pointer function
    static std::map<string_t, std::pair<ValueType, StrucType> > returnArgs;  //!< rtti names of return arguments
    static void initReturnTypes();                                           //!< initialisation of the map

    //! to initialize members, used by the constructors
    void init(void*, const string_t&, const string_t&, FunctType, ArgType, Parameters&, dimen_t d=3);
    Function(); //!< default constructor
    ~Function();//!< destructor

    Point fakePoint() const;            //fake point to compute dimension
    Vector<Point> fakePoints() const;   //fake vector of points to compute dimension

    //@{
    //! constructor along the function prototypes
    Function(funSR_t& f, Parameters& pa = defaultParameters);
    Function(funSC_t& f, Parameters& pa = defaultParameters);
    Function(funVR_t& f, Parameters& pa = defaultParameters);
    Function(funVC_t& f, Parameters& pa = defaultParameters);
    Function(funMR_t& f, Parameters& pa = defaultParameters);
    Function(funMC_t& f, Parameters& pa = defaultParameters);
    Function(kerSR_t& f, Parameters& pa = defaultParameters);
    Function(kerSC_t& f, Parameters& pa = defaultParameters);
    Function(kerVR_t& f, Parameters& pa = defaultParameters);
    Function(kerVC_t& f, Parameters& pa = defaultParameters);
    Function(kerMR_t& f, Parameters& pa = defaultParameters);
    Function(kerMC_t& f, Parameters& pa = defaultParameters);
    Function(vfunSR_t& f, Parameters& pa = defaultParameters);
    Function(vfunSC_t& f, Parameters& pa = defaultParameters);
    Function(vfunVR_t& f, Parameters& pa = defaultParameters);
    Function(vfunVC_t& f, Parameters& pa = defaultParameters);
    Function(vfunMR_t& f, Parameters& pa = defaultParameters);
    Function(vfunMC_t& f, Parameters& pa = defaultParameters);
    Function(vkerSR_t& f, Parameters& pa = defaultParameters);
    Function(vkerSC_t& f, Parameters& pa = defaultParameters);
    Function(vkerVR_t& f, Parameters& pa = defaultParameters);
    Function(vkerVC_t& f, Parameters& pa = defaultParameters);
    Function(vkerMR_t& f, Parameters& pa = defaultParameters);
    Function(vkerMC_t& f, Parameters& pa = defaultParameters);
    //@}
    //@{
    //! constructor along the function prototypes
    Function(funSR_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(funSC_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(funVR_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(funVC_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(funMR_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(funMC_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(kerSR_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(kerSC_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(kerVR_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(kerVC_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(kerMR_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(kerMC_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(vfunSR_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(vfunSC_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(vfunVR_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(vfunVC_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(vfunMR_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(vfunMC_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(vkerSR_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(vkerSC_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(vkerVR_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(vkerVC_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(vkerMR_t& f, dimen_t d, Parameters& pa = defaultParameters);
    Function(vkerMC_t& f, dimen_t d, Parameters& pa = defaultParameters);
    //@}
    //@{
    //! constructor with function name (string) along the function prototypes
    Function(funSR_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(funSC_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(funVR_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(funVC_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(funMR_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(funMC_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(kerSR_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(kerSC_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(kerVR_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(kerVC_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(kerMR_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(kerMC_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(vfunSR_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(vfunSC_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(vfunVR_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(vfunVC_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(vfunMR_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(vfunMC_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(vkerSR_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(vkerSC_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(vkerVR_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(vkerVC_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(vkerMR_t& f, const string_t& na, Parameters& pa = defaultParameters);
    Function(vkerMC_t& f, const string_t& na, Parameters& pa = defaultParameters);
    //@}
    //@{
    //! constructor with function name (string) along the function prototypes
    Function(funSR_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(funSC_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(funVR_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(funVC_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(funMR_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(funMC_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(kerSR_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(kerSC_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(kerVR_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(kerVC_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(kerMR_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(kerMC_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(vfunSR_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(vfunSC_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(vfunVR_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(vfunVC_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(vfunMR_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(vfunMC_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(vkerSR_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(vkerSC_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(vkerVR_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(vkerVC_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(vkerMR_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    Function(vkerMC_t& f, dimen_t d, const string_t& na, Parameters& pa = defaultParameters);
    //@}
    //@{
    //! constructor with function name (const char*) along the function prototypes
    Function(funSR_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(funSC_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(funVR_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(funVC_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(funMR_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(funMC_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(kerSR_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(kerSC_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(kerVR_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(kerVC_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(kerMR_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(kerMC_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(vfunSR_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(vfunSC_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(vfunVR_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(vfunVC_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(vfunMR_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(vfunMC_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(vkerSR_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(vkerSC_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(vkerVR_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(vkerVC_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(vkerMR_t& f, const char* na, Parameters& pa = defaultParameters);
    Function(vkerMC_t& f, const char* na, Parameters& pa = defaultParameters);
    //@}
    //@{
    //! constructor with function name (const char*) along the function prototypes
    Function(funSR_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(funSC_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(funVR_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(funVC_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(funMR_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(funMC_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(kerSR_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(kerSC_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(kerVR_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(kerVC_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(kerMR_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(kerMC_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(vfunSR_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(vfunSC_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(vfunVR_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(vfunVC_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(vfunMR_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(vfunMC_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(vkerSR_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(vkerSC_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(vkerVR_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(vkerVC_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(vkerMR_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    Function(vkerMC_t& f, dimen_t d, const char* na, Parameters& pa = defaultParameters);
    //@}

    // assigment operator = is the default one (no hard copy)

    // templated overloading operator (), safe method
    // using reference as input and output argument (auto resizment if size is too small)!
    template <typename T>
    T& operator()(const Point& x, T& res) const;                   //!< compute the value of the function at a given point
    template <typename T>
    T& operator()(const Point& x, const Point& y, T& res) const;   //!< compute the value of the function at a given couple of points
    template <typename T>
    Vector<T>& operator()(const Vector<Point>& x, Vector<T>& res) const;                         //!< compute the values of the function at a given vector of points
    template <typename T>
    Vector<T>& operator()(const Vector<Point>& x, const Vector<Point>& y, Vector<T>& res) const; //!< compute the values of the function at a given couple of vector of points
    Value operator()(const Point& x) const;   //!< evaluate function at a point, returning a Value
    Value operator()(const Point& x, const Point& y) const;   //!< evaluate kernel function at a couple of points, returning a Value

    // call function with no control (a bit faster)
    // be cautious because the void pointer storing pointer function is recasted with no control!

    //@{
    //! compute the value(s) of the function using correct type function (no check performed)
    real_t funSR(const Point& x) const
    { return reinterpret_cast<funSR_t*>(fun_)(x, *params_); }
    complex_t funSC(const Point& x) const
    { return reinterpret_cast<funSC_t*>(fun_)(x, *params_); }
    Vector<real_t> funVR(const Point& x) const
    { return reinterpret_cast<funVR_t*>(fun_)(x, *params_); }
    Vector<complex_t> funVC(const Point& x) const
    { return reinterpret_cast<funVC_t*>(fun_)(x, *params_); }
    Matrix<real_t> funMR(const Point& x) const
    { return reinterpret_cast<funMR_t*>(fun_)(x, *params_); }
    Matrix<complex_t> funMC(const Point& x) const
    { return reinterpret_cast<funMC_t*>(fun_)(x, *params_); }
    Vector<real_t> vfunSR(const Vector<Point>& x) const
    { return reinterpret_cast<vfunSR_t*>(fun_)(x, *params_); }
    Vector<complex_t> vfunSC(const Vector<Point>& x) const
    { return reinterpret_cast<vfunSC_t*>(fun_)(x, *params_); }
    Vector<Vector<real_t> > vfunVR(const Vector<Point>& x) const
    { return reinterpret_cast<vfunVR_t*>(fun_)(x, *params_); }
    Vector<Vector<complex_t> > vfunVC(const Vector<Point>& x) const
    { return reinterpret_cast<vfunVC_t*>(fun_)(x, *params_); }
    Vector<Matrix<real_t> > vfunMR(const Vector<Point>& x) const
    { return reinterpret_cast<vfunMR_t*>(fun_)(x, *params_); }
    Vector<Matrix<complex_t> > vfunMC(const Vector<Point>& x) const
    { return reinterpret_cast<vfunMC_t*>(fun_)(x, *params_); }
    real_t kerSR(const Point& x, const Point& y) const
    { return reinterpret_cast<kerSR_t*>(fun_)(x, y, *params_); }
    complex_t kerSC(const Point& x, const Point& y) const
    { return reinterpret_cast<kerSC_t*>(fun_)(x, y, *params_); }
    Vector<real_t> kerVR(const Point& x, const Point& y) const
    { return reinterpret_cast<kerVR_t*>(fun_)(x, y, *params_); }
    Vector<complex_t> kerVC(const Point& x, const Point& y) const
    { return reinterpret_cast<kerVC_t*>(fun_)(x, y, *params_); }
    Matrix<real_t> kerMR(const Point& x, const Point& y) const
    { return reinterpret_cast<kerMR_t*>(fun_)(x, y, *params_); }
    Matrix<complex_t> kerMC(const Point& x, const Point& y) const
    { return reinterpret_cast<kerMC_t*>(fun_)(x, y, *params_); }
    Vector<real_t> vkerSR(const Vector<Point>& x, const Vector<Point>& y) const
    { return reinterpret_cast<vkerSR_t*>(fun_)(x, y, *params_); }
    Vector<complex_t> vkerSC(const Vector<Point>& x, const Vector<Point>& y) const
    { return reinterpret_cast<vkerSC_t*>(fun_)(x, y, *params_); }
    Vector<Vector<real_t> > vkerVR(const Vector<Point>& x, const Vector<Point>& y) const
    { return reinterpret_cast<vkerVR_t*>(fun_)(x, y, *params_); }
    Vector<Vector<complex_t> > vkerVC(const Vector<Point>& x, const Vector<Point>& y) const
    { return reinterpret_cast<vkerVC_t*>(fun_)(x, y, *params_); }
    Vector<Matrix<real_t> > vkerMR(const Vector<Point>& x, const Vector<Point>& y) const
    { return reinterpret_cast<vkerMR_t*>(fun_)(x, y, *params_); }
    Vector<Matrix<complex_t> > vkerMC(const Vector<Point>& x, const Vector<Point>& y) const
    { return reinterpret_cast<vkerMC_t*>(fun_)(x, y, *params_); }
    //@}

    const TermVector* termVector() const;  //!< return TermVector pointer when interpolated function (0 if not)
    Point termVectorPoint() const;          //!<return a dof coordinate from TermVector if interpolated function

    //! tabular stuff
    template <typename T>
    Function(Tabular<T>& tab, dimen_t dp, const string_t& na="", FunctType ft =_function); //!< constructor from a Tabular
    template <typename T>
    Function(Tabular<T>& tab, dimen_t dp, Function& fpx, const string_t& na="", FunctType ft =_function);  //!< constructor from a Tabular
    template<typename T>
    T& funTable(const Point& x, T& res) const;        //!< compute function using table_
    template<typename T>
    T& kerTable(const Point& x, const Point& y, T& res) const;  //!< compute kernel using table_
    void createTabular(real_t x0, real_t dx, number_t nx, const string_t& nax="x"); //!< tabulating f(x)
    void createTabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny,
                       const string_t& nax="x", const string_t& nay="y"); //!< tabulating f(x,y)
    void createTabular(real_t x0, real_t dx, number_t nx,
                       real_t y0, real_t dy, number_t ny,
                       real_t z0, real_t dz, number_t nz,
                       const string_t& nax="x", const string_t& nay="y", const string_t& naz="z"); //!< tabulating f(x,y,z)
    template <typename T>
    void createTabularT(const T& t, real_t x0, real_t dx, number_t nx, const string_t& nax="x"); //!< tabulating f(x) - template
    template <typename T>
    void createTabularT(const T& t, real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny,
                        const string_t& nax="x", const string_t& nay="y"); //!< tabulating f(x,y)) - template
    template <typename T>
    void createTabularT(const T& t, real_t x0, real_t dx, number_t nx,
                        real_t y0, real_t dy, number_t ny,
                        real_t z0, real_t dz, number_t nz,
                        const string_t& nax="x", const string_t& nay="y", const string_t& naz="z"); //!< tabulating f(x,y,z) - template

}; // end of class Function

std::ostream& operator<<(std::ostream&, const Function &); //!< print operator

//external functions
bool operator==(const Function&, const Function&); //!< compare functions (same fun and params pointers)
bool operator!=(const Function&, const Function&); //!< compare functions (same fun and params pointers)

template<typename K>
void Function::setParam(const K& v, const string_t& name) const //! set parameter name to v
{
  if (params_ == nullptr) params_ = new Parameters(v,name);
  else
  {
    if (params_->contains(name)) (*params_)(name) = v;
    else (*params_) << Parameter(v,name);
  }
}

//! tabular stuff
template <typename T>
Function::Function(Tabular<T>& tab, dimen_t dp, const string_t& na, FunctType ft)  //! constructor from a Tabular
{
  table_ = &tab;
  functionType_ = ft;
  pointToTabx_ = nullptr;
  T v = tab[0];
  returnedType_=xlifepp::valueType(v);
  returnedStruct_=xlifepp::strucType(v);
  argType_= _pointArg;
  dimPoint_= dp;
  dims_=dimPair(xlifepp::sizeOf(v),1);
  name_ = na;
  fun_=nullptr; grad_=nullptr;
  freeParams_ = false; checkType_=false;
  conjugate_ = false; transpose_ = false;
  requireNx=false;    requireNy=false;
  requireElt=false;   requireDom=false;
  requireDof=false;
}

template <typename T>
Function::Function(Tabular<T>& tab, dimen_t dp, Function& fpx, const string_t& na, FunctType ft)   //! constructor from a Tabular
{
  table_ = &tab;
  functionType_ = ft;
  pointToTabx_ = &fpx;
  T v = tab[0];
  returnedType_=xlifepp::valueType(v);
  returnedStruct_=xlifepp::strucType(v);
  argType_= _pointArg;
  dimPoint_= dp;
  dims_=dimPair(xlifepp::sizeOf(v),1);
  name_ = na;
  fun_=nullptr; grad_=nullptr;
  freeParams_ = false; checkType_=false;
  conjugate_ = false; transpose_ = false;
  requireNx=false;    requireNy=false;
  requireElt=false;   requireDom=false;
  requireDof=false;
}

template<typename T>
inline T& Function::funTable(const Point& x, T& res) const        //! compute function using table_
{
  //assume table_ and fpx are initialized
  const Tabular<T>& ft= *static_cast<const Tabular<T>*>(table_);
  if(pointToTabx_==nullptr) return (res=ft(x));
  if(pointToTabx_->strucType()==_scalar)
    {
      real_t vx;
      return (res=ft((*pointToTabx_)(x,vx)));
    }
  Vector<real_t> vx;
  return (res=ft((*pointToTabx_)(x,vx)));
}

template<typename T>
inline T& Function::kerTable(const Point& x, const Point& y, T& res) const  //! compute kernel using table_
{
  //assume table_ and pointToTabx_ are initialized
  const Tabular<T>& ft= *static_cast<const Tabular<T>*>(table_);
  if(pointToTabx_==nullptr)
    {
      std::vector<real_t> xy(x.size()+y.size());
      std::vector<real_t>::iterator itxy=xy.begin();
      for(number_t k=0; k<x.size(); k++,++itxy) *itxy=x[k];
      for(number_t k=0; k<y.size(); k++,++itxy) *itxy=y[k];
      return (res=ft(xy));
    }
  Vector<real_t> vxy;
  return (res=ft((*pointToTabx_)(x,y,vxy)));
}

template <typename T>
void Function::createTabularT(const T& t, real_t x0, real_t dx, number_t nx, const string_t& nax) //! tabulating f(x) - template
{
  if(functionType_==_kernel) error("free_error", "kernel cannot be tabulated yet");
  Tabular<T>* tab=new Tabular<T>(x0,dx,nx,nax);
  typename std::vector<T>::iterator itv=tab->begin();
  real_t x = x0;
  for(number_t i=0; i<=nx; i++, ++itv, x+=dx) *itv = (*this)(Point(x),*itv);
  table_=tab;
}

template <typename T>
void Function::createTabularT(const T& t, real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny,
                              const string_t& nax, const string_t& nay) //! tabulating f(x,y)) - template
{
  if(functionType_==_kernel) error("free_error", "kernel cannot be tabulated yet");
  Tabular<T>* tab=new Tabular<T>(x0,dx,nx,y0,dy,ny,nax,nay);
  typename std::vector<T>::iterator itv=tab->begin();
  real_t x = x0;
  for(number_t i=0; i<=nx; i++, x+=dx)
    {
      real_t y=y0;
      for(number_t j=0; j<=ny; j++, ++itv, y+=dy) *itv = (*this)(Point(x,y),*itv);
    }
  table_=tab;
}

template <typename T>
void Function::createTabularT(const T& t, real_t x0, real_t dx, number_t nx,
                              real_t y0, real_t dy, number_t ny,
                              real_t z0, real_t dz, number_t nz,
                              const string_t& nax, const string_t& nay, const string_t& naz) //! tabulating f(x,y,z) - template
{
  if(functionType_==_kernel) error("free_error", "kernel cannot be tabulated yet");
  Tabular<T>* tab = new Tabular<T>(x0,dx,nx,y0,dy,ny,z0,dz,nz,nax,nay,naz);
  typename std::vector<T>::iterator itv=tab->begin();
  real_t x = x0;
  for(number_t i=0; i<=nx; i++, x+=dx)
    {
      real_t y=y0;
      for(number_t j=0; j<=ny; j++, y+=dy)
        {
          real_t z=z0;
          for(number_t k=0; k<=nz; k++, ++itv, z+=dz) *itv = (*this)(Point(x,y,z),*itv);
        }
    }
  table_=tab;
}

// templated overloading operator () using reference as input/output argument !!!
// - general method which supports as well function of single point or vector of points as input
// - WARNING! the type T giving by the user has to be compatible with the definition of the function
// by activate the checkType (checkTypeOn) a check of arguments is performed;
// the checkType is then automatically set to off to avoid unnecessary rechecking

/*!
  compute in a safe way the value of the function at a given point.
  the function is either a function of a single point or a function of a vector of points
  n, nx or ny are additional normal vectors
*/
template<typename T>
inline T& Function::operator()(const Point& x, T& res) const
{
  if(checkType_)checkFunctionType(res, _function); // check the type of arguments
  if(argType_ == _pointArg)  // case of a function of a point
    {
      if(functionType_==_function)
        {
          if(table_==nullptr)
            {
              typedef typename PointerF<T>::fun_t* funT;
              funT f = reinterpret_cast<funT>(fun_);
              res = f(x, *params_);
            }
          else // use Tabular
            {
              res=funTable(x,res);
            }
        }
      else //kernel type
        {
          if(table_==nullptr)
            {
              typedef typename PointerF<T>::ker_t* kerT;
              kerT f = reinterpret_cast<kerT>(fun_);
              if(xpar)  res = f(xory, x, *params_); // x is a parameter, function of y
              else      res = f(x, xory, *params_); // y is a parameter, function of x
            }
          else
            {
              if(xpar)  res=kerTable(xory,x,res); // x is a parameter, function of y
              else      res=kerTable(x,xory,res); // y is a parameter, function of x
            }
        }
    }
  else // case of a function of a vector of points (construct a temporary vector)
    {
      Vector<T> R;
      Vector<Point> Vx(1, x);
      if(functionType_==_function)
        {
          typedef typename PointerF<T>::vfun_t* vfunT;
          vfunT f = reinterpret_cast<vfunT>(fun_);
          R = f(Vx, *params_);
        }
      else //kernel type
        {
          Vector<Point> Vxy(1, xory);
          typedef typename PointerF<T>::vker_t* vkerT;
          vkerT f = reinterpret_cast<vkerT>(fun_);
          if(xpar) R = f(Vxy, Vx, *params_);
          else     R = f(Vx, Vxy, *params_);
        }
      res = R[0];
    }
  if(transpose_) res=tran(res);
  if(conjugate_) res=conj(res);
  return res;
}

/*!
  compute in a safe way the value of the kernel function at a given couple of points.
  the function is either a function of a couple of points or a function of a couple of vectors of points
*/
template<typename T>
inline T& Function::operator()(const Point& x, const Point& y, T& res) const
{
  if(checkType_) checkFunctionType(res, _kernel); // check the type of arguments
  if(argType_ == _pointArg)  // case of a function of a couple of points
    {
      if(table_==nullptr)
        {
          typedef typename PointerF<T>::ker_t* kerT;
          kerT f = reinterpret_cast<kerT>(fun_);
          res = f(x, y, *params_);
        }
      else
        {
          res=kerTable(x,y,res);
        }
    }
  else // case of a function of a couple of vectors of points (construction of temporary vectors)
    {
      Vector<T> R;
      typedef typename PointerF<T>::vker_t* vkerT;
      vkerT f = reinterpret_cast<vkerT>(fun_);
      Vector<Point> U(1, x), V(1, y);
      R = f(U, V, *params_);
      res = R[0];
    }
  if(transpose_) res=tran(res);
  if(conjugate_) res=conj(res);
  return res;
}

/*!
  compute in a safe way the values of the function at a vector of points.
  the function is either a function of a point or a function of a vector of points
*/
template<typename T>
inline Vector<T>& Function::operator()(const Vector<Point>& x, Vector<T>& res) const
{
  T R;
  if(checkType_) checkFunctionType(R, _function); // check the type of arguments
  if(argType_ == _vectorOfPointArg)  // case of a function of a vector of points
    {
      typedef typename PointerF<T>::vfun_t* vfunT;
      vfunT f = reinterpret_cast<vfunT>(fun_);
      res = f(x, *params_);
    }
  else // case of a function of a point (loop on points)
    {
      size_t n = x.size();
      if(res.size() < n) res.resize(n); // forcing resizement
      if(table_==nullptr)
        {
          typedef typename PointerF<T>::fun_t* funT;
          funT f = reinterpret_cast<funT>(fun_);
          for(size_t i = 0; i < n; i++) res[i] = f(x[i], *params_);
        }
      else
        {
          for(size_t i = 0; i < n; i++) res[i] = funTable(x[i],res[i]);
        }
    }
  if(transpose_) res=tran(res);
  if(conjugate_) res=conj(res);
  return res;
}

/*!
  compute in a safe way the value of the kernel function at a couple of vectors of points.
  the function is either a function of a couple of points or a function of a couple of vectors of points
*/
template<typename T>
inline Vector<T>& Function::operator()(const Vector<Point>& x, const Vector<Point>& y, Vector<T>& res) const
{
  // check the compatibility of size of vectors x and y
  if(x.size() != y.size()) { error("ker_bad_dim", name(), x.size(), y.size()); }
  T R;
  if(checkType_) checkFunctionType(R, _kernel); // check the type of arguments
  if(argType_ == _vectorOfPointArg)  // case of a function of a couple of vectors of points
    {
      typedef typename PointerF<T>::vker_t* vkerT;
      vkerT f = reinterpret_cast<vkerT>(fun_);
      res = f(x, y, *params_);
    }
  else // case of a function of a couple of points (loop on points)
    {
      size_t n = x.size();
      if(res.size() < n) res.resize(n); // forcing resizement
      if(table_==nullptr)
        {
          typedef typename PointerF<T>::ker_t* funT;
          funT f = reinterpret_cast<funT>(fun_);
          for(size_t i = 0; i < n; i++) res[i] = f(x[i], y[i], *params_);
        }
      else
        {
          for(size_t i = 0; i < n; i++) res[i] = kerTable(x[i], y[i], res[i]);
        }
    }

  if(transpose_) res=tran(res);
  if(conjugate_) res=conj(res);
  return res;
}

// string utilities
string_t type2Str(ValueType t);
string_t struct2Str(StrucType s);
string_t typeFun2Str(FunctType tf);
string_t typeArg2Str(ArgType ta);

/*!
  to check the type of the function when computing its value using the operator ().
  This function uses Run Time Information (RTTI), so it must be used sparingly
  for this reason, when it is invoked, the checkType_ value is reset to false
 */
template<class T>
inline void Function::checkFunctionType(T& res, FunctType tf) const
{
  if(tf == _function)
    {
      isNotAFunction();
    }
  else
    {
      isNotAKernel();
    }

  std::pair<ValueType, StrucType> tsres = returnArgs[string_t(typeid(res).name())];
  std::pair<ValueType, StrucType> tsfun(returnedType_, returnedStruct_);

  if(tsres != tsfun)   // no compatible call
    {
      error("fun_bad_args", name_, type2Str(returnedType_), struct2Str(returnedStruct_), type2Str(tsres.first), struct2Str(tsres.second));
    }

  checkType_ = false;
}

// special operations conj, tran and adj of function
// create Function object on the fly
template <typename T>
inline Function& conj(T(fun)(const Point&, Parameters&))
{
  Function& f = *(new Function(*fun));
  f.conjugate(true);
  return f;
}
template <typename T>
inline Function& conj(T(fun)(const Vector<Point>&, Parameters&))
{
  Function& f = *(new Function(*fun));
  f.conjugate(true);
  return f;
}
template <typename T>
inline Function& conj(T(fun)(const Point&, const Point&, Parameters&))
{
  Function& f = *(new Function(*fun));
  f.conjugate(true);
  return f;
}
template <typename T>
inline Function& conj(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&))
{
  Function& f = *(new Function(*fun));
  f.conjugate(true);
  return f;
}
template <typename T>
inline Function& tran(T(fun)(const Point&, Parameters&))
{
  Function& f = *(new Function(*fun));
  f.transpose(true);
  return f;
}
template <typename T>
inline Function& tran(T(fun)(const Vector<Point>&, Parameters&))
{
  Function& f = *(new Function(*fun));
  f.transpose(true);
  return f;
}
template <typename T>
inline Function& tran(T(fun)(const Point&, const Point&, Parameters&))
{
  Function& f = *(new Function(*fun));
  f.transpose(true);
  return f;
}
template <typename T>
inline Function& tran(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&))
{
  Function& f = *(new Function(*fun));
  f.transpose(true);
  return f;
}
template <typename T>
inline Function& adj(T(fun)(const Point&, Parameters&))
{
  Function& f = *(new Function(*fun));
  f.conjugate(true);
  f.transpose(true);
  return f;
}
template <typename T>
inline Function& adj(T(fun)(const Vector<Point>&, Parameters&))
{
  Function& f = *(new Function(*fun));
  f.conjugate(true);
  f.transpose(true);
  return f;
}
template <typename T>
inline Function& adj(T(fun)(const Point&, const Point&, Parameters&))
{
  Function& f = *(new Function(*fun));
  f.conjugate(true);
  f.transpose(true);
  return f;
}
template <typename T>
inline Function& adj(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&))
{
  Function& f = *(new Function(*fun));
  f.conjugate(true);
  f.transpose(true);
  return f;
}

//some constant function used to create internal constant Function object
real_t real_const_fun(const Point&, Parameters& pa = defaultParameters);
complex_t complex_const_fun(const Point&, Parameters& pa = defaultParameters);
Vector<real_t> real_vector_const_fun(const Point&, Parameters& pa = defaultParameters);
Vector<complex_t> complex_vector_const_fun(const Point&, Parameters& pa = defaultParameters);
Matrix<real_t> real_matrix_const_fun(const Point&, Parameters& pa = defaultParameters);
Matrix<complex_t> complex_matrix_const_fun(const Point&, Parameters& pa = defaultParameters);

//default function Point to (x,y,z)
inline Vector<real_t> point_to_xyz(const Point& p, Parameters& pa = defaultParameters) {return p;}

//=================================================================================
/*! ComparisonFunction class handling expresion  t opc1 a1 ao t opc2 a2 ao ...
    where opc1, opc2, ... are one of =, !=, <, > , >=, <=
          ao is one of && or || (the same in expression)
          a1, a2, ... are objects of same type, supporting comparison operator
    let cop  be a ComparisonFunction object
        cof(t) return the bool value of t opc1 a opao t opc2 2    */
//=================================================================================
enum ComparisonOperator {_noComparison,_isEqual, _isNotEqual, _isLess, _isGreater,_isLessEqual,_isGreaterEqual};
enum VarComparison {_index,_color};

template<typename T = real_t>
class ComparisonFunction : std::list<std::pair<ComparisonOperator,T> >
{
  public:
    typedef typename std::pair<ComparisonOperator,T> ComPair;
    typedef typename std::list<std::pair<ComparisonOperator,T> >::iterator iterator;
    typedef typename std::list<std::pair<ComparisonOperator,T> >::const_iterator const_iterator;

    bool isAnd;                //!< true if boolean operator is and, false if it is or, (default is and)

    ComparisonFunction(bool isand=true) : isAnd(isand) {}
    ComparisonFunction(ComparisonOperator co, const T& a) : isAnd(true) {this->push_back(ComPair(co,a));}

    number_t size() const {return std::list<std::pair<ComparisonOperator,T> >::size();}
    iterator begin() {return std::list<std::pair<ComparisonOperator,T> >::begin();}
    const_iterator begin() const {return std::list<std::pair<ComparisonOperator,T> >::begin();}
    iterator end() {return std::list<std::pair<ComparisonOperator,T> >::end();}
    const_iterator end() const {return std::list<std::pair<ComparisonOperator,T> >::end();}

    void add(const ComparisonFunction<T>& cof)
    {std::list<std::pair<ComparisonOperator,T> >::insert(end(),cof.begin(),cof.end());}

    bool operator()(const T& v) const
    {
      if(size()==0) error("is_void","list");
      if(isAnd)
        {
          for(const_iterator itl=begin(); itl!=end(); ++itl)
            {
              const T& a=itl->second;
              switch(itl->first)
                {
                  case _isEqual: if(v!=a) return false; break;
                  case _isNotEqual: if(v==a) return false; break;
                  case _isLess: if(v>=a) return false; break;
                  case _isGreater: if(v<=a) return false; break;
                  case _isLessEqual: if(v>a) return false; break;
                  case _isGreaterEqual: if(v<a) return false;
                  default: break;
                }
            }
          return true;
        }
      for(const_iterator itl=begin(); itl!=end(); ++itl)
        {
          const T& a=itl->second;
          switch(itl->first)
            {
              case _isEqual: if(v==a) return true; break;
              case _isNotEqual: if(v!=a) return true; break;
              case _isLess: if(v<a) return true; break;
              case _isGreater: if(v>a) return true; break;
              case _isLessEqual: if(v<=a) return true; break;
              case _isGreaterEqual: if(v>=a) return true;
              default: break;
            }
        }
      return false;
    }

    void print(std::ostream&) const;
    void print(PrintStream& os) const {print(os.currentStream());}

    friend std::ostream& operator<<(std::ostream& out, const ComparisonFunction& cof)
    {cof.print(out); return out;}
};

template<typename T>
void ComparisonFunction<T>::print(std::ostream& out) const
{
  string_t orand = " || ";
  if(isAnd) orand = " && ";
  if(size()<2) orand="";
  bool notlast=true;
  number_t i=1;
  for(const_iterator itl=begin(); itl!=end(); ++itl, ++i)
    {
      const T& a=itl->second;
      notlast=(i!=size());
      switch(itl->first)
        {
          case _isEqual: out<<"t == "<<a;
            if(notlast) out<<orand;
            break;
          case _isNotEqual:out<<"t != "<<a;
            if(notlast) out<<orand;
            break;
          case _isLess: out<<"t < "<<a;
            if(notlast) out<<orand;
            break;
          case _isGreater: out<<"t > "<<a;
            if(notlast) out<<orand;
            break;
          case _isLessEqual: out<<"t <= "<<a;
            if(notlast) out<<orand;
            break;
          case _isGreaterEqual: out<<"t >= "<<a;
            if(notlast) out<<orand;
        }
    }
}

template<typename T>
inline ComparisonFunction<T> operator==(VarComparison, const T& a)
{return ComparisonFunction<T>(_isEqual,a);}

template<typename T>
inline ComparisonFunction<T> operator!=(VarComparison, const T& a)
{return ComparisonFunction<T>(_isNotEqual,a);}

template<typename T>
inline ComparisonFunction<T> operator<(VarComparison, const T& a)
{return ComparisonFunction<T>(_isLess,a);}

template<typename T>
inline ComparisonFunction<T> operator>(VarComparison, const T& a)
{return ComparisonFunction<T>(_isGreater,a);}

template<typename T>
inline ComparisonFunction<T> operator<=(VarComparison, const T& a)
{return ComparisonFunction<T>(_isLessEqual,a);}

template<typename T>
inline ComparisonFunction<T> operator>=(VarComparison, const T& a)
{return ComparisonFunction<T>(_isGreaterEqual,a);}

template<typename T>
ComparisonFunction<T> operator &&(const ComparisonFunction<T>& cof1, const ComparisonFunction<T>& cof2)
{
  bool isand1 = cof1.isAnd, isand2 = cof2.isAnd;
  if(cof1.size()<2) isand1=true;
  if(cof2.size()<2) isand2=true;
  if (!isand1 || !isand2) error("mixed_and_or_not_handled");
  ComparisonFunction<T> cof(cof1);
  cof.isAnd=true;
  cof.add(cof2);
  return cof;
}

template<typename T>
ComparisonFunction<T> operator||(const ComparisonFunction<T>& cof1, const ComparisonFunction<T>& cof2)
{
  bool isand1 = cof1.isAnd, isand2 = cof2.isAnd;
  if(cof1.size()<2) isand1=false;
  if(cof2.size()<2) isand2=false;
  if(isand1 || isand2) error("mixed_and_or_not_handled");
  ComparisonFunction<T> cof(cof1);
  cof.isAnd=false;
  cof.add(cof2);
  return cof;
}

} // end of namespace xlifepp

#endif /* FUNCTION_HPP */
