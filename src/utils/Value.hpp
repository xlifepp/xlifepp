/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Value.hpp
  \author E. Lunéville
  \since 02 mar 2012
  \date 9 may 2012

  \brief Definition of the xlifepp::Value class

  Class xlifepp::Value describes a general numerical value which can be of type
  scalar, vector, matrix, vector of vectors or matrices, matrix of matrices
  with real or complex values
  it allows to encapsulate any user values in one class
  Normally, end user has not to use it explicitely
 */

#ifndef VALUE_HPP
#define VALUE_HPP

#include "config.h"
#include "String.hpp"
#include "Vector.hpp"
#include "Matrix.hpp"
#include "Messages.hpp"
#include "Value.hpp"
#include <typeinfo>

namespace xlifepp
{

//useful alias
typedef std::pair<ValueType, StrucType> structPair;

/*!
  \class Value
  to encapsulate any kind of users value (scalar, Vector or Matrix)
*/
class Value
{
  protected:
    ValueType type_;         //!< type of returned value (one among _real, _complex)
    StrucType struct_;       //!< structure of returned value (one among _scalar, _vector, _matrix)
    void* value_p;           //!< void pointeur to the true value

    Value() : type_(_real), struct_(_scalar), value_p(nullptr) {} //!< private default constructor

  public:
    mutable bool conjugate_; //!< temporary conjugate state flag (use in operator construction)
    mutable bool transpose_; //!< temporary transposition state flag (use in operator construction)

  public:
    typedef std::pair<string_t, structPair> mapPair;

    // static definitions
    static std::map<string_t, structPair > theValueTypeRTInames; //!< list of RTI names of supported types
    static void valueTypeRTINamesInit();                       //!< initialization of RTI names of supported types
    template<typename T>
    static bool checkTypeInList(const T&);                     //!< check if a value is in list of supported types
    template<typename T>
    static bool isTypeInList(const T&);                        //!< check if a value is in list of supported types
    template<typename T>
    static structPair typeOf(const T& v);                      //!< return types of a T value

    Value(const Value&);                                       //!< copy constructor
    void copyValue(const Value&);                              //!< full copy of value
    void clearValue();                                         //!< clear value
    Value& operator=(const Value&);                            //!< assign Value

    //@{
    //! constructors only for supported types
    Value(const real_t&);
    Value(const Vector<real_t>&);
    Value(const Matrix<real_t>&);
    Value(const Vector<Vector<real_t> >&);
    Value(const Vector<Matrix<real_t> >&);
    Value(const Matrix<Matrix<real_t> >&);
    Value(const complex_t&);
    Value(const Vector<complex_t>&);
    Value(const Matrix<complex_t>&);
    Value(const Vector<Vector<complex_t> >&);
    Value(const Vector<Matrix<complex_t> >&);
    Value(const Matrix<Matrix<complex_t> >&);
    //@}
    ~Value(); //!< destructor

    template<typename T>
    void checkType(const T&) const;                 //!< check if T is an authorized type
    template<class T> T& valueSafe() const;         //!< return object as value of T type (type check is performed)
    template<class T> T value() const;              //!< return object as value of T type (const, no type check)
    template<class T> T& value() ;                  //!< return object as value of T type (no type check)
    ValueType valueType() const {return type_;}     //!< return the value type (real, complex)
    StrucType strucType() const {return struct_;}   //!< return the structure type (scalar, vector, matrix)
    dimPair dims() const;                           //!< return dimensions of value, (1,1) in scalar case

    friend Value& conj(Value&);
    friend Value& trans(Value&);
    friend Value& adj(Value&);
    void opposite() ;                               //!< change the sign of value

    //get value
    real_t asReal() const;
    complex_t asComplex() const;
    Vector<real_t> asRealVector() const;
    Vector<complex_t> asComplexVector() const;
    Matrix<real_t> asRealMatrix() const;
    Matrix<complex_t> asComplexMatrix() const;

    //algebraic operator
    Value& moveToComplex();
    Value& operator+=(const Value&);
    Value& operator-=(const Value&);
    Value& operator*=(const Value&);
    Value& operator/=(const Value&);

    // print utility
    static void printValueTypeRTINames(std::ostream&);            //!< print RTI names
    void print(std::ostream&) const;                              //!< print Value on a output stream
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator<<(std::ostream&, const Value&);

}; // end of class Value

//=================================================================================
// external function
//=================================================================================
std::ostream& operator<<(std::ostream&, const Value&); //!< outputs Value in a output stream

Value& conj(Value&);                     //!< set to true or false the temporary conjugate flag
Value& trans(Value&);                    //!< set to true or false the temporary transpose flag
Value& adj(Value&);                      //!< set to true or false the temporary transpose/conjugate flag

bool operator==(const Value&, const Value&); //!<compare values
bool operator!=(const Value&, const Value&); //!<compare values

// construct in the memory a conjugate/transpose Value from a scalar, a Vector or a Matrix
// possible memory leak when chaining construction!
// note that the type T is implicitely controled by the Value constructors which allow only few types
// note that the conj and trans defined for type T, if they exist, are prior to the folowing
template<typename T>
Value& conj(const T& v)
{
  Value* vp = new Value(v);
  vp->conjugate_=true;
  return *vp;
}

template<typename T>
Value& trans(const T& v)
{
  Value* vp = new Value(v);
  vp->transpose_=true;
  return *vp;
}

template<typename T>
Value& adj(const T& v)
{
  Value* vp = new Value(v);
  vp->transpose_=true;
  vp->conjugate_=true;
  return *vp;
}

Value operator-(const Value&);   //opposite of a value

//=================================================================================
// implementation of template member functions
//=================================================================================

// check if T is an authorized type (Value, real or complex scalar, vector, matrix)
// stop on error if T is unauthorized type
// return true when T is a Value object
template<typename T>
bool Value::checkTypeInList(const T& v)
{
  std::map<string_t, structPair>::iterator it = theValueTypeRTInames.find(typeid(v).name());
  if (it == theValueTypeRTInames.end()) // type not found in list of value types
  {
    error("value_badtype", typeid(v).name());
  }
  return it == theValueTypeRTInames.begin(); // true when T is a value (first item of theValueTypeRTInames)
}

//
template<typename T>
bool Value::isTypeInList(const T& v)
{
  std::map<string_t, structPair>::iterator it = theValueTypeRTInames.find(typeid(v).name());
  return !(it == theValueTypeRTInames.end());
}


// check if T is of type of current value
template<typename T>
void Value::checkType(const T& v) const
{
  std::map<string_t, structPair>::iterator it = theValueTypeRTInames.find(typeid(v).name());
  if (it == theValueTypeRTInames.end())   // type not found in list of value types
  {
    error("value_badtype", typeid(v).name());
  }
  if((it->second).first != type_ || (it->second).second != struct_)
  {
    error("value_wrongtype", words("value", type_), words("structure", struct_), words("value", (it->second).first), words("structure", (it->second).second));
  }
}

//return structPair=std::pair<ValueType, StrucType> of a value of type T
template<typename T>
structPair Value::typeOf(const T& v)
{
  std::map<string_t, structPair>::iterator it = theValueTypeRTInames.find(typeid(v).name());
  if (it == theValueTypeRTInames.end())   // type not found in list of value types
  {
    error("value_badtype", typeid(v).name());
  }
  return it->second;
}


// cast void pointeur to type T (a reference, not a pointeur!)
// use RTI lib, do not use at low level
template<typename T> T& Value::valueSafe() const
{
  T r; checkType(r);
  T* vp = reinterpret_cast<T*>(value_p);
  return *vp;
}

// cast void pointeur to type T (a reference, not a pointeur!)
// no check of type
//template<typename T> T& Value::value() const
//{
//  T* vp = reinterpret_cast<T*>(value_p);
//  return *vp;
//}

template<> real_t Value::value<real_t>() const;
template<> complex_t Value::value<complex_t>() const;
template<> Vector<real_t> Value::value<Vector<real_t> >() const;
template<> Vector<complex_t> Value::value<Vector<complex_t> >() const;
template<> Matrix<real_t> Value::value<Matrix<real_t> >() const;
template<> Matrix<complex_t> Value::value<Matrix<complex_t> >() const;

template<typename T> T Value::value() const
{
  error("forbidden","Value::value<T>");
  return T(0);
}

template<typename T> T& Value::value()
{
  T* vp = reinterpret_cast<T*>(value_p);
  return *vp;
}

inline real_t Value::asReal() const
{
  if (value_p==nullptr)
  {
    where("Value::asReal()");
    error("null_pointer","value_p");
  }
  if (type_!=_real || struct_!=_scalar)
  {
    where("Value::asReal()");
    error("value_wrongtype", words("value", type_), words("structure", struct_),
                             words("value", _real), words("structure", _scalar));
  }
  return *reinterpret_cast<real_t*>(value_p);
}

inline complex_t Value::asComplex() const
{
  if (value_p==nullptr)
  {
    where("Value::asComplex()");
    error("null_pointer","value_p");
  }
  if (struct_!=_scalar)
  {
    where("Value::asComplex()");
    error("value_wrongtype", words("value", type_), words("structure", struct_),
                             words("value", type_), words("structure", _scalar));
  }
  if (type_==_real) return complex_t(*reinterpret_cast<real_t*>(value_p));
  return *reinterpret_cast<complex_t*>(value_p);
}

inline Vector<real_t> Value::asRealVector() const
{
  if (value_p==nullptr)
  {
    where("Value::asRealVector()");
    error("null_pointer","value_p");
  }
  if (type_!=_real || struct_!=_vector)
  {
    where("Value::asRealVector()");
    error("value_wrongtype", words("value", type_), words("structure", struct_),
                             words("value", _real), words("structure", _vector));
  }
  return *reinterpret_cast<Vector<real_t>*>(value_p);
}

inline Vector<complex_t> Value::asComplexVector() const
{
  if (value_p==nullptr)
  {
    where("Value::asComplexVector()");
    error("null_pointer","value_p");
  }
  if (struct_!=_vector)
  {
    where("Value::asComplexVector()");
    error("value_wrongtype", words("value", type_), words("structure", struct_),
                             words("value", type_), words("structure", _vector));
  }
  if (type_==_real) return Vector<complex_t>(*reinterpret_cast<Vector<real_t>*>(value_p));
  return *reinterpret_cast<Vector<complex_t>*>(value_p);
}

inline Matrix<real_t> Value::asRealMatrix() const
{
  if (value_p==nullptr)
  {
    where("Value::asRealMatrix()");
    error("null_pointer","value_p");
  }
  if (type_!=_real || struct_!=_matrix)
  {
    where("Value::asRealMatrix()");
    error("value_wrongtype", words("value", type_), words("structure", struct_),
                             words("value", _real), words("structure", _matrix));
  }
  return *reinterpret_cast<Matrix<real_t>*>(value_p);
}

inline Matrix<complex_t> Value::asComplexMatrix() const
{
  if (value_p==nullptr)
  {
    where("Value::asComplexMatrix()");
    error("null_pointer","value_p");
  }
  if (type_!=_complex || struct_!=_matrix)
  {
    where("Value::asComplexMatrix()");
    error("value_wrongtype", words("value", type_), words("structure", struct_),
                             words("value", _complex), words("structure", _matrix));
  }
  return *reinterpret_cast<Matrix<complex_t>*>(value_p);
}

//extern typeOf
template<typename T>
structPair typeOf(const T& v)
{
  std::map<string_t, structPair>::iterator it = Value::theValueTypeRTInames.find(typeid(v).name());
  if (it == Value::theValueTypeRTInames.end())   // type not found in list of value types
  {
    error("value_badtype", typeid(v).name());
  }
  return it->second;
}

//tricks to get valuetype of template argument
inline ValueType valueType(const real_t& x){return _real;}
inline ValueType valueType(const complex_t& x){return _complex;}
inline ValueType valueType(const std::vector<real_t>& x){return _real;}
inline ValueType valueType(const std::vector<complex_t>& x){return _complex;}
inline ValueType valueType(const std::vector<Vector<real_t> >& x){return _real;}
inline ValueType valueType(const std::vector<Vector<complex_t> >& x){return _complex;}

//tricks to get structype of template argument
inline StrucType strucType(const real_t& x){return _scalar;}
inline StrucType strucType(const complex_t& x){return _scalar;}
template<typename T> inline StrucType strucType(const std::vector<T>& x){return _vector;}

inline number_t sizeOf(const real_t& x){return 1;}
inline number_t sizeOf(const complex_t& x){return 1;}
inline number_t sizeOf(const std::vector<real_t>& x){return x.size();}
inline number_t sizeOf(const std::vector<complex_t>& x){return x.size();}
inline number_t sizeOf(const std::vector<Vector<real_t> >& x){return x.size()*x[0].size();}
inline number_t sizeOf(const std::vector<Vector<complex_t> >& x){return x.size()*x[0].size();}

} // end of namespace xlifepp

#endif /* VALUE_HPP */

