/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Trace.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 10 dec 2002
  \date 3 may 2012

  \brief Definition of the xlifepp::Trace class
 */

#ifndef TRACE_HPP
#define TRACE_HPP

#include "config.h"
#include "String.hpp"
#include "Collection.hpp"

#include <fstream>
#include <iostream>

namespace xlifepp
{

/*!
   \class Trace
   Trace object allows to keep trace of runtime, as a string list, of the
  hierarchy of functions used up to now.
 */
class Trace
{
  private:
    number_t pos_;                 //!< next position in trace object list member
    static const number_t maxPos_; //!< last position in trace object list member
    Strings fList;                 //!< stack of called functions

    static bool isLogged_;              //!< true if log activated

  public:
    static bool trackingMode;           //!< activate printing in push, pop functions
    static bool traceMemory;            //!< activate printing of large memory allocation
    static bool disablePushPop;            //!< push pop engine off

    //-------------------------------------------------------------------------------
    //  Constructor, Destructor
    //-------------------------------------------------------------------------------
    //! constructor
    Trace()
      : pos_(0), fList(maxPos_) {}
    //! destructor
    ~Trace() {}

    //-------------------------------------------------------------------------------
    // Public access member functions
    //-------------------------------------------------------------------------------
    static bool isLogged()        //! returns true if log activated
      { return isLogged_; }
    static void logOn()           //! activates log
      { isLogged_ = true; }
    static void logOff()          //! deactivates log
      { isLogged_ = false; }

    number_t length() const; //!< function length returns the number of items of f_list

    //-------------------------------------------------------------------------------
    // Public Inline member functions
    //-------------------------------------------------------------------------------

    void push(const string_t&);       //!< "pushes" name into Trace function list member
    void pop();                     //!< "pops" current input out of a Trace function list member
    string_t current(const number_t);   //!< returns string of last l to current inputs of a Trace object list
    string_t current();               //!< returns last input string of a Trace object list
    string_t list();                  //!< returns function list of a Trace object list

    void print(std::ofstream&);     //!< prints function list to opened ofstream or to default print file
    void print();

    void indent();                  //!< function indent prints context-depending indentation to log file

    //----------------------------------------------------------------------------------
    // function log prints message to log file with indentation
    //----------------------------------------------------------------------------------

    //! function not yet implemented
    void noSuchFunction(const string_t&);

    friend Trace& operator<<(Trace&, const string_t& s);

    //-------------------------------------------------------------------------------
    // Private Member functions
    //-------------------------------------------------------------------------------
  private:
    Trace(const Trace&); //!< no copy operator
    void operator=(const Trace&); //!< no assignment operator

}; // end of Trace class

//----------------------------------------------------------------------------------
// External class related functions
//----------------------------------------------------------------------------------
Trace& operator<<(Trace&, const string_t& s); //!< print operator

/*!
  set a maximum value for all verbose levels
  setGlobalVerboseLevel(0) sets all levels to zero
 */
void setGlobalVerboseLevel(const number_t);
number_t verboseLevel(const number_t); //!< sets a maximized value for argument value
string_t& where(const string_t& s);                //!< sets a message to tell where we are in the code (shortcut to when push/pop are not set)
void incompleteFunction(const string_t& s = ""); //!< message sent when function not fully defined
void invalidFunction(const string_t& s = "");    //!< message sent when function not valid
void constructorError();                       //!< message sent when in trouble in object constructor
void setNewHandler();                          //!< defines a new trace handler

} // end of namespace xlifepp

#endif /* TRACE_HPP */
