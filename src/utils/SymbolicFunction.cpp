/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SymbolicFunction.cpp
  \author E. Lunéville
  \since 11 feb 2017
  \date 11 feb 2017

  \brief Implementation of stuff related to symbolic functions
*/

#include "SymbolicFunction.hpp"

namespace xlifepp
{
std::set<VariableName> SymbolicFunction::listOfVar() const
{
  std::set<VariableName> vars;
  if(var != _varUndef) vars.insert(var);
  if(fn1 != nullptr)
    {
      std::set<VariableName> var1 = fn1->listOfVar();
      if(var1.size() > 0) vars.insert(var1.begin(), var1.end());
      }
  if(fn2 != nullptr)
    {
      std::set<VariableName> var2 = fn2->listOfVar();
      if(var2.size() > 0) vars.insert(var2.begin(), var2.end());
      }
  return vars;
}

// value type (real or complex) of function. ValueType is complex if one of coef's has a non null imaginary part
ValueType SymbolicFunction::valueType() const
{
    if(coef.imag()!=0 || par.imag()!=0) return _complex;
    if(fn1!=nullptr && fn1->valueType()==_complex) return _complex;
    if(fn2!=nullptr && fn2->valueType()==_complex) return _complex;
    return _real;
}

bool removeEndParenthesis(string_t& s)
{
    if(s[0]=='(')
    {
      number_t n=s.size(), p=1, i=1;
      while(i<n && p!=0)
      {
        if(s[i]=='(') p++;
        if(s[i]==')') p--;
        i++;
      }
      if(p==0 && i==n) {s=s.substr(1,n-2); return true;}
    }
    return false;
}

void removePlusMinus(string_t& s)
{
    string_t s2;
    number_t n=s.size();
    SymbolicOperation op=_idop;
    for(number_t i=0; i<n;i++)
    {
        char c=s[i];
        if(c=='-')
          {if(op==_minus) op=_plus; else op=_minus;}
        else if(c=='+')
          {if(op!=_minus) op=_plus;}
        else
        {
            if(op==_plus) s2+="+"+string_t(1,c);
            else if(op==_minus) s2+="-"+string_t(1,c);
            else s2+=string_t(1,c);
            op=_idop;
        }
    }
    s=s2;
}

// symbolic function as string (n1,n2,n3) variable names, if not given names are x1,x2,x3
//  lev: node level (default 0, root node)
//  parent: true if inside parenthesis
string_t SymbolicFunction::asString(number_t lev, bool parent, const string_t& n1, const string_t& n2,const string_t& n3) const
{
    string_t s;
    if(var == _varUndef && fn1 == nullptr && fn2 == nullptr) //case of a constant
    {
      if(coef.imag() == 0) s+=tostring(coef.real());
      else s+=tostring(coef);
      return s;
    }

  if(coef != complex_t(1., 0.) && coef != complex_t(-1., 0.))
    {
      if(coef.imag() == 0) s+=tostring(coef.real());
      else s+=tostring(coef);
      s+="*";
    }
  if(coef == complex_t(-1., 0.)) s+="-";
  if(op > _idop && op < _abs) //binary operator, fn1 or fn2 must be not 0
    {
      if(fn1->isConst() && std::abs(fn1->coef.real()) == 1. && op == _multiply) //special case 1* or -1*
        {
          if (fn1->coef.real() == -1.) s+="-";
          s+=fn2->asString(lev+1,false,n1,n2,n3);
        }
    else
        {
          if(op==_atan2 || op==_power)
          {
             s+=opName(op);
             s+="(";
             s+=fn1->asString(lev+1,true,n1,n2,n3);
             s+=",";
             s+=fn2->asString(lev+1,true,n1,n2,n3);
             s+=")";
          }
          else
          {
           if(!parent) s+="(";
           s+=fn1->asString(lev+1,op==_plus || op==_minus,n1,n2,n3);
           s+=opName(op);
           s+=fn2->asString(lev+1,op==_plus || op==_minus,n1,n2,n3);
           if(!parent) s+=")";
         }
       }
    }
  else  //unary operator, var or fn1 have to be set
    {
      if(op != _idop) s+=opName(op)+"(";
      if(var != _varUndef)
      {
          switch(var)
          {
              case _x1: if(n1=="") s+=varName(var); else s+=n1; break;
              case _x2: if(n2=="") s+=varName(var); else s+=n2; break;
              case _x3: if(n3=="") s+=varName(var); else s+=n3; break;
              default: s+=varName(var);
          }
      }
      else if (fn1 != nullptr) s+=fn1->asString(lev+1,(op!=_idop || parent),n1,n2,n3);
      if(op == _pow) s+=", " +tostring(par.real());
      if(op != _idop) s+=")";
    }
    if(lev==0)  //remove first and last parenthesis, +-, -+
    {
      bool rem=true;
      while(rem && s.size()>0 && s[0]=='(') rem=removeEndParenthesis(s);
      removePlusMinus(s);
    }
   return s;
}

void SymbolicFunction::print(std::ostream& out) const
{
  out<<asString();
}

void SymbolicFunction::printTree(std::ostream& out, int lev) const
{
  out << std::endl;
  for(int i = 0; i < lev; ++i) out << " ";
  out << lev << " -> ";
  if(var == _varUndef && fn1 == nullptr && fn2 == nullptr) //case of a constant
    {
      if(coef.imag() == 0) out << coef.real();
      else out << coef;
      return;
    }
  if(coef != complex_t(1., 0.) && coef != complex_t(-1., 0.))
    {
      if(coef.imag() == 0) out << coef.real();
      else out << coef;
      out << " * ";
    }
  if(coef == complex_t(-1., 0.)) out << "-";

  if(op > _idop && op < _abs) //binary operator, fn1 or fn2 must be not 0
    {
      fn1->printTree(out, lev + 1);
      out << std::endl;
      for(int i = 0; i < lev; ++i) out << " ";
      out << lev << " -> " << opName(op) << " ";
      fn2->printTree(out, lev + 1);
    }
  else  //unary operator, var or fn1 have to be set
    {
      if(op != _idop) out << opName(op) << "(";
      if(var != _varUndef) out << varName(var);
      else fn1->printTree(out, lev + 1);
      if(op != _idop) out << ")";
    }
}

string_t varName(VariableName v)
{
  switch(v)
    {
    case _x1: return "x1";
    case _x2: return "x2";
    case _x3: return "x3";
    case _r : return "r";
    case _theta : return "theta";
    case _phi : return "phi";
    default: break;
    }
  return "?";
}

string_t opName(SymbolicOperation o)
{
  switch(o)
    {
    case _idop: return "";
    case _plus: return "+";
    case _minus: return "-";
    case _multiply: return "*";
    case _divide: return "/";
    case _power: return "pow";
    case _atan2: return "atan2";
    case _abs: return "abs";
    case _sign: return "sign";
    case _realPart: return "real";
    case _imagPart: return "imag";
    case _sqrt: return "sqrt";
    case _squared: return "squared";
    case _sin: return "sin";
    case _cos: return "cos";
    case _tan: return "tan";
    case _asin: return "asin";
    case _acos: return "acos";
    case _atan: return "atan";
    case _sinh: return "sinh";
    case _cosh: return "cosh";
    case _tanh: return "tanh";
    case _asinh: return "asinh";
    case _acosh: return "acosh";
    case _atanh: return "atanh";
    case _exp: return "exp";
    case _log: return "log";
    case _log10: return "log10";
    case _pow: return "pow";
    case _equal: return "==";
    case _different: return "!=";
    case _less: return "<";
    case _lessequal: return "<=";
    case  _greater: return ">";
    case _greaterequal: return ">=";
    case _and: return "&&";
    case _or: return "||" ;
    case _not: return "!";
    default: break;
    }
  return "";
}

//reduce node made of constants
void SymbolicFunction::reduceConst()
{
  complex_t zero(0.,0.);
  if(coef==zero)  //null node
  {
      if(fn1!=nullptr) delete fn1;
      if(fn2!=nullptr) delete fn2;
      fn1=nullptr;fn2=nullptr;var=_varUndef; op=_idop;
      return;
  }
  if(fn1==nullptr) //no reduction const or var
  {
      if(var==_varUndef && op>=_abs)   // compute op(cte)
      {
           coef=evalFun(op, coef, par);
           op=_idop;
      }
      return;
  }
  const SymbolicFunction* t;
  if(fn1!=nullptr && !fn1->isConst()) const_cast<SymbolicFunction*>(fn1)->reduceConst();
  if(fn2!=nullptr && !fn2->isConst()) const_cast<SymbolicFunction*>(fn2)->reduceConst();
  if(fn1!=nullptr && fn1->isConst() && fn2==nullptr)  // case of op(cte)
  {
      coef=evalFun(op, fn1->coef, par);
      delete fn1;
      fn1=nullptr;
      op=_idop;
      return;
  }

  if(op==_pow) //reduce power 1 or 0
  {
     if(par==1.) {op=_idop; par=0.;}
     else if(par==0.) {op=_idop; delete fn1; fn1=new SymbolicFunction(1.);}
     //thePrintStream<<"reducing pow "<<ss.str()<<"--> "<<(*this)<<eol<<std::flush;
     return;
  }
  if(op==_power && fn2->isConst()) //reduce power 1
  {
     if(fn2->coef==1.) {op=_idop; delete fn2; fn2=nullptr;}
     else if(fn2->coef==0.) {op=_idop; delete fn1; delete fn2; fn2=nullptr;fn1=new SymbolicFunction(1.);}
     //thePrintStream<<"reducing power "<<ss.str()<<"--> "<<(*this)<<eol<<std::flush;
     return;
  }
  if(fn1!=nullptr && fn1->isConst() && fn2!=nullptr && fn2->isConst()) // compress c fun(c1 op c2) ->  constant (c*(c1 op c2))
  {
      complex_t c1=fn1->coef, c2=fn2->coef;
      if(std::imag(c1)==0. && std::imag(c2)==0.) coef*=evalOp(op,std::real(c1),std::real(c2));
      else coef*=evalOp(op,c1,c2);
      delete fn1; delete fn2;
      fn1=nullptr;fn2=nullptr;var=_varUndef; op=_idop;
      //thePrintStream<<"reducing c fun(c1 op c2) "<<ss.str()<<"--> "<<(*this)<<eol<<std::flush;
      return;
  }
  if(fn1!=nullptr && fn1->isConst() && fn1->coef==zero && fn2!=nullptr) // compress 0 +- f2 -> +-f2 and 0 */f2 -> 0
  {
     switch(op)
     {
       case _plus:
       case _minus:
          {
            delete fn1; fn1=nullptr;
            coef*=fn2->coef;
            op=fn2->op;
            var=fn2->var;
            par=fn2->par;
            SymbolicFunction* sf =const_cast<SymbolicFunction*>(fn2);
            if(sf->fn1!=nullptr)
             {
                fn1=sf->fn1;
                if(sf->fn2!=nullptr) fn2=sf->fn2; else fn2=nullptr;
             }
            sf->fn1=nullptr;sf->fn2=nullptr;
            delete sf;
            if(op==_minus) coef*=-1;
          }
          break;
       case _multiply:
       case _divide:
            delete fn1; delete fn2; fn1=nullptr;fn2=nullptr;var=_varUndef; op=_idop; coef=zero;break;
        default: break;
       }
       return;
  }
  if(fn2!=nullptr && fn2->isConst() && fn2->coef==zero && fn1!=nullptr) // compress f1 +- 0 -> f1 and f1 */ 0 -> 0
  {
     switch(op)
     {
       case _plus:
       case _minus:
          {
            delete fn2; fn2=nullptr;
            coef*=fn1->coef;
            op=fn1->op;
            var=fn1->var;
            par=fn1->par;
            SymbolicFunction* sf =const_cast<SymbolicFunction*>(fn1);
            if(sf->fn1!=nullptr)
             {
                fn1=sf->fn1;
                if(sf->fn2!=nullptr) fn2=sf->fn2;
             }
            sf->fn1=nullptr;sf->fn2=nullptr;
            delete sf;
          }
          break;
       case _multiply:
       case _divide:
            delete fn1; delete fn2; fn1=nullptr;fn2=nullptr;var=_varUndef; op=_idop; coef=zero;break;
        default: break;
       }
       return;
  }
  if((op==_multiply || op==_divide) && fn1!=nullptr && !fn1->isConst() && fn2!=nullptr && fn2->isConst()) // compress c (f1 */ c2) -> (c*c2) f1
  {
      coef*=fn1->coef; par=fn1->par; op=fn1->op; var=fn1->var;
      if(op==_multiply) coef*=fn2->coef; else coef/=fn2->coef;
      t=fn2;
      if(fn1->fn2!=nullptr) fn2=new SymbolicFunction(*fn1->fn2); else fn2=nullptr;
      delete t;
      t=fn1;
      if(fn1->fn1!=nullptr) fn1=new SymbolicFunction(*fn1->fn1); else fn1=nullptr;
      delete t;
      if(fn1!=nullptr && fn2!=nullptr && (op==_multiply || op==_divide)) {const_cast<SymbolicFunction*>(fn1)->coef*=coef; coef=1.;}
      //thePrintStream<<"reducing c (f1 */ c2) "<<ss.str()<<"--> "<<(*this)<<eol<<std::flush;
      return;
  }
  if(op==_multiply && fn1!=nullptr && fn1->isConst() && fn2!=nullptr && !fn2->isConst()) // compress c(c1 * f2) -> (c*c1) f2
  {
      coef*=fn1->coef*fn2->coef;
      par=fn2->par; op=fn2->op; var=fn2->var;
      t=fn1;
      if(fn2->fn1!=nullptr) fn1=new SymbolicFunction(*fn2->fn1); else fn1=nullptr;
      delete t;
      t=fn2;
      if(fn2->fn2!=nullptr) fn2=new SymbolicFunction(*fn2->fn2); else fn2=nullptr;
      delete t;
      if(fn1!=nullptr && fn2!=nullptr && (op==_multiply || op==_divide)) {const_cast<SymbolicFunction*>(fn1)->coef*=coef; coef=1.;}
      //thePrintStream<<"reducing c(c1 * f2) "<<ss.str()<<"--> "<<(*this)<<eol<<std::flush;
      return;
  }
  if(op==_divide && fn1!=nullptr && fn1->isConst() && fn1->coef!=1. && fn2!=nullptr && !fn2->isConst()) // c(c1 / f2) -> (c*c1)/f2
  {
      const_cast<SymbolicFunction*>(fn1)->coef*=coef;
      coef=1.;
      //thePrintStream<<"reducing c(c1 / f2) "<<ss.str()<<"--> "<<(*this)<<eol<<std::flush;
      return;
  }
  if((op==_multiply || op==_divide) && fn1!=nullptr && !fn1->isConst() && fn2!=nullptr && !fn2->isConst()) // compress c (f1 */ f2) -> (c*c1*/c2) f1*/f2
  {
      if(op==_multiply) coef*=fn1->coef*fn2->coef;
      if(op==_divide)  coef*=fn1->coef/fn2->coef;
      const_cast<SymbolicFunction*>(fn1)->coef=1.;
      const_cast<SymbolicFunction*>(fn2)->coef=1.;
      return;
  }

  if((op==_plus || op==_minus) && fn1!=nullptr && fn1->isVar() && fn2!=nullptr && fn2->isVar() && fn1->var==fn2->var && fn1->op==fn2->op && fn1->par==fn2->par) // compress a*x +- b*x
  {
      if(op==_plus) coef*=(fn1->coef)+(fn2->coef); else coef*=(fn1->coef)-(fn2->coef);
      op=fn1->op; var=fn1->var; par=fn1->par;
      delete fn1; delete fn2;
      fn1=nullptr; fn2=nullptr;
      //thePrintStream<<"reducing a*x +- b*x "<<ss.str()<<"--> "<<(*this)<<eol<<std::flush;
      return;
  }
}

bool isequal(const SymbolicFunction& f1, const SymbolicFunction& f2)
{
    bool eq;
    if(f1.coef!=f2.coef) return false;
    if(f1.op!=f2.op) return false;
    if(f1.fn1!=nullptr)
    {
        if(f2.fn1==nullptr) return false;
        if(!isequal(*f1.fn1,*f2.fn1)) return false;
    }
    else //f1.fn1=0
    {
        if(f2.fn1!=nullptr) return false;
        if(f1.var!=f2.var) return false;
    }
    if(f1.fn2!=nullptr)
    {
        if(f2.fn2==nullptr) return false;
        if(!isequal(*f1.fn2,*f2.fn2)) return false;
    }
    return true;
}

//special reduction of function f1 op f2
void SymbolicFunction::reduceFun()
{
    if(fn1!=nullptr && !fn1->isConst()) const_cast<SymbolicFunction*>(fn1)->reduceFun();
    if(fn2!=nullptr && !fn2->isConst()) const_cast<SymbolicFunction*>(fn2)->reduceFun();
    if(fn1==nullptr || fn2==nullptr) return;
    // detect g = e*(a*sin(f)*b*sin(f) + c*cos(f)*d*cos(f)) with a*b=c*d -> g = e*a*b
    if(op==_plus && fn1->op==_multiply && fn2->op==_multiply && fn1->coef==fn2->coef && fn1->fn1!=nullptr && fn1->fn2!=nullptr && fn2->fn1!=nullptr && fn2->fn2!=nullptr)
    {
        if(isequal(*fn1->fn1,*fn1->fn2) && isequal(*fn2->fn1,*fn2->fn2) && ((fn1->fn1->op==_sin && fn2->fn1->op==_cos)||(fn1->fn1->op==_cos && fn2->fn1->op==_sin))
           && fn1->fn1->coef*fn1->fn2->coef==fn2->fn1->coef*fn2->fn2->coef)
           if((fn1->fn1->fn1!=nullptr && fn2->fn1->fn1!=nullptr && isequal(*fn1->fn1->fn1,*fn2->fn1->fn1)) || (fn1->fn1->fn1==nullptr && fn1->fn1->var == fn2->fn1->var))
           {
               coef*=fn1->coef*fn1->fn1->coef*fn1->fn2->coef;
               var=_varUndef;
               delete fn1; delete fn2;
               fn1=nullptr; fn2=nullptr;
               op=_idop;
               return;
           }
    }
}

//reduction of symbolic function
void SymbolicFunction::reduce()
{
    reduceConst();
    reduceFun();
}



// create the derivative of symbolic function
SymbolicFunction& derivative(const SymbolicFunction& f, VariableName v)
{
   if(f.isConst()) return *new SymbolicFunction(0.);// null function
   SymbolicFunction* df=nullptr;
   complex_t c = f.coef;
   if (f.op>_idop && f.op<_abs) //binary operator, fn1 and fn2 are not 0
   {
       SymbolicFunction* df1=nullptr, *df2=nullptr;
       const SymbolicFunction f1 = *f.fn1, f2=*f.fn2;
       if(f1.listOfVar().count(v)!=0)
       {
            df1= &derivative(f1,v);
            df1->reduceConst();
       }
       if(f2.listOfVar().count(v)!=0)
       {
            df2= &derivative(f2,v);
            df2->reduceConst();
       }
       if(df1==nullptr && df2==nullptr) return *new SymbolicFunction(0.);// null function
       switch (f.op)
       {
           case _plus:
               {
                if(df1==nullptr) df = new SymbolicFunction(c*(*df2));      // df1==nullptr and df2!=nullptr
                else if(df2==nullptr) df = new SymbolicFunction(c*(*df1)); // df2==nullptr and df1!=nullptr
                else df = new SymbolicFunction(*df1,*df2,_plus,c);   // df2!=nullptr and df1!=nullptr
               }
             break;
           case _minus:
               {
                if(df1==nullptr) df = new SymbolicFunction(-c*(*df2));     // df1==nullptr and df2!=nullptr
                else if(df2==nullptr) df = new SymbolicFunction(c*(*df1)); // df2==nullptr and df1!=nullptr
                else df = new SymbolicFunction(*df1,*df2,_minus,c);  // df2!=nullptr and df1!=nullptr
               }
             break;
           case _multiply:
               {
                if(df1==nullptr) df = new SymbolicFunction(c*f1*(*df2));        // df1==nullptr and df2!=nullptr
                else if(df2==nullptr) df = new SymbolicFunction(c*(*df1)*f2);   // df2==nullptr and df1!=nullptr
                else df = new SymbolicFunction(c*(f1*(*df2)+(*df1)*f2));  // df2!=nullptr and df1!=nullptr
               }
             break;
           case _divide:
               {
                if(df1==nullptr) df = new SymbolicFunction((-c)*f1*(*df2)/(f2^2));    // df1==nullptr and df2!=nullptr
                else if(df2==nullptr) df = new SymbolicFunction(c*(*df1)/f2);         // df2==nullptr and df1!=nullptr
                else df = new SymbolicFunction(c*((*df1)*f2-(f1*(*df2)/(f2^2))));// df2!=nullptr and df1!=nullptr
               }
             break;
           case _power:
               {
                if(df1==nullptr) df=new SymbolicFunction((*df2)*log(f1)*f);           // df1==nullptr and df2!=nullptr
                else if(df2==nullptr)
                  {
                     if(f2.isConst())
                     {
                        if(f2.coef==0.) df = new SymbolicFunction(0.);
                        else if (f2.coef==1.) df = new SymbolicFunction(c*(*df1));
                        else df = new SymbolicFunction(c*f2*(*df1)*power(f1,std::real(f2.coef)-1));
                     }
                     else df = new SymbolicFunction(c*(f2*(*df1)/f1)*f);  // df2==nullptr and df1!=nullptr
                  }
                else df =new SymbolicFunction(((*df2)*log(f1)+f2*(*df1)/f1)*f); // df2!=nullptr and df1!=nullptr
               }
             break;
           case _atan2:
               {
                if(df1==nullptr) df = new SymbolicFunction((-c)*f1*(*df2)/(f1*f1+f2*f2));    // df1==nullptr and df2!=nullptr
                else if(df2==nullptr) df = new SymbolicFunction( c*f2*(*df1)/(f1*f1+f2*f2)); // df2==nullptr and df1!=nullptr
                else df = new SymbolicFunction(c*(f2*(*df1)-f1*(*df2))/(f1*f1+f2*f2)); // df2!=nullptr and df1!=nullptr
               }
             break;
           case _equal:
           case _different:
           case _greater:
           case _greaterequal:
           case _less:
           case _lessequal:
           case _and:
           case _or:
           default: error("free_error","in symbolic operation, derivative of operator "+opName(f.op)+" is not defined");
      }
      df->reduceConst();
      //thePrintStream<<"deriving/"<<varName(v)<<" "<<f<<" --> "<<(*df)<<eol<<eol<<std::flush;
      return *df;
   }
  if (f.var==v) // c*op(v)
  {
      if(c==complex_t(0.)) return *new SymbolicFunction(0.);// null function
      SymbolicFunction sv(v);
      switch (f.op)
       {
         case _idop:    df = new SymbolicFunction(c);break;
         case _sqrt:    df = new SymbolicFunction((0.5*c)/sqrt(sv));break;
         case _squared: df = new SymbolicFunction((2.*c)*sv);break;
         case _sin:     df = new SymbolicFunction(c*cos(sv));break;
         case _cos:     df = new SymbolicFunction((-c)*sin(sv));break;
         case _tan:     df = new SymbolicFunction(c/(cos(sv)*cos(sv)));break;
         case _sinh:     df = new SymbolicFunction(c*cosh(sv));break;
         case _cosh:     df = new SymbolicFunction(c*sinh(sv));break;
         case _tanh:     df = new SymbolicFunction(c/(cosh(sv)*cosh(sv)));break;
         #if __cplusplus > 199711L
         case _asin:     df = new SymbolicFunction(c/sqrt(1.-sv*sv));break;
         case _acos:     df = new SymbolicFunction((-c)/sqrt(1.-sv*sv));break;
         case _atan:     df = new SymbolicFunction(c/(1.+sv*sv));break;
         case _asinh:    df = new SymbolicFunction(c/sqrt(sv*sv+1.));break;
         case _acosh:    df = new SymbolicFunction(c/sqrt(sv*sv-1.));break;
         case _atanh:    df = new SymbolicFunction(c/(1.-sv*sv));break;
         #endif
         case _exp:      df = new SymbolicFunction(c*exp(sv));break;
         case _log:      df = new SymbolicFunction(c/sv);break;
         case _log10:    df = new SymbolicFunction((c/std::log(10))/sv);break;
         case _pow:      df = new SymbolicFunction((c*f.par)*pow(sv,std::real(f.par)-1.));break;
         default: error("free_error","in symbolic operation, derivative of operator "+opName(f.op)+" is not defined");
     }
     df->reduceConst();
     //thePrintStream<<"deriving/"<<varName(v)<<" "<<f<<" --> "<<(*df)<<eol<<eol<<std::flush;
     return *df;
  }

  if(f.fn1!=nullptr) //unary operator
  {
      const SymbolicFunction f1 = *f.fn1;
      SymbolicFunction* df1=nullptr;
      if(f1.listOfVar().count(v)!=0) {df1= &derivative(f1,v);df1->reduceConst();}
      if(df1==nullptr) return *new SymbolicFunction(0.);// null function
      switch (f.op)
       {
         case _idop:    df = new SymbolicFunction(c*(*df1)); break;
         case _sqrt:    df = new SymbolicFunction((0.5*c)*(*df1)/sqrt(f1));break;
         case _squared: df = new SymbolicFunction((2.*c)*(*df1)*f1);break;
         case _sin:     df = new SymbolicFunction(c*(*df1)*cos(f1));break;
         case _cos:     df = new SymbolicFunction((-c)*(*df1)*sin(f1));break;
         case _tan:     df = new SymbolicFunction(c*(*df1)/(cos(f1)*cos(f1)));break;
         case _sinh:     df = new SymbolicFunction(c*(*df1)*cosh(f1));break;
         case _cosh:     df = new SymbolicFunction(c*(*df1)*sinh(f1));break;
         case _tanh:     df = new SymbolicFunction(c*(*df1)/(cosh(f1)*cosh(f1)));break;
         #if __cplusplus > 199711L
         case _asin:     df = new SymbolicFunction(c*(*df1)/sqrt(1.-f1*f1));break;
         case _acos:     df = new SymbolicFunction((-c)*(*df1)/sqrt(1.-f1*f1));break;
         case _atan:     df = new SymbolicFunction(c*(*df1)/(1.+f1*f1));break;
         case _asinh:    df = new SymbolicFunction(c*(*df1)/sqrt(f1*f1+1.));break;
         case _acosh:    df = new SymbolicFunction(c*(*df1)/sqrt(f1*f1-1.));break;
         case _atanh:    df = new SymbolicFunction(c*(*df1)/(1.-f1*f1));break;
         #endif
         case _exp:      df = new SymbolicFunction(c*(*df1)*exp(f1));break;
         case _log:      df = new SymbolicFunction(c*(*df1)/f1);break;
         case _log10:    df = new SymbolicFunction((c/std::log(10))*(*df1)/f1);break;
         case _pow:      df = new SymbolicFunction((c*f.par)*(*df1)*pow(f1,std::real(f.par)-1.));break;
         case _abs:      df = new SymbolicFunction(c*(*df1)*sign(f1)); break;        //only for real function
         default: error("free_error","in symbolic operation, derivative of operator "+opName(f.op)+" is not defined");
     }
     df->reduceConst();
     //thePrintStream<<"deriving/"<<varName(v)<<" "<<f<<" --> "<<(*df)<<eol<<eol<<std::flush;
     return *df;
  }
  return *new SymbolicFunction(0.);// null function
}

SymbolicFunction& derivative(const SymbolicFunction& f, const SymbolicFunction& v)
{
    if(&v==&x_1) return derivative(f,_x1);
    if(&v==&x_2) return derivative(f,_x2);
    if(&v==&x_3) return derivative(f,_x3);
    std::stringstream ss;
    ss<<"cannot derive "<<f<<" with respect to "<<v;
    error("free_error",ss.str());
    return derivative(f,_x1); //fake return
}

} // end of namespace xlifepp
