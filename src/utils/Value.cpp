/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Value.cpp
  \author E. Lunéville
  \since 04 mar 2012
  \date 04 may 2012

  \brief Implementation of xlifepp::Value class members and related functions
*/

#include "Value.hpp"

namespace xlifepp
{

//------------------------------------------------------------------------------------
// Value member functions
//------------------------------------------------------------------------------------

// copy constructor (full copy)
Value::Value(const Value& v)
  : type_(v.type_), struct_(v.struct_), conjugate_(v.conjugate_), transpose_(v.transpose_) {copyValue(v);}

void Value::copyValue(const Value& v)
{
  value_p = nullptr;
  if(v.value_p != nullptr)
    {
      switch(struct_)
        {
          case _scalar:
            switch(type_)
              {
                case _real:    value_p = reinterpret_cast<void*>(new real_t(*reinterpret_cast<real_t*>(v.value_p))); return;
                case _complex: value_p = reinterpret_cast<void*>(new complex_t(*reinterpret_cast<complex_t*>(v.value_p))); return;
                default: return;
              }
            break;
          case _vector:
            switch(type_)
              {
                case _real:    value_p = reinterpret_cast<void*>(new Vector<real_t>(*reinterpret_cast<Vector<real_t>*>(v.value_p))); return;
                case _complex: value_p = reinterpret_cast<void*>(new Vector<complex_t>(*reinterpret_cast<Vector<complex_t>*>(v.value_p))); return;
                default: return;
              }
          case _matrix:
            switch(type_)
              {
                case _real:    value_p = reinterpret_cast<void*>(new Matrix<real_t>(*reinterpret_cast<Matrix<real_t>*>(v.value_p))); return;
                case _complex: value_p = reinterpret_cast<void*>(new Matrix<complex_t>(*reinterpret_cast<Matrix<complex_t>*>(v.value_p))); return;
                default: return;
              }
          case _vectorofvector:
            switch(type_)
              {
                case _real:    value_p = reinterpret_cast<void*>(new Vector<Vector<real_t> >(*reinterpret_cast<Vector<Vector<real_t> >*>(v.value_p))); return;
                case _complex: value_p = reinterpret_cast<void*>(new Vector<Vector<complex_t> >(*reinterpret_cast<Vector<Vector<complex_t> >*>(v.value_p))); return;
                default: return;
              }
          case _vectorofmatrix:
            switch(type_)
              {
                case _real:    value_p = reinterpret_cast<void*>(new Vector<Matrix<real_t> >(*reinterpret_cast<Vector<Matrix<real_t> >*>(v.value_p))); return;
                case _complex: value_p = reinterpret_cast<void*>(new Vector<Matrix<complex_t> >(*reinterpret_cast<Vector<Matrix<complex_t> >*>(v.value_p))); return;
                default: return;
              }
          case _matrixofmatrix:
            switch(type_)
              {
                case _real:    value_p = reinterpret_cast<void*>(new Matrix<Matrix<real_t> >(*reinterpret_cast<Matrix<Matrix<real_t> >*>(v.value_p))); return;
                case _complex: value_p = reinterpret_cast<void*>(new Matrix<Matrix<complex_t> >(*reinterpret_cast<Matrix<Matrix<complex_t> >*>(v.value_p))); return;
                default: return;
              }
          default: return;
        }
    }
}

Value& Value::operator=(const Value& v)
{
  if(this == &v) return *this;
  clearValue();
  type_ = v.type_;
  struct_ = v.struct_;
  conjugate_ = v.conjugate_;
  transpose_ = v.transpose_;
  copyValue(v);
  return *this;
}

// constructors only for supported types
// the input values (given by users) are copied !
Value::Value(const real_t& v)
  : type_(_real), struct_(_scalar), conjugate_(false), transpose_(false)
{
  value_p = reinterpret_cast<void*>(new real_t(v));
}

Value::Value(const Vector<real_t>& v)
  : type_(_real), struct_(_vector), conjugate_(false), transpose_(false)
{
  value_p = reinterpret_cast<void*>(new Vector<real_t>(v));
}

Value::Value(const Matrix<real_t>& v)
  : type_(_real), struct_(_matrix), conjugate_(false), transpose_(false)
{
  value_p = reinterpret_cast<void*>(new Matrix<real_t>(v));
}

Value::Value(const Vector<Vector<real_t> >& v)
  : type_(_real), struct_(_vectorofvector), conjugate_(false), transpose_(false)
{
  value_p = reinterpret_cast<void*>(new Vector<Vector<real_t> >(v));
}

Value::Value(const Vector<Matrix<real_t> >& v)
  : type_(_real), struct_(_vectorofmatrix), conjugate_(false), transpose_(false)
{
  value_p = reinterpret_cast<void*>(new Vector<Matrix<real_t> >(v));
}

Value::Value(const Matrix<Matrix<real_t> >& v)
  : type_(_real), struct_(_matrixofmatrix), conjugate_(false), transpose_(false)
{
  value_p = reinterpret_cast<void*>(new Matrix<Matrix<real_t> >(v));
}

Value::Value(const complex_t& v)
  : type_(_complex), struct_(_scalar), conjugate_(false), transpose_(false)
{
  value_p = reinterpret_cast<void*>(new complex_t(v));
}

Value::Value(const Vector<complex_t>& v)
  : type_(_complex), struct_(_vector), conjugate_(false), transpose_(false)
{
  value_p = reinterpret_cast<void*>(new Vector<complex_t>(v));
}

Value::Value(const Matrix<complex_t>& v)
  : type_(_complex), struct_(_matrix), conjugate_(false), transpose_(false)
{
  value_p = reinterpret_cast<void*>(new Matrix<complex_t>(v));
}

Value::Value(const Vector<Vector<complex_t> >& v)
  : type_(_complex), struct_(_vectorofvector), conjugate_(false), transpose_(false)
{
  value_p = reinterpret_cast<void*>(new Vector<Vector<complex_t> >(v));
}

Value::Value(const Vector<Matrix<complex_t> >& v)
  : type_(_complex), struct_(_vectorofmatrix), conjugate_(false), transpose_(false)
{
  value_p = reinterpret_cast<void*>(new Vector<Matrix<complex_t> >(v));
}

Value::Value(const Matrix<Matrix<complex_t> >& v)
  : type_(_complex), struct_(_matrixofmatrix), conjugate_(false), transpose_(false)
{
  value_p = reinterpret_cast<void*>(new Matrix<Matrix<complex_t> >(v));
}

Value::~Value()
{
  clearValue();
}

void Value::clearValue()
{
  if(value_p != nullptr)
    {
      switch(struct_)
        {
          case _scalar:
            switch(type_)
              {
                case _real:
                  delete reinterpret_cast<real_t*>(value_p);
                  break;
                case _complex:
                  delete reinterpret_cast<complex_t*>(value_p);
                  break;
                default:
                  break;
              }
            break;
          case _vector:
            switch(type_)
              {
                case _real:
                  delete reinterpret_cast<Vector<real_t>* >(value_p);
                  break;
                case _complex:
                  delete reinterpret_cast<Vector<complex_t>* >(value_p);
                  break;
                default:
                  break;
              }
            break;
          case _matrix:
            switch(type_)
              {
                case _real:
                  delete reinterpret_cast<Matrix<real_t>* >(value_p);
                  break;
                case _complex:
                  delete reinterpret_cast<Matrix<complex_t>* >(value_p);
                  break;
                default:
                  break;
              }
            break;
          case _vectorofvector:
            switch(type_)
              {
                case _real:
                  delete reinterpret_cast<Vector<Vector<real_t> >* >(value_p);
                  break;
                case _complex:
                  delete reinterpret_cast<Vector<Vector<complex_t> >* >(value_p);
                  break;
                default:
                  break;
              }
            break;
          case _vectorofmatrix:
            switch(type_)
              {
                case _real:
                  delete reinterpret_cast<Vector<Matrix<real_t> >* >(value_p);
                  break;
                case _complex:
                  delete reinterpret_cast<Vector<Matrix<complex_t> >* >(value_p);
                  break;
                default:
                  break;
              }
            break;
          case _matrixofmatrix:
            switch(type_)
              {
                case _real:
                  delete reinterpret_cast<Matrix<Matrix<real_t> >* >(value_p);
                  break;
                case _complex:
                  delete reinterpret_cast<Matrix<Matrix<complex_t> >* >(value_p);
                  break;
                default:
                  break;
              }
            break;
          default:
            break;
        }
    }
}

//return dimensions of value as a pair of dimensions, (1,1) in scalar case
dimPair Value::dims() const
{
  switch(struct_)
  {
    case _vector:
      switch(type_)
      {
        case _real: return dimPair(reinterpret_cast<Vector<real_t>* >(value_p)->size(),1);
        default: return dimPair(reinterpret_cast<Vector<complex_t>* >(value_p)->size(),1);
      }
      break;
    case _matrix:
      switch(type_)
      {
        case _real: return reinterpret_cast<Matrix<real_t>* >(value_p)->dims();
        default: return reinterpret_cast<Matrix<real_t>* >(value_p)->dims();
      }
      break;
    case _vectorofvector:
      switch(type_)
      {
        case _real: return dimPair(reinterpret_cast<Vector<Vector<real_t> >* >(value_p)->size(),1);
        default: return dimPair(reinterpret_cast<Vector<Vector<complex_t> >* >(value_p)->size(),1);
      }
      break;
    case _vectorofmatrix:
      switch(type_)
      {
        case _real: return dimPair(reinterpret_cast<Vector<Matrix<real_t> >* >(value_p)->size(),1);
        default: return dimPair(reinterpret_cast<Vector<Matrix<complex_t> >* >(value_p)->size(),1);
      }
      break;
    case _matrixofmatrix:
      switch(type_)
      {
        case _real:return reinterpret_cast<Matrix<Matrix<real_t> >* >(value_p)->dims();
        default: return reinterpret_cast<Matrix<Matrix<complex_t> >* >(value_p)->dims();
      }
      break;
    default:
      break;
  }
  return dimPair(1,1);
}

// initialization of RTI names of value types
// real_t, Vector<real_t>, Matrix<real_t>, complex_t, Vector<complex_t>, Matrix<complex_t>
void Value::valueTypeRTINamesInit()
{
  Value v;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(v).name()), structPair(_real, _scalar)));
  real_t r;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(r).name()), structPair(_real, _scalar)));
  Vector<real_t> vr;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(vr).name()), structPair(_real, _vector)));
  Matrix<real_t> mr;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(mr).name()), structPair(_real, _matrix)));
  Vector<Vector<real_t> > vvr;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(vvr).name()), structPair(_real, _vectorofvector)));
  Vector<Matrix<real_t> > vmr;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(vmr).name()), structPair(_real, _vectorofmatrix)));
  Matrix<Matrix<real_t> > mmr;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(mmr).name()), structPair(_real, _matrixofmatrix)));
  complex_t c;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(c).name()), structPair(_complex, _scalar)));
  Vector<complex_t> vc;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(vc).name()), structPair(_complex, _vector)));
  Matrix<complex_t> mc;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(mc).name()), structPair(_complex, _matrix)));
  Vector<Vector<complex_t> > vvc;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(vvc).name()), structPair(_complex, _vectorofvector)));
  Vector<Matrix<complex_t> > vmc;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(vmc).name()), structPair(_complex, _vectorofmatrix)));
  Matrix<Matrix<complex_t> > mmc;
  theValueTypeRTInames.insert(mapPair(string_t(typeid(mmc).name()), structPair(_complex, _matrixofmatrix)));
}

// print utility
void Value::printValueTypeRTINames(std::ostream& os) //! print RTI names
{
  std::map<string_t, structPair>::iterator it;
  os << message("value_RTInames") << "\n";
  for(it = theValueTypeRTInames.begin(); it != theValueTypeRTInames.end(); it++)
    {
      os << it->first << " -> (" << words("value", (it->second).first) << "," << words("structure", (it->second).second) << ")\n";
    }

}

void Value::print(std::ostream& os) const
{
  if(theVerboseLevel > 0)
    { os << "Value of type (" << words("value", type_) << "," << words("structure", struct_) << ")"; }
  if(theVerboseLevel == 0)
    {
      os << std::endl;
      return;
    }
  os << " =";

  switch(struct_)
    {
      case _scalar:
        {
          if(type_ == _real) { os << value<real_t>(); }
          else             { os << value<complex_t>(); }
          break;
        }
      case _vector:
        {
          if(type_ == _real) { os << value<Vector<real_t> >(); }
          else             { os << value<Vector<complex_t> >(); }
          break;
        }
      case _matrix:
        {
          if(type_ == _real) { os << value<Matrix<real_t> >(); }
          else             { os << value<Matrix<complex_t> >(); }
          break;
        }
      case _vectorofvector:
        {
          if(type_ == _real) { os << value<Vector<Vector<real_t> > >(); }
          else             { os << value<Vector<Vector<complex_t> > >(); }
          break;
        }
      case _vectorofmatrix:
        {
          if(type_ == _real) { os << value<Vector<Matrix<real_t> > >(); }
          else             { os << value<Vector<Matrix<complex_t> > >(); }
          break;
        }
      case _matrixofmatrix:
        {
          if(type_ == _real) { os << value<Matrix<Matrix<real_t> > >(); }
          else             { os << value<Matrix<Matrix<complex_t> > >(); }
          break;
        }
      default: os << "undefined value";
    }
  os << "\n";
}

std::ostream& operator<<(std::ostream& os, const Value& val)
{
  val.print(os);
  return os;
}

void Value::opposite()
{
  if(value_p != nullptr) return;   //nothing done
  switch(struct_)
    {
      case _scalar:
        {
          if(type_ == _real)
            {real_t& val = value<real_t>(); val = -val; return;}
          complex_t& val = value<complex_t>(); val = -val; return;
        }
      case _vector:
        {
          if(type_ == _real)
            {Vector<real_t>& val = value<Vector<real_t> >(); val = -val; return;}
          Vector<complex_t>& val = value<Vector<complex_t> >(); val = -val; return;
        }
      case _matrix:
        {
          if(type_ == _real)
            {Matrix<real_t>& val = value<Matrix<real_t> >(); val = -val; return;}
          Matrix<complex_t>& val = value<Matrix<complex_t> >(); val = -val; return;
        }
      case _vectorofvector:
        {
          if(type_ == _real)
            {Vector<Vector<real_t> >& val = value<Vector<Vector<real_t> > >(); val = -val; return;}
          Vector<Vector<complex_t> >& val = value<Vector<Vector<complex_t> > >(); val = -val; return;
        }
      case _vectorofmatrix:
        {
          if(type_ == _real)
            {Vector<Matrix<real_t> >& val = value<Vector<Matrix<real_t> > >(); val = -val; return;}
          Vector<Matrix<complex_t> >& val = value<Vector<Matrix<complex_t> > >(); val = -val; return;
        }
      case _matrixofmatrix:
        {
          if(type_ == _real)
            {Matrix<Matrix<real_t> >& val = value<Matrix<Matrix<real_t> > >(); val = -val; return;}
          Matrix<Matrix<complex_t> >& val = value<Matrix<Matrix<complex_t> > >(); val = -val; return;
        }
      default: break;  //nothing done
    }
}

//! set to true or false the temporary conjugate flag
Value& conj(Value& v)
{
  v.conjugate_ = !v.conjugate_;
  return v;
}

//! set to true or false the temporary transpose flag
Value& trans(Value& v)
{
  v.transpose_ = !v.transpose_;
  return v;
}

//! set to true or false the temporary conjugate/transpose flag
Value& adj(Value& v)
{
  v.transpose_ = !v.transpose_;
  v.conjugate_ = !v.conjugate_;
  return v;
}

Value operator-(const Value& v)   //opposite of a value
{
  Value w(v);
  w.opposite();
  return w;
}

//compare values
bool operator==(const Value& v1, const Value& v2)
{
  if(v1.valueType() != v2.valueType()) return false;
  if(v1.strucType() != v2.strucType()) return false;
  switch(v1.strucType())
    {
      case _scalar:
        {
          if(v1.valueType() == _real) return v1.value<real_t>() == v2.value<real_t>();
          return v1.value<complex_t>() == v2.value<complex_t>();
        }
      case _vector:
        {
          if(v1.valueType() == _real) return v1.value<Vector<real_t> >() == v2.value<Vector<real_t> >();
          return v1.value<Vector<complex_t> >() == v2.value<Vector<complex_t> >();
        }
      case _matrix:
        {
          if(v1.valueType() == _real) return v1.value<Matrix<real_t> >() == v2.value<Matrix<real_t> >();
          return v1.value<Matrix<complex_t> >() == v2.value<Matrix<complex_t> >();
        }
      case _vectorofvector:
        {
          if(v1.valueType() == _real) return v1.value<Vector<Vector<real_t> > >() == v2.value<Vector<Vector<real_t> > >();
          return v1.value<Vector<Vector<complex_t> > >() == v2.value<Vector<Vector<complex_t> > >();
        }
      case _vectorofmatrix:
        {
          if(v1.valueType() == _real) return v1.value<Vector<Matrix<real_t> > >() == v2.value<Vector<Matrix<real_t> > >();
          return v1.value<Vector<Matrix<complex_t> > >() == v2.value<Vector<Matrix<complex_t> > >();
        }
      case _matrixofmatrix:
        {
          if(v1.valueType() == _real) return v1.value<Matrix<Matrix<real_t> > >() == v2.value<Matrix<Matrix<real_t> > >();
          return v1.value<Matrix<Matrix<complex_t> > >() == v2.value<Matrix<Matrix<complex_t> > >();
        }
      default: break;  //nothing done
    }
  return true;
}

bool operator!=(const Value& v1, const Value& v2)
{
  return !(v1 == v2);
}

//specialisation of value evaluation
template<> real_t Value::value<real_t>() const
{
  if(struct_==_scalar && type_ ==_real) return *(reinterpret_cast<real_t*>(value_p));
  where("Value::value<Real>");
  error("value_wrongtype",words("structure",struct_),words("value",type_),words("structure",_scalar),words("value",_real));
  return 0.;
}

template<> complex_t Value::value<complex_t>() const
{
  if(struct_==_scalar)
  {
    if(type_==_real) return complex_t(*(reinterpret_cast<real_t*>(value_p)));
    else return *(reinterpret_cast<complex_t*>(value_p));
  }
  where("Value::value<Complex>");
  error("value_wrongtype",words("structure",struct_),words("value",type_),words("structure",_scalar),words("value",_complex));
  return complex_t(0);
}

template<> Vector<real_t> Value::value<Vector<real_t> >() const
{
  if(struct_==_vector && type_ ==_real) return *(reinterpret_cast<Vector<real_t>* >(value_p));
  where("Value::value<Vector<Real> >");
  error("value_wrongtype",words("structure",struct_),words("value",type_),words("structure",_vector),words("value",_real));
  return Vector<real_t>();
}

template<> Vector<complex_t> Value::value<Vector<complex_t> >() const
{
  if(struct_==_vector)
  {
    if(type_==_real) return cmplx(*(reinterpret_cast<Vector<real_t>* >(value_p)));
    else return *(reinterpret_cast<Vector<complex_t> *>(value_p));
  }
  where("Value::value<Vector<Complex> >");
  error("value_wrongtype",words("structure",struct_),words("value",type_),words("structure",_vector),words("value",_complex));
  return Vector<complex_t>();
}

template<> Matrix<real_t> Value::value<Matrix<real_t> >() const
{
  if(struct_==_matrix && type_ ==_real) return *(reinterpret_cast<Matrix<real_t>* >(value_p));
  where("Value::value<Matrix<Real> >");
  error("value_wrongtype",words("structure",struct_),words("value",type_),words("structure",_matrix),words("value",_real));
  return 0;
}

template<> Matrix<complex_t> Value::value<Matrix<complex_t> >() const
{
  if(struct_==_matrix)
  {
    if(type_==_real) return cmplx(*(reinterpret_cast<Matrix<real_t>* >(value_p)));
    else return *(reinterpret_cast<Matrix<complex_t> *>(value_p));
  }
  where("Value::value<Matrix<Complex> >");
  error("value_wrongtype",words("structure",struct_),words("value",type_),words("structure",_matrix),words("value",_complex));
  return 0;
}
//not done for Vector<Vector<T> >and Matrix<Matrix<T> >
// move real value to complex value
Value& Value::moveToComplex()
{
   if(type_==_complex) return *this;
   switch(struct_)
   {
     case _scalar :
     {
       real_t r=*(reinterpret_cast<real_t*>(value_p));
       delete reinterpret_cast<real_t*>(value_p);
       value_p=reinterpret_cast<void*>(new complex_t(r));
     }
     break;
     case _vector :
     {
       Vector<real_t> v=*(reinterpret_cast<Vector<real_t>*>(value_p));
       delete reinterpret_cast<Vector<real_t>*>(value_p);
       value_p=reinterpret_cast<void*>(new Vector<complex_t>(v));
     }
     break;
     case _matrix :
     {
       Matrix<real_t> m=*(reinterpret_cast<Matrix<real_t>*>(value_p));
       delete reinterpret_cast<Matrix<real_t>*>(value_p);
       value_p=reinterpret_cast<void*>(new Matrix<complex_t>(m));
     }
     break;
     default : error("free_error", "type not handled in Value::moveToComplex");
   }
   type_=_complex;
   return *this;
}

//algebraic operator
Value& Value::operator+=(const Value& v)
{
  if(dims()!=v.dims())  error("free_error","inconsistent values in Value::+=");
  Value* pv =const_cast<Value*>(&v);
  bool del = false;
  if(type_==_real && v.type_==_complex) moveToComplex();
  if(type_==_complex && v.type_==_real) {pv=new Value(v); pv->moveToComplex(); del=true;}
  switch(struct_)
   {
     case _scalar :
     {
       if(type_==_real)  *(reinterpret_cast<real_t*>(value_p))+=*(reinterpret_cast<real_t*>(pv->value_p));
       else              *(reinterpret_cast<complex_t*>(value_p))+=*(reinterpret_cast<complex_t*>(pv->value_p));
     } break;
     case _vector : // vector += ?
     {
       if(type_==_real)  *(reinterpret_cast<Vector<real_t>*>(value_p))+=*(reinterpret_cast<Vector<real_t>*>(pv->value_p));
       else              *(reinterpret_cast<Vector<complex_t>*>(value_p))+=*(reinterpret_cast<Vector<complex_t>*>(pv->value_p));
     } break;
     case _matrix : // matrix += ?
     {
       if(type_==_real)  *(reinterpret_cast<Matrix<real_t>*>(value_p))+=*(reinterpret_cast<Matrix<real_t>*>(pv->value_p));
       else              *(reinterpret_cast<Vector<complex_t>*>(value_p))+=*(reinterpret_cast<Matrix<complex_t>*>(pv->value_p));
     } break;
     default : error("free_error","in value+=value, struct not handled");
  }
  if(del) delete pv;
  return *this;
}

Value& Value::operator-=(const Value& v)
{
  if(dims()!=v.dims())  error("free_error","inconsistent values in Value::-=");
  Value* pv =const_cast<Value*>(&v);
  bool del = false;
  if(type_==_real && v.type_==_complex) moveToComplex();
  if(type_==_complex && v.type_==_real) {pv=new Value(v); pv->moveToComplex(); del=true;}
  switch(struct_)
   {
     case _scalar :
     {
       if(type_==_real)  *(reinterpret_cast<real_t*>(value_p))-=*(reinterpret_cast<real_t*>(pv->value_p));
       else              *(reinterpret_cast<complex_t*>(value_p))-=*(reinterpret_cast<complex_t*>(pv->value_p));
     } break;
     case _vector : // vector += ?
     {
       if(type_==_real)  *(reinterpret_cast<Vector<real_t>*>(value_p))-=*(reinterpret_cast<Vector<real_t>*>(pv->value_p));
       else              *(reinterpret_cast<Vector<complex_t>*>(value_p))-=*(reinterpret_cast<Vector<complex_t>*>(pv->value_p));
     } break;
     case _matrix : // matrix += ?
     {
       if(type_==_real)  *(reinterpret_cast<Matrix<real_t>*>(value_p))-=*(reinterpret_cast<Matrix<real_t>*>(pv->value_p));
       else              *(reinterpret_cast<Vector<complex_t>*>(value_p))-=*(reinterpret_cast<Matrix<complex_t>*>(pv->value_p));
     } break;
     default : error("free_error","in value+=value, struct not handled");
  }
  if(del) delete pv;
  return *this;
}

Value& Value::operator*=(const Value& v)
{
  if(v.struct_!=_scalar)  error("free_error","only value*=scalar");
  Value* pv =const_cast<Value*>(&v);
  bool del = false;
  if(type_==_real && v.type_==_complex) moveToComplex();
  if(type_==_complex && v.type_==_real) {pv=new Value(v); pv->moveToComplex(); del=true;}
  switch(struct_)
   {
     case _scalar :
     {
       if(type_==_real)  *(reinterpret_cast<real_t*>(value_p))*=*(reinterpret_cast<real_t*>(pv->value_p));
       else              *(reinterpret_cast<complex_t*>(value_p))*=*(reinterpret_cast<complex_t*>(pv->value_p));
     } break;
     case _vector : // vector += ?
     {
       if(type_==_real)  *(reinterpret_cast<Vector<real_t>*>(value_p))*=*(reinterpret_cast<real_t*>(pv->value_p));
       else              *(reinterpret_cast<Vector<complex_t>*>(value_p))*=*(reinterpret_cast<complex_t*>(pv->value_p));
     } break;
     case _matrix : // matrix += ?
     {
       if(type_==_real)  *(reinterpret_cast<Matrix<real_t>*>(value_p))*=*(reinterpret_cast<real_t*>(pv->value_p));
       else              *(reinterpret_cast<Vector<complex_t>*>(value_p))*=*(reinterpret_cast<complex_t*>(pv->value_p));
     } break;
     default : error("free_error","in value+=value, struct not handled");
  }
  if(del) delete pv;
  return *this;
}

Value& Value::operator/=(const Value& v)
{
  if(v.struct_!=_scalar)  error("free_error","only value/=scalar");
  Value* pv =const_cast<Value*>(&v);
  bool del = false;
  if(type_==_real && v.type_==_complex) moveToComplex();
  if(type_==_complex && v.type_==_real) {pv=new Value(v); pv->moveToComplex(); del=true;}
  switch(struct_)
   {
     case _scalar :
     {
       if(type_==_real)  *(reinterpret_cast<real_t*>(value_p))/=*(reinterpret_cast<real_t*>(pv->value_p));
       else              *(reinterpret_cast<complex_t*>(value_p))/=*(reinterpret_cast<complex_t*>(pv->value_p));
     } break;
     case _vector : // vector += ?
     {
       if(type_==_real)  *(reinterpret_cast<Vector<real_t>*>(value_p))/=*(reinterpret_cast<real_t*>(pv->value_p));
       else              *(reinterpret_cast<Vector<complex_t>*>(value_p))/=*(reinterpret_cast<complex_t*>(pv->value_p));
     } break;
     case _matrix : // matrix += ?
     {
       if(type_==_real)  *(reinterpret_cast<Matrix<real_t>*>(value_p))/=*(reinterpret_cast<real_t*>(pv->value_p));
       else              *(reinterpret_cast<Vector<complex_t>*>(value_p))/=*(reinterpret_cast<complex_t*>(pv->value_p));
     } break;
     default : error("free_error","in value+=value, struct not handled");
  }
  if(del) delete pv;
  return *this;
}


} // end of namespace xlifepp
