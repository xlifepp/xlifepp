/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file String.cpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 7 dec 2011
  \date 3 may 2012

  \brief Implementation of xlifepp::string_t utilities.
 */

#include "String.hpp"
#include "PrintStream.hpp"
#include <cctype>
#include <sys/stat.h>

namespace xlifepp
{

//! convert "AbCdefg" to "abcdefg"
string_t lowercase(const string_t& s)
{
  string_t t(s);
  for (string_t::iterator s_i = t.begin(); s_i < t.end() ; ++s_i)
  {
    *s_i = tolower(*s_i);
  }
  return t;
}

//! convert "AbCdefg" to "ABCDEFG"
string_t uppercase(const string_t& s)
{
  string_t t(s);
  for (string_t::iterator s_i = t.begin(); s_i < t.end() ; ++s_i)
  {
    *s_i = toupper(*s_i);
  }
  return t;
}

//! convert "abCdeF" to "AbCdeF"
string_t capitalize(const string_t& s)
{
  string_t t(s);
  *(t.begin()) = toupper(*(t.begin()));
  return t;
}

//! convert " a b c d e f  " to "a b c d e f  "
string_t trimLeading(const string_t& s, const char* delim)
{
  string_t t(s);
  t.erase(0, t.find_first_not_of(delim));
  return t;
}

//! convert " a b c d e f  " to " a b c d e f"
string_t trimTrailing(const string_t& s, const char* delim)
{
  string_t t(s);
  t.erase(t.find_last_not_of(delim) + 1);
  return t;
}

//! convert " a b c d e f  " to "a b c d e f"
string_t trim(const string_t& s, const char* delim)
{
  string_t b = trimLeading(s, delim);
  string_t t = trimTrailing(b, delim);
  return t;
}

//! convert " a b c d e f  " to "abcefd"
string_t delSpace(const string_t& s)
{
  string_t t;
  number_t p = 0;
  while (p != s.size())
  {
    char c = s[p];
    if (c != ' ') { t += c; }
    p++;
  }
  return t;
}

//! replace all string s1 by string s2 in string s
string_t& replaceString(string_t& s, const string_t& s1, const string_t& s2)
{
  number_t p = s.find(s1), l1 = s1.size(), l2=s2.size();
  if (l2 == 0) { return s; }
  while (p != string_t::npos)
  {
    s.replace(p, l1, s2);
    p += l2;
    p = s.find(s1, p);
  }
  return s;
}

//! remove all char c from string s
string_t& removeChar(string_t& s, char c)
{
  string_t ns;
  for (number_t p = 0; p < s.size(); p++)
    if (s[p] != c) { ns+=s[p];}
  s=ns;
  return s;
}

//! replace all char c1 by char c2 in string s
string_t& replaceChar(string_t& s, char c1, char c2)
{
  for (number_t p = 0; p < s.size(); p++)
    if (s[p] == c1) { s[p] = c2; }
  return s;
}

int_t findString(const string_t s, const std::vector<string_t>& vs)
{
  int_t i=0;
  for (std::vector<string_t>::const_iterator it_vs = vs.begin(); it_vs != vs.end(); it_vs++, i++)
  {
    if ((*it_vs).find(s) == 0) return i;
  }
  return -1;
}

// return file name extension using last point as delimiter
string_t fileExtension(const string_t& f)
{
  string_t ext = "";
  string_t s = trim(f);
  number_t p = s.find_last_of('.');
  if (p != string_t::npos) { ext = s.substr(p + 1); }
  return ext;
}

// return root file name and file name extension using last point as delimiter
std::pair<string_t, string_t> fileRootExtension(const string_t& f, const std::vector<string_t>& authorizedExtensions)
{
  string_t s = trim(f);
  std::pair<string_t, string_t> res(f, ""), res2;
  number_t p = s.find_last_of('.');
  if (p != string_t::npos)
  {
    res2.first = s.substr(0, p);
    res2.second = s.substr(p + 1);
    if (authorizedExtensions.size() == 0) { res=res2; }
    else
    {
      if (std::find(authorizedExtensions.begin(), authorizedExtensions.end(), res2.second) != authorizedExtensions.end())
      { res=res2; }
    }

  }
  return res;
}

// build filename from rootname with an additionnal suffix, and extension
string_t fileNameFromComponents(const string_t& rootname, const string_t& suffix, const string_t& extension)
{
  if (suffix == "") return rootname+"."+extension;
  else return rootname+"_"+suffix+"."+extension;
}

// return basename of a file name using last slash and last point as delimiters
string_t basename(const string_t& f)
{
  number_t i=f.find_last_of(".");
  #ifdef OS_IS_WIN
    int_t j=f.find_last_of("\\");
  #elif defined(OS_IS_UNIX)
    int_t j=f.find_last_of("/");
  #endif
  if (j > f.size()) { j=-1; }
  string_t basename = f.substr(0,i).substr(j+1,i);
  return basename;
}

// return basename of a file name using last slash as delimiter
string_t basenameWithExtension(const string_t& f)
{
  #ifdef OS_IS_WIN
    int_t j=f.find_last_of("\\");
  #elif defined(OS_IS_UNIX)
    int_t j=f.find_last_of("/");
  #endif
  if (j > f.size()) { j=-1; }
  string_t basename = f.substr(j+1);
  return basename;
}

// return dirname of a file name using last slash and last point as delimiters
string_t dirname(const string_t& f)
{
  #ifdef OS_IS_WIN
    int_t j=f.find_last_of("\\");
  #elif defined(OS_IS_UNIX)
    int_t j=f.find_last_of("/");
  #endif
  if (j > f.size()) { return ""; }
  return f.substr(0,j);
}

// return basename of a file name using last slash as delimiter
string_t fileWithoutExtension(const string_t& f)
{
  number_t i=f.find_last_of(".");
  return f.substr(0,i);
}

// convert bool to string true or false
string_t booltoWord(bool b)
{
  if (b) return string_t("true");
  return string_t("false");
}

// check if path exists
bool isPathExist(const string_t &path)
{
  struct stat buffer;
  return (stat (path.c_str(), &buffer) == 0);
}

// adapt path to OS format (WIN/LINUX like)
string_t rightPath(const string_t& path)
{
  string_t rpath=path;
  #ifdef OS_IS_WIN
    replaceChar(rpath,'/','\\');
  #else
    replaceChar(rpath,'\\','/');
  #endif
  return rpath;
}

// adapt path to OS format and check if path exists
string_t securedPath(const string_t& path)
{
  string_t rpath = rightPath(path);
  if(!isPathExist(rpath)) error("free_error","path "+rpath+" not found");
  return rpath;
}

//--------------------------------------------------------------------------------
// add (n>0) or remove (n<0) n blanks from the end of string
// the first char is always kept
//--------------------------------------------------------------------------------
void blanks(std::string& s, int_t n)
{
  if (n>0) s.append(n,' ');
  if (n<0) s.erase(std::max(1,int(s.size()+n-1)));
}

//string form of a complex coefficient in a linear combination
string_t coefAsString(bool isFirst, const complex_t& a)
{
    string_t s;
    if(imag(a)!= 0.)
    {
        if(!isFirst) s= " + ";
        s+= tostring(a) + " * ";
    }
    else
    {
        if(real(a) >= 0.)
        {
            if(!isFirst) s= " + ";
            if(a != 1.) s+=tostring(real(a)) + " * ";
        }
        else
        {
            s+= " - ";
            if(a != -1.) s+=tostring(-real(a)) + " * ";
        }
    }
    return s;
}


//format a string to fill exactly l chars with centering option
//when size is greater, string is cut, when l is larger than string size is centering
string_t format(const string_t& s, number_t l, Alignment al)
{
  number_t n=s.size();
  if(n >= l) return s.substr(0,l);
  if(n + 1 == l) return s+" ";
  number_t k=l-n;
  switch (al)
  {
     case _leftAlignment: return s+string_t(k,' ');
     case _rightAlignment: return string_t(k,' ')+s;
     default: break;
  }
  return string_t(k/2,' ')+s+string_t(k-k/2,' ');
}

} // end of namespace xlifepp
