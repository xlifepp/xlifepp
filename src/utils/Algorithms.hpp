/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Algorithms.hpp
  \author E. Lunéville
  \since 14 dec 2011
  \date 08 feb 2016

  \brief templated algorithms generalizing stl algorithms

  names ends with Tpl suffix to avoid possible confusion with stl algorithms
 */

#ifndef ALGORITHMS_HPP
#define ALGORITHMS_HPP

#include <algorithm>
#include <numeric>
#include <vector>

#include "config.h"
#include "complexUtils.hpp"
#include "Trace.hpp"
#include "Messages.hpp"

namespace xlifepp
{

/*
--------------------------------------------------------------------------------
min and max with cast (if possible)
--------------------------------------------------------------------------------
*/
template<typename T, typename U>
T minTpl(const T& a, const U& b) {return ( ((a) < T(b)) ? (a) : T(b) ); } //!< returns minimum of 2 values

template<typename T, typename U, typename V>
T minTpl(const T& a, const U& b, const V& c) { return minTpl(minTpl(a, b), c); } //!< returns minimum of 3 values

template<typename T, typename U>
T maxTpl(const T& a, const U& b) { return ( ((a) > T(b)) ? (a) : T(b) ); } //!< returns maximum of 2 values

template<typename T, typename U, typename V>
T maxTpl(const T& a, const U& b, const V& c) { return maxTpl(maxTpl(a, b), c); } //!< returns maximum of 3 values

/*
--------------------------------------------------------------------------------
unary operations on items list defined by iterators
assuming that operation is valid on items of the list
--------------------------------------------------------------------------------
*/
template<typename T1_iterator, typename R_iterator>
void absTpl(T1_iterator b1, T1_iterator e1, R_iterator Rb) //! returns magnitude of vector entries: R[i] = abs(T1[i])
{
  using std::abs;
  R_iterator Rit(Rb);
  for( T1_iterator t1 = b1; t1 != e1; t1++, Rit++ ) { *Rit = abs(*t1); }
}

template<typename T1_iterator, typename R_iterator>
void conjTpl(T1_iterator b1, T1_iterator e1, R_iterator Rb) //! returns complex conjugate of vector entries: R[i] = conj(T1[i])
{
  R_iterator Rit(Rb);
  for ( T1_iterator t1 = b1; t1 != e1; t1++, Rit++ ) { *Rit = conj(*t1); }
}

template<typename T1_iterator, typename R_iterator>
void realTpl(T1_iterator b1, T1_iterator e1, R_iterator Rb) //! returns real part of vector entries: R[i] = real(T1[i])
{
  R_iterator Rit(Rb);
  for ( T1_iterator t1 = b1; t1 != e1; t1++, Rit++ ) { *Rit = (*t1).real(); }
}

template<typename T1_iterator, typename R_iterator>
void imagTpl(T1_iterator b1, T1_iterator e1, R_iterator Rb) //! returns imaginary part of vector entries: R[i] = imag(T1[i])
{
  R_iterator Rit(Rb);
  for ( T1_iterator t1 = b1; t1 != e1; t1++, Rit++ ) { *Rit = (*t1).imag(); }
}

/*
--------------------------------------------------------------------------------
Min, Max of a items list defined by iterators
assuming that abs() is defined on items of the list and return a real
--------------------------------------------------------------------------------
*/

template<typename T_iterator>
T_iterator minElementTpl(T_iterator b, T_iterator e) //! returns iterator to first occurrence of minimum absolute magnitude in stl container
{
  using std::abs;
  T_iterator itm(b);
  real_t m(abs(*b));
  for (T_iterator it = b + 1; it != e; it++)
  {
    if (m > real_t(abs(*it)))
    {
      m = real_t(abs(*it));
      itm = it;
    }
  }
  return itm;
}

template<typename T_iterator>
T_iterator maxElementTpl(T_iterator b, T_iterator e) //! returns iterator to first occurrence of maximum absolute magnitude in stl container
{
  using std::abs;
  T_iterator itm(b);

  real_t m(abs(*b));
  for (T_iterator it = b + 1; it != e; it++)
  {
    if (m < real_t(abs(*it)))
    {
      m = real_t(abs(*it));
      itm = it;
    }
  }
  return itm;
}


template <typename T> int sign(T val) {
  return (T(0) < val) - (val < T(0));
}

/*
--------------------------------------------------------------------------------
binary operations on items list defined by iterators
assuming that operation is valid on items of the list
lists should have same size
or at least the second list should not be shorter than the first one
--------------------------------------------------------------------------------
*/
template<typename T1_iterator, typename T2_iterator>
complex_t hermitianInnerProductTpl(T1_iterator b1, T1_iterator e1, T2_iterator b2) //! hermitian product
{
  T2_iterator it2(b2);
  complex_t hp(0, 0);
  for ( T1_iterator it1 = b1; it1 != e1; it1++, it2++ ) { hp += *it1 * conj(*it2); }
  return hp;
}

// /*
// --------------------------------------------------------------------------------
// swaps any two objects of same type/class
// --------------------------------------------------------------------------------
// **/
// template<typename T>
// void tpl_swap(T&a, T&b) { T c=a; a=b; b=c; }

// /*
// --------------------------------------------------------------------------------
// tpl_binary_search searches rank in a sorted stl container in range [first,last[
//      returns iterator to object when found, end iterator when not
//      requires an "operator<" in class of container objects
// --------------------------------------------------------------------------------
// **/
// /*
// search in a stl vector of objects, bound by iterators in interval [begin, end[
// [ this function is equivalent to lower_bound with same arguments only when
//    "sought" is indeed a value in the iterator interval or is "greater" then *(end-1) ]
// */
// template<typename Titerator, typename T>
// Titerator tpl_binary_search(Titerator begin_i, Titerator end_i, const T& sought)
// {
//   Titerator found = std::lower_bound(begin_i, end_i, sought);
//   if ( found != end_i and !(sought < *found) ) return found;
//   else return end_i;
// }

// /*
// search an object position in a stl vector of pointers, bound in interval [first, last[
//   also requires "operator==" in class of container objects
// [ for now, this function is only used to find the rank of an element in a vector of
//   pointers to elements sorted by increasing number ]
// */
// template<typename T>
// size_t tpl_binary_search(const std::vector<T*>& sorted_Items, const size_t first, const size_t last,
//                         const T& searched_Item)
// {
//   typename std::vector<T*>::consTiterator sorted_Items_it = sorted_Items.begin();
//   if ( searched_Item == *(*(sorted_Items_it + first)) ) return first;
//   else if ( searched_Item == *(*(sorted_Items_it + last-1)) ) return last-1;

//   size_t rfirst(first), rlast(last-1);
//   while ( rfirst <= rlast )
//   {
//      size_t mid = (rfirst+rlast)/2;
//      if ( searched_Item < *(*(sorted_Items_it + mid)) ) rlast = mid-1;
//      else if ( *(*(sorted_Items_it + mid)) < searched_Item ) rfirst = mid+1;
//      else return mid;
//   }
//   return last;
// }

// /*
// --------------------------------------------------------------------------------
// iterator stl-like template functions
// ** NO CHECKED IS DONE concerning length compatibility of operands **
// - bi (resp. ei) is a begin iterator (resp. end iterator) on operand ti (i = 1, 2, )
// - Rb is a beginning iterator on result R
// --------------------------------------------------------------------------------
// */
// template<typename T1_iterator, typename R_iterator>
// void tpl_abs(T1_iterator b1, T1_iterator e1, R_iterator Rb) //! returns magnitude of vector entries: R[i] = abs(T1[i])
// {
//   R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, Rit++ ) *Rit = abs(*t1);
// }

// template<typename T1_iterator, typename R_iterator>
// void tpl_conj(T1_iterator b1, T1_iterator e1, R_iterator Rb)
// //! returns complex conjugate of vector entries: R[i] = conj(T1[i])
// {
//   R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, Rit++ ) *Rit = conj(*t1);
// }

// template<typename T1_iterator, typename R_iterator>
// void tpl_imag(T1_iterator b1, T1_iterator e1, R_iterator Rb)
// //! returns complex conjugate of vector entries: R[i] = abs(T1[i])
// {
//   R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, Rit++ ) *Rit = (*t1).imag();
// }


// template<typename T1_iterator, typename R_iterator>
// void tpl_opposite(T1_iterator b1, T1_iterator e1, R_iterator Rb)
// //! returns opposite of vector entries: R[i] = -T1[i]
// {
//   R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, Rit++ ) *Rit = - *t1;
// }

// template<typename scalar_, typename TR_iterator>
// void tpl_scale(const scalar_& lambda, TR_iterator b, TR_iterator e)
// //! container scale: R[i] = lambda*R[i]
// {
//   for ( TR_iterator Rit = b; Rit != e; Rit++ ) *Rit *= lambda;
// }

// template<typename scalar_, typename T1_iterator, typename R_iterator>
// void tpl_scale(const scalar_& lambda, T1_iterator b1, T1_iterator e1,
//               R_iterator Rb)
// //! vector scale: R[i] = lambda*T1[i]
// {
//   R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, Rit++ ) *Rit = lambda * *t1;
// }

// template<typename T1_iterator, typename T2_iterator, typename R_iterator>
// void tpl_add(T1_iterator b1, T1_iterator e1, T2_iterator b2,
//             R_iterator Rb)
// //! addition of vectors: R[i] = T1[i] + T2[i]
// {
//   T2_iterator t2(b2); R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, t2++, Rit++ ) *Rit = *t1 + *t2;
// }

// template<typename T1_iterator, typename R_iterator>
// void tpl_add(T1_iterator b1, T1_iterator e1,
//             R_iterator Rb)
// //! addition to vector: R[i] = R[i] + T1[i]
// {
//   R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, Rit++ ) *Rit += *t1;
// }

// template<typename T1_iterator, typename R_iterator>
// void tpl_add(T1_iterator b1, T1_iterator e1,
//             R_iterator Rb, std::vector<size_t>::iterator ptrb)
// //! addition to vector with indirection: R[ptr[i]] = R[ptr[i]] + T1[i]
// {
//   std::vector<size_t>::iterator ptr_i(ptrb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, ptr_i++ )
//      *(Rb + *ptr_i) += *t1;
// }

// template<typename scalar_, typename T1_iterator, typename R_iterator>
// void tpl_scale_then_add(const scalar_& lambda, T1_iterator b1, T1_iterator e1,
//                        R_iterator Rb)
// //! linear combination: R[i] = R[i] + lambda*T1[i]
// {
//   R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, Rit++ )
//      *Rit += lambda * *t1;
// }

// template<typename scalar_, typename T1_iterator, typename R_iterator>
// void tpl_scale_then_add(const scalar_& lambda, T1_iterator b1, T1_iterator e1,
//                        R_iterator Rb, std::vector<size_t>::iterator ptrb)
// //! linear combination with indirection: R[ptr[i]] = R[ptr[i]] + lambda*T1[i]
// {
//   std::vector<size_t>::iterator ptr_i(ptrb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, ptr_i++ )
//      *(Rb + *ptr_i) += lambda * *t1;
// }

// template<typename T1_iterator, typename T2_iterator, typename R_iterator>
// void tpl_subtract(T1_iterator b1, T1_iterator e1, T2_iterator b2, R_iterator Rb)
// //! subtraction of vectors R[i] = T1[i] - T2[i]
// {
//   T2_iterator t2(b2); R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, t2++, Rit++ )
//      *Rit = *t1 - *t2;
// }

// template<typename T1_iterator, typename R_iterator>
// void tpl_subtract(T1_iterator b1, T1_iterator e1, R_iterator Rb)
// //! subtract from vector R[i] = R[i] - T1[i]
// {
//   R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, Rit++ )
//      *Rit -= *t1;
// }

// template<typename T1_iterator, typename T2_iterator, typename R_iterator>
// void tpl_entrywise_multiply(T1_iterator b1, T1_iterator e1, T2_iterator b2, R_iterator Rb)
// {
//   T2_iterator t2(b2); R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, t2++, Rit++ )
//      *Rit = *t1 * *t2;
// }

// template<typename T1_iterator, typename T2_iterator, typename R_iterator>
// void tpl_entrywise_divide(T1_iterator b1, T1_iterator e1, T2_iterator b2, R_iterator Rb)
// {
//   T2_iterator t2(b2); R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, t2++, Rit++ )
//      *Rit = *t1 / *t2;
// }


/* linear combination of 2 stl vectors defined by iterators
   R[i] = a1 * T1[i] + a2 *T2 [i]
   WARNING*** returns input iterator after incremention */

// template<typename scalar1, typename T1_iterator, typename scalar2, typename T2_iterator, typename R_iterator>
// R_iterator& linearCombinationTpl(scalar1 a1, T1_iterator b1, T1_iterator e1,
//                                 scalar2 a2, T2_iterator b2, R_iterator& Rb)
// {
//   T2_iterator t2(b2);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, t2++, Rb++)
//      *Rb = a1 * *t1 + a2 * *t2;
//   return Rb;
// }

// template<typename scalar1, typename T1_iterator, typename scalar2, typename T2_iterator, typename scalar3, typename T3_iterator, typename R_iterator>
// R_iterator& tpl_linear_combination(scalar1 a1, T1_iterator b1, T1_iterator e1,
//                                   scalar2 a2, T2_iterator b2,
//                                   scalar3 a3, T3_iterator b3, R_iterator& Rb)
// //! linear combination of 3 stl vectors defined by iterators
// //!  R[i] = a1 * T1[i] + a2 *T2 [i] + a3 *T3 [i]
// //! ***WARNING*** returns input iterator after incremention
// {
//   T2_iterator t2(b2); T3_iterator t3(b3);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, t2++, t3++, Rb++)
//      *Rb = a1 * *t1 + a2 * *t2 + a3 * *t3;
//   return Rb;
// }

// template<typename T1_iterator, typename R_iterator, typename N_iterator>
// void tpl_redirect(T1_iterator t1b, N_iterator n1b, N_iterator n1e,
//                  R_iterator rb, N_iterator nrb, N_iterator nre)
// {
//   // redirection: R[k] = T1[i] where nr[k] = n1[i] (intersection of nr and n1 may be empty)
//   // t1b and rb are beginning iterators on "vectors" T1 and R
//   // n1b and n1e are begin and end iterators on numbering vector n1 associated with T1
//   // nrb and nre are begin and end iterators on numbering vector nr associated with R
//   T1_iterator t1(t1b);
//   N_iterator nri(nrb), found;
//   for ( N_iterator n1i = n1b; n1i != n1e; n1i++, t1++ )
//   {
//      // find iterator (position) for (global) number *n1i associated with T1
//      // in range [nri, nre[ of iterators on (global) numbers associated with R
//      // *n1i may not be found in range [nri, nre[
//      found = std::lower_bound(nri, nre, *n1i);
//      if ( found != nre and !(*n1i < *found) )
//      {
//         // set current value into corresponding position in result R
//         // and set new search at position found
//         *(rb + (found - nrb)) = *t1;
//         nri = found+1;
//      }
//   }
// }

// template<typename T1_iterator, typename R_iterator, typename N_iterator>
// void tpl_assemble(T1_iterator t1b, N_iterator n1b, N_iterator n1e,
//                  R_iterator rb, N_iterator nrb, N_iterator nre)
// {
//   // assembly: R[k] = R[k] + T1[i] where nr[k] = n1[i] with n1 a subset of nr
//   // t1b and rb are beginning iterators on "vectors" T1 and R
//   // n1b and n1e are begin and end iterators on numbering vector n1 associated with T1
//   // nrb and nre are begin and end iterators on numbering vector nr associated with R
//   T1_iterator t1(t1b);
//   N_iterator nri(nrb), found;
//   for ( N_iterator n1i = n1b; n1i != n1e; n1i++, t1++ )
//   {
//      // find iterator (position) for (global) number *n1i associated with T1
//      // in range [nri, nre[ of iterators on (global) numbers associated with R
//      // *n1i must be lie in range [nri, nre[
//      found = std::lower_bound(nri, nre, *n1i);
//      if ( found != nre )
//      {
//         *(rb + (found - nrb)) += *t1;
//         nri = found+1;
//      }
//   }
// }

// template<typename scalar_, typename T1_iterator, typename R_iterator, typename N_iterator>
// void tpl_assemble(scalar_ lambda, T1_iterator t1b, N_iterator n1b, N_iterator n1e,
//                  R_iterator rb, N_iterator nrb, N_iterator nre)
// {
//   // assembly: R[k] = R[k] + lambda * T1[i] where rnv[k] = nv1[i] with n1 a subset of nr
//   // t1b and rb are beginning iterators on "vectors" T1 and R
//   // n1b and n1e are begin and end iterators on numbering vector n1 associated with T1
//   // nrb and nre are begin and end iterators on numbering vector nv associated with R
//   T1_iterator t1(t1b);
//   N_iterator nri(nrb), found;
//   for ( N_iterator n1i = n1b; n1i != n1e; n1i++, t1++ )
//   {
//      // find iterator (position) for (global) number *n1i associated with T1
//      // in range [nri, nre[ of iterators on (global) numbers associated with R
//      // *n1i must be lie in range [nri, nre[
//      found = std::lower_bound(nri, nre, *n1i);
//      if ( found != nre )
//      {
//         *(rb + (found - nrb)) += lambda * *t1;
//         nri = found+1;
//      }
//   }
// }

//@{
//! norm of scalars
inline real_t norm(const real_t & v) { return std::abs(v); }
inline real_t norm(const complex_t & v) { return std::abs(v); }
//@}

//! norm of vector
template <typename T>
real_t norm(const std::vector<T>& v)
{
  real_t R = 0;
  typename std::vector<T>::const_iterator it;
  for(it = v.begin(); it != v.end(); it++) { R += norm(*it) * norm(*it); }
  return std::sqrt(R);
}

//@{
//! zero for scalars
inline real_t zeros(const real_t& v) { return 0.; }
inline complex_t zeros(const complex_t& v) { return complex_t(0.,0.); }
//@}

//! zero for a vector
template <typename T>
std::vector<T> zeros(const std::vector<T>& v)
{
  std::vector<T> res(v.size(), zeros(v[0]));
  return res;
}

//@{
//! round to zero for scalars
inline real_t roundToZero(const real_t& v, real_t asZero = std::sqrt(theEpsilon))
{
  if (norm(v)<asZero) return zeros(v);
  return round(v/asZero)*asZero;
}
inline complex_t roundToZero(const complex_t& v, real_t asZero = std::sqrt(theEpsilon))
{
  if (norm(v)<asZero) return zeros(v);
  return complex_t(roundToZero(v.real(),asZero), roundToZero(v.imag(),asZero));
}
//@}

//! round to zero for vectors
template <typename T>
std::vector<T> roundToZero(const std::vector<T>& v, real_t asZero = std::sqrt(theEpsilon))
{
  if (norm(v)<asZero) return zeros(v);
  std::vector<T> vv(v.size(),v[0]);
  for (number_t i=0; i < v.size(); ++i) { vv[i]=roundToZero(v[i], asZero); }
  return vv;
}

template <typename T>
Vector<T> roundToZero(const Vector<T>& v, real_t asZero = std::sqrt(theEpsilon))
{
  if (norm(v)<asZero) return zeros(v);
  Vector<T> vv(v.size(),v[0]);
  for (number_t i=0; i < v.size(); ++i) { vv[i]=roundToZero(v[i], asZero); }
  return vv;
}

//! round to nearest integer
template <typename T>
int_t round(const T& v)
{
  #if __cplusplus > 199711L
    return int_t(std::round(v));
  #else
    return int_t(std::floor(v + 0.5));
  #endif
}

template <typename T>
const T& round(const T& t, real_t prec){return t;}

//real specialization
inline real_t round(const real_t& x, real_t prec)
{return xlifepp::round(x/prec)*prec;}

//complex specialization
inline complex_t round(const complex_t& z, real_t prec)
{return complex_t(round(z.real(),prec),round(z.imag(),prec));}

//vector specialization
template<typename T>
std::vector<T> round(const std::vector<T>& v, real_t prec)
{
    std::vector<T> r=v;
    typename std::vector<T>::iterator it;
    for(it=r.begin();it!=r.end();++it) *it=round(*it,prec);
    return r;
}
template<typename T>
Vector<T> round(const Vector<T>& v, real_t prec)
{
    Vector<T> r=v;
    typename Vector<T>::iterator it;
    for(it=r.begin();it!=r.end();++it) *it=round(*it,prec);
    return r;
}

//list specialization
template<typename T>
std::list<T> round(const std::list<T>& l, real_t prec)
{
    std::list<T> r=l;
    typename std::list<T>::iterator it;
    for(it=r.begin();it!=r.end();++it) *it=round(*it,prec);
    return r;
}

//map specialization
template<typename K, typename T>
std::map<K,T> round(const std::map<K,T>& m, real_t prec)
{
 std::map<K,T> r=m;
 typename std::map<K,T>::iterator it;
 for(it=r.begin();it!=r.end();++it)
 {
   it->second=round(it->second,prec);
 }
 return r;
}

// container specialization using iterator
template <typename Iterator>
void round(Iterator itb, Iterator ite, Iterator rt, real_t prec)
{
  for(Iterator it=itb;it!=ite; ++ite, ++rt) *rt = round(*it,prec);
}

//@{
//! crossproduct of two vectors (dimension 2 and 3), be care in 2D return a 1-vector
template <typename T>
std::vector<T> crossProduct(const std::vector<T>& u, const std::vector<T>& v)
{
  dimen_t d = u.size();
  if (d != v.size())
  {
    where("crossProduct");
    error("vec_mismatch_dims",d,v.size());
  }
  if (d < 2 || d > 3) { error("2D_or_3D_only","crossProduct"); }
  if (d == 2) { return std::vector<T>(1, u[0] * v[1] - u[1] * v[0]); }
  std::vector<T> w(3, 0);
  w[0] = u[1] * v[2] - u[2] * v[1];
  w[1] = u[2] * v[0] - u[0] * v[2];
  w[2] = u[0] * v[1] - u[1] * v[0];

  return w;
}

template <typename T>
std::vector<T> operator^(const std::vector<T>& v1, const std::vector<T>& v2)
{ return crossProduct(v1, v2); }
//@}

/*!signed or unsigned angle u to v in [-pi,pi], n unit normal to u-v plane
   in 2D and 3D;  |n|=1 and n^(u^v) = 0 are not checked !
   sign of angle is related to n,
   if n is not given, n=u^v is chosen and thus angle is always >=0 (not signed)
   if u=0 or v=0 it returns 0 */
inline real_t angle(const std::vector<real_t>& u, const std::vector<real_t>& v, const std::vector<real_t>& n=std::vector<real_t>())
{
  dimen_t d = u.size();
  if(d != v.size()){ where("angle"); error("vec_mismatch_dims",d,v.size()); }
  if (d < 2 || d > 3) { error("2D_or_3D_only","angle"); }
  real_t nuv, duv=dot(u,v);
  std::vector<real_t> uxv(3,0);
  uxv[2] = u[0]*v[1]-u[1]*v[0];    // crossProduct() is not used because
  if(u.size()==3)                  // it returns a 1d-vector in 2D
  {                                // besides it is fast
    uxv[0] = u[1]*v[2]-u[2]*v[1];
    uxv[1] = u[2]*v[0]-u[0]*v[2];
  }
  if(n.size()==0) nuv = norm2(uxv); //choose normal n = uxv/|uxv|
  else            nuv = dot(uxv,n);
  if(std::abs(nuv) < theTolerance) return (duv>=0?0.:pi_); //P,Q quasi-parallel
  return std::atan2(nuv,duv);
}


/*!
  permutation of a vector
     v: vector to be permuted
     vp: permuted vector
     p: permutation (index starts from 0)
  permutation is driven by the permutation vector p that may be smaller than the vector v (but not larger!):
           vp[i]= v[p[i]]    i=0, p.size()
*/
template <typename T>
std::vector<T>& permute(std::vector<T>& v, std::vector<T>& vp, const std::vector<number_t>& p)
{
  //check for Id permutation
  std::vector<number_t>::const_iterator itp;
  number_t k=0;
  bool id=true;
  for (itp=p.begin();itp!=p.end() && id;++itp,++k) id = *itp!=k;
  if (id)
  {
    if (&vp==&v) return vp;
    return vp=v;
  }
  //non id permutation
  std::vector<T>* wp=&vp;
  if (wp==&v) wp=new std::vector<T>(v);  // copy v to deal with self-permutation
  else vp.resize(v.size());             // insure correct size of vp
  number_t nwp=wp->size();
  typename std::vector<T>::iterator itwp=wp->begin();
  for (itp=p.begin();itp!=p.end();++itp, ++itwp)
  {
    if (*itp > nwp) error("is_greater", *itp, nwp);
    else *itwp=v[*itp];
  }
  if (&vp==&v) // move wp to vp=v and delete the work copy wp
  {
    vp=*wp;
    delete wp;
  }
  return vp;
}

/*!
  inverse permutation of a vector
     v: vector to be permuted
     vp: permuted vector
     p: permutation (index starts from 0)
  permutation is driven by the permutation vector p that may be smaller than the vector v (but not larger!):
           vp[p[i]]= v[i]    i=0, p.size()
*/
template <typename T>
std::vector<T>&  permuteInv(std::vector<T>& v, std::vector<T>& vp, const std::vector<number_t>& p)
{
  //check for Id permutation
  std::vector<number_t>::const_iterator itp;
  number_t k=0;
  bool id=true;
  for (itp=p.begin();itp!=p.end() && id;++itp,++k) id = *itp!=k;
  if (id)
  {
    if (&vp==&v) return vp;
    return vp=v;
  }
  //non id permutation
  std::vector<T>* wp=&vp;
  if (wp==&v) wp=new std::vector<T>(v);  // copy v to deal with self-permutation
  else vp.resize(v.size());             // insure correct size of vp
  number_t nwp=wp->size();
  typename std::vector<T>::iterator itv=v.begin();
  for (itp=p.begin();itp!=p.end();++itp, ++itv)
  {
    if (*itp > nwp) error("is_greater", *itp, nwp);
    else (*wp)[*itp]=*itv;
  }
  if (&vp==&v) // move wp to vp=v and delete the work copy wp
  {
    vp=*wp;
    delete wp;
  }
  return vp;
}

/*!
  return vector xi=a+i(b_a)/(n-1)  i=0,n-1
*/
template <typename T>
std::vector<T> linSpace(const T& a, const T& b, number_t n)
{
  if (n<2) error("free_error","number of value must be greater than 1");
  T dx=(b-a)/(n-1), x=a;
  std::vector<T> xs(n);
  typename std::vector<T>::iterator it=xs.begin();
  for (;it!=xs.end();++it,x+=dx) *it=x;
  xs[n-1]=b;
  return xs;
}

// //! cross product: R = T1 ^ T2 with dim T1 = dim T2
// template<typename T1_iterator, typename T2_iterator, typename R_iterator>
// void crossProductTpl(const dimen_t d, T1_iterator b1, T2_iterator b2, R_iterator Rb)
// {
//   R_iterator Rit(Rb);
//   if ( d == 3 )
//   {
//      // 3d cross product
//      *Rit++ = *(b1+1) * *(b2+2) - *(b1+2) * *(b2+1);
//      *Rit++ = *(b1+2) * *(b2)   - *(b1)  *  *(b2+2);
//      *Rit   = *(b1)  *  *(b2+1) - *(b1+1) * *(b2);
//   }
//   else if ( d == 2 )
//   {
//      // 2d cross product
//      *Rit = *(b1) * *(b2+1) - *(b1+1) * *(b2);
//   }
// }

// template<typename T1_iterator, typename T2_iterator, typename R_iterator>
// void tpl_cross_product(const dimen_t d1, T1_iterator b1,
//                       const dimen_t d2, T2_iterator b2, R_iterator Rb)
// //! cross product: R = T1 ^ T2 for any combination of dimenions
// {
//   R_iterator Rit(Rb);
//   if ( d1 == d2 )
//   {
//      if ( d1 == 3 )
//      {
//         // 3d cross product
//         *Rit++ = *(b1+1) * *(b2+2) - *(b1+2) * *(b2+1);
//         *Rit++ = *(b1+2) * *(b2)   - *(b1)  *  *(b2+2);
//         *Rit   = *(b1)  *  *(b2+1) - *(b1+1) * *(b2);
//      }
//      else if ( d1 == 2 )
//      {
//         // 2d cross product
//         *Rit = *(b1) * *(b2+1) - *(b1+1) * *(b2);
//      }

//   }
//   else if ( d1 == 3 )
//   {
//      *Rit++ = - *(b1+2) * *(b2+1);
//      *Rit++ =   *(b1+2) * *(b2);
//      *Rit   =   *(b1)   * *(b2+1) - *(b1+1) * *(b2);
//   }
//   else if ( d1 == 2 )
//   {
//      *Rit++ =   *(b1+1) * *(b2+2);
//      *Rit++ = - *(b1)   * *(b2+2);
//      *Rit   =   *(b1)   * *(b2+1) - *(b1+1) * *(b2);
//   }
// }

template<typename T1_iterator, typename T2_iterator, typename R_iterator>
void tensorProductTpl(T1_iterator b1, T1_iterator e1, T2_iterator b2, T2_iterator e2, R_iterator Rb) //! accumulated tensor product: "R[i,j] = R[i,j] + T1[i] * T2[j]"
{
  R_iterator Rit(Rb);
  for ( T1_iterator t1 = b1; t1 != e1; t1++ )
  {
    for ( T2_iterator t2 = b2; t2 != e2; t2++, Rit++ )
    { *Rit += *t1 * *t2; }
  }
}

// template<typename scalar_, typename T1_iterator, typename T2_iterator, typename R_iterator>
// void tpl_tensor_producTscaled(const scalar_& lambda, T1_iterator b1, T1_iterator e1,
//                               T2_iterator b2, T2_iterator e2, R_iterator Rb)
// //! accumulated vector tensor product scaled:
// //! R[i,j] = R[i,j] + lambda * T1[i] * T2[j]
// {
//   R_iterator Rit(Rb);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++ )
//      for ( T2_iterator t2 = b2; t2 != e2; t2++, Rit++ )
//         *Rit += lambda* *t1 * *t2;
// }

// template<typename T1_iterator, typename T2_iterator, typename R_iterator>
// void tpl_tensor_producTsymm(T1_iterator b1, T1_iterator e1,
//                             T2_iterator b2, T2_iterator e2, R_iterator Rb)
// //! accumulated symmetric vector tensor product:
// //! "R[i,j] = R[i,j] + T1[i] * T2[j]", j <= i (only lower triangular part is stored)
// {
//   R_iterator Rit(Rb);
//   number_t r(0);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, r++ )
//      for ( T2_iterator t2 = b2; t2 != b2+tpl_min(r+1,e2-b2); t2++, Rit++ )
//         *Rit += *t1 * *t2;
// }

// template<typename scalar_, typename T1_iterator, typename T2_iterator, typename R_iterator>
// void tpl_tensor_producTsymm_scaled(const scalar_& lambda, T1_iterator b1, T1_iterator e1,
//                                    T2_iterator b2, T2_iterator e2, R_iterator Rb)
// //! accumulated symmetric tensor product scaled:
// //! "R[i,j] = R[i,j] + lambda * T1[i] * T2[j]" , j <= i  (only lower triangular part is stored)
// {
//   R_iterator Rit(Rb);
//   number_t r(0);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, r++  )
//      for ( T2_iterator t2 = b2; t2 != b2+tpl_min(r+1,e2-b2); t2++, Rit++ )
//         *Rit += lambda* *t1 * *t2;
// }

// template<typename unsigned_, typename T1_iterator>
// T1_iterator tpl_Min(T1_iterator b1, T1_iterator e1, unsigned_ no)
// //! returns iterator to first occurrence of minimum magnitude in stl container
// {
//   T1_iterator Tm(b1);
//   unsigned_ m(no);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++ )
//      if ( m > *t1 ) { m = *t1; Tm = t1; }
//   return Tm;
// }
// template<typename unsigned_, typename T1_iterator>
// T1_iterator tpl_Max(T1_iterator b1, T1_iterator e1, unsigned_ no)
// //! returns iterator to first occurrence of maximum magnitude in stl container
// {
//   T1_iterator Tm(b1);
//   unsigned_ m(no);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++ )
//      if ( m < *t1 ) { m = *t1; Tm = t1; }
//   return Tm;
// }

// template<typename T1_iterator>
// T1_iterator tpl_Min(T1_iterator b1, T1_iterator e1)
// //! returns iterator to first occurrence of minimum absolute magnitude in stl container
// {
//   T1_iterator Tm(b1);
//   real_t m(abs(*b1));
//   for ( T1_iterator t1 = b1+1; t1 != e1; t1++ )
//      if ( m > abs(*t1) ) { m = abs(*t1); Tm = t1; }
//   return Tm;
// }
// template<typename T1_iterator>
// T1_iterator tpl_Max(T1_iterator b1, T1_iterator e1)
// //! returns iterator to first occurrence of maximum absolute magnitude in stl container
// {
//   T1_iterator Tm(b1);
//   real_t m(abs(*b1));
//   for ( T1_iterator t1 = b1+1; t1 != e1; t1++ )
//      if ( m < abs(*t1) ) { m = abs(*t1); Tm = t1; }
//   return Tm;
// }

//! if p0 == 0 this is Horner algorithm:
//!         a[n] + x * (a[n-1] + x * (a[n-2] +...+ x * (a[0])...))
//! else
//! compute a[n] + x * (a[n-1] + x * (a[n-2] +...+ x * (a[0] + x * p0)...))
template<typename scalar1, typename T1_iterator, typename scalar2>
scalar2 hornerAlgorithmTpl(scalar1 x, T1_iterator b, T1_iterator e, scalar2 p0)
{
  scalar2 p = p0;
  for ( T1_iterator t = b; t != e; t++ ) { p = x * p + *t; }
  return p;
}


//! Scaled vector R[i] = s * ( T1[i] - T2 [i] )
template<typename scalar1, typename T1_iterator, typename R_iterator>
void  scaledVectorTpl(const scalar1& s, T1_iterator b1, T1_iterator e1, T1_iterator b2, R_iterator Rb)
{
  // vectors should have same size or at least t2 should not be shorter than t1
  // ( ** THIS IS NOT CHECKED ** )
  T1_iterator t2(b2);
  R_iterator Rit(Rb);
  for ( T1_iterator t1 = b1; t1 != e1; t1++, t2++, Rit++ ) { *Rit = s * (*t1 - *t2); }
}

// template<typename scalar1, typename T1_iterator>
// void  tpl_normalize(T1_iterator b1, T1_iterator e1, scalar1 zer0)
// //! Normalize vector: R[i] = R[i] / || TR ||
// {
//   scalar1 norm = 1./sqrt(inner_product(b1, e1, b1, zer0));
//   for ( T1_iterator t1 = b1; t1 != e1; t1++) *t1 *= norm;
// }

// template<typename T1_iterator, typename T2_iterator>
// complex_t hermitian_inner_product(T1_iterator b1, T1_iterator e1, T2_iterator b2, complex_t dp)
// //! hermitian product of vectors
// {
//   // vectors should have same size or at least t2 should not be shorter than t1
//   // ( ** THIS IS NOT CHECKED ** )
//   T2_iterator t2(b2);
//   for ( T1_iterator t1 = b1; t1 != e1; t1++, t2++ ) dp += *t1 * conj(*t2);
//   return dp;
// }

// template<typename M_iterator, typename Viterator, typename R_iterator>
// void tpl_maTvec(M_iterator M_b, Viterator b1, Viterator e1,
//                 R_iterator b2, R_iterator e2)
// {
//   // Matrix x Vector multiplication: R = M * V
//   // M must be a [e2-b2 rows, e1-b1 columns] matrix
//   // (or at least have e2-b2 * e1-b1 entries)
//   // ( ** THIS IS NOT CHECKED ** )

//   // iterator on operand matrix current entry
//   M_iterator m = M_b;
//   // loop on result vector entries
//   for ( R_iterator t2 = b2; t2 != e2; t2++, m += e1-b1 )
//   { *t2 = 0.; *t2 = inner_product(b1, e1, m, *t2); }
// }

// template<typename A_iterator, typename B_iterator, typename R_iterator>
// void tpl_maTmat(A_iterator M_a, const number_t nbk, B_iterator M_b,
//                 const unsigned nbr, const unsigned nbc, R_iterator M_r)
// {
//   // Matrix x Matrix multiplication: R = A * B
//   // R must be a nbr x nbc ZERO matrix,
//   // A must be a nbr x nbk matrix, B a nbk x nbc matrix
//   // ( ** THIS IS NOT CHECKED ** )

//   // iterator on result matrix current entry
//   R_iterator r_rc = M_r;
//   // iterator on r-th row A first entry A[nbr x nbk]
//   A_iterator a_r  = M_a;
//   // iterator on B first entry
//   B_iterator bb   = M_b;
//   // loop on result matrix rows
//   for ( number_t r = 0; r < nbr; a_r += nbk, r++)
//   {
//      // iterator on B c-th column first entry
//      B_iterator b_c = bb;
//      // loop on result matrix columns
//      for ( number_t c = 0; c < nbc; r_rc++, b_c++, c++)
//      {
//         // iterator to r-th row entries of A and c-th column entries of B
//         A_iterator a_rk = a_r;
//         B_iterator b_kc = b_c;
//         // advance on A r-th row entries and on B c-th column entries
//         for ( number_t k = 0; k < nbk; a_rk++, b_kc += nbk, k++ )
//            *r_rc += *a_rk * *b_kc;
//      }
//   }
// }

//! return in R ranks of V components in U vector, not assuming that U and V are sorted
//!     index starts from 1
//! expansive if U and V are large
template<typename T, typename N>
void ranks(const std::vector<T>& U, const std::vector<T>& V, std::vector<N>&R)
{
  R.resize(V.size());
  typename std::vector<T>::const_iterator itv=V.begin(), itu;
  typename std::vector<T>::iterator itr=R.begin();
  for (;itv!=V.end(); ++itv, ++itr)
  {
    *itr=0;
    T t=*itv;
    N k=1;
    itu=U.begin();
    bool notfound=true;
    while (itu!=U.end() && notfound)
    {
      if (*itu == t) {notfound=false;*itr=k;}
      ++itu;++k;
    }
  }
}

/*! return in R ranks of V components in U container, not assuming that U and V are sorted (index starts from 1)
    expansive if U and V are large
    itu_b, itu_e: begin and end iterator for container U
    itv_b, itv_e: begin and end iterator for container V
    itr_b: begin iterator for container R (R has to be sized to V size before)

    itN must support conversion from Number
*/
template<typename itT, typename itN>
void ranks(itT itu_b, itT itu_e, itT itv_b, itT itv_e, itN itr_b)
{
  itT itv=itv_b, itu;
  itN itr=itr_b;
  for (;itv!=itv_e; ++itv, ++itr)
  {
    *itr=0;
    number_t k=1;
    itu=itu_b;
    bool notfound=true;
    while (itu!=itu_e && notfound)
    {
      if (*itu == *itv) {notfound=false;*itr=k;}
      ++itu;++k;
    }
  }
}

//! return in R the ranks of V components in U vector, not assuming that U and V are sorted,  N index starts from 1
//! except if V[k] is not found in map then R[k]=0
//! faster than previous ranks function but requires a map
template<typename T, typename N>
void ranks(const std::map<T,N>& M, const std::vector<T>& V, std::vector<N>& R)
{
  R.resize(V.size());
  typename std::vector<T>::const_iterator itv=V.begin();
  typename std::vector<T>::iterator itr=R.begin();
  typename std::map<T,N>::const_iterator itme=M.end(), itm;
  for (;itv!=V.end(); ++itv, ++itr)
  {
    itm=M.find(*itv);
    if (itm==itme) *itr=0;
    else *itr=itm->second;
  }
}

//! resize and shrink a vector to size s
template<typename T>
void shrink(std::vector<T>& u, number_t s)
{
  number_t l=u.size();
  if (l==s) return;
  u.resize(s);
  if (s<l) //shrink to s
  {
    #if __cplusplus >= 201103L    //c++11 enabled
      u.shrink_to_fit();
    #else
      std::vector<T>(u).swap(u);
    #endif
  }
}


// Newton's algorithm (real or complex scalar) xn+1 = xn - f(xn)/f'(xn)
//  guess value x0 is modified
//  return status s:
//     s > 0 : convergence s = the number of iterations
//     s < 0 : no convergence s = - best f
//     s = theRealMax: Newton's stop |f'(xn)| too close to 0
template <typename T>
real_t newton(T(*f)(const T&), T(*fp)(const T&), T& x0, number_t niter = 100, real_t tol = theTolerance)
{
  T x=x0, bestx=x0;
  T fx=f(x), fpx=fp(x);
  real_t e = std::abs(fx), bestf=e;
  number_t n=0;
  while (e>tol && n<niter)
  {
    x-=fx/fpx;
    fx=f(x); fpx=fp(x);
    e = std::abs(fx);
    if (std::abs(fpx)<theTolerance) return theRealMax;
    if (e<bestf) {bestf=e;x0=x;}  // update root
    n++;
  }
  if(e>tol) return -bestf;
  return real_t(n);
}

} //end of namespace xlifepp

#endif /* ALGORITHMS_HPP */
