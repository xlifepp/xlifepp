/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file EigenInterface.cpp
  \author E. Lunéville
  \since 20 dec 2016
  \date  20 dec 2016

  \brief Interface to Eigen library

  This interface provides some tools to address Eigen functionalities such as SVD, QR, ...
*/

#include "EigenInterface.hpp"

namespace xlifepp
{
/*! general template QR using Eigen, assuming A, Q, R  are pointers to first value of dense row matrix
    A: pointer to dense row matrix
    m,n: number of rows and cols of A
    Q, R: pointer to QR factors as dense row matrices, has to be allocated before
*/

//qr real specialization
template <>
void qr(const real_t* A, number_t m, number_t n, real_t* Q, real_t* R)
{
  #ifdef XLIFEPP_WITH_EIGEN
    typedef typename Eigen::Matrix< real_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    Eigen::Map<EigenMatrix> eA(const_cast<real_t*>(A), m, n);   // map Matrix pointer to Eigen matrix
    Eigen::HouseholderQR<EigenMatrix> QR(eA);                   // QR factorization of eA, size of Q: m x n
    EigenMatrix eQ = QR.householderQ();
    EigenMatrix eR = QR.matrixQR().triangularView<Eigen::Upper>();
    for (number_t i=0; i<m; ++i)
      for (number_t j=0; j<m; ++j, ++Q) *Q=eQ(i,j);
    for (number_t i=0; i<m; ++i)
      for (number_t j=0; j<n; ++j, ++R) *R=eR(i,j);
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

//qr complex specialization
template <>
void qr(const complex_t* A, number_t m, number_t n, complex_t* Q, complex_t* R)
{
  #ifdef XLIFEPP_WITH_EIGEN
    typedef typename Eigen::Matrix< complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    Eigen::Map<EigenMatrix> eA(const_cast<complex_t*>(A), m, n);        // map Matrix pointer to Eigen matrix
    Eigen::HouseholderQR<EigenMatrix> QR(eA);   // QR factorization of eA, size of Q: m x n
    EigenMatrix eQ = QR.householderQ();
    EigenMatrix eR = QR.matrixQR().triangularView<Eigen::Upper>();
    for (number_t i=0; i<m; ++i)
      for (number_t j=0; j<m; ++j, ++Q) *Q=eQ(i,j);
    for (number_t i=0; i<m; ++i)
      for (number_t j=0; j<n; ++j, ++R) *R=eR(i,j);
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

//eigs computation A*X =lambda*X
// A: pointer to a real/complex vector storing a squared matrix (row storage)
// m: size of the matrix (number of rows or columns)
//   Xs: pointer to a row complex vector containing the eigen vectors (i-th row -> i_th eigen vector)
//   ls: pointer to a complex vector containing eigen values
template <>
void eigs(const real_t* A, number_t m, complex_t* Xs, complex_t* ls)
{
  #ifdef XLIFEPP_WITH_EIGEN
    typedef typename Eigen::Matrix< real_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    Eigen::Map<EigenMatrix> eA(const_cast<real_t*>(A), m, m);        // map Matrix pointer to Eigen matrix
    Eigen::EigenSolver<EigenMatrix> es(eA);
    for (number_t i=0; i<m; ++i,++ls )
    {
      *ls = es.eigenvalues()[i];
      for (number_t j=0; j<m; ++j, ++Xs) *Xs=es.eigenvectors()(j,i);
    }
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

template <>
void eigs(const complex_t* A, number_t m, complex_t* Xs, complex_t* ls)
{
  #ifdef XLIFEPP_WITH_EIGEN
    typedef typename Eigen::Matrix< complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    Eigen::Map<EigenMatrix> eA(const_cast<complex_t*>(A), m, m);        // map Matrix pointer to Eigen matrix
    Eigen::ComplexEigenSolver<EigenMatrix> es(eA);
    for (number_t i=0; i<m; ++i,++ls )
    {
      *ls = es.eigenvalues()[i];
      for (number_t j=0; j<m; ++j, ++Xs) *Xs=es.eigenvectors()(j,i);
    }
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

//eigs computation A*X =lambda*B*X
// A: pointer to a real vector storing a squared matrix A (row storage)
// B: pointer to a real vector storing a squared matrix A (row storage)
// m: size of matrix (number of rows or columns)
//   Xs: pointer to a row complex matrix containing the eigen vectors (i-th row -> i_th eigen vector)
//   ls: pointer to a complex vector containing eigen values
template <>
void eigs(const real_t* A, const real_t* B, number_t m, complex_t* Xs, complex_t* ls)
{
  #ifdef XLIFEPP_WITH_EIGEN
    typedef typename Eigen::Matrix< real_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    Eigen::Map<EigenMatrix> eA(const_cast<real_t*>(A), m, m);        // map Matrix pointer A to Eigen matrix eA
    Eigen::Map<EigenMatrix> eB(const_cast<real_t*>(B), m, m);        // map Matrix pointer B to Eigen matrix eB
    Eigen::GeneralizedEigenSolver<EigenMatrix> ges(eA,eB);
    for (number_t i=0; i<m; ++i,++ls )
    {
      *ls = ges.eigenvalues()[i];
      for (number_t j=0; j<m; ++j, ++Xs) *Xs=ges.eigenvectors()(j,i);
    }
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN

}
}
