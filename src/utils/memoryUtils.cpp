/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file memoryUtils.cpp
  \authors E. Lunéville
  \since 07 nov 2013
  \date 07 nov 2013

  \brief Implementation of functions related to memory usage
 */

#include "memoryUtils.hpp"
#include "utils.h"

#ifdef OS_IS_UNIX
  // #include <sys/sysctl.h>
  #include <sys/types.h>
  #include <sys/resource.h>
#endif

#ifdef OS_IS_WIN
  #ifndef _WIN32_WINNT
    #define _WIN32_WINNT 0x0501
  #endif
  #include <windows.h>
  #include <psapi.h>
#endif

namespace xlifepp
{

std::ostream& operator<<(std::ostream& out, const MemoryUnit& mu)
{ out << words("memory unit", mu); return out; }

#ifdef OS_IS_WIN
  real_t Memory::physicalMem(MemoryUnit mu)
  {
    MEMORYSTATUSEX statex;
    statex.dwLength = sizeof(statex);
    GlobalMemoryStatusEx(&statex);
    return byteTo(statex.ullTotalPhys, mu);
  }

  real_t Memory::physicalFreeMem(MemoryUnit mu)
  {
    MEMORYSTATUSEX statex;
    statex.dwLength = sizeof(statex);
    GlobalMemoryStatusEx(&statex);
    return byteTo(statex.ullAvailPhys, mu);
  }

  real_t Memory::virtualMem(MemoryUnit mu)
  {
    MEMORYSTATUSEX statex;
    statex.dwLength = sizeof(statex);
    GlobalMemoryStatusEx(&statex);
    return byteTo(statex.ullTotalVirtual, mu);
  }

  real_t Memory::virtualFreeMem(MemoryUnit mu)
  {
    MEMORYSTATUSEX statex;
    statex.dwLength = sizeof(statex);
    GlobalMemoryStatusEx(&statex);
    return byteTo(statex.ullAvailVirtual, mu);
  }

  real_t Memory::processPhysicalMem(MemoryUnit mu)
  {
    PROCESS_MEMORY_COUNTERS_EX pmc;
    SIZE_T virtualMemUsed, physMemUsed;
    GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)(&pmc), sizeof(pmc));
    return byteTo(pmc.WorkingSetSize, mu);
  }

  real_t Memory::processVirtualMem(MemoryUnit mu)
  {
    PROCESS_MEMORY_COUNTERS_EX pmc;
    SIZE_T virtualMemUsed, physMemUsed;
    GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)(&pmc), sizeof(pmc));
    return byteTo(pmc.PrivateUsage, mu);
  }
#endif

#ifdef OS_IS_UNIX
  real_t Memory::physicalMem(MemoryUnit mu)
  {
  #if defined(_SC_PHYS_PAGES) && defined(_SC_PAGESIZE)
    return byteTo((size_t)sysconf( _SC_PHYS_PAGES ) * (size_t)sysconf( _SC_PAGESIZE ));
  #elif defined(_SC_PHYS_PAGES) && defined(_SC_PAGE_SIZE)
    return byteTo((size_t)sysconf( _SC_PHYS_PAGES ) * (size_t)sysconf( _SC_PAGE_SIZE ));
  #else
    return 0.;
  #endif
  }

  real_t Memory::physicalFreeMem(MemoryUnit mu)
  {
  #if defined(_SC_AVPHYS_PAGES) && defined(_SC_PAGESIZE)
    return byteTo((size_t)sysconf( _SC_AVPHYS_PAGES ) * (size_t)sysconf( _SC_PAGESIZE ));
  #elif defined(_SC_AVPHYS_PAGES) && defined(_SC_PAGE_SIZE)
    return byteTo((size_t)sysconf( _SC_AVPHYS_PAGES ) * (size_t)sysconf( _SC_PAGE_SIZE ));
  #else
    return 0.;
  #endif
  }

  real_t Memory::virtualMem(MemoryUnit mu)
  {
    return 0.;
  }

  real_t Memory::virtualFreeMem(MemoryUnit mu)
  {
  return 0.;
  }

  real_t Memory::processPhysicalMem(MemoryUnit mu)
  {
    rusage usage;
    int who = RUSAGE_SELF;
    int ret = getrusage(who, &usage);
    if (ret == 0) {
    #ifdef __APPLE__
      return byteTo(usage.ru_maxrss, mu);
    #else
      return byteTo(usage.ru_maxrss*1024, mu);
    #endif
    }
    else { return 0.; }
  }

  real_t Memory::processVirtualMem(MemoryUnit mu)
  {
    rusage usage;
    int who = RUSAGE_SELF;
    int ret = getrusage(who, &usage);
    if (ret == 0) {
    #ifdef __APPLE__
      return byteTo(usage.ru_maxrss, mu);
    #else
      return byteTo(usage.ru_maxrss*1024, mu);
    #endif
    }
    else { return 0.; }
  }
#endif

real_t byteTo(number_t mem, MemoryUnit mu)
{
  number_t unitDiv=1024;
  switch(mu)
  {
    case _byte:return mem;
    case _kilobyte: return real_t(mem)/unitDiv;
    case _megabyte: return real_t(mem)/unitDiv/unitDiv;
    case _gigabyte: return real_t(mem)/unitDiv/unitDiv/unitDiv;
    case _terabyte: return real_t(mem)/unitDiv/unitDiv/unitDiv/unitDiv;
    default: error("memunit_not_handled",words("memory unit",mu));
  }
  return mem;
}

} // end of namespace xlifepp
