/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file String.hpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 7 dec 2011
  \date 3 may 2012

  \brief Additionnal functions to xlifepp::string_t typedef

  The xlifepp::string_t is either a std::string (utf-8) or a std::wstring (utf-16)
  defined by a typedef. The choice between std::string or std::wstring
  is governed by the macro variable WIDE_STRING

  New useful capabilities are proposed
    - conversion of a string to anything and anything to string (templated)
    - lowercase, uppercase, capitalize,
    - trimLeading, trimTrailing, trim
    - find string in a vector of strings

  xlifepp::Strings class inheriting from std::vector<string_t> is a simple interface to a vector of strings
 */

#ifndef STRING_HPP
#define STRING_HPP

#include "config.h"
#include "Environment.hpp"
#include "printUtils.hpp"
#include <string>
#include <sstream>
#include <vector>

namespace xlifepp
{

enum Alignment{_centerAlignment,_leftAlignment,_rightAlignment};

//--------------------------------------------------------------------------------
// templated conversion functions T <---> string_t, using sstream capabilities
// T has to define the output/input stringstream operators << and >>
//--------------------------------------------------------------------------------
template<typename T>
string_t tostring(const T& t) //! returns T converted to string
{
  std::ostringstream s;
  if (isTestMode) s.precision(testPrec);
  else s.precision(fullPrec);
  s << t;
  return s.str();
}
template<typename T>
T stringto(const string_t& s) //! returns string converted to T
{
  std::istringstream is(s);
  T t;
  is >> t;
  return t;
}
template<>
inline const void* stringto(const string_t& s) //! returns string converted to T
{return nullptr;}

string_t coefAsString(bool isFirst, const complex_t& a);  //!< string form of a complex coefficient in a linear combination

//--------------------------------------------------------------------------------
// string utilities transformations
string_t lowercase(const string_t&);                                         //!< returns string_t converted to lowercase
string_t uppercase(const string_t&);                                         //!< returns string_t converted to uppercase
string_t capitalize(const string_t&);                                        //!< returns string_t with initial converted to uppercase
string_t trimLeading(const string_t& s, const char* delim = " \t\n\f\r");    //!< trims leading white space from string_t
string_t trimTrailing(const string_t& s, const char* delim = " \t\n\f\r");   //!< trims trailing white space from string_t
string_t trim(const string_t& s, const char* delim = " \t\n\f\r");           //!< trims leading and trailing white space from string_t
string_t delSpace(const string_t&);                                          //!< delete all white space from string_t
string_t& replaceString(string_t& s, const string_t& s1, const string_t& s2);//!< replace string s1 by string s2 in string s
string_t& replaceChar(string_t& s, char c1, char c2);                        //!< replace char c1 by char c2 in string s
string_t& removeChar(string_t& s, char c);                                   //!< remove all char c from string s
string_t booltoWord(bool);                                                   //!< convert bool to string true or false
void blanks(std::string&, int_t n);                                          //!< add to or remove from end of string n blanks(keep always first char)
string_t format(const string_t& s, number_t l, Alignment =_centerAlignment); //!< format string at size s with alignment option

//--------------------------------------------------------------------------------
// sring utilities related to file and path
string_t fileExtension(const string_t& f);                                   //!< return file name extension using last point as delimiter
//! return root file name and file name extension using last point as delimiter
std::pair<string_t, string_t> fileRootExtension(const string_t& f, const std::vector<string_t>& authorizedExtensions=std::vector<string_t>());
//! build filename from rootname with an additionnal suffix, and extension: rootname_suffix.extension
string_t fileNameFromComponents(const string_t& rootname, const string_t& suffix, const string_t& extension);
// build filename from rootname and extension: rootname.extension
inline string_t fileNameFromComponents(const string_t& rootname, const string_t& extension)
{ return fileNameFromComponents(rootname, "", extension); }
string_t basename(const string_t& f);                                        //!< return basename of a file name using last slash and last point as delimiters
string_t basenameWithExtension(const string_t& f);                           //!< return basename of a file name using last slash as delimiter
string_t dirname(const string_t& f);                                         //!< return dirname of a file name using last slash as delimiter
string_t fileWithoutExtension(const string_t& f);                            //!< return file name without extension using last point as delimiter
bool isPathExist(const string_t &path);                                      //!< check if path exists
string_t rightPath(const string_t& path);                                    //!< adapt path to OS format (WIN/LINUX like)
string_t securedPath(const string_t& path);                                  //!< adapt path to OS format and check if path exists

//--------------------------------------------------------------------------------
// various utilities
int_t findString(const string_t, const std::vector<string_t>&); //!< returns position of string in vector<string>


} // end of namespace xlifepp;

#endif /* STRING_HPP */
