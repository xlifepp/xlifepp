/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Kernel.hpp
  \author E. Lunéville
  \since 20 sep 2013
  \date  20 sep 2013

  \brief Definition of the xlifepp::Kernel class

  Class to handle kernels

  xlifepp::Kernel class manage K(x,y) function, it manages
    - a kernel function defined as a Function object
    - a singularity type (_regular,_r,_logr)
    - a singularity order as a real (default is 0)
    - a name
    - a parameters list as a xlifepp::Parameters object to store any additional user's information
      this parameters list is automatically associated to the parameters list of kernel function

  xlifepp::TensorKernel class, inherited from xlifepp::Kernel, manages matrix kernels of the following form
                  sum_mn psi_n(x) Amn phi_m(y)
  such kernel allows to deal with DirichletToNeuman operator. Indeed, the usual DtN term in variational form is
            sum_mn int_Sigma v(x)psi_m(x) Amn int_Gamma u(y)phi_n(y)
  rewritten
            int_Gamma intg_Sigma  v(y) [sum_mn phi_n(y) Amn psi_m(x)] v(x)
  It manages
     - two function pointers (for phy and psi)
     - a 'matrix' describing Amn as a scalar vector
*/


#ifndef KERNEL_HPP
#define KERNEL_HPP


#include "Parameters.hpp"  // Parameters class definitions
#include "Function.hpp"    // Function class definitions
#include "String.hpp"      // String class definitions
#include "VectorEntry.hpp" // generalized vector
#include "config.h"        // typedef, enum, ... definitions

namespace xlifepp
{
//forward class declaration
class OperatorOn;  //experimental


enum KernelType {_generalKernel,_tensorKernel};
enum SingularityType {_notsingular, _r_, _logr,_loglogr};

class TensorKernel;
class KernelExpansion;

/* ---------------------------------------------------------------------------------------
                       Kernel class K(x,y)
   ---------------------------------------------------------------------------------------*/

class Kernel
{
  public:
    Function kernel;               //!< kernel function K(x,y)
    Function gradx;                //!< x derivative grad_x K(x,y)
    Function grady;                //!< y derivative grad_y K(x,y)
    Function gradxy;               //!< x,y derivative grad_x grad_y K(x,y)
    Function ndotgradx;            //!< nx.gradx
    Function ndotgrady;            //!< ny.grady
    Function curlx;                //!< curl_x if available
    Function curly;                //!< curl_y if available
    Function curlxy;               //!< curl_x curl_y if available
    Function tracx;                //!< trac_x if available
    Function tracy;                //!< trac_y if available
    Function tracxy;               //!< trac_x trac_y if available
    Function nxtensornxtimestracy; //!< nxtensornxtimestrac_y
    Function nxtensornxtimesNGr;   //!< nxtensornxtimesNGr
    Function divx;                 //!< div_x if available
    Function divy;                 //!< div_y if available
    Function divxy;                //!< div_x div_y if available
    Function dx1;                  //!< d_x1 if available (useful when matrix kernel)
    Function dx2;                  //!< d_x2 if available (useful when matrix kernel)
    Function dx3;                  //!< d_x3 if available (useful when matrix kernel)
    Kernel* singPart=nullptr;      //!< singular part of kernel
    Kernel* regPart=nullptr;       //!< regular part of kernel

    SingularityType singularType=_notsingular; //!< type of singularity (_notsingular, _r, _logr,_loglogr)
    real_t singularOrder=0.;                   //!< order of singularity
    complex_t singularCoefficient=0.;          //!< coefficient of singularity
    KernelExpansion* expansion=nullptr;        //!< kernel expansion
    SymType symmetry=_noSymmetry;              //!< kernel symmetry :_noSymmetry, _symmetric (K(x,y)=K(y,x)), _skewsymmetric,_adjoint,_skewadjoint
    string_t name="";                          //!< kernel name
    string_t shortname="";                     //!< kernel name
    Parameters userData;                       //!< to store some additional informations
    dimen_t dimPoint=3;                        //!< dimension of point (default=3)

  public :
    //temporary data
    mutable bool checkType_=false; //!< to activate the checking of arguments (see operator ())
    mutable bool conjugate_=false; //!< temporary flag for conjugate operation construction
    mutable bool transpose_=false; //!< temporary flag for transpose operation construction
    mutable bool xpar=false;       //!< true means that point x is consider as a parameter (unused for ordinary function)
    mutable Point xory;            //!< point x or y as parameter of the kernel (unused for ordinary function)

  public:
    bool requireNx=false;  //!< true if user declares that its function requires normal or x-normal vector
    bool requireNy=false;  //!< true if user declares that its function (kernel) requires y-normal vector
    bool requireTx=false;  //!< true if user declares that its function requires tangent or x-tangent vector
    bool requireTy=false;  //!< true if user declares that its function (kernel) requires y-tangent vector
    bool requireElt=false; //!< true if user declares that its function requires element
    bool requireDom=false; //!< true if user declares that its function requires domain
    bool requireDof=false; //!< true if user declares that its function requires dof

  public:
    //constructors
    Kernel() {} //!< basic constructor

    // constructors from Function (kernel)
    Kernel(const Function& ker,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry)   //! constructor from Function object
      : kernel(ker), singularType(st), singularOrder(so), symmetry(sy), dimPoint(ker.dimPoint_)
      { initParameters(ker.params_p()); }

    Kernel(const Function& ker,
           const string_t& na, SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from Function object with name
      : kernel(ker), singularType(st), singularOrder(so), symmetry(sy), dimPoint(ker.dimPoint_) {initParameters(ker.params_p());}

    Kernel(const Function& ker,
           const char* na, SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from Function object with name
      : kernel(ker), singularType(st), singularOrder(so), symmetry(sy), name(na), shortname(na), dimPoint(ker.dimPoint_)
    { initParameters(ker.params_p()); }

    // constructors from fun(Point,Point,Parameters)
    template <typename T>
    Kernel(T(fun)(const Point&, const Point&, Parameters&),
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit scalar kernel function
      : kernel(*fun), singularType(st), singularOrder(so), symmetry(sy)
    { initParameters(); }

    template <typename T>
    Kernel(T(fun)(const Point&, const Point&, Parameters&),dimen_t d,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit scalar kernel function
      : kernel(*fun), singularType(st), singularOrder(so), symmetry(sy), dimPoint(d)
    { initParameters(); }

    template <typename T>
    Kernel(T(fun)(const Point&, const Point&, Parameters&), const string_t& na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit scalar kernel function, with name
      : kernel(*fun), singularType(st), singularOrder(so), symmetry(sy), name(na), shortname(na)
    { initParameters(); }

    template <typename T>
    Kernel(T(fun)(const Point&, const Point&, Parameters&), dimen_t d, const string_t& na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit scalar kernel function, with name
      : kernel(*fun), singularType(st), singularOrder(so), symmetry(sy), name(na), shortname(na), dimPoint(d)
    { initParameters(); }

    template <typename T>
    Kernel(T(fun)(const Point&, const Point&, Parameters&), const char* na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit scalar kernel function, with name
      : kernel(*fun), singularType(st), singularOrder(so), symmetry(sy), name(na), shortname(na)
    { initParameters(); }

    template <typename T>
    Kernel(T(fun)(const Point&, const Point&, Parameters&), dimen_t d, const char* na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit scalar kernel function, with name
      : kernel(*fun), singularType(st), singularOrder(so), symmetry(sy), name(na), shortname(na), dimPoint(d)
    { initParameters(); }

    // constructors from fun(Vector<Point>,Vector<Point>,Parameters)
    template <typename T>
    Kernel(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&),
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit vector kernel function
      : kernel(*fun), singularType(st), singularOrder(so), symmetry(sy)
    { initParameters(); }

    template <typename T>
    Kernel(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&),dimen_t d,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit vector kernel function
      : kernel(*fun), singularType(st), singularOrder(so), symmetry(sy), dimPoint(d)
    { initParameters(); }

    template <typename T>
    Kernel(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), const string_t& na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit vector kernel function, with name
      : kernel(*fun), singularType(st), singularOrder(so), symmetry(sy), name(na), shortname(na)
    { initParameters(); }

    template <typename T>
    Kernel(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), dimen_t d, const string_t& na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit vector kernel function, with name
      : kernel(*fun), singularType(st), singularOrder(so), symmetry(sy), name(na), shortname(na), dimPoint(d)
    { initParameters(); }

    template <typename T>
    Kernel(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), const char* na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit vector kernel function, with name
      : kernel(*fun), singularType(st), singularOrder(so), symmetry(sy), name(na), shortname(na)
    { initParameters(); }

    template <typename T>
    Kernel(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), dimen_t d, const char* na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit vector kernel function, with name
      : kernel(*fun), singularType(st), singularOrder(so), symmetry(sy), name(na), shortname(na), dimPoint(d)
    {initParameters();}

    // constructors from fun(Point,Point,Parameters) and Parameters
    template <typename T>
    Kernel(T(fun)(const Point&, const Point&, Parameters&), const Parameters& pa,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit scalar kernel function and parameters
      : kernel(*fun, const_cast<Parameters&>(pa)), singularType(st),singularOrder(so),expansion(0),symmetry(sy)
    {userData=pa; initParameters(&pa);}

    template <typename T>
    Kernel(T(fun)(const Point&, const Point&, Parameters&), const Parameters& pa, dimen_t d,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit scalar kernel function and parameters
      : kernel(*fun, const_cast<Parameters&>(pa)), singularType(st),singularOrder(so),symmetry(sy), dimPoint(d)
         {userData=pa; initParameters(&pa);}

    template <typename T>
    Kernel(T(fun)(const Point&, const Point&, Parameters&), const Parameters& pa, const string_t& na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit scalar kernel function and parameters, with name
      : kernel(*fun, const_cast<Parameters&>(pa)),singularType(st),singularOrder(so),symmetry(sy),
        name(na),shortname(na)
        {userData=pa; initParameters(&pa);}

    template <typename T>
    Kernel(T(fun)(const Point&, const Point&, Parameters&), const Parameters& pa, dimen_t d, const string_t& na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit scalar kernel function and parameters, with name
      : kernel(*fun, const_cast<Parameters&>(pa)), singularType(st),singularOrder(so),symmetry(sy), name(na),shortname(na),dimPoint(d)
        {userData=pa; initParameters(&pa);}

    template <typename T>
    Kernel(T(fun)(const Point&, const Point&, Parameters&), const Parameters& pa, const char* na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit scalar kernel function and parameters, with name
      : kernel(*fun, const_cast<Parameters&>(pa)),singularType(st),singularOrder(so),symmetry(sy),
        name(na),shortname(na)
        {userData=pa; initParameters(&pa);}

    template <typename T>
    Kernel(T(fun)(const Point&, const Point&, Parameters&), const Parameters& pa, dimen_t d, const char* na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit scalar kernel function and parameters, with name
      : kernel(*fun, const_cast<Parameters&>(pa)),singularType(st),singularOrder(so),symmetry(sy),
        name(na),shortname(na),dimPoint(d)
         {userData=pa; initParameters(&pa);}

    // constructors from fun(Vector<Point>,Vector<Point>,Parameters) and Parameters
    template <typename T>
    Kernel(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), const Parameters& pa,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit vector kernel function and parameters
      : kernel(*fun, const_cast<Parameters&>(pa)),singularType(st),singularOrder(so),symmetry(sy)
         {userData=pa; initParameters(&pa);}

    template <typename T>
    Kernel(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), const Parameters& pa, dimen_t d,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit vector kernel function and parameters
      : kernel(*fun, const_cast<Parameters&>(pa)),singularType(st),singularOrder(so),symmetry(sy)
         {userData=pa; initParameters(&pa);}

    template <typename T>
    Kernel(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), const Parameters& pa, const string_t& na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit vector kernel function and parameters, with name
      : kernel(*fun, const_cast<Parameters&>(pa)),singularType(st),singularOrder(so),symmetry(sy),
        name(na),shortname(na)
         {userData=pa; initParameters(&pa);}

    template <typename T>
    Kernel(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), const Parameters& pa, dimen_t d, const string_t& na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit vector kernel function and parameters, with name
      : kernel(*fun, const_cast<Parameters&>(pa)),singularType(st),singularOrder(so),symmetry(sy),
        name(na),shortname(na)
         {userData=pa; initParameters(&pa);}

    template <typename T>
    Kernel(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), const Parameters& pa, const char* na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit vector kernel function and parameters, with name
      : kernel(*fun, const_cast<Parameters&>(pa)),singularType(st),singularOrder(so),symmetry(sy),
        name(na),shortname(na)
         {userData=pa; initParameters(&pa);}

    template <typename T>
    Kernel(T(fun)(const Vector<Point>&, const Vector<Point>&, Parameters&), const Parameters& pa, dimen_t d, const char* na,
           SingularityType st=_notsingular, real_t so=0, SymType sy=_noSymmetry) //! constructor from explicit vector kernel function and parameters, with name
      : kernel(*fun, const_cast<Parameters&>(pa)),singularType(st),singularOrder(so),symmetry(sy),
        name(na),shortname(na),dimPoint(d)
         {userData=pa; initParameters(&pa);}

    Kernel(const Kernel&);               //!< copy constructor
    Kernel& operator=(const Kernel&);    //!< assign operator
    void copy(const Kernel&);            //!< copy tool used by copy constructor and assign operator
    virtual Kernel* clone() const;       //!< clone current kernel (virtual)
    virtual ~Kernel();                   //!< destructor

    void updateParametersLinks();        //!< update Parameters links
    void updateDimPoint();               //!< update dimPoint_ of related functions
    virtual void initParameters(const Parameters* pars = 0)   //! initialization of Parameters
    {
      if (pars!=nullptr) userData.push(*pars);                  //the add user parameters
      updateParametersLinks();                           //update Parameters links
      updateDimPoint();
    }

    virtual KernelType type() const                 //! return kernel type (_generalKernel, _tensorKernel, ...)
    {return _generalKernel;}

    //set and get Kernel parameters
    void setParameters(const Parameters& par)       //! add a parameter's list
    {userData=par;}
    void setParameter(Parameter& par)               //! add a parameter
    {userData.push(par);}
    template<typename T>
    void setParameter(const string_t& name, T value)//! add a T value in parameter's list
    {userData.add(name,value);}
    template<typename T>
    T getParameter(const string_t& name, T value) const  //! get parameter by its name
    {return userData.get(name,value);}
    void setX(const Point& P) const                //! specify x=P as a parameter to interpret kernel as a y function (see Function class)
    {xpar=true; xory=P; kernel.setX(P);}
    void setY(const Point& P) const                //! specify y=P as a parameter to interpret kernel as a x function (see Function class)
    {xpar=false; xory=P; kernel.setY(P);}
    Parameter& operator()(const string_t& name)
    {return userData(name);}

    //Various utilities
    virtual bool isSymbolic() const                    //! return true is there is no explicit kernel function
    {return kernel.isVoidFunction();}
    virtual ValueType valueType() const                //! return kernel value type (_real or _complex)
    {return kernel.valueType();}
    virtual StrucType strucType() const                //! return kernel structure type (_scalar, _vector or _matrix)
    {return kernel.strucType();}
    virtual dimPair dims() const                       //! return the dimension of returned values
    {return kernel.dims();}
    bool no_ndotgradx() const                          //! true if ndotgradx is well defined
    {return ndotgradx.isVoidFunction();}
    bool no_ndotgrady() const                          //! true if ndotgrady is well defined
    {return ndotgrady.isVoidFunction();}

    //normal requirements
    bool normalRequired() const                       //! true if normal required
      {return requireNx;}
    bool xnormalRequired() const                      //! true if x-normal required
      {return requireNx;}
    bool ynormalRequired() const                      //! true if y-normal required
      {return requireNy;}
    bool tangentRequired() const                       //! true if tangent required
      {return requireTx;}
    bool xtangentRequired() const                      //! true if x-tangent required
      {return requireTx;}
    bool ytangentRequired() const                      //! true if y-tangent required
      {return requireTy;}
    void require(UnitaryVector uv, bool tf=true)
    {switch(uv)
        {
          case _n:
          case _nx: requireNx=tf; return;
          case _ny: requireNy=tf; return;
          case _tau:
          case _taux: requireTx=tf; return;
          case _tauy: requireTy=tf; return;
          default:
            where("Kernel::require(UnitaryVector)");
            error("unitaryvector_not_handled",words("normal",uv));
        }
    }

    //all requirements
    void require(const string_t& s, bool tf=true)
    {
         if(s=="_n" || s=="_nx") {requireNx=tf; return;}
         if(s=="_ny") {requireNy=tf; return;}
         if(s=="_tau" || s=="_taux") {requireTx=tf; return;}
         if(s=="_tauy") {requireTy=tf; return;}
         if(s=="element") {requireElt=tf; return;}
         if(s=="dof") {requireDof=tf; return;}
         if(s=="domain") {requireDom=tf; return;}
         where("Kernel::require(String)");
         error("data_not_handled",s);
    }

    //virtual downcast methods
    virtual const TensorKernel* tensorKernel() const   //! downcast to tensorkernel
    { error("forbidden","Kernel::tensorKernel()"); return nullptr; }
    virtual TensorKernel* tensorKernel()               //! downcast to tensorkernel
    { error("forbidden","Kernel::tensorKernel()"); return nullptr; }

    const Function& operator()(const Point& x, VariableName vn) const   //! specify k(x,y) as y->k(x,y)
    { kernel.setX(x); return kernel; }
    const Function& operator()(VariableName vn, const Point& y) const   //! specify k(x,y) as x->k(x,y)
    { kernel.setY(y); return kernel; }
    const Function& operator()(VariableName) const;                     //!< specify k(x,y) as x->k(x,y) or y->k(x,y)

    //compute Kernel values
    template<typename T>
    T& operator()(const Point&, const Point&, T&) const;  //!< compute kernel value at (x,y)
    template<typename T>
    T& operator()(const Point&, T&) const;                //!< compute kernel value at (x,P) or (P,X) with P=xory
    template<typename T>
    Vector<T>& operator()(const Vector<Point>&,const Vector<Point>&, Vector<T>&) const; //!< compute kernel value at a list of points
    OperatorOn& operator()(VariableName, VariableName) const;   //experimental, to deal with k(_x,_y)

    //! print utilities
    virtual void print(std::ostream&) const;
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

std::ostream& operator<<(std::ostream&, const Kernel&); //!< print operator

//compute Kernel values
template<typename T>
T& Kernel::operator()(const Point& x,const Point& y, T& res) const  //! compute kernel value at (x,y)
{
  return kernel(x,y,res);
}

template<typename T>
T& Kernel::operator()(const Point& x, T& res) const  //! compute kernel value at (x,.) or (.,x)  regarding xory flag
{
  if(xpar) return kernel(xory,x,res);
  else return kernel(x,xory,res);
}

template<typename T>
Vector<T>& Kernel::operator()(const Vector<Point>& x,const Vector<Point>& y, Vector<T>& res) const //! compute kernel value at a list of points
{
  return kernel(x,y,res);
}

/*!
  \class KernelExpansion
  Kernel expansion according to singularity exponents:
  - \f$\displaystyle G(x,y)         = \sum_i F_i(x,y) / |x-y|^{{\alpha_0}_i}\f$
  - \f$\displaystyle grad_x G(x,y)  = \sum_i GxF_i(x,y).(x-y) / |x-y|^{{\alpha_x}_i}\f$
  - \f$\displaystyle grad_y G(x,y)  = \sum_i GyF_i(x,y).(x-y) / |x-y|^{{\alpha_y}_i}\f$
  - \f$\displaystyle grad_{xy} G(x,y) = \sum_i GxyF_i(x,y).(x-y)(x-y)^T / |x-y|^{{\alpha_{xy}}_i} + \sum_i {GxyF_{diag}}_i(x,y) / |x-y|^{{{\alpha_{xy}}_{diag}}_i} *Identity\f$

  with the convention \f$1/|x-y|^{{\alpha_x}_i} = Log(|x-y|)\f$ when \f${\alpha_x}_i = -1\f$
*/
class KernelExpansion
{
  public:
    number_t nbterms;           //!< number of terms in expansion
    std::vector<int_t> alpha;   //!< singularity exponents
    Function F_i;             //!< coefficients of F expansion
    Function gradxF_i;        //!< coefficients of gradx F expansion
    Function gradyF_i;        //!< coefficients of grady F expansion
    Function gradxyF_i;       //!< coefficients of gradxy F expansion
};

} // end of namespace xlifepp

#endif // KERNEL_HPP

