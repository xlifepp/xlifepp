/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Matrix.cpp
  \authors D. Martin , E. Lunéville
  \since 30 jan 2005
  \date 3 may 2012

  \brief Implementation of xlifepp::Matrix class members and related functions
*/

#include "Matrix.hpp"
#include "Vector.hpp"
#include <fstream>

#ifdef XLIFEPP_WITH_EIGEN
  #include "Dense"
#endif

namespace xlifepp
{

std::ostream& operator<<(std::ostream& out, const SpecialMatrix& sm)
{ out << words("matrix", sm); return out; }

//==============================================================================
// specialized extern functions with cast real to complex
//==============================================================================

Matrix<complex_t> adjoint(const Matrix<complex_t>& cB) //! adjoint of a complex matrix
{
  dimen_t nbr(cB.vsize());
  dimen_t nbc(cB.hsize());
  Matrix<complex_t> r(nbc, nbr, complex_t(0.));
  adjoint(nbr, nbc, cB.begin(), r.begin());
  return r;
}

Matrix<complex_t> adj(const Matrix<complex_t>& cB) //! adjoint of a complex matrix
{
  return adjoint(cB);
}

Matrix<real_t> realPart(const Matrix<complex_t>& cB) //! real part of a complex matrix
{
  Matrix<real_t> r(cB.vsize(), cB.hsize(), real_t(0.));
  std::vector<complex_t>::const_iterator it_cB = cB.begin();
  std::vector<real_t>::iterator it_r;
  for (it_r = r.begin(); it_r < r.end(); it_r++, it_cB++) *it_r = (*it_cB).real();
  return r;
}

Matrix<real_t> imagPart(const Matrix<complex_t>& cB) //! imaginary part of a complex matrix
{
  Matrix<real_t> r(cB.vsize(), cB.hsize(), real_t(0.));
  std::vector<complex_t>::const_iterator it_cB = cB.begin();
  std::vector<real_t>::iterator it_r;
  for (it_r = r.begin(); it_r < r.end(); it_r++, it_cB++ ) *it_r = (*it_cB).imag();
  return r;
}

Matrix<real_t> realPart(const Matrix<real_t>& cB) //! real part of a real matrix
{return cB;}

Matrix<real_t> imagPart(const Matrix<real_t>& cB) //! imag part of a real matrix
{return 0 * cB;}

//for compatibilty
//Matrix<real_t> real(const Matrix<complex_t>& cB) //! real part of a complex matrix
//{return realPart(cB);}
//
//Matrix<real_t> imag(const Matrix<complex_t>& cB) //! imag part of a complex matrix
//{return imagPart(cB);}
//
//Matrix<real_t> real(const Matrix<real_t>& cB)    //! real part of a real matrix
//{return cB;}
//
//Matrix<real_t> imag(const Matrix<real_t>& cB)    //! imag part of a real matrix
//{return 0 * cB;}



Matrix<real_t> adjoint(const Matrix<real_t>& rB) //! adjoint of a real matrix (transpose)
{
  return transpose(rB);
}

Matrix<real_t> adj(const Matrix<real_t>& rB) //! adjoint of a real matrix (transpose)
{
  return transpose(rB);
}

Matrix<real_t> conj(const Matrix<real_t>& cB) //! conjugate a real matrix (do nothing)
{
  return Matrix<real_t>(cB);
}

Matrix<complex_t> cmplx(const Matrix<real_t>& rB) //! cast a real matrix to a complex matrix
{
  Matrix<complex_t> r(rB.vsize(), rB.hsize(), complex_t(0.));
  std::vector<real_t>::const_iterator it_rB = rB.begin();
  std::vector<complex_t>::iterator it_r;

  for (it_r = r.begin(); it_r < r.end(); it_r++, it_rB++ )
    {
      *it_r = complex_t(*it_rB);
    }

  return r;
}

Matrix<complex_t> conj(const Matrix<complex_t>& cB) //! conjugate a complex matrix
{
  Matrix<complex_t> r(cB.vsize(), cB.hsize(), complex_t(0.));
  std::vector<complex_t>::const_iterator it_cB = cB.begin();
  std::vector<complex_t>::iterator it_r;

  for (it_r = r.begin(); it_r < r.end(); it_r++, it_cB++ )
    {
      *it_r = conj(*it_cB);
    }

  return r;
}

Matrix<complex_t> operator+(const Matrix<real_t>& rA, const Matrix<complex_t>& cB) //! add a complex matrix and a real matrix
{
  if ( !rA.shareDims(cB) )
    {
      rA.mismatchDims("rA+cB", cB.vsize(), cB.hsize());
    }

  Matrix<complex_t> r(cB);
  std::vector<real_t>::const_iterator it_rA = rA.begin();
  std:: vector<complex_t>::iterator it_r;

  for (it_r = r.begin(); it_r < r.end(); it_r++, it_rA++ )
    {
      *it_r += *it_rA;
    }

  return r;
}

Matrix<complex_t> operator+(const Matrix<complex_t>& cA, const Matrix<real_t>& rB) //! add a real matrix and a complex matrix
{
  if ( !cA.shareDims(rB) )
    {
      cA.mismatchDims("cA+rB", rB.vsize(), rB.hsize());
    }

  Matrix<complex_t> r(cA);
  std::vector<real_t>::const_iterator it_rB = rB.begin();
  std::vector<complex_t>::iterator it_r;

  for (it_r = r.begin(); it_r < r.end(); it_r++, it_rB++ )
    {
      *it_r += *it_rB;
    }

  return r;
}

Matrix<complex_t> operator+(const Matrix<real_t>& rA, const complex_t& x) //! add a complex scalar to a real matrix
{
  Matrix<complex_t> r(cmplx(rA));
  std::vector<complex_t>::iterator it;

  for (it = r.begin(); it < r.end(); it++ )
    {
      *it += x;
    }

  return r;
}

Matrix<complex_t> operator+(const complex_t& x, const Matrix<real_t>& rA) //! add a complex scalar to a real matrix
{
  Matrix<complex_t> r(cmplx(rA));
  std::vector<complex_t>::iterator it;

  for (it = r.begin(); it < r.end(); it++ )
    {
      *it += x;
    }

  return r;
}

Matrix<complex_t> operator-(const Matrix<real_t>& rA, const Matrix<complex_t>& cB) //! complex matrix - real matrix
{
  if ( !rA.shareDims(cB) )
    {
      rA.mismatchDims("rA+cB", cB.vsize(), cB.hsize());
    }

  Matrix<complex_t> r(cB);
  std::vector<real_t>::const_iterator it_rA = rA.begin();
  std::vector<complex_t>::iterator it_r;

  for (it_r = r.begin(); it_r < r.end(); it_r++, it_rA++ )
    {
      *it_r = *it_rA - *it_r;
    }

  return r;
}

Matrix<complex_t> operator-(const Matrix<complex_t>& cA, const Matrix<real_t>& rB) //! real matrix - complex matrix
{
  if ( !cA.shareDims(rB) )
    {
      cA.mismatchDims("cA+rB", rB.vsize(), rB.hsize());
    }

  Matrix<complex_t> r(cA);
  std::vector<real_t>::const_iterator it_rB = rB.begin();
  std::vector<complex_t>::iterator it_r;

  for (it_r = r.begin(); it_r < r.end(); it_r++, it_rB++ )
    {
      *it_r -= *it_rB;
    }

  return r;
}

Matrix<complex_t> operator-(const Matrix<real_t>& rA, const complex_t& x) //! real matrix - complex value
{
  Matrix<complex_t> r(cmplx(rA));
  std::vector<complex_t>::iterator it;

  for (it = r.begin(); it < r.end(); it++ )
    {
      *it -= x;
    }

  return r;
}

Matrix<complex_t> operator-(const complex_t& x, const Matrix<real_t>& rA) //! complex value - real matrix
{
  Matrix<complex_t> r(cmplx(rA));
  std::vector<complex_t>::iterator it;

  for (it = r.begin(); it < r.end(); it++ )
    {
      *it = x - *it;
    }

  return r;
}

Matrix<complex_t> operator*(const Matrix<real_t>& rA, const complex_t& x) //! multiply real matrix by a complex value
{
  Matrix<complex_t> r(cmplx(rA));
  std::vector<complex_t>::iterator it;

  for (it = r.begin(); it < r.end(); it++ )
    {
      *it *= x;
    }

  return r;
}


Matrix<complex_t> operator*(const complex_t& x, const Matrix<real_t>& rA) //! multiply real matrix by a complex value
{
  Matrix<complex_t> r(cmplx(rA));
  std::vector<complex_t>::iterator it;

  for (it = r.begin(); it < r.end(); it++ )
    {
      *it *= x;
    }

  return r;
}

Matrix<complex_t> operator/(const Matrix<real_t>& rA, const complex_t& x) //! divide real matrix by a complex value
{
  if ( std::abs(x) < theEpsilon)
    {
      rA.divideByZero("rA/=x");
    }

  return (1. / x) * rA;
}

Matrix<complex_t> operator*(const Matrix<real_t>& rA, const Matrix<complex_t>& cB) //! multiply real matrix by a complex matrix
{
  dimen_t nbr = static_cast<dimen_t>(rA.vsize());
  dimen_t nbc = static_cast<dimen_t>(cB.hsize());
  dimen_t nbk = static_cast<dimen_t>(rA.hsize());

  if ( nbk != cB.vsize() )
    {
      rA.mismatchDims("rA*cB", cB.vsize(), nbc);
    }

  Matrix<complex_t> r(nbr, nbc, 0);
  matmat(rA.begin(), nbk, cB.begin(), nbr, nbc, r.begin() );
  return r;
}

Matrix<complex_t> operator*(const Matrix<complex_t>& cA, const Matrix<real_t>& rB) //! multiply complex matrix by a real matrix
{
  dimen_t nbr = static_cast<dimen_t>(cA.vsize());
  dimen_t nbc = static_cast<dimen_t>(rB.hsize());
  dimen_t nbk = static_cast<dimen_t>(cA.hsize());

  if ( nbk != rB.vsize() )
    {
      cA.mismatchDims("cA*rB", rB.vsize(), nbc);
    }

  Matrix<complex_t> r(nbr, nbc, 0);
  matmat(cA.begin(), nbk, rB.begin(), nbr, nbc, r.begin() );
  return r;
}

Vector<complex_t> operator*(const Matrix<real_t>& rA, const Vector<complex_t>& cV) //! real matrix * complex vector
{
  if ( rA.hsize() != cV.size() )
    {
      rA.mismatchDims("rA*cV", cV.size(), 1);
    }

  Vector<complex_t> r(rA.vsize());
  matvec(rA.begin(), cV.begin(), cV.end(), r.begin(), r.end() );
  return r;
}

Vector<complex_t> operator*(const Matrix<complex_t>& cA, const Vector<real_t>& rV) //! complex matrix * real vector
{
  if ( cA.hsize() != rV.size() )
    {
      cA.mismatchDims("cA*rV", rV.size(), 1);
    }

  Vector<complex_t> r(cA.vsize());
  matvec(cA.begin(), rV.begin(), rV.end(), r.begin(), r.end() );
  return r;
}

Vector<complex_t> operator*(const Vector<complex_t>& cV, const Matrix<real_t>& rA) //! complex vector * real matrix
{
  if ( rA.vsize() != cV.size() )
    {
      rA.mismatchDims("rA*cV", cV.size(), 1);
    }

  Vector<complex_t> r(rA.hsize());
  vecmat(rA.begin(), cV.begin(), cV.end(), r.begin(), r.end() );
  return r;
}

Vector<complex_t> operator*(const Vector<real_t>& rV, const Matrix<complex_t>& cA) //! real vector * complex matrix
{
  if ( cA.vsize() != rV.size() )
    {
      cA.mismatchDims("cA*rV", rV.size(), 1);
    }

  Vector<complex_t> r(cA.hsize());
  vecmat(cA.begin(), rV.begin(), rV.end(), r.begin(), r.end() );
  return r;
}

//------------------------------------------------------------------------------
// I/O utilities
//------------------------------------------------------------------------------

std::ostream& operator<<(std::ostream& os, const Matrix<real_t>& m) //! real matrix flux insertion (write)
{
  if (m.numberOfRows() == 0) // empty matrix
    {
      os << "[ ]";
      return os;
    }
  dimen_t rows=m.numberOfRows(), cols=m.numberOfColumns();
  std::vector<real_t>::const_iterator it_m = m.begin();
  os << "[";
  for ( dimen_t r = 0; r < rows; r++ )
    {
      for ( dimen_t c = 0; c < cols; c++, it_m++ )
          os << " " << std::setw(entryWidth) << std::setprecision(entryPrec) << *it_m;
      if ( rows - r > 1 ) os << eol;
    }
  os << " ]";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Matrix<complex_t>& m) //! complex matrix flux insertion (write)
{
  number_t prec=entryPrec;
  if (isTestMode) { prec=testPrec; }
  if (m.numberOfRows() == 0) // empty matrix
    {
      os << "[ ]";
      return os;
    }
  dimen_t rows=m.numberOfRows(), cols=m.numberOfColumns();
  std::vector<complex_t>::const_iterator it_m = m.begin();
  os << "[";
  for ( dimen_t r = 0; r < rows; r++ )
  {
    for ( dimen_t c = 0; c < cols; c++, it_m++)
        os << " " << std::setw(2 * entryWidth + 1) << std::setprecision(prec) << *it_m;
    if ( rows - r > 1 )  os << eol;
  }
  os << " ]";
  return os;
}

//@{
//! specialisation of valueType
template<>
ValueType Matrix<complex_t>::valueType() const
{return _complex;}
template<>
ValueType Matrix<Matrix<complex_t> >::valueType() const
{return _complex;}
//@}

//@{
//! specialisation of strucType
template<>
StrucType Matrix<Matrix<real_t> >::strucType() const
{return _matrix;}
template<>
StrucType Matrix<Matrix<complex_t> >::strucType() const
{return _matrix;}
//@}

//@{
//! specialisation of elementSize
template<>
std::pair<number_t,number_t> Matrix<Matrix<real_t> >::elementSize() const
{
  if(size()==0) return std::make_pair(0,0);  //void matrix
  const Matrix<real_t>& m=at(0);
  return std::make_pair(m.numberOfRows(),m.numberOfColumns());
}
template<>
std::pair<number_t,number_t> Matrix<Matrix<complex_t> >::elementSize() const
{
  if(size()==0) return std::make_pair(0,0);  //void matrix
  const Matrix<complex_t>& m=at(0);
  return std::make_pair(m.numberOfRows(),m.numberOfColumns());
}
//@}

//@{
//! specialization of norm2
real_t norm2(const Matrix<real_t>& m)
{
  real_t n=0.;
  std::vector<real_t>::const_iterator it=m.begin();
  for(; it!=m.end(); it++) n += *it * *it;
  return std::sqrt(n);
}
real_t norm2(const Matrix<complex_t>& m)
{
  real_t n=0.;
  std::vector<complex_t>::const_iterator it=m.begin();
  for(; it!=m.end(); it++) n += std::abs(*it)*std::abs(*it);
  return std::sqrt(n);
}

real_t norm2(const Matrix<Matrix<real_t> >& m)
{
  real_t n=0.;
  std::vector<Matrix<real_t> >::const_iterator it=m.begin();
  for(; it!=m.end(); it++)
    {
      real_t nit=norm2(*it);
      n += nit*nit;
    }
  return std::sqrt(n);
}

real_t norm2(const Matrix<Matrix<complex_t> >& m)
{
  real_t n=0.;
  std::vector<Matrix<complex_t> >::const_iterator it=m.begin();
  for(; it!=m.end(); it++)
    {
      real_t nit=norm2(*it);
      n += nit*nit;
    }
  return std::sqrt(n);
}
//@}

//@{
//! specialization of infinite norm
real_t norminfty(const Matrix<real_t>& m)
{
  real_t n=0.;
  std::vector<real_t>::const_iterator it=m.begin();
  for(; it!=m.end(); it++) n =std::max(n,std::abs(*it));
  return n;
}
real_t norminfty(const Matrix<complex_t>& m)
{
  real_t n=0.;
  std::vector<complex_t>::const_iterator it=m.begin();
  for(; it!=m.end(); it++) n =std::max(n,std::abs(*it));
  return n;
}

real_t norminfty(const Matrix<Matrix<real_t> >& m)
{
  real_t n=0.;
  std::vector<Matrix<real_t> >::const_iterator it=m.begin();
  for(; it!=m.end(); it++)
    {
      real_t nit=norminfty(*it);
      n =std::max(n,nit);
    }
  return n;
}

real_t norminfty(const Matrix<Matrix<complex_t> >& m)
{
  real_t n=0.;
  std::vector<Matrix<complex_t> >::const_iterator it=m.begin();
  for(; it!=m.end(); it++)
    {
      real_t nit=norminfty(*it);
      n =std::max(n,nit);
    }
  return n;
}
//@}

} // end of namespace xlifepp
