/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file printUtils.hpp
	\authors D. Martin, N. Kielbasiewicz, E. Lunéville
	\since 04 may 2005
	\date 08 feb 2016

	\brief stl-like template print functions header file
*/

#ifndef PRINT_UTILS_HPP
#define PRINT_UTILS_HPP

//#include "String.hpp"
#include <iostream>

namespace xlifepp
{

/*!
	prints container of integer type on opened ostream with a constant number of
	entries per row; requires "operator<<"
	this is a stl-like utility: iterators are either stl::iterators or C-pointers
 */
template<typename Iterator>
void printRowWise(std::ostream& os, const string_t& title, number_t perRow,
                  number_t width, Iterator it1, Iterator it2)
{
  string_t s =title;
  if (s.empty()) s = "   ";
  os << s ;
  number_t pl=0;
  for (Iterator it = it1; it != it2; it++) pl++;
  if(pl > perRow) os<<eol;
  pl=0;
  for (Iterator it = it1; it != it2; it++, pl++)
  {
    if (pl >= perRow)
    {
      os << " ..." << eol << std::setw(s.size()) << "...";
      pl = 0;
    }
    os << std::setw(width) << *it;
  }
}

/*!
	prints container of real or complex type on opened ostream with a constant number of
	entries per row; requires "operator<<"
	this is a stl-like utility: iterators are either stl::iterators or C-pointers
 */
template<typename Iterator>
void printRowWise(std::ostream& os, const string_t& title, number_t perRow,
                  number_t width, number_t prec, Iterator it1, Iterator it2)
{
  string_t s =title;
  if (s.empty()) s = "   ";
  os << s ;
  number_t pl=0;
  for (Iterator it = it1; it != it2; it++) pl++;
  if(pl > perRow) os<<eol;
  pl=0;
  if (isTestMode) { prec=testPrec; }
  for (Iterator it = it1; it != it2; it++, pl++)
  {
    if (pl >= perRow)
    {
      os << " ..." << eol << std::setw(s.size()) << "...";
      pl = 0;
    }
    os << std::setw(width) << std::setprecision(prec) << *it;
  }
}

template<typename K>
void printVector(std::ostream& os, const string_t& s, const std::vector<K>& v)
{
  os << eol << "printVector " << s;
  typename std::vector<K>::const_iterator it;
  for (it = v.begin(); it != v.end(); it ++) { os << eol << *(it); }
}

//--------------------------------------------------------------------------------
// useful "print"
//--------------------------------------------------------------------------------

//! print a pair
template<typename U, typename V>
std::ostream& operator<<(std::ostream& out, const std::pair<U,V>& p)
{
  out << "(" << p.first << "," << p.second << ")";
  return out;
}

template<typename U>
std::ostream& operator<<(std::ostream& os, const std::vector<U>& v)
{
  typename std::vector<U>::const_iterator it;
  os << "[ ";
  for(it = v.begin(); it != v.end(); it++) { os << *it << " "; }
  os << "]";
  return os;
}

//! print a pair with value being a std::vector
template<typename U, typename V>
std::ostream& operator<<(std::ostream& out, const std::pair<U,std::vector<V> >& p)
{
  out << "(" << p.first << ",";
  typename std::vector<V>::const_iterator it=p.second.begin();
  out << "[";
  for(;it!=p.second.end();it++) out << " " << *it;
  out << " ]";
  out << ")";
  return out;
}

//! print a list
template<typename T>
std::ostream& operator<<(std::ostream& out, const std::list<T>& l)
{
  typename std::list<T>::const_iterator it=l.begin();
  out << "{";
  for(;it!=l.end();it++) out << " " << *it;
  out << " }";
  return out;
}

//! print a set
template<typename T>
std::ostream& operator<<(std::ostream& out, const std::set<T>& s)
{
  typename std::set<T>::const_iterator it=s.begin();
  out << "{";
  for(;it!=s.end();it++) out << " " << *it;
  out << " }";
  return out;
}

//! print a map
template<typename K,  typename T>
std::ostream& operator<<(std::ostream& out, const std::map<K,T>& m)
{
  typename std::map<K,T>::const_iterator it=m.begin();
  out << "{";
  for(;it!=m.end();it++) out << " " << *it;
  out << " }";
  return out;
}

//! print a multimap
template<typename K,  typename T>
std::ostream& operator<<(std::ostream& out, const std::multimap<K,T>& m)
{
  typename std::map<K,T>::const_iterator it=m.begin();
  out << "{";
  for(;it!=m.end();it++) out << " " << *it;
  out << " }";
  return out;
}

} // end of namespace xlifepp

#endif /* PRINT_UTILS_HPP */
