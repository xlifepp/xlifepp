/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Vector.cpp
  \authors D. Martin, E. Lunéville
  \since 07 jul 2007
  \date 3 may 2012

  \brief Implementation of xlifepp::Vector class members and related functions
 */

#include "Vector.hpp"
#include "String.hpp"
#include "printUtils.hpp"
#include "config.h"

#include <fstream>

namespace xlifepp
{

//--------------------------------------------------------------------------------
// specialized member functions
//--------------------------------------------------------------------------------

//! specialization vector<real> -> vector<complex>
template<> template<>
Vector<complex_t>::Vector(const std::vector<real_t>& x)
{
  this->resize(x.size());
  Vector<real_t>::cit_vk itx=x.begin();
  for(it_vk it = this->begin(); it < this->end(); it++, itx++) *it = *itx;
}

//! abusive specialization of complex to real (real part is cast)
template<> template<>
Vector<real_t>::Vector(const std::vector<complex_t>& x)
{
  this->resize(x.size());
  Vector<complex_t>::cit_vk itx=x.begin();
  for(it_vk it = this->begin(); it < this->end(); it++, itx++) *it = real(*itx);
}

//! specialization vector<vector<real_t>> -> vector<vector<complex_t>>
template<> template<>
Vector<Vector<complex_t> >::Vector(const std::vector<Vector<real_t> >& x)
{
  this->resize(x.size());
  Vector<Vector<real_t> >::const_iterator itx=x.begin();
  for(it_vk it = this->begin(); it < this->end(); it++, itx++) *it = *itx;
}

//! abusive specialization of complex to real (real part is cast)
template<> template<>
Vector<Vector<real_t> >::Vector(const std::vector<Vector<complex_t> >& x)
{
  this->resize(x.size());
  Vector<Vector<complex_t> >::const_iterator itx=x.begin();
  for(it_vk it = this->begin(); it < this->end(); it++, itx++) *it = real(*itx);
}

//! specialization vector<complex>=vector<real>
template<> template<>
Vector<complex_t>& Vector<complex_t>::operator=(const Vector<real_t>& x)
{
  this->resize(x.size());
  Vector<real_t>::cit_vk itx=x.begin();
  for(it_vk it = this->begin(); it < this->end(); it++, itx++)
    *it = *itx;
  return *this;
}

//! abusive specialization of complex to real (real part is cast)
template<> template<>
Vector<real_t>& Vector<real_t>::operator=(const Vector<complex_t>& x)
{
  this->resize(x.size());
  Vector<complex_t>::cit_vk itx=x.begin();
  for(it_vk it = this->begin(); it < this->end(); it++, itx++) *it = real(*itx);
  return *this;
}

//! forbidden specialization of complex to real
template<> template<>
Vector<real_t>& Vector<real_t>::operator*=(const complex_t& vc)
{
  error("forbidden", "Vector<Real>::operator*=(const Complex&);");
  return *this;
}

template<> template<>
Vector<real_t>& Vector<real_t>::operator/=(const complex_t& vc)
{
  error("forbidden", "Vector<Real>::operator/=(const Complex&);");
  return *this;
}

//! specialization vector<vector<complex>>=vector<vector<real>>
template<> template<>
Vector<Vector<complex_t> >& Vector<Vector<complex_t> >::operator=(const Vector<Vector<real_t> >& x)
{
  this->resize(x.size());
  Vector<Vector<real_t> >::const_iterator itx=x.begin();
  for(it_vk it = this->begin(); it < this->end(); it++, itx++) *it = *itx;
  return *this;
}

//! abusive specialization of complex to real (real part is cast)
template<> template<>
Vector<Vector<real_t> >& Vector<Vector<real_t> >::operator=(const Vector<Vector<complex_t> >& x)
{
  this->resize(x.size());
  Vector<Vector<complex_t> >::const_iterator itx=x.begin();
  for(it_vk it = this->begin(); it < this->end(); it++, itx++) *it = real(*itx);
  return *this;
}

//@{
//! operations are not specialized for complex to real, cast with constructor before: x+Vector(z) or do real(z+x)
template<> template<>
Vector<complex_t>& Vector<complex_t>::operator+=(const Vector<real_t>& x) //! vector addition
{
    if(size() != x.size()) {mismatchSize("Vector<K>+Vector<K>", x.size());}
    Vector<real_t>::const_iterator itx = x.begin();
    Vector<complex_t>::iterator it;
    for(it = begin(); it != end(); it++, itx++) *it += *itx;
    return *this;
}

template<> template<>
Vector<complex_t>& Vector<complex_t>::operator-=(const Vector<real_t>& x) //! vector substraction
{
    if(size() != x.size()) {mismatchSize("Vector<complex_t>+=Vector<real_t>", x.size());}
    Vector<real_t>::const_iterator itx = x.begin();
    Vector<complex_t>::iterator it;
    for(it = begin(); it != end(); it++, itx++) *it -= *itx;
    return *this;
}

template<> template<>
Vector<Vector<complex_t> >& Vector<Vector<complex_t> >::operator+=(const Vector<Vector<real_t> >& x) //! vector addition
{
    if(size() != x.size()) {mismatchSize("Vector<Vector<complex_t>>-=Vector<Vector<real_t>>", x.size());}
    Vector<Vector<real_t> >::const_iterator itx = x.begin();
    Vector<Vector<complex_t> >::iterator it;
    for(it = begin(); it != end(); it++, itx++) *it += *itx;
    return *this;
}

template<> template<>
Vector<Vector<complex_t> >& Vector<Vector<complex_t> >::operator-=(const Vector<Vector<real_t> >& x) //! vector substraction
{
    if(size() != x.size()) {mismatchSize("Vector<Vector<complex_t>>-=Vector<Vector<real_t>>", x.size());}
    Vector<Vector<real_t> >::const_iterator itx = x.begin();
    Vector<Vector<complex_t> >::iterator it;
    for(it = begin(); it != end(); it++, itx++) *it -= *itx;
    return *this;
}
//@}

//--------------------------------------------------------------------------------
// specialized extern functions returning a real vector
//--------------------------------------------------------------------------------
Vector<real_t> real(const Vector<real_t>& a) //! real part of a real vector
{
  return a;
}

Vector<real_t> real(const Vector<complex_t>& a) //! real part of a complex vector
{
  Vector<real_t> r(a.size());
  Vector<complex_t>::const_iterator ita(a.begin());
  Vector<real_t>::iterator itr = r.begin();
  for (; itr < r.end(); itr++, ita++) { *itr = (*ita).real(); }
  return r;
}

Vector<real_t> imag(const Vector<real_t>& a) //! real part of a complex vector
{
  return real_t(0)*a;
}

Vector<real_t> imag(const Vector<complex_t>& a) //! imaginary part of a complex vector
{
  Vector<real_t> r(a.size());
  Vector<complex_t>::const_iterator ita(a.begin());
  Vector<real_t>::iterator itr = r.begin();
  for (; itr < r.end(); itr++, ita++) { *itr = (*ita).imag(); }
  return r;
}

Vector<real_t> abs(const Vector<real_t>& a) //! abs of a real vector
{
  Vector<real_t> r(a.size());
  Vector<real_t>::const_iterator ita(a.begin());
  Vector<real_t>::iterator itr = r.begin();
  for (; itr < r.end(); itr++, ita++) { *itr = std::abs(*ita); }
  return r;
}

Vector<real_t> abs(const Vector<complex_t>& a) //! abs of a complex vector
{
  Vector<real_t> r(a.size());
  Vector<complex_t>::const_iterator ita(a.begin());
  Vector<real_t>::iterator itr = r.begin();
  for (; itr < r.end(); itr++, ita++) { *itr = std::abs(*ita); }
  return r;
}

Vector<Vector<real_t> > real(const Vector<Vector<real_t> >& a)  //! abs of a vector of real vectors
{
  return a;
}

Vector<Vector<real_t> > real(const Vector<Vector<complex_t> >& a) //! abs of a vector of complex vectors
{
  Vector<Vector<real_t> > r(a.size());
  Vector<Vector<complex_t> >::const_iterator ita(a.begin());
  Vector<Vector<real_t> >::iterator itr = r.begin();
  for (; itr < r.end(); itr++, ita++) { *itr = real(*ita); }
  return r;
}

Vector<Vector<real_t> > imag(const Vector<Vector<real_t> >& a)  //! abs of a vector of real vectors
{
  Vector<Vector<real_t> > r(a.size());
  Vector<Vector<real_t> >::const_iterator ita(a.begin());
  Vector<Vector<real_t> >::iterator itr = r.begin();
  for (; itr < r.end(); itr++, ita++) { *itr = imag(*ita); }
  return r;
}

Vector<Vector<real_t> > imag(const Vector<Vector<complex_t> >& a) //! abs of a vector of complex vectors
{
  Vector<Vector<real_t> > r(a.size());
  Vector<Vector<complex_t> >::const_iterator ita(a.begin());
  Vector<Vector<real_t> >::iterator itr = r.begin();
  for (; itr < r.end(); itr++, ita++) { *itr = imag(*ita); }
  return r;
}

Vector<Vector<real_t> > abs(const Vector<Vector<real_t> >& a)  //! abs of a vector of real vectors
{
  Vector<Vector<real_t> > r(a.size());
  Vector<Vector<real_t> >::const_iterator ita(a.begin());
  Vector<Vector<real_t> >::iterator itr = r.begin();
  for (; itr < r.end(); itr++, ita++) { *itr = abs(*ita); }
  return r;
}

Vector<Vector<real_t> > abs(const Vector<Vector<complex_t> >& a) //! abs of a vector of complex vectors
{
  Vector<Vector<real_t> > r(a.size());
  Vector<Vector<complex_t> >::const_iterator ita(a.begin());
  Vector<Vector<real_t> >::iterator itr = r.begin();
  for (; itr < r.end(); itr++, ita++) { *itr = abs(*ita); }
  return r;
}

//--------------------------------------------------------------------------------
// specialized external functions or operators returning a complex vector
//--------------------------------------------------------------------------------
Vector<complex_t> cmplx(const Vector<complex_t>& a) //! cast a complex vector to a complex vector
{
  return Vector<complex_t>(a);
}

Vector<complex_t> cmplx(const Vector<real_t>& a) //! cast a real vector to a complex vector
{
  Vector<complex_t> r(a.size());
  Vector<real_t>::const_iterator ita = a.begin();
  for (Vector<complex_t>::iterator itr = r.begin(); itr != r.end(); itr++, ita++ ) { *itr = complex_t(*ita); }
  return r;
}

Vector<Vector<complex_t> > cmplx(const Vector<Vector<real_t> >& a) //! cast a real vector to a complex vector
{
  Vector<Vector<complex_t> > r(a.size());
  Vector<Vector<real_t> >::const_iterator ita = a.begin();
  for (Vector<Vector<complex_t> >::iterator itr = r.begin(); itr != r.end(); itr++, ita++ ) { *itr = cmplx(*ita); }
  return r;
}

//--------------------------------------------------------------------------------
// specialized external algebraic functions
//--------------------------------------------------------------------------------

Vector<complex_t> operator+(const Vector<real_t>& rA, const Vector<complex_t>& cB) //! vector addition
{
  if ( rA.size() != cB.size() ) { rA.mismatchSize("Vector<real>+Vector<complex>", cB.size()); }
  Vector<complex_t> r(cB);
  Vector<real_t>::const_iterator it_rA(rA.begin());
  for (Vector<complex_t>::iterator it_r = r.begin(); it_r != r.end(); it_r++, it_rA++ ) { *it_r += *it_rA; }
  return r;
}

Vector<complex_t> operator+(const Vector<complex_t>& cA, const Vector<real_t>& rB) //! vector addition
{
  if ( cA.size() != rB.size() ) { cA.mismatchSize("Vector<complex>+Vector<real>", rB.size()); }
  Vector<complex_t> r(cA);
  Vector<real_t>::const_iterator it_rB(rB.begin());
  for (Vector<complex_t>::iterator it_r = r.begin(); it_r != r.end(); it_r++, it_rB++ ) { *it_r += *it_rB; }
  return r;
}

Vector<complex_t> operator+(const Vector<real_t>& rA, const complex_t& x) //! add a scalar to vector
{
  Vector<complex_t> r(cmplx(rA));
  for (Vector<complex_t>::iterator it = r.begin(); it < r.end(); it++ ) { *it += x; }
  return r;
}

Vector<complex_t> operator+(const complex_t& x, const Vector<real_t>& rA) //! add a scalar to vector
{
  Vector<complex_t> r(cmplx(rA));
  for (Vector<complex_t>::iterator it = r.begin(); it < r.end(); it++ ) { *it += x; }
  return r;
}

Vector<complex_t> operator+(const Vector<complex_t>& cA, const real_t& x)  //! add a scalar to vector
{
  Vector<complex_t> r(cA);
  for (Vector<complex_t>::iterator it = r.begin(); it < r.end(); it++ ) { *it += x; }
  return r;
}

Vector<complex_t> operator+(const real_t& x, const Vector<complex_t>& cA)  //! add a scalar to vector
{
  Vector<complex_t> r(cA);
  for (Vector<complex_t>::iterator it = r.begin(); it < r.end(); it++ ) { *it += x; }
  return r;
}

Vector<complex_t> operator-(const Vector<real_t>& rA, const Vector<complex_t>& cB) //! vector subtraction A - B
{
  if ( rA.size() != cB.size() ) { rA.mismatchSize("Vector<real>-Vector<complex>", cB.size()); }
  Vector<complex_t> r(cB);
  Vector<real_t>::const_iterator it_rA(rA.begin());
  for (Vector<complex_t>::iterator it_r = r.begin(); it_r < r.end(); it_r++, it_rA++ ) { *it_r = *it_rA - *it_r; }
  return r;
}

Vector<complex_t> operator-(const Vector<complex_t>& cA, const Vector<real_t>& rB) //! vector subtraction A - B
{
  if ( cA.size() != rB.size() ) { cA.mismatchSize("Vector<complex>-Vector<real>", rB.size()); }
  Vector<complex_t> r(cA);
  Vector<real_t>::const_iterator it_rB(rB.begin());
  for (Vector<complex_t>::iterator it_r = r.begin(); it_r < r.end(); it_r++, it_rB++ ) { *it_r -= *it_rB; }
  return r;
}

Vector<complex_t> operator-(const Vector<real_t>& rA, const complex_t& x) //! subtract scalar from vector A - x
{
  Vector<complex_t> r(cmplx(rA));
  for (Vector<complex_t>::iterator it = r.begin(); it < r.end(); it++ ) { *it -= x; }
  return r;
}

Vector<complex_t> operator-(const complex_t& x, const Vector<real_t>& rA) //! subtract real vector from complex scalar
{
  Vector<complex_t> r(cmplx(rA));
  for (Vector<complex_t>::iterator it = r.begin(); it < r.end(); it++ ) { *it = x - *it; }
  return r;
}

Vector<complex_t> operator-(const Vector<complex_t>& cA, const real_t& x)  //! substract real scalar from complex vector
{
  Vector<complex_t> r(cA);
  for (Vector<complex_t>::iterator it = r.begin(); it < r.end(); it++ ) { *it -= x; }
  return r;
}

Vector<complex_t> operator-(const real_t& x, const Vector<complex_t>& cA) //! substract complex vector from real scalar
{
  Vector<complex_t> r(cA);
  for (Vector<complex_t>::iterator it = r.begin(); it < r.end(); it++) { *it = x - *it; }
  return r;
}

Vector<complex_t> operator*(const Vector<real_t>& rA, const complex_t& x) //! multiply real vector by complex scalar
{
  Vector<complex_t> r(cmplx(rA));
  for (Vector<complex_t>::iterator it = r.begin(); it < r.end(); it++ ) { *it *= x; }
  return r;
}

Vector<complex_t> operator*(const complex_t& x, const Vector<real_t>& rA) //! multiply real vector by complex scalar
{
  Vector<complex_t> r(cmplx(rA));
  for (Vector<complex_t>::iterator it = r.begin(); it < r.end(); it++ ) { *it *= x; }
  return r;
}

Vector<complex_t> operator*(const Vector<complex_t>& cA, const real_t& x) //! multiply complex vector by real scalar
{
  Vector<complex_t> r(cA);
  for (Vector<complex_t>::iterator it = r.begin(); it < r.end(); it++ ) { *it *= x; }
  return r;
}

Vector<complex_t> operator*(const real_t& x, const Vector<complex_t>& cA) //! multiply real vector by complex scalar
{
  Vector<complex_t> r(cA);
  for (Vector<complex_t>::iterator it = r.begin(); it < r.end(); it++ ) { *it *= x; }
  return r;
}

Vector<complex_t> operator/(const Vector<real_t>& rA, const complex_t& x) //! divide real vector by complex scalar
{
  if (std::abs(x) < theEpsilon) { rA.divideByZero("rA/=x"); }
  return (1. / x) * rA;
}

Vector<complex_t> operator/(const Vector<complex_t>& cA, const real_t& x) //! divide complex vector by real scalar
{
  if (std::abs(x) < theEpsilon) { cA.divideByZero("cA/=x"); }
  return (1. / x) * cA;
}

Vector<Vector<complex_t> > operator*(const complex_t& x, const Vector<Vector<complex_t> >& A)
{
  Vector<Vector<complex_t> > r(A);
   for (Vector<Vector<complex_t> >::iterator it = r.begin(); it < r.end(); it++ ) { *it *= x; }
  return r;
}

Vector<Vector<complex_t> > operator*(const Vector<Vector<complex_t> >& A, const complex_t& x)
{
  Vector<Vector<complex_t> > r(A);
  for (Vector<Vector<complex_t> >::iterator it = r.begin(); it < r.end(); it++ ) { *it *= x; }
  return r;
}

Vector<Vector<complex_t> > operator/(const Vector<Vector<complex_t> >& A, const complex_t& x)
{
  if (std::abs(x) < theEpsilon) { A.divideByZero("A/=x"); }
  Vector<Vector<complex_t> > r(A);
  for (Vector<Vector<complex_t> >::iterator it = r.begin(); it < r.end(); it++ ) { *it /= x; }
  return r;
}

/*! Add then assign complex_t to real_t
    In fact, this kind of function is forbidden but we implement it here
    as a trick to overcome the problem of instantiation in Solver
*/
void addVectorThenAssign(Vector<real_t>& v, const Vector<complex_t>& cA)
{
  v.complexCastError("addVectorThenAssign",cA.size());
}

void multScalarThenAssign(Vector<real_t>& v, const complex_t& c)
{
  v.complexCastError("multScalarThenAssign",1);
}

void scaleVector(const complex_t& k, const Vector<real_t>& v, Vector<real_t>& r)
{
  v.complexCastError("scaleVector",1);
}

void scaleVector(const complex_t& k, const Vector<complex_t>& v, Vector<real_t>& r)
{
  v.complexCastError("scaleVector",v.size());
}

void scaleVector(const real_t& k, const Vector<complex_t>& v, Vector<real_t>& r)
{
  v.complexCastError("scaleVector",v.size());
}

/*!
 *  Specialization of dotRC (real_t real_t)
 */
complex_t dotRC(const Vector<real_t>& vecFirst, const Vector<real_t>& vecSecond)
{
  complex_t z(0.);
  real_t zero(0.);

  z = cmplx(std::inner_product((vecFirst).begin(), (vecFirst).end(), (vecSecond).begin(), zero));

  return z;
}

/*!
 *  Specialization of dotC (real_t real_t) and (complex_t real_t)
 */
complex_t dotC(const Vector<real_t>& vecFirst, const Vector<real_t>& vecSecond)
{
  complex_t z(0.);
  real_t zero(0.);

  z = cmplx(std::inner_product((vecFirst).begin(), (vecFirst).end(), (vecSecond).begin(), zero));

  return z;
}

complex_t dotC(const Vector<complex_t>& vecFirst, const Vector<real_t>& vecSecond)
{
  complex_t z(0.);
  complex_t zero(0.);

  z = std::inner_product((vecFirst).begin(), (vecFirst).end(), (vecSecond).begin(), zero);

  return z;
}

/*!
 *  Specialization of dot (complex_t real_t)
 */

complex_t dot(const Vector<complex_t>& u, const Vector<real_t>& v)
{
    complex_t z(0.), zero(0.);
    z = std::inner_product(u.begin(), u.end(), v.begin(), zero);
    return z;
}

complex_t dot(const Vector<real_t>& u, const Vector<complex_t>& v)
{
    return dot(v,u);
}

/*!
 *  Specialization complex_t/real_t of cross product
 */
Vector<complex_t> crossProduct(const Vector<complex_t>& u, const Vector<real_t>& v)
{
  if(u.size()!=3 || v.size()!=3) error("3D_only","crossProduct");
  Vector<complex_t> w(3);
  w[0]=u[1]*v[2]-u[2]*v[1];
  w[1]=u[2]*v[0]-u[0]*v[2];
  w[2]=u[0]*v[1]-u[1]*v[0];
  return w;
}
Vector<complex_t> crossProduct(const Vector<real_t>& u, const Vector<complex_t>& v)
{
  if(u.size()!=3 || v.size()!=3) error("3D_only","crossProduct");
  Vector<complex_t> w(3);
  w[0]=u[1]*v[2]-u[2]*v[1];
  w[1]=u[2]*v[0]-u[0]*v[2];
  w[2]=u[0]*v[1]-u[1]*v[0];
  return w;
}
complex_t crossProduct2D(const Vector<complex_t>& u, const Vector<real_t>& v)
{
  if(u.size()!=2 || v.size()!=2) error("2D_only","crossProduct2D");
  return u[0]*v[1]-u[1]*v[0];
}
complex_t crossProduct2D(const Vector<real_t>& u, const Vector<complex_t>& v)
{
  if(u.size()!=2 || v.size()!=2) error("2D_only","crossProduct2D");
  return u[0]*v[1]-u[1]*v[0];
}

complex_t dot(const Vector<complex_t>& vecFirst, const Vector<complex_t>& vecSecond)
{
  return hermitianInnerProductTpl((vecFirst).begin(), (vecFirst).end(), (vecSecond).begin());
}

//! return Point as a Vector<real_t>
Vector<real_t> toVector(const Point& p)
{ return Vector<real_t>(p.begin(),p.end());}

/*!< returns q-p as VectorVector<real_t> */
Vector<real_t> toVector(const Point &p, const Point& q)
{ number_t ps = p.size();
  if(ps != q.size())  error("diff_pts_size", "Vector::Vector(const Point&, const Point&)", ps, q.size());
  Vector<real_t> qmp(ps,0.);
  for(number_t i = 0; i < ps; i++) qmp[i] = q[i] - p[i];
  return qmp;
}
//-------------------------------------------------------------------------------
// I/O utilities
// template specialization for real_t and complex_t
//-------------------------------------------------------------------------------

//template<>
//std::ostream& operator<<(std::ostream& os, const std::vector<real_t>& v) //! flux insertion (write)
//{
//  //   printRowWise(os,"  [", entriesPerRow, entryWidth, entryPrec, v.begin(), v.end());
//  //   os << " ]";
//
//  Vector<real_t>::const_iterator it;
//  os << "[ ";
//  for (it = v.begin(); it != v.end(); it++) { os << *it << " "; }
//  os << "]";
//  return os;
//}
//
//template<>
//std::ostream& operator<<(std::ostream& os, const std::vector<complex_t>& v) //! flux insertion (write)
//{
//  //   printRowWise(os,"  [", entriesPerRow/2, 2*entryWidth+1, entryPrec, v.begin(), v.end());
//  //   os << " ]";
//  os << "[ ";
//  Vector<complex_t>::const_iterator it;
//  for (it = v.begin(); it != v.end(); it++)
//  { os << "(" << it->real() << "," << it->imag() << ") "; }
//  os << "]";
//  return os;
//}

//-------------------------------------------------------------------------------
// I/O utilities
// template specialization for real_t and complex_t
//-------------------------------------------------------------------------------
//template<>
//void Vector<real_t >::printRaw(std::ostream& os) const //! print in a raw format (1 column)
//{
//  Vector<real_t>::const_iterator it;
//  for (it = begin(); it != end(); it++) { os << *it << "\n"; }
//}
//
//template<>
//void Vector<complex_t >::printRaw(std::ostream& os) const //! print in a raw format (2 columns)
//{
//  Vector<complex_t>::const_iterator it;
//  for (it = begin(); it != end(); it++) { os << it->real() << " " << it->imag() << "\n"; }
//}


//@{
//! specialisation of valueType
template<>
ValueType Vector<complex_t>::valueType() const
{return _complex;}
template<>
ValueType Vector<Vector<complex_t> >::valueType() const
{return _complex;}
//@}

//@{
//! specialisation of strucType
template<>
StrucType Vector<Vector<real_t> >::strucType() const
{return _vector;}
template<>
StrucType Vector<Vector<complex_t> >::strucType() const
{return _vector;}
//@}

//@{
//! specialisation of elementSize
template<>
number_t Vector<Vector<real_t> >::elementSize() const
{
    if(size()==0) return 0;  //void Vector
    const Vector<real_t>& v=at(0);
    return v.size();
}
template<>
number_t Vector<Vector<complex_t> >::elementSize() const
{
    if(size()==0) return 0;  //void Vector
    const Vector<complex_t>& v=at(0);
    return v.size();
}
//@}

//dimensions of scalar
std::pair<dimen_t, dimen_t> dimsOf(const real_t& v)
{return std::pair<dimen_t, dimen_t> (1, 1);}
std::pair<dimen_t, dimen_t> dimsOf(const complex_t& v)
{return std::pair<dimen_t, dimen_t> (1, 1);}

} // end of namespace xlifepp

