/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Transformation.cpp
  \author N. Kielbasiewicz
  \since 8 oct 2014
  \date 08 feb 2016

  \brief Implementation of xlifepp::Transformation class members and related functions
*/

#include "Transformation.hpp"
#include "printUtils.hpp"
#include "Trace.hpp"

namespace xlifepp
{

//---------------------------------------------------------------------------
// Transformation member functions and related external functions
//---------------------------------------------------------------------------

// constructors
Transformation::Transformation(const string_t& nam, TransformType trt)
: name_(nam), transformType_(trt), mat_(Matrix<real_t>(3,_idMatrix)), vec_(Vector<real_t>(3,0.)), is3D_(true) {}

Transformation::Transformation(real_t a11,real_t a12, real_t a13, real_t a21,real_t a22, real_t a23,real_t a31,real_t a32, real_t a33,
                   real_t b1, real_t b2, real_t b3)
{
  name_="";
  transformType_=_explicitLinear;
  mat_= Matrix<real_t>(3,_idMatrix);
  mat_(1,1)=a11;mat_(1,2)=a12;mat_(1,3)=a13;
  mat_(2,1)=a21;mat_(2,2)=a22;mat_(2,3)=a23;
  mat_(3,1)=a31;mat_(3,2)=a32;mat_(3,3)=a33;
  vec_=Vector<real_t>(3,0.);
  vec_(1)=b1;vec_(2)=b2;vec_(3)=b3;
  is3D_ = (std::abs(mat_(3,1))!=0|| std::abs(mat_(3,2))!=0
          || std::abs(mat_(1,3))!=0 || std::abs(mat_(2,3))!=0 || std::abs(mat_(3,3))!=0);
}
Transformation::Transformation(const Matrix<real_t>& M, const Vector<real_t>& b)
{
  name_="";
  transformType_=_explicitLinear;
  mat_=M;
  vec_=b;
  is3D_ = (std::abs(mat_(3,1))!=0|| std::abs(mat_(3,2))!=0
          || std::abs(mat_(1,3))!=0 || std::abs(mat_(2,3))!=0 || std::abs(mat_(3,3))!=0);
}
//destructor
Transformation::~Transformation()
{
  std::map<number_t, const Transformation*>::iterator it_c;
  for (number_t i=0; i < components_.size(); ++i) { if (components_[i]!=nullptr) { delete components_[i]; } }
}

Transformation::Transformation(const Transformation& t)
{
  name_=t.name_;
  transformType_=t.transformType_;
  mat_=t.mat_;
  vec_=t.vec_;
  is3D_=t.is3D_;

  for (number_t i=0; i < t.components_.size(); ++i) { components_.push_back(t.components_[i]->clone()); }
}

//! assign operator - deep copy
Transformation& Transformation::operator=(const Transformation& t)
{
  if (this != &t)
  {
    name_=t.name_;
    transformType_=t.transformType();
    mat_=t.mat_;
    vec_=t.vec_;
    is3D_=t.is3D_;

    for (number_t i=0; i < components_.size(); ++i) { if (components_[i]!=nullptr) { delete components_[i]; } }
    components_.clear();
    for (number_t i=0; i < t.components_.size(); ++i) { components_.push_back(t.components_[i]->clone()); }
  }
  return *this;
}

void Transformation::buildParam(const Parameter& p, Point& center, real_t& angle, Reals& scale, Reals& dir, Reals& axis, Reals& normal)
{
  ParameterKey key=p.key();
  switch(key)
  {
    case _pk_transformation:
    {
      switch (p.type())
      {
        case _integer:
        case _enumTransformType:
          this->transformType_=TransformType(p.get_n());
          break;
        default:
          error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_center:
    {
      switch (p.type())
      {
        case _pt: center = p.get_pt(); break;
        case _integer: center = Point(real_t(p.get_i())); break;
        case _real: center = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_dir:
    {
      switch (p.type())
      {
        case _integer: dir = std::vector<real_t>(1, real_t(p.get_n())); break;
        case _real: dir = std::vector<real_t>(1, p.get_r()); break;
        case _pt: dir = std::vector<real_t>(p.get_pt()); break;
        case _realVector:
        {
          dir = p.get_rv();
          if (dir.size() > 3) { error("bad_size"); }
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_axis:
    {
      switch (p.type())
      {
        case _integer: axis = std::vector<real_t>(1, real_t(p.get_n())); break;
        case _real: axis = std::vector<real_t>(1, p.get_r()); break;
        case _pt: axis = std::vector<real_t>(p.get_pt()); break;
        case _realVector:
        {
          axis = p.get_rv();
          if (axis.size() > 3) { error("bad_size"); }
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      for (number_t i=axis.size(); i < 3; ++i) { axis.push_back(0.); }
      break;
    }
    case _pk_normal:
    {
      switch (p.type())
      {
        case _realVector:
        {
          normal = p.get_rv();
          if (normal.size() != 3) { error("bad_size"); }
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      for (number_t i=normal.size(); i < 3; ++i) { normal.push_back(0.); }
      break;
    }
    case _pk_angle:
    {
      switch (p.type())
      {
        case _integer: angle = real_t(p.get_i()); break;
        case _real: angle = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_scale:
    {
      switch (p.type())
      {
        case _integer: scale = std::vector<real_t>(1, p.get_n()); break;
        case _real: scale = std::vector<real_t>(1, p.get_r()); break;
        case _realVector: scale = p.get_rv(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default:
      error("param_not_found", words("param key",key));
  }
}

//! build mat_ and vec_ related to the linear transformation (general function)
void Transformation::buildMat()
{
  if(transformType_==_explicitLinear || transformType_==_noTransform) return; //nothing to do
  Point p(0.,0.,0.); Point b=apply(p);
  vec_=b;
  is3D_ = (std::abs(b[2])>theEpsilon);
  p(1)=1.;mat_.column(1,apply(p)-b);
  p(1)=0.;p(2)=1.; mat_.column(2,apply(p)-b);
  p(2)=0.;p(3)=1.; mat_.column(3,apply(p)-b);
  if(is3D_) return;
  is3D_= (std::abs(mat_(3,1))>theEpsilon || std::abs(mat_(3,2))>theEpsilon
        || std::abs(mat_(1,3))>theEpsilon || std::abs(mat_(2,3))>theEpsilon || std::abs(mat_(3,3)-1)>theEpsilon);
}

/*
  Operators to define composite geometries
 */
Transformation composeCompositeAndComposite(const Transformation& t1, const Transformation& t2)
{
  Transformation t;
  t.transformType(_composition);

  number_t n1=t1.components_.size();
  number_t n2=t2.components_.size();

  for (number_t i=0; i < n2; ++i) { t.components_.push_back(t2.components_[i]->clone()); }
  for (number_t i=0; i < n1; ++i) { t.components_.push_back(t1.components_[i]->clone()); }

  t.buildMat();
  return t;
}

Transformation composeCompositeAndCanonical(const Transformation& t1, const Transformation& t2)
{
  Transformation t;
  t.transformType(_composition);

  number_t n1=t1.components_.size();

  t.components_.push_back(t2.clone());
  for (number_t i=0; i < n1; ++i) { t.components_.push_back(t1.components_[i]->clone()); }

  t.buildMat();
  return t;
}

Transformation composeCanonicalAndComposite(const Transformation& t1, const Transformation& t2)
{
  Transformation t;
  t.transformType(_composition);

  number_t n2=t2.components_.size();

  for (number_t i=0; i < n2; ++i) { t.components_.push_back(t2.components_[i]->clone()); }
  t.components_.push_back(t1.clone());

  t.buildMat();
  return t;
}

Transformation composeCanonicalAndCanonical(const Transformation& t1, const Transformation& t2)
{
  Transformation t;
  t.transformType(_composition);

  t.components_.push_back(t2.clone());
  t.components_.push_back(t1.clone());

  t.buildMat();
  return t;
}

Transformation& Transformation::operator*=(const Transformation& t)
{
  // case composite * composite
  if (transformType() == _composition && t.transformType() == _composition)
    { *this = composeCompositeAndComposite(*this, t); return *this; }

  // case composite * canonical (= not composite)
  // here t is necessarily canonical, case composite was done previously
  if (transformType() == _composition)
    { *this = composeCompositeAndCanonical(*this, t); return *this; }


  // here *this is necessarily canonical, case composite was done previously

  // case canonical * composite
  if (t.transformType() == _composition)
    { *this = composeCanonicalAndComposite(*this, t); return *this; }

  // case canonical * canonical
  // here t is necessarily canonical, case composite was done previously
  *this = composeCanonicalAndCanonical(*this,t);
  return *this;
}

Transformation operator*(const Transformation& t1, const Transformation& t2)
{
  // case composite * composite
  if (t1.transformType() == _composition && t2.transformType() == _composition)
    { return composeCompositeAndComposite(t1, t2); }

  // case composite * canonical (= not composite)
  // here t2 is necessarily canonical, case composite was done previously
  if (t1.transformType() == _composition)
    { return composeCompositeAndCanonical(t1, t2); }

  // here t1 is necessarily canonical, case composite was done previously

  // case canonical * composite
  if (t2.transformType() == _composition)
    { return composeCanonicalAndComposite(t1, t2); }

  // case canonical * canonical
  // here t2 is necessarily canonical, case composite was done previously
  return composeCanonicalAndCanonical(t1, t2);
}

/*!
    conversion of a canonical Transformation into a composite Transformation with one component
    - if t is canonical, the components_ vector contains only t
    - if t is composite, it does nothing

    this function is necessary when initializing composite Transformation before using += operator
*/
Transformation toComposite(const Transformation& t)
{
  if (t.transformType() == _composition) { return t; }

  Transformation t2(t.name_, _composition);
  t2.components_.push_back(t.clone());
  t2.buildMat();
  return t2;
}

// print facilities
void Transformation::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  string_t na=name_;
  if (name_ == "") { na = "?"; }
  os << "Transformation " << na << " of type " << words("transform",transformType_) << std::endl;
  if (transformType_ == _composition)
  {
    os << "- components:";
    for (number_t i=0; i < components_.size(); ++i)
    {
      os << std::endl << "[" << i << "] ";
      os << components_[i]->name_ << " of type " << words("transform", components_[i]->transformType_);
    }
  }
  if (theVerboseLevel <= 1) { return; }
  os<<eol;
  seteol(13);  os<<"   matrix: "<<mat_; seteol();
  os<<eol<<"   vector: "<<vec_;
}

std::ostream& operator<<(std::ostream& os, const Transformation& t)
{
  t.print(os);
  return os;
}

//!< return the scale factor (=1. if no homothety)
//
real_t Transformation::scaleFactor() const
{
  real_t scale=1.;
  if (transformType_ == _homothety) { scale=homothety()->factor(); }
  if (transformType_ == _composition)
  {
    for (number_t i=0; i < components_.size(); ++i)
    {
      if (components_[i]->transformType_ == _homothety) { scale *= components_[i]->homothety()->factor(); }
    }
  }
  return scale;
}

Point Transformation::apply(const Point& p) const
{
  if (transformType_==_explicitLinear)
  {
    switch(p.size())
    { case 1: return Point(mat_(1,1)*p(1)+vec_(1));
      case 2: return Point(mat_(1,1)*p(1)+mat_(1,2)*p(2)+vec_(1), mat_(2,1)*p(1)+mat_(2,2)*p(2)+vec_(2));
      default: return (mat_*Vector<real_t>(p)+vec_);
    }
  }
  Point q(p);
  for (number_t i=0; i < components_.size(); ++i) { q=components_[i]->apply(q); }
  return q;
}

//mat_ must be already build
Point Transformation::inverse(const Point& p) const
{
  number_t d=p.size();
  Vector<real_t> x(p);
  if(d<3) x.resize(3,0);
  x-=vec_;
  Vector<real_t> y=xlifepp::inverse(mat_)*x;
  y.resize(d);
  return Point(y);
}

Translation::Translation() : Transformation("Translation", _translation), u_(3,0.)
{
  vec_=u_;
  is3D_=false;
  buildMat();
}

Point Translation::apply(const Point& p) const
{
  Point q(p);
  number_t du=u_.size();
  if (std::abs(u_[du-1])<theEpsilon) du--;
  if (std::abs(u_[du-1])<theEpsilon) du--;
  if (p.dim()<du) q.resize(du,0.);
  for (number_t i=0; i < du; ++i) q[i]+=u_[i];
  return q;
}

void Translation::build(const std::vector<Parameter>& ps)
{
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_dir);

  Point center;
  real_t angle;
  Reals scale, dir(3, 0.), axis, normal;

  for (number_t i=0; i < ps.size(); ++i)
  {
    const Parameter& p=ps[i];
    ParameterKey key=p.key();
    buildParam(p, center, angle, scale, dir, axis, normal);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) { error("unknown_unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);
  }
  if (dir.size() == 1) { dir.push_back(0.); }
  u_=dir;
  vec_=dir;
  is3D_=(dir.size()>2 && std::abs(dir[2])>theEpsilon);
  buildMat();
}

// print facilities
void Translation::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  os << name_ << " of vector " << u_;
}

Rotation2d::Rotation2d() : Transformation("Rotation 2d", _rotation2d), c_(0.,0.), angle_(0.)
{
  buildMat();
}

void Rotation2d::build(const std::vector<Parameter>& ps)
{
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_center);
  params.insert(_pk_angle);

  Point center(0.,0.);
  real_t angle(0.);
  Reals scale, dir, axis, normal;

  for (number_t i=0; i < ps.size(); ++i)
  {
    const Parameter& p=ps[i];
    ParameterKey key=p.key();
    buildParam(p, center, angle, scale, dir, axis, normal);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) { error("unknown_unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);
  }

  if (c_.dim() > 2) { where("Rotation2d::Rotation2d(...)"); warning("free_warning", "3rd coordinate of center point ignored"); }
  c_=center;
  angle_=angle;
  is3D_=false;
  buildMat();
}

Point Rotation2d::apply(const Point& p) const
{
  Point q(std::vector<real_t>(std::max(p.dim(),dimen_t(2)), 0.));
  real_t ca = std::cos(angle_), sa = std::sin(angle_);
  Point tmp=p;
  Point ctmp=c_;
  if (p.dim()==1) tmp.push_back(0.);
  if (c_.dim()==1) ctmp.push_back(0.);
  real_t u = tmp[0]-ctmp[0], v = tmp[1]-ctmp[1];
  q[0] = ctmp[0] + ca*u - sa*v;
  q[1] = ctmp[1] + sa*u + ca*v;
  if (p.dim() == 3) { q[2]=p[2]; }
  return q;
}

// print facilities
void Rotation2d::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  os << name_ << " of center " << c_ << " and angle " << angle_;
}

Rotation3d::Rotation3d() : Transformation("Rotation 3d", _rotation3d), c_(0.,0.,0.), d_(3, 0.), angle_(0.)
{
  buildMat();
}

void Rotation3d::build(const std::vector<Parameter>& ps)
{
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_center);
  params.insert(_pk_axis);
  params.insert(_pk_angle);

  Point center(0.,0.,0.);
  real_t angle(0.);
  Reals scale, dir, axis(3, 0.), normal;

  for (number_t i=0; i < ps.size(); ++i)
  {
    const Parameter& p=ps[i];
    ParameterKey key=p.key();
    buildParam(p, center, angle, scale, dir, axis, normal);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) { error("unknown_unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);
  }

  if (center.dim() > 3) { error("3D_only", "Rotation3d::Rotation3d(...)"); }
  if (axis.size() > 3) { error("3D_only", "Rotation3d::Rotation3d(...)"); }
  c_=center;
  d_=axis;
  angle_=angle;
  is3D_=true;
  buildMat();
}

Point Rotation3d::apply(const Point& p) const
{
  Point pp=p;
  for (number_t i=p.dim(); i < 3; ++i) { pp.push_back(0.); }
  Point cc=c_;
  for (number_t i=cc.dim(); i < 3; ++i) { cc.push_back(0.); }
  std::vector<real_t> dd=d_;
  for (number_t i=dd.size(); i < 3; ++i) { dd.push_back(0.); }

  std::vector<real_t> n=dd/std::sqrt(dot(dd,dd));
  std::vector<real_t> u=pp-cc;
  real_t udotn=dot(u,n);
  // Rodrigues' rotation formulae
  real_t ca = std::cos(angle_);
  Point q = cc + ca*u + (1.-ca)*udotn*n + std::sin(angle_)*crossProduct(n,u);
  return q;
}

// print facilities
void Rotation3d::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  os << name_ << " of center " << c_ << ", axis " << d_ << " and angle " << angle_;
}

void Scaling::build(const std::vector<Parameter>& ps)
{
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_center);
  if (transformType_ != _ptReflection) { params.insert(_pk_scale); }

  Point center(0.,0.,0.);
  real_t angle;
  Reals scale(1,1.), dir, axis, normal;
  if (transformType_ == _ptReflection) { scale[0]=-1.; }

  for (number_t i=0; i < ps.size(); ++i)
  {
    const Parameter& p=ps[i];
    ParameterKey key=p.key();
    buildParam(p, center, angle, scale, dir, axis, normal);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) { error("unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);
  }
  isIsotropic_=(scale.size() == 1);
  if (transformType_ != _scaling)
  {
    if (scale.size() != 1) { error("scalar_only"); }
  }
  c_=center;
  factor_=scale;
  is3D_=false;
  buildMat();
}

Point Scaling::apply(const Point& p) const
{
  Point q(p), c(c_);
  if (q.dim() < c.dim()) { for (number_t i=q.dim(); i < c.size(); ++i) { q.push_back(0.); } }
  else if (q.dim() > c.dim()) { for (number_t i=c.dim(); i < q.size(); ++i) { c.push_back(0.); } }
  if (isIsotropic_)
  {
    const real_t& f=factor_[0];
    for (number_t i=0; i < p.dim(); ++i) { q[i]=c[i]+f*(q[i]-c[i]); }
  }
  else
  {
    for (number_t i=0; i < p.dim(); ++i) { q[i]=c[i]+factor_[i]*(q[i]-c[i]); }
  }
  return q;
}

// print facilities
void Scaling::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  os << name_ << " of center " << c_ << " and factor " << factor_;
}

// print facilities
void Homothety::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  os << name_ << " of center " << c_ << " and factor " << factor_[0];
}

// print facilities
void PointReflection::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  os << name_ << " of center " << c_;
}

Reflection2d::Reflection2d() : Transformation("Reflection 2D", _reflection2d) , c_(0.,0.), d_(2,0.)
{
  buildMat();
}

void Reflection2d::build(const std::vector<Parameter>& ps)
{
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_center);
  params.insert(_pk_dir);

  Point center(0.,0.);
  real_t angle;
  Reals scale, dir(2,0.), axis, normal;

  for (number_t i=0; i < ps.size(); ++i)
  {
    const Parameter& p=ps[i];
    ParameterKey key=p.key();
    buildParam(p, center, angle, scale, dir, axis, normal);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) { error("unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);
  }

  c_=center;
  d_=dir;
  is3D_=false;
  buildMat();
}

Point Reflection2d::apply(const Point& p) const
{
  real_t h;
  Point q(0.,0.);
  q[0]=p[0];
  if (p.dim() > 1) q[1]=p[1];
  Point r=2.*projectionOnStraightLine(q,c_,c_+d_,h) - q;
  if (p.dim() == 3) { r.push_back(p[2]); }
  return r;
}

// print facilities
void Reflection2d::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  os << name_ << " of axis " << c_ << " and " << d_;
}

Reflection3d::Reflection3d()  : Transformation("Reflection 3D", _reflection3d), c_(0,0.,0.), n_(3,0.)
{
  buildMat();
}

void Reflection3d::build(const std::vector<Parameter>& ps)
{
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_center);
  params.insert(_pk_normal);

  Point center(0.,0.,0.);
  real_t angle;
  Reals scale, dir, axis, normal(3,0.);

  for (number_t i=0; i < ps.size(); ++i)
  {
    const Parameter& p=ps[i];
    ParameterKey key=p.key();
    buildParam(p, center, angle, scale, dir, axis, normal);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) { error("unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);
  }

  c_=center;
  n_=normal;
  is3D_=true;
  buildMat();
}

std::vector<real_t> Reflection3d::u() const
{
  std::vector<real_t> u(3,0.);
  if (std::abs(n_[1]) < theEpsilon && std::abs(n_[2]) < theEpsilon)
  {
    u[1]=1.;
    u[2]=0.;
  }
  else
  {
    u[1]=n_[2];
    u[2]=-n_[1];
  }
  return u;
}

std::vector<real_t> Reflection3d::v() const
{
  return crossProduct(n_,u());
}

Point Reflection3d::apply(const Point& p) const
{
  Point pp=p;
  for (number_t i=p.dim(); i < 3; ++i) { pp.push_back(0.); }
  real_t h;
  Point q=2.*projectionOfPointOnPlane(pp,c_,c_+u(),c_+v(),h) - pp;
  return q;
}

// print facilities
void Reflection3d::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  string_t na=name_;
  if (name_ == "") { na = "?"; }
  os << "Reflection 3D " << na << " of plane " << c_ << " and (normal) " << n_;
}

void* cloneTransformation(const void* p)
{
  return reinterpret_cast<void*>((reinterpret_cast<const Transformation*>(p))->clone());
}

void deleteTransformation(void *p)
{
  if (p!=nullptr) delete reinterpret_cast<Transformation*>(p);
}

Parameter& Parameter::operator=(const Transformation& t)
{
  if(type_==_pointerTransformation && reinterpret_cast<const Transformation*>(p_)==&t) return *this;  // same Transformation
  deletePointer();
  Transformation* nt= t.clone();
  p_ = static_cast<void*>(nt);
  type_=_pointerTransformation;
  return *this;
}


} // end of namespace xlifepp
