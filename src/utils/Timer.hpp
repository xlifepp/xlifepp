/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Timer.hpp
  \authors D. Martin, N. Kielbasiewicz
  \since 24 nov 2002
  \date 3 may 2012

  \brief Definition of the xlifepp::Timer class
 */

#ifndef TIMER_HPP
#define TIMER_HPP

#include "config.h"
#include <ctime>

namespace xlifepp
{

class PrintStream;  //forward class

/*!
  \class Timer
  cpu time and elapsed time management class
 */
class Timer
{
  private:
    time_t t;              //!< time (=std::time(0))
    tm localt;             //!< local time (=std::localtime(std::time(0)))
    number_t sec_;         //!< time in seconds since 01/01/1970 00:00:00
    number_t microSec_;    //!< time in microseconds since 01/01/1970 00:00:00
    number_t usrSec_;      //!< user time ("cputime") in seconds since beginning of current process
    number_t usrMicroSec_; //!< user time ("cputime") in microseconds since beginning of current process
    number_t sysSec_;      //!< system time in seconds since beginning of current process
    number_t sysMicroSec_; //!< system time in microseconds since beginning of current process

  public:
    // constructor
    Timer()
      : t(std::time(0)), sec_(0), microSec_(0), usrSec_(0), usrMicroSec_(0), sysSec_(0), sysMicroSec_(0)
    {
      std::time(&t);
      getCpuTime();
      getTime();
    }

    //--------------------------------------------------------------------------------
    //   public Time functions
    //--------------------------------------------------------------------------------

    //! converts time to tm struct
    void update()
    {
      localt = *std::localtime(&t);
	  //localtime_s(&localt,&t);            //Visual Studio
    }
    //! year as an int
    number_t year() const
    {
      return localt.tm_year + 1900;
    }
    //! month as an int [0-11]
    dimen_t month() const
    {
      return static_cast<dimen_t>(localt.tm_mon);
    }
    //! day of month as an int [0-31]
    dimen_t day() const
    {
      return localt.tm_mday;
    }
    //! hour of day (24h clock)
    dimen_t hour() const
    {
      return localt.tm_hour;
    }
    //! minutes of hour [0-59]
    dimen_t minutes() const
    {
      return localt.tm_min;
    }
    //! seconds of minute [0-59]
    dimen_t seconds() const
    {
      return localt.tm_sec;
    }

    void getCpuTime();                    //!< returns user time ("cputime") interval since beginning of process
    real_t deltaCpuTime(Timer* ot) const; //!< returns elapsed user time ("cputime") interval since "ot" time
    void getTime();                       //!< returns time in seconds and milliseconds since 01/01/1970 00:00:00
    real_t deltaTime(Timer* ot) const;    //!< returns elapsed time interval since "ot" time

    void print(std::ostream&) const;      //!< print Timer object
    void print(PrintStream& os) const;

}; // end of class Timer

//--------------------------------------------------------------------------------
// Global Scope Time objects
//--------------------------------------------------------------------------------
extern Timer* theStartTime_p;
extern Timer* theLastTime_p;

inline std::ostream& operator<<(std::ostream& out, const Timer& T)
{T.print(out); return out;}

//--------------------------------------------------------------------------------
// Extern date functions
//--------------------------------------------------------------------------------

string_t theTime();      //!< returns current time e.g. 04h32
string_t theDate();      //!< returns current date as dd.mmm.yyyy e.g. 29.feb.2004
string_t theShortDate(); //!< returns current date as mm/dd/yyyy (en) or dd/mm/yyyy (fr)
string_t theLongDate();  //!< returns current date as Month Day, Year (en) or Day Month Year (fr)
string_t theIsoDate();   //!< returns ISO8601 format of current date (yyyy-mm-dd), e.g. 2004-02-29
string_t theIsoTime();   //!< returns ISO8601 format of current time (hh-mi-ss), e.g. 04-39-51

//--------------------------------------------------------------------------------
// Extern cpu time functions
//--------------------------------------------------------------------------------

real_t cpuTime();                                 //!< returns user time ("cputime") interval in sec. since last runtime 'call'
real_t cpuTime(const string_t& comment, std::ostream & out=std::cout);          //!< returns user time ("cputime") interval in sec. since last runtime 'call' and prints it with comment
real_t totalCpuTime();                            //!< returns elapsed time interval in sec. since first runtime 'call'
real_t totalCpuTime(const string_t& comment,std::ostream & out=std::cout);     //!< returns elapsed time interval in sec. since first runtime 'call' and prints it with comment
real_t elapsedTime();                             //!< returns elapsed time interval in sec. since last runtime 'call'
real_t elapsedTime(const string_t& comment, std::ostream & out=std::cout);          //!< returns elapsed time  interval in sec. since last runtime 'call' and prints it with comment
real_t totalElapsedTime();                        //!< returns elapsed time interval in sec. since first runtime 'call'
real_t totalElapsedTime(const string_t& comment, std::ostream & out=std::cout); //!< returns elapsed time interval in sec. since first runtime 'call' and prints it with comment

real_t cpuTime(const string_t& comment, PrintStream& out);
real_t totalCpuTime(const string_t& comment, PrintStream& out);
real_t elapsedTime(const string_t& comment, PrintStream& out);
real_t totalElapsedTime(const string_t& comment, PrintStream& out);

real_t cpuTime(const string_t& comment, CoutStream& out);
real_t totalCpuTime(const string_t& comment, CoutStream& out);
real_t elapsedTime(const string_t& comment, CoutStream& out);
real_t totalElapsedTime(const string_t& comment, CoutStream& out);

} // end of namespace xlifepp

#endif /* TIMER_HPP */
