/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file memoryUtils.hpp
  \authors E. Lunéville
  \since 07 nov 2013
  \date 07 nov 2013

  \brief Definition of functions related to memory usage
 */

#ifndef MEMORYUTILS_HPP
#define MEMORYUTILS_HPP

#include "config.h"


namespace xlifepp
{

//! memory units
enum MemoryUnit {_byte,_kilobyte,_megabyte,_gigabyte,_terabyte};
//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const MemoryUnit& mu);

/*!
  memory utilities (OS dependant)
 */

class Memory
{
  public:
    static real_t physicalMem(MemoryUnit mu=_megabyte);       //!< amount of physical memory
    static real_t physicalFreeMem(MemoryUnit mu=_megabyte);   //!< amount of physical memory
    static real_t virtualMem(MemoryUnit mu=_megabyte);        //!< amount of virtual memory
    static real_t virtualFreeMem(MemoryUnit mu=_megabyte);    //!< amount of virtual memory
    static real_t processPhysicalMem(MemoryUnit mu=_megabyte);//!< amount of physical memory used by process
    static real_t processVirtualMem(MemoryUnit mu=_megabyte); //!< amount of virtual memory used by process
};

real_t byteTo(number_t mem, MemoryUnit mu=_megabyte);         //!< convert from byte to xxxbyte

} // end of namespace xlifepp

#endif // MEMORYUTILS_HPP
