/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Triplet.hpp
  \author E. Lunéville
  \since 11 feb 2015
  \date 11 feb 2015

  \brief Definition of the xlifepp::Triplet class

  useful class to deal with triplet (a,b,c)
 */

#ifndef TRIPLET_HPP
#define TRIPLET_HPP

#include "config.h"
#include <iostream>

namespace xlifepp
{

template <class K = number_t>
class Triplet
{
  public:
    K first,second,third;
    Triplet(const K&a=K(), const K& b=K(), const K& c=K())
    :first(a),second(b),third(c) {}
    void print(std::ostream&) const;
    void print(PrintStream& os) const {print(os.currentStream());}
};

template <class K>
void Triplet<K>::print(std::ostream& out) const
{
  out<<"("<<first<<", "<<second<<", "<<third<<")";
}

template <class K>
std::ostream& operator<<(std::ostream& out, const Triplet<K>& t)
{
  t.print(out);
  return out;
}

template <class K>
bool operator==(const Triplet<K>& t1, const Triplet<K>& t2)
{
  return (t1.first==t2.first && t1.second==t2.second && t1.third==t2.third);
}

template <class K>
bool operator!=(const Triplet<K>& t1, const Triplet<K>& t2)
{
  return ! (t1==t2);
}

template <class K>
bool operator<(const Triplet<K>& t1, const Triplet<K>& t2)
{
  if(t1.first<t2.first) return true;
  if(t1.first>t2.first) return false;
  if(t1.second<t2.second) return true;
  if(t1.second>t2.second) return false;
  if(t1.third<t2.third) return true;
  return false;
}

template <class K>
bool operator>(const Triplet<K>& t1, const  Triplet<K>& t2)
{
  return t2<t1;
}

} // end of namespace xlifepp

#endif // TRIPLET_HPP
