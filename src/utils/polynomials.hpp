/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file polynomials.hpp
  \authors E. Lunéville
  \since 31 jan 2015
  \date 31 jan 2015

  \brief polynomial stuff including MonomialT, PolynomialT, PolynomialBasisT and PolynomialsBasisT template classes

   this file deals with all stuff related to polynomials as formal polynomials.
   There are 4 classes templated by values of polynomials (K), double by default (K=double)

   MonomialT<K>   : to deal with monomial x^i.y^j.z^k  K is the type of x,y,z
   PolynomialT<K> : to deal with any linear combination of monomials, managed using a list<pair<MonomialT<K>, K> >
   PolynomialBasisT<K> : to deal with set of polynomials, inherits from list<PolynomialT<K> >
   PolynomialsBasisT<K> : to deal with set of vector polynomials, inherits from list<vector<PolynomialT<K> > >

   some particular scalar polynomials spaces may be constructed in 1d, 2d or 3d:
       Pn[x,y,z]   :  sum a_ijk x^i.y^j.z^k   i,j,k >=0  i+j+k <= n
       PHn[x,y,z]  :  sum a_ijk x^i.y^j.z^k   i,j,k >=0  i+j+k = n
       Qn[x,y,z]   :  sum a_ijk x^i.y^j.z^k   i,j,k >=0  i,j,k <= n
       Qns[x,y,z]  :  sum a_ijk x^i.y^j.z^k   i,j,k >=0  i<=nx, j<=ny, k<=nz
   some particular vector polynomials spaces related to FE space are also provided (see PolynomialsBasisT class)

   Main algebraic operations are supported and some derivative operations too.
   Polynomials may be evaluated at point (x,y,z) using operator ()

   Besides, Polynomial may have a tree representation using PolyNodeT<K> class. It may be usefull when evaluating large polynomial

   Note: PolynomialBasis are not mandatory to be basis, no check is performed

   For users,
      Monomial aliases MonomialT<double>
      Polynomial aliases PolynomialT<double>
      PolynomialBasis aliases PolynomialBasisT<double>
      PolynomialsBasis aliases PolynomialsBasisT<double>
*/

#ifndef POLYNOMIALS_HPP
#define POLYNOMIALS_HPP

//===============================================================================
// library dependencies
#include "config.h"
#include "String.hpp"
#include "Messages.hpp"
//===============================================================================

namespace xlifepp
{

enum PolynomialSpace {_Pk,_PHk,_Qk,_Qks,_Rk, _SHk,_Dk,_DQk,_DQ2k,_DQ3k};  //!< particular Polynomial spaces

/*!
  \class MonomialT
  deals with a Monomial of 1, 2 or 3 variables of type T, say x1^a1 x2^a2 x3^a3
  */
template <typename K = real_t>
class MonomialT
{
  public:
    dimen_t a1, a2, a3;

    MonomialT(dimen_t p1=0, dimen_t p2=0, dimen_t p3=0)
      : a1(p1), a2(p2), a3(p3) {}

    K operator()(const K& x1, const K& x2 = K(1), const K& x3 = K(1))const
    {
      K un=K(1), zero=K(0), res=un;
      if(a1==0 && a2==0 && a3==0) return un;
      if(x1==0 && a1>0) return zero;
      if(x2==0 && a2>0) return zero;
      if(x3==0 && a3>0) return zero;
      if(x1!=un && a1>0) res = std::pow(x1,a1);
      if(x2!=un && a2>0) res *= std::pow(x2,a2);
      if(x3!=un && a3>0) res *= std::pow(x3,a3);
      return res;
    }

    K operator()(const std::vector<K>& x) const
    {
      dimen_t d=x.size();
      if(d==1) return operator()(x[0]);
      if(d==2) return operator()(x[0],x[1]);
      return operator()(x[0],x[1],x[2]);
    }

    dimen_t degree() const
    {
      return a1+a2+a3;
    }

    bool isZero() const
    {
      return (a1==0 && a2==0 && a3==0);
    }

    MonomialT<K>& operator*=(const MonomialT<K>& m)
    {
      a1+=m.a1;
      a2+=m.a2;
      a3+=m.a3;
      return *this;
    }

    void swapVar(dimen_t v1, dimen_t v2, dimen_t v3);   //!< swap variables x1->xv1, x2->xv2, x3->xv3

    string_t asString() const;

    void print(std::ostream& out) const
    {
      out << asString();
    }

    void print(PrintStream& os) const {print(os.currentStream());}

    friend std::ostream& operator<<(std::ostream& out, const MonomialT& m)
    {
      m.print(out);
      return out;
    }
};

template <typename K>
string_t MonomialT<K>::asString() const
{
  if(a1==0 && a2==0 && a3==0) return "1";
  string_t s="", p="";
  if(a1>0)
    {
      if(a1==1) s="x";
      else s="x^"+tostring(a1);
    }
  if(s!="") p=".";
  if(a2>0)
    {
      if(a2==1) s+=p+"y";
      else s+=p+"y^"+tostring(a2);
    }
  if(s!="") p=".";
  if(a3>0)
    {
      if(a3==1) s+=p+"z";
      else s+=p+"z^"+tostring(a3);
    }
  return s;
}

// swap variables x1->xv1, x2->xv2, x3->xv3,
// e.g    m=x^i.y^j.z^k m.swap(2,1,3)-> m=y^i.x^j.z^k
//        m=x^i.y^j  m.swap(2,3,1)-> m=y^i.z^j
// v1,v2,v3 has to be a permutation
template <typename K>
void MonomialT<K>::swapVar(dimen_t v1, dimen_t v2, dimen_t v3)
{
  dimen_t b1=a1, b2=a2, b3=a3;
  if(v1==2) a2=b1;
  if(v1==3) a3=b1;
  if(v2==1) a1=b2;
  if(v2==3) a3=b2;
  if(v3==1) a1=b3;
  if(v3==2) a2=b3;
}

template <typename K>
bool operator==(const MonomialT<K>& m1, const MonomialT<K>& m2)
{
  return m1.a1==m2.a1 && m1.a2==m2.a2 && m1.a3==m2.a3;
}
template <typename K>
bool operator!=(const MonomialT<K>& m1, const MonomialT<K>& m2)
{
  return m1.a1!=m2.a1 || m1.a2!=m2.a2 || m1.a3!=m2.a3;
}

template <typename K>
MonomialT<K> operator*(const MonomialT<K>& m1, const MonomialT<K>& m2)
{
  MonomialT<K> mr=m1;
  return mr*=m2;
}

// order to sort monomial (see Polynomial buildTree)
template <typename K>
bool operator < (const MonomialT<K>& m1, const MonomialT<K>& m2)
{
  dimen_t n1 = (m1.a1>0) + (m1.a2>0) + (m1.a3>0);
  dimen_t n2 = (m2.a1>0) + (m2.a2>0) + (m2.a3>0);
  if(n1<n2) return true;
  if(n2<n1) return false;
  dimen_t d1=m1.a1 + m1.a2 + m1.a3, d2=m2.a1 + m2.a2 + m2.a3;
  if(d1<d2) return true;
  if(d2<d1) return false;
  if(m1.a1 < m2.a1) return true;
  if(m2.a1 < m1.a1) return false;
  if(m1.a2 < m2.a2) return true;
  if(m2.a2 < m1.a2) return false;
  if(m1.a3 < m2.a3) return true;
  if(m2.a3 < m1.a3) return false;
  return false;
}

template <typename K>
bool operator > (const MonomialT<K>& m1, const MonomialT<K>& m2)
{return !(m1<m2 || m1==m2);}

//=============================================================================================
/*!
  \class PolyNodeT
  deals with a tree representation of Polynomials
  */
//=============================================================================================

/*tree description of a polynomials (few variables) like a Horner's factorisation
  each node represents a factor (1,x1,x2,x3, ...), the root node represents the polynomial itself
  each node manages
    type: factor type (0: 1, 1: x1, 2:x2, 3:x3, ...)
    coef:efficient of a monomial when node is a leaf, 0 if not a leaf
    child:he first child
    right:he right node
    parent: the parent node
 For intance the tree representing the polynomial p(x1,x2,x3)=2.+4.x1+5.x1x2+x1x3+3.x2x3+5.x3^2 + is ( (t ,c) means (type,coefficient) )

                             root node (0,0) p=2.+4.x1+5.x1x2+x1x3+3.x2x3+5.x3^2
                                                      |
                  1---------------------x1--------------------------x2-----------------------x3
                  |                      |                           |                        |
             (0,2) p=2       (1,0) p=4.x1+5.x1x2+x1x3         (2,0) p=3.x2x3           (3,0) p=5.x3^2
                                         |                           |                        |
                             1----------x2----------x3              -x3-                     -x3-
                             |           |           |               |                        |
                       (0,4) p=4  (2,0) p=5.x2  (3,0) p=x3      (3,0) p=x3               (3,0) p=5.x3
                                         |           |               |                        |
                                        -1-         -1-             -1-                      -1-
                                         |           |               |                        |
                                    (0,5) p=5   (0,1) p=1       (0,3) p=3                (0,5) p=5

    note: not a unique representation, deal with x1 first, then x2, ...

*/

template <typename K=real_t>
class PolyNodeT
{
  public:
    dimen_t type;        //!< 0: 1, 1: x1, 2:x2, 3:x3, ...
    PolyNodeT<K>* parent;//!< the parent node
    K coef;              //!< coefficient of a monomial when node is a leaf, 0 is not a leaf
    PolyNodeT<K>* child; //!< the first child
    PolyNodeT<K>* right; //!< the right node

    PolyNodeT(dimen_t t=0, PolyNodeT<K>* p=nullptr, const K& a=K(0))
      : type(t), parent(p),coef(K(a)), child(nullptr), right(nullptr) {}
    ~PolyNodeT();
    void clear();
    void insert(const MonomialT<K>&, const K&);  //insert monomial a.x1^i1.x2^i2.x3^i3
    bool rootNode() const {return (coef==K(0) && parent==nullptr);}
    bool isZero() const {return (coef==K(0) && child==nullptr);}
    K operator()(const K& x1, const K& x2=K(1), const K& x3=K(1)) const;
    void print(std::ostream&) const;
    void print(PrintStream& os) const {print(os.currentStream());}
    void printTree(std::ostream&, std::string&) const;
    void printTree(PrintStream& os, std::string& s) const {printTree(os.currentStream(), s);}
    string_t varString() const;
};


template <typename K>
PolyNodeT<K>::~PolyNodeT()
{
  clear();
}

//kill all children an set to void node
template <typename K>
void PolyNodeT<K>::clear()
{
  PolyNodeT<K>* p=child, *q=nullptr;
  if(p==nullptr) return;
  while(p!=nullptr)
    {
      q=p->right;
      delete p;
      p=q;
    }
  child=nullptr;
  coef=K(0);
  type=0;
}

//insert monomial a.x1^i1.x2^i2.x3^i3
template <typename K>
void PolyNodeT<K>::insert(const MonomialT<K>& m, const K& a)
{
  if(a==K(0)) return;    //zero monomial are not inserted
  if(coef!=0)  //current node is a leaf (child=0)
    {
      coef+=a;    //same monomial, add them
      return;
    }

  //general case
  if(m.a1>0)
    {
      if(child==nullptr)  //no child, create one
        {
          child = new PolyNodeT<K>(1,this);
          child->insert(MonomialT<K>(m.a1-1,m.a2,m.a3),a);
          return;
        }
      PolyNodeT<K>* p=child;
      while(p!=nullptr && p->type!=1) p=p->right;
      if(p!=nullptr)
        {
          p->insert(MonomialT<K>(m.a1-1,m.a2,m.a3),a);
          return;
        }
      p=child;
      while(p->right!=nullptr) p=p->right;
      p->right = new PolyNodeT<K>(1,this);
      p->right->insert(MonomialT<K>(m.a1-1,m.a2,m.a3),a);
      return;
    }

  if(m.a2>0)
    {
      if(child==nullptr)  //no child, create one
        {
          child = new PolyNodeT<K>(2,this);
          child->insert(MonomialT<K>(m.a1,m.a2-1,m.a3),a);
          return;
        }
      PolyNodeT<K>* p=child;
      while(p!=nullptr && p->type!=2) p=p->right;
      if(p!=nullptr)
        {
          p->insert(MonomialT<K>(m.a1,m.a2-1,m.a3),a);
          return;
        }
      p=child;
      while(p->right!=nullptr) p=p->right;
      p->right = new PolyNodeT<K>(2,this);
      p->right->insert(MonomialT<K>(m.a1,m.a2-1,m.a3),a);
      return;
    }
  if(m.a3>0)
    {
      if(child==nullptr)  //no child, create one
        {
          child = new PolyNodeT<K>(3,this);
          child->insert(MonomialT<K>(m.a1,m.a2,m.a3-1),a);
          return;
        }
      PolyNodeT<K>* p=child;
      while(p!=nullptr && p->type!=3) p=p->right;
      if(p!=nullptr)
        {
          p->insert(MonomialT<K>(m.a1,m.a2,m.a3-1),a);
          return;
        }
      p=child;
      while(p->right!=nullptr) p=p->right;
      p->right = new PolyNodeT<K>(3,this);
      p->right->insert(MonomialT<K>(m.a1,m.a2,m.a3-1),a);
      return;
    }
  //constant polynom: a leaf
  if(child==nullptr)  //no child, create one
    {
      child = new PolyNodeT<K>(0,this);
      child->coef=a;
      return;  //leaf
    }
  PolyNodeT<K>* p=child;
  while(p!=nullptr && p->type!=0) p=p->right;
  if(p!=nullptr)
    {
      p->coef+=a;
      return;
    }
  p=child;
  while(p->right!=nullptr) p=p->right;
  p->right = new PolyNodeT<K>(0,this,a);
}

template <typename K>
K PolyNodeT<K>::operator()(const K& x1, const K& x2, const K& x3) const
{
  if(coef!=K(0))  return coef;
  K val=K(0);
  PolyNodeT<K>* p=child;
  while(p!=nullptr)
    {
      val+=p->operator()(x1,x2,x3);
      p=p->right;
    }
  if(type==1)  return val*=x1;
  if(type==2)  return val*=x2;
  if(type==3)  return val*=x3;
  return val;
}

template <typename K>
string_t PolyNodeT<K>::varString() const
{
  switch(type)
    {
      case 0: return "1";
      case 1: return "x";
      case 2: return "y";
      case 3: return "z";
      default: return "?";
    }
}

template <typename K>
void PolyNodeT<K>::print(std::ostream& out) const
{
  std::string s="";
  if(!rootNode())
    {
      if(coef!=K(0)) out<<coef;
      else out<<varString();
    }
  PolyNodeT<K>* p=child;
  if(p==nullptr) return;
  if(!rootNode()) out<<"(";
  while(p!=nullptr)
    {
      out << s;
      p->print(out);
      p=p->right;
      s="+";
    }
  if(!rootNode()) out<<")";
}

template <typename K>
void PolyNodeT<K>::printTree(std::ostream& out, std::string& e) const
{
  out<<e;
  if(coef!=K(0))  out<<coef;
  if(type>0) out<<"x"<<type;
  out<<std::endl;
  PolyNodeT<K>* p=child;
  std::string f=e;
  e+="  ";
  while(p!=nullptr)
    {
      p->printTree(out,e);
      p=p->right;
    }
  e=f;
}

//=============================================================================================
/*!
  \class PolynomialT
  deals with a Polynomial, say a linear combination of Monomials
*/
//=============================================================================================
template <typename K=real_t>
class PolynomialT
{
  public:
    std::list<std::pair<MonomialT<K>, K > > monomials;  //!< list of monomials and their coefficients
    real_t epsilon;                                     //!< value used to round coefficients to zero
    mutable PolyNodeT<K> tree;                          //!< tree representation

    typedef typename std::list<std::pair<MonomialT<K>, K > >::iterator iterator;
    typedef typename std::list<std::pair<MonomialT<K>, K > >::const_iterator const_iterator;

    PolynomialT()
    {
      epsilon=100000*theEpsilon; //about 1.E-10
      monomials.push_back(std::make_pair(MonomialT<K>(0,0,0),K(0)));   //zero Polynomial
    }
    PolynomialT(const MonomialT<K>& m1,const K& a1=K(1))
    {
      epsilon=100000*theEpsilon; //about 1.E-10
      monomials.push_back(std::make_pair(m1,a1));
    }
    PolynomialT(const MonomialT<K>& m1,const K& a1, const MonomialT<K>& m2, const K& a2)
    {
      epsilon=100000*theEpsilon; //about 1.E-10
      monomials.push_back(std::make_pair(m1,a1));
      monomials.push_back(std::make_pair(m2,a2));
    }

    PolynomialT(const PolynomialT<K>& p)  //copy constructor
    {
      monomials=p.monomials;
      epsilon=p.epsilon;
      tree=PolyNodeT<K>();   //tree is not copied, built on the flight if required
    }

    PolynomialT<K>& operator=(const PolynomialT<K>& p)  //assign operator
    {
      monomials=p.monomials;
      epsilon=p.epsilon;
      if(!tree.isZero()) tree.clear();   //clear current tree
      tree=PolyNodeT<K>();   //tree is not copied, built on the flight if required
      return *this;
    }

    void push_back(const K& a, const MonomialT<K>& m)
    { monomials.push_back(std::make_pair(m,a)); }

    iterator begin() {return monomials.begin();}
    iterator end()   {return monomials.end();}
    const_iterator begin() const {return monomials.begin();}
    const_iterator end() const   {return monomials.end();}

    string_t asString() const;  //!< string form of polynomial
    void buildTree() const;     //!< build tree representation of polynomial

    bool isZero() const  //!< true if 0 polynomial
    {
      for(const_iterator itm=begin(); itm!=end(); ++itm)
        if(itm->second!=K(0)) return false;
      return true;
    }

    //evaluation functions
    K eval(const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! eval polynomial at x1,x2,x3
    {
      if(!tree.isZero())  return tree(x1,x2,x3);  //eval polynomial using tree representation
      K res=K(0); //eval polynomial using monomial representation
      for(const_iterator it=begin(); it!=end(); ++it) res+=it->second * it->first(x1,x2,x3);
      return res;
    }

    K evalTree(const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //eval polynomial using tree representation
    {
      return tree(x1,x2,x3);
    }

    K operator()(const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const
    {
      return eval(x1,x2,x3);
    }

    K operator()(const std::vector<K>& x) const
    {
      dimen_t d=x.size();
      if(d==1) return operator()(x[0]);
      if(d==2) return operator()(x[0],x[1]);
      return operator()(x[0],x[1], x[2]);
    }

    dimen_t degree() const
    {
      dimen_t deg=0;
      for(const_iterator it=begin(); it!=end(); ++it) deg=std::max(deg,it->first.degree());
      return deg;
    }

    PolynomialT<K> operator()(const PolynomialT<K>& px, const PolynomialT<K>& py=PolynomialT<K>(),
                              const PolynomialT<K>& pz=PolynomialT<K>()) const; //!<eval polynomial with other polynomials

    PolynomialT<K>& replace(VariableName, const PolynomialT<K>&);                //!replace one variable by a polynomial

    PolynomialT<K>& operator *=(const MonomialT<K>& m)  //! product by a Monomial
    {
      for(iterator it=begin(); it!=end(); ++it) *it=std::make_pair(it->first*m,it->second);
      return *this;
    }

    PolynomialT<K>& operator *=(const K& k) //product by a scalar
    {
      for(iterator it=begin(); it!=end(); ++it) *it=std::make_pair(it->first,k * it->second);
      return *this;
    }

    PolynomialT<K>& operator /=(const K& k) //product by a scalar
    {
      if(k==K(0)) error("divBy0");
      for(iterator it=begin(); it!=end(); ++it) *it=std::make_pair(it->first,k / it->second);
      return *this;
    }

    PolynomialT<K>& operator +=(const MonomialT<K>& m) //add a Monomial to current Polynomial
    {
      for(iterator it=begin(); it!=end(); ++it)
        if(it->first==m)
          {
            it->second+=1;
            return *this;
          }
      push_back(K(1),m);
      return *this;
    }

    PolynomialT<K>& operator -=(const MonomialT<K>& m) //add a Monomial to current Polynomial
    {
      for(iterator it=begin(); it!=end(); ++it)
        if(it->first==m)
          {
            it->second-=1;
            return *this;
          }
      push_back(K(-1),m);
      return *this;
    }

    PolynomialT<K>& operator +=(const PolynomialT<K>& p) //add a Polynomial to current Polynomial
    {
      for(const_iterator jt=p.begin(); jt!=p.end(); ++jt)
        {
          bool cont=true;
          for(iterator it=begin(); it!=end() && cont; ++it)
            if(it->first==jt->first)
              {
                it->second+=jt->second;
                cont=false;
              }
          if(cont) push_back(jt->second,jt->first);
        }
      clean();
      return *this;
    }

    PolynomialT<K>& operator -=(const PolynomialT<K>& p) //add a Polynomial to current Polynomial
    {
      for(const_iterator jt=p.begin(); jt!=p.end(); ++jt)
        {
          bool cont=true;
          for(iterator it=begin(); it!=end() && cont; ++it)
            if(it->first==jt->first)  //same Monomial
              {
                it->second-=jt->second;
                cont=false;
              }
          if(cont) push_back(-jt->second,jt->first);
        }
      clean();
      return *this;
    }

    PolynomialT<K>& operator *=(const PolynomialT<K>& p) //multiply by a Polynomial to current Polynomial
    {
      PolynomialT<K> res;
      for(const_iterator jt=p.begin(); jt!=p.end(); ++jt)
        res+=jt->second*((*this)*jt->first);
      monomials= res.monomials;
      clean();
      if(!tree.isZero()) buildTree();   //rebuild the tree
      return *this;
    }

    void clean(real_t asZero)  //!remove Monomials with coefficients < asZero
    {
      for(iterator it=begin(); it!=end();)
        if(std::abs(it->second)<asZero) it=monomials.erase(it);
        else it++;
      if(monomials.size()==0) push_back(K(0),MonomialT<K>(0,0,0));
    }

    void clean()  //!remove Monomials with coefficients < epsilon defined in Polynomials
    {
      for(iterator it=begin(); it!=end();)
        if(std::abs(it->second)<epsilon) it=monomials.erase(it);
        else it++;
      if(monomials.size()==0) push_back(K(0),MonomialT<K>(0,0,0));
    }

    void swapVar(dimen_t v1, dimen_t v2, dimen_t v3);   //!< swap variables x1->xv1, x2->xv2, x3->xv3

    static PolynomialT<K> zero()
    {
      return PolynomialT<K>(MonomialT<K>(0,0,0),K(0));
    }

    void print(std::ostream& out) const
    {
      out << asString();
    }

    void print(PrintStream& os) const {print(os.currentStream());}

    friend std::ostream& operator<<(std::ostream& out, const PolynomialT& p)
    {
      p.print(out);
      return out;
    }
};

template <typename K>
string_t PolynomialT<K>::asString() const
{
  string_t s="", p="", as="";
  const_iterator it=begin();
  for (; it!=end(); ++it)
  {
    K a=it->second;
    if (isTestMode) { a=xlifepp::round(a,1e-5);}

    if (a!=0)
    {
      if (std::abs(std::abs(a)-K(1)) > 0.001) as=tostring(a);
      else as="";
      if (a < K(0)&& as=="") as="-";
      if (a < K(0)) s+=as;
      else s+=p+as;
      if (it->first.asString() != "1" || as == "" || as == "-" || as == "+") s+=it->first.asString();
    }
    if (s != "") p="+";
  }
  if (s == "") s="0";
  return s;
}

//build tree representation associated to polynom, works on a mutable data
template <typename K>
void PolynomialT<K>::buildTree() const
{
  if(!tree.isZero()) tree.clear();   //reset to void
  //sort monomials by number or variables and then degrees (x^2^2z, y^3z^2, ^z4, x^2, 1)
  std::map<MonomialT<K>, K > sortMon;
  for(const_iterator it=begin(); it!=end(); ++it) sortMon.insert(*it);
  //insert monomials in tree
  typename std::map<MonomialT<K>, K >::iterator it=sortMon.begin();
  for(; it!=sortMon.end(); ++it)
    if(it->second != K(0)) tree.insert(it->first,it->second);
}

// swap variables x1->xv1, x2->xv2, x3->xv3,
// e.g    m=x^i.y^j.z^k m.swap(2,1,3)-> m=y^i.x^j.z^k
//        m=x^i.y^j  m.swap(2,3,1)-> m=y^i.z^j
// v1,v2,v3 has to be a permutation
template <typename K>
void PolynomialT<K>::swapVar(dimen_t v1, dimen_t v2, dimen_t v3)
{
  for(iterator it=begin(); it!=end(); ++it) it->first.swapVar(v1,v2,v3);
}

//eval polynomial with other polynomials
template <typename K>
PolynomialT<K> PolynomialT<K>::operator()(const PolynomialT<K>& p1, const PolynomialT<K>& p2,
    const PolynomialT<K>& p3) const
{
  PolynomialT<K> res;
  for(const_iterator it=begin(); it!=end(); ++it)   //loop on monomials of current polynomial
    {
      const MonomialT<K>& m=it->first;
      K c=it->second;
      if(c!=K(0))  //non null monomial
        {
          PolynomialT<K> r(MonomialT<K>(0,0,0),c);
          if(m.a1>0 && !p1.isZero())  // replace x1 by p1
            for(number_t i=0; i<m.a1; i++) r*=p1;
          if(m.a2>0 && !p2.isZero())  // replace x2 by p2
            for(number_t i=0; i<m.a2; i++) r*=p2;
          if(m.a3>0 && !p3.isZero())  // replace x3 by p3
            for(number_t i=0; i<m.a3; i++) r*=p3;
          res+=r;
        }
    }
  return res;
}

//!replace variable by Polynomial
template <typename K>
PolynomialT<K>& PolynomialT<K>::replace(VariableName vn, const PolynomialT<K>& p)
{
  PolynomialT<K> res;
  if(vn==_x1) res = (*this)(p);
  if(vn==_x2) res = (*this)(PolynomialT<K>(),p);
  if(vn==_x3) res = (*this)(PolynomialT<K>(),PolynomialT<K>(),p);
  monomials=res.monomials;
  clean();
  if(!tree.isZero()) buildTree();   //rebuild the tree
  return *this;
}

//algebraic operations
template <typename K>
PolynomialT<K> operator *(const PolynomialT<K>& p,const MonomialT<K>& m)
{
  PolynomialT<K> r=p;
  return r*=m;
}

template <typename K>
PolynomialT<K> operator *(const MonomialT<K>& m, const PolynomialT<K>& p)
{
  PolynomialT<K> r=p;
  return r*=m;
}

template <typename K>
PolynomialT<K> operator *(const MonomialT<K>& m, const K& k)  //Monomial * k
{
  PolynomialT<K> r=m;
  return r*=k;
}

template <typename K>
PolynomialT<K> operator *(const K& k, const MonomialT<K>& m)  //Monomial * k
{
  PolynomialT<K> r=m;
  return r*=k;
}

template <typename K>
PolynomialT<K> operator /(const MonomialT<K>& m, const K& k)  //Monomial / k
{
  PolynomialT<K> r=m;
  return r/=k;
}

template <typename K>
PolynomialT<K> operator *(const PolynomialT<K>& p, const K& k)  //Polynomial * k
{
  PolynomialT<K> r=p;
  return r*=k;
}

template <typename K>
PolynomialT<K> operator *(const K& k, const PolynomialT<K>& p)  // k * Polynomial
{
  PolynomialT<K> r=p;
  return r*=k;
}

template <typename K>
PolynomialT<K> operator /(const PolynomialT<K>& p, const K& k)  // Polynomial / k
{
  PolynomialT<K> r=p;
  return r/=k;
}

template <typename K>
PolynomialT<K> operator -(const PolynomialT<K>& m)  // - Polynomial
{
  PolynomialT<K> r=m;
  return r*=K(-1);
}

template <typename K>
PolynomialT<K> operator +(const PolynomialT<K>& p1, const PolynomialT<K>& p2)  // p1+p2
{
  PolynomialT<K> r=p1;
  return r+=p2;
}

template <typename K>
PolynomialT<K> operator -(const PolynomialT<K>& p1, const PolynomialT<K>& p2)  // p1+p2
{
  PolynomialT<K> r=p1;
  return r-=p2;
}

template <typename K>
PolynomialT<K> operator *(const PolynomialT<K>& p1, const PolynomialT<K>& p2)
{
  PolynomialT<K> r=p1;
  typename PolynomialT<K>::const_iterator it=p2.begin();
  for(; it!=p2.end(); ++it)
    {
      r*=it->first;
      r*=it->second;
    }
  return r;
}

//calculate derivatives
template <typename K>
PolynomialT<K> dx(const MonomialT<K>& m)
{
  if(m.a1==0) return PolynomialT<K>();   //zero Polynomial
  return PolynomialT<K>(MonomialT<K>(m.a1-1,m.a2,m.a3),m.a1);
}

template <typename K>
PolynomialT<K> dy(const MonomialT<K>& m)
{
  if(m.a2==0) return PolynomialT<K>();   //zero Polynomial
  return PolynomialT<K>(MonomialT<K>(m.a1,m.a2-1,m.a3),m.a2);
}

template <typename K>
PolynomialT<K> dz(const MonomialT<K>& m)
{
  if(m.a3==0) return PolynomialT<K>();   //zero Polynomial
  return PolynomialT<K>(MonomialT<K>(m.a1,m.a2,m.a3-1),m.a3);
}

template <typename K>
PolynomialT<K> derivative(VariableName vn, const MonomialT<K>& m)
{
  if(vn==_x1) return dx(m);
  if(vn==_x2) return dy(m);
  if(vn==_x3) return dz(m);
  where("derivative(VariableName,MonomialT<K>");
  error("opk_varname_not_handled");
  return PolynomialT<K>(); // fake return
}

template <typename K>
std::vector<PolynomialT<K> > grad(const MonomialT<K>& m, dimen_t d=3)
{
  if(d==0 || d>3)
    {
      where("grad(MonomialT)");
      error("dim_not_in_range",1,3);
    }
  std::vector<PolynomialT<K> > g(d);
  if(d>=1) g[0]= dx(m);
  if(d>=2) g[1]= dy(m);
  if(d>=3) g[2]= dz(m);
  return g;
}

template <typename K>
std::vector<PolynomialT<K> > curl(const MonomialT<K>& m, dimen_t d=3)
{
  if(d<=1 || d>3)
    {
      where("curl(MonomialT)");
      error("dim_not_in_range",2,3);
    }
  std::vector<PolynomialT<K> > g(d);
  if(d==2)
    {
      g[0]=dy(m);
      g[1]=-dx(m);
      return g;
    }
  g[0]=dy(m)-dz(m);
  g[1]=dz(m)-dx(m);
  g[2]=dx(m)-dy(m);
  return g;
}

template <typename K>
PolynomialT<K> integral(VariableName vn, const MonomialT<K>& m)
{
    if(vn==_x1) return PolynomialT<K>(MonomialT<K>(m.a1+1,m.a2,m.a3),1./(m.a1+1));
    if(vn==_x2) return PolynomialT<K>(MonomialT<K>(m.a1,m.a2+1,m.a3),1./(m.a2+1));
    if(vn==_x3) return PolynomialT<K>(MonomialT<K>(m.a1,m.a2,m.a3+1),1./(m.a3+1));
    where("integral(VariableName,MononomialT<K>");
    error("opk_varname_not_handled");
    return PolynomialT<K>::zero(); // fake return
}

template <typename K>
PolynomialT<K> dx(const PolynomialT<K>& p)
{
  PolynomialT<K> dp;
  typename PolynomialT<K>::const_iterator cit= p.begin();
  for(; cit!=p.end(); ++cit)
    {
      PolynomialT<K> d=dx(cit->first);
      dp.push_back(cit->second*d.begin()->second,d.begin()->first);
    }
  dp.clean();
  return dp;
}

template <typename K>
PolynomialT<K> dy(const PolynomialT<K>& p)
{
  PolynomialT<K> dp;
  typename PolynomialT<K>::const_iterator cit= p.begin();
  for(; cit!=p.end(); ++cit)
    {
      PolynomialT<K> d=dy(cit->first);
      dp.push_back(cit->second*d.begin()->second,d.begin()->first);
    }
  dp.clean();
  return dp;
}

template <typename K>
PolynomialT<K> dz(const PolynomialT<K>& p)
{
  PolynomialT<K> dp;
  typename PolynomialT<K>::const_iterator cit= p.begin();
  for(; cit!=p.end(); ++cit)
    {
      PolynomialT<K> d=dz(cit->first);
      dp.push_back(cit->second*d.begin()->second,d.begin()->first);
    }
  dp.clean();
  return dp;
}

template <typename K>
PolynomialT<K> derivative(VariableName vn, const PolynomialT<K>& p)
{
  if(vn==_x1) return dx(p);
  if(vn==_x2) return dy(p);
  if(vn==_x3) return dz(p);
  where("derivative(VariableName,PolynomialT<K>");
  error("opk_varname_not_handled");
  return p; // fake return
}


template <typename K>
PolynomialT<K> integral(VariableName vn, const PolynomialT<K>& p)
{
    PolynomialT<K> ip;
    typename PolynomialT<K>::const_iterator cit= p.begin();
    for(;cit!=p.end(); ++cit)
    {
        PolynomialT<K> i=integral(vn,cit->first);
        ip.push_back(cit->second*i.begin()->second,i.begin()->first);
    }
    ip.clean();
    return ip;
}

template <typename K>
std::vector<PolynomialT<K> > dx(const std::vector<PolynomialT<K> >& ps)
{
  std::vector<PolynomialT<K> > dps(ps.size());
  typename std::vector<PolynomialT<K> >::const_iterator itp=ps.begin();
  typename std::vector<PolynomialT<K> >::iterator itd=dps.begin();
  for(;itp!=ps.end();++itp,++itd) *itd=dx(*itp);
  return dps;
}

template <typename K>
std::vector<PolynomialT<K> > dy(const std::vector<PolynomialT<K> >& ps)
{
  std::vector<PolynomialT<K> > dps(ps.size());
  typename std::vector<PolynomialT<K> >::const_iterator itp=ps.begin();
  typename std::vector<PolynomialT<K> >::iterator itd=dps.begin();
  for(;itp!=ps.end();++itp,++itd) *itd=dy(*itp);
  return dps;
}

template <typename K>
std::vector<PolynomialT<K> > dz(const std::vector<PolynomialT<K> >& ps)
{
  std::vector<PolynomialT<K> > dps(ps.size());
  typename std::vector<PolynomialT<K> >::const_iterator itp=ps.begin();
  typename std::vector<PolynomialT<K> >::iterator itd=dps.begin();
  for(;itp!=ps.end();++itp,++itd) *itd=dz(*itp);
  return dps;
}

template <typename K>
std::vector<PolynomialT<K> > derivative(VariableName vn, const std::vector<PolynomialT<K> >& ps)
{
  if(vn==_x1) return dx(ps);
  if(vn==_x2) return dy(ps);
  if(vn==_x3) return dz(ps);
  where("derivative(VariableName, vector<PolynomialT<K> >");
  error("opk_varname_not_handled");
  return ps; // fake return
}

template <typename K>
std::vector<PolynomialT<K> > integral(VariableName vn, const std::vector<PolynomialT<K> >& ps)
{
  std::vector<PolynomialT<K> > ips(ps.size());
  typename std::vector<PolynomialT<K> >::const_iterator itp=ps.begin();
  typename std::vector<PolynomialT<K> >::iterator iti=ips.begin();
  for(;itp!=ps.end();++itp,++iti) *iti=intg(vn,*itp);
  return ips;
}


template <typename K>
std::vector<PolynomialT<K> > grad(const PolynomialT<K>& p, dimen_t d=3)
{
  if(d==0 || d>3)
    {
      where("grad(PolynomialT)");
      error("dim_not_in_range",1,3);
    }
  std::vector<PolynomialT<K> > g(d);
  if(d>=1) g[0]= dx(p);
  if(d>=2) g[1]= dy(p);
  if(d>=3) g[2]= dz(p);
  return g;
}

//vector curl of a scalar polynomial
template <typename K>
std::vector<PolynomialT<K> > curl(const PolynomialT<K>& p, dimen_t d=3)
{
  if(d<=1 || d>3)
    {
      where("curl(PolynomialT)");
      error("dim_not_in_range",2,3);
    }
  std::vector<PolynomialT<K> > g(d);
  if(d==2)
    {
      g[0]=dy(p);
      g[1]=-dx(p);
      return g;
    }
  g[0]=dy(p)-dz(p);
  g[1]=dz(p)-dx(p);
  g[2]=dx(p)-dy(p);
  return g;
}

//curl of a vector polynomial
template <typename K>
std::vector<PolynomialT<K> > curl(const std::vector<PolynomialT<K> >&  p)
{
  dimen_t d=p.size();
  if(d!=3)
    {
      where("curl(PolynomialT's)");
      error("bad_size", "p", 3, d);
    }
  std::vector<PolynomialT<K> > g(3);
  g[0]=dy(p[2])-dz(p[1]);
  g[1]=dz(p[0])-dx(p[2]);
  g[2]=dx(p[1])-dy(p[0]);
  return g;
}

//div of a vector polynomial, return a scalar polynomial
template <typename K>
PolynomialT<K> div(const std::vector<PolynomialT<K> >& p)
{
  dimen_t d=p.size();
  PolynomialT<K> di=dx(p[0]);
  if(d>=2) di+=dy(p[1]);
  if(d>=3) di+=dz(p[2]);
  return di;
}

//dot product of vector polynomials and vector, assume same size
template <typename K>
PolynomialT<K> dot(const std::vector<PolynomialT<K> >& p, const std::vector<K>& v)
{
  PolynomialT<K> r;
  typename std::vector<PolynomialT<K> >::const_iterator itp=p.begin();
  typename std::vector<K>::const_iterator itv=v.begin();
  for(; itp!=p.end(); ++itp, ++itv) r+= *itp** itv;
  return r;
}

//dot product of vector polynomials, assume same size
template <typename K>
PolynomialT<K> dot(const std::vector<PolynomialT<K> >& p, const std::vector<PolynomialT<K> >& q)
{
  PolynomialT<K> r;
  typename std::vector<PolynomialT<K> >::const_iterator itp=p.begin(), itq=q.begin();
  for(; itp!=p.end(); ++itp, ++itq) r+= *itp** itq;
  return r;
}

//cross product of a vector polynomials and a vector, assume same size 2 or 3, return always a 3d vector
template <typename K>
std::vector<PolynomialT<K> > crossProduct(const std::vector<PolynomialT<K> >& p, const std::vector<K>& v)
{
  number_t s=p.size();
  if(s != v.size()) error("diff_pts_size","crossProduct(vector<PolynomialT>, vector)",s,v.size());
  if(s<=1 || s>3)   error("diff_pts_size","crossProduct(vector<PolynomialT>, vector)",s,v.size());
  std::vector<PolynomialT<K> > r(3);
  r[2]=p[0]*v[1]-p[1]*v[0];
  if(s == 3)
    {
      r[0]=p[1]*v[2]-p[2]*v[1];
      r[1]=p[2]*v[0]-p[0]*v[2];
    }
  return r;
}

//cross product of vector polynomials, assume same size 2 or 3, return always a 3d vector
template <typename K>
std::vector<PolynomialT<K> > crossProduct(const std::vector<PolynomialT<K> >& p, const std::vector<PolynomialT<K> >& q)
{
  number_t s=p.size();
  if(s != q.size()) error("diff_pts_size","crossProduct(vector<PolynomialT>, vector)",s,q.size());
  if(s<=1 || s>3)   error("diff_pts_size","crossProduct(vector<PolynomialT>, vector)",s,q.size());
  std::vector<PolynomialT<K> > r(3);
  r[2]=p[0]*q[1]-p[1]*q[0];
  if(s == 3)
    {
      r[0]=p[1]*q[2]-p[2]*q[1];
      r[1]=p[2]*q[0]-p[0]*q[2];
    }
  return r;
}


//eval vector polynomial at a point (x1,x2,x3)
template <typename K>
std::vector<K> eval(const std::vector<PolynomialT<K> >& p, const K& x1, const K& x2 = K(1), const K& x3 = K(1))
{
  dimen_t d=p.size();
  std::vector<K> r(d,K(0));
  typename std::vector<PolynomialT<K> >::const_iterator itp=p.begin();
  typename std::vector<K>::iterator itr=r.begin();
  for(; itp!=p.end(); ++itp, ++itr) *itr = (*itp)(x1,x2,x3);
  return r;
}

//=============================================================================================
/*!
  \class PolynomialBasisT
  deals with a set of Polynomials
*/
//=============================================================================================
template <typename K=real_t>
class PolynomialBasisT:public std::list<PolynomialT<K> >
{
  public:
    dimen_t dim;       //!< number of variables in Polynomials
    string_t name;     //!< basis name

    typedef typename std::list<PolynomialT<K> >::iterator iterator;
    typedef typename std::list<PolynomialT<K> >::const_iterator const_iterator;

    //constructors
    PolynomialBasisT(dimen_t d=0, const string_t& na="")
      : dim(d), name(na) {}
    PolynomialBasisT(dimen_t d, const MonomialT<K>& m1, const string_t& na="")
      : dim(d), name(na)
    {
      push_back(PolynomialT<K>(m1));
    }
    PolynomialBasisT(dimen_t d, const MonomialT<K>& m1, const MonomialT<K>& m2, const string_t& na="")
      : dim(d), name(na)
    {
      push_back(PolynomialT<K>(m1));
      push_back(PolynomialT<K>(m2));
    }
    PolynomialBasisT(PolynomialSpace, dimen_t, dimen_t, dimen_t =0, dimen_t =0);

    //tools for constructor of particular spaces
    void buildPk(dimen_t);
    void buildPHk(dimen_t);
    void buildQk(dimen_t);
    void buildQks(dimen_t, dimen_t =0, dimen_t=0);
    void buildTree();   //!< build tree representation

    void push_back(const PolynomialT<K>& p)
    {
      std::list<PolynomialT<K> >::push_back(p);
    }
    void add(const MonomialT<K>& m)
    {
      push_back(PolynomialT<K>(m));
    }
    void add(const PolynomialT<K>& m)
    {
      push_back(m);
    }
    void add(const PolynomialBasisT&);

    //iterator access
    iterator begin()
    {
      return std::list<PolynomialT<K> >::begin();
    }
    iterator end()
    {
      return std::list<PolynomialT<K> >::end();
    }
    const_iterator begin() const
    {
      return std::list<PolynomialT<K> >::begin();
    }
    const_iterator end() const
    {
      return std::list<PolynomialT<K> >::end();
    }

    //size accessors
    size_t size() const
    {
      return std::list<PolynomialT<K> >::size();
    }
    void resize(size_t n)
    {
      return std::list<PolynomialT<K> >::resize(n);
    }

    dimen_t degree() const   //! maximal degree
    {
      dimen_t deg=0;
      for(const_iterator it=begin(); it!=end(); ++it) deg=std::max(deg,it->degree());
      return deg;
    }

    PolynomialBasisT<K>& swapVar(dimen_t v1, dimen_t v2, dimen_t v3);   //!< swap variables x1->xv1, x2->xv2, x3->xv3

    std::vector<K>& eval(std::vector<K>& res, const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! evaluate at x=(x1,x2,x3) using monomials representation
    {
      //assuming size of res is up to date !!!
      typename std::vector<K>::iterator ir=res.begin();
      for(const_iterator it=begin(); it!=end(); ++it, ++ir) *ir=(*it)(x1,x2,x3);
      return res;
    }

    std::vector<K> eval(const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! evaluate at x=(x1,x2,x3) using monomials representation
    {
      std::vector<K> res(size(),K(0));
      eval(res, x1, x2, x3);
      return res;
    }

    std::vector<K>& evalTree(std::vector<K>& res, const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! evaluate at x=(x1,x2,x3) using tree representation
    {
      //assuming size of res is up to date !!!
      typename std::vector<K>::iterator ir=res.begin();
      for(const_iterator it=begin(); it!=end(); ++it, ++ir) *ir=it->evalTree(x1,x2,x3);
      return res;
    }

    std::vector<K> evalTree(const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! evaluate at x=(x1,x2,x3) using tree representation
    {
      std::vector<K> res(size(),K(0));
      evalTree(res, x1, x2, x3);
      return res;
    }


    std::vector<K> operator()(const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! evaluate at x=(x1,x2,x3)
    {
      std::vector<K> res(size(),K(0));
      eval(res, x1, x2, x3);
      return res;
    }

    std::vector<K>& operator()(std::vector<K>& res, const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! evaluate at x=(x1,x2,x3)
    {
      //assuming size of res is up to date !!!
      return eval(res,x1,x2,x3);
    }

    void clean()  //! remove monomials with coefficients < asZero
    {
      for(iterator it=begin(); it!=end(); ++it)  it->clean();
    }

    void clean(real_t asZero)  //! remove monomials with coefficients < asZero
    {
      for(iterator it=begin(); it!=end(); ++it)  it->clean(asZero);
    }

    void print(std::ostream& out) const
    {
      out << "Polynomial basis " << name << " in dimension " << dim << ", size " << std::list<PolynomialT<K> >::size() << ", max degree = " << degree() << " :" << std::endl;
      dimen_t i=0;
      for(const_iterator it=begin(); it!=end(); ++it, i++)  out<<"p"<<i<<": "<<*it<<std::endl;
    }

    void print(PrintStream& os) const {print(os.currentStream());}

    friend std::ostream& operator<<(std::ostream& out, const PolynomialBasisT<K>& m)
    {
      m.print(out);
      return out;
    }
};

//constructor for particular Polynomial spaces
template<typename K>
PolynomialBasisT<K>::PolynomialBasisT(PolynomialSpace sp, dimen_t d, dimen_t k, dimen_t k2, dimen_t k3)
{
  dim=d;
  if(dim==0 || dim>3)
    {
      where("PolynomialBasisT<K>::PolynomialBasisT(PolynomialSpace, Dimen, Dimen, Dimen, Dimen)");
      error("dim_not_in_range",1,3);
    }

  switch(sp)
    {
      case _Pk:
        name="P"+tostring(k);
        buildPk(k);
        break;
      case _PHk:
        buildPHk(k);
        break;
      case _Qk:
        name="Q"+tostring(k);
        buildQk(k);
        break;
      case _Qks:
        name="Q"+tostring(k)+"_"+tostring(k2)+"_"+tostring(k3);
        buildQks(k,k2,k3);
        break;
      default:
        where("PolynomialBasisT<K>::PolynomialBasisT(PolynomialSpace, Dimen, Dimen, Dimen, Dimen)");
        error("spacetype_not_handled","sp");
    }
}

//Polynomials of degree at most k (a1+a2+a3<=k)
template<typename K>
void PolynomialBasisT<K>::buildPk(dimen_t k)
{
  name="P"+tostring(k);
  switch(dim)
    {
      case 1:
        for(dimen_t i=0; i<=k; i++) add(MonomialT<K>(i));
        break;
      case 2:
        for(dimen_t i=0; i<=k; i++)
          for(dimen_t j=0; j<=k-i; j++)  add(MonomialT<K>(i,j));
        break;
      default:
        for(dimen_t i=0; i<=k; i++)
          for(dimen_t j=0; j<=k-i; j++)
            for(dimen_t l=0; l<=k-i-j; l++) add(MonomialT<K>(i,j,l));
    }
}

//homogeneous Polynomials of degree k (a1+a2+a3=k)
template<typename K>
void PolynomialBasisT<K>::buildPHk(dimen_t k)
{
  name="PH"+tostring(k);
  switch(dim)
    {
      case 1:
        add(MonomialT<K>(k));
        break;
      case 2:
        for(dimen_t i=0; i<=k; i++) add(MonomialT<K>(i,k-i));
        break;
      default:
        for(dimen_t i=0; i<=k; i++)
          for(dimen_t j=0; j<=k-i; j++) add(MonomialT<K>(i,j,k-i-j));
    }
}

//! Polynomials of degree at most ki on variable i (ai<=ki)
template<typename K>
void PolynomialBasisT<K>::buildQks(dimen_t k1, dimen_t k2, dimen_t k3)
{
  switch(dim)
    {
      case 1:
        for(dimen_t i=0; i<=k1; i++) add(MonomialT<K>(i));
        break;
      case 2:
        for(dimen_t i=0; i<=k1; i++)
          for(dimen_t j=0; j<=k2; j++)  add(MonomialT<K>(i,j));
        break;
      default:
        for(dimen_t i=0; i<=k1; i++)
          for(dimen_t j=0; j<=k2; j++)
            for(dimen_t l=0; l<=k3; l++) add(MonomialT<K>(i,j,l));
    }
}

//! Polynomials of degree at most k on each variable (ai<=k)
template<typename K>
void PolynomialBasisT<K>::buildQk(dimen_t k)
{
  switch(dim)
    {
      case 1:
        for(dimen_t i=0; i<=k; i++) add(MonomialT<K>(i));
        break;
      case 2:
        for(dimen_t i=0; i<=k; i++)
          for(dimen_t j=0; j<=k; j++)  add(MonomialT<K>(i,j));
        break;
      default:
        for(dimen_t i=0; i<=k; i++)
          for(dimen_t j=0; j<=k; j++)
            for(dimen_t l=0; l<=k; l++) add(MonomialT<K>(i,j,l));
    }
}

//! build tree representation of polynomials
template <typename K>
void PolynomialBasisT<K>::buildTree()
{
  for(iterator it=begin(); it!=end(); ++it) it->buildTree();
}

//! add basis to current basis (do not check independancy nor compatibility dim!)
template <typename K>
void PolynomialBasisT<K>:: add(const PolynomialBasisT& pb)
{
  if(pb.size()==0) return;   //nothing to do !
  size_t n=size();
  resize(n+pb.size());
  iterator it=begin()+n;
  const_iterator cit=pb.begin();
  for(; cit!=pb.end(); ++it, ++cit) *it = *cit;
}

//! swap variables x1->xv1, x2->xv2, x3->xv3
template <typename K>
PolynomialBasisT<K>& PolynomialBasisT<K>:: swapVar(dimen_t v1, dimen_t v2, dimen_t v3)
{
  for(iterator it=begin(); it!=end(); ++it) it->swapVar(v1,v2,v3);
  return *this;
}

//! return combination a1*p1+a2*p2+...
template <typename K>
PolynomialT<K> combine(const PolynomialBasisT<K>& ps, const std::vector<K>& a)
{
  PolynomialT<K> p;
  typename PolynomialBasisT<K>::const_iterator it=ps.begin();
  typename std::vector<K>::const_iterator ita=a.begin();
  for(; it!=ps.end(); ++it,++ita)  p+= *ita** it;
  return p;
}

//tensor product of polynomial basis, e.g
//        p basis: p1(x1,x2), p2(x1,x2), ...    q basis: q1(x3), q2(x3), ...
//   ==>  p * q basis: p1(x1,x2)*q1(x3), p1(x1,x2)*q2(x3),..., pi(x1,x2)*qj(x3), ...
// Be cautious: there is no check of variable,
//              for instance if q basis is  q1(x1), q2(x1), ... the result will be pi(x1,x2)*qj(x1) !!!
//              use swapVar on basis q before tensor product: q.swapVar(3,2,1);
template <typename K>
PolynomialBasisT<K> operator*(const PolynomialBasisT<K>& p, const PolynomialBasisT<K>& q)
{
  PolynomialBasisT<K> pq(p.dim+q.dim,p.name+" x "+q.name);
  typename PolynomialBasisT<K>::const_iterator itp, itq;
  for(itp=p.begin(); itp!=p.end(); ++itp)
    for(itq=q.begin(); itq!=q.end(); ++itq)
      pq.push_back(*itp * *itq);
  return pq;
}

//=============================================================================================
/*!
  \class PolynomialsBasisT
  deals with a set of vectors of Polynomials
*/
//=============================================================================================
template <typename K=real_t>
class PolynomialsBasisT : public std::list<std::vector<PolynomialT<K> > >
{
  public:
    dimen_t dimVar;            //!< dimension of Polynomials (number of variables)
    dimen_t dimVec;            //!< dimension of vectors of Polynomials
    string_t name;             //!< basis name
    typedef typename std::list<std::vector<PolynomialT<K> > >::iterator iterator;
    typedef typename std::list<std::vector<PolynomialT<K> > >::const_iterator const_iterator;

    //constructors
    PolynomialsBasisT(dimen_t d=0, dimen_t v=0, const string_t& na="")  //! basic constructor
      :dimVar(d),dimVec(v),name(na) {}
    PolynomialsBasisT(const PolynomialBasisT<K>&, dimen_t, const string_t& na="");       //!< construct PxPxP
    PolynomialsBasisT(PolynomialSpace, dimen_t, dimen_t);                                //!< construct special spaces
    PolynomialsBasisT(const PolynomialBasisT<K>&, const PolynomialBasisT<K>&, const string_t& na=""); //!< construct P1xP2
    PolynomialsBasisT(const PolynomialBasisT<K>&, const PolynomialBasisT<K>&,
                      const PolynomialBasisT<K>&, const string_t& na="");                //!< construct P1xP2xP3
    void buildSHk(dimen_t);   //!< construct SHk = PHk such that x.p=0
    void buildRk(dimen_t);    //!< construct Rk = P(k-1)^n + Sk
    void buildDk(dimen_t);    //!< construct Dk = P(k-1)^n + PH_k-1*x
    void buildDQk(dimen_t);   //!< construct DQk = Qk,k-1,k-1 x Qk-1,k,k-1 x Qk-1,k-1,k
    void buildDQ2k(dimen_t);  //!< construct 2D:  DQ2k= Pk^n + span( curl x^(k+1)y, curl y^(k+1)x ) 3D:  DQ2k= Pk^n + span( curl (yz(wy-wz), xz(wz-wx), xy(wx-wy)) )
    void buildDQ3k(dimen_t);  //!< construct DQ3k = Qk-1,k,k x Qk,k-1,k x Qk,k,k-1
    void buildTree();         //!<construct tree representation of polynomials

    //tools to add vector Polynomials
    void push_back(const std::vector<PolynomialT<K> >& ps)      //! add vector Polynomial to current basis
    {
      std::list<std::vector<PolynomialT<K> > >::push_back(ps);
    }
    void add(const PolynomialT<K>& p1)                          //! add 1d vector of Polynomials to current basis
    {
      std::vector<PolynomialT<K> > ps(1,p1);
      push_back(ps);
    }
    void add(const PolynomialT<K>& p1, const PolynomialT<K>& p2)   //! add 2d vector of Polynomials to current basis
    {
      std::vector<PolynomialT<K> > ps(2,p1);
      ps[1]=p2;
      push_back(ps);
    }
    void add(const PolynomialT<K>& p1, const PolynomialT<K>& p2, const PolynomialT<K>& p3)   //! add 3d vector of Polynomials to current basis
    {
      std::vector<PolynomialT<K> > ps(3);
      ps[0]=p1;
      ps[1]=p2;
      ps[2]=p3;
      push_back(ps);
    }
    void add(const std::vector<PolynomialT<K> >& ps)     //! add vector of Polynomials to current basis
    {
      push_back(ps);
    }
    void add(const PolynomialsBasisT&);    //!< add basis to current basis

    //iterator access
    iterator begin()
    {
      return std::list<std::vector<PolynomialT<K> > >::begin();
    }
    iterator end()
    {
      return std::list<std::vector<PolynomialT<K> > >::end();
    }
    const_iterator begin() const
    {
      return std::list<std::vector<PolynomialT<K> > >::begin();
    }
    const_iterator end() const
    {
      return std::list<std::vector<PolynomialT<K> > >::end();
    }
    size_t size() const
    {
      return std::list<std::vector<PolynomialT<K> > >::size();
    }
    void resize(size_t n)
    {
      return std::list<std::vector<PolynomialT<K> > >::resize(n);
    }

    dimen_t degree() const   //! maximal degree
    {
      dimen_t deg=0;
      for(const_iterator it=begin(); it!=end(); ++it)
        for(dimen_t i=0; i<dimVec; i++) deg=std::max(deg,(*it)[i].degree());
      return deg;
    }

    PolynomialBasisT<K> operator[](number_t i) const //!< return all the i-th components as a PolynomialBasis (i starts from 0)
    {
        if(i>=dimVec) error("free_error","PolynomialBasis::operator[i], i="+tostring(i)+" out of bounds [0,"+tostring(dimVec)+"]");
        PolynomialBasisT<K> pB;
        pB.name=name;
        pB.dim=dimVar;
        const_iterator it=begin();
        for(;it!=end();++it) pB.push_back((*it)[i]);
        return pB;
    }

    void swapVar(dimen_t v1, dimen_t v2, dimen_t v3)   //! swap variables x1->xv1, x2->xv2, x3->xv3
    {
      for(iterator it=begin(); it!=end(); ++it)
        for(dimen_t i=0; i<dimVec; i++)(*it)[i].swapVar(v1,v2,v3);
    }

    void clean()  //!remove monomials with coefficients < epsilon defined in Polynomials
    {
      for(iterator it=begin(); it!=end(); ++it)
        for(dimen_t i=0; i<dimVec; i++)(*it)[i].clean();
    }

    void clean(real_t asZero)  //!remove monomials with coefficients < asZero
    {
      for(iterator it=begin(); it!=end(); ++it)
        for(dimen_t i=0; i<dimVec; i++)(*it)[i].clean(asZero);
    }

    std::vector<std::vector<K> >& eval(std::vector<std::vector<K> >& res, const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! evaluate at x=(x1,x2,x3) using monomials representation
    {
      //assuming size of res is up to date !!!
      typename std::vector<std::vector<K> >::iterator ir=res.begin();
      typename std::vector<K>::iterator iv;
      typename std::vector<PolynomialT<K> >::const_iterator ip;
      std::vector<K> v(dimVec);
      for(const_iterator it=begin(); it!=end(); ++it, ++ir)
        {
          ip=it->begin();
          iv=v.begin();
          for(; ip!=it->end(); ++ip, ++iv)  *iv=ip->eval(x1,x2,x3);
          *ir=v;
        }
      return res;
    }

    std::vector<std::vector<K> > eval(const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! evaluate at x=(x1,x2,x3) using monomials representation
    {
      std::vector<std::vector<K> > res(size());
      eval(res, x1, x2, x3);
      return res;
    }

    std::vector<std::vector<K> >& evalTree(std::vector<std::vector<K> >& res, const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! evaluate at x=(x1,x2,x3) using tree epresentation
    {
      //assuming size of res is up to date !!!
      typename std::vector<std::vector<K> >::iterator ir=res.begin();
      typename std::vector<K>::iterator iv;
      typename std::vector<PolynomialT<K> >::const_iterator ip;
      std::vector<K> v(dimVec);
      for(const_iterator it=begin(); it!=end(); ++it, ++ir)
        {
          ip=it->begin();
          iv=v.begin();
          for(; ip!=it->end(); ++ip, ++iv)  *iv=ip->evalTree(x1,x2,x3);
          *ir=v;
        }
      return res;
    }

    std::vector<std::vector<K> > evalTree(const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! evaluate at x=(x1,x2,x3) using tree representation
    {
      std::vector<std::vector<K> > res(size());
      evalTree(res, x1, x2, x3);
      return res;
    }

    std::vector<std::vector<K> >& operator()(std::vector<std::vector<K> >& res, const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! evaluate at x=(x1,x2,x3)
    {
      //assuming size of res is up to date !!!
      return eval(res,x1,x2,x3);
    }

    std::vector<std::vector<K> > operator()(const K& x1, const K& x2 = K(1), const K& x3 = K(1)) const  //! evaluate at x=(x1,x2,x3)
    {
      std::vector<std::vector<K> > res(size());
      eval(res, x1, x2, x3);
      return res;
    }

    void print(std::ostream& out) const;
    void print(PrintStream& os) const {print(os.currentStream());}

    friend std::ostream& operator<<(std::ostream& out, const PolynomialsBasisT<K>& ps)
    {
      ps.print(out);
      return out;
    }

};

//build tree representation of polynomials
template <typename K>
void PolynomialsBasisT<K>::buildTree()
{
  typename std::vector<PolynomialT<K> >::iterator ip;
  for(iterator it=begin(); it!=end(); ++it)
    {
      ip=it->begin();
      for(; ip!=it->end(); ++ip)  ip->buildTree();
    }
}

//construct a vector Polynomial basis from the product of vector Polynomial and a scalar Polynomial basis
template<typename K>
PolynomialsBasisT<K> operator*(const PolynomialBasisT<K>& pb, const std::vector<MonomialT<K> >& vp)
{
  dimen_t n=vp.size();
  PolynomialsBasisT<K> ps(pb.dim,n);
  typename PolynomialBasisT<K>::const_iterator it=pb.begin();
  switch(n)
    {
      case 1:
        for(; it!=pb.end(); ++it) ps.add(*it * vp[0]);
        break;
      case 2:
        for(; it!=pb.end(); ++it) ps.add(*it * vp[0], *it * vp[1]);
        break;
      case 3:
        for(; it!=pb.end(); ++it) ps.add(*it * vp[0], *it * vp[1], *it * vp[2]);
        break;
      default:
        where("operator*(const PolynomialBasisT<K>&, const std::vector<MonomialT<K> >)");
        error("dim_not_in_range",1,3);
    }
  return ps;
}

//space products
template<typename K>
PolynomialsBasisT<K>::PolynomialsBasisT(const PolynomialBasisT<K>& P, dimen_t v, const string_t& na)  //! construct PxPxP
{
  dimVec=v;
  name = na;
  if(name=="")
    {
      name=P.name;
      if(dimVec>1) name+="^"+tostring(dimVec);
    }
  dimVar = P.dim;
  PolynomialT<K> zero=PolynomialT<K>::zero();
  typename PolynomialBasisT<K>::const_iterator it=P.begin();
  switch(dimVec)
    {
      case 1:
        for(; it!=P.end(); ++it) add(*it);
        return;
      case 2:
        for(; it!=P.end(); ++it)
          {
            add(*it,zero);
            add(zero,*it);
          }
        return;
      case 3:
        for(; it!=P.end(); ++it)
          {
            add(*it,zero,zero);
            add(zero,*it,zero);
            add(zero,zero,*it);
          }
        return;
      default:
        where("PolynomialsBasisT<K>::PolynomialsBasisT(const PolynomialBasisT<K>&, Dimen, const String&)");
        error("dim_not_in_range",1,3);
    }
}

template<typename K>
PolynomialsBasisT<K>::PolynomialsBasisT(const PolynomialBasisT<K>& P1, const PolynomialBasisT<K>& P2, const string_t& na)  //! construct P1xP2
{
  dimVec=2;
  dimVar=P1.dim;
  name = na;
  if(name=="") name=P1.name+"x"+P2.name;
  PolynomialT<K> zero;
  typename PolynomialBasisT<K>::const_iterator it=P1.begin();
  for(; it!=P1.end(); ++it)  add(*it,zero);
  for(it=P2.begin(); it!=P2.end(); ++it)  add(zero,*it);
}

template<typename K>
PolynomialsBasisT<K>::PolynomialsBasisT(const PolynomialBasisT<K>& P1, const PolynomialBasisT<K>& P2,
                                        const PolynomialBasisT<K>& P3,const string_t& na)  //! construct P1xP2xP3
{
  dimVec=3;
  dimVar=P1.dim;
  name = na;
  if(name=="") name=P1.name+"x"+P2.name+"x"+P3.name;
  PolynomialT<K> zero;
  typename PolynomialBasisT<K>::const_iterator it=P1.begin();
  for(; it!=P1.end(); ++it)  add(*it,zero,zero);
  for(it=P2.begin(); it!=P2.end(); ++it)  add(zero,*it,zero);
  for(it=P3.begin(); it!=P3.end(); ++it)  add(zero,zero,*it);
}

//add basis to current basis (do not check independancy nor compatibility dim!)
template <typename K>
void PolynomialsBasisT<K>:: add(const PolynomialsBasisT& ps)
{
  if(ps.size()==0) return;   //nothing to do !
  size_t n=size();
  resize(n+ps.size());
  iterator it=begin();
  for(number_t i=0; i<n; ++i) ++it;
  const_iterator cit=ps.begin();
  for(; cit!=ps.end(); ++it, ++cit) *it = *cit;
}

template <typename K>
void PolynomialsBasisT<K>::print(std::ostream& out) const
{
  out << "Polynomials basis " << name << " in dimension " << dimVar << ", vector size = " << dimVec
      << ", size " << std::list<std::vector<PolynomialT<K> > >::size()
      << ", max degree = "<<degree()<<" :"<<std::endl;
  dimen_t i=0;
  for (const_iterator it=begin(); it!=end(); ++it, i++)
  {
    out << "p" << i << ": [ ";
    string_t vg=",";
    for (dimen_t j=0; j<dimVec; j++)
    {
      if (j == dimVec-1) vg="";
      if (j>0)
      {
        if (i<10) out << "      ";
        else if (i<100) out << "       ";
      }
      out << (*it)[j] << vg;
      if (j != dimVec-1) out << std::endl;
    }
    out<<" ]"<<std::endl;
  }
}

//constructor for particular vector Polynomial spaces (same dim
template<typename K>
PolynomialsBasisT<K>::PolynomialsBasisT(PolynomialSpace sp, dimen_t d, dimen_t k)
{
  dimVar=d;
  if(dimVar==0 || dimVar>3)
    {
      where("PolynomialsBasisT<K>::PolynomialsBasisT(PolynomialSpace, Dimen, Dimen)");
      error("dim_not_in_range",1,3);
    }

  switch(sp)
    {
      case _SHk:
        name="SH"+tostring(k);
        buildSHk(k);
        break;
      case _Rk:
        name="R"+tostring(k);
        buildRk(k);
        break;
      case _Dk:
        name="D"+tostring(k);
        buildDk(k);
        break;
      case _DQk:
        name="DQ"+tostring(k);
        buildDQk(k);
        break;
      case _DQ2k:
        name="DQ2"+tostring(k);
        buildDQ2k(k);
        break;
      case _DQ3k:
        name="DQ3"+tostring(k);
        buildDQ3k(k);
        break;
      default:
        where("PolynomialsBasisT<K>::PolynomialsBasisT(PolynomialSpace, Dimen, Dimen)");
        error("spacetype_not_handled","sp");
    }
}

//construct basis Polynomials p of PHk such that x.p=0 for a given vector v
template <typename K>
void PolynomialsBasisT<K>::buildSHk(dimen_t k)
{
  PolynomialT<K> zero;
  dimVec=dimVar;
  name="SH"+tostring(k);
  switch(dimVar)
    {
      case 2:
        for(int i=1; i<=k; i++) add(MonomialT<K>(i-1,k+1-i),PolynomialT<K>(MonomialT<K>(i,k-i),-1));
        break;
      case 3:
        for(int i=1; i<=k; i++) add(MonomialT<K>(i-1,k+1-i,0),PolynomialT<K>(MonomialT<K>(i,k-i,0),-1), zero);
        for(int i=1; i<=k; i++)
          for(int j=0; j<=k-i; j++) add(MonomialT<K>(i-1,j,k+1-i-j),zero, PolynomialT<K>(MonomialT<K>(i,j,k-i-j),-1));
        for(int j=1; j<=k; j++)
          for(int i=0; i<=k-j; i++) add(zero, MonomialT<K>(i,j-1,k+1-i-j),PolynomialT<K>(MonomialT<K>(i,j,k-i-j),-1));
        break;
      default:
        where("PolynomialsBasisT<K>::buildSHk(Dimen)");
        error("dim_not_in_range",2,3);
    }
}

//construct basis Polynomials p of Rk = P(k-1)^n + Sk
template <typename K>
void PolynomialsBasisT<K>::buildRk(dimen_t k)
{
  dimVec=dimVar;
  name="R"+tostring(k);
  //P(k-1)^n part
  PolynomialBasisT<K> pkm1(_Pk,dimVar,k-1);
  PolynomialsBasisT<K> pkm1n(pkm1,dimVar);
  add(pkm1n);
  //Sk part
  PolynomialsBasisT<K> sk(_SHk,dimVar,k);
  add(sk);
}

//construct basis Polynomials p of Dk= P(k-1)^n + PH_k-1*x
template <typename K>
void PolynomialsBasisT<K>::buildDk(dimen_t k)
{
  dimVec=dimVar;
  name="D"+tostring(k);
  //P(k-1)n part
  PolynomialBasisT<K> pk(_Pk,dimVar,k-1);
  PolynomialsBasisT<K> pkn(pk,dimVar);
  add(pkn);
  //PH_k-1*x part
  std::vector<MonomialT<K> > x(dimVec);
  if(dimVec>=1) x[0]=MonomialT<K>(1,0,0);
  if(dimVec>=2) x[1]=MonomialT<K>(0,1,0);
  if(dimVec>=3) x[2]=MonomialT<K>(0,0,1);
  add(PolynomialBasisT<K>(_PHk,dimVar,k-1) * x);
}

//construct basis Polynomials p of DQk= Qk,k-1,k-1 x Qk-1,k,k-1 x Qk-1,k-1,k
template <typename K>
void PolynomialsBasisT<K>::buildDQk(dimen_t k)
{
  dimVec=dimVar;
  name="DQ"+tostring(k);
  switch(dimVar)
    {
      case 2:
        add(PolynomialsBasisT<K>(PolynomialBasisT<K>(_Qks,2,k,k-1),PolynomialBasisT<K>(_Qks,2,k-1,k)));
        break;
      case 3:
        add(PolynomialsBasisT<K>(PolynomialBasisT<K>(_Qks,3,k,k-1,k-1),PolynomialBasisT<K>(_Qks,3,k-1,k,k-1),PolynomialBasisT<K>(_Qks,3,k-1,k-1,k)));
        break;
      default:
        where("PolynomialsBasisT<K>::buildDQk(Dimen)");
        error("dim_not_in_range",2,3);
    }
}

//construct basis Polynomials p of DQ3k= Qk-1,k,k x Qk,k-1,k x Qk,k,k-1 (3d) and Qk-1,k x Qk,k-1 (2d)
template <typename K>
void PolynomialsBasisT<K>::buildDQ3k(dimen_t k)
{
  dimVec=dimVar;
  name="DQ3"+tostring(k);
  switch(dimVar)
    {
      case 2:
        add(PolynomialsBasisT<K>(PolynomialBasisT<K>(_Qks,2,k-1,k),PolynomialBasisT<K>(_Qks,2,k,k-1)));
        break;
      case 3:
        add(PolynomialsBasisT<K>(PolynomialBasisT<K>(_Qks,3,k-1,k,k),PolynomialBasisT<K>(_Qks,3,k,k-1,k),PolynomialBasisT<K>(_Qks,3,k,k,k-1)));
        break;
      default:
        where("PolynomialsBasisT<K>::buildDQ3k(Dimen)");
        error("dim_not_in_range",2,3);
    }
}

//construct basis Polynomials p of
//   3D:  DQ2k= Pk^n + span( curl (yz(wy-wz), xz(wz-wx), xy(wx-wy)) )
//                      wx in PHk[y,z], wy in PHk[x,z], wz in PHk[x,y]  to be check
//   2D:  DQ2k= Pk^n + span( curl x^(k+1)y, curl y^(k+1)x )
template <typename K>
void PolynomialsBasisT<K>::buildDQ2k(dimen_t k)
{
  if(dimVar<2 || dimVar>3)
    {
      where("PolynomialBasis::buildDQ2k(Dimen)");
      error("dim_not_in_range",2,3);
    }
  dimVec=dimVar;
  name="DQ2"+tostring(k);
  PolynomialBasisT<K> pk(_Pk,dimVar,k);
  PolynomialsBasisT<K> pkn(pk,dimVar);
  add(pkn);
  if(dimVar==2)  //2D case
    {
      MonomialT<K> m1(k+1,1,0),m2(1,k+1,0);
      add(curl(m1,2));
      add(curl(m2,2));
      return;
    }
  //3D case
  PolynomialBasisT<K> pkyz(_PHk,2,k);
  pkyz.swapVar(2,3,1);
  PolynomialBasisT<K> pkxz(_PHk,2,k);
  pkxz.swapVar(1,3,2);
  PolynomialBasisT<K> pkxy(_PHk,2,k);
  PolynomialT<K> zero;
  typename PolynomialBasisT<K>::iterator it=pkyz.begin();
  for(; it!=pkyz.end(); it++)
    {
      std::vector<PolynomialT<K> > p(3);
      p[0]=zero;
      p[1]=-(*it)*MonomialT<K>(1,0,1);
      p[2]= (*it)*MonomialT<K>(1,1,0);
      add(curl(p));
    }
  for(it=pkxz.begin(); it!=pkxz.end(); it++)
    {
      std::vector<PolynomialT<K> > p(3);
      p[0]=(*it)*MonomialT<K>(0,1,1);
      p[1]=zero;
      p[2]= -(*it)*MonomialT<K>(1,1,0);
      add(curl(p));
    }
  for(it=pkxy.begin(); it!=pkxy.end(); it++)
    {
      std::vector<PolynomialT<K> > p(3);
      p[0]=-(*it)*MonomialT<K>(0,1,1);
      p[1]=(*it)*MonomialT<K>(1,0,1);
      p[2]= zero;
      add(curl(p));
    }
}

//construct derivative basis
template <typename K>
PolynomialBasisT<K> dx(const PolynomialBasisT<K>& ps)
{
  PolynomialBasisT<K> r(ps.dim, "dx_"+ps.name);
  typename PolynomialBasisT<K>::const_iterator it=ps.begin();
  for(; it!=ps.end(); ++it) r.push_back(dx(*it));
  return r;
}

template <typename K>
PolynomialBasisT<K> dy(const PolynomialBasisT<K>& ps)
{
  PolynomialBasisT<K> r(ps.dim, "dy_"+ps.name);
  typename PolynomialBasisT<K>::const_iterator it=ps.begin();
  for(; it!=ps.end(); ++it) r.push_back(dy(*it));
  return r;
}

template <typename K>
PolynomialBasisT<K> dz(const PolynomialBasisT<K>& ps)
{
  PolynomialBasisT<K> r(ps.dim, "dz_"+ps.name);
  typename PolynomialBasisT<K>::const_iterator it=ps.begin();
  for(; it!=ps.end(); ++it) r.push_back(dz(*it));
  return r;
}

template <typename K>
PolynomialsBasisT<K> grad(const PolynomialBasisT<K>& ps)
{
  PolynomialsBasisT<K> r(ps.dim,ps.dim, "grad_"+ps.name);
  typename PolynomialBasisT<K>::const_iterator it=ps.begin();
  switch(ps.dim)
    {
      case 1:
        for(; it!=ps.end(); ++it) r.add(dx(*it));
        break;
      case 2:
        for(; it!=ps.end(); ++it) r.add(dx(*it),dy(*it));
        break;
      case 3:
        for(; it!=ps.end(); ++it) r.add(dx(*it),dy(*it),dz(*it));
        break;
    }
  return r;
}

template <typename K>
PolynomialsBasisT<K> curl(const PolynomialsBasisT<K>& ps) //only in 3D
{
  PolynomialsBasisT<K> r(ps.dimVar,ps.dimVec, "curl_"+ps.name);
  typename PolynomialsBasisT<K>::const_iterator it=ps.begin();
  if(ps.dimVec!=3)
    {
      where("curl(PolynomialsBasis)");
      error("bad_size","ps", 3, ps.dimVec);
    }
  for(; it!=ps.end(); ++it) r.add(curl(*it));
  return r;
}

template <typename K>
PolynomialsBasisT<K> dx(const PolynomialsBasisT<K>& ps)
{
  PolynomialsBasisT<K> r(ps.dimVar,ps.dimVec, "dx_"+ps.name);
  typename PolynomialsBasisT<K>::const_iterator it=ps.begin();
  for(; it!=ps.end(); ++it) r.add(dx(*it));
  return r;
}

template <typename K>
PolynomialsBasisT<K> dy(const PolynomialsBasisT<K>& ps)
{
  PolynomialsBasisT<K> r(ps.dimVar,ps.dimVec, "dy_"+ps.name);
  typename PolynomialsBasisT<K>::const_iterator it=ps.begin();
  for(; it!=ps.end(); ++it) r.add(dy(*it));
  return r;
}

template <typename K>
PolynomialsBasisT<K> dz(const PolynomialsBasisT<K>& ps)
{
  PolynomialsBasisT<K> r(ps.dimVar,ps.dimVec, "dz_"+ps.name);
  typename PolynomialsBasisT<K>::const_iterator it=ps.begin();
  for(; it!=ps.end(); ++it) r.add(dz(*it));
  return r;
}

template <typename K>
PolynomialBasisT<K> div(const PolynomialsBasisT<K>& ps) //return a scalar basis
{
  PolynomialBasisT<K> r(ps.dimVar, "div_"+ps.name);
  typename PolynomialsBasisT<K>::const_iterator it=ps.begin();
  if(ps.dimVar==0 || ps.dimVar>3)
    {
      where("div(PolynomialsBasis<K>))");
      error("dim_not_in_range",1,3);
    }
  for(; it!=ps.end(); ++it) r.add(div(*it));
  return r;
}

//! matrix * [p1,p2, ...]
template <typename K>
std::vector<PolynomialT<K> > operator*(const Matrix<K>& mat, const std::vector<PolynomialT<K> >& ps)
{
  number_t m = mat.numberOfRows(), n = mat.numberOfColumns();
  if (n!=ps.size())
  {
    where("operator*(Matrix, vector<PolynomialT>");
    error("bad_dim",n,ps.size());
  }
  std::vector<PolynomialT<K> > rs(m);
  for (number_t i=0; i<m; i++)
    for (number_t j=0; j<n; j++) rs[i]+=mat(i+1,j+1)*ps[j];
  return rs;
}

template <typename K>
PolynomialsBasisT<K> operator*(const Matrix<K>& mat,const PolynomialsBasisT<K>& ps)
{
  PolynomialsBasisT<K> r(ps.dimVar, mat.numberOfRows(), "mat*"+ps.name);
  typename PolynomialsBasisT<K>::const_iterator it=ps.begin();
  for(; it!=ps.end(); ++it) r.add(mat** it);
  return r;
}

//! return combination a1*p1+a2*p2+...
template <typename K>
std::vector<PolynomialT<K> > combine(const PolynomialsBasisT<K>& ps, const std::vector<K>& a)
{
  dimen_t d=ps.dimVec;
  std::vector<PolynomialT<K> > p(d);
  typename PolynomialsBasisT<K>::const_iterator it=ps.begin();
  typename std::vector<K>::const_iterator ita=a.begin();
  for(; it!=ps.end(); ++it,++ita)
    {
      for(dimen_t i=0; i<d; ++i) p[i]+= *ita *(*it)[i];
    }
  return p;
}



//======================== user aliases ===================================
typedef MonomialT<> Monomial;
typedef PolynomialT<> Polynomial;
typedef PolynomialBasisT<> PolynomialBasis;
typedef PolynomialsBasisT<> PolynomialsBasis;
typedef PolyNodeT<> PolyNode;

} // end of namespace xlifepp;

#endif // POLYNOMIALS_HPP
