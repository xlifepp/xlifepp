/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Point.hpp
  \authors D. Martin, E. Lunéville
  \since 29 jan 2005
  \date 3 may 2012

  \brief Definition of the xlifepp::Point class

  class xlifepp::Point holds point coordinates in any dimension
 */

#ifndef POINT_HPP
#define POINT_HPP

#include <vector>
#include <numeric>

#include "config.h"
#include "PrintStream.hpp"

namespace xlifepp
{

// useful alias
typedef std::vector<real_t>::const_iterator cit_vr;
typedef std::vector<real_t>::iterator it_vr;

/*!
  \class Point

  holds point coordinates in any dimension

  Class Point to hide vector<real_t> class to users (see e.g. Function)
  and holds point coordinates in any dimension.
  It is implemented as a child class to stl vector<real_t> and thus
  inherits of all member functions and algorithms of stl class 'vector'.
 */
class Point : public std::vector<real_t>
{
  public:
    typedef real_t SepValueType; //!< useful typedef
    static real_t tolerance; //!< value of the tolerance used in equality test (theTolerance by default)

    //------------------------------------------------------------------------------
    // constructors (use inherited std::vector standard constructors and destructor)
    //------------------------------------------------------------------------------
    Point() {}                                                 //!< void constructor
    // explicit Point(const int, const real_t v = 0.);            //!< constructor by dimension and constant value
    // explicit Point(const dimen_t, const real_t v = 0.);        //!< constructor by dimension and constant value
    explicit Point(const real_t x1);                           //!< 1D constructor by coordinate
    Point(const real_t x1, const real_t x2);                   //!< 2D constructor by coordinates
    Point(const real_t x1, const real_t x2, const real_t x3);  //!< 3D constructor by coordinates
    Point(const dimen_t d, real_t* pt);                        //!< constructor by dimension and real_t array
    Point(const Point& pt);                                    //!< copy constructor
    Point(const std::vector<real_t>& pt);                      //!< constructor from stl vector (not declared explicit to allow autocast)
    Point(const std::vector<real_t>& pt, dimen_t d);           //!< constructor from stl vector with dimension management
    Point(const std::vector<real_t>::const_iterator, dimen_t); //!< constructor from stl vector iterator

    Point& operator=(real_t x1) {this->resize(1); at(0)=x1; return *this;}

    // default copy constructor and assigment operator = of std::vector

    //------------------------------------------------------------------------------
    // accessors (to point coordinates)
    //------------------------------------------------------------------------------
    // first fast call
    // access in math style d-th coordinates ( 0 < d <= dim ) read access (const) and read/write access
    // to get C-style d-th coordinates ( 0 <= d < dim ) use inherited stl vector::operator[]
    real_t operator()(const dimen_t d) const; //!< return the d-th coordinates ( 0 < d <= dim ) (read)
    real_t& operator()(const dimen_t d);      //!< return the d-th coordinates ( 0 < d <= dim ) (read/write)
    real_t coords(const dimen_t d) const      //! return the d-th coordinates ( 0 < d <= dim ) (read)
    {return *(begin() + d - 1);}
    real_t& coords(const dimen_t d)          //! return the d-th coordinates ( 0 < d <= dim ) (read/write)
    {return *(begin() + d - 1);}

    // x,y or z coordinates access
    real_t x() const  //! return the first coordinate  (read)
    {return (*this)(1);}
    real_t& x()       //! return the first coordinate  (read/write)
    {return (*this)(1);}
    real_t y() const  //! return the second coordinate (read)
    {return (*this)(2);}
    real_t& y()       //! return the second coordinate (read/write)
    {return (*this)(2);}
    real_t z() const  //! return the third coordinate  (read)
    {return (*this)(3);}
    real_t& z()       //! return the third coordinate  (read/write)
    {return (*this)(3);}
    dimen_t dim() const //! dimension of point
    {return dimen_t(size());}

    std::vector<real_t> toVect()         //! convert to a std::vector
    {return std::vector<real_t>(*this);}

    //--------------------------------------------------------------------------------
    // algebraic operation
    //--------------------------------------------------------------------------------
    Point& operator+=(const Point&);          //!< add a point to the current point
    Point& operator-=(const Point&);          //!< subtract a point to the current point
    Point& operator+=(const real_t);            //!< add a constant point to the current point
    Point& operator-=(const real_t);            //!< subtract a constant point to the current point
    Point& operator*=(const real_t);            //!< scale the current point
    Point& operator/=(const real_t);            //!< scale the current point
    real_t squareDistance(const Point&) const;  //!< returns square of distance to another point
    real_t distance(const Point&) const;        //!< returns distance to another point
    Point roundToZero(real_t aszero = 100.*theEpsilon) const; //!< round coordinates to zero

    //--------------------------------------------------------------------------------
    // error and output utilities
    //--------------------------------------------------------------------------------
    string_t toString() const;             //!< return coordinates as string (x1,x2,...)
    void dimError(const string_t& s, const dimen_t) const;    //!< error message, the string is to tell in which function the error is detected
    void print(std::ostream&) const;       //!< print on output stream as (x, y, z)
    void print(PrintStream& os) const {print(os.currentStream());}
    void printRaw(std::ostream&) const;    //!< print on output stream as  x y z
    void printRaw(PrintStream& os) const {printRaw(os.currentStream());}
    void printTeX(std::ostream&) const;    //!< prints, on stream os, the coordinates of the point for TeX exploitation;
    void printTeX(PrintStream& os) const {printTeX(os.currentStream());}
    friend std::ostream& operator<<(std::ostream&, const Point&);

}; // end of class Point

std::ostream& operator<<(std::ostream&, const Point&); //!< ostream insert

Point force3D(const Point& p); //!< returns a 3-components copy of a point

//--------------------------------------------------------------------------------
// external operations
//--------------------------------------------------------------------------------
Point operator+(const Point&);                   //!< same point (completeness)
Point operator-(const Point&);                   //!< opposite point
Point operator+(const Point&, const Point&);     //!< sum of two points
Point operator-(const Point&, const Point&);     //!< difference of two points
Point operator+(const Point&, const real_t );      //!< sum of two points
Point operator-(const Point&, const real_t );      //!< difference of two points
Point operator+(const real_t, const Point& );      //!< sum of two points
Point operator-(const real_t, const Point& );      //!< difference of two points
Point operator*(const real_t , const Point&);      //!< scale a point
Point operator*(const Point&, const real_t );      //!< scale a point
Point operator/(const Point&, const real_t );      //!< scale a point
real_t pointDistance(const Point&, const Point&);  //!< returns distance between two points
real_t squareDistance(const Point&, const Point&); //!< returns the square distance between two points

// equal or not
bool operator==(const Point&, const Point&); //!< equality of two points
bool operator!=(const Point&, const Point&); //!< not equality of two points

// partial ordering relation, p<q if it exists i such that p(j)=q(j) j<i and p(i)<q(i)
bool operator< (const Point&, const Point&); //!< leather than between two points
bool operator<=(const Point&, const Point&); //!< leather than or equal (deduced from <)
bool operator> (const Point&, const Point&); //!< greater between two points (deduced from <)
bool operator>=(const Point&, const Point&); //!< greater between or equal (deduced from <)

//for consistent conversion
inline const Point& tran(const Point& p) { return p; }
inline const Point& conj(const Point& p) { return p; }

// special functions used by KdTree<Point>
int_t compare(const Point& p, const real_t& s, int c);                 //!< comparaison function used by KdTree
int_t maxSeparator(const Point& p, const Point& q, int& c, real_t& s); //!< separation function used by KdTree

// operations on points
real_t dist(const Point&, const Point&);                               //!< returns the euclidian distance between 2 points
real_t dot(const Point&, const Point&);                                //!< returns the scalar product between 2 points
real_t norm2(const Point&);                                           //!< returns the square distance between 2 points
Point crossProduct(const Point&, const Point&);                        //!< returns the cross product of 2 points
real_t mixedProduct(const Point&, const Point&, const Point&);         //!< returns the mixed product (AxB).C
real_t crossProduct2D(const Point&O, const Point&A, const Point&B); //!< returns the cross product OAxOB (2D)

//coordinates change
Point toPolar(const Point&);                                           //!< returns Point as Point in polar/cylindrical coordinates
Point toSpherical(const Point&);                                       //!< returns Point as Point in spherical coordinates
Point toEllipticCoordinates(const Point& P, const Point& C,
                            const Point& A1, const Point& A2);                  //!< returns (r,theta) related to the ellipse (C,A1,A2)
Point toEllipticCoordinates(const Point& P, real_t r1, real_t r2);              //!< returns (r,theta) related to the orthogonal ellipse of size (r1,r2)
Point toEllipticCoordinates(const Point& P, const Point& C,
                            const Point& A1, const Point& A2, const Point& A3); //!< returns (r,theta,phi) related to the ellipsoid (C,A1,A2,A3)
Point toEllipticCoordinates(const Point& P, real_t r1, real_t r2, real_t r3);   //!< returns (r,theta,phi) related to the orthogonal ellipsoid of size (r1,r2,r3)
//! compute barycentric coordinates related to segment, triangle or tetrahedron
Point toBarycentricNocheck(const Point& M, const Point& S1, const Point& S2, const Point& S3=Point(), const Point& S4=Point());
Point toBarycentric(const Point& M, const Point& S1, const Point& S2, const Point& S3=Point(), const Point& S4=Point());

///////////////////// various geometric inclusion tests and intersections

//! test if 4 points are coplanar
bool arePointsCoplanar(const Point& p1, const Point& p2, const Point& p3, const Point& p4, real_t tol=10*theEpsilon);
//! test if a set of points is coplanar
bool arePointsCoplanar(const std::vector<Point>& p, real_t tol=10*theEpsilon);
//! test if a point P is inside a segment [AB]
bool isPointInSegment(const Point& P, const Point& A, const Point& B, real_t tol=theEpsilon);
//! test if a point P is inside a triangle ABC
bool isPointInTriangle(const Point& P, const Point& A, const Point& B, const Point& C, real_t tol=theEpsilon);
//! test if a point P is inside a quadrangle ABCD
inline bool isPointInQuadrangle(const Point& P, const Point& A, const Point& B, const Point& C, const Point& D,real_t tol=theEpsilon)
{
  if (!arePointsCoplanar(A,B,C,D,tol)) return false;
  if (isPointInTriangle(P,A,B,C,tol)) return true; return isPointInTriangle(P,A,C,D,tol);
}
//! test if a segment [PQ] is inside a segment [AB]
inline bool isSegmentInSegment(const Point& P, const Point& Q, const Point& A, const Point& B, real_t tol=theEpsilon)
{ return isPointInSegment(P,A,B,tol) && isPointInSegment(Q,A,B,tol); }
//! test if a segment [PQ] is inside a triangle ABC
inline bool isSegmentInTriangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, real_t tol=theEpsilon)
{
  if (!isPointInTriangle(P,A,B,C,tol)) return false;
  return isPointInTriangle(Q,A,B,C,tol);
}
//! test if a segment [PQ] is inside a quadrangle ABCD
inline bool isSegmentInQuadrangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, const Point& D, real_t tol=theEpsilon)
{
  if (!arePointsCoplanar(A,B,C,D,tol)) return false;
  if (!isPointInQuadrangle(P,A,B,C,D,tol)) return false;
  return isPointInQuadrangle(Q,A,B,C,D,tol);
}
//! test if a triangle PQR is inside a triangle ABC
inline bool isTriangleInTriangle(const Point& P, const Point& Q, const Point& R, const Point& A, const Point& B, const Point& C, real_t tol=theEpsilon)
{
  if (!isPointInTriangle(P,A,B,C,tol)) return false;
  if (!isPointInTriangle(Q,A,B,C,tol)) return false;
  return isPointInTriangle(R,A,B,C,tol);
}
//! test if a triangle PQR is inside a quadrangle ABCD
inline bool isTriangleInQuadrangle(const Point& P, const Point& Q, const Point& R, const Point& A, const Point& B, const Point& C, const Point& D, real_t tol=theEpsilon)
{
  if (!arePointsCoplanar(A,B,C,D,tol)) return false;  // non coplanar quadrangle
  if (!isPointInQuadrangle(P,A,B,C,D,tol)) return false;
  if (!isPointInQuadrangle(Q,A,B,C,D,tol)) return false;
  return isPointInQuadrangle(R,A,B,C,D,tol);
}

///////////////////// various specific 2D tools

//! determines if points A, B and C are colinear, a clockwise triangle or a counter-clockwise triangle
int pointOrientation2D(const Point& A, const Point& B, const Point& C, real_t tol=theEpsilon);

//! convex hull of 2D points
std::vector<Point> convexHull2D(std::vector<Point>);  //!< convex hull in 2D

///////////////////// various projection tools

//! orthogonal projection of a point on a straight line
Point projectionOnStraightLine(const Point& M, const Point& A, const Point& B, real_t& h, bool only3D = false);
//! orthogonal projection of a point on a plane
Point projectionOfPointOnPlane(const Point& M, const Point& P1, const Point& P2, const Point& P3, real_t& h, bool only3D = false);
//! orthogonal projection of a segment on a parallel plane
std::pair<Point,Point> projectionOfSegmentOnPlane(const Point& S1, const Point& S2, const Point& P1, const Point& P2, const Point& P3, real_t& h);
//! orthogonal projection of a triangle on a parallel plane
std::vector<Point> projectionOfTriangleOnPlane(const Point& T1, const Point& T2, const Point& T3, const Point& P1, const Point& P2, const Point& P3, real_t& h);
//! projection (minimal distance) of a point M on the segment defined points A, B
Point projectionOnSegment(const Point& M, const Point& A, const Point& B, real_t & h);
//! projection (minimal distance) of point M on triangle given vertices T1, T2, and T3
Point projectionOnTriangle(const Point& M, const Point& T1, const Point& T2, const Point& T3, real_t& h);
//! projection (minimal distance) of point M on quadrangle given points Q1, Q2, Q3 and Q4 (assuming Q1, Q2, Q3 and Q4 are coplanar)
Point projectionOnQuadrangle(const Point& M, const Point& Q1, const Point& Q2, const Point& Q3, const Point& Q4, real_t& h);
//! projection (minimal distance) of point M on tetrahedron given points T1, T2, T3 and T4
Point projectionOnTetrahedron(const Point& M, const Point& T1, const Point& T2, const Point& T3, const Point& T4, real_t& h);

///////////////////// various intersection tools

//! determines if 2 segments [PQ] and [AB] have a intersection or not (faster 2D version)
bool doesSegmentCrossesSegment2D(const Point& P, const Point& Q, const Point& A, const Point& B, real_t tol=theEpsilon);
//! determines if 2 segments [PQ] and [AB] have a unique intersection or not (2D or 3D)
bool doesSegmentCrossesSegment(const Point& P, const Point& Q, const Point& A, const Point& B, real_t tol=theEpsilon);
//! determines if a segment [PQ] and a triangle ABC have a unique intersection or not
bool doesSegmentCrossesTriangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, real_t tol=theEpsilon);
//! determines if a segment [PQ] and a quadrangle ABCD have a unique intersection or not
bool doesSegmentCrossesQuadrangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, const Point& D, real_t tol=theEpsilon);
//! determines if segments [PQ] and [AB] have a intersection or not
bool doesSegmentIntersectsTriangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, real_t tol=theEpsilon);
//! determines if a segment [PQ] and a triangle ABC have a intersection or not
bool doesSegmentIntersectsQuadrangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, const Point& D, real_t tol=theEpsilon);
//! determines if triangles PQR and ABC have a intersection or not
bool doesTriangleIntersectsTriangle(const Point& P, const Point& Q, const Point& R, const Point& A, const Point& B, const Point& C, real_t tol=theEpsilon);
//! determines if a triangle PQR and a quadrangle ABCD have a intersection or not
bool doesTriangleIntersectsQuadrangle(const Point& P, const Point& Q, const Point& R, const Point& A, const Point& B, const Point& C, const Point& D, real_t tol=theEpsilon);
//! determines if quadrangles PQRS and ABCD have a intersection or not
bool doesQuadrangleIntersectsQuadrangle(const Point& P, const Point& Q, const Point& R, const Point& S, const Point& A, const Point& B, const Point& C, const Point& D, real_t tol=theEpsilon);

//! intersection of straight lines
Point intersectionOfStraightLines(const Point& S1, const Point& S2, const Point& T1, const Point& T2, bool& hasIntersect);
//! intersection half line (M,d) with segment [AB]
Point intersectionHalfLineSegment(const Point& M, const std::vector<real_t>& d, const Point& A, const Point& B, bool& hasUniqueIntersection, real_t tol=theEpsilon);
//! intersection half line (M,d) with polygon vs=(v1,v2,...vn)
Point intersectionHalfLinePolygon(const Point& M, const std::vector<real_t>& d, const std::vector<Point>& vs, bool& hasUniqueIntersection, real_t tol=theEpsilon);
//! intersection half line (M,d) with ellipse (C,A,B,tmin,tmax)
Point intersectionHalfLineEllipse(const Point& M, const std::vector<real_t>& d, const Point& C,
                                  const Point& A, const Point& B, bool& hasUniqueIntersection, real_t tmin=0, real_t tmax=2*pi_, real_t tol=theEpsilon);
//! straight line intersection of 2 planes defined respectively by 3 non aligned points
std::pair<Point,Point> intersectionOfPlanes(const Point& S1, const Point& S2, const Point& S3,const Point& T1, const Point& T2, const Point& T3);
//! straight line intersection of 2 planes defined respectively by 3 non aligned points with one vertex in common
Point intersectionOfPlanesWithOneSharedPoint(const Point& S1, const Point& S2, const Point& S3, const Point& T2, const Point& T3);

bool intersectionOfSegments(const Point& P, const Point& Q, const Point& A, const Point& B, Point& I, real_t tol=theEpsilon); //2D-3D
bool intersectionSegmentTriangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, Point& I, Point& J, real_t tol=theEpsilon);
bool intersectionSegmentQuadrangle(const Point& P, const Point& Q, const Point& A, const Point& B, const Point& C, const Point& D, Point& I, Point&J, real_t tol=theEpsilon);

///////////////////// various geometrical tools

//! outward normals to a triangle with given vertices
std::vector<std::vector<real_t> > outwardNormalsOfTriangle(const Point& T1, const Point& T2, const Point& T3);

//! signed distances of a point to the 3 edges of a triangle
std::vector<real_t> signedDistancesToTriangleEdges(const Point& M, const Point& T1, const Point& T2, const Point& T3);
//! lengths of edges of the triangle whose vertices are given
std::vector<real_t> triangleEdgesLengths(const Point& T1, const Point& T2, const Point& T3);
//! lengths of heights of the triangle whose vertices are given
std::vector<real_t> triangleHeightsLengths(const Point& T1, const Point& T2, const Point& T3);

//! determines an edge of a polygon so that the whole polygon is on the same side
std::pair<number_t, number_t> nonSeparatingEdge(const std::vector<Point>& p);
//! common perpendicular of the straight lines
std::pair<Point,Point> commonPerpendicularOfStraightLines(const Point& Am,const Point& Ap,const Point& Bm,const Point& Bp);

//! returns equation of the plane defined by the 3 non aligned given points
std::vector<real_t> eqtOfPlane(const Point& S1, const Point& S2, const Point& S3);

//! computes orientation of a trihedral
std::vector<std::pair<real_t, dimen_t> > trihedralOrientation(const Point& o, const Point& px, const Point& py, const Point& pz);

//vector operations on point (avoid casting to vector)
//dimension are not checked
// inline real_t dot(const Point&A, const Point&B)
// {
//     real_t d=0.;
//     return std::inner_product(A.begin(),A.end(),B.begin(),d);
// }

// fast 3D cross product of "Point", dimension not checked
inline Point cross3D(const Point&A, const Point&B)
{
  Point R(0.,0.,0.);
  R[2] = A[0] * B[1] - A[1] * B[0];
  if(A.size()==2) return R;
  R[0] = A[1] * B[2] - A[2] * B[1];
  R[1] = A[2] * B[0] - A[0] * B[2];
  return R;
}

//create one orthogonal 3D-Point
inline Point orthogonalPoint3D(const Point& P)
{
  real_t x=P[0], y=P[1], z=P[2];
  if(x!=-y || z!=0) return Point(z,z,-x-y);
  return Point(-y-z,x,x); //z=0 & x=-y implies x!=0 if not a null vector
// with no branch but a lot of operations!
//  real_t s=norm(P), g=std::copysign(s, z);  //s if z>=0, elese -s
//  real_t h=z+g;
//  return Point(g*h-x*x, -x*y, -x*h);
}

//create one orthogonal Point Q (OQ.OP=0)
inline Point orthogonalPoint(const Point& P)
{
  switch (P.size())
  {
     case 2: return Point(-P.y(),P.x());
     case 3: return orthogonalPoint3D(P);
     default: break;
  }
  return P; // no orthogonal
}

// 2D coordinates of a set of 3D coplanar points, (p[0],u,v,w) 3D orthogonal basis
std::vector<Point> to2D(const std::vector<Point>& p, Point& u, Point& v, Point& w);
std::vector<Point> to2D(const std::vector<Point>& p);

// split 2D/3D polygon (no hole) in triangles (return triangle numbering)
std::vector<std::vector<number_t> > splitInTriangles(const std::vector<Point>&);

} // end of namespace xlifepp

#endif /* POINT_HPP */
