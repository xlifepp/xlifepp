/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Matrix.hpp
  \authors E. Lunéville, D. Martin
  \since 30 jan 2005
  \date 3 may 2012

  \brief Definition of the xlifepp::Matrix class

  general template class for (small) real or complex dense matrices
  Matrices are rectangular and stored in row-major access.
  Standard operations are inherited from stl vector class

  implemented operations (internal or external) :
  - constructor, copy constructor, assignment operator=, destructor
  - constructor by constant value, vector, special matrices and from input file
  - basic intern algebra operations: += -= *= /= with matrix or scalar
  - unary and binary external operator +, -
  - basic extern algebra operations: + - * / with scalar
  - matrix vector product: overloaded *
  - matrix matrix product: overloaded *
  - transpose and adjoint operation
  - real part, imag part and abs returning a Matrix

  - solve Ax=b via Gauss solver
  - LU, LDLt, LDL* factorizations (not yet implemented)
  - matrix inversion
  - input and output functions
 */

#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "config.h"
#include "Vector.hpp"
#include "Trace.hpp"
#include "Messages.hpp"
#include "String.hpp"
#include "Algorithms.hpp"
#include "complexUtils.hpp"
#include "EigenInterface.hpp"

#include <vector>
#include <fstream>

namespace xlifepp
{

/*!
  \enum SpecialMatrix
  enumeration for special matrices
 */
enum SpecialMatrix
{
  _zeroMatrix = 0, zeroMatrix = _zeroMatrix,
  _idMatrix, idMatrix = _idMatrix,
  _onesMatrix, onesMatrix = _onesMatrix,
  _hilbertMatrix, hilbertMatrix = _hilbertMatrix
};
//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const SpecialMatrix& sm);


/*!
  \class Matrix
  to deal with numeric matrix (say real or complex vector).
  inherited from std::vector<K> (dense storage by row)
  may be used even for matrix of matrices but restricted usage !
 */
template<typename K>
class Matrix : public std::vector<K>
{
protected:
  dimen_t rows_; //!< number of rows (number of columns is defined by size() / rows_)
public:
  typedef K type_t; //!< alias on matrix coefficient type
  //@{
  //! useful typedefs to iterators
  typedef typename std::vector<K>::iterator it_vk;
  typedef typename std::vector<K>::const_iterator cit_vk;
  //@}
  //------------------------------------------------------------------------------
  //constructors/destructor
  //------------------------------------------------------------------------------

  Matrix() //! default constructor (1x1 matrix)
    : rows_(1)
  {
    std::vector<K>::resize(1, K());
  }

  Matrix(const dimen_t r, const dimen_t c) //! constructor for null rectangular matrix
    : rows_(r)
  {
    std::vector<K>::resize(r * c, K(0));
  }

  Matrix(const dimen_t r, const dimen_t c, const K& v) //! constructor for constant rectangular matrix
    : rows_(r)
  {
    std::vector<K>::resize(r * c, v);
  }

//  Matrix(const Matrix<K>& m) //! copy constructor
//    : std::vector<K>(m.begin(), m.end()), rows_(m.rows_) {}

  template <typename KK>
  Matrix(const Matrix<KK>& m) //! copy constructor
    : std::vector<K>(m.begin(), m.end()), rows_(m.numberOfRows()) {}

  template<typename Iterator>
  Matrix(const dimen_t r, const dimen_t c, Iterator it_v, Iterator it_ve) //! constructor from iterators
    : rows_(r)
  {
    std::vector<K>::resize(r * c, K(0.));
    it_vk it_this = begin();
    for(dimen_t k=0; k<r*c && it_v!=it_ve; ++k, ++it_this, ++it_v) *it_this=*it_v;
  }

  Matrix(const std::vector<K>& v) //! constructor for diagonal square matrix
  // this constructor works for Vector<K> because it inherits from std::vector<K>
    : rows_(v.size())
  {
    std::vector<K>::resize(rows_ * rows_, K(0.));
    it_vk it_this = begin();
    for (cit_vk it_v = v.begin(); it_v != v.end(); it_v++, it_this += rows_ + 1)
      *it_this = *it_v;
  }

  Matrix(const std::vector<std::vector<K> >& vv) //! constructor from a sd::vector of std::vectors
  // this constructor works also for Vector<std::vector<K> > because Vector inherits from std::vector
  // but not for std::vector<Vector<K> >, see next constructor
    : rows_(vv.size())
  {
    // define matrix
    size_t cols = 0;
    typename std::vector< std::vector<K> >::const_iterator it_vv;
    for (it_vv = vv.begin(); it_vv != vv.end(); it_vv++)
      cols = std::max(cols, (*it_vv).size());
    std::vector<K>::resize(rows_ * cols, K(0.));
    // copy vector entries into matrix: each vector is used to build (the first entries of) each matrix row
    it_vk it_this = begin(), it;
    for (it_vv = vv.begin(); it_vv != vv.end(); it_vv++, it_this += cols)
      {
        it = it_this;
        for (cit_vk it_v = (*it_vv).begin(); it_v != (*it_vv).end(); it_v++)
          *(it++) = *it_v;
      }
  }

  Matrix(const std::vector<Vector<K> >& vv) //! constructor from a std::vector of Vectors
  // this constructor works for also Vector<Vector<K> > because Vector inherits from std::vector
    : rows_(vv.size())
  {
    // define matrix
    size_t cols = 0;
    typename std::vector<Vector<K> >::const_iterator it_vv;
    for (it_vv = vv.begin(); it_vv != vv.end(); it_vv++)
      cols = std::max(cols, (*it_vv).size());
    std::vector<K>::resize(rows_ * cols, K(0.));
    // copy vector entries into matrix: each vector is used to build (the first entries of) each matrix row
    it_vk it_this = begin(), it;
    for (it_vv = vv.begin(); it_vv != vv.end(); it_vv++, it_this += cols)
      {
        it = it_this;
        for (cit_vk it_v = (*it_vv).begin(); it_v != (*it_vv).end(); it_v++)
          *(it++) = *it_v;
      }
  }

  Matrix(const dimen_t d, const SpecialMatrix sm) //! constructor for special square matrix
    : rows_(d)
  {
    std::vector<K>::resize(rows_ * rows_, K(0.));
    it_vk it = begin();

    switch(sm)
      {
        case _zeroMatrix:
          break;
        case _idMatrix:
          diag(K(1.));
          break;
        case _onesMatrix:
          std::vector<K>::assign(rows_ * rows_, K(1.));
          break;
        case _hilbertMatrix:
          for (dimen_t r = 1; r <= rows_; r++)
            for (dimen_t c = 1; c <= rows_; c++, it++)
              *it = 1. / (r + c -1);
          break;
      }
  }

  string_t typeStr() const  //! type of matrix as string
  { return "?";}

  Matrix(const string_t& f) //! constructor from input file given by name
  { loadFromFile(f.c_str());}

  Matrix(const char* f) //! constructor from input file given by name
  { loadFromFile(f); }

  //------------------------------------------------------------------------------
  //assignment, operator =
  //------------------------------------------------------------------------------
  Matrix<K>& operator=(const Matrix<K>& m) //! assignment operator= (deep copy)
  {
    if (this != &m)    // handle self-assignment
      {
        rows_ = m.rows_;
        std::vector<K>::assign(m.begin(), m.end());
      }
    return *this;
  }

  template <typename KK>
  Matrix<K>& operator=(const Matrix<KK>& m) //! assignment operator=(deep copy)
  {
    rows_ = m.vsize();
    std::vector<K>::assign(m.begin(), m.end());
    return *this;
  }

  Matrix<K>& operator=(const std::vector<std::vector<K> >& vv) //! assignment operator= vector of vector
  {
    rows_ = vv.size();
    size_t cols(0);
    typename std::vector< std::vector<K> >::const_iterator it_vv;
    for (it_vv = vv.begin(); it_vv != vv.end(); it_vv++)  cols = std::max(cols, (*it_vv).size());
    if (size() < rows_ * cols)  std::vector<K>::resize(rows_ * cols);
    // copy vector entries into matrix: each vector is used to build (the first entries of) each matrix row
    it_vk it_this = begin(), it;
    for (it_vv = vv.begin(); it_vv != vv.end(); it_vv++, it_this += cols)
      {
        it = it_this;
        for (cit_vk it_v = (*it_vv).begin(); it_v != (*it_vv).end(); it_v++)
          *it++ = *it_v;
      }
    return *this;
  }

  Matrix<K>& operator=(const std::vector<Vector<K> >& vv) //! assignment operator= vector of Vector
  {
    rows_ = vv.size();
    size_t cols(0);
    typename std::vector< Vector<K> >::const_iterator it_vv;
    for (it_vv = vv.begin(); it_vv != vv.end(); it_vv++)
      cols = std::max(cols, (*it_vv).size());
    if (size() < rows_ * cols) std::vector<K>::resize(rows_ * cols);
    // copy vector entries into matrix: each vector is used to build (the first entries of) each matrix row
    it_vk it_this = begin(), it;
    for (it_vv = vv.begin(); it_vv != vv.end(); it_vv++, it_this += cols)
      {
        it = it_this;
        for (cit_vk it_v = (*it_vv).begin(); it_v != (*it_vv).end(); it_v++)
          *it++ = *it_v;
      }
    return *this;
  }

  //not named "resize" to avoid conflict with the inherited resize function
  void changesize(const dimen_t r, const K& v = K())                 //! resize squared matrix and assign v
  {
    //std::vector<K>::resize(r * r, v);
    std::vector<K>::assign(r*r, v);
    rows_=r;
  }
  void changesize(const dimen_t r, const dimen_t c, const K& v = K()) //! resize matrix and assign v
  {
    //std::vector<K>::resize(r * c, v);
    std::vector<K>::assign(r*c, v);
    rows_=r;
  }

private:

  template<typename KK>
  void assign(const KK& v) //! assign const scalar value to all matrix entries
  {
    for (typename std::vector<K>::iterator it = this->begin(); it < this->end(); it++)
      *it = v;
  }

  //------------------------------------------------------------------------------
  // access functions (size, number of rows, of columns,begin and end, coefficients)
  //------------------------------------------------------------------------------
public:

  size_t size() const //! overloaded size()
  { return std::vector<K>::size();}

  size_t vsize() const //! column length (number of rows)
  { return rows_;}

  dimen_t& rows()      //! access to the the member rows_ (writable)
  { return rows_;}

  dimen_t numberOfRows() const //! number of rows
  { return rows_;}

  size_t hsize() const //! row length (number of columns)
  { return static_cast<size_t>(size() / rows_);}

  dimen_t numberOfColumns() const //! number of columns
  { return static_cast<dimen_t>(size() / rows_); }

  dimen_t numberOfCols() const //! number of columns
  { return static_cast<dimen_t>(size() / rows_);}

  dimPair dims() const //! dimensions
  { return dimPair(rows_,dimen_t(size()/rows_)); }

  it_vk begin() //! overloaded iterator begin
  { return std::vector<K>::begin();}

  cit_vk begin() const //! overloaded const_iterator begin
  { return std::vector<K>::begin();}

  it_vk end() //! overloaded iterator end
  { return std::vector<K>::end();}

  cit_vk end() const //! overloaded const_iterator begin
  { return std::vector<K>::end();}

  const K& operator()(const dimen_t r, const dimen_t c) const //! protected access ( r > 0, c > 0 )
  { return *(begin() + ((r - 1) * size() / rows_ + c - 1));}

  K& operator()(const dimen_t r, const dimen_t c) //! read/write access ( r > 0, c > 0 )
  { return *(begin() + ((r - 1) * size() / rows_ + c - 1));}

  Vector<K> row(const dimen_t r) const //! return r-th row (r > 0) as a stl vector (read only)
  {
    Vector<K> v(begin() + (r - 1)*hsize(), begin() + r * hsize());
    return v;
  }

  void row(const dimen_t r, const std::vector<K>& v) //! set r-th row (r > 0) from a stl vector
  {
    if (v.size() != hsize()) mismatchDims("row A(r,.)=vector", v.size(), 1);
    it_vk it = begin() + (r - 1) * hsize();
    for (cit_vk it_v = v.begin(); it_v != v.end(); it_v++, it++)
      *it = *it_v;
  }

  Vector<K> column(const dimen_t c) const //! return c-th (c > 0) column as a stl vector
  {
    Vector<K> v(rows_);
    int cols = hsize();
    cit_vk it = begin() + c - 1;
    for (it_vk it_v = v.begin(); it_v != v.end(); it += cols, it_v++)
      *it_v = *it;
    return v;
  }

  void column(const dimen_t c, const std::vector<K>& v) //! set c-th column (c > 0) from a stl vector
  {
    if (v.size() > rows_) mismatchDims("column A(.,c)=vector", v.size(), 1);
    int cols = hsize();
    it_vk it = begin() + c - 1;
    for (cit_vk it_v = v.begin(); it_v != v.end(); it += cols, it_v++)
      *it = *it_v;
  }

  Matrix<K>& diag(const Matrix<K>& m) //! change matrix into diagonal matrix made of diagonal of matrix
  {
    dimen_t cols(m.hsize()), i, j;
    cit_vk it_m = m.begin();
    it_vk it = begin();
    for (i = 0; i < std::min(cols, rows_); i++)
      {
        for (j = 0; j < i; it_m++, it++, j++) *it = K();
        *it = *it_m; // diagonal updated
        it_m++;
        it++;
        for (j = i + 1; j < cols; it_m++, it++, j++) *it = K();
      }
    for (i = cols; i < rows_; i++)    // for rows without diagonal entry
      for (j = 0; j < cols; it++, j++) *it = K();
    return *this;
  }

  Vector<K> diag() const //! return diagonal as a Vector
  {
    dimen_t cols(numberOfColumns());
    Vector<K> v(std::min(rows_, cols));
    cit_vk it_this = begin();
    for (it_vk it_v = v.begin(); it_v != v.end(); it_v++, it_this += cols + 1)
      *it_v = *it_this;
    return v;
  }

  Matrix<K>& diag(const K x) //! reset matrix as a diagonal matrix with constant diagonal entries
  {
    dimen_t cols = numberOfColumns();
    it_vk it;
    for (it = begin(); it != end(); it++) *it = 0.;
    it = begin();
    for (size_t r = 0; r < std::min(rows_, cols); r++, it += cols + 1)  *it = x;
    return *this;
  }

  Matrix<K>& diag(const std::vector<K>& v) //! reset matrix as a diagonal matrix with stl vector entries as diagonal entries
  {
    dimen_t cols(numberOfColumns());
    if (v.size() > std::min(rows_, cols)) mismatchDims("diag A(.)=vector", v.size(), 1);
    it_vk it;
    for (it = begin(); it != end(); it++) *it = K(0);
    it = begin();
    for (cit_vk it_v = v.begin(); it_v != v.end(); it += cols + 1, it_v++) *it = *it_v;
    return *this;
  }

  Matrix<K>& adddiag(const K x) //! add x to diagonal coefficients
  {
    dimen_t cols = numberOfColumns();
    it_vk it = begin();
    for (size_t r = 0; r < std::min(rows_, cols); r++, it += cols + 1) *it += x;
    return *this;
  }

  //------------------------------------------------------------------------------
  //        get sub matrix
  //------------------------------------------------------------------------------
  //! extract M[r1,r2],[c1,c2], indices start from 1
  Matrix<K> subMatrix(number_t r1, number_t r2, number_t c1, number_t c2) const
  {
    if (r1==0) r1=1;
    if (r2>rows_) r2=rows_;
    if (r2<r1) error("is_lesser", r2, r1);
    if (c1==0) c1=1;
    number_t nc=numberOfCols();
    if (c2>nc) c2=nc;
    if (c2<c1) error("is_lesser", c2, c1);
    Matrix<K> subm(r2-r1+1,c2-c1+1);
    typename std::vector<K>::iterator its=subm.begin();
    typename std::vector<K>::const_iterator it;
    for (number_t r=r1; r<=r2; ++r)
    {
      it = begin()+(r-1)*nc+c1-1;
      for (number_t c=c1; c<=c2; ++c, ++its, ++it) *its=*it;
    }
    return subm;
  }

  //------------------------------------------------------------------------------
  //     get lower or upper parts as new matrix
  //------------------------------------------------------------------------------
  Matrix<K> lower() const           //! lower part with matrix diag
  {
    dimen_t nbc = numberOfColumns(), nbr=rows_;
    Matrix<K> l(nbr,nbc,K(0));
    cit_vk itb = begin(), it;
    it_vk itlb = l.begin(), itl;
    for(dimen_t i=0; i<nbr; i++)
      {
        it=itb+(i*nbc);
        itl=itlb+(i*nbc);
        for(dimen_t j=0; j<=i; j++, ++itl,++it) *itl=*it;
      }
    return l;
  }

  Matrix<K> lower(const K& d) const //! lower part with diag equal to d
  {
    dimen_t nbc = numberOfColumns(), nbr=rows_;
    Matrix<K> l(nbr,nbc,K(0));
    cit_vk itb = begin(), it;
    it_vk itlb = l.begin(), itl;
    for(dimen_t i=0; i<nbr; i++)
      {
        it=itb+(i*nbc);
        itl=itlb+(i*nbc);
        for(dimen_t j=0; j<i; j++, ++itl,++it) *itl=*it;
        *itl = d;
      }
    return l;
  }

  Matrix<K> upper() const           //! upper part with matrix diag
  {
    dimen_t nbc = numberOfColumns(), nbr=rows_;
    Matrix<K> u(nbr,nbc,K(0));
    cit_vk itb = begin(), it;
    it_vk itub = u.begin(), itu;
    for(dimen_t i=0; i<nbr; i++)
      {
        it=itb+(i*nbc+i);
        itu=itub+(i*nbc+i);
        for(dimen_t j=i; j<nbc; j++, ++itu,++it) *itu=*it;
      }
    return u;

  }
  Matrix<K> upper(const K& d) const //! upper part with diag equal to d
  {
    dimen_t nbc = numberOfColumns(), nbr=rows_;
    Matrix<K> u(nbr,nbc,K(0));
    cit_vk itb = begin(), it;
    it_vk itub = u.begin(), itu;
    for(dimen_t i=0; i<nbr; i++)
      {
        it=itb+(i*nbc+i+1);
        itu=itub+i*nbc+i;
        *itu=d; ++itu;
        for(dimen_t j=i; j<nbc; j++, ++itu,++it) *itu=*it;
      }
    return u;
  }

  //------------------------------------------------------------------------------
  //     generalized access
  //------------------------------------------------------------------------------
  //! return matrix coefficients along rs and cs numbers (indices starts from 1)
  Matrix<K> get(const std::vector<dimen_t>& rs, const std::vector<dimen_t>& cs) const
  {
    if (rs.size()==0)
      {
        where("Matrix<K>::set(Vector<Dimen>, Vector<Dimen>)");
        error("is_void","rs");
      }
    if (cs.size()==0)
      {
        where("Matrix<K>::set(Vector<Dimen>, Vector<Dimen>)");
        error("is_void","cs");
      }
    dimen_t mx = *max_element(rs.begin(), rs.end()), nx = *max_element(cs.begin(), cs.end());
    dimen_t nbcols= size() / rows_;
    if (mx > rows_)
      {
        where("Matrix<K>::set(Vector<Dimen>, Vector<Dimen>)");
        error("index_out_of_range", "rs", 1, rows_);
      }
    if (nx > nbcols)
      {
        where("Matrix<K>::set(Vector<Dimen>, Vector<Dimen>)");
        error("index_out_of_range","cs", 1, nbcols);
      }
    mx = *min_element(rs.begin(), rs.end()); nx = *min_element(cs.begin(), cs.end());
    if (mx == 0)
      {
        where("Matrix<K>::set(Vector<Dimen>, Vector<Dimen>)");
        error("index_out_of_range", "rs", 1, rows_);
      }
    if (nx == 0)
      {
        where("Matrix<K>::set(Vector<Dimen>, Vector<Dimen>)");
        error("index_out_of_range","cs", 1, nbcols);
      }
    dimen_t m=dimen_t(cs.size()), n=dimen_t(cs.size());
    Matrix<K> mat(m,n);
    std::vector<dimen_t>::const_iterator it,jt;
    typename Matrix<K>::iterator itm=mat.begin();
    for (it=rs.begin(); it!=rs.end(); ++it)
      for (jt=cs.begin(); jt!=cs.end(); ++jt, ++itm)
        *itm = *(begin() + (*it - 1) * nbcols + *jt - 1);
    return mat;
  }

  //! return matrix coefficients along rs.first->rs.second and cs.first->cs.second numbers (indices starts from 1)
  Matrix<K> get(const std::pair<dimen_t,dimen_t>& rs, const std::pair<dimen_t, dimen_t>& cs) const
  {
    dimen_t nbcols= size() / rows_;
    if (rs.first==0)
      {
        where("Matrix<K>::get(const std::pair<Dimen,Dimen>&, const std::pair<Dimen,Dimen>&)");
        error("index_out_of_range","rs",1,rows_);
      }
    if (cs.first==0)
      {
        where("Matrix<K>::get(const std::pair<Dimen,Dimen>&, const std::pair<Dimen,Dimen>&)");
        error("index_out_of_range","cs", 1, nbcols);
      }
    if (rs.second > rows_)
      {
        where("Matrix<K>::get(const std::pair<Dimen,Dimen>&, const std::pair<Dimen,Dimen>&)");
        error("index_out_of_range","rs", 1, rows_);
      }
    if (cs.second > nbcols)
      {
        where("Matrix<K>::get(const std::pair<Dimen,Dimen>&, const std::pair<Dimen,Dimen>&)");
        error("index_out_of_range","cs", 1, nbcols);
      }
    if (rs.second < rs.first)
      {
        where("Matrix<K>::get(const std::pair<Dimen,Dimen>&, const std::pair<Dimen,Dimen>&)");
        error("is_lesser",rs.second, rs.first);
      }
    if (cs.second < cs.first)
      {
        where("Matrix<K>::get(const std::pair<Dimen,Dimen>&, const std::pair<Dimen,Dimen>&)");
        error("is_lesser",cs.second, cs.first);
      }
    dimen_t m=rs.second-rs.first+1, n=cs.second-cs.first+1;
    Matrix<K> mat(m,n);
    typename Matrix<K>::iterator itm=mat.begin();
    for (dimen_t r=rs.first; r<=rs.second; ++r)
      for (dimen_t c=cs.first; c<=cs.second; ++c, ++itm)
        *itm = *(begin() + (r - 1) * nbcols + c - 1);
    return mat;
  }

  //@{
  //! shortcuts to get routine
  Matrix<K> get(dimen_t r, const std::pair<dimen_t, dimen_t>& cs) const
    {return get(std::pair<dimen_t,dimen_t>(r,r),cs);}
  Matrix<K> get(const std::pair<dimen_t,dimen_t>& rs, dimen_t c) const
    {return get(rs,std::pair<dimen_t,dimen_t>(c,c));}
  Matrix<K> get(dimen_t r, const std::vector<dimen_t>& cs) const
    {return get(std::vector<dimen_t>(1,r),cs);}
  Matrix<K> get(const std::vector<dimen_t>& rs, dimen_t c) const
    {return get(rs,std::vector<dimen_t>(1,c));}
  Matrix<K> get(dimen_t r1, dimen_t r2, dimen_t c1, dimen_t c2) const
    {return get(std::pair<dimen_t,dimen_t>(r1,r2),std::pair<dimen_t,dimen_t>(c1,c2));}
  //@}

  //@{
  //! overload ()
  Matrix<K> operator()(const std::vector<dimen_t>& rs, const std::vector<dimen_t>& cs) const
    {return get(rs,cs);}
  Matrix<K> operator()(const std::pair<dimen_t,dimen_t>& rs, const std::pair<dimen_t, dimen_t>& cs) const
    {return get(rs,cs);}
  Matrix<K> operator()(dimen_t r, const std::pair<dimen_t, dimen_t>& cs) const
    {return get(std::pair<dimen_t,dimen_t>(r,r),cs);}
  Matrix<K> operator()(const std::pair<dimen_t,dimen_t>& rs, dimen_t c) const
    {return get(rs,std::pair<dimen_t,dimen_t>(c,c));}
  Matrix<K> operator()(dimen_t r, const std::vector<dimen_t>& cs) const
    {return get(std::vector<dimen_t>(1,r),cs);}
  Matrix<K> operator()(const std::vector<dimen_t>& rs, dimen_t c) const
    {return get(rs,std::vector<dimen_t>(1,c));}
  Matrix<K> operator()(dimen_t r1, dimen_t r2, dimen_t c1, dimen_t c2) const
    {return get(std::pair<dimen_t,dimen_t>(r1,r2),std::pair<dimen_t,dimen_t>(c1,c2));}
  //@}

  /*!
    set matrix coefficients along rs and cs numbers (indices starts from 1)
    rs and cs numbers has to be in current matrix index bounds
    matrix is given as a vector with row storage
  */
  void set(const std::vector<dimen_t>& rs, const std::vector<dimen_t>& cs, const std::vector<K>& mat)
  {
    if (rs.size()==0)
      {
        where("Matrix<K>::set(const std::vector<Dimen>&, const std::vector<Dimen>&, const Vector<K>&)");
        error("is_void","rs");
      }
    if (cs.size()==0)
      {
        where("Matrix<K>::set(const std::vector<Dimen>&, const std::vector<Dimen>&, const Vector<K>&)");
        error("is_void","cs");
      }
    dimen_t mx = *max_element(rs.begin(), rs.end()), nx = *max_element(cs.begin(), cs.end());
    dimen_t nbcols= size() / rows_;
    if (mx > rows_)
      {
        where("Matrix<K>::set(const std::vector<Dimen>&, const std::vector<Dimen>&, const Vector<K>&)");
        error("index_out_of_range","rs", 1, rows_);
      }
    if (nx > nbcols)
      {
        where("Matrix<K>::set(const std::vector<Dimen>&, const std::vector<Dimen>&, const Vector<K>&)");
        error("index_out_of_range","cs", 1, nbcols);
      }
    mx = *min_element(rs.begin(), rs.end()); nx = *min_element(cs.begin(), cs.end());
    if (mx == 0)
      {
        where("Matrix<K>::set(const std::vector<Dimen>&, const std::vector<Dimen>&, const Vector<K>&)");
        error("index_out_of_range","rs", 1, rows_);
      }
    if (nx == 0)
      {
        where("Matrix<K>::set(const std::vector<Dimen>&, const std::vector<Dimen>&, const Vector<K>&)");
        error("index_out_of_range","cs", 1, nbcols);
      }

    std::vector<dimen_t>::const_iterator it,jt;
    typename std::vector<K>::const_iterator itm=mat.begin();
    for (it=rs.begin(); it!=rs.end(); ++it)
      for (jt=cs.begin(); jt!=cs.end(); ++jt, ++itm)
        *(begin() + (*it - 1) * nbcols + *jt - 1) = *itm;
  }

  /*!set matrix coefficients along rs.first->rs.second and cs.first->cs.second numbers (indices starts from 1)
     rs and cs numbers has to be in current matrix index bounds
     matrix is given as a vector with row storage
  */
  void set(const std::pair<dimen_t,dimen_t>& rs, const std::pair<dimen_t, dimen_t>& cs, const std::vector<K>& mat)
  {
    dimen_t nbcols= size() / rows_;
    if (rs.first == 0)
      {
        where("Matrix<K>::set(const std::pair<Dimen,Dimen>&, const std::pair<Dimen, Dimen>&, const Vector<K>&)");
        error("index_out_of_range","rs", 1, rows_);
      }
    if (cs.first == 0)
      {
        where("Matrix<K>::set(const std::pair<Dimen,Dimen>&, const std::pair<Dimen, Dimen>&, const Vector<K>&)");
        error("index_out_of_range","cs", 1, nbcols);
      }
    if (rs.second > rows_)
      {
        where("Matrix<K>::set(const std::pair<Dimen,Dimen>&, const std::pair<Dimen, Dimen>&, const Vector<K>&)");
        error("index_out_of_range","rs", 1, rows_);
      }
    if (cs.second > nbcols)
      {
        where("Matrix<K>::set(const std::pair<Dimen,Dimen>&, const std::pair<Dimen, Dimen>&, const Vector<K>&)");
        error("index_out_of_range","cs", 1, nbcols);
      }
    if (rs.second < rs.first)
      {
        where("Matrix<K>::set(const std::pair<Dimen,Dimen>&, const std::pair<Dimen, Dimen>&, const Vector<K>&)");
        error("is_lesser",rs.second, rs.first);
      }
    if (cs.second < cs.first)
      {
        where("Matrix<K>::set(const std::pair<Dimen,Dimen>&, const std::pair<Dimen, Dimen>&, const Vector<K>&)");
        error("is_lesser",cs.second,cs.first);
      }
    typename std::vector<K>::const_iterator itm=mat.begin();
    for (dimen_t r=rs.first; r<=rs.second; ++r)
      for (dimen_t c=cs.first; c<=cs.second; ++c, ++itm)
        *(begin() + (r - 1) * nbcols + c - 1) = *itm;
  }

  //@{
  //! shorcuts to set routine
  void set(dimen_t r, const std::pair<dimen_t, dimen_t>& cs, const std::vector<K>& mat)
    {set(std::pair<dimen_t,dimen_t>(r,r),cs, mat);}
  void set(const std::pair<dimen_t,dimen_t>& rs, dimen_t c, const std::vector<K>& mat)
    {set(rs,std::pair<dimen_t,dimen_t>(c,c), mat);}
  void set(dimen_t r, const std::vector<dimen_t>& cs, const std::vector<K>& mat)
    {set(std::vector<dimen_t>(1,r),cs, mat);}
  void set(const std::vector<dimen_t>& rs, dimen_t c, const std::vector<K>& mat)
    {set(rs,std::vector<dimen_t>(1,c), mat);}
  void set(dimen_t r1, dimen_t r2, dimen_t c1, dimen_t c2, const std::vector<K>& mat)
    {set(std::pair<dimen_t,dimen_t>(r1,r2),std::pair<dimen_t,dimen_t>(c1,c2), mat);}
  //@}

  //------------------------------------------------------------------------------
  // get value type and structure type of matrix using template specialization
  // only main types are handled real/complex value and scalar/matrix/vector structure
  //------------------------------------------------------------------------------
  //! get value type
  ValueType valueType() const { return _real;}
  //! get structure type
  StrucType strucType() const { return _scalar;}
  //! return size of element of a matrix of matrices or a matrix of vectors (size of first block!)
  std::pair<number_t,number_t> elementSize() const { return std::make_pair(1,1);} //default is for scalar matrix

  //------------------------------------------------------------------------------
  // structure stuff and transposition operations
  //------------------------------------------------------------------------------

  template<typename KK>
  bool shareDims(const Matrix<KK>& m) const //! true if matrices bear same dimensions
  {
    return (size() == m.size() && vsize() == m.vsize());
  }

  bool isSymmetric() const //! symmetric matrix test
  {
    if (rows_ != hsize()) return false;
    cit_vk it_this = begin(), it_c = it_this, it_rc, it_cr;
    for (dimen_t r = 0; r < rows_; r++, it_c++, it_this += rows_)
      {
        it_rc = it_this;
        it_cr = it_c;
        for (dimen_t c = 0; c <= r; c++, it_rc++, it_cr += rows_)
          if ((*it_cr) != (*it_rc)) return false;
      }
    return true;
  }

  bool isSkewSymmetric() const //! skew-symmetric matrix test
  {
    if (rows_ != hsize()) return false;
    cit_vk it_this = begin(), it_c = it_this, it_rc, it_cr;
    for (dimen_t r = 0; r < rows_; r++, it_c++, it_this += rows_)
      {
        it_rc = it_this;
        it_cr = it_c;
        for (dimen_t c = 0; c <= r; c++, it_rc++, it_cr += rows_)
          if ((*it_cr) != -(*it_rc)) return false;
      }
    return true;
  }

  bool isSelfAdjoint() const //! self adjoint matrix test
  {
    if (rows_ != hsize()) return false;
    cit_vk it_this = begin(), it_c = it_this, it_rc, it_cr;
    for (dimen_t r = 0; r < rows_; r++, it_c++, it_this += rows_)
      {
        it_rc = it_this;
        it_cr = it_c;
        for (dimen_t c = 0; c <= r; c++, it_rc++, it_cr += rows_)
          if ((*it_cr) != conj(*it_rc)) return false;
      }
    return true;
  }

  bool isSkewAdjoint() const //! skew adjoint matrix test
  {
    if (rows_ != hsize()) return false;
    cit_vk it_this = begin(), it_c = it_this, it_rc, it_cr;
    for (dimen_t r = 0; r < this->rows_; r++, it_c++, it_this += this->rows_)
      {
        it_rc = it_this;
        it_cr = it_c;
        for (dimen_t c = 0; c <= r; c++, it_rc++, it_cr += rows_)
          if ((*it_cr) != -conj(*it_rc)) return false;
      }
    return true;
  }

  Matrix<K>& transpose() //! matrix self-transpostion
  {
    if (rows_ != hsize()) nonSquare("transpose", rows_, hsize());
    it_vk it = begin(), c1 = it, rc, cr;
    K sw(0.);
    for (dimen_t r = 0; r < rows_ - 1; it += rows_, c1++, r++)
      {
        rc = it + r + 1;
        cr = c1 + (r + 1) * rows_;
        for (dimen_t c = r + 1; c < rows_; cr += rows_, rc++, c++)
          {
            sw = *rc;
            *rc = *cr;
            *cr = sw;
          }
      }
    return *this;
  }

  Matrix<K>& adjoint() //! matrix self-adjoint
  {
    if (rows_ != hsize()) nonSquare("adjoint", rows_, hsize());
    it_vk it = begin(), c1 = it, rc, cr;
    K sw(0.);
    for (dimen_t r = 0; r < rows_; it += rows_, c1++, r++)
      {
        rc = it + r;
        *rc = conj(*rc); // diagonal entry
        rc++;
        cr = c1 + (r + 1) * rows_;
        for (dimen_t c = r + 1; c < rows_; cr += rows_, rc++, c++)
          {
            sw = *rc;
            *rc = conj(*cr);
            *cr = conj(sw);
          }
      }
    return *this;
  }

  Matrix<K>& conjugate()  //! to its conjugate (assuming conj(real)->real)
    {
      for(it_vk it = begin(); it != end(); it++) *it = conj(*it);
      return *this;
    }

  Matrix<K>& roundToZero(real_t aszero=10*theEpsilon) //! round to 0 all coefficients smaller (in norm) than a aszero, storage is not modified
  {
    for (it_vk it = begin(); it != end(); it++)
      if (norm(*it) < aszero) *it=K(0);
    return *this;
  }

  //------------------------------------------------------------------------------
  // member operators
  //------------------------------------------------------------------------------

  template<typename KK>
  Matrix<K>& operator+=(const Matrix<KK>& b) //! accumulated matrix addition A+=B
  {
    if (!shareDims(b)) mismatchDims("A+=B", b.vsize(), b.hsize());
    typename Matrix<KK>::const_iterator it_b = b.begin();
    for (it_vk it = begin(); it < end(); it++, it_b++) *it += *it_b;
    return *this;
  }

  Matrix<K>& operator+=(const K& x) //! addition to scalar
  {
    for (it_vk it = begin(); it != end(); it++) *it += x;
    return *this;
  }

  template<typename KK>
  Matrix<K>& operator-=(const Matrix<KK>& b) //! accumulated matrix subtraction (with cast)
  {
    if (!shareDims(b)) mismatchDims("A-=B", b.vsize(), b.hsize());
    typename Matrix<KK>::const_iterator it_b = b.begin();
    for (it_vk it = begin(); it != end(); it++, it_b++) *it -= *it_b;
    return *this;
  }

  Matrix<K>& operator-=(const K& x) //! subtract a scalar
  {
    for (it_vk it = begin(); it < end(); it++) *it -= x;
    return *this;
  }

  template<typename KK>
  Matrix<K>& operator*=(const KK& x) //! overloaded *= multiply by a scalar
  {
    for (it_vk it = begin(); it < end(); it++) *it *= x;
    return *this;
  }

  template<typename KK>
  Matrix<K>& operator/=(const KK& x) //! overloaded /= divide by scalar
  {
    if (std::abs(x) < theEpsilon) { divideByZero("A/=x"); }
    return (*this) *= (1. / x);
  }

  Matrix<real_t> real() const //! real part of a (complex) matrix
  {
    Matrix<real_t> r(vsize(), hsize(), real_t(0.));
    cit_vk it_this(begin());
    std::vector<real_t>::iterator it_r;
    for (it_r = r.begin(); it_r < r.end(); it_r++, it_this++)
      *it_r = realPart(*it_this);
    return r;
  }

  Matrix<real_t> imag() const //! imaginary part of a (complex) matrix
  {
    Matrix<real_t> r(vsize(), hsize(), real_t(0.));
    cit_vk it_this(begin());
    std::vector<real_t>::iterator it_r;
    for (it_r = r.begin(); it_r < r.end(); it_r++, it_this++)
      *it_r = imagPart(*it_this);
    return r;
  }

  //------------------------------------------------------------------------------
  // product by a diagonal matrix stored as std::vector
  //------------------------------------------------------------------------------

  Matrix<K>& diagProduct(const std::vector<K>& diag)  //! product by a diagonal matrix M = diag*M
  {
    number_t nc=numberOfCols(), nd=diag.size();
    if(rows_!=nd) mismatchDims("diagProduct",nd,nd);
    it_vk itm = begin();
    typename std::vector<K>::const_iterator itd=diag.begin();
    for(number_t i=0; i<rows_; ++i, ++itd)
      for(number_t j=0; j<nc; ++j, ++itm) *itm *= *itd;
    return *this;
  }

  Matrix<K>& productDiag(std::vector<K> diag)  //! product by a diagonal matrix M = M*diag
  {
    number_t nc=numberOfCols(), nd=diag.size();
    if(nc!=nd) mismatchDims("diagProduct",nd,nd);
    it_vk itm = begin();
    typename std::vector<K>::const_iterator itd;
    for(number_t i=0; i<rows_; ++i) {
        itd = diag.begin();
        for(number_t j=0; j<nc; ++j, ++itm, ++itd) *itm *= *itd;
    }
    return *this;
  }

  real_t squaredNorm() const //! return squared norm of the matrix
  {
    real_t n=0.;
    cit_vk itm = begin();
    for(; itm!=end(); itm++) n += norm2(*itm) * norm2(*itm);
    return n;
  }

  // special product by a row dense matrix passed by pointer, result R is a row dense matrix
  // matrix has to be allocated, no check here
  void multMatrixRow(K* mat, K* R, number_t p) const    //! R = M * mat
  {
      number_t n=numberOfCols();
      for(number_t i=0; i<rows_; ++i)
        for(number_t j=0; j<p; ++j, ++R)
        {
          *R*=0.;
          cit_vk itm = begin()+i*n;
          K* pM = mat+j;
          for(number_t k=0; k<n; ++k, ++itm, pM+=p) *R+= *itm**pM;
        }
  }
  void multLeftMatrixRow(K* mat, K* R, number_t p) const //! R = mat * M
  {
      number_t n=numberOfCols();
      for(number_t i=0; i<p; ++i)
        for(number_t j=0; j<n; ++j, ++R)
        {
          *R*=0.;
          cit_vk itm = begin()+j;
          K* pM = mat+i*rows_;
          for(number_t k=0; k<rows_; ++k, itm+=n, pM++) *R+= *pM **itm;
        }

  }

  //special product by a col dense matrix passed by pointer, result R is a col dense matrix
  // matrix has to be allocated, no check here
  void multMatrixCol(K* mat, K* R, number_t p) const    //! R = M * mat
  {
      number_t n=numberOfCols();
      for(number_t j=0; j<p; ++j)
        for(number_t i=0; i<rows_; ++i, ++R)

        {
          *R*=0.;
          cit_vk itm = begin()+i*n;
          K* pM = mat+j*n;
          for(number_t k=0; k<n; ++k, ++itm, ++pM) *R+= *itm**pM;
        }

  }
  void multLeftMatrixCol(K* mat, K* R, number_t p) const //! R = mat * M
  {
      number_t n=numberOfCols();
      for(number_t j=0; j<n; ++j)
        for(number_t i=0; i<p; ++i, ++R)

        {
          *R*=0.;
          cit_vk itm = begin()+j;
          K* pM = mat+i;
          for(number_t k=0; k<rows_; ++k, itm+=n, pM+=p) *R+= *pM **itm;
        }
  }

  //------------------------------------------------------------------------------
  // warning and error handlers
  //------------------------------------------------------------------------------

  void nonSquare(const string_t& s, const size_t r, const size_t c) const //! error: operation forbidden for non square matrix
  { error("mat_nonsquare",s,r,c);}

  void mismatchDims(const string_t& s, const size_t r, const size_t c) const //! error: in matrix operation s dimensions disagree dim(op1)= ... dim(op2)=...
  { error("mat_mismatch_dims", s, rows_, hsize(),r,c);}

  void divideByZero(const string_t& s) const //! error: divide by 0 in matrix operation s matrix dimension dim(op1)
  { error("mat/0", s, rows_, hsize());}

  void isSingular() const //! error: non invertible matrix
  { error("mat_noinvert", rows_, hsize());}

  void complexCastWarning(const string_t& s, const size_t r, const size_t c) const //! warning: try to cast a complex to a real
  { error("mat_cast", s, rows_,hsize(), r, c);}

  //-----------------------------------------------------------------------------
  // I/O utilities
  //-----------------------------------------------------------------------------
  /*!
   load matrix from file
   read from file a dense matrix, formated as:
   \verbatim
   nbRows nb_Cols
   a11 a12 ........
   a21 a22 ........
   ....
   \endverbatim
   */
  void loadFromFile(const char* f)
  {
    trace_p->push("Matrix::loadFromFile");
    std::ifstream in(f);
    if (!in.is_open())    // file is not found
      {
        error("mat_badfile", f);
      }
    std::vector<K>::clear(); // reset matrix
    int cols;
    in >> rows_ >> cols;
    std::vector<K>::resize(rows_ * cols);
    int r = 0, c = 0;

    for (it_vk it = begin(); it < end() ; it++, c++)
      {
        if (in.eof())    // error end of file reached
          {
            in.close();
            error("mat_badeof", f, rows_, cols, r, c);
          }
        in >> (*it);
        if (c == rows_)
          {
            c = 0;
            r++;
          }
      }
    in.close();
    trace_p->pop();
  }

  void saveToFile(const char* f) //! save matrix to file
  {
    trace_p->push("Matrix::saveToFile");
    std::ofstream out(f);
    if (! out.is_open())    // error when opening file
      {
        error("mat_badfile", f);
      }
    int cols = hsize();
    out << rows_ << " " << cols << std::endl;
    out.precision(fullPrec);
    cit_vk it = begin();
    for (dimen_t r = 0 ; r < rows_; r++)
      {
        for (dimen_t c = 0; c < cols; c++, it++) { out << (*it) << " "; }
        out << std::endl;
      }
    out.close();
    trace_p->pop();
  }

  friend std::istream& operator>>(std::istream& is, Matrix<K>& m) //! flux extraction(read)
  {
    m.clear(); // reset matrix
    int cols;
    // read first number of rows and columns, then matrix entries row-wise
    is >> m.rows_ >> cols;
    m.resize(m.rows_ * cols);
    for (it_vk it = m.begin(); it != m.end(); it++) is >> (*it);
    return is;
  }

  // specialization for real and complex matrices
  friend std::ostream& operator<<(std::ostream& os, const Matrix<real_t>& m);
  friend std::ostream& operator<<(std::ostream& os, const Matrix<complex_t>& m);

  void print(std::ofstream& os) const { os << *this; } //!< write on a file stream
  void print() const { thePrintStream << *this; } //!< write on default file stream

}; // end of class Matrix

std::ostream& operator<<(std::ostream& os, const Matrix<real_t>& m);    //!< flux insertion (write)
std::ostream& operator<<(std::ostream& os, const Matrix<complex_t>& m); //!< flux insertion (write)

//-----------------------------------------------------------------------------
// type of matrix as string
//-----------------------------------------------------------------------------
template <> inline string_t Matrix<real_t>::typeStr() const  { return "real"; }
template <> inline string_t Matrix<complex_t>::typeStr() const  { return "complex"; }
template <> inline string_t Matrix<Matrix<real_t> >:: typeStr() const  { return "real matrix"; }
template <> inline string_t Matrix<Matrix<complex_t>  >::typeStr() const  { return "complex matrix"; }

//-----------------------------------------------------------------------------
// specialized template operations
//-----------------------------------------------------------------------------
template <typename K>
inline Matrix<real_t> abs(const Matrix<K>& mat)            //! abs of a matrix
{
  Matrix<real_t> r(mat.vsize(), mat.hsize());
  typename Matrix<K>::const_iterator itm=mat.begin();
  std::vector<real_t>::iterator itr;
  for (itr = r.begin(); itr < r.end(); ++itr, ++itm) { *itr = std::abs(*itm); }
  return r;
}

template <typename K>
inline Matrix<Matrix<real_t> > abs(const Matrix<Matrix<K> >& mat)     //! abs of a matrix of matrices
{
  Matrix<Matrix<real_t> > r(mat.vsize(), mat.hsize());
  typename Matrix<Matrix<K> >::const_iterator itm=mat.begin();
  std::vector<Matrix<real_t> >::iterator itr;
  for (itr = r.begin(); itr < r.end(); ++itr, ++itm) { *itr = std::abs(*itm); }
  return r;
}

//-----------------------------------------------------------------------------
// non-member operations
//-----------------------------------------------------------------------------
template<typename K>
bool operator==(const Matrix<K>& a, const Matrix<K>& b) //! matrix comparison (element by element)
{
  if (&a == &b) return true; // same Matrix adresses
  if (!a.shareDims(b)) return false;
  if (&a == &b) return true;
  typename Matrix<K>::cit_vk it_a = a.begin(), it_b = b.begin();
  for (it_a = a.begin(); it_a != a.end(); it_a++, it_b++)
    if (*it_a != *it_b) return false;
  return true;
}

template<typename K>
bool operator!=(const Matrix<K>& a, const Matrix<K>& b) //! matrix comparison (element by element)
{
  return !(a == b);
}

template<typename K>
Matrix<K> operator+(const Matrix<K>& m)  //! unary operator+
{
  return m;
}

template<typename K>
Matrix<K> operator-(const Matrix<K>& m)  //! unary operator- (negation operator)
{
  Matrix<K> r(m);
  typename Matrix<K>::it_vk it;
  for (it = r.begin(); it < r.end(); it++) *it = -*it;
  return r;
}

template<typename M_it, typename MM_it>
void transpose(const dimen_t nbr, const dimen_t nbc, M_it it_m1b, MM_it it_m2b)
{
  MM_it it_rRc = it_m2b;
  M_it it_bC = it_m1b;
  for (dimen_t c = 0; c < nbc; c++, it_bC++)
    {
      M_it it_bCr = it_bC;
      for (dimen_t r = 0; r < nbr; r++, it_bCr += nbc, it_rRc++) *it_rRc = *it_bCr;
    }
}

template<typename M_it, typename MM_it>
void adjoint(const dimen_t nbr, const dimen_t nbc, M_it it_m1b, MM_it it_m2b)
{
  MM_it it_rRc = it_m2b;
  M_it it_bC = it_m1b;
  for (dimen_t c = 0; c < nbc; c++, it_bC++)
    {
      M_it it_bCr = it_bC;
      for (dimen_t r = 0; r < nbr; r++, it_bCr += nbc, it_rRc++) *it_rRc = conj(*it_bCr);
    }
}

template<typename M_it, typename V1_it, typename V2_it>
void matvec(M_it it_mb, const V1_it it_v1b, const V1_it it_v1e, V2_it it_v2b, V2_it it_v2e)
{
  // LO AND BEHOLD! No compatibility check here neither on operand type nor size
  M_it it_m1 = it_mb;
  typename std::iterator_traits<V1_it>::difference_type incr = it_v1e - it_v1b;
  for (V2_it it_v2 = it_v2b; it_v2 != it_v2e; it_v2++, it_m1 += incr)
    {
      *it_v2 = 0.;
      *it_v2 = std::inner_product(it_v1b, it_v1e, it_m1, *it_v2);
    }
}

template<typename K, typename V1_it, typename V2_it>
V2_it matvec(const Matrix<K>& m, const V1_it it_v1b, const V2_it it_v2b)
{
  // LO AND BEHOLD! No check on types or size of operand vector and result vector
  matvec(m.begin(), it_v1b, it_v1b + m.hsize(), it_v2b, it_v2b + m.vsize());
  return it_v2b + m.vsize();
}

template<typename M_it, typename V1_it, typename V2_it>
void vecmat(M_it it_mb, const V1_it it_v1b, const V1_it it_v1e, V2_it it_v2b, V2_it it_v2e)
{
  // LO AND BEHOLD! No compatibility check here neither on operand type nor size
  M_it it_mj(it_mb), it_mij;
  typename std::iterator_traits<V1_it>::difference_type incr(it_v2e - it_v2b);

  for (V2_it it_v2 = it_v2b; it_v2 != it_v2e; it_v2++, it_mj++)
    {
      *it_v2 = 0.;
      it_mij = it_mj;
      for (V1_it it_v1 = it_v1b; it_v1 != it_v1e; it_v1++, it_mij += incr)
        *it_v2 += *it_v1 * *it_mij;
    }
}

template<typename K, typename V1_it, typename V2_it>
V2_it vecmat(const Matrix<K>& m, const V1_it it_v1b, const V2_it it_v2b)
{
  // No check on types or size of operand vector and result vector
  vecmat(m.begin(), it_v1b, it_v1b + m.vsize(), it_v2b, it_v2b + m.hsize());
  return it_v2b + m.hsize();
}

//template<typename A_it, typename B_it, typename R_it>
//void matmat(A_it it_ma, const dimen_t nbk, B_it it_mb, const dimen_t nbr, const dimen_t nbc, R_it it_mr)
//{
//  // No compatibility check here neither on operand type nor size
//  R_it it_rRc = it_mr;
//  A_it it_aR = it_ma;
//  B_it it_b = it_mb;
//  for (dimen_t r = 0; r < nbr; it_aR += nbk, r++) // loop on result (m) rows
//  {
//    B_it it_bC = it_b;
//    for (dimen_t c = 0; c < nbc; it_rRc++, it_bC++, c++) // loop on result (m) columns
//    {
//      A_it it_aRk = it_aR; // pointer to row of A and column of B
//      B_it it_bKc = it_bC;
//      for (dimen_t k = 0; k < nbk; it_aRk++, it_bKc += nbc, k++) // advance on A row entries and on B column entries
//          *it_rRc += *it_aRk * *it_bKc;
//    }
//  }
//}

//new version, with no iterator outbounds
template<typename A_it, typename B_it, typename R_it>
void matmat(A_it it_ma, const dimen_t nbk, B_it it_mb, const dimen_t nbr, const dimen_t nbc, R_it it_mr)
{
  // No compatibility check here neither on operand type nor size
  R_it it_rRc = it_mr;
  A_it it_aR = it_ma;
  B_it it_b = it_mb;
  for (dimen_t r = 0; r < nbr;  r++) // loop on result (m) rows
    {
      B_it it_bC = it_b;
      for (dimen_t c = 0; c < nbc; it_rRc++, it_bC++, c++) // loop on result (m) columns
        {
          A_it it_aRk = it_aR; // pointer to row of A and column of B
          B_it it_bKc = it_bC;
          for (dimen_t k = 0; k < nbk; it_aRk++,  k++) // advance on A row entries and on B column entries
            {
              *it_rRc += *it_aRk * *it_bKc;
              if (k < nbk - 1) it_bKc += nbc;
            }
        }
      if (r < nbr - 1) it_aR += nbk;
    }
}

//-----------------------------------------------------------------------------
// template matrix elementary operations
//-----------------------------------------------------------------------------
template<typename K>
Matrix<K> operator+(const Matrix<K>& a, const Matrix<K>& b) //! sum of two matrices
{
  if (!a.shareDims(b)) a.mismatchDims("A+B", b.vsize(), b.hsize());
  Matrix<K> r(a);
  typename Matrix<K>::cit_vk it_b = b.begin();
  typename Matrix<K>::it_vk it_r;
  for (it_r = r.begin(); it_r < r.end(); it_r++, it_b++) *it_r += *it_b;
  return r;
}
Matrix<complex_t> operator+(const Matrix<real_t>&, const Matrix<complex_t>&); //!< real matrix A + complex matrix B
Matrix<complex_t> operator+(const Matrix<complex_t>&, const Matrix<real_t>&); //!< complex matrix A + real matrix B

template<typename K, typename KK>
Matrix<K> operator+(const KK& x, const Matrix<K>& a) //! scalar x + matrix A
{
  Matrix<K> r(a);
  typename Matrix<K>::it_vk it;
  for (it = r.begin(); it < r.end(); it++) *it += x;
  return r;
}
Matrix<complex_t> operator+(const complex_t&, const Matrix<real_t>&); //!< complex x + real matrix A

template<typename K, typename KK>
Matrix<K> operator+(const Matrix<K>& a, const KK& x) //! matrix A + scalar x
{
  Matrix<K> r(a);
  typename Matrix<K>::it_vk it;
  for (it = r.begin(); it < r.end(); it++) *it += x;
  return r;
}
Matrix<complex_t> operator+(const Matrix<real_t>&, const complex_t&); //!< real matrix A + complex x

template<typename K>
Matrix<K> operator-(const Matrix<K>& a, const Matrix<K>& b) //! matrix A - matrix B
{
  if (!a.shareDims(b)) a.mismatchDims("A-B", b.vsize(), b.hsize());
  Matrix<K> r(a);
  typename Matrix<K>::cit_vk it_b = b.begin();
  typename Matrix<K>::it_vk it_r;
  for (it_r = r.begin(); it_r < r.end(); it_r++, it_b++) *it_r -= *it_b;
  return r;
}
Matrix<complex_t> operator-(const Matrix<real_t>&, const Matrix<complex_t>&); //!< real matrix A - complex matrix B
Matrix<complex_t> operator-(const Matrix<complex_t>&, const Matrix<real_t>&); //!< complex matrix A - real matrix B


template<typename K, typename KK>
Matrix<K> operator-(const KK& x, const Matrix<K>& a) //! scalar x - matrix A
{
  Matrix<K> r(a);
  typename Matrix<K>::cit_vk it_a = a.begin();
  typename Matrix<K>::it_vk it_r;
  for (it_r = r.begin(); it_r < r.end(); it_r++, it_a++) *it_r = x - *it_a;
  return r;
}
Matrix<complex_t> operator-(const complex_t&, const Matrix<real_t>&); //!< complex x - real matrix A

template<typename K, typename KK>
Matrix<K> operator-(const Matrix<K>& a, const KK& x) //! matrix A - scalar x
{
  Matrix<K> r(a);
  typename Matrix<K>::cit_vk it_a = a.begin();
  typename Matrix<K>::it_vk it_r;
  for (it_r = r.begin(); it_r < r.end(); it_r++, it_a++) *it_r = x - *it_a;
  return r;
}
Matrix<complex_t> operator-(const Matrix<real_t>&, const complex_t&); //!< real matrix A - complex x

template<typename K, typename KK>
Matrix<K> operator*(const KK& x, const Matrix<K>& a) //! scalar x * matrix A
{
  Matrix<K> r(a);
  typename Matrix<K>::it_vk it;
  for (it = r.begin(); it < r.end(); it++) *it *= x;
  return r;
}

// define here to solve a template problem
template<typename K>
Vector<Matrix<K> > operator*(const K& x, const Vector<Matrix<K> >& a) //! scalar x * vector of matrix
{
  Vector<Matrix<K> > r(a);
  typename Vector<Matrix<K> >::it_vk it;
  for (it = r.begin(); it < r.end(); it++) *it *= x;
  return r;
}

inline
Vector<Matrix<complex_t> > operator*(const real_t& x, const Vector<Matrix<complex_t> >& a) //! scalar x * vector of matrix
{return complex_t(x)*a;}


Matrix<complex_t> operator*(const complex_t& x, const Matrix<real_t>& a); //!< complex x * real matrix A

//template<typename K, typename KK>
//Matrix<K> operator*(const Matrix<K>& a, const KK& x) //! matrix A * scalar x
//{
//  return x * a;
//}
template<typename K>
Matrix<K> operator*(const Matrix<K>& a, const K& x) //! matrix A * scalar x
{
  return x * a;
}

Matrix<complex_t> operator*(const Matrix<real_t>& A, const complex_t& x); //!< real matrix A * complex x
//Matrix<complex_t> operator*(const Matrix<complex_t>& A, const real_t& x); //!< complex matrix A * real x

template<typename K, typename KK>
Matrix<K> operator/(const Matrix<K>& a, const KK& x) //! matrix A / scalar x
{
  if (std::abs(x) < theEpsilon) { a.divideByZero("A/=x"); }
  return (1. / x) * a;
}
Matrix<complex_t> operator/(const Matrix<real_t>&, const complex_t&); //!< real matrix A / complex x

//-----------------------------------------------------------------------------
// matrix vector multiplications
//-----------------------------------------------------------------------------
template<typename K, typename V>
Vector<K> operator*(const Matrix<K>& m, const Vector<V>& v) //! matrix x vector (template)
{
  if (m.hsize() != v.size()) m.mismatchDims("M*V", v.size(), 1);
  Vector<K> res(m.vsize()); // create result vector
  matvec(m.begin(), v.begin(), v.end(), res.begin(), res.end());
  return res;
}
Vector<complex_t> operator*(const Matrix<real_t>&, const Vector<complex_t>&); //!< real matrix x complex vector
Vector<complex_t> operator*(const Matrix<complex_t>&, const Vector<real_t>&); //!< complex matrix x real vector


template<typename K, typename V>
Vector<K> operator*(const Vector<V>& v, const Matrix<K>& m) //! vector x matrix (template)
{
  if (m.vsize() != v.size()) m.mismatchDims("V*M", v.size(), 1);
  Vector<K> res(m.hsize());
  vecmat(m.begin(), v.begin(), v.end(), res.begin(), res.end());
  return res;
}
Vector<complex_t> operator*(const Vector<complex_t>&, const Matrix<real_t>&); //!< complex vector x real matrix
Vector<complex_t> operator*(const Vector<real_t>&, const Matrix<complex_t>&); //!< real vector x complex matrix

// compatibility with solver objects
template <typename K>
void multMatrixVector(const Matrix<K>& m, const Vector<K>& v, Vector<K>& mv)
{
    mv.resize(m.vsize());
    matvec(m.begin(), v.begin(), v.end(), mv.begin(), mv.end());
}

template <typename K>
void multVectorMatrix(const Vector<K>& v, const Matrix<K>& m, Vector<K>& mv)
{
    mv.resize(m.hsize());
    vecmat(m.begin(), v.begin(), v.end(), mv.begin(), mv.end());
}

//-----------------------------------------------------------------------------
// matrix x matrix multiplication
//-----------------------------------------------------------------------------
template<typename K>
Matrix<K> operator*(const Matrix<K>& a, const Matrix<K>& b) //! matrix A * matrix B
{
  dimen_t nbr = a.vsize();
  dimen_t nbc = b.hsize();
  dimen_t nbk = a.hsize();
  if (nbk != b.vsize()) a.mismatchDims("A*B", b.vsize(), nbc);
  Matrix<K> r(nbr, nbc, K(0.));
  matmat(a.begin(), nbk, b.begin(), nbr, nbc, r.begin());
  return r;
}
Matrix<complex_t> operator*(const Matrix<real_t>&, const Matrix<complex_t>&); //!< real matrix A x complex matrix B
Matrix<complex_t> operator*(const Matrix<complex_t>&, const Matrix<real_t>&); //!< complex matrix A x real matrix B

//-----------------------------------------------------------------------------
// extern diag, transpose and adjoint operation
//-----------------------------------------------------------------------------
template<typename K>
Matrix<K> transpose(const Matrix<K>& kB) //! transpose matrix
{
  dimen_t nbc = kB.hsize(), nbr = kB.vsize();
  Matrix<K> r(nbc, nbr, K(0.));
  transpose(nbr, nbc, kB.begin(), r.begin());
  return r;
}

template<typename K>
Matrix<K> tran(const Matrix<K>& kB) //! transpose matrix
{
  return transpose(kB);
}

Matrix<real_t> adjoint(const Matrix<real_t>&);        //!< adjoint (transpose) real matrix
Matrix<complex_t> adjoint(const Matrix<complex_t>&);  //!< adjoint complex matrix
Matrix<real_t> adj(const Matrix<real_t>&);            //!< adjoint (transpose) real matrix
Matrix<complex_t> adj(const Matrix<complex_t>&);      //!< adjoint complex matrix


template<typename K>
void diag(Matrix<K>& m) //! change matrix into diagonal matrix made of diagonal of matrix
{
  dimen_t rows(m.vsize()), cols(m.hsize()), i, j;
  typename std::vector<K>::iterator it_m=m.begin();
  for (i = 0; i < std::min(cols, rows); i++)
    {
      for (j = 0; j < i; it_m++, j++) *it_m = K(0);
      m++; // diagonal entry untouched
      for (j = i + 1; j < cols; it_m++, j++) *it_m = K(0);
    }
  for (i = cols; i < rows; i++)    // for rows without diagonal entry
    for (j = 0; j < cols; it_m++, j++) *it_m = K(0);
}

//utilities to get dimensions
template<typename K>
std::pair<dimen_t, dimen_t> dimsOf(const Matrix<K>& v)
{return std::pair<dimen_t, dimen_t> (v.numberOfRows(), v.numberOfColumns());}


//-----------------------------------------------------------------------------
// extern specialized functions with real matrix as a result
//-----------------------------------------------------------------------------
Matrix<real_t> realPart(const Matrix<real_t>&);   //!< real part of a real matrix
Matrix<real_t> realPart(const Matrix<complex_t>&);//!< real part of a complex matrix
Matrix<real_t> imagPart(const Matrix<real_t>&);   //!< imag part of a real matrix
Matrix<real_t> imagPart(const Matrix<complex_t>&);//!< imag part of a complex matrix

//for name compatibility
//Matrix<real_t> real(const Matrix<complex_t>&);    //!< real part of a complex matrix
//Matrix<real_t> imag(const Matrix<complex_t>&);    //!< imaginary part of a complex matrix
//Matrix<real_t> real(const Matrix<real_t>&);       //!< real part of a real matrix
//Matrix<real_t> imag(const Matrix<real_t>&);       //!< imaginary part of a real matrix

//-----------------------------------------------------------------------------
// extern specialized functions with complex matrix as a result
//-----------------------------------------------------------------------------
Matrix<complex_t> cmplx(const Matrix<real_t>&);    //!< cast a real matrix to a complex matrix
Matrix<complex_t> conj(const Matrix<complex_t>&);  //!< conjugate complex matrix
Matrix<real_t> conj(const Matrix<real_t>&);        //!< conjugate real matrix (compatibility)

//-----------------------------------------------------------------------------
//  norm2 = sqrt(sum_ij (Mij)^2), specialization real and complex
//-----------------------------------------------------------------------------
real_t norm2(const Matrix<real_t>&);
real_t norm2(const Matrix<complex_t>&);
real_t norm2(const Matrix<Matrix<real_t> >&);
real_t norm2(const Matrix<Matrix<complex_t> >&);

//-----------------------------------------------------------------------------
//  norminfty = max_ij max |Mij|, specialization real and complex
//-----------------------------------------------------------------------------
real_t norminfty(const Matrix<real_t>&);
real_t norminfty(const Matrix<complex_t>&);
real_t norminfty(const Matrix<Matrix<real_t> >&);
real_t norminfty(const Matrix<Matrix<complex_t> >&);

//-----------------------------------------------------------------------------
// Gauss solvers, inverse and LU factorization
//-----------------------------------------------------------------------------
//! Template Gaussian elimination solver with partial pivoting strategy for a square linear system with dense row major access matrix
template<typename K_>
bool gaussSolver(std::vector<K_>& mat, std::vector<K_>& rhs, real_t& minPivot, number_t& row)
{
  // returns false when solver fails; use last args to display error
  //
  // minPivot is absolute magnitude of minimal pivot
  // row is row on which a null pivot is found when algorithm fails
  // (in other words row is the matrix rank)
  // is determinant value required on exit ?

  bool flag(false); // useless unless rhs.size() = 0 !!
  number_t nbRows(rhs.size()), p_row, p_r(0), row_i;
  K_ det(1.), sw_m, pivot, mult;
  real_t abs_pivot;
  row = 0;
  minPivot = theRealMax;
  size_t rc, p_rc, rr, p_rr;

  //
  // Gaussian elimination (matrix triangulation)
  //
  for (row = 0; row < nbRows; row++)
    {
      // current step number
      p_row = row;
      p_rc = nbRows * row + row;
      pivot = mat[p_rc];
      abs_pivot = std::abs(pivot);
      // search for partial pivot of maximum absolute magnitude in row-th column
      rc = p_rc + nbRows;
      for (row_i = row + 1; row_i < nbRows; rc += nbRows, row_i++)
        {
          if (abs_pivot < std::abs(mat[rc]))
            {
              p_row = row_i;
              pivot = mat[rc];
              abs_pivot = std::abs(pivot);
            }
        }
      if (abs_pivot < minPivot)
        {
          minPivot = abs_pivot;
          if (abs_pivot < theZeroThreshold) return false; // non invertible matrix
        }
      // swap entries on current row and pivot row as well as on right hand side (if needed)
      if (p_row > row)
        {
          rc = nbRows * row + row;
          p_rc = nbRows * p_row + row;

          for (number_t col = row; col < nbRows; rc++, p_rc++, col++)
            {
              sw_m = mat[rc];
              mat[rc] = mat[p_rc];
              mat[p_rc] = sw_m;
            }
          sw_m = rhs[row];
          rhs[row] = rhs[p_row];
          rhs[p_row] = sw_m;
        }

      // Gaussian elimination in rows row+1, ..., nbRows
      det *= pivot;
      pivot = 1. / pivot;
      p_rr = nbRows * row + row;
      rr = p_rr + nbRows;
      for (row_i = row + 1; row_i < nbRows; row_i++, rr += nbRows)
        {
          p_rc = p_rr;
          rc = rr;
          mult = - pivot * mat[rc];
          for (number_t col = row; col < nbRows; p_rc++, rc++, col++)
            mat[rc] += mult * mat[p_rc];
          rhs[row_i] += mult * rhs[row];
        }
      flag = true;
    }

  // solve upper triangularized matrix: backward substitution
  for (p_r = nbRows; p_r > 0; p_r--)
    {
      row = p_r - 1;
      rc = nbRows * row + row;
      p_rc = rc + 1;
      for (number_t col = row + 1; col < nbRows; col++, p_rc++)
        rhs[row] -= mat[p_rc] * rhs[col];
      rhs[row] /= mat[rc];
    }
  return flag;
}

/*! Template multiple Gaussian elimination solver with partial pivoting strategy for a square linear system
    with dense row major access matrix
    the number of right hand sides is given by nbrhs
    Note: when giving Id matrix as rhs, it returns the inverse of mat in rhs
*/
template<typename K_>
bool gaussMultipleSolver(std::vector<K_>& mat, std::vector<K_>& rhs, number_t nbrhs, real_t& minPivot, number_t& row)
{
  // returns false when solver fails; use last args to display error
  //
  // minPivot is absolute magnitude of minimal pivot
  // row is row on which a null pivot is found when algorithm fails
  // (in other words row is the matrix rank)
  // is determinant value required on exit ?

  bool flag(false); // useless unless rhs.size() = 0 !!
  number_t nbRows= rhs.size()/nbrhs, p_row, p_r=0, row_i;
  K_ det(1.), sw_m, pivot, mult;
  real_t abs_pivot;
  row = 0;
  minPivot = theRealMax;
  size_t rc, p_rc, rr, p_rr;

  // Gaussian elimination (matrix triangulation)
  for (row = 0; row < nbRows; row++)
    {
      // current step number
      p_row = row;
      p_rc = nbRows * row + row;
      pivot = mat[p_rc];
      abs_pivot = std::abs(pivot);
      // search for partial pivot of maximum absolute magnitude in row-th column
      rc = p_rc + nbRows;
      for (row_i = row + 1; row_i < nbRows; rc += nbRows, row_i++)
        {
          if (abs_pivot < std::abs(mat[rc]))
            {
              p_row = row_i;
              pivot = mat[rc];
              abs_pivot = std::abs(pivot);
            }
        }
      if (abs_pivot < minPivot)
        {
          minPivot = abs_pivot;
          if (abs_pivot < theZeroThreshold) return false; // non invertible matrix
        }
      // swap entries on current row and pivot row as well as on right hand side (if needed)
      if (p_row > row)
        {
          rc = nbRows * row + row;
          p_rc = nbRows * p_row + row;
          for (number_t col = row; col < nbRows; rc++, p_rc++, col++)
            {
              sw_m = mat[rc];
              mat[rc] = mat[p_rc];
              mat[p_rc] = sw_m;
            }
          for (number_t k=0; k<nbrhs; k++)
            {
              number_t brhs= k*nbRows;
              sw_m = rhs[brhs+row];
              rhs[brhs+row] = rhs[brhs+p_row];
              rhs[brhs+p_row] = sw_m;
            }
        }

      // Gaussian elimination in rows row+1, ..., nbRows
      det *= pivot;
      pivot = 1. / pivot;
      p_rr = nbRows * row + row;
      rr = p_rr + nbRows;
      for (row_i = row + 1; row_i < nbRows; row_i++, rr += nbRows)
        {
          p_rc = p_rr;
          rc = rr;
          mult = - pivot * mat[rc];
          for (number_t col = row; col < nbRows; p_rc++, rc++, col++)
            mat[rc] += mult * mat[p_rc];
          for (number_t k=0; k<nbrhs; k++)
            {
              number_t brhs= k*nbRows;
              rhs[brhs+row_i] += mult * rhs[brhs+row];
            }
        }
      flag = true;
    }

  // solve upper triangularized matrix: backward substitution
  for (p_r = nbRows; p_r > 0; p_r--)
    {
      row = p_r - 1;
      rc = nbRows * row + row;
      p_rc = rc + 1;
      for (number_t col = row + 1; col < nbRows; col++, p_rc++)
        {
          for (number_t k=0; k<nbrhs; k++)
            {
              number_t brhs= k*nbRows;
              rhs[brhs+row] -= mat[p_rc] * rhs[brhs+col];
            }
        }
      for (number_t k=0; k<nbrhs; k++)
        {
          number_t brhs= k*nbRows;
          rhs[brhs+row] /= mat[rc];
        }
    }
  return flag;
}

template<typename K>
Matrix<K> inverse(const Matrix<K>& m)
{
  dimen_t nbr=m.numberOfRows(), nbc=m.numberOfColumns();
  if (nbr!=nbc) m.mismatchDims("inverse(Matrix)", nbr, nbc);
  Matrix<K> invm(nbr,nbr);
  K det;
  switch(nbr)
    {
      case 1:
        det = m[0];
        if (std::abs(det) < theZeroThreshold)
          {
            where("inverse(Matrix<K>)");
            error("nulldet",det);
          }
        invm[0] = 1. / det;
        break;
      case 2:
        det = m[0] * m[3] - m[1] * m[2];
        if (std::abs(det) < theZeroThreshold)
          {
            where("inverse(Matrix<K>)");
            error("nulldet",det);
          }
        invm[0] = m[3] / det;
        invm[3] = m[0] / det;
        invm[1] = -m[1] / det;
        invm[2] = -m[2] / det;
        break;
      case 3:
        // set 1st column to signed cofactors of 1st row
        invm[0] = m[4] * m[8] - m[5] * m[7];
        invm[3] = m[5] * m[6] - m[3] * m[8];
        invm[6] = m[3] * m[7] - m[4] * m[6];
        // compute determinant
        det = m[0] * invm[0] + m[1] * invm[3] + m[2] * invm[6];
        // singular matrix ?
        if (std::abs(det) < theZeroThreshold)
          {
            where("inverse(Matrix<K>)");
            error("nulldet", det);
          }
        // divide 1st column by determinant
        invm[0] = invm[0] / det;
        invm[3] = invm[3] / det;
        invm[6] = invm[6] / det;
        // set 2nd column to signed cofactors of 2nd row divided by determinant
        invm[1] = (m[2] * m[7] - m[1] * m[8]) / det;
        invm[4] = (m[0] * m[8] - m[2] * m[6]) / det;
        invm[7] = (m[1] * m[6] - m[0] * m[7]) / det;
        // set 3rd column to signed cofactors of 3rd row  divided by determinant
        invm[2] = (m[1] * m[5] - m[2] * m[4]) / det;
        invm[5] = (m[2] * m[3] - m[0] * m[5]) / det;
        invm[8] = (m[0] * m[4] - m[1] * m[3]) / det;
        break;
      default: //use gaussMultipleSolver
        invm = Matrix<K>(nbr,_idMatrix);
        Matrix<K> mc=m;
        number_t nr=nbr;
        real_t eps=theZeroThreshold;
        bool ok=gaussMultipleSolver(mc, invm, nr, eps, nr);
        invm.transpose();  //because gaussMultipleSolver works with rows and not columns
        if (!ok)
          {
            where("inverse(Matrix<K>)");
            error("mat_noinvert");
          }
    }
  return invm;
}

// LU factorization with no permutation and row permutation

/*! LU factorization with no permutation,
    L lower triangular matrix with diagonal 1 stored in strict lower part of LU
    U upper triangular part stored in upper part of LU
    matrix LU may be the same as A
*/
template <typename T>
Matrix<T>& lu(Matrix<T>& A, Matrix<T>& LU)
{
  if (&A!=&LU) LU=A;   //recopy A to LU to work only with LU matrix
  dimen_t nbr=LU.numberOfRows(), nbc=LU.numberOfColumns();
  typename std::vector<T>::iterator itri, itrk, itlub = LU.begin();
  T piv, v;
  for (dimen_t k=0; k<nbr-1; k++) //main Gauss loop
  {
    piv=LU[k*nbc+k];
    if (std::abs(piv)<theTolerance) error("small_pivot");
    #ifdef XLIFEPP_WITH_OMP
    #pragma omp parallel for firstprivate(v, itri, itrk)
    #endif // XLIFEPP_WITH_OMP
    for (dimen_t i=k+1; i<nbr; i++)
    {
      itri=itlub+i*nbc+k;
      v=*itri/=piv;
      itri++;
      itrk=itlub+k*nbc+k+1;
      for (dimen_t j=k+1; j<nbc; j++, ++itri, ++itrk)  *itri-=v* *itrk;
    }  //end loop i
  }//end loop k
  return LU;
}
template <typename T>
Matrix<T>& lu(Matrix<T>& A) {return lu(A,A);}

/*! LU factorization with real row permutation, PA = LU where P is a permutation matrix represented by a permutation vector p
      L lower triangular matrix with diagonal 1 stored in strict lower part of LU
      U upper triangular part stored in upper part of LU
      be care with operation on LU, permutation may have been applied for instance solving Ax=b should be done as LUx=Pb
    matrix LU may be the same as A
*/
template <typename T>
Matrix<T>& lu(Matrix<T>& A, Matrix<T>& LU, std::vector<dimen_t>& P)
{
  if(&A!=&LU) LU=A;   //recopy A to LU to work only with LU matrix
  dimen_t nbr=LU.numberOfRows(), nbc=LU.numberOfColumns();
  P.resize(nbr);
  std::vector<dimen_t>::iterator itp=P.begin();
  for(dimen_t i=0; i<nbr; i++, ++itp) *itp=i;
  typename std::vector<T>::iterator itri, itrk, itlub = LU.begin();
  dimen_t q;
  T piv, v;
  real_t s,t;

  for(dimen_t k=0; k<nbr-1; k++) //main Gauss loop
    {
      s=std::abs(LU[k*nbc+k]);
      dimen_t l=k;
      for(dimen_t i=k+1; i<nbr; i++)
        {
          t=std::abs(LU[i*nbc+k]);
          if(t > s) {l=i; s=t;}
        }
      if(s<theTolerance) error("mat_noinvert");
      if(l!=k)  //swap rows k and l
        {
          q=P[l]; P[l]=P[k]; P[k]=q;                        //virtual swap
          itrk=itlub+k*nbc;
          itri=itlub+l*nbc;
          for(dimen_t j=0; j<nbc; j++, itrk++, itri++) //real swap
            {
              v=*itri; *itri=*itrk; *itrk=v;
            }
        }
      piv=LU[k*nbc+k];
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for firstprivate(q, v, itri, itrk)
      #endif // XLIFEPP_WITH_OMP
      for(dimen_t i=k+1; i<nbr; i++)
        {
          itri=itlub+i*nbc+k;
          v=*itri/=piv;
          itri++;
          itrk=itlub+k*nbc+k+1;
          for(dimen_t j=k+1; j<nbc; j++, ++itri, ++itrk)  *itri-=v* *itrk;
        }  //end loop i
    }//end loop k
  return LU;
}

template <typename T>
Matrix<T>& lu(Matrix<T>& A, std::vector<dimen_t>& P) {return lu(A,A,P);}

template <typename T>
void lu(Matrix<T>& A, Matrix<T>& L, Matrix<T>& U)
{
  Matrix<T> LU;
  lu(A,LU);
  L = LU.lower(T(1));
  U = LU.upper();
}

template <typename T>
void lu(Matrix<T>& A, Matrix<T>& L, Matrix<T>& U, std::vector<dimen_t>& P)
{
  Matrix<T> LU;
  lu(A,LU,P);
  L = LU.lower(T(1));
  U = LU.upper();
}

//! special forced cast complex
template<class T>
inline T complexToT(const Matrix<complex_t>& c) {return c;}
template<>
inline Matrix<real_t> complexToT(const Matrix<complex_t>& c) { return realPart(c);}
template<>
inline Matrix<real_t> complexToT(const complex_t& c) {return Matrix<real_t>(1,1,c.real());}
template<>
inline Matrix<complex_t> complexToT(const complex_t& c) {return Matrix<complex_t>(1,1,c);}

inline real_t complexToRorC(const complex_t& c, const Matrix<real_t>& mr) {return c.real();}
inline complex_t complexToRorC(const complex_t& c, const Matrix<complex_t>& mc) {return c;}

/*======================================================================================================
                             svd and qr stuff using the Eigen library
  ======================================================================================================*/
/*! SVD or truncated SVD with either rank or precision truncation
    T: type of the matrix (real or complex)
    A: Matrix to be be factorized
    U,D,V: SVD factors A=U*D*V' or A~U*D*V' (output)
    r: prescribed rank
    eps: singularvalue threshold
    if r > 0, the SVD is restricted to the first r singular values
    else it is restricted to the first singular values smaller than eps
    rmk: r=0 and eps=0 gives the full svd
*/
template <typename T>
void svd(Matrix<T>& A, Matrix<T>& U, Vector<T>& D, Matrix<T>& V, number_t r=0, real_t eps=0.)
{
  number_t m=A.numberOfRows(), n=A.numberOfColumns();
  number_t p = r;
  if(p==0) p=std::min(m,n);
  U.changesize(m,p,T());  V.changesize(n,p,T()); D.resize(p,T());
  number_t rk=r;
  svd(&A[0], m, n, &U[0], &D[0], &V[0], rk, eps);
  if(rk<p)
    {
       U.resize(m*rk); V.resize(n*rk); D.resize(rk);
    }
}

/*! Random SVD with either rank or precision truncation
    T: type of the matrix (real or complex)
    A: Matrix to be be factorized
    U,D,V: SVD factors A~U*D*V' (output)
    r: prescribed rank
    eps: energy threshold
    if r > 0, the SVD is restricted to the first r singular values
    else it is restricted to the first singular values smaller than eps
*/
template <typename T>
void rsvd(Matrix<T>& A, Matrix<T>& U, Vector<T>& D, Matrix<T>& V, number_t r=0, real_t eps=theTolerance)
{
  number_t m=A.numberOfRows(), n=A.numberOfColumns();
  number_t p = r;
  if(p==0) p=std::min(m,n);
  U.changesize(m,p,T());  V.changesize(n,p,T()); D.resize(p,T());
  number_t rk=r;
  if(r>0) rsvd(&A[0], m, n, rk, &U[0], &D[0], &V[0]);
  else rsvd(&A[0], m, n, eps, rk, &U[0], &D[0], &V[0]);
  if(rk<p)
    {
       U.resize(m*rk); V.resize(n*rk); D.resize(rk);
    }
}

/*! Improved random SVD with precision truncation
    T: type of the matrix (real or complex)
    A: Matrix to be be factorized
    eps: energy threshold
    U,D,V: SVD factors A~U*D*V' (output)
    t, ts: number of sampling and oversampling
    q: power number (q=0 gives the standard r3svd)
    maxit: maximum of restart
    if t = 0, the parameters t, p, q and maxit are determined by function regarding the matrix dimensions
*/
template <typename T>
void r3svd(Matrix<T>& A, Matrix<T>& U, Vector<T>& D, Matrix<T>& V, real_t eps=theTolerance,
           number_t t = 0, number_t ts = 0, number_t q = 0, number_t maxit = 0)
{
  number_t m=A.numberOfRows(), n=A.numberOfColumns();
  number_t p=std::min(m,n);
  U.changesize(m,p,T());  V.changesize(n,p,T()); D.resize(p,T());
  number_t rk;
  r3svd(&A[0], m, n, eps, rk, &U[0], &D[0], &V[0], t, ts, q, maxit);
  if(rk<p)
    {
      U.resize(m*rk); V.resize(n*rk); D.resize(rk);
    }
}

//! QR factorization
template <typename T>
void qr(const Matrix<T>& A, Matrix<T>& Q, Matrix<T>& R)
{
  number_t m=A.numberOfRows(), n=A.numberOfColumns();
  Q.changesize(m,m,T());  R.changesize(m,n,T());
  qr(&A[0], m, n, &Q[0], &R[0]);
}

/*! computation of eigen values A*X=lambda*X
    ### eigen vectors are stored by row in Xs matrix !
*/
template <typename T>
void eigs(const Matrix<T>& A, Vector<complex_t>& ls, Matrix<complex_t>& Xs)
{
  number_t m=A.numberOfRows();
  if(A.numberOfCols()!=m) error("mat_nonsquare","eigs(A):A",m,A.numberOfCols());
  Xs.changesize(m,m,0.);  ls.resize(m);
  eigs(&A[0], m, &Xs[0], &ls[0]);
}

template <typename T>
void eigs(const Matrix<T>& A, Vector<complex_t>& ls, Vector<Vector<complex_t> >& Xs)
{
  Matrix<complex_t> Xms;
  eigs(A,ls,Xms);
  number_t m=A.numberOfRows();
  Xs.resize(m);
  std::vector<complex_t>::iterator itxs=Xms.begin();
  for(number_t i=0;i<m;++i, itxs+=m) Xs[i]=Vector<complex_t>(itxs,itxs+m);
}

/*! computation of generalized eigen values A*X=lambda*B*X
    ### eigen vectors are stored by row in Xs matrix !
    ### currently works only for REAL matrices
*/
template <typename T>
void eigs(const Matrix<T>& A, const Matrix<T>& B, Vector<complex_t>& ls, Matrix<complex_t>& Xs)
{
  number_t m=A.numberOfRows();
  if(A.numberOfCols()!=m) error("mat_nonsquare","eigs(A,B):A",m,A.numberOfCols());
  if(B.numberOfRows()!=B.numberOfCols()) error("mat_nonsquare","eigs(A,B):B",B.numberOfRows(),B.numberOfCols());
  if(B.numberOfRows()!=m) mismatchDims("eigs(A,B)",m,B.numberOfRows());
  Xs.changesize(m,m,0.);  ls.resize(m);
  eigs(&A[0], &B[0], m, &Xs[0], &ls[0]);
}

template <typename T>
void eigs(const Matrix<T>& A, const Matrix<T>& B, Vector<complex_t>& ls, Vector<Vector<complex_t> >& Xs)
{
  Matrix<complex_t> Xms;
  eigs(A,B,ls,Xms);
  number_t m=A.numberOfRows();
  Xs.resize(m);
  std::vector<complex_t>::iterator itxs=Xms.begin();
  for(number_t i=0;i<m;++i, itxs+=m) Xs[i]=Vector<complex_t>(itxs,itxs+m);
}

} // end of namespace xlifepp

#endif /* MATRIX_HPP */
