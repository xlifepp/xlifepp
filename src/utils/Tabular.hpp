/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Tabular.hpp
  \author E. Lunéville
  \since 01 apr 2019
  \date  01 apr 2019

  \brief Template class to manage tabulated values

  This class managed nD-grids indexed by nD uniform real coordinates defined by
     - start[p]  the starting coordinate
     - step[p]   the coordinate step
     - nbstep[p] the number of steps   (xp)_k = start[p]+k*step[p]  k=0 to nbstep[p]
   Values are stored in a unique vector
      values = [ v0.00 v0.01 ... v0.0n v0.10 v0.11 ... v0.1n ...]
      access to the i1,i2,...in values from the index i1*(s2*s3*...*sn)+i2*(s3*...*sn)+ ... in
*/

#ifndef TABULAR_HPP_INCLUDED
#define TABULAR_HPP_INCLUDED

#include "Parameters.hpp" // Parameters class definition
#include "Value.hpp"
#include "Vector.hpp"
#include "Matrix.hpp"
#include "config.h"

namespace xlifepp
{

template <typename T>
class Tabular : public std::vector<T>
{
    typedef T(*funT1)(real_t);
    typedef T(*funTP1)(real_t, Parameters& pa);
    typedef T(*funT2)(real_t, real_t);
    typedef T(*funTP2)(real_t, real_t, Parameters& pa);
    typedef T(*funT3)(real_t, real_t, real_t);
    typedef T(*funTP3)(real_t, real_t,real_t, Parameters& pa);

  public:
    number_t dim;                 //!< grid dimension
    std::vector<real_t> start;    //!< starting coordinates
    std::vector<real_t> step;     //!< coordinate steps
    std::vector<number_t> nbstep; //!< coordinate steps
    std::vector<string_t> name;   //!< coordinate names
    std::vector<number_t> bs;     //!< block sizes (internal use)
    string_t comment;

    Tabular(real_t x0, real_t dx, number_t nx, const string_t& nax="x1");         //!< construct a 1D grid
    Tabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny,
            const string_t& nax="x1", const string_t& nay="x2");                  //!< construct a 2D grid
    Tabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny,
            real_t z0, real_t dz, number_t nz, const string_t& nax="x1",
            const string_t& nay="x2", const string_t& naz="x3");                  //!< construct a 3D grid
    Tabular(real_t x0, real_t dx, number_t nx, funT1 f, const string_t& nax="x1");                    //!< construct a 1D table
    Tabular(real_t x0, real_t dx, number_t nx, funTP1 f, Parameters& pars, const string_t& nax="x1"); //!< construct a 1D table
    Tabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny, funT2 f,
            const string_t& nax="x1", const string_t& nay="x2");                                      //!< construct a 2D table
    Tabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny, funTP2 f,
            Parameters& pars, const string_t& nax="x1", const string_t& nay="x2");                    //!< construct a 2D table
    Tabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny,
            real_t z0, real_t dz, number_t nz, funT3 f, const string_t& nax="x1",
            const string_t& nay="x2", const string_t& naz="x3");                                      //!< construct a 3D grid
    Tabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny,
            real_t z0, real_t dz, number_t nz, funTP3 f, Parameters& pars,
            const string_t& nax="x1", const string_t& nay="x2", const string_t& naz="x3");            //!< construct a 3D grid
    Tabular(const string_t& filename);

    void addCoordinate(real_t c, real_t dc, number_t nc, const string_t& nac="");
    void createTable(funT1 f);
    void createTable(funTP1 f, Parameters& pars);
    void createTable(funT2 f);
    void createTable(funTP2 f, Parameters& pars);
    void createTable(funT3 f);
    void createTable(funTP3 f, Parameters& pars);
    void clear();
    void globalToIndex(number_t k, number_t d,
                       std::vector<number_t>::iterator ijk) const; //!< gloal num to ijk... numbering
    void saveToFile(const string_t& filename) const;  //!< save table to file
    void loadFromFile(const string_t& filename);      //!< load table from file

    // evaluation function at any point using tabulated value (Q1 interpolation)
    T operator()(real_t x) const
    {
      real_t y = (x-start[0])/step[0];
      if(y<0 || y>nbstep[0])  error("free_error","in Tabular(x), point "+tostring(x)+" is outside the domain");
      number_t k=std::floor(y);
      k=std::min(k,nbstep[0]); //to avoid out of limits due to round error
      real_t a=y-k;
      return (1-a)*(*this)[k]+a*(*this)[k+1];
    }

    T operator()(real_t x, real_t y) const
    {
      real_t x0=start[0], y0=start[1], dx=step[0], dy=step[1];
      number_t nx=nbstep[0], ny=nbstep[1];
      number_t i=std::floor((x-x0)/dx), j=std::floor((y-y0)/dy);
      i=std::min(i,nx); j=std::min(j,ny); //to avoid out of limits due to round error
      real_t x1 = x0+i*dx, y1 = y0+j*dy;
      real_t xmx1=x-x1, ymy1=y-y1, x2mx=dx-xmx1, y2my=dy-ymy1;
      number_t k=i*(ny+1)+j;
      T vij=(*this)[k], vip1j=(*this)[k+ny+1], vijp1=(*this)[k+1], vip1jp1=(*this)[k+ny+2];
      return (y2my*(x2mx*vij+xmx1*vip1j)+ymy1*(xmx1*vip1jp1+x2mx*vijp1))/(dx*dy);
    }

    T valrec(number_t d, number_t k, const std::vector<number_t>& is,
             const std::vector<real_t>& as) const                          //! recursive evaluation
    {
      k+=is[d]*bs[d];
      if(d==dim-1) return (1-as[dim-1])*(*this)[k]+as[dim-1]*(*this)[k+1];
      return (1-as[d])*valrec(d+1,k,is,as)+as[d]*valrec(d+1,k+bs[d],is,as);
    }

    T operator()(const std::vector<real_t>& X) const  //general case
    {
      if(dim==1) return (*this)(X[0]);
      std::vector<number_t> is(dim);
      std::vector<real_t> as(dim);
      for(number_t d=0; d<dim; d++)
        {
          real_t y = (X[d]-start[d])/step[d];
          is[d]=std::min(number_t(std::floor(y)),nbstep[d]);
          as[d]=y-is[d];
        }
      return valrec(0,0,is,as);
    }

    T operator()(real_t x, real_t y, real_t z) const
    {
      std::vector<real_t> xs(3,x); xs[1]=y; xs[2]=z;
      return (*this)(xs);
    }

    void print(std::ostream& os) const //! write to a output stream
    {os << *this;}
    void print(PrintStream& os) const {print(os.currentStream());}
};

//--------------------------------------------------------------------------------------
//  Tabular member functions
//--------------------------------------------------------------------------------------
template <typename T>
Tabular<T>::Tabular(real_t x0, real_t dx, number_t nx, const string_t& nax)
{
  start.push_back(x0); step.push_back(dx); nbstep.push_back(nx); name.push_back(nax); bs.push_back(1);
  std::vector<T>::resize(nx+1); dim=1;
  comment="Tabular";
}

template <typename T>
Tabular<T>::Tabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny,
        const string_t& nax, const string_t& nay)
{
  start.push_back(x0); step.push_back(dx); nbstep.push_back(nx); name.push_back(nax);
  start.push_back(y0); step.push_back(dy); nbstep.push_back(ny); name.push_back(nay);
  bs.push_back(ny+1); bs.push_back(1);
  std::vector<T>::resize((nx+1)*(ny+1)); dim=2;
  comment="Tabular";
}

template <typename T>
Tabular<T>::Tabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny,
                    real_t z0, real_t dz, number_t nz, const string_t& nax,
                    const string_t& nay, const string_t& naz)
{
  start.push_back(x0); step.push_back(dx); nbstep.push_back(nx); name.push_back(nax);
  start.push_back(y0); step.push_back(dy); nbstep.push_back(ny); name.push_back(nay);
  start.push_back(z0); step.push_back(dz); nbstep.push_back(nz); name.push_back(naz);
  bs.push_back((ny+1)*(nz+1)); bs.push_back(nz+1); bs.push_back(1);
  std::vector<T>::resize((nx+1)*(ny+1)*(nz+1)); dim=3;
  comment="Tabular";
}

template <typename T>
Tabular<T>::Tabular(real_t x0, real_t dx, number_t nx, funT1 f, const string_t& nax)
{
  start.push_back(x0); step.push_back(dx); nbstep.push_back(nx); name.push_back(nax); bs.push_back(1);
  std::vector<T>::resize(nx+1); dim=1; createTable(f);
  comment="Tabular";
}

template <typename T>
Tabular<T>::Tabular(real_t x0, real_t dx, number_t nx, funTP1 f, Parameters& pars, const string_t& nax)
{
  start.push_back(x0); step.push_back(dx); nbstep.push_back(nx); name.push_back(nax); bs.push_back(1);
  std::vector<T>::resize(nx+1); dim=1; createTable(f,pars);
  comment="Tabular";
}

template <typename T>
Tabular<T>::Tabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny, funT2 f,
                    const string_t& nax, const string_t& nay)
{
  start.push_back(x0); step.push_back(dx); nbstep.push_back(nx); name.push_back(nax);
  start.push_back(y0); step.push_back(dy); nbstep.push_back(ny); name.push_back(nay);
  bs.push_back(ny+1); bs.push_back(1);
  std::vector<T>::resize((nx+1)*(ny+1)); dim=2; createTable(f);
  comment="Tabular";
}

template <typename T>
Tabular<T>::Tabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny, funTP2 f,
                    Parameters& pars, const string_t& nax, const string_t& nay)
{
  start.push_back(x0); step.push_back(dx); nbstep.push_back(nx); name.push_back(nax);
  start.push_back(y0); step.push_back(dy); nbstep.push_back(ny); name.push_back(nay);
  bs.push_back(ny+1); bs.push_back(1);
  std::vector<T>::resize((nx+1)*(ny+1)); dim=2; createTable(f,pars);
  comment="Tabular";
}

template <typename T>
Tabular<T>::Tabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny,
                    real_t z0, real_t dz, number_t nz, funT3 f, const string_t& nax,
                    const string_t& nay, const string_t& naz)
{
  start.push_back(x0); step.push_back(dx); nbstep.push_back(nx); name.push_back(nax);
  start.push_back(y0); step.push_back(dy); nbstep.push_back(ny); name.push_back(nay);
  start.push_back(z0); step.push_back(dz); nbstep.push_back(nz); name.push_back(naz);
  bs.push_back((ny+1)*(nz+1)); bs.push_back(nz+1); bs.push_back(1);
  std::vector<T>::resize((nx+1)*(ny+1)*(nz+1)); dim=3; createTable(f);
  comment="Tabular";
}

template <typename T>
Tabular<T>::Tabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny,
                    real_t z0, real_t dz, number_t nz, funTP3 f, Parameters& pars,
                    const string_t& nax, const string_t& nay, const string_t& naz)
{
  start.push_back(x0); step.push_back(dx); nbstep.push_back(nx); name.push_back(nax);
  start.push_back(y0); step.push_back(dy); nbstep.push_back(ny); name.push_back(nay);
  start.push_back(z0); step.push_back(dz); nbstep.push_back(nz); name.push_back(naz);
  bs.push_back((ny+1)*(nz+1)); bs.push_back(nz+1); bs.push_back(1);
  std::vector<T>::resize((nx+1)*(ny+1)*(nz+1)); dim=3; createTable(f,pars);
  comment="Tabular";
}

template <typename T>
Tabular<T>::Tabular(const string_t& filename)
{
  loadFromFile(filename);
}

template <typename T>
void Tabular<T>::clear()
{
  dim=0; start.clear(); step.clear(); nbstep.clear(); name.clear(); std::vector<T>::clear();
  comment="Tabular";
}

template <typename T>
void Tabular<T>::addCoordinate(real_t c, real_t dc, number_t nc, const string_t& nac)
{
  start.push_back(c); step.push_back(dc); nbstep.push_back(nc); name.push_back(nac);
  std::vector<T>::resize(std::vector<T>::size()*(nc+1)); dim++;
  for(std::vector<number_t>::iterator it=bs.begin(); it!=bs.end(); ++it) *it*=(nc+1);
  bs.push_back(1);
}

template <typename T>
void Tabular<T>::createTable(funT1 f)
{
  if(dim!=1) error("free_error", "use a "+tostring(dim)+"d function to create Tabular");
  typename std::vector<T>::iterator itv=std::vector<T>::begin();
  real_t x = start[0];
  for(number_t i=0; i<=nbstep[0]; i++, ++itv, x+=step[0]) *itv = f(x);
}

template <typename T>
void Tabular<T>::createTable(funTP1 f, Parameters& pars)
{
  if(dim!=1) error("free_error", "use a "+tostring(dim)+"d function to create Tabular");
  typename std::vector<T>::iterator itv=std::vector<T>::begin();
  real_t x = start[0];
  for(number_t i=0; i<=nbstep[0]; i++, ++itv, x+=step[0]) *itv = f(x, pars);
}

template <typename T>
void Tabular<T>::createTable(funT2 f)
{
  if(dim!=2) error("free_error", "use a "+tostring(dim)+"d function to create Tabular");
  typename std::vector<T>::iterator itv=std::vector<T>::begin();
  real_t x = start[0];
  for(number_t i=0; i<=nbstep[0]; i++, x+=step[0])
    {
      real_t y= start[1];
      for(number_t j=0; j<=nbstep[1]; j++, y+=step[1], ++itv) *itv = f(x,y);
    }
}

template <typename T>
void Tabular<T>::createTable(funTP2 f, Parameters& pars)
{
  if(dim!=2) error("free_error", "use a "+tostring(dim)+"d function to create Tabular");
  typename std::vector<T>::iterator itv=std::vector<T>::begin();
  real_t x = start[0];
  for(number_t i=0; i<=nbstep[0]; i++, x+=step[0])
    {
      real_t y= start[1];
      for(number_t j=0; j<=nbstep[1]; j++, y+=step[1], ++itv) *itv = f(x,y,pars);
    }
}

template <typename T>
void Tabular<T>::createTable(funT3 f)
{
  if(dim!=3) error("free_error", "use a "+tostring(dim)+"d function to create Tabular");
  typename std::vector<T>::iterator itv=std::vector<T>::begin();
  real_t x = start[0];
  for(number_t i=0; i<=nbstep[0]; i++, x+=step[0])
    {
      real_t y= start[1];
      for(number_t j=0; j<=nbstep[1]; j++, y+=step[1])
        {
          real_t z= start[2];
          for(number_t k=0; k<=nbstep[2]; k++, z+=step[2], ++itv) *itv = f(x,y,z);
        }
    }
}

template <typename T>
void Tabular<T>::createTable(funTP3 f, Parameters& pars)
{
  if(dim!=3) error("free_error", "use a "+tostring(dim)+"d function to create Tabular");
  typename std::vector<T>::iterator itv=std::vector<T>::begin();
  real_t x = start[0];
  for(number_t i=0; i<=nbstep[0]; i++, x+=step[0])
    {
      real_t y= start[1];
      for(number_t j=0; j<=nbstep[1]; j++, y+=step[1])
        {
          real_t z= start[2];
          for(number_t k=0; k<=nbstep[2]; k++, z+=step[2], ++itv) *itv = f(x,y,z,pars);
        }
    }
}

template <typename T>
void Tabular<T>::globalToIndex(number_t k, number_t d,
                               std::vector<number_t>::iterator ijk) const
{
  number_t b=bs[d-1];
  *ijk = k/b;
  if(d==dim) return;
  globalToIndex(k-(*ijk)*b,d+1,ijk+1);
}

/*! save table to file with the following format
      dim valuetype valuedim
      comment
      name1 x1 dx1 nx1 name2 x2 dx2 nx2  named dxd nxd
      ...
      x1_i1 x2_i2 ... xd_id v_i1i2...id
      ...
    the values v_i1i2...id are always a list of reals. A complex if writen as 2 reals but valuedim is longer 1
*/
template <typename T>
void Tabular<T>::saveToFile(const string_t& filename) const
{
  T v=std::vector<T>::at(0);
  ValueType vt = valueType(v);
  number_t vd=sizeOf(v);
  std::ofstream os(filename.c_str());
  os.precision(10);
  os<<dim<<" "<<vt<<" "<<vd<<eol<<comment<<eol;
  for(number_t d=0; d<dim; d++)
    os<<name[d]<<" "<<start[d]<<" "<<step[d]<<" "<<nbstep[d]<<" "<<eol;
  std::vector<number_t> ijk(dim);
  typename std::vector<T>::const_iterator itv=std::vector<T>::begin(), itve=std::vector<T>::end();
  number_t k=0;
  for(; itv!=itve; ++itv,k++)
    {
      globalToIndex(k, 1, ijk.begin());
      for(number_t d=0; d<dim; d++) os<<start[d]+step[d]*ijk[d]<<" ";
      if(vd==1) //scalar
        {
          if(vt==_real) os<<*itv<<eol;
          else os<<realPart(*itv)<<" "<<imagPart(*itv)<<eol;
        }
      else error("free_error","vector function tabular is not yet handled");
    }
  os<<eol;
  os.close();
}


inline void toComplex(real_t x, real_t y, real_t& z) {z=x;}
inline void toComplex(real_t x, real_t y, complex_t& z) {z=complex_t(x,y);}

//!< load table from file
template <typename T>
void Tabular<T>:: loadFromFile(const string_t& filename)
{
  clear();
  string_t str;
  std::ifstream is(filename.c_str());
  number_t vt, vd;
  is>>dim>>vt>>vd;    //vd should be 1 while the vector case is not handled
  if(vt!=valueType(T()))  error("free_error","value type of file "+filename+" is not consistant with the type of Tabular");
  std::getline(is,str);
  std::getline(is,comment);
  name.resize(dim); start.resize(dim); step.resize(dim); nbstep.resize(dim); bs.resize(dim);
  number_t s=1;
  for(number_t d=0; d<dim; d++)
    {
      is>>name[d]>>start[d]>>step[d]>>nbstep[d];
      s*=(nbstep[d]+1);
    }
  bs[dim-1]=1;
  for(number_t d=1; d<dim; d++) bs[dim-d-1]=bs[dim-d]*(nbstep[dim-d]+1);
  std::vector<T>::resize(s);
  real_t x,y;
  typename std::vector<T>::iterator itv=std::vector<T>::begin(), itve=std::vector<T>::end();
  if(vt==_real)
    {
      for(; itv!=itve; ++itv)
        {
          for(number_t d=0; d<dim; d++) is>>x;
          is>>(*itv);
        }
    }
  else
    {
      for(; itv!=itve; ++itv)
        {
          for(number_t d=0; d<dim; d++) is>>x;
          is>>x>>y;
          toComplex(x,y,*itv);
        }
    }
  is.close();
}

// print stuff
template <typename T>
std::ostream& operator<<(std::ostream& os, const Tabular<T>& t) //! general vector flux insertion (write)
{
  if(theVerboseLevel==0) return os;
  os<<"tabular of dimension "<<t.dim<<eol;
  for(number_t d=0; d<t.dim; d++)
    os<<"  "<<t.name[d]<<"_0="<<t.start[d]<<" step="<<t.step[d]<<" nbstep="<<t.nbstep[d]<<eol;
  if(theVerboseLevel<3) return os;
  std::vector<number_t> ijk(t.dim);
  typename std::vector<T>::const_iterator itv=t.begin(), itve=t.end();
  number_t k=0;
  os<<"  tabulated values:"<<eol;
  for(; k<std::min(t.size(),theVerboseLevel); ++itv,k++)
    {
      t.globalToIndex(k, 1, ijk.begin());
      for(number_t d=0; d<t.dim; d++) os<<"  "<<t.start[d]+t.step[d]*ijk[d]<<" ";
      os<<" -> "<<*itv<<eol;
    }
  if(t.size()>theVerboseLevel) os<<"..."<<eol;
  return os;
}

} // end of namespace xlifepp

#endif // TABULAR_HPP_INCLUDED
