/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Parameters.hpp
  \author E. Lunéville
  \since 1 nov 2011
  \date 3 may 2012

  \brief Definition of the xlifepp::Parameter and xlifepp::Parameters classes

  The class xlifepp::Parameter handles parameter and encapsulates any integer type,
  real type , complex type, string_t values or const void* pointer

  The class xlifepp::Parameters handles a list of xlifepp::Parameter's
  useful to attached data to user's functions (see the xlifepp::Function class)

  void* pointer is used to store real/complex vectors  or real/complex matrices as parameter:
        _realVector: std::vector<real_t>      _complexVector: std::vector<complex_t>
        _realMatrix: Matrix<real_t>           _complexMatrix: Matrix<complex_t>

  usage:
      \code
      Parameter height=3.5;
      Parameter number_of_iterations_=100;
      Parameter i=complex_t(0.,1.);
      Parameter comment="case 1";
      height+=3;
      Parameter i2=i*i;       //i2 is complex_t
      i2=real(i2);            //i2 becomes real_t

      Parameters data;        //list of parameters
      data << height << Parameter(4,"width") << "case 1" << 1.5;
      data(1)= 2;             //replace the value 3 by 2 (using numerical index)
      data("height")=2;       //same effect (using alpha index)
      data(height)=2;         //same effect (using parameter name)
      double x=data(4);       //x contains 1.5 (autocast)
      x=data("width");        //x contains 4   (autocast)
      \endcode
 */

#ifndef PARAMETERS_HPP
#define PARAMETERS_HPP

//===============================================================================
// library dependencies
#include "config.h"
#include "Collection.hpp"
#include "String.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "Messages.hpp"
#include "ThreadData.hpp"
//===============================================================================
// stl dependencies
#include <map>
#include <vector>
#include <iostream>
#include <sstream>
#if __cplusplus >= 201103L
#include <initializer_list>
#endif
//================================================================================

namespace xlifepp
{
  // forward classes
  class BezierSpline;
  class BSpline;
  class CatmullRomSpline;
  class C2Spline;
  template <class T> class ClusterTree;
  class CollinoIM;
  class DuffyIM;
  template <class T> class FilonIMT;
  class Function;
  class GeomDomain;
  class HMatrixIM;
  class IntegrationMethod;
  class IntegrationMethods;
  class LenoirSalles2dIM;
  class LenoirSalles2dIR;
  class LenoirSalles3dIM;
  class LenoirSalles3dIR;
  class Nurbs;
  class Parametrization;
  class PolynomialIM;
  class ProductIM;
  class QuadratureIM;
  class SauterSchwabIM;
  class SauterSchwabSymIM;
  class Spline;
  class TermVectors;
  class Transformation;

//convert integers with checking boundaries
inline number_t intToNum(int_t i,const string_t& loc="?")
{
  if (i<0) error("is_lesser", i, 0);
  return number_t(i);
}

inline dimen_t intToDim(int_t i,const string_t& loc="?")
{
  if (i<0 || i>int_t(theDimMax)) error("index_out_of_range", i, 0, int_t(theDimMax));
  return dimen_t(i);
}

inline int_t numToInt(number_t n,string_t loc="?")
{
  if (n > number_t(theIntMax)) error("is_greater", n, number_t(theIntMax));
  return int_t(n);
}

inline int_t numToDim(number_t n,string_t loc="?")
{
  if (n > number_t(theDimMax)) error("is_greater", n, number_t(theDimMax));
  return dimen_t(n);
}

/*!
  \class Parameter
  encapsulates any scalar value of type int, real_t, complex_t, string_t or const void*.
  - main standard algebric operations are supported for parameter of numerical type
  - const void* allows to store anything; it is designed for advanced users because void pointers are unsafe
    particular pointers are managed by class:
      std::vector<real_t>*    to store some reals
      std::vector<complex_t>* to store some complexes
      std::vector<int_t>*     to store some integers
      std::vector<string_t>*  to store some strings
      Matrix<real_t>*         to store a real matrix
      Matrix<complex_t>*      to store a complex matrix
      Point*                  to store a point
      std::vector<Point>*     to store some points
*/
class Parameter
{
  public:
    int_t i_;            //!< to store int type data
    real_t r_;           //!< to store real_t type data
    complex_t c_;        //!< to store complex_t type data
    string_t s_;         //!< to store string_t data
    bool b_;             //!< to store bool data
    const void* p_;      //!< to store const void* data
    string_t name_;      //!< parameter name
    Strings shortnames_; //!< parameter shortnames
    ParameterKey key_;   //!< parameter key
    ValueType type_;     //!< type of value

  public:
    /* --------------------------------------------------------------------------------
       constructors, destructor, assignment
    -------------------------------------------------------------------------------- */
    Parameter();                                                    //!< default constructor
    Parameter& operator=(const Parameter& p);                       //!< default assignment operator=
    Parameter(const Parameter& p, const string_t& nm = string_t(""), const string_t& snm = string_t("")); //!< copy constructor with optional arg name
    Parameter(const Parameter& p, const string_t& nm, const Strings& snm);                                //!< copy constructor with optional arg name
    Parameter(ParameterKey key, const string_t& nm = string_t(""), const string_t& snm = string_t(""));   //!< constructor from key
    Parameter(ParameterKey key, const string_t& nm, const Strings& snm);                                  //!< constructor from key
    Parameter(const int_t i, const string_t& nm = string_t(""), const string_t& snm = string_t(""));      //!< constructor from an int_t
    Parameter(const int_t i, const string_t& nm, const Strings& snm);                                     //!< constructor from an int_t
    Parameter(const int i, const string_t& nm = string_t(""), const string_t& snm = string_t(""));        //!< constructor from an int
    Parameter(const int i, const string_t& nm, const Strings& snm);                                       //!< constructor from an int
    Parameter(const bool b, const string_t& nm = string_t(""), const string_t& snm = string_t(""));       //!< constructor from a bool
    Parameter(const bool b, const string_t& nm, const Strings& snm);                                      //!< constructor from a bool
    Parameter(const number_t i, const string_t& nm = string_t(""), const string_t& snm = string_t(""));   //!< constructor from a number_t
    Parameter(const number_t i, const string_t& nm, const Strings& snm);                                  //!< constructor from a number_t
    Parameter(const real_t r, const string_t& nm = string_t(""), const string_t& snm = string_t(""));     //!< constructor from a real_t
    Parameter(const real_t r, const string_t& nm, const Strings& snm);                                    //!< constructor from a real_t
    Parameter(const complex_t& c, const string_t& nm = string_t(""), const string_t& snm = string_t("")); //!< constructor from a complex_t
    Parameter(const complex_t& c, const string_t& nm, const Strings& snm);                                //!< constructor from a complex_t
    Parameter(const string_t& s, const string_t& nm = string_t(""), const string_t& snm = string_t(""));  //!< constructor from a string_t
    Parameter(const string_t& s, const string_t& nm, const Strings& snm);                                 //!< constructor from a string_t
    Parameter(const char* s, const string_t& nm = string_t(""), const string_t& snm = string_t(""));      //!< constructor from a char array
    Parameter(const char* s, const string_t& nm, const Strings& snm);                                     //!< constructor from a char array
    Parameter(const void* p, const string_t& nm = string_t(""), const string_t& snm = string_t(""));      //!< constructor from a const void*
    Parameter(const void* p, const string_t& nm, const Strings& snm);                                     //!< constructor from a const void*

    // additional types managed through the void pointer
    //@{
    //! constructor from a std::vector<real_t>
    explicit Parameter(const std::vector<real_t>& rv, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const std::vector<real_t>& rv, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a std::vector<complex_t>
    explicit Parameter(const std::vector<complex_t>& cv, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const std::vector<complex_t>& cv, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a std::vector<int_t>
    explicit Parameter(const std::vector<int_t>& iv, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const std::vector<int_t>& iv, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a std::vector<string_t>
    explicit Parameter(const std::vector<string_t>& sv, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const std::vector<string_t>& sv, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a std::vector<bool>
    explicit Parameter(const std::vector<bool>& bv, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const std::vector<bool>& bv, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a Matrix<real_t>
    explicit Parameter(const Matrix<real_t>& rm, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const Matrix<real_t>& rm, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a Matrix<complex_t>
    explicit Parameter(const Matrix<complex_t>& cm, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const Matrix<complex_t>& cm, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a Point
    explicit Parameter(const Point& p, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const Point& p, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a vector of Point
    explicit Parameter(const std::vector<Point>& pv, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const std::vector<Point>& pv, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a std::vector<number_t>
    explicit Parameter(const std::vector<number_t>& nv, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const std::vector<number_t>& nv, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a Parametrization (defined in Parametrization.cpp)
    explicit Parameter(const Parametrization& par, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const Parametrization& par, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a Spline
    // explicit Parameter(const Spline& s, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    // explicit Parameter(const Spline& s, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a GeomDomain (defined in GeomDomain.cpp)
    explicit Parameter(const GeomDomain& dom, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const GeomDomain& dom, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a Function (defined in Function.cpp)
    explicit Parameter(const Function& fct, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const Function& fct, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a IntegrationMethod (defined in IntegrationMethod.cpp)
    explicit Parameter(const IntegrationMethod& im, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const IntegrationMethod& im, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a IntegrationMethod (defined in IntegrationMethod.cpp)
    explicit Parameter(const IntegrationMethods& ims, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const IntegrationMethods& ims, const string_t& nm, const Strings& snm);
    //@}
    //@{
    //! constructor from a TermVectors (defined in TermVector.cpp)
    explicit Parameter(const TermVectors& tvs, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    explicit Parameter(const TermVectors& tvs, const string_t& nm, const Strings& snm);
    //@}

#if __cplusplus >= 201103L
    //additional constructors from initializer list (c++2011)

    //! constructor from a initializer_list<real_t>
    Parameter(const std::initializer_list<real_t>& rs, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    //! constructor from a initializer_list<real_t>
    Parameter(const std::initializer_list<real_t>& rs, const string_t& nm, const Strings& snm);
    //! constructor from a initializer_list<int>
    Parameter(const std::initializer_list<int>& ns, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    //! constructor from a initializer_list<int>
    Parameter(const std::initializer_list<int>& ns, const string_t& nm, const Strings& snm);
    //! constructor from a initializer_list<number_t>
    Parameter(const std::initializer_list<number_t>& ns, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    //! constructor from a initializer_list<number_t>
    Parameter(const std::initializer_list<number_t>& ns, const string_t& nm, const Strings& snm);
    //! constructor from a initializer_list<string_t>
    Parameter(const std::initializer_list<string_t>& ss, const string_t& nm = string_t(""), const string_t& snm = string_t(""));
    //! constructor from a initializer_list<string_t>
    Parameter(const std::initializer_list<string_t>& ss, const string_t& nm, const Strings& snm);
#endif

    ~Parameter() { deletePointer(); } //!< parameter destructor
    void deletePointer();             //!< delete pointer if type is a vector or a matrix

    Parameter& operator=(const int_t i);                      //!< set to an int
    Parameter& operator=(const int i);                        //!< set to an int
    Parameter& operator=(const number_t i);                   //!< set to an number_t
    Parameter& operator=(const dimen_t i);                    //!< set to a dimen_t
    Parameter& operator=(const bool b);                       //!< set to a bool
    Parameter& operator=(const ClusteringMethod cm);          //!< set to a ClusteringMethod
    Parameter& operator=(const ComputationType ct);           //!< set to a ComputationType
    Parameter& operator=(const Ctype ct);                     //!< set to a Ctype
    Parameter& operator=(const DimensionType dt);             //!< set to a DimensionType
    Parameter& operator=(const EigenComputationalMode ecm);   //!< set to a EigenComputationalMode
    Parameter& operator=(const EigenSolverType est);          //!< set to a EigenSolverType
    Parameter& operator=(const EigenSortKind esk);            //!< set to a EigenSortKind
    Parameter& operator=(const FESubType fest);               //!< set to a FESubType
    Parameter& operator=(const FEType fet);                   //!< set to a FEType
    Parameter& operator=(const FunctionPart fp);              //!< set to a FunctionPart
    Parameter& operator=(const GeometricEndShape ges);        //!< set to a GeometricEndShape
    Parameter& operator=(const Gtype gt);                     //!< set to a Gtype
    Parameter& operator=(const HMApproximationMethod hmam);   //!< set to a HMApproximationMethod
    Parameter& operator=(const IntegrationMethodType imt);    //!< set to a IntegrationMethodType
    Parameter& operator=(const InterpolationType it);         //!< set to a InterpolationType
    Parameter& operator=(const IOFormat iof);                 //!< set to a IOFormat
    Parameter& operator=(const Iptype it);                    //!< set to a Iptype
    Parameter& operator=(const IterativeSolverType ist);      //!< set to an IterativeSolverType
    Parameter& operator=(const Language l);                   //!< set to a Language
    Parameter& operator=(const MeshGenerator mg);             //!< set to a MeshGenerator
    Parameter& operator=(const Objtype ot);                   //!< set to a Objtype
    Parameter& operator=(const Partitioning p);               //!< set to a Partitioning
    Parameter& operator=(const Ptype pt);                     //!< set to a Ptype
    Parameter& operator=(const QuadRule qr);                  //!< set to a Rtype
    Parameter& operator=(const ReductionMethodType rmt);      //!< set to a ReductionMethodType
    Parameter& operator=(const Rtype rt);                     //!< set to a Rtype
    Parameter& operator=(const ShapeType st);                 //!< set to a ShapeType
    Parameter& operator=(const SobolevType st);               //!< set to a SobolevType
    Parameter& operator=(const SplineBC sbc);                 //!< set to a SplineBC
    Parameter& operator=(const SplineParametrization sp);     //!< set to a SplineParametrization
    Parameter& operator=(const SplineSubtype sst);            //!< set to a SplineSubtype
    Parameter& operator=(const SplineType st);                //!< set to a SplineType
    Parameter& operator=(const StorageAccessType sat);        //!< set to a StorageAccessType
    Parameter& operator=(const StorageType st);               //!< set to a StorageType
    Parameter& operator=(const StructuredMeshSplitRule smsr); //!< set to a StructuredMeshSplitRule
    Parameter& operator=(const TransformType tt);             //!< set to a TransformType
    Parameter& operator=(const SymType st);                   //!< set to a SymType
    Parameter& operator=(const real_t r);                     //!< set to a real_t
    Parameter& operator=(const complex_t& c);                 //!< set to a complex_t
    Parameter& operator=(const string_t& s);                  //!< set to a string_t
    Parameter& operator=(const char* c);                      //!< set to a char array
    Parameter& operator=(const void* p);                      //!< set to a const void*
    Parameter& operator=(const std::vector<real_t>& rv);      //!< set to a std::vector<real_t>
    Parameter& operator=(const std::vector<complex_t>& cv);   //!< set to a std::vector<complex_t>
    Parameter& operator=(const std::vector<int_t>& iv);       //!< set to a std::vector<int_t>
    Parameter& operator=(const std::vector<bool>& bv);        //!< set to a std::vector<bool>
    Parameter& operator=(const std::vector<string_t>& sv);    //!< set to a std::vector<string_t>
    Parameter& operator=(const Matrix<real_t>& rm);           //!< set to a Matrix<real_t>
    Parameter& operator=(const Matrix<complex_t>& cm);        //!< set to a Matrix<complex_t>
    Parameter& operator=(const Point& pt);                    //!< set to a Point
    Parameter& operator=(const std::vector<Point>& ptv);      //!< set to a vector<Point>
    Parameter& operator=(const Points& pts);                  //!< set to a vector<Point>
    Parameter& operator=(const Strings& sv);                  //!< set to a Strings
    Parameter& operator=(const Reals& rv);                    //!< set to a Reals
    Parameter& operator=(const Complexes& cv);                //!< set to a Complexes
    Parameter& operator=(const std::vector<number_t>& nv);    //!< set to a std::vector<number_t>
    Parameter& operator=(const Numbers& nv);                  //!< set to a Numbers

    // for argument machinery, special cases implemented outside
    Parameter& operator=(const Parametrization&);            //!< set to a Parametrization parameter
    void setToSpline(const Spline&);                         //!< set to a Spline parameter
    Parameter& operator=(const Spline& s)                    //! set to a Spline parameter
    { setToSpline(s); return *this; }
    Parameter& operator=(const C2Spline& s)                  //! set to a Spline parameter
    { setToSpline(reinterpret_cast<const Spline&>(s)); return *this; }
    Parameter& operator=(const BSpline& s)                   //! set to a Spline parameter
    { setToSpline(reinterpret_cast<const Spline&>(s)); return *this; }
    Parameter& operator=(const BezierSpline& s)              //! set to a Spline parameter
    { setToSpline(reinterpret_cast<const Spline&>(s)); return *this; }
    Parameter& operator=(const CatmullRomSpline& s)          //! set to a Spline parameter
    { setToSpline(reinterpret_cast<const Spline&>(s)); return *this; }
    Parameter& operator=(const Nurbs& s)                     //! set to a Spline parameter
    { setToSpline(reinterpret_cast<const Spline&>(s)); return *this; }
    Parameter& operator=(const Function& fct);
    Parameter& operator=(const GeomDomain& dom);             //!< set to a GeomDomain
    void setToIntegrationMethod(const IntegrationMethod& im); //!< set to a IntegrationMethod parameter
    Parameter& operator=(const IntegrationMethod& im)        //!< set to a IntegrationMethod
    { setToIntegrationMethod(im); return *this; }
    Parameter& operator=(const CollinoIM& im)                //! set to a CollinoIM parameter
    { setToIntegrationMethod(reinterpret_cast<const IntegrationMethod&>(im)); return *this; }
    Parameter& operator=(const DuffyIM& im)                  //! set to a DuffyIM parameter
    { setToIntegrationMethod(reinterpret_cast<const IntegrationMethod&>(im)); return *this; }
    template <class T> Parameter& operator=(const FilonIMT<T>& im) //! set to a FilonIM parameter
    { setToIntegrationMethod(reinterpret_cast<const IntegrationMethod&>(im)); return *this; }
    Parameter& operator=(const HMatrixIM& im)                //! set to a HMatrixIM parameter
    { setToIntegrationMethod(reinterpret_cast<const IntegrationMethod&>(im)); return *this; }
    Parameter& operator=(const LenoirSalles2dIM& im)         //! set to a LenoirSalles2dIM parameter
    { setToIntegrationMethod(reinterpret_cast<const IntegrationMethod&>(im)); return *this; }
    Parameter& operator=(const LenoirSalles2dIR& im)         //! set to a LenoirSalles2dIR parameter
    { setToIntegrationMethod(reinterpret_cast<const IntegrationMethod&>(im)); return *this; }
    Parameter& operator=(const LenoirSalles3dIM& im)         //! set to a LenoirSalles3dIM parameter
    { setToIntegrationMethod(reinterpret_cast<const IntegrationMethod&>(im)); return *this; }
    Parameter& operator=(const LenoirSalles3dIR& im)         //! set to a LenoirSalles3dIR parameter
    { setToIntegrationMethod(reinterpret_cast<const IntegrationMethod&>(im)); return *this; }
    Parameter& operator=(const PolynomialIM& im)             //! set to a PolynomialIM parameter
    { setToIntegrationMethod(reinterpret_cast<const IntegrationMethod&>(im)); return *this; }
    Parameter& operator=(const QuadratureIM& im)             //! set to a QuadratureIM parameter
    { setToIntegrationMethod(reinterpret_cast<const IntegrationMethod&>(im)); return *this; }
    Parameter& operator=(const SauterSchwabIM& im)           //! set to a SauterSchwabIM parameter
    { setToIntegrationMethod(reinterpret_cast<const IntegrationMethod&>(im)); return *this; }
    Parameter& operator=(const SauterSchwabSymIM& im)        //! set to a SauterSchwabSymIM parameter
    { setToIntegrationMethod(reinterpret_cast<const IntegrationMethod&>(im)); return *this; }
    Parameter& operator=(const IntegrationMethods& ims);     //! set to a IntegrationMethods
    Parameter& operator=(const TermVectors& tvs);            //! set to a TermVectors
    Parameter& operator=(const Transformation& t);           //! set to a Transformation
    Parameter& operator=(const ClusterTree<FeDof>& t);       //! set to a ClusterTree<FeDof>

#if __cplusplus >= 201103L
    //additional assignment from initializer list (c++2011)
    Parameter& operator=(const std::initializer_list<real_t>&);    //!< set from a initializer_list<real_t>
    Parameter& operator=(const std::initializer_list<int>&);       //!< set from a initializer_list<int>
    Parameter& operator=(const std::initializer_list<number_t>&);       //!< set from a initializer_list<number_t>
    Parameter& operator=(const std::initializer_list<string_t>&);  //!< set from a initializer_list<string_t>
#endif

    //! set to any value
    template<typename T_> Parameter& operator=(T_ t)
    {
      if (p_!=nullptr) deletePointer();
      p_ = static_cast<void*>(new T_(t));
      type_=_pointer;
      return *this;
    }

    /* --------------------------------------------------------------------------------
       Accessors
    -------------------------------------------------------------------------------- */
    int_t get_i() const            //! get parameter int_t value
    { return i_; }
    number_t get_n() const         //! get parameter as number_t value
    { return number_t(i_); }
    number_t get_d() const         //! get parameter as dimen_t value
    { return dimen_t(i_); }
    real_t get_r() const           //! get parameter real value
    { return r_; }
    complex_t get_c() const        //! get parameter complex value
    { return c_; }
    string_t get_s() const         //! get parameter string_t value
    { return s_; }
    bool get_b() const             //! get parameter bool value
    { return b_; }
    const void* get_p() const      //! get parameter const void* value
    { return p_; }

    const std::vector<real_t>& get_rv() const    //! get parameter as std::vector<real_t>
    {
      if (type_!=_realVector)
        {
          where("Parameter::get_rv()");
          error("param_badtype", words("value", type_), name_);
        }
      if (p_==nullptr)
        {
          where("Parameter::get_rv()");
          error("null_pointer", "p_");
        }
      return *reinterpret_cast<const std::vector<real_t>*>(p_);
    }
    const std::vector<complex_t>& get_cv() const    //! get parameter as std::vector<complex_t>
    {
      if (type_!=_complexVector)
        {
          where("Parameter::get_cv()");
          error("param_badtype", words("value", type_), name_);
        }
      if (p_==nullptr)
        {
          where("Parameter::get_cv()");
          error("null_pointer", "p_");
        }
      return *reinterpret_cast<const std::vector<complex_t>*>(p_);
    }

    const std::vector<int_t>& get_iv() const    //! get parameter as std::vector<int_t>
    {
      if (type_!=_integerVector)
        {
          where("Parameter::get_iv()");
          error("param_badtype", words("value", type_), name_);
        }
      if (p_==nullptr)
        {
          where("Parameter::get_iv()");
          error("null_pointer", "p_");
        }
      return *reinterpret_cast<const std::vector<int_t>*>(p_);
    }

    std::vector<number_t> get_nv() const    //! get parameter as std::vector<number_t>
    {
      if (type_!=_integerVector)
        {
          where("Parameter::get_nv()");
          error("param_badtype", words("value", type_), name_);
        }
      if (p_==nullptr)
        {
          where("Parameter::get_nv()");
          error("null_pointer", "p_");
        }

      const std::vector<int_t>& iv=*reinterpret_cast<const std::vector<int_t>*>(p_);
      std::vector<number_t> nv(iv.size());
      std::vector<int_t>::const_iterator iti=iv.begin();
      std::vector<number_t>::iterator itn=nv.begin();
      for (; iti!=iv.end(); ++iti,++itn) *itn=intToNum(*iti,"Parameter::get_nv");
      return nv;
    }

    const std::vector<string_t>& get_sv() const    //! get parameter as std::vector<string_t>
    {
      if (type_!=_stringVector)
        {
          where("Parameter::get_sv()");
          error("param_badtype", words("value", type_), name_);
        }
      if (p_==nullptr)
        {
          where("Parameter::get_sv()");
          error("null_pointer", "p_");
        }
      return *reinterpret_cast<const std::vector<string_t>*>(p_);
    }

    const std::vector<bool>& get_bv() const    //! get parameter as std::vector<bool>
    {
      if (type_!=_boolVector)
        {
          where("Parameter::get_bv()");
          error("param_badtype", words("value", type_), name_);
        }
      if (p_==nullptr)
        {
          where("Parameter::get_bv()");
          error("null_pointer", "p_");
        }
      return *reinterpret_cast<const std::vector<bool>*>(p_);
    }

    const Matrix<real_t>& get_rm() const         //! get parameter as Matrix<real_t>
    {
      if (type_!=_realMatrix)
        {
          where("Parameter::get_rm()");
          error("param_badtype", words("value", type_), name_);
        }
      if (p_==nullptr)
        {
          where("Parameter::get_rm()");
          error("null_pointer", "p_");
        }
      return *reinterpret_cast<const Matrix<real_t>*>(p_);
    }

    const Matrix<complex_t>& get_cm() const    //! get parameter as Matrix<complex_t>
    {
      if (type_!=_complexMatrix)
        {
          where("Parameter::get_cm()");
          error("param_badtype", words("value", type_), name_);
        }
      if (p_==nullptr)
        {
          where("Parameter::get_cm()");
          error("null_pointer", "p_");
        }
      return *reinterpret_cast<const Matrix<complex_t>*>(p_);
    }

    const Point& get_pt() const    //! get parameter as Point
    {
      if (type_!=_pt)
        {
          where("Parameter::get_pt()");
          error("param_badtype", words("value", type_), name_);
        }
      if (p_==nullptr)
        {
          where("Parameter::get_pt()");
          error("null_pointer", "p_");
        }
      return *reinterpret_cast<const Point*>(p_);
    }

    const std::vector<Point>& get_ptv() const    //! get parameter as Point
    {
      if (type_!=_ptVector)
        {
          where("Parameter::get_ptv()");
          error("param_badtype", words("value", type_), name_);
        }
      if (p_==nullptr)
        {
          where("Parameter::get_ptv()");
          error("null_pointer", "p_");
        }
      return *reinterpret_cast<const std::vector<Point>*>(p_);
    }

    ValueType type() const          //! get parameter type
    { return type_; }
    ParameterKey key() const        //! get parameter key
    { return key_; }
    string_t typeName() const       //! get parameter type as string
    { return words("value",type_); }
    string_t name() const           //! get name
    { return name_; }
    string_t shortname(number_t i) const //! get i-eth shortname
    { return shortnames_[i]; }
    Strings shortnames() const           //! get shortnames
    { return shortnames_; }
    void name(const string_t& nm)        //! set name
    { name_ = nm; }
    void shortnames(const string_t& snm) //! set shortnames
    { shortnames_ = Strings(snm); }
    void shortnames(const Strings& snm)  //! set shortnames
    { shortnames_ = snm; }

    /* --------------------------------------------------------------------------------
       automatic cast operators
    -------------------------------------------------------------------------------- */
    operator int();                    //!< cast Parameter to int
    operator int_t();                  //!< cast Parameter to int_t
    operator number_t();               //!< cast Parameter to number_t
    operator bool();                   //!< cast Parameter to bool
    operator real_t();                 //!< cast Parameter to real_t
    operator complex_t();              //!< cast Parameter to complex_t
    operator string_t();               //!< cast Parameter to string_t
    operator const void* ();           //!< cast Parameter to const void*
    operator std::vector<real_t>();    //!< cast Parameter to std real_t vector
    operator std::vector<complex_t>(); //!< cast Parameter to std complex_t vector
    operator std::vector<int_t>();     //!< cast Parameter to std int_t vector
    operator std::vector<bool>();      //!< cast Parameter to std bool vector
    operator std::vector<string_t>();  //!< cast Parameter to std string_t vector
    operator Matrix<real_t>();         //!< cast Parameter to real matrix
    operator Matrix<complex_t>();      //!< cast Parameter to complex matrix
    operator Point();                  //!< cast Parameter to point
    operator std::vector<Point>();     //!< cast Parameter to point vector
    operator Reals();                  //!< cast Parameter to Reals
    operator Complexes();              //!< cast Parameter to Complexes
//    operator Vector<real_t>();         //!< cast Parameter to xlife++ real vector
//    operator Vector<complex_t>();      //!< cast Parameter to xlife++ complex vector
    operator std::vector<number_t>();  //!< cast Parameter to std number_t vector
    operator Numbers();                //!< cast Parameter to Numbers
    operator Strings();                //!< cast Parameter to Strings

    /* --------------------------------------------------------------------------------
       internal algebric operators with auto cast when possible
    -------------------------------------------------------------------------------- */
    Parameter& operator+=(const int_t);       //!< add an int_t
    Parameter& operator+=(const int);         //!< add an int
    Parameter& operator+=(const number_t);    //!< add a number_t
    Parameter& operator+=(const real_t);      //!< add a real_t
    Parameter& operator+=(const complex_t&);  //!< add a complex_t
    Parameter& operator+=(const string_t&);   //!< add a string_t
    Parameter& operator+=(const char*);       //!< add a char array
    Parameter& operator+=(const Parameter&);  //!< add a parameter
    Parameter& operator-=(const int_t);       //!< subtract an int_t
    Parameter& operator-=(const int);         //!< subtract an int
    Parameter& operator-=(const number_t);    //!< subtract a number_t
    Parameter& operator-=(const real_t);      //!< subtract a real_t
    Parameter& operator-=(const complex_t&);  //!< subtract a complex_t
    Parameter& operator-=(const Parameter&);  //!< subtract a parameter
    Parameter& operator*=(const int_t);       //!< multiply by an int_t
    Parameter& operator*=(const int);         //!< multiply by an int
    Parameter& operator*=(const number_t);    //!< multiply by a number_t
    Parameter& operator*=(const real_t);      //!< multiply by a real_t
    Parameter& operator*=(const complex_t&);  //!< multiply by a complex_t
    Parameter& operator*=(const Parameter&);  //!< multiply by a parameter
    Parameter& operator/=(const int_t);       //!< divide by an int_t
    Parameter& operator/=(const int);         //!< divide by an int
    Parameter& operator/=(const number_t);    //!< divide by a number_t
    Parameter& operator/=(const real_t);      //!< divide by a real_t
    Parameter& operator/=(const complex_t&);  //!< divide by a complex_t
    Parameter& operator/=(const Parameter&);  //!< divide by a parameter

    /* --------------------------------------------------------------------------------
       comparison operators
    -------------------------------------------------------------------------------- */
    bool operator==(const Parameter&);  //!< compare to a parameter
    bool operator==(const int_t);       //!< compare to an int_t
    bool operator==(const int);         //!< compare to an int
    bool operator==(const bool);        //!< compare to a bool
    bool operator==(const number_t);    //!< compare to a number_t
    bool operator==(const real_t);      //!< compare to a real
    bool operator==(const complex_t&);  //!< compare to a complex
    bool operator==(const string_t&);   //!< compare to a string_t
    bool operator==(const char*);       //!< compare to a string_t char
    bool operator>(const Parameter&);   //!< compare to a parameter
    bool operator>(const int_t);        //!< compare to an int_t
    bool operator>(const int);          //!< compare to an int
    bool operator>(const number_t);     //!< compare to an int_t
    bool operator>(const real_t);       //!< compare to a real
    bool operator>(const complex_t&);   //!< compare to a complex
    bool operator>(const string_t&);    //!< compare to a string_t

    template<typename T>
    bool operator!=(const T& t) //! returns true if parameter is not t
    {
      return !((*this) == t);
    }
    template<typename T>
    bool operator<=(const T& t) //! returns true if parameter is less than or equal to t
    {
      return !((*this) > t);
    }
    template<typename T>
    bool operator>=(const T& t) //! returns true if parameter is greater than or equal to t
    {
      return ((*this) > t || (*this) == t);
    }
    template<typename T>
    bool operator< (const T& t) //! returns true if parameter if less than t
    {
      return !((*this) >= t);
    }

    /* --------------------------------------------------------------------------------
       I/O utilities
    -------------------------------------------------------------------------------- */
    friend std::ostream& operator<<(std::ostream&, const Parameter&);
    friend std::istream& operator>>(std::istream&, Parameter&);

    //@{
    //! error handlers
    void illegalOperation(const string_t& t1, const string_t& op, const string_t& t2) const;
    void illegalOperation2(const string_t& op, const string_t& t) const;
    void undefinedParameter(const string_t& t) const ;
    void illegalDivision() const;
    //@}

}; // end of class Parameter

//=========================================================================================
// extern definitions related to class Parameter
//=========================================================================================
std::ostream& operator<<(std::ostream&, const Parameter&); //!< output operator
std::istream& operator>>(std::istream&, Parameter&);       //!< insertion operator

inline std::ostream& operator<<(std::ostream& os, const ParameterKey& pk) { os << words("param key",pk); return os; }
std::ostream& operator<<(std::ostream&, const std::set<ParameterKey>&);

//void print(const Parameter&);            //!< extern print to default ofstream
ValueType get_value_type(const string_t&); //!< get the type of the parameter value by name
/*--------------------------------------------------------------------------------
    extern casting operator
--------------------------------------------------------------------------------*/
int_t integer(const Parameter&);   //!< cast to int
real_t real(const Parameter&);     //!< cast to real_t
complex_t cmplx(const Parameter&); //!< cast to complex_t
string_t str(const Parameter&);    //!< cast to string_t
const void* pointer(const Parameter&); //!< cast to const void *

/*--------------------------------------------------------------------------------
 extern algebraic operators
--------------------------------------------------------------------------------*/
Parameter operator+(const Parameter& p1, const Parameter& p2); //!< add 2 parameters
Parameter operator+(const Parameter& p, const int_t v);        //!< add parameter and int_t
Parameter operator+(const Parameter& p, const int v);          //!< add parameter and int
Parameter operator+(const Parameter& p, const number_t v);     //!< add parameter and number_t
Parameter operator+(const Parameter& p, const real_t v);       //!< add parameter and real
Parameter operator+(const Parameter& p, const complex_t& v);   //!< add parameter and complex
Parameter operator+(const Parameter& p, const string_t& v);    //!< add parameter and string
Parameter operator+(const Parameter& p, const char* v);        //!< add parameter and char*
Parameter operator+(const int_t v, const Parameter& p);        //!< add int_t and parameter
Parameter operator+(const int v, const Parameter& p);          //!< add int and parameter
Parameter operator+(const number_t v, const Parameter& p);     //!< add number_t and parameter
Parameter operator+(const real_t v, const Parameter& p);       //!< add real and parameter
Parameter operator+(const complex_t& v, const Parameter& p);   //!< add complex and parameter
Parameter operator+(const string_t& v, const Parameter& p);    //!< add string and parameter
Parameter operator+(const char* v, const Parameter& p);        //!< add char* and parameter
Parameter operator-(const Parameter& p1, const Parameter& p2); //!< difference of 2 parameters
Parameter operator-(const Parameter& p, const int_t v);        //!< difference parameter and int_t
Parameter operator-(const Parameter& p, const int v);          //!< difference parameter and int
Parameter operator-(const Parameter& p, const number_t v);     //!< difference parameter and number_t
Parameter operator-(const Parameter& p, const real_t v);       //!< difference parameter and real
Parameter operator-(const Parameter& p, const complex_t& v);   //!< difference parameter and complex
Parameter operator-(const int_t v, const Parameter& p);        //!< difference int_t and parameter
Parameter operator-(const int v, const Parameter& p);          //!< difference int and parameter
Parameter operator-(const number_t v, const Parameter& p);     //!< difference number_t and parameter
Parameter operator-(const real_t v, const Parameter& p);       //!< difference real and parameter
Parameter operator-(const complex_t& v, const Parameter& p);   //!< difference complex and parameter
Parameter operator*(const Parameter& p1, const Parameter& p2); //!< product of 2 parameters
Parameter operator*(const Parameter& p, const int_t v);        //!< product parameter and int_t
Parameter operator*(const Parameter& p, const int v);          //!< product parameter and int
Parameter operator*(const Parameter& p, const number_t v);     //!< product parameter and number_t
Parameter operator*(const Parameter& p, const real_t v);       //!< product parameter and real
Parameter operator*(const Parameter& p, const complex_t& v);   //!< product parameter and complex
Parameter operator*(const int_t v, const Parameter& p);        //!< product int_t and parameter
Parameter operator*(const int v, const Parameter& p);          //!< product int and parameter
Parameter operator*(const number_t v, const Parameter& p);     //!< product number_t and parameter
Parameter operator*(const real_t v, const Parameter& p);       //!< product real and parameter
Parameter operator*(const complex_t& v, const Parameter& p);   //!< product complex and parameter
Parameter operator/(const Parameter& p1, const Parameter& p2); //!< division of 2 parameters
Parameter operator/(const Parameter& p, const int_t v);        //!< division parameter and int
Parameter operator/(const Parameter& p, const int v);          //!< division parameter and int_t
Parameter operator/(const Parameter& p, const number_t v);     //!< division parameter and number_t
Parameter operator/(const Parameter& p, const real_t v);       //!< division parameter and real
Parameter operator/(const Parameter& p, const complex_t& v);   //!< division parameter and complex
Parameter operator/(const int_t v, const Parameter& p);        //!< division int_t and parameter
Parameter operator/(const int v, const Parameter& p);          //!< division int and parameter
Parameter operator/(const number_t v, const Parameter& p);     //!< division number_t and parameter
Parameter operator/(const real_t v, const Parameter& p);       //!< division real and parameter
Parameter operator/(const complex_t& v, const Parameter& p);   //!< division complex and parameter

// template<>
inline Parameter& Parameter::operator=(const int_t i)
{
  i_ = i;
  type_ = _integer;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const int i)
{
  i_ = i;
  type_ = _integer;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const number_t i)
{
  i_ = static_cast<int_t>(i);
  type_ = _integer;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const dimen_t i)
{
  i_ = static_cast<int_t>(i);
  type_ = _integer;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const bool b)
{
  b_ = b;
  type_ = _bool;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const ClusteringMethod cm)
{
  i_ = static_cast<int_t>(cm);
  type_ = _enumClusteringMethod;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const ComputationType ct)
{
  i_ = static_cast<int_t>(ct);
  type_ = _enumComputationType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Ctype ct)
{
  i_ = static_cast<int_t>(ct);
  type_ = _enumCtype;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const DimensionType dt)
{
  i_ = static_cast<int_t>(dt);
  type_ = _enumDimensionType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const EigenComputationalMode ecm)
{
  i_ = static_cast<int_t>(ecm);
  type_ = _enumEigenComputationalMode;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const EigenSolverType est)
{
  i_ = static_cast<int_t>(est);
  type_ = _enumEigenSolverType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const EigenSortKind esk)
{
  i_ = static_cast<int_t>(esk);
  type_ = _enumEigenSortKind;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const FESubType fest)
{
  i_ = static_cast<int_t>(fest);
  type_ = _enumFESubType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const FEType fet)
{
  i_ = static_cast<int_t>(fet);
  type_ = _enumFEType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const FunctionPart fp)
{
  i_ = static_cast<int_t>(fp);
  type_ = _enumFunctionPart;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Gtype gt)
{
  i_ = static_cast<int_t>(gt);
  type_ = _enumGtype;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const GeometricEndShape ges)
{
  i_ = static_cast<int_t>(ges);
  type_ = _enumGeometricEndShape;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const HMApproximationMethod hmam)
{
  i_ = static_cast<int_t>(hmam);
  type_ = _enumHMApproximationMethod;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const IntegrationMethodType imt)
{
  i_ = static_cast<int_t>(imt);
  type_ = _enumIntegrationMethodType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const InterpolationType it)
{
  i_ = static_cast<int_t>(it);
  type_ = _enumInterpolationType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const IOFormat iof)
{
  i_ = static_cast<int_t>(iof);
  type_ = _enumIOFormat;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Iptype it)
{
  i_ = static_cast<int_t>(it);
  type_ = _enumIptype;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const IterativeSolverType ist)
{
  i_ = static_cast<int_t>(ist);
  type_ = _enumIterativeSolverType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Language l)
{
  i_ = static_cast<int_t>(l);
  type_ = _enumLanguage;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const MeshGenerator mg)
{
  i_ = static_cast<int_t>(mg);
  type_ = _enumMeshGenerator;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Objtype ot)
{
  i_ = static_cast<int_t>(ot);
  type_ = _enumObjtype;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Partitioning p)
{
  i_ = static_cast<int_t>(p);
  type_ = _enumPartitioning;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Ptype pt)
{
  i_ = static_cast<int_t>(pt);
  type_ = _enumPtype;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const QuadRule qr)
{
  i_ = static_cast<int_t>(qr);
  type_ = _enumQuadRule;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const ReductionMethodType rmt)
{
  i_ = static_cast<int_t>(rmt);
  type_ = _enumReductionMethodType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Rtype rt)
{
  i_ = static_cast<int_t>(rt);
  type_ = _enumRtype;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const ShapeType st)
{
  i_ = static_cast<int_t>(st);
  type_ = _enumShapeType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const SobolevType st)
{
  i_ = static_cast<int_t>(st);
  type_ = _enumSobolevType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const SplineBC sbc)
{
  i_ = static_cast<int_t>(sbc);
  type_ = _enumSplineBC;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const SplineParametrization sp)
{
  i_ = static_cast<int_t>(sp);
  type_ = _enumSplineParametrization;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const SplineSubtype sst)
{
  i_ = static_cast<int_t>(sst);
  type_ = _enumSplineSubtype;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const SplineType st)
{
  i_ = static_cast<int_t>(st);
  type_ = _enumSplineType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const StorageAccessType sat)
{
  i_ = static_cast<int_t>(sat);
  type_ = _enumStorageAccessType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const StorageType st)
{
  i_ = static_cast<int_t>(st);
  type_ = _enumStorageType;
  return *this;
}

inline Parameter& Parameter::operator=(const StructuredMeshSplitRule smsr)
{
  i_ = static_cast<int_t>(smsr);
  type_ = _enumStructuredMeshSplitRule;
  return *this;
}

inline Parameter& Parameter::operator=(const TransformType tt)
{
  i_ = static_cast<int_t>(tt);
  type_ = _enumTransformType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const SymType st)
{
  i_ = static_cast<int_t>(st);
  type_ = _enumSymType;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const real_t r)
{
  r_ = r ;
  type_ = _real ;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const complex_t& c)
{
  c_ = c;
  type_ = _complex;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const string_t& s)
{
  s_ = s;
  type_ = _string;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const char* s)
{
  s_ = string_t(s);
  type_ = _string;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const void* p)
{
  p_ = p;
  type_ = _pointer;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const std::vector<real_t>& rv)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new std::vector<real_t>(rv));
  type_=_realVector;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const std::vector<complex_t>& cv)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new std::vector<complex_t>(cv));
  type_=_complexVector;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const std::vector<int_t>& iv)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new std::vector<int_t>(iv));
  type_=_integerVector;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const std::vector<bool>& bv)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new std::vector<bool>(bv));
  type_=_boolVector;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const std::vector<string_t>& sv)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new std::vector<string_t>(sv));
  type_=_stringVector;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Matrix<real_t>& rm)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new Matrix<real_t>(rm));
  type_=_realMatrix;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Matrix<complex_t>& cm)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new Matrix<complex_t>(cm));
  type_=_complexMatrix;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Point& pt)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new Point(pt));
  type_=_pt;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const std::vector<Point>& ptv)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new std::vector<Point>(ptv));
  type_=_ptVector;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Points& pts)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new Points(pts));
  type_=_ptVector;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Strings& sv)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new std::vector<string_t>(sv));
  type_=_stringVector;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Reals& rv)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new std::vector<real_t>(rv));
  type_=_realVector;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Complexes& rv)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new std::vector<complex_t>(rv));
  type_=_complexVector;
  return *this;
}

//template<>
inline Parameter& Parameter::operator=(const std::vector<number_t>& nv)
{
  if (p_!=nullptr) deletePointer();
  std::vector<int_t>& iv= *new std::vector<int_t>(nv.size());  //cast to vector<int_t>
  std::vector<int_t>::iterator iti=iv.begin();
  std::vector<number_t>::const_iterator itl=nv.begin();
  for (; itl!=nv.end(); ++itl,++iti) *iti=numToInt(*itl);
  p_ = static_cast<void*>(&iv);
  type_=_integerVector;
  return *this;
}

// template<>
inline Parameter& Parameter::operator=(const Numbers& nv)
{
  if (p_!=nullptr) deletePointer();
  std::vector<int_t>& iv= *new std::vector<int_t>(nv.size());  //cast to vector<int_t>
  std::vector<int_t>::iterator iti=iv.begin();
  std::vector<number_t>::const_iterator itl=nv.begin();
  for (; itl!=nv.end(); ++itl,++iti) *iti=numToInt(*itl);
  p_ = static_cast<void*>(&iv);
  type_=_integerVector;
  return *this;
}

void deleteGeomDomain(void* p);               // special delete, implemented in GeomDomain.cpp
void* cloneGeomDomain(const void* p);         // special clone, implemented in GeomDomain.cpp
void deleteFunction(void* p);                 // special delete, implemented in Function.cpp
void* cloneFunction(const void* p);           // special clone, implemented in Function.cpp
void deleteIntegrationMethod(void* p);        // special delete, implemented in IntegrationMethod.cpp
void* cloneIntegrationMethod(const void* p);  // special clone, implemented in IntegrationMethod.cpp
void deleteIntegrationMethods(void* p);       // special delete, implemented in IntegrationMethod.cpp
void* cloneIntegrationMethods(const void* p); // special clone, implemented in IntegrationMethod.cpp
void deleteParametrization(void* p);          // special delete, implemented in Parametrization.cpp
void* cloneParametrization(const void* p);    // special clone, implemented in Parametrization.cpp
void deleteSpline(void* );                    // special delete, implemented in Spline.cpp
void* cloneSpline(const void* p);             // special clone, implemented in Spline.cpp
void deleteTermVectors(void* p);              // special delete, implemented in TermVector.cpp
void* cloneTermVectors(const void* p);        // special clone, implemented in TermVector.cpp
void deleteTransformation(void* p);           // special delete, implemented in Transformation.cpp
void* cloneTransformation(const void* p);     // special clone, implemented in Transformation.cpp
void deleteClusterTreeFeDof(void* p);         // special delete, implemented in ClusterTree.cpp
void* cloneClusterTreeFeDof(const void* p);   // special clone, implemented in ClusterTree.cpp

//@{
//! extern main numerical function, automatic cast (not defined for string_t arg !)
//  disabled to avoid conflicts with std functions !
// Parameter abs(const Parameter&);
// Parameter conj(const Parameter&);
// Parameter sqrt(const Parameter&);
// Parameter log(const Parameter&);
// Parameter log10(const Parameter&);
// Parameter exp(const Parameter&);
// Parameter sin(const Parameter&);
// Parameter cos(const Parameter&);
// Parameter tan(const Parameter&);
// Parameter sinh(const Parameter&);
// Parameter cosh(const Parameter&);
// Parameter tanh(const Parameter&);
// Parameter acos(const Parameter&);
// Parameter asin(const Parameter&);
// Parameter atan(const Parameter&);
// Parameter acosh(const Parameter&);
// Parameter asinh(const Parameter&);
// Parameter atanh(const Parameter&);
// Parameter pow(const Parameter&, const Parameter&);
// Parameter pow(const Parameter&, int_t);
// Parameter pow(const Parameter&, int);
// Parameter pow(const Parameter&, number_t);
// Parameter pow(const Parameter&, real_t);
// Parameter pow(const Parameter&, const complex_t&);
//@}
// other functions to be defined later

/*!
  \struct ParameterTrait
  \tparam K type to work with
  Trait to process parameter list
*/
template<typename K>
struct ParameterTrait
{
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<>
struct ParameterTrait<int_t>
{
  static inline int_t get(const Parameter& para) { return para.get_i();}
  static inline int_t set(const string_t& name, const int_t value) { return value;}
};

template<>
struct ParameterTrait<int>
{
  static inline int get(const Parameter& para) { return para.get_i();}
  static inline int set(const string_t& name, const int value) { return value;}
};

template<>
struct ParameterTrait<bool>
{
  static inline bool get(const Parameter& para) { return para.get_b();}
  static inline bool set(const string_t& name, const bool value) { return value;}
};

template<>
struct ParameterTrait<number_t>
{
  static inline number_t get(const Parameter& para) { return para.get_i();}
  static inline number_t set(const string_t& name, const number_t value) { return value;}
};

template<>
struct ParameterTrait<dimen_t>
{
  static inline dimen_t get(const Parameter& para) { return dimen_t(para.get_i());}
  static inline dimen_t set(const string_t& name, const dimen_t value) { return value;}
};

template<>
struct ParameterTrait<real_t>
{
  static inline real_t get(const Parameter& para) { return para.get_r();}
  static inline real_t set(const string_t& name, const real_t value) { return value; }
};

template<>
struct ParameterTrait<string_t>
{
  static inline string_t get(const Parameter& para) { return para.get_s();}
  static inline string_t set(const string_t& name, const string_t& value) { return value;}
};

template<>
struct ParameterTrait<const void*>
{
  static inline const void* get(const Parameter& para) { return para.get_p(); }
  static inline Parameter set(const string_t& name, const void* value) { Parameter para(const_cast<const void*>(value), name); return para;}
};

template<>
struct ParameterTrait<complex_t>
{
  static inline complex_t get(const Parameter& para) { return para.get_c();}
  static inline complex_t set(const string_t& name, const complex_t& value) { return value;}
};

template<>
struct ParameterTrait<std::vector<real_t> >
{
  static inline const std::vector<real_t>& get(const Parameter& para) { return para.get_rv();}
  static inline const std::vector<real_t>& set(const string_t& name, const std::vector<real_t>& value) { return value;}
};

template<>
struct ParameterTrait<Reals>
{
  static inline const Reals& get(const Parameter& para) { return reinterpret_cast<const Reals&>(para.get_rv());}
  static inline const Reals& set(const string_t& name, const Reals& value) { return value;}
};

template<>
struct ParameterTrait<Complexes>
{
  static inline const Complexes& get(const Parameter& para) { return reinterpret_cast<const Complexes&>(para.get_cv());}
  static inline const Complexes& set(const string_t& name, const Complexes& value) { return value;}
};

template<>
struct ParameterTrait<std::vector<complex_t> >
{
  static inline const std::vector<complex_t>& get(const Parameter& para) { return para.get_cv();}
  static inline const std::vector<complex_t>& set(const string_t& name, const std::vector<complex_t>& value) { return value;}
};

template<>
struct ParameterTrait<std::vector<int_t> >
{
  static inline const std::vector<int_t>& get(const Parameter& para) { return para.get_iv();}
  static inline const std::vector<int_t>& set(const string_t& name, const std::vector<int_t>& value) { return value;}
};

template<>
struct ParameterTrait<std::vector<bool> >
{
  static inline const std::vector<bool>& get(const Parameter& para) { return para.get_bv();}
  static inline const std::vector<bool>& set(const string_t& name, const std::vector<bool>& value) { return value;}
};

template<>
struct ParameterTrait<std::vector<number_t> >
{
  static inline const std::vector<number_t> get(const Parameter& para) { return para.get_nv();}
  static inline const std::vector<number_t> set(const string_t& name, const std::vector<number_t>& value) { return value;}
};

template<>
struct ParameterTrait<Numbers>
{
  static inline const Numbers get(const Parameter& para) { return para.get_nv();}
  static inline const Numbers set(const string_t& name, const Numbers& value) { return value;}
};

template<>
struct ParameterTrait<std::vector<string_t> >
{
  static inline const std::vector<string_t>& get(const Parameter& para) { return para.get_sv();}
  static inline const std::vector<string_t>& set(const string_t& name, const std::vector<string_t>& value) { return value;}
};

template<>
struct ParameterTrait<Strings>
{
  static inline const Strings get(const Parameter& para) { return Strings(para.get_sv());}
  static inline const Strings set(const string_t& name, const Strings& value) { return value;}
};

//template<>
//struct ParameterTrait<Vector<real_t> >
//{
//  static inline const Vector<real_t> get(const Parameter& para) { return Vector<real_t>(para.get_rv());}
//  static inline const Vector<real_t> set(const string_t& name, const Vector<real_t>& value) { return value;}
//};
//
//template<>
//struct ParameterTrait<Vector<complex_t> >
//{
//  static inline const Vector<complex_t> get(const Parameter& para) { return Vector<complex_t>(para.get_cv());}
//  static inline const Vector<complex_t> set(const string_t& name, const Vector<complex_t>& value) { return value;}
//};

template<>
struct ParameterTrait<Matrix<real_t> >
{
  static inline const Matrix<real_t>& get(const Parameter& para) { return para.get_rm();}
  static inline const Matrix<real_t>& set(const string_t& name, const Matrix<real_t>& value) { return value;}
};

template<>
struct ParameterTrait<Matrix<complex_t> >
{
  static inline const Matrix<complex_t>& get(const Parameter& para) { return para.get_cm();}
  static inline const Matrix<complex_t>& set(const string_t& name, const Matrix<complex_t>& value) { return value;}
};

template<>
struct ParameterTrait<Point>
{
  static inline const Point& get(const Parameter& para) { return para.get_pt();}
  static inline const Point& set(const string_t& name, const Point& value) { return value;}
};

template<>
struct ParameterTrait<std::vector<Point> >
{
  static inline const std::vector<Point>& get(const Parameter& para) { return para.get_ptv();}
  static inline const std::vector<Point>& set(const string_t& name, const std::vector<Point>& value) { return value;}
};

template<>
struct ParameterTrait<Parametrization>
{
  static inline const Parametrization& get(const Parameter& para) { return *reinterpret_cast<const Parametrization*>(para.get_p());}
  static inline const Parametrization& set(const string_t& name, const Parametrization& value) { return value;}
};

template<>
struct ParameterTrait<Spline>
{
  static inline const Spline& get(const Parameter& para) { return *reinterpret_cast<const Spline*>(para.get_p());}
  static inline const Spline& set(const string_t& name, const Spline& value) { return value;}
};

template<>
struct ParameterTrait<IntegrationMethod>
{
  static inline const IntegrationMethod& get(const Parameter& para) { return *reinterpret_cast<const IntegrationMethod*>(para.get_p());}
  static inline const IntegrationMethod& set(const string_t& name, const IntegrationMethod& value) { return value;}
};

// template<>
// struct ParameterTrait<bool>
// {
//   static inline bool get(const Parameter& para)
//   {
//     int_t val = para.get_i();
//     if (1 == val) return true;
//     else return false;
//   }

//   static inline int set(const string_t& name, const bool value)
//   {
//     int intVal = 0;
//     if (value) intVal = 1;
//     return intVal;
//   }
// };

#endif

//! check if a name is not a system key name
bool isNameAvailable(const string_t& keyname);
//! check if the current line is not a comment
bool isComment(const string_t& line);

/*!
  \class Parameters
  encapsulates a list of Parameter's.

  creation:
      \code
      Parameter height=3;
      Parameters data;
      data << height << Parameter(4,"width") << "case 1" << 1.5;
      \endcode

  string_t "case 1" has default name "parameter3"
  real_t "1.5" has default name "parameter4"

  usage:
      \code
      Parameter a=data("height");  // access by name, contains height
      Parameter c=data(4);         // access by rank, contains 1.5
      data(1)= 2;                  // replace the value 3 by 2
      data("height")=2;            // same effect
      data(height)=2;              // same effect, caution here as object height is hereafter modified
      double x=data(4);            // x contains 1.5
      x=data("width");             // x contains 4
      \endcode

  No deletion of list element is possible
  No algebraic operations are performed on Parameters
  Automatic parameter naming is a crutch for tired users,
  it is advised that the user manages names of parameters:
      \code
      Parameter height=3;
      Parameter width=4;
      Parameter wavenumber=1.5;
      Parameter comment="case 1";
      Parameters data;
      data << height << width << wavenumber << comment;
      \endcode
 */
class Parameters
{
  public:
    std::vector<Parameter*> list_;               //!< parameters (pointer) list
    bool freeParams_;                            //!< flag to delete parameter pointers
  private:
    std::map<string_t, number_t> indexFromName_;  //!< alpha index to maintain alphabetical order
    std::map<string_t, number_t> indexFromShortNames_;  //!< alpha index to maintain alphabetical order

  public:
    /* ----------------------------------------------------------------------------
       constructors
    ---------------------------------------------------------------------------- */
    // use compiler supplied assignment operator=
    Parameters();                                                                            //!< default constructor
    Parameters(const Parameters&);                                            //!< copy constructor with full copy behavior
    Parameters(Parameter&);                                                        //!< constructor with a Parameter
    Parameters(const void*, const string_t& nm = string_t(""), const string_t& snm = string_t("")); //!< constructor with a void pointer
    Parameters(const void*, const string_t& nm, const Strings& snm); //!< constructor with a void pointer
    template<typename T>
    Parameters(const T& t, const string_t& nm = string_t(""), const string_t& snm = string_t(""))    //! constructor with T type
      : list_(0)
    {
      string_t na=nm;
      if (na.empty()) na="parameter1";
      push(*new Parameter(t, na, snm));
      freeParams_=true;
    }
    template<typename T>
    Parameters(const T& t, const string_t& nm, const Strings& snm)    //! constructor with T type
      : list_(0)
    {
      string_t na=nm;
      if (na.empty()) na="parameter1";
      push(*new Parameter(t, na, snm));
      freeParams_=true;
    }
    Parameters& operator=(const Parameters& pars);   //!< assign operator with full copy behavior
    ~Parameters();                                                              //!< destructor

    number_t size() const {return list_.size();}                  //!< return the number of parameters
    /* ----------------------------------------------------------------------------
       list insertion
    ---------------------------------------------------------------------------- */
    void push(Parameter&);                    //!< append a parameter into list
    void push(const Parameters&);             //!< append a list of parameters
    Parameters& operator<<(Parameter*);       //!< insert a parameter into list via stream
    Parameters& operator<<(Parameter&);       //!< insert a parameter into list via stream
    Parameters& operator<<(const Parameter&); //!< insert a parameter into list via stream
    Parameters& operator<<(const void*);      //!< insert a void pointer into list via stream
    template<typename T>
    Parameters& operator<<(const T& t) // insert a T value in list via stream
    {
      number_t n = list_.size();
      string_t na = "parameter" + tostring(n + 1);
      push(*new Parameter(t, na));
      return *this;
    }

    /* ----------------------------------------------------------------------------
       access operator
    ----------------------------------------------------------------------------*/
    bool contains(const Parameter*) const;           //!< check whether Parameter is in Parameters
    bool contains(const Parameter&) const;           //!< check whether Parameter is in Parameters
    bool contains(const string_t& s) const;          //!< check whether Parameter named s is in Parameter
    bool contains(const char* s) const;              //!< check whether Parameter named s is in Parameter
    Parameter& operator()(const string_t& s) const;    //!< access by name (string)
    Parameter& operator()(const char* s) const;      //!< access by name (char *)
    Parameter& operator()(const Parameter& p) const;   //!< access by object
    Parameter& operator()(const number_t n) const;     //!< access by rank >=1

    Parameter& getFromShortName(const string_t& s) const; //!< access by shortname
    Parameter& getFromShortName(const char* s) const; //!< access by shortname

    // Set parameter value, if not exists create it
    template<typename K>
    void set(const string_t& name, K value)
    {
      std::map<string_t, number_t>::const_iterator ind_i = indexFromName_.find(name);
      if (ind_i == indexFromName_.end()) add(name,value);
      else *list_[ind_i->second]=value;
    }

    template<typename K>
    void set(const char* name, K value)
    {
      set(string_t(name), value);
    }

    // Trick to get parameter from a name
    // In case there is no parameter existed, a new one will be created. In fact, it's a little bit
    // conflict from the concept of parameter; therefore, this function should be limited in using for
    // eigenSolver, in which some parameters should be default, except users would like to modify them.
    //! gets a parameter from a name (non const)
    template<typename K>
    K get(const string_t& name, K value)
    {
      if (contains(name)) return ParameterTrait<K>::get((*this)(name));
      add(name, value);
      return value;
    }
    //! gets a parameter from a name (const)
    template<typename K>
    K get(const string_t& name, K value) const
    {
      if (contains(name)) return ParameterTrait<K>::get((*this)(name));
      where("Parameters::get(String,K)");
      error("param_not_found",name);
      return value;  //fake return
    }

    //! gets a parameter from a name (non const)
    template<typename K>
    K get(const char* name, K value)
    {
      if (contains(name)) return ParameterTrait<K>::get((*this)(name));
      add(string_t(name), value);
      return value;
    }
    //! gets a parameter from a name (const)
    template<typename K>
    K get(const char* name, K value) const
    {
      if (contains(name)) return ParameterTrait<K>::get((*this)(name));
      where("Parameters::get(const char*, K)");
      error("param_not_found",string_t(name));
      return value;  //fake return
    }

    //! adds a parameter by its name and value
    template<typename K>
    void add(const string_t& name, const K& value)
    {
      Parameter* para = new Parameter(ParameterTrait<K>::set(name, value), name);
      push(*para);
    }
    template<typename K>
    void add(const char* name, const K& value)
    {
      Parameter* para = new Parameter(ParameterTrait<K>::set(name, value), name);
      push(*para);
    }
    template<typename K>
    void add(const string_t& name, const string_t& shortname, const K& value)
    {
      Parameter* para = new Parameter(ParameterTrait<K>::set(name, value), name, shortname);
      push(*para);
    }
    template<typename K>
    void add(const char* name, const string_t& shortname, const K& value)
    {
      Parameter* para = new Parameter(ParameterTrait<K>::set(name, value), name, shortname);
      push(*para);
    }
    template<typename K>
    void add(const string_t& name, const Strings& shortnames, const K& value)
    {
      Parameter* para = new Parameter(ParameterTrait<K>::set(name, value), name, shortnames);
      push(*para);
    }
    template<typename K>
    void add(const char* name, const Strings& shortnames, const K& value)
    {
      Parameter* para = new Parameter(ParameterTrait<K>::set(name, value), name, shortnames);
      push(*para);
    }

    //general get void* parameter and cast to reference to K
    template<typename K>
    K& get(const string_t& name) const
    {
      std::map<string_t, number_t>::const_iterator ind_i = indexFromName_.find(name);
      if (ind_i == indexFromName_.end()) error("param_not_found", name);
      Parameter& p = *list_[ind_i->second];
      if (p.type_!=_pointer) error("param_not_found", name + " as void*");
      const K* obj_p = reinterpret_cast<const K*>(p.get_p());
      if (obj_p==nullptr) error("null_pointer", "obj_p");
      return const_cast<K&>(*obj_p);
    }

    template<typename K>
    K& get(const char* name) const
    { return get<K>(string_t(name)); }

    template<typename K>
    void addObject(const string_t& name, const K& obj)
    { push(*new Parameter(static_cast<const void*>(&obj),name)); }

    template<typename K>
    void addObject(const char* name, const K& v)
    { addObject<K>(string_t(name),v); }

    template<typename K>
    void addOption(const string_t& name, const K& v)
    {
      if (!isNameAvailable(name)) error("option_not_available", name);
      if (!isNameAvailable(string_t("-")+name)) error("option_not_available", string_t("-")+name);
      if (!isNameAvailable(string_t("--")+name)) error("option_not_available", string_t("--")+name);
      std::vector<string_t> sn(2);
      sn[0]=string_t("-")+name;
      sn[1]=string_t("--")+name;
      add(name, sn, v);
    }

    void addOption(const string_t& name, const char* v)
    {
      addOption(name, string_t(v));
    }

    template<typename K>
    void addOption(const string_t& name, const string_t& shortname, const K& v)
    {
      if (!isNameAvailable(name)) error("option_not_available", name);
      if (!isNameAvailable(string_t("-")+name)) error("option_not_available", string_t("-")+name);
      if (!isNameAvailable(string_t("--")+name)) error("option_not_available", string_t("--")+name);
      if (!isNameAvailable(shortname)) error("option_not_available", shortname);
      std::vector<string_t> sn(3);
      sn[0]=string_t("-")+name;
      sn[1]=string_t("--")+name;
      sn[2]=shortname;
      add(name, sn, v);
    }

    void addOption(const string_t& name, const string_t& shortname, const char* v)
    {
      addOption(name, shortname, string_t(v));
    }

    template<typename K>
    void addOption(const string_t& name, const Strings& shortnames, const K& v)
    {
      if (!isNameAvailable(name)) error("option_not_available", name);
      if (!isNameAvailable(string_t("-")+name)) error("option_not_available", string_t("-")+name);
      if (!isNameAvailable(string_t("--")+name)) error("option_not_available", string_t("--")+name);
      for (number_t i=0; i < shortnames.size(); ++i)
      {
        if (!isNameAvailable(shortnames[i])) error("option_not_available", shortnames[i]);
      }
      std::vector<string_t> sn(shortnames);
      sn.push_back(string_t("-")+name);
      sn.push_back(string_t("--")+name);
      add(name, sn, v);
    }

    void addOption(const string_t& name, const Strings& shortnames, const char* v)
    {
      addOption(name, shortnames, string_t(v));
    }

    void associateVector(const string_t& un, number_t d=3)
    {push(*new Parameter(static_cast<const void*>(new Vector<real_t>(d)),un));} //!< associate to the parameters a  vector

    void associateVector(UnitaryVector uv, number_t d=3)
    {
      error("free_error","Parameters::associateVector(Unitary vector) no longer manages the transmission"
                         " of normals to user functions, use new ThreadData machinery");
    }

    /////////////////
    // deprecated access to normal vectors, use now global function getN, getNx, getNy using ThreadData machinery
    /////////////////
    //! get unitary vector ("_n","_nx","_ny",...)
    const Vector<real_t>& getVector(const string_t& un) const
    {
      if (un=="_n")
      {
        warning("deprecated","Parameters::getVector(\"_n\")","extern getN()");
        return getNx();
      }
      if (un=="_nx")
      {
        warning("deprecated","Parameters::getVector(\"_nx\")","extern getN()");
        return getNx();
      }
      if (un=="_ny")
      {
        warning("deprecated","Parameters::getVector(\"_ny\")","extern getN()");
        return getNy();
      }
      where("Parameters::getVector(String)");
      error("unitaryvector_not_handled",un);
      return getNx();  // dummy return
    }

    //! get unitary vector (_n,_nx,_ny,...)
    const Vector<real_t>& getVector(UnitaryVector uv) const
    {
      switch (uv)
      {
        case _n:
          warning("deprecated","Parameters::getVector(_n)","extern getN()");
          return getNx();
        case _nx:
          warning("deprecated","Parameters::getVector(_nx)","extern getNx()");
          return getNx();
        case _ny:
          warning("deprecated","Parameters::getVector(_ny)","extern getNy()");
          return getNy();
        default:
          where("Parameters::getVector(UnitaryVector)");
          error("unitaryvector_not_handled",words("normal",uv));
      }
      return getNx(); // dummy return
    }

    void parse(const string_t& filename, bool errorOnNotFound=false);
    void parse(int argc, char** argv, bool errorOnNotFound=false);
    void parse(const string_t& filename, int argc, char** argv, bool errorOnNotFound=false)
    {
      parse(filename, false);
      parse(argc, argv, errorOnNotFound);
    }

    //! special safe access to some internal int parameters, return def value if not exist
    int safeInt(const string_t& s, int def=0) const;
    int basisIndex() const
    {return safeInt("basis index",1);}
    int basisSize() const
    {return safeInt("basis size",0);}
    /*
    ----------------------------------------------------------------------------
       I/O utilities
    ----------------------------------------------------------------------------
    */
    friend std::ostream& operator<<(std::ostream&, const Parameters&);

}; // end of class Parameters

std::ostream& operator<<(std::ostream&, const Parameters&); //!< flux insertion (write)
void print(const Parameters&); //!< extern print to default ostream

//access to particular parameters values (user shortcuts)
inline
const Vector<real_t>& getNormalVectorFrom(const Parameters& pa)
{
  warning("deprecated","getNormalVectorFrom(Parameters)","extern getN()");
  return getNx();
}

inline
const Vector<real_t>& getNxVectorFrom(const Parameters& pa)
{
  warning("deprecated","getNxVectorFrom(Parameters)","extern getNx()");
  return getNx();
}

inline
const Vector<real_t>& getNyVectorFrom(const Parameters& pa)
{
  warning("deprecated","getNyVectorFrom(Parameters)","extern getNy()");
  return getNy();
}

} // end of namespace xlifepp;

#endif /* PARAMETERS_HPP */
