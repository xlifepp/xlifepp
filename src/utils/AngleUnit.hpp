/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file AngleUnit.hpp
  \author E. Lunéville
  \since 2 apr 2021
  \date 2 apr 2021

  \brief tool class to manage angle conversion, basically product by ratio pi/180 (_degToRad) or 180/pi (_radToDeg)
         but possibly round to closest standard angle to avoid rounded errors
         may be extended to some other angle units


 */

#ifndef ANGLEUNIT_HPP
#define ANGLEUNIT_HPP

#include "config.h"
#include "Algorithms.hpp"
#include "Messages.hpp"
#include <cmath>

namespace xlifepp
{
enum ConvAngleType{_noAngleConversion,_degToRad,_radToDeg};

class AngleUnit
{
 public:
  ConvAngleType type;  // type of conversion
  real_t eps;          // eps in rounding process
  AngleUnit(AngleUnitType ts, AngleUnitType td, real_t tol=0.) : eps(std::abs(tol))
  {
    type = _noAngleConversion;
    switch(td)
    {
     case _rad: switch(ts)
       {
         case _rad: return;
         case _deg: type = _degToRad; return;
         default: error("free_error","unknown AngleUnitType in AngleUnit constructor");
       }
       break;
     case _deg: switch(ts)
       {
         case _deg: return;
         case _rad: type = _radToDeg; return;
         default: error("free_error","unknown AngleUnitType in AngleUnit constructor");
       }
       break;
    default: error("free_error","unknown AngleUnitType in AngleUnit constructor");
    }
  }
};

inline real_t operator*(real_t a, const AngleUnit& u)
{
    switch(u.type)
    {
        case _noAngleConversion: return a;
        case _degToRad: a*= pi_/180; break;
        case _radToDeg: a*= 180/pi_; break;
        default: error("free_error","unknown conversion type in AngleUnit");
    }
    if(u.eps>0.) a=round(a,u.eps);
    return a;
}



} //end of namespace xlifepp

#endif /* ANGLEUNIT_HPP */
