/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SparseMatrix.hpp
  \authors E. Lunéville
  \since 3 may 2021
  \date 3 may 2021

  \brief Definition of the xlifepp::SparseMatrix class

  general template class for (not too large) real or complex sparse matrices
  it is based on a (i,j,v) representation
  Matrices may be rectangular and stored in a map<pair<number_t, number_t>,T>
  Supported operations
    - constructor, copy constructor, assignment operator=, destructor
    - constructor by constant value, vector, matrix, special matrices and from input file
    - access operator M(i,j) (read and write)
    - basic intern algebra operations: += -= *= /= with matrix or scalar
    - unary and binary external operator +, -
    - basic extern algebra operations: + - * / with scalar
    - matrix vector product: overloaded *
    - matrix matrix product: overloaded *
    - transpose and adjoint operation
    - real part, imag part and abs returning a SparseMatrix
    - toMatrix converter
    - input and output functions

    NOTE: do not confuse this class with LargeMatrix class that handles "sparse" matrices with more efficient compressed storage
 */

#ifndef SPARSEMATRIX_HPP
#define SPARSEMATRIX_HPP

#include "config.h"
#include "Vector.hpp"
#include "Matrix.hpp"
#include <map>

namespace xlifepp
{
/*!
  \class SparseMatrix
  to deal with numeric sparse matrix (say real or complex vector)
  using (i,j,v) representation
 */

template<typename K>
class SparseMatrix : public std::map<NumPair,K>
{
 private:
  number_t rows_; //!< number of rows
  number_t cols_; //!< number of columns
 public:
  //@{
  //! useful typedefs to iterators
  typedef typename std::map<NumPair,K>::iterator it_sm;
  typedef typename std::map<NumPair,K>::const_iterator cit_sm;
  //@}
  //------------------------------------------------------------------------------
  //constructors/destructor
  //------------------------------------------------------------------------------
  SparseMatrix(const number_t r=0, const number_t c=0) //! constructor for null rectangular matrix
    : rows_(r), cols_(c)  {}
  SparseMatrix(const std::vector<K>& v) //! constructor for diagonal square matrix
  {
    number_t n=v.size();
    rows_= n, cols_=n;
    typename std::vector<K>::const_iterator it=v.begin();
    for(number_t i=1;i<=n;i++,++it) (*this)[NumPair(i,i)]=*it;
  }
  SparseMatrix(const number_t n, SpecialMatrix sm) //! constructor for v*Id matrix (v!=0)
  {
    if(sm!=_idMatrix) error("free_error", "only Id matrix in SparseMatrix constructor");
    rows_= 0, cols_=0;
    if(n==0) return;
    rows_= n, cols_=n;
    for(number_t i=1;i<=n;i++) (*this)[NumPair(i,i)]=K(1);
  }
  SparseMatrix(const Matrix<K>& m) //! constructor from a standard matrix
  {
    rows_=m.numberOfRows(); cols_=m.numberOfCols();
    typename std::vector<K>::const_iterator it = m.begin();
    for(dimen_t r = 1 ; r <= rows_; r++)
      for(dimen_t c = 1; c <=cols_; c++, it++)
        if(*it!=K(0))(*this)[NumPair(r,c)]=*it;
  }
  SparseMatrix(const string_t& f) //! constructor from input file given by name
  { loadFromFile(f.c_str());}
  SparseMatrix(const char* f) //! constructor from input file given by name
  { loadFromFile(f); }

  //------------------------------------------------------------------------------
  //insertion
  //------------------------------------------------------------------------------
  it_sm insert(number_t i, number_t j, const K& v)  // add a NEW coefficient to matrix
  {
      if(i==0 || j==0) return end(); // nothing done
      std::pair<it_sm,bool> p=std::map<NumPair,K>::insert(std::make_pair(NumPair(i,j),v));
      rows_=std::max(rows_,i);
      cols_=std::max(cols_,j);
      return p.first;
  }

  void setRow(number_t i, const std::vector<K>& v)
  {
      for(number_t j=0;j<v.size();j++)
        if(v[j]!=K(0)) (*this)(i,j+1)=v[j];
  }

  void setCol(number_t j, const std::vector<K>& v)
  {
      for(number_t i=0;i<v.size();i++)
        if(v[i]!=K(0)) (*this)(i+1,j)=v[i];
  }

  std::vector<K> getRow(number_t i) const
  {
      std::vector<K> v(cols_,K(0));
      for (cit_sm it = begin();it!=end();it++)  //not optimal
         if(it->first.first==i) v[it->first.second-1]=it->second;
     return v;
  }

  std::vector<K> getCol(number_t j) const
  {
      std::vector<K> v(rows_,K(0));
      for (cit_sm it = begin();it!=end();it++)  //not optimal
         if(it->first.second==j) v[it->first.first-1]=it->second;
     return v;
  }


  //------------------------------------------------------------------------------
  // access functions (size, number of rows, of columns,begin and end, coefficients)
  //------------------------------------------------------------------------------
  number_t size() const //! overloaded size()
  { return std::map<NumPair,K>::size();}
  number_t cols() const //! access to the the mumber of cols_
  { return cols_;}
  number_t rows() const //! access to the the mumber of rows_
  { return rows_;}
  NumPair dims() const //! dimensions
  { return NumPair(rows_,cols_);}

  it_sm begin() //! overloaded iterator begin
  { return std::map<NumPair,K>::begin();}
  cit_sm begin() const //! overloaded const_iterator begin
  { return std::map<NumPair,K>::begin();}
  it_sm end() //! overloaded iterator end
  { return std::map<NumPair,K>::end();}
  cit_sm end() const //! overloaded const_iterator begin
  { return std::map<NumPair,K>::end();}

  const K& operator()(number_t i, number_t j) const //! protected access ( i > 0, j > 0 )
  {
    cit_sm it=this->find(NumPair(i,j));
    if(it==end()) return K(0);
    return it->second;
  }
  K& operator()(number_t i,number_t j) //! read/write access ( i > 0, j > 0 )
  {
    it_sm it=this->find(NumPair(i,j));
    if(it==end()) it=insert(i,j,K(0));
    return it->second;
  }

  string_t typeStr() const  //! type of matrix as string (see specialization)
  { return "?";}

  SparseMatrix& clean()  //! remove zeros without changing matrix size
  {
    it_sm it = begin();
    while(it!=end())  //not optimal
    {
      if(it->second==K(0)){this->erase(it); it=begin();}
      else it++;
    }
    return *this;
  }

  SparseMatrix& resize(number_t m, number_t n)  //! change matrix size, removing outside coeff
  {
    if(m==0 || n==0){this->clear();return *this;}
    it_sm it = begin();
    while(it!=end())  //not optimal
    {
      if(it->first.first>m || it->first.second>n){this->erase(it); it=begin();}
      else it++;
    }
    rows_=m; cols_=n;
    return *this;
  }

  Matrix<K> toMatrix() const  //! create dense matrix from sparse matrix
  {
    Matrix<K> m(rows_,cols_);
    for(cit_sm it = begin();it!=end();++it)
       m(it->first.first,it->first.second)=it->second;
    return m;
  }

  //------------------------------------------------------------------------------
  // structure stuff and transposition operations
  //------------------------------------------------------------------------------
  template<typename KK>
  bool shareDims(const SparseMatrix<KK>& m) const //! true if matrices bear same dimensions
  {
    return (rows_ == m.rows() && cols_ == m.cols());
  }

  bool isSymmetric() const //! symmetric matrix test
  {
    if (rows_ != cols_) return false;
    cit_sm it = begin(), its;
    for(;it!=end();++it)
    {
      number_t i=it->first.first, j=it->first.second;
      if( i>j )
      {
        its=this->find(NumPair(j,i));
        if(its==end()) return false;
        if(it->second != its->second) return false;
      }
    }
    return true;
  }

  bool isSkewSymmetric() const //! skew-symmetric matrix test
  {
    if (rows_ != cols_) return false;
    cit_sm it = begin(), its;
    for(;it!=end();++it)
    {
      number_t i=it->first.first, j=it->first.second;
      if(i==j && it->second!=-it->second) return false;
      if( i>j )
      {
        its=this->find(NumPair(j,i));
        if(its==end()) return false;
        if(it->second != -its->second) return false;
      }
    }
    return true;
  }

  bool isSelfAdjoint() const //! self adjoint matrix test
  {
    if (rows_ != cols_) return false;
    cit_sm it = begin(), its;
    for(;it!=end();++it)
    {
      number_t i=it->first.first, j=it->first.second;
      if(i==j && it->second!=conj(it->second)) return false;
      if( i>j )
      {
        its=this->find(NumPair(j,i));
        if(its==end()) return false;
        if(it->second != conj(its->second)) return false;
      }
    }
    return true;
  }

  bool isSkewAdjoint() const //! skew adjoint matrix test
  {
    if (rows_ != cols_) return false;
    cit_sm it = begin(), its;
    for(;it!=end();++it)
    {
      number_t i=it->first.first, j=it->first.second;
      if(i==j && it->second!=-conj(it->second)) return false;
      if( i>j )
      {
        its=this->find(NumPair(j,i));
        if(its==end()) return false;
        if(it->second != -conj(its->second)) return false;
      }
    }
    return true;
  }

  SparseMatrix<K>& transpose() //! matrix transpostion
  {
    bool lower = (rows_ >= cols_);
    number_t c=cols_; cols_=rows_; rows_=c;
    K w;
    it_sm it = begin(), its;
    while (it!=end())
    {
      number_t i=it->first.first, j=it->first.second;
      if ((lower && i>j) || (!lower && j>i))
      {
        its=this->find(NumPair(j,i));
        w=it->second;
        if (its!=end())
        {
          it->second=its->second;
          its->second=w;
          it++;
        }
        else
        {
          (*this)[NumPair(j,i)]=w;  // create symmetric coefficient
          #if __cplusplus >= 201103L
          it=this->erase(it); // erase coefficient
          #else
          this->erase(it++);
          #endif
        }
      }
      else { it++; }
    }
    return *this;
  }

  SparseMatrix<K>& adjoint() //! matrix adjoint
  {
    bool lower = (rows_ >= cols_);
    number_t c=cols_; cols_=rows_; rows_=c;
    K w;
    it_sm it = begin(), its;
    while (it!=end())
    {
      number_t i=it->first.first, j=it->first.second;
      if ((lower && i>=j) || (!lower && j>=i))
      {
        its=this->find(NumPair(j,i));
        w=it->second;
        if (its!=end())
        {
          it->second=conj(its->second);
          its->second=conj(w);
          it++;
        }
        else
        {
          (*this)[NumPair(j,i)]=conj(w);  // create symmetric coefficient
          #if __cplusplus >= 201103L
          it=this->erase(it); // erase coefficient
          #else
          this->erase(it++);
          #endif
        }
      }
      else { it++; }
    }
    return *this;
  }

  SparseMatrix<K>& conjugate()  //! to its conjugate (assuming conj(real)->real)
  {
    it_sm it = begin();
    for(;it!=end();++it) it->second = conj(it->second);
    return *this;
  }

  //------------------------------------------------------------------------------
  // member operators
  //------------------------------------------------------------------------------
  template<typename KK>
  void add(number_t i, number_t j, const KK& vij) // add coefficient to current at i,j position
  {
     if(vij==K(0)) return; //nothing to do
     it_sm it=this->find(NumPair(i,j));
     if(it!=end()) // coeff exists, update and remove if zero (do not change matrix sizes!)
     {
       it->second+=vij;
       if(it->second==K(0)) std::map<NumPair,K>::erase(it);
     }
     else //new non zero coeff, may increase matrix sizes
     {
       (*this)[NumPair(i,j)]=vij;
       rows_=std::max(rows_,i);
       cols_=std::max(cols_,j);
     }
  }
  template<typename KK>
  void sub(number_t i, number_t j, const KK& vij) // add coefficient to current at i,j position
  {add(i,j,-vij);}
  //add a sparse matrix, dimension may be different
  template<typename KK>
  SparseMatrix<K>& operator+=(const SparseMatrix<KK>& b) //! accumulated matrix addition A+=B
  {
    //if (!shareDims(b)) mismatchDims("A+=B", b.rows(), b.cols());
    typename SparseMatrix<KK>::const_iterator itb = b.begin();
    for (; itb != b.end(); itb++) add(itb->first.first, itb->first.second, itb->second);
    return *this;
  }
  template<typename KK>
  SparseMatrix<K>& operator+=(const KK& x) //! add a scalar
  {
    for(it_sm it = begin();it!=end();++it) it->second+=x;
    return *this;
  }
  //substract a sparse matrix, dimension may be different
  template<typename KK>
  SparseMatrix<K>& operator-=(const SparseMatrix<KK>& b) //! accumulated matrix subtraction (with cast)
  {
    //if (!shareDims(b)) mismatchDims("A+=B", b.rows(), b.cols());
    cit_sm itb = b.begin();
    for (; itb != b.end(); itb++) add(itb->first.first, itb->first.second, -itb->second);
    return *this;
  }
  template<typename KK>
  SparseMatrix<K>& operator-=(const KK& x) //! subtract a scalar
  {
    for(it_sm it = begin();it!=end();++it) it->second-=x;
    return *this;
  }
  template<typename KK>
  SparseMatrix<K>& operator*=(const KK& x) //! overloaded *= multiply by a scalar
  {
    if(x==KK(0))
    { this->clear();
      rows_=0;cols_=0;
      return *this;
    }
    for(it_sm it = begin();it!=end();++it) it->second*=x;
    return *this;
  }
  template<typename KK>
  SparseMatrix<K>& operator/=(const KK& x) //! overloaded /= divide by scalar
  {
    if (std::abs(x) < theEpsilon) { divideByZero("A/=x"); }
    for(it_sm it = begin();it!=end();++it) it->second/=x;
    return *this;
  }

  static real_t real(const real_t& x) {return x;}
  static real_t imag(const real_t& x) {return real_t(0);}
  static real_t real(const complex_t& z) {return z.real();}
  static real_t imag(const complex_t& z) {return z.imag();}

  SparseMatrix<real_t> real() const //! real part of a (complex) matrix
  {
    SparseMatrix<real_t> r(rows_,cols_);
    for(cit_sm it = begin();it!=end();++it)
      r[NumPair(it->first.first,it->first.second)]=real(it->second);
    return r;
  }

  SparseMatrix<real_t> imag() const //! imaginary part of a (complex) matrix
  {
    SparseMatrix<real_t> r(rows_,cols_);
    for(cit_sm it = begin();it!=end();++it)
      r[NumPair(it->first.first,it->first.second)]=imag(it->second);
    return r;
  }

  //------------------------------------------------------------------------------
  // product by a diagonal matrix stored as std::vector
  //------------------------------------------------------------------------------
  template<typename KK>
  SparseMatrix<K>& diagProduct(const std::vector<KK>& diag)  //! product by a diagonal matrix M = diag*M
  {
    for(it_sm it = begin();it!=end();++it) it->second*=diag[it->first.first-1];
    return *this;
  }
  template<typename KK>
  SparseMatrix<K>& productDiag(std::vector<KK> diag)  //! product by a diagonal matrix M = M*diag
  {
    for(it_sm it = begin();it!=end();++it) it->second*=diag[it->first.second-1];
    return *this;
  }

  real_t squaredNorm() const //! return squared norm of the matrix
  {
    real_t n=0.,nij;
    for(cit_sm it = begin();it!=end();++it)
    {
      nij=norm2(it->second);
      n+=nij*nij;
    }
    return n;
  }

  //------------------------------------------------------------------------------
  // warning and error handlers
  //------------------------------------------------------------------------------
  void nonSquare(const string_t& s, const size_t r, const size_t c) const //! error: operation forbidden for non square matrix
  { error("mat_nonsquare",s,r,c);}

  void mismatchDims(const string_t& s, const size_t r, const size_t c) const //! error: in matrix operation s dimensions disagree dim(op1)= ... dim(op2)=...
  { error("mat_mismatch_dims", s, rows_, cols_,r,c);}

  void divideByZero(const string_t& s) const //! error: divide by 0 in matrix operation s matrix dimension dim(op1)
  { error("mat/0", s, rows_, cols_);}

  void isSingular() const //! error: non invertible matrix
  { error("mat_noinvert", rows_, cols_);}

  void complexCastWarning(const string_t& s, const size_t r, const size_t c) const //! warning: try to cast a complex to a real
  { error("mat_cast", s, rows_,cols_, r, c);}

  //-----------------------------------------------------------------------------
  // I/O utilities
  //-----------------------------------------------------------------------------
  /*!
   load matrix from file
   read from file a sparse matrix, formated as:
   \verbatim
   i j aij
   ....
   \endverbatim
   */
  void loadFromFile(const char* f)
  {
    trace_p->push("SparseMatrix::loadFromFile");
    std::ifstream in(f);
    if (!in.is_open()) error("mat_badfile", f);   // file is not found
    this->clear(); // reset matrix
    number_t i,j;
    K v;
    while(!in.eof())
    {
      in >> i;
      if(in.eof()) break;
      in >> j ;
      if(in.eof()) break;
      in>> v;
      if(v!=K(0))  // 0 coefficient are excluded
      {
       rows_=std::max(rows_,i);
       cols_=std::max(cols_,j);
       it_sm itm=this->find(NumPair(i,j));
       if(itm==this->end()) (*this)[NumPair(i,j)]=v;
       else warning("free_warning", "duplicate coefficient in file: "+tostring(i)+" "+tostring(j)+" "+tostring(v));
      }
    }
    in.close();
    trace_p->pop();
  }
  //! save matrix to file formated as: i j aij
  void saveToFile(const char* f) const
  {
    trace_p->push("SparseMatrix::saveToFile");
    std::ofstream out(f);
    if (! out.is_open())   error("mat_badfile", f);  // error when opening file
    out.precision(fullPrec);
    cit_sm itm=this->begin();
    for(;itm!=this->end();++itm)
       out << itm->first.first << " " << itm->first.second << " "<<itm->second<<std::endl;
    out.close();
    trace_p->pop();
  }

  friend std::ostream& operator<<(std::ostream& os, const SparseMatrix<K>& m)
  {
   os <<m.rows()<<"x"<<m.cols()<<" sparse matrix:";
   if(m.size()==0) {os<<"[]";return os;}
   os << "\n[\n";
   typename SparseMatrix<K>::cit_sm it = m.begin();
   for(;it!=m.end();++it)
          os << " (" << it->first.first<<" "<<it->first.second<<")->" << it->second<<eol;
   os << "]";
   return os;
  }

  void print(std::ofstream& os) const { os << *this; } //!< write on a file stream
  void print() const { thePrintStream << *this; }      //!< write on default file stream

}; // end of class Matrix

//-----------------------------------------------------------------------------
// type of matrix as string
//-----------------------------------------------------------------------------
template <> inline string_t SparseMatrix<real_t>::typeStr() const  { return "real"; }
template <> inline string_t SparseMatrix<complex_t>::typeStr() const  { return "complex"; }

//-----------------------------------------------------------------------------
// specialized template operations
//-----------------------------------------------------------------------------
template <typename K>
inline SparseMatrix<real_t> abs(const SparseMatrix<K>& mat)            //! abs of a matrix
{
  SparseMatrix<real_t> r(mat.rows(),mat.cols());
  typename SparseMatrix<K>::cit_sm it=mat.begin();
  for(;it!=mat.end();++it)
    r(it->first.first,it->first.second)=std::abs(it->second);
  return r;
}

//-----------------------------------------------------------------------------
// non-member operations
//-----------------------------------------------------------------------------
template<typename K>
bool operator==(const SparseMatrix<K>& a, const SparseMatrix<K>& b) //! matrix comparison (element by element)
{
  if (&a == &b) return true; // same SparseMatrix adresses
  if (!a.shareDims(b)) return false;
  typename SparseMatrix<K>::cit_sm ita = a.begin(), itb = b.begin();
  for(;ita!=a.end();++ita, ++itb)
     if (ita->second != itb->second) return false;
  return true;
}

template<typename K>
bool operator!=(const SparseMatrix<K>& a, const SparseMatrix<K>& b) //! matrix comparison (element by element)
{
  return !(a == b);
}

template<typename K>
SparseMatrix<K> operator+(const SparseMatrix<K>& m)  //! unary operator+
{
  return m;
}

template<typename K>
SparseMatrix<K> operator-(const SparseMatrix<K>& m)  //! unary operator- (negation operator)
{
  SparseMatrix<K> r=m;
  typename SparseMatrix<K>::it_sm it=r.begin();
  for(;it!=r.end();++it) it->second*=-1;
  return r;
}

template<typename K, typename ITV, typename ITR>
void matvec(const SparseMatrix<K>& m, const ITV itv, const ITR itr)
{// itv and itr has to point to the beginning of vectors of the right size
 typename SparseMatrix<K>::cit_sm itm=m.begin();
 for(;itm!=m.end();++itm)
    *(itr+itm->first.first-1)+=itm->second * *(itv+itm->first.second-1);
}

template<typename K, typename ITV, typename ITR>
void vecmat(const SparseMatrix<K>& m, const ITV itv, const ITR itr)
{// itv and itr has to point to the beginning of vectors of the right size
 typename SparseMatrix<K>::cit_sm itm=m.begin();
 for(;itm!=m.end();++itm)
    *(itr+itm->first.second-1)+=itm->second * *(itv+itm->first.first-1);
}

//-----------------------------------------------------------------------------
// template matrix elementary operations
//-----------------------------------------------------------------------------
template<typename K, typename KK>
SparseMatrix<K> operator+(const SparseMatrix<K>& a, const SparseMatrix<KK>& b) //! sum of two matrices
{
  SparseMatrix<K> r(a);
  return r+=b;
}
template<typename K, typename KK>
SparseMatrix<K> operator+(const KK& x, const SparseMatrix<K>& a) //! scalar x + matrix A
{
  SparseMatrix<K> r(a);
  return r+=x;
}
template<typename K, typename KK>
SparseMatrix<K> operator+(const SparseMatrix<K>& a, const KK& x) //! matrix A + scalar x
{
  SparseMatrix<K> r(a);
  return r+=x;
}
template<typename K, typename KK>
SparseMatrix<K> operator-(const SparseMatrix<K>& a, const SparseMatrix<KK>& b) //! matrix A - matrix B
{
  SparseMatrix<K> r(a);
  return r-=b;
}
template<typename K, typename KK>
SparseMatrix<K> operator-(const KK& x, const SparseMatrix<K>& a) //! scalar x - matrix A
{
  SparseMatrix<K> r(a);
  r*=-1;
  return r+=x;
}
template<typename K, typename KK>
SparseMatrix<K> operator-(const SparseMatrix<K>& a, const KK& x) //! matrix A - scalar x
{
  SparseMatrix<K> r(a);
  return r-=x;
}
template<typename K, typename KK>
SparseMatrix<K> operator*(const KK& x, const SparseMatrix<K>& a) //! scalar x * matrix A
{
  SparseMatrix<K> r(a);
  return r*=x;
}
template<typename K, typename KK>
SparseMatrix<K> operator*(const SparseMatrix<K>& a, const KK& x) //! matrix A * scalar x
{
  SparseMatrix<K> r(a);
  return r*=x;
}
template<typename K, typename KK>
SparseMatrix<K> operator/(const SparseMatrix<K>& a, const KK& x) //! matrix A / scalar x
{
  if (std::abs(x) < theEpsilon) { a.divideByZero("A/=x"); }
  SparseMatrix<K> r(a);
  return r/=x;
}
//-----------------------------------------------------------------------------
// matrix vector multiplications
//-----------------------------------------------------------------------------
template<typename K, typename V>
Vector<K> operator*(const SparseMatrix<K>& m, const std::vector<V>& v) //! matrix x vector (template)
{
  if (m.cols() != v.size()) m.mismatchDims("M*V", v.size(), 1);
  Vector<K> res(m.rows()); // create result vector
  matvec(m, v.begin(), res.begin());
  return res;
}
template<typename K, typename V>
Vector<K> operator*(const std::vector<V>& v, const SparseMatrix<K>& m) //! vector x matrix (template)
{
  if (m.rows() != v.size()) m.mismatchDims("V*M", v.size(), 1);
  Vector<K> res(m.cols());
  vecmat(m, v.begin(), res.begin());
  return res;
}
//-----------------------------------------------------------------------------
// extern diag, transpose and adjoint operation
//-----------------------------------------------------------------------------
template<typename K>
SparseMatrix<K> transpose(const SparseMatrix<K>& m)
{
   SparseMatrix<K> r=m;
   return r.transpose();
}
template<typename K>
SparseMatrix<K> adjoint(const SparseMatrix<K>& m)
{
  SparseMatrix<K> r=m;
  return r.adjoint();
}
template<typename K>
SparseMatrix<K> tran(const SparseMatrix<K>& m) //! transpose matrix
{
  return transpose(m);
}
//utilities to get dimensions
template<typename K>
std::pair<dimen_t, dimen_t> dimsOf(const SparseMatrix<K>& v)
{return std::pair<dimen_t, dimen_t> (v.rows(), v.cols());}

} // end of namespace xlifepp

#endif /* MATRIX_HPP */
