/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ThreadData.hpp
  \author E. Lunéville
  \since 25 apr 2018
  \date 25 apr 2018

  \brief ThreadData class handling some possible shared data in omp computation(normal vectors, element, dof, ...)

  When doing FE computation, some local informations related to one elements (normal vectors, element itself, dof, ...)
  have to be passed to user functions. When multithreading (say omp) is enable, these informations may depend on the thread,
  so multiple data (one by thread) has to be stored somewhere. It is the purpose of the ThreadData class that handles the following vectors

    vector<Vector<real_t>*> theCurrentNxs   -> normal or x-normal pointer vectors
    vector<Vector<real_t>*> theCurrentNys   -> y-normal pointer vectors
    vector<GeomElement*> theCurrentElements -> geom element pointer vectors
    vector<Dof*> theCurrentDofs             -> dof pointer vectors
    vector<number_t> theCurrentBasisIndexes -> basis indexes (used in spectral computation)
    GeomDomain* currentDomainx              -> domain or domainx pointer vectors (not omp)
    GeomDomain* currentDomainy              -> domainy pointer vectors (not omp)

  and proposes some functions to set or get data

    setNx, getNx, setNy, getNy, setElement, getElement, setDof, getDof, setBasisIndex, getBasisIndex,  setDomain, getDomain, setDerivative, getDerivative

  At the initialization, XLiFE++ instances one ThreadData object: theThreadData (see globalScopeData.hpp, config.hpp and init())
  To make the user life easier, the users can use the extern functions getNx, getN, getNy, getElement, getFeDof, getDof, getBasisIndex, getDomain
  that addresses the theThreadData object

  Note: when single thread computation, only the first element of vectors is used

  To users: This machinery replaces the transmission of normal vectors by the parameter associated to Function or Kernel
             How it works now? Suppose you want to use a function involving the normal vector at point x

             a) define a standard C++ XLiFE++ function
                Real fun(const Point& x, Parameters& pa=defaultParameters)
                  { ...
                    Vector<Real>& n=getN();   //to get the normal vector at x if computed!
                    Number n=getBasisIndex(); //to get the basis index if spectral computation
                    ...
                  }

             b) create a Function object (with or without parameters) in your program
                  ...
                  Function F(fun);          // Function object with no parameter handling the C++ function fun
                  F.require(_n);            // tell to XLiFE++ that F will use the normal n
                                            // may be disable F.require(_n, false)

             Remember that the normal will be updated, only if your function is involved in a FE computation on a manifold (boundary for instance)

*/

#ifndef THREAD_DATA_HPP
#define THREAD_DATA_HPP

#include "config.h"
#include "Vector.hpp"

namespace xlifepp
{
//forward classes
class GeomElement;
class Dof;
class FeDof;
class GeomDomain;

//=================================================================================
/*!
   \class ThreadData
   class handling some possible shared data in omp computation(normal vectors, element, dof, ...)
*/
//=================================================================================

class ThreadData
{
  public:
    std::vector<Vector<real_t>*> theCurrentNxs;     //!< normal pointers or x-normal pointers
    std::vector<Vector<real_t>*> theCurrentNys;     //!< y-normal pointers
    std::vector<Vector<real_t>*> theCurrentTxs;     //!< tangent pointers or x-tangent pointers
    std::vector<Vector<real_t>*> theCurrentBxs;     //!< bitangent pointers or x-bitangent pointers
    std::vector<Vector<real_t>*> theCurrentTys;     //!< y-tangent pointers
    std::vector<Vector<real_t>*> theCurrentBys;     //!< y-bitangent pointers
    std::vector<GeomElement*> theCurrentElements;   //!< element pointers
    std::vector<Dof*> theCurrentDofs;               //!< dof pointers
    std::vector<number_t> theCurrentBasisIndexes;   //!< basis indexes
    std::vector<number_t> theCurrentDerivatives;    //!< derivative indexes
    GeomDomain* currentDomainx;                     //!< domain pointer or domain_x pointer
    GeomDomain* currentDomainy;                     //!< domain_y pointer

    ThreadData(number_t s=1) { resize(s); }
    void resize(number_t);  //! resize global vectors theCurrentNxs, theCurrentNys, ... to size s
    void reset(number_t t)  //! reset all data of thread t
    {
      theCurrentNxs[t]=nullptr; theCurrentNys[t]=nullptr;
      theCurrentElements[t]=nullptr; theCurrentDofs[t]=nullptr;
      theCurrentBasisIndexes[t]=0;
      theCurrentDerivatives[t]=0;
    }
    void reset() { reset(currentThread()); }  //! reset all data of current thread
    void resetAll()                           //! reset all data of any thread
    {
      for(number_t t=0;t<theCurrentNxs.size();t++) reset(t);
      currentDomainx=nullptr; currentDomainy=nullptr;
    }

    //set data
    void setN(Vector<real_t>* np)  { theCurrentNxs[currentThread()]=np; }
    void setNx(Vector<real_t>* np) { theCurrentNxs[currentThread()]=np; }
    void setNy(Vector<real_t>* np) { theCurrentNys[currentThread()]=np; }
    void setT(Vector<real_t>* np)  { theCurrentTxs[currentThread()]=np; }
    void setTx(Vector<real_t>* np) { theCurrentTxs[currentThread()]=np; }
    void setTy(Vector<real_t>* np) { theCurrentTys[currentThread()]=np; }
    void setB(Vector<real_t>* np)  { theCurrentBxs[currentThread()]=np; }
    void setBx(Vector<real_t>* np) { theCurrentBxs[currentThread()]=np; }
    void setBy(Vector<real_t>* np) { theCurrentBys[currentThread()]=np; }
    void setElement(GeomElement* ep) { theCurrentElements[currentThread()]=ep; }
    void setDomainx(GeomDomain* dp) { currentDomainx=dp; }
    void setDomainy(GeomDomain* dp) { currentDomainy=dp; }
    void setDof(Dof* dp) { theCurrentDofs[currentThread()]=dp; }
    void setBasisIndex(number_t i) {theCurrentBasisIndexes[currentThread()]=i;}
    void setDerivative(number_t i) {theCurrentDerivatives[currentThread()]=i;}

    void setN(Vector<real_t>* np, number_t t)  { theCurrentNxs[t]=np; }
    void setNx(Vector<real_t>* np, number_t t) { theCurrentNxs[t]=np; }
    void setNy(Vector<real_t>* np, number_t t) { theCurrentNys[t]=np; }
    void setT(Vector<real_t>* np, number_t t)  { theCurrentTxs[t]=np; }
    void setTx(Vector<real_t>* np, number_t t) { theCurrentTxs[t]=np; }
    void setTy(Vector<real_t>* np, number_t t) { theCurrentTys[t]=np; }
    void setB(Vector<real_t>* np, number_t t)  { theCurrentBxs[t]=np; }
    void setBx(Vector<real_t>* np, number_t t) { theCurrentBxs[t]=np; }
    void setBy(Vector<real_t>* np, number_t t) { theCurrentBys[t]=np; }
    void setElement(GeomElement* ep, number_t t) { theCurrentElements[t]=ep; }
    void setDof(Dof* dp, number_t t) { theCurrentDofs[t]=dp; }
    void setBasisIndex(number_t i, number_t t) {theCurrentBasisIndexes[t]=i;}
    void setDerivative(number_t i, number_t t) {theCurrentDerivatives[t]=i;}

    //get normal data
    Vector<real_t>& getN(number_t t) const
    {
      Vector<real_t>* p=theCurrentNxs[t];
      if (p==nullptr) p = theCurrentNxs[0];
      if (p==nullptr) error("omp_no_data","n/nx", t);
      return *p;
    }
    Vector<real_t>& getNx(number_t t) const
    {
      Vector<real_t>* p=theCurrentNxs[t];
      if (p==nullptr) p = theCurrentNxs[0];
      if (p==nullptr) error("omp_no_data","n/nx", t);
      return *p;
    }
    Vector<real_t>& getNx() const { return getNx(currentThread()); }
    Vector<real_t>& getNy(number_t t) const
    {
      Vector<real_t>* p=theCurrentNys[t];
      if (p==nullptr) p = theCurrentNys[0];
      if (p==nullptr) error("omp_no_data","ny", t);
      return *p;
    }
    Vector<real_t>& getNy() const { return getNy(currentThread()); }

    //get tangent data
    Vector<real_t>& getT(number_t t) const
    {
      Vector<real_t>* p = theCurrentTxs[t];
      if (p==nullptr) p = theCurrentTxs[0];
      if (p==nullptr) error("omp_no_data","t/tx", t);
      return *p;
    }
    Vector<real_t>& getTx(number_t t) const
    {
      Vector<real_t>* p = theCurrentTxs[t];
      if (p==nullptr) p = theCurrentTxs[0];
      if (p==nullptr) error("omp_no_data","t/tx", t);
      return *p;
    }
    Vector<real_t>& getTx() const { return getTx(currentThread()); }
    Vector<real_t>& getTy(number_t t) const
    {
      Vector<real_t>* p = theCurrentTys[t];
      if (p==nullptr) p = theCurrentTys[0];
      if (p==nullptr) error("omp_no_data","ty", t);
      return *p;
    }
    Vector<real_t>& getTy() const { return getTy(currentThread()); }

    // get bitangent data
    Vector<real_t>& getB(number_t t) const
    {
      Vector<real_t>* p = theCurrentBxs[t];
      if (p==nullptr) p = theCurrentBxs[0];
      if (p==nullptr) error("omp_no_data","b/bx", t);
      return *p;
    }
    Vector<real_t>& getBx(number_t t) const
    {
      Vector<real_t>* p = theCurrentBxs[t];
      if (p==nullptr) p = theCurrentBxs[0];
      if (p==nullptr) error("omp_no_data","b/bx", t);
      return *p;
    }
    Vector<real_t>& getBx() const { return getBx(currentThread()); }
    Vector<real_t>& getBy(number_t t) const
    {
      Vector<real_t>* p = theCurrentBys[t];
      if (p==nullptr) p = theCurrentBys[0];
      if (p==nullptr) error("omp_no_data","by", t);
      return *p;
    }
    Vector<real_t>& getBy() const { return getBy(currentThread()); }

    // get element data
    GeomElement& getElement(number_t t) const
    {
      GeomElement* p=theCurrentElements[t];
      if (p==nullptr) p = theCurrentElements[0];
      if (p==nullptr) error("omp_no_data","elt", t);
      return *p;
    }
    GeomElement& getElement() const { return getElement(currentThread()); }
    GeomElement* getElementP(number_t t) const { return theCurrentElements[t]; }
    GeomElement* getElementP() const { return theCurrentElements[currentThread()]; }

    GeomDomain& getDomainx() const
    {
      if (currentDomainx==nullptr) error("omp_no_data","domain or domain_x", 0);
      return *currentDomainx;
    }
    GeomDomain& getDomainy() const
    {
      if (currentDomainy==nullptr) error("omp_no_data","domain_y", 0);
      return *currentDomainy;
    }

    Dof& getDof(number_t t) const
    {
      Dof* p=theCurrentDofs[t];
      if (p==nullptr) p = theCurrentDofs[0];
      if (p==nullptr) error("omp_no_data","domain", t);
      return *p;
    }
    Dof& getDof() const { return getDof(currentThread()); }

    FeDof& getFeDof(number_t t) const;
    FeDof& getFeDof() const { return getFeDof(currentThread()); }

    number_t getBasisIndex(number_t t) const {return theCurrentBasisIndexes[t];}
    number_t getBasisIndex() const {return theCurrentBasisIndexes[currentThread()];}

    number_t getDerivative(number_t t) const {return theCurrentDerivatives[t];}
    number_t getDerivative() const {return theCurrentDerivatives[currentThread()];}

    void print(std::ostream&) const;
    void print(PrintStream& os) const { print(os.currentStream()); }
};

inline void setN(Vector<real_t>& p) { theThreadData.setNx(&p,currentThread()); }
inline void setNx(Vector<real_t>& p) { theThreadData.setNx(&p,currentThread()); }
inline void setNy(Vector<real_t>& p) { theThreadData.setNy(&p,currentThread()); }
inline void setNx(Vector<real_t>* p) { theThreadData.setNx(p,currentThread()); }
inline void setNy(Vector<real_t>* p) { theThreadData.setNy(p,currentThread()); }
inline void setN(Vector<real_t>* p, number_t t) { theThreadData.setNx(p,t); }
inline void setNx(Vector<real_t>* p, number_t t) { theThreadData.setNx(p,t); }
inline void setNy(Vector<real_t>* p, number_t t) { theThreadData.setNy(p,t); }

inline void setT(Vector<real_t>& p) { theThreadData.setTx(&p,currentThread()); }
inline void setTx(Vector<real_t>& p) { theThreadData.setTx(&p,currentThread()); }
inline void setTy(Vector<real_t>& p) { theThreadData.setTy(&p,currentThread()); }
inline void setTx(Vector<real_t>* p) { theThreadData.setTx(p,currentThread()); }
inline void setTy(Vector<real_t>* p) { theThreadData.setTy(p,currentThread()); }
inline void setT(Vector<real_t>* p, number_t t) { theThreadData.setTx(p,t); }
inline void setTx(Vector<real_t>* p, number_t t) { theThreadData.setTx(p,t); }
inline void setTy(Vector<real_t>* p, number_t t) { theThreadData.setTy(p,t); }

inline void setB(Vector<real_t>& p) { theThreadData.setBx(&p,currentThread()); }
inline void setBx(Vector<real_t>& p) { theThreadData.setBx(&p,currentThread()); }
inline void setBy(Vector<real_t>& p) { theThreadData.setBy(&p,currentThread()); }
inline void setBx(Vector<real_t>* p) { theThreadData.setBx(p,currentThread()); }
inline void setBy(Vector<real_t>* p) { theThreadData.setBy(p,currentThread()); }
inline void setB(Vector<real_t>* p, number_t t) { theThreadData.setBx(p,t); }
inline void setBx(Vector<real_t>* p, number_t t) { theThreadData.setBx(p,t); }
inline void setBy(Vector<real_t>* p, number_t t) { theThreadData.setBy(p,t); }

inline void setElement(GeomElement* p, number_t t) { theThreadData.setElement(p,t); }
inline void setDof(Dof* p,number_t t) { theThreadData.setDof(p,t); }
inline void setBasisIndex(number_t i,number_t t) { theThreadData.setBasisIndex(i,t); }
inline void setDerivative(number_t i,number_t t) { theThreadData.setDerivative(i,t); }

inline void setElement(GeomElement* p) { theThreadData.setElement(p,currentThread()); }
inline void setDomain(GeomDomain* p) { theThreadData.setDomainx(p); }
inline void setDomainx(GeomDomain* p) { theThreadData.setDomainx(p); }
inline void setDomainy(GeomDomain* p) { theThreadData.setDomainy(p); }
inline void setDof(Dof* p) { theThreadData.setDof(p,currentThread()); }
inline void setBasisIndex(number_t i) { theThreadData.setBasisIndex(i,currentThread()); }
inline void setDerivative(number_t i) { theThreadData.setDerivative(i,currentThread()); }

inline Vector<real_t>& getN(number_t t) { return theThreadData.getNx(t); }
inline Vector<real_t>& getNx(number_t t) { return theThreadData.getNx(t); }
inline Vector<real_t>& getNy(number_t t) { return theThreadData.getNy(t); }
inline Vector<real_t>& getN() { return theThreadData.getNx(currentThread()); }
inline Vector<real_t>& getNx() { return theThreadData.getNx(currentThread()); }
inline Vector<real_t>& getNy() { return theThreadData.getNy(currentThread()); }

inline Vector<real_t>& getT(number_t t) { return theThreadData.getTx(t); }
inline Vector<real_t>& getTx(number_t t) { return theThreadData.getTx(t); }
inline Vector<real_t>& getTy(number_t t) { return theThreadData.getTy(t); }
inline Vector<real_t>& getT() { return theThreadData.getTx(currentThread()); }
inline Vector<real_t>& getTx() { return theThreadData.getTx(currentThread()); }
inline Vector<real_t>& getTy() { return theThreadData.getTy(currentThread()); }

inline Vector<real_t>& getB(number_t t) { return theThreadData.getBx(t); }
inline Vector<real_t>& getBx(number_t t) { return theThreadData.getBx(t); }
inline Vector<real_t>& getBy(number_t t) { return theThreadData.getBy(t); }
inline Vector<real_t>& getB() { return theThreadData.getBx(currentThread()); }
inline Vector<real_t>& getBx() { return theThreadData.getBx(currentThread()); }
inline Vector<real_t>& getBy() { return theThreadData.getBy(currentThread()); }

inline GeomElement& getElement(number_t t) { return theThreadData.getElement(t); }
inline GeomElement* getElementP(number_t t){ return theThreadData.getElementP(t); }
inline GeomElement& getElement() { return theThreadData.getElement(currentThread()); }
inline GeomElement* getElementP() { return theThreadData.getElementP(currentThread()); }

inline Dof& getDof(number_t t) { return theThreadData.getDof(t); }
inline Dof& getDof() {return theThreadData.getDof(currentThread()); }
inline FeDof& getFeDof(number_t t) { return theThreadData.getFeDof(t); }
inline FeDof& getFeDof() {return theThreadData.getFeDof(currentThread()); }

inline number_t getBasisIndex(number_t t) { return theThreadData.getBasisIndex(t); }
inline number_t getBasisIndex() { return theThreadData.getBasisIndex(currentThread()); }

inline number_t getDerivative(number_t t) { return theThreadData.getDerivative(t); }
inline number_t getDerivative() { return theThreadData.getDerivative(currentThread()); }

inline GeomDomain& getDomain() { return theThreadData.getDomainx(); }
inline GeomDomain& getDomainx() { return theThreadData.getDomainx(); }
inline GeomDomain& getDomainy() { return theThreadData.getDomainy(); }

inline Vector<real_t>& getVector(UnitaryVector un)
{
  if (un==_n || un==_nx) return getNx();
  if (un==_ny) return getNy();
  if (un==_tau || un==_taux) return getTx();
  if (un==_tauy) return getTy();
  if (un==_btau || un==_btaux) return getBx();
  if (un==_btauy) return getBy();
  where("Parameters::getVector(String)");
  error("unitaryvector_not_handled",un);
  return getNx();  // dummy return
}

}
#endif // THREAD_DATA_HPP
