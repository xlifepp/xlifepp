/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
   \file ThreadData.cpp
   \author E. Lunéville
   \since 25 apr 2018
   \date  25 apr 2018
   \brief Implementation of xlifepp::ThreadData class members and related utilities
*/

#include "ThreadData.hpp"
#include "term.h"

namespace xlifepp
{
//! resize global vectors theCurrentNxs, theCurrentNys, ... to size s
void ThreadData::resize(number_t s)
{
  if(s==0) s=1;              // size must be at least 1
  theCurrentNxs.assign(s,0);
  theCurrentNys.assign(s,0);
  theCurrentTxs.assign(s,0);
  theCurrentTys.assign(s,0);
  theCurrentBxs.assign(s,0);
  theCurrentBys.assign(s,0);
  theCurrentElements.assign(s,0);
  theCurrentDofs.assign(s,0);
  theCurrentBasisIndexes.assign(s,0);
  theCurrentDerivatives.assign(s,0);
  currentDomainx=nullptr;
  currentDomainy=nullptr;
}

void ThreadData::print(std::ostream& out) const
{
    number_t k=0;
    std::vector<Vector<real_t>*>::const_iterator itnx=theCurrentNxs.begin(), itny=theCurrentNys.begin();
    std::vector<GeomElement*>::const_iterator ite=theCurrentElements.begin();
    std::vector<Dof*>::const_iterator ito=theCurrentDofs.begin();
    std::vector<number_t>::const_iterator iti=theCurrentBasisIndexes.begin();
    std::vector<number_t>::const_iterator itd=theCurrentDerivatives.begin();
    for(;itnx!=theCurrentNxs.end();++itnx,++itny,++ite,++ito,++iti,++itd,k++)
    {
        out<<"thread "<<k<<": n/nx = ";
        if(*itnx==nullptr) out<<"void"; else out<<**itnx;
        out<<", ny = ";
        if(*itny==nullptr) out<<"void"; else out<<**itny;
        out<<", elt ";
        if(*ite==nullptr) out<<"void"; else out<<(*ite)->number();
        out<<", dof ";
        if(*ito==nullptr) out<<"void"; else out<<(*ito)->dofData();
        out<<", basis index "<<(*iti);
        out<<", derivative index "<<(*itd);
        out<<eol;
    }
    out<<"domain/domainx: ";
    if(currentDomainx==nullptr) out<<"void"; else out<<currentDomainx->name();
    out<<", domainy: ";
    if(currentDomainy==nullptr) out<<"void"; else out<<currentDomainy->name();
    out<<eol;
}

FeDof& ThreadData::getFeDof(number_t t) const
{
    Dof* p=theCurrentDofs[t];
    if(p==nullptr) error("omp_no_data","domain", t);
    return static_cast<FeDof&>(*p);
}

}  //end of namespace xlifepp
