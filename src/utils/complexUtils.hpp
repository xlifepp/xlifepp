/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file complexUtils.hpp
  \author E. Lunéville
  \since 14 dec 2011
  \date 3 may 2012

  \brief overloaded functions "missing" in stl::class complex
 */


#ifndef COMPLEX_UTILS_HPP
#define COMPLEX_UTILS_HPP

#include "config.h"

namespace xlifepp
{

// definition of asin, acos, atan, asinh, acosh, atanh, cbrt (cubic root) for complex value
// equivalent formula
complex_t asin(const complex_t&);
complex_t acos(const complex_t&);
complex_t atan(const complex_t&);
complex_t asinh(const complex_t&);
complex_t acosh(const complex_t&);
complex_t atanh(const complex_t&);
complex_t cbrt(const complex_t&);
real_t asinh(const real_t&);
real_t acosh(const real_t&);
real_t atanh(const real_t&);
real_t cbrt(const real_t&);

// hybrid algebraic operations
complex_t operator+(const int_t, const complex_t&);
complex_t operator+(const int, const complex_t&);
complex_t operator+(const number_t, const complex_t&);
complex_t operator+(const complex_t&, const int_t);
complex_t operator+(const complex_t&, const int);
complex_t operator+(const complex_t&, const number_t);
// complex_t operator+=(const int);
complex_t operator-(const int_t, const complex_t&);
complex_t operator-(const number_t, const complex_t&);
complex_t operator-(const int, const complex_t&);
complex_t operator-(const complex_t&, const int_t);
complex_t operator-(const complex_t&, const int);
complex_t operator-(const complex_t&, const number_t);
// complex_t operator-=(const int);
complex_t operator*(const int_t, const complex_t&);
complex_t operator*(const int, const complex_t&);
complex_t operator*(const number_t, const complex_t&);
complex_t operator*(const complex_t&, const int_t);
complex_t operator*(const complex_t&, const int);
complex_t operator*(const complex_t&, const number_t);
// complex_t operator*=(const int);
// complex_t operator/(const int, const complex_t&);
complex_t operator/(const complex_t& z, const int_t);
complex_t operator/(const complex_t& z, const int);
complex_t operator/(const complex_t& z, const number_t);
// complex_t operator/=(const int);

// other complex operations for consistency
complex_t cmplx(const real_t&);
real_t realPart(const complex_t&);
real_t realPart(const real_t&);
real_t imagPart(const complex_t&);
real_t imagPart(const real_t&);
real_t conj(const real_t&);
real_t tran(const real_t&);
complex_t tran(const complex_t&);
real_t adj(const real_t&);
complex_t adj(const complex_t&);
real_t diag(const real_t&);
complex_t diag(const complex_t&);
real_t norm2(const real_t&);
real_t norm2(const complex_t&);
complex_t maxAbsVal(const real_t&);
complex_t maxAbsVal(const complex_t&);

//! forced cast complex to any
template<class T>
inline T complexToT(const complex_t& c){return c;}
template<>
inline real_t complexToT(const complex_t& c){return c.real();}  //real specialization
inline real_t complexToRorC(const complex_t& c, const real_t& r) {return c.real();}
inline complex_t complexToRorC(const complex_t& c, const complex_t& cc) {return c;}

//! various useful definition to insure consistancy with other classes

inline complex_t cmplx(const real_t& x)
{  return complex_t(x);}

inline const complex_t& cmplx(const complex_t& x)
{  return x;}

inline real_t realPart(const complex_t& x)
{  return x.real();}

inline real_t realPart(const real_t& x)
{  return x;}

inline real_t imagPart(const complex_t& x)
{  return x.imag();}

inline real_t imagPart(const real_t& x)
{  return 0.;}

inline real_t conj(const real_t& r)
{  return r;}

inline real_t tran(const real_t& r)
{  return r;}

inline real_t transpose(const real_t& r)
{  return r;}

inline complex_t tran(const complex_t& c)
{  return c;}

inline complex_t transpose(const complex_t& c)
{  return c;}

inline real_t adj(const real_t& r)
{  return r;}

inline real_t adjoint(const real_t& r)
{  return r;}

inline complex_t adj(const complex_t& c)
{  return conj(c);}

inline complex_t adjoint(const complex_t& c)
{  return conj(c);}

inline real_t diag(const real_t& r)
{  return r;}

inline complex_t diag(const complex_t& z)
{  return z;}

inline real_t norm2(const real_t& r)
{    return std::abs(r);}

inline real_t norm2(const complex_t& z)
{    return std::abs(z);}

inline real_t norminfty(const real_t& r)
{    return std::abs(r);}

inline real_t norminfty(const complex_t& z)
{    return std::abs(z);}

inline complex_t maxAbsVal(const real_t& r)
{    return complex_t(r);}

inline complex_t maxAbsVal(const complex_t& z)
{    return z;}


} // end of namespace xlifepp;

#endif // COMPLEX_UTILS_HPP
