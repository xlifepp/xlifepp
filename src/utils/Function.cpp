/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Function.cpp
  \author E. Lunéville
  \since 1 nov 2011
  \date 3 may 2012

  \brief Implementation of xlifepp::Function class members and related functions
*/

//===============================================================================
// library dependencies
#include "Function.hpp"
//===============================================================================

namespace xlifepp
{

// utilities: convert type enum to string
string_t type2Str(ValueType t)
{
  switch(t)
    {
      case _real:
        return "real";

      case _complex:
        return "complex";

      default:
        return "undefined";
    }
}
string_t struct2Str(StrucType s)
{
  switch(s)
    {
      case _scalar:
        return "scalar";

      case _vector:
        return "vector";

      case _matrix:
        return "matrix";

      default:
        return "undefined";
    }
}
string_t typeFun2Str(FunctType tf)
{
  switch(tf)
    {
      case _function:
        return "function";

      case _kernel:
        return "kernel";

      default:
        return "undefined";
    }
}
string_t typeArg2Str(ArgType ta)
{
  switch(ta)
    {
      case _pointArg:
        return "single point";

      case _vectorOfPointArg:
        return "vector of points";

      default:
        return "undefined";
    }
}

// copy constructor
Function::Function(const Function & f, bool fullcopy)
{
  if(fullcopy) fullCopy(f);
  else
    {
      returnedType_=f.returnedType_;
      returnedStruct_=f.returnedStruct_;
      functionType_=f.functionType_;
      argType_=f.argType_;
      params_=f.params_;    //parameters shared
      freeParams_=false;
      dims_=f.dims_;
      name_=f.name_;
      fun_=f.fun_;
      checkType_=f.checkType_;
      conjugate_=f.conjugate_;
      transpose_=f.transpose_;
      xpar=f.xpar;
      xory=f.xory;
      grad_=f.grad_;
      requireNx=f.requireNx;
      requireNy=f.requireNy;
      requireTx=f.requireTx;
      requireTy=f.requireTy;
      requireElt=f.requireElt;
      requireDom=f.requireDom;
      requireDof=f.requireDof;
      dimPoint_=f.dimPoint_;
      table_= f.table_;
      pointToTabx_=f.pointToTabx_;
    }
}

// full copy of  a Function, the Parameters member is duplicated and, thus no longer shared
void Function::fullCopy(const Function & f)
{
  returnedType_=f.returnedType_;
  returnedStruct_=f.returnedStruct_;
  functionType_=f.functionType_;
  argType_=f.argType_;
  params_=nullptr;
  if(f.params_!=nullptr)
  {
    params_=new Parameters(*f.params_) ;
    freeParams_=true;                      //as params_ is now a copy, it is the responsability of  Function to delete it
  }
  dims_=f.dims_;
  name_=f.name_;
  fun_=f.fun_;
  checkType_=f.checkType_;
  conjugate_=f.conjugate_;
  transpose_=f.transpose_;
  xpar=f.xpar;
  xory=f.xory;
  grad_=nullptr;
  if(f.grad_!=nullptr) grad_=new Function(*f.grad_);
  requireNx=f.requireNx;
  requireNy=f.requireNy;
  requireTx=f.requireTx;
  requireTy=f.requireTy;
  requireElt=f.requireElt;
  requireDom=f.requireDom;
  requireDof=f.requireDof;
  dimPoint_=f.dimPoint_;
  table_= f.table_; // NO HARD COPY
  pointToTabx_=nullptr;
  if(f.pointToTabx_!=nullptr) pointToTabx_=new Function(*f.pointToTabx_);
}

// utilities: output
void Function::printInfo(std::ostream& out) const
{
  out << name_ << ": " << typeFun2Str(functionType_) << " of " << typeArg2Str(argType_);
  out << " returning " << type2Str(returnedType_) << " " << struct2Str(returnedStruct_);
  if (table_!=nullptr) out << " tabulated";
  if (requireNx||requireNy||requireElt||requireDom||requireDof) out << eol;
  string_t req="require";
  if (requireNx) { out << req << " n or nx"; req = ","; }
  if (requireNy) { out << req << " ny"; req = ","; }
  if (requireTx) { out << req << " t or tx"; req = ","; }
  if (requireTy) { out << req << " ty"; req = ","; }
  if (requireElt) { out << req << " element"; req = ","; }
  if (requireDom) { out << req << " domain"; req = ","; }
  if (requireDof) { out << req << " dof"; }
  if (theVerboseLevel<5) return;
  if (params_!=nullptr)
  {
    if (req != "require") out << eol;
    out << "Parameters: " << *params_;
  }
}

// utilities: error managment
void Function::isNotAFunction() const
{
  if(functionType_ != _function)
    {
      info("fun_desc", name_, typeFun2Str(functionType_), typeArg2Str(argType_), type2Str(returnedType_), struct2Str(returnedStruct_));
      error("is_not_fun");
    }
}
void Function::isNotAKernel() const
{
  if(functionType_ != _kernel)
    {
      info("fun_desc", name_, typeFun2Str(functionType_), typeArg2Str(argType_), type2Str(returnedType_), struct2Str(returnedStruct_));
      error("is_not_ker");
    }
}

//static funcion; initializes the map returnTypes using Run Time Information
void Function::initReturnTypes()
{
  typedef std::pair<ValueType, StrucType> struc_pair;
  typedef std::pair<string_t, struc_pair> map_pair;
  returnTypes.insert(map_pair(string_t("undefined"), struc_pair(_real, _scalar)));

  PointerF<real_t>::fun_t fr0;
  PointerF<real_t>::vfun_t vfr;
  PointerF<real_t>::ker_t kr;
  PointerF<real_t>::vker_t vkr;
  returnTypes.insert(map_pair(string_t(typeid(fr0).name()), struc_pair(_real, _scalar)));
  returnTypes.insert(map_pair(string_t(typeid(vfr).name()), struc_pair(_real, _scalar)));
  returnTypes.insert(map_pair(string_t(typeid(kr).name()), struc_pair(_real, _scalar)));
  returnTypes.insert(map_pair(string_t(typeid(vkr).name()), struc_pair(_real, _scalar)));
  PointerF<complex_t>::fun_t fc;
  PointerF<complex_t>::vfun_t vfc;
  PointerF<complex_t>::ker_t kc;
  PointerF<complex_t>::vker_t vkc;
  returnTypes.insert(map_pair(string_t(typeid(fc).name()), struc_pair(_complex, _scalar)));
  returnTypes.insert(map_pair(string_t(typeid(vfc).name()), struc_pair(_complex, _scalar)));
  returnTypes.insert(map_pair(string_t(typeid(kc).name()), struc_pair(_complex, _scalar)));
  returnTypes.insert(map_pair(string_t(typeid(vkc).name()), struc_pair(_complex, _scalar)));
  PointerF<Vector<real_t> >::fun_t fvr;
  PointerF<Vector<real_t> >::vfun_t vfvr;
  PointerF<Vector<real_t> >::ker_t kvr;
  PointerF<Vector<real_t> >::vker_t vkvr;
  returnTypes.insert(map_pair(string_t(typeid(fvr).name()), struc_pair(_real, _vector)));
  returnTypes.insert(map_pair(string_t(typeid(vfvr).name()), struc_pair(_real, _vector)));
  returnTypes.insert(map_pair(string_t(typeid(kvr).name()), struc_pair(_real, _vector)));
  returnTypes.insert(map_pair(string_t(typeid(vkvr).name()), struc_pair(_real, _vector)));
  PointerF<Vector<complex_t> >::fun_t fvc;
  PointerF<Vector<complex_t> >::vfun_t vfvc;
  PointerF<Vector<complex_t> >::ker_t kvc;
  PointerF<Vector<complex_t> >::vker_t vkvc;
  returnTypes.insert(map_pair(string_t(typeid(fvc).name()), struc_pair(_complex, _vector)));
  returnTypes.insert(map_pair(string_t(typeid(vfvc).name()), struc_pair(_complex, _vector)));
  returnTypes.insert(map_pair(string_t(typeid(kvc).name()), struc_pair(_complex, _vector)));
  returnTypes.insert(map_pair(string_t(typeid(vkvc).name()), struc_pair(_complex, _vector)));
  PointerF<Matrix<real_t> >::fun_t fmr;
  PointerF<Matrix<real_t> >::vfun_t vfmr;
  PointerF<Matrix<real_t> >::ker_t kmr;
  PointerF<Matrix<real_t> >::vker_t vkmr;
  returnTypes.insert(map_pair(string_t(typeid(fmr).name()), struc_pair(_real, _matrix)));
  returnTypes.insert(map_pair(string_t(typeid(vfmr).name()), struc_pair(_real, _matrix)));
  returnTypes.insert(map_pair(string_t(typeid(kmr).name()), struc_pair(_real, _matrix)));
  returnTypes.insert(map_pair(string_t(typeid(vkmr).name()), struc_pair(_real, _matrix)));
  PointerF<Matrix<complex_t> >::fun_t fmc;
  PointerF<Matrix<complex_t> >::vfun_t vfmc;
  PointerF<Matrix<complex_t> >::ker_t kmc;
  PointerF<Matrix<complex_t> >::vker_t vkmc;
  returnTypes.insert(map_pair(string_t(typeid(fmc).name()), struc_pair(_complex, _matrix)));
  returnTypes.insert(map_pair(string_t(typeid(vfmc).name()), struc_pair(_complex, _matrix)));
  returnTypes.insert(map_pair(string_t(typeid(kmc).name()), struc_pair(_complex, _matrix)));
  returnTypes.insert(map_pair(string_t(typeid(vkmc).name()), struc_pair(_complex, _matrix)));

  real_t r;
  complex_t c;
  returnArgs.insert(map_pair(string_t(typeid(r).name()), struc_pair(_real, _scalar)));
  returnArgs.insert(map_pair(string_t(typeid(c).name()), struc_pair(_complex, _scalar)));
  Vector<real_t> vr;
  Vector<complex_t> vc;
  returnArgs.insert(map_pair(string_t(typeid(vr).name()), struc_pair(_real, _vector)));
  returnArgs.insert(map_pair(string_t(typeid(vc).name()), struc_pair(_complex, _vector)));
  Matrix<real_t> mr;
  Matrix<complex_t> mc;
  returnArgs.insert(map_pair(string_t(typeid(mr).name()), struc_pair(_real, _matrix)));
  returnArgs.insert(map_pair(string_t(typeid(mc).name()), struc_pair(_complex, _matrix)));
  Vector<Vector<real_t> > vvr;
  Vector<Vector<complex_t> > vvc;
  returnArgs.insert(map_pair(string_t(typeid(vvr).name()), struc_pair(_real, _vector)));
  returnArgs.insert(map_pair(string_t(typeid(vvc).name()), struc_pair(_complex, _vector)));
  Vector<Matrix<real_t> > vmr;
  Vector<Matrix<complex_t> > vmc;
  returnArgs.insert(map_pair(string_t(typeid(vmr).name()), struc_pair(_real, _matrix)));
  returnArgs.insert(map_pair(string_t(typeid(vmc).name()), struc_pair(_complex, _matrix)));
}

// main initialisation function used by constructors
void Function::init(void* f, const string_t& na, const string_t& fn, FunctType tf, ArgType ta, Parameters& pa, dimen_t d)
{
  fun_ = f;
  functionType_ = tf;
  argType_ = ta;
  if(&pa == &defaultParameters) //create new parameters
    {
      params_= new Parameters();
      freeParams_=true;
    }
  else params_ = &pa;
  returnedType_ = _real;
  returnedStruct_ = _scalar;
  std::map<string_t, std::pair<ValueType, StrucType> >::iterator itr=returnTypes.find(fn);
  if(itr!=returnTypes.end())
    {
      returnedType_ = itr->second.first;
      returnedStruct_ = itr->second.second;
    }
  else error("funtype_not_found");
  dims_=dimPair(1,1);
  name_ = na;
  dimPoint_=d;

  //check for deprecated normal transmission
  if(params_->contains("_n"))
    {
      warning("free_warning","associate normal vector n using Parameters of function is deprecated, use Function::require(_n)");
      requireNx=true;
    }
  if(params_->contains("_nx"))
    {
      warning("free_warning","associate normal vector nx using Parameters of function is deprecated, use Function::require(_nx)");
      requireNx=true;
    }
  if(params_->contains("_ny"))
    {
      warning("free_warning","associate normal vector ny using Parameters of function is deprecated, use Function::require(_ny)");
      requireNy=true;
    }
  if(params_->contains("_tau"))
    {
      warning("free_warning","associate normal vector n using Parameters of function is deprecated, use Function::require(_tau)");
      requireTx=true;
    }
  if(params_->contains("_taux"))
    {
      warning("free_warning","associate normal vector nx using Parameters of function is deprecated, use Function::require(_taux)");
      requireTx=true;
    }
  if(params_->contains("_tauy"))
    {
      warning("free_warning","associate normal vector ny using Parameters of function is deprecated, use Function::require(_tauy)");
      requireTy=true;
    }
}

Point Function::fakePoint() const
{
  Point x; x.resize(dimPoint_,0.);
  return x;
}

Vector<Point> Function::fakePoints() const
{
  Point x=fakePoint();
  return Vector<Point>(1,x);
}

// constructors of Function along function types
Function::Function()
{
  returnedType_=_none; returnedStruct_=_undefStrucType;
  functionType_=_function; argType_=_pointArg;
  dims_=dimPair(1,1);
  name_ = "?";
  dimPoint_=3;
}

//! constructor along the function prototypes
Function::Function(funSR_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _pointArg, pa);
}
Function::Function(funSC_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _pointArg, pa);
}
Function::Function(funVR_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funVC_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funMR_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funMC_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(kerSR_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _pointArg, pa);
}
Function::Function(kerSC_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _pointArg, pa);
}
Function::Function(kerVR_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerVC_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerMR_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerMC_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(vfunSR_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _vectorOfPointArg, pa);
}
Function::Function(vfunSC_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _vectorOfPointArg, pa);
}
Function::Function(vfunVR_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunVC_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunMR_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunMC_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vkerSR_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _vectorOfPointArg, pa);
}
Function::Function(vkerSC_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _vectorOfPointArg, pa);
}
Function::Function(vkerVR_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerVC_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerMR_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerMC_t& f, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}

//! constructor along the function prototypes
Function::Function(funSR_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _pointArg, pa, d);
}
Function::Function(funSC_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _pointArg, pa, d);
}
Function::Function(funVR_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funVC_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funMR_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funMC_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(kerSR_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _pointArg, pa, d);
}
Function::Function(kerSC_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _pointArg, pa, d);
}
Function::Function(kerVR_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerVC_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerMR_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerMC_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(vfunSR_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _vectorOfPointArg, pa, d);
}
Function::Function(vfunSC_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _vectorOfPointArg, pa, d);
}
Function::Function(vfunVR_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunVC_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunMR_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunMC_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _function, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vkerSR_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
}
Function::Function(vkerSC_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
}
Function::Function(vkerVR_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerVC_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerMR_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerMC_t& f, dimen_t d, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), "?", typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}

//! constructor with function name (string) along the function prototypes
Function::Function(funSR_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _pointArg, pa);
}
Function::Function(funSC_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _pointArg, pa);
}
Function::Function(funVR_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funVC_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funMR_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funMC_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(kerSR_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _pointArg, pa);
}
Function::Function(kerSC_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _pointArg, pa);
}
Function::Function(kerVR_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerVC_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerMR_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerMC_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(vfunSR_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _vectorOfPointArg, pa);
}
Function::Function(vfunSC_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _vectorOfPointArg, pa);
}
Function::Function(vfunVR_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunVC_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunMR_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunMC_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vkerSR_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _vectorOfPointArg, pa);
}
Function::Function(vkerSC_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _vectorOfPointArg, pa);
}
Function::Function(vkerVR_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerVC_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerMR_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerMC_t& f, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}

//! constructor with function name (string) along the function prototypes
Function::Function(funSR_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _pointArg, pa, d);
}
Function::Function(funSC_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _pointArg, pa, d);
}
Function::Function(funVR_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funVC_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funMR_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funMC_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(kerSR_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _pointArg, pa, d);
}
Function::Function(kerSC_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _pointArg, pa, d);
}
Function::Function(kerVR_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerVC_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerMR_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerMC_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(vfunSR_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _vectorOfPointArg, pa, d);
}
Function::Function(vfunSC_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _vectorOfPointArg, pa, d);
}
Function::Function(vfunVR_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunVC_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunMR_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunMC_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _function, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vkerSR_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
}
Function::Function(vkerSC_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
}
Function::Function(vkerVR_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerVC_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerMR_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerMC_t& f, dimen_t d, const string_t& na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), na, typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}

//! constructor with function name (const char*) along the function prototypes
Function::Function(funSR_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _pointArg, pa);
}
Function::Function(funSC_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _pointArg, pa);
}
Function::Function(funVR_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funVC_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funMR_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funMC_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(kerSR_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _pointArg, pa);
}
Function::Function(kerSC_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _pointArg, pa);
}
Function::Function(kerVR_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerVC_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerMR_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerMC_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _pointArg, pa);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(vfunSR_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _vectorOfPointArg, pa);
}
Function::Function(vfunSC_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _vectorOfPointArg, pa);
}
Function::Function(vfunVR_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunVC_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunMR_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunMC_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vkerSR_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _vectorOfPointArg, pa);
}
Function::Function(vkerSC_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _vectorOfPointArg, pa);
}
Function::Function(vkerVR_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerVC_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerMR_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerMC_t& f, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _vectorOfPointArg, pa);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}

//! constructor with function name (const char*) along the function prototypes
Function::Function(funSR_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _pointArg, pa, d);
}
Function::Function(funSC_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _pointArg, pa, d);
}
Function::Function(funVR_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funVC_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funMR_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(funMC_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  dims_=f(x,pa).dims();
}
Function::Function(kerSR_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _pointArg, pa, d);
}
Function::Function(kerSC_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _pointArg, pa, d);
}
Function::Function(kerVR_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerVC_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerMR_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(kerMC_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _pointArg, pa, d);
  Point x=fakePoint();
  Vector<real_t> n(x.dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(x,x+1,pa).dims();
}
Function::Function(vfunSR_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _vectorOfPointArg, pa, d);
}
Function::Function(vfunSC_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _vectorOfPointArg, pa, d);
}
Function::Function(vfunVR_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunVC_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunMR_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vfunMC_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _function, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,pa).begin()->dims();
}
Function::Function(vkerSR_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
}
Function::Function(vkerSC_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
}
Function::Function(vkerVR_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerVC_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerMR_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}
Function::Function(vkerMC_t& f, dimen_t d, const char* na, Parameters& pa)
{
  init(reinterpret_cast<void*>(&f), string_t(na), typeid(f).name(), _kernel, _vectorOfPointArg, pa, d);
  Vector<Point> vx=fakePoints();
  Vector<real_t> n(vx[0].dim(),0);     //fake normal vector
  setNx(&n); setNy(&n);
  setTx(&n); setTy(&n);
  dims_=f(vx,vx+(vx[0]+1.),pa).begin()->dims();
}

Function::~Function()
{
  //if(freeParams_) delete params_; //because params pointer may be shared, do not destroy -> possible memory leak
}

//access to parameter by name or rank, whenever it exists
Parameter& Function::parameter(const string_t& s) const
{
  if(params_==nullptr )  error("param_not_found",s);
  if(!params_->contains(s)) setParam(0,s);
  return (*params_)(s);
}

Parameter& Function::parameter(const char* s) const
{
  if(params_==nullptr ) error("param_not_found",s);
  if(!params_->contains(s)) setParam(0,string_t(s));
  return (*params_)(s);
}

Parameter& Function::parameter(const size_t n) const
{
  if(params_==nullptr || n ==0 || n > params_->size())  error("param_badind",n,params_->size());
  return (*params_)(n);
}

//!< associate a unitary vector to the function - DEPRECATED -
void Function::associateVector(UnitaryVector uv, number_t d)
{
  if(uv==_n)
    {
      requireNx=true;
      setNx(new Vector<real_t>(d,0.));
      warning("deprecated","Parameters::associateVector(UnitaryVector)","Function::require(_n)");
      return;
    }
  if(uv==_nx)
    {
      requireNx=true;
      setNx(new Vector<real_t>(d,0.));
      warning("deprecated","Parameters::associateVector(UnitaryVector)","Function::require(_nx)");
      return;
    }
  if(uv==_ny)
    {
      requireNy=true;
      setNy(new Vector<real_t>(d,0.));
      warning("deprecated","Parameters::associateVector(UnitaryVector)","Function::require(_ny)");
      return;
    }
  if(uv==_tau)
    {
      requireTx=true;
      setTx(new Vector<real_t>(d,0.));
      warning("deprecated","Parameters::associateVector(UnitaryVector)","Function::require(_n)");
      return;
    }
  if(uv==_taux)
    {
      requireNx=true;
      setTx(new Vector<real_t>(d,0.));
      warning("deprecated","Parameters::associateVector(UnitaryVector)","Function::require(_nx)");
      return;
    }
  if(uv==_tauy)
    {
      requireTy=true;
      setTy(new Vector<real_t>(d,0.));
      warning("deprecated","Parameters::associateVector(UnitaryVector)","Function::require(_ny)");
      return;
    }
  where("Function::associateVector(UnitaryVector)");
  warning("deprecated","Parameters::associateVector(UnitaryVector)","Function::require()");
  error("unitaryvector_not_handled",words("normal",uv));
}

void Function::createTabular(real_t x0, real_t dx, number_t nx, const string_t& nax) //! tabulating f(x)
{
  if(returnedStruct_ == _scalar)
    {
      if(returnedType_ == _real) createTabularT(real_t(0),x0,dx,nx,nax);
      else createTabularT(complex_t(0),x0,dx,nx,nax);
    }
  else error("not_yet_handled","Function::createTabular for non scalar function");
}

void Function::createTabular(real_t x0, real_t dx, number_t nx, real_t y0, real_t dy, number_t ny,
                             const string_t& nax, const string_t& nay) //! tabulating f(x,y)
{
  if(returnedStruct_ == _scalar)
    {
      if(returnedType_ == _real) createTabularT(real_t(0),x0,dx,nx,y0,dy,ny,nax,nay);
      else createTabularT(complex_t(0),x0,dx,nx,y0,dy,ny,nax,nay);
    }
  else error("not_yet_handled","Function::createTabular for non scalar function");

}

void Function::createTabular(real_t x0, real_t dx, number_t nx,
                             real_t y0, real_t dy, number_t ny,
                             real_t z0, real_t dz, number_t nz,
                             const string_t& nax, const string_t& nay, const string_t& naz) //! tabulating f(x,y,z)
{
  if(returnedStruct_ == _scalar)
    {
      if(returnedType_ == _real) createTabularT(real_t(0),x0,dx,nx,y0,dy,ny,z0,dz,nz,nax,nay,naz);
      else createTabularT(complex_t(0),x0,dx,nx,y0,dy,ny,z0,dz,nz,nax,nay,naz);
    }
  else error("not_yet_handled","Function::createTabular for non scalar function");
}

// evaluate function at a point, returning a Value
Value Function::operator()(const Point& x) const
{
   switch(returnedStruct_)
   {
    case _scalar :
      {
        if(returnedType_==_real) {real_t res; return Value((*this)(x,res));}
        else {complex_t res; return Value((*this)(x,res));}
      }
      break;
    case _vector :
      {
        if(returnedType_==_real) {Vector<real_t> res; return Value((*this)(x,res));}
        else {Vector<complex_t> res; return Value((*this)(x,res));}
      }
      break;
    case _matrix :
      {
        if(returnedType_==_real) {Matrix<real_t> res; return Value((*this)(x,res));}
        else {Matrix<complex_t> res; return Value((*this)(x,res));}
      }
      break;
   }
   return Value(0); //fake return
}
// evaluate kernel function at a couple of points, returning a Value
Value Function::operator()(const Point& x, const Point& y) const
{
   switch(returnedStruct_)
   {
    case _scalar :
      {
        if(returnedType_==_real) {real_t res; return Value((*this)(x,y,res));}
        else {complex_t res; return Value((*this)(x,y,res));}
      }
      break;
    case _vector :
      {
        if(returnedType_==_real) {Vector<real_t> res; return Value((*this)(x,y,res));}
        else {Vector<complex_t> res; return Value((*this)(x,y,res));}
      }
      break;
    case _matrix :
      {
        if(returnedType_==_real) {Matrix<real_t> res; return Value((*this)(x,y,res));}
        else {Matrix<complex_t> res; return Value((*this)(x,y,res));}
      }
      break;
   }
   return Value(0); //fake return
}

//compare functions (same function and param pointers)
bool operator==(const Function& f1, const Function& f2)
{
  return (f1.fun_p()==f2.fun_p() && f1.params_p()==f2.params_p());
}

bool operator!=(const Function& f1, const Function& f2)
{
  return !(f1==f2);
}

//some constant functions used to create constant Function object
real_t real_const_fun(const Point& P, Parameters& pa)
{
  real_t v=0;
  return pa.get("const_value", v);
}

complex_t complex_const_fun(const Point& P, Parameters& pa)
{
  complex_t v=0;
  return pa.get("const_value", v);
}

Vector<real_t> real_vector_const_fun(const Point& P, Parameters& pa)
{
  const void* p=nullptr;
  p=pa.get("const_vector_value", p);
  if(p!=nullptr) return *reinterpret_cast<const Vector<real_t>* >(p);
  return Vector<real_t>(0.);
}

Vector<complex_t> complex_vector_const_fun(const Point& P, Parameters& pa)
{
  const void* p=nullptr;
  p=pa.get("const_vector_value", p);
  if(p!=nullptr) return *reinterpret_cast<const Vector<complex_t>* >(p);
  return Vector<complex_t>(1,complex_t(0.));
}

Matrix<real_t> real_matrix_const_fun(const Point& P, Parameters& pa)
{
  const void* p=nullptr;
  p=pa.get("const_matrix_value", p);
  if(p!=nullptr) return *reinterpret_cast<const Matrix<real_t>* >(p);
  return 0;
}

Matrix<complex_t> complex_matrix_const_fun(const Point& P, Parameters& pa)
{
  const void* p=nullptr;
  p=pa.get("const_matrix_value", p);
  if(p!=nullptr) return *reinterpret_cast<const Matrix<complex_t>* >(p);
  return 0;
}

std::ostream& operator<<(std::ostream& os, const Function& f)
{
  f.printInfo(os);
  return os;
}

Parameter::Parameter(const Function& fct, const string_t& nm, const string_t& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pointerFunction)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  p_ = static_cast<void *>(new Function(fct));
}

Parameter::Parameter(const Function& fct, const string_t& nm, const Strings& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pointerFunction)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  p_ = static_cast<void *>(new Function(fct));
}

Parameter& Parameter::operator=(const Function& fct)
{
  if (p_!=nullptr) deletePointer();
  p_ = static_cast<void*>(new Function(fct));
  type_=_pointerFunction;
  return *this;
}

void deleteFunction(void *p)
{
  if (p!=nullptr) delete reinterpret_cast<Function*>(p);
}

void* cloneFunction(const void* p)
{
  return reinterpret_cast<void*>(new Function(*reinterpret_cast<const Function*>(p)));
}

} // end of namespace xlifepp

