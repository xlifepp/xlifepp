/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Graph.hpp
  \authors D. Martin, E. Lunéville
  \since 7 may 2005
  \date 3 may 2012

  \brief Definition of the xlifepp::Graph class

  A xlifepp::Graph object is a set of "nodes" and "connection set" between them.
  It is implemented as a child class of stl:vector as a vector<vector<xlifepp::number_t> >
  Each node is represented by a vector<xlifepp::number_t> containing the connection between this node and others.
  Node ni and nj are "connected" if nj belongs to vector<number_t> defining ni.
  The degree of node ni is the number of nodes connected to it.
 */

#ifndef GRAPH_HPP
#define GRAPH_HPP

#include "config.h"
#include "PrintStream.hpp"

namespace xlifepp
{

/*!
  \class Graph
  class to deal with general graphs
*/
class Graph : public std::vector<std::vector<number_t> >
{
  public:
    Graph(number_t dim) //! basic constructor
    { resize(dim); }

    void sortByAscendingDegree(); //!< sort Graph by ascending degree (i.e. by vector<number_t> size)
    number_t nodeDegree(number_t n) const //! degree of node n
    {return ((*(begin() + n - 1)).size());}
    number_t maximumDegree() const;  //!< compute maximum Degree i.e. maximum number of neighbors taken over all nodes
    //! compute band width (maximum distance between connected nodes) and length of skyline storage associated to sets
    std::pair<number_t, number_t> bandWitdhAndSkyline() const;
    number_t bandWidth() const; //!< compute band width (maximum distance between connected nodes)
    number_t skylineSize() const; //!< compute length of skyline storage associated to sets
    std::vector<number_t> renumber(number_t nb_Nodes); //!< renumbering algorithm to minimize band width
    number_t renumEngine(std::vector<bool>& markers, std::vector<number_t>& n_tree,
                       number_t& itree, number_t max_degree, number_t& i_begin, number_t& i_level); //!< renumbering algorithm to minimize band width

    void print(std::ostream&, const string_t&) const; //!< print utility
    void print(PrintStream& os, const string_t& s) const {print(os.currentStream(),s);}
    void print(const string_t& title = "") const; //!< print utility
    void printNodes(std::ostream&, const std::vector<number_t>&); //!< print utility
    void printNodes(PrintStream& os, const std::vector<number_t>& ns)  {printNodes(os.currentStream(), ns);}
    void printNodes(const std::vector<number_t>&); //!< print utility

}; // end of class Graph

} // end of namespace xlifepp

#endif // GRAPH.HPP
