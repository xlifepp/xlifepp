/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SymbolicFunction.hpp
  \author E. Lunéville
  \since 11 feb 2017
  \date 11 feb 2017

  \brief SymbolicFunction class

  SymbolicFunction class handles any function of 1 to 3 variables (say x_1, x_2, x_3)  in a symbolic form where
    - available algebraic binary operations  are + - * / ^
    - available boolean binary operations  are == != < <= > >= && ||
    - available unary operations are provided by the SymbolicOperation enumeration (most of standard mathematical function)
    - available constant are real scalars or complex scalars

  for instance to define a symbolic function, write simply
      SymbolicFunction fs = sin(x_1)*cos(x_2)-2*sqrt(x_1*x_2);
      SymbolicFunction fs = (x_1 > 1) && (x_2 < 1);

   The order of operations follows the C++ order rules
   To evaluate the function, write as usual
      fs(0.5,1);             //real evaluation
      fs(Complex(0.,1.),1.); //complex evaluation
      when fs is a boolean expression the result is either 0 (false) or 1 (true) returned as a real_t or a complex_t regarding the input arguments
 */

#ifndef SYMBOLICFUNCTION_HPP_INCLUDED
#define SYMBOLICFUNCTION_HPP_INCLUDED

#include <string>
#include <complex>
#include <vector>
#include <set>
#include <iostream>
#include <cstdlib>
#include "config.h" // typedef, enum, ... definitions
#include "Algorithms.hpp"
#include "Messages.hpp"
#include "PrintStream.hpp"

namespace xlifepp
{

/*!
  \class SymbolicFunction
  binary node to represent symbolic functions
*/
class SymbolicFunction
{
  public:
    const SymbolicFunction* fn1; //!< first node involved (0 by default)
    const SymbolicFunction* fn2; //!< second node involved (0 by default)
    VariableName var;            //!< variable involved (undef by default)
    SymbolicOperation op;        //!< operation involved (id by default)
    complex_t coef;              //!< coefficient to apply to
    complex_t par;               //!< additional parameter required by some operation (e.g power)

    //@{
    //! constructor/destructor
    explicit SymbolicFunction(VariableName v=_varUndef) : fn1(nullptr),fn2(nullptr),var(v),op(_idop), coef(1.), par(0.) {}
    explicit SymbolicFunction(const real_t& r) : fn1(nullptr),fn2(nullptr),var(_varUndef),op(_idop), coef(r), par(0.) {}
    explicit SymbolicFunction(const complex_t& c) : fn1(nullptr),fn2(nullptr),var(_varUndef),op(_idop), coef(c), par(0.) {}
    SymbolicFunction(const SymbolicFunction& f, SymbolicOperation o, complex_t c=1.,complex_t p=0.)
      : fn2(nullptr), var(_varUndef), op(o), coef(1.), par(p) {fn1=new SymbolicFunction(f);reduce();}
    SymbolicFunction(const SymbolicFunction& f1, const SymbolicFunction& f2, SymbolicOperation o, complex_t c=1., complex_t p=0.)
      : var(_varUndef),op(o), coef(1.), par(p) {fn1=new SymbolicFunction(f1);fn2=new SymbolicFunction(f2);reduce();}
    SymbolicFunction(const SymbolicFunction& fn)
    {
        fn1=nullptr; if(fn.fn1!=nullptr) fn1 = new SymbolicFunction(*fn.fn1);
        fn2=nullptr; if(fn.fn2!=nullptr) fn2 = new SymbolicFunction(*fn.fn2);
        var=fn.var; op=fn.op; coef=fn.coef; par=fn.par;
    }

    ~SymbolicFunction() {if(fn1!=nullptr) delete fn1; if(fn2!=nullptr) delete fn2;}
    //@}

    SymbolicFunction& operator=(const SymbolicFunction& fn) {
      delete fn1; fn1=nullptr; if(fn.fn1!=nullptr) { fn1 = new SymbolicFunction(*fn.fn1); }
      delete fn2; fn2=nullptr; if(fn.fn2!=nullptr) { fn2 = new SymbolicFunction(*fn.fn2); }
      var=fn.var; op=fn.op; coef=fn.coef; par=fn.par;
      return *this;
    }

    bool isSingle() const                      //! true if a single expression (constant or variable name)
    {return fn1==nullptr && fn2==nullptr;}
    bool isConst() const                       //! true if a constant expression
    {return isSingle() && var==_varUndef;}
    bool isVar() const                         //! true if a var expression
    {return isSingle() && var!=_varUndef;}
    std::set<VariableName> listOfVar() const;  //!< list of involved variables
    number_t numberOfVar()const {return listOfVar().size();}
    ValueType valueType() const;               //!< value type (real or complex) of function

    void reduceFun();                          //!< reduce function in symbolic function
    void reduceConst();                        //!< reduce constant in symbolic function
    void reduce();                             //!< reduce symbolic function

    complex_t operator()(const std::vector<complex_t>& xs) const;
    complex_t operator()(const complex_t& x1) const
    {return (*this)(std::vector<complex_t>(1,x1));}
    complex_t operator()(const complex_t& x1, const complex_t& x2) const
    {std::vector<complex_t> xs(2,x1); xs[1]=x2; return (*this)(xs);}
    complex_t operator()(const complex_t& x1, const complex_t& x2, const complex_t& x3) const
    {std::vector<complex_t> xs(3,x1); xs[1]=x2; xs[2]=x3; return (*this)(xs);}

    real_t operator()(const std::vector<real_t>& xs) const;
    real_t operator()(const real_t& x1) const
    {return (*this)(std::vector<real_t>(1,x1));}
    real_t operator()(const real_t& x1, const real_t& x2) const
    {std::vector<real_t> xs(2,x1); xs[1]=x2; return (*this)(xs);}
    real_t operator()(const real_t& x1, const real_t& x2, const real_t& x3) const
    {std::vector<real_t> xs(3,x1); xs[1]=x2; xs[2]=x3; return (*this)(xs);}

    string_t asString(number_t l=0, bool par=false, const string_t& n1="", const string_t& n2="",const string_t& n3="") const;
    void print(std::ostream& out=std::cout) const;
    void printTree(std::ostream& out=std::cout, int lev=0) const;
};

inline std::ostream& operator<<(std::ostream& out, const SymbolicFunction& fn)
{fn.print(out); return out;}
string_t varName(VariableName v);
string_t opName(SymbolicOperation o);
bool isequal(const SymbolicFunction&, const SymbolicFunction&); //!< check equality of two symbolic functions

//binary operation on SymbolicFunction's
inline SymbolicFunction& operator+(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_plus);}
inline SymbolicFunction& operator-(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_minus);}
inline SymbolicFunction& operator*(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_multiply);}
inline SymbolicFunction& operator/(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_divide);}
inline SymbolicFunction& operator^(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_power);}
inline SymbolicFunction& operator==(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_equal);}
inline SymbolicFunction& operator!=(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_different);}
inline SymbolicFunction& operator>(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_greater);}
inline SymbolicFunction& operator>=(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_greaterequal);}
inline SymbolicFunction& operator<(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_less);}
inline SymbolicFunction& operator<=(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_lessequal);}
inline SymbolicFunction& operator&&(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_and);}
inline SymbolicFunction& operator||(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_or);}
inline SymbolicFunction& atan2(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_atan2);}
inline SymbolicFunction& power(const SymbolicFunction& f1, const SymbolicFunction& f2) {return * new SymbolicFunction(f1,f2,_power);}

//binary operation on SymbolicFunction and constant
inline SymbolicFunction& operator+(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_plus);}
inline SymbolicFunction& operator-(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_minus);}
inline SymbolicFunction& operator*(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_multiply);}
inline SymbolicFunction& operator/(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_divide);}
inline SymbolicFunction& operator^(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_power);}
inline SymbolicFunction& operator==(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_equal);}
inline SymbolicFunction& operator!=(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_different);}
inline SymbolicFunction& operator>(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_greater);}
inline SymbolicFunction& operator>=(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_greaterequal);}
inline SymbolicFunction& operator<(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_less);}
inline SymbolicFunction& operator<=(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_lessequal);}
inline SymbolicFunction& atan2(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_atan2);}
inline SymbolicFunction& power(const SymbolicFunction & f, const real_t& r)
{SymbolicFunction f2(r); return *new SymbolicFunction(f,f2,_power);}


inline SymbolicFunction& operator+(const SymbolicFunction & f, const complex_t& c)
{SymbolicFunction f2(c); return *new SymbolicFunction(f,f2,_plus);}
inline SymbolicFunction& operator-(const SymbolicFunction & f, const complex_t& c)
{SymbolicFunction f2(c); return *new SymbolicFunction(f,f2,_minus);}
inline SymbolicFunction& operator*(const SymbolicFunction & f, const complex_t& c)
{SymbolicFunction f2(c); return *new SymbolicFunction(f,f2,_multiply);}
inline SymbolicFunction& operator/(const SymbolicFunction & f, const complex_t& c)
{SymbolicFunction f2(c); return *new SymbolicFunction(f,f2,_divide);}
inline SymbolicFunction& operator==(const SymbolicFunction & f, const complex_t& c)
{SymbolicFunction f2(c); return *new SymbolicFunction(f,f2,_equal);}
inline SymbolicFunction& operator!=(const SymbolicFunction & f, const complex_t& c)
{SymbolicFunction f2(c); return *new SymbolicFunction(f,f2,_different);}
inline SymbolicFunction& operator>(const SymbolicFunction & f, const complex_t& c)
{SymbolicFunction f2(c); return *new SymbolicFunction(f,f2,_greater);}
inline SymbolicFunction& operator>=(const SymbolicFunction & f, const complex_t& c)
{SymbolicFunction f2(c); return *new SymbolicFunction(f,f2,_greaterequal);}
inline SymbolicFunction& operator<(const SymbolicFunction & f, const complex_t& c)
{SymbolicFunction f2(c); return *new SymbolicFunction(f,f2,_less);}
inline SymbolicFunction& operator<=(const SymbolicFunction & f, const complex_t& c)
{SymbolicFunction f2(c); return *new SymbolicFunction(f,f2,_lessequal);}

//binary operation on constant and SymbolicFunction
inline SymbolicFunction& operator+(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r); return *new SymbolicFunction(f1,f,_plus);}
inline SymbolicFunction& operator-(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r); return *new SymbolicFunction(f1,f,_minus);}
inline SymbolicFunction& operator*(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r);  return * new SymbolicFunction(f1,f,_multiply);}
inline SymbolicFunction& operator/(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r);  return * new SymbolicFunction(f1,f,_divide);}
inline SymbolicFunction& operator^(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r);  return * new SymbolicFunction(f1,f,_power);}
inline SymbolicFunction& operator==(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r); return *new SymbolicFunction(f1,f,_equal);}
inline SymbolicFunction& operator!=(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r); return *new SymbolicFunction(f1,f,_different);}
inline SymbolicFunction& operator>(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r);  return * new SymbolicFunction(f1,f,_greater);}
inline SymbolicFunction& operator>=(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r);  return * new SymbolicFunction(f1,f,_greaterequal);}
inline SymbolicFunction& operator<(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r); return *new SymbolicFunction(f1,f,_less);}
inline SymbolicFunction& operator<=(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r); return *new SymbolicFunction(f1,f,_lessequal);}
inline SymbolicFunction& atan2(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r); return *new SymbolicFunction(f1,f,_atan2);}
inline SymbolicFunction& power(const real_t& r, const SymbolicFunction & f)
{SymbolicFunction f1(r); return *new SymbolicFunction(f1,f,_power);}

inline SymbolicFunction& operator+(const complex_t& c, const SymbolicFunction & f)
{SymbolicFunction f1(c); return *new SymbolicFunction(f1,f,_plus);}
inline SymbolicFunction& operator-(const complex_t& c, const SymbolicFunction & f)
{SymbolicFunction f1(c); return *new SymbolicFunction(f1,f,_minus);}
inline SymbolicFunction& operator*(const complex_t& c, const SymbolicFunction & f)
{SymbolicFunction f1(c);  return * new SymbolicFunction(f1,f,_multiply);}
inline SymbolicFunction& operator/(const complex_t& c, const SymbolicFunction & f)
{SymbolicFunction f1(c);  return * new SymbolicFunction(f1,f,_divide);}
inline SymbolicFunction& operator==(const complex_t& c, const SymbolicFunction & f)
{SymbolicFunction f1(c); return *new SymbolicFunction(f1,f,_equal);}
inline SymbolicFunction& operator!=(const complex_t& c, const SymbolicFunction & f)
{SymbolicFunction f1(c); return *new SymbolicFunction(f1,f,_different);}
inline SymbolicFunction& operator>(const complex_t& c, const SymbolicFunction & f)
{SymbolicFunction f1(c);  return * new SymbolicFunction(f1,f,_greater);}
inline SymbolicFunction& operator>=(const complex_t& c, const SymbolicFunction & f)
{SymbolicFunction f1(c);  return * new SymbolicFunction(f1,f,_greaterequal);}
inline SymbolicFunction& operator<(const complex_t& c, const SymbolicFunction & f)
{SymbolicFunction f1(c); return *new SymbolicFunction(f1,f,_less);}
inline SymbolicFunction& operator<=(const complex_t& c, const SymbolicFunction & f)
{SymbolicFunction f1(c); return *new SymbolicFunction(f1,f,_lessequal);}

//unary operation on SymbolicFunction
inline SymbolicFunction& operator-(const SymbolicFunction& f)
{SymbolicFunction& f1=* new SymbolicFunction(f); f1.coef*=-1.; return f1;}
inline SymbolicFunction& operator+(const SymbolicFunction& f) {return * new SymbolicFunction(f);}
inline SymbolicFunction& operator!(const SymbolicFunction& f)  {return * new SymbolicFunction(f,_not);}
inline SymbolicFunction& abs(const SymbolicFunction& f)  {return * new SymbolicFunction(f,_abs);}
inline SymbolicFunction& sign(const SymbolicFunction& f)  {return * new SymbolicFunction(f,_sign);}
inline SymbolicFunction& real(const SymbolicFunction& f) {return * new SymbolicFunction(f,_realPart);}
inline SymbolicFunction& imag(const SymbolicFunction& f) {return * new SymbolicFunction(f,_imagPart);}
inline SymbolicFunction& conj(const SymbolicFunction& f) {return * new SymbolicFunction(f,_conj);}
inline SymbolicFunction& sqrt(const SymbolicFunction& f) {return * new SymbolicFunction(f,_sqrt);}
inline SymbolicFunction& squared(const SymbolicFunction& f) {return * new SymbolicFunction(f,_squared);}
inline SymbolicFunction& sin(const SymbolicFunction& f)  {return * new SymbolicFunction(f,_sin);}
inline SymbolicFunction& cos(const SymbolicFunction& f)  {return * new SymbolicFunction(f,_cos);}
inline SymbolicFunction& tan(const SymbolicFunction& f)  {return * new SymbolicFunction(f,_tan);}
inline SymbolicFunction& asin(const SymbolicFunction& f) {return * new SymbolicFunction(f,_asin);}
inline SymbolicFunction& acos(const SymbolicFunction& f) {return * new SymbolicFunction(f,_acos);}
inline SymbolicFunction& atan(const SymbolicFunction& f) {return * new SymbolicFunction(f,_atan);}
inline SymbolicFunction& sinh(const SymbolicFunction& f) {return * new SymbolicFunction(f,_sinh);}
inline SymbolicFunction& cosh(const SymbolicFunction& f) {return * new SymbolicFunction(f,_cosh);}
inline SymbolicFunction& tanh(const SymbolicFunction& f) {return * new SymbolicFunction(f,_tanh);}
inline SymbolicFunction& exp(const SymbolicFunction& f)  {return * new SymbolicFunction(f,_exp);}
inline SymbolicFunction& log(const SymbolicFunction& f)  {return * new SymbolicFunction(f,_log);}
inline SymbolicFunction& log10(const SymbolicFunction& f) {return * new SymbolicFunction(f,_log10);}
inline SymbolicFunction& pow(const SymbolicFunction& f, const double& p) {return * new SymbolicFunction(f,_pow,1.,p);}
inline SymbolicFunction& asinh(const SymbolicFunction& f) {return * new SymbolicFunction(f,_asinh);}
inline SymbolicFunction& acosh(const SymbolicFunction& f) {return * new SymbolicFunction(f,_acosh);}
inline SymbolicFunction& atanh(const SymbolicFunction& f) {return * new SymbolicFunction(f,_atanh);}

//binary operation
inline real_t evalOp(SymbolicOperation op, const real_t& x, const real_t& y)
{
  switch (op)
  {
    case _plus: return x+y;
    case _minus: return x-y;
    case _multiply: return x*y;
    case _divide: return x/y;
    case _power: return std::pow(x,y);
    case _atan2: return std::atan2(x,y);
    case _equal: return real_t(x==y);
    case _different: return real_t(x!=y);
    case _greater: return real_t(x>y);
    case _greaterequal: return real_t(x>=y);
    case _less: return real_t(x<y);
    case _lessequal: return real_t(x<=y);
    case _and: if(x>0 && y>0) return 1.; else return 0.;
    case _or: if(x>0 || y>0) return 1.; else return 0.;
    default: break;
  }
  error("symbolic_op_not_handled");
  return 0.;
}

inline complex_t evalOp(SymbolicOperation op, const complex_t& x, const complex_t& y)
{
  switch (op)
  {
    case _plus: return x+y;
    case _minus: return x-y;
    case _multiply: return x*y;
    case _divide: return x/y;
    case _power: return std::pow(x,y);
    case _equal: return real_t(x==y);
    case _different: return real_t(x!=y);
    case _greater: return real_t(x.real()>y.real());
    case _greaterequal: return real_t(x.real()>=y.real());
    case _less: return  real_t(x.real()<y.real());
    case _lessequal: return real_t(x.real()<=y.real());
    case _and: if(x.real()>0 && y.real()>0) return 1.; else return 0.;
    case _or: if(x.real()>0 || y.real()>0) return 1.; else return 0.;
    default: break;
  }
  error("symbolic_op_not_handled");
  return 0.;
}

//unary real function
inline real_t evalFun(SymbolicOperation op, const real_t& x, const real_t& p=0.)
{
  switch (op)
  {
    case _abs: return std::abs(x);
    case _sign: return (x>0)-(x<0);
    case _realPart:
    case _conj:return x;
    case _imagPart: return 0.;
    case _sqrt: return std::sqrt(x);
    case _squared: return x*x;
    case _sin: return std::sin(x);
    case _cos: return std::cos(x);
    case _tan: return std::tan(x);
    case _asin: return std::asin(x);
    case _acos: return std::acos(x);
    case _atan: return std::atan(x);
    case _sinh: return std::sinh(x);
    case _cosh: return std::cosh(x);
    case _tanh: return std::tanh(x);
    #if __cplusplus > 199711L
    case _asinh:return std::asinh(x);
    case _acosh:return std::acosh(x);
    case _atanh:return std::atanh(x);
    #endif
    case _exp: return std::exp(x);
    case _log: return std::log(x);
    case _log10: return std::log10(x);
    case _pow: return std::pow(x,p); break;
    case _not: if(x>0) return 0.; else return 1.;

    default: break;
  }
  error("symbolic_op_not_handled");
  return 0.;
}

inline complex_t evalFun(SymbolicOperation op, const complex_t& z, const complex_t& p=0.)
{
  switch (op)
  {
    case _abs: return std::abs(z);
    case _sign: if(std::imag(z)==0.) return (std::real(z)>0)-(std::real(z)<0);
                else error("free_error", "SymbolicFunction evaluation, try to get the sign of a complex number");
    case _realPart: return z.real();
    case _imagPart: return z.imag();
    case _conj:return std::conj(z);
    case _sqrt: return std::sqrt(z);
    case _squared: return z*z;
    case _sin: return std::sin(z);
    case _cos: return std::cos(z);
    case _tan: return std::tan(z);
    case _sinh: return std::sinh(z);
    case _cosh: return std::cosh(z);
    case _tanh: return std::tanh(z);
    #if __cplusplus > 199711L
    case _asin: return std::asin(z);
    case _acos: return std::acos(z);
    case _atan: return std::atan(z);
    case _asinh:return std::asinh(z);
    case _acosh:return std::acosh(z);
    case _atanh:return std::atanh(z);
    #endif
    case _exp: return std::exp(z);
    case _log: return std::log(z);
    case _log10: return std::log10(z);
    case _not: if(z.real()>0) return 0.; else return 1.;
    case _pow:
    default: break;
  }
  error("symbolic_op_not_handled");
  return 0.;
}
//evaluate function
inline real_t SymbolicFunction::operator()(const std::vector<real_t>& xs) const
{
  if (op>_idop && op<_abs) return coef.real()*evalOp(op,(*fn1)(xs),(*fn2)(xs)); //binary operator, fn1 or fn2 must be not 0
  if (var!=_varUndef)
  {
    real_t x=0.; size_t d=xs.size();
    switch(var)
    { case _r : x=norm(xs); break;
      case _theta :
      {
        if(d>1) x=std::atan2(xs[1],xs[0]);
      }
      break;
      case _phi :
      {
        x=norm(xs);
        if(d>2 && x>theEpsilon) x=std::asin(xs[2]/x); else x=0.;
      }
      default:
      {
        size_t n=int(var)-1;
        if (n<xs.size()) x=xs[n];
        else warning("free_warning","in SymbolicFunction::operator(), the variable _x"+varName(var)+" is not set, assuming 0!");
      }
    }
    if (op==_idop) return coef.real()*x;
    return coef.real()*evalFun(op,x, par.real());
  }
  if(fn1!=nullptr) return coef.real()* evalFun(op,(*fn1)(xs), par.real());
  return coef.real();  //case of a constant
}

inline complex_t SymbolicFunction::operator()(const std::vector<complex_t>& xs) const
{
  if (op>_idop && op<_abs) return coef*evalOp(op,(*fn1)(xs),(*fn2)(xs)); //binary operator, fn1 and fn2 are not 0
  if (var!=_varUndef)
  {
    size_t n=int(var)-1;
    complex_t x=0.;
    if (n<xs.size()) x=xs[n];
    else warning("free_warning","in SymbolicFunction::operator(), the variable _x"+varName(var)+" is not set, assuming 0!");
    if (op==_idop) return coef*x;
    return coef*evalFun(op,x, par);
  }
  if(fn1!=nullptr) return coef*evalFun(op,(*fn1)(xs), par);
  return coef;  //case of a constant
}

SymbolicFunction& derivative(const SymbolicFunction& f, VariableName v); // symbolic derivative of a symbolic function
SymbolicFunction& derivative(const SymbolicFunction& f, const SymbolicFunction& v); // symbolic derivative of a symbolic function

} // end of namespace xlifepp

#endif // SYMBOLICFUNCTION_HPP_INCLUDED
