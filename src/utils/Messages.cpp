/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Messages.cpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 25 jun 2003
  \date 3 may 2012

  \brief Implementation of xlifepp::Messages class members and related functions
 */

#include "Messages.hpp"
#include "Trace.hpp"
#include "Environment.hpp"
#include <iostream>
#include <sstream>

namespace xlifepp
{

std::ostream& operator<<(std::ostream& out, const MsgType& mt)
{ out << words("message", mt); return out; }

//--------------------------------------------------------------------------------
// Constructors, destructor
//--------------------------------------------------------------------------------

//! default constructor
Messages::Messages()
  : msgType_("XLiFE++"), msgStream_p(nullptr), msgFile_(""), traceWhere(false) {}

//! Constructor from file
Messages::Messages(const string_t& file, std::ofstream& out, const string_t& msgFile, const string_t& typeMsg)
  : msgType_(typeMsg), msgStream_p(&out), msgFile_(msgFile)
{
  this->loadFormat(file);
  traceWhere = true;
}

//! destructor
Messages::~Messages()
{
  while ( stringIndex_.size() > 0 )
  {
    delete stringIndex_.begin()->second;
    stringIndex_.erase(stringIndex_.begin());
  }
}

/*!
  reads message formats from file
  format: # int_id string_id string %x string %x %y ...
  create an MsgFormat object for each item read
 */
void Messages::loadFormat(const string_t& file)
{
  // open the file file_msg and connect to an ifstream
  std::ifstream ifs(file.c_str());

  // error: unable to open file for input
  if (!ifs)
  {
    // neither Message nor Error have been set up here so output an explicit error message here
    string_t stars(76, '-');

    if ( theEnvironment_p->language() == _fr )
    {
      std::cout << ";-( " << stars << "\n"
                << ";-( " << trace_p->current() << " Erreur: Impossible d'ouvrir le fichier " << file << "\n"
                << ";-( " << stars << "\n" << "\n";
    }
    else
    {
      std::cout << ";-( " << stars << "\n"
                << ";-( " << trace_p->current() << " Error: Unable to open input file " << file << "\n"
                << ";-( " << stars << "\n" << "\n";
    }

    exit(1);
  }

  // read file
  string_t format(""), word(""), ids(""), type(""), status(""), consol("");
  MsgType m = _info;
  bool stop(false), outOnConsol(false);
  // main loop on the stream
  string_t line("");
  getline(ifs, line , '@');

  while (!ifs.eof())
  {
    ifs >> ids;
    // a comment, jump over
    if (ids[0] == '/')
    {
      getline(ifs, format, '@');
    }
    else
    {
      ifs >> type >> status >> consol;
      getline(ifs, format, '@');

      if (format[0] == '\n')
      {
        format.erase(0, 1); // erase first eol
      }

      format.erase(format.length() - 1, 1); // erase last eol

      if (uppercase(type) == "ERROR" )
      {
        m = _error;
      }

      if (uppercase(type) == "WARNING" )
      {
        m = _warning;
      }

      if (uppercase(type) == "INFO" )
      {
        m = _info;
      }

      stop = (uppercase(status) == "STOP");
      outOnConsol = (uppercase(consol) == "STDOUT");
      // create new format and index it
      if (find(ids) == 0) {
        stringIndex_.insert(std::pair<string_t,MsgFormat*>(ids,new MsgFormat(trim(format), ids, m, stop, outOnConsol)));
      }
    }
  }

  ifs.close();
}

//! append a new message format in list
void Messages::append(MsgFormat& msgFmt)
{
  // if msgFmt.string already exists, forget msg
  if ( find(msgFmt.stringId()) == 0 )
  {
    stringIndex_.insert(std::pair<string_t,MsgFormat*>(msgFmt.stringId(),&msgFmt)); // append to string index
  }
}

//! append new message formats in list from file
void Messages::appendFromFile(const string_t& filename)
{
  this->loadFormat(filename);
}

//! find format by stringId (log complexity)
MsgFormat* Messages::find(const string_t& s)
{
  std::map<string_t, MsgFormat*>::iterator it = stringIndex_.find(s);

  if ( it == stringIndex_.end() )
  {
    return nullptr;
  }

  return it->second;
}

//! print on the ofstream out the list of all formats of message
void Messages::printList(std::ofstream& out)
{
  // language choice
  string_t mes = "LIST of " + msgType_ + " type MESSAGES";

  if ( theEnvironment_p->language() == _fr )
  {
    mes = "LISTE des MESSAGES de TYPE ";
  }

  out << mes << "\n";

  std::map <string_t, MsgFormat*>::iterator it;

  for (it = stringIndex_.begin(); it != stringIndex_.end(); ++it)
  {
    out << it->second->stringId() << ": " << it->second->format() << "\n";
  }

  mes = "END of LIST " + string_t(67, '-');

  if ( theEnvironment_p->language() == _fr )
  {
    mes = "FIN de LISTE " + string_t(66, '-');
  }

  out << mes << "\n" << "\n";
}

//--------------------------------------------------------------------------------
// External functions
//--------------------------------------------------------------------------------

/*!
  main function to throw an error/warning/info message where
  \param msgIds: the string id of a message in this collection
  \param msgData: a data object containing the message parameters
  \param msgSrc: pointer to an error message formats collection

  exemple: to throw the internal error with an int msgId and a string s paramaters
      data << s;
      msg("msg_undef", data, errInternal,_error);

  NOTE that MsgData structure is cleared after call.
  data and errInternal,_error have default values theMessageData and theMessages_p
*/
string_t message(const string_t& msgIds, MsgData& msgData, Messages* msgSrc)
{
  // string_t stream ro write in a string
  std::stringstream ss;
  // locate message with msgId
  MsgFormat* msgFmt = msgSrc->find(msgIds);

  // integrity test
  if ( msgFmt == nullptr )
  {
    // specify out stream
    std::ofstream& out = *(msgSrc->msgStream());
    MsgData edt;
    edt << msgIds << msgSrc->msgType();
    error("msg_undef", edt, msgSrc); // throw an errorundef internal error
    out.close(); // close outpout stream
    exit(1);
  }

  // message type is not checked, analyze the message format
  string_t t;
  int p = 0, q1 = msgFmt->format().find("%", p);
  int q = q1, cI = 0, cB = 0, cR = 0, cS = 0, cC = 0;

  while ( q != -1 )
  {
    ss << msgFmt->format().substr(p, q - p);
    t = msgFmt->format()[q + 1];

    if ( t == "i" )
    {
      ss << msgData.intParameter(cI);
      cI++;
    }

    if ( t == "r" )
    {
      ss << msgData.realParameter(cR);
      cR++;
    }

    if ( t == "s" )
    {
      ss << msgData.stringParameter(cS);
      cS++;
    }

    if ( t == "b" )
    {
      ss << msgData.booleanParameter(cB);
      cB++;
    }

    if ( t == "c" )
    {
      ss << msgData.complexParameter(cC);
      cC++;
    }

    p = q + 2;
    q = msgFmt->format().find("%", p);
  }

  // end of format
  int l = msgFmt->format().length();

  if ( p < l )
  {
    ss << msgFmt->format().substr(p, l - p);
  }

  // declare error data read (reset msgData)
  msgData.readData();

  return ss.str();
}

//! general message handler
void msg(const string_t& msgIds, MsgData& msgData, Messages* msgSrc, MsgType msgType = _info)
{
  if(currentThread()>0) return;  //to avoid multiple message in multithreading and possibly data race

  //null pointer checking
  if (msgSrc==nullptr)
  {
      std::cout<<"error in msg function: msgSrc pointer is null, probably environment has not been initialized"<<std::endl;
      abort();
  }

  // locate internal number of message msgId
  MsgFormat* msgFmt = msgSrc->find(msgIds);

  // integrity test
  if ( msgFmt == nullptr )
  {
    // specify out stream
    std::ofstream& out = *(msgSrc->msgStream());
    MsgData edt;
    edt << msgIds << msgSrc->msgType();
    error("msg_undef", edt, msgSrc); // throw an errorundef internal error
    out.close(); // close outpout stream
    abort();
  }

  // check the integrity of the message type
  if (theVerboseLevel>50)
  {
    if (msgType != msgFmt->type())
    {
      MsgData edt;
      edt << words("message",msgType) << words("message",msgFmt->type());
      warning("msg_wrong_type", edt, msgSrc);
    }
  }

  // build the location of the message
  if (theWhereData == "" && msgType != _info)
  {
    theWhereData = trace_p->list() + " ...\n";
		msgSrc->traceWhere=true;
  }

  if (msgType == _warning)
  {
    MsgData edt;
    edt << msgSrc->msgType() << msgFmt->stringId();
    info("on_warning", edt, msgSrc);
    msgSrc->traceWhere = false;
  }

  if (msgType == _error)
  {
    MsgData edt;
    edt << msgSrc->msgType() << msgFmt->stringId() << trace_p->list();
    info("on_error", edt, msgSrc);
    msgSrc->traceWhere = false;
  }

  // build the message
  string_t mes = message(msgIds, msgData, msgSrc);

  // for an info message, we do not print whereData if empty
  if (theWhereData == "" && msgSrc->traceWhere && msgType == _info)
  {
    msgSrc->traceWhere = false;
  }

  // print on console (if wanted) and on file
  if (msgFmt->console())
  {
    if (msgSrc->traceWhere)
    {
      if (theVerboseLevel > 0) std::cout << theWhereData;
    }

    if (theVerboseLevel > 0) std::cout << mes << "\n" << std::flush; // print on console
  }

  std::ofstream& out = *(msgSrc->msgStream()); // print on printfile

  if (msgSrc->traceWhere)
  {
    out << theWhereData;
  }

  out << mes << "\n" << std::flush;

  theWhereData = "";

  // message after an error
  if (msgType == _error)
  {
    MsgData edt;
    info("on_enderror", edt, msgSrc);
  }

  msgSrc->traceWhere = true;

  // on a stop error, close output stream and exit
  if (msgType == _error)
  {
    out.close();
    abort();
  }
}

//! shortcut of msg for info type messages
void info(const string_t& msgIds, MsgData& msgData, Messages* msgSrc)
{
  msg(msgIds, msgData, msgSrc, _info);
}

//! shortcut of msg for error type messages
void error(const string_t& msgIds, MsgData& msgData, Messages* msgSrc)
{
  msg(msgIds, msgData, msgSrc, _error);
}

//! shortcut of msg for warning type messages
void warning(const string_t& msgIds, MsgData& msgData, Messages* msgSrc)
{
  msg(msgIds, msgData, msgSrc, _warning);
}

} // end of namespace xlifepp
