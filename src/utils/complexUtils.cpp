/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file complexUtils.cpp
  \author E. Lunéville
  \since 14 dec 2011
  \date 3 may 2012

  \brief inverse trigonometric functions asin, acos, atan, asinh, acosh, atanh
 */

#include "complexUtils.hpp"

namespace xlifepp
{

// definition of asin, acos, atan, asinh, acosh and atanh for complex value
// equivalent formula
complex_t asin(const complex_t& z)
{
  #if __cplusplus > 199711L
  return std::asin(z);
  #else
  return -i_ * asinh(i_*z);
  #endif
}

complex_t acos(const complex_t& z)
{
  #if __cplusplus > 199711L
  return std::acos(z);
  #else
  return 2.*atan(1.) - asin(z);
  #endif
}

complex_t atan(const complex_t& z)
{
  #if __cplusplus > 199711L
  return std::atan(z);
  #else
  return - i_ * atanh(i_*z);
  #endif
}

complex_t asinh(const complex_t& z)
{
  #if __cplusplus > 199711L
  return std::asinh(z);
  #else
  if (std::abs(z) > 1e6)
  {
    // asinh(z) ~ \frac{z}{2 \sqrt{z^2}} * \left( \log{4z^2} - \sum_k=1^{\infty} \frac{(-1)^k Pochammer{1/2,k} }{k k! z^{2k}} \right)
    // only k=1,2 terms are enough (maybe just one is really enough)
    // http://functions.wolfram.com/ElementaryFunctions/ArcSinh/06/01/06/01/
    return z/(2.*std::abs(z))*(std::log(4.*z*z) + 1./(2.*z*z)+ 3./(16.*z*z*z*z));
  }
  else if (std::abs(z) < theTolerance)
  {
    // asinh(z) ~ \sum_k=0^{\infty} \frac{(-1)^k Pochammer{1/2,k} z^{2k+1}}{(2k+1) k!}
    // only k=0,1 terms are enough (maybe just one is really enough)
    // http://functions.wolfram.com/ElementaryFunctions/ArcSinh/06/01/03/01/
    return z - z*z*z/6.-z*z*z*z*z/40.;
  }
  else
  {
    return log(z + sqrt(z*z + 1.));
  }
  #endif
}

complex_t acosh(const complex_t& z)
{
  #if __cplusplus > 199711L
  return std::acosh(z);
  #else
  if (std::abs(z) > 1e6)
  {
    // acosh(z) ~ \log{2z} - \sum_k=1^{\infty} \frac{Pochammer{1/2,k} }{2k k! z^{2k}}
    // only k=1,2 terms are enough (maybe just one is really enough)
    // http://functions.wolfram.com/ElementaryFunctions/ArcCosh/06/01/06/01/
    return std::log(2.*z)-1./(4.*z*z)-1./(32.*z*z*z*z);
  }
  else if (std::abs(z) < theTolerance)
  {
    if (z.imag() > 0.) {
      // acosh(z) ~ i ( pi/2 -\sum_k=0^{\infty} \frac{Pochammer{1/2,k} z^{2k+1}}{(2k+1) k!})
      // only k=0,1,2 terms are enough (maybe just two are really enough)
      // http://functions.wolfram.com/ElementaryFunctions/ArcCosh/06/01/03/01/01/
      return i_*(pi_/2.-z-z*z*z/6.-3.*z*z*z*z*z/40.);
    }
    else
    {
      // acosh(z) ~ -i ( pi/2 -\sum_k=0^{\infty} \frac{Pochammer{1/2,k} z^{2k+1}}{(2k+1) k!})
      // only k=0,1,2 terms are enough (maybe just two are really enough)
      // http://functions.wolfram.com/ElementaryFunctions/ArcCosh/06/01/03/01/02/
      return -i_*(pi_/2.-z-z*z*z/6.-3.*z*z*z*z*z/40.);
    }

  }
  else if (std::abs(z-1.) < theTolerance)
  {
    // acosh(z) ~ \sqrt{2(z-1)}*\sum_k=0^{\infty} \frac{Pochammer{1/2,k} (1-z)^k}{2^k (2k+1) k!}
    // only k=0,1,2 terms are enough
    // http://functions.wolfram.com/ElementaryFunctions/ArcCosh/06/01/04/01/
    return std::sqrt(2.*(z-1.))*(1.-(z-1.)/12.+3.*(z-1.)*(z-1.)/160.);
  }
  else if (std::abs(z+1) < theTolerance)
  {
    if (z.imag() > 0.)
    {
      // acosh(z) ~ i (Pi - \sqrt{2(z+1)}*\sum_k=0^{\infty} \frac{Pochammer{1/2,k} (1+z)^k}{2^k (2k+1) k!})
      // only k=0,1 terms are enough (maybe just one is really enough)
      // http://functions.wolfram.com/ElementaryFunctions/ArcCosh/06/01/05/01/01/
      return i_*(pi_-std::sqrt(2.*(z+1.))*(1.-(z+1.)/12.+3.*(z+1.)*(z+1.)/160.));
    }
    else
    {
      // acosh(z) ~ -i (Pi - \sqrt{2(z+1)}*\sum_k=0^{\infty} \frac{Pochammer{1/2,k} (1+z)^k}{2^k (2k+1) k!})
      // only k=0,1 terms are enough (maybe just one is really enough)
      // http://functions.wolfram.com/ElementaryFunctions/ArcCosh/06/01/05/01/02/
      return -i_*(pi_-std::sqrt(2.*(z+1.))*(1.-(z+1.)/12.+3.*(z+1.)*(z+1.)/160.));
    }
  }
  else
  {
    return log(z + sqrt(z - 1.) * sqrt(z + 1.));
  }
  #endif // __cplusplus
}

complex_t atanh(const complex_t& z)
{
  #if __cplusplus > 199711L
  return std::atanh(z);
  #else
  if (std::abs(z) > 1e6)
  {
    // atanh(z) ~ \frac{Pi z}{2 \sqrt{-z^2}} + \sum_k=0^{\infty} \frac{1}{(2k+1) z^{2k+1}}
    // only k=0,1 terms are enough (maybe just one is really enough)
    // http://functions.wolfram.com/ElementaryFunctions/ArcTanh/06/01/06/01/
    return pi_*z/(2.*std::sqrt(-z*z))+1./z+1./(3.*z*z*z);
  }
  else if (std::abs(z) < theTolerance)
  {
    // atanh(z) ~ \sum_k=0^{\infty} \frac{z^{2k+1}}{2k+1}
    // only k=1,2 terms are enough (maybe just one is really enough)
    // http://functions.wolfram.com/ElementaryFunctions/ArcTanh/06/01/03/01/
    return (z+z*z*z/3.);
  }
  else if (std::abs(z-1.) < theTolerance)
  {
    // atanh(z) ~ 0.5*(\log{2}-\log{1-z}-\sum_k=1^{\infty} \frac{0.5^k (1-z)^k}{k})
    // only k=1,2 terms are enough (maybe just one is really enough)
    // http://functions.wolfram.com/ElementaryFunctions/ArcTanh/06/01/04/01/
    return (std::log(2.)-std::log(1.-z)-(1.-z)/2.-(z-1.)*(z-1.)/8.)/2.;
  }
  else if (std::abs(z+1) < theTolerance)
  {
    // atanh(z) ~ 0.5*(-\log{2}+\log{1+z}+\sum_k=1^{\infty} \frac{0.5^k (1+z)^k}{k})
    // only k=1,2 terms are enough (maybe just one is really enough)
    // http://functions.wolfram.com/ElementaryFunctions/ArcTanh/06/01/05/01/
    return (-std::log(2.)+std::log(1.+z)+(1.+z)/2.+(z+1.)*(z+1.)/8.)/2.;
  }
  else
  {
    return (log(1. + z) - log(1. - z)) / 2.;
  }
  #endif
}

complex_t cbrt(const complex_t& z)
{
  real_t m=std::abs(z);
  real_t theta=std::arg(z);
  return std::polar(cbrt(m),theta/3.);
}

real_t asinh(const real_t& r)
{
  return real(asinh(complex_t(r, 0)));
}

real_t acosh(const real_t& r)
{
  return real(acosh(complex_t(r, 0)));
}

real_t atanh(const real_t& r)
{
  return real(atanh(complex_t(r, 0)));
}

real_t cbrt(const real_t& r)
{
  //return std::cbrt(r);   //only in C++2011
  return pow(r,1./3.);
}

// algebraic operators
complex_t operator+(const int_t i, const complex_t& z)
{
  return z + real_t(i);
}
complex_t operator+(const int i, const complex_t& z)
{
  return z + real_t(i);
}
complex_t operator+(const number_t n, const complex_t& z)
{
  return z + real_t(n);
}
complex_t operator+(const complex_t& z, const int_t i)
{
  return z + real_t(i);
}
complex_t operator+(const complex_t& z, const int i)
{
  return z + real_t(i);
}
complex_t operator+(const complex_t& z, const number_t n)
{
  return z + real_t(n);
}
// complex_t operator+=(const int);
complex_t operator-(const int_t i, const complex_t& z)
{
  return real_t(i) - z;
}
complex_t operator-(const int i, const complex_t& z)
{
  return real_t(i) - z;
}
complex_t operator-(const number_t n, const complex_t& z)
{
  return real_t(n) - z;
}
complex_t operator-(const complex_t& z, const int_t i)
{
  return z - real_t(i);
}
complex_t operator-(const complex_t& z, const int i)
{
  return z - real_t(i);
}
complex_t operator-(const complex_t& z, const number_t n)
{
  return z - real_t(n);
}
// complex_t operator-=(const int);
complex_t operator*(const int_t i, const complex_t& z)
{
  return real_t(i) * z;
}
complex_t operator*(const int i, const complex_t& z)
{
  return real_t(i) * z;
}
complex_t operator*(const number_t n, const complex_t& z)
{
  return real_t(n) * z;
}
complex_t operator*(const complex_t& z, const int_t i)
{
  return real_t(i) * z;
}
complex_t operator*(const complex_t& z, const int i)
{
  return real_t(i) * z;
}
complex_t operator*(const complex_t& z, const number_t n)
{
  return real_t(n) * z;
}
// complex_t operator*=(const int);
// complex_t operator/(const int, const complex_t&);
complex_t operator/(const complex_t& z, const int_t i)
{
  return z / real_t(i);
}
complex_t operator/(const complex_t& z, const int i)
{
  return z / real_t(i);
}
complex_t operator/(const complex_t& z, const number_t n)
{
  return z / real_t(n);
}
// complex_t operator/=(const int);


} // end of namespace xlifepp
