/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
   \file Timer.cpp
   \authors D. Martin, N. Kielbasiewicz, E. Lunéville
   \since 24 nov 2002
   \date 3 may 2012

   \brief Implementation of xlifepp::Timer class members and related functions
 */

#include "Timer.hpp"
#include "Environment.hpp"
#include "String.hpp"
#include "PrintStream.hpp"

#include <iostream>
#include <ctime>

#ifdef OS_IS_UNIX
  #include <sys/resource.h>
  #include <sys/time.h>
#endif

#ifdef OS_IS_WIN
  #include <sys/timeb.h>
  //#include <windef.h>
  //#include <stdio.h>
  //#include <cstdarg>
  //#include <winbase.h>
  #include <windows.h>
#endif

namespace xlifepp
{

//--------------------------------------------------------------------------------
//   Time functions
//--------------------------------------------------------------------------------
void Timer::getCpuTime() //! returns user time ("cputime") interval since beginning of process
{
  #ifdef OS_IS_UNIX
    struct rusage tt;
    getrusage(RUSAGE_SELF, &tt);
    usrSec_ = tt.ru_utime.tv_sec;
    usrMicroSec_ = tt.ru_utime.tv_usec;
    sysSec_ = tt.ru_stime.tv_sec;
    sysMicroSec_ = tt.ru_stime.tv_usec;
  #endif

  #ifdef OS_IS_WIN
    HANDLE h = GetCurrentProcess();
    FILETIME creationTime, exitTime, kernelTime, userTime;
    SYSTEMTIME st;

    if(GetProcessTimes(h, &creationTime, &exitTime, &kernelTime, &userTime) != 0)
      {
        FileTimeToSystemTime(&userTime, &st);
        // LPSYSTEMTIME does not deal with microsecond, only with milliseconds
        // LPFILETIME is the number of 100-nanosecond intervals, should be used to compute microseconds
        // do not take into account days, available for execution time less than 24 hours
        usrSec_ = st.wHour * 3600 + st.wMinute * 60 + st.wSecond;
        usrMicroSec_ = st.wMilliseconds * 1000;
      }

    struct _timeb tb;

    _ftime(&tb);

    if(theStartTime_p != nullptr)  // is null when initialize the StartTime
      {
        sysSec_ = number_t(tb.time - theStartTime_p->sec_);
        sysMicroSec_ = number_t(tb.millitm * 1000 - theStartTime_p->microSec_);
      }
    else
      {
        sysSec_ = 0;
        sysMicroSec_ = 0;
      }

  #endif
}

real_t Timer::deltaCpuTime(Timer* ot) const //! returns elapsed user time ("cputime") interval since "ot" time in seconds
{
  //return 1.e-2 * int(1.e+2 * (usrSec_ - ot->usrSec_) + 1.e-4 * (usrMicroSec_ - ot->usrMicroSec_));
  return ((1.e+6 * static_cast<real_t>(usrSec_ - ot->usrSec_) +
           (static_cast<real_t>(usrMicroSec_) - static_cast<real_t>(ot->usrMicroSec_)))
          *1.e-6);
}

void Timer::getTime()//! returns time in seconds and microseconds since 01/01/1970 00:00:00
{
  #ifdef OS_IS_UNIX
    struct timeval tt;
    gettimeofday(&tt, (struct timezone*) 0) ;
    sec_  = tt.tv_sec;
    microSec_ = tt.tv_usec;
  #endif

  #ifdef OS_IS_WIN
    struct _timeb tb;
    _ftime(&tb);
    sec_ = tb.time;
    microSec_  = tb.millitm * 1000;
  #endif
}

real_t Timer::deltaTime(Timer* ot) const //! returns elapsed time interval since "ot" time, in seconds
{
  //return 1.e-2 * int(1.e+2 * (sysSec_ - ot->sysSec_) + 1.e-4 * (sysMicroSec_ - ot->sysMicroSec_));
  return ((1.e+6 * static_cast<real_t>(sec_ -  ot->sec_) +
           (static_cast<real_t>(microSec_) - static_cast<real_t>(ot->microSec_)))
          *1.e-6);
}

// print Timer object
void Timer::print(std::ostream& out) const
{
  out << "time since 01/01/1970 00:00:00 in seconds =  "<< sec_<<  ",  in microseconds = " << microSec_;
  out << ", cpu time since beginning in seconds=  "<< usrSec_<<", in microseconds = "<<usrMicroSec_<<eol;
}

void Timer::print(PrintStream& os) const {print(os.currentStream());}

//--------------------------------------------------------------------------------
//   Extern current time and date functions
//--------------------------------------------------------------------------------
string_t theTime() //! returns current time
{
  dimen_t h = theLastTime_p->hour();
  string_t hh = tostring(h);

  if(h < 10)
    {
      hh = "0" + hh;
    }

  dimen_t m = theLastTime_p->minutes();
  string_t mn = tostring(m);

  if(m < 10)
    {
      mn = "0" + mn;
    }

  return hh + "h" + mn;
}

string_t theIsoTime() //! returns ISO8601 format of current date (hh-mi-ss)
{
  dimen_t h = theLastTime_p->hour();
  string_t hh = tostring(h);

  if(h < 10)
    {
      hh = "0" + hh;
    }

  dimen_t m = theLastTime_p->minutes();
  string_t mn = tostring(m);

  if(m < 10)
    {
      mn = "0" + mn;
    }

  dimen_t s = theLastTime_p->seconds();
  string_t ss = tostring(s);

  if(s < 10)
    {
      ss = "0" + ss;
    }

  return hh + "-" + mn + "-" + ss;
}

string_t theDate() //! returns current date as dd.mmm.yyyy
{
  return tostring(theLastTime_p->day()) + "." + words("short months", theLastTime_p->month()) + "." + tostring(theLastTime_p->year());
}

string_t theIsoDate() //! returns ISO8601 format of current date (yyyy-mm-dd)
{
  dimen_t d = theLastTime_p->day(), m = theLastTime_p->month();
  string_t dd = tostring(d);
  string_t mm = tostring(m);

  if(m < 10)
    {
      mm = "0" + mm;
    }

  if(d < 10)
    {
      dd = "0" + dd;
    }

  return tostring(theLastTime_p->year()) + "-" + mm + "-" + dd;
}


string_t theShortDate() //! returns current date as mm/dd/yyyy (en) or dd/mm/yyyy (fr)
{
  string_t dat;
  string_t dd = tostring(theLastTime_p->day());

  if(theLastTime_p->day() < 10)
    {
      dd = "0" + dd;
    }

  string_t mm = tostring(theLastTime_p->month());

  if(theLastTime_p->month() < 10)
    {
      mm = "0" + mm;
    }

  switch(theEnvironment_p->language())
    {
      case _fr:
        dat = dd + "/" + mm + "/" + tostring(theLastTime_p->year());
        break;

      case _en:
      default:
        dat = mm + "/" + dd + "/" + tostring(theLastTime_p->year());
        break;
    }

  return dat;
}

string_t theLongDate() //! returns current date as Month Day, Year (en) or Day Month Year (fr)
{
  string_t dat;

  switch(theEnvironment_p->language())
    {
      case _fr:
        dat = tostring(theLastTime_p->day()) + " " + words("months", theLastTime_p->month()) + " " + tostring(theLastTime_p->year());
        break;

      case _en:
      default:
        dat = words("months", theLastTime_p->month()) + " " + tostring(theLastTime_p->day()) + ", " + tostring(theLastTime_p->year());
        break;
    }

  return dat;
}

//--------------------------------------------------------------------------------
//   Extern cpu time functions
//--------------------------------------------------------------------------------

//@{
/*!
  returns user time ("cputime") interval since last runtime 'call'
  according to unit defined in Time::deltaCpuTime
 */
real_t cpuTime()
{
  Timer current;
  current.update();
  real_t res = current.deltaCpuTime(theLastTime_p);
  *theLastTime_p = current;
  return res;
}

real_t cpuTime(const string_t& comment, std::ostream & out)
{
  Timer current;
  current.update();
  real_t res = current.deltaCpuTime(theLastTime_p);
  *theLastTime_p = current;
  out << "cpu time -> " << comment << ": " << res << "s." << std::endl;
  return res;
}
//@}

//@{
/*!
  returns elapsed time interval since last runtime 'call'
  according to unit defined in Time::deltaTime
 */
real_t elapsedTime()
{
  Timer current;
  current.update();
  real_t res = current.deltaTime(theLastTime_p);
  *theLastTime_p = current;
  return res;
}

real_t elapsedTime(const string_t& comment, std::ostream & out)
{
  Timer current;
  current.update();
  real_t res = current.deltaTime(theLastTime_p);
  *theLastTime_p = current;
  out << "elapsed time -> " << comment << ": " << res << "s." << std::endl;
  return res;
}
//@}

//@{
/*!
  returns user time ("cputime") interval since first runtime 'call'
  according to unit defined in Time::deltacpuTime
 */
real_t totalCpuTime()
{
  Timer current;
  current.update();
  real_t res = current.deltaCpuTime(theStartTime_p);
  return res;
}

real_t totalCpuTime(const string_t& comment,  std::ostream & out)
{
  Timer current;
  current.update();
  real_t res = current.deltaCpuTime(theStartTime_p);
  out << "total cputime -> " << comment << ": " << res << "s." << std::endl;
  return res;
}
//@}

//@{
/*!
  returns elapsed time interval since first runtime 'call'
  according to unit defined in Time::deltaTime
 */
real_t totalElapsedTime()
{
  Timer current;
  current.update();
  real_t res = current.deltaTime(theStartTime_p);
  return res;
}

real_t totalElapsedTime(const string_t& comment,  std::ostream & out)
{
  Timer current;
  current.update();
  real_t res = current.deltaTime(theStartTime_p);
  out << "total elapsed time -> " << comment << ": " << res << "s." << std::endl;
  return res;
}
//@}

real_t cpuTime(const string_t& comment, PrintStream& out)
{return cpuTime(comment,out.currentStream());}

real_t totalCpuTime(const string_t& comment, PrintStream& out)
{return totalCpuTime(comment,out.currentStream());}

real_t elapsedTime(const string_t& comment, PrintStream& out)
{return elapsedTime(comment,out.currentStream());}

real_t totalElapsedTime(const string_t& comment, PrintStream& out)
{return totalElapsedTime(comment,out.currentStream());}

real_t cpuTime(const string_t& comment, CoutStream& out)
{
  real_t res = cpuTime();
  out<<"cpu time -> " << comment << ": " << res << "s." << std::endl;
  return res;
}

real_t totalCpuTime(const string_t& comment, CoutStream& out)
{
  real_t res = totalCpuTime();
  out<<"total cpu time -> " << comment << ": " << res << "s." << std::endl;
  return res;
}

real_t elapsedTime(const string_t& comment, CoutStream& out)
{
  real_t res = elapsedTime();
  out<<"elapsed time -> " << comment << ": " << res << "s." << std::endl;
  return res;
}

real_t totalElapsedTime(const string_t& comment, CoutStream& out)
{
  real_t res = totalElapsedTime();
  out<<"total elapsed time -> " << comment << ": " << res << "s." << std::endl;
  return res;
}

} // end of namespace xlifeppp

