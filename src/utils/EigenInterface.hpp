/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file EigenInterface.hpp
  \author E. Lunéville
  \since 20 dec 2016
  \date  20 dec 2016

  \brief Interface to Eigen library

  This interface provides some tools to address high level Eigen functionalities (QR, EIGS, SVD, ...)

   row matrix passed by pointer:
     void qr(const T* A, number_t m, number_t n, T* Q, T* R)
     void eigs(const T* A, number_t m, complex_t* vs, complex_t* ls)
     void eigs(const T* A, const T* B, number_t m, complex_t* vs, complex_t* ls)
     void svd(T* A, number_t m, number_t n, T* U, T* D, T* V, number_t& rk, real_t eps=0.)
     void rsvd(T* A, number_t m, number_t n, number_t r, T* U, T* D, T* V)
     void rsvd(T* A, number_t m, number_t n, real_t eps, number_t& rk, T* U, T* D, T* V)
     void r3svd(T* A, number_t m, number_t n, real_t eps, number_t& rk, T* U, T* D, T* V,
                number_t t = 0, number_t p = 0, number_t q = 0, number_t maxit = 0)

   matrix passed as a M object requiring the member functions
          M::numberOfRows(), M::numberOfCols(), M::squaredNorm()
          M::multMatrixRow(T* M, T* R, number_t p) i.e M*matRow
          M::multLeftMatrixRow(T* M, T* R, number_t p) i.e matRow*M:
     void   svd(M& A, vector<T>& U, vector<T>& D, vector<T>& V, number_t& rk, real_t eps=0.)
     void  rsvd(M& A, number_t r, vector<T>& U, vector<T>& D, vector<T>& V)
     void  rsvd(M& A, real_t eps, number_t& rk, vector<T>& U, vector<T>& D, vector<T>& V)
     void r3svd(M& A, real_t eps, number_t& rk, vector<T>& U, vector<T>& D, vector<T>& V,
                number_t t = 0, number_t p = 0, number_t q = 0, number_t maxit = 0)

    ### as Eigen matrix product fails in gcc 32 bits with O3 because of a mis-alignment in vectorization
        there is a specific implementation in case of gcc win32 using a local "slower" eigen matrix product
*/

#ifndef EIGENINTERFACE_HPP_INCLUDED
#define EIGENINTERFACE_HPP_INCLUDED

#include "config.h"
#include "Environment.hpp"
#include "PrintStream.hpp"
#include "../mathsResources/randomGenerators.hpp"

#ifdef XLIFEPP_WITH_EIGEN
  #include "Dense"
#endif

namespace xlifepp
{

/*---------------------------------------------------------------------------------------------
                                           QR stuff
---------------------------------------------------------------------------------------------*/
/*! general template QR using Eigen, assuming A, Q, R  are pointers to first value of dense row matrix
    A: pointer to dense row matrix
    m,n: number of rows and cols of A
    Q, R: pointer to QR factors as dense row matrices, has to be allocated before
*/
template <typename T>
void qr(const T* A, number_t m, number_t n, T* Q, T* R)
{
  std::pair<ValueType, StrucType> st=typeOf(*A);
  string_t s= words("value",st.first)+" "+words("structure",st.second)+" matrix";
  error("scalar_only");
}

template <>
void qr(const real_t*, number_t, number_t, real_t*, real_t*);
template <>
void qr(const complex_t*, number_t, number_t, complex_t*, complex_t*);


//---------------------------------------------------------------------------------------------
//                                           Eigen stuff
//---------------------------------------------------------------------------------------------*/
/*! general template eigenvector computation using Eigen, assuming A, U are pointers to first value of DENSE ROW matrix
//    A, B: pointers to dense row squared matrix
// m: matrix size
// Xs: pointer to a dense row matrix, storing eigen vectors
// ls: pointer to a vector, storing eigen values
*/

//generalized eigen problem A*X=lambda*X
template <typename T>
void eigs(const T* A, number_t m, complex_t* Xs, complex_t* ls)
{
  std::pair<ValueType, StrucType> st=typeOf(*A);
  string_t s= words("value",st.first)+" "+words("structure",st.second)+" matrix";
  error("scalar_only");
}
template <>
void eigs(const real_t* A, number_t m, complex_t* Xs, complex_t* ls);     //real matrix
template <>
void eigs(const complex_t* A, number_t m, complex_t* Xs, complex_t* ls);  //complex matrix

//generalized eigen problem A*X=lambda*B*X only available for real matrices in Eigen!
template <typename Ta, typename Tb>
void eigs(const Ta* A, const Tb* B, number_t m, complex_t* Xs, complex_t* lambda)
{
  std::pair<ValueType, StrucType> stA = typeOf(*A), stB = typeOf(*B);
  string_t sA= words("value",stA.first)+" "+words("structure",stA.second)+" matrix";
  string_t sB= words("value",stB.first)+" "+words("structure",stB.second)+" matrix";
  error("scalar_only");
}
template <>
void eigs(const real_t* A, const real_t* B, number_t m, complex_t* Xs, complex_t* ls);   //real matrix

/*---------------------------------------------------------------------------------------------
                                           SVD stuff
---------------------------------------------------------------------------------------------*/
/*! general template svd using Eigen, assuming A, U, V are pointers to first value of DENSE ROW matrix
    A: pointer to dense row matrix
    m,n: number of rows and cols of A
    U, V: pointer to dense row matrices, has to be allocated before
    S: pointer to S vector, has to be allocated before
    rk: prescribed rank of truncature, if 0 not used, the real rank at output
    eps: prescribed precision, if rk > 0 not used
          r=0 and eps=0 gives the full svd

    note: may be used when A is a pointer to a m x n col dense matrix, by permuting arguments when calling:
                  svd(A,n,m,V,D,U,rk,eps) produces the SVD of A
           produces the SVD of A = U*D*V' where U, V are pointers to row dense matrices!
*/
template <typename T>
void svd(T* A, number_t m, number_t n, T* U, T* D, T* V, number_t& rk, real_t eps=0.)
{
  #ifdef XLIFEPP_WITH_EIGEN
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    Eigen::Map<EigenMatrix> eA(A, m, n);    // map Matrix pointer to Eigen matrix
    Eigen::JacobiSVD<EigenMatrix> svd(eA, Eigen::ComputeThinU | Eigen::ComputeThinV);
    number_t r=1, s=svd.singularValues().size();  //r=1 means that at least one singular values is kept even it is lower than eps
    if(rk>0) s=std::min(s,rk);
    //real_t sv=svd.singularValues()(r);
    real_t sv=*(svd.singularValues().data()+r);
    while(sv >= eps && r<s)  { r++; if(r<s) sv= *(svd.singularValues().data()+r);}
    rk=r;
    for(number_t i=0; i<m; ++i)
      for(number_t j=0; j<r; ++j, ++U) *U = svd.matrixU()(i,j);
    for(number_t i=0; i<n; ++i)
      for(number_t j=0; j<r; ++j, ++V) *V = svd.matrixV()(i,j);
    for(number_t i=0; i<r; ++i, ++D)   *D = *(svd.singularValues().data()+i);
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

/*---------------------------------------------------------------------------------------------
                                    random SVD stuff
---------------------------------------------------------------------------------------------*/
// as Eigen matrix product fails in gcc 32 bits with O3 because of a mis-alignment in vectorization
// there is a special implementation in case of gcc win32 using a local "slower" eigen matrix product
// see below

#if !defined( __MINGW32__) || !defined( COMPILER_IS_32_BITS)

/*! Random SVD compression method of a row dense matrix given by pointer
    T: type of the result
    A: pointer to the first element of the dense matrix to be compressed, stored as row major access (dense row)
    m, n: number of rows and cols of matrix to be compressed
    r: prescribed rank >0 of truncature
    U, V: pointer to dense row matrices, has to be allocated before
    S: pointer to S vector, has to be allocated before

    note: may be used when A is a pointer to a m x n col dense matrix, by permuting arguments when calling:
                  rsvd(A,n,m,rk,V,D,U)
           produces the RSVD of A = U*D*V' where U, V are pointers to row dense matrices!
*/
template<typename T>
void rsvd(T* A, number_t m, number_t n, number_t r, T* U, T* D, T* V)
{
  #ifdef XLIFEPP_WITH_EIGEN
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    Eigen::Map<EigenMatrix> eA(A, m, n);           // map Matrix pointer to Eigen matrix
    number_t p=2*r;
    EigenMatrix G(n,p);
    normalDistribution(G.data(),n,p);
    EigenMatrix Y=eA*G;                            //product Y = A * G where G is n x 2r Gaussian matrix
    Eigen::HouseholderQR<EigenMatrix> qr(Y);       //QR factorization
    EigenMatrix Q(EigenMatrix::Identity(m,p));
    Q = qr.householderQ() * Q;
    EigenMatrix qtA = Q.adjoint()*eA;             //product Qt*A
    Eigen::JacobiSVD<EigenMatrix> svd(qtA, Eigen::ComputeThinU | Eigen::ComputeThinV);
    EigenMatrix eU=Q*svd.matrixU();               //do SVD of eY
    //fill matrix U,V,D
    for(number_t i=0; i<m; ++i)
      for(number_t j=0; j<r; ++j, ++U) *U = eU(i,j);
    for(number_t i=0; i<n; ++i)
      for(number_t j=0; j<r; ++j, ++V) *V = svd.matrixV()(i,j);
    for(number_t i=0; i<r; ++i, ++D)   *D = *(svd.singularValues().data()+i);
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

/*! Random SVD compression method of a row dense matrix given by pointer
    from the paper: Stephanie Chaillat,  George Biros.  FaIMS: A fast algorithm for the inverse medium problem with multiple frequencies
                     and multiple sources for the scalar Helmholtz equation.  Journal of Computational Physics, Elsevier, 2012, 231 (12), pp.4403-4421.
    T: type of the result
    A: pointer to the first element of the dense matrix to be compressed, stored as row major access (dense row)
    m, n: number of rows and cols of matrix to be compressed
    eps: desired precision for the SVD matrix
    rk: rank of rsvd on output
    U, V: pointer to dense row matrices, has to be allocated before
    S: pointer to S vector, has to be allocated before

    note: may be used when A is a pointer to a m x n col dense matrix, by permuting arguments when calling:
                  rsvd(A,n,m,eps,V,D,U)
           produces the RSVD of A = U*D*V' where U, V are pointers to row dense matrices!
*/
template<typename T>
void rsvd(T* A, number_t m, number_t n, real_t eps, number_t& rk, T* U, T* D, T* V)
{
  #ifdef XLIFEPP_WITH_EIGEN
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    Eigen::Map<EigenMatrix> eA(A, m, n);
    number_t r=1;
    number_t t= std::max(m/10,number_t(2));  // number of sampling
    bool cont=true;
    number_t mn=std::min(m,n);
    EigenMatrix Q;
    while(cont)
      {
        number_t p=r+t;
        EigenMatrix G(n,p);
        normalDistribution(G.data(),n,p);
        EigenMatrix Y=eA*G;
        Eigen::JacobiSVD<EigenMatrix> svd(Y, Eigen::ComputeThinU | Eigen::ComputeThinV);
        Q=svd.matrixU().leftCols(r);
        EigenMatrix E=Y-Q*(Q.adjoint()*Y);
        if(E.norm()>eps*svd.singularValues()(0) && (r+t)<=mn) r+=t;
        else cont=false;
      }
    //fill matrix U,V,D
    Eigen::JacobiSVD<EigenMatrix> svd(Q.adjoint()*eA, Eigen::ComputeThinU | Eigen::ComputeThinV);
    EigenMatrix eU=Q*svd.matrixU();
    for(number_t i=0; i<m; ++i)
      for(number_t j=0; j<r; ++j, ++U) *U = eU(i,j);
    for(number_t i=0; i<n; ++i)
      for(number_t j=0; j<r; ++j, ++V) *V = svd.matrixV()(i,j);
    for(number_t i=0; i<r; ++i, ++D)   *D = *(svd.singularValues().data()+i);
    rk=r;
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

/*! improved Random SVD compression method of a row dense matrix given by pointer
    from https://arxiv.org/ftp/arxiv/papers/1605/1605.08134.pdf

    T: type of the result
    A: pointer to the first element of the dense matrix to be compressed, stored as row major access (dense row)
    m, n: number of rows and cols of matrix to be compressed
    eps: energy threshold
    rk: rank of r3svd on output
    U, V: pointer to dense row matrices, has to be allocated before
    D: pointer to S vector, has to be allocated before
    t: number of sampling
    p: number of over sampling
    q: power number (q=0 gives the standard r3svd)
    maxit: maximum of iterations
    if t = 0, the parameters t, p, q, maxit are determined by function regarding the matrix dimensions

    note: may be used when A is a pointer to a m x n col dense matrix, by permuting arguments when calling:
                  r3svd(A,n,m,eps,V,D,U, t, p ,q, maxit)
           produces the R3SVD of A = U*D*V' where U, V are pointers to row dense matrices!
*/
template<typename T>
void r3svd(T* A, number_t m, number_t n, real_t eps, number_t& rk, T* U, T* D, T* V,
           number_t t = 0, number_t p = 0, number_t q = 0, number_t maxit = 0)
{
  #ifdef XLIFEPP_WITH_EIGEN
    if(t==0)
      {
        t = std::max(m/10,number_t(2));  // number of sampling
        p = std::max(t/3,number_t(1));   // number of oversampling
        q = 0;                           // power number
        maxit = std::min(m,n)/t;         // number of restart
      }
    real_t tau= 1.-eps;
    number_t tp = t+p, tt= t*maxit;

    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> EigenMatrixCol;
    Eigen::Map<EigenMatrix> eA(A, m, n);  //map matrix pointer to Eigen matrix
    real_t nA=eA.squaredNorm(), nAp=0.;

    //generate Gaussian matrix
    EigenMatrix G(n,tp);
    EigenMatrix Im(EigenMatrix::Identity(m,tp)), In(EigenMatrix::Identity(n,tp));
    normalDistribution(G.data(),n,tp);
    number_t r=0;
    EigenMatrixCol UL(EigenMatrixCol::Zero(m,tt)), VL(EigenMatrixCol::Zero(n,tt)), sigmaL(EigenMatrixCol::Zero(tt,1)), UBi, VBi;
    EigenMatrix Q;
    bool cont = true;

    for(number_t it=0; it<maxit && cont; ++it)  //main loop
      {
        Eigen::Map<EigenMatrixCol> VLr(VL.data(), n, r);// reduce to r first columns
        EigenMatrix Y=eA*G;                          // product Y = A * G, size of Y: m x tp
        Eigen::HouseholderQR<EigenMatrix> qr(Y);     // QR factorization of Y, size of Q: m x tp
        Q = qr.householderQ() * Im;
        if(q>0 && r>0)       //power scheme
          {
            for(number_t j=0; j<q; ++j)
              {
                Y = eA.adjoint()*Q;
                Y -= VLr*(VLr.adjoint()*Y);
                Eigen::HouseholderQR<EigenMatrix> qr1(Y);
                Q = qr1.householderQ() * In;
                Y = eA * Q;
                //Y -= VL*(VL.adjoint()*Y);   error in r3svd paper ?
                Eigen::HouseholderQR<EigenMatrix> qr2(Y);
                Q = qr2.householderQ() * Im;
              }
          }
        Eigen::JacobiSVD<EigenMatrix> svd(Q.adjoint()*eA, Eigen::ComputeThinU | Eigen::ComputeThinV); //QtA = UBi * SBi * VBi'
        UBi=Q*svd.matrixU();                             // size of UBi: m x tp
        VBi=svd.matrixV();                               // size of VBi: n x tp
        if(r>0)
          {
            VBi-=VLr*(VLr.adjoint()*VBi);
            Eigen::HouseholderQR<EigenMatrix> qr2(VBi);
            VBi=qr2.householderQ() * In;                 // size of VBi: n x tp
          }
        for(number_t j=0; j<t && cont; ++j, ++r)         //update UL, VL, sigmaL
          {
            real_t sj = *(svd.singularValues().data()+j);
            *(sigmaL.data()+r) = sj;
            UL.col(r)=UBi.col(j);
            VL.col(r)=VBi.col(j);
            nAp+=sj*sj;
            if(nAp >= tau * nA) cont = false;            //stop criteria
          }
        if(cont)
          {
            Eigen::Map<EigenMatrixCol> VLr2(VL.data(), n, r); //reduce to r first columns
            G-=VLr2*(VLr2.adjoint()*G);                       //update G
          }
      }
    //sort singular values
    std::multimap<real_t, number_t> idx;
    for(number_t j=0; j<r; ++j) idx.insert(std::make_pair(std::abs(*(sigmaL.data()+j)),j));
    //fill matrix
    std::multimap<real_t, number_t>::reverse_iterator itx;
    for(number_t i=0; i<m; ++i)
      {
        itx = idx.rbegin();
        for(number_t j=0; j<r; ++j, ++U, ++itx) *U = UL(i,itx->second);
      }
    for(number_t i=0; i<n; ++i)
      {
        itx = idx.rbegin();
        for(number_t j=0; j<r; ++j, ++V, ++itx) *V = VL(i,itx->second);
      }
    itx = idx.rbegin();
    for(number_t i=0; i<r; ++i, ++D, ++itx)   *D = *(sigmaL.data()+itx->second);
    rk=r;
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

/*! Random SVD compression method of a matrix of class M
    T: type of the result
    A: matrix of class M, requires the member functions:
             M::numberOfRows(), M::numberOfCols()
             M::multMatrixRow(T* M, T* R, number_t p) i.e M*matRow
             M::multLeftMatrixRow(T* M, T* R, number_t p) i.e matRow*M
    r: prescribed rank >0 of truncature
    U, V: vectors storing dense row matrices
    S: vector storing S
*/
template<typename M, typename T>
void rsvd(M& A, number_t r, std::vector<T>& U, std::vector<T>& D, std::vector<T>& V)
{
  #ifdef XLIFEPP_WITH_EIGEN
    number_t m=A.numberOfRows(), n=A.numberOfCols();
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    number_t p=2*r;
    EigenMatrix G(n,p);
    normalDistribution(G.data(),n,p);
    EigenMatrix Y(m,p);
    A.multMatrixRow(G.data(),Y.data(),p);          //product Y = A * G where G is n x 2r Gaussian matrix
    Eigen::HouseholderQR<EigenMatrix> qr(Y);       //QR factorization
    EigenMatrix Q(EigenMatrix::Identity(m,p));
    Q = qr.householderQ() * Q;
    EigenMatrix qtA(p,n);
    EigenMatrix Qadj=Q.adjoint();
    A.multLeftMatrixRow(Qadj.data(),qtA.data(),p); //product Qt*A
    Eigen::JacobiSVD<EigenMatrix> svd(qtA, Eigen::ComputeThinU | Eigen::ComputeThinV);
    EigenMatrix eU=Q*svd.matrixU();
    //fill matrix U,V,D
    U.resize(m*r); V.resize(n*r); D.resize(r);
    typename std::vector<T>::iterator itv=U.begin();
    for(number_t i=0; i<m; ++i)
      for(number_t j=0; j<r; ++j, ++itv) *itv = eU(i,j);
    itv=V.begin();
    for(number_t i=0; i<n; ++i)
      for(number_t j=0; j<r; ++j, ++itv) *itv = svd.matrixV()(i,j);
    itv=D.begin();
    for(number_t i=0; i<r; ++i, ++itv)   *itv = *(svd.singularValues().data()+i);
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

/*! Random SVD compression method of a matrix of class M
    from the paper: Stephanie Chaillat,  George Biros.  FaIMS: A fast algorithm for the inverse medium problem with multiple frequencies
                     and multiple sources for the scalar Helmholtz equation.  Journal of Computational Physics, Elsevier, 2012, 231 (12), pp.4403-4421.
    T: type of the result
    A: matrix of class M, requires the member functions:
             M::numberOfRows(), M::numberOfCols()
             M::multMatrixRow(T* M, T* R, number_t p) i.e M*matRow
             M::multLeftMatrixRow(T* M, T* R, number_t p) i.e matRow*M
    eps: desired precision for the SVD matrix
    rk: rank of rsvd on output
    U, V: vectors storing dense row matrices
    S: vector storing S
*/
template<typename M, typename T>
void rsvd(M& A, real_t eps, number_t& rk, std::vector<T>& U, std::vector<T>& D, std::vector<T>& V)
{
  #ifdef XLIFEPP_WITH_EIGEN
    number_t m=A.numberOfRows(), n=A.numberOfCols();
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    number_t r=1;
    number_t t= std::max(m/10,number_t(2));  // number of sampling
    number_t mn=std::min(m,n);
    bool cont=true;
    EigenMatrix Q;
    number_t p;
    while(cont)
      {
        p=r+t;
        EigenMatrix G(n,p);
        normalDistribution(G.data(),n,p);
        EigenMatrix Y(m,p);
        A.multMatrixRow(G.data(),Y.data(),p);
        Eigen::JacobiSVD<EigenMatrix> svd(Y, Eigen::ComputeThinU | Eigen::ComputeThinV);
        Q=svd.matrixU().leftCols(r);
        EigenMatrix E=Y-Q*(Q.adjoint()*Y);
        if(E.norm()>eps*(*svd.singularValues().data()) && (r+t)<=mn) r+=t;
        else cont=false;
      }
    EigenMatrix qtA(r,n);
    EigenMatrix Qadj=Q.adjoint();
    A.multLeftMatrixRow(Qadj.data(),qtA.data(),r);
    Eigen::JacobiSVD<EigenMatrix> svd(qtA, Eigen::ComputeThinU | Eigen::ComputeThinV);
    EigenMatrix eU=Q*svd.matrixU();
    //fill matrix U,V,D
    U.resize(m*r); V.resize(n*r);D.resize(r);
    typename std::vector<T>::iterator itv=U.begin();
    for(number_t i=0; i<m; ++i)
      for(number_t j=0; j<r; ++j, ++itv) *itv = eU(i,j);
    itv=V.begin();
    for(number_t i=0; i<n; ++i)
      for(number_t j=0; j<r; ++j, ++itv) *itv = svd.matrixV()(i,j);
    itv=D.begin();
    for(number_t i=0; i<r; ++i, ++itv)   *itv = *(svd.singularValues().data()+i);
    rk=r;
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

/*! improved Random SVD compression method of a matrix of class M
    from https://arxiv.org/ftp/arxiv/papers/1605/1605.08134.pdf
    T: type of the result
    A: matrix of class M, requires the member functions:
             M::numberOfRows(), M::numberOfCols(), M::squaredNorm()
             M::multMatrixRow(T* M, T* R, number_t p) i.e M*matRow
             M::multLeftMatrixRow(T* M, T* R, number_t p) i.e matRow*M
    eps: energy threshold
    rk: rank of r3svd on output
    U, V: vectors storing dense row matrices
    S: vector storing S
    t: number of sampling
    p: number of over sampling
    q: power number (q=0 gives the standard r3svd)
    maxit: maximum of iterations
    if t = 0, the parameters t, p, q, maxit are determined by function regarding the matrix dimensions
*/
template<typename M, typename T>
void r3svd(M& A, real_t eps, number_t& rk, std::vector<T>& U, std::vector<T>& D, std::vector<T>& V,
           number_t t = 0, number_t p = 0, number_t q = 0, number_t maxit = 0)
{
  #ifdef XLIFEPP_WITH_EIGEN
    number_t m=A.numberOfRows(), n=A.numberOfCols();
    if(t==0)
      {
        t = std::max(m/10,number_t(2));  // number of sampling
        p = std::max(t/3,number_t(1));   // number of oversampling
        q = 0;                           // power number
        maxit = std::min(m,n)/t;         // number of restart
      }
    real_t tau= 1.-eps;
    number_t tp = t+p, tt= t*maxit;

    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> EigenMatrixCol;
    real_t nA=A.squaredNorm(), nAp=0.;

    //generate Gaussian matrix
    EigenMatrix G(n,tp);
    EigenMatrix Im(EigenMatrix::Identity(m,tp)), In(EigenMatrix::Identity(n,tp));
    normalDistribution(G.data(),n,tp);
    number_t r=0;
    EigenMatrixCol UL(EigenMatrixCol::Zero(m,tt)), VL(EigenMatrixCol::Zero(n,tt)), sigmaL(EigenMatrixCol::Zero(tt,1)), UBi, VBi;
    EigenMatrix Q;
    bool cont = true;

    for(number_t it=0; it<maxit && cont; ++it)  //main loop
      {
        Eigen::Map<EigenMatrixCol> VLr(VL.data(), n, r);// reduce to r first columns
        EigenMatrix Y(m,tp);
        A.multMatrixRow(G.data(),Y.data(),tp);          // product Y = A * G, size of Y: m x tp
        Eigen::HouseholderQR<EigenMatrix> qr(Y);        // QR factorization of Y, size of Q: m x tp
        Q = qr.householderQ() * Im;
        EigenMatrix qtA(tp,n), Qadj;
        if(q>0 && r>0)       //power scheme
          {
            for(number_t j=0; j<q; ++j)
              {
                Qadj=Q.adjoint();
                A.multLeftMatrixRow(Qadj.data(),qtA.data(),p);
                Y=qtA.adjoint();
                Y -= VLr*(VLr.adjoint()*Y);
                Eigen::HouseholderQR<EigenMatrix> qr1(Y);
                Q = qr1.householderQ() * In;
                A.multMatrixRow(Q.data(),Y.data(),tp);   // product Y = A * Q, size of Y: m x tp
                //Y -= VL*(VL.adjoint()*Y);   error in r3svd paper ?
                Eigen::HouseholderQR<EigenMatrix> qr2(Y);
                Q = qr2.householderQ() * Im;
              }
          }
        Qadj=Q.adjoint();
        A.multLeftMatrixRow(Qadj.data(),qtA.data(),tp);
        Eigen::JacobiSVD<EigenMatrix> svd(qtA, Eigen::ComputeThinU | Eigen::ComputeThinV); //QtA = UBi * SBi * VBi'
        UBi=Q*svd.matrixU();                             // size of UBi: m x tp
        VBi=svd.matrixV();                               // size of VBi: n x tp
        if(r>0)
          {
            VBi-=VLr*(VLr.adjoint()*VBi);
            Eigen::HouseholderQR<EigenMatrix> qr2(VBi);
            VBi=qr2.householderQ() * In;                 // size of VBi: n x tp
          }
        for(number_t j=0; j<t && cont; ++j, ++r)         //update UL, VL, sigmaL
          {
            real_t sj = *(svd.singularValues().data()+j);
            //sigmaL(r) = sj;
            *(sigmaL.data()+r) = sj;
            UL.col(r)=UBi.col(j);
            VL.col(r)=VBi.col(j);
            nAp+=sj*sj;
            if(nAp >= tau * nA) cont = false;            //stop criteria
          }
        if(cont)
          {
            Eigen::Map<EigenMatrixCol> VLr2(VL.data(), n, r); //reduce to r first columns
            G-=VLr2*(VLr2.adjoint()*G);                       //update G
          }
      }
    //sort singular values
    std::multimap<real_t, number_t> idx;
    //for(number_t j=0; j<r; ++j) idx.insert(std::make_pair(std::abs(sigmaL(j)),j));
    for(number_t j=0; j<r; ++j) idx.insert(std::make_pair(std::abs(*(sigmaL.data()+j)),j));
    //fill matrix
    U.resize(m*r); V.resize(n*r);D.resize(r);
    std::multimap<real_t, number_t>::reverse_iterator itx;
    typename std::vector<T>::iterator itv=U.begin();
    for(number_t i=0; i<m; ++i)
      {
        itx = idx.rbegin();
        for(number_t j=0; j<r; ++j, ++itv, ++itx) *itv = UL(i,itx->second);
      }
    itv=V.begin();
    for(number_t i=0; i<n; ++i)
      {
        itx = idx.rbegin();
        for(number_t j=0; j<r; ++j, ++itv, ++itx) *itv = VL(i,itx->second);
      }
    itx = idx.rbegin();
    itv=D.begin();
    //for(number_t i=0; i<r; ++i, ++itv, ++itx)   *itv = sigmaL(itx->second);
    for(number_t i=0; i<r; ++i, ++itv, ++itx)   *itv = *(sigmaL.data()+itx->second);
    rk=r;
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

#else //if !defined( __MINGW32__) || !defined( COMPILER_IS_32_BITS)

/*-----------------------------------------------------------------------------------
    special implementation for gcc win32 using a local eigen matrix product
-------------------------------------------------------------------------------------*/

/*! product of two row dense matrix C=A*B replacing the Eigen product
    pA: pointer to a row dense matrix A of size m x p, stored in row major
    pB: pointer to a row dense matrix B of size p x n, stored in row major
    pC: pointer to a row dense matrix C of size m x n, stored in row major
*/
template<typename T>
void eigenMatrixProduct(const T* pA, const T* pB,  T* pC, number_t m, number_t p, number_t n)
{
  for(number_t i=0; i<m; ++i)
    for(number_t j=0; j<n; ++j, ++pC)
      {
        *pC*=0.;
        const T* pAi = pA+i*p;
        const T* pBj = pB+j;
        for(number_t k=0; k<p; ++k, ++pAi, pBj+=n) *pC += *pAi * *pBj;
      }
}

/*! Random SVD compression method of a row dense matrix given by pointer
    T: type of the result
    A: pointer to the first element of the dense matrix to be compressed, stored as row major access (dense row)
    m, n: number of rows and cols of matrix to be compressed
    r: prescribed rank >0 of truncation
    U, V: pointer to dense row matrices, has to be allocated before
    S: pointer to S vector, has to be allocated before

    note: may be used when A is a pointer to a m x n col dense matrix, by permuting arguments when calling:
                  rsvd(A,n,m,rk,V,D,U)
           produces the RSVD of A = U*D*V' where U, V are pointers to row dense matrices!
*/
template<typename T>
void rsvd(T* A, number_t m, number_t n, number_t r, T* U, T* D, T* V)
{
  #ifdef XLIFEPP_WITH_EIGEN
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    Eigen::Map<EigenMatrix> eA(A, m, n);
    //do the product Y = A * G where G is n x 2r Gaussian matrix
    number_t p=2*r;
    EigenMatrix G(n,p);
    normalDistribution(G.data(),n,p);
    EigenMatrix Y(m,p);
    eigenMatrixProduct(eA.data(),G.data(),Y.data(), m, n, p);
    //QR factorization
    Eigen::HouseholderQR<EigenMatrix> qr(Y);
    EigenMatrix Q(EigenMatrix::Identity(m,p));
    Q = qr.householderQ() * Q;
    //product Qt*A
    EigenMatrix qtA(p,n);
    EigenMatrix Qadj=Q.adjoint();
    eigenMatrixProduct(Qadj.data(),eA.data(),qtA.data(),p,m,n);
    //do SVD of eY
    Eigen::JacobiSVD<EigenMatrix> svd(qtA, Eigen::ComputeThinU | Eigen::ComputeThinV);
    EigenMatrix eU(n,p);
    eigenMatrixProduct(Q.data(),svd.matrixU().data(),eU.data(),n, p, p);
    //fill low rank matrix
    for(number_t i=0; i<m; ++i)
      for(number_t j=0; j<r; ++j, ++U) *U = eU(i,j);
    for(number_t i=0; i<n; ++i)
      for(number_t j=0; j<r; ++j, ++V) *V = svd.matrixV()(i,j);
    for(number_t i=0; i<r; ++i, ++D)   *D = *(svd.singularValues().data()+i);
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

/*! Random SVD compression method of a row dense matrix given by pointer
    from the paper: Stephanie Chaillat,  George Biros.  FaIMS: A fast algorithm for the inverse medium problem with multiple frequencies
                     and multiple sources for the scalar Helmholtz equation.  Journal of Computational Physics, Elsevier, 2012, 231 (12), pp.4403-4421.
    T: type of the result
    A: pointer to the first element of the dense matrix to be compressed, stored as row major access (dense row)
    m, n: number of rows and cols of matrix to be compressed
    eps: desired precision for the SVD matrix
    rk: rank of rsvd on output
    U, V: pointer to dense row matrices, has to be allocated before
    S: pointer to S vector, has to be allocated before

    note: may be used when A is a pointer to a m x n col dense matrix, by permuting arguments when calling:
                  rsvd(A,n,m,eps,V,D,U)
           produces the RSVD of A = U*D*V' where U, V are pointers to row dense matrices!
*/
template<typename T>
void rsvd(T* A, number_t m, number_t n, real_t eps, number_t& rk, T* U, T* D, T* V)
{
  #ifdef XLIFEPP_WITH_EIGEN
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    Eigen::Map<EigenMatrix> eA(A, m, n);
    number_t r=1;
    number_t t= std::max(m/10,number_t(2));  // number of sampling
    bool cont=true;
    number_t mn=std::min(m,n);
    EigenMatrix Q, Qt;
    while(cont)
      {
        number_t p=r+t;
        EigenMatrix G(n,p);
        normalDistribution(G.data(),n,p);
        EigenMatrix Y(m,p), QtY(r,p), QQtY(m,p);
        eigenMatrixProduct(eA.data(),G.data(),Y.data(), m, n, p);
        Eigen::JacobiSVD<EigenMatrix> svd(Y, Eigen::ComputeThinU | Eigen::ComputeThinV);
        Q=svd.matrixU().leftCols(r);
        Qt=Q.adjoint();
        eigenMatrixProduct(Qt.data(),Y.data(),QtY.data(),r,m,p);
        eigenMatrixProduct(Q.data(),QtY.data(),QQtY.data(),m,r,p);
        EigenMatrix E=Y-QQtY;
        if(E.norm()>eps*(*svd.singularValues().data()) && r+t<=mn) r+=t;
        else cont=false;
      }
  //fill low rank matrix
    EigenMatrix QtA(r,n);
    eigenMatrixProduct(Qt.data(),eA.data(),QtA.data(),r,m,n);
    Eigen::JacobiSVD<EigenMatrix> svd(QtA, Eigen::ComputeThinU | Eigen::ComputeThinV);
    EigenMatrix eU(m,r);
    eigenMatrixProduct(Q.data(),svd.matrixU().data(),eU.data(),m,r,r);
    for(number_t i=0; i<m; ++i)
      for(number_t j=0; j<r; ++j, ++U) *U = eU(i,j);
    for(number_t i=0; i<n; ++i)
      for(number_t j=0; j<r; ++j, ++V) *V = svd.matrixV()(i,j);
    for(number_t i=0; i<r; ++i, ++D)   *D = *(svd.singularValues().data()+i);
    rk=r;
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

/*! improved Random SVD compression method of a row dense matrix given by pointer
    from https://arxiv.org/ftp/arxiv/papers/1605/1605.08134.pdf

    T: type of the result
    A: pointer to the first element of the dense matrix to be compressed, stored as row major access (dense row)
    m, n: number of rows and cols of matrix to be compressed
    eps: energy threshold
    rk: rank of r3svd on output
    U, V: pointer to dense row matrices, has to be allocated before
    D: pointer to S vector, has to be allocated before
    t: number of sampling
    p: number of over sampling
    q: power number (q=0 gives the standard r3svd)
    maxit: maximum of iterations
    if t = 0, the parameters t, p, q, maxit are determined by function regarding the matrix dimensions

    note: may be used when A is a pointer to a m x n col dense matrix, by permuting arguments when calling:
                  r3svd(A,n,m,eps,V,D,U, t, p ,q, maxit)
           produces the R3SVD of A = U*D*V' where U, V are pointers to row dense matrices!
*/
template<typename T>
void r3svd(T* A, number_t m, number_t n, real_t eps, number_t& rk, T* U, T* D, T* V,
           number_t t = 0, number_t p = 0, number_t q = 0, number_t maxit = 0)
{
  if(t==0)
    {
      t = std::max(m/10,number_t(2));  // number of sampling
      p = std::max(t/3,number_t(1));   // number of oversampling
      q = 0;                           // power number
      maxit = std::min(m,n)/t;         // number of restart
    }
  real_t tau= 1.-eps;
  number_t tp = t+p, tt= t*maxit;

  #ifdef XLIFEPP_WITH_EIGEN
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    Eigen::Map<EigenMatrix> eA(A, m, n);
    real_t nA=eA.squaredNorm(), nAp=0.;

    //generate Gaussian matrix
    EigenMatrix G(n,tp);
    EigenMatrix Im(EigenMatrix::Identity(m,tp)), In(EigenMatrix::Identity(n,tp));
    normalDistribution(G.data(),n,tp);
    number_t r=0;
    EigenMatrix UL(EigenMatrix::Zero(m,tt)), VL(EigenMatrix::Zero(n,tt)), sigmaL(EigenMatrix::Zero(tt,1)), UBi, VBi, Q;
    EigenMatrix Y(m,tp), V2(n,tp);
    bool cont = true;

    for(number_t it=0; it<maxit && cont; ++it)  //main loop
      {
        EigenMatrix V1(r,tp);
        eigenMatrixProduct(eA.data(),G.data(),Y.data(), m, n, tp);  // product Y = A * G, size of Y: m x tp
        Eigen::HouseholderQR<EigenMatrix> qr(Y);                    // QR factorization of Y, size of Q: m x tp
        EigenMatrix  Qr=qr.householderQ();
        Q = qr.householderQ() * Im;
        EigenMatrix VLr=VL.leftCols(r);                             // restrict to first r col
        EigenMatrix VLt=VLr.adjoint();
        if(q>0 && r>0)   //apply power scheme
          {
            EigenMatrix At=eA.adjoint(), Y1(n,tp);
            for(number_t j=0; j<q; ++j)
              {
                eigenMatrixProduct(At.data(), Q.data(), Y1.data(), n, m, tp);    //Y1 = A.adjoint()*Q;
                eigenMatrixProduct(VLt.data(), Y1.data(), V1.data(), r, n, tp);
                eigenMatrixProduct(V1.data(), VLr.data(), V2.data(), n, r, tp);
                Y1-=V2;                                                    // Y1 -= VLr*(VLr.adjoint()*Y1);
                Eigen::HouseholderQR<EigenMatrix> qr1(Y1);
                Q = qr1.householderQ() * In;                                 //size of Q n x  tp
                eigenMatrixProduct(eA.data(), Q.data(), Y.data(), m, n, tp); //Y2 = A*Q;
                //Y -= VL*(VL.adjoint()*Y);    error in r3svd paper ?
                Eigen::HouseholderQR<EigenMatrix> qr2(Y);
                Q = qr2.householderQ() * Im;
              }
          }
        EigenMatrix QtA(tp,n);
        EigenMatrix Qadj=Q.adjoint();
        eigenMatrixProduct(Qadj.data(),eA.data(),QtA.data(),tp,m,n);
        Eigen::JacobiSVD<EigenMatrix> svd(QtA, Eigen::ComputeThinU | Eigen::ComputeThinV); //QtA = UBi * SBi * VBi'
        UBi.resize(m,tp);
        eigenMatrixProduct(Q.data(), svd.matrixU().data(), UBi.data(), m, tp, tp);
        VBi=svd.matrixV();                                          // size of VBi: n x tp
        if(r>0)
          {
            eigenMatrixProduct(VLt.data(), VBi.data(), V1.data(), r,  n, tp);
            eigenMatrixProduct(VLr.data(), V1.data(), V2.data(), n, r, tp);
            VBi-=V2;                                                          // VBi-=VLr*(VLr.adjoint()*VBi);
            Eigen::HouseholderQR<EigenMatrix> qr2(VBi);
            VBi=qr2.householderQ() * In;               // size of VBi: n x tp
          }
        for(number_t j=0; j<t && cont; ++j, ++r)       //update UL, VL, sigmaL
          {
            real_t sj = *(svd.singularValues().data()+j);
            sigmaL(r) = sj;
            UL.col(r)=UBi.col(j);
            VL.col(r)=VBi.col(j);
            nAp+=sj*sj;
            if(nAp >= tau * nA) cont = false;          //stop criteria
          }
        if(cont)
          {
            EigenMatrix VLr2=VL.leftCols(r);          // restrict to first r col
            VLt=VLr2.adjoint();
            V1.resize(r,tp);
            eigenMatrixProduct(VLt.data(), G.data(), V1.data(), r, n, tp);
            eigenMatrixProduct(VLr2.data(), V1.data(), V2.data(), n, r, tp);
            G-=V2;
          }
      }
    //sort singular values
    std::multimap<real_t, number_t> idx;
    for(number_t j=0; j<r; ++j) idx.insert(std::make_pair(std::abs(*(sigmaL.data()+j)),j));
    //fill matrix
    std::multimap<real_t, number_t>::reverse_iterator itx;
    for(number_t i=0; i<m; ++i)
      {
        itx = idx.rbegin();
        for(number_t j=0; j<r; ++j, ++U, ++itx) *U = UL(i,itx->second);
      }
    for(number_t i=0; i<n; ++i)
      {
        itx = idx.rbegin();
        for(number_t j=0; j<r; ++j, ++V, ++itx) *V = VL(i,itx->second);
      }
    itx = idx.rbegin();
    for(number_t i=0; i<r; ++i, ++D, ++itx)   *D = *(sigmaL.data()+itx->second);
    rk=r;
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

/*! Random SVD compression method of a matrix of class M
    T: type of the result
    A: matrix of class M, requires the member functions:
             M::numberOfRows(), M::numberOfCols()
             M::multMatrixRow(T* M, T* R, number_t p) i.e M*matRow
             M::multLeftMatrixRow(T* M, T* R, number_t p) i.e matRow*M
    r: prescribed rank >0 of truncature
    U, V: vectors storing dense row matrices
    S: vector storing S
*/
template<typename M, typename T>
void rsvd(M& A, number_t r, std::vector<T>& U, std::vector<T>& D, std::vector<T>& V)
{
  #ifdef XLIFEPP_WITH_EIGEN
    number_t m=A.numberOfRows(), n=A.numberOfCols();
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    number_t p=2*r;
    EigenMatrix G(n,p);
    normalDistribution(G.data(),n,p);
    EigenMatrix Y(m,p);
    A.multMatrixRow(G.data(),Y.data(),p);          //product Y = A * G where G is n x 2r Gaussian matrix
    Eigen::HouseholderQR<EigenMatrix> qr(Y);       //QR factorization
    EigenMatrix Q(EigenMatrix::Identity(m,p));
    Q = qr.householderQ() * Q;
    EigenMatrix qtA(p,n);
    EigenMatrix Qadj=Q.adjoint();
    A.multLeftMatrixRow(Qadj.data(),qtA.data(),p); //product Qt*A
    Eigen::JacobiSVD<EigenMatrix> svd(qtA, Eigen::ComputeThinU | Eigen::ComputeThinV);
    EigenMatrix eU(m,p);
    eigenMatrixProduct(Q.data(),svd.matrixU().data(),eU.data(),m, p, p);
    //fill low rank matrix
    U.resize(m*r); V.resize(n*r); D.resize(r);
    typename std::vector<T>::iterator itv=U.begin();
    for(number_t i=0; i<m; ++i)
      for(number_t j=0; j<r; ++j, ++itv) *itv = eU(i,j);
    itv=V.begin();
    for(number_t i=0; i<n; ++i)
      for(number_t j=0; j<r; ++j, ++itv) *itv = svd.matrixV()(i,j);
    itv=D.begin();
    for(number_t i=0; i<r; ++i, ++itv)   *itv = *(svd.singularValues().data()+i);
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

/*! Random SVD compression method of a matrix of class M
    from the paper: Stephanie Chaillat,  George Biros.  FaIMS: A fast algorithm for the inverse medium problem with multiple frequencies
                     and multiple sources for the scalar Helmholtz equation.  Journal of Computational Physics, Elsevier, 2012, 231 (12), pp.4403-4421.
    T: type of the result
    A: matrix of class M, requires the member functions:
             M::numberOfRows(), M::numberOfCols()
             M::multMatrixRow(T* M, T* R, number_t p) i.e M*matRow
             M::multLeftMatrixRow(T* M, T* R, number_t p) i.e matRow*M
    eps: desired precision for the SVD matrix
    rk: rank of rsvd on output
    U, V: pointer to dense row matrices, has to be allocated before
    S: pointer to S vector, has to be allocated before
*/
template<typename M, typename T>
void rsvd(M& A, real_t eps, number_t& rk,std::vector<T>& U, std::vector<T>& D, std::vector<T>& V)
{
  #ifdef XLIFEPP_WITH_EIGEN
    number_t m=A.numberOfRows(), n=A.numberOfCols();
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    number_t r=1;
    number_t t= std::max(m/10,number_t(2));  // number of sampling
    number_t mn=std::min(m,n);
    bool cont=true;
    EigenMatrix Q, Qt;
    number_t p;
    while(cont)
      {
        p=r+t;
        EigenMatrix G(n,p);
        normalDistribution(G.data(),n,p);
        EigenMatrix Y(m,p), QtY(r,p), QQtY(m,p);
        A.multMatrixRow(G.data(),Y.data(),p);
        Eigen::JacobiSVD<EigenMatrix> svd(Y, Eigen::ComputeThinU | Eigen::ComputeThinV);
        Q=svd.matrixU().leftCols(r);
        Qt=Q.adjoint();
        eigenMatrixProduct(Qt.data(),Y.data(),QtY.data(),r,m,p);
        eigenMatrixProduct(Q.data(),QtY.data(),QQtY.data(),m,r,p);
        EigenMatrix E=Y-QQtY;
        if(E.norm()>eps*(*svd.singularValues().data()) && r+t<=mn) r+=t;
        else cont=false;
      }
    EigenMatrix qtA(r,n);
    A.multLeftMatrixRow(Qt.data(),qtA.data(),r);
    Eigen::JacobiSVD<EigenMatrix> svd(qtA, Eigen::ComputeThinU | Eigen::ComputeThinV);
    EigenMatrix eU(m,r);
    eigenMatrixProduct(Q.data(),svd.matrixU().data(),eU.data(),m,r,r);
    //fill matrix U,V,D
    U.resize(m*r); V.resize(n*r);D.resize(r);
    typename std::vector<T>::iterator itv=U.begin();
    for(number_t i=0; i<m; ++i)
      for(number_t j=0; j<r; ++j, ++itv) *itv = eU(i,j);
    itv=V.begin();
    for(number_t i=0; i<n; ++i)
      for(number_t j=0; j<r; ++j, ++itv) *itv = svd.matrixV()(i,j);
    itv=D.begin();
    for(number_t i=0; i<r; ++i, ++itv)   *itv = *(svd.singularValues().data()+i);
    rk=r;
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

/*! improved Random SVD compression method of a matrix of class M
    from https://arxiv.org/ftp/arxiv/papers/1605/1605.08134.pdf
    T: type of the result
    A: matrix of class M, requires the member functions:
             M::numberOfRows(), M::numberOfCols(), M::squaredNorm()
             M::multMatrixRow(T* M, T* R, number_t p) i.e M*matRow
             M::multLeftMatrixRow(T* M, T* R, number_t p) i.e matRow*M
    eps: energy threshold
    rk: rank of r3svd on output
    U, V: vectors storing dense row matrices
    S: vector storing S
    t: number of sampling
    p: number of over sampling
    q: power number (q=0 gives the standard r3svd)
    maxit: maximum of iterations
    if t = 0, the parameters t, p, q, maxit are determined by function regarding the matrix dimensions
*/
template<typename M, typename T>
void r3svd(M& A, real_t eps, number_t& rk, std::vector<T>& U, std::vector<T>& D, std::vector<T>& V,
           number_t t = 0, number_t p = 0, number_t q = 0, number_t maxit = 0)
{
  number_t m=A.numberOfRows(), n=A.numberOfCols();
  if(t==0)
    {
      t = std::max(m/10,number_t(2));  // number of sampling
      p = std::max(t/3,number_t(1));   // number of oversampling
      q = 0;                           // power number
      maxit = std::min(m,n)/t;         // number of restart
    }
  real_t tau= 1.-eps;
  number_t tp = t+p, tt= t*maxit;

  #ifdef XLIFEPP_WITH_EIGEN
    typedef typename Eigen::Matrix< T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrix;
    real_t nA=A.squaredNorm(), nAp=0.;

    //generate Gaussian matrix
    EigenMatrix G(n,tp);
    EigenMatrix Im(EigenMatrix::Identity(m,tp)), In(EigenMatrix::Identity(n,tp));
    normalDistribution(G.data(),n,tp);
    number_t r=0;
    EigenMatrix UL(EigenMatrix::Zero(m,tt)), VL(EigenMatrix::Zero(n,tt)), sigmaL(EigenMatrix::Zero(tt,1)), UBi, VBi, Q;
    EigenMatrix Y(m,tp), V2(n,tp);
    bool cont = true;

    for(number_t it=0; it<maxit && cont; ++it)  //main loop
      {
        EigenMatrix V1(r,tp);
        A.multMatrixRow(G.data(),Y.data(),tp);    // product Y = A * G, size of Y: m x tp
        Eigen::HouseholderQR<EigenMatrix> qr(Y);  // QR factorization of Y, size of Q: m x tp
        EigenMatrix  Qr=qr.householderQ();
        Q = qr.householderQ() * Im;
        EigenMatrix VLr=VL.leftCols(r);           // restrict to first r col
        EigenMatrix VLt=VLr.adjoint();
        EigenMatrix qtA(tp,n), Qadj, Y1;
        if(q>0 && r>0)       //power scheme
          {
            for(number_t j=0; j<q; ++j)
              {
                Qadj=Q.adjoint();
                A.multLeftMatrixRow(Qadj.data(),qtA.data(),tp);
                Y1=qtA.adjoint();                //Y1 = A.adjoint()*Q;
                eigenMatrixProduct(VLt.data(), Y1.data(), V1.data(), r, n, tp);
                eigenMatrixProduct(V1.data(), VLr.data(), V2.data(), n, r, tp);
                Y1-=V2;                                                    // Y1 -= VLr*(VLr.adjoint()*Y1);
                Eigen::HouseholderQR<EigenMatrix> qr1(Y1);
                Q = qr1.householderQ() * In;                               //size of Q n x tp
                A.multMatrixRow(Q.data(),Y.data(),tp);   // product Y = A * Q, size of Y: m x tp
                //Y -= VL*(VL.adjoint()*Y);    error in r3svd paper ?
                Eigen::HouseholderQR<EigenMatrix> qr2(Y);
                Q = qr2.householderQ() * Im;
              }
          }
        Qadj=Q.adjoint();
        A.multLeftMatrixRow(Qadj.data(),qtA.data(),tp);
        Eigen::JacobiSVD<EigenMatrix> svd(qtA, Eigen::ComputeThinU | Eigen::ComputeThinV); //QtA = UBi * SBi * VBi'
        UBi.resize(m,tp);
        eigenMatrixProduct(Q.data(), svd.matrixU().data(), UBi.data(), m, tp, tp);
        VBi=svd.matrixV();                                          // size of VBi: n x tp
        if(r>0)
          {
            eigenMatrixProduct(VLt.data(), VBi.data(), V1.data(), r,  n, tp);
            eigenMatrixProduct(VLr.data(), V1.data(), V2.data(), n, r, tp);
            VBi-=V2;                                                          // VBi-=VLr*(VLr.adjoint()*VBi);
            Eigen::HouseholderQR<EigenMatrix> qr2(VBi);
            VBi=qr2.householderQ() * In;                       // size of VBi: n x tp
          }
        for(number_t j=0; j<t && cont; ++j, ++r)     //update UL, VL, sigmaL
          {
            real_t sj = *(svd.singularValues().data()+j);
            sigmaL(r) = sj;
            UL.col(r)=UBi.col(j);
            VL.col(r)=VBi.col(j);
            nAp+=sj*sj;
            if(nAp >= tau * nA) cont = false;          //stop criteria
          }
        if(cont)
          {
            EigenMatrix VLr2=VL.leftCols(r);                       // restrict to first r col
            VLt=VLr2.adjoint();
            V1.resize(r,tp);
            eigenMatrixProduct(VLt.data(), G.data(), V1.data(), r, n, tp);
            eigenMatrixProduct(VLr2.data(), V1.data(), V2.data(), n, r, tp);
            G-=V2;
          }
      }
    //sort singular values
    std::multimap<real_t, number_t> idx;
    for(number_t j=0; j<r; ++j) idx.insert(std::make_pair(std::abs(*(sigmaL.data()+j)),j));
    //fill matrix
    U.resize(m*r); V.resize(n*r);D.resize(r);
    std::multimap<real_t, number_t>::reverse_iterator itx;
    typename std::vector<T>::iterator itv = U.begin();
    for(number_t i=0; i<m; ++i)
      {
        itx = idx.rbegin();
        for(number_t j=0; j<r; ++j, ++itv, ++itx) *itv = UL(i,itx->second);
      }
    itv = V.begin();
    for(number_t i=0; i<n; ++i)
      {
        itx = idx.rbegin();
        for(number_t j=0; j<r; ++j, ++itv, ++itx) *itv = VL(i,itx->second);
      }
    itx = idx.rbegin();
    itv = D.begin();
    for(number_t i=0; i<r; ++i, ++itv, ++itx)   *itv = *(sigmaL.data()+itx->second);
    rk=r;
  #else
    error("xlifepp_without_eigen");
  #endif // XLIFEPP_WITH_EIGEN
}

#endif //if !defined( __MINGW32__) || !defined( COMPILER_IS_32_BITS)

} //end of namespace xlifepp

#endif // EIGENINTERFACE_HPP_INCLUDED
