/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Parameters.cpp
  \author E. Lunéville
  \since 1 nov 2011
  \date 3 may 2012

  \brief Implementation of xlifepp::Parameter and xlifepp::Parameters classes members and related functions
*/

//===============================================================================
// library dependencies
#include "Parameters.hpp"
#include "complexUtils.hpp" // complex_t utilities
#include "Trace.hpp"
#include "Messages.hpp"
#include "Environment.hpp"
#include "PrintStream.hpp"
// stl dependencies
#include <iostream>
#include <fstream>
//===============================================================================

namespace xlifepp
{

/*
=================================================================================
 Parameter class implementation
=================================================================================
*/
Parameter::Parameter() //! default constructor
  : i_(0), r_(0.), c_(0.), s_(""), b_(false), p_(nullptr), name_(""), key_(_pk_none), type_(_none)
{}

Parameter& Parameter::operator=(const Parameter& p) //! assignment operator=
{
  if ( this == &p ) return *this;// handle self-assignment
  i_ = p.i_;
  r_ = p.r_;
  c_ = p.c_;
  s_ = p.s_;
  b_=p.b_;
  name_ = p.name_;
  shortnames_ = p.shortnames_;
  key_=p.key_;
  type_ = p.type_;
  deletePointer();   //clean pointer of vector/matrix
  switch (p.type_) //full copy in case of vector/matrix
  {
    case _pointer: p_=p.p_; break; //pointer copy, no reallocation
    case _realVector:
    {
      p_ = static_cast<void *>(new std::vector<real_t>(*reinterpret_cast<const std::vector<real_t>*>(p.p_)));
      break;
    }
    case _complexVector:
    {
      p_ = static_cast<void *>(new std::vector<complex_t>(*reinterpret_cast<const std::vector<complex_t>*>(p.p_)));
      break;
    }
    case _integerVector:
    {
      p_ = static_cast<void *>(new std::vector<number_t>(*reinterpret_cast<const std::vector<number_t>*>(p.p_)));
      break;
    }
    case _boolVector:
    {
      p_ = static_cast<void *>(new std::vector<bool>(*reinterpret_cast<const std::vector<bool>*>(p.p_)));
      break;
    }
    case _stringVector:
    {
      p_ = static_cast<void *>(new std::vector<string_t>(*reinterpret_cast<const std::vector<string_t>*>(p.p_)));
      break;
    }
    case _ptVector:
    {
      p_ = static_cast<void *>(new std::vector<Point>(*reinterpret_cast<const std::vector<Point>*>(p.p_)));
      break;
    }
    case _realMatrix:
    {
      p_ = static_cast<void *>(new Matrix<real_t>(*reinterpret_cast<const Matrix<real_t>*>(p.p_)));
      break;
    }
    case _complexMatrix:
    {
      p_ = static_cast<void *>(new Matrix<complex_t>(*reinterpret_cast<const Matrix<complex_t>*>(p.p_)));
      break;
    }
    case _pt:
    {
      p_ = static_cast<void *>(new Point(*reinterpret_cast<const Point*>(p.p_)));
      break;
    }
    case _pointerClusterTreeFeDof:
    {
      p_ = cloneClusterTreeFeDof(p.p_);
      break;
    }
    case _pointerGeomDomain:
    {
      p_ = cloneGeomDomain(p.p_);
      break;
    }
    case _pointerFunction:
    {
      p_ = cloneFunction(p.p_);
      break;
    }
    case _pointerIntegrationMethod:
    {
      p_ = cloneIntegrationMethod(p.p_);
      break;
    }
    case _pointerIntegrationMethods:
    {
      p_ = cloneIntegrationMethods(p.p_);
      break;
    }
    case _pointerParametrization:
    {
      p_ = cloneParametrization(p.p_);
      break;
    }
    case _pointerSpline:
    {
      p_ = cloneSpline(p.p_);
      break;
    }
    case _pointerTermVectors:
    {
      p_ = cloneTermVectors(p.p_);
      break;
    }
    case _pointerTransformation:
    {
      p_= cloneTransformation(p.p_);
      break;
    }
    default: break;
  }
  return *this;
}

// copy constructor with extra arg name
Parameter::Parameter(const Parameter& p, const string_t& nm, const string_t& snm)
  : i_(p.i_), r_(p.r_), c_(p.c_), s_(p.s_), b_(p.b_), p_(p.p_), name_(nm), key_(p.key_), type_(p.type_)
{
  if ( nm == "" ) name_ = p.name_;
  if ( snm == "" ) shortnames_ = p.shortnames_;
  switch (p.type_) //full copy in case of vector/matrix
  {
    case _realVector:
    {
      p_ = static_cast<void *>(new std::vector<real_t>(*reinterpret_cast<const std::vector<real_t>*>(p.p_)));
      break;
    }
    case _complexVector:
    {
      p_ = static_cast<void *>(new std::vector<complex_t>(*reinterpret_cast<const std::vector<complex_t>*>(p.p_)));
      break;
    }
    case _integerVector:
    {
      p_ = static_cast<void *>(new std::vector<number_t>(*reinterpret_cast<const std::vector<number_t>*>(p.p_)));
      break;
    }
    case _boolVector:
    {
      p_ = static_cast<void *>(new std::vector<bool>(*reinterpret_cast<const std::vector<bool>*>(p.p_)));
      break;
    }
    case _stringVector:
    {
      p_ = static_cast<void *>(new std::vector<string_t>(*reinterpret_cast<const std::vector<string_t>*>(p.p_)));
      break;
    }
    case _ptVector:
    {
      p_ = static_cast<void *>(new std::vector<Point>(*reinterpret_cast<const std::vector<Point>*>(p.p_)));
      break;
    }
    case _realMatrix:
    {
      p_ = static_cast<void *>(new Matrix<real_t>(*reinterpret_cast<const Matrix<real_t>*>(p.p_)));
      break;
    }
    case _complexMatrix:
    {
      p_ = static_cast<void *>(new Matrix<complex_t>(*reinterpret_cast<const Matrix<complex_t>*>(p.p_)));
      break;
    }
    case _pt:
    {
      p_ = static_cast<void *>(new Point(*reinterpret_cast<const Point*>(p.p_)));
      break;
    }
    case _pointerClusterTreeFeDof:
    {
      p_ = cloneClusterTreeFeDof(p.p_);
      break;
    }
    case _pointerGeomDomain:
    {
      p_ = cloneGeomDomain(p.p_);
      break;
    }
    case _pointerFunction:
    {
      p_ = cloneFunction(p.p_);
      break;
    }
    case _pointerIntegrationMethod:
    {
      p_ = cloneIntegrationMethod(p.p_);
      break;
    }
    case _pointerIntegrationMethods:
    {
      p_ = cloneIntegrationMethods(p.p_);
      break;
    }
    case _pointerParametrization:
    {
      p_ = cloneParametrization(p.p_);
      break;
    }
    case _pointerSpline:
    {
      p_ = cloneSpline(p.p_);
      break;
    }
    case _pointerTermVectors:
    {
      p_ = cloneTermVectors(p.p_);
      break;
    }
    case _pointerTransformation:
    {
      p_= cloneTransformation(p.p_);
      break;
    }
    default: break;
  }
}

// copy constructor with extra arg name
Parameter::Parameter(const Parameter& p, const string_t& nm, const Strings& snm)
  : i_(p.i_), r_(p.r_), c_(p.c_), s_(p.s_), b_(p.b_), p_(p.p_), name_(nm), key_(p.key_), type_(p.type_)
{
  if ( nm == "" ) name_ = p.name_;
  if ( snm.size() == 1 && snm[0] == "" ) shortnames_ = p.shortnames_;
  switch (p.type_) //full copy in case of vector/matrix
  {
    case _realVector:
    {
      p_ = static_cast<void *>(new std::vector<real_t>(*reinterpret_cast<const std::vector<real_t>*>(p.p_)));
      break;
    }
    case _complexVector:
    {
      p_ = static_cast<void *>(new std::vector<complex_t>(*reinterpret_cast<const std::vector<complex_t>*>(p.p_)));
      break;
    }
    case _integerVector:
    {
      p_ = static_cast<void *>(new std::vector<number_t>(*reinterpret_cast<const std::vector<number_t>*>(p.p_)));
      break;
    }
    case _boolVector:
    {
      p_ = static_cast<void *>(new std::vector<bool>(*reinterpret_cast<const std::vector<bool>*>(p.p_)));
      break;
    }
    case _stringVector:
    {
      p_ = static_cast<void *>(new std::vector<string_t>(*reinterpret_cast<const std::vector<string_t>*>(p.p_)));
      break;
    }
    case _ptVector:
    {
      p_ = static_cast<void *>(new std::vector<Point>(*reinterpret_cast<const std::vector<Point>*>(p.p_)));
      break;
    }
    case _realMatrix:
    {
      p_ = static_cast<void *>(new Matrix<real_t>(*reinterpret_cast<const Matrix<real_t>*>(p.p_)));
      break;
    }
    case _complexMatrix:
    {
      p_ = static_cast<void *>(new Matrix<complex_t>(*reinterpret_cast<const Matrix<complex_t>*>(p.p_)));
      break;
    }
    case _pt:
    {
      p_ = static_cast<void *>(new Point(*reinterpret_cast<const Point*>(p.p_)));
      break;
    }
    case _pointerClusterTreeFeDof:
    {
      p_ = cloneClusterTreeFeDof(p.p_);
      break;
    }
    case _pointerGeomDomain:
    {
      p_ = cloneGeomDomain(p.p_);
      break;
    }
    case _pointerFunction:
    {
      p_ = cloneFunction(p.p_);
      break;
    }
    case _pointerIntegrationMethod:
    {
      p_ = cloneIntegrationMethod(p.p_);
      break;
    }
    case _pointerIntegrationMethods:
    {
      p_ = cloneIntegrationMethods(p.p_);
      break;
    }
    case _pointerParametrization:
    {
      p_ = cloneParametrization(p.p_);
      break;
    }
    case _pointerSpline:
    {
      p_ = cloneSpline(p.p_);
      break;
    }
    case _pointerTermVectors:
    {
      p_ = cloneTermVectors(p.p_);
      break;
    }
    case _pointerTransformation:
    {
      p_= cloneTransformation(p.p_);
      break;
    }
    default: break;
  }
}

Parameter::Parameter(ParameterKey key, const string_t& nm, const string_t& snm) //! default constructor
  : i_(0), r_(0.), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(key), type_(_none)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
}

Parameter::Parameter(ParameterKey key, const string_t& nm, const Strings& snm) //! default constructor
  : i_(0), r_(0.), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(key), type_(_none)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
}

// constructors from an int
Parameter::Parameter(const int_t i, const string_t& nm, const string_t& snm)
: i_(i), r_(0.), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integer)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
}
Parameter::Parameter(const int_t i, const string_t& nm, const Strings& snm)
: i_(i), r_(0.), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integer)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
}
Parameter::Parameter(const int i, const string_t& nm, const string_t& snm)
: i_(i), r_(0.), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integer)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
}
Parameter::Parameter(const int i, const string_t& nm, const Strings& snm)
: i_(i), r_(0.), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integer)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
}
Parameter::Parameter(const number_t i, const string_t& nm, const string_t& snm)
: i_(static_cast<int_t>(i)), r_(0.), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integer)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
}
Parameter::Parameter(const number_t i, const string_t& nm, const Strings& snm)
: i_(static_cast<int_t>(i)), r_(0.), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integer)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
}

Parameter::Parameter(const bool b, const string_t& nm, const Strings& snm)
: i_(0), r_(0.), c_(0.), s_(""), b_(b), p_(nullptr), name_(nm), key_(_pk_none), type_(_bool)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
}
Parameter::Parameter(const bool b, const string_t& nm, const string_t& snm)
: i_(0), r_(0.), c_(0.), s_(""), b_(b), p_(nullptr), name_(nm), key_(_pk_none), type_(_bool)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
}

// constructor from a real_t, complex_t
Parameter::Parameter(const real_t r, const string_t& nm, const string_t& snm)
: i_(0), r_(r), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_real)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
}
Parameter::Parameter(const real_t r, const string_t& nm, const Strings& snm)
: i_(0), r_(r), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_real)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
}
Parameter::Parameter(const complex_t& c, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(c), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_complex)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
}
Parameter::Parameter(const complex_t& c, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(c), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_complex)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
}

// constructor from a string or a char array
Parameter::Parameter(const string_t& s, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(0.), s_(s), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_string)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
}
Parameter::Parameter(const string_t& s, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(0.), s_(s), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_string)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
}
Parameter::Parameter(const char* s, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(0.), s_(string_t(s)), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_string)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
}
Parameter::Parameter(const char* s, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(0.), s_(string_t(s)), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_string)
{}

// constructor from a const void*
Parameter::Parameter(const void* p, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(p), name_(nm), key_(_pk_none), type_(_pointer)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
}
Parameter::Parameter(const void* p, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(p), name_(nm), key_(_pk_none), type_(_pointer)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
}

// constructor from a std::vector
Parameter::Parameter(const std::vector<real_t>& rv, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_realVector)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  p_ = static_cast<void *>(new std::vector<real_t>(rv));
}
Parameter::Parameter(const std::vector<real_t>& rv, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_realVector)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  p_ = static_cast<void *>(new std::vector<real_t>(rv));
}

Parameter::Parameter(const std::vector<complex_t>& cv, const string_t& nm, const string_t& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_complexVector)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  p_ = static_cast<void *>(new std::vector<complex_t>(cv));
}
Parameter::Parameter(const std::vector<complex_t>& cv, const string_t& nm, const Strings& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_complexVector)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  p_ = static_cast<void *>(new std::vector<complex_t>(cv));
}

Parameter::Parameter(const std::vector<int_t>& iv, const string_t& nm, const string_t& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integerVector)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  p_ = static_cast<void *>(new std::vector<int_t>(iv));
}
Parameter::Parameter(const std::vector<int_t>& iv, const string_t& nm, const Strings& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integerVector)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  p_ = static_cast<void *>(new std::vector<int_t>(iv));
}

Parameter::Parameter(const std::vector<bool>& bv, const string_t& nm, const string_t& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integerVector)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  p_ = static_cast<void *>(new std::vector<bool>(bv));
}
Parameter::Parameter(const std::vector<bool>& bv, const string_t& nm, const Strings& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integerVector)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  p_ = static_cast<void *>(new std::vector<bool>(bv));
}

Parameter::Parameter(const std::vector<string_t>& sv, const string_t& nm, const string_t& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_stringVector)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  p_ = static_cast<void *>(new std::vector<string_t>(sv));
}
Parameter::Parameter(const std::vector<string_t>& sv, const string_t& nm, const Strings& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_stringVector)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  p_ = static_cast<void *>(new std::vector<string_t>(sv));
}

// constructor from a Matrix
Parameter::Parameter(const Matrix<real_t>& rm, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_realMatrix)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  p_ = static_cast<void *>(new Matrix<real_t>(rm));
}
Parameter::Parameter(const Matrix<real_t>& rm, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_realMatrix)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  p_ = static_cast<void *>(new Matrix<real_t>(rm));
}

Parameter::Parameter(const Matrix<complex_t>& cm, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_complexMatrix)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  p_ = static_cast<void *>(new Matrix<complex_t>(cm));
}
Parameter::Parameter(const Matrix<complex_t>& cm, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_complexMatrix)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  p_ = static_cast<void *>(new Matrix<complex_t>(cm));
}

// constructor from Point
Parameter::Parameter(const Point& p, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pt)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  p_ = static_cast<void *>(new Point(p));
}
Parameter::Parameter(const Point& p, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pt)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  p_ = static_cast<void *>(new Point(p));
}

Parameter::Parameter(const std::vector<Point>& pv, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_ptVector)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  p_ = static_cast<void *>(new std::vector<Point>(pv));
}
Parameter::Parameter(const std::vector<Point>& pv, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_ptVector)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  p_ = static_cast<void *>(new std::vector<Point>(pv));
}

Parameter::Parameter(const std::vector<number_t>& nv, const string_t& nm, const string_t& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integerVector)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  std::vector<int_t>& iv= *new std::vector<int_t>(nv.size());  // cast to vector<int_t>
  std::vector<int_t>::iterator iti=iv.begin();
  std::vector<number_t>::const_iterator itn=nv.begin();
  for(;itn!=nv.end();++itn,++iti) *iti=numToInt(*itn);
  p_ = static_cast<void *>(&iv);
}
Parameter::Parameter(const std::vector<number_t>& nv, const string_t& nm, const Strings& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integerVector)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  std::vector<int_t>& iv= *new std::vector<int_t>(nv.size());  // cast to vector<int_t>
  std::vector<int_t>::iterator iti=iv.begin();
  std::vector<number_t>::const_iterator itn=nv.begin();
  for(;itn!=nv.end();++itn,++iti) *iti=numToInt(*itn);
  p_ = static_cast<void *>(&iv);
}

//additional constructors from initializer list (c++2011)
#if __cplusplus >= 201103L
Parameter::Parameter(const std::initializer_list<real_t>& rs, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_realVector)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  p_ = static_cast<void *>(new std::vector<real_t>(rs));
}
Parameter::Parameter(const std::initializer_list<real_t>& rs, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_realVector)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  p_ = static_cast<void *>(new std::vector<real_t>(rs));
}

Parameter::Parameter(const std::initializer_list<int>& ns, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integerVector)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  std::vector<int_t>& iv= *new std::vector<int_t>(ns.size());
  std::vector<int_t>::iterator iti=iv.begin();
  std::initializer_list<int>::const_iterator itl=ns.begin();
  for(;itl!=ns.end();++itl,++iti) *iti=*itl;
  p_ = static_cast<void *>(&iv);
}
Parameter::Parameter(const std::initializer_list<int>& ns, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integerVector)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  std::vector<int_t>& iv= *new std::vector<int_t>(ns.size());
  std::vector<int_t>::iterator iti=iv.begin();
  std::initializer_list<int>::const_iterator itl=ns.begin();
  for(;itl!=ns.end();++itl,++iti) *iti=*itl;
  p_ = static_cast<void *>(&iv);
}

Parameter::Parameter(const std::initializer_list<number_t>& ns, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integerVector)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  std::vector<int_t>& iv= *new std::vector<int_t>(ns.size());
  std::vector<int_t>::iterator iti=iv.begin();
  std::initializer_list<number_t>::const_iterator itl=ns.begin();
  for(;itl!=ns.end();++itl,++iti) *iti=*itl;
  p_ = static_cast<void *>(&iv);
}
Parameter::Parameter(const std::initializer_list<number_t>& ns, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_integerVector)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  std::vector<int_t>& iv= *new std::vector<int_t>(ns.size());
  std::vector<int_t>::iterator iti=iv.begin();
  std::initializer_list<number_t>::const_iterator itl=ns.begin();
  for(;itl!=ns.end();++itl,++iti) *iti=*itl;
  p_ = static_cast<void *>(&iv);
}

Parameter::Parameter(const std::initializer_list<string_t>& ss, const string_t& nm, const string_t& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_stringVector)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  p_ = static_cast<void *>(new std::vector<string_t>(ss));
}
Parameter::Parameter(const std::initializer_list<string_t>& ss, const string_t& nm, const Strings& snm)
: i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_stringVector)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  p_ = static_cast<void *>(new std::vector<string_t>(ss));
}

Parameter& Parameter::operator=(const std::initializer_list<real_t>& rs)
{
  deletePointer();
  p_ = static_cast<void *>(new std::vector<real_t>(rs));
  type_=_realVector;
  return *this;
}

Parameter& Parameter::operator=(const std::initializer_list<int>& ns)
{
  deletePointer();
  std::vector<int_t>& iv= *new std::vector<int_t>(ns.size());
  std::vector<int_t>::iterator iti=iv.begin();
  std::initializer_list<int>::const_iterator itl=ns.begin();
  for(;itl!=ns.end();++itl,++iti) *iti=*itl;
  p_ = static_cast<void *>(&iv);
  type_=_integerVector;
  return *this;
}

Parameter& Parameter::operator=(const std::initializer_list<number_t>& ns)
{
  deletePointer();
  std::vector<int_t>& iv= *new std::vector<int_t>(ns.size());
  std::vector<int_t>::iterator iti=iv.begin();
  std::initializer_list<number_t>::const_iterator itl=ns.begin();
  for(;itl!=ns.end();++itl,++iti) *iti=*itl;
  p_ = static_cast<void *>(&iv);
  type_=_integerVector;
  return *this;
}

Parameter& Parameter::operator=(const std::initializer_list<string_t>& ss)
{
  deletePointer();
  p_ = static_cast<void *>(new std::vector<string_t>(ss));
  type_=_stringVector;
  return *this;
}
#endif

/*
--------------------------------------------------------------------------------
 automatic cast operators of a Parameter
 to an int, a real, a complex, a string or a pointer
 used by syntax int i = param or real_t x=param, .....
--------------------------------------------------------------------------------
*/
Parameter::operator int_t()
{
  switch ( type_ )
  {
    case _integer: return i_;
    case _real:    return int_t(r_);  //may be dangerous
    default:       illegalOperation(words("value",type_), "cast to", "Int");
  }
  return 0;
}
Parameter::operator int()
{
  switch ( type_ )
  {
    case _integer: return i_;
    case _real:    return int(r_);  //may be dangerous
    default:       illegalOperation(words("value",type_), "cast to", "Int");
  }
  return 0;
}
Parameter::operator number_t()
{
  switch ( type_ )
  {
    case _integer: return number_t(i_);
    default:       illegalOperation(words("value",type_), "cast to", "Number");
  }
  return 0;
}

Parameter::operator bool()
{
  switch ( type_ )
  {
    case _bool: return b_;
    default:    illegalOperation(words("value",type_), "cast to", "bool");
  }
  return false;
}

Parameter::operator real_t()
{
  switch ( type_ )
  {
    case _integer: return real_t(i_);
    case _real:    return r_;
    case _complex: return real(c_);
    default:       illegalOperation(words("value",type_), "cast to", "Real");
  }
  return 0.;
}

Parameter::operator complex_t()
{
  switch ( type_ )
  {
    case _integer: return complex_t(real_t(i_), real_t(0));
    case _real:    return complex_t(r_, real_t(0));
    case _complex: return c_;
    default:       illegalOperation(words("value",type_), "cast to", "Complex");
  }
  return complex_t(0., 0.);
}

Parameter::operator string_t()
{
  switch ( type_ )
  {
    case _integer: return tostring(i_);
    case _real:    return tostring(r_);
    case _complex: return tostring(c_);
    case _string:  return s_;
    default:       illegalOperation(words("value",type_), "cast to", "String");
  }
  return "";
}

Parameter::operator const void* ()
{
  switch ( type_ )
  {
    case _pointer: return p_;
    default:       illegalOperation(words("value",type_), "cast to", "String");
  }
  return nullptr;
}

Parameter::operator std::vector<int_t>()
{
  if(p_==nullptr || type_ !=_integerVector) illegalOperation(words("value",type_), "cast to", "int vector");
  return *reinterpret_cast<const std::vector<int_t>*>(p_);
}

Parameter::operator std::vector<real_t>()
{
  if(p_==nullptr || type_ !=_realVector) illegalOperation(words("value",type_), "cast to", "real vector");
  return *reinterpret_cast<const std::vector<real_t>*>(p_);
}

Parameter::operator std::vector<complex_t>()
{
  if(p_==nullptr || type_ !=_complexVector) illegalOperation(words("value",type_), "cast to", "complex vector");
  return *reinterpret_cast<const std::vector<complex_t>*>(p_);
}

Parameter::operator std::vector<string_t>()
{
  if(p_==nullptr || type_ !=_stringVector) illegalOperation(words("value",type_), "cast to", "string vector");
  return *reinterpret_cast<const std::vector<string_t>*>(p_);
}

Parameter::operator std::vector<bool>()
{
  if(p_==nullptr || type_ !=_boolVector) illegalOperation(words("value",type_), "cast to", "bool vector");
  return *reinterpret_cast<const std::vector<bool>*>(p_);
}

Parameter::operator Matrix<real_t>()
{
  if(p_==nullptr || type_ !=_realMatrix)  illegalOperation(words("value",type_), "cast to", "real matrix");
  return *reinterpret_cast<const Matrix<real_t>*>(p_);
}

Parameter::operator Matrix<complex_t>()
{
  if(p_==nullptr || type_ !=_complexMatrix)  illegalOperation(words("value",type_), "cast to", "complex matrix");
  return *reinterpret_cast<const Matrix<complex_t>*>(p_);
}

Parameter::operator Point()
{
  if(p_==nullptr || type_ !=_pt)  illegalOperation(words("value",type_), "cast to", "Point");
  return *reinterpret_cast<const Point*>(p_);
}

Parameter::operator std::vector<Point>()
{
  if(p_!=nullptr && type_ ==_ptVector)  return *reinterpret_cast<const std::vector<Point>*>(p_);
  illegalOperation(words("value",type_), "cast to", "Point vector");
  return std::vector<Point>();  //fake return
}

Parameter::operator std::vector<number_t>()
{
  if(p_==nullptr || type_ !=_integerVector) illegalOperation(words("value",type_), "cast to", "number vector");
  const std::vector<int_t>& iv=*reinterpret_cast<const std::vector<int_t>*>(p_);
  std::vector<number_t> nv(iv.size());
  std::vector<int_t>::const_iterator iti=iv.begin();
  std::vector<number_t>::iterator itn=nv.begin();
  for(;iti!=iv.end();++iti,++itn) *itn=intToNum(*iti);
  return nv;
}

Parameter::operator Numbers()
{
  if(p_==nullptr || type_ !=_integerVector) illegalOperation(words("value",type_), "cast to", "Numbers");
  const std::vector<int_t>& iv=*reinterpret_cast<const std::vector<int_t>*>(p_);
  Numbers nv; nv.resize(iv.size());
  std::vector<int_t>::const_iterator iti=iv.begin();
  std::vector<number_t>::iterator itn=nv.begin();
  for(;iti!=iv.end();++iti,++itn) *itn=intToNum(*iti);
  return nv;
}

Parameter::operator Reals()
{
  if(p_==nullptr || type_ !=_realVector) illegalOperation(words("value",type_), "cast to", "Reals");
  return *reinterpret_cast<const Reals*>(p_);
}

Parameter::operator Complexes()
{
  if(p_==nullptr || type_ !=_complexVector) illegalOperation(words("value",type_), "cast to", "Complexes");
  return *reinterpret_cast<const Complexes*>(p_);
}

Parameter::operator Strings()
{
  if(p_==nullptr || type_ !=_stringVector) illegalOperation(words("value",type_), "cast to", "Strings");
  return *reinterpret_cast<const Strings*>(p_);
}

//Parameter::operator Vector<real_t>()
//{
//  if(p_==nullptr || type_ !=_realVector) illegalOperation(words("value",type_), "cast to", "real vector");
//  return Vector<real_t>(*reinterpret_cast<const std::vector<real_t>*>(p_));
//}
//
//Parameter::operator Vector<complex_t>()
//{
//  if(p_==nullptr || type_ !=_complexVector) illegalOperation(words("value",type_), "cast to", "complex vector");
//  return Vector<complex_t>(*reinterpret_cast<const std::vector<complex_t>*>(p_));
//}


//! delete pointer if type is a vector, a matrix, ....
void Parameter::deletePointer()
{
  if (p_==nullptr) return; //nothing to delete
  switch (type_ )
  {
    case _realVector:     delete reinterpret_cast<const std::vector<real_t>*>(p_); break;
    case _complexVector:  delete reinterpret_cast<const std::vector<complex_t>*>(p_); break;
    case _integerVector:  delete reinterpret_cast<const std::vector<int_t>*>(p_); break;
    case _stringVector:   delete reinterpret_cast<const std::vector<string_t>*>(p_); break;
    case _realMatrix:     delete reinterpret_cast<const Matrix<real_t>*>(p_); break;
    case _complexMatrix:  delete reinterpret_cast<const Matrix<complex_t>*>(p_); break;
    case _pt:             delete reinterpret_cast<const Point*>(p_); break;
    case _ptVector:       delete reinterpret_cast<const std::vector<Point>*>(p_); break;
    case _pointerClusterTreeFeDof: deleteClusterTreeFeDof(const_cast<void*>(p_)); break;
    case _pointerGeomDomain: deleteGeomDomain(const_cast<void*>(p_)); break;
    case _pointerFunction: deleteFunction(const_cast<void*>(p_)); break;
    case _pointerIntegrationMethod: deleteIntegrationMethod(const_cast<void*>(p_)); break;
    case _pointerIntegrationMethods: deleteIntegrationMethods(const_cast<void*>(p_)); break;
    case _pointerParametrization: deleteParametrization(const_cast<void*>(p_)); break;
    case _pointerSpline: deleteSpline(const_cast<void*>(p_)); break;
    case _pointerTermVectors: deleteTermVectors(const_cast<void*>(p_)); break;
    case _pointerTransformation: deleteTransformation(const_cast<void*>(p_)); break;
    default: break;
  }
}

// addition of a parameter if possible
Parameter& Parameter::operator+=(const Parameter& p)
{
  switch ( p.type_ )
  {
    case _integer:
      (*this) += p.i_;
      break;

    case _real:
      (*this) += p.r_;
      break;

    case _complex:
      (*this) += p.c_;
      break;

    case _string:
      (*this) += p.s_;
      break;

    case _pointer:
      illegalOperation("any", "+=", "pointer");
      break;

    default:
      break; // no operation, void assumed
  }
  return *this;
}

// substract a parameter if possible
Parameter& Parameter::operator-=(const Parameter& p)
{
  switch ( p.type_ )
  {
    case _integer:
      (*this) -= p.i_;
      break;

    case _real:
      (*this) -= p.r_;
      break;

    case _complex:
      (*this) -= p.c_;
      break;

    case _string:
      illegalOperation("any", "-=", "String");
      break;

    case _pointer:
      illegalOperation("any", "-=", "pointer");
      break;

    default:
      break; // no operation, void assumed
  }
  return *this;
}

// product by a parameter if possible
Parameter& Parameter::operator*=(const Parameter& p)
{
  switch ( p.type_ )
  {
    case _integer:
      (*this) *= p.i_;
      break;

    case _real:
      (*this) *= p.r_;
      break;

    case _complex:
      (*this) *= p.c_;
      break;

    case _string:
      illegalOperation("any", "*=", "String");
      break;

    case _pointer:
      illegalOperation("any", "*=", "pointer");
      break;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

// divide by a parameter if possible
Parameter& Parameter::operator/=(const Parameter& p)
{
  switch ( p.type_ )
  {
    case _integer:
      (*this) /= p.i_;
      break;

    case _real:
      (*this) /= p.r_;
      break;

    case _complex:
      (*this) /= p.c_;
      break;

    case _string:
      illegalOperation("any", "/=", "String");
      break;

    case _pointer:
      illegalOperation("any", "/=", "pointer");
      break;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

// addition of an integer, a real, a complex or a string if possible
Parameter& Parameter::operator+=(const int_t i)
{
  switch ( type_ )
  {
    case _integer:
      i_ += i;
      break;

    case _real:
      r_ += i;
      break;

    case _complex:
      c_ += complex_t(real_t(i), 0.);
      break;

    case _string:
      illegalOperation("String", "+=", "Int");
      break;

    case _pointer:
      illegalOperation("pointer", "+=", "Int");
      break;

    case _none:
      type_ = _integer;
      i_ = i;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator+=(const int i)
{
  switch ( type_ )
  {
    case _integer:
      i_ += i;
      break;

    case _real:
      r_ += i;
      break;

    case _complex:
      c_ += complex_t(real_t(i), 0.);
      break;

    case _string:
      illegalOperation("String", "+=", "Int");
      break;

    case _pointer:
      illegalOperation("pointer", "+=", "Int");
      break;

    case _none:
      type_ = _integer;
      i_ = i;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator+=(const number_t i)
{
  switch ( type_ )
  {
    case _integer:
      i_ += i;
      break;

    case _real:
      r_ += i;
      break;

    case _complex:
      c_ += complex_t(real_t(i), 0.);
      break;

    case _string:
      illegalOperation("String", "+=", "Number");
      break;

    case _pointer:
      illegalOperation("pointer", "+=", "Number");
      break;

    case _none:
      type_ = _integer;
      i_ = i;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator+=(const real_t r)
{
  switch ( type_ )
  {
    case _integer:
      r_ = i_ + r;
      type_ = _real;
      break;

    case _real:
      r_ += r;
      break;

    case _complex:
      c_ += complex_t(r, 0.);
      break;

    case _string:
      illegalOperation("String", "+=", "Real");
      break;

    case _pointer:
      illegalOperation("pointer", "+=", "Real");
      break;

    case _none:
      type_ = _real;
      r_ = r;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator+=(const complex_t& c)
{
  switch ( type_ )
  {
    case _integer:
      c_ = complex_t(real_t(i_), 0.) + c;
      type_ = _complex;
      break;

    case _real:
      c_ = complex_t(r_, 0) + c;
      type_ = _complex;
      break;

    case _complex:
      c_ += c;
      break;

    case _string:
      illegalOperation("String", "+=", "Complex");
      break;

    case _pointer:
      illegalOperation("pointer", "+=", "Complex");
      break;

    case _none:
      type_ = _complex;
      c_ = c;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator+=(const string_t& s)
{
  switch ( type_ )
  {
    case _integer:
      illegalOperation("Int", "+=", "String");
      break;

    case _real:
      illegalOperation("Real", "+=", "String");
      break;

    case _complex:
      illegalOperation("Complex", "+=", "String");
      break;

    case _string:
      s_ += s;
      break;

    case _pointer:
      illegalOperation("pointer", "+=", "String");
      break;

    case _none:
      type_ = _string;
      s_ = s;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator+=(const char* s)
{
  switch ( type_ )
  {
    case _integer:
      illegalOperation("Int", "+=", "String");
      break;

    case _real:
      illegalOperation("Real", "+=", "String");
      break;

    case _complex:
      illegalOperation("Complex", "+=", "String");
      break;

    case _string:
      s_ += string_t(s);
      break;

    case _pointer:
      illegalOperation("pointer", "+=", "String");
      break;

    case _none:
      type_ = _string;
      s_ = s;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

// substract an integer, a real or a complex if possible
Parameter& Parameter::operator-=(const int_t i)
{
  switch ( type_ )
  {
    case _integer:
      i_ -= i;
      break;

    case _real:
      r_ -= i;
      break;

    case _complex:
      c_ -= complex_t(real_t(i), 0.);
      break;

    case _string:
      illegalOperation("String", "-=", "Int");
      break;

    case _pointer:
      illegalOperation("pointer", "-=", "Int");
      break;

    case _none:
      type_ = _integer;
      i_ = i;

    default:
      break; // no operation, void assumed
  }

  return *this;
}
Parameter& Parameter::operator-=(const int i)
{
  switch ( type_ )
  {
    case _integer:
      i_ -= i;
      break;

    case _real:
      r_ -= i;
      break;

    case _complex:
      c_ -= complex_t(real_t(i), 0.);
      break;

    case _string:
      illegalOperation("String", "-=", "Int");
      break;

    case _pointer:
      illegalOperation("pointer", "-=", "Int");
      break;

    case _none:
      type_ = _integer;
      i_ = i;

    default:
      break; // no operation, void assumed
  }

  return *this;
}
Parameter& Parameter::operator-=(const number_t i)
{
  switch ( type_ )
  {
    case _integer:
      i_ -= i;
      break;

    case _real:
      r_ -= i;
      break;

    case _complex:
      c_ -= complex_t(real_t(i), 0.);
      break;

    case _string:
      illegalOperation("string", "-=", "Number");
      break;

    case _pointer:
      illegalOperation("pointer", "-=", "Number");
      break;

    case _none:
      type_ = _integer;
      i_ = i;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator-=(const real_t r)
{
  switch ( type_ )
  {
    case _integer:
      r_ = i_ - r;
      type_ = _real;
      break;

    case _real:
      r_ -= r;
      break;

    case _complex:
      c_ -= complex_t(r, 0.);
      break;

    case _string:
      illegalOperation("String", "-=", "Real");
      break;

    case _pointer:
      illegalOperation("pointer", "-=", "Real");
      break;

    case _none:
      type_ = _real;
      r_ = r;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator-=(const complex_t& c)
{
  switch ( type_ )
  {
    case _integer:
      c_ = complex_t(real_t(i_), 0.) - c;
      type_ = _complex;
      break;

    case _real:
      c_ = complex_t(r_, 0.) - c;
      type_ = _complex;
      break;

    case _complex:
      c_ -= c;
      break;

    case _string:
      illegalOperation("String", "-=", "Complex");
      break;

    case _pointer:
      illegalOperation("pointer", "-=", "Complex");
      break;

    case _none:
      type_ = _complex;
      c_ = c;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

// product by an integer, a real or a complex if possible
Parameter& Parameter::operator*=(const int_t i)
{
  switch ( type_ )
  {
    case _integer:
      i_ *= i;
      break;

    case _real:
      r_ *= i;
      break;

    case _complex:
      c_ *= complex_t(real_t(i), 0.);
      break;

    case _string:
      illegalOperation("String", "*=", "Int");
      break;

    case _pointer:
      illegalOperation("pointer", "*=", "Int");
      break;

    case _none:
      type_ = _integer;
      i_ = i;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator*=(const int i)
{
  switch ( type_ )
  {
    case _integer:
      i_ *= i;
      break;

    case _real:
      r_ *= i;
      break;

    case _complex:
      c_ *= complex_t(real_t(i), 0.);
      break;

    case _string:
      illegalOperation("String", "*=", "Int");
      break;

    case _pointer:
      illegalOperation("pointer", "*=", "Int");
      break;

    case _none:
      type_ = _integer;
      i_ = i;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator*=(const number_t i)
{
  switch ( type_ )
  {
    case _integer:
      i_ *= i;
      break;

    case _real:
      r_ *= i;
      break;

    case _complex:
      c_ *= complex_t(real_t(i), 0.);
      break;

    case _string:
      illegalOperation("String", "*=", "Number");
      break;

    case _pointer:
      illegalOperation("pointer", "*=", "Number");
      break;

    case _none:
      type_ = _integer;
      i_ = i;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator*=(const real_t r)
{
  switch ( type_ )
  {
    case _integer:
      r_ = i_ * r;
      type_ = _real;
      break;

    case _real:
      r_ *= r;
      break;

    case _complex:
      c_ *= complex_t(r, 0.);
      break;

    case _string:
      illegalOperation("String", "*=", "Real");
      break;

    case _pointer:
      illegalOperation("pointer", "*=", "Real");
      break;

    case _none:
      type_ = _real;
      r_ = r;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator*=(const complex_t& c)
{
  switch ( type_ )
  {
    case _integer:
      c_ = c * complex_t(real_t(i_), 0.);
      type_ = _complex;
      break;

    case _real:
      c_ = c * complex_t(r_, 0.);
      type_ = _complex;
      break;

    case _complex:
      c_ *= c;
      break;

    case _string:
      illegalOperation("String", "*=", "Complex");
      break;

    case _pointer:
      illegalOperation("pointer", "*=", "Complex");
      break;

    case _none:
      type_ = _complex;
      c_ = c;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

// divide by an integer, a real or a complex if possible
Parameter& Parameter::operator/=(const int_t i)
{
  if (i == 0)
  {
    illegalDivision(); // divide by 0
  }

  switch ( type_ )
  {
    case _integer: // the result may be real_t, real_t casting is enforced

      if ( (i_ - (i_ / i)*i) == 0 )
      {
        i_ /= i;
      }
      else
      {
        r_ = real_t(i_) / real_t(i);
        type_ = _real;
      }

      break;

    case _real:
      r_ /= i;
      break;

    case _complex:
      c_ /= complex_t(real_t(i), 0.);
      break;

    case _string:
      illegalOperation("String", "/=", "Int");
      break;

    case _pointer:
      illegalOperation("pointer", "/=", "Int");
      break;

    case _none:
      type_ = _integer;
      i_ = i;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator/=(const int i)
{
  if (i == 0)
  {
    illegalDivision(); // divide by 0
  }

  switch ( type_ )
  {
    case _integer: // the result may be real_t, real_t casting is enforced

      if ( (i_ - (i_ / i)*i) == 0 )
      {
        i_ /= i;
      }
      else
      {
        r_ = real_t(i_) / real_t(i);
        type_ = _real;
      }

      break;

    case _real:
      r_ /= i;
      break;

    case _complex:
      c_ /= complex_t(real_t(i), 0.);
      break;

    case _string:
      illegalOperation("String", "/=", "Int");
      break;

    case _pointer:
      illegalOperation("pointer", "/=", "Int");
      break;

    case _none:
      type_ = _integer;
      i_ = i;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator/=(const number_t i)
{
  if (i == 0)
  {
    illegalDivision(); // divide by 0
  }

  switch ( type_ )
  {
    case _integer: // the result may be real_t, real_t casting is enforced

      if ( (i_ - (i_ / i)*i) == 0 )
      {
        i_ /= i;
      }
      else
      {
        r_ = real_t(i_) / real_t(i);
        type_ = _real;
      }

      break;

    case _real:
      r_ /= i;
      break;

    case _complex:
      c_ /= complex_t(real_t(i), 0.);
      break;

    case _string:
      illegalOperation("String", "/=", "Number");
      break;

    case _pointer:
      illegalOperation("pointer", "/=", "Number");
      break;

    case _none:
      type_ = _integer;
      i_ = i;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator/=(const real_t r)
{
  if ( std::abs(r) <= theZeroThreshold)
  {
    illegalDivision(); // divide by 0
  }

  switch ( type_ )
  {
    case _integer:
      r_ = i_ / r;
      type_ = _real;
      break;

    case _real:
      r_ /= r;
      break;

    case _complex:
      c_ /= complex_t(r, 0.);
      break;

    case _string:
      illegalOperation("String", "/=", "Real");
      break;

    case _pointer:
      illegalOperation("pointer", "/=", "Real");
      break;

    case _none:
      type_ = _real;
      r_ = r;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

Parameter& Parameter::operator/=(const complex_t& c)
{
  if ( std::abs(c) <= theZeroThreshold)
  {
    illegalDivision(); // divide by 0
  }

  switch ( type_ )
  {
    case _integer:
      c_ = complex_t(real_t(i_), 0.) / c;
      type_ = _complex;
      break;

    case _real:
      c_ = complex_t(r_, 0.) / c;
      type_ = _complex;
      break;

    case _complex:
      c_ /= c;
      break;

    case _string:
      illegalOperation("String", "/=", "Complex");
      break;

    case _pointer:
      illegalOperation("pointer", "/=", "Complex");
      break;

    case _none:
      type_ = _complex;
      c_ = c;

    default:
      break; // no operation, void assumed
  }

  return *this;
}

/*
--------------------------------------------------------------------------------
 comparison operators
--------------------------------------------------------------------------------
*/
// compare to a parameter
bool Parameter::operator==(const Parameter& p)
{
  switch ( p.type_ )
  {
    case _integer:
      return ( (*this) == p.i_ );
      break;

    case _bool:
      return ( (*this) == p.b_ );
      break;

    case _real:
      return ( (*this) == p.r_ );
      break;

    case _complex:
      return ( (*this) == p.c_ );
      break;

    case _string:
      return ( (*this) == p.s_ );
      break;

    case _pointer:
      return ( p.type_ == _pointer && p_ == p.p_ );
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

// compare to an _integer
bool Parameter::operator==(const int_t i)
{
  switch ( type_ )
  {
    case _integer:
      return ( i_ == i );
      break;

    case _real:
      return ( std::abs(r_ - real_t(i)) <= theZeroThreshold);
      break; // comparison with real

    case _complex:
      return ( std::abs(c_ - complex_t(i, 0)) <= theZeroThreshold);
      break;// comparison with complex

    case _string:
      return ( s_ == tostring(i) );
      break; // comparison with string

    case _pointer:
      illegalOperation("pointer", "==", "Int");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

bool Parameter::operator==(const int i)
{
  switch ( type_ )
  {
    case _integer:
      return ( i_ == static_cast<int_t>(i) );
      break;

    case _real:
      return ( std::abs(r_ - real_t(i)) <= theZeroThreshold);
      break; // comparison with real

    case _complex:
      return ( std::abs(c_ - complex_t(i, 0)) <= theZeroThreshold);
      break;// comparison with complex

    case _string:
      return ( s_ == tostring(i) );
      break; // comparison with string

    case _pointer:
      illegalOperation("pointer", "==", "Int");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

bool Parameter::operator==(const bool b)
{
  switch ( type_ )
  {
    case _bool:
      return ( b_ == static_cast<bool>(b) );
      break;

    case _integer:
      illegalOperation("integer", "==", "bool");
      break; // comparison with integer

    case _real:
      illegalOperation("real", "==", "bool");
      break; // comparison with real

    case _complex:
      illegalOperation("complex", "==", "bool");
      break;// comparison with complex

    case _string:
      illegalOperation("string", "==", "bool");
      break; // comparison with string

    case _pointer:
      illegalOperation("pointer", "==", "bool");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

bool Parameter::operator==(const number_t i)
{
  switch ( type_ )
  {
    case _integer:
      return ( i_ == static_cast<int_t>(i) );
      break;

    case _real:
      return ( std::abs(r_ - real_t(i)) <= theZeroThreshold);
      break; // comparison with real

    case _complex:
      return ( std::abs(c_ - complex_t(i, 0)) <= theZeroThreshold);
      break;// comparison with complex

    case _string:
      return ( s_ == tostring(i) );
      break; // comparison with string

    case _pointer:
      illegalOperation("pointer", "==", "Number");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

// compare to a _real
bool Parameter::operator==(const real_t r)
{
  switch ( type_ )
  {
    case _integer:
      return ( std::abs(i_ - r) <= theZeroThreshold);
      break; // comparison with real

    case _real:
      return ( std::abs(r_ - r) <= theZeroThreshold);
      break;

    case _complex:
      return ( std::abs(c_ - complex_t(r, 0)) <= theZeroThreshold);
      break; // comparison with complex

    case _string:
      return ( s_ == tostring(r) );
      break; // comparison with string

    case _pointer:
      illegalOperation("pointer", "==", "Real");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

// compare to a _complex
bool Parameter::operator==(const complex_t& c)
{
  switch ( type_ )
  {
    case _integer:
      return ( std::abs(complex_t(i_, 0) - c) <= theZeroThreshold);
      break;

    case _real:
      return ( std::abs(complex_t(r_, 0) - c) <= theZeroThreshold);
      break;

    case _complex:
      return ( std::abs(c_ - c) <= theZeroThreshold);
      break;

    case _string:
      return ( s_ == tostring(c) );
      break;

    case _pointer:
      illegalOperation("pointer", "==", "Complex");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

// compare to a string
bool Parameter::operator==(const string_t& s)
{
  switch ( type_ )
  {
    case _integer:
      return ( tostring(i_) == s );
      break;

    case _real:
      return ( tostring(r_) == s );
      break;

    case _complex:
      return ( tostring(c_) == s );
      break;

    case _string:
      return ( s_ == s );
      break;

    case _pointer:
      illegalOperation("pointer", "==", "String");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

// compare to a char* string
bool Parameter::operator==(const char* s)
{
  switch ( type_ )
  {
    case _integer:
      return ( tostring(i_) == string_t(s) );
      break;

    case _real:
      return ( tostring(r_) == string_t(s) );
      break;

    case _complex:
      return ( tostring(c_) == string_t(s) );
      break;

    case _string:
      return ( s_ == string_t(s) );
      break;

    case _pointer:
      illegalOperation("pointer", "==", "char *");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

// compare to a parameter
bool Parameter::operator>(const Parameter& p)
{
  switch ( p.type_ )
  {
    case _integer:
      return ( (*this) > p.i_ );
      break;

    case _real:
      return ( (*this) > p.r_ );
      break;

    case _complex:
      illegalOperation("Complex", ">", "Parameter");
      break;

    case _string:
      return ( (*this) > p.s_ );
      break;

    case _pointer:
      illegalOperation("pointer", ">", "Parameter");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

// compare to an _integer
bool Parameter::operator>(const int_t i)
{
  switch ( type_ )
  {
    case _integer:
      return ( i_ > i );
      break;

    case _real:
      return ( r_ > real_t(i) );
      break;

    case _complex:
      illegalOperation("Complex", ">", "Int");
      break;

    case _string:
      illegalOperation("String", ">", "Int");
      break;

    case _pointer:
      illegalOperation("pointer", ">", "Int");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

bool Parameter::operator>(const int i)
{
  switch ( type_ )
  {
    case _integer:
      return ( i_ > i );
      break;

    case _real:
      return ( r_ > real_t(i) );
      break;

    case _complex:
      illegalOperation("Complex", ">", "Int");
      break;

    case _string:
      illegalOperation("String", ">", "Int");
      break;

    case _pointer:
      illegalOperation("pointer", ">", "Int");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

bool Parameter::operator>(const number_t i)
{
  switch ( type_ )
  {
    case _integer:
      return (i_ < 0 ?  false: static_cast<number_t>(i_) > i );
      break;

    case _real:
      return ( r_ > real_t(i) );
      break;

    case _complex:
      illegalOperation("Complex", ">", "Number");
      break;

    case _string:
      illegalOperation("String", ">", "Number");
      break;

    case _pointer:
      illegalOperation("pointer", ">", "Number");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

// compare to a _real
bool Parameter::operator>(const real_t r)
{
  switch ( type_ )
  {
    case _integer:
      return ( real_t(i_) > r);
      break;

    case _real:
      return ( r_ > r );
      break;

    case _complex:
      illegalOperation("Complex", ">", "Real");
      break;

    case _string:
      illegalOperation("String", ">", "Real");
      break;

    case _pointer:
      illegalOperation("pointer", ">", "Real");
      break;

    default:
      break; // no type, false by default
  }

  return false;
}

// compare to a complex
bool Parameter::operator>(const complex_t& c)
{
  illegalOperation("any", ">", "Complex");
  return true;
}

// compare to a string
bool Parameter::operator>(const string_t& s)
{
  illegalOperation("any", ">", "String");
  return true;
}

/*
--------------------------------------------------------------------------------
 error handling utilities
--------------------------------------------------------------------------------
*/
void Parameter::illegalOperation(const string_t& t1, const string_t& op, const string_t& t2) const
{
  error("param_badop", (t1 + " " + op + " " + t2));
}
void Parameter::illegalOperation2(const string_t& op, const string_t& t) const
{
  error("param_badop", (op + "(" + t + ")"));
}
void Parameter::undefinedParameter(const string_t& t) const
{
  error("param_undef", t);
}
void Parameter::illegalDivision() const
{
  error("param_divby0");
}
/*
--------------------------------------------------------------------------------
 input/ouput utilities
--------------------------------------------------------------------------------
*/

// flux insertion (write)
std::ostream& operator<<(std::ostream& os, const Parameter& obj)
{
  os << obj.name_;
  if (obj.shortnames_.size() > 0)
  {
    os << ", aliases: " << join(obj.shortnames_, ",");
  }

  switch ( obj.type_ )
  {
    case _integer:
    {
      os << " (Int/Number) = " << obj.i_ << " ";
      break;
    }
    case _bool:
    {
      os << " (bool) = " << obj.b_ << " ";
      break;
    }
    case _real:
    {
      os << " (Real) = " << obj.r_ <<  " ";
      break;
    }
    case _complex:
    {
      os << " (Complex) = " << obj.c_ << " ";
      break;
    }
    case _string:
    {
      os << " (String) = " << obj.s_ <<  " ";
      break;
    }
    case _pt:
    {
      os << " (Point) = " << *reinterpret_cast<const Point*>(obj.p_) <<  " ";
      break;
    }
    case _pointer:
    {
      os << " (pointer) = " << words("not printed") <<  " ";
      break;
    }
    case _integerVector:
    {
      const std::vector<int_t>* data=reinterpret_cast<const std::vector<int_t>*>(obj.p_);
      os << " (Ints/Numbers size " << data->size() << ") = " << *data <<  " ";
      break;
    }
    case _boolVector:
    {
      const std::vector<bool>* data=reinterpret_cast<const std::vector<bool>*>(obj.p_);
      os << " (bools size " << data->size() << ") = " << *data <<  " ";
      break;
    }
    case _realVector:
    {
      const std::vector<real_t>* data=reinterpret_cast<const std::vector<real_t>*>(obj.p_);
      os << " (Reals size " << data->size() << ") = " << *data <<  " ";
      break;
    }
    case _complexVector:
    {
      const std::vector<complex_t>* data=reinterpret_cast<const std::vector<complex_t>*>(obj.p_);
      os << " (Complexes size " << data->size() << ") = " << *data <<  " ";
      break;
    }
    case _stringVector:
    {
      const std::vector<string_t>* data=reinterpret_cast<const std::vector<string_t>*>(obj.p_);
      os << " (Strings size " << data->size() << ") = " << *data <<  " ";
      break;
    }
    case _ptVector:
    {
      os << " (Points) = " << *reinterpret_cast<const std::vector<Point>*>(obj.p_) <<  " ";
      break;
    }
    case _integerMatrix:
    {
      const Matrix<int_t>* data=reinterpret_cast<const Matrix<int_t>*>(obj.p_);
      os << " (IntegerMatrix size " << data->numberOfRows() << "x" << data->numberOfCols() << ") = " << *data <<  " ";
      break;
    }
    case _boolMatrix:
    {
      const Matrix<bool>* data=reinterpret_cast<const Matrix<bool>*>(obj.p_);
      os << " (BoolMatrix size " << data->numberOfRows() << "x" << data->numberOfCols() << ") = " << *data <<  " ";
      break;
    }
    case _realMatrix:
    {
      const Matrix<real_t>* data=reinterpret_cast<const Matrix<real_t>*>(obj.p_);
      os << " (RealMatrix size " << data->numberOfRows() << "x" << data->numberOfCols() << ") = " << *data <<  " ";
      break;
    }
    case _complexMatrix:
    {
      const Matrix<complex_t>* data=reinterpret_cast<const Matrix<complex_t>*>(obj.p_);
      os << " (ComplexMatrix size " << data->numberOfRows() << "x" << data->numberOfCols() << ") = " << *data <<  " ";
      break;
    }
    case _stringMatrix:
    {
      const Matrix<string_t>* data=reinterpret_cast<const Matrix<string_t>*>(obj.p_);
      os << " (StringMatrix size " << data->numberOfRows() << "x" << data->numberOfCols() << ") = " << *data <<  " ";
      break;
    }
    case _ptMatrix:
    {
      const Matrix<Point>* data=reinterpret_cast<const Matrix<Point>*>(obj.p_);
      os << " (PointMatrix size " << data->numberOfRows() << "x" << data->numberOfCols() << ") = " << *data <<  " ";
      break;
    }
    case _pointerClusterTreeFeDof:
    {
      os << " (ClusterTree<FeDof>) " << " ";
      break;
    }
    case _pointerGeomDomain:
    {
      os << " (Domain) " << " ";
      break;
    }
    case _pointerFunction:
    {
      os << " (Function) " << " ";
      break;
    }
    case _pointerIntegrationMethod:
    {
      os << " (IntegrationMethod) " << " ";
      break;
    }
    case _pointerIntegrationMethods:
    {
      os << " (IntegrationMethods) " << " ";
      break;
    }
    case _pointerTermVectors:
    {
      os << " (TermVectors) " << " ";
      break;
    }
    default:
    {
      os << " (untyped) " <<  " ";
      break;
    }
  }
  return os;
}

void print(const Parameter& p)
{
  thePrintStream << p;
}

// flux extraction(read)
std::istream& operator>>(std::istream& is, Parameter& p)
{
  // reading string
  string_t s;
  is >> s;

  // analyse string to identify its type
  switch ( get_value_type(s) )
  {
    case _integer:
      p.i_ = stringto<int_t>(s);
      p.type_ = _integer;
      break;

    case _bool:
      p.i_ = stringto<bool>(s);
      p.type_ = _bool;
      break;

    case _real:
      p.r_ = stringto<real_t>(s);
      p.type_ = _real;
      break;

    case _complex:
      p.c_ = stringto<complex_t>(s);
      p.type_ = _complex;
      break;

    case _string:
      p.s_ = s;
      p.type_ = _string;
      break;

    case _pointer:
      p.p_ = stringto<const void*>(s);
      p.type_ = _pointer;
      break;

    default:
      break;
  }

  return is;
}

std::ostream& operator<<(std::ostream& os, const std::set<ParameterKey>& pks)
{
  std::set<ParameterKey>::const_iterator it;
  os << "{";
  for (it=pks.begin(); it != pks.end(); ++it) { os << " " << *it; }
  os << " }" << std::endl;
  return os;
}
// utility: return the type of a string
ValueType get_value_type(const string_t& s)
{
  // not robust, has to be upgraded
  // s is a "true string" if one alpha character appears except + - . e ( ) ,
  bool numeric(true);
  int l(s.size()), i(0);
  char c;

  while ( i < l && numeric )
  {
    c = s[i];
    i++;
    // Ascii codes | 40 41 42 43 44 45 46 47 48 ...57 | 65 ... 90 | 97 ... 122
    // | ( ) * + , - . / 0 ... 9 | A ... Z | a ... z
    numeric = (c == 40) || (c == 41) || (c > 42 && c < 47) || (c > 47 && c < 57 ) || (c == 'e');
  }

  if ( numeric )
  {
    std::istringstream is(s);
    complex_t z;
    is >> z;

    if ( std::abs(z.imag()) <= theZeroThreshold) // _real
    {
      real_t r = z.real();

      if ( int(r) == r )
      {
        return _integer;
      }
      else
      {
        return _real;
      }
    }
    else
    {
      return _complex;
    }
  }

  return _string;
}
/*
--------------------------------------------------------------------------------
extern casting operator
--------------------------------------------------------------------------------
*/
int_t integer(const Parameter& p) // cast to int_t
{
  switch ( p.type() )
  {
    case _integer:
      return p.get_i();
      break;

    case _real:
      return int(p.get_r());
      break;

    case _complex:
      return int(p.get_c().real());
      break;

    case _string:
      stringto<int>(p.get_s());
      break; // try to convert in int

    case _pointer:
      p.illegalOperation("pointer", "cast to", "Int");
      break;

    default:
      break;
  }

  return 0;
}

real_t real(const Parameter& p)
// cast to real_t
{
  switch ( p.type() )
  {
    case _integer:
      return real_t(p.get_i());
      break;

    case _real:
      return p.get_r();
      break;

    case _complex:
      return p.get_c().real();
      break;

    case _string:
      return stringto<real_t>(p.get_s());
      break; // try to convert in real_t

    case _pointer:
      p.illegalOperation("pointer", "cast to", "Real");
      break;

    default:
      break;
  }

  return 0.;
}

complex_t cmplx(const Parameter& p) // cast to complex_t
{
  switch ( p.type() )
  {
    case _integer:
      return complex_t(real_t(p.get_i()), 0.);
      break;

    case _real:
      return complex_t(p.get_r(), 0.);
      break;

    case _complex:
      return p.get_c();
      break;

    case _string:
      stringto<complex_t>(p.get_s());
      break; // try to convert in complex_t

    case _pointer:
      p.illegalOperation("pointer", "cast to", "Complex");
      break;

    default:
      break;
  }

  return complex_t(0);
}

string_t str(const Parameter& p) // cast to string
{
  std::stringstream ss;
  string_t s("");

  switch ( p.type() )
  {
    case _integer:
    {
      ss << p.get_i();
      ss >> s;
      break;
    }
    case _real:
    {
      ss << p.get_r();
      ss >> s;
      break;
    }
    case _complex:
    {
      ss << p.get_c();
      ss >> s;
      break;
    }
    case _string:
    {
      s = p.get_s();
      break;
    }
    case _pointer:
    {
      ss << p.get_p();
      ss >> s;
      break;
    }
    case _none:
    {
      s = "";
      break;
    }
    default: break;
  }

  return s;
}

const void* pointer(const Parameter& p) // cast to string
{
  const void* q = nullptr;

  switch ( p.type() )
  {
    case _integer: p.illegalOperation("Int", "cast to", "pointer"); break;
    case _real: p.illegalOperation("Real", "cast to", "pointer"); break;
    case _complex: p.illegalOperation("Complex", "cast to", "pointer"); break;
    case _string: p.illegalOperation("String", "cast to", "pointer"); break;
    case _pointer: q = p.get_p(); break;
    case _none: break;
    default: break;
  }

  return q;
}

/*
--------------------------------------------------------------------------------
 extern algebraic operators
--------------------------------------------------------------------------------
*/
// addition
Parameter operator+(const Parameter& p1, const Parameter& p2)
{
  Parameter res(p1);
  res += p2;
  return res;
}

Parameter operator+(const Parameter& p, const int_t v)
{
  Parameter res(p);
  res += v;
  return res;
}

Parameter operator+(const Parameter& p, const int v)
{
  Parameter res(p);
  res += v;
  return res;
}

Parameter operator+(const Parameter& p, const number_t v)
{
  Parameter res(p);
  res += v;
  return res;
}

Parameter operator+(const Parameter& p, const real_t v)
{
  Parameter res(p);
  res += v;
  return res;
}

Parameter operator+(const Parameter& p, const complex_t& v)
{
  Parameter res(p);
  res += v;
  return res;
}

Parameter operator+(const Parameter& p, const string_t& v)
{
  Parameter res(p);
  res += v;
  return res;
}

Parameter operator+(const Parameter& p, const char* v)
{
  Parameter res(p);
  res += string_t(v);
  return res;
}

Parameter operator+(const int_t v, const Parameter& p)
{
  Parameter res(p);
  res += v;
  return res;
}

Parameter operator+(const int v, const Parameter& p)
{
  Parameter res(p);
  res += v;
  return res;
}

Parameter operator+(const number_t v, const Parameter& p)
{
  Parameter res(p);
  res += v;
  return res;
}

Parameter operator+(const real_t v, const Parameter& p)
{
  Parameter res(p);
  res += v;
  return res;
}

Parameter operator+(const complex_t& v, const Parameter& p)
{
  Parameter res(p);
  res += v;
  return res;
}

Parameter operator+(const string_t& v, const Parameter& p)
{
  Parameter res(p);
  res += v;
  return res;
}

Parameter operator+(const char* v, const Parameter& p)
{
  Parameter res(p);
  res += string_t(v);
  return res;
}

//difference
Parameter operator-(const Parameter& p)
{
  Parameter r(p);

  switch ( p.type() )
  {
    case _integer: r = -p.get_i(); break;
    case _real: r = -p.get_r(); break;
    case _complex: r = -p.get_c(); break;
    case _string: p.illegalOperation("parameter", "minus", "String"); break;
    case _pointer: p.illegalOperation("parameter", "minus", "pointer"); break;
    case _none: break;
    default: break;
  }

  return r;
}

Parameter operator-(const Parameter& p1, const Parameter& p2)
{
  Parameter res(p1);
  res -= p2;
  return res;
}

Parameter operator-(const Parameter& p, const int_t v)
{
  Parameter res(p);
  res -= v;
  return res;
}

Parameter operator-(const Parameter& p, const int v)
{
  Parameter res(p);
  res -= v;
  return res;
}

Parameter operator-(const Parameter& p, const number_t v)
{
  Parameter res(p);
  res -= v;
  return res;
}

Parameter operator-(const Parameter& p, const real_t v)
{
  Parameter res(p);
  res -= v;
  return res;
}

Parameter operator-(const Parameter& p, const complex_t& v)
{
  Parameter res(p);
  res -= v;
  return res;
}

Parameter operator-(const int_t v, const Parameter& p)
{
  Parameter res(-p);
  res += v;
  return res;
}

Parameter operator-(const int v, const Parameter& p)
{
  Parameter res(-p);
  res += v;
  return res;
}

Parameter operator-(const number_t v, const Parameter& p)
{
  Parameter res(-p);
  res += v;
  return res;
}

Parameter operator-(const real_t v, const Parameter& p)
{
  Parameter res(-p);
  res += v;
  return res;
}

Parameter operator-(const complex_t& v, const Parameter& p)
{
  Parameter res(-p);
  res += v;
  return res;
}

//product
Parameter operator*(const Parameter& p1, const Parameter& p2)
{
  Parameter res(p1);
  res *= p2;
  return res;
}

Parameter operator*(const Parameter& p, const int_t v)
{
  Parameter res(p);
  res *= v;
  return res;
}

Parameter operator*(const Parameter& p, const int v)
{
  Parameter res(p);
  res *= v;
  return res;
}

Parameter operator*(const Parameter& p, const number_t v)
{
  Parameter res(p);
  res *= v;
  return res;
}

Parameter operator*(const Parameter& p, const real_t v)
{
  Parameter res(p);
  res *= v;
  return res;
}

Parameter operator*(const Parameter& p, const complex_t& v)
{
  Parameter res(p);
  res *= v;
  return res;
}

Parameter operator*(const int_t v, const Parameter& p)
{
  Parameter res(p);
  res *= v;
  return res;
}

Parameter operator*(const int v, const Parameter& p)
{
  Parameter res(p);
  res *= v;
  return res;
}

Parameter operator*(const number_t v, const Parameter& p)
{
  Parameter res(p);
  res *= v;
  return res;
}

Parameter operator*(const real_t v, const Parameter& p)
{
  Parameter res(p);
  res *= v;
  return res;
}

Parameter operator*(const complex_t& v, const Parameter& p)
{
  Parameter res(p);
  res *= v;
  return res;
}

//division
Parameter operator/(const Parameter& p1, const Parameter& p2)
{
  Parameter res(p1);
  res /= p2;
  return res;
}

Parameter operator/(const Parameter& p, const int_t v)
{
  Parameter res(p);
  res /= v;
  return res;
}

Parameter operator/(const Parameter& p, const int v)
{
  Parameter res(p);
  res /= v;
  return res;
}

Parameter operator/(const Parameter& p, const number_t v)
{
  Parameter res(p);
  res /= v;
  return res;
}

Parameter operator/(const Parameter& p, const real_t v)
{
  Parameter res(p);
  res /= v;
  return res;
}

Parameter operator/(const Parameter& p, const complex_t& v)
{
  Parameter res(p);
  res /= v;
  return res;
}

Parameter operator/(const int_t v, const Parameter& p)
{
  Parameter res(v);
  res /= p;
  return res;
}

Parameter operator/(const int v, const Parameter& p)
{
  Parameter res(v);
  res /= p;
  return res;
}

Parameter operator/(const number_t v, const Parameter& p)
{
  Parameter res(v);
  res /= p;
  return res;
}

Parameter operator/(const real_t v, const Parameter& p)
{
  Parameter res(v);
  res /= p;
  return res;
}

Parameter operator/(const complex_t& v, const Parameter& p)
{
  Parameter res(v);
  res /= p;
  return res;
}

/*
--------------------------------------------------------------------------------
extern numerical function
--------------------------------------------------------------------------------
*/
// Parameter abs(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = int_t(std::abs(real_t(p.get_i())));
//       break;

//     case _real:
//       res = std::abs(p.get_r());
//       break;

//     case _complex:
//       res = std::abs(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("abs", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("abs", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("abs(?)");
//   }

//   return res;
// }
// Parameter conj(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = p.get_i();
//       break;

//     case _real:
//       res = p.get_r();
//       break;

//     case _complex:
//       res = conj(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("conj", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("conj", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("conj(?)");
//   }

//   return res;
// }
// Parameter sqrt(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::sqrt(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::sqrt(p.get_r());
//       break;

//     case _complex:
//       res = std::sqrt(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("sqrt", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("sqrt", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("sqrt(?)");
//   }

//   return res;
// }
// Parameter log(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::log(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::log(p.get_r());
//       break;

//     case _complex:
//       res = std::log(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("log", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("log", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("log(?)");
//   }

//   return res;
// }
// Parameter log10(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::log10(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::log10(p.get_r());
//       break;

//     case _complex:
//       res = std::log10(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("log10", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("log10", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("log10(?)");
//   }

//   return res;
// }
// Parameter exp(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::exp(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::exp(p.get_r());
//       break;

//     case _complex:
//       res = std::exp(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("exp", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("exp", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("exp(?)");
//   }

//   return res;
// }
// Parameter sin(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::sin(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::sin(p.get_r());
//       break;

//     case _complex:
//       res = std::sin(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("sin", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("sin", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("sin(?)");
//   }

//   return res;
// }
// Parameter cos(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::cos(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::cos(p.get_r());
//       break;

//     case _complex:
//       res = std::cos(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("cos", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("cos", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("cos(?)");
//   }

//   return res;
// }
// Parameter tan(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::tan(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::tan(p.get_r());
//       break;

//     case _complex:
//       res = std::tan(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("tan", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("tan", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("tan(?)");
//   }

//   return res;
// }
// Parameter sinh(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::sinh(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::sinh(p.get_r());
//       break;

//     case _complex:
//       res = std::sinh(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("sinh", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("sinh", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("sinh(?)");
//   }

//   return res;
// }

// Parameter cosh(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )

//   {
//     case _integer:
//       res = std::cosh(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::cosh(p.get_r());
//       break;

//     case _complex:
//       res = std::cosh(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("cosh", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("cosh", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("cosh(?)");
//   }

//   return res;
// }
// Parameter tanh(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::tanh(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::tanh(p.get_r());
//       break;

//     case _complex:
//       res = std::tanh(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("tanh", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("tanh", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("tanh(?)");
//   }

//   return res;
// }
// Parameter acos(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::acos(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::acos(p.get_r());
//       break;

//     case _complex:
//       res = acos(p.get_c());
//       break; // not stl standard

//     case _string:
//       res.illegalOperation2("acos", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("acos", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("acos(?)");
//   }

//   return res;
// }
// Parameter asin(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::asin(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::asin(p.get_r());
//       break;

//     case _complex:
//       res = asin(p.get_c());
//       break; // not stl standard

//     case _string:
//       res.illegalOperation2("asin", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("asin", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("asin(?)");
//   }

//   return res;
// }
// Parameter atan(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::atan(real_t(p.get_i()));
//       break;

//     case _real:
//       res = std::atan(p.get_r());
//       break;

//     case _complex:
//       res = atan(p.get_c());
//       break; // not stl standard

//     case _string:
//       res.illegalOperation2("atan", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("atan", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("atan(?)");
//   }

//   return res;
// }

// // acosh, asinh and atanh are not standard in c++, may not exist ...
// Parameter acosh(const Parameter& p)
// {
//   Parameter res;

//   //res.illegalOperation2("acosh","all types"); // undefined function for string !!!
//   switch ( p.type() )
//   {
//     case _integer:
//       res = acosh(real_t(p.get_i()));
//       break;

//     case _real:
//       res = acosh(p.get_r());
//       break;

//     case _complex:
//       res = acosh(p.get_c());
//       break; // not stl standard

//     case _string:
//       res.illegalOperation2("acosh", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("acosh", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("acosh(?)");
//   }

//   return res;
// }

// Parameter asinh(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = asinh(real_t(p.get_i()));
//       break;

//     case _real:
//       res = asinh(p.get_r());
//       break;

//     case _complex:
//       res = asinh(p.get_c());
//       break; // not stl standard

//     case _string:
//       res.illegalOperation2("asinh", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("asinh", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("asinh(?)");
//   }

//   return res;
// }

// Parameter atanh(const Parameter& p)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = atanh(real_t(p.get_i()));
//       break;

//     case _real:
//       res = atanh(p.get_r());
//       break;

//     case _complex:
//       res = atanh(p.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("atanh", "String"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("atanh", "pointer"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("atanh(?)");
//   }

//   return res;
// }
// Parameter pow(const Parameter& p, const Parameter& q)
// {
//   Parameter res;

//   switch (q.type())
//   {
//     case _integer:
//       res = xlifepp::pow(p, q.get_i());
//       break;

//     case _real:
//       res = xlifepp::pow(p, q.get_r());
//       break;

//     case _complex:
//       res = xlifepp::pow(p, q.get_c());
//       break;

//     case _string:
//       res.illegalOperation2("pow", "x,String"); // undefined function for string exponent !!!

//     case _pointer:
//       res.illegalOperation2("pow", "x,pointer"); // undefined function for pointer exponent !!!

//     default:
//       res.undefinedParameter("pow(x,?)");
//   }

//   return res;
// }
// Parameter pow(const Parameter& p, int_t i)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = int_t(pow(real_t(p.get_i()), real_t(i)));
//       break;

//     case _real:
//       res = std::pow(p.get_r(), real_t(i));
//       break;

//     case _complex:
//       res = std::pow(p.get_c(), real_t(i));
//       break;

//     case _string:
//       res.illegalOperation2("pow", "String, Int"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("pow", "pointer, Int"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("pow(?,Int)");
//   }

//   return res;
// }
// Parameter pow(const Parameter& p, int i)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = int_t(std::pow(real_t(p.get_i()), real_t(i)));
//       break;

//     case _real:
//       res = std::pow(p.get_r(), real_t(i));
//       break;

//     case _complex:
//       res = std::pow(p.get_c(), real_t(i));
//       break;

//     case _string:
//       res.illegalOperation2("pow", "String, int"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("pow", "pointer, int"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("pow(?,int)");
//   }

//   return res;
// }
// Parameter pow(const Parameter& p, number_t n)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = int_t(std::pow(real_t(p.get_i()), real_t(n)));
//       break;

//     case _real:
//       res = std::pow(p.get_r(), real_t(n));
//       break;

//     case _complex:
//       res = std::pow(p.get_c(), real_t(n));
//       break;

//     case _string:
//       res.illegalOperation2("pow", "String, Number"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("pow", "pointer, Number"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("pow(?,Number)");
//   }

//   return res;
// }
// Parameter pow(const Parameter& p, real_t r)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::pow(real_t(p.get_i()), r);
//       break;

//     case _real:
//       res = std::pow(p.get_r(), r);
//       break;

//     case _complex:
//       res = std::pow(p.get_c(), complex_t(r, 0));
//       break;

//     case _string:
//       res.illegalOperation2("pow", "String,Real"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("pow", "pointer,Real"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("pow(?,Real)");
//   }

//   return res;
// }
// Parameter pow(const Parameter& p, const complex_t& c)
// {
//   Parameter res;

//   switch ( p.type() )
//   {
//     case _integer:
//       res = std::pow(complex_t(p.get_i(), 0), c);
//       break;

//     case _real:
//       res = std::pow(complex_t(p.get_r(), 0), c);
//       break;

//     case _complex:
//       res = std::pow(p.get_c(), c);
//       break;

//     case _string:
//       res.illegalOperation2("pow", "String,Complex"); // undefined function for string !!!

//     case _pointer:
//       res.illegalOperation2("pow", "pointer,Complex"); // undefined function for pointer !!!

//     default:
//       res.undefinedParameter("pow(?,Complex)");
//   }

//   return res;
// }

/*
=================================================================================
 Parameters class implementation
=================================================================================
*/
/*
--------------------------------------------------------------------------------
 constructors/destructor
--------------------------------------------------------------------------------
*/
//default constructor
Parameters::Parameters() {freeParams_=false;}

// copy constructor with FULL COPY of parameters
Parameters::Parameters(const Parameters& pars)
{
  std::vector<Parameter*>::const_iterator itp=pars.list_.begin();
  for (;itp!=pars.list_.end(); ++itp) push(*new Parameter(**itp));
  freeParams_=true;
}

// assign operator with FULL COPY of parameters
Parameters& Parameters::operator=(const Parameters& pars)
{
  list_.clear();
  indexFromName_.clear();
  indexFromShortNames_.clear();
  std::vector<Parameter*>::const_iterator itp=pars.list_.begin();
  for (;itp!=pars.list_.end(); ++itp) push(*new Parameter(**itp));
  freeParams_=true;
  return *this;
}

// constructor with a Parameter
Parameters::Parameters(Parameter& p)
  : list_(0)
{
  freeParams_=false;
  push(p);
}

// constructor with an explicit (void*,string) parameter
Parameters::Parameters(const void* p, const string_t& nm, const string_t& snm)
  : list_(0)
{
  string_t na=nm;
  if (na.empty()) na="parameter1";
  push(*new Parameter(p, na, snm));
  freeParams_=true;
}

Parameters::Parameters(const void* p, const string_t& nm, const Strings& snm)
  : list_(0)
{
  string_t na=nm;
  if (na.empty()) na="parameter1";
  push(*new Parameter(p, na, snm));
  freeParams_=true;
}

// destructor, delete Param pointers if required by freeParams_
Parameters::~Parameters()
{
  if (freeParams_)
  {
    std::vector<Parameter*>::const_iterator itp=list_.begin();
    for (;itp!=list_.end(); ++itp) delete *itp;
  }
}
/*
--------------------------------------------------------------------------------
 insertion operator
--------------------------------------------------------------------------------
*/
void Parameters::push(Parameter& p) // add a parameter to a list
{
  if (p.name().empty() ) // if no name set default name: parameterxx
    p.name("parameter" + tostring(list_.size()));
  std::map<string_t, number_t>::const_iterator ind_i = indexFromName_.find(p.name());
  if (ind_i!=indexFromName_.end()) //modify its value
  {
    *list_[ind_i->second] = p;
  }
  else
  {
    indexFromName_[p.name()] = list_.size();
    for (number_t snid=0; snid < p.shortnames().size(); ++snid)
    {
      indexFromShortNames_[p.shortname(snid)] = list_.size();
    }
    list_.push_back(&p);
  }
}

void Parameters::push(const Parameters& pars)
{
  std::vector<Parameter*>::const_iterator itp=pars.list_.begin();
  for(;itp!=pars.list_.end(); ++itp) push(**itp);
}

Parameters& Parameters::operator<<(Parameter* p) // insert a parameter (as a pointer) in list via stream
{
  push(*p);
  return *this;
}

Parameters& Parameters::operator<<(Parameter& p) // insert a parameter in list via stream
{
  push(p);
  return *this;
}
Parameters& Parameters::operator<<(const Parameter& p) // insert a parameter in list via stream, (in case of const, full copy of the parameter)
{
  Parameter* par=new Parameter(p);
  push(*par);
  return *this;
}

Parameters& Parameters::operator<<(const void* p) // insert a string in list via stream
{
  // create a new parameter, name=parameterxx, where xx is a unique number
  number_t n = list_.size();
  string_t na = "parameter" + tostring(n + 1);
  Parameter* p_ = new Parameter(p, na);
  push(*p_);
  return *this;
}
/*
--------------------------------------------------------------------------------
 access operator
--------------------------------------------------------------------------------
*/
bool Parameters::contains(const Parameter* p) const // check whether Parameter is in Parameters
{
  for ( std::vector<Parameter*>::const_iterator it = list_.begin(); it != list_.end(); it++)
    if ( *it == p )
    {
      return true;
    }

  return false;
}
bool Parameters::contains(const Parameter& p) const // check whether Parameter is in Parameters
{
  return contains(&p);
}

bool Parameters::contains(const string_t& s) const // check whether Parameter given by name is in Parameters
{
  std::map<string_t, number_t>::const_iterator it = indexFromName_.find(s);
  if (it != indexFromName_.end()) { return true; }
  else
  {
    it = indexFromShortNames_.find(s);
    return (it != indexFromShortNames_.end());
  }
  return (it != indexFromName_.end());
}

bool Parameters::contains(const char* s) const
{
  return contains(string_t(s));
}

Parameter& Parameters::operator()(const string_t& s) const // access by name
{
  std::map<string_t, number_t>::const_iterator ind_i = indexFromName_.find(s);
  if (ind_i == indexFromName_.end()) error("param_not_found", s);
  return *list_[ind_i->second];
}

Parameter& Parameters::operator()(const char* s) const // access by name
{
  return (*this)(string_t(s));
}

Parameter& Parameters::operator()(const Parameter& p) const // access by object
{
  return (*this)(p.name());
}

Parameter& Parameters::operator()(const number_t n) const // access by position in list
{
  if (n == 0 || n > list_.size()) // index out of limit
  {
    theMessageData << int_t(n) << int_t(list_.size());
    error("param_badind");
  }
  return *list_[n - 1];
}

Parameter& Parameters::getFromShortName(const string_t& s) const // access by shortname
{
  std::map<string_t, number_t>::const_iterator ind_i = indexFromShortNames_.find(s);
  if (ind_i == indexFromShortNames_.end()) error("param_not_found", s);
  return *list_[ind_i->second];
}

Parameter& Parameters::getFromShortName(const char* s) const // access by shortname
{
  return getFromShortName(string_t(s));
}

void Parameters::parse(const string_t& filename, bool errorOnNotFound)
{
  std::ifstream fin(filename.c_str());
  if (fin)
  {
    string_t line;
    while (getline(fin, line))
    {
      if (!isComment(line)) {
        std::istringstream iss(line);
        string_t key;
        iss >> key;
        if (isNameAvailable(key) && !contains(key)) { if (errorOnNotFound) error("param_not_found", key); }
        // the parameter exists
        // we get the parameter index in the list
        std::map<string_t, number_t>::const_iterator it = indexFromName_.find(key);
        if (it == indexFromName_.end()) { it=indexFromShortNames_.find(key); }
        // "it" is not at end because contains(argv[i]) is necessarily true here
        // we look at each argument to get its type and guess how many values we have to read
        Parameter * param=list_[it->second];
        switch (param->type_)
        {
          case _integer:
          {
            int_t itmp;
            iss >> itmp;
            *param=itmp;
            break;
          }
          case _bool:
          {
            bool btmp;
            iss >> btmp;
            *param=btmp;
            break;
          }
          case _real:
          {
            real_t rtmp;
            iss >> rtmp;
            *param=rtmp;
            break;
          }
          case _string:
          {
            string_t sval, stmp;
            bool addSpace=false;
            while (iss >> stmp)
            {
              if (addSpace) { sval +=" "; }
              sval+=stmp;
              addSpace=true;
            }
            if (sval[0] == '\"') // string is inside double quotes
            {
              number_t n=sval.size();
              sval=sval.substr(1,n-2);
            }
            *param=sval;
            break;
          }
          case _complex:
          {
            complex_t ctmp;
            iss >> ctmp;
            *param=ctmp;
            break;
          }
          case _integerVector:
          {
            std::vector<int_t> v;
            int_t itmp;
            while (iss >> itmp)
            {
              // if it is not an option key, it is considered as a value
              v.push_back(itmp);
            }
            *param=v;
            break;
          }
          case _realVector:
          {
            std::vector<real_t> v;
            real_t rtmp;
            while (iss >> rtmp)
            {
              // if it is not an option key, it is considered as a value
              v.push_back(rtmp);
            }
            *param=v;
            break;
          }
          case _complexVector:
          {
            std::vector<complex_t> v;
            complex_t ctmp;
            while (iss >> ctmp)
            {
              // if it is not an option key, it is considered as a value
              v.push_back(ctmp);
            }
            *param=v;
            break;
          }
          case _stringVector:
          {
            std::vector<string_t> v;
            string_t stmp;
            while (iss >> stmp)
            {
              string_t sval;
              if (stmp[0] == '\"')
              {
                bool addSpace=false;
                number_t n=stmp.size();
                while(stmp[n-1] != '\"')
                {
                  if (addSpace) { sval +=" "; }
                  sval+=stmp;
                  iss >> stmp;
                  n=stmp.size();
                  addSpace=true;
                }
                if (addSpace) { sval +=" "; }
                sval += stmp;
                n=sval.size();
                sval=sval.substr(1,n-2);
              }
              else { sval=stmp; }
              // if it is not an option key, it is considered as a value
              v.push_back(sval);
            }
            *param=v;
            break;
          }
          default:
            error("param_badtype",words("value",param->type_), words("param key",param->key_));
        }
      }
    }
    fin.close();
  }
  else
  {
    error("bad_file", filename);
  }

}

void Parameters::parse(int argc, char** argv, bool errorOnNotFound)
{
  if (argc > 1)
  {
    std::vector<string_t> clargs(argc-1);
    for (number_t i=1; i < argc; ++i) clargs[i-1]=argv[i];
    while (clargs.size() > 0)
    {
      if (isNameAvailable(clargs[0]))
      { // the key exists and is not a system key
        if (!contains(clargs[0])) { if (errorOnNotFound) error("param_not_found", clargs[0]); }
        // the parameter exists
        // we get the parameter index in the list
        std::map<string_t, number_t>::const_iterator it = indexFromName_.find(clargs[0]);
        if (it == indexFromName_.end()) { it=indexFromShortNames_.find(clargs[0]); }
        // "it" is not at end because contains(clargs[0]) is necessarily true here
        // we look at each argument to get its type and guess how many values we have to read
        Parameter * param=list_[it->second];
        switch (param->type_)
        {
          case _integer:
          {
            if (clargs.size() < 2) { error("param_value_not_found", words("value", _integer), clargs[0]); }
            else { if (contains(clargs[1])) { error("param_value_not_found", words("value", _integer), clargs[0]); } }
            *param=atoi(clargs[1].c_str());
            clargs.erase(clargs.begin(), clargs.begin()+2);
            break;
          }
          case _bool:
          {
            if (clargs.size() < 2) { error("param_value_not_found", words("value", _bool), clargs[0]); }
            else { if (contains(clargs[1])) { error("param_value_not_found", words("value", _bool), clargs[0]); } }
            *param= (clargs[1] == string_t("true"));
            clargs.erase(clargs.begin(), clargs.begin()+2);
            break;
          }
          case _real:
          {
            if (clargs.size() < 2) { error("param_value_not_found", words("value", _real), clargs[0]); }
            else { if (contains(clargs[1])) { error("param_value_not_found", words("value", _real), clargs[0]); } }
            *param=atof(clargs[1].c_str());
            clargs.erase(clargs.begin(), clargs.begin()+2);
            break;
          }
          case _complex:
          {
            if (clargs.size() < 2) { error("param_value_not_found", words("value", _complex), clargs[0]); }
            else { if (contains(clargs[1])) { error("param_value_not_found", words("value", _complex), clargs[0]); } }
            real_t real_part=atof(clargs[1].c_str());
            real_t imag_part=0.;
            number_t iter=2;
            if (clargs.size() > 2) {
              if (!contains(clargs[2])) { imag_part=atof(clargs[2].c_str()); }
              iter=3;
            }
            *param=complex_t(real_part, imag_part);
            clargs.erase(clargs.begin(), clargs.begin()+iter);
            break;
          }
          case _string:
          {
            if (clargs.size() < 2) { error("param_value_not_found", words("value", _string), clargs[0]); }
            else { if (contains(clargs[1])) { error("param_value_not_found", words("value", _string), clargs[0]); } }
            *param=clargs[1].c_str();
            clargs.erase(clargs.begin(), clargs.begin()+2);
            break;
          }
          case _integerVector:
          {
            if (clargs.size() < 2) { error("param_value_not_found", words("value", _integerVector), clargs[0]); }
            else { if (contains(clargs[1])) { error("param_value_not_found", words("value", _integerVector), clargs[0]); } }
            std::vector<int_t> v;
            clargs.erase(clargs.begin());
            while (clargs.size() > 0 && !contains(clargs[0]))
            {
              // if it is not an option key, it is considered as a value
              v.push_back(atoi(clargs[0].c_str()));
              clargs.erase(clargs.begin());
            }
            *param=v;
            break;
          }
          case _realVector:
          {
            if (clargs.size() < 2) { error("param_value_not_found", words("value", _realVector), clargs[0]); }
            else { if (contains(clargs[1])) { error("param_value_not_found", words("value", _realVector), clargs[0]); } }
            std::vector<real_t> v;
            clargs.erase(clargs.begin());
            while (clargs.size() > 0 && !contains(clargs[0]))
            {
              // if it is not an option key, it is considered as a value
              v.push_back(atof(clargs[0].c_str()));
              clargs.erase(clargs.begin());
            }
            *param=v;
            break;
          }
          case _complexVector:
          {
            if (clargs.size() < 3) { error("param_value_not_found", words("value", _complexVector), clargs[0]); }
            else { if (contains(clargs[1])) { error("param_value_not_found", words("value", _complexVector), clargs[0]); } }
            std::vector<complex_t> v;
            clargs.erase(clargs.begin());
            while (clargs.size() > 2 && !contains(clargs[0]))
            {
              real_t real_part=atof(clargs[0].c_str());
              real_t imag_part=atof(clargs[1].c_str());
              // if it is not an option key, it is considered as a value
              v.push_back(complex_t(real_part, imag_part));
              clargs.erase(clargs.begin(), clargs.begin()+2);
            }
            *param=v;
            break;
          }
          case _stringVector:
          {
            if (clargs.size() < 2) { error("param_value_not_found", words("value", _stringVector), clargs[0]); }
            else { if (contains(clargs[1])) { error("param_value_not_found", words("value", _stringVector), clargs[0]); } }
            std::vector<string_t> v;
            clargs.erase(clargs.begin());
            while (clargs.size() > 0 && !contains(clargs[0]))
            {
              // if it is not an option key, it is considered as a value
              v.push_back(argv[0]);
              clargs.erase(clargs.begin());
            }
            *param=v;
            break;
          }
          default:
            error("param_badtype",words("value",param->type_), words("param key",param->key_));
        }
      }
      else
      {
        // the key is a system key or does not exists
        // it is ignored
        clargs.erase(clargs.begin());
      }
    }
  }
  // nothing done when no arguments
}

//! special safe access to some int parameters
//  s   : parameter name
//  def : default value if not found
int Parameters::safeInt(const string_t& s, int def) const
{
  auto ind_i = indexFromName_.find(s);
  if (ind_i == indexFromName_.end()) return def;
  return list_[ind_i->second]->i_;
}

/*
--------------------------------------------------------------------------------
 I/O functions
--------------------------------------------------------------------------------
*/
std::ostream& operator<<(std::ostream& os, const Parameters& pars) // flux insertion (write)
{
  std::vector<Parameter*>::const_iterator itp=pars.list_.begin();
  for ( ; itp!=pars.list_.end(); itp++)
  {
    if (itp != pars.list_.begin()) os << eol;
    os << **itp;
  }

  return os;
}

void print(const Parameters& pars)
{
  thePrintStream << pars;
}

/*!
  check if a name is not a system key name
*/
bool isNameAvailable(const string_t& keyname)
{
  const std::vector<string_t>& keys=theEnvironment_p->systemKeys();
  if (std::find(keys.begin(), keys.end(), keyname) != keys.end()) return false;
  return true;
}

//! check if the current line is not a comment
bool isComment(const string_t& line){
  if (line[0] == '/' && line[1] == '/') { return true; }
  return false;
}

} // end of namespace xlifepp;
