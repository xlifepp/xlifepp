/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Kernel.cpp
  \author E. Lunéville
  \since 20 sep 2013
  \date  20 sep 2013

  \brief Implementation of xlifepp::Kernel class members and related functions
*/

#include "Kernel.hpp"


namespace xlifepp
{
//! copy constructor
Kernel::Kernel(const Kernel& ker)
{
  copy(ker);
}

//! assign operator
Kernel& Kernel::operator=(const Kernel& ker)
{
  copy(ker);
  return *this;
}

void Kernel::copy(const Kernel& ker)
{
  //copy Functions's
  kernel=ker.kernel;
  gradx=ker.gradx; grady=ker.grady; gradxy=ker.gradxy;
  ndotgradx=ker.ndotgradx; ndotgrady=ker.ndotgrady;
  curlx=ker.curlx; curly=ker.curly; curlxy=ker.curlxy;
  tracx=ker.tracx; tracy=ker.tracy; tracxy=ker.tracxy;
  nxtensornxtimestracy=ker.nxtensornxtimestracy;
  nxtensornxtimesNGr=ker.nxtensornxtimesNGr;
  divx=ker.divx; divy=ker.divy; divxy=ker.divxy;
  dx1=ker.dx1; dx2=ker.dx2; dx3=ker.dx3;
  //copy names, properties
  name=ker.name; shortname=ker.shortname;
  singularType = ker.singularType; singularOrder=ker.singularOrder;
  singularCoefficient=ker.singularCoefficient;
  symmetry=ker.symmetry;
  //copy parameters and update links to parameters
  userData=ker.userData;
  updateParametersLinks();
  //reallocate singpart and regpart if allocated
  singPart=nullptr;
  if (ker.singPart!=nullptr) singPart=ker.singPart->clone(); //full copy
  regPart=nullptr;
  if (ker.regPart!=nullptr) regPart=ker.regPart->clone();   //full copy
  //copy temporary members
  expansion=nullptr;  //NOT MANAGED
  checkType_=ker.checkType_;
  conjugate_=ker.conjugate_; transpose_=ker.transpose_;
  xpar=ker.xpar;  xory=ker.xory;
  requireNx=ker.requireNx;
  requireNy=ker.requireNy;
  requireTx=ker.requireTx;
  requireTy=ker.requireTy;
  requireElt=ker.requireElt;
  requireDom=ker.requireDom;
  requireDof=ker.requireDof;
  dimPoint=ker.dimPoint;
  updateDimPoint();
}

//!< clone current kernel (virtual)
Kernel* Kernel::clone() const
{
  return new Kernel(*this);
}

void Kernel::updateParametersLinks()
{
  kernel.params_p()=&userData;
  if (!gradx.isVoidFunction()) gradx.params_p()=&userData;
  if (!grady.isVoidFunction()) grady.params_p()=&userData;
  if (!gradxy.isVoidFunction()) gradxy.params_p()=&userData;
  if (!ndotgradx.isVoidFunction()) ndotgradx.params_p()=&userData;
  if (!ndotgrady.isVoidFunction()) ndotgrady.params_p()=&userData;
  if (!curlx.isVoidFunction()) curlx.params_p()=&userData;
  if (!curly.isVoidFunction()) curly.params_p()=&userData;
  if (!curlxy.isVoidFunction()) curlxy.params_p()=&userData;
  if (!divx.isVoidFunction()) divx.params_p()=&userData;
  if (!divy.isVoidFunction()) divy.params_p()=&userData;
  if (!divxy.isVoidFunction()) divxy.params_p()=&userData;
  if (!dx1.isVoidFunction()) dx1.params_p()=&userData;
  if (!dx2.isVoidFunction()) dx2.params_p()=&userData;
  if (!dx3.isVoidFunction()) dx3.params_p()=&userData;
}

void Kernel::updateDimPoint()
{
  kernel.dimPoint_=dimPoint;
  if (!gradx.isVoidFunction()) gradx.dimPoint_=dimPoint;
  if (!grady.isVoidFunction()) grady.dimPoint_=dimPoint;
  if (!gradxy.isVoidFunction()) gradxy.dimPoint_=dimPoint;
  if (!ndotgradx.isVoidFunction()) ndotgradx.dimPoint_=dimPoint;
  if (!ndotgrady.isVoidFunction()) ndotgrady.dimPoint_=dimPoint;
  if (!curlx.isVoidFunction()) curlx.dimPoint_=dimPoint;
  if (!curly.isVoidFunction()) curly.dimPoint_=dimPoint;
  if (!curlxy.isVoidFunction()) curlxy.dimPoint_=dimPoint;
  if (!tracx.isVoidFunction()) tracx.params_p()=&userData;
  if (!tracy.isVoidFunction()) tracy.params_p()=&userData;
  if (!tracxy.isVoidFunction()) tracxy.params_p()=&userData;
  if (!nxtensornxtimestracy.isVoidFunction()) nxtensornxtimestracy.params_p()=&userData;
  if (!nxtensornxtimesNGr.isVoidFunction()) nxtensornxtimesNGr.params_p()=&userData;
  if (!divx.isVoidFunction()) divx.dimPoint_=dimPoint;
  if (!divy.isVoidFunction()) divy.dimPoint_=dimPoint;
  if (!divxy.isVoidFunction()) divxy.dimPoint_=dimPoint;
  if (!dx1.isVoidFunction()) dx1.dimPoint_=dimPoint;
  if (!dx2.isVoidFunction()) dx2.dimPoint_=dimPoint;
  if (!dx3.isVoidFunction()) dx3.dimPoint_=dimPoint;
}

Kernel::~Kernel()
{
  if (singPart!=nullptr && singPart!=this) delete singPart;
  if (regPart!=nullptr && regPart!=this) delete regPart;
}

const Function& Kernel::operator()(VariableName xory) const   // specify k(x,y) as x->k(x,y) or y->k(x,y)
{
  Point p(0.);
  if (xory==_x) kernel.setY(p);
  else  kernel.setX(p);
  return kernel;
}

void Kernel::print(std::ostream& os) const
{
  os<<"Kernel object: "<< name;
  os<<",  kernel function: ";
  if (isSymbolic()) os<<", symbolic kernel (no kernel function)";
  else os<<kernel.name();
  os<<",  singularity type: "<<singularType;
  if (singularType!=_notsingular) os<<",  singularity order: "<<singularOrder;
  string_t req="require ";
  if (requireNx) {os<<req<<"n or nx"; req=", ";}
  if (requireNy) {os<<req<<"ny"; req=", ";}
  if (requireTx) {os<<req<<"t or tx"; req=", ";}
  if (requireTy) {os<<req<<"ty"; req=", ";}
  if (requireElt) {os<<req<<"element"; req=", ";}
  if (requireDom) {os<<req<<"domain"; req=", ";}
  if (requireDof) {os<<req<<"dof";}
  if (req!="require") os<<eol;
  if (theVerboseLevel>10 && userData.size()>0) os<<", "<<userData;
  os<<"\n";
}

std::ostream& operator<<(std::ostream& os, const Kernel& k)
{
  k.print(os);
  return os;
}

} // end of namespace xlifepp

