/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file winUtils.hpp
  \author E. Lunéville
  \since 27 dec 2011
  \date 3 may 2012

  \brief Provides specific utilities when running with windows
  uname()
 */

#ifndef WIN_UTILS_HPP
#define WIN_UTILS_HPP

#ifdef OS_IS_WIN

  namespace xlifepp
  {

  /*!
    \struct StrUname
    Utility structure to emulate uname() function
  */
  struct StrUname
  {
    string_t osName;      //!< windows name
    string_t machineName; //!< the name of the machine running the library
    string_t processor;   //!< processor name
  };

  StrUname winUname(); // return os, machine and processor names

  } // end of namespace xlifepp

#endif

#endif /* WIN_UTILS_HPP */
