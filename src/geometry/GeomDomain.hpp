/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomDomain.hpp
  \author E. Lunéville
  \since 04 apr 2012
  \date 04 apr 2012

  \brief Definition of the classes related to geometric domains

  Geometric domains may have various representation:
  - a mesh description (a set of elementary patch, say a part of mesh)
  - a union of domains
  - an intersection of domains
  - an analytic description (say a parametrized geometric domain)

  the base class is GeomDomain whom inherit the classes:
  - xlifepp::MeshDomain for mesh type description
  - xlifepp::CompositeDomain for union or intersection of any domains
  - xlifepp::AnalyticDomain for analytic description (not implemented)

  astract/non abstract pattern is used:
  \verbatim
             pointer to                        child
  GeomDomain ---------> GeomDomain -------| MeshDomain      (mesh)
                                          | CompositeDomain (union or intersection of domain)
                                          | AnalyticDomain  (analytic description, for future)
                                          | PointDomain     (points cloud, for future)
  \endverbatim
*/

#ifndef GEOM_DOMAIN_HPP
#define GEOM_DOMAIN_HPP

#include "config.h"
#include "utils.h"
#include "GeomElement.hpp"
#include "Geometry.hpp"
#ifdef XLIFEPP_WITH_METIS
  #include "metis.h"
#endif

namespace xlifepp
{

class Mesh;

//--------------------------------------------------------------------------------

//backward declaration
class MeshDomain;
class MeshDomainPartition;
class CompositeDomain;
class AnalyticalDomain;
class PointsDomain;
class PartitionData;

//! type of domain
enum DomainType {_undefDomain = 0, _analyticDomain, _meshDomain, _compositeDomain, _pointsDomain};
//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const DomainType& dt);
//! type of composition of domains
enum SetOperationType {_union, _intersection};
//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const SetOperationType& sot);
//! type of extension if an extended domain
enum DomainExtensionType {_noExtension, _vertexExtension,_sideExtension, _ficticiousExtension};

/*!
   \class DomainInfo
   to store general information of geometric domain
*/
class DomainInfo
{
  public:
    string_t name;          //!< name of domain
    dimen_t dim;            //!< dimension of domain (the max when compositeDomain)
    DomainType domType;     //!< type of domain
    const Mesh* mesh_p;     //!< pointer to mesh
    string_t description;   //!< description of the domain
    bool toSave;            //!< true if the domain is, by construction, intended to be saved into a file (by the saveToFile function)
    bool statsComputed;     //!< true if Domain::computeStatistics is called
    bool sideStatsComputed;     //!< true if Domain::computeStatistics is called with boolean true
    real_t measure, boundaryMeasure,                          //!< numerical measure of domain and boundary domain
           minEltSize, maxEltSize, averageEltSize,            //!< min, max, average size of elements
           minEltMeasure, maxEltMeasure, averageEltMeasure,   //!< min, max, average measure of elements
           minSideMeasure, maxSideMeasure, averageSideMeasure;//!< min, max, average of side measures
    number_t nbOfNodes, nbOfVertices, nbOfElements;           //!< number of nodes, vertices and elements
    mutable number_t maxEltOrder;                             //!< max element order (computed when calling MeshDomain::order())

    DomainInfo(const string_t& na, dimen_t d, DomainType dt, const Mesh* m, const string_t& desc="", bool ts=true) //! basic constructor
      : name(na), dim(d), domType(dt), mesh_p(m), description(desc), toSave(ts), statsComputed(false), sideStatsComputed(false),
        measure(0), boundaryMeasure(0),minEltSize(0), maxEltSize(0), averageEltSize(0),
        minEltMeasure(0), maxEltMeasure(0), averageEltMeasure(0),
        minSideMeasure(0), maxSideMeasure(0), averageSideMeasure(0), maxEltOrder(0) {}

    void print(std::ostream&) const; //!< print DomainInfo informations
    void print(PrintStream& os) const {print(os.currentStream());}
};

std::ostream& operator<<(std::ostream&, const DomainInfo&); //!< print operator

/*!
  \class GeomDomain
  base class of geometric domain
*/
class GeomDomain
{
  protected:
    DomainInfo* domainInfo_p;     //!< pointer to domain information class
    GeomDomain* domain_p;         //!< pointer to its child or itself if a child
    GeomDomain* up_p;             //!< pointer to the parent (0 if no parent)
    mutable Geometry* geometry_p; //!< pointer to the supporting geometry (0 if not defined)


  public:
    static std::vector<const GeomDomain*> theDomains; //!< list of all existing domains
    static void clearGlobalVector();                  //!< delete all domain objects

    GeomDomain(const GeomDomain& d)              //! forbidden copy constructor
    {error("forbidden","GeomDomain::GeomDomain(const GeomDomain&)");}
    GeomDomain& operator=(const GeomDomain& d)   //! forbidden assign operator
    {error("forbidden","GeomDomain::operator=(const GeomDomain&)"); return *this;}

    //constructors
    GeomDomain(const string_t& na="", dimen_t d=0, const Mesh* m=nullptr, Geometry* g=nullptr);                          //!< default and basic constructor
    GeomDomain(const Mesh&, const string_t&, dimen_t, const string_t& desc="", Geometry* g=nullptr, bool ts=true); //!< constructor of a meshdomain
    GeomDomain(SetOperationType sot, const GeomDomain&, const GeomDomain&, const string_t& na = "");  //!< constructor of a composite domain with two domains
    GeomDomain(SetOperationType sot, const std::vector<const GeomDomain*>&, const string_t& na = ""); //!< constructor of a composite domain with n domains
    GeomDomain(const std::vector<Point>&, const string_t& na = ""); //!< constructor of points domain
    virtual ~GeomDomain();                                    //!< destructor

    //accessors
    const GeomDomain* up() const                              //! return up_p pointer (const)
    {return up_p;}
    const GeomDomain* domainp() const                         //! return domain_p pointer (const)
    {return domain_p;}
    DomainInfo& domainInfo()                                  //! return domainInfo
    {return *domainInfo_p;}
    const DomainInfo& domainInfo()const                       //! return domainInfo (const)
    {return *domainInfo_p;}
    const string_t& name() const                              //! return domain name (const)
    {return domainInfo_p->name;}
    virtual string_t sortedName() const                       //! return domain name where composite elements are sorted by name (const)
    {return domainInfo_p->name;}
    string_t nameWithoutSharp() const;                        //!< return domain name without sharp when there is one (const)
    void rename(const string_t & na)                          //! rename domain
    {domainInfo_p->name=na;}
    dimen_t dim() const                                       //! return domain dimension (const)
    {return domainInfo_p->dim;}
    dimen_t spaceDim() const;                                 //!< return the space dimension, say dim of points (const)
    DomainType domType() const                                //! return domain type (const)
    {return domainInfo_p->domType;}
    const string_t domTypeName() const                        //! return domain type name(const)
    {return words("domain type",domainInfo_p->domType);}
    const Mesh* mesh() const                                  //! return mesh pointer (const)
    {return domainInfo_p->mesh_p;}
    const string_t& description() const                       //! return domain description (const)
    {return domainInfo_p->description;}
    void setDescription(const string_t & desc)                //! set domain description
    { domainInfo_p->description=desc; }
    bool isToSave() { return domainInfo_p->toSave; }          //!< returns true if the domain is intended to be saved into a file
    void addSuffix(const string_t& s);                        //!< add a suffix to domain name
    Geometry* geometry() const;                               //!< get geometry, return geometry_p if not null else find the supporting geometry
    void setGeometry(Geometry*);                              //!< set geometry pointer
    bool hasParametrization() const;                          //!< true if parametrization allocated
    const Parametrization& parametrization() const;           //!< return parametrization if allocated in Geometry, error if not
    const Parametrization& boundaryParametrization() const;   //!< return boundary parametrization if allocated in Geometry, error if not
    void setParametrization(const Parametrization&);          //!< set parametrization (attached to geometry_p, replace previous one!)
    void setBoundaryParametrization(const Parametrization&);  //!< set boundary_parametrization (attached to geometry_p, replace previous one!)

    virtual bool isUnion() const;                             //!< true if the domain is an union of domains (const)
    virtual bool isIntersection() const;                      //!< true if the domain is a intersection of domains (const)
    virtual bool include(const GeomDomain&) const;            //!< true if the current domain includes a given domain (const)
    virtual MeshDomain* meshDomain();                         //!< access to child meshDomain
    virtual const MeshDomain* meshDomain() const;             //!< access to child MeshDomain (const)

    virtual MeshDomainPartition* meshDomainPartition();       //!< access to meshDomainPartition pointer
    virtual const MeshDomainPartition* meshDomainPartition() const; //!< access to meshDomainPartition pointer (const)
    void partition(Parameter& p1);
    void partition(Parameter& p1, Parameter& p2);
    void partition(Parameter& p1, Parameter& p2, Parameter& p3);
    void partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4);
    void partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4
                  ,Parameter& p5);
    void partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4
                  ,Parameter& p5, Parameter& p6);
    void partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4
                  ,Parameter& p5, Parameter& p6, Parameter& p7);
    void partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4
                  ,Parameter& p5, Parameter& p6, Parameter& p7, Parameter& p8);
    void partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4
                  ,Parameter& p5, Parameter& p6, Parameter& p7, Parameter& p8
                  , Parameter& p9);
    void partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4
                  ,Parameter& p5, Parameter& p6, Parameter& p7, Parameter& p8
                  , Parameter& p9, Parameter& p10);

    void buildPartitionData(const std::vector<Parameter>& ps);

    PartitionData* partitionData();

    virtual CompositeDomain* compositeDomain();               //!< access to child CompositeDomain
    virtual const CompositeDomain* compositeDomain() const;   //!< access to child CompositeDomain (const)
    virtual AnalyticalDomain* analyticalDomain() ;            //!< access to child AnalyticalDomain
    virtual const AnalyticalDomain* analyticalDomain() const; //!< access to child AnalyticalDomain (const)
    virtual bool isSideDomain() const;                        //!< true if a side domain
    virtual bool isSidesDomain() const;                       //!< true if it is a set of element sides (MeshDomain::isSidesOf_p!=nullptr)
    virtual bool isInterface() const;                         //!< true if an interface
    virtual number_t numberOfElements() const;                //!< number of geometric elements of domain
    virtual GeomElement* element(number_t);                   //!< access to k-th element (k>=1), 0 if not available
    virtual void clearGeomMapData();                          //!< clear temporary GeomData structures of MeshElement's
    virtual void setMaterialId(number_t);                     //!< set material id (>0) for all elements of domain
    virtual void setDomainId(number_t);                       //!< set the DomainId of all elements of domain
    virtual number_t order() const;                           //!< return order of element
    virtual bool colorFlag() const;                           //!< return useColorFlag status
    virtual void setColorFlag(bool) const;                    //!< set useColorFlag status
    virtual std::pair<OrientationType, const GeomDomain*> normalOrientation() const //!< return current orientation of normal vectors
    {return std::make_pair(_undefOrientationType,(GeomDomain*)0);}

    //specific MeshDomain virtual functions
    virtual real_t measure() const;                           //!< measure (length, surface or volume) of MeshDomain
    virtual void computeStatistics(bool computeSideInfo=false);//!< update geometric domain info (measure, characteristic sizes, ...)
    //@{
    //! set normal orientation of a meshdomain of dimension SpaceDim-1
    virtual void setNormalOrientation(OrientationType, const GeomDomain&) const;
    virtual void setNormalOrientation(OrientationType, const Point&) const;
    virtual void setNormalOrientation(OrientationType = _undefOrientationType, const GeomDomain* gp=nullptr, const Point* p=nullptr) const;
    //@}
    virtual Vector<real_t> getNormal(const Point& x, OrientationType ort = _undefOrientationType, const GeomDomain& dom = GeomDomain()) const  //!< return normal to a domain at a given point
    {
        where("GeomDomain::getNormal");
        error("domain_type_not_handled",words("domain type",domType()));
        return Vector<real_t>();
    }
    //! return the largest domain including list of subdomains (forbidden here)
    virtual const GeomDomain* largestDomain(std::vector<const GeomDomain*>) const
    {
      where("GeomDomain::largestDomain()");
      error("domain_type_not_handled",words("domain type",domType()));
      return nullptr;
    }
    virtual GeomDomain& extendDomain(bool useVertex=false, const GeomDomain& omega = GeomDomain()) const //! find or create extension of a side domain
    {
        where("GeomDomain::extendDomain()");
        error("domain_type_not_handled",words("domain type",domType()));
        return const_cast<GeomDomain&>(omega);
    }

    virtual GeomDomain& sidesDomain(const string_t& na="") const  //! find or create sides domain related to current one
    {
        where("GeomDomain::sidesDomain()");
        error("domain_type_not_handled",words("domain type",domType()));
        return const_cast<GeomDomain&>(*this);
    }

    virtual GeomDomain& ficticiousDomain(const GeomDomain& omega) const           //!< find or create a ficticious domain of a side domain
    {
        where("GeomDomain::ficticiousDomain()");
        error("domain_type_not_handled",words("domain type",domType()));
        return const_cast<GeomDomain&>(*this);
    }
    const GeomDomain* extendedDomain(bool useVertex=false, const GeomDomain& omega = GeomDomain()) const; //!< find extension of a side domain
    void updateParentOfSideElements();                                //!< update parent sides info of a side domain (only for mesh domain)

    virtual GeomDomain& removeElts(const std::set<GeomElement*>& elts, const string_t& namedom="") const//!< remove elements from current domain, creating new GeoDomain if different from current
    {
        where("GeomDomain::remove(set<GeomElement*>)");
        error("domain_type_not_handled",words("domain type",domType()));
        return const_cast<GeomDomain&>(*this);
    }
    virtual GeomDomain& removeElts(const GeomDomain& dom, const string_t& namedom="") const//!< remove dom elements from current domain, creating new GeoDomain if different from current
    {
        where("GeomDomain::remove(Domain)");
        error("domain_type_not_handled",words("domain type",domType()));
        return const_cast<GeomDomain&>(*this);
    }

    //print and export stuff
    virtual void print(std::ostream&) const;                          //!< print GeomDomain
    virtual void print(PrintStream& os) const {print(os.currentStream());}
    void saveNormalsToFile(const string_t&, IOFormat iof=_vtk) const; //!< export normal to file
    void saveNormalsToVtk(const string_t&) const;                     //!< export normal to vtk file
    void saveColorsToFile(const string_t& filename) const ;           //!< export element colors to vtk file

    //static functions
    static const GeomDomain* findDomain(const GeomDomain*);      //!< find a domain of mesh by its pointer
    static const GeomDomain* findDomain(const string_t& na);     //!< find a domain by its name
    static const GeomDomain* findCompositeDomain(SetOperationType, const std::vector<const GeomDomain*>&); //!< find an union/intersection composite domain by the domain pointers
    static const GeomDomain* findMeshDomainComposite(SetOperationType, const std::vector<const GeomDomain*>&); //!< find an union/intersection mesh domain by the domain pointers
    static const GeomDomain* findUnionDomain(const GeomDomain*); //!< find mesh domain corresponding to an union domain
    static void printTheDomains(std::ostream&);                  //!< print all domains
    static void printTheDomains(PrintStream & os)                //! print all domains on os
    {printTheDomains(os.currentStream());}
};

//! print geomdomain
std::ostream& operator<<(std::ostream&, const GeomDomain&);
//! construct or identify the geometrical symbolic union of domains
const GeomDomain* geomUnionOf(std::vector<const GeomDomain*>&, const GeomDomain*);
//! merge some geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const std::vector<const GeomDomain*>&, S_ name);
//! merge some geometrical domains (true union of elements), non const version
template<typename S_> GeomDomain& merge(const std::vector<GeomDomain*>&, S_ name);
//! merge 2 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, S_ name);
//! merge 3 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, S_ name);
//! merge 4 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&, S_ name);
//! merge 5 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, S_ name);
//! merge 6 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, S_ name);
//! merge 7 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, S_ name);
//! merge 8 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&, S_ name);
//! merge 9 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, S_ name);
//! merge 10 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, S_ name);
//! merge 11 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, S_ name);
//! merge 12 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&, S_ name);
//! merge 13 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, S_ name);
//! merge 14 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, S_ name);
//! merge 15 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, S_ name);
//! merge 16 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&, S_ name);
//! merge 17 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, S_ name);
//! merge 18 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, S_ name);
//! merge 19 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, S_ name);
//! merge 20 geometrical domains (true union of elements)
template<typename S_> GeomDomain& merge(const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&,
                                        const GeomDomain&, const GeomDomain&, const GeomDomain&, const GeomDomain&, S_ name);
void locateError(bool warn, const string_t&, const GeomDomain&, const Point&, real_t d); //!< error management for locate tool
inline real_t measure(const GeomDomain& g)  //! measure (length, surface or volume) of Domain
{return g.measure();}

GeomDomain& crack(const GeomDomain& side1, const GeomDomain& side2);  //!< defined a crack domain from both sides of the crack (alias)
GeomDomain& sides(GeomDomain& dom);  //!< access to domain defined from all sides of elements of domain dom, create it if not defined
GeomDomain& internalSides(GeomDomain& dom);  //!< access to domain defined from all internal sides of elements of domain dom, create it if not defined
GeomDomain& operator+(const GeomDomain& dom1, const GeomDomain& dom2); //!< create the domain made of union of elements (same dimension)
GeomDomain& operator-(const GeomDomain& dom1, const GeomDomain& dom2); //!< create the domain made of dom1 elements that are not in dom2

/*!
   \class MeshDomain
   class to handle domain with mesh description
*/
class MeshDomain : public GeomDomain
{
  public:
    std::list<MeshDomain*> parents;                  //!< mesh domain parents if side domain (see updateParents)
    std::vector<GeomElement*> geomElements;          //!< list of geometric elements
    std::set<ShapeType> shapeTypes;                  //!< list of element shape types in mesh domain (counted once)
    const MeshDomain* extensionof_p=nullptr;               //!< side domain pointer, if domain is an extension of a side domain, else 0
    const MeshDomain* extensionin_p=nullptr;               //!< extension domain pointer, if domain is an extension of a domain, else 0
    const MeshDomain* dualCrackDomain_p=nullptr;           //!< other side, if domain is a side of crack else 0
    DomainExtensionType extensionType=_noExtension;  //!< extension type
    mutable std::pair<OrientationType, const GeomDomain*> normalOrientationRule;  //!< the normal orientation rule
    mutable MeshDomain* sidesDomain_p=nullptr;             //!< domain of all sides of current domain (see sidesDomain)
    mutable const MeshDomain* isSidesOf_p=nullptr;         //!< parent domain when a sides domain

    //additional structures created if required, declared as mutable to allow construction on fly by const methods
    mutable std::map<Point,std::list<GeomElement*> > vertexElements;  //!< list of elements by vertex
    mutable KdTree<Point> kdtree;                                     //!< kdtree structure using to fast locate element containing a given point
    mutable std::map<GeomElement*, std::list<GeoNumPair> > sideToExt; //!< map side element -> list of extended elements (for a side domain!)
    mutable bool parentSidesUptodate=false;                           //!< true: parents of side elements are up to date
    mutable bool orientationComputed=false;                           //!< true: orientations are computed
    mutable bool jacobianComputed=false;                              //!< true: jacobians  are computed
    mutable bool diffEltComputed=false;                               //!< true: differential elements  are computed
    mutable bool normalComputed=false;                                //!< true: normals  are computed
    mutable bool inverseJacobianComputed=false;                       //!< true: inverse of jacobians  are computed
    mutable bool useColor=false;                                      //!< true: restrict integral to GeomElement with 0 color (only intg rep), default=false

    mutable PartitionData* partitionData_p=nullptr;                         //!< partition data related to mesh domain

    //constructor
    MeshDomain() {}
    MeshDomain(const Mesh&, const string_t&, dimen_t, const string_t&); //!< constructor
    ~MeshDomain();

    void removeExt();                                                    //!< remove extension of current domain

    //accessors and basic tools
    void buildPartitionData(const std::vector<Parameter>& ps);
    void setShapeTypes();                          //!< set the element shape types in mesh domain
    virtual MeshDomain* meshDomain()               //! access to meshDomain pointer
    {return this;}
    virtual const MeshDomain* meshDomain() const   //! access to meshDomain pointer (const)
    {return this;}
    virtual bool isUnion() const                   //! MeshDomain is not an union of domains
    {return false;}
    virtual bool isIntersection() const            //! MeshDomain is not an intersection of domains
    {return false;}
    virtual number_t numberOfElements() const      //! number of geometric elements of meshdomain
    {return geomElements.size();}
    virtual GeomElement* element(number_t k)       //! access to k-th element (k>=1), 0 if not available
    {
      if(k<1 || k>geomElements.size()) error("index_out_of_range","k", 1, geomElements.size());
      return geomElements[k-1];
    }
    virtual const GeomElement* element(number_t k) const      //! access to k-th element (k>=1), 0 if not available
    {
      if(k<1 || k>geomElements.size()) error("index_out_of_range","k", 1, geomElements.size());
      return geomElements[k-1];
    }
    virtual number_t order() const;                //!< order of geometric elements of domain

      PartitionData* partitionData()
     {
      return partitionData_p;
     }

    virtual void setMaterialId(number_t);          //!< set material id (>0) for all geom elements of domain
    virtual void setDomainId(number_t);            //!< set the DomainId of all elements of domain
    virtual bool colorFlag() const {return useColor;}
    virtual void setColorFlag(bool f) const {useColor=f;}

    //various tools
    virtual bool include(const GeomDomain&) const; //!< true if the current domain includes a given domain (const)
    bool include(const MeshDomain& d) const;       //!< true if the current domain includes a given mesh domain (const)
    bool isSideDomain() const;                     //!< true if MeshDomain is a side of meshdomain (first element has one parent)
    bool isSidesDomain() const;                    //!< true if MeshDomain is a set of element sides (isSidesOf_p!=nullptr)
    bool isInterface() const;                      //!< true if MeshDomain is an interface between domains (first element has more than one parent)
    bool isCrack() const;                          //!< true if MeshDomain is a side of a crack
    bool isSideOf(const MeshDomain& d) const;      //!< true if MeshDomain is a side of a given mesh domain
    std::set<number_t> nodeNumbers() const;        //!< return the set of all node numbers of meshdomain, ascending order
    std::set<number_t> vertexNumbers() const;      //!< return the set of all vertex numbers of meshdomain, ascending order
    std::vector<Point> nodes() const;              //!< return the set of all nodes of meshdomain, numbers ascending order
    std::vector<Point> vertices() const;           //!< return the set of all vertices of meshdomain, numbers ascending order
    real_t measure() const;                        //!< measure (length, surface or volume) of MeshDomain
    void computeStatistics(bool computeSideInfo=false); //!< update geometric domain info (measure, characteristic sizes, ...)
    bool isUnionOf(const std::vector<const GeomDomain*>&) const;           //!< check if an union of subdomains is equal to the current mesh domain
    const GeomDomain* largestDomain(std::vector<const GeomDomain*>) const; //!< return the largest domain including list of subdomains
    GeomDomain& extendDomain(bool useVertex=false,
                             const GeomDomain& omega = GeomDomain()) const;//!< find or create extension of a side domain
    GeomDomain& ficticiousDomain(const GeomDomain& omega) const;           //!< find or create a ficticious domain of a side domain
    GeomDomain& sidesDomain(const string_t& na="") const;                  //!< find or create sides domain related to current one
    GeomDomain& internalSidesDomain(const string_t& na="") const;          //!< find or create internal sides domain related to current one
    GeomDomain& removeElts(const std::set<GeomElement*>& elts, const string_t& namedom="") const; //!< remove set of GeomElements from current domain, creating new domain if different from current
    GeomDomain& removeElts(const GeomDomain& dom, const string_t& namedom="") const;              //!< remove dom GeomElements from current domain, creating new GeoDomain if different from current

    //tools to update informations
    void buildSides(std::vector<GeomElement*>& sidelts) const; //!< build vector of side elements of current domain
    void buildGeomData() const;                    //!< build measure and orientation of domain elements
    void updateDomainParents(bool keep=false);     //!< update parents if keep=false or  parents is empty
    void updateSides(std::map<string_t,std::vector<GeoNumPair> >&);  //!< update parent sides of elements of a side domain
    void buildVertexElements() const;              //!< build VertexElementsMap from geomElements
    void buildKdTree() const;                      //!< build KdTree
    const std::map<GeomElement*, std::list<GeoNumPair> >& buildSideToExt( const GeomDomain& = GeomDomain()) const; //!< build sideToExt map
    void buildIsoNodes() const;                    //!< build isoGeometric nodes in parameters domain
    void buildIsoNodesExt() const;                 //!< build isoGeometric nodes in parameters domain when multimaps
    void exportIsoNodes(const string_t &filename, number_t n=10) const; //! export to file the edges related to isoNodes (if defined) in a raw format
    template <typename Iterator>
    std::vector<std::pair<Point,GeomElement*> > projections(Iterator itp, number_t np) const; //!< build projections of points onto current domain
    std::vector<std::pair<Point,GeomElement*> > projections(const std::vector<Point>& pts, number_t np) const
    {return projections(pts.begin(),pts.size());}

    //tools to locate a point in domain
    GeomElement* locate(const Point&, bool silent=false) const;//!< locate element containing a given point
    GeomElement* nearest(Point& pt,real_t&) const;             //!< locate nearest element of a given point
    std::list<GeomElement*> elementsCloseTo(Point& pt) const;  //!< locate elements close to a given point

    // ================ tools to rebuild the domain
    //! set color of geom elements of domain according to vertex values and a coloring rule
    void setColor(const std::vector<real_t>&, const std::map<number_t,number_t>&, ColoringRule=defaultColoringRule) const;
    void setColor(const std::vector<Vector<real_t> >& val, const std::map<number_t,number_t>& vIndex, VectorColoringRule=defaultVectorColoringRule) const;
    //! rebuild  by collecting elements when color satisfies a criterium given by ComparisonFunction
    void rebuild(const ComparisonFunction<>&, bool restrictedToDomain=false);
    //! rebuild a side domain from plain domain elements
    void rebuild(const std::map<string_t, std::vector<GeoNumPair> >&, std::map<string_t, GeomElement*>&,
                 const std::map<GeomDomain*,std::set<GeomElement*>*>&);

    // =========== tools to deal with normal orientations
    std::pair<OrientationType, const GeomDomain*> normalOrientation() const //! return current orientation of normal vectors
    {return normalOrientationRule;}
    void setNormalOrientation(OrientationType, const GeomDomain&) const; //!< set normal orientation of a SpaceDim-1 meshdomain related to a GeomDomain
    void setNormalOrientation(OrientationType, const Point&) const;      //!< set normal orientation of a SpaceDim-1 meshdomain related to a Point
    void setNormalOrientation(OrientationType = _undefOrientationType, const GeomDomain* gp=nullptr, const Point* p=nullptr) const;  //!< set normal orientation of a SpaceDim-1 meshdomain
    void setOrientationForManifold(OrientationType = _towardsInfinite, const Point* P= nullptr) const;       //!< set normal orientation of a manifold
    void setOrientationForBoundary(OrientationType ort =_outwardsDomain, const GeomDomain*  =nullptr) const; //!< set normal orientation of a boundary
    void reverseOrientations() const; //!< reverse all the element orientations
    Vector<real_t> getNormal(const Point&, OrientationType = _undefOrientationType, const GeomDomain& = GeomDomain()) const;  //!< return normal to the current domain at a given point
    void clearGeomMapData(); //!< clear temporary GeomData structures of MeshElement's

    //in/out stuff
    virtual void print(std::ostream&) const; //!< print MeshDomain informations
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

std::ostream& operator<<(std::ostream&, const MeshDomain&); //!< print operator
// void addNode(Node<GeomElement>*, GeomElement*, std::map<GeomElement*,bool>&,std::map<string_t, std::list<GeomElement*> >&); //!< utility
void createPath(Node<GeomElement>*, std::map<GeomElement*,bool>&,std::map<string_t, std::list<GeomElement*> >&); //!< utility

/*!
   \class PartitionData
   to store general information of geometric domain
*/
class PartitionData
{
  private:
    //Donnees membres
    // !< list of elements sharing interface between other domains
    std::map<number_t, GeomElement*> elementsSharingInterface;

    std::vector<MeshDomain* >  meshdom_p;
    std::map<number_t, int_t>  elementsByPartition_m;
    std::map<number_t, int_t>  verticesByPartition_m;
    xlifepp::number_t          nbParts_;
    int*                       Options_;
    std::list<number_t>        interfaceVertices_l;
    std::list<number_t>        interfaceElements_l;
    std::vector<GeomElement*>  interfaceSides_v;
    // xlifepp::string_t          PartMesh_;
    xlifepp::number_t          PartMesh_;
    int                        Ncommon_, ptype_,ctype_, objtype_, rtype_, iptype_;
    xlifepp::real_t            balance_;
    xlifepp::number_t          objval_;
    xlifepp::number_t          nbInterfaceSides_;
    xlifepp::string_t          nmMeshFile_;


  private:
    //@{
    //! true constructor functions
    void buildParam(const Parameter& p);
    void build(const std::vector<Parameter>& ps);
    void buildPartitionParam(const Parameter& gp);
    void buildDefaultParam(ParameterKey key);
    void buildDefaultParam();
    std::set<ParameterKey> getParamsKeys();
    //@}
  public:
    PartitionData()
    {
      nbParts_=2; PartMesh_=dual; Ncommon_=1;
      nmMeshFile_="", nbInterfaceSides_=0;
      ptype_=_rec; ctype_=_rm; objtype_=_cut, rtype_=_fm, iptype_=_grow;
    }
    PartitionData(xlifepp::MeshDomain& mail, const std::vector<Parameter>& ps);

    //Get info
    int_t    Option(xlifepp::string_t Option );
    number_t nbParts()  {return nbParts_;}
    string_t nmMeshFile() {return nmMeshFile_;}
    number_t PartMesh() {return PartMesh_;}
    number_t Ncommon()  {return Ncommon_;}
    number_t objval()   {return objval_;}
    number_t balance()  {return balance_;}
    number_t nbInterfaceSides() { return nbInterfaceSides_;}

    //Fonctions pour le partitionnement
    // list of interface vertices
    std::list<number_t>& InterfaceVertices()          { return interfaceVertices_l; }
    // list of interface elements
    std::list<number_t>& InterfaceElements()          { return interfaceElements_l; }
    // Vector containing interface Sides number
    std::vector<GeomElement*> InterfaceSideElements() { return interfaceSides_v; }
    std::vector<xlifepp::MeshDomain* >  & meshdom()   { return meshdom_p; }
    xlifepp::MeshDomain* meshdom( number_t md)&       { return meshdom_p[md]; }

    std::map<number_t, int_t>  elementsByPartition() { return elementsByPartition_m; }
    std::map<number_t, int_t>  verticesByPartition() { return verticesByPartition_m; }
    // METIS partitionning is built by this function:
    xlifepp::number_t metisPartition(number_t& nbElts, number_t& nbVertices
                                    ,const std::vector< GeomElement * >&   VgeomElements
                                    ,int* epart);
    xlifepp::number_t EltsVertPartInterface( number_t& nbElts
                                           , const std::vector< GeomElement * >&  VgeomElements
                                           , int* epart);
    xlifepp::number_t partMeshdom( xlifepp::MeshDomain& Carre);
    xlifepp::number_t interfaceSides(number_t, const std::vector< GeomElement * >& VgeomElements);
    xlifepp::number_t buildOption(Parameters&  Param=defaultParameters
                                 ,int * options=nullptr);
    xlifepp::number_t meshDomainElementsPartition(number_t&, number_t&
                                                 ,const std::vector< GeomElement * >&);
    xlifepp::number_t elementPart(number_t numG);
    int               vertexPart(number_t& numG);
    xlifepp::number_t nbEltsPart(number_t part) {return elementsByPartition_m.size();}
    xlifepp::number_t nbVerticesPart(number_t part) {return verticesByPartition_m.size();}
};



/*!
   \class CompositeDomain
   class to handle union or intersection of domains
*/
class CompositeDomain : public GeomDomain
{
  protected:
    SetOperationType setOpType_;                            //!< type of the set operation
    std::vector<const GeomDomain*> domains_;                //!< list of domains
  public:
    CompositeDomain(SetOperationType, const std::vector<const GeomDomain*>&,
                    const string_t&);                       //!< constructor
    SetOperationType setOpType() const                      //! return type of operation (_union,_intersection)
    {return setOpType_;}
    string_t sortedName() const;                            //!< return domain name where composite elements are sorted by name (const)
    const std::vector<const GeomDomain*>& domains() const   //! access to list of domains (const)
    {return domains_;}
    virtual CompositeDomain* compositeDomain()              //! access to CompositeDomain pointer
    {return this;}
    virtual const CompositeDomain* compositeDomain() const  //! access to CompositeDomain pointer (const)
    {return this;}
    virtual bool isUnion() const                             //! true if an union of domains (const)
    {return setOpType_ == _union;}
    virtual bool isIntersection()const                      //! true if an intersection of domains (const)
    {return setOpType_ == _intersection;}
    virtual bool include(const GeomDomain&) const;          //!< true if the current domain includes a given domain (const)
    virtual bool isSideDomain() const;                      //!< true if composite domain is aside domain
    std::vector<const GeomDomain*> basicDomains() const;    //!< return the list of basic domains (recursive)
    virtual number_t numberOfElements() const;              //!< number of geometric elements of composite domain is not available
    virtual void print(std::ostream&) const;                //!< print CompositeDomain informations
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

std::ostream& operator<<(std::ostream&, const CompositeDomain&); //!< print operator

/*!
   \class AnalyticalDomain
   class to handle domain with analytical description
   to be developed later
*/
class AnalyticalDomain : public GeomDomain
{
  public:
    AnalyticalDomain* analyticalDomain()              //! access to AnalyticalDomain pointer
    {return this;}
    const AnalyticalDomain* analyticalDomain() const  //! access to AnalyticalDomain pointer (const)
    {return this;}
    virtual bool include(const GeomDomain&) const    //! true if the current domain includes a given domain (const)
    {return false;}                                  // no inclusion managed
};

/*!
   \class PointsDomain
   class handles a list of points (cloud)
*/
class PointsDomain : public GeomDomain
{
  public:
    std::vector<Point> points;                     //!< list of points

    PointsDomain(const std::vector<Point>&, const string_t& = "");  //!< PointsDomain constructor

    PointsDomain* pointsDomain()                   //! access to PointsDomain pointer
    {return this;}
    const PointsDomain* pointsDomain()  const      //! access to PointsDomain pointer (const)
    {return this;}
    const Point& point(number_t n) const           //! access to n-th point (n>=1) (const)
    {return points[n-1];}
    Point& point(number_t n)                       //! access to n-th point (n>=1)
    {return points[n-1];}
    virtual bool include(const GeomDomain&) const  //! true if the current domain includes a given domain (const)
    {return false;}                                  // no inclusion managed
    virtual void print(std::ostream&) const;       //!< print PointsDomain informations
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

std::ostream& operator<<(std::ostream&, const PointsDomain&); //!< print operator

//! useful typedef for domain objects
typedef std::pair<const GeomDomain*,const GeomDomain*> DomainPair;

/*! build the projection of a list of points (given by an iterator) onto the current domain
    return a vector<std::pair<Point,GeomElement*> > containing for each point the projection P and the GeomElement E containing P
    input:
      itp: iterator on the first point (*itp should be a Point&)
      np  : number of points to deal with

    loop on points(Vi)
      di=+inf
      loop on element of current side domain (Ek)
         compute Pik the projection of Vi on Ek and dik=|Pik-Vi|
         if(dik<di) Pi=Pik, di=dik
*/
template <typename Iterator>
std::vector<std::pair<Point,GeomElement*> > MeshDomain::projections(Iterator itp, number_t np) const
{
   std::vector<std::pair<Point,GeomElement*> > proj(np);
   std::vector<GeomElement*>::const_iterator itg;
   #ifdef XLIFEPP_WITH_OMP
   #pragma omp parallel for
   #endif // XLIFEPP_WITH_OMP
   for(number_t i=0; i<np; i++)
   {
     Iterator it=itp;
     for(number_t j=0;j<i;j++) it++;
     real_t di=theRealMax, dik; Point Pi, Pik;
     for(itg=geomElements.begin();itg!=geomElements.end();++itg)
     {
         MeshElement* melt=(*itg)->meshElement();
         if(melt==nullptr) melt=(*itg)->buildSideMeshElement();
         Pik=melt->projection(*it,dik);
         if(dik<di){di=dik;Pi=Pik;}
     }
     proj[i]=std::make_pair(Pi,*itg);
   }
   return proj;
}

//=================================================================================
// utilities related to GeomDomain or GeomElement
//=================================================================================

//@{
/*!
  extract GeomDomain or GeomElement information from Parameters
  \note if pointer parameter does not exist, it is created with 0 value (see Parameters.get member function)
*/
inline string_t domainNameFromParameters(Parameters& pars)
{
  const void* p=nullptr;
  const GeomDomain* dom=static_cast<const GeomDomain*>(pars.get("Domain pointer",p));
  if(dom!=nullptr) return dom->name();
  return "?";
}

inline number_t materialIdFromParameters(Parameters& pars)
{
  const void* p=nullptr;
  const GeomElement* gelt=static_cast<const GeomElement*>(pars.get("GeomElement pointer",p));
  if(gelt!=nullptr) return gelt->materialId;
  return 0;
}

inline number_t domainIdFromParameters(Parameters& pars)
{
  const void* p=nullptr;
  const GeomElement* gelt=static_cast<const GeomElement*>(pars.get("GeomElement pointer",p));
  if(gelt!=nullptr) return gelt->domainId;
  return 0;
}

inline const GeomDomain* domainFromParameters(Parameters& pars)
{
  const void* p=nullptr;
  return static_cast<const GeomDomain*>(pars.get("Domain pointer",p));
}

inline const GeomElement* geomElementFromParameters(Parameters& pars)
{
  const void* p=nullptr;
  return static_cast<const GeomElement*>(pars.get("GeomElement pointer",p));
}
//@}

//! extract normal vector from Parameters
inline Vector<real_t>& normalVectorFromParameters(const Parameters& pa)
{
  return const_cast<Vector<real_t>&>(*static_cast<const Vector<real_t>*>(pa("_n").p_));
}


//=================================================================================
//geometric facilities
//=================================================================================
bool pointInElement(const Point&, const GeomElement&, real_t tol = theEpsilon);
bool intersect(const GeomElement&, const GeomElement&, real_t tol = theEpsilon);

typedef PCollection<GeomDomain> Domains;  //!< collection of GeomDomain pointers

} // end of namespace xlifepp

#endif // GEOM_DOMAIN_HPP
