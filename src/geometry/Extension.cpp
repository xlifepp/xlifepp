/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Extension.cpp
  \author E. Lunéville
  \since 27 feb 2017
  \date  27 feb 2017

  \brief Implementation of xlifepp::Extension class members and related functions
*/


#include "Extension.hpp"


namespace xlifepp
{

string_t Extension::name() const
{
  string_t s="ext_"+sideDomain_->name();
  if(domain_!=nullptr) s+="_to_"+domain_->name();
  return s;
}

void Extension::print(std::ostream& out) const
{
  if(theVerboseLevel==0) return;
  out<<"Extension from "<<sideDomain_->name();
  if(domain_!=nullptr) out<<" to domain "<<domain_->name();
  out<<" using Lagrange FE  of order "<<order_;
  out<<", extended domain: "<<extendedDomain_->name();
  out<<eol;
}

//! create the map: extended domain element -> list of dofs on sideDomain (local number in domain element)
// works only for one order exentension
void Extension::buildDomToSide() const
{
  std::set<number_t> s0;
  std::set<number_t> sideDomNodes= sideDomain_->meshDomain()->nodeNumbers();
  const std::vector<GeomElement*>& elts = extendedDomain_->meshDomain()->geomElements;
  std::vector<GeomElement*>::const_iterator ite=elts.begin();
  for(ite=elts.begin(); ite!=elts.end(); ++ite)
    {
      domToSide[*ite] = s0;
      const MeshElement* melt=(*ite)->meshElement();
      for(number_t k=1; k<=melt->nodeNumbers.size(); k++)
        if(sideDomNodes.find(melt->nodeNumbers[k-1])!=sideDomNodes.end()) domToSide[*ite].insert(k);
    }
}

// compute extension function at a point,
//    sum_{i on sideDomain} wi(x)  where wi are shapevalues of geometric elements of extendedDomain
real_t Extension::operator()(const Point& x) const
{
  const MeshDomain* mdom=extendedDomain_->meshDomain();
  GeomElement* gelt=mdom->locate(x);
  if(gelt==nullptr) return 0.;
  return (*this)(x,*gelt);
}

real_t Extension::operator()(const Point& x, const GeomElement& elt) const
{
  if(domToSide.size()==0) buildDomToSide();
  //compute ShapeValues at point x
  MeshElement* melt = const_cast<MeshElement*>(elt.meshElement());
  if(melt == nullptr) melt = elt.buildSideMeshElement();  //it is a side element with no meshElement structure, build one
  if(melt->geomMapData_p ==nullptr) melt->geomMapData_p= new GeomMapData(melt);
  Point q =  melt->geomMapData_p->geomMapInverse(x);
  const RefElement* relt_p = melt->refElement();
  ShapeValues shv(*relt_p,false, false);
  relt_p->computeShapeValues(q.begin(),shv);
  std::set<number_t>& ns=domToSide[&const_cast< GeomElement&>(elt)];
  std::set<number_t>::iterator its=ns.begin();
  real_t res=0;
  for(; its!=ns.end(); ++its) res+= shv.w[*its-1];
  return res;
}

// compute gradient of extension function at a point
//    sum_{i on sideDomain} grad(wi)(x)  where wi are shapevalues of geometric elements of extendedDomain
std::vector<real_t> Extension::grad(const Point& x) const
{
  const MeshDomain* mdom=extendedDomain_->meshDomain();
  GeomElement* gelt=mdom->locate(x);
  if(gelt==nullptr) return 0*x;
  return grad(x,*gelt);
}

std::vector<real_t> Extension::grad(const Point& x, const GeomElement& elt) const
{
  if(domToSide.size()==0) buildDomToSide();
  //compute ShapeValues at point x
  MeshElement* melt = const_cast<MeshElement*>(elt.meshElement());
  if(melt == nullptr) melt = elt.buildSideMeshElement();  //it is a side element with no meshElement structure, build one
  if(melt->geomMapData_p ==nullptr) melt->geomMapData_p= new GeomMapData(melt);
  Point q =  melt->geomMapData_p->geomMapInverse(x);
  const RefElement* relt_p = melt->refElement();
  ShapeValues shv(*relt_p,true,false);
  relt_p->computeShapeValues(q.begin(),shv,true);
  std::set<number_t>& ns=domToSide[&const_cast< GeomElement&>(elt)];
  std::set<number_t>::iterator its=ns.begin();
  number_t d=shv.dw.size();
  std::vector<real_t> res(d,0);
  for(number_t i=0; i<d; i++)
    for(; its!=ns.end(); ++its) res[i]+= shv.dw[i][*its-1];
  return res;
}

/*! update shape values and nodes of a Lagrange element related to a given geometric element
    return false if element does not belong to the extension domain
       ext  : pointer to the Extension object giving the order of Lagrange approximation
       gelt : pointer to geometric element
       itx  : iterator on the reference point where to compute shapevalues
       der1 : if true compute derivative of shape values
       der2 : if true compute second derivative of shape values (NOT IMPLEMENTED)
*/
bool ExtensionData::compute(const Extension* ext, GeomElement* gelt, std::vector<real_t>::const_iterator itx, bool der1, bool der2)
{
  extension=ext;
  std::map<GeomElement*,std::set<number_t> >::const_iterator itm=extension->domToSide.find(gelt);
  if(itm==extension->domToSide.end()) return false;  //outside the extension
  RefElement* relt=findRefElement(gelt->shapeType(), findInterpolation(_Lagrange,_standard,extension->order_,_H1));
  ShapeValues shv(*relt,der1,der2);
  relt->computeShapeValues(itx, shv, der1, der2);
  const std::set<number_t>& ns=itm->second;
  number_t n=ns.size();
  nodes.resize(n); w.resize(n);
  number_t dim=0;
  if(der1)
    {
      dim=shv.dw.size();
      dw.resize(dim);
      for(number_t d=0; d<dim; d++) dw[d].resize(n);
    }
  std::set<number_t>::const_iterator its=ns.begin();
  std::vector<real_t>::iterator itw=w.begin();
  std::vector<Point>::iterator itp=nodes.begin();
  GeomMapData* mapdata= gelt->meshElement()->geomMapData_p;
  for(number_t k=0; k<n; ++k, ++its, ++itw, ++itp)
    {
      number_t i = *its;
      *itp =  mapdata->geomMap(relt->refDofs[i-1]->point());
      *itw = shv.w[i-1];
      if(der1)
        for(number_t d=0; d<dim; d++) dw[d][k] = shv.dw[d][i-1];
    }
  return true;
}

} // end of namespace xlifepp
