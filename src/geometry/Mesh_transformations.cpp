/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Ngdyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Mesh_transformations.cpp
  \author N. Kielbasiewicz
  \since 15 oct 2014
  \date 17 oct 2014

  \brief Implementation of xlifepp::Mesh class members and related functions about transformations
*/

#include "Mesh.hpp"
#include "utils.h"

namespace xlifepp
{

//! apply a geometrical transformation on a Mesh
Mesh& Mesh::transform(const Transformation& t)
{
  // copy a Mesh and apply transformation on nodes ( std::vector<Point> )
  if (t.transformType() == _composition)
  {
    for (number_t j=0; j < t.components().size(); ++j)
    { for (number_t i=0; i < nodes.size(); ++i) { nodes[i]=t.component(j)->apply(nodes[i]); } }
  }
  else { for (number_t i=0; i < nodes.size(); ++i) { nodes[i]=t.apply(nodes[i]); } }
  geometry_p->transform(t);
  return *this;
}

//! apply a translation on a Mesh (one key)
Mesh& Mesh::translate(const Parameter& p1)
{
  return this->transform(Translation(p1));
}

//! apply a translation on a Mesh (vector version)
Mesh& Mesh::translate(std::vector<real_t> u)
{
  warning("deprecated", "Mesh::translate(Reals)", "Mesh::translate(_direction=xxx)");
  return this->transform(Translation(_direction=u));
}

//! apply a translation on a Mesh (3 reals version)
Mesh& Mesh::translate(real_t ux, real_t uy, real_t uz)
{
  warning("deprecated", "Mesh::translate(Real, Real, Real)", "Mesh::translate(_direction={ux, uy, uz})");
  return this->transform(Translation(_direction={ux, uy, uz}));
}

//! apply a rotation on a Mesh (one key)
Mesh& Mesh::rotate2d(const Parameter& p1)
{
  return this->transform(Rotation2d(p1));
}

//! apply a rotation on a Mesh (2 keys)
Mesh& Mesh::rotate2d(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Rotation2d(p1, p2));
}

//! apply a rotation on a Mesh
Mesh& Mesh::rotate2d(const Point& c, real_t angle)
{
  warning("deprecated", "Mesh::rotate2d(Point, Real)", "Mesh::rotate2d(_center=xxx, _angle=yyy)");
  return this->transform(Rotation2d(_center=c, _angle=angle));
}

//! apply a rotation on a Mesh (one key)
Mesh& Mesh::rotate3d(const Parameter& p1)
{
  return this->transform(Rotation3d(p1));
}

//! apply a rotation on a Mesh (2 keys)
Mesh& Mesh::rotate3d(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Rotation3d(p1, p2));
}

//! apply a rotation on a Mesh (3 keys)
Mesh& Mesh::rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  return this->transform(Rotation3d(p1, p2, p3));
}

//! apply a rotation on a Mesh
Mesh& Mesh::rotate3d(const Point& c, std::vector<real_t> d, real_t angle)
{
  warning("deprecated", "Mesh::rotate3d(Point, Reals, Real)", "Mesh::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
  return this->transform(Rotation3d(_center=c, _axis=d, _angle=angle));
}

//! apply a rotation on a Mesh
Mesh& Mesh::rotate3d(real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "Mesh::rotate3d(Real, Real, Real)", "Mesh::rotate3d(_axis={xxx,yyy}, _angle=zzz)");
  return this->transform(Rotation3d(_center=Point(0.,0.,0.), _axis={dx, dy}, _angle=angle));
}

//! apply a rotation on a Mesh
Mesh& Mesh::rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "Mesh::rotate3d(Real, Real, Real, Real)", "Mesh::rotate3d(_axis={xxx, yyy, zzz}, _angle=ttt)");
  return this->transform(Rotation3d(_center=Point(0.,0.,0.), _axis={dx, dy, dz}, _angle=angle));
}

//! apply a rotation on a Mesh
Mesh& Mesh::rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "Mesh::rotate3d(Point, Real, Real, Real)", "Mesh::rotate3d(_center=xxx, _axis={dx, dy}, _angle=ttt)");
  return this->transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
}

//! apply a rotation on a Mesh
Mesh& Mesh::rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "Mesh::rotate3d(Point, Real, Real, Real, Real)", "Mesh::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=zzz)");
  return this->transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
}

//! apply a homothety on a Mesh (one key)
Mesh& Mesh::homothetize(const Parameter& p1)
{
  return this->transform(Homothety(p1));
}

//! apply a homothety on a Mesh (2 keys)
Mesh& Mesh::homothetize(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Homothety(p1, p2));
}

//! apply a homothety on a Mesh
Mesh& Mesh::homothetize(const Point& c, real_t factor)
{
  warning("deprecated", "Mesh::homothetize(Point, Real)", "Mesh::homothetize(_center=xxx, _scale=yyy)");
  return this->transform(Homothety(_center=c, _scale=factor));
}

//! apply a homothety on a Mesh
Mesh& Mesh::homothetize(real_t factor)
{
  warning("deprecated", "Mesh::homothetize(Real)", "Mesh::homothetize(_scale=yyy)");
  return this->transform(Homothety(_center=Point(0.,0.,0.), _scale=factor));
}

//! apply a point reflection on a Mesh
Mesh& Mesh::pointReflect(const Parameter& p1)
{
  return this->transform(PointReflection(p1));
}


//! apply a point reflection on a Mesh
Mesh& Mesh::pointReflect(const Point& c)
{
  warning("deprecated", "Mesh::pointReflect(Point)", "Mesh::pointReflect(_center=xxx)");
  return this->transform(PointReflection(_center=c));
}

//! apply a reflection2d on a Mesh (one key)
Mesh& Mesh::reflect2d(const Parameter& p1)
{
  return this->transform(Reflection2d(p1));
}

//! apply a reflection2d on a Mesh (2 keys)
Mesh& Mesh::reflect2d(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Reflection2d(p1, p2));
}

//! apply a reflection2d on a Mesh
Mesh& Mesh::reflect2d(const Point& c, std::vector<real_t> d)
{
  warning("deprecated", "Mesh::reflect2d(Point, Reals)", "Mesh::reflect2d(_center=xxx, _direction=yyy)");
  return this->transform(Reflection2d(_center=c, _direction=d));
}

//! apply a reflection2d on a Mesh
Mesh& Mesh::reflect2d(const Point& c, real_t dx, real_t dy)
{
  warning("deprecated", "Mesh::reflect2d(Point, Real, Real)", "Mesh::reflect2d(_center=xxx, _direction={yyy, zzz})");
  return this->transform(Reflection2d(_center=c, _direction={dx, dy}));
}

//! apply a reflection3d on a Mesh (one key)
Mesh& Mesh::reflect3d(const Parameter& p1)
{
  return this->transform(Reflection3d(p1));
}

//! apply a reflection3d on a Mesh (2 keys)
Mesh& Mesh::reflect3d(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Reflection3d(p1, p2));
}

//! apply a reflection3d on a Mesh
Mesh& Mesh::reflect3d(const Point& c, std::vector<real_t> n)
{
  warning("deprecated", "Mesh::reflect3d(Point, Reals)", "Mesh::reflect3d(_center=xxx, _normal=yyy)");
  return this->transform(Reflection3d(_center=c, _normal=n));
}

//! apply a reflection3d on a Mesh
Mesh& Mesh::reflect3d(const Point& c, real_t nx, real_t ny, real_t nz)
{
  warning("deprecated", "Mesh::reflect3d(Point, Real, Real, Real)", "Mesh::reflect3d(_center=xxx, _normal={nx, ny, nz})");
  return this->transform(Reflection3d(_center=c, _normal={nx, ny, nz}));
}

//! apply a geometrical transformation on a Mesh (external)
Mesh transform(const Mesh& m, const Transformation& t, const string_t& meshname, const string_t& suffix)
{
  // copy a Mesh and apply transformation on nodes ( std::vector<Point> )
  Mesh m2;

  if (t.transformType() == _composition)
  {
    for (number_t j=0; j < t.components().size(); ++j)
    { for (number_t i=0; i < m.nodes.size(); ++i) { m2.nodes.push_back(t.component(j)->apply(m.nodes[i])); } }
  }
  else { for (number_t i=0; i < m.nodes.size(); ++i) { m2.nodes.push_back(t.apply(m.nodes[i])); } }

  m2.copyAllButNodes(m);
  m2.geometry_p->transform(t);
  if (meshname != "") { m2.name(meshname); }
  if (suffix != "") { m2.addSuffix(suffix); }
  return m2;
}

//! apply a translation on a Mesh (no vector) (external)
Mesh translate(const Mesh& m)
{
  return transform(m, Translation());
}

//! apply a translation on a Mesh (1 key) (external)
Mesh translate(const Mesh& m, const Parameter& p1)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Translation(ps), meshname, suffix);
}

//! apply a translation on a Mesh (2 keys) (external)
Mesh translate(const Mesh& m, const Parameter& p1, const Parameter& p2)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Translation(ps), meshname, suffix);
}

//! apply a translation on a Mesh (3 keys) (external)
Mesh translate(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Translation(ps), meshname, suffix);
}

//! apply a translation on a Mesh (vector version) (external)
Mesh translate(const Mesh& m, std::vector<real_t> u)
{
  warning("deprecated", "translate(Mesh, Reals)", "translate(Mesh, _direction=xxx)");
  return transform(m, Translation(_direction=u));
}

//! apply a translation on a Mesh (3 reals version) (external)
Mesh translate(const Mesh& m, real_t ux, real_t uy, real_t uz)
{
  warning("deprecated", "translate(Mesh, Real, Real, Real)", "translate(Mesh, _direction={ux,uy,uz})");
  return transform(m, Translation(_direction={ux, uy, uz}));
}

//! apply a rotation 2D on a Mesh (1 key) (external)
Mesh rotate2d(const Mesh& m, const Parameter& p1)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Rotation2d(ps), meshname, suffix);
}

//! apply a rotation 2D on a Mesh (2 keys) (external)
Mesh rotate2d(const Mesh& m, const Parameter& p1, const Parameter& p2)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Rotation2d(ps), meshname, suffix);
}

//! apply a rotation 2D on a Mesh (3 keys) (external)
Mesh rotate2d(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Rotation2d(ps), meshname, suffix);
}

//! apply a rotation 2D on a Mesh (4 keys) (external)
Mesh rotate2d(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3, p4};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Rotation2d(ps), meshname, suffix);
}

//! apply a rotation 2D on a Mesh (external)
Mesh rotate2d(const Mesh& m, const Point& c, real_t angle)
{
  warning("deprecated", "rotate2d(Mesh, Point, Real)", "rotate2d(Mesh, _center=xxx, _angle=yyy)");
  return transform(m, Rotation2d(_center=c, _angle=angle));
}

//! apply a rotation 3D on a Mesh (4 keys) (external)
Mesh rotate3d(const Mesh& m, const Parameter& p1)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Rotation3d(ps), meshname, suffix);
}

//! apply a rotation 3D on a Mesh (4 keys) (external)
Mesh rotate3d(const Mesh& m, const Parameter& p1, const Parameter& p2)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Rotation3d(ps), meshname, suffix);
}

//! apply a rotation 3D on a Mesh (4 keys) (external)
Mesh rotate3d(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Rotation3d(ps), meshname, suffix);
}

//! apply a rotation 3D on a Mesh (4 keys) (external)
Mesh rotate3d(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3, p4};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Rotation3d(ps), meshname, suffix);
}

//! apply a rotation 3D on a Mesh (4 keys) (external)
Mesh rotate3d(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Rotation3d(ps), meshname, suffix);
}

//! apply a rotation 3D on a Mesh (external)
Mesh rotate3d(const Mesh& m, const Point& c, std::vector<real_t> d, real_t angle)
{
  warning("deprecated", "rotate3d(Mesh, Point, Reals, Real)", "rotate3d(Mesh, _center=xxx, _axis=yyy, _angle=zzz)");
  return transform(m, Rotation3d(_center=c, _axis=d, _angle=angle));
}

//! apply a rotation 3D on a Mesh (external)
Mesh rotate3d(const Mesh& m, real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "rotate3d(Mesh, Real, Real, Real)", "rotate3d(Mesh, _axis={xxx, yyy}, _angle=zzz)");
  return transform(m, Rotation3d(_center=Point(0.,0.,0.), _axis={dx, dy}, _angle=angle));
}

//! apply a rotation 3D on a Mesh (external)
Mesh rotate3d(const Mesh& m, real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "rotate3d(Mesh, Real, Real, Real, Real)", "rotate3d(Mesh, _axis={dx, dy, dz}, _angle=yyy)");
  return transform(m, Rotation3d(_center=Point(0.,0.,0.), _axis={dx, dy, dz}, _angle=angle));
}

//! apply a rotation 3D on a Mesh (external)
Mesh rotate3d(const Mesh& m, const Point& c, real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "rotate3d(Mesh, Point, Real, Real, Real)", "rotate3d(Mesh, _center=xxx, _axis={dx, dy}, _angle=yyy)");
  return transform(m, Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
}

//! apply a rotation 3D on a Mesh (external)
Mesh rotate3d(const Mesh& m, const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "rotate3d(Mesh, Point, Real, Real, Real, Real)", "rotate3d(Mesh, _center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
  return transform(m, Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
}

//! apply a homothety on a Mesh (1 key) (external)
Mesh homothetize(const Mesh& m, const Parameter& p1)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Homothety(ps), meshname, suffix);
}

//! apply a homothety on a Mesh (2 keys) (external)
Mesh homothetize(const Mesh& m, const Parameter& p1, const Parameter& p2)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Homothety(ps), meshname, suffix);
}

//! apply a homothety on a Mesh (3 keys) (external)
Mesh homothetize(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Homothety(ps), meshname, suffix);
}

//! apply a homothety on a Mesh (4 keys) (external)
Mesh homothetize(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3, p4};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Homothety(ps), meshname, suffix);
}

//! apply a homothety on a Mesh (external)
Mesh homothetize(const Mesh& m, const Point& c, real_t factor)
{
  warning("deprecated", "homothetize(Mesh, Point, Real)", "homothetize(Mesh, _center=xxx, _scale==yyy)");
  return transform(m, Homothety(_center=c, _scale=factor));
}

//! apply a homothety on a Mesh (external)
Mesh homothetize(const Mesh& m, real_t factor)
{
  warning("deprecated", "homothetize(Mesh, Real)", "homothetize(Mesh, _scale==xxx)");
  return transform(m, Homothety(_center=Point(0.,0.,0.), _scale=factor));
}

//! apply a point reflection on a Mesh (1 keys) (external)
Mesh pointReflect(const Mesh& m, const Parameter& p1)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, PointReflection(ps), meshname, suffix);
}

//! apply a point reflection on a Mesh (2 keys) (external)
Mesh pointReflect(const Mesh& m, const Parameter& p1, const Parameter& p2)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, PointReflection(ps), meshname, suffix);
}

//! apply a point reflection on a Mesh (3 keys) (external)
Mesh pointReflect(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, PointReflection(ps), meshname, suffix);
}

//! apply a point reflection on a Mesh (external)
Mesh pointReflect(const Mesh& m, const Point& c)
{
  warning("deprecated", "pointReflect(Mesh, Point)", "pointReflect(Mesh, _center==xxx)");
  return transform(m, PointReflection(_center=c));
}

//! apply a reflection 2D on a Mesh (1 key) (external)
Mesh reflect2d(const Mesh& m, const Parameter& p1)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Reflection2d(ps), meshname, suffix);
}

//! apply a reflection 2D on a Mesh (2 keys) (external)
Mesh reflect2d(const Mesh& m, const Parameter& p1, const Parameter& p2)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Reflection2d(ps), meshname, suffix);
}

//! apply a reflection 2D on a Mesh (3 keys) (external)
Mesh reflect2d(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Reflection2d(ps), meshname, suffix);
}

//! apply a reflection 2D on a Mesh (4 keys) (external)
Mesh reflect2d(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3, p4};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Reflection2d(ps), meshname, suffix);
}

//! apply a reflection 2D on a Mesh (external)
Mesh reflect2d(const Mesh& m, const Point& c, std::vector<real_t> d)
{
  warning("deprecated", "reflect2d(Mesh, Point, Reals)", "reflect2d(Mesh, _center==xxx, _direction=yyy)");
  return transform(m, Reflection2d(c, d));
}

//! apply a reflection 2D on a Mesh (external)
Mesh reflect2d(const Mesh& m, const Point& c, real_t dx, real_t dy)
{
  warning("deprecated", "reflect2d(Mesh, Point, Reals)", "reflect2d(Mesh, _center==xxx, _direction={ux, uy})");
  return transform(m, Reflection2d(c, dx, dy));
}

//! apply a reflection 3D on a Mesh (1 key) (external)
Mesh reflect3d(const Mesh& m, const Parameter& p1)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Reflection3d(ps), meshname, suffix);
}

//! apply a reflection 3D on a Mesh (2 keys) (external)
Mesh reflect3d(const Mesh& m, const Parameter& p1, const Parameter& p2)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Reflection3d(ps), meshname, suffix);
}

//! apply a reflection 3D on a Mesh (3 keys) (external)
Mesh reflect3d(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Reflection3d(ps), meshname, suffix);
}

//! apply a reflection 3D on a Mesh (4 keys) (external)
Mesh reflect3d(const Mesh& m, const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  string_t meshname="", suffix="";
  std::vector<Parameter> ps={p1, p2, p3, p4};
  buildNameAndSuffixTransformParams(ps, meshname, suffix);
  return transform(m, Reflection3d(ps), meshname, suffix);
}

//! apply a reflection 3D on a Mesh (external)
Mesh reflect3d(const Mesh& m, const Point& c, std::vector<real_t> n)
{
  warning("deprecated", "reflect3d(Mesh, Point, Reals)", "reflect3d(Mesh, _center==xxx, _normal=yyy)");
  return transform(m, Reflection3d(c, n));
}

//! apply a reflection 3D on a Mesh (external)
Mesh reflect3d(const Mesh& m, const Point& c, real_t nx, real_t ny, real_t nz)
{
  warning("deprecated", "reflect3d(Mesh, Point, Reals)", "reflect3d(Mesh, _center==xxx, _normal={nz, ny, nz})");
  return transform(m, Reflection3d(c, nx, ny, nz));
}

} // end of namespace xlifepp
