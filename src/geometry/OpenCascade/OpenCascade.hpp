/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file OpenCascade.hpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 23 apr 2020
  \date 23 apr 2020

  \brief Definition of the xlifepp::OpenCascade stuff

*/

#ifndef OPENCASCADE_HPP
#define OPENCASCADE_HPP

#ifdef XLIFEPP_WITH_OPENCASCADE
  #include "config.h"
  #include "utils.h"
  #include "OpenCascade.h"

  namespace xlifepp
  {

  /*! OCData class relating an xlife++ geometry to OpenCascade Brep geometry (TopoDS_Shape)
  */
  class OCData
  {
    private:
      TopoDS_Shape* OCShape_;          //!< main OC object representing any shape
      TDocStd_Document* OCDoc_; //!< additional properties related to OC shape

      // management of shapes tag (related to GMSH tags)
      TopTools_DataMapOfShapeInteger vertex2Tag, edge2Tag, wire2Tag, shell2Tag, face2Tag, solid2Tag;
      TopTools_DataMapOfIntegerShape tag2Vertex, tag2Edge, tag2Wire, tag2Shell, tag2Face, tag2Solid;
      int lastVertexTag, lastEdgeTag, lastWireTag, lastShellTag, lastFaceTag, lastSolidTag;
      void buildOCTags(const TopoDS_Vertex&);                       //!< tag an OC vertex
      void buildOCTags(const TopoDS_Edge&, bool recursive=true);    //!< tag an OC Edge
      void buildOCTags(const TopoDS_Wire&, bool recursive=true);    //!< tag an OC Wire
      void buildOCTags(const TopoDS_Face&, bool recursive=true);    //!< tag an OC Face
      void buildOCTags(const TopoDS_Shell&, bool recursive=true);   //!< tag an OC Shell
      void buildOCTags(const TopoDS_Solid&, bool recursive=true);   //!< tag an OC Solid
      void buildOCTags(bool recursive=true);       //!< tag an OC Shape

      void buildOCShape(const Geometry&);   //!< create OpenCascade data related to any geometry
      void buildOCDocG(const Geometry&);    //!< create TDocStd_Document related to any Geometry
      void buildOCDoc(const Curve&);        //!< create TDocStd_Document related to any Curve (1D)

    public:
        OCData(TopoDS_Shape* ocshape=nullptr, TDocStd_Document* ocdoc=nullptr)  //! basic constructor
          : OCShape_(ocshape), OCDoc_(ocdoc) {}
      OCData(const Geometry& shape)  //! constructor from geometry
          {buildOCShape(shape); if(OCDoc_==nullptr) buildOCTags();}
        ~OCData();
        OCData(const OCData&);            //!< copy constructor
        OCData& operator=(const OCData&);  //!< assignment
        void setOCDoc(TDocStd_Document* doc)
        {OCDoc_=doc;}

        //! return internal OC shape object as TopoDS_Shape reference
        const TopoDS_Shape& topoDS() const {return *OCShape_;}
        TopoDS_Shape* topoDSP() const {return OCShape_;}
        //! return some OC properties (names, hsteps, nnodes) as TDocStd_Document reference
        const TDocStd_Document& OCDoc() const {return *OCDoc_; }
        TDocStd_Document* OCDocH() const {return OCDoc_; }
        ShapeDocPair asShapeDocPair() const;

        //! true if OC shape is null
        bool isNull() const {return OCShape_==nullptr;}
        bool hasDoc() const {return OCDoc_!=nullptr;}

        void transform(const Transformation& t);  //!< apply a transformation to TopoDs_Shape (TDocStd_Document not changed)

        void print(std::ostream&, bool recursive=true) const;         //!< dump OCData on ostream
        void print(CoutStream&, bool recursive=true) const;           //!< dump OCData on CoutStream
        void printDoc(std::ostream& out) const;  //!< print doc part

        void saveToStep(const string_t&) const;  //!< export OCShape_ to STEP file
        void saveToBrep(const string_t&) const;  //!< export OCShape_ to BREP file
        void updateDoc(const string_t&);         //!< update indexes of shape regarding the step file
  };

  inline std::ostream& operator<<(std::ostream& out, const OCData& oc) {oc.print(out); return out;}
  inline CoutStream& operator<<(CoutStream& out, const OCData& oc) {oc.print(out); return out;}


  // external tools
  gp_Pnt to_gpPnt(const std::vector<real_t>&);           //!< xlifepp point to OpenCascade point
  gp_XYZ to_gpXYZ(const std::vector<real_t>&);           //!< xlifepp point to OpenCascade XYZ point
  gp_Dir to_gpDir(const std::vector<real_t>&);           //!< xlifepp point to OpenCascade dir
  gp_Vec to_gpVec(const std::vector<real_t>&);           //!< xlifepp point to OpenCascade vec
  Point toPoint(const gp_Pnt&);                          //!< OpenCascade point to xlifepp Point

  const Standard_GUID& addNameAttribute(TDF_Label&, const string_t&);    //!< add a name attribute to a TDF_Label
  const Standard_GUID& addIntegerAttribute(TDF_Label&, int_t);           //!< add a integer attribute to a TDF_Label
  const Standard_GUID& addRealAttribute(TDF_Label&, real_t);             //!< add a real attribute to a TDF_Label
  const Standard_GUID& addNamedDataAttribute(TDF_Label&, int_t, const string_t&, int_t=0, int_t=0, real_t=0); //!<  add a named data attribute to TDF_Label

  TopoDS_Shape* ocGeometry(Geometry&);                   //!< create OpenCascade object related to any Geometry (calling others)
  TopoDS_Shape* ocSegment(Segment&);                     //!< create OpenCascade object related to Segment (1D)
  TopoDS_Shape* ocCircArc(CircArc&);                     //!< create OpenCascade object related to CircArc (1D)
  TopoDS_Shape* ocEllArc(EllArc&);                       //!< create OpenCascade object related to EllArc  (1D)
  TopoDS_Shape* ocSplineArc(SplineArc&);                 //!< create OpenCascade object related to SplineArc (1D) - only BSpline arc
  TopoDS_Shape* ocParametrizedArc(ParametrizedArc&);     //!< create OpenCascade object (Bspline) related to ParametrizedArc(1D)
  ShapeDocPair  ocParametrizedArcLin(ParametrizedArc&);  //!< create OpenCascade object (segment union) related to ParametrizedArc(1D)
  TopoDS_Shape* ocPolygon(Polygon&);                     //!< create OpenCascade object related to Polygon (2D)
  TopoDS_Shape* ocEllipse(Ellipse&);                     //!< create OpenCascade object related to Ellipse (2D)
  TopoDS_Shape* ocDisk(Disk&);                           //!< create OpenCascade object related to Disk (2D)
  ShapeDocPair  ocParametrizedSurface(ParametrizedSurface&);   //!< create OpenCascade object (Nurbs) related to ParametrizedSurface(2D)
  ShapeDocPair  ocParametrizedSurfaceLin(ParametrizedSurface&);//!< create OpenCascade object (triangle union or quadrangle union) related to ParametrizedSurface(2D
  ShapeDocPair  ocSplineSurface(SplineSurface&);         //!< create OpenCascade object to SplineSurface(2D)
  TopoDS_Shape* ocCuboid(Cuboid&);                       //!< create OpenCascade object related to full Cuboid (3D)
  TopoDS_Shape* ocPolyhedron(Polyhedron&);               //!< create OpenCascade object related to Polyhedron (3D)
  TopoDS_Shape* ocEllipsoid(Ellipsoid&);                 //!< create OpenCascade object related to Ellipsoid (3D)
  TopoDS_Shape* ocTrunk(Trunk&);                         //!< create OpenCascade object related to Trunk (3D)
  TopoDS_Shape* ocRevTrunk(RevTrunk&);                   //!< create OpenCascade object related to RevTrunk (3D)

  ShapeDocPair ocLoop(Geometry&);                        //!< create OpenCascade object related to loop geometry
  ShapeDocPair ocExtrusion(Geometry&);                   //!< create OpenCascade object related to extrusion geometry
  ShapeDocPair ocComposite(Geometry&);                   //!< create OpenCascade object related to composite geometry
  ShapeDocPair ocShape(Geometry&);                       //!< create OpenCascade object related to OC geometry
  ShapeDocPair ocBooleanOperation(GeoOperation, ShapeDocPair&, ShapeDocPair&); //!< create oc boolean object (fuse,cut,..) from OCData's

  //internal tools related to TDF construction
  void dependanceMap(const TopoDS_Shape& newShape, const std::list<const TopoDS_Shape*>& shapes, const BRepTools_History& hist,
                    std::map<int ,TopoDS_Shape>& hashShape, std::map<int ,TopoDS_Shape>& hashNewShape,
                    std::map<int ,std::pair<number_t,std::list<std::pair<int,int> > > >& newToOld);
  void setLabels(TopoDS_Shape&, TDF_Label&, TopAbs_ShapeEnum, std::map<int ,std::pair<number_t,std::list<std::pair<int,int> > > >& ,
                std::map<int ,TopoDS_Shape>&, std::map<int ,TopoDS_Shape>&, std::map<int,Handle(TDataStd_NamedData)>&);

  string_t asString(TopAbs_ShapeEnum);                   //!< TopAbs_ShapeEnum as string
  string_t asString(const TopoDS_Shape&);                //!< brief description of a TopoDS_shape

  void print(const TopoDS_Shape&,std::ostream& out, bool recursive=true);  //!< main print function on standard ostream
  void print(const TopoDS_Shape&,CoutStream& out, bool recursive=true);   //!< main print function on CoutStream
  void print(const TopoDS_Vertex&,std::ostream& out, number_t lev=0);
  void print(const TopoDS_Edge&,std::ostream& out, number_t lev=0, bool recursive=true);
  void print(const TopoDS_Wire&,std::ostream& out, number_t lev=0, bool recursive=true);
  void print(const TopoDS_Face&,std::ostream& out, number_t lev=0, bool recursive=true);
  void print(const TopoDS_Shell&,std::ostream& out, number_t lev=0, bool recursive=true);
  void print(const TopoDS_Solid&,std::ostream& out, number_t lev=0, bool recursive=true);
  void printInfo(const TopoDS_Shape&, std::ostream&, number_t maxv=6); //!< compact print of TopoDS_shape (type and vertices) on standard ostream
  void printInfo(const TopoDS_Shape&, CoutStream&,  number_t maxv=6);  //!< compact print of TopoDS_shape (type and vertices) on CoutStream


  std::list<Point> OCvertices(const TopoDS_Shape&, real_t tol=theTolerance); //!< cleaned OC vertices list of a an OC shape (do not duplicate vertices close to tol)
  real_t length(const TopoDS_Edge &);                    //!< edge length (vertices distance)
  number_t commonPoints(const std::list<Point>&, const std::vector<const Point*>&, real_t tol=theTolerance); //!< number of common points between two lists of points
  number_t commonPoints(const std::list<Point>&, const std::list<Point>&, real_t tol= theTolerance);
  number_t sameNnodes(const std::vector<number_t>&);     //!< return the number of nodes if nnodes are all the same (>0) else return 0
  real_t sameHsteps(const std::vector<real_t>&);         //!< return the common hstep if hsteps are all the same (>0) else return 0
  GeomAbs_CurveType edgeType(const TopoDS_Edge&);        //!< return type of TopoDS_Edge
  string_t asString(GeomAbs_CurveType);                  //!< type of curve as String
  GeomAbs_SurfaceType faceType(const TopoDS_Face&);      //!< return type of TopoDS_Face
  string_t asString(GeomAbs_SurfaceType);                //!< type of face as String
  TopAbs_ShapeEnum toTopAbsShapeEnum(OCShapeType);       //!< convert OCShapeType to TopAbs_ShapeEnum

  void dump(const TopoDS_Shape&,std::ostream&);          //!< dump TopoDS_Shape info on ostream (OC tool)
  void dump(const TopoDS_Shape&,CoutStream&);            //!< dump TopoDS_Shape info on CoutStream (OC tool)
  void dump(BRepTools_History&,std::ostream&);           //!< dump BrepTools history on ostream (OC tool)
  void dump(BRepTools_History&, CoutStream&);            //!< dump BrepTools history on CoutStream (OC tool)
  void viewHistory(const std::map<int ,TopoDS_Shape>&, const BRepTools_History&); //!< print history of initial shapes

  void saveToStep(const TopoDS_Shape&, const string_t&); //!< save TopoDS_Shape to STEP file
  TopoDS_Shape loadFromStep(const string_t&);            //!< load STEP file as a TopoDS_Shape
  void saveToBrep(const TopoDS_Shape&, const string_t&); //!< save TopoDS_Shape to BREP file
  void changeBrepHeader(const string_t&);                //!< change header in BREP file

  inline std::ostream& operator<<(std::ostream& out, const TopoDS_Shape& ts) {dump(ts,out); return out;}
  inline CoutStream& operator<<(CoutStream& out, const TopoDS_Shape& ts) {dump(ts,out); return out;}

  //==========================================================================================================
  //                    Inherited class Geom_ParametrizedCurve of OC Geom_BoundedCurve
  //==========================================================================================================
  class ParametrizeArc;

  /*! Inherited class Geom_ParametrizedCurve of OC Geom_BoundedCurve to relate XLiFE++ ParametrizedArc
  */
  class Geom_ParametrizedCurve: public Geom_BoundedCurve
  {
    public:
    const ParametrizedArc* parc_;    //explicit link to the XLiFE++ parametrized arc

    Geom_ParametrizedCurve(const ParametrizedArc* parc): parc_(parc) {}

    //! Changes the direction of parametrization of <me>.
    //! The StartPoint of the initial curve becomes the EndPoint of the reversed curve
    //! the EndPoint of the initial curve becomes the StartPoint of the reversed curve.
    Standard_EXPORT void Reverse() Standard_OVERRIDE;

    //! Returns the  parameter on the  reversed  curve for the point of parameter U on <me>.
    //! returns UFirst + ULast - U
    Standard_EXPORT Standard_Real ReversedParameter (const Standard_Real U) const Standard_OVERRIDE;

    //! Returns the continuity of the curve, the curve is at least C0. Raised if N < 0.
    Standard_EXPORT Standard_Boolean IsCN (const Standard_Integer N) const Standard_OVERRIDE;

    //! Returns true if the distance between the first point and the last point of the curve is lower or equal to Resolution
    Standard_EXPORT Standard_Boolean IsClosed() const Standard_OVERRIDE;

    //! Returns True if the curve is periodic.
    Standard_EXPORT Standard_Boolean IsPeriodic() const Standard_OVERRIDE;

    //! Returns the global continuity of the curve  C0, C1, C2, C3, CN  (the order of continuity is infinite).
    Standard_EXPORT GeomAbs_Shape Continuity() const Standard_OVERRIDE;

    //! Returns in P the point of parameter U.
    Standard_EXPORT void D0 (const Standard_Real U, gp_Pnt& P) const Standard_OVERRIDE;

    //! Raised if the continuity of the curve is not C1.
    Standard_EXPORT void D1 (const Standard_Real U, gp_Pnt& P, gp_Vec& V1) const Standard_OVERRIDE;

    //! Raised if the continuity of the curve is not C2.
    Standard_EXPORT void D2 (const Standard_Real U, gp_Pnt& P, gp_Vec& V1, gp_Vec& V2) const Standard_OVERRIDE;

    //! Raised if the continuity of the curve is not C3.
    Standard_EXPORT void D3 (const Standard_Real U, gp_Pnt& P, gp_Vec& V1, gp_Vec& V2, gp_Vec& V3) const Standard_OVERRIDE;

    //! For the point of parameter U computes the vector corresponding to the Nth derivative.
    Standard_EXPORT gp_Vec DN (const Standard_Real U, const Standard_Integer N) const Standard_OVERRIDE;

    //! Returns the start point of the curve
    Standard_EXPORT gp_Pnt StartPoint() const Standard_OVERRIDE;

    //! Returns the last point of the curve
    Standard_EXPORT gp_Pnt EndPoint() const Standard_OVERRIDE;

    //! Returns the value of the first parameter
    Standard_EXPORT Standard_Real FirstParameter() const Standard_OVERRIDE;

    //! Returns the value of the last parameter
    Standard_EXPORT Standard_Real LastParameter() const Standard_OVERRIDE;

    //! Applies the transformation T to this curve
    Standard_EXPORT void Transform (const gp_Trsf& T) Standard_OVERRIDE;

    //! Creates a new object which is a copy of this curve
    Standard_EXPORT Handle(Geom_Geometry) Copy() const Standard_OVERRIDE;

  };

  } // end of namespace xlifepp

#endif // XLIFEPP_WITH_OPENCASCADE

#endif // OPENCASCADE_HPP
