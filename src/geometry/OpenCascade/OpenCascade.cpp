/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file OpenCascade.cpp
  \authors E. Lunéville
  \since 23 apr 2020
  \date 23 apr 2020

  \brief implementation of xlifepp::OpenCascade stuff
*/

#include "config.h"
#include "utils.h"

#ifdef XLIFEPP_WITH_OPENCASCADE
  #include "../geometries1D.hpp"
  #include "../geometries2D.hpp"
  #include "../geometries3D.hpp"
  #include "../Mesh.hpp"
  #include "OpenCascade.hpp"


  namespace xlifepp
  {
  // Geometry member functions related to OpenCascade
  void Geometry::clearOC()
  {
    if (ocData_!=nullptr) delete ocData_;
    ocData_=nullptr;
  }

  // create ocData_ from g.ocData_
  void Geometry::cloneOC(const Geometry& g)
  {
    if (ocData_!=nullptr)  clearOC();
    if (g.ocData_!=nullptr)ocData_= new OCData(*g.ocData_);
  }

  const OCData& Geometry::ocData() const
  {
    if (ocData_==nullptr)  buildOCData();
    return *ocData_;
  }

  OCData& Geometry::ocData()
  {
    if (ocData_==nullptr)  buildOCData();
    return *ocData_;
  }

  const TopoDS_Shape& Geometry::ocShape() const
  {
    if (ocData_==nullptr) buildOCData();
    return ocData_->topoDS();
  }

  void Geometry::buildOCData() const
  {
    ocData_=new OCData(*this);
  }

  bool Geometry::hasDoc() const
  {
    if (ocData_!=nullptr && ocData_->hasDoc()) return true;
    return false;
  }

  //! apply a transformation to TopoDs_Shape (TDocStd_Document not changed)
  void Geometry::ocTransformP(const Transformation& t)
  {
    if (ocData_!=nullptr) ocData_->transform(t);
  }


  void Geometry::printOCInfo(CoutStream& out) const
  {
    printOCInfo(std::cout);
    printOCInfo(out.printStream->currentStream());
  }

  void Geometry::printOCInfo(std::ostream& out) const
  {
    if (ocData_==nullptr) buildOCData();
    Handle(TNaming_NamedShape) NS;
    Handle(TDataStd_NamedData) ND;
    number_t i=1;
    for (TDF_ChildIterator itl(ocData().OCDoc().Main()); itl.More(); itl.Next(),i++)
    {
      TDF_Label l=itl.Value();
      if (l.FindAttribute(TNaming_NamedShape::GetID(),NS))
      {
        const TopoDS_Shape sh=NS->Get();
        if (l.FindAttribute(TDataStd_NamedData::GetID(),ND))
        {
          out<<"OC shape "<<i<<" id="<<ND->GetInteger("num")<<" name=\""<< ND->GetString("name")<<"\" ";
          if (sh.ShapeType()==TopAbs_EDGE && ND->GetInteger("nnode")!=0) out<< " nnode="<<ND->GetInteger("nnode")<<" ";
          if (sh.ShapeType()==TopAbs_VERTEX && ND->GetReal("hstep")!=0.) out<< " hstep="<<ND->GetReal("hstep")<<" ";
        }
        else out<<" OC item not identified: ";
        printInfo(sh,out,6);
        out<<eol;
      }
    }
  }

  //! set name to some OC shapes given by a type and some num/id (nums[0]=0 means all shapes of type oct)
  void Geometry::setOCName(OCShapeType oct, const std::vector<number_t>& nums, const string_t& name)
  {
    if (ocData_==nullptr) buildOCData();
    std::set<number_t> setnums(nums.begin(),nums.end());
    bool all=nums[0]==0;
    Handle(TNaming_NamedShape) NS;
    Handle(TDataStd_NamedData) ND;
    TopAbs_ShapeEnum soct=toTopAbsShapeEnum(oct);
    for (TDF_ChildIterator itl(ocData().OCDoc().Main()); itl.More() && (all || setnums.size()>0); itl.Next())
    {
      TDF_Label l=itl.Value();
      if (l.FindAttribute(TNaming_NamedShape::GetID(),NS))
      {
        const TopoDS_Shape sh=NS->Get();  //current OC shape
        if (sh.ShapeType()==soct)
        {
          if (l.FindAttribute(TDataStd_NamedData::GetID(),ND))
          {
            number_t n=number_t(ND->GetInteger("num"));
            if (all || setnums.find(n)!=setnums.end())
            {
              ND->SetString("name",name.c_str());
              if (!all) setnums.erase(n);
            }
          }
        }
      }
    }
    if (!all && setnums.size()>0) warning("free_warning","some OC "+xlifepp::asString(soct)+" have not be not found by Geometry::setOCName");
  }

  //! set hstep (>0) to some OC vertices given by some num/id (nums[0]=0 means all vertices)
  void Geometry::setOCHstep(const std::vector<number_t>& nums, real_t hstep)
  {
    if (hstep<=0.) error("free_error","hstep cannot be negative in Geometry::setOCInfo");
    if (ocData_==nullptr) buildOCData();
    std::set<number_t> setnums(nums.begin(),nums.end());
    bool all=nums[0]==0;
    Handle(TNaming_NamedShape) NS;
    Handle(TDataStd_NamedData) ND;
    for (TDF_ChildIterator itl(ocData().OCDoc().Main()); itl.More() && (all || setnums.size()>0); itl.Next())
    {
      TDF_Label l=itl.Value();
      if (l.FindAttribute(TNaming_NamedShape::GetID(),NS))
      {
        const TopoDS_Shape sh=NS->Get();  //current OC shape
        if (sh.ShapeType()==TopAbs_VERTEX)
        {
          if (l.FindAttribute(TDataStd_NamedData::GetID(),ND))
          {
            number_t n=number_t(ND->GetInteger("num"));
            if (all || setnums.find(n)!=setnums.end())
            {
              ND->SetReal("hstep",hstep);
              if (!all) setnums.erase(n);
            }
          }
        }
      }
    }
    if (!all && setnums.size()>0) warning("free_warning","some OC VERTEX have not be not found by Geometry::setOCHsteps");
  }

  //! set nnode (>1) to some OC edges given by some num/id (nums[0]=0 means all vertices)
  void Geometry::setOCNnode(const std::vector<number_t>& nums, number_t nnode)
  {
    if (nnode<2) error("free_error","nnode has to be greater than 1 in Geometry::setOCInfo");
    if (ocData_==nullptr) buildOCData();
    std::set<number_t> setnums(nums.begin(),nums.end());
    bool all=nums[0]==0;
    Handle(TNaming_NamedShape) NS;
    Handle(TDataStd_NamedData) ND;
    for (TDF_ChildIterator itl(ocData().OCDoc().Main()); itl.More() && (all || setnums.size()>0); itl.Next())
    {
      TDF_Label l=itl.Value();
      if (l.FindAttribute(TNaming_NamedShape::GetID(),NS))
      {
        const TopoDS_Shape sh=NS->Get();  //current OC shape
        if (sh.ShapeType()==TopAbs_EDGE)
        {
          if (l.FindAttribute(TDataStd_NamedData::GetID(),ND))
          {
            number_t n=number_t(ND->GetInteger("num"));
            if (all || setnums.find(n)!=setnums.end())
            {
              ND->SetInteger("nnode",nnode);
              if (!all) setnums.erase(n);
            }
          }
        }
      }
    }
    if (!all && setnums.size()>0) warning("free_warning","some OC EDGE have not be not found by Geometry::setOCHsteps");
  }

  //!< set name to an OC shape given by its type and num/id (0 means all shapes of OCShapeTYpe)
  void Geometry::setOCName(OCShapeType oct, number_t num, const string_t& name)
  {
    std::vector<number_t> nums(1,num);
    setOCName(oct,nums,name);
  }

  //! set hstep (!=0) to an OC vertex given by its num/id (0 means all vertices)
  void Geometry::setOCHstep(number_t num, real_t hstep)
  {
    std::vector<number_t> nums(1,num);
    setOCHstep(nums,hstep);
  }

  //! set nnode (>1) to an OC edge given by its num/id (0 means all edges)
  void Geometry::setOCNnode(number_t num, number_t nnode)
  {
    std::vector<number_t> nums(1,num);
    setOCNnode(nums,nnode);
  }

  /*! build a Geometry from brep file
    file: file name of brep
    domNames: list of names of entity of highest dimension (default is void)
                if void, create a main domain with Omega as name (all entity of highest dimension will be named Omega)
                if unique, create a main domain with this name (all entity of highest dimension will be named with this name)
                il several, try to named each domain of highest dimension with the side names list; if end of list is reached other entity will be named with the last name used
    sideNames: list of side names of entity of highest dimension-1 (default is void)
                if void, do not attach side names
                if unique, attach the side name to each entity of highest dimension-1
                if several, try to named each domain of highest dimension - 1 with the side names list; if end of list is reached other entitie entity will be named with the last name used
    hsteps: list of characteristic mesh size attached to vertices (default is void)
                if void, set a unique default hstep to each vertex (hstep ~ geometry size/10)
                if unique, attach this value to each vertex
                if several, try to attach hsteps value to each vertex; if end of list is reached, last hstep will be attached to the rest of vertices
      NOTE: naming entities or attached hstep with several names or hsteps is hazardous if you don't know the order of objects in brep, so it is adviced to use this feature with caution

      This function create the OCData structure and fill in TopoDS_Shape and TDocStd_Document, set the Geometry::ShapeType to _fromFile, build bounding boxes
      components, geometries, loops maps are not filled in.
  */
  void Geometry::loadFromBrep(const string_t& file, const Strings& domNames,const Strings& sideNames, const Reals& hsteps)
  {
    //read brep file
    std::ifstream f(file);
    if(!f.good()) error("free_error","in LoadFromBrep, file "+file+" not found");
    f.close();
    TopoDS_Shape * tds=new TopoDS_Shape();
    BRep_Builder bb;
    BRepTools::Read(*tds,file.c_str(),bb);
    //create TDFDocument
    string_t name="Omega", sidename="";
    real_t h=0.1;
    number_t dimnames= domNames.size(), dimsides=sideNames.size(), dimhs=hsteps.size();
    if (dimnames==1) name=domNames[0];
    if (dimsides==1) sidename=sideNames[0];
    if (dimhs==1) h=hsteps[0];
    number_t numc=0, numface=0,numedge=0,numvertex=0;
    dimen_t dim=0;
    real_t xmin=theRealMax,xmax=-theRealMax,ymin=theRealMax,ymax=-theRealMax,zmin=theRealMax,zmax=-theRealMax;
    TDocStd_Document* ocd = new TDocStd_Document("MDTV-Standard");
    TDF_Label root = ocd->Main();
    ShapeExtend_Explorer shexp;
    Handle(TopTools_HSequenceOfShape) tls=shexp.SeqFromCompound(*tds, true);
    dimen_t highestDim=0;

    for (number_t i=0;i<tls->Length();i++)
    {
      switch (tls->Value(i+1).ShapeType())
      {
        case TopAbs_SOLID: highestDim=3; break;
        case TopAbs_SHELL: highestDim=std::max(highestDim,dimen_t(2)); break;
        case TopAbs_FACE: highestDim=std::max(highestDim,dimen_t(2)); break;
        case TopAbs_WIRE: highestDim=std::max(highestDim,dimen_t(1)); break;
        case TopAbs_EDGE: highestDim=std::max(highestDim,dimen_t(1)); break;
        default: break;
      }
    }

    std::set<int> hashc; int hc;

    for (number_t i=0;i<tls->Length();i++) //main loop on components
    {
      if (dimnames>1)
      {if (i<dimnames) name=domNames[i]; else name=domNames[dimnames-1];}
      const TopoDS_Shape& sh=tls->Value(i+1);
      TopAbs_ShapeEnum sht=sh.ShapeType();
      if (sht==TopAbs_SOLID)
      {
        dim=std::max(dim,dimen_t(3));
        TDF_Label sL = root.NewChild();
        TNaming_Builder(sL).Generated(sh);
        numc++;
        addNamedDataAttribute(sL, numc, name, 0, 0, 0.);
        TopExp_Explorer exp(sh,TopAbs_FACE);
        for (;exp.More();exp.Next())
        {
          hc=exp.Current().HashCode(INT_MAX);
          if (hashc.find(hc)==hashc.end())
          {
            hashc.insert(hc);
            TDF_Label fL = root.NewChild();
            TNaming_Builder(fL).Generated(exp.Current());
            if (dimsides>1)
              {if (numface<dimsides) sidename=sideNames[numface]; else name=sideNames[dimsides-1];}
            numface++;
            addNamedDataAttribute(fL, numface, sidename, 0, 0, 0.);
          }
        }
        exp.Init(sh,TopAbs_EDGE);
        for (;exp.More();exp.Next())
        {
          hc=exp.Current().HashCode(INT_MAX);
          if (hashc.find(hc)==hashc.end())
          {
            hashc.insert(hc);
            TDF_Label eL = root.NewChild();
            TNaming_Builder(eL).Generated(exp.Current());
            numedge++;
            addNamedDataAttribute(eL, numedge, "", 0, 0, 0.);
          }
        }
        exp.Init(sh,TopAbs_VERTEX);
        for (;exp.More();exp.Next())
        {
          hc=exp.Current().HashCode(INT_MAX);
          if (hashc.find(hc)==hashc.end())
          {
            hashc.insert(hc);
            TDF_Label vL = root.NewChild();
            TNaming_Builder(vL).Generated(exp.Current());
            if (dimhs>1)
              {if (numvertex<dimhs) h=hsteps[numvertex]; else h=hsteps[dimhs-1];}
            numvertex++;
            addNamedDataAttribute(vL, numvertex,"", 0, 0, h);
            //update xmin, xmax, ...
            Point v=toPoint(BRep_Tool::Pnt(TopoDS::Vertex(exp.Current())));
            xmin=std::min(xmin,v[0]);xmax=std::max(xmax,v[0]);
            ymin=std::min(ymin,v[1]);ymax=std::max(ymax,v[1]);
            zmin=std::min(zmin,v[2]);zmax=std::max(zmax,v[2]);
          }
        }
      }

        if (highestDim==2 && sht==TopAbs_SHELL)  //SHELL has no boundary so no sidenames
        {
          dim=std::max(dim,dimen_t(2));
          TDF_Label sL = root.NewChild();
          TNaming_Builder(sL).Generated(sh);
          numc++;
          addNamedDataAttribute(sL, numc, name, 0, 0, 0.);
          TopExp_Explorer exp(sh,TopAbs_FACE);
          for (;exp.More();exp.Next())
          {
            hc=exp.Current().HashCode(INT_MAX);
            if (hashc.find(hc)==hashc.end())
            {
              hashc.insert(hc);
              TDF_Label fL = root.NewChild();
              TNaming_Builder(fL).Generated(exp.Current());
              numface++;
              addNamedDataAttribute(fL, numface, "", 0, 0, 0.);
            }
          }
          exp.Init(sh,TopAbs_EDGE);
          for (;exp.More();exp.Next())
          {
            hc=exp.Current().HashCode(INT_MAX);
            if (hashc.find(hc)==hashc.end())
            {
              hashc.insert(hc);
              TDF_Label eL = root.NewChild();
              TNaming_Builder(eL).Generated(exp.Current());
              numedge++;
              addNamedDataAttribute(eL, numedge, "", 0, 0, 0.);
            }
          }
          exp.Init(sh,TopAbs_VERTEX);
          for (;exp.More();exp.Next())
          {
            hc=exp.Current().HashCode(INT_MAX);
            if (hashc.find(hc)==hashc.end())
            {
              hashc.insert(hc);
              TDF_Label vL = root.NewChild();
              TNaming_Builder(vL).Generated(exp.Current());
              if (dimhs>1)
              {if (numvertex<dimhs) h=hsteps[numvertex]; else h=hsteps[dimhs-1];}
              numvertex++;
              addNamedDataAttribute(vL, numvertex,"", 0, 0, h);
              //update xmin, xmax, ...
              Point v=toPoint(BRep_Tool::Pnt(TopoDS::Vertex(exp.Current())));
              xmin=std::min(xmin,v[0]);xmax=std::max(xmax,v[0]);
              ymin=std::min(ymin,v[1]);ymax=std::max(ymax,v[1]);
              zmin=std::min(zmin,v[2]);zmax=std::max(zmax,v[2]);
            }
          }
        }

        if (highestDim==2 && sht==TopAbs_FACE)  //FACE have EDGE boundary may be named
        {
          dim=std::max(dim,dimen_t(2));
          TDF_Label fL = root.NewChild();
          TNaming_Builder(fL).Generated(sh);
          numc++;
          addNamedDataAttribute(fL, numc, name, 0, 0, 0.);
          TopExp_Explorer exp(sh,TopAbs_EDGE);
          for (;exp.More();exp.Next())
          {
            hc=exp.Current().HashCode(INT_MAX);
            if (hashc.find(hc)==hashc.end())
            {
              hashc.insert(hc);
              TDF_Label eL = root.NewChild();
              TNaming_Builder(eL).Generated(exp.Current());
              if (dimsides>1)
                {if (numedge<dimsides) sidename=sideNames[numedge]; else name=sideNames[dimsides-1];}
              numedge++;
              addNamedDataAttribute(eL, numedge, sidename, 0, 0, 0.);
            }
          }
          exp.Init(sh,TopAbs_VERTEX);
          for (;exp.More();exp.Next())
          {
            hc=exp.Current().HashCode(INT_MAX);
            if (hashc.find(hc)==hashc.end())
            {
              hashc.insert(hc);
              TDF_Label vL = root.NewChild();
              TNaming_Builder(vL).Generated(exp.Current());
              if (dimhs>1)
                {if (numvertex<dimhs) h=hsteps[numvertex]; else h=hsteps[dimhs-1];}
              numvertex++;
              addNamedDataAttribute(vL, numvertex,"", 0, 0, h);
              //update xmin, xmax, ...
              Point v=toPoint(BRep_Tool::Pnt(TopoDS::Vertex(exp.Current())));
              xmin=std::min(xmin,v[0]);xmax=std::max(xmax,v[0]);
              ymin=std::min(ymin,v[1]);ymax=std::max(ymax,v[1]);
              zmin=std::min(zmin,v[2]);zmax=std::max(zmax,v[2]);
            }
          }
        }
        if (highestDim==1 && sht==TopAbs_WIRE)  //WIRE has no side, so not sidenames
        {
          dim=std::max(dim,dimen_t(1));
          TDF_Label wL = root.NewChild();
          TNaming_Builder(wL).Generated(sh);
          numc++;
          addNamedDataAttribute(wL, numc, name, 0, 0, 0.);
          TopExp_Explorer exp(sh,TopAbs_EDGE);
          for (;exp.More();exp.Next())
          {
            hc=exp.Current().HashCode(INT_MAX);
            if (hashc.find(hc)==hashc.end())
            {
                hashc.insert(hc);
                TDF_Label eL = root.NewChild();
                TNaming_Builder(eL).Generated(exp.Current());
                numedge++;
                addNamedDataAttribute(eL, numedge, sidename, 0, 0, 0.);
            }
          }
          exp.Init(sh,TopAbs_VERTEX);
          for (;exp.More();exp.Next())
          {
            hc=exp.Current().HashCode(INT_MAX);
            if (hashc.find(hc)==hashc.end())
            {
              hashc.insert(hc);
              TDF_Label vL = root.NewChild();
              TNaming_Builder(vL).Generated(exp.Current());
              if (dimhs>1)
              {if (numvertex<dimhs) h=hsteps[numvertex]; else h=hsteps[dimhs-1];}
              numvertex++;
              addNamedDataAttribute(vL, numvertex,"", 0, 0, h);
              //update xmin, xmax, ...
              Point v=toPoint(BRep_Tool::Pnt(TopoDS::Vertex(exp.Current())));
              xmin=std::min(xmin,v[0]);xmax=std::max(xmax,v[0]);
              ymin=std::min(ymin,v[1]);ymax=std::max(ymax,v[1]);
              zmin=std::min(zmin,v[2]);zmax=std::max(zmax,v[2]);
            }
          }
        }

        if (highestDim==1 && sht==TopAbs_EDGE)  //EDGE has boundary vertex may be named
        {
          dim=std::max(dim,dimen_t(1));
          TDF_Label eL = root.NewChild();
          TNaming_Builder(eL).Generated(sh);
          numc++;
          addNamedDataAttribute(eL, numc, name, 0, 0, 0.);
          TopExp_Explorer exp(sh,TopAbs_VERTEX);
          for (;exp.More();exp.Next())
          {
            if (hashc.find(hc)==hashc.end())
            {
              hashc.insert(hc);
              TDF_Label vL = root.NewChild();
              TNaming_Builder(vL).Generated(exp.Current());
              if (dimhs>1)
                {if (numvertex<dimhs) h=hsteps[numvertex]; else h=hsteps[dimhs-1];}
              if (dimsides>1)
                {if (numvertex<dimsides) sidename=sideNames[numvertex]; else name=sideNames[dimsides-1];}
              numvertex++;
              addNamedDataAttribute(vL, numvertex,sidename, 0, 0, h);
              //update xmin, xmax, ...
              Point v=toPoint(BRep_Tool::Pnt(TopoDS::Vertex(exp.Current())));
              xmin=std::min(xmin,v[0]);xmax=std::max(xmax,v[0]);
              ymin=std::min(ymin,v[1]);ymax=std::max(ymax,v[1]);
              zmin=std::min(zmin,v[2]);zmax=std::max(zmax,v[2]);
            }
          }
        }
    }
    if (dimhs==0) //update h
    {
      h=theRealMax;
      real_t d=xmax-xmin; if (d>0) h=std::min(h,d);
      d=ymax-ymin;if (d>0) h=std::min(h,d);
      d=zmax-zmin;if (d>0) h=std::min(h,d);
      if (h<theTolerance) h=1.;
      h/=10;
      Handle(TNaming_NamedShape) NS;
      Handle(TDataStd_NamedData) ND;
      for (TDF_ChildIterator itl(ocd->Main()); itl.More(); itl.Next())
      {
        TDF_Label l=itl.Value();
        if (l.FindAttribute(TNaming_NamedShape::GetID(),NS))
        {
          if (NS->Get().ShapeType()==TopAbs_VERTEX)
          {
              if (l.FindAttribute(TDataStd_NamedData::GetID(),ND)) ND->SetReal("hstep",h);
          }
        }
      }
    }

    //create new OCData
    if (ocData_!=nullptr) delete ocData_;
    ocData_ = new OCData(tds,ocd);

    //update bounding boxes
    dim_=dim;
    boundingBox=BoundingBox(xmin,xmax,ymin,ymax,zmin,zmax);
    minimalBox=MinimalBox(xmin,xmax,ymin,ymax,zmin,zmax);
  }

  //! recursive construction of the TopoDS_Shape related to current node
  ShapeDocPair GeoNode::ocBuild()
  {
    ShapeDocPair op1, op2;
    if (geom1_!=nullptr) op1=geom1_->ocData().asShapeDocPair(); else op1=node1_->ocBuild();
    if (geom2_!=nullptr) op2=geom2_->ocData().asShapeDocPair();
    else if (node2_!=nullptr) op2=node2_->ocBuild();
    ShapeDocPair res;
    //theCout<<string_t(level_,' ')<<"ocBuild node: "<<xlifepp::asString(geop_)<<" "<<op1<<" "<<op2<<eol<<std::flush;
    switch (geop_)
    {
      case _noneGeOp: res=op1; break;
      case _plusGeOp:
      case _minusGeOp:
      case _commonGeOp: res = ocBooleanOperation(geop_,op1,op2); break;
      case _loopGeOp:
      case _extrusionGeOp:
      default: error("free_error","geometry operation not yet available in GeoNode::ocBuild()");
    }
    //deallocate transient objects created on fly, excluding nodes that are "pure" geometry (geop_=_noneGeOp)
    if (geom1_==nullptr && node1_->geop_!=_noneGeOp)
    {
      delete op1.first;
      //delete op1.second;
    }
    if (geom2_==nullptr && node2_!=nullptr && node2_->geop_!=_noneGeOp)
    {
      delete op2.first;
      //delete op2.second;
    }
    return res;
  }

  //! common part of two geometries
  Geometry operator^(const Geometry& g1, const Geometry& g2)
  {
    trace_p->push("Geometry::operator^");
    if (&g1 == &g2) return g1;
    Geometry g;
    g.shape(_ocShape);
    g.dim(std::min(g1.dim(),g2.dim()));
    g.boundingBox=g1.boundingBox;
    g.boundingBox^=g2.boundingBox;
    g.minimalBox=MinimalBox(g.boundingBox.bounds());
    g.init();
    g.geoNode_=new GeoNode(_commonGeOp,g1.clone(),g2.clone());
    trace_p->pop();
    return g;
  }

  //! common part of the current geometry and an other one
  Geometry& Geometry::operator^=(const Geometry& g)
  {
    trace_p->push("Geometry::operator^=");
    if (this == &g) return *this;
    shape_=_ocShape;
    dim_=std::min(dim(),g.dim());
    boundingBox^=g.boundingBox;
    minimalBox=MinimalBox(boundingBox.bounds());
    if (geoNode_==nullptr) geoNode_=new GeoNode(_commonGeOp,this,g.clone());
    else geoNode_=new GeoNode(_commonGeOp,this->geoNode_,g.clone());
    clearCompositeData();
    clearExtrusionData();
    clearParametrization();
    trace_p->pop();
    return *this;
  }

  //=============================================================================
  //                         OCData member functions
  //=============================================================================

  OCData::~OCData()
  {
    if (OCShape_!=nullptr) delete OCShape_;
  }

  OCData::OCData(const OCData& ocd)
  {
    OCShape_=nullptr;
    if (ocd.OCShape_!=nullptr) OCShape_= new TopoDS_Shape(*ocd.OCShape_);
    if (ocd.OCDoc_!=nullptr) OCDoc_=ocd.OCDoc_; //shared pointer
  }

  OCData& OCData::operator=(const OCData& ocd)
  {
    if (&ocd==this)  return *this;
    if (OCShape_!=nullptr)  delete OCShape_;
    if (OCDoc_!=nullptr) delete OCDoc_;
    if (ocd.OCShape_!=nullptr) OCShape_=new TopoDS_Shape(*ocd.OCShape_);
    if (ocd.OCDoc_!=nullptr) OCDoc_=ocd.OCDoc_;
    return *this;
  }

  ShapeDocPair OCData::asShapeDocPair() const
  {
    return ShapeDocPair(OCShape_,OCDoc_);
  }

  void OCData::buildOCShape(const Geometry& geom)
  {
    //trace_p->push("OCData::buildOCShape"); // may be deep recursive process, saturating the trace_p stack
    switch (geom.shape())
    {
      case _segment:
      {
        OCShape_ = ocSegment(const_cast<Segment &>(*geom.segment()));  // build OC shape
        buildOCDocG(geom);       // build OC doc
        break;
      }
      case _circArc:
      {
        OCShape_ = ocCircArc(const_cast<CircArc &>(*geom.circArc()));  // build OC shape
        buildOCDocG(geom);       // build OC doc
        break;
      }
      case _ellArc:
      {
        OCShape_ = ocEllArc(const_cast<EllArc &>(*geom.ellArc()));  // build OC shape
        buildOCDocG(geom);       // build OC doc
        break;
      }
      case _splineArc:
      {
          OCShape_ = ocSplineArc(const_cast<SplineArc &>(*geom.splineArc()));  // build OC shape
          buildOCDocG(geom);       // build OC doc
          break;
      }
      case _parametrizedArc:
      {
        if (geom.parametrizedArc()->partitioning()==_splinePartition)
        {
          OCShape_ = ocParametrizedArc(const_cast<ParametrizedArc &>(*geom.parametrizedArc()));  // build OC shape
          buildOCDocG(geom);       // build OC doc
        }
        else
        {
          ShapeDocPair data =  ocParametrizedArcLin(const_cast<ParametrizedArc &>(*geom.parametrizedArc()));  // build OC shape and OC doc
          OCShape_=data.first;
          OCDoc_=data.second;
        }
        break;
      }
      case _polygon:
      case _triangle:
      case _quadrangle:
      case _parallelogram:
      case _rectangle:
      case _square:
      {
        OCShape_ = ocPolygon(const_cast<Polygon &>(*geom.polygon()));  // build OC shape
        buildOCDocG(geom);       // build OC doc
        break;
      }
      case _ellipse:
      {
        OCShape_ = ocEllipse(const_cast<Ellipse &>(*geom.ellipse()));  // build OC shape
        buildOCDocG(geom);       // build OC doc
        break;
      }
      case _disk:
      {
        OCShape_ = ocDisk(const_cast<Disk &>(*geom.disk()));  // build OC shape
        buildOCDocG(geom);       // build OC doc
        break;
      }
      case _parametrizedSurface:
      {
        ShapeDocPair data;
        if (geom.parametrizedSurface()->partitioning()==_splinePartition)
            data =  ocParametrizedSurface(const_cast<ParametrizedSurface &>(*geom.parametrizedSurface()));  // build OC shape and OC doc
        else
            data =  ocParametrizedSurfaceLin(const_cast<ParametrizedSurface &>(*geom.parametrizedSurface()));  // build OC shape and OC doc
        OCShape_=data.first;
        OCDoc_=data.second;
        break;
      }
      case _splineSurface:
      {
        ShapeDocPair data;
        data = ocSplineSurface(const_cast<SplineSurface &>(*geom.splineSurface()));  // build OC shape and OC doc
        OCShape_=data.first;
        OCDoc_=data.second;
        break;
      }

      case _polyhedron:
      case _tetrahedron:
      case _hexahedron:
      case _parallelepiped:
      case _cuboid:
      case _cube:
      {
        OCShape_ = ocPolyhedron(const_cast<Polyhedron &>(*geom.polyhedron()));  // build OC shape
        buildOCDocG(geom);       // build OC doc
        break;
      }
      case _ball:
      case _ellipsoid:
      {
        OCShape_ = ocEllipsoid(const_cast<Ellipsoid &>(*geom.ellipsoid()));  // build OC shape
        buildOCDocG(geom);       // build OC doc
        break;
      }
      case _trunk:
      case _cylinder:
      case _prism:
      case _cone:
      case _pyramid:
      {
        OCShape_ = ocTrunk(const_cast<Trunk &>(*geom.trunk()));  // build OC shape
        buildOCDocG(geom);       // build OC doc
        break;
      }
      case _revTrunk:
      case _revCylinder:
      case _revCone:
      {
        OCShape_ = ocRevTrunk(const_cast<RevTrunk &>(*geom.revTrunk()));  // build OC shape
        buildOCDocG(geom);       // build OC doc
        break;
      }
      case _composite:
      {
        ShapeDocPair data = ocComposite(const_cast<Geometry &>(geom));  // build OC shape and OC doc
        OCShape_=data.first;
        OCDoc_=data.second;
        break;
      }
      case _loop:
      {
        ShapeDocPair data =  ocLoop(const_cast<Geometry &>(geom));  // build OC shape and OC doc
        OCShape_=data.first;
        OCDoc_=data.second;
        break;
      }
      case _extrusion:
      {
        ShapeDocPair data =  ocExtrusion(const_cast<Geometry &>(geom));  // build OC shape and OC doc
        OCShape_=data.first;
        OCDoc_=data.second;
        break;
      }
      case _ocShape:
      {
        ShapeDocPair data =  ocShape(const_cast<Geometry &>(geom));  // build OC shape and OC doc
        OCShape_=data.first;
        OCDoc_=data.second;
        break;
      }
      default:
        error("not_yet_implemented"," convert "+words("shape",geom.shape())+" to OpenCascade object not yet available");
    }
    //trace_p->pop();
  }

  // build doc for any xlifepp canonical geometry
  // OC shape must be already built !
  void OCData::buildOCDocG(const Geometry& geo)
  {
    number_t dim = geo.dim();

    // create a TDF document
    OCDoc_ = new TDocStd_Document("MDTV-Standard");
    TDF_Label root = OCDoc_->Main();
    TDF_Label shapeL = root.NewChild();
    TopExp_Explorer exp;
    //register  main shape
    if (geo.isCanonical() && OCShape_->ShapeType()==TopAbs_COMPOUND) // only the first compound
    {
      if (dim==3)
      {
        exp.Init(*OCShape_,TopAbs_SOLID);
        if (exp.More())
          TNaming_Builder(shapeL).Generated(TopoDS::Solid(exp.Current()));
        else
        {
          exp.Init(*OCShape_,TopAbs_SHELL);
          if (exp.More())
            TNaming_Builder(shapeL).Generated(TopoDS::Shell(exp.Current()));
        }
      }
      if (dim==2)
      {
        exp.Init(*OCShape_,TopAbs_FACE);
        TNaming_Builder(shapeL).Generated(TopoDS::Face(exp.Current()));
      }
      if (dim==1)
      {
        exp.Init(*OCShape_,TopAbs_EDGE);
        TNaming_Builder(shapeL).Generated(TopoDS::Edge(exp.Current()));
      }
    }
    else
      TNaming_Builder(shapeL).Generated(*OCShape_);
    addNamedDataAttribute(shapeL,1,geo.domName());

    std::vector<std::pair<ShapeType,std::vector<const Point*> > >  faces = geo.surfs();
    std::vector<std::pair<ShapeType,std::vector<const Point*> > >  edges = geo.curves();
    std::vector<const Point*> vertices = geo.boundNodes();
    const std::vector<string_t>& sidenames = geo.sideNames();
    number_t nbsn = sidenames.size();
    string_t sn="";
    number_t no=1;
    std::set<number_t> nums;

    // register faces and sidenames
    if (dim>=3)
    {
      for (exp.Init(*OCShape_, TopAbs_FACE); exp.More(); exp.Next(), no++)
      {
        TopoDS_Face f=TopoDS::Face(exp.Current());
        TDF_Label faceL = root.NewChild();
        TNaming_Builder(faceL).Generated(f);
        number_t n=0;
        std::list<Point> OCvs = OCvertices(f,theTolerance);
        number_t i=0;
        for (; i<faces.size(); i++)
          if (commonPoints(OCvs,faces[i].second,theTolerance)>2) break; // 3 common points imply same faces
        if (i==faces.size()) theCout<<"   OC face "<<no<<" not identified to a geo face, may be not named"<<eol;
        sn="";
        if (nbsn>0) sn=sidenames[std::min(i,nbsn-1)]; //register sidenames
        addNamedDataAttribute(faceL,no,sn,i+1,0,0.);
      }
    }
    //register edges and nnodes and sidenames when dim=2
    if (dim>=2)
    {
      no=1; sn="";
      for (exp.Init(*OCShape_, TopAbs_EDGE); exp.More(); exp.Next())
      {
        TopoDS_Edge e=TopoDS::Edge(exp.Current());
        std::list<Point> OCvs = OCvertices(e,theTolerance);
        number_t i=0, nnode=0;
        for (; i<edges.size(); i++)
          if (commonPoints(OCvs,edges[i].second,theTolerance)>1) break; // 2 common points imply same edges
        if (i==edges.size())
        {
          //theCout<<" -> not identified to a geo edge, may be a generated edge (not named)"<<eol;  // do not reference
          no++;
        }
        else if (nums.find(i)==nums.end())
        {
          nums.insert(i);
          TDF_Label edgeL = root.NewChild();
          TNaming_Builder(edgeL).Generated(e);
          if (geo.h().size()==0 && geo.n().size()!=0) nnode=geo.n()[i];
          if (dim==2 && nbsn>0) sn=sidenames[std::min(i,nbsn-1)]; //register sidenames
          addNamedDataAttribute(edgeL,no,sn,i+1,nnode,0.);
          no++;
        }
      }
    }
    //register vertex and hsteps and sidenames when dim=1
    nums.clear();
    no=1;
    sn="";
    for (exp.Init(*OCShape_, TopAbs_VERTEX); exp.More(); exp.Next())
    {
      TopoDS_Vertex v=TopoDS::Vertex(exp.Current());
      Point V = toPoint(BRep_Tool::Pnt(TopoDS::Vertex(exp.Current())));
      number_t i=0;
      for (; i<vertices.size(); i++)
        if (norm(V-force3D(*vertices[i]))<theTolerance) break;
      if (i==vertices.size())
        theCout<<"OC vertex "<<V<<" not identified to a geo vertex, may be not named"<<eol;
      else if (nums.find(i)==nums.end())
      {
        nums.insert(i);
        TDF_Label vertexL = root.NewChild();
        TNaming_Builder(vertexL).Generated(v);
        real_t hstep=0;
        number_t nh=geo.h().size();
        if (nh>0) hstep=geo.h()[std::min(i,nh-1)];
        if (dim==1 && nbsn>0) sn=sidenames[std::min(i,nbsn-1)]; //register sidenames
        addNamedDataAttribute(vertexL,no,sn,i,0,hstep);
        no++;
      }
    }
  }

  // create tag->shape and shape->tag maps
  void OCData::buildOCTags(const TopoDS_Vertex& vertex)
  {
    if (vertex.IsNull()) return;
    if (vertex2Tag.IsBound(vertex)) return;  //already tagged
    lastVertexTag++;
    vertex2Tag.Bind(vertex, lastVertexTag);
    tag2Vertex.Bind(lastVertexTag,vertex);
  }

  void OCData::buildOCTags(const TopoDS_Edge& edge, bool recursive)
  {
    if (edge.IsNull())
      return;
    if (!edge2Tag.IsBound(edge))
    {
      lastEdgeTag++;
      edge2Tag.Bind(edge,lastEdgeTag);
      tag2Edge.Bind(lastEdgeTag,edge);
    }
    if (recursive)
    {
      TopExp_Explorer exp;
      for (exp.Init(edge, TopAbs_VERTEX); exp.More(); exp.Next())
        buildOCTags(TopoDS::Vertex(exp.Current()));
    }
  }

  void OCData::buildOCTags(const TopoDS_Wire& wire, bool recursive)
  {
    if (wire.IsNull())
      return;
    if (!wire2Tag.IsBound(wire))
    {
      lastWireTag++;
      wire2Tag.Bind(wire,lastWireTag);
      tag2Wire.Bind(lastWireTag,wire);
    }
    if (recursive)
    {
      TopExp_Explorer exp;
      for (exp.Init(wire, TopAbs_EDGE); exp.More(); exp.Next())
        buildOCTags(TopoDS::Edge(exp.Current()),recursive);
    }
  }

  void OCData::buildOCTags(const TopoDS_Face& face, bool recursive)
  {
    if (face.IsNull())
      return;
    if (!face2Tag.IsBound(face))
    {
      lastFaceTag++;
      face2Tag.Bind(face,lastFaceTag);
      tag2Face.Bind(lastFaceTag,face);
    }
    if (recursive)
    {
      TopExp_Explorer exp;
      for (exp.Init(face, TopAbs_WIRE); exp.More(); exp.Next())
        buildOCTags(TopoDS::Wire(exp.Current()),recursive);
      for (exp.Init(face, TopAbs_EDGE); exp.More(); exp.Next())
        buildOCTags(TopoDS::Edge(exp.Current()),recursive);
    }
  }

  void OCData::buildOCTags(const TopoDS_Shell& shell, bool recursive)
  {
    if (shell.IsNull())
      return;
    if (!shell2Tag.IsBound(shell))
    {
      lastShellTag++;
      shell2Tag.Bind(shell,lastShellTag);
      tag2Shell.Bind(lastShellTag,shell);
    }
    if (recursive)
    {
      TopExp_Explorer exp;
      for (exp.Init(shell, TopAbs_FACE); exp.More(); exp.Next())
        buildOCTags(TopoDS::Face(exp.Current()),recursive);
    }
  }

  void OCData::buildOCTags(const TopoDS_Solid& solid, bool recursive)
  {
    if (solid.IsNull())
      return;
    if (!solid2Tag.IsBound(solid))
    {
      lastSolidTag++;
      solid2Tag.Bind(solid,lastSolidTag);
      tag2Solid.Bind(lastSolidTag,solid);
    }
    if (recursive)
    {
      TopExp_Explorer exp;
      for (exp.Init(solid, TopAbs_FACE); exp.More(); exp.Next())
        buildOCTags(TopoDS::Face(exp.Current()),recursive);
      for (exp.Init(solid, TopAbs_SHELL); exp.More(); exp.Next())
        buildOCTags(TopoDS::Shell(exp.Current()),recursive);
    }
  }

  void OCData::buildOCTags(bool recursive)
  {
    lastVertexTag=0;
    lastEdgeTag=0;
    lastWireTag=0;
    lastShellTag=0;
    lastFaceTag=0;
    lastSolidTag=0;
    if (OCShape_==nullptr)
      return;
    TopExp_Explorer exp;
    for (exp.Init(*OCShape_, TopAbs_SOLID); exp.More(); exp.Next())
      buildOCTags(TopoDS::Solid(exp.Current()),recursive);
    for (exp.Init(*OCShape_, TopAbs_FACE); exp.More(); exp.Next())
      buildOCTags(TopoDS::Face(exp.Current()),recursive);
    for (exp.Init(*OCShape_, TopAbs_EDGE); exp.More(); exp.Next())
      buildOCTags(TopoDS::Edge(exp.Current()),recursive);
    for (exp.Init(*OCShape_, TopAbs_VERTEX); exp.More(); exp.Next())
      buildOCTags(TopoDS::Vertex(exp.Current()));
  }

  void OCData::transform(const Transformation& t)
  {
      const Matrix<real_t>& A=t.mat();
      const Vector<real_t>& b=t.vec();  //transformation AP+b
      //move to OC transformation MP+V
      gp_Mat M(to_gpXYZ(A.column(1)),to_gpXYZ(A.column(2)),to_gpXYZ(A.column(3))); // matrix
      gp_XYZ V = to_gpXYZ(b);
      gp_GTrsf tr(M,V);   // transformation: Q=M*P+V
      BRepBuilderAPI_GTransform bgt(*OCShape_,tr);
      *OCShape_ = bgt.Shape();

      //as sub-shapes are mofified, update TNaming_NamedShape's of OCDoc_
      TDocStd_Document* ocdoc = new TDocStd_Document("MDTV-Standard");
      TDF_Label root = ocdoc->Main();
      Handle(TNaming_NamedShape) NS;
      Handle(TDataStd_NamedData) ND;
      for (TDF_ChildIterator itl(OCDoc_->Main()); itl.More(); itl.Next())  //travel old OCDoc
      {
        TDF_Label l=itl.Value();
        if (l.FindAttribute(TNaming_NamedShape::GetID(),NS))
        {
          TopoDS_Shape sh=NS->Get();
          TopoDS_Shape nsh=bgt.ModifiedShape(sh);
          TDF_Label lab = root.NewChild();
          TNaming_Builder(lab).Generated(nsh);
          if (l.FindAttribute(TDataStd_NamedData::GetID(),ND))
          {
            addNamedDataAttribute(lab, ND->GetInteger("num"),tostring(ND->GetString("name")),
                                  ND->GetInteger("tag"),ND->GetInteger("nnode"),ND->GetReal("hstep"));
          }
        }
      }
      OCDoc_=ocdoc;
  }

  //! print OCData on ostream
  void OCData::print(std::ostream& out, bool recursive) const
  {
    if (OCShape_==nullptr) out<<"empty shape"<<eol;
    out<<"shape "<<eol;
    xlifepp::print(*OCShape_,out,recursive);
    printDoc(out);
  }

  //! dump OCData on CoutStream
  void OCData::print(CoutStream& out,bool recursive) const
  {
    print(std::cout,recursive);
    print(out.printStream->currentStream(),recursive);
  }

  void OCData::printDoc(std::ostream& out) const
  {
    if (OCDoc_!=nullptr)
    {
      Handle(TNaming_NamedShape) NS;
      Handle(TDataStd_NamedData) ND;
      for (TDF_ChildIterator itl(OCDoc_->Main()); itl.More(); itl.Next())
      {
        TDF_Label l=itl.Value();
        if (l.FindAttribute(TNaming_NamedShape::GetID(),NS))
          out<< asString(NS->Get().ShapeType());
        if (l.FindAttribute(TDataStd_NamedData::GetID(),ND))
          out<<" "<<ND->GetInteger("num")<<" "<< ND->GetString("name")<< " tag="<<ND->GetInteger("tag")
            << " nnode="<<ND->GetInteger("nnode") << " hstep="<<ND->GetReal("hstep")<<eol<<std::flush;
      }
    }
  }

  //! export OCShape_ to STEP file
  void OCData::saveToStep(const string_t& f) const
  {
    xlifepp::saveToStep(*OCShape_,f);
  }

  //! export OCShape_ to BREP file
  void OCData::saveToBrep(const string_t& f) const
  {
    xlifepp::saveToBrep(*OCShape_,f);
  }

  //!< update indexes of shape regarding the step file, because step writer may reorder some shapes
  void OCData::updateDoc(const string_t& fn)
  {
    TopoDS_Shape stepshape = loadFromStep(fn);
    // travel all attributes of doc
    Handle(TNaming_NamedShape) NS;
    Handle(TDataStd_NamedData) ND;
    std::map<TopAbs_ShapeEnum,number_t> shenum;
    shenum[TopAbs_SOLID]=4; shenum[TopAbs_FACE]=3; shenum[TopAbs_EDGE]=1; shenum[TopAbs_VERTEX]=0;
    TopExp_Explorer exp;
    for (TDF_ChildIterator itl(OCDoc_->Main()); itl.More(); itl.Next())
    {
      TDF_Label l=itl.Value();
      if (l.FindAttribute(TNaming_NamedShape::GetID(),NS))
      {
        TopAbs_ShapeEnum sht=NS->Get().ShapeType();
        std::list<Point> pts=OCvertices(NS->Get());
        number_t r=1;
        exp.Init(stepshape,sht);
        for (;exp.More();exp.Next(),r++)
        {
            theCout<<"pts="<<pts<<" OC vertices="<<OCvertices(exp.Current())<<eol;
            if (commonPoints(pts,OCvertices(exp.Current()))>shenum[sht]) break;
        }
        if (!exp.More()) r = 0;
        if (r>0 && l.FindAttribute(TDataStd_NamedData::GetID(),ND)) ND->SetInteger("num",r);
      }
    }
  }

  //=============================================================================
  //                               external tools
  //=============================================================================
  //print stuff
  void dump(const TopoDS_Shape& ts,std::ostream& out)
  {
    BRepTools::Dump(ts,out);
  }

  void dump(const TopoDS_Shape& ts,CoutStream& out)
  {
    BRepTools::Dump(ts,std::cout);
    BRepTools::Dump(ts,out.printStream->currentStream());
  }

  void dump(BRepTools_History& h, std::ostream& out)
  {
    h.Dump(out);
  }

  void dump(BRepTools_History& h, CoutStream& out)
  {
    h.Dump(std::cout);
    h.Dump(out.printStream->currentStream());
  }

  // convert xlife++ Point to various OpenCascade structures
  gp_Pnt to_gpPnt(const std::vector<real_t>& P)
  {
    number_t d=P.size();
    gp_Pnt gp;
    if (d>0)
      gp.SetX(P[0]);
    if (d>1)
      gp.SetY(P[1]);
    if (d>2)
      gp.SetZ(P[2]);
    return gp;
  }

  gp_XYZ to_gpXYZ(const std::vector<real_t>& P)
  {
    number_t d=P.size();
    gp_XYZ gp(0,0,0);
    if (d>0)
      gp.SetX(P[0]);
    if (d>1)
      gp.SetY(P[1]);
    if (d>2)
      gp.SetZ(P[2]);
    return gp;
  }

  gp_Dir to_gpDir(const std::vector<real_t>& P) //unitary vector
  {
    return gp_Dir(to_gpXYZ(P));
  }

  gp_Vec to_gpVec(const std::vector<real_t>& v)
  {
    return gp_Vec(to_gpXYZ(v));
  }


  Point toPoint(const gp_Pnt& P)
  {
    return Point(P.X(),P.Y(),P.Z());
  }

  // ====================================================
  // build OpenCascade object related to xlife++ geometry
  // ====================================================
  //! create TopoDS_Shape related to any Geometry
  TopoDS_Shape* ocGeometry(Geometry& geom)
  {
    switch (geom.shape())
    {
      case _segment:
        return ocSegment(*geom.segment());
      case _circArc:
        return ocCircArc(*geom.circArc());
      case _ellArc:
        return ocEllArc(*geom.ellArc());
      case _splineArc:
        return ocSplineArc(*geom.splineArc());
      case _parametrizedArc:
        {
            if (geom.parametrizedArc()->partitioning()==_splinePartition) return ocParametrizedArc(*geom.parametrizedArc());
            else return ocParametrizedArcLin(*geom.parametrizedArc()).first;
        }
      case _polygon:
      case _triangle:
      case _quadrangle:
      case _parallelogram:
      case _rectangle:
      case _square:
        return ocPolygon(*geom.polygon());
      case _ellipse:
        return ocEllipse(*geom.ellipse());
      case _disk:
        return ocDisk(*geom.disk());
      case _cuboid:
      case _cube:
        if (geom.cuboid()->nbOctants()==8)
          return ocCuboid(*geom.cuboid());  // no break to deal with partial cuboid
      case _polyhedron:
      case _tetrahedron:
      case _hexahedron:
      case _parallelepiped:
        return ocPolyhedron(*geom.polyhedron());
      case _ball:
      case _ellipsoid:
        return ocEllipsoid(*geom.ellipsoid());
      case _trunk:
      case _cylinder:
      case _prism:
      case _cone:
      case _pyramid:
        return ocTrunk(*geom.trunk());
      case _revTrunk:
      case _revCylinder:
      case _revCone:
        return ocRevTrunk(*geom.revTrunk());
      case _composite:
        return ocComposite(geom).first;
      case _loop:
        return ocLoop(geom).first;
      case _extrusion:
        return ocExtrusion(geom).first;
      case _ocShape:
        return ocShape(geom).first;
      default:
        error("not_yet_implemented"," convert "+words("shape",geom.shape())+" to OpenCascade object not yet available");
    }
    return nullptr;
  }

  //! create TopoDS_Shape related to segment (1D)
  TopoDS_Shape* ocSegment(Segment& s)
  {
    TopoDS_Edge e = BRepBuilderAPI_MakeEdge(to_gpPnt(s.p1()), to_gpPnt(s.p2()));
    return new TopoDS_Shape(e);
  }

  //! create TopoDS_Shape related to  CircArc (1D)
  TopoDS_Shape* ocCircArc(CircArc& ca)
  {
    const Point& P1=ca.p1();
    const Point& P2=ca.p2();
    const Point& C=ca.center();
    real_t r=ca.radius();
    Point IC=(P1+P2)/2-C;
    Point P12=C+r*IC/norm(IC);
    Handle(Geom_TrimmedCurve) ac = GC_MakeArcOfCircle(to_gpPnt(P1), to_gpPnt(P12), to_gpPnt(P2));
    TopoDS_Edge e = BRepBuilderAPI_MakeEdge(ac);
    return new TopoDS_Shape(e);
  }

  //! create TopoDS_Shape related to EllArc (1D)
  TopoDS_Shape* ocEllArc(EllArc& ela)
  {
    const Point& P1=ela.p1();
    const Point& P2=ela.p2();
    const Point& C=ela.center();
    const Point& A=ela.apogee();
    const Point& B=ela.apogee2();
    Point CA=A-C, CB=B-C;
    Point N=crossProduct(CA,CB);
    N/=norm(N);
    real_t nca=norm(CA);
    CA/=nca;
    gp_Ax2 ax(to_gpPnt(C),to_gpDir(N),to_gpDir(CA));
    gp_Elips el(ax, nca, norm(CB));
    Handle(Geom_TrimmedCurve) ac = GC_MakeArcOfEllipse(el, to_gpPnt(P1), to_gpPnt(P2),true);
    TopoDS_Edge e = BRepBuilderAPI_MakeEdge(ac);
    return new TopoDS_Shape(e);
  }

  //! create TopoDS_Shape related to SplineArc (1D) (only BSpline arc)
  TopoDS_Shape* ocSplineArc(SplineArc& spa)
  {
    if (spa.type()!= _BSpline) error("free_error","Open Cascade supports only BSpline arc");
    const BSpline& bsp=reinterpret_cast<const BSpline&>(spa.spline());   //downcast Spline to BSpline

    if (spa.subtype()==_SplineInterpolation) //create OC interpolate BSpline
    {
      const std::vector<Point>& pts=bsp.interpolationPoints();
      std::pair<SplineBC,SplineBC> bcs=bsp.boundaryConditions();
      number_t np=pts.size();
      Handle(TColgp_HArray1OfPnt) ocpts = new TColgp_HArray1OfPnt(1,np);
      for (number_t i=0;i<np;++i) ocpts->SetValue(i+1,to_gpPnt(pts[i]));
      real_t tol=1.E-7;
      GeomAPI_Interpolate gi(ocpts, bsp.isPeriodic() || bsp.isClosed(),tol);
      gp_Vec t0=to_gpVec(bsp.yps()), t1=to_gpVec(bsp.ype());
      theCout<<"yps=("<<t0.X()<<","<<t0.Y()<<","<<t0.Z()<<") ype=("<<t1.X()<<","<<t1.Y()<<","<<t1.Z()<<")"<<eol<<std::flush;
      if (bcs.first==_clampedBC && bcs.second==_clampedBC) gi.Load(t0,t1);
      else if (bcs.first==_clampedBC || bcs.second==_clampedBC)
      {
        TColgp_Array1OfVec tgs(1,np);
        Handle(TColStd_HArray1OfBoolean) flags = new TColStd_HArray1OfBoolean(1,np);
        for (number_t i=0;i<np;++i) {tgs.SetValue(i+1,gp_Vec()); flags->SetValue(i+1,false);}
        if (bcs.first==_clampedBC)  {tgs.SetValue(1,t0); flags->SetValue(1,true);}
        else {tgs.SetValue(np,t1); flags->SetValue(np,true);}
        gi.Load(tgs,flags);
      }
      gi.Perform();
      TopoDS_Edge e = BRepBuilderAPI_MakeEdge(gi.Curve()).Edge();
      return new TopoDS_Shape(e);
    }

    //create OC approximate BSpline
    const std::vector<Point>& pts=bsp.controlPoints();
    const std::multimap<real_t,number_t>& knots=bsp.knots();
    const std::vector<real_t>& weights=bsp.weights();
    std::multimap<real_t,number_t>::const_iterator itm=knots.begin();
    real_t val=itm->first;
    number_t nk=1 , np=pts.size();  itm++;
    for (;itm!=knots.end();++itm) if (itm->first!=val) {nk++;val=itm->first;}
    TColgp_Array1OfPnt ocpts(1,np);
    TColStd_Array1OfReal  ocweights(1,np);
    for (number_t i=0;i<np;++i)
    {
      ocpts.SetValue(i+1,to_gpPnt(pts[i]));
      ocweights.SetValue(i+1,weights[i]);
    }
    TColStd_Array1OfReal ocknots(1,nk);
    TColStd_Array1OfInteger ocmults(1,nk);
    itm=knots.begin();
    val=itm->first;
    number_t i=1, mult=1;
    ocknots.SetValue(i,val);
    ocmults.SetValue(i,mult);
    itm++;
    for (;itm!=knots.end();++itm)
    {
      if (itm->first!=val)
      {
        val=itm->first;
        mult=1;
        i++;
        ocknots.SetValue(i,val);
        ocmults.SetValue(i,mult);
      }
      else
      {
        mult++;
        ocmults.SetValue(i,mult);
      }
    }
    Handle(Geom_BSplineCurve) bsc= new Geom_BSplineCurve(ocpts, ocweights, ocknots, ocmults,bsp.degree(),bsp.isPeriodic());
    TopoDS_Edge e = BRepBuilderAPI_MakeEdge(bsc).Edge();
    return new TopoDS_Shape(e);
  }

  //! create TopoDS_Shape related to ParametrizedArc (1D)
  //! caution: split curve in nbparts and use BSpline so the OC arc (edge) only approximates the parametrized arc
  TopoDS_Shape* ocParametrizedArc(ParametrizedArc& para)
  {
    const Parametrization& parm=para.parametrization();
    number_t np=para.nbParts()+1;
    Handle(TColgp_HArray1OfPnt) ocpts = new TColgp_HArray1OfPnt(1,np);
    TColgp_Array1OfVec tgs(1,np);
    Handle(TColStd_HArray1OfBoolean) flags = new TColStd_HArray1OfBoolean(1,np);
    real_t tmin=para.tmin(), tmax=para.tmax(), t=tmin, dt=(tmax-tmin)/(np-1);
    for (number_t i=1;i<=np;++i,t+=dt)
    {
      if (i==np) t=tmax;
      Point pt(t);
      ocpts->SetValue(i,to_gpPnt(parm(pt)));
      tgs.SetValue(i,to_gpVec(parm.tangent(pt)));
      flags->SetValue(i,true);
    }
    real_t tol=1.E-7;
    GeomAPI_Interpolate gi(ocpts, para.isPeriodic() || para.isClosed(),tol);
    gi.Load(tgs,flags,false);
    gi.Perform();
    TopoDS_Edge e = BRepBuilderAPI_MakeEdge(gi.Curve());
    return new TopoDS_Shape(e);
  }


  //! create TopoDS_Shape related to ParametrizedArc (1D)
  //! caution: split curve in nbparts segments (only an approximate of the parametrized arc)
  ShapeDocPair ocParametrizedArcLin(ParametrizedArc& para)
  {
    const std::vector<Point>& pts=para.p();
    TopTools_ListOfShape args, tools;
    args.Append(BRepBuilderAPI_MakeEdge(to_gpPnt(pts[0]),to_gpPnt(pts[1])).Shape());
    for (number_t i=1;i<para.nbParts();++i)
      tools.Append(BRepBuilderAPI_MakeEdge(to_gpPnt(pts[i]),to_gpPnt(pts[i+1])).Shape());
    BRepAlgoAPI_BooleanOperation bop;
    bop.SetArguments(args); bop.SetTools(tools);
    bop.SetOperation(BOPAlgo_FUSE);
    bop.Build();
    //bop.SimplifyResult(true,false);
    if (bop.HasErrors()) {std::cout<< "Errors in ocBooleanOperation ";bop.DumpErrors(std::cout); std::cout<<std::flush;}
    if (bop.HasWarnings()) {std::cout<< "Warnings in ocBooleanOperation ";bop.DumpWarnings(std::cout); std::cout<<std::flush;}
    TopoDS_Shape * shape= new TopoDS_Shape(bop.Shape());

    TDocStd_Document* ocdoc = new TDocStd_Document("MDTV-Standard");
    TDF_Label root = ocdoc->Main();
    TDF_Label shapeL = root.NewChild();
    TNaming_Builder(shapeL).Generated(*shape);
    addNamedDataAttribute(shapeL,1,"",0,0,0.);
    number_t no=1;
    string_t name=para.domName();
    if (name=="") name="Omega";
    TopExp_Explorer exp(*shape,TopAbs_EDGE);
    for (;exp.More();exp.Next(),no++)
    {
      TDF_Label edgeL = root.NewChild();
      TNaming_Builder(edgeL).Generated(exp.Current());
      number_t nnode=0;
      addNamedDataAttribute(edgeL,no,name,0,nnode,0.);
    }
    exp.Init(*shape,TopAbs_VERTEX);
    no=1; real_t h=1.;
    std::set<int> hc;
    if (para.h().size()>0) h=para.h()[0];
    for (;exp.More();exp.Next())
    {
      int hcc=exp.Current().HashCode(INT_MAX);
      if (hc.find(hcc)==hc.end())
      {
        hc.insert(hcc);
        TDF_Label vertexL = root.NewChild();
        TNaming_Builder(vertexL).Generated(exp.Current());
        addNamedDataAttribute(vertexL,no,"",0,0,h);
        no++;
      }
    }

    //finalization
    return ShapeDocPair(shape,ocdoc);
  }

  //! create TopoDS_Shape related to Polygon and its childs (2D)
  TopoDS_Shape* ocPolygon(Polygon& po)
  {
    const std::vector<Point>& pts = po.p();
    BRepBuilderAPI_MakePolygon pol;
    std::vector<Point>::const_iterator itp=pts.begin();
    for (; itp!=pts.end(); ++itp)
      pol.Add(to_gpPnt(*itp));
    pol.Close();
    return new TopoDS_Shape(BRepBuilderAPI_MakeFace(pol.Wire(),true));
  }

  //! create TopoDS_Shape related to Ellipse (2D)
  TopoDS_Shape* ocEllipse(Ellipse& ell)
  {
    const std::vector<Point>& pts = ell.p();
    const Point& C=pts[0];
    const Point& A=pts[1];
    const Point& B=pts[2];
    real_t r1=ell.radius1(), r2=ell.radius2();
    Point CA=A-C, CB=B-C;
    Point N=crossProduct(CA,CB);
    gp_Ax2 ax(to_gpPnt(C),to_gpDir(N/=norm(N)),to_gpDir(CA/norm(CA)));
    if (r1<r2)
      ax.SetXDirection(to_gpDir(CB/norm(CB)));
    gp_Elips el(ax, std::max(r1,r2),std::min(r1,r2));
    number_t nbsn=ell.sideNames().size();
    if (!ell.isSector()) //full ellipse
    {
      // build with 4 arcs
      Point P1=C+CA, P2=C+CB, P3=C-CA, P4=C-CB;
      Handle(Geom_TrimmedCurve) ac1 = GC_MakeArcOfEllipse(el, to_gpPnt(P1), to_gpPnt(P2),true);
      TopoDS_Edge ae1 = BRepBuilderAPI_MakeEdge(ac1);
      Handle(Geom_TrimmedCurve) ac2 = GC_MakeArcOfEllipse(el, to_gpPnt(P2), to_gpPnt(P3),true);
      TopoDS_Edge ae2 = BRepBuilderAPI_MakeEdge(ac2);
      Handle(Geom_TrimmedCurve) ac3 = GC_MakeArcOfEllipse(el, to_gpPnt(P3), to_gpPnt(P4),true);
      TopoDS_Edge ae3 = BRepBuilderAPI_MakeEdge(ac3);
      Handle(Geom_TrimmedCurve) ac4 = GC_MakeArcOfEllipse(el, to_gpPnt(P4), to_gpPnt(P1),true);
      TopoDS_Edge ae4 = BRepBuilderAPI_MakeEdge(ac4);
      BRepBuilderAPI_MakeWire mkw(ae1,ae2,ae3,ae4);
      return new TopoDS_Shape(BRepBuilderAPI_MakeFace(mkw.Wire(),true));
    }
    // ellipse sector
    const std::vector<Point>& v=ell.v();  // v[0] center, v[1] first arc point, v[2]/v[3] second/third arc point
    Handle(Geom_TrimmedCurve) ac = GC_MakeArcOfEllipse(el, to_gpPnt(v[1]), to_gpPnt(v[2]),true);
    TopoDS_Edge ae = BRepBuilderAPI_MakeEdge(ac);
    TopoDS_Edge s1 = BRepBuilderAPI_MakeEdge(to_gpPnt(C), to_gpPnt(v[1]));
    if (v.size()==3)
    {
      TopoDS_Edge s2 = BRepBuilderAPI_MakeEdge(to_gpPnt(v[2]), to_gpPnt(C));
      BRepBuilderAPI_MakeWire mkw(ae,s1,s2);
      return new TopoDS_Shape(BRepBuilderAPI_MakeFace(mkw.Wire(),true));
    }
    Handle(Geom_TrimmedCurve) ac2 = GC_MakeArcOfEllipse(el, to_gpPnt(v[2]), to_gpPnt(v[3]),true);
    TopoDS_Edge ae2 = BRepBuilderAPI_MakeEdge(ac2);
    TopoDS_Edge s2 = BRepBuilderAPI_MakeEdge(to_gpPnt(v[3]), to_gpPnt(C));
    BRepBuilderAPI_MakeWire mkw(ae,ae2,s1,s2);
    return new TopoDS_Shape(BRepBuilderAPI_MakeFace(mkw.Wire(),true));
  }

  //! create TopoDS_Shape related to Disk (2D)
  TopoDS_Shape* ocDisk(Disk& di)
  {
    const std::vector<Point>& pts = di.p();
    const Point& C=pts[0];
    const Point& A=pts[1];
    const Point& B=pts[2];
    real_t r=di.radius();
    Point CA=A-C, CB=B-C;
    Point N=crossProduct(CA,CB);
    gp_Ax2 ax(to_gpPnt(C),to_gpDir(N/=norm(N)),to_gpDir(CA/norm(CA)));
    gp_Circ ci(ax,r);
    if (!di.isSector()) //full disk in 4 parts
    {
      Handle(Geom_TrimmedCurve) ac1 = GC_MakeArcOfCircle(ci, to_gpPnt(pts[1]), to_gpPnt(pts[2]),true);
      Handle(Geom_TrimmedCurve) ac2 = GC_MakeArcOfCircle(ci, to_gpPnt(pts[2]), to_gpPnt(pts[3]),true);
      Handle(Geom_TrimmedCurve) ac3 = GC_MakeArcOfCircle(ci, to_gpPnt(pts[3]), to_gpPnt(pts[4]),true);
      Handle(Geom_TrimmedCurve) ac4 = GC_MakeArcOfCircle(ci, to_gpPnt(pts[4]), to_gpPnt(pts[1]),true);
      TopoDS_Edge ae1 = BRepBuilderAPI_MakeEdge(ac1);
      TopoDS_Edge ae2 = BRepBuilderAPI_MakeEdge(ac2);
      TopoDS_Edge ae3 = BRepBuilderAPI_MakeEdge(ac3);
      TopoDS_Edge ae4 = BRepBuilderAPI_MakeEdge(ac4);
      BRepBuilderAPI_MakeWire mkw(ae1,ae2,ae3,ae4);
      return new TopoDS_Shape(BRepBuilderAPI_MakeFace(mkw.Wire(),true));
    }
    // disk sector
    const std::vector<Point>& v=di.v();  // v[0] center, v[1] first arc point, v[2]/v[3] second/third arc point
    Handle(Geom_TrimmedCurve) ac = GC_MakeArcOfCircle(ci, to_gpPnt(v[1]), to_gpPnt(v[2]),true);
    TopoDS_Edge ae = BRepBuilderAPI_MakeEdge(ac);
    TopoDS_Edge s1 = BRepBuilderAPI_MakeEdge(to_gpPnt(C), to_gpPnt(v[1]));
    if (v.size()==3)
    {
      TopoDS_Edge s2 = BRepBuilderAPI_MakeEdge(to_gpPnt(v[2]), to_gpPnt(C));
      BRepBuilderAPI_MakeWire mkw(ae,s1,s2);
      return new TopoDS_Shape(BRepBuilderAPI_MakeFace(mkw.Wire(),true));
    }
    Handle(Geom_TrimmedCurve) ac2 = GC_MakeArcOfCircle(ci, to_gpPnt(v[2]), to_gpPnt(v[3]),true);
    TopoDS_Edge ae2 = BRepBuilderAPI_MakeEdge(ac2);
    TopoDS_Edge s2 = BRepBuilderAPI_MakeEdge(to_gpPnt(v[3]), to_gpPnt(C));
    BRepBuilderAPI_MakeWire mkw(ae,ae2,s1,s2);
    return new TopoDS_Shape(BRepBuilderAPI_MakeFace(mkw.Wire(),true));
  }

  //! create TopoDS_Shape related to ParametrizedSurface (2D)
  //! using OC Nurbs to approximate the parametrized surface
  //! do not use the linear partition (union of triangles) but a grid partition (parametrization support must be a rectangle)
  ShapeDocPair ocParametrizedSurface(ParametrizedSurface& pars)
  {
    const Parametrization& parm=pars.parametrization();
    if (parm.geomSupport().shape()!=_rectangle)
      error("free_error","ocParametrizedSurface can deal only with rectangular parametrization, use explicit linear partition");
    real_t h=std::sqrt(parm.geomSupport().measure()/pars.nbParts());
    RealPair bx=parm.bounds(_x), by=parm.bounds(_y);
    number_t nx= number_t(std::max(std::round(std::abs(bx.second-bx.first)/h),1.));
    number_t ny= number_t(std::max(std::round(std::abs(by.second-by.first)/h),1.));
    real_t dx=(bx.second-bx.first)/nx, dy=(by.second-by.first)/ny;
    TColgp_Array2OfPnt ocpts(1,nx+1,1,ny+1);
    real_t x=bx.first, y;
    for (number_t i=0;i<=nx;i++,x+=dx)
    {
      y=by.first;
      for (number_t j=0;j<=ny;j++,y+=dy) ocpts.SetValue(i+1,j+1,to_gpPnt(parm(x,y)));
    }
    GeomAPI_PointsToBSplineSurface gi(ocpts);
    TopoDS_Shape* shape= new TopoDS_Shape(BRepBuilderAPI_MakeFace(gi.Surface(),theTolerance).Face());

    //create doc
    TDocStd_Document* ocdoc = new TDocStd_Document("MDTV-Standard");
    TDF_Label root = ocdoc->Main();
    TDF_Label shapeL = root.NewChild();
    TNaming_Builder(shapeL).Generated(*shape);
    addNamedDataAttribute(shapeL,1,"",0,0,0.);
    // reference faces
    number_t no=1;
    string_t name=pars.domName();
    if (name=="") name="Omega";
    TopExp_Explorer exp(*shape,TopAbs_FACE);
    for (;exp.More();exp.Next(),no++)
    {
      TDF_Label faceL = root.NewChild();
      TNaming_Builder(faceL).Generated(exp.Current());
      addNamedDataAttribute(faceL,no,name,0,0,0.);
    }
    // reference edges
    std::vector<Point> vs(4);
    vs[0]=parm(bx.first,by.first);vs[1]=parm(bx.second,by.first);
    vs[2]=parm(bx.second,by.second);vs[3]=parm(bx.first,by.second);
    theCout<<"vs="<<vs<<eol;
    std::vector<std::vector<const Point*> > bns;
    name="";
    string_t na;
    number_t nbsn=pars.sideNames().size();
    if (nbsn>0) // it should be !!!
    {
      bns.resize(4,std::vector<const Point*>(2,0)); //side nodes
      bns[0][0]=&vs[0];bns[0][1]=&vs[1];bns[1][0]=&vs[1];bns[1][1]=&vs[2];
      bns[2][0]=&vs[2];bns[2][1]=&vs[3];bns[3][0]=&vs[3];bns[3][1]=&vs[0];
      name=pars.sideNames()[0];
    }
    no=1; number_t nnode=0;
    if (pars.h().size()==0) nnode=pars.n()[0];
    std::set<int> hc;
    exp.Init(*shape,TopAbs_EDGE);
    for (;exp.More();exp.Next())
    {
      int hcc=exp.Current().HashCode(INT_MAX);
      if (hc.find(hcc)==hc.end())
      {
        hc.insert(hcc);
        TDF_Label edgeL = root.NewChild();
        TNaming_Builder(edgeL).Generated(exp.Current());
        na="";
        if (nbsn>0)
        {
          std::list<Point> ocv=OCvertices(exp.Current());
          for (number_t s=0;s<4;s++)
          {
            if (commonPoints(ocv,bns[s])>=2)  // side found
            {
              if (nbsn>1) na=pars.sideNames()[std::min(s,nbsn-1)];
              else na=name;
            }
          }
        }
        addNamedDataAttribute(edgeL,no,na,0,nnode,0.);
        no++;
      }
    }
    // reference vertices
    exp.Init(*shape,TopAbs_VERTEX);
    no=1; h=0.;
    if (pars.h().size()>0) h=pars.h()[0];
    for (;exp.More();exp.Next())
    {
      int hcc=exp.Current().HashCode(INT_MAX);
      if (hc.find(hcc)==hc.end())
      {
        hc.insert(hcc);
        TDF_Label vertexL = root.NewChild();
        TNaming_Builder(vertexL).Generated(exp.Current());
        addNamedDataAttribute(vertexL,no,"",0,0,h);
        no++;
      }
    }
    //finalization
    return ShapeDocPair(shape,ocdoc);
  }

  //! create TopoDS_Shape related to ParametrizedSurface (2D)
  //! using union of triangles to approximate the parametrized surface
  ShapeDocPair ocParametrizedSurfaceLin(ParametrizedSurface& pars)
  {
    // travel xlifepp parametrization mesh
    BRepBuilderAPI_Sewing sewer;
    Mesh* mesh=pars.parametrizationP()->meshP();
    if (mesh==nullptr) error("free_error","no mesh support in ParametrizedSurface, a mesh is required by ocParametrizedSurfaceLin");
    const std::vector<GeomElement*>& elts=mesh->elements();
    std::vector<GeomElement*>::const_iterator ite=elts.begin();
    for (;ite!=elts.end();++ite)
    {
      std::vector<number_t> vns=(*ite)->vertexNumbers();
      BRepBuilderAPI_MakePolygon pol;
      for (number_t k=0;k<vns.size();k++) pol.Add(to_gpPnt(pars.p()[vns[k]-1]));
      pol.Close();
      sewer.Add(BRepBuilderAPI_MakeFace(pol.Wire(),true).Shape());
    }
    sewer.Perform();
    TopoDS_Shape* shape=new TopoDS_Shape(sewer.SewedShape());

    //create doc
    TDocStd_Document* ocdoc = new TDocStd_Document("MDTV-Standard");
    TDF_Label root = ocdoc->Main();
    TDF_Label shapeL = root.NewChild();
    TNaming_Builder(shapeL).Generated(*shape);
    addNamedDataAttribute(shapeL,1,"",0,0,0.);
    // reference faces
    number_t no=1;
    string_t name=pars.domName();
    if (name=="") name="Omega";
    TopExp_Explorer exp(*shape,TopAbs_FACE);
    for (;exp.More();exp.Next(),no++)
    {
      TDF_Label faceL = root.NewChild();
      TNaming_Builder(faceL).Generated(exp.Current());
      addNamedDataAttribute(faceL,no,name,0,0,0.);
    }
    // reference edges
    no=1; number_t nnode=0;
    std::vector<std::vector<const Point*> > bns(pars.nbSides()); //side nodes
    name="";
    string_t na;
    number_t nbsn=pars.sideNames().size();
    if (nbsn>0) // it should be !!!
    {
      for (number_t s=0;s<pars.nbSides();s++) bns[s]=pars.nodesOnSide(s+1);
      name=pars.sideNames()[0];
    }
    if (pars.h().size()==0) nnode=pars.n()[0];
    std::set<int> hc;
    exp.Init(*shape,TopAbs_EDGE);
    for (;exp.More();exp.Next())
    {
      int hcc=exp.Current().HashCode(INT_MAX);
      if (hc.find(hcc)==hc.end())
      {
        hc.insert(hcc);
        TDF_Label edgeL = root.NewChild();
        TNaming_Builder(edgeL).Generated(exp.Current());
        na="";
        if (nbsn>0)
        {
          std::list<Point> ocv=OCvertices(exp.Current());
          for (number_t s=0;s<pars.nbSides();s++)
          {
            if (commonPoints(ocv,bns[s])>=2)  // side found
            {
              if (nbsn>1) na=pars.sideNames()[std::min(s,nbsn-1)];
              else na=name;
            }
          }
        }
        addNamedDataAttribute(edgeL,no,na,0,nnode,0.);
        no++;
      }
    }
    // reference vertices
    exp.Init(*shape,TopAbs_VERTEX);
    no=1; real_t h=0.;
    if (pars.h().size()>0) h=pars.h()[0];
    for (;exp.More();exp.Next())
    {
      int hcc=exp.Current().HashCode(INT_MAX);
      if (hc.find(hcc)==hc.end())
      {
        hc.insert(hcc);
        TDF_Label vertexL = root.NewChild();
        TNaming_Builder(vertexL).Generated(exp.Current());
        addNamedDataAttribute(vertexL,no,"",0,0,h);
        no++;
      }
    }

    //finalization
    return ShapeDocPair(shape,ocdoc);
  }

  //! create TopoDS_Shape related to SplineSurface (2D)
  ShapeDocPair ocSplineSurface(SplineSurface& sps)
  {
    const Nurbs* spnu=sps.spline()->nurbs();
    const std::vector<std::vector<Point> >& pts=spnu->controlPointsM();
    const std::multimap<real_t,number_t>& uknots=spnu->bsu()->knots();
    const std::multimap<real_t,number_t>& vknots=spnu->bsv()->knots();
    const std::vector<std::vector<real_t> >& weights=spnu->weightsM();
    // set oc points (poles)
    number_t nbu=pts.size(), nbv=pts[0].size();
    TColgp_Array2OfPnt ocpts(1,nbu,1,nbv);
    for (number_t i=0;i<nbu;++i)
      for (number_t j=0;j<nbv;++j)
      ocpts.SetValue(i+1,j+1,to_gpPnt(pts[i][j]));
    //set oc u-knots
    std::multimap<real_t,number_t>::const_iterator itm=uknots.begin();
    real_t val=itm->first; itm++;
    number_t nku=1;
    for (;itm!=uknots.end();++itm) if (itm->first!=val) {nku++;val=itm->first;}
    TColStd_Array1OfReal ocuknots(1,nku);
    TColStd_Array1OfInteger ocumults(1,nku);
    itm=uknots.begin(); val=itm->first;
    number_t i=1, mult=1;
    ocuknots.SetValue(i,val); ocumults.SetValue(i,mult); itm++;
    for (;itm!=uknots.end();++itm)
    {
      if (itm->first!=val)
      {
        val=itm->first;
        mult=1;  i++;
        ocuknots.SetValue(i,val);
        ocumults.SetValue(i,mult);
      }
      else
      {
        mult++;
        ocumults.SetValue(i,mult);
      }
    }
    //set oc v-knots
    itm=vknots.begin(); val=itm->first; itm++;
    number_t nkv=1;
    for (;itm!=vknots.end();++itm) if (itm->first!=val) {nkv++;val=itm->first;}
    TColStd_Array1OfReal ocvknots(1,nkv);
    TColStd_Array1OfInteger ocvmults(1,nkv);
    itm=vknots.begin(); val=itm->first;
    i=1; mult=1;
    ocvknots.SetValue(i,val); ocvmults.SetValue(i,mult); itm++;
    for (;itm!=vknots.end();++itm)
    {
      if (itm->first!=val)
      {
        val=itm->first;
        mult=1;  i++;
        ocvknots.SetValue(i,val);
        ocvmults.SetValue(i,mult);
      }
      else
      {
        mult++;
        ocvmults.SetValue(i,mult);
      }
    }
    // create TopoDS_Shape
    Handle(Geom_BSplineSurface) bss;
    if (spnu->noWeight())
    {
      bss= new Geom_BSplineSurface(ocpts, ocuknots, ocvknots, ocumults, ocvmults, spnu->bsu()->degree(), spnu->bsv()->degree(), spnu->bsu()->isPeriodic(), spnu->bsv()->isPeriodic());
    }
    else
    {
      TColStd_Array2OfReal  ocweights(1,nbu,1,nbv);
      for (number_t i=0;i<nbu;++i)
        for (number_t j=0;i<nbv;++j)
          ocweights.SetValue(i+1,j+1,weights[i][j]);
      bss= new Geom_BSplineSurface(ocpts, ocweights, ocuknots, ocvknots, ocumults, ocvmults, spnu->bsu()->degree(), spnu->bsv()->degree(), spnu->bsu()->isPeriodic(), spnu->bsv()->isPeriodic());
    }
    TopoDS_Shape* shape= new TopoDS_Shape(BRepBuilderAPI_MakeFace(bss,theTolerance).Face());

    //create doc
    TDocStd_Document* ocdoc = new TDocStd_Document("MDTV-Standard");
    TDF_Label root = ocdoc->Main();
    TDF_Label shapeL = root.NewChild();
    TNaming_Builder(shapeL).Generated(*shape);
    string_t name=sps.domName();
    if (name=="") name="Omega";
    addNamedDataAttribute(shapeL,1,name,0,0,0.);
    // reference edges
    number_t no=1;
    std::vector<Point>& p=sps.p();
    std::vector<std::vector<const Point*> > bns;
    name="";
    string_t na;
    number_t nbsn=sps.sideNames().size();
    if (nbsn>0) // it should be !!!
    {
      bns.resize(4,std::vector<const Point*>(2,0)); //side nodes
      bns[0][0]=&p[0];bns[0][1]=&p[1];bns[1][0]=&p[1];bns[1][1]=&p[2];
      bns[2][0]=&p[2];bns[2][1]=&p[3];bns[3][0]=&p[3];bns[3][1]=&p[0];
      name=sps.sideNames()[0];
    }
    no=1; number_t nnode=0;
    if (sps.h().size()==0) nnode=sps.n()[0];
    std::set<int> hc;
    std::set<number_t> sides;  // to avoid to pick same sidenames in case of "degenerated" nurbs (only 2 edges)
    TopExp_Explorer exp(*shape,TopAbs_EDGE);
    for (;exp.More();exp.Next())
    {
      int hcc=exp.Current().HashCode(INT_MAX);
      if (hc.find(hcc)==hc.end())
      {
        hc.insert(hcc);
        TDF_Label edgeL = root.NewChild();
        TNaming_Builder(edgeL).Generated(exp.Current());
        na="";
        if (nbsn>0)
        {
          std::list<Point> ocv=OCvertices(exp.Current());
          number_t nocv=ocv.size();
          for (number_t s=0;nocv>1 && s<4;s++)
          {
            if (commonPoints(ocv,bns[s],100*theTolerance)>=2)  // side found
            {
              if (sides.find(s)==sides.end())
              {
                if (nbsn>1) na=sps.sideNames()[std::min(s,nbsn-1)];
                else na=name;
                sides.insert(s);
                break;
              }
            }
          }
        }
        addNamedDataAttribute(edgeL,no,na,0,nnode,0.);
        no++;
      }
    }
    // reference vertices
    exp.Init(*shape,TopAbs_VERTEX);
    real_t h=1.; no=1;
    if (sps.h().size()>0) h=sps.h()[0];
    for (;exp.More();exp.Next())
    {
      int hcc=exp.Current().HashCode(INT_MAX);
      if (hc.find(hcc)==hc.end())
      {
        hc.insert(hcc);
        TDF_Label vertexL = root.NewChild();
        TNaming_Builder(vertexL).Generated(exp.Current());
        addNamedDataAttribute(vertexL,no,"",0,0,h);
        no++;
      }
    }
    //finalization
    return ShapeDocPair(shape,ocdoc);
  }

  //! create TopoDS_Shape related to Polyhedron (3D)
  TopoDS_Shape* ocPolyhedron(Polyhedron& pol)
  {
    const std::vector<Polygon*>& faces= pol.faces(); //!< faces of the polyhedron
    number_t nf=faces.size();
    if (nf==0)
      error("free_error","polyhedronToOpenCascade fails: no faces defined in polyhedron");
    BRepBuilderAPI_Sewing sewer;
    for (number_t f=0; f<nf; f++) //loop on faces
    {
      TopoDS_Shape* tdf=ocGeometry(*faces[f]);
      sewer.Add(*tdf);
      delete tdf;
    }
    sewer.Perform();
    TopoDS_Shape sewedFaces(sewer.SewedShape());
    BRepBuilderAPI_MakeSolid brep_solid(TopoDS::Shell(sewedFaces));
    return new TopoDS_Shape(brep_solid.Solid());
  }

  //! create TopoDS_Shape related to Cuboid (3D)
  TopoDS_Shape* ocCuboid(Cuboid& cu)  // full cuboid (nbOctants=8)
  {
    const std::vector<Point>& pts = cu.p();
    const Point& O=pts[0];  // origin
    const Point& A=pts[1];  // "x" vertex
    const Point& B=pts[3];  // "y" vertex
    const Point& C=pts[4];  // "z" vertex
    Point N=C-O, Vx=A-O;
    real_t nN=norm(N), nVx=norm(Vx);
    gp_Ax2 ax(to_gpPnt(O),to_gpDir(N/nN),to_gpDir(Vx/nVx));
    BRepPrimAPI_MakeBox box(ax, nVx, norm(B-O), nN);
    box.Build();
    return new TopoDS_Shape(box.Solid());
  }

  //! create TopoDS_Shape related to Ball/Ellipsoid (3D)
  TopoDS_Shape* ocEllipsoid(Ellipsoid& ell)
  {
    ShapeType sh= ell.shape();
    real_t r=1.;
    if (sh==_ball) r=ell.radius1();
    Point O=ell.center();

    //create unit sphere (center O) with different octant
    TopoDS_Shape oct1 = BRepPrimAPI_MakeSphere(to_gpPnt(O),r,0, pi_/2,pi_/2).Shape();
    TopoDS_Shape oct=oct1, oct2;
    number_t nbo=ell.nbOctants();
    if (nbo==0) nbo=8;
    if (nbo>1)
    {
      gp_Ax1 oz(to_gpPnt(O),gp_Dir(0,0,1));
      gp_Trsf rot;
      rot.SetRotation(oz,pi_/2);
      oct = BRepAlgoAPI_Fuse(oct,BRepBuilderAPI_Transform(oct1,rot,true).Shape());
      if (nbo>2)
      {
        rot.SetRotation(oz,pi_);
        oct = BRepAlgoAPI_Fuse(oct,BRepBuilderAPI_Transform(oct1,rot,true).Shape());
        if (nbo>3)
        {
          rot.SetRotation(oz,3*pi_/2);
          oct = BRepAlgoAPI_Fuse(oct,BRepBuilderAPI_Transform(oct1,rot,true).Shape());
          if (nbo>4)
          {
            oct1=BRepPrimAPI_MakeSphere(to_gpPnt(O),r,-pi_/2, 0, pi_/2).Shape();
            oct = BRepAlgoAPI_Fuse(oct,oct1);
            if (nbo>5)
            {
              rot.SetRotation(oz,pi_/2);
              oct = BRepAlgoAPI_Fuse(oct,BRepBuilderAPI_Transform(oct1,rot,true).Shape());
              if (nbo>6)
              {
                rot.SetRotation(oz,pi_);
                oct = BRepAlgoAPI_Fuse(oct,BRepBuilderAPI_Transform(oct1,rot,true).Shape());
                if (nbo>7)
                {
                  rot.SetRotation(oz,3*pi_/2);
                  oct = BRepAlgoAPI_Fuse(oct,BRepBuilderAPI_Transform(oct1,rot,true).Shape());
                } //end 7
              } //end 6
            } //end 5
          } //end 4
        } //end 3
      } //end 2
    } //end 1

    if (oct.ShapeType()==TopAbs_COMPOUND)
    {
      TopExp_Explorer exps(oct,TopAbs_SOLID);
      number_t ns=0;
      for (;exps.More();exps.Next()) ns++;
      if (ns==1)
      {
        exps.Init(oct,TopAbs_SOLID);
        oct=TopoDS::Solid(exps.Current());
      }
    }

    if (sh==_ellipsoid) // transform unit sphere to ellipsoid
    {
      const std::vector<Point>& pts = ell.p();
      real_t r1=ell.radius1(), r2=ell.radius2(), r3=ell.radius3();
      const Point& A=pts[1];  // "x" apogee
      const Point& B=pts[2];  // "y" apogee
      const Point& C=pts[6];  // "z" apogee
      Point N=C-O, Vx=A-O;
      N/=norm(N);
      Vx/=norm(Vx);
      gp_Mat M(to_gpXYZ(Vx),to_gpXYZ(cross3D(N,Vx)),to_gpXYZ(N)); // rotation matrix
      M*=gp_Mat(r1,0, 0,0,r2,0,0,0,r3);                           // scaling
      gp_XYZ V = to_gpXYZ(O)-M*to_gpXYZ(O);
      gp_GTrsf tr(M,V);   // transformation: Q=M*P+V = M*(P-O)+O
      oct = BRepBuilderAPI_GTransform(oct,tr).Shape();
    }
    return new TopoDS_Shape(oct);
  }

  //! create TopoDS_Shape related to Trunk and its childs (3D)
  TopoDS_Shape* ocTrunk(Trunk& tru)
  {
    if (tru.isElliptical())
    {
      const Point& c1=tru.center1();
      const Point& c2=tru.center2();
      const std::vector<Point>& P=tru.p();
      Point vx=P[1]-c1, vy=P[2]-c1;
      BRepOffsetAPI_ThruSections bt(true,true);
      Ellipse e1(_center=c1, _v1=P[1],_v2=P[2],_domain_name="base",_side_names=Strings("S1","S2","S3","S4"));
      TopoDS_Shape *base1 = ocEllipse(e1);
      TopoDS_Wire wb=TopoDS::Wire(TopExp_Explorer(*base1,TopAbs_WIRE).Current());
      bt.AddWire(wb);
      delete base1;
      real_t scale=tru.scale();
      if (tru.shape()==_cone || scale==0.)
        bt.AddVertex(BRepBuilderAPI_MakeVertex(to_gpPnt(tru.cone()->apex())));
      else
      {
        Point Q1 = c2 + scale*vx, Q2 = c2 + scale*vy;
        Ellipse e2(_center=c2, _v1=Q1,_v2=Q2,_domain_name="base",_side_names=Strings("S1","S2","S3","S4"));
        TopoDS_Shape *base2 = ocEllipse(e2);
        TopoDS_Wire wb2=TopoDS::Wire(TopExp_Explorer(*base2,TopAbs_WIRE).Current());
        bt.AddWire(wb2);
        delete base2;
      }
      return new TopoDS_Shape(bt.Shape());
    }
    //polygonal basis (convert to  polyhedron)
    bool isPyramid = (tru.shape()==_pyramid);
    const Polygon& base=*tru.basis()->polygon(); // trunk basis
    number_t bpsize = base.p().size();
    const std::vector<Point>& P= tru.p();        // trunk points
    real_t scale=tru.scale();
    if (isPyramid || scale==0.)
    {
      std::vector<Polygon> faces(bpsize+1);
      faces[0]=base;
      for (number_t i=0; i<bpsize-1; i++)
        faces[i+1]=Triangle(_v1=P[i],_v2=P[i+1],_v3=P[bpsize],_domain_name="face_"+tostring(i+1));
      faces[bpsize]=Triangle(_v1=P[bpsize-1],_v2=P[0],_v3=P[bpsize],_domain_name="face_"+tostring(bpsize));
      Polyhedron pol(_faces=faces,_domain_name="polyhedron");
      return ocPolyhedron(pol);
    }
    else
    {
      Polygon base2(base);
      base2.translate(_direction=std::vector<real_t>(tru.origin()-P[0]));
      if (scale!=1.)
        base2.homothetize(_center=tru.origin(),_scale=scale);
      std::vector<Polygon> faces(bpsize+2);
      faces[0]=base;
      faces[1]=base2;
      for (number_t i=0; i<bpsize-1; i++)
      {
        faces[i+2]=Quadrangle(_v1=P[i],_v2=P[i+1],_v3=P[bpsize+i+1],_v4=P[bpsize+i],_domain_name="face_"+tostring(i+1));
      }
      faces[bpsize+1]=Quadrangle(_v1=P[bpsize-1],_v2=P[0],_v3=P[bpsize],_v4=P[2*bpsize-1],_domain_name="face_"+tostring(bpsize));
      Polyhedron pol(_faces=faces,_domain_name="polyhedron");
      return ocPolyhedron(pol);
    }
  }

  TopoDS_Shape* ocRevTrunk(RevTrunk& rtr)
  {
    const std::vector<Point>& p=rtr.p();
    const Point& c1=rtr.center1();
    const Point& c2=rtr.center2();
    real_t r1=rtr.radius1(), r2=rtr.radius2();
    Point N=c2-c1;
    N/=norm(N); //length and trunk direction
    TopoDS_Shape* tm=nullptr, *tmo=nullptr;
    //trunk part
    real_t scale=r2/r1;
    if (scale!=0)
      tm=ocTrunk(*(Trunk(_center1= c1,_v1=p[1],_v2=p[2],_scale=r2/r1,_center2=c2)).trunk());
    else
      tm=ocTrunk(*(Cone(_center1= c1,_v1=p[1],_v2=p[2],_apex=c2)).trunk());
    tmo=tm;

    //end shape 1
    TopoDS_Shape* te=nullptr;
    real_t d1 = rtr.distance1();
    switch (rtr.endShape1())
    {
      case _gesFlat:
        break;
      case _gesCone:
        te = ocTrunk(*(Cone(_center1=c1,_v1=p[1],_v2=p[2],_apex=c1-d1*N)).trunk());
        break;// cone (d1!=0)
      case _gesSphere:
      case _gesEllipsoid:
        te = ocEllipsoid(*(Ellipsoid(_center=c1,_v1=p[1],_v2=p[2],_v6=c1-d1*N,_nboctants=4)).ellipsoid());
        break; //ellipsoid (d1!=0)
      case _gesNone: // remove base face
      {
        TopoDS_Shape f;
        TopExp_Explorer exp;
        std::vector<const Point*> vs(4);
        vs[0]=&p[1];
        vs[1]=&p[1];
        vs[2]=&p[3];
        vs[3]=&p[4];
        for (exp.Init(*tm, TopAbs_FACE); exp.More(); exp.Next())
        {
          f=TopoDS::Face(exp.Current());
          std::list<Point> OCvs = OCvertices(f,theTolerance);
          if (commonPoints(OCvs,vs,theTolerance)>2)
            break;  //face localized
        }
        ShapeBuild_ReShape rmf;
        rmf.Remove(f);
        tm = new TopoDS_Shape(rmf.Apply(*tm));
        delete tmo;
        tmo=tm;
        break;
      }
      default:
        break;
    }
    if (te!=nullptr) {
      tm= new TopoDS_Shape(BRepAlgoAPI_Fuse(*tm,*te));
      delete te;
    }

    //end shape 2
    te=nullptr;
    real_t d2 = rtr.distance2();
    // shape of the second end
    switch (rtr.endShape2())
    {
      case _gesFlat:
        break;
      case _gesCone:
        te = ocTrunk(*(Cone(_center1=c2,_v1=p[6],_v2=p[7],_apex=c2+d2*N)).trunk());
        break;// cone (d2!=0)
      case _gesSphere:
      case _gesEllipsoid:
        te = ocEllipsoid(*(Ellipsoid(_center=c2,_v1=p[6],_v2=p[7],_v6=c2+d2*N,_nboctants=4)).ellipsoid());
        break; //ellipsoid (d2!=0)
      case _gesNone: // remove base face
      {
        TopoDS_Shape f;
        TopExp_Explorer exp;
        std::vector<const Point*> vs(4);
        vs[0]=&p[6];
        vs[1]=&p[7];
        vs[2]=&p[8];
        vs[3]=&p[9];
        for (exp.Init(*tm, TopAbs_FACE); exp.More(); exp.Next())
        {
          f=TopoDS::Face(exp.Current());
          std::list<Point> OCvs = OCvertices(f,theTolerance);
          if (commonPoints(OCvs,vs,theTolerance)>2)
            break;  //face localized
        }
        dump(f,theCout);
        theCout<<std::flush;
        ShapeBuild_ReShape rmf;
        rmf.Remove(f);
        tm = new TopoDS_Shape(rmf.Apply(*tm));
        break;
      }
      default:
        break;
    }
    if (te!=nullptr) {
      tm= new TopoDS_Shape(BRepAlgoAPI_Fuse(*tm,*te));
      delete te;
    }
    if (tmo!=tm)
      delete tmo;

    //shape healing
    Handle(ShapeFix_Shape) sfs = new ShapeFix_Shape(*tm);
    sfs->SetPrecision(theTolerance);
    sfs->Perform();
    TopoDS_Shape hs = sfs->Shape();
    if (hs!=*tm)
    {
      tmo=tm;
      tm = new TopoDS_Shape(hs);
      delete tmo;
    }
    return tm;
  }

  ShapeDocPair ocComposite(Geometry& geo)
  {
    GeoNode* cur=geo.geoNode_;
    if (cur==nullptr) // composite with no geoNode, build from geometry
    {
      return geo.ocData().asShapeDocPair();
    }
    return cur->ocBuild();
  }

  ShapeDocPair ocShape(Geometry& geo)
  {
    GeoNode* cur=geo.geoNode_;
    if (cur==nullptr) // ocShape with no geoNode, build from geometry
    {
      return geo.ocData().asShapeDocPair();
    }
    return cur->ocBuild();
  }

  ShapeDocPair ocBooleanOperation(GeoOperation op, ShapeDocPair& oc1, ShapeDocPair& oc2)
  {
    const TopoDS_Shape& shape1=*oc1.first;
    const TopoDS_Shape& shape2=*oc2.first;
    if (!BRepCheck_Analyzer(shape1).IsValid()) theCout<<"in ocBooleanOperation, BRepCheck_Analyzer says shape 1 is not valid"<<eol;
    if (!BRepCheck_Analyzer(shape2).IsValid()) theCout<<"in ocBooleanOperation, BRepCheck_Analyzer says shape 2 is not valid"<<eol;
    TopoDS_Shape* shaper=nullptr;
    Handle(BRepTools_History) hist;
    BRepAlgoAPI_BooleanOperation bop;
    TopTools_ListOfShape args, tools;
    args.Append(shape1); tools.Append(shape2);
    bop.SetArguments(args); bop.SetTools(tools);
    switch (op)
    {
    case _plusGeOp: bop.SetOperation(BOPAlgo_FUSE); break;
    case _minusGeOp: bop.SetOperation(BOPAlgo_CUT); break;
    case _commonGeOp: bop.SetOperation(BOPAlgo_COMMON); break;
    default: error("free_error","unknown boolean operation in ocBooleanOperation");
    }
    bop.SetFuzzyValue(0.);
    bop.SetNonDestructive(true);
    //bop.SetGlue(BOPAlgo_GlueShift);
    bop.Build();
    bop.SimplifyResult(true,false);
    if (bop.HasErrors()) {std::cout<< "Errors in ocBooleanOperation ";bop.DumpErrors(std::cout); std::cout<<std::flush;}
    if (bop.HasWarnings()) {std::cout<< "Warnings in ocBooleanOperation ";bop.DumpWarnings(std::cout); std::cout<<std::flush;}
    hist=bop.History();
    shaper=new TopoDS_Shape(bop.Shape());

    //shape healing
  //  Handle(ShapeFix_Shape) sfs = new ShapeFix_Shape(*shaper);
  //  sfs->SetPrecision(theTolerance);
  //  sfs->Perform();
  //  TopoDS_Shape hshaper = sfs->Shape();
  //  if (hshaper!=*shaper)
  //  {
  //    TopoDS_Shape* old=shaper;
  //    shaper = new TopoDS_Shape(hshaper);
  //    delete old;
  //  }

    //create dependance map newToOld, i.e for each subshape of the new shape
    //    it status (unchanged=0, modified=1 or generated=2)
    //    the list of <subshape/root shape> involved (only one when unchanged, at least one when modified or generated)
    // because newToOld map is based on hashcodes of subshapes, two additional maps are built to index shapes with their hashcode
    // Note: using pointers on subshapes (avoiding hascodde maps) seems not to be robust ...
    std::list<const TopoDS_Shape*> shapes;
    shapes.push_back(&shape1);  shapes.push_back(&shape2);
    std::map<int ,TopoDS_Shape> hashShape, hashNewShape;
    std::map<int ,std::pair<number_t,std::list<std::pair<int,int> > > > newToOld;
    dependanceMap(*shaper,shapes,*hist,hashShape,hashNewShape,newToOld);  //topological dependance in process

    //create map relating original subshapes hashcode to Label
    std::map<int,Handle(TDataStd_NamedData)> shapeToLabel;
    Handle(TNaming_NamedShape) NS;
    Handle(TDataStd_NamedData) ND;
    int hc;
    for (TDF_ChildIterator itl(oc1.second->Main()); itl.More(); itl.Next())
    {
      TDF_Label l=itl.Value();
      if (l.FindAttribute(TNaming_NamedShape::GetID(),NS))
      {
        hc=NS->Get().HashCode(INT_MAX);
        if (l.FindAttribute(TDataStd_NamedData::GetID(),ND))  shapeToLabel[hc]=ND;
      }
    }
    for (TDF_ChildIterator itl(oc2.second->Main()); itl.More(); itl.Next())
    {
      TDF_Label l=itl.Value();
      if (l.FindAttribute(TNaming_NamedShape::GetID(),NS))
      {
        hc=NS->Get().HashCode(INT_MAX);
        if (l.FindAttribute(TDataStd_NamedData::GetID(),ND))  shapeToLabel[hc]=ND;
      }
    }

    if (theVerboseLevel>10)
    {
      theCout<<"shapeToLabel:"<<eol;
      std::map<int,Handle(TDataStd_NamedData)>::iterator its;
      for (its=shapeToLabel.begin();its!=shapeToLabel.end();++its)
      {
        int h=its->first;
        theCout<<asString(hashShape[h].ShapeType())<<" "<<h<<" -> ";
        Handle(TDataStd_NamedData) ND=its->second;
        theCout<<" "<<ND->GetInteger("num")<<" "<< ND->GetString("name")<< " tag="<<ND->GetInteger("tag")
            << " nnode="<<ND->GetInteger("nnode") << " hstep="<<ND->GetReal("hstep")<<eol;
      }
    }

    // create the TDF document
    std::list<std::pair<int,int> >::iterator itt;
    TDocStd_Document* ocdoc = new TDocStd_Document("MDTV-Standard");
    TDF_Label root = ocdoc->Main();
    setLabels(*shaper,root,TopAbs_SOLID,newToOld,hashShape,hashNewShape,shapeToLabel);
    setLabels(*shaper,root,TopAbs_FACE,newToOld,hashShape,hashNewShape,shapeToLabel);
    setLabels(*shaper,root,TopAbs_EDGE,newToOld,hashShape,hashNewShape,shapeToLabel);
    setLabels(*shaper,root,TopAbs_VERTEX,newToOld,hashShape,hashNewShape,shapeToLabel);

    //finalization
    return ShapeDocPair(shaper,ocdoc);
  }


  // relates newShape items to shapes item using the history
  // for each newShape item (solid, face, edge, vertex) associates the list of shapes item related to (unchanged 0, modified 1 or generated 2 )
  void dependanceMap(const TopoDS_Shape& newShape, const std::list<const TopoDS_Shape*>& shapes, const BRepTools_History& hist,
                        std::map<int ,TopoDS_Shape>& hashShape, std::map<int ,TopoDS_Shape>& hashNewShape,
                        std::map<int ,std::pair<number_t,std::list<std::pair<int,int> > > >& newToOld)

  {
    newToOld.clear();
    hashShape.clear();
    hashNewShape.clear();
    std::map<int ,TopoDS_Shape>::iterator ith;
    int h;
    TopExp_Explorer exp(newShape,TopAbs_SOLID);
    for (;exp.More();exp.Next()) // reference all SOLID sub-shapes of newShape
    {
      //printInfo(exp.Current(),theCout);
      h = exp.Current().HashCode(INT_MAX);
      newToOld[h]=std::make_pair(0,std::list<std::pair<int,int> >());
      hashNewShape.insert(std::make_pair(h,exp.Current()));
    }
    exp.Init(newShape,TopAbs_FACE);
    for (;exp.More();exp.Next()) // reference all FACE sub-shapes of newShape
    {
      //printInfo(exp.Current(),theCout);
      h = exp.Current().HashCode(INT_MAX);
      newToOld[h]=std::make_pair(0,std::list<std::pair<int,int> >());
      hashNewShape.insert(std::make_pair(h,exp.Current()));
    }
    exp.Init(newShape,TopAbs_EDGE);
    for (;exp.More();exp.Next()) // reference all EDGE sub-shapes of newShape
    {
      //printInfo(exp.Current(),theCout);
      h = exp.Current().HashCode(INT_MAX);
      newToOld[h]=std::make_pair(0,std::list<std::pair<int,int> >());
      hashNewShape.insert(std::make_pair(h,exp.Current()));
    }
    exp.Init(newShape,TopAbs_VERTEX);
    for (;exp.More();exp.Next()) // reference all VERTEX sub-shapes of newShape
    {
      //printInfo(exp.Current(),theCout);
      h = exp.Current().HashCode(INT_MAX);
      newToOld[h]=std::make_pair(0,std::list<std::pair<int,int> >());
      hashNewShape.insert(std::make_pair(h,exp.Current()));
    }

    std::list<const TopoDS_Shape*>::const_iterator its;
    TopTools_ListOfShape tls;
    TopTools_ListIteratorOfListOfShape itl;
    std::map<int ,std::pair<number_t,std::list<std::pair<int,int> > > >::iterator itm;
    for (its=shapes.begin();its!=shapes.end();++its) //Query modified and generated shapes for each initial shapes
    {
      int hs=(*its)->HashCode(INT_MAX);
      hashShape[hs]=**its;
      exp.Init(**its,TopAbs_SOLID); // ##### History does not track solids ! Do it in an other way: see below
      for (;exp.More();exp.Next()) // travel SOLID subshape of current shape *its
      {
        int hss = exp.Current().HashCode(INT_MAX);
        hashShape[hss]=exp.Current();
        tls = hist.Modified(exp.Current());
        itl.Initialize(tls);
        for (; itl.More(); itl.Next()) // travel modified shape related to current sub-shape itt.Value()
        {
            h=itl.Value().HashCode(INT_MAX);
            itm=newToOld.find(h);
            if (itm!=newToOld.end())
            {
                itm->second.first = 1;
                itm->second.second.push_back(std::make_pair(hss,hs)); //register modified shape
            }
        }
        tls = hist.Generated(exp.Current());
        itl.Initialize(tls);
        for (; itl.More(); itl.Next()) // travel generated shape related to current sub-shape itl.Value()
        {
            h=itl.Value().HashCode(INT_MAX);
            itm=newToOld.find(h);
            if (itm!=newToOld.end())
            {
                itm->second.first = 2;
                itm->second.second.push_back(std::make_pair(hss,hs)); //register modified shape
            }
        }
        itm=newToOld.find(hss);
        if (itm!=newToOld.end() && itm->second.second.size()==0) // find original shape in newShape
              itm->second.second.push_back(std::make_pair(hss,hs)); //register unchanged shape
      }
      exp.Init(**its,TopAbs_FACE);
      for (;exp.More();exp.Next()) // travel FACE subshape of current shape *its
      {
        int hss = exp.Current().HashCode(INT_MAX);
        hashShape[hss]=exp.Current();
        tls = hist.Modified(exp.Current());
        itl.Initialize(tls);
        for (; itl.More(); itl.Next()) // travel modified shape related to current sub-shape itl.Value()
        {
          h=itl.Value().HashCode(INT_MAX);
          itm=newToOld.find(h);
          if (itm!=newToOld.end())
          {
            itm->second.first = 1;
            itm->second.second.push_back(std::make_pair(hss,hs)); //register modified shape
          }
        }
        tls = hist.Generated(exp.Current());
        itl.Initialize(tls);
        for (; itl.More(); itl.Next()) // travel generated shape related to current sub-shape itl.Value()
        {
          h=itl.Value().HashCode(INT_MAX);
          itm=newToOld.find(h);
          if (itm!=newToOld.end())
          {
            itm->second.first = 2;
            itm->second.second.push_back(std::make_pair(hss,hs)); //register modified shape
          }
        }
        itm=newToOld.find(hss);
        if (itm!=newToOld.end()&& itm->second.second.size()==0) // find original shape in newShape
            itm->second.second.push_back(std::make_pair(hss,hs)); //register unchanged shape
      }
      exp.Init(**its,TopAbs_EDGE);
      for (;exp.More();exp.Next()) // travel EDGE subshape of current shape *its
      {
        int hss = exp.Current().HashCode(INT_MAX);
        hashShape[hss]=exp.Current();
        tls = hist.Modified(exp.Current());
        itl.Initialize(tls);
        for (; itl.More(); itl.Next()) // travel modified shape related to current sub-shape itl.Value()
        {
          h=itl.Value().HashCode(INT_MAX);
          itm=newToOld.find(h);
          if (itm!=newToOld.end())
          {
            itm->second.first = 1;
            itm->second.second.push_back(std::make_pair(hss,hs)); //register modified shape
          }
        }
        tls = hist.Generated(exp.Current());
        itl.Initialize(tls);
        for (; itl.More(); itl.Next()) // travel generated shape related to current sub-shape itl.Value()
        {
          h=itl.Value().HashCode(INT_MAX);
          itm=newToOld.find(h);
          if (itm!=newToOld.end())
          {
            itm->second.first = 2;
            itm->second.second.push_back(std::make_pair(hss,hs)); //register modified shape
          }
        }
        itm=newToOld.find(hss);
        if (itm!=newToOld.end()&& itm->second.second.size()==0) // find original shape in newShape
            itm->second.second.push_back(std::make_pair(hss,hs)); //register unchanged shape
      }
      exp.Init(**its,TopAbs_VERTEX);
      for (;exp.More();exp.Next()) // travel VERTEX subshape of current shape *its
      {
        int hss = exp.Current().HashCode(INT_MAX);
        hashShape[hss]=exp.Current();
        tls = hist.Modified(exp.Current());
        itl.Initialize(tls);
        for (; itl.More(); itl.Next()) // travel modified shape related to current sub-shape itl.Value()
        {
          h=itl.Value().HashCode(INT_MAX);
          itm=newToOld.find(h);
          if (itm!=newToOld.end())
          {
            itm->second.first = 1;
            itm->second.second.push_back(std::make_pair(hss,hs)); //register modified shape
          }
        }
        tls = hist.Generated(exp.Current());
        itl.Initialize(tls);
        for (; itl.More(); itl.Next()) // travel generated shape related to current sub-shape itl.Value()
        {
          h=itl.Value().HashCode(INT_MAX);
          itm=newToOld.find(h);
          if (itm!=newToOld.end())
          {
            itm->second.first = 2;
            itm->second.second.push_back(std::make_pair(hss,hs)); //register modified shape
          }
        }
        itm=newToOld.find(hss);
        if (itm!=newToOld.end()&& itm->second.second.size()==0) // find original shape in newShape
            itm->second.second.push_back(std::make_pair(hss,hs)); //register unchanged shape
      }
    }

    //deal with special case of solids
    exp.Init(newShape,TopAbs_SOLID);
    for (;exp.More();exp.Next()) // travel all SOLID sub-shapes of newShape
    {
      h = exp.Current().HashCode(INT_MAX);
      TopExp_Explorer expf(exp.Current(),TopAbs_FACE);
      bool found=false;
      for (;expf.More() && !found ;expf.Next()) // travel all Face of current new solid
      {
        int hf=expf.Current().HashCode(INT_MAX);
        std::pair<number_t,std::list<std::pair<int,int> > >& par=newToOld[hf];
        if (par.first==0) //unchanged face
        {
          int hs=par.second.begin()->second;   //hashcode of parents
          if (hashShape[hs].ShapeType()==TopAbs_SOLID) found=true;
          else if (hashShape[hs].ShapeType()==TopAbs_COMPOUND)
          {
            ShapeExtend_Explorer shexp;
            opencascade::handle<TopTools_HSequenceOfShape> tls=shexp.SeqFromCompound (hashShape[hs],false);
            for (number_t i=0;i<tls->Length();i++)
            {
              if (tls->Value(i+1).ShapeType()==TopAbs_SOLID)  //assume only one solid!
              {
                hs=tls->Value(i+1).HashCode(INT_MAX);
                found=true;
              }
            }
          }
          if (found)
          {
            newToOld[h].first=1;
            newToOld[h].second.push_back(std::make_pair(hs,hs));
          }
        }
      }
    }

    //print the maps
    if (theVerboseLevel>10)
    {
      theCout<<"VIEW HISTORY"<<eol;
      viewHistory(hashShape,hist);
      theCout<<"hashNewShape content"<<eol;
      for (ith=hashNewShape.begin();ith!=hashNewShape.end();++ith)
      {theCout<<ith->first<<" -> ";printInfo(ith->second,theCout);}
      theCout<<"hashShape content"<<eol;
      for (ith=hashShape.begin();ith!=hashShape.end();++ith)
      {theCout<<ith->first<<" -> ";printInfo(ith->second,theCout);}
      theCout<<std::flush;
      std::list<std::pair<int,int> >::iterator it;
      theCout<<"newToOld map content"<<eol;
      for (itm=newToOld.begin();itm!=newToOld.end();++itm)
      {
        number_t s=itm->second.first;
        if (s==0) theCout<<"unchanged ";
        if (s==1) theCout<<"modified ";
        if (s==2) theCout<<"generated ";
        printInfo(hashNewShape[itm->first], theCout);
        std::list<std::pair<int,int> >& shs=itm->second.second;
        for (it=shs.begin();it!=shs.end();++it)
        {
          theCout<<"    "; printInfo(hashShape[it->first],theCout);
          theCout<<"    subshape of ";printInfo(hashShape[it->second],theCout);
        }
      }
    }
  }

  //tools to set labels related to subshapes of same type
  void setLabels(TopoDS_Shape& shape, TDF_Label& root, TopAbs_ShapeEnum tsh, std::map<int ,std::pair<number_t,std::list<std::pair<int,int> > > >& newToOld,
                std::map<int ,TopoDS_Shape>& hashShape, std::map<int ,TopoDS_Shape>& hashNewShape, std::map<int,Handle(TDataStd_NamedData)>& shapeToLabel)
  {
    number_t nnode, tag, i=1;
    real_t hstep;
    string_t name;
    number_t no=1;
    int hc;
    std::map<int ,std::pair<number_t,std::list<std::pair<int,int> > > >::iterator itm;
    std::map<int,Handle(TDataStd_NamedData)>::iterator its;;
    std::list<std::pair<int,int> >::iterator itt;
    std::set<int>hcs;
    TopExp_Explorer exp(shape,tsh);
    for (;exp.More();exp.Next())
    {
        hc=exp.Current().HashCode(INT_MAX);
        if (hcs.find(hc)==hcs.end())  //to avoid duplicated shape
        {
          hcs.insert(hc);
          itm=newToOld.find(hc);
          //theCout<<"no="<<no<<" ";printInfo(exp.Current(),theCout);
          if (itm!=newToOld.end())  //shape found
          {
            TDF_Label shapeL = root.NewChild();
            TNaming_Builder(shapeL).Generated(exp.Current());
            int status = itm->second.first;
            std::list<std::pair<int,int> >& shapes=itm->second.second; // subshapes dependance
            if (status==0) //unchanged shape take same attributes from older
            {
              its=shapeToLabel.find(shapes.begin()->first);
              if (its!=shapeToLabel.end()) //Label found
              {
                Handle(TDataStd_NamedData) data=its->second;
                name=tostring(data->GetString("name"));
                nnode=data->GetInteger("nnode");
                hstep=data->GetReal("hstep");
                tag=data->GetInteger("tag");
                addNamedDataAttribute(shapeL,no,name,tag,nnode,hstep);
              }
            }
            if (status==1) //modified shape, compute new attributes from olders
            {
              if (shapes.size()==1) //only one inherited shape (inherited name)
              {
                int sub =shapes.begin()->first;
                its=shapeToLabel.find(sub);
                if (its!=shapeToLabel.end()) //Label found
                {
                  Handle(TDataStd_NamedData) data=its->second;
                  name=tostring(data->GetString("name"));
                  nnode=data->GetInteger("nnode");
                  if (nnode!=0) // inherited edge, adjust nnode using "length" ratio of newshape and oldshape
                  {
                    TopoDS_Shape& subshape=hashShape[sub];  //old subshape
                    if (shape.ShapeType()==TopAbs_EDGE && subshape.ShapeType()==TopAbs_EDGE) //it should be
                    {
                      real_t l0=length(TopoDS::Edge(subshape)), ln=length(TopoDS::Edge(shape));
                      nnode=std::max(number_t(2),number_t(ln*nnode/l0));
                    }
                  }
                  hstep=data->GetReal("hstep");
                  tag=data->GetInteger("tag");
                  addNamedDataAttribute(shapeL,no,name,tag,nnode,hstep);
                }
              }
              else // more than one ancestors, name and nnodes may be composed
              {
                name=""; hstep=0.; nnode=0; number_t in=0, ih=0;
                for (itt=shapes.begin();itt!=shapes.end();++itt)
                {
                  int sub =itt->first;
                  its=shapeToLabel.find(sub);
                  if (its!=shapeToLabel.end()) //Label found
                  {
                    Handle(TDataStd_NamedData) data=its->second;
                    string_t namei=tostring(data->GetString("name"));
                    number_t nnodei=data->GetInteger("nnode");
                    real_t hstepi=data->GetReal("hstep");
                    if (namei!="")
                    {
                      if (name.find(namei)==string_t::npos)
                      {
                        if (name!="") name+="&";
                        name+=namei;
                      }
                    }
                    if (nnodei!=0) {nnode+=nnodei;in++;}
                    if (hstepi!=0) {hstep+=hstepi;ih++;}
                  }
                }
                if (ih>0) hstep/=ih;
                if (in>0) nnode=std::max(2,int(nnode/in));
                addNamedDataAttribute(shapeL,no,name,0,nnode,hstep);  //tag no longer managed
              }
            }
            if (status==2) //generated shape, compute new attributes from olders, vertex with edge ancestors or edge with face ancestors
            {
              nnode=0; hstep=0; name="";
              if (tsh==TopAbs_VERTEX)
              {
                Point P=toPoint(BRep_Tool::Pnt(TopoDS::Vertex(exp.Current())));
                for (itt=shapes.begin();itt!=shapes.end();++itt)
                {
                  int sub =itt->first;
                  TopoDS_Shape& sh=hashShape[sub];
                  if (sh.ShapeType()==TopAbs_EDGE)
                  {
                    real_t hstep1=0, hstep2=0;
                    TopExp_Explorer expi(sh,TopAbs_VERTEX);
                    its=shapeToLabel.find(expi.Current().HashCode(INT_MAX));
                    Point V1=toPoint(BRep_Tool::Pnt(TopoDS::Vertex(expi.Current()))), V2;
                    if (its!=shapeToLabel.end()) hstep1=its->second->GetReal("hstep");
                    if (expi.More())
                    {
                      expi.Next();
                      its=shapeToLabel.find(expi.Current().HashCode(INT_MAX));
                      V2=toPoint(BRep_Tool::Pnt(TopoDS::Vertex(expi.Current())));
                      if (its!=shapeToLabel.end()) hstep2=its->second->GetReal("hstep");
                    }

                    if (hstep1>0 && hstep2>0) hstep+=(hstep1*norm(V2-P)+hstep2*norm(V1-P))/norm(V1-V2);
                    else hstep+=std::max(hstep1,hstep2);
                  }
              }
              hstep/=shapes.size();
            }
            addNamedDataAttribute(shapeL,no,name,0,nnode,hstep);  //tag no longer managed
          }
        }
        no++;
      }
    }
  }

  void viewHistory(const std::map<int ,TopoDS_Shape>& shapesin, const BRepTools_History& hist)
  {
    std::map<int ,TopoDS_Shape>::const_iterator its;
    for (its=shapesin.begin(); its!=shapesin.end(); ++its)
    {
      const TopoDS_Shape& sh=its->second;
      theCout<<"original ";printInfo(sh,theCout);
      if (hist.IsRemoved(sh)) theCout<<"DELETED "<<eol;
      theCout<<"MODIFIED SHAPE: ";
      TopTools_ListOfShape tls = hist.Modified(sh);
      if (tls.Extent()>0)
      {
        theCout<<tls.Extent()<<eol;
        TopTools_ListIteratorOfListOfShape itl;
        itl.Initialize(tls);
        for (; itl.More(); itl.Next())  printInfo(itl.Value(),theCout);
      }
      else theCout<<" None"<<eol;

      theCout<<"GENERATED SHAPE: ";
      tls = hist.Generated(sh);
      if (tls.Extent()>0)
      {
        theCout<<tls.Extent()<<eol;
        TopTools_ListIteratorOfListOfShape itl;
        itl.Initialize(tls);
        for (; itl.More(); itl.Next())  printInfo(itl.Value(),theCout);
      }
      else theCout<<" None"<<eol;
    }
  }

  //! create TopoDS_Shape related to any loop Geometry
  ShapeDocPair ocLoop(Geometry& geo)
  {
    if (geo.shape()!=_loop) error("free_error"," ocLoop handles only loop geometry");
    std::map<number_t, Geometry*>& gc = geo.components();
    std::map<number_t, std::vector<number_t> >::const_iterator itg=geo.loops().begin();
    std::vector<number_t>::const_iterator itn;
    TopoDS_Shape* tl=nullptr;
    for (; itg!=geo.loops().end(); ++itg)
    {
      for (itn=itg->second.begin(); itn!=itg->second.end(); ++itn)
      {
        Geometry* g=gc[*itn];
        const TopoDS_Shape *tg = &gc[*itn]->ocShape();
        if (tl==nullptr) tl = new TopoDS_Shape(*tg);
        else     *tl = BRepAlgoAPI_Fuse(*tl,*tg);
      }
    }
    if (geo.dim()==2)
    {
      BRepBuilderAPI_MakeWire mw;
      TopExp_Explorer exp(*tl, TopAbs_EDGE);
      for (; exp.More(); exp.Next())
        mw.Add(TopoDS::Edge(exp.Current()));
      BRepBuilderAPI_MakeFace brmf(mw.Wire());
      tl= new TopoDS_Shape(brmf.Face());
    }
    else if (geo.dim()==3)
    {
      BRepBuilderAPI_Sewing sewer;
      TopExp_Explorer exp(*tl, TopAbs_FACE);
      for (; exp.More(); exp.Next())
        sewer.Add(TopoDS::Face(exp.Current()));
      sewer.Perform();
      TopoDS_Shape sewedFaces(sewer.SewedShape());
      BRepBuilderAPI_MakeSolid bs(TopoDS::Shell(sewer.SewedShape()));
      tl = new TopoDS_Shape(bs.Solid());
    }
    else  error("free_error","ocLoop cannot handle 1D geometry");

    //create doc
    TDocStd_Document* ocdoc = new TDocStd_Document("MDTV-Standard");
    TDF_Label root = ocdoc->Main();
    string_t name=geo.domName();
    if (name=="") name = "Omega";
    TDF_Label shapeL = root.NewChild();
    TNaming_Builder(shapeL).Generated(*tl);
    addNamedDataAttribute(shapeL,1,name,0,0,0.);
    number_t no=1;
    std::map<number_t, Geometry*>::iterator itc;
    if (geo.dim()==2)
    {
      std::set<int> hc;
      TopExp_Explorer exp(*tl,TopAbs_EDGE);
      for (;exp.More();exp.Next(),no++)
      {
        std::list<Point> ocv=OCvertices(exp.Current());
        for (itc=gc.begin();itc!=gc.end();++itc)
        {
          if (commonPoints(ocv,itc->second->boundNodes())>1) break;
        }
        if (itc!=gc.end()) // should be a curve
        {
          TDF_Label edgeL = root.NewChild();
          TNaming_Builder(edgeL).Generated(exp.Current());
          number_t nnode=0;
          if (itc->second->withNnodes()) nnode=itc->second->curve()->n(0);
          addNamedDataAttribute(edgeL,no,itc->second->domName(),0,nnode,0.);
        }
      }
      exp.Init(*tl,TopAbs_VERTEX);
      no=1; real_t h=0.,nh=0;
      for (;exp.More();exp.Next())
      {
        int hcc=exp.Current().HashCode(INT_MAX);
        if (hc.find(hcc)==hc.end())
        {
          hc.insert(hcc);
          Point V=toPoint(BRep_Tool::Pnt(TopoDS::Vertex(exp.Current())));
          for (itc=gc.begin();itc!=gc.end();++itc)
          {
            const Curve* cu=itc->second->curve();
            if (cu->h().size()>0)
            {
              if (V==force3D(cu->p1())) {h+=cu->h1();nh++;}
              if (V==force3D(cu->p2())) {h+=cu->h2();nh++;}
            }
          }
          TDF_Label vertexL = root.NewChild();
          TNaming_Builder(vertexL).Generated(exp.Current());
          if (nh>1) h/=nh;
          addNamedDataAttribute(vertexL,no,"",0,0,h);
          no++;
        }
      }
    }

    if (geo.dim()==3)
    {
      std::set<int> hc;
      TopExp_Explorer exp(*tl,TopAbs_FACE);
      for (;exp.More();exp.Next(),no++)
      {
        std::list<Point> ocv=OCvertices(exp.Current());
        for (itc=gc.begin();itc!=gc.end();++itc)
        {
          if (commonPoints(ocv,itc->second->boundNodes())>2) break;
        }
        if (itc!=gc.end()) // should be a face
        {
          TDF_Label faceL = root.NewChild();
          TNaming_Builder(faceL).Generated(exp.Current());
          addNamedDataAttribute(faceL,no,itc->second->domName(),0,0,0.);
        }
      }
      exp.Init(*tl,TopAbs_EDGE);
      no=1;
      hc.clear();
      for (;exp.More();exp.Next())
      {
        int hcc=exp.Current().HashCode(INT_MAX);
        if (hc.find(hcc)==hc.end())
        {
          hc.insert(hcc);
          std::list<Point> ocv=OCvertices(exp.Current());
          number_t nnode=0, ns=0;
          for (itc=gc.begin();itc!=gc.end();++itc)
          {
            if (itc->second->withNnodes())
            {
              std::vector<std::pair<ShapeType,std::vector<const Point*> > > cs=itc->second->curves();
              for (number_t i=0;i<cs.size();i++)
                if (commonPoints(ocv,cs[i].second)==2) {nnode+=itc->second->nnodesPerBorder()[i];ns++;}
            }
          }
          TDF_Label edgeL = root.NewChild();
          TNaming_Builder(edgeL).Generated(exp.Current());
          if (ns>1) nnode/=ns;
          addNamedDataAttribute(edgeL,no,"",0,nnode,0.);
          no++;
        }
      }
      hc.clear();
      no=1;
      exp.Init(*tl,TopAbs_VERTEX);
      for (;exp.More();exp.Next())
      {
        int hcc=exp.Current().HashCode(INT_MAX);
        if (hc.find(hcc)==hc.end())
        {
          real_t h=0.,nh=0;
          hc.insert(hcc);
          Point V=toPoint(BRep_Tool::Pnt(TopoDS::Vertex(exp.Current())));
          for (itc=gc.begin();itc!=gc.end();++itc)
          {
            if (!itc->second->withNnodes())
            {
              std::vector<const Point*> vs=itc->second->boundNodes();
              for (number_t i=0;i<vs.size();i++)
                if (V==*vs[i]) {h+=itc->second->surface()->h()[i];nh++;}
            }
          }
          TDF_Label vertexL = root.NewChild();
          TNaming_Builder(vertexL).Generated(exp.Current());
          if (nh>1) h/=nh;
          addNamedDataAttribute(vertexL,no,"",0,0,h);
          no++;
        }
      }
    }

  return std::make_pair(tl,ocdoc);
  }

  //! create TopoDS_Shape related to any extruded Geometry
  ShapeDocPair ocExtrusion(Geometry& geo)
  {
    ExtrusionData* ext=geo.extrusionData();
    if (ext==nullptr) error("free_error","no extrusion data to do OCExtrusion");
    TopoDS_Shape* tl=nullptr;
    Transformation* tf=ext->extrusion();
    number_t layers=ext->layers();
    const TopoDS_Shape& section = geo.components()[0]->ocShape();
    //saveToBrep(section,"section.brep");
    std::vector<real_t> dir(3);
    dir[2]=1; // z axis
    Point center(0.,0.,0.);
    double_t angle=0.;
    bool is3D=false;
    std::map<int,int> faceNum; //map hashcode of lateral face to edge rank
    switch (tf->transformType())
    {
      case _translation:
      {
        Vector<real_t> v=tf->translation()->u();
        if (layers>1) v/=real_t(layers);
        BRepPrimAPI_MakePrism bpp(section,to_gpVec(v));
        tl= new TopoDS_Shape(bpp.Shape());
        //relate EDGES to FACES from Generated data using hashcode
        std::set<int> hashc;
        number_t ne=0;
        TopExp_Explorer expe(section,TopAbs_EDGE);
        for (;expe.More();expe.Next())
        {
          int he=expe.Current().HashCode(INT_MAX); //edge hash code
          if (hashc.find(he)==hashc.end())
          {
            hashc.insert(he);
            TopTools_ListOfShape tls=bpp.Generated(expe.Current());
            TopTools_ListIteratorOfListOfShape itl;
            itl.Initialize(tls);
            for (; itl.More(); itl.Next()) // travel modified shape related to current sub-shape itt.Value()
            {
              int h=itl.Value().HashCode(INT_MAX);
              if (itl.Value().ShapeType()==TopAbs_FACE) {ne++;faceNum[h]=ne;break;}
            }
          }
        }
        break;
      }
      case _rotation3d:
      {
        dir = tf->rotation3d()->axisDirection();
        center=tf->rotation3d()->axisPoint();
        angle=tf->rotation3d()->angle();
        if (layers>1) angle/=layers;
        is3D=true;
      }
      case _rotation2d:
      {
        if (!is3D)
        {
          center=tf->rotation2d()->center();
          angle=tf->rotation2d()->angle();
        }
        gp_Ax1 axe(to_gpPnt(center),to_gpDir(dir));
        BRepPrimAPI_MakeRevol bpp(section,axe,angle);
        tl= new TopoDS_Shape(bpp.Shape());
        //relate EDGES to FACES from Generated data using hashcode
        std::set<int> hashc;
        number_t ne=0;
        TopExp_Explorer expe(section,TopAbs_EDGE);
        for (;expe.More();expe.Next())
        {
          int he=expe.Current().HashCode(INT_MAX); //edge hash code
          if (hashc.find(he)==hashc.end())
          {
            hashc.insert(he);
            TopTools_ListOfShape tls=bpp.Generated(expe.Current());
            TopTools_ListIteratorOfListOfShape itl;
            itl.Initialize(tls);
            for (; itl.More(); itl.Next()) // travel generated face related to current edge
            {
              int h=itl.Value().HashCode(INT_MAX);
              if (itl.Value().ShapeType()==TopAbs_FACE) {ne++;faceNum[h]=ne;break;}
            }
          }
        }
        break;
      }
      default:
        error("free_error","transformation not handle in ocExtrusion");
        break;
    }

    //create doc
    TDocStd_Document* ocdoc = new TDocStd_Document("MDTV-Standard");
    TDF_Label root = ocdoc->Main();
    string_t name=ext->domName();
    if (name=="") name = "Omega";
    number_t nf=1, ne=1, nv=1;
    std::list<Point> sectionVertices=OCvertices(section);
    TopExp_Explorer exps(*tl,TopAbs_SOLID);
    for (;exps.More();exps.Next())
    {
      const TopoDS_Shape& tds=exps.Current();
      TDF_Label shapeL = root.NewChild();
      TNaming_Builder(shapeL).Generated(tds);
      addNamedDataAttribute(shapeL,1,name,0,0,0.);

      //loop on faces (face side naming following section edge order)
      string_t baseName = "", topName = "", latname="";
      bool hasLatNames=false;
      if (ext->baseDomainNames().size()>0) {baseName=ext->baseDomainNames()[0];topName=baseName;}
      if (ext->baseDomainNames().size()>1)  topName=ext->baseDomainNames()[1];
      number_t nbln=ext->lateralSideNames().size();  //several lateral names
      if (nbln>0) latname=ext->lateralSideNames()[0];
      TopExp_Explorer exp(tds,TopAbs_FACE);
      for (;exp.More();exp.Next(),nf++)
      {
        string_t name="";
        int hf=exp.Current().HashCode(INT_MAX);
        if (faceNum.find(hf)!=faceNum.end())  //lateral faces
        {
          if (nbln==1) name=latname;
          if (nbln>1) name = ext->lateralSideNames()[std::min(faceNum[hf]-1,int(nbln)-1)];
        }
        else //detect base or top
        {
          if (commonPoints(sectionVertices,OCvertices(exp.Current())) >2) name=baseName;
          else name=topName;
        }
        TDF_Label faceL = root.NewChild();
        TNaming_Builder(faceL).Generated(exp.Current());
        addNamedDataAttribute(faceL,nf,name,0,0,0.);
      }
      //loop on edges
      number_t nnode=0, nh=ext->h().size(), nn=ext->n().size();
      real_t hstep=0.;
      if (nh>0) hstep=ext->h()[0]; //use hsteps
      else nnode= std::max(ext->n()[0],number_t(2));
      std::set<int> hashc;
      exp.Init(tds,TopAbs_EDGE);
      for (;exp.More();exp.Next())
      {
        int he=exp.Current().HashCode(INT_MAX);
        if (hashc.find(he)==hashc.end())
        {
          hashc.insert(he);
          if (nn>1) nnode=ext->n()[std::min(ne,nn-1)];
          ne++;
          TDF_Label edgeL = root.NewChild();
          TNaming_Builder(edgeL).Generated(exp.Current());
          addNamedDataAttribute(edgeL,ne,"",0,nnode,0.);
        }
      }
      //loop on vertices
      hashc.clear();
      exp.Init(tds,TopAbs_VERTEX);
      number_t nv=0;
      for (;exp.More();exp.Next())
      {
        int hv=exp.Current().HashCode(INT_MAX);
        if (hashc.find(hv)==hashc.end())
        {
          hashc.insert(hv);
          if (nh>1) hstep=ext->h()[std::min(nv,nh-1)];
          nv++;
          TDF_Label edgeL = root.NewChild();
          TNaming_Builder(edgeL).Generated(exp.Current());
          addNamedDataAttribute(edgeL,nv,"",0,0,hstep);
        }
      }
    }

    return ShapeDocPair(tl,ocdoc);
  }

  // add a name attribute to a label
  const Standard_GUID& addNameAttribute(TDF_Label& lab, const string_t& nam)
  {
    Handle(TDataStd_Name) tdname = new TDataStd_Name();
    tdname->Set(nam.c_str());
    lab.AddAttribute(tdname);
    return tdname->ID();
  }

  // add an integer attribute to a label
  const Standard_GUID& addIntegerAttribute(TDF_Label& lab, int_t i)
  {
    Handle(TDataStd_Integer) tdint = new TDataStd_Integer();
    tdint->Set(i);
    lab.AddAttribute(tdint);
    return tdint->ID();
  }

  // add a real attribute to a label
  const Standard_GUID& addIntegerAttribute(TDF_Label& lab, real_t r)
  {
    Handle(TDataStd_Real) tdr = new TDataStd_Real();
    tdr->Set(r);
    lab.AddAttribute(tdr);
    return tdr->ID();
  }

  //!  add a named data attribute to TDF_Label
  //      lab: current label where to link data atributes
  //      num: OC solid/face/edge/vertex number
  //      name: xlife name of the shape (may be empty)
  // tag: xlife solid/face/edge/vertex number (may be 0)
  //      nnode: xlife nnode property for edge (else 0)
  //      hstep: xlife hstep property for vertex (else 0)
  const Standard_GUID& addNamedDataAttribute(TDF_Label& lab, int_t num, const string_t& name, int_t tag, int_t nnode, real_t hstep)
  {
    Handle(TDataStd_NamedData) tdata = new TDataStd_NamedData();
    tdata->SetInteger("num",num);
    tdata->SetString("name",name.c_str());
    tdata->SetInteger("tag",tag);
    tdata->SetInteger("nnode",nnode);
    tdata->SetReal("hstep",hstep);
    lab.AddAttribute(tdata);
    return tdata->ID();
  }

  //! save TopoDS_Shape to STEP file
  void saveToStep(const TopoDS_Shape& tds, const string_t& fn)
  {
    if (tds.IsNull()) return;
    Interface_Static::SetIVal("write.step.assembly",2);
    STEPControl_Writer writer;
    IFSelect_ReturnStatus stat = writer.Transfer(tds,STEPControl_AsIs);
    if (stat==IFSelect_RetDone) stat = writer.Write(fn.c_str());
    if (stat!=IFSelect_RetDone)
      error("free_error"," failed to export TopoDS_Shape to STEP file");
  }

  //! save TopoDS_Shape to BREP file
  void saveToBrep(const TopoDS_Shape& tds, const string_t& fn)
  {
    if (tds.IsNull()) return;
    if (!BRepTools::Write(tds,fn.c_str()))
      error("free_error"," failed to export TopoDS_Shape to BREP file");
    // changeBrepHeader(fn);
  }

  //! change header in BREP file
  //  replace new header : 'CASCADE Topology V3, (c) Open Cascade' by old one : 'CASCADE Topology V1, (c) Matra-Datavision'
  //  because new header is not compliant with gmsh !!!
  void changeBrepHeader(const string_t& fn)
  {
      std::fstream iost(fn,std::ios::in);
      if(!iost.is_open())
        error("free_error"," failed to open "+fn);
      string_t line,
              newheader = "CASCADE Topology V3, (c) Open Cascade",
              oldheader = "CASCADE Topology V1, (c) Matra-Datavision";
      std::list<string_t> lines;
      string_t::size_type pos = 0;
      while(std::getline(iost, line))
      {
        if(line==newheader) line=oldheader;
        lines.push_back(line);
      }
    iost.close();
    iost.open(fn, std::ios::out | std::ios::trunc);
    for(auto it=lines.begin(); it!=lines.end(); ++it)
        iost<<(*it)<<eol;
    iost.close();
  }

  //! load STEP file to TopoDS_SHAPE
  TopoDS_Shape loadFromStep(const string_t& fn)
  {
    STEPControl_Reader reader;
    IFSelect_ReturnStatus stat = reader.ReadFile(fn.c_str());
    if (stat!=IFSelect_RetDone) error("free_error","loadSTEP fails to read "+fn);
    reader.NbRootsForTransfer();
    reader.TransferRoots();
    return reader.OneShape();
  }

  //!< brief description of a TopoDS_shape
  string_t asString(const TopoDS_Shape& ts)
  {
    string_t s="TopoDS_shape: ";
    if (ts.IsNull())
      return s+="empty";
    s+=asString(ts.ShapeType())+" ";
    s+=tostring(ts.NbChildren())+" child(s)";
    s+=" hash="+tostring(ts.HashCode(INT_MAX));
    return s;
  }

  string_t asString(TopAbs_ShapeEnum ts)
  {
    switch (ts)
    {
      case TopAbs_SHAPE:
        return string_t("SHAPE");
      case TopAbs_COMPOUND:
        return string_t("COMPOUND");
      case TopAbs_COMPSOLID:
        return string_t("COMPSOLID");
      case TopAbs_SOLID:
        return string_t("SOLID");
      case TopAbs_SHELL:
        return string_t("SHELL");
      case TopAbs_FACE:
        return string_t("FACE");
      case TopAbs_WIRE:
        return string_t("WIRE");
      case TopAbs_EDGE:
        return string_t("EDGE");
      case TopAbs_VERTEX:
        return string_t("VERTEX");
      default:
        break;
    }
    return string_t("?");
  }

  void printInfo(const TopoDS_Shape& tds, std::ostream& out, number_t maxv)
  {
    TopAbs_ShapeEnum  sh=tds.ShapeType();
    if (sh==TopAbs_COMPOUND)
    {
      out<<"COMPOUND ("<<tds.HashCode(INT_MAX)<<"), ";
      ShapeExtend_Explorer shexp;
      opencascade::handle<TopTools_HSequenceOfShape> tls=shexp.SeqFromCompound (tds,false);
      out<<tls->Length()<<" components:";
      for (number_t i=0;i<tls->Length();i++)
          printInfo(tls->Value(i+1),out);
      return;
    }
    switch (sh)
    {
      case TopAbs_SOLID: out<<"SOLID ("<<tds.HashCode(INT_MAX)<<")"; break;
      case TopAbs_COMPSOLID: out<<"COMPSOLID ("<<tds.HashCode(INT_MAX)<<")"; break;
      case TopAbs_SHELL: out<<"SHELL ("<<tds.HashCode(INT_MAX)<<")"; break;
      case TopAbs_FACE: out<<"FACE ("<<tds.HashCode(INT_MAX)<<") "<<asString(faceType(TopoDS::Face(tds))); break;
      case TopAbs_WIRE: out<<"WIRE ("<<tds.HashCode(INT_MAX)<<")"; break;
      case TopAbs_EDGE: out<<"EDGE ("<<tds.HashCode(INT_MAX)<<") "<<asString(edgeType(TopoDS::Edge(tds))); break;
      case TopAbs_VERTEX: out<<"VERTEX ("<<tostring(tds.HashCode(INT_MAX))<<")"; break;
      default: break;
    }
    out<<", vertices: ";
    std::list<Point> vs=OCvertices(tds);
    std::list<Point>::iterator itl=vs.begin();
    number_t i=0;
    for (;itl!=vs.end() && i < maxv; ++itl, i++)  out<<(*itl)<<" ";
    if (vs.size()>maxv) out<<"...";
  }

  void printInfo(const TopoDS_Shape& tds, CoutStream& out, number_t maxv)
  {
    printInfo(tds,std::cout,maxv);
    printInfo(tds,out.printStream->currentStream(),maxv);
  }

  void print(const TopoDS_Shape& tds, std::ostream& out, bool recursive)
  {
    TopExp_Explorer exp;
    bool done=false;
    number_t lev=0;
    for (exp.Init(tds, TopAbs_SOLID); exp.More(); exp.Next())
    {
      print(TopoDS::Solid(exp.Current()),out,lev,recursive);
      done=true;
    }
    if (!done)
      for (exp.Init(tds, TopAbs_FACE); exp.More(); exp.Next())
      {
        print(TopoDS::Face(exp.Current()),out,lev,recursive);
        done=true;
      }
    if (!done)
      for (exp.Init(tds, TopAbs_EDGE); exp.More(); exp.Next())
      {
        print(TopoDS::Edge(exp.Current()),out,lev,recursive);
        done=true;
      }
    if (!done)
      for (exp.Init(tds, TopAbs_VERTEX); exp.More(); exp.Next())
        print(TopoDS::Vertex(exp.Current()),out,lev);
  }

  void print(const TopoDS_Solid& solid,std::ostream& out, number_t lev, bool recursive)
  {
    for (number_t i=0; i<lev; i++)  out<<"|  ";
    out<<"solid "<<eol;
    //out<<"solid "<<solid2Tag.Find(solid)<<eol;
    TopExp_Explorer exp;
    for (exp.Init(solid, TopAbs_FACE); exp.More(); exp.Next())
      print(TopoDS::Face(exp.Current()),out,lev+1,recursive);
    for (exp.Init(solid, TopAbs_SHELL); exp.More(); exp.Next())
      print(TopoDS::Shell(exp.Current()),out,lev+1,recursive);
  }

  void print(const TopoDS_Face& face,std::ostream& out, number_t lev, bool recursive)
  {
    for (number_t i=0; i<lev; i++) out<<"|  ";
    out<<"face "<<asString(faceType(face))<<" "<<eol;
    //out<<"face "<<face2Tag.Find(face)<<eol;
    TopExp_Explorer exp;
    for (exp.Init(face, TopAbs_WIRE); exp.More(); exp.Next())
      print(TopoDS::Wire(exp.Current()),out,lev+1,recursive);
    for (exp.Init(face, TopAbs_EDGE); exp.More(); exp.Next())
      print(TopoDS::Edge(exp.Current()),out,lev+1,recursive);
  }

  void print(const TopoDS_Shell& shell,std::ostream& out, number_t lev, bool recursive)
  {
    for (number_t i=0; i<lev; i++) out<<"|  ";
    out<<"shell "<<eol;
    //out<<"shell "<<shell2Tag.Find(shell)<<eol;
    TopExp_Explorer exp;
    for (exp.Init(shell, TopAbs_FACE); exp.More(); exp.Next())
      print(TopoDS::Face(exp.Current()),out,lev+1,recursive);
  }

  void print(const TopoDS_Wire& wire,std::ostream& out, number_t lev, bool recursive)
  {
    for (number_t i=0; i<lev; i++) out<<"|  ";
    out<<"wire "<<eol;
    //out<<"wire "<<wire2Tag.Find(wire)<<eol;
    TopExp_Explorer exp;
    for (exp.Init(wire, TopAbs_EDGE); exp.More(); exp.Next())
      print(TopoDS::Edge(exp.Current()),out,lev+1,recursive);
  }

  void print(const TopoDS_Edge& edge,std::ostream& out, number_t lev, bool recursive)
  {
    for (number_t i=0; i<lev; i++) out<<"|  ";
    out<<"edge "<<asString(edgeType(edge))<<" "<<eol;
    //out<<"edge "<<edge2Tag.Find(edge)<<eol;
    TopExp_Explorer exp;
    for (exp.Init(edge, TopAbs_VERTEX); exp.More(); exp.Next())
      print(TopoDS::Vertex(exp.Current()),out,lev+1);
  }

  void print(const TopoDS_Vertex& vertex,std::ostream& out, number_t lev)
  {
    for (number_t i=0; i<lev; i++) out<<"|  ";
    out<<"vertex ";
    //out<<"vertex "<<vertex2Tag.Find(vertex)<<" ";
    out<<toPoint(BRep_Tool::Pnt(vertex))<<eol;
  }

  void print(const TopoDS_Shape& shape,CoutStream& out, bool recursive)
  {
    print(shape, std::cout, recursive);
    print(shape, out.printStream->currentStream());
  }


  GeomAbs_CurveType edgeType(const TopoDS_Edge& e)
  {
    if (e.IsNull()) return GeomAbs_OtherCurve; // null curve
    return BRepAdaptor_Curve(e).GetType();
  }

  string_t asString(GeomAbs_CurveType type)
  {
    switch (type)
    {
      case GeomAbs_Line: return "line";
      case GeomAbs_Circle: return "circle";
      case GeomAbs_Ellipse: return "ellipse";
      case GeomAbs_Hyperbola: return "hyperbola";
      case GeomAbs_Parabola: return "parabola";
      case GeomAbs_BezierCurve: return "Bezier";
      case GeomAbs_BSplineCurve: return "Bspline";
      case GeomAbs_OtherCurve:
      default: break;
    }
    return "other or void";
  }

  GeomAbs_SurfaceType faceType(const TopoDS_Face& f)
  {
    if (f.IsNull()) return GeomAbs_OtherSurface; // null surface
    return BRepAdaptor_Surface(f).GetType();
  }

  string_t asString(GeomAbs_SurfaceType type)
  {
    switch (type)
    {
      case GeomAbs_Plane: return "plane";
      case GeomAbs_Cylinder: return "cylinder";
      case GeomAbs_Cone: return "cone";
      case GeomAbs_Sphere: return "sphere";
      case GeomAbs_Torus: return "torus";
      case GeomAbs_BezierSurface: return "Bezier surface";
      case GeomAbs_BSplineSurface: return "Bspline surface";
      case GeomAbs_SurfaceOfRevolution: return "revolution surface";
      case GeomAbs_SurfaceOfExtrusion: return "extruded surface";
      case GeomAbs_OffsetSurface: return "offset surface";
      case GeomAbs_OtherSurface:
      default: break;
    }
    return "other or void";
  }

  TopAbs_ShapeEnum toTopAbsShapeEnum(OCShapeType oct)
  {
    switch (oct)
    {
      case _OCSolid: return TopAbs_SOLID;
      case _OCShell:  return TopAbs_SHELL;
      case _OCFace:   return TopAbs_FACE;
      case _OCWire:   return TopAbs_WIRE;
      case _OCEdge:   return TopAbs_EDGE;
      case _OCVertex: return TopAbs_VERTEX;
      case _undefOCShape:
      default: break;
    }
    return TopAbs_SHAPE;
  }

  //cleaned OC vertices list of a an OC shape (do not duplicate vertices close to tol)
  std::list<Point> OCvertices(const TopoDS_Shape& sh, real_t tol)
  {
    std::list<Point> vs;
    std::list<Point>::iterator itv;
    TopExp_Explorer exp(sh,TopAbs_VERTEX); // explore vertices
    for (; exp.More(); exp.Next())
    {
      Point V=toPoint(BRep_Tool::Pnt(TopoDS::Vertex(exp.Current())));
      for (itv=vs.begin(); itv!=vs.end(); ++itv)
        if (itv->distance(V) < tol) break;
      if (itv==vs.end()) vs.push_back(V);
    }
    return vs;
  }


  real_t length(const TopoDS_Edge & e)
  {
    std::list<Point> vs=OCvertices(e);
    if (vs.size()<2) return 0.;
    std::list<Point>::iterator it=vs.begin();
    Point & v1 = *it; it++;
    return v1.distance(*it);
  }

  // give number of common points of 2 lists of points
  number_t commonPoints(const std::list<Point>& v, const std::vector<const Point*>& pv, real_t tol)
  {
    number_t n=0;
    if (v.size()==0 || pv.size()==0)
      return n;   //nothing to do
    std::list<Point>::const_iterator itv = v.begin();
    if (itv->dim() == pv[0]->dim())  //same point dimension
    {
      for (; itv!=v.end(); ++itv)
      {
        for (std::vector<const Point*>::const_iterator itp=pv.begin(); itp!= pv.end() ; ++itp)
        {
          //theCout<<"itv="<<(*itv)<<" itp="<<(**itp)<<eol<<std::flush;
          if (itv->distance(**itp)<tol) {
            n++;
            break;
          }
        }
      }
      return n;
    }
    // not same point dimension
    for (; itv!=v.end(); ++itv)
    {
      for (std::vector<const Point*>::const_iterator itp=pv.begin(); itp!= pv.end() ; ++itp)
      {
        //theCout<<"itv="<<(*itv)<<" itp="<<(**itp)<<eol<<std::flush;
        if (*itv==force3D(**itp)) {
          n++;
          break;
        }
      }
    }
    return n;
  }

  //!< number of common points between two lists of points
  number_t commonPoints(const std::list<Point>&pts1, const std::list<Point>&pts2, real_t tol)
  {
    std::vector<const Point*> hpts(pts2.size());
    std::list<Point>::const_iterator itl;
    std::vector<const Point*>::iterator ith=hpts.begin();
    for (itl=pts2.begin();itl!=pts2.end();++itl, ++ith) *ith = &(*itl);
    return commonPoints(pts1,hpts,tol);
  }

  // return the number of nodes if nnodes are all the same else return 0
  number_t sameNnodes(const std::vector<number_t>& nnodes)
  {
    number_t n0=0;
    if (nnodes.size()>0)
    {
      n0=nnodes[0];
      for (number_t k=1; k<nnodes.size(); k++)
        if (nnodes[k]!=n0) return 0;
    }
    return n0;
  }

  // return the common hstep if hsteps are all the same else return 0
  real_t sameHsteps(const std::vector<real_t>& hs)
  {
    real_t h0=0;
    if (hs.size()>0)
    {
      h0=hs[0];
      for (number_t k=1; k<hs.size(); k++)
        if (hs[k]!=h0)
          return 0.;
    }
    return h0;
  }

  //! writing a 2D/3D geometry using OC data in a geo file (create a step file)
  void saveToBrepGeo(Geometry& g, MeshData& meshData, ShapeType sh, number_t order, MeshPattern pattern, StructuredMeshSplitRule splitDirection, const string_t& geofile, const string_t& brepfile)
  {
    saveToBrep(g.ocShape(), brepfile); //!< save TopoDS_Shape to BREP file
    //create geo and add
    bool buildSurf=true, buildVol=true;
    if (sh==_segment) {buildSurf=false;buildVol=false;}
    if (sh==_triangle || sh==_quadrangle) buildVol=false;
    std::ofstream fout(geofile.c_str());
    if (isTestMode)fout.precision(testPrec);
    else fout.precision(fullPrec);
    fout<<"Merge \""<<brepfile<<"\";"<<eol;
    fout << "Mesh.MeshSizeMin = " << meshData.hmin() << ";" << eol;
    fout << "Mesh.MeshSizeMax = " << meshData.hmax() << ";" << eol;
    // travel doc of ocData
    if (g.hasDoc())
    {
      std::map<string_t,std::list<number_t> > volumeName;
      std::map<string_t,std::list<number_t> > surfaceName;
      std::map<string_t,std::list<number_t> > curveName;
      std::map<string_t,std::list<number_t> > pointName;
      std::map<string_t,std::list<number_t> >::iterator it;
      Handle(TNaming_NamedShape) NS;
      Handle(TDataStd_NamedData) ND;
      for (TDF_ChildIterator itl(g.ocData().OCDoc().Main()); itl.More(); itl.Next())
      {
        TDF_Label l=itl.Value();
        if (l.FindAttribute(TNaming_NamedShape::GetID(),NS))
        {
          TopAbs_ShapeEnum sh=NS->Get().ShapeType();
          if (l.FindAttribute(TDataStd_NamedData::GetID(),ND))
          {
            int_t n = ND->GetInteger("num");
            const TCollection_ExtendedString & na = ND->GetString("name");
            if (!na.IsEmpty())
            {
              string_t sa=tostring(na);
              switch (sh)
              {
                case TopAbs_VERTEX:
                {
                  it=pointName.find(sa);
                  if (it==pointName.end())
                    pointName[sa]=std::list<number_t>(1,n);
                  else
                    it->second.push_back(n);
                }
                break;
                case TopAbs_EDGE:
                {
                  it=curveName.find(sa);
                  if (it==curveName.end())
                    curveName[sa]=std::list<number_t>(1,n);
                  else
                    it->second.push_back(n);
                }
                break;
                case TopAbs_FACE:
                {
                  it=surfaceName.find(sa);
                  if (it==surfaceName.end())
                    surfaceName[sa]=std::list<number_t>(1,n);
                  else
                    it->second.push_back(n);
                }
                break;
                case TopAbs_SOLID:
                {
                  it=volumeName.find(sa);
                  if (it==volumeName.end())
                    volumeName[sa]=std::list<number_t>(1,n);
                  else
                    it->second.push_back(n);
                }
                break;
                default:
                  break;
              }
            }
            real_t h=ND->GetReal("hstep");
            number_t no=ND->GetInteger("nnode");
            if (h!=0) // vertex characteristic length
            {
              if (sh==TopAbs_VERTEX)
                fout<<"Characteristic Length{"<<n<<"} = "<<h<<";"<<eol;
            }
            else if (no!=0 && sh==TopAbs_EDGE)
              fout<<"Transfinite Line {"<<n<<"} = "<<no<<";"<<eol;
          }
        }
      }
      // write Physical attributes
      for (it=pointName.begin(); it!=pointName.end(); ++it)
      {
        std::list<number_t>::iterator il=it->second.begin(),  ile=it->second.end();
        fout<<"Physical Point(\""<<it->first<<"\") = {"<<*il;
        ++il;
        for (; il!=ile; ++il)
          fout<<","<<*il;
        fout<<"};"<<eol;
      }
      for (it=curveName.begin(); it!=curveName.end(); ++it)
      {
        std::list<number_t>::iterator il=it->second.begin(),  ile=it->second.end();
        fout<<"Physical Line(\""<<it->first<<"\") = {"<<*il;
        ++il;
        for (; il!=ile; ++il)
          fout<<","<<*il;
        fout<<"};"<<eol;
      }
      if (buildSurf)
      {
        for (it=surfaceName.begin(); it!=surfaceName.end(); ++it)
        {
          std::list<number_t>::iterator il=it->second.begin(),  ile=it->second.end();
          fout<<"Physical Surface(\""<<it->first<<"\") = {"<<*il;
          ++il;
          for (; il!=ile; ++il)  fout<<","<<*il;
          fout<<"};"<<eol;
        }
      }
      if (buildVol)
      {
        for (it=volumeName.begin(); it!=volumeName.end(); ++it)
        {
          std::list<number_t>::iterator il=it->second.begin(),  ile=it->second.end();
          fout<<"Physical Volume(\""<<it->first<<"\") = {"<<*il;
          ++il;
          for (; il!=ile; ++il) fout<<","<<*il;
          fout<<"};"<<eol;
        }
      }
    }
    fout.close();
  }

  //==========================================================================================================
  //                    Geom_ParametrizedCurve member functions
  //==========================================================================================================

  void Geom_ParametrizedCurve::Reverse()
  {
    error("not_yet_implemented","Geom_ParametrizedCurve::Reverse");
  }

  Standard_Real Geom_ParametrizedCurve::ReversedParameter(const Standard_Real U) const
  {
    error("not_yet_implemented","Geom_ParametrizedCurve::ReversedParameter");
    return 0.;
  }

  Standard_Boolean Geom_ParametrizedCurve::IsCN(const Standard_Integer n) const
  {
    ContinuityOrder co=parc_->parametrization().contOrder;
    switch (co)
    {
      case _regC0:
      case _regG1: return n<1;
      case _regC1:
      case _regG2: return n<2;
      case _regC2: return n<3;
      case _regC3: return n<4;
      case _regCinf: return true;
      default: break;
    }
    return false;
  }

  Standard_Boolean Geom_ParametrizedCurve::IsClosed() const
  {
    return parc_->isClosed();
  }

  Standard_Boolean Geom_ParametrizedCurve::IsPeriodic() const
  {
    return parc_->parametrization().periods[0]!=0;
  }

  GeomAbs_Shape Geom_ParametrizedCurve::Continuity() const
  {
    ContinuityOrder co=parc_->parametrization().contOrder;
    switch (co)
    {
      case _regC0: return GeomAbs_C0 ;
      case _regG1: return GeomAbs_G1 ;
      case _regC1: return GeomAbs_C1 ;
      case _regG2: return GeomAbs_G2 ;
      case _regC2: return GeomAbs_C2 ;
      case _regC3: return GeomAbs_C3 ;
      case _regCinf:return GeomAbs_CN ;
      default: break;
    }
    return GeomAbs_C0;  // no equivalence with _notRegular
  }

  void Geom_ParametrizedCurve::D0(const Standard_Real U, gp_Pnt& P) const
  {
    if (U<-theTolerance || U>1+theTolerance) error("free_error","parameter out of bounds in Geom_ParametrizedCurve::D0");
    Point pt=parc_->parametrization()(U);
    P=to_gpPnt(pt);
  }

  void Geom_ParametrizedCurve::D1(const Standard_Real U, gp_Pnt& P, gp_Vec& V1) const
  {
    if (U<-theTolerance || U>1+theTolerance) error("free_error","parameter out of bounds in Geom_ParametrizedCurve::D1");
    Point pt=parc_->parametrization()(U,_id);
    P=to_gpPnt(pt);
    Point dp=parc_->parametrization()(U,_dt);
    V1=to_gpVec(dp);
  }

  void Geom_ParametrizedCurve::D2(const Standard_Real U, gp_Pnt& P, gp_Vec& V1, gp_Vec& V2) const
  {
    if (U<-theTolerance || U>1+theTolerance) error("free_error","parameter out of bounds in Geom_ParametrizedCurve::D2");
    Point pt=parc_->parametrization()(U,_id);
    P=to_gpPnt(pt);
    Point dp=parc_->parametrization()(U,_dt);
    V1=to_gpVec(dp);
    Point d2p=parc_->parametrization()(U,_dt2);
    V2=to_gpVec(d2p);
  }

  void Geom_ParametrizedCurve::D3(const Standard_Real U, gp_Pnt& P, gp_Vec& V1, gp_Vec& V2, gp_Vec& V3) const
  {
    if (U<-theTolerance || U>1+theTolerance) error("free_error","parameter out of bounds in Geom_ParametrizedCurve::D3");
    Point pt=parc_->parametrization()(U,_id);
    P=to_gpPnt(pt);
    Point dp=parc_->parametrization()(U,_dt);
    V1=to_gpVec(dp);
    Point d2p=parc_->parametrization()(U,_dt2);
    V2=to_gpVec(d2p);
    Point d3p=parc_->parametrization()(U,_dt3);
    V3=to_gpVec(d3p);
  }

  gp_Vec Geom_ParametrizedCurve::DN(const Standard_Real U, const Standard_Integer N) const
  {
    if (U<-theTolerance || U>1+theTolerance) error("free_error","parameter out of bounds in Geom_ParametrizedCurve::DN");
    switch (N)
    {
      case 0: return to_gpVec(parc_->parametrization()(U,_id));
      case 1: return to_gpVec(parc_->parametrization()(U,_dt));
      case 2: return to_gpVec(parc_->parametrization()(U,_dt2));
      case 3: return to_gpVec(parc_->parametrization()(U,_dt3));
      default: break;
    }
    error("free_error","in Geom_ParametrizedCurve::DN, N must be 0,1,2,3 ");
    return gp_Vec();
  }

  gp_Pnt Geom_ParametrizedCurve::StartPoint() const
  {
    return to_gpPnt(parc_->p1());
  }

  gp_Pnt Geom_ParametrizedCurve::EndPoint() const
  {
    return to_gpPnt(parc_->p2());
  }

  Standard_Real Geom_ParametrizedCurve::FirstParameter() const
  {
    return 0.;
  }

  Standard_Real Geom_ParametrizedCurve::LastParameter() const
  {
    return 1.;
  }

  void Geom_ParametrizedCurve::Transform(const gp_Trsf& T)
  {
    error("not_yet_implemented","Geom_ParametrizedCurve::Transform");
  }

  Handle(Geom_Geometry) Geom_ParametrizedCurve::Copy() const
  {
    Handle(Geom_ParametrizedCurve) C=new Geom_ParametrizedCurve(parc_);  //shared pointer
    return C;
  }

  }  //end of namespace xlifepp
#endif // XLIFEPP_WITH_OPENCASCADE
