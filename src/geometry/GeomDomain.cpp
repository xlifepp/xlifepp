/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomDomain.cpp
  \author E. Lunéville
  \since 04 apr 2012
  \date 24 sept 2012

  \brief Implementation of xlifepp::GeomDomain classes and functionalities
*/

#include "GeomDomain.hpp"
#include "DomainMap.hpp"
#include "Mesh.hpp"

namespace xlifepp
{

std::ostream& operator<<(std::ostream& out, const DomainType& dt)
{ out << words("domain type", dt); return out; }
std::ostream& operator<<(std::ostream& out, const SetOperationType& sot)
{ out << words("setop", sot); return out; }

//---------------------------------------------------------------------------
// GeomDomain member functions and related external functions
//---------------------------------------------------------------------------
//default and basic constructor (for internal use)
GeomDomain::GeomDomain(const string_t& na, dimen_t d, const Mesh* m, Geometry* g)
{
  domainInfo_p = new DomainInfo(na, d, _undefDomain, m);
  domain_p = this;
  up_p=nullptr;
  geometry_p=g;
  //not inserted to the list of GeomDomains
}

//pseudo constructor of a meshdomain
GeomDomain::GeomDomain(const Mesh& me, const string_t& na, dimen_t d, const string_t& desc, Geometry* g, bool ts)
{
  domain_p = static_cast<GeomDomain*>(new MeshDomain(me, na, d, desc));
  up_p=nullptr;
  domain_p->up_p = this;
  geometry_p=g;
  domainInfo_p = domain_p->domainInfo_p;
  domainInfo_p->toSave = ts;
  theDomains.push_back(domain_p);
}

//pseudo constructor of a composite domain from two domains
GeomDomain::GeomDomain(SetOperationType sot, const GeomDomain& dom1, const GeomDomain& dom2, const string_t& na)
{
  std::vector<const GeomDomain*> doms(2, &dom1);
  doms[1] = &dom2;
  domain_p = static_cast<GeomDomain*>(new CompositeDomain(sot, doms, na));
  up_p=nullptr;
  geometry_p=nullptr;
  domain_p->up_p = this;
  domainInfo_p = domain_p->domainInfo_p;
  theDomains.push_back(domain_p);
}

//pseudo constructor of a composite domain from n domains
//composite domain is no more than the list of domains
GeomDomain::GeomDomain(SetOperationType sot, const std::vector<const GeomDomain*>& doms, const string_t& na)
{
  domain_p = static_cast<GeomDomain*>(new CompositeDomain(sot, doms, na));
  up_p=nullptr;
  geometry_p=nullptr;
  domain_p->up_p = this;
  domainInfo_p = domain_p->domainInfo_p;
  theDomains.push_back(domain_p);
}

//pseudo constructor of a points domain
GeomDomain::GeomDomain(const std::vector<Point>& pts, const string_t& na)
{
  domain_p = static_cast<GeomDomain*>(new PointsDomain(pts, na));
  up_p=nullptr;
  geometry_p=nullptr;
  domain_p->up_p = this;
  domainInfo_p = domain_p->domainInfo_p;
  theDomains.push_back(domain_p);
}

//destructor
GeomDomain::~GeomDomain()
{
  if (domain_p!=0 && domain_p != this)  //delete child
  {
    delete domain_p;
    delete domainInfo_p;
    return;
  }
  //update domainMap
  DomainMap::removeMaps(*this);
  //it is a child, remove it from the list of GeomDomains
  std::vector<const GeomDomain*>::iterator itd=find(theDomains.begin(), theDomains.end(), this);
  if (itd!=theDomains.end()) theDomains.erase(itd);
}

//implementation of Parameter constructors from GeomDomain here to avoid cross dependancy
Parameter::Parameter(const GeomDomain& dom, const string_t& nm, const string_t& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pointerGeomDomain)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  GeomDomain* domb=const_cast<GeomDomain*>(&dom);
  p_ = static_cast<void *>(domb);
}

Parameter::Parameter(const GeomDomain& dom, const string_t& nm, const Strings& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pointerGeomDomain)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  GeomDomain* domb=const_cast<GeomDomain*>(&dom);
  p_ = static_cast<void *>(domb);
}

Parameter& Parameter::operator=(const GeomDomain& dom)
{
  if (p_!=nullptr) deletePointer();
  GeomDomain* domb=const_cast<GeomDomain*>(&dom);
  p_ = static_cast<void *>(domb);
  type_=_pointerGeomDomain;
  return *this;
}

void deleteGeomDomain(void *p)
{
  p=nullptr;
}

void* cloneGeomDomain(const void* p)
{
  return const_cast<void *>(p);
}

// delete all domain objects
void GeomDomain::clearGlobalVector()
{
  while (GeomDomain::theDomains.size() > 0) { delete GeomDomain::theDomains[0]; }
}

void GeomDomain::buildPartitionData(const std::vector<Parameter>& ps)
{
  this->meshDomain()->buildPartitionData(ps);
}

//acces to MeshDomain child
const MeshDomain* GeomDomain::meshDomain() const
{
  if (domain_p != this) { return domain_p->meshDomain(); } // call with the true domain
  error("domain_notmesh", name(), words("domain type", domType()));
  return nullptr;
}
MeshDomain* GeomDomain::meshDomain()
{
  if (domain_p != this) { return domain_p->meshDomain(); } // call with the true domain
  error("domain_notmesh", name(), words("domain type", domType()));
  return nullptr;
}

//acces to MeshDomainPartition child
const MeshDomainPartition* GeomDomain::meshDomainPartition() const
{
  if (domain_p != this) { return domain_p->meshDomainPartition(); } // call with the true domain}
  error("domain_notmeshpartition", name(), words("domain type", domType()));
  return nullptr;
}
MeshDomainPartition* GeomDomain::meshDomainPartition()
{
  if (domain_p != this) { return domain_p->meshDomainPartition(); } // call with the true domain
  error("domain_notmeshpartition", name(), words("domain type", domType()));
  return nullptr;
}

//acces to CompositeDomain child
const CompositeDomain* GeomDomain::compositeDomain() const
{
  if (domain_p != this) { return domain_p->compositeDomain(); } // call with the true domain
  error("domain_notcomposite", name(), words("domain type", domType()));
  return nullptr;
}
CompositeDomain* GeomDomain::compositeDomain()
{
  if (domain_p != this) { return domain_p->compositeDomain(); } // call with the true domain
  error("domain_notcomposite", name(), words("domain type", domType()));
  return nullptr;
}

//acces to AnalyticalDomain child
const AnalyticalDomain* GeomDomain::analyticalDomain() const
{
  if (domain_p != this) { return domain_p->analyticalDomain(); } // call with the true domain
  error("domain_notanalytical", name(), words("domain type", domType()));
  return nullptr;
}
AnalyticalDomain* GeomDomain::analyticalDomain()
{
  if (domain_p != this) { return domain_p->analyticalDomain(); } //call with the true domain
  error("domain_notanalytical", name(), words("domain type", domType()));
  return nullptr;
}

// return the space dimension, say dim of points (const)
dimen_t GeomDomain::spaceDim() const
{
  if (domainInfo_p->mesh_p!=nullptr)
  {
    dimen_t d = domainInfo_p->mesh_p->spaceDim();   //may be 0 if void mesh
    if (d==0) { d = domainInfo_p->dim; }
    return d;
  }
  return 0;
}

//test the type of composite domain
bool GeomDomain::isUnion() const
{
  if (domain_p != this) { return domain_p->isUnion(); }
  return false;
}

bool GeomDomain::isIntersection() const
{
  if (domain_p != this) { return domain_p->isIntersection(); }
  return false;
}

// true if the current domain includes a given domain (const)
bool GeomDomain::include(const GeomDomain& d) const
{
  if (domain_p != this) { return domain_p->include(d); }
  return false;
}

// true if a side domain
bool GeomDomain::isSideDomain() const
{
  if (domain_p != this) { return domain_p->isSideDomain(); }
  return false;
}

// true if a set of element sides
bool GeomDomain::isSidesDomain() const
{
  if (domain_p != this) { return domain_p->isSidesDomain(); }
  return false;
}

// true if a side interface
bool GeomDomain::isInterface() const
{
  if (domain_p != this) { return domain_p->isInterface(); }
  return false;
}

//number of geometric elements of domain
number_t GeomDomain::numberOfElements() const
{
  if (domain_p != this) { return domain_p->numberOfElements(); } // call with the true domain
  return 0;
}

//access to k-th element (k>=1), 0 if not available
GeomElement* GeomDomain::element(number_t k)
{
  if (domain_p != this) { return domain_p->element(k); } // call with the true domain
  where("GeomDomain::element");
  error("geoelt_not_found");
  return nullptr;
}

//order of geometric elements of domain
number_t GeomDomain::order() const
{
  if (domain_p != this) { return domain_p->order(); } // call with the true domain
  where("GeomDomain::order()");
  warning("not_handled_return",tostring(0));
  return 0;
}

//get and set useColor flag
bool GeomDomain::colorFlag() const
{
  if (domain_p != this) { return domain_p->colorFlag(); } // call with the true domain
  where("GeomDomain::colorFlag()");
  warning("not_handled_return",tostring(0));
  return false;
}

void GeomDomain::setColorFlag(bool f) const
{
  if (domain_p != this) { return domain_p->setColorFlag(f); } // call with the true domain
  where("GeomDomain::setColorFlag()");
  warning("not_handled_return",tostring(0));
}


//! compute the measure (lenght, surface or volume) of GeomDomain
real_t GeomDomain::measure() const
{
  if (domainInfo_p->measure==0)
  {
    if (domain_p != this) domainInfo_p->measure=domain_p->measure(); // call with the true domain
    else
    {
      where("GeomDomain::measure()");
      error("domain_not_handled",words("domain type",domType()));
    }
  }
  return domainInfo_p->measure;
}

//!< update geometric domain info (measure, characteristic sizes, ...)
void GeomDomain::computeStatistics(bool computeSideInfo)
{
  if (domain_p != this) domain_p->computeStatistics(computeSideInfo); // call with the true domain
  else
  {
    where("GeomDomain::computeStatistics()");
    error("domain_not_handled",words("domain type",domType()));
  }
}

//! set normal orientation of a meshdomain of dimension SpaceDim-1
void GeomDomain::setNormalOrientation(OrientationType ori, const GeomDomain& dom) const
{
  if (domain_p != this) { return domain_p->setNormalOrientation(ori, dom); } // call with the true domain
  where("GeomDomain::setNormalOrientation(OrientationType, Domain)");
  error("domain_not_handled",words("domain type",domType()));
}

void GeomDomain::setNormalOrientation(OrientationType ori, const Point& P) const
{
  if (domain_p != this) { return domain_p->setNormalOrientation(ori, P); } // call with the true domain
  where("GeomDomain::setNormalOrientation(OrientationType, Point)");
  error("domain_not_handled",words("domain type",domType()));
}

void GeomDomain::setNormalOrientation(OrientationType ori, const GeomDomain* gp, const Point* p) const
{
  if (domain_p != this) { return domain_p->setNormalOrientation(ori, gp, p); } // call with the true domain
  where("GeomDomain::setNormalOrientation(OrientationType, Domain*)");
  error("domain_not_handled",words("domain type",domType()));
}


// set material id (>0) for all elements of domain
void GeomDomain::setMaterialId(number_t id)
{
  if (domain_p != this) { domain_p->setMaterialId(id); } // call with the true domain
}


// set domain id (>0) for all elements of domain
void GeomDomain::setDomainId(number_t id)
{
  if (domain_p != this) { domain_p->setDomainId(id); } // call with the true domain
}

//! return domain name without sharp when there is one (const)
string_t GeomDomain::nameWithoutSharp() const
{
  string_t domName=domainInfo_p->name;
  if (domName.find("#")==0) { return domName.substr(1);}
  return domName;
}

//print utility
void GeomDomain::print(std::ostream& out) const
{
  if (domain_p != this) { return domain_p->print(out); } // call with the true domain
}

std::ostream& operator<<(std::ostream& out, const GeomDomain& g)
{
  g.print(out);
  return out;
}

void GeomDomain::addSuffix(const string_t& s)
{
  rename(name()+"_"+s);
}

// clear temporary GeomData structures of MeshElement's
void GeomDomain::clearGeomMapData()
{
  if (domain_p != this) { domain_p->clearGeomMapData(); } // call with the true domain
}

// get supporting geometry,
//   if geometry_p is null, find the supporting geometry and update geometry_p
//   return geometry_p (may be 0 if supporting geometry has not been found)
Geometry* GeomDomain::geometry() const
{
  if (geometry_p!=nullptr) return geometry_p;    // geometry already set, return it
  const Mesh* mesh_p=domainInfo_p->mesh_p;
  if (mesh_p==nullptr)  return nullptr;          // if no mesh no way to find supporting geometry
  Geometry* geom_p=mesh_p->geometry_p;
  if (geom_p==nullptr)  return nullptr;          // if no geometry related to mesh, no way to find supporting geometry
  geometry_p = geom_p->find(domainInfo_p->name); // find geometry with name domainInfo_p->name
  return geometry_p;
}

//!< set geometry pointer
void GeomDomain::setGeometry(Geometry* gp)
{
  if (geometry_p!=nullptr) warning("free_warning","geometry pointer of domain "+name()+" is overwritten, may be an error");
  geometry_p = gp;
  if(domain_p!=0 && domain_p!=this) domain_p->geometry_p=gp; // case of GeomDomain handling a MeshDomain
}

//!< true if parametrization allocated
bool GeomDomain::hasParametrization() const
{
  if (geometry_p!=nullptr && geometry_p->parametrizationP()!=nullptr) return true;
  return false;
}

//!< return parametrization if allocated in Geometry, error if not
const Parametrization& GeomDomain::parametrization() const
{
  if (geometry_p!=0 ) return geometry_p->parametrization();
  error("free_error","Domain "+name()+" has no associated geometry and so no parametrization");
  return *new Parametrization();  // fake return
}

//!< return boundary parametrization if allocated in Geometry, error if not
const Parametrization& GeomDomain::boundaryParametrization() const
{
  if (geometry_p!=nullptr ) return geometry_p->boundaryParametrization();
  error("free_error","Domain "+name()+" has no associated geometry and so no parametrization");
  return *new Parametrization();  // fake return
}

//! attached parametrization to domain geometry if exists, replace previous parametrization
//! Note : copy of given parametrization !
void GeomDomain::setParametrization(const Parametrization& par)
{
  if (geometry_p==nullptr) geometry_p = new Geometry(); //fake geometry suupportint the parametrization
  geometry_p->setParametrization(par);
}

//! attached boundary parametrization to domain geometry if exists, replace previous parametrization
//! Note : copy of given parametrization
void GeomDomain::setBoundaryParametrization(const Parametrization& par)
{
  if (geometry_p!=nullptr) geometry_p->setBoundaryParametrization(par);
  else warning("free_warning","domain geometry not set, boundary parametrization cannot be attached to");
}
//static member functions, find if domain belongs to the list of domains
const GeomDomain* GeomDomain::findDomain(const GeomDomain* dom)
{
  std::vector<const GeomDomain*>::iterator it;
  for (it = theDomains.begin(); it != theDomains.end(); it++)
    if (*it == dom) return dom;
  return nullptr;
}
//!< find a domain by its name (static function)
const GeomDomain* GeomDomain::findDomain(const string_t& na)
{
  std::vector<const GeomDomain*>::iterator it;
  for (it = theDomains.begin(); it != theDomains.end(); it++)
    if ((*it)->name() == na) return *it;
  return nullptr;
}

//static member functions, find an union or intersection of domains
const GeomDomain* GeomDomain::findCompositeDomain(SetOperationType sot, const std::vector<const GeomDomain*>& doms)
{
  std::set<const GeomDomain*> sdoms(doms.begin(), doms.end());
  std::vector<const GeomDomain*>::const_iterator it;
  for (it = theDomains.begin(); it != theDomains.end(); it++)
  {
    const GeomDomain* git = *it;
    if (git->domType() == _compositeDomain &&
        git->compositeDomain()->setOpType() == sot &&
        git->compositeDomain()->domains().size() == doms.size())
    {
      number_t n = 0;
      //while (n < doms.size() && git->compositeDomain()->domains()[n] == doms[n]) { n++; }
      while (n < doms.size() && sdoms.find(git->compositeDomain()->domains()[n])!=sdoms.end()) { n++; }
      if (n == doms.size()) { return git; } // union is found
    }
  }
  return nullptr;   //not found
}

void GeomDomain::partition(Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  buildPartitionData(ps);
}

void GeomDomain::partition(Parameter& p1, Parameter& p2)
{
  std::vector<Parameter> ps={p1,p2};
  buildPartitionData(ps);
}

void GeomDomain::partition(Parameter& p1, Parameter& p2, Parameter& p3)
{
  std::vector<Parameter> ps={p1,p2,p3};
  buildPartitionData(ps);
}
void GeomDomain::partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4)
{
  std::vector<Parameter> ps={p1,p2,p3,p4};
  buildPartitionData(ps);
}
void GeomDomain::partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4, Parameter& p5)
{
  std::vector<Parameter> ps={p1,p2,p3,p4,p5};
  buildPartitionData(ps);
}
void GeomDomain::partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4, Parameter& p5, Parameter& p6)
{
  std::vector<Parameter> ps={p1,p2,p3,p4,p5,p6};
  buildPartitionData(ps);
}
void GeomDomain::partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4, Parameter& p5, Parameter& p6, Parameter& p7)
{
  std::vector<Parameter> ps={p1,p2,p3,p4,p5,p6,p7};
  buildPartitionData(ps);
}
void GeomDomain::partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4, Parameter& p5, Parameter& p6, Parameter& p7, Parameter& p8)
{
  std::vector<Parameter> ps={p1,p2,p3,p4,p5,p6,p7,p8};
  buildPartitionData(ps);
}
void GeomDomain::partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4, Parameter& p5, Parameter& p6, Parameter& p7, Parameter& p8, Parameter& p9)
{
  std::vector<Parameter> ps={p1,p2,p3,p4,p5,p6,p7,p8,p9};
  buildPartitionData(ps);
}
void GeomDomain::partition(Parameter& p1, Parameter& p2, Parameter& p3, Parameter& p4, Parameter& p5, Parameter& p6, Parameter& p7, Parameter& p8, Parameter& p9, Parameter& p10)
{
  std::vector<Parameter> ps={p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
  buildPartitionData(ps);
}

PartitionData* GeomDomain::partitionData()
{
  return this->meshDomain()->partitionData_p;
}

//static member functions, find if a union domain dom has been already built,
//  use the description member of mesh domain  = "dom1+dom2+...."
const GeomDomain* GeomDomain::findUnionDomain(const GeomDomain* dom)
{
  if (dom->domType() == _compositeDomain && dom->compositeDomain()->setOpType() == _union)
  {
    const std::vector<const GeomDomain*>& doms=dom->compositeDomain()->domains();    // domains of union
    std::vector<const GeomDomain*>::const_iterator it=doms.begin();
    string_t desc =(*it)->name();
    it++;
    for (; it!=doms.end(); ++it)
      desc+="+"+(*it)->name();
    for (it = theDomains.begin(); it != theDomains.end(); it++)
    {
      if ((*it)->domType()==_meshDomain && (*it)->description()==desc)
        return (*it);
    }
    return nullptr;
  }
  where("GeomDomain::findUnionDomain(GeomDomain)");
  error("domain_notunion",dom->name());
  return nullptr;
}

//static member functions, find if a mesh union domain dom has been already built,
//  same mesh and same description  = "dom1+dom2+...."
const GeomDomain* GeomDomain::findMeshDomainComposite(SetOperationType sot, const std::vector<const GeomDomain*>& doms)
{
  std::vector<const GeomDomain*>::const_iterator it=doms.begin();
  string_t desc =(*it)->name(); it++;
  const Mesh* mp=(*it)->mesh();  // mesh of doms
  for (; it!=doms.end(); ++it)  desc+="+"+(*it)->name();
  for (it = theDomains.begin(); it != theDomains.end(); it++)  //travel all mesh domains
  {
    if ((*it)->domType()==_meshDomain && (*it)->description()==desc && (*it)->mesh()==mp)   return (*it);
  }
  return nullptr;
}

//print all domains of theDomains list
void GeomDomain::printTheDomains(std::ostream& out)
{
  if (theVerboseLevel <= 0) return;
  std::vector<const GeomDomain*>::const_iterator it;
  out << "list of all domains in memory:"<<eol;
  for (it = theDomains.begin(); it != theDomains.end(); it++)
  {
    out<<"  "<<(*it)<<": "<<(**it)<<eol;
  }
}

/*! construct or identify the geometrical union of domains.
    Geometrical union is a real union which performs element or side element inclusion in other elements or side elements
    it uses the element id number to test inclusion of a list of elements in an other list of elements
    for side element it uses the parent element id number. !!! Side of side element is not handled for the moment

    largeDom is a large domain containing all domains given in doms

    GeomDomains may be a meshDomain or a compositeDomain of union type (domains intersection are not managed)
      when a domain contains all others, the function returns it
      when the union gives an existing domain, the function returns it
      else the function return a new compositeDomain (union)

    \note do not confuse geometrical union with union of domains which is a semantic union (no geometrical analysis is performed)
          Geometrical union is an internal tool, end users do not have to use it
*/

const GeomDomain* geomUnionOf(std::vector<const GeomDomain*>& doms, const GeomDomain* largeDom)
{
  // trivial cases
  if (doms.size()==0)
  {
    where("geomUnionOf");
    error("is_void",words("list"));
  }
  if (doms.size()==1)
    return doms[0];
  // list of basic domains by splitting composite domains
  std::vector<const GeomDomain*> basicdoms;
  std::vector<const GeomDomain*>::const_iterator itd;
  for (itd=doms.begin(); itd!=doms.end(); itd++)
  {
    if ((*itd)->domType() == _compositeDomain)
    {
      std::vector<const GeomDomain*> bdom = (*itd)->compositeDomain()->basicDomains();
      basicdoms.insert(basicdoms.end(),bdom.begin(),bdom.end());
    }
    else
      basicdoms.push_back(*itd);
  }
  std::sort(basicdoms.begin(),basicdoms.end());
  std::vector<const GeomDomain*>::iterator itf=std::unique(basicdoms.begin(),basicdoms.end());
  basicdoms.resize(std::distance(basicdoms.begin(),itf));
  if (basicdoms.size()==1)
    return basicdoms[0];  //same domains

  // try to reduce domains by identifying inclusion, performs inclusion test between all pair of domains
  number_t nbdom =basicdoms.size();
  std::vector<bool> erasedoms(nbdom,false);
  for (number_t i=0; i<nbdom; i++)
  {
    if (!erasedoms[i])
    {
      for (number_t j=i+1; j<nbdom; j++)
      {
        if (!erasedoms[j])
        {
          if (basicdoms[i]->include(*basicdoms[j]))
            erasedoms[j]=true;
          else if (basicdoms[j]->include(*basicdoms[i]))
            erasedoms[i]=true;
        }
      }
    }
  }
  for (number_t i=0; i<nbdom; i++)
    if (erasedoms[i]) basicdoms[i]=nullptr;
  std::sort(basicdoms.begin(),basicdoms.end());
  itf=std::unique(basicdoms.begin(),basicdoms.end());
  basicdoms.resize(std::distance(basicdoms.begin(),itf));
  if (basicdoms[0]==nullptr)
    basicdoms.erase(basicdoms.begin());  //erase 0 pointer
  nbdom =basicdoms.size();
  if (nbdom==1)
    return basicdoms[0];    //remains only one domain
  //try to identify if the remaining union is not known
  const GeomDomain* dom = GeomDomain::findMeshDomainComposite(_union, basicdoms);
  if (dom!=nullptr)
  {
    if (dom->up()!=nullptr)
      return dom->up();
    else
      return dom;
  }
  dom=GeomDomain::findCompositeDomain(_union,basicdoms);
  if (dom!=nullptr)
    return dom; //composite union found and meshdomain not built
  //check if the union is the "whole domain" (case of a partition of a fundamental mesh domain for instance)
  bool imd= basicdoms[0]->domType()==_meshDomain;
  if (imd)
  {
    const Mesh* msh=basicdoms[0]->meshDomain()->mesh();
    number_t i=0;
    while (imd && i<nbdom)
    {
      if (basicdoms[i]->domType()==_meshDomain)
        imd= basicdoms[i]->meshDomain()->mesh() == msh;  //same mesh
      else
        imd=false;
      i++;
    }
    if (imd && largeDom->meshDomain()->isUnionOf(basicdoms))
      return largeDom;
  }
  // create a new union
  return new GeomDomain(_union, basicdoms);
}

/*!  merge some geometrical domains (true union of elements)
     GeomDomains must be MeshDomain of same dimension
*/
template<typename S_>
GeomDomain& merge(const std::vector<const GeomDomain*>& doms, S_ name)
{
  trace_p->push("merge(std::vector<GeomDomain*>)");
  if (doms.size()==0)
    error("is_void","doms");

  //erase doubloon
  std::vector<const GeomDomain*> domsc= doms;
  std::vector<const GeomDomain*>::iterator itd;
  std::sort(domsc.begin(),domsc.end());
  itd = std::unique(domsc.begin(), domsc.end());
  domsc.resize(std::distance(domsc.begin(),itd));

  //check if only one domain to merge
  number_t nbd=domsc.size();
  if (nbd==1)
  {
    if (domsc[0]->domType()==_meshDomain)
    {
      trace_p->pop();
      return const_cast<GeomDomain&>(*(domsc[0]));
    }
    error("domain_notmesh",domsc[0]->name(), words("domain type",domsc[0]->domType()));
  }

  //check consistancy (meshdomains of same size)
  itd=domsc.begin();
  const GeomDomain* dom= *itd;
  if (dom->domType()!=_meshDomain)
    error("domain_notmesh",dom->name(), words("domain type",dom->domType()));
  dimen_t dim=dom->dim();
  const Mesh* msh=dom->mesh();
  string_t desc=dom->name();  // desc as "dom1.name+dom2.name+ ..." is used by GeomDomain::findUnionDomain and GeomDomain::findMeshDomainComposite to identify built union domain
  itd++;
  for (; itd!=domsc.end(); itd++)
  {
    dom= *itd;
    if (dom->domType()!=_meshDomain)
      error("domain_notmesh",dom->name(), words("domain type", dom->domType()));
    if (dom->dim() != dim)
      error("domains_mismatch_dims",dom->name(), name);
    if (dom->mesh() != msh)
      error("domains_mismatch_meshes",dom->name(), name);
    desc+="+"+dom->name();
  }

  //now merge in a new meshdomain
  bool toSave = false; // the new meshdomain is not intended to be saved into a file
  GeomDomain* ndom = new GeomDomain(*msh, name, dim, desc, 0, toSave);
  MeshDomain* mdom = ndom->meshDomain();
  std::set<ShapeType>& shs= mdom->shapeTypes;
  std::set<GeomElement*> selts;
  for (itd=domsc.begin(); itd!=domsc.end(); itd++)
  {
    const MeshDomain* mtd=(*itd)->meshDomain();
    selts.insert(mtd->geomElements.begin(),mtd->geomElements.end());
    shs.insert(mtd->shapeTypes.begin(),mtd->shapeTypes.end());
  }
  mdom->geomElements.resize(selts.size());
  std::set<GeomElement*>::iterator its=selts.begin();
  std::vector<GeomElement*>::iterator itg= mdom->geomElements.begin();
  for (; its!=selts.end(); its++, itg++)  *itg = *its;
  //add to list of domains of mesh
  msh->addDomain(*mdom);
  trace_p->pop();
  return *ndom;
}

template GeomDomain& merge(const std::vector<const GeomDomain*>& doms, string_t name);
template GeomDomain& merge(const std::vector<const GeomDomain*>& doms, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, S_ name)
{
  std::vector<const GeomDomain*> ds(2);
  ds[0]=&d1; ds[1]=&d2;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, S_ name)
{
  std::vector<const GeomDomain*> ds(3);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4, S_ name)
{
  std::vector<const GeomDomain*> ds(4);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, S_ name)
{
  std::vector<const GeomDomain*> ds(5);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, S_ name)
{
  std::vector<const GeomDomain*> ds(6);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, S_ name)
{
  std::vector<const GeomDomain*> ds(7);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8, S_ name)
{
  std::vector<const GeomDomain*> ds(8);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                  const GeomDomain& d9, S_ name)
{
  std::vector<const GeomDomain*> ds(9);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8; ds[8]=&d9;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                  const GeomDomain& d9, const GeomDomain& d10, S_ name)
{
  std::vector<const GeomDomain*> ds(10);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8; ds[8]=&d9; ds[9]=&d10;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                  const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, S_ name)
{
  std::vector<const GeomDomain*> ds(11);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8; ds[8]=&d9; ds[9]=&d10;
  ds[10]=&d11;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                  const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12, S_ name)
{
  std::vector<const GeomDomain*> ds(12);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8; ds[8]=&d9; ds[9]=&d10;
  ds[10]=&d11; ds[11]=&d12;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                  const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                  const GeomDomain& d13, S_ name)
{
  std::vector<const GeomDomain*> ds(13);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8; ds[8]=&d9; ds[9]=&d10;
  ds[10]=&d11; ds[11]=&d12; ds[12]=&d13;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                  const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                  const GeomDomain& d13, const GeomDomain& d14, S_ name)
{
  std::vector<const GeomDomain*> ds(14);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8; ds[8]=&d9; ds[9]=&d10;
  ds[10]=&d11; ds[11]=&d12; ds[12]=&d13; ds[13]=&d14;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                  const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                  const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, S_ name)
{
  std::vector<const GeomDomain*> ds(15);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8; ds[8]=&d9; ds[9]=&d10;
  ds[10]=&d11; ds[11]=&d12; ds[12]=&d13; ds[13]=&d14; ds[14]=&d15;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                  const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                  const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16, S_ name)
{
  std::vector<const GeomDomain*> ds(16);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8; ds[8]=&d9; ds[9]=&d10;
  ds[10]=&d11; ds[11]=&d12; ds[12]=&d13; ds[13]=&d14; ds[14]=&d15; ds[15]=&d16;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                           string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                           const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                  const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                  const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                  const GeomDomain& d17, S_ name)
{
  std::vector<const GeomDomain*> ds(17);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8; ds[8]=&d9; ds[9]=&d10;
  ds[10]=&d11; ds[11]=&d12; ds[12]=&d13; ds[13]=&d14; ds[14]=&d15; ds[15]=&d16; ds[16]=&d17;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                           const GeomDomain& d17, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                           const GeomDomain& d17, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                  const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                  const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                  const GeomDomain& d17, const GeomDomain& d18, S_ name)
{
  std::vector<const GeomDomain*> ds(18);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8; ds[8]=&d9; ds[9]=&d10;
  ds[10]=&d11; ds[11]=&d12; ds[12]=&d13; ds[13]=&d14; ds[14]=&d15; ds[15]=&d16; ds[16]=&d17; ds[17]=&d18;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                           const GeomDomain& d17, const GeomDomain& d18, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                           const GeomDomain& d17, const GeomDomain& d18, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                  const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                  const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                  const GeomDomain& d17, const GeomDomain& d18, const GeomDomain& d19, S_ name)
{
  std::vector<const GeomDomain*> ds(19);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8; ds[8]=&d9; ds[9]=&d10;
  ds[10]=&d11; ds[11]=&d12; ds[12]=&d13; ds[13]=&d14; ds[14]=&d15; ds[15]=&d16; ds[16]=&d17; ds[17]=&d18;
  ds[18]=&d19;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                           const GeomDomain& d17, const GeomDomain& d18, const GeomDomain& d19, string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                           const GeomDomain& d17, const GeomDomain& d18, const GeomDomain& d19, const char* name);

template<typename S_>
GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                  const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                  const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                  const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                  const GeomDomain& d17, const GeomDomain& d18, const GeomDomain& d19, const GeomDomain& d20, S_ name)
{
  std::vector<const GeomDomain*> ds(20);
  ds[0]=&d1; ds[1]=&d2; ds[2]=&d3; ds[3]=&d4; ds[4]=&d5; ds[5]=&d6; ds[6]=&d7; ds[7]=&d8; ds[8]=&d9; ds[9]=&d10;
  ds[10]=&d11; ds[11]=&d12; ds[12]=&d13; ds[13]=&d14; ds[14]=&d15; ds[15]=&d16; ds[16]=&d17; ds[17]=&d18;
  ds[18]=&d19; ds[19]=&d20;
  return merge(ds, name);
}

template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                           const GeomDomain& d17, const GeomDomain& d18, const GeomDomain& d19, const GeomDomain& d20,
                           string_t name);
template GeomDomain& merge(const GeomDomain& d1, const GeomDomain& d2, const GeomDomain& d3, const GeomDomain& d4,
                           const GeomDomain& d5, const GeomDomain& d6, const GeomDomain& d7, const GeomDomain& d8,
                           const GeomDomain& d9, const GeomDomain& d10, const GeomDomain& d11, const GeomDomain& d12,
                           const GeomDomain& d13, const GeomDomain& d14, const GeomDomain& d15, const GeomDomain& d16,
                           const GeomDomain& d17, const GeomDomain& d18, const GeomDomain& d19, const GeomDomain& d20,
                           const char* name);

/*!  merge some geometrical domains (true union of elements)
     GeomDomains must be MeshDomain of same dimension
*/
template<typename S_>
GeomDomain& merge(const std::vector<GeomDomain*>& doms, S_ name)
{
  trace_p->push("merge(std::vector<GeomDomain*>)");
  if (doms.size()==0)
    error("is_void","doms");

  //erase doubloon
  std::vector<GeomDomain*> domsc= doms;
  std::vector<GeomDomain*>::iterator itd;
  std::sort(domsc.begin(),domsc.end());
  itd = std::unique(domsc.begin(), domsc.end());
  domsc.resize(std::distance(domsc.begin(),itd));

  //check if only one domain to merge
  number_t nbd=domsc.size();
  if (nbd==1)
  {
    if (domsc[0]->domType()==_meshDomain)
    {
      trace_p->pop();
      return *(domsc[0]);
    }
    error("domain_notmesh",domsc[0]->name(), words("domain type",domsc[0]->domType()));
  }

  //check consistancy (meshdomains of same size)
  itd=domsc.begin();
  GeomDomain* dom= *itd;
  if (dom->domType()!=_meshDomain)
    error("domain_notmesh",dom->name(), words("domain type",dom->domType()));
  dimen_t dim=dom->dim();
  const Mesh* msh=dom->mesh();
  string_t desc=dom->name();
  itd++;
  for (; itd!=domsc.end(); itd++)
  {
    dom= *itd;
    if (dom->domType()!=_meshDomain)
      error("domain_notmesh",dom->name(), words("domain type", dom->domType()));
    if (dom->dim() != dim)
      error("domains_mismatch_dims",dom->name(), name);
    if (dom->mesh() != msh)
      error("domains_mismatch_meshes",dom->name(), name);
    desc+="+"+dom->name();
  }

  //now merge in a new meshdomain
  bool toSave = false; // the new meshdomain is not intended to be saved into a file
  GeomDomain* ndom = new GeomDomain(*msh, name, dim, desc, 0, toSave);
  MeshDomain* mdom = ndom->meshDomain();
  std::set<ShapeType>& shs= mdom->shapeTypes;
  std::set<GeomElement*> selts;
  for (itd=domsc.begin(); itd!=domsc.end(); itd++)
  {
    const MeshDomain* mtd=(*itd)->meshDomain();
    selts.insert(mtd->geomElements.begin(),mtd->geomElements.end());
    shs.insert(mtd->shapeTypes.begin(),mtd->shapeTypes.end());
  }

  mdom->geomElements.resize(selts.size());
  std::set<GeomElement*>::iterator its=selts.begin();
  std::vector<GeomElement*>::iterator itg= mdom->geomElements.begin();
  for (; its!=selts.end(); its++, itg++)
    * itg = *its;

  trace_p->pop();
  return *ndom;
}

template GeomDomain& merge(const std::vector<GeomDomain*>& doms, string_t name);
template GeomDomain& merge(const std::vector<GeomDomain*>& doms, const char* name);

//!< create the domain made of union of elements (same dimension)
GeomDomain& operator+(const GeomDomain& dom1, const GeomDomain& dom2)
{
    return merge(dom1,dom2,dom1.name()+"+"+dom2.name());
}

//update parent sides info of a side domain (only for mesh domain)
void GeomDomain::updateParentOfSideElements()
{
  trace_p->push("GeomDomain::updateParentOfSideElements");
  MeshDomain* mdom=meshDomain();
  if (mdom==nullptr)
  {
    where("GeomDomain::updateParentOfSideElements()");
    error("domain_notmesh", name());
  }
  if (mdom->parentSidesUptodate)
  {
    trace_p->pop();
    return;
  }
  std::map<string_t,std::vector<GeoNumPair> > sideIndex;
  mdom->updateSides(sideIndex);
  mdom->parentSidesUptodate=true;
  trace_p->pop();
}

/*! defined a crack domain from both sides of the crack
    in fact, define the crack domain as side 1
*/
GeomDomain& crack(const GeomDomain& side1, const GeomDomain& side2)
{
  GeomDomain& s1=const_cast<GeomDomain&>(side1);
  GeomDomain& s2=const_cast<GeomDomain&>(side2);
  MeshDomain* m1=s1.meshDomain(), *m2=s2.meshDomain();
  if (m1==nullptr)
    error("free_error","crack(...), "+side1.name()+" not a mesh domain");
  if (m2==nullptr)
    error("free_error","crack(...), "+side2.name()+" not a mesh domain");
  m1->dualCrackDomain_p = m2;
  m2->dualCrackDomain_p = m1;
  return s1;
}

//! access to domain defined from all sides of elements of domain dom, create it if not defined
GeomDomain& sides(GeomDomain& dom)
{
  MeshDomain* mdom=dom.meshDomain();
  if (mdom==nullptr)
  {
    where("sides(GeomDomain)");
    error("domain_notmesh",dom.name(),dom.domTypeName());
  }
  return mdom->sidesDomain();
}

//! access to domain defined from all internal sides of elements of domain dom, create it if not defined
GeomDomain& internalSides(GeomDomain& dom)
{
  MeshDomain* mdom=dom.meshDomain();
  if (mdom==nullptr)
  {
    where("internalSides(GeomDomain)");
    error("domain_notmesh",dom.name(),dom.domTypeName());
  }
  return mdom->internalSidesDomain();
}

//===============================================================================================================
// MeshDomain member functions and related external functions
//===============================================================================================================
//constructor
MeshDomain::MeshDomain(const Mesh& me, const string_t& na, dimen_t d, const string_t& desc)
{
  domainInfo_p->domType=_meshDomain;
  domainInfo_p->mesh_p=&me;
  domainInfo_p->name=na;
  domainInfo_p->dim=d;
  domainInfo_p->description=desc;
  setShapeTypes();
  normalOrientationRule = std::make_pair(_undefOrientationType,(const GeomDomain*)(nullptr));
}

//destructor removing domains, data that depend to current domains
MeshDomain::~MeshDomain()
{
   removeExt();  // remove extension of current domain
   if(sidesDomain_p!=nullptr) delete sidesDomain_p;
   if(partitionData_p!=nullptr) delete partitionData_p;
}

// delete and remove side extension of current domain
void MeshDomain::removeExt()
{
  auto it =GeomDomain::theDomains.begin();
  while(it!=GeomDomain::theDomains.end())
  {
    if((*it)->domType()==_meshDomain)
    {
      const MeshDomain * mdom=(*it)->meshDomain();
      if(mdom!=nullptr && mdom->extensionof_p == this)
      {
        delete mdom;       // remove also from theDomains vector
        it=GeomDomain::theDomains.begin(); // not optimal but safe
      }
      else it++;
    }
    else it++;
  }
}

//set the list of element shape types in mesh domain (counted once)
void MeshDomain::setShapeTypes()
{
  trace_p->push("MeshDomain::setShapeTypes");
  std::vector<GeomElement*>::const_iterator itg;
  shapeTypes.clear();
  if (dim() == 0) { shapeTypes.insert(_point); }
  else
  {
    for (itg = geomElements.begin(); itg != geomElements.end(); itg++)
      {
        shapeTypes.insert((*itg)->shapeType());
      }
  }
  trace_p->pop();
}

//return the set of all node numbers of domain, ascending order
std::set<number_t> MeshDomain::nodeNumbers() const
{
  std::set<number_t> snn;
  std::vector<GeomElement*>::const_iterator itg;
  for (itg = geomElements.begin(); itg != geomElements.end(); itg++)
  {
    std::vector<number_t> nn=(*itg)->nodeNumbers();
    snn.insert(nn.begin(),nn.end());
  }
  return snn;
}

//return the set of all vertex numbers of domain, ascending order
std::set<number_t> MeshDomain::vertexNumbers() const
{
  std::set<number_t> snn;
  std::vector<GeomElement*>::const_iterator itg;
  for (itg = geomElements.begin(); itg != geomElements.end(); itg++)
  {
    std::vector<number_t> nn=(*itg)->vertexNumbers();
    snn.insert(nn.begin(),nn.end());
  }
  return snn;
}

// return the set of all nodes of meshdomain, numbers ascending order (see nodeNumbers)
std::vector<Point> MeshDomain::nodes() const
{
  std::set<number_t> nn=nodeNumbers();
  std::vector<Point> pts(nn.size());
  const std::vector<Point>& meshnodes=mesh()->nodes;
  std::set<number_t>::iterator itn=nn.begin();
  std::vector<Point>::iterator itp=pts.begin();
  for (; itn!=nn.end(); itn++, itp++)
    *itp = meshnodes[*itn -1];
  return pts;
}

//! return the set of all nodes of meshdomain, numbers ascending order (see nodeNumbers)
std::vector<Point> MeshDomain::vertices() const
{
  std::set<number_t> nn=vertexNumbers();
  std::vector<Point> pts(nn.size());
  const std::vector<Point>& meshnodes=mesh()->nodes;
  std::set<number_t>::iterator itn=nn.begin();
  std::vector<Point>::iterator itp=pts.begin();
  for (; itn!=nn.end(); itn++, itp++)
    *itp = meshnodes[*itn -1];
  return pts;
}

//! return the measure (length, surface or volume) of MeshDomain
real_t MeshDomain::measure() const
{
  real_t mes=0.;
  std::vector<GeomElement*>::const_iterator itg;
  for (itg = geomElements.begin(); itg != geomElements.end(); itg++)
    mes+=(*itg)->measure();
  return mes;
}

//! update geometric domain info (measure, characteristic sizes, ...)
void MeshDomain::computeStatistics(bool computeSideInfo)
{
  domainInfo_p->statsComputed=true;
  domainInfo_p->nbOfElements=geomElements.size();
  domainInfo_p->nbOfNodes = nodeNumbers().size();
  domainInfo_p->nbOfVertices=vertexNumbers().size();
  real_t& mes = domainInfo_p->measure;
  real_t& hmin = domainInfo_p->minEltSize;
  real_t& hmax = domainInfo_p->maxEltSize;
  real_t& havg = domainInfo_p->averageEltSize;
  real_t& mmin = domainInfo_p->minEltMeasure;
  real_t& mmax = domainInfo_p->maxEltMeasure;
  real_t& mavg = domainInfo_p->averageEltMeasure;
  hmin=theRealMax; hmax=0; havg=0;
  mmin=theRealMax; mmax=0; mavg=0;
  real_t h,m;
  std::vector<GeomElement*>::const_iterator itg;
  for (itg = geomElements.begin(); itg != geomElements.end(); ++itg)
  {
    m=(*itg)->measure();
    mes+=m;
    mmin=std::min(mmin,m); mmax=std::max(mmax,m);
    h=(*itg)->size();
    havg+=h;
    hmin=std::min(hmin,h); hmax=std::max(hmax,h);
  }
  havg/=domainInfo_p->nbOfElements;
  mavg=mes/domainInfo_p->nbOfElements;

  if (computeSideInfo && dim()>1)  //compute side info
  {
    domainInfo_p->sideStatsComputed=true;
    GeomDomain& sdom = sidesDomain();  // compute sides domain if not exist
    sdom.computeStatistics(false);
    domainInfo_p->minSideMeasure=sdom.domainInfo().minEltMeasure;
    domainInfo_p->maxSideMeasure=sdom.domainInfo().maxEltMeasure;
    domainInfo_p->averageSideMeasure=sdom.domainInfo().averageEltMeasure;
    real_t& bmes = domainInfo_p->boundaryMeasure;
    for (itg = sdom.meshDomain()->geomElements.begin(); itg != sdom.meshDomain()->geomElements.end(); ++itg)
    {
      if ((*itg)->parentSides().size()<2) bmes+=(*itg)->measure();
    }
  }
}

/*! check if an union of mesh subdomains is equal to the current mesh domain
    assuming that the current mesh domain includes all domains
    perform only analysis on the number of elements (not robust)
*/
bool MeshDomain::isUnionOf(const std::vector<const GeomDomain*>& doms) const
{
  if (domType() != _meshDomain) { error("domain_notmesh", name(), words("domain type", domType())); }
  trace_p->push("MeshDomain::isUnionOf");
  //number analysis if possible
  number_t nbelt = 0;
  for (number_t k = 0; k < doms.size(); k++)
  {
    if (doms[k]->domType() != _meshDomain)
      { error("domain_notmesh", doms[k]->name(), words("domain type", doms[k]->domType())); }
    if (doms[k] == this)
    {
      trace_p->pop();
      return true;
    }
    if (doms[k]->dim() == dim()) { nbelt += doms[k]->numberOfElements(); }
  }
  if (nbelt < numberOfElements()) //obviously is not union of
    {trace_p->pop(); return false;}

  //deeper analysis, make a list of different element (pointers) and compare size
  std::set<const GeomElement*> difElts;
  for (number_t k = 0; k < doms.size(); k++)
    if (doms[k]->dim() == dim())
      difElts.insert(doms[k]->meshDomain()->geomElements.begin(),
                     doms[k]->meshDomain()->geomElements.end());
  //test on number of elements, works if domains of union are subdomains
  //a more robust algorithm have to compare the two lists of pointers, unnecessary for the moment
  trace_p->pop();
  return difElts.size() == numberOfElements();
}

/*! return the largest domain including a list of subdomains (doms), in a geometric meaning, it may be
  - the current domain if the union is the current domain
  - one of the domain, if one contains all of the others and is different from current one
  - the union of subdomains (created if does not exist) else
*/
const GeomDomain* MeshDomain::largestDomain(std::vector<const GeomDomain*> doms) const
{
  trace_p->push("MeshDomain::largestDomain");
  if (doms.size() == 0)
  {
    trace_p->pop();
    return nullptr;
  }
  if (doms.size() == 1)
  {
    trace_p->pop();
    return doms[0];
  }
  std::sort(doms.begin(), doms.end());                  //sort domains along pointers
  std::vector<const GeomDomain*>::iterator itg = unique(doms.begin(), doms.end()); //keep unique
  doms.resize(itg - doms.begin());                      //doms is now a sorted list with no duplicated domains
  if (doms.size() == 1)
  {
    trace_p->pop();  //only one domain, return it
    return doms[0];
  }
  if (isUnionOf(doms))
  {
    trace_p->pop();  //if the union is the current domain return this
    return this;
  }
  trace_p->pop();
  return geomUnionOf(doms,this);
}

//! return true if MeshDomain is a side of meshdomain (first elment has a parent)
bool MeshDomain::isSideDomain() const
{
  if (geomElements.size()>0)
    return geomElements.front()->isSideElement();
  return false;
}

//!< true if MeshDomain is a set of element sides (isSidesOf_p!=nullptr)
bool MeshDomain::isSidesDomain() const
{
    return isSidesOf_p!=nullptr;
}

//! true if MeshDomain is an interface between domains (first element has more than one parent)
bool MeshDomain::isInterface() const
{
  if (geomElements.size()>0)
    return geomElements.front()->numberOfParents()>1;
  return false;
}

//!< true if MeshDomain is a side of a crack
bool MeshDomain::isCrack() const
{
  return dualCrackDomain_p!=nullptr;
}

//!< true if MeshDomain is a side of a given mesh domain
bool MeshDomain::isSideOf(const MeshDomain& dom) const
{
  for (auto& d : parents)
    if (d == &dom)  return true;
  std::vector<GeoNumPair>& parelts=geomElements[0]->parentSides();
  if (parelts.size()==0)  return false;
  std::set<GeomElement*> gelts(dom.geomElements.begin(),dom.geomElements.end());
  bool found=false;
  number_t k=0;
  while (!found && k<parelts.size())
  {
    found = gelts.find(parelts[k].first)!=gelts.end();
    k++;
  }
  return found;
}

/*! return true if the current domain includes a given domain
    inclusion has to be understood in a geometric meaning, not in algebraic meaning
    as side is included in its parent element, a boundary domain may be included in a volumic domain
*/
bool MeshDomain::include(const GeomDomain& d) const
{
  if (d.domType()==_meshDomain)
    return include(*d.meshDomain());
  if (d.domType()==_compositeDomain)
  {
    bool inc=true;
    const std::vector<const GeomDomain*>& doms=d.compositeDomain()->domains();
    std::vector<const GeomDomain*>::const_iterator itd=doms.begin();
    while (itd != doms.end() && inc)
    {
      inc = inc && include(**itd);
      itd++;
    }
    return inc;
  }
  error("domain_notmeshorcomposite",d.name(),words("domain type", d.domType()));
  return false;
}

// true if the current domain includes a given MeshDomain (const)
bool MeshDomain::include(const MeshDomain& d) const
{
  for (auto& e : d.parents)
    if (e==this) return true;
  //robust analysis (each element of d geometrically belongs to an element of current mesh
  std::vector<number_t> numelts(geomElements.size());
  std::vector<GeomElement*>::const_iterator itel=geomElements.begin();
  std::vector<number_t>::iterator itn=numelts.begin();
  for (; itel != geomElements.end(); itel++, itn++)
    *itn = (*itel)->number();
  std::sort(numelts.begin(), numelts.end());
  itel = d.geomElements.begin();
  bool inc=true;
  while (itel != d.geomElements.end() && inc)
  {
    GeomElement* gelt=*itel;
    inc = std::binary_search(numelts.begin(),numelts.end(),gelt->number());
    if (!inc && gelt->isSideElement())
    {
      std::vector<GeoNumPair> eltpar=gelt->parentSides();
      std::vector<GeoNumPair>::const_iterator itg=eltpar.begin();
      while (itg != eltpar.end() && !inc)
      {
        GeomElement* parelt=itg->first;
        inc = std::binary_search(numelts.begin(),numelts.end(),parelt->number());
        if (!inc && parelt->isSideElement())
          error("sos_elt_not_handled");
        itg++;
      }
    }
    if (!inc)
      return false;
    itel++;
  }
  return inc;
}

/*! find or create the extension of a side domain
    two cases are considered:
      extension from side elements: gives the list of all geometric elements having a side on the side domain,
                                     extended name: 'side domain name'_sextension
      extension from side vertices: gives the list of all geometric elements having a vertex on the side domain,
                                     extended name: 'side domain name'_vextension
    the former extension is used when computing integral over side domain involving non tangential operator
    while the second one is used when extending a function from side to inside domain

    useVertex: if true do extension from side vertices else do extension from side elements
    omega: optional inside domain, restrict extension to elements of omega,
           when omega is void (or not a MeshDomain) take all parent elements

*/
GeomDomain& MeshDomain::extendDomain(bool useVertex, const GeomDomain& omega) const
{
  string_t naext = name();
  if (useVertex)  naext+="_vextension";
  else           naext+="_sextension";
  bool restrict = omega.domType()==_meshDomain;
  if (restrict)   naext+="_"+omega.name();
  std::vector<const GeomDomain*>::iterator it;
  for (it = theDomains.begin(); it != theDomains.end(); it++)
    if ((*it)->name() == naext) return const_cast<GeomDomain&>(**it);

  GeomDomain* domext=new GeomDomain(*mesh(), naext, dim()+1, "extension constructed by extendDomain()");
  MeshDomain* mdomext=domext->meshDomain();
  mdomext->extensionof_p=this;
  mdomext->extensionin_p=nullptr;  //free extension: all parents touching
  if (restrict) mdomext->extensionin_p=omega.meshDomain(); //restricted extension to the elements of given domain omega

  if (!useVertex)  //create extension from side elements
  {
    std::set<GeomElement*> eltomega;
    if (restrict)
    {
      const MeshDomain* momega=omega.meshDomain();
      eltomega.insert(momega->geomElements.begin(),momega->geomElements.end());
    }
    std::vector<GeomElement*>::const_iterator itg = geomElements.begin();
    std::vector<GeoNumPair>::const_iterator itp;
    std::set<GeomElement*> elts;
    for (; itg!=geomElements.end(); ++itg)
    {
      std::vector<GeoNumPair>& pars=(*itg)->parentSides();
      if (pars.size()==0)
      {
        where("MeshDomain::extendDomain");
        error("domain_not_side",name());
      }
      std::list<GeoNumPair> parelts;
      for (itp=pars.begin(); itp!=pars.end(); ++itp)
        if (!restrict || (restrict && eltomega.find(itp->first)!=eltomega.end()))
        {
          elts.insert(itp->first);
          parelts.push_back(*itp);
        }
      mdomext->sideToExt[*itg]= parelts;
    }
    if (elts.size()==0)
    {
      where("MeshDomain::extendDomain(bool, Domain)");
      error("is_void","elts");
    }
    mdomext->geomElements.resize(elts.size());
    std::vector<GeomElement*>::iterator itf=mdomext->geomElements.begin();
    std::set<GeomElement*>::iterator itl=elts.begin();
    for (; itl!=elts.end(); ++itl, ++itf) *itf = *itl;
    mdomext->setShapeTypes();
    mdomext->extensionType = _sideExtension;
    return *domext;
  }

  //create extension from side vertices
  const std::vector<GeomElement*>* eltomega = &mesh()->elements(); //all elements
  if (restrict) eltomega = &omega.meshDomain()->geomElements;
  std::set<number_t> vns = vertexNumbers();  //side vertices
  std::set<GeomElement*> elts;
  std::vector<GeomElement*>::const_iterator itg=eltomega->begin();
  for (; itg!=eltomega->end(); ++itg)
  {
    std::vector<number_t> vn =(*itg)->vertexNumbers();
    std::vector<number_t>::iterator itn=vn.begin();
    for (; itn!=vn.end(); ++itn)
      if (vns.find(*itn)!=vns.end()) elts.insert(*itg);
  }
  if (elts.size()==0)
  {
    where("MeshDomain::extendDomain(bool, Domain)");
    error("is_void","elts");
  }
  mdomext->geomElements.resize(elts.size());
  std::vector<GeomElement*>::iterator itf=mdomext->geomElements.begin();
  std::set<GeomElement*>::iterator its=elts.begin();
  for (; its!=elts.end(); ++its, ++itf) *itf = *its;
  mdomext->setShapeTypes();
  mdomext->extensionType = _vertexExtension;
  return *domext;
}

/*! if not exists, builds the domains made of ALL sides of all elements of the current domain
    else returns *sidesDomain_p
    if current domain dimension = mesh dimension, the GeomElement* stored in Mesh::Sides are used
    else new GeomElement are created using MeshDomain::buildSides
    na: name of the sidesDomain if empty, name will be "sides of "+this->name()
         when sidesDomain already exists, na has no effect
*/
GeomDomain& MeshDomain::sidesDomain(const string_t& na) const
{
  if (sidesDomain_p!=nullptr) return *sidesDomain_p;
  trace_p->push("GeomDomain::sidesDomain");
  string_t desc = "sides of "+name(), name=na;
  if (name=="") name= desc;
  sidesDomain_p = new MeshDomain(*mesh(), name, dim()-1, desc);
  sidesDomain_p->parents.push_back( const_cast<MeshDomain*>(this));
  sidesDomain_p->isSidesOf_p=this;
  domainInfo_p->mesh_p->addDomain(*sidesDomain_p);
  if (isSideDomain()) // loop on geom element of current domain and create sides
  {
    this->buildSides(sidesDomain_p->geomElements);
  }
  else // not a side domain, retrieve side elements from mesh::sides
  {
     Mesh* me = const_cast<Mesh*>(mesh());
     if (me->sides().size()==0) me->buildSides();
     std::set<GeomElement*> sidelts;
     std::vector<GeomElement*>::const_iterator itg=meshDomain()->geomElements.begin();
     for (;itg!=meshDomain()->geomElements.end();++itg)
     {
        MeshElement* melt = (*itg)->meshElement();
        for (number_t s = 0; s < (*itg)->numberOfSides(); s++)
           sidelts.insert(me->sides()[melt->sideNumbers[s]-1]);
     }
     sidesDomain_p->geomElements.assign(sidelts.begin(),sidelts.end());
     sidesDomain_p->setShapeTypes();
  }
  trace_p->pop();
  return *sidesDomain_p;
}

/*! if not exists, builds the domains made of INTERNAL sides of all elements of the current domain
    if current domain dimension = mesh dimension, the GeomElement* stored in Mesh::Sides are used
    else new GeomElement are created using MeshDomain::buildSides
    na : name of the sidesDomain if empty, name will be "sides of "+this->name()
         when sidesDomain already exists, na has no effect
    Note : do not confuse internal sides domain built here and sides domain built in sidesDomain
           and stored in the pointer sidesDomain_p of parent domain
*/
GeomDomain& MeshDomain::internalSidesDomain(const string_t& na) const
{
  string_t desc = "internal sides of "+name(), name=na;
  if (name=="") name= desc;
  GeomDomain *sdom=const_cast<GeomDomain*>(GeomDomain::findDomain(na));
  if (sdom!=nullptr) return *sdom;
  trace_p->push("GeomDomain::internalSidesDomain");
  MeshDomain* mdom = new MeshDomain(*mesh(), name, dim()-1, desc);
  mdom->parents.push_back(const_cast<MeshDomain*>(this));
  mdom->isSidesOf_p=this;
  domainInfo_p->mesh_p->addDomain(*sidesDomain_p);
  if (isSideDomain()) // loop on geom element of current domain and create sides
  {
    this->buildSides(mdom->geomElements);
    std::vector<GeomElement*>::iterator itg=mdom->geomElements.begin();
    std::list<GeomElement*> sidelts;
    for (;itg!=mdom->geomElements.begin();++itg)
       if ((*itg)->parentSides().size()>1) sidelts.push_back(*itg);
    mdom->geomElements.assign(sidelts.begin(),sidelts.end());
  }
  else // not a side domain, retrieve side elements from mesh::sides
  {
     Mesh* me = const_cast<Mesh*>(mesh());
     if (me->sides().size()==0) me->buildSides();
     std::set<GeomElement*> sidelts;
     std::vector<GeomElement*>::const_iterator itg=meshDomain()->geomElements.begin();
     for (;itg!=meshDomain()->geomElements.end();++itg)
     {
        MeshElement* melt = (*itg)->meshElement();
        for (number_t s = 0; s < (*itg)->numberOfSides(); s++)
        {
          GeomElement *gelt=me->sides()[melt->sideNumbers[s]-1];
          if (gelt->parentSides().size()>1) sidelts.insert(gelt);
        }
     }
     mdom->geomElements.assign(sidelts.begin(),sidelts.end());
     mdom->setShapeTypes();
  }
  trace_p->pop();
  return *mdom;
}

/*! create side elements (ALL SIDES) of the current domain and store them in geomElements vector
    algorithm similar  to Mesh::buildSides
    internal tool, called by sidesDomain() but may be used out of this context
*/
void MeshDomain::buildSides(std::vector<GeomElement*>& sidelts) const
{
  trace_p->push("MeshDomain::buildSides");
  std::map<string_t, number_t> sideIndex; //temporary index where the string key is the ordered list of the side vertex numbers
  std::vector<number_t>::iterator itn;
  std::map<string_t, number_t>::iterator itsn;
  number_t nbside = 0;
  number_t& lastIndex = mesh()->lastIndex_;
  // main loop on domain elements
  std::vector<GeomElement*>::const_iterator itg=geomElements.begin();
  for (;itg!=geomElements.end();++itg)
  {
    GeomElement* gelt = *itg;
    MeshElement* melt = gelt->buildSideMeshElement();
    for (number_t s = 0; s < gelt->numberOfSides(); s++)   // loop on element sides
    {
      string_t key = gelt->encodeElement(s + 1); // encode vertex numbers of side s
      itsn = sideIndex.find(key);                // search side in sideIndex map
      if (itsn == sideIndex.end())                // create a new side element
      {
        sidelts.push_back(new GeomElement(gelt, s + 1, ++lastIndex)); //update geomElements
        nbside++;
        sideIndex[key] = nbside;            // update sideIndex
        melt->sideNumbers[s] = nbside;      // update melt->sideNumbers
      }
      else // update side numbering of element
      {
        melt->sideNumbers[s] = itsn->second; //update sideNumbers
        std::vector<GeoNumPair>& parside = sidelts[itsn->second - 1]->parentSides(); //update parentSides_
        if (std::find(parside.begin(), parside.end(), GeoNumPair(gelt, s + 1)) == parside.end())
          { parside.push_back(GeoNumPair(gelt, s + 1)); }
      } // end of else
    } // end for (number_t s
  } //end for (itg
  trace_p->pop();
}

/*! create a ficticious domain of current domain (say gamma) related to a given domain (say omega)
    the ficticious domain is the set of elements intersecting any element of Gamma
    the name of ficticious domain is: gamma.name_F_omega.name
    - the sideToExt map of ficticious domain gives the list of elements of omega intersecting an element of gamma
    that is useful to compute FEM integral involving unknown on gamma and unknown on omega
    - generally gamma is a domain of dimension n-1 as domain omega is of dimension n, but the algorithm used is general
    and is able to deal with any kind of domains whereas the function intersect(GeomElement, GeomElement) can deal with any kind of elements
*/
GeomDomain& MeshDomain::ficticiousDomain(const GeomDomain& omega) const
{
  if (omega.domType()!=_meshDomain)
    error("free_error", "MeshDomain::ficticiousDomain requires omega to be a meshdomain");
  const MeshDomain* momega=omega.meshDomain();
  string_t naext = name()+"_F_"+omega.name();
  std::vector<const GeomDomain*>::iterator it;
  for (it = theDomains.begin(); it != theDomains.end(); it++)
    if ((*it)->name() == naext) return const_cast<GeomDomain&>(**it);

  trace_p->push("MeshDomain::ficticiousDomain");
  GeomDomain* ficdom=new GeomDomain(*omega.mesh(), naext, omega.dim(), "ficticious domain");
  MeshDomain* mficdom=ficdom->meshDomain();
  mficdom->shapeTypes=momega->shapeTypes;      //same shapes
  mficdom->extensionof_p = this;
  mficdom->extensionin_p = momega;
  mficdom->normalOrientationRule=momega->normalOrientationRule;
  mficdom->jacobianComputed=momega->jacobianComputed;
  mficdom->diffEltComputed=momega->diffEltComputed;
  mficdom->normalComputed=momega->normalComputed;
  mficdom->inverseJacobianComputed=momega->inverseJacobianComputed;

  // set map function gamma -> omega (0 if not exist)
  const DomainMap* dmap = domainMap(static_cast<const GeomDomain&>(*this),omega);
  const Function* fmap = nullptr;
  if (dmap!=nullptr) fmap=&(dmap->map1to2());

  // tree algorithm that can deal with any kind of elements if intersect(GeomElement, GeomElement) function deal with
  // specific algorithms depending of kind of elements should be faster
  momega->buildVertexElements();
  std::vector<GeomElement*>::const_iterator ito, itg;
  std::vector<GeomElement*>::iterator itng = mficdom->geomElements.begin();
  std::set<GeomElement*> elts;
  for (itg=geomElements.begin(); itg!=geomElements.end(); ++itg, ++itng) //loop on elements of gamma
  {
    std::list<GeoNumPair> parelts;
    GeomElement* geltm=(*itg);
    MeshElement* meltm=geltm->meshElement();
    if (meltm==nullptr)  meltm=geltm->buildSideMeshElement();
    GeomElement* gelt=nullptr;
    if (fmap==nullptr) gelt=momega->locate(meltm->vertex(1));
    else // use fmap
    {
      geltm=new GeomElement(**itg); // create mapped element (only nodes are required fot intersect function)
      meltm=geltm->meshElement();
      GeomElement* gelti;
      for (number_t i=0;i<meltm->numberOfNodes();i++)
      {
          Point Q;
          real_t d;
          (*fmap)(meltm->node(i+1),Q);
          if (dmap->useNearest) gelti = momega->nearest(Q,d);  // locate nearest element, Q is the projection on element
          else gelti = momega->locate(Q);    // locate element containing Q
          if (i==0) gelt=gelti;
          meltm->nodes[i]=new Point(Q);
      }
    }
    if (gelt==nullptr) error("free_error", "in MeshDomain::ficticiousDomain, vertex not located in domain "+momega->name());
    std::set<GeomElement*> pickedElts;
    pickedElts.insert(gelt);
    Node<GeomElement> treeElts(gelt,0,0);
    childNodes(&treeElts,momega,geltm,pickedElts);
    Node<GeomElement>* curnode=&treeElts;
    bool firstchild=true, add = true;
    while (curnode!=nullptr)
    {
      if (add)
      {
        elts.insert(curnode->obj_);
        parelts.push_back(GeoNumPair(curnode->obj_,0));
      }
      if (firstchild && curnode->child_!=nullptr)
      {
        curnode=curnode->child_;
        add=true;
      }
      else if (curnode->right_!=nullptr)
      {
        curnode=curnode->right_;
        firstchild=true;
        add=true;
      }
      else
      {
        curnode=curnode->parent_;
        firstchild=false;
        add=false;
      }
    }
    // fill sideToExt
    mficdom->sideToExt[*itg]= parelts;
    if (fmap!=nullptr) // clear temporary gelt and mapped nodes
    {
      for (number_t i=0;i<meltm->numberOfNodes();i++) delete meltm->nodes[i];
      delete geltm;
    }
  }

  mficdom->geomElements.assign(elts.begin(),elts.end());
  mficdom->extensionType = _ficticiousExtension;
  trace_p->pop();
  return *mficdom;
}

//!< remove elements (set) from current domain, creating new domain if different from current
// use set<GeomElement*> to search fast common GeomElements
GeomDomain& MeshDomain::removeElts(const std::set<GeomElement*>& elts, const string_t& namedom) const
{
  trace_p->push("MeshDomain::removeElts");
  std::list<number_t> index;
  std::vector<GeomElement*>::const_iterator itg=geomElements.begin();
  std::set<GeomElement*>::const_iterator itse=elts.end();
  number_t i=0;
  for (;itg!=geomElements.end();++itg,i++)
    if (elts.find(*itg)==itse) index.push_back(i);
  if (index.size()==geomElements.size()) {trace_p->pop();return const_cast<MeshDomain&>(*this);}  //nothing to remove
  // ceate new domain
  string_t na=namedom;
  if (na=="") na=name()+"_remove_elts";
  GeomDomain* dom=new GeomDomain(*mesh(), na, dim(),"built by MeshDomain::removeElts(set<GeomElement*>)");
  MeshDomain* mdom=dom->meshDomain();
  std::vector<GeomElement*>& gelts=mdom->geomElements;
  gelts.reserve(index.size());
  std::list<number_t>::iterator itn= index.begin(), itne=index.end();
  for (; itn!=itne; ++itn) gelts.push_back(geomElements[*itn]);
  //inherited properties
  mdom->shapeTypes=shapeTypes;      //same shapes
  mdom->parentSidesUptodate=parentSidesUptodate; ;
  mdom->normalOrientationRule=normalOrientationRule;
  mdom->jacobianComputed=jacobianComputed;
  mdom->diffEltComputed=diffEltComputed;
  mdom->normalComputed=normalComputed;
  mdom->inverseJacobianComputed=inverseJacobianComputed;
  mdom->useColor=useColor;
  mdom->isSidesOf_p=isSidesOf_p;
  if (vertexElements.size()>0) mdom->buildVertexElements();
  if (!kdtree.isVoid()) mdom->buildKdTree();
  //extensionof_p, extensionin_p, dualCrackDomain_p, extensionType  NOT UPDATED, may be it should be
  trace_p->pop();
  return *dom;
}

//!< remove elements of dom from current domain, creating new domain if different from current
GeomDomain& MeshDomain::removeElts(const GeomDomain& dom, const string_t& namedom) const
{
  if (dom.domType()!=_meshDomain)
    error("free_error", "MeshDomain::removeElts requires a meshdomain");
  const MeshDomain* mdom=dom.meshDomain();
  std::set<GeomElement*> elts;
  std::vector<GeomElement*>::const_iterator itg=mdom->geomElements.begin(), itge=mdom->geomElements.end();
  for (;itg!=itge; ++itg) elts.insert(*itg);
  GeomDomain& ndom=const_cast<GeomDomain&>(removeElts(elts,namedom));
  if (&ndom!=this && namedom=="") ndom.domainInfo().name=name()+" - "+dom.name();
  return ndom;
}

void MeshDomain::buildPartitionData(const std::vector<Parameter>& ps)
{
  this->partitionData_p=new PartitionData(*this, ps);
}

//!< create the domain made of dom1 elements that are not in dom2
GeomDomain& operator-(const GeomDomain& dom1, const GeomDomain& dom2)
{
  return dom1.removeElts(dom2,dom1.name()+" - "+dom2.name());
}

//!< internal tool used by MeshDomain::ficticiousDomain
void childNodes(Node<GeomElement>* curnode, const MeshDomain* momega, GeomElement* gelt, std::set<GeomElement*>& pickedElts)
{
  //make list of available neighbour elements of current element
  MeshElement* melt = (curnode->obj_)->meshElement();
  std::set<GeomElement*> availableNeighbours;
  std::set<GeomElement*>::iterator ita;
  for (number_t i=1; i<=melt->numberOfVertices(); i++)
  {
    std::list<GeomElement*>& ani=momega->vertexElements[melt->vertex(i)];
    std::list<GeomElement*>::iterator ite=ani.begin(),itee=ani.end();
    for (; ite!=itee; ++ite)
      if (pickedElts.find(*ite)==pickedElts.end())
      {
        pickedElts.insert(*ite);
        if (intersect(*gelt,**ite)) availableNeighbours.insert(*ite);
      }
  }
  //create children
  bool firstchild=true;
  for (ita=availableNeighbours.begin(); ita!=availableNeighbours.end(); ++ita)
  {
    if (firstchild)
    {
      curnode->child_ = new Node<GeomElement>(*ita,curnode,curnode->level_+1);
      curnode=curnode->child_;
      firstchild=false;
      childNodes(curnode,momega,gelt,pickedElts);
    }
    else
    {
      curnode->right_ = new Node<GeomElement>(*ita,curnode->parent_,curnode->level_);
      curnode=curnode->right_;
      childNodes(curnode,momega,gelt,pickedElts);
    }
  }
}

//!< test intersection of E1 with E2 with a tolerance (default is theEpsilon)
bool intersect(const GeomElement& E1, const GeomElement& E2, real_t tol)
{
  MeshElement* mE1=const_cast<MeshElement*>(E1.meshElement());
  if (mE1==nullptr)  mE1=E1.buildSideMeshElement();
  MeshElement* mE2=const_cast<MeshElement*>(E2.meshElement());
  if (mE2==nullptr)  mE2=E2.buildSideMeshElement();
  Point I;
  switch (E1.shapeType())
  {
    case _segment:
    {
      switch (E2.shapeType())
      {
        case _segment:
          return doesSegmentCrossesSegment(mE1->vertex(1),mE1->vertex(2),mE2->vertex(1),mE2->vertex(2),tol);
          break;
        case _triangle:
          return doesSegmentIntersectsTriangle(mE1->vertex(1),mE1->vertex(2),mE2->vertex(1),mE2->vertex(2), mE2->vertex(3),tol);
          break;
        case _quadrangle:
          return doesSegmentIntersectsQuadrangle(mE1->vertex(1),mE1->vertex(2),mE2->vertex(1),mE2->vertex(2), mE2->vertex(3),mE2->vertex(4),tol);
          break;
        default:
          break;
      }
      break;
    }
    case _triangle:
    {
      switch (E2.shapeType())
      {
        case _triangle:
          return doesTriangleIntersectsTriangle(mE1->vertex(1),mE1->vertex(2),mE1->vertex(3),mE2->vertex(1),mE2->vertex(2), mE2->vertex(3),tol);
          break;
        case _quadrangle:
          return doesTriangleIntersectsQuadrangle(mE1->vertex(1),mE1->vertex(2),mE1->vertex(3),mE2->vertex(1),mE2->vertex(2), mE2->vertex(3),mE2->vertex(4),tol);
          break;
        default:
          break;
      }
      break;
    }
    case _quadrangle:
    {
      switch (E2.shapeType())
      {
        case _triangle:
          return doesTriangleIntersectsQuadrangle(mE2->vertex(1),mE2->vertex(2),mE2->vertex(3), mE1->vertex(1),mE1->vertex(2), mE1->vertex(3),mE1->vertex(4),tol);
          break;
        case _quadrangle:
          return doesQuadrangleIntersectsQuadrangle(mE1->vertex(1),mE1->vertex(2),mE1->vertex(3),mE1->vertex(4),mE2->vertex(1),mE2->vertex(2), mE2->vertex(3),mE2->vertex(4),tol);
          break;
        default:
          break;
      }
      break;
    }
    default:
      break;
  }
  error("free_error", "intersection of "+words("shape",E2.shapeType())+" and "+ words("shape",E2.shapeType())+  "not yet available");
  return false;
}

//test if point lies in element
bool pointInElement(const Point& P, const GeomElement& E, real_t tol)
{
  MeshElement* mE=const_cast<MeshElement*>(E.meshElement());
  if (mE==nullptr) mE=E.buildSideMeshElement();
  switch (E.shapeType())
  {
    case _segment: return isPointInSegment(P, mE->vertex(1),mE->vertex(2),tol);
                   break;
    case _triangle:return isPointInTriangle(P, mE->vertex(1),mE->vertex(2),mE->vertex(3),tol);
                   break;
    case _quadrangle:
    {
      if (isPointInTriangle(P, mE->vertex(1),mE->vertex(2),mE->vertex(3),tol)) return true;
      return isPointInTriangle(P, mE->vertex(1),mE->vertex(3),mE->vertex(4),tol);
      break;
    }
    default:
      break;
  }
  error("free_error", "inElement function for "+ words("shapetype",E.shapeType())+  "not yet available");
  return false;
}

//find extended domain of a side domain, return 0 if not found
const GeomDomain* GeomDomain::extendedDomain(bool useVertex, const GeomDomain& omega) const
{
  string_t naext = name();
  if (useVertex) naext+="_vextension";
  else          naext+="_sextension";
  if (omega.domType()==_meshDomain)  naext+="_"+omega.name();
  std::vector<const GeomDomain*>::iterator it;
  for (it = theDomains.begin(); it != theDomains.end(); it++)
    if ((*it)->name() == naext) return *it;
  return nullptr;
}

/*! build VertexElements map from geomElements
  for each vertex v the list of elements having v as vertex
*/
void MeshDomain::buildVertexElements() const
{
  if (vertexElements.size()!=0) return; // already constructed
  std::map<Point,std::list<GeomElement*> >::iterator itmpl;
  std::vector<GeomElement*>::const_iterator itg=geomElements.begin();
  for (; itg!=geomElements.end(); itg++)
  {
    const Mesh* mp = (*itg)->meshP();
    std::vector<number_t> vn=(*itg)->vertexNumbers();
    std::vector<number_t>::iterator itn=vn.begin();
    for (; itn!=vn.end(); itn++)
    {
      Point P=mp->nodes[*itn-1];
      itmpl=vertexElements.find(P);
      if (itmpl==vertexElements.end())
        vertexElements.insert(std::make_pair(P,std::list<GeomElement*>(1,*itg)));
      else itmpl->second.push_back(*itg);
    }
  }
}

/*! build KdTree structure: "ordered vertices"
*/
void MeshDomain::buildKdTree() const
{
  if (!kdtree.isVoid()) return;
  if (vertexElements.size()==0) buildVertexElements();
  std::map<Point,std::list<GeomElement*> >::const_iterator itmpl=vertexElements.begin();
  for (; itmpl!=vertexElements.end(); itmpl++) kdtree.insert(itmpl->first);
}

// locate element containing point pt, silent mode -> do not emit warning message when fails (default = false)
GeomElement* MeshDomain::locate(const Point& pt, bool silent) const
{
  if (dim()==0)  //point domain
  {
    if (norm(nodes()[0]-pt)<theTolerance) return geomElements[0];
    return nullptr;
  }

  //dim>1
  if (kdtree.isVoid()) buildKdTree();  //build kdtree on fly (done if not allocated)
  const Point* pl=kdtree.searchNearest(pt);
  if (pl==nullptr) //abnormal failure
  {
    if (!silent) warning("free_warning","MeshDomain::locate, searchNearest fails, point"+tostring(pt)+" not found in domain "+name());
    return nullptr;
  }
  //search GeomElement containing pt
//  std::map<Point,std::list<GeomElement*> >::const_iterator itmpl=vertexElements.find(*pl);
//  if (itmpl==vertexElements.end()) //abnormal failure
//  {
//    theCout<<std::setprecision(fullPrec)<<"Point::tolerance="<<Point::tolerance<<eol;
//    theCout<<std::setprecision(fullPrec)<<"MeshDomain::locate point: "<<pt<<", nearest vertex: "<<(*pl)<<eol;
//    theCout<<std::setprecision(fullPrec)<<"lower_bound: "<<vertexElements.lower_bound(*pl)->first<<eol;
//    theCout<<std::setprecision(fullPrec)<<vertexElements<<eol;
//    where("MeshDomain::locate"); error("abnormal_failure");
//  }
  std::map<Point,std::list<GeomElement*> >::const_iterator itmpl=vertexElements.lower_bound(*pl);
  Point q=itmpl->first;
  while (q!=*pl && itmpl!=vertexElements.end())
  {
    itmpl++;
    if (itmpl==vertexElements.end()) //abnormal failure
    {
      theCout<<std::setprecision(fullPrec)<<"Point::tolerance="<<Point::tolerance<<eol;
      theCout<<std::setprecision(fullPrec)<<"MeshDomain::locate point: "<<pt<<", nearest vertex: "<<(*pl)<<eol;
      theCout<<std::setprecision(fullPrec)<<"lower_bound: "<<vertexElements.lower_bound(*pl)->first<<eol;
      theCout<<std::setprecision(fullPrec)<<vertexElements<<eol;
      where("MeshDomain::locate"); error("abnormal_failure");
    }
    q=itmpl->first;
  }

  std::list<GeomElement*>::const_iterator itl=itmpl->second.begin();
  for (; itl!=itmpl->second.end(); ++itl)
  {
    if ((*itl)->contains(pt)) return *itl;
  }
  //no container element found, extend the searching to the next layer of elements
  std::set<GeomElement*> checkElts;
  for (itl=itmpl->second.begin(); itl!=itmpl->second.end(); ++itl)
    checkElts.insert(*itl);
  std::map<Point,std::list<GeomElement*> >::const_iterator itmpln;
  std::list<GeomElement*>::const_iterator itln;
  for (itl=itmpl->second.begin(); itl!=itmpl->second.end(); ++itl)
  {
    MeshElement* melt=(*itl)->meshElement();
    for (number_t i=1; i<=melt->numberOfVertices(); i++)
    {
      itmpln = vertexElements.find(melt->vertex(i));
      for (itln=itmpln->second.begin(); itln!=itmpln->second.end(); ++itln)  //next layer
      {
        if ((checkElts.find(*itln)==checkElts.end()) && (*itln)->contains(pt))
          return *itl;
        checkElts.insert(*itln);
      }
    }
  }
  // fails to find ...
  if (!silent)
  {
    string_t mes = "MeshDomain::locate, point"+tostring(pt)+" not found in elements:\n";
    warning("free_warning",mes);
  }
  return nullptr;
}

// build the sideToExt map, that is the map related a side element s to the set of elements having s as side
//    - this function builds the extended domain if it does not exist,
//      if omega is specified the extension is restricted to elements of omega
//    - if domain is not a side domain, function fails !
const std::map<GeomElement*, std::list<GeoNumPair> >& MeshDomain::buildSideToExt(const GeomDomain& omega) const
{
  if (sideToExt.size()>0)
    return sideToExt;
  GeomDomain& dom_ext = extendDomain(false, omega);  //build extension from sides if not exist
  std::set<GeomElement*> elts_ext(dom_ext.meshDomain()->geomElements.begin(),dom_ext.meshDomain()->geomElements.end());
  std::vector<GeomElement*>::const_iterator ite=geomElements.begin();
  for (; ite!=geomElements.end(); ++ite)  //loop on side elements
  {
    std::vector<GeoNumPair>& parents=(*ite)->parentSides(); //parents of side element
    std::vector<GeoNumPair>::iterator itgn=parents.begin();
    for (; itgn!=parents.end(); ++itgn)
      if (elts_ext.find(itgn->first)!=elts_ext.end())
        sideToExt[*ite].push_back(*itgn);  //keep only parents in extension
  }
  return sideToExt;
}

// locate element nearest point pt
// on output: pt is the projection on nearest element of input point
//            md is the distance to the nearest element
GeomElement* MeshDomain::nearest(Point& pt, real_t& md) const
{
  if (dim()==0)  //point domain
  {
    md =norm(nodes()[0]-pt);
    return geomElements[0];
  }
  if (kdtree.isVoid()) buildKdTree();  //build kdtree on fly
  dimen_t d=kdtree.dim();
  Point q(std::vector<real_t>(d,0.));
  for (dimen_t i=0; i<pt.dim(); i++) q[i]=pt[i];
  const Point* pl=kdtree.searchNearest(q);
  if (pl==nullptr) return nullptr;

  std::map<Point,std::list<GeomElement*> >::const_iterator itmpl=vertexElements.find(*pl);
  if (itmpl==vertexElements.end())
  {
    where("MeshDomain::nearest");
    error("abnormal_failure");
  }

  //find nearest element using projection on element
  real_t t;
  md=theRealMax;
  Point pr=pt;
  GeomElement* ge=nullptr;
  std::list<GeomElement*>::const_iterator itl=itmpl->second.begin();
  for (; itl!=itmpl->second.end(); itl++)
  {
    //compute projection on element
    Point q = (*itl)->projection(pt,t);
    if (t<md)
    {
      md=t;
      pr=q;
      ge=*itl;
      if (md<theTolerance) return ge;  //inside element
    }
  }

  //if md>0 extend search to neighbor elements
  if (md>theTolerance)
  {
    std::set<GeomElement*> checkElts;
    for (itl=itmpl->second.begin(); itl!=itmpl->second.end(); ++itl)  checkElts.insert(*itl);
    std::map<Point,std::list<GeomElement*> >::const_iterator itmpln;
    std::list<GeomElement*>::const_iterator itln;
    for (itl=itmpl->second.begin(); itl!=itmpl->second.end(); ++itl)
    {
      MeshElement* melt=(*itl)->meshElement();
      for (number_t i=1; i<=melt->numberOfVertices(); i++)
      {
        itmpln = vertexElements.find(melt->vertex(i));
        for (itln=itmpln->second.begin(); itln!=itmpln->second.end(); ++itln)  //next layer
        {
          if (checkElts.find(*itln)==checkElts.end())
          {
             checkElts.insert(*itln);
             Point q = (*itln)->projection(pt,t);
             if (t<md)
             {
                md=t;
                pr=q;
                ge=*itln;
                if (md<theTolerance) return ge; //inside element
             }
          }
        }
      }
    }
  }
  pt=pr;
  return ge;
}

//!< build isoGeometric nodes in parameters domain
/*
  domain should have a parametrization G : parameters domain -> domain
  for each element build
     - isoNodes (invG(Mi)) in parameters domain
     - parametrization index (1 when only one map)
  Note : isoNodes not rebuilt if already exist
*/
void MeshDomain::buildIsoNodes() const
{
   if (!hasParametrization()) error("free_error","MeshDomain::buildIsoNodes() requires a parametrized domain");
   std::ofstream fout;
   const Parametrization& par = parametrization();
   if(par.nbOfMaps>1) // use extended method
   {
       buildIsoNodesExt();
       return;
   }
   std::vector<GeomElement*>::const_iterator ite=geomElements.begin();
   if ((*ite)->meshElement()->isoNodes.size()>0) return; // already built
   for (; ite!=geomElements.end(); ++ite)  //loop on  elements
   {
      MeshElement* melt= (*ite)->meshElement();
      if (melt==nullptr) melt = (*ite)->buildSideMeshElement();
      std::vector<Point*>& nodes = melt->nodes;
      melt->isoNodes.resize(nodes.size());
      auto iti = melt->isoNodes.begin();
      for (auto itn=nodes.begin(); itn!=nodes.end(); ++itn, ++iti)
         *iti = new Point(par.toParameter(**itn));
//    check iso element coordinates, may be change periodic components if crossing the periodic cut
      for (number_t i=0;i<par.dimg;i++)
      {
        real_t ti=par.periods[i], ti2=ti*0.5;
        if (ti!=0)  // check if coordinates 'cross' component cut, assume [0,ti] interval
        {
           real_t tmin=ti, tmax=0., t;
           for (iti=melt->isoNodes.begin();iti!=melt->isoNodes.end(); ++iti)
           {
               t=(**iti)[i];
               tmin=std::min(tmin,t);
               tmax=std::max(tmax,t);
           }
           if (tmax-tmin>ti2) // change coordinates greater than ti/2, substracting period ti
           {
             for (iti=melt->isoNodes.begin();iti!=melt->isoNodes.end(); ++iti)
                if ((**iti)[i]>ti2) (**iti)[i]-=ti;
           }
        }
      }
   }
}
//!< build isoGeometric nodes in parameters domain
/*
  domain should have a parametrization G : parameters domain -> domain
  for each element build
     - isoNodes (invG(Mi)) in parameters domain
     - parametrization index (1 when only one map)
  Note : isoNodes not rebuilt if already exist
*/
void MeshDomain::buildIsoNodesExt() const
{
   if (!hasParametrization()) error("free_error","MeshDomain::buildIsoNodes() requires a parametrized domain");
   std::ofstream fout;
   const Parametrization& par = parametrization();
   auto ite=geomElements.begin();
   if ((*ite)->meshElement()->isoNodes.size()>0) return; // already built
   for (; ite!=geomElements.end(); ++ite)  //loop on  elements
   {
      MeshElement* melt= (*ite)->meshElement();
      if (melt==nullptr) melt = (*ite)->buildSideMeshElement();
      std::vector<Point*>& nodes = melt->nodes;
      melt->isoNodes.resize(nodes.size());
      par.toParameters(nodes, melt->mapIndex, melt->isoNodes);
      thePrintStream<<" nodes=("<<*nodes[0]<<","<<*nodes[1]<<","<<*nodes[2]<<")"
                    <<" isonodes=("<<*melt->isoNodes[0]<<","<<*melt->isoNodes[1]<<","<<*melt->isoNodes[2]<<")"
                    <<" mapindex="<<melt->mapIndex<<eol;
   }
   //build the set sideMapTransition
   Mesh* msh = const_cast<Mesh*>(mesh());
   if(msh->sides().size()==0) msh->buildSides();
    for (ite=geomElements.begin(); ite!=geomElements.end(); ++ite)  //loop on  elements
    {
       MeshElement* melt= (*ite)->meshElement();
       for(int s=0;s<(*ite)->numberOfSides();s++)
       {
           GeoNumPair gn=(*ite)->elementSharingSide(s+1);
           if(gn.first!=nullptr && gn.first->meshElement()->mapIndex!=melt->mapIndex)
             melt->sideMapTransition.insert(s);
       }
    }
}

//!< export iso edges in a raw format, if isoNodes have been already built
/* if filename is not empty, create the file containing for each side  isonodes in parametrization space and n points of the transformed edge :
   one file by maps "filename_map_i.dat"
    u1_1 v1_1 u1_2 v1_2 u1_3 v1_3 x1_1 y1_1 [z1_1]... x1_n y1_n [z1_n]    //first edge
    ...
    up_1 vp_1 up_2 vp_2 up_3 vp_3 xp_1 yp_1 [zp_1]... xp_n yp_n [zp_n]    // edge p

    Note : restrict to segment or triangle in 2D or 3D
*/
void MeshDomain::exportIsoNodes(const string_t &filename, number_t n) const
{
  auto ite = geomElements.begin();
  if((*ite)->meshElement()->isoNodes.size()==0)  // no isonodes
    warning("free_error","call MeshDomain::exportIsoNodes() but no isoNodes built, no export");
  std::ofstream fout;
  if(filename=="")
    warning("free_error","call MeshDomain::exportIsoNodes() with no filename, no export");
  if(n<2)
    warning("free_error","call MeshDomain::exportIsoNodes() nb points n=0 or 1, no export");
  const Parametrization& par = parametrization();
  number_t nbmap = par.nbOfMaps;
  dimen_t d=spaceDim();
  real_t u1,v1,u2,v2, s, ds=1./(n-1);
  Point p;
  for(int m=1; m<=nbmap; m++)
  {
    fout.open(filename+"_map_"+tostring(m)+".dat");
    for(ite = geomElements.begin(); ite!=geomElements.end(); ++ite)   //loop on  elements
    {
      MeshElement* melt= (*ite)->meshElement();
      if(melt->mapIndex==m)
      {
        auto itn=melt->isoNodes.begin();
        if(melt->isoNodes.size()>2)  //triangle
        {
          u1=(**itn)[0]; v1=(**itn)[1]; itn++;
          u2=(**itn)[0]; v2=(**itn)[1];
          fout<<u1<<" "<<v1<<" "<<u2<<" "<<v2<<" ";
          for(auto i=0; i<n; i++)
          {
            s=i*ds;
            p=par(u1+s*(u2-u1),v1+s*(v2-v1));
            fout<<p[0]<<" "<<p[1]<<" ";
            if(d==3) fout<<p[2]<<" ";
          }
          fout<<eol;
          u1=u2; v1=v2; itn++;
          u2=(**itn)[0]; v2=(**itn)[1];
          fout<<u1<<" "<<v1<<" "<<u2<<" "<<v2<<" ";
          for(auto i=0; i<n; i++)
          {
            s=i*ds;
            p=par(u1+s*(u2-u1),v1+s*(v2-v1));
            fout<<p[0]<<" "<<p[1]<<" ";
            if(d==3) fout<<p[2]<<" ";
          }
          fout<<eol;
          u1=u2; v1=v2;
          u2=(**melt->isoNodes.begin())[0];
          v2=(**melt->isoNodes.begin())[1];
          fout<<u1<<" "<<v1<<" "<<u2<<" "<<v2<<" ";
          for(auto i=0; i<n; i++)
          {
            s=i*ds;
            p=par(u1+s*(u2-u1),v1+s*(v2-v1));
            fout<<p[0]<<" "<<p[1]<<" ";
            if(d==3) fout<<p[2]<<" ";
          }
          fout<<eol;
        }
        else // segment
        {
          u1=(**itn)[0]; itn++;
          u2=(**itn)[0];
          fout<<u1<<" "<<u2<<" ";
          for(auto i=0; i<n; i++)
          {
            s=i*ds;
            p=par(u1+s*(u2-u1));
            fout<<p[0]<<" "<<p[1]<<" ";
            if(d==3) fout<<p[2]<<" ";
          }
          fout<<eol;
        } // end if
      }
    }
    fout.close();
  }
}

// locate elements containing a given point
// differs from the MeshDomain::nearest function that finds the nearest element
std::list<GeomElement*> MeshDomain::elementsCloseTo(Point& pt) const
{
  std::list<GeomElement*> gelts;
  real_t t;
  if (dim()==0)  //for boundary of 1D domain !
  {
    if (norm(nodes()[0]-pt)<theTolerance)
      gelts.push_back(geomElements[0]);
    return gelts;
  }
  if (kdtree.isVoid()) buildKdTree();  //build kdtree on fly
  dimen_t d=kdtree.dim();
  Point q(std::vector<real_t>(d,0.));
  for (dimen_t i=0; i<pt.dim(); i++) q[i]=pt[i];
  const Point* pl=kdtree.searchNearest(q);
  if (pl==nullptr)
  {
    where("MeshDomain::elementsCloseTo");
    error("abnormal_failure");
  }
  std::map<Point,std::list<GeomElement*> >::const_iterator itmpl=vertexElements.find(*pl);
  if (itmpl==vertexElements.end())
  {
    where("MeshDomain::elementsCloseTo");
    error("abnormal_failure");
  }
  std::list<GeomElement*>::const_iterator itl=itmpl->second.begin();
  for (; itl!=itmpl->second.end(); itl++)
  {
    (*itl)->projection(pt,t);
    if (t<theTolerance) gelts.push_back(*itl);
  }
  return gelts;
}

//handling point location error
void locateError(bool warn, const string_t& com, const GeomDomain& dom, const Point& P, real_t d)
{
  if (warn) warning("node_outside",com, P.toString(),dom.name(),d);
  else error("node_not_found",com, P.toString(),dom.name(),d);
}


/*---------------------------------------------------------------------------------------------
 set measures and orientation of elements of a meshdomain
 ---------------------------------------------------------------------------------------------*/
void MeshDomain::buildGeomData() const
{
  trace_p->push("MeshDomain::buildGeomData");
  std::vector<GeomElement*>::const_iterator ite = geomElements.begin();
  for (; ite != geomElements.end(); ite++)
  {
    MeshElement* me=(*ite)->meshElement();
    if (me==nullptr)
      me = (*ite)->buildSideMeshElement();
    me->computeMeasures();
    me->computeOrientation();
  }

  //manifold, boundary, interface, compute orientation of element by a global analysis of main domain elements
  if (dim() == spaceDim() -1)
    setNormalOrientation();  //use default _towardsInfinite for manifold or interface, _outwardsDomain for boundary
  orientationComputed = true;
  jacobianComputed = true;
  trace_p->pop();
}

/*! set orientation of meshElements of a manifold/boundary say a mesh domain of dimension SpaceDim-1
    - update the member data normalDefinition
    - compute or update the orientation of geometric elements of the current meshdomain

    if the domain is not a domain of dimension SpaceDim-1, nothing is done
    if ort = _towardsInfinite (resp._outwardsInfinite), geom is not used
    if ort = _towardsDomain (resp._outwardsDomain), geom has to be a parent domain of boundary domain
    if orientations have never been computed, compute them using setOrientationForManifold or setOrientationForBoundary
    if orientations are currently computed, detect if the orientations have to be reversed

    for instance , consider the following domains

         |------------------------------|
         |                              |---->  Gamma.setNormalOrientation(_towardsInfinite)
         |         |------------|       |     = Gamma.setNormalOrientation(_outwardsDomain,Omega2)
         |         |            |       |
         | Omega2  |   Omega1   |       |
         |         |            |------------>  Sigma.setNormalOrientation(_towardsInfinite)
         |         |------------|       |     = Sigma.setNormalOrientation(_outwardsDomain,Omega1)
         |              Sigma           |     = Sigma.setNormalOrientation(_towardsDomain, Omega2)
         |------------------------------|
                        Gamma

     Default rule (ort =_undefOrientationType) :
          boundary -> _outwardsDomain   (well adapted to FE-IR)
          manifold -> _towardsInfinite  (well adapted to BEM)

    WARNING:  - For a non closed manifold/boundary, setNormalOrientation(_towardsInfinite/outwardsInfinite)
                 constructs consistent normal orientations but not predictable, use additional "interior" point
               - In case of an interface, _outwardsDomain with no domain specified may give inconsistent normal orientations

*/
void MeshDomain::setNormalOrientation(OrientationType ort, const GeomDomain&  geom) const
{
  setNormalOrientation(ort, &geom);
}

void MeshDomain::setNormalOrientation(OrientationType ort, const Point&  P) const
{
  setNormalOrientation(ort, 0, &P);
}

void MeshDomain::setNormalOrientation(OrientationType ort, const GeomDomain*  geom, const Point* p) const
{
  trace_p->push("MeshDomain::setNormalOrientation");
  if (dim() != spaceDim()-1)
  {
    warning("free_warning", "in setNormalOrientation, domain "+name()+" is not a manifold, nothing is done");
    trace_p->pop();
    return;
  }
  if (ort==_undefOrientationType)
  {
    if (isInterface() || !isSideDomain())
      ort=_towardsInfinite; //interface or manifold: choose _towardsInfinite
    else
      ort=_outwardsDomain;  //boundary: choose _outwardsDomain
  }

  if (!orientationComputed)
  {
    normalOrientationRule=std::pair<OrientationType, const GeomDomain*>(ort, geom);
    switch (ort)
      {
      case _towardsInfinite:
      case _outwardsInfinite:
        setOrientationForManifold(ort,p);
        break;
      case _towardsDomain:
      case _outwardsDomain:
        setOrientationForBoundary(ort, geom);
        break;
      default:
        error("not_handled", "MeshDomain::setNormalOrientation(OrientationType, Domain*)");
      }
    trace_p->pop();
    return;
  }

  //orientations have already been computed, detect if orientations have to be reversed
  switch (normalOrientationRule.first)
  {
    case _towardsInfinite:
      switch (ort)
      {
        case _outwardsInfinite:
          reverseOrientations();
          break;
        case _towardsDomain:
        case _outwardsDomain:
          setOrientationForBoundary(ort, geom);
          break;
        case _towardsInfinite:
          break;
        default:
          error("not_handled", "MeshDomain::setNormalOrientation(OrientationType, Domain*)");
      }
      break;
    case _outwardsInfinite:
    {
      switch (ort)
      {
        case _towardsInfinite:
          reverseOrientations();
          break;
        case _towardsDomain:
        case _outwardsDomain:
          setOrientationForBoundary(ort, geom);
          break;
        case _outwardsInfinite:
          break;
        default:
          error("not_handled", "MeshDomain::setNormalOrientation(OrientationType, Domain*)");
      }
      break;
    }
    case _towardsDomain:
    {
      switch (ort)
      {
        case _outwardsDomain:
          reverseOrientations();
          break;
        case _towardsInfinite:
        case _outwardsInfinite:
          setOrientationForManifold(ort,p);
          break;
        case _towardsDomain:
          break;
        default:
          error("not_handled", "MeshDomain::setNormalOrientation(OrientationType, Domain*)");
      }
      break;
    }
    case _outwardsDomain:
    {
      switch (ort)
      {
        case _towardsDomain:
          reverseOrientations();
          break;
        case _towardsInfinite:
        case _outwardsInfinite:
          setOrientationForManifold(ort,p);
          break;
        case _outwardsDomain:
          break;
        default:
          error("not_handled", "MeshDomain::setNormalOrientation(OrientationType, Domain*)");
      }
      break;
    }
    default:
      error("not_handled", "MeshDomain::setNormalOrientation(OrientationType, Domain*)");
  }
  normalOrientationRule=std::pair<OrientationType,  const GeomDomain*>(ort, geom);
  orientationComputed = true;
  trace_p->pop();
}

/*! set orientation of meshElements of a boundary of a domain, i.e a sign to apply to normal computed from the jacobian
    according to an orientation rule
    -  ort: one of direction _towardsInfinite, _outwardsInfinite,_towardsDomain, _outwardsDomain
    -  geom: pointer to (parent) domain concerned by orientation
              if geom = 0 , compute in/outward normal from first element parent
*/

void MeshDomain::setOrientationForBoundary(OrientationType ort, const GeomDomain*  geom) const
{
  trace_p->push("MeshDomain::setOrientationForBoundary");
  if (dim() != spaceDim()-1) { error("domain_space_mismatch_dims", dim(), spaceDim()-1); }
  if (ort!=_towardsDomain && ort!=_outwardsDomain) { error("domain_orientation_only", words("orientation type", ort)); }
  if (spaceDim() == 1)
  {
    trace_p->pop();  //nothing to do
    return;
  }
  if (geomElements.size()==0)
  {
    trace_p->pop();  //void domain
    return;
  }
  const MeshDomain* mdom = nullptr;
  if (geom!=nullptr)
    mdom = geom->meshDomain();
  std::set<GeomElement*> index_elt;
  if (mdom!=nullptr)
    index_elt.insert(mdom->geomElements.begin(),mdom->geomElements.end()); //create index of GeomElement*

  std::vector<GeomElement*>::const_iterator itg=geomElements.begin();
  for (; itg!=geomElements.end(); itg++)
  {
    GeomElement* gelt = *itg;
    if (!gelt->isSideElement())
      error("geoelt_notside");
    //compute normal from jacobian
    MeshElement* melt = gelt->meshElement();
    if (melt == nullptr)
      melt = gelt->buildSideMeshElement();
    if (melt->geomMapData_p == nullptr)
      melt->geomMapData_p=new GeomMapData(melt, Point(0., 0., 0.));
    GeomMapData* gp = melt->geomMapData_p;
    gp->computeJacobianMatrix(Point(0., 0., 0.));
    gp->computeNormalVector();
    //compute outward normal from parent
    std::vector<real_t> ns;
    if (geom==nullptr) //take first parent
    {
      if (gelt->numberOfParents()>1)
        warning("free_warning"," in MeshDomain::setOrientationForBoundary , element has more than one parent, orientation may be hazardous");
      std::pair<GeomElement*, number_t> gn =gelt->parentSide(0);   // ### WARNING: consider only the first parent, may be hazardous for interface
      ns = gn.first->normalVector(gn.second);    // compute outward normal vector from parent
    }
    else //locate parent in geom
    {
      std::vector<GeoNumPair>& pars = gelt->parentSides();
      std::vector<GeoNumPair>::iterator itps=pars.begin();
      bool notFound=true;
      GeomElement* parelt = nullptr;
      number_t side = 0;
      for (; itps!=pars.end() && notFound; ++itps)
        {
          parelt = itps->first;
          side = itps->second;
          if (index_elt.find(parelt)!=index_elt.end())
            notFound=false;
        }
      if (notFound)
        error("geoelt_parent_not_found",geom->name());
      ns = parelt->normalVector(side);    // compute outward normal vector from parent
    }

    //set orientation
    real_t norm=0.;
    norm = inner_product(gp->normalVector.begin(), gp->normalVector.end(), ns.begin(), norm);
    if (norm<0) melt->orientation=-1;
    else       melt->orientation=1;
    if (ort==_towardsDomain)  melt->orientation*=-1;  //reverse orientation
    gp->normalize();  //normalize using orientation
  }

  normalComputed = true;
  orientationComputed = true;
  trace_p->pop();
  return;
}

/*! set orientation of meshElements of a manifold say a mesh domain of dimension SpaceDim-1
    orientation (-1 or 1) is chosen such as the orientation * normal vector is an outward normal, say going to infinite
    if  ort = _outwardsInfinite, the orientations are reversed
    P is an additional point used to specify where is the "interior" of an open manifold

                                      |
                                      |
                               P      |-----> towardsInfinite related to P
                                      |
                                      |
                                      |

    This process does not suppose that the manifold is a boundary of domain
      - first step: compute one normal by element (given by cross product of two first columns of jacobian)
      - second step: find a path in element list such as two consecutive elements share a side
                      may be more than one path (case of non connected components)
      - pick one normal (by connected component) and decide the outward orientation by analysis the intersection of a half line and all other elements
      - travel this path and choose same orientation for all element using a continuity method based on geometric computation

    For a non closed manifold, this process constructs consistent normal orientations but not predictable. Use additional point P

    NOTE: to be changed in future by numbering analysis

*/
void MeshDomain::setOrientationForManifold(OrientationType ort, const Point* p) const
{
  trace_p->push("MeshDomain::setOrientationForManifold");
  if (dim() != spaceDim()-1) { error("domain_space_mismatch_dims", dim(), spaceDim()-1); }
  if (ort!=_towardsInfinite && ort!=_outwardsInfinite) { error("infinite_orientation_only", words("orientation type", ort)); }
  if (spaceDim() == 1)
  {
    trace_p->pop();  //nothing to do
    return;
  }
  if (geomElements.size()==0)
  {
    trace_p->pop();  //void domain
    return;
  }

  //no parent mesh, use process for real manifolds
  std::map<GeomElement*,bool> markElt;
  std::map<GeomElement*,bool>::iterator itm;
  std::map<string_t, std::list<GeomElement*> > sideIndex; //to store side of elements
  std::map<string_t, std::list<GeomElement*> >::iterator itsn;
  std::map<GeomElement*, Vector<real_t> > normalVectors;  //normal vectors indexed by GeomElement pointer
  //compute one normal per element and build temporary structures (side index and marked elements)
  std::vector<GeomElement*>::const_iterator itg=geomElements.begin();
  for (; itg!=geomElements.end(); itg++)
  {
    MeshElement* melt = (*itg)->meshElement();
    if (melt==nullptr)
      melt = (*itg)->buildSideMeshElement();
    melt->orientation = 1;
    GeomMapData* gp=new GeomMapData((*itg)->meshElement(), Point(0., 0., 0.));
    gp->computeJacobianMatrix(Point(0., 0., 0.));
    gp->computeNormalVector();
    gp->normalize();
    normalVectors[*itg]=gp->normalVector;
    (*itg)->meshElement()->geomMapData_p = gp;  //keep geomdata in memory

    //build sides of elements
    markElt[*itg]=false;
    for (number_t s = 0; s < (*itg)->numberOfSides(); s++) //loop on element sides
    {
      string_t key = (*itg)->encodeElement(s + 1);  //encode vertex numbers of side s
      itsn = sideIndex.find(key);                 //search side in sideIndex map
      if (itsn == sideIndex.end())
        sideIndex[key]=std::list<GeomElement*>(1,*itg);  //new side found
      else
        itsn->second.push_back(*itg);                    //add current element to side
    }
  }

  normalComputed = true;

  //build element paths
  std::vector< Node<GeomElement>* > paths;
  GeomElement* elt=markElt.begin()->first;
  while (elt!=nullptr)
  {
    markElt[elt]=true;
    Node<GeomElement>* elts= new Node<GeomElement>(elt,0,0);
    //addNode(elts,elt,markElt,sideIndex);  //add nodes recursively
    createPath(elts,markElt,sideIndex);
    paths.push_back(elts);
    elt=nullptr;
    itm=markElt.begin();
    while (itm!=markElt.end() && elt==nullptr)  //find not marked element
    {
      if (!itm->second)
        elt=itm->first;
      else
        itm++;
    }
  }

  //for each path, set normals in the same orientation using continuity method
  std::vector< Node<GeomElement>* >::iterator itp=paths.begin();
  real_t alpha=2*mesh()->geometry_p->boundingBox.diameter();
  for (; itp!=paths.end(); ++itp)
  {
    //find one outward normal
    GeomElement* elt1,*elt2;
    Node<GeomElement>* nelt1 = *itp, *nelt2;
    elt1=nelt1->objectp();
    Vector<real_t>& n1=normalVectors[elt1];
    MeshElement* melt1=elt1->meshElement();
    Point G=melt1->center(), Q=G+Point(alpha*n1);
    Point I,J;
    if (p==nullptr) //use nb of intersection with other elements
    {
      number_t nbi=0;
      for (itg=geomElements.begin(); itg!=geomElements.end(); itg++)
      {
        MeshElement* melt=(*itg)->meshElement();
        if (*itg != elt1)
        {
          if (dim()==1 && intersectionOfSegments(G,Q,melt->vertex(1),melt->vertex(2),I,theTolerance)) ++nbi;
          if (dim()==2 && melt->shapeType()==_triangle
              &&intersectionSegmentTriangle(G,Q,melt->vertex(1),melt->vertex(2),melt->vertex(3),I,J,theTolerance)) ++nbi;
          if (dim()==2 && melt->shapeType()==_quadrangle
              && intersectionSegmentQuadrangle(G,Q,melt->vertex(1),melt->vertex(2),melt->vertex(3),melt->vertex(4),I,J,theTolerance)) ++nbi;
        }
      }
      melt1->orientation=1;
      if (nbi%2 != 0) melt1->orientation = -1;
    }
    else //use P as an "interior" point
    {
      melt1->orientation=1;
      if (dot(*p-G,Point(n1))>0)  melt1->orientation=-1;
    }

    //travel path of elements and orient normals as the first one
    nelt2=nelt1->child_;
    while (nelt2 !=nullptr)
    {
      nelt1=nelt2->parent_;
      elt1=nelt1->objectp();
      elt2=nelt2->objectp();
      MeshElement* melt1=elt1->meshElement(), *melt2=elt2->meshElement();
      if (spaceDim()==2) //2D case: curve in R2 (use numbering)
      {
        if (elt1->vertexNumber(2)!= elt2->vertexNumber(1) && elt1->vertexNumber(1)!= elt2->vertexNumber(2))
          melt2->orientation = -melt1->orientation;
        else
          melt2->orientation = melt1->orientation;
      }
      else //3D case: surface in R3
      {
        Vector<real_t> n1=normalVectors[elt1]*real_t(melt1->orientation), n2=normalVectors[elt2];
        //common side
        std::pair<number_t, number_t> pside(0,0);
        for (number_t s1 = 1; s1<= elt1->numberOfSides() && pside.first==0; s1++) //loop on elt1 sides
        {
          string_t key1 = elt1->encodeElement(s1); //encode vertex numbers of side s
          for (number_t s2 = 1; s2 <= elt2->numberOfSides() && pside.first==0; s2++) //loop on elt2 sides
            if (elt2->encodeElement(s2)==key1)
            {
              pside.first=s1;  //common side found
              pside.second=s2;
            }
        }

        const GeomRefElement* gr1=melt1->geomRefElement(), *gr2=melt2->geomRefElement();
        melt2->orientation = 1;

        //orthogonal plane of elt1 containing common side
        number_t sv1=gr1->sideVertexNumber(1,pside.first),
                  sv2=gr1->sideVertexNumber(2,pside.first),
                  sv3=3;
        if (sv1!=1 && sv2!=1)
          sv3=1;
        if (sv1!=2 && sv2!=2)
          sv3=2;
        Point t=cross3D(n1, melt1->vertex(sv2)-melt1->vertex(sv1));
        real_t c=dot(t,melt1->vertex(sv1));
        number_t sw1=gr2->sideVertexNumber(1,pside.second),
                  sw2=gr2->sideVertexNumber(2,pside.second),
                  sw3=3;
        if (sw1!=1 && sw2!=1)
          sw3=1;
        if (sw1!=2 && sw2!=2)
          sw3=2;
        real_t sig=dot(n1,n2);
        if ((dot(t,melt1->vertex(sv3))-c) * (dot(t,melt2->vertex(sw3))-c) >theTolerance)
        {
          if (sig>-theTolerance)
             melt2->orientation = -1;  //take opposite
        }
        else if ((dot(t,melt1->vertex(sv3))-c) * (dot(t,melt2->vertex(sw3))-c) <-theTolerance)
        {
          if (sig<-theTolerance)
             melt2->orientation = -1;  //take opposite
        }
        else
        {
          //quasi orthogonal case, use intersection with tetrahedron faces (sv1,sv2,sv3,sw3)
          // n1 and n2 both intersect tetrahedron faces or n1 and n2 both do not intersect tetrahedron faces
          real_t h1=std::sqrt(melt1->measure()), h2=std::sqrt(melt2->measure());
          Point G1=melt1->center(), Q1=G1+Point(10*h1*n1);
          Point G2=melt2->center(), Q2=G2+Point(10*h2*n2);
          Point S1=melt1->vertex(sv1), S2=melt1->vertex(sv2), S3=melt1->vertex(sv3), S4=melt2->vertex(sw3);
          Point I;
          number_t nb1=0;
          if (intersectionSegmentTriangle(G1, Q1, S4, S1, S2, I, J, theTolerance)) ++nb1;
          if (nb1==0 && intersectionSegmentTriangle(G1, Q1, S4, S1, S3, I, J, theTolerance)) ++nb1;
          if (nb1==0 && intersectionSegmentTriangle(G1, Q1, S4, S2,S3, I, J, theTolerance)) ++nb1;
          number_t nb2=0;
          if (intersectionSegmentTriangle(G2, Q2, S3, S2, S1, I, J, theTolerance)) ++nb2;
          if (nb2==0 && intersectionSegmentTriangle(G2, Q2, S3, S2, S4, I, J, theTolerance)) ++nb2;
          if (nb2==0 && intersectionSegmentTriangle(G2, Q2, S3, S1, S4, I, J, theTolerance)) ++nb2;
          if ((nb1==0 && nb2!=0)|| (nb1!=0 && nb2==0))
             melt2->orientation = -1;     //take opposite
        }
      }
      nelt1=nelt2;
      nelt2=nelt2->child_;         //try with child
      while (nelt2==nullptr && nelt1!=nullptr)  //go right or up
      {
        nelt2=nelt1->right_;
        nelt1=nelt1->parent_;
      }
    }
  }

  orientationComputed = true;
  if (ort==_outwardsInfinite)
    reverseOrientations();   //reverse orientation if outwards choice

  //delete trees
  for (itp=paths.begin(); itp!=paths.end(); ++itp)
    delete *itp;

  //set normal vectors according to their orientations
  for (itg=geomElements.begin(); itg!=geomElements.end(); itg++)
  {
    MeshElement* melt = (*itg)->meshElement();
    if (melt->orientation == -1)
      melt->geomMapData_p->normalVector*=-1.;
  }
  trace_p->pop();
}

// reverse all the element orientations
void MeshDomain::reverseOrientations() const
{
  if (!orientationComputed)
  {
    warning("free_warning","in MeshDomain::reverseOrientations, try to reverse orientation, though they are not computed, nothing done");
    return;
  }
  std::vector<GeomElement*>::const_iterator itg=geomElements.begin();
  for (; itg!=geomElements.end(); itg++)
  {
      MeshElement* melt = (*itg)->meshElement();
      melt->orientation *=-1;
      melt->geomMapData_p->normalVector*=-1.;
  }
}

//! return normal to a domain gamma at a given point x, normal orientation may be imposed (_towardsInfinite, _outwardsInfinite, _towardsDomain, _outwardsDomain)
Vector<real_t> MeshDomain::getNormal(const Point& x, OrientationType ort, const GeomDomain& dom) const
{
  const GeomDomain * gdom=nullptr;
  if (dom.domType()==_meshDomain) gdom=&dom;
  if (!normalComputed) setNormalOrientation(ort,gdom);
  //locate nearest element
  Point y=x; real_t md;
  GeomElement* gelt= nearest(y,md);
  if (md>gelt->size()/1000.) error("free_error", "in getNormal, point seems not to be located on the domain "+name());
   MeshElement* melt=gelt->meshElement();
  if (!melt->linearMap) //re-compute normal at y
  {
    Point q=melt->geomMapData_p->geomMapInverse(y);
    melt->geomMapData_p->computeJacobianMatrix(q);
    melt->geomMapData_p->computeOrientedNormal();  //compute oriented normal
  }
  Vector<real_t> n = melt->geomMapData_p->normalVector;
  if (ort==_undefOrientationType ||(normalOrientationRule.first==ort && normalOrientationRule.second==gdom)) return n;
  if ((normalOrientationRule.first==_towardsInfinite && ort==_outwardsInfinite) ||
      (normalOrientationRule.first==_outwardsInfinite && ort==_towardsInfinite) ) return -n;
  // case outwards/towards Domain
  if (normalOrientationRule.second==gdom)  //same to/out domain
  {
    if ((normalOrientationRule.first==_towardsDomain && ort==_outwardsDomain) ||
        (normalOrientationRule.first==_outwardsDomain && ort==_towardsDomain) ) return -n;
  }
  //too complex cases to simply determine if we have to keep keep or reverse orientation
  error("free_error","in getNormal, new choice of normal orientation requires a full re-computation of normal orientations, do it yourself");
  return n;
}

//create continuous path from a root element, stored in a  node tree
// node: root node element
//   markElt: for each element, a flag to tell if element is or is not available
//   sideIndex: the map: side -> elements having this side
void createPath(Node<GeomElement>* node, std::map<GeomElement*,bool>& markElt,
                std::map<string_t, std::list<GeomElement*> >& sideIndex)
{
  std::map<string_t, std::list<GeomElement*> >::iterator itsn;
  std::list<GeomElement*>::iterator itl;
  Node<GeomElement>* n = node, *newnode=nullptr;
  GeomElement* elt=n->obj_, *nextelt=nullptr;
  while (n!=nullptr)
  {
    elt=n->obj_;
    nextelt=nullptr;
    number_t nbs = elt->numberOfSides(), s=0;
    //find available element sharing a side of current element elt
    while (s < nbs && nextelt==nullptr) //loop on element sides
    {
      itsn = sideIndex.find(elt->encodeElement(s + 1));   //search side in sideIndex map
      if (itsn!=sideIndex.end())  //side found
      {
        for (itl= itsn->second.begin(); itl!=itsn->second.end(); itl++) // loop on element sharing side
          if (!markElt[*itl])
            nextelt=*itl;
      }
      if (nextelt!=nullptr) //element found, create child node
      {
        newnode=new Node<GeomElement>(nextelt, n, n->level_+1);
        markElt[nextelt]=true;
        if (n->child_==nullptr)
          n->child_=newnode;
        else //update right node
        {
          n=n->child_;
          while (n->right_!=nullptr)
            n=n->right_;
          n->right_=newnode;
        }
        n=newnode;
      }
      else
        s++; //next side
    }
    if (nextelt==nullptr)
      n=n->parent_; //no element found, go up
  }
}

//utility for elements path construction, recursive
// *** OLD VERSION: no longer used, because a recursion stack overflow for large mesh, see new createPath function (not recursive)
//void addNode(Node<GeomElement>* node, GeomElement* elt, std::map<GeomElement*,bool>& markElt,
//             std::map<string_t, std::list<GeomElement*> >& sideIndex)
//{
//  Node<GeomElement>* prevChild=nullptr, *n;
//  std::map<string_t, std::list<GeomElement*> >::iterator itsn;
//  std::list<GeomElement*>::iterator itl;
//  for (number_t s = 0; s < elt->numberOfSides(); s++) //loop on element sides
//    {
//      itsn = sideIndex.find(elt->encodeElement(s + 1));   //search side in sideIndex map
//      if (itsn!=sideIndex.end())  //side found
//        {
//          for (itl= itsn->second.begin(); itl!=itsn->second.end(); itl++) // loop on element sharing side
//            {
//              if (!markElt[*itl])  //create child
//                {
//                  n=new Node<GeomElement>(*itl, node, node->level_+1);
//                  markElt[*itl]=true;
//                  if (prevChild==nullptr) node->child_=n;
//                  else prevChild->right_ = n;
//                  addNode(n, *itl, markElt, sideIndex);
//                  prevChild = n;
//                }
//            }
//        }
//    }
//}

/*! update domain parents (not the grandparents)
    loop on domains of mesh with dimension + 1
    check if it is a parent checking ONLY the first element
*/
void MeshDomain::updateDomainParents(bool keep)
{
   if (dim()== mesh()->meshDim()) return;  // plain domain
   if (keep && parents.size()>0) return;
   auto doms=mesh()->domains();
   for (auto d : doms)
   {
      MeshDomain* md=d->meshDomain();
      if (md->dim() == dim() +1) // check only domain of dim+1
      {
        if (find(parents.begin(),parents.end(),md)==parents.end()) // md not in parents list
        {
          std::map<string_t, std::vector<GeoNumPair> > sd;
          createSideIndex(*md,sd);  //side index of md
          string_t s = geomElements[0]->encodeElement();
          if (sd.find(s)!=sd.end()) //first element inside md, assume all elements are inside
          parents.push_back(md);
        }
     }
   }
}

/*! update parent sides if a side domain
    the parent element of a side element may be incomplete, depending on the mesher used
    this function looks for all parent elements of a side elements and updates the parentSides_ vector of each geometric element
    works only for side domain
    sideIndex: sides of mesh indexed by the list of ordered vertex numbers -> list of (parent element, side number)
               may be constructed here
*/
void MeshDomain::updateSides(std::map<string_t, std::vector<GeoNumPair> >& sideIndex)
{
  trace_p->push("MeshDomain::updateSides");
  if (!isSideDomain())
  {
    warning("free_warning","MeshDomain::updateSides() only available for side domain, "+name()+" is not");
    trace_p->pop();
    return;
  }
  if (sideIndex.size()==0)
  {
    if (dim()== mesh()->meshDim()-1) mesh()->createSideIndex(sideIndex); //update sideIndex of mesh and use it
    else // side of side domain (may be multiple domain parents)
    {
      if (parents.size()==0) updateDomainParents();
      for (auto md : parents)
      {
        std::map<string_t, std::vector<GeoNumPair>> sd;
        createSideIndex(*md,sd);       //case of a side of side, create sideIndex of Domain
        for (auto& e : sd)              //merge sd in sideIndex
        {
           auto it=sideIndex.find(e.first);
           if (it==sideIndex.end()) sideIndex[e.first]=e.second;
           else it->second.insert(it->second.end(),e.second.begin(),e.second.end());
        }
      }
    }
  }
  //update parent sides
  std::vector<GeomElement*>::iterator ite=geomElements.begin();
  std::map<string_t,std::vector<GeoNumPair> >::iterator iti;
  for (; ite!=geomElements.end(); ++ite) //loop on elements of side domain
  {
    string_t s=(*ite)->encodeElement();
    iti=sideIndex.find(s);
    if (iti==sideIndex.end()) error("geoelt_not_found");
    (*ite)->parentSides()=iti->second;  //old parentSides information are override
  }
  trace_p->pop();
  return;
}

/*! set color of geom elements of domain according to vertex values and a coloring rule
    val: real vector of values on domain vertices
    vIndex: map  mesh vertex number -> domain vertex number
    cr: a ColoringRule, i.e; a function of the form: real_t cr(const Geomelement& gelt, const std::vector<real_t>& v)
*/
void MeshDomain::setColor(const std::vector<real_t>& val, const std::map<number_t,number_t>& vIndex, ColoringRule cr) const
{
  bool hasIndex= vIndex.size()>0;
  std::vector<GeomElement*>::const_iterator itg=geomElements.begin();
  std::map<number_t,number_t>::const_iterator iti;
  for (; itg!=geomElements.end(); ++itg)
  {
    number_t nbv=(*itg)->numberOfVertices();
    std::vector<real_t > v(nbv);
    std::vector<real_t >::iterator itv=v.begin();
    if (hasIndex)
      for (number_t i=1; i<=nbv; ++i, ++itv)
      {
        iti=vIndex.find((*itg)->vertexNumber(i));
        if (iti==vIndex.end())
        {
          where("MeshDomain::setColor");
          error("vertex_not_found");
        }
        *itv=val[iti->second-1];
      }
    else
      for (number_t i=1; i<=nbv; ++i, ++itv)
        *itv=val[(*itg)->vertexNumber(i)-1];
    (*itg)->color = cr(**itg, v);
  }
}

/*! set color of geom elements of domain according to vertex vertex values and a vector coloring rule
    val: vector of real vectors on domain vertices
    vIndex: map  mesh vertex number -> domain vertex number
    vcr: a VectorColoringRule, i.e; a function of the form: real_t cr(const Geomelement& gelt, const std::vector<Vector<real_t> >& v)
*/
void MeshDomain::setColor(const std::vector<Vector<real_t> >& val, const std::map<number_t,number_t>& vIndex, VectorColoringRule vcr) const
{
  bool hasIndex= vIndex.size()>0;
  std::vector<GeomElement*>::const_iterator itg=geomElements.begin();
  std::map<number_t,number_t>::const_iterator iti;
  for (; itg!=geomElements.end(); ++itg)
  {
    number_t nbv=(*itg)->numberOfVertices();
    std::vector<Vector<real_t> > v(nbv);
    std::vector<Vector<real_t> >::iterator itv=v.begin();
    if (hasIndex)
      for (number_t i=1; i<=nbv; ++i, ++itv)
      {
        iti=vIndex.find((*itg)->vertexNumber(i));
        if (iti==vIndex.end())
        {
          where("MeshDomain::setColor");
          error("vertex_not_found");
        }
        *itv=val[iti->second-1];
      }
    else
      for (number_t i=1; i<=nbv; ++i, ++itv)
        *itv=val[(*itg)->vertexNumber(i)-1];
    (*itg)->color = vcr(**itg, v);
  }
}

/*! rebuild current domain by collecting elements satisfying a criterium given by a a ComparisonFunction
           dom = {elt such that cf(elt.color)=true}
    update all related data
    \note this function rebuilds domain only from mesh elements, if required do not forget to create new elements before
*/
void MeshDomain::rebuild(const ComparisonFunction<>& cf, bool restrictedToDomain)
{
  const Mesh* mh=mesh();
  shapeTypes.clear();
  //create list of new GeomElements
  const std::vector<GeomElement*>* melts_p=&mh->elements();
  if (restrictedToDomain)
    melts_p= &geomElements;
  const std::vector<GeomElement*>& melts=*melts_p;
  std::list<GeomElement*> elts;
  std::vector<GeomElement*>::const_iterator ite=melts.begin();
  for (; ite!=melts.end(); ++ite)
    if (cf((*ite)->color))
    {
      elts.push_back(*ite);
      shapeTypes.insert((*ite)->shapeType());  //update shapetype
    }
  geomElements.resize(elts.size());
  std::vector<GeomElement*>::iterator iti=geomElements.begin();
  std::list<GeomElement*>::iterator itl=elts.begin();
  for (; itl!=elts.end(); ++itl, ++iti)
    *iti=*itl;

  //update related data
  if (vertexElements.size()>0)
  {
    vertexElements.clear();
    buildVertexElements();
  }
  if (!kdtree.isVoid())
  {
    kdtree.clear();   //clear old kdtree
    buildKdTree();    //build new one
  }
  parentSidesUptodate=false;
  orientationComputed=false;
  jacobianComputed=false;
  diffEltComputed=false;
  normalComputed=false;
  inverseJacobianComputed=false;
  setDescription("rebuilt, made of "+tostring(geomElements.size())+" elements");
}

/*! rebuild current side domain by collecting side elements being a side of elements of doms
       sideIndex: vector of sides described as a pair (parent elt pointer, side number) and indexed by a string
       sideEltIndex: vector of side element indexed by a string
       parelts: set of parent elements for each involved domains
    built either boundary (one domain) or interface (two domains)
*/
void MeshDomain::rebuild(const std::map<string_t,std::vector<GeoNumPair> >& sideIndex,
                         std::map<string_t, GeomElement*>& sideltIndex,
                         const std::map<GeomDomain*,std::set<GeomElement*>*>& parelts)
{
  if (!isSideDomain())
  {
    where("MeshDomain::rebuild(...)");
    error("domain_not_side",name());
  }
  if (parelts.size()==0)
  {
    where("MeshDomain::rebuild(...)");
    error("is_void","parelts");
  }
  if (parelts.size()>2)
  {
    where("MeshDomain::rebuild(...)");
    error("dim_not_in_range",0,1);
  }
  bool isBoundary = parelts.size()==1;
  std::list<GeomElement*> newelts;
  std::map<string_t, std::vector<GeoNumPair> >::const_iterator its=sideIndex.begin(), itse=sideIndex.end();
  std::map<string_t, GeomElement*>::const_iterator itre, itrend=sideltIndex.end();
  std::set<GeomElement*>* eltspar=parelts.begin()->second;
  std::set<GeomElement*>::const_iterator ite=eltspar->begin();
  std::map<GeomDomain*,std::set<GeomElement*>*>::const_iterator itm;

  for (; ite!=eltspar->end(); ++ite)
  {
    for (number_t s=1; s<=(*ite)->numberOfSides(); ++s) //loop on side of element
    {
      string_t key = (*ite)->encodeElement(s);
      its=sideIndex.find(key);
      if (its!=itse)
      {
        bool sideOK = false;
        const std::vector<GeoNumPair>& parents = its->second;
        number_t nbpar= parents.size();
        if (isBoundary)  //only one parent
        {
          if (nbpar==1)
            sideOK = true;
        }
        else if (nbpar==2) //parent elts in each parent domain (interface)
        {
          std::set<GeomDomain*> doms;
          for (number_t p=0; p<nbpar; ++p)
          {
            GeomElement* e = parents[p].first;
            itm=parelts.begin();
            while (itm!=parelts.end())
            {
              if (itm->second->find(e)==itm->second->end())
                itm++;
              else
              {
                doms.insert(itm->first);
                itm=parelts.end();
              }
            }
          }
          sideOK= doms.size()==nbpar;
        }

        if (sideOK)   // side found
        {
          GeomElement* sidelt=nullptr;
          itre=sideltIndex.find(key);
          if (itre!=itrend)
            sidelt=itre->second;
          else //create new sidelt
          {
            sidelt=new GeomElement(*ite,s,mesh()->lastIndex_++);
            sidelt->parentSides()=parents;
            sideltIndex[key]=sidelt;  //add new side element to global list
          }
          newelts.push_back(sidelt);
        }
      }
    }
  }
  geomElements.assign(newelts.begin(),newelts.end());

  setDescription("rebuilt, made of "+tostring(geomElements.size())+" elements");
  if (vertexElements.size()>0)
  {
    vertexElements.clear();
    buildVertexElements();
  }
  if (!kdtree.isVoid())
  {
    kdtree.clear();   //clear old kdtree
    buildKdTree();    //build new one
  }
  parentSidesUptodate=true;
  orientationComputed=false;
  jacobianComputed=false;
  diffEltComputed=false;
  normalComputed=false;
  inverseJacobianComputed=false;
}

//order of geometric elements of mesh domain (computed if not set)
number_t MeshDomain::order() const
{
  if (geomElements.size()==0)
  {
    warning("free_warning","MeshDomain::order(), domain "+name()+ " has no geometric element (return 0 as order)");
    return 0;
  }
  number_t ord = domainInfo_p->maxEltOrder;
  if (ord>0) return ord;  //already set
  std::vector<GeomElement*>::const_iterator itg=geomElements.begin();
  for (;itg!=geomElements.end();++itg)
  {
    MeshElement* melt = (*itg)->meshElement();
    if (melt == nullptr)  melt = (*itg)->buildSideMeshElement();
    ord=std::max(ord,melt->order());
   }
   domainInfo_p->maxEltOrder=ord;
   return ord;
}

// clear temporary GeomMapData structures of Element's
void MeshDomain::clearGeomMapData()
{
  std::vector<GeomElement*>::iterator itg=geomElements.begin();
  for (; itg != geomElements.end(); itg++)
    (*itg)->clearGeomMapData();
}

// set material id (>0) for all elements of domain
void MeshDomain::setMaterialId(number_t id)
{
  std::vector<GeomElement*>::iterator itg=geomElements.begin();
  for (; itg != geomElements.end(); itg++)
    (*itg)->materialId=id;
}

// set domain id (>0) for all elements of domain
void MeshDomain::setDomainId(number_t id)
{
  std::vector<GeomElement*>::iterator itg=geomElements.begin();
  for (; itg != geomElements.end(); itg++)
    (*itg)->domainId=id;
}

//print utility
void DomainInfo::print(std::ostream& out) const
{
  if (theVerboseLevel == 0) { return; }
  string_t mn=mesh_p->name();
  if (mn=="") mn="?";
  out << message("meshdomain_info", name, dim, mn);
  if (theVerboseLevel<2) return;
  if (!statsComputed) return;    //no additional info
  out << eol << "       ";
  out << message("meshdomain_stats", nbOfElements, nbOfNodes, nbOfVertices, measure,
                 minEltMeasure, maxEltMeasure, averageEltMeasure, minEltSize, maxEltSize, averageEltSize);
  if (!sideStatsComputed) { out << eol; return; }    //no additional side info
  out << message("meshdomain_side_stats", boundaryMeasure, minSideMeasure, maxSideMeasure, averageSideMeasure) << eol;
}

void MeshDomain::print(std::ostream& out) const
{
  if (theVerboseLevel == 0) { return; }
  domainInfo_p->print(out);
  if (geometry_p!=nullptr)  out<<", linked to geometry "<<geometry_p->domName()<<" ("<<geometry_p->asString()<<")";
  out << ", orientation ";
  if (!orientationComputed) out << "not ";
  out << "computed";
  if (extensionof_p!=nullptr)
  {
    if (extensionType==_sideExtension) out << ", side extension of side domain " << extensionof_p->name();
    else if (extensionType==_vertexExtension) out << ", vertex extension of side domain " << extensionof_p->name();
    else out << ", ficticious domain related to " << extensionof_p->name();
  }
  if (dualCrackDomain_p!=nullptr) out << ", side of a crack, dual side: " << dualCrackDomain_p->name();
  if (parents.size()>0)
  {
      out<<", domain parents : ";
      for (auto& e : parents) out<<e->name()<<" ";
  }
  if (theVerboseLevel < 3) return;

  number_t tvl = theVerboseLevel, n = std::min(theVerboseLevel, geomElements.size());
  verboseLevel(std::max(theVerboseLevel/10, number_t(1u)));
  out << ", " << geomElements.size() << " elements";
  for (number_t k = 0; k < n; k++) { out << eol << "   " << (*geomElements[k]); }
  if (n < geomElements.size()) { out << "\n   ...\n   " << (*geomElements[geomElements.size() - 1]); }
  if (sideToExt.size()>0)
  {
    n = std::min(tvl, sideToExt.size());
    out << eol << "List of elements of extension:";
    std::map<GeomElement*, std::list<GeoNumPair> >::const_iterator itm=sideToExt.begin();
    for (number_t k = 0; k < n; k++, ++itm)
    {
      out << eol << "  side elt " << itm->first->number()<<", extension elts:";
      const std::list<GeoNumPair>& gelts=itm->second;
      std::list<GeoNumPair>::const_iterator ite=gelts.begin();
      for (; ite!=gelts.end(); ++ite) out << ite->first->number() << " ";
    }
    if (n < sideToExt.size()) out << "\n   ...\n   ";
  }
  verboseLevel(tvl);
}

std::ostream& operator<<(std::ostream& out, const MeshDomain& g)
{
  g.print(out);
  return out;
}

//utility to save normals on a file, restrict to a domain that is a manifold
// up to now, export only in vtk format
void GeomDomain::saveNormalsToFile(const string_t& filename, IOFormat iof) const
{
  trace_p->push("GeomDomain::saveNormalsToFile");
  if (dim() != spaceDim()-1)
    error("free_warning", "in GeomDomain::saveNormalsToFile, domain "+name()+" is not a manifold, no normal vectors!");

  switch (iof)
  {
    case _vtk:
      saveNormalsToVtk(filename);
      break;
    default:
      error("not_yet_implemented", "GeomDomain::saveNormalsToFile in vtu format");
  }

  trace_p->pop();
}

//! utility to save normals of a manifold domain on a vtk file
void GeomDomain::saveNormalsToVtk(const string_t& filename) const
{
  trace_p->push("GeomDomain::saveNormalsToVtk");

  if (!meshDomain()->orientationComputed)
    warning("free_warning","in GeomDomain::saveNormalsToVtk, normal orientations have not been yet set, orientations may be hazardous");

  string_t fn = trim(filename);
  std::pair<string_t, string_t> rootext = fileRootExtension(fn, Environment::authorizedSaveToFileExtensions());
  fn = rootext.first;
  fn+=".vtk";
  std::ofstream out(fn.c_str());
  mesh()->vtkExport(*this, out);
  out<<"CELL_DATA "<<meshDomain()->geomElements.size()<<eol;
  out<<"NORMALS normals float\n";
  std::vector<GeomElement*>::const_iterator itg=meshDomain()->geomElements.begin();
  MeshElement* melt = (*itg)->meshElement();
  if (melt==nullptr)
    melt = (*itg)->buildSideMeshElement();
  dimen_t sd=melt->spaceDim();
  for (; itg!=meshDomain()->geomElements.end(); itg++)
  {
    melt = (*itg)->meshElement();
    if (melt==nullptr)
      melt = (*itg)->buildSideMeshElement();
    GeomMapData gp(melt, Point(std::vector<real_t>(sd,0.)));
    gp.computeJacobianMatrix(melt->geomRefElement()->centroid());
    gp.computeNormalVector();  //not normalized
    gp.normalize();            //normalized and oriented
    out<<gp.normalVector[0]<<" "<<gp.normalVector[1]<<" ";
    if (sd==3)
      out<<gp.normalVector[2] <<eol;
    else
      out<<0<<eol;
  }
  out.close();
  trace_p->pop();
}

//! utility to save element colors of a domain on a vtk file
void GeomDomain::saveColorsToFile(const string_t& filename) const
{
  trace_p->push("GeomDomain::saveColorsToFile");
  string_t fn = trim(filename);
  std::pair<string_t, string_t> rootext = fileRootExtension(fn, Environment::authorizedSaveToFileExtensions());
  fn = rootext.first;
  fn+=".vtk";
  std::ofstream out(fn.c_str());
  mesh()->vtkExport(*this, out);
  out<<"CELL_DATA "<<meshDomain()->geomElements.size()<<eol;
  out<<"SCALARS color float 1"<<std::endl;
  out<<"LOOKUP_TABLE default"<< std::endl;
  std::vector<GeomElement*>::const_iterator itg=meshDomain()->geomElements.begin();
  for (; itg!=meshDomain()->geomElements.end(); itg++)
    out<<(*itg)->color<<eol;
  out.close();
  trace_p->pop();
}


//---------------------------------------------------------------------------
// PartitionData member functions
//---------------------------------------------------------------------------
//constructor
void PartitionData::build(const std::vector<Parameter>& ps)
{
  #ifdef XLIFEPP_WITH_METIS
    trace_p->push("PartitionData::build");
    std::set<ParameterKey> params=getParamsKeys(), usedParams;
    buildDefaultParam();
    // managing params
    for (number_t i=0; i < ps.size(); ++i)
    {
      ParameterKey key=ps[i].key();
      buildParam(ps[i]);
      if (params.find(key) != params.end()) { params.erase(key); }
      else
      {
        if (usedParams.find(key) == usedParams.end())
        {
          error("partition_unexpected_param_key", words("param key",key), words("shape",_segment));
        }
        else { warning("param_already_used", words("param key",key)); }
      }
      usedParams.insert(key);
      // user must use
      std::set<ParameterKey>::const_iterator it_p;
    }
    if (ptype_==0 && objtype_==1)
    {
      warning("non_coherent_parameters", "Ptype=Recursive", "Objtype=Vol");
      ptype_=-1;
      objtype_=-1;
      Options_[METIS_OPTION_PTYPE]=ptype_;
      Options_[METIS_OPTION_OBJTYPE]=objtype_;
    }
    if (ptype_==1 && iptype_==1)
    {
      warning("non_coherent_parameters", "Ptype=Kway", "IPtype=Random");
      ptype_=-1;
      iptype_=-1;
      Options_[METIS_OPTION_PTYPE]=ptype_;
      Options_[METIS_OPTION_IPTYPE]=iptype_;
    }
    trace_p->pop();
  #else
    error("xlifepp_without_metis");
  #endif
}

std::set<ParameterKey> PartitionData::getParamsKeys()
{
  std::set<ParameterKey> params;
  params.insert(_pk_nbParts);
  params.insert(_pk_partmesh);
  params.insert(_pk_ncommon);
  params.insert(_pk_ptype);
  params.insert(_pk_ctype);
  params.insert(_pk_objtype);
  params.insert(_pk_rtype);
  params.insert(_pk_iptype);
  return params;
}

void PartitionData::buildParam(const Parameter& p)
{
  #ifdef XLIFEPP_WITH_METIS
    trace_p->push("PartitionData::buildParam");
    ParameterKey key=p.key();
    switch (key)
    {
      case _pk_nbParts:
      {
        switch (p.type())
        {
          case _integer:
            nbParts_=p.get_n();
            break;
          default:
            error("param_badtype",words("value",p.type()),words("param key",key));
        }
        break;
      }
      case _pk_partmesh:
      {
        switch (p.type())
        {
          case _integer:
          case _enumGtype:
            PartMesh_=Gtype(p.get_n());
            if (PartMesh_ == dual) { Options_[METIS_OPTION_GTYPE]=METIS_GTYPE_DUAL; }
            else if (PartMesh_ == nodal ){ Options_[METIS_OPTION_GTYPE]=METIS_GTYPE_NODAL; }
            break;
          default:
            error("param_badtype",words("value",p.type()),words("param key",key));
        }
        break;
      }
      case _pk_ncommon:
      {
        switch (p.type())
        {
          case _integer:
            Ncommon_=p.get_n();
            Options_[METIS_OPTION_NCOMMON]=Ncommon_;
            break;
          default:
            error("param_badtype",words("value",p.type()),words("param key",key));
        }
        break;
      }
      case _pk_ptype:
      {
        switch (p.type())
        {
          case _integer:
          case _enumPtype:
            ptype_=Ptype(p.get_n());
            if (ptype_ == _rec) { Options_[METIS_OPTION_PTYPE]=METIS_PTYPE_RB; }
            else { Options_[METIS_OPTION_PTYPE]=METIS_PTYPE_KWAY; }
            break;
          default:
            error("param_badtype",words("value",p.type()),words("param key",key));
        }
        break;
      }
      case _pk_ctype:
      {
        switch (p.type())
        {
          case _integer:
          case _enumCtype:
            ctype_=Ctype(p.get_n());
            if (ctype_ == _rm) { Options_[METIS_OPTION_CTYPE]=METIS_CTYPE_RM; }
            else { Options_[METIS_OPTION_CTYPE]=METIS_CTYPE_SHEM; }
            break;
          default:
            error("param_badtype",words("value",p.type()),words("param key",key));
        }
        break;
      }
      case _pk_objtype:
      {
        switch (p.type())
        {
          case _integer:
          case _enumObjtype:
            objtype_=Objtype(p.get_n());
            if (objtype_ == _cut) { Options_[METIS_OPTION_OBJTYPE]=METIS_OBJTYPE_CUT; }
            else { Options_[METIS_OPTION_OBJTYPE]=METIS_OBJTYPE_VOL; }
            break;
          default:
            error("param_badtype",words("value",p.type()),words("param key",key));
        }
        break;
      }
      case _pk_rtype:
      {
        switch (p.type())
        {
          case _integer:
          case _enumRtype:
            rtype_=Rtype(p.get_n());
            Options_[METIS_OPTION_RTYPE]=rtype_;
            if (rtype_ == _fm) { Options_[METIS_OPTION_RTYPE]=METIS_RTYPE_FM; }
            else if (rtype_ == _greedy) { Options_[METIS_OPTION_RTYPE]=METIS_RTYPE_GREEDY; }
            else if (rtype_ == _sep2sided) { Options_[METIS_OPTION_RTYPE]=METIS_RTYPE_SEP2SIDED; }
            else  { Options_[METIS_OPTION_RTYPE]=METIS_RTYPE_SEP1SIDED; }
            break;
          default:
            error("param_badtype",words("value",p.type()),words("param key",key));
        }
        break;
      }
      case _pk_iptype:
      {
        switch (p.type())
        {
          case _integer:
          case _enumIptype:
            iptype_=Iptype(p.get_n());
            if (iptype_ == _grow) { Options_[METIS_OPTION_IPTYPE]=METIS_IPTYPE_GROW; }
            else { Options_[METIS_OPTION_IPTYPE]=METIS_IPTYPE_RANDOM; }
            break;
          default:
            error("param_badtype",words("value",p.type()),words("param key",key));
        }
        break;
      }
      default:
          error("param_not_found", words("param key",key));
    }
    trace_p->pop();
  #else
    error("xlifepp_without_metis");
  #endif
}

void PartitionData::buildDefaultParam(ParameterKey key)
{
  trace_p->push("PartitionData::buildDefaultParam(Key)");
  switch (key)
  {
    case _pk_nbParts: nbParts_=2; break;
    case _pk_partmesh: PartMesh_=dual; break;
    case _pk_ncommon: Ncommon_=1; break;
    case _pk_ctype: ctype_=-1; break;
    case _pk_ptype: ptype_=-1; break;
    case _pk_objtype: objtype_=-1; break;
    case _pk_rtype: rtype_=-1; break;
    case _pk_iptype: iptype_=-1; break;
    default: error("unexpected_param_key", words("param key",key));
  }
  trace_p->pop();
}

void PartitionData::buildDefaultParam()
{
  trace_p->push("PartitionData::buildDefaultParam()");
  nbParts_=2;
  PartMesh_=dual;
  Ncommon_=1;
  ctype_=-1;
  ptype_=-1;
  objtype_=-1;
  rtype_=-1;
  iptype_=-1;
  trace_p->pop();
}

PartitionData::PartitionData(MeshDomain& md, const std::vector<Parameter>& ps)
{
  #ifdef XLIFEPP_WITH_METIS
    Options_=new int[METIS_NOPTIONS];
    METIS_SetDefaultOptions(Options_);
    build(ps);
    nmMeshFile_+=md.name();
    nmMeshFile_+="_"+tostring(nbParts_)+"P_"+tostring(PartMesh_);
    nmMeshFile_+="_"+tostring(Ncommon_)+"NC";
    switch (ptype_)
    {
      case -1:
        nmMeshFile_+="";//"_Ptype";
        break;
      case 0:
        nmMeshFile_+="_Recursive";
        break;
      case 1:
        nmMeshFile_+="_Kway";
        break;
    }
    switch (ctype_)
    {
      case -1:
        nmMeshFile_+="";//"_Ctype";
        break;
      case 0:
        nmMeshFile_+="_RM";
        break;
      case 1:
        nmMeshFile_+="_Shem";
        break;
    }
    switch (rtype_)
    {
      case -1:
        nmMeshFile_+="";//"_Rtype";
        break;
      case 0:
        nmMeshFile_+="_FM";
        break;
      case 1:
        nmMeshFile_+="_Greedy";
        break;
      case 2:
        nmMeshFile_+="_SEP2SIDED";
        break;
      case 3:
        nmMeshFile_+="_SEP1SIDED";
        break;
    }
    switch (iptype_)
    {
      case -1:
        nmMeshFile_+="";//"_IPtype";
        break;
      case 0:
        nmMeshFile_+="_Grow";
        break;
      case 1:
        nmMeshFile_+="_Random";
        break;
      case 2:
        nmMeshFile_+="_Edge";
        break;
      case 3:
        nmMeshFile_+="_Node";
        break;
    }
    switch (objtype_)
    {
      case -1:
        nmMeshFile_+="";//"_Objtype";
        break;
      case 0:
        nmMeshFile_+="_Cut";
        break;
      case 1:
        nmMeshFile_+="_Vol";
        break;
    }

    number_t nbElems=md.numberOfElements();
    number_t nbVertices=(md.mesh() )->nbOfVertices();
    nbInterfaceSides_=meshDomainElementsPartition(nbElems, nbVertices, md.geomElements );
    number_t mdom=partMeshdom( md);
  #else
    error("xlifepp_without_metis");
  #endif
}

int_t PartitionData::Option(xlifepp::string_t Option )
{
  int_t OptionVal;
  #ifdef XLIFEPP_WITH_METIS
    if ( Option == "Ptype")
    {
      OptionVal=Options_[METIS_OPTION_PTYPE];
    }
    else if (Option == "PartMesh")
    {
      if (PartMesh_ == nodal )
      {
          OptionVal=0;
      }
      else if (PartMesh_ == dual )
      {
          OptionVal=1;
      }
    }
    else if (Option == "Ctype")
    {
      OptionVal=Options_[METIS_OPTION_CTYPE];
    }
    else if (Option == "nbParts")
    {
      OptionVal=nbParts_;
    }
    else if (Option == "Rtype")
    {
      OptionVal=Options_[METIS_OPTION_RTYPE];
    }
    else if ( Option == "nbCuts")
    {
      OptionVal=Options_[METIS_OPTION_NCUTS];
    }
    else if ( Option == "Niter")
    {
      OptionVal=Options_[METIS_OPTION_NITER];
    }
    else if ( Option == "Seed")
    {
      OptionVal=Options_[METIS_OPTION_SEED];
    }
    else if ( Option == "No2hop")
    {
      OptionVal=Options_[METIS_OPTION_NO2HOP];
    }
    else if (Option == "IPtype")
    {
        OptionVal=Options_[METIS_OPTION_IPTYPE];
    }
    else if (Option == "Ncommon")
    {
        OptionVal=Ncommon_;
    }
    else if (Option == "UFactor")
    {
        OptionVal=Options_[METIS_OPTION_UFACTOR];
    }
    // case KWAY
    // if (Option[METIS_OPTION_PTYPE]!=0) //pas recursive
    // {
    if (Option == "OBJtype")
    {
      OptionVal=Options_[METIS_OPTION_OBJTYPE];
    }
    else if (Option =="Contig")
    {
      OptionVal=Options_[METIS_OPTION_CONTIG];
    }
    else if (Option=="MinConn")
    {
      OptionVal=Options_[METIS_OPTION_MINCONN];
    }
    // }
  #else
    error("xlifepp_without_metis");
  #endif
  return OptionVal;
}

number_t PartitionData::buildOption( Parameters&  Param, int * options)
{
  #ifdef XLIFEPP_WITH_METIS
    // ***********************************************************************************************
    // ! Metis options and key-words are:
    // N.B.: Metis programs are build for partition nodes, this code is for partition elements.
    // ! These 2 following options are used by gmsh.
    // * PTYPE  for [METIS OPTION PTYPE]
    //    Specifies the partitioning method. Possible values are:
    //    METIS_PTYPE_RB Multilevel recursive bisectioning.
    //    METIS_PTYPE_KWAY Multilevel k-way partitioning.
    // * Rtype for METIS_OPTION_RTYPE
    //    Determines the algorithm used for refinement. Possible values are:
    //      METIS_RTYPE_FM FM-based cut refinement.
    //      METIS_RTYPE_GREEDY Greedy-based cut and volume refinement.(default gmsh)
    //      METIS_RTYPE_SEP2SIDED Two-sided node FM refinement.
    //      METIS_RTYPE_SEP1SIDED One-sided node FM refinement.
    // * Ctype for METIS_OPTION_CTYPE
    //    Specifies the matching scheme to be used during coarsening. Possible values are:
    //      METIS_CTYPE_RM Random matching.
    //      METIS_CTYPE_SHEM Sorted heavy-edge matching (default gmsh)
    // ! Other valid options for Recursive and KWay method are:
    // * IPtype for METIS_OPTION_IPTYPE
    //     Determines the algorithm used during initial partitioning. Possible values are:
    //       METIS IPTYPE GROW Grows a bisection using a greedy strategy.
    //       METIS IPTYPE RANDOM Computes a bisection at random followed by a refinement.
    //       METIS IPTYPE EDGE Derives a separator from an edge cut.
    //       METIS IPTYPE NODE Grow a bisection using a greedy node-based strategy.
    // * No2Hop for METIS_OPTION_NO2HOP
    //    Specifies that the coarsening will not perform any 2-hop matchings
    //    when the standards matching approach fails to sufficiently coarsen the graph. Possible values are:
    //       0 Performs a 2–hop matching.
    //       1 Does not perform a 2–hop matching.
    // * nbCuts for METIS_OPTION_NCUTS
    //    Specifies the number of different partitionings that it will compute.
    //    The final partitioning is the one that achieves the best edgecut or communication volume.
    //    Default is 1.
    // * Niter for METIS_OPTION_NITER
    //    Specifies the number of iterations for the refinement algorithms
    //    at each stage of the uncoarsening process.
    // * UFactor for METIS_OPTION_UFACTOR
    //    Specifies the maximum allowed load imbalance among the partitions.
    //    For -ptype=rb, the default value is 1 and for-ptype=kway, the default value is 30.
    // * Seed for METIS_OPTION_SEED
    //    Specifies the seed for the random number generator.
    // ! The 3 following options are valid only for KWay method.
    // * OBJtype for METIS_OPTION_OBJTYPE
    //    Specifies the type of objbjective. Possible values are:
    //       METIS OBJTYPE CUT Edge-cut minimization. (default)
    //       METIS OBJTYPE VOL Total communication volume minimization.
    // * Contig for METIS_OPTION_CONTIG
    //    Specifies that the partitioning routines should try to produce partitions that are contiguous.
    //    Note that if the input graph is not connected this option is ignored.
    //       0 Does not force contiguous partitions.
    //       1 Forces contiguous partition
    // * MinConn for METIS_OPTION_MINCONN
    //    Specifies that the partitioning routines should try to minimize the maximum degree of
    //    the subdomain graph.
    //    i.e., the graph in which each partition is a node, and edges connect subdomains
    //          with a shared interface.
    //       0 Does not explicitly minimize the maximum connectivity.
    //       1 Explicitly minimize the maximum connectivity.
    for (number_t p=1; p<Param.size()+1; p++)
    {
      if ((Param(p)).name() == "Ptype")
      {
        options[METIS_OPTION_PTYPE]=Param("Ptype").get_i();
      }
      else if ((Param(p)).name() == "Ctype")
      {
        options[METIS_OPTION_CTYPE]=Param("Ctype").get_i();
      }
      else if ((Param(p)).name() == "Rtype")
      {
        options[METIS_OPTION_RTYPE]=Param("Rtype").get_i();
      }
      else if ( (Param(p)).name() == "nbCuts")
      {
        options[METIS_OPTION_NCUTS]=Param("nbCuts").get_i();
      }
      else if ( (Param(p)).name() == "Niter")
      {
        options[METIS_OPTION_NITER]=Param("Niter").get_i();
      }
      else if ( (Param(p)).name() == "Seed")
      {
        options[METIS_OPTION_SEED]=Param("Seed").get_i();
      }
      else if ( (Param(p)).name() == "No2hop")
      {
        options[METIS_OPTION_NO2HOP]=Param("No2hop").get_i();
      }
      else if ((Param(p)).name() == "IPtype")
      {
        options[METIS_OPTION_IPTYPE]=Param("IPtype").get_i();
      }
      else if ((Param(p)).name() == "UFactor")
      {
        options[METIS_OPTION_UFACTOR]=Param("UFactor").get_i();
      }
      // case KWAY
      // if (options[METIS_OPTION_PTYPE]!=0) //pas recursive
      // {
      if ((Param(p)).name() == "OBJtype")
      {
        options[METIS_OPTION_OBJTYPE]=Param("OBJtype").get_i();
      }
      else if ((Param(p)).name() =="Contig")
      {
        options[METIS_OPTION_CONTIG]=Param("Contig").get_i();
      }
      else if ((Param(p)).name()=="MinConn")
      {
        options[METIS_OPTION_MINCONN]=Param("MinConn").get_i();
      }
      // }
    }
  #else
    error("xlifepp_without_metis");
  #endif
  return 1.;
}

number_t PartitionData::metisPartition(number_t& nbElts, number_t& nbVertices
                                      ,const std::vector< GeomElement * >&  VgeomElements
                                      ,int* epart )
{
  int objval;
  #ifdef XLIFEPP_WITH_METIS
    if (theVerboseLevel>10)
    {
      theCout << " Elements number :"<<nbElts<<eol;
    }
    int* eptr;
    eptr=new int[nbElts+1];
    eptr[0]=0;
    for (int i=0; i<nbElts; i++)
        eptr[i+1]=eptr[i]+(VgeomElements[i])->numberOfVertices();
    int* eind;
    eind=new int[ eptr[nbElts] ];
    int k=0;
    std::vector< number_t > numVi;
    for (int i=0; i<nbElts; i++)
    {
      numVi=(VgeomElements[i])->vertexNumbers();
      std::sort(numVi.begin(), numVi.end());
      for (int j=0; j<numVi.size();j++)
      {
        eind[k]=numVi[j]-1; //? -1???
        k++;
      }
    }
    int* npart_;
    npart_=new int[nbVertices];
    int nbP=nbParts_;
    int nbV=nbVertices;
    int nbEl=nbElts;

    int status;
    if (PartMesh_==dual)
    {
      status = METIS_PartMeshDual(&nbEl, &nbV, eptr, eind,
                                  nullptr, nullptr, &Ncommon_, &nbP,
                                  nullptr, Options_, &objval, epart, npart_ );
    }
    else if (PartMesh_==nodal)
    {
      status = METIS_PartMeshNodal(&nbEl, &nbV, eptr, eind,
                                    nullptr, nullptr, &nbP,
                                    nullptr, Options_, &objval, epart, npart_ );
    }
    objval_=objval;
    delete[] eptr;
    eptr=nullptr;
    delete[] eind;
    eind=nullptr;
  #else
    error("xlifepp_without_metis");
  #endif
  return objval_;
}

number_t PartitionData::EltsVertPartInterface(number_t& nbElts
                                             ,const std::vector< GeomElement * >&  VgeomElements
                                             ,int* epart)
{
 GeomElement * GEi;
 number_t numTrg, numVer;
 Point Sj;
 number_t nbVer;
 std::vector<number_t> nbElemByPart(nbParts_);
 for (number_t i = 0; i < nbElts; i++)
 {
    nbElemByPart[epart[i]]++;
    elementsByPartition_m.insert(std::make_pair( VgeomElements[i]->number()-1, epart[i] ) ) ;
    // loop on elements vertices
    GEi=VgeomElements[i];
    numTrg=GEi->number();
    nbVer=GEi->numberOfVertices();
    bool found;
    for (number_t j=0; j<nbVer; j++)
    {
      numVer = GEi->vertexNumber(j+1);
      if (verticesByPartition_m.find(numVer) != verticesByPartition_m.end())
      {
        // the vertex numver has been already found.
        if ( (verticesByPartition_m.find(numVer) )->second != epart[i] )
        {
            (verticesByPartition_m.find(numVer) )->second=-1;
            interfaceVertices_l.push_back(verticesByPartition_m.find(numVer)->first);
        }
      }
      else
      {
          verticesByPartition_m.insert(std::make_pair(numVer, epart[i]  ) );
      }
    }
  }
  balance_=*max_element(nbElemByPart.begin(), nbElemByPart.end())*nbParts_*1./nbElts;
  for (number_t i = 0; i < nbElts; i++)
  {
    GEi=VgeomElements[i];
    numTrg=GEi->number();
    nbVer=GEi->numberOfVertices();
    bool found;
    for (number_t j=0; j<nbVer; j++)
    {
      numVer = GEi->vertexNumber(j+1);
      if ( (verticesByPartition_m.find(numVer)->second)==-1)
          interfaceElements_l.push_back(GEi->number());
    }
  }
  interfaceVertices_l.sort();
  interfaceVertices_l.unique();
  interfaceElements_l.sort();
  interfaceElements_l.unique();
  return 1.;
}

number_t PartitionData::interfaceSides(number_t nbElts
                                      ,const std::vector< GeomElement * >&   VgeomElements )
{
  std::list<number_t>::iterator itIV=(interfaceVertices_l).begin();
  std::list<number_t>::iterator itIE=(interfaceElements_l).begin();
  bool found;
  number_t nbBoundarySides=0;
  int partP, partM, partmin, partmax;
  std::map<string_t, number_t> sideIndex;
  std::list<number_t>::iterator itel;
  std::list<number_t>::iterator itn;
  std::map<string_t, number_t>::iterator itsn;
  number_t nbside = 0;
  std::vector< GeomElement * > Sides;
  number_t nbS=nbElts;
  for (int i=0; i <VgeomElements.size(); i++)
  {
    string_t key = (VgeomElements[i])->encodeElement();       //encode vertex numbers
    if (sideIndex.find(key) == sideIndex.end())                //add GeomElement in map
    {
        Sides.push_back(VgeomElements[i]);
        nbside++;
        sideIndex[key] = nbside;
    }
  } // end of for (itel=
  MeshElement* melt;
  for (int i=0; i <VgeomElements.size(); i++)
  {
    melt = (VgeomElements[i])->meshElement();
    for (number_t s = 0; s < (VgeomElements[i])->numberOfSides(); s++)   // loop on element sides
    {
      string_t key = ((VgeomElements)[i])->encodeElement(s + 1); // encode vertex numbers of side s
      itsn = sideIndex.find(key);                // search side in sideIndex map
      if (itsn == sideIndex.end())                // create a new side element
      {
        Sides.push_back(new GeomElement(VgeomElements[i], s + 1, ++VgeomElements[i]->meshP()->lastIndex_)); //update sides_
        nbside++;
        sideIndex[key] = nbside;             // update sideIndex
        melt->sideNumbers[s] = nbside;       // update sideNumbers
      }
      else // update side numbering of element
      {
        melt->sideNumbers[s] = itsn->second; //update sideNumbers
        std::vector<GeoNumPair>& parside = Sides[itsn->second - 1]->parentSides(); //update parentSides_
        if (std::find(parside.begin(), parside.end(), GeoNumPair(VgeomElements[i], s + 1))
          == parside.end())
        { parside.push_back(GeoNumPair(VgeomElements[i], s + 1)); }
        if (Sides[itsn->second - 1]->numberOfParents()==2)
        {
          if ( elementsByPartition_m.find((Sides[itsn->second - 1]->parent(0) )->number() -1)->second
            != elementsByPartition_m.find((Sides[itsn->second - 1]->parent(1) )->number() -1)->second  )
          {
            interfaceSides_v.push_back(Sides[itsn->second - 1]);
            partP=elementPart((Sides[itsn->second - 1]->parent(1) )->number());  ;
            partM=elementPart((Sides[itsn->second - 1]->parent(0) )->number());
          }
        }
      } // end of else
    } // end for (number_t s
  } //end for (itel
  sideIndex.clear();
  return interfaceSides_v.size();
}

number_t PartitionData::meshDomainElementsPartition(number_t& nbElts, number_t& nbVertices
                                                   ,const std::vector< GeomElement * >&   VgeomElements )
{
  number_t nbis;
  #ifdef XLIFEPP_WITH_METIS
    int* epart;
    epart=new int[nbElts];
    objval_=metisPartition(nbElts, nbVertices, VgeomElements, epart );
    number_t UN=EltsVertPartInterface(nbElts, VgeomElements, epart);
    nbis=interfaceSides(nbElts, VgeomElements);

    if (!METIS_OK)
    {
      theCout << "METIS_ERROR_INPUT:"<< METIS_ERROR_INPUT<<std::endl;
      theCout << "METIS_ERROR_MEMORY:"<< METIS_ERROR_MEMORY<<std::endl;
      theCout << "METIS_ERROR:"<< METIS_ERROR<<std::endl;
      error("echec METIS");
    }
    delete[] epart;
    epart=nullptr;
  #else
    error("xlifepp_without_metis");
  #endif
  return nbis;
}

number_t PartitionData::partMeshdom( MeshDomain& md)
{
  string_t basisDomName=md.name();
  dimen_t dim=md.dim();
  string_t nMd=md.name();
  std::vector<string_t> DomName;
  DomName.resize(DomName.size()+(nbParts_*(nbParts_+1)/2+1) );
  string_t ss;
  std::vector< GeomElement* > GeoElts=md.mesh()->elements();
  std::vector< GeomElement* > GeoEltsP;
  std::vector< std::vector< GeomElement* > > GeoEltsDom(nbParts_);
  number_t nbElems=elementsByPartition_m.size();
  meshdom_p.resize(meshdom_p.size()+nbParts_*(nbParts_+1)/2+1);
  for (int_t p=nbParts_-1; p>-1; p--)
  {
    DomName[p]=basisDomName+"_"+tostring(p+1);
    ss="Part "+tostring(p);
    meshdom_p[p]= (new GeomDomain(*(md.mesh()), DomName[p], dim, ss ) )->meshDomain();
  }
  int i=0;
  for ( std::map<number_t, int_t>::iterator itMap = elementsByPartition_m.begin()
      ; itMap !=elementsByPartition_m.end() ; ++itMap )
  {
    (meshdom_p[itMap->second]->geomElements).push_back(GeoElts[itMap->first]);
  }
  for (int_t p=nbParts_-1; p>-1; p--)
  {
    if ( ((meshdom_p[p])->numberOfElements() > 0 ) )
    {
      meshdom_p[p]->setShapeTypes();
      (md.mesh() )->insertDomain(*(meshdom_p[p]) );
    }
  }
  number_t pq=nbParts_;
  for (number_t p=0; p<nbParts_; p++)
  {
    for (number_t q=p+1; q<nbParts_; q++)
    {
     pq++;
     DomName[pq]="D_"+basisDomName+"_"+tostring(p+1)+"_"+basisDomName+"_"+tostring(q+1);
     ss="F "+tostring(p)+"w"+tostring(q);
     meshdom_p[pq]= (new GeomDomain(*(md.mesh()), DomName[pq], dim-1, ss ) )->meshDomain();
    }
  }
  number_t NumF, NumP, NumM, PNumP, PNumM, mNum,MNum, indice;
  number_t nbSidesMesh=0;
  for (number_t i=0; i<interfaceSides_v.size(); i++)
  {
    NumF=(interfaceSides_v[i])->number();
    NumP=(interfaceSides_v[i]->parent(1))->number();
    PNumP=elementPart( NumP );
    NumM=(interfaceSides_v[i]->parent(0))->number();
    PNumM=elementPart( NumM );
    mNum=std::min(PNumP,PNumM);
    MNum=std::max(PNumP,PNumM);
    indice=nbParts_+1./2.*(-2.*(double) nbParts_+2.*(double) mNum*(double)nbParts_-(double) mNum*(double) mNum -(double) mNum +2.*(double) MNum);
    (meshdom_p[indice]->geomElements).push_back(interfaceSides_v[i]);
  }
  for (number_t p=DomName.size()-1; p>nbParts_; p--)
  {
    if (DomName[p]!="")
      if ( ((meshdom_p[p])->numberOfElements() > 0 ) )
      {
        meshdom_p[p]->setShapeTypes();
        (md.mesh() )->insertDomain(*(meshdom_p[p]) );
      }
  }
  return meshdom_p.size();
}

number_t PartitionData::elementPart(number_t numG)
{
  number_t numG_part;
  if (elementsByPartition_m.find(numG-1) != elementsByPartition_m.end())
  {
    numG_part=(elementsByPartition_m.find(numG-1) )->second;
  }
  else
  {
    error("elt_not_found");
  }
  return numG_part+1;
}

int PartitionData::vertexPart(number_t& numG)
{
  int numG_part;
  if (verticesByPartition_m.find(numG) != verticesByPartition_m.end())
  {
    numG_part=(verticesByPartition_m.find(numG) )->second;
  }
  else
  {
    error("vertex_not_found");
  }
  return numG_part;
}

//---------------------------------------------------------------------------
// CompositeDomain member functions and related external functions
//---------------------------------------------------------------------------
CompositeDomain::CompositeDomain(SetOperationType sot, const std::vector<const GeomDomain*>& doms,
                                 const string_t& na)
{
  if (doms.size() == 0) { error("is_void","domains"); }
  setOpType_ = sot;
  domains_ = doms;
  string_t nam = "", op = "+";
  if (na == "") //create a name
  {
    if (sot == _intersection) { op = "&"; }
    for (number_t n = 0; n < doms.size(); n++)
    { nam += doms[n]->name() + op; }
    nam.erase(nam.size() - 1); //erase last char
  }
  dimen_t d = 0;
  for (number_t n = 0; n < doms.size(); n++)
    if (doms[n]->dim() > d) { d = doms[n]->dim(); }
  //domainInfo_p = new DomainInfo(nam, d, _compositeDomain, doms[0]->mesh());
  domainInfo_p->domType=_compositeDomain;
  domainInfo_p->mesh_p=doms[0]->mesh();
  domainInfo_p->name=nam;
  domainInfo_p->dim=d;
  domainInfo_p->description="";
}

//! number of geometric elements of composite domain is not available, too complex to compute it
number_t CompositeDomain::numberOfElements() const
{
  error("not_handled", "CompositeDomain::numberOfElements()");
  return 0;
}

//! return domain name where composite elements are sorted by name (const)
string_t CompositeDomain::sortedName() const
{
  string_t nam, op="+";
  if (setOpType_ == _intersection) op="&";
  std::set<string_t> sortedNames;
  std::vector<const GeomDomain*>::const_iterator itd;
  for (itd=domains_.begin(); itd!=domains_.end(); itd++)
  {
    sortedNames.insert((*itd)->name());
  }
  std::set<string_t>::const_iterator its;
  for (its=sortedNames.begin(); its != sortedNames.end(); ++its)
  {
    if (its != sortedNames.begin()) { nam += op; }
    nam += *its;
  }
  return nam;
}


/*! return the list of basic domains (MeshDomain or AnalyticalDomain)
    if in the domains list of current CompositeDomain it appears a CompositeDomain
    this compositeDomain is also split in basic domains by recursion
    \note there is no difference between union and intersection CompositeDomain
*/
std::vector<const GeomDomain*> CompositeDomain::basicDomains() const
{
  std::vector<const GeomDomain*> basicdoms;
  std::vector<const GeomDomain*>::const_iterator itd;
  for (itd=domains_.begin(); itd!=domains_.end(); itd++)
  {
    const GeomDomain* dom = *itd;
    if (dom->domType()!=_compositeDomain)
      basicdoms.push_back(dom);
    else
    {
      std::vector<const GeomDomain*> bdoms=dom->compositeDomain()->basicDomains();
      basicdoms.insert(basicdoms.end(),bdoms.begin(),bdoms.end());
    }
  }
  return basicdoms;
}

// true if the current composite domain includes a given domain (const)
// inclusion has to be understood in a geometric meaning, not in algebraic meaning
// as side is included in its parent element, a boundary domain may be included in a volumic domain
bool CompositeDomain::include(const GeomDomain& d) const
{
  bool inc=true;
  if (d.domType()==_meshDomain)
  {
    std::vector<const GeomDomain*>::const_iterator itd=domains_.begin();
    while (itd != domains_.end() && inc)
    {
      if ((*itd)->domType()==_meshDomain)
        inc= *itd==&d;
      else
        inc=(*itd)->include(d);
      if (!inc)
        return false;
      itd++;
    }
  }
  if (d.domType()==_compositeDomain)
  {
    const std::vector<const GeomDomain*>& doms=d.compositeDomain()->domains();
    std::vector<const GeomDomain*>::const_iterator itd=doms.begin();
    while (itd != doms.end() && inc)
    {
      inc = inc && include(**itd);
      itd++;
    }
    return inc;
  }
  error("domain_notmeshorcomposite",d.name(),d.domType());
  return false;
}

//! true if composite domain is a side domain
bool CompositeDomain::isSideDomain() const
{
  std::vector<const GeomDomain*>::const_iterator itd;
  for (itd=domains_.begin(); itd!=domains_.end(); ++itd)
    if (!(*itd)->isSideDomain())
      return false;
  return true;
}


void CompositeDomain::print(std::ostream& out) const
{
  if (theVerboseLevel <= 0) { return; }
  string_t st;
  for (number_t n = 0; n < domains_.size(); n++)
  { st += "'" + domains_[n]->name() + "' "; }
  out << message("compositedomain_info", name(), dim(), words("setop", setOpType_), st);
  if (theVerboseLevel > 2)
    for (number_t n = 0; n < domains_.size(); n++)
    { out << "\n    " << *domains_[n]; }
}

std::ostream& operator<<(std::ostream& out, const CompositeDomain& g)
{
  g.print(out);
  return out;
}

//---------------------------------------------------------------------------
// PointsDomain member functions and related external functions
//---------------------------------------------------------------------------
PointsDomain::PointsDomain(const std::vector<Point>& pts, const string_t& na)
{
  if (pts.size()==0)
  {
    where("PointsDomain::PointsDomain");
    error("is_void","pts");
  }
  domainInfo_p->domType=_pointsDomain;
  domainInfo_p->mesh_p=nullptr;
  domainInfo_p->name=na;
  domainInfo_p->dim=pts[0].dim();
  points=pts;
}

// print PointsDomain informations
void PointsDomain::print(std::ostream& out) const
{
  if (theVerboseLevel <= 0) { return; }
  number_t nbp=points.size();
  out << message("pointsdomain_info", name(), dim(), nbp) << "\n";
  if (theVerboseLevel < 3) { return; }
  number_t n = std::min(theVerboseLevel,nbp);
  for (number_t k = 0; k < n; k++)
  { out << "   " << points[k]; }
  if (n < nbp) { out << "   ...\n   " << (points[nbp - 1]); }
}

std::ostream& operator<<(std::ostream& out, const PointsDomain& g)
{
  g.print(out);
  return out;
}

//==================================================================================================
//geometric facilities only 2D (to be moved elsewhere ?)
//==================================================================================================


} // end of namespace xlifepp
