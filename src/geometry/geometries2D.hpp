/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file geometries2D.hpp
  \authors Y. Lafranche, N. Kielbasiewicz
  \since 18 oct 2012
  \date 30 jul 2015

  \brief Definition of the classes corresponding to 2D canonical geometrical shapes.

  It is quite easy to find in which category a geometry is, according to their mathematical
  definition, but they are some exceptions:
  - Cylinder and Cone are surfaces but can also represent volumes inside
  - Ellipse is a closed curve or the surface inside (whereas there is the
    distinction circle / disk)
  - Ellipsoid is a closed surface or the volume inside (whereas there is the
    distinction Sphere / Ball)

  It becomes all the more tricky than if we respect the geometrical hierarchy, we will derive a
  prism (semantically 3D) from a cylinder (2D or 3D) or a pyramid (semantically 3D) from a cone.
  Another example is the general definition of a cylinder, from a surface and an axis: it supposes
  that the basis is a surface. But ellipses are suposed to be curves !!!

  This is the reason why we chose to defines ellipses as surfaces, ellipsoids as volumes (coherence) and cylinders
  and cones as volumes.

*/

#ifndef GEOMETRIES_2D_HPP
#define GEOMETRIES_2D_HPP

#include "Geometry.hpp"

namespace xlifepp
{
//============================================================================================
/*!
  \class Surface
  base class for 2D geometries
*/
//============================================================================================
class Surface : public Geometry
{
  public:
    //! default constructor for 2D geometries
    Surface();

  protected:
    std::vector<Point> p_;    //!< construction points including true vertices
    std::vector<real_t> h_;   //!< local mesh step on each vertex of the surface
    std::vector<number_t> n_; //!< number of nodes on each edge

    //@{
    //! true constructor functions
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    virtual void initParametrization()
    { error("free_error", "no initParametrization defined"); }

    //@}

  public:
    Surface(const Surface& s) : Geometry(s), p_(s.p_), h_(s.h_), n_(s.n_) {} //!< copy constructor
    virtual Geometry* clone() const { return new Surface(*this); } //!< virtual copy constructor for Geometry
    virtual Surface* cloneS() const { return new Surface(*this); } //!< virtual copy constructor for Surface
    virtual ~Surface() {} //!< virtual destructor

    //@{
    //! accessor to number of nodes per edge and discretized steps
    const std::vector<number_t>& n() const { return n_; }
    std::vector<number_t>& n() { return n_; }
    number_t n(number_t i) const { return n_[i - 1]; }
    number_t& n(number_t i) { return n_[i - 1]; }
    const std::vector<real_t>& h() const { return h_; }
    std::vector<real_t>& h() { return h_; }
    real_t h(number_t i) const { return h_[i - 1]; }
    real_t& h(number_t i) { return h_[i - 1]; }
    //@}

    //@{
    //! accessor to nodes/vertices
    const std::vector<Point>& p() const { return p_; }
    std::vector<Point>& p() { return p_; }
    const Point& p(number_t i) const { return p_[i - 1]; }
    Point& p(number_t i) { return p_[i - 1]; }
    void setHstep(real_t hs) { h_ = std::vector<real_t>(p_.size(), hs); }
    bool withNnodes() const { return (h_.size() == 0); }
    virtual void setNnodes(number_t i)
    {
      number_t d = n_.size();
      n_.clear();
      n_.resize(d, i);
    }
    //@}

    const Surface* surface() const //! access to Surface object (const), virtual in Geometry
    { return this; }
    Surface* surface()  //! access to Surface object (non const), virtual in Geometry
    { return this; }

    //! test if a surface is a polygon
    bool isPolygon() const
    { return (shape_ == _polygon || shape_ == _triangle || shape_ == _quadrangle || shape_ == _parallelogram || shape_ == _rectangle || shape_ == _square); }

    virtual bool isTranslated(const Geometry&, Point&) const;//! check if points of current surface are points of a translated points of an other surface
    virtual void computeBB();                        //! computes the bounding box (in x,y,z)
    virtual void computeMB()                         //! computes the minimal box (default=bounding box)
    { minimalBox = MinimalBox(boundingBox.bounds()); }

    //! format as string
    virtual string_t asString() const { error("shape_not_handled", words("shape", shape_)); return string_t(); }

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Surface
    virtual Surface& transform(const Transformation& t);
    //! apply a translation on a Surface (1 key)
    virtual Surface& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Surface (vector version)
    virtual Surface& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Surface::translate(Reals)", "Surface::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Surface (3 reals version)
    virtual Surface& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Surface::translate(Real, Real, Real)", "Surface::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Surface (1 key)
    virtual Surface& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Surface (2 keys)
    virtual Surface& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Surface
    virtual Surface& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Surface::rotate2d(Point, Real)", "Surface::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Surface (1 key)
    virtual Surface& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Surface (2 keys)
    virtual Surface& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Surface (3 keys)
    virtual Surface& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Surface
    virtual Surface& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Surface::rotate3d(Point, Reals, Real)", "Surface::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Surface
    virtual Surface& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Surface::rotate3d(Real, Real, Real)", "Surface::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Surface
    virtual Surface& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Surface::rotate3d(Real, Real, Real, Real)", "Surface::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Surface
    virtual Surface& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Surface::rotate3d(Point, Real, Real, Real)", "Surface::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Surface
    virtual Surface& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Surface::rotate3d(Point, Real, Real, Real, Real)", "Surface::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Surface (1 key)
    virtual Surface& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Surface (2 keys)
    virtual Surface& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Surface
    virtual Surface& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Surface::homothetize(Point, Real)", "Surface::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Surface
    virtual Surface& homothetize(real_t factor)
    {
      warning("deprecated", "Surface::homothetize(Real)", "Surface::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Surface (1 key)
    virtual Surface& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Surface
    virtual Surface& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Surface::pointReflect(Point)", "Surface::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Surface (1 key)
    virtual Surface& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Surface (2 keys)
    virtual Surface& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Surface
    virtual Surface& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Surface::reflect2d(Point, Reals)", "Surface::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Surface
    virtual Surface& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Surface::reflect2d(Point, Real, Real)", "Surface::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Surface (1 key)
    virtual Surface& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Surface (2 keys)
    virtual Surface& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Surface
    virtual Surface& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Surface::reflect3d(Point, Reals)", "Surface::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Surface
    virtual Surface& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Surface::reflect3d(Point, Real, Real, Real)", "Surface::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

//============================================================================================
/*!
  \class Polygon
  definition of a polygonal geometry in R^3

  Polygon constructors are based on a key-value system. Here are the available keys:
  - _vertices: the vertices defining the polygon, oriented along the boundary (clockwise or counterclockwise)
  - _nnodes: to define the number of nodes on each edge of the Polygon
  - _hsteps: to define the local mesh steps on the vertices of the Polygon
  - _domain_name: to define the domain name
  - _side_names/_edge_names: to define the side names
  - _vertex_names: to define the side of side names
  - _varnames: to define the variable names for print purpose
 */
//============================================================================================
class Polygon : public Surface
{
  public:
    //! default constructor
    Polygon();
    //! default constructor with vertices
    Polygon(const std::vector<Point>& vertices, const std::vector<number_t>& n = std::vector<number_t>(4, 2),
            const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //! default constructor with vertices
    Polygon(const std::vector<Point>& vertices, const std::vector<real_t>& h, const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //@{
    //! key-value constructor
    Polygon(Parameter p1);
    Polygon(Parameter p1, Parameter p2);
    Polygon(Parameter p1, Parameter p2, Parameter p3);
    Polygon(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Polygon(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Polygon(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    //@}

  protected:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    virtual void initParametrization();
    //@}

  public:
    //! destructor
    virtual ~Polygon() {}

    //! format as string
    virtual string_t asString() const;
    void printDetail(std::ostream& os) const; //!< print more infos

    virtual Geometry* clone() const { return new Polygon(*this); } //!< virtual copy constructor for Geometry
    virtual Surface* cloneS() const { return new Polygon(*this); } //!< virtual copy constructor for Surface
    virtual Polygon* clonePG() const { return new Polygon(*this); } //!< virtual copy constructor for Polygon

    std::vector<int_t> nnodesPerBorder(); //!< i-th value is nnodes of the Geometry's i-th border, else -1 if defined with hsteps

    virtual std::vector<const Point*> boundNodes() const; //!< returns list of nodes on boundary (const)
    virtual std::vector<Point*> nodes(); //!< returns list of every node (non const)
    virtual std::vector<const Point*> nodes() const;    //!< returns list of every node (const)
    virtual std::vector<const Point*> vertices() const; //!< returns list of vertices (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< return list of edges (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< return list of surfaces (const)
    virtual number_t nbSides() const { return p_.size(); } //!< returns the number of sides
    virtual void collect(const string_t& n, std::list<Geometry*>&) const; //!< collect in a list all canonical geometry's with name n
    virtual Point centroid() const; //!< return centroid of polygon
    virtual bool isPlane() const {return true;}

    //! computes the minimal box
    virtual void computeMB();
    //! access to child Polygon object (const)
    virtual const Polygon* polygon() const {return this;}
    //! access to child Polygon object
    virtual Polygon* polygon() {return this;}
    //! create boundary geometry
    virtual Geometry& buildBoundary() const;

    //------------------------------------------------
    //         transformations facilities
    //------------------------------------------------
    //! apply a geometrical transformation on a Polygon
    virtual Polygon& transform(const Transformation& t);
    //! apply a translation on a Polygon (1 key)
    virtual Polygon& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Polygon (vector version)
    virtual Polygon& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Polygon::translate(Reals)", "Polygon::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Polygon (3 reals version)
    virtual Polygon& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Polygon::translate(Real, Real, Real)", "Polygon::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Polygon (1 key)
    virtual Polygon& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Polygon (2 keys)
    virtual Polygon& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Polygon
    virtual Polygon& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Polygon::rotate2d(Point, Real)", "Polygon::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Polygon (1 key)
    virtual Polygon& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Polygon (2 keys)
    virtual Polygon& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Polygon (3 keys)
    virtual Polygon& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Polygon
    virtual Polygon& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Polygon::rotate3d(Point, Reals, Real)", "Polygon::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Polygon
    virtual Polygon& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Polygon::rotate3d(Real, Real, Real)", "Polygon::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Polygon
    virtual Polygon& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Polygon::rotate3d(Real, Real, Real, Real)", "Polygon::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Polygon
    virtual Polygon& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Polygon::rotate3d(Point, Real, Real, Real)", "Polygon::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Polygon
    virtual Polygon& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Polygon::rotate3d(Point, Real, Real, Real, Real)", "Polygon::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Polygon (1 key)
    virtual Polygon& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Polygon (2 keys)
    virtual Polygon& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Polygon
    virtual Polygon& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Polygon::homothetize(Point, Real)", "Polygon::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Polygon
    virtual Polygon& homothetize(real_t factor)
    {
      warning("deprecated", "Polygon::homothetize(Real)", "Polygon::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Polygon (1 key)
    virtual Polygon& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Polygon
    virtual Polygon& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Polygon::pointReflect(Point)", "Polygon::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Polygon (1 key)
    virtual Polygon& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Polygon (2 keys)
    virtual Polygon& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Polygon
    virtual Polygon& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Polygon::reflect2d(Point, Reals)", "Polygon::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Polygon
    virtual Polygon& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Polygon::reflect2d(Point, Real, Real)", "Polygon::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Polygon (1 key)
    virtual Polygon& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Polygon (2 keys)
    virtual Polygon& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Polygon
    virtual Polygon& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Polygon::reflect3d(Point, Reals)", "Polygon::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Polygon
    virtual Polygon& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Polygon::reflect3d(Point, Real, Real, Real)", "Polygon::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

//============================================================================================
/*!
  \class Triangle
  definition of a triangular geometry in R^3

  Triangle constructors are based on a key-value system. Here are the available keys:
  - _v1, _v2, _v3: to define the vertices of the triangle, counterclockwise oriented
  - _nnodes: to define the number of nodes on each edge of the Triangle
  - _hsteps: to define the local mesh steps on the vertices of the Triangle
  - _domain_name: to define the domain name
  - _side_names/_edge_names: to define the side names
  - _vertex_names: to define the side of side names
  - _varnames: to define the variable names for print purpose
 */
//============================================================================================
class Triangle : public Polygon
{
  public:
    //! default constructor
    Triangle();
    //! default constructor with 3 Point
    Triangle(const Point& p1, const Point& p2, const Point& p3, const std::vector<number_t>& n = std::vector<number_t>(3, 2), const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(3,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(3,""));
    //! default constructor with 3 Point
    Triangle(const Point& p1, const Point& p2, const Point& p3, const std::vector<real_t>& h, const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(3,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(3,""));
    //@{
    //! key-value constructor
    Triangle(Parameter p1, Parameter p2, Parameter p3);
    Triangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Triangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Triangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Triangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Triangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    //@}

  private:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    void initParametrization();
    //@}

  public:
    //! destructor
    virtual ~Triangle() {}

    virtual Geometry* clone() const { return new Triangle(*this); } //!< virtual copy constructor for Geometry
    virtual Surface* cloneS() const { return new Triangle(*this); } //!< virtual copy constructor for Surface
    virtual Polygon* clonePG() const { return new Triangle(*this); } //!< virtual copy constructor for Polygon

    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< return list of surfaces (const)
    virtual number_t nbSides() const { return 3; } //!< returns the number of sides
    real_t measure() const;

    //! access to child Triangle object (const)
    virtual const Triangle* triangle() const {return this;}
    //! access to child Triangle object
    virtual Triangle* triangle() {return this;}

    //! format as string
    virtual string_t asString() const;

    //------------------------------------------------
    //         transformations facilities
    //------------------------------------------------
    //! apply a geometrical transformation on a Triangle
    virtual Triangle& transform(const Transformation& t);
    //! apply a translation on a Triangle (1 key)
    virtual Triangle& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Triangle (vector version)
    virtual Triangle& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Triangle::translate(Reals)", "Triangle::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Triangle (3 reals version)
    virtual Triangle& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Triangle::translate(Real, Real, Real)", "Triangle::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Triangle (1 key)
    virtual Triangle& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Triangle (2 keys)
    virtual Triangle& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Triangle
    virtual Triangle& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Triangle::rotate2d(Point, Real)", "Triangle::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Triangle (1 key)
    virtual Triangle& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Triangle (2 keys)
    virtual Triangle& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Triangle (3 keys)
    virtual Triangle& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Triangle
    virtual Triangle& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Triangle::rotate3d(Point, Reals, Real)", "Triangle::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Triangle
    virtual Triangle& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Triangle::rotate3d(Real, Real, Real)", "Triangle::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Triangle
    virtual Triangle& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Triangle::rotate3d(Real, Real, Real, Real)", "Triangle::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Triangle
    virtual Triangle& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Triangle::rotate3d(Point, Real, Real, Real)", "Triangle::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Triangle
    virtual Triangle& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Triangle::rotate3d(Point, Real, Real, Real, Real)", "Triangle::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Triangle (1 key)
    virtual Triangle& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Triangle (2 keys)
    virtual Triangle& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Triangle
    virtual Triangle& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Triangle::homothetize(Point, Real)", "Triangle::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Triangle
    virtual Triangle& homothetize(real_t factor)
    {
      warning("deprecated", "Triangle::homothetize(Real)", "Triangle::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Triangle (1 key)
    virtual Triangle& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Triangle
    virtual Triangle& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Triangle::pointReflect(Point)", "Triangle::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Triangle (1 key)
    virtual Triangle& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Triangle (2 keys)
    virtual Triangle& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Triangle
    virtual Triangle& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Triangle::reflect2d(Point, Reals)", "Triangle::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Triangle
    virtual Triangle& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Triangle::reflect2d(Point, Real, Real)", "Triangle::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Triangle (1 key)
    virtual Triangle& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Triangle (2 keys)
    virtual Triangle& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Triangle
    virtual Triangle& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Triangle::reflect3d(Point, Reals)", "Triangle::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Triangle
    virtual Triangle& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Triangle::reflect3d(Point, Real, Real, Real)", "Triangle::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }

    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const;   //!< barycentric parametrization (1-u-v)*p1+u*p2+v*p3
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const;   //!< inverse of barycentric parametrization (u,v)=invf(p)
    Vector<real_t> duffyParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< duffy parametrization on [0,1]x[0,1]
    Vector<real_t> invDuffyParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< inverse of duffy parametrization u,v)=invf(p)
};

inline Vector<real_t> parametrization_Triangle(const Point& pt, Parameters& pars, DiffOpType d = _id)  //! extern parametrization call (Duffy)
{return reinterpret_cast<const Triangle*>(pars("geometry").get_p())->duffyParametrization(pt, pars, d);}
inline Vector<real_t> invParametrization_Triangle(const Point& pt, Parameters& pars, DiffOpType d = _id) //! extern invParametrization call (Duffy)
{return reinterpret_cast<const Triangle*>(pars("geometry").get_p())->invDuffyParametrization(pt, pars, d);}

//============================================================================================
/*!
  \class Quadrangle
  definition of a quadrangular geometry in R^3

  Quadrangle constructors are based on a key-value system. Here are the available keys:
  - _v1, _v2, _v3, _v4: to define the vertices of the Quadrangle, counterclockwise oriented
  - _nnodes: to define the number of nodes on each edge of the Quadrangle
  - _hsteps: to define the local mesh steps on the vertices of the Quadrangle
  - _domain_name: to define the domain name
  - _side_names/_edge_names: to define the side names
  - _vertex_names: to define the side of side names
  - _varnames: to define the variable names for print purpose
 */
//============================================================================================
class Quadrangle : public Polygon
{
  public:
    //! default constructor
    Quadrangle();
    //! default constructor with 4 points
    Quadrangle(const Point& p1, const Point& p2, const Point& p3, const Point& p4, const std::vector<number_t>& n = std::vector<number_t>(4, 2), const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //! default constructor with 4 points
    Quadrangle(const Point& p1, const Point& p2, const Point& p3, const Point& p4, const std::vector<real_t>& h, const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //@{
    //! key-value constructor
    Quadrangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Quadrangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Quadrangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Quadrangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Quadrangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Quadrangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    //@}

  protected:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    virtual void initParametrization();
    //@}

  public:
    //! destructor
    virtual ~Quadrangle() {}

    virtual Geometry* clone() const { return new Quadrangle(*this); } //!< virtual copy constructor for Geometry
    virtual Surface* cloneS() const { return new Quadrangle(*this); } //!< virtual copy constructor for Surface
    virtual Polygon* clonePG() const { return new Quadrangle(*this); } //!< virtual copy constructor for Polygon

    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of surfaces (const)
    virtual number_t nbSides() const { return 4; } //!< returns the number of sides
    real_t measure() const;

    //! access to child Quadrangle object (const)
    virtual const Quadrangle* quadrangle() const {return this;}
    //! access to child Quadrangle object
    virtual Quadrangle* quadrangle() {return this;}

    //! format as string
    virtual string_t asString() const;

    //------------------------------------------------
    //         transformations facilities
    //------------------------------------------------
    //! apply a geometrical transformation on a Quadrangle
    virtual Quadrangle& transform(const Transformation& t);
    //! apply a translation on a Quadrangle (1 key)
    virtual Quadrangle& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Quadrangle (vector version)
    virtual Quadrangle& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Quadrangle::translate(Reals)", "Quadrangle::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Quadrangle (3 reals version)
    virtual Quadrangle& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Quadrangle::translate(Real, Real, Real)", "Quadrangle::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Quadrangle (1 key)
    virtual Quadrangle& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Quadrangle (2 keys)
    virtual Quadrangle& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Quadrangle
    virtual Quadrangle& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Quadrangle::rotate2d(Point, Real)", "Quadrangle::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Quadrangle (1 key)
    virtual Quadrangle& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Quadrangle (2 keys)
    virtual Quadrangle& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Quadrangle (3 keys)
    virtual Quadrangle& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Quadrangle
    virtual Quadrangle& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Quadrangle::rotate3d(Point, Reals, Real)", "Quadrangle::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Quadrangle
    virtual Quadrangle& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Quadrangle::rotate3d(Real, Real, Real)", "Quadrangle::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Quadrangle
    virtual Quadrangle& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Quadrangle::rotate3d(Real, Real, Real, Real)", "Quadrangle::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Quadrangle
    virtual Quadrangle& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Quadrangle::rotate3d(Point, Real, Real, Real)", "Quadrangle::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Quadrangle
    virtual Quadrangle& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Quadrangle::rotate3d(Point, Real, Real, Real, Real)", "Quadrangle::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Quadrangle (1 key)
    virtual Quadrangle& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Quadrangle (2 keys)
    virtual Quadrangle& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Quadrangle
    virtual Quadrangle& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Quadrangle::homothetize(Point, Real)", "Quadrangle::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Quadrangle
    virtual Quadrangle& homothetize(real_t factor)
    {
      warning("deprecated", "Quadrangle::homothetize(Real)", "Quadrangle::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Quadrangle (1 key)
    virtual Quadrangle& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Quadrangle
    virtual Quadrangle& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Quadrangle::pointReflect(Point)", "Quadrangle::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Quadrangle (1 key)
    virtual Quadrangle& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Quadrangle (2 keys)
    virtual Quadrangle& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Quadrangle
    virtual Quadrangle& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Quadrangle::reflect2d(Point, Reals)", "Quadrangle::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Quadrangle
    virtual Quadrangle& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Quadrangle::reflect2d(Point, Real, Real)", "Quadrangle::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Quadrangle (1 key)
    virtual Quadrangle& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Quadrangle (2 keys)
    virtual Quadrangle& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Quadrangle
    virtual Quadrangle& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Quadrangle::reflect3d(Point, Reals)", "Quadrangle::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Quadrangle
    virtual Quadrangle& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Quadrangle::reflect3d(Point, Real, Real, Real)", "Quadrangle::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }

    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< parametrization (1-u-v)*p1+u*p2+v*p4-uv(p2-p1+p4-p3)
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< inverse of parametrization (u,v)=invf(p)
};

inline Vector<real_t> parametrization_Quadrangle(const Point& pt, Parameters& pars, DiffOpType d = _id)  //! extern parametrization call
{return reinterpret_cast<const Quadrangle*>(pars("geometry").get_p())->funParametrization(pt, pars, d);}
inline Vector<real_t> invParametrization_Quadrangle(const Point& pt, Parameters& pars, DiffOpType d = _id) //! extern invParametrization call
{return reinterpret_cast<const Quadrangle*>(pars("geometry").get_p())->invParametrization(pt, pars, d);}

//============================================================================================
/*!
  \class Parallelogram
  definition of a parallelogram geometry in R^3

  Parallelogram constructors are based on a key-value system. Here are the available keys:
  - _v1, _v2, _v3, _v4: to define the vertices of the Parallelogram, counterclockwise oriented (_v3 is optional)
  - _nnodes: to define the number of nodes on each edge of the Parallelogram
  - _hsteps: to define the local mesh steps on the vertices of the Parallelogram
  - _domain_name: to define the domain name
  - _side_names/_edge_names: to define the side names
  - _vertex_names: to define the side of side names
  - _varnames: to define the variable names for print purpose
 */
//============================================================================================
class Parallelogram : public Quadrangle
{
  public:
    //! default constructor
    Parallelogram();
    //! default constructor with 3 points
    Parallelogram(const Point& p1, const Point& p2, const Point& p4, const std::vector<number_t>& n = std::vector<number_t>(4, 2), const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //! default constructor with 3 points
    Parallelogram(const Point& p1, const Point& p2, const Point& p4, const std::vector<real_t>& h, const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //@{
    //! key-value constructor
    Parallelogram(Parameter p1, Parameter p2, Parameter p3);
    Parallelogram(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Parallelogram(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Parallelogram(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Parallelogram(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Parallelogram(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Parallelogram(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    //@}

  protected:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    virtual void initParametrization();
    //@}

  public:
    //! destructor
    virtual ~Parallelogram() {}

    //! returns the number of nodes on first or third edge
    number_t n1() const {return n_[0];}
    //! returns the number of nodes on second or fourth edge
    number_t n2() const {return n_[1];}

    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new Parallelogram(*this); } //!< virtual copy constructor for Geometry
    virtual Surface* cloneS() const { return new Parallelogram(*this); } //!< virtual copy constructor for Surface
    virtual Polygon* clonePG() const { return new Parallelogram(*this); } //!< virtual copy constructor for Polygon

    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of surfaces (const)
    real_t length1() const; //!< length of first or third side
    real_t length2() const; //!< length of second or fourth side
    real_t measure() const; //!< surface of the parallelogram

    //! access to child Parallelogram object (const)
    virtual const Parallelogram* parallelogram() const {return this;}
    //! access to child Parallelogram object
    virtual Parallelogram* parallelogram() {return this;}

    //------------------------------------------------
    //         transformations facilities
    //------------------------------------------------
    //! apply a geometrical transformation on a Parallelogram
    virtual Parallelogram& transform(const Transformation& t);
    //! apply a translation on a Parallelogram (1 key)
    virtual Parallelogram& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Parallelogram (vector version)
    virtual Parallelogram& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Parallelogram::translate(Reals)", "Parallelogram::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Parallelogram (3 reals version)
    virtual Parallelogram& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Parallelogram::translate(Real, Real, Real)", "Parallelogram::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Parallelogram (1 key)
    virtual Parallelogram& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Parallelogram (2 keys)
    virtual Parallelogram& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Parallelogram
    virtual Parallelogram& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Parallelogram::rotate2d(Point, Real)", "Parallelogram::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Parallelogram (1 key)
    virtual Parallelogram& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Parallelogram (2 keys)
    virtual Parallelogram& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Parallelogram (3 keys)
    virtual Parallelogram& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Parallelogram
    virtual Parallelogram& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Parallelogram::rotate3d(Point, Reals, Real)", "Parallelogram::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Parallelogram
    virtual Parallelogram& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Parallelogram::rotate3d(Real, Real, Real)", "Parallelogram::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Parallelogram
    virtual Parallelogram& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Parallelogram::rotate3d(Real, Real, Real, Real)", "Parallelogram::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Parallelogram
    virtual Parallelogram& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Parallelogram::rotate3d(Point, Real, Real, Real)", "Parallelogram::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Parallelogram
    virtual Parallelogram& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Parallelogram::rotate3d(Point, Real, Real, Real, Real)", "Parallelogram::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Parallelogram (1 key)
    virtual Parallelogram& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Parallelogram (2 keys)
    virtual Parallelogram& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Parallelogram
    virtual Parallelogram& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Parallelogram::homothetize(Point, Real)", "Parallelogram::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Parallelogram
    virtual Parallelogram& homothetize(real_t factor)
    {
      warning("deprecated", "Parallelogram::homothetize(Real)", "Parallelogram::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Parallelogram (1 key)
    virtual Parallelogram& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Parallelogram
    virtual Parallelogram& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Parallelogram::pointReflect(Point)", "Parallelogram::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Parallelogram (1 key)
    virtual Parallelogram& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Parallelogram (2 keys)
    virtual Parallelogram& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Parallelogram
    virtual Parallelogram& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Parallelogram::reflect2d(Point, Reals)", "Parallelogram::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Parallelogram
    virtual Parallelogram& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Parallelogram::reflect2d(Point, Real, Real)", "Parallelogram::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Parallelogram (1 key)
    virtual Parallelogram& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Parallelogram (2 keys)
    virtual Parallelogram& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Parallelogram
    virtual Parallelogram& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Parallelogram::reflect3d(Point, Reals)", "Parallelogram::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Parallelogram
    virtual Parallelogram& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Parallelogram::reflect3d(Point, Real, Real, Real)", "Parallelogram::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }

    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< parametrization (1-u-v)*p1+u*p2+v*p4
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< inverse of parametrization (u,v)=invf(p)
};

inline Vector<real_t> parametrization_Parallelogram(const Point& pt, Parameters& pars, DiffOpType d = _id)  //! extern parametrization call
{return reinterpret_cast<const Parallelogram*>(pars("geometry").get_p())->funParametrization(pt, pars, d);}
inline Vector<real_t> invParametrization_Parallelogram(const Point& pt, Parameters& pars, DiffOpType d = _id) //! extern invParametrization call
{return reinterpret_cast<const Parallelogram*>(pars("geometry").get_p())->invParametrization(pt, pars, d);}

//============================================================================================
/*!
  \class Rectangle
  definition of a rectangular geometry in R^3

  Rectangle constructors are based on a key-value system. Here are the available keys:
  - _v1, _v2, _v4: to define the vertices of the Rectangle, counterclockwise oriented
  - _center: to define the center of the Rectangle
  - _origin: to define the first vertex of the Rectangle (same definition as _v1)
  - _xlength, _ylength: the lengths of the Rectangle
  - _xmin, _xmax, _ymin, _ymax: to define the Rectangle v1=(xmin, ymin), v2=(xmax, ymin), v4=(xmin, ymax)
  - _nnodes: to define the number of nodes on each edge of the Rectangle
  - _hsteps: to define the local mesh steps on the vertices of the Rectangle
  - _domain_name: to define the domain name
  - _side_names/_edge_names: to define the side names
  - _vertex_names: to define the side of side names
  - _varnames: to define the variable names for print purpose
 */
//============================================================================================
class Rectangle : public Parallelogram
{
  protected:
    Point center_;   //!< center of the rectangle
    Point origin_;   //!< "bottom left" vertex of the rectangle (is equal to p_[0])
    bool isCenter_;  //!< true when center key is used
    bool isOrigin_;  //!< true when origin key is used
    real_t xlength_; //!< first length of the rectangle
    real_t ylength_; //!< second length of the rectangle
  private:
    // initialized when rectangle defined with xmin, xmax, ymin, ymax keys
    real_t xmin_, xmax_, ymin_, ymax_;
    // true when key group (_xmin, _xmax, _ymin, _ymax) is given
    bool isBounds_;

  public:
    //! default constructor
    Rectangle();
    //! default constructor with 3 points
    Rectangle(const Point& p1, const Point& p2, const Point& p4, const std::vector<number_t>& n = std::vector<number_t>(4, 2), const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //! default constructor with 3 points
    Rectangle(const Point& p1, const Point& p2, const Point& p4, const std::vector<real_t>& h, const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //@{
    //! key-value constructor
    Rectangle(Parameter p1, Parameter p2, Parameter p3);
    Rectangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Rectangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Rectangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Rectangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Rectangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Rectangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    //@}

  protected:
    //@{
    //! true constructor functions
    void buildP();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! destructor
    virtual ~Rectangle() {}

    //! returns the number of nodes on first or third edge
    number_t nx() const {return n_[0];}
    //! returns the number of nodes on second or fourth edge
    number_t ny() const {return n_[1];}

    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new Rectangle(*this); } //!< virtual copy constructor for Geometry
    virtual Surface* cloneS() const { return new Rectangle(*this); } //!< virtual copy constructor for Surface
    virtual Polygon* clonePG() const { return new Rectangle(*this); } //!< virtual copy constructor for Polygon

    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of surfaces (const)
    real_t xlength() const { return xlength_; }
    real_t ylength() const { return ylength_; }
    real_t measure() const { return (xlength_ * ylength_); }

    virtual void computeMB() { minimalBox = MinimalBox(p_[0], p_[1], p_[3]); } //!< computes the minimal box

    //! access to child Rectangle object (const)
    virtual const Rectangle* rectangle() const {return this;}
    //! access to child Rectangle object
    virtual Rectangle* rectangle() {return this;}

    //------------------------------------------------
    //         transformations facilities
    //------------------------------------------------
    //! apply a geometrical transformation on a Rectangle
    virtual Rectangle& transform(const Transformation& t);
    //! apply a translation on a Rectangle (1 key)
    virtual Rectangle& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Rectangle (vector version)
    virtual Rectangle& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Rectangle::translate(Reals)", "Rectangle::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Rectangle (3 reals version)
    virtual Rectangle& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Rectangle::translate(Real, Real, Real)", "Rectangle::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Rectangle (1 key)
    virtual Rectangle& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Rectangle (2 keys)
    virtual Rectangle& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Rectangle
    virtual Rectangle& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Rectangle::rotate2d(Point, Real)", "Rectangle::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Rectangle (1 key)
    virtual Rectangle& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Rectangle (2 keys)
    virtual Rectangle& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Rectangle (3 keys)
    virtual Rectangle& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Rectangle
    virtual Rectangle& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Rectangle::rotate3d(Point, Reals, Real)", "Rectangle::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Rectangle
    virtual Rectangle& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Rectangle::rotate3d(Real, Real, Real)", "Rectangle::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Rectangle
    virtual Rectangle& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Rectangle::rotate3d(Real, Real, Real, Real)", "Rectangle::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Rectangle
    virtual Rectangle& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Rectangle::rotate3d(Point, Real, Real, Real)", "Rectangle::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Rectangle
    virtual Rectangle& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Rectangle::rotate3d(Point, Real, Real, Real, Real)", "Rectangle::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Rectangle (1 key)
    virtual Rectangle& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Rectangle (2 keys)
    virtual Rectangle& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Rectangle
    virtual Rectangle& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Rectangle::homothetize(Point, Real)", "Rectangle::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Rectangle
    virtual Rectangle& homothetize(real_t factor)
    {
      warning("deprecated", "Rectangle::homothetize(Real)", "Rectangle::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Rectangle (1 key)
    virtual Rectangle& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Rectangle
    virtual Rectangle& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Rectangle::pointReflect(Point)", "Rectangle::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Rectangle (1 key)
    virtual Rectangle& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Rectangle (2 keys)
    virtual Rectangle& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Rectangle
    virtual Rectangle& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Rectangle::reflect2d(Point, Reals)", "Rectangle::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Rectangle
    virtual Rectangle& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Rectangle::reflect2d(Point, Real, Real)", "Rectangle::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Rectangle (1 key)
    virtual Rectangle& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Rectangle (2 keys)
    virtual Rectangle& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Rectangle
    virtual Rectangle& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Rectangle::reflect3d(Point, Reals)", "Rectangle::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Rectangle
    virtual Rectangle& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Rectangle::reflect3d(Point, Real, Real, Real)", "Rectangle::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

//============================================================================================
/*!
  \class SquareGeo
  definition of a square geometry in R^3

  SquareGeo constructors are based on a key-value system. Here are the available keys:
  - _v1, _v2, _v4: to define the vertices of the Square, counterclockwise oriented
  - _center: to define the center of the SquareGeo
  - _origin: to define the first vertex of the SquareGeo (same definition as _v1)
  - _length: the length of the SquareGeo
  - _xmin, _xmax, _ymin, _ymax: to define the SquareGeo v1=(xmin, ymin), v2=(xmax, ymin), v4=(xmin, ymax)
  - _nnodes: to define the number of nodes on each edge of the SquareGeo
  - _hsteps: to define the local mesh steps on the vertices of the SquareGeo
  - _domain_name: to define the domain name
  - _side_names/_edge_names: to define the side names
  - _vertex_names: to define the side of side names
  - _varnames: to define the variable names for print purpose
 */
class SquareGeo : public Rectangle
{
  public:
    //! default constructor
    SquareGeo();
    //! default constructor with 3 points
    SquareGeo(const Point& p1, const Point& p2, const Point& p4, const std::vector<number_t>& n = std::vector<number_t>(4, 2), const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //! default constructor with 3 points
    SquareGeo(const Point& p1, const Point& p2, const Point& p4, const std::vector<real_t>& h, const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //@{
    //! key-value constructor
    SquareGeo(Parameter p1, Parameter p2);
    SquareGeo(Parameter p1, Parameter p2, Parameter p3);
    SquareGeo(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    SquareGeo(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    SquareGeo(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    SquareGeo(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    SquareGeo(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    SquareGeo(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    //@}

  private:
    //@{
    //! true constructor functions
    void buildP();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! destructor
    virtual ~SquareGeo() {}

    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new SquareGeo(*this); } //!< virtual copy constructor for Geometry
    virtual Surface* cloneS() const { return new SquareGeo(*this); } //!< virtual copy constructor for Surface
    virtual Polygon* clonePG() const { return new SquareGeo(*this); } //!< virtual copy constructor for Polygon

    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of surfaces (const)

    //! access to child SquareGeo object (const)
    virtual const SquareGeo* square() const {return this;}
    //! access to child SquareGeo object
    virtual SquareGeo* square() {return this;}

    //------------------------------------------------
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a SquareGeo
    virtual SquareGeo& transform(const Transformation& t);
    //! apply a translation on a SquareGeo (1 key)
    virtual SquareGeo& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a SquareGeo (vector version)
    virtual SquareGeo& translate(std::vector<real_t> u)
    {
      warning("deprecated", "SquareGeo::translate(Reals)", "SquareGeo::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a SquareGeo (3 reals version)
    virtual SquareGeo& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "SquareGeo::translate(Real, Real, Real)", "SquareGeo::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a SquareGeo (1 key)
    virtual SquareGeo& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a SquareGeo (2 keys)
    virtual SquareGeo& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a SquareGeo
    virtual SquareGeo& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "SquareGeo::rotate2d(Point, Real)", "SquareGeo::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a SquareGeo (1 key)
    virtual SquareGeo& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a SquareGeo (2 keys)
    virtual SquareGeo& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a SquareGeo (3 keys)
    virtual SquareGeo& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a SquareGeo
    virtual SquareGeo& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "SquareGeo::rotate3d(Point, Reals, Real)", "SquareGeo::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a SquareGeo
    virtual SquareGeo& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "SquareGeo::rotate3d(Real, Real, Real)", "SquareGeo::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a SquareGeo
    virtual SquareGeo& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "SquareGeo::rotate3d(Real, Real, Real, Real)", "SquareGeo::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a SquareGeo
    virtual SquareGeo& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "SquareGeo::rotate3d(Point, Real, Real, Real)", "SquareGeo::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a SquareGeo
    virtual SquareGeo& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "SquareGeo::rotate3d(Point, Real, Real, Real, Real)", "SquareGeo::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a SquareGeo (1 key)
    virtual SquareGeo& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a SquareGeo (2 keys)
    virtual SquareGeo& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a SquareGeo
    virtual SquareGeo& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "SquareGeo::homothetize(Point, Real)", "SquareGeo::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a SquareGeo
    virtual SquareGeo& homothetize(real_t factor)
    {
      warning("deprecated", "SquareGeo::homothetize(Real)", "SquareGeo::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a SquareGeo (1 key)
    virtual SquareGeo& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a SquareGeo
    virtual SquareGeo& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "SquareGeo::pointReflect(Point)", "SquareGeo::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a SquareGeo (1 key)
    virtual SquareGeo& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a SquareGeo (2 keys)
    virtual SquareGeo& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a SquareGeo
    virtual SquareGeo& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "SquareGeo::reflect2d(Point, Reals)", "SquareGeo::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a SquareGeo
    virtual SquareGeo& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "SquareGeo::reflect2d(Point, Real, Real)", "SquareGeo::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a SquareGeo (1 key)
    virtual SquareGeo& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a SquareGeo (2 keys)
    virtual SquareGeo& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a SquareGeo
    virtual SquareGeo& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "SquareGeo::reflect3d(Point, Reals)", "SquareGeo::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a SquareGeo
    virtual SquareGeo& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "SquareGeo::reflect3d(Point, Real, Real, Real)", "SquareGeo::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
  \class Ellipse
  definition of an elliptic geometry in R^3 (surface)

  Ellipse constructors are based on a key-value system. Here are the available keys:
  - _center: to define the center of the Ellipse
  - _v1, _v2: to define apogees of the Ellipse
  - _xlength, _ylength: to define axis lengths of the Ellipse
  - _xradius, _yradius: to define semi-axis lengths of the Ellipse
  - _angle1, _angle2: to define an elliptic sector from a pair of angles. The angular origin is determined by _v1
  - _type: indicator to fit curved boundaries (default) or not which gives flat (or plane) boundaries
  - _nnodes: to define the number of nodes on the edges of the Ellipse
  - _hsteps: to define the local mesh steps on build points of the Ellipse
  - _domain_name: to define the domain name
  - _side_names/_edge_names: to define the side names
  - _vertex_names: to define the side of side names
  - _varnames: to define the variable names for print purpose
 */
//============================================================================================
class Ellipse : public Surface
{
  protected:
    /*!
      vertices of the ellipse
      - p_[0] is the center of the ellipse
      - p_[1] first apogee of the ellipse
      - p_[2] second apogee of the ellipse
      - p_[3] first perigee of the ellipse
      - p_[4] second perigee of the ellipse
    */

    std::vector<Point> v_;    //!< true list of vertices according to number of octants
    real_t xradius_, yradius_;
    bool isAxis_;
    real_t thetamin_;       //!< first angle defining the sector, given in radian
    real_t thetamax_;       //!< second angle defining the sector, given in radian
    bool isSector_;         //!< true if thetamin_ > 0 or thetamax_ < 2pi
    dimen_t type_;          //!< indicator to fit curved boundaries (default) or not which gives flat (or plane) boundaries

  public:
    //! default constructor
    Ellipse();
    //! default constructor with 3 points
    Ellipse(const Point& center, const Point& p1, const Point& p2, const std::vector<number_t>& n = std::vector<number_t>(4, 2), const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //! default constructor with 3 points
    Ellipse(const Point& center, const Point& p1, const Point& p2, const std::vector<real_t>& h, const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //@{
    //! key-value constructor
    Ellipse(Parameter p1, Parameter p2, Parameter p3);
    Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11);
    //@}

  protected:
    //@{
    //! true constructor functions
    void buildPVNAndH();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    virtual void initParametrization();
    //@}

  public:
    //! destructor
    virtual ~Ellipse() {}

    //! accessor to point
    Point center() const { return p_[0]; }
    real_t length1() const { return 2.*xradius_; } //!< return first axis length
    real_t length2() const { return 2.*yradius_; } //!< return second axis length
    real_t radius1() const { return xradius_; }    //!< return first semi-axis length
    real_t radius2() const { return yradius_; }    //!< return second semi-axis length
    virtual number_t nbSubdiv() const;             //!< returns number of subdivision
    virtual dimen_t type() const { return type_; } //!< returns type of subdivision
    real_t thetamin() const { return thetamin_; }  //!< returns the first angle
    real_t thetamax() const { return thetamax_; }  //!< returns the second angle
    bool isSector() const { return isSector_; }    //!< returns the flag being true if it is an elliptical sector
    virtual bool isPlane() const {return true;}

    //! accessor to true vertices
    const std::vector<Point>& v() const { return v_; }
    //! accessor to vertex i (read only)
    const Point& v(number_t i) const { return v_[i - 1]; }

    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new Ellipse(*this); } //!< virtual copy constructor for Geometry
    virtual Surface* cloneS() const { return new Ellipse(*this); } //!< virtual copy constructor for Surface

    std::vector<int_t> nnodesPerBorder(); //!< i-th value is nnodes of the Geometry's i-th border, else -1 if defined with hsteps

    virtual std::vector<const Point*> boundNodes() const; //!< returns list of points on boundary (const)
    virtual std::vector<Point*> nodes(); //!< returns list of every main point (non const)
    virtual std::vector<Point*> wholeNodes(); //!< returns list of every point (p_ and v_) (non const)
    virtual std::vector<const Point*> nodes() const; //!< returns list of every point (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< returns list of edges (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of surfaces (const)
    virtual number_t nbSides() const { return 4; } //!< returns the number of sides
    real_t measure() const { return pi_ * xradius_ * yradius_; }

    virtual void collect(const string_t& n, std::list<Geometry*>&) const; //!< collect in a list all canonical geometry's with name n

    //! computes the bounding box
    virtual void computeBB();
    //! computes the minimal box
    virtual void computeMB();

    //! access to child Ellipse object (const)
    virtual const Ellipse* ellipse() const {return this;}
    //! access to child Ellipse object
    virtual Ellipse* ellipse() {return this;}
    //! access to child Disk object (const)
    virtual const Disk* disk() const
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _disk)); return nullptr; }
    //! access to child Disk object
    virtual Disk* disk()
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _disk)); return nullptr; }
    //! create boundary geometry
    virtual Geometry& buildBoundary() const;

    //------------------------------------------------
    //         transformations facilities
    //------------------------------------------------
    //! apply a geometrical transformation on a Ellipse
    virtual Ellipse& transform(const Transformation& t);
    //! apply a translation on a Ellipse (1 key)
    virtual Ellipse& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Ellipse (vector version)
    virtual Ellipse& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Ellipse::translate(Reals)", "Ellipse::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Ellipse (3 reals version)
    virtual Ellipse& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Ellipse::translate(Real, Real, Real)", "Ellipse::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Ellipse (1 key)
    virtual Ellipse& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Ellipse (2 keys)
    virtual Ellipse& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Ellipse
    virtual Ellipse& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Ellipse::rotate2d(Point, Real)", "Ellipse::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Ellipse (1 key)
    virtual Ellipse& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Ellipse (2 keys)
    virtual Ellipse& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Ellipse (3 keys)
    virtual Ellipse& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Ellipse
    virtual Ellipse& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Ellipse::rotate3d(Point, Reals, Real)", "Ellipse::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Ellipse
    virtual Ellipse& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Ellipse::rotate3d(Real, Real, Real)", "Ellipse::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Ellipse
    virtual Ellipse& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Ellipse::rotate3d(Real, Real, Real, Real)", "Ellipse::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Ellipse
    virtual Ellipse& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Ellipse::rotate3d(Point, Real, Real, Real)", "Ellipse::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Ellipse
    virtual Ellipse& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Ellipse::rotate3d(Point, Real, Real, Real, Real)", "Ellipse::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Ellipse (1 key)
    virtual Ellipse& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Ellipse (2 keys)
    virtual Ellipse& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Ellipse
    virtual Ellipse& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Ellipse::homothetize(Point, Real)", "Ellipse::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Ellipse
    virtual Ellipse& homothetize(real_t factor)
    {
      warning("deprecated", "Ellipse::homothetize(Real)", "Ellipse::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Ellipse (1 key)
    virtual Ellipse& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Ellipse
    virtual Ellipse& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Ellipse::pointReflect(Point)", "Ellipse::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Ellipse (1 key)
    virtual Ellipse& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Ellipse (2 keys)
    virtual Ellipse& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Ellipse
    virtual Ellipse& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Ellipse::reflect2d(Point, Reals)", "Ellipse::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Ellipse
    virtual Ellipse& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Ellipse::reflect2d(Point, Real, Real)", "Ellipse::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Ellipse (1 key)
    virtual Ellipse& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Ellipse (2 keys)
    virtual Ellipse& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Ellipse
    virtual Ellipse& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Ellipse::reflect3d(Point, Reals)", "Ellipse::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Ellipse
    virtual Ellipse& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Ellipse::reflect3d(Point, Real, Real, Real)", "Ellipse::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }

    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< parametrization P0+(P1-P0)*r*cos(t)+P2-P0)*r*sin(t)
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< inverse of parametrization (u,v)=invf(p)
};

inline Vector<real_t> parametrization_Ellipse(const Point& pt, Parameters& pars, DiffOpType d = _id)  //! extern parametrization call
{return reinterpret_cast<const Ellipse*>(pars("geometry").get_p())->funParametrization(pt, pars, d);}
inline Vector<real_t> invParametrization_Ellipse(const Point& pt, Parameters& pars, DiffOpType d = _id) //! extern invParametrization call
{return reinterpret_cast<const Ellipse*>(pars("geometry").get_p())->invParametrization(pt, pars, d);}

//============================================================================================
/*!
  \class Disk
  definition of a circular geometry in R^3 (surface)

  Disk constructors are based on a key-value system. Here are the available keys:
  - _center: to define the center of the Disk
  - _v1, _v2: to define "apogees" of the Disk
  - _radius: to define semi-axis lengths of the Disk
  - _angle1, _angle2: to define an circular sector from a pair of angles. The angular origin is determined by _v1
  - _type: indicator to fit curved boundaries (default) or not which gives flat (or plane) boundaries
  - _nnodes: to define the number of nodes on the edges of the Disk
  - _hsteps: to define the local mesh steps on build points of the Disk
  - _domain_name: to define the domain name
  - _side_names/_edge_names: to define the side names
  - _vertex_names: to define the side of side names
  - _varnames: to define the variable names for print purpose
 */
//============================================================================================
class Disk : public Ellipse
{
  public:
    //! default constructor
    Disk();
    //! default constructor with 3 points
    Disk(const Point& center, const Point& p1, const Point& p2, const std::vector<number_t>& n = std::vector<number_t>(4, 2), const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //! default constructor with 3 points
    Disk(const Point& center, const Point& p1, const Point& p2, const std::vector<real_t>& h, const string_t& domName = string_t(), const std::vector<string_t>& sideNames = std::vector<string_t>(4,""), const std::vector<string_t>& sideOfSideNames = std::vector<string_t>(4,""));
    //@{
    //! key-value constructor
    Disk(Parameter p1, Parameter p2);
    Disk(Parameter p1, Parameter p2, Parameter p3);
    Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11);
    //@}

  protected:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! destructor
    virtual ~Disk() {}

    //! format as string
    virtual string_t asString() const;

    //! computes the bounding box
    // virtual void computeBB();
    //! computes the minimal box
    // virtual void computeMB();

    virtual Geometry* clone() const { return new Disk(*this); } //!< virtual copy constructor for Geometry
    virtual Surface* cloneS() const { return new Disk(*this); } //!< virtual copy constructor for Surface

    //virtual std::vector<std::pair<ShapeType,std::vector<const Point*> > > curves() const; //!< returns list of edges

    //! access to child Disk object (const)
    virtual const Disk* disk() const {return this;}
    //! access to child Disk object
    virtual Disk* disk() {return this;}
    real_t radius() const { return xradius_; }    //!< return radius

    //------------------------------------------------
    //         transformations facilities
    //------------------------------------------------
    //! apply a geometrical transformation on a Disk
    virtual Disk& transform(const Transformation& t);
    //! apply a translation on a Disk (1 key)
    virtual Disk& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Disk (vector version)
    virtual Disk& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Disk::translate(Reals)", "Disk::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Disk (3 reals version)
    virtual Disk& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Disk::translate(Real, Real, Real)", "Disk::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Disk (1 key)
    virtual Disk& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Disk (2 keys)
    virtual Disk& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Disk
    virtual Disk& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Disk::rotate2d(Point, Real)", "Disk::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Disk (1 key)
    virtual Disk& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Disk (2 keys)
    virtual Disk& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Disk (3 keys)
    virtual Disk& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Disk
    virtual Disk& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Disk::rotate3d(Point, Reals, Real)", "Disk::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Disk
    virtual Disk& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Disk::rotate3d(Real, Real, Real)", "Disk::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Disk
    virtual Disk& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Disk::rotate3d(Real, Real, Real, Real)", "Disk::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Disk
    virtual Disk& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Disk::rotate3d(Point, Real, Real, Real)", "Disk::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Disk
    virtual Disk& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Disk::rotate3d(Point, Real, Real, Real, Real)", "Disk::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Disk (1 key)
    virtual Disk& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Disk (2 keys)
    virtual Disk& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Disk
    virtual Disk& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Disk::homothetize(Point, Real)", "Disk::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Disk
    virtual Disk& homothetize(real_t factor)
    {
      warning("deprecated", "Disk::homothetize(Real)", "Disk::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Disk (1 key)
    virtual Disk& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Disk
    virtual Disk& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Disk::pointReflect(Point)", "Disk::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Disk (1 key)
    virtual Disk& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Disk (2 keys)
    virtual Disk& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Disk
    virtual Disk& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Disk::reflect2d(Point, Reals)", "Disk::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Disk
    virtual Disk& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Disk::reflect2d(Point, Real, Real)", "Disk::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Disk (1 key)
    virtual Disk& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Disk (2 keys)
    virtual Disk& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Disk
    virtual Disk& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Disk::reflect3d(Point, Reals)", "Disk::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Disk
    virtual Disk& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Disk::reflect3d(Point, Real, Real, Real)", "Disk::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

typedef Disk Circle;

//===================================================================================================
/*!
  \class ParametrizedSurface
  definition of a ParametrizedSurface geometry in R^3 : x1(u,v), x2(u,v), x3(u,v)
  As gmsh does not support parametrized surface, to be exported, ParametrizedSurface can be split into _nbparts parts
    - smaller quadrangles or triangles (_linearPartition)
    - smaller nurbs (_splinePartition)  NOT YET AVAILABLE

  ParametrizedSurface constructors are based on a key-value system. Here are the available keys:
  - _parametrization: parametrization object is handled by Geometry
  - _partitioning: type of partition, one of _nonePartition*, _linearPartition, _splinePartition
  - _nbparts: number of partitions
  - _nnodes: to define the number of nodes on the bound edges
  - _hsteps: to define the local mesh steps on the bounds nodes
  - _domain_name: to define the domain name
  - _side_names/_edge_names: to define the side names
  - _vertex_names: to define the side of side names
  - _varnames: to define the variable names for print purpose
*/
//===================================================================================================
class ParametrizedSurface : public Surface
{
  protected:
    Partitioning partitioning_; //!< partitioning type  (one of _nonePartition*, _linearPartition _splinePartition)
    number_t nbParts_;          //!< number of partitions (>0)
    Transformation* transformation_;  //!< pointer to an additional linear transformation to apply to (0 by default: no additional linear transformation)

  public:
    //! default constructor
    ParametrizedSurface();
    //@{
    //! key-value constructor
    ParametrizedSurface(Parameter p1, Parameter p2);
    ParametrizedSurface(Parameter p1, Parameter p2, Parameter p3);
    ParametrizedSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    ParametrizedSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    ParametrizedSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    ParametrizedSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    ParametrizedSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    //@}

  private:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    ParametrizedSurface(const ParametrizedSurface&);                             //!< copy constructor
    virtual Geometry* clone() const { return new ParametrizedSurface(*this); }  //! virtual copy constructor for Geometry
    virtual Surface* cloneS() const { return new ParametrizedSurface(*this); }  //!< virtual copy constructor for Surface
    //! destructor
    virtual ~ParametrizedSurface() {if (transformation_ != nullptr) delete transformation_;}

    //! return Partitioning
    Partitioning partitioning() const {return partitioning_;}
    //! return nb of partitions
    number_t nbParts() const {return nbParts_;}
    //! return pointer to mesh related to geometry support of parametrization
    const Mesh& supportMesh() const {return *parametrization_->meshP();}

    //! format as string
    virtual string_t asString() const;

    //! access to child ParametrizedSurface object (const)
    virtual const ParametrizedSurface* parametrizedSurface() const {return this;}
    //! access to child ParametrizedSurface object
    virtual ParametrizedSurface* parametrizedSurface() {return this;}

    virtual number_t nbSides() const { return 4; }                //!< returns the number of sides
    virtual std::vector<const Point*> nodes() const;              //!< returns the nodes (including internal nodes if partition)
    virtual std::vector<const Point*> boundNodes() const;         //!< returns the nodes on boundary (including internal nodes if partition)
    std::vector<const Point*> nodesOnSide(number_t s = 0) const;  //!< return the nodes located on the side s>=1 (any side if s=0)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const;
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const;
    virtual Geometry& buildBoundary() const;                      //!< create boundary geometry of ParametrizedSurface (composite of ArcParametrization)

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a ParametrizedSurface
    virtual ParametrizedSurface& transform(const Transformation& t);
    //! apply a translation on a ParametrizedSurface (1 key)
    virtual ParametrizedSurface& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a ParametrizedSurface (vector version)
    virtual ParametrizedSurface& translate(std::vector<real_t> u)
    {
      warning("deprecated", "ParametrizedSurface::translate(Reals)", "ParametrizedSurface::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a ParametrizedSurface (3 reals version)
    virtual ParametrizedSurface& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "ParametrizedSurface::translate(Real, Real, Real)", "ParametrizedSurface::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a ParametrizedSurface (1 key)
    virtual ParametrizedSurface& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a ParametrizedSurface (2 keys)
    virtual ParametrizedSurface& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a ParametrizedSurface
    virtual ParametrizedSurface& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "ParametrizedSurface::rotate2d(Point, Real)", "ParametrizedSurface::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a ParametrizedSurface (1 key)
    virtual ParametrizedSurface& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a ParametrizedSurface (2 keys)
    virtual ParametrizedSurface& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a ParametrizedSurface (3 keys)
    virtual ParametrizedSurface& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a ParametrizedSurface
    virtual ParametrizedSurface& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "ParametrizedSurface::rotate3d(Point, Reals, Real)", "ParametrizedSurface::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a ParametrizedSurface
    virtual ParametrizedSurface& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "ParametrizedSurface::rotate3d(Real, Real, Real)", "ParametrizedSurface::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a ParametrizedSurface
    virtual ParametrizedSurface& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "ParametrizedSurface::rotate3d(Real, Real, Real, Real)", "ParametrizedSurface::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a ParametrizedSurface
    virtual ParametrizedSurface& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "ParametrizedSurface::rotate3d(Point, Real, Real, Real)", "ParametrizedSurface::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a ParametrizedSurface
    virtual ParametrizedSurface& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "ParametrizedSurface::rotate3d(Point, Real, Real, Real, Real)", "ParametrizedSurface::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a ParametrizedSurface (1 key)
    virtual ParametrizedSurface& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a ParametrizedSurface (2 keys)
    virtual ParametrizedSurface& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a ParametrizedSurface
    virtual ParametrizedSurface& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "ParametrizedSurface::homothetize(Point, Real)", "ParametrizedSurface::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a ParametrizedSurface
    virtual ParametrizedSurface& homothetize(real_t factor)
    {
      warning("deprecated", "ParametrizedSurface::homothetize(Real)", "ParametrizedSurface::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a ParametrizedSurface (1 key)
    virtual ParametrizedSurface& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a ParametrizedSurface
    virtual ParametrizedSurface& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "ParametrizedSurface::pointReflect(Point)", "ParametrizedSurface::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a ParametrizedSurface (1 key)
    virtual ParametrizedSurface& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a ParametrizedSurface (2 keys)
    virtual ParametrizedSurface& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a ParametrizedSurface
    virtual ParametrizedSurface& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "ParametrizedSurface::reflect2d(Point, Reals)", "ParametrizedSurface::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a ParametrizedSurface
    virtual ParametrizedSurface& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "ParametrizedSurface::reflect2d(Point, Real, Real)", "ParametrizedSurface::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a ParametrizedSurface (1 key)
    virtual ParametrizedSurface& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a ParametrizedSurface (2 keys)
    virtual ParametrizedSurface& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a ParametrizedSurface
    virtual ParametrizedSurface& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "ParametrizedSurface::reflect3d(Point, Reals)", "ParametrizedSurface::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a ParametrizedSurface
    virtual ParametrizedSurface& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "ParametrizedSurface::reflect3d(Point, Real, Real, Real)", "ParametrizedSurface::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }

};

//===================================================================================================
/*!
  \class SplineSurface
  definition of a SplineSurface geometry in R^3 from a Nurbs object (non uniform rational Bspline)
  only nurbs are available either approximation nurbs or interpolation nurbs (default when subtype is not specified)

  SplineSurface constructors are based on a key-value system. Here are the available keys:
  - _vertices: list of control points
  - _spline: spline object
  - _spline_type: type of spline one of _C2Spline, _CatmullRomSpline, _BSpline, _BezierSpline, _Nurbs
  - _spline_subtype: subtype of spline, one of _SplineInterpolation, _SplineApproximation
  - _spline_BC: spline boundary condition, one of _naturalBC, _clampedBC, _periodicBC
  - _degree: degree of spline (default 3)
  - _nbu: number of control/interpolation points in u-direction (_Nurbs only)
  - _spline_parametrization: spline parametrization type, one of _xParametrization, _uniformParametrization,_chordalParametrization, _centripetalParametrization
  - _weights: weights of control points (for _BSpline only)
  - _nnodes: to define the number of nodes on the arc
  - _hsteps: to define the local mesh steps on the bounds
  - _domain_name: to define the domain name
  - _side_names/_edge_names: to define the side names
  - _vertex_names: to define the side of side names
  - _varnames: to define the variable names for print purpose
*/
//===================================================================================================
class SplineSurface : public Surface
{
  protected:
    Spline* spline_;     //!< spline pointer (nurbs)

  public:
    //! default constructor
    SplineSurface();
    //@{
    //! key-value constructor
    SplineSurface(Parameter p1);
    SplineSurface(Parameter p1, Parameter p2);
    SplineSurface(Parameter p1, Parameter p2, Parameter p3);
    SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    //@}

  private:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    SplineSurface(const SplineSurface&);                                  //!< copy constructor
    void copy(const SplineSurface&);                                      //!< real copy
    SplineSurface& operator=(const SplineSurface&);                       //!< assign operator
    virtual Geometry* clone() const { return new SplineSurface(*this); }  //! virtual copy constructor for Geometry
    virtual Surface* cloneS() const { return new SplineSurface(*this); }  //!< virtual copy constructor for Surface
    virtual ~SplineSurface();

    const Spline* spline() const {return spline_;};
    //! access to child SplineSurface object (const)
    virtual const SplineSurface* splineSurface() const {return this;}
    //! access to child SplineSurface object
    virtual SplineSurface* splineSurface() {return this;}
    string_t asString() const; //!< format as string
    virtual Geometry& buildBoundary() const;      //!< create boundary geometry of SplineSurface (composite of SplineArc (BSpline))

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a SplineSurface
    virtual SplineSurface& transform(const Transformation& t);
    //! apply a translation on a SplineSurface (1 key)
    virtual SplineSurface& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a SplineSurface (vector version)
    virtual SplineSurface& translate(std::vector<real_t> u)
    {
      warning("deprecated", "SplineSurface::translate(Reals)", "SplineSurface::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a SplineSurface (3 reals version)
    virtual SplineSurface& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "SplineSurface::translate(Real, Real, Real)", "SplineSurface::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a SplineSurface (1 key)
    virtual SplineSurface& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a SplineSurface (2 keys)
    virtual SplineSurface& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a SplineSurface
    virtual SplineSurface& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "SplineSurface::rotate2d(Point, Real)", "SplineSurface::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a SplineSurface (1 key)
    virtual SplineSurface& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a SplineSurface (2 keys)
    virtual SplineSurface& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a SplineSurface (3 keys)
    virtual SplineSurface& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a SplineSurface
    virtual SplineSurface& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "SplineSurface::rotate3d(Point, Reals, Real)", "SplineSurface::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a SplineSurface
    virtual SplineSurface& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "SplineSurface::rotate3d(Real, Real, Real)", "SplineSurface::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a SplineSurface
    virtual SplineSurface& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "SplineSurface::rotate3d(Real, Real, Real, Real)", "SplineSurface::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a SplineSurface
    virtual SplineSurface& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "SplineSurface::rotate3d(Point, Real, Real, Real)", "SplineSurface::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a SplineSurface
    virtual SplineSurface& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "SplineSurface::rotate3d(Point, Real, Real, Real, Real)", "SplineSurface::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a SplineSurface (1 key)
    virtual SplineSurface& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a SplineSurface (2 keys)
    virtual SplineSurface& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a SplineSurface
    virtual SplineSurface& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "SplineSurface::homothetize(Point, Real)", "SplineSurface::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a SplineSurface
    virtual SplineSurface& homothetize(real_t factor)
    {
      warning("deprecated", "SplineSurface::homothetize(Real)", "SplineSurface::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a SplineSurface (1 key)
    virtual SplineSurface& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a SplineSurface
    virtual SplineSurface& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "SplineSurface::pointReflect(Point)", "SplineSurface::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a SplineSurface (1 key)
    virtual SplineSurface& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a SplineSurface (2 keys)
    virtual SplineSurface& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a SplineSurface
    virtual SplineSurface& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "SplineSurface::reflect2d(Point, Reals)", "SplineSurface::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a SplineSurface
    virtual SplineSurface& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "SplineSurface::reflect2d(Point, Real, Real)", "SplineSurface::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a SplineSurface (1 key)
    virtual SplineSurface& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a SplineSurface (2 keys)
    virtual SplineSurface& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a SplineSurface
    virtual SplineSurface& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "SplineSurface::reflect3d(Point, Reals)", "SplineSurface::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a SplineSurface
    virtual SplineSurface& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "SplineSurface::reflect3d(Point, Real, Real, Real)", "SplineSurface::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }

    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< interface to spline parametrization
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< interface to spline parametrization
};

inline Vector<real_t> parametrization_SplineSurface(const Point& pt, Parameters& pars, DiffOpType d = _id)  //! extern parametrization call
{return reinterpret_cast<const SplineSurface*>(pars("geometry").get_p())->funParametrization(pt, pars, d);}
inline Vector<real_t> invParametrization_SplineSurface(const Point& pt, Parameters& pars, DiffOpType d = _id) //! extern invParametrization call
{return reinterpret_cast<const SplineSurface*>(pars("geometry").get_p())->invParametrization(pt, pars, d);}


//============================================================================================
/*!
   \class SetOfElems
   This class defines an initial mesh from a list of points (pts), a list of elements (elems)
   and a list of boundaries (bounds).
     Each element is defined by N numbers corresponding to the rank of its N vertices
   in the vector pts ; the numbers start from 1. Thus, elems[k][i] is the number of the
   vertex (i+1) of the element number (k+1) (if the elements are numbered starting
   from 1), and 1 <= elems[k][i] <= pts.size().
     The boundaries of the polygonal domain defined by the elements are also defined
   by point numbers with the same convention. Each boundary is defined by the list of
   numbers of the points they belong to, given in any order.
     The last argument nbsubdiv is the number of subdivisions wanted. The initial
   mesh corresponds to nbsubdiv=0.
   -> Important: the subdivision algorithm generate a new point on each edge of the mesh.
      It belongs to boundary B if the two end points of the edge belong to the boundary B ;
      it is an internal point otherwise. Owing to this rule, in order to get a correct result,
      care must be taken in the definition of the boundaries since too few boundaries may
      lead to get boundary points that are in fact inside the domain. For instance,
      subdividing one single triangle leads to mandatory define its three edges as boundaries.
*/
//============================================================================================
class SetOfElems : public Geometry
{
  private:
    std::vector<Point> pts_; //!< list of points
    const std::vector<std::vector<number_t> >& elems_; //!< list of elements (an element is defined by its list of nodes)
    const std::vector<std::vector<number_t> >& bounds_; //!< list of elements on boundary
    ShapeType elemShape_;             //!< shape of the elements in the mesh
    number_t nbsubdiv_;           //!< number of subdivisions to be performed to get the final mesh
    const number_t nbElems_, nbBounds_;

  public:
    //! default constructor
    SetOfElems(const std::vector<Point>& pts, const std::vector<std::vector<number_t> >& elems,
               const std::vector<std::vector<number_t> >& bounds, const std::vector<string_t>& names,
               const ShapeType esh, const number_t nbsubdiv = 1)
      : Geometry(BoundingBox(pts), BoundingBox(pts).dim(), string_t("Omega"), _setofelems),
        pts_(pts), elems_(elems), bounds_(bounds), elemShape_(esh), nbsubdiv_(nbsubdiv),
        nbElems_(elems_.size()), nbBounds_(bounds_.size())
    { sideNames_ = names; }

    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new SetOfElems(*this); } //!< virtual copy constructor

    virtual std::vector<Point*> nodes(); //!< list of every point (non const)
    virtual std::vector<const Point*> nodes() const; //!< list of every point (const)

    // access functions to data members
    std::vector<Point>& pts() { return pts_; } //!< return list of points (non const)
    const std::vector<Point>& pts() const { return pts_; } //!< returns list of points (const)
    const std::vector<std::vector<number_t> >& elems() const { return elems_; } //!< returns list of elements
    const std::vector<std::vector<number_t> >& bounds() const { return bounds_; } //!< returns list of elements on boundary
    ShapeType elemShape() const { return elemShape_; } //!< returns shape of the elements
    virtual number_t nbSubdiv() const { return nbsubdiv_; } //!< returns number of subdivision
    virtual number_t nbSides() const { return bounds_.size(); } //!< returns the number of sides

    //! access to child SetOfElems object (const)
    virtual const SetOfElems* setofelems() const {return this;}
    //! access to child SetOfElems object
    virtual SetOfElems* setofelems() {return this;}

    //------------------------------------------------
    //         transformations facilities
    //------------------------------------------------
    //! apply a geometrical transformation on a SetOfElems
    virtual SetOfElems& transform(const Transformation& t);
    //! apply a translation on a SetOfElems (1 key)
    virtual SetOfElems& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a SetOfElems (vector version)
    virtual SetOfElems& translate(std::vector<real_t> u)
    {
      warning("deprecated", "SetOfElems::translate(Reals)", "SetOfElems::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a SetOfElems (3 reals version)
    virtual SetOfElems& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "SetOfElems::translate(Real, Real, Real)", "SetOfElems::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a SetOfElems (1 key)
    virtual SetOfElems& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a SetOfElems (2 keys)
    virtual SetOfElems& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a SetOfElems
    virtual SetOfElems& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "SetOfElems::rotate2d(Point, Real)", "SetOfElems::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a SetOfElems (1 key)
    virtual SetOfElems& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a SetOfElems (2 keys)
    virtual SetOfElems& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a SetOfElems (3 keys)
    virtual SetOfElems& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a SetOfElems
    virtual SetOfElems& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "SetOfElems::rotate3d(Point, Reals, Real)", "SetOfElems::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a SetOfElems
    virtual SetOfElems& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "SetOfElems::rotate3d(Real, Real, Real)", "SetOfElems::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a SetOfElems
    virtual SetOfElems& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "SetOfElems::rotate3d(Real, Real, Real, Real)", "SetOfElems::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a SetOfElems
    virtual SetOfElems& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "SetOfElems::rotate3d(Point, Real, Real, Real)", "SetOfElems::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a SetOfElems
    virtual SetOfElems& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "SetOfElems::rotate3d(Point, Real, Real, Real, Real)", "SetOfElems::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a SetOfElems (1 key)
    virtual SetOfElems& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a SetOfElems (2 keys)
    virtual SetOfElems& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a SetOfElems
    virtual SetOfElems& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "SetOfElems::homothetize(Point, Real)", "SetOfElems::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a SetOfElems
    virtual SetOfElems& homothetize(real_t factor)
    {
      warning("deprecated", "SetOfElems::homothetize(Real)", "SetOfElems::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a SetOfElems (1 key)
    virtual SetOfElems& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a SetOfElems
    virtual SetOfElems& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "SetOfElems::pointReflect(Point)", "SetOfElems::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a SetOfElems (1 key)
    virtual SetOfElems& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a SetOfElems (2 keys)
    virtual SetOfElems& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a SetOfElems
    virtual SetOfElems& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "SetOfElems::reflect2d(Point, Reals)", "SetOfElems::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a SetOfElems
    virtual SetOfElems& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "SetOfElems::reflect2d(Point, Real, Real)", "SetOfElems::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a SetOfElems (1 key)
    virtual SetOfElems& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a SetOfElems (2 keys)
    virtual SetOfElems& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a SetOfElems
    virtual SetOfElems& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "SetOfElems::reflect3d(Point, Reals)", "SetOfElems::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a SetOfElems
    virtual SetOfElems& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "SetOfElems::reflect3d(Point, Real, Real, Real)", "SetOfElems::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

} // end of namespace xlifepp

#endif // GEOMETRIES_2D_HPP
