/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Parametrization.cpp
  \authors E. Lunéville
  \since 10 jan 2017
  \date 27 jan 2020

  \brief Implementation of the xlifepp::Parametrization member functions and related stuff
*/

#include "Parametrization.hpp"
#include "geometries1D.hpp"
#include "geometries2D.hpp"
#include "geometries3D.hpp"
#include "Mesh.hpp"

namespace xlifepp
{
// constructors
// parametrization from Geometry (mainly composite)
Parametrization::Parametrization(Geometry * g)
  : geom_p(g), geomSupport_p(nullptr), meshSupport_p(nullptr), f_p(nullptr), curvature_p(nullptr), length_p(nullptr),
    invParametrization_p(nullptr), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr),
    name(""), dim(0), dimg(0), np(1000),  s0(0), s1(0), contOrder(_regCinf)
{
  if (g==nullptr) return; // default constructor
  if (g->isCanonical()) error("free_error""Parametrization(Geometry *) is not designed for canonical geometry");
  dimg=g->dim();
  dim=g->dimPoint();
  switch (dimg)
  {
    case 1:
      geomSupport_p=new Segment(_v1=Point(0),_v2=Point(1),_domain_name="["+tostring(0)+", "+tostring(1)+"]");
      break;
    default: break;  //TO BE DONE
  }
}

// parametrization from a support Geometry and a  function
Parametrization::Parametrization(const Geometry& geo, par_fun f, const Parameters& pars, const string_t& na, dimen_t dimp)
  : geom_p(nullptr), f_p(f),  curvature_p(nullptr), length_p(nullptr), invParametrization_p(nullptr), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr),
    name(na), dim(dimp), np(1000), params(pars), s0(0), s1(0), contOrder(_regCinf)
{
  geomSupport_p=geo.clone();
  init();
}

// 1d parametrization from segment [a,b]
Parametrization::Parametrization(real_t a, real_t b, par_fun f, const Parameters& pars, const string_t& na, dimen_t dimp)
  : geom_p(nullptr), f_p(f),  curvature_p(nullptr), length_p(nullptr), invParametrization_p(nullptr), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr),
    name(na), dim(dimp), np(1000), params(pars), s0(0), s1(0), contOrder(_regCinf)
{
  geomSupport_p=new Segment(_v1=Point(a),_v2=Point(b),_domain_name="["+tostring(a)+", "+tostring(b)+"]");
  init();
}

Parametrization::Parametrization(real_t a, real_t b, par_fun f, par_fun invf, const Parameters& pars, const string_t& na, dimen_t dimp)
  : geom_p(nullptr), f_p(f),  curvature_p(nullptr), length_p(nullptr), invParametrization_p(invf), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr),
    name(na), dim(dimp), np(1000), params(pars), s0(0), s1(0), contOrder(_regCinf)
{
  geomSupport_p=new Segment(_v1=Point(a),_v2=Point(b),_domain_name="["+tostring(a)+", "+tostring(b)+"]");
  init();
}

// 2d parametrization from rectangle [a,b]x[c,d]
Parametrization::Parametrization(real_t a, real_t b, real_t c, real_t d, par_fun f, const Parameters& pars, const string_t& na, dimen_t dimp)
  : geom_p(nullptr), f_p(f),  curvature_p(nullptr), length_p(nullptr), invParametrization_p(nullptr), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr),
    name(na), dim(dimp), np(1000), params(pars), s0(0), s1(0), contOrder(_regCinf)
{
  geomSupport_p=new Rectangle(_xmin=a,_xmax=b,_ymin=c,_ymax=d,
                _domain_name="["+tostring(a)+", "+tostring(b)+"]x["+tostring(c)+", "+tostring(d)+"]");
  init();
}

Parametrization::Parametrization(real_t a, real_t b, real_t c, real_t d, par_fun f, par_fun invf, const Parameters& pars, const string_t& na, dimen_t dimp)
  : geom_p(nullptr), f_p(f),  curvature_p(nullptr), length_p(nullptr), invParametrization_p(invf), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr),
    name(na), dim(dimp), np(1000), params(pars), s0(0), s1(0), contOrder(_regCinf)
{
  geomSupport_p=new Rectangle(_xmin=a,_xmax=b,_ymin=c,_ymax=d,
                _domain_name="["+tostring(a)+", "+tostring(b)+"]x["+tostring(c)+", "+tostring(d)+"]");
  init();
}

//copy constructor
Parametrization::Parametrization(const Parametrization& par)
{
  meshSupport_p=nullptr; geomSupport_p=nullptr; geom_p=nullptr;
  copy(par);
}

//assign operator
Parametrization& Parametrization::operator=(const Parametrization& par)
{
  if (this==&par) return *this;  //nothing to do
  clearPointers();
  copy(par);
  return *this;
}

Parametrization::~Parametrization()  //delete symbolic function pointers
{
  clearPointers();
}

//! initialization, set dim
void Parametrization::init()
{
  if(dim==0)
  {
     Point p=geomSupport_p->firstNode();
     if (p.size()==0) //use dimension to get a point
     {
      dimen_t d=geom_p->dimPoint();
      switch(d)
      {
        case 1: p=Point(0.); break;
        case 2: p=Point(0.,0.); break;
        case 3: p=Point(0.,0.,0.);  break;
        default: where("Parametrization::init()"); error("dim_not_in_range", 1, 3);
      }
     }
     Vector<real_t> q;
     if (f_p!=nullptr) q=f_p(p,params,_id);
     dim=q.size();
  }
  dimg=geomSupport_p->dim();
  meshSupport_p=nullptr;
  geom_p=nullptr;
  s0=0;s1=0;
  contOrder=_regCinf;
  periods.resize(dimg);
  periods*=0;
}

void Parametrization::clearPointers()
{
  if (geomSupport_p!=nullptr) delete geomSupport_p;
  meshSupport_p=nullptr;
  geomSupport_p=nullptr;
  std::vector<Parameter*>::iterator it=params.list_.begin();
  for (;it!=params.list_.end();++it)
    if ((*it)->type_==_pointer && (*it)->name_[0]=='@') delete reinterpret_cast<const SymbolicFunction*>((*it)->p_);
}

// copy par to a cleaned parametrization
void Parametrization::copy(const Parametrization& par)
{
  if (geomSupport_p!=nullptr) delete geomSupport_p;
  if (meshSupport_p!=nullptr) delete meshSupport_p;
  meshSupport_p=nullptr;
  geom_p=par.geom_p; //shared geometry pointer
  geomSupport_p=nullptr;
  if (par.geomSupport_p != nullptr) geomSupport_p=par.geomSupport_p->clone();
  f_p=par.f_p;
  curabcs_=par.curabcs_;
  curvature_p=par.curvature_p;
  length_p=par.length_p;
  invParametrization_p=par.invParametrization_p;
  normal_p=par.normal_p;
  tangent_p=par.tangent_p;
  curabc_p=par.curabc_p;
  christoffel_p=par.christoffel_p;
  name=par.name;
  dimg=par.dimg;
  dim=par.dim;
  params=par.params;
  np=par.np;
  contOrder=par.contOrder;
  periods=par.periods;
  nbOfMaps=par.nbOfMaps;
  singularSide=par.singularSide;
  //hard copy of symbolic function pointers of params
  std::vector<Parameter*>::iterator it=params.list_.begin();
  for (;it!=params.list_.end();++it)
    if ((*it)->type_==_pointer && (*it)->name_[0]=='@') (*it)->p_=new SymbolicFunction(*reinterpret_cast<const SymbolicFunction*>((*it)->p_));
  if (par.meshSupport_p!=nullptr) meshSupport_p = new Mesh(*par.meshSupport_p);  //hard copy
}

/*! locate canonical geometry when parametrization of a piecewise geometry
    1D: assuming global parametrization t from 0 to 1 (n=nbParts number of canonical geometries)
    use the connectedParts_ structure (vector<vector<number_t>>) that provides a collection of connected parts
    each parts being a sorted collection of canonical geometries
       connectedParts_[0]  : geom01 geom02 ...  n0 canonical geometries   parametrized from 0 to n0/n
       connectedParts_[1]  : geom11 geom12 ...  n1 canonical geometries   parametrized from n0 to (n0+n1)/n
       ..
       connectedParts_[m]  : geom11 geom12 ...  nm canonical geometries   parametrized from (n0+n1..+nm-1)/n to 1
*/
Geometry* Parametrization::locateGeometry(Point& t) const
{
  if (geom_p->isCanonical()) return geom_p;
  if (dimg==1)  // curve parametrized from 0 to 1 (nbparts_ the number of canonical geometries)
  {
    real_t x=t[0];
    RealPair xm=geom_p->parametrization().bounds(_x);
    if(x<xm.first-theTolerance) error("free_error"," out of parametrization bounds of "+geom_p->domName());
    else x=std::max(x,xm.first);
    if(x>xm.second+theTolerance) error("free_error"," out of parametrization bounds of "+geom_p->domName());
    else x=std::min(x,xm.second-theTolerance);  //to manage the special case x=xmax
    x*=geom_p->nbParts();
    int n=std::floor(x);
    x-=n;
    std::vector<std::vector<int_t> >::const_iterator it=geom_p->connectedParts().begin();
    int s=it->size();
    while (n>s && it!=geom_p->connectedParts().end()) {it++; s+=it->size(); }
    if(it!=geom_p->connectedParts().end())
    {
        n-=s-it->size();
        if(n==it->size()) n--;
    }
    else
    {
        it--;
        n=it->size()-1;
        x=1.;
    }
    Geometry* g=geom_p->components()[std::abs((*it)[n])];  //canonical geometry
    if((*it)[n]<0) x=1-x;  //reverse arc
    t[0]=x;
    return g;
  }
  error("free_error","Parametrization::locateGeometry is not implemented in case of piecewise surface");
  return nullptr;
}

Point Parametrization::operator()(const Point& t, DiffOpType d) const  //compute parametrization
{
  if (f_p!=nullptr) return Point(f_p(t,const_cast<Parameters&>(params),d));
  if (geom_p!=nullptr && geom_p->connectedParts().size()>0)  //piecewise geometry
  {
    Point tt(t);
    Geometry* g=locateGeometry(tt);
    const Parametrization& pg=g->parametrization();
    return pg(tt,d);
  }
  error("free_error","unable to find a way to compute parametrization");
  return Point(); // dummy return
}

//!< inverse function of the parametrization
// when composite geometry with N components, the n-th geometry (n=1,N) has parameter in [(n-1)/N, n/N[
Point Parametrization::toParameter(const Point& p) const
{
  Point q;
  if(p.size()==0) return q;     // nothing done when null point given
  if(invParametrization_p!=nullptr)
  {
      q=Point(invParametrization_p(p,const_cast<Parameters&>(params),_id));
      return q;
  }
  if(dimg>2) parfun_error("toParameter",_id);
  //estimate starting point using linear interpolation
  q.resize(dimg);
  if(dimg==1) q[0]=norm(p-(*this)(0))/norm((*this)(1)-(*this)(0));
  if(dimg==2) // based on distance to patch vertices
  {
    real_t d1=norm((*this)(0.,0.)-p), d2=norm((*this)(1.,0.)-p),
           d3=norm((*this)(1.,1.)-p), d4=norm((*this)(0.,1.)-p);
    q[0]=0.5*(d1/(d1+d2)+d4/(d4+d3)); q[1]=0.5*(d1/(d1+d4)+d2/(d2+d3));
  }
  //general case: solve f(q)=p, using Newton method on min |f(q)-p|^2/2
  Point R,d1,d11;
  real_t g1,h11,res,eps;
  R=(*this)(q)-p;
  res=norm(R);
  eps=1E-8*res;
  if(dimg==1)  // dim 1
  {
     while(res>eps)
     {
       d1=(*this)(q,_d1); d11=(*this)(q,_d11);
       g1=dot(R,d1);
       h11=dot(d1,d1)+dot(R,d11);
       q[0]-=g1/h11;
       R=(*this)(q)-p;
       res=norm(R);
     }
     return q;
  }
  Point d2,d22,d12;
  real_t g2,h12,h22,det,t1,t2;
  while(res>eps) // dim 2
  {
    d1=(*this)(q,_d1); d2=(*this)(q,_d2);
    d11=(*this)(q,_d11); d22=(*this)(q,_d22); d12=(*this)(q,_d12);
    g1=dot(R,d1); g2=dot(R,d2);
    h11=dot(d1,d1)+dot(R,d11); h22=dot(d2,d2)+dot(R,d22); h12=dot(d1,d2)+dot(R,d12);
    det=1/(h11*h22-h12*h12);
    t1=(h22*g1-h12*g2)*det; t2=(h11*g2-h12*g1)*det;
    q[0]-=t1;q[1]-=t2;
    R=(*this)(q)-p;
    res=norm(R);
  }
  return q;
}

/*! extended inversion of parametrization
    constructs parameters Pi associated to nodes Mi, i.e G(Pi) = Mi
    when parametrization is a multi-map parametrization :
      all Pi must be associated to the same map and the map index is returned
      if (Pi) may be associated to several maps, the best one is selected according to criteria handled by the parametrization
         dmin = min(dist(Pi,parametrization 'singular' points))
         choose parametrization such that dmin is maximum
*/

void Parametrization::toParameters(const std::vector<Point*>& nodes, number_t& mapIndex, std::vector<Point*>& parnodes) const
{
    number_t n=nodes.size();
    parnodes.assign(n,nullptr);
    std::vector<Point> pars(n);
    if(params.contains("dist")) params("dist")=0.;
    else params << Parameter(0.,"dist");
    real_t dist=0, dmin, d;
    bool restaure=false;
    for(number_t p=0;p<nbOfMaps;p++) //loop on maps
    {
        params("mapIndex")=p+1;
        dmin=theRealMax;
        auto itp=pars.begin();
        for(auto itn=nodes.begin();itn!=nodes.end();++itn,++itp) //loop on points
        {
            *itp=toParameter(**itn);
            d=params("dist");
            if(d<dmin) dmin=d;
        }
        if(dmin>dist) // choose this parametrization
        {
            mapIndex=p+1;
            itp=pars.begin();
            for(auto itn=parnodes.begin();itn!=parnodes.end();++itn,++itp)
            {
                if(*itn!=nullptr) delete *itn;
                *itn=new Point(*itp);
            }
            dist=dmin;
            restaure=false;
        }
        else restaure=true;
    }
    // correct periodic effects on each parameter
    // choose components xi (+0, +1 ,-1) leading to an element in parameters space
    // such that the edge lengths are less than lmax
    if(restaure)
    {
       auto itp=pars.begin();
       for(auto itn=parnodes.begin();itn!=parnodes.end();++itn,++itp) *itp=**itn;
    }
    std::vector<Point> pts=pars;
    bool change=false;
    real_t lmax=0.6;
    for(int j=0;j<periods.size();j++)
    {
      if(periods[j]!=0) // correct j-period
      {
        int nbtry=0;
        int nbc=0;
        while(nbtry<3)
        {
          bool first=false;
          thePrintStream<<"nbtry="<<nbtry<<" pts ="<<pts<<eol;
          for(int i=0;i<n;i++)
          {
            int ip=i+1; if(ip==n) ip=0;
            if(pts[ip][j] > pts[i][j]+lmax) {pts[ip][j]-=1;nbc++;if(ip==0) first=true;}
            else
                if(pts[ip][j] < pts[i][j]-lmax) {pts[ip][j]+=1;nbc++;if(ip==0) first=true;}
          }
          if(first)
          {
             nbtry++;
             pts=pars;
             if(nbtry==1)
             {
               if(pts[0][j]<0.5)  pts[0][j]+=1; else pts[0][j]-=1;
             }
             if(nbtry==2)
             {
               if(pts[0][j]<0.5)  pts[0][j]-=1; else pts[0][j]+=1;
             }
             nbc=1;;
          }
          else nbtry=3;
        }
        if(nbc>0) {change=true;pars=pts;thePrintStream<<"new pts ="<<pts<<eol;}
      }
    }
    if(change)
    {
       auto itp=pts.begin();
       for(auto itn=parnodes.begin();itn!=parnodes.end();++itn,++itp)
       {
         delete *itn;
         *itn=new Point(*itp);
       }
    }
}

//! compute parameter derivatives dt related to dp
//  dp = [J(t)]*dt (t parameter computed outside)
Point Parametrization::toDerParameter(const Point& t, const Point &dp) const
{
 Point dt;
 number_t n=t.size();  //parameter size
 dt.resize(t.size(),0.);
 real_t dw;
 Point d1=(*this)(t,_d1);
 if(n==1)
 {
    dw=norm(d1);
    if(std::abs(dw)> theTolerance) dt[0]=norm(dp)/dw;
    return dt;
 }
 if(n==2)
 {
   Point d2=(*this)(t,_d2);
   real_t a=dot(d1,d1), b=dot(d2,d2), c=dot(d1,d2), e=dot(dp,d1), f=dot(dp,d2), d=a*b-c*c;
   dt[0]=(b*e-c*f)/d;
   dt[1]=(a*f-c*e)/d;
   return dt;
 }
  error("free_error","Parametrization::toDerParameter no yet available for parameter dim > 2");
  return dt;  //fake return
}

//!< local length of a curve: sqrt(d1f1^2+d1f2^2+[d1f3^2]) and sqrt(d2f1^2+d2f2^2+[d2f3^2]) if 2D parametrization
Vector<real_t> Parametrization::lengths(const Point& t, DiffOpType d) const
{
  if(length_p!=nullptr) return length_p(t,const_cast<Parameters&>(params),d); // use explicit function
  if(f_p!=nullptr)  // use implicit function
  {
   Vector<real_t> ls(dimg);
   if(d==_id)
    {
        ls[0]=norm2(f_p(t,const_cast<Parameters&>(params),_d1));
        if(dim==1)
        {
          if(dimg==1) ls[0]=std::sqrt(1+ls[0]*ls[0]);  // case x->y(x)
          return ls;
        }
        if(dimg>1) ls[1]=norm2(f_p(t,const_cast<Parameters&>(params),_d2));
        if(dimg>2) ls[2]=norm2(f_p(t,const_cast<Parameters&>(params),_d3));
        return ls;
    }
   parfun_error("length",d);
  }
  if(geom_p!=nullptr && geom_p->connectedParts().size()>0)  //piecewise geometry
  {
     Point tt(t);
     Geometry* g=locateGeometry(tt);
     return g->parametrization().lengths(tt,d)*real_t(geom_p->nbParts());

  }
  error("free_error","unable to find a way to compute lengths");
  return Vector<real_t>(); // dummy return
}

/*! curvature
    2D/3D curve: ||x"(t) ^ x'(t)||/||x'(t)||^3
    3D surface: eigen values of the weingarten map (principal curvatures)
*/
Vector<real_t> Parametrization::curvatures(const Point& t, DiffOpType d) const
{
  if(curvature_p!=nullptr) return curvature_p(t,const_cast<Parameters&>(params),d);
  if(d!=_id) parfun_error("curvatures",d);
  if(f_p!=nullptr)  // use implicit function
  {
      if(dimg==1) // curve
      {
      Vector<real_t> d2=f_p(t,const_cast<Parameters&>(params),_d11), d1=f_p(t,const_cast<Parameters&>(params),_d1);
      Vector<real_t> cs(1);
      switch(dim)
      {
        case 1: cs[0]= norm2(d2)/std::pow(lengths(t,d)[0],3); break;
        case 2: cs[0]= norm2(crossProduct2D(d2,d1))/std::pow(norm2(d1),3); break;
        default: cs[0]= norm2(crossProduct(d2,d1))/std::pow(norm2(d1),3);
      }
      return cs;
      }
      if(dimg==2) //uv surface
      {
        Vector<real_t> cs(2,0.);
        if(dim==3) // 3D surface (if dim ==2 then plane surface -> 0 curvatures)
        {
          Matrix<real_t> W=weingarten(t);
          real_t K=W(1,1)*W(2,2)-W(2,1)*W(1,2);  //Gauss curvature
          real_t M=0.5*(W(1,1)+W(2,2));          //mean curvature
          real_t s=std::sqrt(std::max(M*M-K,0.));//M^2-K may be non positive due to round error
          cs[0]=M+s; cs[1]=M-s;
        }
        return cs;
       }
      parfun_error("curvatures",d);
  }
  if(geom_p!=nullptr && geom_p->connectedParts().size()>0)  //piecewise geometry
  {
     Point tt(t);
     Geometry* g=locateGeometry(tt);
     return g->parametrization().curvatures(tt,d);
  }
  error("free_error","unable to find a way to compute curvatures");
  return Vector<real_t>(); // dummy return
}

real_t Parametrization::gaussCurvature(const Point& P, DiffOpType d) const
{
  if(dim!=3 || dimg!=2) parmap_error("gaussCurvature",dim);
  if(d!=_id) parfun_error("gaussCurvature",d);
  Vector<real_t> cs=curvatures(P,d);
  return cs[0]*cs[1];
}

real_t Parametrization::meanCurvature(const Point& P, DiffOpType d ) const
{
  if(dim!=3 || dimg!=2) parmap_error("meanCurvature",dim);
  if(d!=_id) parfun_error("meanCurvature",d);
  Vector<real_t> cs=curvatures(P,d);
  return 0.5*(cs[0]+cs[1]);
}

/*! torsion only for  3D curve: det(d1f, d11f, d111f)/|d1f x d11f|
*/
real_t Parametrization::torsion(const Point& t, DiffOpType d) const
{
  if(dim!=3 || dimg!=1) parmap_error("torsion",dim);
  if(f_p==nullptr) error("null_pointer","f_p");
  if(d!=_id) parfun_error("torsion",d);
  Vector<real_t> d1=f_p(t,const_cast<Parameters&>(params),_d1),
                d11=f_p(t,const_cast<Parameters&>(params),_d11),
                d111=f_p(t,const_cast<Parameters&>(params),_d111);
  return dot(d1,crossProduct(d11,d111))/norm2(crossProduct(d1,d11));
}

//!< normal vector
Vector<real_t> Parametrization::normals(const Point& t, DiffOpType d) const
{

  if(normal_p!=nullptr) return normal_p(t,const_cast<Parameters&>(params),d);
  if(d!=_id) parfun_error("normal",d);
  if(f_p!=nullptr)  // use implicit function
  {
   // compute normal from derivatives
   if(dim==1 && dimg==1) //x->y(x)
    {
      Vector<real_t> d1(2);
      d1[0]=-f_p(t, const_cast<Parameters&>(params),_d1)[0];
      d1[1]=1.;
      return d1/norm2(d1);
    }
   if(dim==2 && dimg==1)  // curve in 2D
    {
      Vector<real_t> d1=f_p(t, const_cast<Parameters&>(params),_d1);
      real_t x=d1[0];
      d1[0]=d1[1];
      d1[1]=-x;
      return d1/norm2(d1);
    }
   if(dim==2 && dimg==2)  return Vector<real_t>(0); // Plane surface (no normals , return void vector)
   if(dim==3)
    {
      if(dimg==1)  //3D curve, Frenet normals: N = dsT/c =(d1f x d11f) x d1f) / |(d1f x d11f) x d1f|,   binormal bN = T x N
      {
         Vector<real_t> d1 =f_p(t, const_cast<Parameters&>(params),_d1),
                        d11=f_p(t, const_cast<Parameters&>(params),_d11);
         Vector<real_t> ns=crossProduct(crossProduct(d1,d11),d1);
         ns/=norm(ns);
         Vector<real_t> bn = crossProduct(d1/norm(d1),ns);
         ns.insert(ns.end(),bn.begin(),bn.end());
         return ns;
      }
      if(dimg==2) // 3d surface normal: d1f x d2f / |d1f x d2f|
      {
          Vector<real_t> d1=f_p(t, const_cast<Parameters&>(params),_d1), d2=f_p(t, const_cast<Parameters&>(params),_d2);
          Vector<real_t> d1xd2=crossProduct(d1,d2);
          return d1xd2/norm(d1xd2);
      }
    }
  }
  if(geom_p!=nullptr && geom_p->connectedParts().size()>0)  //piecewise geometry
  {
     Point tt(t);
     Geometry* g=locateGeometry(tt);
     return g->parametrization().normals(tt,d);
  }
  error("free_error","unable to find a way to compute normals");
  return Vector<real_t>(); // dummy return
}

Vector<real_t> Parametrization::normal(const Point& P, DiffOpType d) const   //!< shortcut to the first normal vector
{
     Vector<real_t> ns=normals(P,d);
     if(dim==3 && dimg==1) ns.resize(3);
     return ns;
}

//! binormal vector (only for 3D curve)
Vector<real_t> Parametrization::binormal(const Point& P, DiffOpType d) const
{
    if(dim!=3 || dimg!=1) error("free_error","binormal vector only available for 3D curve");
    Vector<real_t> ns=normals(P,d);
    return Vector<real_t>(ns.begin()+3,ns.end());
}

//!< tangent vectors
Vector<real_t> Parametrization::tangents(const Point& P, DiffOpType d) const
{
  if(tangent_p!=nullptr) return tangent_p(P,const_cast<Parameters&>(params),d);
  if(d!=_id) parfun_error("tangent",d);
  if(f_p!=nullptr) //use implicit function
  {
   if(dim==1 && dimg==1) //x->y(x)
     {
      Vector<real_t> d1(2);
      d1[1]=f_p(P, const_cast<Parameters&>(params),_d1)[0];
      d1[0]=1.;
      return d1/norm2(d1);
     }
   if(dim==2 && dimg==2)  return Vector<real_t>(0); // Plane surface (no tangents , return void vector)
   Vector<real_t> t=f_p(P, const_cast<Parameters&>(params),_d1);
   t/=norm2(t);
   if(dimg==2 && dim==3) // 3d surface bitangent t x n
     {
      Vector<real_t> t2=crossProduct(t,normal(P,d));
      t.insert(t.end(),t2.begin(),t2.end());
     }
  return t;
  }
  if(geom_p!=nullptr && geom_p->connectedParts().size()>0)  //piecewise geometry
  {
     Point tt(P);
     Geometry* g=locateGeometry(tt);
     return g->parametrization().tangents(tt,d);
  }
  error("free_error","unable to find a way to compute tangents");
  return Vector<real_t>(); // dummy return
}

Vector<real_t> Parametrization::tangent(const Point& P, DiffOpType d) const   //!< shortcut to the first tangent vector
{
     Vector<real_t> ts=tangents(P,d);
     if(dim==3 && dimg==2) ts.resize(3);
     return ts;
}

//!< second tangent vector (only for 3D surface )
Vector<real_t> Parametrization::bitangent(const Point& P, DiffOpType d) const
{
  if(dim!=3 || dimg!=2) error("free_error","bitangent vector only available for 3D surface");
  Vector<real_t> ts=tangents(P,d);
  return Vector<real_t>(ts.begin()+3,ts.end());
}

/*! curvilinear abcissa
   curve:  s(t)=int_0^t length(r)dr
   surface: s1(t)=int_0^t length1(r)dr, s2(t)=int_0^t length2(r)dr
*/
Vector<real_t> Parametrization::curabcs(const Point& tp, DiffOpType d) const
{
  if(geomSupport_p->shape()!=_segment) error("free_error"," not a curve in Parametrization::curabc");
  if(d==_d1) return Vector<real_t>(length(tp,_id));
  if(d==_d11)return Vector<real_t>(length(tp,_d1));
  if(d!=_id) parfun_error("normal",d);
  if(curabc_p!=nullptr) return curabc_p(tp,const_cast<Parameters&>(params),d);
  // use trapeze quadrature
  std::vector<const Point*> bounds=geomSupport_p->boundNodes();
  Vector<real_t> p(dimg);
  Vector<real_t> a=*bounds[0], b=*bounds[1];
  Vector<real_t> dp=(b-a)/real_t(np);
  Vector<Vector<real_t> >::iterator itv;
  if(curabcs_.size()!=np+1)  // reset curabcs
    {
      curabcs_.resize(np+1,Vector<real_t>());
      itv=curabcs_.begin();
      *itv=Vector<real_t>(dimg,0.); ++itv;
      p=a;
      Vector<real_t> lp=lengths(p,_id), l(dimg,0.), s(dimg,0.);
      p+=dp;
      number_t k=1;
      for(; k<=np && itv!=curabcs_.end(); p+=dp, ++itv, k++)
        {
          l=lengths(p,_id);
          for(number_t i=0;i<dimg;i++) s[i]+=0.5*dp[i]*(l[i]+lp[i]);
          *itv=s;
          lp=l;
        }
    }
  Vector<real_t> cs(dimg);
  for(number_t i=0;i<dimg;i++)
  {
      real_t t=tp[i];
      number_t n=std::floor((t-a[i])/dp[i]);
      if(n>np-1) n=np-1;
      real_t r=(t-n*dp[i])/dp[i];
      cs[i]=(1-r)*curabcs_[n][i]+r*curabcs_[n+1][i];
  }
  return cs;
}

/*! first fundamental form |E  F|  E = d1f.d1f   G=d2f.d2f  second fundamental form |L  M|  L = -d1f.d1n = d11f.n     N = -d2f.d2n = d22f.n
                           |F  G|  F = d1f.d2f                                      |M  N|  M = -(d1f.d2n+d2f.d1n) = d12f.n
                           1   |LG-MF  MG-NF|    k1,k2 eigenvalues of W -> main curvatures
   Weingarten matrix W = ----- |            |    Gauss curvature K = k1*k2 = (LN-MM)/((EG-FF)
                         EG-FF |ME-LF  NE-MF|    mean curvature  H =(k1+k2)/2 = (LG+EN-2MF)/2(EG-FF)
*/
Matrix<real_t> Parametrization::weingarten(const Point& t) const //!< Weingarten matrix (only for 3D surface)
{
    if(dim!=3 || dimg!=2) error("free_error"," Weingarten matrix available only for 3D surface");
    Vector<real_t> d1 =(*this)(t,_d1), d2 =(*this)(t,_d2);
    Vector<real_t> n=crossProduct(d1,d2);
    n/=norm(n);
    Vector<real_t> d11=(*this)(t,_d11),d12=(*this)(t,_d12), d22=(*this)(t,_d22);
    real_t E=dot(d1,d1), F=dot(d1,d2), G=dot(d2,d2), L=dot(d11,n), M=dot(d12,n), N=dot(d22,n), D=E*G-F*F;
    Matrix<real_t> W(2,2);
    W(1,1)=(L*G-M*F)/D; W(1,2)=(M*G-N*F)/D; W(2,1)=(M*E-L*F)/D; W(2,2)=(N*E-M*F)/D;
    return W;
}

// normal curvature in direction d (tangent of a curve on surface) :
//   kn = (L*up*up+2*M*up*vp+N*vp*vp)/(E*up*up+2*F*up*vp+G*vp*vp)
//   with up=(t|dvo)/(du|dvo), vp=(t|duo)/(du|dvo)  duo=du x (du x dv) , dvo=dv x (du x dv)
real_t Parametrization::normalCurvature(const Point& uv, const Vector<real_t>& d) const
{
   if(dim!=3 || dimg!=2) error("free_error"," normalCurvature available only for 3D surface");
   Vector<real_t> du =(*this)(uv,_d1), dv =(*this)(uv,_d2),
                  duu=(*this)(uv,_d11),duv=(*this)(uv,_d12), dvv=(*this)(uv,_d22);
   Vector<real_t> n=crossProduct(du,dv); n/=norm(n);
   real_t E=dot(du,du), F=dot(du,dv), G=dot(dv,dv), L=dot(duu,n), M=dot(duv,n), N=dot(dvv,n);
   Vector<real_t> duo= crossProduct(du,n), dvo= crossProduct(dv,n);
   real_t dudvo = dot(du,dvo), up = dot(d,dvo)/dudvo, vp=dot(d,duo)/dudvo, up2=up*up, vp2=vp*vp, upvp=up*vp;
   return std::abs((L*up2+2*M*upvp+N*vp2)/(E*up2+2*F*up*vp+G*vp2));
 }

// compute at the same time Gauss curvature cs[0], mean curvature cs[1] and normal curvature in direction d cs[2]
Vector<real_t> Parametrization::curvatures(const Point& uv, const Vector<real_t>& d) const
{
   if(dim!=3 || dimg!=2) error("free_error"," normalCurvature available only for 3D surface");
   Vector<real_t> cs(3,0.);
   Vector<real_t> du =(*this)(uv,_d1), dv =(*this)(uv,_d2),
                  duu=(*this)(uv,_d11),duv=(*this)(uv,_d12), dvv=(*this)(uv,_d22); //surface derivatives at uv
   Vector<real_t> n=crossProduct(du,dv); n/=norm(n);
   real_t E=dot(du,du), F=dot(du,dv), G=dot(dv,dv), L=dot(duu,n), M=dot(duv,n), N=dot(dvv,n), D=E*G-F*F;
   real_t W11=(L*G-M*F)/D, W12=(M*G-N*F)/D, W21=(M*E-L*F)/D, W22=(N*E-M*F)/D;  //weingarten matrix
   cs[0]=W11*W22-W21*W12;   //Gauss curvature
   cs[1]=0.5*(W11+W22);     //mean curvature
   Vector<real_t> duo= crossProduct(du,n), dvo= crossProduct(dv,n);
   real_t dudvo = dot(du,dvo), up = dot(d,dvo)/dudvo, vp=dot(d,duo)/dudvo, up2=up*up, vp2=vp*vp, upvp=up*vp;
   cs[2]=std::abs((L*up2+2*M*upvp+N*vp2)/(E*up2+2*F*up*vp+G*vp2)); // normal curvature
   return cs;
 }


//! Jacobian matrix n x m: Jij = djP_i, i=1,n; j=1,m
Matrix<real_t> Parametrization::jacobian(const Point& t, DiffOpType d) const
{
    if(d!=_id) parfun_error("jacobian",d);
    Matrix<real_t> J(dim,dimg);
    Vector<real_t> dj =(*this)(t,_d1);
    typename Vector<real_t>::iterator it=dj.begin();
    for(number_t i=0;i<dim; i++, ++it) *(J.begin()+i*dimg) = *it;
    if(dimg==1) return J;
    dj =(*this)(t,_d2);
    it=dj.begin();
    for(number_t i=0;i<dim; i++, ++it) *(J.begin()+i*dimg+1) = *it;
    if(dimg==2) return J;
    dj =(*this)(t,_d3);
    it=dj.begin();
    for(number_t i=0;i<dim; i++, ++it) *(J.begin()+i*dimg+2) = *it;
    if(dimg==3) return J;
    parfun_error("jacobian",d);
    return J;
}
//! metric tensor given by G=Jt*J stored as a vector (G11,G21,G22,[G31,G32,G33]), size = dimg*(dimg+1)/2
Vector<real_t> Parametrization::metricTensor(const Point& t, DiffOpType d) const
{
    if(d!=_id) parfun_error("metricTensor",d);
    Matrix<real_t> J=jacobian(t,d);
    Vector<real_t> G((dimg*(dimg+1))/2,0.);
    typename Vector<real_t>::iterator it=G.begin();
    for(number_t i=1;i<=dimg;i++)
      for(number_t j=1;j<=i;j++,it++)
        for(number_t k=1;k<=dim;k++) *it+=J(k,i)*J(k,j);
    return G;
}

/*! the Christophel symbols are given by Cijk = 1/2*inv(g)ku*(giu_j + gju_i - gij_u) + 1/2*inv(g)kv*(giv_j + gjv_i - gij_v)
    where g =	|guu, gvu|	is the metric tensor (guv=gvu)
	    	    |guv, gvv|
    C is the vector:[Guuu Guvu Gvuu Gvvu Guuv Guvv Gvuv Gvvv]
*/
Vector<real_t> Parametrization::christoffel(const Point& t, DiffOpType dop)  const
{
  if(dim!=3 || dimg!=2) error("free_error"," Christoffel symbols available only for 3D surface");
  if(dop!=_id) parfun_error("christoffel",dop);
  if(christoffel_p!=nullptr) return christoffel_p(t,const_cast<Parameters&>(params),dop);
  Point du  = (*this)(t,_d1),  dv  = (*this)(t,_d2),
        duu = (*this)(t,_d11), duv = (*this)(t,_d12), dvv = (*this)(t,_d22);
  real_t guu = dot(du,du), guv = dot(du,dv), gvv = dot(dv,dv), det = guu*gvv - guv*guv;
  real_t duudv = dot(duu,dv), dudvu = dot(du,duv), dvdvu = dot(dv,duv), dudvv = dot(du,dvv);
  real_t guu_u = 2*dot(duu,du), guv_u = duudv + dudvu, gvv_u = 2*dvdvu,
         guu_v = 2*dudvu, guv_v = dvdvu + dudvv, gvv_v = 2*dot(dvv,dv);
  real_t d=1/(2*det), a1= 2*guv_u - guu_v, a2= 2*guv_v - gvv_u;
  Vector<real_t> C(8);
  C[0] = ( gvv*guu_u - guv*a1 )*d;     // Guuu
  C[1] = ( gvv*guu_v - guv*gvv_u )*d;  // Guvu
  C[2] = C[1];
  //C[2] = ( gvv*guu_v - guv*gvv_u )*d;  // Gvuu
  C[3] = ( gvv*a2 - guv*gvv_v )*d;     // Gvvu
  C[4] = ( guu*a1 - guv*guu_u )*d;     // Guuv
  C[5] = ( guu*gvv_u - guv*guu_v )*d;  // Guvv
  C[6] = C[5];
  //C[6] = ( guu*gvv_u - guv*guu_v )*d;  // Gvuv
  C[7] = ( guu*gvv_v - guv*a2 )*d;     // Gvvv
  return C;
}

void Parametrization::print(std::ostream& os) const
{
  if(theVerboseLevel==0) return;
  if(isPiecewise()) os<<"Piecewise parametrization ("<<words("ContinuityOrder",contOrder)<<")";
  else os<<"Parametrization ";
  if(name.size()>0) os<<"'"<<name<<"'";
  if(geom_p==nullptr && f_p==nullptr)
    {
      os<<" is not defined"<<eol;
      return;
    }
  string_t type="";
  if(f_p==nullptr) type="composite ";
  switch(geomSupport_p->dim())
    {
      case 1: type+="curve"; break;
      case 2: type+="surface"; break;
      case 3: type+="volume";  break;
      default: type ="?";
    }
  os<<" of a "<<type;
  if(geom_p!=nullptr) os<<" (geometry "<<geom_p->domName()<<")";
  switch (geomSupport_p->shape())
  {
  case _segment:
     { RealPair xs=bounds(_x);
       real_t t0=xs.first, tf=xs.second;
       os<<", def. domain: segment ["<<t0<<","<<tf<<"]";
       if(!isPiecewise() || contOrder>_notRegular)
         os<<", par("<<t0<<")="<<roundToZero((*this)(Point(t0)))<<", par("<<tf<<")="<<roundToZero((*this)(Point(tf)));
     }
     break;
  case _rectangle:
  case _square:
     { RealPair xs=bounds(_x), ys=xs=bounds(_y);
       real_t u0=xs.first, uf=xs.second, v0=ys.first, vf=ys.second ;
       os<<", def. domain: rectangle ["<<u0<<","<<uf<<"]"<<" X ["<<v0<<","<<vf<<"]";
       if(!isPiecewise() || contOrder>_notRegular)
         os<<", par("<<u0<<","<<v0<<")="<<roundToZero((*this)(Point(u0,v0)))<<", par("<<uf<<","<<v0<<")="<<roundToZero((*this)(Point(uf,v0)))
           <<", par("<<u0<<","<<vf<<")="<<roundToZero((*this)(Point(u0,vf)))<<", par("<<uf<<","<<vf<<")="<<roundToZero((*this)(Point(uf,vf)));
     }
     break;
  case _cube:
  case _cuboid:
     { RealPair xs=bounds(_x), ys=xs=bounds(_y), zs=xs=bounds(_z);
       os<<", def. domain: cuboid ["<<xs.first<<","<<xs.second<<"]"<<" X ["<<ys.first<<","<<ys.second<<"]"<<" X ["<<zs.first<<","<<zs.second<<"]";
     }
     break;
  default: break;
  }
  os<<", space dim: "<<dim;
  if(periods[0]!=0) os<<", u-period = "<<periods[0];
  if(dimg>1 && periods[1]!=0) os<<", v-period = "<<periods[1];
  if(dimg>2 && periods[2]!=0) os<<", w-period = "<<periods[2];
  if(meshSupport_p!=nullptr) os<<" (geometry support is meshed)";
  os<<", parametrization";
  if(params.contains("x1"))
   {
       os<<": ("<<(*reinterpret_cast<const SymbolicFunction*>(params("x1").get_p()));
       if(params.contains("x2")) os<< ","<<(*reinterpret_cast<const SymbolicFunction*>(params("x2").get_p()));
       if(params.contains("x3")) os<< ","<<(*reinterpret_cast<const SymbolicFunction*>(params("x3").get_p()));
       os<<")";
   }
  number_t n=0;
  if(invParametrization_p!=nullptr)
    {
      os<< ", inverse parametrization";
      if(params.contains("invParametrization1"))
      {
          os<<": ("<<(*reinterpret_cast<const SymbolicFunction*>(params("invParametrization1").get_p()));
          if(params.contains("invParametrization2")) os<< ","<<(*reinterpret_cast<const SymbolicFunction*>(params("invParametrization2").get_p()));
          os<<")";
      }
      n++;
    }
  if(length_p!=nullptr)
    {
      os<< ", local length";
      if(params.contains("length"))
          os<<": "<<(*reinterpret_cast<const SymbolicFunction*>(params("length").get_p()));
      n++;
    }
  if(normal_p!=nullptr)
    {
      os<< ", normal";
      if(params.contains("n1"))
      {
          os<<": ("<<(*reinterpret_cast<const SymbolicFunction*>(params("n1").get_p()));
          if(params.contains("n2")) os<< ","<<(*reinterpret_cast<const SymbolicFunction*>(params("n2").get_p()));
          if(params.contains("n3")) os<< ","<<(*reinterpret_cast<const SymbolicFunction*>(params("n3").get_p()));
          os<<")";
      }
      n++;
    }
  if(curvature_p!=nullptr)
    {
      os<< ", curvature";
      if(params.contains("curvature"))
          os<<": "<<(*reinterpret_cast<const SymbolicFunction*>(params("curvature").get_p()));
      n++;
    }
  os<<" function";
  if(n>0) os<<"s";
  os<<" defined";
}
//========================================================================================
// inherited arc parametrizations of a surface parametrization (passed through parameters)
//========================================================================================
Vector<real_t> surfToArcParametrizationu0(const Point& p, Parameters& pars, DiffOpType d)
{
    DiffOpType der=d;
    if(der==_dx || der==_grad) der=_dy;
    if(der==_dxx) der=_dyy;
    const Parametrization* surfParametrization = reinterpret_cast<const Parametrization*>(pars("surface_parametrization").get_p());
    return (*surfParametrization)(Point(0.,p[0]),der);
}
Vector<real_t> surfToArcParametrizationu1(const Point& p, Parameters& pars, DiffOpType d)
{
    DiffOpType der=d;
    if(der==_dx || der==_grad) der=_dy;
    if(der==_dxx) der=_dyy;
    const Parametrization* surfParametrization = reinterpret_cast<const Parametrization*>(pars("surface_parametrization").get_p());
    return (*surfParametrization)(Point(1.,p[0]),der);
}
Vector<real_t> surfToArcParametrizationv0(const Point& p, Parameters& pars, DiffOpType d)
{
    const Parametrization* surfParametrization = reinterpret_cast<const Parametrization*>(pars("surface_parametrization").get_p());
    return (*surfParametrization)(Point(p[0],0.),d);
}
Vector<real_t> surfToArcParametrizationv1(const Point& p, Parameters& pars, DiffOpType d)
{
    const Parametrization* surfParametrization = reinterpret_cast<const Parametrization*>(pars("surface_parametrization").get_p());
    return (*surfParametrization)(Point(p[0],1.),d);
}

//========================================================================================
// stuff related to symbolic representation
//========================================================================================

//! parametrization from geometry and one symbolic function (1D -> 1D, only segment mapping)
Parametrization::Parametrization(const Geometry& geo, const SymbolicFunction& f, const Parameters& pars, const string_t& na)
: geom_p(nullptr), geomSupport_p(nullptr), meshSupport_p(nullptr), f_p(nullptr), curvature_p(nullptr), length_p(nullptr),
  invParametrization_p(nullptr), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr), name(na), dim(0), np(1000), s0(0), s1(0), contOrder(_regCinf)
{
    dim=geo.dim(); dimg=dim;
    periods.resize(dimg,0);
    if(dim!=1) error("free_error"," bad construction of Parametrization, support dimension not consistent with functions given");
    geomSupport_p=new Geometry(geo);
    params<<Parameter(dim,"dim")<<Parameter(dim,"dimgeo");
    buildSymbolic(f);
    params.push(pars);
}

//! 1d parametrization from segment [a,b] and one symbolic function (1D -> 1D, only segment mapping)
Parametrization::Parametrization(real_t a, real_t b, const SymbolicFunction& f, const Parameters& pars, const string_t& na)
: geom_p(nullptr), geomSupport_p(nullptr), meshSupport_p(nullptr), f_p(nullptr), curvature_p(nullptr), length_p(nullptr),
  invParametrization_p(nullptr), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr), name(na), dim(0), np(1000), s0(0), s1(0), contOrder(_regCinf)
{
   geomSupport_p=new Segment(_v1=Point(a),_v2=Point(b),_domain_name="["+tostring(a)+", "+tostring(b)+"]",_side_names=Strings("s1","s2"));
   dim=1;dimg=1;
   periods.resize(dimg,0);
   params<<Parameter(dim,"dim")<<Parameter(dimg,"dimgeo");
   buildSymbolic(f);
   params.push(pars);
}

//! parametrization from Geometry and two symbolic functions (1D/2D -> 2D, 2D curve or 2D mapping)
Parametrization::Parametrization(const Geometry& geo, const SymbolicFunction& f1, const SymbolicFunction& f2, const Parameters& pars, const string_t& na)
: geom_p(nullptr), geomSupport_p(nullptr), meshSupport_p(nullptr), f_p(nullptr), curvature_p(nullptr), length_p(nullptr),
  invParametrization_p(nullptr), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr), name(na), dim(0), np(1000), s0(0), s1(0), contOrder(_regCinf)
{
   geomSupport_p=new Geometry(geo);
   dim=2; dimg=geomSupport_p->dim();
   periods.resize(dimg,0);
   params<<Parameter(dim,"dim")<<Parameter(dimg,"dimgeo");
   buildSymbolic(f1,f2);
   params.push(pars);
}

//! 1d parametrization from segment [a,b] and two symbolic functions (1D -> 2D, 2D curve)
Parametrization::Parametrization(real_t a, real_t b, const SymbolicFunction& f1, const SymbolicFunction& f2, const Parameters& pars, const string_t& na)
: geom_p(nullptr), geomSupport_p(nullptr), meshSupport_p(nullptr), f_p(nullptr), curvature_p(nullptr), length_p(nullptr),
  invParametrization_p(nullptr), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr), name(na), dim(0), np(1000), s0(0), s1(0), contOrder(_regCinf)
{
   geomSupport_p=new Segment(_v1=Point(a),_v2=Point(b),_domain_name="["+tostring(a)+", "+tostring(b)+"]",_side_names=Strings("s1","s2"));
   dim=2;dimg=geomSupport_p->dim();
   periods.resize(dimg,0);
   params<<Parameter(dim,"dim")<<Parameter(dimg,"dimgeo");
   buildSymbolic(f1,f2);
   params.push(pars);
}

//! 2d parametrization from rectangle [a,b]x[c,d] (2D mapping)
Parametrization::Parametrization(real_t a, real_t b, real_t c, real_t d, const SymbolicFunction& f1, const SymbolicFunction& f2, const Parameters& pars, const string_t& na)
: geom_p(nullptr), geomSupport_p(nullptr), meshSupport_p(nullptr), f_p(nullptr), curvature_p(nullptr), length_p(nullptr),
  invParametrization_p(nullptr), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr), name(na), dim(0), np(1000), s0(0), s1(0), contOrder(_regCinf)
{
   geomSupport_p=new Rectangle(_v1=Point(a,c),_v2=Point(b,c),_v4=Point(a,d),_domain_name="["+tostring(a)+", "+tostring(b)+"]x["+tostring(c)+", "+tostring(d)+"]"
                               ,_side_names=Strings("s1","s2","s3","s4"));
   dim=2;dimg=geomSupport_p->dim();
   periods.resize(dimg,0);
   params<<Parameter(dim,"dim")<<Parameter(dimg,"dimgeo");
   buildSymbolic(f1,f2);
   params.push(pars);
}

//! parametrization from Geometry to 3D (3D curve, 3D surface or 3D mapping)
Parametrization::Parametrization(const Geometry& geo, const SymbolicFunction& f1, const SymbolicFunction& f2, const SymbolicFunction& f3,
                const Parameters& pars, const string_t& na)
: geom_p(nullptr), geomSupport_p(nullptr), meshSupport_p(nullptr), f_p(nullptr),  curvature_p(nullptr), length_p(nullptr),
  invParametrization_p(nullptr), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr), name(na), dim(0), np(1000), s0(0), s1(0), contOrder(_regCinf)
{
   geomSupport_p=new Geometry(geo);
   dim=3;dimg=geomSupport_p->dim();
   periods.resize(dimg,0);
   params<<Parameter(dim,"dim")<<Parameter(dimg,"dimgeo");
   buildSymbolic(f1,f2,f3);
   params.push(pars);
}

//! surface parametrization from rectangle [a,b]x[c,d] (3D surface)
Parametrization::Parametrization(real_t a, real_t b, real_t c, real_t d, const SymbolicFunction& f1, const SymbolicFunction& f2, const SymbolicFunction& f3,
                const Parameters& pars, const string_t& na)
: geom_p(nullptr), geomSupport_p(nullptr), meshSupport_p(nullptr), f_p(nullptr),  curvature_p(nullptr), length_p(nullptr),
  invParametrization_p(nullptr), normal_p(nullptr), tangent_p(nullptr), curabc_p(nullptr), christoffel_p(nullptr), name(na), dim(0), np(1000), s0(0), s1(0), contOrder(_regCinf)
{
   geomSupport_p=new Rectangle(_v1=Point(a,c),_v2=Point(b,c),_v4=Point(a,d),_domain_name="["+tostring(a)+", "+tostring(b)+"]x["+tostring(c)+", "+tostring(d)+"]"
                               ,_side_names=Strings("s1","s2","s3","s4"));
   dim=3;dimg=geomSupport_p->dim();
   params<<Parameter(dim,"dim")<<Parameter(dimg,"dimgeo");
   periods.resize(dimg,0);
   buildSymbolic(f1,f2,f3);
   params.push(pars);
}

void Parametrization::buildSymbolic(const SymbolicFunction& f)
{
     f_p = symbolic_f;
     SymbolicFunction* fp=new SymbolicFunction(f);
     params<<Parameter(reinterpret_cast<void*>(fp),"@x1");
     SymbolicFunction* dfp=new SymbolicFunction(derivative(f,_x1));
     SymbolicFunction* d2fp=new SymbolicFunction(derivative(*dfp,_x1));
     params<<Parameter(reinterpret_cast<void*>(dfp),"@dx1") <<Parameter(reinterpret_cast<void*>(d2fp),"@d11x1");
}

void Parametrization::buildSymbolic(const SymbolicFunction& f1,const SymbolicFunction& f2)
{
    f_p = symbolic_f;
    SymbolicFunction* f1p=new SymbolicFunction(f1), *f2p=new SymbolicFunction(f2);
    params<<Parameter(reinterpret_cast<void*>(f1p),"@x1")<<Parameter(reinterpret_cast<void*>(f2p),"@x2");
    number_t d=geomSupport_p->dim();

    if(d==1)  // 2D curve
    {
        SymbolicFunction* df1p=new SymbolicFunction(derivative(f1,_x1));
        SymbolicFunction* df2p=new SymbolicFunction(derivative(f2,_x1));
        SymbolicFunction* d2f1p=new SymbolicFunction(derivative(*df1p,_x1));
        SymbolicFunction* d2f2p=new SymbolicFunction(derivative(*df2p,_x1));
        params<<Parameter(reinterpret_cast<void*>(df1p),"@d1x1")<<Parameter(reinterpret_cast<void*>(df2p),"@d1x2")
              <<Parameter(reinterpret_cast<void*>(d2f1p),"@d11x1")<<Parameter(reinterpret_cast<void*>(d2f2p),"@d11x2");
        return;
    }
    if(d==2)  // 2D map
    {
        SymbolicFunction* d1f1p=new SymbolicFunction(derivative(f1,_x1)), *d2f1p=new SymbolicFunction(derivative(f1,_x2));
        SymbolicFunction* d1f2p=new SymbolicFunction(derivative(f2,_x1)), *d2f2p=new SymbolicFunction(derivative(f2,_x2));
        params<<Parameter(reinterpret_cast<void*>(d1f1p),"@d1x1")<<Parameter(reinterpret_cast<void*>(d2f1p),"@d2x1")
              <<Parameter(reinterpret_cast<void*>(d1f2p),"@d1x2")<<Parameter(reinterpret_cast<void*>(d2f2p),"@d2x2");
        SymbolicFunction* d11f1p=new SymbolicFunction(derivative(*d1f1p,_x1)), *d11f2p=new SymbolicFunction(derivative(*d1f2p,_x1));
        SymbolicFunction* d21f1p=new SymbolicFunction(derivative(*d1f1p,_x2)), *d21f2p=new SymbolicFunction(derivative(*d1f2p,_x2));
        SymbolicFunction* d22f1p=new SymbolicFunction(derivative(*d2f1p,_x2)), *d22f2p=new SymbolicFunction(derivative(*d2f2p,_x2));
        params<<Parameter(reinterpret_cast<void*>(d11f1p),"@d11x1")<<Parameter(reinterpret_cast<void*>(d11f2p),"@d11x2")
              <<Parameter(reinterpret_cast<void*>(d21f1p),"@d21x1")<<Parameter(reinterpret_cast<void*>(d21f2p),"@d21x2")
              <<Parameter(reinterpret_cast<void*>(d22f1p),"@d22x1")<<Parameter(reinterpret_cast<void*>(d22f2p),"@d22x2");
        return;
    }
    error("free_error"," bad construction of Parametrization, support dimension not consistent with functions given");
}

void Parametrization::buildSymbolic(const SymbolicFunction& f1,const SymbolicFunction& f2, const SymbolicFunction& f3)
{
    f_p = symbolic_f;
    SymbolicFunction* f1p=new SymbolicFunction(f1), *f2p=new SymbolicFunction(f2), *f3p=new SymbolicFunction(f3);
    params<<Parameter(reinterpret_cast<void*>(f1p),"@x1")<<Parameter(reinterpret_cast<void*>(f2p),"@x2")<<Parameter(reinterpret_cast<void*>(f3p),"@x3");
    number_t d=geomSupport_p->dim();
    if(d==1)  // 3D curve
    {
        SymbolicFunction *df1p=new SymbolicFunction(derivative(f1,_x1)),
                         *df2p=new SymbolicFunction(derivative(f2,_x1)),
                         *df3p=new SymbolicFunction(derivative(f3,_x1));
        params<<Parameter(reinterpret_cast<void*>(df1p),"@d1x1")
              <<Parameter(reinterpret_cast<void*>(df2p),"@d1x2")
              <<Parameter(reinterpret_cast<void*>(df3p),"@d1x3");
        SymbolicFunction *d2f1p=new SymbolicFunction(derivative(*df1p,_x1)),
                         *d2f2p=new SymbolicFunction(derivative(*df2p,_x1)),
                         *d2f3p=new SymbolicFunction(derivative(*df3p,_x1));
        params<<Parameter(reinterpret_cast<void*>(d2f1p),"@d11x1")<<Parameter(reinterpret_cast<void*>(d2f2p),"@d11x2")<<Parameter(reinterpret_cast<void*>(d2f3p),"@d11x3");
        return;
    }
    if(d==2)  // 3D surface
    {
        SymbolicFunction *d1f1p=new SymbolicFunction(derivative(f1,_x1)), *d2f1p=new SymbolicFunction(derivative(f1,_x2)),
                         *d1f2p=new SymbolicFunction(derivative(f2,_x1)), *d2f2p=new SymbolicFunction(derivative(f2,_x2)),
                         *d1f3p=new SymbolicFunction(derivative(f3,_x1)), *d2f3p=new SymbolicFunction(derivative(f3,_x2));
        params<<Parameter(reinterpret_cast<void*>(d1f1p),"@d1x1")<<Parameter(reinterpret_cast<void*>(d2f1p),"@d2x1")
              <<Parameter(reinterpret_cast<void*>(d1f2p),"@d1x2")<<Parameter(reinterpret_cast<void*>(d2f2p),"@d2x2")
              <<Parameter(reinterpret_cast<void*>(d1f3p),"@d1x3")<<Parameter(reinterpret_cast<void*>(d2f3p),"@d2x3");
        SymbolicFunction *d11f1p=new SymbolicFunction(derivative(*d1f1p,_x1)),
                         *d11f2p=new SymbolicFunction(derivative(*d1f2p,_x1)),
                         *d11f3p=new SymbolicFunction(derivative(*d1f3p,_x1)),
                         *d21f1p=new SymbolicFunction(derivative(*d1f1p,_x2)),
                         *d21f2p=new SymbolicFunction(derivative(*d1f2p,_x2)),
                         *d21f3p=new SymbolicFunction(derivative(*d1f3p,_x2)),
                         *d22f1p=new SymbolicFunction(derivative(*d2f1p,_x2)),
                         *d22f2p=new SymbolicFunction(derivative(*d2f2p,_x2)),
                         *d22f3p=new SymbolicFunction(derivative(*d2f3p,_x2));
        params<<Parameter(reinterpret_cast<void*>(d11f1p),"@d11x1")<<Parameter(reinterpret_cast<void*>(d11f2p),"@d11x2")<<Parameter(reinterpret_cast<void*>(d11f3p),"@d11x3")
              <<Parameter(reinterpret_cast<void*>(d21f1p),"@d21x1")<<Parameter(reinterpret_cast<void*>(d21f2p),"@d21x2")<<Parameter(reinterpret_cast<void*>(d21f3p),"@d21x3")
              <<Parameter(reinterpret_cast<void*>(d22f1p),"@d22x1")<<Parameter(reinterpret_cast<void*>(d22f2p),"@d22x2")<<Parameter(reinterpret_cast<void*>(d22f3p),"@d22x3");
        return;
    }
    if(d==3)  // 3D map
    {
        SymbolicFunction *d1f1p=new SymbolicFunction(derivative(f1,_x1)), *d2f1p=new SymbolicFunction(derivative(f1,_x2)), *d3f1p=new SymbolicFunction(derivative(f1,_x3)),
                         *d1f2p=new SymbolicFunction(derivative(f2,_x1)), *d2f2p=new SymbolicFunction(derivative(f2,_x2)), *d3f2p=new SymbolicFunction(derivative(f2,_x3)),
                         *d1f3p=new SymbolicFunction(derivative(f3,_x1)), *d2f3p=new SymbolicFunction(derivative(f3,_x2)), *d3f3p=new SymbolicFunction(derivative(f3,_x3));
        params<<Parameter(reinterpret_cast<void*>(d1f1p),"@d1x1")<<Parameter(reinterpret_cast<void*>(d2f1p),"@d2x1")<<Parameter(reinterpret_cast<void*>(d3f1p),"@d3x1")
              <<Parameter(reinterpret_cast<void*>(d1f2p),"@d1x2")<<Parameter(reinterpret_cast<void*>(d2f2p),"@d2x2")<<Parameter(reinterpret_cast<void*>(d3f2p),"@d3x2")
              <<Parameter(reinterpret_cast<void*>(d1f3p),"@d1x3")<<Parameter(reinterpret_cast<void*>(d2f3p),"@d2x3")<<Parameter(reinterpret_cast<void*>(d3f3p),"@d3x3");
        SymbolicFunction *d11f1p=new SymbolicFunction(derivative(*d1f1p,_x1)),
                         *d11f2p=new SymbolicFunction(derivative(*d1f2p,_x1)),
                         *d11f3p=new SymbolicFunction(derivative(*d1f3p,_x1)),
                         *d21f1p=new SymbolicFunction(derivative(*d1f1p,_x2)),
                         *d21f2p=new SymbolicFunction(derivative(*d1f2p,_x2)),
                         *d21f3p=new SymbolicFunction(derivative(*d1f3p,_x2)),
                         *d31f1p=new SymbolicFunction(derivative(*d1f1p,_x3)),
                         *d31f2p=new SymbolicFunction(derivative(*d1f2p,_x3)),
                         *d31f3p=new SymbolicFunction(derivative(*d1f3p,_x3)),
                         *d22f1p=new SymbolicFunction(derivative(*d2f1p,_x2)),
                         *d22f2p=new SymbolicFunction(derivative(*d2f2p,_x2)),
                         *d22f3p=new SymbolicFunction(derivative(*d2f3p,_x2)),
                         *d32f1p=new SymbolicFunction(derivative(*d2f1p,_x3)),
                         *d32f2p=new SymbolicFunction(derivative(*d2f2p,_x3)),
                         *d32f3p=new SymbolicFunction(derivative(*d2f3p,_x3));
        params<<Parameter(reinterpret_cast<void*>(d11f1p),"@d11x1")<<Parameter(reinterpret_cast<void*>(d11f2p),"@d11x2")<<Parameter(reinterpret_cast<void*>(d11f3p),"@d11x3")
              <<Parameter(reinterpret_cast<void*>(d21f1p),"@d21x1")<<Parameter(reinterpret_cast<void*>(d21f2p),"@d21x2")<<Parameter(reinterpret_cast<void*>(d21f3p),"@d21x3")
              <<Parameter(reinterpret_cast<void*>(d31f1p),"@d31x1")<<Parameter(reinterpret_cast<void*>(d31f2p),"@d31x2")<<Parameter(reinterpret_cast<void*>(d31f3p),"@d31x3")
              <<Parameter(reinterpret_cast<void*>(d22f1p),"@d22x1")<<Parameter(reinterpret_cast<void*>(d22f2p),"@d22x2")<<Parameter(reinterpret_cast<void*>(d22f3p),"@d22x3")
              <<Parameter(reinterpret_cast<void*>(d32f1p),"@d32x1")<<Parameter(reinterpret_cast<void*>(d32f2p),"@d32x2")<<Parameter(reinterpret_cast<void*>(d32f3p),"@d32x3");
        // length, curvature, normal , tangent are not defined
    }
    error("free_error"," bad construction of Parametrization, support dimension not consistent with functions given");
}

//! return bounds of geometry in v direction (_x1,_x2,_x3)
RealPair Parametrization::bounds(VariableName v) const
{
    if(geomSupport_p==nullptr) return RealPair(0.,1.);  // dummy return
    number_t i = number_t(v);
    if (i==0 || i>geomSupport_p->dim()) error("free_error"," index "+tostring(i)+" out in Parametrization::bounds");
    Point p1,p2;
    if (geomSupport_p->shape()==_segment)
    {
        p1=geomSupport_p->segment()->p1(); p2=geomSupport_p->segment()->p2();
        return RealPair(p1(i),p2(i));
    }
    return geomSupport_p->boundingBox.bounds(i);
}

//! true if p is located on singular side
bool Parametrization::onSingularSide(const Point& p) const
{
  if(singularSide.size()==0) return false;
  std::list<std::pair<number_t, real_t> >::const_iterator it=singularSide.begin();
  for(;it!=singularSide.end();++it)
  {
      if(std::abs(p(it->first)-it->second)>theTolerance) return false;
  }
  return true;
}

//!< true if there exists at least one periodic direction
bool Parametrization::isPeriodic() const
{
  for(number_t i=0;i<dimg;i++)
    if(periods[i]!=0) return true;
  return false;
}

// create a mesh of the geometry associated to a parametrization
//    hs: discretization step (if 0 use current hstep )
//    sh: shape to be used
void Parametrization::createMesh(real_t hs, ShapeType sh)
{
    if(geomSupport_p==nullptr) return;   // null geometry pointer
    trace_p->push("Parametrization::createMesh");
    if(meshSupport_p!=nullptr) delete meshSupport_p;
    if(hs!=0.) geomSupport_p->setHstep(hs);
    if(geomSupport_p->dim()==1) meshSupport_p=new Mesh(*geomSupport_p,1);
    else if(geomSupport_p->dim()==2)
     {
         if(sh==_noShape) meshSupport_p=new Mesh(*geomSupport_p,_triangle,1);
         else  meshSupport_p=new Mesh(*geomSupport_p,sh,1);
    }
    else error("free_error","Parametrization::createMesh deals only with 1D or 2D geometry");
    trace_p->pop();
    return;
}

//associate symbolic function to invParametrization (1D parametrization)
void Parametrization::setinvParametrization(const SymbolicFunction& f)
{
    SymbolicFunction* fp=new SymbolicFunction(f);  //to be delete by destructor
    invParametrization_p = symbolic_invParametrization;
    if(params.contains("@invParametrization1")) params("@invParametrization1") = reinterpret_cast<void*>(fp);
    else params<<Parameter(reinterpret_cast<void*>(fp),"@invParametrization1");
}

//associate symbolic function to invParametrization (2D parametrization)
void Parametrization::setinvParametrization(const SymbolicFunction& f1, const SymbolicFunction& f2)
{
    SymbolicFunction* fp1=new SymbolicFunction(f1);  //to be delete by destructor
    SymbolicFunction* fp2=new SymbolicFunction(f2);  //to be delete by destructor
    invParametrization_p = symbolic_invParametrization;
    if(params.contains("@invParametrization1")) params("@invParametrization1") = reinterpret_cast<void*>(fp1);
    else params<<Parameter(reinterpret_cast<void*>(fp1),"@invParametrization1");
    if(params.contains("@invParametrization2")) params("@invParametrization2") = reinterpret_cast<void*>(fp2);
    else params<<Parameter(reinterpret_cast<void*>(fp2),"@invParametrization2");
}

//associate symbolic function to length
void Parametrization::setLength(const SymbolicFunction& f)
{
    SymbolicFunction* fp=new SymbolicFunction(f);  //to be delete by destructor
    length_p = symbolic_length;
    if(params.contains("@length")) params("@length") = reinterpret_cast<void*>(fp);
    else params<<Parameter(reinterpret_cast<void*>(fp),"@length");
}

//associate symbolic function to curvature
void Parametrization::setCurvature(const SymbolicFunction& f)
{
    SymbolicFunction* fp=new SymbolicFunction(f);  //to be delete by destructor
    curvature_p = symbolic_curvature;
    if(params.contains("@curvature")) params("@curvature") = reinterpret_cast<void*>(fp);
    else params<<Parameter(reinterpret_cast<void*>(fp),"@curvature");
}

void Parametrization::setCurvatures(const SymbolicFunction& f1, const SymbolicFunction& f2)
{
    SymbolicFunction* fp=new SymbolicFunction(f1);  //to be delete by destructor
    curvature_p = symbolic_curvature;
    if(params.contains("@curvature")) params("@curvature") = reinterpret_cast<void*>(fp);
    else params<<Parameter(reinterpret_cast<void*>(fp),"@curvature");
    fp=new SymbolicFunction(f2);  //to be delete by destructor
    if(params.contains("@bicurvature")) params("@bicurvature") = reinterpret_cast<void*>(fp);
    else params<<Parameter(reinterpret_cast<void*>(fp),"@bicurvature");
}

//associate symbolic function to curvilinear abcissa (curve)
void Parametrization::setCurabc(const SymbolicFunction& f)
{
    SymbolicFunction* fp=new SymbolicFunction(f);  //to be delete by destructor
    curabc_p = symbolic_curabc;
    if(params.contains("@curabc")) params("@curabc") = reinterpret_cast<void*>(fp);
    else params<<Parameter(reinterpret_cast<void*>(fp),"@curabc");
}

void Parametrization::setCurabcs(const SymbolicFunction& f1, const SymbolicFunction& f2)
{
    SymbolicFunction* fp=new SymbolicFunction(f1);  //to be delete by destructor
    curabc_p = symbolic_curabc;
    if(params.contains("@curabc")) params("@curabc") = reinterpret_cast<void*>(fp);
    else params<<Parameter(reinterpret_cast<void*>(fp),"@curabc");
    fp=new SymbolicFunction(f2);  //to be delete by destructor
    if(params.contains("@bicurabc")) params("@bicurabc") = reinterpret_cast<void*>(fp);
    else params<<Parameter(reinterpret_cast<void*>(fp),"@bicurabc");
}

//associate symbolic function to invParametrization (3D parametrization)
void Parametrization::setNormal(const SymbolicFunction& f1, const SymbolicFunction& f2)
{
    SymbolicFunction* fp1=new SymbolicFunction(f1);  //to be delete by destructor
    SymbolicFunction* fp2=new SymbolicFunction(f2);  //to be delete by destructor
    normal_p = symbolic_normal;
    if(params.contains("@n1")) params("@n1") = reinterpret_cast<void*>(fp1);
    else params<<Parameter(reinterpret_cast<void*>(fp1),"@n1");
    if(params.contains("@n2")) params("@n2") = reinterpret_cast<void*>(fp2);
    else params<<Parameter(reinterpret_cast<void*>(fp2),"@n2");
}

void Parametrization::setNormal(const SymbolicFunction& f1, const SymbolicFunction& f2, const SymbolicFunction& f3)
{
    SymbolicFunction* fp1=new SymbolicFunction(f1);  //to be delete by destructor
    SymbolicFunction* fp2=new SymbolicFunction(f2);  //to be delete by destructor
    SymbolicFunction* fp3=new SymbolicFunction(f2);  //to be delete by destructor
    normal_p = symbolic_normal;
    if(params.contains("@n1")) params("@n1") = reinterpret_cast<void*>(fp1);
    else params<<Parameter(reinterpret_cast<void*>(fp1),"@n1");
    if(params.contains("@n2")) params("@n2") = reinterpret_cast<void*>(fp2);
    else params<<Parameter(reinterpret_cast<void*>(fp2),"@n2");
    if(params.contains("@n3")) params("@n3") = reinterpret_cast<void*>(fp3);
    else params<<Parameter(reinterpret_cast<void*>(fp3),"@n3");
}

void Parametrization::setNormals(const SymbolicFunction& f1, const SymbolicFunction& f2, const SymbolicFunction& f3,
                                 const SymbolicFunction& f4, const SymbolicFunction& f5, const SymbolicFunction& f6)
{
    SymbolicFunction* fp1=new SymbolicFunction(f1);  //to be delete by destructor
    SymbolicFunction* fp2=new SymbolicFunction(f2);  //to be delete by destructor
    SymbolicFunction* fp3=new SymbolicFunction(f2);  //to be delete by destructor
    normal_p = symbolic_normal;
    if(params.contains("@n1")) params("@n1") = reinterpret_cast<void*>(fp1);
    else params<<Parameter(reinterpret_cast<void*>(fp1),"@n1");
    if(params.contains("@n2")) params("@n2") = reinterpret_cast<void*>(fp2);
    else params<<Parameter(reinterpret_cast<void*>(fp2),"@n2");
    if(params.contains("@n3")) params("@n3") = reinterpret_cast<void*>(fp3);
    else params<<Parameter(reinterpret_cast<void*>(fp3),"@n3");
    fp1=new SymbolicFunction(f4);  //to be delete by destructor
    fp2=new SymbolicFunction(f5);  //to be delete by destructor
    fp3=new SymbolicFunction(f6);  //to be delete by destructor
    normal_p = symbolic_normal;
    if(params.contains("@bn1")) params("@bn1") = reinterpret_cast<void*>(fp1);
    else params<<Parameter(reinterpret_cast<void*>(fp1),"@bn1");
    if(params.contains("@bn2")) params("@bn2") = reinterpret_cast<void*>(fp2);
    else params<<Parameter(reinterpret_cast<void*>(fp2),"@bn2");
    if(params.contains("@bn3")) params("@bn3") = reinterpret_cast<void*>(fp3);
    else params<<Parameter(reinterpret_cast<void*>(fp3),"@bn3");
}

//associate symbolic function to invParametrization (3D parametrization)
void Parametrization::setTangent(const SymbolicFunction& f1, const SymbolicFunction& f2)
{
    SymbolicFunction* fp1=new SymbolicFunction(f1);  //to be delete by destructor
    SymbolicFunction* fp2=new SymbolicFunction(f2);  //to be delete by destructor
    normal_p = symbolic_normal;
    if(params.contains("@t1")) params("@t1") = reinterpret_cast<void*>(fp1);
    else params<<Parameter(reinterpret_cast<void*>(fp1),"@t1");
    if(params.contains("@t2")) params("@t2") = reinterpret_cast<void*>(fp2);
    else params<<Parameter(reinterpret_cast<void*>(fp2),"@t2");
}

void Parametrization::setTangent(const SymbolicFunction& f1, const SymbolicFunction& f2, const SymbolicFunction& f3)
{
    SymbolicFunction* fp1=new SymbolicFunction(f1);  //to be delete by destructor
    SymbolicFunction* fp2=new SymbolicFunction(f2);  //to be delete by destructor
    SymbolicFunction* fp3=new SymbolicFunction(f2);  //to be delete by destructor
    normal_p = symbolic_normal;
    if(params.contains("@t1")) params("@t1") = reinterpret_cast<void*>(fp1);
    else params<<Parameter(reinterpret_cast<void*>(fp1),"@t1");
    if(params.contains("@t2")) params("@t2") = reinterpret_cast<void*>(fp2);
    else params<<Parameter(reinterpret_cast<void*>(fp2),"@t2");
    if(params.contains("@t3")) params("@t3") = reinterpret_cast<void*>(fp3);
    else params<<Parameter(reinterpret_cast<void*>(fp3),"@t3");
}

void Parametrization::setTangents(const SymbolicFunction& f1, const SymbolicFunction& f2, const SymbolicFunction& f3,
                                  const SymbolicFunction& f4, const SymbolicFunction& f5, const SymbolicFunction& f6)
{
    SymbolicFunction* fp1=new SymbolicFunction(f1);  //to be delete by destructor
    SymbolicFunction* fp2=new SymbolicFunction(f2);  //to be delete by destructor
    SymbolicFunction* fp3=new SymbolicFunction(f2);  //to be delete by destructor
    normal_p = symbolic_normal;
    if(params.contains("@t1")) params("@t1") = reinterpret_cast<void*>(fp1);
    else params<<Parameter(reinterpret_cast<void*>(fp1),"@t1");
    if(params.contains("@t2")) params("@t2") = reinterpret_cast<void*>(fp2);
    else params<<Parameter(reinterpret_cast<void*>(fp2),"@t2");
    if(params.contains("@t3")) params("@t3") = reinterpret_cast<void*>(fp3);
    else params<<Parameter(reinterpret_cast<void*>(fp3),"@t3");
    fp1=new SymbolicFunction(f4);  //to be delete by destructor
    fp2=new SymbolicFunction(f5);  //to be delete by destructor
    fp3=new SymbolicFunction(f6);  //to be delete by destructor
    normal_p = symbolic_normal;
    if(params.contains("@bt1")) params("@bt1") = reinterpret_cast<void*>(fp1);
    else params<<Parameter(reinterpret_cast<void*>(fp1),"@bt1");
    if(params.contains("@bt2")) params("@bt2") = reinterpret_cast<void*>(fp2);
    else params<<Parameter(reinterpret_cast<void*>(fp2),"@bt2");
    if(params.contains("@bt3")) params("@bt3") = reinterpret_cast<void*>(fp3);
    else params<<Parameter(reinterpret_cast<void*>(fp3),"@bt3");
}

Vector<real_t> symbolic_f(const Point& tp, Parameters& pars, DiffOpType d)
{
    number_t dim=pars(1), dimgeo=pars(2);
    Vector<real_t> v(dim);
    if(dimgeo==1) //curve
    {
        real_t t=tp[0];
        if(d==_id)
        {
            for(number_t i=0;i<dim;i++) v[i]=(*reinterpret_cast<const SymbolicFunction*>(pars(3+i).get_p()))(t);
            return v;
        }
        if(d==_d1)
        {
            for(number_t i=0;i<dim;i++) v[i]=(*reinterpret_cast<const SymbolicFunction*>(pars("@d1x"+tostring(i+1)).get_p()))(t);
            return v;
        }
        if(d==_d11)
        {
            for(number_t i=0;i<dim;i++) v[i]=(*reinterpret_cast<const SymbolicFunction*>(pars("@d11x"+tostring(i+1)).get_p()))(t);
            return v;
        }
        error("free_error","in symbolic_f, derivative order greater than 2 not handled");
    }
    if(dimgeo==2) //surface
    {
        real_t t1=tp[0],t2=tp[1];
        string_t s;
        if(d==_id)
        {
            for(number_t i=0;i<dim;i++) v[i]=(*reinterpret_cast<const SymbolicFunction*>(pars(3+i).get_p()))(t1,t2);
            return v;
        }
        switch (d)
        {
            case _d1: s="@d1x";break;
            case _d2: s="@d2x";break;
            case _d11: s="@d11x";break;
            case _d12:
            case _d21: s="@d21x";break;
            case _d22: s="@d22x";break;
            default: error("free_error","in symbolic_f, derivative not handled");
        }
        for(number_t i=0;i<dim;i++) v[i]=(*reinterpret_cast<const SymbolicFunction*>(pars(s+tostring(i+1)).get_p()))(t1,t2);
        return v;
    }
    error("free_error","in symbolic_f, parmeter dimension greater than 2 not handled");
    return Vector<real_t>();
}

Vector<real_t> symbolic_invParametrization(const Point& p, Parameters& pars, DiffOpType d)
{
    number_t dim=pars(1), dimgeo=pars(2);
    Vector<real_t> v(dimgeo);
    if(dim==2)
    {
        real_t t1=p[0],t2=p[1];
        for(number_t i=0;i<dimgeo;i++) v[i]=(*reinterpret_cast<const SymbolicFunction*>(pars("@invParametrization"+tostring(i+1)).get_p()))(t1,t2);
        return v;
    }
    if(dim==3)
    {
        real_t t1=p[0],t2=p[1],t3=p[2];
        for(number_t i=0;i<dimgeo;i++) v[i]=(*reinterpret_cast<const SymbolicFunction*>(pars("@invParametrization"+tostring(i+1)).get_p()))(t1,t2,t3);
        return v;
    }
    error("free_error","in symbolic_f, dimension greater than 2 not handled");
    return Vector<real_t>();
}

Vector<real_t> symbolic_length(const Point& tp, Parameters& pars, DiffOpType d)
{
    number_t dim=pars(1), dimgeo=pars(2);
    Vector<real_t> res(1);
    const SymbolicFunction& fl=*reinterpret_cast<const SymbolicFunction*>(pars("@length").get_p());
    if(dimgeo==1) res[0]=fl(tp[0]);
    else if(dimgeo==2) res[0]= fl(tp[0],tp[1]);
    else error("free_error","in symbolic_length, dimension greater than 2 not handled");
    return res;
}

Vector<real_t> symbolic_curvature(const Point& tp, Parameters& pars, DiffOpType d)
{
    number_t dim=pars(1), dimgeo=pars(2);
    Vector<real_t> res(1);
    if(dim==3 && dimgeo==2)  res.resize(2);  // 2 curvatures
    const SymbolicFunction& fc=*reinterpret_cast<const SymbolicFunction*>(pars("@curvature").get_p());
    if(dimgeo==1) res[0]= fc(tp[0]);
    else if(dimgeo==2) res[0]= fc(tp[0],tp[1]);
    else error("free_error","in symbolic_curvature, parameter dimension greater than 2 not handled");
    if(dim==3 && dimgeo==2)  //3D surface
    {
        const SymbolicFunction& fb=*reinterpret_cast<const SymbolicFunction*>(pars("@bicurvature").get_p());
        res[1]= fb(tp[0],tp[1]);
    }
    return res;
}

Vector<real_t> symbolic_curabc(const Point& tp, Parameters& pars, DiffOpType d)
{
    number_t dim=pars(1), dimgeo=pars(2);
    Vector<real_t> res(1);
    if(dim==3 && dimgeo==2)  res.resize(2);  // 2 curvatures
    const SymbolicFunction& fa=*reinterpret_cast<const SymbolicFunction*>(pars("@curabc").get_p());
    if(dimgeo==1) res[0]=fa(tp[0]);
    else if(dimgeo==2) res[0]= fa(tp[0],tp[1]);
    else error("free_error","in symbolic_curabc, dimension greater than 2 not handled");
    if(dim==3 && dimgeo==2)  //3D surface
    {
        const SymbolicFunction& fb=*reinterpret_cast<const SymbolicFunction*>(pars("@bicurabc").get_p());
        res[1]= fb(tp[0],tp[1]);
    }
    return res;
}

Vector<real_t> symbolic_normal(const Point& tp, Parameters& pars, DiffOpType d)
{
    number_t dim=pars(1), dimgeo=pars(2);
    Vector<real_t> res(dim);
    if(dim==3 && dimgeo==1) res.resize(2*dim); // 3D curve has 2 normal vectors
    if(dimgeo==1)  // curve
    {
        real_t t=tp[0];
        for(number_t i=0;i<dim;i++)
            res[i]= (*reinterpret_cast<const SymbolicFunction*>(pars("@n"+tostring(i+1)).get_p()))(t);
        if(dim==3) // 3D curve: add binormal (assume symbolic binormal is given by user)
          for(number_t i=0;i<dim;i++)
            res[i+3]= (*reinterpret_cast<const SymbolicFunction*>(pars("@bn"+tostring(i+1)).get_p()))(t);
    }
    else if(dimgeo==2)
    {
        real_t t1=tp[0], t2=tp[1];
        for(number_t i=0;i<dim;i++)
            res[i]= (*reinterpret_cast<const SymbolicFunction*>(pars("@n"+tostring(i+1)).get_p()))(t1,t2);
    }
    else error("free_error","in symbolic_normal, dimension greater than 2 not handled");
    return res;
}

Vector<real_t> symbolic_tangent(const Point& tp, Parameters& pars, DiffOpType d)
{
    number_t dim=pars(1), dimgeo=pars(2);
    Vector<real_t> res(dim);
    if(dim==3 && dimgeo==2) res.resize(2*dim); // 3D surface has 2 tangent vectors
    if(dimgeo==1)
    {
        real_t t=tp[0];
        for(number_t i=0;i<dim;i++)
            res[i]= (*reinterpret_cast<const SymbolicFunction*>(pars("@t"+tostring(i+1)).get_p()))(t);
    }
    else if(dimgeo==2)
    {
        real_t t1=tp[0], t2=tp[1];
        for(number_t i=0;i<dim;i++)
            res[i]= (*reinterpret_cast<const SymbolicFunction*>(pars("@t"+tostring(i+1)).get_p()))(t1,t2);
        if(dim==3) // 3D surface: add bitangent (assume symbolic bitangent is given by user)
          for(number_t i=0;i<dim;i++)
            res[i+3]= (*reinterpret_cast<const SymbolicFunction*>(pars("@bt"+tostring(i+1)).get_p()))(t1,t2);
    }

    else error("free_error","in symbolic_normal, dimension greater than 2 not handled");
    return res;
}

//implementation of Parameter constructors from Parametrization here to avoid cross dependancy
Parameter::Parameter(const Parametrization& par, const string_t& nm, const string_t& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pointerParametrization)
{
  if ( snm != "" ) shortnames_.resize(1,snm);
  Parametrization& parb=*new Parametrization(par);
  p_ = static_cast<void *>(&parb);
}

Parameter::Parameter(const Parametrization& par, const string_t& nm, const Strings& snm)
 : i_(0), r_(0), c_(0.), s_(""), b_(false), p_(nullptr), name_(nm), key_(_pk_none), type_(_pointerParametrization)
{
  if ( snm.size() > 1 || snm[0] != "" ) shortnames_ = snm;
  Parametrization& parb=*new Parametrization(par);
  p_ = static_cast<void *>(&parb);
}

Parameter& Parameter::operator=(const Parametrization& par)
{
  if(type_==_pointerParametrization && reinterpret_cast<const Parametrization*>(p_)==&par) return *this;  // same parametrization
  deletePointer();
  Parametrization* np= new Parametrization(par);
  p_ = static_cast<void*>(np);
  type_=_pointerParametrization;
  return *this;
}

void deleteParametrization(void *p)
{
    if(p!=nullptr) delete reinterpret_cast<Parametrization*>(p);
}

void* cloneParametrization(const void* p)
{
    return reinterpret_cast<void*>(new Parametrization(*reinterpret_cast<const Parametrization*>(p)));
}

//===========================================================================================
//                         PiecewiseParametrization class functions
//===========================================================================================
PiecewiseParametrization::PiecewiseParametrization(Geometry* g, number_t n1,number_t n2, number_t n3)
: Parametrization(g)
{
  ns.resize(3);
  ns[0]=n1; ns[1]=n2; ns[2]=n3;  //n1 should not be equal to 0
  if(n2==0)
     geomSupport_p=new Segment(_v1=Point(0.),_v2=Point(1.),_domain_name="unit segment");
  else
   {
     if(n3==0) geomSupport_p=new SquareGeo(_origin=Point(0.,0.),_length=1.,_domain_name="unit square");
     else      geomSupport_p=new Cube(_origin=Point(0.,0.,0.),_length=1.,_domain_name="unit cube");
   }
  init(); // dim not correctly set in that case
  if(g!=nullptr) dim=g->dimPoint();
  contOrder = _notRegular;  // by default, if C0, C1 ... user update
  params=Parameters(reinterpret_cast<const void *>(this),"piecewise parametrization");
  f_p = parametrization_Piecewise;
  invParametrization_p = invParametrization_Piecewise;
  curvature_p = nullptr;
  length_p =0;
  normal_p = normal_Piecewise;
  tangent_p = tangent_Piecewise;
}

PiecewiseParametrization::~PiecewiseParametrization()
{
  //C0 parametrization
  std::map<Triplet<>, std::pair<Parametrization*, Transformation*> >:: iterator itm;
  for(itm=parsMap.begin();itm!=parsMap.end();++itm)
    if(itm->second.second!=nullptr) delete itm->second.second;
}

//! build the map neighborParsMap: Parametrization* -> vector of (Parametrization*,side number)
//  neighborParsMap[p][s] -> neighbor parametrisation by side s and its side number
//                        |
//      parametrization p | parametrization neighborParsMap[p][s].first
//                 side s | side neighborParsMap[p][s].second
//                        |
// give a way to travel across patches using non C0 parametrisation
void PiecewiseParametrization::buildNeighborParsMap(std::map<std::set<number_t>, std::list<std::pair<Geometry*, number_t> > >& sidemap)
{
  Parametrization* pz=nullptr;
  number_t nbs=2*dimg;
  std::map<std::set<number_t>, std::list<std::pair<Geometry*, number_t> > >::iterator its=sidemap.begin();
  for(;its!=sidemap.end();++its)
  {
    std::list<std::pair<Geometry*, number_t> >::iterator itg = its->second.begin();
    Parametrization* par1=const_cast<Parametrization*>(&itg->first->parametrization()), *par2=nullptr;
    number_t s1=itg->second, s2=0;
    if(its->second.size()>1)
    {
         itg++; par2=const_cast<Parametrization*>(&itg->first->parametrization()); s2=itg->second;
    }
    if(neighborParsMap.find(par1)==neighborParsMap.end())
       neighborParsMap[par1]=std::vector<std::pair<Parametrization*,number_t> >(nbs,std::make_pair(pz,0));
    neighborParsMap[par1][s1]=std::make_pair(par2,s2);
    if(par2!=nullptr)
    {
      if(neighborParsMap.find(par2)==neighborParsMap.end())
         neighborParsMap[par2]=std::vector<std::pair<Parametrization*,number_t> >(nbs,std::make_pair(pz,0));
      neighborParsMap[par2][s2]=std::make_pair(par1,s1);
    }
  }
  if(parsMap.size()==0) //build parsMap
  {
    std::map<Parametrization*, std::vector<std::pair<Parametrization*,number_t> > >::iterator itm=neighborParsMap.begin();
    number_t i=1;
    Transformation* tr=nullptr;
    for(;itm!=neighborParsMap.end();++itm, i++) add(itm->first,tr,i,1,1);
  }
  buildReverseMap();
}

//! build vertices vector and vertexIndexes map
//   neighborParsMap must be constructed
void PiecewiseParametrization::buildVertices()
{
  std::map<Parametrization*, std::vector<std::pair<Parametrization*,number_t> > >::iterator itm=neighborParsMap.begin();
  std::map<Point,number_t> vs;
  std::map<Point,number_t>::iterator itv;
  std::vector<number_t> ms;
  Point P;
  if(dimg==1) ms.resize(2);
  if(dimg==2) ms.resize(4);
  if(dimg==3) ms.resize(8);
  P.resize(dimg);
  for(;itm!=neighborParsMap.end();++itm)
  {
    Parametrization& par=*(itm->first);
    for(number_t i=0;i<ms.size();i++)
    {
      P*=0;
      if((i==1 ||i==2|| i==5 ||i==6)) P[0]=1;
      if(dimg>1 && (i==2 ||i==3|| i==6 ||i==7)) P[1]=1;
      if(dimg==3 && i>3) P[2]=1;
      Point Q=par(P);
      itv=vs.find(Q);
      if(itv==vs.end())
      {
        ms[i]=vs.size();
        vs.insert(std::make_pair(Q,ms[i]));
      }
      else ms[i]=itv->second;
    }
    vertexIndexes[&par]=ms;
  }
  vertices.resize(vs.size());
  for(itv=vs.begin();itv!=vs.end();++itv)
     vertices[itv->second]=itv->first;
}

/*! locate parametrization or nearest parametrization (generalization of toParameter function)
    p: point to be located
    dp: a direction to choose parametrization when on boundary (if void not used)
    par0 : first parametrization candidate (may be 0)
    q: parameter point found
    d: maximal distance p-par(q), output the distance found (if d=0, search parametrization such that par(q)=p)
  return Parametrization if found else return 0, q and d are updated
  - when d>0 and p does not belongs to a patch, for each parametrization a "projection" of p is done and the smallest distance (if <d) gives the parametrization
    the projection is the following:
    . project p on the linear patchs get from vertices of the parametrization. It gives some barycentric coordinates (l1,l2,...) related to the vertices
    . use this barycentric coordinates to get a parameter q by linear interpolation
    . project q on the parameters domain
    . "projection" of p: pr = par(q) and distance = norm(pr-p);
  - when there is more than one admissible parametrizations and one dp direction, the parametrizations selected is the one with the smallest angle between the projection of dp and dp
*/
Parametrization* PiecewiseParametrization::locateParametrization(const Point& p, const Point& dp, Parametrization* par0, Point& q, real_t& d) const
{
  number_t n=p.size(), ns;
  std::set<Parametrization*> pars;
  std::multimap<real_t,std::pair<Parametrization*,Point> > candidates;  //candidate parametrizations indexed by the distance of p to the bounding box of parametrized patch
  std::vector<std::pair<Parametrization*,number_t> >::const_iterator itp;
  std::multimap<real_t,std::pair<Parametrization*,Point> >::iterator itm;
  std::map<Parametrization*, std::vector<std::pair<Parametrization*,number_t> > >::const_iterator itn;
  Parametrization* par=par0, *par1;
  if(par==nullptr) par = neighborParsMap.begin()->first;
  //theCout<<"\nlocate p="<<p<<" dp="<<dp;
  Point ps, ps2, t, q1, q2;
  q1.resize(dimg), q2.resize(dimg);
  real_t d1, d2;

  while(pars.size()<neighborParsMap.size())
  {
    //theCout<<"  search in "<<(*par)<<eol;
    pars.insert(par);
    q=par->toParameter(p);
    if(q.size()>0) // parametrization found
    {
      if(dp.size()==0) return par;  // no direction to improve the choice, return the first one
      //theCout<<"   find in "<<(*par)<<" q="<<q<<eol;
      candidates.insert(std::make_pair(0.,std::make_pair(par,q)));
    }
    else if(d>0)// search a "projection" on patch
    {
      const std::vector<number_t>& vs=vertexIndexes.at(par); // cartesian bounding box of par
      const Point &v1=vertices[vs[0]], &v2=vertices[vs[1]];
      q1.clear();d1=theRealMax;
      if(dimg==1) // curve
      {
        ps=projectionOnSegment(p,v1,v2,d1);
        t=v2-v1;
        q1[0]=dot(ps-v1,t)/dot(t,t);
      }
      else if (dimg==2) // surface
      {
        const Point &v3=vertices[vs[2]], &v4=vertices[vs[3]];
        ps=toBarycentricNocheck(p,v1,v2,v3);
        if(ps.size()!=0)
        {
          q1=Point(ps[1]+ps[2],ps[2]);
          q1[0]=std::max(0.,std::min(q1[0],1.)); q1[1]=std::max(0.,std::min(q1[1],1.));
          d1=norm(p-(*par)(q1));
        }
        ps=toBarycentricNocheck(p,v1,v3,v4);
        if(ps.size()!=0)
        {
          q2=Point(ps[1],ps[1]+ps[2]);
          q2[0]=std::max(0.,std::min(q2[0],1.)); q2[1]=std::max(0.,std::min(q2[1],1.));
          d2=norm(p-(*par)(q2));
          if(d2<d1) {q1=q2;d1=d2;}
        }
      }
      else error("free_error","PiecewiseParametrization::locateParametrization not yet designed for 3D parametrization");
      //theCout<<"   check  d="<<d1<<" < "<<d<<" q="<<q1<<eol;
      if(d1<=d) candidates.insert(std::make_pair(d1,std::make_pair(par,q1))); // stored as a candidate
    }
    //update next parametrization to visit
    const std::vector<std::pair<Parametrization*,number_t> >& np=neighborParsMap.at(par);
    par=nullptr;
    for(itp=np.begin();itp!=np.end();++itp) //loop on neighbor parametrization
    {
      par=itp->first;
      if(par!=nullptr && pars.find(par)==pars.end()) break;
    }
    if(par==nullptr && pars.size()<neighborParsMap.size()) // no free neighbor parametrization, find a free one in all free
    {
       for(itn=neighborParsMap.begin();itn!=neighborParsMap.end();++itn)
       {
         par=itn->first;
         if(pars.find(par)==pars.end()) break;
       }
    }
  }
  //theCout<<" candidates: "<<candidates<<eol;
  // keep candidates having admissible direction projection
  Point du,dv,pdp , np;
  if(dimg==2)
  {
    for(itm=candidates.begin();itm!=candidates.end();)
    {
      par1=itm->second.first; q1=itm->second.second;
      du=(*par1)(q1,_d1); du/=norm(du);
      dv=(*par1)(q1,_d2); dv/=norm(dv);
      np=cross3D(du,dv);
      pdp=dp-dot(dp,np)*np;
      if((q1[0]<theTolerance && dot(du,pdp)<-theTolerance) ||  // u=0 and outgoing
         (q1[0]>1-theTolerance && dot(du,pdp)>theTolerance)||  // u=1 and outgoing
         (q1[1]<theTolerance && dot(dv,pdp)<-theTolerance) ||  // v=0 and outgoing
         (q1[1]>1-theTolerance && dot(dv,pdp)>theTolerance))   // v=1 and outgoing
         candidates.erase(itm++);
      else ++itm;
    }
  }
  //theCout<<" admissible candidates: "<<candidates<<eol<<std::flush;
  if(dp.size()==0) //return the first one if admissible
  {
     d1=candidates.begin()->first;
     if(d1>d) {q.clear();return nullptr;}
     d=d1;
     q=candidates.begin()->second.second;
     return candidates.begin()->second.first;
  }
  //use the direction dp to improve the selection
  real_t smin=theRealMax, s;
  Point dpn=dp/norm(dp);
  par=nullptr;q.clear();
  //theCout<<"  check best slope"<<eol;
  for(itm=candidates.begin();itm!=candidates.end();++itm)
  {
    par1=itm->second.first; q1=itm->second.second;
    du=(*par1)(q1,_d1); du/=norm(du);
    if(dimg==1)  s=std::abs(1-dot(du,dpn));
    else
    {
      dv=(*par1)(q1,_d2); dv/=norm(dv);
      s=std::abs(dot(cross3D(du,dv),dpn));
    }
    s+=itm->first;  //balance slope and distance
    //theCout<<" s="<<s<<" d="<<itm->first<<(*par1)<<eol;
    if(s<smin) {par=par1;q=q1;d=itm->first;smin=s;}
  }
  return par;
}

//!< inverse function of the parametrization
Point PiecewiseParametrization::toParameter(const Point& p) const
{
   Parameters pars;
   if(contOrder==_regC0) return invC0Parametrization(p,pars,_id);
   return invParametrization(p, pars,_id);
}

// virtual global parametrization
//    u-interval [0,1] is split in n parts [0,1/n],]1/n,2/n], ... ](n-1)/n,1]
//    v,w-interval [0,1] not split
//    first parametrization of map vertexIndexes corresponds to some parameters in [0,1/n]x[0,1]x[0,1], ...
//    last parametrization of map vertexIndexes corresponds to some parameters in [(n-1)/n,1]x[0,1]x[0,1]
// assume each local parametrization defined on [0,1]x[0,1]x[0,1]
// ### be cautious: discontinuous parametrization at i/n: f(i/n+,...)!=f(i/n-,...)
Vector<real_t> PiecewiseParametrization::funParametrization(const Point& pt, Parameters& pars, DiffComputation dc, DiffOpType d) const
{
 if(contOrder==_regC0) return funC0Parametrization(pt,pars,dc,_id);
 number_t n=neighborParsMap.size();
 Point q=pt;
 number_t i=std::floor(std::max((pt[0]-theTolerance),0.)*n);
 q[0]=n*pt[0]-i;
 std::map<Parametrization*, std::vector<std::pair<Parametrization*,number_t> > >::const_iterator itm=neighborParsMap.begin();
 for(number_t k=0;k<i;k++) itm++;
 Parametrization& par=*itm->first;
 switch (dc)
 {
   case _IdComputation: return par(q,d);
   case _lengthsComputation: return par.lengths(q,d);
   case _curvaturesComputation: return par.curvatures(q,d);
   case _normalComputation: return par.normal(q,d);
   case _tangentComputation: return par.tangent(q,d);
   default: error("free_error"," computation type not handled in PiecewiseParametrization::funParametrization");
 }
 return Vector<real_t>();   // fake return
}

Vector<real_t> PiecewiseParametrization::invParametrization(const Point& p, Parameters& pars, DiffOpType d) const
{
 if(contOrder==_regC0) return invC0Parametrization(p,pars,_id);
 Point q; real_t di=0.;
 Parametrization* par = locateParametrization(p,Point(),0,q,di);
 if(par==nullptr) error("free_error","PiecewiseParametrization::invParametrization, locateParamerization fails");
 std::map<Parametrization*, std::vector<std::pair<Parametrization*,number_t> > >::const_iterator itm=neighborParsMap.begin();
 number_t n=neighborParsMap.size(),k=0;
 for(;k<n;k++)
   if(par==itm->first) break;
 q[0]=(k+q[0])/n;
 return q;
}

void PiecewiseParametrization::print(std::ostream& os) const
{
 Parametrization::print(os);
 if(theVerboseLevel<2) return;
 number_t d=1;
 if(ns[1]>0) d=2;
 if(ns[2]>0) d=3;
 os<<", "<<parsMap.size()<<" parts"<<eol;
 os<<"  vertices: "<<roundToZero(vertices);
 std::map<Triplet<>, std::pair<Parametrization*, Transformation*> >::const_iterator itm= parsMap.begin();
 for(;itm!=parsMap.end();++itm)
 {
   Triplet<> ijk=itm->first;
   os<<"\n  part "<<ijk.first;
   if(contOrder>_notRegular)
   {
     if(d>1) os<<" "<<ijk.second;
     if(d>2) os<<" "<<ijk.third;
   }

   os<<": "<<(*itm->second.first)<<eol;
   if(itm->second.second!=nullptr) os<<"  "<<(*itm->second.second);
   else os<<"  no transformation";
   os<<", ";
   os<<"vertex numbers: "<< vertexIndexes.at(itm->second.first)<<", ";
   const std::vector<std::pair<Parametrization*,number_t> >& nps = neighborParsMap.at(itm->second.first);
   if(nps.size()>0) os<<" neighbor parts:";
   Strings s("v=0","u=1","v=1","u=0","w=0","w=1");
   for(number_t i=0;i<nps.size();i++)
   {
     if(nps[i].first!=nullptr)
     {
       Triplet<> ijk = parsRMap.at(nps[i].first);
       if(contOrder==_notRegular)
         os<<" part "<<ijk.first<<" ("<<s[i]<<" <=> "<<s[nps[i].second]<<")";
       else
       {
         os<<" part "<<ijk.first;
         if(d>1) os<<" "<<ijk.second;
         if(d>2) os<<" "<<ijk.third;
       }
     }
   }
 }
}

//-------------------------------------------------------------------------------------------------------
//  specific stuff for C0 parametrisation
//-------------------------------------------------------------------------------------------------------

bool PiecewiseParametrization::add(Parametrization* p, Transformation * tr, number_t i, number_t j, number_t k)
{
  Triplet<> t(i,j,k);
  if(parsMap.find(t)!=parsMap.end()) {warning("free_warning","duplicated cell in PiecewiseParametrization");return false;}
  if(tr!=nullptr) parsMap[t]=std::make_pair(p,new Transformation(*tr)); // copy transformation if tr!=nullptr
  else parsMap[t]=std::make_pair(p,tr);                           // tr=0 means identity transformation
  return true;
}

void PiecewiseParametrization::buildReverseMap()
{
  parsRMap.clear();
  std::map<Triplet<>, std::pair<Parametrization*, Transformation*> >:: iterator itm;
  for(itm=parsMap.begin();itm!=parsMap.end();++itm)
    {
      if(parsRMap.find(itm->second.first)!=parsRMap.end()) error("free_error","duplicated parametrization in PiecewiseParametrization");
      parsRMap[itm->second.first]=itm->first;
    }
}

//locate cell
std::map<Triplet<>, std::pair<Parametrization*, Transformation*> >::const_iterator PiecewiseParametrization::locateCell(const Point& pt) const
{
//    Triplet<> ijk;
//    if(dimg>=1) ijk.first = std::min(1+std::max(int_t(0),int_t(std::floor((pt[0]-theTolerance)*ns[0]))),int_t(ns[0]));
//    if(dimg>=2) ijk.second= std::min(1+std::max(int_t(0),int_t(std::floor((pt[1]-theTolerance)*ns[1]))),int_t(ns[1]));
//    if(dimg>=3) ijk.third = std::min(1+std::max(int_t(0),int_t(std::floor((pt[2]-theTolerance)*ns[2]))),int_t(ns[2]));
    std::map<Triplet<>, std::pair<Parametrization*, Transformation*> >::const_iterator ite = parsMap.end(), itm=ite;
    std::list<int_t> i, j, k;
    int_t z=int_t(0), n, i0, j0, k0;
    double t0;
    if(dimg>=1)
    {
      t0=ns[0]*pt[0]; n = int_t(ns[0]);
      i0=floor(t0); i0=std::min(1+std::max(z,i0),n);
      i.push_back(i0);
      if(std::abs(i0-t0)<theTolerance)
      { if(t0>i0 && i0>1) i.push_back(i0-1);
        if(t0<i0 && i0<n) i.push_back(i0+1);
      }
    }
    if(dimg==1)
      {
        for(std::list<int_t>::iterator iti=i.begin();iti!=i.end() && itm==ite; ++iti)
            itm= parsMap.find(Triplet<>(*iti,0,0));
        return itm;
      }

    if(dimg>=2)
    {
      t0=ns[1]*pt[1]; n = int_t(ns[1]);
      j0=floor(t0); j0=std::min(1+std::max(z,j0),n);
      j.push_back(j0);
      if(std::abs(j0-t0)<theTolerance)
      { if(t0>j0 && j0>1) j.push_back(j0-1);
        if(t0<j0 && j0<n) j.push_back(j0+1);
      }
    }
    //theCout<<"pt="<<pt<<" i0 ="<<i0<<" j0="<<j0<<" i="<<i<<" j="<<j<<eol<<std::flush;
    if(dimg==2)
      {
        for(std::list<int_t>::iterator iti=i.begin();iti!=i.end() && itm==ite; ++iti)
          for(std::list<int_t>::iterator itj=j.begin();itj!=j.end() && itm==ite; ++itj)
            itm= parsMap.find(Triplet<>(*iti,*itj,0));
        return itm;
      }
    //dimg=3
    t0=ns[2]*pt[2]; n = int_t(ns[2]);
    k0=floor(t0); k0=std::min(1+std::max(z,k0),n);
    k.push_back(k0);
    if(std::abs(k0-t0)<theTolerance)
      { if(t0>k0 && k0>1) k.push_back(k0-1);
        if(t0<k0 && k0<n) k.push_back(k0+1);
      }
    for(std::list<int_t>::iterator iti=i.begin();iti!=i.end() && itm==ite; ++iti)
      for(std::list<int_t>::iterator itj=j.begin();itj!=j.end() && itm==ite; ++itj)
        for(std::list<int_t>::iterator itk=k.begin();itk!=k.end() && itm==ite; ++itk)
          itm= parsMap.find(Triplet<>(*iti,*itj,*itk));
    return itm;
}
// compute parametrization using local parametrization functions
// because they are global, curvilinear abcissa are computed using global quadrature and local lengths (see Parametrization::curabcs function)
Vector<real_t> PiecewiseParametrization::funC0Parametrization(const Point& pt, Parameters& pars, DiffComputation dc, DiffOpType d) const
{
    Vector<real_t> R; R.clear();
    std::map<Triplet<>, std::pair<Parametrization*, Transformation*> >::const_iterator itm = locateCell(pt);
    if(itm==parsMap.end()) return R;  // out of parametrization return void vector
    const Triplet<>& ijk=itm->first;
    const Parametrization& par_ijk = *(itm->second.first);
    const Transformation* t_ijk = itm->second.second;
    Point q=pt;
    q[0]=pt[0]*ns[0]-ijk.first+1;
    if(dimg>=2) q[1]=pt[1]*ns[1]-ijk.second+1;
    if(dimg>=3) q[2]=pt[2]*ns[2]-ijk.third+1;
    if(t_ijk!=nullptr) q=t_ijk->apply(q);
    switch (dc)
      {
          case _IdComputation: return par_ijk(q,d);
          case _lengthsComputation: return par_ijk.lengths(q,d);
          case _curvaturesComputation: return par_ijk.curvatures(q,d);
          case _normalComputation: return par_ijk.normal(q,d);
          case _tangentComputation: return par_ijk.tangent(q,d);
          default: error("free_error"," computation type not handled in PiecewiseParametrization::funParametrization");
      }
      return R;   // fake return
}

Vector<real_t> PiecewiseParametrization::invC0Parametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  std::map<Triplet<>, std::pair<Parametrization*, Transformation*> >::const_iterator itm=parsMap.begin();
  for(;itm!=parsMap.end();++itm)
  {
    Point q=itm->second.first->toParameter(pt);
    Triplet<> ijk=itm->first;
    if(q.size()>0)
    {
      Point qi=q;
      if(itm->second.second!=nullptr) qi= itm->second.second->inverse(q);  //apply inverse transformation to parameter
      if(ijk.first!=0)  qi[0]=std::min(std::max((qi[0]+ijk.first-1) /ns[0],0.),1.);
      if(ijk.second!=0) qi[1]=std::min(std::max((qi[1]+ijk.second-1)/ns[1],0.),1.);
      if(ijk.third!=0)  qi[2]=std::min(std::max((qi[2]+ijk.third-1) /ns[2],0.),1.);
      return qi;
    }
  }
  return Vector<real_t>();
}

} // end of namespace xlifepp
