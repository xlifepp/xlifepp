/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file geometries3D.hpp
  \authors Y. Lafranche, N. Kielbasiewicz
  \since 18 oct 2012
  \date 30 mars 2021

  \brief Definition of the classes corresponding to 3D canonical geometrical shapes.

  It is quite easy to find in which category a geometry is, according to their mathematical
  definition, but they are some exceptions:
  - Cylinder and Cone are surfaces but can also represent volumes inside
  - Ellipse is a closed curve or the surface inside (whereas there is the
    distinction Circle / Disk)
  - Ellipsoid is a closed surface or the volume inside (whereas there is the
    distinction Sphere / Ball)

  It becomes all the more tricky than if we respect the geometrical hierarchy, we will derive a
  prism (semantically 3D) from a cylinder (2D or 3D) or a pyramid (semantically 3D) from a cone.
  Another example is the general definition of a cylinder, from a surface and an axis: it supposes
  that the basis is a surface. But ellipses are supposed to be curves !!!

  This is the reason why we chose to defines ellipses as surfaces, ellipsoids as volumes (coherence) and cylinders
  and cones as volumes.

*/

#ifndef GEOMETRIES_3D_HPP
#define GEOMETRIES_3D_HPP

#include "Geometry.hpp"

namespace xlifepp
{

/*!
  \class Volume
  base class for 3D geometries
*/
class Volume : public Geometry
{
  public:
    //! default constructor for 3D geometries
    Volume();

  protected:
    std::vector<Point> p_;        //!< construction points
    std::vector<number_t> n_;     //!< number of nodes on each edge
    std::vector<real_t> h_;       //!< local mesh step on each vertex
    //@{
    //! true constructor functions
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    Volume(const Volume& v) : Geometry(v) {p_ = v.p_; n_ = v.n_; h_ = v.h_;} //!< copy constructor
    virtual Geometry* clone() const { return new Volume(*this); } //!< virtual copy constructor for Geometry
    virtual ~Volume() {} //!< virtual destructor

    //! accessor to points
    const std::vector<Point>& p() const { return p_; }
    //! accessor to point i
    const Point& p(number_t i) const { return p_[i - 1]; }
    //! accessor to number of nodes (read only)
    const std::vector<number_t>& n() const { return n_; }
    //! accessor to number of nodes (read/write)
    std::vector<number_t>& n() { return n_;}
    //! accessor to number of nodes on edge i (read only)
    number_t n(number_t i) const { return n_[i - 1]; }
    //! accessor to local steps (read only)
    const std::vector<real_t>& h() const {return h_;}
    //! accessor to local steps (read only)
    std::vector<real_t>& h() {return h_;}
    //! accessor to local step on vertex i (read only)
    real_t h(number_t i) const { return h_[i - 1];}
    void setHstep(real_t hs) {h_ = std::vector<real_t>(p_.size(), hs);}
    bool withNnodes() const { return (h_.size() == 0); }
    void setNnodes(number_t i) {number_t d = n_.size(); n_.clear(); n_.resize(d, i);}

    //! check if points of current volume are points of a translated points of an other volume
    virtual bool isTranslated(const Geometry&, Point&) const;

    const Volume* volume() const //! access to Volume object (const), virtual in Geometry
    { return this; }
    Volume* volume()  //! access to Volume object (non const), virtual in Geometry
    { return this; }

    //! format as string
    virtual string_t asString() const { error("shape_not_handled", words("shape", shape_)); return string_t(); }

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Volume
    virtual Volume& transform(const Transformation& t);
    //! apply a translation on a Volume (1 key)
    virtual Volume& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Volume (vector version)
    virtual Volume& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Volume::translate(Reals)", "Volume::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Volume (3 reals version)
    virtual Volume& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Volume::translate(Real, Real, Real)", "Volume::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Volume (1 key)
    virtual Volume& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Volume (2 keys)
    virtual Volume& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Volume
    virtual Volume& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Volume::rotate2d(Point, Real)", "Volume::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Volume (1 key)
    virtual Volume& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Volume (2 keys)
    virtual Volume& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Volume (3 keys)
    virtual Volume& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Volume
    virtual Volume& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Volume::rotate3d(Point, Reals, Real)", "Volume::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Volume
    virtual Volume& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Volume::rotate3d(Real, Real, Real)", "Volume::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Volume
    virtual Volume& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Volume::rotate3d(Real, Real, Real, Real)", "Volume::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Volume
    virtual Volume& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Volume::rotate3d(Point, Real, Real, Real)", "Volume::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Volume
    virtual Volume& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Volume::rotate3d(Point, Real, Real, Real, Real)", "Volume::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Volume (1 key)
    virtual Volume& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Volume (2 keys)
    virtual Volume& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Volume
    virtual Volume& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Volume::homothetize(Point, Real)", "Volume::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Volume
    virtual Volume& homothetize(real_t factor)
    {
      warning("deprecated", "Volume::homothetize(Real)", "Volume::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Volume (1 key)
    virtual Volume& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Volume
    virtual Volume& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Volume::pointReflect(Point)", "Volume::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Volume (1 key)
    virtual Volume& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Volume (2 keys)
    virtual Volume& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Volume
    virtual Volume& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Volume::reflect2d(Point, Reals)", "Volume::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Volume
    virtual Volume& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Volume::reflect2d(Point, Real, Real)", "Volume::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Volume (1 key)
    virtual Volume& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Volume (2 keys)
    virtual Volume& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Volume
    virtual Volume& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Volume::reflect3d(Point, Reals)", "Volume::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Volume
    virtual Volume& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Volume::reflect3d(Point, Real, Real, Real)", "Volume::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
  \class Polyhedron
  definition of a polyhedral geometry in R^3
  Generally, a polyhedron is a list of polygonal faces. But data is storages differently to avoid pointer manipulation
  So, we decide to store:
  - the list of vertices -> Vector<Point> p_
  - the definition of faces as a list of points -> Vector<Vector<number_t> > faces_
    it is so that p_[faces_[i][j]] is the j-st vertex of the i-st face of the polyhedron
  - the number of nodes on each edge -> Vector<Vector<number_t> > n_
    it is so that n_[i][j] is the number of nodes on j-st edge [ p_[faces_[i][j]] p_[faces_[i][j+1]] ] of the i-st face
    of the polyhedron

  Polyhedron constructors are based on a key-value system. Here are the available keys:
  - _faces: the geometrical faces defining the Polyhedron
  - _nnodes: to define the number of nodes on each edge of the Polyhedron
  - _hsteps: to define the local mesh steps on the vertices of the Polyhedron
  - _domain_name: to define the domain name
  - _varnames: to define the variable names for print purpose
  Domain names on boundaries (faces, edges and vertices) are defined by the Geometry objects given to _faces key
 */
class Polyhedron : public Volume
{
  protected:
    std::vector<Polygon*> faces_; //!< faces of the polyhedron
    //std::vector<std::pair<number_t, number_t> > edges_; //!< list of edges described by the pair of point numbers (starting from 0)

  public:
    //! default constructor
    Polyhedron();
    //@{
    //! key-value constructor
    Polyhedron(Parameter p1);
    Polyhedron(Parameter p1, Parameter p2);
    //@}
  
  protected:
    //@{
    //! true constructor functions
    void buildP();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! copy constructor
    Polyhedron(const Polyhedron& ph);
    //! destructor
    virtual ~Polyhedron()
    {
      for (number_t i = 0; i < faces_.size(); ++i) { delete faces_[i]; }
      faces_.clear();
    }

    //! accessor to faces
    const std::vector<Polygon*>& faces() const { return faces_; }
    //! accessor to number of faces
    number_t nbFaces() const { return faces_.size(); }
    //! accessor to number of nodes on edge i
    number_t nOnEdge(number_t i, number_t j) const { return faces_[i - 1]->n(j); }

    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new Polyhedron(*this); } //!< virtual copy constructor

    bool withNnodes() const { return (h_.size() == 0); } //!< check if Polyhedron is defined only with _nnodes or with _hsteps option

    virtual std::vector<const Point*> boundNodes() const;//!< returns list of points on boundary (const)
    virtual std::vector<Point*> nodes();                 //!< returns list of nodes (non const)
    virtual std::vector<const Point*> nodes() const;     //!< returns list of nodes (const)
    virtual std::vector<Point*> wholeNodes()             //!< returns list of every point (non const)
    {return nodes();}
    virtual std::vector<const Point*> wholeNodes() const //!< returns list of every point (const)
    {return nodes();}
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< returns list of edges
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of faces
    virtual number_t nbSides() const { return faces_.size(); } //!< returns the number of sides
    virtual void setFaces()             //! set the faces vector
    {error("not_handled", "Polyhedron::setFaces()");}
    //    virtual void setEdges();            //!< set the edges vector
    void updateSideNames();             //!< update side names from face domain names (do not use for child of Polyhedron)
    virtual void collect(const string_t& n, std::list<Geometry*>&) const; //!< collect in a list all canonical geometry's with name n

    //! computes the minimal box
    virtual void computeMB() { minimalBox = MinimalBox(boundingBox.bounds()); }
    //! access to child Polyhedron object (const)
    virtual const Polyhedron* polyhedron() const {return this;}
    //! access to child Polyhedron object
    virtual Polyhedron* polyhedron() {return this;}
    //! create boundary geometry
    virtual Geometry& buildBoundary() const;

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Polyhedron
    virtual Polyhedron& transform(const Transformation& t);
    //! apply a translation on a Polyhedron (1 key)
    virtual Polyhedron& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Polyhedron (vector version)
    virtual Polyhedron& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Polyhedron::translate(Reals)", "Polyhedron::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Polyhedron (3 reals version)
    virtual Polyhedron& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Polyhedron::translate(Real, Real, Real)", "Polyhedron::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Polyhedron (1 key)
    virtual Polyhedron& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Polyhedron (2 keys)
    virtual Polyhedron& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Polyhedron
    virtual Polyhedron& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Polyhedron::rotate2d(Point, Real)", "Polyhedron::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Polyhedron (1 key)
    virtual Polyhedron& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Polyhedron (2 keys)
    virtual Polyhedron& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Polyhedron (3 keys)
    virtual Polyhedron& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Polyhedron
    virtual Polyhedron& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Polyhedron::rotate3d(Point, Reals, Real)", "Polyhedron::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Polyhedron
    virtual Polyhedron& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Polyhedron::rotate3d(Real, Real, Real)", "Polyhedron::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Polyhedron
    virtual Polyhedron& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Polyhedron::rotate3d(Real, Real, Real, Real)", "Polyhedron::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Polyhedron
    virtual Polyhedron& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Polyhedron::rotate3d(Point, Real, Real, Real)", "Polyhedron::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Polyhedron
    virtual Polyhedron& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Polyhedron::rotate3d(Point, Real, Real, Real, Real)", "Polyhedron::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Polyhedron (1 key)
    virtual Polyhedron& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Polyhedron (2 keys)
    virtual Polyhedron& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Polyhedron
    virtual Polyhedron& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Polyhedron::homothetize(Point, Real)", "Polyhedron::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Polyhedron
    virtual Polyhedron& homothetize(real_t factor)
    {
      warning("deprecated", "Polyhedron::homothetize(Real)", "Polyhedron::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Polyhedron (1 key)
    virtual Polyhedron& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Polyhedron
    virtual Polyhedron& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Polyhedron::pointReflect(Point)", "Polyhedron::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Polyhedron (1 key)
    virtual Polyhedron& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Polyhedron (2 keys)
    virtual Polyhedron& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Polyhedron
    virtual Polyhedron& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Polyhedron::reflect2d(Point, Reals)", "Polyhedron::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Polyhedron
    virtual Polyhedron& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Polyhedron::reflect2d(Point, Real, Real)", "Polyhedron::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Polyhedron (1 key)
    virtual Polyhedron& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Polyhedron (2 keys)
    virtual Polyhedron& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Polyhedron
    virtual Polyhedron& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Polyhedron::reflect3d(Point, Reals)", "Polyhedron::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Polyhedron
    virtual Polyhedron& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Polyhedron::reflect3d(Point, Real, Real, Real)", "Polyhedron::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
  \class Tetrahedron
  definition of a tetrahedron geometry in R^3

  Tetrahedron constructors are based on a key-value system. Here are the available keys:
  - _v1, _v2, _v3, _v4: to define the vertices of the tetrahedron
  - _nnodes: to define the number of nodes on each edge of the Tetrahedron
  - _hsteps: to define the local mesh steps on the vertices of the Tetrahedron
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class Tetrahedron : public Polyhedron
{
  public:
    //! default constructor
    Tetrahedron();
    //@{
    //! key-value constructor
    Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    //@}

  private:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:

    //! format as string
    virtual string_t asString() const;
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< returns list of edges
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of faces

    virtual Geometry* clone() const { return new Tetrahedron(*this); } //!< virtual copy constructor

    //    virtual std::vector<std::pair<ShapeType,std::vector<const Point*> > > curves() const; //!< returns list of edges
    //    virtual std::vector<std::pair<ShapeType,std::vector<const Point*> > > surfs() const; //!< returns list of faces
    virtual number_t nbSides() const { return 4; } //!< returns the number of sides (faces)
    virtual void setFaces();                       //!< set the faces vector when built
    //    virtual void setEdges();                       //!< set the edges vector
    real_t measure() const;

    //! computes the minimal box
    virtual void computeMB() { minimalBox = MinimalBox(p_[0], p_[1], p_[2], p_[3]); }

    //! access to child Tetrahedron object (const)
    virtual const Tetrahedron* tetrahedron() const {return this;}
    //! access to child Tetrahedron object
    virtual Tetrahedron* tetrahedron() {return this;}

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Tetrahedron
    virtual Tetrahedron& transform(const Transformation& t);
    //! apply a translation on a Tetrahedron (1 key)
    virtual Tetrahedron& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Tetrahedron (vector version)
    virtual Tetrahedron& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Tetrahedron::translate(Reals)", "Tetrahedron::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Tetrahedron (3 reals version)
    virtual Tetrahedron& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Tetrahedron::translate(Real, Real, Real)", "Tetrahedron::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Tetrahedron (1 key)
    virtual Tetrahedron& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Tetrahedron (2 keys)
    virtual Tetrahedron& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Tetrahedron
    virtual Tetrahedron& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Tetrahedron::rotate2d(Point, Real)", "Tetrahedron::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Tetrahedron (1 key)
    virtual Tetrahedron& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Tetrahedron (2 keys)
    virtual Tetrahedron& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Tetrahedron (3 keys)
    virtual Tetrahedron& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Tetrahedron
    virtual Tetrahedron& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Tetrahedron::rotate3d(Point, Reals, Real)", "Tetrahedron::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Tetrahedron
    virtual Tetrahedron& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Tetrahedron::rotate3d(Real, Real, Real)", "Tetrahedron::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Tetrahedron
    virtual Tetrahedron& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Tetrahedron::rotate3d(Real, Real, Real, Real)", "Tetrahedron::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Tetrahedron
    virtual Tetrahedron& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Tetrahedron::rotate3d(Point, Real, Real, Real)", "Tetrahedron::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Tetrahedron
    virtual Tetrahedron& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Tetrahedron::rotate3d(Point, Real, Real, Real, Real)", "Tetrahedron::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Tetrahedron (1 key)
    virtual Tetrahedron& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Tetrahedron (2 keys)
    virtual Tetrahedron& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Tetrahedron
    virtual Tetrahedron& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Tetrahedron::homothetize(Point, Real)", "Tetrahedron::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Tetrahedron
    virtual Tetrahedron& homothetize(real_t factor)
    {
      warning("deprecated", "Tetrahedron::homothetize(Real)", "Tetrahedron::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Tetrahedron (1 key)
    virtual Tetrahedron& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Tetrahedron
    virtual Tetrahedron& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Tetrahedron::pointReflect(Point)", "Tetrahedron::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Tetrahedron (1 key)
    virtual Tetrahedron& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Tetrahedron (2 keys)
    virtual Tetrahedron& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Tetrahedron
    virtual Tetrahedron& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Tetrahedron::reflect2d(Point, Reals)", "Tetrahedron::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Tetrahedron
    virtual Tetrahedron& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Tetrahedron::reflect2d(Point, Real, Real)", "Tetrahedron::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Tetrahedron (1 key)
    virtual Tetrahedron& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Tetrahedron (2 keys)
    virtual Tetrahedron& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Tetrahedron
    virtual Tetrahedron& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Tetrahedron::reflect3d(Point, Reals)", "Tetrahedron::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Tetrahedron
    virtual Tetrahedron& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Tetrahedron::reflect3d(Point, Real, Real, Real)", "Tetrahedron::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
  \class Hexahedron
  definition of a hexahedron geometry in R^3

  Hexahedron constructors are based on a key-value system. Here are the available keys:
  - _v1, _v2, _v3, _v4, _v5, _v6, _v7, _v8: to define the vertices of the Hexahedron
  - _nnodes: to define the number of nodes on each edge of the Hexahedron
  - _hsteps: to define the local mesh steps on the vertices of the Hexahedron
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class Hexahedron : public Polyhedron
{
  public:
    //! default constructor
    Hexahedron();
    //@{
    //! key-value constructor
    Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11);
    Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12);
    Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13);
    Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14);
    //@}
    
  protected:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! format as string
    virtual string_t asString() const;
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< returns list of edges
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of faces

    virtual Geometry* clone() const { return new Hexahedron(*this); } //!< virtual copy constructor

    //    virtual std::vector<std::pair<ShapeType,std::vector<const Point*> > > curves() const; //!< returns list of edges
    //    virtual std::vector<std::pair<ShapeType,std::vector<const Point*> > > surfs() const; //!< returns list of faces
    virtual number_t nbSides() const { return 8; } //!< returns the number of sides
    virtual void setFaces();                       //!< set the faces vector when built
    //    virtual void setEdges();                       //!< set the edges vector

    //! access to child Hexahedron object (const)
    virtual const Hexahedron* hexahedron() const {return this;}
    //! access to child Hexahedron object
    virtual Hexahedron* hexahedron() {return this;}

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Hexahedron
    virtual Hexahedron& transform(const Transformation& t);
    //! apply a translation on a Hexahedron (1 key)
    virtual Hexahedron& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Hexahedron (vector version)
    virtual Hexahedron& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Hexahedron::translate(Reals)", "Hexahedron::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Hexahedron (3 reals version)
    virtual Hexahedron& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Hexahedron::translate(Real, Real, Real)", "Hexahedron::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Hexahedron (1 key)
    virtual Hexahedron& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Hexahedron (2 keys)
    virtual Hexahedron& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Hexahedron
    virtual Hexahedron& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Hexahedron::rotate2d(Point, Real)", "Hexahedron::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Hexahedron (1 key)
    virtual Hexahedron& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Hexahedron (2 keys)
    virtual Hexahedron& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Hexahedron (3 keys)
    virtual Hexahedron& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Hexahedron
    virtual Hexahedron& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Hexahedron::rotate3d(Point, Reals, Real)", "Hexahedron::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Hexahedron
    virtual Hexahedron& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Hexahedron::rotate3d(Real, Real, Real)", "Hexahedron::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Hexahedron
    virtual Hexahedron& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Hexahedron::rotate3d(Real, Real, Real, Real)", "Hexahedron::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Hexahedron
    virtual Hexahedron& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Hexahedron::rotate3d(Point, Real, Real, Real)", "Hexahedron::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Hexahedron
    virtual Hexahedron& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Hexahedron::rotate3d(Point, Real, Real, Real, Real)", "Hexahedron::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Hexahedron (1 key)
    virtual Hexahedron& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Hexahedron (2 keys)
    virtual Hexahedron& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Hexahedron
    virtual Hexahedron& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Hexahedron::homothetize(Point, Real)", "Hexahedron::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Hexahedron
    virtual Hexahedron& homothetize(real_t factor)
    {
      warning("deprecated", "Hexahedron::homothetize(Real)", "Hexahedron::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Hexahedron (1 key)
    virtual Hexahedron& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Hexahedron
    virtual Hexahedron& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Hexahedron::pointReflect(Point)", "Hexahedron::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Hexahedron (1 key)
    virtual Hexahedron& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Hexahedron (2 keys)
    virtual Hexahedron& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Hexahedron
    virtual Hexahedron& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Hexahedron::reflect2d(Point, Reals)", "Hexahedron::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Hexahedron
    virtual Hexahedron& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Hexahedron::reflect2d(Point, Real, Real)", "Hexahedron::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Hexahedron (1 key)
    virtual Hexahedron& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Hexahedron (2 keys)
    virtual Hexahedron& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Hexahedron
    virtual Hexahedron& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Hexahedron::reflect3d(Point, Reals)", "Hexahedron::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Hexahedron
    virtual Hexahedron& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Hexahedron::reflect3d(Point, Real, Real, Real)", "Hexahedron::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
  \class Parallelepiped
  definition of a parallelepiped geometry in R^3

  Parallelepiped constructors are based on a key-value system. Here are the available keys:
  - _v1, _v2, _v4, _v5: to define the vertices of the Parallelepiped
  - _nnodes: to define the number of nodes on each edge of the Parallelepiped
  - _hsteps: to define the local mesh steps on the vertices of the Parallelepiped
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class Parallelepiped : public Hexahedron
{
  protected:
    dimen_t nboctants_;    //!< number of octants to be filled in the 3D space
    std::vector<Point> v_; //!< true list of vertices according to number of octants

  public:
    //! default constructor
    Parallelepiped();
    //@{
    //! key-value constructor
    Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    //@}
    
  protected:
    //@{
    //! true constructor functions
    void buildVNHAndBBox();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! copy constructor
    Parallelepiped(const Parallelepiped& p);
    //! accessor to vertices
    const std::vector<Point>& p() const { return v_; }
    //! accessor to vertex i
    Point p(number_t i) const { return v_[i - 1]; }
    //! returns number of nodes on edges 1, 3, 5 and 7
    number_t n1() const {return n(1);}
    //! returns number of nodes on edges 2, 4, 6 and 8
    number_t n2() const {return n(2);}
    //! retusn number of nodes on edges 9, 10, 11 and 12
    number_t n3() const {return n(9);}

    //! format as string
    virtual string_t asString() const;

    virtual dimen_t nbOctants() const { return nboctants_; } //!< returns number of octants

    virtual Geometry* clone() const { return new Parallelepiped(*this); } //!< virtual copy constructor

    virtual void setFaces();                              //!< set the faces vector when built
    virtual std::vector<const Point*> boundNodes() const; //!< returns list of points on boundary (const)
    virtual std::vector<const Point*> wholeNodes() const; //!< return list of every point (const)
    virtual std::vector<Point*> wholeNodes();             //!< return list of every point (non const)
    //virtual std::vector<std::pair<ShapeType,std::vector<const Point*> > > surfs() const; //!< returns list of faces
    real_t measure() const;

    //! computes the minimal box
    virtual void computeMB();

    //! access to child Parallelepiped object (const)
    virtual const Parallelepiped* parallelepiped() const {return this;}
    //! access to child Parallelepiped object
    virtual Parallelepiped* parallelepiped() {return this;}

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Parallelepiped
    virtual Parallelepiped& transform(const Transformation& t);
    //! apply a translation on a Parallelepiped (1 key)
    virtual Parallelepiped& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Parallelepiped (vector version)
    virtual Parallelepiped& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Parallelepiped::translate(Reals)", "Parallelepiped::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Parallelepiped (3 reals version)
    virtual Parallelepiped& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Parallelepiped::translate(Real, Real, Real)", "Parallelepiped::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Parallelepiped (1 key)
    virtual Parallelepiped& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Parallelepiped (2 keys)
    virtual Parallelepiped& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Parallelepiped
    virtual Parallelepiped& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Parallelepiped::rotate2d(Point, Real)", "Parallelepiped::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Parallelepiped (1 key)
    virtual Parallelepiped& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Parallelepiped (2 keys)
    virtual Parallelepiped& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Parallelepiped (3 keys)
    virtual Parallelepiped& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Parallelepiped
    virtual Parallelepiped& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Parallelepiped::rotate3d(Point, Reals, Real)", "Parallelepiped::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Parallelepiped
    virtual Parallelepiped& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Parallelepiped::rotate3d(Real, Real, Real)", "Parallelepiped::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Parallelepiped
    virtual Parallelepiped& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Parallelepiped::rotate3d(Real, Real, Real, Real)", "Parallelepiped::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Parallelepiped
    virtual Parallelepiped& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Parallelepiped::rotate3d(Point, Real, Real, Real)", "Parallelepiped::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Parallelepiped
    virtual Parallelepiped& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Parallelepiped::rotate3d(Point, Real, Real, Real, Real)", "Parallelepiped::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Parallelepiped (1 key)
    virtual Parallelepiped& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Parallelepiped (2 keys)
    virtual Parallelepiped& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Parallelepiped
    virtual Parallelepiped& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Parallelepiped::homothetize(Point, Real)", "Parallelepiped::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Parallelepiped
    virtual Parallelepiped& homothetize(real_t factor)
    {
      warning("deprecated", "Parallelepiped::homothetize(Real)", "Parallelepiped::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Parallelepiped (1 key)
    virtual Parallelepiped& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Parallelepiped
    virtual Parallelepiped& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Parallelepiped::pointReflect(Point)", "Parallelepiped::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Parallelepiped (1 key)
    virtual Parallelepiped& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Parallelepiped (2 keys)
    virtual Parallelepiped& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Parallelepiped
    virtual Parallelepiped& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Parallelepiped::reflect2d(Point, Reals)", "Parallelepiped::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Parallelepiped
    virtual Parallelepiped& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Parallelepiped::reflect2d(Point, Real, Real)", "Parallelepiped::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Parallelepiped (1 key)
    virtual Parallelepiped& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Parallelepiped (2 keys)
    virtual Parallelepiped& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Parallelepiped
    virtual Parallelepiped& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Parallelepiped::reflect3d(Point, Reals)", "Parallelepiped::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Parallelepiped
    virtual Parallelepiped& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Parallelepiped::reflect3d(Point, Real, Real, Real)", "Parallelepiped::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
  \class Cuboid
  definition of a cuboid (=rectangular parallelepiped) geometry in R^3

  Cuboid constructors are based on a key-value system. Here are the available keys:
  - _v1, _v2, _v4, _v5: to define the vertices of the Cuboid
  - _center: to define the center of the Cuboid
  - _origin: to define the first vertex of the Cuboid (same definition as _v1)
  - _xlength, _ylength, _zlength: the lengths of the Cuboid
  - _xmin, _xmax, _ymin, _ymax, _zmin, _zmax: to define the Cuboid
        v1=(xmin, ymin, zmin), v2=(xmax, ymin, zmin), v4=(xmin, ymax, zmin), v5=(xmin, ymin, zmax)
  - _nnodes: to define the number of nodes on each edge of the Rectangle
  - _hsteps: to define the local mesh steps on the vertices of the Rectangle
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class Cuboid : public Parallelepiped
{
  protected:
    // initialized when rectangle defined with center or origin keys
    Point center_, origin_;
    // true when corresponding key group is given
    bool isCenter_, isOrigin_;
    // initialized when rectangle defined with center or origin keys
    real_t xlength_, ylength_, zlength_;
  private:
    // initialized when rectangle defined with xmin, xmax, ymin, ymax keys
    real_t xmin_, xmax_, ymin_, ymax_, zmin_, zmax_;
    // true when corresponding key group is given
    bool isBounds_;

  public:
    //! default constructor
    Cuboid();
    //@{
    //! key-value constructor
    Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11);
    Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12);
    //@}
    
  protected:
    //@{
    //! true constructor functions
    void buildP();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! copy constructor
    Cuboid(const Cuboid& c);
    //! returns number of nodes on edges 1, 3, 5 and 7
    number_t nx() const {return n(1);}
    //! returns number of nodes on edges 2, 4, 6 and 8
    number_t ny() const {return n(2);}
    //! retusn number of nodes on edges 9, 10, 11 and 12
    number_t nz() const {return n(9);}

    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new Cuboid(*this); } //!< virtual copy constructor

    virtual void setFaces();                                      //!< set the faces vector when built
    //virtual std::vector<std::pair<ShapeType,std::vector<const Point*> > > surfs() const; //!< returns list of faces
    real_t measure() const { return xlength_ * ylength_ * zlength_; }

    //! computes the minimal box
    virtual void computeMB() { minimalBox = MinimalBox(p_[0], p_[1], p_[3], p_[4]); }

    //! access to child Cuboid object (const)
    virtual const Cuboid* cuboid() const {return this;}
    //! access to child Cuboid object
    virtual Cuboid* cuboid() {return this;}

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Cuboid
    virtual Cuboid& transform(const Transformation& t);
    //! apply a translation on a Cuboid (1 key)
    virtual Cuboid& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Cuboid (vector version)
    virtual Cuboid& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Cuboid::translate(Reals)", "Cuboid::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Cuboid (3 reals version)
    virtual Cuboid& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Cuboid::translate(Real, Real, Real)", "Cuboid::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Cuboid (1 key)
    virtual Cuboid& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Cuboid (2 keys)
    virtual Cuboid& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Cuboid
    virtual Cuboid& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Cuboid::rotate2d(Point, Real)", "Cuboid::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Cuboid (1 key)
    virtual Cuboid& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Cuboid (2 keys)
    virtual Cuboid& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Cuboid (3 keys)
    virtual Cuboid& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Cuboid
    virtual Cuboid& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Cuboid::rotate3d(Point, Reals, Real)", "Cuboid::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Cuboid
    virtual Cuboid& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Cuboid::rotate3d(Real, Real, Real)", "Cuboid::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Cuboid
    virtual Cuboid& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Cuboid::rotate3d(Real, Real, Real, Real)", "Cuboid::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Cuboid
    virtual Cuboid& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Cuboid::rotate3d(Point, Real, Real, Real)", "Cuboid::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Cuboid
    virtual Cuboid& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Cuboid::rotate3d(Point, Real, Real, Real, Real)", "Cuboid::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Cuboid (1 key)
    virtual Cuboid& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Cuboid (2 keys)
    virtual Cuboid& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Cuboid
    virtual Cuboid& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Cuboid::homothetize(Point, Real)", "Cuboid::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Cuboid
    virtual Cuboid& homothetize(real_t factor)
    {
      warning("deprecated", "Cuboid::homothetize(Real)", "Cuboid::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Cuboid (1 key)
    virtual Cuboid& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Cuboid
    virtual Cuboid& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Cuboid::pointReflect(Point)", "Cuboid::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Cuboid (1 key)
    virtual Cuboid& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Cuboid (2 keys)
    virtual Cuboid& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Cuboid
    virtual Cuboid& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Cuboid::reflect2d(Point, Reals)", "Cuboid::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Cuboid
    virtual Cuboid& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Cuboid::reflect2d(Point, Real, Real)", "Cuboid::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Cuboid (1 key)
    virtual Cuboid& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Cuboid (2 keys)
    virtual Cuboid& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Cuboid
    virtual Cuboid& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Cuboid::reflect3d(Point, Reals)", "Cuboid::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Cuboid
    virtual Cuboid& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Cuboid::reflect3d(Point, Real, Real, Real)", "Cuboid::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
  \class Cube
  definition of a cubic geometry in R^3

  Cube constructors are based on a key-value system. Here are the available keys:
  - _v1, _v2, _v4, _v5: to define the vertices of the Cube
  - _center: to define the center of the Cube
  - _origin: to define the first vertex of the Cube (same definition as _v1)
  - _length: the length of the Cube
  - _xmin, _xmax, _ymin, _ymax, _zmin, _zmax: to define the Cube
        v1=(xmin, ymin, zmin), v2=(xmax, ymin, zmin), v4=(xmin, ymax, zmin), v5=(xmin, ymin, zmax)
  - _nnodes: to define the number of nodes on each edge of the Cube
  - _hsteps: to define the local mesh steps on the vertices of the Cube
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class Cube : public Cuboid
{
  public:
    //! default constructor
    Cube();
    //@{
    //! key-value constructor
    Cube(Parameter p1, Parameter p2);
    Cube(Parameter p1, Parameter p2, Parameter p3);
    Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11);
    Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12);
    //@}
    
  private:
    //@{
    //! true constructor functions
    void buildP();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! copy constructor
    Cube(const Cube& c);
    //! format as string
    virtual string_t asString() const;

    // access functions to data members
    const Point& center()  const { return center_; } //!< returns center
    real_t edgeLen() const { return xlength_; } //!< returns edge length
    std::vector<std::pair<real_t, dimen_t> > rotations() const //! returns rotations
    { return trihedralOrientation(p_[0], p_[1], p_[3], p_[4]); }

    virtual number_t nbSubdiv() const; //!< returns number of subdivision

    virtual Geometry* clone() const { return new Cube(*this); } //!< virtual copy constructor

    virtual void setFaces();                                    //!< set the faces vector when built
    //virtual std::vector<std::pair<ShapeType,std::vector<const Point*> > > surfs() const; //!< returns list of faces
    real_t measure() const { return xlength_ * ylength_ * zlength_ / 8.*nboctants_; }

    //! access to child Cube object (const)
    virtual const Cube* cube() const {return this;}
    //! access to child Cube object
    virtual Cube* cube() {return this;}

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Cube
    virtual Cube& transform(const Transformation& t);
    //! apply a translation on a Cube (1 key)
    virtual Cube& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Cube (vector version)
    virtual Cube& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Cube::translate(Reals)", "Cube::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Cube (3 reals version)
    virtual Cube& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Cube::translate(Real, Real, Real)", "Cube::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Cube (1 key)
    virtual Cube& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Cube (2 keys)
    virtual Cube& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Cube
    virtual Cube& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Cube::rotate2d(Point, Real)", "Cube::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Cube (1 key)
    virtual Cube& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Cube (2 keys)
    virtual Cube& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Cube (3 keys)
    virtual Cube& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Cube
    virtual Cube& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Cube::rotate3d(Point, Reals, Real)", "Cube::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Cube
    virtual Cube& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Cube::rotate3d(Real, Real, Real)", "Cube::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Cube
    virtual Cube& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Cube::rotate3d(Real, Real, Real, Real)", "Cube::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Cube
    virtual Cube& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Cube::rotate3d(Point, Real, Real, Real)", "Cube::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Cube
    virtual Cube& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Cube::rotate3d(Point, Real, Real, Real, Real)", "Cube::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Cube (1 key)
    virtual Cube& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Cube (2 keys)
    virtual Cube& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Cube
    virtual Cube& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Cube::homothetize(Point, Real)", "Cube::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Cube
    virtual Cube& homothetize(real_t factor)
    {
      warning("deprecated", "Cube::homothetize(Real)", "Cube::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Cube (1 key)
    virtual Cube& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Cube
    virtual Cube& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Cube::pointReflect(Point)", "Cube::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Cube (1 key)
    virtual Cube& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Cube (2 keys)
    virtual Cube& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Cube
    virtual Cube& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Cube::reflect2d(Point, Reals)", "Cube::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Cube
    virtual Cube& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Cube::reflect2d(Point, Real, Real)", "Cube::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Cube (1 key)
    virtual Cube& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Cube (2 keys)
    virtual Cube& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Cube
    virtual Cube& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Cube::reflect3d(Point, Reals)", "Cube::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Cube
    virtual Cube& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Cube::reflect3d(Point, Real, Real, Real)", "Cube::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
  \class Ellipsoid
  definition of an ellipsoidal geometry in R^3 (volume)

  Ellipsoid constructors are based on a key-value system. Here are the available keys:
  - _center: to define the center of the Ellipsoid
  - _v1, _v2, _v6: to define apogees of the Ellipsoid
  - _xlength, _ylength, _zlength: to define axis lengths of the Ellipsoid
  - _xradius, _yradius, _zradius: to define semi-axis lengths of the Ellipsoid
  - _type: indicator to fit curved boundaries (default) or not which gives flat (or plane) boundaries
  - _nboctants: to define an ellipsoidal sector from octants.
  - _nnodes: to define the number of nodes on the edges of the Ellipsoid
  - _hsteps: to define the local mesh steps on build points of the Ellipsoid
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class Ellipsoid : public Volume
{
  protected:
    /*!
      vertices of the ellipsoid
      - p_[0] is the center of the ellipsoid
      - p_[1] first apogee of the ellipsoid
      - p_[2] second apogee of the ellipsoid
      - p_[3] first perigee of the ellipsoid
      - p_[4] second perigee of the ellipsoid
      - p_[5] third perigee of the ellipsoid
      - p_[6] third apogee of the ellipsoid
    */
    std::vector<Point> v_; //!< true list of vertices according to number of octants
    real_t xradius_, yradius_, zradius_;
    bool isAxis_;
    dimen_t nboctants_; //!< number of octants to be filled in the 3D space
    dimen_t type_;     //!< indicator to fit curved boundaries (default) or not which gives flat (or plane) boundaries

  public:
    //! default constructor
    Ellipsoid();
    //@{
    //! key-value constructor
    Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11);
    Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12);
    //@}
    
  protected:
    //@{
    //! true constructor functions
    void buildPVNHAndBBox();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! copy constructor
    Ellipsoid(const Ellipsoid& e);

    //! accessor to point
    const Point& center() const { return p_[0];}
    //! accessor to true vertices
    const std::vector<Point>& v() const { return v_; }
    //! accessor to vertex i
    const Point& v(number_t i) const { return v_[i - 1]; }

    real_t length1() const { return 2.*xradius_; } //!< return first axis length
    real_t length2() const { return 2.*yradius_; } //!< return second axis length
    real_t length3() const { return 2.*zradius_; } //!< return third axis length
    real_t radius1() const { return xradius_; } //!< return first semi-axis length
    real_t radius2() const { return yradius_; } //!< return second semi-axis length
    real_t radius3() const { return zradius_; } //!< return third semi-axis length
    std::vector<std::pair<real_t, dimen_t> > rotations() const //! returns rotations
    { return trihedralOrientation(p_[0], p_[1], p_[2], p_[6]); }
    virtual dimen_t nbOctants() const { return nboctants_; } //!< returns number of octants
    virtual number_t nbSubdiv() const; //!< returns number of subdivisions
    virtual dimen_t type() const { return type_; } //!< return type of subdivision
    bool isBall() const; //! true if a ball
    bool isFull() const; //! true if a full ellipsoid

    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new Ellipsoid(*this); } //!< virtual copy constructor
    bool withNnodes() const { return (h_.size() == 0); } //!< check if Ellipsoid is defined only with _nnodes or with _hsteps option

    virtual std::vector<Point*> nodes();                  //!< returns list of every main point (non const)
    virtual std::vector<Point*> wholeNodes();             //!< return list of every point (non const)
    virtual std::vector<const Point*> nodes() const;      //!< returns list of every point (const)
    virtual std::vector<const Point*> wholeNodes() const; //!< returns list of every point (const)
    virtual std::vector<const Point*> boundNodes() const; //!< returns list of points on boundary (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< returns list of edges
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of faces
    virtual number_t nbSides() const { return 8; }        //!< returns the number of sides
    real_t measure() const;

    //! computes the minimal box
    virtual void computeMB();

    //! access to child Ellipsoid object (const)
    virtual const Ellipsoid* ellipsoid() const {return this;}
    //! access to child Ellipsoid object
    virtual Ellipsoid* ellipsoid() {return this;}

    Geometry& buildBoundary() const;                            //!< create boundary Geometry

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Ellipsoid
    virtual Ellipsoid& transform(const Transformation& t);
    //! apply a translation on a Ellipsoid (1 key)
    virtual Ellipsoid& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Ellipsoid (vector version)
    virtual Ellipsoid& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Ellipsoid::translate(Reals)", "Ellipsoid::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Ellipsoid (3 reals version)
    virtual Ellipsoid& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Ellipsoid::translate(Real, Real, Real)", "Ellipsoid::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Ellipsoid (1 key)
    virtual Ellipsoid& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Ellipsoid (2 keys)
    virtual Ellipsoid& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Ellipsoid
    virtual Ellipsoid& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Ellipsoid::rotate2d(Point, Real)", "Ellipsoid::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Ellipsoid (1 key)
    virtual Ellipsoid& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Ellipsoid (2 keys)
    virtual Ellipsoid& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Ellipsoid (3 keys)
    virtual Ellipsoid& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Ellipsoid
    virtual Ellipsoid& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Ellipsoid::rotate3d(Point, Reals, Real)", "Ellipsoid::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Ellipsoid
    virtual Ellipsoid& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Ellipsoid::rotate3d(Real, Real, Real)", "Ellipsoid::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Ellipsoid
    virtual Ellipsoid& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Ellipsoid::rotate3d(Real, Real, Real, Real)", "Ellipsoid::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Ellipsoid
    virtual Ellipsoid& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Ellipsoid::rotate3d(Point, Real, Real, Real)", "Ellipsoid::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Ellipsoid
    virtual Ellipsoid& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Ellipsoid::rotate3d(Point, Real, Real, Real, Real)", "Ellipsoid::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Ellipsoid (1 key)
    virtual Ellipsoid& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Ellipsoid (2 keys)
    virtual Ellipsoid& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Ellipsoid
    virtual Ellipsoid& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Ellipsoid::homothetize(Point, Real)", "Ellipsoid::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Ellipsoid
    virtual Ellipsoid& homothetize(real_t factor)
    {
      warning("deprecated", "Ellipsoid::homothetize(Real)", "Ellipsoid::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Ellipsoid (1 key)
    virtual Ellipsoid& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Ellipsoid
    virtual Ellipsoid& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Ellipsoid::pointReflect(Point)", "Ellipsoid::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Ellipsoid (1 key)
    virtual Ellipsoid& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Ellipsoid (2 keys)
    virtual Ellipsoid& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Ellipsoid
    virtual Ellipsoid& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Ellipsoid::reflect2d(Point, Reals)", "Ellipsoid::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Ellipsoid
    virtual Ellipsoid& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Ellipsoid::reflect2d(Point, Real, Real)", "Ellipsoid::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Ellipsoid (1 key)
    virtual Ellipsoid& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Ellipsoid (2 keys)
    virtual Ellipsoid& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Ellipsoid
    virtual Ellipsoid& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Ellipsoid::reflect3d(Point, Reals)", "Ellipsoid::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Ellipsoid
    virtual Ellipsoid& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Ellipsoid::reflect3d(Point, Real, Real, Real)", "Ellipsoid::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
   \class Ball
  definition of an spherical geometry in R^3 (volume)

  Ball constructors are based on a key-value system. Here are the available keys:
  - _center: to define the center of the Ball
  - _v1, _v2, _v6: to define apogees of the Ball
  - _radius: to define radius of the Ball
  - _type: indicator to fit curved boundaries (default) or not which gives flat (or plane) boundaries
  - _nboctants: to define an ellipsoidal sector from octants.
  - _nnodes: to define the number of nodes on the edges of the Ball
  - _hsteps: to define the local mesh steps on build points of the Ball
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class Ball : public Ellipsoid
{
  public:
    //! default constructor
    Ball();
    //@{
    //! key-value constructor
    Ball(Parameter p1, Parameter p2);
    Ball(Parameter p1, Parameter p2, Parameter p3);
    Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11);
    Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12);
    //@}
    
  protected:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! copy constructor
    Ball(const Ball& b);
    //! format as string: "Ball (center=(.,.,.), radius = R)"
    virtual string_t asString() const;

    // access functions to data members
    real_t radius() const { return xradius_; }  //!< returns radius

    virtual Geometry* clone() const { return new Ball(*this); } //!< virtual copy constructor

    //! access to child Ball object (const)
    virtual const Ball* ball() const {return this;}
    //! access to child Ball object
    virtual Ball* ball() {return this;}

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Ball
    virtual Ball& transform(const Transformation& t);
    //! apply a translation on a Ball (1 key)
    virtual Ball& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Ball (vector version)
    virtual Ball& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Ball::translate(Reals)", "Ball::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Ball (3 reals version)
    virtual Ball& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Ball::translate(Real, Real, Real)", "Ball::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Ball (1 key)
    virtual Ball& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Ball (2 keys)
    virtual Ball& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Ball
    virtual Ball& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Ball::rotate2d(Point, Real)", "Ball::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Ball (1 key)
    virtual Ball& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Ball (2 keys)
    virtual Ball& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Ball (3 keys)
    virtual Ball& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Ball
    virtual Ball& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Ball::rotate3d(Point, Reals, Real)", "Ball::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Ball
    virtual Ball& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Ball::rotate3d(Real, Real, Real)", "Ball::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Ball
    virtual Ball& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Ball::rotate3d(Real, Real, Real, Real)", "Ball::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Ball
    virtual Ball& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Ball::rotate3d(Point, Real, Real, Real)", "Ball::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Ball
    virtual Ball& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Ball::rotate3d(Point, Real, Real, Real, Real)", "Ball::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Ball (1 key)
    virtual Ball& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Ball (2 keys)
    virtual Ball& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Ball
    virtual Ball& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Ball::homothetize(Point, Real)", "Ball::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Ball
    virtual Ball& homothetize(real_t factor)
    {
      warning("deprecated", "Ball::homothetize(Real)", "Ball::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Ball (1 key)
    virtual Ball& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Ball
    virtual Ball& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Ball::pointReflect(Point)", "Ball::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Ball (1 key)
    virtual Ball& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Ball (2 keys)
    virtual Ball& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Ball
    virtual Ball& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Ball::reflect2d(Point, Reals)", "Ball::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Ball
    virtual Ball& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Ball::reflect2d(Point, Real, Real)", "Ball::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Ball (1 key)
    virtual Ball& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Ball (2 keys)
    virtual Ball& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Ball
    virtual Ball& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Ball::reflect3d(Point, Reals)", "Ball::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Ball
    virtual Ball& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Ball::reflect3d(Point, Real, Real, Real)", "Ball::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
    //! parametrization stuff
    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType dif) const;
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType dif) const;
    Vector<real_t> funBoundaryParametrization(const Point& pt, Parameters& pars, DiffOpType dif) const;
    Vector<real_t> invBoundaryParametrization(const Point& pt, Parameters& pars, DiffOpType dif) const;
};

typedef Ball Sphere;

/*!
  \class Trunk
  A Trunk is a volume defined by a cylinder or a trunk of cone. A cylinder is a trunk of a cylinder !!!
  To define it, we need a basis, a translation and a homothety to build the other basis.
  To make it more simple, we will need a basis, the first point of the other basis (determining the translation
  and the center of the homothety), and a scale factor. This "first point" of a basis is defined as basis.p(1) : it is
  the first vertex for a polygon, but the center for an ellipse or a disk

  Trunk constructors are based on a key-value system. Here are the available keys:
  - _basis: to define the geometrical basis of a Trunk (a child object of Surface)
  - _origin: the origin point of the second basis (the first vertex of a Polygon, but the center of an Ellipse or a Disk)
  - _scale: the scale factor to define the second basis
  - _center1, _center2: to define centers of bases when they are elliptical
  - _v1, _v2: to define apogees of the basis when it is elliptical
  - _nnodes: to define the number of nodes on the edges of the Trunk
  - _hsteps: to define the local mesh steps on build points of the Trunk
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class Trunk : public Volume
{
  protected:
    //! first surfacic basis
    Surface* basis_;
    //! top geometry, built on demand
    mutable Surface* top_;
    //! scale factor
    real_t scale_;
    Point origin_, center1_, p1_, p2_;
    bool isElliptical_, isN_;

  public:
    //! default constructor
    Trunk(real_t scale = 1, bool defineBasisAndP = true);
    //@{
    //! key-value constructor
    Trunk(Parameter p1, Parameter p2, Parameter p3);
    Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11);
    //@}
    
  protected:
    //@{
    //! true constructor functions
    void buildPAndBasis();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! copy constructor
    Trunk(const Trunk& t);
    //! destructor
    virtual ~Trunk() { if (basis_ != nullptr) { delete basis_; if (top_ != nullptr) delete top_;} }
    Trunk& operator=(const Trunk& tr);

    //! accessor to basis of trunk
    Surface* basis() const { return basis_; }
    //! accessor to scale factor of trunk
    real_t scale() const { return scale_; }
    //! accessor to origin_ of trunk
    const Point& origin() const { return origin_; }
    //! accessor to origin_ of trunk
    const Point& center1() const { return center1_; }
    //! accessor to origin_ of trunk
    const Point& center2() const { return origin_; }
    //! accessor to origin_ of trunk
    const Point& p1() const { return p1_; }
    //! accessor to origin_ of trunk
    const Point& p2() const { return p2_; }
    bool isElliptical() const {return isElliptical_;}

    //! format as string
    virtual string_t asString() const;
    //! virtual copy constructor
    virtual Geometry* clone() const { return new Trunk(*this); }
    //! check if Trunk is defined only with _nnodes or with _hsteps option
    bool withNnodes() const { return (h_.size() == 0); }

    virtual std::vector<const Point*> boundNodes() const; //!< returns list of points on boundary (const)
    virtual std::vector<Point*> nodes(); //!< return list of every point (non const)
    virtual std::vector<const Point*> nodes() const; //!< returns list of every point (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< returns list of curves (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of faces (const)
    virtual number_t nbSides() const { return basis_->nbSides() + 2; } //!< returns the number of sides
    real_t measure() const;

    virtual void computeMB() { minimalBox = MinimalBox(boundingBox.bounds()); }

    virtual const Trunk* trunk() const //! access to child Trunk object (const)
    { return this; }
    virtual Trunk* trunk() //! access to child Trunk object
    { return this; }


    //! create top geometry
    void buildTop() const;
    //! access to top geometry, created if not exist
    Surface* top() const;
    //! create boundary Geometry
    virtual Geometry& buildBoundary() const;

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Trunk
    virtual Trunk& transform(const Transformation& t);
    //! apply a translation on a Trunk (1 key)
    virtual Trunk& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Trunk (vector version)
    virtual Trunk& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Trunk::translate(Reals)", "Trunk::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Trunk (3 reals version)
    virtual Trunk& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Trunk::translate(Real, Real, Real)", "Trunk::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Trunk (1 key)
    virtual Trunk& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Trunk (2 keys)
    virtual Trunk& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Trunk
    virtual Trunk& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Trunk::rotate2d(Point, Real)", "Trunk::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Trunk (1 key)
    virtual Trunk& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Trunk (2 keys)
    virtual Trunk& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Trunk (3 keys)
    virtual Trunk& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Trunk
    virtual Trunk& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Trunk::rotate3d(Point, Reals, Real)", "Trunk::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Trunk
    virtual Trunk& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Trunk::rotate3d(Real, Real, Real)", "Trunk::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Trunk
    virtual Trunk& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Trunk::rotate3d(Real, Real, Real, Real)", "Trunk::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Trunk
    virtual Trunk& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Trunk::rotate3d(Point, Real, Real, Real)", "Trunk::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Trunk
    virtual Trunk& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Trunk::rotate3d(Point, Real, Real, Real, Real)", "Trunk::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Trunk (1 key)
    virtual Trunk& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Trunk (2 keys)
    virtual Trunk& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Trunk
    virtual Trunk& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Trunk::homothetize(Point, Real)", "Trunk::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Trunk
    virtual Trunk& homothetize(real_t factor)
    {
      warning("deprecated", "Trunk::homothetize(Real)", "Trunk::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Trunk (1 key)
    virtual Trunk& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Trunk
    virtual Trunk& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Trunk::pointReflect(Point)", "Trunk::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Trunk (1 key)
    virtual Trunk& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Trunk (2 keys)
    virtual Trunk& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Trunk
    virtual Trunk& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Trunk::reflect2d(Point, Reals)", "Trunk::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Trunk
    virtual Trunk& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Trunk::reflect2d(Point, Real, Real)", "Trunk::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Trunk (1 key)
    virtual Trunk& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Trunk (2 keys)
    virtual Trunk& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Trunk
    virtual Trunk& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Trunk::reflect3d(Point, Reals)", "Trunk::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Trunk
    virtual Trunk& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Trunk::reflect3d(Point, Real, Real, Real)", "Trunk::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
   \class Cylinder
   A cylinder is a volume defined by a section (the basis) and a direction vector
   The direction vector is not necessarily orthogonal to the basis, but both bases are necessarily parallel
   A Cylinder is a Trunk with scale factor equal to 1 !!!

  Cylinder constructors are based on a key-value system. Here are the available keys:
  - _basis: to define the geometrical basis of a Cylinder (a child object of Surface)
  - _direction: a direction vector of the axis of the Cylinder
  - _center1, _center2: to define centers of bases when they are elliptical
  - _v1, _v2: to define apogees of the basis when it is elliptical
  - _nnodes: to define the number of nodes on the edges of the Cylinder
  - _hsteps: to define the local mesh steps on build points of the Cylinder
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class Cylinder : public Trunk
{
  protected:
    //! direction vector
    std::vector<real_t> dir_;

  public:
    //! default constructor
    Cylinder(bool defineBasisAndP = true);
    //! default constructor with basis and direction
    Cylinder(const Surface& basis, const std::vector<real_t>& direction);
    //@{
    //! key-value constructor
    Cylinder(Parameter p1, Parameter p2);
    Cylinder(Parameter p1, Parameter p2, Parameter p3);
    Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    //@}
    
  protected:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! copy constructor
    Cylinder(const Cylinder& c);

    //! accessor to direction vector
    std::vector<real_t> dir() const { return dir_; }

    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new Cylinder(*this); } //!< virtual copy constructor

    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of faces (const)

    virtual void computeMB()
    {
      minimalBox = MinimalBox(basis_->minimalBox.boundPt(1),
                              basis_->minimalBox.boundPt(2),
                              basis_->minimalBox.boundPt(3),
                              basis_->minimalBox.boundPt(1) + dir_);
    }

    //! access to child Cylinder object (const)
    virtual const Cylinder* cylinder() const {return this;}
    //! access to child Cylinder2d object
    virtual Cylinder* cylinder() {return this;}

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Cylinder
    virtual Cylinder& transform(const Transformation& t);
    //! apply a translation on a Cylinder (1 key)
    virtual Cylinder& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Cylinder (vector version)
    virtual Cylinder& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Cylinder::translate(Reals)", "Cylinder::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Cylinder (3 reals version)
    virtual Cylinder& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Cylinder::translate(Real, Real, Real)", "Cylinder::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Cylinder (1 key)
    virtual Cylinder& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Cylinder (2 keys)
    virtual Cylinder& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Cylinder
    virtual Cylinder& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Cylinder::rotate2d(Point, Real)", "Cylinder::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Cylinder (1 key)
    virtual Cylinder& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Cylinder (2 keys)
    virtual Cylinder& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Cylinder (3 keys)
    virtual Cylinder& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Cylinder
    virtual Cylinder& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Cylinder::rotate3d(Point, Reals, Real)", "Cylinder::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Cylinder
    virtual Cylinder& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Cylinder::rotate3d(Real, Real, Real)", "Cylinder::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Cylinder
    virtual Cylinder& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Cylinder::rotate3d(Real, Real, Real, Real)", "Cylinder::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Cylinder
    virtual Cylinder& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Cylinder::rotate3d(Point, Real, Real, Real)", "Cylinder::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Cylinder
    virtual Cylinder& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Cylinder::rotate3d(Point, Real, Real, Real, Real)", "Cylinder::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Cylinder (1 key)
    virtual Cylinder& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Cylinder (2 keys)
    virtual Cylinder& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Cylinder
    virtual Cylinder& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Cylinder::homothetize(Point, Real)", "Cylinder::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Cylinder
    virtual Cylinder& homothetize(real_t factor)
    {
      warning("deprecated", "Cylinder::homothetize(Real)", "Cylinder::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Cylinder (1 key)
    virtual Cylinder& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Cylinder
    virtual Cylinder& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Cylinder::pointReflect(Point)", "Cylinder::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Cylinder (1 key)
    virtual Cylinder& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Cylinder (2 keys)
    virtual Cylinder& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Cylinder
    virtual Cylinder& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Cylinder::reflect2d(Point, Reals)", "Cylinder::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Cylinder
    virtual Cylinder& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Cylinder::reflect2d(Point, Real, Real)", "Cylinder::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Cylinder (1 key)
    virtual Cylinder& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Cylinder (2 keys)
    virtual Cylinder& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Cylinder
    virtual Cylinder& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Cylinder::reflect3d(Point, Reals)", "Cylinder::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Cylinder
    virtual Cylinder& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Cylinder::reflect3d(Point, Real, Real, Real)", "Cylinder::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
  \class Prism
  definition of a prismatic geometry in R^3

  Prism constructors are based on a key-value system. Here are the available keys:
  - _basis: to define the geometrical basis of a Prism (a child object of Polygon)
  - _direction: a direction vector of the axis of the Cylinder
  - _v1, _v2, _v3: to define vertices of the basis when it is triangular
  - _nnodes: to define the number of nodes on the edges of the Prism
  - _hsteps: to define the local mesh steps on build points of the Prism
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class Prism : public Cylinder
{
  private:
    bool isTriangular_;
    Point p3_;

  public:
    //! default constructor
    Prism();
    //@{
    //! key-value constructor
    Prism(Parameter p1, Parameter p2);
    Prism(Parameter p1, Parameter p2, Parameter p3);
    Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    //@}
    
  protected:
    //@{
    //! true constructor functions
    void buildPBasisNAndH();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! copy constructor
    Prism(const Prism& p);
    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new Prism(*this); } //!< virtual copy constructor

    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of faces (const)
    virtual void collect(const string_t& n, std::list<Geometry*>&) const; //!< collect all canonical geometry's with name n

    //! access to child Prism object (const)
    virtual const Prism* prism() const {return this;}
    //! access to child Prism object
    virtual Prism* prism() {return this;}

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Prism
    virtual Prism& transform(const Transformation& t);
    //! apply a translation on a Prism (1 key)
    virtual Prism& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Prism (vector version)
    virtual Prism& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Prism::translate(Reals)", "Prism::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Prism (3 reals version)
    virtual Prism& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Prism::translate(Real, Real, Real)", "Prism::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Prism (1 key)
    virtual Prism& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Prism (2 keys)
    virtual Prism& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Prism
    virtual Prism& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Prism::rotate2d(Point, Real)", "Prism::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Prism (1 key)
    virtual Prism& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Prism (2 keys)
    virtual Prism& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Prism (3 keys)
    virtual Prism& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Prism
    virtual Prism& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Prism::rotate3d(Point, Reals, Real)", "Prism::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Prism
    virtual Prism& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Prism::rotate3d(Real, Real, Real)", "Prism::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Prism
    virtual Prism& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Prism::rotate3d(Real, Real, Real, Real)", "Prism::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Prism
    virtual Prism& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Prism::rotate3d(Point, Real, Real, Real)", "Prism::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Prism
    virtual Prism& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Prism::rotate3d(Point, Real, Real, Real, Real)", "Prism::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Prism (1 key)
    virtual Prism& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Prism (2 keys)
    virtual Prism& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Prism
    virtual Prism& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Prism::homothetize(Point, Real)", "Prism::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Prism
    virtual Prism& homothetize(real_t factor)
    {
      warning("deprecated", "Prism::homothetize(Real)", "Prism::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Prism (1 key)
    virtual Prism& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Prism
    virtual Prism& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Prism::pointReflect(Point)", "Prism::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Prism (1 key)
    virtual Prism& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Prism (2 keys)
    virtual Prism& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Prism
    virtual Prism& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Prism::reflect2d(Point, Reals)", "Prism::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Prism
    virtual Prism& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Prism::reflect2d(Point, Real, Real)", "Prism::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Prism (1 key)
    virtual Prism& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Prism (2 keys)
    virtual Prism& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Prism
    virtual Prism& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Prism::reflect3d(Point, Reals)", "Prism::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Prism
    virtual Prism& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Prism::reflect3d(Point, Real, Real, Real)", "Prism::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
   \class Cone
   A cone is a volume defined by a section (the basis) and a direction vector
   The direction vector is not necessarily orthogonal to the basis, but both bases are necessarily parallel
   A Cone is a Trunk with scale factor equal to 0 !!!

  Cone constructors are based on a key-value system. Here are the available keys:
  - _basis: to define the geometrical basis of a Cone (a child object of Surface)
  - _apex: the apex of the Cone
  - _center1: to define the center of the basis when they are elliptical
  - _v1, _v2: to define apogees of the basis when it is elliptical
  - _nnodes: to define the number of nodes on the edges of the Cone
  - _hsteps: to define the local mesh steps on build points of the Cone
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class Cone : public Trunk
{
  public:
    //! default constructor
    Cone(bool defineBasisAndP = true);
    //! default constructor with basis and apex
    Cone(const Surface& basis, const Point& apex);
    //@{
    //! key-value constructor
    Cone(Parameter p1, Parameter p2);
    Cone(Parameter p1, Parameter p2, Parameter p3);
    Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    //@}
    
  protected:
    //@{
    //! true constructor functions
    void buildPBasisNAndH();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! copy constructor
    Cone(const Cone& c);

    //! accessor to apex_
    Point apex() const { return origin_; }

    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new Cone(*this); } //!< virtual copy constructor

    virtual std::vector<const Point*> boundNodes() const; //!< returns list of points on boundary (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< returns list of curves (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of faces (const)

    virtual void computeMB()
    {
      minimalBox = MinimalBox(basis_->minimalBox.boundPt(1),
                              basis_->minimalBox.boundPt(2),
                              basis_->minimalBox.boundPt(3),
                              origin_);
    }

    //! access to child Cone object (const)
    virtual const Cone* cone() const {return this;}
    //! access to child Cone2d object
    virtual Cone* cone() {return this;}

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Cone
    virtual Cone& transform(const Transformation& t);
    //! apply a translation on a Cone (1 key)
    virtual Cone& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Cone (vector version)
    virtual Cone& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Cone::translate(Reals)", "Cone::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Cone (3 reals version)
    virtual Cone& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Cone::translate(Real, Real, Real)", "Cone::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Cone (1 key)
    virtual Cone& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Cone (2 keys)
    virtual Cone& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Cone
    virtual Cone& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Cone::rotate2d(Point, Real)", "Cone::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Cone (1 key)
    virtual Cone& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Cone (2 keys)
    virtual Cone& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Cone (3 keys)
    virtual Cone& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Cone
    virtual Cone& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Cone::rotate3d(Point, Reals, Real)", "Cone::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Cone
    virtual Cone& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Cone::rotate3d(Real, Real, Real)", "Cone::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Cone
    virtual Cone& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Cone::rotate3d(Real, Real, Real, Real)", "Cone::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Cone
    virtual Cone& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Cone::rotate3d(Point, Real, Real, Real)", "Cone::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Cone
    virtual Cone& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Cone::rotate3d(Point, Real, Real, Real, Real)", "Cone::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Cone (1 key)
    virtual Cone& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Cone (2 keys)
    virtual Cone& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Cone
    virtual Cone& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Cone::homothetize(Point, Real)", "Cone::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Cone
    virtual Cone& homothetize(real_t factor)
    {
      warning("deprecated", "Cone::homothetize(Real)", "Cone::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Cone (1 key)
    virtual Cone& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Cone
    virtual Cone& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Cone::pointReflect(Point)", "Cone::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Cone (1 key)
    virtual Cone& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Cone (2 keys)
    virtual Cone& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Cone
    virtual Cone& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Cone::reflect2d(Point, Reals)", "Cone::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Cone
    virtual Cone& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Cone::reflect2d(Point, Real, Real)", "Cone::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Cone (1 key)
    virtual Cone& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Cone (2 keys)
    virtual Cone& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Cone
    virtual Cone& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Cone::reflect3d(Point, Reals)", "Cone::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Cone
    virtual Cone& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Cone::reflect3d(Point, Real, Real, Real)", "Cone::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
  \class Pyramid
  definition of a pyramidal geometry in R^3

  Pyramid constructors are based on a key-value system. Here are the available keys:
  - _basis: to define the geometrical basis of a Pyramid (a child object of Polygon)
  - _apex: the apex of the Pyramid
  - _v1, _v2, _v3, _v4: to define vertices of the basis when it is quadrangular
  - _nnodes: to define the number of nodes on the edges of the Pyramid
  - _hsteps: to define the local mesh steps on build points of the Pyramid
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class Pyramid : public Cone
{
  private:
    bool isQuadrangular_;
    Point p3_, p4_;

  public:
    //! default constructor
    Pyramid();
    //@{
    //! key-value constructor
    Pyramid(Parameter p1, Parameter p2);
    Pyramid(Parameter p1, Parameter p2, Parameter p3);
    Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11);
    //@}
    
  protected:
    //@{
    //! true constructor functions
    void buildPBasisNAndH();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! copy constructor
    Pyramid(const Pyramid& p);

    //@{
    //! accessor to point
    Point p1() const { return p_[0]; }
    Point p2() const { return p_[1]; }
    Point p3() const { return p_[2]; }
    Point p4() const { return p_[3]; }
    Point p5() const { return p_[4]; }
    //@}

    //@{
    //! accessor to number of element on edge
    number_t n1() const {return n_[0];}
    number_t n2() const {return n_[1];}
    number_t n3() const {return n_[2];}
    number_t n4() const {return n_[3];}
    number_t n5() const {return n_[4];}
    number_t n6() const {return n_[5];}
    number_t n7() const {return n_[6];}
    number_t n8() const {return n_[7];}
    //@}

    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new Pyramid(*this); } //!< virtual copy constructor

    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of faces (const)
    virtual void collect(const string_t& n, std::list<Geometry*>&) const; //!< collect in a all canonical geometry's with name n

    //! access to child Pyramid object (const)
    virtual const Pyramid* pyramid() const {return this;}
    //! access to child Pyramid object
    virtual Pyramid* pyramid() {return this;}

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Pyramid
    virtual Pyramid& transform(const Transformation& t);
    //! apply a translation on a Pyramid (1 key)
    virtual Pyramid& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Pyramid (vector version)
    virtual Pyramid& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Pyramid::translate(Reals)", "Pyramid::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Pyramid (3 reals version)
    virtual Pyramid& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Pyramid::translate(Real, Real, Real)", "Pyramid::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Pyramid (1 key)
    virtual Pyramid& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Pyramid (2 keys)
    virtual Pyramid& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Pyramid
    virtual Pyramid& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Pyramid::rotate2d(Point, Real)", "Pyramid::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Pyramid (1 key)
    virtual Pyramid& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Pyramid (2 keys)
    virtual Pyramid& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Pyramid (3 keys)
    virtual Pyramid& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Pyramid
    virtual Pyramid& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Pyramid::rotate3d(Point, Reals, Real)", "Pyramid::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Pyramid
    virtual Pyramid& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Pyramid::rotate3d(Real, Real, Real)", "Pyramid::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Pyramid
    virtual Pyramid& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Pyramid::rotate3d(Real, Real, Real, Real)", "Pyramid::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Pyramid
    virtual Pyramid& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Pyramid::rotate3d(Point, Real, Real, Real)", "Pyramid::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Pyramid
    virtual Pyramid& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Pyramid::rotate3d(Point, Real, Real, Real, Real)", "Pyramid::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Pyramid (1 key)
    virtual Pyramid& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Pyramid (2 keys)
    virtual Pyramid& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Pyramid
    virtual Pyramid& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Pyramid::homothetize(Point, Real)", "Pyramid::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Pyramid
    virtual Pyramid& homothetize(real_t factor)
    {
      warning("deprecated", "Pyramid::homothetize(Real)", "Pyramid::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Pyramid (1 key)
    virtual Pyramid& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Pyramid
    virtual Pyramid& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Pyramid::pointReflect(Point)", "Pyramid::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Pyramid (1 key)
    virtual Pyramid& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Pyramid (2 keys)
    virtual Pyramid& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Pyramid
    virtual Pyramid& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Pyramid::reflect2d(Point, Reals)", "Pyramid::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Pyramid
    virtual Pyramid& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Pyramid::reflect2d(Point, Real, Real)", "Pyramid::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Pyramid (1 key)
    virtual Pyramid& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Pyramid (2 keys)
    virtual Pyramid& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Pyramid
    virtual Pyramid& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Pyramid::reflect3d(Point, Reals)", "Pyramid::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Pyramid
    virtual Pyramid& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Pyramid::reflect3d(Point, Real, Real, Real)", "Pyramid::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
   \class RevTrunk
   A object of revolution is defined by its axis (P1,P2) and the radii of two circles
   obtained by intersection with a plane orthogonal to (P1,P2). The object is delimited
   by the two planes, orthogonal to (P1,P2), passing by P1 and P2, and this is the default
   end shape of the object when it is meshed with volumic elements (gesFlat or equivalently
   in this case, gesNone).
   When the elements are surfacic, the default is to leave the ends empty (gesNone).

   Moreover, on both ends of this object, one can add part of a cone (gesCone), an ellipsoid
   (gesEllipsoid) or a sphere (gesSphere), connected to the boundary circle of the object.
   Let us consider the boundary circle whose center is P1. The apex of the cone or the ellipsoid
   is assumed to lie on the line (P1,P2) at a given distance distance1_ from P1. This distance
   is irrelevant in the case of the sphere. The same apply on the other end of the object.
   When the elements are surfacic, one can additonnaly chose a flat "lid" (gesFlat) ; the
   distance argument is then also irrelevant in this case.

  RevTrunk constructors are based on a key-value system. Here are the available keys:
  - _center1, _center2: to define centers of bases when they are elliptical
  - _radius1, _radius2: the radii of the bases
  - _end1_shape, _end2_shape: the shape of extension parts on both ends of the RevTrunk
  - _end1_distance, _end2_distance: the lengths of extension parts on both ends of the RevTrunk
  - _nbsubdomains: the number of slices of the RevTrunk (subdivision mesg generator only)
  - _type: indicator to fit curved boundaries (default) or not which gives flat (or plane) boundaries
  - _nnodes: to define the number of nodes on the edges of the RevTrunk
  - _hsteps: to define the local mesh steps on build points of the RevTrunk
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class RevTrunk : public Trunk
{
  protected:
    real_t radius1_, radius2_;
    number_t nbSubDomains_;       //!< number of main subDomains
    GeometricEndShape endShape1_; //!< shape of the first end
    real_t distance1_;            //!< height of the first end
    GeometricEndShape endShape2_; //!< shape of the second end
    real_t distance2_;            //!< height of the second end
    dimen_t type_;                //!< type of the subdivision

  public:
    //! default constructor
    RevTrunk(real_t scale = 1., bool defineBasisAndP = true);
    //@{
    //! key-value constructor
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11);
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12);
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13);
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14);
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14, Parameter p15);
    RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14, Parameter p15, Parameter p16);
    //@}
    
  protected:
    //@{
    //! true constructor functions
    void buildP();
    void buildPScaleAndBasis();
    void checkNH();
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! copy constructor
    RevTrunk(const RevTrunk& r);
    //! format as string: "RevTrunk (P1=(.,.,.), P2=(.,.,.), radius=R, end1=., end2=., d1=., d2=.)"
    virtual string_t asString() const;

    real_t radius1() const { return radius1_; }   //!< returns radius of first basis
    real_t radius2() const { return radius2_; }        //!< returns radius of second basis
    GeometricEndShape endShape1() const { return endShape1_; } //!< returns shape of first end
    GeometricEndShape endShape2() const { return endShape2_; } //!< returns shape of second end
    real_t distance1() const { return distance1_; }            //!< returns height of first end
    real_t distance2() const { return distance2_; }            //!< returns height of second end

    virtual number_t nbSubdomains() const { return nbSubDomains_; }        //!< returns number of elements on lateral edges
    virtual number_t nbSubdiv() const; //!< returns number of subdivisions
    virtual dimen_t type() const { return type_; }  //!< returns type of subdivision

    virtual Geometry* clone() const { return new RevTrunk(*this); } //!< virtual copy constructor

    //! access to child RevTrunk object (const)
    virtual const RevTrunk* revTrunk() const {return this;}
    //! access to child RevTrunk object
    virtual RevTrunk* revTrunk() {return this;}

    virtual std::vector<const Point*> boundNodes() const;   //!< returns list of points on boundary (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< returns list of curves (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const; //!< returns list of faces (const)

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a RevTrunk
    virtual RevTrunk& transform(const Transformation& t);
    //! apply a translation on a RevTrunk (1 key)
    virtual RevTrunk& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a RevTrunk (vector version)
    virtual RevTrunk& translate(std::vector<real_t> u)
    {
      warning("deprecated", "RevTrunk::translate(Reals)", "RevTrunk::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a RevTrunk (3 reals version)
    virtual RevTrunk& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "RevTrunk::translate(Real, Real, Real)", "RevTrunk::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a RevTrunk (1 key)
    virtual RevTrunk& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a RevTrunk (2 keys)
    virtual RevTrunk& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a RevTrunk
    virtual RevTrunk& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "RevTrunk::rotate2d(Point, Real)", "RevTrunk::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a RevTrunk (1 key)
    virtual RevTrunk& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a RevTrunk (2 keys)
    virtual RevTrunk& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a RevTrunk (3 keys)
    virtual RevTrunk& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a RevTrunk
    virtual RevTrunk& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "RevTrunk::rotate3d(Point, Reals, Real)", "RevTrunk::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a RevTrunk
    virtual RevTrunk& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "RevTrunk::rotate3d(Real, Real, Real)", "RevTrunk::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a RevTrunk
    virtual RevTrunk& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "RevTrunk::rotate3d(Real, Real, Real, Real)", "RevTrunk::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a RevTrunk
    virtual RevTrunk& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "RevTrunk::rotate3d(Point, Real, Real, Real)", "RevTrunk::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a RevTrunk
    virtual RevTrunk& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "RevTrunk::rotate3d(Point, Real, Real, Real, Real)", "RevTrunk::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a RevTrunk (1 key)
    virtual RevTrunk& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a RevTrunk (2 keys)
    virtual RevTrunk& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a RevTrunk
    virtual RevTrunk& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "RevTrunk::homothetize(Point, Real)", "RevTrunk::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a RevTrunk
    virtual RevTrunk& homothetize(real_t factor)
    {
      warning("deprecated", "RevTrunk::homothetize(Real)", "RevTrunk::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a RevTrunk (1 key)
    virtual RevTrunk& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a RevTrunk
    virtual RevTrunk& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "RevTrunk::pointReflect(Point)", "RevTrunk::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a RevTrunk (1 key)
    virtual RevTrunk& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a RevTrunk (2 keys)
    virtual RevTrunk& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a RevTrunk
    virtual RevTrunk& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "RevTrunk::reflect2d(Point, Reals)", "RevTrunk::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a RevTrunk
    virtual RevTrunk& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "RevTrunk::reflect2d(Point, Real, Real)", "RevTrunk::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a RevTrunk (1 key)
    virtual RevTrunk& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a RevTrunk (2 keys)
    virtual RevTrunk& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a RevTrunk
    virtual RevTrunk& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "RevTrunk::reflect3d(Point, Reals)", "RevTrunk::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a RevTrunk
    virtual RevTrunk& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "RevTrunk::reflect3d(Point, Real, Real, Real)", "RevTrunk::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
   \class RevCylinder
   A cylinder of revolution is defined by its axis (P1,P2) and the radius of any circle
   obtained by intersection with a plane orthogonal to (P1,P2).
   See more details at RevTrunk.

  RevCylinder constructors are based on a key-value system. Here are the available keys:
  - _center1, _center2: to define centers of bases when they are elliptical
  - _radius: the radius of the bases
  - _end1_shape, _end2_shape: the shape of extension parts on both ends of the RevCylinder
  - _end1_distance, _end2_distance: the lengths of extension parts on both ends of the RevCylinder
  - _nbsubdomains: the number of slices of the RevCylinder (subdivision mesg generator only)
  - _type: indicator to fit curved boundaries (default) or not which gives flat (or plane) boundaries
  - _nnodes: to define the number of nodes on the edges of the RevCylinder
  - _hsteps: to define the local mesh steps on build points of the RevCylinder
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class RevCylinder : public RevTrunk
{
  public:
    //! default constructor
    RevCylinder();
    //@{
    //! key-value constructor
    RevCylinder(Parameter p1, Parameter p2, Parameter p3);
    RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11);
    RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12);
    RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13);
    RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14);
    RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14, Parameter p15);
    //@}

  protected:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new RevCylinder(*this); } //!< virtual copy constructor

    //! computes the minimal box
    virtual void computeMB()
    {
      minimalBox = MinimalBox(basis_->minimalBox.boundPt(1), basis_->minimalBox.boundPt(2), basis_->minimalBox.boundPt(3),
                              basis_->minimalBox.boundPt(1) + origin_ - center1_);
    }

    //! access to child RevCylinder object (const)
    virtual const RevCylinder* revCylinder() const {return this;}
    //! access to child RevCylinder object
    virtual RevCylinder* revCylinder() {return this;}

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a RevCylinder
    virtual RevCylinder& transform(const Transformation& t);
    //! apply a translation on a RevCylinder (1 key)
    virtual RevCylinder& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a RevCylinder (vector version)
    virtual RevCylinder& translate(std::vector<real_t> u)
    {
      warning("deprecated", "RevCylinder::translate(Reals)", "RevCylinder::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a RevCylinder (3 reals version)
    virtual RevCylinder& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "RevCylinder::translate(Real, Real, Real)", "RevCylinder::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a RevCylinder (1 key)
    virtual RevCylinder& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a RevCylinder (2 keys)
    virtual RevCylinder& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a RevCylinder
    virtual RevCylinder& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "RevCylinder::rotate2d(Point, Real)", "RevCylinder::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a RevCylinder (1 key)
    virtual RevCylinder& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a RevCylinder (2 keys)
    virtual RevCylinder& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a RevCylinder (3 keys)
    virtual RevCylinder& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a RevCylinder
    virtual RevCylinder& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "RevCylinder::rotate3d(Point, Reals, Real)", "RevCylinder::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a RevCylinder
    virtual RevCylinder& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "RevCylinder::rotate3d(Real, Real, Real)", "RevCylinder::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a RevCylinder
    virtual RevCylinder& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "RevCylinder::rotate3d(Real, Real, Real, Real)", "RevCylinder::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a RevCylinder
    virtual RevCylinder& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "RevCylinder::rotate3d(Point, Real, Real, Real)", "RevCylinder::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a RevCylinder
    virtual RevCylinder& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "RevCylinder::rotate3d(Point, Real, Real, Real, Real)", "RevCylinder::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a RevCylinder (1 key)
    virtual RevCylinder& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a RevCylinder (2 keys)
    virtual RevCylinder& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a RevCylinder
    virtual RevCylinder& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "RevCylinder::homothetize(Point, Real)", "RevCylinder::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a RevCylinder
    virtual RevCylinder& homothetize(real_t factor)
    {
      warning("deprecated", "RevCylinder::homothetize(Real)", "RevCylinder::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a RevCylinder (1 key)
    virtual RevCylinder& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a RevCylinder
    virtual RevCylinder& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "RevCylinder::pointReflect(Point)", "RevCylinder::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a RevCylinder (1 key)
    virtual RevCylinder& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a RevCylinder (2 keys)
    virtual RevCylinder& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a RevCylinder
    virtual RevCylinder& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "RevCylinder::reflect2d(Point, Reals)", "RevCylinder::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a RevCylinder
    virtual RevCylinder& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "RevCylinder::reflect2d(Point, Real, Real)", "RevCylinder::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a RevCylinder (1 key)
    virtual RevCylinder& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a RevCylinder (2 keys)
    virtual RevCylinder& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a RevCylinder
    virtual RevCylinder& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "RevCylinder::reflect3d(Point, Reals)", "RevCylinder::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a RevCylinder
    virtual RevCylinder& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "RevCylinder::reflect3d(Point, Real, Real, Real)", "RevCylinder::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

/*!
   \class RevCone
   A truncated cone of revolution is defined by its axis (P1,P2) and two radii R1 and R2.
   R1 (resp. R2) is the radius of the circle lying in the plane orthogonal to (P1,P2) passing
   by P1 (resp. P2).
   R1 (or R2) can be 0, in which case we get a cone of revolution.
   See more details at RevTrunk.

  RevCone constructors are based on a key-value system. Here are the available keys:
  - _center: to define the center of the basis
  - _apex: the apex of the RevCone
  - _radius: the radius of the basis
  - _end_shape: the shape of extension part on the end of the RevCone
  - _end_distance: the length of extension part on the end of the RevCone
  - _nbsubdomains: the number of slices of the RevCone (subdivision mesh generator only)
  - _type: indicator to fit curved boundaries (default) or not which gives flat (or plane) boundaries
  - _nnodes: to define the number of nodes on the edges of the RevCone
  - _hsteps: to define the local mesh steps on build points of the RevCone
  - _domain_name: to define the domain name
  - _side_names/_face_name: to define the side names
  - _edge_name: to define the side of side names
  - _vertex_name: to define the side of side of side names
  - _varnames: to define the variable names for print purpose
 */
class RevCone : public RevTrunk
{
  public:
    //! default constructor
    RevCone();
    RevCone(Parameter p1, Parameter p2, Parameter p3);
    RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9);
    RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10);
    RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11);
    RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12);
    RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13);
    //@}

  protected:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}

  public:
    //! accessor to apex_
    Point apex() const { return origin_; }

    //! format as string
    virtual string_t asString() const;

    virtual Geometry* clone() const { return new RevCone(*this); } //!< virtual copy constructor

    virtual void computeMB()
    {
      minimalBox = MinimalBox(basis_->minimalBox.boundPt(1),
                              basis_->minimalBox.boundPt(2),
                              basis_->minimalBox.boundPt(3),
                              origin_);
    }

    //! access to child RevCone object (const)
    virtual const RevCone* revCone() const {return this;}
    //! access to child RevCone object
    virtual RevCone* revCone() {return this;}

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a RevCone
    virtual RevCone& transform(const Transformation& t);
    //! apply a translation on a RevCone (1 key)
    virtual RevCone& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a RevCone (vector version)
    virtual RevCone& translate(std::vector<real_t> u)
    {
      warning("deprecated", "RevCone::translate(Reals)", "RevCone::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a RevCone (3 reals version)
    virtual RevCone& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "RevCone::translate(Real, Real, Real)", "RevCone::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a RevCone (1 key)
    virtual RevCone& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a RevCone (2 keys)
    virtual RevCone& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a RevCone
    virtual RevCone& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "RevCone::rotate2d(Point, Real)", "RevCone::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a RevCone (1 key)
    virtual RevCone& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a RevCone (2 keys)
    virtual RevCone& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a RevCone (3 keys)
    virtual RevCone& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a RevCone
    virtual RevCone& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "RevCone::rotate3d(Point, Reals, Real)", "RevCone::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a RevCone
    virtual RevCone& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "RevCone::rotate3d(Real, Real, Real)", "RevCone::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a RevCone
    virtual RevCone& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "RevCone::rotate3d(Real, Real, Real, Real)", "RevCone::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a RevCone
    virtual RevCone& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "RevCone::rotate3d(Point, Real, Real, Real)", "RevCone::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a RevCone
    virtual RevCone& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "RevCone::rotate3d(Point, Real, Real, Real, Real)", "RevCone::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a RevCone (1 key)
    virtual RevCone& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a RevCone (2 keys)
    virtual RevCone& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a RevCone
    virtual RevCone& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "RevCone::homothetize(Point, Real)", "RevCone::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a RevCone
    virtual RevCone& homothetize(real_t factor)
    {
      warning("deprecated", "RevCone::homothetize(Real)", "RevCone::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a RevCone (1 key)
    virtual RevCone& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a RevCone
    virtual RevCone& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "RevCone::pointReflect(Point)", "RevCone::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a RevCone (1 key)
    virtual RevCone& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a RevCone (2 keys)
    virtual RevCone& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a RevCone
    virtual RevCone& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "RevCone::reflect2d(Point, Reals)", "RevCone::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a RevCone
    virtual RevCone& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "RevCone::reflect2d(Point, Real, Real)", "RevCone::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a RevCone (1 key)
    virtual RevCone& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a RevCone (2 keys)
    virtual RevCone& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a RevCone
    virtual RevCone& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "RevCone::reflect3d(Point, Reals)", "RevCone::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a RevCone
    virtual RevCone& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "RevCone::reflect3d(Point, Real, Real, Real)", "RevCone::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

//============================================================================================
/*!
  \class EllipsoidSidePart
  definition of a part of ellipsoid side in R^3 (surface),
  defined by an ellipsoid and angles theta, phi related to ellisoid axes:
       theta = (1-u)theta_min + u theta_max, phi = (1-v)phi_min + v phi_max
       P(u,v) =  P0 + (P1-P0) cos(theta) cos(phi) + (P2-P0) sin(theta) cos(phi) + (P6-P0) sin(phi)
  no construction via key parameter, no geometric transformation available, no mesh (p_,n_,h_ not managed)
  not a user geometry for the moment !!!
 */
//============================================================================================
enum EllipsoidParametrizationType {_thetaphi, _stereographic, _bistereographic, _bispherical};

class EllipsoidSidePart : public Surface
{
  protected:
    Ellipsoid ellipsoid;
    real_t theta_min, theta_max; // azymuth theta = atan2(y,x) in ]-pi,pi],
    real_t phi_min, phi_max;     // elevation phi = asin(z/r) in [-pi/2,pi/2]
    EllipsoidParametrizationType parametrizationType; // _thetaphi,_stereographic,_bistereographic
    EllipsoidSidePart(); //! default constructor
  public:
    //! eplicit constructor
    EllipsoidSidePart(const Geometry& g, real_t tmin, real_t tmax, real_t pmin, real_t pmax, EllipsoidParametrizationType = _thetaphi);
    //! destructor
    virtual ~EllipsoidSidePart() {}
    //! format as string
    virtual string_t asString() const;
    //! virtual copy constructor for Geometry
    virtual Geometry* clone() const { return new EllipsoidSidePart(*this); }
    //! access to child EllipsoidSidePart object (const)
    virtual const EllipsoidSidePart* ellipsoidSidePart() const {return this;}
    //! access to child EllipsoidSidePart object
    virtual EllipsoidSidePart* ellipsoidSidePart() {return this;}
    //! true if full ellisoid side
    bool isFull() const;

    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< parametrization
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< inverse of parametrization
    Vector<real_t> thetaPhiParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const;           //!< theta-phi parametrization
    Vector<real_t> invThetaPhiParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const;        //!< inv of theta-phi parametrization
    Vector<real_t> stereographicParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const;      //!< stereographic parametrization
    Vector<real_t> invStereographicParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const;   //!< inv of stereographic parametrization
    Vector<real_t> biStereographicParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const;    //!< bi-stereographic parametrization
    Vector<real_t> invBiStereographicParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< inv of bi-stereographic parametrization
    Vector<real_t> biSphericalParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const;        //!< bi-spherical parametrization (front)
    Vector<real_t> biSphericalParametrizationStandard(const Point& pt, Parameters& pars, DiffOpType d) const;      //!< standard bi-spherical parametrization
    Vector<real_t> biSphericalExtension(MeshElement* melt, const Point& pt, Parameters& pars, DiffOpType d) const; //!< extended bi-spherical parametrization
    Vector<real_t> invBiSphericalParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const;     //!< inv of bi-spherical parametrization

    virtual GeometricGeodesic geodesic(const Point& x, const Point& dx, bool wCA = false, bool wT = false); //! build a geometric geodesic starting at (x,dx)
};

inline Vector<real_t> parametrization_EllipsoidSidePart(const Point& pt, Parameters& pars, DiffOpType d = _id)  //! extern parametrization call
{return reinterpret_cast<const EllipsoidSidePart*>(pars("geometry").get_p())->funParametrization(pt, pars, d);}
inline Vector<real_t> invParametrization_EllipsoidSidePart(const Point& pt, Parameters& pars, DiffOpType d = _id) //! extern invParametrization call
{return reinterpret_cast<const EllipsoidSidePart*>(pars("geometry").get_p())->invParametrization(pt, pars, d);}

//============================================================================================
/*!
  \class TrunkSidePart
  definition of a part of trunk side in R^3 (lateral surface)
  Trunk is defined by its basis_ (B), center1 (C), origin_(O) and scale factor (s) of the top
  part of side trunk is defined from a Trunk object
  and either 2 points of basis boundary or two angles if elliptical trunk
  General parametrisation of a side part ( g(u) parametrization of the basis boundary)
            P(u,v) = (1-v+sv)g(u) + v(O -sC)
  no construction via key parameter, no geometric transformation available, no mesh (p_,n_,h_ not managed)
  not a user geometry for the moment !!!
 */
//============================================================================================
class TrunkSidePart : public Surface
{
  protected:
    Trunk trunk_;             //! trunk object
    Geometry* basisBoundary_; //! boundary of basis as a Geometry object (build by constructor if required)
    real_t t_min, t_max;      //! bounds of basis boundary parameter
    bool fromAngles_;         //! definition from angles
    bool shortest_;           //! shortest arc (not managed)
    TrunkSidePart();          //! default constructor
  public:
    //! eplicit constructor
    TrunkSidePart(const Geometry& g, real_t tmin, real_t tmax, bool sh = true);
    TrunkSidePart(const Geometry& g, const Point& a = Point(), const Point& b = Point(), bool sh = true);
    //! destructor
    virtual ~TrunkSidePart() {if (basisBoundary_ != nullptr) delete basisBoundary_;}
    //! format as string
    virtual string_t asString() const;
    virtual Geometry* clone() const { return new TrunkSidePart(*this); } //!< virtual copy constructor for Geometry
    //! access to child TrunkSidePart object (const)
    virtual const TrunkSidePart* trunkSidePart() const {return this;}
    //! access to child TrunkSidePart object
    virtual TrunkSidePart* trunkSidePart() {return this;}

    const Trunk& relTrunk() const {return trunk_;}
    real_t tmin() const {return t_min;}
    real_t tmax() const {return t_max;}
    real_t scale() const { return trunk_.scale(); }
    const Point& origin() const { return trunk_.origin(); }
    const Point& center1() const { return trunk_.center1(); }
    const Point& center2() const { return trunk_.origin(); }
    const Point& p1() const { return trunk_.p1(); }
    const Point& p2() const { return trunk_.p2(); }

    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< parametrization
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< inverse of parametrization
};

inline Vector<real_t> parametrization_TrunkSidePart(const Point& pt, Parameters& pars, DiffOpType d = _id)  //! extern parametrization call
{return reinterpret_cast<const TrunkSidePart*>(pars("geometry").get_p())->funParametrization(pt, pars, d);}
inline Vector<real_t> invParametrization_TrunkSidePart(const Point& pt, Parameters& pars, DiffOpType d = _id) //! extern invParametrization call
{return reinterpret_cast<const TrunkSidePart*>(pars("geometry").get_p())->invParametrization(pt, pars, d);}


} // end of namespace xlifepp

#endif // GEOMETRIES_3D_HPP
