 /*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DomainMap.cpp
  \author E. Lunéville
  \since 04 apr 2012
  \date 24 sept 2012

  \brief Implementation of xlifepp::DomainMap class and functionalities
*/

#include "DomainMap.hpp"

namespace xlifepp
{

//---------------------------------------------------------------------------
// DomainMap member functions and related external functions
//---------------------------------------------------------------------------
//constructor
DomainMap::DomainMap(const GeomDomain& d1, const GeomDomain& d2, const Function& f, bool nearest)
    : dom1_p(&d1), dom2_p(&d2), map1to2_(f), useNearest(nearest)
{
  name = d1.name()+" --> "+d2.name();
  if(theDomainMaps.find(name)!= theDomainMaps.end())
    error("domain_duplicate_map",name);
  theDomainMaps[name]=this;;
}

DomainMap::DomainMap(const GeomDomain& d1, const GeomDomain& d2, const Function& f, const string_t& n, bool nearest)
    : dom1_p(&d1), dom2_p(&d2), map1to2_(f), name(n), useNearest(nearest)
{
  if(theDomainMaps.find(name)!= theDomainMaps.end())
    error("domain_duplicate_map",name);
  theDomainMaps[name]=this;;
}

//destructor
DomainMap::~DomainMap()
{
  std::map<string_t, DomainMap *>::iterator it=theDomainMaps.begin();
  for(;it!=theDomainMaps.end();++it)
     if(it->second==this)
     {
       theDomainMaps.erase(it);
       return;
     }
}

// delete all map objects
void DomainMap::clearGlobalVector()
{
  while ( DomainMap::theDomainMaps.size() > 0 )
  {
    delete DomainMap::theDomainMaps.begin()->second;
  }
}

//remove maps involving domain d
void DomainMap::removeMaps(const GeomDomain& d)
{
  if(DomainMap::theDomainMaps.size()==0) return;
  std::map<string_t,DomainMap *>::iterator it =DomainMap::theDomainMaps.begin();
  while(it!=DomainMap::theDomainMaps.end())
  {
    if(&it->second->dom1()==&d || &it->second->dom2()==&d)
    {
       delete it->second;   //be cautious DomainMap destructor removes map from theDomainMaps
       it=DomainMap::theDomainMaps.begin(); // not optimal but safe
    }
    else it++;
  }
}

//find domainmap from dom1 to dom2
const DomainMap* domainMap(const GeomDomain& d1, const GeomDomain& d2)
{
  std::map<string_t, DomainMap *>::iterator it=DomainMap::theDomainMaps.begin();
  for(;it!=DomainMap::theDomainMaps.end();it++)
     if(&it->second->dom1()==&d1 || &it->second->dom2()==&d2) return it->second;
  return nullptr;
}

//find map from dom1 to dom2
const Function* findMap(const GeomDomain& d1, const GeomDomain& d2)
{
  std::map<string_t, DomainMap *>::iterator it=DomainMap::theDomainMaps.begin();
  for(;it!=DomainMap::theDomainMaps.end();it++)
    if(&it->second->dom1()==&d1 || &it->second->dom2()==&d2) return &it->second->map1to2();
  // try to build one in simple case (translation of translated geomtry)
  return buildMap(d1,d2); //return 0 if building fails
}

//return a translation map if one geometry is a translation of the other
// restricted to canonical geometries (two points, two segments, two disks, ...)
const Function* buildMap(const GeomDomain& d1, const GeomDomain& d2)
{
    if(d1.dim()!=d2.dim()) return nullptr;
    Point T(0.);
    bool id=false;
    if(d1.dim()==0) //special case of points, translation is given by P2-P1
    {
       T = d2.meshDomain()->element(1)->center()-d1.meshDomain()->element(1)->center();
    }
    else
    {
        Geometry* g1=d1.geometry(), *g2=d2.geometry();
        if(g1==nullptr || g2==nullptr) return nullptr;  // no geometry
        if(g1->shape()!=g2->shape()) return nullptr;
        if(!g1->isTranslated(*g2,T)) return nullptr;
    }
    Parameters * parsmap= new Parameters(bool(0),"id");
    Parameters & pa = *parsmap;
    pa<<Parameter(T,"T");
    if(norm(T)<theTolerance) pa("id")=true;
    new DomainMap(d1,d2,Function(domainTranslation, pa),"trans "+d1.name()+" --> "+d2.name());
    return findMap(d1,d2);
}

Vector<real_t> domainTranslation(const Point& P, Parameters& pa)
{
    bool id=pa("id");
    if(id) return P;
    Point T = pa("T");
    return T+=P;
}

//define new map with default name "d1.name-->d2.name"
void defineMap(const GeomDomain& d1, const GeomDomain& d2, const Function& f,bool nearest)
{
  new DomainMap(d1,d2,f,nearest);
}
//define new map with name n
void defineMap(const GeomDomain& d1, const GeomDomain& d2, const Function& f,const string_t& n, bool nearest)
{
  new DomainMap(d1,d2,f,n,nearest);
}

} // end of namespace xlifepp
