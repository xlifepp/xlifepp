/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file geometries1D.hpp
  \authors Y. Lafranche, N. Kielbasiewicz
  \since 18 oct 2012
  \date 30 jul 2015

  \brief Definition of the classes corresponding to 1D canonical geometrical shapes.

*/

#ifndef GEOMETRIES_1D_HPP
#define GEOMETRIES_1D_HPP

#include "Geometry.hpp"

namespace xlifepp
{
//===================================================================================================
/*!
  \class Curve
  base class for 1D geometries
*/
//===================================================================================================
class Curve : public Geometry
{
  public:
    //! default constructor
    Curve();
    
  protected:
    bool isClosed_; //!< true if the curve is closed
    //! number of nodes (generally only one value)  # move scalar to vector in order to deal with spline (Eric)
    std::vector<number_t> n_;
    //! local steps on control points
    std::vector<real_t> h_;
    //! first vertex of the arc
    Point p1_;
    //! second vertex of the arc
    Point p2_;
    
    //@{
    //! true constructor functions
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}
    
  public:
    Curve(const Curve& c) : Geometry(c), isClosed_(c.isClosed_) {} //!< copy constructor
    virtual bool isClosed() const { return isClosed_; } //!< accessor to know if the curve is closed or not
    virtual Geometry* clone() const { return new Curve(*this); } //!< virtual copy constructor for Geometry
    virtual ~Curve() {} //!< virtual destructor
    
    //! format as string
    virtual string_t asString() const { error("shape_not_handled", words("shape", shape_)); return string_t(); }
    virtual void computeBB();
    virtual void computeMB()                                         //! computes the minimal box
    { minimalBox = MinimalBox(boundingBox.bounds());}                //! default minimal box = bounding box
    
    //! accessor to number of nodes on curve (read only)
    const std::vector<number_t>& n() const { return n_;}
    //! accessor to number of nodes on curve (read/write)
    std::vector<number_t>& n() { return n_;}
    //! accessor to number of nodes on curve (read/write)
    number_t& n(number_t i) { return n_[i];}
    //! accessor to number of nodes on curve (read only)
    number_t n(number_t i) const { return n_[i];}
    //! return the local steps on the curve
    const std::vector<real_t>& h() const {return h_;}
    //! return the local steps on the curve
    std::vector<real_t>& h() {return h_;}
    //! return the local step on the first vertex
    real_t h1() const {return h_[0];}
    //! return the local step on the second vertex
    real_t h2() const {return h_[1];}
    //! check if Segment is defined only with _nnodes or with _hsteps option
    bool withNnodes() const { return (h_.size() == 0); }
    //! set hsteps vector from scalar h
    virtual void setHstep(real_t h) {h_[0] = h; h_[1] = h;}
    //! set the main nnodes, if not overriden do nothing
    virtual void setNnodes(number_t i) {number_t d = n_.size(); n_.clear(); n_.resize(d, i);}
    
    
    //@{
    //! accessor to point
    const Point& p1() const {return p1_; }
    const Point& p2() const {return p2_; }
    //@}
    
    const Curve* curve() const //! access to Curve object (const), virtual in Geometry
    { return this; }
    Curve* curve()  //! access to Curve object (non const), virtual in Geometry
    { return this; }
    
    //! reverse orientation of curve
    Curve reverse() const { error("shape_not_handled", words("shape", shape_)); return Curve();}
    
    //! returns list of points on boundary (const)
    virtual std::vector<const Point*> boundNodes() const;
    //! return the list of curves
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const;
    //! return the list of surfaces
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const
    { return std::vector<std::pair<ShapeType, std::vector<const Point*> > >(); } // no faces
    
    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Curve
    virtual Curve& transform(const Transformation& t);
    //! apply a translation on a Curve (1 key)
    virtual Curve& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Curve (vector version)
    virtual Curve& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Curve::translate(Reals)", "Curve::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Curve (3 reals version)
    virtual Curve& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Curve::translate(Real, Real, Real)", "Curve::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Curve (1 key)
    virtual Curve& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Curve (2 keys)
    virtual Curve& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Curve
    virtual Curve& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Curve::rotate2d(Point, Real)", "Curve::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Curve (1 key)
    virtual Curve& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Curve (2 keys)
    virtual Curve& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Curve (3 keys)
    virtual Curve& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Curve
    virtual Curve& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Curve::rotate3d(Point, Reals, Real)", "Curve::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Curve
    virtual Curve& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Curve::rotate3d(Real, Real, Real)", "Curve::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Curve
    virtual Curve& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Curve::rotate3d(Real, Real, Real, Real)", "Curve::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Curve
    virtual Curve& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Curve::rotate3d(Point, Real, Real, Real)", "Curve::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Curve
    virtual Curve& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Curve::rotate3d(Point, Real, Real, Real, Real)", "Curve::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Curve (1 key)
    virtual Curve& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Curve (2 keys)
    virtual Curve& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Curve
    virtual Curve& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Curve::homothetize(Point, Real)", "Curve::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Curve
    virtual Curve& homothetize(real_t factor)
    {
      warning("deprecated", "Curve::homothetize(Real)", "Curve::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Curve (1 key)
    virtual Curve& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Curve
    virtual Curve& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Curve::pointReflect(Point)", "Curve::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Curve (1 key)
    virtual Curve& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Curve (2 keys)
    virtual Curve& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Curve
    virtual Curve& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Curve::reflect2d(Point, Reals)", "Curve::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Curve
    virtual Curve& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Curve::reflect2d(Point, Real, Real)", "Curve::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Curve (1 key)
    virtual Curve& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Curve (2 keys)
    virtual Curve& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Curve
    virtual Curve& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Curve::reflect3d(Point, Reals)", "Curve::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Curve
    virtual Curve& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Curve::reflect3d(Point, Real, Real, Real)", "Curve::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
};

//===================================================================================================
/*!
  \class Segment
  definition of a segment geometry in R^3

  Segment constructors are based on a key-value system. Here are the available keys:
  - _xmin, _xmax: to define a Segment along the x axis
  - _v1, _v2: to define a Segment from its bounds
  - _nnodes: to define the number of nodes on the Segment
  - _hsteps: to define the local mesh steps on the bounds of a Segment
  - _domain_name: to define the domain name
  - _side_names/_vertex_names: to define the side names
  - _varnames: to define the variable names for print purpose
 */
//===================================================================================================
class Segment : public Curve
{
  public:
    //! default constructor
    Segment();
    //@{
    //! key-value constructor
    Segment(Parameter p1, Parameter p2);
    Segment(Parameter p1, Parameter p2, Parameter p3);
    Segment(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    Segment(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    Segment(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    //@}
    //! copy constructor
    Segment(const Segment&);
    
  private:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}
    
  public:
    //! destructor
    virtual ~Segment() {}
    
    //! format as string
    string_t asString() const;
    
    //! reverse orientation of segment
    Segment& reverse();
    
    virtual Geometry* clone() const { return new Segment(*this); } //!< virtual copy constructor for Geometry
    virtual void computeMB() { minimalBox = MinimalBox(p1_, p2_); } //!< computes the minimal box
    
    virtual std::vector<Point*> nodes(); //!< returns list of every node (non const)
    virtual std::vector<const Point*> nodes() const; //!< returns list of every node (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< returns list of curves (const)
    virtual number_t nbSides() const { return 2; } //!< returns the number of sides
    real_t measure() const { return p1_.distance(p2_); }
    bool isTranslated(const Geometry& g, Point& T);//!< rue if current is translated from g, T the translation vector
    virtual bool isPlane() const {return true;}
    
    //! access to child Segment object (const)
    virtual const Segment* segment() const {return this;}
    //! access to child Segment object
    virtual Segment* segment() {return this;}
    
    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Segment
    virtual Segment& transform(const Transformation& t);
    //! apply a translation on a Segment (1 key)
    virtual Segment& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Segment (vector version)
    virtual Segment& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Segment::translate(Reals)", "Segment::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Segment (3 reals version)
    virtual Segment& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Segment::translate(Real, Real, Real)", "Segment::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Segment (1 key)
    virtual Segment& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Segment (2 keys)
    virtual Segment& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Segment
    virtual Segment& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Segment::rotate2d(Point, Real)", "Segment::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Segment (1 key)
    virtual Segment& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Segment (2 keys)
    virtual Segment& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Segment (3 keys)
    virtual Segment& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Segment
    virtual Segment& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Segment::rotate3d(Point, Reals, Real)", "Segment::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Segment
    virtual Segment& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Segment::rotate3d(Real, Real, Real)", "Segment::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Segment
    virtual Segment& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Segment::rotate3d(Real, Real, Real, Real)", "Segment::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Segment
    virtual Segment& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Segment::rotate3d(Point, Real, Real, Real)", "Segment::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Segment
    virtual Segment& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Segment::rotate3d(Point, Real, Real, Real, Real)", "Segment::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Segment (1 key)
    virtual Segment& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Segment (2 keys)
    virtual Segment& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Segment
    virtual Segment& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Segment::homothetize(Point, Real)", "Segment::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Segment
    virtual Segment& homothetize(real_t factor)
    {
      warning("deprecated", "Segment::homothetize(Real)", "Segment::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Segment (1 key)
    virtual Segment& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Segment
    virtual Segment& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Segment::pointReflect(Point)", "Segment::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Segment (1 key)
    virtual Segment& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Segment (2 keys)
    virtual Segment& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Segment
    virtual Segment& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Segment::reflect2d(Point, Reals)", "Segment::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Segment
    virtual Segment& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Segment::reflect2d(Point, Real, Real)", "Segment::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Segment (1 key)
    virtual Segment& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Segment (2 keys)
    virtual Segment& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Segment
    virtual Segment& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Segment::reflect3d(Point, Reals)", "Segment::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Segment
    virtual Segment& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Segment::reflect3d(Point, Real, Real, Real)", "Segment::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
    
    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< parametrization (1-t)*p1+t*p2
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< inverse of parametrization t=(p-p1|p2-p1)/|p2-p1|^2
};
Segment operator~(const Segment& s); //!< copy a Segment and reverse its orientation
inline Vector<real_t> parametrization_Segment(const Point& pt, Parameters& pars, DiffOpType d = _id)  //! extern parametrization call
{ return reinterpret_cast<const Segment*>(pars("geometry").get_p())->funParametrization(pt, pars, d); }
inline Vector<real_t> invParametrization_Segment(const Point& pt, Parameters& pars, DiffOpType d = _id) //! extern invParametrization call
{ return reinterpret_cast<const Segment*>(pars("geometry").get_p())->invParametrization(pt, pars, d); }

//===================================================================================================
/*!
  \class EllArc
  definition of an elliptic arc geometry in R^3 (curve)

  EllArc constructors are based on a key-value system. Here are the available keys:
  - _center: to define the center of the ellipse supporting the arc
  - _apogee: to define the apogee of the ellipse supporting the arc
  - _v1, _v2: to define the bounds of the arc
  - _nnodes: to define the number of nodes on the arc
  - _hsteps: to define the local mesh steps on the bounds of the arc
  - _domain_name: to define the domain name
  - _side_names/_vertex_names: to define the side names
  - _varnames: to define the variable names for print purpose
 */
//===================================================================================================
class EllArc : public Curve
{
  protected:
    //! center of the ellipse arc
    Point c_;
    //! first apogee of the ellipse arc
    Point a_;
    //! second apogee of the ellipse arc
    Point b_;
    //! angle of the first vertex of the arc (radian)
    real_t thetamin_;
    //! angle of the second vertex of the arc (radian)
    real_t thetamax_;
    
  public:
    //! default constructor with side names
    EllArc();
    //@{
    //! key-value constructor
    EllArc(Parameter p1, Parameter p2, Parameter p3);
    EllArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    EllArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    EllArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    EllArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    EllArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    //@}
    //! copy constructor
    EllArc(const EllArc& e);
    
  private:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}
    
  public:
    //! destructor
    virtual ~EllArc() {}
    
    //@{
    //! accessor to construction point
    const Point& center() const { return c_; }
    const Point& apogee() const { return a_; }
    const Point& apogee2() const { return b_; }
    //@}
    
    //! format as string
    virtual string_t asString() const;
    
    //! reverse orientation of arc
    EllArc& reverse();
    
    virtual Geometry* clone() const { return new EllArc(*this); } //!< virtual copy constructor for Geometry
    
    virtual std::vector<Point*> nodes(); //!< returns list of every point (non const)
    virtual std::vector<const Point*> nodes() const; //!< returns list of every point (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< returns list of curves (const)
    virtual number_t nbSides() const { return 2; } //!< returns the number of sides
    virtual bool isPlane() const {return true;}
    
    //! compute the second apogee
    virtual void computeBAndAngles();
    //! computes the minimal box
    virtual void computeMB();
    //! compute the bounding box
    virtual void computeBB();
    
    //! access to child EllArc object (const)
    virtual const EllArc* ellArc() const {return this;}
    //! access to child EllArc object
    virtual EllArc* ellArc() {return this;}
    
    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a EllArc
    virtual EllArc& transform(const Transformation& t);
    //! apply a translation on a EllArc (1 key)
    virtual EllArc& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a EllArc (vector version)
    virtual EllArc& translate(std::vector<real_t> u)
    {
      warning("deprecated", "EllArc::translate(Reals)", "EllArc::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a EllArc (3 reals version)
    virtual EllArc& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "EllArc::translate(Real, Real, Real)", "EllArc::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a EllArc (1 key)
    virtual EllArc& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a EllArc (2 keys)
    virtual EllArc& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a EllArc
    virtual EllArc& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "EllArc::rotate2d(Point, Real)", "EllArc::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a EllArc (1 key)
    virtual EllArc& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a EllArc (2 keys)
    virtual EllArc& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a EllArc (3 keys)
    virtual EllArc& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a EllArc
    virtual EllArc& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "EllArc::rotate3d(Point, Reals, Real)", "EllArc::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a EllArc
    virtual EllArc& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "EllArc::rotate3d(Real, Real, Real)", "EllArc::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a EllArc
    virtual EllArc& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "EllArc::rotate3d(Real, Real, Real, Real)", "EllArc::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a EllArc
    virtual EllArc& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "EllArc::rotate3d(Point, Real, Real, Real)", "EllArc::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a EllArc
    virtual EllArc& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "EllArc::rotate3d(Point, Real, Real, Real, Real)", "EllArc::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a EllArc (1 key)
    virtual EllArc& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a EllArc (2 keys)
    virtual EllArc& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a EllArc
    virtual EllArc& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "EllArc::homothetize(Point, Real)", "EllArc::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a EllArc
    virtual EllArc& homothetize(real_t factor)
    {
      warning("deprecated", "EllArc::homothetize(Real)", "EllArc::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a EllArc (1 key)
    virtual EllArc& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a EllArc
    virtual EllArc& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "EllArc::pointReflect(Point)", "EllArc::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a EllArc (1 key)
    virtual EllArc& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a EllArc (2 keys)
    virtual EllArc& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a EllArc
    virtual EllArc& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "EllArc::reflect2d(Point, Reals)", "EllArc::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a EllArc
    virtual EllArc& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "EllArc::reflect2d(Point, Real, Real)", "EllArc::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a EllArc (1 key)
    virtual EllArc& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a EllArc (2 keys)
    virtual EllArc& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a EllArc
    virtual EllArc& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "EllArc::reflect3d(Point, Reals)", "EllArc::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a EllArc
    virtual EllArc& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "EllArc::reflect3d(Point, Real, Real, Real)", "EllArc::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
    
    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< parametrization c+(a-c)cos(t)+(b-c)sin(t)
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< inverse of parametrization c+(a-c)cos(t)+(b-c)sin(t)
};

EllArc operator~(const EllArc& e); //!< copy a EllArc and reverse its orientation
inline Vector<real_t> parametrization_EllArc(const Point& pt, Parameters& pars, DiffOpType d = _id)  //! extern parametrization call
{return reinterpret_cast<const EllArc*>(pars("geometry").get_p())->funParametrization(pt, pars, d);}
inline Vector<real_t> invParametrization_EllArc(const Point& pt, Parameters& pars, DiffOpType d = _id) //! extern invParametrization call
{return reinterpret_cast<const EllArc*>(pars("geometry").get_p())->invParametrization(pt, pars, d);}

//===================================================================================================
/*!
  \class CircArc
  definition of a circular arc geometry in R^3 (curve)

  EllArc constructors are based on a key-value system. Here are the available keys:
  - _center: to define the center of the circle supporting the arc
  - _v1, _v2: to define the bounds of the arc
  - _nnodes: to define the number of nodes on the arc
  - _hsteps: to define the local mesh steps on the bounds of the arc
  - _domain_name: to define the domain name
  - _side_names/_vertex_names: to define the side names
  - _varnames: to define the variable names for print purpose
 */
//===================================================================================================
class CircArc : public Curve
{
  protected:
    //! center of the circle arc
    Point c_;
    //! first "apogee" of the arc (=p1_)
    Point a_;
    //! second "apogee" of the arc
    Point b_;
    //! angle of the second vertex of the arc (always 0.)
    real_t thetamin_;
    //! angle of the second vertex of the arc (radian)
    real_t thetamax_;
    //! radius
    real_t radius_;
    
  public:
    //! default constructor with side names
    CircArc();
    //@{
    //! key-value constructor
    CircArc(Parameter p1, Parameter p2, Parameter p3);
    CircArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    CircArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    CircArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    CircArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    //@}
    //! copy constructor
    CircArc(const CircArc& s);
    
  private:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}
    
  public:
    //! destructor
    virtual ~CircArc() {}
    
    //@{
    //! accessor to circle data
    const Point& center() const { return c_; }
    real_t radius() const { return radius_; }
    //@}
    
    //! format as string
    virtual string_t asString() const;
    
    //! reverse orientation of arc
    CircArc& reverse();
    
    virtual Geometry* clone() const { return new CircArc(*this); } //!< virtual copy constructor for Geometry
    
    virtual std::vector<Point*> nodes(); //!< returns list of every point (non const)
    virtual std::vector<const Point*> nodes() const; //!< returns list of every point (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< returns list of curves (const)
    virtual number_t nbSides() const { return 2; } //!< returns the number of sides
    real_t measure() const;
    virtual bool isPlane() const {return true;}
    
    //! compute the second apogee
    virtual void computeBAndAngle();
    //! computes the minimal box
    virtual void computeMB();
    //! computes the bounding box
    virtual void computeBB();
    
    //! access to child CircArc object (const)
    virtual const CircArc* circArc() const {return this;}
    //! access to child CircArc object
    virtual CircArc* circArc() {return this;}
    
    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a CircArc
    virtual CircArc& transform(const Transformation& t);
    //! apply a translation on a CircArc (1 key)
    virtual CircArc& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a CircArc (vector version)
    virtual CircArc& translate(std::vector<real_t> u)
    {
      warning("deprecated", "CircArc::translate(Reals)", "CircArc::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a CircArc (3 reals version)
    virtual CircArc& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "CircArc::translate(Real, Real, Real)", "CircArc::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a CircArc (1 key)
    virtual CircArc& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a CircArc (2 keys)
    virtual CircArc& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a CircArc
    virtual CircArc& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "CircArc::rotate2d(Point, Real)", "CircArc::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a CircArc (1 key)
    virtual CircArc& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a CircArc (2 keys)
    virtual CircArc& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a CircArc (3 keys)
    virtual CircArc& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a CircArc
    virtual CircArc& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "CircArc::rotate3d(Point, Reals, Real)", "CircArc::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a CircArc
    virtual CircArc& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "CircArc::rotate3d(Real, Real, Real)", "CircArc::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a CircArc
    virtual CircArc& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "CircArc::rotate3d(Real, Real, Real, Real)", "CircArc::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a CircArc
    virtual CircArc& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "CircArc::rotate3d(Point, Real, Real, Real)", "CircArc::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a CircArc
    virtual CircArc& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "CircArc::rotate3d(Point, Real, Real, Real, Real)", "CircArc::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a CircArc (1 key)
    virtual CircArc& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a CircArc (2 keys)
    virtual CircArc& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a CircArc
    virtual CircArc& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "CircArc::homothetize(Point, Real)", "CircArc::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a CircArc
    virtual CircArc& homothetize(real_t factor)
    {
      warning("deprecated", "CircArc::homothetize(Real)", "CircArc::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a CircArc (1 key)
    virtual CircArc& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a CircArc
    virtual CircArc& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "CircArc::pointReflect(Point)", "CircArc::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a CircArc (1 key)
    virtual CircArc& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a CircArc (2 keys)
    virtual CircArc& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a CircArc
    virtual CircArc& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "CircArc::reflect2d(Point, Reals)", "CircArc::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a CircArc
    virtual CircArc& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "CircArc::reflect2d(Point, Real, Real)", "CircArc::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a CircArc (1 key)
    virtual CircArc& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a CircArc (2 keys)
    virtual CircArc& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a CircArc
    virtual CircArc& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "CircArc::reflect3d(Point, Reals)", "CircArc::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a CircArc
    virtual CircArc& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "CircArc::reflect3d(Point, Real, Real, Real)", "CircArc::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
    
    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< parametrization c+(a-c)cos(t)+(b-c)sin(t)
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< inverse of parametrization c+(a-c)cos(t)+(b-c)sin(t)
};

CircArc operator~(const CircArc& e); //!< copy a CircArc and reverse its orientation
inline Vector<real_t> parametrization_CircArc(const Point& pt, Parameters& pars, DiffOpType d = _id)  //! extern parametrization call
{return reinterpret_cast<const CircArc*>(pars("geometry").get_p())->funParametrization(pt, pars, d);}
inline Vector<real_t> invParametrization_CircArc(const Point& pt, Parameters& pars, DiffOpType d = _id) //! extern invParametrization call
{return reinterpret_cast<const CircArc*>(pars("geometry").get_p())->invParametrization(pt, pars, d);}

//===================================================================================================
/*!
  \class SplineArc
  definition of a spline geometry in R^3

  SplineArc constructors are based on a key-value system. Here are the available keys:
  - _vertices: list of control points
  - _spline: spline object
  - _spline_type: type of spline one of _C2Spline, _CatmullRomSpline, _BSpline, _BezierSpline, _Nurbs
  - _spline_subtype: subtype of spline, one of _SplineInterpolation, _SplineApproximation
  - _spline_BC: spline boundary condition, one of _naturalBC, _clampedBC, _periodicBC
  - _degree: degree of spline (default 3)
  - _tension: tension factor for _CatmullRomSpline
  - _spline_parametrization: spline parametrization type, one of _xParametrization, _uniformParametrization,_chordalParametrization, _centripetalParametrization
  - _weights: weights of control points (for _BSpline only)
  - _tangent_0, _tangent_1: tangent vectors at the bounds of the arc (for _clampedBC only)
  - _nnodes: to define the number of nodes on the arc
  - _hsteps: to define the local mesh steps on the bounds
  - _domain_name: to define the domain name
  - _side_names/_vertex_names: to define the side names
  - _varnames: to define the variable names for print purpose
*/
//===================================================================================================
class SplineArc : public Curve
{
  protected:
    Spline* spline_; //!< pointer to spline object (see Spline class)
    
  public:
    SplineArc();                   //!< default constructor
    //@{
    //! key-value constructor
    SplineArc(Parameter p1);
    SplineArc(Parameter p1, Parameter p2);
    SplineArc(Parameter p1, Parameter p2, Parameter p3);
    SplineArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    SplineArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    SplineArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    SplineArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7);
    SplineArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8);
    //@}
    
    SplineArc(const SplineArc&);            //!< copy constructor
    SplineArc& operator=(const SplineArc&); //!< assign operator
    void copy(const SplineArc&);            //!< copy tool (hard copy)
    ~SplineArc();                           //!< destructor
    
    //! accessors
    const std::vector<Point>& controlPoints() const
    {
      if (spline_ == nullptr) { error("null_pointer", "spline_"); }
      return spline_->controlPoints();
    }
    
    std::vector<Point>& controlPoints()
    {
      if (spline_ == nullptr)  { error("null_pointer", "spline_"); }
      return spline_->controlPoints();
    }
    
    const Spline* splineP() const {return const_cast<const Spline*>(spline_);}
    Spline* splineP() {return spline_;}
    
    const Spline& spline() const
    {
      if (spline_ == nullptr) { error("null_pointer", "spline_"); }
      return const_cast<const Spline&>(*spline_);
    }
    
    Spline& spline()
    {
      if (spline_ == nullptr) { error("null_pointer", "spline_"); }
      return *spline_;
    }
    
    SplineType type() const
    { if (spline_ != nullptr) return spline_->type(); else return _noSpline; }
    
    SplineSubtype subtype() const
    { if (spline_ != nullptr) return spline_->subtype(); else return _noSplineSubtype; }
    
    number_t degree() const
    { if (spline_ != nullptr) return spline_->degree(); else return 0; }
    
    bool isClosed() const
    { if (spline_ != nullptr) return spline_->isClosed(); else return false; }
    
    std::vector<const Point*> boundNodes() const;
    std::vector<const Point*> nodes() const;
    number_t lastNodeIndex() const;
    std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const;
    
  private:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}
    
  public:
    string_t asString() const;
    virtual Geometry* clone() const { return new SplineArc(*this); } //!< virtual copy constructor for Geometry
    virtual void printDetail(std::ostream&) const; //!< print additional information
    
    //! access to child SplineArc object (const)
    virtual const SplineArc* splineArc() const {return this;}
    //! access to child SplineArc object
    virtual SplineArc* splineArc() {return this;}
    
    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a SplineArc
    virtual SplineArc& transform(const Transformation& t);
    //! apply a translation on a SplineArc (1 key)
    virtual SplineArc& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a SplineArc (vector version)
    virtual SplineArc& translate(std::vector<real_t> u)
    {
      warning("deprecated", "SplineArc::translate(Reals)", "SplineArc::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a SplineArc (3 reals version)
    virtual SplineArc& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "SplineArc::translate(Real, Real, Real)", "SplineArc::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a SplineArc (1 key)
    virtual SplineArc& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a SplineArc (2 keys)
    virtual SplineArc& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a SplineArc
    virtual SplineArc& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "SplineArc::rotate2d(Point, Real)", "SplineArc::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a SplineArc (1 key)
    virtual SplineArc& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a SplineArc (2 keys)
    virtual SplineArc& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a SplineArc (3 keys)
    virtual SplineArc& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a SplineArc
    virtual SplineArc& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "SplineArc::rotate3d(Point, Reals, Real)", "SplineArc::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a SplineArc
    virtual SplineArc& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "SplineArc::rotate3d(Real, Real, Real)", "SplineArc::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a SplineArc
    virtual SplineArc& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "SplineArc::rotate3d(Real, Real, Real, Real)", "SplineArc::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a SplineArc
    virtual SplineArc& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "SplineArc::rotate3d(Point, Real, Real, Real)", "SplineArc::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a SplineArc
    virtual SplineArc& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "SplineArc::rotate3d(Point, Real, Real, Real, Real)", "SplineArc::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a SplineArc (1 key)
    virtual SplineArc& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a SplineArc (2 keys)
    virtual SplineArc& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a SplineArc
    virtual SplineArc& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "SplineArc::homothetize(Point, Real)", "SplineArc::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a SplineArc
    virtual SplineArc& homothetize(real_t factor)
    {
      warning("deprecated", "SplineArc::homothetize(Real)", "SplineArc::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a SplineArc (1 key)
    virtual SplineArc& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a SplineArc
    virtual SplineArc& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "SplineArc::pointReflect(Point)", "SplineArc::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a SplineArc (1 key)
    virtual SplineArc& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a SplineArc (2 keys)
    virtual SplineArc& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a SplineArc
    virtual SplineArc& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "SplineArc::reflect2d(Point, Reals)", "SplineArc::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a SplineArc
    virtual SplineArc& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "SplineArc::reflect2d(Point, Real, Real)", "SplineArc::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a SplineArc (1 key)
    virtual SplineArc& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a SplineArc (2 keys)
    virtual SplineArc& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a SplineArc
    virtual SplineArc& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "SplineArc::reflect3d(Point, Reals)", "SplineArc::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a SplineArc
    virtual SplineArc& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "SplineArc::reflect3d(Point, Real, Real, Real)", "SplineArc::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
    
    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< interface to spline parametrization
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< interface to spline parametrization
};

SplineArc operator~(const SplineArc& s); //!< copy a SplineArc and reverse its orientation
inline Vector<real_t> parametrization_SplineArc(const Point& pt, Parameters& pars, DiffOpType d = _id)  //! extern parametrization call
{return reinterpret_cast<const SplineArc*>(pars("geometry").get_p())->funParametrization(pt, pars, d);}
inline Vector<real_t> invParametrization_SplineArc(const Point& pt, Parameters& pars, DiffOpType d = _id) //! extern invParametrization call
{return reinterpret_cast<const SplineArc*>(pars("geometry").get_p())->invParametrization(pt, pars, d);}

//===================================================================================================
/*!
  \class ParametrizedArc
  definition of a ParametrizedArc geometry in R^3 : x1(t), x2(t), x3(t)
  if an additional linear transformation T is applied to: T(x1(t),x2(t),x3(t)) = A(x1(t),x2(t),x3(t)) + b
                                                            where a=T.mat() and b=T.vec()

  ParametrizedArc constructors are based on a key-value system. Here are the available keys:
  - _tmin, _tmax: reals defining bounds of parameter t in parametrization
  - _parametrization: parametrization object is handled by Geometry
  - _partitioning: type of partition, one of _nonePartition*, _linearPartition, _splinePartition
  - _nbparts: number of partitions
  - _nnodes: to define the number of nodes on the Segment
  - _hsteps: to define the local mesh steps on the bounds of a Segment
  - _domain_name: to define the domain name
  - _side_names/_vertex_names: to define the side names
  - _varnames: to define the variable names for print purpose

   built at construction
     p1_,p2_, p_: bounds node and nodes
   As gmsh does not support parametrized curve, when exported to GMSH, ParametrizedArc is split into nbparts_
    - smaller segments (_linearPartition)
    - smaller splines (_splinePartition)
   Be caution, the parameter t should be taken in the interval [0,1] when using the function parametrization_ParametrizedArc
               whereas it should be taken in [tmin,tmax] when using the object arc_parametrization_
*/
//===================================================================================================
class ParametrizedArc : public Curve
{
  private:
    mutable real_t tmin_, tmax_;//!< bounds of parameter t in parametrization
    std::vector<Point> p_;      //!< vertices of the arc
    Partitioning partitioning_; //!< partitioning type  (one of _nonePartition*, _linearPartition _splinePartition)
    number_t nbParts_;          //!< number of partitions (>0)
    Transformation* transformation_;       //!< pointer to an additional transformation to apply to (0 by default: no additional linear transformation)
    Parametrization* arc_parametrization_; //!< Parametrization pointer related to arc (should be allocated)
    
  public:
    //! default constructor
    ParametrizedArc();
    //@{
    //! key-value constructor
    ParametrizedArc(Parameter p1, Parameter p2);
    ParametrizedArc(Parameter p1, Parameter p2, Parameter p3);
    ParametrizedArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4);
    ParametrizedArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
    ParametrizedArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
    //@}
    
  private:
    //@{
    //! true constructor functions
    void build(const std::vector<Parameter>& ps);
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    //@}
    
  public:
    //! copy constructor
    ParametrizedArc(const ParametrizedArc&);
    //! destructor
    ~ParametrizedArc() {if (transformation_ != nullptr) delete transformation_;}
    
    //@{
    //! accessor to parameter bounds
    real_t tmin() const {return tmin_; }
    real_t tmax() const {return tmax_; }
    //@}
    
    const std::vector<Point>& p() const {return p_;}
    //! return Partitioning
    Partitioning partitioning() const {return partitioning_;}
    //! return nb of partitions
    number_t nbParts() const {return nbParts_;}
    //! format as string
    string_t asString() const;
    
    //! reverse orientation of ParametrizedArc
    ParametrizedArc& reverse();
    
    virtual Geometry* clone() const { return new ParametrizedArc(*this); } //!< virtual copy constructor for Geometry
    
    virtual std::vector<Point*> nodes();                          //!< returns list of every node (non const)
    virtual std::vector<const Point*> nodes() const;              //!< returns list of every node (const)
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const; //!< returns list of curves (const)
    virtual number_t nbSides() const { return 2; }                //!< returns the number of sides
    real_t measure() const { return p1_.distance(p2_); }
    virtual bool isClosed() const                                //! true if last point is the same as first point
    { return dist(p1_, p2_) < theTolerance; }
    bool isPeriodic()
    {if (arc_parametrization_ != nullptr) return arc_parametrization_->periods[0] != 0; else return false;}
    //! access to child ParametrizedArc object (const)
    virtual const ParametrizedArc* parametrizedArc() const {return this;}
    //! access to child ParametrizedArc object
    virtual ParametrizedArc* parametrizedArc() {return this;}
    
    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a ParametrizedArc
    virtual ParametrizedArc& transform(const Transformation& t);
    //! apply a translation on a ParametrizedArc (1 key)
    virtual ParametrizedArc& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a ParametrizedArc (vector version)
    virtual ParametrizedArc& translate(std::vector<real_t> u)
    {
      warning("deprecated", "ParametrizedArc::translate(Reals)", "ParametrizedArc::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a ParametrizedArc (3 reals version)
    virtual ParametrizedArc& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "ParametrizedArc::translate(Real, Real, Real)", "ParametrizedArc::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a ParametrizedArc (1 key)
    virtual ParametrizedArc& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a ParametrizedArc (2 keys)
    virtual ParametrizedArc& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a ParametrizedArc
    virtual ParametrizedArc& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "ParametrizedArc::rotate2d(Point, Real)", "ParametrizedArc::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a ParametrizedArc (1 key)
    virtual ParametrizedArc& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a ParametrizedArc (2 keys)
    virtual ParametrizedArc& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a ParametrizedArc (3 keys)
    virtual ParametrizedArc& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a ParametrizedArc
    virtual ParametrizedArc& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "ParametrizedArc::rotate3d(Point, Reals, Real)", "ParametrizedArc::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a ParametrizedArc
    virtual ParametrizedArc& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "ParametrizedArc::rotate3d(Real, Real, Real)", "ParametrizedArc::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a ParametrizedArc
    virtual ParametrizedArc& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "ParametrizedArc::rotate3d(Real, Real, Real, Real)", "ParametrizedArc::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a ParametrizedArc
    virtual ParametrizedArc& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "ParametrizedArc::rotate3d(Point, Real, Real, Real)", "ParametrizedArc::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a ParametrizedArc
    virtual ParametrizedArc& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "ParametrizedArc::rotate3d(Point, Real, Real, Real, Real)", "ParametrizedArc::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a ParametrizedArc (1 key)
    virtual ParametrizedArc& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a ParametrizedArc (2 keys)
    virtual ParametrizedArc& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a ParametrizedArc
    virtual ParametrizedArc& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "ParametrizedArc::homothetize(Point, Real)", "ParametrizedArc::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a ParametrizedArc
    virtual ParametrizedArc& homothetize(real_t factor)
    {
      warning("deprecated", "ParametrizedArc::homothetize(Real)", "ParametrizedArc::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a ParametrizedArc (1 key)
    virtual ParametrizedArc& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a ParametrizedArc
    virtual ParametrizedArc& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "ParametrizedArc::pointReflect(Point)", "ParametrizedArc::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a ParametrizedArc (1 key)
    virtual ParametrizedArc& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a ParametrizedArc (2 keys)
    virtual ParametrizedArc& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a ParametrizedArc
    virtual ParametrizedArc& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "ParametrizedArc::reflect2d(Point, Reals)", "ParametrizedArc::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a ParametrizedArc
    virtual ParametrizedArc& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "ParametrizedArc::reflect2d(Point, Real, Real)", "ParametrizedArc::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a ParametrizedArc (1 key)
    virtual ParametrizedArc& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a ParametrizedArc (2 keys)
    virtual ParametrizedArc& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a ParametrizedArc
    virtual ParametrizedArc& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "ParametrizedArc::reflect3d(Point, Reals)", "ParametrizedArc::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a ParametrizedArc
    virtual ParametrizedArc& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "ParametrizedArc::reflect3d(Point, Real, Real, Real)", "ParametrizedArc::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }
    
    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< parametrization
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d = _id) const; //!< inverse of parametrization
};

ParametrizedArc operator~(const ParametrizedArc& s); //!< copy a ParametrizedArc and reverse its orientation
inline Vector<real_t> parametrization_ParametrizedArc(const Point& pt, Parameters& pars, DiffOpType d = _id)  //! extern parametrization call
{return reinterpret_cast<const ParametrizedArc*>(pars("geometry").get_p())->funParametrization(pt, pars, d);}
inline Vector<real_t> invParametrization_ParametrizedArc(const Point& pt, Parameters& pars, DiffOpType d = _id) //! extern invParametrization call
{return reinterpret_cast<const ParametrizedArc*>(pars("geometry").get_p())->invParametrization(pt, pars, d);}

//===================================================================================================
/*!
   \class SetOfPoints
   This class defines a set of points. The points are implicitly ordered according to their position in the vector.
   This allows the definition of a polygonal line made of successive segments from arbitrary points in 1D, 2D or 3D.
*/
//===================================================================================================
class SetOfPoints : public Geometry
{
  private:
    std::vector<Point> pts_; //!< list of points
    
  public:
    //! default constructor
    SetOfPoints(const std::vector<Point>& pts, const std::vector<string_t>& names)
      : Geometry(BoundingBox(pts), BoundingBox(pts).dim(), string_t("Omega"), _setofpoints), pts_(pts)
    { sideNames_ = names; }
    
    //! format as string
    virtual string_t asString() const;
    
    virtual Geometry* clone() const { return new SetOfPoints(*this); } //!< virtual copy constructor
    
    virtual std::vector<Point*> nodes(); //!< list of every point (non const)
    virtual std::vector<const Point*> nodes() const; //!< list of every point (const)
    real_t measure() const;
    
    // access functions to data members
    std::vector<Point>& pts() { return pts_; } //!< return list of points (non const)
    const std::vector<Point>& pts() const { return pts_; } //!< returns list of points (const)
    
    //! access to child SetOfPoints object (const)
    virtual const SetOfPoints* setofpoints() const { return this; }
    //! access to child SetOfPoints object
    virtual SetOfPoints* setofpoints() { return this; }
};

} // end of namespace xlifepp

#endif // GEOMETRIES_1D_HPP
