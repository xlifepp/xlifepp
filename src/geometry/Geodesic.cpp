/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Geodesic.cpp
  \authors E. Lunéville
  \since 30 aug 2021
  \date  24 nov 2021

  \brief Implementation of xlifepp::Geodesic classes members and related functions
*/

#include "Geodesic.hpp"
#include "GeomDomain.hpp"
#include "geometries1D.hpp"
#include "geometries2D.hpp"
#include "geometries3D.hpp"

namespace xlifepp
{
//===========================================================================================
//                Geodesic class handling any geodesic (base class)
//===========================================================================================
Geodesic::Geodesic(const Geodesic& geod)
{
  copy(geod);
}

void Geodesic::copy(const Geodesic& geod)
{
  dom_p=geod.dom_p;  //shared pointer
  geo_p=geod.geo_p;  //shared pointer
  par_p=geod.par_p;  //shared pointer
  type_=geod.type_;
  if(geod.xs_.size()>0)
  {
    xs_=geod.xs_;
    dxs_=geod.dxs_;
    curAbcs_=geod.curAbcs_;
    curvatures_=geod.curvatures_;
    normals_=geod.normals_;
  }
  withCurAbc=geod.withCurAbc;
  withTangent=geod.withTangent;
  withCurvatures=geod.withCurvatures;
  sx_=geod.sx_; sdx_=geod.sdx_;
  ex_=geod.ex_; edx_=geod.edx_;
  length_=geod.length_;
  meshgeod_=nullptr;
  if(type_!=_meshedGeodesic && geod.meshgeod_!=nullptr) meshgeod_=new MeshedGeodesic(*geod.meshgeod_);
  fcur_p=geod.fcur_p;
  fnor_p=geod.fnor_p;
}

void Geodesic::clear()
{
  if(xs_.size()>0) xs_.clear();
  if(dxs_.size()>0) dxs_.clear();
  if(curAbcs_.size()>0) curAbcs_.clear();
  if(curvatures_.size()>0) curvatures_.clear();
  if(normals_.size()>0) normals_.clear();
  if(field_.size()>0) field_.clear();
  if(fieldTypes_.size()>0) fieldTypes_.clear();
  sx_.clear();sdx_.clear();
  ex_.clear();edx_.clear();
  if(type_!=_meshedGeodesic && meshgeod_!=nullptr) delete meshgeod_;
}

Geodesic::~Geodesic()
{
  if(type_!=_meshedGeodesic && meshgeod_!=nullptr) delete meshgeod_;
}

Geodesic& Geodesic::operator=(const Geodesic& geod)
{
  clear();
  copy(geod);
  return *this;
}

//initialize par_p pointer regarding domain and geometry
void Geodesic::init()
{
  par_p=nullptr;
  if(dom_p!=nullptr)
  {
    if(geo_p==nullptr) geo_p = dom_p->geometry();
    if (dom_p->dim() == geo_p->dim()) par_p = const_cast<Parametrization*>(&geo_p->parametrization());
    if (dom_p->dim() == geo_p->dim()-1) par_p = const_cast<Parametrization*>(&geo_p->boundaryParametrization());
    return;
  }
  if(geo_p!=nullptr) par_p = const_cast<Parametrization*>(geo_p->parametrizationP());
  return;
}

/*! return curvatures (Gauss, mean, normal) if available
    if fcur_p is allocated use it first
    if parametrization of supporting geometry is available, use it
    else no access to curvatures, stop in error
    p: point where to take the curvatures
    dx: geodesic tangent
    fromParameters: if true p is a point in parameters space  else it is a point on manifold
*/
Vector<real_t> Geodesic::curvatures(const Point& p, const Point& dx, bool fromParameters) const
{
  if(fcur_p!=nullptr) return fcur_p(p,dx,fromParameters,const_cast<Parameters&>(params_));
  if(geo_p==nullptr)  error("free_error","Geodesic::curvatures not available (no explicit function and no supporting geometry)");
  if(par_p==nullptr)  error("free_error","Geodesic::curvatures not available (no Parametrization available)");
  if(fromParameters) return par_p->curvatures(p,dx);
  Point q=par_p->toParameter(p);
  if(q.size()==0) error("free_error","Geodesic::curvatures fails (toParameter fails)");
  return par_p->curvatures(q,dx);
}

/*! return normal to surface normal at a point if available
    if fnor_p is allocated use it first
    if parametrization of supporting geometry is available, use it
    else no access to normal, stop in error
    p: point where to take the curvatures
    fromParameters: if true p is a point in parameters space  else it is a point on manifold
*/
Vector<real_t> Geodesic::normal(const Point& p, bool fromParameters) const
{
  if(fnor_p!=nullptr) return fnor_p(p,fromParameters,const_cast<Parameters&>(params_));
  if(geo_p==nullptr)  error("free_error","Geodesic::normal not available (no explicit function and no supporting geometry)");
  if(par_p==nullptr)  error("free_error","Geodesic::normal not available (no Parametrization available)");
  if(fromParameters) return par_p->normal(p,_id);
  Point q=par_p->toParameter(p);
  if(q.size()==0) error("free_error","Geodesic::normal fails (toParameter fails)");
  return par_p->normal(q);
}

/*! build the projection of the geodesic onto the domain mesh
    create a MeshedGeodesic stored using pointer meshgeod_
    and attach Geodesic information to each GeomElement crossed by the Geodesic
    use GeomElement::userData_p to store a list<pair<Geodesic*, number_t>>
    several Geodesic's may be concerned
    Geodesic has to be computed before
    useless for MeshedGeodesic (already done by compute)
    update xs_ and may be dxs_ and curAbcs_, curvatures_
*/
void Geodesic::buildMeshedGeodesic(const GeomDomain& dom, bool trace)
{
  typedef std::list<std::pair<MeshedGeodesic*,number_t> > listGeodNum;
  if(type_==_meshedGeodesic || xs_.size()==0) return; //nothing to do
  if(meshgeod_!=nullptr) delete meshgeod_;
  meshgeod_=new MeshedGeodesic(const_cast<GeomDomain&>(dom),withCurAbc,withTangent);
  std::list<Point> ys;             //new list
  std::list<Point> dys;            //new list
  std::list<Point> ns;             //new list
  std::list<real_t> cas;           //new list
  std::list<Vector<real_t> > cus;  //new list
  std::list<GeoNumPair> elts;      //new list
  std::vector<Point>::const_iterator itp=xs_.begin();
  std::vector<Point>::const_iterator itdp=dxs_.begin();
  std::vector<Point>::const_iterator itn=normals_.begin();
  std::vector<Vector<real_t> >::const_iterator itcu=curvatures_.begin();
  number_t i=0, s, sp;
  Point P=*itp++, Q=P, dP=(*itp-P)/norm(*itp-P), dQ, I, n, pQ, PQ, nP, nQ;
  --itp;
  Vector<real_t> cuP,cuQ;
  meshgeod_->sx_=*itp; meshgeod_->sdx_=sdx_;
  ys.push_back(*itp);
  if(withTangent) {dys.push_back(*itdp); nP=*itn; ns.push_back(nP); nQ=nP;}
  if(withCurAbc) cas.push_back(*curAbcs_.begin());
  if(withCurvatures) {cuP=*curvatures_.begin(); cus.push_back(cuP); cuQ=cuP;}
  bool hasInter;
  real_t distance, nPQ, nPI, l=0;
  number_t nt=currentThread();
  if(trace) thePrintStream<<" buildMeshedGeodesic, starting point="<<P<<" starting dir="<<dP<<eol<<std::flush;
  //find starting element
  GeomElement* gelt = meshgeod_->locateStartingElement(P,dP,s);
  if(gelt==nullptr) error("free_error","Geodesic::trackingGeomElement fails because starting element not found");
  static_cast<listGeodNum*>(gelt->userData_ps[nt])->push_back(std::make_pair(meshgeod_,i));
  elts.push_back(GeoNumPair(gelt,i));
  ++itp; Q=*itp;
  if(withTangent) {++itdp; dQ=*itdp; ++itn; nQ=*itn;}
  if(withCurvatures) {++itcu; cuQ=*itcu;}
  bool withnPQ=withCurAbc || withTangent || withCurvatures;
  //main loop on current geodesic point
  while(itp!=xs_.end())
  {
     if(trace) thePrintStream<<"P="<<P<<" Q="<<Q<<" gelt="<<gelt<<" vertices: "<<gelt->vertex(1)<<" "<<gelt->vertex(2)<<" "<<gelt->vertex(3);
     const std::vector<real_t>& n=gelt->normalVector();
     pQ=Q-dot(n,Q-P)*n;  //projection of Q on gelt plane (fails if pQ=P, not checked)
     if(trace) thePrintStream<<" n="<<n<<" pQ="<<pQ<<std::flush;
     if(withnPQ) nPQ=norm(P-pQ);
     for(s=1;s<=3;s++)   //check if [P,pQ] crosses one side of gelt
     {
       sp=(s==3?1:s+1);
       if(norm(P-gelt->vertex(s))>theTolerance && norm(P-gelt->vertex(sp))>theTolerance) // P not vertex s or s+1
       {
         hasInter=intersectionOfSegments(P,pQ, gelt->vertex(s),gelt->vertex(sp),I,theTolerance) && norm(I-P)>theTolerance;
         if(hasInter) break;
       }
     }
     if(hasInter)
     {
       if(trace) thePrintStream<<" inter I="<<I;
       ys.push_back(I);
       real_t nPI=norm(P-I);
       if(withTangent) {dys.push_back(dP+(nPI/nPQ)*(dQ-dP)); ns.push_back(nP+(nPI/nPQ)*(nQ-nP));}   //linear interpolation
       l+=nPI;
       if(withCurAbc)  cas.push_back(l);
       if(withCurvatures) cus.push_back(cuP+(nPI/nPQ)*(cuQ-cuP)); //linear interpolation ! may be better if using parametrization if available
       i++;
       static_cast<listGeodNum*>(gelt->userData_ps[nt])->push_back(std::make_pair(meshgeod_,i));
       P=I;
       GeomElement* ngelt=nullptr;
       PQ=Q-P;
       for(number_t k=1;k<=3;k++)   //check if P is a vertex
       {
         if(norm(P-gelt->vertex(k))<theTolerance)  //P=vertex(k)
         {
            ngelt=meshgeod_->locateElementFromVertex(P,PQ,sp,gelt); // locate next gelt using special process
            if(trace) thePrintStream<<" on vertex ngelt="<<ngelt;
            break;
         }
       }
       if(ngelt!=nullptr) gelt=ngelt;
       else gelt=gelt->elementSharingSide(s).first; //update gelt
       if(gelt==nullptr) break;  //stop cannot continue tracking
     }
     else //stay in element
     {
       //check that pQ is really in gelt, if not project on current triangle
       real_t h=0;
       pQ=projectionOnTriangle(pQ,gelt->vertex(1),gelt->vertex(2),gelt->vertex(3),h);
       if(trace)
       {if(h>theTolerance) thePrintStream<<" active projection on triangle";
         thePrintStream<<" stay pQ="<<pQ<<std::flush;}
       ys.push_back(pQ);
       if(withTangent) {dys.push_back(dQ);ns.push_back(nQ);}
       l+=nPQ;
       if(withCurAbc) cas.push_back(l);
       if(withCurvatures) cus.push_back(cuQ);
       i++;
       //update info
       static_cast<listGeodNum*>(gelt->userData_ps[nt])->push_back(std::make_pair(meshgeod_,i));
       P=pQ;
       ++itp;
       if(itp==xs_.end()) break;
       Q=*itp;
       if(withTangent)
       {
         dP=dQ; ++itdp; dQ=*itdp;
         nP=nQ; ++itn;  nQ=*itn;
       }
       if(withCurvatures){cuP=cuQ;++itcu;cuQ=*itcu;}
       //check if P is a vertex
       GeomElement* ngelt=nullptr; PQ=Q-P; bool stop=false;
       for(number_t k=1;k<=3;k++)
       {
         if(norm(P-gelt->vertex(k))<theTolerance)  //P=vertex(k)
         {
            ngelt=meshgeod_->locateElementFromVertex(P,PQ,sp,gelt); // locate next gelt using special process
            if(ngelt!=nullptr) gelt=ngelt; else stop = true;              // tracking fails, stop on vertex and no gelt found
            if(trace) thePrintStream<<" on vertex, update gelt="<<ngelt;
            break;
         }
       }
       if(stop) break;
     }
     elts.push_back(GeoNumPair(gelt,i));
     if(trace) thePrintStream<<eol<<std::flush;
  }
  // create vectors from lists
  meshgeod_->xs_.assign(ys.begin(),ys.end());
  meshgeod_->elts_.assign(elts.begin(),elts.end());
  if(withTangent)
  {
    meshgeod_->dxs_.assign(dys.begin(),dys.end());
    meshgeod_->normals_.assign(ns.begin(),ns.end());
  }
  if(withCurAbc) meshgeod_->curAbcs_.assign(cas.begin(),cas.end());
  if(withCurvatures) meshgeod_->curvatures_.assign(cus.begin(),cus.end());
  meshgeod_->ex_=ys.back();
  if(withTangent) meshgeod_->edx_=dys.back();
  meshgeod_->length_=l;
}

void Geodesic::print(std::ostream& os) const
{
  os<<"Geodesic";
  printData(os);
}

void Geodesic::printData(std::ostream& os) const
{
  if(dom_p!=nullptr) os<<" on "<<dom_p->name(); else os<<"geodesic (no domain)";
  if(geo_p!=nullptr) os<<", on geometry "<<geo_p->domName(); else os<<", no geometry";
  if(par_p!=nullptr) os<<" parametrized";
  os<<", starts  at x="<<sx_<<" dx="<<sdx_<<", nb points="<<xs_.size();
  os<<", ends at x="<<ex_<<" dx="<<edx_<<", length="<<length_;
  if(meshgeod_!=nullptr) os<<", has a related meshed geodesic";
  if(fcur_p!=nullptr) os<<", curvatures function assigned";
  if(fnor_p!=nullptr) os<<", normal function assigned";
  if(xs_.size()==0) {os<< " not computed"<<eol; return;}
  theCout<<eol;
  if(theVerboseLevel<2) return;
  os<<" xs = "<<xs_<<eol;
  os<<" dxs = "<<dxs_<<eol;
  os<<" normals = "<<normals_<<eol;
  os<<" curAbcs = "<<curAbcs_<<eol;
  os<<" curvatures (Gauss, mean, normal) = "<<curvatures_<<eol;
}

/*!export geodesic data as
   raw xyz format: xi yi zi (useful for matlab)
   csv format: column header x,y,z and xi,yi,zi (useful for paraview)
*/
void Geodesic::saveToFileG(const string_t& file) const
{
   std::pair<string_t, string_t> namext= fileRootExtension(file);
   bool csv = namext.second=="csv";
   std::vector<Point>::const_iterator itp;
   string_t filename, sep=" ";
   std::ofstream out;
   if(xs_.size()!=0)
   {
     filename=namext.first+"_x."+namext.second;
     out.open(filename.c_str());
     if(csv){out<<"x,y,z"<<eol;sep=",";}
     for(itp=xs_.begin(); itp!=xs_.end();++itp)
       out<<(*itp)[0]<<sep<<(*itp)[1]<<sep<<(*itp)[2]<<std::endl;
     out.close();
   }
   if(dxs_.size()!=0)
   {
     filename=namext.first+"_dx."+namext.second;
     out.open(filename.c_str());
     if(csv){out<<"x,y,z"<<eol;sep=",";}
     for(itp=dxs_.begin(); itp!=dxs_.end();++itp)
       out<<(*itp)[0]<<sep<<(*itp)[1]<<sep<<(*itp)[2]<<std::endl;
     out.close();
   }
   if(curAbcs_.size()!=0)
   {
     filename=namext.first+"_abc."+namext.second;
     out.open(filename.c_str());
     if(csv){out<<"abc"<<eol;sep=",";}
     std::vector<real_t>::const_iterator it = curAbcs_.begin();
     for(; it!=curAbcs_.end();++it)
       out<<(*it)<<std::endl;
     out.close();
   }
   if(curvatures_.size()!=0)
   {
     filename=namext.first+"_cur."+namext.second;
     out.open(filename.c_str());
     if(csv){out<<"x,y,z"<<eol;sep=",";}
     std::vector<Vector<real_t> >::const_iterator itv = curvatures_.begin();
     for(; itv!=curvatures_.end();++itv)
       out<<(*itv)[0]<<sep<<(*itv)[1]<<sep<<(*itv)[2]<<std::endl;
     out.close();
   }
}

//===========================================================================================
//         ParametrizedGeodesic class handling any geodesic got using parametrization
//===========================================================================================
ParametrizedGeodesic::ParametrizedGeodesic(GeomDomain& dp, bool wCA, bool wT, bool wC)
: Geodesic(dp, wCA, wT, wC)
{
   if(par_p==nullptr) error("free_error","ParametrizedGeodesic: unable to find a parametrization!");
}

ParametrizedGeodesic::ParametrizedGeodesic(Geometry& gp, bool wCA, bool wT, bool wC)
: Geodesic(gp, wCA, wT, wC)
{
   if(par_p==nullptr) error("free_error","ParametrizedGeodesic: unable to find a parametrization!");
}

ParametrizedGeodesic::ParametrizedGeodesic(const Parametrization& pa, bool wCA, bool wT, bool wC)
{
   withCurAbc=wCA;
   withTangent=wT;
   withCurvatures=wC;
   dom_p=new GeomDomain("virtual domain");
   geo_p=nullptr;
   par_p=const_cast<Parametrization*>(&pa); // shared pointer !!!
   fcur_p=nullptr; fnor_p=nullptr;
   meshgeod_=nullptr;
   length_=0;
}

ParametrizedGeodesic::ParametrizedGeodesic(Geometry& geom, const Point& x, const Point& dx, real_t lmax,
                                            number_t nmax, real_t dt, bool wCA, bool wT, bool wC)
: Geodesic(geom, wCA, wT, wC)
{
   if(par_p==nullptr) error("free_error","ParametrizedGeodesic: unable to find a parametrization!");
   real_t l=0;
   Point xx=x, dxx=dx;
   compute(xx,dxx,l,lmax,nmax, dt);
}

void ParametrizedGeodesic::clear()
{
  Geodesic::clear();
  if(us_.size()>0) us_.clear();
  if(dus_.size()>0) dus_.clear();
}

// geodesic function, use par parametrization is not 0 else use intern par_p parametrization
Point ParametrizedGeodesic::F(const Point& u, const Point& d, const Parametrization* par) const
{
	Vector<real_t> g;
	if(par==nullptr) g=par_p->christoffel(u); else g=par->christoffel(u);
	real_t	du = d[0], dv = d[1], dudv=du*dv, dudu=du*du, dvdv=dv*dv;
	Point ddu( g[0]*dudu + 2*g[1]*dudv + g[3]*dvdv,
			   g[4]*dudu + 2*g[5]*dudv + g[7]*dvdv);
//	Point ddu( du*g[0]*du + du*g[1]*dv + dv*g[2]*du + dv*g[3]*dv,
//			   du*g[4]*du + du*g[5]*dv + dv*g[6]*du + dv*g[7]*dv);
	return -ddu;
}

// compute geodesic using RK4 with n points and fixed step - Single parametrization on [0,1]x[0,1]
//  x: starting point in R3 space
//  dx: starting direction in R3 space
//  l: current curvilinear length of geodesic
//  lmax: stop if reached (lmax<=0 means no limitation)
//  nmax: number max of computed points
//  dt: step of RK4
//  update us_, xs_ list
//  update dus_, dxs_ list if required
//  update curAbcs_ list if required
Geodesic& ParametrizedGeodesic::compute(Point& x, Point& dx, real_t& l,real_t lmax, number_t nmax, real_t dt)
{
 if(par_p->isPiecewise()&& par_p->contOrder==_notRegular) return computePiecewise(x,dx,l,lmax,nmax,dt);
 sx_=x; sdx_=dx;
 real_t l0=l;
 xs_.push_back(x);
 if (withTangent) dxs_.push_back(dx);
 Point uv=par_p->toParameter(x);
 Point duv=par_p->toDerParameter(uv,dx);
 us_.push_back(uv);
 if(withTangent) {dus_.push_back(duv); normals_.push_back(par_p->normal(uv));}
 if(withCurAbc) curAbcs_.push_back(l0);
 if(withCurvatures)  curvatures_.push_back(par_p->curvatures(uv,dx));
 number_t k=0;
 compute(*par_p,uv,duv,l,lmax,k,nmax,dt);
 length_=l-l0;
 ex_=x; edx_=dx;
 return *this;
}

// check if p in [0,1]x[0,1] and project onto; return true if no change
// o: original point using to make a directionnal op projection (i=0 or i=1):
//     q = p + a(o-p) with a=(i-p[i])/(o[i]-p[i])
// on output: p = q
bool ParametrizedGeodesic::checkBound(Point &p, const Point& o, const Parametrization& par, real_t tol) const
{
  bool in=true;
  real_t a;
  if(par.periods[0]==0)  // not u-periodic
  {
    if(p[0]<0){a=-p[0]/(o[0]-p[0]); p+=a*(o-p);in=false;}
    if(p[0]>1){a=(1-p[0])/(o[0]-p[0]); p+=a*(o-p);in=false;}
  }
  else // u-periodic
  {
    if(p[0]<0)  p[0]+=par.periods[0];
    if(p[0]>1)  p[0]-=par.periods[0];
  }
  if(par.periods[1]==0)  // not v-periodic
  {
    if(p[1]<0){a=-p[1]/(o[1]-p[1]); p+=a*(o-p);in=false;}
    if(p[1]>1){a=(1-p[1])/(o[1]-p[1]); p+=a*(o-p);in=false;}
  }
  else // v-periodic
  {
    if(p[1]<0)  p[1]+=par.periods[1];
    if(p[1]>1)  p[1]-=par.periods[1];
  }
  return in;
}

// check if p in [0,1]x[0,1] and project onto u axis or v axis; return true if no change
// p is updated
bool ParametrizedGeodesic::checkBound(Point &p, const Parametrization& par, real_t tol) const
{
  bool in=true;
  real_t a;
  if(par.periods[0]==0)  // not u-periodic
  {
    if(p[0]< tol)  {p[0]=0;in=false;}
    if(p[0]>1-tol) {p[0]=1;in=false;}
  }
  else // u-periodic
  {
    if(p[0]<0)  p[0]+=par.periods[0];
    if(p[0]>1)  p[0]-=par.periods[0];
  }
  if(par.periods[1]==0)  // not v-periodic
  {
    if(p[1]<tol)   {p[1]=0;in=false;}
    if(p[1]>1-tol) {p[1]=1;in=false;}
  }
  else // u-periodic
  {
    if(p[1]<0)  p[1]+=par.periods[1];
    if(p[1]>1)  p[1]-=par.periods[1];
  }
  return in;
}
//compute Geodesic with regular parametrization (par)
Geodesic& ParametrizedGeodesic::compute(const Parametrization& par, Point& uv, Point& duv,
                                        real_t& l, real_t lmax, number_t& k, number_t n, real_t dt)
{//RK4 loop
 std::list<Point> xs;              //new list
 std::list<Point> dxs;             //new list
 std::list<Point> normals;         //new list
 std::list<real_t> curAbcs;        //new list
 std::list<Vector<real_t> > curvs; //new list
 std::list<Point> us;              //new list
 std::list<Point> dus;             //new list
 Point du1, ddu1, du2, ddu2, du3, ddu3, du4, ddu4, nuv, nduv, tuv, dx, x0=par(uv);;
 bool change = false, first=true;
 while(k<n && !change)
 {
   // RK4 step
   du1 = duv;
   ddu1 = F(uv, du1, &par);
   nuv = uv + du1*dt/2;
   if(checkBound(nuv,uv,par))
   {
     du2 = duv + ddu1*dt/2;
     ddu2 = F(nuv, du2, &par);
     nuv = uv + du2*dt/2;
     if(checkBound(nuv,uv,par))
     {
       du3 = duv + ddu2*dt/2;
       ddu3 = F(nuv, du3, &par);
       nuv = uv + du3*dt;
       if(checkBound(nuv,uv,par))
       {
         du4 = duv + ddu3*dt;
         ddu4 = F(nuv, du4, &par);
         nuv = uv + ( du1 + 2* du2 + 2* du3 +  du4) * dt/6;
         if(checkBound(nuv,uv,par)) nduv = duv + (ddu1 + 2*ddu2 + 2*ddu3 + ddu4) * dt/6;
         else change=true;
       }
       else change=true;
     }
     else change=true;
   }
   else change=true;
   // update uv and duv

   if(change) // update duv using Euler step
   {
     if(!checkBound(uv,uv,par,theTolerance)) return *this; //uv on boundary, choose other parametrization
     dt = dot(nuv-uv,du1)/dot(du1,du1);
     duv+=dt*ddu1;
   }
   else duv=nduv;
   uv=nuv;    //i n [0,1]x[0,1] by construction
   k++;
   // update vectors us_,xs_, ...
   us.push_back(uv);
   Point xn= par(uv);
   if(first) {l+=norm(xn-x0); first=false;} else l+=norm(xn-xs.back());  // update curvilinear length
   xs.push_back(xn);
   dx=duv[0]*par(uv,_d1)+duv[1]*par(uv,_d2);
   if(withTangent) { dus.push_back(duv); dxs.push_back(dx); normals.push_back(par.normal(uv));}
   if(withCurvatures) curvs.push_back(par.curvatures(uv,dx));
   if(withCurAbc) curAbcs.push_back(l);
   //theCout<<"  iter "<<k<<": u="<<uv<<" duv="<<duv<<" x="<<xn<<" dx="<<dx<<" l="<<l<<eol<<std::flush;
   if(lmax>0 && l>lmax) break;
 }
 //update ex and edx
 ex_=xs.back();
 edx_= duv[0]*par(uv,_d1)+duv[1]*par(uv,_d2);
 //insert list into vector
 xs_.insert(xs_.end(), xs.begin(), xs.end());
 us_.insert(us_.end(), us.begin(), us.end());
 if(withTangent)
 {
   dxs_.insert(dxs_.end(), dxs.begin(), dxs.end());
   dus_.insert(dus_.end(), dus.begin(), dus.end());
   normals_.insert(normals_.end(), normals.begin(), normals.end());
 }
 if(withCurAbc) curAbcs_.insert(curAbcs_.end(), curAbcs.begin(), curAbcs.end());
 if(withCurvatures) curvatures_.insert(curvatures_.end(), curvs.begin(), curvs.end());
 return *this;
}

//compute geodesic when a piecewise discontinuous parametrization
Geodesic& ParametrizedGeodesic::computePiecewise(Point& x, Point& dx, real_t& l, real_t lmax, number_t n, real_t dt)
{
  if(!par_p->isPiecewise()) return compute(x,dx,lmax,l,n,dt);
  real_t l0=l, d=10*dt;
  clear();
  //std::ofstream out("dxtrans.dat");
  sx_=x;sdx_=dx;
  xs_.push_back(x);
  if (withTangent) dxs_.push_back(dx);
  number_t k=0, pk, trypar, trymax;
  const PiecewiseParametrization* ppar=static_cast<const PiecewiseParametrization*>(par_p);
  //locate cell containing x
  Point uv, px=x, pdx=dx;
  Parametrization* locpar = ppar->locateParametrization(x,dx,0,uv,d);
  //theCout<<"init ParametrizedGeodesic: "<<" x0="<<x<<" dx0="<<dx<<" l0="<<l<<eol;
  if(locpar==nullptr)
    error("free_error","ParametrizedGeodesic::computePiecewise: local parametrization not found, x may not be on manifold");
  Point duv=locpar->toDerParameter(uv,dx);
  //theCout<<"-> "<<(*locpar)<<eol<<"  uv0="<<uv<<" duv0="<<duv<<" x="<<x<<" dx="<<dx<<eol<<std::flush;
  us_.push_back(uv);
  if(withTangent) {dus_.push_back(duv); normals_.push_back(locpar->normal(uv));}
  if(withCurAbc) curAbcs_.push_back(l0);
  if(withCurvatures) curvatures_.push_back(locpar->curvatures(uv,dx));

  while(true) // main loop on parametrization
  {
//   theCout<<(*locpar)<<eol<<"  uv0="<<uv<<" duv0="<<duv<<" x0="<<x<<" dx0="<<dx<<" l0="<<l<<eol<<std::flush;
//    out<<x[0]<<" "<<x[1]<<" "<<x[2]<<eol;
//    out<<x[0]+dx[0]<<" "<<x[1]+dx[1]<<" "<<x[2]+dx[2]<<eol;
    pk=k;
    compute(*locpar,uv,duv,l,lmax,k,n,dt);
    if(k>=n ||(lmax>0 && l>lmax)) break;
    //update x, dx
    if(k==pk) {x=px;dx=pdx;} // retry with same starting x,dx
    else
    {
      x=(*locpar)(uv);
      dx= duv[0]*(*locpar)(uv,_d1)+duv[1]*(*locpar)(uv,_d2);
      px=x; pdx=dx;
      trypar=0; trymax=2;
      if(locpar->onSingularSide(uv)) trymax=3;
    }
    if(!locpar->onSingularSide(uv)) // not on singularity
    {
      std::list<number_t> ss;
      Point n1; //compute n1 (outward normal to patch in tangential plane)
      if(std::abs(uv[0])<theTolerance)  {ss.push_back(3);n1=-(*locpar)(uv,_d1);}
      if(std::abs(uv[0]-1)<theTolerance){ss.push_back(1);n1= (*locpar)(uv,_d1);}
      if(std::abs(uv[1])<theTolerance)  {ss.push_back(0);n1=-(*locpar)(uv,_d2);}
      if(std::abs(uv[1]-1)<theTolerance){ss.push_back(2);n1= (*locpar)(uv,_d2);}
      if(n1.size()>0) n1/=norm(n1);
      //locate neighbor parametrization
      std::list<number_t>::iterator its=ss.begin();
      while(its!=ss.end())
      {
        //theCout<<" search "<<x<<" in neighbors "<<ppar->neighborParsMap.at(locpar)<<" side="<<(*its)<<std::flush;
        locpar = ppar->neighborParsMap.at(locpar)[*its].first;
        if(locpar==nullptr) break;   // no neighbor (touch boundary)
        //theCout<<" neighbor "<<(*locpar)<<std::flush;
        trypar++;
        uv=locpar->toParameter(x);
        //theCout<<" uv="<<uv<<eol<<std::flush;
        if(uv.size()>0) break; // uv ok
        ++its;
      }
      if(trypar==2) break;
      if(uv.size()==0) break;   // no way to go on
      // project using rotation, compute n2 (outward normal to patch in tangential plane)
      Point n2;
      if(std::abs(uv[0])<theTolerance)  n2 =-(*locpar)(uv,_d1);
      if(std::abs(uv[0]-1)<theTolerance)n2 = (*locpar)(uv,_d1);
      if(std::abs(uv[1])<theTolerance)  n2 =-(*locpar)(uv,_d2);
      if(std::abs(uv[1]-1)<theTolerance)n2 = (*locpar)(uv,_d2);
      if(n2.size()>0) n2/=norm(n2);
      //theCout<<"  n1="<<n1<<" n2="<<n2;
      dx -= dot(dx,n1)*(n1+n2);
      //theCout<<" new dx="<<dx<<eol<<std::flush;
    }
    else trypar++;
    if(trypar==trymax) break;
    duv=locpar->toDerParameter(uv,dx);
  }
  //out.close();
  length_=l-l0;
  ex_=x; edx_=dx;
  return *this;
}

void ParametrizedGeodesic::print(std::ostream& os) const
{
  os<<"Parametrized geodesic";
  printData(os);
  if(theVerboseLevel<2) return;
  os<<" us = "<<us_<<eol;
  os<<" dus = "<<dus_<<eol;
}

void ParametrizedGeodesic::saveToFile(const string_t& file) const
{
  if(us_.size()==0) return;
  string_t filename=file+"_u.dat";
  std::ofstream out(filename.c_str());
  std::vector<Point>::const_iterator itp = us_.begin();
  for(; itp!=us_.end();++itp)
    out<<(*itp)[0]<<" "<<(*itp)[1]<<std::endl;
  out.close();
  if(dus_.size()!=0)
  {
    filename=file+"_du.dat";
    out.open(filename.c_str());
    for(itp=dus_.begin(); itp!=dus_.end();++itp)
      out<<(*itp)[0]<<" "<<(*itp)[1]<<std::endl;
    out.close();
  }
  Geodesic::saveToFileG(file);
}

//===========================================================================================
//              MeshedGeodesic class handling any geodesic got using mesh
//===========================================================================================
void MeshedGeodesic::saveToFile(const string_t& file, bool withElts) const
{
  Geodesic::saveToFileG(file);
  if(elts_.size()==0 || !withElts) return;
  string_t filename=file+"_elt.dat";
  std::ofstream out(filename.c_str());
  std::vector<GeoNumPair>::const_iterator ite=elts_.begin();
  for(; ite!=elts_.end();++ite)
  {
    GeomElement* gelt = ite->first;
    for(number_t i=1;i<=gelt->numberOfVertices();i++)
    {
      const Point& vi=gelt->vertex(i);
      for(number_t k=0;k<vi.size();k++)
          out<<vi[k]<<" ";
      out<<std::endl;
    }
  }
 out.close();
}

void MeshedGeodesic::clear()
{
  Geodesic::clear();
  if(elts_.size()>0) elts_.clear();
}

// computes a geodesic on a 2D manifold previously meshed, from de 3D point x with 3D direction d
//  x: starting point in 3D space (on manifold, not check here)
//  d: starting direction in 3D space (not 0),  projected onto the tangent plane (here)
//  l: curvilinear length at end of computation
//  lmax: curvilinear length of geodesic, stop if reached (l<=0 means no limitation)
//  nbp: number max of computed points
//  dt: unused
//  update xs_ vector (size of xs gives the effective number of computed points
//  update dxs_ vector if required
//  update curAbcs_ vector if required
//
//  NOTE: assuming triangle element
Geodesic& MeshedGeodesic::compute(Point& x, Point& dx, real_t& l, real_t lmax, number_t nbp, real_t dt)
{
 clear();
 real_t l0=l;
 sx_=x;sdx_=dx;
 GeomDomain& sdom=dom_p->sidesDomain(); //create side domain (set of all edges) if not exist
 MeshDomain* mdom=dom_p->meshDomain();
 dom_p->setNormalOrientation();         // orientation of surface
 number_t s=0;
 GeomElement* gelt=locateStartingElement(x,dx,s);
 if(gelt==nullptr)  // locate starting gelt fails
 {
   warning("free_warning","MeshedGeodesic::compute stops, starting element not found");
   return *this;
 }
 elts_.push_back(GeoNumPair(gelt,s));
 number_t n=0;
 return compute(x, dx, gelt, l, lmax, n, nbp, dt);
 length_=l-l0;
 ex_=x; edx_=dx;
 return *this;
}

//effective computation function
Geodesic& MeshedGeodesic::compute(Point& x, Point& dx, GeomElement* gelt, real_t& l, real_t lmax, number_t& n, number_t nbp, real_t dt)
{
 std::list<Point> xs;             //new list
 std::list<Point> dxs;            //new list
 std::list<Point> normals;        //new list
 std::list<real_t> curAbcs;       //new list
 std::list<Vector<real_t> > curvs;//new list
 std::list<GeoNumPair> elts;      //new list
 number_t side = 0; // relative index of crossed edge (undefined when starting)
 Point nor = gelt->normalVector(); nor /= norm(nor);	// normal vector of the starting element
 Point w, n1, n2, P1P2, P1x, xx;
 dx -= dot(dx, nor)*nor;				// projection of the velocity on the starting element (tangent vector space)
 if (dot(dx,dx) < theTolerance) error("free_error","MeshedGeodesic::compute: the velocity is quasi-orthogonal to the surface !");
 if(lmax<=0 && nbp==0)   error("free_error","MeshedGeodesic::compute: specify nbp>0 or l>0");
 if(nbp==0) nbp=std::floor((lmax/gelt->size()))+1; // when nbp=0, use lmax and element size to estimates nbp
 xs.push_back(x);
 if(withTangent) {dxs.push_back(dx); normals.push_back(gelt->normalVector());}  //use gelt normal, may be using parametrization should be better
 if(withCurAbc) curAbcs.push_back(0.);
 if(withCurvatures) curvs.push_back(curvatures(x,dx,false));
 number_t i=0;
 //theCout << "\n  iter "<<i<<": x="<<x <<" d="<< dx <<" gelt: "<<(*gelt)<<eol<<std::flush;

 // init side if x is on one side
 for (number_t s = 1; s <= gelt->numberOfSides(); s++)
 {
   const Point& P1 = gelt->vertex(1, s);  // vertices of side s
   const Point& P2 = gelt->vertex(2, s);
   P1P2=P2-P1; P1x=x-P1;
   if(norm(cross3D(P1P2,P1x))<theTolerance) // P1,P2,x aligned
   {
     real_t a=dot(x-P1,P1P2)/dot(P1P2,P1P2);
     if(a>=-theTolerance && a<=1+theTolerance) {side=s;break;}
   }
  }
 //main computation loop
 while(i<nbp)
 {// crossing using straight line,  side = starting edge index,  newSide = ending edge index (to be found)
  if(gelt==nullptr) break; // next gelt not found, stop the geodesic !
//  theCout << *gelt << eol << "elt vertices [" << gelt->vertex(1)[0] << " " << gelt->vertex(2)[0]<<" "<< gelt->vertex(3)[0]<<"],["
//  << gelt->vertex(1)[1] << " " << gelt->vertex(2)[1]<<" "<< gelt->vertex(3)[1]<<"],["
//  << gelt->vertex(1)[2] << " " << gelt->vertex(2)[2]<<" "<< gelt->vertex(3)[2]<<"]"<<eol<<std::flush;
  if(gelt->userData_p==nullptr)
    gelt->userData_p = static_cast<void *>(new std::list<std::pair<Geodesic*,number_t> >(std::list<std::pair<Geodesic*,number_t> >(1,std::make_pair(this,i))));
  else static_cast<std::list<std::pair<Geodesic*,number_t> >*>(gelt->userData_p)->push_back(std::make_pair(this,i));
  w=cross3D(dx,gelt->normalVector()); // w: normal to dx in (P1,P2,P3) plane;
  bool inter=false, isVertex=false;
  for (number_t s = 1; s <= gelt->numberOfSides(); s++) // s = side index
  {
	if (s != side) // solve x-P1+ gamma.d = lambda(P2-P1) with gamma>0 and 0<=lambda<=1
	{
	  const Point& P1 = gelt->vertex(1, s);  // vertices of side s
	  const Point& P2 = gelt->vertex(2, s);
	  P1P2=P2-P1; P1x=x-P1;
	  real_t  b=dot(P1P2,w);
	  if(std::abs(b)<theTolerance) // b=0 means d parallel to P1P2
      {
        real_t e=dot(P1P2,dx);
        if(norm(P1-x)<theTolerance && e>0) {x=P2;isVertex=true;side=s;inter=true;break;}
        if(norm(P2-x)<theTolerance && e<0) {x=P1;isVertex=true;side=s;inter=true;break;}
      }
	  else
      {
        real_t a=dot(P1x,w), c=a/b; // lambda=a/b
	    xx=P1P2; xx*=c;xx+=P1;
//	    theCout<<"P1="<<P1<<" P2="<<P2<<" a="<<a<<" b="<<b<<" c="<<c<<" xx="<<xx
//	           <<" s="<<s<<" |P1-xx|="<<norm(P1-xx)<<" |P2-xx|="<<norm(P2-xx)<<eol<<std::flush;
	    //PROBLEM: when d is quasi-parallel to P1x (a~0), locateElement may detect that dx is admissible whereas the following does not detect intersection !!!
	    if(c>-theTolerance && c<1+theTolerance)  // means -eps << lambda <= 1+eps
        {
          if(a*dot(P1P2,dx)/b-dot(P1x,dx)>theTolerance)
          {
            x=xx;side=s;inter=true;
            if(norm(P1-xx)<theTolerance) {x=P1;isVertex=true;}
            if(norm(P2-xx)<theTolerance) {x=P2;isVertex=true;}
            break;
          }
        }
       else
        {
          if(norm(P1-xx)<1000*theTolerance) {x=P1;isVertex=true;side=s;inter=true;break;}
          if(norm(P2-xx)<1000*theTolerance) {x=P2;isVertex=true;side=s;inter=true;break;}
        }
	  }
	}
  }
  if(!inter)
  {
    theCout<<"no intersection, gelt="<<gelt<<" side="<<side<<eol<<std::flush;
    warning("free_warning","MeshedGeodesic::compute stops before end at x="+tostring(x));
    return *this;
  }
  // crossing new edge, newElt = elt on the other side of edge
  number_t newSide=0;
  if(isVertex) gelt=locateElementFromVertex(x,dx,newSide,gelt);
  else
  {
    const GeomElement* edge = gelt->sideElement(side);
    if(edge->parentSides().size()==1) return *this;  // no neighbor element, reach boundary
    GeomElement* newElt = edge->parentSide(0).first; // pointer to the first
    if (newElt == gelt)
    {
      newElt  = edge->parentSide(1).first;  //at most 2 neighbor elements
	  newSide = edge->parentSide(1).second;
    }
    else newSide = edge->parentSide(0).second;
    // split d in normal and tangent vectors then rotate
    n1 = gelt->normalVector(side); n1 /= norm(n1);
    n2 = newElt->normalVector(newSide); n2 /= norm(n2);
    dx -= dot(dx,n1)*(n1+n2);
    gelt = newElt;
  }
  //update data
  side = newSide;
  real_t ll=norm(x-xs.back());
  if(ll>0)  //new point
  {
    l+=ll;
    elts.push_back(GeoNumPair(gelt,side));
    xs.push_back(x);
    if (withTangent) {dxs.push_back(dx); normals.push_back(n2);}  //use gelt normal, may be using parametrization should be better
    if(lmax>0 || withCurAbc) // check curvilinear length if required
    {
     if(withCurAbc) curAbcs.push_back(l);
     if(lmax>0 && l>=lmax) break;
    }
    if (withCurvatures)  curvs.push_back(curvatures(x,dx,false));
    i++;
//    theCout << "\n  iter "<<i<<": x="<<x <<" dx="<< dx <<" xdx=["<<x[0]<<" "<<x[0]+dx[0]/10<<"],["<<x[1]<<" "<<x[1]+dx[1]/10<<"],["
//    <<x[2]<<" "<<x[2]+dx[2]/10<<"] l= "<<l<<" side="<<side<<" gelt: "<<(*gelt)<<eol;
  }
  else elts.back()=GeoNumPair(gelt,side); //same point, change gelt, side
 }

 //insert list into vector
 xs_.assign(xs.begin(), xs.end());
 elts_.insert(elts_.end(),elts.begin(),elts.end());
 if(withTangent)
 {
   dxs_.assign(dxs.begin(), dxs.end());
   normals_.assign(normals.begin(), normals.end());
 }
 if(withCurAbc) curAbcs_.assign(curAbcs.begin(), curAbcs.end());
 if(withCurvatures) curvatures_.assign(curvs.begin(), curvs.end());
 return *this;
}

// locate starting GeomElement from x and dx
//   x may be not on mesh so it has to be projected on mesh (say px)
//   px+eps*dx may not be included in GeomElement, so dx may be also be projected on (say pdx)
//   so we are searching for a GeomElement such that px and eps*pdx belongs to the GeomElement
// output GeomElement pointer if successes else return 0
// update also x, dx with px, pdx and side s if on side (s=0 if not on side)
// !!!! assuming triangle element !!!
GeomElement* MeshedGeodesic::locateStartingElement(Point& x, Point& dx, number_t& s) const
{
 s=0;
 real_t distance;
 GeomElement* gelt  = dom_p->meshDomain()->nearest(x, distance); // projection of x on the meshDomain
 if(gelt==nullptr)
 {
    warning("free_warning","MeshedGeodesic:: locateStartingElement stops, search nearest element fails");
    return nullptr;
 }
 //theCout<<"try gelt: ";gelt->printVertices(theCout,_matlab);theCout<<"x="<<x<<" dx="<<dx<<eol<<std::flush;

 //check if x is in the interior of gelt
 Point b=toBarycentric(x, gelt->vertex(1),gelt->vertex(2),gelt->vertex(3));
 if(b(1)>theTolerance && b(1)<1-theTolerance && b(2)>theTolerance && b(2)<1-theTolerance && b(3)>theTolerance && b(3)<1-theTolerance)
 {
    Point n=cross3D(gelt->vertex(2)-gelt->vertex(1),gelt->vertex(3)-gelt->vertex(1));
    dx-=dot(dx,n)*n;
    return gelt;
 }
 // check if it is a vertex
 for(s=1;s<=3;s++)
    if(b(s)>1-theTolerance) break;
 if(s<=3) // x=vertex(s)
 {
     gelt=locateElementFromVertex(x,dx,s,gelt);
     if(gelt!=nullptr) return gelt;
     warning("free_warning","MeshedGeodesic:: locateStartingElement stops, search starting element element from vertex fails");
     return nullptr;
 }
 //is on side
 s=2;
 if(b(2)<theTolerance) s=3;
 if(b(3)<theTolerance) s=1;
 Point dy, u, v;
 number_t sp1;
 bool first=true;
 while(gelt!=nullptr)
 {
   const std::vector<real_t>& n =gelt->normalVector();
   dy = dx-dot(dx,n)*n;   //project dx on gelt
   if(dot(dy,gelt->normalVector(s))<theTolerance)
   {
     dx=dy;
     return gelt;
   }
   if(first)
   {
     GeoNumPair gn=gelt->elementSharingSide(s);
     gelt=gn.first; s=gn.second;
     first=false;
   }
   else gelt=nullptr;
 }
 warning("free_warning","MeshedGeodesic:: locateStartingElement stops, search nearest element from sides fails");
 return nullptr;
}

// tool to locate right element when x is a vertex, return 0 if x is not a vertex
// explore all elements having x as vertex and select those such that the projection of dx belongs to
// excluding gelt0 (may be null, no exclusion)
// update dx ans side number s
GeomElement* MeshedGeodesic::locateElementFromVertex(Point& x, Point& dx, number_t& s, GeomElement* gelt0) const
{
 GeomElement* gelt=nullptr;
 MeshDomain* mdom=dom_p->meshDomain();
 std::map<Point,std::list<GeomElement*> >::iterator itm=mdom->vertexElements.find(x);
 if(itm!=mdom->vertexElements.end()) //if y is a vertex find the right element
 {
   Point nor, dy, u, v, n, nu, nv;
   number_t i, kg=0, kmin=0;
   real_t mint=0;
   std::list<GeomElement*>& gelts = itm->second;
   std::list<GeomElement*>::iterator itl=gelts.begin();
   bool found=false, isu=true;
   for(;itl!=gelts.end();++itl,kg++)
   {
     gelt=*itl;
     if(gelt!=gelt0)
     {
       nor = gelt->normalVector(); nor /= norm(nor);	// normal vector of the starting element
       dy = dx-dot(dx, nor)*nor;
       for(i=1;i<=3 && !found;i++)
       {
         if(norm(x-gelt->vertex(i))<theTolerance)
         {
           number_t j=i+1, k=i+2;
           if(i==2) k=1;
           if(i==3) {j=1; k=2;}
           u=gelt->vertex(j)-x; v=gelt->vertex(k)-x;
           n=cross3D(u,v);nv=cross3D(n,v); nu=cross3D(n,u);
           real_t dnu=dot(dy,nu), dnv=dot(dy,nv);
           found = (dnv<theTolerance && dnu>-theTolerance); //in (u,v) cone
//           theCout<<"check gelt="<<gelt<<" vertices [" << gelt->vertex(1)[0] << " " << gelt->vertex(2)[0]<<" "<< gelt->vertex(3)[0]<<"],["
//                  << gelt->vertex(1)[1] << " " << gelt->vertex(2)[1]<<" "<< gelt->vertex(3)[1]<<"],["
//                  << gelt->vertex(1)[2] << " " << gelt->vertex(2)[2]<<" "<< gelt->vertex(3)[2]<<"]"<<eol
//                  <<" x="<<x<<" dy="<<dy<<" xdy=["<<x[0]<<" "<<x[0]+dy[0]/10<<"],["<<x[1]<<" "<<x[1]+dy[1]/10<<"],["<<x[2]<<" "<<x[2]+dy[2]/10<<"]"
//                  <<" i="<<i<<" dot(dy,cross3D(n,v)="<<dot(dy,cross3D(n,v))<<" dot(dy,cross3D(n,u))="<<dot(dy,cross3D(n,u))
//                  <<" found="<<(found?"true":"false")<<eol<<std::flush;
           if(found) s=i;
           else //compute min sin(d,u), sin(d,v), useful if first test fails
           {
             real_t tu=dot(dy,u)/(norm(dy)*norm(u)),
                    tv=dot(dy,v)/(norm(dy)*norm(v));
             if(tu>mint){mint=tu;s=i;isu=true;kmin=kg;}
             if(tv>mint){mint=tv;s=i;isu=false;kmin=kg;}
           }
         }
       }
       if(found) break;  //gelt found
     }
   }
   if(found) {dx=dy; return gelt;} //update dx and return

   //not found, project on the closest edge if cos(angle(uv,dx))>0.99
   //theCout<<"mint="<<mint<<" kmin="<<kmin<<" isu="<<(isu?"true":"false")<<eol<<std::flush;
   if(mint<0.99) return gelt;  //locate fails
   itl=gelts.begin();
   for(number_t k=0;k<kmin;k++) itl++;
   gelt=*itl;
   number_t s1=s+1, s2=s+2;
   if(s==2) s2=1;
   if(s==3) {s1=1; s2=2;}
   if(isu)  dx=gelt->vertex(s1)-x;
   else     dx=gelt->vertex(s2)-x;
 }
 return gelt;
}

void MeshedGeodesic::print(std::ostream& os) const
{
 os<<"Meshed geodesic";
 printData(os);
}
//===========================================================================================
//         GeometricGeodesic class handling any geodesic described by a geometry
//===========================================================================================
GeometricGeodesic::GeometricGeodesic(GeomDomain& dom, Geometry& geom, const Point& x, const Point& dx, real_t lmax, bool wCA, bool wT, bool wC)
: Geodesic(dom, wCA, wT, wC)
{
  //check geometry
  if(geom.dim()!=1) error("free_error","GeometricGeodesic based only on 1D geometry");
  if(geom.parametrizationP()==nullptr) error("free_error","GeometricGeodesic required a 1D geometry with parametrisation");
  geom_p = geom.clone();
  sx_=x; sdx_=dx;
  fcur_p=nullptr; fnor_p=nullptr; // use domain parametrization
}

GeometricGeodesic::GeometricGeodesic(GeomDomain& dom, const Point& x, const Point& dx, real_t lmax, bool wCA, bool wT, bool wC)
: Geodesic(dom, wCA, wT, wC)
{
  sx_=x; sdx_=dx;
  geo_p=dom.geometry();
  fcur_p=nullptr; fnor_p=nullptr;  // use domain parametrization
  build(lmax);
}

GeometricGeodesic::GeometricGeodesic(Geometry& geom, const Point& x, const Point& dx, real_t lmax, bool wCA, bool wT, bool wC)
{
  number_t dim=geom.dim();
  if(dim<1 ||dim>2) error("free_error","GeometricGeodesic works only with 1D/2D geometry");
  dom_p=nullptr; sx_=x; sdx_=dx;
  withTangent=wT;  withCurAbc=wCA; withCurvatures=wC;
  if(dim==1) // explicit geodesic geometry given
  {
    if(geom.parametrizationP()==nullptr) error("free_error","GeometricGeodesic required a 1D geometry with parametrisation");
    geom_p = geom.clone();
    dom_p=nullptr;  geo_p=nullptr; fcur_p=nullptr; fnor_p=nullptr;
    length_= 0;  // uninitialized, to be done later
    return;
  }
  // build the geometric geodesic
  geo_p=&geom;
  fcur_p=nullptr; fnor_p=nullptr;
  build(lmax);
}

GeometricGeodesic::GeometricGeodesic(Geometry& geom, const Point& x, const Point& dx, real_t lmax, number_t nmax, real_t dl, bool wCA, bool wT, bool wC)
{
  number_t dim=geom.dim();
  if(dim<1 ||dim>2) error("free_error","GeometricGeodesic works only with 1D/2D geometry");
  dom_p=nullptr; sx_=x; sdx_=dx;
  withTangent=wT;  withCurAbc=wCA; withCurvatures=wC;
  if(dim==1) // explicit geodesic geometry given
  {
    if(geom.parametrizationP()==nullptr) error("free_error","GeometricGeodesic required a 1D geometry with parametrisation");
    geom_p = geom.clone();
    dom_p=nullptr;  geo_p=nullptr;
    fcur_p=nullptr; fnor_p=nullptr;  // no curvature and normal function assigned, user should give explicit functions if demanded
    length_= 0;          // uninitialized, to be done later
  }
  else // build the geometric geodesic
  {
    geo_p=&geom; fcur_p=nullptr;  fnor_p=nullptr;
    build(lmax);
  }
  real_t l=0.;
  compute(sx_,sdx_,l,lmax,nmax,dl);
}

//!* effective construction of GeometricGeodesic; geo_p, sx_, sdx_ has to be initialized
// up to now, deals only with plane surface. Should be enriched
void GeometricGeodesic::build(real_t lmax)
{
  Point Q;
  if(geo_p->isPlane()) //straight line
  {
    bool hasUniqueIntersection;
    if (geo_p->surface()->isPolygon())
      Q =intersectionHalfLinePolygon(sx_,sdx_,geo_p->surface()->p(),hasUniqueIntersection);
    if (geo_p->shape()==_ellipse||geo_p->shape()==_disk)
      Q =intersectionHalfLineEllipse(sx_,sdx_,geo_p->surface()->p(1),geo_p->surface()->p(2),geo_p->surface()->p(3),
                                      hasUniqueIntersection, geo_p->ellipse()->thetamin(),geo_p->ellipse()->thetamax());
    if (hasUniqueIntersection) error("free_error","building GeometricGeodesic fails");
    length_=norm(sx_-Q);
    if(length_>lmax) {Q=sx_+lmax*sdx_; length_=lmax;}
    ex_=Q; edx_=sdx_;
    geom_p = new Segment(_v1=sx_,_v2=ex_);
    fcur_p=zeroCurvatures;  // plane has (0,0) curvature
    fnor_p=planeNormal;     // plane has constant normal
    std::vector<Point> & ps=geo_p->surface()->p();
    params_<<Parameter(cross3D(ps[1]-ps[0],ps[2]-ps[0]),"normal"); // normal orientation not chosen
    return;
  }
  error("free_error","unable to build GeometricGeodesic for "+geo_p->domName());
}

//! function returning the plane normal stored in Parameters pars
Vector<real_t> planeNormal(const Point& p, bool fromParameters, Parameters& pars)
{
  return Point(pars("normal").get_pt());
}

GeometricGeodesic::~GeometricGeodesic()
{
  if(geom_p!=nullptr) delete geom_p;
}

GeometricGeodesic::GeometricGeodesic(const GeometricGeodesic& gg)
{
  copy(gg);
}

GeometricGeodesic& GeometricGeodesic::operator=(const GeometricGeodesic& gg)
{
  if(geom_p!=nullptr) delete geom_p;
  copy(gg);
  return *this;
}

void GeometricGeodesic::copy(const GeometricGeodesic& gg)
{
  geom_p = nullptr;
  if(gg.geom_p!=nullptr) geom_p=gg.geom_p->clone();
  sx_=gg.sx_; sdx_=gg.sdx_;
  dom_p=gg.dom_p;
  xs_=gg.xs_;
  dxs_=gg.dxs_;
  curAbcs_=gg.curAbcs_;
  curvatures_=gg.curvatures_;
  withCurAbc=gg.withCurAbc;
  withTangent=gg.withTangent;
  withCurvatures=gg.withCurvatures;
  fcur_p=gg.fcur_p;
}

// computes geometric geodesic (fill in xs_, dxs_, curAbcs_)
//  x: starting point in 3D space (on manifold, not check here) // NOT USED
//  d: starting direction in 3D space (not 0),  projected onto the tangent plane (here) // NOT USED
//  lmax: curvilinear length of geodesic  (>0)
//  nbp: number max of computed points (>0)
//  dt: step related to arc length (ds)
//  update xs_ vector (size of xs gives the effective number of computed points
//  update dxs_ vector if required
//  update curAbcs_ vector if required
//  update curvatures_ vector (principal curvatures of the surface at geodesic points)
//  update normalCurvatures_ vector (normal curvature of geodesic:
//         X1,X2 unitary Weingarten eigenvectors related to principal curvatures c1, c2
//         normal curvature = |c1(dxs_|X1)^2+c2(dxs_|X2)^2|
Geodesic& GeometricGeodesic::compute(Point& x, Point& d, real_t& l, real_t lmax, number_t nbp, real_t dt)
{
 if (withCurvatures && fcur_p==nullptr)
   error("free_error","GeometricGeodesic::compute: demands curvatures whereas curvature function is not assigned!");
 if(lmax<=0 || nbp==0)
   error("free_error","GeometricGeodesic::compute: specify nbp>0 and lmax>0");
 std::list<Point> xs;             //new list
 std::list<Point> dxs;            //new list
 std::list<Point> normals;        //new list
 std::list<real_t> curAbcs;       //new list
 std::list<Vector<real_t> > curvs;//new list
 real_t l0=l;
 if(nbp==0) nbp=100; // when nbp=0, use l and element size to estimates nbp
 xs.push_back(sx_);
 if (withTangent) {dxs.push_back(sdx_); normals.push_back(normal(sx_,false));}
 if (withCurAbc) curAbcs.push_back(0.);
 if (withCurvatures) curvs.push_back(curvatures(sx_,d,false));
 const Parametrization& par=geom_p->parametrization();
 real_t t0=par.toParameter(x)[0];
 if(dt==0.) //estimate dt such that |par(t+dt)-x0|*nbp ~ l;
 {
   dt=lmax/nbp;
   real_t t2=t0+dt, t1=t0, tm;
   Point x0=(*xs.begin()), x1=x0, x2=par(t2), xm;
   while(norm(x2-x0)*nbp<lmax) {t2+=dt;x2=par(t2);}
   while(t2-t1 > dt/10)
   {
     tm=(t1+t2)/2; xm=par(tm);
     if(norm(xm-x0)*nbp<lmax) {t1=tm;x1=xm;}
     else {t2=tm;x2=xm;}
   }
   dt=0.5*(t1+t2)-t0;
 }
 else dt/=norm(par(t0,_d1));  //normalize dt by the norm of initial velocity
 real_t ti=t0;
 //build xs_
 int i=0; bool stop=false; Point dx;
 while(i<nbp)
 {
   ti+=dt;
   Point xn=par(ti);
   if(xn.size()==0) {xn=ex_; stop=true;}
   l+=norm(xn-xs.back());
   xs.push_back(xn);
   if(withTangent || withCurvatures)
   {
     dx=par(ti,_d1);
     if (withTangent) {dxs.push_back(dx); normals.push_back(normal(xn,false));}
     if (withCurvatures) curvs.push_back(curvatures(xn,dx,false));
   }
   if (withCurAbc) curAbcs.push_back(l);

   i++;
   if(stop || l>lmax)
   {
     //theCout<<"lmax atteint: l="<<l<<" x="<<xn<<eol<<std::flush;
     break;
   }
 }
 length_=l-l0;
 //assign list to vector
 xs_.assign(xs.begin(), xs.end());
 if(withTangent)
 {
   dxs_.assign(dxs.begin(), dxs.end());
   normals_.assign(normals.begin(), normals.end());
 }
 if(withCurAbc) curAbcs_.assign(curAbcs.begin(), curAbcs.end());
 if(withCurvatures) curvatures_.assign(curvs.begin(), curvs.end());
 return *this;
}

bool hasGeometricGeodesic(const Geometry& geo)
{
  if(geo.dim()!=2) return false;
  if(geo.isPlane())
    return( geo.surface()->isPolygon()|| geo.shape()==_ellipse||geo.shape()==_disk);
  return false;
}

void GeometricGeodesic::print(std::ostream& os) const
{
   os<<"Geometric geodesic";
   printData(os);
}
//===========================================================================================
//         AnalyticGeodesic class handling any geodesic described by a parametrization
//===========================================================================================
AnalyticGeodesic::AnalyticGeodesic(const Parametrization& pa, bool wCA, bool wT, bool wC)
{
   withCurAbc=wCA;
   withTangent=wT;
   withCurvatures=wC;
   dom_p=new GeomDomain("virtual domain");
   parg_=pa;
   init_p=nullptr;
   fcur_p=nullptr;
   smin_=0.;smax_=0.;
}

AnalyticGeodesic::AnalyticGeodesic(const Geometry& geom, const Point& x, const Point& dx,
                                   real_t lmax, number_t nbp, real_t dt, bool wCA, bool wT, bool wC)
{
   build(geom,wCA,wT,wC);
   real_t l=0;
   Point xx=x, dxx=dx;
   compute(xx,dxx,l,lmax,nbp,dt);
}

void AnalyticGeodesic::print(std::ostream& os) const
{
   os<<"Analytic geodesic";
   printData(os);
}

void AnalyticGeodesic::set(const Point& x, const Point& dx)
{
    parg_.params<<Parameter(x,"x")<<Parameter(dx,"dx");
    sx_=x; sdx_=dx;
}

// computes analytic geodesic (fill in xs_, dxs_, curAbcs_)
//   x: starting point in 3D space (on manifold, not check here)
//   d: starting direction in 3D space (not 0),  projected onto the tangent plane
// lmax: curvilinear length of geodesic
//   nbmax: number max of computed points
// dt: step related to arc length
//   update xs_ vector (size of xs gives the effective number of computed points
//   update dxs_ vector if required
//   update curAbcs_ vector if required
//   update curvatures_ vector if required
// specify non null couple (nbp,l), (nbp, dt) or (dt,l)
Geodesic& AnalyticGeodesic::compute(Point& x, Point& d, real_t& l, real_t lmax, number_t nbmax, real_t dt)
{
 if(lmax<=0 && nbmax==0) error("free_error","AnalyticGeodesic::compute: specify non null couple (nbmax,lmax), (nbmax, dt) or (dt,lmax)");
 set(x,d);
 init_p(parg_.params);
 real_t l0=l;
 sx_=x; sdx_=d;
 std::list<Point> xs;             //new list
 std::list<Point> dxs;            //new list
 std::list<Point> normals;        //new list
 std::list<real_t> curAbcs;       //new list
 std::list<Vector<real_t> > curvs;//new list
 xs.push_back(x);
 if (withTangent) {dxs.push_back(d); normals.push_back(normal(x,false));}
 if (withCurAbc) curAbcs.push_back(l);
 if (withCurvatures) curvs.push_back(curvatures(x,d,false));
 //estimate dt from dl or lmax/nbmax
 real_t t0=parg_.toParameter(x)[0];
 if(dt==0)
 {
   if(lmax<=0 || nbmax==0) error("free_error","AnalyticGeodesic::compute: specify non null couple (nbmax,lmax), (nbmax, dt) or (dt,lmax)");
   else dt=lmax/nbmax;
 }
 real_t dl=dt;
 real_t t2=t0+dt, t1=t0, tm;
 Point x1=x, x2=parg_(t2), xm;
 if(x2.size()>0)
    while(norm(x2-x)<dl) {t2+=dt;x2=parg_(t2);}
 while(t2-t1 > dt/10)
 {
   tm=(t1+t2)/2; xm=parg_(tm);
   if(xm.size()>0)
   {
     if(norm(xm-x)<dl) t1=tm; else t2=tm;
   }
   else t2=tm;
 }
 dt=0.5*(t1+t2)-t0;
 if(lmax<=0 && nbmax>0 && dt>0) lmax=theRealMax;
 if(nbmax==0 && dt>0 && lmax>0) nbmax=theIntMax;

 //build xs_, ...
 real_t ti=t0; number_t i=0; bool cont=true;
 //theCout<<"\n    iter "<<i<<" t="<<ti<<" x="<<x<<" dx="<<d<<" l="<<l<<std::flush;
 Point xi, dxi;
 while(i<nbmax && l<lmax && cont)
 {
   ti+=dt;
   if(ti>smax_) {ti=smax_; cont=false;}
   if(ti<smin_) {ti=smin_, cont=false;}
   xi=parg_(ti);
   if(xi.size()==0) {ti-=dt;break;}; //out of parameter bounds
   l+=norm(xi-xs.back());
   xs.push_back(xi);
   if (withTangent || withCurvatures)
   {
     dxi=parg_(ti,_d1);
     if(withTangent) {dxs.push_back(dxi); normals.push_back(normal(xi,false));}
     if(withCurvatures) curvs.push_back(curvatures(xi,dxi,false));
   }
   if (withCurAbc) curAbcs.push_back(l);
   //theCout<<"\n    iter "<<i<<" t="<<ti<<" x="<<xi<<" dx="<<parg_(ti,_d1)<<" l="<<l<<std::flush;
   i++;
 }
 length_=l-l0;
 ex_=xs.back();edx_=parg_(ti,_d1);
 //assign list to vector
 xs_.assign(xs.begin(), xs.end());
 if(withTangent)
 {
   dxs_.assign(dxs.begin(), dxs.end());
   normals_.assign(normals.begin(), normals.end());
 }
 if(withCurAbc) curAbcs_.assign(curAbcs.begin(), curAbcs.end());
 if(withCurvatures) curvatures_.assign(curvs.begin(), curvs.end());
 return *this;
}

bool hasAnalyticGeodesic(const Geometry& geo)
{
  switch(geo.shape())
  {
    case _trunkSidePart: return true;
    default: break;
  }
  return false;
}
AnalyticGeodesic::AnalyticGeodesic(const Geometry& geo, bool wCA, bool wT, bool wC)
{
   build(geo,wCA,wT,wC);
}

AnalyticGeodesic::AnalyticGeodesic(const GeomDomain& dom, bool wCA, bool wT, bool wC)
{
    dom_p=const_cast<GeomDomain*>(&dom);
    Geometry& geo=*dom.geometry();
    build(geo,wCA,wT,wC);
}

// build AnalyticGeodesic object regarding geometry
//   initialize the Parameters structure 'params' with some geometrical data of supporting geometry
//   initialize function pointers of related to geodesic parametrization, to geodesic inverse parametrization, to curvatures function
//   for trunkSidePart (cylinder side part) :
//      params: 'ag'->this, 'C'->center1, 'T'->origin, 'A'-> basis first point, 'tmin'->theta min, 'tmax'->theta_max
//      Parametrization.f_p = cylinderSidePartGeodesic
//      Parametrization.invParametrization_p = invCylinderSidePartGeodesic
//      init_p = initCylinderSidePartGeodesic
//      fcur_p = cylinderSidePartGeodesicCurvatures
void AnalyticGeodesic::build(const Geometry& geo, bool wCA, bool wT, bool wC)
{
  withCurAbc=wCA;
  withTangent=wT;
  withCurvatures=wC;
  switch(geo.shape())
  {
    case _trunkSidePart: // actually, only disk cylinder side part !
    {
      const TrunkSidePart& tr=*geo.trunkSidePart();
      const Trunk& tru = tr.relTrunk();
      if(!tru.isElliptical()) break; //basis is not a ellipse
      const Ellipse* ell = dynamic_cast<const Ellipse*>(tru.basis());
      if(ell==nullptr) break;              //basis is not a ellipse
      if(ell->radius1()!=ell->radius2() && dynamic_cast<const Disk*>(ell)==nullptr) break; //basis is not a disk
      const Point& C=tr.center1();
      const Point& T=tr.origin();
      const Point& A=tru.p()[1];
      const Point CA=A-C, CT=T-C;
      if(std::abs(dot(CA,CT))>theTolerance) break;  //direction not perpendicular to basis
      params_<<Parameter((void *)(this),"ag")<<Parameter(C,"C")<<Parameter(T,"T")<<Parameter(A,"A")<<Parameter(tr.tmin(),"tmin")<<Parameter(tr.tmax(),"tmax");
      parg_=Parametrization(0.,1., cylinderSidePartGeodesic, params_,"cylinder geodesic", 3);
      parg_.setinvParametrization(invCylinderSidePartGeodesic);
      init_p=initCylinderSidePartGeodesic;
      fcur_p=cylinderSidePartGeodesicCurvatures;
      fnor_p=cylinderSidePartGeodesicNormal;
      return;
    }
    default: break;
  }
  error("free_error","cannot build an AnalyticGeodesic for "+geo.domName());
}

//-------------------------------------------------------------------------------------------
//                     cylinder side part geodesic stuff
//-------------------------------------------------------------------------------------------
//parametrization of geodesics of a cylinder side part
// C basis center, T top center, A first apogee of basis, tmin, tmax" min and max angle relative to A
// parametrization of the cylinder side part: (u,v) in [0,1]x[0,1]
//    P(u,v) = C+cos(t)CA + sin(t)d^CA+ vCT   t=tmin+u(tmax-tmin) ; d=CT/norm(CT)
// parametrization of geodesics starting at (x,dx)
//    g(s)= cos(as+b)CA+sin(as+b)d^CA+(ls+m)CT  as+b in [tmin,tmax] and ls+m in [0,1]
//    with m=x.CT/CT.CT l=dx.CT/CT.CT
//         b=atan2(sin(b),cos(b)) sin(b)=x.(d^CA)/(d^CA).(d^CA)  cos(b)=x.CA/CA.CA
//         if sin(b)!=nullptr a=-dx.CA/sin(b)CA.CA else a=dx.(d^CA)/cos(b)(d^CA).(d^CA)
// params contains "pointer to ag","C","T","A","tmin","tmax","x","dx","a","b","l","m" in this order
// t must be in [smin_,smax_] if not, parametrization returns a void vector
Vector<real_t> cylinderSidePartGeodesic(const Point& P, Parameters& params, DiffOpType dif)
{
   std::vector<Parameter*>::iterator itp=params.list_.begin();
   const AnalyticGeodesic* ag=static_cast<const AnalyticGeodesic*>((*itp)->get_p()); itp++;
   const Point& C=(*itp)->get_pt(); itp++;
   const Point& T=(*itp)->get_pt(); itp++;
   const Point& A=(*itp)->get_pt(); itp++;
   real_t tmin=(*itp)->get_r(); itp++;
   real_t tmax=(*itp)->get_r(); itp++;
   if(itp==params.list_.end()) error("free_error","starting point not found in cylinderSidePartGeodesic");
   itp++;itp++;  // jump x,dx
   if(itp==params.list_.end())
   {
     initCylinderSidePartGeodesic(params);
     itp=params.list_.begin();
     for(number_t i=0;i<8;i++) itp++;
   }
   real_t a=(*itp)->get_r(); itp++;
   real_t b=(*itp)->get_r(); itp++;
   real_t l=(*itp)->get_r(); itp++;
   real_t m=(*itp)->get_r();

   real_t s=P[0], asb=a*s+b, lsm=l*s+m;
   if(asb<tmin-theTolerance||asb>tmax+theTolerance||lsm<-theTolerance||lsm>1+theTolerance) return Vector<real_t>(0); // outside bounds return void Vector
   asb=std::min(std::max(tmin,asb),tmax);
   lsm=std::min(std::max(0.,lsm),1.);
   Point CA=A-C, CT=T-C, CB=cross3D(CT,CA)/norm(CT);
   switch(dif)
   {
     case _id: return  std::cos(asb)*CA+std::sin(asb)*CB+(lsm)*CT;
     case _d1: return  a*(std::cos(asb)*CB-std::sin(asb)*CA)+l*CT;
     case _d11: return  -(a*a)*(std::cos(asb)*CA+std::sin(asb)*CB);
     case _d111: return  -(a*a*a)*(std::cos(asb)*CB-std::sin(asb)*CA);
     default: break;
   }
  error("free_error","cylinderSidePartGeodesic: only up to third derivative");
  return Vector<real_t>(0);
}

// inverse of cylinderSidePartGeodesic 3D->1D
Vector<real_t> invCylinderSidePartGeodesic(const Point& pt, Parameters& params, DiffOpType d)
{
   if(d!=_id) error("free_error","invCylinderSidePartGeodesic: only value, no derivative");
   std::vector<Parameter*>::iterator itp=params.list_.begin();
   const AnalyticGeodesic* ag=static_cast<const AnalyticGeodesic*>((*itp)->get_p()); itp++;
   const Point& C=(*itp)->get_pt(); itp++;
   const Point& T=(*itp)->get_pt(); itp++;
   const Point& A=(*itp)->get_pt(); itp++;
   real_t tmin=(*itp)->get_r(); itp++;
   real_t tmax=(*itp)->get_r(); itp++;
   itp++;itp++;  // jump x,dx
   real_t a=(*itp)->get_r(); itp++;
   real_t b=(*itp)->get_r(); itp++;
   real_t l=(*itp)->get_r(); itp++;
   real_t m=(*itp)->get_r();
   //note: l=0 means geodesic is a circle, a=0 means geodesic is a parallel
   Point CT=T-C;
   Vector<real_t> t(1,0.);
   if(std::abs(l)>theTolerance) t[0]=(dot(pt,CT)/dot(CT,CT)-m)/l; //use ls+m = dot(P,CT)/dot(CT,CT),
   else //use cos(as+b)= = dot(P,CA)/dot(CA,CA) and sin(as+b)= = dot(P,CB)/dot(CB,CB)
   {
     Point CA=A-C, CB=cross3D(CT,CA)/norm(CT);
     real_t cb=dot(pt,CA)/dot(CA,CA), sb=dot(pt,CB)/dot(CB,CB);
     t[0]=(std::atan2(sb,cb)-b)/a;  // l and a cannot be both 0 (geodesic=point)
   }
   real_t s=a*t[0]+b;
   if(s<tmin || s>tmax) t.clear(); //out of bounds
   return t;
}

void initCylinderSidePartGeodesic(Parameters& params)
{
   std::vector<Parameter*>::iterator itp=params.list_.begin();
   const AnalyticGeodesic* ag=static_cast<const AnalyticGeodesic*>((*itp)->get_p());itp++;
   const Point& C=(*itp)->get_pt(); itp++;
   const Point& T=(*itp)->get_pt(); itp++;
   const Point& A=(*itp)->get_pt(); itp++;
   real_t tmin=(*itp)->get_r(); itp++;
   real_t tmax=(*itp)->get_r(); itp++;
   const Point& x=(*itp)->get_pt(); itp++;
   const Point& dx=(*itp)->get_pt(); itp++;
   Point CA=A-C, CT=T-C, CB=cross3D(CT,CA)/norm(CT);
   real_t sb=dot(x,CB)/dot(CB,CB), cb=dot(x,CA)/dot(CA,CA);
   real_t b=std::atan2(sb,cb), a;
   if(std::abs(sb)>0.1) a=-dot(dx,CA)/(sb*dot(CA,CA));
   else  a=dot(dx,CB)/(cb*dot(CB,CB));
   if(b<0) b+=2*pi_;   //move to [0,2pi]
   if(std::abs(b-2*pi_)<theTolerance) b=0;   //move to [0,2pi[
   params<<Parameter(a,"a")<<Parameter(b,"b");
   real_t nCT=dot(CT,CT);
   real_t l=dot(dx,CT)/nCT, m=dot(x,CT)/nCT;
   params<<Parameter(l,"l")<<Parameter(m,"m");
   // bounds on s: tmin <= as+b <= tmax_ and 0 <= ls+m <= 1
   if(a>0) {ag->smin_=(tmin-b)/a; ag->smax_=(tmax-b)/a;}
   if(a<0) {ag->smax_=(tmin-b)/a; ag->smin_=(tmax-b)/a;}
   if(a==0 && b>=tmin && b<=tmax) {ag->smin_=0; ag->smax_=1.;}
   if(l>0) {ag->smin_=std::max(ag->smin_,-m/l); ag->smax_=std::min(ag->smax_,(1-m)/l);}
   if(l<0) {ag->smax_=std::min(ag->smax_,-m/l); ag->smin_=std::max(ag->smin_,(1-m)/l);}
}

// cylinder side curvatures: Gauss = 0, mean R/2 (principal curvatures (R,0))
//     normal curvature relative to d: kn= R dot(d/|d|,a x n)^2 with a=CT/|CT| and n the outward normal to the cylinder side
Vector<real_t> cylinderSidePartGeodesicCurvatures(const Point& p,  const Point& d, bool fromParameters, Parameters& pars)
{
   std::vector<Parameter*>::iterator itp=pars.list_.begin();
   itp++;
   const Point& C=(*itp)->get_pt(); itp++;
   const Point& T=(*itp)->get_pt(); itp++;
   const Point& A=(*itp)->get_pt();
   real_t R=norm(A-C);
   Vector<real_t> cs(3,0.); cs[0]=R; // basis radius
   Point Cp=p-C, CT=T-C;
   Point n = Cp+dot(Cp,CT)/dot(CT,CT)*CT; n/=norm(n);
   CT/=norm(CT);
   real_t kn = dot(d/norm(d),cross3D(CT,n));
   cs[2]=R*kn*kn;
   return cs;
}

// cylinder side outward normal: n = p-C +(p-C|T-C)(T-C)/(T-C|T-C)  C basis center, T top center (C!=T)
Vector<real_t> cylinderSidePartGeodesicNormal(const Point& p, bool fromParameters, Parameters& pars)
{
   std::vector<Parameter*>::iterator itp=pars.list_.begin(); itp++;
   const Point& C=(*itp)->get_pt(); itp++;
   const Point& T=(*itp)->get_pt();
   Point Cp=p-C, CT=T-C;
   CT*=(dot(Cp,CT)/dot(CT,CT));
   CT+=Cp;
   return CT/norm(CT);  //unitary normal
}

//===========================================================================================
//         CompositeGeodesic class handling a collection of Geodesics
//===========================================================================================
CompositeGeodesic::CompositeGeodesic(const CompositeGeodesic& cg)
{copy(cg);}

CompositeGeodesic& CompositeGeodesic::operator=(const CompositeGeodesic& cg)
{
  if(this==&cg) return *this;
  clear();
  copy(cg);
  return *this;
}

void CompositeGeodesic::copy(const CompositeGeodesic& cg)
{
   std::list<Geodesic*>::const_iterator itg =cg.geodesics_.begin();
   for(;itg!=cg.geodesics_.end();++itg) geodesics_.push_back(cg.clone());
}

void CompositeGeodesic::clear()
{
  std::list<Geodesic*>::iterator itg=geodesics_.begin();
  for(;itg!=geodesics_.end();++itg) delete *itg;
  geodesics_.clear();
  length_=0;
}

CompositeGeodesic::~CompositeGeodesic()
{clear();}

void CompositeGeodesic::add(const Geodesic& geod)
{
   geodesics_.push_back(geod.clone());
}

//locate Geometry (!=last) in Components list that contains x and x+eps*dx (eps>0)
// use inverse parametrization, return 0 if fails
Geometry* locate(const Point& x, Point& dx, const std::map<number_t, Geometry*>& components, Geometry* last)
{
  //theCout<<"\nin locate: x="<<x<<" dx="<<dx<<" "<<std::flush;
  //if(last!=nullptr) theCout<<last->parametrization()<<eol<<std::flush; else theCout<<eol;
  std::map<Geometry*,Point> geos;
  Point uv, n1, n2, du, dv;
  // find all geometries concerned by x
  std::map<number_t, Geometry*>::const_iterator itm=components.begin();
  for(;itm!=components.end();++itm)
  {
    //theCout<<" check "<<itm->second->parametrization();
    uv=itm->second->parametrization().toParameter(x);
    if(itm->second!=last)
    {
       //theCout<<" -> uv="<<uv<<eol<<std::flush;
       if(uv.size()>0) geos.insert(std::make_pair(itm->second,uv));
    }
    else  //compute normal to boundary and project dx onto tangent plane
    {
       du=itm->second->parametrization()(uv,_d1);
       dv=itm->second->parametrization()(uv,_d2);
       if(std::abs(uv[0])<=theTolerance) n1=-du;
       else if(std::abs(uv[0]-1)<=theTolerance) n1=du;
       else if(std::abs(uv[1])<=theTolerance) n1=-dv;
       else if(std::abs(uv[1]-1)<=theTolerance) n1=dv;
       //theCout<<" -> "<<" uv="<<uv<<" n1="<<n1<<eol<<std::flush;
    }
  }
  if(geos.size()==0)
  {
    if(last==nullptr) error("free_error","CompositeGeodesic: unable to find geometry where is located the starting point "+tostring(x));
    return nullptr;
  }
  real_t a=1E-6;
  Point y, n, uvy;
  std::map<Geometry*,Point>::iterator itg;
  // find from geometries concerned by x, the right one (ie Proj(x+adx) in geometry for a enough small)
  while(a>theTolerance)
  {
    y=x+a*dx;
    for(itg=geos.begin();itg!=geos.end();++itg)
    {
      const Parametrization& par=itg->first->parametrization();
      uv=itg->second;
      du=par(uv,_d1); dv=par(uv,_d2);
      n=cross3D(du,dv);n/=norm(n);
      y-=a*dot(dx,n)*n;
      uvy=par.toParameter(y);
      //theCout<<"\n  test y="<<y<<" uv="<<uv<<" "<<par<<eol<<std::flush;
      if(uvy.size()>0)  //located, update dx using rotation
      {
        if(n1.size()>0)
        {
          if(std::abs(uv[0])<=theTolerance) n2=-du;
          else if(std::abs(uv[0]-1)<=theTolerance) n2=du;
          else if(std::abs(uv[1])<=theTolerance) n2=-dv;
          else if(std::abs(uv[1]-1)<=theTolerance) n2=dv;
          n1/=norm(n1);n2/=norm(n2);
          dx-=dot(dx,n1)*(n1+n2);
        }
        //theCout<<"\nout locate: x="<<x<<" uv="<<uv<<" n1="<<n1<<" n2="<<n2<<" dx="<<dx<<" "<<par<<eol<<std::flush;
        return itg->first;
      }
    }
    a/=10;
  }
  //theCout<<"\nout locate: x="<<x<<" dx="<<dx<<" NOT FOUND"<<eol<<std::flush;
  return nullptr;
}

/*! compute CompositeGeodesic
    fill in local xs_, dxs_, .. vectors but not global ones (use toGlobal())
*/
Geodesic& CompositeGeodesic::compute(Point& x, Point& dx, real_t& l, real_t lmax, number_t nmax, real_t dt)
{
   if(geo_p==nullptr) error("free_error","no geometry to build composite geodesic");
   clear();
   sx_=x;sdx_=dx;
   if(geo_p->isCanonical())
   {
     if(hasGeometricGeodesic(*geo_p))
       geodesics_.push_back(new GeometricGeodesic(*geo_p,x,dx,lmax,nmax,dt,withTangent,withCurAbc));
     if(hasAnalyticGeodesic(*geo_p))
       geodesics_.push_back(new AnalyticGeodesic(*geo_p,x,dx,lmax,nmax,dt,withTangent,withCurAbc));
     if(geodesics_.size()==0)
       geodesics_.push_back(new ParametrizedGeodesic(*geo_p,x,dx,lmax,nmax,dt,withTangent,withCurAbc));
     if(geodesics_.size()==0) error("free_error","in CompositeGeodesic::build, unable to build geodesic for "+geo_p->domName());
     else length_=(*geodesics_.begin())->length_;
   }
   else  //travel components
   { //locate first component (assume each geometry is parametrized and toParameter returns a void vector if not in geometry)
     Geometry* g, *pg=nullptr;
     while(length_<lmax)
     {
       Geometry* g=locate(x,dx,geo_p->components(),pg);
       if(g==nullptr) break;
       if(hasGeometricGeodesic(*g)) geodesics_.push_back(new GeometricGeodesic(*g,x,dx,lmax-length_,nmax,dt,withTangent,withCurAbc));
       else
       {
         if(hasAnalyticGeodesic(*g)) geodesics_.push_back(new AnalyticGeodesic(*g,x,dx,lmax-length_,nmax,dt,withTangent,withCurAbc));
         else                        geodesics_.push_back(new ParametrizedGeodesic(*g,x,dx,lmax-length_,nmax,dt,withTangent,withCurAbc));
       }
       length_+=geodesics_.back()->length_;
       x=geodesics_.back()->ex_;
       dx=geodesics_.back()->edx_;
       //theCout<<"out of new geodesic: x"<<x<<" dx="<<dx<<eol<<std::flush;
       //projection on
       pg=g;
     }
     if(geodesics_.size()==0) error("free_error","in CompositeGeodesic::build, unable to build geodesic for "+geo_p->domName());
   }
   return *this;
}

//!< return principal curvatures if available
Vector<real_t> CompositeGeodesic::curvatures(const Point& p, bool fromParameters)
{
   error("free_error","CompositeGeodesic::curvatures not available");
   return Vector<real_t>();
}

void CompositeGeodesic::print(std::ostream& os) const
{
   os<<"Composite geodesic with "<<geodesics_.size()<<" geodesics (length="<<length_<<"):"<<eol;
   std::list<Geodesic*>::const_iterator itg=geodesics_.begin();
   for(;itg!=geodesics_.end();++itg)
     os<<(**itg)<<eol;
}

//!< fill xs_, dxs_, curAbcs_ from local xs_, dxs_, curAbcs_
void CompositeGeodesic::toGlobal()
{
   xs_.clear();dxs_.clear();curAbcs_.clear();
   std::list<Geodesic*>::const_iterator itg=geodesics_.begin();
   real_t s=0.;
   for(;itg!=geodesics_.end();++itg)
   {
     std::vector<Point>::const_iterator itp=(*itg)->xs_.begin();
     if(itg!=geodesics_.begin()) itp++;
     for(;itp!=(*itg)->xs_.end();++itp) xs_.push_back(*itp);
     if(withTangent)
     {
       itp=(*itg)->dxs_.begin();
       if(itg!=geodesics_.begin()) itp++;
       for(;itp!=(*itg)->xs_.end();++itp) dxs_.push_back(*itp);
     }
     if(withCurAbc)
     {
       std::vector<real_t>::const_iterator itc=(*itg)->curAbcs_.begin();
       if(itg!=geodesics_.begin()) itc++;
       for(;itc!=(*itg)->curAbcs_.end();++itc) curAbcs_.push_back(s+*itc);
       s=curAbcs_.back();
     }
   }
}


//===========================================================================================
// 3D shadow/light stuff
//===========================================================================================
// reverse point order
SegElt& SegElt::reverse()
{
    Point q=P1_; P1_=P2_; P2_=q;
    number_t s=s1_;s1_=s2_;s2_=s;
    return *this;
}
void SegElt::print(std::ostream& out) const
{
   if(gelt2_==nullptr)
   {
      out<<"Interior segment ["<<P1_[0]<<" "<<P2_[0]<<"],["<<P1_[1]<<" "<<P2_[1]<<"],["<<P1_[2]<<" "<<P2_[2]<<"]";
      out<<", GeomElement "<<gelt1_->number()<<" ";
      gelt1_->printVertices(out,_matlab);
      out<<", s1="<<s1_<<" s2="<<s2_;
   }
   else
   {
      out<<"Edge segment ["<<P1_[0]<<" "<<P2_[0]<<"],["<<P1_[1]<<" "<<P2_[1]<<"],["<<P1_[2]<<" "<<P2_[2]<<"]";
      out<<", GeomElement "<<gelt1_->number()<<" ";
      gelt1_->printVertices(out,_matlab);
      out<<", s1="<<s1_ <<", GeomElement "<<gelt2_->number()<<" ";
      gelt2_->printVertices(out,_matlab);
      out<<", s2="<<s2_;
   }
}

} // end of namespace xlifepp
