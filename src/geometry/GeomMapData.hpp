/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomMapData.hpp
  \author D. Martin, E. Lunéville
  \since 13 apr 2012
  \date 23 may 2012

  \brief Definition of the xlifepp::GeomMapData class

  A xlifepp::GeomMapData is a data structure to store data related to the map from reference element
  to geometric element (jacobian matrix, its determinant and the differential element,...)

  GeomMapData can managed an additional isoGeometric mapping G (isoPar_p pointer) from parameters domain to the real domain
                F                           G
        ref elt -> elt in parameters domain ->  elt in real domain
                        elt.isoNodes                 elt.nodes
  the mapping is H : x -> G(F(x)) and derivative D[G(F(x)]=D.G(F(x))DF(x), i.e jacH = jacG * jacF
  when useParametrization=true, all the computation function add an additional step to apply the isoGeometric mapping G
  To use it, the Parametrization pointer has to be given, the useParametrization flag set to true
  and the GeomElement::isoNodes have to be built (see GeomDomain::buildIsoNodes)

  The standard case (no isoGeometric mapping) corresponds to the case G = Id !

*/

#ifndef GEOM_MAP_DATA_HPP
#define GEOM_MAP_DATA_HPP

#include "utils.h"

namespace xlifepp
{
//forward declaration
class MeshElement;
class Quadrature;
class ShapeValues;
class ExtensionData;

//------------------------------------------------------------------------------------
/*!
 \class GeomMapData
 object handles useful computational data of a geometric element
 It stores jacobian, its determinant, ... depending of a point
*/
//------------------------------------------------------------------------------------
class GeomMapData
{
  private:
    const MeshElement* geomElement_p;        //!< current geometric element

  public:
    Point currentPoint;                      //!< point in reference element used for last computation
    Matrix<real_t> jacobianMatrix;           //!< jacobian matrix of mapping from reference element to current one
    Matrix<real_t> inverseJacobianMatrix;    //!< inverse jacobian matrix (when needed)
    real_t jacobianDeterminant;              //!< jacobian determinant
    real_t differentialElement;              //!< differential element for elementary integrals (abs(jac. det.))
    Vector<real_t> normalVector;             //!< unit outward normal vector at a given boundary point
    Vector<real_t> tangentVector;            //!< unit tangent vector at a given boundary point
    Vector<real_t> bitangentVector;          //!< second unit tangent vector at a given boundary point
    Matrix<real_t> metricTensor;             //!< symetric metric tensor (t_i|t_j)
    real_t metricTensorDeterminant;          //!< metric_tensor determinant for differential geometry
    const Parametrization* isoPar_p;         //!< pointer to isoGeometric parametrization (default 0)
    bool useIsoNodes;                        //!< if true use MeshElement::isoNodes (default false)
    bool useParametrization;                 //!< if true take into account isogeometric parametrization (default false)

	dimen_t elementDim;                      //!< dim of reference space
	dimen_t spaceDim;                        //!< dim of physical space

	std::map<Quadrature*,std::vector<Point> > phyPoints;      //!< to store image of quadrature points in physical space
	ExtensionData* extdata;                                   //!< pointer to additionnal data used by extension
    mutable std::vector< Vector<real_t> > sideNormalVectors;  //!< normal to the sides of the element (build if required)
    std::vector< Vector<real_t> >& sideNV() const;            //!< get normals to the sides of the element

    //constructor
    GeomMapData() : geomElement_p(nullptr), isoPar_p(nullptr), extdata(nullptr){}
    GeomMapData(const MeshElement*, const Point&,
                bool withJ=false, bool withInvJ=false, bool withN=false);   //!< basic constructor
    GeomMapData(const MeshElement*, std::vector<real_t>::const_iterator,
                bool withJ=false, bool withInvJ=false, bool withN=false);   //!< basic constructor
    GeomMapData(const MeshElement*,
                bool withJ=false, bool withInvJ=false, bool withN=false);   //!< basic constructor (element centroid as point)

    const MeshElement& geomElement() const                        //!access to related MeshElement
    {return *geomElement_p;}
    real_t measures(number_t s) const;                            //!access to measure of element(s=0) or measure of side s>0

    //point mapping
    Point geomMap(const ShapeValues& shv, number_t side);          //!< mapping from ref. elt onto geom. elt. Element (shape functions given, thread safe)
    Point geomMap(const std::vector<real_t>& p, number_t side = 0);//!< mapping from ref. elt onto geom. elt. Element of point p
    Point geomMap(std::vector<real_t>::const_iterator p, number_t side = 0); //!< mapping from ref. elt onto geom. elt. Element of point p
    Point geomMapInverse(const std::vector<real_t>&, real_t eps=theTolerance,
                         number_t maxIter = 50);                  //!< mapping from elt onto ref. elt. of point p
    Point piolaMap(number_t side = 0);                            //!< Piola mapping from ref. elt onto geom. elt. Element (shape functions known)
    Matrix<real_t> covariantPiolaMap(const Point& =Point());      //!< return the covariant Piola map matrix at current point
    Matrix<real_t> contravariantPiolaMap(const Point& =Point());  //!< return the contravariant Piola map matrix at current point

    //geometric computation functions
    void computeJacobianMatrix(const ShapeValues& shv, number_t side =0); //!< compute jacobian matrix (shape functions given)
    void computeJacobianMatrix(const std::vector<real_t>&,
                               number_t side = 0);      //!< compute jacobian matrix at a point given as vector or Point
    void computeJacobianMatrix(std::vector<real_t>::const_iterator,
                               number_t side=0);        //!< compute jacobian matrix at a point given by iterator

    real_t computeJacobianDeterminant();                //!< compute jacobian determinant (jacobian already computed)
    void invertJacobianMatrix();                        //!< compute inverse of jacobian matrix (jacobian already computed)
    void computeDifferentialElement();                  //!< compute differential element assuming jacobian matrix is update
    void computeNormalVector();                         //!< computes normal vector at a point of element
    void computeTangentVector();                        //!< computes tangent (and bitangent) vector at a point of element
// Unused    real_t diffElement();                               //!< update and return differential element
// Unused    real_t diffElement(number_t);                       //!< update and return differential element on side
    void normalize();                                   //!< normalize normal vector to element
    void computeOrientedNormal();                       //!< compute oriented unit normal vector, assuming jacobian matrix is update
    void computeMetricTensor();                         //!< compute metric tensor for surface differential geometry
    void computeSurfaceGradient(real_t, real_t, std::vector<real_t>&); //!< compute surface gradient gradS from 2D reference gradient (grad0, grad1)
    void print(std::ostream&) const;                    //!< print GeoMapData
    void print(PrintStream& os) const {print(os.currentStream());}

    friend std::ostream& operator<<(std::ostream&, const GeomMapData&);
};

std::ostream& operator<<(std::ostream&, const GeomMapData&); //!< print operator

} // end of namespace xlifepp

#endif // GEOM_MAP_DATA_HPP
