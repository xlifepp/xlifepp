/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file geometries3D-SidePart.cpp
  \authors E. Lunéville
  \since 29 mars 2021
  \date 29 mars 2021

  \brief This file contains the implementation of methods of sectors or side part child classes of 3D objects
         EllipsoidSidePart, CylinderSidePart, ConeSidePart
*/

#include "geometries1D.hpp"
#include "geometries2D.hpp"
#include "geometries3D.hpp"
#include "geometries_utils.hpp"
#include "Geodesic.hpp"
#include <cmath>

namespace xlifepp
{
//===============================================================================================
//                    EllipsoidSidePart stuff
//===============================================================================================
//! explicit constructor
EllipsoidSidePart::EllipsoidSidePart(const Geometry& g, real_t tmin, real_t tmax, real_t pmin, real_t pmax, EllipsoidParametrizationType ep)
{
  if (g.shape() != _ellipsoid && g.shape() != _ball)
  { error("free_error", "EllipsoidSidePart constructor requires an Ellipsoid/Ball object"); }
  ellipsoid = Ellipsoid(*g.ellipsoid());
  shape_ = _ellipsoidSidePart;
  if (tmin < -pi_ || tmin > pi_ || tmax < -pi_ || tmax > pi_) { error("free_error", "tmin and tmax should belong to [-pi,pi]"); }
  if (pmin < -pi_ / 2 || pmin > pi_ / 2 || pmax < -pi_ / 2 || pmax > pi_ / 2) { error("free_error", "pmin and pmax should belong to [-pi/2,pi/2]"); }
  theta_min = tmin; theta_max = tmax; phi_min = pmin; phi_max = pmax;
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  if (ep==_bispherical) pars<<Parameter(0,"mapIndex")<<Parameter(0.,"dist");
  parametrization_ = new Parametrization(SquareGeo(Point(0., 0.), Point(1., 0.), Point(0., 1.)), parametrization_EllipsoidSidePart,
                                         pars, "EllipsoidSidePart parametrization");
  parametrization_->setinvParametrization(invParametrization_EllipsoidSidePart);
  if (!isFull() && ep != _thetaphi)
  { error("free_error", "for partial EllipsoidSide, only the theta-phi paramatrization is available"); }
  parametrizationType = ep;
  if (ep == _thetaphi || ep == _bistereographic || ep==_bispherical) { parametrization_->periods[0] = 1; } // u-periodic par(u+1,v)=par(u,v)
  if (ep==_bispherical) parametrization_->nbOfMaps=2;
  parametrization_->geomP() = this;
}

bool EllipsoidSidePart::isFull() const
{
  return theta_min == -pi_ && theta_max == pi_ && phi_min == -pi_ / 2 && phi_max == pi_ / 2;
}

//! EllipsoidSidePart parametrization
Vector<real_t> EllipsoidSidePart::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  switch (parametrizationType)
  {
    case _stereographic: return stereographicParametrization(pt, pars, d);
    case _bistereographic: return biStereographicParametrization(pt, pars, d);
    case _bispherical: return biSphericalParametrization(pt, pars, d);
    default: break;
  }
  return thetaPhiParametrization(pt, pars, d);
}

//! inverse of EllipsoidSidePart parametrization (u,v)=invf(p)
Vector<real_t> EllipsoidSidePart::invParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  switch (parametrizationType)
  {
    case _stereographic: return invStereographicParametrization(pt, pars, d);
    case _bistereographic: return invBiStereographicParametrization(pt, pars, d);
    case _bispherical: return invBiSphericalParametrization(pt, pars, d);
    default: break;
  }
  return invThetaPhiParametrization(pt, pars, d);
}

/*!  (theta-phi) parametrization of EllipsoidSidePart
     theta = (1-u)theta_min + u theta_max, phi = (1-v)phi_min + v phi_max
     P - P0 = (P1-P0) cos(theta) cos(phi) + (P2-P0) sin(theta) cos(phi) + (P6-P0) sin(phi)
*/
Vector<real_t> EllipsoidSidePart::thetaPhiParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  real_t u = pt[0], v = pt[1];
  real_t dt = theta_max - theta_min, the = theta_min + u * dt,
         dp = phi_max - phi_min, phi = phi_min + v * dp;
  //to avoid singularities in case |phi|=pi/2
  if(std::abs(phi-0.5*pi_)<theTolerance) phi=0.5*pi_-theTolerance;
  if(std::abs(phi+0.5*pi_)<theTolerance) phi=-0.5*pi_+theTolerance;
  real_t ct = std::cos(the), st = std::sin(the), cp = std::cos(phi), sp = std::sin(phi);
  const std::vector<Point>& P = ellipsoid.p();
  Point R = P[0], P1 = P[1], P2 = P[2], P6 = P[6];
  P1 -= R; P2 -= R; P6 -= R;
  switch (d)
  {
    case _id: R += cp * (ct * P1 + st * P2) + sp * P6;                  break;
    case _d1: R *= 0; R += cp * dt * (ct * P2 - st * P1);               break;
    case _d2: R *= 0; R += cp * dp * P6 - dp * sp * (ct * P1 + st * P2);    break;
    case _d11: R *= 0; R += -cp * dt * dt * (st * P2 + ct * P1);           break;
    case _d22: R *= 0; R += -(dp * dp) * (sp * P6 + cp * (ct * P1 + st * P2)); break;
    case _d12:
    case _d21: R *= 0; R += sp * dp * dt * (st * P1 - ct * P2);            break;
    default: parfun_error("EllipsoidSidePart parametrization", d);
  }
  R = roundToZero(R, theEpsilon);
  return R;
}

//! inverse of EllipsoidSidePart parametrization (u,v)=invf(p)
Vector<real_t> EllipsoidSidePart::invThetaPhiParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  if (d != _id) { parfun_error("EllipsoidSidePart::invParametrization", d); }
  Vector<real_t> uv(2, 0.);
  const std::vector<Point>& P = ellipsoid.p();
  Point R = P[0], P1 = P[1], P2 = P[2], P6 = P[6];
  P1 -= R; P2 -= R; P6 -= R;
  Point Q = pt; Q -= R;
  Point u1 = crossProduct(P2, P6), u2 = crossProduct(P1, P6), u3 = crossProduct(P1, P2);
  real_t sp = dot(Q, u3) / dot(P6, u3); // dot(P6,u3)=0 => degenerated ellipsoid
  if (std::abs(sp) > 1 + theTolerance)
  { error("free_error", "EllipsoidSidePart::invParametrization fails because point is not located in the ellipsoid side part"); }
  if (sp >= 1) //north pole return (0,1)
  {uv[1] = 1; return uv;}
  if (sp <= -1) //south pole return (0,0)
  {return uv;}
  uv[1] = std::asin(sp);
  real_t cp = std::cos(uv[1]);
  if (std::abs(cp) > theTolerance) // if not, apogee of ellipsoid, take the=0;
  {
    real_t st = dot(Q, u2) / (cp * dot(P2, u2)), ct = dot(Q, u1) / (cp * dot(P1, u1));
    uv[0] = std::atan2(st, ct);
  }
  //return to original u,v in [0,1]
  uv[0] = (uv[0] - theta_min) / (theta_max - theta_min);
  uv[1] = (uv[1] - phi_min) / (phi_max - phi_min);
  return uv;
}

/*!  bi-theta-phi parametrization of EllipsoidSidePart based on two theta-phi maps
     nu > 0 (default pi/10), -pi <= theta <=pi and  -pi/2+nu <= phi <= pi/2-nu

     1) map with P6 as north pole
        P - P0 = (P1-P0) cos(theta) cos(phi) + (P2-P0) sin(theta) cos(phi) + (P6-P0) sin(phi)
        a1=(P-P0,P6-P0)/(P6-P0,P6-P0), d1=min(|1-a1|,|1+a1|) minimal 'distance' to poles

     2) map with P1 as north pole
        P - P0 = (P1-P0) sin(phi) + (P2-P0) sin(theta) cos(phi) + (P6-P0) cos(theta) cos(phi)
        a2=(P-P0,P1-P0)/(P1-P0,P1-P0), d2=min(|1-a2|,|1+a2|) minimal 'distance' to poles

     note that |a1|<cos(nu) and |a2|<cos(nu)

     map is chosen according the parameter "mapIndex"
     does not managed theta_min, theta_max, phi_min, phi_max
*/

Vector<real_t> EllipsoidSidePart::biSphericalParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  GeomElement* gelt=getElementP();
  if(gelt!=nullptr)
  {
    MeshElement* melt=gelt->meshElement();
    if(melt!=nullptr)
      {
         pars("mapIndex")= melt->mapIndex;
         if(melt->sideMapTransition.size()>0) return biSphericalExtension(melt,pt,pars,d);
      }
  }
  return biSphericalParametrizationStandard(pt,pars,d);
}

Vector<real_t> EllipsoidSidePart::biSphericalParametrizationStandard(const Point& pt, Parameters& pars, DiffOpType d) const
{
  real_t u = pt[0], v = pt[1]; // in [0,1]
  real_t the = pi_*(2*u-1), phi = pi_*(v-0.5);
  number_t mapIndex = pars("mapIndex");
  real_t ct = std::cos(the), st = std::sin(the), cp = std::cos(phi), sp = std::sin(phi);
  const std::vector<Point>& P = ellipsoid.p();
  Point R = P[0], P1 = P[1], P2 = P[2], P6 = P[6];
  P1 -= R; P2 -= R; P6 -= R;
  if(mapIndex==1)
  {
    switch (d)
    {
     case _id: R += cp * (ct * P1 + st * P2) + sp * P6;                  break;
     case _d1: R *= 0; R += cp * 2*pi_ * (ct * P2 - st * P1);               break;
     case _d2: R *= 0; R += cp * pi_ * P6 - pi_ * sp * (ct * P1 + st * P2);    break;
     case _d11: R *= 0; R += -cp * 4*pi_*pi_ * (st * P2 + ct * P1);           break;
     case _d22: R *= 0; R += -(pi_ * pi_) * (sp * P6 + cp * (ct * P1 + st * P2)); break;
     case _d12:
     case _d21: R *= 0; R += sp * pi_ * 2*pi_ * (st * P1 - ct * P2);            break;
     default: parfun_error("EllipsoidSidePart::biSphericalParametrization", d);
    }
  }
  else
  {
    switch (d)
    {
     case _id: R += cp * (ct * P6 + st * P2) + sp * P1;                  break;
     case _d1: R *= 0; R += cp * 2*pi_ * (ct * P2 - st * P6);               break;
     case _d2: R *= 0; R += cp * pi_ * P1 - pi_ * sp * (ct * P6 + st * P2);    break;
     case _d11: R *= 0; R += -cp * 4*pi_*pi_ * (st * P2 + ct * P6);           break;
     case _d22: R *= 0; R += -(pi_ * pi_) * (sp * P1 + cp * (ct * P6 + st * P2)); break;
     case _d12:
     case _d21: R *= 0; R += sp * pi_ * 2*pi_ * (st * P6 - ct * P2);            break;
     default: parfun_error("EllipsoidSidePart::biSphericalParametrization", d);
    }
  }
  R = roundToZero(R, theEpsilon);
  return R;
}

//truncture function used by biSphericalExtension (0<=a<1)
//          | 0                        if t<=a
// rho(t) = | (t-a)^2(3-a-2t)/(1-a)^3  if a<=t<=1
//          | 1                        if t<=0
// C1 function on R
real_t rhoExt(real_t t, real_t a=0.5)
{
    real_t a1=1-a;
    if(t<=a)  return 0.;
    if(t>=1.) return 1.;
    return (t-a)*(t-a)*(3-a-2*t)/(a1*a1*a1);
}

/*! extended biSpherical parametrization on a transition element
    see documentation for details
*/
Vector<real_t> EllipsoidSidePart::biSphericalExtension(MeshElement* melt, const Point& M, Parameters& pars, DiffOpType d) const
{
  const std::vector<Point>& P = ellipsoid.p();
  number_t nbs=melt->numberOfSides();
  auto it=melt->isoNodes.begin();
  Point Gt=**it;it++;
  for(number_t i=1;i<nbs;i++, it++) Gt+=**it;
  Gt/=nbs;
  for(number_t s=1; s<=nbs;s++)
  {
     int sp1=s+1, sp2=s+2;
     if(sp1>nbs) {sp1=sp1-nbs;}
     if(sp2>nbs) {sp2=sp2-nbs;}
     const Point &S1=melt->vertex(s), &S2=melt->vertex(sp1);
     const Point &St1=*melt->isoNodes[s-1], &St2=*melt->isoNodes[sp1-1], &St3=*melt->isoNodes[sp2-1];
     if(isPointInTriangle(M, Gt, St1, St2))
     {
       if(melt->sideMapTransition.find(s)==melt->sideMapTransition.end() || dist(M,Gt)<theTolerance) // no extension
          return biSphericalParametrizationStandard(M,pars,d);
       //use extension in side direction
       Point u=St2-St1, v=St3-St1;
       Point n=v -(dot(u,v)/dot(u,u))*u;
       if(dot(St1-Gt,n) <0) n*=-1;
       //intersection M + aGtM (a>0) and [St1,St2] : Pt = M + (MSt1.n/GtM.n)GtM     G!=M
       Point GtM = M-Gt;
       real_t a = dot(St1-M,n)/dot(GtM,n);
       Point Pt = M + a*GtM;
       real_t eta = rhoExt(norm(GtM)/norm(Pt-Gt));
       if(eta==0.) return biSphericalParametrizationStandard(M,pars,d); // outside of extension area
       //intersection of M + bGtM (b>0) and the curve C = inv([S1,S2] -> Qt
       //  solve f(b)=0 with f(a)= phi(M + bGtM).n with n = (S1-P0) x (S2-P0)
       //  using Newton algorithm
       //  b_k+1=bk-f(b_k)/f'(b_k) and b_0=a
       //  with f'(b)= (d1 phi(M + bGtM).n,d2 phi(M + bGtM).n).GtM
       n=cross3D(S1-P[0],S2-P[0]);
       number_t k=0, kmax=20;
       real_t eps = norm(Pt-Gt)/1000, ek= theRealMax, bk=a;
       Point Qt=Pt;
       Point Pk = biSphericalParametrizationStandard(Qt, pars, _id);
       Point d1fb=biSphericalParametrizationStandard(Qt, pars, _d1);
       Point d2fb=biSphericalParametrizationStandard(Qt, pars, _d2);
       real_t fk=dot(Pk-P[0],n);
       real_t dfk=dot(d1fb,n)*GtM[0]+dot(d2fb,n)*GtM[1];
       while(k<kmax && std::abs(ek) > eps && std::abs(dfk) > eps)
       {
          ek=fk/dfk;
          bk-=ek;
          Qt=M+bk*GtM;
          Pk = biSphericalParametrizationStandard(Qt, pars, _id);
          fk=dot(Pk-P[0],n);
          d1fb=biSphericalParametrizationStandard(Qt, pars, _d1);
          d2fb=biSphericalParametrizationStandard(Qt, pars, _d2);
          dfk=dot(d1fb,n)*GtM[0]+dot(d2fb,n)*GtM[1];
       }
       if(k==kmax)
         warning("free_warning","no convergence of Newton algorithm in EllipsoidSidePart::biSphericalExtension");

       //apply scaling S: M -> M+rho(|GtM|/|GtPt|)(Qt-M)  rho(0)=0, rho(1)=1 -> S(Gt)=Gt, S(Pt)=Qt
       return biSphericalParametrizationStandard(M+eta*(Qt-M),pars,d);
     }
  }
  return biSphericalParametrizationStandard(M,pars,d);
}

/*! inverse of EllipsoidSidePart biSpherical parametrization (u,v)=invf(p)
    mapIndex = 1 use first spherical map,  compute d1=min(|P-P6|,|P+P6|),
    mapIndex = 2 use second spherical map, compute d2=min(|P-P1|,|P+P1|),
    mapIndex = 0 use 'best' spherical map, map 1 if d1>=d2, else map 2
    di is strored in parametres pars("dist")
*/

Vector<real_t> EllipsoidSidePart::invBiSphericalParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  if (d != _id) { parfun_error("EllipsoidSidePart::invBiSphericalParametrization", d); }
  number_t mapIndex = pars("mapIndex");
  Vector<real_t> uv(2, 0.);
  const std::vector<Point>& P = ellipsoid.p();
  Point R = P[0], P1 = P[1], P2 = P[2], P6 = P[6];
  real_t di=std::min(pt.distance(P6), pt.distance(-P6));
  real_t d2=std::min(pt.distance(P1), pt.distance(-P1));
  if(mapIndex==0)
  {
     mapIndex=1;
     if(d2>di) mapIndex=2;
  }
  if(mapIndex==2) {P1=P[6];P6=P[1];di=d2;}
  pars("dist")=di;
  P1 -= R; P2 -= R; P6 -= R;
  Point Q = pt; Q -= R;
  Point u1 = crossProduct(P2, P6), u2 = crossProduct(P1, P6), u3 = crossProduct(P1, P2);
  real_t sp = dot(Q, u3) / dot(P6, u3); // dot(P6,u3)=0 => degenerated ellipsoid
  if (std::abs(sp) > 1 + theTolerance)
  { error("free_error", "EllipsoidSidePart::invBiSphericalParametrization fails because point is not located in the ellipsoid side part"); }
  if (sp >= 1) //north pole return (0,1)
  {uv[1] = 1; return uv;}
  if (sp <= -1) //south pole return (0,0)
  {return uv;}
  uv[1] = std::asin(sp);
  real_t cp = std::cos(uv[1]);
  if (std::abs(cp) > theTolerance) // if not, apogee of ellipsoid, take the=0;
  {
    real_t st = dot(Q, u2) / (cp * dot(P2, u2)), ct = dot(Q, u1) / (cp * dot(P1, u1));
    uv[0] = std::atan2(st, ct);
  }
  //return to original u,v in [0,1]
  uv[0] = std::min(1.,std::max(0.5*(1+uv[0]/pi_),0.));
  uv[1] = std::min(1.,std::max(uv[1]/pi_ +0.5,0.));
  return uv;
}

/*! Stereographic parametrization of sphere S(C,R), (u,v) in ]0,1[x]0,1[ :
    (u,v) -> (s,t) = (tan(pi(u-0.5),tan(pi(v-0.5)) maps ]0,1[x]0,1[ to R2
    Stereographic parametrization of sphere S(C,R) (s,t) in R2
                   2s                     2t                  1-s^2-t^2
    x =  Cx + R ---------   y = Cy + R ---------   z = Cz + R ---------
                1+s^2+t^2              1+s^2+t^2              1+s^2+t^2

    (s,t) = (0,0) -> C+(0,0,R)
    (s,t) = (cos(a),sin(a)) -> C+(Rcos(a), Rsin(a),0)
    |(s,t)|-> +inf -> C+(0,0,-R)
    C-inf parametrization R^2 -> S/south

    To deal with any ellipsoid E, use the transformation that maps the unit sphere S(0,1) to E:
       (x,y,z) -> C + x(P1-C) + y(P2-C) + z(P3-C)  (P1,P2,P3 first apogees of E)
*/
Vector<real_t> EllipsoidSidePart::stereographicParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  //move [0,1[x[0,1[ to R2 with (u,v) -> (tan(pi(u-0.5),tan(pi(v-0.5))
  real_t u = pt[0], v = pt[1];
  u = std::min(std::max(theTolerance, u), 1 - theTolerance); //avoid u=0 and u=1
  v = std::min(std::max(theTolerance, v), 1 - theTolerance); //avoid v=0 and v=1
  u = std::tan(pi_ * (u - 0.5)); v = std::tan(pi_ * (v - 0.5)); //mapping to R2
  real_t  w2 = u * u + v * v, g = 1 / (1 + w2), x, y, z;
  real_t g2, u2, v2, du, dv;
  switch (d)
  {
    case _id: x = 2 * u * g; y = 2 * v * g; z = (1 - w2) * g; break;
    case _d1: g2 = g * g; du = pi_ * (1 + u * u); x = du * (2 * g - 4 * u * u * g2); y = -4 * du * u * v * g2; z = -4 * du * u * g2; break;
    case _d2: g2 = g * g; dv = pi_ * (1 + v * v); x = -4 * dv * u * v * g2; y = dv * (2 * g - 4 * v * v * g2); z = -4 * dv * v * g2; break;
    case _d11: g2 = g * g; u2 = u * u; du = pi_ * (1 + u2); du *= du; x = 4 * du * u * g2 * (4 * u2 * g - 3); y = 4 * du * v * g2 * (2 * u2 * g - 1); z = 4 * du * g2 * (2 * u2 * g - 1); break;
    case _d22: g2 = g * g; v2 = v * v; dv = pi_ * (1 + v2); dv *= dv; x = 4 * dv * u * g2 * (2 * v2 * g - 1); y = 4 * dv * v * g2 * (4 * v2 * g - 3); z = 4 * dv * g2 * (2 * v2 * g - 1); break;
    case _d12:
    case _d21: g2 = g * g; du = pi_ * (1 + u * u); du *= pi_ * (1 + v * v); x = 4 * du * v * g2 * (4 * u * u * g - 1); y = 4 * du * u * g2 * (4 * v * v * g - 1); z = 16 * du * u * v * g2 * g; break;
    default: parfun_error("EllipsoidSidePart stereographic parametrization", d);
  }
  //move to ellipsoid of axes (P1-P0,P2-P0,P6-P0)
  const std::vector<Point>& P = ellipsoid.p();
  Point R = P[0];
  R *= (1 - x - y - z); R += x * P[1]; R += y * P[2]; R += z * P[6];
  return R;
}

//! inverse of stereographic parametrization: u = (x-Cx)/(R+z-Cz)   v = (y-Cy)/(R+z-Cz)
Vector<real_t> EllipsoidSidePart::invStereographicParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  //move to unit sphere
  const std::vector<Point>& P = ellipsoid.p();
  Point P0 = pt - P[0], P1 = P[1] - P[0], P2 = P[2] - P[0], P3 = P[6] - P[0];
  Point Q = cross3D(P1, P2); real_t z = dot(P0, Q) / dot(P3, Q);
  if (std::abs(z) > 1) { error("free_error", "EllipsoidSidePart::invStereographicParametrization fails, point does not belong to ellipsoid"); }
  Vector<real_t> uv(2);
  Q = cross3D(P2, P3); uv[0] = dot(P0, Q) / dot(P1, Q) / (1 + z);
  Q = cross3D(P1, P3); uv[1] = dot(P0, Q) / dot(P2, Q) / (1 + z);
  uv[0] = 0.5 + std::atan(uv[0]) / pi_;
  uv[1] = 0.5 + std::atan(uv[1]) / pi_;
  return uv;
}

/*! Bi-stereographic parametrization of sphere S(C,R), u in [0,1], v in [0,1] (t=2pi.v-pi) :

                                4ucos(t)               4usin(t)              4u^2-1
    0 <= u <= 0.5 : x =  Cx + R --------    y = Cy + R --------   z = Cz + R ------
                                 1+4u^2                 1+4u^2               4u^2+1

                                4(1-u)cos(t)               4(1-u)sin(t)              1-4(1-u)^2
    0.5 < u <= 1  : x =  Cx + R ------------    y = Cy + R ------------   z = Cz + R ----------
                                 1+4(1-u)^2                 1+4(1-u)^2               1+4(1-u)^2

    u = 0 -> C+(0,0,-R)
    u = 0.5 -> C+(Rcos(t), Rsin(t),0)
    u = 1 -> C+(0,0,R)
    0<t<1 gives circle parallel to the xy plane
    0<u<1 gives meridian parallel to perpendicular plane to xy plane

    C0 Parametrization [0,1]x[0,1]->S, but not C1 at u=0.5 !

    To deal with any ellipsoid E, use the transformation that maps the unit sphere S(0,1) to E:
       (x,y,z) -> C + x(P1-C) + y(P2-C) + z(P3-C)  (P1,P2,P3 first apogees of E)
*/
Vector<real_t> EllipsoidSidePart::biStereographicParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  real_t u = pt[0], v = pt[1], t = pi_ * (2 * v - 1), x, y, z;
  real_t ct = 4 * std::cos(t), st = 4 * std::sin(t), s = 1;
  if (u > 0.5)
  {
    u = 1 - u;
    s = -1;
  }
  if (u < 100*theEpsilon) { u = 100*theEpsilon; } //to avoid 0 degenerency
  real_t g = 1 / (1 + 4 * u * u), dg, d2g;
  switch (d)
  {
    case _id: x = u * ct * g; y = u * st * g; z = (4 * u * u - 1) * g; z *= s; break;
    case _d1: dg = -8 * u * g * g; x = s * ct * (g + u * dg); y = s * st * (g + u * dg); z = 8 * u * g + (4 * u * u - 1) * dg; break;
    case _d2: x = -2 * pi_ * u * st * g; y = 2 * pi_ * u * ct * g; z = 0; break;
    case _d11: dg = -8 * u * g * g; d2g = 8 * g * g * (16 * u * u * g - 1); x = ct * (2 * dg + u * d2g); y = st * (2 * dg + u * d2g); z = 8 * g + 16 * u * dg + (4 * u * u - 1) * d2g; z *= s; break;
    case _d22: x = -4 * pi_ * pi_ * u * ct * g; y = -4 * pi_ * pi_ * u * st * g; z = 0; break;
    case _d12:
    case _d21: dg = -8 * u * g * g; x = -2 * pi_ * s * st * (g + u * dg); y = 2 * pi_ * s * ct * (g + u * dg); z = 0; break;
    default: parfun_error("EllipsoidSidePart stereographic parametrization", d);
  }
  //thePrintStream<<std::scientific<<"u="<<u<<" v="<<v<<" x="<<x<<" y="<<y<<" z="<<z<<eol;
  //move to ellipsoid of axes (P1-P0,P2-P0,P6-P0)
  const std::vector<Point>& P = ellipsoid.p();
  Point R = P[0];
  R *= (1 - x - y - z); R += x * P[1]; R += y * P[2]; R += z * P[6];
  return R;
}

//! inverse of bi-stereographic parametrization
Vector<real_t> EllipsoidSidePart::invBiStereographicParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  //move to unit sphere
  const std::vector<Point>& P = ellipsoid.p();
  Point P0 = pt - P[0], P1 = P[1] - P[0], P2 = P[2] - P[0], P3 = P[6] - P[0];
  Point Q = cross3D(P1, P2); real_t z = dot(P0, Q) / dot(P3, Q);
  if (std::abs(z) > 1) { error("free_error", "EllipsoidSidePart::invBiStereographicParametrization fails, point does not belong to ellipsoid"); }
  Q = cross3D(P2, P3); real_t x = dot(P0, Q) / dot(P1, Q);
  Q = cross3D(P1, P3); real_t y = dot(P0, Q) / dot(P2, Q);
  Vector<real_t> uv(2);
  if (z <= 0) { uv[0] = 0.5 * std::sqrt((1 + z) / (1 - z)); }
  else     { uv[0] = 1 - 0.5 * std::sqrt((1 - z) / (1 + z)); }
  uv[1] = (1 + std::atan2(y, x) / pi_) / 2;
  return uv;
}

/*! build a geometric geodesic starting at (x,dx)
    x must be located on ellipsoid and dx must in the tangent space (not checked here)
*/
GeometricGeodesic EllipsoidSidePart::geodesic(const Point& x, const Point& dx, bool wCA, bool wT)
{
  if (!ellipsoid.isBall() || !isFull()) { error("free_error", " building geometric geodesic is only available for a full sphere"); }
  const std::vector<Point>& P = ellipsoid.p();
  real_t R = ellipsoid.radius1();
  Point O = P[0];
  Point d = dx / norm(dx);
  Point x2 = O + R * d, x3 = (1 + R) * O - R * x, x4 = O - R * d;
  Geometry g = CircArc(_center = O, _v1 = x, _v2 = x2) + CircArc(_center = O, _v1 = x2, _v2 = x3) + CircArc(_center = O, _v1 = x3, _v2 = x4) + CircArc(_center = O, _v1 = x4, _v2 = x);
  return GeometricGeodesic(g, x, dx, wCA, wT);
}

//! format as string: "EllipsoidSidePart ...)"
string_t EllipsoidSidePart::asString() const
{
  string_t s("EllipsoidSidePart: ");
  s += ellipsoid.asString();
  s += ", theta in [" + tostring(theta_min) + "," + tostring(theta_max) + "], phi in ["
       + tostring(phi_min) + "," + tostring(phi_max) + "]";
  return s;
}

//===============================================================================================
//                    TrunkSidePart stuff
//===============================================================================================
//! explicit constructor from Trunk and angles in [0,2pi]
TrunkSidePart::TrunkSidePart(const Geometry& g, real_t tmin, real_t tmax, bool sh)
{
  const Trunk* tr = dynamic_cast<const Trunk*>(&g);
  if (tr == nullptr)  { error("free_error", "TrunkSidePart constructor requires a Trunk object"); }
  if (!tr->isElliptical())
  { error("free_error", "TrunkSidePart constructor with angles requires an elliptical trunk"); }
  if (std::abs(tmax - tmin) < theTolerance)
  { error("free_error", "TrunkSidePart constructor with too close angles"); }
  const Ellipse* ell = static_cast<const Ellipse*>(tr->basis());
  trunk_ = Trunk(_basis = *ell, _origin = tr->origin(), _scale = tr->scale());
  shape_ = _trunkSidePart;
  fromAngles_ = true;
  if (tmin < 0) {tmin += pi_; tmax += pi_;} //assume angle in [-pi,pi], translate to [0,2pi]
  t_min = tmin; t_max = tmax; // bound angles
  //t_min=0.;t_max=1.;    // full parametrization
  tmin /= (2 * pi_); tmax /= (2 * pi_);
  shortest_ = sh;
  Point C = trunk_.basis()->p(1), A = trunk_.basis()->p(2);
  Point P1 = ell->parametrization()(1., tmin), P2 = ell->parametrization()(1., tmax);
  basisBoundary_ = new EllArc(_center = C, _apogee = A, _v1 = P1, _v2 = P2);
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  parametrization_ = new Parametrization(SquareGeo(Point(0., 0.), Point(1., 0.), Point(0., 1.)), parametrization_TrunkSidePart,
                                         pars, "TrunkSidePart parametrization");
  parametrization_->setinvParametrization(invParametrization_TrunkSidePart);
  parametrization_->geomP() = this;
}

//! explicit constructor from Trunk and points defining angles
TrunkSidePart::TrunkSidePart(const Geometry& g, const Point& a, const Point& b, bool sh)
{
  const Trunk* tr = dynamic_cast<const Trunk*>(&g);
  if (tr == nullptr)  { error("free_error", "TrunkSidePart constructor requires a Trunk object"); }
  trunk_ = Trunk(_basis = *tr->basis(), _origin = tr->origin(), _scale = tr->scale());
  shape_ = _trunkSidePart;
  fromAngles_ = false;
  basisBoundary_ = trunk_.basis()->boundary().clone();
  t_min = 0.; t_max = 1.; //default value if no point given, all basis boundary is selected
  if (a.size() > 0 && &a != &b)
  {
    t_min = basisBoundary_->parametrization().toParameter(a)[0];
    t_max = basisBoundary_->parametrization().toParameter(b)[0];
  }
  shortest_ = sh;
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  parametrization_ = new Parametrization(SquareGeo(Point(0., 0.), Point(1., 0.), Point(0., 1.)), parametrization_TrunkSidePart,
                                         pars, "TrunkSidePart parametrization");
  parametrization_->setinvParametrization(invParametrization_TrunkSidePart);
  parametrization_->geomP() = this;
}

/*!  Parametrization of TrunkSidePart
     Trunk is defined by its basis_ (B), center1 (C) in basis, origin_(O) in top and scale factor (s) of the top
     general parametrisation of a side part ( g(u) parametrization of the basis boundary)
            P(u,v) = (1-v+sv)g(u) + v(O-sC)
*/
Vector<real_t> TrunkSidePart::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  //real_t u=t_min+pt[0]*(t_max-t_min), v=pt[1];
  real_t u = pt[0], v = pt[1];
  const Point& C = trunk_.center1();
  const Point& O = trunk_.origin();
  real_t s = trunk_.scale();
  Point R = O, Q = O - s * C;
  const Parametrization& par = basisBoundary_->parametrization();
  switch (d)
  {
    case _id: R = (1 - v * (1 - s)) * par(u) + v * Q;  break;
    case _d1: R = (1 - v * (1 - s)) * par(u, _d1);  break;
    case _d2: R = (s - 1) * par(u) + Q;          break;
    case _d11: R = (1 - v * (1 - s)) * par(u, _d11); break;
    case _d22: R *= 0;                      break;
    case _d12:
    case _d21: R = (s - 1) * par(u, _d1);        break;
    default: parfun_error("TrunkSidePart parametrization", d);
  }
  return R;
}

//! inverse of TrunkSidePart parametrization (u,v)=invf(p)
Vector<real_t> TrunkSidePart::invParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  if (d != _id) { parfun_error("TrunkSidePart::invParametrization", d); }
  Vector<real_t> uv(2, 0.);
  const Point& C = trunk_.center1(), &O = trunk_.origin();
  Point n = cross3D(trunk_.basis()->p(2) - C, trunk_.basis()->p(3) - C); n /= norm(n); // unitary normal vector to basis

  real_t s = trunk_.scale();
  uv[1] = dot(pt - C, n) / dot(O - C, n);
  //theCout<<"\nTrunkSidePart::invParametrization pt="<<pt<<" uv[1]="<<uv[1]<<std::flush;
  if (uv[1] < -theTolerance || uv[1] > 1 + theTolerance) {uv.clear(); return uv;}
  uv[1] = std::max(std::min(uv[1], 1.), 0.);
  real_t a = 1 - (1 - s) * uv[1];
  Point Q = s * C - O; Q *= uv[1]; Q += pt; Q /= a; // (pt-v*(O-sC)) / (1-(1-s)v)
  Point t = basisBoundary_->parametrization().toParameter(Q);
  //theCout<<" Q="<<Q<<" t="<<t;
  if (t.size() == 0) {uv.clear(); return uv;}
  uv[0] = t[0];
  if (uv[0] < -theTolerance || uv[0] > 1. + theTolerance) {uv.clear(); return uv;}
  uv[0] = std::max(std::min(uv[0], 1.), 0.);
  return uv;
}

//! format as string: "TrunkSidePart ...)"
string_t TrunkSidePart::asString() const
{
  string_t s("TrunkSidePart: ");
  s += trunk_.asString();
  s += ", t in [" + tostring(t_min) + "," + tostring(t_max) + "]";
  return s;
}

} // end of namespace xlifepp
