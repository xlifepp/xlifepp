/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file geometries3D-Trunk.cpp
  \authors N. Kielbasiewicz, Y. Lafranche
  \since 18 oct 2012
  \date 9 sep 2015

  \brief This file contains the implementation of methods of Trunk and non revolution child classes defined in geometries3D.hpp
*/

#include "geometries2D.hpp"
#include "geometries3D.hpp"
#include "geometries_utils.hpp"
#include <cmath>

namespace xlifepp
{
//===============================================================================================
//                                     Trunk stuff
//===============================================================================================
Trunk::Trunk(real_t scale, bool defineBasisAndP) : Volume(), scale_(scale), origin_(0., 0., 1.), center1_(0., 0., 0.), p1_(1., 0., 0.),
  p2_(0., 1., 0.), isElliptical_(true), isN_(true)
{
  basis_ = nullptr;
  top_ = nullptr;
  shape_ = _trunk;
  if (defineBasisAndP) { basis_ = new Disk(center1_, p1_, p2_, std::vector<number_t>(4, 2)); }
  if (scale_ == 0.)
  {
    n_.resize(8, 2);
    if (defineBasisAndP)
    {
      p_.resize(6);
      for (number_t i = 0; i < 5; ++i) { p_[i] = basis_->p(i + 1); }
      p_[5] = origin_;
    }
  }
  else
  {
    n_.resize(12, 2);
    if (defineBasisAndP)
    {
      p_.resize(10);
      for (number_t i = 0; i < 5; ++i)
      {
        p_[i] = basis_->p(i + 1);
        p_[i + 5] = origin_ + scale_ * (basis_->p(i + 1) - basis_->p(1));
      }
    }
  }
  h_.resize(0);
  if (scale_ <= 1.)
  {
    boundingBox = BoundingBox(3.*center1_ - p1_ - p2_,
                              center1_ + p1_ - p2_,
                              center1_ + p2_ - p1_,
                              2.*center1_ + origin_ - p1_ - p2_);
    minimalBox = MinimalBox(3.*center1_ - p1_ - p2_,
                            center1_ + p1_ - p2_,
                            center1_ + p2_ - p1_,
                            2.*center1_ + origin_ - p1_ - p2_);
  }
  else
  {
    boundingBox = BoundingBox((1. + 2.*scale_) * center1_ - scale_ * (p1_ + p2_),
                              center1_ + scale_ * (p1_ - p2_),
                              center1_ + scale_ * (p2_ - p1_),
                              2.*scale_ * center1_ + origin_ - scale_ * (p1_ - p2_));
    minimalBox = MinimalBox((1. + 2.*scale_) * center1_ - scale_ * (p1_ + p2_),
                            center1_ + scale_ * (p1_ - p2_),
                            center1_ + scale_ * (p2_ - p1_),
                            2.*scale_ * center1_ + origin_ - scale_ * (p1_ - p2_));
  }
}

void Trunk::buildPAndBasis()
{
  bool ellipticalBasis = false;
  if (basis_ != nullptr) { ellipticalBasis = (basis_->shape() == _ellipse || basis_->shape() == _disk); } //user data _basis= ...
  if (ellipticalBasis)
  {
    center1_ = basis_->p(1);
    p_.resize(10);
    for (number_t i = 0; i < 5; ++i)
    {
      p_[i] = basis_->p(i + 1);
      p_[i + 5] = origin_ + scale_ * (basis_->p(i + 1) - center1_);
    }
    return;
  }
  
  if (isElliptical_) // user data _center1= ...
  {
    if (dot(p1_ - center1_, p2_ - center1_) > theTolerance) { error("geometry_incoherent_points", words("shape", _trunk)); }
    if (std::abs(center1_.distance(p1_) - center1_.distance(p2_)) < theTolerance)
    {
      if (isN_) { basis_ = new Disk(center1_, p1_, p2_, Numbers(n_[0], n_[1], n_[2], n_[3]), oneOfStrings(sideNames_)); }
      else { basis_ = new Disk(center1_, p1_, p2_, Reals(h_[0], h_[1], h_[2], h_[3]), oneOfStrings(sideNames_)); }
    }
    else
    {
      if (isN_) { basis_ = new Ellipse(center1_, p1_, p2_, Numbers(n_[0], n_[1], n_[2], n_[3]), oneOfStrings(sideNames_)); }
      else { basis_ = new Ellipse(center1_, p1_, p2_, Reals(h_[0], h_[1], h_[2], h_[3]), oneOfStrings(sideNames_)); }
    }
    p_.resize(10);
    for (number_t i = 0; i < 5; ++i)
    {
      p_[i] = basis_->p(i + 1);
      p_[i + 5] = origin_ + scale_ * (basis_->p(i + 1) - basis_->p(1));
    }
  }
  else //not elliptical
  {
    number_t bsize = basis_->boundNodes().size();
    number_t bpsize = basis_->p().size();
    number_t psize = 2 * bpsize;
    p_.resize(psize);
    for (number_t i = 0; i < bpsize; ++i)
    {
      p_[i] = basis_->p(i + 1);
      p_[i + bpsize] = origin_ + scale_ * (basis_->p(i + 1) - basis_->p(1));
    }
    if (isN_)
    {
      for (number_t i = 0; i < bsize; ++i) { basis_->n(i + 1) = (n_[i] > 2) ? n_[i] : 2; }
    }
    else
    {
      basis_->h().resize(bsize);
      for (number_t i = 0; i < bsize; ++i) { basis_->h(i + 1) = h_[i]; }
    }
  }
}

void Trunk::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Trunk::build");
  shape_ = _trunk;
  basis_ = nullptr;
  top_ = nullptr;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _trunk)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }

    // user must use only one of the following combination: (basis,origin,scale) or (center1,v1,v2,center2,scale)
    if ((key == _pk_center1 || key == _pk_v1 || key == _pk_v2 || key == _pk_center2) && usedParams.find(_pk_basis) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_basis)); }
    if ((key == _pk_center1 || key == _pk_v1 || key == _pk_v2 || key == _pk_center2) && usedParams.find(_pk_origin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_origin)); }
    if ((key == _pk_basis || key == _pk_origin) && usedParams.find(_pk_center1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center1)); }
    if ((key == _pk_basis || key == _pk_origin) && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_basis || key == _pk_origin) && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if ((key == _pk_basis || key == _pk_origin) && usedParams.find(_pk_center2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center2)); }
  }
  
  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (basis,origin,scale) or (center1,v1,v2,center2,scale) has to be set (no default values)
  if (params.find(_pk_scale) != params.end()) { error("param_missing", "scale"); }
  if (params.find(_pk_basis) == params.end() && params.find(_pk_origin) != params.end()) { error("param_missing", "origin"); }
  if (params.find(_pk_origin) == params.end() && params.find(_pk_basis) != params.end()) { error("param_missing", "basis"); }
  if (params.find(_pk_center1) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_center1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_center1) == params.end() && params.find(_pk_center2) != params.end()) { error("param_missing", "center2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_center1) != params.end()) { error("param_missing", "center1"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_center2) != params.end()) { error("param_missing", "center2"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_center1) != params.end()) { error("param_missing", "center1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_center1) != params.end()) { error("param_missing", "center2"); }
  if (params.find(_pk_center2) == params.end() && params.find(_pk_center1) != params.end()) { error("param_missing", "center1"); }
  if (params.find(_pk_center2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_center2) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  
  isElliptical_ = true;
  // now, we clean unwanted keys
  if (params.find(_pk_center1) != params.end())
  { params.erase(_pk_center1); params.erase(_pk_v1); params.erase(_pk_v2); params.erase(_pk_center2); isElliptical_ = false; }
  if (params.find(_pk_basis) != params.end()) { params.erase(_pk_basis); params.erase(_pk_origin); }
  
  bool ellipticalBasis = false;
  if (basis_ != nullptr) { ellipticalBasis = (basis_->shape() == _ellipse || basis_->shape() == _disk); }
  
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  
  // checking if the user want to define a cone, to tell him to use the rightful object
  if (!isElliptical_)
  {
    if (scale_ == 0.)
    {
      if (basis_->isPolygon())  { error("bad_geometry", asString(), words("shape", shape_), words("shape", _pyramid)); }
      else { error("bad_geometry", asString(), words("shape", shape_), words("shape", _cone)); }
    }
  }
  
  // checking nnodes and hsteps size
  isN_ = (h_.size() == 0);
  number_t bsize = 0, nsize = 0, hsize = 0;
  if (isElliptical_ || ellipticalBasis) { bsize = 4; nsize = 12; hsize = 8;}
  else
  {
    bsize = basis_->boundNodes().size();
    nsize = 3 * bsize;
    hsize = 2 * bsize;
  }
  if (isN_)
  {
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(12, n0); }
    else if (n_.size() == 3)
    {
      number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
      n_.clear();
      n_.resize(nsize, n0);
      for (number_t i = bsize; i < 2 * bsize; ++i) { n_[i] = n1; }
      for (number_t i = 2 * bsize; i < nsize; ++i) { n_[i] = n2; }
    }
    else if (n_.size() != nsize) {error("bad_size", "nnodes", nsize, n_.size()); }
  }
  if (h_.size() != 0)
  {
    if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(hsize, h0); }
    else if (h_.size() != hsize) {error("bad_size", "hsteps", hsize, h_.size()); }
  }
  
  // p_ is not (fully) built so we do
  buildPAndBasis();
  isElliptical_ = isElliptical_ || ellipticalBasis;
  boundingBox = BoundingBox(p_);
  computeMB();
  trace_p->pop();
}

void Trunk::buildParam(const Parameter& p)
{
  trace_p->push("Trunk::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_center1:
    {
      switch (p.type())
      {
        case _pt: center1_ = p.get_pt(); break;
        case _integer: center1_ = Point(real_t(p.get_i())); break;
        case _real: center1_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v1:
    {
      switch (p.type())
      {
        case _pt: p1_ = p.get_pt(); break;
        case _integer: p1_ = Point(real_t(p.get_i())); break;
        case _real: p1_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v2:
    {
      switch (p.type())
      {
        case _pt: p2_ = p.get_pt(); break;
        case _integer: p2_ = Point(real_t(p.get_i())); break;
        case _real: p2_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_center2:
    {
      switch (p.type())
      {
        case _pt: origin_ = p.get_pt(); break;
        case _integer: origin_ = Point(real_t(p.get_i())); break;
        case _real: origin_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_origin:
    {
      switch (p.type())
      {
        case _pt: origin_ = p.get_pt(); break;
        case _integer: origin_ = Point(real_t(p.get_i())); break;
        case _real: origin_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_basis:
    {
      switch (p.type())
      {
        case _pointer:
        {
          const Surface* surf = reinterpret_cast<const Surface*>(p.get_p());
          basis_ = surf->cloneS();
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_scale:
    {
      switch (p.type())
      {
        case _integer: scale_ = real_t(p.get_n()); break;
        case _real: scale_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      if (scale_ == 0.) { error("is_null", "scale"); }
      break;
    }
    case _pk_nnodes:
    {
      switch (p.type())
      {
        case _integerVector:
        {
          std::vector<number_t> n = p.get_nv();
          n_.resize(n.size());
          for (number_t i = 0; i < n.size(); ++i) { n_[i] = n[i] > 2 ? n[i] : 2; }
          break;
        }
        case _integer: n_ = std::vector<number_t>(1, p.get_n() > 2 ? p.get_n() : 2); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_hsteps:
    {
      switch (p.type())
      {
        case _realVector: h_ = p.get_rv(); break;
        case _integer: h_ = std::vector<real_t>(1, real_t(p.get_n())); break;
        case _real: h_ = std::vector<real_t>(1, p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Volume::buildParam(p); break;
  }
  trace_p->pop();
}

void Trunk::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Trunk::buildDefaultParam");
  switch (key)
  {
    case _pk_scale: scale_ = 1.; break;
    case _pk_nnodes: n_ = std::vector<number_t>(1, 2); break;
    default: Volume::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> Trunk::getParamsKeys()
{
  std::set<ParameterKey> params = Volume::getParamsKeys();
  params.insert(_pk_basis);
  params.insert(_pk_origin);
  params.insert(_pk_scale);
  params.insert(_pk_center1);
  params.insert(_pk_v1);
  params.insert(_pk_v2);
  params.insert(_pk_center2);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  return params;
}

Trunk::Trunk(Parameter p1, Parameter p2, Parameter p3) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Trunk::Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Trunk::Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Trunk::Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Trunk::Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Trunk::Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Trunk::Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Trunk::Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Trunk::Trunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  build(ps);
}

Trunk::Trunk(const Trunk& t) : Volume(t), basis_(t.basis_->cloneS()), scale_(t.scale_),
  origin_(t.origin_), center1_(t.center1_), p1_(t.p1_), p2_(t.p2_),
  isElliptical_(t.isElliptical_), isN_(t.isN_)
{
  top_ = nullptr;
  if (t.top_ != nullptr) { top_ = static_cast<Surface*>(t.top_->clone()); }
}

Trunk& Trunk::operator=(const Trunk& tr)
{
  if (this == &tr) { return *this; }
  Geometry::operator=(tr);
  p_ = tr.p_; n_ = tr.n_; h_ = tr.h_;
  if (top_ != nullptr) { delete top_; }
  if (basis_ != nullptr) { delete basis_; }
  top_ = nullptr;
  basis_ = static_cast<Surface*>(tr.basis_->clone());
  if (tr.top_ != nullptr) { top_ = static_cast<Surface*>(tr.top_->clone()); }
  scale_ = tr.scale_;
  origin_ = tr.origin_; center1_ = tr.center1_;
  p1_ = tr.p1_; p2_ = tr.p2_;
  isElliptical_ = tr.isElliptical_; isN_ = tr.isN_;
  return *this;
}

string_t Trunk::asString() const
{
  number_t i = basis_->p().size();
  string_t s("Trunk (basis = { ");
  s += basis_->asString() + " }, orig = " + p_[i].roundToZero().toString() + ", scale = " + tostring(scale_) + ")";
  return s;
}

std::vector<const Point*> Trunk::boundNodes() const
{
  std::vector<const Point*> snodes = basis_->boundNodes();
  number_t ssize = snodes.size();
  number_t ssize2 = p_.size() / 2;
  std::vector<const Point*> nodes(2 * ssize);
  number_t i = 0;
  for (number_t j = 0; j < ssize; ++j)
  {
    for (number_t k = 0; k < ssize2; ++k)
    {
      if (*snodes[j] == p_[k])
      {
        nodes[i] = snodes[j];
        nodes[i + ssize] = &p_[k + ssize2];
        ++i;
      }
    }
  }
  return nodes;
}

std::vector<Point*> Trunk::nodes()
{
  number_t ssize = p_.size();
  std::vector<Point*> nodes(ssize);
  for (number_t i = 0; i < ssize; ++i) { nodes[i] = &p_[i]; }
  return nodes;
}

std::vector<const Point*> Trunk::nodes() const
{
  number_t ssize = p_.size();
  std::vector<const Point*> nodes(ssize);
  for (number_t i = 0; i < ssize; ++i) { nodes[i] = &p_[i]; }
  return nodes;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Trunk::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > scurves = basis_->curves();
  number_t ssize = scurves.size();
  number_t ssize2 = p_.size() / 2;
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves(3 * ssize);
  for (number_t i = 0; i < ssize; ++i)
  {
    curves[i] = scurves[i];
    number_t s = scurves[i].second.size();
    std::vector<const Point*> curve(s), curve2(2);
    for (number_t j = 0; j < s; ++j)
    {
      number_t k = 0;
      for (; k < ssize2; ++k)
      {
        if (*scurves[i].second[j] == p_[k]) { curve[j] = &p_[k + ssize2]; break; }
      }
      curve2[0] = scurves[i].second[0];
      curve2[1] = curve[0];
      curves[i + ssize] = std::make_pair(scurves[i].first, curve);
      curves[i + 2 * ssize] = std::make_pair(_segment, curve2);
    }
  }
  return curves;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Trunk::surfs() const
{
  number_t ssize = basis_->surfs()[0].second.size();
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(2 + ssize);
  surfs[0] = basis_->surfs()[0];
  surfs[1] = top()->surfs()[0];
  //  number_t ssize2 = p_.size()/2;
  //  std::vector<const Point*> ssurf2(ssize);
  //  for (number_t i=0; i < ssize; ++i)
  //    for (number_t j=0; j < ssize2; ++j)
  //      if (*surfs[0].second[i] == p_[j]) ssurf2[i] = &p_[j+ssize2];
  //  surfs[1]=std::make_pair(surfs[0].first, ssurf2);
  
  for (number_t i = 0; i < ssize; ++i)
  {
    std::vector<const Point*> vertices(4);
    if (i == 0)
    {
      vertices[0] = surfs[0].second[ssize - 1]; vertices[1] = surfs[0].second[0];
      vertices[2] = surfs[1].second[0]; vertices[3] = surfs[1].second[ssize - 1];
    }
    else
    {
      vertices[0] = surfs[0].second[i - 1]; vertices[1] = surfs[0].second[i];
      vertices[2] = surfs[1].second[i]; vertices[3] = surfs[1].second[i - 1];
    }
    surfs[i + 2] = std::make_pair(_trunkSidePart, vertices);
  }
  return surfs;
}

real_t Trunk::measure() const
{
  real_t area = basis_->measure();
  number_t bpsize = basis_->p().size();
  real_t h;
  Point P = projectionOnTriangle(p_[bpsize], p_[0], p_[1], p_[2], h);
  return area * h * (1. + scale_ + scale_ * scale_) / 3.;
}

//! access to top geometry, created if not exist
Surface* Trunk::top() const
{
  if (top_ == nullptr) { buildTop(); }
  return top_;
}

//! create top geometry (top_ is mutable)
void Trunk::buildTop() const
{
  if (top_ != nullptr) { return; }
  top_ = static_cast<Surface*>(basis_->clone());
  if (scale_ != 1) { top_->homothetize(_center=origin_, _scale=scale_); }
  if (isElliptical_) { top_->translate(_direction=std::vector<real_t>(p_[5] - p_[0])); } // use centers
  else { top_->translate(_direction=std::vector<real_t>(origin_ - p_[0])); }
}

//! create boundary geometry of trunk as a composite (union of faces)
Geometry& Trunk::buildBoundary() const
{
  number_t nb = 0, ns = 0;
  const std::vector<string_t> bn = basis_->sideNames();
  for (number_t i = 0; i < bn.size(); i++) if (bn[i] != "") { nb++; }
  for (number_t i = 0; i < sideNames_.size(); i++) if (sideNames_[i] != "") { ns++; }
  if (nb > 1 || ns > 3)
  { error("free_error", "Trunk::buildBoundary() does not handle Trunk with multiple sidenames yet"); }
  string_t fname = "";
  if (sideNames_.size() == 1) { fname = sideNames_[0]; }
  boundaryGeometry_ = new Geometry(boundingBox, 2);
  Geometry& g = *boundaryGeometry_;
  g.shape(_composite);
  g.minimalBox = minimalBox;
  string_t name = "";
  if (domName_ != "") { name = domName_ + "_boundary"; }
  if (name != "") { g.domName(name); }
  std::map<number_t, Geometry*>& cps = g.components();
  std::map<number_t, std::vector<number_t> >& geos = g.geometries();
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > ss = surfs();
  if (isElliptical_) // boundary = 4 ellipses (basis) + 4 ellipses (top) + 4 TrunkSidePart
  {
    const Ellipse* ell = static_cast<const Ellipse*>(basis_);
    real_t pis2 = pi_ / 2;
    for (number_t i = 0; i < 4; i++)
    {
      cps[i] = new Ellipse(_center = ell->center(), _xradius = ell->radius1(), _yradius = ell->radius2(), _angle1 = i * pis2, _angle2 = (i + 1) * pis2);
      fname = basis_->domName();
      if (fname != "") { fname += tostring(i + 1); }
      cps[i]->domName(fname);
      if (h().size() > 0) { cps[i]->setHstep(basis_->surface()->h()[0]); }
      else { cps[i]->setNnodes(basis_->surface()->n()[0]); }
    }
    ell = static_cast<const Ellipse*>(top());
    for (number_t i = 0; i < 4; i++)
    {
      cps[i + 4] = new Ellipse(_center = ell->center(), _xradius = ell->radius1(), _yradius = ell->radius2(), _angle1 = i * pis2, _angle2 = (i + 1) * pis2);
      fname = basis_->domName();
      if (fname != "") { fname += tostring(i + 1); }
      cps[i + 4]->domName(fname);
      if (h().size() > 0) { cps[i + 4]->setHstep(top()->surface()->h()[0]); }
      else { cps[i + 4]->setNnodes(top()->surface()->n()[0]); }
    }
    for (number_t i = 0; i < 4; i++) // general case, boundary = basis + top + sum of lateral faces
    {
      fname = "";
      if (sideNames_.size() > i) { fname = sideNames_[i + 2]; }
      cps[i + 8] = new TrunkSidePart(*this, i * pis2, (i + 1)*pis2);
      cps[i + 8]->domName(fname);
      if (h().size() > 0) { cps[i + 8]->setHstep(h()[0]); }
      else { cps[i + 8]->setNnodes(n()[0]); }
    }
    for (number_t i = 0; i < 12; i++) { geos[i].push_back(i); }
  }
  else //assume basis is a polygon (faces are quadrangle)
    for (number_t i = 2; i < ss.size(); i++) // general case, boundary = basis + top + sum of lateral faces
    {
      if (sideNames_.size() > i) { fname = sideNames_[i]; }
      std::vector<const Point*>& vs = ss[i].second;
      cps[i] = new Quadrangle(*vs[0], *vs[1], *vs[2], *vs[3]);
      cps[i]->domName(fname);
      if (h().size() > 0) { cps[i]->setHstep(h()[0]); }
      else { cps[i]->setNnodes(n()[0]); }
      geos[i].push_back(i);
    }
  //for(number_t i=0;i<cps.size();i++) theCout<<(*cps[i])<<eol;
  return g;
}

//===============================================================================================
//                                     Cylinder stuff
//===============================================================================================
Cylinder::Cylinder(bool defineBasisAndP) : Trunk(1., defineBasisAndP), dir_({0., 0., 1.})
{
  shape_ = _cylinder;
}

Cylinder::Cylinder(const Surface& basis, const std::vector<real_t>& direction) : Trunk(1., false), dir_(direction)
{
  basis_ = basis.cloneS();
  number_t bpsize = basis_->p().size();
  number_t psize = 2 * bpsize;
  p_.resize(psize);
  for (number_t i = 0; i < bpsize; ++i)
  {
    p_[i] = basis_->p(i + 1);
    p_[i + bpsize] = basis_->p(i + 1) + Point(direction);
  }
  origin_ = p_[bpsize];
  boundingBox = BoundingBox(p_);
  shape_ = _cylinder;
  isElliptical_ = (basis_->shape() == _ellipse || basis_->shape() == _disk);
  computeMB();
}

void Cylinder::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Cylinder::build");
  shape_ = _cylinder;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _cylinder)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }

    // user must use only one of the following combination: (basis,direction) or (center1,v1,v2,center2)
    if ((key == _pk_center1 || key == _pk_v1 || key == _pk_v2 || key == _pk_center2) && usedParams.find(_pk_basis) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_basis)); }
    if ((key == _pk_center1 || key == _pk_v1 || key == _pk_v2 || key == _pk_center2) && usedParams.find(_pk_dir) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_dir)); }
    if ((key == _pk_basis || key == _pk_dir) && usedParams.find(_pk_center1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center1)); }
    if ((key == _pk_basis || key == _pk_dir) && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_basis || key == _pk_dir) && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if ((key == _pk_basis || key == _pk_dir) && usedParams.find(_pk_center2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center2)); }
  }
  
  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (basis,direction) or (center1,v1,v2,center2) has to be set (no default values)
  if (params.find(_pk_basis) == params.end() && params.find(_pk_dir) != params.end()) { error("param_missing", "direction"); }
  if (params.find(_pk_dir) == params.end() && params.find(_pk_basis) != params.end()) { error("param_missing", "basis"); }
  if (params.find(_pk_center1) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_center1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_center1) == params.end() && params.find(_pk_center2) != params.end()) { error("param_missing", "center2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_center1) != params.end()) { error("param_missing", "center1"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_center2) != params.end()) { error("param_missing", "center2"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_center1) != params.end()) { error("param_missing", "center1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_center1) != params.end()) { error("param_missing", "center2"); }
  if (params.find(_pk_center2) == params.end() && params.find(_pk_center1) != params.end()) { error("param_missing", "center1"); }
  if (params.find(_pk_center2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_center2) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  
  isElliptical_ = true;
  // now, we clean unwanted keys
  if (params.find(_pk_center1) != params.end())
  { params.erase(_pk_center1); params.erase(_pk_v1); params.erase(_pk_v2); params.erase(_pk_center2); isElliptical_ = false; }
  if (params.find(_pk_basis) != params.end()) { params.erase(_pk_basis); params.erase(_pk_dir); }
  
  bool ellipticalBasis = false;  // different from isElliptical
  if (basis_ != nullptr) { ellipticalBasis = (basis_->shape() == _ellipse || basis_->shape() == _disk); }
  
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  
  if (ellipticalBasis) // pdate center1 and origin (required by buildPAndBasis())
  {
    center1_ = basis_->p(1);
    origin_ = center1_ + dir_;
  }
  
  // checking nnodes and hsteps size
  isN_ = (h_.size() == 0);
  number_t bsize = 0, nsize = 0, hsize = 0;
  if (isElliptical_ || ellipticalBasis) { bsize = 4; nsize = 12; hsize = 8; }
  else
  {
    bsize = basis_->boundNodes().size();
    nsize = 3 * bsize;
    hsize = 2 * bsize;
  }
  if (isN_)
  {
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(nsize, n0); }
    else if (n_.size() == 3)
    {
      number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
      n_.clear();
      n_.resize(nsize, n0);
      for (number_t i = bsize; i < 2 * bsize; ++i) { n_[i] = n1; }
      for (number_t i = 2 * bsize; i < nsize; ++i) { n_[i] = n2; }
    }
    else if (n_.size() != nsize) {error("bad_size", "nnodes", nsize, n_.size()); }
  }
  if (h_.size() != 0)
  {
    if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(hsize, h0); }
    else if (h_.size() != hsize) {error("bad_size", "hsteps", hsize, h_.size()); }
  }
  
  // p_ is not (fully) built so we do
  buildPAndBasis();
  if (isElliptical_) { dir_ = origin_ - center1_; }
  isElliptical_ = isElliptical_ || ellipticalBasis; // update isElliptical_
  boundingBox = BoundingBox(p_);
  computeMB();
  trace_p->pop();
}

void Cylinder::buildParam(const Parameter& p)
{
  trace_p->push("Cylinder::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_dir:
    {
      switch (p.type())
      {
        case _pt: dir_ = std::vector<real_t>(p.get_pt()); break;
        case _realVector: dir_ = p.get_rv(); break;
        case _integer: dir_ = {real_t(p.get_i()), 0., 0.}; break;
        case _real: dir_ = {p.get_r(), 0., 0.}; break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Trunk::buildParam(p); break;
  }
  trace_p->pop();
}

void Cylinder::buildDefaultParam(ParameterKey key)
{
  Trunk::buildDefaultParam(key);
}

std::set<ParameterKey> Cylinder::getParamsKeys()
{
  std::set<ParameterKey> params = Trunk::getParamsKeys();
  // _scale is disabled: default value is 1 as in Trunk::buildDefaultParam(ParameterKey)
  // _origin is replaced by _direction
  params.erase(_pk_scale);
  params.erase(_pk_origin);
  params.insert(_pk_dir);
  return params;
}

Cylinder::Cylinder(Parameter p1, Parameter p2) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

Cylinder::Cylinder(Parameter p1, Parameter p2, Parameter p3) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Cylinder::Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Cylinder::Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5)
  : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Cylinder::Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Cylinder::Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Cylinder::Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Cylinder::Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Cylinder::Cylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Cylinder::Cylinder(const Cylinder& c) : Trunk(c), dir_(c.dir_) {}

string_t Cylinder::asString() const
{
  string_t s("Cylinder (basis = { ");
  s += basis_->asString() + " }, dir = " + tostring(roundToZero(dir_));
  s += ")";
  return s;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Cylinder::surfs() const
{
  number_t ssize = basis_->surfs()[0].second.size();
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(2 + ssize);
  surfs[0] = basis_->surfs()[0];
  surfs[1] = top()->surfs()[0];
  //  number_t ssize2 = p_.size()/2;
  //  std::vector<const Point*> ssurf2(ssize);
  //  for (number_t i=0; i < ssize; ++i)
  //    for (number_t j=0; j < ssize2; ++j)
  //      if (*surfs[0].second[i] == p_[j]) ssurf2[i] = &p_[j+ssize2];
  //  surfs[1]=std::make_pair(surfs[0].first, ssurf2);
  
  for (number_t i = 0; i < ssize; ++i)
  {
    std::vector<const Point*> vertices(4);
    if (i == 0)
    {
      vertices[0] = surfs[0].second[ssize - 1]; vertices[1] = surfs[0].second[0];
      vertices[2] = surfs[1].second[0]; vertices[3] = surfs[1].second[ssize - 1];
    }
    else
    {
      vertices[0] = surfs[0].second[i - 1]; vertices[1] = surfs[0].second[i];
      vertices[2] = surfs[1].second[i]; vertices[3] = surfs[1].second[i - 1];
    }
    surfs[i + 2] = std::make_pair(_cylinderSidePart, vertices);
  }
  return surfs;
}

//===============================================================================================
//                                     Prism stuff
//===============================================================================================
Prism::Prism() : Cylinder(Triangle(), Reals(0., 0., 1.)), isTriangular_(true)
{ shape_ = _prism; }

void Prism::buildPBasisNAndH()
{
  isN_ = (h_.size() == 0);
  if (isTriangular_)
  {
    if (isN_)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(9, n0); }
      else if (n_.size() == 3)
      {
        number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
        n_.clear();
        n_.resize(9, n0);
        for (number_t i = 4; i < 8; ++i) { n_[i] = n1; }
        for (number_t i = 8; i < 12; ++i) { n_[i] = n2; }
      }
      else if (n_.size() != 9) {error("bad_size", "nnodes", 9, n_.size()); }
      basis_ = new Triangle(p1_, p2_, p3_, Numbers(n_[0], n_[1], n_[2]), oneOfStrings(sideNames_));
    }
    else
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(6, h0); }
      else if (h_.size() != 6) {error("bad_size", "hsteps", 6, h_.size()); }
      basis_ = new Triangle(p1_, p2_, p3_, Reals(h_[0], h_[1], h_[2]), oneOfStrings(sideNames_));
    }
    p_.resize(6);
    for (number_t i = 0; i < 3; ++i)
    {
      p_[i] = basis_->p(i + 1);
      p_[i + 3] = basis_->p(i + 1) + dir_;
    }
  }
  else
  {
    if (!basis_->isPolygon()) { error("polygon_needed", words("shape", basis_->shape())); }
    
    number_t bsize = basis_->boundNodes().size();
    number_t bpsize = basis_->p().size();
    number_t psize = 2 * bpsize;
    number_t nsize = 3 * bsize;
    number_t hsize = 2 * bsize;
    p_.resize(psize);
    for (number_t i = 0; i < bpsize; ++i)
    {
      p_[i] = basis_->p(i + 1);
      p_[i + bpsize] = basis_->p(i + 1) + dir_;
    }
    if (isN_)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(nsize, n0); }
      else if (n_.size() == 3)
      {
        std::vector<number_t> n = n_;
        n_.clear();
        n_.resize(nsize, n[0]);
        for (number_t i = bsize; i < 2 * bsize; ++i) { n_[i] = n[1]; }
        for (number_t i = 2 * bsize; i < nsize; ++i) { n_[i] = n[2]; }
      }
      else if (n_.size() != nsize) {error("bad_size", "nnodes", nsize, n_.size()); }
      for (number_t i = 0; i < bsize; ++i) { basis_->n(i + 1) = n_[i]; }
    }
    else
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(hsize, h0); }
      else if (h_.size() != hsize) {error("bad_size", "hsteps", hsize, h_.size()); }
      for (number_t i = 0; i < bsize; ++i) { basis_->h(i + 1) = h_[i]; }
    }
  }
}

void Prism::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Prism::build");
  shape_ = _prism;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _prism)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }

    // user must use only one of the following combination: (basis,direction) or (v1,v2,v3,direction)
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v3) && usedParams.find(_pk_basis) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_basis)); }
    if (key == _pk_basis && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if (key == _pk_basis && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if (key == _pk_basis && usedParams.find(_pk_v3) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v3)); }
  }
  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (basis,direction) or (v1,v2,v3,direction) has to be set (no default values)
  if (params.find(_pk_dir) != params.end()) { error("param_missing", "direction"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v3) != params.end()) { error("param_missing", "v3"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v3) != params.end()) { error("param_missing", "v3"); }
  if (params.find(_pk_v3) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v3) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  
  isTriangular_ = true;
  // now, we clean unwanted keys
  if (params.find(_pk_v1) != params.end())
  { params.erase(_pk_v1); params.erase(_pk_v2); params.erase(_pk_v3); isTriangular_ = false; }
  if (params.find(_pk_basis) != params.end()) { params.erase(_pk_basis); }
  
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  
  // p_ is not (fully) built so we do
  buildPBasisNAndH();
  
  boundingBox = BoundingBox(p_);
  computeMB();
  trace_p->pop();
}

void Prism::buildParam(const Parameter& p)
{
  trace_p->push("Prism::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_v1:
    {
      switch (p.type())
      {
        case _pt: p1_ = p.get_pt(); break;
        case _integer: p1_ = Point(real_t(p.get_i())); break;
        case _real: p1_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v2:
    {
      switch (p.type())
      {
        case _pt: p2_ = p.get_pt(); break;
        case _integer: p2_ = Point(real_t(p.get_i())); break;
        case _real: p2_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v3:
    {
      switch (p.type())
      {
        case _pt: p3_ = p.get_pt(); break;
        case _integer: p3_ = Point(real_t(p.get_i())); break;
        case _real: p3_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Cylinder::buildParam(p); break;
  }
  trace_p->pop();
}

void Prism::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Prism::buildDefaultParam");
  Cylinder::buildDefaultParam(key);
  trace_p->pop();
}

std::set<ParameterKey> Prism::getParamsKeys()
{
  std::set<ParameterKey> params = Cylinder::getParamsKeys();
  //_center1 and _center2 are disabled (only for elliptical basis, so not for a prism)
  params.erase(_pk_center1);
  params.erase(_pk_center2);
  params.insert(_pk_v3);
  return params;
}

Prism::Prism(Parameter p1, Parameter p2) : Cylinder(false)
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

Prism::Prism(Parameter p1, Parameter p2, Parameter p3) : Cylinder(false)
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Prism::Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Cylinder(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Prism::Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Cylinder(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Prism::Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Cylinder(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Prism::Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Cylinder(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Prism::Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Cylinder(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Prism::Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Cylinder(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Prism::Prism(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Cylinder(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Prism::Prism(const Prism& p) : Cylinder(p), isTriangular_(p.isTriangular_), p3_(p.p3_)
{}

string_t Prism::asString() const
{
  string_t s("Prism (basis = { ");
  s += basis_->asString() + " }, dir = " + tostring(roundToZero(dir_));
  s += ")";
  return s;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Prism::surfs() const
{
  number_t ssize = basis_->surfs()[0].second.size();
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(2 + ssize);
  surfs[0] = basis_->surfs()[0];
  number_t ssize2 = p_.size() / 2;
  
  std::vector<const Point*> ssurf2(ssize);
  
  for (number_t i = 0; i < ssize; ++i)
  {
    for (number_t j = 0; j < ssize2; ++j)
    {
      if (*surfs[0].second[i] == p_[j]) { ssurf2[i] = &p_[j + ssize2]; }
    }
  }
  surfs[1] = std::make_pair(surfs[0].first, ssurf2);
  
  for (number_t i = 0; i < ssize; ++i)
  {
    std::vector<const Point*> vertices(4);
    if (i == 0)
    {
      vertices[0] = surfs[0].second[ssize - 1]; vertices[1] = surfs[0].second[0];
      vertices[2] = surfs[1].second[0]; vertices[3] = surfs[1].second[ssize - 1];
    }
    else
    {
      vertices[0] = surfs[0].second[i - 1]; vertices[1] = surfs[0].second[i];
      vertices[2] = surfs[1].second[i]; vertices[3] = surfs[1].second[i - 1];
    }
    surfs[i + 2] = std::make_pair(_quadrangle, vertices);
  }
  return surfs;
}

//! collect all canonical geometry's with name n
void Prism::collect(const string_t& n, std::list<Geometry*>& geoms) const
{
  if (domName_ == n) { geoms.push_back(const_cast<Geometry*>(static_cast<const Geometry*>(this))); }
  if (sideNames_.size() == 0) { return; } //no side named
  // loop on sides
  if (!basis_->isPolygon())
  {
    where("Prism::collect(...)");
    error("geometry_not_polygonal", basis_->asString());
  }
  string_t sn;
  if (sideNames_.size() > 0) { sn = sideNames_[0]; }
  if (basis_->domName() == n || sn == n) // basis is named n
  {
    Geometry* gb = basis_->clone();
    gb->domName() = n;
    geoms.push_back(gb);
  }
  bool ss = sideNames_.size() > 1;
  if (!ss && sn != n) { return; } // other sides are not named n
  if (ss) { sn = sideNames_[1]; }
  std::vector<Point> pts = basis_->p();
  number_t nb = pts.size();
  if (sn == n) // top is named n
  {
    Geometry* gb = basis_->clone();
    Polygon*  gbp = dynamic_cast<Polygon*>(gb);
    gb->domName() = n;
    for (number_t s = 1; s <= nb; ++s) { gbp->p(s) = p_[s + nb - 1]; }
    geoms.push_back(gb);
  }
  //other sides
  for (number_t s = 0; s < pts.size(); ++s)
  {
    number_t sp = s + 1;
    if (sp == pts.size()) { sp = 0; }
    if (ss) { sn = sideNames_[s + 2]; }
    if (sn == n) { geoms.push_back(new Parallelogram(_v1 = p_[s], _v2 = p_[sp], _v4 = p_[s + nb], _nnodes = 2, _domain_name = sn)); }
  }
}

//===============================================================================================
//                                     Cone stuff
//===============================================================================================
Cone::Cone(bool defineBasisAndP) : Trunk(0., defineBasisAndP)
{
  shape_ = _cone;
}

Cone::Cone(const Surface& basis, const Point& apex) : Trunk(0., false)
{
  basis_ = basis.cloneS();
  origin_ = apex;
  buildPBasisNAndH();
}

void Cone::buildPBasisNAndH()
{
  if (isElliptical_)
  {
    if (dot(p1_ - center1_, p2_ - center1_) > theTolerance) { error("geometry_incoherent_points", words("shape", _cone)); }
    if (std::abs(center1_.distance(p1_) - center1_.distance(p2_)) < theTolerance)
    {
      if (isN_)
      {
        if (n_.size() == 1)
        {
          number_t n0 = n_[0];
          n_.clear();
          n_.resize(8, n0);
        }
        else if (n_.size() == 2)
        {
          number_t n0 = n_[0], n1 = n_[1];
          n_.clear();
          n_.resize(8, n0);
          for (number_t i = 4; i < 8; ++i) { n_[i] = n1; }
        }
        else if (n_.size() != 8) {error("bad_size", "nnodes", 8, n_.size()); }
        basis_ = new Disk(center1_, p1_, p2_, Numbers(n_[0], n_[1], n_[2], n_[3]), oneOfStrings(sideNames_));
      }
      else
      {
        if (h_.size() == 1) { real_t h0 = h_[0]; h_.clear(); h_.resize(5, h0); }
        else if (h_.size() != 5) {error("bad_size", "hsteps", 5, h_.size()); }
        basis_ = new Disk(center1_, p1_, p2_, Reals(h_[0], h_[1], h_[2], h_[3]), oneOfStrings(sideNames_));
      }
    }
    else
    {
      if (isN_)
      {
        if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(8, n0); }
        else if (n_.size() == 2)
        {
          number_t n0 = n_[0], n1 = n_[1];
          n_.clear();
          n_.resize(8, n0);
          for (number_t i = 4; i < 8; ++i) { n_[i] = n1; }
        }
        else if (n_.size() != 8) {error("bad_size", "nnodes", 8, n_.size()); }
        basis_ = new Ellipse(center1_, p1_, p2_, Numbers(n_[0], n_[1], n_[2], n_[3]), oneOfStrings(sideNames_));
      }
      else
      {
        if (h_.size() == 1) { real_t h0 = h_[0]; h_.clear(); h_.resize(5, h0); }
        else if (h_.size() != 5) {error("bad_size", "hsteps", 5, h_.size()); }
        basis_ = new Ellipse(center1_, p1_, p2_, Reals(h_[0], h_[1], h_[2], h_[3]), oneOfStrings(sideNames_));
      }
    }
    p_.resize(10);
    for (number_t i = 0; i < 5; ++i)
    {
      p_[i] = basis_->p(i + 1);
      p_[i + 5] = basis_->p(i + 1) + origin_ - basis_->p(1);
    }
    p_[5] = origin_;
  }
  else
  {
    number_t bsize = basis_->boundNodes().size();
    number_t bpsize = basis_->p().size();
    number_t psize = bpsize + 1;
    number_t nsize = 2 * bsize;
    number_t hsize = bsize + 1;
    p_.resize(psize);
    for (number_t i = 0; i < bpsize; ++i) { p_[i] = basis_->p(i + 1); }
    p_[bpsize] = origin_;
    if (isN_)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(nsize, n0); }
      else if (n_.size() == 2)
      {
        std::vector<number_t> n = n_;
        n_.clear();
        n_.resize(nsize, n[0]);
        for (number_t i = bsize; i < 2 * bsize; ++i) { n_[i] = n[1]; }
      }
      else if (n_.size() != nsize) {error("bad_size", "nnodes", nsize, n_.size()); }
      for (number_t i = 0; i < bsize; ++i) { basis_->n(i + 1) = (n_[i] > 2) ? n_[i] : 2; }
    }
    else
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(hsize, h0); }
      else if (h_.size() != hsize) {error("bad_size", "hsteps", hsize, h_.size()); }
      for (number_t i = 0; i < bsize; ++i) { basis_->h(i + 1) = h_[i]; }
    }
  }
}

void Cone::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Cone::build");
  shape_ = _cone;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _cone)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }

    // user must use only one of the following combination: (basis,apex) or (center1,v1,v2,apex)
    if ((key == _pk_center1 || key == _pk_v1 || key == _pk_v2) && usedParams.find(_pk_basis) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_basis)); }
    if ((key == _pk_center1 || key == _pk_v1 || key == _pk_v2) && usedParams.find(_pk_origin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_origin)); }
    if ((key == _pk_basis) && usedParams.find(_pk_center1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center1)); }
    if ((key == _pk_basis) && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_basis) && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
  }
  
  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (basis,apex) or (center1,v1,v2,apex) has to be set (no default values)
  if (params.find(_pk_apex) != params.end()) { error("param_missing", "apex"); }
  if (params.find(_pk_center1) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_center1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_center1) != params.end()) { error("param_missing", "center1"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_center1) != params.end()) { error("param_missing", "center1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  
  isElliptical_ = true;
  // now, we clean unwanted keys
  if (params.find(_pk_center1) != params.end())
  { params.erase(_pk_center1); params.erase(_pk_v1); params.erase(_pk_v2); isElliptical_ = false; }
  if (params.find(_pk_basis) != params.end()) { params.erase(_pk_basis); }
  
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  
  // p_ is not (fully) built so we do
  buildPBasisNAndH();
  
  boundingBox = BoundingBox(p_);
  computeMB();
  trace_p->pop();
}

void Cone::buildParam(const Parameter& p)
{
  trace_p->push("Cone::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_apex:
    {
      switch (p.type())
      {
        case _pt: origin_ = p.get_pt(); break;
        case _integer: origin_ = Point(real_t(p.get_i())); break;
        case _real: origin_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Trunk::buildParam(p); break;
  }
  trace_p->pop();
}

void Cone::buildDefaultParam(ParameterKey key)
{
  Trunk::buildDefaultParam(key);
}

std::set<ParameterKey> Cone::getParamsKeys()
{
  std::set<ParameterKey> params = Trunk::getParamsKeys();
  // center2 and origin keys are replaced by apex
  // _scale is disabled (value 0)
  params.erase(_pk_scale);
  scale_ = 0.;
  params.erase(_pk_origin);
  params.erase(_pk_center2);
  params.insert(_pk_apex);
  return params;
}

Cone::Cone(Parameter p1, Parameter p2) : Trunk(0., false)
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

Cone::Cone(Parameter p1, Parameter p2, Parameter p3) : Trunk(0., false)
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Cone::Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Trunk(0., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Cone::Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Trunk(0., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Cone::Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Trunk(0., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Cone::Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Trunk(0., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Cone::Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Trunk(0., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Cone::Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Trunk(0., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Cone::Cone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Trunk(0., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Cone::Cone(const Cone& c) : Trunk(c) {}

string_t Cone::asString() const
{
  string_t s("Cone (basis = { ");
  s += basis_->asString() + " }, apex = (" + origin_.roundToZero().toString() + ")";
  return s;
}

std::vector<const Point*> Cone::boundNodes() const
{
  std::vector<const Point*> snodes = basis_->boundNodes();
  number_t ssize = snodes.size();
  std::vector<const Point*> nodes(ssize + 1);
  
  for (number_t i = 0; i < ssize; ++i) { nodes[i] = snodes[i]; }
  nodes[ssize] = &origin_;
  
  return nodes;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Cone::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > scurves = basis_->curves();
  std::vector<const Point*> snodes = basis_->boundNodes();
  number_t ssize = scurves.size();
  
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves(2 * ssize);
  
  for (number_t i = 0; i < ssize; ++i)
  {
    curves[i] = scurves[i];
    std::vector<const Point*> curve(2);
    curve[0] = snodes[i];
    curve[1] = &origin_;
    curves[i + ssize] = std::make_pair(_segment, curve);
  }
  
  return curves;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Cone::surfs() const
{
  std::vector<const Point*> snodes = basis_->boundNodes();
  number_t ssize = basis_->surfs()[0].second.size();
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(1 + ssize);
  surfs[0] = basis_->surfs()[0];
  
  for (number_t i = 0; i < ssize; ++i)
  {
    std::vector<const Point*> surf(3);
    if (i == ssize - 1)
    {
      surf[0] = snodes[ssize - 1];
      surf[1] = snodes[0];
    }
    else
    {
      surf[0] = snodes[i];
      surf[1] = snodes[i + 1];
    }
    surf[2] = &origin_;
    surfs[i + 1] = std::make_pair(_coneSidePart, surf);
  }
  
  return surfs;
}

//===============================================================================================
//                                     Pyramid stuff
//===============================================================================================
Pyramid::Pyramid()
  : Cone(Quadrangle(Point(0., 0., 0.), Point(1., 0., 0.), Point(1., 1., 0.), Point(0., 1., 0.)), Point(0., 0., 1.))
{
  shape_ = _pyramid;
  computeMB();
}

void Pyramid::buildPBasisNAndH()
{
  if (isQuadrangular_)
  {
    if (isN_)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(8, n0); }
      else if (n_.size() == 2)
      {
        number_t n0 = n_[0], n1 = n_[1];
        n_.clear();
        n_.resize(4, n0);
        for (number_t i = 4; i < 8; ++i) { n_[i] = n1; }
      }
      else if (n_.size() != 8) {error("bad_size", "nnodes", 8, n_.size()); }
      basis_ = new Quadrangle(p1_, p2_, p3_, p4_, Numbers(n_[0], n_[1], n_[2], n_[3]), oneOfStrings(sideNames_));
    }
    else
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(5, h0); }
      else if (h_.size() != 5) {error("bad_size", "hsteps", 5, h_.size()); }
      basis_ = new Quadrangle(p1_, p2_, p3_, p4_, Reals(h_[0], h_[1], h_[2], h_[3]), oneOfStrings(sideNames_));
    }
    p_.resize(5);
    for (number_t i = 0; i < 4; ++i) { p_[i] = basis_->p(i + 1); }
    p_[4] = origin_;
  }
  else
  {
    if (!basis_->isPolygon()) { error("polygon_needed", words("shape", basis_->shape())); }
    
    number_t bsize = basis_->boundNodes().size();
    number_t bpsize = basis_->p().size();
    number_t psize = bpsize + 1;
    number_t nsize = 2 * bsize;
    number_t hsize = bsize + 1;
    p_.resize(psize);
    for (number_t i = 0; i < bpsize; ++i) { p_[i] = basis_->p(i + 1); }
    p_[bpsize] = origin_;
    if (isN_)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(nsize, n0); }
      else if (n_.size() == 2)
      {
        std::vector<number_t> n = n_;
        n_.clear();
        n_.resize(nsize, n[0]);
        for (number_t i = bsize; i < 2 * bsize; ++i) { n_[i] = n[1]; }
      }
      else if (n_.size() != nsize) {error("bad_size", "nnodes", nsize, n_.size()); }
      for (number_t i = 0; i < bsize; ++i) { basis_->n(i + 1) = (n_[i] > 2) ? n_[i] : 2; }
    }
    else
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(hsize, h0); }
      else if (h_.size() != hsize) {error("bad_size", "hsteps", hsize, h_.size()); }
      for (number_t i = 0; i < bsize; ++i) { basis_->h(i + 1) = h_[i]; }
    }
  }
}

void Pyramid::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Pyramid::build");
  shape_ = _pyramid;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _pyramid)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }

    // user must use only one of the following combination: (basis,apex) or (v1,v2,v3,v4,apex)
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v3 || key == _pk_v4) && usedParams.find(_pk_basis) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_basis)); }
    if (key == _pk_basis && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if (key == _pk_basis && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if (key == _pk_basis && usedParams.find(_pk_v3) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v3)); }
    if (key == _pk_basis && usedParams.find(_pk_v4) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v4)); }
  }
  
  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (basis,apex) or (v1,v2,v3,v4,apex) has to be set (no default values)
  if (params.find(_pk_apex) != params.end()) { error("param_missing", "apex"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v3) != params.end()) { error("param_missing", "v3"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v3) != params.end()) { error("param_missing", "v3"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v3) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v3) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v3) == params.end() && params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v4) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v4) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v4) == params.end() && params.find(_pk_v3) != params.end()) { error("param_missing", "v3"); }
  
  isElliptical_ = false; // a Pyramid cannot have an elliptical basis
  isQuadrangular_ = true;
  // now, we clean unwanted keys
  if (params.find(_pk_v1) != params.end())
  { params.erase(_pk_v1); params.erase(_pk_v2); params.erase(_pk_v3); params.erase(_pk_v4); isQuadrangular_ = false; }
  if (params.find(_pk_basis) != params.end()) { params.erase(_pk_basis); }
  
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  
  // p_ is not (fully) built so we do
  buildPBasisNAndH();
  boundingBox = BoundingBox(p_);
  computeMB();
  trace_p->pop();
}

void Pyramid::buildParam(const Parameter& p)
{
  trace_p->push("Pyramid::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_v1:
    {
      switch (p.type())
      {
        case _pt: p1_ = p.get_pt(); break;
        case _integer: p1_ = Point(real_t(p.get_i())); break;
        case _real: p1_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v2:
    {
      switch (p.type())
      {
        case _pt: p2_ = p.get_pt(); break;
        case _integer: p2_ = Point(real_t(p.get_i())); break;
        case _real: p2_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v3:
    {
      switch (p.type())
      {
        case _pt: p3_ = p.get_pt(); break;
        case _integer: p3_ = Point(real_t(p.get_r())); break;
        case _real: p3_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v4:
    {
      switch (p.type())
      {
        case _pt: p4_ = p.get_pt(); break;
        case _integer: p4_ = Point(real_t(p.get_r())); break;
        case _real: p4_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Cone::buildParam(p); break;
  }
  trace_p->pop();
}

void Pyramid::buildDefaultParam(ParameterKey key)
{
  Cone::buildDefaultParam(key);
}

std::set<ParameterKey> Pyramid::getParamsKeys()
{
  std::set<ParameterKey> params = Cone::getParamsKeys();
  //_center1 is disabled (only for elliptical basis, so not for a pyramid)
  // _v3 and _v4 introduced for quadrangular basis
  params.erase(_pk_center1);
  params.insert(_pk_v3);
  params.insert(_pk_v4);
  return params;
}

Pyramid::Pyramid(Parameter p1, Parameter p2) : Cone(false)
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

Pyramid::Pyramid(Parameter p1, Parameter p2, Parameter p3) : Cone(false)
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Pyramid::Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Cone(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Pyramid::Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Cone(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Pyramid::Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Cone(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Pyramid::Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Cone(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Pyramid::Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Cone(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Pyramid::Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Cone(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Pyramid::Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Cone(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Pyramid::Pyramid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11) : Cone(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  build(ps);
}

Pyramid::Pyramid(const Pyramid& p) : Cone(p), isQuadrangular_(p.isQuadrangular_), p3_(p.p3_), p4_(p.p4_) {}

string_t Pyramid::asString() const
{
  string_t s("Pyramid (basis = { ");
  s += basis_->asString() + " }, apex = " + origin_.roundToZero().toString() + ")";
  return s;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Pyramid::surfs() const
{
  std::vector<const Point*> snodes = basis_->boundNodes();
  number_t ssize = basis_->surfs()[0].second.size();
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(1 + ssize);
  surfs[0] = basis_->surfs()[0];
  
  for (number_t i = 0; i < ssize; ++i)
  {
    std::vector<const Point*> surf(3);
    if (i == ssize - 1)
    {
      surf[0] = snodes[ssize - 1];
      surf[1] = snodes[0];
    }
    else
    {
      surf[0] = snodes[i];
      surf[1] = snodes[i + 1];
    }
    surf[2] = &origin_;
    surfs[i] = std::make_pair(_triangle, surf);
  }
  
  return surfs;
}

//! collect all canonical geometry's with name n
void Pyramid::collect(const string_t& n, std::list<Geometry*>& geoms) const
{
  if (domName_ == n) { geoms.push_back(const_cast<Geometry*>(static_cast<const Geometry*>(this))); }
  //loop on sides
  if (!basis_->isPolygon())
  {
    where("Pyramid::collect(...)");
    error("geometry_not_polygonal", basis_->asString());
  }
  string_t sn;
  if (sideNames_.size() > 0) { sn = sideNames_[0]; }
  if (basis_->domName() == n || sn == n) // basis is named n
  {
    Geometry* gb = basis_->clone();
    gb->domName() = n;
    geoms.push_back(gb);
  }
  if (sideNames_.size() == 1 && sn != n) { return; } // other sides are not named n
  std::vector<Point> pts = basis_->p();
  bool ss = sideNames_.size() > 1;
  for (number_t s = 0; s < pts.size(); ++s)
  {
    number_t sp = s + 1;
    if (sp == pts.size()) { sp = 0; }
    if (ss) { sn = sideNames_[s + 1]; }
    if (sn == n) { geoms.push_back(new Triangle(_v1 = origin_, _v2 = pts[s], _v3 = pts[sp], _nnodes = 2, _domain_name = sn)); }
  }
}

//===============================================================================================
//                                     RevTrunk stuff
//===============================================================================================
RevTrunk::RevTrunk(real_t scale, bool defineBasisAndP)
  : Trunk(scale, defineBasisAndP), radius1_(1.), radius2_(scale), nbSubDomains_(1), endShape1_(_gesFlat), distance1_(0.),
    endShape2_(_gesFlat), distance2_(0.), type_(1)
{ shape_ = _revTrunk; }

void RevTrunk::buildP()
{
  // radius1_ != 0 (assumed here)
  // the aim is to convert data center1_, origin_, radius1_, radius2_ to trihedron center1_, p1_, p2_, origin_
  // we have p1=c1, p4=c2
  // p2 and p3 are arbitrary but they respect the following rules:
  // dot(p1_-center1_,origin_-center1)=0 and norm(p1_-center1_)=radius1_
  // norm(p2_-center1_)=radius1_ and center_1, p1_, p2_, origin_ is a direct trihedron
  // we add the following rules to get a unique p1_ (and p2_ by the way) : (p1_-center1_)[2]=0. and [ (p1_-center1_)[0] > 0
  // or (p1_-center1_)[1] > 0 ]
  // Let d be origin_-center1, u be p1_-center1_ and v be p2_-center1_:
  // So we have: dot(d,u)=0, norm(u)=radius1_, uz=0., (ux > 0. or uy > 0.) and v=(d^u)*radius1_/norm(d^u)
  //
  // there is an exception for the additional rules: when dx=dy=0.
  // in this case: u=(radius1_, 0. 0.) => if (dz>0) , v=(0., radius1_, 0.) else v=(0.,-radius1_, 0.)
  // scale_=radius2_/radius1_
  Point d = origin_ - center1_;
  Point u(0., 0., 0.), v(0., 0., 0.);
  real_t& dx = d[0];
  real_t& dy = d[1];
  real_t& dz = d[2];
  real_t& ux = u[0];
  real_t& uy = u[1];
  // real_t& uz=u[2];
  // real_t& vx=v[0];
  real_t& vy = v[1];
  // real_t& vz=v[2];
  
  // dot(d,u)=0 <=> ux dx + uy dy = 0.
  if (dy == 0.)
  {
    if (dx == 0.)
    {
      ux = radius1_;
      if (dz > 0.) { vy = radius1_; }
      else { vy = -radius1_; }
    }
    else
    {
      uy = radius1_;
      // d^u = (dy uz - dz uy, dz ux - dx uz, dx uy - dy ux) = (-dz uy, 0., dx uy)
      Point dcrossu(dz * uy, 0., dx * uy);
      v = dcrossu * radius1_ / norm(dcrossu);
    }
  }
  else
  {
    if (dx == 0.)
    {
      ux = radius1_;
      // d^u = (dy uz - dz uy, dz ux - dx uz, dx uy - dy ux) = (0., dz ux, -dy ux)
      Point dcrossu(0., dz * ux, -dy * ux);
      v = dcrossu * radius1_ / norm(dcrossu);
    }
    else
    {
      // general case uy=-ux dx / dy
      // we use norm(u)=r1 and ux > 0 => ux = r1/sqrt(1+(dx/dy)^2)
      ux = radius1_ / std::sqrt(1. + (dx / dy) * (dx / dy));
      uy = -ux * dx / dy;
      // d^u = (dy uz - dz uy, dz ux - dx uz, dx uy - dy ux) = (-dz uy, dz ux, dx uy - dy ux)
      Point dcrossu(-dz * uy, dz * ux, dx * uy - dy * ux);
      v = dcrossu * radius1_ / norm(dcrossu);
    }
  }
  
  if (radius2_ != 0)  { p_.resize(10); }
  else  { p_.resize(6); }
  p_[0] = center1_; p_[1] = p1_ = center1_ + u; p_[2] = p2_ = center1_ + v; p_[3] = center1_ - u; p_[4] = center1_ - v; p_[5] = origin_;
  if (radius2_ != 0.) { p_[6] = origin_ + scale_ * u; p_[7] = origin_ + scale_ * v; p_[8] = origin_ - scale_ * u; p_[9] = origin_ - scale_ * v; }
  
  d /= norm(d);
  // add construction points of endshapes to p
  number_t n = p_.size();
  switch (endShape1_)
  {
    case _gesCone:
    case _gesSphere:
    case _gesEllipsoid: p_.resize(n + 1); p_[n] = center1_ - distance1_ * d; n += 1; break; // cone
    case _gesFlat:
    case _gesNone:
    default:    break;
  }
  switch (endShape2_)
  {
    case _gesCone:
    case _gesSphere:
    case _gesEllipsoid: p_.resize(n + 1); p_[n] = origin_ + distance2_ * d; break; // cone
    case _gesFlat:
    case _gesNone:
    default:    break;
  }
}

void RevTrunk::buildPScaleAndBasis()
{
  scale_ = radius2_ / radius1_;
  buildP();
  
  if (isN_)
  {
    std::vector<number_t> n(4);
    for (number_t i = 0; i < 4; ++i) { n[i] = n_[i]; }
    basis_ = new Disk(center1_, p1_, p2_, n);
  }
  else
  {
    std::vector<real_t> h(4);
    for (number_t i = 0; i < 4; ++i) { h[i] = h_[i]; }
    basis_ = new Disk(center1_, p1_, p2_, h);
  }
}

void RevTrunk::build(const std::vector<Parameter>& ps)
{
  trace_p->push("RevTrunk::build");
  shape_ = _revTrunk;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _revTrunk)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }
  }
  
  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // ((center1,radius1,center2,radius2) has to be set (no default values)
  if (params.find(_pk_center1) != params.end()) { error("param_missing", "center1"); }
  if (params.find(_pk_radius1) != params.end()) { error("param_missing", "radius1"); }
  if (params.find(_pk_center2) != params.end()) { error("param_missing", "center2"); }
  if (params.find(_pk_radius2) != params.end()) { error("param_missing", "radius2"); }
  // _end1_shape and _end1_distance have to be set both
  if (params.find(_pk_end1_shape) == params.end() && params.find(_pk_end1_distance) != params.end() && endShape1_ != _gesNone && endShape1_ != _gesFlat)
  { error("param_missing", "end1_distance"); }
  if (params.find(_pk_end1_distance) == params.end() && params.find(_pk_end1_shape) != params.end())
  { error("param_missing", "end1_shape"); }
  // _end2_shape and _end2_distance have to be set both and with _end1_shape and _end1_distance
  if (params.find(_pk_end2_shape) == params.end() && params.find(_pk_end1_shape) != params.end())
  { error("param_missing", "end1_shape"); }
  if (params.find(_pk_end2_shape) == params.end() && params.find(_pk_end1_distance) != params.end() && endShape1_ != _gesNone && endShape1_ != _gesFlat)
  { error("param_missing", "end1_distance"); }
  if (params.find(_pk_end2_shape) == params.end() && params.find(_pk_end2_distance) != params.end() && endShape2_ != _gesNone && endShape2_ != _gesFlat)
  { error("param_missing", "end2_distance"); }
  if (params.find(_pk_end2_distance) == params.end() && params.find(_pk_end1_shape) != params.end())
  { error("param_missing", "end1_shape"); }
  if (params.find(_pk_end2_distance) == params.end() && params.find(_pk_end1_distance) != params.end() && endShape1_ != _gesNone && endShape1_ != _gesFlat)
  { error("param_missing", "end1_distance"); }
  if (params.find(_pk_end2_distance) == params.end() && params.find(_pk_end2_shape) != params.end())
  { error("param_missing", "end2_shape"); }
  
  // check height of revtrunk
  if (center1_ == origin_) { error("degenerated_elt", "revtrunk"); }
  
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  
  // checking nnodes and hsteps size
  checkNH();
  // p_ is not (fully) built so we do
  buildPScaleAndBasis();
  // create bounding boxes
  boundingBox = BoundingBox(p_);
  computeMB();
  trace_p->pop();
}

void RevTrunk::checkNH()
{
  isN_ = false;
  if (n_.size() != 0)
  {
    isN_ = true;
    number_t nsize = 12;
    number_t nshort = 3;
    if (endShape1_ != _gesNone && endShape1_ != _gesFlat)
    {
      nsize += 4;
      nshort++;
    }
    if (endShape2_ != _gesNone && endShape2_ != _gesFlat)
    {
      nsize += 4;
      nshort++;
    }
    if (n_.size() == 1) { n_.resize(nsize, n_[0]); }
    else if (n_.size() == nshort)
    {
      std::vector<number_t> ntmp = n_;
      n_.resize(nsize);
      for (number_t i = 0; i < 4; ++i)
      {
        n_[i] = ntmp[0];
        n_[i + 4] = ntmp[1];
        n_[i + 8] = ntmp[2];
      }
      if (ntmp.size() >= 4)
      {
        for (number_t i = 0; i < 4; ++i)
        {
          n_[i + 12] = ntmp[3];
        }
      }
      if (ntmp.size() == 5)
      {
        for (number_t i = 0; i < 4; ++i)
        {
          n_[i + 16] = ntmp[4];
        }
      }
    }
    else if (n_.size() != nsize) { error("bad_size", "nodes", nsize, n_.size()); }
  }
  if (h_.size() != 0)
  {
    number_t hsize = 8;
    number_t hshort = 2;
    if (endShape1_ != _gesNone && endShape1_ != _gesFlat)
    {
      hsize++;
      hshort++;
    }
    if (endShape2_ != _gesNone && endShape2_ != _gesFlat)
    {
      hsize++;
      hshort++;
    }
    if (h_.size() == 1) { h_.resize(hsize, h_[0]); }
    else if (h_.size() == hshort)
    {
      std::vector<real_t> htmp = h_;
      h_.resize(hsize);
      for (number_t i = 0; i < 4; ++i)
      {
        h_[i] = htmp[0];
        h_[i + 4] = htmp[1];
        h_[i + 8] = htmp[2];
      }
      if (htmp.size() >= 4)
      {
        for (number_t i = 0; i < 4; ++i)
        {
          h_[i + 12] = htmp[3];
        }
      }
      if (htmp.size() == 5)
      {
        for (number_t i = 0; i < 4; ++i)
        {
          h_[i + 16] = htmp[4];
        }
      }
    }
    else if (h_.size() != hsize) { error("bad_size", "hsteps", hsize, h_.size()); }
  }
}

void RevTrunk::buildParam(const Parameter& p)
{
  trace_p->push("RevTrunk::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_radius1:
    {
      switch (p.type())
      {
        case _integer: radius1_ = real_t(p.get_i()); break;
        case _real: radius1_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      if (radius1_ == 0.) { error("is_null", "radius1"); }
      break;
    }
    case _pk_radius2:
    {
      switch (p.type())
      {
        case _integer: radius2_ = real_t(p.get_i()); break;
        case _real: radius2_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      if (radius2_ == 0.) { error("is_null", "radius2"); }
      break;
    }
    case _pk_end1_shape:
    {
      switch (p.type())
      {
        case _integer:
        case _enumGeometricEndShape:
          endShape1_ = GeometricEndShape(p.get_n());
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_end1_distance:
    {
      switch (p.type())
      {
        case _integer: distance1_ = real_t(p.get_i()); break;
        case _real: distance1_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_end2_shape:
    {
      switch (p.type())
      {
        case _integer:
        case _enumGeometricEndShape:
          endShape2_ = GeometricEndShape(p.get_n());
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_end2_distance:
    {
      switch (p.type())
      {
        case _integer: distance2_ = real_t(p.get_i()); break;
        case _real: distance2_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nbsubdomains:
    {
      switch (p.type())
      {
        case _integer: nbSubDomains_ = p.get_n(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_type:
    {
      switch (p.type())
      {
        case _integer: type_ = dimen_t(p.get_n()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Trunk::buildParam(p); break;
  }
  trace_p->pop();
}

void RevTrunk::buildDefaultParam(ParameterKey key)
{
  trace_p->push("RevTrunk::buildDefaultParam");
  switch (key)
  {
    case _pk_end1_shape: endShape1_ = _gesFlat; break;
    case _pk_end1_distance: distance1_ = 0.; break;
    case _pk_end2_shape: endShape2_ = _gesFlat; break;
    case _pk_end2_distance: distance2_ = 0.; break;
    case _pk_nbsubdomains: nbSubDomains_ = 1; break;
    case _pk_type: type_ = 1; break;
    case _pk_nnodes: n_ = std::vector<number_t>(1, 2); break;
    default: Trunk::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> RevTrunk::getParamsKeys()
{
  std::set<ParameterKey> params = Trunk::getParamsKeys();
  // _scale is replaced by radius1 and radius2, use _center2_ instead of _origin
  // _basis, _v1, _v2 are disabled
  params.erase(_pk_scale);
  params.erase(_pk_origin);
  params.erase(_pk_basis);
  params.erase(_pk_v1);
  params.erase(_pk_v2);
  params.insert(_pk_radius1);
  params.insert(_pk_radius2);
  params.insert(_pk_end1_shape);
  params.insert(_pk_end2_shape);
  params.insert(_pk_end1_distance);
  params.insert(_pk_end2_distance);
  params.insert(_pk_nbsubdomains);
  params.insert(_pk_type);
  return params;
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5)
  : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5,
                   Parameter p6) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  build(ps);
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12};
  build(ps);
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13};
  build(ps);
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14};
  build(ps);
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14, Parameter p15) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15};
  build(ps);
}

RevTrunk::RevTrunk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14, Parameter p15, Parameter p16) : Trunk(1., false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16};
  build(ps);
}

RevTrunk::RevTrunk(const RevTrunk& r) : Trunk(r), radius1_(r.radius1_), radius2_(r.radius2_), nbSubDomains_(r.nbSubDomains_),
  endShape1_(r.endShape1_), distance1_(r.distance1_),
  endShape2_(r.endShape2_), distance2_(r.distance2_), type_(r.type_)
{}

number_t RevTrunk::nbSubdiv() const
{
  number_t n0 = *std::max_element(n_.begin(), n_.end());
  return static_cast<number_t>(theTolerance + std::log(n0 - 1) / std::log(2));
}

//! Format RevTrunk as string: "RevTrunk(C1=(.,.,.), C2=(.,.,.), radius1=R1, radius2=R2, end1=., end2=., d1=., d2=.)"
string_t RevTrunk::asString() const
{
  string_t s("RevTrunk (C1 = ");
  s += center1_.roundToZero().toString() + ", C2 = " + origin_.roundToZero().toString();
  s += ", radius1 = " + tostring(radius1_) + ", radius2 = " + tostring(radius2_);
  s += ", endShape1 = " + tostring(endShape1_) + ", d1 = " + tostring(distance1_);
  s += ", endShape2 = " + tostring(endShape2_) + ", d2 = " + tostring(distance2_) + ")";
  return s;
}

//! returns list of points on boundary (const)
// p_ without centers of central trunk
std::vector<const Point*> RevTrunk::boundNodes() const
{
  std::vector<const Point*> ns;
  if (radius2_ != 0) // central part is not a cone:  centers p[0], p[5] omitted
  {
    ns.resize(p_.size() - 2);
    ns[0] = &p_[1]; ns[1] = &p_[2]; ns[2] = &p_[3]; ns[3] = &p_[4];
    number_t n = 4;
    for (number_t i = 6; i < p_.size(); i++, n++) { ns[n] = &p_[i]; }
  }
  else // central part is a cone: center p[0] omitted
  {
    ns.resize(p_.size() - 1);
    for (number_t i = 1; i < p_.size(); i++) { ns[i - 1] = &p_[i]; }
  }
  return ns;
}

//! returns list of curves (const)
std::vector<std::pair<ShapeType, std::vector<const Point*> > > RevTrunk::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > cs;
  std::vector<const Point*> vs(2);
  number_t n = 0, m = 0;
  if (radius2_ > 0) { cs.resize(12); }
  else { cs.resize(8); }
  for (number_t i = 0; i < 4; i++)
  {
    vs[0] = &p_[i + 1]; vs[1] = &p_[i + 2];
    if (i == 3) { vs[1] = &p_[1]; }
    cs[i] = std::make_pair(_circArc, vs);
  }
  if (radius2_ > 0)
  {
    for (number_t i = 0; i < 4; i++)
    {
      vs[0] = &p_[i + 6]; vs[1] = &p_[i + 7];
      if (i == 3) { vs[1] = &p_[6]; }
      cs[i + 4] = std::make_pair(_circArc, vs);
    }
    for (number_t i = 0; i < 4; i++)
    {
      vs[0] = &p_[i + 1]; vs[1] = &p_[i + 6];
      cs[i + 8] = std::make_pair(_segment, vs);
    }
    n = 12; m = 10;
  }
  else
  {
    vs[1] = &p_[5];
    for (number_t i = 0; i < 4; i++)
    {
      vs[0] = &p_[i + 1];
      cs[i + 4] = std::make_pair(_segment, vs);
    }
    n = 8; m = 6;
  }
  
  switch (endShape1_)
  {
    case _gesCone:
    case _gesSphere:
    case _gesEllipsoid:
    {
      cs.resize(n + 4);
      vs[1] = &p_[m];
      ShapeType sh = _segment;
      if (endShape1_ == _gesSphere) { sh = _circArc; }
      if (endShape1_ == _gesEllipsoid) { sh = _ellArc; }
      for (number_t i = 0; i < 4; i++)
      {
        vs[0] = &p_[i + 1];
        cs[n + i] = std::make_pair(sh, vs);
      }
      n += 4; m += 1;
      break;
    }
    case _gesFlat:
    case _gesNone:
    default:
      break;
  }
  
  switch (endShape2_)
  {
    case _gesCone:
    case _gesSphere:
    case _gesEllipsoid:
    {
      cs.resize(n + 4);
      vs[1] = &p_[m];
      ShapeType sh = _segment;
      if (endShape2_ == _gesSphere) { sh = _circArc; }
      if (endShape2_ == _gesEllipsoid) { sh = _ellArc; }
      for (number_t i = 0; i < 4; i++)
      {
        vs[0] = &p_[i + 6];
        cs[n + i] = std::make_pair(sh, vs);
      }
      break;
    }
    case _gesFlat:
    case _gesNone:
    default:    break;
  }
  
  return cs;
}

//! returns list of faces (const)
std::vector<std::pair<ShapeType, std::vector<const Point*> > > RevTrunk::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > ss;
  std::vector<const Point*> vs;
  number_t n, m, id = 0;
  if (radius2_ != 0) //cylinder
  { n = 4; m = 10; }
  else //cone
  { n = 4; m = 6; }
  
  switch (endShape1_)
  {
    case _gesFlat:
    {
      n++;
      break;
    }
    case _gesCone:
    case _gesSphere:
    case _gesEllipsoid:
    {
      n += 4; m++;
      break;
    }
    case _gesNone:
    default:
      break;
  }
  
  switch (endShape2_)
  {
    case _gesFlat:
    {
      n++;
      break;
    }
    case _gesCone:
    case _gesSphere:
    case _gesEllipsoid:
    {
      n += 4;
      break;
    }
    case _gesNone:
    default:
      break;
  }
  
  ss.resize(n);
  
  switch (endShape1_)
  {
    case _gesFlat:
    {
      vs.resize(4);
      vs[0] = &p_[1]; vs[1] = &p_[2]; vs[2] = &p_[3]; vs[3] = &p_[4];
      ss[0] = std::make_pair(_disk, vs);
      id++;
      break;
    }
    case _gesCone:
    case _gesSphere:
    case _gesEllipsoid:
    {
      vs.resize(3);
      vs[2] = &p_[10];
      ShapeType sh = _coneSidePart;
      if (endShape1_ == _gesSphere) { sh = _sphereSidePart; }
      if (endShape1_ == _gesEllipsoid) { sh = _ellipsoidSidePart; }
      for (number_t i = 0; i < 4; i++)
      {
        vs[0] = &p_[i + 1]; vs[1] = &p_[i + 2];
        if (i == 3) { vs[1] = &p_[1]; }
        ss[i] = std::make_pair(sh, vs);
      }
      id += 4;
      break;
    }
    case _gesNone:
    default:
      break;
  }
  
  vs.resize(4);
  switch (endShape2_)
  {
    case _gesFlat:
    {
      vs[0] = &p_[6]; vs[1] = &p_[7]; vs[2] = &p_[8]; vs[3] = &p_[9];
      ss[id] = std::make_pair(_disk, vs);
      id++;
      break;
    }
    case _gesCone:
    case _gesSphere:
    case _gesEllipsoid:
    {
      vs.resize(3);
      vs[2] = &p_[m];
      ShapeType sh = _segment;
      if (endShape2_ == _gesSphere) { sh = _sphereSidePart; }
      if (endShape2_ == _gesEllipsoid) { sh = _ellipsoidSidePart; }
      for (number_t i = 0; i < 4; i++)
      {
        vs[0] = &p_[i + 6]; vs[1] = &p_[i + 7];
        if (i == 3) { vs[1] = &p_[6]; }
        ss[id + i] = std::make_pair(sh, vs);
      }
      id += 4;
      break;
    }
    case _gesNone:
    default:
      break;
  }
  
  vs.resize(4);
  if (radius2_ != 0) //cylinder
  {
    for (number_t i = 0; i < 4; i++)
    {
      vs[0] = &p_[i + 1]; vs[1] = &p_[i + 6]; vs[2] = &p_[i + 7]; vs[3] = &p_[i + 2];
      if (i == 3) {vs[2] = &p_[6]; vs[3] = &p_[1];}
      ss[id + i] = std::make_pair(_cylinderSidePart, vs);
    }
  }
  else //cone
  {
    vs.resize(3);
    vs[2] = &p_[5];
    for (number_t i = 0; i < 4; i++)
    {
      vs[0] = &p_[i + 1]; vs[1] = &p_[i + 2];
      if (i == 3) { vs[1] = &p_[1]; }
      ss[id + i] = std::make_pair(_coneSidePart, vs);
    }
  }
  
  return ss;
}

//===============================================================================================
//                                     RevCylinder stuff
//===============================================================================================
RevCylinder::RevCylinder() : RevTrunk()
{
  shape_ = _revCylinder;
  computeMB();
}

void RevCylinder::build(const std::vector<Parameter>& ps)
{
  trace_p->push("RevCylinder::build");
  shape_ = _revCylinder;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _revCylinder)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }
  }
  
  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (center1,center2,radius) has to be set (no default values)
  if (params.find(_pk_center1) != params.end()) { error("param_missing", "center1"); }
  if (params.find(_pk_center2) != params.end()) { error("param_missing", "center2"); }
  if (params.find(_pk_radius) != params.end()) { error("param_missing", "radius"); }
  // _end1_shape and _end1_distance have to be set both
  if (params.find(_pk_end1_shape) == params.end() && params.find(_pk_end1_distance) != params.end() && endShape1_ != _gesNone && endShape1_ != _gesFlat)
  { error("param_missing", "end1_distance"); }
  if (params.find(_pk_end1_distance) == params.end() && params.find(_pk_end1_shape) != params.end())
  { error("param_missing", "end1_shape"); }
  // _end2_shape and _end2_distance have to be set both and with _end1_shape and _end1_distance
  if (params.find(_pk_end2_shape) == params.end() && params.find(_pk_end1_shape) != params.end())
  { error("param_missing", "end1_shape"); }
  if (params.find(_pk_end2_shape) == params.end() && params.find(_pk_end1_distance) != params.end() && endShape1_ != _gesNone && endShape1_ != _gesFlat)
  { error("param_missing", "end1_distance"); }
  if (params.find(_pk_end2_shape) == params.end() && params.find(_pk_end2_distance) != params.end() && endShape2_ != _gesNone && endShape2_ != _gesFlat)
  { error("param_missing", "end2_distance"); }
  if (params.find(_pk_end2_distance) == params.end() && params.find(_pk_end1_shape) != params.end())
  { error("param_missing", "end1_shape"); }
  if (params.find(_pk_end2_distance) == params.end() && params.find(_pk_end1_distance) != params.end() && endShape1_ != _gesNone && endShape1_ != _gesFlat)
  { error("param_missing", "end1_distance"); }
  if (params.find(_pk_end2_distance) == params.end() && params.find(_pk_end2_shape) != params.end())
  { error("param_missing", "end2_shape"); }
  
  // check height of revtrunk
  if (center1_ == origin_) { error("degenerated_elt", "revcylinder"); }
  
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  
  // checking nnodes and hsteps size
  checkNH();
  // p_ is not (fully) built so we do
  buildPScaleAndBasis();
  // create bounding boxes
  boundingBox = BoundingBox(p_);
  computeMB();
  trace_p->pop();
}

void RevCylinder::buildParam(const Parameter& p)
{
  trace_p->push("RevCylinder::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_radius:
    {
      switch (p.type())
      {
        case _integer: radius1_ = radius2_ = real_t(p.get_n()); break;
        case _real: radius1_ = radius2_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: RevTrunk::buildParam(p); break;
  }
  trace_p->pop();
}

void RevCylinder::buildDefaultParam(ParameterKey key)
{
  RevTrunk::buildDefaultParam(key);
}

std::set<ParameterKey> RevCylinder::getParamsKeys()
{
  std::set<ParameterKey> params = RevTrunk::getParamsKeys();
  // _scale is disabled: default value is 1 as in RevTrunk::buildDefaultParam(ParameterKey)
  // _radius1 and _radius2 are replaced by _radius
  // _basis, _v1, _v2 are disabled
  params.erase(_pk_scale);
  params.erase(_pk_basis);
  params.erase(_pk_v1);
  params.erase(_pk_v2);
  params.erase(_pk_radius1);
  params.erase(_pk_radius2);
  params.insert(_pk_radius);
  return params;
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5)
  : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10)
  : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  build(ps);
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12};
  build(ps);
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13};
  build(ps);
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14};
  build(ps);
}

RevCylinder::RevCylinder(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14, Parameter p15) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15};
  build(ps);
}

string_t RevCylinder::asString() const
{
  string_t s("RevCylinder (C1 = ");
  s += center1_.roundToZero().toString() + ", C2 = " + origin_.roundToZero().toString() + ", radius = " + tostring(radius1_);
  s += ", endShape1 = " + tostring(endShape1_) + ", d1 = " + tostring(distance1_);
  s += ", endShape2 = " + tostring(endShape2_) + ", d2 = " + tostring(distance2_) + ")";
  return s;
}

//===============================================================================================
//                                     RevCone stuff
//===============================================================================================
RevCone::RevCone() : RevTrunk(0.)
{
  shape_ = _revCone;
  computeMB();
}

void RevCone::build(const std::vector<Parameter>& ps)
{
  trace_p->push("RevCone::build");
  shape_ = _revCone;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _revCone)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }
  }
  
  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (center,radius,apex) has to be set (no default values)
  if (params.find(_pk_center) != params.end()) { error("param_missing", "center"); }
  if (params.find(_pk_radius) != params.end()) { error("param_missing", "radius"); }
  if (params.find(_pk_apex) != params.end()) { error("param_missing", "apex"); }
  
  // _end_shape and _end_distance have to be set both
  if (params.find(_pk_end_shape) == params.end() && params.find(_pk_end_distance) != params.end() && endShape1_ != _gesNone && endShape1_ != _gesFlat)
  { error("param_missing", "end_distance"); }
  if (params.find(_pk_end_distance) == params.end() && params.find(_pk_end_shape) != params.end())
  { error("param_missing", "end_shape"); }
  
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  
  checkNH();             // checking nnodes and hsteps size
  buildPScaleAndBasis(); // p_ is not (fully) built so we do
  radius2_ = 0.;         //to be consistant enforce radius2=0
  boundingBox = BoundingBox(p_); //create bounding boxes
  computeMB();
  trace_p->pop();
}

void RevCone::buildParam(const Parameter& p)
{
  trace_p->push("RevCone::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_center:
    {
      switch (p.type())
      {
        case _pt: center1_ = p.get_pt(); break;
        case _integer: center1_ = Point(real_t(p.get_i())); break;
        case _real: center1_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_radius:
    {
      switch (p.type())
      {
        case _integer: radius1_ = radius2_ = real_t(p.get_n()); break;
        case _real: radius1_ = radius2_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_apex:
    {
      switch (p.type())
      {
        case _pt: origin_ = p.get_pt(); break;
        case _integer: origin_ = Point(real_t(p.get_i())); break;
        case _real: origin_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_end_shape:
    {
      switch (p.type())
      {
        case _integer:
        case _enumGeometricEndShape:
          endShape1_ = GeometricEndShape(p.get_n());
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_end_distance:
    {
      switch (p.type())
      {
        case _integer: distance1_ = real_t(p.get_i()); break;
        case _real: distance1_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: RevTrunk::buildParam(p); break;
  }
  trace_p->pop();
}

void RevCone::buildDefaultParam(ParameterKey key)
{
  trace_p->push("RevCone::buildDefaultParam");
  switch (key)
  {
    case _pk_end_shape: endShape1_ = _gesFlat; break;
    case _pk_end_distance: distance1_ = 0.; break;
    default: RevTrunk::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> RevCone::getParamsKeys()
{
  // _center1 is replaced by _center, _center2 and _origin are replaced by _apex,
  // _end*_shape and _end*_distance are replaced by _end_shape and end_distance
  // _basis, _scale, _v1, _v2 are disabled
  
  std::set<ParameterKey> params = RevTrunk::getParamsKeys();
  params.insert(_pk_center);
  params.insert(_pk_radius);
  params.insert(_pk_apex);
  params.insert(_pk_end_shape);
  params.insert(_pk_end_distance);
  params.erase(_pk_basis);
  params.erase(_pk_scale);
  params.erase(_pk_center1);
  params.erase(_pk_center2);
  params.erase(_pk_origin);
  params.erase(_pk_v1);
  params.erase(_pk_v2);
  params.erase(_pk_radius1);
  params.erase(_pk_radius2);
  params.erase(_pk_end1_shape);
  params.erase(_pk_end1_distance);
  params.erase(_pk_end2_shape);
  params.erase(_pk_end2_distance);
  return params;
}

RevCone::RevCone(Parameter p1, Parameter p2, Parameter p3) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

RevCone::RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

RevCone::RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5)
  : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

RevCone::RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

RevCone::RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

RevCone::RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

RevCone::RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

RevCone::RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

RevCone::RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  build(ps);
}

RevCone::RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12};
  build(ps);
}

RevCone::RevCone(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13) : RevTrunk(false)
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13};
  build(ps);
}

//! format as string: "RevCone (C1=(.,.,.), C2=(.,.,.), radius1=R1, radius2=R2)"
string_t RevCone::asString() const
{
  string_t s("RevCone (C1 = ");
  s += center1_.roundToZero().toString() + ", radius = " + tostring(radius1_) + ", apex = " + origin_.roundToZero().toString();
  s += ", endShape1 = " + tostring(endShape1_) + ", d1 = " + tostring(distance1_) + ")";
  return s;
}

} // end of namespace xlifepp
