/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file geometries_utils.cpp
  \authors N. Kielbasiewicz, Y. Lafranche, E. Lunéville
  \since 18 oct 2012
  \date 5 may 2020

  \brief This file contains the implementation of methods defined in geometries_utils.hpp
*/

#include "geometries_utils.hpp"
#include "geometries1D.hpp"
#include "geometries2D.hpp"
#include "geometries3D.hpp"

namespace xlifepp {

number_t nodesDim(const Geometry& g)
{
  switch (g.shape())
  {
    case _segment:
      return g.segment()->p1().size();
    case _ellArc:
      return g.ellArc()->p1().size();
    case _circArc:
      return g.circArc()->p1().size();
    case _parametrizedArc:
      return g.parametrizedArc()->p1().size();
    case _splineArc:
      return g.splineArc()->spline().dim();
    case _polygon:
      return g.polygon()->p(1).size();
    case _triangle:
      return g.triangle()->p(1).size();
    case _quadrangle:
      return g.quadrangle()->p(1).size();
    case _parallelogram:
      return g.parallelogram()->p(1).size();
    case _rectangle:
      return g.rectangle()->p(1).size();
    case _square:
      return g.square()->p(1).size();
    case _ellipse:
      return g.ellipse()->p(1).size();
    case _disk:
      return g.disk()->p(1).size();
    case _parametrizedSurface:
      return g.parametrizedSurface()->p(1).size();
    case _splineSurface:
      return g.splineSurface()->p(1).size();
    case _polyhedron:
      return g.polyhedron()->p(1).size();
    case _tetrahedron:
      return g.tetrahedron()->p(1).size();
    case _hexahedron:
      return g.hexahedron()->p(1).size();
    case _parallelepiped:
      return g.parallelepiped()->p(1).size();
    case _cuboid:
      return g.cuboid()->p(1).size();
    case _cube:
      return g.cube()->p(1).size();
    case _ellipsoid:
      return g.ellipsoid()->p(1).size();
    case _ball:
      return g.ball()->p(1).size();
    case _trunk:
      return g.trunk()->p(1).size();
    case _cylinder:
      return g.cylinder()->p(1).size();
    case _prism:
      return g.prism()->p(1).size();
    case _cone:
      return g.cone()->p(1).size();
    case _pyramid:
      return g.pyramid()->p(1).size();
    case _revTrunk:
      return g.revTrunk()->p(1).size();
    case _revCylinder:
      return g.revCylinder()->p(1).size();
    case _revCone:
      return g.revCone()->p(1).size();
    case _extrusion:
      return nodesDim(*g.components().begin()->second);
    case _loop:
      return nodesDim(*g.components().begin()->second);
    case _composite:
      return nodesDim(*g.components().begin()->second);
    case _ocShape:
      return 3;
    case _fromFile:
      return 3;
    default:
      where("nodesDim(const Geometry&)");
      error("shape_not_handled", words("shape",g.shape()));
      break;
  }
  return 0; // dummy return
}

string_t oneOfStrings(const string_t& sn, int i)
{
  return sn;
}

string_t oneOfStrings(const std::vector<string_t>& sn, int i)
{
  if (sn.size() > i) { return sn[i]; }
  return string_t();
}

string_t oneOfStringsIfVector(const string_t& sn, int i)
{
  return string_t();
}

string_t oneOfStringsIfVector(const std::vector<string_t>& sn, int i)
{
  if (sn.size() > i) { return sn[i]; }
  return string_t();
}

number_t nbSubDomainsIfScalar(string_t sn, number_t nbSubDomains)
{
  return nbSubDomains;
}

number_t nbSubDomainsIfScalar(const std::vector<string_t>& sn, number_t nbSubDomains)
{
  return 1;
}

//---------------------------------------------------------------------------
// 1D geometry transformations facilities
//---------------------------------------------------------------------------

//! apply a geometrical transformation on a Curve
Curve& Curve::transform(const Transformation& t)
{
  where("Curve::transform ("+words("transform",t.transformType())+")");
  error("shape_not_handled",words("shape",shape_));
  return *this;
}

//! apply a geometrical transformation on a Segment
Segment& Segment::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a EllArc
EllArc& EllArc::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  b_=t.apply(b_);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a CircArc
CircArc& CircArc::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  a_=t.apply(a_);
  b_=t.apply(b_);
  if (t.scaleFactor()!=1.) radius_=c_.distance(a_);//consistant uptdate
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation to a SplineArc
SplineArc& SplineArc::transform(const Transformation& t)
{
  if(spline_==nullptr) return *this; //no spline no transformation
  if(spline_->type()==_C2Spline)
     error("free_error","geometrical transformation not allowed for C2-spline");
  // apply transformation to controlPoints
  std::vector<Point>& cpts=spline_->controlPoints();
  for (number_t i=0; i < cpts.size(); ++i) cpts[i]=t.apply(cpts[i]);
  //apply transformation to tangent vector at bounds
  spline_->yps()=t.apply(Point(spline_->yps()));
  spline_->ype()=t.apply(Point(spline_->ype()));
  // apply transformation to interpolation points if exist (only for B-spline)
  if(spline_->type()==_BSpline)
  {
      BSpline* bs=static_cast<BSpline*>(spline_);
      std::vector<Point>& ipts=bs->interpolationPoints();
      for (number_t i=0; i < ipts.size(); ++i) ipts[i]=t.apply(ipts[i]);
  }
  // apply transformation to bounding boxes
  boundingBox.transform(t);
  minimalBox.transform(t);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation to a ParametrizedArc
ParametrizedArc& ParametrizedArc::transform(const Transformation& t)
{
  // apply transformation to Points
  p1_=t.apply(p1_); p2_=t.apply(p2_);
  for (number_t i=0; i < p_.size(); ++i) p_[i]=t.apply(p_[i]);
  //update transformation_
  if(transformation_==nullptr) transformation_= t.clone();
  else *transformation_ = t* *transformation_;
  // apply transformation to bounding boxes
  boundingBox.transform(t);
  minimalBox.transform(t);
  ocTransform(t);
  return *this;
}

//---------------------------------------------------------------------------
// 2D geometry transformations facilities
//---------------------------------------------------------------------------

//! apply a geometrical transformation on a Surface
Surface& Surface::transform(const Transformation& t)
{
  where("Surface::transform ("+words("transform",t.transformType())+")");
  error("shape_not_handled",words("shape",shape_));
  return *this;
}

//! apply a geometrical transformation on a Polygon
Polygon& Polygon::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Triangle
Triangle& Triangle::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Quadrangle
Quadrangle& Quadrangle::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Parallelogram
Parallelogram& Parallelogram::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Rectangle
Rectangle& Rectangle::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  center_=t.apply(center_);
  origin_=t.apply(origin_);
  if(t.scaleFactor()!=1.) //consistant uptdate
  {
    xlength_=nodes[0]->distance(*nodes[1]);
    ylength_=nodes[0]->distance(*nodes[3]);
  }
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a SquareGeo
SquareGeo& SquareGeo::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  center_=t.apply(center_);
  origin_=t.apply(origin_);
  if(t.scaleFactor()!=1.) //consistant uptdate
  {
      xlength_=nodes[0]->distance(*nodes[1]);
      ylength_=xlength_;
  }
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Ellipse
Ellipse& Ellipse::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->wholeNodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  if(t.scaleFactor()!=1.) //consistant uptdate
    {
      xradius_=p_[0].distance(p_[1]);
      yradius_=p_[0].distance(p_[2]);
    }
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Disk
Disk& Disk::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->wholeNodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  if(t.scaleFactor()!=1.) //consistant uptdate
    {
      xradius_=p_[0].distance(p_[1]);
      yradius_=xradius_;
    }
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation to a ParametrizedSurface
ParametrizedSurface& ParametrizedSurface::transform(const Transformation& t)
{
  error("not_yet_implemented","ParametrizedSurface::transform");
  return *this;
}

//! apply a geometrical transformation to a SetOfElems
SetOfElems& SetOfElems::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation to a SplineSurface
SplineSurface& SplineSurface::transform(const Transformation& t)
{
  if(spline_==nullptr) return *this; //no spline no transformation
  // apply transformation to controlPoints of Nurbs
  Nurbs* nu = reinterpret_cast<Nurbs*>(spline_); //downcast to Nurbs
  std::vector<std::vector<Point> >& cpts=nu->controlPointsM();
  for (number_t i=0; i < cpts.size(); ++i)
     for (number_t j=0; j< cpts[i].size(); ++j)
       cpts[i][j]=t.apply(cpts[i][j]);
  // apply transformation to bounding boxes
  boundingBox.transform(t);
  minimalBox.transform(t);
  ocTransform(t);
  return *this;
}

//---------------------------------------------------------------------------
// 3D geometry transformations facilities
//---------------------------------------------------------------------------

//! apply a geometrical transformation on a Volume
Volume& Volume::transform(const Transformation& t)
{
  where("Volume::transform ("+words("transform",t.transformType())+")");
  error("shape_not_handled",words("shape",shape_));
  return *this;
}

//! apply a geometrical transformation on a Polyhedron
Polyhedron& Polyhedron::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  for(number_t f=0;f<faces_.size();f++) faces_[f]->transform(t);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Tetrahedron
Tetrahedron& Tetrahedron::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  for(number_t f=0;f<faces_.size();f++) faces_[f]->transform(t);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Hexahedron
Hexahedron& Hexahedron::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  // apply transformation on nodes ( std::vector<const Point*> )
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  for(number_t f=0;f<faces_.size();f++) faces_[f]->transform(t);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Parallelepiped
Parallelepiped& Parallelepiped::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->wholeNodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  for(number_t f=0;f<faces_.size();f++) faces_[f]->transform(t);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Cuboid
Cuboid& Cuboid::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->wholeNodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  center_=t.apply(center_);
  origin_=t.apply(origin_);
  if(t.scaleFactor()!=1.) //consistant uptdate
  {
    xlength_=p_[0].distance(p_[1]);
    ylength_=p_[0].distance(p_[3]);
    zlength_=p_[0].distance(p_[4]);
  }
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Cube
Cube& Cube::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->wholeNodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  center_=t.apply(center_);
  origin_=t.apply(origin_);
  if(t.scaleFactor()!=1.) //consistant uptdate
  {
     xlength_=p_[0].distance(p_[1]);
     ylength_=xlength_; zlength_=xlength_;
  }
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Ellipsoid
Ellipsoid& Ellipsoid::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->wholeNodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  if(t.scaleFactor()!=1.) //consistant uptdate
  {
    xradius_=p_[0].distance(p_[1]);
    yradius_=p_[0].distance(p_[3]);
    zradius_=p_[0].distance(p_[6]);
  }
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Ball
Ball& Ball::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->wholeNodes();
  // apply transformation on nodes ( std::vector<const Point*> )
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  if(t.scaleFactor()!=1.) //consistant uptdate
  {
    xradius_=p_[0].distance(p_[1]);
    yradius_=xradius_;
    zradius_=xradius_;
  }
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Trunk
Trunk& Trunk::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  center1_=t.apply(center1_);
  origin_=t.apply(origin_);
  p1_=t.apply(p1_);
  p2_=t.apply(p2_);
  basis_->transform(t);
  // scale parameter not transformed
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Cylinder
Cylinder& Cylinder::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  center1_=t.apply(center1_);
  origin_=t.apply(origin_);
  p1_=t.apply(p1_);
  p2_=t.apply(p2_);
  basis_->transform(t);
  dir_=origin_-center1_;
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Prism
Prism& Prism::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  center1_=t.apply(center1_);
  origin_=t.apply(origin_);
  p1_=t.apply(p1_);
  p2_=t.apply(p2_);
  p2_=t.apply(p3_);
  basis_->transform(t);
  dir_=origin_-center1_;
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Cone
Cone& Cone::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  center1_=t.apply(center1_);
  origin_=t.apply(origin_);
  p1_=t.apply(p1_);
  p2_=t.apply(p2_);
  basis_->transform(t);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a Pyramid
Pyramid& Pyramid::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  center1_=t.apply(center1_);
  origin_=t.apply(origin_);
  p1_=t.apply(p1_);
  p2_=t.apply(p2_);
  basis_->transform(t);
  p3_=t.apply(p3_);
  p4_=t.apply(p4_);
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a RevTrunk
RevTrunk& RevTrunk::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  center1_=t.apply(center1_);
  origin_=t.apply(origin_);
  p1_=t.apply(p1_);
  p2_=t.apply(p2_);
  basis_->transform(t);
  real_t s=t.scaleFactor();
  if(s!=1.)
  {
      radius1_*=s; radius2_*=s;
      distance1_*=s; distance2_*=s;
  }
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a RevCylinder
RevCylinder& RevCylinder::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  center1_=t.apply(center1_);
  origin_=t.apply(origin_);
  p1_=t.apply(p1_);
  p2_=t.apply(p2_);
  basis_->transform(t);
  real_t s=t.scaleFactor();
  if(s!=1.)
  {
      radius1_*=s; radius2_*=s;
      distance1_*=s; distance2_*=s;
  }
  ocTransform(t);
  return *this;
}

//! apply a geometrical transformation on a RevCone
RevCone& RevCone::transform(const Transformation& t)
{
  std::vector<Point*> nodes=this->nodes();
  // apply transformation on nodes ( std::vector<const Point*> )
  for (number_t i=0; i < nodes.size(); ++i) { *nodes[i]=t.apply(*nodes[i]); }
  boundingBox.transform(t);
  minimalBox.transform(t);
  center1_=t.apply(center1_);
  origin_=t.apply(origin_);
  p1_=t.apply(p1_);
  p2_=t.apply(p2_);
  basis_->transform(t);
  real_t s=t.scaleFactor();
  if(s!=1.)
  {
      radius1_*=s; radius2_*=s;
      distance1_*=s; distance2_*=s;
  }
  ocTransform(t);
  return *this;
}

//---------------------------------------------------------------------------
// transformations facilities (template external)
//---------------------------------------------------------------------------

template <class Geom>
Geom transform(const Geom& g, const Transformation& t)
{
  Geom g2=g;
  // we call the corresponding member function, as a Geom object carries few data
  g2.transform(t);
  return g2;
}

template Curve transform(const Curve& g, const Transformation& t);
template Segment transform(const Segment& g, const Transformation& t);
template EllArc transform(const EllArc& g, const Transformation& t);
template CircArc transform(const CircArc& g, const Transformation& t);
template SplineArc transform(const SplineArc& g, const Transformation& t);
template ParametrizedArc transform(const ParametrizedArc& g, const Transformation& t);
template Surface transform(const Surface& g, const Transformation& t);
template Polygon transform(const Polygon& g, const Transformation& t);
template Triangle transform(const Triangle& g, const Transformation& t);
template Quadrangle transform(const Quadrangle& g, const Transformation& t);
template Parallelogram transform(const Parallelogram& g, const Transformation& t);
template Rectangle transform(const Rectangle& g, const Transformation& t);
template SquareGeo transform(const SquareGeo& g, const Transformation& t);
template Ellipse transform(const Ellipse& g, const Transformation& t);
template Disk transform(const Disk& g, const Transformation& t);
template SetOfElems transform(const SetOfElems & g, const Transformation& t);
template Volume transform(const Volume& g, const Transformation& t);
template Polyhedron transform(const Polyhedron& g, const Transformation& t);
template Tetrahedron transform(const Tetrahedron& g, const Transformation& t);
template Hexahedron transform(const Hexahedron& g, const Transformation& t);
template Parallelepiped transform(const Parallelepiped& g, const Transformation& t);
template Cuboid transform(const Cuboid& g, const Transformation& t);
template Cube transform(const Cube& g, const Transformation& t);
template Ellipsoid transform(const Ellipsoid& g, const Transformation& t);
template Ball transform(const Ball& g, const Transformation& t);
template Trunk transform(const Trunk& g, const Transformation& t);
template Cylinder transform(const Cylinder& g, const Transformation& t);
template Prism transform(const Prism& g, const Transformation& t);
template Cone transform(const Cone& g, const Transformation& t);
template Pyramid transform(const Pyramid& g, const Transformation& t);
template RevTrunk transform(const RevTrunk& g, const Transformation& t);
template RevCylinder transform(const RevCylinder& g, const Transformation& t);
template RevCone transform(const RevCone& g, const Transformation& t);

} // end of namespace xlifepp
