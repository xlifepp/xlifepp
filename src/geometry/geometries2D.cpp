/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file geometries2D.cpp
  \authors N. Kielbasiewicz, Y. Lafranche
  \since 18 oct 2012
  \date 30 jul 2015

  \brief This file contains the implementation of methods of classes defined in geometries2D.hpp
*/

#include "geometries2D.hpp"
#include "geometries1D.hpp"
#include "Mesh.hpp"

namespace xlifepp
{

//===========================================================
// Surface and child classes member functions
//===========================================================

//! default surface is inside the bounding box [0,1]^2
Surface::Surface() : Geometry(BoundingBox(0., 1., 0., 1.), 2)
{
  // funhx_=nullptr;
  // funhy_=nullptr;
}

void Surface::buildParam(const Parameter& p)
{
  trace_p->push("Surface::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_edge_names:
    case _pk_side_names:
    {
      if (p.type() == _string) { sideNames_.resize(1, p.get_s()); }
      else if (p.type() == _stringVector) { sideNames_ = p.get_sv(); }
      else { error("param_badtype", words("value", p.type()), words("param key", key)); }
      break;
    }
    case _pk_vertex_names:
    {
      if (p.type() == _string) { sideOfSideNames_.resize(1, p.get_s()); }
      else if (p.type() == _stringVector) { sideOfSideNames_ = p.get_sv(); }
      else { error("param_badtype", words("value", p.type()), words("param key", key)); }
      break;
    }
    default: Geometry::buildParam(p); break;
  }
  for (number_t i = 0; i < sideNames_.size(); ++i)
  {
    if (sideNames_[i].find("#") == 0) { error("domain_name_invalid"); }
    if (sideNames_[i].find(" ") == 0) { error("domain_name_invalid"); }
  }
  for (number_t i = 0; i < sideOfSideNames_.size(); ++i)
  {
    if (sideOfSideNames_[i].find("#") == 0) { error("domain_name_invalid"); }
    if (sideOfSideNames_[i].find(" ") == 0) { error("domain_name_invalid"); }
  }
  trace_p->pop();
}

void Surface::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Surface::buildDefaultParam");
  switch (key)
  {
    case _pk_edge_names:
    case _pk_side_names: sideNames_.resize(0); break;
    case _pk_vertex_names: sideOfSideNames_.resize(0); break;
    default: Geometry::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> Surface::getParamsKeys()
{
  std::set<ParameterKey> params = Geometry::getParamsKeys();
  params.insert(_pk_side_names);
  params.insert(_pk_edge_names);
  params.insert(_pk_vertex_names);
  return params;
}


bool Surface::isTranslated(const Geometry& g, Point& T) const
{
  if (shape() != g.shape()) { return false; }
  const Surface& s = *g.surface();
  return isTranslatedPoints(p_, g.surface()->p_, T);
}

//! compute approximated bounding box using parametrization if exists
//! heavy computation, overiden by childs if possible
void Surface::computeBB()
{
  if (parametrization_ == nullptr) { return; } //no parametrization -> no computation
  number_t n = 50;
  RealPair us = parametrization_->bounds(_x1), vs = parametrization_->bounds(_x2);
  real_t du = (us.second - us.first) / n, u = us.first;
  real_t dv = (vs.second - vs.first) / n, v = vs.first;
  number_t d = parametrization_->dim;
  std::vector<RealPair> rps(d, RealPair(theRealMax, -theRealMax));
  for (number_t i = 0; i <= n; i++, u += du)
  {
    v = vs.first;
    for (number_t j = 0; j <= n; j++, v += dv)
    {
      Point P = (*parametrization_)(u, v);
      for (number_t k = 0; k < d; k++)
      {
        if (P[k] < rps[k].first) { rps[k].first = P[k]; }
        if (P[k] > rps[k].second) { rps[k].second = P[k]; }
      }
    }
  }
  boundingBox = BoundingBox(rps);
}

//==========================================================
// Polygon class member functions
//===========================================================

//! default polygon is triangle is (0,0) (1,0) (0,1), parametrization not allocated
Polygon::Polygon() : Surface()
{
  n_.resize(3, 2);
  p_.resize(3);
  p_[0] = Point(0., 0., 0.);
  p_[1] = Point(1., 0., 0.);
  p_[2] = Point(0., 1., 0.);
  shape_ = _polygon;
  computeMB();
}

Polygon::Polygon(const std::vector<Point>& vertices, const std::vector<number_t>& n, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Surface()
{
  n_ = n;
  p_ = vertices;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  shape_ = _polygon;
  boundingBox = BoundingBox(p_);
  computeMB();
  initParametrization();
}

Polygon::Polygon(const std::vector<Point>& vertices, const std::vector<real_t>& h, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames)
{
  h_ = h;
  p_ = vertices;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  shape_ = _polygon;
  boundingBox = BoundingBox(p_);
  computeMB();
  initParametrization();
}

void Polygon::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Polygon::build");
  shape_ = _polygon;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _polygon)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or edge_names, not both
    if (key == _pk_edge_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_edge_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_edge_names)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _edge_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_edge_names); }
  if (usedParams.find(_pk_edge_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // vertices has to be set (no default values)
  if (params.find(_pk_vertices) != params.end()) { error("param_missing", "vertices"); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  // checking nnodes and hsteps size
  if (n_.size() != 0)
  {
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(p_.size(), n0); }
    else if (n_.size() != p_.size()) {error("bad_size", "nnodes", p_.size(), n_.size()); }
  }
  if (h_.size() != 0)
  {
    if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(p_.size(), h0); }
    else if (h_.size() != p_.size()) {error("bad_size", "hsteps", p_.size(), h_.size()); }
  }

  boundingBox = BoundingBox(p_);
  computeMB();
  initParametrization();
  trace_p->pop();
}

// build a PiecewiseParametrization based on a triangle partitionning
void Polygon::initParametrization()
{
  std::vector<std::vector<number_t> > triangles = splitInTriangles(p_);
  // theCout<<"triangles: "<<triangles<<eol;
  std::vector<std::vector<number_t> >::iterator it = triangles.begin();
  auxiliaryGeometry_ = new Triangle(_v1 = p_[it->at(0)], _v2 = p_[it->at(1)], _v3 = p_[it->at(2)]);
  it++;
  for (; it != triangles.end(); ++it)
  { (*auxiliaryGeometry_) += Triangle(_v1 = p_[it->at(0)], _v2 = p_[it->at(1)], _v3 = p_[it->at(2)]); }
  // theCout<<"auxiliary: "<<(*auxiliaryGeometry_)<<eol;
  std::map<number_t, Geometry*>::const_iterator ita;
  std::map<number_t, Geometry*>& comp = auxiliaryGeometry_->components();
  // for (ita=comp.begin(); ita!=comp.end(); ++ita)
  // {
  //   theCout << (*ita->second) << eol;
  //   theCout << (ita->second->parametrization()) << eol;
  // }
  auxiliaryGeometry_->buildC0PiecewiseParametrization();
  parametrization_ = auxiliaryGeometry_->parametrizationP();
}

void Polygon::buildParam(const Parameter& p)
{
  trace_p->push("Polygon::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_vertices:
    {
      switch (p.type())
      {
        case _ptVector: p_ = p.get_ptv(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      if (!arePointsCoplanar(p_)) { error("vertices_not_coplanar", words("shape", _polygon)); }
      break;
    }
    case _pk_nnodes:
    {
      switch (p.type())
      {
        case _integerVector:
        {
          std::vector<number_t> n = p.get_nv();
          n_.resize(n.size());
          for (number_t i = 0; i < n.size(); ++i) { n_[i] = n[i] > 2 ? n[i] : 2; }
          break;
        }
        case _integer: n_ = std::vector<number_t>(1, p.get_n() > 2 ? p.get_n() : 2); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_hsteps:
    {
      switch (p.type())
      {
        case _realVector: h_ = p.get_rv(); break;
        case _real: h_ = std::vector<real_t>(1, p.get_r()); break;
        case _integer: h_ = std::vector<real_t>(1, real_t(p.get_n())); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Surface::buildParam(p); break;
  }
  trace_p->pop();
}

void Polygon::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Polygon::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_ = std::vector<number_t>(p_.size(), 2); break;
    default: Surface::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> Polygon::getParamsKeys()
{
  std::set<ParameterKey> params = Surface::getParamsKeys();
  params.insert(_pk_vertices);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  return params;
}

Polygon::Polygon(Parameter p1) : Surface()
{
  std::vector<Parameter> ps={p1};
  build(ps);
}

Polygon::Polygon(Parameter p1, Parameter p2) : Surface()
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

Polygon::Polygon(Parameter p1, Parameter p2, Parameter p3) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Polygon::Polygon(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Polygon::Polygon(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Polygon::Polygon(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

string_t Polygon::asString() const
{
  string_t s("Polygon (");
  s += tostring(p_.size()) + " vertices)";
  return s;
}

//! print additional information
void Polygon::printDetail(std::ostream& os) const
{
  os << "  Points: " << p_;
}

void Polygon::computeMB()
{
  if (boundingBox.dim() == 2)
  {
    minimalBox = MinimalBox(boundingBox.bounds());
  }
  else // boundingBox is 3D but minimalBox is 2D
  {
    // we choose determine an edge separating keeping the whole polygon on the same side
    std::pair<number_t, number_t> edge = nonSeparatingEdge(p_);
    Point& p1 = p_[edge.first];
    Point& p2 = p_[edge.second];
    Point p21 = p2 - p1;
    std::vector<Point> vertices(3);
    // we project each other vertex on this edge and keep extremal projections
    number_t vmin = edge.first, vmax = edge.second;
    real_t xmin = 0., xmax = 0., h;
    for (number_t i = 0; i < p_.size(); ++i)
    {
      if (i != edge.first && i != edge.second)
      {
        Point p = projectionOnStraightLine(p_[i], p1, p2, h);
        real_t x = dot(p21, p - p1);
        if (x < xmin) { xmin = x; vmin = i; }
        if (x > xmax) { xmax = x; vmax = i; }
      }
    }
    // save 2 first points defining the minimal box
    if (vmin != edge.first) { vertices[0] = projectionOnStraightLine(p_[vmin], p1, p2, h); }
    else { vertices[0] = p1; }
    if (vmax != edge.second) { vertices[1] = projectionOnStraightLine(p_[vmax], p1, p2, h); }
    else { vertices[1] = p2; }

    /*
      ------------------------------------------------------
      use another vertex to determine an orthogonal bihedral
      ------------------------------------------------------
    */
    // we project each other vertex on newly built axis and keep maximal projection
    Point v21 = vertices[1] - vertices[0];
    real_t hmax = 0.;
    number_t vhmax = vmin;
    for (number_t i = 0; i < p_.size(); ++i)
    {
      if (i != edge.first && i != edge.second)
      {
        Point p = projectionOnStraightLine(p_[i], vertices[0], vertices[1], h);
        if (h > hmax) { hmax = h; vhmax = i; }
      }
    }
    // save last point
    vertices[2] = p_[vhmax] + vertices[0] - projectionOnStraightLine(p_[vhmax], vertices[0], vertices[1], h);
    minimalBox = MinimalBox(vertices);
  }
}

std::vector<int_t> Polygon::nnodesPerBorder()
{
  if (n_.size() == 0) { return std::vector<int_t>(h_.size(), -1); }
  else
  {
    std::vector<int_t> n(n_.size());
    for (number_t i = 0; i < n.size(); ++i) { n[i] = n_[i]; }
    return n;
  }
}

std::vector<const Point*> Polygon::boundNodes() const
{
  number_t nbpts = p_.size();
  std::vector<const Point*> nodes(nbpts);
  for (number_t i = 0; i < nbpts; ++i) { nodes[i] = &p_[i]; }
  return nodes;
}

std::vector<Point*> Polygon::nodes()
{
  number_t nbpts = p_.size();
  std::vector<Point*> nodes(nbpts);
  for (number_t i = 0; i < nbpts; ++i) { nodes[i] = &p_[i]; }
  return nodes;
}

std::vector<const Point*> Polygon::nodes() const
{
  number_t nbpts = p_.size();
  std::vector<const Point*> nodes(nbpts);
  for (number_t i = 0; i < nbpts; ++i) { nodes[i] = &p_[i]; }
  return nodes;
}

std::vector<const Point*> Polygon::vertices() const  //same as nodes()
{
  return nodes();
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Polygon::curves() const
{
  number_t nbpts = p_.size();
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves(nbpts);

  for (number_t i = 0; i < nbpts - 1; ++i)
  {
    std::vector<const Point*> vertices(2);
    vertices[0] = &p_[i]; vertices[1] = &p_[i + 1];
    curves[i] = std::make_pair(_segment, vertices);
  }
  std::vector<const Point*> vertices(2);
  vertices[0] = &p_[nbpts - 1]; vertices[1] = &p_[0];
  curves[nbpts - 1] = std::make_pair(_segment, vertices);
  return curves;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Polygon::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(1);
  surfs[0] = std::make_pair(_polygon, boundNodes());
  return surfs;
}

//! return centroid of polygon
Point Polygon::centroid() const
{
  number_t n = p_.size();
  Point G = 0 * p_[0];
  for (number_t i = 0; i < n; ++i) { G += p_[i]; }
  return G /= n;
}

//! collect in a list all canonical geometry's with name n (may be the polygon itself and some polygon sides)
void Polygon::collect(const string_t& n, std::list<Geometry*>& geoms) const
{
  if (domName_ == n) { geoms.push_back(const_cast<Geometry*>(static_cast<const Geometry*>(this))); }
  //loop on sides
  std::vector<string_t>::const_iterator its = sideNames_.begin();
  number_t ns = sideNames_.size();
  for (number_t s = 0; s < sideNames_.size(); ++its, ++s)
  {
    if (*its == n) //create geometry related to side
    {
      number_t sp1 = s + 1;
      if (sp1 == ns) { sp1 = 0; }
      geoms.push_back(new Segment(_v1 = p_[s], _v2 = p_[sp1], _domain_name = n));
    }
  }
}

//! create boundary geometry of polygons
Geometry& Polygon::buildBoundary() const
{
  boundaryGeometry_ = new Geometry(boundingBox, 1);
  Geometry& g = *boundaryGeometry_;
  g.shape(_composite);
  g.minimalBox = minimalBox;
  string_t name = "";
  if (domName_ != "") { name = domName_ + "_boundary"; }
  if (name != "") { g.domName(name); }
  number_t nbpts = p_.size();
  std::map<number_t, Geometry*>& cps = g.components();
  std::map<number_t, std::vector<number_t> >& geos = g.geometries();
  number_t i = 0; Segment* seg = 0;
  for (; i < nbpts; ++i)
  {
    number_t ip1 = i + 1;
    if (ip1 == nbpts) { ip1 = 0; }
    if (n_.size() > 0) { seg = new Segment(_v1 = p_[i], _v2 = p_[ip1], _nnodes = n_[i]); }
    else { seg = new Segment(_v1 = p_[i], _v2 = p_[ip1], _hsteps = Reals(h_[i], h_[ip1])); }
    if (sideNames_.size() == 1) { seg->domName(sideNames_[0]); }
    else if (sideNames_.size() > i) { seg->domName(sideNames_[i]); }
    cps[i] = seg;
    geos[i].push_back(i);
  }
  return g;
}

//==========================================================
// Triangle class member functions
//===========================================================

//! default triangle is (0,0) (1,0) (0,1), parametrization not allocated
Triangle::Triangle() : Polygon()
{ shape_ = _triangle; }

Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3, const std::vector<number_t>& n, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames)
  : Polygon()
{
  n_ = n;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  p_.resize(3);
  p_[0] = p1; p_[1] = p2; p_[2] = p3;
  boundingBox = BoundingBox(p1, p2, p3);
  computeMB();
  shape_ = _triangle;
  initParametrization();
}

Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3, const std::vector<real_t>& h, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames)
  : Polygon()
{
  h_ = h;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  p_.resize(3);
  p_[0] = p1; p_[1] = p2; p_[2] = p3;
  boundingBox = BoundingBox(p1, p2, p3);
  computeMB();
  shape_ = _triangle;
  initParametrization();
}

void Triangle::initParametrization()
{
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  //parametrization_ = new Parametrization(Triangle(Point(0.,0.),Point(1.,0.),Point(0.,1.)), parametrization_Triangle, pars,"Triangle parametrization");
  parametrization_ = new Parametrization(SquareGeo(Point(0., 0.), Point(1., 0.), Point(0., 1.)), parametrization_Triangle, pars, "Triangle Duffy parametrization");
  parametrization_->setinvParametrization(invParametrization_Triangle);
}

void Triangle::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Triangle::build");
  shape_ = _triangle;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // _vertices key is replaced by _v1, _v2 and _v3
  params.erase(_pk_vertices);

  p_.resize(3);
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _triangle)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or edge_names, not both
    if (key == _pk_edge_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_edge_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_edge_names)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _edge_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_edge_names); }
  if (usedParams.find(_pk_edge_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // (v1,v2,v3) has to be set (no default values)
  if (params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v3) != params.end()) { error("param_missing", "v3"); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  // checking nnodes and hsteps size
  if (n_.size() != 0)
  {
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(3, n0); }
    else if (n_.size() != 3) {error("bad_size", "nnodes", 3, n_.size()); }
  }
  if (h_.size() != 0)
  {
    if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(3, h0); }
    else if (h_.size() != 3) {error("bad_size", "hsteps", 3, h_.size()); }
  }

  boundingBox = BoundingBox(p_[0], p_[1], p_[2]);
  computeMB();
  initParametrization();
  trace_p->pop();
}

void Triangle::buildParam(const Parameter& p)
{
  trace_p->push("Triangle::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_v1:
    {
      switch (p.type())
      {
        case _pt: p_[0] = p.get_pt(); break;
        case _integer: p_[0] = Point(real_t(p.get_i())); break;
        case _real: p_[0] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v2:
    {
      switch (p.type())
      {
        case _pt: p_[1] = p.get_pt(); break;
        case _integer: p_[1] = Point(real_t(p.get_i())); break;
        case _real: p_[1] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v3:
    {
      switch (p.type())
      {
        case _pt: p_[2] = p.get_pt(); break;
        case _integer: p_[2] = Point(real_t(p.get_i())); break;
        case _real: p_[2] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Polygon::buildParam(p); break;
  }
  trace_p->pop();
}

void Triangle::buildDefaultParam(ParameterKey key)
{
  Polygon::buildDefaultParam(key);
}

std::set<ParameterKey> Triangle::getParamsKeys()
{
  std::set<ParameterKey> params = Polygon::getParamsKeys();
  params.insert(_pk_v1);
  params.insert(_pk_v2);
  params.insert(_pk_v3);
  return params;
}

Triangle::Triangle(Parameter p1, Parameter p2, Parameter p3) : Polygon()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Triangle::Triangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Polygon()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Triangle::Triangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Polygon()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Triangle::Triangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Polygon()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Triangle::Triangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Polygon()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Triangle::Triangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Polygon()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

string_t Triangle::asString() const
{
  string_t s("Triangle (");
  s += p_[0].roundToZero().toString() + ", " + p_[1].roundToZero().toString() + ", " + p_[2].roundToZero().toString() + ")";
  return s;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Triangle::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(1);
  surfs[0] = std::make_pair(_triangle, boundNodes());
  return surfs;
}

real_t Triangle::measure() const
{
  real_t h;
  Point P = projectionOnStraightLine(p_[2], p_[0], p_[1], h);
  return 0.5 * (p_[0].distance(p_[1])) * h;
}

//! parametrization (P1) : p = (1-u-v)*p1+u*p2+v*p3 = p1+u*(p2-p1)+v*(p3-p1)
//!                        with 0<= u+v <=1
Vector<real_t> Triangle::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  number_t dim = p_[0].size();
  Vector<real_t> R(dim);
  switch (d)
  {
    case _id:
      for (number_t k = 0; k < dim; k++) { R[k] = p_[0][k] + pt[0] * (p_[1][k] - p_[0][k]) + pt[1] * (p_[2][k] - p_[0][k]); }
      break;
    case _d1:
      for (number_t k = 0; k < dim; k++) { R[k] = (p_[1][k] - p_[0][k]); }
      break;
    case _d2:
      for (number_t k = 0; k < dim; k++) { R[k] = (p_[2][k] - p_[0][k]); }
      break;
    case _d11:
    case _d22:
    case _d12:
    case _d21:
      for (number_t k = 0; k < dim; k++) { R[k] = 0; }
      break;
    default:
      parfun_error("Triangle parametrization", d);
  }
  return R;
}

//! inverse of parametrization (u,v)=invf(p)
Vector<real_t> Triangle::invParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  if (d != _id) { parfun_error("Triangle::invParametrization", d); }
  Vector<real_t> uv(2, 0.);
  Point p1p2 = p_[1]; p1p2 -= p_[0];
  Point p1p3 = p_[2]; p1p3 -= p_[0];
  Point p1p = pt; p1p -= p_[0];
  real_t a = dot(p1p2, p1p2), b = dot(p1p2, p1p3), c = dot(p1p3, p1p3), e = b * b - a * c;
  real_t f = dot(p1p, p1p2), g = dot(p1p, p1p3);
  uv[0] = (b * g - c * f) / e; uv[1] = (b * f - a * g) / e;
  if (uv[0] < -theTolerance || uv[0] > 1 + theTolerance || uv[1] < -theTolerance || uv[1] > 1 + theTolerance)
  { error("free_error", "Triangle::invParametrization fails because point is not located in the triangle"); }
  uv[0] = std::min(std::max(0., uv[0]), 1.); //rounded
  uv[1] = std::min(std::max(0., uv[1]), 1.);
  if (uv[0] + uv[1] > 1)
  { error("free_error", "Triangle::invParametrization fails because point is not located in the triangle"); }
  return uv;
}

/*! Duffy parametrization:  p=(1-u)*p1+u(1-v)*p2+uv*p3  with 0<= u,v <=1
    u=0, 0<=v<=1 ->  p1      u=1, 0<=v<=1 -> [p2,p3]
    v=0, 0<=u<=1 -> [p1,p2]  v=1, 0<=u<=1 -> [p1,p3] */
Vector<real_t> Triangle::duffyParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  number_t dim = p_[0].size();
  Vector<real_t> R(dim);
  real_t u = pt[0], v = pt[1];
  switch (d)
  {
    case _id:
      for (number_t k = 0; k < dim; k++) { R[k] = (1 - u) * p_[0][k] + u * (1 - v) * p_[1][k] + u * v * p_[2][k]; }
      break;
    case _d1:
      for (number_t k = 0; k < dim; k++) { R[k] = (p_[1][k] - p_[0][k]) + v * (p_[2][k] - p_[1][k]); }
      break;
    case _d2:
      for (number_t k = 0; k < dim; k++) { R[k] = u * (p_[2][k] - p_[1][k]); }
      break;
    case _d12:
    case _d21:
      for (number_t k = 0; k < dim; k++) { R[k] = p_[2][k] - p_[1][k]; }
      break;
    case _d11:
    case _d22:
      for (number_t k = 0; k < dim; k++) { R[k] = 0; }
      break;
    default:
      parfun_error("Triangle Duffy parametrization", d);
  }
  return R;
}

/*! inverse of Duffy parametrization
    u =( p-p1).o32 / (p2-p1).o32  with o32 any orthogonal vector to p3-p2
    uv = (p-p1).o21 / (p3-p2).o21  with o21 any orthogonal vector to p2-p
    if p=p1 then u=0 and v may be anything
*/
Vector<real_t> Triangle::invDuffyParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  if (d != _id) { parfun_error("Triangle::invDuffyParametrization", d); }
  Point p12 = force3D(p_[1] - p_[0]), p13 = force3D(p_[2] - p_[0]), p23 = force3D(p_[2] - p_[1]);
  Point o = cross3D(p12, p13), o32 = cross3D(o, p23);
  Vector<real_t> uv(2, 0.);
  Point p = force3D(pt - p_[0]);
  uv[0] = dot(p, o32) / dot(p12, o32);
  if (uv[0] > 1 + theTolerance || uv[0] < -theTolerance)
  { error("free_error", "Triangle::invDuffyParametrization, u=" + tostring(uv[0]) + " is out of the parametrization"); }
  if (std::abs(uv[0]) > theTolerance)
  {
    Point o21 = cross3D(o, p12);
    uv[1] = dot(p, o21) / dot(p23, o21);
    if (uv[1] > 1 + theTolerance || uv[1] < -theTolerance)
    { error("free_error", "Triangle::invDuffyParametrization, v=" + tostring(uv[1]) + " is out of the parametrization"); }
  }
  uv[0] = std::min(std::max(0., uv[0]), 1.); //rounded
  uv[1] = std::min(std::max(0., uv[1]), 1.);
  return uv;
}

//==========================================================
// Quadrangle class member functions
//===========================================================

//! default quadrangle is [0,1]^2 no parametrization
Quadrangle::Quadrangle() : Polygon()
{
  n_.resize(4, 2);
  p_.resize(4);
  p_[0] = Point(0., 0., 0.);
  p_[1] = Point(1., 0., 0.);
  p_[2] = Point(1., 1., 0.);
  p_[3] = Point(0., 1., 0.);
  boundingBox = BoundingBox(p_[0], p_[1], p_[3]);
  computeMB();
  shape_ = _quadrangle;
}

Quadrangle::Quadrangle(const Point& p1, const Point& p2, const Point& p3, const Point& p4, const std::vector<number_t>& n, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Polygon()
{
  n_ = n;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  p_.resize(4);
  p_[0] = p1;
  p_[1] = p2;
  p_[2] = p3;
  p_[3] = p4;
  boundingBox = BoundingBox(p1, p2, p3, p4);
  computeMB();
  shape_ = _quadrangle;
  initParametrization();
}

Quadrangle::Quadrangle(const Point& p1, const Point& p2, const Point& p3, const Point& p4, const std::vector<real_t>& h, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Polygon()
{
  h_ = h;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  p_.resize(4);
  p_[0] = p1;
  p_[1] = p2;
  p_[2] = p3;
  p_[3] = p4;
  boundingBox = BoundingBox(p1, p2, p3, p4);
  computeMB();
  shape_ = _quadrangle;
  initParametrization();
}

void Quadrangle::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Quadrangle::build");
  shape_ = _quadrangle;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // _vertices key is replaced by _v1, _v2, _v3 and _v4
  params.erase(_pk_vertices);

  p_.resize(4);
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _quadrangle)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or edge_names, not both
    if (key == _pk_edge_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_edge_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_edge_names)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _edge_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_edge_names); }
  if (usedParams.find(_pk_edge_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // (v1,v2,v3,v4) has to be set (no default values)
  if (params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v3) != params.end()) { error("param_missing", "v3"); }
  if (params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }

  // check if 4 vertices are coplanar
  if (!arePointsCoplanar(p_)) { error("vertices_not_coplanar", words("shape", _quadrangle)); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  // checking nnodes and hsteps size
  if (n_.size() != 0)
  {
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(4, n0); }
    else if (n_.size() != 4) {error("bad_size", "nnodes", 4, n_.size()); }
  }
  if (h_.size() != 0)
  {
    if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(4, h0); }
    else if (h_.size() != 4) {error("bad_size", "hsteps", 4, h_.size()); }
  }

  boundingBox = BoundingBox(p_[0], p_[1], p_[3]);
  computeMB();
  initParametrization();
  trace_p->pop();
}

void Quadrangle:: initParametrization()
{
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  parametrization_ = new Parametrization(SquareGeo(), parametrization_Quadrangle, pars, "Quadrangle parametrization");
  parametrization_->setinvParametrization(invParametrization_Quadrangle);

}

void Quadrangle::buildParam(const Parameter& p)
{
  trace_p->push("Quadrangle::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_v1:
    {
      switch (p.type())
      {
        case _pt: p_[0] = p.get_pt(); break;
        case _integer: p_[0] = Point(real_t(p.get_i())); break;
        case _real: p_[0] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v2:
    {
      switch (p.type())
      {
        case _pt: p_[1] = p.get_pt(); break;
        case _integer: p_[1] = Point(real_t(p.get_i())); break;
        case _real: p_[1] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v3:
    {
      switch (p.type())
      {
        case _pt: p_[2] = p.get_pt(); break;
        case _integer: p_[2] = Point(real_t(p.get_i())); break;
        case _real: p_[2] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v4:
    {
      switch (p.type())
      {
        case _pt: p_[3] = p.get_pt(); break;
        case _integer: p_[3] = Point(real_t(p.get_i())); break;
        case _real: p_[3] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Polygon::buildParam(p); break;
  }
  trace_p->pop();
}

void Quadrangle::buildDefaultParam(ParameterKey key)
{
  Polygon::buildDefaultParam(key);
}

std::set<ParameterKey> Quadrangle::getParamsKeys()
{
  std::set<ParameterKey> params = Polygon::getParamsKeys();
  params.insert(_pk_v1);
  params.insert(_pk_v2);
  params.insert(_pk_v3);
  params.insert(_pk_v4);
  return params;
}

Quadrangle::Quadrangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Polygon()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Quadrangle::Quadrangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Polygon()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Quadrangle::Quadrangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Polygon()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Quadrangle::Quadrangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Polygon()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Quadrangle::Quadrangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Polygon()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Quadrangle::Quadrangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Polygon()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

string_t Quadrangle::asString() const
{
  string_t s("Quadrangle (");
  s += p_[0].roundToZero().toString() + ", " + p_[1].roundToZero().toString() + ", " + p_[2].roundToZero().toString() + ", " + p_[3].roundToZero().toString() + ")";
  return s;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Quadrangle::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(1);
  surfs[0] = std::make_pair(_quadrangle, boundNodes());
  return surfs;
}

real_t Quadrangle::measure() const
{
  real_t h1, h2;
  Point P1 = projectionOnStraightLine(p_[1], p_[0], p_[2], h1);
  Point P2 = projectionOnStraightLine(p_[3], p_[0], p_[2], h2);
  return 0.5 * (p_[0].distance(p_[2])) * (h1 + h2);
}

//! parametrization (Q1) : p = p1+u*(p2-p1)+v*(p4-p1)-uv*(p2-p1+p4-p3), (u,v) in [0,1]x[0,1]
Vector<real_t> Quadrangle::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  number_t dim = p_[0].size();
  Vector<real_t> R(dim);
  switch (d)
  {
    case _id:
      for (number_t k = 0; k < dim; k++)
      { R[k] = p_[0][k] + pt[0] * (p_[1][k] - p_[0][k]) + pt[1] * (p_[3][k] - p_[0][k]) - pt[0] * pt[1] * (p_[1][k] - p_[0][k] + p_[3][k] - p_[2][k]); }
      break;
    case _d1:
      for (number_t k = 0; k < dim; k++)
      { R[k] = (p_[1][k] - p_[0][k]) - pt[1] * (p_[1][k] - p_[0][k] + p_[3][k] - p_[2][k]); }
      break;
    case _d2:
      for (number_t k = 0; k < dim; k++) { R[k] = (p_[3][k] - p_[0][k]) - pt[0] * (p_[1][k] - p_[0][k] + p_[3][k] - p_[2][k]); }
      break;
    case _d11:
    case _d22:
      for (number_t k = 0; k < dim; k++)  { R[k] = 0.; }
      break;
    case _d12:
    case _d21:
      for (number_t k = 0; k < dim; k++) { R[k] = -(p_[1][k] - p_[0][k] + p_[3][k] - p_[2][k]); }
      break;
    default: parfun_error("Quadrangle parametrization", d);
  }
  return R;
}
//! inverse of parametrization (u,v)=invf(p)
Vector<real_t> Quadrangle::invParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  //to be done
  parfun_error("Quadrangle::invParametrization", d);
  return Vector<real_t>();
}

//==========================================================
// Parallelogram class member functions
//===========================================================

//! default parallelogram is [0,1]^2 No parametrization
Parallelogram::Parallelogram() : Quadrangle()
{
  shape_ = _parallelogram;
  computeMB();
}

Parallelogram::Parallelogram(const Point& p1, const Point& p2, const Point& p4, const std::vector<number_t>& n, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Quadrangle()
{
  n_ = n;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  p_.resize(4);
  p_[0] = p1;
  p_[1] = p2;
  p_[2] = p2 + p4 - p1;
  p_[3] = p4;
  boundingBox = BoundingBox(p1, p2, p_[2], p4);
  computeMB();
  shape_ = _parallelogram;
  initParametrization();
}

Parallelogram::Parallelogram(const Point& p1, const Point& p2, const Point& p4, const std::vector<real_t>& h, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Quadrangle()
{
  h_ = h;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  p_[0] = p1;
  p_[1] = p2;
  p_[2] = p2 + p4 - p1;
  p_[3] = p4;
  boundingBox = BoundingBox(p1, p2, p_[2], p4);
  computeMB();
  shape_ = _parallelogram;
  initParametrization();
}

void Parallelogram::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Parallelogram::build");
  shape_ = _parallelogram;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // _vertices key is replaced by _v1, _v2, _v3 and _v4
  params.erase(_pk_vertices);
  params.erase(_pk_v3);

  p_.resize(4);
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _parallelogram)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or edge_names, not both
    if (key == _pk_edge_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_edge_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_edge_names)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _edge_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_edge_names); }
  if (usedParams.find(_pk_edge_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // (v1,v2,v4) has to be set (no default values)
  if (params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }

  Point q = p_[1] - p_[0];
  Point q2 = p_[3] - p_[0];
  real_t a = dot(q, q);
  real_t b = dot(q, q2);
  real_t c = dot(q2, q2);
  if (std::abs(a * c - b * b) < theTolerance) { error("geometry_incoherent_points", words("shape", _parallelogram)); }

  // p_ is not fully built
  p_[2] = p_[1] + p_[3] - p_[0];

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  // checking nnodes and hsteps size
  if (n_.size() != 0)
  {
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(4, n0); }
    else if (n_.size() == 2)
    {
      number_t n0 = n_[0], n1 = n_[1];
      n_.resize(4, n0);
      n_[1] = n1;
      n_[3] = n1;
    }
    else if (n_.size() != 4) {error("bad_size", "nnodes", 4, n_.size()); }
  }
  if (h_.size() != 0)
  {
    if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(4, h0); }
    else if (h_.size() != 4) {error("bad_size", "hsteps", 4, h_.size()); }
  }

  boundingBox = BoundingBox(p_[0], p_[1], p_[3]);
  computeMB();
  initParametrization();
  trace_p->pop();
}

void Parallelogram::initParametrization()
{
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  parametrization_ = new Parametrization(SquareGeo(), parametrization_Parallelogram, pars, "Parallelogram parametrization");
  parametrization_->setinvParametrization(invParametrization_Parallelogram);

}

void Parallelogram::buildParam(const Parameter& p)
{
  Quadrangle::buildParam(p);
}

void Parallelogram::buildDefaultParam(ParameterKey key)
{
  Quadrangle::buildDefaultParam(key);
}

std::set<ParameterKey> Parallelogram::getParamsKeys()
{
  std::set<ParameterKey> params = Quadrangle::getParamsKeys();
  return params;
}

Parallelogram::Parallelogram(Parameter p1, Parameter p2, Parameter p3) : Quadrangle()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Parallelogram::Parallelogram(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Quadrangle()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Parallelogram::Parallelogram(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Quadrangle()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Parallelogram::Parallelogram(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Quadrangle()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Parallelogram::Parallelogram(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Quadrangle()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Parallelogram::Parallelogram(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Quadrangle()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Parallelogram::Parallelogram(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Quadrangle()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

string_t Parallelogram::asString() const
{
  string_t s("Parallelogram (");
  s += p_[0].roundToZero().toString() + ", " + p_[1].roundToZero().toString() + ", " + p_[3].roundToZero().toString() + ")";
  return s;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Parallelogram::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(1);
  surfs[0] = std::make_pair(_parallelogram, boundNodes());
  return surfs;
}

//! length of first or third side
real_t Parallelogram::length1() const
{
  return p_[0].distance(p_[1]);
}

//! length of second or fourth side
real_t Parallelogram::length2() const
{
  return p_[1].distance(p_[2]);
}

//! surface of the parallelogram
real_t Parallelogram::measure() const
{
  real_t h;
  Point P = projectionOnStraightLine(p_[2], p_[0], p_[1], h);
  return (p_[0].distance(p_[1])) * h;
}

//! parametrization (P1) : p = p1+u*(p2-p1)+v*(p4-p1)
Vector<real_t> Parallelogram::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  number_t dim = p_[0].size();
  Point R; R.resize(dim);
  switch (d)
  {
    case _id:
      for (number_t k = 0; k < dim; k++) { R[k] = p_[0][k] + pt[0] * (p_[1][k] - p_[0][k]) + pt[1] * (p_[3][k] - p_[0][k]); }
      break;
    case _d1:
      for (number_t k = 0; k < dim; k++) { R[k] = (p_[1][k] - p_[0][k]); }
      break;
    case _d2:
      for (number_t k = 0; k < dim; k++) { R[k] = (p_[3][k] - p_[0][k]); }
      break;
    case _d11:
    case _d22:
    case _d12:
    case _d21:
      for (number_t k = 0; k < dim; k++) { R[k] = 0; }
      break;
    default: parfun_error("Quadrangle parametrization", d);
  }
  return R;
}

//! inverse of parametrization (u,v)=invf(p)
Vector<real_t> Parallelogram::invParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  if (d != _id) { parfun_error("Parallelogram::invParametrization", d); }
  Vector<real_t> uv(2, 0.);
  Point p1p2 = p_[1]; p1p2 -= p_[0];
  Point p1p4 = p_[3]; p1p4 -= p_[0];
  Point p1p = pt; p1p -= p_[0];
  real_t a = dot(p1p2, p1p2), b = dot(p1p2, p1p4), c = dot(p1p4, p1p4), e = b * b - a * c;
  real_t f = dot(p1p, p1p2), g = dot(p1p, p1p4);
  uv[0] = (b * g - c * f) / e; uv[1] = (b * f - a * g) / e;
  if (uv[0] < -theTolerance || uv[0] > 1 + theTolerance || uv[1] < -theTolerance || uv[1] > 1 + theTolerance)
  { error("free_error", "Parallelogram::invParametrization fails because point is not located in the parallelogram"); }
  uv[0] = std::min(std::max(0., uv[0]), 1.); //rounded
  uv[1] = std::min(std::max(0., uv[1]), 1.);
  return uv;
}

//==========================================================
// Rectangle class member functions
//===========================================================

//! default rectangle is [0,1]^2
Rectangle::Rectangle() : Parallelogram(), center_(Point(0.5, 0.5)), origin_(Point(0., 0.)), isCenter_(false), isOrigin_(false),
  xlength_(1.), ylength_(1.), xmin_(0.), xmax_(1.), ymin_(0.), ymax_(1.), isBounds_(false)
{
  shape_ = _rectangle;
  computeMB();
}

Rectangle::Rectangle(const Point& p1, const Point& p2, const Point& p4, const std::vector<number_t>& n, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Parallelogram()
{
  n_ = n;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  p_.resize(4);
  p_[0] = p1;
  p_[1] = p2;
  p_[2] = p2 + p4 - p1;
  p_[3] = p4;
  boundingBox = BoundingBox(p1, p2, p4);
  computeMB();
  shape_ = _rectangle;
  initParametrization();
}

Rectangle::Rectangle(const Point& p1, const Point& p2, const Point& p4, const std::vector<real_t>& h, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Parallelogram()
{
  h_ = h;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  p_[0] = p1;
  p_[1] = p2;
  p_[2] = p2 + p4 - p1;
  p_[3] = p4;
  boundingBox = BoundingBox(p1, p2, p4);
  computeMB();
  shape_ = _rectangle;
  initParametrization();
}

void Rectangle::buildP()
{
  if (isBounds_)
  {
    p_[0] = Point(xmin_, ymin_);
    p_[1] = Point(xmax_, ymin_);
    p_[2] = Point(xmax_, ymax_);
    p_[3] = Point(xmin_, ymax_);
    origin_ = p_[0];
    center_ = (p_[0] + p_[2]) / 2.;
    xlength_ = xmax_ - xmin_;
    ylength_ = ymax_ - ymin_;
  }
  else if (isCenter_)
  {
    Point shift0(std::vector<real_t>(center_.size()));
    shift0[0] -= xlength_ / 2.; shift0[1] -= ylength_ / 2.;
    p_[0] = center_ + shift0;
    Point shift1(std::vector<real_t>(center_.size()));
    shift1[0] += xlength_ / 2.; shift1[1] -= ylength_ / 2.;
    p_[1] = center_ + shift1;
    Point shift2(std::vector<real_t>(center_.size()));
    shift2[0] += xlength_ / 2.; shift2[1] += ylength_ / 2.;
    p_[2] = center_ + shift2;
    Point shift3(std::vector<real_t>(center_.size()));
    shift3[0] -= xlength_ / 2.; shift3[1] += ylength_ / 2.;
    p_[3] = center_ + shift3;
    origin_ = p_[0];
    xmin_ = xmax_ = ymin_ = ymax_ = 0.;
  }
  else if (isOrigin_)
  {
    p_[0] = origin_;
    Point shift1(std::vector<real_t>(origin_.size()));
    shift1[0] += xlength_;
    p_[1] = origin_ + shift1;
    Point shift2(std::vector<real_t>(origin_.size()));
    shift2[0] += xlength_; shift2[1] += ylength_;
    p_[2] = origin_ + shift2;
    Point shift3(std::vector<real_t>(origin_.size()));
    shift3[1] += ylength_;
    p_[3] = origin_ + shift3;
    center_ = (p_[0] + p_[2]) / 2.;
    xmin_ = xmax_ = ymin_ = ymax_ = 0.;
  }
  else // vertices keys are used so p_[2] is not defined
  {
    p_[2] = p_[1] + p_[3] - p_[0];
    origin_ = p_[0];
    center_ = (p_[0] + p_[2]) / 2.;
    xlength_ = p_[0].distance(p_[1]);
    ylength_ = p_[0].distance(p_[3]);
    xmin_ = xmax_ = ymin_ = ymax_ = 0.;
    if (dot(p_[3] - p_[0], p_[1] - p_[0]) > theTolerance) { error("geometry_incoherent_points", words("shape", _rectangle)); }
  }
}

void Rectangle::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Rectangle::build");
  shape_ = _rectangle;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // _vertices key is replaced by (_v1,_v2,_v4), (_center|_origin, _xlength, _ylength) or (_xmin, _xmax, _ymin, _ymax)
  params.erase(_pk_vertices);
  // _v3 is disabled
  params.erase(_pk_v3);

  p_.resize(4);
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _rectangle)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or edge_names, not both
    if (key == _pk_edge_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_edge_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_edge_names)); }
    // user must use only one of the following combination: (v1,v2,v4), (xmin,xmax,ymin,ymax),
    // (center,xlength,ylength), (origin,xlength,ylength)
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4) && usedParams.find(_pk_xmin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xmin)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4) && usedParams.find(_pk_xmax) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xmax)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4) && usedParams.find(_pk_ymin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ymin)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4) && usedParams.find(_pk_ymax) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ymax)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4) && usedParams.find(_pk_center) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4) && usedParams.find(_pk_origin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_origin)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4) && usedParams.find(_pk_xlength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xlength)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4) && usedParams.find(_pk_ylength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ylength)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax) && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax) && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax) && usedParams.find(_pk_v4) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v4)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax) && usedParams.find(_pk_center) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax) && usedParams.find(_pk_origin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_origin)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax) && usedParams.find(_pk_xlength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xlength)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax) && usedParams.find(_pk_ylength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ylength)); }
    if (key == _pk_center && usedParams.find(_pk_origin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_origin)); }
    if (key == _pk_origin && usedParams.find(_pk_center) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength) && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength) && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength) && usedParams.find(_pk_v4) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v4)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength) && usedParams.find(_pk_xmin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xmin)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength) && usedParams.find(_pk_xmax) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xmax)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength) && usedParams.find(_pk_ymin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ymin)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength) && usedParams.find(_pk_ymax) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ymax)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _edge_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_edge_names); }
  if (usedParams.find(_pk_edge_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // (v1,v2,v4) or (xmin,xmax,ymin,ymax) or (center,xlength,ylength) has to be set (no default values)
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v4) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v4) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_xmin) == params.end() && params.find(_pk_xmax) != params.end()) { error("param_missing", "xmax"); }
  if (params.find(_pk_xmin) == params.end() && params.find(_pk_ymin) != params.end()) { error("param_missing", "ymin"); }
  if (params.find(_pk_xmin) == params.end() && params.find(_pk_ymax) != params.end()) { error("param_missing", "ymax"); }
  if (params.find(_pk_xmax) == params.end() && params.find(_pk_xmin) != params.end()) { error("param_missing", "xmin"); }
  if (params.find(_pk_xmax) == params.end() && params.find(_pk_ymin) != params.end()) { error("param_missing", "ymin"); }
  if (params.find(_pk_xmax) == params.end() && params.find(_pk_ymax) != params.end()) { error("param_missing", "ymax"); }
  if (params.find(_pk_ymin) == params.end() && params.find(_pk_xmin) != params.end()) { error("param_missing", "xmin"); }
  if (params.find(_pk_ymin) == params.end() && params.find(_pk_xmax) != params.end()) { error("param_missing", "xmax"); }
  if (params.find(_pk_ymin) == params.end() && params.find(_pk_ymax) != params.end()) { error("param_missing", "ymax"); }
  if (params.find(_pk_ymax) == params.end() && params.find(_pk_xmin) != params.end()) { error("param_missing", "xmin"); }
  if (params.find(_pk_ymax) == params.end() && params.find(_pk_xmax) != params.end()) { error("param_missing", "xmax"); }
  if (params.find(_pk_ymax) == params.end() && params.find(_pk_ymin) != params.end()) { error("param_missing", "ymin"); }
  if ((params.find(_pk_center) == params.end() || params.find(_pk_origin) == params.end()) && params.find(_pk_xlength) != params.end())
  { error("param_missing", "xlength"); }
  if ((params.find(_pk_center) == params.end() || params.find(_pk_origin) == params.end()) && params.find(_pk_ylength) != params.end())
  { error("param_missing", "ylength"); }
  if (params.find(_pk_xlength) == params.end() && params.find(_pk_ylength) != params.end()) { error("param_missing", "ylength"); }
  if (params.find(_pk_ylength) == params.end() && params.find(_pk_xlength) != params.end()) { error("param_missing", "xlength"); }
  if (params.find(_pk_xlength) == params.end() && params.find(_pk_center) != params.end() && params.find(_pk_origin) != params.end())
  { error("param_missing", "center or origin"); }
  isCenter_ = true;
  isOrigin_ = true;
  isBounds_ = true;

  // now, we clean unwanted keys
  if (params.find(_pk_xlength) != params.end()) { params.erase(_pk_xlength); params.erase(_pk_ylength); }
  if (params.find(_pk_center) != params.end()) { params.erase(_pk_center); isCenter_ = false; }
  if (params.find(_pk_origin) != params.end()) { params.erase(_pk_origin); isOrigin_ = false; }
  if (params.find(_pk_xmin) != params.end())
  { params.erase(_pk_xmin); params.erase(_pk_xmax); params.erase(_pk_ymin); params.erase(_pk_ymax); isBounds_ = false; }
  if (params.find(_pk_v1) != params.end()) { params.erase(_pk_v1); params.erase(_pk_v2); params.erase(_pk_v4); }
  // p_ is not (fully) built so we do
  buildP();

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  // checking nnodes and hsteps size
  if (n_.size() != 0)
  {
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(4, n0); }
    else if (n_.size() == 2)
    {
      number_t n0 = n_[0], n1 = n_[1];
      n_.resize(4, n0);
      n_[1] = n1;
      n_[3] = n1;
    }
    else if (n_.size() != 4) {error("bad_size", "nnodes", 4, n_.size()); }
  }
  if (h_.size() != 0)
  {
    if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(4, h0); }
    else if (h_.size() != 4) {error("bad_size", "hsteps", 4, h_.size()); }
  }

  boundingBox = BoundingBox(p_[0], p_[1], p_[3]);
  computeMB();
  initParametrization();
  trace_p->pop();
}

void Rectangle::buildParam(const Parameter& p)
{
  trace_p->push("Rectangle::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_center:
    {
      switch (p.type())
      {
        case _pt: center_ = p.get_pt(); break;
        case _integer: center_ = Point(real_t(p.get_i())); break;
        case _real: center_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_origin:
    {
      switch (p.type())
      {
        case _pt: origin_ = p.get_pt(); break;
        case _integer: origin_ = Point(real_t(p.get_i())); break;
        case _real: origin_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_xlength:
    {
      switch (p.type())
      {
        case _integer: xlength_ = real_t(p.get_n()); break;
        case _real: xlength_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_ylength:
    {
      switch (p.type())
      {
        case _integer: ylength_ = real_t(p.get_n()); break;
        case _real: ylength_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_xmin:
    {
      switch (p.type())
      {
        case _integer: xmin_ = real_t(p.get_i()); break;
        case _real: xmin_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_xmax:
    {
      switch (p.type())
      {
        case _integer: xmax_ = real_t(p.get_i()); break;
        case _real: xmax_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_ymin:
    {
      switch (p.type())
      {
        case _integer: ymin_ = real_t(p.get_i()); break;
        case _real: ymin_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_ymax:
    {
      switch (p.type())
      {
        case _integer: ymax_ = real_t(p.get_i()); break;
        case _real: ymax_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Parallelogram::buildParam(p); break;
  }
  trace_p->pop();
}

void Rectangle::buildDefaultParam(ParameterKey key)
{
  Parallelogram::buildDefaultParam(key);
}

std::set<ParameterKey> Rectangle::getParamsKeys()
{
  std::set<ParameterKey> params = Parallelogram::getParamsKeys();
  params.insert(_pk_center);
  params.insert(_pk_origin);
  params.insert(_pk_xlength);
  params.insert(_pk_ylength);
  params.insert(_pk_xmin);
  params.insert(_pk_xmax);
  params.insert(_pk_ymin);
  params.insert(_pk_ymax);
  return params;
}

Rectangle::Rectangle(Parameter p1, Parameter p2, Parameter p3) : Parallelogram()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Rectangle::Rectangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Parallelogram()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Rectangle::Rectangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Parallelogram()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Rectangle::Rectangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Parallelogram()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Rectangle::Rectangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Parallelogram()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Rectangle::Rectangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Parallelogram()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Rectangle::Rectangle(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Parallelogram()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

string_t Rectangle::asString() const
{
  string_t s("Rectangle (");
  s += p_[0].roundToZero().toString() + ", " + p_[1].roundToZero().toString() + ", " + p_[3].roundToZero().toString() + ")";
  return s;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Rectangle::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(1);
  surfs[0] = std::make_pair(_rectangle, boundNodes());
  return surfs;
}

//==========================================================
// SquareGeo class member functions
//===========================================================
//! default square is [0,1]^2  No parametrization
SquareGeo::SquareGeo() : Rectangle()
{ shape_ = _square; }

SquareGeo::SquareGeo(const Point& p1, const Point& p2, const Point& p4, const std::vector<number_t>& n, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Rectangle()
{
  if (dot(p2 - p1, p4 - p1) > theTolerance) { error("geometry_incoherent_points", words("shape", _square)); }
  n_ = n;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  p_.resize(4);
  p_[0] = p1;
  p_[1] = p2;
  p_[2] = p2 + p4 - p1;
  p_[3] = p4;
  boundingBox = BoundingBox(p_[0], p_[1], p_[3]);
  computeMB();
  shape_ = _square;
  initParametrization();
}

SquareGeo::SquareGeo(const Point& p1, const Point& p2, const Point& p4, const std::vector<real_t>& h, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Rectangle()
{
  if (dot(p2 - p1, p4 - p1) > theTolerance) { error("geometry_incoherent_points", words("shape", _square)); }
  h_ = h;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  p_.resize(4);
  p_[0] = p1;
  p_[1] = p2;
  p_[2] = p2 + p4 - p1;
  p_[3] = p4;
  boundingBox = BoundingBox(p_[0], p_[1], p_[3]);
  computeMB();
  shape_ = _square;
  initParametrization();
}

void SquareGeo::buildP()
{
  if (isCenter_)
  {
    Point shift0(std::vector<real_t>(center_.size()));
    shift0[0] -= xlength_ / 2.; shift0[1] -= ylength_ / 2.;
    p_[0] = center_ + shift0;
    Point shift1(std::vector<real_t>(center_.size()));
    shift1[0] += xlength_ / 2.; shift1[1] -= ylength_ / 2.;
    p_[1] = center_ + shift1;
    Point shift2(std::vector<real_t>(center_.size()));
    shift2[0] += xlength_ / 2.; shift2[1] += ylength_ / 2.;
    p_[2] = center_ + shift2;
    Point shift3(std::vector<real_t>(center_.size()));
    shift3[0] -= xlength_ / 2.; shift3[1] += ylength_ / 2.;
    p_[3] = center_ + shift3;
    origin_ = p_[0];
  }
  else if (isOrigin_)
  {
    p_[0] = origin_;
    Point shift1(std::vector<real_t>(origin_.size()));
    shift1[0] += xlength_;
    p_[1] = origin_ + shift1;
    Point shift2(std::vector<real_t>(origin_.size()));
    shift2[0] += xlength_; shift2[1] += ylength_;
    p_[2] = origin_ + shift2;
    Point shift3(std::vector<real_t>(origin_.size()));
    shift3[1] += ylength_;
    p_[3] = origin_ + shift3;
    center_ = (p_[0] + p_[2]) / 2.;
  }
  else // vertices keys are used so p_[2] is not defined
  {
    p_[2] = p_[1] + p_[3] - p_[0];
    origin_ = p_[0];
    center_ = (p_[0] + p_[2]) / 2.;
    xlength_ = p_[0].distance(p_[1]);
    ylength_ = p_[0].distance(p_[3]);
    if (dot(p_[3] - p_[0], p_[1] - p_[0]) > theTolerance) { error("geometry_incoherent_points", words("shape", _square)); }
  }
}

void SquareGeo::build(const std::vector<Parameter>& ps)
{
  trace_p->push("SquareGeo::build");
  shape_ = _square;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // _vertices key is replaced by (_v1,_v2,_v4) or (_center|_origin, _length)
  params.erase(_pk_vertices);
  // _v3, _xlength, _ylength, _xmin, _xmax, _ymin and _ymax are disabled
  params.erase(_pk_v3);
  params.erase(_pk_xlength);
  params.erase(_pk_ylength);
  params.erase(_pk_xmin);
  params.erase(_pk_xmax);
  params.erase(_pk_ymin);
  params.erase(_pk_ymax);

  p_.resize(4);
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _square)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or edge_names, not both
    if (key == _pk_edge_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_edge_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_edge_names)); }
    // user must use only one of the following combination: (v1,v2,v4), (center,length) or (origin,length)
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4) && usedParams.find(_pk_center) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4) && usedParams.find(_pk_origin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_origin)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4) && usedParams.find(_pk_length) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_length)); }
    if (key == _pk_center && usedParams.find(_pk_origin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_origin)); }
    if (key == _pk_origin && usedParams.find(_pk_center) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_length) && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_length) && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_length) && usedParams.find(_pk_v4) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v4)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _edge_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_edge_names); }
  if (usedParams.find(_pk_edge_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // (v1,v2,v4) or (xmin,xmax,ymin,ymax) or (center,xlength,ylength) has to be set (no default values)
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v4) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v4) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if ((params.find(_pk_center) == params.end() || params.find(_pk_origin) == params.end()) && params.find(_pk_length) != params.end())
  { error("param_missing", "length"); }
  if (params.find(_pk_length) == params.end() && params.find(_pk_center) != params.end() && params.find(_pk_origin) != params.end())
  { error("param_missing", "center or origin"); }
  isCenter_ = true;
  isOrigin_ = true;
  // now, we clean unwanted keys
  if (params.find(_pk_length) != params.end()) { params.erase(_pk_length); }
  if (params.find(_pk_center) != params.end()) { params.erase(_pk_center); isCenter_ = false; }
  if (params.find(_pk_origin) != params.end()) { params.erase(_pk_origin); isOrigin_ = false; }
  if (params.find(_pk_v1) != params.end()) { params.erase(_pk_v1); params.erase(_pk_v2); params.erase(_pk_v4); }

  // p_ is not (fully) built so we do
  buildP();
  if (std::abs(p_[0].distance(p_[1]) - p_[0].distance(p_[3])) > theTolerance)
  { error("geometry_incoherent_points", words("shape", _square)); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  // checking nnodes and hsteps size
  if (n_.size() != 0)
  {
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(4, n0); }
    else if (n_.size() == 2)
    {
      number_t n0 = n_[0], n1 = n_[1];
      n_.resize(4, n0);
      n_[1] = n1;
      n_[3] = n1;
    }
    else if (n_.size() != 4) {error("bad_size", "nnodes", 4, n_.size()); }
  }
  if (h_.size() != 0)
  {
    if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(4, h0); }
    else if (h_.size() != 4) {error("bad_size", "hsteps", 4, h_.size()); }
  }

  boundingBox = BoundingBox(p_[0], p_[1], p_[3]);
  computeMB();
  initParametrization();
  trace_p->pop();
}

void SquareGeo::buildParam(const Parameter& p)
{
  trace_p->push("SquareGeo::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_length:
    {
      switch (p.type())
      {
        case _integer: xlength_ = ylength_ = real_t(p.get_n()); break;
        case _real: xlength_ = ylength_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Rectangle::buildParam(p); break;
  }
  trace_p->pop();
}

void SquareGeo::buildDefaultParam(ParameterKey key)
{
  Rectangle::buildDefaultParam(key);
}

std::set<ParameterKey> SquareGeo::getParamsKeys()
{
  std::set<ParameterKey> params = Rectangle::getParamsKeys();
  params.insert(_pk_length);
  return params;
}

SquareGeo::SquareGeo(Parameter p1, Parameter p2) : Rectangle()
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

SquareGeo::SquareGeo(Parameter p1, Parameter p2, Parameter p3) : Rectangle()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

SquareGeo::SquareGeo(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Rectangle()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

SquareGeo::SquareGeo(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Rectangle()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

SquareGeo::SquareGeo(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Rectangle()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

SquareGeo::SquareGeo(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Rectangle()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

SquareGeo::SquareGeo(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Rectangle()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

SquareGeo::SquareGeo(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Rectangle()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

string_t SquareGeo::asString() const
{
  string_t s("SquareGeo (");
  s += p_[0].roundToZero().toString() + ", " + p_[1].roundToZero().toString() + ", " + p_[3].roundToZero().toString() + ")";
  return s;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > SquareGeo::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(1);
  surfs[0] = std::make_pair(_square, boundNodes());
  return surfs;
}

//==========================================================
// Ellipse class member functions
//===========================================================
//! default ellipse is circle of center (0,0,0) and radius 1, no parametrization
Ellipse::Ellipse()
  : Surface(), xradius_(1.), yradius_(1.), thetamin_(0.), thetamax_(360.), isSector_(false), type_(1)
{
  p_.resize(5);
  p_[0] = Point(0., 0.);
  p_[1] = Point(1., 0.);
  p_[2] = Point(0., 1.);
  p_[3] = Point(-1., 0.);
  p_[4] = Point(0., -1.);
  v_ = p_;
  n_.resize(4, 2);
  shape_ = _ellipse;
  boundingBox = BoundingBox(p_[0] + p_[2] - p_[3], p_[0] + p_[2] - p_[1], p_[0] + p_[1] - p_[2]);
  computeBB();
  computeMB();
  initParametrization();
}

Ellipse::Ellipse(const Point& center, const Point& p1, const Point& p2, const std::vector<number_t>& n, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Surface(), xradius_(center.distance(p1)), yradius_(center.distance(p2)), thetamin_(0.), thetamax_(360.), isSector_(false), type_(1)
{
  n_ = n;
  p_.resize(5);
  p_[0] = center;
  p_[1] = p1;
  p_[2] = p2;
  p_[3] = 2.*center - p1;
  p_[4] = 2.*center - p2;
  v_ = p_;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  shape_ = _ellipse;
  boundingBox = BoundingBox(p_[0] + p_[2] - p_[3], p_[0] + p_[2] - p_[1], p_[0] + p_[1] - p_[2]);
  computeBB();
  computeMB();
  initParametrization();
}

Ellipse::Ellipse(const Point& center, const Point& p1, const Point& p2, const std::vector<real_t>& h, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Surface(), xradius_(center.distance(p1)), yradius_(center.distance(p2)), thetamin_(0.), thetamax_(360.), isSector_(false), type_(1)
{
  h_ = h;
  p_.resize(5);
  p_[0] = center;
  p_[1] = p1;
  p_[2] = p2;
  p_[3] = 2.*center - p1;
  p_[4] = 2.*center - p2;
  v_ = p_;
  domName_ = domName;
  sideNames_ = sideNames;
  sideOfSideNames_ = sideOfSideNames;
  shape_ = _ellipse;
  boundingBox = BoundingBox(p_[0] - p_[2] - p_[3], p_[0] + p_[2] - p_[1], p_[0] + p_[1] - p_[2]);
  computeBB();
  computeMB();
  initParametrization();
}

void Ellipse::buildPVNAndH()
{
  if (isAxis_)
  {
    Point shift1(std::vector<real_t>(p_[0].size()));
    shift1[0] += xradius_;
    p_[1] = p_[0] + shift1;
    Point shift2(std::vector<real_t>(p_[0].size()));
    shift2[1] += yradius_;
    p_[2] = p_[0] + shift2;
    Point shift3(std::vector<real_t>(p_[0].size()));
    shift3[0] -= xradius_;
    p_[3] = p_[0] + shift3;
    Point shift4(std::vector<real_t>(p_[0].size()));
    shift4[1] -= yradius_;
    p_[4] = p_[0] + shift4;
  }
  else // vertices keys are used so p3_ and p4_ are not defined
  {
    p_[3] = 2.*p_[0] - p_[1];
    p_[4] = 2.*p_[0] - p_[2];
    xradius_ = p_[0].distance(p_[1]);
    yradius_ = p_[0].distance(p_[2]);
    if (dot(p_[2] - p_[0], p_[1] - p_[0]) > theTolerance) { error("geometry_incoherent_points", words("shape", _ellipse)); }
  }

  // if thetamax_ is smaller than thetamin_, we add 2pi
  real_t dpi = 2 * pi_;
  while (thetamax_ < thetamin_) { thetamax_ += dpi; }
  real_t thetarange = thetamax_ - thetamin_;
  if (thetarange > dpi + theEpsilon) { error("sector_bad_angles_range", dpi, thetarange); }

  // fill v
  if (std::abs(thetarange - dpi) < theTolerance) // this is the full disk
  {
    isSector_ = false;
    v_ = p_;
  }
  else
  {
    // this is a sector
    // A point M on the ellipse (C, P1, P2) is of the form M-C=cos(t)*(P1-C)+sin(t)*(P2-C) with t in [0, 2.*pi_[
    // For us, it is just in degrees !!!
    isSector_ = true;
    if (thetarange - pi_ > -theEpsilon)
    {
      v_.resize(4);
      v_[0] = p_[0];
      // 2 arcs will be needed to define the sector
      // we will take the intersection of the bissectrix of (CP1,CP2) and the ellipse
      real_t angle = thetamin_;
      v_[1] = p_[0] + std::cos(angle) * (p_[1] - p_[0]) + std::sin(angle) * (p_[2] - p_[0]);
      angle = (thetamin_ + thetamax_) / 2;
      v_[2] = p_[0] + std::cos(angle) * (p_[1] - p_[0]) + std::sin(angle) * (p_[2] - p_[0]);
      angle = thetamax_;
      v_[3] = p_[0] + std::cos(angle) * (p_[1] - p_[0]) + std::sin(angle) * (p_[2] - p_[0]);
    }
    else
    {
      v_.resize(3);
      v_[0] = p_[0];
      // only one arc will be needed to define the sector
      real_t angle = thetamin_;
      v_[1] = p_[0] + std::cos(angle) * (p_[1] - p_[0]) + std::sin(angle) * (p_[2] - p_[0]);
      angle = thetamax_;
      v_[2] = p_[0] + std::cos(angle) * (p_[1] - p_[0]) + std::sin(angle) * (p_[2] - p_[0]);
    }
  }

  number_t nsize = v_.size();
  if (!isSector_) { nsize = 4; }
  if (n_.size() > 0)
  {
    if (n_.size() == 1)
    {
      number_t n0 = n_[0];
      n_.clear();
      n_.resize(nsize, n0);
    }
    else if (n_.size() == 3 and nsize == 4 and isSector_) { n_ = {n_[0], n_[1], n_[1], n_[2]}; }
    else if (n_.size() != nsize) { error("bad_size", "nnodes", nsize, n_.size()); }
  }

  number_t hsize = v_.size();
  if (!isSector_) { hsize = 4; }
  if (h_.size() > 0)
  {
    if (h_.size() == 1)
    {
      real_t h0 = h_[0];
      h_.clear();
      h_.resize(hsize, h0);
    }
    else if (h_.size() == 3 and hsize == 4 and isSector_) { h_ = {h_[0], h_[1], h_[1], h_[2]}; }
    else if (h_.size() != hsize) { error("bad_size", "hsteps", hsize, h_.size()); }
  }
}

void Ellipse::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Ellipse::build");
  shape_ = _ellipse;
  p_.resize(5);
  std::set<ParameterKey> params = getParamsKeys(), usedParams;

  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) { error("geom_unexpected_param_key", words("param key", key), words("shape", _ellipse)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or edge_names, not both
    if (key == _pk_edge_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_edge_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_edge_names)); }
    // user must use only one of the following combination: (center,v1,v2), (center,xlength,ylength) or (center,xradius,yradius)
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_xradius || key == _pk_yradius) && usedParams.find(_pk_xlength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xlength)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_xradius || key == _pk_yradius) && usedParams.find(_pk_ylength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ylength)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_xlength || key == _pk_ylength) && usedParams.find(_pk_xradius) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xradius)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_xlength || key == _pk_ylength) && usedParams.find(_pk_yradius) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_yradius)); }
    if ((key == _pk_xlength || key == _pk_ylength || key == _pk_xradius || key == _pk_yradius) && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_xlength || key == _pk_ylength || key == _pk_xradius || key == _pk_yradius) && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _edge_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_edge_names); }
  if (usedParams.find(_pk_edge_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // (center,v1,v2) or (center,xlength,ylength) or (center, xradius,yradius) has to be set (no default values)
  if (params.find(_pk_center) != params.end()) { error("param_missing", "center"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_xlength) == params.end() && params.find(_pk_ylength) != params.end()) { error("param_missing", "ylength"); }
  if (params.find(_pk_ylength) == params.end() && params.find(_pk_xlength) != params.end()) { error("param_missing", "xlength"); }
  if (params.find(_pk_xradius) == params.end() && params.find(_pk_yradius) != params.end()) { error("param_missing", "yradius"); }
  if (params.find(_pk_yradius) == params.end() && params.find(_pk_xradius) != params.end()) { error("param_missing", "xradius"); }
  // angle1 and angle2 have to be set both
  if (params.find(_pk_angle1) == params.end() && params.find(_pk_angle2) != params.end()) { error("param_missing", "angle2"); }
  if (params.find(_pk_angle2) == params.end() && params.find(_pk_angle1) != params.end()) { error("param_missing", "angle1"); }

  isAxis_ = true;
  // now, we clean unwanted keys
  if (params.find(_pk_xlength) != params.end()) { params.erase(_pk_xlength); params.erase(_pk_ylength); isAxis_ = false; }
  if (params.find(_pk_xradius) != params.end()) { params.erase(_pk_xradius); params.erase(_pk_yradius); isAxis_ = false; }
  if (params.find(_pk_v1) != params.end()) { params.erase(_pk_v1); params.erase(_pk_v2); isAxis_ = true; }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  // p_ is not (fully) built so we do
  buildPVNAndH();

  computeBB();
  computeMB();
  initParametrization();
  trace_p->pop();
}

void Ellipse::initParametrization()
{
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  parametrization_ = new Parametrization(SquareGeo(_origin = Point(0., 0.), _length = 1.),
                                         parametrization_Ellipse, pars, "Ellipse parametrization");
  parametrization_->setinvParametrization(invParametrization_Ellipse);
  parametrization_->geomP() = this;
  parametrization_->singularSide.push_back(std::make_pair(1, 0.)); //declare u=0 as singular side
  if (!isSector_) { parametrization_->periods[1] = 1.; }
}

void Ellipse::buildParam(const Parameter& p)
{
  trace_p->push("Ellipse::buildParam");
  bool warndeg = true;
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_center:
    {
      switch (p.type())
      {
        case _pt: p_[0] = p.get_pt(); break;
        case _integer: p_[0] = Point(real_t(p.get_i())); break;
        case _real: p_[0] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v1:
    {
      switch (p.type())
      {
        case _pt: p_[1] = p.get_pt(); break;
        case _integer: p_[1] = Point(real_t(p.get_i())); break;
        case _real: p_[1] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v2:
    {
      switch (p.type())
      {
        case _pt: p_[2] = p.get_pt(); break;
        case _integer: p_[2] = Point(real_t(p.get_i())); break;
        case _real: p_[2] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_xlength:
    {
      switch (p.type())
      {
        case _integer: xradius_ = 0.5 * real_t(p.get_n()); break;
        case _real: xradius_ = 0.5 * p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_ylength:
    {
      switch (p.type())
      {
        case _integer: yradius_ = 0.5 * real_t(p.get_n()); break;
        case _real: yradius_ = 0.5 * p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_xradius:
    {
      switch (p.type())
      {
        case _integer: xradius_ = real_t(p.get_n()); break;
        case _real: xradius_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_yradius:
    {
      switch (p.type())
      {
        case _integer: yradius_ = real_t(p.get_n()); break;
        case _real: yradius_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_angle1:
    {
      switch (p.type())
      {
        case _integer: thetamin_ = real_t(p.get_i()); break;
        case _real: thetamin_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      if (std::abs(thetamin_) > 2 * pi_)
      {
        warning("free_warning", "Now, angles in Ellipse constructor must be given in radian.\nHowever you can always specified angle in degree by using the syntax _angle1=45*deg_");
        warndeg = false;
      }
      break;
    }
    case _pk_angle2:
    {
      switch (p.type())
      {
        case _integer: thetamax_ = real_t(p.get_i()); break;
        case _real: thetamax_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      if (warndeg && std::abs(thetamax_) > 2 * pi_)
      { warning("free_warning", "Now, angles in Ellipse constructor must be given in radian.\nHowever you can always specified angle in degree by using the syntax _angle1=45*deg_"); }
      break;
    }
    case _pk_type:
    {
      switch (p.type())
      {
        case _integer: type_ = dimen_t(p.get_n()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nnodes:
    {
      switch (p.type())
      {
        case _integerVector:
        {
          std::vector<number_t> nv = p.get_nv();
          n_.clear();
          n_.resize(nv.size());
          for (number_t i = 0; i < nv.size(); ++i) { n_[i] = nv[i] > 2 ? nv[i] : 2; }
          break;
        }
        case _integer:
        {
          number_t n = p.get_n();
          n_.clear();
          n_.resize(1, (n > 2 ? n : 2));
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_hsteps:
    {
      switch (p.type())
      {
        case _realVector: h_ = p.get_rv(); break;
        case _integer: h_ = std::vector<real_t>(1, real_t(p.get_i())); break;
        case _real: h_ = std::vector<real_t>(1, p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Surface::buildParam(p); break;
  }
  trace_p->pop();
}

void Ellipse::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Ellipse::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_.resize(1, 2); break;
    case _pk_angle1: thetamin_ = 0.; break;
    case _pk_angle2: thetamax_ = 2 * pi_; break;
    case _pk_type: type_ = 1; break;
    default: Surface::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> Ellipse::getParamsKeys()
{
  std::set<ParameterKey> params = Surface::getParamsKeys();
  params.insert(_pk_center);
  params.insert(_pk_v1);
  params.insert(_pk_v2);
  params.insert(_pk_xlength);
  params.insert(_pk_ylength);
  params.insert(_pk_xradius);
  params.insert(_pk_yradius);
  params.insert(_pk_angle1);
  params.insert(_pk_angle2);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  params.insert(_pk_type);
  return params;
}

Ellipse::Ellipse(Parameter p1, Parameter p2, Parameter p3) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Ellipse::Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Ellipse::Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Ellipse::Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Ellipse::Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Ellipse::Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Ellipse::Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Ellipse::Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Ellipse::Ellipse(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  build(ps);
}

number_t Ellipse::nbSubdiv() const
{
  number_t n = n_[0];
  for (number_t i = 1; i < n_.size(); ++i)
  {
    if (n_[i] > n) { n = n_[i]; }
  }
  return static_cast<number_t>(theTolerance + std::log(n - 1) / std::log(2));
}

string_t Ellipse::asString() const
{
  string_t s("Ellipse (center ");
  s += p_[0].roundToZero().toString() + ", 1st apogee " + p_[1].roundToZero().toString() + ", 2nd apogee " + p_[2].roundToZero().toString();
  if (isSector_) { s += " sector " + tostring(thetamin_) + " -> " + tostring(thetamax_); }
  s += ")";
  return s;
}

//! computes the bounding box
void Ellipse::computeBB()
{
  if (!isSector_)
  {
    // full ellipse
    boundingBox = BoundingBox(p_[0] + p_[3] - p_[2], p_[0] + p_[1] - p_[2], p_[0] + p_[2] - p_[1]);
  }
  else
  {
    // elliptic sector
    Point i = p_[0] + std::cos((thetamin_ + thetamax_) / 2) * (p_[1] - p_[0]) + std::sin((thetamin_ + thetamax_) / 2) * (p_[2] - p_[0]);
    // intersection of the bissectrix of (CP1,CP2) and (P1P2)
    bool intExists;
    number_t vsize = v_.size();
    number_t vid = vsize - 1;
    Point j = intersectionOfStraightLines(v_[0], i, v_[1], v_[vid], intExists);
    // compute the minimal box
    Point k = v_[1] + i - j;
    Point l = v_[vid] + i - j;
    Point m = v_[1] + j - v_[0];
    Point n = v_[vid] + j - v_[0];
    real_t h;
    Point hk = projectionOnStraightLine(k, m, n, h);
    Point hl = projectionOnStraightLine(l, m, n, h);
    Point v1 = n, v4 = l;
    if (dot(hl - n, m - n) < 0.) { v1 = hl; }
    else { v4 = n + l - hl; }
    Point v2 = m;
    if (dot(hk - m, n - m) > 0.) { v2 = hk; }
    boundingBox = BoundingBox(v1, v2, v4);
  }
}

//! computes the minimal box
void Ellipse::computeMB()
{
  if (!isSector_)
  {
    // size should be 5 here
    minimalBox = MinimalBox(p_[0] + p_[3] - p_[2], p_[0] + p_[1] - p_[2], p_[0] + p_[2] - p_[1]);
  }
  else
  {
    // elliptic sector
    // intersection of the bissectrix of (CP1,CP2) and the arc
    Point i = p_[0] + std::cos((thetamin_ + thetamax_) / 2) * (p_[1] - p_[0]) + std::sin((thetamin_ + thetamax_) / 2) * (p_[2] - p_[0]);
    // intersection of the bissectrix of (CP1,CP2) and (P1P2)
    bool intExists;
    number_t vsize = v_.size();
    number_t vid = vsize - 1;
    Point j = intersectionOfStraightLines(v_[0], i, v_[1], v_[vid], intExists);
    // compute the minimal box
    Point k = v_[1] + i - j;
    Point l = v_[vid] + i - j;
    Point m = v_[1] + j - v_[0];
    Point n = v_[vid] + j - v_[0];
    real_t h;
    Point hk = projectionOnStraightLine(k, m, n, h);
    Point hl = projectionOnStraightLine(l, m, n, h);
    Point v1 = n, v4 = l;
    if (dot(hl - n, m - n) < 0.) { v1 = hl; }
    else { v4 = n + l - hl; }
    Point v2 = m;
    if (dot(hk - m, n - m) > 0.) { v2 = hk; }
    minimalBox = MinimalBox(v1, v2, v4);
  }
}

//! collect in a list all canonical geometry's with name n (may be the ellipse itself and some ellipse sides: EllArc)
void Ellipse::collect(const string_t& n, std::list<Geometry*>& geoms) const
{
  if (domName_ == n)
  {
    geoms.push_back(const_cast<Geometry*>(static_cast<const Geometry*>(this)));
    return;
  }
  //loop on sides
  number_t ns = sideNames_.size();
  if (ns == 0) { return; }
  std::vector<string_t> snames = sideNames_;
  if (ns == 1)  {
    if (!isSector()) { snames.resize(4, sideNames_[0]); }
    else { snames.resize(v_.size(), sideNames_[0]); }
  }
  else if (ns == 3 and isSector() and v_.size() == 4) { snames = {sideNames_[0], sideNames_[1], sideNames_[1], sideNames_[2]}; }
  ns = snames.size();
  std::vector<string_t>::const_iterator its = snames.begin();
  if (!isSector())
  {
    std::vector<Point> p(4);
    p[0] = p_[1]; p[1] = p_[2]; p[2] = p_[3]; p[3] = p_[4];
    for (number_t s = 0; s < ns; ++its, ++s)
    {
      if (*its == n) //create geometry related to side
      {
        number_t sp1 = s + 1;
        if (sp1 == ns) { sp1 = 0; }
        geoms.push_back(new EllArc(_center = p_[0], _v1 = p[s], _v2 = p[sp1], _domain_name = n));
      }
    }
  }
  else // ellipse sector
  {
    for (number_t s = 0; s < ns; ++its, ++s)
    {
      if (*its == n) //create geometry related to side
      {
        if (s == 0) { geoms.push_back(new Segment(_v1 = v_[0], _v2 = v_[1], _domain_name = n)); }
        if (s == 1) { geoms.push_back(new EllArc(_center = p_[0], _v1 = v_[1], _v2 = v_[2], _domain_name = n)); }
        if (s == 2)
        {
          if (v_.size() == 4) { geoms.push_back(new EllArc(_center = p_[0], _v1 = v_[2], _v2 = v_[3], _domain_name = n)); }
          else { geoms.push_back(new Segment(_v1 = v_[0], _v2 = v_[2], _domain_name = n)); }
        }
        if (s == 3) { geoms.push_back(new Segment(_v1 = v_[0], _v2 = v_[3], _domain_name = n)); }
      }
    }
  }
}

std::vector<int_t> Ellipse::nnodesPerBorder()
{
  if (h_.size() != 0) { return std::vector<int_t>(h_.size(), -1); }
  else
  {
    std::vector<int_t> n(n_.size());
    for (number_t i = 0; i < n.size(); ++i) { n[i] = n_[i]; }
    return n;
  }
}

std::vector<const Point*> Ellipse::boundNodes() const
{
  if (!isSector_)
  {
    std::vector<const Point*> nodes(4);
    nodes[0] = &p_[1]; nodes[1] = &p_[2]; nodes[2] = &p_[3]; nodes[3] = &p_[4];
    return nodes;
  }
  else
  {
    std::vector<const Point*> nodes(v_.size());
    for (number_t i = 0; i < v_.size(); ++i) { nodes[i] = &v_[i]; }
    return nodes;
  }
}

std::vector<Point*> Ellipse::nodes()
{
  std::vector<Point*> nodes(v_.size());
  for (number_t i = 0; i < v_.size(); ++i) { nodes[i] = &v_[i]; }
  return nodes;
}

std::vector<Point*> Ellipse::wholeNodes()
{
  std::vector<Point*> nodes(v_.size() + p_.size());
  for (number_t i = 0; i < v_.size(); ++i) { nodes[i] = &v_[i]; }
  for (number_t i = v_.size(); i < nodes.size(); ++i) { nodes[i] = &p_[i - v_.size()]; }
  return nodes;
}

std::vector<const Point*> Ellipse::nodes() const
{
  std::vector<const Point*> nodes(v_.size());
  for (number_t i = 0; i < v_.size(); ++i) { nodes[i] = &v_[i]; }
  return nodes;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Ellipse::curves() const
{
  if (!isSector_)
  {
    std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves(4);
    std::vector<const Point*> vertices(2);
    vertices[0] = &p_[1]; vertices[1] = &p_[2];
    curves[0] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3];
    curves[1] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4];
    curves[2] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[1];
    curves[3] = std::make_pair(_ellArc, vertices);
    return curves;
  }
  else
  {
    std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves(v_.size());
    std::vector<const Point*> vertices(2);
    vertices[0] = &v_[0]; vertices[1] = &v_[1];
    curves[0] = std::make_pair(_segment, vertices);
    vertices[0] = &v_[1]; vertices[1] = &v_[2];
    curves[1] = std::make_pair(_ellArc, vertices);
    if (v_.size() == 4)
    {
      vertices[0] = &v_[2]; vertices[1] = &v_[3];
      curves[2] = std::make_pair(_ellArc, vertices);
      vertices[0] = &v_[3]; vertices[1] = &v_[0];
      curves[3] = std::make_pair(_segment, vertices);
    }
    else
    {
      vertices[0] = &v_[2]; vertices[1] = &v_[0];
      curves[2] = std::make_pair(_segment, vertices);
    }
    return curves;
  }
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Ellipse::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(1);
  ShapeType sh = _ellipse;
  if (isSector_) { sh = _ellipticSector; }
  surfs[0] = std::make_pair(sh, boundNodes());
  return surfs;
}

//! create boundary geometry of ellipse
Geometry& Ellipse::buildBoundary() const
{
  boundaryGeometry_ = new Geometry(boundingBox, 1);
  Geometry& g = *boundaryGeometry_;
  g.shape(_composite);
  g.minimalBox = minimalBox;
  string_t name = "";
  if (domName_ != "") { name = domName_ + "_boundary"; }
  if (name != "") { g.domName(name); }
  std::map<number_t, Geometry*>& cps = g.components();
  std::map<number_t, std::vector<number_t> >& geos = g.geometries();
  Geometry* gi = nullptr;
  number_t i = 0;
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > cs = curves();
  std::vector<std::pair<ShapeType, std::vector<const Point*> > >::iterator itc = cs.begin();
  for (; itc != cs.end(); ++itc, ++i)
  {
    number_t ip1 = i + 1;
    if (ip1 == h_.size()) { ip1 = 0; }
    if (itc->first == _segment)
    {
      if (n_.size() > 0) { gi = new Segment(_v1 = *itc->second.at(0), _v2 = *itc->second.at(1), _nnodes = n_[i]); }
      else { gi = new Segment(_v1 = *itc->second.at(0), _v2 = *itc->second.at(1), _hsteps = Reals(h_[i], h_[ip1])); }
    }
    else
    {
      if (n_.size() > 0) { gi = new EllArc(_center = p_[0], _v1 = *itc->second.at(0), _v2 = *itc->second.at(1), _nnodes = n_[i]); }
      else { gi = new EllArc(_center = p_[0], _v1 = *itc->second.at(0), _v2 = *itc->second.at(1), _hsteps = Reals(h_[i], h_[ip1])); }
    }
    if (sideNames_.size() == 1) { gi->domName(sideNames_[0]); }
    else if (sideNames_.size() > i) { gi->domName(sideNames_[i]); }
    cps[i] = gi;
    geos[i].push_back(i);
  }
  return g;
}

//! parametrization P0 + (P1-P0)*r*cos(s) + (P2-P0)*r*sin(s)  0 <= r <= 1,   0 <= t <= 1, s= thetamin_+ t*(thetamax_-thetamin_)
Vector<real_t> Ellipse::funParametrization(const Point& pt, Parameters& pars, DiffOpType dif) const
{
  real_t dth = thetamax_ - thetamin_;
  real_t r = pt[0], t = thetamin_ + pt[1] * dth, c = std::cos(t), s = std::sin(t);
  number_t dim = p_[0].size();
  Vector<real_t> R(dim);
  switch (dif)
  {
    case _id:
      for (number_t k = 0; k < dim; k++)
      {
        real_t ak = p_[0][k];
        R[k] = ak + r * (c * (p_[1][k] - ak) + s * (p_[2][k] - ak));
      }
      break;
    case _d1:
      for (number_t k = 0; k < dim; k++) //dr
      {
        real_t ak = p_[0][k];
        R[k] = c * (p_[1][k] - ak) + s * (p_[2][k] - ak);
      }
      break;
    case _d2:
      for (number_t k = 0; k < dim; k++) //dt
      {
        real_t ak = p_[0][k];
        R[k] = r * (c * (p_[2][k] - ak) - s * (p_[1][k] - ak));
        R[k] *= dth;
      }
      break;
    case _d11:
      for (number_t k = 0; k < dim; k++) { R[k] = 0.; } //drr
      break;
    case _d22:
      for (number_t k = 0; k < dim; k++) //dtt
      {
        real_t ak = p_[0][k];
        R[k] = -r * (s * (p_[2][k] - ak) + c * (p_[1][k] - ak));
        R[k] *= dth * dth;
      }
      break;
    case _d21:
    case _d12:
      for (number_t k = 0; k < dim; k++) //dtdr
      {
        real_t ak = p_[0][k];
        R[k] = c * (p_[2][k] - ak) - s * (p_[1][k] - ak);
        R[k] *= dth;
      }
      break;
    default: parfun_error("Ellipse parametrization", dif);
  }
  return R;
}

//! inverse of parametrization (u,v)=invf(pt)
Vector<real_t> Ellipse::invParametrization(const Point& pt, Parameters& pars, DiffOpType dif) const
{
  if (dif != _id) { parfun_error("Ellipse::invParametrization", dif); }
  Vector<real_t> uv(2);
  Point p0p = pt - p_[0], p0p1 = p_[1] - p_[0], p0p2 = p_[2] - p_[0];
  //theCout<<"\nEllipse::invParametrization pt="<<pt<<" p0p="<<p0p<<" p0p1="<<p0p1<<" p0p2="<<p0p2<<" thetamin="<<thetamin_<<" thetamax="<<thetamax_<<std::flush;
  //coplanar check
  if (std::abs(dot(crossProduct(p0p1, p0p2), p0p)) > theTolerance) {uv.clear(); return uv;}
  real_t a = dot(p0p1, p0p1), b = dot(p0p2, p0p2), c = dot(p0p1, p0p2), f = dot(p0p, p0p1), g = dot(p0p, p0p2);
  real_t d = a * b - c * c, x = (b * f - c * g) / d, y = (a * g - c * f) / d;
  uv[0] = std::sqrt(x * x + y * y);
  //theCout<<" uv[0]="<<uv[0]<<std::flush;
  if (uv[0] > 1 + theTolerance) {uv.clear(); return uv;} // not ok
  if (uv[0] > 1) { uv[0] = 1; }
  //theCout<<" nuv[0]="<<uv[0]<<std::flush;
  real_t th = std::atan2(y, x);
  //theCout<<" th="<<th<<std::flush;
  //  if (th<-theTolerance) th+=2*pi_;
  //  if (th<0) th=0;
  //try with th
  uv[1] = (th - thetamin_) / (thetamax_ - thetamin_);
  if (uv[1] < -theTolerance || uv[1] > 1 + theTolerance)
  {
    if (uv[1] < -theTolerance && th <= theTolerance) //try with th+2pi
    {
      th += 2 * pi_;
      uv[1] = (th - thetamin_) / (thetamax_ - thetamin_);
      if (uv[1] < -theTolerance || uv[1] > 1 + theTolerance) {uv.clear(); return uv;}
    }
    else if (uv[1] > 1 + theTolerance && th >= 2 * pi_ - theTolerance) //try with th-2pi
    {
      th -= 2 * pi_;
      uv[1] = (th - thetamin_) / (thetamax_ - thetamin_);
      if (uv[1] < -theTolerance || uv[1] > 1 + theTolerance) {uv.clear(); return uv;}
    }
  }
  //theCout<<" uv[1]="<<uv[1]<<std::flush;
  if (uv[1] < -theTolerance || uv[1] > 1 + theTolerance) {uv.clear(); return uv;} // not ok
  if (uv[1] > 1) { uv[1] = 1; }
  if (uv[1] < 0) { uv[1] = 0; }
  //theCout<<" nuv[1]="<<uv[1]<<std::flush;
  return uv;
}

//==========================================================
// Disk class member functions
//===========================================================
Disk::Disk() : Ellipse()
{ shape_ = _disk; }

Disk::Disk(const Point& center, const Point& p1, const Point& p2, const std::vector<number_t>& n, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Ellipse(center, p1, p2, n, domName, sideNames, sideOfSideNames)
{ shape_ = _disk; }

Disk::Disk(const Point& center, const Point& p1, const Point& p2, const std::vector<real_t>& h, const string_t& domName, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames) : Ellipse(center, p1, p2, h, domName, sideNames, sideOfSideNames)
{ shape_ = _disk; }

void Disk::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Disk::build");
  shape_ = _disk;
  p_.resize(5);
  std::set<ParameterKey> params = getParamsKeys(), usedParams;

  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _disk)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or edge_names, not both
    if (key == _pk_edge_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_edge_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_edge_names)); }
    // user must use only one of the following combination: (center,v1,v2) or (center,radius)
    if ((key == _pk_v1 || key == _pk_v2) && usedParams.find(_pk_radius) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_radius)); }
    if (key == _pk_radius && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if (key == _pk_radius && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _edge_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_edge_names); }
  if (usedParams.find(_pk_edge_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // (center,v1,v2) or (center,xlength,ylength) has to be set (no default values)
  if (params.find(_pk_center) != params.end()) { error("param_missing", "center"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  // angle1 and angle2 have to be set both
  if (params.find(_pk_angle1) == params.end() && params.find(_pk_angle2) != params.end()) { error("param_missing", "angle2"); }
  if (params.find(_pk_angle2) == params.end() && params.find(_pk_angle1) != params.end()) { error("param_missing", "angle1"); }

  isAxis_ = true;
  // now, we clean unwanted keys
  if (params.find(_pk_radius) != params.end()) { params.erase(_pk_radius); isAxis_ = false; }
  if (params.find(_pk_v1) != params.end()) { params.erase(_pk_v1); params.erase(_pk_v2); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  // p_ is not (fully) built so we do
  buildPVNAndH();

  if (std::abs(p_[0].distance(p_[1]) - p_[0].distance(p_[2])) > theTolerance)
  { error("geometry_incoherent_points", words("shape", _disk)); }

  computeBB();
  computeMB();

  initParametrization();
  trace_p->pop();
}

void Disk::buildParam(const Parameter& p)
{
  trace_p->push("Disk::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_radius:
    {
      switch (p.type())
      {
        case _integer: xradius_ = yradius_ = real_t(p.get_n()); break;
        case _real: xradius_ = yradius_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Ellipse::buildParam(p); break;
  }
  trace_p->pop();
}

void Disk::buildDefaultParam(ParameterKey key)
{
  Ellipse::buildDefaultParam(key);
}

std::set<ParameterKey> Disk::getParamsKeys()
{
  std::set<ParameterKey> params = Ellipse::getParamsKeys();
  params.insert(_pk_radius);
  // xlength, ylength, xradius, and yradius are replaced by radius
  params.erase(_pk_xlength);
  params.erase(_pk_ylength);
  params.erase(_pk_xradius);
  params.erase(_pk_yradius);
  return params;
}

Disk::Disk(Parameter p1, Parameter p2) : Ellipse()
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

Disk::Disk(Parameter p1, Parameter p2, Parameter p3) : Ellipse()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Disk::Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Ellipse()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Disk::Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Ellipse()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Disk::Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Ellipse()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Disk::Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Ellipse()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Disk::Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Ellipse()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Disk::Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Ellipse()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Disk::Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Ellipse()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Disk::Disk(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11) : Ellipse()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  build(ps);
}

string_t Disk::asString() const
{
  string_t s("Disk (center = ");
  s += p_[0].roundToZero().toString() + ", 1st point " + p_[1].roundToZero().toString() + ", 2nd point" + p_[2].roundToZero().toString() + ")";
  return s;
}

//==========================================================
// ParametrizedSurface class member functions
//===========================================================
//! default is the ParametrizedSurface [0,1]x[0,1]
ParametrizedSurface::ParametrizedSurface() : Surface()
{
  n_.resize(1, 2);
  shape_ = _parametrizedSurface;
  partitioning_ = _linearPartition ;
  nbParts_ = 1;
  transformation_ = nullptr;
  computeMB();
}

//! copy constructor
ParametrizedSurface::ParametrizedSurface(const ParametrizedSurface& ps)
  : Surface(ps), partitioning_(ps.partitioning_), nbParts_(ps.nbParts_)
{
  p_ = ps.p_;
  n_ = ps.n_;
  h_ = ps.h_;
  transformation_ = nullptr;
  if (ps.transformation_ != nullptr) { transformation_ = new Transformation(*ps.transformation_); }
}

void ParametrizedSurface::build(const std::vector<Parameter>& ps)
{
  trace_p->push("ParametrizedSurface::build");
  shape_ = _parametrizedSurface;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _parametrizedSurface)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or edge_names, not both
    if (key == _pk_edge_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_edge_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_edge_names)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // parametrization has to be set (no default values)
  if (params.find(_pk_parametrization) != params.end())  { error("param_missing", "parametrization"); }
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  if (parametrization_->dimg != 2)
  { error("free_error", "building parametrizedSurface requires a 2D->3D parametrization"); }

  // create a mesh of support geometry using nbParts
  real_t hp = std::sqrt(parametrization_->geomSupport().measure() / nbParts_);
  parametrization_->createMesh(hp, _triangle);
  std::vector<Point>& nodes = parametrization_->meshP()->nodes;
  p_.resize(nodes.size());
  std::vector<Point>::iterator itn = nodes.begin(), itp = p_.begin();
  for (; itn != nodes.end(); ++itn, ++itp) { *itp = (*parametrization_)(*itn); }

  //set hsteps
  if (h_.size() != 0)
  {
    if (h_.size() == 1) { h_.resize(p_.size(), h_[0]); }
    else { error("bad_size", "hsteps", 1, h_.size()); }
  }
  transformation_ = nullptr;
  computeBB();
  computeMB();
  trace_p->pop();
}

void ParametrizedSurface::buildParam(const Parameter& p)
{
  trace_p->push("ParametrizedSurface::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_parametrization:
    {
      switch (p.type())
      {
        case _pointerParametrization: parametrization_ = new Parametrization(*reinterpret_cast<const Parametrization*>(p.get_p())); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nnodes:
    {
      switch (p.type())
      {
        case _integer:
        {
          number_t n = p.get_n();
          n_.clear();
          n_.resize(1, (n > 2 ? n : 2));
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_hsteps:
    {
      switch (p.type())
      {
        case _integer: h_ = std::vector<real_t>(1, real_t(p.get_n())); break;
        case _real: h_ = std::vector<real_t>(1, p.get_r()); break;
        case _realVector: h_ = p.get_rv(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_partitioning:
    {
      switch (p.type())
      {
        case _integer:
        case _enumPartitioning:
          partitioning_ = Partitioning(p.get_n());
          break;
        default:
          error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nbParts:
    {
      switch (p.type())
      {
        case _integer:
        {
          number_t n = p.get_n();
          nbParts_ = n > 1 ? n : 1;
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Surface::buildParam(p); break;
  }
  trace_p->pop();
}

void ParametrizedSurface::buildDefaultParam(ParameterKey key)
{
  trace_p->push("ParametrizedSurface::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_ = std::vector<number_t>(1, 2); break;
    case _pk_partitioning: partitioning_ = _nonePartition; break;
    case _pk_nbParts: nbParts_ = 1; break;
    default: Surface::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> ParametrizedSurface::getParamsKeys()
{
  std::set<ParameterKey> params = Surface::getParamsKeys();
  params.insert(_pk_parametrization);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  params.insert(_pk_partitioning);
  params.insert(_pk_nbParts);
  return params;
}

ParametrizedSurface::ParametrizedSurface(Parameter p1, Parameter p2) : Surface()
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

ParametrizedSurface::ParametrizedSurface(Parameter p1, Parameter p2, Parameter p3) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

ParametrizedSurface::ParametrizedSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

ParametrizedSurface::ParametrizedSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

ParametrizedSurface::ParametrizedSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

ParametrizedSurface::ParametrizedSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

ParametrizedSurface::ParametrizedSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

string_t ParametrizedSurface::asString() const
{
  string_t s("ParametrizedSurface (");
  s += parametrization_->name + ")";
  return s;
}

std::vector<const Point*> ParametrizedSurface::nodes() const
{
  std::vector<const Point*> nodes(p_.size());
  for (number_t i = 0; i < p_.size(); ++i) { nodes[i] = &p_[i]; }
  return nodes;
}

// return all nodes after linear split located on boundary (based on the mesh of parameters domain)
std::vector<const Point*> ParametrizedSurface::boundNodes() const
{
  const Mesh& ms = *parametrization_->meshP();
  std::map<string_t, std::vector<GeoNumPair> > sideIndex;
  createSideIndex(ms.elements(), sideIndex);
  std::map<string_t, std::vector<GeoNumPair> >::iterator itm = sideIndex.begin();
  std::vector<number_t> vertIndex(p_.size(), 0);
  for (; itm != sideIndex.end(); ++itm)
  {
    if (itm->second.size() == 1) //edge on boundary
    {
      std::vector<number_t> vs = itm->second[0].first->vertexNumbers(itm->second[0].second);
      vertIndex[vs[0] - 1] += 1; vertIndex[vs[1] - 1] += 1;
    }
  }
  std::vector<const Point*> bns(p_.size());
  std::vector<const Point*>::iterator itp = bns.begin();
  number_t i = 0, k = 0;
  std::vector<number_t>::iterator it = vertIndex.begin();
  for (; it != vertIndex.end(); ++it, i++)
  {
    if (*it != 0)
    {
      *itp = &p_[i];
      itp++; k++;
    }
  }
  if (k > 0) { bns.resize(k); }
  else { bns.clear(); }
  return bns;
}

// return all nodes after linear split located on the side s>=1 of parameters domain with sidenames "s1", "s2", ...
// if s=0 return boundNodes (all sides)
std::vector<const Point*> ParametrizedSurface::nodesOnSide(number_t s) const
{
  if (s == 0) { return boundNodes(); }
  std::vector<const Point*> nodes;
  const Mesh& ms = *parametrization_->meshP();
  const GeomDomain* dom = ms.domainP("s" + tostring(s));
  if (dom == nullptr) { return nodes; } //empty !
  std::set<number_t> ns = dom->meshDomain()->vertexNumbers();
  nodes.resize(ns.size(), nullptr);
  std::set<number_t>::iterator its = ns.begin();
  std::vector<const Point*>::iterator itn = nodes.begin();
  for (; its != ns.end(); ++its, ++itn) { *itn = &p_[*its - 1]; }
  return nodes;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > ParametrizedSurface::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > cs;
  return cs;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > ParametrizedSurface::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > ss(1);
  ss[0] = std::make_pair(_parametrizedSurface, boundNodes());
  return ss;
}

//! create boundary geometry of ParametrizedSurface
/* based on 4 ParametrizedArc (v=0,u=1,v=1,u=0)
*/
Geometry& ParametrizedSurface::buildBoundary() const
{
  boundaryGeometry_ = new Geometry(boundingBox, 1);
  Geometry& g = *boundaryGeometry_;
  g.shape(_composite);
  g.minimalBox = minimalBox;
  string_t name = "";
  if (domName_ != "") { name = domName_ + "_boundary"; }
  if (name != "") { g.domName(name); }
  std::map<number_t, Geometry*>& cps = g.components();
  std::map<number_t, std::vector<number_t> >& geos = g.geometries();
  Parameters params(parametrization_, "surface_parametrization");
  std::vector<Parametrization*> pars(4);
  pars[0] = new Parametrization(0., 1., surfToArcParametrizationv0, params);
  pars[1] = new Parametrization(0., 1., surfToArcParametrizationu1, params);
  pars[2] = new Parametrization(0., 1., surfToArcParametrizationv1, params);
  pars[3] = new Parametrization(0., 1., surfToArcParametrizationu0, params);
  Geometry* gi = nullptr;
  for (number_t i = 0; i < 4; i++)
  {
    number_t ip1 = i + 1;
    if (ip1 == 4) { ip1 = 0; }
    if (withNnodes()) { gi = new ParametrizedArc(_parametrization = *pars[i], _nnodes = n_[i]); }
    else { gi = new ParametrizedArc(_parametrization = *pars[i], _hsteps = Reals(h_[i], h_[ip1])); }
    if (sideNames_.size() == 1) { gi->domName(sideNames_[0]); }
    else if (sideNames_.size() > i) { gi->domName(sideNames_[i]); }
    cps[i] = gi;
    geos[i].push_back(i);
  }
  return g;
}


//==========================================================
// SplineSurface class member functions
//===========================================================

//! default is a void SplineSurface (spline_=0)
SplineSurface::SplineSurface() : Surface(), spline_(nullptr)
{
  shape_ = _splineSurface;
}

//! effective building function from parameter keys
void SplineSurface::build(const std::vector<Parameter>& ps)
{
  trace_p->push("SplineSurface::build");
  shape_ = _splineSurface;
  SplineBC bc = _undefBC;
  SplineParametrization par = _undefParametrization;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  std::vector<real_t> weights;
  number_t nbu = 0;
  // managing params
  std::vector<Point> controlPoints;
  SplineType type = _Nurbs;
  SplineSubtype subtype = _SplineInterpolation;
  number_t deg = 3;
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    const Parameter& p = ps[i];
    switch (key)
    {
      case _pk_spline:
      {
        if (p.type() == _pointerSpline) { spline_ = (reinterpret_cast<const Spline*>(p.get_p()))->clone(); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_spline_type:
      {
        if (p.type() == _integer) { type = SplineType(p.get_n()); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_spline_subtype:
      {
        if (p.type() == _integer) { subtype = SplineSubtype(p.get_n()); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_spline_BC:
      {
        if (p.type() == _integer) { bc = SplineBC(p.get_n()); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_degree:
      {
        if (p.type() == _integer) { deg = p.get_n(); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_spline_parametrization:
      {
        if (p.type() == _integer) { par = SplineParametrization(p.get_n()); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_weights:
      {
        if (p.type() == _realVector) { weights = p.get_rv(); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_vertices:
      {
        if (p.type() == _ptVector) { controlPoints = *reinterpret_cast<const std::vector<Point>*>(p.get_p()); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_nbu:
      {
        if (p.type() == _integer) { nbu = p.get_n(); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_nnodes:
      {
        if (p.type() == _integer) { n_[0] = std::max(number_t(2), p.get_n()); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_hsteps:
      {
        switch (p.type())
        {
          case _integer: h_ = std::vector<real_t>(1, real_t(p.get_n())); break;
          case _real: h_ = std::vector<real_t>(1, p.get_r()); break;
          case _realVector: h_ = p.get_rv(); break;
          default: error("param_badtype", words("value", p.type()), words("param key", key));
        }
        break;
      }
      default: Surface::buildParam(p); break;
    }
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _parametrizedArc)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);

    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or edge_names, not both
    if (key == _pk_edge_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_edge_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_edge_names)); }
  }

  // check spline pointer or vertices
  if (params.find(_pk_spline) != params.end() && params.find(_pk_vertices) != params.end()) { error("param_missing", "_spline or _vertices"); }
  if (params.find(_pk_vertices) == params.end() && params.find(_pk_nbu) != params.end()) { error("param_missing", "nb_u"); }
  if (params.find(_pk_vertices) != params.end() && params.find(_pk_nbu) == params.end()) { error("param_missing", "_vertices"); }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _edge_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_edge_names); }
  if (usedParams.find(_pk_edge_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // check type, subtype and spline parametrization
  if (type != _Nurbs) { error("free_error", "only _Nurbs type is available in SplineSurface"); }
  if (subtype == _noSplineSubtype) { subtype = _SplineInterpolation; } //default subtype is interpolation
  if (par == _undefParametrization)  { par = _centripetalParametrization; }

  // check undef parameter
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  // build nurbs if not set by user
  if (spline_ == nullptr) { spline_ = new Nurbs(subtype, controlPoints, nbu, deg, deg, bc, bc, bc, bc, weights); } // build spline (nurbs) object

  // check h size and correct it in order to have size(h)=controlPoints.size()
  number_t nh = h_.size(), nc = spline_->nurbs()->nbcp();
  if (nh != 0)
  {
    if (nh == 1) { h_ = std::vector<real_t>(nc, h_[0]); }
    else
    {
      int_t m = nc - nh;
      switch (m)
      {
        case 0: break;
        case 1: h_.insert(h_.begin(), h_[0]); break;
        case 2: h_.insert(h_.begin(), h_[0]); h_.push_back(h_[nh]); break;
        default: error("bad_size", "hsteps", nc, nh);
      }
    }
  }

  //set parametrization
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  parametrization_ = new Parametrization(0., 1., 0., 1., parametrization_SplineSurface, pars, "SplineSurface parametrization");
  parametrization_->setinvParametrization(invParametrization_SplineSurface);
  computeBB();
  computeMB();

  // set p_
  p_.resize(4);
  p_[0] = spline_->nurbs()->evaluate(0., 0.); p_[1] = spline_->nurbs()->evaluate(1., 0.);
  p_[2] = spline_->nurbs()->evaluate(1., 1.); p_[3] = spline_->nurbs()->evaluate(0., 1.);
  trace_p->pop();
}

void SplineSurface::buildParam(const Parameter& p)
{
  trace_p->push("SplineSurface::buildParam");
  ParameterKey key = p.key();
  trace_p->pop();
}

void SplineSurface::buildDefaultParam(ParameterKey key)
{
  trace_p->push("SplineSurface::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_ = std::vector<number_t>(1, 2); break;
    case _pk_spline: spline_ = 0; break;
    case _pk_hsteps:           // optional parameters not managed by the class
    case _pk_vertices:        // local management - no default value
    case _pk_weights:
    case _pk_nbu:
    case _pk_degree:
    case _pk_spline_BC:
    case _pk_spline_parametrization:
    case _pk_spline_type:
    case _pk_spline_subtype: break;
    default: Surface::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> SplineSurface::getParamsKeys()
{
  std::set<ParameterKey> params = Surface::getParamsKeys();
  params.insert(_pk_vertices);  // control points
  params.insert(_pk_weights);   // weights
  params.insert(_pk_nbu);       // number of u control points
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  params.insert(_pk_spline);         // nurbs pointer
  params.insert(_pk_spline_type);    // only Nurbs up to now
  params.insert(_pk_spline_subtype); // splineInterpolation or splineApproximation
  params.insert(_pk_degree);         // degree
  params.insert(_pk_spline_BC);
  params.insert(_pk_spline_parametrization);
  return params;
}

SplineSurface::SplineSurface(Parameter p1) : Surface()
{
  std::vector<Parameter> ps={p1};
  build(ps);
}

SplineSurface::SplineSurface(Parameter p1, Parameter p2) : Surface()
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

SplineSurface::SplineSurface(Parameter p1, Parameter p2, Parameter p3) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

SplineSurface::SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

SplineSurface::SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

SplineSurface::SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

SplineSurface::SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

SplineSurface::SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

SplineSurface::SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

SplineSurface::SplineSurface(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Surface()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

//! copy constructor
SplineSurface::SplineSurface(const SplineSurface& sps) : Surface(sps)
{
  spline_ = 0;
  copy(sps);
}

//! assign operator
SplineSurface& SplineSurface::operator=(const SplineSurface& sps)
{
  if (&sps == this) { return *this; }
  if (spline_ != nullptr) { delete spline_; }
  spline_ = 0;
  copy(sps);
  return *this;
}

//! copy tool (hard copy)
void SplineSurface::copy(const SplineSurface& sps)
{
  if (&sps == this) { return; }
  if (sps.spline_ != nullptr) { spline_ = sps.spline_->clone(); }
  p_ = sps.p_;
  n_ = sps.n_;
  h_ = sps.h_;
}

//! destructor
SplineSurface::~SplineSurface()
{
  if (spline_ != nullptr) { delete spline_; }
}

string_t SplineSurface::asString() const
{
  string_t s("SplineSurface (");
  s += parametrization_->name + ")";
  return s;
}

/*! create boundary geometry of SplineSurface (nurbs), a composite geometry of 4 SplineArc defined from BSpline
    with the following weights and control points built from weights and control points of Nurbs
       wi = sum_j wij.Bjq(v) and Pi=sum_j wij.Bjq(v)Pij/wi    v=0 or v=1
       wj = sum_i wij.Bip(u) and Pi=sum_i wij.Bip(u)Pij/wi    u=0 or u=1
    note that when wij=1 for any i,j, wi=wj=1 for any i,j
*/
Geometry& SplineSurface::buildBoundary() const
{
  boundaryGeometry_ = new Geometry(boundingBox, 1);
  Geometry& g = *boundaryGeometry_;
  g.shape(_composite);
  g.minimalBox = minimalBox;
  string_t name = "";
  if (domName_ != "") { name = domName_ + "_boundary"; }
  if (name != "") { g.domName(name); }
  std::map<number_t, Geometry*>& cps = g.components();
  std::map<number_t, std::vector<number_t> >& geos = g.geometries();
  const Nurbs* nurbs = spline_->nurbs();
  const BSpline* bs_uv = nullptr;
  const std::vector<std::vector<Point> >& cpts = nurbs->controlPointsM(); // control points of nurbs
  const std::vector<std::vector<real_t> > ws = nurbs->weightsM();        // weights of nurbs
  Vector<real_t> B, dB, d2B;
  number_t k = 0;
  bool noWeight = nurbs->noWeight();

  for (number_t s = 0; s < 4; s++)
  {
    number_t m;
    if (s == 0 || s == 2) {bs_uv = nurbs->bsv(); m = cpts.size();}
    else {bs_uv = nurbs->bsu(); m = cpts[0].size();}
    real_t t = 0.;
    if (s == 1 || s == 2) { t = 1.; }
    bs_uv->computeB(bs_uv->toKnotsParameter(t), _id, k, B, dB, d2B);
    number_t a = k - bs_uv->degree();
    real_t n = B.size();
    std::vector<Point> pts(m, Point(0., 0., 0.)); //control points of uv-Bspline
    std::vector<real_t> w;               //weights of uv-Bspline
    if (noWeight)  // wij=1
    {
      if (s == 0 || s == 2)
        for (number_t i = 0; i < m; i++)
          for (number_t j = a; j <= k; j++) { pts[i] += cpts[i][j] * B[j]; }
      else
        for (number_t i = 0; i < m; i++)
          for (number_t j = a; j <= k; j++) { pts[i] += cpts[j][i] * B[j]; }
    }
    else
    {
      w.resize(m, 0.);
      if (s == 0 || s == 2)
        for (number_t i = 0; i < m; i++)
        {
          for (number_t j = a; j <= k; j++)
          {
            pts[i] += ws[i][j] * cpts[i][j] * B[j];
            w[i] += ws[i][j] * B[j];
          }
          pts[i] /= w[i];
        }
      else
        for (number_t i = 0; i < m; i++)
        {
          for (number_t j = a; j <= k; j++)
          {
            pts[i] += ws[j][i] * cpts[j][i] * B[j];
            w[i] += ws[i][i] * B[j];
          }
          pts[i] /= w[i];
        }
    }
    string_t name = "";
    if (sideNames_.size() == 1) { name = sideNames_[0]; }
    else if (sideNames_.size() > s) { name = sideNames_[s]; }
    BSpline* bs = nullptr;
    if (noWeight) { bs = new BSpline(pts, bs_uv->degree()); }
    else { bs = new BSpline(pts, bs_uv->degree(), _undefBC, _undefBC, w); }
    if (n_.size() > 0) { cps[s] = new SplineArc(_spline = *bs, _nnodes = n_[0], _domain_name = name); }
    else { cps[s] = new SplineArc(_spline = bs, _hsteps = h_[0], _domain_name = name); }
    geos[s].push_back(s);
    delete bs;
  }
  return g;
}

//!< interface to nurbs parametrization (u,v in [0,1])
Vector<real_t> SplineSurface::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  const Parametrization& par_spline = spline_->parametrization();
  if (pt[0] < -theTolerance || pt[0] > 1 + theTolerance)
  { error("free_error", "in Splinesurface, parameter " + tostring(pt[0]) + " is outside parameter intervall [0,1]"); }
  if (pt[1] < -theTolerance || pt[1] > 1 + theTolerance)
  { error("free_error", "in Splinesurface, parameter " + tostring(pt[1]) + " is outside parameter intervall [0,1]"); }
  Vector<real_t> res = par_spline(pt, d);
  return res;
}

//!< inverse of parametrization
Vector<real_t> SplineSurface::invParametrization(const Point& p, Parameters& pars, DiffOpType d) const
{
  error("free_error", "in SplineSurface, no inverse");
  return Vector<real_t>();
}


//==========================================================
// SetOfElems class member functions
//===========================================================
string_t SetOfElems::asString() const
{
  string_t s("SetOfElems (");
  s += tostring(pts_.size()) + " points, " + tostring(nbElems_) + " elements, " + tostring(nbBounds_) + " boundaries)";
  return s;
}

std::vector<Point*> SetOfElems::nodes()
{
  std::vector<Point*> nodes(pts_.size());
  for (number_t i = 0; i < pts_.size(); i++)
  {
    nodes[i] = &pts_[i];
  }
  return nodes;
}

std::vector<const Point*> SetOfElems::nodes() const
{
  std::vector<const Point*> nodes(pts_.size());
  for (number_t i = 0; i < pts_.size(); i++)
  {
    nodes[i] = &pts_[i];
  }
  return nodes;
}

} // end of namespace xlifepp
