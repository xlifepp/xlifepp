/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DomainMap.hpp
  \author E. Lunéville
  \since 04 apr 2012
  \date 04 apr 2012

  \brief Definition of xlifepp::DomainMap class and functionalities
 */

#ifndef DOMAIN_MAP_HPP
#define DOMAIN_MAP_HPP

#include "config.h"
#include "utils.h"
#include "GeomDomain.hpp"

namespace xlifepp
{

/*!
    \class DomainMap
    handles map (declared as Function) between two GeomDomain's.
    Only one map between domains may be defined !!!
 */
class DomainMap
{
  protected:
    const GeomDomain* dom1_p; //!< start domain
    const GeomDomain* dom2_p; //!< end domain
    Function map1to2_;        //!< map function from domain1 to domain2
    DomainMap(){}             //!< shadow default constructor

  public:
    bool useNearest;          //!< use nearest method instead of locate method after mapping
    string_t name;            //!< a unique name
    DomainMap(const GeomDomain&, const GeomDomain&, const Function&, bool nearest=false);  //!< explicit constructor (no name)
    DomainMap(const GeomDomain&, const GeomDomain&, const Function&, const string_t& n, bool nearest=false);  //!< explicit constructor
    ~DomainMap(); //!< destructor

    //list of all DomainMap and related tools
    static std::map<string_t, DomainMap *> theDomainMaps; //!< list of existing maps indexed by name
    static void clearGlobalVector();               //!< delete all map objects
    static void removeMaps(const GeomDomain&);     //!< delete and remove maps involving a given domain

    //accessors
    const GeomDomain& dom1() const //! returns start domain
    {return *dom1_p;}
    const GeomDomain& dom2() const //! returns end domain
    {return *dom2_p;}
    const Function& map1to2() const //! returns map function
    {return map1to2_;}
};

void defineMap(const GeomDomain&, const GeomDomain&, const Function&, bool nearest=false); //!< define a map between 2 geomdomains, default name
void defineMap(const GeomDomain&, const GeomDomain&, const Function&, const string_t&, bool nearest=false); //!< define a map between 2 geomdomains with explicit name
const DomainMap* domainMap(const GeomDomain&, const GeomDomain&);      //!< find DomainMap from dom1 to dom2
const Function* findMap(const GeomDomain&, const GeomDomain&);         //!< find map between 2 geomdomains
const Function* buildMap(const GeomDomain&, const GeomDomain&);        //!< build map from dom1 to dom2 in simple cases
Vector<real_t> domainTranslation(const Point&, Parameters& pa = defaultParameters); //!< default translation for domain map


} // end of namespace xlifepp

#endif // DOMAIN_MAP_HPP
